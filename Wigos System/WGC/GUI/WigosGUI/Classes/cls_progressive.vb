'-------------------------------------------------------------------
' Copyright � 2014 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_progressive.vb
'
' DESCRIPTION:   Progressive Jackpot class
'
' AUTHOR:        Jes�s �ngel Blanco
'
' CREATION DATE: 08-AUG-2014
'
' REVISION HISTORY:
'
' Date         Author     Description
' -----------  ---------  -----------------------------------------------
' 08-AUG-2014  JAB        Initial version.
' 09-SEP-2014  FJC        Change 'InserProgressive'  Function with @parameters
' 12-SEP-2014  FJC        Change 'UpdateProgressive' Function with @parameters (update/insert levels only)
' 13-OCT-2014  JMV        Saving contribution in terminals at UpdateProgressive()
' 15-DEC-2014  JMM        Allow to modify levels amount
' 25-FEB-2015  FJC        Fixed Bug WIG-2108: Error when we are removing existing levels from a progressive.
' 21-NOV-2016  GMV        Product Backlog Item 19930:Jackpots - GUI - Show on WinUp setting
' 08-FEB-2018  LTC        PBI 31459: WIGOS-8097 AGG - Progressive Jackpots configuration for brand, server and multiseat 
'------------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports WSI.Common
Imports System.Text

Public Class CLASS_PROGRESSIVE
  Inherits CLASS_BASE

#Region " Const "

  ' Grid Columns
  Private Const GRID_COLUMN_PGL_LEVEL_ID As Integer = 0
  Private Const GRID_COLUMN_PGL_NAME As Integer = 1
  Private Const GRID_COLUMN_PGL_CONTRIBUTION_PCT As Integer = 2
  Private Const GRID_COLUMN_PGL_AMOUNT As Integer = 3
  Private Const GRID_COLUMN_PGL_SHOW_ON_WINUP As Integer = 5

#End Region

#Region " Structures "

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_PROGRESSIVE
    Public progressive_id As Int64
    Public progressive_name As String
    Public progressive_created As DateTime
    Public progressive_contribution_pct As Decimal
    Public progressive_num_levels As Integer
    Public progressive_amount As Decimal
    Public progressive_levels_list As DataTable
    Public progressive_status As Boolean
    Public progressive_status_changed As DateTime
    Public progressive_last_provisioned_hour As DateTime

    Public progressive_terminal_list As New TerminalList
    Public progressive_screen_mode As Integer

    ' Progressive HandPay
    Public progressive_is_handpay As Boolean = False
    Public progressive_current_level_id As Integer
    Public progressive_current_level_name As String
    Public progressive_current_terminal_id As Integer
    Public progressive_current_terminal_name As String
    Public progressive_handpay_amount As Decimal
    ' LTC 2018-FEB-08
    Public progressive_brand As String
    Public progressive_server As String
    Public progressive_is_multiseat As Boolean = False

  End Class

#End Region

#Region " Enums "

  Public Enum ENUM_PROGRESSIVE_SCREEN_MODE
    MODE_EDIT = 0
    MODE_NEW = 1
  End Enum

  Public Enum ENUM_PROGRESSIVE_LEVELS
    LEVEL_DELETEABLE = 0
    LEVEL_NOT_DELETEABLE_AMOUNT_DIFF_THAN_ZERO = 1
    LEVEL_NOT_DELETEABLE_PROVISIONED_LEVEL = 2
    LEVEL_NOT_DELETEABLE_ISNT_LAST_LEVEL_GRID = 3
  End Enum

#End Region

#Region " Members "

  Protected m_progressive As TYPE_PROGRESSIVE
  Protected m_terminal_list As DataTable
  ''Private m_deleteable_level As ENUM_PROGRESSIVE_DELETEABLE_LEVELS
#End Region

#Region " Properties "

  Property ProgressiveId() As Int64
    Get
      Return m_progressive.progressive_id
    End Get
    Set(ByVal Value As Int64)
      m_progressive.progressive_id = Value
    End Set
  End Property ' ProgresiveId

  Property ProgressiveName() As String
    Get
      Return m_progressive.progressive_name
    End Get
    Set(ByVal Value As String)
      m_progressive.progressive_name = Value
    End Set
  End Property ' ProgresiveName

  Property ProgressiveCreated() As DateTime
    Get
      Return m_progressive.progressive_created
    End Get
    Set(ByVal Value As DateTime)
      m_progressive.progressive_created = Value
    End Set
  End Property ' ProgressiveCreated

  Property ProgressiveContributionPct() As Decimal
    Get
      Return m_progressive.progressive_contribution_pct
    End Get
    Set(ByVal Value As Decimal)
      m_progressive.progressive_contribution_pct = Value
    End Set
  End Property ' ProgressiveContributionPct

  Property ProgressiveNumLevels() As Integer
    Get
      Return m_progressive.progressive_num_levels
    End Get
    Set(ByVal Value As Integer)
      m_progressive.progressive_num_levels = Value
    End Set
  End Property ' ProgressiveNumLevels

  Property ProgressiveAmount() As Decimal
    Get
      Return m_progressive.progressive_amount
    End Get
    Set(ByVal Value As Decimal)
      m_progressive.progressive_amount = Value
    End Set
  End Property ' ProgressiveAmount

  Property ProgressiveLevelsList() As DataTable
    Get
      Return m_progressive.progressive_levels_list
    End Get
    Set(ByVal Value As DataTable)
      m_progressive.progressive_levels_list = Value
    End Set
  End Property ' ProgressiveLevelsList

  Property ProgressiveStatus() As Boolean
    Get
      Return m_progressive.progressive_status
    End Get
    Set(ByVal Value As Boolean)
      m_progressive.progressive_status = Value
    End Set
  End Property ' ProgressiveStatus

  Property ProgressiveStatusChanged() As DateTime
    Get
      Return m_progressive.progressive_status_changed
    End Get
    Set(ByVal Value As DateTime)
      m_progressive.progressive_status_changed = Value
    End Set
  End Property ' ProgressiveStatusChanged

  Property ProgressiveLastProvisionedHour() As DateTime
    Get
      Return m_progressive.progressive_last_provisioned_hour
    End Get
    Set(ByVal Value As DateTime)
      m_progressive.progressive_last_provisioned_hour = Value
    End Set
  End Property ' ProgressiveLastProvisionedHour

  Public Property ProgressiveScreenMode() As Integer
    Get
      Return m_progressive.progressive_screen_mode
    End Get
    Set(ByVal Value As Integer)
      m_progressive.progressive_screen_mode = Value
    End Set
  End Property ' ProgressiveScreenMode

  Public Property ProgressiveTerminalList() As TerminalList
    Get
      Return m_progressive.progressive_terminal_list
    End Get
    Set(ByVal Value As TerminalList)
      m_progressive.progressive_terminal_list = Value
    End Set
  End Property ' ProgressiveTerminalList

  Public Property ProgressiveIsHandPay() As Boolean
    Get
      Return m_progressive.progressive_is_handpay
    End Get
    Set(ByVal Value As Boolean)
      m_progressive.progressive_is_handpay = Value
    End Set
  End Property ' ProgressiveIsHandPay

  Public Property ProgressiveCurrentLevelId() As Integer
    Get
      Return m_progressive.progressive_current_level_id
    End Get
    Set(ByVal Value As Integer)
      m_progressive.progressive_current_level_id = Value
    End Set
  End Property ' ProgressiveCurrentLevel

  Public Property ProgressiveCurrentLevelName() As String
    Get
      Return m_progressive.progressive_current_level_name
    End Get
    Set(ByVal Value As String)
      m_progressive.progressive_current_level_name = Value
    End Set
  End Property ' ProgressiveCurrentLevelName

  Public Property ProgressiveCurrentTerminalId() As Integer
    Get
      Return m_progressive.progressive_current_terminal_id
    End Get
    Set(ByVal Value As Integer)
      m_progressive.progressive_current_terminal_id = Value
    End Set
  End Property ' ProgressiveCurrentTerminal

  Public Property ProgressiveCurrentTerminalName() As String
    Get
      Return m_progressive.progressive_current_terminal_name
    End Get
    Set(ByVal Value As String)
      m_progressive.progressive_current_terminal_name = Value
    End Set
  End Property ' ProgressiveCurrentTerminalName

  Public Property ProgressiveHandpayAmount() As Decimal
    Get
      Return m_progressive.progressive_handpay_amount
    End Get
    Set(ByVal Value As Decimal)
      m_progressive.progressive_handpay_amount = Value
    End Set
  End Property ' ProgressiveHandpayAmount

  Public Property TerminalList() As DataTable
    Get
      Return m_terminal_list
    End Get
    Set(ByVal Value As DataTable)
      m_terminal_list = Value
    End Set
  End Property ' ProgressiveLevelsList

  ' LTC 2018-FEB-08
  Property ProgressiveBrand() As String
    Get
      Return m_progressive.progressive_brand
    End Get
    Set(ByVal Value As String)
      m_progressive.progressive_brand = Value
    End Set
  End Property ' ProgressiveBrand

  Property ProgressiveServer() As String
    Get
      Return m_progressive.progressive_server
    End Get
    Set(ByVal Value As String)
      m_progressive.progressive_server = Value
    End Set
  End Property ' ProgressiveServer

  Property ProgressiveIsMultiseat() As Boolean
    Get
      Return m_progressive.progressive_is_multiseat
    End Get
    Set(ByVal Value As Boolean)
      m_progressive.progressive_is_multiseat = Value
    End Set
  End Property ' ProgressiveIsUniqueMachine

#End Region

#Region " Constructor / Destructor "

  Public Sub New()
    Call MyBase.New()
    Me.m_progressive = New TYPE_PROGRESSIVE

  End Sub ' New

  Protected Overrides Sub Finalize()
    MyBase.Finalize()
  End Sub ' Finalize

#End Region

#Region " Overrides "

  '----------------------------------------------------------------------------
  ' PURPOSE: Returns the related auditor data for the current object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  '
  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _aux_field_name As String
    Dim _temp As String

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_PROGRESSIVE)

    ' Progressive(award)
    If ProgressiveIsHandPay Then

      ' Progressive award
      _auditor_data.SetName(0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5397))                            ' Progresivo otorgado

      _temp = ""
      _temp = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5217) & ": " & ProgressiveName & ", " & _
              GLB_NLS_GUI_PLAYER_TRACKING.GetString(5281) & ": " & ProgressiveCurrentLevelName & ", " & _
              GLB_NLS_GUI_PLAYER_TRACKING.GetString(5214) & ": " & ProgressiveHandpayAmount & ", " & _
              GLB_NLS_GUI_PLAYER_TRACKING.GetString(2254) & ": " & ProgressiveCurrentTerminalName
      _auditor_data.SetField(0, _temp, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5034), CLASS_AUDITOR_DATA.ENUM_FIELD_TYPE.FIELD_ALWAYS_VISIBLE)

    Else

      ' Progressive(Edit)
      _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(5202), ProgressiveName)
      _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5209), ProgressiveName)                     ' Nombre
      _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5213), GUI_FormatNumber(ProgressiveContributionPct, 4, ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE) & "%")    ' Contribuci�n
      _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5214), GUI_FormatCurrency(ProgressiveAmount, 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE, True))   ' Monto
      _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5211), IIf(ProgressiveStatus, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5218), _
                                                                                          GLB_NLS_GUI_PLAYER_TRACKING.GetString(5219)))                   ' Estatus
      _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5210), GUI_FormatNumber(ProgressiveNumLevels))                ' Niveles

      ' Assigned terminals
      _temp = ""
      _temp = ProgressiveTerminalList.AuditStringList(WSI.Common.GROUP_ELEMENT_TYPE.TERM, "")
      _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5352), IIf(String.IsNullOrEmpty(_temp), AUDIT_NONE_STRING, _temp))

      ' Assigned all terminals from provider
      _temp = ""
      _temp = ProgressiveTerminalList.AuditStringList(WSI.Common.GROUP_ELEMENT_TYPE.PROV, "")
      _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5353), IIf(String.IsNullOrEmpty(_temp), AUDIT_NONE_STRING, _temp))

      ' Zones
      _temp = ""
      _temp = ProgressiveTerminalList.AuditStringList(WSI.Common.GROUP_ELEMENT_TYPE.ZONE, "")
      _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5356), IIf(String.IsNullOrEmpty(_temp), AUDIT_NONE_STRING, _temp))

      ' Groups
      _temp = ""
      _temp = ProgressiveTerminalList.AuditStringList(WSI.Common.GROUP_ELEMENT_TYPE.GROUP, "")
      _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5357), IIf(String.IsNullOrEmpty(_temp), AUDIT_NONE_STRING, _temp))

      ' LTC 2018-FEB-08
      ' Brand
      _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(9014), ProgressiveBrand)

      ' Server
      _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(9015), ProgressiveServer)

      ' Unique Machine
      _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(9012), ProgressiveIsMultiseat)


      If ProgressiveLevelsList.Rows.Count > 0 Then
        For Each _row As DataRow In ProgressiveLevelsList.Rows
          ' Level Id
          _aux_field_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5281) & " " & _row(0).ToString() & " (" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5282) & ")"
          _auditor_data.SetField(0, _row(0).ToString(), _aux_field_name)

          ' Level name
          _aux_field_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5281) & " " & _row(0).ToString() & " (" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5209) & ")"
          _auditor_data.SetField(0, _row(1).ToString(), _aux_field_name)

          ' Level contribution
          _aux_field_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5281) & " " & _row(0).ToString() & " (" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5213) & ")"
          _auditor_data.SetField(0, GUI_FormatNumber(_row(2), 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE) & "%", _aux_field_name)

          ' Level amount
          _aux_field_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5281) & " " & _row(0).ToString() & " (" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5214) & ")"
          _auditor_data.SetField(0, GUI_FormatCurrency(_row(3), 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE, True), _aux_field_name)

          ' Show On Winup
          If WSI.Common.GeneralParam.GetBoolean("Features", "WinUP", False) Then
            _aux_field_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5281) & " " & _row(0).ToString() & " (" & GLB_NLS_GUI_JACKPOT_MGR.GetString(499) & ")"
            _auditor_data.SetField(0, _row(5), _aux_field_name)
          End If


        Next
      End If

    End If

    Return _auditor_data
  End Function ' AuditorData

  '----------------------------------------------------------------------------
  ' PURPOSE: Can't delete the given object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  '
  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
  End Function 'DB_Delete

  '----------------------------------------------------------------------------
  ' PURPOSE: Can't insert the given object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  '
  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As ENUM_STATUS
    Dim _rc As Integer

    If ProgressiveIsHandPay Then
      ' Insert progressive handpay
      '''_rc = IIf(CreateProgressiveHandPay(ProgressiveId, ProgressiveCurrentLevelId, ProgressiveHandpayAmount, ProgressiveCurrentTerminalId), ENUM_CONFIGURATION_RC.CONFIGURATION_OK, ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB)
    Else
      ' Insert progressive
      _rc = InsertProgressive()
    End If

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
        Return ENUM_STATUS.STATUS_DUPLICATE_KEY

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select
  End Function 'DB_Insert

  '----------------------------------------------------------------------------
  ' PURPOSE: Reads from the database into the given object
  '
  ' PARAMS:
  '   - INPUT:
  '     - ObjectId: An object, it must be 'casted' to the desired KEY.
  '
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  '
  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As ENUM_STATUS

    Dim _rc As Integer

    ' Read Progressive information
    If ObjectId <> -1 Then
      _rc = GetProgressive(ObjectId)
    End If

    Select Case _rc
      Case ENUM_DB_RC.DB_RC_OK
        Return ENUM_STATUS.STATUS_OK

      Case ENUM_DB_RC.DB_RC_ERROR_NOT_FOUND
        Return CLASS_BASE.ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function   'DB_Read

  '----------------------------------------------------------------------------
  ' PURPOSE: Updates the given object to the database.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  '
  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _rc As Integer

    If ProgressiveIsHandPay Then
      ' Insert progressive handpay
      Using _db_trx As New DB_TRX()
        _rc = IIf(WSI.Common.Progressives.RegisterProgressiveJackpotAwarded(ProgressiveId, _
                                                                 ProgressiveCurrentLevelId, _
                                                                 ProgressiveHandpayAmount, _
                                                                 ProgressiveCurrentTerminalId, _
                                                                 _db_trx.SqlTransaction()), _
                  ENUM_CONFIGURATION_RC.CONFIGURATION_OK, _
                  ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB)

        If (_rc = ENUM_CONFIGURATION_RC.CONFIGURATION_OK) Then
          _db_trx.Commit()
        Else
          _db_trx.Rollback()
        End If

      End Using

    Else
      ' Update progressive
      _rc = UpdateProgressive()
    End If

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
        Return ENUM_STATUS.STATUS_DUPLICATE_KEY

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return CLASS_BASE.ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function 'DB_Update

  '----------------------------------------------------------------------------
  ' PURPOSE: Returns a duplicate of the given object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim _temp As CLASS_PROGRESSIVE

    _temp = New CLASS_PROGRESSIVE

    _temp.ProgressiveId = Me.m_progressive.progressive_id
    _temp.ProgressiveName = Me.m_progressive.progressive_name
    _temp.ProgressiveCreated = Me.m_progressive.progressive_created
    _temp.ProgressiveContributionPct = Me.m_progressive.progressive_contribution_pct
    _temp.ProgressiveNumLevels = Me.m_progressive.progressive_num_levels
    _temp.ProgressiveAmount = Me.m_progressive.progressive_amount
    _temp.ProgressiveLevelsList = Me.m_progressive.progressive_levels_list.Copy()
    _temp.ProgressiveStatus = Me.m_progressive.progressive_status
    _temp.ProgressiveStatusChanged = Me.m_progressive.progressive_status_changed
    _temp.ProgressiveLastProvisionedHour = Me.m_progressive.progressive_last_provisioned_hour

    ' LTC 2018-FEB-08
    _temp.ProgressiveBrand = Me.ProgressiveBrand
    _temp.ProgressiveServer = Me.ProgressiveServer
    _temp.ProgressiveIsMultiseat = Me.ProgressiveIsMultiseat

    _temp.ProgressiveTerminalList = New TerminalList()
    _temp.ProgressiveTerminalList.FromXml(Me.ProgressiveTerminalList.ToXml)

    _temp.ProgressiveScreenMode = Me.ProgressiveScreenMode

    _temp.ProgressiveIsHandPay = Me.ProgressiveIsHandPay
    _temp.ProgressiveCurrentLevelId = Me.ProgressiveCurrentLevelId
    _temp.ProgressiveCurrentLevelName = Me.ProgressiveCurrentLevelName
    _temp.ProgressiveCurrentTerminalId = Me.ProgressiveCurrentTerminalId
    _temp.ProgressiveCurrentTerminalName = Me.ProgressiveCurrentTerminalName
    _temp.ProgressiveHandpayAmount = Me.ProgressiveHandpayAmount

    Return _temp

  End Function 'Duplicate 

#End Region

#Region " Private "

  '----------------------------------------------------------------------------
  ' PURPOSE: Insert the progressive information.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  '
  Private Function GetProgressive(ByVal ItemProgressiveId As Integer) As Integer
    Dim _sb_progressive As StringBuilder
    Dim _sb_progressive_levels As StringBuilder
    Dim _dt_progressive As DataTable
    Dim _dt_progressive_levels As DataTable

    Try
      ' LTC 2018-FEB-08
      ' Progressive Data
      _sb_progressive = New StringBuilder()
      _sb_progressive.AppendLine("      SELECT   PGS_PROGRESSIVE_ID                           ")
      _sb_progressive.AppendLine("             , PGS_NAME                                     ")
      _sb_progressive.AppendLine("             , PGS_CREATED                                  ")
      _sb_progressive.AppendLine("             , PGS_CONTRIBUTION_PCT                         ")
      _sb_progressive.AppendLine("             , PGS_NUM_LEVELS                               ")
      _sb_progressive.AppendLine("             , PGS_AMOUNT                                   ")
      _sb_progressive.AppendLine("             , PGS_TERMINAL_LIST                            ")
      _sb_progressive.AppendLine("             , PGS_STATUS                                   ")
      _sb_progressive.AppendLine("             , PGS_STATUS_CHANGED                           ")
      _sb_progressive.AppendLine("             , PGS_LAST_PROVISIONED_HOUR                    ")
      _sb_progressive.AppendLine("             , PGS_BRAND                                    ")
      _sb_progressive.AppendLine("             , PGS_SERVER                                   ")
      _sb_progressive.AppendLine("             , PGS_MULTISEAT                                ")
      _sb_progressive.AppendLine("        FROM   PROGRESSIVES                                 ")
      _sb_progressive.AppendLine("       WHERE   PGS_PROGRESSIVE_ID = " & ItemProgressiveId)

      _dt_progressive = GUI_GetTableUsingSQL(_sb_progressive.ToString(), 1)
      If IsNothing(_dt_progressive) Then
        Return CLASS_BASE.ENUM_STATUS.STATUS_NOT_FOUND
      End If
      If _dt_progressive.Rows.Count() <> 1 Then
        Return CLASS_BASE.ENUM_STATUS.STATUS_NOT_FOUND
      End If

      Me.ProgressiveId = ItemProgressiveId
      Me.ProgressiveName = NullTrim(_dt_progressive.Rows(0).Item("PGS_NAME"))
      Me.ProgressiveCreated = _dt_progressive.Rows(0).Item("PGS_CREATED")
      Me.ProgressiveContributionPct = _dt_progressive.Rows(0).Item("PGS_CONTRIBUTION_PCT")
      Me.ProgressiveNumLevels = _dt_progressive.Rows(0).Item("PGS_NUM_LEVELS")
      'Me.ProgressiveAmount = GUI_FormatCurrency(_dt_progressive.Rows(0).Item("PGS_AMOUNT"), 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE)
      Me.ProgressiveAmount = _dt_progressive.Rows(0).Item("PGS_AMOUNT")
      Me.ProgressiveTerminalList.FromXml(_dt_progressive.Rows(0).Item("PGS_TERMINAL_LIST"))
      Me.ProgressiveStatus = _dt_progressive.Rows(0).Item("PGS_STATUS")
      Me.ProgressiveStatusChanged = _dt_progressive.Rows(0).Item("PGS_STATUS_CHANGED")

      If _dt_progressive.Rows(0).IsNull("PGS_LAST_PROVISIONED_HOUR") Then
        Me.ProgressiveLastProvisionedHour = DateTime.MinValue
      Else
        Me.ProgressiveLastProvisionedHour = _dt_progressive.Rows(0).Item("PGS_LAST_PROVISIONED_HOUR")
      End If

      ' LTC 2018-FEB-08
      If _dt_progressive.Rows(0).IsNull("PGS_BRAND") Then
        Me.ProgressiveBrand = ""
      Else
        Me.ProgressiveBrand = _dt_progressive.Rows(0).Item("PGS_BRAND")
      End If

      If _dt_progressive.Rows(0).IsNull("PGS_SERVER") Then
        Me.ProgressiveServer = ""
      Else
        Me.ProgressiveServer = _dt_progressive.Rows(0).Item("PGS_SERVER")
      End If

      If _dt_progressive.Rows(0).IsNull("PGS_MULTISEAT") Then
        Me.ProgressiveIsMultiseat = False
      Else
        Me.ProgressiveIsMultiseat = _dt_progressive.Rows(0).Item("PGS_MULTISEAT")
      End If

      ' Progressive Levels Data
      _sb_progressive_levels = New StringBuilder()
      _sb_progressive_levels.AppendLine("     SELECT   PGL_LEVEL_ID                                                                             ")
      _sb_progressive_levels.AppendLine("            , PGL_NAME                                                                                 ")
      _sb_progressive_levels.AppendLine("            , PGL_CONTRIBUTION_PCT                                                                     ")
      _sb_progressive_levels.AppendLine("            , PGL_AMOUNT                                                                               ")
      _sb_progressive_levels.AppendLine("            , CASE WHEN  EXISTS                                                                        ")
      _sb_progressive_levels.AppendLine("                              (  SELECT  PP.PGP_PROGRESSIVE_ID                                         ")
      _sb_progressive_levels.AppendLine("                                   FROM  PROGRESSIVES_PROVISIONS PP                                    ")
      _sb_progressive_levels.AppendLine("                           			 WHERE  PP.PGP_PROVISION_ID IN                                        ")
      _sb_progressive_levels.AppendLine(" 				                              ( SELECT  PPL.PPL_PROVISION_ID                                  ")
      _sb_progressive_levels.AppendLine("                                           FROM  PROGRESSIVES_PROVISIONS_LEVELS PPL                    ")
      _sb_progressive_levels.AppendLine("				                                   WHERE  PPL.PPL_LEVEL_ID = PL.PGL_LEVEL_ID                    ")
      _sb_progressive_levels.AppendLine("                                                 AND PP.PGP_PROGRESSIVE_ID = PL.PGL_PROGRESSIVE_ID ) ) ")
      _sb_progressive_levels.AppendLine("                   THEN " & ENUM_PROGRESSIVE_LEVELS.LEVEL_NOT_DELETEABLE_PROVISIONED_LEVEL)
      _sb_progressive_levels.AppendLine("                   ELSE ")
      _sb_progressive_levels.AppendLine("                     CASE WHEN PL.PGL_AMOUNT <> 0 THEN " & ENUM_PROGRESSIVE_LEVELS.LEVEL_NOT_DELETEABLE_AMOUNT_DIFF_THAN_ZERO)
      _sb_progressive_levels.AppendLine("                     ELSE " & ENUM_PROGRESSIVE_LEVELS.LEVEL_DELETEABLE & " END")
      _sb_progressive_levels.AppendLine("               END AS LEVEL_USED                                                                       ")
      If WSI.Common.GeneralParam.GetBoolean("Features", "WinUP", False) Then
        _sb_progressive_levels.AppendLine("            ,  PGL_SHOW_ON_WINUP                                                                        ")
      End If

      _sb_progressive_levels.AppendLine("       FROM   PROGRESSIVES_LEVELS PL                                                                   ")
      _sb_progressive_levels.AppendLine("      WHERE   PGL_PROGRESSIVE_ID = " & ItemProgressiveId)

      _dt_progressive_levels = GUI_GetTableUsingSQL(_sb_progressive_levels.ToString(), 5000)
      Me.ProgressiveLevelsList = _dt_progressive_levels.Copy()

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return CLASS_BASE.ENUM_STATUS.STATUS_OK
  End Function ' GetProgressive

  '----------------------------------------------------------------------------
  ' PURPOSE: Update the progressive information.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  '
  Private Function UpdateProgressive() As Integer
    Dim _sb_progressive As StringBuilder
    Dim _sb_terminal As StringBuilder
    Dim _sql_command As System.Data.SqlClient.SqlCommand
    Dim _sql_trans As System.Data.SqlClient.SqlTransaction
    Dim _sql_adapter As System.Data.SqlClient.SqlDataAdapter
    Dim _num_updated As Integer

    _sql_command = Nothing
    _sql_trans = Nothing

    Try
      _sb_progressive = New StringBuilder()

      If Not GUI_BeginSQLTransaction(_sql_trans) Then
        'TODO JMM 28-NOV-2012: Return an empty object
      End If

      ' LTC 2018-FEB-08
      ' Update / Insert Progressive
      With _sb_progressive
        .AppendLine("       UPDATE   PROGRESSIVES                                   ")
        .AppendLine("          SET   PGS_NAME             = @pPgsName               ")
        .AppendLine("              , PGS_CONTRIBUTION_PCT = @pPgsContributionPct    ")
        .AppendLine("              , PGS_NUM_LEVELS       = @pPgsNumLevels          ")
        .AppendLine("              , PGS_TERMINAL_LIST    = @pPgsTerminalList       ")
        .AppendLine("              , PGS_STATUS           = @pPgsStatus             ")
        .AppendLine("              , PGS_STATUS_CHANGED   = @pPgsStatusChanged      ")
        .AppendLine("              , PGS_AMOUNT           = @pPgsAmount             ")
        .AppendLine("              , PGS_BRAND            = @pPgsBrand              ")
        .AppendLine("              , PGS_SERVER           = @pPgsServer             ")
        .AppendLine("              , PGS_MULTISEAT        = @pPgsMultiseat          ")
        .AppendLine("        WHERE   PGS_PROGRESSIVE_ID   = @pProgressiveId         ")
      End With

      _sql_command = New SqlCommand(_sb_progressive.ToString())
      _sql_command.Transaction = _sql_trans
      _sql_command.Connection = _sql_trans.Connection

      With _sql_command.Parameters
        .Add("@pPgsName", SqlDbType.NVarChar).Value = ProgressiveName
        .Add("@pPgsContributionPct", SqlDbType.Decimal).Value = ProgressiveContributionPct
        .Add("@pPgsNumLevels", SqlDbType.Int).Value = ProgressiveNumLevels
        .Add("@pPgsTerminalList", SqlDbType.Xml).Value = ProgressiveTerminalList.ToXml
        .Add("@pPgsStatus", SqlDbType.Int).Value = IIf(ProgressiveStatus, 1, 0)
        .Add("@pPgsStatusChanged", SqlDbType.DateTime).Value = GUI_FormatDate(ProgressiveStatusChanged, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
        .Add("@pProgressiveId", SqlDbType.BigInt).Value = ProgressiveId
        .Add("@pPgsAmount", SqlDbType.Money).Value = ProgressiveAmount
        .Add("@pPgsBrand", SqlDbType.VarChar).Value = ProgressiveBrand
        .Add("@pPgsServer", SqlDbType.VarChar).Value = ProgressiveServer
        .Add("@pPgsMultiseat", SqlDbType.Bit).Value = ProgressiveIsMultiseat
      End With

      _sql_command.ExecuteNonQuery()

      'Update terminal contribution
      TerminalList.Columns.Add("TE_JACKPOT_CONTRIBUTION_PCT", Type.GetType("System.Decimal"))
      For Each _row As DataRow In TerminalList.Rows
        _row.Item("TE_JACKPOT_CONTRIBUTION_PCT") = ProgressiveContributionPct
        _row.AcceptChanges()
        _row.SetModified()
      Next

      _sb_terminal = New StringBuilder()
      With _sb_terminal
        .AppendLine("       UPDATE    TERMINALS                                       ")
        .AppendLine("          SET    TE_JACKPOT_CONTRIBUTION_PCT = @pTrmContribution ")
        .AppendLine("        WHERE    TE_TERMINAL_ID              = @pTerminalId      ")
      End With

      _sql_command = New SqlCommand(_sb_terminal.ToString())

      With _sql_command.Parameters
        .Add("@pTrmContribution", SqlDbType.Decimal).SourceColumn = "TE_JACKPOT_CONTRIBUTION_PCT"
        .Add("@pTerminalId", SqlDbType.BigInt).SourceColumn = "ID"
      End With

      _sql_command.Connection = _sql_trans.Connection
      _sql_command.Transaction = _sql_trans
      _sql_adapter = New SqlDataAdapter

      _sql_adapter.UpdateCommand = _sql_command
      _num_updated = _sql_adapter.Update(TerminalList)

      If _num_updated <> TerminalList.Rows.Count Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      '_sql_command.ExecuteNonQuery()
      'Next

      ' Delete Levels from Progressive What Are Deleteables
      DeleteLevel(ProgressiveId, _sql_trans)

      ' Update Progressive Levels
      For Each _row As DataRow In ProgressiveLevelsList.Rows
        InsertOrUpdateLevel(ProgressiveId, _row, _sql_trans)
      Next

      ' Commit
      If Not GUI_EndSQLTransaction(_sql_trans, True) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

    Catch _ex As Exception
      Log.Exception(_ex)
      GUI_EndSQLTransaction(_sql_trans, False)

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

  End Function ' UpdateProgressive

  '----------------------------------------------------------------------------
  ' PURPOSE: Insert a new progressive.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  '
  Private Function InsertProgressive() As Integer
    Dim _sb_progressive As StringBuilder
    Dim _sb_terminal As StringBuilder
    Dim _sql_command As System.Data.SqlClient.SqlCommand
    Dim _output_param As System.Data.SqlClient.SqlParameter
    Dim _sql_trans As System.Data.SqlClient.SqlTransaction
    Dim _progessive_id As Long
    Dim _num_rows_to_insert As Integer
    Dim _num_rows_inserted As Integer
    Dim _sql_adapter As System.Data.SqlClient.SqlDataAdapter
    Dim _num_updated As Integer

    _sql_command = Nothing
    _sql_trans = Nothing

    _progessive_id = 0
    _num_rows_to_insert = 0
    _num_rows_inserted = 0

    Try
      _sb_progressive = New StringBuilder()

      If Not GUI_BeginSQLTransaction(_sql_trans) Then
        'TODO JMM 28-NOV-2012: Return an empty object
      End If

      With _sb_progressive
        ' LTC 2018-FEB-08
        ''.AppendLine("      DECLARE   @pProgressiveId AS BIGINT                                                                                            ")
        .AppendLine("  INSERT INTO   PROGRESSIVES(                                                                                                        ")
        .AppendLine("                PGS_NAME                                                                                                             ")
        .AppendLine("              , PGS_CREATED                                                                                                          ")
        .AppendLine("              , PGS_CONTRIBUTION_PCT                                                                                                 ")
        .AppendLine("              , PGS_NUM_LEVELS                                                                                                       ")
        .AppendLine("              , PGS_AMOUNT                                                                                                           ")
        .AppendLine("              , PGS_TERMINAL_LIST                                                                                                    ")
        .AppendLine("              , PGS_STATUS                                                                                                           ")
        .AppendLine("              , PGS_BRAND                                                                                                            ")
        .AppendLine("              , PGS_SERVER                                                                                                           ")
        .AppendLine("              , PGS_MULTISEAT                                                                                                        ")
        .AppendLine("              )                                                                                                                      ")
        .AppendLine("       VALUES ( @pPgsName                                                                                                            ")
        .AppendLine("              , GETDATE()                                                                                                            ")
        .AppendLine("              , @pPgsContributionPct                                                                                                 ")
        .AppendLine("              , @pPgsNumLevels                                                                                                       ")
        .AppendLine("              , @pPgsAmount                                                                                                          ")
        .AppendLine("              , @pPgsTerminalList                                                                                                    ")
        .AppendLine("              , @pPgsStatus                                                                                                          ")
        .AppendLine("              , @pPgsBrand                                                                                                           ")
        .AppendLine("              , @pPgsServer                                                                                                          ")
        .AppendLine("              , @pPgsMultiseat                                                                                                       ")
        .AppendLine("                )                                                                                                                    ")
        .AppendLine("          SET   @pProgressiveId = SCOPE_IDENTITY()                                                                                   ")

        _num_rows_to_insert += 1

      End With

      _sql_command = New SqlCommand(_sb_progressive.ToString())
      _sql_command.Transaction = _sql_trans
      _sql_command.Connection = _sql_trans.Connection

      'Parameters
      _sql_command.Parameters.Add("@pPgsName", SqlDbType.NVarChar).Value = ProgressiveName
      _sql_command.Parameters.Add("@pPgsContributionPct", SqlDbType.Decimal).Value = ProgressiveContributionPct
      _sql_command.Parameters.Add("@pPgsNumLevels", SqlDbType.Int).Value = ProgressiveNumLevels
      _sql_command.Parameters.Add("@pPgsAmount", SqlDbType.Money).Value = ProgressiveAmount
      _sql_command.Parameters.Add("@pPgsTerminalList", SqlDbType.Xml).Value = ProgressiveTerminalList.ToXml
      _sql_command.Parameters.Add("@pPgsStatus", SqlDbType.Int).Value = IIf(ProgressiveStatus, 1, 0)
      _sql_command.Parameters.Add("@pPgsBrand", SqlDbType.VarChar).Value = ProgressiveBrand
      _sql_command.Parameters.Add("@pPgsServer", SqlDbType.VarChar).Value = ProgressiveServer
      _sql_command.Parameters.Add("@pPgsMultiseat", SqlDbType.Bit).Value = ProgressiveIsMultiseat

      _output_param = New SqlParameter("@pProgressiveId", SqlDbType.BigInt)
      _output_param.Direction = ParameterDirection.Output
      _sql_command.Parameters.Add(_output_param)

      _num_rows_inserted = _sql_command.ExecuteNonQuery()

      If _output_param.Value Is DBNull.Value Then

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      'Get ProgressiveId if all right
      _progessive_id = _output_param.Value

      ' Insert Progressive Levels
      For Each _row As DataRow In ProgressiveLevelsList.Rows
        _num_rows_inserted += InsertOrUpdateLevel(_progessive_id, _row, _sql_trans)
        _num_rows_to_insert += 1
      Next

      If _num_rows_inserted <> _num_rows_to_insert Then
        GUI_EndSQLTransaction(_sql_trans, False)

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      'Update terminal contribution
      TerminalList.Columns.Add("TE_JACKPOT_CONTRIBUTION_PCT", Type.GetType("System.Decimal"))
      For Each _row As DataRow In TerminalList.Rows
        _row.Item("TE_JACKPOT_CONTRIBUTION_PCT") = ProgressiveContributionPct
        _row.AcceptChanges()
        _row.SetModified()
      Next

      _sb_terminal = New StringBuilder()
      With _sb_terminal
        .AppendLine("       UPDATE    TERMINALS                                       ")
        .AppendLine("          SET    TE_JACKPOT_CONTRIBUTION_PCT = @pTrmContribution ")
        .AppendLine("        WHERE    TE_TERMINAL_ID              = @pTerminalId      ")
      End With

      _sql_command = New SqlCommand(_sb_terminal.ToString())

      With _sql_command.Parameters
        .Add("@pTrmContribution", SqlDbType.Decimal).SourceColumn = "TE_JACKPOT_CONTRIBUTION_PCT"
        .Add("@pTerminalId", SqlDbType.BigInt).SourceColumn = "ID"
      End With

      _sql_command.Connection = _sql_trans.Connection
      _sql_command.Transaction = _sql_trans
      _sql_adapter = New SqlDataAdapter

      _sql_adapter.UpdateCommand = _sql_command
      _num_updated = _sql_adapter.Update(TerminalList)

      If _num_updated <> TerminalList.Rows.Count Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If



      ' Commit
      If Not GUI_EndSQLTransaction(_sql_trans, True) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

    Catch _ex As Exception
      Log.Exception(_ex)
      GUI_EndSQLTransaction(_sql_trans, False)
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try
  End Function ' InsertProgressive

  '----------------------------------------------------------------------------
  ' PURPOSE: Insert or update progressive data.
  '
  ' PARAMS:
  '   - INPUT:  ProgressiveId (Id)
  '             Row (DataRow)
  '             SQLTrans (Transaction)
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - Rows inserted (integer)
  '
  ' NOTES:
  '
  Private Function InsertOrUpdateLevel(ByVal ProgressiveId As Long, _
                                  ByVal Row As DataRow, _
                                  ByVal SqlTrans As System.Data.SqlClient.SqlTransaction) As Integer
    Dim _sb_sql As StringBuilder
    Dim _sql_command As System.Data.SqlClient.SqlCommand
    Dim _rows_inserted_updated As Long

    _sb_sql = New StringBuilder()
    _sql_command = Nothing
    _rows_inserted_updated = 0

    With _sb_sql

      .AppendLine("  IF NOT EXISTS (   SELECT  PGL_PROGRESSIVE_ID                                   ")
      .AppendLine("                      FROM  PROGRESSIVES_LEVELS                                  ")
      .AppendLine("                     WHERE  PGL_PROGRESSIVE_ID = @pProgressiveId                 ")

      .AppendLine("                       AND  PGL_LEVEL_ID = @pPglLevelId                          ")
      .AppendLine("                )                                                                ")
      .AppendLine("BEGIN                                                                            ")
      .AppendLine("  INSERT INTO   PROGRESSIVES_LEVELS(                                             ")
      .AppendLine("                PGL_PROGRESSIVE_ID                                               ")
      .AppendLine("              , PGL_LEVEL_ID                                                     ")
      .AppendLine("              , PGL_NAME                                                         ")
      .AppendLine("              , PGL_CONTRIBUTION_PCT                                             ")
      .AppendLine("              , PGL_AMOUNT                                                       ")
      If WSI.Common.GeneralParam.GetBoolean("Features", "WinUP", False) Then
        .AppendLine("              , PGL_SHOW_ON_WINUP                                               ")
      End If
      .AppendLine("              )                                                       ")
      .AppendLine("       VALUES   (                                                                ")
      .AppendLine("                @pProgressiveId                                                  ")
      .AppendLine("              , @pPglLevelId                                                     ")
      .AppendLine("              , @pPglName                                                        ")
      .AppendLine("              , @pPglContributionPct                                             ")
      .AppendLine("              , @pPglAmount                                                      ")
      If WSI.Common.GeneralParam.GetBoolean("Features", "WinUP", False) Then
        .AppendLine("              , @pPglShowOnWinup                                                 ")
      End If

      .AppendLine("                )                                                                ")
      .AppendLine("END                                                                              ")
      .AppendLine("ELSE                                                                             ")
      .AppendLine("       UPDATE   PROGRESSIVES_LEVELS                                              ")
      .AppendLine("          SET   PGL_NAME             = @pPglName                                 ")
      .AppendLine("              , PGL_CONTRIBUTION_PCT = @pPglContributionPct                      ")
      .AppendLine("              , PGL_AMOUNT           = @pPglAmount                               ")
      If WSI.Common.GeneralParam.GetBoolean("Features", "WinUP", False) Then
        .AppendLine("              , PGL_SHOW_ON_WINUP    = @pPglShowOnWinup                          ")
      End If

      .AppendLine("        WHERE   PGL_PROGRESSIVE_ID   = @pProgressiveId                           ")
      .AppendLine("          AND   PGL_LEVEL_ID         = @pPglLevelId                              ")
      .AppendLine(";                                                                                ")

    End With

    _sql_command = New SqlCommand(_sb_sql.ToString())
    _sql_command.Transaction = SqlTrans
    _sql_command.Connection = SqlTrans.Connection

    With _sql_command.Parameters
      .Add("@pProgressiveId", SqlDbType.BigInt).Value = ProgressiveId
      .Add("@pPglLevelId", SqlDbType.Int).Value = Row(GRID_COLUMN_PGL_LEVEL_ID)
      .Add("@pPglName", SqlDbType.NVarChar).Value = Row(GRID_COLUMN_PGL_NAME)
      .Add("@pPglContributionPct", SqlDbType.Decimal).Value = Row(GRID_COLUMN_PGL_CONTRIBUTION_PCT)
      .Add("@pPglAmount", SqlDbType.Money).Value = Row(GRID_COLUMN_PGL_AMOUNT)
      If WSI.Common.GeneralParam.GetBoolean("Features", "WinUP", False) Then
        .Add("@pPglShowOnWinup", SqlDbType.Bit).Value = Row(GRID_COLUMN_PGL_SHOW_ON_WINUP)
      End If


    End With
    _rows_inserted_updated = _sql_command.ExecuteNonQuery()

    Return _rows_inserted_updated

  End Function ' InsertOrUpdateLevel

  '----------------------------------------------------------------------------
  ' PURPOSE: Delete Levels what are deleteables (not used) from a progressive.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  '
  Private Function DeleteLevel(ByVal ProgressiveId As Long, _
                                  ByVal SqlTrans As System.Data.SqlClient.SqlTransaction) As Integer
    Dim _sb_sql As StringBuilder
    Dim _sql_command As System.Data.SqlClient.SqlCommand
    Dim _rows_deleted As Long

    _sb_sql = New StringBuilder()
    _sql_command = Nothing
    _rows_deleted = 0

    With _sb_sql

      _sb_sql.AppendLine(" DELETE   PROGRESSIVES_LEVELS                                                                                                               ")
      _sb_sql.AppendLine("  WHERE   PGL_PROGRESSIVE_ID = @plProgressiveId                                                                                             ")
      _sb_sql.AppendLine("    AND   PGL_PROGRESSIVE_ID NOT IN (                                                                                                       ")
      _sb_sql.AppendLine("                                      SELECT  PP.PGP_PROGRESSIVE_ID                                                                         ")
      _sb_sql.AppendLine("                                        FROM  PROGRESSIVES_PROVISIONS PP                                                                    ")
      _sb_sql.AppendLine("                           			       WHERE  PP.PGP_PROVISION_ID IN                                                                        ")
      _sb_sql.AppendLine(" 				                                                          ( SELECT  PPL.PPL_PROVISION_ID                                            ")
      _sb_sql.AppendLine("                                                                      FROM  PROGRESSIVES_PROVISIONS_LEVELS PPL                              ")
      _sb_sql.AppendLine("				                                                             WHERE  PP.PGP_PROGRESSIVE_ID = PROGRESSIVES_LEVELS.PGL_PROGRESSIVE_ID  ")
      _sb_sql.AppendLine("				                                                               AND  PPL.PPL_LEVEL_ID = PROGRESSIVES_LEVELS.PGL_LEVEL_ID ))          ")

    End With

    _sql_command = New SqlCommand(_sb_sql.ToString())
    _sql_command.Transaction = SqlTrans
    _sql_command.Connection = SqlTrans.Connection

    With _sql_command.Parameters
      .Add("@plProgressiveId", SqlDbType.BigInt).Value = ProgressiveId
    End With
    _rows_deleted = _sql_command.ExecuteNonQuery()

    Return _rows_deleted

  End Function ' DeleteLevel

#End Region

End Class
