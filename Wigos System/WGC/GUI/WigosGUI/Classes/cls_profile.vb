'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_profile.vb
' DESCRIPTION:   Profile class for profile edition
' AUTHOR:        Jaume Sala
' CREATION DATE: 04-JUL-2002
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 04-JUL-2002  JSV    Initial version.
' 01-OCT-2012  JAB    Fixed bug, when the profile�s name has a single quote fails.
' 06-MAR-2013  ICS    Added a new feature to limit the number of users per profile.
' 07-FEB-2014  LJM    Fixed bug WIG-526: Now we show a different message.
' 21-FEB-2014  AMF    Fixed bug WIG-483: Multisite audit control
' 24-FEB-2014  AMF    Fixed bug WIG-666: No Multisite audit control
' 22-AUG-2014  AMF    Fixed Bug WIG-1197: Feature of form
' 20-AUG-2015  ETP    Fixed Bug-3721 Added filter IsVisibleProfileForm to CheckProfileFitsInto
' 03-JUN-2016  ESE    Product Backlog Item 13581:Generic reports: Add Report Tool to GUI
' 15-SEP-2016  ETP    Fixed bugs 17585 - 17607: Generic reports: add permissions config.
'-------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports GUI_Controls.Misc

Public Class CLASS_PROFILE
  Inherits CLASS_BASE

#Region " Constants "

  ' Fields length
  Public Const MAX_PROFILE_NAME_LEN As Integer = 20
  Public Const MAX_REPORT_NAME_LEN As Integer = 200

  ' Array length
  Public Const PROFILE_FORM_ARRAY_LENGTH As Integer = 999 'changed from 100 .OC- 17-may-2004

  Public Const INVALID_PROFILE_ID As Integer = -1

  Private Const INVALID_PROFILE_NAME As String = ""
  Private Const FIRST_CALL_YES As Integer = 1
  Private Const FIRST_CALL_NO As Integer = 0
  Private Const AUDIT_NO_PERMISION_STRING As String = "-"
  Private Const AUDIT_NONE_STRING As String = "---"

  Public Enum ENUM_PERMISION
    NOT_ALLOWED = 0
    ALLOWED = 1
    SOME_ALLOWED = 2
  End Enum

#End Region

#Region " GUI_Configuration.dll "

#Region " Structures "

  ' Related to GUI_USER_PROFILES
  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_USER_PROFILE
    Public control_block As Integer
    Public profile_id As Integer
    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_PROFILE_NAME_LEN + 1)> _
    Public profile_name As String
    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=3)> _
    Public filler_0 As String
    Public max_users As Integer
    Public gup_master As Boolean
    Public gup_master_id As Integer

    Public Sub New()
      MyBase.New()
      control_block = Marshal.SizeOf(GetType(TYPE_USER_PROFILE))
    End Sub

  End Class

  ' Related to GUI_PROFILE_FORMS and GUI_FORMS
  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_PROFILE_FORM
    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_REPORT_NAME_LEN)> _
    Public nls As String
    Public gui_id As Integer
    Public form_id As Integer
    Public nls_id As Integer
    Public feature As WSI.Common.ENUM_FEATURES
    Public read_perm As Integer
    Public write_perm As Integer
    Public delete_perm As Integer
    Public execute_perm As Integer

    Public ReadOnly Property FormName()
      Get
        Dim _exists_nls As Boolean
        Dim _form_name As String

        _form_name = NLS_GetString(_exists_nls, nls_id)

        If Not _exists_nls Then
          If nls Is vbNullString Or nls = "" Then
            _form_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5650, form_id.ToString())
          Else
            _form_name = nls
          End If
        End If

        Return _form_name
      End Get
    End Property

  End Class

  ' Class to pass fixed length arrays of forms permisions
  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_PROFILE

    Public control_block As Integer
    Public user_profile As New TYPE_USER_PROFILE()
    Public forms_list As New CLASS_VECTOR()
    Public first_call As Integer           ' To insert or update profile table only once, before insert or update profile_forms table.
    Public commit As Integer               ' On insert and update, last call must set this field to true.
    Public last_gui As Integer             ' GUI ID for last register manipulated, to start operation from next register.
    Public last_form As Integer            ' Form ID for last register manipulated, to start operation from next register. 

    Public Sub New(Optional ByVal NumberItems As Integer = PROFILE_FORM_ARRAY_LENGTH)
      MyBase.New()
      control_block = Marshal.SizeOf(GetType(TYPE_PROFILE))
      Call forms_list.Init(GetType(TYPE_PROFILE_FORM), NumberItems)
    End Sub

    Protected Overrides Sub Finalize()
      forms_list.Done()
      MyBase.Finalize()
    End Sub

  End Class

#End Region

#End Region

#Region " Members "

  Protected m_user_profile As New TYPE_USER_PROFILE
  Protected m_profile_forms() As TYPE_PROFILE_FORM

  ' Gui Ids and Audot Codes
  Protected m_collections_ids As New CLASS_AUDITOR_DATA(AUDIT_NAME_GENERIC)

#End Region

#Region " Properties "

  Public Property ProfileId() As Integer
    Get
      Return m_user_profile.profile_id
    End Get
    Set(ByVal Value As Integer)
      m_user_profile.profile_id = Value
    End Set
  End Property

  Public Property ProfileName() As String
    Get
      Return m_user_profile.profile_name
    End Get
    Set(ByVal Value As String)
      m_user_profile.profile_name = Value
    End Set
  End Property

  Public Property ProfileForms() As TYPE_PROFILE_FORM()
    Get
      Return m_profile_forms
    End Get
        Set(ByVal Value() As TYPE_PROFILE_FORM)
            m_profile_forms = Value
        End Set
  End Property

  Public ReadOnly Property NumForms() As Integer
    Get
      If IsNothing(m_profile_forms) Then
        Return 0
      Else
        Return m_profile_forms.Length
      End If
    End Get
  End Property

  Public ReadOnly Property GuiName(ByVal GuiId As Integer) As String
    Get
      Return m_collections_ids.GUI.NameFromId(GuiId)
    End Get
  End Property

  Public Property MaxUsers() As Integer
    Get
      Return m_user_profile.max_users
    End Get
    Set(ByVal Value As Integer)
      m_user_profile.max_users = Value
    End Set
  End Property

  Public Property GupMaster() As Boolean
    Get
      Return m_user_profile.gup_master
    End Get
    Set(ByVal Value As Boolean)
      m_user_profile.gup_master = Value
    End Set
  End Property

  Public Property GupMasterId() As Integer
    Get
      Return m_user_profile.gup_master_id
    End Get
    Set(ByVal Value As Integer)
      m_user_profile.gup_master_id = Value
    End Set
  End Property

#End Region

#Region " Overrides functions "

  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim rc As Integer
    'XVV Variable not use Dim idx_form As Integer
    'XVV Variable not use Dim idx_last_form As Integer
    Dim form_item As New TYPE_PROFILE_FORM
    Dim profile As New TYPE_PROFILE

    Me.ProfileId = ObjectId

    ' Read profile name, max_users, gup_master and gup_master_id
    rc = ReadProfileInfo(m_user_profile)

    ' Read profile forms array
    If rc = ENUM_CONFIGURATION_RC.CONFIGURATION_OK Then
      rc = DB_ReadProfileForms(Me.ProfileId, SqlCtx)
    End If

    Select Case rc

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim rc As Integer
    Dim num_items As Integer
    Dim idx_form As Integer
    Dim idx_last_form As Integer
    Dim profile As New TYPE_PROFILE

    ' Set user Profile Information
    profile.user_profile.profile_id = Me.ProfileId
    profile.user_profile.profile_name = Me.ProfileName

    ' Set users limit
    profile.user_profile.max_users = Me.MaxUsers
    profile.user_profile.gup_master_id = Me.GupMasterId
    profile.user_profile.gup_master = Me.GupMaster

    ' Set the params values
    profile.first_call = FIRST_CALL_YES

    ' Because the array to be passed by 'UpdateProfile' function is a 
    ' fixed size array, we must do repeated calls to update all forms.
    ' At the last call we will update profile name.
    idx_last_form = -1
    rc = ENUM_CONFIGURATION_RC.CONFIGURATION_OK
    While idx_last_form < Me.NumForms - 1 _
          And rc = ENUM_CONFIGURATION_RC.CONFIGURATION_OK

      ' Calculates the number of items to be copied.
      ' If all items will be copied, set commit to true.
      If ((Me.NumForms - 1) - idx_last_form) > profile.forms_list.MaxItems Then
        num_items = profile.forms_list.MaxItems
        profile.commit = 0
      Else
        num_items = (Me.NumForms - 1) - idx_last_form
        profile.commit = 1
      End If

      ' Copy next block of items into array to be passed as a param.
      Call profile.forms_list.Clear()
      For idx_form = 0 To num_items - 1
        Call profile.forms_list.SetItem(idx_form, m_profile_forms(idx_last_form + 1 + idx_form))
      Next
      idx_last_form = idx_last_form + num_items

      rc = UpdateProfile(profile, SqlCtx)
      profile.first_call = FIRST_CALL_NO

    End While

    Select Case rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
        Return ENUM_STATUS.STATUS_DUPLICATE_KEY

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim rc As Integer
    Dim num_items As Integer
    Dim idx_form As Integer
    Dim idx_last_form As Integer
    Dim profile As New TYPE_PROFILE

    ' Set user Profile Information
    profile.user_profile.profile_id = Me.ProfileId
    profile.user_profile.profile_name = Me.ProfileName
    profile.user_profile.max_users = Me.MaxUsers
    profile.user_profile.gup_master_id = Me.GupMasterId
    profile.user_profile.gup_master = Me.GupMaster

    ' Set the params values
    profile.first_call = FIRST_CALL_YES

    ' Because the array to be passed by 'InsertProfile' function is a 
    ' fixed size array, we must do repeated calls to Insert all forms.
    ' At the last call we will insert profile Id and name.
    idx_last_form = -1
    rc = ENUM_CONFIGURATION_RC.CONFIGURATION_OK
    While idx_last_form < Me.NumForms - 1 _
          And rc = ENUM_CONFIGURATION_RC.CONFIGURATION_OK

      ' Calculates the number of items to be copied.
      ' If all items will be copied, set commit to true.
      If ((Me.NumForms - 1) - idx_last_form) > profile.forms_list.MaxItems Then
        num_items = profile.forms_list.MaxItems
        profile.commit = 0
      Else
        num_items = (Me.NumForms - 1) - idx_last_form
        profile.commit = 1
      End If

      ' Copy next block of items into array to be passed as a param.
      Call profile.forms_list.Clear()
      For idx_form = 0 To num_items - 1
        'OC 17-may-2004 
        Call profile.forms_list.SetItem((idx_last_form + 1 + idx_form), m_profile_forms(idx_last_form + 1 + idx_form))
        'Call profile.forms_list.SetItem(idx_form, m_profile_forms(idx_last_form + 1 + idx_form))
      Next
      idx_last_form = idx_last_form + num_items

      rc = InsertProfile(profile, SqlCtx)
      profile.first_call = FIRST_CALL_NO

    End While

    Select Case rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
        Return ENUM_STATUS.STATUS_DUPLICATE_KEY

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim rc As Integer

    ' Delete profile data
    rc = DeleteProfile(Me.ProfileId, SqlCtx)

    Select Case rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function

  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim auditor_data As CLASS_AUDITOR_DATA
    Dim idx_form As Integer
    Dim name_string As String
    Dim perm_string As String

    ' Create a new auditor_code for user/profile manipulation
    auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_PROFILES)
    Call auditor_data.SetName(GLB_NLS_GUI_CONFIGURATION.Id(319), Me.ProfileName)

    'Profile Name
    Call auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(326), IIf(Me.ProfileName = "", AUDIT_NONE_STRING, Me.ProfileName))

    'Max users
    If (Me.MaxUsers > 0) Then
      Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1763), GUI_FormatNumber(Me.MaxUsers, 0))
    Else
      Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1763), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1764))
    End If

    ' Permisions: One field for each form
    For idx_form = 0 To Me.NumForms - 1
      If WSI.Common.GeneralParam.GetBoolean("MultiSite", "IsCenter") And _
         Not Me.GupMaster And _
         Me.ProfileForms(idx_form).gui_id <> ENUM_GUI.MULTISITE_GUI Then

        Continue For
      End If

      If Not WSI.Common.GeneralParam.GetBoolean("MultiSite", "IsCenter", False) And _
         Not WSI.Common.GeneralParam.GetBoolean("Site", "MultiSiteMember", False) And _
         Me.ProfileForms(idx_form).gui_id = ENUM_GUI.MULTISITE_GUI Then

        Continue For
      End If

      'Field name composed by gui_name + form_name
      name_string = Me.GuiName(Me.ProfileForms(idx_form).gui_id) & ": " & Me.ProfileForms(idx_form).FormName

      perm_string = IIf(Me.ProfileForms(idx_form).read_perm = 0, AUDIT_NO_PERMISION_STRING, GLB_NLS_GUI_CONFIGURATION.GetString(341))
      perm_string = perm_string & IIf(Me.ProfileForms(idx_form).write_perm = 0, AUDIT_NO_PERMISION_STRING, GLB_NLS_GUI_CONFIGURATION.GetString(342))
      perm_string = perm_string & IIf(Me.ProfileForms(idx_form).delete_perm = 0, AUDIT_NO_PERMISION_STRING, GLB_NLS_GUI_CONFIGURATION.GetString(343))
      perm_string = perm_string & IIf(Me.ProfileForms(idx_form).execute_perm = 0, AUDIT_NO_PERMISION_STRING, GLB_NLS_GUI_CONFIGURATION.GetString(344))
      Call auditor_data.SetField(0, perm_string, name_string)

    Next idx_form

    Return auditor_data

  End Function

  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim temp_profile As CLASS_PROFILE
    Dim idx_form As Integer

    temp_profile = New CLASS_PROFILE

    ' duplicate profile info
    temp_profile.m_user_profile.control_block = Me.m_user_profile.control_block
    temp_profile.m_user_profile.profile_id = Me.m_user_profile.profile_id
    temp_profile.m_user_profile.profile_name = Me.m_user_profile.profile_name
    temp_profile.m_user_profile.max_users = Me.m_user_profile.max_users
    temp_profile.m_user_profile.gup_master_id = Me.m_user_profile.gup_master_id
    temp_profile.m_user_profile.gup_master = Me.m_user_profile.gup_master
    temp_profile.m_user_profile.filler_0 = Me.m_user_profile.filler_0

    ' Duplicate array of forms permisions
    If IsNothing(m_profile_forms) Then
      temp_profile.m_profile_forms = Nothing
    Else
      ReDim temp_profile.m_profile_forms(Me.m_profile_forms.Length - 1)
      For idx_form = 0 To Me.m_profile_forms.Length - 1
        temp_profile.m_profile_forms(idx_form) = New TYPE_PROFILE_FORM
        temp_profile.m_profile_forms(idx_form).gui_id = Me.m_profile_forms(idx_form).gui_id
        temp_profile.m_profile_forms(idx_form).form_id = Me.m_profile_forms(idx_form).form_id
        temp_profile.m_profile_forms(idx_form).nls_id = Me.m_profile_forms(idx_form).nls_id
        temp_profile.m_profile_forms(idx_form).nls = Me.m_profile_forms(idx_form).nls
        temp_profile.m_profile_forms(idx_form).feature = Me.m_profile_forms(idx_form).feature
        temp_profile.m_profile_forms(idx_form).read_perm = Me.m_profile_forms(idx_form).read_perm
        temp_profile.m_profile_forms(idx_form).write_perm = Me.m_profile_forms(idx_form).write_perm
        temp_profile.m_profile_forms(idx_form).delete_perm = Me.m_profile_forms(idx_form).delete_perm
        temp_profile.m_profile_forms(idx_form).execute_perm = Me.m_profile_forms(idx_form).execute_perm
      Next

    End If

    Return temp_profile

  End Function

#End Region

#Region " Private functions "

  Shared Function ReadProfileInfo(ByVal pProfile As TYPE_USER_PROFILE) As Integer
    Dim str_sql As String
    Dim data_table As DataTable

    pProfile.profile_name = " "

    str_sql = "SELECT  ISNULL (GUP_NAME, ' ') AS GUP_NAME " & _
               "    ,  GUP_MAX_USERS " & _
               "    ,  CASE WHEN GUP_MASTER_ID IS NULL THEN 0 ELSE 1 END AS GUP_MASTER " & _
               "    ,  ISNULL (GUP_MASTER_ID, 0) AS GUP_MASTER_ID " & _
               " FROM  GUI_USER_PROFILES " & _
              " WHERE  GUP_PROFILE_ID = " & pProfile.profile_id

    data_table = GUI_GetTableUsingSQL(str_sql, 5000)

    If IsNothing(data_table) Or data_table.Rows.Count = 0 Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    If Not IsNothing(data_table) Then
      pProfile.profile_name = NullTrim(data_table.Rows.Item(0).Item(0))
      pProfile.max_users = data_table.Rows.Item(0).Item(1)
      pProfile.gup_master = data_table.Rows.Item(0).Item(2)
      pProfile.gup_master_id = data_table.Rows.Item(0).Item(3)
    End If

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
  End Function

  Private Function ReadProfile(ByVal pProfile As TYPE_PROFILE, _
                               ByVal Context As Integer) As Integer
    Dim form_item As New TYPE_PROFILE_FORM
    Dim str_sql As String
    Dim idx_db_row As Integer
    Dim num_db_rows As Integer = 0
    Dim data_table As DataTable
    Dim _current_gui_id As ENUM_GUI
    Dim _gui_forms As CLASS_GUI_FORMS
    Dim _form_data As CLASS_GUI_FORMS.CLASS_GUI_FORM

    str_sql = "SELECT  GF_GUI_ID " & _
                    ", GF_FORM_ID " & _
                    ", ISNULL (GPF_READ_PERM, 0) AS GPF_READ_PERM " & _
                    ", ISNULL (GPF_WRITE_PERM, 0) AS GPF_WRITE_PERM " & _
                    ", ISNULL (GPF_DELETE_PERM, 0) AS GPF_DELETE_PERM " & _
                    ", ISNULL (GPF_EXECUTE_PERM, 0) AS GPF_EXECUTE_PERM " & _
               " FROM  GUI_PROFILE_FORMS RIGHT OUTER JOIN GUI_FORMS " & _
                    "    ON (GPF_GUI_ID = GF_GUI_ID) AND (GPF_FORM_ID = GF_FORM_ID) AND (GPF_PROFILE_ID = " & pProfile.user_profile.profile_id & " ) " & _
              " WHERE  (    GF_GUI_ID > " & pProfile.last_gui & _
                      " OR  (    GF_GUI_ID = " & pProfile.last_gui & _
                           " AND GF_FORM_ID > " & pProfile.last_form & "))" & _
           " ORDER BY  GF_GUI_ID " & _
                    ", GF_FORM_ORDER "

    data_table = GUI_GetTableUsingSQL(str_sql, 5000)

    If IsNothing(data_table) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    If data_table.Rows.Count() < 1 Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    _current_gui_id = -1
    _gui_forms = Nothing

    num_db_rows = data_table.Rows.Count()
    For idx_db_row = 0 To num_db_rows - 1

      form_item.gui_id = data_table.Rows(idx_db_row).Item("GF_GUI_ID")
      form_item.form_id = data_table.Rows(idx_db_row).Item("GF_FORM_ID")

      If _current_gui_id <> form_item.gui_id OrElse _gui_forms Is Nothing Then
        _current_gui_id = form_item.gui_id
        _gui_forms = GetGUIForms(form_item.gui_id)

        If _gui_forms Is Nothing Then
          Continue For
        End If
      End If

      _form_data = _gui_forms.GetFormData(form_item.form_id)

      If _form_data Is Nothing Then
        Continue For
      End If

      form_item.nls_id = _form_data.NlsId
      form_item.nls = _form_data.Nls
      form_item.feature = _form_data.Feature

      form_item.read_perm = IIf(data_table.Rows(idx_db_row).Item("GPF_READ_PERM"), 1, 0)
      form_item.write_perm = IIf(data_table.Rows(idx_db_row).Item("GPF_WRITE_PERM"), 1, 0)
      form_item.delete_perm = IIf(data_table.Rows(idx_db_row).Item("GPF_DELETE_PERM"), 1, 0)
      form_item.execute_perm = IIf(data_table.Rows(idx_db_row).Item("GPF_EXECUTE_PERM"), 1, 0)

      pProfile.forms_list.SetItem(pProfile.forms_list.Count, form_item)
    Next

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
  End Function

  Private Function UpdateProfile(ByVal pProfile As TYPE_PROFILE, _
                                 ByVal Context As Integer) As Integer
    Dim form_item As New TYPE_PROFILE_FORM
    Dim str_sql As String
    Dim db_count As Integer = 0
    Dim idx_item As Integer
    Dim num_items As Integer = 0

    'XVV 16/04/2007
    'Assign Nothing to variable because compiler generate a warning
    Dim SqlTrans As System.Data.SqlClient.SqlTransaction = Nothing

    Dim data_table As DataTable
    Dim num_rows_updated As Integer

    If pProfile.first_call = 1 Then

      If Not GUI_BeginSQLTransaction(SqlTrans) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      str_sql = "SELECT  COUNT(*) " & _
                 " FROM  GUI_USER_PROFILES " & _
                " WHERE  " & GUI_FilterField("GUP_NAME", pProfile.user_profile.profile_name, False, True, False) & _
                   " AND GUP_PROFILE_ID <> " & pProfile.user_profile.profile_id

      data_table = GUI_GetTableUsingSQL(str_sql, 5000, , SqlTrans)
      If IsNothing(data_table) Then
        GUI_EndSQLTransaction(SqlTrans, False)

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      db_count = data_table.Rows(0).Item(0)

      If db_count > 0 Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
      End If

      str_sql = "UPDATE  GUI_USER_PROFILES " & _
                  " SET  GUP_NAME = '" & pProfile.user_profile.profile_name.Replace("'", "''") & "' " & _
                     " , GUP_MAX_USERS  = " & pProfile.user_profile.max_users

      If pProfile.user_profile.gup_master Then
        str_sql += " , GUP_MASTER_ID  = " & pProfile.user_profile.gup_master_id
      End If

      str_sql += " WHERE  GUP_PROFILE_ID = " & pProfile.user_profile.profile_id

      If Not GUI_SQLExecuteNonQuery(str_sql, SqlTrans) Then
        GUI_EndSQLTransaction(SqlTrans, False)

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

    End If

    num_items = pProfile.forms_list.Count
    For idx_item = 0 To num_items - 1

      pProfile.forms_list.GetItem(idx_item, form_item)

      str_sql = "UPDATE  GUI_PROFILE_FORMS " & _
                  " SET  GPF_READ_PERM = " & form_item.read_perm & _
                     " , GPF_WRITE_PERM = " & form_item.write_perm & _
                     " , GPF_DELETE_PERM = " & form_item.delete_perm & _
                     " , GPF_EXECUTE_PERM = " & form_item.execute_perm & _
                " WHERE  GPF_PROFILE_ID = " & pProfile.user_profile.profile_id & _
                   " AND GPF_GUI_ID = " & form_item.gui_id & _
                   " AND GPF_FORM_ID = " & form_item.form_id

      If Not GUI_SQLExecuteNonQuery(str_sql, SqlTrans, num_rows_updated) Then
        GUI_EndSQLTransaction(SqlTrans, False)

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      If num_rows_updated = 0 Then

        str_sql = "INSERT INTO   GUI_PROFILE_FORMS " & _
                             " ( GPF_PROFILE_ID " & _
                             " , GPF_GUI_ID" & _
                             " , GPF_FORM_ID" & _
                             " , GPF_READ_PERM" & _
                             " , GPF_WRITE_PERM" & _
                             " , GPF_DELETE_PERM" & _
                             " , GPF_EXECUTE_PERM" & _
                             " )" & _
                      " VALUES " & _
                             " ( " & pProfile.user_profile.profile_id & _
                             " , " & form_item.gui_id & _
                             " , " & form_item.form_id & _
                             " , " & form_item.read_perm & _
                             " , " & form_item.write_perm & _
                             " , " & form_item.delete_perm & _
                             " , " & form_item.execute_perm & _
                             " )"

        If Not GUI_SQLExecuteNonQuery(str_sql, SqlTrans) Then
          GUI_EndSQLTransaction(SqlTrans, False)

          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

      End If

    Next

    If pProfile.commit = 1 Then
      If Not GUI_EndSQLTransaction(SqlTrans, True) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If
    End If

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
  End Function

  Private Function InsertProfile(ByVal pProfile As TYPE_PROFILE, _
                                 ByVal Context As Integer) As Integer
    Dim form_item As New TYPE_PROFILE_FORM
    Dim str_sql As String
    Dim db_count As Integer = 0
    Dim idx_item As Integer
    Dim num_items As Integer = 0
    Dim db_profile_id As Integer
    'XVV 16/04/2007
    'Assign Nothing to SQL Trans because compiler generate warning for not have value
    Dim SqlTrans As System.Data.SqlClient.SqlTransaction = Nothing
    Dim data_table As DataTable

    If pProfile.first_call = 1 Then

      If Not GUI_BeginSQLTransaction(SqlTrans) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      str_sql = "SELECT  COUNT(*) " & _
                 " FROM  GUI_USER_PROFILES " & _
                " WHERE  " & GUI_FilterField("GUP_NAME", pProfile.user_profile.profile_name, False, True, False)

      data_table = GUI_GetTableUsingSQL(str_sql, 5000)
      If IsNothing(data_table) Then
        GUI_EndSQLTransaction(SqlTrans, False)

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      db_count = data_table.Rows(0).Item(0)

      If db_count > 0 Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
      End If

      str_sql = "SELECT MAX(ISNULL(GUP_PROFILE_ID, 0)) + 1 " & _
                 " FROM GUI_USER_PROFILES "

      data_table = GUI_GetTableUsingSQL(str_sql, 5000)
      If IsNothing(data_table) Then
        GUI_EndSQLTransaction(SqlTrans, False)

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      db_profile_id = data_table.Rows(0).Item(0)
      pProfile.user_profile.profile_id = db_profile_id

      str_sql = "INSERT INTO   GUI_USER_PROFILES " & _
                           " ( GUP_PROFILE_ID " & _
                           " , GUP_NAME " & _
                           " , GUP_MAX_USERS "

      If pProfile.user_profile.gup_master Then
        str_sql += " , GUP_MASTER_ID "
      End If

      str_sql += " )" & _
                 " VALUES " & _
                 " ( " & db_profile_id & _
                 " , '" & pProfile.user_profile.profile_name.Replace("'", "''") & "' " & _
                 " , " & pProfile.user_profile.max_users

      If pProfile.user_profile.gup_master Then
        str_sql += " , " & db_profile_id
      End If

      str_sql += " )"

      If Not GUI_SQLExecuteNonQuery(str_sql, SqlTrans) Then
        GUI_EndSQLTransaction(SqlTrans, False)

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

    End If

    num_items = pProfile.forms_list.Count
    For idx_item = 0 To num_items - 1

      pProfile.forms_list.GetItem(idx_item, form_item)

      str_sql = "INSERT INTO   GUI_PROFILE_FORMS " & _
                           " ( GPF_PROFILE_ID " & _
                           " , GPF_GUI_ID " & _
                           " , GPF_FORM_ID " & _
                           " , GPF_READ_PERM " & _
                           " , GPF_WRITE_PERM " & _
                           " , GPF_DELETE_PERM " & _
                           " , GPF_EXECUTE_PERM " & _
                           " )" & _
                   " VALUES " & _
                           " ( " & pProfile.user_profile.profile_id & _
                           " , " & form_item.gui_id & _
                           " , " & form_item.form_id & _
                           " , " & form_item.read_perm & _
                           " , " & form_item.write_perm & _
                           " , " & form_item.delete_perm & _
                           " , " & form_item.execute_perm & _
                           " ) "

      If Not GUI_SQLExecuteNonQuery(str_sql, SqlTrans) Then
        GUI_EndSQLTransaction(SqlTrans, False)

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

    Next

    If pProfile.commit = 1 Then
      If Not GUI_EndSQLTransaction(SqlTrans, True) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If
    End If

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
  End Function

  Private Function DeleteProfile(ByVal ProfileId As Integer, ByVal Context As Integer) As Integer
    Dim str_sql As String
    Dim db_count As Integer = 0

    'XVV 16/04/2007
    'Assign Nothing to variable because compiler generate a warning
    Dim SqlTrans As System.Data.SqlClient.SqlTransaction = Nothing

    Dim data_table As DataTable

    If Not GUI_BeginSQLTransaction(SqlTrans) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    str_sql = "SELECT  COUNT(*) " & _
               " FROM  GUI_USERS " & _
              " WHERE  GU_PROFILE_ID = " & ProfileId

    data_table = GUI_GetTableUsingSQL(str_sql, 5000)
    If IsNothing(data_table) Then
      GUI_EndSQLTransaction(SqlTrans, False)

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    db_count = data_table.Rows(0).Item(0)

    If db_count > 0 Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DEPENDENCIES
    End If

    str_sql = "DELETE  GUI_PROFILE_FORMS " & _
              " WHERE  GPF_PROFILE_ID = " & ProfileId

    If Not GUI_SQLExecuteNonQuery(str_sql, SqlTrans) Then
      GUI_EndSQLTransaction(SqlTrans, False)

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    str_sql = "DELETE  GUI_USER_PROFILES " & _
              " WHERE  GUP_PROFILE_ID = " & ProfileId

    If Not GUI_SQLExecuteNonQuery(str_sql, SqlTrans) Then
      GUI_EndSQLTransaction(SqlTrans, False)

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    If Not GUI_EndSQLTransaction(SqlTrans, True) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
  End Function

#End Region

#Region " Public functions "

  ' PURPOSE: Read all forms registers and save they into member array
  '          initializing permisions in member array to true
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - status code returned from Database
  '
  ' NOTE: Setting Profile Id to inexisting value returns all forms with no permisions.

  Public Function DB_ReadProfileForms(ByVal profile_id As Integer, _
                                      ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim rc As Integer
    Dim idx_form As Integer
    Dim idx_last_form As Integer
    Dim form_item As New TYPE_PROFILE_FORM
    Dim profile As New TYPE_PROFILE

    ' Dispose actual array of form permissions to redim a new one
    m_profile_forms = Nothing

    ' Set the params values
    profile.user_profile.profile_id = profile_id
    profile.last_gui = 0
    profile.last_form = 0

    rc = ENUM_CONFIGURATION_RC.CONFIGURATION_OK_MORE
    While rc = ENUM_CONFIGURATION_RC.CONFIGURATION_OK_MORE

      rc = ReadProfile(profile, SqlCtx)

      ' If there is more registers, get values for next start point
      If rc = ENUM_CONFIGURATION_RC.CONFIGURATION_OK_MORE Then
        Call profile.forms_list.GetItem(profile.forms_list.Count - 1, form_item)
        profile.last_gui = form_item.gui_id
        profile.last_form = form_item.form_id
      End If

      ' Concatenate new items to the end of member array
      If profile.forms_list.Count > 0 _
          And (rc = ENUM_CONFIGURATION_RC.CONFIGURATION_OK _
          Or rc = ENUM_CONFIGURATION_RC.CONFIGURATION_OK_MORE) Then

        'Redim member array to save new forms readed
        If IsNothing(m_profile_forms) Then
          idx_last_form = -1
          ReDim m_profile_forms(profile.forms_list.Count - 1)
        Else
          idx_last_form = m_profile_forms.Length - 1
          ReDim Preserve m_profile_forms(m_profile_forms.Length + profile.forms_list.Count - 1)
        End If

        ' Copy new items to the end of member array 
        For idx_form = 0 To profile.forms_list.Count - 1
          Call profile.forms_list.GetItem(idx_form, form_item)
          m_profile_forms(idx_last_form + 1 + idx_form) = New TYPE_PROFILE_FORM
          m_profile_forms(idx_last_form + 1 + idx_form).gui_id = form_item.gui_id
          m_profile_forms(idx_last_form + 1 + idx_form).form_id = form_item.form_id
          m_profile_forms(idx_last_form + 1 + idx_form).nls_id = form_item.nls_id
          m_profile_forms(idx_last_form + 1 + idx_form).nls = form_item.nls
          m_profile_forms(idx_last_form + 1 + idx_form).feature = form_item.feature
          m_profile_forms(idx_last_form + 1 + idx_form).read_perm = form_item.read_perm
          m_profile_forms(idx_last_form + 1 + idx_form).write_perm = form_item.write_perm
          m_profile_forms(idx_last_form + 1 + idx_form).delete_perm = form_item.delete_perm
          m_profile_forms(idx_last_form + 1 + idx_form).execute_perm = form_item.execute_perm
        Next

      End If

    End While

    Select Case rc

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function

  ' PURPOSE : Restricts the permissions of the profile based on the permissions of the pattern profile
  '
  '  PARAMS :
  '     - INPUT :
  '         - PatternProfile 
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  '
  ' NOTE :

  Public Sub RestrictProfileTo(ByRef PatternProfile As CLASS_PROFILE)

    Dim _idx_form As Integer
    Dim _idx_pattern_form As Integer
    Dim _form_item As New TYPE_PROFILE_FORM
    Dim _found As Boolean

    For _idx_form = 0 To Me.NumForms - 1
      _found = False
      For _idx_pattern_form = 0 To PatternProfile.NumForms - 1
        ' Search the gui+form
        If Me.ProfileForms(_idx_form).gui_id = PatternProfile.ProfileForms(_idx_pattern_form).gui_id _
       And Me.ProfileForms(_idx_form).form_id = PatternProfile.ProfileForms(_idx_pattern_form).form_id Then
          _found = True

          Exit For
        End If
      Next

      If _found Then
        Me.ProfileForms(_idx_form).read_perm = PatternProfile.ProfileForms(_idx_pattern_form).read_perm
        Me.ProfileForms(_idx_form).write_perm = PatternProfile.ProfileForms(_idx_pattern_form).write_perm
        Me.ProfileForms(_idx_form).delete_perm = PatternProfile.ProfileForms(_idx_pattern_form).delete_perm
        Me.ProfileForms(_idx_form).execute_perm = PatternProfile.ProfileForms(_idx_pattern_form).execute_perm
      Else
        Me.ProfileForms(_idx_form).read_perm = False
        Me.ProfileForms(_idx_form).write_perm = False
        Me.ProfileForms(_idx_form).delete_perm = False
        Me.ProfileForms(_idx_form).execute_perm = False
      End If
    Next

  End Sub   ' RestrictProfileTo

  ' PURPOSE : Check whether the permissions of the profile fit into the pattern profile
  '
  '  PARAMS :
  '     - INPUT :
  '         - PatternProfile 
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  '
  ' NOTE :

  Public Function CheckProfileFitsInto(ByRef PatternProfile As CLASS_PROFILE) As Boolean

    Dim _idx_form As Integer
    Dim _idx_pattern_form As Integer
    Dim _form_item As New TYPE_PROFILE_FORM
    Dim _found As Boolean

    For _idx_form = 0 To Me.NumForms - 1

      If Not IsVisibleProfileForm(PatternProfile.ProfileForms(_idx_form).feature, PatternProfile.ProfileForms(_idx_form).gui_id) Then
        Continue For
      End If

      _found = False

      For _idx_pattern_form = 0 To PatternProfile.NumForms - 1
        ' Search the gui+form
        If Me.ProfileForms(_idx_form).gui_id = PatternProfile.ProfileForms(_idx_pattern_form).gui_id _
       And Me.ProfileForms(_idx_form).form_id = PatternProfile.ProfileForms(_idx_pattern_form).form_id Then
          _found = True

          Exit For
        End If
      Next

      If Not _found Then
        Return False
      End If

      If Me.ProfileForms(_idx_form).read_perm _
     And Not PatternProfile.ProfileForms(_idx_pattern_form).read_perm Then
        Return False
      End If

      If Me.ProfileForms(_idx_form).write_perm _
     And Not PatternProfile.ProfileForms(_idx_pattern_form).write_perm Then
        Return False
      End If

      If Me.ProfileForms(_idx_form).delete_perm _
     And Not PatternProfile.ProfileForms(_idx_pattern_form).delete_perm Then
        Return False
      End If

      If Me.ProfileForms(_idx_form).execute_perm _
     And Not PatternProfile.ProfileForms(_idx_pattern_form).execute_perm Then
        Return False
      End If
    Next

    Return True

  End Function   ' CheckProfileFitsInto

#End Region

#Region " Shared Functions "

  ' PURPOSE: Read Database to obtain the gup_master
  '
  '  PARAMS:
  '     - INPUT:
  '           - ProfileId
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - gup_master
  '
  Shared Function GetGupMaster(ByVal ProfileId As Integer) As Boolean

    Dim profile_info As CLASS_PROFILE.TYPE_USER_PROFILE
    Dim rc As Integer

    profile_info = New CLASS_PROFILE.TYPE_USER_PROFILE
    profile_info.profile_id = ProfileId
    rc = ReadProfileInfo(profile_info)

    If rc <> ENUM_CONFIGURATION_RC.CONFIGURATION_OK Then
      Return False
    End If

    Return profile_info.gup_master

  End Function ' GetGupMaster

  ' PURPOSE: Read Database to obtain the prolile name for the given ID
  '
  '  PARAMS:
  '     - INPUT:
  '           - ProfileId
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - ProfileName or INVALID_PROFILE_NAME if error
  '
  Shared Function GetProfileNameForId(ByVal ProfileId As Integer, ByRef SqlCtx As Integer) As String

    Dim profile_name As String
    Dim profile_info As CLASS_PROFILE.TYPE_USER_PROFILE
    Dim rc As Integer

    profile_info = New CLASS_PROFILE.TYPE_USER_PROFILE
    profile_info.profile_id = ProfileId
    rc = ReadProfileInfo(profile_info)
    If rc = ENUM_CONFIGURATION_RC.CONFIGURATION_OK Then
      profile_name = profile_info.profile_name
    Else
      profile_name = INVALID_PROFILE_NAME
    End If

    Return profile_name

  End Function

  ' PURPOSE: Get the number of users assigned to a profile
  '
  '  PARAMS:
  '     - INPUT:
  '           - ProfileId
  '
  '     - OUTPUT:
  '           - Users
  '
  ' RETURNS:
  '     - ProfileName or INVALID_PROFILE_NAME if error
  '
  Shared Function UsersOfProfile(ByVal ProfileId As Integer, _
                                 ByRef Users As Integer) As Integer
    Dim _str_sql As String
    Dim _data_table As DataTable


    _str_sql = "SELECT  COUNT(*)  " & _
              "  FROM  GUI_USERS " & _
              " WHERE  GU_PROFILE_ID = " & ProfileId

    _data_table = GUI_GetTableUsingSQL(_str_sql, 5000)

    If IsNothing(_data_table) Or _data_table.Rows.Count = 0 Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    If Not IsNothing(_data_table) Then
      Users = _data_table.Rows.Item(0).Item(0)
    End If

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
  End Function

  ' PURPOSE: Check if this profile can be assigned to more users
  '
  '  PARAMS:
  '     - INPUT:
  '             - ProfileId
  '
  '     - OUTPUT:
  '             - MaxUsers
  '
  ' RETURNS:
  '     - TRUE: Has not yet reached the users limit.
  '     - FALSE: It has reached the maximum permitted users. 
  '
  Shared Function CanAssignMoreUsers(ByVal ProfileId As Integer, _
                                     ByRef MaxUsers As Integer) As Boolean

    Dim _profile_info As CLASS_PROFILE.TYPE_USER_PROFILE
    Dim _rc As Integer
    Dim _users As Integer

    _profile_info = New CLASS_PROFILE.TYPE_USER_PROFILE
    _profile_info.profile_id = ProfileId

    MaxUsers = 0

    _rc = ReadProfileInfo(_profile_info)

    If _rc <> ENUM_CONFIGURATION_RC.CONFIGURATION_OK Then
      Return False
    End If

    MaxUsers = _profile_info.max_users

    ' 0 is unlimited number of users.
    If MaxUsers = 0 Then
      Return True
    End If

    _rc = UsersOfProfile(ProfileId, _users)

    If _rc <> ENUM_CONFIGURATION_RC.CONFIGURATION_OK Then
      Return False
    End If

    Return _users < MaxUsers

  End Function

#End Region

End Class
