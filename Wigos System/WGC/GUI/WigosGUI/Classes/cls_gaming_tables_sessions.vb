'-------------------------------------------------------------------
' Copyright � 2009 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_gaming_tables_sessions.vb
' DESCRIPTION:   Gaming tables sessions class
' AUTHOR:        Sergi Mart�nez
' CREATION DATE: 16-DIC-2013
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 16-DIC-2013  SMN    First
'--------------------------------------------------------------------

Imports WSI.Common
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports System.Text

Public Class CLASS_GAMING_TABLES_SESSIONS
  Inherits CLASS_BASE

#Region " Structures "
  <StructLayout(LayoutKind.Sequential)> _
Public Class TYPE_GAMING_TABLE_SESSION
    Public GTS_UNIQUE_ID As Int64
    Public GTS_GAMING_TABLES_ID As Int64
    Public GTS_CS_SESSION_ID As Int64
    Public GTS_OPEN_DATETIME As Date
    Public GTS_OPEN_USER_ID As Int64
    Public GTS_CLOSE_DATETIME As Date
    Public GTS_CLOSE_USER_ID As Int64
    Public GTS_INITIAL_AMOUNT As Decimal
    Public GTS_COLLECTED_AMOUNT As Decimal
    Public GTS_STATUS As Integer
    Public GTS_CLIENT_VISITS As Integer
  End Class
#End Region ' Structures

#Region " Members "

  Protected m_data_gaming_tables_sessions As New TYPE_GAMING_TABLE_SESSION

#End Region ' Members

#Region " Properties "

  Public Property GtsUniqueId() As Integer
    Get
      Return m_data_gaming_tables_sessions.GTS_UNIQUE_ID
    End Get
    Set(ByVal Value As Integer)
      m_data_gaming_tables_sessions.GTS_UNIQUE_ID = Value
    End Set
  End Property

  Public Property GtsClientVisits() As Integer
    Get
      Return m_data_gaming_tables_sessions.GTS_CLIENT_VISITS
    End Get
    Set(ByVal Value As Integer)
      m_data_gaming_tables_sessions.GTS_CLIENT_VISITS = Value
    End Set
  End Property

  Public Property GtsCloseDateTime() As Date
    Get
      Return m_data_gaming_tables_sessions.GTS_CLOSE_DATETIME
    End Get
    Set(ByVal Value As Date)
      m_data_gaming_tables_sessions.GTS_CLOSE_DATETIME = Value
    End Set
  End Property

  Public Property GtsCloseUserId() As Integer
    Get
      Return m_data_gaming_tables_sessions.GTS_CLOSE_USER_ID
    End Get
    Set(ByVal Value As Integer)
      m_data_gaming_tables_sessions.GTS_CLOSE_USER_ID = Value
    End Set
  End Property

  Public Property GtsCollectedAmount() As Integer
    Get
      Return m_data_gaming_tables_sessions.GTS_COLLECTED_AMOUNT
    End Get
    Set(ByVal Value As Integer)
      m_data_gaming_tables_sessions.GTS_COLLECTED_AMOUNT = Value
    End Set
  End Property

  Public Property GtsCsSessionId() As Integer
    Get
      Return m_data_gaming_tables_sessions.GTS_CS_SESSION_ID
    End Get
    Set(ByVal Value As Integer)
      m_data_gaming_tables_sessions.GTS_CS_SESSION_ID = Value
    End Set
  End Property

  Public Property GtsGamingTableId() As Integer
    Get
      Return m_data_gaming_tables_sessions.GTS_GAMING_TABLES_ID
    End Get
    Set(ByVal Value As Integer)
      m_data_gaming_tables_sessions.GTS_GAMING_TABLES_ID = Value
    End Set
  End Property

  Public Property GtsInitialAmount() As Decimal
    Get
      Return m_data_gaming_tables_sessions.GTS_INITIAL_AMOUNT
    End Get
    Set(ByVal Value As Decimal)
      m_data_gaming_tables_sessions.GTS_INITIAL_AMOUNT = Value
    End Set
  End Property

  Public Property GtsOpenDateTime() As Date
    Get
      Return m_data_gaming_tables_sessions.GTS_OPEN_DATETIME
    End Get
    Set(ByVal Value As Date)
      m_data_gaming_tables_sessions.GTS_OPEN_DATETIME = Value
    End Set
  End Property

  Public Property GtsOpenUserId() As Integer
    Get
      Return m_data_gaming_tables_sessions.GTS_OPEN_USER_ID
    End Get
    Set(ByVal Value As Integer)
      m_data_gaming_tables_sessions.GTS_OPEN_USER_ID = Value
    End Set
  End Property

  Public Property GtsStatus() As Integer
    Get
      Return m_data_gaming_tables_sessions.GTS_STATUS
    End Get
    Set(ByVal Value As Integer)
      m_data_gaming_tables_sessions.GTS_STATUS = Value
    End Set
  End Property
#End Region ' Properties

#Region " Overrides "

  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA
    Dim auditor_data As CLASS_AUDITOR_DATA

    auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_ACCOUNT)

    Return auditor_data
  End Function

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return ENUM_STATUS.STATUS_OK
  End Function

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return ENUM_STATUS.STATUS_OK
  End Function

  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As ENUM_STATUS

    Me.GtsUniqueId = ObjectId
    _rc = ENUM_STATUS.STATUS_ERROR

    Using _db_trx As New DB_TRX()
      ' Read terminal data
      _rc = GamingTablesSessions_DbRead(_db_trx.SqlTransaction)
    End Using

    Return _rc

  End Function

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return ENUM_STATUS.STATUS_OK
  End Function

  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE
    Dim temp_gts As CLASS_GAMING_TABLES_SESSIONS

    temp_gts = New CLASS_GAMING_TABLES_SESSIONS

    Return temp_gts
  End Function


#End Region ' Overrides

#Region " Private Functions "


  '----------------------------------------------------------------------------
  ' PURPOSE: Reads an object from the database.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - STATUS_OK
  '     - STATUS_NOT_FOUND
  '     - STATUS_ERROR
  '
  ' NOTES:
  Private Function GamingTablesSessions_DbRead(ByVal SqlTrans As SqlTransaction) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _sb As StringBuilder
    Dim _data_table As DataTable

    Try

      _sb = New StringBuilder()

      _sb.AppendLine(" SELECT     GTS_UNIQUE_ID               ")
      _sb.AppendLine("         ,  GTS_GAMING_TABLES_ID        ")
      _sb.AppendLine("         ,  GTS_CS_SESSION_ID           ")
      _sb.AppendLine("         ,  GTS_OPEN_DATETIME           ")
      _sb.AppendLine("         ,  GTS_OPEN_USER_ID            ")
      _sb.AppendLine("         ,  GTS_CLOSE_DATETIME          ")
      _sb.AppendLine("         ,  GTS_CLOSE_USER_ID           ")
      _sb.AppendLine("         ,  GTS_INITIAL_AMOUNT          ")
      _sb.AppendLine("         ,  GTS_COLLECTED_AMOUNT        ")
      _sb.AppendLine("         ,  GTS_STATUS                  ")
      _sb.AppendLine("         ,  GTS_CLIENT_VISITS           ")
      _sb.AppendLine("   FROM     GAMING_TABLES_SESSIONS      ")
      _sb.AppendLine("  WHERE     GTS_UNIQUE_ID = " + Me.GtsUniqueId.ToString())

      _data_table = GUI_GetTableUsingSQL(_sb.ToString(), 5000)

      If IsNothing(_data_table) Then
        Return ENUM_STATUS.STATUS_ERROR
      End If

      If _data_table.Rows.Count() = 0 Then
        Return ENUM_STATUS.STATUS_NOT_FOUND
      End If

      If _data_table.Rows.Count() <> 1 Then
        Return ENUM_STATUS.STATUS_ERROR
      End If

      Me.GtsGamingTableId = IIf(IsDBNull(_data_table.Rows(0).Item("GTS_GAMING_TABLES_ID")), 0, _data_table.Rows(0).Item("GTS_GAMING_TABLES_ID"))
      Me.GtsCsSessionId = IIf(IsDBNull(_data_table.Rows(0).Item("GTS_CS_SESSION_ID")), 0, _data_table.Rows(0).Item("GTS_CS_SESSION_ID"))
      Me.GtsOpenDateTime = IIf(IsDBNull(_data_table.Rows(0).Item("GTS_OPEN_DATETIME")), Date.MinValue, _data_table.Rows(0).Item("GTS_OPEN_DATETIME"))
      Me.GtsOpenUserId = IIf(IsDBNull(_data_table.Rows(0).Item("GTS_OPEN_USER_ID")), 0, _data_table.Rows(0).Item("GTS_OPEN_USER_ID"))
      Me.GtsCloseDateTime = IIf(IsDBNull(_data_table.Rows(0).Item("GTS_CLOSE_DATETIME")), Date.MinValue, _data_table.Rows(0).Item("GTS_CLOSE_DATETIME"))
      Me.GtsCloseUserId = IIf(IsDBNull(_data_table.Rows(0).Item("GTS_CLOSE_USER_ID")), 0, _data_table.Rows(0).Item("GTS_CLOSE_USER_ID"))
      Me.GtsInitialAmount = IIf(IsDBNull(_data_table.Rows(0).Item("GTS_INITIAL_AMOUNT")), 0, _data_table.Rows(0).Item("GTS_INITIAL_AMOUNT"))
      Me.GtsCollectedAmount = IIf(IsDBNull(_data_table.Rows(0).Item("GTS_COLLECTED_AMOUNT")), 0, _data_table.Rows(0).Item("GTS_COLLECTED_AMOUNT"))
      Me.GtsStatus = IIf(IsDBNull(_data_table.Rows(0).Item("GTS_STATUS")), 0, _data_table.Rows(0).Item("GTS_STATUS"))
      Me.GtsClientVisits = IIf(IsDBNull(_data_table.Rows(0).Item("GTS_CLIENT_VISITS")), 0, _data_table.Rows(0).Item("GTS_CLIENT_VISITS"))

      Return ENUM_STATUS.STATUS_OK

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return ENUM_STATUS.STATUS_ERROR
  End Function

#End Region

End Class