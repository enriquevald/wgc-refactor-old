'------------------------------------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'------------------------------------------------------------------------------------------------
'
' MODULE NAME : cls_layout_config.vb
'
' DESCRIPTION : 
'
' REVISION HISTORY:
' 
' Date         Author Description
' -----------  ------ ---------------------------------------------------------------------------
' 17-JUL-2014  RRR    Initial version.
'

#Region " Imports "

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports WSI.Common
Imports WSI.Common.OperationsSchedule
Imports System.Text
Imports System.Data.SqlClient
Imports GUI_Controls.Misc
Imports System.Runtime.InteropServices

#End Region ' Imports

Public Class cls_layout_range
  Inherits CLASS_BASE

#Region " Members "

  Private m_id As Int32
  Private m_name As String
  Private m_field As String
  Private m_type As Int32

#End Region ' Members

#Region "Properties"

  Public Property Id() As Int32
    Get
      Return m_id
    End Get
    Set(ByVal value As Int32)
      m_id = value
    End Set
  End Property

  Public Property Name() As String
    Get
      Return m_name
    End Get
    Set(ByVal value As String)
      m_name = value
    End Set
  End Property

  Public Property Field() As String
    Get
      Return m_field
    End Get
    Set(ByVal value As String)
      m_field = value
    End Set
  End Property

  Public Property Type() As Int32
    Get
      Return m_type
    End Get
    Set(ByVal value As Int32)
      m_type = value
    End Set
  End Property

#End Region ' Properties

#Region " Overrides "

  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim _auditor_data As CLASS_AUDITOR_DATA

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_LAYOUT_CONFIGURATION)

    ' TODO: Utilizar PlayerTracking
    Call _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(5101), Me.Name)

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5102), Me.Name)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5103), Me.Field)

    Return _auditor_data

  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE : reads the given object to the database.
  '
  ' PARAMS :
  '     - INPUT :
  '         - SqlCtx: SqlTransaction.
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '
  ' NOTES :
  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As ENUM_STATUS

    Me.Id = ObjectId
    Using _db_trx As New DB_TRX()

      _rc = Me.Read(_db_trx.SqlTransaction)

    End Using ' _db_trx

    Return _rc

  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE : Inserts the given object to the database.
  '
  ' PARAMS :
  '     - INPUT :
  '         - SqlCtx: SqlTransaction.
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '
  ' NOTES :
  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As ENUM_STATUS

    _rc = ENUM_STATUS.STATUS_ERROR

    Using _db_trx As New DB_TRX()
      ' Insert terminal data
      _rc = Me.Insert(_db_trx.SqlTransaction)
    End Using

    Return _rc

  End Function ' DB_Insert

  '----------------------------------------------------------------------------
  ' PURPOSE : Updates the given object to the database.
  '
  ' PARAMS :
  '     - INPUT :
  '         - SqlCtx: SqlTransaction.
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '
  ' NOTES :
  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _rc As ENUM_STATUS

    _rc = ENUM_STATUS.STATUS_ERROR

    Using _db_trx As New DB_TRX()

      _rc = Me.Update(_db_trx.SqlTransaction)

      If _rc <> ENUM_STATUS.STATUS_OK Then

        Return _rc
      End If

      If _db_trx.Commit() Then

        Return ENUM_STATUS.STATUS_OK
      End If

    End Using ' _db_trx

    Return _rc
  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE : Deletes the given object from the database.
  '
  ' PARAMS :
  '     - INPUT :
  '         - SqlCtx: SqlTransaction.
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_DEPENDENCIES
  '     - STATUS_ERROR
  '
  ' NOTES :

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As ENUM_STATUS

    _rc = ENUM_STATUS.STATUS_ERROR

    Using _db_trx As New DB_TRX()
      ' Delete terminal data
      _rc = Delete(_db_trx.SqlTransaction)
    End Using

    Return _rc

  End Function ' DB_Delete

  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim _clone As cls_layout_range
    _clone = New cls_layout_range()

    _clone.m_id = Me.m_id
    _clone.m_name = Me.m_name
    _clone.m_field = Me.m_field
    _clone.m_type = Me.m_type

    Return _clone

  End Function

#End Region ' Overrides

#Region " Private Function "

  '----------------------------------------------------------------------------
  ' PURPOSE : Reads a Layout Range object from DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - Trx
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '
  ' NOTES :
  '
  Private Function Read(ByVal Trx As SqlTransaction) As ENUM_STATUS

    Dim _sb As StringBuilder

    Try
      _sb = New StringBuilder()
      _sb.AppendLine(" SELECT LR_ID         ")
      _sb.AppendLine("      , LR_NAME       ")
      _sb.AppendLine("      , LR_FIELD      ")
      _sb.AppendLine("      , LR_TYPE       ")
      _sb.AppendLine("   FROM LAYOUT_RANGES ")
      _sb.AppendLine("  WHERE LR_ID = @pId  ")

      Using _cmd As New SqlCommand(_sb.ToString(), Trx.Connection, Trx)

        _cmd.Parameters.Add("@pId", SqlDbType.Int).Value = Me.m_id

        Using _reader As SqlDataReader = _cmd.ExecuteReader()
          While _reader.Read()

            'Name
            If IsDBNull(_reader(1)) Then
              Me.m_name = False
            Else
              Me.m_name = _reader.GetString(1)
            End If

            'Field
            If IsDBNull(_reader(2)) Then
              Me.m_field = 0
            Else
              m_field = _reader.GetString(2)
            End If

            'Type
            If IsDBNull(_reader(3)) Then
              m_type = False
            Else
              m_type = _reader.GetInt32(3)
            End If

          End While
        End Using
      End Using

      Return ENUM_STATUS.STATUS_OK

    Catch ex As Exception
      Log.Exception(ex)
    End Try

    Return ENUM_STATUS.STATUS_ERROR

  End Function ' Read

  '----------------------------------------------------------------------------
  ' PURPOSE : Updates a Layout Range object in DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - Trx
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '
  ' NOTES :
  '
  Private Function Update(ByVal Trx As SqlTransaction) As ENUM_STATUS
    Dim _sb As StringBuilder

    Try

      _sb = New StringBuilder()

      _sb.AppendLine(" UPDATE LAYOUT_RANGES      ")
      _sb.AppendLine("    SET LR_NAME = @pName   ")
      _sb.AppendLine("      , LR_FIELD = @pField ")
      _sb.AppendLine("      , LR_TYPE = @pType   ")
      _sb.AppendLine("  WHERE LR_ID = @pId       ")

      Using _cmd As New SqlCommand(_sb.ToString, Trx.Connection, Trx)

        _cmd.Parameters.Add("@pId", SqlDbType.Int).Value = Me.m_id
        _cmd.Parameters.Add("@pName", SqlDbType.VarChar).Value = Me.m_name
        _cmd.Parameters.Add("@pField", SqlDbType.VarChar).Value = Me.m_field
        _cmd.Parameters.Add("@pType", SqlDbType.Int).Value = Me.m_type

        If _cmd.ExecuteNonQuery() <> 1 Then
          Return ENUM_STATUS.STATUS_ERROR
        End If

      End Using

      Return ENUM_STATUS.STATUS_OK

    Catch _ex As Exception

      Return ENUM_STATUS.STATUS_ERROR

    End Try

  End Function ' Update

  '----------------------------------------------------------------------------
  ' PURPOSE : Inserts a Layout Range object into DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - Trx
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '
  ' NOTES :
  '
  Private Function Insert(ByVal SqlTrans As SqlTransaction) As ENUM_STATUS
    Dim _sb As StringBuilder
    Dim _param_id As SqlParameter
    Dim _row_affected As Integer

    Try

      _sb = New StringBuilder()
      _sb.AppendLine("INSERT INTO  LAYOUT_RANGES	          ")
      _sb.AppendLine("     VALUES	(@pName					          ")
      _sb.AppendLine("           , @pField	  		          ")
      _sb.AppendLine("           , @pType)	  		          ")
      _sb.AppendLine("        SET  @pId = SCOPE_IDENTITY(); ")

      Using _cmd As New SqlCommand(_sb.ToString(), SqlTrans.Connection, SqlTrans)

        _cmd.Parameters.Add("@pName", SqlDbType.VarChar).Value = Me.Name
        _cmd.Parameters.Add("@pField", SqlDbType.VarChar).Value = Me.Field
        _cmd.Parameters.Add("@pType", SqlDbType.Int).Value = Me.Type

        _param_id = _cmd.Parameters.Add("@pId", SqlDbType.Int)
        _param_id.Direction = ParameterDirection.Output

        _row_affected = _cmd.ExecuteNonQuery()

        If _row_affected <> 1 Or IsDBNull(_param_id.Value) Then
          Return ENUM_STATUS.STATUS_ERROR
        End If

        Me.Id = _param_id.Value

        SqlTrans.Commit()
        Return ENUM_STATUS.STATUS_OK

      End Using

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return ENUM_STATUS.STATUS_ERROR

  End Function ' Insert

  '----------------------------------------------------------------------------
  ' PURPOSE : Deletes a Layout Range object from DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - SqlTrans
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_DEPENDENCIES
  '     - STATUS_ERROR
  '
  ' NOTES :

  Private Function Delete(ByVal SqlTrans As SqlTransaction) As Integer

    Dim _sb As StringBuilder

    Try

      _sb = New StringBuilder()
      _sb.AppendLine("DELETE FROM LAYOUT_RANGES         ")
      _sb.AppendLine(" WHERE LR_ID = " & Me.Id.ToString())

      If Not GUI_SQLExecuteNonQuery(_sb.ToString(), SqlTrans) Then
        Return ENUM_STATUS.STATUS_ERROR
      End If

      SqlTrans.Commit()
      Return ENUM_STATUS.STATUS_OK

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return ENUM_STATUS.STATUS_ERROR

  End Function ' Delete


#End Region ' Private Function

End Class
