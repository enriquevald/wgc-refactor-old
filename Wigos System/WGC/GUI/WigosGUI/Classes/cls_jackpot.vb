﻿'-------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : cls_jackpot.vb
'
' DESCRIPTION : countr class for user edition
'              
' REVISION HISTORY :
'
' Date         Author  Description
' -----------  ------  -----------------------------------------------
' 31-MAR-2017  JBP     Initial version
'-------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports System.Text
Imports WSI.Common
Imports WSI.Common.Jackpot

Public Class CLASS_JACKPOT
  Inherits CLASS_BASE

#Region " Constants "

#End Region ' Constants

#Region " Enums "

#End Region ' Enums 

#Region " Structures "

#End Region ' Structures

#Region " Members "

  Protected m_list As New List(Of Jackpot)
  Protected m_selected As New Jackpot

#End Region ' Members

#Region " Properties "

  Public Property Selected As Jackpot
    Get
      Return Me.m_selected
    End Get
    Set(value As Jackpot)
      Me.m_selected = value
    End Set
  End Property

  Public Property List As List(Of Jackpot)
    Get
      Return Me.m_list
    End Get
    Set(value As List(Of Jackpot))
      Me.m_list = value
    End Set

  End Property

#End Region ' Properties

#Region " Public Functions "

  Public Sub New()

    Me.List = New Jackpot().GetAllJackpots()

  End Sub

  ''' <summary>
  ''' Returns the related auditor data for the current object.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function AuditorData() As CLASS_AUDITOR_DATA
    Dim _auditor_data As CLASS_AUDITOR_DATA

    ' Set Jackpot auditor data
    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_JACKPOT_CONFIG)
    _auditor_data.SetName(GLB_NLS_GUI_JACKPOT_MGR.Id(535), " - " + Me.Selected.Name)

    ' Jackpot Settings
    Call AuditorData_JackpotSettings(_auditor_data)

    ' Contribution
    Call AuditorData_JackpotContributions(_auditor_data)

    ' Award Filters
    Call AuditorData_JackpotAwardFilters(_auditor_data)

    ' Award WeekDays
    Call AuditorData_JackpotAwardWeekDays(_auditor_data)

    ' Prize Sharing
    Call AuditorData_JackpotPrizeSharing(_auditor_data)

    ' Happy Hour
    Call AuditorData_JackpotHappyHour(_auditor_data)

    ' Customer Tier
    Call AuditorData_JackpotCustomerTier(_auditor_data)

    ' Flags
    Call AuditorData_JackpotFlags(_auditor_data) ' TODO UNRANKED - RLO: repasar

    Return _auditor_data

  End Function ' AuditorData

  ''' <summary>
  ''' Deletes the given object from the database.
  ''' </summary>
  ''' <param name="SqlCtx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As CLASS_BASE.ENUM_STATUS

    Try

      Return ENUM_STATUS.STATUS_ERROR

    Catch ex As Exception
      Log.Error(String.Format("Jackpot DB_Delete: There was an error deleting JackpotId: {0}", Me.Selected.Id))
      Log.Exception(ex)

      Return ENUM_STATUS.STATUS_ERROR
    End Try

    Return ENUM_STATUS.STATUS_OK

  End Function

  ''' <summary>
  ''' Reads from the database into the given object
  ''' </summary>
  ''' <param name="ObjectId"></param>
  ''' <param name="SqlCtx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function DB_Read(ObjectId As Object, ByRef SqlCtx As Integer) As CLASS_BASE.ENUM_STATUS
    Dim _jackpot As Jackpot

    Try

      Using _db_trx As New DB_TRX()

        _jackpot = New Jackpot(ObjectId, _db_trx.SqlTransaction)

        If _jackpot Is Nothing Then
          Return ENUM_STATUS.STATUS_ERROR
        End If

        Me.m_list.SetJackpotById(ObjectId, _jackpot)
        Me.m_selected = _jackpot

      End Using

    Catch ex As Exception
      Log.Error(String.Format("Jackpot DB_Read: There was an error getting JackpotId:{0}", ObjectId))
      Log.Exception(ex)

      Return ENUM_STATUS.STATUS_ERROR

    End Try

    Return ENUM_STATUS.STATUS_OK

  End Function ' DB_Read

  ''' <summary>
  ''' Inserts the given object to the database.
  ''' </summary>
  ''' <param name="SqlCtx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As CLASS_BASE.ENUM_STATUS

    Try

      Using _db_trx As New DB_TRX()

        ' Save Jackpot
        If Not Me.Selected.Save(_db_trx.SqlTransaction) Then
          Return ENUM_STATUS.STATUS_ERROR
        End If

        Me.m_list.Add(Me.Selected.Clone())
        _db_trx.Commit()

      End Using

    Catch ex As Exception
      Log.Error(String.Format("Jackpot DB_Insert: There was an error getting JackpotId:{0}", Me.Selected.Id))
      Log.Exception(ex)

      Return ENUM_STATUS.STATUS_ERROR
    End Try

    Return ENUM_STATUS.STATUS_OK

  End Function ' DB_Insert

  ''' <summary>
  ''' Updates the given object to the database.
  ''' </summary>
  ''' <param name="SqlCtx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As CLASS_BASE.ENUM_STATUS

    Try

      ' Save Jackpot
      If Not Me.Selected.Save() Then
        Return ENUM_STATUS.STATUS_ERROR
      End If

    Catch ex As Exception

      Log.Error(String.Format("Jackpot DB_Update: There was an error getting JackpotId:{0}", Me.Selected.Id))
      Log.Exception(ex)

      Return ENUM_STATUS.STATUS_ERROR
    End Try

    Return ENUM_STATUS.STATUS_OK

  End Function ' DB_Update

  ''' <summary>
  ''' Returns a duplicate of the given object.
  ''' Not includes ID
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function Duplicate() As CLASS_BASE

    Dim _temp_object As CLASS_JACKPOT

    _temp_object = New CLASS_JACKPOT

    _temp_object.List = Me.List.Clone()

    If Not Me.Selected Is Nothing Then
      _temp_object.Selected = Me.Selected.Clone()
    End If

    Return _temp_object

  End Function ' Duplicate

#Region " Jackpot "

#End Region

#End Region ' Public Functions

#Region " Private Functions "


  Private Function BooleanToAuditString(ByVal Value As Boolean) As String
    Return IIf(Value, GLB_NLS_GUI_AUDITOR.GetString(336), GLB_NLS_GUI_AUDITOR.GetString(337))
  End Function

#Region " Auditor Data "

  ''' <summary>
  ''' Add Jackpot settings fields to auditor data
  ''' </summary>
  ''' <param name="AuditorData"></param>
  ''' <remarks></remarks>
  Private Sub AuditorData_JackpotSettings(ByRef AuditorData As CLASS_AUDITOR_DATA)
    Call AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(282), Me.Selected.Name) ' Name

    If Me.Selected.Enabled Then
      AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(218), GLB_NLS_GUI_JACKPOT_MGR.GetString(218)) ' Enabled
    Else
      AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(218), GLB_NLS_GUI_JACKPOT_MGR.GetString(250)) ' Disabled
    End If

    AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(490), Me.Selected.PayoutMode) ' Payout Mode
    AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(500), Me.Selected.Type) ' Type
    AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(501), Me.Selected.IsoCode) ' IsoCode
    AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(502), Me.Selected.CreditPrizeType) ' Credit Prize Type

    AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(285), GUI_FormatNumber(Me.Selected.Minimum, 2)) ' Minimum
    AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(286), GUI_FormatNumber(Me.Selected.Maximum, 2)) ' Maximum
    AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(287), GUI_FormatNumber(Me.Selected.Average, 2)) ' Average

    AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(573), GUI_FormatNumber(Me.Selected.AdvNotificationEnabled, 0)) ' Advance notification enabled
    AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(566), GUI_FormatNumber(Me.Selected.AdvNotificationTime, 0)) ' Advance Notification

    AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(557), GUI_FormatNumber(Me.Selected.PrizeSharingPct, 2)) ' Prize sharing %
    AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(556), GUI_FormatNumber(Me.Selected.HiddenPct, 2)) ' Hidden %
    AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(558), GUI_FormatNumber(Me.Selected.HappyHourPct, 2)) ' Happy hour %

    ' Compensate Contribution
    AuditorData.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8409), Me.Selected.CompensationFixedPct) ' Compensation  fixed type
    AuditorData.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8414), Me.Selected.CompensationType) ' Compensation type

    ' Jackpot Behaviour
    AuditorData.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8423), Me.Selected.ProbabilityWithMax) ' Probability Increase
    AuditorData.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8424), Me.Selected.MaximumNumPending) ' Maximum Number Pending

  End Sub ' AuditorData_JackpotSettings

  ''' <summary>
  ''' Add Jackpot contribution settings fields to auditor data
  ''' </summary>
  ''' <param name="AuditorData"></param>
  ''' <remarks></remarks>
  Private Sub AuditorData_JackpotContributions(ByRef AuditorData As CLASS_AUDITOR_DATA)

    If Me.Selected.Contributions Is Nothing Then
      Return
    End If

    ' Add all contributions groups to auditor data
    For Each _contribution As JackpotContribution In Me.Selected.Contributions.Items

      ' Set fields
      AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(504), _contribution.Name) ' Contribution group
      '                                                                                                                           Fixed                                 Weekly
      AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(525), IIf(_contribution.Type = Jackpot.ContributionType.Fixed, GLB_NLS_GUI_JACKPOT_MGR.GetString(505), GLB_NLS_GUI_JACKPOT_MGR.GetString(506))) ' Contribution type -> Fixed | Weekly

      If _contribution.Type = Jackpot.ContributionType.Fixed Then
        AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(505), GUI_FormatNumber(_contribution.FixedContribution, 2)) ' Contribution
      Else
        AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(289), GUI_FormatNumber(_contribution.Monday, 2)) ' Monday
        AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(290), GUI_FormatNumber(_contribution.Tuesday, 2)) ' Tuesday
        AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(291), GUI_FormatNumber(_contribution.Wednesday, 2)) ' Wednesday
        AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(292), GUI_FormatNumber(_contribution.Thursday, 2)) ' Thursday
        AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(293), GUI_FormatNumber(_contribution.Friday, 2)) ' Friday
        AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(294), GUI_FormatNumber(_contribution.Saturday, 2)) ' Saturday
        AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(295), GUI_FormatNumber(_contribution.Sunday, 2)) ' Sunday
      End If

      AuditorData.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8321), _contribution.ContributionState)  ' Contribution State
      AuditorData.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5220), _contribution.Terminals) ' Terminals
      AuditorData.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8322), _contribution.IsConfigured) ' IsConfigured
    Next

  End Sub ' AuditorData_JackpotContributions

  ''' <summary>
  ''' Add Jackpot award filters settings fields to auditor data
  ''' </summary>
  ''' <param name="AuditorData"></param>
  ''' <remarks></remarks>
  Private Sub AuditorData_JackpotAwardFilters(ByRef AuditorData As CLASS_AUDITOR_DATA)

    If Me.Selected.AwardConfig.Filter Is Nothing Then
      Return
    End If

    ' Set fields
    AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(522), GUI_FormatNumber(Me.Selected.AwardConfig.Filter.MinOccupancy, 2)) ' MinOccupancy
    AuditorData.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8415), GUI_FormatNumber(Me.Selected.AwardConfig.Filter.MinimumBet, 2)) ' MinimumBet
    AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(523), Me.BooleanToAuditString(Me.Selected.AwardConfig.Filter.ExcludeAccountsWithPromo))   ' ExcludeAccountsWithPromo
    AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(524), Me.BooleanToAuditString(Me.Selected.AwardConfig.Filter.ExcludeAnonymousAccount))    ' ExcludeAnonymousAccount
    AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(530), Me.BooleanToAuditString(Me.Selected.AwardConfig.Filter.AgeRange))                   ' AgeRange
    AuditorData.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2789), Me.Selected.AwardConfig.Filter.AndSmallerThan)                                 ' AndSmallerThan
    AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(531), Me.BooleanToAuditString(Me.Selected.AwardConfig.Filter.AnyDayOfMonth))              ' AnyDayOfMonth
    AuditorData.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5239), Me.Selected.AwardConfig.Filter.ByCreationDate)               ' ByCreationDate
    AuditorData.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(746), Me.Selected.AwardConfig.Filter.ByDateOfBirth)                 ' ByDateOfBirth
    AuditorData.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(213), Me.Selected.AwardConfig.Filter.ByGender)                      ' ByGender
    AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(8413), Me.Selected.AwardConfig.Filter.CreationBetweenThat)               ' CreationBetweenThat
    AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(533), Me.Selected.AwardConfig.Filter.CreationBetweenThis)               ' CreationBetweenThis
    AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(534), Me.Selected.AwardConfig.Filter.DayAndMonth)                       ' DayAndMonth
    AuditorData.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6723), Me.Selected.AwardConfig.Filter.DayOfCreationPlus)            ' DayOfCreationPlus
    AuditorData.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6722), Me.Selected.AwardConfig.Filter.JustDayOfCreation)            ' JustDayOfCreation 
    AuditorData.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2702), Me.Selected.AwardConfig.Filter.OlderThan)                    ' OlderThan
    AuditorData.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2790), Me.Selected.AwardConfig.Filter.OrOlderThan)                  ' OrOlderThan
    AuditorData.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2703), Me.Selected.AwardConfig.Filter.SmallerThan)                  ' SmallerThan
    AuditorData.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8395), Me.Selected.AwardConfig.Filter.SelectedEGM.ToString())          ' SelectedEGM
    AuditorData.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8396), Me.Selected.AwardConfig.Filter.IsConfigured)                    ' IsConfigured
    AuditorData.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8397), Me.Selected.AwardConfig.Filter.SelectionEGMType)                ' SelectionEGMType

  End Sub ' AuditorData_JackpotAwardFilters

  ''' <summary>
  ''' Add Jackpot award weekdays settings fields to auditor data
  ''' </summary>
  ''' <param name="AuditorData"></param>
  ''' <remarks></remarks>
  Private Sub AuditorData_JackpotAwardWeekDays(ByRef AuditorData As CLASS_AUDITOR_DATA)
    Dim _award_time_from_monday As Date
    Dim _award_time_to_monday As Date
    Dim _award_time_from_tuesday As Date
    Dim _award_time_to_tuesday As Date
    Dim _award_time_from_wednesday As Date
    Dim _award_time_to_wednesday As Date
    Dim _award_time_from_thursday As Date
    Dim _award_time_to_thursday As Date
    Dim _award_time_from_friday As Date
    Dim _award_time_to_friday As Date
    Dim _award_time_from_saturday As Date
    Dim _award_time_to_saturday As Date
    Dim _award_time_from_sunday As Date
    Dim _award_time_to_sunday As Date
    Dim _is_checked As Boolean

    If Me.Selected.AwardConfig.WeekDays Is Nothing Then
      Return
    End If

    ' Set fields
    _award_time_from_monday = Me.Selected.AwardConfig.WeekDays.MondayFrom.HourInMinutesToDate()
    _award_time_to_monday = Me.Selected.AwardConfig.WeekDays.MondayTo.HourInMinutesToDate()
    _award_time_from_tuesday = Me.Selected.AwardConfig.WeekDays.TuesdayFrom.HourInMinutesToDate()
    _award_time_to_tuesday = Me.Selected.AwardConfig.WeekDays.TuesdayTo.HourInMinutesToDate()
    _award_time_from_wednesday = Me.Selected.AwardConfig.WeekDays.WednesdayFrom.HourInMinutesToDate()
    _award_time_to_wednesday = Me.Selected.AwardConfig.WeekDays.WednesdayTo.HourInMinutesToDate()
    _award_time_from_thursday = Me.Selected.AwardConfig.WeekDays.ThursdayFrom.HourInMinutesToDate()
    _award_time_to_thursday = Me.Selected.AwardConfig.WeekDays.ThursdayTo.HourInMinutesToDate()
    _award_time_from_friday = Me.Selected.AwardConfig.WeekDays.FridayFrom.HourInMinutesToDate()
    _award_time_to_friday = Me.Selected.AwardConfig.WeekDays.FridayTo.HourInMinutesToDate()
    _award_time_from_saturday = Me.Selected.AwardConfig.WeekDays.SaturdayFrom.HourInMinutesToDate()
    _award_time_to_saturday = Me.Selected.AwardConfig.WeekDays.SaturdayTo.HourInMinutesToDate()
    _award_time_from_sunday = Me.Selected.AwardConfig.WeekDays.SundayFrom.HourInMinutesToDate()
    _award_time_to_sunday = Me.Selected.AwardConfig.WeekDays.SundayTo.HourInMinutesToDate()

    '(289)   "Monday"
    _is_checked = (Me.Selected.AwardConfig.WeekDays.MondayFrom >= 0)
    AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(289), IIf(_is_checked, GLB_NLS_GUI_AUDITOR.GetString(336), GLB_NLS_GUI_AUDITOR.GetString(337)))

    If _is_checked Then
      AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(508), _award_time_from_monday)
      AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(509), _award_time_to_monday)
    End If

    '(290)   "Tuesday"
    _is_checked = (Me.Selected.AwardConfig.WeekDays.TuesdayFrom >= 0)
    AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(290), IIf(_is_checked, GLB_NLS_GUI_AUDITOR.GetString(336), GLB_NLS_GUI_AUDITOR.GetString(337)))

    If _is_checked Then
      AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(510), _award_time_from_tuesday)
      AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(511), _award_time_to_tuesday)
    End If

    '(291)   "Wednesday"
    _is_checked = (Me.Selected.AwardConfig.WeekDays.WednesdayFrom >= 0)
    AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(291), IIf(_is_checked, GLB_NLS_GUI_AUDITOR.GetString(336), GLB_NLS_GUI_AUDITOR.GetString(337)))

    If _is_checked Then
      AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(512), _award_time_from_wednesday)
      AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(513), _award_time_to_wednesday)
    End If

    '(292)   "Thursday"
    _is_checked = (Me.Selected.AwardConfig.WeekDays.ThursdayFrom >= 0)
    AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(292), IIf(_is_checked, GLB_NLS_GUI_AUDITOR.GetString(336), GLB_NLS_GUI_AUDITOR.GetString(337)))

    If _is_checked Then
      AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(514), _award_time_from_thursday)
      AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(515), _award_time_to_thursday)
    End If

    '(293)   "Friday"
    _is_checked = (Me.Selected.AwardConfig.WeekDays.FridayFrom >= 0)
    AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(293), IIf(_is_checked, GLB_NLS_GUI_AUDITOR.GetString(336), GLB_NLS_GUI_AUDITOR.GetString(337)))

    If _is_checked Then
      AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(516), _award_time_from_friday)
      AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(517), _award_time_to_friday)
    End If

    '(294)   "Saturday"
    _is_checked = (Me.Selected.AwardConfig.WeekDays.SaturdayFrom >= 0)
    AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(294), IIf(_is_checked, GLB_NLS_GUI_AUDITOR.GetString(336), GLB_NLS_GUI_AUDITOR.GetString(337)))

    If _is_checked Then
      AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(518), _award_time_from_saturday)
      AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(519), _award_time_to_saturday)
    End If

    '(295)   "Sunday"
    _is_checked = (Me.Selected.AwardConfig.WeekDays.SundayFrom >= 0)
    AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(295), IIf(_is_checked, GLB_NLS_GUI_AUDITOR.GetString(336), GLB_NLS_GUI_AUDITOR.GetString(337)))

    If _is_checked Then
      AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(520), _award_time_from_sunday)
      AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(521), _award_time_to_sunday)
    End If

    AuditorData.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8398), Me.Selected.AwardConfig.WeekDays.IsConfigured)      ' IsConfigured

  End Sub ' AuditorData_JackpotAwardWeekDays

  ''' <summary>
  ''' Add Jackpot Prize sharing settings fields to auditor data
  ''' </summary>
  ''' <param name="AuditorData"></param>
  ''' <remarks></remarks>
  Private Sub AuditorData_JackpotPrizeSharing(ByRef AuditorData As CLASS_AUDITOR_DATA)

    If Me.Selected.AwardPrizeConfig.PrizeSharing Is Nothing Then
      Return
    End If

    ' Set fields
    AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(486), Me.Selected.AwardPrizeConfig.PrizeSharing.Enabled)       ' Enabled
    AuditorData.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8358), Me.Selected.AwardPrizeConfig.PrizeSharing.MaxNumWinner)  ' Number of maximun winners
    'AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(0), Me.Selected.AwardPrizeConfig.PrizeSharing.IncludeWinner) ' The winner of jackpot pasrticipate in prize sharing
    AuditorData.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8359), Me.Selected.AwardPrizeConfig.PrizeSharing.SameArea)      ' Same area
    AuditorData.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8360), Me.Selected.AwardPrizeConfig.PrizeSharing.SameBank)      ' Same bank  
    AuditorData.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8361), Me.Selected.AwardPrizeConfig.PrizeSharing.SameJackpot)   ' Same jackpot
    AuditorData.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8412), Me.Selected.AwardPrizeConfig.PrizeSharing.IsConfigured)   ' IsConfigured
    AuditorData.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1603), GUI_FormatNumber(Me.Selected.AwardPrizeConfig.PrizeSharing.Minimum, 2))   ' Minimum Amount
    AuditorData.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8362), GUI_FormatNumber(Me.Selected.AwardPrizeConfig.PrizeSharing.Maximum, 2))   ' Maximum Amount

  End Sub ' AuditorData_JackpotPrizeSharing

  ''' <summary>
  ''' Add Jackpot happy hour settings fields to auditor data
  ''' </summary>
  ''' <param name="AuditorData"></param>
  ''' <remarks></remarks>
  Private Sub AuditorData_JackpotHappyHour(ByRef AuditorData As CLASS_AUDITOR_DATA)

    If Me.Selected.AwardPrizeConfig.HappyHour Is Nothing Then
      Return
    End If

    ' Set fields
    AuditorData.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8399), Me.Selected.AwardPrizeConfig.HappyHour.AmountDistributionAfter)  ' Falta cambiar las NLS
    AuditorData.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8400), Me.Selected.AwardPrizeConfig.HappyHour.AmountDistributionBefore)
    AuditorData.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8401), Me.Selected.AwardPrizeConfig.HappyHour.AwardsAfter)
    AuditorData.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8402), Me.Selected.AwardPrizeConfig.HappyHour.AwardsBefore)
    AuditorData.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8403), Me.Selected.AwardPrizeConfig.HappyHour.EventType)
    AuditorData.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8404), Me.Selected.AwardPrizeConfig.HappyHour.TimeAfter)
    AuditorData.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8405), Me.Selected.AwardPrizeConfig.HappyHour.TimeBefore)
    AuditorData.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8406), Me.Selected.AwardPrizeConfig.HappyHour.IsConfigured)   ' IsConfigured
    AuditorData.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8407), GUI_FormatNumber(Me.Selected.AwardPrizeConfig.HappyHour.Minimum, 2))   ' Minimum Amount
    AuditorData.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8408), GUI_FormatNumber(Me.Selected.AwardPrizeConfig.HappyHour.Maximum, 2))   ' Maximum Amount

  End Sub ' AuditorData_JackpotHappyHour

  ''' <summary>
  ''' Add Jackpot customer tier settings fields to auditor data
  ''' </summary>
  ''' <param name="AuditorData"></param>
  ''' <remarks></remarks>
  Private Sub AuditorData_JackpotCustomerTier(ByRef AuditorData As CLASS_AUDITOR_DATA)

    If Me.Selected.AwardPrizeConfig.CustomerTier Is Nothing Then
      Return
    End If

    ' Set fields
    AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(551), Me.Selected.AwardPrizeConfig.CustomerTier.ByLevel)                                                        ' By Level
    AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(552), Me.Selected.AwardPrizeConfig.CustomerTier.Anonymous)                                                      ' Anonymous
    AuditorData.SetField(0, Me.Selected.AwardPrizeConfig.CustomerTier.Level1, GLB_NLS_GUI_JACKPOT_MGR.GetString(553, GetCashierPlayerTrackingData("Level01.Name"))) ' Level 1
    AuditorData.SetField(0, Me.Selected.AwardPrizeConfig.CustomerTier.Level1, GLB_NLS_GUI_JACKPOT_MGR.GetString(553, GetCashierPlayerTrackingData("Level02.Name"))) ' Level 2
    AuditorData.SetField(0, Me.Selected.AwardPrizeConfig.CustomerTier.Level1, GLB_NLS_GUI_JACKPOT_MGR.GetString(553, GetCashierPlayerTrackingData("Level03.Name"))) ' Level 3
    AuditorData.SetField(0, Me.Selected.AwardPrizeConfig.CustomerTier.Level1, GLB_NLS_GUI_JACKPOT_MGR.GetString(553, GetCashierPlayerTrackingData("Level04.Name"))) ' Level 4
    AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(8410), Me.Selected.AwardPrizeConfig.CustomerTier.Vip)                                                            ' VIP
    AuditorData.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(8411), Me.Selected.AwardPrizeConfig.CustomerTier.IsConfigured)                                                   ' IsConfigured

  End Sub ' AuditorData_JackpotCustomerTier

  ''' <summary>
  ''' Add Jackpot flags settings fields to auditor data
  ''' </summary>
  ''' <param name="AuditorData"></param>
  ''' <remarks></remarks>
  Public Sub AuditorData_JackpotFlags(ByRef AuditorData As CLASS_AUDITOR_DATA)

    If Me.Selected.AwardConfig.Filter.Flags Is Nothing Then
      Return
    End If

    ' Set all flags
    For Each _flag As JackpotAwardFilterFlag In Me.Selected.AwardConfig.Filter.Flags.Items

      AuditorData.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8289), _flag.Name) ' Name
      AuditorData.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8290), _flag.Color) ' Color
      AuditorData.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8291), _flag.FlagCount) ' Flag Count
      AuditorData.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8292), _flag.FlagId) ' Flag ID
      AuditorData.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8293), _flag.Type) ' Type

    Next

  End Sub ' AuditorData_JackpotFlags

#End Region ' Auditor Data

#End Region ' Private Functions

End Class
