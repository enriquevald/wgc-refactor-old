'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_tito_terminal_configuration.vb
' DESCRIPTION:   Terminal configuration for user edition
' AUTHOR:        David Rigal
' CREATION DATE: 10-OCT-2013
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 10-OCT-2013  DRV    Initial version.
'-------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports WSI.Common
Imports System.Runtime.InteropServices
Imports System.Text
Imports System.Data.SqlClient
Public Class cls_tito_terminal_configuration
  Inherits CLASS_BASE
#Region " ENUMS "

#End Region

#Region " STRUCTURES "

  Public Class TYPE_TERMINAL_CONFIG
    Public terminal_id As Int64
    Public terminal_name As String
    Public terminal_status As Integer
    Public validation_type As Integer
    Public allow_cashable As Boolean
    Public allow_promotion_tickets As Boolean
    Public allow_ticket_creation As Boolean
    Public allow_bills As Boolean
  End Class

#End Region

#Region " MEMBERS "

  Protected m_terminal_configuration As New TYPE_TERMINAL_CONFIG

#End Region

#Region " PROPERTIES "

  Public Property TerminalId() As Int64
    Get
      Return m_terminal_configuration.terminal_id
    End Get
    Set(ByVal value As Int64)
      m_terminal_configuration.terminal_id = value
    End Set
  End Property

  Public Property TerminalName() As String
    Get
      Return m_terminal_configuration.terminal_name
    End Get
    Set(ByVal value As String)
      m_terminal_configuration.terminal_name = value
    End Set
  End Property

  Public Property TerminalStatus() As Integer
    Get
      Return m_terminal_configuration.terminal_status
    End Get
    Set(ByVal value As Integer)
      m_terminal_configuration.terminal_status = value
    End Set
  End Property

  Public Property ValidationType() As WSI.Common.TITO_VALIDATION_TYPE
    Get
      Return m_terminal_configuration.validation_type
    End Get
    Set(ByVal value As WSI.Common.TITO_VALIDATION_TYPE)
      m_terminal_configuration.validation_type = value
    End Set
  End Property

  Public Property AllowCashable() As Boolean
    Get
      Return m_terminal_configuration.allow_cashable
    End Get
    Set(ByVal value As Boolean)
      m_terminal_configuration.allow_cashable = value
    End Set
  End Property

  Public Property AllowPromotionTickets() As Boolean
    Get
      Return m_terminal_configuration.allow_promotion_tickets
    End Get
    Set(ByVal value As Boolean)
      m_terminal_configuration.allow_promotion_tickets = value
    End Set
  End Property

  Public Property AllowTicketCreation() As Boolean
    Get
      Return m_terminal_configuration.allow_ticket_creation
    End Get
    Set(ByVal value As Boolean)
      m_terminal_configuration.allow_ticket_creation = value
    End Set
  End Property

  Public Property AllowBills() As Boolean
    Get
      Return m_terminal_configuration.allow_bills
    End Get
    Set(ByVal value As Boolean)
      m_terminal_configuration.allow_bills = value
    End Set
  End Property
#End Region

#Region " OVERRIDES "

  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim _auditor_data As CLASS_AUDITOR_DATA

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_TERMINALS)

    Call _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(1097), Me.TerminalName)           ' 1097 "Terminal"
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2717), IIf(Me.AllowTicketCreation, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)))   ' 2717 "Allow Ticket Creation"
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2718), IIf(Me.AllowCashable, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)))         ' 2718 "Allow Cashable Redemption"
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2719), IIf(Me.AllowPromotionTickets, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279))) ' 2719 "Allow Promotional Redemption"
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2723), IIf(Me.AllowBills, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)))            ' 2723 "Allow Bills"


    Return _auditor_data

  End Function

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

  End Function

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

  End Function

  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _rc As Integer

    Me.m_terminal_configuration.terminal_id = ObjectId

    ' Read gift data
    _rc = ReadTerminalConfiguration(m_terminal_configuration, SqlCtx)

    Select Case _rc

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select
  End Function

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _rc As Integer

    ' Update gift data
    _rc = UpdateTerminalConfiguration(m_terminal_configuration, SqlCtx)

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
        Return ENUM_STATUS.STATUS_DUPLICATE_KEY

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select
  End Function

  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE
    Dim _temp_terminal As cls_tito_terminal_configuration

    _temp_terminal = New cls_tito_terminal_configuration()
    _temp_terminal.TerminalId = Me.TerminalId
    _temp_terminal.TerminalName = Me.TerminalName
    _temp_terminal.TerminalStatus = Me.TerminalStatus
    _temp_terminal.ValidationType = Me.ValidationType
    _temp_terminal.AllowCashable = Me.AllowCashable
    _temp_terminal.AllowPromotionTickets = Me.AllowPromotionTickets
    _temp_terminal.AllowTicketCreation = Me.AllowTicketCreation
    _temp_terminal.AllowBills = Me.AllowBills

    Return _temp_terminal
  End Function

#End Region

#Region " PRIVATE FUNCTIONS "
  '----------------------------------------------------------------------------
  ' PURPOSE : Reads a terminal object from DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - Item.terminal_configuration
  '         - Context
  '
  '     - OUTPUT :
  '         - Item
  '
  ' RETURNS :
  '     - CONFIGURATION_OK
  '     - CONFIGURATION_ERROR_DB
  '
  ' NOTES :

  Private Function ReadTerminalConfiguration(ByVal Item As TYPE_TERMINAL_CONFIG, _
                            ByVal Context As Integer) As Integer

    Dim _sql_query As String
    Dim _data_table As DataTable
    Dim _sql_tx As System.Data.SqlClient.SqlTransaction = Nothing

    _sql_query = "   SELECT    TE_TERMINAL_ID                   AS ID         " _
                    + "      , TE_NAME                          AS NAME       " _
                    + "      , TE_STATUS                        AS STATUS     " _
                    + "      , TE_VALIDATION_TYPE               AS TYPE       " _
                    + "      , TE_ALLOWED_CASHABLE_REDEMPTION   AS CASHABLE   " _
                    + "      , TE_ALLOWED_PROMO_REDEMPTION      AS PROMOTION  " _
                    + "      , TE_ALLOWED_EMISSION              AS EMISSION   " _
                    + "      , TE_ALLOWED_BILLS                 AS BILLS      " _
                    + " FROM   TERMINALS                                      " _
                    + "WHERE   TE_TERMINAL_ID                   =  " + Item.terminal_id.ToString()

    _data_table = GUI_GetTableUsingSQL(_sql_query, 5000)
    If IsNothing(_data_table) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    If _data_table.Rows.Count() <> 1 Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    Item.terminal_name = NullTrim(_data_table.Rows(0).Item("NAME"))
    Item.terminal_status = _data_table.Rows(0).Item("STATUS")
    Item.validation_type = IIf(_data_table.Rows(0).Item("TYPE") Is DBNull.Value, False, _data_table.Rows(0).Item("TYPE"))
    Item.allow_cashable = IIf(_data_table.Rows(0).Item("CASHABLE") Is DBNull.Value, False, _data_table.Rows(0).Item("CASHABLE"))
    Item.allow_promotion_tickets = IIf(_data_table.Rows(0).Item("PROMOTION") Is DBNull.Value, False, _data_table.Rows(0).Item("PROMOTION"))
    Item.allow_ticket_creation = IIf(_data_table.Rows(0).Item("EMISSION") Is DBNull.Value, False, _data_table.Rows(0).Item("EMISSION"))
    Item.allow_bills = IIf(_data_table.Rows(0).Item("BILLS") Is DBNull.Value, False, _data_table.Rows(0).Item("BILLS"))


    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

  End Function ' ReadGift
  '----------------------------------------------------------------------------
  ' PURPOSE : Updates the given terminal configuration object into the DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - Item
  '         - Context
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - CONFIGURATION_OK
  '     - CONFIGURATION_ERROR_DB
  '
  ' NOTES :

  Private Function UpdateTerminalConfiguration(ByVal Item As TYPE_TERMINAL_CONFIG, _
                              ByVal Context As Integer) As Integer
    Dim _sql_query As String
    Dim _sql_tx As System.Data.SqlClient.SqlTransaction = Nothing
    Dim _sql_command As SqlCommand
    Dim _rc As Integer

    ' Steps
    '     - Check whether there is no other gift with the same name 
    '     - Update the gift's data 

    _rc = 0

    Try
     
      '   - Update the gift's data 
      If Not GUI_BeginSQLTransaction(_sql_tx) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      _sql_query = "UPDATE   TERMINALS" _
                   + " SET   TE_ALLOWED_CASHABLE_REDEMPTION    = @pCashable " _
                      + "  , TE_ALLOWED_PROMO_REDEMPTION       = @pPromotion" _
                      + "  , TE_ALLOWED_EMISSION               = @pEmission" _
                      + "  , TE_ALLOWED_BILLS                  = @pBills" _
                 + " WHERE   TE_TERMINAL_ID                    = @pTerminalId "

      _sql_command = New SqlCommand(_sql_query, _sql_tx.Connection, _sql_tx)
      _sql_command.Parameters.Add("@pCashable", SqlDbType.Bit).Value = Item.allow_cashable
      _sql_command.Parameters.Add("@pPromotion", SqlDbType.Bit).Value = Item.allow_promotion_tickets
      _sql_command.Parameters.Add("@pEmission", SqlDbType.Bit).Value = Item.allow_ticket_creation
      _sql_command.Parameters.Add("@pBills", SqlDbType.Bit).Value = Item.allow_bills
      _sql_command.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = Item.terminal_id

      _rc = _sql_command.ExecuteNonQuery()

      If _rc <> 1 Then
        ' Rollback  
        GUI_EndSQLTransaction(_sql_tx, False)

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      ' Commit
      If Not GUI_EndSQLTransaction(_sql_tx, True) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

    Catch ex As Exception
      ' Rollback  
      GUI_EndSQLTransaction(_sql_tx, False)
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
  End Function ' UpdateGift
#End Region
End Class
