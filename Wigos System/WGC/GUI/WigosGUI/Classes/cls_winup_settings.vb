'-------------------------------------------------------------------
' Copyright � 2016 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : cls_winup_settings.vb
'
' DESCRIPTION : settings winup class
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 26-OCT-2016  SDS    Initial version
' 02-DIC-2016  RDS    Added Images settings
' 16-ENE-2017  PDM    Task 22704:GUI // Secciones - Verificar auditor�a
'--------------------------------------------------------------------
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports WSI.Common
Imports Newtonsoft.Json.Linq
Imports System.IO
Imports Newtonsoft.Json
Imports System.Text

Public Class CLASS_WINUP_SETTINGS
  Inherits CLASS_BASE

#Region " Constants "

  Public Const GP_GROUP As String = "WinUP"
  Public Const GP_SUBJECT_KEY_ABOUT As String = "PlayersClub.About"
  Public Const GP_SUBJECT_KEY_JOIN As String = "PlayersClub.Join"
  Public Const GP_SUBJECT_KEY_LINK As String = "PlayersClub.Link"
  Public Const GP_SUBJECT_KEY_TERMS As String = "PlayersClub.Terms"
  Public Const GP_SUBJECT_KEY_SUBJECT As String = "Feedback.Subject"
  Public Const GP_SUBJECT_KEY_MESSAGE As String = "Feedback.Message"
  Public Const GP_SUBJECT_KEY_HEIGHT As String = "Images.Height"
  Public Const GP_SUBJECT_KEY_WIDTH As String = "Images.Width"
  Public Const GP_SUBJECT_KEY_WEIGHT As String = "Images.Weight"
  Public Const GP_SUBJECT_KEY_WEIGHT_UNIT As String = "Images.WeightUnit"
  Public Const GP_SUBJECT_KEY_TIMEOUTCODE As String = "AuthorizationCode.Timeout"

  Public Const IMAGE_WEIGHT_UNIT_KB As String = "KB"
  Public Const IMAGE_WEIGHT_UNIT_MB As String = "MB"

  Public Const GP_SUBJECT_KEY_ENABLED As String = "Mailing.Enabled"
  Public Const GP_SUBJECT_KEY_SERVERADDRESS As String = "Mailing.ServerAddress"
  Public Const GP_SUBJECT_KEY_SERVERPORT As String = "Mailing.ServerPort"
  Public Const GP_SUBJECT_KEY_DOMAIN As String = "Mailing.Domain"
  Public Const GP_SUBJECT_KEY_EMAILADDRESS As String = "Mailing.EmailAddress"
  Public Const GP_SUBJECT_KEY_USERNAME As String = "Mailing.Username"
  Public Const GP_SUBJECT_KEY_PASSWORD As String = "Mailing.Password"
  Public Const GP_SUBJECT_KEY_SECURED As String = "Mailing.Secured"
#End Region
  Public Class TYPE_PLAYERS_CLUB
    Public about As String
    Public join As String
    Public link As String
    Public terms As String
  End Class
  Public Class TYPE_FEEDBACK
    Public subject As String
    Public message As String
  End Class
  Public Class TYPE_IMAGES
    Public height As String
    Public width As String
    Public weight As String
    Public weight_unit As String
  End Class

  'SDS 09-NOV-2016
  Public Class TYPE_FOOTER
    Implements IComparable(Of TYPE_FOOTER)

    Public sectionSchemaId As Long
    Public footerOrder As Integer
    Public footerImageId As Long
    Public footerImage As Image
    Public footerActive As Boolean
    Public footerStatus As Integer '0:nada,1:cambio
    Public footerName As String

    Public Function CompareTo(ByVal other As TYPE_FOOTER) As Integer Implements System.IComparable(Of TYPE_FOOTER).CompareTo
      If footerOrder = other.footerOrder Then
        Return 0
      Else
        If footerOrder < other.footerOrder Then
          Return -1
        Else
          Return 1
        End If
      End If
    End Function

  End Class

  Public Class TYPE_LOGIN
    Public timeoutCode As Integer
  End Class

  Public FooterList As New List(Of TYPE_FOOTER)

  Public Class TYPE_MAILING
    Public enabled As Boolean
    Public serverAddress As String
    Public serverPort As String
    Public domain As String
    Public emailAddress As String
    Public userName As String
    Public password As String
    Public secured As Boolean
  End Class
#Region " Members "

  Protected m_type_players_club As New TYPE_PLAYERS_CLUB
  Protected m_type_feedback As New TYPE_FEEDBACK
  Protected m_type_images As New TYPE_IMAGES
  Protected m_type_footer As New TYPE_FOOTER
  Protected m_type_login As New TYPE_LOGIN
  Protected m_type_mailing As New TYPE_MAILING

#End Region 'Members

#Region "Properties"

  Public Property About() As String
    Get
      Return m_type_players_club.about
    End Get

    Set(ByVal Value As String)
      m_type_players_club.about = Value
    End Set

  End Property ' About

  Public Property Join() As String
    Get
      Return m_type_players_club.join
    End Get

    Set(ByVal Value As String)
      m_type_players_club.join = Value
    End Set

  End Property ' Join
  Public Property Link() As String
    Get
      Return m_type_players_club.link
    End Get

    Set(ByVal Value As String)
      m_type_players_club.link = Value
    End Set

  End Property ' Link
  Public Property Terms() As String
    Get
      Return m_type_players_club.terms
    End Get

    Set(ByVal Value As String)
      m_type_players_club.terms = Value
    End Set

  End Property ' Terms

  Public Property Subject() As String
    Get
      Return m_type_feedback.subject
    End Get

    Set(ByVal Value As String)
      m_type_feedback.subject = Value
    End Set

  End Property ' Subject
  Public Property Message() As String
    Get
      Return m_type_feedback.message
    End Get

    Set(ByVal Value As String)
      m_type_feedback.message = Value
    End Set

  End Property ' Message
  Public Property Height() As String
    Get
      Return m_type_images.height
    End Get

    Set(ByVal Value As String)
      m_type_images.height = Value
    End Set

  End Property ' Height
  Public Property Width() As String
    Get
      Return m_type_images.width
    End Get

    Set(ByVal Value As String)
      m_type_images.width = Value
    End Set

  End Property ' Width
  Public Property Weight() As String
    Get
      Return m_type_images.weight
    End Get

    Set(ByVal Value As String)
      m_type_images.weight = Value
    End Set

  End Property ' Weight
  Public Property Weight_Unit() As String
    Get
      Return m_type_images.weight_unit
    End Get

    Set(ByVal Value As String)
      m_type_images.weight_unit = Value
    End Set

  End Property ' Weight_Unit
  'SDS 09-NOV-2016
  Public Property SectionSchemaId() As Long
    Get
      Return m_type_footer.sectionSchemaId
    End Get

    Set(ByVal Value As Long)
      m_type_footer.sectionSchemaId = Value
    End Set

  End Property ' SectionSchemaId

  Public Property FooterOrder() As Integer
    Get
      Return m_type_footer.footerOrder
    End Get

    Set(ByVal Value As Integer)
      m_type_footer.footerOrder = Value
    End Set

  End Property ' FooterOrder
  Public Property FooterStatus() As Integer
    Get
      Return m_type_footer.footerStatus
    End Get

    Set(ByVal Value As Integer)
      m_type_footer.footerStatus = Value
    End Set

  End Property ' footerStatus
  Public Property FooterImage() As Image
    Get
      Return m_type_footer.footerImage
    End Get

    Set(ByVal Value As Image)
      m_type_footer.footerImage = Value
    End Set

  End Property ' FooterImage
  Public Property FooterImageId() As Long
    Get
      Return m_type_footer.footerImageId
    End Get

    Set(ByVal Value As Long)
      m_type_footer.footerImageId = Value
    End Set

  End Property ' FooterImageId
  Public Property FooterActive() As Boolean
    Get
      Return m_type_footer.footerActive
    End Get

    Set(ByVal Value As Boolean)
      m_type_footer.footerActive = Value
    End Set

  End Property ' FooterActive

  Public Property FooterName() As String
    Get
      Return m_type_footer.footerName
    End Get

    Set(ByVal Value As String)
      m_type_footer.footerName = Value
    End Set

  End Property ' footerName

  Public Property TimeoutCode() As Integer
    Get
      Return m_type_login.timeoutCode
    End Get

    Set(ByVal Value As Integer)
      m_type_login.timeoutCode = Value
    End Set

  End Property ' timeoutCode

  Public Property Enabled() As Boolean
    Get
      Return m_type_mailing.enabled
    End Get

    Set(ByVal Value As Boolean)
      m_type_mailing.enabled = Value
    End Set

  End Property ' ServerAddress
  Public Property ServerAddress() As String
    Get
      Return m_type_mailing.serverAddress
    End Get

    Set(ByVal Value As String)
      m_type_mailing.serverAddress = Value
    End Set

  End Property ' ServerAddress
  Public Property ServerPort() As String
    Get
      Return m_type_mailing.serverPort
    End Get

    Set(ByVal Value As String)
      m_type_mailing.serverPort = Value
    End Set

  End Property ' ServerAddress
  Public Property Domain() As String
    Get
      Return m_type_mailing.domain
    End Get

    Set(ByVal Value As String)
      m_type_mailing.domain = Value
    End Set

  End Property ' ServerAddress
  Public Property EmailAddress() As String
    Get
      Return m_type_mailing.emailAddress
    End Get

    Set(ByVal Value As String)
      m_type_mailing.emailAddress = Value
    End Set

  End Property ' ServerAddress
  Public Property UserName() As String
    Get
      Return m_type_mailing.userName
    End Get

    Set(ByVal Value As String)
      m_type_mailing.userName = Value
    End Set

  End Property ' ServerAddress
  Public Property Password() As String
    Get
      Return m_type_mailing.password
    End Get

    Set(ByVal Value As String)
      m_type_mailing.password = Value
    End Set

  End Property ' ServerAddress
  Public Property Secured() As Boolean
    Get
      Return m_type_mailing.secured
    End Get

    Set(ByVal Value As Boolean)
      m_type_mailing.secured = Value
    End Set

  End Property ' ServerAddress
#End Region 'Properties

#Region "Overrides"

  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim auditor_data As CLASS_AUDITOR_DATA

    auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_GENERAL_PARAMS)

    auditor_data.SetName(GLB_NLS_GUI_CONFIGURATION.Id(236), GLB_NLS_GUI_PLAYER_TRACKING.GetString(7694))

    Call auditor_data.SetField(0, m_type_players_club.about, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7697))
    Call auditor_data.SetField(0, m_type_players_club.join, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7698))
    Call auditor_data.SetField(0, m_type_players_club.link, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7699))
    Call auditor_data.SetField(0, m_type_players_club.terms, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7700))
    Call auditor_data.SetField(0, m_type_feedback.subject, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7746))
    Call auditor_data.SetField(0, m_type_feedback.message, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7747))

    'SDS 09-NOV-2016
    For Each footer As TYPE_FOOTER In GetAllFooters()
      Call auditor_data.SetField(0, footer.footerActive, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7752))
      Call auditor_data.SetField(0, footer.footerOrder, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7751))
      Call auditor_data.SetField(0, footer.sectionSchemaId, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7750))
      Call auditor_data.SetField(0, footer.footerStatus, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7749) + footer.footerOrder.ToString())
    Next

    Call auditor_data.SetField(0, m_type_images.height, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7799))
    Call auditor_data.SetField(0, m_type_images.width, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7800))
    Call auditor_data.SetField(0, m_type_images.weight, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7801))

    Call auditor_data.SetField(0, m_type_login.timeoutCode, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7778))

    'SDS 29-NOV-2016 Mailing settings
    Call auditor_data.SetField(0, m_type_mailing.enabled, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7782))
    Call auditor_data.SetField(0, m_type_mailing.serverAddress, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7783))
    Call auditor_data.SetField(0, m_type_mailing.serverPort, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7784))
    Call auditor_data.SetField(0, m_type_mailing.domain, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7789))
    Call auditor_data.SetField(0, m_type_mailing.emailAddress, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7785))
    Call auditor_data.SetField(0, m_type_mailing.userName, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7786))
    Call auditor_data.SetField(0, m_type_mailing.password, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7787))
    Call auditor_data.SetField(0, m_type_mailing.secured, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7793))

    Return auditor_data

  End Function ' AuditorData

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
  End Function ' DB_Delete

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
  End Function ' DB_Insert

  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    ' Read Jackpot parameters information
    Return Winup_DbRead()



  End Function ' DB_Read

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim rc As Integer

    rc = Winup_DbUpdate()


    Select Case rc
      Case ENUM_DB_RC.DB_RC_OK
        Return ENUM_STATUS.STATUS_OK

      Case ENUM_DB_RC.DB_RC_ERROR_NOT_FOUND
        Return CLASS_BASE.ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Update

  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim temp_winup_settings As CLASS_WINUP_SETTINGS

    temp_winup_settings = New CLASS_WINUP_SETTINGS

    temp_winup_settings.About = Me.About
    temp_winup_settings.Link = Me.Link
    temp_winup_settings.Join = Me.Join
    temp_winup_settings.Terms = Me.Terms

    temp_winup_settings.Subject = Me.Subject
    temp_winup_settings.Message = Me.Message

    temp_winup_settings.Height = Me.Height
    temp_winup_settings.Weight = Me.Weight
    temp_winup_settings.Weight_Unit = Me.Weight_Unit
    temp_winup_settings.Width = Me.Width

    temp_winup_settings.TimeoutCode = Me.TimeoutCode

    temp_winup_settings.Enabled = Me.Enabled
    temp_winup_settings.ServerAddress = Me.ServerAddress
    temp_winup_settings.ServerPort = Me.ServerPort
    temp_winup_settings.EmailAddress = Me.EmailAddress
    temp_winup_settings.Domain = Me.Domain
    temp_winup_settings.UserName = Me.UserName
    temp_winup_settings.Password = Me.Password
    temp_winup_settings.Secured = Me.Secured

    temp_winup_settings.FooterList = Me.FooterList

    Return temp_winup_settings
  End Function ' Duplicate

#End Region ' Overrides

#Region "Private Functions"

  ''' <summary>
  ''' Return all footers by order
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Function GetAllFooters() As IEnumerable(Of TYPE_FOOTER)
    ' Concat
    Dim _footers As List(Of TYPE_FOOTER) = FooterList

    _footers.Sort()

    Return _footers

  End Function
  ''GetAllFooters

  Private Function Winup_DbRead() As Integer
    Try

      Me.About = GeneralParam.GetString(GP_GROUP, GP_SUBJECT_KEY_ABOUT)
      Me.Join = GeneralParam.GetString(GP_GROUP, GP_SUBJECT_KEY_JOIN)
      Me.Link = GeneralParam.GetString(GP_GROUP, GP_SUBJECT_KEY_LINK)
      Me.Terms = GeneralParam.GetString(GP_GROUP, GP_SUBJECT_KEY_TERMS)

      Me.Subject = GeneralParam.GetString(GP_GROUP, GP_SUBJECT_KEY_SUBJECT)
      Me.Message = GeneralParam.GetString(GP_GROUP, GP_SUBJECT_KEY_MESSAGE)

      Me.Height = GeneralParam.GetString(GP_GROUP, GP_SUBJECT_KEY_HEIGHT)
      Me.Width = GeneralParam.GetString(GP_GROUP, GP_SUBJECT_KEY_WIDTH)
      Me.Weight = GeneralParam.GetString(GP_GROUP, GP_SUBJECT_KEY_WEIGHT)
      Me.Weight_Unit = GeneralParam.GetString(GP_GROUP, GP_SUBJECT_KEY_WEIGHT_UNIT)

      Me.TimeoutCode = GeneralParam.GetString(GP_GROUP, GP_SUBJECT_KEY_TIMEOUTCODE)

      Me.Enabled = GeneralParam.GetString(GP_GROUP, GP_SUBJECT_KEY_ENABLED)
      Me.ServerAddress = GeneralParam.GetString(GP_GROUP, GP_SUBJECT_KEY_SERVERADDRESS)
      Me.ServerPort = GeneralParam.GetString(GP_GROUP, GP_SUBJECT_KEY_SERVERPORT)
      Me.EmailAddress = GeneralParam.GetString(GP_GROUP, GP_SUBJECT_KEY_EMAILADDRESS)
      Me.Domain = GeneralParam.GetString(GP_GROUP, GP_SUBJECT_KEY_DOMAIN)
      Me.UserName = GeneralParam.GetString(GP_GROUP, GP_SUBJECT_KEY_USERNAME)
      Me.Password = GeneralParam.GetString(GP_GROUP, GP_SUBJECT_KEY_PASSWORD)
      Me.Secured = GeneralParam.GetString(GP_GROUP, GP_SUBJECT_KEY_SECURED)

      Call ReadFooter()
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

    Catch _ex As Exception
      Return ENUM_STATUS.STATUS_ERROR

    End Try

  End Function ' Winup_DbRead

  Private Function Winup_DbUpdate() As Integer

    Dim commit_trx As Boolean
    Dim result As Integer

    Try
      Using _db_trx As New DB_TRX()

        If Not GUI_BeginSQLTransaction(_db_trx.SqlTransaction) Then
          Exit Function
        End If

        commit_trx = False
        result = ENUM_CONFIGURATION_RC.CONFIGURATION_OK

        GUI_Controls.DB_GeneralParam_Update(GP_GROUP, GP_SUBJECT_KEY_ABOUT, Me.About, _db_trx.SqlTransaction)
        GUI_Controls.DB_GeneralParam_Update(GP_GROUP, GP_SUBJECT_KEY_JOIN, Me.Join, _db_trx.SqlTransaction)
        GUI_Controls.DB_GeneralParam_Update(GP_GROUP, GP_SUBJECT_KEY_LINK, Me.Link, _db_trx.SqlTransaction)
        GUI_Controls.DB_GeneralParam_Update(GP_GROUP, GP_SUBJECT_KEY_TERMS, Me.Terms, _db_trx.SqlTransaction)

        GUI_Controls.DB_GeneralParam_Update(GP_GROUP, GP_SUBJECT_KEY_SUBJECT, Me.Subject, _db_trx.SqlTransaction)
        GUI_Controls.DB_GeneralParam_Update(GP_GROUP, GP_SUBJECT_KEY_MESSAGE, Me.Message, _db_trx.SqlTransaction)

        GUI_Controls.DB_GeneralParam_Update(GP_GROUP, GP_SUBJECT_KEY_HEIGHT, Me.Height, _db_trx.SqlTransaction)
        GUI_Controls.DB_GeneralParam_Update(GP_GROUP, GP_SUBJECT_KEY_WIDTH, Me.Width, _db_trx.SqlTransaction)
        GUI_Controls.DB_GeneralParam_Update(GP_GROUP, GP_SUBJECT_KEY_WEIGHT, Me.Weight, _db_trx.SqlTransaction)
        GUI_Controls.DB_GeneralParam_Update(GP_GROUP, GP_SUBJECT_KEY_WEIGHT_UNIT, Me.Weight_Unit, _db_trx.SqlTransaction)


        GUI_Controls.DB_GeneralParam_Update(GP_GROUP, GP_SUBJECT_KEY_TIMEOUTCODE, Me.TimeoutCode, _db_trx.SqlTransaction)


        GUI_Controls.DB_GeneralParam_Update(GP_GROUP, GP_SUBJECT_KEY_ENABLED, Me.Enabled, _db_trx.SqlTransaction)
        GUI_Controls.DB_GeneralParam_Update(GP_GROUP, GP_SUBJECT_KEY_SERVERADDRESS, Me.ServerAddress, _db_trx.SqlTransaction)
        GUI_Controls.DB_GeneralParam_Update(GP_GROUP, GP_SUBJECT_KEY_SERVERPORT, Me.ServerPort, _db_trx.SqlTransaction)
        GUI_Controls.DB_GeneralParam_Update(GP_GROUP, GP_SUBJECT_KEY_EMAILADDRESS, Me.EmailAddress, _db_trx.SqlTransaction)
        GUI_Controls.DB_GeneralParam_Update(GP_GROUP, GP_SUBJECT_KEY_DOMAIN, Me.Domain, _db_trx.SqlTransaction)
        GUI_Controls.DB_GeneralParam_Update(GP_GROUP, GP_SUBJECT_KEY_USERNAME, Me.UserName, _db_trx.SqlTransaction)
        GUI_Controls.DB_GeneralParam_Update(GP_GROUP, GP_SUBJECT_KEY_PASSWORD, Me.Password, _db_trx.SqlTransaction)
        GUI_Controls.DB_GeneralParam_Update(GP_GROUP, GP_SUBJECT_KEY_SECURED, Me.Secured, _db_trx.SqlTransaction)


        UpdateFooter()
        _db_trx.Commit()

      End Using

    Catch ex As Exception
      ' Do nothing
      Log.Error("Exception on Winup_DbUpdate(): " & ex.Message)
      result = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

    Return result

  End Function ' Winup_DbUpdate




  Private Sub ReadFooter()
    Dim _response As String
    Dim _api As CLS_API
    Dim _url, _responseSanitized As String
    Dim _jarray As JArray
    Dim _tbl As DataTable
    Dim _items() As String = {"backgroundImage", "iconImage", "parent", "childs"}
    ' _url = "Http://localhost:20397/api/" + "/sectionsschema/getFooter/"
    _url = WSI.Common.GeneralParam.GetString("WinUP", "Backend.Url") + "/sectionsschema/getFooter/"
    _api = New CLS_API(_url)
    _response = _api.getData()

    _responseSanitized = _api.sanitizeData(_response)
    _jarray = _api.removeItems(JArray.Parse(_responseSanitized), _items)
    _tbl = JsonConvert.DeserializeObject(Of DataTable)(_jarray.ToString())

    For Each row As DataRow In _tbl.Rows
      Dim footer As New TYPE_FOOTER
      footer.footerStatus = 0 'from DB
      If row("sectionSchemaId").ToString() <> "" Then
        footer.sectionSchemaId = row("sectionSchemaId")
      End If
      If row("footerImageId").ToString() <> "" Then
        footer.footerImageId = row("footerImageId")
      End If
      If row("footerActive").ToString() <> "" Then
        footer.footerActive = row("footerActive")
      End If
      If row("footerOrder").ToString() <> "" Then
        footer.footerOrder = row("footerOrder")
      End If

      If row("name").ToString() <> "" Then
        footer.footerName = row("name")
      End If

      If row("footerImageId").ToString() <> "" Then
        Dim _imageTmpResponse As JObject
        _url = WSI.Common.GeneralParam.GetString("WinUP", "Backend.Url") + "/image/getbyid/" + row("footerImageId").ToString()
        _api = New CLS_API(_url)
        _imageTmpResponse = JObject.Parse(_api.sanitizeData(_api.getData()))
        Dim imgData As Byte() = _imageTmpResponse.Item("imageData")
        If Not imgData Is Nothing Then
          Using ms As New MemoryStream(imgData)
            footer.footerImage = System.Drawing.Image.FromStream(ms)
          End Using
        Else
          footer.footerImage = Nothing
        End If

      End If
      FooterList.Add(footer)
    Next
  End Sub 'ReadFooter

  Private Sub UpdateFooter()
    Dim _url As String
    Dim _api As CLS_API
    Dim response As String
    Dim _jsonFooterList As JArray
    Dim _jObj As New JObject


    _jsonFooterList = getJSONFooterList(FooterList)

    _url = WSI.Common.GeneralParam.GetString("WinUP", "Backend.Url") + "/sectionsschema/updatefooter/" '+ footer.sectionSchemaId.ToString()
    _api = New CLS_API(_url)
    response = _api.putData(Encoding.UTF8.GetBytes(_jsonFooterList.ToString()))

  End Sub

  '----------------------------------------------------------------------------
  ' PURPOSE : Return the image type extension of the image 
  '
  ' PARAMS :
  '     - INPUT :
  '         None
  '
  '     - OUTPUT :
  '         String
  '
  ' RETURNS :
  '     - string
  '
  ' NOTES :
  Private Function GetImageType(img As Image) As String
    Dim _result As String

    _result = ""
    If (img.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Jpeg)) Then
      _result = ".jpg"
    End If
    If (img.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Bmp)) Then
      _result = ".bmp"
    End If
    If (img.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Png)) Then
      _result = ".png"
    End If
    If (img.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Gif)) Then
      _result = ".gif"
    End If

    Return _result
  End Function
  Private Function getJSONFooterList(foo As List(Of TYPE_FOOTER)) As JArray
    Dim _jObj As New JArray

    For Each footer As TYPE_FOOTER In FooterList
      Dim JsonFooter = ToJSONObject(footer)
      _jObj.Add(JsonFooter)
    Next
    Return _jObj
  End Function
  Private Function ToJSONObject(foo As TYPE_FOOTER) As JObject
    Dim _jObj As New JObject
    _jObj.Add("sectionSchemaId", foo.sectionSchemaId)
    _jObj.Add("footerActive", foo.footerActive)
    _jObj.Add("footerOrder", foo.footerOrder)

    If Not foo.footerImageId > 0 Then
      Dim imgFooterData As Byte()
      If Not IsNothing(foo.footerImage) Then
        Dim img As New ImageConverter()
        Dim _url As String
        imgFooterData = img.ConvertTo(foo.footerImage, GetType(Byte()))

        Dim jObjBackground As New JObject
        jObjBackground.Add("Name", "FooterIcon")
        jObjBackground.Add("fileName", "FooterIcon" + foo.sectionSchemaId.ToString() + GetImageType(foo.footerImage))
        '        jObjBackground.Add("ContentType", "image/jpeg")
        'jObjBackground.Add("ContentType", "image/png")

        jObjBackground.Add("Data", imgFooterData)

        _url = WSI.Common.GeneralParam.GetString("WinUP", "Backend.Url") + "/image/SaveImage"
        '_url = "Http://localhost:20397/api/" + "/image/SaveImage"
        Dim _api = New CLS_API(_url)

        Dim response As String = _api.postData(Encoding.UTF8.GetBytes(jObjBackground.ToString()))
        Dim jresponse As JObject = JObject.Parse(response)
        _jObj.Add("footerImageId", jresponse.Item("imageId"))

      End If
    Else
      _jObj.Add("footerImageId", foo.footerImageId)
    End If

    Return _jObj
  End Function
  Private Function ToJSON(foo As TYPE_FOOTER) As String
    Dim _jObj As New JObject
    _jObj.Add("sectionSchemaId", foo.sectionSchemaId)
    _jObj.Add("footerActive", foo.footerActive)
    _jObj.Add("footerOrder", foo.footerOrder)

    If Not foo.footerImageId > 0 Then
      Dim imgFooterData As Byte()
      If Not IsNothing(foo.footerImage) Then
        Dim img As New ImageConverter()
        Dim _url As String
        imgFooterData = img.ConvertTo(foo.footerImage, GetType(Byte()))

        Dim jObjBackground As New JObject
        jObjBackground.Add("Name", "FooterIcon")
        jObjBackground.Add("fileName", "FooterIcon" + foo.sectionSchemaId.ToString() + GetImageType(foo.footerImage))
        '        jObjBackground.Add("ContentType", "image/jpeg")
        'jObjBackground.Add("ContentType", "image/png")

        jObjBackground.Add("Data", imgFooterData)

        '_url = WSI.Common.GeneralParam.GetString("WinUP", "Backend.Url") + "/image/SaveImage"
        _url = "Http://localhost:20397/api/" + "/image/SaveImage"
        Dim _api = New CLS_API(_url)

        Dim response As String = _api.postData(Encoding.UTF8.GetBytes(jObjBackground.ToString()))
        Dim jresponse As JObject = JObject.Parse(response)
        _jObj.Add("footerImageId", jresponse.Item("imageId"))

      End If
    Else
      _jObj.Add("footerImageId", foo.footerImageId)
    End If

    Return _jObj.ToString()
  End Function
#End Region

#Region "Public Functions"
  Public Function GetImageMaxSize() As Size
    Dim _height As Integer
    Dim _width As Integer
    _height = 1921
    _width = 1081

    If WSI.Common.GeneralParam.GetBoolean(GP_GROUP, GP_SUBJECT_KEY_HEIGHT, False) Then
      _height = Convert.ToInt32(GeneralParam.GetString(GP_GROUP, GP_SUBJECT_KEY_HEIGHT))
    End If

    If WSI.Common.GeneralParam.GetBoolean(GP_GROUP, GP_SUBJECT_KEY_WIDTH, False) Then
      _width = Convert.ToInt32(GeneralParam.GetString(GP_GROUP, GP_SUBJECT_KEY_WIDTH))
    End If

    Return New Size(_width, _height)

  End Function

  Public Function GetImageMaxWeightKB() As Integer
    Dim _weight As Integer
    Dim _weight_unit As String
    _weight = 2048
    _weight_unit = "KB"

    If WSI.Common.GeneralParam.GetBoolean(GP_GROUP, GP_SUBJECT_KEY_WEIGHT, False) Then
      _weight = Convert.ToInt32(GeneralParam.GetString(GP_GROUP, GP_SUBJECT_KEY_WEIGHT))

      If WSI.Common.GeneralParam.GetBoolean(GP_GROUP, GP_SUBJECT_KEY_WEIGHT_UNIT, False) Then
        _weight_unit = GeneralParam.GetString(GP_GROUP, GP_SUBJECT_KEY_WEIGHT_UNIT)
        If _weight_unit = IMAGE_WEIGHT_UNIT_MB Then
          _weight = _weight * 1024
        End If
      End If

    End If

    Return _weight

  End Function
#End Region
End Class
