'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   CLASS_ACCOUNT_BLOCK
' DESCRIPTION:   
' AUTHOR:        Javi Barea
' CREATION DATE: 08-MAY-2013
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 08-MAY-2013  JBP    Initial version
'--------------------------------------------------------------------

Imports GUI_Controls
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports WSI.Common
Imports System.Data.SqlClient
Imports System.Text

Public Class CLASS_ACCOUNT_BLOCK
  Inherits CLASS_BASE

#Region " Constants "

  Private Const AC_BLOCKED_ID = 0
  Private Const AC_BLOCK_REASON_ID = 1
  Private Const AC_BLOCK_DESCRIPTION_ID = 2
  Private Const AC_HOLDER_NAME_ID = 3

#End Region

#Region " Members "

  Dim m_card_data As CardData

#End Region ' Members

#Region " Properties "

  Public Property CardData() As CardData
    Get
      Return Me.m_card_data
    End Get
    Set(ByVal value As CardData)
      Me.m_card_data = value
    End Set
  End Property

#End Region ' Properties

#Region " Publics "

  Public Sub New(ByVal AccountId As Integer)

    m_card_data = New CardData()

    Me.DB_Read(AccountId, 0)

  End Sub
  ' PURPOSE: Block account data base implementation
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Public Function BlockUnblockAccount(ByVal Description) As Boolean

    Dim _session_info As CashierSessionInfo

    Try

      Using _db_trx As New DB_TRX()

        m_card_data.Blocked = Not m_card_data.Blocked
        m_card_data.BlockReason = IIf(m_card_data.Blocked, AccountBlockReason.FROM_GUI, AccountBlockReason.NONE)
        m_card_data.BlockDescription = Description

        ' It's not a real cashier session. Don't open a cashier session for this movement.
        _session_info = New CashierSessionInfo()
        _session_info.AuthorizedByUserName = CurrentUser.Name
        _session_info.TerminalId = 0
        _session_info.TerminalName = Environment.MachineName
        _session_info.CashierSessionId = 0

        If Not CardData.DB_BlockUnblockCard(m_card_data, _session_info, _db_trx.SqlTransaction) Then

          Return False

        End If

        If AuditorData() Is Nothing Then

          Return False

        End If

        _db_trx.Commit()

        Return True

      End Using

    Catch ex As Exception
      Log.Exception(ex)
    End Try

    Return False

  End Function ' BlockUnblockAccount ()

#End Region ' Publics

#Region " Privates "


#End Region ' Privates

#Region " Overrides "

  '----------------------------------------------------------------------------
  ' PURPOSE: Returns the related auditor data for the current object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  Public Overrides Function AuditorData() As CLASS_AUDITOR_DATA

    Dim _auditor_data As CLASS_AUDITOR_DATA

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_ACCOUNT)

    ' Audit data
    '     - Account
    _auditor_data.SetName(GLB_NLS_GUI_STATISTICS.Id(207), _
                         GLB_NLS_GUI_STATISTICS.GetString(207) & " " & IIf(m_card_data.Blocked, _
                         GLB_NLS_GUI_INVOICING.GetString(238), GLB_NLS_GUI_INVOICING.GetString(239)) & _
                         ": " & m_card_data.AccountId.ToString() & " - " & m_card_data.PlayerTracking.HolderName)

    If Not _auditor_data.Notify(GLB_CurrentUser.GuiId, _
                               GLB_CurrentUser.Id, _
                               GLB_CurrentUser.Name, _
                               CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.GENERIC, _
                               0) Then
      Return Nothing

    End If

    Return _auditor_data

  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE: Returns a duplicate of the given object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Return New CLASS_PLAYER_DATA()

  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE: Deletes the given object from the database.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return ENUM_STATUS.STATUS_ERROR
  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE: Inserts the given object to the database.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return ENUM_STATUS.STATUS_ERROR
  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE: Reads from the database into the given object
  '
  ' PARAMS:
  '   - INPUT:
  '     - ObjectId: An object, it must be 'casted' to the desired KEY.
  '
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Read(ByVal AccountId As Object, ByRef SqlCtx As Integer) As ENUM_STATUS

    Dim _str_bld As StringBuilder

    _str_bld = New StringBuilder()

    Try

      _str_bld.AppendLine("SELECT   AC_BLOCKED                  ")
      _str_bld.AppendLine("       , AC_BLOCK_REASON             ")
      _str_bld.AppendLine("       , AC_BLOCK_DESCRIPTION        ")
      _str_bld.AppendLine("       , AC_HOLDER_NAME              ")
      _str_bld.AppendLine("  FROM   ACCOUNTS                    ")
      _str_bld.AppendLine(" WHERE   AC_ACCOUNT_ID = @pAccountId ")


      Using _db_trx As New DB_TRX()

        Using _cmd As SqlCommand = New SqlCommand(_str_bld.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _cmd.Parameters.Add("@pAccountId", SqlDbType.NVarChar).Value = AccountId

          Using _reader As SqlDataReader = _cmd.ExecuteReader()

            If Not _reader.HasRows() Then
              Return ENUM_STATUS.STATUS_NOT_FOUND
            End If

            If _reader.Read() Then

              Me.m_card_data.AccountId = AccountId
              Me.m_card_data.Blocked = _reader.GetBoolean(AC_BLOCKED_ID)
              Me.m_card_data.BlockReason = _reader.GetInt32(AC_BLOCK_REASON_ID)
              Me.m_card_data.BlockDescription = _reader.GetString(AC_BLOCK_DESCRIPTION_ID)
              Me.m_card_data.PlayerTracking.HolderName = _reader.GetString(AC_HOLDER_NAME_ID)

              _reader.Close()

            End If
            

          End Using '_reader

        End Using '_cmd

      End Using

    Catch
      Return ENUM_STATUS.STATUS_ERROR
    Finally

    End Try

    Return ENUM_STATUS.STATUS_OK

  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE: Updates the given object to the database.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS



    Return ENUM_STATUS.STATUS_OK

  End Function

#End Region ' Overrides

End Class
