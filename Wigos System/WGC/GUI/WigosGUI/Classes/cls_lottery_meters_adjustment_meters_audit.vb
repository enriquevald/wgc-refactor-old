﻿'-------------------------------------------------------------------
' Copyright © 2007 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_lottery_meters_adjustment_meters_audit.vb
' DESCRIPTION:   Class that return the audit changes for frm_lottery_meters_adjustment_meters
' AUTHOR:        Francis Gretz
' CREATION DATE: 10-JAN-2018
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 10-JAN-2018  FGB    Initial version.
Imports GUI_CommonOperations
Imports GUI_Controls
Imports WSI.Common.EGMMeterAdjustment

Public Class CLS_LOTTERY_METERS_ADJUSTMENT_METERS_AUDIT

#Region " Members "
  Private _terminal_name As String

  Public Property m_terminal_name() As String
    Get
      Return _terminal_name
    End Get

    Set(ByVal value As String)
      _terminal_name = value
    End Set
  End Property


  'Texts for audit
  Private ReadOnly m_text_yes As String
  Private ReadOnly m_text_no As String

  Private ReadOnly m_text_hour As String
  Private ReadOnly m_text_CI As String
  Private ReadOnly m_text_CO As String
  Private ReadOnly m_text_JP As String
  Private ReadOnly m_text_ignore As String
  Private ReadOnly m_text_cut_point As String
#End Region

#Region "Structures"
  Public Structure TYPE_AUDITING_ITEM
    Dim ItemDateTime As DateTime
    Dim ItemString As String
  End Structure
#End Region

#Region " Constructor "
  Public Sub New()
    'Get texts for messages
    m_text_yes = GLB_NLS_GUI_PLAYER_TRACKING.GetString(278)         'Yes
    m_text_no = GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)          'No

    m_text_hour = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8822)       'Hour
    m_text_CI = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8825)         'CI
    m_text_CO = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8826)         'CO
    m_text_JP = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8827)         'JP
    m_text_ignore = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8829)     'Ignore
    m_text_cut_point = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8830)  'Cut point
  End Sub
#End Region

#Region " Public Functions "
  Public Sub GenerateAuditChanges(ByVal DtChangedMeters As DataTable, ByVal TerminalName As String)
    Dim _auditor As CLASS_AUDITOR_DATA
    Dim _changes_list As List(Of TYPE_AUDITING_ITEM)

    m_terminal_name = TerminalName

    _auditor = New CLASS_AUDITOR_DATA(AUDIT_EGM_METERS)
    _changes_list = New List(Of TYPE_AUDITING_ITEM)

    'Get changes of each row
    For Each _row As DataRow In DtChangedMeters.Rows
      Select Case _row.RowState
        Case DataRowState.Modified, DataRowState.Deleted, DataRowState.Added
          GetAuditVersionValues(_auditor, _row, _changes_list)
      End Select
    Next

    'Are there changes?
    If (_changes_list.Count > 0) Then
      Dim _first_row As DataRow

      _first_row = DtChangedMeters.Rows(0)

      'Sort List
      _changes_list.Sort(Function(x, y) x.ItemDateTime.CompareTo(y.ItemDateTime))

      'Set common values
      GetAuditCommonValues(_auditor, _first_row, GetRowVersionForRowState(_first_row))

      'Set fields
      Call AddChangesToAuditorFields(_auditor, _changes_list)

      'Notify
      _auditor.Notify(GLB_CurrentUser.GuiId, GLB_CurrentUser.Id, GLB_CurrentUser.Name, _
                      CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.GENERIC, 0)
    End If

    _auditor = Nothing
  End Sub
#End Region

#Region " Private Functions "
  ''' <summary>
  ''' Add changes to auditor fields
  ''' </summary>
  ''' <param name="Auditor"></param>
  ''' <param name="ChangesList">Sorted list by date of row changes</param>
  ''' <remarks></remarks>
  Private Sub AddChangesToAuditorFields(Auditor As CLASS_AUDITOR_DATA, ChangesList As List(Of TYPE_AUDITING_ITEM))
    Dim _idx As Integer

    For _idx = 0 To (ChangesList.Count - 1)
      'Audit string with all row changes
      Call Auditor.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8973), ChangesList.Item(_idx).ItemString) '8973 - Meter changes
    Next _idx
  End Sub

  ''' <summary>
  ''' Get common values for audit
  ''' </summary>
  ''' <param name="Audit"></param>
  ''' <param name="Row"></param>
  ''' <param name="RowVersion"></param>
  ''' <remarks></remarks>
  Private Sub GetAuditCommonValues(ByRef Audit As CLASS_AUDITOR_DATA, Row As DataRow, RowVersion As DataRowVersion)
    Call Audit.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(8820), GLB_NLS_GUI_PLAYER_TRACKING.GetString(8820)) 'EGM Meters

    If (WSI.Common.Misc.IsCenter) Then
      Call Audit.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5658), Row(EGMMeterAdjustmentConstants.SQL_COLUMN_SITE_ID, RowVersion), "", CLASS_AUDITOR_DATA.ENUM_FIELD_TYPE.FIELD_ALWAYS_VISIBLE)     'Site Id
    End If

    Call Audit.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(268), Row(EGMMeterAdjustmentConstants.SQL_COLUMN_WORKING_DAY, RowVersion), "", CLASS_AUDITOR_DATA.ENUM_FIELD_TYPE.FIELD_ALWAYS_VISIBLE)    'Working day
    Call Audit.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1097), m_terminal_name, "", CLASS_AUDITOR_DATA.ENUM_FIELD_TYPE.FIELD_ALWAYS_VISIBLE)                                                       'Terminal name
    'Call Audit.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1097), Row(EGMMeterAdjustmentConstants.SQL_COLUMN_TERMINAL_ID, RowVersion), "", CLASS_AUDITOR_DATA.ENUM_FIELD_TYPE.FIELD_ALWAYS_VISIBLE)  'Terminal Id
    Call Audit.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2008), Row(EGMMeterAdjustmentConstants.SQL_COLUMN_GAME_ID, RowVersion), "", CLASS_AUDITOR_DATA.ENUM_FIELD_TYPE.FIELD_ALWAYS_VISIBLE)       'Game Id
    Call Audit.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2270), Row(EGMMeterAdjustmentConstants.SQL_COLUMN_DENOMINATION, RowVersion), "", CLASS_AUDITOR_DATA.ENUM_FIELD_TYPE.FIELD_ALWAYS_VISIBLE)  'Denomination
  End Sub

  ''' <summary>
  ''' Get all meters changes for audit
  ''' </summary>
  ''' <param name="Row"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetAllMetersChangesForAudit(Row As DataRow) As String
    Dim _str_change_CI As String
    Dim _str_change_CO As String
    Dim _str_change_JP As String
    Dim _str_change_Ignored As String

    _str_change_CI = GetAuditNumberValueChanges(Row, EGMMeterAdjustmentConstants.SQL_COLUMN_MC_0000)            'Coin In
    _str_change_CO = GetAuditNumberValueChanges(Row, EGMMeterAdjustmentConstants.SQL_COLUMN_MC_0001)            'Coin Out
    _str_change_JP = GetAuditNumberValueChanges(Row, EGMMeterAdjustmentConstants.SQL_COLUMN_MC_0002)            'Jackpot
    _str_change_Ignored = GetAuditBooleanValueChanges(Row, EGMMeterAdjustmentConstants.SQL_COLUMN_USER_IGNORED) 'Ignored?

    Dim _changes_list As List(Of String)
    _changes_list = New List(Of String)

    If (Not String.IsNullOrEmpty(_str_change_CI)) Then
      _changes_list.Add(m_text_CI + ": " + _str_change_CI)      'Coin In
    End If

    If (Not String.IsNullOrEmpty(_str_change_CO)) Then
      _changes_list.Add(m_text_CO + ": " + _str_change_CO)      'Coin Out
    End If

    If (Not String.IsNullOrEmpty(_str_change_JP)) Then
      _changes_list.Add(m_text_JP + ": " + _str_change_JP)      'Jackpot
    End If

    If (Not String.IsNullOrEmpty(_str_change_Ignored)) Then
      _changes_list.Add(m_text_ignore + ": " + _str_change_Ignored) 'Ignore
    End If

    Return String.Join("; ", _changes_list.ToArray())
  End Function

  ''' <summary>
  ''' Get version values from record for audit
  ''' </summary>
  ''' <param name="Audit"></param>
  ''' <param name="Row"></param>
  ''' <param name="ChangesList">SortedList(Of DateTime, String) 'DateTime, text</param>'
  ''' <remarks></remarks>
  Private Sub GetAuditVersionValues(ByRef Audit As CLASS_AUDITOR_DATA, Row As DataRow, ByRef ChangesList As List(Of TYPE_AUDITING_ITEM))
    Dim _item_audit As TYPE_AUDITING_ITEM

    If ((Row.RowState = DataRowState.Unchanged) Or ((Row.RowState = DataRowState.Detached))) Then
      Exit Sub
    End If

    'Get all changes
    Dim _str_audit_all_changes As String
    _str_audit_all_changes = GetAllMetersChangesForAudit(Row)

    'Audit if there are changes
    If (Not String.IsNullOrEmpty(_str_audit_all_changes)) Then
      Dim _hour As String = GetAuditHour(Row, EGMMeterAdjustmentConstants.SQL_COLUMN_DATETIME)
      Dim _datetime As DateTime = GetAuditDateTime(Row, EGMMeterAdjustmentConstants.SQL_COLUMN_DATETIME)

      'Add hour at the beginning
      _str_audit_all_changes = m_text_hour + ": " + _hour + " - " + _str_audit_all_changes

      'Is a cut-point?
      Dim _is_cut_point As Boolean
      _is_cut_point = GetRecordIsCutPoint(Row, EGMMeterAdjustmentConstants.SQL_COLUMN_RECORD_TYPE)

      If (_is_cut_point) Then
        _str_audit_all_changes = _str_audit_all_changes + "; " + m_text_cut_point + ": " + m_text_yes '8830 - Is a cut-point
      End If

      _item_audit.ItemDateTime = _datetime
      _item_audit.ItemString = _str_audit_all_changes

      ChangesList.Add(_item_audit)
    End If

  End Sub

  ''' <summary>
  ''' Get audit value for hour of changed row
  ''' </summary>
  ''' <param name="Row"></param>
  ''' <param name="ColumnIndex"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetAuditHour(Row As DataRow, ColumnIndex As Integer) As String
    Dim _row_version As DataRowVersion = GetRowVersionForRowState(Row)

    Return GetAuditHourValue(Row, ColumnIndex, _row_version)
  End Function

  ''' <summary>
  ''' Get audit value for hour of changed row
  ''' </summary>
  ''' <param name="Row"></param>
  ''' <param name="ColumnIndex"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetAuditDateTime(Row As DataRow, ColumnIndex As Integer) As DateTime
    Dim _row_version As DataRowVersion = GetRowVersionForRowState(Row)

    Return GetColumnAsDateTime(Row, ColumnIndex, _row_version)
  End Function

  ''' <summary>
  ''' Get orw version for a row state
  ''' </summary>
  ''' <param name="Row"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetRowVersionForRowState(Row As DataRow) As DataRowVersion
    Select Case Row.RowState
      Case DataRowState.Added
        Return DataRowVersion.Current

      Case DataRowState.Modified, DataRowState.Deleted
        Return DataRowVersion.Original

      Case Else
        Throw New Exception("GetRowVersionForRowState - RowState is not valid: " + Row.RowState)

    End Select
  End Function

  ''' <summary>
  ''' Get values (old and new) for audit
  ''' </summary>
  ''' <param name="Row"></param>
  ''' <param name="ColumnIndex"></param>
  ''' <remarks></remarks>
  Private Function GetAuditBooleanValueChanges(Row As DataRow, ColumnIndex As Integer) As String
    Dim _old_value As String
    Dim _new_value As String

    _old_value = GetAuditBooleanValue(Row, ColumnIndex, DataRowVersion.Original)
    _new_value = GetAuditBooleanValue(Row, ColumnIndex, DataRowVersion.Current)

    If String.Equals(_old_value, _new_value) Then
      Return String.Empty
    End If

    Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(1636, _new_value, _old_value)
  End Function

  ''' <summary>
  ''' Get values (old and new) for audit
  ''' </summary>
  ''' <param name="Row"></param>
  ''' <param name="ColumnIndex"></param>
  ''' <remarks></remarks>
  Private Function GetAuditNumberValueChanges(Row As DataRow, ColumnIndex As Integer) As String
    Dim _old_value As String
    Dim _new_value As String

    _old_value = GetAuditNumberValue(Row, ColumnIndex, DataRowVersion.Original)
    _new_value = GetAuditNumberValue(Row, ColumnIndex, DataRowVersion.Current)

    If String.Equals(_old_value, _new_value) Then
      Return String.Empty
    End If

    Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(1636, _new_value, _old_value)
  End Function

  ''' <summary>
  ''' Check if the value for a row version is valid
  ''' </summary>
  ''' <param name="Row"></param>
  ''' <param name="ColumnIndex"></param>
  ''' <param name="RowVersion"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function CheckValueForRowVersionIsValid(Row As DataRow, ColumnIndex As Integer, RowVersion As DataRowVersion) As Boolean
    Select Case Row.RowState
      Case DataRowState.Added
        If (RowVersion = DataRowVersion.Original) Then
          Return False
        End If

      Case DataRowState.Deleted
        If (RowVersion = DataRowVersion.Current) Then
          Return False
        End If
    End Select

    If Convert.IsDBNull(Row(ColumnIndex, RowVersion)) Then
      Return False
    End If

    Return True
  End Function

  ''' <summary>
  ''' Get boolean value for audit
  ''' </summary>
  ''' <param name="Row"></param>
  ''' <param name="ColumnIndex"></param>
  ''' <param name="RowVersion"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetAuditBooleanValue(Row As DataRow, ColumnIndex As Integer, RowVersion As DataRowVersion) As String
    If (Not CheckValueForRowVersionIsValid(Row, ColumnIndex, RowVersion)) Then
      Return AUDIT_NONE_STRING  'Value is NULL
    End If

    Return IIf(Boolean.Parse(Row(ColumnIndex, RowVersion)), m_text_yes, m_text_no)
  End Function

  ''' <summary>
  ''' Get number value for audit
  ''' </summary>
  ''' <param name="Row"></param>
  ''' <param name="ColumnIndex"></param>
  ''' <param name="RowVersion"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetAuditNumberValue(Row As DataRow, ColumnIndex As Integer, RowVersion As DataRowVersion) As String
    If (Not CheckValueForRowVersionIsValid(Row, ColumnIndex, RowVersion)) Then
      Return AUDIT_NONE_STRING  'Value is NULL
    End If

    Return GUI_FormatNumber(Row(ColumnIndex, RowVersion), 0, ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE)
  End Function

  ''' <summary>
  ''' Return number value of a column
  ''' </summary>
  ''' <param name="Row"></param>
  ''' <param name="ColumnIndex"></param>
  ''' <param name="RowVersion"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetColumnAsNumber(Row As DataRow, ColumnIndex As Integer, RowVersion As DataRowVersion) As Integer
    If (Not CheckValueForRowVersionIsValid(Row, ColumnIndex, RowVersion)) Then
      Return 0  'Value is NULL
    End If

    Return Row(ColumnIndex, RowVersion)
  End Function

  ''' <summary>
  ''' Get number value for audit
  ''' </summary>
  ''' <param name="Row"></param>
  ''' <param name="ColumnIndex"></param>
  ''' <param name="RowVersion"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetAuditHourValue(Row As DataRow, ColumnIndex As Integer, RowVersion As DataRowVersion) As String
    'Check if Rowversion id correct for the State
    Dim _date As DateTime
    _date = GetColumnAsDateTime(Row, ColumnIndex, RowVersion)

    If (_date.Equals(DateTime.MinValue)) Then
      Return AUDIT_NONE_STRING  'Value is NULL
    End If

    Return _date
  End Function

  ''' <summary>
  ''' Get record type
  ''' </summary>
  ''' <param name="Row"></param>
  ''' <param name="ColumnIndex"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetRecordType(Row As DataRow, ColumnIndex As Integer) As Integer
    Dim _row_version As DataRowVersion
    Dim _record_type As Integer

    _row_version = GetRowVersionForRowState(Row)

    _record_type = GetColumnAsNumber(Row, ColumnIndex, _row_version)

    Return _record_type
  End Function

  ''' <summary>
  ''' Get if a record is a cut-point
  ''' </summary>
  ''' <param name="Row"></param>
  ''' <param name="ColumnIndex"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetRecordIsCutPoint(Row As DataRow, ColumnIndex As Integer) As Boolean
    Dim _record_type As Integer

    _record_type = GetRecordType(Row, ColumnIndex)

    Return (_record_type = EGMMeterAdjustmentConstants.RECORD_TYPE_CUT)
  End Function

  ''' <summary>
  ''' Get datetime value of a column
  ''' </summary>
  ''' <param name="Row"></param>
  ''' <param name="ColumnIndex"></param>
  ''' <param name="RowVersion"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetColumnAsDateTime(Row As DataRow, ColumnIndex As Integer, RowVersion As DataRowVersion) As DateTime
    'Check if Rowversion id correct for the State
    If (Not CheckValueForRowVersionIsValid(Row, ColumnIndex, RowVersion)) Then
      Return DateTime.MinValue  'Value is NULL
    End If

    Return Row(ColumnIndex, RowVersion)
  End Function
#End Region

End Class
