'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_operations_schedule.vb
' DESCRIPTION:   Operations Schedule class
' AUTHOR:        Quim Morales
' CREATION DATE: 18-FEB-2013
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 18-FEB-2013  QMP    Initial version
' 09-SEP-2013  QMP    Added Work Shift Duration and Max Daily Cash Openings
' 22-JUN-2015  SGB    Backlog item 2162: Added check for enabled machines
' 06-JUL-2015  SGB    Fixed Bug: WIG-2547 Change NLS in auditor
'--------------------------------------------------------------------

#Region " Imports "

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports WSI.Common
Imports WSI.Common.OperationsSchedule
Imports System.Text
Imports System.Data.SqlClient
Imports GUI_Controls.Misc

#End Region ' Imports

Public Class CLASS_OPERATIONS_SCHEDULE
  Inherits CLASS_BASE

#Region " Members "

  Dim m_schedule_enabled As Boolean
  Dim m_disable_machines As Boolean
  Dim m_days_of_week(6) As TYPE_OPERATIONS_SCHEDULE_ITEM
  Dim m_exceptions As List(Of TYPE_OPERATIONS_SCHEDULE_ITEM)
  Dim m_exceptions_deleted As List(Of TYPE_OPERATIONS_SCHEDULE_ITEM)
  Dim m_closing_hour As Integer
  Dim m_recipients As List(Of String)
  Dim m_recipients_added As List(Of String)
  Dim m_recipients_deleted As List(Of String)
  Dim m_notifications_enabled As Boolean
  Dim m_notifications_subject As String
  Dim m_work_shift_duration As Integer
  Dim m_max_daily_cash_openings As Integer

#End Region ' Members

#Region " Properties "

  Public Property ScheduleEnabled() As Boolean
    Get
      Return m_schedule_enabled
    End Get
    Set(ByVal value As Boolean)
      m_schedule_enabled = value
    End Set
  End Property ' ScheduleEnabled

  Public Property DisableMachines() As Boolean
    Get
      Return m_disable_machines
    End Get
    Set(ByVal value As Boolean)
      m_disable_machines = value
    End Set
  End Property ' DisableMachine

  Public Property DayOfWeek_OperationsAllowed(ByVal DayOfWeek As DayOfWeek) As Boolean
    Get
      Return m_days_of_week(DayOfWeek).operations_allowed
    End Get
    Set(ByVal value As Boolean)
      m_days_of_week(DayOfWeek).operations_allowed = value
    End Set
  End Property ' DayOfWeek_OperationsAllowed

  Public Property DayOfWeek_Time1FromMinutes(ByVal DayOfWeek As DayOfWeek) As Integer
    Get
      Return m_days_of_week(DayOfWeek).time1_from_minutes
    End Get
    Set(ByVal value As Integer)
      m_days_of_week(DayOfWeek).time1_from_minutes = value
    End Set
  End Property ' DayOfWeek_Time1FromMinutes

  Public Property DayOfWeek_Time1ToMinutes(ByVal DayOfWeek As DayOfWeek) As Integer
    Get
      Return m_days_of_week(DayOfWeek).time1_to_minutes
    End Get
    Set(ByVal value As Integer)
      m_days_of_week(DayOfWeek).time1_to_minutes = value
    End Set
  End Property ' DayOfWeek_Time1ToMinutes

  Public Property DayOfWeek_Time2FromMinutes(ByVal DayOfWeek As DayOfWeek) As Integer
    Get
      Return m_days_of_week(DayOfWeek).time2_from_minutes
    End Get
    Set(ByVal value As Integer)
      m_days_of_week(DayOfWeek).time2_from_minutes = value
    End Set
  End Property ' DayOfWeek_Time2FromMinutes

  Public Property DayOfWeek_Time2ToMinutes(ByVal DayOfWeek As DayOfWeek) As Integer
    Get
      Return m_days_of_week(DayOfWeek).time2_to_minutes
    End Get
    Set(ByVal value As Integer)
      m_days_of_week(DayOfWeek).time2_to_minutes = value
    End Set
  End Property ' DayOfWeek_Time2ToMinutes

  Public Property ClosingHour() As Integer
    Get
      Return m_closing_hour
    End Get
    Set(ByVal value As Integer)
      m_closing_hour = value
    End Set
  End Property ' ClosingHour

  Public Property ExceptionsList() As List(Of TYPE_OPERATIONS_SCHEDULE_ITEM)
    Get
      Return m_exceptions
    End Get
    Set(ByVal value As List(Of TYPE_OPERATIONS_SCHEDULE_ITEM))
      m_exceptions = value
    End Set
  End Property ' ExceptionsList

  Public Property DeletedExceptionsList() As List(Of TYPE_OPERATIONS_SCHEDULE_ITEM)
    Get
      Return m_exceptions_deleted
    End Get
    Set(ByVal value As List(Of TYPE_OPERATIONS_SCHEDULE_ITEM))
      m_exceptions_deleted = value
    End Set
  End Property ' DeletedExceptionsList

  Public Property Recipients() As List(Of String)
    Get
      Return m_recipients
    End Get
    Set(ByVal value As List(Of String))
      m_recipients = value
    End Set
  End Property ' Recipients

  Public Property RecipientsAdded() As List(Of String)
    Get
      Return m_recipients_added
    End Get
    Set(ByVal value As List(Of String))
      m_recipients_added = value
    End Set
  End Property ' RecipientsAdded

  Public Property RecipientsDeleted() As List(Of String)
    Get
      Return m_recipients_deleted
    End Get
    Set(ByVal value As List(Of String))
      m_recipients_deleted = value
    End Set
  End Property ' RecipientsDeleted

  Public Property NotificationsEnabled() As Boolean
    Get
      Return m_notifications_enabled
    End Get
    Set(ByVal value As Boolean)
      m_notifications_enabled = value
    End Set
  End Property ' NotificationsEnabled

  Public Property NotificationsSubject() As String
    Get
      Return m_notifications_subject
    End Get
    Set(ByVal value As String)
      m_notifications_subject = value
    End Set
  End Property ' NotificationsSubject

  Public Property WorkShiftDuration() As Integer
    Get
      Return m_work_shift_duration
    End Get
    Set(ByVal value As Integer)
      m_work_shift_duration = value
    End Set
  End Property ' WorkShiftDuration

  Public Property MaxDailyCashOpenings() As Integer
    Get
      Return m_max_daily_cash_openings
    End Get
    Set(ByVal value As Integer)
      m_max_daily_cash_openings = value
    End Set
  End Property ' MaxDailyCashOpenings

#End Region ' Properties

#Region " Overrides "

  '----------------------------------------------------------------------------
  ' PURPOSE: Reads from the database into the given object
  '
  ' PARAMS:
  '   - INPUT:
  '     - ObjectId: An object, it must be 'casted' to the desired KEY.
  '     - SqlCtx
  '
  '   - OUTPUT: None
  '
  ' RETURNS:
  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As ENUM_STATUS

    Using _db_trx As New DB_TRX()

      _rc = Me.Read(_db_trx.SqlTransaction)

      If _rc <> ENUM_STATUS.STATUS_OK Then
        Call _db_trx.Rollback()
      Else
        _rc = IIf(_db_trx.Commit() = True, ENUM_STATUS.STATUS_OK, ENUM_STATUS.STATUS_ERROR)
      End If

    End Using ' _db_trx

    Return _rc

  End Function ' DB_Read

  '----------------------------------------------------------------------------
  ' PURPOSE: Updates the given object to the database.
  '
  ' PARAMS:
  '   - INPUT: SqlCtx
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As ENUM_STATUS

    Using _db_trx As New DB_TRX()

      _rc = Me.Update(_db_trx.SqlTransaction)

      If _rc <> ENUM_STATUS.STATUS_OK Then
        Call _db_trx.Rollback()
      Else
        _rc = IIf(_db_trx.Commit() = True, ENUM_STATUS.STATUS_OK, ENUM_STATUS.STATUS_ERROR)
      End If

    End Using ' _db_trx

    Return _rc

  End Function ' DB_Update

  '----------------------------------------------------------------------------
  ' PURPOSE: Returns a duplicate of the given object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim _clone As CLASS_OPERATIONS_SCHEDULE
    Dim _recipient As String

    _clone = New CLASS_OPERATIONS_SCHEDULE()
    _clone.m_exceptions = New List(Of TYPE_OPERATIONS_SCHEDULE_ITEM)
    _clone.m_exceptions_deleted = New List(Of TYPE_OPERATIONS_SCHEDULE_ITEM)
    _clone.m_recipients = New List(Of String)
    _clone.m_recipients_added = New List(Of String)
    _clone.m_recipients_deleted = New List(Of String)

    _clone.m_schedule_enabled = m_schedule_enabled
    _clone.m_disable_machines = m_disable_machines

    For _idx As Integer = 0 To m_days_of_week.Length - 1
      _clone.m_days_of_week(_idx) = m_days_of_week(_idx).Clone()
    Next

    For Each _exception As TYPE_OPERATIONS_SCHEDULE_ITEM In m_exceptions
      _clone.m_exceptions.Add(_exception.Clone())
    Next

    For Each _exception As TYPE_OPERATIONS_SCHEDULE_ITEM In m_exceptions_deleted
      _clone.m_exceptions_deleted.Add(_exception.Clone())
    Next

    _clone.m_closing_hour = m_closing_hour

    For Each _recipient In m_recipients
      _clone.m_recipients.Add(_recipient)
    Next

    For Each _recipient In m_recipients_added
      _clone.m_recipients_added.Add(_recipient)
    Next

    For Each _recipient In m_recipients_deleted
      _clone.m_recipients_deleted.Add(_recipient)
    Next

    _clone.m_notifications_enabled = m_notifications_enabled
    _clone.m_notifications_subject = m_notifications_subject

    _clone.m_work_shift_duration = m_work_shift_duration
    _clone.m_max_daily_cash_openings = m_max_daily_cash_openings

    Return _clone

  End Function ' Duplicate

  '----------------------------------------------------------------------------
  ' PURPOSE: Returns the related auditor data for the current object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - CLASS_AUDITOR_DATA
  '
  ' NOTES:
  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _date As Date
    Dim _day_of_week As String
    Dim _sched As TYPE_OPERATIONS_SCHEDULE_ITEM
    Dim _recipient As String

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_CASHIER_CONFIGURATION)
    _date = New Date()
    _day_of_week = ""

    Call _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(1663), "") ' Operations Schedule and Shifts

    'Schedule Enabled
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6518), _
                                IIf(m_schedule_enabled, GLB_NLS_GUI_ALARMS.GetString(318), _
                                                        GLB_NLS_GUI_ALARMS.GetString(319))) ' Enable Operations Schedule

    'Enabled machines
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6574), _
                                IIf(m_disable_machines, GLB_NLS_GUI_ALARMS.GetString(318), _
                                                        GLB_NLS_GUI_ALARMS.GetString(319))) ' Enable Machines

    'Notifications
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1709), _
                                IIf(m_notifications_enabled, GLB_NLS_GUI_ALARMS.GetString(318), _
                                                             GLB_NLS_GUI_ALARMS.GetString(319))) ' Notifications enabled

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1710), m_notifications_subject) ' Notifications subject

    'Closing Hour
    If m_closing_hour = -1 Then
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1677), "No Configurado") ' Workday Start
    Else
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1677), _
                                  WSI.Common.Format.CustomFormatTime(New Date().AddHours(m_closing_hour), False)) ' Workday Start
    End If

    'Days of week
    For Each _sched In m_days_of_week
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1676), _sched.ToString()) ' Schedule
    Next

    'Exceptions
    For Each _sched In m_exceptions
      If _sched.id < 0 Then
        Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1685), _sched.ToString()) ' New exception
      End If
    Next

    For Each _sched In m_exceptions_deleted
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1692), _sched.ToString()) ' Deleted exception
    Next

    'Recipients
    For Each _recipient In m_recipients_added
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1707), _recipient) ' New recipient
    Next

    For Each _recipient In m_recipients_deleted
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1708), _recipient) ' Deleted recipient
    Next

    'Work Shift Duration and Max Daily Cash Openings
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2586), _
                                IIf(m_work_shift_duration = 0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2587), _
                                                               m_work_shift_duration.ToString() + " " + GLB_NLS_GUI_PLAYER_TRACKING.GetString(2582))) ' Work Shift Duration

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2584), _
                                IIf(m_max_daily_cash_openings = 0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2587), _
                                                                   m_max_daily_cash_openings)) ' Maximum Daily Cash Openings per User

    Return _auditor_data

  End Function ' AuditorData

  '----------------------------------------------------------------------------
  ' PURPOSE : Deletes the given object from the database.
  '
  ' PARAMS :
  '     - INPUT : SqlCtx
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - ENUM_STATUS
  '
  ' NOTES : 
  '
  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Return ENUM_STATUS.STATUS_NOT_SUPPORTED

  End Function ' DB_Delete

  '----------------------------------------------------------------------------
  ' PURPOSE : Inserts the given object in the database.
  '
  ' PARAMS :
  '     - INPUT : SqlCtx
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - ENUM_STATUS
  '
  ' NOTES : 
  '
  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Return ENUM_STATUS.STATUS_NOT_SUPPORTED

  End Function ' DB_Insert

#End Region ' Overrides

#Region " Public Functions "

  Public Sub New()

    m_exceptions = New List(Of TYPE_OPERATIONS_SCHEDULE_ITEM)
    m_exceptions_deleted = New List(Of TYPE_OPERATIONS_SCHEDULE_ITEM)
    m_recipients = New List(Of String)
    m_recipients_added = New List(Of String)
    m_recipients_deleted = New List(Of String)

  End Sub ' New

#End Region ' Public Functions

#Region " Private Functions "

  '----------------------------------------------------------------------------
  ' PURPOSE: Reads operations schedules from the database.
  '
  ' PARAMS:
  '   - INPUT: Trx As SqlTransaction
  '
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Private Function Read(ByVal Trx As SqlTransaction) As ENUM_STATUS

    Dim _sb As StringBuilder
    Dim _sched As TYPE_OPERATIONS_SCHEDULE_ITEM
    Dim _recipients_string As String
    Dim _recipients_array() As String
    Dim _recipient As String
    Dim _working_day As Date
    Dim _now As Date
    Dim _general_params As GeneralParam.Dictionary

    _now = WGDB.Now

    'Populate General Params dictionary with the current database values
    _general_params = GeneralParam.Dictionary.Create(Trx)

    'Get closing hour
    m_closing_hour = _general_params.GetInt32("WigosGUI", "ClosingTime", -1)

    If m_closing_hour = -1 Then
      Return ENUM_STATUS.STATUS_ERROR
    End If

    'Determine the right working day, depending on closing time
    _working_day = WSI.Common.Misc.Opening(_now, m_closing_hour, 0)

    Try

      _sb = New StringBuilder()
      _sb.AppendLine(" SELECT   OS_UNIQUE_ID          ")
      _sb.AppendLine("        , OS_TYPE               ")
      _sb.AppendLine("        , OS_DAY_OF_WEEK        ")
      _sb.AppendLine("        , OS_DATE_FROM          ")
      _sb.AppendLine("        , OS_DATE_TO            ")
      _sb.AppendLine("        , OS_OPERATIONS_ALLOWED ")
      _sb.AppendLine("        , OS_TIME1_FROM         ")
      _sb.AppendLine("        , OS_TIME1_TO           ")
      _sb.AppendLine("        , OS_TIME2_FROM         ")
      _sb.AppendLine("        , OS_TIME2_TO           ")
      _sb.AppendLine("   FROM   OPERATIONS_SCHEDULE   ")
      _sb.AppendLine("  WHERE   OS_DATE_TO >= @pNow   ")
      _sb.AppendLine("     OR   OS_DATE_TO IS NULL    ")

      Using _cmd As New SqlCommand(_sb.ToString(), Trx.Connection, Trx)

        _cmd.Parameters.Add("@pNow", SqlDbType.DateTime).Value = _working_day.Date

        Using _reader As SqlDataReader = _cmd.ExecuteReader()

          While _reader.Read()

            _sched = New TYPE_OPERATIONS_SCHEDULE_ITEM
            _sched.id = _reader.GetInt64(0)
            _sched.type = _reader.GetInt32(1)

            If IsDBNull(_reader(2)) Then
              _sched.day_of_week = DayOfWeek.Sunday
            Else
              _sched.day_of_week = _reader.GetInt32(2)
            End If

            If IsDBNull(_reader(3)) Then
              _sched.date_from = Date.MinValue
            Else
              _sched.date_from = _reader.GetDateTime(3)
            End If

            If IsDBNull(_reader(4)) Then
              _sched.date_to = Date.MaxValue
            Else
              _sched.date_to = _reader.GetDateTime(4)
            End If

            _sched.operations_allowed = _reader.GetBoolean(5)
            _sched.time1_from_minutes = _reader.GetInt32(6)
            _sched.time1_to_minutes = _reader.GetInt32(7)

            If IsDBNull(_reader(8)) Then
              _sched.time2_from_minutes = -1
            Else
              _sched.time2_from_minutes = _reader.GetInt32(8)
            End If

            If IsDBNull(_reader(9)) Then
              _sched.time2_to_minutes = -1
            Else
              _sched.time2_to_minutes = _reader.GetInt32(9)
            End If

            'Add to corresponding structure
            If _sched.type = OPERATIONS_SCHEDULE_TYPE.DAY_OF_WEEK Then
              m_days_of_week(_sched.day_of_week) = _sched
            ElseIf _sched.type = OPERATIONS_SCHEDULE_TYPE.DATE_RANGE Then
              m_exceptions.Add(_sched)
            End If
          End While
        End Using
      End Using

      'Disable machine
      m_disable_machines = _general_params.GetBoolean("OperationsSchedule", "DisableMachine")

      'Operations Schedule Enabled
      m_schedule_enabled = _general_params.GetBoolean("OperationsSchedule", "Enabled")

      'Notifications
      m_notifications_enabled = _general_params.GetBoolean("OperationsSchedule", "MailingEnabled")
      m_notifications_subject = _general_params.GetString("OperationsSchedule", "Subject")

      _recipients_string = _general_params.GetString("OperationsSchedule", "MailingList")

      If _recipients_string.Length > 0 Then
        _recipients_array = _recipients_string.Split(",")
        For Each _recipient In _recipients_array
          m_recipients.Add(_recipient)
        Next
      End If

      'Work Shift Duration and Max Daily Cash Openings
      m_work_shift_duration = _general_params.GetInt32("Cashier", "WorkShiftDurationHours")
      m_max_daily_cash_openings = _general_params.GetInt32("Cashier", "MaxDailyCashOpenings")


    Catch _ex As Exception

      Return ENUM_STATUS.STATUS_ERROR

    End Try

    Return ENUM_STATUS.STATUS_OK

  End Function ' Read

  '----------------------------------------------------------------------------
  ' PURPOSE: Updates operation schedules to the database.
  '
  ' PARAMS:
  '   - INPUT: Trx As SqlTransaction
  '
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Private Function Update(ByVal Trx As SqlTransaction) As ENUM_STATUS

    Try

      If Not DB_GeneralParam_Update("OperationsSchedule", "DisableMachine", IIf(m_disable_machines, "1", "0"), Trx) Then
        Return ENUM_STATUS.STATUS_ERROR
      End If

      'Update Closing Hour
      If Not DB_GeneralParam_Update("WigosGUI", "ClosingTime", m_closing_hour, Trx) Then
        Return ENUM_STATUS.STATUS_ERROR
      End If

      'Update Notifications
      If Not DB_GeneralParam_Update("OperationsSchedule", "Enabled", IIf(m_schedule_enabled, "1", "0"), Trx) Then
        Return ENUM_STATUS.STATUS_ERROR
      End If

      If Not DB_GeneralParam_Update("OperationsSchedule", "MailingEnabled", IIf(m_notifications_enabled, "1", "0"), Trx) Then
        Return ENUM_STATUS.STATUS_ERROR
      End If

      If Not DB_GeneralParam_Update("OperationsSchedule", "Subject", m_notifications_subject, Trx) Then
        Return ENUM_STATUS.STATUS_ERROR
      End If

      If Not DB_GeneralParam_Update("OperationsSchedule", "MailingList", String.Join(",", m_recipients.ToArray()), Trx) Then
        Return ENUM_STATUS.STATUS_ERROR
      End If

      'Update days of week
      For Each _sched As TYPE_OPERATIONS_SCHEDULE_ITEM In m_days_of_week
        If Not UpdateItem(_sched, Trx) Then
          Return ENUM_STATUS.STATUS_ERROR
        End If
      Next

      'Update/insert/delete exceptions
      For Each _sched As TYPE_OPERATIONS_SCHEDULE_ITEM In m_exceptions
        If _sched.id < 0 Then
          If Not InsertItem(_sched, Trx) Then
            Return ENUM_STATUS.STATUS_ERROR
          End If
        Else
          If Not UpdateItem(_sched, Trx) Then
            Return ENUM_STATUS.STATUS_ERROR
          End If
        End If
      Next

      For Each _sched As TYPE_OPERATIONS_SCHEDULE_ITEM In m_exceptions_deleted
        If Not DeleteItem(_sched.id, Trx) Then
          Return ENUM_STATUS.STATUS_ERROR
        End If
      Next

      'Update Work Shift Duration and Max Daily Cash Openings
      If Not DB_GeneralParam_Update("Cashier", "WorkShiftDurationHours", m_work_shift_duration, Trx) Then
        Return ENUM_STATUS.STATUS_ERROR
      End If

      If Not DB_GeneralParam_Update("Cashier", "MaxDailyCashOpenings", m_max_daily_cash_openings, Trx) Then
        Return ENUM_STATUS.STATUS_ERROR
      End If

    Catch _ex As Exception

      Return ENUM_STATUS.STATUS_ERROR

    End Try

    Return ENUM_STATUS.STATUS_OK

  End Function ' Update

  '----------------------------------------------------------------------------
  ' PURPOSE: Updates a single operations schedule item to the database.
  '
  ' PARAMS:
  '   - INPUT: Schedule as TYPE_OPERATIONS_SCHEDULE_ITEM
  '            Trx As SqlTransaction
  '
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - Boolean
  '
  ' NOTES:
  Private Function UpdateItem(ByVal Schedule As TYPE_OPERATIONS_SCHEDULE_ITEM, ByVal Trx As SqlTransaction) As Boolean

    Dim _sb As StringBuilder

    Try

      _sb = New StringBuilder
      _sb.AppendLine(" UPDATE   OPERATIONS_SCHEDULE                         ")
      _sb.AppendLine("    SET   OS_TYPE               = @pType              ")
      _sb.AppendLine("        , OS_DAY_OF_WEEK        = @pDayOfWeek         ")
      _sb.AppendLine("        , OS_DATE_FROM          = @pDateFrom          ")
      _sb.AppendLine("        , OS_DATE_TO            = @pDateTo            ")
      _sb.AppendLine("        , OS_OPERATIONS_ALLOWED = @pOperationsAllowed ")
      _sb.AppendLine("        , OS_TIME1_FROM         = @pTime1From         ")
      _sb.AppendLine("        , OS_TIME1_TO           = @pTime1To           ")
      _sb.AppendLine("        , OS_TIME2_FROM         = @pTime2From         ")
      _sb.AppendLine("        , OS_TIME2_TO           = @pTime2To           ")
      _sb.AppendLine("  WHERE   OS_UNIQUE_ID          = @pId                ")

      Using _cmd As New SqlCommand(_sb.ToString(), Trx.Connection, Trx)

        _cmd.Parameters.Add("@pType", SqlDbType.Int).Value = Schedule.type

        If Schedule.type = OPERATIONS_SCHEDULE_TYPE.DAY_OF_WEEK Then
          _cmd.Parameters.Add("@pDayOfWeek", SqlDbType.Int).Value = Schedule.day_of_week
          _cmd.Parameters.Add("@pDateFrom", SqlDbType.DateTime).Value = DBNull.Value
          _cmd.Parameters.Add("@pDateTo", SqlDbType.DateTime).Value = DBNull.Value
        Else
          _cmd.Parameters.Add("@pDayOfWeek", SqlDbType.Int).Value = DBNull.Value
          _cmd.Parameters.Add("@pDateFrom", SqlDbType.DateTime).Value = Schedule.date_from
          _cmd.Parameters.Add("@pDateTo", SqlDbType.DateTime).Value = Schedule.date_to
        End If

        _cmd.Parameters.Add("@pOperationsAllowed", SqlDbType.Bit).Value = Schedule.operations_allowed
        _cmd.Parameters.Add("@pTime1From", SqlDbType.Int).Value = Schedule.time1_from_minutes
        _cmd.Parameters.Add("@pTime1To", SqlDbType.Int).Value = Schedule.time1_to_minutes

        If Schedule.time2_from_minutes = -1 Then
          _cmd.Parameters.Add("@pTime2From", SqlDbType.Int).Value = DBNull.Value
        Else
          _cmd.Parameters.Add("@pTime2From", SqlDbType.Int).Value = Schedule.time2_from_minutes
        End If

        If Schedule.time2_to_minutes = -1 Then
          _cmd.Parameters.Add("@pTime2To", SqlDbType.Int).Value = DBNull.Value
        Else
          _cmd.Parameters.Add("@pTime2To", SqlDbType.Int).Value = Schedule.time2_to_minutes
        End If

        _cmd.Parameters.Add("@pId", SqlDbType.BigInt).Value = Schedule.id

        If _cmd.ExecuteNonQuery() <> 1 Then
          Return False
        End If

      End Using

    Catch _ex As Exception

      Return False
    End Try

    Return True

  End Function ' UpdateItem

  '----------------------------------------------------------------------------
  ' PURPOSE: Inserts a single operations schedule item to the database.
  '
  ' PARAMS:
  '   - INPUT: Schedule as TYPE_OPERATIONS_SCHEDULE_ITEM
  '            Trx As SqlTransaction
  '
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - Boolean
  '
  ' NOTES:
  Private Function InsertItem(ByVal Schedule As TYPE_OPERATIONS_SCHEDULE_ITEM, ByVal Trx As SqlTransaction) As Boolean

    Dim _sb As StringBuilder

    'Only exceptions can be inserted
    If Schedule.type <> OPERATIONS_SCHEDULE_TYPE.DATE_RANGE Then
      Return False
    End If

    Try

      _sb = New StringBuilder
      _sb.AppendLine(" INSERT INTO   OPERATIONS_SCHEDULE   ")
      _sb.AppendLine("             ( OS_TYPE               ")
      _sb.AppendLine("             , OS_DAY_OF_WEEK        ")
      _sb.AppendLine("             , OS_DATE_FROM          ")
      _sb.AppendLine("             , OS_DATE_TO            ")
      _sb.AppendLine("             , OS_OPERATIONS_ALLOWED ")
      _sb.AppendLine("             , OS_TIME1_FROM         ")
      _sb.AppendLine("             , OS_TIME1_TO           ")
      _sb.AppendLine("             , OS_TIME2_FROM         ")
      _sb.AppendLine("             , OS_TIME2_TO           ")
      _sb.AppendLine("             )                       ")
      _sb.AppendLine("             VALUES                  ")
      _sb.AppendLine("             ( @pType                ")
      _sb.AppendLine("             , @pDayOfWeek           ")
      _sb.AppendLine("             , @pDateFrom            ")
      _sb.AppendLine("             , @pDateTo              ")
      _sb.AppendLine("             , @pOperationsAllowed   ")
      _sb.AppendLine("             , @pTime1From           ")
      _sb.AppendLine("             , @pTime1To             ")
      _sb.AppendLine("             , @pTime2From           ")
      _sb.AppendLine("             , @pTime2To             ")
      _sb.AppendLine("             )                       ")

      Using _cmd As New SqlCommand(_sb.ToString(), Trx.Connection, Trx)

        _cmd.Parameters.Add("@pType", SqlDbType.Int).Value = Schedule.type
        _cmd.Parameters.Add("@pDayOfWeek", SqlDbType.Int).Value = DBNull.Value
        _cmd.Parameters.Add("@pDateFrom", SqlDbType.DateTime).Value = Schedule.date_from
        _cmd.Parameters.Add("@pDateTo", SqlDbType.DateTime).Value = Schedule.date_to
        _cmd.Parameters.Add("@pOperationsAllowed", SqlDbType.Bit).Value = Schedule.operations_allowed
        _cmd.Parameters.Add("@pTime1From", SqlDbType.Int).Value = Schedule.time1_from_minutes
        _cmd.Parameters.Add("@pTime1To", SqlDbType.Int).Value = Schedule.time1_to_minutes

        If Schedule.time2_from_minutes = -1 Then
          _cmd.Parameters.Add("@pTime2From", SqlDbType.Int).Value = DBNull.Value
        Else
          _cmd.Parameters.Add("@pTime2From", SqlDbType.Int).Value = Schedule.time2_from_minutes
        End If

        If Schedule.time2_to_minutes = -1 Then
          _cmd.Parameters.Add("@pTime2To", SqlDbType.Int).Value = DBNull.Value
        Else
          _cmd.Parameters.Add("@pTime2To", SqlDbType.Int).Value = Schedule.time2_to_minutes
        End If

        If _cmd.ExecuteNonQuery <> 1 Then

          Return False
        End If

      End Using

    Catch _ex As Exception

      Return False
    End Try

    Return True

  End Function ' InsertItem

  '----------------------------------------------------------------------------
  ' PURPOSE: Deletes a single operations schedule item to the database by ID.
  '
  ' PARAMS:
  '   - INPUT: ScheduleId as Int64
  '            Trx As SqlTransaction
  '
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - Boolean
  '
  ' NOTES:
  Private Function DeleteItem(ByVal ScheduleId As Int64, ByVal Trx As SqlTransaction) As Boolean

    Dim _sb As StringBuilder

    Try

      _sb = New StringBuilder
      _sb.AppendLine(" DELETE  OPERATIONS_SCHEDULE            ")
      _sb.AppendLine("  WHERE  OS_UNIQUE_ID = @pId            ")
      _sb.AppendLine("    AND  OS_TYPE      = @pTypeDateRange ")

      Using _cmd As New SqlCommand(_sb.ToString(), Trx.Connection, Trx)

        _cmd.Parameters.Add("@pId", SqlDbType.BigInt).Value = ScheduleId
        _cmd.Parameters.Add("@pTypeDateRange", SqlDbType.Int).Value = OPERATIONS_SCHEDULE_TYPE.DATE_RANGE

        If _cmd.ExecuteNonQuery <> 1 Then

          Return False
        End If

      End Using

    Catch _ex As Exception

      Return False
    End Try

    Return True

  End Function ' DeleteItem

#End Region ' Private Functions

End Class ' CLASS_OPERATIONS_SCHEDULE
