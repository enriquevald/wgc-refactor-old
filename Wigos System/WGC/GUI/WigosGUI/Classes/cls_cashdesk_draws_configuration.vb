'------------------------------------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'------------------------------------------------------------------------------------------------
'
' MODULE NAME : cls_cashdesk_draws_configuration.vb
'
' DESCRIPTION : Cash desk draws parameters configuration class
'
' REVISION HISTORY:
' 
' Date         Author Description
' -----------  ------ ---------------------------------------------------------------------------
' 02-AUG-2013  MMG    Initial version
' 13-AUG-2013  DMR    Modified CashdeskDrawsConf_DbRead function
' 14-AUG-2013  DMR    Modified AuditorData function
' 19-AUG-2013  MMG    Modified following functions:
'                       - CashdeskDrawsConf_DbRead
'                       - CashdeskDrawsConf_DbUpdate 
' 22-AUG-2013  MMG    Added new members and properties (enable winner prizes 1,2,3,4 and loser prize)
' 26-AUG-2013  MMG    Modified AuditorData function
' 26-AUG-2013  MMG    Modified CashdeskDrawsConf_DbRead function (now it's using the general params dictionary)
' 04-SEP-2013  MMG    Added new members and properties (turn on/of winner prizes 1,2,3,4 as non redimible credits)
' 26-SEP-2013  MMG    Added functions for enable/disable virtual terminal when cashdeskdraw is enabled/disabled
' 28-OCT-2013  ACM    Fixed Bug #WIG-322 Wrong auditor code.
' 01-APR-2014  DHA    Fixed Bug #WIG-780 Wrong format of Company B percentage when it has a decimal part (Rounded to integer)
' 14-OCT-2015  OCT    Add New Promotion "In Kind (RE)"
' 13-JAN-2015  JML    Product Backlog Item 6559: Cashier draw (RE in kind)
' 12-SEP-2016  RGR    Product Backlog Item 17445:Televisa - Cashier draw (Cash prize) - Configuration
' 14-SEP-2016  LTC    PBI 17115: Tables 46 - CashDeskDraw for tables: GUI - Configuration
' 14-SEP-2016  RGR    Product Backlog Item 17445:Televisa - Cashier draw (Cash prize) - Configuration Change Enum
' 22-SEP-2016  LTC    Product Backlog Item 17960:Tables 46 - Cashier draw for tables : Configuration Screen
' 23-SEP-2016  RGR    Product Backlog Item 17965: Televisa - Draw Cash (Cash prize) - Probability of prizes
' 10-OCT-2016  RGR    Bug 18880: Draw tables: the percentage configuration tables draw does not correspond to company A and B.
' 05-FEB-2018  AGS    Bug 31411:WIGOS-8056 Application crashes when leaving empty a configuration field in Terminal Draw screen
' 14-FEB-2018  AGS    Fixed Bug 31411:WIGOS-8056 Application crashes when leaving empty a configuration field in Terminal Draw screen
' 04-JUL-2018  DLM    Bug 33475: WIGOS-13382 Losing changes warning message appear without any change in the screen
'-----------------------------------------------------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports System.Text
Imports WSI.Common

Public Class CLASS_CASHDESK_DRAWS_CONFIGURATION
  Inherits CLASS_BASE

#Region "Enums"

  Public Enum ENUM_WAIT_NOT_CONECTED
    DONT_RECHARGE = 0
    RECHARGE_REDIMIBLE = 1
    RECHARGE_NONREDIMIBLE = 2

  End Enum

  Public Enum ENUM_DRAW_LOCATION
    WEBSERVICE_OR_EXTERNALDB = 0
    LOCAL_DB = 1

  End Enum

  ' Listed will be added when necessary a new draw configuration
  Public Enum ENUM_DRAW_CONFIGURATION
    DRAW_00 = 0
    DRAW_01 = 1
    DRAW_02 = 2

  End Enum

#End Region 'Enums

#Region "constants"

#End Region

#Region "Structure"

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_CASHDESK_DRAWS_CONFIGURATION

    Public enabled As Boolean
    Public show_cashdesk_draws As Boolean
    Public voucher_on_cashdesk_draws_winner As Boolean
    Public voucher_on_cashdesk_draws_loser As Boolean
    Public ask_for_participation As Boolean
    Public wait_not_connected As ENUM_WAIT_NOT_CONECTED
    Public draw_location As ENUM_DRAW_LOCATION
    Public server_address1 As String
    Public server_address2 As String
    Public serverDB_connection_string As String
    Public balls_extracted As Long
    Public balls_of_participant As Long
    Public total_balls_number As Long
    Public tax_provisions_enable As Boolean
    Public loser_prize_enabled As Boolean
    Public loser_prize_fixed As Decimal
    Public loser_prize_percentage As Decimal
    Public loser_prize_access_percentage As Double
    Public loser_prize_tax_provisions As Decimal
    Public loser_prize_probability As Decimal
    Public number_of_participants As Integer
    Public number_of_winners As Integer
    Public winner_prize1_fixed As Decimal
    Public winner_prize1_percentage As Decimal
    Public winner_prize1_access_percentage As Double
    Public winner_prize1_tax_provisions As Decimal
    Public winner_prize1_probability As Decimal
    Public winner_prize2_fixed As Decimal
    Public winner_prize2_percentage As Decimal
    Public winner_prize2_access_percentage As Double
    Public winner_prize2_tax_provisions As Decimal
    Public winner_prize2_probability As Decimal
    Public winner_prize3_fixed As Decimal
    Public winner_prize3_percentage As Decimal
    Public winner_prize3_access_percentage As Double
    Public winner_prize3_tax_provisions As Decimal
    Public winner_prize3_probability As Decimal
    Public winner_prize4_fixed As Decimal
    Public winner_prize4_percentage As Decimal
    Public winner_prize4_access_percentage As Double
    Public winner_prize4_tax_provisions As Decimal
    Public winner_prize4_probability As Decimal
    Public winner_prize1_enabled As Boolean
    Public winner_prize2_enabled As Boolean
    Public winner_prize3_enabled As Boolean
    Public winner_prize4_enabled As Boolean
    Public winner_prize1_credit_type As PrizeType
    Public winner_prize2_credit_type As PrizeType
    Public winner_prize3_credit_type As PrizeType
    Public winner_prize4_credit_type As PrizeType
    Public loser_credit_type As PrizeType
    Public winner_voucher_title As String
    Public loser_voucher_title As String
    Public prize_mode As CashDeskDrawBusinessLogic.PrizeMode
    Public participation_price As Decimal
    Public participation_max_price As Decimal
    Public terminal_game_url As String
    Public terminal_game_name As String
    Public terminal_game_timeout As Long
    Public show_buy_dialog As Boolean
    Public player_can_cancel_draw As Boolean
    Public terminal_game_group_draws As Long
    Public terminal_game_withdraw_with_pending As Boolean
    Public terminal_game_cancel_with_pending As Boolean
  End Class

#Region "Cash Desk Draw Instance"

#End Region 'Cash Desk Draw Instance

#End Region 'Structure

#Region "Members "

  Protected m_cd_configuration As New TYPE_CASHDESK_DRAWS_CONFIGURATION
  Private m_draw_configuration As ENUM_DRAW_CONFIGURATION
#End Region

#Region "Properties"

  Public Property Enabled() As Boolean
    Get
      Return m_cd_configuration.enabled
    End Get
    Set(ByVal value As Boolean)
      m_cd_configuration.enabled = value
    End Set
  End Property

  Public Property ShowCashdeskDraws() As Boolean
    Get
      Return m_cd_configuration.show_cashdesk_draws
    End Get
    Set(ByVal value As Boolean)
      m_cd_configuration.show_cashdesk_draws = value
    End Set
  End Property

  Public Property VoucherOnCashdeskDrawsWinner() As Boolean
    Get
      Return m_cd_configuration.voucher_on_cashdesk_draws_winner
    End Get
    Set(ByVal value As Boolean)
      m_cd_configuration.voucher_on_cashdesk_draws_winner = value
    End Set
  End Property
  Public Property VoucherOnCashdeskDrawsLoser() As Boolean
    Get
      Return m_cd_configuration.voucher_on_cashdesk_draws_loser
    End Get
    Set(ByVal value As Boolean)
      m_cd_configuration.voucher_on_cashdesk_draws_loser = value
    End Set
  End Property

  Public Property AskForParticipation() As Boolean
    Get
      Return m_cd_configuration.ask_for_participation
    End Get

    Set(ByVal value As Boolean)
      m_cd_configuration.ask_for_participation = value
    End Set
  End Property

  Public Property WaitNotConnected() As ENUM_WAIT_NOT_CONECTED
    Get
      Return m_cd_configuration.wait_not_connected
    End Get

    Set(ByVal value As ENUM_WAIT_NOT_CONECTED)
      m_cd_configuration.wait_not_connected = value
    End Set
  End Property

  Public Property DrawLocation() As ENUM_DRAW_LOCATION
    Get
      Return m_cd_configuration.draw_location
    End Get

    Set(ByVal value As ENUM_DRAW_LOCATION)
      m_cd_configuration.draw_location = value
    End Set
  End Property

  Public Property ServerAddress1() As String
    Get
      Return m_cd_configuration.server_address1
    End Get

    Set(ByVal value As String)
      m_cd_configuration.server_address1 = value
    End Set
  End Property

  Public Property ServerAddress2() As String
    Get
      Return m_cd_configuration.server_address2
    End Get

    Set(ByVal value As String)
      m_cd_configuration.server_address2 = value
    End Set
  End Property

  Public Property ServerDbConnectionString() As String
    Get
      Return m_cd_configuration.serverDB_connection_string
    End Get

    Set(ByVal value As String)
      m_cd_configuration.serverDB_connection_string = value
    End Set
  End Property

  Public Property BallsExtracted() As Long
    Get
      Return m_cd_configuration.balls_extracted
    End Get

    Set(ByVal value As Long)
      m_cd_configuration.balls_extracted = value
    End Set
  End Property

  Public Property BallsOfParticipant() As Long
    Get
      Return m_cd_configuration.balls_of_participant
    End Get

    Set(ByVal value As Long)
      m_cd_configuration.balls_of_participant = value
    End Set
  End Property

  Public Property TotalBallsNumber() As Long
    Get
      Return m_cd_configuration.total_balls_number
    End Get

    Set(ByVal value As Long)
      m_cd_configuration.total_balls_number = value
    End Set
  End Property

  Public Property LoserPrizeFixed() As Decimal
    Get
      Return m_cd_configuration.loser_prize_fixed
    End Get

    Set(ByVal value As Decimal)
      m_cd_configuration.loser_prize_fixed = value
    End Set
  End Property

  Public Property LoserPrizePercentage() As Decimal
    Get
      Return m_cd_configuration.loser_prize_percentage
    End Get

    Set(ByVal value As Decimal)
      m_cd_configuration.loser_prize_percentage = value
    End Set
  End Property
  Public Property LoserPrizeAccessPercentage() As Double
    Get
      Return m_cd_configuration.loser_prize_access_percentage
    End Get

    Set(ByVal value As Double)
      m_cd_configuration.loser_prize_access_percentage = value
    End Set
  End Property

  Public Property LoserPrizeProbability() As Decimal
    Get
      Return m_cd_configuration.loser_prize_probability
    End Get

    Set(ByVal value As Decimal)
      m_cd_configuration.loser_prize_probability = value
    End Set
  End Property

  Public Property LoserTaxProvisionsPercentage() As Decimal
    Get
      Return m_cd_configuration.loser_prize_tax_provisions
    End Get

    Set(ByVal value As Decimal)
      m_cd_configuration.loser_prize_tax_provisions = value
    End Set
  End Property

  Public Property TaxProvisionsEnable() As Boolean
    Get
      Return m_cd_configuration.tax_provisions_enable
    End Get

    Set(ByVal value As Boolean)
      m_cd_configuration.tax_provisions_enable = value
    End Set
  End Property

  Public Property NumberOfParticipants() As Integer
    Get
      Return m_cd_configuration.number_of_participants
    End Get

    Set(ByVal value As Integer)
      m_cd_configuration.number_of_participants = value
    End Set
  End Property

  Public Property NumberOfWinners() As Integer
    Get
      Return m_cd_configuration.number_of_winners
    End Get

    Set(ByVal value As Integer)
      m_cd_configuration.number_of_winners = value
    End Set
  End Property

  Public Property WinnerPrize1Fixed() As Decimal
    Get
      Return m_cd_configuration.winner_prize1_fixed
    End Get

    Set(ByVal value As Decimal)
      m_cd_configuration.winner_prize1_fixed = value
    End Set
  End Property

  Public Property WinnerPrize1Percentage() As Decimal
    Get
      Return m_cd_configuration.winner_prize1_percentage
    End Get

    Set(ByVal value As Decimal)
      m_cd_configuration.winner_prize1_percentage = value
    End Set
  End Property
  Public Property WinnerPrize1AccessPercentage() As Double
    Get
      Return m_cd_configuration.winner_prize1_access_percentage
    End Get

    Set(ByVal value As Double)
      m_cd_configuration.winner_prize1_access_percentage = value
    End Set
  End Property

  Public Property WinnerPrize1Probability() As Decimal
    Get
      Return m_cd_configuration.winner_prize1_probability
    End Get

    Set(ByVal value As Decimal)
      m_cd_configuration.winner_prize1_probability = value
    End Set
  End Property


  Public Property WinnerTaxProvisions1Percentage() As Decimal
    Get
      Return m_cd_configuration.winner_prize1_tax_provisions
    End Get

    Set(ByVal value As Decimal)
      m_cd_configuration.winner_prize1_tax_provisions = value
    End Set
  End Property

  Public Property WinnerPrize2Fixed() As Decimal
    Get
      Return m_cd_configuration.winner_prize2_fixed
    End Get

    Set(ByVal value As Decimal)
      m_cd_configuration.winner_prize2_fixed = value
    End Set
  End Property

  Public Property WinnerPrize2Percentage() As Decimal
    Get
      Return m_cd_configuration.winner_prize2_percentage
    End Get

    Set(ByVal value As Decimal)
      m_cd_configuration.winner_prize2_percentage = value
    End Set
  End Property
  Public Property WinnerPrize2AccessPercentage() As Double
    Get
      Return m_cd_configuration.winner_prize2_access_percentage
    End Get

    Set(ByVal value As Double)
      m_cd_configuration.winner_prize2_access_percentage = value
    End Set
  End Property

  Public Property WinnerPrize2Probability() As Decimal
    Get
      Return m_cd_configuration.winner_prize2_probability
    End Get

    Set(ByVal value As Decimal)
      m_cd_configuration.winner_prize2_probability = value
    End Set
  End Property

  Public Property WinnerTaxProvisions2Percentage() As Decimal
    Get
      Return m_cd_configuration.winner_prize2_tax_provisions
    End Get

    Set(ByVal value As Decimal)
      m_cd_configuration.winner_prize2_tax_provisions = value
    End Set
  End Property

  Public Property WinnerPrize3Fixed() As Decimal
    Get
      Return m_cd_configuration.winner_prize3_fixed
    End Get

    Set(ByVal value As Decimal)
      m_cd_configuration.winner_prize3_fixed = value
    End Set
  End Property

  Public Property WinnerPrize3Percentage() As Decimal
    Get
      Return m_cd_configuration.winner_prize3_percentage
    End Get

    Set(ByVal value As Decimal)
      m_cd_configuration.winner_prize3_percentage = value
    End Set
  End Property
  Public Property WinnerPrize3AccessPercentage() As Double
    Get
      Return m_cd_configuration.winner_prize3_access_percentage
    End Get

    Set(ByVal value As Double)
      m_cd_configuration.winner_prize3_access_percentage = value
    End Set
  End Property

  Public Property WinnerPrize3Probability() As Decimal
    Get
      Return m_cd_configuration.winner_prize3_probability
    End Get

    Set(ByVal value As Decimal)
      m_cd_configuration.winner_prize3_probability = value
    End Set
  End Property

  Public Property WinnerTaxProvisions3Percentage() As Decimal
    Get
      Return m_cd_configuration.winner_prize3_tax_provisions
    End Get

    Set(ByVal value As Decimal)
      m_cd_configuration.winner_prize3_tax_provisions = value
    End Set
  End Property

  Public Property WinnerPrize4Fixed() As Decimal
    Get
      Return m_cd_configuration.winner_prize4_fixed
    End Get

    Set(ByVal value As Decimal)
      m_cd_configuration.winner_prize4_fixed = value
    End Set
  End Property

  Public Property WinnerPrize4Percentage() As Decimal
    Get
      Return m_cd_configuration.winner_prize4_percentage
    End Get

    Set(ByVal value As Decimal)
      m_cd_configuration.winner_prize4_percentage = value
    End Set
  End Property
  Public Property WinnerPrize4AccessPercentage() As Double
    Get
      Return m_cd_configuration.winner_prize4_access_percentage
    End Get

    Set(ByVal value As Double)
      m_cd_configuration.winner_prize4_access_percentage = value
    End Set
  End Property

  Public Property WinnerPrize4Probability() As Decimal
    Get
      Return m_cd_configuration.winner_prize4_probability
    End Get

    Set(ByVal value As Decimal)
      m_cd_configuration.winner_prize4_probability = value
    End Set
  End Property

  Public Property WinnerTaxProvisions4Percentage() As Decimal
    Get
      Return m_cd_configuration.winner_prize4_tax_provisions
    End Get

    Set(ByVal value As Decimal)
      m_cd_configuration.winner_prize4_tax_provisions = value
    End Set
  End Property

  Public Property WinnerPrize1Enabled() As Boolean
    Get
      Return m_cd_configuration.winner_prize1_enabled
    End Get
    Set(ByVal value As Boolean)
      m_cd_configuration.winner_prize1_enabled = value
    End Set
  End Property

  Public Property WinnerPrize2Enabled() As Boolean
    Get
      Return m_cd_configuration.winner_prize2_enabled
    End Get
    Set(ByVal value As Boolean)
      m_cd_configuration.winner_prize2_enabled = value
    End Set
  End Property

  Public Property WinnerPrize3Enabled() As Boolean
    Get
      Return m_cd_configuration.winner_prize3_enabled
    End Get
    Set(ByVal value As Boolean)
      m_cd_configuration.winner_prize3_enabled = value
    End Set
  End Property

  Public Property WinnerPrize4Enabled() As Boolean
    Get
      Return m_cd_configuration.winner_prize4_enabled
    End Get
    Set(ByVal value As Boolean)
      m_cd_configuration.winner_prize4_enabled = value
    End Set
  End Property

  Public Property LoserPrizeEnabled() As Boolean
    Get
      Return m_cd_configuration.loser_prize_enabled
    End Get
    Set(ByVal value As Boolean)
      m_cd_configuration.loser_prize_enabled = value
    End Set
  End Property

  Public Property WinnerPrize1CreditType() As PrizeType
    Get
      Return m_cd_configuration.winner_prize1_credit_type
    End Get
    Set(ByVal value As PrizeType)
      m_cd_configuration.winner_prize1_credit_type = value
    End Set
  End Property

  Public Property WinnerPrize2CreditType() As PrizeType
    Get
      Return m_cd_configuration.winner_prize2_credit_type
    End Get
    Set(ByVal value As PrizeType)
      m_cd_configuration.winner_prize2_credit_type = value
    End Set
  End Property

  Public Property WinnerPrize3CreditType() As PrizeType
    Get
      Return m_cd_configuration.winner_prize3_credit_type
    End Get
    Set(ByVal value As PrizeType)
      m_cd_configuration.winner_prize3_credit_type = value
    End Set
  End Property

  Public Property WinnerPrize4CreditType() As PrizeType
    Get
      Return m_cd_configuration.winner_prize4_credit_type
    End Get
    Set(ByVal value As PrizeType)
      m_cd_configuration.winner_prize4_credit_type = value
    End Set
  End Property

  Public Property LoserCreditType() As PrizeType
    Get
      Return m_cd_configuration.loser_credit_type
    End Get
    Set(ByVal value As PrizeType)
      m_cd_configuration.loser_credit_type = value
    End Set
  End Property

  Public Property WinnerVoucherTitle() As String
    Get
      Return m_cd_configuration.winner_voucher_title
    End Get
    Set(ByVal value As String)
      m_cd_configuration.winner_voucher_title = value
    End Set
  End Property

  Public Property LoserVoucherTitle() As String
    Get
      Return m_cd_configuration.loser_voucher_title
    End Get
    Set(ByVal value As String)
      m_cd_configuration.loser_voucher_title = value
    End Set
  End Property

  Public Property PrizeMode() As CashDeskDrawBusinessLogic.PrizeMode
    Get
      Return m_cd_configuration.prize_mode
    End Get
    Set(ByVal value As CashDeskDrawBusinessLogic.PrizeMode)
      m_cd_configuration.prize_mode = value
    End Set
  End Property

  Public Property ParticipationMaxPrice() As Decimal
    Get
      Return m_cd_configuration.participation_max_price
    End Get
    Set(ByVal value As Decimal)
      m_cd_configuration.participation_max_price = value
    End Set
  End Property
  Public Property TerminalGameUrl() As String
    Get
      Return m_cd_configuration.terminal_game_url
    End Get
    Set(ByVal value As String)
      m_cd_configuration.terminal_game_url = value
    End Set
  End Property
  Public Property TerminalGameName() As String
    Get
      Return m_cd_configuration.terminal_game_name
    End Get
    Set(ByVal value As String)
      m_cd_configuration.terminal_game_name = value
    End Set
  End Property
  Public Property TerminalGameTimeout() As Long
    Get
      Return m_cd_configuration.terminal_game_timeout
    End Get
    Set(ByVal value As Long)
      m_cd_configuration.terminal_game_timeout = value
    End Set
  End Property

  Public Property ParticipationPrice() As Decimal
    Get
      Return m_cd_configuration.participation_price
    End Get
    Set(ByVal value As Decimal)
      m_cd_configuration.participation_price = value
    End Set
  End Property
  Public Property ShowBuyDialog() As Boolean
    Get
      Return m_cd_configuration.show_buy_dialog
    End Get
    Set(ByVal value As Boolean)
      m_cd_configuration.show_buy_dialog = value
    End Set
  End Property
  Public Property PlayerCanCancelDraw() As Boolean
    Get
      Return m_cd_configuration.player_can_cancel_draw
    End Get
    Set(ByVal value As Boolean)
      m_cd_configuration.player_can_cancel_draw = value
    End Set
  End Property
  Public Property GroupDraws() As Long
    Get
      Return m_cd_configuration.terminal_game_group_draws
    End Get
    Set(ByVal value As Long)
      m_cd_configuration.terminal_game_group_draws = value
    End Set
  End Property
  Public Property WithdrawalWithPending() As Boolean
    Get
      Return m_cd_configuration.terminal_game_withdraw_with_pending
    End Get
    Set(value As Boolean)
      m_cd_configuration.terminal_game_withdraw_with_pending = value
    End Set
  End Property
  Public Property CancelWithPending() As Boolean
    Get
      Return m_cd_configuration.terminal_game_cancel_with_pending
    End Get
    Set(value As Boolean)
      m_cd_configuration.terminal_game_cancel_with_pending = value
    End Set
  End Property
#End Region

#Region "Constructors"

  Public Sub New()
    m_draw_configuration = ENUM_DRAW_CONFIGURATION.DRAW_00
  End Sub

  Public Sub New(ByVal DrawConfiguration As ENUM_DRAW_CONFIGURATION)
    m_draw_configuration = DrawConfiguration
  End Sub

#End Region 'Constructors

#Region "Overrides"

  '----------------------------------------------------------------------------
  ' PURPOSE: Returns the related auditor data for the current object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim auditor_data As CLASS_AUDITOR_DATA
    Dim str_value As String
    Dim str_field As String
    Dim str_fixed As String
    Dim str_percentage As String
    Dim str_probability As String
    Dim str_enabled As String
    Dim str_credit_type As String

    auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_CASHIER_CONFIGURATION)
    str_value = ""
    str_field = ""
    str_fixed = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2450) 'Fixed
    str_percentage = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2451) 'Percentage
    str_probability = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7626) 'Probability
    str_enabled = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2525) 'Enabled
    str_credit_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2598) 'Credit type

    'Set name of the auditor data object
    Select Case m_draw_configuration
      Case ENUM_DRAW_CONFIGURATION.DRAW_00
        Call auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(2283), "")
      Case ENUM_DRAW_CONFIGURATION.DRAW_01
        Call auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(7588), "")
      Case ENUM_DRAW_CONFIGURATION.DRAW_02
        Call auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(7823), "")
      Case Else
        Call auditor_data.SetName("", "")

    End Select

    'Set values
    Call auditor_data.SetField(0, IIf(Me.Enabled, GLB_NLS_GUI_AUDITOR.GetString(336), GLB_NLS_GUI_AUDITOR.GetString(337)) _
                                   , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2435))

    Call auditor_data.SetField(0, IIf(Me.ShowCashdeskDraws, GLB_NLS_GUI_AUDITOR.GetString(336), GLB_NLS_GUI_AUDITOR.GetString(337)) _
                                   , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2438))

    Call auditor_data.SetField(0, IIf(Me.VoucherOnCashdeskDrawsWinner, GLB_NLS_GUI_AUDITOR.GetString(336), GLB_NLS_GUI_AUDITOR.GetString(337)) _
                                   , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2439))

    ' 14-FEB-2018  AGS    Fixed Bug 31411:WIGOS-8056 Application crashes when leaving empty a configuration field in Terminal Draw screen
    Call auditor_data.SetField(0, Trim(Me.WinnerVoucherTitle), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2550))

    Call auditor_data.SetField(0, IIf(Me.VoucherOnCashdeskDrawsLoser, GLB_NLS_GUI_AUDITOR.GetString(336), GLB_NLS_GUI_AUDITOR.GetString(337)) _
                                  , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2529))

    ' 14-FEB-2018  AGS    Fixed Bug 31411:WIGOS-8056 Application crashes when leaving empty a configuration field in Terminal Draw screen
    Call auditor_data.SetField(0, Trim(Me.LoserVoucherTitle), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2551))

    Call auditor_data.SetField(0, IIf(Me.AskForParticipation, GLB_NLS_GUI_AUDITOR.GetString(336), GLB_NLS_GUI_AUDITOR.GetString(337)) _
                                   , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2440))

    Select Case Me.WaitNotConnected
      Case ENUM_WAIT_NOT_CONECTED.DONT_RECHARGE
        str_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2442)

      Case ENUM_WAIT_NOT_CONECTED.RECHARGE_REDIMIBLE
        str_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2443)

      Case ENUM_WAIT_NOT_CONECTED.RECHARGE_NONREDIMIBLE
        str_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2444)

    End Select

    str_field = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2441) 'Wait not connected
    Call auditor_data.SetField(0, str_value, str_field)

    Select Case Me.DrawLocation
      Case ENUM_DRAW_LOCATION.LOCAL_DB
        str_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2470) 'On local DB       

      Case ENUM_DRAW_LOCATION.WEBSERVICE_OR_EXTERNALDB
        If Me.ServerDbConnectionString.Trim() = "" Then
          str_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2471) 'On webservice          

        Else
          str_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2472) 'On external DB

        End If

    End Select
    str_field = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2469) 'Draw location
    Call auditor_data.SetField(0, str_value, str_field)

    Call auditor_data.SetField(0, Me.ServerAddress1, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2436))
    Call auditor_data.SetField(0, Me.ServerAddress2, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2437))
    Call auditor_data.SetField(0, Me.ServerDbConnectionString, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2473))

    Call auditor_data.SetField(0, Me.BallsExtracted, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2446))

    Call auditor_data.SetField(0, Me.BallsOfParticipant, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2447))

    Call auditor_data.SetField(0, Me.TotalBallsNumber, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2448))

    str_field = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2449) 'consolation prize
    Call auditor_data.SetField(0, IIf(Me.LoserPrizeEnabled, GLB_NLS_GUI_AUDITOR.GetString(336), GLB_NLS_GUI_AUDITOR.GetString(337)) _
                                  , str_field & "." & str_enabled) 'Enabled
    Call auditor_data.SetField(0, GUI_FormatNumber(Me.LoserPrizeFixed, 2), str_field & "." & str_fixed) 'Fixed
    Call auditor_data.SetField(0, GUI_FormatNumber(Me.LoserPrizePercentage, 2), str_field & "." & str_percentage) 'Percentage
    Call auditor_data.SetField(0, GUI_FormatNumber(Me.LoserPrizeProbability, 2), str_field & "." & str_probability) 'Probability

    Select Case Me.LoserCreditType
      Case PrizeType.NoRedeemable
        str_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2126) 'Non-redeemable

      Case PrizeType.Redeemable
        str_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2125) 'Redeemable

    End Select
    Call auditor_data.SetField(0, str_value, str_field & "." & str_credit_type) 'Credit type

    Call auditor_data.SetField(0, Me.NumberOfParticipants, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2453))

    Call auditor_data.SetField(0, Me.NumberOfWinners, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2454))

    str_field = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2456) 'Prize 1
    Call auditor_data.SetField(0, IIf(Me.WinnerPrize1Enabled, GLB_NLS_GUI_AUDITOR.GetString(336), GLB_NLS_GUI_AUDITOR.GetString(337)) _
                                      , str_field & "." & str_enabled) 'Enabled
    Call auditor_data.SetField(0, GUI_FormatNumber(Me.WinnerPrize1Fixed, 2), str_field & "." & str_fixed) 'Fixed
    Call auditor_data.SetField(0, GUI_FormatNumber(Me.WinnerPrize1Percentage, 2), str_field & "." & str_percentage) 'Percentage
    Call auditor_data.SetField(0, GUI_FormatNumber(Me.WinnerPrize1Probability, 2), str_field & "." & str_probability) 'Probability

    Select Case Me.WinnerPrize1CreditType
      Case PrizeType.NoRedeemable
        str_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2126) 'Non-redeemable

      Case PrizeType.Redeemable
        str_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2125) 'Redeemable

      Case PrizeType.UNR
        str_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2599) 'In kind (UNR)

        'SDS 14-10-2015
      Case PrizeType.ReInKind
        str_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6797) 'In kind (RE)

    End Select
    Call auditor_data.SetField(0, str_value, str_field & "." & str_credit_type) 'Credit type

    str_field = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2457) 'Prize 2
    Call auditor_data.SetField(0, IIf(Me.WinnerPrize2Enabled, GLB_NLS_GUI_AUDITOR.GetString(336), GLB_NLS_GUI_AUDITOR.GetString(337)) _
                                      , str_field & "." & str_enabled) 'Enabled
    Call auditor_data.SetField(0, GUI_FormatNumber(Me.WinnerPrize2Fixed, 2), str_field & "." & str_fixed) 'Fixed
    Call auditor_data.SetField(0, GUI_FormatNumber(Me.WinnerPrize2Percentage, 2), str_field & "." & str_percentage) 'Percentage
    Call auditor_data.SetField(0, GUI_FormatNumber(Me.WinnerPrize2Probability, 2), str_field & "." & str_probability) 'Probability

    Select Case Me.WinnerPrize2CreditType
      Case PrizeType.NoRedeemable
        str_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2126) 'Non-redeemable

      Case PrizeType.Redeemable
        str_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2125) 'Redeemable

      Case PrizeType.UNR
        str_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2599) 'In kind (UNR)

        'SDS 14-10-2015
      Case PrizeType.ReInKind
        str_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6797) 'In kind (RE)

    End Select
    Call auditor_data.SetField(0, str_value, str_field & "." & str_credit_type) 'Credit type

    str_field = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2458) 'Prize 3
    Call auditor_data.SetField(0, IIf(Me.WinnerPrize3Enabled, GLB_NLS_GUI_AUDITOR.GetString(336), GLB_NLS_GUI_AUDITOR.GetString(337)) _
                                      , str_field & "." & str_enabled) 'Enabled
    Call auditor_data.SetField(0, GUI_FormatNumber(Me.WinnerPrize3Fixed, 2), str_field & "." & str_fixed) 'Fixed
    Call auditor_data.SetField(0, GUI_FormatNumber(Me.WinnerPrize3Percentage, 2), str_field & "." & str_percentage) 'Percentage
    Call auditor_data.SetField(0, GUI_FormatNumber(Me.WinnerPrize3Probability, 2), str_field & "." & str_probability) 'Probability

    Select Case Me.WinnerPrize3CreditType
      Case PrizeType.NoRedeemable
        str_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2126) 'Non-redeemable

      Case PrizeType.Redeemable
        str_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2125) 'Redeemable

      Case PrizeType.UNR
        str_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2599) 'In kind (UNR)

        'SDS 14-10-2015
      Case PrizeType.ReInKind
        str_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6797) 'In kind (RE)
    End Select
    Call auditor_data.SetField(0, str_value, str_field & "." & str_credit_type) 'Credit type

    str_field = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2459) 'Prize 4
    Call auditor_data.SetField(0, IIf(Me.WinnerPrize4Enabled, GLB_NLS_GUI_AUDITOR.GetString(336), GLB_NLS_GUI_AUDITOR.GetString(337)) _
                                      , str_field & "." & str_enabled) 'Enabled
    Call auditor_data.SetField(0, GUI_FormatNumber(Me.WinnerPrize4Fixed, 2), str_field & "." & str_fixed) 'Fixed
    Call auditor_data.SetField(0, GUI_FormatNumber(Me.WinnerPrize4Percentage, 2), str_field & "." & str_percentage) 'Percentage
    Call auditor_data.SetField(0, GUI_FormatNumber(Me.WinnerPrize4Probability, 2), str_field & "." & str_probability) 'Probability

    Select Case Me.WinnerPrize4CreditType
      Case PrizeType.NoRedeemable
        str_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2126) 'Non-redeemable

      Case PrizeType.Redeemable
        str_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2125) 'Redeemable

      Case PrizeType.UNR
        str_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2599) 'In kind (UNR)

        'SDS 14-10-2015
      Case PrizeType.ReInKind
        str_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6797) 'In kind (RE)

    End Select
    Call auditor_data.SetField(0, str_value, str_field & "." & str_credit_type) 'Credit type

    Select Case Me.PrizeMode
      Case CashDeskDrawBusinessLogic.PrizeMode.Promotion
        str_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2589) 'Promotion

      Case CashDeskDrawBusinessLogic.PrizeMode.PlayerSession
        str_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2590) 'Player session

      Case CashDeskDrawBusinessLogic.PrizeMode.CashPrize
        str_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7586) 'Cash price

      Case CashDeskDrawBusinessLogic.PrizeMode.Terminal
        str_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7822) 'Cash price

    End Select

    str_field = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2588) 'The prize is added as:

    Call auditor_data.SetField(0, str_value, str_field)

    Call auditor_data.SetField(0, GUI_FormatNumber(Me.ParticipationPrice, 2), GLB_NLS_GUI_PLAYER_TRACKING.GetString(7587))

    Call auditor_data.SetField(0, GUI_FormatNumber(Me.ParticipationMaxPrice, 2), GLB_NLS_GUI_PLAYER_TRACKING.GetString(7888))

    Call auditor_data.SetField(0, Me.TerminalGameUrl, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7889))
    Call auditor_data.SetField(0, Me.TerminalGameName, GLB_NLS_GUI_PLAYER_TRACKING.GetString(253))
    Call auditor_data.SetField(0, Me.TerminalGameTimeout, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7892))

    str_field = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8454)
    Call auditor_data.SetField(0, IIf(Me.ShowBuyDialog, GLB_NLS_GUI_AUDITOR.GetString(336), GLB_NLS_GUI_AUDITOR.GetString(337)) _
                                  , str_field & "." & str_enabled) 'Enabled

    str_field = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8455)
    Call auditor_data.SetField(0, IIf(Me.PlayerCanCancelDraw, GLB_NLS_GUI_AUDITOR.GetString(336), GLB_NLS_GUI_AUDITOR.GetString(337)) _
                                  , str_field & "." & str_enabled) 'Enabled

    str_field = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8763)
    Call auditor_data.SetField(0, IIf(Me.WithdrawalWithPending, GLB_NLS_GUI_AUDITOR.GetString(336), GLB_NLS_GUI_AUDITOR.GetString(337)) _
                                  , str_field & "." & str_enabled) 'Enabled

    str_field = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8762)
    Call auditor_data.SetField(0, IIf(Me.CancelWithPending, GLB_NLS_GUI_AUDITOR.GetString(336), GLB_NLS_GUI_AUDITOR.GetString(337)) _
                                  , str_field & "." & str_enabled) 'Enabled

    str_field = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8764)
    Call auditor_data.SetField(0, Me.GroupDraws, str_field)


    Return auditor_data

  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE: Can't delete the given object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_NOT_SUPPORTED
  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE: Can't insert the given object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_NOT_SUPPORTED
  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE: Reads from the database into the given object
  '
  ' PARAMS:
  '   - INPUT:
  '     - ObjectId: An object, it must be 'casted' to the desired KEY.
  '
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As ENUM_STATUS

    ' Read Cash Desk Draws configuration parameters information
    _rc = CashdeskDrawsConf_DbRead()

    Select Case _rc
      Case ENUM_DB_RC.DB_RC_OK
        Return ENUM_STATUS.STATUS_OK

      Case ENUM_DB_RC.DB_RC_ERROR_DB
        Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE: Updates the given object to the database.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As ENUM_STATUS

    Using _db_trx As New DB_TRX()

      _rc = CashdeskDrawsConf_DbUpdate(_db_trx.SqlTransaction)

      If _rc <> ENUM_STATUS.STATUS_OK Then
        Call _db_trx.Rollback()
      Else
        _rc = IIf(_db_trx.Commit() = True, ENUM_STATUS.STATUS_OK, ENUM_STATUS.STATUS_ERROR)
      End If

    End Using ' _db_trx

    Return _rc

  End Function 'DB_Update

  '----------------------------------------------------------------------------
  ' PURPOSE: Returns a duplicate of the given object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim _dup_cashdesk_draws_conf As CLASS_CASHDESK_DRAWS_CONFIGURATION

    _dup_cashdesk_draws_conf = New CLASS_CASHDESK_DRAWS_CONFIGURATION(m_draw_configuration)

    With _dup_cashdesk_draws_conf
      .Enabled = Me.Enabled
      .ShowCashdeskDraws = Me.ShowCashdeskDraws
      .VoucherOnCashdeskDrawsWinner = Me.VoucherOnCashdeskDrawsWinner
      .WinnerVoucherTitle = Me.WinnerVoucherTitle
      .VoucherOnCashdeskDrawsLoser = Me.VoucherOnCashdeskDrawsLoser
      .LoserVoucherTitle = Me.LoserVoucherTitle
      .AskForParticipation = Me.AskForParticipation
      .WaitNotConnected = Me.WaitNotConnected
      .DrawLocation = Me.DrawLocation
      .ServerAddress1 = Me.ServerAddress1
      .ServerAddress2 = Me.ServerAddress2
      .ServerDbConnectionString = Me.ServerDbConnectionString
      .BallsExtracted = Me.BallsExtracted
      .BallsOfParticipant = Me.BallsOfParticipant
      .TotalBallsNumber = Me.TotalBallsNumber
      .LoserPrizeFixed = Me.LoserPrizeFixed
      .LoserPrizePercentage = Me.LoserPrizePercentage
      .LoserPrizeProbability = Me.LoserPrizeProbability
      .LoserPrizeAccessPercentage = Me.LoserPrizeAccessPercentage
      .LoserTaxProvisionsPercentage = Me.LoserTaxProvisionsPercentage
      .TaxProvisionsEnable = Me.TaxProvisionsEnable
      .LoserPrizeEnabled = Me.LoserPrizeEnabled
      .NumberOfParticipants = Me.NumberOfParticipants
      .NumberOfWinners = Me.NumberOfWinners
      .WinnerPrize1Fixed = Me.WinnerPrize1Fixed
      .WinnerPrize1Percentage = Me.WinnerPrize1Percentage
      .WinnerPrize1Probability = Me.WinnerPrize1Probability
      .WinnerPrize1AccessPercentage = Me.WinnerPrize1AccessPercentage
      .WinnerTaxProvisions1Percentage = Me.WinnerTaxProvisions1Percentage
      .WinnerPrize1Enabled = Me.WinnerPrize1Enabled
      .WinnerPrize1CreditType = Me.WinnerPrize1CreditType
      .WinnerPrize2Fixed = Me.WinnerPrize2Fixed
      .WinnerPrize2Percentage = Me.WinnerPrize2Percentage
      .WinnerPrize2Probability = Me.WinnerPrize2Probability
      .WinnerPrize2AccessPercentage = Me.WinnerPrize2AccessPercentage
      .WinnerTaxProvisions2Percentage = Me.WinnerTaxProvisions2Percentage
      .WinnerPrize2Enabled = Me.WinnerPrize2Enabled
      .WinnerPrize2CreditType = Me.WinnerPrize2CreditType
      .WinnerPrize3Fixed = Me.WinnerPrize3Fixed
      .WinnerPrize3Percentage = Me.WinnerPrize3Percentage
      .WinnerPrize3Probability = Me.WinnerPrize3Probability
      .WinnerPrize3AccessPercentage = Me.WinnerPrize3AccessPercentage
      .WinnerTaxProvisions3Percentage = Me.WinnerTaxProvisions3Percentage
      .WinnerPrize3Enabled = Me.WinnerPrize3Enabled
      .WinnerPrize3CreditType = Me.WinnerPrize3CreditType
      .WinnerPrize4Fixed = Me.WinnerPrize4Fixed
      .WinnerPrize4Percentage = Me.WinnerPrize4Percentage
      .WinnerPrize4Probability = Me.WinnerPrize4Probability
      .WinnerPrize4AccessPercentage = Me.WinnerPrize4AccessPercentage
      .WinnerTaxProvisions4Percentage = Me.WinnerTaxProvisions4Percentage
      .WinnerPrize4Enabled = Me.WinnerPrize4Enabled
      .WinnerPrize4CreditType = Me.WinnerPrize4CreditType
      .LoserCreditType = Me.LoserCreditType
      .PrizeMode = Me.PrizeMode
      .ParticipationPrice = Me.ParticipationPrice
      .TerminalGameUrl = Me.TerminalGameUrl
      .TerminalGameName = Me.TerminalGameName
      .TerminalGameTimeout = Me.TerminalGameTimeout
      .ParticipationMaxPrice = Me.ParticipationMaxPrice
      .PlayerCanCancelDraw = Me.PlayerCanCancelDraw
      .ShowBuyDialog = Me.ShowBuyDialog
      .GroupDraws = Me.GroupDraws
      .WithdrawalWithPending = Me.WithdrawalWithPending
      .CancelWithPending = Me.CancelWithPending
    End With

    Return _dup_cashdesk_draws_conf

  End Function 'Duplicate

#End Region 'Overrides

#Region "Private functions"
  '----------------------------------------------------------------------------
  ' PURPOSE: Calls the cashDesk_draws_Configuration_DbRead and checks if the database result is ok
  '                                              
  ' PARAMS:
  '   - INPUT: 
  '   - OUTPUT:
  '
  ' RETURNS:
  '
  ' NOTES:
  Private Function CashdeskDrawsConf_DbRead() As Integer

    Dim _db_trx_status As ENUM_DB_RC
    Dim _general_params As WSI.Common.GeneralParam.Dictionary
    Dim _acces_perc_value As Decimal
    Dim _tax_provisions_perc As Decimal
    Dim _tax_provisions_enable As Boolean
    Dim _prize_types As List(Of PrizeType)
    Dim _loser_prize_types As List(Of PrizeType)
    Dim _prize_modes As List(Of CashDeskDrawBusinessLogic.PrizeMode)
    Dim _split_data As New WSI.Common.TYPE_SPLITS
    Dim _id As Integer
    Dim _postFij As String

    _db_trx_status = ENUM_DB_RC.DB_RC_OK

    _prize_types = New List(Of PrizeType)(New PrizeType() {PrizeType.Redeemable, PrizeType.NoRedeemable, PrizeType.UNR, PrizeType.ReInKind})
    _loser_prize_types = New List(Of PrizeType)(New PrizeType() {PrizeType.Redeemable, PrizeType.NoRedeemable})
    _prize_modes = New List(Of CashDeskDrawBusinessLogic.PrizeMode)(New CashDeskDrawBusinessLogic.PrizeMode() {CashDeskDrawBusinessLogic.PrizeMode.Promotion, CashDeskDrawBusinessLogic.PrizeMode.PlayerSession, CashDeskDrawBusinessLogic.PrizeMode.CashPrize, CashDeskDrawBusinessLogic.PrizeMode.Terminal})

    Try
      _general_params = WSI.Common.GeneralParam.Dictionary.Create()

      'LTC 14-SEP-2016
      _id = CType(m_draw_configuration, Integer)
      _postFij = vbNullString

      DB_CheckCashDreskDrawConfig(_id, "")

      If _id <> 0 Then
        _postFij = "." & _id.ToString("00").PadLeft(2)
      End If

      'LTC 22-SEP-2016
      'CashDesk.Draw gp_group
      Me.Enabled = _general_params.GetBoolean("CashDesk.Draw" + _postFij, "Enabled", False)
      Me.ShowCashdeskDraws = _general_params.GetBoolean("CashDesk.Draw" + _postFij, "ShowCashDeskDraws", False)
      Me.VoucherOnCashdeskDrawsWinner = _general_params.GetBoolean("CashDesk.Draw" + _postFij, "VoucherOnCashDeskDraws.Winner", False)
      Me.WinnerVoucherTitle = _general_params.GetString("CashDesk.Draw" + _postFij, "WinnerPrizeVoucherTitle")
      Me.VoucherOnCashdeskDrawsLoser = _general_params.GetBoolean("CashDesk.Draw" + _postFij, "VoucherOnCashDeskDraws.Loser", False)
      Me.AskForParticipation = _general_params.GetBoolean("CashDesk.Draw" + _postFij, "AskForParticipation", False)
      Me.LoserVoucherTitle = _general_params.GetString("CashDesk.Draw" + _postFij, "LoserPrizeVoucherTitle")
      Me.WaitNotConnected = _general_params.GetInt32("CashDesk.Draw" + _postFij, "ActionOnServerDown")

      Me.DrawLocation = _general_params.GetInt32("CashDesk.Draw" + _postFij, "LocalServer")
      Me.ServerDbConnectionString = _general_params.GetString("CashDesk.Draw" + _postFij, "ServerBDConnectionString")
      Select Case Me.DrawLocation
        Case ENUM_DRAW_LOCATION.LOCAL_DB
          Me.ServerAddress1 = ""
          Me.ServerAddress2 = ""
          Me.WaitNotConnected = ENUM_WAIT_NOT_CONECTED.DONT_RECHARGE

        Case ENUM_DRAW_LOCATION.WEBSERVICE_OR_EXTERNALDB
          If Me.ServerDbConnectionString.Trim() = "" Then
            'On webservice          
            Me.ServerAddress1 = _general_params.GetString("CashDesk.Draw" + _postFij, "ServerAddress1")
            Me.ServerAddress2 = _general_params.GetString("CashDesk.Draw" + _postFij, "ServerAddress2")

          Else
            'On external DB
            Me.ServerAddress1 = ""
            Me.ServerAddress2 = ""

          End If

        Case Else
          Me.DrawLocation = ENUM_DRAW_LOCATION.LOCAL_DB
          Me.ServerAddress1 = ""
          Me.ServerAddress2 = ""

      End Select

      'Drawconfiguration gp_group
      Me.BallsExtracted = _general_params.GetInt64("CashDesk.DrawConfiguration" + _postFij, "BallsExtracted")
      Me.BallsOfParticipant = _general_params.GetInt64("CashDesk.DrawConfiguration" + _postFij, "BallsOfParticipant")
      Me.TotalBallsNumber = _general_params.GetInt64("CashDesk.DrawConfiguration" + _postFij, "TotalsBallsNumber")

      Me.LoserPrizeFixed = _general_params.GetDecimal("CashDesk.DrawConfiguration" + _postFij, "LoserPrize1.Fixed")
      Me.LoserPrizePercentage = _general_params.GetDecimal("CashDesk.DrawConfiguration" + _postFij, "LoserPrize1.Percentage")
      Me.LoserPrizeEnabled = _general_params.GetBoolean("CashDesk.DrawConfiguration" + _postFij, "LoserPrize1.Enabled", False)
      Me.LoserPrizeProbability = _general_params.GetDecimal("CashDesk.DrawConfiguration" + _postFij, "LoserPrize1.Probability", 0)

      Me.NumberOfParticipants = _general_params.GetInt32("CashDesk.DrawConfiguration" + _postFij, "NumberOfParticipants")
      Me.NumberOfWinners = _general_params.GetInt32("CashDesk.DrawConfiguration" + _postFij, "NumberOfWinners")

      Me.WinnerPrize1Fixed = _general_params.GetDecimal("CashDesk.DrawConfiguration" + _postFij, "WinnerPrize1.Fixed")
      Me.WinnerPrize1Percentage = _general_params.GetDecimal("CashDesk.DrawConfiguration" + _postFij, "WinnerPrize1.Percentage")
      Me.WinnerPrize1Enabled = _general_params.GetBoolean("CashDesk.DrawConfiguration" + _postFij, "WinnerPrize1.Enabled", False)
      Me.WinnerPrize1Probability = _general_params.GetDecimal("CashDesk.DrawConfiguration" + _postFij, "WinnerPrize1.Probability", 0)

      Me.WinnerPrize2Fixed = _general_params.GetDecimal("CashDesk.DrawConfiguration" + _postFij, "WinnerPrize2.Fixed")
      Me.WinnerPrize2Percentage = _general_params.GetDecimal("CashDesk.DrawConfiguration" + _postFij, "WinnerPrize2.Percentage")
      Me.WinnerPrize2Enabled = _general_params.GetBoolean("CashDesk.DrawConfiguration" + _postFij, "WinnerPrize2.Enabled", False)
      Me.WinnerPrize2Probability = _general_params.GetDecimal("CashDesk.DrawConfiguration" + _postFij, "WinnerPrize2.Probability", 0)

      Me.WinnerPrize3Fixed = _general_params.GetDecimal("CashDesk.DrawConfiguration" + _postFij, "WinnerPrize3.Fixed")
      Me.WinnerPrize3Percentage = _general_params.GetDecimal("CashDesk.DrawConfiguration" + _postFij, "WinnerPrize3.Percentage")
      Me.WinnerPrize3Enabled = _general_params.GetBoolean("CashDesk.DrawConfiguration" + _postFij, "WinnerPrize3.Enabled", False)
      Me.WinnerPrize3Probability = _general_params.GetDecimal("CashDesk.DrawConfiguration" + _postFij, "WinnerPrize3.Probability", 0)

      Me.WinnerPrize4Fixed = _general_params.GetDecimal("CashDesk.DrawConfiguration" + _postFij, "WinnerPrize4.Fixed")
      Me.WinnerPrize4Percentage = _general_params.GetDecimal("CashDesk.DrawConfiguration" + _postFij, "WinnerPrize4.Percentage")
      Me.WinnerPrize4Enabled = _general_params.GetBoolean("CashDesk.DrawConfiguration" + _postFij, "WinnerPrize4.Enabled", False)
      Me.WinnerPrize4Probability = _general_params.GetDecimal("CashDesk.DrawConfiguration" + _postFij, "WinnerPrize4.Probability", 0)

      WSI.Common.Split.ReadSplitParameters(_split_data)

      If Not _split_data.enabled_b Then
        _acces_perc_value = 0
      Else
        _acces_perc_value = _split_data.company_b.cashin_pct
        If m_draw_configuration = ENUM_DRAW_CONFIGURATION.DRAW_01 Then
          _acces_perc_value = _split_data.company_b.chips_sale_pct
        End If
      End If

      Me.WinnerPrize1AccessPercentage = _acces_perc_value
      Me.WinnerPrize2AccessPercentage = _acces_perc_value
      Me.WinnerPrize3AccessPercentage = _acces_perc_value
      Me.WinnerPrize4AccessPercentage = _acces_perc_value
      Me.LoserPrizeAccessPercentage = _acces_perc_value

      _tax_provisions_enable = WSI.Common.Misc.IsTaxProvisionsEnabled()
      Me.TaxProvisionsEnable = _tax_provisions_enable

      If _tax_provisions_enable Then
        _tax_provisions_perc = _general_params.GetDecimal("Cashier.TaxProvisions", "Pct", 0)
      Else
        _tax_provisions_perc = 0.0
      End If

      Me.LoserTaxProvisionsPercentage = _tax_provisions_perc
      Me.WinnerTaxProvisions1Percentage = _tax_provisions_perc
      Me.WinnerTaxProvisions2Percentage = _tax_provisions_perc
      Me.WinnerTaxProvisions3Percentage = _tax_provisions_perc
      Me.WinnerTaxProvisions4Percentage = _tax_provisions_perc

      Me.WinnerPrize1CreditType = _general_params.GetInt32("CashDesk.DrawConfiguration" + _postFij, "WinnerPrize1.PrizeType")
      If Not _prize_types.Contains(Me.WinnerPrize1CreditType) Then
        Me.WinnerPrize1CreditType = PrizeType.Redeemable

      End If

      Me.WinnerPrize2CreditType = _general_params.GetInt32("CashDesk.DrawConfiguration" + _postFij, "WinnerPrize2.PrizeType")
      If Not _prize_types.Contains(Me.WinnerPrize2CreditType) Then
        Me.WinnerPrize2CreditType = PrizeType.Redeemable

      End If

      Me.WinnerPrize3CreditType = _general_params.GetInt32("CashDesk.DrawConfiguration" + _postFij, "WinnerPrize3.PrizeType")
      If Not _prize_types.Contains(Me.WinnerPrize3CreditType) Then
        Me.WinnerPrize3CreditType = PrizeType.Redeemable

      End If

      Me.WinnerPrize4CreditType = _general_params.GetInt32("CashDesk.DrawConfiguration" + _postFij, "WinnerPrize4.PrizeType")
      If Not _prize_types.Contains(Me.WinnerPrize4CreditType) Then
        Me.WinnerPrize4CreditType = PrizeType.Redeemable

      End If

      Me.LoserCreditType = _general_params.GetInt32("CashDesk.DrawConfiguration" + _postFij, "LoserPrize1.PrizeType")
      If Not _loser_prize_types.Contains(Me.LoserCreditType) Then
        Me.LoserCreditType = PrizeType.Redeemable

      End If

      Me.PrizeMode = _general_params.GetInt32("CashDesk.Draw" + _postFij, "AccountingMode")
      If Not _prize_modes.Contains(Me.PrizeMode) Then
        Me.PrizeMode = CashDeskDrawBusinessLogic.PrizeMode.Promotion

      End If

      Me.ParticipationPrice = _general_params.GetDecimal("CashDesk.DrawConfiguration" + _postFij, "ParticipationPrice", 0)
      Me.ParticipationMaxPrice = _general_params.GetDecimal("CashDesk.DrawConfiguration" + _postFij, "ParticipationMaxPrice", 0)

      Me.TerminalGameUrl = _general_params.GetString("CashDesk.DrawConfiguration" + _postFij, "TerminalGameUrl", "")
      Me.TerminalGameName = _general_params.GetString("CashDesk.DrawConfiguration" + _postFij, "TerminalGameName", "")
      Me.TerminalGameTimeout = _general_params.GetInt64("CashDesk.DrawConfiguration" + _postFij, "TerminalGameTimeout", 0)
      Me.PlayerCanCancelDraw = _general_params.GetBoolean("CashDesk.DrawConfiguration" + _postFij, "PlayerCanCancelDraw", False)
      Me.ShowBuyDialog = _general_params.GetBoolean("CashDesk.DrawConfiguration" + _postFij, "ShowTheBuyDialog", False)

      Me.GroupDraws = _general_params.GetInt32("CashDesk.Draw.02", "GroupDraws", 1, 1, 20)
      Me.WithdrawalWithPending = _general_params.GetBoolean("CashDesk.Draw.02", "LetWithdrawalWithPendingDraw", False)
      Me.CancelWithPending = _general_params.GetBoolean("CashDesk.Draw.02", "LetCancelRechargeWithPendingDraw", False)

    Catch _ex As Exception
      Return ENUM_STATUS.STATUS_ERROR

    End Try

    Return _db_trx_status

  End Function ' CashdeskDrawsConf_DbRead

  '----------------------------------------------------------------------------
  ' PURPOSE: Update the cashdesk draws configuration object and returns the status of db transaction 
  '                                              
  ' PARAMS:
  '   - INPUT: 
  '   - OUTPUT:
  '
  ' RETURNS:
  '
  ' NOTES:
  Private Function CashdeskDrawsConf_DbUpdate(ByVal Trx As SqlTransaction) As Integer

    Try
      'LTC 14-SEP-2016
      Dim _id As Integer
      Dim _postFij As String

      _id = CType(m_draw_configuration, Integer)
      _postFij = vbNullString

      If _id <> 0 Then
        _postFij = "." & _id.ToString("00").PadLeft(2)
      End If
      'LTC 22-SEP-2016
      If Not DB_GeneralParam_Update("CashDesk.Draw" + _postFij, "Enabled", IIf(Me.Enabled, "1", "0"), Trx) Then
        Return ENUM_DB_RC.DB_RC_ERROR_DB

      End If

      If _id = 2 Then
        If Not DB_GeneralParam_Update("CashDesk.Draw" + _postFij, "GroupDraws", Me.GroupDraws, Trx) Then
          Return ENUM_DB_RC.DB_RC_ERROR_DB
        End If

        If Not DB_GeneralParam_Update("CashDesk.Draw" + _postFij, "LetWithdrawalWithPendingDraw", IIf(Me.WithdrawalWithPending, "1", "0"), Trx) Then
          Return ENUM_DB_RC.DB_RC_ERROR_DB
        End If

        If Not DB_GeneralParam_Update("CashDesk.Draw" + _postFij, "LetCancelRechargeWithPendingDraw", IIf(Me.CancelWithPending, "1", "0"), Trx) Then
          Return ENUM_DB_RC.DB_RC_ERROR_DB
        End If
      End If

      'Enable/disable virtual terminal
      If Not DB_VirtualTerminal_Update(Trx) Then
        Return ENUM_DB_RC.DB_RC_ERROR_DB

      End If

      If Not DB_GeneralParam_Update("CashDesk.Draw" + _postFij, "ShowCashDeskDraws", IIf(Me.ShowCashdeskDraws, "1", "0"), Trx) Then
        Return ENUM_DB_RC.DB_RC_ERROR_DB

      End If

      If Not DB_GeneralParam_Update("CashDesk.Draw" + _postFij, "VoucherOnCashDeskDraws.Winner", IIf(Me.VoucherOnCashdeskDrawsWinner, "1", "0"), Trx) Then
        Return ENUM_DB_RC.DB_RC_ERROR_DB

      End If

      If Not DB_GeneralParam_Update("CashDesk.Draw" + _postFij, "WinnerPrizeVoucherTitle", Me.WinnerVoucherTitle, Trx) Then
        Return ENUM_DB_RC.DB_RC_ERROR_DB

      End If

      If Not DB_GeneralParam_Update("CashDesk.Draw" + _postFij, "VoucherOnCashDeskDraws.Loser", IIf(Me.VoucherOnCashdeskDrawsLoser, "1", "0"), Trx) Then
        Return ENUM_DB_RC.DB_RC_ERROR_DB

      End If

      If Not DB_GeneralParam_Update("CashDesk.Draw" + _postFij, "LoserPrizeVoucherTitle", Me.LoserVoucherTitle, Trx) Then
        Return ENUM_DB_RC.DB_RC_ERROR_DB

      End If

      If Not DB_GeneralParam_Update("CashDesk.Draw" + _postFij, "AskForParticipation", IIf(Me.AskForParticipation, "1", "0"), Trx) Then
        Return ENUM_DB_RC.DB_RC_ERROR_DB

      End If

      If Not DB_GeneralParam_Update("CashDesk.Draw" + _postFij, "ActionOnServerDown", CType(Me.WaitNotConnected, Integer).ToString(), Trx) Then
        Return ENUM_DB_RC.DB_RC_ERROR_DB

      End If

      If Not DB_GeneralParam_Update("CashDesk.Draw" + _postFij, "LocalServer", Me.DrawLocation, Trx) Then
        Return ENUM_DB_RC.DB_RC_ERROR_DB

      End If

      If Not DB_GeneralParam_Update("CashDesk.Draw" + _postFij, "ServerAddress1", Me.ServerAddress1, Trx) Then
        Return ENUM_DB_RC.DB_RC_ERROR_DB

      End If

      If Not DB_GeneralParam_Update("CashDesk.Draw" + _postFij, "ServerAddress2", Me.ServerAddress2, Trx) Then
        Return ENUM_DB_RC.DB_RC_ERROR_DB

      End If

      If Not DB_GeneralParam_Update("CashDesk.Draw" + _postFij, "ServerBDConnectionString", Me.ServerDbConnectionString, Trx) Then
        Return ENUM_DB_RC.DB_RC_ERROR_DB

      End If

      If Not DB_GeneralParam_Update("CashDesk.Drawconfiguration" + _postFij, "BallsExtracted", Me.BallsExtracted.ToString(), Trx) Then
        Return ENUM_DB_RC.DB_RC_ERROR_DB

      End If

      If Not DB_GeneralParam_Update("CashDesk.Drawconfiguration" + _postFij, "BallsOfParticipant", Me.BallsOfParticipant.ToString(), Trx) Then
        Return ENUM_DB_RC.DB_RC_ERROR_DB

      End If

      If Not DB_GeneralParam_Update("CashDesk.Drawconfiguration" + _postFij, "TotalsBallsNumber", Me.TotalBallsNumber.ToString(), Trx) Then
        Return ENUM_DB_RC.DB_RC_ERROR_DB

      End If

      If Not DB_GeneralParam_Update("CashDesk.Drawconfiguration" + _postFij, "LoserPrize1.Fixed", GUI_LocalNumberToDBNumber(Me.LoserPrizeFixed.ToString()), Trx) Then
        Return ENUM_DB_RC.DB_RC_ERROR_DB

      End If

      If Not DB_GeneralParam_Update("CashDesk.Drawconfiguration" + _postFij, "LoserPrize1.Percentage", GUI_LocalNumberToDBNumber(Me.LoserPrizePercentage.ToString()), Trx) Then
        Return ENUM_DB_RC.DB_RC_ERROR_DB

      End If

      If Not DB_GeneralParam_Update("CashDesk.Drawconfiguration" + _postFij, "LoserPrize1.Probability", GUI_LocalNumberToDBNumber(Me.LoserPrizeProbability.ToString()), Trx) Then
        Return ENUM_DB_RC.DB_RC_ERROR_DB

      End If

      If Not DB_GeneralParam_Update("CashDesk.Drawconfiguration" + _postFij, "LoserPrize1.Enabled", IIf(Me.LoserPrizeEnabled, "1", "0"), Trx) Then
        Return ENUM_DB_RC.DB_RC_ERROR_DB

      End If

      If Not DB_GeneralParam_Update("CashDesk.Drawconfiguration" + _postFij, "NumberOfParticipants", Me.NumberOfParticipants.ToString(), Trx) Then
        Return ENUM_DB_RC.DB_RC_ERROR_DB

      End If

      If Not DB_GeneralParam_Update("CashDesk.Drawconfiguration" + _postFij, "NumberOfWinners", Me.NumberOfWinners.ToString(), Trx) Then
        Return ENUM_DB_RC.DB_RC_ERROR_DB

      End If

      If Not DB_GeneralParam_Update("CashDesk.Drawconfiguration" + _postFij, "WinnerPrize1.Fixed", GUI_LocalNumberToDBNumber(Me.WinnerPrize1Fixed.ToString()), Trx) Then
        Return ENUM_DB_RC.DB_RC_ERROR_DB

      End If

      If Not DB_GeneralParam_Update("CashDesk.Drawconfiguration" + _postFij, "WinnerPrize1.Percentage", GUI_LocalNumberToDBNumber(Me.WinnerPrize1Percentage.ToString()), Trx) Then
        Return ENUM_DB_RC.DB_RC_ERROR_DB

      End If

      If Not DB_GeneralParam_Update("CashDesk.Drawconfiguration" + _postFij, "WinnerPrize1.Probability", GUI_LocalNumberToDBNumber(Me.WinnerPrize1Probability.ToString()), Trx) Then
        Return ENUM_DB_RC.DB_RC_ERROR_DB

      End If

      If Not DB_GeneralParam_Update("CashDesk.Drawconfiguration" + _postFij, "WinnerPrize1.Enabled", IIf(Me.WinnerPrize1Enabled, "1", "0"), Trx) Then
        Return ENUM_DB_RC.DB_RC_ERROR_DB

      End If

      If Not DB_GeneralParam_Update("CashDesk.Drawconfiguration" + _postFij, "WinnerPrize2.Fixed", GUI_LocalNumberToDBNumber(Me.WinnerPrize2Fixed.ToString()), Trx) Then
        Return ENUM_DB_RC.DB_RC_ERROR_DB

      End If

      If Not DB_GeneralParam_Update("CashDesk.Drawconfiguration" + _postFij, "WinnerPrize2.Percentage", GUI_LocalNumberToDBNumber(Me.WinnerPrize2Percentage.ToString()), Trx) Then
        Return ENUM_DB_RC.DB_RC_ERROR_DB

      End If

      If Not DB_GeneralParam_Update("CashDesk.Drawconfiguration" + _postFij, "WinnerPrize2.Probability", GUI_LocalNumberToDBNumber(Me.WinnerPrize2Probability.ToString()), Trx) Then
        Return ENUM_DB_RC.DB_RC_ERROR_DB

      End If

      If Not DB_GeneralParam_Update("CashDesk.Drawconfiguration" + _postFij, "WinnerPrize2.Enabled", IIf(Me.WinnerPrize2Enabled, "1", "0"), Trx) Then
        Return ENUM_DB_RC.DB_RC_ERROR_DB

      End If

      If Not DB_GeneralParam_Update("CashDesk.Drawconfiguration" + _postFij, "WinnerPrize3.Fixed", GUI_LocalNumberToDBNumber(Me.WinnerPrize3Fixed.ToString()), Trx) Then
        Return ENUM_DB_RC.DB_RC_ERROR_DB

      End If

      If Not DB_GeneralParam_Update("CashDesk.Drawconfiguration" + _postFij, "WinnerPrize3.Percentage", GUI_LocalNumberToDBNumber(Me.WinnerPrize3Percentage.ToString()), Trx) Then
        Return ENUM_DB_RC.DB_RC_ERROR_DB

      End If

      If Not DB_GeneralParam_Update("CashDesk.Drawconfiguration" + _postFij, "WinnerPrize3.Probability", GUI_LocalNumberToDBNumber(Me.WinnerPrize3Probability.ToString()), Trx) Then
        Return ENUM_DB_RC.DB_RC_ERROR_DB

      End If

      If Not DB_GeneralParam_Update("CashDesk.Drawconfiguration" + _postFij, "WinnerPrize3.Enabled", IIf(Me.WinnerPrize3Enabled, "1", "0"), Trx) Then
        Return ENUM_DB_RC.DB_RC_ERROR_DB

      End If

      If Not DB_GeneralParam_Update("CashDesk.Drawconfiguration" + _postFij, "WinnerPrize4.Fixed", GUI_LocalNumberToDBNumber(Me.WinnerPrize4Fixed.ToString()), Trx) Then
        Return ENUM_DB_RC.DB_RC_ERROR_DB

      End If

      If Not DB_GeneralParam_Update("CashDesk.Drawconfiguration" + _postFij, "WinnerPrize4.Percentage", GUI_LocalNumberToDBNumber(Me.WinnerPrize4Percentage.ToString()), Trx) Then
        Return ENUM_DB_RC.DB_RC_ERROR_DB

      End If

      If Not DB_GeneralParam_Update("CashDesk.Drawconfiguration" + _postFij, "WinnerPrize4.Probability", GUI_LocalNumberToDBNumber(Me.WinnerPrize4Probability.ToString()), Trx) Then
        Return ENUM_DB_RC.DB_RC_ERROR_DB

      End If

      If Not DB_GeneralParam_Update("CashDesk.Drawconfiguration" + _postFij, "WinnerPrize4.Enabled", IIf(Me.WinnerPrize4Enabled, "1", "0"), Trx) Then
        Return ENUM_DB_RC.DB_RC_ERROR_DB

      End If

      If Not DB_GeneralParam_Update("CashDesk.Drawconfiguration" + _postFij, "WinnerPrize1.PrizeType", CType(Me.WinnerPrize1CreditType, Integer).ToString(), Trx) Then
        Return ENUM_DB_RC.DB_RC_ERROR_DB

      End If

      If Not DB_GeneralParam_Update("CashDesk.Drawconfiguration" + _postFij, "WinnerPrize2.PrizeType", CType(Me.WinnerPrize2CreditType, Integer).ToString(), Trx) Then
        Return ENUM_DB_RC.DB_RC_ERROR_DB

      End If

      If Not DB_GeneralParam_Update("CashDesk.Drawconfiguration" + _postFij, "WinnerPrize3.PrizeType", CType(Me.WinnerPrize3CreditType, Integer).ToString(), Trx) Then
        Return ENUM_DB_RC.DB_RC_ERROR_DB

      End If

      If Not DB_GeneralParam_Update("CashDesk.Drawconfiguration" + _postFij, "WinnerPrize4.PrizeType", CType(Me.WinnerPrize4CreditType, Integer).ToString(), Trx) Then
        Return ENUM_DB_RC.DB_RC_ERROR_DB

      End If

      If Not DB_GeneralParam_Update("CashDesk.Drawconfiguration" + _postFij, "LoserPrize1.PrizeType", CType(Me.LoserCreditType, Integer).ToString(), Trx) Then
        Return ENUM_DB_RC.DB_RC_ERROR_DB

      End If

      If Not DB_GeneralParam_Update("CashDesk.Draw" + _postFij, "AccountingMode", CType(Me.PrizeMode, Integer).ToString(), Trx) Then
        Return ENUM_DB_RC.DB_RC_ERROR_DB

      End If

      If Not DB_GeneralParam_Update("CashDesk.DrawConfiguration" + _postFij, "ParticipationPrice", GUI_LocalNumberToDBNumber(Me.ParticipationPrice.ToString()), Trx) Then
        Return ENUM_DB_RC.DB_RC_ERROR_DB

      End If
      If Not DB_GeneralParam_Update("CashDesk.DrawConfiguration" + _postFij, "ParticipationMaxPrice", GUI_LocalNumberToDBNumber(Me.ParticipationMaxPrice.ToString()), Trx) Then
        Return ENUM_DB_RC.DB_RC_ERROR_DB

      End If
      If Not DB_GeneralParam_Update("CashDesk.DrawConfiguration" + _postFij, "TerminalGameUrl", Me.TerminalGameUrl.ToString(), Trx) Then
        Return ENUM_DB_RC.DB_RC_ERROR_DB

      End If
      If Not DB_GeneralParam_Update("CashDesk.DrawConfiguration" + _postFij, "TerminalGameName", Me.TerminalGameName.ToString(), Trx) Then
        Return ENUM_DB_RC.DB_RC_ERROR_DB

      End If
      If Not DB_GeneralParam_Update("CashDesk.DrawConfiguration" + _postFij, "TerminalGameTimeout", CType(Me.TerminalGameTimeout, Long).ToString(), Trx) Then
        Return ENUM_DB_RC.DB_RC_ERROR_DB

      End If

      If Not DB_GeneralParam_Update("CashDesk.DrawConfiguration" + _postFij, "ShowTheBuyDialog", IIf(Me.ShowBuyDialog, "1", "0"), Trx) Then
        Return ENUM_DB_RC.DB_RC_ERROR_DB

      End If
      If Not DB_GeneralParam_Update("CashDesk.DrawConfiguration" + _postFij, "PlayerCanCancelDraw", IIf(Me.PlayerCanCancelDraw, "1", "0"), Trx) Then
        Return ENUM_DB_RC.DB_RC_ERROR_DB

      End If
      Return ENUM_DB_RC.DB_RC_OK

    Catch _ex As Exception
      Return ENUM_STATUS.STATUS_ERROR

    End Try

  End Function 'CashdeskDrawsConf_DbUpdate

  '----------------------------------------------------------------------------
  ' PURPOSE: Updates the Virtual Terminal status (enabled/disabled) and returns the status of db transaction 
  '                                              
  ' PARAMS:
  '   - INPUT: 
  '   - OUTPUT:
  '
  ' RETURNS:
  '
  ' NOTES:

  Public Function DB_VirtualTerminal_Update(ByVal Trx As SqlTransaction) As Boolean
    Dim _sb As StringBuilder

    Try
      _sb = New StringBuilder()
      _sb.AppendLine("UPDATE  TERMINALS ")
      _sb.AppendLine("   SET   TE_STATUS                 = @pStatus")
      _sb.AppendLine("        ,TE_BLOCKED                = @pBlocked")
      _sb.AppendLine("        ,TE_ACTIVE                 = @pActive")

      If Me.Enabled Then
        _sb.AppendLine("        , TE_ACTIVATION_DATE = GETDATE()")

      Else
        _sb.AppendLine("        ,TE_RETIREMENT_DATE        = GETDATE ()")
        _sb.AppendLine("        ,TE_RETIREMENT_REQUESTED   = GETDATE ()")

      End If

      _sb.AppendLine(" WHERE   TE_TERMINAL_TYPE          = @pTerminalType")
      _sb.AppendLine("   AND   TE_STATUS                 <> @pStatus")

      Using _cmd As SqlCommand = New SqlCommand(_sb.ToString(), Trx.Connection, Trx)
        _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = IIf(Me.Enabled, TerminalStatus.ACTIVE, TerminalStatus.RETIRED)
        _cmd.Parameters.Add("@pBlocked", SqlDbType.Bit).Value = IIf(Me.Enabled, False, True)
        _cmd.Parameters.Add("@pActive", SqlDbType.Bit).Value = IIf(Me.Enabled, True, False)
        _cmd.Parameters.Add("@pTerminalType", SqlDbType.SmallInt).Value = TerminalTypes.CASHDESK_DRAW

        _cmd.ExecuteNonQuery()

        Return True

      End Using
    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return False

  End Function 'DB_VirtualTerminal_Update

  'LTC 14-SEP-2016
  '----------------------------------------------------------------------------
  ' PURPOSE: Verify whether cashdeskdraw configuration parameters are on db, otherwise insert them on general_params table
  '                                              
  ' PARAMS:
  '   - INPUT: CashDeskName
  '   - OUTPUT:
  '
  ' RETURNS:
  '
  ' NOTES:
  Private Sub DB_CheckCashDreskDrawConfig(CashDeskId As Integer, CashDeskName As String)

    Dim _sb As StringBuilder

    Try
      _sb = New StringBuilder()

      _sb.AppendLine("EXECUTE [dbo].[sp_CheckCashDeskDrawConfig] ")
      _sb.AppendLine(" @pID ")
      _sb.AppendLine(",@pName ")

      Using _db_trx As New DB_TRX()
        Using _cmd As SqlCommand = New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          _cmd.Parameters.Add("@pID", SqlDbType.Int).Value = CashDeskId
          _cmd.Parameters.Add("@pName", SqlDbType.VarChar).Value = CashDeskName

          _cmd.ExecuteNonQuery()

        End Using
        _db_trx.Commit()
      End Using
    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

  End Sub

#End Region 'Private functions

End Class
