'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_ads_steps_edit
' DESCRIPTION:   Ads_step entity class for user edition
' AUTHOR:        Artur Nebot
' CREATION DATE: 16-MAY-2012
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 16-MAY-2012  ANG    Initial version
'--------------------------------------------------------------------
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports WSI.Common.MEDIA_TYPE
Imports System.Text
Imports System.IO
Imports WSI.Common


Public Class CLASS_GAMES_ADMIN
  Inherits CLASS_BASE

#Region " Constants "

  Protected Const AUDIT_LABEL_TITLE As Integer = 7553
  Protected Const AUDIT_LABEL_CODE As Integer = 7556
  Protected Const AUDIT_LABEL_CATEGORY As Integer = 7555
  Protected Const AUDIT_LABEL_LAST_UPDATE As Integer = 7560
  Protected Const AUDIT_LABEL_IMAGE As Integer = 7561

  Protected Const AUDIT_LABEL_APP As Integer = 7557
  Protected Const AUDIT_LABEL_URL As Integer = 7650

#End Region

#Region " Enums "

#End Region

#Region " Structures "
  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_GAMES_ADMIN
    Public gea_code As String
    Public category As Int64
    Public title As String
    Public app_visible As Boolean
    Public image As Image
    Public last_update As Date
    Public url As String
    Public id As Integer
  End Class
#End Region

#Region " Members "
  Protected m_games As New TYPE_GAMES_ADMIN
#End Region

#Region " Properties "

  Public Property Id() As Integer
    Get
      Return m_games.id
    End Get
    Set(ByVal value As Integer)
      m_games.id = value
    End Set
  End Property

  Public Property Category() As Integer
    Get
      Return m_games.category
    End Get
    Set(ByVal value As Integer)
      m_games.category = value
    End Set
  End Property
  Public Property Code() As String
    Get
      Return m_games.gea_code
    End Get
    Set(ByVal value As String)
      m_games.gea_code = value
    End Set
  End Property

  Public Property Title() As String
    Get
      Return m_games.title
    End Get
    Set(ByVal value As String)
      m_games.title = value
    End Set
  End Property

  Public Property LastUpdate() As Date
    Get
      Return m_games.last_update
    End Get
    Set(ByVal value As Date)
      m_games.last_update = value
    End Set
  End Property


  Public Property App_Visible() As Boolean
    Get
      Return m_games.app_visible
    End Get
    Set(ByVal value As Boolean)
      m_games.app_visible = value
    End Set
  End Property

  Public Property Url() As String
    Get
      Return m_games.url
    End Get
    Set(ByVal value As String)
      m_games.url = value
    End Set
  End Property


  Public Property Image() As Image
    Get
      Return m_games.image
    End Get
    Set(ByVal value As Image)
      m_games.image = value
    End Set
  End Property
#End Region

#Region " Override Functions "

  '----------------------------------------------------------------------------
  ' PURPOSE : Reads from the database into the given object
  '
  ' PARAMS :
  '     - INPUT :
  '         - ObjectId: An object, it must be 'casted' to the desired KEY.
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '
  ' NOTES :

  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As Integer

    Me.Id = ObjectId

    ' Read ads data
    _rc = ReadGame(m_games, SqlCtx)

    Select Case _rc

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE : Updates the given object to the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES :

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As Integer

    ' Update ads data
    _rc = UpdateGame(m_games, SqlCtx)

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
        Return ENUM_STATUS.STATUS_DUPLICATE_KEY

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select
  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE : Inserts the given object to the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES :

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _rc As Integer

    _rc = InsertGame(m_games, SqlCtx)

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
        Return ENUM_STATUS.STATUS_DUPLICATE_KEY

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select
  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE : Deletes the given object from the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DEPENDENCIES
  '
  ' NOTES :

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _rc As Integer

    _rc = DeleteGame(m_games, SqlCtx)

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DEPENDENCIES
        Return ENUM_STATUS.STATUS_DEPENDENCIES

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select
  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE : Returns the related auditor data for the current object.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - The newly created audit object
  '
  ' NOTES :

  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim _auditor_data As CLASS_AUDITOR_DATA

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_PLAYER_PROMOTIONS)


    Call _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(AUDIT_LABEL_TITLE), Me.Title)

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(AUDIT_LABEL_TITLE), Me.Title)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(AUDIT_LABEL_CATEGORY), Me.Category)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(AUDIT_LABEL_CODE), Me.Code)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(AUDIT_LABEL_APP), Me.App_Visible)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(AUDIT_LABEL_URL), Me.Url)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(AUDIT_LABEL_IMAGE), GetInfoCRC(Me.Image))

    Return _auditor_data
  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE : Returns a duplicate of the given object.
  '
  ' PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - The newly created object
  '
  ' NOTES :

  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE
    Dim _copy As CLASS_GAMES_ADMIN
    _copy = New CLASS_GAMES_ADMIN

    _copy.m_games.category = Me.m_games.category
    _copy.m_games.gea_code = Me.m_games.gea_code
    _copy.m_games.title = Me.m_games.title
    _copy.m_games.app_visible = Me.m_games.app_visible
    _copy.m_games.url = Me.m_games.url

    If Not Me.m_games.image Is Nothing Then
      _copy.m_games.image = Me.m_games.image.Clone()
    Else
      _copy.m_games.image = Nothing
    End If

    Return _copy
  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE : Returns object info as String ( Mainly for easy debug ) 
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT : 
  '
  ' RETURNS :
  '     - String with instance info
  '
  ' NOTES :

  Public Overrides Function ToString() As String

    Dim _str_bld As System.Text.StringBuilder
    _str_bld = New System.Text.StringBuilder()

    _str_bld.AppendLine("Code =  " & Me.Code)
    _str_bld.AppendLine("Title =  " & Me.Title)

    Return _str_bld.ToString()
  End Function

#End Region

#Region " Private functions"

  '----------------------------------------------------------------------------
  ' PURPOSE : Reads a advertisement object from DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - TYPE_ADS_STEPS
  '         - Context
  '
  '     - OUTPUT :
  '         - Item
  '
  ' RETURNS :
  '     - CONFIGURATION_OK
  '     - CONFIGURATION_ERROR_DB
  '
  ' NOTES :

  Private Function ReadGame(ByVal Item As TYPE_GAMES_ADMIN, _
                           ByVal Context As Integer) As Integer

    Dim _sql_cmd As SqlCommand
    Dim _sql_string_builder As System.Text.StringBuilder
    Dim _code As String
    Dim _datatable As DataTable
    Dim _sql_transaction As System.Data.SqlClient.SqlTransaction
    Dim _rc As Integer
    Dim _do_commit As Boolean

    _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    _sql_transaction = Nothing
    _code = Item.gea_code
    _sql_string_builder = New System.Text.StringBuilder()
    _do_commit = False

    Try

      If Not GUI_BeginSQLTransaction(_sql_transaction) Then

        _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        Return _rc
      End If

      With _sql_string_builder
        .AppendLine("SELECT   ge_game_external_category_id ")
        .AppendLine("       , ge_code   ")
        .AppendLine("       , ge_title   ")
        .AppendLine("     	, ge_image   ")
        .AppendLine("       , ge_url   ")
        .AppendLine("	      , ge_app_visible   ")
        .AppendLine("	      , ge_last_update_date    ")
        .AppendLine("  FROM   mapp_games_external")
        .AppendLine(" WHERE   ge_game_external_id = @pId ")
      End With

      _sql_cmd = New SqlCommand(_sql_string_builder.ToString())
      _sql_cmd.Parameters.Add("@pId", SqlDbType.BigInt).Value = Me.Id

      _datatable = GUI_GetTableUsingCommand(_sql_cmd, 1)

      If Not IsNothing(_datatable) AndAlso _datatable.Rows.Count = 1 Then

        With Item
          .category = _datatable.Rows(0).Item("ge_game_external_category_id")
          .gea_code = _datatable.Rows(0).Item("ge_code")
          .title = _datatable.Rows(0).Item("ge_title")
          .app_visible = _datatable.Rows(0).Item("ge_app_visible")
          .last_update = _datatable.Rows(0).Item("ge_last_update_date")
          .url = IIf(IsDBNull(_datatable.Rows(0).Item("ge_url")), "", _datatable.Rows(0).Item("ge_url"))

          If Not IsDBNull(_datatable.Rows(0).Item("ge_image")) Then
            Dim imgData As Byte() = _datatable.Rows(0).Item("ge_image")
            Using ms As New MemoryStream(imgData)
              .image = Image.FromStream(ms)
            End Using
          Else
            .image = Nothing
          End If

        End With

        _do_commit = True
        _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return _rc

      Else

        _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        Return _rc

      End If
    Catch ex As Exception
      _do_commit = False
    Finally

      GUI_EndSQLTransaction(_sql_transaction, _do_commit)

    End Try

    Return _rc

  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE : Deletes a advertisement object from DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - TYPE_ADS_STEPS
  '         - Context
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - CONFIGURATION_OK
  '     - CONFIGURATION_ERROR_DB
  '
  ' NOTES :

  Private Function DeleteGame(ByVal Item As TYPE_GAMES_ADMIN, _
                             ByVal Context As Integer) As Integer

    Dim _code As String
    Dim _sql_string_builder As System.Text.StringBuilder
    Dim _sql_cmd As SqlCommand
    Dim _sql_transaction As System.Data.SqlClient.SqlTransaction
    Dim _row_affected As Integer
    Dim _do_commit As Boolean

    _sql_transaction = Nothing
    _row_affected = 0
    _code = Item.gea_code
    _do_commit = False
    _sql_string_builder = New System.Text.StringBuilder()

    Try

      If Not GUI_BeginSQLTransaction(_sql_transaction) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      With _sql_string_builder
        .AppendLine("DELETE FROM   mapp_games_external")
        .AppendLine("      WHERE   ge_game_external_id = @pCode")
      End With

      _sql_cmd = New SqlCommand(_sql_string_builder.ToString(), _sql_transaction.Connection, _sql_transaction)
      _sql_cmd.Parameters.Add("@pCode", SqlDbType.BigInt).Value = Me.Id
      _row_affected = _sql_cmd.ExecuteNonQuery()

      If _row_affected <> 1 Then
        _do_commit = False
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      Else
        _do_commit = True
      End If



    Catch ex As Exception

      _do_commit = False

    Finally

      GUI_EndSQLTransaction(_sql_transaction, _do_commit)
    End Try

    If _do_commit Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
    Else
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

  End Function ' DeleteAds

  '----------------------------------------------------------------------------
  ' PURPOSE : Adds a advertisement object into DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - TYPE_ADS_STEPS
  '         - Context
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - CONFIGURATION_OK
  '     - CONFIGURATION_ERROR_DB
  '
  ' NOTES :

  Private Function InsertGame(ByVal Item As TYPE_GAMES_ADMIN, _
                             ByVal Context As Integer) As Integer

    Dim _sql_string_builder As System.Text.StringBuilder
    Dim _sql_cmd As SqlCommand
    Dim _sql_transaction As System.Data.SqlClient.SqlTransaction
    Dim _row_affected As Integer
    Dim _do_commit As Boolean
    Dim _rc As Integer
    Dim _para_game_id As SqlParameter

    _sql_string_builder = New StringBuilder()
    _sql_transaction = Nothing
    _row_affected = 0
    _do_commit = False
    _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

    Try

      If Not GUI_BeginSQLTransaction(_sql_transaction) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If
      Dim imgData As Byte()
      imgData = Nothing
      
      If Not IsNothing(Item.image) Then
        Dim img As New ImageConverter()
        imgData = img.ConvertTo(Item.image, GetType(Byte()))
      End If

      With _sql_string_builder
        .AppendLine("INSERT INTO   mapp_games_external              ")
        .AppendLine("            ( ge_game_external_category_id		  ")
        .AppendLine("            , ge_code                       		")
        .AppendLine("            , ge_title				                  ")
        .AppendLine("            , ge_image			                    ")
        .AppendLine("            , ge_app_visible			              ")
        .AppendLine("            , ge_url		                        ")
        .AppendLine("            , ge_date_creation		                        ")
        .AppendLine("            , ge_last_update_date		          ")

        .AppendLine("            )						                      ")
        .AppendLine("     VALUES ( @pCategory			             	    ")
        .AppendLine("            , @pCode       	                	")
        .AppendLine("            , @pTitle			                    ")
        .AppendLine("            , @pImage					                ")
        .AppendLine("            , @pApp			                    	")
        .AppendLine("            , @pUrl		                        ")
        .AppendLine("            , @pDateCreation		                        ")
        .AppendLine("            , @pLastUpdate		                	")
        .AppendLine("            );						                      ")
        .AppendLine("SET @pGameId = SCOPE_IDENTITY();")
      End With

      _sql_cmd = New SqlCommand(_sql_string_builder.ToString(), _sql_transaction.Connection, _sql_transaction)
      _sql_cmd.Parameters.Add("@pCategory", SqlDbType.Int).Value = Item.category

      _sql_cmd.Parameters.Add("@pCode", SqlDbType.NVarChar).Value = Item.gea_code
      _sql_cmd.Parameters.Add("@pTitle", SqlDbType.NVarChar).Value = Item.title

      If Not IsNothing(Item.image) Then
        _sql_cmd.Parameters.Add("@pImage", SqlDbType.VarBinary).Value = imgData
      Else
        _sql_cmd.Parameters.Add("@pImage", SqlDbType.VarBinary).Value = DBNull.Value
      End If

      _sql_cmd.Parameters.Add("@pApp", SqlDbType.Bit).Value = Item.app_visible
      _sql_cmd.Parameters.Add("@pUrl", SqlDbType.NVarChar).Value = Item.url

      _sql_cmd.Parameters.Add("@pLastUpdate", SqlDbType.DateTime).Value = WSI.Common.WGDB.Now
      _sql_cmd.Parameters.Add("@pDateCreation", SqlDbType.DateTime).Value = WSI.Common.WGDB.Now

      _para_game_id = _sql_cmd.Parameters.Add("@pGameId", SqlDbType.BigInt)
      _para_game_id.Direction = ParameterDirection.Output

      _row_affected = _sql_cmd.ExecuteNonQuery()

      If _row_affected <> 1 Then
        _do_commit = False
        _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        Return _rc
      Else
        _do_commit = True
        _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_OK
      End If

    Catch ex As Exception
      _do_commit = False
      _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

    Finally

      GUI_EndSQLTransaction(_sql_transaction, _do_commit)

    End Try

    Return _rc

  End Function ' InsertArea

  '----------------------------------------------------------------------------
  ' PURPOSE : Updates the given advertisement object into the DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - TYPE_ADS_STEPS
  '         - Context
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - CONFIGURATION_OK
  '     - CONFIGURATION_ERROR_DB
  '
  ' NOTES :

  Private Function UpdateGame(ByVal Item As TYPE_GAMES_ADMIN, _
                             ByVal Context As Integer) As Integer

    Dim _sql_string_builder As System.Text.StringBuilder
    Dim _sql_cmd As SqlCommand
    Dim _sql_transaction As System.Data.SqlClient.SqlTransaction
    Dim _row_affected As Integer
    Dim _do_commit As Boolean
    Dim _rc As Integer

    _sql_string_builder = New System.Text.StringBuilder()
    _sql_transaction = Nothing
    _row_affected = 0
    _do_commit = False

    Try

      If Not GUI_BeginSQLTransaction(_sql_transaction) Then
        _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

        Return _rc
      End If

      Dim imgData As Byte()
      imgData = Nothing

      If Not IsNothing(Item.image) Then
        Dim img As New ImageConverter()
        imgData = img.ConvertTo(Item.image, GetType(Byte()))
      End If

      With _sql_string_builder

        .AppendLine("UPDATE   mapp_games_external                                 ")
        .AppendLine("   SET   ge_title                     = @pTitle              ")
        .AppendLine("       , ge_game_external_category_id = @pCategory           ")
        .AppendLine("       , ge_image                     = @pImage              ")
        .AppendLine("       , ge_app_visible               = @pApp                ")
        .AppendLine("       , ge_url                        = @pUrl                ")
        .AppendLine("       , ge_last_update_date          = @pLastUpdate         ")
        .AppendLine("       , ge_code                       = @pCode         ")
        .AppendLine(" WHERE   ge_game_external_id           = @pId               ")
      End With

      _sql_cmd = New SqlCommand(_sql_string_builder.ToString(), _sql_transaction.Connection, _sql_transaction)
      _sql_cmd.Parameters.Add("@pCode", SqlDbType.NVarChar).Value = Item.gea_code
      _sql_cmd.Parameters.Add("@pTitle", SqlDbType.NVarChar).Value = Item.title
      _sql_cmd.Parameters.Add("@pCategory", SqlDbType.Int).Value = Item.category
      _sql_cmd.Parameters.Add("@pLastUpdate", SqlDbType.DateTime).Value = WSI.Common.WGDB.Now
      _sql_cmd.Parameters.Add("@pApp", SqlDbType.Bit).Value = Item.app_visible
      _sql_cmd.Parameters.Add("@pUrl", SqlDbType.NVarChar).Value = Item.url

      If Not IsNothing(Item.image) Then
        _sql_cmd.Parameters.Add("@pImage", SqlDbType.VarBinary).Value = imgData
      Else
        _sql_cmd.Parameters.Add("@pImage", SqlDbType.VarBinary).Value = DBNull.Value
      End If

      _sql_cmd.Parameters.Add("@pId", SqlDbType.BigInt).Value = Item.id

      _row_affected = _sql_cmd.ExecuteNonQuery()

      If _row_affected <> 1 Then
        _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        Return _rc
      Else
        _do_commit = True
        _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_OK
      End If

    Catch ex As Exception
      _do_commit = False

    Finally

      GUI_EndSQLTransaction(_sql_transaction, _do_commit)

    End Try

    Return _rc

  End Function ' UpdateArea


  ' PURPOSE : Exist name of Catalog
  '
  ' PARAMS :
  '     - INPUT :
  '         - name
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK  -> Not Exists
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DUPLICATE_KEY -> Exist
  '
  ' NOTES :
  Public Function ExistsCode(ByVal Code As String, ByVal Id As Long) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _value As Object
    Dim _db_count As Integer
    Dim _str_sql As String

    Using _db_trx As New DB_TRX()

      ' CHECK If name already exists
      _str_sql = "SELECT  COUNT(*) " & _
                " FROM mapp_games_external  " & _
              "  WHERE   ge_code = @pCode AND ge_game_external_id <> @pId"

      Using _cmd As New SqlClient.SqlCommand(_str_sql, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
        _cmd.Parameters.Add("@pCode", SqlDbType.NVarChar).Value = Code
        _cmd.Parameters.Add("@pId", SqlDbType.BigInt).Value = Id

        _value = _cmd.ExecuteScalar()
      End Using

    End Using

    If _value IsNot Nothing And _value IsNot DBNull.Value Then
      _db_count = CInt(_value)
    Else
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    If _db_count > 0 Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
    Else
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
    End If


  End Function ' ExistsCatalogName

#End Region

End Class
