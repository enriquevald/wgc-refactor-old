'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_ads_steps_edit
' DESCRIPTION:   Ads_step entity class for user edition
' AUTHOR:        Artur Nebot
' CREATION DATE: 16-MAY-2012
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 16-MAY-2012  ANG    Initial version
' 03-AGO-2017  CSR    Bug 29170:WIGOS-4218 The user is not able to edit the campaigns created before enable WinUP
'--------------------------------------------------------------------
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports WSI.Common.MEDIA_TYPE
Imports WSI.Common
Imports System.Text


Public Class CLASS_ADS_STEPS
  Inherits CLASS_BASE

#Region " Constants "

  Protected Const AUDIT_LABEL_NAME As Integer = 693
  Protected Const AUDIT_LABEL_DESCRIPTION As Integer = 707
  Protected Const AUDIT_LABEL_INITIALDATE As Integer = 695
  Protected Const AUDIT_LABEL_FINALDATE As Integer = 696
  Protected Const AUDIT_LABEL_ENABLED As Integer = 697
  Protected Const AUDIT_LABEL_CAMPAIGN As Integer = 712

  Protected Const AUDIT_LABEL_WINUP_LIST_DESCRIPTION As Integer = 7935
  Protected Const AUDIT_LABEL_WINUP_TITLE As Integer = 7939
  Protected Const AUDIT_LABEL_WINUP_VALIDFROM As Integer = 295
  Protected Const AUDIT_LABEL_WINUP_VALIDTO As Integer = 296
  Protected Const AUDIT_LABEL_PROMOBOX_ENABLED As Integer = 7932
  Protected Const AUDIT_LABEL_WINUP_ENABLED As Integer = 7933

  Protected Const AUDIT_LABEL_TIMEFROM1 As Integer = 726
  Protected Const AUDIT_LABEL_TIMETO1 As Integer = 727
  Protected Const AUDIT_LABEL_TIMEFROM2 As Integer = 728
  Protected Const AUDIT_LABEL_TIMETO2 As Integer = 729
  Protected Const AUDIT_LABEL_WEEKDAY As Integer = 706
  Protected Const AUDIT_LABEL_EMPTY As Integer = 710
  Protected Const AUDIT_LABEL_IMAGE As Integer = 734

  'SDS 27-07-2016 add fields for app mobile PBI 15957
  Protected Const AUDIT_LABEL_ABSTRACT As Integer = 7448
  Protected Const AUDIT_LABEL_TARGET As Integer = 7447
  Protected Const AUDIT_LABEL_ORDER As Integer = 7449
  Protected Const AUDIT_LABEL_SECTION As Integer = 7585
  Protected Const AUDIT_LABEL_LASTUPDATE As Integer = 7660

  Protected Const AUDIT_LABEL_IMAGELIST As Integer = 7450
  Protected Const AUDIT_LABEL_IMAGEDETAIL As Integer = 7451
#End Region

#Region " Enums "

#End Region

#Region " Structures "
  <StructLayout(LayoutKind.Sequential)>
  Public Class TYPE_ADS_STEPS
    Public ads_step_id As Int64
    Public name As String
    Public description As String
    Public winup_list_description As String
    Public winup_title As String
    Public winup_valid_from As Date
    Public winup_valid_to As Date
    Public initial_date As Date
    Public final_date As Date
    Public enabled As Boolean
    Public winup_enabled As Boolean
    Public promobox_enabled As Boolean
    Public weekday As Integer
    Public ad_time_from1 As Integer
    Public ad_time_to1 As Integer
    Public ad_time_from2 As Nullable(Of Integer)
    Public ad_time_to2 As Nullable(Of Integer)
    Public image As Image

    'SDS 27-07-2016 add fields for app mobile PBI 15957
    Public resource_id As Nullable(Of Long)
    Public ad_target As Integer
    Public ad_abstract As String
    Public ad_order As Integer
    Public ad_imageList As Image
    Public ad_imageListResource_id As Nullable(Of Long)
    Public ad_imageDetail As Image
    Public ad_imageDetailResource_id As Nullable(Of Long)
    Public ad_target_schema As Integer

    'SDS 12-10-2016 date by update images
    Public ad_lastUpdate As Date
  End Class
#End Region

#Region " Members "
  Protected m_ads_steps As New TYPE_ADS_STEPS
#End Region

#Region " Properties "

  Public Property AdsStepId() As Int64
    Get
      Return m_ads_steps.ads_step_id
    End Get
    Set(ByVal value As Int64)
      m_ads_steps.ads_step_id = value
    End Set
  End Property

  Public Property Name() As String
    Get
      Return m_ads_steps.name
    End Get
    Set(ByVal value As String)
      m_ads_steps.name = value
    End Set
  End Property

  Public Property Description() As String
    Get
      Return m_ads_steps.description
    End Get
    Set(ByVal value As String)
      m_ads_steps.description = value
    End Set
  End Property
  Public Property WinupListDescription() As String
    Get
      Return m_ads_steps.winup_list_description
    End Get
    Set(ByVal value As String)
      m_ads_steps.winup_list_description = value
    End Set
  End Property
  Public Property WinupTitle() As String
    Get
      Return m_ads_steps.winup_title
    End Get
    Set(ByVal value As String)
      m_ads_steps.winup_title = value
    End Set
  End Property
  Public Property WinupValidFrom() As Date
    Get
      Return m_ads_steps.winup_valid_from
    End Get
    Set(ByVal value As Date)
      m_ads_steps.winup_valid_from = value
    End Set
  End Property
  Public Property WinupValidTo() As Date
    Get
      Return m_ads_steps.winup_valid_to
    End Get
    Set(ByVal value As Date)
      m_ads_steps.winup_valid_to = value
    End Set
  End Property
  Public Property InitalDate() As Date
    Get
      Return m_ads_steps.initial_date
    End Get
    Set(ByVal value As Date)
      m_ads_steps.initial_date = value
    End Set
  End Property

  Public Property FinalDate() As Date
    Get
      Return m_ads_steps.final_date
    End Get
    Set(ByVal value As Date)
      m_ads_steps.final_date = value
    End Set
  End Property

  Public Property Enabled() As Boolean
    Get
      Return m_ads_steps.enabled
    End Get
    Set(ByVal value As Boolean)
      m_ads_steps.enabled = value
    End Set
  End Property
  Public Property PromoboxEnabled() As Boolean
    Get
      Return m_ads_steps.promobox_enabled
    End Get
    Set(ByVal value As Boolean)
      m_ads_steps.promobox_enabled = value
    End Set
  End Property
  Public Property WinupEnabled() As Boolean
    Get
      Return m_ads_steps.winup_enabled
    End Get
    Set(ByVal value As Boolean)
      m_ads_steps.winup_enabled = value
    End Set
  End Property
  Public Property Weekday() As Integer
    Get
      Return m_ads_steps.weekday
    End Get
    Set(ByVal value As Integer)
      Me.m_ads_steps.weekday = value
    End Set
  End Property

  Public Property TimeFrom() As Integer
    Get
      Return m_ads_steps.ad_time_from1
    End Get
    Set(ByVal value As Integer)
      Me.m_ads_steps.ad_time_from1 = value
    End Set
  End Property

  Public Property TimeTo() As Integer
    Get
      Return m_ads_steps.ad_time_to1
    End Get
    Set(ByVal value As Integer)
      Me.m_ads_steps.ad_time_to1 = value
    End Set
  End Property

  Public ReadOnly Property HasSecondTime() As Boolean
    Get
      Return IIf(m_ads_steps.ad_time_from2.HasValue And m_ads_steps.ad_time_to2.HasValue, True, False)
    End Get
  End Property

  Public Property SecondTimeFrom() As Nullable(Of Integer)
    Get
      Return m_ads_steps.ad_time_from2
    End Get
    Set(ByVal value As Nullable(Of Integer))
      Me.m_ads_steps.ad_time_from2 = value
    End Set
  End Property

  Public Property SecondTimeTo() As Nullable(Of Integer)
    Get
      Return m_ads_steps.ad_time_to2
    End Get
    Set(ByVal value As Nullable(Of Integer))
      Me.m_ads_steps.ad_time_to2 = value
    End Set
  End Property

  Public Property Image() As Image
    Get
      Return m_ads_steps.image
    End Get
    Set(ByVal value As Image)
      m_ads_steps.image = value
    End Set
  End Property

  'SDS 27-07-2016 add fields for app mobile PBI 15957
  Public Property Abstract() As String
    Get
      Return m_ads_steps.ad_abstract
    End Get
    Set(ByVal value As String)
      m_ads_steps.ad_abstract = value
    End Set
  End Property
  Public Property Target() As Integer
    Get
      Return m_ads_steps.ad_target
    End Get
    Set(ByVal value As Integer)
      m_ads_steps.ad_target = value
    End Set
  End Property
  Public Property TargetSchemaId() As Integer
    Get
      Return m_ads_steps.ad_target_schema
    End Get
    Set(ByVal value As Integer)
      m_ads_steps.ad_target_schema = value
    End Set
  End Property
  Public Property Order() As Integer
    Get
      Return m_ads_steps.ad_order
    End Get
    Set(ByVal value As Integer)
      m_ads_steps.ad_order = value
    End Set
  End Property
  Public Property ImageList() As Image
    Get
      Return m_ads_steps.ad_imageList
    End Get
    Set(ByVal value As Image)
      m_ads_steps.ad_imageList = value
    End Set
  End Property
  Public Property ImageDetail() As Image
    Get
      Return m_ads_steps.ad_imageDetail
    End Get
    Set(ByVal value As Image)
      m_ads_steps.ad_imageDetail = value
    End Set
  End Property
  Public Property LastUpdate() As Date
    Get
      Return m_ads_steps.ad_lastUpdate
    End Get
    Set(ByVal value As Date)
      m_ads_steps.ad_lastUpdate = value
    End Set
  End Property
#End Region

#Region " Override Functions "

  '----------------------------------------------------------------------------
  ' PURPOSE : Reads from the database into the given object
  '
  ' PARAMS :
  '     - INPUT :
  '         - ObjectId: An object, it must be 'casted' to the desired KEY.
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '
  ' NOTES :

  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As Integer

    Me.AdsStepId = ObjectId

    ' Read ads data
    _rc = ReadAds(m_ads_steps, SqlCtx)

    Select Case _rc

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE : Updates the given object to the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES :

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As Integer

    ' Update ads data
    _rc = UpdateAds(m_ads_steps, SqlCtx)

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
        Return ENUM_STATUS.STATUS_DUPLICATE_KEY

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select
  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE : Inserts the given object to the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES :

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _rc As Integer

    _rc = InsertAds(m_ads_steps, SqlCtx)

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
        Return ENUM_STATUS.STATUS_DUPLICATE_KEY

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select
  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE : Deletes the given object from the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DEPENDENCIES
  '
  ' NOTES :

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _rc As Integer

    _rc = DeleteAds(m_ads_steps, SqlCtx)

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DEPENDENCIES
        Return ENUM_STATUS.STATUS_DEPENDENCIES

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select
  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE : Returns the related auditor data for the current object.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - The newly created audit object
  '
  ' NOTES :

  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _date_from2 As Object
    Dim _date_to2 As Object

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_PLAYER_PROMOTIONS)


    Call _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(AUDIT_LABEL_CAMPAIGN), Me.Name)

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(AUDIT_LABEL_CAMPAIGN), Me.Name)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(AUDIT_LABEL_DESCRIPTION), Me.Description)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(AUDIT_LABEL_INITIALDATE), Me.InitalDate)

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(AUDIT_LABEL_FINALDATE), Me.FinalDate)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(AUDIT_LABEL_ENABLED), Me.Enabled)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(AUDIT_LABEL_WEEKDAY), Me.Weekday)


    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(AUDIT_LABEL_TIMEFROM1), Me.TimeFrom)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(AUDIT_LABEL_TIMETO1), Me.TimeTo)

    If Me.HasSecondTime Then
      _date_from2 = Me.SecondTimeFrom
      _date_to2 = Me.SecondTimeTo
    Else
      _date_from2 = GLB_NLS_GUI_PLAYER_TRACKING.Id(AUDIT_LABEL_EMPTY)
      _date_to2 = GLB_NLS_GUI_PLAYER_TRACKING.Id(AUDIT_LABEL_EMPTY)
    End If

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(AUDIT_LABEL_TIMEFROM2), _date_from2)

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(AUDIT_LABEL_TIMETO2), _date_to2)

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(AUDIT_LABEL_IMAGE), GetInfoCRC(Me.Image))

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(AUDIT_LABEL_PROMOBOX_ENABLED), Me.PromoboxEnabled)

    If WSI.Common.GeneralParam.GetBoolean("Features", "WinUP", False) Then
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(AUDIT_LABEL_WINUP_LIST_DESCRIPTION), Me.WinupListDescription)
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(AUDIT_LABEL_WINUP_TITLE), Me.WinupTitle)

      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(AUDIT_LABEL_WINUP_VALIDFROM), Me.WinupValidFrom)

      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(AUDIT_LABEL_WINUP_VALIDTO), Me.WinupValidTo)

      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(AUDIT_LABEL_WINUP_ENABLED), Me.WinupEnabled)

      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(AUDIT_LABEL_ABSTRACT), Me.Abstract)
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(AUDIT_LABEL_TARGET), Me.Target)
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(AUDIT_LABEL_ORDER), Me.Order)
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(AUDIT_LABEL_SECTION), Me.TargetSchemaId)
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(AUDIT_LABEL_LASTUPDATE), Me.LastUpdate)

      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(AUDIT_LABEL_IMAGELIST), GetInfoCRC(Me.ImageList))
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(AUDIT_LABEL_IMAGEDETAIL), GetInfoCRC(Me.ImageDetail))
    End If

    Return _auditor_data
  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE : Returns a duplicate of the given object.
  '
  ' PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - The newly created object
  '
  ' NOTES :

  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE
    Dim _copy As CLASS_ADS_STEPS
    _copy = New CLASS_ADS_STEPS

    _copy.m_ads_steps.ads_step_id = Me.m_ads_steps.ads_step_id
    _copy.m_ads_steps.name = Me.m_ads_steps.name
    _copy.m_ads_steps.description = Me.m_ads_steps.description
    _copy.m_ads_steps.initial_date = Me.m_ads_steps.initial_date
    _copy.m_ads_steps.final_date = Me.m_ads_steps.final_date
    _copy.m_ads_steps.enabled = Me.m_ads_steps.enabled
    _copy.m_ads_steps.weekday = Me.m_ads_steps.weekday
    _copy.m_ads_steps.ad_time_from1 = Me.m_ads_steps.ad_time_from1
    _copy.m_ads_steps.ad_time_to1 = Me.m_ads_steps.ad_time_to1
    _copy.m_ads_steps.ad_time_from2 = Me.m_ads_steps.ad_time_from2
    _copy.m_ads_steps.ad_time_to2 = Me.m_ads_steps.ad_time_to2

    _copy.m_ads_steps.winup_list_description = Me.m_ads_steps.winup_list_description
    _copy.m_ads_steps.winup_valid_from = Me.m_ads_steps.winup_valid_from
    _copy.m_ads_steps.winup_valid_to = Me.m_ads_steps.winup_valid_to
    _copy.m_ads_steps.promobox_enabled = Me.m_ads_steps.promobox_enabled
    _copy.m_ads_steps.winup_enabled = Me.m_ads_steps.winup_enabled
    _copy.m_ads_steps.winup_title = Me.m_ads_steps.winup_title

    If Not Me.m_ads_steps.image Is Nothing Then
      _copy.m_ads_steps.image = Me.m_ads_steps.image.Clone()
      _copy.m_ads_steps.resource_id = Me.m_ads_steps.resource_id
    Else
      _copy.m_ads_steps.image = Nothing
      _copy.m_ads_steps.resource_id = Nothing
    End If

    'SDS 27-07-2016 add fields for app mobile PBI 15957
    _copy.m_ads_steps.ad_abstract = Me.m_ads_steps.ad_abstract
    _copy.m_ads_steps.ad_order = Me.m_ads_steps.ad_order
    _copy.m_ads_steps.ad_target = Me.m_ads_steps.ad_target
    _copy.m_ads_steps.ad_target_schema = Me.m_ads_steps.ad_target_schema
    _copy.m_ads_steps.ad_lastUpdate = Me.m_ads_steps.ad_lastUpdate

    If Not Me.m_ads_steps.ad_imageDetail Is Nothing Then
      _copy.m_ads_steps.ad_imageDetail = Me.m_ads_steps.ad_imageDetail.Clone()
      _copy.m_ads_steps.ad_imageDetailResource_id = Me.m_ads_steps.ad_imageDetailResource_id
    Else
      _copy.m_ads_steps.ad_imageDetail = Nothing
      _copy.m_ads_steps.ad_imageDetailResource_id = Nothing
    End If

    If Not Me.m_ads_steps.ad_imageList Is Nothing Then
      _copy.m_ads_steps.ad_imageList = Me.m_ads_steps.ad_imageList.Clone()
      _copy.m_ads_steps.ad_imageListResource_id = Me.m_ads_steps.ad_imageListResource_id
    Else
      _copy.m_ads_steps.ad_imageList = Nothing
      _copy.m_ads_steps.ad_imageListResource_id = Nothing
    End If

    Return _copy
  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE : Returns object info as String ( Mainly for easy debug ) 
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT : 
  '
  ' RETURNS :
  '     - String with instance info
  '
  ' NOTES :

  Public Overrides Function ToString() As String

    Dim _str_bld As System.Text.StringBuilder
    _str_bld = New System.Text.StringBuilder()

    _str_bld.AppendLine("Id =  " & Me.AdsStepId)
    _str_bld.AppendLine("Name =  " & Me.Name)
    _str_bld.AppendLine("Description = " & Me.Description)
    _str_bld.AppendLine("Enabled  =" & Me.Enabled)

    Return _str_bld.ToString()
  End Function

#End Region

#Region " Private functions"

  '----------------------------------------------------------------------------
  ' PURPOSE : Reads a advertisement object from DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - TYPE_ADS_STEPS
  '         - Context
  '
  '     - OUTPUT :
  '         - Item
  '
  ' RETURNS :
  '     - CONFIGURATION_OK
  '     - CONFIGURATION_ERROR_DB
  '
  ' NOTES :

  Private Function ReadAds(ByVal Item As TYPE_ADS_STEPS,
                           ByVal Context As Integer) As Integer

    Dim _sql_cmd As SqlCommand
    Dim _sql_string_builder As System.Text.StringBuilder
    Dim _ads_id As Integer
    Dim _datatable As DataTable
    Dim _sql_transaction As System.Data.SqlClient.SqlTransaction
    Dim _rc As Integer
    Dim _do_commit As Boolean

    _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    _sql_transaction = Nothing
    _ads_id = Item.ads_step_id
    _sql_string_builder = New System.Text.StringBuilder()
    _do_commit = False

    Try

      If Not GUI_BeginSQLTransaction(_sql_transaction) Then

        _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        Return _rc
      End If

      With _sql_string_builder
        .AppendLine("SELECT   AD_ID ")
        .AppendLine("       , AD_NAME   ")
        .AppendLine("       , AD_DESCRIPTION   ")
        .AppendLine("       , AD_FROM   ")
        .AppendLine("     	, AD_TO   ")
        .AppendLine("       , AD_ENABLED   ")
        .AppendLine("	      , AD_WEEKDAY   ")
        .AppendLine("	      , AD_TIME_FROM1   ")
        .AppendLine("	      , AD_TIME_TO1    ")
        .AppendLine("	      , AD_TIME_FROM2   ")
        .AppendLine("	      , AD_TIME_TO2    ")
        .AppendLine("	      , ASCR_RESOURCE_ID  ")
        .AppendLine("	      , AD_PROMOBOX_ENABLED  ")


        'SDS 27-07-2016 add fields for app mobile PBI 15957
        If WSI.Common.GeneralParam.GetBoolean("Features", "WinUP", False) Then
          .AppendLine("	      , AD_WINUP_ENABLED  ")
          .AppendLine("	      , AD_WINUP_VALID_FROM  ")
          .AppendLine("	      , AD_WINUP_VALID_TO  ")
          .AppendLine("	      , AD_LIST_DESCRIPTION  ")
          .AppendLine("	      , AD_WINUP_TITLE  ")

          .AppendLine("	      , AD_TARGET  ")
          .AppendLine("	      , AD_ABSTRACT  ")
          .AppendLine("	      , AD_ORDER  ")
          .AppendLine("	      , AD_LIST_RESOURCE_ID  ")
          .AppendLine("	      , AD_DETAIL_RESOURCE_ID  ")
          .AppendLine("	      , AD_TARGET_SCHEMAID  ")
          .AppendLine("	      , AD_LAST_UPDATE_DATE  ")
        End If

        .AppendLine("  FROM   WKT_ADS A")
        .AppendLine("		      INNER JOIN WKT_AD_STEPS S ON A.AD_ID = S.AS_AD_ID")
        .AppendLine("	        INNER JOIN WKT_AD_STEP_DETAILS D ON D.ASCR_STEP_ID = S.AS_STEP_ID")
        .AppendLine("	        LEFT OUTER JOIN WKT_RESOURCES R ON D.ASCR_RESOURCE_ID = R.RES_RESOURCE_ID  ")
        .AppendLine(" WHERE   AD_ID = @pAdId ")
      End With

      _sql_cmd = New SqlCommand(_sql_string_builder.ToString())
      _sql_cmd.Parameters.Add("@pAdId", SqlDbType.BigInt).Value = _ads_id

      _datatable = GUI_GetTableUsingCommand(_sql_cmd, 1)

      If Not IsNothing(_datatable) AndAlso _datatable.Rows.Count = 1 Then

        With Item
          .ads_step_id = _datatable.Rows(0).Item("AD_ID")
          .name = _datatable.Rows(0).Item("AD_NAME")
          .description = _datatable.Rows(0).Item("AD_DESCRIPTION")

          .initial_date = _datatable.Rows(0).Item("AD_FROM")
          .final_date = _datatable.Rows(0).Item("AD_TO")
          .enabled = _datatable.Rows(0).Item("AD_ENABLED")
          .weekday = _datatable.Rows(0).Item("AD_WEEKDAY")
          .ad_time_from1 = _datatable.Rows(0).Item("AD_TIME_FROM1")
          .ad_time_to1 = _datatable.Rows(0).Item("AD_TIME_TO1")


          .promobox_enabled = _datatable.Rows(0).Item("AD_PROMOBOX_ENABLED")


          If IsDBNull(_datatable.Rows(0).Item("AD_TIME_FROM2")) Then
            .ad_time_from2 = Nothing
          Else
            .ad_time_from2 = _datatable.Rows(0).Item("AD_TIME_FROM2")
          End If

          If IsDBNull(_datatable.Rows(0).Item("AD_TIME_TO2")) Then
            .ad_time_to2 = Nothing
          Else
            .ad_time_to2 = _datatable.Rows(0).Item("AD_TIME_TO2")
          End If

          If Not IsDBNull(_datatable.Rows(0).Item("ASCR_RESOURCE_ID")) Then
            .resource_id = _datatable.Rows(0).Item("ASCR_RESOURCE_ID")
            .image = LoadImage(m_ads_steps.resource_id, _sql_transaction)
          Else
            .resource_id = Nothing
            .image = Nothing
          End If

          If WSI.Common.GeneralParam.GetBoolean("Features", "WinUP", False) Then
            If Not IsDBNull(_datatable.Rows(0).Item("AD_WINUP_TITLE")) Then
              .winup_title = _datatable.Rows(0).Item("AD_WINUP_TITLE")
            Else
              .winup_title = Nothing
            End If

            .winup_enabled = _datatable.Rows(0).Item("AD_WINUP_ENABLED")

            If Not IsDBNull(_datatable.Rows(0).Item("AD_LIST_DESCRIPTION")) Then
              .winup_list_description = _datatable.Rows(0).Item("AD_LIST_DESCRIPTION")
            Else
              .winup_list_description = Nothing
            End If

            If IsDBNull(_datatable.Rows(0).Item("AD_WINUP_VALID_FROM")) Then
              .winup_valid_from = Nothing
            Else
              .winup_valid_from = _datatable.Rows(0).Item("AD_WINUP_VALID_FROM")
            End If

            If IsDBNull(_datatable.Rows(0).Item("AD_WINUP_VALID_TO")) Then
              .winup_valid_to = Nothing
            Else
              .winup_valid_to = _datatable.Rows(0).Item("AD_WINUP_VALID_TO")
            End If
            'CSR 03-AGO-2017 Bug 29170:WIGOS-4218 The user is not able to edit the campaigns created before enable WinUP
            If IsDBNull(_datatable.Rows(0).Item("AD_LAST_UPDATE_DATE")) Then
              .ad_lastUpdate = Nothing
            Else
              .ad_lastUpdate = _datatable.Rows(0).Item("AD_LAST_UPDATE_DATE")
            End If

            'SDS 27-07-2016 add fields for app mobile PBI 15957
            If Not IsDBNull(_datatable.Rows(0).Item("AD_ABSTRACT")) Then
              .ad_abstract = _datatable.Rows(0).Item("AD_ABSTRACT")
            Else
              .ad_abstract = Nothing
            End If

            If Not IsDBNull(_datatable.Rows(0).Item("AD_TARGET")) Then
              .ad_target = _datatable.Rows(0).Item("AD_TARGET")
            Else
              .ad_target = Nothing
            End If

            If Not IsDBNull(_datatable.Rows(0).Item("AD_ORDER")) Then
              .ad_order = _datatable.Rows(0).Item("AD_ORDER")
            Else
              .ad_order = Nothing
            End If

            If Not IsDBNull(_datatable.Rows(0).Item("AD_TARGET_SCHEMAID")) Then
              .ad_target_schema = _datatable.Rows(0).Item("AD_TARGET_SCHEMAID")
            Else
              .ad_target_schema = Nothing
            End If

            If Not IsDBNull(_datatable.Rows(0).Item("AD_LIST_RESOURCE_ID")) Then
              .ad_imageListResource_id = _datatable.Rows(0).Item("AD_LIST_RESOURCE_ID")
              .ad_imageList = LoadImage(m_ads_steps.ad_imageListResource_id, _sql_transaction)
            Else
              .ad_imageListResource_id = Nothing
              .ad_imageList = Nothing
            End If

            If Not IsDBNull(_datatable.Rows(0).Item("AD_DETAIL_RESOURCE_ID")) Then
              .ad_imageDetailResource_id = _datatable.Rows(0).Item("AD_DETAIL_RESOURCE_ID")
              .ad_imageDetail = LoadImage(m_ads_steps.ad_imageDetailResource_id, _sql_transaction)
            Else
              .ad_imageDetailResource_id = Nothing
              .ad_imageDetail = Nothing
            End If

          End If

        End With

        _do_commit = True
        _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return _rc

      Else

        _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        Return _rc

      End If
    Catch ex As Exception
      _do_commit = False
    Finally

      GUI_EndSQLTransaction(_sql_transaction, _do_commit)

    End Try

    Return _rc

  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE : Deletes a advertisement object from DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - TYPE_ADS_STEPS
  '         - Context
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - CONFIGURATION_OK
  '     - CONFIGURATION_ERROR_DB
  '
  ' NOTES :

  Private Function DeleteAds(ByVal Item As TYPE_ADS_STEPS,
                             ByVal Context As Integer) As Integer

    Dim _ads_id As Long
    Dim _sql_string_builder As System.Text.StringBuilder
    Dim _sql_cmd As SqlCommand
    Dim _sql_transaction As System.Data.SqlClient.SqlTransaction
    Dim _row_affected As Integer
    Dim _do_commit As Boolean

    _sql_transaction = Nothing
    _row_affected = 0
    _ads_id = Item.ads_step_id
    _do_commit = False
    _sql_string_builder = New System.Text.StringBuilder()

    Try

      If Not GUI_BeginSQLTransaction(_sql_transaction) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      With _sql_string_builder
        .AppendLine("DELETE FROM   WKT_ADS")
        .AppendLine("      WHERE   AD_ID = @pAdId")
      End With

      _sql_cmd = New SqlCommand(_sql_string_builder.ToString(), _sql_transaction.Connection, _sql_transaction)
      _sql_cmd.Parameters.Add("@pAdId", SqlDbType.BigInt).Value = _ads_id
      _row_affected = _sql_cmd.ExecuteNonQuery()

      If _row_affected <> 1 Then
        _do_commit = False
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      Else
        _do_commit = True
      End If

      If Item.resource_id.HasValue Then
        If Not WSI.Common.WKTResources.Delete(Item.resource_id, _sql_transaction) Then
          _do_commit = False
          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If
      End If

      'SDS 27-07-2016 add fields for app mobile PBI 15957
      If Item.ad_imageDetailResource_id.HasValue Then
        If Not WSI.Common.WKTResources.Delete(Item.ad_imageDetailResource_id, _sql_transaction) Then
          _do_commit = False
          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If
      End If

      If Item.ad_imageListResource_id.HasValue Then
        If Not WSI.Common.WKTResources.Delete(Item.ad_imageListResource_id, _sql_transaction) Then
          _do_commit = False
          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If
      End If

    Catch ex As Exception

      _do_commit = False

    Finally

      GUI_EndSQLTransaction(_sql_transaction, _do_commit)
    End Try

    If _do_commit Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
    Else
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

  End Function ' DeleteAds

  '----------------------------------------------------------------------------
  ' PURPOSE : Adds a advertisement object into DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - TYPE_ADS_STEPS
  '         - Context
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - CONFIGURATION_OK
  '     - CONFIGURATION_ERROR_DB
  '
  ' NOTES :

  Private Function InsertAds(ByVal Item As TYPE_ADS_STEPS,
                             ByVal Context As Integer) As Integer

    Dim _sql_string_builder As System.Text.StringBuilder
    Dim _sql_cmd As SqlCommand
    Dim _sql_transaction As System.Data.SqlClient.SqlTransaction
    Dim _row_affected As Integer
    Dim _do_commit As Boolean
    Dim _rc As Integer
    Dim _date_from2 As Object
    Dim _date_to2 As Object
    Dim _resource_id As Nullable(Of Long)
    Dim _para_wkt_ads_id As SqlParameter
    Dim _para_wkt_ads_steep_id As SqlParameter

    'SDS 27-07-2016 add fields for app mobile PBI 15957
    Dim _list_resource_id As Nullable(Of Long)
    Dim _detail_resource_id As Nullable(Of Long)

    _sql_string_builder = New StringBuilder()
    _sql_transaction = Nothing
    _row_affected = 0
    _do_commit = False
    _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

    Try

      If Not GUI_BeginSQLTransaction(_sql_transaction) Then

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      If Me.CheckAlredyExist(Item.name, _sql_transaction) Then
        _do_commit = False

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
      End If

      ' If advertisement has image, add to resources
      If Not IsNothing(Item.image) Then

        SaveImage(_resource_id, Item.image, _sql_transaction)
        If Not _resource_id.HasValue Then
          _do_commit = False

          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

      End If

      'SDS 27-07-2016 add fields for app mobile PBI 15957
      If Not IsNothing(Item.ad_imageDetail) Then

        SaveImage(_detail_resource_id, Item.ad_imageDetail, _sql_transaction)
        If Not _detail_resource_id.HasValue Then
          _do_commit = False

          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

      End If

      If Not IsNothing(Item.ad_imageList) Then

        SaveImage(_list_resource_id, Item.ad_imageList, _sql_transaction)
        If Not _list_resource_id.HasValue Then
          _do_commit = False

          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

      End If
      If IsNothing(Item.ad_abstract) Then
        Item.ad_abstract = ""
      End If

      With _sql_string_builder
        .AppendLine("INSERT INTO   WKT_ADS				  ")
        .AppendLine("            ( AD_NAME				  ")
        .AppendLine("            , AD_DESCRIPTION		")
        .AppendLine("            , AD_FROM				  ")
        .AppendLine("            , AD_TO					  ")
        .AppendLine("            , AD_ENABLED			  ")
        .AppendLine("            , AD_WEEKDAY			  ")
        .AppendLine("            , AD_TIME_FROM1		")
        .AppendLine("            , AD_TIME_TO1			")
        .AppendLine("            , AD_TIME_FROM2		")
        .AppendLine("            , AD_TIME_TO2			")
        .AppendLine("            , AD_PROMOBOX_ENABLED    ")


        If WSI.Common.GeneralParam.GetBoolean("Features", "WinUP", False) Then
          .AppendLine("            , AD_LIST_DESCRIPTION    ")
          .AppendLine("            , AD_WINUP_VALID_FROM    ")
          .AppendLine("            , AD_WINUP_VALID_TO      ")

          .AppendLine("            , AD_WINUP_ENABLED       ")
          .AppendLine("            , AD_WINUP_TITLE       ")

          'SDS 27-07-2016 add fields for app mobile PBI 15957
          .AppendLine("            , AD_TARGET		  	")
          .AppendLine("            , AD_ABSTRACT			")
          .AppendLine("            , AD_ORDER		    	")
          .AppendLine("            , AD_LIST_RESOURCE_ID			")
          .AppendLine("            , AD_DETAIL_RESOURCE_ID		")
          .AppendLine("            , AD_TARGET_SCHEMAID		")
          .AppendLine("            , AD_LAST_UPDATE_DATE		")
        End If


        .AppendLine("            )						      ")
        .AppendLine("     VALUES ( @pName				    ")
        .AppendLine("            , @pDescription		")
        .AppendLine("            , @pFrom				    ")
        .AppendLine("            , @pTo					    ")
        .AppendLine("            , @pEnabled				")
        .AppendLine("            , @pAdWeekday			")
        .AppendLine("            , @pAdTimeFrom1		")
        .AppendLine("            , @pAdTimeTo1			")
        .AppendLine("            , @pAdTimeFrom2		")
        .AppendLine("            , @pAdTimeTo2      ")
        .AppendLine("            , @pPromoboxEnabled     ")


        If WSI.Common.GeneralParam.GetBoolean("Features", "WinUP", False) Then
          .AppendLine("            , @pListDescription     ")
          .AppendLine("            , @pWinupValidFrom      ")
          .AppendLine("            , @pWinupValidTo        ")

          .AppendLine("            , @pWinupEnabled        ")
          .AppendLine("            , @pWinupTitle        ")

          'SDS 27-07-2016 add fields for app mobile PBI 15957
          .AppendLine("            , @pAdTarget       ")
          .AppendLine("            , @pAdAbstract     ")
          .AppendLine("            , @pAdOrder        ")
          .AppendLine("            , @pAdListResourceId      ")
          .AppendLine("            , @pAdDetailResourceId 	 ")
          .AppendLine("            , @pAdTargetSchemaId		")
          .AppendLine("            , @pAdLastUpdate		")
        End If

        .AppendLine("            ) ;		")
        .AppendLine("SET @pAdvId = SCOPE_IDENTITY();")
      End With

      _sql_cmd = New SqlCommand(_sql_string_builder.ToString(), _sql_transaction.Connection, _sql_transaction)
      _sql_cmd.Parameters.Add("@pName", SqlDbType.NVarChar).Value = Item.name
      _sql_cmd.Parameters.Add("@pDescription", SqlDbType.NVarChar).Value = Item.description
      _sql_cmd.Parameters.Add("@pFrom", SqlDbType.DateTime).Value = Item.initial_date
      _sql_cmd.Parameters.Add("@pTo", SqlDbType.DateTime).Value = Item.final_date
      _sql_cmd.Parameters.Add("@pEnabled", SqlDbType.Bit).Value = Item.enabled
      _sql_cmd.Parameters.Add("@pAdWeekday", SqlDbType.Int).Value = Item.weekday
      _sql_cmd.Parameters.Add("@pAdTimeFrom1", SqlDbType.Int).Value = Item.ad_time_from1
      _sql_cmd.Parameters.Add("@pAdTimeTo1", SqlDbType.Int).Value = Item.ad_time_to1
      _sql_cmd.Parameters.Add("@pPromoboxEnabled", SqlDbType.Bit).Value = Item.promobox_enabled


      If WSI.Common.GeneralParam.GetBoolean("Features", "WinUP", False) Then
        _sql_cmd.Parameters.Add("@pListDescription", SqlDbType.NVarChar).Value = Item.winup_list_description
        _sql_cmd.Parameters.Add("@pWinupValidFrom", SqlDbType.DateTime).Value = Item.winup_valid_from
        _sql_cmd.Parameters.Add("@pWinupValidTo", SqlDbType.DateTime).Value = Item.winup_valid_to
        _sql_cmd.Parameters.Add("@pWinupTitle", SqlDbType.NVarChar).Value = Item.winup_title
        _sql_cmd.Parameters.Add("@pWinupEnabled", SqlDbType.Bit).Value = Item.winup_enabled

        'SDS 27-07-2016 add fields for app mobile PBI 15957
        _sql_cmd.Parameters.Add("@pAdTarget", SqlDbType.Int).Value = Item.ad_target
        _sql_cmd.Parameters.Add("@pAdAbstract", SqlDbType.NVarChar).Value = Item.ad_abstract
        _sql_cmd.Parameters.Add("@pAdOrder", SqlDbType.Int).Value = Item.ad_order
        _sql_cmd.Parameters.Add("@pAdLastUpdate", SqlDbType.DateTime).Value = Date.Now
        If _list_resource_id.HasValue Then
          _sql_cmd.Parameters.Add("@pAdListResourceId", SqlDbType.BigInt).Value = _list_resource_id
        Else
          _sql_cmd.Parameters.Add("@pAdListResourceId", SqlDbType.BigInt).Value = DBNull.Value
        End If

        If _detail_resource_id.HasValue Then
          _sql_cmd.Parameters.Add("@pAdDetailResourceId", SqlDbType.BigInt).Value = _detail_resource_id
        Else
          _sql_cmd.Parameters.Add("@pAdDetailResourceId", SqlDbType.BigInt).Value = DBNull.Value
        End If

        _sql_cmd.Parameters.Add("@pAdTargetSchemaId", SqlDbType.Int).Value = Item.ad_target_schema

      End If

      _para_wkt_ads_id = _sql_cmd.Parameters.Add("@pAdvId", SqlDbType.BigInt)
      _para_wkt_ads_id.Direction = ParameterDirection.Output

      If (Me.HasSecondTime) Then
        _date_from2 = Item.ad_time_from2
        _date_to2 = Item.ad_time_to2
      Else
        _date_from2 = DBNull.Value
        _date_to2 = DBNull.Value
      End If
      _sql_cmd.Parameters.Add("@pAdTimeFrom2", SqlDbType.Int).Value = _date_from2
      _sql_cmd.Parameters.Add("@pAdTimeTo2", SqlDbType.Int).Value = _date_to2

      _row_affected = _sql_cmd.ExecuteNonQuery()

      If _row_affected <> 1 Or IsDBNull(_para_wkt_ads_id.Value) Then
        _do_commit = False
        _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

        Return _rc
      End If

      _sql_string_builder.Length = 0

      With _sql_string_builder
        .AppendLine("INSERT INTO   WKT_AD_STEPS  ")
        .AppendLine("            ( AS_AD_ID      ")
        .AppendLine("            , AS_STEP_ORDER ")
        .AppendLine("            , AS_DURATION   ")
        .AppendLine("            )               ")
        .AppendLine("     VALUES ( @pAdvId      ")
        .AppendLine("	           , @pOrder      ")
        .AppendLine("	           , @pDuration   ")
        .AppendLine("	           );             ")
        .AppendLine("SET @pAdvStepId = SCOPE_IDENTITY();")
      End With

      _sql_cmd = New SqlCommand(_sql_string_builder.ToString(), _sql_transaction.Connection, _sql_transaction)
      _sql_cmd.Parameters.Add("@pAdvId", SqlDbType.BigInt).Value = _para_wkt_ads_id.Value
      _sql_cmd.Parameters.Add("@pOrder", SqlDbType.Int).Value = 1
      _sql_cmd.Parameters.Add("@pDuration", SqlDbType.Int).Value = 600
      _para_wkt_ads_steep_id = _sql_cmd.Parameters.Add("@pAdvStepId", SqlDbType.BigInt)
      _para_wkt_ads_steep_id.Direction = ParameterDirection.Output

      _row_affected = _sql_cmd.ExecuteNonQuery()

      If _row_affected <> 1 Or IsDBNull(_para_wkt_ads_steep_id.Value) Then
        _do_commit = False
        _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

        Return _rc
      End If

      _sql_string_builder.Length = 0

      With _sql_string_builder
        .AppendLine("INSERT INTO   WKT_AD_STEP_DETAILS ")
        .AppendLine("            ( ASCR_STEP_ID ")
        .AppendLine("            , ASCR_ZORDER ")
        .AppendLine("            , ASCR_RECT_X ")
        .AppendLine("            , ASCR_RECT_Y ")
        .AppendLine("            , ASCR_RECT_WIDTH")
        .AppendLine("            , ASCR_RECT_HEIGHT")
        .AppendLine("            , ASCR_RESOURCE_ID)")
        .AppendLine("     VALUES ( @pAdvId")
        .AppendLine("            , @pZorder")
        .AppendLine("            , @pRectX")
        .AppendLine("            , @pRectY")
        .AppendLine("            , @pRectWidth")
        .AppendLine("            , @pRectHeight")
        .AppendLine("            , @pResourceID")
        .AppendLine("            );")
      End With

      _sql_cmd = New SqlCommand(_sql_string_builder.ToString(), _sql_transaction.Connection, _sql_transaction)
      _sql_cmd.Parameters.Add("@pAdvId", SqlDbType.BigInt).Value = _para_wkt_ads_steep_id.Value
      _sql_cmd.Parameters.Add("@pZorder", SqlDbType.Int).Value = 1
      _sql_cmd.Parameters.Add("@pRectX", SqlDbType.Int).Value = 0
      _sql_cmd.Parameters.Add("@pRectY", SqlDbType.Int).Value = 0

      If (_resource_id.HasValue) Then
        _sql_cmd.Parameters.Add("@pResourceID", SqlDbType.BigInt).Value = _resource_id
        _sql_cmd.Parameters.Add("@pRectWidth", SqlDbType.Int).Value = Item.image.Width
        _sql_cmd.Parameters.Add("@pRectHeight", SqlDbType.Int).Value = Item.image.Height
      Else
        _sql_cmd.Parameters.Add("@pResourceID", SqlDbType.BigInt).Value = DBNull.Value
        _sql_cmd.Parameters.Add("@pRectWidth", SqlDbType.Int).Value = 0
        _sql_cmd.Parameters.Add("@pRectHeight", SqlDbType.Int).Value = 0
      End If

      _row_affected = _sql_cmd.ExecuteNonQuery()

      If _row_affected <> 1 Then
        _do_commit = False
        _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        Return _rc
      Else
        _do_commit = True
        _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_OK
      End If

    Catch ex As Exception
      _do_commit = False
      _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

    Finally

      GUI_EndSQLTransaction(_sql_transaction, _do_commit)

    End Try

    Return _rc

  End Function ' InsertArea

  '----------------------------------------------------------------------------
  ' PURPOSE : Updates the given advertisement object into the DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - TYPE_ADS_STEPS
  '         - Context
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - CONFIGURATION_OK
  '     - CONFIGURATION_ERROR_DB
  '
  ' NOTES :

  Private Function UpdateAds(ByVal Item As TYPE_ADS_STEPS,
                             ByVal Context As Integer) As Integer

    Dim _sql_string_builder As System.Text.StringBuilder
    Dim _sql_cmd As SqlCommand
    Dim _sql_transaction As System.Data.SqlClient.SqlTransaction
    Dim _row_affected As Integer
    Dim _do_commit As Boolean
    Dim _date_from2 As Object
    Dim _date_to2 As Object
    Dim _rc As Integer

    _sql_string_builder = New System.Text.StringBuilder()
    _sql_transaction = Nothing
    _row_affected = 0
    _do_commit = False

    Try

      If Not GUI_BeginSQLTransaction(_sql_transaction) Then
        _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

        Return _rc
      End If

      'SDS 27-07-2016 add fields for app mobile PBI 15957
      If Not IsNothing(Item.ad_imageDetail) Then
        SaveImage(Item.ad_imageDetailResource_id, Item.ad_imageDetail, _sql_transaction)
      End If
      If Not IsNothing(Item.ad_imageList) Then
        SaveImage(Item.ad_imageListResource_id, Item.ad_imageList, _sql_transaction)
      End If

      If IsNothing(Item.ad_abstract) Then
        Item.ad_abstract = ""
      End If

      With _sql_string_builder

        .AppendLine("UPDATE   WKT_ADS                        ")
        .AppendLine("   SET   AD_NAME        = @pName          ")
        .AppendLine("       , AD_DESCRIPTION = @pDescription   ")
        .AppendLine("       , AD_FROM        = @pFrom          ")
        .AppendLine("       , AD_TO          = @pTo             ")
        .AppendLine("       , AD_ENABLED     = @pEnable         ")
        .AppendLine("       , AD_WEEKDAY     = @pWeekday        ")
        .AppendLine("       , AD_TIME_FROM1  = @pTimeFrom1      ")
        .AppendLine("       , AD_TIME_TO1    = @pTimeTo1        ")
        .AppendLine("       , AD_TIME_FROM2  = @pTimeFrom2      ")
        .AppendLine("       , AD_TIME_TO2    = @pTimeTo2        ")
        .AppendLine("       , AD_PROMOBOX_ENABLED    = @pPromoboxEnabled        ")

        If WSI.Common.GeneralParam.GetBoolean("Features", "WinUP", False) Then
          'SDS 27-07-2016 add fields for app mobile PBI 15957
          .AppendLine("       , AD_LIST_DESCRIPTION    = @pListDescription        ")
          .AppendLine("       , AD_WINUP_VALID_FROM    = @pWinupValidFrom        ")
          .AppendLine("       , AD_WINUP_VALID_TO    = @pWinupValidTo        ")

          .AppendLine("       , AD_WINUP_ENABLED    = @pWinupEnabled        ")
          .AppendLine("       , AD_WINUP_TITLE    = @pWinupTitle        ")
          .AppendLine("       , AD_TARGET		   = @pAdTarget      	")
          .AppendLine("       , AD_ABSTRACT		 = @pAdAbstract     ")
          .AppendLine("       , AD_ORDER		   = @pAdOrder       	")
          .AppendLine("       , AD_LIST_RESOURCE_ID	  = @pAdListResourceId		")
          .AppendLine("       , AD_DETAIL_RESOURCE_ID	= @pAdDetailResourceId	")
          .AppendLine("       , AD_TARGET_SCHEMAID	= @pAdTargetSchemaId	")
          .AppendLine("       , AD_LAST_UPDATE_DATE	= @pAdLastUpdate	")
        End If


        .AppendLine(" WHERE   AD_ID          = @pId          ")

      End With

      _sql_cmd = New SqlCommand(_sql_string_builder.ToString(), _sql_transaction.Connection, _sql_transaction)
      _sql_cmd.Parameters.Add("@pName", SqlDbType.NVarChar).Value = Item.name
      _sql_cmd.Parameters.Add("@pDescription", SqlDbType.NVarChar).Value = Item.description
      _sql_cmd.Parameters.Add("@pFrom", SqlDbType.DateTime).Value = Item.initial_date
      _sql_cmd.Parameters.Add("@pTo", SqlDbType.DateTime).Value = Item.final_date
      _sql_cmd.Parameters.Add("@pEnable", SqlDbType.Bit).Value = Item.enabled
      _sql_cmd.Parameters.Add("@pWeekday", SqlDbType.Int).Value = Item.weekday
      _sql_cmd.Parameters.Add("@pTimeFrom1", SqlDbType.Int).Value = Item.ad_time_from1
      _sql_cmd.Parameters.Add("@pTimeTo1", SqlDbType.Int).Value = Item.ad_time_to1
      _sql_cmd.Parameters.Add("@pPromoboxEnabled", SqlDbType.Bit).Value = Item.promobox_enabled

      If WSI.Common.GeneralParam.GetBoolean("Features", "WinUP", False) Then
        'SDS 27-07-2016 add fields for app mobile PBI 15957
        _sql_cmd.Parameters.Add("@pListDescription", SqlDbType.NVarChar).Value = Item.winup_list_description
        _sql_cmd.Parameters.Add("@pWinupValidFrom", SqlDbType.DateTime).Value = Item.winup_valid_from
        _sql_cmd.Parameters.Add("@pWinupValidTo", SqlDbType.DateTime).Value = Item.winup_valid_to

        _sql_cmd.Parameters.Add("@pWinupEnabled", SqlDbType.Bit).Value = Item.winup_enabled
        _sql_cmd.Parameters.Add("@pWinupTitle", SqlDbType.NVarChar).Value = Item.winup_title

        _sql_cmd.Parameters.Add("@pAdTarget", SqlDbType.Int).Value = Item.ad_target
        _sql_cmd.Parameters.Add("@pAdAbstract", SqlDbType.NVarChar).Value = Item.ad_abstract
        _sql_cmd.Parameters.Add("@pAdOrder", SqlDbType.Int).Value = Item.ad_order
        _sql_cmd.Parameters.Add("@pAdTargetSchemaId", SqlDbType.Int).Value = Item.ad_target_schema
        _sql_cmd.Parameters.Add("@pAdLastUpdate", SqlDbType.DateTime).Value = WGDB.Now

        If Item.ad_imageListResource_id.HasValue Then
          _sql_cmd.Parameters.Add("@pAdListResourceId", SqlDbType.BigInt).Value = Item.ad_imageListResource_id
        Else
          _sql_cmd.Parameters.Add("@pAdListResourceId", SqlDbType.BigInt).Value = DBNull.Value
        End If

        If Item.ad_imageDetailResource_id.HasValue Then
          _sql_cmd.Parameters.Add("@pAdDetailResourceId", SqlDbType.BigInt).Value = Item.ad_imageDetailResource_id
        Else
          _sql_cmd.Parameters.Add("@pAdDetailResourceId", SqlDbType.BigInt).Value = DBNull.Value
        End If

      End If

      If (Me.HasSecondTime) Then
        _date_from2 = Item.ad_time_from2
        _date_to2 = Item.ad_time_to2
      Else
        _date_from2 = DBNull.Value
        _date_to2 = DBNull.Value
      End If

      _sql_cmd.Parameters.Add("@pTimeFrom2", SqlDbType.Int).Value = _date_from2
      _sql_cmd.Parameters.Add("@pTimeTo2", SqlDbType.Int).Value = _date_to2
      _sql_cmd.Parameters.Add("@pId", SqlDbType.BigInt).Value = Item.ads_step_id

      _row_affected = _sql_cmd.ExecuteNonQuery()

      If _row_affected <> 1 Then
        _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

        Return _rc
      End If

      If Not IsNothing(Item.image) Then
        SaveImage(Item.resource_id, Item.image, _sql_transaction)
      End If

      _sql_string_builder.Length = 0
      With _sql_string_builder
        .AppendLine("UPDATE   STEP_DETAILS                                         ")
        .AppendLine("   SET   ASCR_RESOURCE_ID = @pResourceId                      ")
        .AppendLine("  FROM   WKT_AD_STEP_DETAILS STEP_DETAILS,WKT_AD_STEPS STEPS  ")
        .AppendLine(" WHERE   STEP_DETAILS.ASCR_STEP_ID = STEPS.AS_STEP_ID         ")
        .AppendLine("   AND   AS_AD_ID = @pId                                      ")
      End With

      _sql_cmd = New SqlCommand(_sql_string_builder.ToString(), _sql_transaction.Connection, _sql_transaction)
      _sql_cmd.Parameters.Add("@pId", SqlDbType.BigInt).Value = Item.ads_step_id

      If Item.resource_id.HasValue Then
        _sql_cmd.Parameters.Add("@pResourceId", SqlDbType.BigInt).Value = Item.resource_id
      Else
        _sql_cmd.Parameters.Add("@pResourceId", SqlDbType.BigInt).Value = DBNull.Value
      End If

      _row_affected = _sql_cmd.ExecuteNonQuery()
      If _row_affected <> 1 Then
        _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

        Return _rc
      Else
        _do_commit = True
        _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_OK
      End If

    Catch ex As Exception
      _do_commit = False

    Finally

      GUI_EndSQLTransaction(_sql_transaction, _do_commit)

    End Try

    Return _rc

  End Function ' UpdateArea

  '----------------------------------------------------------------------------
  ' PURPOSE : Check if alredy exist row in database
  '
  ' PARAMS :
  '     - INPUT :
  '         - Name
  '         - Transaction
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - TRUE , Row alerdy exist
  '     - FALSE , Row not exist!
  '
  ' NOTES :

  Private Function CheckAlredyExist(ByVal Name As String, ByVal SqlTrx As SqlTransaction) As Boolean

    Dim _sql_cmd As SqlCommand
    Dim _sql_string As String
    Dim _row_count As Boolean

    _sql_string = "SELECT COUNT(AD_ID) FROM WKT_ADS WHERE AD_NAME = @pName"
    _sql_cmd = New SqlCommand(_sql_string, SqlTrx.Connection, SqlTrx)
    _sql_cmd.Parameters.Add("@pName", SqlDbType.NVarChar).Value = Name
    _row_count = _sql_cmd.ExecuteScalar()

    If _row_count <> 0 Then
      Return True
    End If

    Return False

  End Function

#End Region

End Class
