'-------------------------------------------------------------------
' Copyright � 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_draw.vb
' DESCRIPTION:   Draw class for user edition
' AUTHOR:        Raul Cervera
' CREATION DATE: 09-JUN-2010
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 09-JUN-2010  RCI    Initial version.
' 02-MAY-2011  LCM    Special and Provider sections added.
' 20-OCT-2011  MPO    add new fields: Limit per operation and first cash in constrained
' 21-DEC-2011  RCI    Added Bingo format flag
' 18-JAN-2012  RCI    Solved bug when only are ProviderList (not OfferList), the Offer is set with the ProviderList value.
' 23-MAY-2012  MPO    Set or Get the image and icon of the gift. Save/Retrieve of the db.
' 18-JAN-2013  QMP    OfferAuditString: changed capitalization
' 23-APR-2013  QMP    Fixed Bug #715: Raffles with awarded tickets can be deleted
' 24-MAY-2003  HBB    It is changed the holder account type to level filter
' 20-SEP-2013  DRV    Added points per level
' 07-APR-2014  JBP    Added IsVip property.
'-------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports WSI.Common
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient

Public Class CLASS_DRAW
  Inherits CLASS_BASE

#Region " Constants "

  ' Fields length
  Public Const MAX_NAME_LEN As Integer = 50

  Public Const DEFAULT_MAX_NUMBERS_PER_VOUCHER As Integer = 20
  Public Const MAX_NUMBERS_PER_VOUCHER As Integer = 300

  ' Database codes
  Private Const DR_STATUS_OPENED As Integer = 0
  Private Const DR_STATUS_CLOSED As Integer = 1
  Private Const DR_STATUS_DISABLED As Integer = 2

#End Region ' Constants

#Region " Enums "

#End Region ' Enums 

#Region " GUI_Configuration.dll "

#Region " Structures "

  <StructLayout(LayoutKind.Sequential)> _
   Public Class TYPE_DRAW
    Public draw_id As Integer
    Public name As String
    Public starting_date As New TYPE_DATE_TIME
    Public ending_date As New TYPE_DATE_TIME
    Public last_number As Integer
    Public initial_number As Long
    Public max_number As Long
    Public number_price As Double
    Public min_cash_in As Double
    Public min_played_pct As Double
    Public min_spent_pct As Double
    Public status As Integer
    Public limited As Boolean
    Public limit As Integer
    Public limit_per_voucher As Integer
    Public credit_type As DrawType
    Public header As String
    Public footer As String
    Public detail1 As String
    Public detail2 As String
    Public detail3 As String
    Public provider_list As New ProviderList
    Public offer_list As New DrawOfferList
    Public points_level1 As Double
    Public points_level2 As Double
    Public points_level3 As Double
    Public points_level4 As Double
    Public level_filter As Int32
    Public gender_filter As DrawGenderFilter
    Public first_cash_in_constrained As Boolean
    Public limit_per_operation As Double
    Public bingo_format As Boolean
    Public image As Image
    Public icon As Image
    Public small_resource_id As Nullable(Of Long)
    Public large_resource_id As Nullable(Of Long)
    Public visible_on_promobox As Integer
    Public award_on_promobox As Integer
    Public text_on_promobox As String
    Public is_vip As Boolean
  End Class

#End Region ' Structures

#End Region ' GUI_Configuration.dll

#Region " Members "

  Protected m_draw As New TYPE_DRAW

#End Region ' Members

#Region " Properties "

  Public Property DrawId() As Integer
    Get
      Return m_draw.draw_id
    End Get
    Set(ByVal Value As Integer)
      m_draw.draw_id = Value
    End Set
  End Property

  Public Property Name() As String
    Get
      Return m_draw.name
    End Get
    Set(ByVal Value As String)
      m_draw.name = Value
    End Set
  End Property

  Public Property StartingDate() As Date
    Get
      If m_draw.starting_date.IsNull Then
        Return GUI_GetDate()
      Else
        Return m_draw.starting_date.Value
      End If
    End Get
    Set(ByVal Value As Date)
      If IsNothing(m_draw.starting_date) Then
        m_draw.starting_date = New TYPE_DATE_TIME
      End If
      m_draw.starting_date.Value = Value
    End Set
  End Property

  Public Property EndingDate() As Date
    Get
      If m_draw.ending_date.IsNull Then
        Return GUI_GetDate()
      Else
        Return m_draw.ending_date.Value
      End If
    End Get
    Set(ByVal Value As Date)
      If IsNothing(m_draw.ending_date) Then
        m_draw.ending_date = New TYPE_DATE_TIME
      End If
      m_draw.ending_date.Value = Value
    End Set
  End Property

  Public Property LastNumber() As Integer
    Get
      Return m_draw.last_number
    End Get
    Set(ByVal Value As Integer)
      m_draw.last_number = Value
    End Set
  End Property

  Public Property InitialNumber() As Long
    Get
      Return m_draw.initial_number
    End Get
    Set(ByVal Value As Long)
      m_draw.initial_number = Value
    End Set
  End Property

  Public Property MaxNumber() As Long
    Get
      Return m_draw.max_number
    End Get
    Set(ByVal Value As Long)
      m_draw.max_number = Value
    End Set
  End Property

  Public Property NumberPrice() As Double
    Get
      Return m_draw.number_price
    End Get
    Set(ByVal Value As Double)
      m_draw.number_price = Value
    End Set
  End Property

  Public Property MinCashIn() As Double
    Get
      Return m_draw.min_cash_in
    End Get
    Set(ByVal Value As Double)
      m_draw.min_cash_in = Value
    End Set
  End Property

  Public Property MinPlayedPct() As Double
    Get
      Return m_draw.min_played_pct
    End Get
    Set(ByVal Value As Double)
      m_draw.min_played_pct = Value
    End Set
  End Property

  Public Property MinSpentPct() As Double
    Get
      Return m_draw.min_spent_pct
    End Get
    Set(ByVal Value As Double)
      m_draw.min_spent_pct = Value
    End Set
  End Property

  Public Property FirstCashInConstrained() As Boolean
    Get
      Return m_draw.first_cash_in_constrained
    End Get
    Set(ByVal value As Boolean)
      m_draw.first_cash_in_constrained = value
    End Set
  End Property

  Public Property Status() As Integer
    Get
      Return m_draw.status
    End Get
    Set(ByVal Value As Integer)
      m_draw.status = Value
    End Set
  End Property

  Public Property Limited() As Boolean
    Get
      Return m_draw.limited
    End Get
    Set(ByVal Value As Boolean)
      m_draw.limited = Value
    End Set
  End Property

  Public Property Limit() As Integer
    Get
      Return m_draw.limit
    End Get
    Set(ByVal Value As Integer)
      m_draw.limit = Value
    End Set
  End Property

  Public Property LimitPerVoucher() As Integer
    Get
      Return m_draw.limit_per_voucher
    End Get
    Set(ByVal Value As Integer)
      m_draw.limit_per_voucher = Value
    End Set
  End Property

  Public Property LimitPerOperation() As Integer
    Get
      Return m_draw.limit_per_operation
    End Get
    Set(ByVal value As Integer)
      m_draw.limit_per_operation = value
    End Set
  End Property

  Public Property CreditType() As DrawType
    Get
      Return m_draw.credit_type
    End Get
    Set(ByVal Value As DrawType)
      m_draw.credit_type = Value
    End Set
  End Property

  Public Property LevelFilter() As Int32
    Get
      Return m_draw.level_filter
    End Get
    Set(ByVal Value As Int32)
      m_draw.level_filter = Value
    End Set
  End Property

  Public Property Header() As String
    Get
      Return m_draw.header
    End Get
    Set(ByVal Value As String)
      m_draw.header = Value
    End Set
  End Property

  Public Property Footer() As String
    Get
      Return m_draw.footer
    End Get
    Set(ByVal Value As String)
      m_draw.footer = Value
    End Set
  End Property

  Public Property Detail1() As String
    Get
      Return m_draw.detail1
    End Get
    Set(ByVal Value As String)
      m_draw.detail1 = Value
    End Set
  End Property

  Public Property Detail2() As String
    Get
      Return m_draw.detail2
    End Get
    Set(ByVal Value As String)
      m_draw.detail2 = Value
    End Set
  End Property

  Public Property Detail3() As String
    Get
      Return m_draw.detail3
    End Get
    Set(ByVal Value As String)
      m_draw.detail3 = Value
    End Set
  End Property

  Public ReadOnly Property ProviderList() As ProviderList
    Get
      Return m_draw.provider_list
    End Get
  End Property

  Public ReadOnly Property OfferList() As DrawOfferList
    Get
      Return m_draw.offer_list
    End Get
  End Property

  Public Property GenderFilter() As DrawGenderFilter
    Get
      Return m_draw.gender_filter
    End Get
    Set(ByVal Value As DrawGenderFilter)
      m_draw.gender_filter = Value
    End Set
  End Property

  Public Property HasBingoFormat() As Boolean
    Get
      Return m_draw.bingo_format
    End Get
    Set(ByVal Value As Boolean)
      m_draw.bingo_format = Value
    End Set
  End Property

  Public Property Image() As Image
    Get
      Return Me.m_draw.image
    End Get
    Set(ByVal value As Image)
      Me.m_draw.image = value
    End Set
  End Property

  Public Property Icon() As Image
    Get
      Return Me.m_draw.icon
    End Get
    Set(ByVal value As Image)
      Me.m_draw.icon = value
    End Set
  End Property

  Public Property PointsLevel1() As Double
    Get
      Return Me.m_draw.points_level1
    End Get
    Set(ByVal value As Double)
      Me.m_draw.points_level1 = value
    End Set
  End Property

  Public Property PointsLevel2() As Double
    Get
      Return Me.m_draw.points_level2
    End Get
    Set(ByVal value As Double)
      Me.m_draw.points_level2 = value
    End Set
  End Property

  Public Property PointsLevel3() As Double
    Get
      Return Me.m_draw.points_level3
    End Get
    Set(ByVal value As Double)
      Me.m_draw.points_level3 = value
    End Set
  End Property

  Public Property PointsLevel4() As Double
    Get
      Return Me.m_draw.points_level4
    End Get
    Set(ByVal value As Double)
      Me.m_draw.points_level4 = value
    End Set
  End Property

  Public Property VisibleOnPromoBOX() As Boolean
    Get
      Return m_draw.visible_on_promobox
    End Get
    Set(ByVal Value As Boolean)
      m_draw.visible_on_promobox = Value
    End Set
  End Property

  Public Property AwardOnPromoBOX() As Boolean
    Get
      Return m_draw.award_on_promobox
    End Get
    Set(ByVal Value As Boolean)
      m_draw.award_on_promobox = Value
    End Set
  End Property

  Public Property TextOnPromoBOX() As String
    Get
      Return m_draw.text_on_promobox
    End Get
    Set(ByVal Value As String)
      m_draw.text_on_promobox = Value
    End Set
  End Property

  Public Property IsVip() As Boolean
    Get
      Return m_draw.is_vip
    End Get
    Set(ByVal Value As Boolean)
      m_draw.is_vip = Value
    End Set
  End Property

#End Region ' Properties

#Region " Overrides functions "

  '----------------------------------------------------------------------------
  ' PURPOSE: Reads from the database into the given object
  '
  ' PARAMS:
  '   - INPUT:
  '     - ObjectId: An object, it must be 'casted' to the desired KEY.
  '
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim rc As Integer

    Me.DrawId = ObjectId

    ' Read draw data
    rc = ReadDraw(m_draw, SqlCtx)

    Select Case rc

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function 'DB_Read

  '----------------------------------------------------------------------------
  ' PURPOSE: Updates the given object to the database.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim rc As Integer

    ' Update draw data
    rc = UpdateDraw(m_draw, SqlCtx)

    Select Case rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
        Return ENUM_STATUS.STATUS_DUPLICATE_KEY

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function 'DB_Update

  '----------------------------------------------------------------------------
  ' PURPOSE: Inserts the given object to the database.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim rc As Integer

    ' Insert draw data
    rc = InsertDraw(m_draw, SqlCtx)

    Select Case rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
        Return ENUM_STATUS.STATUS_DUPLICATE_KEY

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function 'DB_Insert

  '----------------------------------------------------------------------------
  ' PURPOSE: Deletes the given object from the database.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim rc As Integer

    ' Delete draw data
    rc = DeleteDraw(Me.DrawId, Me.m_draw.small_resource_id, Me.m_draw.large_resource_id, SqlCtx)

    Select Case rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DEPENDENCIES
        Return ENUM_STATUS.STATUS_DEPENDENCIES

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function 'DB_Delete

  '----------------------------------------------------------------------------
  ' PURPOSE: Returns a duplicate of the given object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim temp_draw As CLASS_DRAW

    temp_draw = New CLASS_DRAW
    temp_draw.m_draw.draw_id = Me.m_draw.draw_id
    temp_draw.m_draw.name = Me.m_draw.name
    temp_draw.m_draw.last_number = Me.m_draw.last_number
    temp_draw.m_draw.initial_number = Me.m_draw.initial_number
    temp_draw.m_draw.max_number = Me.m_draw.max_number
    temp_draw.m_draw.number_price = Me.m_draw.number_price
    temp_draw.m_draw.min_cash_in = Me.m_draw.min_cash_in
    temp_draw.m_draw.min_played_pct = Me.m_draw.min_played_pct
    temp_draw.m_draw.min_spent_pct = Me.m_draw.min_spent_pct
    temp_draw.m_draw.first_cash_in_constrained = Me.m_draw.first_cash_in_constrained
    temp_draw.m_draw.status = Me.m_draw.status
    temp_draw.m_draw.limited = Me.m_draw.limited
    temp_draw.m_draw.limit = Me.m_draw.limit
    temp_draw.m_draw.limit_per_voucher = Me.m_draw.limit_per_voucher
    temp_draw.m_draw.limit_per_operation = Me.m_draw.limit_per_operation
    temp_draw.m_draw.credit_type = Me.m_draw.credit_type
    temp_draw.m_draw.header = Me.m_draw.header
    temp_draw.m_draw.footer = Me.m_draw.footer
    temp_draw.m_draw.detail1 = Me.m_draw.detail1
    temp_draw.m_draw.detail2 = Me.m_draw.detail2
    temp_draw.m_draw.detail3 = Me.m_draw.detail3

    temp_draw.m_draw.provider_list = New WSI.Common.ProviderList()
    temp_draw.m_draw.provider_list.FromXml(Me.m_draw.provider_list.ToXml())

    temp_draw.m_draw.offer_list = New WSI.Common.DrawOfferList()
    temp_draw.m_draw.offer_list.FromXml(Me.m_draw.offer_list.ToXml())

    If Me.m_draw.starting_date.IsNull Then
      temp_draw.m_draw.starting_date.IsNull = True
    Else
      temp_draw.m_draw.starting_date.Value = Me.m_draw.starting_date.Value
    End If
    If Me.m_draw.ending_date.IsNull Then
      temp_draw.m_draw.ending_date.IsNull = True
    Else
      temp_draw.m_draw.ending_date.Value = Me.m_draw.ending_date.Value
    End If

    temp_draw.m_draw.points_level1 = Me.m_draw.points_level1
    temp_draw.m_draw.points_level2 = Me.m_draw.points_level2
    temp_draw.m_draw.points_level3 = Me.m_draw.points_level3
    temp_draw.m_draw.points_level4 = Me.m_draw.points_level4

    temp_draw.m_draw.level_filter = Me.m_draw.level_filter
    temp_draw.m_draw.gender_filter = Me.m_draw.gender_filter
    temp_draw.m_draw.bingo_format = Me.m_draw.bingo_format
    temp_draw.m_draw.large_resource_id = Me.m_draw.large_resource_id
    temp_draw.m_draw.small_resource_id = Me.m_draw.small_resource_id

    If IsNothing(Me.m_draw.icon) Then
      temp_draw.Icon = Nothing
    Else
      temp_draw.Icon = Me.m_draw.icon.Clone
    End If

    If IsNothing(Me.m_draw.image) Then
      temp_draw.Image = Nothing
    Else
      temp_draw.Image = Me.m_draw.image.Clone
    End If

    temp_draw.VisibleOnPromoBOX = Me.m_draw.visible_on_promobox
    temp_draw.AwardOnPromoBOX = Me.m_draw.award_on_promobox
    temp_draw.TextOnPromoBOX = Me.m_draw.text_on_promobox

    temp_draw.IsVip = Me.m_draw.is_vip

    Return temp_draw

  End Function 'Duplicate

  '----------------------------------------------------------------------------
  ' PURPOSE: Returns the related auditor data for the current object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim auditor_data As CLASS_AUDITOR_DATA
    Dim string_date As String
    Dim gender_filter As String
    Dim _yes_text As String
    Dim _no_text As String

    ' Create a new auditor_code for draw manipulation
    auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_DRAWS)
    Call auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(319), Me.Name)

    _yes_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(278)
    _no_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)

    ' Name
    Call auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(323), Me.Name)

    ' Starting Date
    If Me.StartingDate = Nothing Then
      string_date = AUDIT_NONE_STRING
    Else
      string_date = GUI_FormatDate(Me.StartingDate, _
                                   ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If
    Call auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(307), string_date)

    ' Ending Date
    If Me.EndingDate = Nothing Then
      string_date = AUDIT_NONE_STRING
    Else
      string_date = GUI_FormatDate(Me.EndingDate, _
                                   ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(277), string_date)

    ' Last Number
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(317), IIf(Me.LastNumber = -1, "---", GUI_FormatNumber(Me.LastNumber, 0)))

    ' Number Price
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(318), GUI_FormatCurrency(Me.NumberPrice, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT))

    ' Status
    Select Case Me.Status
      Case DR_STATUS_OPENED
        Call auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(261), GLB_NLS_GUI_PLAYER_TRACKING.GetString(314))

      Case DR_STATUS_CLOSED
        Call auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(261), GLB_NLS_GUI_PLAYER_TRACKING.GetString(315))

      Case DR_STATUS_DISABLED
        Call auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(261), GLB_NLS_GUI_PLAYER_TRACKING.GetString(316))
    End Select

    ' Limited
    If Me.Limited Then
      Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(332), GUI_FormatNumber(Me.Limit, 0))
    Else
      Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(332), GLB_NLS_GUI_PLAYER_TRACKING.GetString(333))
    End If

    ' Limit per Voucher
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(337), GUI_FormatNumber(Me.LimitPerVoucher, 0))

    ' Limit per Operation
    If Me.LimitPerOperation = 0 Then
      Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(535), GLB_NLS_GUI_CONFIGURATION.GetString(328))
    Else
      Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(535), GUI_FormatNumber(Me.LimitPerOperation, 0))
    End If

    ' Min CashIn, Min Played & Min Spent
    Call auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(374), GUI_FormatNumber(Me.MinCashIn, 2))
    If Me.MinPlayedPct = 0 Then
      Call auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(339), GLB_NLS_GUI_CONFIGURATION.GetString(328))
    Else
      Call auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(339), GUI_FormatNumber(Me.MinPlayedPct, 2))
    End If
    If Me.MinSpentPct = 0 Then
      Call auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(340), GLB_NLS_GUI_CONFIGURATION.GetString(328))
    Else
      Call auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(340), GUI_FormatNumber(Me.MinSpentPct, 2))
    End If
    If Me.FirstCashInConstrained Then
      Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(536), GLB_NLS_GUI_CONFIGURATION.GetString(328))
    Else
      Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(536), GLB_NLS_GUI_CONFIGURATION.GetString(311))
    End If

    ' Initial and max number
    Call auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(300), GUI_FormatNumber(Me.InitialNumber, 0))
    Call auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(301), GUI_FormatNumber(Me.MaxNumber, 0))

    Select Case CreditType
      Case WSI.Common.DrawType.BY_PLAYED_CREDIT
        Call auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(299), GLB_NLS_GUI_CONFIGURATION.GetString(302))

      Case WSI.Common.DrawType.BY_REDEEMABLE_SPENT
        Call auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(299), GLB_NLS_GUI_CONFIGURATION.GetString(303))

      Case WSI.Common.DrawType.BY_TOTAL_SPENT
        Call auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(299), GLB_NLS_GUI_CONFIGURATION.GetString(313))

      Case WSI.Common.DrawType.BY_POINTS
        Call auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(299), GLB_NLS_GUI_CONFIGURATION.GetString(329))

      Case WSI.Common.DrawType.BY_CASH_IN
        Call auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(299), GLB_NLS_GUI_CONFIGURATION.GetString(330))
    End Select

    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(372), IIf(Header = "", "---", Header))
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(373), IIf(Footer = "", "---", Footer))
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(392), IIf(Detail1 = "", "---", Detail1))
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(393), IIf(Detail2 = "", "---", Detail2))
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(394), IIf(Detail3 = "", "---", Detail3))

    'Offers
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(471), OfferListAuditString(GLB_NLS_GUI_PLAYER_TRACKING.GetString(325), GLB_NLS_GUI_PLAYER_TRACKING.GetString(326), GLB_NLS_GUI_PLAYER_TRACKING.GetString(327), GLB_NLS_GUI_PLAYER_TRACKING.GetString(328), GLB_NLS_GUI_PLAYER_TRACKING.GetString(329), GLB_NLS_GUI_PLAYER_TRACKING.GetString(330), GLB_NLS_GUI_PLAYER_TRACKING.GetString(331), GLB_NLS_GUI_PLAYER_TRACKING.GetString(480), GLB_NLS_GUI_PLAYER_TRACKING.GetString(481), GLB_NLS_GUI_PLAYER_TRACKING.GetString(479), GLB_NLS_GUI_PLAYER_TRACKING.GetString(482), GLB_NLS_GUI_PLAYER_TRACKING.GetString(483), GLB_NLS_GUI_PLAYER_TRACKING.GetString(484), GLB_NLS_GUI_PLAYER_TRACKING.GetString(500)))

    'Providers
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(475), ProviderList.AuditString(GLB_NLS_GUI_CONFIGURATION.GetString(497), GLB_NLS_GUI_CONFIGURATION.GetString(499), GLB_NLS_GUI_CONFIGURATION.GetString(498), GLB_NLS_GUI_PLAYER_TRACKING.GetString(501)))

    ' Number points price
    Call auditor_data.SetField(0, IIf(Me.m_draw.points_level1 = -1, "---", Me.m_draw.points_level1), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2624, WSI.Common.GeneralParam.GetString("PlayerTracking", "Level01.Name")))
    Call auditor_data.SetField(0, IIf(Me.m_draw.points_level2 = -1, "---", Me.m_draw.points_level2), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2624, WSI.Common.GeneralParam.GetString("PlayerTracking", "Level02.Name")))
    Call auditor_data.SetField(0, IIf(Me.m_draw.points_level3 = -1, "---", Me.m_draw.points_level3), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2624, WSI.Common.GeneralParam.GetString("PlayerTracking", "Level03.Name")))
    Call auditor_data.SetField(0, IIf(Me.m_draw.points_level4 = -1, "---", Me.m_draw.points_level4), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2624, WSI.Common.GeneralParam.GetString("PlayerTracking", "Level04.Name")))

    ' Level Filter
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(404), LevelFilterText(Me.LevelFilter))

    ' Gender Filter
    gender_filter = AUDIT_NONE_STRING
    Select Case Me.GenderFilter
      Case WSI.Common.DrawGenderFilter.ALL
        gender_filter = AUDIT_NONE_STRING

      Case WSI.Common.DrawGenderFilter.MEN_ONLY
        gender_filter = GLB_NLS_GUI_PLAYER_TRACKING.GetString(214)

      Case WSI.Common.DrawGenderFilter.WOMEN_ONLY
        gender_filter = GLB_NLS_GUI_PLAYER_TRACKING.GetString(215)

    End Select
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(213), gender_filter)

    ' Bingo Format
    If Me.HasBingoFormat Then
      Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(560), GLB_NLS_GUI_CONFIGURATION.GetString(264))
    Else
      Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(560), GLB_NLS_GUI_CONFIGURATION.GetString(265))
    End If

    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(719), GetInfoCRC(Me.Icon))
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(687), GetInfoCRC(Me.Image))

    ' JMM 30-APR-2014
    ' Visible on PromoBOX field
    If Me.VisibleOnPromoBOX = True Then
      Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2868), _yes_text)
    Else
      Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2868), _no_text)
    End If
    ' Awardable on PromoBOX field
    If Me.AwardOnPromoBOX = True Then
      Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2863), _yes_text)
    Else
      Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2863), _no_text)
    End If

    ' JMM 04-NOV-2013: 'TextOnPromoBOX' field added
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(4916), Me.TextOnPromoBOX)

    ' JBP 07-APR-2014: Draw is Vip
    auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(4804), IIf(Me.IsVip, _yes_text, _no_text))

    Return auditor_data

  End Function 'AuditorData

  '----------------------------------------------------------------------------
  ' PURPOSE: Reads last draw indicated by m_draw.credit_type from the database into the m_draw object
  '
  ' PARAMS:
  '   - INPUT:
  '
  '   - OUTPUT:
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Function GetDrawDefaultValues() As Integer
    Dim sql_connection As SqlConnection
    Dim sql_command As SqlCommand
    Dim str_sql As String
    Dim sql_reader As SqlDataReader
    Dim num_rows As Integer

    Try
      sql_connection = NewSQLConnection()

      If Not (sql_connection Is Nothing) Then
        ' Connected
        If sql_connection.State <> ConnectionState.Open Then
          ' Connecting ..
          sql_connection.Open()
        End If

        If Not sql_connection.State = ConnectionState.Open Then
          Exit Try
        End If
      End If

      str_sql = "SELECT TOP 1 " & _
                    "   DR_INITIAL_NUMBER " & _
                    " , DR_MAX_NUMBER " & _
                    " , ISNULL(DR_HEADER, ' ') AS DR_HEADER " & _
                    " , ISNULL(DR_FOOTER, ' ') AS DR_FOOTER " & _
                    " , ISNULL(DR_DETAIL1, ' ') AS DR_DETAIL1 " & _
                    " , ISNULL(DR_DETAIL2, ' ') AS DR_DETAIL2 " & _
                    " , ISNULL(DR_DETAIL3, ' ') AS DR_DETAIL3 " & _
                " FROM DRAWS " & _
                " WHERE DR_CREDIT_TYPE = @credit_type " & _
             " ORDER BY DR_ID DESC "

      sql_command = New SqlCommand(str_sql)
      sql_command.Connection = sql_connection

      sql_command.Parameters.Add("@credit_type", SqlDbType.Int).Value = m_draw.credit_type

      sql_reader = sql_command.ExecuteReader()

      num_rows = 0
      While sql_reader.Read()
        m_draw.initial_number = sql_reader("DR_INITIAL_NUMBER")
        m_draw.max_number = sql_reader("DR_MAX_NUMBER")
        m_draw.header = NullTrim(sql_reader("DR_HEADER"))
        m_draw.footer = NullTrim(sql_reader("DR_FOOTER"))
        m_draw.detail1 = NullTrim(sql_reader("DR_DETAIL1"))
        m_draw.detail2 = NullTrim(sql_reader("DR_DETAIL2"))
        m_draw.detail3 = NullTrim(sql_reader("DR_DETAIL3"))

        num_rows = num_rows + 1
      End While

      sql_reader.Close()

      If num_rows = 0 Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
      End If
      If num_rows <> 1 Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

    Catch ex As Exception
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
  End Function ' GetDrawDefaultValues

#End Region

#Region " Private Functions "

  '----------------------------------------------------------------------------
  ' PURPOSE: Reads the draw indicated by pDraw.draw_id from the database into the pDraw object
  '
  ' PARAMS:
  '   - INPUT:
  '     - pDraw As TYPE_DRAW (only field draw_id)
  '
  '   - OUTPUT:
  '     - pDraw As TYPE_DRAW (the rest of fields)
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Private Function ReadDraw(ByVal Draw As TYPE_DRAW, ByVal Context As Integer) As Integer
    Dim sql_connection As SqlConnection
    Dim sql_command As SqlCommand
    Dim str_sql As String
    Dim sql_reader As SqlDataReader
    Dim num_rows As Integer
    Dim _xml As String
    Dim _sql_trans As SqlTransaction

    Try
      sql_connection = NewSQLConnection()

      If Not (sql_connection Is Nothing) Then
        ' Connected
        If sql_connection.State <> ConnectionState.Open Then
          ' Connecting ..
          sql_connection.Open()
        End If

        If Not sql_connection.State = ConnectionState.Open Then
          Exit Try
        End If
      End If

      str_sql = "SELECT DR_NAME " & _
                    " , DR_STARTING_DATE " & _
                    " , DR_ENDING_DATE " & _
                    " , DR_LAST_NUMBER " & _
                    " , DR_NUMBER_PRICE " & _
                    " , DR_STATUS " & _
                    " , DR_LIMITED " & _
                    " , DR_LIMIT " & _
                    " , DR_LIMIT_PER_VOUCHER " & _
                    " , DR_INITIAL_NUMBER " & _
                    " , DR_MAX_NUMBER " & _
                    " , DR_CREDIT_TYPE " & _
                    " , ISNULL(DR_HEADER, ' ') AS DR_HEADER " & _
                    " , ISNULL(DR_FOOTER, ' ') AS DR_FOOTER " & _
                    " , ISNULL(DR_DETAIL1, ' ') AS DR_DETAIL1 " & _
                    " , ISNULL(DR_DETAIL2, ' ') AS DR_DETAIL2 " & _
                    " , ISNULL(DR_DETAIL3, ' ') AS DR_DETAIL3 " & _
                    " , DR_PROVIDER_LIST " & _
                    " , DR_OFFER_LIST " & _
                    " , ISNULL(DR_MIN_PLAYED_PCT, 0) AS DR_MIN_PLAYED_PCT " & _
                    " , ISNULL(DR_MIN_SPENT_PCT, 0) AS DR_MIN_SPENT_PCT " & _
                    " , ISNULL(DR_MIN_CASH_IN, 0) AS DR_MIN_CASH_IN " & _
                    " , DR_LEVEL_FILTER " & _
                    " , DR_GENDER_FILTER " & _
                    " , DR_FIRST_CASH_IN_CONSTRAINED " & _
                    " , DR_LIMIT_PER_OPERATION " & _
                    " , DR_BINGO_FORMAT " & _
                    " , DR_SMALL_RESOURCE_ID " & _
                    " , DR_LARGE_RESOURCE_ID " & _
                    " , ISNULL(DR_POINTS_LEVEL1, -1) AS DR_POINTS_LEVEL1 " & _
                    " , ISNULL(DR_POINTS_LEVEL2, -1) AS DR_POINTS_LEVEL2 " & _
                    " , ISNULL(DR_POINTS_LEVEL3, -1) AS DR_POINTS_LEVEL3 " & _
                    " , ISNULL(DR_POINTS_LEVEL4, -1) AS DR_POINTS_LEVEL4 " & _
                    " , DR_VISIBLE_ON_PROMOBOX " & _
                    " , DR_AWARD_ON_PROMOBOX " & _
                    " , ISNULL(DR_TEXT_ON_PROMOBOX, ' ') AS DR_TEXT_ON_PROMOBOX" & _
                    " , ISNULL(DR_VIP, 0) AS DR_VIP " & _
                " FROM DRAWS " & _
                " WHERE DR_ID = @draw_id"

      sql_command = New SqlCommand(str_sql)
      sql_command.Connection = sql_connection

      sql_command.Parameters.Add("@draw_id", SqlDbType.Int).Value = Draw.draw_id

      sql_reader = sql_command.ExecuteReader()

      num_rows = 0
      While sql_reader.Read()
        Draw.name = sql_reader("DR_NAME")
        Draw.starting_date.Value = sql_reader("DR_STARTING_DATE")
        Draw.ending_date.Value = sql_reader("DR_ENDING_DATE")
        Draw.last_number = sql_reader("DR_LAST_NUMBER")
        Draw.number_price = sql_reader("DR_NUMBER_PRICE")
        Draw.status = sql_reader("DR_STATUS")
        Draw.limited = sql_reader("DR_LIMITED")
        Draw.limit = sql_reader("DR_LIMIT")
        Draw.limit_per_voucher = sql_reader("DR_LIMIT_PER_VOUCHER")
        Draw.initial_number = sql_reader("DR_INITIAL_NUMBER")
        Draw.max_number = sql_reader("DR_MAX_NUMBER")
        Draw.credit_type = sql_reader("DR_CREDIT_TYPE")
        Draw.header = NullTrim(sql_reader("DR_HEADER"))
        Draw.footer = NullTrim(sql_reader("DR_FOOTER"))
        Draw.detail1 = NullTrim(sql_reader("DR_DETAIL1"))
        Draw.detail2 = NullTrim(sql_reader("DR_DETAIL2"))
        Draw.detail3 = NullTrim(sql_reader("DR_DETAIL3"))
        Draw.level_filter = sql_reader("DR_LEVEL_FILTER")
        Draw.gender_filter = sql_reader("DR_GENDER_FILTER")
        Draw.bingo_format = sql_reader("DR_BINGO_FORMAT")
        Draw.points_level1 = sql_reader("DR_POINTS_LEVEL1")
        Draw.points_level2 = sql_reader("DR_POINTS_LEVEL2")
        Draw.points_level3 = sql_reader("DR_POINTS_LEVEL3")
        Draw.points_level4 = sql_reader("DR_POINTS_LEVEL4")

        ' DR_PROVIDER_LIST
        _xml = Nothing
        If (Not sql_reader.IsDBNull(17)) Then
          _xml = sql_reader("DR_PROVIDER_LIST")
        End If
        Draw.provider_list.FromXml(_xml)

        'DR_OFFER_LIST
        _xml = Nothing
        If (Not sql_reader.IsDBNull(18)) Then
          _xml = sql_reader("DR_OFFER_LIST")
        End If
        Draw.offer_list.FromXml(_xml)

        Draw.min_cash_in = sql_reader("DR_MIN_CASH_IN")
        Draw.min_played_pct = sql_reader("DR_MIN_PLAYED_PCT")
        Draw.min_spent_pct = sql_reader("DR_MIN_SPENT_PCT")
        Draw.first_cash_in_constrained = sql_reader("DR_FIRST_CASH_IN_CONSTRAINED")
        Draw.limit_per_operation = sql_reader("DR_LIMIT_PER_OPERATION")

        If sql_reader.IsDBNull(27) Then
          Draw.icon = Nothing
          Draw.small_resource_id = Nothing
        Else
          Draw.small_resource_id = sql_reader.GetInt64(27)
        End If

        If sql_reader.IsDBNull(28) Then
          Draw.image = Nothing
          Draw.large_resource_id = Nothing
        Else
          Draw.large_resource_id = sql_reader.GetInt64(28)
        End If

        ' JMM 30-APR-2014
        ' Visible On PromoBOX check
        Draw.visible_on_promobox = sql_reader.GetBoolean(sql_reader.GetOrdinal("DR_VISIBLE_ON_PROMOBOX"))
        ' Award On PromoBOX check
        Draw.award_on_promobox = sql_reader.GetBoolean(sql_reader.GetOrdinal("DR_AWARD_ON_PROMOBOX"))
        ' Text On PromoBOX string
        Draw.text_on_promobox = NullTrim(sql_reader("DR_TEXT_ON_PROMOBOX"))

        'JBP 07-APR-2014: VIP
        Draw.is_vip = sql_reader("DR_VIP")

        num_rows = num_rows + 1
      End While

      sql_reader.Close()

      _sql_trans = sql_connection.BeginTransaction()

      If Draw.small_resource_id.HasValue Then
        Draw.icon = LoadImage(Draw.small_resource_id.Value, _sql_trans)
      End If

      If Draw.large_resource_id.HasValue Then
        Draw.image = LoadImage(Draw.large_resource_id.Value, _sql_trans)
      End If

      _sql_trans.Rollback()
      _sql_trans.Dispose()

      If num_rows = 0 Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
      End If
      If num_rows <> 1 Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

    Catch ex As Exception
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
  End Function ' ReadDraw

  '----------------------------------------------------------------------------
  ' PURPOSE: Updates the given Draw to the database
  '
  ' PARAMS:
  '   - INPUT:
  '     - pDraw As TYPE_DRAW
  '
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Private Function UpdateDraw(ByVal pDraw As TYPE_DRAW, ByVal Context As Integer) As Integer
    Dim sql_command As SqlCommand
    Dim str_sql As String
    Dim value As Object
    Dim db_count As Integer = 0
    Dim num_rows_updated As Integer
    Dim _xml As String = Nothing

    'XVV 16/04/2007
    'Assign Nothing to variable because compiler generate a warning
    Dim SqlTrans As SqlTransaction = Nothing

    Try
      If Not GUI_BeginSQLTransaction(SqlTrans) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      str_sql = "SELECT COUNT(*) " & _
                " FROM DRAWS " & _
                "WHERE DR_NAME = @name " & _
                "  AND DR_ID  <> @draw_id "

      sql_command = New SqlCommand(str_sql)
      sql_command.Transaction = SqlTrans
      sql_command.Connection = SqlTrans.Connection

      sql_command.Parameters.Add("@name", SqlDbType.NVarChar).Value = pDraw.name
      sql_command.Parameters.Add("@draw_id", SqlDbType.Int).Value = pDraw.draw_id

      value = sql_command.ExecuteScalar()
      If value IsNot Nothing And value IsNot DBNull.Value Then
        db_count = CInt(value)
      Else
        GUI_EndSQLTransaction(SqlTrans, False)

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If
      sql_command = Nothing

      If db_count > 0 Then
        GUI_EndSQLTransaction(SqlTrans, False)

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
      End If

      SaveImage(pDraw.large_resource_id, pDraw.image, SqlTrans)
      SaveImage(pDraw.small_resource_id, pDraw.icon, SqlTrans)

      str_sql = "UPDATE   DRAWS " & _
                  " SET   DR_NAME                       = @name " & _
                      " , DR_STARTING_DATE              = @starting_date " & _
                      " , DR_ENDING_DATE                = @ending_date " & _
                      " , DR_NUMBER_PRICE               = @number_price " & _
                      " , DR_STATUS                     = @status " & _
                      " , DR_LIMITED                    = @limited " & _
                      " , DR_LIMIT                      = @limit " & _
                      " , DR_LIMIT_PER_VOUCHER          = @limit_per_voucher " & _
                      " , DR_INITIAL_NUMBER             = @initial_number " & _
                      " , DR_MAX_NUMBER                 = @max_number " & _
                      " , DR_CREDIT_TYPE                = @credit_type " & _
                      " , DR_HEADER                     = @header " & _
                      " , DR_FOOTER                     = @footer " & _
                      " , DR_DETAIL1                    = @detail1 " & _
                      " , DR_DETAIL2                    = @detail2 " & _
                      " , DR_DETAIL3                    = @detail3 " & _
                      " , DR_MIN_PLAYED_PCT             = @min_played_pct " & _
                      " , DR_MIN_SPENT_PCT              = @min_spent_pct " & _
                      " , DR_MIN_CASH_IN                = @min_cash_in " & _
                      " , DR_PROVIDER_LIST              = @provider_list " & _
                      " , DR_OFFER_LIST                 = @offer_list " & _
                      " , DR_LEVEL_FILTER               = @level_filter " & _
                      " , DR_GENDER_FILTER              = @gender_filter " & _
                      " , DR_FIRST_CASH_IN_CONSTRAINED  = @first_cash_in_constrained " & _
                      " , DR_LIMIT_PER_OPERATION        = @limit_per_operation " & _
                      " , DR_BINGO_FORMAT               = @bingo_format " & _
                      " , DR_SMALL_RESOURCE_ID          = @SmallResourceId " & _
                      " , DR_LARGE_RESOURCE_ID          = @LargeResourceId " & _
                      " , DR_POINTS_LEVEL1              = @PointsLevel1 " & _
                      " , DR_POINTS_LEVEL2              = @PointsLevel2 " & _
                      " , DR_POINTS_LEVEL3              = @PointsLevel3 " & _
                      " , DR_POINTS_LEVEL4              = @PointsLevel4 " & _
                      " , DR_VISIBLE_ON_PROMOBOX        = @VisibleOnPromoBOX " & _
                      " , DR_AWARD_ON_PROMOBOX          = @AwardOnPromoBOX " & _
                      " , DR_TEXT_ON_PROMOBOX           = @TextOnPromoBOX " & _
                      " , DR_VIP                        = @Vip " & _
                " WHERE   DR_ID                = @draw_id " & _
                  " AND   @max_number >= DR_LAST_NUMBER " & _
                  " AND   @initial_number = CASE DR_LAST_NUMBER WHEN -1 THEN @initial_number " & _
                  "       ELSE DR_INITIAL_NUMBER END "

      sql_command = New SqlCommand(str_sql)
      sql_command.Transaction = SqlTrans
      sql_command.Connection = SqlTrans.Connection

      sql_command.Parameters.Add("@name", SqlDbType.NVarChar).Value = pDraw.name
      sql_command.Parameters.Add("@starting_date", SqlDbType.DateTime).Value = pDraw.starting_date.Value
      sql_command.Parameters.Add("@ending_date", SqlDbType.DateTime).Value = pDraw.ending_date.Value
      sql_command.Parameters.Add("@number_price", SqlDbType.Float).Value = pDraw.number_price
      sql_command.Parameters.Add("@status", SqlDbType.Int).Value = pDraw.status
      sql_command.Parameters.Add("@limited", SqlDbType.Int).Value = pDraw.limited
      sql_command.Parameters.Add("@limit", SqlDbType.Int).Value = pDraw.limit
      sql_command.Parameters.Add("@limit_per_voucher", SqlDbType.Int).Value = pDraw.limit_per_voucher
      sql_command.Parameters.Add("@initial_number", SqlDbType.BigInt).Value = pDraw.initial_number
      sql_command.Parameters.Add("@max_number", SqlDbType.BigInt).Value = pDraw.max_number
      sql_command.Parameters.Add("@credit_type", SqlDbType.Int).Value = pDraw.credit_type
      sql_command.Parameters.Add("@header", SqlDbType.NVarChar).Value = IIf(pDraw.header = "", DBNull.Value, pDraw.header)
      sql_command.Parameters.Add("@footer", SqlDbType.NVarChar).Value = IIf(pDraw.footer = "", DBNull.Value, pDraw.footer)
      sql_command.Parameters.Add("@detail1", SqlDbType.NVarChar).Value = IIf(pDraw.detail1 = "", DBNull.Value, pDraw.detail1)
      sql_command.Parameters.Add("@detail2", SqlDbType.NVarChar).Value = IIf(pDraw.detail2 = "", DBNull.Value, pDraw.detail2)
      sql_command.Parameters.Add("@detail3", SqlDbType.NVarChar).Value = IIf(pDraw.detail3 = "", DBNull.Value, pDraw.detail3)
      sql_command.Parameters.Add("@min_played_pct", SqlDbType.Float).Value = pDraw.min_played_pct
      sql_command.Parameters.Add("@min_spent_pct", SqlDbType.Float).Value = pDraw.min_spent_pct
      sql_command.Parameters.Add("@min_cash_in", SqlDbType.Float).Value = pDraw.min_cash_in
      sql_command.Parameters.Add("@level_filter", SqlDbType.Int).Value = pDraw.level_filter
      sql_command.Parameters.Add("@gender_filter", SqlDbType.Int).Value = pDraw.gender_filter
      sql_command.Parameters.Add("@first_cash_in_constrained", SqlDbType.Int).Value = pDraw.first_cash_in_constrained
      sql_command.Parameters.Add("@limit_per_operation", SqlDbType.Int).Value = pDraw.limit_per_operation
      sql_command.Parameters.Add("@bingo_format", SqlDbType.Int).Value = pDraw.bingo_format

      If pDraw.small_resource_id.HasValue Then
        sql_command.Parameters.Add("@SmallResourceId", SqlDbType.BigInt).Value = pDraw.small_resource_id.Value
      Else
        sql_command.Parameters.Add("@SmallResourceId", SqlDbType.BigInt).Value = DBNull.Value
      End If

      If pDraw.large_resource_id.HasValue Then
        sql_command.Parameters.Add("@LargeResourceId", SqlDbType.BigInt).Value = pDraw.large_resource_id.Value
      Else
        sql_command.Parameters.Add("@LargeResourceId", SqlDbType.BigInt).Value = DBNull.Value
      End If

      _xml = pDraw.provider_list.ToXml
      If Not _xml Is Nothing Then
        sql_command.Parameters.Add("@provider_list", SqlDbType.Xml).Value = _xml
      Else
        sql_command.Parameters.Add("@provider_list", SqlDbType.Xml).Value = DBNull.Value
      End If

      _xml = pDraw.offer_list.ToXml
      If Not _xml Is Nothing Then
        sql_command.Parameters.Add("@offer_list", SqlDbType.Xml).Value = _xml
      Else
        sql_command.Parameters.Add("@offer_list", SqlDbType.Xml).Value = DBNull.Value
      End If

      sql_command.Parameters.Add("@draw_id", SqlDbType.Int).Value = pDraw.draw_id

      sql_command.Parameters.Add("@PointsLevel1", SqlDbType.Float).Value = IIf(pDraw.points_level1 = -1, DBNull.Value, pDraw.points_level1)
      sql_command.Parameters.Add("@PointsLevel2", SqlDbType.Float).Value = IIf(pDraw.points_level2 = -1, DBNull.Value, pDraw.points_level2)
      sql_command.Parameters.Add("@PointsLevel3", SqlDbType.Float).Value = IIf(pDraw.points_level3 = -1, DBNull.Value, pDraw.points_level3)
      sql_command.Parameters.Add("@PointsLevel4", SqlDbType.Float).Value = IIf(pDraw.points_level4 = -1, DBNull.Value, pDraw.points_level4)

      sql_command.Parameters.Add("@VisibleOnPromoBOX", SqlDbType.Bit).Value = pDraw.visible_on_promobox
      sql_command.Parameters.Add("@AwardOnPromoBOX", SqlDbType.Bit).Value = pDraw.award_on_promobox
      sql_command.Parameters.Add("@TextOnPromoBOX", SqlDbType.NVarChar, 50).Value = pDraw.text_on_promobox

      sql_command.Parameters.Add("@Vip", SqlDbType.Bit).Value = pDraw.is_vip

      num_rows_updated = sql_command.ExecuteNonQuery()

      If num_rows_updated <> 1 Then
        GUI_EndSQLTransaction(SqlTrans, False)

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      If Not GUI_EndSQLTransaction(SqlTrans, True) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

    Catch ex As Exception
      GUI_EndSQLTransaction(SqlTrans, False)
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
  End Function ' UpdateDraw

  '----------------------------------------------------------------------------
  ' PURPOSE: Inserts the given Draw to the database
  '
  ' PARAMS:
  '   - INPUT:
  '     - pDraw As TYPE_DRAW
  '
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Private Function InsertDraw(ByVal pDraw As TYPE_DRAW, ByVal Context As Integer) As Integer
    Dim sql_command As SqlCommand
    Dim str_sql As String
    Dim value As Object
    Dim db_count As Integer = 0
    Dim num_rows_updated As Integer
    Dim _xml As String = Nothing

    'XVV 16/04/2007
    'Assign Nothing to variable because compiler generate a warning
    Dim SqlTrans As SqlTransaction = Nothing

    Try
      If Not GUI_BeginSQLTransaction(SqlTrans) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      str_sql = "SELECT  COUNT(*) " & _
                  " FROM  DRAWS " & _
                " WHERE  DR_NAME = @name"

      sql_command = New SqlCommand(str_sql)
      sql_command.Transaction = SqlTrans
      sql_command.Connection = SqlTrans.Connection

      sql_command.Parameters.Add("@name", SqlDbType.NVarChar).Value = pDraw.name

      value = sql_command.ExecuteScalar()
      If value IsNot Nothing And value IsNot DBNull.Value Then
        db_count = CInt(value)
      Else
        GUI_EndSQLTransaction(SqlTrans, False)

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If
      sql_command = Nothing

      If db_count > 0 Then
        GUI_EndSQLTransaction(SqlTrans, False)

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
      End If

      SaveImage(pDraw.large_resource_id, pDraw.image, SqlTrans)
      SaveImage(pDraw.small_resource_id, pDraw.icon, SqlTrans)

      ' Table DRAWS has the Primary Key DR_ID as Identity Auto-Increment.
      str_sql = "INSERT INTO   DRAWS " & _
                           " ( DR_NAME " & _
                           " , DR_STARTING_DATE " & _
                           " , DR_ENDING_DATE " & _
                           " , DR_LAST_NUMBER " & _
                           " , DR_NUMBER_PRICE " & _
                           " , DR_STATUS " & _
                           " , DR_LIMITED " & _
                           " , DR_LIMIT " & _
                           " , DR_LIMIT_PER_VOUCHER " & _
                           " , DR_INITIAL_NUMBER " & _
                           " , DR_MAX_NUMBER " & _
                           " , DR_CREDIT_TYPE " & _
                           " , DR_HEADER " & _
                           " , DR_FOOTER " & _
                           " , DR_DETAIL1 " & _
                           " , DR_DETAIL2 " & _
                           " , DR_DETAIL3 " & _
                           " , DR_MIN_PLAYED_PCT " & _
                           " , DR_MIN_SPENT_PCT " & _
                           " , DR_MIN_CASH_IN " & _
                           " , DR_PROVIDER_LIST " & _
                           " , DR_OFFER_LIST " & _
                           " , DR_LEVEL_FILTER " & _
                           " , DR_GENDER_FILTER " & _
                           " , DR_FIRST_CASH_IN_CONSTRAINED " & _
                           " , DR_LIMIT_PER_OPERATION " & _
                           " , DR_BINGO_FORMAT " & _
                           " , DR_SMALL_RESOURCE_ID " & _
                           " , DR_LARGE_RESOURCE_ID " & _
                           " , DR_POINTS_LEVEL1 " & _
                           " , DR_POINTS_LEVEL2 " & _
                           " , DR_POINTS_LEVEL3 " & _
                           " , DR_POINTS_LEVEL4 " & _
                           " , DR_VISIBLE_ON_PROMOBOX " & _
                           " , DR_AWARD_ON_PROMOBOX " & _
                           " , DR_TEXT_ON_PROMOBOX " & _
                           " , DR_VIP " & _
                           " ) " & _
                    " VALUES " & _
                           " ( @name" & _
                           " , @starting_date" & _
                           " , @ending_date" & _
                           " , @last_number" & _
                           " , @number_price" & _
                           " , @status" & _
                           " , @limited" & _
                           " , @limit" & _
                           " , @limit_per_voucher" & _
                           " , @initial_number " & _
                           " , @max_number " & _
                           " , @credit_type " & _
                           " , @header " & _
                           " , @footer " & _
                           " , @detail1 " & _
                           " , @detail2 " & _
                           " , @detail3 " & _
                           " , @min_played_pct " & _
                           " , @min_spent_pct " & _
                           " , @min_cash_in " & _
                           " , @provider_list " & _
                           " , @offer_list " & _
                           " , @level_filter " & _
                           " , @gender_filter " & _
                           " , @first_cash_in_constrained " & _
                           " , @limit_per_operation " & _
                           " , @bingo_format " & _
                           " , @pSmallResourceId " & _
                           " , @pLargeResourceId " & _
                           " , @pPointsLevel1" & _
                           " , @pPointsLevel2" & _
                           " , @pPointsLevel3" & _
                           " , @pPointsLevel4" & _
                           " , @pVisibleOnPromoBOX" & _
                           " , @pAwardOnPromoBOX" & _
                           " , @pTextOnPromoBOX" & _
                           " , @pVip" & _
                           " )"

      sql_command = New SqlCommand(str_sql)
      sql_command.Transaction = SqlTrans
      sql_command.Connection = SqlTrans.Connection

      sql_command.Parameters.Add("@name", SqlDbType.NVarChar).Value = pDraw.name
      sql_command.Parameters.Add("@starting_date", SqlDbType.DateTime).Value = pDraw.starting_date.Value
      sql_command.Parameters.Add("@ending_date", SqlDbType.DateTime).Value = pDraw.ending_date.Value
      sql_command.Parameters.Add("@last_number", SqlDbType.Int).Value = pDraw.last_number
      sql_command.Parameters.Add("@number_price", SqlDbType.Float).Value = pDraw.number_price
      sql_command.Parameters.Add("@status", SqlDbType.Int).Value = pDraw.status
      sql_command.Parameters.Add("@limited", SqlDbType.Int).Value = pDraw.limited
      sql_command.Parameters.Add("@limit", SqlDbType.Int).Value = pDraw.limit
      sql_command.Parameters.Add("@limit_per_voucher", SqlDbType.Int).Value = pDraw.limit_per_voucher
      sql_command.Parameters.Add("@initial_number", SqlDbType.BigInt).Value = pDraw.initial_number
      sql_command.Parameters.Add("@max_number", SqlDbType.BigInt).Value = pDraw.max_number
      sql_command.Parameters.Add("@credit_type", SqlDbType.Int).Value = pDraw.credit_type
      sql_command.Parameters.Add("@header", SqlDbType.NVarChar).Value = IIf(pDraw.header = "", DBNull.Value, pDraw.header)
      sql_command.Parameters.Add("@footer", SqlDbType.NVarChar).Value = IIf(pDraw.footer = "", DBNull.Value, pDraw.footer)
      sql_command.Parameters.Add("@detail1", SqlDbType.NVarChar).Value = IIf(pDraw.detail1 = "", DBNull.Value, pDraw.detail1)
      sql_command.Parameters.Add("@detail2", SqlDbType.NVarChar).Value = IIf(pDraw.detail2 = "", DBNull.Value, pDraw.detail2)
      sql_command.Parameters.Add("@detail3", SqlDbType.NVarChar).Value = IIf(pDraw.detail3 = "", DBNull.Value, pDraw.detail3)
      sql_command.Parameters.Add("@min_played_pct", SqlDbType.Float).Value = pDraw.min_played_pct
      sql_command.Parameters.Add("@min_spent_pct", SqlDbType.Float).Value = pDraw.min_spent_pct
      sql_command.Parameters.Add("@min_cash_in", SqlDbType.Float).Value = pDraw.min_cash_in
      sql_command.Parameters.Add("@level_filter", SqlDbType.Int).Value = pDraw.level_filter
      sql_command.Parameters.Add("@gender_filter", SqlDbType.Int).Value = pDraw.gender_filter
      sql_command.Parameters.Add("@first_cash_in_constrained", SqlDbType.Int).Value = pDraw.first_cash_in_constrained
      sql_command.Parameters.Add("@limit_per_operation", SqlDbType.Int).Value = pDraw.gender_filter
      sql_command.Parameters.Add("@bingo_format", SqlDbType.Int).Value = pDraw.bingo_format

      If pDraw.small_resource_id.HasValue Then
        sql_command.Parameters.Add("@pSmallResourceId", SqlDbType.BigInt).Value = pDraw.small_resource_id.Value
      Else
        sql_command.Parameters.Add("@pSmallResourceId", SqlDbType.BigInt).Value = DBNull.Value
      End If

      If pDraw.large_resource_id.HasValue Then
        sql_command.Parameters.Add("@pLargeResourceId", SqlDbType.BigInt).Value = pDraw.large_resource_id.Value
      Else
        sql_command.Parameters.Add("@pLargeResourceId", SqlDbType.BigInt).Value = DBNull.Value
      End If

      _xml = pDraw.provider_list.ToXml
      If Not _xml Is Nothing Then
        sql_command.Parameters.Add("@provider_list", SqlDbType.Xml).Value = _xml
      Else
        sql_command.Parameters.Add("@provider_list", SqlDbType.Xml).Value = DBNull.Value
      End If

      _xml = pDraw.offer_list.ToXml
      If Not _xml Is Nothing Then
        sql_command.Parameters.Add("@offer_list", SqlDbType.Xml).Value = _xml
      Else
        sql_command.Parameters.Add("@offer_list", SqlDbType.Xml).Value = DBNull.Value
      End If

      sql_command.Parameters.Add("@pPointsLevel1", SqlDbType.Float).Value = IIf(pDraw.points_level1 = -1, DBNull.Value, pDraw.points_level1)
      sql_command.Parameters.Add("@pPointsLevel2", SqlDbType.Float).Value = IIf(pDraw.points_level2 = -1, DBNull.Value, pDraw.points_level2)
      sql_command.Parameters.Add("@pPointsLevel3", SqlDbType.Float).Value = IIf(pDraw.points_level3 = -1, DBNull.Value, pDraw.points_level3)
      sql_command.Parameters.Add("@pPointsLevel4", SqlDbType.Float).Value = IIf(pDraw.points_level4 = -1, DBNull.Value, pDraw.points_level4)

      sql_command.Parameters.Add("@pVisibleOnPromoBOX", SqlDbType.Bit).Value = pDraw.visible_on_promobox
      sql_command.Parameters.Add("@pAwardOnPromoBOX", SqlDbType.Bit).Value = pDraw.award_on_promobox
      sql_command.Parameters.Add("@pTextOnPromoBOX", SqlDbType.NVarChar, 50).Value = pDraw.text_on_promobox

      sql_command.Parameters.Add("@pVip", SqlDbType.Bit).Value = pDraw.is_vip

      num_rows_updated = sql_command.ExecuteNonQuery()

      If num_rows_updated <> 1 Then
        GUI_EndSQLTransaction(SqlTrans, False)

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      If Not GUI_EndSQLTransaction(SqlTrans, True) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

    Catch ex As Exception
      GUI_EndSQLTransaction(SqlTrans, False)
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
  End Function ' InsertDraw

  '----------------------------------------------------------------------------
  ' PURPOSE: Deletes the given Draw from the database
  '
  ' PARAMS:
  '   - INPUT:
  '     - pDraw As TYPE_DRAW
  '
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Private Function DeleteDraw(ByVal DrawId As Integer, _
                              ByVal SmallResourceId As Nullable(Of Long), _
                              ByVal LargeResourceId As Nullable(Of Long), _
                              ByVal Context As Integer) As Integer

    Dim sql_command As SqlCommand
    Dim str_sql As String
    Dim db_count As Integer = 0
    Dim num_rows_updated As Integer
    Dim data_table As DataTable

    'XVV 16/04/2007
    'Assign Nothing to variable because compiler generate a warning
    Dim SqlTrans As SqlTransaction = Nothing

    'Dim data_table As DataTable

    Try
      If Not GUI_BeginSQLTransaction(SqlTrans) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      ' QMP 23-APR-2013: Check if tickets for the raffle have already been awarded
      str_sql = " SELECT   COUNT(*)     " & _
                "   FROM   DRAW_TICKETS " & _
                "  WHERE   DT_DRAW_ID = " & DrawId

      data_table = GUI_GetTableUsingSQL(str_sql, 5000)
      If IsNothing(data_table) Then
        GUI_EndSQLTransaction(SqlTrans, False)

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      db_count = data_table.Rows(0).Item(0)

      If db_count > 0 Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DEPENDENCIES
      End If

      str_sql = "DELETE  DRAWS " & _
                " WHERE  DR_ID = @draw_id"

      sql_command = New SqlCommand(str_sql)
      sql_command.Transaction = SqlTrans
      sql_command.Connection = SqlTrans.Connection

      sql_command.Parameters.Add("@draw_id", SqlDbType.Int).Value = DrawId

      num_rows_updated = sql_command.ExecuteNonQuery()

      If num_rows_updated <> 1 Then
        GUI_EndSQLTransaction(SqlTrans, False)

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      If SmallResourceId.HasValue Then
        If Not WSI.Common.WKTResources.Delete(SmallResourceId, SqlTrans) Then
          ' Rollback  
          GUI_EndSQLTransaction(SqlTrans, False)

          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If
      End If

      If LargeResourceId.HasValue Then
        If Not WSI.Common.WKTResources.Delete(LargeResourceId, SqlTrans) Then
          ' Rollback  
          GUI_EndSQLTransaction(SqlTrans, False)

          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If
      End If

      If Not GUI_EndSQLTransaction(SqlTrans, True) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

    Catch ex As Exception
      GUI_EndSQLTransaction(SqlTrans, False)
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
  End Function ' DeleteDraw

#End Region ' Private Functions

#Region "Public Functions"

  Public Function OfferListAuditString(ByVal NLS_day1 As String, ByVal NLS_day2 As String, ByVal NLS_day3 As String, ByVal NLS_day4 As String, ByVal NLS_day5 As String, ByVal NLS_day6 As String, ByVal NLS_day7 As String, ByVal NLS_singleDay As String, ByVal NLS_severalDays As String, ByVal NLS_allDays As String, ByVal NLS_timeFrom As String, ByVal NLS_timeTo As String, ByVal NLS_and As String, ByVal NLS_noOffers As String) As String

    Dim _ret As String = ""
    Dim _curOffer As WSI.Common.DrawOffer

    If Not OfferList Is Nothing Then

      For _index As Integer = 0 To OfferList.Count - 1

        _curOffer = OfferList.GetOffer(_index)
        _ret = _ret & OfferAuditString(_curOffer, NLS_day1, NLS_day2, NLS_day3, NLS_day4, NLS_day5, NLS_day6, NLS_day7, NLS_singleDay, NLS_severalDays, NLS_allDays, NLS_timeFrom, NLS_timeTo, NLS_and) & ";"

      Next _index

      _ret = _ret.TrimEnd(";")

    End If

    If _ret = String.Empty Then
      _ret = NLS_noOffers
    End If

    OfferListAuditString = _ret

  End Function

  Public Function OfferAuditString(ByVal curOffer As WSI.Common.DrawOffer, ByVal NLS_day1 As String, ByVal NLS_day2 As String, ByVal NLS_day3 As String, ByVal NLS_day4 As String, ByVal NLS_day5 As String, ByVal NLS_day6 As String, ByVal NLS_day7 As String, ByVal NLS_singleDay As String, ByVal NLS_severalDays As String, ByVal NLS_allDays As String, ByVal NLS_timeFrom As String, ByVal NLS_timeTo As String, ByVal NLS_and As String) As String

    Dim _ret As String = ""
    Dim _offerType As Integer = 0
    Dim _elapsed As TimeSpan
    Dim _time As String
    Dim _str_binary As String
    Dim _bit_array As Char()
    Dim _weekDayDesc As String = ""
    Dim _tmp As String = ""

    'Tipo A: NxM el   {d�a espec�fico}                       [de <hora inicial> a <hora final>]
    'tipoA = curOffer.Weekday = 0

    'Tipo B: NxM todos los d�as                              [de <hora inicial> a <hora final>]
    'tipoB = curOffer.Weekday = 127

    'Tipo C: NxM los  {d�as de la semana- 1} y {�ltimo d�a}  [de <hora inicial> a <hora final>]
    'tipoC = (Not tipoA And Not tipoB And Not tipoD)

    ' Get current offer type
    If (curOffer.Weekday = 0) Then
      _offerType = 1
    ElseIf (curOffer.Weekday = 127) Then
      _offerType = 2
    Else
      _offerType = 3
    End If

    ' Get current offer weekdaystring
    If (curOffer.Weekday <> 0) Then

      _str_binary = Convert.ToString(curOffer.Weekday, 2)
      _str_binary = New String("0", 7 - _str_binary.Length) + _str_binary

      _bit_array = _str_binary.ToCharArray()

      If (_bit_array(5) = "1") Then _weekDayDesc = _weekDayDesc & NLS_day1
      If (_bit_array(4) = "1") Then _weekDayDesc = _weekDayDesc & IIf(_weekDayDesc.Length > 0, ", " & NLS_day2, NLS_day2)
      If (_bit_array(3) = "1") Then _weekDayDesc = _weekDayDesc & IIf(_weekDayDesc.Length > 0, ", " & NLS_day3, NLS_day3)
      If (_bit_array(2) = "1") Then _weekDayDesc = _weekDayDesc & IIf(_weekDayDesc.Length > 0, ", " & NLS_day4, NLS_day4)
      If (_bit_array(1) = "1") Then _weekDayDesc = _weekDayDesc & IIf(_weekDayDesc.Length > 0, ", " & NLS_day5, NLS_day5)
      If (_bit_array(0) = "1") Then _weekDayDesc = _weekDayDesc & IIf(_weekDayDesc.Length > 0, ", " & NLS_day6, NLS_day6)
      If (_bit_array(6) = "1") Then _weekDayDesc = _weekDayDesc & IIf(_weekDayDesc.Length > 0, ", " & NLS_day7, NLS_day7)

    End If

    If curOffer.N > 0 Then
      _ret = _ret & " " & curOffer.N.ToString() & "x" & curOffer.M.ToString() & " "
    Else
      _ret = _ret & curOffer.N.ToString() & " "
    End If

    Select Case curOffer.GenderFilter
      Case DrawGenderFilter.MEN_ONLY
        _ret = _ret & GLB_NLS_GUI_PLAYER_TRACKING.GetString(521) & " "

      Case DrawGenderFilter.WOMEN_ONLY
        _ret = _ret & GLB_NLS_GUI_PLAYER_TRACKING.GetString(522) & " "

      Case DrawGenderFilter.ALL
    End Select

    ' Get current offer time
    If ((curOffer.TimeFrom <> 0) Or (curOffer.TimeTo <> 0)) Then

      _elapsed = TimeSpan.FromSeconds(curOffer.TimeFrom)
      _time = " " & NLS_timeFrom & " " & WSI.Common.Format.CustomFormatTime(DateTime.Today.Add(_elapsed), False)

      _elapsed = TimeSpan.FromSeconds(curOffer.TimeTo)
      _time = _time & " " & NLS_timeTo & " " & WSI.Common.Format.CustomFormatTime(DateTime.Today.Add(_elapsed), False)
    Else

      _time = ""

    End If

    Select Case _offerType
      Case 1
        _ret = _ret & NLS_singleDay & " " & WSI.Common.Format.CustomFormatDateTime(curOffer.Day, False) & _time
      Case 2
        _ret = _ret & NLS_allDays & _time
      Case 3
        Dim _lastCommaPos As Integer
        _lastCommaPos = _weekDayDesc.LastIndexOf(",")
        If _lastCommaPos <> -1 Then
          _tmp = _weekDayDesc.Substring(0, _lastCommaPos) & " " & NLS_and & _weekDayDesc.Trim.Substring(_lastCommaPos + 1, _weekDayDesc.Length - _lastCommaPos - 1)
          _weekDayDesc = _tmp
        End If
        _ret = _ret & NLS_severalDays & " " & _weekDayDesc & _time
      Case Else
        '
    End Select

    OfferAuditString = _ret.Trim

  End Function

#End Region 'Public Functions

End Class ' CLASS_DRAW
