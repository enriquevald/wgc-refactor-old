'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : cls_provider.vb
'
' DESCRIPTION : Provider Data class
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 15-JUN-2012  JML    Initial version
' 31-MAY-2013  XCD    Don't show hide providers
'--------------------------------------------------------------------
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient

Public Class CLASS_PROVIDER_DATA
  Inherits CLASS_BASE

#Region " Constants "

  Public Const SQL_COLUMN_PROVIDER_ID As Integer = 0
  Public Const SQL_COLUMN_PROVIDER_NAME As Integer = 1
  Public Const SQL_COLUMN_PV_ONLY_REDEEMABLE As Integer = 2

#End Region  ' Constants

#Region " Structures "

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_DATA_PER_PROVIDER
    Public data_per_provider As DataTable

    ' Init structure
    Public Sub New()
      data_per_provider = New DataTable()
    End Sub
  End Class

#End Region ' Structures

#Region " Members "

  Protected m_data_per_provider As New TYPE_DATA_PER_PROVIDER

#End Region    ' Members

#Region "Properties"

  Public Overloads Property DataPerProvider() As DataTable
    Get
      Return m_data_per_provider.data_per_provider
    End Get

    Set(ByVal Value As DataTable)
      m_data_per_provider.data_per_provider = Value
    End Set

  End Property ' DataPerProvider

#End Region   ' Properties

#Region "Overrides"

  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim auditor_data As CLASS_AUDITOR_DATA

    auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_PLAYER_PROMOTIONS)

    'auditor_data.SetName(GLB_NLS_GUI_CONFIGURATION.Id(438), "")
    auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(1054), "")

    ' RCI 20-JAN-2012
    For Each _points_mult As DataRow In Me.DataPerProvider.Rows
      Call auditor_data.SetField(0, GetRepresentativeValue(_points_mult(SQL_COLUMN_PV_ONLY_REDEEMABLE)), _
                                    _points_mult(SQL_COLUMN_PROVIDER_NAME) & ". " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1053))
    Next

    Return auditor_data

  End Function ' AuditorData

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
  End Function   ' DB_Delete

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
  End Function   ' DB_Insert

  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim rc As Integer

    ' Read provider data
    rc = ProviderData_DbRead(SqlCtx)

    Select Case rc
      Case ENUM_DB_RC.DB_RC_OK
        Return ENUM_STATUS.STATUS_OK

      Case ENUM_DB_RC.DB_RC_ERROR_NOT_FOUND
        Return CLASS_BASE.ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function     ' DB_Read

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim rc As Integer

    ' Update provider data
    rc = ProviderData_DbUpdate(SqlCtx)

    Select Case rc
      Case ENUM_DB_RC.DB_RC_OK
        Return ENUM_STATUS.STATUS_OK

      Case ENUM_DB_RC.DB_RC_ERROR_NOT_FOUND
        Return CLASS_BASE.ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function   ' DB_Update

  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE
    Dim temp_provider_data As CLASS_PROVIDER_DATA

    temp_provider_data = New CLASS_PROVIDER_DATA

    temp_provider_data.m_data_per_provider.data_per_provider = Me.m_data_per_provider.data_per_provider.Copy()

    Return temp_provider_data
  End Function   ' Duplicate

#End Region ' Overrides

#Region "Private Functions"

  Private Function ProviderData_DbRead(ByVal Context As Integer) As Integer

    Dim str_sql As String
    Dim data_table_providers As DataTable

    str_sql = "SELECT   PV_ID " & _
              "       , PV_NAME " & _
              "       , PV_ONLY_REDEEMABLE  " & _
              "  FROM   PROVIDERS " & _
              " WHERE   PV_HIDE = 0 " & _
              "ORDER BY PV_NAME "

    data_table_providers = GUI_GetTableUsingSQL(str_sql, 5000)

    m_data_per_provider.data_per_provider = data_table_providers

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

  End Function    ' ProviderData_DbRead

  Private Function ProviderData_DbUpdate(ByVal Context As Integer) As Integer

    Dim SqlTrans As System.Data.SqlClient.SqlTransaction = Nothing
    Dim commit_trx As Boolean
    Dim result As Integer
    Dim _sql_txt As System.Text.StringBuilder
    Dim _num_modified As Integer
    Dim _num_updated As Integer

    Try

      If Not GUI_BeginSQLTransaction(SqlTrans) Then
        Exit Function
      End If

      commit_trx = False

      'MPO 20-JAN-2012
      _num_modified = m_data_per_provider.data_per_provider.Select("", "", _
                                                      DataViewRowState.ModifiedCurrent).Length
      If _num_modified > 0 Then
        _sql_txt = New System.Text.StringBuilder()
        _sql_txt.AppendLine("UPDATE   PROVIDERS")
        _sql_txt.AppendLine("   SET   PV_ONLY_REDEEMABLE   = @pOnlyRedeemable")
        _sql_txt.AppendLine(" WHERE   PV_ID                = @pProviderId")

        Using _da As New SqlDataAdapter()
          Using _sql_cmd As New SqlCommand(_sql_txt.ToString(), SqlTrans.Connection, SqlTrans)
            _sql_cmd.Parameters.Add("@pOnlyRedeemable", SqlDbType.Bit).SourceColumn = "PV_ONLY_REDEEMABLE"
            _sql_cmd.Parameters.Add("@pProviderId", SqlDbType.Int).SourceColumn = "PV_ID"

            _da.UpdateCommand = _sql_cmd
            _num_updated = _da.Update(m_data_per_provider.data_per_provider)
          End Using
        End Using

        If _num_modified <> _num_updated Then
          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If
      End If

      commit_trx = True

    Catch ex As Exception
      ' Do nothing
      Debug.WriteLine(ex.Message)

    Finally
      ' Either commit or rollback
      If Not (SqlTrans.Connection Is Nothing) Then
        If commit_trx Then
          SqlTrans.Commit()
          result = ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Else
          SqlTrans.Rollback()
          result = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If
      End If

      SqlTrans.Dispose()
      SqlTrans = Nothing

    End Try

    Return result

  End Function  ' ProviderData_DbUpdate

  Private Function GetRepresentativeValue(ByVal Value As String) As String

    If Value Then
      Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(698) ' "Si"
    Else
      Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(699) ' "No"
    End If

  End Function ' GetRepresentativeValue

#End Region

End Class
