﻿'-------------------------------------------------------------------
' Copyright © 2016 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : Class to manage Customer Entrance Price
'
' DESCRIPTION : Customer Entrance Price
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 28-JAN-2016  FJC    Initial version (Product Backlog Item 8554:Visitas / Recepción: Crear ticket de entrada: GUI precios entrada)
' 09-MAY-2016  FJC    Product Backlog Item 12927:Visitas / Recepción: Caducidad de las entradas variable
' 09-MAY-2016  FJC    Fixed Bug 12965:Visitas / Recepción: No se graba correctamente la auditoría al eliminar un precio de entrada.
'--------------------------------------------------------------------

Imports WSI.Common
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports System.Text

Public Class cls_customer_entrance_price
  Inherits CLASS_BASE

#Region "Constants"
  Public Const SQL_COLUMN_ENTRANCE_PRICE_ID As Integer = 0
  Public Const SQL_COLUMN_DESCRIPTION As Integer = 1
  Public Const SQL_COLUMN_PRICE As Integer = 2
  Public Const SQL_COLUMN_CUSTOMER_LEVEL As Integer = 3
  Public Const SQL_COLUMN_DEFAULT As Integer = 4
  Public Const SQL_COLUMN_VALID_GAMING_DAYS As Integer = 5
  Public Const SQL_COLUMN_ROWSTATE As Integer = 6

  Public Const SQL_COLUMN_ENTRANCE_PRICE_ID_DESCRIPTION = "CUEP_PRICE_ID"
  Public Const SQL_COLUMN_DESCRIPTION_DESCRIPTION = "CUEP_DESCRIPTION"
  Public Const SQL_COLUMN_PRICE_DESCRIPTION = "CUEP_PRICE"
  Public Const SQL_COLUMN_CUSTOMER_LEVEL_DESCRIPTION = "CUEP_CUSTOMER_LEVEL"
  Public Const SQL_COLUMN_DEFAULT_DESCRIPTION = "CUEP_DEFAULT"
  Public Const SQL_COLUMN_VALID_GAMING_DAYS_DESCRIPTION = "CUEP_VALID_GAMING_DAYS"
  Public Const SQL_COLUMN_ROW_DELETE_STATE = "ROW_DELETE_STATE"
#End Region

#Region "Members"
  Private m_datatable_customer_entrance_prices As New DataTable
  Private m_datatable_customer_entrance_prices_copy = New DataTable
#End Region

#Region "Properties"

  Public Property CustomerEntrancePricesAll() As DataTable
    Get
      Return m_datatable_customer_entrance_prices
    End Get
    Set(ByVal value As DataTable)
      m_datatable_customer_entrance_prices = value
    End Set
  End Property ' CustomerEntrancePricesAll

#End Region

#Region "Overrides"

  ''' <summary>
  ''' 
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function AuditorData() As CLASS_AUDITOR_DATA
    Dim auditor_data As CLASS_AUDITOR_DATA
    Dim _idx As Integer
    Dim _string_data As String
    Dim _string_delete As String
    Dim _dt_table_copy As DataTable

    _string_data = String.Empty
    _string_delete = String.Empty
    _dt_table_copy = New DataTable

    Using DbTrx As New DB_TRX()
      _dt_table_copy = Me.CustomerEntrancePricesAll.Copy

      auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_CASHIER_CONFIGURATION)
      auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(7072), " ")

      Try
        For _idx = 0 To _dt_table_copy.Rows.Count - 1
          _string_delete = String.Empty

          '' Description
          _string_data = _dt_table_copy.Rows(_idx)(cls_customer_entrance_price.SQL_COLUMN_DESCRIPTION)
          If Not IsDBNull(_dt_table_copy.Rows(_idx)(cls_customer_entrance_price.SQL_COLUMN_ROWSTATE)) AndAlso _
             _dt_table_copy.Rows(_idx)(cls_customer_entrance_price.SQL_COLUMN_ROWSTATE) = System.Data.DataRowState.Deleted Then

            _string_delete = " - " & GLB_NLS_GUI_COMMONMISC.GetString(9) & " - "
          End If
          Call auditor_data.SetField(0, _string_data & _string_delete, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6860))

          '' Level
          Call auditor_data.SetField(0, _dt_table_copy.Rows(_idx)(cls_customer_entrance_price.SQL_COLUMN_CUSTOMER_LEVEL), GLB_NLS_GUI_PLAYER_TRACKING.GetString(7008))

          '' Price
          Call auditor_data.SetField(0, _dt_table_copy.Rows(_idx)(cls_customer_entrance_price.SQL_COLUMN_PRICE), GLB_NLS_GUI_PLAYER_TRACKING.GetString(318))

          '' Check Default
          Call auditor_data.SetField(0, _dt_table_copy.Rows(_idx)(cls_customer_entrance_price.SQL_COLUMN_DEFAULT), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2753))

          '' Expiration Days
          Call auditor_data.SetField(0, _dt_table_copy.Rows(_idx)(cls_customer_entrance_price.SQL_COLUMN_VALID_GAMING_DAYS), GLB_NLS_GUI_PLAYER_TRACKING.GetString(7301))

        Next

      Catch ex As Exception

        Debug.Print(ex.Message)
      End Try
    End Using

    Return auditor_data
  End Function

  ''' <summary>
  ''' 
  ''' </summary>
  ''' <param name="SqlCtx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As CLASS_BASE.ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
  End Function

  ''' <summary>
  ''' 
  ''' </summary>
  ''' <param name="SqlCtx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As CLASS_BASE.ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
  End Function

  ''' <summary>
  ''' 
  ''' </summary>
  ''' <param name="ObjectId"></param>
  ''' <param name="SqlCtx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function DB_Read(ObjectId As Object, ByRef SqlCtx As Integer) As CLASS_BASE.ENUM_STATUS

    ' Read currency data
    If (WSI.Common.Entrances.CustomerEntrancePrices.GetCEPricesByLevel(-1, CustomerEntrancePricesAll)) Then
      CustomerEntrancePricesAll.Columns.Add(SQL_COLUMN_ROW_DELETE_STATE, Type.GetType("System.Int16"))

      Return ENUM_STATUS.STATUS_OK

    Else
      Return ENUM_STATUS.STATUS_ERROR
    End If

  End Function

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As CLASS_BASE.ENUM_STATUS
    Try
      m_datatable_customer_entrance_prices_copy = CustomerEntrancePricesAll.Copy

      If (ManagementDtPrices()) Then
        Using _db_trx As New DB_TRX()
          If WSI.Common.Entrances.CustomerEntrancePrices.InsertCustomerEntrancePrices(m_datatable_customer_entrance_prices_copy, _db_trx.SqlTransaction) Then
            _db_trx.Commit()

            Return ENUM_STATUS.STATUS_OK
          End If
        End Using
      End If
    Catch ex As Exception
      Log.Exception(ex)
    End Try

    Return ENUM_STATUS.STATUS_ERROR
  End Function

  ''' <summary>
  ''' Returns a duplicate of the given object.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE
    Dim temp_customer_entrance_prices_data As cls_customer_entrance_price

    temp_customer_entrance_prices_data = New cls_customer_entrance_price

    temp_customer_entrance_prices_data.m_datatable_customer_entrance_prices = Me.m_datatable_customer_entrance_prices.Copy()

    Return temp_customer_entrance_prices_data
  End Function     ' Duplicate
#End Region

#Region " Private"
  Private Function ManagementDtPrices()
    Dim dr As DataRow

    Try
      For _idx As Integer = m_datatable_customer_entrance_prices_copy.Rows.Count - 1 To 0 Step -1
        dr = m_datatable_customer_entrance_prices_copy.Rows(_idx)
        If Not IsDBNull(dr(SQL_COLUMN_ROWSTATE)) AndAlso dr(SQL_COLUMN_ROWSTATE) = System.Data.DataRowState.Deleted Then
          dr.Delete()
        End If
      Next

      Return True
    Catch ex As Exception
      Log.Exception(ex)

    End Try

    Return False
  End Function

#End Region
End Class
