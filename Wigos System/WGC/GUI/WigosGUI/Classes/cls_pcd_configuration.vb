'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_pcd_configuration.vb
' DESCRIPTION:   pcd class for pcd edition
' AUTHOR:        Xavier Cots
' CREATION DATE: 30-MAR-2015
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 30-MAR-2015  XCD    Initial version.
' 29-MAY-2015  FAV    'GetPCDConfigurationsByCommunicationType' method added.
'-------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Text
Imports WSI.Common
Imports System.Data.SqlClient


Public Class CLASS_PCD_CONFIGURATION
  Inherits CLASS_BASE

#Region " Constants "

  ' Fields length
  Public Const MAX_NAME_LEN As Integer = 20 ' In DB nvarchar(50)
  Public Const MAX_DESCRIPTION_LEN As Integer = 190 ' In DB nvarchar(200)
  Public Const AUDIT_NONE_STRING As String = "---"
  Public Const PCD_NUM_INPUT_PORTS As Integer = SmibProtocols.PCDProtocolConfiguration.PCD_NUM_INPUT_PORTS
  Public Const PCD_INPUT_TYPE_DOOR_MASK As Integer = SmibProtocols.PCDProtocolConfiguration.PCD_DOOR_MASK

  Public Const PCD_NUM_OUTPUT_PORTS As Integer = SmibProtocols.PCDProtocolConfiguration.PCD_NUM_OUTPUT_PORTS
  Public Const PCD_TOTAL_PORTS As Integer = (PCD_NUM_INPUT_PORTS + PCD_NUM_OUTPUT_PORTS)
  Public Const PCD_INPUT_NUMBER_MASK As Integer = SmibProtocols.PCDProtocolConfiguration.PCD_PORT_METER_MASK
  Public Const PCD_EGM_NULL_VALUE As Integer = SmibProtocols.PCDProtocolConfiguration.PCD_NULL_VALUE
  Public Const MAX_OUTPUT_MS_LEN As Integer = 5 ' In DB nvarchar(50)

  ' Define pmt_pcd_io_type with bits
  ' 1st byte Input/Output
  Public Const PCD_IO_TYPE_INPUT As Integer = SmibProtocols.PCDProtocolConfiguration.IoType.INPUT
  Public Const PCD_IO_TYPE_OUTPUT As Integer = SmibProtocols.PCDProtocolConfiguration.IoType.OUTPUT
  ' 2nd byte Pulse/Signal types
  Public Const PCD_IO_TYPE_PULSE_UP As Integer = SmibProtocols.PCDProtocolConfiguration.EdgeType.PULSE_UP
  Public Const PCD_IO_TYPE_PULSE_DOWN As Integer = SmibProtocols.PCDProtocolConfiguration.EdgeType.PULSE_DOWN
  Public Const PCD_IO_TYPE_SIGNAL_UP As Integer = SmibProtocols.PCDProtocolConfiguration.EdgeType.SIGNAL_DOWN_UP
  Public Const PCD_IO_TYPE_SIGNAL_DOWN As Integer = SmibProtocols.PCDProtocolConfiguration.EdgeType.SIGNAL_UP_DOWN
  ' 3rd and 4th free

  ' Private Const
  Private Const MAX_SQL_ROWS As Integer = 5000

#End Region

#Region " GUI_Configuration.dll "

#Region " Structures "

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_PCD_PORT
    Public pcd_io_number As Integer
    Public egm_number As Integer
    Public egm_number_multiplier As Decimal
    Public pulse_type As Integer
    Public time_pulse_down_ms As Integer
    Public time_pulse_up_ms As Integer
  End Class

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_PCD_CONFIGURATION
    Public configuration_id As Int32
    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_NAME_LEN + 1)> _
    Public configuration_name As String
    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_DESCRIPTION_LEN + 1)> _
    Public description As String
    Public input_ports(PCD_NUM_INPUT_PORTS) As TYPE_PCD_PORT
    Public output_ports(PCD_NUM_OUTPUT_PORTS) As TYPE_PCD_PORT

    Public Sub New()
      Dim _idx_ports As Integer

      For _idx_ports = 0 To PCD_NUM_INPUT_PORTS - 1
        input_ports(_idx_ports) = New TYPE_PCD_PORT()
        InitInputPort(_idx_ports)
      Next

      For _idx_ports = 0 To PCD_NUM_OUTPUT_PORTS - 1
        output_ports(_idx_ports) = New TYPE_PCD_PORT()
        InitOutputPort(_idx_ports)
      Next
    End Sub

    Public Sub InitInputPort(ByVal Port As Integer)
      input_ports(Port).egm_number_multiplier = 0
      input_ports(Port).egm_number = CLASS_PCD_CONFIGURATION.PCD_EGM_NULL_VALUE
      input_ports(Port).pcd_io_number = 0
      input_ports(Port).pulse_type = CLASS_PCD_CONFIGURATION.PCD_IO_TYPE_INPUT
      input_ports(Port).time_pulse_down_ms = 0
      input_ports(Port).time_pulse_up_ms = 0
    End Sub

    Public Sub InitOutputPort(ByVal Port As Integer)
      output_ports(Port).egm_number_multiplier = 0
      output_ports(Port).egm_number = CLASS_PCD_CONFIGURATION.PCD_EGM_NULL_VALUE
      output_ports(Port).pcd_io_number = 0
      output_ports(Port).pulse_type = CLASS_PCD_CONFIGURATION.PCD_IO_TYPE_OUTPUT
      output_ports(Port).time_pulse_down_ms = 0
      output_ports(Port).time_pulse_up_ms = 0
    End Sub
  End Class
#End Region

#Region " Members "

  Protected m_pcd_configuration As New TYPE_PCD_CONFIGURATION()

#End Region

#Region " Enums "

#End Region

#End Region

#Region " Properties "
  Public Property ConfigurationId() As Integer
    Get
      Return m_pcd_configuration.configuration_id
    End Get
    Set(ByVal value As Integer)
      m_pcd_configuration.configuration_id = value
    End Set
  End Property
  Public Property ConfigurationName() As String
    Get
      Return m_pcd_configuration.configuration_name
    End Get
    Set(ByVal value As String)
      m_pcd_configuration.configuration_name = value
    End Set
  End Property

  Public Property ConfigurationDescription() As String
    Get
      Return m_pcd_configuration.description
    End Get
    Set(ByVal value As String)
      m_pcd_configuration.description = value
    End Set
  End Property


  Public Property ConfigurationInputPort(ByVal port_number As Integer) As TYPE_PCD_PORT
    Get
      Return m_pcd_configuration.input_ports(port_number)
    End Get
    Set(ByVal value As TYPE_PCD_PORT)
      m_pcd_configuration.input_ports(port_number).egm_number_multiplier = value.egm_number_multiplier
      m_pcd_configuration.input_ports(port_number).pcd_io_number = value.pcd_io_number
      m_pcd_configuration.input_ports(port_number).egm_number = value.egm_number
      m_pcd_configuration.input_ports(port_number).pulse_type = value.pulse_type
      m_pcd_configuration.input_ports(port_number).time_pulse_down_ms = value.time_pulse_down_ms
      m_pcd_configuration.input_ports(port_number).time_pulse_up_ms = value.time_pulse_up_ms
    End Set
  End Property

  Public Property ConfigurationOutputPort(ByVal port_number As Integer) As TYPE_PCD_PORT
    Get
      Return m_pcd_configuration.output_ports(port_number)
    End Get
    Set(ByVal value As TYPE_PCD_PORT)
      m_pcd_configuration.output_ports(port_number).egm_number_multiplier = value.egm_number_multiplier
      m_pcd_configuration.output_ports(port_number).pcd_io_number = value.pcd_io_number
      m_pcd_configuration.output_ports(port_number).egm_number = value.egm_number
      m_pcd_configuration.output_ports(port_number).pulse_type = value.pulse_type
      m_pcd_configuration.output_ports(port_number).time_pulse_down_ms = value.time_pulse_down_ms
      m_pcd_configuration.output_ports(port_number).time_pulse_up_ms = value.time_pulse_up_ms
    End Set
  End Property

#End Region

#Region " Overrides functions "

  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As Integer

    Me.m_pcd_configuration.configuration_id = ObjectId

    ' Read flag data
    _rc = ReadPCDConfiguration(m_pcd_configuration, SqlCtx)

    Select Case _rc

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As Integer

    ' Update PCD data
    _rc = UpdatePCDConfiguration(m_pcd_configuration, SqlCtx)

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_ALREADY_EXISTS
        Return CLASS_BASE.ENUM_STATUS.STATUS_ALREADY_EXISTS

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As Integer

    ' Insert PCD data
    _rc = InsertPCDConfiguration(m_pcd_configuration, SqlCtx)

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_ALREADY_EXISTS
        Return CLASS_BASE.ENUM_STATUS.STATUS_ALREADY_EXISTS

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As Integer

    'Delete PCD data
    _rc = DeletePCDConfiguration(Me.m_pcd_configuration.configuration_id, SqlCtx)

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DEPENDENCIES
        Return ENUM_STATUS.STATUS_DEPENDENCIES

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_CONTEXT
        Return ENUM_STATUS.STATUS_NOT_SUPPORTED

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function

  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _str_sas_meter As String
    Dim _str_denom As String
    Dim _str_port As String
    Dim _str_pulse_type As String
    Dim _str_times As String

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_TERMINALS)
    Call _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(6017), IIf(String.IsNullOrEmpty(Me.ConfigurationName), AUDIT_NONE_STRING, Me.ConfigurationName))

    ' Configuration Name
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6018), IIf(String.IsNullOrEmpty(Me.ConfigurationName), AUDIT_NONE_STRING, Me.ConfigurationName)) 'Nombre
    ' Description
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6026), IIf(String.IsNullOrEmpty(Me.ConfigurationDescription), AUDIT_NONE_STRING, Me.ConfigurationDescription)) 'Descripcion

    ' Input Ports
    For _idx_port As Integer = 0 To PCD_NUM_INPUT_PORTS - 1
      _str_sas_meter = IIf(Me.ConfigurationInputPort(_idx_port).egm_number = PCD_EGM_NULL_VALUE, AUDIT_NONE_STRING, Hex(Me.ConfigurationInputPort(_idx_port).egm_number))

      _str_port = IIf(Me.ConfigurationInputPort(_idx_port).pcd_io_number = PCD_EGM_NULL_VALUE, AUDIT_NONE_STRING, Me.ConfigurationInputPort(_idx_port).pcd_io_number.ToString())

      If Me.ConfigurationInputPort(_idx_port).egm_number > PCD_INPUT_TYPE_DOOR_MASK Then
        Select Case Me.ConfigurationInputPort(_idx_port).pulse_type
          Case PCD_IO_TYPE_PULSE_DOWN
            _str_pulse_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6098)
          Case PCD_IO_TYPE_PULSE_UP
            _str_pulse_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6097)
          Case PCD_IO_TYPE_SIGNAL_DOWN
            _str_pulse_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6100)
          Case PCD_IO_TYPE_SIGNAL_UP
            _str_pulse_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6099)
          Case Else
            _str_pulse_type = AUDIT_NONE_STRING
        End Select
      Else
        _str_pulse_type = IIf(Me.ConfigurationInputPort(_idx_port).egm_number = PCD_EGM_NULL_VALUE, AUDIT_NONE_STRING, Me.ConfigurationInputPort(_idx_port).egm_number_multiplier.ToString())
      End If

      Call _auditor_data.SetField(0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6027, _str_port, _str_pulse_type, _str_sas_meter))
    Next

    ' Output ports
    For _idx_port As Integer = 0 To PCD_NUM_OUTPUT_PORTS - 1
      _str_sas_meter = IIf(Me.ConfigurationOutputPort(_idx_port).egm_number = PCD_EGM_NULL_VALUE, AUDIT_NONE_STRING, Hex(Me.ConfigurationOutputPort(_idx_port).egm_number))
      _str_denom = IIf(Me.ConfigurationOutputPort(_idx_port).egm_number = PCD_EGM_NULL_VALUE, AUDIT_NONE_STRING, Me.ConfigurationOutputPort(_idx_port).egm_number_multiplier.ToString())
      _str_port = IIf(Me.ConfigurationOutputPort(_idx_port).pcd_io_number = PCD_EGM_NULL_VALUE, AUDIT_NONE_STRING, Me.ConfigurationOutputPort(_idx_port).pcd_io_number.ToString())

      Select Case Me.ConfigurationOutputPort(_idx_port).pulse_type
        Case PCD_IO_TYPE_PULSE_DOWN
          _str_pulse_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6098)
          _str_times = AUDIT_NONE_STRING + "/" + AUDIT_NONE_STRING
        Case PCD_IO_TYPE_PULSE_UP
          _str_pulse_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6097)
          _str_times = AUDIT_NONE_STRING + "/" + AUDIT_NONE_STRING
        Case PCD_IO_TYPE_SIGNAL_DOWN
          _str_pulse_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6100)
          _str_times = Me.ConfigurationOutputPort(_idx_port).time_pulse_down_ms.ToString() + "/" + Me.ConfigurationOutputPort(_idx_port).time_pulse_up_ms.ToString()
        Case PCD_IO_TYPE_SIGNAL_UP
          _str_pulse_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6099)
          _str_times = Me.ConfigurationOutputPort(_idx_port).time_pulse_down_ms.ToString() + "/" + Me.ConfigurationOutputPort(_idx_port).time_pulse_up_ms.ToString()
        Case Else
          _str_pulse_type = AUDIT_NONE_STRING
          _str_times = AUDIT_NONE_STRING + "/" + AUDIT_NONE_STRING
      End Select

      Call _auditor_data.SetField(0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6096, _str_port, _str_denom, _str_sas_meter, _str_pulse_type, _str_times))
    Next

    Return _auditor_data

  End Function

  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim _temp_pcd_configuration As CLASS_PCD_CONFIGURATION

    _temp_pcd_configuration = New CLASS_PCD_CONFIGURATION

    _temp_pcd_configuration.m_pcd_configuration.configuration_id = Me.m_pcd_configuration.configuration_id
    _temp_pcd_configuration.m_pcd_configuration.configuration_name = Me.m_pcd_configuration.configuration_name
    _temp_pcd_configuration.m_pcd_configuration.description = Me.m_pcd_configuration.description

    For _idx_port As Integer = 0 To PCD_NUM_INPUT_PORTS - 1
      _temp_pcd_configuration.m_pcd_configuration.input_ports(_idx_port).egm_number_multiplier = Me.m_pcd_configuration.input_ports(_idx_port).egm_number_multiplier
      _temp_pcd_configuration.m_pcd_configuration.input_ports(_idx_port).pcd_io_number = Me.m_pcd_configuration.input_ports(_idx_port).pcd_io_number
      _temp_pcd_configuration.m_pcd_configuration.input_ports(_idx_port).egm_number = Me.m_pcd_configuration.input_ports(_idx_port).egm_number
      _temp_pcd_configuration.m_pcd_configuration.input_ports(_idx_port).pulse_type = Me.m_pcd_configuration.input_ports(_idx_port).pulse_type
      _temp_pcd_configuration.m_pcd_configuration.input_ports(_idx_port).time_pulse_down_ms = Me.m_pcd_configuration.input_ports(_idx_port).time_pulse_down_ms
      _temp_pcd_configuration.m_pcd_configuration.input_ports(_idx_port).time_pulse_up_ms = Me.m_pcd_configuration.input_ports(_idx_port).time_pulse_up_ms
    Next

    For _idx_port As Integer = 0 To PCD_NUM_OUTPUT_PORTS - 1
      _temp_pcd_configuration.m_pcd_configuration.output_ports(_idx_port).egm_number_multiplier = Me.m_pcd_configuration.output_ports(_idx_port).egm_number_multiplier
      _temp_pcd_configuration.m_pcd_configuration.output_ports(_idx_port).pcd_io_number = Me.m_pcd_configuration.output_ports(_idx_port).pcd_io_number
      _temp_pcd_configuration.m_pcd_configuration.output_ports(_idx_port).egm_number = Me.m_pcd_configuration.output_ports(_idx_port).egm_number
      _temp_pcd_configuration.m_pcd_configuration.output_ports(_idx_port).pulse_type = Me.m_pcd_configuration.output_ports(_idx_port).pulse_type
      _temp_pcd_configuration.m_pcd_configuration.output_ports(_idx_port).time_pulse_down_ms = Me.m_pcd_configuration.output_ports(_idx_port).time_pulse_down_ms
      _temp_pcd_configuration.m_pcd_configuration.output_ports(_idx_port).time_pulse_up_ms = Me.m_pcd_configuration.output_ports(_idx_port).time_pulse_up_ms
    Next

    Return _temp_pcd_configuration

  End Function

#End Region

#Region " Private Functions "

  Private Function ReadPCDConfiguration(ByVal pPCDConfiguration As TYPE_PCD_CONFIGURATION, _
                            ByVal Context As Integer) As Integer

    Dim _pcd_info As DataSet
    Dim _do_commit As Boolean
    Dim _sql_trx As System.Data.SqlClient.SqlTransaction
    Dim _pcd_input_number As Integer
    Dim _pcd_output_number As Integer
    Dim _pcd_port_type As Integer
    Dim _pcd_config As DataTable
    Dim _pcd_values As DataTable


    _sql_trx = Nothing
    _do_commit = False

    Try

      If Not GUI_BeginSQLTransaction(_sql_trx) Then

        _do_commit = False
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_DB_NOT_CONNECT

      End If

      _pcd_info = WSI.Common.TerminalMetersInfo.ReadPCDConfiguration(_sql_trx, pPCDConfiguration.configuration_id)
      If _pcd_info.Tables.Count < 2 Then
        _do_commit = False
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
      End If

      _pcd_config = _pcd_info.Tables(0)
      _pcd_values = _pcd_info.Tables(1)


      With pPCDConfiguration
        .configuration_id = _pcd_config.Rows(0)("SC_CONFIGURATION_ID")
        .configuration_name = _pcd_config.Rows(0)("SC_NAME")
        .description = _pcd_config.Rows(0)("SC_DESCRIPTION")

        _pcd_input_number = 0
        _pcd_output_number = 0

        For Each _reader As DataRow In _pcd_values.Rows
          _pcd_port_type = _reader("PMT_PCD_IO_TYPE")

          If (_pcd_port_type And PCD_IO_TYPE_INPUT) = PCD_IO_TYPE_INPUT Then
            .input_ports(_pcd_input_number).pcd_io_number = _reader("PMT_PCD_IO_NUMBER") Mod (PCD_INPUT_NUMBER_MASK - 1)
            .input_ports(_pcd_input_number).egm_number_multiplier = IIf(IsDBNull(_reader("PMT_EGM_NUMBER_MULTIPLIER")), 0, _reader("PMT_EGM_NUMBER_MULTIPLIER"))
            .input_ports(_pcd_input_number).egm_number = IIf(IsDBNull(_reader("PMT_EGM_NUMBER")), PCD_EGM_NULL_VALUE, _reader("PMT_EGM_NUMBER"))
            .input_ports(_pcd_input_number).pulse_type = _pcd_port_type And &HF0
            _pcd_input_number += 1
          ElseIf (_pcd_port_type And PCD_IO_TYPE_OUTPUT) = PCD_IO_TYPE_OUTPUT Then
            .output_ports(_pcd_output_number).pcd_io_number = _reader("PMT_PCD_IO_NUMBER")
            .output_ports(_pcd_output_number).egm_number_multiplier = IIf(IsDBNull(_reader("PMT_EGM_NUMBER_MULTIPLIER")), 0, _reader("PMT_EGM_NUMBER_MULTIPLIER"))
            .output_ports(_pcd_output_number).egm_number = _reader("PMT_EGM_NUMBER")
            .output_ports(_pcd_output_number).pulse_type = _pcd_port_type And &HF0
            .output_ports(_pcd_output_number).time_pulse_down_ms = IIf(IsDBNull(_reader("PMT_OUPUT_TIME_PULSE_DOWN")), 0, _reader("PMT_OUPUT_TIME_PULSE_DOWN"))
            .output_ports(_pcd_output_number).time_pulse_up_ms = IIf(IsDBNull(_reader("PMT_OUPUT_TIME_PULSE_UP")), 0, _reader("PMT_OUPUT_TIME_PULSE_UP"))
            _pcd_output_number += 1
          End If
        Next
      End With


      _do_commit = True
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

    Catch ex As Exception
      Log.Error(ex.Message)
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    Finally
      GUI_EndSQLTransaction(_sql_trx, _do_commit)
    End Try

  End Function

  Private Function UpdatePCDConfiguration(ByVal pPCDConfiguration As TYPE_PCD_CONFIGURATION, _
                              ByVal Context As Integer) As Integer

    Dim _sql_trx As System.Data.SqlClient.SqlTransaction = Nothing
    Dim _sb As StringBuilder
    Dim _param_sas_meter_id As SqlParameter
    Dim _param_meter_denomination As SqlParameter
    Dim _param_pcd_meter As SqlParameter
    Dim _param_pcd_io_type As SqlParameter
    Dim _param_pcd_time_pulse_up As SqlParameter
    Dim _param_pcd_time_pulse_down As SqlParameter

    If Not GUI_BeginSQLTransaction(_sql_trx) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    Try
      _sb = New StringBuilder()
      _sb.AppendLine(" SELECT   SC_NAME ")
      _sb.AppendLine("   FROM   SMIB_CONFIGURATION ")
      _sb.AppendLine("  WHERE   SC_CONFIGURATION_ID = @pConfigurationId ")
      _sb.AppendLine("    AND   SC_COMM_TYPE = @pCommType ")

      Using _cmd As New SqlClient.SqlCommand(_sb.ToString(), _sql_trx.Connection, _sql_trx)

        _cmd.Parameters.Add("@pConfigurationId", SqlDbType.BigInt).Value = pPCDConfiguration.configuration_id
        _cmd.Parameters.Add("@pCommType", SqlDbType.Int).Value = SMIB_COMMUNICATION_TYPE.PULSES

        If Not pPCDConfiguration.configuration_name.Equals(_cmd.ExecuteScalar().ToString()) Then
          ' Configuration name changed. Check if there are configurations with same name
          _sb.Length = 0 'Reset stringBuilder
          _sb.AppendLine(" SELECT   COUNT(*) ")
          _sb.AppendLine("   FROM   SMIB_CONFIGURATION ")
          _sb.AppendLine("  WHERE   SC_NAME = @pName ")
          _sb.AppendLine("    AND   SC_CONFIGURATION_ID != @pConfigurationId ")
          _sb.AppendLine("    AND   SC_COMM_TYPE = @pCommType ")

          ' Assign query parameters
          _cmd.Parameters.Add("@pName", SqlDbType.NVarChar).Value = pPCDConfiguration.configuration_name

          ' Assign current command text
          _cmd.CommandText = _sb.ToString()

          ' Check if flag name already exists
          If (_cmd.ExecuteScalar() > 0) Then
            GUI_EndSQLTransaction(_sql_trx, False)
            Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_ALREADY_EXISTS
          Else
            ' Update configuration name
            _sb.Length = 0 'Reset stringBuilder

            _sb.AppendLine(" UPDATE   SMIB_CONFIGURATION ")
            _sb.AppendLine("    SET   SC_NAME = @pName ")
            _sb.AppendLine("  WHERE   SC_CONFIGURATION_ID = @pConfigurationId ")
            _sb.AppendLine("    AND   SC_COMM_TYPE = @pCommType ")

            ' Assign current command text
            _cmd.CommandText = _sb.ToString()

            If _cmd.ExecuteNonQuery() <> 1 Then
              Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
            End If
          End If
        End If

        _sb.Length = 0 'Reset stringBuilder
        _sb.AppendLine(" UPDATE   SMIB_CONFIGURATION ")
        _sb.AppendLine("    SET   SC_DESCRIPTION = @pDescription ")
        _sb.AppendLine("        , SC_MODIFIED = GETDATE() ")
        _sb.AppendLine("  WHERE   SC_CONFIGURATION_ID = @pConfigurationId ")
        _sb.AppendLine("    AND   SC_COMM_TYPE = @pCommType ")

        _cmd.CommandText = _sb.ToString()

        _cmd.Parameters.Add("@pDescription", SqlDbType.NVarChar, 200).Value = pPCDConfiguration.description

        If _cmd.ExecuteNonQuery() <> 1 Then
          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

        ' Update DB
        _sb.Length = 0 'Reset stringBuilder

        _sb.AppendLine(" DELETE   PCD_METERS_TRANSLATION ")
        _sb.AppendLine("  WHERE   PMT_CONFIGURATION_ID = @pConfigurationId ")

        ' Assign current command text
        _cmd.CommandText = _sb.ToString()

        If _cmd.ExecuteNonQuery() < 0 Then
          GUI_EndSQLTransaction(_sql_trx, False)
          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

        _sb.Length = 0

        _sb.AppendLine(" INSERT INTO   PCD_METERS_TRANSLATION ")
        _sb.AppendLine("             ( PMT_CONFIGURATION_ID ")
        _sb.AppendLine("             , PMT_PCD_IO_NUMBER ")
        _sb.AppendLine("             , PMT_PCD_IO_TYPE ")
        _sb.AppendLine("             , PMT_EGM_NUMBER ")
        _sb.AppendLine("             , PMT_EGM_NUMBER_MULTIPLIER ")
        _sb.AppendLine("             , PMT_OUPUT_TIME_PULSE_DOWN ")
        _sb.AppendLine("             , PMT_OUPUT_TIME_PULSE_UP  ) ")
        _sb.AppendLine("      VALUES   ")
        _sb.AppendLine("             ( @pConfigurationId ")
        _sb.AppendLine("             , @pMeterId ")
        _sb.AppendLine("             , @pMeterType ")
        _sb.AppendLine("             , @pSasMeterId ")
        _sb.AppendLine("             , @pMeterDenom ")
        _sb.AppendLine("             , @pTimePulseDown ")
        _sb.AppendLine("             , @pTimePulseUp ) ")

        ' Assign current command text
        _cmd.CommandText = _sb.ToString()

        'Configuration Id already set
        _param_meter_denomination = _cmd.Parameters.Add("@pMeterDenom", SqlDbType.Money)
        _param_pcd_meter = _cmd.Parameters.Add("@pMeterId", SqlDbType.Int)
        _param_sas_meter_id = _cmd.Parameters.Add("@pSasMeterId", SqlDbType.Int)
        _param_pcd_io_type = _cmd.Parameters.Add("@pMeterType", SqlDbType.Int)
        _param_pcd_time_pulse_up = _cmd.Parameters.Add("@pTimePulseUp", SqlDbType.Int)
        _param_pcd_time_pulse_down = _cmd.Parameters.Add("@pTimePulseDown", SqlDbType.Int)

        ' Input ports
        _param_pcd_time_pulse_down.Value = DBNull.Value
        _param_pcd_time_pulse_up.Value = DBNull.Value
        For _idx_port As Integer = 0 To PCD_NUM_INPUT_PORTS - 1
          With pPCDConfiguration.input_ports(_idx_port)
            If .egm_number = PCD_EGM_NULL_VALUE Then
              Continue For
            End If
            _param_meter_denomination.Value = IIf(.egm_number_multiplier <= 0, DBNull.Value, .egm_number_multiplier)
            _param_pcd_meter.Value = .pcd_io_number + (PCD_INPUT_NUMBER_MASK - 1)
            _param_sas_meter_id.Value = .egm_number
            _param_pcd_io_type.Value = .pulse_type Or PCD_IO_TYPE_INPUT

            If _cmd.ExecuteNonQuery() <> 1 Then
              GUI_EndSQLTransaction(_sql_trx, False)
              Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
            End If
          End With
        Next

        ' Output Ports
        For _idx_port As Integer = 0 To PCD_NUM_OUTPUT_PORTS - 1
          With pPCDConfiguration.output_ports(_idx_port)
            If .egm_number = PCD_EGM_NULL_VALUE Then
              Continue For
            End If
            _param_meter_denomination.Value = IIf(.egm_number_multiplier <= 0, DBNull.Value, .egm_number_multiplier)
            _param_pcd_meter.Value = .pcd_io_number
            _param_sas_meter_id.Value = .egm_number
            _param_pcd_io_type.Value = .pulse_type Or PCD_IO_TYPE_OUTPUT
            If .pulse_type And PCD_IO_TYPE_SIGNAL_UP = PCD_IO_TYPE_SIGNAL_UP _
            Or .pulse_type And PCD_IO_TYPE_SIGNAL_DOWN = PCD_IO_TYPE_SIGNAL_DOWN Then
              _param_pcd_time_pulse_down.Value = .time_pulse_down_ms
              _param_pcd_time_pulse_up.Value = .time_pulse_up_ms
            Else
              _param_pcd_time_pulse_down.Value = DBNull.Value
              _param_pcd_time_pulse_up.Value = DBNull.Value
            End If

            If _cmd.ExecuteNonQuery() <> 1 Then
              GUI_EndSQLTransaction(_sql_trx, False)
              Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
            End If
          End With

        Next

        If Not GUI_EndSQLTransaction(_sql_trx, True) Then
          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

      End Using

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

    Catch ex As Exception
      If Not GUI_EndSQLTransaction(_sql_trx, False) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

  End Function

  Private Function InsertPCDConfiguration(ByVal pPCDConfiguration As TYPE_PCD_CONFIGURATION, _
                              ByVal Context As Integer) As Integer

    Dim _sql_trx As System.Data.SqlClient.SqlTransaction = Nothing
    Dim _sb As StringBuilder
    Dim _param_configuration_id As SqlParameter
    Dim _param_meter_denomination As SqlParameter
    Dim _param_pcd_meter As SqlParameter
    Dim _param_pcd_io_type As SqlParameter
    Dim _param_sas_meter_id As SqlParameter
    Dim _param_pcd_time_pulse_up As SqlParameter
    Dim _param_pcd_time_pulse_down As SqlParameter

    If Not GUI_BeginSQLTransaction(_sql_trx) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    Try
      _sb = New StringBuilder()

      _sb.AppendLine("SELECT   COUNT(*) ")
      _sb.AppendLine("  FROM   SMIB_CONFIGURATION ")
      _sb.AppendLine(" WHERE   SC_NAME = @pName ")
      _sb.AppendLine("   AND   SC_COMM_TYPE = @pCommType ")

      Using _cmd As New SqlClient.SqlCommand(_sb.ToString(), _sql_trx.Connection, _sql_trx)

        ' Assign query parameters
        _cmd.Parameters.Add("@pName", SqlDbType.NVarChar).Value = pPCDConfiguration.configuration_name
        _cmd.Parameters.Add("@pCommType", SqlDbType.Int).Value = SMIB_COMMUNICATION_TYPE.PULSES

        ' Check if flag name already exists
        If (_cmd.ExecuteScalar() > 0) Then
          GUI_EndSQLTransaction(_sql_trx, False)
          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_ALREADY_EXISTS
        End If

        ' Insert to DB
        _sb.Length = 0 'Reset stringBuilder

        _sb.AppendLine(" INSERT INTO   SMIB_CONFIGURATION ")
        _sb.AppendLine("             ( SC_NAME ")
        _sb.AppendLine("             , SC_COMM_TYPE ")
        _sb.AppendLine("             , SC_DESCRIPTION ")
        _sb.AppendLine("             , SC_CREATED ")
        _sb.AppendLine("             , SC_MODIFIED ) ")
        _sb.AppendLine("      VALUES ")
        _sb.AppendLine("             ( @pName ")
        _sb.AppendLine("             , @pCommType ")
        _sb.AppendLine("             , @pDescription ")
        _sb.AppendLine("             , GETDATE() ")
        _sb.AppendLine("             , GETDATE() ) ")

        _sb.AppendLine("SET @pConfigurationId = SCOPE_IDENTITY()")

        _cmd.CommandText = _sb.ToString()

        _cmd.Parameters.Add("@pDescription", SqlDbType.NVarChar, 200).Value = pPCDConfiguration.description

        _param_configuration_id = _cmd.Parameters.Add("@pConfigurationId", SqlDbType.Int)
        _param_configuration_id.Direction = ParameterDirection.Output

        If _cmd.ExecuteNonQuery() <> 1 Then
          GUI_EndSQLTransaction(_sql_trx, False)
          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        Else
          pPCDConfiguration.configuration_id = _param_configuration_id.Value
        End If

        _sb.Length = 0

        _sb.AppendLine(" INSERT INTO   PCD_METERS_TRANSLATION ")
        _sb.AppendLine("             ( PMT_CONFIGURATION_ID ")
        _sb.AppendLine("             , PMT_PCD_IO_NUMBER ")
        _sb.AppendLine("             , PMT_PCD_IO_TYPE ")
        _sb.AppendLine("             , PMT_EGM_NUMBER ")
        _sb.AppendLine("             , PMT_EGM_NUMBER_MULTIPLIER ")
        _sb.AppendLine("             , PMT_OUPUT_TIME_PULSE_DOWN ")
        _sb.AppendLine("             , PMT_OUPUT_TIME_PULSE_UP  ) ")
        _sb.AppendLine("      VALUES   ")
        _sb.AppendLine("             ( @pConfigurationId ")
        _sb.AppendLine("             , @pMeterId ")
        _sb.AppendLine("             , @pMeterType ")
        _sb.AppendLine("             , @pSasMeterId ")
        _sb.AppendLine("             , @pMeterDenom ")
        _sb.AppendLine("             , @pTimePulseDown ")
        _sb.AppendLine("             , @pTimePulseUp ) ")

        ' Assign current command text
        _cmd.CommandText = _sb.ToString()

        _param_configuration_id.Direction = ParameterDirection.Input

        'Configuration Id already set
        _param_meter_denomination = _cmd.Parameters.Add("@pMeterDenom", SqlDbType.Money)
        _param_pcd_meter = _cmd.Parameters.Add("@pMeterId", SqlDbType.Int)
        _param_pcd_io_type = _cmd.Parameters.Add("@pMeterType", SqlDbType.Int)
        _param_sas_meter_id = _cmd.Parameters.Add("@pSasMeterId", SqlDbType.Int)
        _param_pcd_time_pulse_down = _cmd.Parameters.Add("@pTimePulseDown", SqlDbType.Int)
        _param_pcd_time_pulse_up = _cmd.Parameters.Add("@pTimePulseUp", SqlDbType.Int)

        ' Input ports
        _param_pcd_time_pulse_up.Value = DBNull.Value
        _param_pcd_time_pulse_down.Value = DBNull.Value
        For _idx_port As Integer = 0 To PCD_NUM_INPUT_PORTS - 1
          With pPCDConfiguration.input_ports(_idx_port)
            If .egm_number = PCD_EGM_NULL_VALUE Then
              Continue For
            End If
            _param_meter_denomination.Value = IIf(.egm_number_multiplier <= 0, DBNull.Value, .egm_number_multiplier)
            _param_pcd_meter.Value = .pcd_io_number + (PCD_INPUT_NUMBER_MASK - 1)
            _param_sas_meter_id.Value = .egm_number
            _param_pcd_io_type.Value = .pulse_type Or PCD_IO_TYPE_INPUT

            If _cmd.ExecuteNonQuery() <> 1 Then
              GUI_EndSQLTransaction(_sql_trx, False)
              Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
            End If
          End With
        Next

        ' Output ports
        For _idx_port As Integer = 0 To PCD_NUM_OUTPUT_PORTS - 1
          With pPCDConfiguration.output_ports(_idx_port)
            If .egm_number = PCD_EGM_NULL_VALUE Then
              Continue For
            End If
            _param_meter_denomination.Value = IIf(.egm_number_multiplier <= 0, DBNull.Value, .egm_number_multiplier)
            _param_pcd_meter.Value = .pcd_io_number
            _param_sas_meter_id.Value = .egm_number
            _param_pcd_io_type.Value = .pulse_type Or PCD_IO_TYPE_OUTPUT

            If .pulse_type = PCD_IO_TYPE_SIGNAL_DOWN _
            Or .pulse_type = PCD_IO_TYPE_SIGNAL_UP Then
              _param_pcd_time_pulse_up.Value = .time_pulse_up_ms
              _param_pcd_time_pulse_down.Value = .time_pulse_down_ms
            Else
              _param_pcd_time_pulse_up.Value = DBNull.Value
              _param_pcd_time_pulse_down.Value = DBNull.Value
            End If

            If _cmd.ExecuteNonQuery() <> 1 Then
              GUI_EndSQLTransaction(_sql_trx, False)
              Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
            End If
          End With
        Next

        If Not GUI_EndSQLTransaction(_sql_trx, True) Then
          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

      End Using

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

    Catch ex As Exception
      If Not GUI_EndSQLTransaction(_sql_trx, False) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

  End Function

  Private Function DeletePCDConfiguration(ByVal ConfigurationId As Int64, _
                              ByVal Context As Integer) As Integer

    'Dim _sql_query As String
    Dim _db_count As Integer = 0
    Dim _str_bld As StringBuilder
    Dim _sql_tx As System.Data.SqlClient.SqlTransaction = Nothing

    _str_bld = New StringBuilder()
    '     - Remove the FLAG item from the FLAGS table
    Try
      If Not GUI_BeginSQLTransaction(_sql_tx) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      ' Before removing, check dependencies with other tables

      ' Table account flags
      _str_bld.AppendLine("SELECT   COUNT(*) ")
      _str_bld.AppendLine("  FROM   TERMINALS ")
      _str_bld.AppendLine(" WHERE   TE_SMIB2EGM_CONF_ID = @pConfigurationId ")
      _str_bld.AppendLine("   AND   TE_SMIB2EGM_COMM_TYPE = @pCommType ")

      Using _cmd As New SqlCommand(_str_bld.ToString(), _sql_tx.Connection, _sql_tx)
        _cmd.Parameters.Add("@pConfigurationId", SqlDbType.BigInt).Value = ConfigurationId
        _cmd.Parameters.Add("@pCommType", SqlDbType.Int).Value = SMIB_COMMUNICATION_TYPE.PULSES

        If (_cmd.ExecuteScalar() > 0) Then
          GUI_EndSQLTransaction(_sql_tx, False)
          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DEPENDENCIES
        End If

        _str_bld.Length = 0
        _str_bld.AppendLine(" DELETE PCD_METERS_TRANSLATION WHERE PMT_CONFIGURATION_ID = @pConfigurationId ")

        _cmd.CommandText = _str_bld.ToString()

        If _cmd.ExecuteNonQuery() < 0 Then
          GUI_EndSQLTransaction(_sql_tx, False)
          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

        _str_bld.Length = 0
        _str_bld.AppendLine(" DELETE   SMIB_CONFIGURATION ")
        _str_bld.AppendLine("  WHERE   SC_CONFIGURATION_ID = @pConfigurationId ")
        _str_bld.AppendLine("    AND   SC_COMM_TYPE = @pCommType ")

        _cmd.CommandText = _str_bld.ToString()

        If _cmd.ExecuteNonQuery() <> 1 Then
          GUI_EndSQLTransaction(_sql_tx, False)
          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

        ' Commit
        If Not GUI_EndSQLTransaction(_sql_tx, True) Then
          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

      End Using
    Catch ex As Exception
      ' Rollback  
      GUI_EndSQLTransaction(_sql_tx, False)

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

  End Function

  Private Function ReadPCDConfigurationByCommunicationType(ByVal pCommType As SMIB_COMMUNICATION_TYPE, ByVal pDbTrx As DB_TRX) As DataTable
    Dim _str_bld As StringBuilder
    Dim _table As DataTable

    _str_bld = New StringBuilder()
    _table = New DataTable()

    _str_bld.AppendLine("SELECT   SC_CONFIGURATION_ID ")
    _str_bld.AppendLine("       , SC_NAME ")
    _str_bld.AppendLine("  FROM   SMIB_CONFIGURATION ")
    _str_bld.AppendLine(" WHERE   SC_COMM_TYPE = @pCommType ")

    Using _sql_cmd As New SqlCommand(_str_bld.ToString())
      _sql_cmd.Parameters.Add("@pCommType", SqlDbType.Int).Value = pCommType
      Using _sql_da As New SqlDataAdapter(_sql_cmd)
        pDbTrx.Fill(_sql_da, _table)
      End Using
    End Using

    Return _table
  End Function

  Private Function ReadPCDConfigurationByID(ByVal pConfigurationId As Int64, ByVal pDbTrx As DB_TRX) As DataTable
    Dim _str_bld As StringBuilder
    Dim _table As DataTable

    _str_bld = New StringBuilder()
    _table = New DataTable()

    _str_bld.AppendLine("SELECT   SC_CONFIGURATION_ID ")
    _str_bld.AppendLine("       , SC_COMM_TYPE ")
    _str_bld.AppendLine("       , SC_NAME ")
    _str_bld.AppendLine("       , SC_DESCRIPTION ")
    _str_bld.AppendLine("  FROM   SMIB_CONFIGURATION ")
    _str_bld.AppendLine(" WHERE   SC_CONFIGURATION_ID = @pConfigurationId ")

    Using _sql_cmd As New SqlCommand(_str_bld.ToString())
      _sql_cmd.Parameters.Add("@pConfigurationId", SqlDbType.BigInt).Value = pConfigurationId
      Using _sql_da As New SqlDataAdapter(_sql_cmd)
        pDbTrx.Fill(_sql_da, _table)
      End Using
    End Using

    Return _table
  End Function

#End Region

#Region "Public Functions"

  Public Sub InitInputPort(ByVal Port As Integer)
    m_pcd_configuration.InitInputPort(Port)
  End Sub

  Public Sub InitOutputPort(ByVal Port As Integer)
    m_pcd_configuration.InitOutputPort(Port)
  End Sub

  'FAV 29-MAY-2015
  ' PURPOSE: Return a datatable of SMIB_CONFIGURATION rows by communication type
  '
  '  PARAMS:
  '     - INPUT:
  '         - CommunicationType SMIB_COMMUNICATION_TYPE
  '
  '     - OUTPUT:
  '
  ' RETURNS: DataTable
  Public Function GetPCDConfigurationsByCommunicationType(ByVal CommunicationType As SMIB_COMMUNICATION_TYPE) As DataTable
    Dim _table As DataTable
    Using _db_trx As New DB_TRX()
      _table = ReadPCDConfigurationByCommunicationType(CommunicationType, _db_trx)
    End Using
    Return _table
  End Function     ' GetPCDConfigurationsByCommunicationType

  'FAV 29-MAY-2015
  ' PURPOSE: Return a datatable of SMIB_CONFIGURATION rows by Id
  '
  '  PARAMS:
  '     - INPUT:
  '         - pConfigurationId Int64
  '     - OUTPUT:
  '
  ' RETURNS: DataTable
  Public Function GetPCDConfigurationsById(ByVal pConfigurationId As Int64) As DataTable
    Dim _table As DataTable
    Using _db_trx As New DB_TRX()
      _table = ReadPCDConfigurationByID(pConfigurationId, _db_trx)
    End Using
    Return _table
  End Function
#End Region

End Class