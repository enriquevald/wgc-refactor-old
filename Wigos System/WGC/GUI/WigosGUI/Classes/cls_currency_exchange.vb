'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : cls_currency_exchange.vb
'
' DESCRIPTION : Currency Exchange Data class
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 18-JUN-2013  DHA    Initial version   
'--------------------------------------------------------------------
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports System.Text
Imports WSI.Common
Imports System.Reflection

Public Class CLASS_CURRENCY_EXCHANGE
  Inherits CLASS_BASE

#Region " Constants "

  Public Const SQL_COLUMN_CUR_CURRENCY_TYPE As Integer = 0
  Public Const SQL_COLUMN_CUR_CURRENCY_ISO_CODE As Integer = 1
  Public Const SQL_COLUMN_CUR_CURRENCY_DESCRIPTION As Integer = 2
  Public Const SQL_COLUMN_CUR_CURRENCY_CHANGE As Integer = 3
  Public Const SQL_COLUMN_CUR_CURRENCY_NUM_DECIMALS As Integer = 4
  Public Const SQL_COLUMN_CUR_CURRENCY_VARIABLE_COMMISSION As Integer = 5
  Public Const SQL_COLUMN_CUR_CURRENCY_FIXED_COMMISSION As Integer = 6
  Public Const SQL_COLUMN_CUR_CURRENCY_VARIABLE_NR2 As Integer = 7
  Public Const SQL_COLUMN_CUR_CURRENCY_FIXED_NR2 As Integer = 8
  Public Const SQL_COLUMN_CUR_CURRENCY_STATUS As Integer = 9

  Public Const EMTPY_VALUE As String = "---"

#End Region  ' Constants 

#Region " Structures "

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_DATA_CURRENCY_EXCHANGE
    Public data_table_cur_exc As DataTable

    ' Init structure
    Public Sub New()
      data_table_cur_exc = New DataTable()
    End Sub
  End Class

#End Region ' Structures

#Region " Members "

  Protected m_data_currency_exchange As New TYPE_DATA_CURRENCY_EXCHANGE
  Protected Shared m_national_currency As New CurrencyExchange

#End Region    ' Members

#Region "Properties"

  Public Overloads Property DataCurrencyExchange() As DataTable
    Get
      Return m_data_currency_exchange.data_table_cur_exc
    End Get

    Set(ByVal Value As DataTable)
      m_data_currency_exchange.data_table_cur_exc = Value
    End Set

  End Property  ' DataCurrencyExchange

  Public ReadOnly Property DataNationalCurrencyExchange() As CurrencyExchange
    Get
      Return m_national_currency
    End Get

  End Property

#End Region   ' Properties

#Region "Overrides"

  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _type_desc As String

    _type_desc = String.Empty

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_CASHIER_CONFIGURATION)

    _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(2079), "")

    For Each _currency_exchange As DataRow In Me.DataCurrencyExchange.Rows
      Select Case (CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_TYPE)
        Case CurrencyExchangeType.CARD
          _type_desc = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2156)
        Case CurrencyExchangeType.CURRENCY
          _type_desc = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2157)
      End Select

      ' Description
      Call _auditor_data.SetField(0, _currency_exchange(SQL_COLUMN_CUR_CURRENCY_DESCRIPTION), _
                                String.Format("{0} ({1}) - {2}", _currency_exchange(SQL_COLUMN_CUR_CURRENCY_ISO_CODE), _
                                _type_desc, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2142)))

      ' Change
      If _currency_exchange(SQL_COLUMN_CUR_CURRENCY_CHANGE) Is DBNull.Value Then
        Call _auditor_data.SetField(0, EMTPY_VALUE, _
                                String.Format("{0} ({1}) - {2}", _currency_exchange(SQL_COLUMN_CUR_CURRENCY_ISO_CODE), _
                                _type_desc, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2144)))
      Else
        Call _auditor_data.SetField(0, _currency_exchange(SQL_COLUMN_CUR_CURRENCY_CHANGE), _
                                String.Format("{0} ({1}) - {2}", _currency_exchange(SQL_COLUMN_CUR_CURRENCY_ISO_CODE), _
                                _type_desc, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2144)))
      End If

      ' Num Decimals
      If _currency_exchange(SQL_COLUMN_CUR_CURRENCY_NUM_DECIMALS) Is DBNull.Value Then
        Call _auditor_data.SetField(0, EMTPY_VALUE, _
                                String.Format("{0} ({1}) - {2}", _currency_exchange(SQL_COLUMN_CUR_CURRENCY_ISO_CODE), _
                                _type_desc, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2145)))
      Else
        Call _auditor_data.SetField(0, _currency_exchange(SQL_COLUMN_CUR_CURRENCY_NUM_DECIMALS), _
                                String.Format("{0} ({1}) - {2}", _currency_exchange(SQL_COLUMN_CUR_CURRENCY_ISO_CODE), _
                                _type_desc, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2145)))
      End If

      ' Fixed Commissions
      If _currency_exchange(SQL_COLUMN_CUR_CURRENCY_FIXED_COMMISSION) Is DBNull.Value Then
        Call _auditor_data.SetField(0, EMTPY_VALUE, _
                                String.Format("{0} ({1}) - {2}", _currency_exchange(SQL_COLUMN_CUR_CURRENCY_ISO_CODE), _
                                _type_desc, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2158)))
      Else
        Call _auditor_data.SetField(0, _currency_exchange(SQL_COLUMN_CUR_CURRENCY_FIXED_COMMISSION), _
                                String.Format("{0} ({1}) - {2}", _currency_exchange(SQL_COLUMN_CUR_CURRENCY_ISO_CODE), _
                                _type_desc, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2158)))
      End If

      ' Variable Comissions
      If _currency_exchange(SQL_COLUMN_CUR_CURRENCY_VARIABLE_COMMISSION) Is DBNull.Value Then
        Call _auditor_data.SetField(0, EMTPY_VALUE, _
                                String.Format("{0} ({1}) - {2}", _currency_exchange(SQL_COLUMN_CUR_CURRENCY_ISO_CODE), _
                                _type_desc, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2159)))
      Else
        Call _auditor_data.SetField(0, _currency_exchange(SQL_COLUMN_CUR_CURRENCY_VARIABLE_COMMISSION), _
                                String.Format("{0} ({1}) - {2}", _currency_exchange(SQL_COLUMN_CUR_CURRENCY_ISO_CODE), _
                                _type_desc, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2159)))
      End If

      ' Fixed NR2
      If _currency_exchange(SQL_COLUMN_CUR_CURRENCY_FIXED_NR2) Is DBNull.Value Then
        Call _auditor_data.SetField(0, EMTPY_VALUE, _
                                String.Format("{0} ({1}) - {2}", _currency_exchange(SQL_COLUMN_CUR_CURRENCY_ISO_CODE), _
                                _type_desc, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2160)))
      Else
        Call _auditor_data.SetField(0, _currency_exchange(SQL_COLUMN_CUR_CURRENCY_FIXED_NR2), _
                                String.Format("{0} ({1}) - {2}", _currency_exchange(SQL_COLUMN_CUR_CURRENCY_ISO_CODE), _
                                _type_desc, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2160)))
      End If

      ' Variable NR2
      If _currency_exchange(SQL_COLUMN_CUR_CURRENCY_VARIABLE_NR2) Is DBNull.Value Then
        Call _auditor_data.SetField(0, EMTPY_VALUE, _
                                String.Format("{0} ({1}) - {2}", _currency_exchange(SQL_COLUMN_CUR_CURRENCY_ISO_CODE), _
                                _type_desc, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2161)))
      Else
        Call _auditor_data.SetField(0, _currency_exchange(SQL_COLUMN_CUR_CURRENCY_VARIABLE_NR2), _
                                String.Format("{0} ({1}) - {2}", _currency_exchange(SQL_COLUMN_CUR_CURRENCY_ISO_CODE), _
                                _type_desc, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2161)))
      End If

      ' Status
      Call _auditor_data.SetField(0, _currency_exchange(SQL_COLUMN_CUR_CURRENCY_STATUS), _
                                String.Format("{0} ({1}) - {2}", _currency_exchange(SQL_COLUMN_CUR_CURRENCY_ISO_CODE), _
                                _type_desc, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2143)))
    Next

    Return _auditor_data

  End Function   ' AuditorData

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR

  End Function     ' DB_Delete

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR

  End Function     ' DB_Insert

  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim rc As Integer

    ' Read currency exchange data
    rc = CurrencyExchangeData_DbRead(SqlCtx)

    Select Case rc
      Case ENUM_DB_RC.DB_RC_OK
        Return ENUM_STATUS.STATUS_OK

      Case ENUM_DB_RC.DB_RC_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function       ' DB_Read

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim rc As Integer

    ' Update currency exchange data
    rc = CurrencyExchangeData_DbUpdate(SqlCtx)

    Select Case rc
      Case ENUM_DB_RC.DB_RC_OK
        Return ENUM_STATUS.STATUS_OK

      Case ENUM_DB_RC.DB_RC_ERROR_NOT_FOUND
        Return CLASS_BASE.ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function     ' DB_Update

  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE
    Dim temp_currencies_exchange_data As CLASS_CURRENCY_EXCHANGE

    temp_currencies_exchange_data = New CLASS_CURRENCY_EXCHANGE

    temp_currencies_exchange_data.m_data_currency_exchange.data_table_cur_exc = Me.m_data_currency_exchange.data_table_cur_exc.Copy()

    Return temp_currencies_exchange_data
  End Function     ' Duplicate

#End Region ' Overrides

#Region "Private Functions"

  Private Function CurrencyExchangeData_DbRead(ByVal Context As Integer) As Integer

    Dim _national_currency As CurrencyExchange
    Dim _currencies_list As New List(Of CurrencyExchange)
    Dim _sort_rows() As DataRow
    Dim _dt_currency As DataTable
    Dim _idx_row As Integer
    Dim _idx_column As Integer
    Dim _row As DataRow
    Dim _sorted_rows() As DataRow
    Dim _idx_last_row As Integer

    m_national_currency = Nothing
    _national_currency = Nothing
    _idx_last_row = 0

    If WSI.Common.CurrencyExchange.GetAllowedCurrencies(False, True, _national_currency, _currencies_list) Then
      If _currencies_list.Count < 1 Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
      End If
      _dt_currency = ConvertListToDataTable(_currencies_list)
      _sorted_rows = New DataRow(_dt_currency.Rows.Count - 1) {}

      m_data_currency_exchange.data_table_cur_exc = _dt_currency.Clone()

      ' Sort currency exchange table: first national currency exchange
      _sort_rows = _dt_currency.Select("m_currency_code = '" & _national_currency.CurrencyCode & "' And m_type = " & _national_currency.Type)
      _sort_rows.CopyTo(_sorted_rows, _idx_last_row)
      _idx_last_row = _sort_rows.Length

      ' Sort currency exchange table: others national currency exchange
      _sort_rows = _dt_currency.Select("m_currency_code = '" & _national_currency.CurrencyCode & "' And m_type <> " & _national_currency.Type)
      _sort_rows.CopyTo(_sorted_rows, _idx_last_row)
      _idx_last_row = _idx_last_row + _sort_rows.Length

      ' Sort currency exchange table: others currency exchange
      _sort_rows = _dt_currency.Select("m_currency_code <> '" & _national_currency.CurrencyCode & "'", "m_currency_code ASC")
      _sort_rows.CopyTo(_sorted_rows, _idx_last_row)
      _idx_last_row = _idx_last_row + _sort_rows.Length

      For _idx_row = 0 To _sorted_rows.Length - 1
        _row = m_data_currency_exchange.data_table_cur_exc.NewRow()
        For _idx_column = 0 To _sorted_rows(_idx_row).ItemArray.Length - 1

          If _sorted_rows(_idx_row)(_idx_column) Is DBNull.Value Then
            _row(_idx_column) = _sorted_rows(_idx_row)(_idx_column)
          Else
            Select Case (_idx_column)
              Case SQL_COLUMN_CUR_CURRENCY_CHANGE
                _row(_idx_column) = GUI_ParseCurrency(_sorted_rows(_idx_row)(_idx_column))
              Case SQL_COLUMN_CUR_CURRENCY_NUM_DECIMALS
                _row(_idx_column) = GUI_ParseNumber(_sorted_rows(_idx_row)(_idx_column))
              Case SQL_COLUMN_CUR_CURRENCY_VARIABLE_COMMISSION
                _row(_idx_column) = GUI_ParseCurrency(_sorted_rows(_idx_row)(_idx_column))
              Case SQL_COLUMN_CUR_CURRENCY_FIXED_COMMISSION
                _row(_idx_column) = GUI_ParseCurrency(_sorted_rows(_idx_row)(_idx_column))
              Case SQL_COLUMN_CUR_CURRENCY_VARIABLE_NR2
                _row(_idx_column) = GUI_ParseCurrency(_sorted_rows(_idx_row)(_idx_column))
              Case SQL_COLUMN_CUR_CURRENCY_FIXED_NR2
                _row(_idx_column) = GUI_ParseCurrency(_sorted_rows(_idx_row)(_idx_column))
              Case Else
                _row(_idx_column) = _sorted_rows(_idx_row)(_idx_column)
            End Select
          End If
        Next
        m_data_currency_exchange.data_table_cur_exc.Rows.Add(_row)
      Next

      m_data_currency_exchange.data_table_cur_exc.AcceptChanges()
      m_national_currency = _national_currency
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
    End If
    Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

  End Function    ' CurrencyExchangeData_DbRead

  Private Function CurrencyExchangeData_DbUpdate(ByVal Context As Integer) As Integer

    Dim SqlTrans As System.Data.SqlClient.SqlTransaction = Nothing
    Dim commit_trx As Boolean
    Dim result As Integer

    Dim _sql_txt_update As System.Text.StringBuilder
    Dim _num_to_update As Integer

    Dim _sql_txt_insert As System.Text.StringBuilder

    Dim _num_updated As Integer
    Dim _rc As Integer

    Try

      If Not GUI_BeginSQLTransaction(SqlTrans) Then
        Exit Function
      End If

      commit_trx = False

      ' Insert currency exchange audit
      For Each row As DataRow In m_data_currency_exchange.data_table_cur_exc.Rows
        If Not row(SQL_COLUMN_CUR_CURRENCY_CHANGE, DataRowVersion.Original).Equals(row(SQL_COLUMN_CUR_CURRENCY_CHANGE)) Then
          _sql_txt_insert = New System.Text.StringBuilder()
          _sql_txt_insert.AppendLine("INSERT INTO   CURRENCY_EXCHANGE_AUDIT")
          _sql_txt_insert.AppendLine("              ( CEA_TYPE ")
          _sql_txt_insert.AppendLine("              , CEA_CURRENCY_ISO_CODE ")
          _sql_txt_insert.AppendLine("              , CEA_DATETIME ")
          _sql_txt_insert.AppendLine("              , CEA_OLD_CHANGE ")
          _sql_txt_insert.AppendLine("              , CEA_NEW_CHANGE ) ")
          _sql_txt_insert.AppendLine("              VALUES ")
          _sql_txt_insert.AppendLine("              ( @pCeaType ")
          _sql_txt_insert.AppendLine("              , @pCeaIsoCode ")
          _sql_txt_insert.AppendLine("              , GetDate() ")
          _sql_txt_insert.AppendLine("              , @pCeaOldChange ")
          _sql_txt_insert.AppendLine("              , @pCeaNewChange ) ")

          Using _sql_cmd As New SqlCommand(_sql_txt_insert.ToString(), SqlTrans.Connection, SqlTrans)
            _sql_cmd.Parameters.Add("@pCeaType", SqlDbType.Int).Value = row(SQL_COLUMN_CUR_CURRENCY_TYPE)
            _sql_cmd.Parameters.Add("@pCeaIsoCode", SqlDbType.NVarChar).Value = row(SQL_COLUMN_CUR_CURRENCY_ISO_CODE)
            _sql_cmd.Parameters.Add("@pCeaOldChange", SqlDbType.Decimal).Value = row(SQL_COLUMN_CUR_CURRENCY_CHANGE, DataRowVersion.Original)
            _sql_cmd.Parameters.Add("@pCeaNewChange", SqlDbType.Decimal).Value = row(SQL_COLUMN_CUR_CURRENCY_CHANGE)
            _rc = _sql_cmd.ExecuteNonQuery()

            If _rc <> 1 Then
              ' Rollback  
              Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
            End If

          End Using
        End If
      Next

      _num_to_update = m_data_currency_exchange.data_table_cur_exc.Select("", "", DataViewRowState.ModifiedCurrent).Length

      ' Update currency exchange
      If _num_to_update > 0 Then
        _sql_txt_update = New System.Text.StringBuilder()
        _sql_txt_update.AppendLine("UPDATE   CURRENCY_EXCHANGE")
        _sql_txt_update.AppendLine("   SET   CE_DESCRIPTION   = @pCurDescription")
        _sql_txt_update.AppendLine("       , CE_CHANGE      = @pCurChange")
        _sql_txt_update.AppendLine("       , CE_NUM_DECIMALS    = @pCurNumDecimals")
        _sql_txt_update.AppendLine("       , CE_VARIABLE_COMMISSION    = @pCurVariableCommission")
        _sql_txt_update.AppendLine("       , CE_FIXED_COMMISSION    = @pCurFixedCommission")
        _sql_txt_update.AppendLine("       , CE_VARIABLE_NR2    = @pCurVariableNR2")
        _sql_txt_update.AppendLine("       , CE_FIXED_NR2    = @pCurFixedNR2")
        _sql_txt_update.AppendLine("       , CE_STATUS    = @pCurStatus")
        _sql_txt_update.AppendLine(" WHERE   CE_CURRENCY_ISO_CODE  = @pCurIsoCode")
        _sql_txt_update.AppendLine("         AND CE_TYPE  = @pCurType")

        Using _da As New SqlDataAdapter()
          Using _sql_cmd As New SqlCommand(_sql_txt_update.ToString(), SqlTrans.Connection, SqlTrans)
            _sql_cmd.Parameters.Add("@pCurDescription", SqlDbType.NVarChar).SourceColumn = "M_DESCRIPTION"
            _sql_cmd.Parameters.Add("@pCurChange", SqlDbType.Decimal).SourceColumn = "M_CHANGE_RATE"
            _sql_cmd.Parameters.Add("@pCurNumDecimals", SqlDbType.Int).SourceColumn = "M_DECIMALS"
            _sql_cmd.Parameters.Add("@pCurVariableCommission", SqlDbType.Money).SourceColumn = "M_VARIABLE_COMISSION"
            _sql_cmd.Parameters.Add("@pCurFixedCommission", SqlDbType.Money).SourceColumn = "M_FIXED_COMISSION"
            _sql_cmd.Parameters.Add("@pCurVariableNR2", SqlDbType.Money).SourceColumn = "M_VARIABLE_NR2"
            _sql_cmd.Parameters.Add("@pCurFixedNR2", SqlDbType.Money).SourceColumn = "M_FIXED_NR2"
            _sql_cmd.Parameters.Add("@pCurStatus", SqlDbType.Bit).SourceColumn = "M_STATUS"
            _sql_cmd.Parameters.Add("@pCurIsoCode", SqlDbType.NVarChar).SourceColumn = "M_CURRENCY_CODE"
            _sql_cmd.Parameters.Add("@pCurType", SqlDbType.Int).SourceColumn = "M_TYPE"

            _da.UpdateCommand = _sql_cmd
            _num_updated = _da.Update(m_data_currency_exchange.data_table_cur_exc)
          End Using
        End Using

        If _num_to_update <> _num_updated Then
          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If
      End If

      commit_trx = True

    Catch ex As Exception
      ' Do nothing
      Debug.WriteLine(ex.Message)

    Finally
      ' Either commit or rollback
      If Not (SqlTrans.Connection Is Nothing) Then
        If commit_trx Then
          SqlTrans.Commit()
          result = ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Else
          SqlTrans.Rollback()
          result = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If
      End If

      SqlTrans.Dispose()
      SqlTrans = Nothing

    End Try

    Return result

  End Function  ' CurrencyExchangeData_DbUpdate

  ' PURPOSE: Convert list to table
  '
  '  PARAMS:
  '     - INPUT:
  '           - list
  '     - OUTPUT:
  '           - datatable
  '
  ' RETURNS:
  '     - None

  Private Function ConvertListToDataTable(Of T)(ByVal list As IList(Of T)) As DataTable
    Dim _table As New DataTable()
    Dim _fields() As FieldInfo

    _fields = GetType(T).GetFields(BindingFlags.Instance Or BindingFlags.NonPublic)

    For Each field As FieldInfo In _fields
      If field.FieldType.IsGenericType AndAlso field.FieldType.GetGenericTypeDefinition() Is GetType(Nullable(Of )) Then
        _table.Columns.Add(field.Name, field.FieldType.GetGenericArguments()(0))
      Else
        _table.Columns.Add(field.Name, field.FieldType)
      End If
    Next
    For Each item As T In list
      Dim row As DataRow = _table.NewRow()
      For Each field As FieldInfo In _fields
        If field.GetValue(item) Is Nothing Then
          row(field.Name) = DBNull.Value
        Else
          row(field.Name) = field.GetValue(item)
        End If

      Next
      _table.Rows.Add(row)
    Next
    Return _table
  End Function  ' ConvertListToDataTable

  Private Function GetRepresentativeValue(ByVal Value As String) As String

    If Value Then
      Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(698) ' "Si"
    Else
      Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(699) ' "No"
    End If

  End Function ' GetRepresentativeValue

#End Region

End Class
