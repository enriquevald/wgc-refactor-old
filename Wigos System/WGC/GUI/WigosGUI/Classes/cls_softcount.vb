
'-------------------------------------------------------------------
' Copyright � 2014 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : cls_soft_count.vb
'
' DESCRIPTION :
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 21-OCT-2014  HBB    Initial version   
' 11-APR-2017  RAB    PBI 26539:MES10 Ticket validation - Prevent the use of dealer copy tickets (GUI)
'--------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports System.Text
Imports WSI.Common

Public Enum ENUM_SOFTCOUNT_TYPE
  TYPE_TERMINAL = 0
  TYPE_CASHIER = 1
End Enum

Public Enum ENUM_SOFTCOUNT_FIELD
  FIELD_STACKER = 0
  FIELD_FLOORID = 1
End Enum


  Public Class CLASS_SOFTCOUNT
    Inherits CLASS_BASE

#Region "Members"
  'terminal
  Private m_stacker_id As Int64
  Private m_floor_id As String
  Private m_terminal_name As String
  Private m_insertion_date As DateTime
  Private m_extraction_date As DateTime


  'cajero
  'Private x1, x2
  Private m_insertdate As DateTime
  Private m_extrac_date As DateTime

  'comunes
  Private m_cage_movement As Int64
  Private m_list_of_currencies As List(Of String)
  Private m_table_denominations As DataTable

  Private m_user As String
  Private m_xml_data As String
  Private m_softcount_type As ENUM_SOFTCOUNT_TYPE
  Private m_softcount_field As ENUM_SOFTCOUNT_FIELD

#End Region

#Region "Constants"
  Private Const SQL_COLUMN_INSERTION_DATETIME As Int32 = 0
  Private Const SQL_COLUMN_EXTRACTION_DATETIME As Int32 = 1
  Private Const SQL_COLUMN_TERMINAL_NAME As Int32 = 2
#End Region

#Region "Properties"

  Public Property StackerId() As Int64
    Get
      Return m_stacker_id
    End Get

    Set(ByVal Value As Int64)
      m_stacker_id = Value
    End Set
  End Property

  Public Property FloorId() As String
    Get
      Return m_floor_id
    End Get
    Set(ByVal value As String)
      m_floor_id = value
    End Set
  End Property

  Public Property TerminalName() As String
    Get
      Return m_terminal_name
    End Get
    Set(ByVal value As String)
      m_terminal_name = value
    End Set
  End Property

  Public Property User() As String
    Get
      Return m_user
    End Get
    Set(ByVal value As String)
      m_user = value
    End Set
  End Property

  Public Property XMLData() As String
    Get
      Return m_xml_data
    End Get
    Set(ByVal value As String)
      m_xml_data = value
    End Set
  End Property

  Public Property InsertionDate() As DateTime
    Get
      Return m_insertion_date
    End Get
    Set(ByVal value As DateTime)
      m_insertion_date = value
    End Set
  End Property

  Public Property ExtractionDate() As DateTime
    Get
      Return m_extraction_date
    End Get
    Set(ByVal value As DateTime)
      m_extraction_date = value
    End Set
  End Property

  Public Property SofctountType() As ENUM_SOFTCOUNT_TYPE
    Get
      Return m_softcount_type
    End Get
    Set(ByVal value As ENUM_SOFTCOUNT_TYPE)
      m_softcount_type = value
    End Set
  End Property

  Public ReadOnly Property ListOfCurrencies() As List(Of String)
    Get
      Return m_list_of_currencies
    End Get
  End Property

  Public ReadOnly Property TableDenominations() As DataTable
    Get
      Return m_table_denominations
    End Get
  End Property

#End Region

#Region "Overrides Functions"
  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim _auditor_data As CLASS_AUDITOR_DATA

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_CAGE)

    Return _auditor_data
  End Function

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

  End Function

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

  End Function

  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim rc As Integer

    rc = ReadCageMovement()

    Select Case rc
      Case ENUM_DB_RC.DB_RC_OK
        Return ENUM_STATUS.STATUS_OK

      Case ENUM_DB_RC.DB_RC_ERROR_NOT_FOUND
        Return CLASS_BASE.ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select


  End Function

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

  End Function

  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim _temp_cage_control As CLASS_SOFTCOUNT

    _temp_cage_control = New CLASS_SOFTCOUNT()

    _temp_cage_control.InsertionDate = Me.InsertionDate
    _temp_cage_control.ExtractionDate = Me.ExtractionDate
    _temp_cage_control.TerminalName = Me.TerminalName
    _temp_cage_control.SofctountType = Me.SofctountType
    _temp_cage_control.m_list_of_currencies = Me.m_list_of_currencies
    _temp_cage_control.m_table_denominations = Me.m_table_denominations

    Return _temp_cage_control

  End Function

#End Region

#Region "public functions"

  Public Function IsTicketValid(ByVal ValidationNumber As String) As Boolean

  End Function

  Public Sub GetCurrenciesList()

    Dim _currencies_accepted As String
    Dim _current_iso_code As String

    _currencies_accepted = ""
    _current_iso_code = ""
    m_list_of_currencies = New List(Of String)

    Select Case m_softcount_type
      Case ENUM_SOFTCOUNT_TYPE.TYPE_TERMINAL
        _current_iso_code = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode", "MXN")
        m_list_of_currencies.Add(_current_iso_code) 'this list is prepared to have more than one isocode, when m_sofctount_type = TYPE_CASHIER

      Case ENUM_SOFTCOUNT_TYPE.TYPE_CASHIER

    End Select

    If TITO.Utils.IsTitoMode() Then
      m_list_of_currencies.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4600))
    End If

    'queda pendiente ver si se necesitara hacer esta query para coger las monedas
    'Dim _sql As StringBuilder
    'Dim _table As DataTable

    '_table = New DataTable()

    '_sql = New StringBuilder()

    '_sql.AppendLine("  FROM SELECT  DISTINCT (CGC_ISO_CODE) ")
    '_sql.AppendLine("          FROM  CAGE_CURRENCIES ")
    '_sql.AppendLine("         UNION ")
    '_sql.AppendLine("        SELECT  CHS_ISO_CODE AS ISO ")
    '_sql.AppendLine("          FROM  CHIPS_SETS ")
    '_sql.AppendLine("         WHERE  CHS_ALLOWED = 1 ")

    'Using _sql_trx As New DB_TRX()
    '  Using _sql_cmd As New SqlCommand(_sql.ToString(), _sql_trx.SqlTransaction.Connection, _sql_trx.SqlTransaction)

    '    Using _sql_da As New SqlDataAdapter(_sql_cmd)
    '      _sql_da.Fill(_table)
    '    End Using
    '  End Using
    'End Using

  End Sub

#End Region

#Region "Private functions"

  Private Sub ConvertToXml(ByVal Tables As DataSet)

  End Sub

  Private Function ReadCageMovement() As ENUM_STATUS

    Dim _cage_movement_data As DataTable

    _cage_movement_data = GetTerminalCageMovementInfo(ENUM_SOFTCOUNT_TYPE.TYPE_TERMINAL)

    If _cage_movement_data.Rows.Count > 0 Then
      Call LoadCageMovement(_cage_movement_data)
      Return ENUM_STATUS.STATUS_OK
    Else
      Return ENUM_DB_RC.DB_RC_ERROR_NOT_FOUND
    End If


  End Function

  Private Sub LoadCageMovement(ByVal CageMovementData As DataTable)

    Select Case m_softcount_type
      Case ENUM_SOFTCOUNT_TYPE.TYPE_TERMINAL
        If CageMovementData.Rows.Count > 0 Then
          TerminalName = CageMovementData.Rows(0).Item(SQL_COLUMN_TERMINAL_NAME)
          ExtractionDate = CageMovementData.Rows(0).Item(SQL_COLUMN_EXTRACTION_DATETIME)
          InsertionDate = CageMovementData.Rows(0).Item(SQL_COLUMN_INSERTION_DATETIME)
        End If

    End Select


  End Sub

  Private Sub InsertSoftCount()

  End Sub

#End Region

#Region "SQL access"
  Public Sub GetDenominationsByCurrencies()
    Dim _sb As StringBuilder
    Dim _table As DataTable

    _sb = New StringBuilder()
    _table = New DataTable()
    m_table_denominations = New DataTable()

    _sb.AppendLine("   SELECT   CGC_ISO_CODE AS ISO_CODE                 ")
    _sb.AppendLine("          , CGC_DENOMINATION AS DENOMINATION         ")
    _sb.AppendLine("     FROM   CAGE_CURRENCIES                          ")
    _sb.AppendLine("    WHERE   CGC_DENOMINATION > 0                     ")
    _sb.AppendLine("UNION                                                ")
    _sb.AppendLine("   SELECT   CHS_ISO_CODE AS ISO_CODE                 ")
    _sb.AppendLine("          , CH_DENOMINATION AS DENOMINATION          ")
    _sb.AppendLine("     FROM   CHIPS_SETS                               ")
    _sb.AppendLine("LEFT   JOIN CHIPS_SETS_CHIPS ON CSC_SET_ID = CHS_CHIP_SET_ID ")
    _sb.AppendLine("LEFT   JOIN CHIPS ON CSC_CHIP_ID = CH_CHIP_ID ")
    _sb.AppendLine("    WHERE   CH_DENOMINATION > 0                      ")
    _sb.AppendLine("      AND   CHS_ALLOWED = 1                          ")
    _sb.AppendLine("      AND   CH_ALLOWED = 1                           ")
    _sb.AppendLine(" ORDER BY   ISO_CODE, DENOMINATION DESC              ")

    Using _sql_trx As New DB_TRX()
      Using _sql_cmd As New SqlCommand(_sb.ToString(), _sql_trx.SqlTransaction.Connection, _sql_trx.SqlTransaction)

        Using _sql_da As New SqlDataAdapter(_sql_cmd)
          _sql_da.Fill(m_table_denominations)
        End Using
      End Using
    End Using

  End Sub

  Private Function GetTerminalCageMovementInfo(ByVal SoftCountType As ENUM_SOFTCOUNT_TYPE) As DataTable

    Dim _sb As StringBuilder
    Dim _table As DataTable

    _sb = New StringBuilder()
    _table = New DataTable()

    _sb.AppendLine("    SELECT   TOP 1                                                      ")
    _sb.AppendLine("             MC_DATETIME                                                ")
    _sb.AppendLine("           , MC_EXTRACTION_DATETIME                                     ")
    _sb.AppendLine("           , TE_NAME                                                    ")
    _sb.AppendLine("      FROM   MONEY_COLLECTIONS                                          ")
    _sb.AppendLine("INNER JOIN   TERMINALS ON MC_TERMINAL_ID = TE_TERMINAL_ID               ")
    _sb.AppendLine("     WHERE   (MC_STACKER_ID = @pStackerId                               ")
    _sb.AppendLine("        OR   TE_FLOOR_ID = @pFloorId)                                   ")
    _sb.AppendLine("       AND   MC_STATUS =  @pStatus                                      ")

    Using _sql_trx As New DB_TRX()
      Using _sql_cmd As New SqlCommand(_sb.ToString(), _sql_trx.SqlTransaction.Connection, _sql_trx.SqlTransaction)
        _sql_cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = TITO_MONEY_COLLECTION_STATUS.PENDING
        _sql_cmd.Parameters.Add("@pStackerId", SqlDbType.BigInt).Value = m_stacker_id
        _sql_cmd.Parameters.Add("@pFloorId", SqlDbType.NVarChar).Value = m_floor_id
        _sql_cmd.Parameters.Add("@pSoftCountType", SqlDbType.Int).Value = SoftCountType

        Using _sql_da As New SqlDataAdapter(_sql_cmd)
          _sql_da.Fill(_table)
        End Using
      End Using
    End Using

    Return _table
  End Function

  Public Function GetTicketData(ByVal TicketNumber As String, Optional ByVal TicketId As Int64 = 0) As DataTable
    Dim _sb As StringBuilder
    Dim _ticket_number As Int64
    Dim _table_result As DataTable

    _table_result = New DataTable()

    If TicketNumber = "" Or Not Int64.TryParse(TicketNumber, _ticket_number) Then
      Return _table_result
    End If

    _sb = New StringBuilder()

    _sb.AppendLine("   SELECT   TI_TICKET_ID                                    ")
    _sb.AppendLine("          , TI_AMOUNT                                       ")
    _sb.AppendLine("   FROM     TICKETS                                         ")
    _sb.AppendLine("   WHERE    TI_VALIDATION_NUMBER = @pTicketNumber           ")
    _sb.AppendLine("      AND   TI_COLLECTED_MONEY_COLLECTION IS NULL           ")
    _sb.AppendLine("      AND   TI_TYPE_ID <> @pChipsDealerCopy                 ")

    Try
      Using _db_trx As New DB_TRX()
        Using _sql_cmd As New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          _sql_cmd.Parameters.Add("@pTicketNumber", SqlDbType.BigInt).Value = TicketNumber
          _sql_cmd.Parameters.Add("@pChipsDealerCopy", SqlDbType.Int).Value = TITO_TICKET_TYPE.CHIPS_DEALER_COPY
          Using _sql_da As New SqlDataAdapter(_sql_cmd)
            _db_trx.Fill(_sql_da, _table_result)

          End Using

        End Using
      End Using
    Catch ex As Exception
      Return _table_result
    End Try

    Return _table_result

  End Function

#End Region

End Class
