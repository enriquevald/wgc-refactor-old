'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_jackpot_params.vb
' DESCRIPTION:   Jackpot Parameters class
' AUTHOR:        Agust� Poch
' CREATION DATE: 29-NOV-2002
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 29-NOV-2002  APB    Initial version
' 09-JAN-2003  SCM    Added functionalities
' 07-OCT-2005  JMP    New configuration in Jackpot_parameters table
' 21-NOV-2016  GMV    Product Backlog Item 19930:Jackpots - GUI - Show on WinUp setting
'--------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient

Public Class CLASS_JACKPOT_PARAMS
  Inherits CLASS_BASE

#Region " Constants "

#End Region

#Region "Structure"

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_JACKPOT_PARAMS
    Public enabled As Integer
    Public contribution_pct As Double
    Public awarding_start As Integer
    Public awarding_end As Integer
    Public awarding_days As Integer
    Public compensation_pct As Double
    Public jackpot_instances As New List(Of TYPE_JACKPOT_INSTANCE)
    Public block_mode As Integer
    Public block_interval As Integer
    Public animation_interval As Integer
    Public recent_jackpot_interval As Integer
    Public promo_message As New List(Of String)
    Public min_award_amount As Double

  End Class

#Region "Jackpot Instance"

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_JACKPOT_INSTANCE
    Public index As Integer
    Public name As String
    Public contribution_pct As Double
    Public min_bet_amount As Double
    Public min_amount As Double
    Public max_amount As Double
    Public average As Double
    Public show_on_winup As Boolean 'GMV 2016-11-21 Show on mobile app
  End Class

#End Region

#End Region

#Region "Constructor / Destructor"

  Public Sub New()
    Call MyBase.New()
  End Sub

  Protected Overrides Sub Finalize()
    MyBase.Finalize()

  End Sub

#End Region

#Region " Members "

  Protected m_jackpot_params As New TYPE_JACKPOT_PARAMS

#End Region 'Members

#Region "Properties"

  Public Property Enabled() As Boolean
    Get
      Return m_jackpot_params.enabled
    End Get

    Set(ByVal Value As Boolean)
      m_jackpot_params.enabled = Value
    End Set

  End Property

  Public Property WrkStart() As Integer
    Get
      Return m_jackpot_params.awarding_start
    End Get

    Set(ByVal value As Integer)
      m_jackpot_params.awarding_start = value
    End Set

  End Property

  Public Property WrkEnd() As Integer
    Get
      Return m_jackpot_params.awarding_end
    End Get

    Set(ByVal value As Integer)
      m_jackpot_params.awarding_end = value
    End Set

  End Property

  Public Property ContributionPct() As Double
    Get
      Return m_jackpot_params.contribution_pct
    End Get

    Set(ByVal value As Double)
      m_jackpot_params.contribution_pct = value
    End Set

  End Property

  Public Property CompensationPct() As Double
    Get
      Return m_jackpot_params.compensation_pct
    End Get

    Set(ByVal value As Double)
      m_jackpot_params.compensation_pct = value
    End Set

  End Property

  Public Property JackpotInstances() As List(Of TYPE_JACKPOT_INSTANCE)
    Get
      Return m_jackpot_params.jackpot_instances
    End Get

    Set(ByVal value As List(Of TYPE_JACKPOT_INSTANCE))
      m_jackpot_params.jackpot_instances = value
    End Set

  End Property

  Public Property WorkingDays() As Integer
    Get
      Return m_jackpot_params.awarding_days
    End Get
    Set(ByVal value As Integer)
      m_jackpot_params.awarding_days = value
    End Set
  End Property

  Public ReadOnly Property WorkingDaysText() As String
    Get
      Dim str_binary As String
      Dim bit_array As Char()
      Dim str_days As String

      str_binary = Convert.ToString(m_jackpot_params.awarding_days, 2)
      str_binary = New String("0", 7 - str_binary.Length) + str_binary

      bit_array = str_binary.ToCharArray()

      str_days = ""
      If (bit_array(6) = "1") Then str_days = str_days & " " & GLB_NLS_GUI_JACKPOT_MGR.GetString(304) '"Sunday "
      If (bit_array(5) = "1") Then str_days = str_days & " " & GLB_NLS_GUI_JACKPOT_MGR.GetString(298) '"Monday "
      If (bit_array(4) = "1") Then str_days = str_days & " " & GLB_NLS_GUI_JACKPOT_MGR.GetString(299) '"Tuesday "
      If (bit_array(3) = "1") Then str_days = str_days & " " & GLB_NLS_GUI_JACKPOT_MGR.GetString(300) '"Wednesday "
      If (bit_array(2) = "1") Then str_days = str_days & " " & GLB_NLS_GUI_JACKPOT_MGR.GetString(301) '"Thursday "
      If (bit_array(1) = "1") Then str_days = str_days & " " & GLB_NLS_GUI_JACKPOT_MGR.GetString(302) '"Friday "
      If (bit_array(0) = "1") Then str_days = str_days & " " & GLB_NLS_GUI_JACKPOT_MGR.GetString(303) '"Saturday "

      Return str_days.TrimEnd
    End Get
  End Property



  Public Property BlockMode() As Integer
    Get
      Return m_jackpot_params.block_mode
    End Get

    Set(ByVal value As Integer)
      m_jackpot_params.block_mode = value
    End Set

  End Property

  Public Property BlockInterval() As Integer
    Get
      Return m_jackpot_params.block_interval
    End Get

    Set(ByVal value As Integer)
      m_jackpot_params.block_interval = value
    End Set

  End Property

  Public Property AnimInterval() As Integer
    Get
      Return m_jackpot_params.animation_interval
    End Get

    Set(ByVal value As Integer)
      m_jackpot_params.animation_interval = value
    End Set

  End Property

  Public Property RecentInterval() As Integer
    Get
      Return m_jackpot_params.recent_jackpot_interval

    End Get

    Set(ByVal value As Integer)
      m_jackpot_params.recent_jackpot_interval = value
    End Set

  End Property

  Public Property PromoMess(ByVal idx As Integer) As String
    Get
      Return m_jackpot_params.promo_message(idx)

    End Get

    Set(ByVal value As String)
      m_jackpot_params.promo_message(idx) = value
    End Set

  End Property

  Public Property MinAwardAmount() As String
    Get
      Return m_jackpot_params.min_award_amount

    End Get

    Set(ByVal value As String)
      m_jackpot_params.min_award_amount = value
    End Set

  End Property


#End Region

#Region " Overrides functions "

  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As ENUM_STATUS

    Dim rc As Integer

    ' Read Jackpot parameters information
    rc = JackpotConf_DbRead(m_jackpot_params, SqlCtx)

    Select Case rc
      Case ENUM_DB_RC.DB_RC_OK
        Return ENUM_STATUS.STATUS_OK

      Case ENUM_DB_RC.DB_RC_ERROR_NOT_FOUND
        Return CLASS_BASE.ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function   'DB_Read

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim rc As Integer

    ' Read Jackpot parameters information
    rc = JackpotConf_DbUpdate(m_jackpot_params, SqlCtx)

    Select Case rc
      Case ENUM_DB_RC.DB_RC_OK
        Return ENUM_STATUS.STATUS_OK

      Case ENUM_DB_RC.DB_RC_ERROR_NOT_FOUND
        Return CLASS_BASE.ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select
  End Function 'DB_Update

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
  End Function 'DB_Insert

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
  End Function 'DB_Delete

  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim auditor_data As CLASS_AUDITOR_DATA
    Dim str_aux As String
    Dim str_aux2 As String
    Dim seconds As Integer
    Dim hours As Integer
    Dim minutes As Integer
    Dim days As Integer
    Dim date_time As Date
    Dim idx As Integer
    Dim base_field_name As String
    Dim aux_field_name As String

    auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_JACKPOT_PARAMETERS)

    auditor_data.SetName(GLB_NLS_GUI_JACKPOT_MGR.Id(202), "")

    If Me.Enabled = True Then
      auditor_data.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(218), GLB_NLS_GUI_JACKPOT_MGR.GetString(218))
    Else
      auditor_data.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(218), GLB_NLS_GUI_JACKPOT_MGR.GetString(250))
    End If

    date_time = GUI_GetDateTime()
    Call Format_SecondsToDdHhMmSs(Me.WrkStart, days, hours, minutes, seconds)
    str_aux2 = GUI_FormatTime(New DateTime(date_time.Year, date_time.Month, date_time.Day, hours, minutes, seconds), ModuleDateTimeFormats.ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    str_aux = GLB_NLS_GUI_JACKPOT_MGR.GetString(230) & "." & GLB_NLS_GUI_JACKPOT_MGR.GetString(204)
    auditor_data.SetField(0, str_aux2, str_aux)

    date_time = GUI_GetDateTime()
    Call Format_SecondsToDdHhMmSs(Me.WrkEnd, days, hours, minutes, seconds)
    str_aux2 = GUI_FormatTime(New DateTime(date_time.Year, date_time.Month, date_time.Day, hours, minutes, seconds), ModuleDateTimeFormats.ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    str_aux = GLB_NLS_GUI_JACKPOT_MGR.GetString(230) & "." & GLB_NLS_GUI_JACKPOT_MGR.GetString(205)
    auditor_data.SetField(0, str_aux2, str_aux)

    ' Contribution
    auditor_data.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(279), GUI_FormatNumber(Me.ContributionPct, 2))

    '' Compensation
    'auditor_data.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(281), GUI_FormatNumber(Me.CompensationPct))

    ' Working Days
    auditor_data.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(296), WorkingDaysText())

    For idx = 0 To Me.JackpotInstances.Count - 1

      base_field_name = GLB_NLS_GUI_JACKPOT_MGR.GetString(305, CStr(Me.JackpotInstances(idx).index))

      ' Instance Name
      aux_field_name = base_field_name & "." & GLB_NLS_GUI_JACKPOT_MGR.GetString(282)
      Call auditor_data.SetField(0, Me.JackpotInstances(idx).name, aux_field_name)

      ' Instance Contribution
      aux_field_name = base_field_name & "." & GLB_NLS_GUI_JACKPOT_MGR.GetString(283)
      Call auditor_data.SetField(0, Me.JackpotInstances(idx).contribution_pct, aux_field_name)

      ' Instance Minimum Bet
      aux_field_name = base_field_name & "." & GLB_NLS_GUI_JACKPOT_MGR.GetString(284)
      Call auditor_data.SetField(0, Me.JackpotInstances(idx).min_bet_amount, aux_field_name)

      ' Instance Minimum
      aux_field_name = base_field_name & "." & GLB_NLS_GUI_JACKPOT_MGR.GetString(285)
      Call auditor_data.SetField(0, Me.JackpotInstances(idx).min_amount, aux_field_name)

      ' Instance Maximum
      aux_field_name = base_field_name & "." & GLB_NLS_GUI_JACKPOT_MGR.GetString(286)
      Call auditor_data.SetField(0, Me.JackpotInstances(idx).max_amount, aux_field_name)

      ' Instance Average
      aux_field_name = base_field_name & "." & GLB_NLS_GUI_JACKPOT_MGR.GetString(287)
      Call auditor_data.SetField(0, Me.JackpotInstances(idx).average, aux_field_name)

      ' Show on Winup
      aux_field_name = base_field_name & "." & GLB_NLS_GUI_JACKPOT_MGR.GetString(499)
      Call auditor_data.SetField(0, Me.JackpotInstances(idx).show_on_winup, aux_field_name)

    Next

    ' Jackpot award parameters
    ' 319 "Duraci�n de la animaci�n del Jackpot"
    ' 320 "Todos los terminales anuncian el �ltimo Jackpot durante"

    auditor_data.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(317), BlockMode())
    auditor_data.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(318), BlockInterval())
    auditor_data.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(319), AnimInterval())
    auditor_data.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(320), RecentInterval())
    auditor_data.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(327), PromoMess(0))
    auditor_data.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(328), PromoMess(1))
    auditor_data.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(329), PromoMess(2))
    auditor_data.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(326), MinAwardAmount())

    Return auditor_data

  End Function 'AuditorData

  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim temp_jackpot As CLASS_JACKPOT_PARAMS
    Dim idx As Integer
    Dim item As TYPE_JACKPOT_INSTANCE

    temp_jackpot = New CLASS_JACKPOT_PARAMS

    temp_jackpot.m_jackpot_params.compensation_pct = Me.m_jackpot_params.compensation_pct
    temp_jackpot.m_jackpot_params.contribution_pct = Me.m_jackpot_params.contribution_pct
    temp_jackpot.m_jackpot_params.enabled = Me.m_jackpot_params.enabled
    temp_jackpot.m_jackpot_params.awarding_end = Me.m_jackpot_params.awarding_end
    temp_jackpot.m_jackpot_params.awarding_start = Me.m_jackpot_params.awarding_start
    temp_jackpot.m_jackpot_params.awarding_days = Me.m_jackpot_params.awarding_days

    temp_jackpot.m_jackpot_params.jackpot_instances = New List(Of TYPE_JACKPOT_INSTANCE)

    For idx = 0 To Me.m_jackpot_params.jackpot_instances.Count - 1

      item = New TYPE_JACKPOT_INSTANCE
      item.contribution_pct = Me.m_jackpot_params.jackpot_instances(idx).contribution_pct
      item.average = Me.m_jackpot_params.jackpot_instances(idx).average
      item.index = Me.m_jackpot_params.jackpot_instances(idx).index
      item.max_amount = Me.m_jackpot_params.jackpot_instances(idx).max_amount
      item.min_amount = Me.m_jackpot_params.jackpot_instances(idx).min_amount
      item.min_bet_amount = Me.m_jackpot_params.jackpot_instances(idx).min_bet_amount
      item.name = Me.m_jackpot_params.jackpot_instances(idx).name
      item.show_on_winup = Me.m_jackpot_params.jackpot_instances(idx).show_on_winup


      temp_jackpot.m_jackpot_params.jackpot_instances.Add(item)
    Next


    temp_jackpot.m_jackpot_params.block_mode = Me.m_jackpot_params.block_mode
    temp_jackpot.m_jackpot_params.block_interval = Me.m_jackpot_params.block_interval
    temp_jackpot.m_jackpot_params.animation_interval = Me.m_jackpot_params.animation_interval
    temp_jackpot.m_jackpot_params.recent_jackpot_interval = Me.m_jackpot_params.recent_jackpot_interval

    temp_jackpot.m_jackpot_params.promo_message = New List(Of String)

    temp_jackpot.m_jackpot_params.promo_message.Add(Me.m_jackpot_params.promo_message(0))
    temp_jackpot.m_jackpot_params.promo_message.Add(Me.m_jackpot_params.promo_message(1))
    temp_jackpot.m_jackpot_params.promo_message.Add(Me.m_jackpot_params.promo_message(2))
    temp_jackpot.m_jackpot_params.min_award_amount = Me.m_jackpot_params.min_award_amount

    Return temp_jackpot

  End Function 'Duplicate 

#End Region 'Overrides functions

#Region "DB Functions"

  Private Function JackpotConf_DbRead(ByVal JackpotParams As TYPE_JACKPOT_PARAMS, _
                                      ByVal Context As Integer) As Integer

    Dim str_sql As String
    Dim data_table_parameters As DataTable
    Dim data_table_instances As DataTable
    Dim idx As Integer

    str_sql = "SELECT  C2JP_ENABLED " _
                  & ", C2JP_CONTRIBUTION_PCT " _
                  & ", C2JP_AWARDING_START " _
                  & ", C2JP_AWARDING_END " _
                  & ", C2JP_COMPENSATION_PCT " _
                  & ", C2JP_AWARDING_DAYS " _
                  & ", C2JP_BLOCK_MODE " _
                  & ", C2JP_BLOCK_INTERVAL " _
                  & ", C2JP_ANIMATION_INTERVAL " _
                  & ", C2JP_RECENT_INTERVAL " _
                  & ", C2JP_PROMO_MESSAGE1 " _
                  & ", C2JP_PROMO_MESSAGE2 " _
                  & ", C2JP_PROMO_MESSAGE3 " _
                  & ", C2JP_BLOCK_MIN_AMOUNT " _
            & " FROM  C2_JACKPOT_PARAMETERS "

    data_table_parameters = GUI_GetTableUsingSQL(str_sql, 5000)

    str_sql = "SELECT C2JI_INDEX " _
              & ", C2JI_NAME " _
              & ", C2JI_CONTRIBUTION_PCT " _
              & ", C2JI_MINIMUM_BET " _
              & ", C2JI_MINIMUM " _
              & ", C2JI_MAXIMUM " _
              & ", C2JI_AVERAGE "
    If WSI.Common.GeneralParam.GetBoolean("Features", "WinUP", False) Then
      str_sql = str_sql & ", C2JI_SHOW_ON_WINUP "
    End If

      str_sql = str_sql & "FROM C2_JACKPOT_INSTANCES "

    data_table_instances = GUI_GetTableUsingSQL(str_sql, 5000)

    If data_table_parameters.Rows.Count() < 1 Or data_table_instances.Rows.Count() < 1 Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    If IsNothing(data_table_parameters) Or IsNothing(data_table_instances) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    JackpotParams.enabled = data_table_parameters.Rows(0).Item("C2JP_ENABLED")
    JackpotParams.contribution_pct = data_table_parameters.Rows(0).Item("C2JP_CONTRIBUTION_PCT")
    JackpotParams.compensation_pct = data_table_parameters.Rows(0).Item("C2JP_COMPENSATION_PCT")
    JackpotParams.awarding_start = data_table_parameters.Rows(0).Item("C2JP_AWARDING_START")
    JackpotParams.awarding_end = data_table_parameters.Rows(0).Item("C2JP_AWARDING_END")
    JackpotParams.awarding_days = data_table_parameters.Rows(0).Item("C2JP_AWARDING_DAYS")
    JackpotParams.block_mode = data_table_parameters.Rows(0).Item("C2JP_BLOCK_MODE")
    JackpotParams.block_interval = data_table_parameters.Rows(0).Item("C2JP_BLOCK_INTERVAL")
    JackpotParams.animation_interval = data_table_parameters.Rows(0).Item("C2JP_ANIMATION_INTERVAL")
    JackpotParams.recent_jackpot_interval = data_table_parameters.Rows(0).Item("C2JP_RECENT_INTERVAL")

    If Not IsDBNull(data_table_parameters.Rows(0).Item("C2JP_PROMO_MESSAGE1")) Then
      JackpotParams.promo_message.Add(data_table_parameters.Rows(0).Item("C2JP_PROMO_MESSAGE1"))
    Else
      JackpotParams.promo_message.Add("")
    End If
    If Not IsDBNull(data_table_parameters.Rows(0).Item("C2JP_PROMO_MESSAGE2")) Then
      JackpotParams.promo_message.Add(data_table_parameters.Rows(0).Item("C2JP_PROMO_MESSAGE2"))
    Else
      JackpotParams.promo_message.Add("")
    End If
    If Not IsDBNull(data_table_parameters.Rows(0).Item("C2JP_PROMO_MESSAGE3")) Then
      JackpotParams.promo_message.Add(data_table_parameters.Rows(0).Item("C2JP_PROMO_MESSAGE3"))
    Else
      JackpotParams.promo_message.Add("")
    End If
    JackpotParams.min_award_amount = data_table_parameters.Rows(0).Item("C2JP_BLOCK_MIN_AMOUNT")

    For idx = 0 To data_table_instances.Rows.Count() - 1
      Dim jackpot_inst As New TYPE_JACKPOT_INSTANCE

      jackpot_inst.index = data_table_instances.Rows(idx).Item("C2JI_INDEX")
      jackpot_inst.name = data_table_instances.Rows(idx).Item("C2JI_NAME")
      jackpot_inst.contribution_pct = data_table_instances.Rows(idx).Item("C2JI_CONTRIBUTION_PCT")
      jackpot_inst.min_bet_amount = data_table_instances.Rows(idx).Item("C2JI_MINIMUM_BET")
      jackpot_inst.min_amount = data_table_instances.Rows(idx).Item("C2JI_MINIMUM")
      jackpot_inst.max_amount = data_table_instances.Rows(idx).Item("C2JI_MAXIMUM")
      jackpot_inst.average = data_table_instances.Rows(idx).Item("C2JI_AVERAGE")
      If WSI.Common.GeneralParam.GetBoolean("Features", "WinUP", False) Then
        If data_table_instances.Rows(idx).Item("C2JI_SHOW_ON_WINUP") IsNot DBNull.Value Then
          jackpot_inst.show_on_winup = data_table_instances.Rows(idx).Item("C2JI_SHOW_ON_WINUP")
        End If
      End If
      
      JackpotParams.jackpot_instances.Add(jackpot_inst)

    Next

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

  End Function

  Private Function JackpotConf_DbUpdate(ByVal JackpotParams As TYPE_JACKPOT_PARAMS, _
                                        ByVal Context As Integer) As Integer

    Dim str_sql As String
    Dim idx As Integer

    Dim SqlTrans As System.Data.SqlClient.SqlTransaction = Nothing
    Dim commit_trx As Boolean

    Dim sql_command As SqlCommand
    Dim rc As Integer
    Dim result As Integer

    Try

      If Not GUI_BeginSQLTransaction(SqlTrans) Then
        Exit Function
      End If

      commit_trx = False

      ' UPDATE JACKPOT PARAMETERS
      str_sql = "UPDATE   C2_JACKPOT_PARAMETERS " & _
                   "SET   C2JP_CONTRIBUTION_PCT = @p1" & _
                   " ,    C2JP_AWARDING_START = @p2" & _
                   " ,    C2JP_AWARDING_END = @p3" & _
                   " ,    C2JP_COMPENSATION_PCT = @p4" & _
                   " ,    C2JP_ENABLED = @p5" & _
                   " ,    C2JP_AWARDING_DAYS = @p6" & _
                   " ,    C2JP_BLOCK_MODE = @p7" & _
                   " ,    C2JP_BLOCK_INTERVAL = @p8" & _
                   " ,    C2JP_ANIMATION_INTERVAL = @p9" & _
                   " ,    C2JP_RECENT_INTERVAL = @p10" & _
                   " ,    C2JP_PROMO_MESSAGE1 = @p11" & _
                   " ,    C2JP_PROMO_MESSAGE2 = @p12" & _
                   " ,    C2JP_PROMO_MESSAGE3 = @p13" & _
                   " ,    C2JP_BLOCK_MIN_AMOUNT = @p14"

      sql_command = New SqlCommand(str_sql)
      sql_command.Connection = SqlTrans.Connection
      sql_command.Transaction = SqlTrans

      sql_command.Parameters.Add("@p1", SqlDbType.Float, 1).Value = JackpotParams.contribution_pct
      sql_command.Parameters.Add("@p2", SqlDbType.Int, 1).Value = JackpotParams.awarding_start
      sql_command.Parameters.Add("@p3", SqlDbType.Int, 1).Value = JackpotParams.awarding_end
      sql_command.Parameters.Add("@p4", SqlDbType.Float, 1).Value = JackpotParams.compensation_pct
      sql_command.Parameters.Add("@p5", SqlDbType.Int, 1).Value = JackpotParams.enabled
      sql_command.Parameters.Add("@p6", SqlDbType.Int, 1).Value = JackpotParams.awarding_days
      sql_command.Parameters.Add("@p7", SqlDbType.Int, 1).Value = JackpotParams.block_mode
      sql_command.Parameters.Add("@p8", SqlDbType.Int, 1).Value = JackpotParams.block_interval
      sql_command.Parameters.Add("@p9", SqlDbType.Int, 1).Value = JackpotParams.animation_interval
      sql_command.Parameters.Add("@p10", SqlDbType.Int, 1).Value = JackpotParams.recent_jackpot_interval
      sql_command.Parameters.Add("@p11", SqlDbType.VarChar, 255).Value = JackpotParams.promo_message(0)
      sql_command.Parameters.Add("@p12", SqlDbType.VarChar, 255).Value = JackpotParams.promo_message(1)
      sql_command.Parameters.Add("@p13", SqlDbType.VarChar, 255).Value = JackpotParams.promo_message(2)
      sql_command.Parameters.Add("@p14", SqlDbType.Money, 1).Value = JackpotParams.min_award_amount

      rc = sql_command.ExecuteNonQuery()

      If rc <> 1 Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      ' UPDATE JACKPOT INSTANCES
      str_sql = "UPDATE   C2_JACKPOT_INSTANCES " & _
                "   SET   C2JI_NAME = @p1" & _
                " ,       C2JI_CONTRIBUTION_PCT = @p2" & _
                " ,       C2JI_MINIMUM_BET = @p3" & _
                " ,       C2JI_MINIMUM = @p4" & _
                " ,       C2JI_MAXIMUM = @p5" & _
                " ,       C2JI_AVERAGE = @p6" & _
                " ,       C2JI_SHOW_ON_WINUP    = @p8" & _
                " WHERE   C2JI_INDEX = @p7"

      For idx = 0 To JackpotParams.jackpot_instances.Count - 1
        sql_command = New SqlCommand(str_sql)
        sql_command.Connection = SqlTrans.Connection
        sql_command.Transaction = SqlTrans

        sql_command.Parameters.Add("@p1", SqlDbType.NVarChar, 20).Value = JackpotParams.jackpot_instances(idx).name
        sql_command.Parameters.Add("@p2", SqlDbType.Float, 1).Value = JackpotParams.jackpot_instances(idx).contribution_pct
        sql_command.Parameters.Add("@p3", SqlDbType.Float, 1).Value = JackpotParams.jackpot_instances(idx).min_bet_amount
        sql_command.Parameters.Add("@p4", SqlDbType.Float, 1).Value = JackpotParams.jackpot_instances(idx).min_amount
        sql_command.Parameters.Add("@p5", SqlDbType.Float, 1).Value = JackpotParams.jackpot_instances(idx).max_amount
        sql_command.Parameters.Add("@p6", SqlDbType.Float, 1).Value = JackpotParams.jackpot_instances(idx).average
        sql_command.Parameters.Add("@p7", SqlDbType.Int, 1).Value = JackpotParams.jackpot_instances(idx).index
        sql_command.Parameters.Add("@p8", SqlDbType.Bit).Value = JackpotParams.jackpot_instances(idx).show_on_winup

        rc = sql_command.ExecuteNonQuery()

        If rc <> 1 Then
          Exit For
        End If

      Next

      If rc = 1 Then
        commit_trx = True
      End If

    Catch ex As Exception
      ' Do nothing
      Debug.WriteLine(ex.Message)

    Finally
      ' Either commit or rollback
      If Not (SqlTrans.Connection Is Nothing) Then
        If commit_trx Then
          SqlTrans.Commit()
          result = ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Else
          SqlTrans.Rollback()
          result = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If
      End If

      SqlTrans.Dispose()
      SqlTrans = Nothing

    End Try

    Return result

  End Function ' JackpotConf_DbUpdate

#End Region

End Class
