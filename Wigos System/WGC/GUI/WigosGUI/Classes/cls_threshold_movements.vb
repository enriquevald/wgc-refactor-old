﻿'-------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_ threshold_movements.vb
' DESCRIPTION:   Threshold Class
' AUTHOR:        Alberto Marcos
' CREATION DATE: 16-NOV-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 16-NOV-2017  AMF    Initial Version
' 27-NOV-2017  MS     [WIGOS-6840] Document type data instead of "player identification number" data in "Threshold movements report" results grid
'--------------------------------------------------------------------
Imports GUI_CommonOperations
Imports System.Runtime.InteropServices
Imports System.Xml
Imports WSI.Common
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Text
Imports System.Data.SqlClient
Imports WSI.Common.CreditLines
Imports GUI_Controls.frm_base_sel

Public Class CLASS_THRESHOLD_MOVEMENTS

#Region " Public Methods "

  ''' <summary>
  ''' Get list currencies
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function getListCurrencies() As Dictionary(Of Integer, String)

    Dim _dic_status As Dictionary(Of Integer, String)

    _dic_status = New Dictionary(Of Integer, String)

    SetSType(_dic_status, CurrencyExchangeType.CURRENCY)
    SetSType(_dic_status, CurrencyExchangeType.CARD)
    SetSType(_dic_status, CurrencyExchangeType.CHECK)

    Return _dic_status

  End Function ' getListCurrencies

  ''' <summary>
  ''' Get Threshold movements
  ''' </summary>
  ''' <param name="DateFrom"></param>
  ''' <param name="DateTo"></param>
  ''' <param name="Types"></param>
  ''' <param name="User"></param>
  ''' <param name="HolderName"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function getThresholdMovements(ByVal DateFrom As uc_date_picker, ByVal DateTo As uc_date_picker, ByVal Types As uc_checked_list, ByRef User As String, ByRef HolderName As String) As String

    Dim _sql_str As New StringBuilder

    _sql_str.AppendLine("   SELECT   TO_THRESHOLD_ID                                      ")
    _sql_str.AppendLine("          , TO_TRANSACTION_DATE                                  ")
    _sql_str.AppendLine("          , TO_TRANSACTION_TYPE                                  ")
    _sql_str.AppendLine("          , TO_TRANSACTION_SUBTYPE                               ")
    _sql_str.AppendLine("          , TO_CASH_AMOUNT                                       ")
    _sql_str.AppendLine("          , TO_TOTAL_AMOUNT                                      ")
    _sql_str.AppendLine("          , TO_USER_ID                                           ")
    _sql_str.AppendLine("          , TO_USER_NAME                                         ")
    _sql_str.AppendLine("          , TO_HOLDER_NAME                                       ")
    _sql_str.AppendLine("          , TO_HOLDER_ADDRESS_01                                 ")
    _sql_str.AppendLine("          , TO_HOLDER_ADDRESS_02                                 ")
    _sql_str.AppendLine("          , TO_HOLDER_ADDRESS_03                                 ")
    _sql_str.AppendLine("          , TO_HOLDER_EXT_NUM                                    ")
    _sql_str.AppendLine("          , TO_HOLDER_CITY                                       ")
    _sql_str.AppendLine("          , TO_HOLDER_FED_ENTITY                                 ")
    _sql_str.AppendLine("          , TO_HOLDER_ZIP                                        ")
    _sql_str.AppendLine("          , TO_HOLDER_ADDRESS_COUNTRY                            ")
    _sql_str.AppendLine("          , TO_HOLDER_NATIONALITY                                ")
    _sql_str.AppendLine("          , TO_HOLDER_BIRTH_DATE                                 ")
    _sql_str.AppendLine("          , TO_HOLDER_OCCUPATION_ID                              ")
    _sql_str.AppendLine("          , TO_HOLDER_ID_TYPE                                    ")
    _sql_str.AppendLine("          , TO_HOLDER_ID                                         ")
    _sql_str.AppendLine("          , TO_ACCOUNT_ID                                        ")
    _sql_str.AppendLine("          , (SELECT   GP_KEY_VALUE                               ")
    _sql_str.AppendLine("               FROM   GENERAL_PARAMS                             ")
    _sql_str.AppendLine("              WHERE   GP_GROUP_KEY   = 'Site'                    ")
    _sql_str.AppendLine("                AND   GP_SUBJECT_KEY = 'Identifier') AS SITE_ID  ")
    _sql_str.AppendLine("          , AC_HOLDER_NAME                                       ")
    _sql_str.AppendLine("     FROM   THRESHOLD_OPERATIONS                                 ")
    _sql_str.AppendLine("INNER JOIN  ACCOUNTS ON TO_ACCOUNT_ID = AC_ACCOUNT_ID            ")
    _sql_str.AppendLine("    WHERE   1 = 1                                                ")

    If DateFrom.Checked Then
      _sql_str.AppendLine(String.Format("      AND   TO_TRANSACTION_DATE >= {0}                           ", GUI_FormatDateDB(DateFrom.Value)))
    End If

    If DateTo.Checked Then
      _sql_str.AppendLine(String.Format("      AND   TO_TRANSACTION_DATE <  {0}                           ", GUI_FormatDateDB(DateTo.Value)))
    End If

    If Not Types.IsNoneSelected Then
      _sql_str.AppendLine(String.Format("      AND   TO_TRANSACTION_TYPE IN ({0})                         ", Types.SelectedIndexesList()))
    End If

    If Not String.IsNullOrEmpty(User) Then
      _sql_str.AppendLine(String.Format("      AND   TO_USER_ID = '{0}'                                   ", User))
    End If

    If Not String.IsNullOrEmpty(HolderName) Then
      _sql_str.AppendLine(String.Format("      AND   {0} ", GUI_FilterField("TO_HOLDER_NAME", HolderName, False, False, True)))
    End If

    _sql_str.AppendLine(" ORDER BY   TO_TRANSACTION_DATE DESC, TO_HOLDER_NAME              ")

    Return _sql_str.ToString

  End Function ' getThresholdMovements

#End Region ' Public Methods

#Region " Private Functions "

  ''' <summary>
  ''' Set type
  ''' </summary>
  ''' <param name="DicStatus"></param>
  ''' <param name="CurrencyType"></param>
  ''' <remarks></remarks>
  Private Sub SetSType(ByRef DicStatus As Dictionary(Of Integer, String), ByVal CurrencyType As CurrencyExchangeType)
    DicStatus.Add(CurrencyType, CurrencyExchange.GetDescriptionType(CurrencyType))
  End Sub ' SetSType

#End Region ' Private Functions

End Class