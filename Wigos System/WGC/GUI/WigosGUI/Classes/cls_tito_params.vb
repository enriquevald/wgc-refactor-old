'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : cls_tito_params.vb
'
' DESCRIPTION : Configure tito parameters
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 03-JUL-2013  FBA    Initial version
' 25-SEP-2013  NMR    Renamed general TITO Params for Redeem control
' 14-OCT-2013  DRV    Fixed Auditor Data, and read and update errors
' 17-OCT-2013  DRV    Changed the way that reads and update emission an redemption ticket parameters
' 25-OCT-2013  DRV    Deleted ticket configuration for different tickets and make it unique for all tickets 
' 12-NOV-2013  ACM    Some GP names has changed
' 07-JAN-2014  ICS    Added the stacker capacity property
' 10-FEB-2014  ICS    Added the jackpot limit before prize property
' 13-AUG-2015  ETP    Fixed Bug-3711 Maximum Ticket in, ticked out, and prizes has been defined as Decimal for avoid round
' 17-OCT-2016  PDM    PBI 18245:eBox - Ticket Amount Not Multiple of Machine Denomination: GP - monto m�nimo aceptado por la m�quina (GUI y BD)
' 26-OCT-2016  FAV    PBI 19597:eBox - Ticket Amount Not Multiple of Machine Denomination: Review
'--------------------------------------------------------------------
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports System.Text
Imports WSI.Common

Public Class CLASS_TITO_PARAMS
  Inherits CLASS_BASE

#Region " Members "

  Dim m_min_allowed_ticket_in As Decimal
  Dim m_max_allowed_Reedem As Decimal
  Dim m_max_allowed_creation As Decimal
  Dim m_detail_collection_level As Integer
  Dim m_ticket_collection As Boolean
  Dim m_stacker_capacity As Integer
  Dim m_allow_truncate As Boolean
  Dim m_min_denomination_multiple As Decimal
  'ticket personalization
  Dim m_ticket_location As String
  Dim m_ticket_address_1 As String
  Dim m_ticket_address_2 As String
  Dim m_ticket_promo_redim_title As String
  Dim m_ticket_promo_non_redim_title As String
  Dim m_ticket_cashable_title As String
  Dim m_ticket_debit_title As String
  'Ticket Configuration
  Dim m_ticket_allow_cashable_creation As Boolean
  Dim m_ticket_allow_promotional_creation As Boolean
  Dim m_ticket_allow_redemption As String
  'Ticket Appeareance
  Dim m_hide_ticket_number As Boolean
  Dim m_showed_numbers As Integer

  Dim m_cashable_ticket_expiration_days As Int32
  Dim m_promotional_cashable_expiration_days As Int32
  Dim m_promotional_non_cashable_expiration_days As Int32
  Dim m_ticket_considered_prize_from As Decimal
  Dim m_ticket_allow_collection_in_cashier As Boolean

  Dim m_jackpot_considered_prize_from As Decimal

#End Region ' Members

#Region "Constants"

  Public Const AUDIT_NONE_STRING As String = "---"
#End Region

#Region "Properties"

  Public Property MinAllowedTicketIn() As Decimal
    Get
      Return m_min_allowed_ticket_in
    End Get

    Set(ByVal Value As Decimal)
      m_min_allowed_ticket_in = Value
    End Set
  End Property ' MaxAllowedATM

  Public Property AllowTrucate() As Boolean
    Get
      Return m_allow_truncate
    End Get

    Set(ByVal Value As Boolean)
      m_allow_truncate = Value
    End Set
  End Property ' MaxAllowedATM

  Public Property MinDenominationMultiple() As Decimal
    Get
      Return m_min_denomination_multiple
    End Get

    Set(ByVal Value As Decimal)
      m_min_denomination_multiple = Value
    End Set
  End Property ' MinDenominationMultiple


  Public Property MaxAllowedRedeem() As Decimal
    Get
      Return m_max_allowed_Reedem
    End Get

    Set(ByVal Value As Decimal)
      m_max_allowed_Reedem = Value
    End Set
  End Property ' MaxAllowedATM
  Public Property MaxAllowedCreation() As Decimal
    Get
      Return m_max_allowed_creation
    End Get
    Set(ByVal value As Decimal)
      m_max_allowed_creation = value
    End Set
  End Property

  Public Property DetailCollectionLevel() As Integer
    Get
      Return m_detail_collection_level
    End Get

    Set(ByVal Value As Integer)
      m_detail_collection_level = Value
    End Set
  End Property ' DetailCollectionLevel

  Public Property TicketCollection() As Boolean
    Get
      Return m_ticket_collection
    End Get
    Set(ByVal value As Boolean)
      m_ticket_collection = value
    End Set
  End Property

  Public Property TicketLocation() As String
    Get
      Return m_ticket_location
    End Get

    Set(ByVal Value As String)
      m_ticket_location = Value
    End Set
  End Property ' TicketLocation

  Public Property TicketAddress1() As String
    Get
      Return m_ticket_address_1
    End Get

    Set(ByVal Value As String)
      m_ticket_address_1 = Value
    End Set
  End Property ' TicketAddress1

  Public Property TicketAddress2() As String
    Get
      Return m_ticket_address_2
    End Get

    Set(ByVal Value As String)
      m_ticket_address_2 = Value
    End Set
  End Property ' TicketAddress2

  Public Property PromoRedimTicketTitle() As String
    Get
      Return m_ticket_promo_redim_title
    End Get

    Set(ByVal Value As String)
      m_ticket_promo_redim_title = Value
    End Set
  End Property ' TicketTitle

  Public Property PromoNonRedimTicketTitle() As String
    Get
      Return m_ticket_promo_non_redim_title
    End Get
    Set(ByVal value As String)
      m_ticket_promo_non_redim_title = value
    End Set
  End Property

  Public Property CashableTicketTitle() As String
    Get
      Return m_ticket_cashable_title
    End Get
    Set(ByVal value As String)
      m_ticket_cashable_title = value
    End Set
  End Property

  Public Property DebitTicketTitle() As String
    Get
      Return m_ticket_debit_title
    End Get

    Set(ByVal Value As String)
      m_ticket_debit_title = Value
    End Set
  End Property

  Public Property AllowTicketRedemption() As Boolean
    Get
      Return m_ticket_allow_redemption
    End Get
    Set(ByVal value As Boolean)
      m_ticket_allow_redemption = value
    End Set
  End Property

  Public Property AllowCashableCreation() As Boolean
    Get
      Return m_ticket_allow_cashable_creation
    End Get
    Set(ByVal value As Boolean)
      m_ticket_allow_cashable_creation = value
    End Set
  End Property

  Public Property AllowPromotionalCreation() As Boolean
    Get
      Return m_ticket_allow_promotional_creation
    End Get
    Set(ByVal value As Boolean)
      m_ticket_allow_promotional_creation = value
    End Set
  End Property

  Public Property HideTicketNumber() As Boolean
    Get
      Return m_hide_ticket_number
    End Get
    Set(ByVal value As Boolean)
      m_hide_ticket_number = value
    End Set
  End Property

  Public Property ShowedNumbers() As Integer
    Get
      Return m_showed_numbers
    End Get
    Set(ByVal value As Integer)
      m_showed_numbers = value
    End Set
  End Property

  Public Property CashableTicketExpirationDays() As Int32
    Get
      Return m_cashable_ticket_expiration_days
    End Get
    Set(ByVal value As Int32)
      m_cashable_ticket_expiration_days = value
    End Set
  End Property

  Public Property PromotionalCashableExpirationDays() As Int32
    Get
      Return m_promotional_cashable_expiration_days
    End Get
    Set(ByVal value As Int32)
      m_promotional_cashable_expiration_days = value
    End Set
  End Property

  Public Property PromotionalNonCashableExpirationDays() As Int32
    Get
      Return m_promotional_non_cashable_expiration_days
    End Get
    Set(ByVal value As Int32)
      m_promotional_non_cashable_expiration_days = value
    End Set
  End Property

  Public Property TicketConsideredPrizeFrom() As Decimal
    Get
      Return m_ticket_considered_prize_from
    End Get
    Set(ByVal value As Decimal)
      m_ticket_considered_prize_from = value
    End Set
  End Property

  Public Property JackpotConsideredPrizeFrom() As Decimal
    Get
      Return m_jackpot_considered_prize_from
    End Get
    Set(ByVal value As Decimal)
      m_jackpot_considered_prize_from = value
    End Set
  End Property

  Public Property TicketAllowCollectionInCashier() As Boolean
    Get
      Return m_ticket_allow_collection_in_cashier
    End Get
    Set(ByVal value As Boolean)
      m_ticket_allow_collection_in_cashier = value
    End Set
  End Property

  Public Property StackerCapacity() As Integer
    Get
      Return m_stacker_capacity
    End Get
    Set(ByVal value As Integer)
      m_stacker_capacity = value
    End Set
  End Property

#End Region ' Properties

#Region " Overrides functions "

  '----------------------------------------------------------------------------
  ' PURPOSE: Returns the related auditor data for the current object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim auditor_data As CLASS_AUDITOR_DATA

    auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_CASHIER_CONFIGURATION)

    'auditor_data.IsAuditable = False
    auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(2302), "")

    Call auditor_data.SetField(0, IIf(Me.TicketCollection, GLB_NLS_GUI_AUDITOR.GetString(336), GLB_NLS_GUI_AUDITOR.GetString(337)) _
                                , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2734) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2481))

    Call auditor_data.SetField(0, GUI_FormatCurrency(Me.MaxAllowedRedeem), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2293))
    Call auditor_data.SetField(0, GUI_FormatCurrency(Me.MaxAllowedCreation), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2864))
    Call auditor_data.SetField(0, GUI_FormatNumber(Me.StackerCapacity, 0), GLB_NLS_GUI_PLAYER_TRACKING.GetString(4438))

    Call auditor_data.SetField(0, GUI_FormatCurrency(Me.MinAllowedTicketIn), GLB_NLS_GUI_PLAYER_TRACKING.GetString(7676))
    Call auditor_data.SetField(0, IIf(Me.AllowTrucate, GLB_NLS_GUI_AUDITOR.GetString(336), GLB_NLS_GUI_AUDITOR.GetString(337)), GLB_NLS_GUI_PLAYER_TRACKING.GetString(7677))
    Call auditor_data.SetField(0, GUI_FormatCurrency(Me.MinDenominationMultiple), GLB_NLS_GUI_PLAYER_TRACKING.GetString(7679))

    Select Case Me.DetailCollectionLevel
      Case "0"
        Call auditor_data.SetField(0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2305), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2734) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2298))

      Case "1"
        Call auditor_data.SetField(0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2306), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2734) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2298))

      Case "2"
        Call auditor_data.SetField(0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2307), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2734) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2298))

      Case Else
        Call auditor_data.SetField(0, Me.DetailCollectionLevel, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2298))

    End Select

    'ticket type personalization
    Call auditor_data.SetField(0, IIf(String.IsNullOrEmpty(Me.TicketLocation), AUDIT_NONE_STRING, Me.TicketLocation), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2321))
    Call auditor_data.SetField(0, IIf(String.IsNullOrEmpty(Me.TicketAddress1), AUDIT_NONE_STRING, Me.TicketAddress1), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2322))
    Call auditor_data.SetField(0, IIf(String.IsNullOrEmpty(Me.TicketAddress2), AUDIT_NONE_STRING, Me.TicketAddress2), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2323))
    Call auditor_data.SetField(0, IIf(String.IsNullOrEmpty(Me.PromoRedimTicketTitle), AUDIT_NONE_STRING, Me.PromoRedimTicketTitle), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2320, GLB_NLS_GUI_PLAYER_TRACKING.GetString(3032)))
    Call auditor_data.SetField(0, IIf(String.IsNullOrEmpty(Me.DebitTicketTitle), AUDIT_NONE_STRING, Me.DebitTicketTitle), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2320, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2325)))
    Call auditor_data.SetField(0, IIf(String.IsNullOrEmpty(Me.DebitTicketTitle), AUDIT_NONE_STRING, Me.CashableTicketTitle), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2320, GLB_NLS_GUI_PLAYER_TRACKING.GetString(3033)))
    Call auditor_data.SetField(0, IIf(String.IsNullOrEmpty(Me.DebitTicketTitle), AUDIT_NONE_STRING, Me.PromoNonRedimTicketTitle), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2320, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2326)))


    'ticket configuration
    Call auditor_data.SetField(0, IIf(Me.m_ticket_allow_cashable_creation, GLB_NLS_GUI_AUDITOR.GetString(336), GLB_NLS_GUI_AUDITOR.GetString(337)), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2718))
    Call auditor_data.SetField(0, IIf(Me.m_ticket_allow_redemption, GLB_NLS_GUI_AUDITOR.GetString(336), GLB_NLS_GUI_AUDITOR.GetString(337)), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2717))
    Call auditor_data.SetField(0, IIf(Me.m_ticket_allow_promotional_creation, GLB_NLS_GUI_AUDITOR.GetString(336), GLB_NLS_GUI_AUDITOR.GetString(337)), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2719))

    'Ticket Appearance
    Call auditor_data.SetField(0, IIf(Me.m_hide_ticket_number, GLB_NLS_GUI_AUDITOR.GetString(336), GLB_NLS_GUI_AUDITOR.GetString(337)), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2867))
    Call auditor_data.SetField(0, Me.m_showed_numbers, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2875))

    Call auditor_data.SetField(0, Me.CashableTicketExpirationDays, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4343))
    Call auditor_data.SetField(0, Me.PromotionalCashableExpirationDays, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4344))
    Call auditor_data.SetField(0, Me.PromotionalNonCashableExpirationDays, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4345))
    Call auditor_data.SetField(0, Me.TicketConsideredPrizeFrom, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4347))
    Call auditor_data.SetField(0, Me.JackpotConsideredPrizeFrom, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4382))
    Call auditor_data.SetField(0, IIf(Me.TicketAllowCollectionInCashier, GLB_NLS_GUI_AUDITOR.GetString(336), GLB_NLS_GUI_AUDITOR.GetString(337)), GLB_NLS_GUI_PLAYER_TRACKING.GetString(4346))

    Return auditor_data

  End Function ' AuditorData

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_NOT_SUPPORTED
  End Function ' DB_Delete

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_NOT_SUPPORTED
  End Function ' DB_Insert

  '----------------------------------------------------------------------------
  ' PURPOSE: Reads from the database into the given object
  '
  ' PARAMS:
  '   - INPUT:
  '     - ObjectId: An object, it must be 'casted' to the desired KEY.
  '
  '   - OUTPUT: ENUM_STATUS
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As ENUM_STATUS

    Using _db_trx As New DB_TRX()

      _rc = ReadConfig(_db_trx.SqlTransaction)

      If _rc <> ENUM_STATUS.STATUS_OK Then
        Call _db_trx.Rollback()
      Else
        _rc = IIf(_db_trx.Commit() = True, ENUM_STATUS.STATUS_OK, ENUM_STATUS.STATUS_ERROR)
      End If

    End Using ' _db_trx

    Return _rc

  End Function ' DB_Read

  '----------------------------------------------------------------------------
  ' PURPOSE: Updates the given object to the database.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As ENUM_STATUS

    Using _db_trx As New DB_TRX()

      _rc = UpdateConfig(_db_trx.SqlTransaction)

      If _rc <> ENUM_STATUS.STATUS_OK Then
        Call _db_trx.Rollback()
      Else
        _rc = IIf(_db_trx.Commit() = True, ENUM_STATUS.STATUS_OK, ENUM_STATUS.STATUS_ERROR)
      End If

    End Using ' _db_trx

    Return _rc

  End Function ' DB_Update

  '----------------------------------------------------------------------------
  ' PURPOSE: Returns a duplicate of the given object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE
    Dim _temp_tito_params As CLASS_TITO_PARAMS

    _temp_tito_params = New CLASS_TITO_PARAMS()
    _temp_tito_params.MaxAllowedRedeem = Me.MaxAllowedRedeem
    _temp_tito_params.MaxAllowedCreation = Me.MaxAllowedCreation
    _temp_tito_params.DetailCollectionLevel = Me.DetailCollectionLevel
    _temp_tito_params.TicketCollection = Me.TicketCollection
    _temp_tito_params.StackerCapacity = Me.StackerCapacity

    _temp_tito_params.TicketAddress1 = Me.TicketAddress1
    _temp_tito_params.TicketAddress2 = Me.TicketAddress2
    _temp_tito_params.TicketLocation = Me.TicketLocation
    _temp_tito_params.PromoRedimTicketTitle = Me.PromoRedimTicketTitle
    _temp_tito_params.PromoNonRedimTicketTitle = Me.PromoNonRedimTicketTitle
    _temp_tito_params.CashableTicketTitle = Me.CashableTicketTitle
    _temp_tito_params.DebitTicketTitle = Me.DebitTicketTitle

    _temp_tito_params.AllowCashableCreation = Me.AllowCashableCreation
    _temp_tito_params.AllowPromotionalCreation = Me.AllowPromotionalCreation
    _temp_tito_params.AllowTicketRedemption = Me.AllowTicketRedemption

    _temp_tito_params.HideTicketNumber = Me.HideTicketNumber
    _temp_tito_params.ShowedNumbers = Me.ShowedNumbers

    _temp_tito_params.CashableTicketExpirationDays = Me.CashableTicketExpirationDays
    _temp_tito_params.PromotionalCashableExpirationDays = Me.PromotionalCashableExpirationDays
    _temp_tito_params.PromotionalNonCashableExpirationDays = Me.PromotionalNonCashableExpirationDays
    _temp_tito_params.TicketConsideredPrizeFrom = Me.TicketConsideredPrizeFrom
    _temp_tito_params.JackpotConsideredPrizeFrom = Me.JackpotConsideredPrizeFrom()
    _temp_tito_params.TicketAllowCollectionInCashier = Me.TicketAllowCollectionInCashier

    _temp_tito_params.AllowTrucate = Me.AllowTrucate
    _temp_tito_params.MinAllowedTicketIn = Me.MinAllowedTicketIn
    _temp_tito_params.MinDenominationMultiple = Me.MinDenominationMultiple

    Return _temp_tito_params

  End Function ' Duplicate

#End Region 'Overrides 

#Region "Private Functions"

  '----------------------------------------------------------------------------
  ' PURPOSE: It calls the TITO_Configuration_DbUpdate and checks if the database result is ok
  '                                              
  ' PARAMS:
  '   - INPUT: 
  '   - OUTPUT:
  '
  ' RETURNS:
  '
  ' NOTES:
  Private Function UpdateConfig(ByVal Trx As SqlTransaction) As Integer

    Try

      If Not DB_GeneralParam_Update("TITO", "Tickets.MinAllowedTicketIn", Me.MinAllowedTicketIn.ToString(), Trx) Then

        Return ENUM_DB_RC.DB_RC_ERROR_DB
      End If

      If Not DB_GeneralParam_Update("TITO", "Tickets.AllowTruncate", IIf(Me.AllowTrucate, "1", "0"), Trx) Then

        Return ENUM_DB_RC.DB_RC_ERROR_DB
      End If

      If Not DB_GeneralParam_Update("TITO", "Tickets.MinDenominationMultiple", Me.MinDenominationMultiple.ToString(), Trx) Then

        Return ENUM_DB_RC.DB_RC_ERROR_DB
      End If

      If Not DB_GeneralParam_Update("TITO", "Tickets.MaxAllowedTicketIn", Me.MaxAllowedRedeem.ToString(), Trx) Then

        Return ENUM_DB_RC.DB_RC_ERROR_DB
      End If

      If Not DB_GeneralParam_Update("TITO", "Tickets.MaxAllowedTicketOut", Me.MaxAllowedCreation.ToString(), Trx) Then

        Return ENUM_DB_RC.DB_RC_ERROR_DB
      End If

      If Not DB_GeneralParam_Update("TITO", "NoteAcCollectionType", Me.DetailCollectionLevel.ToString(), Trx) Then

        Return ENUM_DB_RC.DB_RC_ERROR_DB
      End If

      If Not DB_GeneralParam_Update("TITO", "TicketsCollection", IIf(Me.TicketCollection, "1", "0"), Trx) Then

        Return ENUM_DB_RC.DB_RC_ERROR_DB
      End If

      If Not DB_GeneralParam_Update("TITO", "Tickets.Location", Me.TicketLocation, Trx) Then

        Return ENUM_DB_RC.DB_RC_ERROR_DB
      End If

      If Not DB_GeneralParam_Update("TITO", "Tickets.Address1", Me.TicketAddress1, Trx) Then

        Return ENUM_DB_RC.DB_RC_ERROR_DB
      End If

      If Not DB_GeneralParam_Update("TITO", "Tickets.Address2", Me.TicketAddress2, Trx) Then

        Return ENUM_DB_RC.DB_RC_ERROR_DB
      End If

      If Not DB_GeneralParam_Update("TITO", "PromotionalTickets.Redeemable.Title", Me.PromoRedimTicketTitle, Trx) Then

        Return ENUM_DB_RC.DB_RC_ERROR_DB
      End If

      If Not DB_GeneralParam_Update("TITO", "PromotionalTickets.NonRedeemable.Title", Me.PromoNonRedimTicketTitle, Trx) Then

        Return ENUM_DB_RC.DB_RC_ERROR_DB
      End If

      If Not DB_GeneralParam_Update("TITO", "CashableTickets.Title", Me.CashableTicketTitle, Trx) Then

        Return ENUM_DB_RC.DB_RC_ERROR_DB
      End If

      If Not DB_GeneralParam_Update("TITO", "DebitTickets.Title", Me.DebitTicketTitle, Trx) Then

        Return ENUM_DB_RC.DB_RC_ERROR_DB
      End If

      If Not DB_GeneralParam_Update("TITO", "CashableTickets.AllowEmission", IIf(Me.AllowCashableCreation, "1", "0"), Trx) Then

        Return ENUM_DB_RC.DB_RC_ERROR_DB
      End If

      If Not DB_GeneralParam_Update("TITO", "PromotionalTickets.AllowEmission", IIf(Me.AllowPromotionalCreation, "1", "0"), Trx) Then

        Return ENUM_DB_RC.DB_RC_ERROR_DB
      End If

      If Not DB_GeneralParam_Update("TITO", "AllowRedemption", IIf(Me.AllowTicketRedemption, "1", "0"), Trx) Then

        Return ENUM_DB_RC.DB_RC_ERROR_DB
      End If

      If Not DB_GeneralParam_Update("TITO", "Tickets.ShowLastNNumbers", Me.ShowedNumbers, Trx) Then

        Return ENUM_DB_RC.DB_RC_ERROR_DB
      End If

      If Not DB_GeneralParam_Update("TITO", "CashableTickets.ExpirationDays", Me.CashableTicketExpirationDays, Trx) Then

        Return ENUM_DB_RC.DB_RC_ERROR_DB
      End If

      If Not DB_GeneralParam_Update("TITO", "PromotionalTickets.Redeemable.ExpirationDays", Me.PromotionalCashableExpirationDays, Trx) Then

        Return ENUM_DB_RC.DB_RC_ERROR_DB
      End If

      If Not DB_GeneralParam_Update("TITO", "PromotionalTickets.NonRedeemable.ExpirationDays", Me.PromotionalNonCashableExpirationDays, Trx) Then

        Return ENUM_DB_RC.DB_RC_ERROR_DB
      End If

      If Not DB_GeneralParam_Update("TITO", "Cashier.TicketsCollection", IIf(Me.TicketAllowCollectionInCashier, "1", "0"), Trx) Then

        Return ENUM_DB_RC.DB_RC_ERROR_DB
      End If

      If Not DB_GeneralParam_Update("TITO", "Tickets.LimitBeforePrize", Me.TicketConsideredPrizeFrom, Trx) Then

        Return ENUM_DB_RC.DB_RC_ERROR_DB
      End If

      If Not DB_GeneralParam_Update("TITO", "Jackpot.LimitBeforePrize", Me.JackpotConsideredPrizeFrom, Trx) Then

        Return ENUM_DB_RC.DB_RC_ERROR_DB
      End If

      If Not DB_GeneralParam_Update("TITO", "StackerCapacity", Me.StackerCapacity.ToString(), Trx) Then

        Return ENUM_DB_RC.DB_RC_ERROR_DB
      End If

      Return ENUM_DB_RC.DB_RC_OK

    Catch _ex As Exception
      Return ENUM_STATUS.STATUS_ERROR
    End Try

  End Function ' UpdateConfig


  '----------------------------------------------------------------------------
  ' PURPOSE: It calls the TITO_Configuration_DbRead and checks if the database result is ok
  '                                              
  ' PARAMS:
  '   - INPUT: 
  '   - OUTPUT:
  '
  ' RETURNS:
  '
  ' NOTES:
  Private Function ReadConfig(ByVal Trx As SqlTransaction) As Integer
    Dim _general_param As GeneralParam.Dictionary
    Try
      _general_param = GeneralParam.Dictionary.Create()

      Me.MaxAllowedRedeem = _general_param.GetDecimal("TITO", "Tickets.MaxAllowedTicketIn", 0)
      Me.MaxAllowedCreation = _general_param.GetDecimal("TITO", "Tickets.MaxAllowedTicketOut", 0)
      Me.DetailCollectionLevel = _general_param.GetInt32("TITO", "NoteAcCollectionType", 0)
      Me.TicketCollection = _general_param.GetBoolean("TITO", "TicketsCollection", 0)
      Me.TicketLocation = _general_param.GetString("TITO", "Tickets.Location")
      Me.TicketAddress1 = _general_param.GetString("TITO", "Tickets.Address1")
      Me.TicketAddress2 = _general_param.GetString("TITO", "Tickets.Address2")
      Me.PromoRedimTicketTitle = _general_param.GetString("TITO", "PromotionalTickets.Redeemable.Title")
      Me.PromoNonRedimTicketTitle = _general_param.GetString("TITO", "PromotionalTickets.NonRedeemable.Title")
      Me.CashableTicketTitle = _general_param.GetString("TITO", "CashableTickets.Title")
      Me.DebitTicketTitle = _general_param.GetString("TITO", "DebitTickets.Title")
      Me.AllowCashableCreation = _general_param.GetBoolean("TITO", "CashableTickets.AllowEmission", False)
      Me.AllowPromotionalCreation = _general_param.GetBoolean("TITO", "PromotionalTickets.AllowEmission", False)
      Me.AllowTicketRedemption = _general_param.GetBoolean("TITO", "AllowRedemption", False)
      Me.ShowedNumbers = _general_param.GetInt32("TITO", "Tickets.ShowLastNNumbers", 0)
      If Me.ShowedNumbers = 0 Then
        Me.HideTicketNumber = False
      Else
        Me.HideTicketNumber = True
      End If
      Me.CashableTicketExpirationDays = _general_param.GetInt32("TITO", "CashableTickets.ExpirationDays", 1)
      Me.PromotionalCashableExpirationDays = _general_param.GetInt32("TITO", "PromotionalTickets.Redeemable.ExpirationDays", 1)
      Me.PromotionalNonCashableExpirationDays = _general_param.GetInt32("TITO", "PromotionalTickets.NonRedeemable.ExpirationDays", 1)
      Me.TicketAllowCollectionInCashier = _general_param.GetInt64("TITO", "Cashier.TicketsCollection", 0)
      Me.TicketConsideredPrizeFrom = _general_param.GetDecimal("TITO", "Tickets.LimitBeforePrize", 0)
      Me.JackpotConsideredPrizeFrom = _general_param.GetDecimal("TITO", "Jackpot.LimitBeforePrize", 0)

      Me.StackerCapacity = _general_param.GetInt32("TITO", "StackerCapacity", 0)

      Me.AllowTrucate = _general_param.GetBoolean("TITO", "Tickets.AllowTruncate", False)
      Me.MinAllowedTicketIn = _general_param.GetDecimal("TITO", "Tickets.MinAllowedTicketIn", 0)
      Me.MinDenominationMultiple = _general_param.GetDecimal("TITO", "Tickets.MinDenominationMultiple", 0)

    Catch _ex As Exception
      Return ENUM_STATUS.STATUS_ERROR
    End Try

    Return ENUM_STATUS.STATUS_OK

  End Function ' ReadConfig

#End Region ' Private Functions

End Class
