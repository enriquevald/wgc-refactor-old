'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : cls_password_configuration.vb
'
' DESCRIPTION : Configure Password Setting
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 03-AUG-2012  HBB    Initial version   
' 28-OCT-2013  ACM    Fixed Bug #WIG-322 Wrong auditor code.
' 14-OCT-2016  RAB    PBI 18098: General params: Automatically add the GP to the system
'--------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports System.Text
Imports WSI.Common

Public Class PASSWORD_CONFIGURATION
  Inherits CLASS_BASE

#Region "Structure"
  Public Class TYPE_PASSWORD_CONFIGURATION

    Public pass_number_of_character As Int32
    Public login_number_of_character As Int32
    Public num_of_digits As Int32
    Public num_of_lowercase As Int32
    Public num_of_uppercase As Int32
    Public num_of_special_character As Int32
    Public num_of_login_attempt As Int32
    Public num_of_days_without_login As Int32
    Public num_of_pass_history As Int32
    Public check_pass_rules As Int32
    Public change_pass_in_n_days As Int32
    Public change_pass_fist_time As Int32
    Public time_out_gui As Int32
    Public time_out_cashier As Int32

  End Class
#End Region

#Region " Members "

  Protected m_password_configuration As New TYPE_PASSWORD_CONFIGURATION()

#End Region

#Region "Properties"

  Public Property PassNumberOfCharacter() As Int32
    Get
      Return m_password_configuration.pass_number_of_character
    End Get

    Set(ByVal Value As Int32)
      m_password_configuration.pass_number_of_character = Value
    End Set
  End Property

  Public Property LoginNumberOfCharacter() As Int32
    Get
      Return m_password_configuration.login_number_of_character
    End Get
    Set(ByVal Value As Int32)
      m_password_configuration.login_number_of_character = Value
    End Set
  End Property

  Public Property NumOfDigits() As Int32
    Get
      Return m_password_configuration.num_of_digits
    End Get

    Set(ByVal Value As Int32)
      m_password_configuration.num_of_digits = value
    End Set
  End Property

  Public Property NumOfLowercase() As Int32
    Get
      Return m_password_configuration.num_of_lowercase
    End Get

    Set(ByVal Value As Int32)
      m_password_configuration.num_of_lowercase = Value
    End Set
  End Property

  Public Property NumOfUppercase() As Int32
    Get
      Return m_password_configuration.num_of_uppercase
    End Get

    Set(ByVal Value As Int32)
      m_password_configuration.num_of_uppercase = Value
    End Set
  End Property

  Public Property NumOfSpecialCharacter() As Int32
    Get
      Return m_password_configuration.num_of_special_character
    End Get

    Set(ByVal Value As Int32)
      m_password_configuration.num_of_special_character = Value
    End Set
  End Property

  Public Property NumOfLoginAttempt() As Int32
    Get
      Return m_password_configuration.num_of_login_attempt
    End Get

    Set(ByVal Value As Int32)
      m_password_configuration.num_of_login_attempt = Value
    End Set
  End Property

  Public Property NumOfDaysWithoutLogin() As Int32
    Get
      Return m_password_configuration.num_of_days_without_login
    End Get

    Set(ByVal Value As Int32)
      m_password_configuration.num_of_days_without_login = Value
    End Set
  End Property

  Public Property NumOfPassHistory() As Int32
    Get
      Return m_password_configuration.num_of_pass_history
    End Get
    Set(ByVal Value As Int32)
      m_password_configuration.num_of_pass_history = Value
    End Set
  End Property

  Public Property CheckPasswordConfiguration() As Int32
    Get
      Return m_password_configuration.check_pass_rules
    End Get
    Set(ByVal value As Int32)
      m_password_configuration.check_pass_rules = value
    End Set
  End Property

  Public Property ChangePassInNDays() As Int32
    Get
      Return m_password_configuration.change_pass_in_n_days
    End Get
    Set(ByVal Value As Int32)
      m_password_configuration.change_pass_in_n_days = value
    End Set
  End Property

  Public Property ChangePassFirstTime() As Int32
    Get
      Return m_password_configuration.change_pass_fist_time
    End Get
    Set(ByVal Value As Int32)
      m_password_configuration.change_pass_fist_time = value
    End Set
  End Property

  Public Property TimeOutGUI() As Int32
    Get
      Return m_password_configuration.time_out_gui
    End Get
    Set(ByVal Value As Int32)
      m_password_configuration.time_out_gui = Value
    End Set
  End Property

  Public Property TimeOutCashier() As Int32
    Get
      Return m_password_configuration.time_out_cashier
    End Get
    Set(ByVal Value As Int32)
      m_password_configuration.time_out_cashier = Value
    End Set
  End Property


#End Region 'Properties

#Region " Overrides functions "

  '----------------------------------------------------------------------------
  ' PURPOSE: Returns the related auditor data for the current object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim auditor_data As CLASS_AUDITOR_DATA

    auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_CASHIER_CONFIGURATION)
    auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(1175), "")

    Call auditor_data.SetField(0, Me.CheckPasswordConfiguration, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1183))
    Call auditor_data.SetField(0, Me.PassNumberOfCharacter, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1201) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1167))
    Call auditor_data.SetField(0, Me.NumOfDigits, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1201) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1168))
    Call auditor_data.SetField(0, Me.NumOfLowercase, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1201) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1169))
    Call auditor_data.SetField(0, Me.NumOfUppercase, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1201) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1170))
    Call auditor_data.SetField(0, Me.NumOfSpecialCharacter, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1201) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1171))
    Call auditor_data.SetField(0, Me.NumOfLoginAttempt, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1172))
    Call auditor_data.SetField(0, Me.NumOfDaysWithoutLogin, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1173))
    Call auditor_data.SetField(0, Me.NumOfPassHistory, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1174))
    Call auditor_data.SetField(0, Me.LoginNumberOfCharacter, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1202) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1199))
    Call auditor_data.SetField(0, Me.TimeOutGUI, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1209))
    Call auditor_data.SetField(0, Me.TimeOutCashier, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1210))
    Call auditor_data.SetField(0, Me.ChangePassFirstTime, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1212))
    Call auditor_data.SetField(0, Me.ChangePassInNDays, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1211))

    Return auditor_data

  End Function

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
  End Function

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE: Reads from the database into the given object
  '
  ' PARAMS:
  '   - INPUT:
  '     - ObjectId: An object, it must be 'casted' to the desired KEY.
  '
  '   - OUTPUT: ENUM_STATUS
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim rc As Integer

    rc = PasswordConf_DbRead(SqlCtx)

    Select Case rc
      Case ENUM_DB_RC.DB_RC_OK
        Return ENUM_STATUS.STATUS_OK

      Case ENUM_DB_RC.DB_RC_ERROR_NOT_FOUND
        Return CLASS_BASE.ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE: Updates the given object to the database.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim rc As Integer

    rc = PasswordConf_DbUpdate()

    Select Case rc
      Case ENUM_DB_RC.DB_RC_OK
        Return ENUM_STATUS.STATUS_OK

      Case ENUM_DB_RC.DB_RC_ERROR_NOT_FOUND
        Return CLASS_BASE.ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR
    End Select

  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE: Returns a duplicate of the given object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE
    Dim _temp_pass_conf As PASSWORD_CONFIGURATION

    _temp_pass_conf = New PASSWORD_CONFIGURATION()

    _temp_pass_conf.CheckPasswordConfiguration = Me.CheckPasswordConfiguration
    _temp_pass_conf.NumOfDaysWithoutLogin = Me.NumOfDaysWithoutLogin
    _temp_pass_conf.NumOfDigits = Me.NumOfDigits
    _temp_pass_conf.NumOfLoginAttempt = Me.NumOfLoginAttempt
    _temp_pass_conf.NumOfLowercase = Me.NumOfLowercase
    _temp_pass_conf.NumOfPassHistory = Me.NumOfPassHistory
    _temp_pass_conf.NumOfSpecialCharacter = Me.NumOfSpecialCharacter
    _temp_pass_conf.NumOfUppercase = Me.NumOfUppercase
    _temp_pass_conf.PassNumberOfCharacter = Me.PassNumberOfCharacter
    _temp_pass_conf.LoginNumberOfCharacter = Me.LoginNumberOfCharacter
    _temp_pass_conf.ChangePassInNDays = Me.ChangePassInNDays
    _temp_pass_conf.TimeOutCashier = Me.TimeOutCashier
    _temp_pass_conf.TimeOutGUI = Me.TimeOutGUI
    _temp_pass_conf.ChangePassFirstTime = Me.ChangePassFirstTime

    Return _temp_pass_conf

  End Function

#End Region 'Overrides functions

#Region "Private Functions"

  '----------------------------------------------------------------------------
  ' PURPOSE: It calls the Password_Configuration_DbUpdate method to update the database
  '                                              
  ' PARAMS:
  '   - INPUT: 
  '   - OUTPUT:
  '
  ' RETURNS:
  '
  ' NOTES:
  Private Function PasswordConf_DbUpdate() As Integer

    Try
      Using _db_trx As New DB_TRX()

        Dim _gp_update_list As New List(Of String())

        _gp_update_list.Add(GeneralParam.AddToList("User", "Password.RulesEnabled", Me.CheckPasswordConfiguration))
        _gp_update_list.Add(GeneralParam.AddToList("User", "MaxDaysWithoutLogin", Me.NumOfDaysWithoutLogin))
        _gp_update_list.Add(GeneralParam.AddToList("User", "Password.MinDigits", Me.NumOfDigits))
        _gp_update_list.Add(GeneralParam.AddToList("User", "MaxLoginAttempts", Me.NumOfLoginAttempt))
        _gp_update_list.Add(GeneralParam.AddToList("User", "Password.MinLowerCase", Me.NumOfLowercase))
        _gp_update_list.Add(GeneralParam.AddToList("User", "Password.MaxPasswordHistory", Me.NumOfPassHistory))
        _gp_update_list.Add(GeneralParam.AddToList("User", "Password.MinSpecialCase", Me.NumOfSpecialCharacter))
        _gp_update_list.Add(GeneralParam.AddToList("User", "Password.MinUpperCase", Me.NumOfUppercase))
        _gp_update_list.Add(GeneralParam.AddToList("User", "Password.MinLength", Me.PassNumberOfCharacter))
        _gp_update_list.Add(GeneralParam.AddToList("User", "Login.MinLength", Me.LoginNumberOfCharacter))
        _gp_update_list.Add(GeneralParam.AddToList("User", "Password.ChangeInNDays", Me.ChangePassInNDays))
        _gp_update_list.Add(GeneralParam.AddToList("User", "SessionTimeOut.GUI", Me.TimeOutGUI))
        _gp_update_list.Add(GeneralParam.AddToList("User", "SessionTimeOut.Cashier", Me.TimeOutCashier))

        For Each _general_param As String() In _gp_update_list
          If (Not GUI_Controls.DB_GeneralParam_Update(_general_param(GeneralParam.COLUMN_NAME.Group),
                                                      _general_param(GeneralParam.COLUMN_NAME.Subject),
                                                      _general_param(GeneralParam.COLUMN_NAME.Value),
                                                      _db_trx.SqlTransaction)) Then

            Return ENUM_DB_RC.DB_RC_ERROR_DB
          End If
        Next

        _db_trx.SqlTransaction.Commit()
        Return ENUM_DB_RC.DB_RC_OK
      End Using

    Catch
    End Try
  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE: Update the database General_Params. If it goes ok the method returns true 
  '                                              if it doesn't go ok return false
  ' PARAMS:
  '   - INPUT: General_Params.Key
  '   - INPUT: General_Params.SubKey 
  '   - OUTPUT: get value from the database
  '
  ' RETURNS:
  '     - It returns True or false, depends if the transaction was fine or not
  '
  ' NOTES:
  Private Function Password_Configuration_DbRead(ByVal Key As String, _
                                                ByVal SubKey As String, ByRef Value As String, _
                                                ByVal Trx As SqlTransaction) As Boolean
    Dim _sb As StringBuilder
    Dim _obj As Object

    Value = ""

    Try
      _sb = New StringBuilder()
      _sb.AppendLine("SELECT   GP_KEY_VALUE ")
      _sb.AppendLine("  FROM   GENERAL_PARAMS ")
      _sb.AppendLine(" WHERE   GP_GROUP_KEY   = @pGroup")
      _sb.AppendLine("   AND   GP_SUBJECT_KEY = @pSubject")

      Using _cmd As SqlCommand = New SqlCommand(_sb.ToString(), Trx.Connection, Trx)
        _cmd.Parameters.Add("@pGroup", SqlDbType.NVarChar).Value = Key
        _cmd.Parameters.Add("@pSubject", SqlDbType.NVarChar).Value = SubKey
        _obj = _cmd.ExecuteScalar()

        If _obj IsNot Nothing AndAlso _obj IsNot DBNull.Value Then
          Value = CStr(_obj)
        End If

        Return True
      End Using
    Catch
    End Try

    Return False
  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE: It calls the Password_Configuration_DbRead and checks if the database result is ok
  '                                              
  ' PARAMS:
  '   - INPUT: 
  '   - OUTPUT:
  '
  ' RETURNS:
  '
  ' NOTES:
  Private Function PasswordConf_DbRead(ByVal Context As Integer) As Integer

    Dim _final_db_result
    Dim _value As String

    _value = ""
    _final_db_result = ENUM_DB_RC.DB_RC_OK

    Try

      Using _db_trx As New DB_TRX()

        If Not Me.Password_Configuration_DbRead("User", "Password.RulesEnabled", _value, _db_trx.SqlTransaction) Or Not Integer.TryParse(_value, Me.CheckPasswordConfiguration) Then
          Me.CheckPasswordConfiguration = -1
          _final_db_result = ENUM_DB_RC.DB_RC_ERROR_DB
        End If

        If Not Me.Password_Configuration_DbRead("User", "Password.MinLength", _value, _db_trx.SqlTransaction) Or Not Integer.TryParse(_value, Me.PassNumberOfCharacter) Then
          Me.PassNumberOfCharacter = -1
          _final_db_result = ENUM_DB_RC.DB_RC_ERROR_DB
        End If

        If Not Me.Password_Configuration_DbRead("User", "Login.MinLength", _value, _db_trx.SqlTransaction) Or Not Integer.TryParse(_value, Me.LoginNumberOfCharacter) Then
          Me.LoginNumberOfCharacter = -1
          _final_db_result = ENUM_DB_RC.DB_RC_ERROR_DB
        End If

        If Not Me.Password_Configuration_DbRead("User", "Password.MinDigits", _value, _db_trx.SqlTransaction) OrElse Not Integer.TryParse(_value, Me.NumOfDigits) Then
          Me.NumOfDigits = -1
          _final_db_result = ENUM_DB_RC.DB_RC_ERROR_DB
        End If

        If Not Me.Password_Configuration_DbRead("User", "Password.MinLowerCase", _value, _db_trx.SqlTransaction) Or Not Integer.TryParse(_value, Me.NumOfLowercase) Then
          Me.NumOfLowercase = -1
          _final_db_result = ENUM_DB_RC.DB_RC_ERROR_DB
        End If

        If Not Me.Password_Configuration_DbRead("User", "Password.MinUpperCase", _value, _db_trx.SqlTransaction) Or Not Integer.TryParse(_value, Me.NumOfUppercase) Then
          Me.NumOfUppercase = -1
          _final_db_result = ENUM_DB_RC.DB_RC_ERROR_DB
        End If

        If Not Me.Password_Configuration_DbRead("User", "Password.MinSpecialCase", _value, _db_trx.SqlTransaction) Or Not Integer.TryParse(_value, Me.NumOfSpecialCharacter) Then
          Me.NumOfSpecialCharacter = -1
          _final_db_result = ENUM_DB_RC.DB_RC_ERROR_DB
        End If

        If Not Me.Password_Configuration_DbRead("User", "MaxLoginAttempts", _value, _db_trx.SqlTransaction) Or Not Integer.TryParse(_value, Me.NumOfLoginAttempt) Then
          Me.NumOfLoginAttempt = -1
          _final_db_result = ENUM_DB_RC.DB_RC_ERROR_DB
        End If

        If Not Me.Password_Configuration_DbRead("User", "Password.MaxPasswordHistory", _value, _db_trx.SqlTransaction) Or Not Integer.TryParse(_value, Me.NumOfPassHistory) Then
          Me.NumOfPassHistory = -1
          _final_db_result = ENUM_DB_RC.DB_RC_ERROR_DB
        End If

        If Not Me.Password_Configuration_DbRead("User", "MaxDaysWithoutLogin", _value, _db_trx.SqlTransaction) Or Not Integer.TryParse(_value, Me.NumOfDaysWithoutLogin) Then
          Me.NumOfDaysWithoutLogin = -1
          _final_db_result = ENUM_DB_RC.DB_RC_ERROR_DB
        End If

        If Not Me.Password_Configuration_DbRead("User", "Password.ChangeInNDays", _value, _db_trx.SqlTransaction) Or Not Integer.TryParse(_value, Me.ChangePassInNDays) Then
          Me.ChangePassInNDays = -1
          _final_db_result = ENUM_DB_RC.DB_RC_ERROR_DB
        End If

        If Not Me.Password_Configuration_DbRead("User", "SessionTimeOut.Cashier", _value, _db_trx.SqlTransaction) Or Not Integer.TryParse(_value, Me.TimeOutCashier) Then
          Me.TimeOutCashier = -1
          _final_db_result = ENUM_DB_RC.DB_RC_ERROR_DB
        End If

        If Not Me.Password_Configuration_DbRead("User", "SessionTimeOut.GUI", _value, _db_trx.SqlTransaction) Or Not Integer.TryParse(_value, Me.TimeOutGUI) Then
          Me.TimeOutGUI = -1
          _final_db_result = ENUM_DB_RC.DB_RC_ERROR_DB
        End If

      End Using

      Return _final_db_result
    Catch
    End Try

    Return ENUM_DB_RC.DB_RC_ERROR_DB
  End Function

#End Region

End Class
