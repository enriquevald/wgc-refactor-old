'-------------------------------------------------------------------
' Copyright � 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : cls_gift.vb
'
' DESCRIPTION : gift class for user edition
'              
' REVISION HISTORY :
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 23-SEP-2010  TJG    Initial version
' 10-MAY-2012  MPO    Set or Get the image of gift (is possible to add several images)
' 23-MAY-2012  MPO    Set or Get the image and icon of the gift. Save/Retrieve of the db.
' 6-MAY-2013   MAR    Added daily and monthly client and global limits variables
' 18-SEP-2013  DRV    Added Points for level
' 07-APR-2014  JBP    Added IsVip property.
'-------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient

Public Class CLASS_GIFT
  Inherits CLASS_BASE

#Region " Constants "

  ' Fields length
  Public Const MAX_NAME_LEN As Integer = 50
  Public Const MAX_DESCRIPTION_LEN As Integer = 200

#End Region

#Region " Enums "

  Public Enum ENUM_GIFT_TYPE
    OBJECT_GIFT = 0
    NOT_REDEEMABLE_CREDIT = 1
    DRAW_NUMBERS = 2
    SERVICES = 3
    REDEEMABLE_CREDIT = 4
    UNKNOWN = 999
  End Enum

  Public Enum ENUM_GIFT_STATUS
    NOT_AVAILABLE = 0
    AVAILABLE = 1
    UNKNOWN = 999
  End Enum

#End Region ' Enums 

#Region " Structures "

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_GIFT
    Public gift_id As Int64
    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_NAME_LEN + 1)> _
    Public name As String
    Public available As Integer
    Public type As Integer
    '  Public points As Double
    Public conversion_to_nrc As Double
    Public current_stock As Integer
    Public request_counter As Integer
    Public delivery_counter As Integer
    Public image As Image
    Public icon As Image
    Public small_resource_id As Nullable(Of Long)
    Public large_resource_id As Nullable(Of Long)
    Public description As String
    Public account_daily_limit As Integer
    Public account_monthly_limit As Integer
    Public global_daily_limit As Integer
    Public global_monthly_limit As Integer
    Public points_level1 As Double
    Public points_level2 As Double
    Public points_level3 As Double
    Public points_level4 As Double
    Public change_table_name As String
    Public award_on_promobox As Integer
    Public text_on_promobox As String
    Public is_vip As Boolean
  End Class

#End Region

#Region " Members "

  Protected m_gift As New TYPE_GIFT

#End Region

#Region " Properties "

  Public Property GiftId() As Int64
    Get
      Return m_gift.gift_id
    End Get

    Set(ByVal Value As Int64)
      m_gift.gift_id = Value
    End Set
  End Property

  Public Property Name() As String
    Get
      Return m_gift.name
    End Get

    Set(ByVal Value As String)
      m_gift.name = Value
    End Set
  End Property

  Public Property Available() As Boolean
    Get
      Return m_gift.available
    End Get

    Set(ByVal Value As Boolean)
      m_gift.available = Value
    End Set
  End Property

  Public Property Type() As Integer
    Get
      Return m_gift.type
    End Get

    Set(ByVal Value As Integer)
      m_gift.type = Value
    End Set
  End Property

  'Public Property Points() As Double
  '  Get
  '    Return m_gift.points
  '  End Get

  '  Set(ByVal Value As Double)
  '    m_gift.points = Value
  '  End Set
  'End Property

  Public Property ConversionToNRC() As Double
    Get
      Return m_gift.conversion_to_nrc
    End Get

    Set(ByVal Value As Double)
      m_gift.conversion_to_nrc = Value
    End Set
  End Property

  Public Property CurrentStock() As Integer
    Get
      Return m_gift.current_stock
    End Get

    Set(ByVal Value As Integer)
      m_gift.current_stock = Value
    End Set
  End Property

  Public Property RequestCounter() As Integer
    Get
      Return m_gift.request_counter
    End Get

    Set(ByVal Value As Integer)
      m_gift.request_counter = Value
    End Set
  End Property

  Public Property DeliveryCounter() As Integer
    Get
      Return m_gift.delivery_counter
    End Get

    Set(ByVal Value As Integer)
      m_gift.delivery_counter = Value
    End Set
  End Property

  Public Property Image() As Image
    Get
      Return Me.m_gift.image
    End Get
    Set(ByVal value As Image)
      Me.m_gift.image = value
    End Set
  End Property

  Public Property Icon() As Image
    Get
      Return Me.m_gift.icon
    End Get
    Set(ByVal value As Image)
      Me.m_gift.icon = value
    End Set
  End Property


  'Public Property IconId() As Long
  '  Get
  '    Return Me.m_gift.small_resource_id
  '  End Get
  '  Set(ByVal value As Long)
  '    Me.m_gift.small_resource_id = value
  '  End Set
  'End Property

  'Public Property ImageId() As Long
  '  Get
  '    If Me.m_gift.large_resource_id.HasValue Then
  '      Return Me.m_gift.large_resource_id.Value
  '    Else
  '      Return 0
  '    End If
  '  End Get
  '  Set(ByVal value As Long)
  '    Me.m_gift.large_resource_id = value
  '  End Set
  'End Property

  Public Property Description() As String
    Get
      Return Me.m_gift.description
    End Get
    Set(ByVal value As String)
      Me.m_gift.description = value
    End Set
  End Property

  Public Property AccountDailyLimit() As Integer
    Get
      Return Me.m_gift.account_daily_limit
    End Get
    Set(ByVal value As Integer)
      Me.m_gift.account_daily_limit = value
    End Set
  End Property

  Public Property AccountMonthlyLimit() As Integer
    Get
      Return Me.m_gift.account_monthly_limit
    End Get
    Set(ByVal value As Integer)
      Me.m_gift.account_monthly_limit = value
    End Set
  End Property

  Public Property GlobalDailyLimit() As Integer
    Get
      Return Me.m_gift.global_daily_limit
    End Get
    Set(ByVal value As Integer)
      Me.m_gift.global_daily_limit = value
    End Set
  End Property

  Public Property GlobalMonthlyLimit() As Integer
    Get
      Return Me.m_gift.global_monthly_limit
    End Get
    Set(ByVal value As Integer)
      Me.m_gift.global_monthly_limit = value
    End Set
  End Property

  Public Property PointsLevel1() As Double
    Get
      Return m_gift.points_level1
    End Get

    Set(ByVal Value As Double)
      m_gift.points_level1 = Value
    End Set
  End Property

  Public Property PointsLevel2() As Double
    Get
      Return m_gift.points_level2
    End Get

    Set(ByVal Value As Double)
      m_gift.points_level2 = Value
    End Set
  End Property

  Public Property PointsLevel3() As Double
    Get
      Return m_gift.points_level3
    End Get

    Set(ByVal Value As Double)
      m_gift.points_level3 = Value
    End Set
  End Property

  Public Property PointsLevel4() As Double
    Get
      Return m_gift.points_level4
    End Get

    Set(ByVal Value As Double)
      m_gift.points_level4 = Value
    End Set
  End Property

  Public ReadOnly Property BelongsToRedemptionTable() As Boolean
    Get
      If String.IsNullOrEmpty(Me.m_gift.change_table_name) Then
        Return False
      Else
        Return m_gift.change_table_name.Length > 0
      End If
    End Get
  End Property

  Public ReadOnly Property ChangeTableName() As String
    Get
      Return m_gift.change_table_name
    End Get
  End Property

  Public Property AwardOnPromoBOX() As Boolean
    Get
      Return m_gift.award_on_promobox
    End Get
    Set(ByVal Value As Boolean)
      m_gift.award_on_promobox = Value
    End Set
  End Property

  Public Property TextOnPromoBOX() As String
    Get
      Return m_gift.text_on_promobox
    End Get
    Set(ByVal Value As String)
      m_gift.text_on_promobox = Value
    End Set
  End Property

  Public Property IsVip() As Boolean
    Get
      Return m_gift.is_vip
    End Get
    Set(ByVal Value As Boolean)
      m_gift.is_vip = Value
    End Set
  End Property

#End Region ' Structures

#Region " Overrides functions "

  '----------------------------------------------------------------------------
  ' PURPOSE : Reads from the database into the given object
  '
  ' PARAMS :
  '     - INPUT :
  '         - ObjectId: An object, it must be 'casted' to the desired KEY.
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '
  ' NOTES :

  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As Integer

    Me.GiftId = ObjectId

    ' Read gift data
    _rc = ReadGift(m_gift, SqlCtx)

    Select Case _rc

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Read

  '----------------------------------------------------------------------------
  ' PURPOSE : Updates the given object to the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES :

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As Integer

    ' Update gift data
    _rc = UpdateGift(m_gift, SqlCtx)

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
        Return ENUM_STATUS.STATUS_DUPLICATE_KEY

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Update

  '----------------------------------------------------------------------------
  ' PURPOSE : Inserts the given object to the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES :

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As Integer

    ' Insert gift data
    _rc = InsertGift(m_gift, SqlCtx)

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
        Return ENUM_STATUS.STATUS_DUPLICATE_KEY

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Insert

  '----------------------------------------------------------------------------
  ' PURPOSE : Deletes the given object from the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DEPENDENCIES
  '
  ' NOTES :

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As Integer

    ' Delete gift data
    _rc = DeleteGift(Me.GiftId, Me.m_gift.small_resource_id, Me.m_gift.large_resource_id, SqlCtx)

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DEPENDENCIES
        Return ENUM_STATUS.STATUS_DEPENDENCIES

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Delete

  '----------------------------------------------------------------------------
  ' PURPOSE : Returns a duplicate of the given object.
  '
  ' PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - The newly created object
  '
  ' NOTES :

  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim temp_gift As CLASS_GIFT

    temp_gift = New CLASS_GIFT
    temp_gift.m_gift.gift_id = Me.m_gift.gift_id
    temp_gift.m_gift.name = Me.m_gift.name
    'temp_gift.m_gift.points = Me.m_gift.points
    temp_gift.m_gift.conversion_to_nrc = Me.m_gift.conversion_to_nrc
    temp_gift.m_gift.type = Me.m_gift.type
    temp_gift.m_gift.available = Me.m_gift.available
    temp_gift.m_gift.current_stock = Me.m_gift.current_stock
    temp_gift.m_gift.request_counter = Me.m_gift.request_counter
    temp_gift.m_gift.delivery_counter = Me.m_gift.delivery_counter
    temp_gift.m_gift.description = Me.m_gift.description
    temp_gift.m_gift.large_resource_id = Me.m_gift.large_resource_id
    temp_gift.m_gift.small_resource_id = Me.m_gift.small_resource_id

    temp_gift.m_gift.account_daily_limit = Me.m_gift.account_daily_limit
    temp_gift.m_gift.account_monthly_limit = Me.m_gift.account_monthly_limit
    temp_gift.m_gift.global_daily_limit = Me.m_gift.global_daily_limit
    temp_gift.m_gift.global_monthly_limit = Me.m_gift.global_monthly_limit
    temp_gift.m_gift.points_level1 = Me.m_gift.points_level1
    temp_gift.m_gift.points_level2 = Me.m_gift.points_level2
    temp_gift.m_gift.points_level3 = Me.m_gift.points_level3
    temp_gift.m_gift.points_level4 = Me.m_gift.points_level4
    temp_gift.m_gift.change_table_name = Me.m_gift.change_table_name

    ' PromoBOX
    temp_gift.AwardOnPromoBOX = Me.m_gift.award_on_promobox
    temp_gift.TextOnPromoBOX = Me.m_gift.text_on_promobox

    temp_gift.IsVip = Me.m_gift.is_vip

    If IsNothing(Me.m_gift.icon) Then
      temp_gift.m_gift.icon = Nothing
    Else
      temp_gift.m_gift.icon = Me.m_gift.icon.Clone
    End If

    If IsNothing(Me.m_gift.image) Then
      temp_gift.m_gift.image = Nothing
    Else
      temp_gift.m_gift.image = Me.m_gift.image.Clone
    End If

    Return temp_gift

  End Function ' Duplicate

  '----------------------------------------------------------------------------
  ' PURPOSE : Returns the related auditor data for the current object.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - The newly created audit object
  '
  ' NOTES :

  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _yes_text As String
    Dim _no_text As String

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_GIFTS)

    _yes_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(278)
    _no_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)

    Call _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(339), Me.Name)      ' 339 "Regalo"
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(353), Me.Name)     ' 353 "Nombre"

    ' 355 "Tipo"
    ' 356 "Objeto"
    ' 357 "Cr�ditos No Redimibles"
    ' 313 "Cr�ditos Redimibles"
    ' 395 "Boleto de Sorteo"
    ' 396 "Servicios"
    Select Case Me.Type
      Case CLASS_GIFT.ENUM_GIFT_TYPE.OBJECT_GIFT
        Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(355), GLB_NLS_GUI_PLAYER_TRACKING.GetString(356))

      Case CLASS_GIFT.ENUM_GIFT_TYPE.NOT_REDEEMABLE_CREDIT
        Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(355), GLB_NLS_GUI_PLAYER_TRACKING.GetString(357))

      Case CLASS_GIFT.ENUM_GIFT_TYPE.REDEEMABLE_CREDIT
        Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(355), GLB_NLS_GUI_PLAYER_TRACKING.GetString(313))

      Case CLASS_GIFT.ENUM_GIFT_TYPE.DRAW_NUMBERS
        Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(355), GLB_NLS_GUI_PLAYER_TRACKING.GetString(395))

      Case CLASS_GIFT.ENUM_GIFT_TYPE.SERVICES
        Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(355), GLB_NLS_GUI_PLAYER_TRACKING.GetString(396))

      Case Else
    End Select

    ' 357 "Cr�ditos No Redimibles"
    ' 313 "Cr�ditos Redimibles"
    ' 381 "Stock"
    Select Case Me.Type
      Case CLASS_GIFT.ENUM_GIFT_TYPE.OBJECT_GIFT
        Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(357), "---")
        Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(313), "---")
        Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(381), Me.CurrentStock)

      Case CLASS_GIFT.ENUM_GIFT_TYPE.NOT_REDEEMABLE_CREDIT
        Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(357), Me.ConversionToNRC)
        Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(313), "---")
        Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(381), "---")

      Case CLASS_GIFT.ENUM_GIFT_TYPE.REDEEMABLE_CREDIT
        Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(357), "---")
        Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(313), Me.ConversionToNRC)
        Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(381), "---")

      Case CLASS_GIFT.ENUM_GIFT_TYPE.DRAW_NUMBERS
        Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(357), "---")
        Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(313), "---")
        Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(381), "---")

      Case CLASS_GIFT.ENUM_GIFT_TYPE.SERVICES
        Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(357), "---")
        Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(313), "---")
        Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(381), "---")

      Case Else
    End Select


    '''if points for level = -1 it means that the gift it's not available for that level in the DB it's saved as NULL
    '' 2624 "Puntos Nivel %1"
    Call _auditor_data.SetField(0, IIf(Me.m_gift.points_level1 = -1, "---", Me.m_gift.points_level1), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2624, WSI.Common.GeneralParam.GetString("PlayerTracking", "Level01.Name")))
    Call _auditor_data.SetField(0, IIf(Me.m_gift.points_level2 = -1, "---", Me.m_gift.points_level2), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2624, WSI.Common.GeneralParam.GetString("PlayerTracking", "Level02.Name")))
    Call _auditor_data.SetField(0, IIf(Me.m_gift.points_level3 = -1, "---", Me.m_gift.points_level3), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2624, WSI.Common.GeneralParam.GetString("PlayerTracking", "Level03.Name")))
    Call _auditor_data.SetField(0, IIf(Me.m_gift.points_level4 = -1, "---", Me.m_gift.points_level4), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2624, WSI.Common.GeneralParam.GetString("PlayerTracking", "Level04.Name")))



    ' 358 "Disponible"    
    ' 359 "S�"
    ' 360 "No"
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(358), GLB_NLS_GUI_PLAYER_TRACKING.GetString(IIf(Me.Available, 359, 360)))

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(719), GetInfoCRC(Me.Icon))
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(687), GetInfoCRC(Me.Image))

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(688), Me.Description)

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1930), Me.AccountDailyLimit)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1931), Me.AccountMonthlyLimit)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1989), Me.GlobalDailyLimit)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1990), Me.GlobalMonthlyLimit)

    ' JMM 30-APR-2014
    ' Redeemable on PromoBOX
    If Me.AwardOnPromoBOX = True Then
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(4917), _yes_text)
    Else
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(4917), _no_text)
    End If
    ' Name on PromoBOX
    _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(4916), Me.TextOnPromoBOX)

    ' JBP 07-APR-2014: Gift is Vip
    _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(4804), IIf(Me.IsVip, _yes_text, _no_text))

    Return _auditor_data

  End Function ' AuditorData

#End Region

#Region " Private Functions "

  '----------------------------------------------------------------------------
  ' PURPOSE : Reads a gift object from DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - Item.gift_id
  '         - Context
  '
  '     - OUTPUT :
  '         - Item
  '
  ' RETURNS :
  '     - CONFIGURATION_OK
  '     - CONFIGURATION_ERROR_DB
  '
  ' NOTES :

  Private Function ReadGift(ByVal Item As TYPE_GIFT, _
                            ByVal Context As Integer) As Integer

    Dim _sql_query As String
    Dim _data_table As DataTable
    Dim _sql_tx As System.Data.SqlClient.SqlTransaction = Nothing

    _sql_query = "SELECT  GI_NAME" _
                    + " , GI_TYPE" _
                    + " , GI_AVAILABLE" _
                    + " , ISNULL (GI_CONVERSION_TO_NRC, 0) _GI_CONVERSION_TO_NRC" _
                    + " , GI_CURRENT_STOCK" _
                    + " , GI_REQUEST_COUNTER " _
                    + " , GI_DELIVERY_COUNTER " _
                    + " , GI_LARGE_RESOURCE_ID " _
                    + " , GI_SMALL_RESOURCE_ID " _
                    + " , GI_DESCRIPTION " _
                    + " , ISNULL (GI_ACCOUNT_DAILY_LIMIT, 0)  GI_ACCOUNT_DAILY_LIMIT " _
                    + " , ISNULL (GI_ACCOUNT_MONTHLY_LIMIT, 0)  GI_ACCOUNT_MONTHLY_LIMIT " _
                    + " , ISNULL (GI_GLOBAL_DAILY_LIMIT, 0)  GI_GLOBAL_DAILY_LIMIT " _
                    + " , ISNULL (GI_GLOBAL_MONTHLY_LIMIT, 0)  GI_GLOBAL_MONTHLY_LIMIT" _
                    + " , GI_POINTS_LEVEL1" _
                    + " , GI_POINTS_LEVEL2" _
                    + " , GI_POINTS_LEVEL3" _
                    + " , GI_POINTS_LEVEL4" _
                    + " , PTC_NAME" _
                    + " , GI_AWARD_ON_PROMOBOX" _
                    + " , ISNULL (GI_TEXT_ON_PROMOBOX, '')  GI_TEXT_ON_PROMOBOX" _
                    + " , ISNULL(GI_VIP, 0) GI_VIP" _
               + " FROM   GIFTS" _
               + " LEFT   JOIN POINTS_TO_CREDITS ON PTC_POINTS_TO_CREDITS_ID = GI_POINTS_TO_CREDITS_ID" _
              + " WHERE   GI_GIFT_ID = " + Item.gift_id.ToString()

    _data_table = GUI_GetTableUsingSQL(_sql_query, 5000)
    If IsNothing(_data_table) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    If _data_table.Rows.Count() <> 1 Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    Item.name = NullTrim(_data_table.Rows(0).Item("GI_NAME"))
    Item.type = _data_table.Rows(0).Item("GI_TYPE")
    Item.available = _data_table.Rows(0).Item("GI_AVAILABLE")
    'Item.points = _data_table.Rows(0).Item("GI_POINTS")
    Item.conversion_to_nrc = _data_table.Rows(0).Item("_GI_CONVERSION_TO_NRC")
    Item.current_stock = _data_table.Rows(0).Item("GI_CURRENT_STOCK")
    Item.request_counter = _data_table.Rows(0).Item("GI_REQUEST_COUNTER")
    Item.delivery_counter = _data_table.Rows(0).Item("GI_DELIVERY_COUNTER")
    Item.description = _data_table.Rows(0).Item("GI_DESCRIPTION")

    Item.account_daily_limit = _data_table.Rows(0).Item("GI_ACCOUNT_DAILY_LIMIT")
    Item.account_monthly_limit = _data_table.Rows(0).Item("GI_ACCOUNT_MONTHLY_LIMIT")
    Item.global_daily_limit = _data_table.Rows(0).Item("GI_GLOBAL_DAILY_LIMIT")
    Item.global_monthly_limit = _data_table.Rows(0).Item("GI_GLOBAL_MONTHLY_LIMIT")

    'if points for level = -1 it means that the gift it's not available for that level in the DB it's saved as NULL
    If IsDBNull(_data_table.Rows(0).Item("GI_POINTS_LEVEL1")) Then
      Item.points_level1 = -1
    Else
      Item.points_level1 = _data_table.Rows(0).Item("GI_POINTS_LEVEL1")
    End If

    If IsDBNull(_data_table.Rows(0).Item("GI_POINTS_LEVEL2")) Then
      Item.points_level2 = -1
    Else
      Item.points_level2 = _data_table.Rows(0).Item("GI_POINTS_LEVEL2")
    End If

    If IsDBNull(_data_table.Rows(0).Item("GI_POINTS_LEVEL3")) Then
      Item.points_level3 = -1
    Else
      Item.points_level3 = _data_table.Rows(0).Item("GI_POINTS_LEVEL3")
    End If

    If IsDBNull(_data_table.Rows(0).Item("GI_POINTS_LEVEL4")) Then
      Item.points_level4 = -1
    Else
      Item.points_level4 = _data_table.Rows(0).Item("GI_POINTS_LEVEL4")
    End If

    If IsDBNull(_data_table.Rows(0).Item("GI_SMALL_RESOURCE_ID")) Then
      Item.icon = Nothing
      Item.small_resource_id = Nothing
    Else
      Item.small_resource_id = _data_table.Rows(0).Item("GI_SMALL_RESOURCE_ID")
      If Not GUI_BeginSQLTransaction(_sql_tx) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If
      Item.icon = LoadImage(Item.small_resource_id.Value, _sql_tx)
      Call GUI_EndSQLTransaction(_sql_tx, False)
    End If

    If IsDBNull(_data_table.Rows(0).Item("GI_LARGE_RESOURCE_ID")) Then
      Item.image = Nothing
      Item.large_resource_id = Nothing
    Else
      Item.large_resource_id = _data_table.Rows(0).Item("GI_LARGE_RESOURCE_ID")
      If Not GUI_BeginSQLTransaction(_sql_tx) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If
      Item.image = LoadImage(Item.large_resource_id.Value, _sql_tx)
      Call GUI_EndSQLTransaction(_sql_tx, False)
    End If

    If IsDBNull(_data_table.Rows(0).Item("PTC_NAME")) Then
      Item.change_table_name = ""
    Else
      Item.change_table_name = _data_table.Rows(0).Item("PTC_NAME")
    End If

    ' JMM 30-APR-2014
    ' Redeemable on PromoBOX check
    Item.award_on_promobox = _data_table.Rows(0).Item("GI_AWARD_ON_PROMOBOX")
    ' Name on PromoBOX
    Item.text_on_promobox = _data_table.Rows(0).Item("GI_TEXT_ON_PROMOBOX")

    ' JBP 07-APR-2014: Vip
    Item.is_vip = _data_table.Rows(0).Item("GI_VIP")

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

  End Function ' ReadGift

  '----------------------------------------------------------------------------
  ' PURPOSE : Deletes a gift object from DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - GiftId
  '         - Context
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - CONFIGURATION_OK
  '     - CONFIGURATION_ERROR_DB
  '
  ' NOTES :

  Private Function DeleteGift(ByVal GiftId As Integer, _
                              ByVal SmallResourceId As Nullable(Of Long), _
                              ByVal LargeResourceId As Nullable(Of Long), _
                              ByVal Context As Integer) As Integer
    Dim _sql_query As String
    Dim _sql_tx As System.Data.SqlClient.SqlTransaction = Nothing

    ' Steps
    '     - Check dependecies with other tables (OPERATIONS???)
    '     - Remove the gift item from the GIFTS table

    '     - Check dependecies with other tables (OPERATIONS???) 
    '       return ENUM_STATUS.STATUS_DEPENDENCIES
    '
    ' <<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    '     - Remove the gift item from the GIFTS table
    Try
      If Not GUI_BeginSQLTransaction(_sql_tx) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      _sql_query = "DELETE   GIFTS" _
                 + " WHERE   GI_GIFT_ID = " + GiftId.ToString()

      If Not GUI_SQLExecuteNonQuery(_sql_query, _sql_tx) Then
        ' Rollback  
        GUI_EndSQLTransaction(_sql_tx, False)

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      If SmallResourceId.HasValue Then
        If Not WSI.Common.WKTResources.Delete(SmallResourceId, _sql_tx) Then
          ' Rollback  
          GUI_EndSQLTransaction(_sql_tx, False)

          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If
      End If

      If LargeResourceId.HasValue Then
        If Not WSI.Common.WKTResources.Delete(LargeResourceId, _sql_tx) Then
          ' Rollback  
          GUI_EndSQLTransaction(_sql_tx, False)

          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If
      End If

      ' Commit
      If Not GUI_EndSQLTransaction(_sql_tx, True) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

    Catch ex As Exception
      ' Rollback  
      GUI_EndSQLTransaction(_sql_tx, False)

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

  End Function ' DeleteGift

  '----------------------------------------------------------------------------
  ' PURPOSE : Adds a gift object into DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - Item
  '         - Context
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - CONFIGURATION_OK
  '     - CONFIGURATION_ERROR_DB
  '
  ' NOTES :

  Private Function InsertGift(ByVal Item As TYPE_GIFT, _
                              ByVal Context As Integer) As Integer
    Dim _sql_query As String
    Dim _sql_tx As System.Data.SqlClient.SqlTransaction = Nothing
    Dim _data_table As DataTable
    Dim _sql_command As SqlCommand
    Dim _rc As Integer

    ' Steps
    '     - Check whether there is no other gift with the same name 
    '     - Insert the gift item to the GIFTS table

    _rc = 0

    Try
      '     - Check whether there is no other gift with the same name 
      _sql_query = "SELECT   COUNT (*)" _
                  + " FROM   GIFTS" _
                 + " WHERE   GI_NAME = @pName "

      _sql_command = New SqlCommand(_sql_query)
      _sql_command.Parameters.Add("@pName", SqlDbType.NVarChar).Value = Item.name
      _data_table = GUI_GetTableUsingCommand(_sql_command, 10)

      If Not IsNothing(_data_table) Then
        If _data_table.Rows.Count() > 0 Then
          If _data_table.Rows(0).Item(0) > 0 Then
            Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
          End If
        End If
      End If

      If Not GUI_BeginSQLTransaction(_sql_tx) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      SaveImage(Item.large_resource_id, Item.image, _sql_tx)
      SaveImage(Item.small_resource_id, Item.icon, _sql_tx)

      _sql_query = "INSERT INTO GIFTS ( GI_NAME" _
                                  + " , GI_TYPE" _
                                  + " , GI_AVAILABLE" _
                                  + " , GI_CONVERSION_TO_NRC" _
                                  + " , GI_CURRENT_STOCK" _
                                  + " , GI_REQUEST_COUNTER" _
                                  + " , GI_DELIVERY_COUNTER" _
                                  + " , GI_SMALL_RESOURCE_ID " _
                                  + " , GI_LARGE_RESOURCE_ID " _
                                  + " , GI_DESCRIPTION " _
                                  + " , GI_ACCOUNT_DAILY_LIMIT " _
                                  + " , GI_ACCOUNT_MONTHLY_LIMIT " _
                                  + " , GI_GLOBAL_DAILY_LIMIT " _
                                  + " , GI_GLOBAL_MONTHLY_LIMIT " _
                                  + " , GI_POINTS_LEVEL1 " _
                                  + " , GI_POINTS_LEVEL2 " _
                                  + " , GI_POINTS_LEVEL3 " _
                                  + " , GI_POINTS_LEVEL4 " _
                                  + " , GI_AWARD_ON_PROMOBOX " _
                                  + " , GI_TEXT_ON_PROMOBOX " _
                                  + " , GI_VIP " _
                                  + " )" _
                           + " VALUES ( @pName" _
                                  + " , @pType" _
                                  + " , @pAviable" _
                                  + " , @pConversionToNrc" _
                                  + " , @pCurrentStock " _
                                  + " , 0 " _
                                  + " , 0 " _
                                  + " , @pSmallResourceId " _
                                  + " , @pLargeResourceId " _
                                  + " , @pDescription " _
                                  + " , @pAccountDailyLimit " _
                                  + " , @pAccountMonthlyLimit " _
                                  + " , @pGlobalDailyLimit " _
                                  + " , @pGlobalMonthlyLimit " _
                                  + " , @pPointsLevel1 " _
                                  + " , @pPointsLevel2 " _
                                  + " , @pPointsLevel3 " _
                                  + " , @pPointsLevel4 " _
                                  + " , @pAwardOnPromoBOX " _
                                  + " , @pTextOnPromoBOX " _
                                  + " , @pVip " _
                                  + " ) "

      _sql_command = New SqlCommand(_sql_query, _sql_tx.Connection, _sql_tx)
      _sql_command.Parameters.Add("@pName", SqlDbType.NVarChar).Value = Item.name
      _sql_command.Parameters.Add("@pType", SqlDbType.Int).Value = Item.type
      _sql_command.Parameters.Add("@pAviable", SqlDbType.Bit).Value = Item.available
      '_sql_command.Parameters.Add("@pPoints", SqlDbType.Decimal).Value = Item.points
      _sql_command.Parameters.Add("@pConversionToNrc", SqlDbType.Decimal).Value = Item.conversion_to_nrc
      _sql_command.Parameters.Add("@pCurrentStock", SqlDbType.Int).Value = Item.current_stock

      If Item.small_resource_id.HasValue Then
        _sql_command.Parameters.Add("@pSmallResourceId", SqlDbType.BigInt).Value = Item.small_resource_id
      Else
        _sql_command.Parameters.Add("@pSmallResourceId", SqlDbType.BigInt).Value = DBNull.Value
      End If

      If Item.large_resource_id.HasValue Then
        _sql_command.Parameters.Add("@pLargeResourceId", SqlDbType.BigInt).Value = Item.large_resource_id
      Else
        _sql_command.Parameters.Add("@pLargeResourceId", SqlDbType.BigInt).Value = DBNull.Value
      End If

      _sql_command.Parameters.Add("@pDescription", SqlDbType.NVarChar).Value = Item.description

      _sql_command.Parameters.Add("@pAccountDailyLimit", SqlDbType.Int).Value = Item.account_daily_limit
      _sql_command.Parameters.Add("@pAccountMonthlyLimit", SqlDbType.Int).Value = Item.account_monthly_limit
      _sql_command.Parameters.Add("@pGlobalDailyLimit", SqlDbType.Int).Value = Item.global_daily_limit
      _sql_command.Parameters.Add("@pGlobalMonthlyLimit", SqlDbType.Int).Value = Item.global_monthly_limit

      '''if points for level = -1 it means that the gift it's not available for that level in the DB it's saved as NULL
      If Item.points_level1 <> -1 Then
        _sql_command.Parameters.Add("@pPointsLevel1", SqlDbType.Money).Value = Item.points_level1
      Else
        _sql_command.Parameters.Add("@pPointsLevel1", SqlDbType.Money).Value = DBNull.Value
      End If

      If Item.points_level2 <> -1 Then
        _sql_command.Parameters.Add("@pPointsLevel2", SqlDbType.Money).Value = Item.points_level2
      Else
        _sql_command.Parameters.Add("@pPointsLevel2", SqlDbType.Money).Value = DBNull.Value
      End If

      If Item.points_level3 <> -1 Then
        _sql_command.Parameters.Add("@pPointsLevel3", SqlDbType.Money).Value = Item.points_level3
      Else
        _sql_command.Parameters.Add("@pPointsLevel3", SqlDbType.Money).Value = DBNull.Value
      End If

      If Item.points_level4 <> -1 Then
        _sql_command.Parameters.Add("@pPointsLevel4", SqlDbType.Money).Value = Item.points_level4
      Else
        _sql_command.Parameters.Add("@pPointsLevel4", SqlDbType.Money).Value = DBNull.Value
      End If

      _sql_command.Parameters.Add("@pAwardOnPromoBOX", SqlDbType.Bit).Value = Item.award_on_promobox
      _sql_command.Parameters.Add("@pTextOnPromoBOX", SqlDbType.NVarChar, 50).Value = Item.text_on_promobox

      _sql_command.Parameters.Add("@pVip", SqlDbType.Bit).Value = Item.is_vip

      _rc = _sql_command.ExecuteNonQuery()

      If _rc <> 1 Then
        ' Rollback  
        GUI_EndSQLTransaction(_sql_tx, False)

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      ' Commit
      If Not GUI_EndSQLTransaction(_sql_tx, True) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

    Catch ex As Exception
      ' Rollback  
      GUI_EndSQLTransaction(_sql_tx, False)

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

  End Function ' InsertGift

  '----------------------------------------------------------------------------
  ' PURPOSE : Updates the given gift object into the DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - Item
  '         - Context
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - CONFIGURATION_OK
  '     - CONFIGURATION_ERROR_DB
  '
  ' NOTES :

  Private Function UpdateGift(ByVal Item As TYPE_GIFT, _
                              ByVal Context As Integer) As Integer
    Dim _sql_query As String
    Dim _sql_tx As System.Data.SqlClient.SqlTransaction = Nothing
    Dim _data_table As DataTable
    Dim _sql_command As SqlCommand
    Dim _rc As Integer

    ' Steps
    '     - Check whether there is no other gift with the same name 
    '     - Update the gift's data 

    _rc = 0

    Try
      '   - Check whether there is no other gift with the same name 
      _sql_query = "SELECT   COUNT (*)" _
                  + " FROM   GIFTS" _
                 + " WHERE   GI_NAME    =  @pName " _
                   + " AND   GI_GIFT_ID <> @pGiftId "

      _sql_command = New SqlCommand(_sql_query)
      _sql_command.Parameters.Add("@pName", SqlDbType.NVarChar).Value = Item.name
      _sql_command.Parameters.Add("@pGiftId", SqlDbType.BigInt).Value = Item.gift_id
      _data_table = GUI_GetTableUsingCommand(_sql_command, 10)

      If Not IsNothing(_data_table) Then
        If _data_table.Rows.Count() > 0 Then
          If _data_table.Rows(0).Item(0) > 0 Then
            Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
          End If
        End If
      End If

      '   - Update the gift's data 
      If Not GUI_BeginSQLTransaction(_sql_tx) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      SaveImage(Item.large_resource_id, Item.image, _sql_tx)
      SaveImage(Item.small_resource_id, Item.icon, _sql_tx)

      _sql_query = "UPDATE   GIFTS" _
                   + " SET   GI_NAME                   = @pName" _
                      + "  , GI_TYPE                   = @pType" _
                      + "  , GI_AVAILABLE              = @pAviable" _
                      + "  , GI_CONVERSION_TO_NRC      = @pConversionToNrc" _
                      + "  , GI_CURRENT_STOCK          = @pCurrentStock " _
                      + "  , GI_SMALL_RESOURCE_ID      = @pSmallResourceId " _
                      + "  , GI_LARGE_RESOURCE_ID      = @pLargeResourceId " _
                      + "  , GI_DESCRIPTION            = @pDescription " _
                      + "  , GI_ACCOUNT_DAILY_LIMIT    = @pAccountDailyLimit " _
                      + "  , GI_ACCOUNT_MONTHLY_LIMIT  = @pAccountMonthlyLimit " _
                      + "  , GI_GLOBAL_DAILY_LIMIT     = @pGlobalDailyLimit " _
                      + "  , GI_GLOBAL_MONTHLY_LIMIT   = @pGlobalMonthlyLimit " _
                      + "  , GI_POINTS_LEVEL1          = @pPointsLevel1 " _
                      + "  , GI_POINTS_LEVEL2          = @pPointsLevel2 " _
                      + "  , GI_POINTS_LEVEL3          = @pPointsLevel3 " _
                      + "  , GI_POINTS_LEVEL4          = @pPointsLevel4 " _
                      + "  , GI_AWARD_ON_PROMOBOX      = @pAwardOnPromoBOX " _
                      + "  , GI_TEXT_ON_PROMOBOX       = @pTextOnPromoBOX " _
                      + "  , GI_VIP                    = @pVip " _
                 + " WHERE   GI_GIFT_ID           = @pGiftId "

      _sql_command = New SqlCommand(_sql_query, _sql_tx.Connection, _sql_tx)
      _sql_command.Parameters.Add("@pName", SqlDbType.NVarChar).Value = Item.name
      _sql_command.Parameters.Add("@pType", SqlDbType.Int).Value = Item.type
      _sql_command.Parameters.Add("@pAviable", SqlDbType.Bit).Value = Item.available
      '_sql_command.Parameters.Add("@pPoints", SqlDbType.Decimal).Value = Item.points
      _sql_command.Parameters.Add("@pConversionToNrc", SqlDbType.Decimal).Value = Item.conversion_to_nrc
      _sql_command.Parameters.Add("@pCurrentStock", SqlDbType.Int).Value = Item.current_stock

      If Item.small_resource_id.HasValue Then
        _sql_command.Parameters.Add("@pSmallResourceId", SqlDbType.BigInt).Value = Item.small_resource_id
      Else
        _sql_command.Parameters.Add("@pSmallResourceId", SqlDbType.BigInt).Value = DBNull.Value
      End If

      If Item.large_resource_id.HasValue Then
        _sql_command.Parameters.Add("@pLargeResourceId", SqlDbType.BigInt).Value = Item.large_resource_id
      Else
        _sql_command.Parameters.Add("@pLargeResourceId", SqlDbType.BigInt).Value = DBNull.Value
      End If

      _sql_command.Parameters.Add("@pDescription", SqlDbType.NVarChar).Value = Item.description
      _sql_command.Parameters.Add("@pAccountDailyLimit", SqlDbType.Int).Value = Item.account_daily_limit
      _sql_command.Parameters.Add("@pAccountMonthlyLimit", SqlDbType.Int).Value = Item.account_monthly_limit
      _sql_command.Parameters.Add("@pGlobalDailyLimit", SqlDbType.Int).Value = Item.global_daily_limit
      _sql_command.Parameters.Add("@pGlobalMonthlyLimit", SqlDbType.Int).Value = Item.global_monthly_limit

      'if points for level = -1 it means that the gift it's not available for that level in the DB it's saved as NULL
      If Item.points_level1 <> -1 Then
        _sql_command.Parameters.Add("@pPointsLevel1", SqlDbType.Money).Value = Item.points_level1
      Else
        _sql_command.Parameters.Add("@pPointsLevel1", SqlDbType.Money).Value = DBNull.Value
      End If

      If Item.points_level2 <> -1 Then
        _sql_command.Parameters.Add("@pPointsLevel2", SqlDbType.Money).Value = Item.points_level2
      Else
        _sql_command.Parameters.Add("@pPointsLevel2", SqlDbType.Money).Value = DBNull.Value
      End If

      If Item.points_level3 <> -1 Then
        _sql_command.Parameters.Add("@pPointsLevel3", SqlDbType.Money).Value = Item.points_level3
      Else
        _sql_command.Parameters.Add("@pPointsLevel3", SqlDbType.Money).Value = DBNull.Value
      End If

      If Item.points_level4 <> -1 Then
        _sql_command.Parameters.Add("@pPointsLevel4", SqlDbType.Money).Value = Item.points_level4
      Else
        _sql_command.Parameters.Add("@pPointsLevel4", SqlDbType.Money).Value = DBNull.Value
      End If

      _sql_command.Parameters.Add("@pAwardOnPromoBOX", SqlDbType.Bit).Value = Item.award_on_promobox
      _sql_command.Parameters.Add("@pTextOnPromoBOX", SqlDbType.NVarChar, 50).Value = Item.text_on_promobox

      _sql_command.Parameters.Add("@pVip", SqlDbType.Bit).Value = Item.is_vip

      _sql_command.Parameters.Add("@pGiftId", SqlDbType.BigInt).Value = Item.gift_id
      _rc = _sql_command.ExecuteNonQuery()

      If _rc <> 1 Then
        ' Rollback  
        GUI_EndSQLTransaction(_sql_tx, False)

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      ' Commit
      If Not GUI_EndSQLTransaction(_sql_tx, True) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

    Catch ex As Exception
      ' Rollback  
      GUI_EndSQLTransaction(_sql_tx, False)
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
  End Function ' UpdateGift

#End Region ' Private Functions

End Class
