'------------------------------------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'------------------------------------------------------------------------------------------------
'
' MODULE NAME : cls_money_laundering_configuration.vb
'
' DESCRIPTION : 
'
' REVISION HISTORY:
' 
' Date         Author Description
' -----------  ------ ---------------------------------------------------------------------------
' 
' 
' 28-OCT-2013  ACM    Fixed Bug #WIG-322 Wrong auditor code.
'

#Region " Imports "

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports WSI.Common
Imports WSI.Common.OperationsSchedule
Imports System.Text
Imports System.Data.SqlClient
Imports GUI_Controls.Misc

#End Region ' Imports

Public Class CLS_MONEY_LAUNDERING_CONFIG
  Inherits CLASS_BASE

#Region " Members "

  Private m_enabled As Boolean
  Private m_base_amount As Double
  Private m_base_name As String
  Private m_name As String

  Public Class LimitsConfig

    Private m_limit As Double
    Private m_message As String
    Private m_limit_show_message As Boolean
    Private m_warning_limit As Double
    Private m_warning_message As String
    Private m_dont_allow_mb As Boolean
    Private m_permission_to_exeed_limit As ENUM_AML_REQUEST_PERMISSION_TO_EXCEED_LIMIT

    Public Property Limit() As Double
      Get
        Return Me.m_limit
      End Get
      Set(ByVal value As Double)
        Me.m_limit = value
      End Set
    End Property
    Public Property Message() As String
      Get
        Return Me.m_message
      End Get
      Set(ByVal value As String)
        m_message = value
      End Set
    End Property
    Public Property WarningLimit() As Double
      Get
        Return m_warning_limit
      End Get
      Set(ByVal value As Double)
        m_warning_limit = value
      End Set
    End Property
    Public Property ShowReportMessage() As Boolean
      Get
        Return m_limit_show_message
      End Get
      Set(ByVal value As Boolean)
        m_limit_show_message = value
      End Set
    End Property
    Public Property WarningMessage() As String
      Get
        Return m_warning_message
      End Get
      Set(ByVal value As String)
        m_warning_message = value
      End Set
    End Property
    Public Property DontAllowMB() As Boolean
      Get
        Return m_dont_allow_mb
      End Get
      Set(ByVal value As Boolean)
        m_dont_allow_mb = value
      End Set
    End Property

    Public Property PermissionToExeedLimit() As ENUM_AML_REQUEST_PERMISSION_TO_EXCEED_LIMIT
      Get
        Return m_permission_to_exeed_limit
      End Get
      Set(ByVal value As ENUM_AML_REQUEST_PERMISSION_TO_EXCEED_LIMIT)
        m_permission_to_exeed_limit = value
      End Set
    End Property

    Public Function Duplicate() As LimitsConfig
      Dim _clone As New LimitsConfig
      _clone.m_limit = Me.m_limit
      _clone.m_message = Me.m_message
      _clone.m_warning_limit = Me.m_warning_limit
      _clone.m_warning_message = Me.m_warning_message
      _clone.m_dont_allow_mb = Me.m_dont_allow_mb
      _clone.m_limit_show_message = Me.m_limit_show_message
      _clone.m_permission_to_exeed_limit = Me.m_permission_to_exeed_limit

      Return _clone
    End Function

    Public Shared Sub FillAuditorData(ByRef AuditorData As CLASS_AUDITOR_DATA _
                                      , ByRef LimitsData As LimitsConfig _
                                      , ByVal OperationTypeNlsId As Int32 _
                                      , ByVal LimitNlsId As Int32 _
                                      , ByVal WarningNlsId As Int32 _
                                      , ByVal ActivateWarningNlsId As Int32)
      Dim _str_warning_message As String
      Dim _yes_text As String
      Dim _no_text As String

      _yes_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(278)
      _no_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)

      '' [Winnings | Recharge].Limit = Value
      Call AuditorData.SetField(0, LimitsData.Limit, GLB_NLS_GUI_PLAYER_TRACKING.GetString(OperationTypeNlsId) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(LimitNlsId))

      '' [Winnings | Recharge].Message = Value
      Call AuditorData.SetField(0, LimitsData.Message, GLB_NLS_GUI_PLAYER_TRACKING.GetString(OperationTypeNlsId) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(LimitNlsId) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1428))

      '' [Winnings | Recharge].ShowMessage = Value
      Call AuditorData.SetField(0, IIf(LimitsData.ShowReportMessage, _yes_text, _no_text) _
                                , GLB_NLS_GUI_PLAYER_TRACKING.GetString(OperationTypeNlsId) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(LimitNlsId) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2715))

      If LimitsData.WarningLimit > 0 Then
        '' [Winnings | Recharge].Activate [identification|report] warning = Yes
        Call AuditorData.SetField(0, _yes_text, GLB_NLS_GUI_PLAYER_TRACKING.GetString(OperationTypeNlsId) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(ActivateWarningNlsId))

        '' [Winnings | Recharge].Activate [identification|report] Message
        Call AuditorData.SetField(0, LimitsData.WarningLimit, GLB_NLS_GUI_PLAYER_TRACKING.GetString(OperationTypeNlsId) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(WarningNlsId) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2389))
      Else
        '' [Winnings | Recharge].Activate [identification|report] warning = Yes
        Call AuditorData.SetField(0, _no_text, GLB_NLS_GUI_PLAYER_TRACKING.GetString(OperationTypeNlsId) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(ActivateWarningNlsId))

        '' [Winnings | Recharge].Activate [identification|report] Message = String.Empty
        Call AuditorData.SetField(0, AUDIT_NONE_STRING, GLB_NLS_GUI_PLAYER_TRACKING.GetString(OperationTypeNlsId) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(WarningNlsId) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2389))
      End If

      If String.IsNullOrEmpty(LimitsData.WarningMessage) Then
        _str_warning_message = AUDIT_NONE_STRING
      Else
        _str_warning_message = LimitsData.WarningMessage
      End If

      '' [Winnings | Recharge].Warning.Message
      Call AuditorData.SetField(0, _str_warning_message, GLB_NLS_GUI_PLAYER_TRACKING.GetString(OperationTypeNlsId) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(WarningNlsId) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1428))

      '' [Winnings | Recharge]."Not allowed MB recharge when warning" = [ Yes | No ]
      Call AuditorData.SetField(0 _
                                , IIf(LimitsData.DontAllowMB, _yes_text, _no_text) _
                                , GLB_NLS_GUI_PLAYER_TRACKING.GetString(OperationTypeNlsId) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(LimitNlsId) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2390))

      Select Case (LimitsData.PermissionToExeedLimit)
        Case ENUM_AML_REQUEST_PERMISSION_TO_EXCEED_LIMIT.Never
          _str_warning_message = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1287)
        Case ENUM_AML_REQUEST_PERMISSION_TO_EXCEED_LIMIT.OnlyWhenCross
          _str_warning_message = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2849)
        Case ENUM_AML_REQUEST_PERMISSION_TO_EXCEED_LIMIT.Always
          _str_warning_message = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2850)
      End Select
      Call AuditorData.SetField(0, _str_warning_message, _
                                GLB_NLS_GUI_PLAYER_TRACKING.GetString(OperationTypeNlsId) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(LimitNlsId) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2852))

    End Sub


    Public Shared Sub FillAuditorData(ByRef AuditorData As CLASS_AUDITOR_DATA _
                                    , ByRef LimitsData As LimitsConfig _
                                    , ByVal OperationTypeNlsId As Int32 _
                                    , ByVal LimitNlsId As Int32 _
                                    , ByVal ActivateWarningNlsId As Int32)
      Dim _str_warning_message As String
      Dim _yes_text As String
      Dim _no_text As String

      _yes_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(278)
      _no_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)

      If LimitsData.Limit > 0 Then
        '' [Winnings | Recharge].Activate [identification|report] warning = Yes
        Call AuditorData.SetField(0, _yes_text, GLB_NLS_GUI_PLAYER_TRACKING.GetString(OperationTypeNlsId) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(ActivateWarningNlsId))

        '' [Winnings | Recharge].Limit = Value
        Call AuditorData.SetField(0, LimitsData.Limit, GLB_NLS_GUI_PLAYER_TRACKING.GetString(OperationTypeNlsId) & _
                               "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(LimitNlsId))
      Else
        '' [Winnings | Recharge].Activate [identification|report] warning = Yes
        Call AuditorData.SetField(0, _no_text, GLB_NLS_GUI_PLAYER_TRACKING.GetString(OperationTypeNlsId) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(ActivateWarningNlsId))

        '' [Winnings | Recharge].Activate [identification|report] Message = String.Empty
        Call AuditorData.SetField(0, AUDIT_NONE_STRING, GLB_NLS_GUI_PLAYER_TRACKING.GetString(OperationTypeNlsId) & _
                                 "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(LimitNlsId))
      End If

      If String.IsNullOrEmpty(LimitsData.Message) Then
        _str_warning_message = AUDIT_NONE_STRING
      Else
        _str_warning_message = LimitsData.Message
      End If

      '' [Winnings | Recharge].Message = Value
      Call AuditorData.SetField(0, _str_warning_message, GLB_NLS_GUI_PLAYER_TRACKING.GetString(OperationTypeNlsId) & _
                               "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(LimitNlsId) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1428))
    End Sub
  End Class

  Private m_recharge_identification As LimitsConfig = New LimitsConfig()
  Private m_recharge_report As LimitsConfig = New LimitsConfig()
  Private m_recharge_maximum As LimitsConfig = New LimitsConfig()
  Private m_prize_identification As LimitsConfig = New LimitsConfig()
  Private m_prize_report As LimitsConfig = New LimitsConfig()
  Private m_prize_maximum As LimitsConfig = New LimitsConfig()
  Private m_has_beneficiary As Boolean
  Private m_expiration_days As Int64

#End Region

#Region " Properties "

  Public Property Enabled() As Boolean
    Get
      Return m_enabled
    End Get
    Set(ByVal value As Boolean)
      m_enabled = value
    End Set
  End Property

  Public Property BaseAmount() As Double
    Get
      Return m_base_amount
    End Get
    Set(ByVal value As Double)
      m_base_amount = value
    End Set
  End Property

  Public Property BaseName() As String
    Get
      Return m_base_name
    End Get
    Set(ByVal value As String)
      m_base_name = value
    End Set
  End Property

  Public ReadOnly Property RechargeIdentification() As LimitsConfig
    Get
      Return Me.m_recharge_identification
    End Get
  End Property

  Public Property HasBeneficiary() As Boolean
    Get
      Return m_has_beneficiary
    End Get
    Set(ByVal value As Boolean)
      m_has_beneficiary = value
    End Set
  End Property

  Public Property ExpirationDays() As Int64
    Get
      Return m_expiration_days
    End Get
    Set(ByVal value As Int64)
      m_expiration_days = value
    End Set
  End Property

  Public ReadOnly Property RechargeMaximum() As LimitsConfig
    Get
      Return Me.m_recharge_maximum
    End Get
  End Property

  Public ReadOnly Property RechargeReport() As LimitsConfig
    Get
      Return Me.m_recharge_report
    End Get
  End Property

  Public ReadOnly Property PrizeIdentification() As LimitsConfig
    Get
      Return Me.m_prize_identification
    End Get
  End Property

  Public ReadOnly Property PrizeReport() As LimitsConfig
    Get
      Return Me.m_prize_report
    End Get
  End Property

  Public ReadOnly Property PrizeMaximum() As LimitsConfig
    Get
      Return Me.m_prize_maximum
    End Get
  End Property

  Public Property Name() As String
    Get
      Return m_name
    End Get
    Set(ByVal value As String)
      m_name = value
    End Set
  End Property

#End Region

#Region " Overrides "

  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _operation_nls As Int32
    Dim _yes_text As String
    Dim _no_text As String

    _yes_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(278)
    _no_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)

    ' TODO: AUDIT CODE
    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_CASHIER_CONFIGURATION)

    Call _auditor_data.SetName(0, "", GLB_NLS_GUI_PLAYER_TRACKING.GetString(2948, Accounts.getAMLName())) '

    Call _auditor_data.SetField(0, IIf(Me.Enabled, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)), _
                                 GLB_NLS_GUI_PLAYER_TRACKING.GetString(2386, Accounts.getAMLName()))

    'Name
    Call _auditor_data.SetField(0, Me.Name, GLB_NLS_GUI_PLAYER_TRACKING.GetString(362))

    '' Limits.SMVGDF
    Call _auditor_data.SetField(0, Me.BaseAmount, GLB_NLS_GUI_PLAYER_TRACKING.GetString(282) & "." & GeneralParam.GetString("AntiMoneyLaundering", "BaseName"))

    ''Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(0), Me.BaseName) '' Not Editable field

    '' Recharge
    _operation_nls = 233     '' Recharge

    '' Recharge Identification 
    '' (2387)  "Identification limit"
    '' (2413)  "Identification Warning"
    '' (2388)  "Activate Identification Warning"
    Call LimitsConfig.FillAuditorData(_auditor_data, RechargeIdentification, _operation_nls, 2387, 2413, 2388)

    '' Recharge Maximum
    '' (2387)  "Maximum limit"
    '' (2413)  "Maximum Warning"
    Call LimitsConfig.FillAuditorData(_auditor_data, RechargeMaximum, _operation_nls, 2883, 2882)

    '' Recharge Report
    '' (2391)  "Report limit"
    '' (2414)  "Report Warning"
    '' (2392)  "Activate Report Warning"
    Call LimitsConfig.FillAuditorData(_auditor_data, RechargeReport, _operation_nls, 2391, 2414, 2392)

    '' Winnnings
    _operation_nls = 1592     '' Winnings

    '' Winnings Identification
    '' (2387)  "Identification limit"
    '' (2413)   "Identification Warning"
    '' (2388)  "Activate Identification Warning"
    Call LimitsConfig.FillAuditorData(_auditor_data, PrizeIdentification, _operation_nls, 2387, 2413, 2388)

    '' Recharge Maximum
    '' (2387)  "Maximum limit"
    '' (2413)  "Maximum Warning"
    Call LimitsConfig.FillAuditorData(_auditor_data, PrizeMaximum, _operation_nls, 2883, 2882)

    '' Winnings Report
    '' (2391)  "Report limit"
    '' (2414)   "Report Warning"
    '' (2392)  "Activate Report Warning"
    Call LimitsConfig.FillAuditorData(_auditor_data, PrizeReport, _operation_nls, 2391, 2414, 2392)

    If m_has_beneficiary Then
      Call _auditor_data.SetField(0, _yes_text, GLB_NLS_GUI_PLAYER_TRACKING.GetString(282) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5085))
    Else
      Call _auditor_data.SetField(0, _no_text, GLB_NLS_GUI_PLAYER_TRACKING.GetString(282) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5085))
    End If

    ' Expiration days
    Call _auditor_data.SetField(0, ExpirationDays, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6053))

    Return _auditor_data

  End Function

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Return ENUM_STATUS.STATUS_NOT_SUPPORTED

  End Function

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Return ENUM_STATUS.STATUS_NOT_SUPPORTED

  End Function

  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As ENUM_STATUS

    Using _db_trx As New DB_TRX()

      _rc = Me.Read(_db_trx.SqlTransaction)

    End Using ' _db_trx

    Return _rc

  End Function

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _rc As ENUM_STATUS

    _rc = ENUM_STATUS.STATUS_ERROR

    Using _db_trx As New DB_TRX()

      _rc = Me.Update(_db_trx.SqlTransaction)

      If _rc <> ENUM_STATUS.STATUS_OK Then

        Return _rc
      End If

      If _db_trx.Commit() Then

        Return ENUM_STATUS.STATUS_OK
      End If

    End Using ' _db_trx

    Return _rc
  End Function

  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim _clone As CLS_MONEY_LAUNDERING_CONFIG
    _clone = New CLS_MONEY_LAUNDERING_CONFIG()

    _clone.m_base_amount = Me.m_base_amount
    _clone.m_base_name = Me.m_base_name
    _clone.m_name = Me.m_name

    _clone.m_enabled = Me.m_enabled

    _clone.m_prize_identification = Me.m_prize_identification.Duplicate()
    _clone.m_prize_report = Me.m_prize_report.Duplicate()
    _clone.m_prize_maximum = Me.m_prize_maximum.Duplicate()
    _clone.m_recharge_identification = Me.m_recharge_identification.Duplicate()
    _clone.m_recharge_report = Me.m_recharge_report.Duplicate()
    _clone.m_recharge_maximum = Me.m_recharge_maximum.Duplicate()
    _clone.m_has_beneficiary = Me.m_has_beneficiary
    _clone.m_expiration_days = Me.m_expiration_days

    Return _clone

  End Function

#End Region

#Region " Public Functions "

  Public Sub New()

    ' Implementar el constructor

  End Sub ' New

#End Region

#Region " Private Function "

  Private Function Read(ByVal Trx As SqlTransaction) As ENUM_STATUS

    Dim _gp As GeneralParam.Dictionary

    'Populate General Params dictionary with the current database values
    _gp = GeneralParam.Dictionary.Create(Trx)

    Try

      Me.m_enabled = _gp.GetBoolean("AntiMoneyLaundering", "Enabled")
      Me.m_name = _gp.GetString("AntiMoneyLaundering", "Name")
      Me.m_base_amount = _gp.GetDecimal("AntiMoneyLaundering", "BaseAmount")
      Me.m_base_name = _gp.GetString("AntiMoneyLaundering", "BaseName")

      Me.m_recharge_identification.Limit = _gp.GetDecimal("AntiMoneyLaundering", "Recharge.Identification.Limit")
      Me.m_recharge_identification.Message = _gp.GetString("AntiMoneyLaundering", "Recharge.Identification.Message")
      Me.m_recharge_identification.WarningLimit = _gp.GetDecimal("AntiMoneyLaundering", "Recharge.Identification.Warning.Limit")
      Me.m_recharge_identification.WarningMessage = _gp.GetString("AntiMoneyLaundering", "Recharge.Identification.Warning.Message")
      Me.m_recharge_identification.DontAllowMB = _gp.GetBoolean("AntiMoneyLaundering", "Recharge.Identification.DontAllowMB")

      Me.m_recharge_report.Limit = _gp.GetDecimal("AntiMoneyLaundering", "Recharge.Report.Limit")
      Me.m_recharge_report.Message = _gp.GetString("AntiMoneyLaundering", "Recharge.Report.Message")
      Me.m_recharge_report.ShowReportMessage = _gp.GetString("AntiMoneyLaundering", "Recharge.Report.ShowMessage")
      Me.m_recharge_report.WarningLimit = _gp.GetDecimal("AntiMoneyLaundering", "Recharge.Report.Warning.Limit")
      Me.m_recharge_report.WarningMessage = _gp.GetString("AntiMoneyLaundering", "Recharge.Report.Warning.Message")
      Me.m_recharge_report.DontAllowMB = _gp.GetBoolean("AntiMoneyLaundering", "Recharge.Report.DontAllowMB")

      Me.m_recharge_maximum.Limit = _gp.GetDecimal("AntiMoneyLaundering", "Recharge.MaxAllowed.Limit")
      Me.m_recharge_maximum.Message = _gp.GetString("AntiMoneyLaundering", "Recharge.MaxAllowed.Message")

      Me.m_prize_identification.Limit = _gp.GetDecimal("AntiMoneyLaundering", "Prize.Identification.Limit")
      Me.m_prize_identification.Message = _gp.GetString("AntiMoneyLaundering", "Prize.Identification.Message")
      Me.m_prize_identification.WarningLimit = _gp.GetDecimal("AntiMoneyLaundering", "Prize.Identification.Warning.Limit")
      Me.m_prize_identification.WarningMessage = _gp.GetString("AntiMoneyLaundering", "Prize.Identification.Warning.Message")

      Me.m_prize_report.Limit = _gp.GetDecimal("AntiMoneyLaundering", "Prize.Report.Limit")
      Me.m_prize_report.Message = _gp.GetString("AntiMoneyLaundering", "Prize.Report.Message")
      Me.m_prize_report.ShowReportMessage = _gp.GetString("AntiMoneyLaundering", "Prize.Report.ShowMessage")
      Me.m_prize_report.WarningLimit = _gp.GetDecimal("AntiMoneyLaundering", "Prize.Report.Warning.Limit")
      Me.m_prize_report.WarningMessage = _gp.GetString("AntiMoneyLaundering", "Prize.Report.Warning.Message")

      Me.m_prize_maximum.Limit = _gp.GetDecimal("AntiMoneyLaundering", "Prize.MaxAllowed.Limit")
      Me.m_prize_maximum.Message = _gp.GetString("AntiMoneyLaundering", "Prize.MaxAllowed.Message")

      Me.m_recharge_report.PermissionToExeedLimit = _gp.GetInt32("AntiMoneyLaundering", "Recharge.Report.RequestPermissionToExceedLimit")
      Me.m_prize_report.PermissionToExeedLimit = _gp.GetInt32("AntiMoneyLaundering", "Prize.Report.RequestPermissionToExceedLimit")

      Me.m_has_beneficiary = _gp.GetBoolean("AntiMoneyLaundering", "HasBeneficiary")

      Me.m_expiration_days = _gp.GetInt64("AntiMoneyLaundering", "ExternalControl.FileExpirationDays")

      Return ENUM_STATUS.STATUS_OK

    Catch ex As Exception
      Log.Exception(ex)
    End Try

    Return ENUM_STATUS.STATUS_ERROR

  End Function

  Private Function Update(ByVal Trx As SqlTransaction) As ENUM_STATUS
    Dim _dic_key As Dictionary(Of String, String)

    Try

      _dic_key = New Dictionary(Of String, String)

      _dic_key.Add("Enabled", Me.ToStr(Me.m_enabled))
      _dic_key.Add("Name", Me.m_name)
      _dic_key.Add("BaseAmount", Me.ToStr(Me.m_base_amount))
      _dic_key.Add("BaseName", Me.m_base_name)

      _dic_key.Add("Recharge.Identification.Limit", Me.ToStr(Me.m_recharge_identification.Limit))
      _dic_key.Add("Recharge.Identification.Message", Me.m_recharge_identification.Message)
      _dic_key.Add("Recharge.Identification.Warning.Limit", Me.ToStr(Me.m_recharge_identification.WarningLimit))
      _dic_key.Add("Recharge.Identification.Warning.Message", Me.m_recharge_identification.WarningMessage)
      _dic_key.Add("Recharge.Identification.DontAllowMB", Me.ToStr(Me.m_recharge_identification.DontAllowMB))

      _dic_key.Add("Recharge.Report.Limit", Me.ToStr(Me.m_recharge_report.Limit))
      _dic_key.Add("Recharge.Report.Message", Me.m_recharge_report.Message)
      _dic_key.Add("Recharge.Report.ShowMessage", Me.ToStr(Me.m_recharge_report.ShowReportMessage))
      _dic_key.Add("Recharge.Report.Warning.Limit", Me.ToStr(Me.m_recharge_report.WarningLimit))
      _dic_key.Add("Recharge.Report.Warning.Message", Me.m_recharge_report.WarningMessage)
      _dic_key.Add("Recharge.Report.DontAllowMB", Me.ToStr(Me.m_recharge_report.DontAllowMB))
      _dic_key.Add("Recharge.Report.RequestPermissionToExceedLimit", Me.ToStr(Me.m_recharge_report.PermissionToExeedLimit))

      _dic_key.Add("Recharge.MaxAllowed.Limit", Me.ToStr(Me.m_recharge_maximum.Limit))
      _dic_key.Add("Recharge.MaxAllowed.Message", Me.m_recharge_maximum.Message)

      _dic_key.Add("Prize.Identification.Limit", Me.ToStr(Me.m_prize_identification.Limit))
      _dic_key.Add("Prize.Identification.Message", Me.m_prize_identification.Message)
      _dic_key.Add("Prize.Identification.Warning.Limit", Me.ToStr(Me.m_prize_identification.WarningLimit))
      _dic_key.Add("Prize.Identification.Warning.Message", Me.m_prize_identification.WarningMessage)

      _dic_key.Add("Prize.Report.Limit", Me.ToStr(Me.m_prize_report.Limit))
      _dic_key.Add("Prize.Report.Message", Me.m_prize_report.Message)
      _dic_key.Add("Prize.Report.ShowMessage", Me.ToStr(Me.m_prize_report.ShowReportMessage))
      _dic_key.Add("Prize.Report.Warning.Limit", Me.ToStr(Me.m_prize_report.WarningLimit))
      _dic_key.Add("Prize.Report.Warning.Message", Me.m_prize_report.WarningMessage)
      _dic_key.Add("Prize.Report.RequestPermissionToExceedLimit", Me.ToStr(Me.m_prize_report.PermissionToExeedLimit))

      _dic_key.Add("Prize.MaxAllowed.Limit", Me.ToStr(Me.m_prize_maximum.Limit))
      _dic_key.Add("Prize.MaxAllowed.Message", Me.m_prize_maximum.Message)


      For Each _dic As KeyValuePair(Of String, String) In _dic_key

        If Not DB_GeneralParam_Update("AntiMoneyLaundering", _dic.Key, _dic.Value, Trx) Then
          Return ENUM_STATUS.STATUS_ERROR
        End If

      Next

      If Not DB_GeneralParam_Update("AntiMoneyLaundering", "HasBeneficiary", Me.ToStr(Me.m_has_beneficiary), Trx) Then
        Return ENUM_STATUS.STATUS_ERROR
      End If

      If Not DB_GeneralParam_Update("AntiMoneyLaundering", "ExternalControl.FileExpirationDays", Me.ToStr(Me.m_expiration_days), Trx) Then
        Return ENUM_STATUS.STATUS_ERROR
      End If

      Return ENUM_STATUS.STATUS_OK

    Catch ex As Exception
      Log.Exception(ex)
    End Try

    Return ENUM_STATUS.STATUS_ERROR

  End Function

  Private Function ToStr(ByVal Value As Boolean) As String

    If Value Then
      Return "1"
    End If

    Return "0"

  End Function

  Private Function ToStr(ByVal Value As Double) As String

    Return GUI_LocalNumberToDBNumber(Value.ToString())

  End Function

#End Region

End Class

