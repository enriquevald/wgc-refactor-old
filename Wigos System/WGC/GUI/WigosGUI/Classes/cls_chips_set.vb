
'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : cls_chips_set.vb
'
' DESCRIPTION : Chips Set class
' 
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 01-JUL-2014  DLL    Initial version   
' 10-JUL-2014  DCS    Add private functions to search and Validate items
'                     Modify functions to allow delete items 
' 09-FEB-2016  RGR    Task 5778: Group chips: Design changes in Group chips
' 19-FEB-2016  EOR    Task 9597: Edit Group Chips: Error to disabled Group of Chips 
' 22-FEB-2016  RGR    Product Backlog Item 9339:Sprint Review 18 Winions - Mex
' 01-APR-2016  EOR    Product Backlog Item 10936:Mesas (Fase 1): adjust chips form
' 09-JUN-2016  RAB    PBI 11755: Tables (Phase 1): Correction errors as a result of the backward compatibility of the chips group
' 28-JUN-2016  DHA    Bug 28431:WIGOS-3038 [Ticket #6429] does not recognize any new chips group
'--------------------------------------------------------------------

#Region "Imports"
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Data.SqlClient
Imports System.Text
Imports WSI.Common
#End Region ' Imports

Public Class CLS_CHIPS_SET
  Inherits CLASS_BASE

#Region "Enum"

  Private Enum ROWACTION
    NORMAL = 0
    MODIFIED = 1
    DELETED = 2
  End Enum

#End Region ' Enum

#Region "Constants"

  Public Const SQL_COLUMN_ID As Integer = 0
  Public Const SQL_COLUMN_DENOMINATION As Integer = 1
  Public Const SQL_COLUMN_COLOR As Integer = 2
  Public Const SQL_COLUMN_NAME As Integer = 3
  Public Const SQL_COLUMN_DRAWING As Integer = 4
  Public Const SQL_COLUMN_ISO_CODE As Integer = 5
  Public Const SQL_COLUMN_ALLOWED As Integer = 6
  Public Const SQL_COLUMN_CHIP_TYPE As Integer = 7
  Public Const SQL_COLUMN_ID_AUX As Integer = 8


  Private Const AUDIT_STATE_COLUMN_NAME As String = "ACTION"
  Private Const CHIPS_NUM_ROWS_AUDIT_MAX As Integer = 100

#End Region ' Constants 

#Region "Structures "

  Public Class CHIPS_SETS
    Public chips_id As Int32
    Public chip_type As FeatureChips.ChipType
    Public name As String
    Public enabled As Boolean
    Public iso_code As String
    Public chips_definition As DataTable

    Sub New()
      chips_id = 0
      chip_type = 0
      name = String.Empty
      enabled = False
      iso_code = String.Empty
      chips_definition = New DataTable()
    End Sub
  End Class

#End Region ' Structures

#Region "Members "
  Protected m_chips_set As New CHIPS_SETS
#End Region ' Members

#Region "Properties"
  Public Property ChipsSetsId() As Int32
    Get
      Return m_chips_set.chips_id
    End Get
    Set(ByVal value As Int32)
      m_chips_set.chips_id = value
    End Set
  End Property

  Public Property ChipType() As Integer
    Get
      Return m_chips_set.chip_type
    End Get
    Set(ByVal value As Integer)
      m_chips_set.chip_type = value
    End Set
  End Property

  Public Property Name() As String
    Get
      Return m_chips_set.name
    End Get
    Set(ByVal value As String)
      m_chips_set.name = value
    End Set
  End Property

  Public Property Enabled() As Boolean
    Get
      Return m_chips_set.enabled
    End Get
    Set(ByVal value As Boolean)
      m_chips_set.enabled = value
    End Set
  End Property

  Public Property IsoCode() As String
    Get
      Return m_chips_set.iso_code
    End Get
    Set(ByVal value As String)
      m_chips_set.iso_code = value
    End Set
  End Property

  Public Property ChipsDefinition() As DataTable
    Get
      Return m_chips_set.chips_definition
    End Get
    Set(ByVal value As DataTable)
      m_chips_set.chips_definition = value
    End Set
  End Property

#End Region ' Properties

#Region "Overrides"

  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA
    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _dt As DataTable
    Dim _str_sort As String
    Dim _dv As DataView
    Dim _action_value As ROWACTION
    Dim _yes_text As String
    Dim _no_text As String
    Dim _str_denomination As String
    Dim _color As Color
    Dim _chip_type_dic As Dictionary(Of FeatureChips.ChipType, String)

    _yes_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(278)
    _no_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)
    _str_sort = String.Empty
    _action_value = ROWACTION.NORMAL
    _dt = New DataTable()
    _str_denomination = String.Empty
    _color = New Color()
    _chip_type_dic = GetNamesChipType()

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_CASHIER_CONFIGURATION)

    Call _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(5059), " - " & Me.m_chips_set.name)
    Call _auditor_data.SetField(0, m_chips_set.name, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4399))

    Call _auditor_data.SetField(0, m_chips_set.chips_definition.ToString(), GLB_NLS_GUI_PLAYER_TRACKING.GetString(5062))
    Call _auditor_data.SetField(0, m_chips_set.iso_code, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2076))

    If m_chips_set.enabled = True Then
      Call _auditor_data.SetField(0, _yes_text, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4400))
    Else
      Call _auditor_data.SetField(0, _no_text, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4400))
    End If

    If Not Me.ChipsDefinition Is Nothing And Me.ChipsDefinition.Rows.Count > 0 Then

      _dt = Me.ChipsDefinition.Copy
      _dt.Columns.Add(AUDIT_STATE_COLUMN_NAME, Type.GetType("System.Int16"))

      ' sorting by ID
      _str_sort = "CH_CHIP_ID DESC"

      ' check row by state
      For Each _dr As DataRow In _dt.Rows
        Select Case _dr.RowState
          Case DataRowState.Added, DataRowState.Modified
            _dr(AUDIT_STATE_COLUMN_NAME) = ROWACTION.MODIFIED
          Case DataRowState.Deleted
            _dr.RejectChanges()
            _dr(AUDIT_STATE_COLUMN_NAME) = ROWACTION.DELETED
          Case Else
            _dr(AUDIT_STATE_COLUMN_NAME) = ROWACTION.NORMAL
        End Select
      Next

      ' sort new datatable
      _dv = _dt.DefaultView
      _dv.Sort = _str_sort
      _dt = _dv.ToTable()

      For Each _dr As DataRow In _dt.Rows

        _action_value = _dr(AUDIT_STATE_COLUMN_NAME)

        ' doesn't show denomination if is color
        If Me.ChipType <> FeatureChips.ChipType.COLOR Then
          _str_denomination = GUI_FormatCurrency(_dr(SQL_COLUMN_DENOMINATION))
        Else
          _color = Color.FromArgb(_dr(SQL_COLUMN_COLOR))
          _str_denomination = _color.ToString()
        End If

        Select Case _action_value
          Case ROWACTION.MODIFIED, ROWACTION.NORMAL

            If Me.ChipType <> FeatureChips.ChipType.COLOR Then
              If IsDBNull(_dr(SQL_COLUMN_DENOMINATION)) Then
                _auditor_data.SetField(0, AUDIT_NONE_STRING, _str_denomination & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4357))
              Else
                _auditor_data.SetField(0, GUI_FormatCurrency(_dr(SQL_COLUMN_DENOMINATION)), _str_denomination & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4357))
              End If
            End If

            If IsDBNull(_dr(SQL_COLUMN_ALLOWED)) Then
              _auditor_data.SetField(0, AUDIT_NONE_STRING, _str_denomination & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4400))
            Else
              If _dr(SQL_COLUMN_ALLOWED) Then
                _auditor_data.SetField(0, _yes_text, _str_denomination & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4400))
              Else
                _auditor_data.SetField(0, _no_text, _str_denomination & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4400))
              End If
            End If

            If IsDBNull(_dr(SQL_COLUMN_COLOR)) Then
              _auditor_data.SetField(0, AUDIT_NONE_STRING, _str_denomination & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1291))
            Else
              _auditor_data.SetField(0, Color.FromArgb(_dr(SQL_COLUMN_COLOR)).ToString(), _str_denomination & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1291))
            End If

            If IsDBNull(_dr(SQL_COLUMN_NAME)) Then
              _auditor_data.SetField(0, "", _str_denomination & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4399))
            Else
              _auditor_data.SetField(0, _dr(SQL_COLUMN_NAME), _str_denomination & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4399))
            End If

            If IsDBNull(_dr(SQL_COLUMN_DRAWING)) Then
              _auditor_data.SetField(0, "", _str_denomination & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6924))
            Else
              _auditor_data.SetField(0, _dr(SQL_COLUMN_DRAWING), _str_denomination & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6924))
            End If

          Case ROWACTION.DELETED
            If Me.ChipType <> FeatureChips.ChipType.COLOR Then
              _auditor_data.SetField(0, AUDIT_NONE_STRING, _str_denomination & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4357))
            End If
            _auditor_data.SetField(0, AUDIT_NONE_STRING, _str_denomination & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4400))
            _auditor_data.SetField(0, AUDIT_NONE_STRING, _str_denomination & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1291))
            _auditor_data.SetField(0, AUDIT_NONE_STRING, _str_denomination & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4399))
            _auditor_data.SetField(0, AUDIT_NONE_STRING, _str_denomination & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5075))
          Case Else
        End Select
      Next
    End If

    ' ID Chip Set = 0  is new group
    If Me.ChipsSetsId <> 0 Then
      For _idx_blanks As Integer = _auditor_data.FieldCount - 1 To CHIPS_NUM_ROWS_AUDIT_MAX
        Call _auditor_data.SetField(0, AUDIT_NONE_STRING, "Dummy")
      Next
    End If

    Return _auditor_data

  End Function ' AuditorData

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _num_rows_updated As Integer

    _num_rows_updated = 0
    Using _db_trx As New DB_TRX()

      _num_rows_updated = UpdateChips(_db_trx, Me.ChipsSetsId)

      If _num_rows_updated >= 1 Then
        _db_trx.Commit()
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
      ElseIf _num_rows_updated < 0 Then
        _db_trx.Rollback()
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      ElseIf _num_rows_updated = 0 Then
        _db_trx.Rollback()
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
      End If

    End Using

  End Function ' DB_Delete

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Return ChipsSets_Insert(SqlCtx)

  End Function ' DB_Insert

  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Me.ChipsSetsId = ObjectId

    Return ReadChipsSets()

  End Function ' DB_Read

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _result As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    _result = ChipsSets_Update(SqlCtx)

    Return _result
  End Function ' DB_Update

  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim _target As CLS_CHIPS_SET

    _target = New CLS_CHIPS_SET

    _target.Name = Me.Name
    _target.Enabled = Me.Enabled
    _target.ChipsDefinition = Me.ChipsDefinition.Copy()
    _target.ChipsSetsId = Me.ChipsSetsId
    _target.IsoCode = Me.IsoCode
    _target.ChipType = Me.ChipType

    Return _target

  End Function ' Duplicate

#End Region ' Overrides

#Region "Private Functions "

  '----------------------------------------------------------------------------
  ' PURPOSE: Reads an object from the database.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES:
  Private Function GetTableChips() As DataTable
    Dim _sb As StringBuilder
    _sb = New StringBuilder()

    _sb.AppendLine("       SELECT  CH.CH_CHIP_ID,")
    _sb.AppendLine("               CH.CH_DENOMINATION,")
    _sb.AppendLine("               CH.CH_COLOR,")
    _sb.AppendLine("               CH.CH_NAME,")
    _sb.AppendLine("               CH.CH_DRAWING,")
    _sb.AppendLine("               CH.CH_ISO_CODE,")
    _sb.AppendLine("               CH.CH_ALLOWED,")
    _sb.AppendLine("               CH.CH_CHIP_TYPE,")
    _sb.AppendLine("               CH.CH_CHIP_ID AS CH_CHIP_ID_AUX")
    _sb.AppendLine("         FROM  CHIPS_SETS_CHIPS AS CSC")
    _sb.AppendLine("   INNER JOIN  CHIPS AS CH   ON CSC.CSC_CHIP_ID = CH.CH_CHIP_ID")
    _sb.AppendLine("        WHERE  CSC.CSC_SET_ID = " + Me.m_chips_set.chips_id.ToString())
    _sb.AppendLine("     ORDER BY  CH.CH_DENOMINATION DESC")

    Return GUI_GetTableUsingSQL(_sb.ToString(), 5000)
  End Function

  Private Function ReadChipsSets() As Integer

    Dim _sb As StringBuilder
    Dim _table As DataTable
    Try
      _sb = New StringBuilder()

      _sb.AppendLine("   SELECT CHS_CHIP_SET_ID,")
      _sb.AppendLine("          CHS_ISO_CODE,")
      _sb.AppendLine("          CHS_NAME,")
      _sb.AppendLine("          CHS_ALLOWED,")
      _sb.AppendLine("          CHS_CHIP_TYPE")
      _sb.AppendLine("    FROM  CHIPS_SETS")
      _sb.AppendLine("   WHERE  CHS_CHIP_SET_ID = " + Me.m_chips_set.chips_id.ToString())

      _table = GUI_GetTableUsingSQL(_sb.ToString(), 5000)

      If IsNothing(_table) Then
        Return ENUM_STATUS.STATUS_ERROR
      End If

      If _table.Rows.Count() > 1 Then
        Return ENUM_STATUS.STATUS_ERROR
      End If

      If _table.Rows.Count() = 1 Then
        Me.ChipType = IIf(IsDBNull(_table.Rows(0).Item("chs_chip_type")), Nothing, _table.Rows(0).Item("chs_chip_type"))
        Me.IsoCode = IIf(IsDBNull(_table.Rows(0).Item("chs_iso_code")), String.Empty, _table.Rows(0).Item("chs_iso_code"))
        Me.Name = IIf(IsDBNull(_table.Rows(0).Item("chs_name")), String.Empty, _table.Rows(0).Item("chs_name"))
        Me.Enabled = IIf(IsDBNull(_table.Rows(0).Item("chs_allowed")), False, _table.Rows(0).Item("chs_allowed"))
      Else
        Me.ChipType = FeatureChips.ChipType.COLOR
      End If

      Me.m_chips_set.chips_definition = GetTableChips()

      Return ENUM_STATUS.STATUS_OK

    Catch ex As Exception
      Return ENUM_STATUS.STATUS_ERROR
    End Try
  End Function ' ReadChipsSets

  '----------------------------------------------------------------------------
  ' PURPOSE: Insert the given object into the database.
  '
  ' PARAMS:
  '   - INPUT:  None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES:
  Private Function ChipsSets_Insert(ByVal Context As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _db_count As Integer
    Dim _num_rows_updated As Integer
    Dim _param As SqlClient.SqlParameter
    Dim _sb As StringBuilder
    Dim _operations_flags As Int32

    _db_count = 0
    _sb = New StringBuilder()

    Try

      Using _db_trx As New DB_TRX()

        ' INSERT Chips Sets
        _sb.AppendLine("INSERT INTO   CHIPS_SETS ")
        _sb.AppendLine("            ( CHS_CHIP_TYPE ")
        _sb.AppendLine("            , CHS_ISO_CODE ")
        _sb.AppendLine("            , CHS_NAME ")
        _sb.AppendLine("            , CHS_ALLOWED ")
        _sb.AppendLine("            , CHS_ALLOWED_OPERATIONS_FLAGS ")
        _sb.AppendLine("            ) ")
        _sb.AppendLine("   VALUES")
        _sb.AppendLine("            ( @pChipType ")
        _sb.AppendLine("            , @pIsoCode ")
        _sb.AppendLine("            , @pName ")
        _sb.AppendLine("            , @pAllowed ")
        _sb.AppendLine("            , @pAllowedOperationsFlags ")
        _sb.AppendLine("            ) ")
        _sb.AppendLine(" SET @pChipsSetsID = SCOPE_IDENTITY() ")

        Using _cmd As New SqlClient.SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _cmd.Parameters.Add("@pChipType", SqlDbType.Int).Value = m_chips_set.chip_type
          _cmd.Parameters.Add("@pIsoCode", SqlDbType.NVarChar).Value = IIf(String.IsNullOrEmpty(m_chips_set.iso_code), Cage.CHIPS_COLOR, m_chips_set.iso_code)
          _cmd.Parameters.Add("@pName", SqlDbType.NVarChar).Value = m_chips_set.name
          _cmd.Parameters.Add("@pAllowed", SqlDbType.Bit).Value = m_chips_set.enabled

          If m_chips_set.chip_type = FeatureChips.ChipType.RE Then
            _operations_flags = FeatureChips.ChipsSets.ChipSet.AllowedSetOperations.PURCHASE Or FeatureChips.ChipsSets.ChipSet.AllowedSetOperations.SALE
          Else
            _operations_flags = 0
          End If

          _cmd.Parameters.Add("@pAllowedOperationsFlags", SqlDbType.Int).Value = _operations_flags

          _param = _cmd.Parameters.Add("@pChipsSetsID", SqlDbType.Int)
          _param.Direction = ParameterDirection.Output

          _num_rows_updated = _cmd.ExecuteNonQuery()
        End Using


        If _num_rows_updated <> 1 Then
          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

        _num_rows_updated = UpdateChips(_db_trx, _param.Value)

        If _num_rows_updated > 0 Then
          ' Min one row
          _db_trx.Commit()
        Else
          _db_trx.Rollback()
          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

      End Using '_db_trx

    Catch ex As Exception
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

  End Function ' ChipsSets_Insert

  '----------------------------------------------------------------------------
  ' PURPOSE: Insert the given object into the database.
  '
  ' PARAMS:
  '   - INPUT:  Chip ID
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - number of rows updated
  '
  ' NOTES:
  Private Function UpdateChips(ByVal DbTrx As DB_TRX, ByVal ChipSetId As Integer) As Integer
    Dim _sb_update As StringBuilder
    Dim _sb_insert As StringBuilder
    Dim _sb_delete As StringBuilder
    Dim _num_rows As Integer

    _sb_update = New StringBuilder()
    _sb_insert = New StringBuilder()
    _sb_delete = New StringBuilder()
    _num_rows = 0

    _sb_insert.AppendLine("INSERT INTO   CHIPS (")
    _sb_insert.AppendLine("              CH_CHIP_TYPE")
    _sb_insert.AppendLine("            , CH_ISO_CODE")
    _sb_insert.AppendLine("            , CH_DENOMINATION")
    _sb_insert.AppendLine("            , CH_ALLOWED")
    _sb_insert.AppendLine("            , CH_COLOR")
    _sb_insert.AppendLine("            , CH_NAME")
    _sb_insert.AppendLine("            , CH_DRAWING)")
    _sb_insert.AppendLine("VALUES(")
    _sb_insert.AppendLine("              @pChipType")
    _sb_insert.AppendLine("            , @pIsoCode")
    _sb_insert.AppendLine("            , @pDenomination")
    _sb_insert.AppendLine("            , @pAllowed")
    _sb_insert.AppendLine("            , @pColor")
    _sb_insert.AppendLine("            , @pName")
    _sb_insert.AppendLine("            , @pDrawing)")
    _sb_insert.AppendLine("   SET @pChipID = SCOPE_IDENTITY()")
    _sb_insert.AppendLine(" ")
    _sb_insert.AppendLine("INSERT INTO   CHIPS_SETS_CHIPS (")
    _sb_insert.AppendLine("              CSC_CHIP_ID ")
    _sb_insert.AppendLine("            , CSC_SET_ID)")
    _sb_insert.AppendLine("VALUES(")
    _sb_insert.AppendLine("    @pChipID")
    _sb_insert.AppendLine("   ,@pChipSetId)")

    _sb_update.AppendLine("UPDATE  CHIPS        ")
    _sb_update.AppendLine(" SET     CH_CHIP_TYPE    = @pChipType     ")
    _sb_update.AppendLine("       , CH_ISO_CODE     = @pIsoCode     ")
    _sb_update.AppendLine("       , CH_DENOMINATION = @pDenomination    ")
    _sb_update.AppendLine("       , CH_ALLOWED      = @pAllowed    ")
    _sb_update.AppendLine("       , CH_COLOR        = @pColor    ")
    _sb_update.AppendLine("       , CH_NAME         = @pName    ")
    _sb_update.AppendLine("       , CH_DRAWING      = @pDrawing    ")
    _sb_update.AppendLine(" WHERE   CH_CHIP_ID      = @pID  ")

    _sb_delete.AppendLine("DELETE FROM CHIPS  ")
    _sb_delete.AppendLine("      WHERE CH_CHIP_ID  = @ChipDelID ")

    _sb_delete.AppendLine("DELETE FROM CHIPS_SETS_CHIPS")
    _sb_delete.AppendLine("   WHERE CSC_CHIP_ID = @ChipDelID ")

    Using _cmd_update As SqlCommand = New SqlCommand(_sb_update.ToString(), DbTrx.SqlTransaction.Connection, DbTrx.SqlTransaction)
      Using _cmd_insert As SqlCommand = New SqlCommand(_sb_insert.ToString(), DbTrx.SqlTransaction.Connection, DbTrx.SqlTransaction)
        Using _cmd_delete As SqlCommand = New SqlCommand(_sb_delete.ToString(), DbTrx.SqlTransaction.Connection, DbTrx.SqlTransaction)

          _cmd_update.Parameters.Add("@pChipType", SqlDbType.NVarChar).SourceColumn = "CH_CHIP_TYPE"
          _cmd_update.Parameters.Add("@pIsoCode", SqlDbType.NVarChar).SourceColumn = "CH_ISO_CODE"
          _cmd_update.Parameters.Add("@pDenomination", SqlDbType.Money).SourceColumn = "CH_DENOMINATION"
          _cmd_update.Parameters.Add("@pAllowed", SqlDbType.Bit).SourceColumn = "CH_ALLOWED"
          _cmd_update.Parameters.Add("@pColor", SqlDbType.Int).SourceColumn = "CH_COLOR"
          _cmd_update.Parameters.Add("@pName", SqlDbType.NVarChar).SourceColumn = "CH_NAME"
          _cmd_update.Parameters.Add("@pDrawing", SqlDbType.NVarChar).SourceColumn = "CH_DRAWING"
          _cmd_update.Parameters.Add("@pID", SqlDbType.BigInt).SourceColumn = "CH_CHIP_ID"

          _cmd_insert.Parameters.Add("@pChipSetId", SqlDbType.BigInt).Value = ChipSetId ' Param is group chips
          _cmd_insert.Parameters.Add("@pChipType", SqlDbType.NVarChar).SourceColumn = "CH_CHIP_TYPE"
          _cmd_insert.Parameters.Add("@pIsoCode", SqlDbType.NVarChar).SourceColumn = "CH_ISO_CODE"
          _cmd_insert.Parameters.Add("@pDenomination", SqlDbType.Money).SourceColumn = "CH_DENOMINATION"
          _cmd_insert.Parameters.Add("@pAllowed", SqlDbType.Bit).SourceColumn = "CH_ALLOWED"
          _cmd_insert.Parameters.Add("@pColor", SqlDbType.Int).SourceColumn = "CH_COLOR"
          _cmd_insert.Parameters.Add("@pName", SqlDbType.NVarChar).SourceColumn = "CH_NAME"
          _cmd_insert.Parameters.Add("@pDrawing", SqlDbType.NVarChar).SourceColumn = "CH_DRAWING"
          _cmd_insert.Parameters.Add("@pChipID", SqlDbType.BigInt).Direction = ParameterDirection.Output

          _cmd_delete.Parameters.Add("@ChipDelID", SqlDbType.BigInt).SourceColumn = "CH_CHIP_ID"

          Using _da As SqlDataAdapter = New SqlDataAdapter()
            _da.UpdateCommand = _cmd_update
            _da.InsertCommand = _cmd_insert
            _da.DeleteCommand = _cmd_delete

            If Not m_chips_set.chips_definition.GetChanges Is Nothing Then
              _num_rows = _da.Update(m_chips_set.chips_definition.GetChanges)
            End If

          End Using
        End Using
      End Using
    End Using

    Return _num_rows
  End Function ' UpdateChips

  ''' <summary>
  ''' Get last chip id any chip groups
  ''' </summary>
  ''' <param name="ChipId">Variable by reference to last Chip ID</param>
  ''' <returns>True or false depending on whether it has obtained result or not</returns>
  ''' <remarks></remarks>
  Public Function GetLastChipId(ByRef ChipId As Long) As Boolean
    Dim _sb As StringBuilder
    Dim _chip_id

    _sb = New StringBuilder()

    Using _db_trx As New DB_TRX()
      _sb.AppendLine("SELECT TOP 1 *          ")
      _sb.AppendLine("FROM CHIPS              ")
      _sb.AppendLine("ORDER BY CH_CHIP_ID DESC")
      Using _cmd As New SqlClient.SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
        _chip_id = _cmd.ExecuteScalar()
        If _chip_id IsNot Nothing And _chip_id IsNot DBNull.Value Then

          ChipId = CType(_chip_id, Int64)
          Return True
        Else
          Return False
        End If
      End Using
    End Using
  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE: Update the given object into the database.
  '
  ' PARAMS:
  '   - INPUT:  None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES:
  Private Function ChipsSets_Update(ByVal Context As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _str_sql As String
    Dim _num_rows_updated As Integer
    Dim _num_rows_chips_updated As Integer
    Dim _success As Boolean
    Try

      Using _db_trx As New DB_TRX()

        ' UPDATE Chips Sets
        _str_sql = "  UPDATE CHIPS_SETS                       " & _
                    "    SET CHS_NAME = @pName                " & _
                    "      , CHS_CHIP_TYPE = @pChipType       " & _
                    "      , CHS_ISO_CODE = @pIsoCode         " & _
                    "      , CHS_ALLOWED = @pAllowed          " & _
                    "  WHERE CHS_CHIP_SET_ID   = @pChipSetId  "

        Using _cmd As New SqlClient.SqlCommand(_str_sql, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _cmd.Parameters.Add("@pChipType", SqlDbType.Int).Value = m_chips_set.chip_type
          _cmd.Parameters.Add("@pIsoCode", SqlDbType.NVarChar).Value = m_chips_set.iso_code
          _cmd.Parameters.Add("@pName", SqlDbType.NVarChar).Value = m_chips_set.name
          _cmd.Parameters.Add("@pAllowed", SqlDbType.Bit).Value = m_chips_set.enabled
          _cmd.Parameters.Add("@pChipSetID", SqlDbType.BigInt).Value = m_chips_set.chips_id

          _num_rows_updated = _cmd.ExecuteNonQuery()

        End Using

        'UPDATE CHIPS

        _num_rows_chips_updated = UpdateChips(_db_trx, m_chips_set.chips_id)

        If _num_rows_chips_updated > 0 Or _num_rows_updated > 0 Then
          ' Min one row
          _db_trx.Commit()
          _success = True
        Else
          _db_trx.Rollback()
          _success = False
        End If

      End Using '_db_trx

      If Not _success Then

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If


    Catch ex As Exception
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

  End Function ' ChipsSets_Insert

  '----------------------------------------------------------------------------
  ' PURPOSE: Count num groups by Chip Type
  '
  ' PARAMS:
  '   - INPUT:  None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - Number of Chips type group
  '
  ' NOTES:
  Private Function CountNumGroupByChipType(ByVal ChipType As FeatureChips.ChipType) As Integer
    Dim _count As Int64
    Dim _sb As StringBuilder

    _sb = New StringBuilder()

    Using _db_trx As New DB_TRX()
      _sb.Append("   SELECT COUNT(CHS_NAME)")
      _sb.Append("    FROM  CHIPS_SETS")
      _sb.Append("   WHERE  CHS_CHIP_TYPE = @PCHIPTYPE")
      Using _cmd As New SqlClient.SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
        _cmd.Parameters.Add("@pChipType", SqlDbType.Int).Value = ChipType
        _count = _cmd.ExecuteScalar()
      End Using
    End Using

    Return _count
  End Function 'CountChipTypeColor

  ' PURPOSE : Read object (amount movements) from the DB and 
  '
  ' PARAMS :
  '     - INPUT :
  '         - Chip ID
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK  -> Not pending Movements
  '     - CONFIGURATION_ERROR_DB
  '     - STATUS_NOT_FOUND
  '     - CONFIGURATION_ERROR_DEPENDENCIES -> Pending Movements
  '
  ' NOTES :
  Public Function NotPendingMovements_Chips(ByVal chipID As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _sb As StringBuilder
    Dim _value As Object
    Dim _db_count As Integer

    _sb = New StringBuilder()

    Using _db_trx As New DB_TRX()

      _sb.AppendLine("      SELECT  count(*)")
      _sb.AppendLine("        FROM  ")
      _sb.AppendLine("           (   SELECT  CMD.cmd_MOVEMENT_ID    AS MOVEMENT_ID   ")
      _sb.AppendLine("                 FROM  CAGE_MOVEMENT_DETAILS CMD   ")
      _sb.AppendLine("           INNER JOIN  CAGE_PENDING_MOVEMENTS CGM ON CMD.CMD_MOVEMENT_ID = CGM.CPM_MOVEMENT_ID  ")
      _sb.AppendLine("                WHERE  CMD.cmd_chip_id= @pChipID ")
      _sb.AppendLine("            UNION ALL  ")
      _sb.AppendLine("               SELECT  CGM.CGM_MOVEMENT_ID AS MOVEMENT_ID  ")
      _sb.AppendLine("                 FROM  CAGE_MOVEMENTS  CGM  ")
      _sb.AppendLine("           INNER JOIN  CAGE_PENDING_MOVEMENTS CPM ON CGM.CGM_MOVEMENT_ID = CPM.CPM_MOVEMENT_ID   ")
      _sb.AppendLine("           INNER JOIN  CASHIER_MOVEMENTS CM ON CGM.CGM_cashier_session_id = CM.cm_session_id   ")
      _sb.AppendLine("                WHERE  CPM.CPM_USER_ID = -1  ")
      _sb.AppendLine("                  AND  CGM.CGM_CASHIER_SESSION_ID <> NULL ")
      _sb.AppendLine("                  AND  CM.cm_type IN (@pCloseSession, @pFillerOut) ")
      _sb.AppendLine("           )  Temp   ")

      Using _sql_cmd As New SqlClient.SqlCommand(_sb.ToString, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
        _sql_cmd.Parameters.Add("@pCloseSession", SqlDbType.Int).Value = CASHIER_MOVEMENT.CAGE_CLOSE_SESSION
        _sql_cmd.Parameters.Add("@pFillerOut", SqlDbType.Int).Value = CASHIER_MOVEMENT.CAGE_FILLER_OUT
        _sql_cmd.Parameters.Add("@pChipID", SqlDbType.Int).Value = chipID

        _value = _sql_cmd.ExecuteScalar()
      End Using
    End Using

    If _value IsNot Nothing And _value IsNot DBNull.Value Then
      _db_count = CInt(_value)
    Else
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    If _db_count > 0 Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DEPENDENCIES
    ElseIf _db_count < 0 Then
      Return ENUM_STATUS.STATUS_NOT_FOUND
    Else
      Return ENUM_STATUS.STATUS_OK
    End If


  End Function ' NotPendingMovements_Chips

  ' PURPOSE : Check if the chips sets is assigned in gambling table
  '
  ' PARAMS :
  '     - INPUT :
  '         - chipSetsID
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK  -> Not pending Movements
  '     - CONFIGURATION_ERROR_DB
  '     - STATUS_NOT_FOUND
  '     - CONFIGURATION_ERROR_DEPENDENCIES -> Pending Movements
  '
  ' NOTES :
  Public Function IsAssignedIn_GamingTable(ByVal chipSetsID As Integer, ByRef GamingTablessAsigned As List(Of String)) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _sb As StringBuilder
    Dim _gaming_tables_assigned As DataTable

    _sb = New StringBuilder()
    _gaming_tables_assigned = New DataTable()
    GamingTablessAsigned = Nothing

    Try

      Using _db_trx As New DB_TRX()

        _sb.AppendLine("      SELECT   DISTINCT (GT_NAME)    ")
        _sb.AppendLine("        FROM   GAMING_TABLE_CHIPS_SETS ")
        _sb.AppendLine("       INNER   JOIN GAMING_TABLES ON GTCS_GAMING_TABLE_ID = GT_GAMING_TABLE_ID ")
        _sb.AppendLine("       WHERE   GTCS_CHIPS_SET_ID = " & chipSetsID)
        _sb.AppendLine("       ORDER   BY GT_NAME ASC ")

        _gaming_tables_assigned = GUI_GetTableUsingSQL(_sb.ToString(), 500, , _db_trx.SqlTransaction)

      End Using

      If _gaming_tables_assigned.Rows.Count > 0 Then

        GamingTablessAsigned = New List(Of String)

        For Each _row As DataRow In _gaming_tables_assigned.Rows
          GamingTablessAsigned.Add(_row(0))
        Next

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_ALREADY_EXISTS
      ElseIf _gaming_tables_assigned.Rows.Count < 0 Then
        Return ENUM_STATUS.STATUS_ERROR
      Else
        Return ENUM_STATUS.STATUS_OK
      End If

    Catch ex As Exception
      Return ENUM_STATUS.STATUS_ERROR
    End Try

  End Function ' IsAssignedIn_GamingTable

#End Region ' Private Functions

#Region "Public Functions"

  Public Shared Function GetNamesChipType() As IDictionary(Of FeatureChips.ChipType, String)
    Dim _dic As Dictionary(Of FeatureChips.ChipType, String)

    _dic = New Dictionary(Of FeatureChips.ChipType, String)()

    _dic.Add(FeatureChips.ChipType.RE, FeatureChips.GetChipTypeDescription(CurrencyExchangeType.CASINO_CHIP_RE, Nothing))
    _dic.Add(FeatureChips.ChipType.NR, FeatureChips.GetChipTypeDescription(CurrencyExchangeType.CASINO_CHIP_NRE, Nothing))
    _dic.Add(FeatureChips.ChipType.COLOR, FeatureChips.GetChipTypeDescription(CurrencyExchangeType.CASINO_CHIP_COLOR, Nothing))

    Return _dic
  End Function 'GetNamesChipType

  Public Function ExistsChipsSets(ByVal groupID As Integer, ByVal chipName As String, ByVal chipIsoCode As String, ByVal chipType As FeatureChips.ChipType) As Boolean
    Dim _group_id As Int64
    Dim _sb As StringBuilder

    _sb = New StringBuilder()

    Using _db_trx As New DB_TRX()
      _sb.Append("   SELECT ISNULL(MIN(CHS_CHIP_SET_ID),0) ")
      _sb.Append("    FROM  CHIPS_SETS ")
      _sb.Append("   WHERE  CHS_NAME      = @pName ")
      _sb.Append("     AND  CHS_ISO_CODE = @pIsoCode ")
      _sb.Append("     AND  CHS_CHIP_TYPE = @pChipType ")

      Using _cmd As New SqlClient.SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
        _cmd.Parameters.Add("@pName", SqlDbType.NVarChar).Value = chipName
        _cmd.Parameters.Add("@pIsoCode", SqlDbType.NVarChar).Value = IIf(chipIsoCode.Equals("--"), Cage.CHIPS_COLOR, chipIsoCode)
        _cmd.Parameters.Add("@pChipType", SqlDbType.Int).Value = chipType

        _group_id = _cmd.ExecuteScalar()
      End Using
    End Using

    If _group_id = 0 _
        Or groupID = _group_id Then
      Return False
    End If

    Return True
  End Function 'ExistsChipsSets

  Public Function IsValidCreateChipSetType(ByVal ChipType As FeatureChips.ChipType) As Boolean
    Dim _count_groups As Integer
    Dim _max_chip_groups As Integer
    Dim _valid As Boolean

    _valid = True
    _count_groups = CountNumGroupByChipType(ChipType)
    _max_chip_groups = FeatureChips.NumMaxGroupsByChipType(ChipType)

    If _count_groups >= _max_chip_groups Then
      _valid = False
    End If

    Return _valid
  End Function 'IsValidCreateChipSetColor

  Public Function IsValidCreateChipSetValueOrNotValue(ByVal chipIsoCode As String, ByVal chipType As FeatureChips.ChipType) As Boolean
    Dim _count As Int16
    Dim _sb As StringBuilder

    _sb = New StringBuilder()

    Using _db_trx As New DB_TRX()
      _sb.Append("   SELECT COUNT(CHS_NAME) ")
      _sb.Append("     FROM CHIPS_SETS ")
      _sb.Append("    WHERE CHS_ISO_CODE = @pIsoCode ")
      _sb.Append("      AND CHS_CHIP_TYPE = @pChipType ")

      Using _cmd As New SqlClient.SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
        _cmd.Parameters.Add("@pIsoCode", SqlDbType.NVarChar).Value = IIf(chipIsoCode.Equals("--"), Cage.CHIPS_COLOR, chipIsoCode)
        _cmd.Parameters.Add("@pChipType", SqlDbType.Int).Value = chipType

        _count = _cmd.ExecuteScalar()
      End Using
    End Using

    If _count = 0 Then
      Return False
    End If

    Return True
  End Function 'IsValidCreateChipSetValueOrNotValue

#End Region ' Public Functions

End Class
