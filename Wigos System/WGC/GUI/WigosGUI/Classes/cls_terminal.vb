'-------------------------------------------------------------------
' Copyright � 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : cls_terminal.vb
'
' DESCRIPTION : terminal class for user edition
'              
' REVISION HISTORY :
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 05-NOV-2010  MBF    Initial version
' 17-NOV-2011  MPO    Add new fields: Denomination, MultipleDenomination, Program, Payout theorical.
' 05-JUL-2012  JML    Add new field: Tax_registration
' 02-AUG-2012  MPO    Added field "sas_flags" --> extended_meter, credits_play_mode_sas
' 19-MAR-2013  LEM    Fixed Bug #652: Database error if terminal name include character ('). 
' 05-JUL-2013  JCOR   Added new properties and ENUM_CONTRACT_TYPE.
' 23-OCT-2013  DRV    Added TITO properties
' 21-DEC-2013  RCI    When changing TERMINAL NAME, change the related CASHIER TERMINAL NAME if exists.
' 09-JAN-2014  ICS    Close open play sessions when terminal is retired
' 31-MAR-2014  DHA    Added SAS Authentification params
' 11-JUL-2014  XIT    Added Multilanguage Support
' 13-AUG-2014  LEM    Added new fields "MachineId", "Position", "Top Award", "Max Bet", "Number Lines"
'                     Join "Denomination" and "Multidenomination" into only field "Denomination" type Int32 references PK terminal_denominations table
' 29-SEP-2014  FJC    Add SAS_FLAG: SPECIAL_PROGRESSIVE_METER  
' 06-OCT-2014  LEM    Call TerminalReport.ForceRefresh on update/insert.
' 02-DEC-2014  DLL    Added new column GameTheme
' 24-MAR-2015  AMF    Added menu item 'Meter Delta'
' 13-APR-2015  FJC    BackLog Item 940
' 12-MAY-2015  MPO    WIG-2325
' 18-MAY-2015  DLL    WIG-2361: relation terminal-game lost when duplicate terminal.
' 19-MAY-2015  DHA & DDM    Refactoring code for WIG-2361
' 20-MAY-2015  DHA    Added new terminals fields to Insert Statement
' 29-MAY-2015  FAV    Added 'Communication type' and 'smib configuration'(Id and name for pulses type)
' 05-JUN-2015  AMF    Product Backlog Item 1936 
' 17-JUN-2015  DCS    Fixed Bug #WIG-2459: Don't work insert terminals
' 29-JUN-2015  SGB    Backlog item 2161: Added check for enabled machines
' 07-JUL-2015  SGB    Backlog item 2671: Undo last changes: "Added check for enabled machines" & input menssage is bloqued
' 05-AUG-2015  DCS    Fixed Bug WIG-2624 can not insert new terminals 3GS
' 25-AUG-2015  SGB    Fixed Bug 3982: Don't see label blocked
' 08-SEP-2015  FOS    Backlog Item 3709
' 20-NOV-2015  RGR    Product Backlog Item 6451:Garcia River-03 add field Equity (%).
' 25-AUG-2015  SGB    Fixed Bug 5468: Updated CT_NAME for all updated terminals when te_name is updated.
' 20-NOV-2015  RGR    Product Backlog Item 6451:Garcia River-03 add field Equity (%).
' 07-JAN-2016  DDS    Product Backlog Item 8145:Floor Dual Currency: Definici�n de moneda en m�quina
' 26-JAN-2016  ETP    Fixed Bug 8439: Addded comunication type SAS IGT Poker for audit.
' 02-FEB-2016  RAB    Product Backlog Item 8857:Bally 3.5 BCDs: GUI: Modificaci�n de la pantalla de edici�n de terminales
' 08-FEB-2016  RAB    Bug 9017:Errores en Edici�n de Terminales: no permite editar
' 30-MAY-2016  FAV    Bug 13778: Remove warning about communication type equal to none
' 09-MAY-2016  RAB    PBI 12627:AFIP Client: Recovery accounting denomination
' 01-JUL-2016  LTC    Bug 13716:% Equity - Is registered with String Not Found
' 17-AUG-2016  FAV    Product Backlog Item 16835: Add SAS Host Id
' 18-OCT-2016  FAV    PBI 18240: eBox- 'Min. denomination' field added for TITO mode
' 19-OCT-2016  PDM    PBI 18245:eBox - Ticket Amount Not Multiple of Machine Denomination: GP - monto m�nimo aceptado por la m�quina (GUI y BD)
' 20-OCT-2016  AMF    Bug 19358:WigosGUI: Al realizar modificaciones en la configuraci�n de un terminal la auditoria no esta mostrando los datos correctos
' 26-OCT-2016  FAV    PBI 19597:eBox - Ticket Amount Not Multiple of Machine Denomination: Review
' 22-NOV-2016  PDM    PBI 20566:Cage Str Team 1 - Cambios Review Sprint 32
' 16-JAN-2017  RAB    Bug 22724: Field type of counters not shown the information correctly.
' 16-JAN-2016  JMM    PBI 19744:Pago autom�tico de handpays
' 16-FEB-2016  FAV    PBI 24711: Machine on reserve.
' 09-MAY-2017  ATB    PBI 27209:Terminals Editor screen - Add new dates
' 08-JAN-2018  LTC    PBI 31209:WIGOS-7488 AGG - EGM manufacturing date
' 01-MAR-2018  DPC    Bug 31769:[WIGOS-8193]: [Ticket #12514] Reporte de terminales No muestra el comando 1B activado V03.06.0035
'-------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports WSI.Common

Public Class CLASS_TERMINAL
  Inherits CLASS_BASE

#Region " Constants "

  ' Fields length
  Public Const MAX_NAME_LEN As Integer = 50

  Private Const MASK_MACHINE_BLOCKED_MANUALLY As Integer = &H4

#End Region ' Constants

#Region " Enums "

  Public Enum ENUM_CONTRACT_TYPE
    NONE = 0
    PURCHASE = 1
    SALE = 2
    FIXED_INCOME = 3
    OTHERS = 4
  End Enum

  Public Enum ENUM_RETIREMENT_PLAY_SESSIONS_STATUS
    NONE = 0         ' There is no open play session when terminal is retired
    REMAIN_OPEN = 1  ' Play session remains open after retirement
    CLOSE = 2        ' Play sessions have been closed after retirement 
  End Enum

#End Region ' Enums 

#Region " Structures "

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_TERMINAL
    Public terminal_id As Int64
    Public master_id As Int64
    Public change_id As Int64
    Public base_name As String
    Public status As Integer
    Public name As String
    Public external_id As String
    Public blocked As Boolean
    Public provider_id As String
    Public build_id As Int32
    Public client_id As Int32
    Public vendor_id As String
    Public type As Integer
    Public type_name As String
    Public retirement_date As Date
    Public retirement_requested As Date
    Public denomination As String
    Public program As String
    Public tax_registration As String
    Public theoretical_payout As Decimal
    Public bank_id As Integer
    Public bank_name As String
    Public area_name As String
    Public game_type As Integer
    Public game_type_name As String
    Public floor_id As String
    Public activation_date As Date
    Public extended_meter As ENUM_SAS_FLAGS
    Public credits_play_mode_sas As ENUM_SAS_FLAGS
    Public serial_number As String
    Public cabinet_type As String
    Public jackpot_contribution_pct As Decimal
    Public contract_type As String
    Public contract_id As String
    Public order_number As String
    Public tito_configuration_type As Boolean
    Public tito_allowed_cashable_emission As Boolean
    Public tito_allowed_promotional_emission As Boolean
    Public tito_allowed_redemption As Boolean
    Public tito_max_allowed_ti As Decimal
    Public tito_max_allowed_to As Decimal
    Public tito_min_allowed_ti As Decimal
    Public tito_allow_truncate As Boolean
    Public use_cmd_0x1B_handpays As ENUM_SAS_FLAGS
    Public retirement_play_sessions As ENUM_RETIREMENT_PLAY_SESSIONS_STATUS
    Public authentication_method As Integer
    Public authentication_seed As String
    Public authentication_signature As String
    Public authentication_last_checked As Date
    Public authentication_status As ENUM_AUTHENTICATION_STATUS
    Public machine_status As Integer
    Public position As Int32
    Public machine_id As String
    Public top_award As Decimal
    Public max_bet As Decimal
    Public number_lines As String
    Public progressive_meter As ENUM_SAS_FLAGS
    Public game_theme As String
    Public machine_serial_number As String
    Public meter_delta_id As Int64
    Public meter_delta_desc As String
    Public asset_number As Int64
    Public machine_asset_number As Int64
    Public communication_type As Integer
    Public smib_configuration_id As Int64
    Public smib_configuration_name As String
    Public serial_number_3GS As String
    Public machine_number_3GS As Int64
    ' Ini ColJuegos
    Public brand_code As String
    Public model As String
    Public manufacture_year As Integer
    Public sclm As Boolean
    Public met_homologated As Boolean
    Public bet_code As String
    ' End ColJuegos
    Public IsNewOffline As Boolean
    Public IsCoinCollection As Boolean
    Public bcd4 As Int32
    Public bcd5 As Int32
    Public no_RTE As Boolean
    Public single_byte As Boolean
    Public ignore_no_bet_plays As Boolean
    Public aft_although_lock_0 As Boolean

    Public equity_percentage As Decimal
    Public currency_iso_code As String
    Public accouting_denomination As Decimal
    Public chk_equity_percentage As Boolean

    Public sas_host_id As Int32

    Public enable_disable_note_acceptor As ENUM_SAS_FLAGS

    Public reserve_terminal As ENUM_SAS_FLAGS

    Public lock_egm_on_reserve As ENUM_SAS_FLAGS

    Public tito_min_denomination As Decimal

    Public creation_date As Date
    Public replacement_date As Date

    'LTC 2018-JAN-08
    Public manufacturing_month As Integer
    Public manufacturing_day As Integer

  End Class

#End Region ' Structures

#Region " Constructor / Destructor "

  Public Sub New()
    Call MyBase.New()

    Me.m_terminal = New TYPE_TERMINAL
    Me.AssetNumber = -1

  End Sub ' New

#End Region

#Region " Members "

  Protected m_terminal As TYPE_TERMINAL

#End Region ' Members

#Region " Properties "

  Public Property MachineId() As String
    Get
      Return m_terminal.machine_id
    End Get
    Set(ByVal value As String)
      m_terminal.machine_id = value
    End Set
  End Property

  Public Property Position() As Int32
    Get
      Return m_terminal.position
    End Get
    Set(ByVal value As Int32)
      m_terminal.position = value
    End Set
  End Property

  Public Property TopAward() As Decimal
    Get
      Return m_terminal.top_award
    End Get
    Set(ByVal value As Decimal)
      m_terminal.top_award = value
    End Set
  End Property

  Public Property MaxBet() As Decimal
    Get
      Return m_terminal.max_bet
    End Get
    Set(ByVal value As Decimal)
      m_terminal.max_bet = value
    End Set
  End Property

  Public Property NumberLines() As String
    Get
      Return m_terminal.number_lines
    End Get
    Set(ByVal value As String)
      m_terminal.number_lines = value
    End Set
  End Property

  Public Property TerminalId() As Int64
    Get
      Return m_terminal.terminal_id
    End Get

    Set(ByVal Value As Int64)
      m_terminal.terminal_id = Value
    End Set
  End Property

  Public Property Name() As String
    Get
      Return m_terminal.name
    End Get

    Set(ByVal Value As String)
      m_terminal.name = Value
    End Set
  End Property

  Public Property BaseName() As String
    Get
      Return m_terminal.base_name
    End Get

    Set(ByVal Value As String)
      m_terminal.base_name = Value
    End Set
  End Property

  Public Property Status() As Integer
    Get
      Return m_terminal.status
    End Get

    Set(ByVal Value As Integer)
      m_terminal.status = Value
    End Set
  End Property

  Public Property ExternalId() As String
    Get
      Return m_terminal.external_id
    End Get

    Set(ByVal Value As String)
      m_terminal.external_id = Value
    End Set
  End Property

  Public Property Blocked() As Boolean
    Get
      Return m_terminal.blocked
    End Get

    Set(ByVal Value As Boolean)
      m_terminal.blocked = Value
    End Set
  End Property

  Public Property ProviderId() As String
    Get
      Return m_terminal.provider_id
    End Get

    Set(ByVal Value As String)
      m_terminal.provider_id = Value
    End Set
  End Property

  Public Property BuildId() As Integer
    Get
      Return m_terminal.build_id
    End Get

    Set(ByVal Value As Integer)
      m_terminal.build_id = Value
    End Set
  End Property

  Public Property ClientId() As Integer
    Get
      Return m_terminal.client_id
    End Get

    Set(ByVal Value As Integer)
      m_terminal.client_id = Value
    End Set
  End Property

  Public Property VendorId() As String
    Get
      Return m_terminal.vendor_id
    End Get

    Set(ByVal Value As String)
      m_terminal.vendor_id = Value
    End Set
  End Property

  Public Property Type() As Integer
    Get
      Return m_terminal.type
    End Get

    Set(ByVal Value As Integer)
      m_terminal.type = Value
    End Set
  End Property

  Public Property TypeName() As String
    Get
      Return m_terminal.type_name
    End Get

    Set(ByVal Value As String)
      m_terminal.type_name = Value
    End Set
  End Property

  Public Property RetirementDate() As Date
    Get
      Return m_terminal.retirement_date
    End Get

    Set(ByVal Value As Date)
      m_terminal.retirement_date = Value
    End Set

  End Property

  Public Property RetirementRequested() As Date
    Get
      Return m_terminal.retirement_requested
    End Get

    Set(ByVal Value As Date)
      m_terminal.retirement_requested = Value
    End Set

  End Property

  Public Property Denomination() As String
    Get
      Return Me.m_terminal.denomination
    End Get

    Set(ByVal Value As String)
      Me.m_terminal.denomination = Value
    End Set

  End Property

  Public Property Program() As String
    Get
      Return m_terminal.program
    End Get

    Set(ByVal Value As String)
      m_terminal.program = Value
    End Set

  End Property

  Public Property TaxRegistration() As String
    Get
      Return m_terminal.tax_registration
    End Get

    Set(ByVal Value As String)
      m_terminal.tax_registration = Value
    End Set

  End Property

  Public Property TheoreticalPayout() As Decimal
    Get
      Return m_terminal.theoretical_payout
    End Get

    Set(ByVal Value As Decimal)
      m_terminal.theoretical_payout = Value
    End Set

  End Property

  Public Property EquityPercentage As Decimal
    Get
      Return m_terminal.equity_percentage
    End Get

    Set(ByVal Value As Decimal)
      m_terminal.equity_percentage = Value
    End Set
  End Property

  Public Property ChkEquityPercentage As Boolean
    Get
      Return m_terminal.chk_equity_percentage
    End Get

    Set(ByVal Value As Boolean)
      m_terminal.chk_equity_percentage = Value
    End Set
  End Property


  Public Property CurrencyIsoCode As String
    Get
      Return m_terminal.currency_iso_code
    End Get

    Set(ByVal Value As String)
      m_terminal.currency_iso_code = Value
    End Set

  End Property


  Public Property BankId() As Integer
    Get
      Return m_terminal.bank_id
    End Get

    Set(ByVal Value As Integer)
      m_terminal.bank_id = Value
    End Set

  End Property

  Public Property BankName() As String
    Get
      Return m_terminal.bank_name
    End Get

    Set(ByVal Value As String)
      m_terminal.bank_name = Value
    End Set

  End Property

  Public Property AreaName() As String
    Get
      Return m_terminal.area_name
    End Get

    Set(ByVal Value As String)
      m_terminal.area_name = Value
    End Set

  End Property

  Public Property GameType() As Integer
    Get
      Return m_terminal.game_type
    End Get

    Set(ByVal Value As Integer)
      m_terminal.game_type = Value
    End Set

  End Property

  Public Property GameTypeName() As String
    Get
      Return m_terminal.game_type_name
    End Get

    Set(ByVal Value As String)
      m_terminal.game_type_name = Value
    End Set

  End Property

  Public Property FloorId() As String
    Get
      Return m_terminal.floor_id
    End Get

    Set(ByVal Value As String)
      m_terminal.floor_id = Value
    End Set

  End Property

  Public Property ActivationDate() As Date
    Get
      Return m_terminal.activation_date
    End Get

    Set(ByVal Value As Date)
      m_terminal.activation_date = Value
    End Set
  End Property

  Public Property CreationDate() As Date
    Get
      Return m_terminal.creation_date
    End Get
    Set(value As Date)
      m_terminal.creation_date = value
    End Set
  End Property

  Public Property ReplacementDate() As Date
    Get
      Return m_terminal.replacement_date
    End Get
    Set(value As Date)
      m_terminal.replacement_date = value
    End Set
  End Property

  Public Property ExtendedMeter() As ENUM_SAS_FLAGS
    Get
      Return m_terminal.extended_meter
    End Get
    Set(ByVal value As ENUM_SAS_FLAGS)
      m_terminal.extended_meter = value
    End Set
  End Property

  Public Property CreditsPlayModeSAS() As ENUM_SAS_FLAGS
    Get
      Return m_terminal.credits_play_mode_sas
    End Get
    Set(ByVal value As ENUM_SAS_FLAGS)
      m_terminal.credits_play_mode_sas = value
    End Set
  End Property

  Public Property UseCmd0x1bHandpays() As ENUM_SAS_FLAGS
    Get
      Return m_terminal.use_cmd_0x1B_handpays
    End Get
    Set(ByVal value As ENUM_SAS_FLAGS)
      m_terminal.use_cmd_0x1B_handpays = value
    End Set
  End Property

  Public Property ProgressiveMeter() As ENUM_SAS_FLAGS
    Get
      Return m_terminal.progressive_meter
    End Get
    Set(ByVal value As ENUM_SAS_FLAGS)
      m_terminal.progressive_meter = value
    End Set
  End Property

  Public Property SerialNumber() As String
    Get
      Return m_terminal.serial_number
    End Get
    Set(ByVal value As String)
      m_terminal.serial_number = value
    End Set
  End Property

  Public Property CabinetType() As String
    Get
      Return m_terminal.cabinet_type
    End Get
    Set(ByVal value As String)
      m_terminal.cabinet_type = value
    End Set
  End Property

  Public Property JackpotContributionPct() As Decimal
    Get
      Return m_terminal.jackpot_contribution_pct
    End Get
    Set(ByVal value As Decimal)
      m_terminal.jackpot_contribution_pct = value
    End Set
  End Property

  Public Property ContractType() As String
    Get
      Return m_terminal.contract_type
    End Get
    Set(ByVal value As String)
      m_terminal.contract_type = value
    End Set
  End Property

  Public Property ContractId() As String
    Get
      Return m_terminal.contract_id
    End Get
    Set(ByVal value As String)
      m_terminal.contract_id = value
    End Set
  End Property

  Public Property OrderNumber() As String
    Get
      Return m_terminal.order_number
    End Get
    Set(ByVal value As String)
      m_terminal.order_number = value
    End Set
  End Property

  Public Property TitoConfigurationType() As Boolean
    Get
      Return m_terminal.tito_configuration_type
    End Get
    Set(ByVal value As Boolean)
      m_terminal.tito_configuration_type = value
    End Set
  End Property

  Public Property TitoAllowedCashableEmission() As Boolean
    Get
      Return m_terminal.tito_allowed_cashable_emission
    End Get
    Set(ByVal value As Boolean)
      m_terminal.tito_allowed_cashable_emission = value
    End Set
  End Property

  Public Property TitoAllowedPromotionalEmission() As Boolean
    Get
      Return m_terminal.tito_allowed_promotional_emission
    End Get
    Set(ByVal value As Boolean)
      m_terminal.tito_allowed_promotional_emission = value
    End Set
  End Property

  Public Property TitoAllowedRedemption() As Boolean
    Get
      Return m_terminal.tito_allowed_redemption
    End Get
    Set(ByVal value As Boolean)
      m_terminal.tito_allowed_redemption = value
    End Set
  End Property

  Public Property TitoMaxAllowedTicketIn() As Decimal
    Get
      Return m_terminal.tito_max_allowed_ti
    End Get
    Set(ByVal value As Decimal)
      m_terminal.tito_max_allowed_ti = value
    End Set
  End Property

  Public Property TitoMinAllowedTicketIn() As Decimal
    Get
      Return m_terminal.tito_min_allowed_ti
    End Get
    Set(ByVal value As Decimal)
      m_terminal.tito_min_allowed_ti = value
    End Set
  End Property

  Public Property TitoAllowTruncate() As Boolean
    Get
      Return m_terminal.tito_allow_truncate
    End Get
    Set(ByVal value As Boolean)
      m_terminal.tito_allow_truncate = value
    End Set
  End Property

  Public Property TitoMaxAllowedTicketOut() As Decimal
    Get
      Return m_terminal.tito_max_allowed_to
    End Get
    Set(ByVal value As Decimal)
      m_terminal.tito_max_allowed_to = value
    End Set
  End Property

  Public Property RetirementPlaySessions() As ENUM_RETIREMENT_PLAY_SESSIONS_STATUS
    Get
      Return m_terminal.retirement_play_sessions
    End Get
    Set(ByVal value As ENUM_RETIREMENT_PLAY_SESSIONS_STATUS)
      m_terminal.retirement_play_sessions = value
    End Set
  End Property

  Public Property AuthenticationMethod() As ENUM_AUTHENTICATION_METHOD
    Get
      Return m_terminal.authentication_method
    End Get
    Set(ByVal value As ENUM_AUTHENTICATION_METHOD)
      m_terminal.authentication_method = value
    End Set
  End Property

  Public Property AuthenticationSeed() As String
    Get
      Return m_terminal.authentication_seed
    End Get
    Set(ByVal value As String)
      m_terminal.authentication_seed = value
    End Set
  End Property

  Public Property AuthenticationSignature() As String
    Get
      Return m_terminal.authentication_signature
    End Get
    Set(ByVal value As String)
      m_terminal.authentication_signature = value
    End Set
  End Property

  Public Property AuthenticationLastChecked() As Date
    Get
      Return m_terminal.authentication_last_checked
    End Get
    Set(ByVal value As Date)
      m_terminal.authentication_last_checked = value
    End Set
  End Property

  Public Property AuthenticationStatus() As ENUM_AUTHENTICATION_STATUS
    Get
      Return m_terminal.authentication_status
    End Get
    Set(ByVal value As ENUM_AUTHENTICATION_STATUS)
      m_terminal.authentication_status = value
    End Set
  End Property

  Public Property MachineStatus() As Integer
    Get
      Return m_terminal.machine_status
    End Get
    Set(ByVal value As Integer)
      m_terminal.machine_status = value
    End Set
  End Property

  Public Property GameTheme() As String
    Get
      Return m_terminal.game_theme
    End Get
    Set(ByVal value As String)
      m_terminal.game_theme = value
    End Set
  End Property

  Public Property MachineSerialNumber() As String
    Get
      Return m_terminal.machine_serial_number
    End Get
    Set(ByVal value As String)
      m_terminal.machine_serial_number = value
    End Set
  End Property

  Public Property MeterDelta() As Int64
    Get
      Return m_terminal.meter_delta_id
    End Get
    Set(ByVal value As Int64)
      m_terminal.meter_delta_id = value
    End Set
  End Property

  Public Property MeterDeltaDesc() As String
    Get
      Return m_terminal.meter_delta_desc
    End Get
    Set(ByVal value As String)
      m_terminal.meter_delta_desc = value
    End Set
  End Property

  Public Property Change() As Integer
    Get
      Return m_terminal.change_id
    End Get

    Set(ByVal Value As Integer)
      m_terminal.change_id = Value
    End Set
  End Property


  Public Property AssetNumber() As Int64
    Get
      Return m_terminal.asset_number
    End Get
    Set(ByVal value As Int64)
      m_terminal.asset_number = value
    End Set
  End Property

  Public Property MachineAssetNumber() As Int64
    Get
      Return m_terminal.machine_asset_number
    End Get
    Set(ByVal value As Int64)
      m_terminal.machine_asset_number = value
    End Set
  End Property

  'FAV 29-MAY-2015
  Public Property CommunicationType() As Integer
    Get
      Return m_terminal.communication_type
    End Get
    Set(ByVal Value As Integer)
      m_terminal.communication_type = Value
    End Set
  End Property

  Public Property SmibConfigurationId() As Int64
    Get
      Return m_terminal.smib_configuration_id
    End Get
    Set(ByVal Value As Int64)
      m_terminal.smib_configuration_id = Value
    End Set
  End Property

  Public Property SmibConfigurationName() As String
    Get
      Return m_terminal.smib_configuration_name
    End Get
    Set(ByVal Value As String)
      m_terminal.smib_configuration_name = Value
    End Set
  End Property
  ' Ini ColJuegos
  Public Property BrandCode() As String
    Get
      Return m_terminal.brand_code
    End Get
    Set(ByVal value As String)
      m_terminal.brand_code = value
    End Set
  End Property

  Public Property Model() As String
    Get
      Return m_terminal.model
    End Get
    Set(ByVal value As String)
      m_terminal.model = value
    End Set
  End Property

  Public Property ManufactureYear() As Integer
    Get
      Return m_terminal.manufacture_year
    End Get
    Set(ByVal value As Integer)
      m_terminal.manufacture_year = value
    End Set
  End Property

  Public Property SCLM() As Boolean
    Get
      Return m_terminal.sclm
    End Get
    Set(ByVal value As Boolean)
      m_terminal.sclm = value
    End Set
  End Property

  Public Property METHomologated() As Boolean
    Get
      Return m_terminal.met_homologated
    End Get
    Set(ByVal value As Boolean)
      m_terminal.met_homologated = value
    End Set
  End Property
  Public Property IsNewOffline As Boolean
    Get
      Return m_terminal.IsNewOffline
    End Get
    Set(value As Boolean)
      m_terminal.IsNewOffline = value
    End Set
  End Property
  Public Property BetCode() As String
    Get
      Return m_terminal.bet_code
    End Get
    Set(ByVal value As String)
      m_terminal.bet_code = value
    End Set
  End Property
  ' Fin ColJuegos


  Public Property BCD4() As Integer
    Get
      Return m_terminal.bcd4
    End Get
    Set(ByVal value As Integer)
      m_terminal.bcd4 = value
    End Set
  End Property


  Public Property BCD5() As Integer
    Get
      Return m_terminal.bcd5
    End Get
    Set(ByVal value As Integer)
      m_terminal.bcd5 = value
    End Set
  End Property

  Public Property IsCoinCollection As Boolean
    Get
      Return m_terminal.IsCoinCollection
    End Get
    Set(value As Boolean)
      m_terminal.IsCoinCollection = value
    End Set
  End Property

  ' RAB: Product Backlog Item 8857:Bally 3.5 BCDs: GUI: Modificaci�n de la pantalla de edici�n de terminales
  Public Property No_RTE As Boolean
    Get
      Return m_terminal.no_RTE
    End Get
    Set(value As Boolean)
      m_terminal.no_RTE = value
    End Set
  End Property

  Public Property Single_Byte As Boolean
    Get
      Return m_terminal.single_byte
    End Get
    Set(value As Boolean)
      m_terminal.single_byte = value
    End Set
  End Property

  Public Property Ignore_no_bet_plays As Boolean
    Get
      Return m_terminal.ignore_no_bet_plays
    End Get
    Set(value As Boolean)
      m_terminal.ignore_no_bet_plays = value
    End Set
  End Property

  Public Property AccountingDenomination() As Decimal
    Get
      Return m_terminal.accouting_denomination
    End Get

    Set(ByVal Value As Decimal)
      m_terminal.accouting_denomination = Value
    End Set

  End Property

  Public Property AFT_although_lock_0 As Boolean
    Get
      Return m_terminal.aft_although_lock_0
    End Get

    Set(Value As Boolean)
      m_terminal.aft_although_lock_0 = Value
    End Set

  End Property

  Public Property SASHostId() As Integer
    Get
      Return m_terminal.sas_host_id
    End Get

    Set(ByVal Value As Integer)
      m_terminal.sas_host_id = Value
    End Set
  End Property

  Public Property EnableDisableNoteAcceptor() As ENUM_SAS_FLAGS
    Get
      Return m_terminal.enable_disable_note_acceptor
    End Get
    Set(ByVal value As ENUM_SAS_FLAGS)
      m_terminal.enable_disable_note_acceptor = value
    End Set
  End Property

  Public Property ReserveTerminal() As ENUM_SAS_FLAGS
    Get
      Return m_terminal.reserve_terminal
    End Get
    Set(ByVal value As ENUM_SAS_FLAGS)
      m_terminal.reserve_terminal = value
    End Set
  End Property

  Public Property LockEgmOnReserve() As ENUM_SAS_FLAGS
    Get
      Return m_terminal.lock_egm_on_reserve
    End Get
    Set(ByVal value As ENUM_SAS_FLAGS)
      m_terminal.lock_egm_on_reserve = value
    End Set
  End Property

  Public Property TitoMinDenomination() As Decimal
    Get
      Return m_terminal.tito_min_denomination
    End Get
    Set(ByVal value As Decimal)
      m_terminal.tito_min_denomination = value
    End Set
  End Property

  'LTC 2018-JAN-08
  Public Property ManufacturingMonth As Integer
    Get
      Return m_terminal.manufacturing_month
    End Get
    Set(value As Integer)
      m_terminal.manufacturing_month = value
    End Set

  End Property

  Public Property ManufacturingDay As Integer
    Get
      Return m_terminal.manufacturing_day
    End Get
    Set(value As Integer)
      m_terminal.manufacturing_day = value
    End Set

  End Property

#End Region ' Properties

#Region " Shared Functions "

  Shared Function StatusName(ByVal StatusValue As Integer) As String
    Select Case StatusValue
      Case WSI.Common.TerminalStatus.ACTIVE
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(397)   ' 397 "Activo"

      Case WSI.Common.TerminalStatus.OUT_OF_SERVICE
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(398)   ' 398 "Fuera de servicio"

      Case WSI.Common.TerminalStatus.RETIRED
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(399)   ' 399 "Retirado"

      Case Else
    End Select

    Return ""

  End Function

  Shared Function DefaultEquityPercentage() As Decimal

    Return GeneralParam.GetDecimal("Terminal", "EquityPercentage", 50)

  End Function


  ' PURPOSE: ContractType name selection 
  '
  '  PARAMS:
  '     - INPUT:
  '         - ContractType.
  '     - OUTPUT:
  '
  ' RETURNS:

  Shared Function ContractTypeName(ByVal ContractType As Integer) As String

    Dim _item_catalog As String
    _item_catalog = ""

    Dim catalog As New Catalog()
    Dim _item_catalog_list As New List(Of WSI.Common.Catalog.Catalog_Item)
    _item_catalog_list = catalog.GetCatalogItems(catalog.CatalogSystemType.ContractType)

    For Each cat As WSI.Common.Catalog.Catalog_Item In _item_catalog_list
      If ContractType = cat.CatalogItemId Then
        _item_catalog = cat.Name
      End If

    Next

    Return _item_catalog

  End Function



  ' PURPOSE : Read the terminal name from DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - TerminalId
  '         - SqlTrx
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - String: Terminal name

  Public Shared Function GetTerminalName(ByVal TerminalId As Int32, ByVal SqlTrx As SqlTransaction) As String
    Dim _str_sql As String
    Dim _obj As Object
    Dim _te_name As String

    _te_name = ""

    Try
      _str_sql = "SELECT   TE_NAME " & _
                 "  FROM   TERMINALS " & _
                 " WHERE   TE_TERMINAL_ID = @pTerminalId "

      Using _sql_cmd As New SqlCommand(_str_sql, SqlTrx.Connection, SqlTrx)
        _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId
        _obj = _sql_cmd.ExecuteScalar()
        If _obj IsNot Nothing And _obj IsNot DBNull.Value Then
          _te_name = _obj
        End If
      End Using

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return _te_name

  End Function ' GetTerminalName

  ''' <summary>
  ''' Get ENUM_SAS_FLAGS from SasFlag and SasFlagUseSiteDefault parameters
  ''' </summary>
  ''' <param name="SasFlag">Sas flag in decimal format</param>
  ''' <param name="SasFlagUseSiteDefault">Sas flag use site default</param>
  ''' <param name="Bcd4">Parameter by reference of the bit 4 to 8 in the case of non extended - customized</param>
  ''' <param name="Bcd5">Parameter by reference of the bit 5 to 10 in the case of non extended - customized</param>
  ''' <returns>ENUM_SAS_FLAGS:
  '''              - ENUM_SAS_FLAGS.SITE_DEFINED               : Site defined
  '''              - ENUM_SAS_FLAGS.ACTIVATED                  : Extended
  '''              - ENUM_SAS_FLAGS.NOT_ACTIVATED              : Non extended
  '''              - ENUM_SAS_FLAGS.NON_EXTENDED_METERS_LENGHT : Non extended - customized
  ''' </returns>
  ''' <remarks></remarks>
  Public Shared Function GetEnumSasFlag(ByVal SasFlag As Integer, ByVal SasFlagUseSiteDefault As Integer, Optional ByRef Bcd4 As Integer = 0, Optional ByRef Bcd5 As Integer = 0) As ENUM_SAS_FLAGS
    'SAS Flags
    '   - Use extended meters
    '   - Credits play mode
    '   - Use command 0x1B Handpays
    '   - Special Progressive Meter

    '   - Use extended meters
    If WSI.Common.Terminal.SASFlagActived(SasFlagUseSiteDefault, SAS_FLAGS.USE_EXTENDED_METERS) Then
      Return ENUM_SAS_FLAGS.SITE_DEFINED
    Else
      ' RAB: Product Backlog Item 8857:Bally 3.5 BCDs: GUI: Modificaci�n de la pantalla de edici�n de terminales

      Dim _flag As Int32

      Bcd4 = SasFlag And &HF0
      Bcd4 >>= 4
      Bcd5 = SasFlag And &HF00
      Bcd5 >>= 8
      _flag = SasFlag And &HF

      If (_flag = 0 And Bcd4 <> 0 And Bcd5 <> 0) Then
        Return ENUM_SAS_FLAGS.NON_EXTENDED_METERS_LENGHT
      ElseIf (_flag = 0 And Bcd4 = 0 And Bcd5 = 0) Then
        Return ENUM_SAS_FLAGS.NOT_ACTIVATED
      ElseIf (_flag = 1 And Bcd4 = 0 And Bcd5 = 0) Then
        Return ENUM_SAS_FLAGS.ACTIVATED
      End If
    End If
  End Function

  Public Shared Function Cmd0x1Bhandpays(ByVal SasFlagsSiteDefault As Integer, ByVal SasFlags As Integer) As ENUM_SAS_FLAGS

    Dim _use_cmd_0x1B_handpays As ENUM_SAS_FLAGS

    _use_cmd_0x1B_handpays = ENUM_SAS_FLAGS.NOT_ACTIVATED

    If WSI.Common.Terminal.SASFlagActived(SasFlagsSiteDefault, SAS_FLAGS.USE_HANDPAY_INFORMATION) Then
      _use_cmd_0x1B_handpays = ENUM_SAS_FLAGS.SITE_DEFINED
    ElseIf WSI.Common.Terminal.SASFlagActived(SasFlags, SAS_FLAGS.USE_HANDPAY_INFORMATION) Then
      _use_cmd_0x1B_handpays = ENUM_SAS_FLAGS.ACTIVATED

      If WSI.Common.Terminal.SASFlagActived(SasFlags, SAS_FLAGS.SAS_FLAGS_AUTOMATIC_HANDPAY_PAYMENT) Then
        _use_cmd_0x1B_handpays = ENUM_SAS_FLAGS.ACTIVATED_AND_AUTOMATICALLY
      End If
    End If

    Return _use_cmd_0x1B_handpays

  End Function

#End Region ' Public Functions

#Region " Overrides functions "

  '----------------------------------------------------------------------------
  ' PURPOSE : Reads from the database into the given object
  '
  ' PARAMS :
  '     - INPUT :
  '         - ObjectId: An object, it must be 'casted' to the desired KEY.
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '
  ' NOTES :

  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As Integer

    Me.TerminalId = ObjectId

    ' Read terminal data
    _rc = ReadTerminal(m_terminal, SqlCtx)

    Select Case _rc

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Read

  '----------------------------------------------------------------------------
  ' PURPOSE : Updates the given object to the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES :

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As Integer

    ' Update terminal data
    _rc = UpdateTerminal(m_terminal, SqlCtx)

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Call TerminalReport.ForceRefresh()
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
        Return ENUM_STATUS.STATUS_DUPLICATE_KEY

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Update

  '----------------------------------------------------------------------------
  ' PURPOSE : Inserts the given object to the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES :

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As Integer

    ' Insert terminal data
    _rc = InsertTerminal(m_terminal, SqlCtx)

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Call TerminalReport.ForceRefresh()
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
        Return ENUM_STATUS.STATUS_DUPLICATE_KEY

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Insert

  '----------------------------------------------------------------------------
  ' PURPOSE : Deletes the given object from the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DEPENDENCIES
  '
  ' NOTES :

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As Integer

    ' Delete terminal data
    _rc = DeleteTerminal(Me.TerminalId, SqlCtx)

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DEPENDENCIES
        Return ENUM_STATUS.STATUS_DEPENDENCIES

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Delete

  '----------------------------------------------------------------------------
  ' PURPOSE : Returns a duplicate of the given object.
  '
  ' PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - The newly created object
  '
  ' NOTES :

  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim temp_object As CLASS_TERMINAL

    temp_object = New CLASS_TERMINAL
    temp_object.m_terminal.terminal_id = Me.m_terminal.terminal_id
    temp_object.m_terminal.master_id = Me.m_terminal.master_id
    temp_object.m_terminal.change_id = Me.m_terminal.change_id
    temp_object.m_terminal.name = Me.m_terminal.name
    temp_object.m_terminal.base_name = Me.m_terminal.base_name
    temp_object.m_terminal.status = Me.m_terminal.status
    temp_object.m_terminal.external_id = Me.m_terminal.external_id
    temp_object.m_terminal.blocked = Me.m_terminal.blocked
    temp_object.m_terminal.provider_id = Me.m_terminal.provider_id
    temp_object.m_terminal.build_id = Me.m_terminal.build_id
    temp_object.m_terminal.client_id = Me.m_terminal.client_id
    temp_object.m_terminal.vendor_id = Me.m_terminal.vendor_id
    temp_object.m_terminal.type = Me.m_terminal.type
    temp_object.m_terminal.type_name = Me.m_terminal.type_name
    temp_object.m_terminal.retirement_date = Me.m_terminal.retirement_date
    temp_object.m_terminal.retirement_requested = Me.m_terminal.retirement_requested
    temp_object.m_terminal.denomination = Me.m_terminal.denomination
    temp_object.m_terminal.program = Me.m_terminal.program
    temp_object.m_terminal.tax_registration = Me.m_terminal.tax_registration
    temp_object.m_terminal.theoretical_payout = Me.m_terminal.theoretical_payout
    temp_object.m_terminal.bank_id = Me.m_terminal.bank_id
    temp_object.m_terminal.bank_name = Me.m_terminal.bank_name
    temp_object.m_terminal.area_name = Me.m_terminal.area_name
    temp_object.m_terminal.game_type = Me.m_terminal.game_type
    temp_object.m_terminal.game_type_name = Me.m_terminal.game_type_name
    temp_object.m_terminal.floor_id = Me.m_terminal.floor_id
    temp_object.m_terminal.activation_date = Me.m_terminal.activation_date
    temp_object.m_terminal.extended_meter = Me.m_terminal.extended_meter

    ' RAB: ' RAB: Product Backlog Item 8857:Bally 3.5 BCDs: GUI: Modificaci�n de la pantalla de edici�n de terminales
    temp_object.m_terminal.bcd4 = IIf(Me.m_terminal.bcd4 = 0, 4, Me.m_terminal.bcd4)
    temp_object.m_terminal.bcd5 = IIf(Me.m_terminal.bcd5 = 0, 5, Me.m_terminal.bcd5)
    temp_object.m_terminal.no_RTE = Me.m_terminal.no_RTE
    temp_object.m_terminal.single_byte = Me.m_terminal.single_byte
    temp_object.m_terminal.ignore_no_bet_plays = Me.m_terminal.ignore_no_bet_plays
    temp_object.m_terminal.aft_although_lock_0 = Me.m_terminal.aft_although_lock_0
    temp_object.m_terminal.credits_play_mode_sas = Me.m_terminal.credits_play_mode_sas
    temp_object.m_terminal.serial_number = Me.m_terminal.serial_number
    temp_object.m_terminal.machine_serial_number = Me.m_terminal.machine_serial_number
    temp_object.m_terminal.cabinet_type = Me.m_terminal.cabinet_type
    temp_object.m_terminal.jackpot_contribution_pct = Me.m_terminal.jackpot_contribution_pct
    temp_object.m_terminal.contract_id = Me.m_terminal.contract_id
    temp_object.m_terminal.contract_type = Me.m_terminal.contract_type
    temp_object.m_terminal.order_number = Me.m_terminal.order_number
    temp_object.m_terminal.tito_allowed_cashable_emission = Me.m_terminal.tito_allowed_cashable_emission
    temp_object.m_terminal.tito_allowed_redemption = Me.m_terminal.tito_allowed_redemption
    temp_object.m_terminal.tito_allowed_promotional_emission = Me.m_terminal.tito_allowed_promotional_emission
    temp_object.m_terminal.tito_configuration_type = Me.m_terminal.tito_configuration_type
    temp_object.m_terminal.tito_max_allowed_ti = Me.m_terminal.tito_max_allowed_ti
    temp_object.m_terminal.tito_max_allowed_to = Me.m_terminal.tito_max_allowed_to
    temp_object.m_terminal.use_cmd_0x1B_handpays = Me.m_terminal.use_cmd_0x1B_handpays
    temp_object.m_terminal.retirement_play_sessions = Me.m_terminal.retirement_play_sessions
    temp_object.m_terminal.authentication_method = Me.AuthenticationMethod
    temp_object.m_terminal.authentication_seed = Me.AuthenticationSeed
    temp_object.m_terminal.authentication_signature = Me.AuthenticationSignature
    temp_object.m_terminal.authentication_last_checked = Me.AuthenticationLastChecked
    temp_object.m_terminal.authentication_status = Me.AuthenticationStatus
    temp_object.m_terminal.machine_status = Me.MachineStatus
    temp_object.m_terminal.machine_id = Me.m_terminal.machine_id
    temp_object.m_terminal.position = Me.m_terminal.position
    temp_object.m_terminal.top_award = Me.m_terminal.top_award
    temp_object.m_terminal.max_bet = Me.m_terminal.max_bet
    temp_object.m_terminal.number_lines = Me.m_terminal.number_lines
    temp_object.m_terminal.progressive_meter = Me.m_terminal.progressive_meter
    temp_object.m_terminal.game_theme = Me.m_terminal.game_theme
    temp_object.m_terminal.asset_number = Me.m_terminal.asset_number
    temp_object.m_terminal.machine_asset_number = Me.m_terminal.machine_asset_number
    temp_object.m_terminal.meter_delta_id = Me.m_terminal.meter_delta_id
    temp_object.m_terminal.meter_delta_desc = Me.m_terminal.meter_delta_desc

    'FAV 29-MAY-2015
    temp_object.m_terminal.communication_type = Me.m_terminal.communication_type
    temp_object.m_terminal.smib_configuration_id = Me.m_terminal.smib_configuration_id
    temp_object.m_terminal.smib_configuration_name = Me.m_terminal.smib_configuration_name

    ' DCS 05-AUG-2015: WIG-2624 can not insert new terminals 3GS
    temp_object.m_terminal.serial_number_3GS = Me.m_terminal.serial_number_3GS
    temp_object.m_terminal.machine_number_3GS = Me.m_terminal.machine_number_3GS

    ' ColJuegos
    If WSI.Common.Misc.IsColJuegosEnabled Then
      temp_object.m_terminal.brand_code = Me.m_terminal.brand_code
      temp_object.m_terminal.model = Me.m_terminal.model

      temp_object.m_terminal.met_homologated = Me.m_terminal.met_homologated
      temp_object.m_terminal.bet_code = Me.m_terminal.bet_code
    End If

    'LTC 2018-JAN-08
    temp_object.m_terminal.manufacture_year = Me.m_terminal.manufacture_year

    'DRV 01-JUL-2015
    temp_object.m_terminal.IsNewOffline = Me.m_terminal.IsNewOffline

    temp_object.m_terminal.IsCoinCollection = Me.m_terminal.IsCoinCollection

    temp_object.EquityPercentage = Me.m_terminal.equity_percentage
    temp_object.ChkEquityPercentage = Me.m_terminal.chk_equity_percentage
    temp_object.CurrencyIsoCode = Me.m_terminal.currency_iso_code
    temp_object.AccountingDenomination = Me.m_terminal.accouting_denomination

    temp_object.EquityPercentage = Me.m_terminal.equity_percentage
    temp_object.ChkEquityPercentage = Me.m_terminal.chk_equity_percentage

    temp_object.SASHostId = Me.m_terminal.sas_host_id

    temp_object.EnableDisableNoteAcceptor = Me.m_terminal.enable_disable_note_acceptor
    temp_object.ReserveTerminal = Me.m_terminal.reserve_terminal
    temp_object.LockEgmOnReserve = Me.m_terminal.lock_egm_on_reserve

    temp_object.m_terminal.tito_min_denomination = Me.m_terminal.tito_min_denomination
    temp_object.m_terminal.tito_min_allowed_ti = Me.m_terminal.tito_min_allowed_ti
    temp_object.m_terminal.tito_allow_truncate = Me.m_terminal.tito_allow_truncate

    temp_object.m_terminal.creation_date = Me.m_terminal.creation_date
    temp_object.m_terminal.replacement_date = Me.m_terminal.replacement_date

    'LTC 2018-JAN-04
    temp_object.m_terminal.manufacturing_month = Me.m_terminal.manufacturing_month
    temp_object.m_terminal.manufacturing_day = Me.m_terminal.manufacturing_day

    Return temp_object

  End Function ' Duplicate

  '----------------------------------------------------------------------------
  ' PURPOSE : Returns the related auditor data for the current object.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - The newly created audit object
  '
  ' NOTES :

  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _is_tito_mode As Boolean
    Dim _audit_text As String
    Dim _system_mode As SYSTEM_MODE
    Dim _gp_machine_id As String
    Dim _gp_game_theme As String

    _is_tito_mode = TITO.Utils.IsTitoMode
    ' Read System mode
    _system_mode = WSI.Common.Misc.SystemMode()

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_TERMINALS)

    'to avoid ask for lost changes when a terminal haven't never been changed and user cancels
    If Me.Change = 0 Then
      Call _auditor_data.SetName(GLB_NLS_GUI_CONFIGURATION.Id(470), Me.BaseName)      ' 470 "Terminal"
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(353), Me.BaseName)   ' 353 "Nombre"
    Else
      Call _auditor_data.SetName(GLB_NLS_GUI_CONFIGURATION.Id(470), Me.Name)      ' 470 "Terminal"
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(353), Me.Name)   ' 353 "Nombre"
    End If

    ' 259 "Estado"
    Call _auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(259), CLASS_TERMINAL.StatusName(Me.Status))

    Call _auditor_data.SetField(IIf(WSI.Common.Misc.IsColJuegosEnabled, GLB_NLS_GUI_PLAYER_TRACKING.Id(6384), GLB_NLS_GUI_AUDITOR.Id(331)), Me.ExternalId)
    Call _auditor_data.SetField(GLB_NLS_GUI_AUDITOR.Id(332), Me.Blocked)
    Call _auditor_data.SetField(GLB_NLS_GUI_AUDITOR.Id(341), Me.ProviderId)
    Call _auditor_data.SetField(GLB_NLS_GUI_AUDITOR.Id(344), IIf(String.IsNullOrEmpty(Me.VendorId), AUDIT_NONE_STRING, Me.VendorId))

    ' 455 "Fecha de Retirada"
    If Me.Status = WSI.Common.TerminalStatus.RETIRED Then
      Call _auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(455), Me.RetirementDate)
    Else
      Call _auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(455), AUDIT_NONE_STRING)
    End If

    ' Sesiones de juego: Cierre manual
    Select Case Me.RetirementPlaySessions
      Case CLASS_TERMINAL.ENUM_RETIREMENT_PLAY_SESSIONS_STATUS.REMAIN_OPEN
        Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(4442), GLB_NLS_GUI_AUDITOR.GetString(337))
      Case CLASS_TERMINAL.ENUM_RETIREMENT_PLAY_SESSIONS_STATUS.CLOSE
        Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(4442), GLB_NLS_GUI_AUDITOR.GetString(336))
      Case Else
        Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(4442), AUDIT_NONE_STRING)
    End Select

    If Not String.IsNullOrEmpty(Me.Denomination) Then
      Call _auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(420), Me.Denomination)
    Else
      Call _auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(420), AUDIT_NONE_STRING)
    End If

    If Me.Program = "" Then
      Call _auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(418), AUDIT_NONE_STRING)
    Else
      Call _auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(418), Me.Program)
    End If

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(IIf(WSI.Common.Misc.IsColJuegosEnabled, 6385, 1100)), IIf(Me.TaxRegistration = String.Empty, AUDIT_NONE_STRING, Me.TaxRegistration))

    If Me.TheoreticalPayout = -1D Then 'NOTHING
      Call _auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(419), AUDIT_NONE_STRING)
    Else
      Call _auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(419), GUI_FormatNumber(Me.TheoreticalPayout * 100, 2))
    End If

    'SSC 15-FEB-2012
    If Me.BankName = "" Then 'NOTHING
      Call _auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(431), AUDIT_NONE_STRING)
    Else
      Call _auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(431), Me.BankName + " (" + Me.AreaName + ")")
    End If

    If Me.GameTypeName = "" Then 'NOTHING
      Call _auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(427), AUDIT_NONE_STRING)
    Else
      Call _auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(427), Me.GameTypeName)
    End If

    Call _auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(432), Me.FloorId)
    Call _auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(428), Me.ActivationDate)

    ' RAB: Product Backlog Item 8857:Bally 3.5 BCDs: GUI: Modificaci�n de la pantalla de edici�n de terminales
    ' Extended meter
    _audit_text = "---"
    If Me.ExtendedMeter = ENUM_SAS_FLAGS.SITE_DEFINED Then
      _audit_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4432) '4432 "Site defined"
    ElseIf Me.ExtendedMeter = ENUM_SAS_FLAGS.ACTIVATED Then
      _audit_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7081)
    ElseIf Me.ExtendedMeter = ENUM_SAS_FLAGS.NOT_ACTIVATED Then
      _audit_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7082)
    ElseIf Me.ExtendedMeter = ENUM_SAS_FLAGS.EXTENDED_METERS_LENGHT Then
      _audit_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7083) + "(" + Me.BCD4.ToString() + " , " + Me.BCD5.ToString() + ")" '7083 "Extended meters lenght"
    ElseIf Me.ExtendedMeter = ENUM_SAS_FLAGS.NON_EXTENDED_METERS_LENGHT Then
      _audit_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7084) + "(" + Me.BCD4.ToString() + " , " + Me.BCD5.ToString() + ")" '7084 "Non extended meters lenght"
    End If
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1155), _audit_text)

    ' No RTE    
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7088), IIf(Me.m_terminal.no_RTE, GLB_NLS_GUI_AUDITOR.GetString(336), GLB_NLS_GUI_AUDITOR.GetString(337)))

    ' Single byte    
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7089), IIf(Me.m_terminal.single_byte, GLB_NLS_GUI_AUDITOR.GetString(336), GLB_NLS_GUI_AUDITOR.GetString(337)))

    ' Ignore no bet plays
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7111), IIf(Me.m_terminal.ignore_no_bet_plays, GLB_NLS_GUI_AUDITOR.GetString(336), GLB_NLS_GUI_AUDITOR.GetString(337)))

    ' No AFT on lock 0
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7496), IIf(Me.m_terminal.aft_although_lock_0, GLB_NLS_GUI_AUDITOR.GetString(336), GLB_NLS_GUI_AUDITOR.GetString(337)))

    ' CreditsPlayModeSAS
    _audit_text = "---"
    If Me.CreditsPlayModeSAS = ENUM_SAS_FLAGS.SITE_DEFINED Then
      _audit_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4432) '4432 "Site defined"
    ElseIf Me.CreditsPlayModeSAS = ENUM_SAS_FLAGS.MACHINE_DEFINED Then
      _audit_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1158) '1158 "Machine-defined"
    ElseIf Me.CreditsPlayModeSAS = ENUM_SAS_FLAGS.SYSTEM_DEFINED Then
      _audit_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1157) '1157 "System-defined"
    End If
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1156), _audit_text)

    ' Use SAS cmd 0x1BHandpays
    _audit_text = "---"
    If Me.UseCmd0x1bHandpays = ENUM_SAS_FLAGS.SITE_DEFINED Then
      _audit_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4432) '4432 "Site defined"
    ElseIf Me.UseCmd0x1bHandpays = ENUM_SAS_FLAGS.ACTIVATED Then
      _audit_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(698) '698 "Yes"
    ElseIf Me.UseCmd0x1bHandpays = ENUM_SAS_FLAGS.NOT_ACTIVATED Then
      _audit_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(699) '699 "No"
    ElseIf Me.UseCmd0x1bHandpays = ENUM_SAS_FLAGS.ACTIVATED_AND_AUTOMATICALLY Then
      _audit_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7808) '7808 "Activated & Automatic"
    End If
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(4433), _audit_text)

    ' Progressive Meters
    _audit_text = "---"
    If Me.ProgressiveMeter = ENUM_SAS_FLAGS.SITE_DEFINED Then
      _audit_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4432) '4432 "Site defined"
    ElseIf Me.ProgressiveMeter = ENUM_SAS_FLAGS.ACTIVATED Then
      _audit_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(698) '698 "Yes"
    ElseIf Me.ProgressiveMeter = ENUM_SAS_FLAGS.NOT_ACTIVATED Then
      _audit_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(699) '699 "No"
    End If
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5590), _audit_text)

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(IIf(WSI.Common.Misc.IsColJuegosEnabled, 6386, 1639)), IIf(Me.SerialNumber = String.Empty, AUDIT_NONE_STRING, Me.SerialNumber))

    _gp_machine_id = GeneralParam.GetString("EGM", "MachineId.Name")
    If Not String.IsNullOrEmpty(_gp_machine_id) Then
      Call _auditor_data.SetField(0, IIf(String.IsNullOrEmpty(Me.MachineId), AUDIT_NONE_STRING, Me.MachineId), _gp_machine_id)
    End If

    If Me.MachineSerialNumber = "" Then
      Call _auditor_data.SetField(0, AUDIT_NONE_STRING, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1639) & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6048))
    Else
      Call _auditor_data.SetField(0, Me.MachineSerialNumber, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1639) & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6048))
    End If

    ' ColJuegos
    If WSI.Common.Misc.IsColJuegosEnabled Then
      ' Brand code
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6387), IIf(Me.BrandCode = String.Empty, AUDIT_NONE_STRING, Me.BrandCode))

      ' Model
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6388), IIf(Me.Model = String.Empty, AUDIT_NONE_STRING, Me.Model))

      ' Manufacture year
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6389), IIf(Me.ManufactureYear <= 0, AUDIT_NONE_STRING, GUI_FormatNumber(Me.ManufactureYear, 0)))

      ' MET homologated
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6392), IIf(Me.METHomologated, GLB_NLS_GUI_AUDITOR.GetString(336), GLB_NLS_GUI_AUDITOR.GetString(337)))

      ' Bet code
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6391), IIf(Me.BetCode = String.Empty, AUDIT_NONE_STRING, Me.BetCode))
    End If

    ' TODO NLS's
    If Me.AssetNumber < 0 Then
      Call _auditor_data.SetField(0, AUDIT_NONE_STRING, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6143) & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6049))
    Else
      Call _auditor_data.SetField(0, Me.AssetNumber, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6143) & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6049))
    End If

    If Me.MachineAssetNumber < 0 Then
      Call _auditor_data.SetField(0, AUDIT_NONE_STRING, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6143) & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6048))
    Else
      Call _auditor_data.SetField(0, Me.MachineAssetNumber, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6143) & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6048))
    End If
    _gp_machine_id = GeneralParam.GetString("EGM", "MachineId.Name")
    If Not String.IsNullOrEmpty(_gp_machine_id) Then
      Call _auditor_data.SetField(0, IIf(String.IsNullOrEmpty(Me.MachineId), AUDIT_NONE_STRING, Me.MachineId), _gp_machine_id)
    End If
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5222), IIf(Me.Position <= 0, AUDIT_NONE_STRING, GUI_FormatNumber(Me.Position, 0)))
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5223), IIf(Me.TopAward <= 0, AUDIT_NONE_STRING, GUI_FormatCurrency(Me.TopAward, 2)))
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5224), IIf(Me.MaxBet <= 0, AUDIT_NONE_STRING, GUI_FormatCurrency(Me.MaxBet, 2)))
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5225), IIf(String.IsNullOrEmpty(Me.NumberLines), AUDIT_NONE_STRING, Me.NumberLines))

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6061), Me.MeterDeltaDesc)

    If Me.CabinetType = "" Then
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1640), AUDIT_NONE_STRING)
    Else
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1640), Me.CabinetType)
    End If

    If Me.JackpotContributionPct = -1D Then 'NOTHING
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1641), AUDIT_NONE_STRING)
    Else
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1641), GUI_FormatNumber(Me.JackpotContributionPct, 2))
    End If

    ' JCOR 03-JUL-2013
    If ContractType = 0 Then
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2248), AUDIT_NONE_STRING)
    Else
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2248), ContractTypeName(Me.ContractType))
    End If

    If Me.ContractId = "" Then
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2249), AUDIT_NONE_STRING)
    Else
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2249), Me.ContractId)
    End If

    If Me.OrderNumber = "" Then
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2250), AUDIT_NONE_STRING)
    Else
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2250), Me.OrderNumber)
    End If
    If _is_tito_mode And Me.m_terminal.type = WSI.Common.TerminalTypes.SAS_HOST Then
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2775), IIf(Me.m_terminal.tito_configuration_type, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2776), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2778)))
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2717), IIf(Me.m_terminal.tito_allowed_redemption, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)))   ' 2717 "Allow Ticket Creation"
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2718), IIf(Me.m_terminal.tito_allowed_cashable_emission, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)))         ' 2718 "Allow Cashable Redemption"
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2719), IIf(Me.m_terminal.tito_allowed_promotional_emission, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279))) ' 2719 "Allow Promotional Redemption"
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7677), IIf(Me.m_terminal.tito_allow_truncate, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279))) ' 2719 "Allow Truncate"
      Call _auditor_data.SetField(0, GUI_FormatCurrency(Me.m_terminal.tito_max_allowed_ti, 2), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2293)) '2293  "Max. Ticket In amount"
      Call _auditor_data.SetField(0, GUI_FormatCurrency(Me.m_terminal.tito_max_allowed_to, 2), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2864)) '2864  "Max. Ticket Out amount*"
      Call _auditor_data.SetField(0, GUI_FormatCurrency(Me.m_terminal.tito_min_allowed_ti, 2), GLB_NLS_GUI_PLAYER_TRACKING.GetString(7676)) '7676  "Min. Ticket In amount*"

    End If

    'If _system_mode = SYSTEM_MODE.WASS Then
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(4782), IIf((Me.m_terminal.machine_status And MASK_MACHINE_BLOCKED_MANUALLY) = MASK_MACHINE_BLOCKED_MANUALLY, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)))   ' Machine Blocked
    'End If

    'FAV 30-MAY-2016
    If (Me.m_terminal.type = WSI.Common.TerminalTypes.SAS_HOST) Then
      'FAV 29-MAY-2015
      'ETP 26-JAN-2016
      Dim _str_comunication_type As String
      _str_comunication_type = Me.m_terminal.communication_type.ToString()

      Select Case (Me.m_terminal.communication_type)
        Case SMIB_COMMUNICATION_TYPE.WCP
          _str_comunication_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5894)
        Case SMIB_COMMUNICATION_TYPE.SAS
          _str_comunication_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2096)
        Case SMIB_COMMUNICATION_TYPE.PULSES
          _str_comunication_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6378)
        Case SMIB_COMMUNICATION_TYPE.SAS_IGT_POKER
          _str_comunication_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7027)
        Case Else
          _str_comunication_type = Me.m_terminal.communication_type.ToString()
          Log.Warning("cls_terminals. Comunication type not defined = " + _str_comunication_type)
      End Select

      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2777), _str_comunication_type)
    End If

    If String.IsNullOrEmpty(Me.m_terminal.smib_configuration_name) Then
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2775), AUDIT_NONE_STRING)
    Else
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2775), Me.m_terminal.smib_configuration_name)
    End If

    'DDS 05-JAN-2016
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2076), Me.m_terminal.currency_iso_code)


    _auditor_data.SetRelated(CLASS_AUDITOR_DATA.ENUM_AUDITOR_RELATED.TERMINAL, Me.m_terminal.terminal_id)

    _gp_game_theme = GeneralParam.GetString("EGM", "GameTheme.Name")
    If Not String.IsNullOrEmpty(_gp_game_theme) Then
      Call _auditor_data.SetField(0, IIf(String.IsNullOrEmpty(Me.GameTheme), AUDIT_NONE_STRING, Me.GameTheme), _gp_game_theme)
    End If

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6707), Me.IsCoinCollection)

    If _is_tito_mode Then
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6950), GUI_FormatNumber(Me.EquityPercentage)) ' 01-JUL-2016  LTC
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7776), IIf(Me.ChkEquityPercentage, GLB_NLS_GUI_PLAYER_TRACKING.GetString(279), GLB_NLS_GUI_PLAYER_TRACKING.GetString(278))) ' 22-NOV-2016  PDM
    End If

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7511), GUI_FormatNumber(Me.SASHostId))

    _audit_text = "---"
    If Me.EnableDisableNoteAcceptor = ENUM_SAS_FLAGS.SITE_DEFINED Then
      _audit_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4432) '4432 "Site defined"
    ElseIf Me.EnableDisableNoteAcceptor = ENUM_SAS_FLAGS.ACTIVATED Then
      _audit_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(278)
    ElseIf Me.EnableDisableNoteAcceptor = ENUM_SAS_FLAGS.NOT_ACTIVATED Then
      _audit_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)
    End If
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7637), _audit_text)

    _audit_text = "---"
    If Me.LockEgmOnReserve = ENUM_SAS_FLAGS.SITE_DEFINED Then
      _audit_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4432)
    ElseIf Me.LockEgmOnReserve = ENUM_SAS_FLAGS.ACTIVATED Then
      _audit_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(278)
    ElseIf Me.LockEgmOnReserve = ENUM_SAS_FLAGS.NOT_ACTIVATED Then
      _audit_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)
    End If
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7913), _audit_text)

    Call _auditor_data.SetField(0, GUI_FormatCurrency(Me.m_terminal.tito_min_denomination, 2), GLB_NLS_GUI_PLAYER_TRACKING.GetString(7679))

    'LTC 2018-JAN-08
    If Not WSI.Common.Misc.IsColJuegosEnabled Then
      If Me.ManufactureYear > 0 And Me.ManufacturingMonth > 0 And Me.ManufacturingDay > 0 Then
        Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8970), Convert.ToString(New Date(Me.ManufactureYear, Me.ManufacturingMonth, Me.ManufacturingDay)))
      Else

        Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8970), IIf(Me.ManufactureYear <= 0, AUDIT_NONE_STRING, GUI_FormatNumber(Me.ManufactureYear, 0)))
      End If

    End If


    Return _auditor_data

  End Function ' AuditorData

  '----------------------------------------------------------------------------
  ' PURPOSE : Gets the terminal's gmae 
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '         - GameId
  '         - GameName
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '
  ' NOTES :

  Public Function TerminalGame(ByRef GameId As Integer, _
                               ByRef GameName As String) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _sql_query As String
    Dim _data_table As DataTable

    GameId = 0
    GameName = ""

    Try
      If Me.Type = TerminalTypes.T3GS Then
        _sql_query = "SELECT GM_GAME_ID, GM_NAME" _
                    & " FROM GAMES" _
                   & " WHERE ( GM_GAME_ID IN ( SELECT TGT_TARGET_GAME_ID" _
                                             & " FROM TERMINAL_GAME_TRANSLATION" _
                                            & " WHERE TGT_TERMINAL_ID = " & Me.TerminalId.ToString() _
                                              & " AND TGT_SOURCE_GAME_ID IN ( SELECT GM_GAME_ID" _
                                                                            & " FROM GAMES" _
                                                                           & " WHERE GM_NAME = 'GAME_3GS'" _
                                                                         & ")" _
                                          & ")" _
                          & ")"

        _data_table = GUI_GetTableUsingSQL(_sql_query, 5000)

        If IsNothing(_data_table) Then
          Return ENUM_STATUS.STATUS_ERROR
        End If

        If _data_table.Rows.Count() < 1 Then
          _data_table = Nothing

          _sql_query = "SELECT GM_GAME_ID, GM_NAME" _
                      & " FROM GAMES" _
                     & " WHERE GM_NAME = 'GAME_3GS'"

          _data_table = GUI_GetTableUsingSQL(_sql_query, 5000)

          If IsNothing(_data_table) Then
            Return ENUM_STATUS.STATUS_ERROR
          End If

          If _data_table.Rows.Count() < 1 Then
            Return ENUM_STATUS.STATUS_ERROR
          End If

          Return ENUM_STATUS.STATUS_ERROR
        End If

        GameId = _data_table.Rows(0).Item("GM_GAME_ID")
        GameName = _data_table.Rows(0).Item("GM_NAME")

        Return ENUM_STATUS.STATUS_OK
      Else
      End If
    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

  End Function ' TerminalGame

  Public Function CloneTerminal() As Boolean
    Dim _rc As Integer
    Dim _terminal_cloned As CLASS_TERMINAL
    Dim _ctxt As Int32

    Try
      ' Read current terminal
      _rc = ReadTerminal(Me.m_terminal, _ctxt)

      If ENUM_CONFIGURATION_RC.CONFIGURATION_OK <> _rc Then
        Return False
      End If

      ' Duplicate terminal
      _terminal_cloned = Duplicate()

      ' Retire old current terminal
      If Me.m_terminal.status <> WSI.Common.TerminalStatus.RETIRED Then
        Me.m_terminal.status = WSI.Common.TerminalStatus.RETIRED
        Me.m_terminal.retirement_date = WGDB.Now
      End If

      Me.m_terminal.external_id = ""

      UpdateTerminal(Me.m_terminal, _ctxt)

      If ENUM_CONFIGURATION_RC.CONFIGURATION_OK <> _rc Then
        Return False
      End If

      ' Active new terminal
      _terminal_cloned.m_terminal.status = WSI.Common.TerminalStatus.ACTIVE
      _terminal_cloned.m_terminal.activation_date = WGDB.Now

      ' Insert terminal
      _rc = InsertTerminal(_terminal_cloned.m_terminal, _ctxt)

      If ENUM_CONFIGURATION_RC.CONFIGURATION_OK <> _rc Then
        Return False
      End If

      ' Increment change id value
      _terminal_cloned.m_terminal.change_id += 1

      ' Audit changes
      RegisterAuditCloneTerminal(Me.m_terminal, _terminal_cloned.m_terminal)

      ' Refresh temp terminals
      TerminalReport.ForceRefresh()

      Return True

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return False

  End Function ' CloneTerminal

  ' PURPOSE: Audit import action
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub RegisterAuditCloneTerminal(ByVal OldTerminal As TYPE_TERMINAL, ByVal NewTerminal As TYPE_TERMINAL)
    Dim _old_auditor As CLASS_AUDITOR_DATA
    Dim _new_auditor As CLASS_AUDITOR_DATA
    Dim _old_value As String
    Dim _new_value As String

    _old_auditor = New CLASS_AUDITOR_DATA(AUDIT_CODE_TERMINALS)
    _new_auditor = New CLASS_AUDITOR_DATA(AUDIT_CODE_TERMINALS)

    _old_auditor.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(6095), "")
    _new_auditor.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(6095), "")

    _old_value = OldTerminal.base_name & IIf(OldTerminal.change_id = 0, "", "-" & OldTerminal.change_id)
    _new_value = NewTerminal.base_name & IIf(NewTerminal.change_id = 0, "", "-" & NewTerminal.change_id)

    If (_old_value <> _new_value) Then
      '_current_value =  & _current_value
      _old_auditor.SetField(GLB_NLS_GUI_AUDITOR.Id(339), _old_value)
      _new_auditor.SetField(GLB_NLS_GUI_AUDITOR.Id(339), _new_value)
    End If

    ' Notify
    _new_auditor.Notify(CurrentUser.GuiId, _
                         CurrentUser.Id, _
                         CurrentUser.Name, _
                         CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.UPDATE, _
                         0, _
                         _old_auditor)

  End Sub ' RegisterAudit

#End Region ' Overrides Functions 

#Region " Private Functions "

  '----------------------------------------------------------------------------
  ' PURPOSE : Reads a gift object from DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - Item.gift_id
  '         - Context
  '
  '     - OUTPUT :
  '         - Item
  '
  ' RETURNS :
  '     - CONFIGURATION_OK
  '     - CONFIGURATION_ERROR_DB
  '
  ' NOTES :

  Private Function ReadTerminal(ByVal Item As TYPE_TERMINAL, _
                                ByVal Context As Integer) As Integer

    Dim _sql_query As String
    Dim _data_table As DataTable
    Dim _aux_data_table As DataTable
    Dim _sas_flags As Integer
    Dim _sas_flags_site_default As Integer
    Dim _is_tito_mode As Boolean
    Dim _system_mode As SYSTEM_MODE
    Dim _pcd_configuration As CLASS_PCD_CONFIGURATION

    _is_tito_mode = TITO.Utils.IsTitoMode

    ' Read System mode
    _system_mode = WSI.Common.Misc.SystemMode()

    If _is_tito_mode Then
      _sql_query = "SELECT TE_NAME" _
                    + ", TE_STATUS" _
                    + ", TE_EXTERNAL_ID" _
                    + ", TE_BLOCKED" _
                    + ", TE_PROVIDER_ID" _
                    + ", TE_BUILD_ID" _
                    + ", TE_CLIENT_ID" _
                    + ", TE_VENDOR_ID" _
                    + ", TE_RETIREMENT_DATE" _
                    + ", TE_RETIREMENT_REQUESTED" _
                    + ", TE_TERMINAL_TYPE" _
                    + ", TT.TT_NAME" _
                    + ", ISNULL(TE_MULTI_DENOMINATION, TE_DENOMINATION) TE_DENOMINATION" _
                    + ", TE_PROGRAM" _
                    + ", TE_REGISTRATION_CODE" _
                    + ", TE_THEORETICAL_PAYOUT" _
                    + ", TE_BANK_ID" _
                    + ", BK_NAME" _
                    + ", TE_GAME_TYPE" _
                    + ", TE_FLOOR_ID" _
                    + ", TE_ACTIVATION_DATE" _
                    + ", TE_SAS_FLAGS " _
                    + ", TE_SERIAL_NUMBER" _
                    + ", TE_CABINET_TYPE" _
                    + ", TE_JACKPOT_CONTRIBUTION_PCT" _
                    + ", TE_CONTRACT_TYPE" _
                    + ", TE_CONTRACT_ID" _
                    + ", TE_ORDER_NUMBER" _
                    + ", ISNULL(TE_ALLOWED_CASHABLE_EMISSION, (SELECT GP_KEY_VALUE FROM general_params WHERE gp_group_key = 'TITO' and gp_subject_key = 'CashableTickets.AllowEmission')) AS CASHABLE" _
                    + ", ISNULL(TE_ALLOWED_PROMO_EMISSION, (SELECT GP_KEY_VALUE FROM general_params WHERE gp_group_key = 'TITO' and gp_subject_key = 'PromotionalTickets.AllowEmission')) AS PROMOTIONAL" _
                    + ", ISNULL(TE_ALLOWED_REDEMPTION, (SELECT GP_KEY_VALUE FROM general_params WHERE gp_group_key = 'TITO' AND gp_subject_key = 'AllowRedemption'))                      AS REDEMPTION" _
                    + ", CASE WHEN " _
                    + "      (TE_ALLOWED_CASHABLE_EMISSION IS NULL" _
                    + "   AND TE_ALLOWED_PROMO_EMISSION IS NULL" _
                    + "   AND TE_ALLOWED_REDEMPTION IS NULL )" _
                    + "   THEN 0 ELSE 1 END                                                AS PERSONALIZED_CONFIG" _
                    + ", TE_MAX_ALLOWED_TI" _
                    + ", TE_MAX_ALLOWED_TO" _
                    + ", TE_SAS_FLAGS_USE_SITE_DEFAULT" _
                    + ", TE_MACHINE_ID" _
                    + ", TE_POSITION" _
                    + ", TE_TOP_AWARD" _
                    + ", TE_MAX_BET" _
                    + ", TE_NUMBER_LINES" _
                    + ", TE_GAME_THEME" _
                    + ", TE_MACHINE_SERIAL_NUMBER" _
                    + ", ISNULL(TE_METER_DELTA_ID, -1) AS TE_METER_DELTA_ID" _
                    + ", TE_MASTER_ID" _
                    + ", TE_CHANGE_ID" _
                    + ", TE_BASE_NAME" _
                    + ", TE_MACHINE_ASSET_NUMBER " _
                    + ", TE_ASSET_NUMBER " _
                    + ", TE_SMIB2EGM_COMM_TYPE" _
                    + ", TE_SMIB2EGM_CONF_ID" _
                    + ", TS_MACHINE_FLAGS" _
                    + ", TE_CREATION_DATE" _
                    + ", TE_REPLACEMENT_DATE"

      ' 08-SEP-2015 FOS
      _sql_query &= ", TE_COIN_COLLECTION"

      ' 20-NOV-2015 RGR
      _sql_query &= ", TE_EQUITY_PERCENTAGE"

      ' 22-NOV-2016 PDM
      _sql_query &= ", TE_CHK_EQUITY_PERCENTAGE"

      ' ColJuegos
      If WSI.Common.Misc.IsColJuegosEnabled Then
        _sql_query &= ", TE_BRAND_CODE" _
                    + ", TE_MODEL" _
                    + ", TE_MET_HOMOLOGATED" _
                    + ", TE_BET_CODE"
      End If

      'FloorDualCurrency
      _sql_query &= ", TE_ISO_CODE" _
                  + ", TE_SAS_ACCOUNTING_DENOM "

      _sql_query &= ", TE_TITO_HOST_ID" _
                  + ", TE_MIN_DENOMINATION"

      _sql_query &= ", TE_MIN_ALLOWED_TI" _
                  + ", ISNULL(TE_ALLOW_TRUNCATE, (SELECT GP_KEY_VALUE FROM general_params WHERE gp_group_key = 'TITO' AND gp_subject_key = 'Tickets.AllowTruncate'))                      AS TE_ALLOW_TRUNCATE"




      'LTC 2018-JAN-08
      _sql_query &= ", TE_MANUFACTURE_YEAR"
      _sql_query &= ", TE_MANUFACTURE_MONTH"
      _sql_query &= ", TE_MANUFACTURE_DAY"

      _sql_query &= " FROM TERMINALS TE" _
                    + " LEFT   JOIN TERMINAL_TYPES TT" _
                    + " ON   TT_TYPE = TE.TE_TERMINAL_TYPE" _
                    + " LEFT   JOIN BANKS" _
                    + " ON   TE_BANK_ID = BK_BANK_ID" _
                    + " LEFT JOIN TERMINAL_STATUS" _
                    + " ON TS_TERMINAL_ID = " + Item.terminal_id.ToString()
      _sql_query &= " WHERE TE_TERMINAL_ID = " + Item.terminal_id.ToString()
    Else
      _sql_query = "SELECT TE_NAME" _
                    + ", TE_STATUS" _
                    + ", TE_EXTERNAL_ID" _
                    + ", TE_BLOCKED" _
                    + ", TE_PROVIDER_ID" _
                    + ", TE_BUILD_ID" _
                    + ", TE_CLIENT_ID" _
                    + ", TE_VENDOR_ID" _
                    + ", TE_RETIREMENT_DATE" _
                    + ", TE_RETIREMENT_REQUESTED" _
                    + ", TE_TERMINAL_TYPE" _
                    + ", TT_NAME" _
                    + ", ISNULL(TE_MULTI_DENOMINATION, TE_DENOMINATION) TE_DENOMINATION" _
                    + ", TE_PROGRAM" _
                    + ", TE_REGISTRATION_CODE" _
                    + ", TE_THEORETICAL_PAYOUT" _
                    + ", TE_BANK_ID" _
                    + ", BK_NAME" _
                    + ", TE_GAME_TYPE" _
                    + ", TE_FLOOR_ID" _
                    + ", TE_ACTIVATION_DATE" _
                    + ", TE_SAS_FLAGS " _
                    + ", TE_SERIAL_NUMBER" _
                    + ", TE_CABINET_TYPE" _
                    + ", TE_JACKPOT_CONTRIBUTION_PCT" _
                    + ", TE_CONTRACT_TYPE" _
                    + ", TE_CONTRACT_ID" _
                    + ", TE_ORDER_NUMBER" _
                    + ", TE_SAS_FLAGS_USE_SITE_DEFAULT" _
                    + ", TE_MACHINE_ID" _
                    + ", TE_POSITION" _
                    + ", TE_TOP_AWARD" _
                    + ", TE_MAX_BET" _
                    + ", TE_NUMBER_LINES" _
                    + ", TE_GAME_THEME" _
                    + ", TE_MACHINE_SERIAL_NUMBER" _
                    + ", ISNULL(TE_METER_DELTA_ID, -1) AS TE_METER_DELTA_ID" _
                    + ", TE_MASTER_ID" _
                    + ", TE_CHANGE_ID" _
                    + ", TE_BASE_NAME" _
                    + ", TE_MACHINE_ASSET_NUMBER " _
                    + ", TE_ASSET_NUMBER " _
                    + ", TE_CREATION_DATE" _
                    + ", TE_REPLACEMENT_DATE"
      'DHA 31-MAR-2014
      'If _system_mode = SYSTEM_MODE.WASS Then
      _sql_query &= ", TE_AUTHENTICATION_METHOD" _
                   + ", TE_AUTHENTICATION_SEED" _
                   + ", TE_AUTHENTICATION_SIGNATURE" _
                   + ", TE_AUTHENTICATION_LAST_CHECKED" _
                   + ", TE_AUTHENTICATION_STATUS" _
                   + ", TS_MACHINE_FLAGS"
      'End If
      ' 08-SEP-2015 FOS
      _sql_query &= ", TE_COIN_COLLECTION"

      ' ColJuegos
      If WSI.Common.Misc.IsColJuegosEnabled Then
        _sql_query &= ", TE_BRAND_CODE" _
                    + ", TE_MODEL" _
                    + ", TE_MET_HOMOLOGATED" _
                    + ", TE_BET_CODE"
      End If

      'FAV 29-MAY-2015
      _sql_query &= ", TE_SMIB2EGM_COMM_TYPE" _
                     + ", TE_SMIB2EGM_CONF_ID"

      'FloorDualCurrency
      _sql_query &= ", TE_ISO_CODE" _
                  + ", TE_SAS_ACCOUNTING_DENOM "

      _sql_query &= ", TE_TITO_HOST_ID"

      'LTC 2018-JAN-08
      _sql_query &= ", TE_MANUFACTURE_YEAR"
      _sql_query &= ", TE_MANUFACTURE_MONTH"
      _sql_query &= ", TE_MANUFACTURE_DAY"

      _sql_query &= " FROM TERMINALS" _
                    + " INNER JOIN TERMINAL_TYPES ON TT_TYPE = TE_TERMINAL_TYPE " _
                    + " INNER JOIN BANKS ON TE_BANK_ID = BK_BANK_ID "
      'If _system_mode = SYSTEM_MODE.WASS Then
      _sql_query &= " LEFT JOIN TERMINAL_STATUS ON TS_TERMINAL_ID = " + Item.terminal_id.ToString()
      'End If

      _sql_query &= " WHERE TE_TERMINAL_ID = " + Item.terminal_id.ToString()

    End If
    _data_table = GUI_GetTableUsingSQL(_sql_query, 5000)

    If IsNothing(_data_table) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    If _data_table.Rows.Count() <> 1 Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    Item.name = IIf(IsDBNull(_data_table.Rows(0).Item("TE_NAME")), "", _data_table.Rows(0).Item("TE_NAME"))
    Item.status = _data_table.Rows(0).Item("TE_STATUS")
    Item.external_id = IIf(IsDBNull(_data_table.Rows(0).Item("TE_EXTERNAL_ID")), "", _data_table.Rows(0).Item("TE_EXTERNAL_ID"))
    Item.blocked = _data_table.Rows(0).Item("TE_BLOCKED")
    Item.provider_id = IIf(IsDBNull(_data_table.Rows(0).Item("TE_PROVIDER_ID")), "", _data_table.Rows(0).Item("TE_PROVIDER_ID"))
    Item.build_id = IIf(IsDBNull(_data_table.Rows(0).Item("TE_BUILD_ID")), 0, _data_table.Rows(0).Item("TE_BUILD_ID"))
    Item.client_id = IIf(IsDBNull(_data_table.Rows(0).Item("TE_CLIENT_ID")), 0, _data_table.Rows(0).Item("TE_CLIENT_ID"))
    Item.vendor_id = IIf(IsDBNull(_data_table.Rows(0).Item("TE_VENDOR_ID")), "", _data_table.Rows(0).Item("TE_VENDOR_ID"))

    ' Terminal's type 
    Item.type = _data_table.Rows(0).Item("TE_TERMINAL_TYPE")
    Item.type_name = NullTrim(_data_table.Rows(0).Item("TT_NAME"))

    ' Retirement dates
    If Not IsDBNull(_data_table.Rows(0).Item("TE_RETIREMENT_DATE")) Then
      Item.retirement_date = _data_table.Rows(0).Item("TE_RETIREMENT_DATE")
    Else
      Item.retirement_date = Nothing
    End If

    If Not IsDBNull(_data_table.Rows(0).Item("TE_RETIREMENT_REQUESTED")) Then
      Item.retirement_requested = _data_table.Rows(0).Item("TE_RETIREMENT_REQUESTED")
    Else
      Item.retirement_requested = Nothing
    End If

    If Not IsDBNull(_data_table.Rows(0).Item("TE_DENOMINATION")) Then
      Item.denomination = _data_table.Rows(0).Item("TE_DENOMINATION")
    End If

    If Not IsDBNull(_data_table.Rows(0).Item("TE_PROGRAM")) Then
      Item.program = _data_table.Rows(0).Item("TE_PROGRAM")
    Else
      Item.program = ""
    End If

    If Not IsDBNull(_data_table.Rows(0).Item("TE_REGISTRATION_CODE")) Then
      Item.tax_registration = _data_table.Rows(0).Item("TE_REGISTRATION_CODE")
    Else
      Item.tax_registration = ""
    End If

    If Not IsDBNull(_data_table.Rows(0).Item("TE_THEORETICAL_PAYOUT")) Then
      Item.theoretical_payout = _data_table.Rows(0).Item("TE_THEORETICAL_PAYOUT")
    Else
      Item.theoretical_payout = -1D 'NOTHING
    End If

    Item.bank_id = IIf(IsDBNull(_data_table.Rows(0).Item("TE_BANK_ID")), -1I, _data_table.Rows(0).Item("TE_BANK_ID"))
    Item.bank_name = IIf(IsDBNull(_data_table.Rows(0).Item("BK_NAME")), "", _data_table.Rows(0).Item("BK_NAME"))

    If _is_tito_mode Then
      If Not (IsDBNull(_data_table.Rows(0).Item("CASHABLE"))) Then
        Item.tito_allowed_cashable_emission = _data_table.Rows(0).Item("CASHABLE")
      Else
        Item.tito_allowed_cashable_emission = False
      End If
      If Not (IsDBNull(_data_table.Rows(0).Item("REDEMPTION"))) Then
        Item.tito_allowed_redemption = _data_table.Rows(0).Item("REDEMPTION")
      Else
        Item.tito_allowed_redemption = False
      End If
      If Not (IsDBNull(_data_table.Rows(0).Item("PROMOTIONAL"))) Then
        Item.tito_allowed_promotional_emission = _data_table.Rows(0).Item("PROMOTIONAL")
      Else
        Item.tito_allowed_promotional_emission = False
      End If
      If Not (IsDBNull(_data_table.Rows(0).Item("PERSONALIZED_CONFIG"))) Then
        Item.tito_configuration_type = _data_table.Rows(0).Item("PERSONALIZED_CONFIG")
      Else
        Item.tito_configuration_type = False
      End If
      If Not (IsDBNull(_data_table.Rows(0).Item("TE_MAX_ALLOWED_TI"))) Then
        Item.tito_max_allowed_ti = _data_table.Rows(0).Item("TE_MAX_ALLOWED_TI")
      Else
        Item.tito_max_allowed_ti = 0
      End If
      If Not (IsDBNull(_data_table.Rows(0).Item("TE_MAX_ALLOWED_TO"))) Then
        Item.tito_max_allowed_to = _data_table.Rows(0).Item("TE_MAX_ALLOWED_TO")
      Else
        Item.tito_max_allowed_to = 0
      End If

      If Not IsDBNull(_data_table.Rows(0).Item("TE_EQUITY_PERCENTAGE")) Then
        Item.equity_percentage = _data_table.Rows(0).Item("TE_EQUITY_PERCENTAGE")
      Else
        Item.equity_percentage = 0
      End If

      If Not IsDBNull(_data_table.Rows(0).Item("TE_CHK_EQUITY_PERCENTAGE")) Then
        Item.chk_equity_percentage = _data_table.Rows(0).Item("TE_CHK_EQUITY_PERCENTAGE")
      Else
        Item.chk_equity_percentage = False
      End If

      If Not IsDBNull(_data_table.Rows(0).Item("TE_MIN_DENOMINATION")) Then
        Item.tito_min_denomination = _data_table.Rows(0).Item("TE_MIN_DENOMINATION")
      Else
        Item.tito_min_denomination = 0
      End If

      If Not (IsDBNull(_data_table.Rows(0).Item("TE_MIN_ALLOWED_TI"))) Then
        Item.tito_min_allowed_ti = _data_table.Rows(0).Item("TE_MIN_ALLOWED_TI")
      Else
        Item.tito_min_allowed_ti = 0
      End If

      If Not (IsDBNull(_data_table.Rows(0).Item("TE_ALLOW_TRUNCATE"))) Then
        Item.tito_allow_truncate = _data_table.Rows(0).Item("TE_ALLOW_TRUNCATE")
      Else
        Item.tito_allow_truncate = False
      End If

    End If

    ' Serial Number
    Item.serial_number = IIf(IsDBNull(_data_table.Rows(0).Item("TE_SERIAL_NUMBER")), "", _data_table.Rows(0).Item("TE_SERIAL_NUMBER"))
    Item.machine_serial_number = IIf(IsDBNull(_data_table.Rows(0).Item("TE_MACHINE_SERIAL_NUMBER")), "", _data_table.Rows(0).Item("TE_MACHINE_SERIAL_NUMBER"))

    ' Asset Number
    Item.asset_number = IIf(IsDBNull(_data_table.Rows(0).Item("TE_ASSET_NUMBER")), -1, _data_table.Rows(0).Item("TE_ASSET_NUMBER"))
    Item.machine_asset_number = IIf(IsDBNull(_data_table.Rows(0).Item("TE_MACHINE_ASSET_NUMBER")), -1, _data_table.Rows(0).Item("TE_MACHINE_ASSET_NUMBER"))
    _aux_data_table = GUI_GetTableUsingSQL("SELECT AR_NAME FROM BANKS INNER JOIN AREAS ON AR_AREA_ID = BK_AREA_ID WHERE BK_BANK_ID = " & Item.bank_id, Integer.MaxValue)

    If Not IsNothing(_aux_data_table) Then
      If _aux_data_table.Rows.Count() > 0 Then
        Item.area_name = _aux_data_table.Rows.Item(0).Item("AR_NAME")
      Else
        Item.area_name = ""
      End If
    End If

    Item.game_type = IIf(IsDBNull(_data_table.Rows(0).Item("TE_GAME_TYPE")), -1I, _data_table.Rows(0).Item("TE_GAME_TYPE"))

    _aux_data_table = GUI_GetTableUsingSQL("SELECT GT_NAME FROM GAME_TYPES WHERE GT_GAME_TYPE = " & Item.game_type & " AND GT_LANGUAGE_ID = " & Resource.LanguageId, Integer.MaxValue)

    If Not IsNothing(_aux_data_table) Then
      If _aux_data_table.Rows.Count() > 0 Then
        Item.game_type_name = _aux_data_table.Rows.Item(0).Item("GT_NAME")
      Else
        Item.game_type_name = "---"
      End If
    End If

    If Not IsDBNull(_data_table.Rows(0).Item("TE_JACKPOT_CONTRIBUTION_PCT")) Then
      Item.jackpot_contribution_pct = _data_table.Rows(0).Item("TE_JACKPOT_CONTRIBUTION_PCT")
    Else
      Item.jackpot_contribution_pct = -1D 'NOTHING
    End If

    'FAV 29-MAY-2015
    Item.communication_type = IIf(IsDBNull(_data_table.Rows(0).Item("TE_SMIB2EGM_COMM_TYPE")), 2, _data_table.Rows(0).Item("TE_SMIB2EGM_COMM_TYPE"))
    Item.smib_configuration_id = IIf(IsDBNull(_data_table.Rows(0).Item("TE_SMIB2EGM_CONF_ID")), -1I, _data_table.Rows(0).Item("TE_SMIB2EGM_CONF_ID"))

    _pcd_configuration = New CLASS_PCD_CONFIGURATION()
    _aux_data_table = _pcd_configuration.GetPCDConfigurationsById(Item.smib_configuration_id)


    If Not IsNothing(_aux_data_table) Then
      If _aux_data_table.Rows.Count() > 0 Then
        Item.smib_configuration_name = _aux_data_table.Rows.Item(0).Item("SC_NAME")
      Else
        Item.smib_configuration_name = Nothing
      End If
    End If

    Item.contract_id = IIf(IsDBNull(_data_table.Rows(0).Item("TE_CONTRACT_ID")), "", _data_table.Rows(0).Item("TE_CONTRACT_ID"))
    Item.contract_type = IIf(IsDBNull(_data_table.Rows(0).Item("TE_CONTRACT_TYPE")), "", _data_table.Rows(0).Item("TE_CONTRACT_TYPE"))
    Item.order_number = IIf(IsDBNull(_data_table.Rows(0).Item("TE_ORDER_NUMBER")), "", _data_table.Rows(0).Item("TE_ORDER_NUMBER"))

    Item.serial_number = IIf(IsDBNull(_data_table.Rows(0).Item("TE_SERIAL_NUMBER")), "", _data_table.Rows(0).Item("TE_SERIAL_NUMBER"))
    Item.machine_serial_number = IIf(IsDBNull(_data_table.Rows(0).Item("TE_MACHINE_SERIAL_NUMBER")), "", _data_table.Rows(0).Item("TE_MACHINE_SERIAL_NUMBER"))
    Item.cabinet_type = IIf(IsDBNull(_data_table.Rows(0).Item("TE_CABINET_TYPE")), "", _data_table.Rows(0).Item("TE_CABINET_TYPE"))


    Item.floor_id = IIf(IsDBNull(_data_table.Rows(0).Item("TE_FLOOR_ID")), "", _data_table.Rows(0).Item("TE_FLOOR_ID"))

    Item.machine_id = IIf(IsDBNull(_data_table.Rows(0).Item("TE_MACHINE_ID")), "", _data_table.Rows(0).Item("TE_MACHINE_ID"))
    Item.position = IIf(IsDBNull(_data_table.Rows(0).Item("TE_POSITION")), 0, _data_table.Rows(0).Item("TE_POSITION"))
    Item.top_award = IIf(IsDBNull(_data_table.Rows(0).Item("TE_TOP_AWARD")), 0, _data_table.Rows(0).Item("TE_TOP_AWARD"))
    Item.max_bet = IIf(IsDBNull(_data_table.Rows(0).Item("TE_MAX_BET")), 0, _data_table.Rows(0).Item("TE_MAX_BET"))
    Item.number_lines = IIf(IsDBNull(_data_table.Rows(0).Item("TE_NUMBER_LINES")), "", _data_table.Rows(0).Item("TE_NUMBER_LINES"))

    Item.game_theme = IIf(IsDBNull(_data_table.Rows(0).Item("TE_GAME_THEME")), "", _data_table.Rows(0).Item("TE_GAME_THEME"))

    ' ColJuegos
    If WSI.Common.Misc.IsColJuegosEnabled Then
      Item.brand_code = IIf(IsDBNull(_data_table.Rows(0).Item("TE_BRAND_CODE")), "", _data_table.Rows(0).Item("TE_BRAND_CODE"))
      Item.model = IIf(IsDBNull(_data_table.Rows(0).Item("TE_MODEL")), "", _data_table.Rows(0).Item("TE_MODEL"))
      Item.met_homologated = IIf(IsDBNull(_data_table.Rows(0).Item("TE_MET_HOMOLOGATED")), False, _data_table.Rows(0).Item("TE_MET_HOMOLOGATED"))
      Item.bet_code = IIf(IsDBNull(_data_table.Rows(0).Item("TE_BET_CODE")), "", _data_table.Rows(0).Item("TE_BET_CODE"))
    End If

    'LTC 2018-JAN-08
    Item.manufacture_year = IIf(IsDBNull(_data_table.Rows(0).Item("TE_MANUFACTURE_YEAR")), 0, _data_table.Rows(0).Item("TE_MANUFACTURE_YEAR"))

    Item.meter_delta_id = IIf(IsDBNull(_data_table.Rows(0).Item("TE_METER_DELTA_ID")), 0, _data_table.Rows(0).Item("TE_METER_DELTA_ID"))
    _aux_data_table = GUI_GetTableUsingSQL("SELECT TMD_DESCRIPTION FROM TERMINAL_METER_DELTA WHERE TMD_ID = " & Item.meter_delta_id, Integer.MaxValue)

    If Not IsNothing(_aux_data_table) Then
      If _aux_data_table.Rows.Count() > 0 Then
        Item.meter_delta_desc = _aux_data_table.Rows.Item(0).Item("TMD_DESCRIPTION")
      Else
        Item.meter_delta_desc = ""
      End If
    End If

    If Not IsDBNull(_data_table.Rows(0).Item("TE_ACTIVATION_DATE")) Then
      Item.activation_date = _data_table.Rows(0).Item("TE_ACTIVATION_DATE")
    Else
      Item.activation_date = Nothing
    End If

    ' Read post process
    '     - Adjust terminal's type name: determine whether the Win terminal is a 3GS or not
    '     - Search the last terminal's connection when the terminal was retired and there is no retirement date

    '     - Adjust terminal's type name: determine whether the Win terminal is a 3GS or not
    If Item.type = TerminalTypes.WIN Then
      _aux_data_table = Nothing

      _sql_query = "SELECT COUNT (*)" _
                  + " FROM TERMINALS_3GS" _
                 + " WHERE T3GS_TERMINAL_ID = " + Item.terminal_id.ToString()

      _aux_data_table = GUI_GetTableUsingSQL(_sql_query, 5000)

      If Not IsNothing(_aux_data_table) Then
        If _aux_data_table.Rows.Count() > 0 Then
          If _aux_data_table.Rows(0).Item(0) > 0 Then
            Item.type_name = "3GS"
          End If
        End If
      End If
    ElseIf Item.type = TerminalTypes.T3GS Then
      ' DCS 05-AUG-2015: WIG-2624 can not insert new terminals 3GS
      _sql_query = "SELECT   T3GS_SERIAL_NUMBER" _
                 + "       , T3GS_MACHINE_NUMBER" _
                 + "  FROM   TERMINALS_3GS" _
                 + " WHERE   T3GS_TERMINAL_ID = " + Item.terminal_id.ToString()

      _aux_data_table = GUI_GetTableUsingSQL(_sql_query, 5000)

      If Not IsNothing(_aux_data_table) Then
        If _aux_data_table.Rows.Count() > 0 Then
          Item.type_name = "3GS"
          ' DCS 05-AUG-2015: WIG-2624 can not insert new terminals 3GS
          Item.serial_number_3GS = _aux_data_table.Rows(0).Item(0)
          Item.machine_number_3GS = _aux_data_table.Rows(0).Item(1)
        End If
      End If
    ElseIf Item.type = TerminalTypes.T3GS Then
      ' DCS 05-AUG-2015: WIG-2624 can not insert new terminals 3GS
      _sql_query = "SELECT   T3GS_SERIAL_NUMBER" _
                 + "       , T3GS_MACHINE_NUMBER" _
                 + "  FROM   TERMINALS_3GS" _
                 + " WHERE   T3GS_TERMINAL_ID = " + Item.terminal_id.ToString()

      _aux_data_table = GUI_GetTableUsingSQL(_sql_query, 5000)

      If Not IsNothing(_aux_data_table) Then
        If _aux_data_table.Rows.Count() > 0 Then
          Item.type_name = "3GS"
          ' DCS 05-AUG-2015: WIG-2624 can not insert new terminals 3GS
          Item.serial_number_3GS = _aux_data_table.Rows(0).Item(0)
          Item.machine_number_3GS = _aux_data_table.Rows(0).Item(1)
        End If
      End If
    End If

    Dim _4bcd As Int32
    Dim _5bcd As Int32

    _sas_flags = _data_table.Rows(0).Item("TE_SAS_FLAGS")
    _sas_flags_site_default = _data_table.Rows(0).Item("TE_SAS_FLAGS_USE_SITE_DEFAULT")

    Item.extended_meter = GetEnumSasFlag(_sas_flags, _sas_flags_site_default, _4bcd, _5bcd)
    Item.bcd4 = _4bcd
    Item.bcd5 = _5bcd

    '   - Credits play mode
    Item.credits_play_mode_sas = ENUM_SAS_FLAGS.SYSTEM_DEFINED

    If WSI.Common.Terminal.SASFlagActived(_sas_flags_site_default, SAS_FLAGS.CREDITS_PLAY_MODE_SAS_MACHINE) Then
      Item.credits_play_mode_sas = ENUM_SAS_FLAGS.SITE_DEFINED
    ElseIf WSI.Common.Terminal.SASFlagActived(_sas_flags, SAS_FLAGS.CREDITS_PLAY_MODE_SAS_MACHINE) Then
      Item.credits_play_mode_sas = ENUM_SAS_FLAGS.MACHINE_DEFINED
    End If

    '   - Use command 0x1B Handpays
    Item.use_cmd_0x1B_handpays = Cmd0x1Bhandpays(_sas_flags_site_default, _sas_flags)

    '   - Special Progressive Meter
    Item.progressive_meter = ENUM_SAS_FLAGS.NOT_ACTIVATED

    If WSI.Common.Terminal.SASFlagActived(_sas_flags_site_default, SAS_FLAGS.SPECIAL_PROGRESSIVE_METER) Then
      Item.progressive_meter = ENUM_SAS_FLAGS.SITE_DEFINED
    ElseIf WSI.Common.Terminal.SASFlagActived(_sas_flags, SAS_FLAGS.SPECIAL_PROGRESSIVE_METER) Then
      Item.progressive_meter = ENUM_SAS_FLAGS.ACTIVATED
    End If

    ' RAB: Product Backlog Item 8857:Bally 3.5 BCDs: GUI: Modificaci�n de la pantalla de edici�n de terminales
    '   - No RTE
    If WSI.Common.Terminal.SASFlagActived(_sas_flags, SAS_FLAGS.USE_FLAGS_FORCE_NO_RTE) Then
      Item.no_RTE = True
    Else
      Item.no_RTE = False
    End If

    '   - Single byte
    If WSI.Common.Terminal.SASFlagActived(_sas_flags, SAS_FLAGS.ALLOW_SINGLE_BYTE) Then
      Item.single_byte = True
    Else
      Item.single_byte = False
    End If

    '   - Ignore no bet plays
    If WSI.Common.Terminal.SASFlagActived(_sas_flags, SAS_FLAGS.SAS_FLAGS_IGNORE_NO_BET_PLAYS) Then
      Item.ignore_no_bet_plays = True
    Else
      Item.ignore_no_bet_plays = False
    End If

    '   - No AFT on lock 0
    If WSI.Common.Terminal.SASFlagActived(_sas_flags, SAS_FLAGS.SAS_FLAGS_AFT_ALTHOUGH_LOCK_ZERO) Then
      Item.aft_although_lock_0 = True
    Else
      Item.aft_although_lock_0 = False
    End If

    '   - Enable/Disable note acceptor
    Item.enable_disable_note_acceptor = ENUM_SAS_FLAGS.NOT_ACTIVATED

    If WSI.Common.Terminal.SASFlagActived(_sas_flags_site_default, SAS_FLAGS.SAS_FLAGS_ENABLE_DISABLE_NOTE_ACCEPTOR) Then
      Item.enable_disable_note_acceptor = ENUM_SAS_FLAGS.SITE_DEFINED
    ElseIf WSI.Common.Terminal.SASFlagActived(_sas_flags, SAS_FLAGS.SAS_FLAGS_ENABLE_DISABLE_NOTE_ACCEPTOR) Then
      Item.enable_disable_note_acceptor = ENUM_SAS_FLAGS.ACTIVATED
    End If

    '   - Reserve terminal
    Item.reserve_terminal = ENUM_SAS_FLAGS.NOT_ACTIVATED

    If WSI.Common.Terminal.SASFlagActived(_sas_flags_site_default, SAS_FLAGS.SAS_FLAGS_RESERVE_TERMINAL) Then
      Item.reserve_terminal = ENUM_SAS_FLAGS.SITE_DEFINED
    ElseIf WSI.Common.Terminal.SASFlagActived(_sas_flags, SAS_FLAGS.SAS_FLAGS_RESERVE_TERMINAL) Then
      Item.reserve_terminal = ENUM_SAS_FLAGS.ACTIVATED
    End If

    '   - Lock EGM on reserve
    Item.lock_egm_on_reserve = ENUM_SAS_FLAGS.NOT_ACTIVATED

    If WSI.Common.Terminal.SASFlagActived(_sas_flags_site_default, SAS_FLAGS.SAS_FLAGS_LOCK_EGM_ON_RESERVE) Then
      Item.lock_egm_on_reserve = ENUM_SAS_FLAGS.SITE_DEFINED
    ElseIf WSI.Common.Terminal.SASFlagActived(_sas_flags, SAS_FLAGS.SAS_FLAGS_LOCK_EGM_ON_RESERVE) Then
      Item.lock_egm_on_reserve = ENUM_SAS_FLAGS.ACTIVATED
    End If

    'DHA 31-MAR-2014
    If _system_mode = SYSTEM_MODE.WASS Then
      If Not IsDBNull(_data_table.Rows(0).Item("TE_AUTHENTICATION_METHOD")) Then
        Item.authentication_method = _data_table.Rows(0).Item("TE_AUTHENTICATION_METHOD")
      Else
        Item.authentication_method = Nothing
      End If

      If Not IsDBNull(_data_table.Rows(0).Item("TE_AUTHENTICATION_SEED")) Then
        Item.authentication_seed = _data_table.Rows(0).Item("TE_AUTHENTICATION_SEED")
      Else
        Item.authentication_seed = Nothing
      End If

      If Not IsDBNull(_data_table.Rows(0).Item("TE_AUTHENTICATION_SIGNATURE")) Then
        Item.authentication_signature = _data_table.Rows(0).Item("TE_AUTHENTICATION_SIGNATURE")
      Else
        Item.authentication_signature = Nothing
      End If
      If Not IsDBNull(_data_table.Rows(0).Item("TE_AUTHENTICATION_LAST_CHECKED")) Then
        Item.authentication_last_checked = _data_table.Rows(0).Item("TE_AUTHENTICATION_LAST_CHECKED")
      Else
        Item.authentication_last_checked = Nothing
      End If

      If Not IsDBNull(_data_table.Rows(0).Item("TE_AUTHENTICATION_STATUS")) Then
        Item.authentication_status = _data_table.Rows(0).Item("TE_AUTHENTICATION_STATUS")
      Else
        Item.authentication_status = Nothing
      End If
    End If

    If Not IsDBNull(_data_table.Rows(0).Item("TS_MACHINE_FLAGS")) Then
      Item.machine_status = _data_table.Rows(0).Item("TS_MACHINE_FLAGS")
    Else
      Item.machine_status = Nothing
    End If


    Item.master_id = _data_table.Rows(0).Item("TE_MASTER_ID")
    Item.change_id = _data_table.Rows(0).Item("TE_CHANGE_ID")
    Item.base_name = _data_table.Rows(0).Item("TE_BASE_NAME")

    Item.IsCoinCollection = _data_table.Rows(0).Item("TE_COIN_COLLECTION")

    If Not IsDBNull(_data_table.Rows(0).Item("TE_ISO_CODE")) Then
      Item.currency_iso_code = _data_table.Rows(0).Item("TE_ISO_CODE")
    Else
      Item.currency_iso_code = Nothing
    End If

    If Not IsDBNull(_data_table.Rows(0).Item("TE_SAS_ACCOUNTING_DENOM")) Then
      Item.accouting_denomination = _data_table.Rows(0).Item("TE_SAS_ACCOUNTING_DENOM")
    Else
      Item.accouting_denomination = Nothing
    End If

    If Not IsDBNull(_data_table.Rows(0).Item("TE_TITO_HOST_ID")) Then
      Item.sas_host_id = _data_table.Rows(0).Item("TE_TITO_HOST_ID")
    Else
      Item.sas_host_id = 0
    End If

    Item.creation_date = _data_table.Rows(0).Item("TE_CREATION_DATE")
    If Not IsDBNull(_data_table.Rows(0).Item("TE_REPLACEMENT_DATE")) Then
      Item.replacement_date = _data_table.Rows(0).Item("TE_REPLACEMENT_DATE")
    Else
      Item.replacement_date = Nothing
    End If

    'LTC 2018-JAN-08
    If Not IsDBNull(_data_table.Rows(0).Item("TE_MANUFACTURE_MONTH")) Then
      Item.manufacturing_month = _data_table.Rows(0).Item("TE_MANUFACTURE_MONTH")
    Else
      Item.manufacturing_month = 0
    End If

    If Not IsDBNull(_data_table.Rows(0).Item("TE_MANUFACTURE_DAY")) Then
      Item.manufacturing_day = _data_table.Rows(0).Item("TE_MANUFACTURE_DAY")
    Else
      Item.manufacturing_day = 0
    End If

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

  End Function ' ReadTerminal

  '----------------------------------------------------------------------------
  ' PURPOSE : Deletes a Terminal object from DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - Context
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - CONFIGURATION_OK
  '     - CONFIGURATION_ERROR_DB
  '
  ' NOTES :

  Private Function DeleteTerminal(ByVal Id As Integer, _
                                  ByVal Context As Integer) As Integer

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR

  End Function ' DeleteTerminal

  '----------------------------------------------------------------------------
  ' PURPOSE : Adds a Terminal object into DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - Item
  '         - Context
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - CONFIGURATION_OK
  '     - CONFIGURATION_ERROR_DB
  '
  ' NOTES :

  Private Function InsertTerminal(ByVal Item As TYPE_TERMINAL, _
                                  ByVal Context As Integer) As Integer
    Dim _sql_tx As System.Data.SqlClient.SqlTransaction = Nothing
    Dim _data_table As DataTable
    Dim _sql_command As System.Data.SqlClient.SqlCommand = Nothing
    Dim _is_tito_mode As Boolean
    Dim _system_mode As SYSTEM_MODE
    Dim _sb As System.Text.StringBuilder
    Dim _param As SqlClient.SqlParameter
    Dim _sas_flags As SAS_FLAGS
    Dim _sas_flags_use_site_default As SAS_FLAGS
    Dim _communication_type As SMIB_COMMUNICATION_TYPE
    Dim _num_rows_to_insert As Integer

    _is_tito_mode = TITO.Utils.IsTitoMode
    _system_mode = WSI.Common.Misc.SystemMode()

    Try
      '   - Check whether there is no other terminal with the same name 
      _sb = New System.Text.StringBuilder
      _sb.AppendLine("SELECT   COUNT (*)")
      _sb.AppendLine("  FROM   TERMINALS")
      _sb.AppendLine(" WHERE   TE_NAME = '" + Item.name.Replace("'", "''") + "'")
      _sb.AppendLine("   AND   TE_CHANGE_ID <> " + Item.change_id.ToString())
      _sb.AppendLine("   AND   TE_MASTER_ID <> " + Item.master_id.ToString())

      _data_table = GUI_GetTableUsingSQL(_sb.ToString(), 5000)
      If Not IsNothing(_data_table) Then
        If _data_table.Rows.Count() > 0 Then
          If _data_table.Rows(0).Item(0) > 0 Then
            Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
          End If
        End If
      End If

      '   - Insert the terminals's data 
      If Not GUI_BeginSQLTransaction(_sql_tx) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      _sb = New System.Text.StringBuilder
      _sb.AppendLine(" INSERT INTO   TERMINALS ")
      _sb.AppendLine("             ( TE_BASE_NAME ")
      _sb.AppendLine("             , TE_STATUS ")
      _sb.AppendLine("             , TE_EXTERNAL_ID ")
      _sb.AppendLine("             , TE_BLOCKED ")
      _sb.AppendLine("             , TE_PROVIDER_ID ")
      _sb.AppendLine("             , TE_VENDOR_ID ")
      _sb.AppendLine("             , TE_RETIREMENT_DATE ")
      _sb.AppendLine("             , TE_RETIREMENT_REQUESTED ")
      _sb.AppendLine("             , TE_MULTI_DENOMINATION ")
      _sb.AppendLine("             , TE_PROGRAM ")
      _sb.AppendLine("             , TE_REGISTRATION_CODE ")
      _sb.AppendLine("             , TE_THEORETICAL_PAYOUT ")
      _sb.AppendLine("             , TE_BANK_ID ")
      _sb.AppendLine("             , TE_GAME_TYPE ")
      _sb.AppendLine("             , TE_SAS_FLAGS ")
      _sb.AppendLine("             , TE_FLOOR_ID ")
      _sb.AppendLine("             , TE_SERIAL_NUMBER ")
      _sb.AppendLine("             , TE_CABINET_TYPE ")
      _sb.AppendLine("             , TE_JACKPOT_CONTRIBUTION_PCT ")
      _sb.AppendLine("             , TE_CONTRACT_TYPE ")
      _sb.AppendLine("             , TE_CONTRACT_ID ")
      _sb.AppendLine("             , TE_ORDER_NUMBER ")
      _sb.AppendLine("             , TE_SAS_FLAGS_USE_SITE_DEFAULT ")
      _sb.AppendLine("             , TE_MACHINE_ID ")
      _sb.AppendLine("             , TE_POSITION ")
      _sb.AppendLine("             , TE_TOP_AWARD ")
      _sb.AppendLine("             , TE_MAX_BET ")
      _sb.AppendLine("             , TE_NUMBER_LINES ")
      _sb.AppendLine("             , TE_GAME_THEME ")
      _sb.AppendLine("             , TE_TERMINAL_TYPE ")
      _sb.AppendLine("             , TE_TYPE ")

      If _is_tito_mode Then
        _sb.AppendLine("             , TE_ALLOWED_CASHABLE_EMISSION ")
        _sb.AppendLine("             , TE_ALLOWED_PROMO_EMISSION ")
        _sb.AppendLine("             , TE_ALLOWED_REDEMPTION ")
        _sb.AppendLine("             , TE_MAX_ALLOWED_TI ")
        _sb.AppendLine("             , TE_MAX_ALLOWED_TO ")
        _sb.AppendLine("             , TE_MIN_DENOMINATION ")
        _sb.AppendLine("             , TE_MIN_ALLOWED_TI ")
        _sb.AppendLine("             , TE_ALLOW_TRUNCATE ")
      End If

      _sb.AppendLine("             , TE_MACHINE_SERIAL_NUMBER")
      _sb.AppendLine("             , TE_METER_DELTA_ID")
      _sb.AppendLine("             , TE_ASSET_NUMBER")
      _sb.AppendLine("             , TE_MACHINE_ASSET_NUMBER")
      _sb.AppendLine("             , TE_MASTER_ID")
      _sb.AppendLine("             , TE_CHANGE_ID")
      _sb.AppendLine("             , TE_SMIB2EGM_COMM_TYPE")
      _sb.AppendLine("             , TE_COIN_COLLECTION")

      _sb.AppendLine("             , TE_ISO_CODE")
      _sb.AppendLine("             , TE_CREATION_DATE")
      If Not IsDBNull(Item.replacement_date) AndAlso Item.replacement_date <> DateTime.MinValue Then
        _sb.AppendLine("             , TE_REPLACEMENT_DATE")
      End If

      _sb.AppendLine("             ) ")
      _sb.AppendLine("             VALUES")
      _sb.AppendLine("             ( @name ")
      _sb.AppendLine("             , @status ")
      _sb.AppendLine("             , @external_id ")
      _sb.AppendLine("             , @blocked ")
      _sb.AppendLine("             , @provider_id ")
      _sb.AppendLine("             , @vendor_id ")
      _sb.AppendLine("             , @retirement_date ")
      _sb.AppendLine("             , NULL ")
      _sb.AppendLine("             , @denomination ")
      _sb.AppendLine("             , @program ")
      _sb.AppendLine("             , @tax_registration ")
      _sb.AppendLine("             , @theoretical_payout ")
      _sb.AppendLine("             , @bank_id ")
      _sb.AppendLine("             , @game_type ")
      _sb.AppendLine("             , @pSasFlags ")
      _sb.AppendLine("             , @floor_id ")
      _sb.AppendLine("             , @pserial_number ")
      _sb.AppendLine("             , @pcabinet_type ")
      _sb.AppendLine("             , @pjackpot_contribution ")
      _sb.AppendLine("             , @contract_type ")
      _sb.AppendLine("             , @contract_id ")
      _sb.AppendLine("             , @order_number ")
      _sb.AppendLine("             , @pSasFlagsUseSiteDefault ")
      _sb.AppendLine("             , @pMachineId ")
      _sb.AppendLine("             , @pPosition ")
      _sb.AppendLine("             , @pTopAward ")
      _sb.AppendLine("             , @pMaxBet ")
      _sb.AppendLine("             , @pNumberLines ")
      _sb.AppendLine("             , @pGameTheme")
      _sb.AppendLine("             , @pTerminalType")
      _sb.AppendLine("             , @pType")

      If _is_tito_mode Then
        _sb.AppendLine("             , @pCashable ")
        _sb.AppendLine("             , @pPromotional ")
        _sb.AppendLine("             , @pRedemption ")
        _sb.AppendLine("             , @pMaxAllowedTi ")
        _sb.AppendLine("             , @pMaxAllowedTo ")
        _sb.AppendLine("             , @pMinDenomination ")
        _sb.AppendLine("             , @pMinAllowedTi ")
        _sb.AppendLine("             , @pAllowTruncate ")
      End If

      _sb.AppendLine("             , @pMachineSerialNumber")
      _sb.AppendLine("             , @pMeterDeltaId")
      _sb.AppendLine("             , @pAssetNumber")
      _sb.AppendLine("             , @pMachineAssetNumber")
      _sb.AppendLine("             , @pMasterId")
      _sb.AppendLine("             , @pChangeId")
      _sb.AppendLine("             , @pCommunicationType")
      _sb.AppendLine("             , @pCoinCollection")

      _sb.AppendLine("             , @pCurrencyIsoCode")
      _sb.AppendLine("             , @pCreationDate")
      If Not IsDBNull(Item.replacement_date) AndAlso Item.replacement_date <> DateTime.MinValue Then
        _sb.AppendLine("             , @pReplacementDate")
      End If

      _sb.AppendLine("             )")

      _sb.AppendLine("SET @pTerminalId = SCOPE_IDENTITY()")

      If Item.IsNewOffline Then
        _sb.AppendLine(" ")
        _sb.AppendLine(" UPDATE   TERMINALS ")
        _sb.AppendLine("    SET   TE_MASTER_ID = TE_TERMINAL_ID ")
        _sb.AppendLine("  WHERE   TE_TERMINAL_ID = @pTerminalId ")
        _sb.AppendLine(" ")
        _sb.AppendLine(" DECLARE  @GameId AS INTEGER ")
        _sb.AppendLine(" ")
        _sb.AppendLine(" SELECT   @GameId = GM_GAME_ID ")
        _sb.AppendLine("   FROM   GAMES ")
        _sb.AppendLine("  WHERE   GM_NAME = 'GAME_OFFLINE' ")
        _sb.AppendLine(" ")
        _sb.AppendLine("  INSERT INTO TERMINAL_GAME_TRANSLATION ( TGT_TERMINAL_ID ")
        _sb.AppendLine("                                        , TGT_SOURCE_GAME_ID ")
        _sb.AppendLine("                                        , TGT_TARGET_GAME_ID ")
        _sb.AppendLine("                                        ) ")
        _sb.AppendLine("                                 VALUES ( @pTerminalId ")
        _sb.AppendLine("                                        , @GameId ")
        _sb.AppendLine("                                        , @GameId ")
        _sb.AppendLine("                                        ) ")
      End If

      ' DCS 05-AUG-2015: WIG-2624 can not insert new terminals 3GS
      If Item.type = TerminalTypes.T3GS Then

        _sb.AppendLine(" DECLARE @game_id AS INT                               ")
        _sb.AppendLine(" SET @game_id = (SELECT GM_GAME_ID FROM GAMES WHERE GM_NAME = 'GAME_3GS') ")

        _sb.AppendLine(" INSERT INTO   TERMINALS_3GS                                      ")
        _sb.AppendLine("             ( T3GS_VENDOR_ID                                     ")
        _sb.AppendLine("             , T3GS_SERIAL_NUMBER                                 ")
        _sb.AppendLine("             , T3GS_MACHINE_NUMBER                                ")
        _sb.AppendLine("             , T3GS_TERMINAL_ID                                   ")
        _sb.AppendLine("             )                                                    ")
        _sb.AppendLine("             VALUES                                               ")
        _sb.AppendLine("             ( @vendor_id                                         ")
        _sb.AppendLine("             , @serial_number_3gs                                 ")
        _sb.AppendLine("             , @machine_number_3gs                                ")
        _sb.AppendLine("             , @pTerminalId                                       ")
        _sb.AppendLine("             )                                                    ")

        _sb.AppendLine("  INSERT INTO TERMINAL_GAME_TRANSLATION ( TGT_TERMINAL_ID         ")
        _sb.AppendLine("                                        , TGT_SOURCE_GAME_ID      ")
        _sb.AppendLine("                                        , TGT_TARGET_GAME_ID      ")
        _sb.AppendLine("                                        )                         ")
        _sb.AppendLine("                                 VALUES ( @pTerminalId            ")
        _sb.AppendLine("                                        , @game_id                ")
        _sb.AppendLine("                                        , @game_id                ")
        _sb.AppendLine("                                        )                         ")
      End If

      _sql_command = New SqlCommand(_sb.ToString())
      _sql_command.Transaction = _sql_tx
      _sql_command.Connection = _sql_tx.Connection

      _sql_command.Parameters.Add("@name", SqlDbType.NVarChar).Value = IIf(Item.name = "", DBNull.Value, Item.base_name)
      _sql_command.Parameters.Add("@external_id", SqlDbType.NVarChar).Value = IIf(Item.external_id = "", DBNull.Value, Item.external_id)
      _sql_command.Parameters.Add("@provider_id", SqlDbType.NVarChar).Value = IIf(Item.provider_id = "", DBNull.Value, Item.provider_id)
      _sql_command.Parameters.Add("@vendor_id", SqlDbType.NVarChar).Value = IIf(Item.vendor_id = "", DBNull.Value, Item.vendor_id)
      _sql_command.Parameters.Add("@status", SqlDbType.Int).Value = Item.status
      _sql_command.Parameters.Add("@blocked", SqlDbType.Bit).Value = Item.blocked
      _sql_command.Parameters.Add("@terminal_id", SqlDbType.BigInt).Value = Item.terminal_id
      _sql_command.Parameters.Add("@retirement_date", SqlDbType.DateTime).Value = IIf(Item.status = WSI.Common.TerminalStatus.RETIRED, _
                                                                                      Item.retirement_date, _
                                                                                      DBNull.Value)
      _sql_command.Parameters.Add("@denomination", SqlDbType.NVarChar).Value = IIf(Item.denomination Is Nothing, DBNull.Value, Item.denomination)

      If String.IsNullOrEmpty(Item.program) Then
        _sql_command.Parameters.Add("@program", SqlDbType.NVarChar).Value = DBNull.Value
      Else
        _sql_command.Parameters.Add("@program", SqlDbType.NVarChar).Value = Item.program
      End If

      If String.IsNullOrEmpty(Item.tax_registration) Then
        _sql_command.Parameters.Add("@tax_registration", SqlDbType.NVarChar).Value = DBNull.Value
      Else
        _sql_command.Parameters.Add("@tax_registration", SqlDbType.NVarChar).Value = Item.tax_registration
      End If

      If Item.theoretical_payout = -1D Then ' NOTHING
        _sql_command.Parameters.Add("@theoretical_payout", SqlDbType.Decimal).Value = DBNull.Value
      Else
        _sql_command.Parameters.Add("@theoretical_payout", SqlDbType.Decimal).Value = Item.theoretical_payout
      End If

      If Item.bank_id = -1I Then
        _sql_command.Parameters.Add("@bank_id", SqlDbType.Int).Value = DBNull.Value
      Else
        _sql_command.Parameters.Add("@bank_id", SqlDbType.Int).Value = Item.bank_id
      End If

      _sql_command.Parameters.Add("@game_type", SqlDbType.Int).Value = Item.game_type
      _sql_command.Parameters.Add("@floor_id", SqlDbType.NVarChar).Value = IIf(Item.floor_id = "", DBNull.Value, Item.floor_id)



      _sas_flags = SAS_FLAGS.NONE
      _sas_flags_use_site_default = SAS_FLAGS.NONE

      If Item.extended_meter = ENUM_SAS_FLAGS.SITE_DEFINED Then
        _sas_flags_use_site_default = _sas_flags_use_site_default Or SAS_FLAGS.USE_EXTENDED_METERS
      ElseIf Item.extended_meter = ENUM_SAS_FLAGS.ACTIVATED Then
        _sas_flags = _sas_flags Or SAS_FLAGS.USE_EXTENDED_METERS
      End If

      If Item.credits_play_mode_sas = ENUM_SAS_FLAGS.SITE_DEFINED Then
        _sas_flags_use_site_default = _sas_flags_use_site_default Or SAS_FLAGS.CREDITS_PLAY_MODE_SAS_MACHINE
      ElseIf Item.credits_play_mode_sas = ENUM_SAS_FLAGS.MACHINE_DEFINED Then
        _sas_flags = _sas_flags Or SAS_FLAGS.CREDITS_PLAY_MODE_SAS_MACHINE
      End If

      If Item.use_cmd_0x1B_handpays = ENUM_SAS_FLAGS.SITE_DEFINED Then
        _sas_flags_use_site_default = _sas_flags_use_site_default Or SAS_FLAGS.USE_HANDPAY_INFORMATION
        _sas_flags_use_site_default = _sas_flags_use_site_default Or SAS_FLAGS.SAS_FLAGS_AUTOMATIC_HANDPAY_PAYMENT
      ElseIf Item.use_cmd_0x1B_handpays = ENUM_SAS_FLAGS.ACTIVATED Then
        _sas_flags = _sas_flags Or SAS_FLAGS.USE_HANDPAY_INFORMATION
      ElseIf Item.use_cmd_0x1B_handpays = ENUM_SAS_FLAGS.ACTIVATED_AND_AUTOMATICALLY Then
        _sas_flags = _sas_flags Or SAS_FLAGS.USE_HANDPAY_INFORMATION
        _sas_flags = _sas_flags Or SAS_FLAGS.SAS_FLAGS_AUTOMATIC_HANDPAY_PAYMENT
      End If

      '   - Special Progressive Meter
      If Item.progressive_meter = ENUM_SAS_FLAGS.SITE_DEFINED Then
        _sas_flags_use_site_default = _sas_flags_use_site_default Or SAS_FLAGS.SPECIAL_PROGRESSIVE_METER
      ElseIf Item.progressive_meter = ENUM_SAS_FLAGS.ACTIVATED Then
        _sas_flags = _sas_flags Or SAS_FLAGS.SPECIAL_PROGRESSIVE_METER
      End If

      _sql_command.Parameters.Add("@pSasFlags", SqlDbType.Int).Value = _sas_flags
      _sql_command.Parameters.Add("@pSasFlagsUseSiteDefault", SqlDbType.Int).Value = _sas_flags_use_site_default
      _sql_command.Parameters.Add("@pserial_number", SqlDbType.NVarChar).Value = IIf(String.IsNullOrEmpty(Item.serial_number), DBNull.Value, Item.serial_number)
      _sql_command.Parameters.Add("@pcabinet_type", SqlDbType.NVarChar).Value = IIf(String.IsNullOrEmpty(Item.cabinet_type), DBNull.Value, Item.cabinet_type)
      _sql_command.Parameters.Add("@pjackpot_contribution", SqlDbType.Decimal).Value = IIf(Item.jackpot_contribution_pct = -1D, DBNull.Value, Item.jackpot_contribution_pct)
      _sql_command.Parameters.Add("@contract_type", SqlDbType.NVarChar).Value = IIf(String.IsNullOrEmpty(Item.contract_type), DBNull.Value, Item.contract_type)
      _sql_command.Parameters.Add("@contract_id", SqlDbType.NVarChar).Value = IIf(String.IsNullOrEmpty(Item.contract_id), DBNull.Value, Item.contract_id)
      _sql_command.Parameters.Add("@order_number", SqlDbType.NVarChar).Value = IIf(String.IsNullOrEmpty(Item.order_number), DBNull.Value, Item.order_number)
      _sql_command.Parameters.Add("@pMachineId", SqlDbType.NVarChar).Value = IIf(String.IsNullOrEmpty(Item.machine_id), DBNull.Value, Item.machine_id)
      _sql_command.Parameters.Add("@pPosition", SqlDbType.Int).Value = IIf(Item.position > 0, Item.position, DBNull.Value)
      _sql_command.Parameters.Add("@pTopAward", SqlDbType.BigInt).Value = IIf(Item.top_award > 0, Item.top_award, DBNull.Value)
      _sql_command.Parameters.Add("@pMaxBet", SqlDbType.Int).Value = IIf(Item.max_bet > 0, Item.max_bet, DBNull.Value)
      _sql_command.Parameters.Add("@pNumberLines", SqlDbType.NVarChar).Value = IIf(String.IsNullOrEmpty(Item.number_lines), DBNull.Value, Item.number_lines)
      _sql_command.Parameters.Add("@pGameTheme", SqlDbType.NVarChar).Value = IIf(String.IsNullOrEmpty(Item.game_theme), DBNull.Value, Item.game_theme)

      If _is_tito_mode Then
        If Me.m_terminal.tito_configuration_type Then
          _sql_command.Parameters.Add("@pCashable", SqlDbType.Bit).Value = Item.tito_allowed_cashable_emission
          _sql_command.Parameters.Add("@pPromotional", SqlDbType.Bit).Value = Item.tito_allowed_promotional_emission
          _sql_command.Parameters.Add("@pRedemption", SqlDbType.Bit).Value = Item.tito_allowed_redemption
          _sql_command.Parameters.Add("@pMaxAllowedTi", SqlDbType.Money).Value = Item.tito_max_allowed_ti
          _sql_command.Parameters.Add("@pMaxAllowedTo", SqlDbType.Money).Value = Item.tito_max_allowed_to
          _sql_command.Parameters.Add("@pMinAllowedTi", SqlDbType.Money).Value = Item.tito_min_allowed_ti
          _sql_command.Parameters.Add("@pAllowTruncate", SqlDbType.Bit).Value = Item.tito_allow_truncate
          _sql_command.Parameters.Add("@pMinDenomination", SqlDbType.Money).Value = Item.tito_min_denomination
        Else
          _sql_command.Parameters.Add("@pCashable", SqlDbType.Bit).Value = DBNull.Value
          _sql_command.Parameters.Add("@pPromotional", SqlDbType.Bit).Value = DBNull.Value
          _sql_command.Parameters.Add("@pRedemption", SqlDbType.Bit).Value = DBNull.Value
          _sql_command.Parameters.Add("@pMaxAllowedTi", SqlDbType.Money).Value = DBNull.Value
          _sql_command.Parameters.Add("@pMaxAllowedTo", SqlDbType.Money).Value = DBNull.Value
          _sql_command.Parameters.Add("@pMinAllowedTi", SqlDbType.Money).Value = DBNull.Value
          _sql_command.Parameters.Add("@pAllowTruncate", SqlDbType.Bit).Value = DBNull.Value
          _sql_command.Parameters.Add("@pMinDenomination", SqlDbType.Money).Value = DBNull.Value
        End If
      End If

      _sql_command.Parameters.Add("@pTerminalType", SqlDbType.Int).Value = Item.type
      _sql_command.Parameters.Add("@pType", SqlDbType.Int).Value = 1

      _sql_command.Parameters.Add("@pMachineSerialNumber", SqlDbType.VarChar).Value = IIf(String.IsNullOrEmpty(Item.machine_serial_number), DBNull.Value, Item.machine_serial_number)
      _sql_command.Parameters.Add("@pMeterDeltaId", SqlDbType.BigInt).Value = IIf(Item.meter_delta_id >= 0, Item.meter_delta_id, DBNull.Value)
      _sql_command.Parameters.Add("@pAssetNumber", SqlDbType.BigInt).Value = IIf(Item.asset_number >= 0, Item.asset_number, DBNull.Value)
      _sql_command.Parameters.Add("@pMachineAssetNumber", SqlDbType.BigInt).Value = IIf(Item.machine_asset_number >= 0, Item.machine_asset_number, DBNull.Value)

      _sql_command.Parameters.Add("@pMasterId", SqlDbType.Int).Value = Item.master_id
      _sql_command.Parameters.Add("@pChangeId", SqlDbType.Int).Value = IIf(Not Item.IsNewOffline, Item.change_id + 1, 0)

      ' DCS 05-AUG-2015: WIG-2624 can not insert new terminals 3GS
      If Item.type = TerminalTypes.T3GS Then

        _sql_command.Parameters.Add("@serial_number_3gs", SqlDbType.NVarChar).Value = IIf(String.IsNullOrEmpty(Item.serial_number_3GS), DBNull.Value, Item.serial_number_3GS)
        _sql_command.Parameters.Add("@machine_number_3gs", SqlDbType.BigInt).Value = IIf(Item.machine_number_3GS >= 0, Item.machine_number_3GS, DBNull.Value)

      End If

      'FAV 29-MAY-2015
      _communication_type = WSI.Common.Terminal.GetDefaultCommunicationTypeByTerminalType(Item.type)
      _sql_command.Parameters.Add("@pCommunicationType", SqlDbType.Int).Value = _communication_type

      If _is_tito_mode Then
        _sql_command.Parameters.Add("@pCoinCollection", SqlDbType.Bit).Value = Item.IsCoinCollection
      Else
        _sql_command.Parameters.Add("@pCoinCollection", SqlDbType.Bit).Value = False
      End If

      _sql_command.Parameters.Add("@pCurrencyIsoCode", SqlDbType.NVarChar).Value = Item.currency_iso_code
      _sql_command.Parameters.Add("@pCreationDate", SqlDbType.DateTime).Value = Item.creation_date
      If Not IsDBNull(Item.replacement_date) AndAlso Item.replacement_date <> DateTime.MinValue Then
        _sql_command.Parameters.Add("@pReplacementDate", SqlDbType.DateTime).Value = Item.replacement_date
      End If

      _param = _sql_command.Parameters.Add("@pTerminalId", SqlDbType.Int)
      _param.Direction = ParameterDirection.Output


      ' DCS 05-AUG-2015: WIG-2624 can not insert new terminals 3GS
      _num_rows_to_insert = 1

      If Item.type = TerminalTypes.T3GS Then
        ' When insert a 3GS terminal makes 3 inserts otherwise 1
        _num_rows_to_insert = 3
      End If

      If Item.IsNewOffline Then
        _sql_command.ExecuteNonQuery()

        If _param.Value Is Nothing OrElse _param.Value = 0 Then
          GUI_EndSQLTransaction(_sql_tx, False)

          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If
      Else
        If _sql_command.ExecuteNonQuery() <> _num_rows_to_insert Then
          GUI_EndSQLTransaction(_sql_tx, False)
          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If
      End If

      Item.terminal_id = _param.Value


      ' DHA 08-APR-2014
      'If _system_mode = SYSTEM_MODE.WASS Then
      TerminalStatusFlags.DB_SetValue(TerminalStatusFlags.BITMASK_TYPE.Machine_status, Item.terminal_id, Me.m_terminal.machine_status, _sql_tx)
      'End If

      ' Commit
      If Not GUI_EndSQLTransaction(_sql_tx, True) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

    Catch ex As Exception
      ' Rollback  
      GUI_EndSQLTransaction(_sql_tx, False)
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

  End Function ' InsertTerminal

  '----------------------------------------------------------------------------
  ' PURPOSE : Updates the given Terminal object into the DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - Item
  '         - Context
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - CONFIGURATION_OK
  '     - CONFIGURATION_ERROR_DB
  '
  ' NOTES :

  Private Function UpdateTerminal(ByVal Item As TYPE_TERMINAL, _
                                  ByVal Context As Integer) As Integer
    Dim _sql_query As System.Text.StringBuilder
    Dim _sql_tx As System.Data.SqlClient.SqlTransaction = Nothing
    Dim _data_table As DataTable
    Dim _previous_status As Integer
    Dim _previous_provider_id As String = String.Empty
    Dim _previous_terminal_name As String = String.Empty
    Dim _previous_terminal_base As String = String.Empty
    Dim _previous_master_id As Int64
    Dim _sql_command As System.Data.SqlClient.SqlCommand = Nothing
    Dim _num_rows_updated As Integer
    Dim _num_play_sessions As Integer
    Dim _max_started As Date
    Dim _max_finished As Date
    Dim _game_unknown_id As Integer
    Dim _sql_string_builder As System.Text.StringBuilder
    Dim _sql_cmd As SqlCommand
    Dim _is_tito_mode As Boolean
    Dim _system_mode As SYSTEM_MODE
    Dim _4bcd As Int32
    Dim _5bcd As Int32
    _is_tito_mode = TITO.Utils.IsTitoMode
    ' Read System mode
    _system_mode = WSI.Common.Misc.SystemMode()

    _sql_string_builder = New System.Text.StringBuilder()

    ' Steps
    '     - Check whether there is no other terminal with the same name 
    '     - Get previous Status
    '     - The terminal can not have play sessions after the working day related to the selected retirement date
    '     - Update the terminal's data 
    '     - 'Invalidate' the terminal's connection activity if the terminal is being retired
    '     - Delete from TERMINALS_3GS table when retiring a 3GS terminal 
    '     - Delete from GAME_METERS table when the terminal is being retired
    '     - Insert event if status changed
    '     - Change terminal name of all same master_id when terminal name is changed
    '     - Change provider id of all same master_id when provider is changed

    Try
      '   - Check whether there is no other terminal with the same name
      ' RAB 8-FEB-2016
      _sql_query = New System.Text.StringBuilder()
      _sql_query.AppendLine("SELECT COUNT (*)")
      _sql_query.AppendLine(" FROM TERMINALS")
      _sql_query.AppendLine(" WHERE   TE_BASE_NAME = '" + Item.name.Replace("'", "''") + "'")
      _sql_query.AppendLine("   AND   TE_MASTER_ID <> " + Item.master_id.ToString())

      _data_table = GUI_GetTableUsingSQL(_sql_query.ToString, 5000)
      If Not IsNothing(_data_table) Then
        If _data_table.Rows.Count() > 0 Then
          If _data_table.Rows(0).Item(0) > 0 Then
            Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
          End If
        End If
      End If

      '   - Get previous Status and other previous fields
      _sql_query = New System.Text.StringBuilder()
      _sql_query.AppendLine("SELECT TE_STATUS")
      _sql_query.AppendLine("     , TE_PROVIDER_ID")
      _sql_query.AppendLine("     , ISNULL(TE_NAME, '')")
      _sql_query.AppendLine("     , TE_MASTER_ID")
      _sql_query.AppendLine("     , TE_BASE_NAME")
      _sql_query.AppendLine(" FROM  TERMINALS")
      _sql_query.AppendLine(" WHERE TE_TERMINAL_ID = " + Item.terminal_id.ToString())

      _data_table = GUI_GetTableUsingSQL(_sql_query.ToString, 5000)
      If Not IsNothing(_data_table) Then
        If _data_table.Rows.Count() > 0 Then
          _previous_status = _data_table.Rows(0).Item(0)
          _previous_provider_id = _data_table.Rows(0).Item(1)
          _previous_terminal_name = _data_table.Rows(0).Item(2)
          _previous_master_id = _data_table.Rows(0).Item(3)
          _previous_terminal_base = _data_table.Rows(0).Item(4)
        Else
          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If
      End If

      If _previous_status <> WSI.Common.TerminalStatus.RETIRED _
           And Item.status = WSI.Common.TerminalStatus.RETIRED Then
        ' The terminal is being retired
        Item.external_id = ""
      End If

      '   - The terminal can not have play sessions after the working day related to the selected retirement date
      If Item.status = WSI.Common.TerminalStatus.RETIRED Then
        _num_play_sessions = CountPlaySessions(Item.terminal_id, _
                                               WSI.Common.Misc.Opening(Item.retirement_date.AddDays(1)), _
                                               _max_started, _
                                               _max_finished)
        If _num_play_sessions > 0 Then
          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DEPENDENCIES
        End If
      End If

      '   - Update the terminals's data 
      If Not GUI_BeginSQLTransaction(_sql_tx) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      _sql_query = New System.Text.StringBuilder()
      _sql_query.AppendLine("UPDATE TERMINALS")
      _sql_query.AppendLine(" SET TE_STATUS          = @status")
      _sql_query.AppendLine(", TE_EXTERNAL_ID     = @external_id")
      _sql_query.AppendLine(", TE_BLOCKED         = @blocked")
      _sql_query.AppendLine(", TE_PROVIDER_ID     = @provider_id")
      _sql_query.AppendLine(", TE_VENDOR_ID       = @vendor_id")
      _sql_query.AppendLine(", TE_RETIREMENT_DATE = @retirement_date")
      _sql_query.AppendLine(", TE_RETIREMENT_REQUESTED = CASE WHEN @retirement_requested_flag = 0 THEN NULL")
      _sql_query.AppendLine("                                 WHEN @retirement_requested_flag = 1 THEN TE_RETIREMENT_REQUESTED")
      _sql_query.AppendLine("                                 ELSE GETDATE()")
      _sql_query.AppendLine("                            END")
      _sql_query.AppendLine(", TE_MULTI_DENOMINATION         = @denomination")
      _sql_query.AppendLine(", TE_PROGRAM                    = @program")
      _sql_query.AppendLine(", TE_REGISTRATION_CODE          = @tax_registration")
      _sql_query.AppendLine(", TE_THEORETICAL_PAYOUT         = @theoretical_payout")
      _sql_query.AppendLine(", TE_BANK_ID                    = @bank_id")
      _sql_query.AppendLine(", TE_GAME_TYPE                  = @game_type")
      _sql_query.AppendLine(", TE_FLOOR_ID                   = @floor_id")
      _sql_query.AppendLine(", TE_SAS_FLAGS                  = @pSasFlags | (TE_SAS_FLAGS & 0x00080000)")
      _sql_query.AppendLine(", TE_SERIAL_NUMBER              = @pserial_number")
      _sql_query.AppendLine(", TE_CABINET_TYPE               = @pcabinet_type")
      _sql_query.AppendLine(", TE_JACKPOT_CONTRIBUTION_PCT   = @pjackpot_contribution")
      _sql_query.AppendLine(", TE_CONTRACT_TYPE              = @contract_type")
      _sql_query.AppendLine(", TE_CONTRACT_ID                = @contract_id")
      _sql_query.AppendLine(", TE_ORDER_NUMBER               = @order_number")
      _sql_query.AppendLine(", TE_SAS_FLAGS_USE_SITE_DEFAULT = @pSasFlagsUseSiteDefault")
      _sql_query.AppendLine(", TE_MACHINE_ID                 = @pMachineId")
      _sql_query.AppendLine(", TE_POSITION                   = @pPosition")
      _sql_query.AppendLine(", TE_TOP_AWARD                  = @pTopAward")
      _sql_query.AppendLine(", TE_MAX_BET                    = @pMaxBet")
      _sql_query.AppendLine(", TE_NUMBER_LINES               = @pNumberLines")
      _sql_query.AppendLine(", TE_GAME_THEME                 = @pGameTheme")
      _sql_query.AppendLine(", TE_MACHINE_SERIAL_NUMBER      = @pMachineSerialNumber")
      _sql_query.AppendLine(", TE_METER_DELTA_ID             = @pMeterDeltaId")
      _sql_query.AppendLine(", TE_ASSET_NUMBER      		     = @pAssetNumber")
      _sql_query.AppendLine(", TE_MACHINE_ASSET_NUMBER       = @pMachineAssetNumber")
      _sql_query.AppendLine(", TE_SMIB2EGM_COMM_TYPE         = @pCommunicationType")
      _sql_query.AppendLine(", TE_SMIB2EGM_CONF_ID           = @pSmibConfigurationId")

      ' ColJuegos
      If WSI.Common.Misc.IsColJuegosEnabled Then
        _sql_query.AppendLine(", TE_BRAND_CODE                   = @pBrandCode")
        _sql_query.AppendLine(", TE_MODEL                        = @pModel")
        _sql_query.AppendLine(", TE_MET_HOMOLOGATED              = @pMETHomologated")
        _sql_query.AppendLine(", TE_BET_CODE                     = @pBetCode")
      End If

      'LTC 2018-JAN-08
      _sql_query.AppendLine(", TE_MANUFACTURE_YEAR             = @pManufactureYear")

      ' FloorDualCurrency
      _sql_query.AppendLine(", TE_ISO_CODE                       = @pCurrencyIsoCode")

      _sql_query.AppendLine(", TE_TITO_HOST_ID                 = @pTitoHostId")

      'LTC 2018-JAN-08
      _sql_query.AppendLine(", TE_MANUFACTURE_MONTH             = @pManufacturingMonth")
      _sql_query.AppendLine(", TE_MANUFACTURE_DAY             = @pManufacturingDay")

      If _is_tito_mode Then
        _sql_query.AppendLine(", TE_ALLOWED_CASHABLE_EMISSION    = @pCashable")
        _sql_query.AppendLine(", TE_ALLOWED_PROMO_EMISSION       = @pPromotional")
        _sql_query.AppendLine(", TE_ALLOWED_REDEMPTION           = @pRedemption")
        _sql_query.AppendLine(", TE_MAX_ALLOWED_TI               = @pMaxAllowedTi")
        _sql_query.AppendLine(", TE_MAX_ALLOWED_TO               = @pMaxAllowedTo")
        _sql_query.AppendLine(", TE_MIN_DENOMINATION             = @pMinDenomination")
        _sql_query.AppendLine(", TE_COIN_COLLECTION              = @pCoinCollection")
        _sql_query.AppendLine(", TE_EQUITY_PERCENTAGE            = @pEquityPercentage")
        _sql_query.AppendLine(", TE_CHK_EQUITY_PERCENTAGE            = @pChkEquityPercentage")


        _sql_query.AppendLine(", TE_MIN_ALLOWED_TI               = @pMinAllowedTi")
        _sql_query.AppendLine(", TE_ALLOW_TRUNCATE               = @pAllowTruncate")

        _sql_query.AppendLine(" WHERE TE_TERMINAL_ID             = @terminal_id")
      Else
        _sql_query.AppendLine(" WHERE TE_TERMINAL_ID             = @terminal_id ")
      End If

      _sql_command = New SqlCommand(_sql_query.ToString)
      _sql_command.Transaction = _sql_tx
      _sql_command.Connection = _sql_tx.Connection

      '_sql_command.Parameters.Add("@base_name", SqlDbType.NVarChar).Value = IIf(Item.name = "", DBNull.Value, Item.name)
      _sql_command.Parameters.Add("@external_id", SqlDbType.NVarChar).Value = IIf(Item.external_id = "", DBNull.Value, Item.external_id)
      _sql_command.Parameters.Add("@provider_id", SqlDbType.NVarChar).Value = IIf(Item.provider_id = "", DBNull.Value, Item.provider_id)
      _sql_command.Parameters.Add("@vendor_id", SqlDbType.NVarChar).Value = IIf(Item.vendor_id = "", DBNull.Value, Item.vendor_id)

      _sql_command.Parameters.Add("@status", SqlDbType.Int).Value = Item.status
      _sql_command.Parameters.Add("@blocked", SqlDbType.Bit).Value = Item.blocked

      _sql_command.Parameters.Add("@terminal_id", SqlDbType.BigInt).Value = Item.terminal_id

      _sql_command.Parameters.Add("@retirement_date", SqlDbType.DateTime).Value = IIf(Item.status = WSI.Common.TerminalStatus.RETIRED, _
                                                                                      Item.retirement_date, _
                                                                                      DBNull.Value)

      _sql_command.Parameters.Add("@denomination", SqlDbType.NVarChar).Value = IIf(Item.denomination Is Nothing, DBNull.Value, Item.denomination)

      If String.IsNullOrEmpty(Item.program) Then
        _sql_command.Parameters.Add("@program", SqlDbType.NVarChar).Value = DBNull.Value
      Else
        _sql_command.Parameters.Add("@program", SqlDbType.NVarChar).Value = Item.program
      End If

      If String.IsNullOrEmpty(Item.tax_registration) Then
        _sql_command.Parameters.Add("@tax_registration", SqlDbType.NVarChar).Value = DBNull.Value
      Else
        _sql_command.Parameters.Add("@tax_registration", SqlDbType.NVarChar).Value = Item.tax_registration
      End If

      If Item.theoretical_payout = -1D Then ' NOTHING
        _sql_command.Parameters.Add("@theoretical_payout", SqlDbType.Decimal).Value = DBNull.Value
      Else
        _sql_command.Parameters.Add("@theoretical_payout", SqlDbType.Decimal).Value = Item.theoretical_payout
      End If
      'SSC 15-FEB-2012
      ' Added bank_id, smoking_area, game_type, floor_id
      If Item.bank_id = -1I Then
        _sql_command.Parameters.Add("@bank_id", SqlDbType.Int).Value = DBNull.Value
      Else
        _sql_command.Parameters.Add("@bank_id", SqlDbType.Int).Value = Item.bank_id
      End If

      _sql_command.Parameters.Add("@game_type", SqlDbType.Int).Value = Item.game_type
      _sql_command.Parameters.Add("@floor_id", SqlDbType.NVarChar).Value = IIf(Item.floor_id = "", DBNull.Value, Item.floor_id)

      If Item.status = WSI.Common.TerminalStatus.RETIRED Then
        If Item.retirement_requested = Nothing Then
          _sql_command.Parameters.Add("@retirement_requested_flag", SqlDbType.Int).Value = 2
        Else
          _sql_command.Parameters.Add("@retirement_requested_flag", SqlDbType.Int).Value = 1
        End If
      Else
        _sql_command.Parameters.Add("@retirement_requested_flag", SqlDbType.Int).Value = 0
      End If

      Dim _sas_flags As SAS_FLAGS
      Dim _sas_flags_use_site_default As SAS_FLAGS

      _sas_flags = SAS_FLAGS.NONE
      _sas_flags_use_site_default = SAS_FLAGS.NONE

      If Item.extended_meter = ENUM_SAS_FLAGS.SITE_DEFINED Then
        _sas_flags_use_site_default = _sas_flags_use_site_default Or SAS_FLAGS.USE_EXTENDED_METERS
      ElseIf Item.extended_meter = ENUM_SAS_FLAGS.ACTIVATED OrElse Item.extended_meter = ENUM_SAS_FLAGS.EXTENDED_METERS_LENGHT Then
        _sas_flags = _sas_flags Or SAS_FLAGS.USE_EXTENDED_METERS
      End If

      If Item.credits_play_mode_sas = ENUM_SAS_FLAGS.SITE_DEFINED Then
        _sas_flags_use_site_default = _sas_flags_use_site_default Or SAS_FLAGS.CREDITS_PLAY_MODE_SAS_MACHINE
      ElseIf Item.credits_play_mode_sas = ENUM_SAS_FLAGS.MACHINE_DEFINED Then
        _sas_flags = _sas_flags Or SAS_FLAGS.CREDITS_PLAY_MODE_SAS_MACHINE
      End If

      If Item.use_cmd_0x1B_handpays = ENUM_SAS_FLAGS.SITE_DEFINED Then
        _sas_flags_use_site_default = _sas_flags_use_site_default Or SAS_FLAGS.USE_HANDPAY_INFORMATION
        _sas_flags_use_site_default = _sas_flags_use_site_default Or SAS_FLAGS.SAS_FLAGS_AUTOMATIC_HANDPAY_PAYMENT
      ElseIf Item.use_cmd_0x1B_handpays = ENUM_SAS_FLAGS.ACTIVATED Then
        _sas_flags = _sas_flags Or SAS_FLAGS.USE_HANDPAY_INFORMATION
      ElseIf Item.use_cmd_0x1B_handpays = ENUM_SAS_FLAGS.ACTIVATED_AND_AUTOMATICALLY Then
        _sas_flags = _sas_flags Or SAS_FLAGS.USE_HANDPAY_INFORMATION
        _sas_flags = _sas_flags Or SAS_FLAGS.SAS_FLAGS_AUTOMATIC_HANDPAY_PAYMENT
      End If

      ' RAB: Product Backlog Item 8857:Bally 3.5 BCDs: GUI: Modificaci�n de la pantalla de edici�n de terminales
      If Item.no_RTE = True Then
        _sas_flags = _sas_flags Or SAS_FLAGS.USE_FLAGS_FORCE_NO_RTE
      End If

      If Item.single_byte = True Then
        _sas_flags = _sas_flags Or SAS_FLAGS.ALLOW_SINGLE_BYTE
      End If

      If Item.ignore_no_bet_plays = True Then
        _sas_flags = _sas_flags Or SAS_FLAGS.SAS_FLAGS_IGNORE_NO_BET_PLAYS
      End If

      If (Item.extended_meter = ENUM_SAS_FLAGS.ACTIVATED OrElse Item.extended_meter = ENUM_SAS_FLAGS.NOT_ACTIVATED OrElse Item.extended_meter = ENUM_SAS_FLAGS.SITE_DEFINED) Then
        Item.bcd4 = 0
        Item.bcd5 = 0
      End If

      If Item.aft_although_lock_0 = True Then
        _sas_flags = _sas_flags Or SAS_FLAGS.SAS_FLAGS_AFT_ALTHOUGH_LOCK_ZERO
      End If

      ' ACC/JMV: Product Backlog Item 8857:Bally 3.5 BCDs: GUI: Modificaci�n de la pantalla de edici�n de terminales
      _4bcd = Item.bcd4
      _5bcd = Item.bcd5

      _4bcd <<= 4
      _5bcd <<= 8
      _sas_flags = _sas_flags Or _4bcd
      _sas_flags = _sas_flags Or _5bcd

      '   - Special Progressive Meter
      If Item.progressive_meter = ENUM_SAS_FLAGS.SITE_DEFINED Then
        _sas_flags_use_site_default = _sas_flags_use_site_default Or SAS_FLAGS.SPECIAL_PROGRESSIVE_METER
      ElseIf Item.progressive_meter = ENUM_SAS_FLAGS.ACTIVATED Then
        _sas_flags = _sas_flags Or SAS_FLAGS.SPECIAL_PROGRESSIVE_METER
      End If

      If Item.enable_disable_note_acceptor = ENUM_SAS_FLAGS.SITE_DEFINED Then
        _sas_flags_use_site_default = _sas_flags_use_site_default Or SAS_FLAGS.SAS_FLAGS_ENABLE_DISABLE_NOTE_ACCEPTOR
      ElseIf Item.enable_disable_note_acceptor = ENUM_SAS_FLAGS.ACTIVATED Then
        _sas_flags = _sas_flags Or SAS_FLAGS.SAS_FLAGS_ENABLE_DISABLE_NOTE_ACCEPTOR
      End If

      If Item.reserve_terminal = ENUM_SAS_FLAGS.SITE_DEFINED Then
        _sas_flags_use_site_default = _sas_flags_use_site_default Or SAS_FLAGS.SAS_FLAGS_RESERVE_TERMINAL
      ElseIf Item.reserve_terminal = ENUM_SAS_FLAGS.ACTIVATED Then
        _sas_flags = _sas_flags Or SAS_FLAGS.SAS_FLAGS_RESERVE_TERMINAL
      End If

      If Item.lock_egm_on_reserve = ENUM_SAS_FLAGS.SITE_DEFINED Then
        _sas_flags_use_site_default = _sas_flags_use_site_default Or SAS_FLAGS.SAS_FLAGS_LOCK_EGM_ON_RESERVE
      ElseIf Item.lock_egm_on_reserve = ENUM_SAS_FLAGS.ACTIVATED Then
        _sas_flags = _sas_flags Or SAS_FLAGS.SAS_FLAGS_LOCK_EGM_ON_RESERVE
      End If

      _sql_command.Parameters.Add("@pSasFlags", SqlDbType.Int).Value = _sas_flags
      _sql_command.Parameters.Add("@pSasFlagsUseSiteDefault", SqlDbType.Int).Value = _sas_flags_use_site_default

      'HBB 05-FEB-2013
      _sql_command.Parameters.Add("@pserial_number", SqlDbType.NVarChar).Value = IIf(String.IsNullOrEmpty(Item.serial_number), DBNull.Value, Item.serial_number)
      _sql_command.Parameters.Add("@pcabinet_type", SqlDbType.NVarChar).Value = IIf(String.IsNullOrEmpty(Item.cabinet_type), DBNull.Value, Item.cabinet_type)
      _sql_command.Parameters.Add("@pjackpot_contribution", SqlDbType.Decimal).Value = IIf(Item.jackpot_contribution_pct = -1D, DBNull.Value, Item.jackpot_contribution_pct)

      'JCOR 02-JUL-2013 
      _sql_command.Parameters.Add("@contract_type", SqlDbType.NVarChar).Value = IIf(String.IsNullOrEmpty(Item.contract_type), DBNull.Value, Item.contract_type)
      _sql_command.Parameters.Add("@contract_id", SqlDbType.NVarChar).Value = IIf(String.IsNullOrEmpty(Item.contract_id), DBNull.Value, Item.contract_id)
      _sql_command.Parameters.Add("@order_number", SqlDbType.NVarChar).Value = IIf(String.IsNullOrEmpty(Item.order_number), DBNull.Value, Item.order_number)

      _sql_command.Parameters.Add("@pMachineId", SqlDbType.NVarChar).Value = IIf(String.IsNullOrEmpty(Item.machine_id), DBNull.Value, Item.machine_id)
      _sql_command.Parameters.Add("@pPosition", SqlDbType.Int).Value = IIf(Item.position > 0, Item.position, DBNull.Value)
      _sql_command.Parameters.Add("@pTopAward", SqlDbType.Decimal).Value = IIf(Item.top_award > 0, Item.top_award, DBNull.Value)
      _sql_command.Parameters.Add("@pMaxBet", SqlDbType.Decimal).Value = IIf(Item.max_bet > 0, Item.max_bet, DBNull.Value)
      _sql_command.Parameters.Add("@pNumberLines", SqlDbType.NVarChar).Value = IIf(String.IsNullOrEmpty(Item.number_lines), DBNull.Value, Item.number_lines)
      _sql_command.Parameters.Add("@pGameTheme", SqlDbType.NVarChar).Value = IIf(String.IsNullOrEmpty(Item.game_theme), DBNull.Value, Item.game_theme)
      _sql_command.Parameters.Add("@pMachineSerialNumber", SqlDbType.VarChar).Value = IIf(String.IsNullOrEmpty(Item.machine_serial_number), DBNull.Value, Item.machine_serial_number)
      _sql_command.Parameters.Add("@pMeterDeltaId", SqlDbType.BigInt).Value = IIf(Item.meter_delta_id >= 0, Item.meter_delta_id, DBNull.Value)
      _sql_command.Parameters.Add("@pAssetNumber", SqlDbType.BigInt).Value = IIf(Item.asset_number >= 0, Item.asset_number, DBNull.Value)
      _sql_command.Parameters.Add("@pMachineAssetNumber", SqlDbType.BigInt).Value = IIf(Item.machine_asset_number >= 0, Item.machine_asset_number, DBNull.Value)

      'FAV 29-MAY-2015
      _sql_command.Parameters.Add("@pCommunicationType", SqlDbType.Int).Value = Item.communication_type
      _sql_command.Parameters.Add("@pSmibConfigurationId", SqlDbType.BigInt).Value = IIf(Item.smib_configuration_id >= 0, Item.smib_configuration_id, DBNull.Value)

      ' ColJuegos
      If WSI.Common.Misc.IsColJuegosEnabled Then
        _sql_command.Parameters.Add("@pBrandCode", SqlDbType.NVarChar).Value = IIf(String.IsNullOrEmpty(Item.brand_code), DBNull.Value, Item.brand_code)
        _sql_command.Parameters.Add("@pModel", SqlDbType.NVarChar).Value = IIf(String.IsNullOrEmpty(Item.model), DBNull.Value, Item.model)

        _sql_command.Parameters.Add("@pMETHomologated", SqlDbType.Bit).Value = Item.met_homologated
        _sql_command.Parameters.Add("@pBetCode", SqlDbType.NVarChar).Value = IIf(String.IsNullOrEmpty(Item.bet_code), DBNull.Value, Item.bet_code)
      End If

      'LTC 2018-JAN-08
      _sql_command.Parameters.Add("@pManufactureYear", SqlDbType.Int).Value = IIf(Item.manufacture_year > 0, Item.manufacture_year, DBNull.Value)

      If _is_tito_mode Then
        If Me.m_terminal.tito_configuration_type Then
          _sql_command.Parameters.Add("@pCashable", SqlDbType.Bit).Value = Me.m_terminal.tito_allowed_cashable_emission
          _sql_command.Parameters.Add("@pPromotional", SqlDbType.Bit).Value = Me.m_terminal.tito_allowed_promotional_emission
          _sql_command.Parameters.Add("@pRedemption", SqlDbType.Bit).Value = Me.m_terminal.tito_allowed_redemption
          _sql_command.Parameters.Add("@pMaxAllowedTi", SqlDbType.Money).Value = Me.m_terminal.tito_max_allowed_ti
          _sql_command.Parameters.Add("@pMaxAllowedTo", SqlDbType.Money).Value = Me.m_terminal.tito_max_allowed_to
          _sql_command.Parameters.Add("@pMinAllowedTi", SqlDbType.Money).Value = Me.m_terminal.tito_min_allowed_ti
          _sql_command.Parameters.Add("@pAllowTruncate", SqlDbType.Bit).Value = Me.m_terminal.tito_allow_truncate
          _sql_command.Parameters.Add("@pMinDenomination", SqlDbType.Money).Value = Me.m_terminal.tito_min_denomination
        Else
          _sql_command.Parameters.Add("@pCashable", SqlDbType.Bit).Value = DBNull.Value
          _sql_command.Parameters.Add("@pPromotional", SqlDbType.Bit).Value = DBNull.Value
          _sql_command.Parameters.Add("@pRedemption", SqlDbType.Bit).Value = DBNull.Value
          _sql_command.Parameters.Add("@pMaxAllowedTi", SqlDbType.Money).Value = DBNull.Value
          _sql_command.Parameters.Add("@pMaxAllowedTo", SqlDbType.Money).Value = DBNull.Value
          _sql_command.Parameters.Add("@pMinAllowedTi", SqlDbType.Money).Value = DBNull.Value
          _sql_command.Parameters.Add("@pAllowTruncate", SqlDbType.Bit).Value = DBNull.Value
          _sql_command.Parameters.Add("@pMinDenomination", SqlDbType.Money).Value = DBNull.Value

        End If

        _sql_command.Parameters.Add("@pCoinCollection", SqlDbType.Bit).Value = Me.IsCoinCollection

      End If

      'RGR 20-NOV-2015
      If _is_tito_mode Then
        _sql_command.Parameters.Add("@pEquityPercentage", SqlDbType.Money).Value = Me.m_terminal.equity_percentage
        _sql_command.Parameters.Add("@pChkEquityPercentage", SqlDbType.Bit).Value = Me.m_terminal.chk_equity_percentage
      Else
        _sql_command.Parameters.Add("@pEquityPercentage", SqlDbType.Money).Value = DBNull.Value
        _sql_command.Parameters.Add("@pChkEquityPercentage", SqlDbType.Bit).Value = DBNull.Value
      End If

      _sql_command.Parameters.Add("@pTitoHostId", SqlDbType.Int).Value = Me.m_terminal.sas_host_id

      _sql_command.Parameters.Add("@pCurrencyIsoCode", SqlDbType.NVarChar).Value = Item.currency_iso_code

      'LTC 2018-JAN-08
      If WSI.Common.Misc.IsColJuegosEnabled Then
        _sql_command.Parameters.Add("@pManufacturingMonth", SqlDbType.Int).Value = DBNull.Value
        _sql_command.Parameters.Add("@pManufacturingDay", SqlDbType.Int).Value = DBNull.Value
      Else
        _sql_command.Parameters.Add("@pManufacturingMonth", SqlDbType.Int).Value = IIf(Item.manufacturing_month > 0, Item.manufacturing_month, DBNull.Value)
        _sql_command.Parameters.Add("@pManufacturingDay", SqlDbType.Int).Value = IIf(Item.manufacturing_day > 0, Item.manufacturing_day, DBNull.Value)
      End If

      _num_rows_updated = _sql_command.ExecuteNonQuery()

      If _num_rows_updated <> 1 Then
        GUI_EndSQLTransaction(_sql_tx, False)

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      '   - 'Invalidate' the terminal's connection activity if the terminal is being retired
      If Item.status = WSI.Common.TerminalStatus.RETIRED Then
        _sql_query = New System.Text.StringBuilder()
        _sql_query.AppendLine("UPDATE TERMINALS_CONNECTED")
        _sql_query.AppendLine(" SET TC_STATUS    = @status")
        _sql_query.AppendLine(" , TC_CONNECTED = @connected")
        _sql_query.AppendLine(" WHERE TC_TERMINAL_ID = @terminal_id")
        _sql_query.AppendLine(" AND TC_DATE >= @retirement_date")

        _sql_command = New SqlCommand(_sql_query.ToString)
        _sql_command.Transaction = _sql_tx
        _sql_command.Connection = _sql_tx.Connection

        _sql_command.Parameters.Add("@status", SqlDbType.Int).Value = WSI.Common.TerminalStatus.RETIRED
        _sql_command.Parameters.Add("@connected", SqlDbType.Bit).Value = 0    ' Not Connected
        _sql_command.Parameters.Add("@retirement_date", SqlDbType.DateTime).Value = Item.retirement_date
        _sql_command.Parameters.Add("@terminal_id", SqlDbType.BigInt).Value = Item.terminal_id
        _num_rows_updated = _sql_command.ExecuteNonQuery()

        ' TJG 21-MAR-2011
        ' - Delete from TERMINALS_3GS table when retiring a 3GS terminal 
        If Item.type = TerminalTypes.T3GS Then
          _sql_query = New System.Text.StringBuilder()
          _sql_query.AppendLine("DELETE FROM TERMINALS_3GS")
          _sql_query.AppendLine(" WHERE T3GS_TERMINAL_ID = @terminal_id")

          _sql_command = New SqlCommand(_sql_query.ToString)
          _sql_command.Transaction = _sql_tx
          _sql_command.Connection = _sql_tx.Connection

          _sql_command.Parameters.Add("@terminal_id", SqlDbType.BigInt).Value = Item.terminal_id

          _num_rows_updated = _sql_command.ExecuteNonQuery()
        End If

        ' RCI 08-APR-2011
        ' - Delete from GAME_METERS table when the terminal is being retired
        _sql_query = New System.Text.StringBuilder()
        _sql_query.AppendLine("DELETE   GAME_METERS ")
        _sql_query.AppendLine(" WHERE   GM_TERMINAL_ID = @terminal_id")

        _sql_command = New SqlCommand(_sql_query.ToString)
        _sql_command.Transaction = _sql_tx
        _sql_command.Connection = _sql_tx.Connection

        _sql_command.Parameters.Add("@terminal_id", SqlDbType.BigInt).Value = Item.terminal_id

        _num_rows_updated = _sql_command.ExecuteNonQuery()

      End If

      '   - Insert event if status changed
      If _previous_status <> Item.status Then
        _sql_query = New System.Text.StringBuilder()
        _sql_query.AppendLine("INSERT INTO EVENT_HISTORY (EH_TERMINAL_ID ")
        _sql_query.AppendLine("                         , EH_SESSION_ID ")
        _sql_query.AppendLine("                         , EH_DATETIME ")
        _sql_query.AppendLine("                         , EH_EVENT_TYPE ")
        _sql_query.AppendLine("                         , EH_OPERATION_CODE ")
        _sql_query.AppendLine("                         , EH_OPERATION_DATA) ")
        _sql_query.AppendLine("       VALUES (@p1, @p2, @p3, @p4, @p5, @p6) ")

        _sql_command = New SqlCommand(_sql_query.ToString)
        _sql_command.Connection = _sql_tx.Connection
        _sql_command.Transaction = _sql_tx

        _sql_command.Parameters.Add("@p1", SqlDbType.Int, 4, "EH_TERMINAL_ID").Value = TerminalId
        _sql_command.Parameters.Add("@p2", SqlDbType.BigInt, 8, "EH_SESSION_ID").Value = 0
        _sql_command.Parameters.Add("@p3", SqlDbType.DateTime, 8, "EH_DATETIME").Value = WSI.Common.WGDB.Now
        _sql_command.Parameters.Add("@p4", SqlDbType.Int, 4, "EH_EVENT_TYPE").Value = 2 ' WCP_EV_OPERATION
        _sql_command.Parameters.Add("@p5", SqlDbType.Int, 4, "EH_OPERATION_CODE").Value = 31
        _sql_command.Parameters.Add("@p6", SqlDbType.Money).Value = ((_previous_status + 1) * 100) + Item.status

        _num_rows_updated = _sql_command.ExecuteNonQuery()

        If _num_rows_updated <> 1 Then
          GUI_EndSQLTransaction(_sql_tx, False)

          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

      End If

      'If change provider, reset terminal-game-relation
      Dim _para_wkt_ads_steep_id As SqlParameter
      Dim _row_affected As Integer

      If Not IsNothing(_previous_provider_id) And _previous_provider_id <> Item.provider_id Then
        _sql_query = New System.Text.StringBuilder()
        _sql_query.AppendLine("SELECT GM_GAME_ID")
        _sql_query.AppendLine(" FROM GAMES")
        _sql_query.AppendLine(" WHERE GM_NAME = 'UNKNOWN'")

        _data_table = GUI_GetTableUsingSQL(_sql_query.ToString, 5000)
        If Not IsNothing(_data_table) Then
          If _data_table.Rows.Count() > 0 Then
            _game_unknown_id = _data_table.Rows(0).Item(0)
          Else
            'If not exists, create new terminal
            With _sql_string_builder
              .AppendLine("INSERT INTO   GAMES  ")
              .AppendLine("            ( GM_NAME      ")
              .AppendLine("            )               ")
              .AppendLine("     VALUES ( @pAdvId      ")
              .AppendLine("	           );             ")
              .AppendLine("SET @pAdvStepId = SCOPE_IDENTITY();")
            End With

            _sql_cmd = New SqlCommand(_sql_string_builder.ToString(), _sql_tx.Connection, _sql_tx)
            _sql_cmd.Parameters.Add("@pAdvId", SqlDbType.NVarChar, 50).Value = "UNKNOWN"
            _para_wkt_ads_steep_id = _sql_cmd.Parameters.Add("@pAdvStepId", SqlDbType.BigInt)
            _para_wkt_ads_steep_id.Direction = ParameterDirection.Output

            _row_affected = _sql_cmd.ExecuteNonQuery()
            If _row_affected <> 1 Then
              GUI_EndSQLTransaction(_sql_tx, False)

              Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
            End If
            _game_unknown_id = _para_wkt_ads_steep_id.Value
          End If
        End If

        _sql_string_builder.Length = 0
        With _sql_string_builder
          .AppendLine("UPDATE  TERMINAL_GAME_TRANSLATION  ")
          .AppendLine("   SET  TGT_TRANSLATED_GAME_ID = NULL") '@pTargetGame")
          .AppendLine("       ,TGT_PAYOUT_IDX  = NULL")
          .AppendLine(" WHERE TGT_TERMINAL_ID = @pTerminalId")
        End With
        _sql_cmd = New SqlCommand(_sql_string_builder.ToString(), _sql_tx.Connection, _sql_tx)
        _sql_cmd.Parameters.Add("@pTargetGame", SqlDbType.Int).Value = _game_unknown_id
        _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId
        _row_affected = _sql_cmd.ExecuteNonQuery()
        'If _row_affected <> 1 Then
        '  GUI_EndSQLTransaction(_sql_tx, False)

        '  Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        'End If
      End If


      ' DHA 08-APR-2014
      'If _system_mode = SYSTEM_MODE.WASS Then
      TerminalStatusFlags.DB_SetValue(TerminalStatusFlags.BITMASK_TYPE.Machine_status, Item.terminal_id, Me.m_terminal.machine_status, _sql_tx)
      'End If

      'Terminal has changed. Change all base name of this master_id
      If _previous_terminal_base <> Item.base_name Then
        _sql_query = New System.Text.StringBuilder()
        _sql_query.AppendLine("UPDATE TERMINALS")
        _sql_query.AppendLine(" SET   TE_BASE_NAME = @base_name")
        _sql_query.AppendLine(" WHERE TE_MASTER_ID = @master_id")
        _sql_command = New SqlCommand(_sql_query.ToString)
        _sql_command.Transaction = _sql_tx
        _sql_command.Connection = _sql_tx.Connection

        _sql_command.Parameters.Add("@base_name", SqlDbType.NVarChar).Value = Item.base_name
        _sql_command.Parameters.Add("@master_id", SqlDbType.BigInt).Value = Item.master_id
        _sql_command.ExecuteNonQuery()
      End If

      'Provider_id has changed. Change all provider_id of this master_id
      If _previous_provider_id <> Item.provider_id Then
        _sql_query = New System.Text.StringBuilder()
        _sql_query.AppendLine("UPDATE TERMINALS")
        _sql_query.AppendLine(" SET   TE_PROVIDER_ID = @provider_id")
        _sql_query.AppendLine(" WHERE TE_MASTER_ID   = @master_id")
        _sql_command = New SqlCommand(_sql_query.ToString)
        _sql_command.Transaction = _sql_tx
        _sql_command.Connection = _sql_tx.Connection

        _sql_command.Parameters.Add("@provider_id", SqlDbType.NVarChar).Value = Item.provider_id
        _sql_command.Parameters.Add("@master_id", SqlDbType.BigInt).Value = Item.master_id
        _sql_command.ExecuteNonQuery()
      End If

      ' RCI 21-DEC-2013: Terminal name changes: Update the CT_NAME if the terminal exists
      ' ETP 27-JAN-2015: Terminal name changes: Update the CT_NAME for all base_name changes
      If _previous_terminal_name <> Item.name Then
        _sql_string_builder.Length = 0
        With _sql_string_builder
          .AppendLine("UPDATE   CASHIER_TERMINALS                                  ")
          .AppendLine("   SET   CT_NAME        = TE_NAME                           ")
          .AppendLine("  FROM   CASHIER_TERMINALS                                  ")
          .AppendLine(" INNER   JOIN TERMINALS ON TE_TERMINAL_ID = CT_TERMINAL_ID  ")
          .AppendLine(" WHERE   TE_MASTER_ID = @pMasterId                          ")
        End With
        _sql_cmd = New SqlCommand(_sql_string_builder.ToString(), _sql_tx.Connection, _sql_tx)
        _sql_cmd.Parameters.Add("@pMasterId", SqlDbType.Int).Value = Item.master_id

        ' Don't care about Query result
        _row_affected = _sql_cmd.ExecuteNonQuery()
      End If

      ' Commit
      If Not GUI_EndSQLTransaction(_sql_tx, True) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

    Catch _ex As Exception
      ' Rollback  
      GUI_EndSQLTransaction(_sql_tx, False)
      Log.Exception(_ex)
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
  End Function ' UpdateTerminal

#End Region ' Private Functions

End Class