'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   CLASS_ACCOUNT_REQUEST_PIN.vb
' DESCRIPTION:   
' AUTHOR:        Joshwa Marcalle
' CREATION DATE: 20-MAR-2014
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------------------------
' 20-MAR-2014  JRM    Initial version. Description contained in RequestPinCodeDUT.docx DUT
' 28-MAR-2014  DLL    Incorrect text audit. Set number for each check when is needed Yes or Not
' 11-APR-2014  LEM    Deleted PromoBOX funcionality
' -------------------------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports WSI.Common
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports System.Text

Public Class CLASS_ACCOUNT_REQUEST_PIN
  Inherits CLASS_BASE


#Region " Members "

  Protected m_request_pin_cashier As Int32
  Protected m_request_pin_ebox As Int32
  Protected m_disable_for_anonymous_accounts As Int32

#End Region

#Region " Properties "

  Public Property RequestPinCashier() As Int32
    Get
      Return m_request_pin_cashier
    End Get
    Set(ByVal Value As Int32)
      m_request_pin_cashier = Value
    End Set
  End Property

  Public Property RequestPinEBox() As Int32
    Get
      Return m_request_pin_ebox
    End Get
    Set(ByVal Value As Int32)
      m_request_pin_ebox = Value
    End Set
  End Property

  Public Property DisableForAnonymousAccounts() As Int32
    Get
      Return m_disable_for_anonymous_accounts
    End Get
    Set(ByVal Value As Int32)
      m_disable_for_anonymous_accounts = Value
    End Set
  End Property

#End Region

#Region " Overrides function "

  '----------------------------------------------------------------------------
  ' PURPOSE : Returns the related auditor data for the current object.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - The newly created audit object
  '
  ' NOTES :
  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA
    Dim _auditor_data As CLASS_AUDITOR_DATA

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_CASHIER_CONFIGURATION)

    Call _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(4747), "")

    Call _auditor_data.SetField(0, IIf(Me.RequestPinCashier And Accounts.PinRequestOperationType.WITHDRAWAL, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)), _
                                GLB_NLS_GUI_PLAYER_TRACKING.GetString(4749) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4752))

    Call _auditor_data.SetField(0, IIf(Me.RequestPinCashier And Accounts.PinRequestOperationType.GIFT_REQUEST, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)), _
                                GLB_NLS_GUI_PLAYER_TRACKING.GetString(4749) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4754))

    Call _auditor_data.SetField(0, IIf(Me.RequestPinEBox And Accounts.PinRequestOperationType.PLAY, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)), _
                                GLB_NLS_GUI_PLAYER_TRACKING.GetString(4751) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4753))

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(4760), IIf(Me.DisableForAnonymousAccounts, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)))

    Return _auditor_data
  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE : Reads from the database into the given object
  '
  ' PARAMS :
  '     - INPUT :
  '         - ObjectId: An object, it must be 'casted' to the desired KEY.
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '
  ' NOTES :

  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _rc As Integer

    _rc = GetConfigurationData()

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select
  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE : Inserts the given object to the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES :
  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE : Deletes the given object from the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES :
  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE : Updates the given object to the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES :
  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _rc As Integer

    _rc = Me.UpdateConfigurationData()

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
        Return ENUM_STATUS.STATUS_DUPLICATE_KEY

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select


  End Function


  ' PURPOSE: UpdateConfigurationData
  '                  
  '  PARAMS:         
  '     - INPUT:     
  '           - 
  '     - OUTPUT:    
  '                  
  ' RETURNS:         
  '     - Integer
  '                  
  Private Function UpdateConfigurationData() As Integer

    Dim _rc As Integer
    Dim _sql_transaction As SqlTransaction
    Dim _do_commit As Boolean

    _sql_transaction = Nothing

    _rc = CLASS_BASE.ENUM_STATUS.STATUS_OK
    _do_commit = True

    Try

      If Not GUI_BeginSQLTransaction(_sql_transaction) Then
        _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

        Return _rc
      End If
 
      If (Not DB_GeneralParam_Update("Account", "RequestPin.Cashier", RequestPinCashier.ToString(), _sql_transaction)) Then
        _do_commit = False
      End If

      If (_do_commit And Not DB_GeneralParam_Update("Account", "RequestPin.eBox", RequestPinEBox.ToString(), _sql_transaction)) Then
        _do_commit = False
      End If

      If (_do_commit And Not DB_GeneralParam_Update("Account", "RequestPin.DisabledForAnonymous", DisableForAnonymousAccounts.ToString(), _sql_transaction)) Then
        _do_commit = False
      End If
    
      GUI_EndSQLTransaction(_sql_transaction, _do_commit)

    Catch _ex As Exception
      _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "DB_Update", _
                            "UpdateConfiguration", _
                            _ex.Message)

    End Try

    Return _rc

  End Function 'UpdateConfigurationData

  '----------------------------------------------------------------------------
  ' PURPOSE : Returns a duplicate of the given object.
  '
  ' PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - The newly created object
  '
  ' NOTES :
  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE
    Dim _target As CLASS_ACCOUNT_REQUEST_PIN

    _target = New CLASS_ACCOUNT_REQUEST_PIN

    _target.m_request_pin_cashier = Me.RequestPinCashier
    _target.m_request_pin_ebox = Me.RequestPinEBox
    _target.m_disable_for_anonymous_accounts = Me.DisableForAnonymousAccounts

    Return _target

  End Function

#End Region

#Region " Private functions "

  '----------------------------------------------------------------------------
  ' PURPOSE : Fill TYPE_MULTISITE_CONFIG.
  '
  ' PARAMS :
  '     - INPUT : An instance of TYPE_MULTISITE_CONFIG object
  '
  '     - OUTPUT : An instance of TYPE_MULTISITE_CONFIG with data filled
  '
  ' RETURNS :
  '     - ENUM_CONFIGURATION_RC
  '
  ' NOTES :
  Private Function GetConfigurationData() As Integer

    Dim _general_param As GeneralParam.Dictionary
    Try
      _general_param = GeneralParam.Dictionary.Create()

      Me.RequestPinCashier = _general_param.GetInt32("Account", "RequestPin.Cashier", 0)
      Me.RequestPinEBox = _general_param.GetInt32("Account", "RequestPin.eBox", 0)
      Me.DisableForAnonymousAccounts = _general_param.GetInt32("Account", "RequestPin.DisabledForAnonymous", 0)

    Catch _ex As Exception
      Return ENUM_STATUS.STATUS_ERROR
    End Try

    Return ENUM_STATUS.STATUS_OK

  End Function ' GetConfigurationData
 
#End Region ' Private functions 

End Class
