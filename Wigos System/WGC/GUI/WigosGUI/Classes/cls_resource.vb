'-------------------------------------------------------------------
' Copyright � 2011 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_resource.vb
' DESCRIPTION:   Get a resource 
' AUTHOR:        Marcos Piedra Osuna
' CREATION DATE: 12-MAR-2012
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 12-MAR-2012  MPO    Initial version
' 12-FEB-2015  FJC    New GUI Menu
' -------------------------------------------------------------------

Imports System.Globalization
Imports System.Resources

Public Class SqlQuery
  Private Shared m_resource As ResourceManager = New ResourceManager("WigosGUI.Resource.SQL", _
                                                                     System.Reflection.Assembly.GetExecutingAssembly())

  ' PURPOSE: Returns a resource string from the sql resource files
  '
  '  PARAMS:
  '     - INPUT:
  '           - Name: Name Of the sql resource
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none 

  Public Shared Function CommandText(ByVal Name As String) As String
    Return m_resource.GetString(Name)
  End Function

End Class

Public Class CLASS_GUI_RESOURCES

  Private Shared m_gui_resource As ResourceManager = New ResourceManager("WigosGUI.Resources", _
                                                                         System.Reflection.Assembly.GetExecutingAssembly())
  ' PURPOSE: Returns a image resource from the resource 
  '
  '  PARAMS:
  '     - INPUT:
  '           - Name: Name Of the resource
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - Image 
  Shared Function GetImage(ByVal ImageName As String) As System.Drawing.Image
    Dim _image As Image

    _image = Nothing

    Try
      '' Try to get image on default resource 
      _image = m_gui_resource.GetObject(ImageName)

    Catch ex As Exception

      WSI.Common.Log.Exception(ex)

      Return Nothing

    End Try

    Return _image

  End Function

  ' PURPOSE: Returns a image resource from the resource converted to Icon
  '
  '  PARAMS:
  '     - INPUT:
  '           - Name: Name Of the resource
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - Icon 
  Shared Function GetImageToIcon(ByVal ImageName As String) As System.Drawing.Icon
    Dim _image As Image
    Dim _img_bitmap As Bitmap

    _img_bitmap = Nothing
    _image = Nothing

    Try
      '' Try to get image on default resource 
      _image = GetImage(ImageName)

      If Not _image Is Nothing Then
        _img_bitmap = _image                          'Assign Image to Bitmap

        Return Icon.FromHandle(_img_bitmap.GetHicon)  'Converts Bitmap to Icon and returns
      Else

        Return Nothing
      End If

    Catch ex As Exception

      WSI.Common.Log.Exception(ex)

      Return Nothing
    End Try

  End Function

End Class