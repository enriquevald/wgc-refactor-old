'-------------------------------------------------------------------
' Copyright � 2014 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : cls_gaming_tables_configuration.vb
'
' DESCRIPTION : Gaming Tables Configuration Data class
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------- ------ -----------------------------------------------
' 24-JAN-2014 JBP    Initial version   
' 05-MAR-2014 DLL    Fixed bug WIG-696: Chips are enabled when gaming is enabled
' 06-MAY-2014 DLL    Fixed Bug WIG-885 check if all gaming table session are closed
' 29-MAY-2014 SMN    Add new General Param
' 16-JUN-2014 DLL    Fixed bug WIG-1034: Enable or disable chips always when update
' 25-JUL-2014 DLL    Chips are moved to a separate table
' 14-OCT-2014 SMN    Fixed Bug WIG-1487: If gaming tables check is disabled, denomination grid is empty 
' 02-NOV-2015 ECP    Backlog Item 5638 - Task 5723 Gambling Table Parameters, eliminate functionality: chip's automatic distribuition configuration
' 29-JUN-2016 FOS    Product Backlog Item 13403: Tables: GUI modifications (Phase 1)
' 06-JUL-2016 FOS    Product Backlog Item 15070: Tables: GUI configuration (Phase 4)
' 08-SEP-2016 FOS    PBI 16373: Tables: Split PlayerTracking to GamingTables (Phase 5)
' 17-JAN-2017 FOS    Fixed Bug 22773:GUI - Gambling Tables, enable redeem chips at first time activated gambling tables
' 22-MAR-2017 JML    PBI 25975:MES10 Ticket validation - Configuration
' 14-JUN-2017 DHA    PBI 27996:WIGOS-2482 - Dealer copy ticket - Validation in gaming table
' 14-JUL-2017 DHA    Bug 28794:WIGOS-3230 [Ticket #6556] Doesn't satisfy recharge limit
' 04-SEP-2017 RAB    PBI 28942: WIGOS-3412 MES16 - Points assignment: Configuration
'--------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports WSI.Common
Imports System.Text

Public Class CLASS_GAMING_TABLES_CONFIGURATION
  Inherits CLASS_BASE

#Region " Enum "
  Public Enum CASHIER_MODE_PURCHASE
    CHIPS = 0
    TICKET = 1
  End Enum

  Public Enum ASSIGNMENT_POINTS_MODE
    AUTOMATIC = 0
    MANUAL = 1
  End Enum
#End Region ' Enum

#Region " Constants "

#End Region  ' Constants 

#Region " Structures "
  Public Class TYPE_GAMING_TABLES_CONFIGURATION
    Public auto_distribute As Boolean
    Public auto_distribute_default_type As Integer
    Public chips_max_allowed_purchases As Integer
    Public chips_max_allowed_sales As Integer
    Public chips_stock_control As Boolean
    Public mode As WSI.Common.GamingTableBusinessLogic.GT_MODE
    Public enabled_player_tracking As Boolean
    Public enabled_insert_visits As Boolean
    Public cashier_dealercopy_mode As Integer
    Public cashier_dealercopy_signature As Boolean
    Public cashier_dealercopy_barcode As Boolean
    Public cashier_dealercopy_title As String
    Public dt_allowed_by_currency As DataTable
    Public cashier_concilition_enable As Boolean
    Public cashier_conciliation_check_account As Boolean
    Public assignment_points_mode As Integer

    Sub New()
      Me.auto_distribute = False
      Me.auto_distribute_default_type = 0
      Me.chips_max_allowed_purchases = 0
      Me.chips_max_allowed_sales = 0
      Me.chips_stock_control = False
      Me.mode = WSI.Common.GamingTableBusinessLogic.GT_MODE.DISABLED
      Me.enabled_player_tracking = False
      Me.enabled_insert_visits = False
      Me.cashier_dealercopy_mode = CASHIER_MODE_PURCHASE.CHIPS
      Me.cashier_dealercopy_signature = False
      Me.cashier_dealercopy_barcode = False
      Me.cashier_dealercopy_title = String.Empty
      Me.dt_allowed_by_currency = New DataTable()
      Me.assignment_points_mode = 0
    End Sub
  End Class


  Public Class TYPE_ALLOWED_CURRENCIES
    Public max_allowed_purchases As Decimal
    Public max_allowed_sales As Decimal
    Public iso_code As String
    Public status As Integer
  End Class

#End Region ' Structures

#Region " Members "
  Protected m_gaming_tables_configuration As New TYPE_GAMING_TABLES_CONFIGURATION
  Protected m_gaming_tables_configuration_allowed As New TYPE_ALLOWED_CURRENCIES
  Protected m_dt_allowed_by_currency As DataTable

#End Region    ' Members

#Region "Properties"

  Public Property AutoDistribute() As Boolean
    Get
      Return m_gaming_tables_configuration.auto_distribute
    End Get
    Set(ByVal value As Boolean)
      m_gaming_tables_configuration.auto_distribute = value
    End Set
  End Property

  Public Property AutoDistributeDefaultType() As Integer
    Get
      Return m_gaming_tables_configuration.auto_distribute_default_type
    End Get
    Set(ByVal value As Integer)
      m_gaming_tables_configuration.auto_distribute_default_type = value
    End Set
  End Property

  Public Property ChipsMaxAllowedPurchases() As Integer
    Get
      Return m_gaming_tables_configuration.chips_max_allowed_purchases
    End Get
    Set(ByVal value As Integer)
      m_gaming_tables_configuration.chips_max_allowed_purchases = value
    End Set
  End Property

  Public Property ChipsMaxAllowedSales() As Integer
    Get
      Return m_gaming_tables_configuration.chips_max_allowed_sales
    End Get
    Set(ByVal value As Integer)
      m_gaming_tables_configuration.chips_max_allowed_sales = value
    End Set
  End Property

  Public Property ChipsStockControl() As Boolean
    Get
      Return m_gaming_tables_configuration.chips_stock_control
    End Get
    Set(ByVal value As Boolean)
      m_gaming_tables_configuration.chips_stock_control = value
    End Set
  End Property

  Public Property Mode() As WSI.Common.GamingTableBusinessLogic.GT_MODE
    Get
      Return m_gaming_tables_configuration.mode
    End Get
    Set(ByVal value As WSI.Common.GamingTableBusinessLogic.GT_MODE)
      m_gaming_tables_configuration.mode = value
    End Set
  End Property

  Public Property PlayerTracking() As Boolean
    Get
      Return m_gaming_tables_configuration.enabled_player_tracking
    End Get
    Set(ByVal value As Boolean)
      m_gaming_tables_configuration.enabled_player_tracking = value
    End Set
  End Property

  Public Property EnabledInsertVisits() As Boolean
    Get
      Return m_gaming_tables_configuration.enabled_insert_visits
    End Get
    Set(ByVal value As Boolean)
      m_gaming_tables_configuration.enabled_insert_visits = value
    End Set
  End Property

  Public Property DealercopyMode() As Integer
    Get
      Return m_gaming_tables_configuration.cashier_dealercopy_mode
    End Get
    Set(ByVal value As Integer)
      m_gaming_tables_configuration.cashier_dealercopy_mode = value
    End Set
  End Property


  Public Property DealercopySignature() As Boolean
    Get
      Return m_gaming_tables_configuration.cashier_dealercopy_signature
    End Get
    Set(ByVal value As Boolean)
      m_gaming_tables_configuration.cashier_dealercopy_signature = value
    End Set
  End Property

  Public Property DealercopyBarcode() As Boolean
    Get
      Return m_gaming_tables_configuration.cashier_dealercopy_barcode
    End Get
    Set(ByVal value As Boolean)
      m_gaming_tables_configuration.cashier_dealercopy_barcode = value
    End Set
  End Property

  Public Property DealercopyTitle() As String
    Get
      Return m_gaming_tables_configuration.cashier_dealercopy_title
    End Get
    Set(ByVal value As String)
      m_gaming_tables_configuration.cashier_dealercopy_title = value
    End Set
  End Property

  Public Property CurrenciesAllowed() As DataTable
    Get
      Return m_gaming_tables_configuration.dt_allowed_by_currency
    End Get
    Set(ByVal value As DataTable)
      m_gaming_tables_configuration.dt_allowed_by_currency = value
    End Set
  End Property

  Public Property CashierConcilitionEnable() As Boolean
    Get
      Return m_gaming_tables_configuration.cashier_concilition_enable
    End Get
    Set(ByVal value As Boolean)
      m_gaming_tables_configuration.cashier_concilition_enable = value
    End Set
  End Property

  Public Property CashierConciliationCheckAccount() As Boolean
    Get
      Return m_gaming_tables_configuration.cashier_conciliation_check_account
    End Get
    Set(ByVal value As Boolean)
      m_gaming_tables_configuration.cashier_conciliation_check_account = value
    End Set
  End Property

  Public Property AssignmentPointsMode() As Integer
    Get
      Return m_gaming_tables_configuration.assignment_points_mode
    End Get
    Set(ByVal value As Integer)
      m_gaming_tables_configuration.assignment_points_mode = value
    End Set
  End Property

#End Region   ' Properties

#Region "Overrides"

  '----------------------------------------------------------------------------
  ' PURPOSE : Returns the related auditor data for the current object.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - The newly created audit object
  '
  ' NOTES :
  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _dv_allowed_currencies As DataView
    Dim _dt_allowed_currencies As DataTable
    Dim _chips_max_allowed_purchase As Integer
    Dim _chips_max_allowed_sales As Integer
    Dim _description As String

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_CASHIER_CONFIGURATION)
    _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(2901), "")

    Call _auditor_data.SetField(0, GetModeString(Me.Mode), GLB_NLS_GUI_PLAYER_TRACKING.GetString(4551))
    Call _auditor_data.SetField(0, GetEnabledString(Me.PlayerTracking), GLB_NLS_GUI_PLAYER_TRACKING.GetString(7565))

    Call _auditor_data.SetField(0, GetEnabledString(Me.ChipsStockControl), GLB_NLS_GUI_PLAYER_TRACKING.GetString(4550))
    Call _auditor_data.SetField(0, GetEnabledString(Me.EnabledInsertVisits), GLB_NLS_GUI_PLAYER_TRACKING.GetString(4552))

    Call _auditor_data.SetField(0, Me.DealercopyMode, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4920))
    Call _auditor_data.SetField(0, GetEnabledString(Me.DealercopyBarcode), GLB_NLS_GUI_PLAYER_TRACKING.GetString(4919))
    Call _auditor_data.SetField(0, GetEnabledString(Me.DealercopySignature), GLB_NLS_GUI_PLAYER_TRACKING.GetString(658))
    Call _auditor_data.SetField(0, Me.DealercopyTitle, GLB_NLS_GUI_PLAYER_TRACKING.GetString(392))

    Call _auditor_data.SetField(0, Me.GetEnabledString(Me.CashierConcilitionEnable), GLB_NLS_GUI_PLAYER_TRACKING.GetString(7978))
    Call _auditor_data.SetField(0, Me.GetEnabledString(Me.CashierConciliationCheckAccount), GLB_NLS_GUI_PLAYER_TRACKING.GetString(8394))

    _dv_allowed_currencies = Me.CurrenciesAllowed.DefaultView
    _dv_allowed_currencies.Sort = "GTC_ISO_CODE" & " DESC"
    _dt_allowed_currencies = _dv_allowed_currencies.ToTable()

    For Each _row As DataRow In _dt_allowed_currencies.Rows
      _chips_max_allowed_purchase = 0
      _chips_max_allowed_sales = 0

      _description = _row("GTC_ISO_CODE")
      If Not String.IsNullOrEmpty(_row("GTC_MAX_ALLOWED_PURCHASE").ToString()) Then
        _chips_max_allowed_purchase = _row("GTC_MAX_ALLOWED_PURCHASE")
      End If
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5059), String.Format("{0} - {1}", _description, _chips_max_allowed_purchase))

      If Not String.IsNullOrEmpty(_row("GTC_MAX_ALLOWED_SALES").ToString()) Then
        _chips_max_allowed_sales = _row("GTC_MAX_ALLOWED_SALES")
      End If
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5059), String.Format("{0} - {1}", _description, _chips_max_allowed_sales))

    Next

    Call _auditor_data.SetField(0, GetAssignmentPointsMode(Me.AssignmentPointsMode), GLB_NLS_GUI_PLAYER_TRACKING.GetString(8607))
    If Me.AssignmentPointsMode > 0 Then
      Call _auditor_data.SetField(0, Me.AssignmentPointsMode, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8612))
    End If

    Return _auditor_data

  End Function ' AuditorData

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR

  End Function ' DB_Delete

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR

  End Function ' DB_Insert

  '----------------------------------------------------------------------------
  ' PURPOSE : Reads from the database into the given object
  '
  ' PARAMS :
  '     - INPUT :
  '         - ObjectId: An object, it must be 'casted' to the desired KEY.
  '         - SqlCtx: SqlTransaction.
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '
  ' NOTES :
  '
  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _rc As ENUM_STATUS

    _rc = ENUM_STATUS.STATUS_ERROR

    If GamingTablesConfiguration_DbRead() Then
      _rc = ENUM_STATUS.STATUS_OK
    End If

    Return _rc

  End Function ' DB_Read

  '----------------------------------------------------------------------------
  ' PURPOSE : Updates the given object to the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '
  ' NOTES :
  '
  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _rc As ENUM_STATUS

    _rc = ENUM_STATUS.STATUS_ERROR

    If GamingTablesConfiguration_DbUpdate() Then
      _rc = ENUM_STATUS.STATUS_OK
    End If

    Return _rc

  End Function ' DB_Update

  '----------------------------------------------------------------------------
  ' PURPOSE : Returns a duplicate of the given object.
  '
  ' PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - The newly created object
  '
  ' NOTES :
  '
  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim _target As CLASS_GAMING_TABLES_CONFIGURATION

    _target = New CLASS_GAMING_TABLES_CONFIGURATION

    _target.m_gaming_tables_configuration.auto_distribute = Me.AutoDistribute
    _target.m_gaming_tables_configuration.auto_distribute_default_type = Me.AutoDistributeDefaultType
    _target.m_gaming_tables_configuration.chips_max_allowed_purchases = Me.ChipsMaxAllowedPurchases
    _target.m_gaming_tables_configuration.chips_max_allowed_sales = Me.ChipsMaxAllowedSales
    _target.m_gaming_tables_configuration.chips_stock_control = Me.ChipsStockControl
    _target.m_gaming_tables_configuration.mode = Me.Mode
    _target.m_gaming_tables_configuration.enabled_player_tracking = Me.PlayerTracking
    _target.m_gaming_tables_configuration.enabled_insert_visits = Me.EnabledInsertVisits
    _target.m_gaming_tables_configuration.cashier_dealercopy_barcode = Me.DealercopyBarcode
    _target.m_gaming_tables_configuration.cashier_dealercopy_mode = Me.DealercopyMode
    _target.m_gaming_tables_configuration.cashier_dealercopy_signature = Me.DealercopySignature
    _target.m_gaming_tables_configuration.cashier_dealercopy_title = Me.DealercopyTitle
    _target.m_gaming_tables_configuration.dt_allowed_by_currency = Me.CurrenciesAllowed.Copy()
    _target.m_gaming_tables_configuration.cashier_concilition_enable = Me.CashierConcilitionEnable
    _target.m_gaming_tables_configuration.cashier_conciliation_check_account = Me.CashierConciliationCheckAccount
    _target.m_gaming_tables_configuration.assignment_points_mode = Me.AssignmentPointsMode

    Return _target

  End Function ' Duplicate

#End Region ' Overrides

#Region "Private Functions"
  '----------------------------------------------------------------------------
  ' PURPOSE: Get distribution type string
  '          
  ' PARAMS:
  '   - INPUT: Transaction
  '   - OUTPUT: None
  '
  ' RETURNS:
  '   - True
  '   - False
  '
  ' NOTES:
  Private Function GetEnabledString(ByVal IsEnabled As Boolean) As String
    Dim _field_value As String

    If IsEnabled Then
      _field_value = GLB_NLS_GUI_CONFIGURATION.GetString(264)
    Else
      _field_value = GLB_NLS_GUI_CONFIGURATION.GetString(265)
    End If

    Return _field_value

  End Function

  ''' <summary>
  ''' Returns the Mode as String
  ''' </summary>
  ''' <param name="Mode"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetModeString(ByVal Mode As WSI.Common.GamingTableBusinessLogic.GT_MODE) As String
    Dim _field_value As String

    _field_value = String.Empty

    Select Case Mode
      Case WSI.Common.GamingTableBusinessLogic.GT_MODE.DISABLED
        _field_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7562)

      Case GamingTableBusinessLogic.GT_MODE.GUI_AND_CASHIER
        _field_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7563)

      Case GamingTableBusinessLogic.GT_MODE.GUI_AND_CASHIER
        _field_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7564)

      Case Else
        _field_value = String.Empty
    End Select

    Return _field_value

  End Function


  ''' <summary>
  ''' Returns the assignment point mode according to the days introduced
  ''' </summary>
  ''' <param name="Days">Integer with the days introduced</param>
  ''' <returns></returns>

  Private Function GetAssignmentPointsMode(ByVal Days As Integer) As String
    Dim _field_value As String
    _field_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8020) ' Automatic mode

    If Days > 0 Then
      _field_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8019) ' Manual mode
    End If

    Return _field_value

  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE: Update the database General_Params. If it goes ok the method returns true 
  '                                              if it doesn't go ok return false
  ' PARAMS:
  '   - INPUT: Transaction
  '   - OUTPUT: None
  '
  ' RETURNS:
  '   - True
  '   - False
  '
  ' NOTES:
  Private Function GamingTablesConfiguration_DbUpdate() As Boolean

    Try

      Using _db_trx As New DB_TRX

        'General Params Chips Max Allowed Purchases 
        If Not DB_GeneralParam_Update("GamingTables", "Chips.MaxAllowedPurchases", Me.ChipsMaxAllowedPurchases, _db_trx.SqlTransaction) Then
          Return False
        End If

        'General Params Chips Max Allowed Sales 
        If Not DB_GeneralParam_Update("GamingTables", "Chips.MaxAllowedSales", Me.ChipsMaxAllowedSales, _db_trx.SqlTransaction) Then
          Return False
        End If

        'General Params Chips Stock Control
        If Not DB_GeneralParam_Update("GamingTables", "ChipsStockControl", IIf(Me.ChipsStockControl, 1, 0), _db_trx.SqlTransaction) Then
          Return False
        End If

        'General Params Gaming Tables Mode
        If Not DB_GeneralParam_Update("GamingTables", "Mode", Int(Me.Mode), _db_trx.SqlTransaction) Then
          Return False
        End If

        'General Params Enabled PlayerTracking
        If Not DB_GeneralParam_Update("GamingTable.PlayerTracking", "Enabled", IIf(Me.PlayerTracking, 1, 0), _db_trx.SqlTransaction) Then
          Return False
        End If

        'General Params Enabled Insert Visits
        If Not DB_GeneralParam_Update("GamingTables", "EnabledInsertVisits", IIf(Me.EnabledInsertVisits, 1, 0), _db_trx.SqlTransaction) Then
          Return False
        End If


        'General Params Cashier.Mode
        If Not DB_GeneralParam_Update("GamingTables", "Cashier.Mode", Me.DealercopyMode, _db_trx.SqlTransaction) Then
          Return False
        End If

        'General Params Cashier.Mode.DealerCopy.Barcode
        '#If DEBUG Then
        '        If Not DB_GeneralParam_Update("GamingTables", "Cashier.Mode.DealerCopy.Barcode", IIf(Me.DealercopyBarcode, 1, 0), _db_trx.SqlTransaction) Then
        '          Return False
        '        End If
        '#End If

        'General Params Cashier.Mode.DealerCopy.Signature
        If Not DB_GeneralParam_Update("GamingTables", "Cashier.Mode.DealerCopy.Signature", IIf(Me.DealercopySignature, 1, 0), _db_trx.SqlTransaction) Then
          Return False
        End If

        'General Params Cashier.Mode.DealerCopy.Title
        If Not DB_GeneralParam_Update("GamingTables", "Cashier.Mode.DealerCopy.Title", Me.DealercopyTitle, _db_trx.SqlTransaction) Then
          Return False
        End If

        'Set valueType enabled o not in function of gamingtables mode
        If Not DB_GeneralParam_Update("FeatureChips", "ValueType.Enabled", IIf(Int(Me.Mode) = 0, 0, 1), _db_trx.SqlTransaction) Then
          Return False
        End If

        'General Params GamingTable.PlayerTracking - BuyIn.Mode
        If Not DB_GeneralParam_Update("GamingTable.PlayerTracking", "BuyIn.Mode", IIf(Me.CashierConcilitionEnable, 1, 0), _db_trx.SqlTransaction) Then
          Return False
        End If

        'General Params GamingTable.PlayerTracking - TicketValidation.CheckAccount
        If Not DB_GeneralParam_Update("GamingTable.PlayerTracking", "TicketValidation.CheckAccount", IIf(Me.CashierConciliationCheckAccount, 1, 0), _db_trx.SqlTransaction) Then
          Return False
        End If


        If Not CageMeters.UpdateAndCreateCageSessionMeters(_db_trx.SqlTransaction) Then
          Return False
        End If

        'General Params GamingTables - PointsAssignment.Mode
        If Not DB_GeneralParam_Update("GamingTables", "PointsAssignment.Mode", Me.AssignmentPointsMode, _db_trx.SqlTransaction) Then
          Return False
        End If

        If Not Me.Save(Me.CurrenciesAllowed, _db_trx.SqlTransaction) Then
          Return False
        End If

        _db_trx.Commit()

      End Using


      Return True

    Catch ex As SqlException
      Debug.Print(ex.Message)
    End Try

    Return False

  End Function ' GamingTablesConfiguration_DbUpdate

  '----------------------------------------------------------------------------
  ' PURPOSE: Get all GamingTables General Params
  '                                              
  ' PARAMS:
  '   - INPUT: 
  '   - OUTPUT:
  '
  ' RETURNS:
  '   - True
  '   - False
  '
  ' NOTES:
  '
  Private Function GamingTablesConfiguration_DbRead() As Boolean

    Dim _general_params As WSI.Common.GeneralParam.Dictionary

    Try
      _general_params = WSI.Common.GeneralParam.Dictionary.Create()

      Me.Mode = _general_params.GetInt32("GamingTables", "Mode", GamingTableBusinessLogic.GT_MODE.DISABLED)
      Me.PlayerTracking = _general_params.GetBoolean("GamingTable.PlayerTracking", "Enabled", False)

      Me.ChipsMaxAllowedPurchases = _general_params.GetInt32("GamingTables", "Chips.MaxAllowedPurchases")
      Me.ChipsMaxAllowedSales = _general_params.GetInt32("GamingTables", "Chips.MaxAllowedSales")
      Me.ChipsStockControl = _general_params.GetBoolean("GamingTables", "ChipsStockControl")
      Me.EnabledInsertVisits = _general_params.GetBoolean("GamingTables", "EnabledInsertVisits")

      Me.DealercopyMode = _general_params.GetInt32("GamingTables", "Cashier.Mode", CASHIER_MODE_PURCHASE.CHIPS)
      Me.DealercopyBarcode = _general_params.GetBoolean("GamingTables", "Cashier.Mode.DealerCopy.Barcode")
      Me.DealercopySignature = _general_params.GetBoolean("GamingTables", "Cashier.Mode.DealerCopy.Signature")
      Me.DealercopyTitle = _general_params.GetString("GamingTables", "Cashier.Mode.DealerCopy.Title")
      Me.CurrenciesAllowed = ReadAllowedCurrencies()

      Me.CashierConcilitionEnable = _general_params.GetBoolean("GamingTable.PlayerTracking", "BuyIn.Mode")
      Me.CashierConciliationCheckAccount = _general_params.GetBoolean("GamingTable.PlayerTracking", "TicketValidation.CheckAccount")
      Me.AssignmentPointsMode = _general_params.GetInt32("GamingTables", "PointsAssignment.Mode")

      Return True

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return False

  End Function ' GamingTablesConfiguration_DbRead

  ''' <summary>
  ''' Function to save the changes the values from form
  ''' </summary>
  ''' <param name="dt_currencies_from_form"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function Save(ByRef dt_currencies_from_form As DataTable, ByVal Trx As SqlTransaction) As Boolean

    Dim _rc As ENUM_STATUS
    Dim _is_update As Boolean
    Dim _type_from_database As TYPE_ALLOWED_CURRENCIES
    Dim _type_from_form As TYPE_ALLOWED_CURRENCIES

    m_dt_allowed_by_currency = ReadAllowedCurrencies()

    If m_dt_allowed_by_currency.Rows.Count > 0 Then

      For Each actually_item As DataRow In dt_currencies_from_form.Rows
        _type_from_form = New TYPE_ALLOWED_CURRENCIES()
        _type_from_form.iso_code = actually_item(0)
        _type_from_form.max_allowed_purchases = actually_item(1)
        _type_from_form.max_allowed_sales = actually_item(2)
        _type_from_form.status = 0
        _is_update = False

        For Each item As DataRow In m_dt_allowed_by_currency.Rows
          _type_from_database = New TYPE_ALLOWED_CURRENCIES()
          _type_from_database.iso_code = item(0)
          _type_from_database.max_allowed_purchases = item(1)
          _type_from_database.max_allowed_sales = item(2)
          _type_from_database.status = 0

          If (_type_from_form.iso_code = _type_from_database.iso_code) Then
            _is_update = True
            Exit For
          End If
        Next

        _rc = Me.Save(_type_from_form, Trx, _is_update)
      Next
    Else
      _is_update = False
      For Each item As DataRow In Me.CurrenciesAllowed.Rows
        _type_from_database = New TYPE_ALLOWED_CURRENCIES()
        _type_from_database.iso_code = item(0)
        _type_from_database.max_allowed_purchases = item(1)
        _type_from_database.max_allowed_sales = item(2)
        _type_from_database.status = 0
        _rc = Me.Save(_type_from_database, Trx, _is_update)
      Next
    End If

    Return IIf(_rc = ENUM_STATUS.STATUS_OK, True, False)
    'Return True
  End Function

  ''' <summary>
  ''' Save changes for each type of allowed currencies
  ''' </summary>
  ''' <param name="gamingTableConfiguration"></param>
  ''' <param name="Trx"></param>
  ''' <param name="isUpdate"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function Save(ByRef gamingTableConfiguration As TYPE_ALLOWED_CURRENCIES, ByVal Trx As SqlTransaction, ByVal isUpdate As Boolean) As Boolean
    Dim _sb As StringBuilder
    _sb = New StringBuilder()

    If (isUpdate) Then
      _sb.AppendLine("     UPDATE  GAMING_TABLE_CONFIGURATION                         ")
      _sb.AppendLine("     SET      GTC_MAX_ALLOWED_PURCHASE = @pMaxAllowedPurchase   ")
      _sb.AppendLine("	          , GTC_MAX_ALLOWED_SALES    = @pMaxAllowedSales      ")
      _sb.AppendLine("	          , GTC_STATUS               = @pStatus               ")
      _sb.AppendLine("	  WHERE     GTC_ISO_CODE             = @pIsoCode              ")
    Else
      _sb.AppendLine(" INSERT INTO  GAMING_TABLE_CONFIGURATION    ")
      _sb.AppendLine("            ( GTC_MAX_ALLOWED_PURCHASE      ")
      _sb.AppendLine("            , GTC_MAX_ALLOWED_SALES         ")
      _sb.AppendLine("            , GTC_ISO_CODE                  ")
      _sb.AppendLine("            , GTC_STATUS                    ")
      _sb.AppendLine("            )                               ")
      _sb.AppendLine("     VALUES ( @pMaxAllowedPurchase          ")
      _sb.AppendLine("	          , @pMaxAllowedSales             ")
      _sb.AppendLine("	          , @pIsoCode                     ")
      _sb.AppendLine("	          , @pStatus                      ")
      _sb.AppendLine("	          );                              ")
    End If

    Try

      Using _sql_cmd = New SqlCommand(_sb.ToString(), Trx.Connection, Trx)
        '_sql_cmd.Parameters.Add("@pId", SqlDbType.Int).Value = 0
        _sql_cmd.Parameters.Add("@pMaxAllowedPurchase", SqlDbType.Decimal).Value = gamingTableConfiguration.max_allowed_purchases
        _sql_cmd.Parameters.Add("@pMaxAllowedSales", SqlDbType.Decimal).Value = gamingTableConfiguration.max_allowed_sales
        _sql_cmd.Parameters.Add("@pIsoCode", SqlDbType.NVarChar).Value = gamingTableConfiguration.iso_code
        _sql_cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = 0

        If _sql_cmd.ExecuteNonQuery() <> 1 Then
          Return ENUM_STATUS.STATUS_ERROR
        End If

      End Using


    Catch ex As Exception
      Return False
    End Try


  End Function

  ''' <summary>
  ''' Function to read all allowed currencies from database.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function ReadAllowedCurrencies() As DataTable
    Dim _sb As StringBuilder
    Dim _table As DataTable
    Dim _currencies_accepted As String
    Dim _str_query As String

    Try

      _sb = New StringBuilder()
      _currencies_accepted = String.Empty
      _str_query = String.Empty

      _currencies_accepted = "'" & GeneralParam.GetString("RegionalOptions", "CurrenciesAccepted").Replace(";", "', '") & "'"
      _str_query = " AND GTC_ISO_CODE IN (" & _currencies_accepted & ")"

      'Table for audit elements allowed / unallowed
      _sb.AppendLine(" SELECT  GTC_ISO_CODE")
      _sb.AppendLine("        ,GTC_MAX_ALLOWED_PURCHASE")
      _sb.AppendLine("        ,GTC_MAX_ALLOWED_SALES")
      _sb.AppendLine(" FROM    GAMING_TABLE_CONFIGURATION")
      _sb.AppendLine(" WHERE   GTC_ISO_CODE <> 'X01' ")
      _sb.AppendLine(_str_query)

      _table = GUI_GetTableUsingSQL(_sb.ToString(), 500)

      If IsNothing(_table) Then
        Return Nothing
      Else
        Return _table
      End If

    Catch ex As Exception
      Return Nothing
    End Try

  End Function ' ReadAllowedCurrencies

#End Region

#Region " Public Functions "

  'DLL 05-MAY-2014
  Public Function IsAllGamingTableSessionClossed() As Boolean
    Dim _sb As StringBuilder

    _sb = New StringBuilder()

    _sb.AppendLine("    SELECT   TOP 1 CS_SESSION_ID ")
    _sb.AppendLine("      FROM   CASHIER_SESSIONS ")
    _sb.AppendLine("INNER JOIN   GAMING_TABLES_SESSIONS ON CS_SESSION_ID = GTS_CASHIER_SESSION_ID ")
    _sb.AppendLine("     WHERE   CS_STATUS = @pStatusOpen")

    Try

      Using _db_trx As New DB_TRX()
        Using _sql_cmd As SqlCommand = New SqlCommand(_sb.ToString())

          _sql_cmd.Parameters.Add("@pStatusOpen", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.OPEN
          Using _sql_reader As SqlDataReader = _db_trx.ExecuteReader(_sql_cmd)

            Return Not _sql_reader.Read()

          End Using
        End Using
      End Using

    Catch ex As Exception
      Return False
    End Try
  End Function ' IsAllGamingTableSessionClossed

#End Region

End Class
