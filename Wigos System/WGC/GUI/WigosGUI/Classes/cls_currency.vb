'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : cls_currency.vb
'
' DESCRIPTION : Currency Data class
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 18-JUN-2012  JML    Initial version   
' 28-NOV-2013  JCA    Delete 'X01' from datatable (Cage.CHIPS_ISO_CODE)
' 05-MAR-2014  CCG    Fixed bug WIGOSTITO-1110: Insert in Cash Cage denominations that are missed. 
' 25-JUL-2014  DLL    Chips are moved to a separate table
' 09-OCT-2014  RCI    Fixed bug WIG-1461: CAA_ISO_CODE doesn't exist (the correct is CGC_ISO_CODE).
'--------------------------------------------------------------------
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports System.Text

Public Class CLASS_CURRENCY
  Inherits CLASS_BASE

#Region " Constants "

  Public Const SQL_COLUMN_CUR_CURRENCY_ISO_CODE As Integer = 0
  Public Const SQL_COLUMN_CUR_CURRENCY_ALLOWED As Integer = 1
  Public Const SQL_COLUMN_CUR_CURRENCY_NAME As Integer = 2
  Public Const SQL_COLUMN_CUR_CURRENCY_ALIAS_1 As Integer = 3
  Public Const SQL_COLUMN_CUR_CURRENCY_ALIAS_2 As Integer = 4

  Public Const SQL_COLUMN_BILL_CURRENCY_ISO_CODE As Integer = 0
  Public Const SQL_COLUMN_BILL_CURRENCY_TYPE As Integer = 1
  Public Const SQL_COLUMN_BILL_CURRENCY_DENOMINATION As Integer = 2
  Public Const SQL_COLUMN_BILL_CURRENCY_REJECTED As Integer = 3
  Public Const SQL_COLUMN_BILL_CURRENCY_EDITABLE As Integer = 4      '0-No editable, 1-Editable, 8-Deleted

#End Region  ' Constants 

#Region " Structures "

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_DATA_CURRENCY
    Public data_table_cur As DataTable

    ' Init structure
    Public Sub New()
      data_table_cur = New DataTable()
    End Sub
  End Class

#End Region ' Structures

#Region " Members "

  Protected m_data_currency As New TYPE_DATA_CURRENCY
  Protected m_data_currency_denomination As New TYPE_DATA_CURRENCY

#End Region    ' Members

#Region "Properties"

  Public Overloads Property DataCurrency() As DataTable
    Get
      Return m_data_currency.data_table_cur
    End Get

    Set(ByVal Value As DataTable)
      m_data_currency.data_table_cur = Value
    End Set

  End Property             ' DataCurrency

  Public Overloads Property DataCurrencyDenomination() As DataTable
    Get
      Return m_data_currency_denomination.data_table_cur
    End Get

    Set(ByVal Value As DataTable)
      m_data_currency_denomination.data_table_cur = Value
    End Set

  End Property ' DataCurrencyDenomination

#End Region   ' Properties

#Region "Overrides"

  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim auditor_data As CLASS_AUDITOR_DATA

    auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_ACCEPTORS)

    auditor_data.SetName(GLB_NLS_GUI_CONFIGURATION.Id(439), "")
    
    ' Currencies
    For Each _currency_enable As DataRow In Me.DataCurrency.Rows
      Call auditor_data.SetField(0, GetRepresentativeValue(_currency_enable(SQL_COLUMN_CUR_CURRENCY_ALLOWED)) & ", " & _
                                    _currency_enable(SQL_COLUMN_CUR_CURRENCY_NAME) & ", " & _
                                    _currency_enable(SQL_COLUMN_CUR_CURRENCY_ALIAS_1) & ", " & _
                                    _currency_enable(SQL_COLUMN_CUR_CURRENCY_ALIAS_2) & ", ", _
                                    _currency_enable(SQL_COLUMN_BILL_CURRENCY_ISO_CODE))
    Next


    ' currency denominations
    For Each _currency_reject As DataRow In Me.DataCurrencyDenomination.Rows
      Select Case _currency_reject.RowState
        Case DataRowState.Deleted
          Call auditor_data.SetField(0, "", _
                                        _currency_reject(SQL_COLUMN_BILL_CURRENCY_ISO_CODE) & " - " & _
                                        GUI_FormatCurrency(_currency_reject(SQL_COLUMN_BILL_CURRENCY_DENOMINATION), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & " " & _
                                        GLB_NLS_GUI_COMMONMISC.GetString(9) & ". " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1061))
        Case DataRowState.Added
          Call auditor_data.SetField(0, GetRepresentativeValue(_currency_reject(SQL_COLUMN_BILL_CURRENCY_REJECTED)), _
                                        _currency_reject(SQL_COLUMN_BILL_CURRENCY_ISO_CODE) & " - " & _
                                        GUI_FormatCurrency(_currency_reject(SQL_COLUMN_BILL_CURRENCY_DENOMINATION), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & " " & _
                                        GLB_NLS_GUI_COMMONMISC.GetString(10) & ". " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1061))
        Case DataRowState.Modified
          ' Is not possible use row.delete for save data to auditor, then we use a mark for delete it, That mark is:
          '                      _currency_reject(SQL_COLUMN_BILL_CURRENCY_EDITABLE) = DataRowState.Deleted
          If _currency_reject(SQL_COLUMN_BILL_CURRENCY_EDITABLE) = DataRowState.Deleted Then
            Call auditor_data.SetField(0, "", _
                                          _currency_reject(SQL_COLUMN_BILL_CURRENCY_ISO_CODE) & " - " & _
                                          GUI_FormatCurrency(_currency_reject(SQL_COLUMN_BILL_CURRENCY_DENOMINATION), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & " " & _
                                          GLB_NLS_GUI_COMMONMISC.GetString(9) & ". " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1061))
          Else
            Call auditor_data.SetField(0, GetRepresentativeValue(_currency_reject(SQL_COLUMN_BILL_CURRENCY_REJECTED)), _
                                          _currency_reject(SQL_COLUMN_BILL_CURRENCY_ISO_CODE) & " - " & _
                                          GUI_FormatCurrency(_currency_reject(SQL_COLUMN_BILL_CURRENCY_DENOMINATION), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & ".     " & _
                                          GLB_NLS_GUI_PLAYER_TRACKING.GetString(1061))
          End If

        Case Else
          Call auditor_data.SetField(0, GetRepresentativeValue(_currency_reject(SQL_COLUMN_BILL_CURRENCY_REJECTED)), _
                                        _currency_reject(SQL_COLUMN_BILL_CURRENCY_ISO_CODE) & " - " & _
                                        GUI_FormatCurrency(_currency_reject(SQL_COLUMN_BILL_CURRENCY_DENOMINATION), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & ".     " & _
                                        GLB_NLS_GUI_PLAYER_TRACKING.GetString(1061))
      End Select
    Next

    Return auditor_data

  End Function   ' AuditorData

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR

  End Function     ' DB_Delete

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR

  End Function     ' DB_Insert

  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim rc As Integer

    ' Read currency data
    rc = CurrencyData_DbRead(SqlCtx)

    Select Case rc
      Case ENUM_DB_RC.DB_RC_OK
        Return ENUM_STATUS.STATUS_OK

      Case ENUM_DB_RC.DB_RC_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function       ' DB_Read

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim rc As Integer

    ' Update currency data
    rc = CurrencyData_DbUpdate(SqlCtx)

    Select Case rc
      Case ENUM_DB_RC.DB_RC_OK
        Return ENUM_STATUS.STATUS_OK

      Case ENUM_DB_RC.DB_RC_ERROR_NOT_FOUND
        Return CLASS_BASE.ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function     ' DB_Update

  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE
    Dim temp_currencies_data As CLASS_CURRENCY

    temp_currencies_data = New CLASS_CURRENCY

    temp_currencies_data.m_data_currency.data_table_cur = Me.m_data_currency.data_table_cur.Copy()
    temp_currencies_data.m_data_currency_denomination.data_table_cur = Me.m_data_currency_denomination.data_table_cur.Copy()

    Return temp_currencies_data
  End Function     ' Duplicate

#End Region ' Overrides

#Region "Private Functions"

  Private Function CurrencyData_DbRead(ByVal Context As Integer) As Integer

    Dim _sb As StringBuilder
    Dim _data_table_currency As DataTable

    _sb = New StringBuilder()

    'DLL11111
    ' DLL 07-JUL-2014 Only Currencies
    _sb.AppendLine("  SELECT   CUR_ISO_CODE ")
    _sb.AppendLine("         , CUR_ALLOWED  ")
    _sb.AppendLine("         , ISNULL(CUR_NAME, '')   AS CUR_NAME  ")
    _sb.AppendLine("         , ISNULL(CUR_ALIAS1, '') AS CUR_ALIAS1  ")
    _sb.AppendLine("         , ISNULL(CUR_ALIAS2, '') AS CUR_ALIAS2  ")
    _sb.AppendLine("    FROM   CURRENCIES  ")
    _sb.AppendLine("   INNER   JOIN CURRENCY_EXCHANGE ON CUR_ISO_CODE = CE_CURRENCY_ISO_CODE ")
    _sb.AppendLine("   WHERE   CE_TYPE = '" & WSI.Common.CurrencyExchangeType.CURRENCY & "' ")
    _sb.AppendLine("ORDER BY   CUR_ISO_CODE  ")

    _data_table_currency = GUI_GetTableUsingSQL(_sb.ToString(), 5000)

    m_data_currency.data_table_cur = _data_table_currency

    _sb = New StringBuilder()
    _sb.AppendLine("  SELECT   CUD_ISO_CODE ")
    _sb.AppendLine("         , CUD_TYPE ")
    _sb.AppendLine("         , CUD_DENOMINATION ")
    _sb.AppendLine("         , CUD_REJECTED ")
    _sb.AppendLine("         , 0 AS EDITABLE_ROW ")
    _sb.AppendLine("    FROM   CURRENCY_DENOMINATIONS ")
    _sb.AppendLine("ORDER BY   CUD_ISO_CODE ")
    _sb.AppendLine("         , CUD_TYPE ")
    _sb.AppendLine("         , CUD_DENOMINATION ")

    _data_table_currency = GUI_GetTableUsingSQL(_sb.ToString(), 5000)

    m_data_currency_denomination.data_table_cur = _data_table_currency

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

  End Function    ' CurrencyDenominationData_DbRead

  Private Function CurrencyData_DbUpdate(ByVal Context As Integer) As Integer

    Dim SqlTrans As System.Data.SqlClient.SqlTransaction = Nothing
    Dim commit_trx As Boolean
    Dim result As Integer

    Dim _sql_txt_insert As System.Text.StringBuilder
    Dim _num_to_insert As Integer
    Dim _sql_cmd_ins As SqlCommand

    Dim _sql_txt_delete As System.Text.StringBuilder
    Dim _num_to_delete As Integer
    Dim _sql_cmd_del As SqlCommand

    Dim _sql_txt_update As System.Text.StringBuilder
    Dim _num_to_update As Integer
    Dim _sql_cmd_upd As SqlCommand

    Dim _num_updated As Integer
    Dim _cur_denomi_to_del() As DataRow


    Try

      If Not GUI_BeginSQLTransaction(SqlTrans) Then
        Exit Function
      End If

      commit_trx = False

      ' --- Currencies
      _num_to_update = m_data_currency.data_table_cur.Select("", "", DataViewRowState.ModifiedCurrent).Length

      If _num_to_update > 0 Then
        _sql_txt_update = New System.Text.StringBuilder()
        _sql_txt_update.AppendLine("UPDATE   CURRENCIES")
        _sql_txt_update.AppendLine("   SET   CUR_ALLOWED   = @pCurAllowed")
        _sql_txt_update.AppendLine("       , CUR_NAME      = @pCurName")
        _sql_txt_update.AppendLine("       , CUR_ALIAS1    = @pCurAlias1")
        _sql_txt_update.AppendLine("       , CUR_ALIAS2    = @pCurAlias2")
        _sql_txt_update.AppendLine(" WHERE   CUR_ISO_CODE  = @pCurIsoCode")

        Using _da As New SqlDataAdapter()
          Using _sql_cmd As New SqlCommand(_sql_txt_update.ToString(), SqlTrans.Connection, SqlTrans)
            _sql_cmd.Parameters.Add("@pCurAllowed", SqlDbType.Bit).SourceColumn = "CUR_ALLOWED"
            _sql_cmd.Parameters.Add("@pCurName", SqlDbType.NVarChar).SourceColumn = "CUR_NAME"
            _sql_cmd.Parameters.Add("@pCurAlias1", SqlDbType.NVarChar).SourceColumn = "CUR_ALIAS1"
            _sql_cmd.Parameters.Add("@pCurAlias2", SqlDbType.NVarChar).SourceColumn = "CUR_ALIAS2"
            _sql_cmd.Parameters.Add("@pCurIsoCode", SqlDbType.NVarChar).SourceColumn = "CUR_ISO_CODE"

            _da.UpdateCommand = _sql_cmd
            _num_updated = _da.Update(m_data_currency.data_table_cur)
          End Using
        End Using

        If _num_to_update <> _num_updated Then
          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If
      End If

      ' Now is possible delete the row with the mark for more simple update of DB.
      _cur_denomi_to_del = m_data_currency_denomination.data_table_cur.Select("EDITABLE_ROW = 8", "")
      For _idx_row_to_del As Integer = 0 To _cur_denomi_to_del.Length - 1
        _cur_denomi_to_del(_idx_row_to_del).Delete()
      Next

      ' --- Currency denomination
      _num_to_update = m_data_currency_denomination.data_table_cur.Select("", "", DataViewRowState.ModifiedCurrent).Length
      _num_to_insert = m_data_currency_denomination.data_table_cur.Select("", "", DataViewRowState.Added).Length
      _num_to_delete = m_data_currency_denomination.data_table_cur.Select("", "", DataViewRowState.Deleted).Length

      If _num_to_update > 0 Or _num_to_insert > 0 Or _num_to_delete > 0 Then
        _sql_txt_update = New System.Text.StringBuilder()
        _sql_txt_update.AppendLine("UPDATE   CURRENCY_DENOMINATIONS")
        _sql_txt_update.AppendLine("   SET   CUD_REJECTED     = @pCudRejected")
        _sql_txt_update.AppendLine(" WHERE   CUD_ISO_CODE     = @pCudIsoCode")
        _sql_txt_update.AppendLine("   AND   CUD_TYPE         = @pCudType")
        _sql_txt_update.AppendLine("   AND   CUD_DENOMINATION = @pCudDenomination")

        _sql_txt_insert = New System.Text.StringBuilder()
        _sql_txt_insert.AppendLine("INSERT INTO   CURRENCY_DENOMINATIONS")
        _sql_txt_insert.AppendLine("          (   CUD_ISO_CODE ")
        _sql_txt_insert.AppendLine("          ,   CUD_TYPE ")
        _sql_txt_insert.AppendLine("          ,   CUD_DENOMINATION ")
        _sql_txt_insert.AppendLine("          ,   CUD_REJECTED ) ")
        _sql_txt_insert.AppendLine("     SELECT   @pCudIsoCode")
        _sql_txt_insert.AppendLine("          ,   @pCudType")
        _sql_txt_insert.AppendLine("          ,   @pCudDenomination")
        _sql_txt_insert.AppendLine("          ,   @pCudRejected ")

        _sql_txt_delete = New System.Text.StringBuilder()
        _sql_txt_delete.AppendLine("DELETE FROM  CURRENCY_DENOMINATIONS")
        _sql_txt_delete.AppendLine("      WHERE  CUD_ISO_CODE          = @pCudIsoCode")
        _sql_txt_delete.AppendLine("        AND  CUD_TYPE              = @pCudType")
        _sql_txt_delete.AppendLine("        AND  CUD_DENOMINATION      = @pCudDenomination")

        Using _da As New SqlDataAdapter()


          _sql_cmd_upd = New SqlCommand(_sql_txt_update.ToString(), SqlTrans.Connection, SqlTrans)
          _sql_cmd_upd.Parameters.Add("@pCudRejected", SqlDbType.Bit).SourceColumn = "CUD_REJECTED"
          _sql_cmd_upd.Parameters.Add("@pCudIsoCode", SqlDbType.NVarChar).SourceColumn = "CUD_ISO_CODE"
          _sql_cmd_upd.Parameters.Add("@pCudType", SqlDbType.Int).SourceColumn = "CUD_TYPE"
          _sql_cmd_upd.Parameters.Add("@pCudDenomination", SqlDbType.Money).SourceColumn = "CUD_DENOMINATION"
          _da.UpdateCommand = _sql_cmd_upd

          _sql_cmd_ins = New SqlCommand(_sql_txt_insert.ToString(), SqlTrans.Connection, SqlTrans)
          _sql_cmd_ins.Parameters.Add("@pCudIsoCode", SqlDbType.NVarChar).SourceColumn = "CUD_ISO_CODE"
          _sql_cmd_ins.Parameters.Add("@pCudType", SqlDbType.Int).SourceColumn = "CUD_TYPE"
          _sql_cmd_ins.Parameters.Add("@pCudDenomination", SqlDbType.Money).SourceColumn = "CUD_DENOMINATION"
          _sql_cmd_ins.Parameters.Add("@pCudRejected", SqlDbType.Bit).SourceColumn = "CUD_REJECTED"
          _da.InsertCommand = _sql_cmd_ins

          _sql_cmd_del = New SqlCommand(_sql_txt_delete.ToString(), SqlTrans.Connection, SqlTrans)
          _sql_cmd_del.Parameters.Add("@pCudIsoCode", SqlDbType.NVarChar).SourceColumn = "CUD_ISO_CODE"
          _sql_cmd_del.Parameters.Add("@pCudType", SqlDbType.Int).SourceColumn = "CUD_TYPE"
          _sql_cmd_del.Parameters.Add("@pCudDenomination", SqlDbType.Money).SourceColumn = "CUD_DENOMINATION"
          _da.DeleteCommand = _sql_cmd_del

          _num_updated = _da.Update(m_data_currency_denomination.data_table_cur)
        End Using

        If _num_to_update + _num_to_insert + _num_to_delete <> _num_updated Then
          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If
      End If

      ' Update Cage currency if any denomination declared in bill validator doesn't exist in Cage.
      If Me.UpdateCageCurrencies(SqlTrans) = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB Then

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      ' --- Ok
      commit_trx = True

    Catch ex As Exception
      ' Do nothing
      Debug.WriteLine(ex.Message)

    Finally
      ' Either commit or rollback
      If Not (SqlTrans.Connection Is Nothing) Then
        If commit_trx Then
          SqlTrans.Commit()
          result = ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Else
          SqlTrans.Rollback()
          result = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If
      End If

      SqlTrans.Dispose()
      SqlTrans = Nothing

    End Try

    Return result

  End Function  ' CurrencyData_DbUpdate

  Private Function GetRepresentativeValue(ByVal Value As String) As String

    If Value Then
      Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(698) ' "Si"
    Else
      Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(699) ' "No"
    End If

  End Function ' GetRepresentativeValue

  ' PURPOSE: Returns the Currencies stored in Cash Cage
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: Table
  '
  ' RETURNS:
  '     - True if the operation goes well
  '
  ' NOTES:
  Private Function GetCageCurrencies(ByRef Table As DataTable) As Boolean
    Dim _sb As System.Text.StringBuilder
    Dim _result As Boolean

    _result = False

    _sb = New System.Text.StringBuilder

    _sb.AppendLine("SELECT   CGC_ISO_CODE ")
    _sb.AppendLine("       , CGC_DENOMINATION ")
    _sb.AppendLine("  FROM   CAGE_CURRENCIES ")

    Try

      Table = GUI_GetTableUsingSQL(_sb.ToString, Integer.MaxValue)
      _result = True

    Catch ex As Exception
      _result = False
    End Try

    Return _result
  End Function ' GetCageCurrencies

  ' PURPOSE: Insert missed denominations in Cash Cage
  '
  ' PARAMS:
  '   - INPUT: 
  '           IsoCode
  '           Denomination
  '           Allowed
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - True if the operation goes well
  '
  ' NOTES:
  Private Function InsertCurrencyIntoCage(ByVal IsoCode As String _
                                        , ByVal Denomination As Integer _
                                        , ByVal Allowed As Boolean _
                                        , ByRef DbTrx As SqlTransaction) As Boolean

    Dim _sb As System.Text.StringBuilder
    Dim _result As Boolean

    _sb = New System.Text.StringBuilder
    _result = True

    _sb.AppendLine("INSERT INTO   CAGE_CURRENCIES ")
    _sb.AppendLine("            ( CGC_ISO_CODE ")
    _sb.AppendLine("            , CGC_DENOMINATION ")
    _sb.AppendLine("            , CGC_ALLOWED ")
    _sb.AppendLine("            ) ")
    _sb.AppendLine("     VALUES ( @pIsoCode ")
    _sb.AppendLine("            , @pDenomination ")
    _sb.AppendLine("            , @pAllowed ")
    _sb.AppendLine("            )")

    Try

      Using _cmd As New SqlClient.SqlCommand(_sb.ToString(), DbTrx.Connection, DbTrx)

        _cmd.Parameters.Add("@pIsoCode", SqlDbType.NVarChar).Value = IsoCode
        _cmd.Parameters.Add("@pDenomination", SqlDbType.Int).Value = Denomination
        _cmd.Parameters.Add("@pAllowed", SqlDbType.Bit).Value = Allowed

        If _cmd.ExecuteNonQuery() <> 1 Then
          _result = False
        End If

      End Using

    Catch ex As Exception
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "cls_currency", _
                            "InsertCurrencyInCageAmounts", _
                            ex.Message)

      _result = False
    End Try

    Return _result
  End Function ' InsertCurrencyIntoCage

  ' PURPOSE: Update the Cash Cage Currencies
  '
  ' PARAMS:
  '   - INPUT: DbTrx
  '   - OUTPUT: Table
  '
  ' RETURNS:
  '     - True if the operation goes well
  '
  ' NOTES:
  Private Function UpdateCageCurrencies(ByRef DbTrx As SqlTransaction) As Integer
    Dim _cage_currencies As DataTable
    Dim _result As Integer
    Dim _row As DataRow
    Dim _cage_rows As DataRow()
    Dim _allowed As Boolean
    Dim _iso_code As String
    Dim _denomination As Integer

    _cage_currencies = New DataTable
    _result = ENUM_CONFIGURATION_RC.CONFIGURATION_OK

    If Me.GetCageCurrencies(_cage_currencies) Then

      For Each _row In Me.m_data_currency_denomination.data_table_cur.Select("CUD_REJECTED = 0")
        _iso_code = _row.Item(SQL_COLUMN_BILL_CURRENCY_ISO_CODE)
        _denomination = _row.Item(SQL_COLUMN_BILL_CURRENCY_DENOMINATION)
        _allowed = WSI.Common.GeneralParam.GetString("RegionalOptions", "CurrenciesAccepted").Contains(_iso_code)

        _cage_rows = _cage_currencies.Select("CGC_ISO_CODE = '" & _iso_code & "' AND CGC_DENOMINATION = '" & _denomination & "'")

        If _cage_rows.Length = 0 Or _cage_rows Is Nothing Then

          If Not InsertCurrencyIntoCage(_iso_code, _denomination, _allowed, DbTrx) Then
            _result = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
          End If

        End If

      Next

    End If

    Return _result
  End Function ' UpdateCageCurrencies

#End Region

End Class
