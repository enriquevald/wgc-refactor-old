'-------------------------------------------------------------------
' Copyright � 2009 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : cls_cashier_configuration.vb
'
' DESCRIPTION : Cashier Configuration class
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 31-MAR-2009  DRG    Initial version
' 17-SEP-2010  TJG    Player Tracking parameters
' 23-DEC-2011  MPO    Add properties TaxesBusinessAPrizeCouponText, TaxesBusinessAShowCashInText
' 04-JAN-2012  JMM    Optionally, Hide Totals in A and B tickets. Remove field for the ticket Cash in.
' 20-JAN-2012  RCI & MPO   In PlayerTracking tab, added a table of points multiplier per provider.
' 20-MAR-2012  JAB    Add "Promo and CouponTogether" field
' 02-MAY-2012  MPO    Evidence configuration
' 03-JUL-2012  JCM    Cupon Prize text enabled/disabled dependends of Split B company value
' 28-FEB-2013  ANG    Add IsMultiSiteMemeber to supports MultiSite.
' 13-MAY-2013  AMF    GiftExpirationDays the same behavior as PointsExpirationDays
' 31-MAY-2013  XCD    Don't show hide providers
' 14-JUN-2013  JML    Fixed Bug #856: Exception Invalid object in MultiSite GUI logs
' 27-JUN-2013  PGJ    Added account customization voucher footer
' 28-OCT-2013  QMP    Always show "Account Customization Footer" tab
' 25-FEB-2013  JFC    Fixed bug WIG-420: MaxAllowedAccountBalance no longer used.
' 06-JUN-2014  RCI    Fixed Bug WIG-1022: Witholding.OnPrizeGreaterThan saved incorrectly when regional settings are Spanish from Spain.
' 30-JUN-2014  AMF    Personal Card Replacement
' 24-FEB-2015  YNM    Fixed Bug WIG-2052: Incorrect decimal format on AuditorData. 
' 01-JUL-2015  FOS    Created new parameters to amount input company b
' 07-AUG-2015  DCS    Refactor and use common properties
' 30-SEP-2015  FOS    Generated documents external retention
' 25-APR-2016  ETP    Fixed bug 10973: General Params has to be readed from BBDD.
' 20-JUN-2016  LTC    Product Backlog Item 13612,13613,13614: Witholding Evidence.
' 14-OCT-2016  RAB    PBI 18098: General params: Automatically add the GP to the system
' 17-OCT-2016  FAV    PBI 17896: CFDI configuration tab.
' 25-OCT-2016  JBP    PBI 19598:Constancias - Mejoras REVIEW Sprint 31
' 22-NOV-2016  JBP    PBI 20566:Cage Str Team 1 - Cambios Review Sprint 32
' 08-MAR-2017  JMM    PBI 25267:Third TAX - Refund TAX Configuration
' 06-MAR-2018  EOR    Bug 31760: WIGOS-1007 WigosGUI - When the user try to close the "General operations" screen without any modification the system show a warning 
' 10-MAY-2018  AGS    Bug 32652:WIGOS-11712 Unhandled exception when opening Loyalty program form on WigosMultiSiteGUI.
'--------------------------------------------------------------------
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports WSI.Common
Imports WSI.Common.ExtentionMethods

Public Class CLASS_CASHIER_CONFIGURATION
  Inherits CLASS_BASE

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_CASHIER_CONFIGURATION
    Public cashier_type As CashlessMode
  End Class

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_WCP_CASHIER_CONFIGURATION
    Public card_price As String
    Public personal_card_price As String
    Public personal_card_replacement_price As String
    Public personal_card_replacement_price_in_points As String
    Public personal_card_replacement_charge_after_times As String
    Public card_refundable As String
    Public cashier_mode As String
    Public language As String
    Public min_time_close_session As String
    Public voucher_header As String
    Public voucher_footer As String
    Public topx As String
    Public topy As String
    Public bottomx As String
    Public bottomy As String
    Public font_size As String
    Public printer_name As String
    Public font_bold As String
    Public allow_anon As String
    Public handpay_time_period As String
    Public handpay_time_to_cancel As String

    Public voucher_footer_account_customize As String

    Public tax_1_name As String
    Public tax_2_name As String
    Public tax_3_name As String
    Public tax_1_pct As String
    Public tax_2_pct As String
    Public tax_3_pct As String
    Public tax_1_apply_if As String
    Public tax_2_apply_if As String
    Public tax_3_apply_if As String
    Public tax_promo_1_pct As String
    Public tax_promo_2_pct As String
    Public tax_promo_3_pct As String
    Public tax_promo_1_apply_if As String
    Public tax_promo_2_apply_if As String
    Public tax_promo_3_apply_if As String
    Public enable_promo_taxes As String

  End Class

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_ALESIS_CASHIER_CONFIGURATION
    Public database_ip As String
    Public database_name As String
    Public vendor_id As String
    Public username As String
    Public password As String
    Public configured_terminals As New List(Of TYPE_TERMINAL)
  End Class

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_PLAYER_TRACKING_LEVEL
    Public name As String
    Public factor_redeemable_played_to_1_point As String
    Public factor_total_played_to_1_point As String
    Public factor_redeemable_spent_to_1_point As String
    Public points_to_enter As String
    Public days_on_level As String
    Public points_to_keep As String
    Public max_days_no_activity As String
    Public upgrade_downgrade_action As String

    ' Init structure
    Public Sub New()
      name = ""
      factor_redeemable_played_to_1_point = ""
      factor_total_played_to_1_point = ""
      factor_redeemable_spent_to_1_point = ""
      points_to_enter = ""
      days_on_level = ""
      points_to_keep = ""
      max_days_no_activity = ""
      upgrade_downgrade_action = ""
    End Sub
  End Class

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_TAXES_CONFIGURATION
    Public business_name As String
    Public concept As String
    Public cashin_pct As String
    Public tax_name As String
    Public tax_pct As String
    Public voucher_header As String
    Public voucher_footer As String
    Public voucher_footer_cashin As String
    Public voucher_footer_devolution As String
    Public voucher_footer_cancel As String
    Public enabled As String
    Public hide_promo As String
    Public text_promotion As String
    Public prize_coupon As String
    Public prize_coupon_text As String
    Public hide_cashin As String
    ' JAB 21-MAR-2012 Add "Promo and CouponTogether" field
    Public promo_and_coupon_together As String
    Public text_promo_and_coupon_together As String
    Public pay_title As String
    Public dev_title As String
  End Class

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_EVIDENCE_CONFIGURATION
    Public company_id1 As String
    Public company_id2 As String
    Public company_name As String

    Public representative_id1 As String
    Public representative_id2 As String
    Public representative_name As String
    Public representative_sign As Image

    Public enabled As Boolean
    Public editable_minutes As Integer
    Public prize_greater_than As Decimal
    Public generated_witholding_document As Integer 'LTC 22-JUN-2016

    ' Init structure
    Public Sub New()
      company_id1 = ""
      company_id2 = ""
      company_name = ""
      representative_id1 = ""
      representative_id2 = ""
      representative_name = ""
      representative_sign = Nothing
      enabled = False
      editable_minutes = 0
      prize_greater_than = 0
      generated_witholding_document = 0 'LTC 22-JUN-2016
    End Sub

  End Class

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_CFDI_CASHIER_CONFIGURATION
    Public active As String
    Public url As String
    Public user As String
    Public password As String
    Public scheduling_load As Integer
    Public scheduling_process As Integer
    Public retries As Integer
    Public site As String
    Public RFCAnonymous As String
    Public RFCForeign As String
    Public EFAnonymous As String
    Public EFForeign As String
  End Class

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_CFDI_REGISTERS_STATUS
    Public TotalPending As Integer
    Public TotalSend As Integer
    Public TotalRetries As Integer
    Public TotalError As Integer

    Public Sub New()
      TotalPending = 0
      TotalSend = 0
      TotalRetries = 0
      TotalError = 0
    End Sub
  End Class

  Public Const MAX_LEVELS As Integer = 4

  Public Const SQL_COLUMN_PROVIDER_ID As Integer = 0
  Public Const SQL_COLUMN_PROVIDER_NAME As Integer = 1
  Public Const SQL_COLUMN_POINTS_MULTIPLIER As Integer = 2

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_PLAYER_TRACKING_CONFIGURATION
    Public is_multisite_member As Boolean
    Public points_expiration As String
    Public points_expiration_day_month As String
    Public gift_expiration As String
    Public levels_days_counting_points As String
    Public num_levels As Integer
    ' Arrays in VB are defined with the last item. Ex: level(3) has lenght 4, and items: 0, 1, 2 and 3.
    Public level(MAX_LEVELS - 1) As TYPE_PLAYER_TRACKING_LEVEL
    Public points_multiplier_per_provider As DataTable
    Public levels_expiration_day_month As String
    Public max_points As String
    Public max_points_per_minute As String
    Public has_zero_plays As Boolean
    Public has_mismatch As Boolean

    ' Init structure
    Public Sub New()
      Dim idx As Integer

      points_expiration = ""
      points_expiration_day_month = ""
      gift_expiration = ""
      levels_days_counting_points = ""

      num_levels = MAX_LEVELS

      For idx = 0 To num_levels - 1
        level(idx) = New TYPE_PLAYER_TRACKING_LEVEL()
      Next

      points_multiplier_per_provider = New DataTable()

      levels_expiration_day_month = ""
      is_multisite_member = False

      max_points = ""
      max_points_per_minute = ""
      has_zero_plays = False
      has_mismatch = False

    End Sub
  End Class

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_TERMINAL
    Public name As String
    Public id As Integer
    Public external_id As String
    Public machine_id As Integer
    Public status As Integer
    Public Added As Boolean
    Public Updated As Boolean
    Public Deleted As Boolean

    Public Sub Clear()
      name = ""
      id = 0
      external_id = ""
      machine_id = 0
      Added = False
      Updated = False
      Deleted = False
    End Sub
  End Class

#Region " Enums "

#End Region

#Region " Members "

  Protected m_cashier_configuration As New TYPE_CASHIER_CONFIGURATION
  Protected m_wcp_cashier_configuration As New TYPE_WCP_CASHIER_CONFIGURATION
  Protected m_alesis_cashier_configuration As New TYPE_ALESIS_CASHIER_CONFIGURATION
  Protected m_player_tracking_configuration As New TYPE_PLAYER_TRACKING_CONFIGURATION
  Protected m_taxes_business_a_configuration As New TYPE_TAXES_CONFIGURATION
  Protected m_taxes_business_b_configuration As New TYPE_TAXES_CONFIGURATION
  Protected m_evidence_configuration As New TYPE_EVIDENCE_CONFIGURATION
  Protected m_cfdi_cashier_configuration As New TYPE_CFDI_CASHIER_CONFIGURATION
  Protected m_comments As String
  Protected m_card_player_concept As String
  Protected m_card_player_amount_company_b As Boolean
  Protected m_player_level_icons As New List(Of PlayerLevelIcon)

#End Region 'Members

#Region "Properties"

  Public Property CashierType() As Integer
    Get
      Return m_cashier_configuration.cashier_type
    End Get

    Set(ByVal Value As Integer)
      m_cashier_configuration.cashier_type = Value
    End Set

  End Property ' CashierType

  Public Overloads ReadOnly Property ConfiguredContains(ByVal terminal As TYPE_TERMINAL) As Boolean
    Get
      Dim temp_term As TYPE_TERMINAL
      For Each temp_term In Me.ConfTerms
        If (terminal.id = temp_term.id) Then
          Return True
        End If
      Next
      Return False
    End Get
  End Property ' ConfiguredContains

  Public Overloads ReadOnly Property Changed(ByVal terminal As TYPE_TERMINAL) As Boolean
    Get
      Dim temp_term As TYPE_TERMINAL
      For Each temp_term In Me.ConfTerms
        If (terminal.id = temp_term.id) Then
          If (terminal.machine_id = temp_term.machine_id) Then
            Return False
          Else
            Return True
          End If
        End If
      Next
      Return False
    End Get
  End Property ' Changed

  Public Overloads Property DataBaseIp() As String
    Get
      Return m_alesis_cashier_configuration.database_ip
    End Get

    Set(ByVal Value As String)
      m_alesis_cashier_configuration.database_ip = Value
    End Set

  End Property ' DataBaseIp

  Public Overloads Property DataBaseName() As String
    Get
      Return m_alesis_cashier_configuration.database_name
    End Get

    Set(ByVal Value As String)
      m_alesis_cashier_configuration.database_name = Value
    End Set

  End Property ' DataBaseName

  Public Overloads Property VendorId() As String
    Get
      Return m_alesis_cashier_configuration.vendor_id
    End Get

    Set(ByVal Value As String)
      m_alesis_cashier_configuration.vendor_id = Value
    End Set

  End Property ' VendorId

  Public Overloads Property Username() As String
    Get
      Return m_alesis_cashier_configuration.username
    End Get

    Set(ByVal Value As String)
      m_alesis_cashier_configuration.username = Value
    End Set

  End Property ' Username

  Public Overloads Property Password() As String
    Get
      Return m_alesis_cashier_configuration.password
    End Get

    Set(ByVal Value As String)
      m_alesis_cashier_configuration.password = Value
    End Set

  End Property ' Password

  Public Overloads Property ConfTerms() As List(Of TYPE_TERMINAL)
    Get
      Return m_alesis_cashier_configuration.configured_terminals
    End Get

    Set(ByVal Value As List(Of TYPE_TERMINAL))
      m_alesis_cashier_configuration.configured_terminals = Value
    End Set

  End Property ' ConfTerms

  Public Overloads Property CardPrice() As String
    Get
      Return m_wcp_cashier_configuration.card_price
    End Get

    Set(ByVal Value As String)
      m_wcp_cashier_configuration.card_price = Value
    End Set

  End Property ' CardPrice

  Public Overloads Property PersonalCardPrice() As String
    Get
      Return m_wcp_cashier_configuration.personal_card_price
    End Get

    Set(ByVal Value As String)
      m_wcp_cashier_configuration.personal_card_price = Value
    End Set

  End Property ' PersonalCardPrice

  Public Overloads Property PersonalCardReplacementPrice() As String
    Get
      Return m_wcp_cashier_configuration.personal_card_replacement_price
    End Get

    Set(ByVal Value As String)
      m_wcp_cashier_configuration.personal_card_replacement_price = Value
    End Set

  End Property ' PersonalCardReplacementPrice

  Public Overloads Property PersonalCardReplacementPriceInPoints() As String
    Get
      Return m_wcp_cashier_configuration.personal_card_replacement_price_in_points
    End Get

    Set(ByVal Value As String)
      m_wcp_cashier_configuration.personal_card_replacement_price_in_points = Value
    End Set

  End Property ' PersonalCardReplacementPriceInPoints

  Public Overloads Property PersonalCardReplacementChargeAfterTimes() As String
    Get
      Return m_wcp_cashier_configuration.personal_card_replacement_charge_after_times
    End Get

    Set(ByVal Value As String)
      m_wcp_cashier_configuration.personal_card_replacement_charge_after_times = Value
    End Set

  End Property ' PersonalCardReplacementChargeAfterTimes

  Public Overloads Property CardRefundable() As String
    Get
      Return m_wcp_cashier_configuration.card_refundable
    End Get

    Set(ByVal Value As String)
      m_wcp_cashier_configuration.card_refundable = Value
    End Set

  End Property ' CardRefundable

  Public Overloads Property CashierMode() As String
    Get
      Return m_wcp_cashier_configuration.cashier_mode
    End Get

    Set(ByVal Value As String)
      m_wcp_cashier_configuration.cashier_mode = Value
    End Set

  End Property ' CashierMode

  Public Overloads Property Language() As String
    Get
      Return m_wcp_cashier_configuration.language
    End Get

    Set(ByVal Value As String)
      m_wcp_cashier_configuration.language = Value
    End Set

  End Property ' Language

  Public Overloads Property Taxes1Pct() As String
    Get
      Return m_wcp_cashier_configuration.tax_1_pct
    End Get

    Set(ByVal Value As String)
      m_wcp_cashier_configuration.tax_1_pct = Value
    End Set

  End Property ' Taxes1Pct

  Public Overloads Property Taxes2Pct() As String
    Get
      Return m_wcp_cashier_configuration.tax_2_pct
    End Get

    Set(ByVal Value As String)
      m_wcp_cashier_configuration.tax_2_pct = Value
    End Set

  End Property ' Taxes2Pct

  Public Overloads Property Taxes3Pct() As String
    Get
      Return m_wcp_cashier_configuration.tax_3_pct
    End Get

    Set(ByVal Value As String)
      m_wcp_cashier_configuration.tax_3_pct = Value
    End Set

  End Property ' Taxes3Pct

  Public Overloads Property TaxesPromo1Pct() As String
    Get
      Return m_wcp_cashier_configuration.tax_promo_1_pct
    End Get

    Set(ByVal Value As String)
      m_wcp_cashier_configuration.tax_promo_1_pct = Value
    End Set

  End Property ' Taxes1Pct

  Public Overloads Property TaxesPromo2Pct() As String
    Get
      Return m_wcp_cashier_configuration.tax_promo_2_pct
    End Get

    Set(ByVal Value As String)
      m_wcp_cashier_configuration.tax_promo_2_pct = Value
    End Set

  End Property ' Taxes2Pct

  Public Overloads Property TaxesPromo3Pct() As String
    Get
      Return m_wcp_cashier_configuration.tax_promo_3_pct
    End Get

    Set(ByVal Value As String)
      m_wcp_cashier_configuration.tax_promo_3_pct = Value
    End Set

  End Property ' Taxes3Pct

  Public Overloads Property EnableRedeemPromotionTaxes() As String
    Get
      Return m_wcp_cashier_configuration.enable_promo_taxes
    End Get

    Set(ByVal Value As String)
      m_wcp_cashier_configuration.enable_promo_taxes = Value
    End Set

  End Property ' Taxes2Pct

  Public Overloads Property MinTimeToCloseSession() As String
    Get
      Return m_wcp_cashier_configuration.min_time_close_session
    End Get

    Set(ByVal Value As String)
      m_wcp_cashier_configuration.min_time_close_session = Value
    End Set

  End Property ' MinTimeToCloseSession

  Public Overloads Property VoucherHeader() As String
    Get
      Return m_wcp_cashier_configuration.voucher_header
    End Get

    Set(ByVal Value As String)
      m_wcp_cashier_configuration.voucher_header = Value
    End Set

  End Property ' VoucherHeader

  Public Overloads Property VoucherFooter() As String
    Get
      Return m_wcp_cashier_configuration.voucher_footer
    End Get

    Set(ByVal Value As String)
      m_wcp_cashier_configuration.voucher_footer = Value
    End Set

  End Property ' VoucherFooter

  Public Overloads Property VoucherFooterAccountsCustomize() As String
    Get
      Return m_wcp_cashier_configuration.voucher_footer_account_customize
    End Get

    Set(ByVal Value As String)
      m_wcp_cashier_configuration.voucher_footer_account_customize = Value
    End Set

  End Property ' VoucherFooterAccountsCustomize

  Public Overloads Property CardPrintTopX() As String
    Get
      Return m_wcp_cashier_configuration.topx
    End Get

    Set(ByVal Value As String)
      m_wcp_cashier_configuration.topx = Value
    End Set

  End Property ' CardPrintTopX

  Public Overloads Property CardPrintTopY() As String
    Get
      Return m_wcp_cashier_configuration.topy
    End Get

    Set(ByVal Value As String)
      m_wcp_cashier_configuration.topy = Value
    End Set

  End Property ' CardPrintTopY

  Public Overloads Property CardPrintBottomX() As String
    Get
      Return m_wcp_cashier_configuration.bottomx
    End Get

    Set(ByVal Value As String)
      m_wcp_cashier_configuration.bottomx = Value
    End Set

  End Property ' CardPrintBottomX

  Public Overloads Property CardPrintBottomY() As String
    Get
      Return m_wcp_cashier_configuration.bottomy
    End Get

    Set(ByVal Value As String)
      m_wcp_cashier_configuration.bottomy = Value
    End Set

  End Property ' CardPrintBottomY

  Public Overloads Property CardPrintFontSize() As String
    Get
      Return m_wcp_cashier_configuration.font_size
    End Get

    Set(ByVal Value As String)
      m_wcp_cashier_configuration.font_size = Value
    End Set

  End Property ' CardPrintFontSize

  Public Overloads Property CardPrinterName() As String
    Get
      Return m_wcp_cashier_configuration.printer_name
    End Get

    Set(ByVal Value As String)
      m_wcp_cashier_configuration.printer_name = Value
    End Set

  End Property ' CardPrinterName

  Public Overloads Property CardPrintBold() As String
    Get
      Return m_wcp_cashier_configuration.font_bold
    End Get

    Set(ByVal Value As String)
      m_wcp_cashier_configuration.font_bold = Value
    End Set

  End Property ' CardPrintBold

  Public Overloads Property AllowAnon() As String
    Get
      Return m_wcp_cashier_configuration.allow_anon
    End Get

    Set(ByVal Value As String)
      m_wcp_cashier_configuration.allow_anon = Value
    End Set

  End Property ' AllowAnon

  Public Overloads Property HandPayTimePeriod() As String
    Get
      Return m_wcp_cashier_configuration.handpay_time_period
    End Get

    Set(ByVal Value As String)
      m_wcp_cashier_configuration.handpay_time_period = Value
    End Set

  End Property ' HandPayTimePeriod

  Public Overloads Property HandPayTimeToCancel() As String
    Get
      Return m_wcp_cashier_configuration.handpay_time_to_cancel
    End Get

    Set(ByVal Value As String)
      m_wcp_cashier_configuration.handpay_time_to_cancel = Value
    End Set

  End Property ' HandPayTimeToCancel

  Public Overloads Property PlayerTrackingPointsExpiration() As String
    Get
      Return m_player_tracking_configuration.points_expiration
    End Get

    Set(ByVal Value As String)
      m_player_tracking_configuration.points_expiration = Value
    End Set

  End Property ' PlayerTrackingPointsExpiration

  Public Overloads Property PlayerTrackingPointsExpirationDayMonth() As String
    Get
      Return m_player_tracking_configuration.points_expiration_day_month
    End Get

    Set(ByVal Value As String)
      m_player_tracking_configuration.points_expiration_day_month = Value
    End Set

  End Property ' PlayerTrackingPointsExpirationDayMonth

  Public Overloads Property PlayerTrackingMaxPoints() As String
    Get
      Return m_player_tracking_configuration.max_points
    End Get

    Set(ByVal Value As String)
      m_player_tracking_configuration.max_points = Value
    End Set

  End Property ' PlayerTrackingMaxPoints

  Public Overloads Property PlayerTrackingHasZeroPlays() As Boolean
    Get
      Return m_player_tracking_configuration.has_zero_plays
    End Get

    Set(ByVal Value As Boolean)
      m_player_tracking_configuration.has_zero_plays = Value
    End Set

  End Property ' PlayerTrackingHasZeroPlays

  Public Overloads Property PlayerTrackingHasMismatch() As Boolean
    Get
      Return m_player_tracking_configuration.has_mismatch
    End Get

    Set(ByVal Value As Boolean)
      m_player_tracking_configuration.has_mismatch = Value
    End Set

  End Property ' PlayerTrackingHasMismatch

  Public Overloads Property PlayerTrackingMaxPointsPerMinute() As String
    Get
      Return m_player_tracking_configuration.max_points_per_minute
    End Get

    Set(ByVal Value As String)
      m_player_tracking_configuration.max_points_per_minute = Value
    End Set

  End Property ' PlayerTrackingMaxPoints

  Public Overloads ReadOnly Property IsMultisiteMember() As Boolean
    Get
      Return m_player_tracking_configuration.is_multisite_member
    End Get
  End Property

  Public Overloads Property PlayerTrackingGiftExpiration() As String
    Get
      Return m_player_tracking_configuration.gift_expiration
    End Get

    Set(ByVal Value As String)
      m_player_tracking_configuration.gift_expiration = Value
    End Set

  End Property ' PlayerTrackingGiftExpiration

  Public Overloads Property PlayerTrackingLevelsExpirationDayMonth() As String
    Get
      Return m_player_tracking_configuration.levels_expiration_day_month
    End Get

    Set(ByVal Value As String)
      m_player_tracking_configuration.levels_expiration_day_month = Value
    End Set

  End Property ' PlayerTrackingLevelsExpirationDayMonth


  Public Overloads Property PlayerTrackingLevelsDaysCountingPoints() As String
    Get
      Return m_player_tracking_configuration.levels_days_counting_points
    End Get

    Set(ByVal Value As String)
      m_player_tracking_configuration.levels_days_counting_points = Value
    End Set

  End Property ' PlayerTrackingLevelsDaysCountingPoints

  Public Overloads Property PlayerTrackingLevelName(ByVal LevelId As Integer) As String
    Get
      If LevelId < 0 Or LevelId >= m_player_tracking_configuration.num_levels Then
        Return ""
      End If

      Return m_player_tracking_configuration.level(LevelId).name
    End Get

    Set(ByVal Value As String)
      If LevelId >= 0 Or LevelId < m_player_tracking_configuration.num_levels Then
        m_player_tracking_configuration.level(LevelId).name = Value
      End If
    End Set

  End Property ' PlayerTrackingLevelName

  Public Overloads Property PlayerTrackingRedeemPlayedTo1Point(ByVal LevelId As Integer) As String

    Get
      If LevelId < 0 Or LevelId >= m_player_tracking_configuration.num_levels Then
        Return ""
      End If

      Return m_player_tracking_configuration.level(LevelId).factor_redeemable_played_to_1_point
    End Get

    Set(ByVal Value As String)
      If LevelId >= 0 Or LevelId < m_player_tracking_configuration.num_levels Then
        m_player_tracking_configuration.level(LevelId).factor_redeemable_played_to_1_point = Value
      End If
    End Set

  End Property ' PlayerTrackingRedeemPlayedTo1Point

  Public Overloads Property PlayerTrackingTotalPlayedTo1Point(ByVal LevelId As Integer) As String

    Get
      If LevelId < 0 Or LevelId >= m_player_tracking_configuration.num_levels Then
        Return ""
      End If

      Return m_player_tracking_configuration.level(LevelId).factor_total_played_to_1_point
    End Get

    Set(ByVal Value As String)
      If LevelId >= 0 Or LevelId < m_player_tracking_configuration.num_levels Then
        m_player_tracking_configuration.level(LevelId).factor_total_played_to_1_point = Value
      End If
    End Set

  End Property ' PlayerTrackingTotalPlayedTo1Point

  Public Overloads Property PlayerTrackingRedeemableSpentTo1Point(ByVal LevelId As Integer) As String

    Get
      If LevelId < 0 Or LevelId >= m_player_tracking_configuration.num_levels Then
        Return ""
      End If

      Return m_player_tracking_configuration.level(LevelId).factor_redeemable_spent_to_1_point
    End Get

    Set(ByVal Value As String)
      If LevelId >= 0 Or LevelId < m_player_tracking_configuration.num_levels Then
        m_player_tracking_configuration.level(LevelId).factor_redeemable_spent_to_1_point = Value
      End If
    End Set

  End Property ' PlayerTrackingRedeemableSpentTo1Point

  Public Overloads Property PlayerTrackingPointsToEnter(ByVal LevelId As Integer) As String

    Get
      If LevelId < 0 Or LevelId >= m_player_tracking_configuration.num_levels Then
        Return ""
      End If

      Return m_player_tracking_configuration.level(LevelId).points_to_enter
    End Get

    Set(ByVal Value As String)
      If LevelId >= 0 Or LevelId < m_player_tracking_configuration.num_levels Then
        m_player_tracking_configuration.level(LevelId).points_to_enter = Value
      End If
    End Set

  End Property ' PlayerTrackingPointsToEnter

  Public Overloads Property PlayerTrackingDaysOnLevel(ByVal LevelId As Integer) As String

    Get
      If LevelId < 0 Or LevelId >= m_player_tracking_configuration.num_levels Then
        Return ""
      End If

      Return m_player_tracking_configuration.level(LevelId).days_on_level
    End Get

    Set(ByVal Value As String)
      If LevelId >= 0 Or LevelId < m_player_tracking_configuration.num_levels Then
        m_player_tracking_configuration.level(LevelId).days_on_level = Value
      End If
    End Set

  End Property ' PlayerTrackingDaysOnLevel
  Public Overloads Property PlayerTrackingPointsToKeep(ByVal LevelId As Integer) As String

    Get
      If LevelId < 0 Or LevelId >= m_player_tracking_configuration.num_levels Then
        Return ""
      End If

      Return m_player_tracking_configuration.level(LevelId).points_to_keep
    End Get

    Set(ByVal Value As String)
      If LevelId >= 0 Or LevelId < m_player_tracking_configuration.num_levels Then
        m_player_tracking_configuration.level(LevelId).points_to_keep = Value
      End If
    End Set

  End Property ' PlayerTrackingPointsToKeep
  Public Overloads Property PlayerTrackingMaxDaysNoActivity(ByVal LevelId As Integer) As String

    Get
      If LevelId < 0 Or LevelId >= m_player_tracking_configuration.num_levels Then
        Return ""
      End If

      Return m_player_tracking_configuration.level(LevelId).max_days_no_activity
    End Get

    Set(ByVal Value As String)
      If LevelId >= 0 Or LevelId < m_player_tracking_configuration.num_levels Then
        m_player_tracking_configuration.level(LevelId).max_days_no_activity = Value
      End If
    End Set

  End Property ' PlayerTrackingMaxDaysNoActivity

  Public Overloads Property PlayerTrackingUpgradeDowngradeAction(ByVal LevelId As Integer) As String

    Get
      If LevelId < 0 Or LevelId >= m_player_tracking_configuration.num_levels Then
        Return ""
      End If

      Return m_player_tracking_configuration.level(LevelId).upgrade_downgrade_action
    End Get

    Set(ByVal Value As String)
      If LevelId >= 0 Or LevelId < m_player_tracking_configuration.num_levels Then
        m_player_tracking_configuration.level(LevelId).upgrade_downgrade_action = Value
      End If
    End Set

  End Property ' PlayerTrackingUpgradeDowngradeAction

  Public Overloads Property PlayerTrackingPointsMultiplierProviders() As DataTable
    Get
      Return m_player_tracking_configuration.points_multiplier_per_provider
    End Get

    Set(ByVal Value As DataTable)
      m_player_tracking_configuration.points_multiplier_per_provider = Value
    End Set

  End Property ' PlayerTrackingPointsMultiplierProviders

  Public Overloads Property TaxesBusinessAName() As String
    Get
      Return m_taxes_business_a_configuration.business_name
    End Get

    Set(ByVal Value As String)
      m_taxes_business_a_configuration.business_name = Value
    End Set

  End Property ' TaxesBusinessAName

  Public Overloads Property TaxesBusinessAConcept() As String
    Get
      Return m_taxes_business_a_configuration.concept
    End Get

    Set(ByVal Value As String)
      m_taxes_business_a_configuration.concept = Value
    End Set

  End Property ' TaxesBusinessAConcept

  Public Overloads Property TaxesBusinessAPayTitle() As String
    Get
      Return m_taxes_business_a_configuration.pay_title
    End Get

    Set(ByVal Value As String)
      m_taxes_business_a_configuration.pay_title = Value
    End Set

  End Property ' TaxesBusinessAPayTitle

  Public Overloads Property TaxesBusinessADevTitle() As String
    Get
      Return m_taxes_business_a_configuration.dev_title
    End Get

    Set(ByVal Value As String)
      m_taxes_business_a_configuration.dev_title = Value
    End Set

  End Property ' TaxesBusinessADevTitle

  Public Overloads Property TaxesBusinessACashInPct() As String
    Get
      Return m_taxes_business_a_configuration.cashin_pct
    End Get

    Set(ByVal Value As String)
      m_taxes_business_a_configuration.cashin_pct = Value
    End Set

  End Property ' TaxesBusinessACashInPct

  Public Overloads Property TaxesBusinessATaxName() As String
    Get
      Return m_taxes_business_a_configuration.tax_name
    End Get

    Set(ByVal Value As String)
      m_taxes_business_a_configuration.tax_name = Value
    End Set

  End Property ' TaxesBusinessATaxName

  Public Overloads Property TaxesBusinessATaxPct() As String
    Get
      Return m_taxes_business_a_configuration.tax_pct
    End Get

    Set(ByVal Value As String)
      m_taxes_business_a_configuration.tax_pct = Value
    End Set

  End Property ' TaxesBusinessATaxPct

  Public Overloads Property TaxesBusinessAVoucherHeader() As String
    Get
      Return m_taxes_business_a_configuration.voucher_header
    End Get

    Set(ByVal Value As String)
      m_taxes_business_a_configuration.voucher_header = Value
    End Set

  End Property ' TaxesBusinessAVoucherHeader

  Public Overloads Property TaxesBusinessAVoucherFooter() As String
    Get
      Return m_taxes_business_a_configuration.voucher_footer
    End Get

    Set(ByVal Value As String)
      m_taxes_business_a_configuration.voucher_footer = Value
    End Set

  End Property ' TaxesBusinessAVoucherFooter

  Public Overloads Property TaxesBusinessAVoucherFooterCashIn() As String
    Get
      Return m_taxes_business_a_configuration.voucher_footer_cashin
    End Get

    Set(ByVal Value As String)
      m_taxes_business_a_configuration.voucher_footer_cashin = Value
    End Set

  End Property ' TaxesBusinessAVoucherFooterCashIn

  Public Overloads Property TaxesBusinessAVoucherFooterDev() As String
    Get
      Return m_taxes_business_a_configuration.voucher_footer_devolution
    End Get

    Set(ByVal Value As String)
      m_taxes_business_a_configuration.voucher_footer_devolution = Value
    End Set

  End Property ' TaxesBusinessAVoucherFooterDev

  Public Overloads Property TaxesBusinessAVoucherFooterCancel() As String
    Get
      Return m_taxes_business_a_configuration.voucher_footer_cancel
    End Get

    Set(ByVal Value As String)
      m_taxes_business_a_configuration.voucher_footer_cancel = Value
    End Set

  End Property ' TaxesBusinessAVoucherFooterCancel

  Public Overloads Property TaxesBusinessAPrizeCoupon() As String
    Get
      Return m_taxes_business_a_configuration.prize_coupon
    End Get

    Set(ByVal Value As String)
      m_taxes_business_a_configuration.prize_coupon = Value
    End Set

  End Property ' TaxesBusinessAPrizeCoupon

  Public Overloads Property TaxesBusinessAPrizeCouponText() As String
    Get
      Return m_taxes_business_a_configuration.prize_coupon_text
    End Get
    Set(ByVal value As String)
      m_taxes_business_a_configuration.prize_coupon_text = value
    End Set
  End Property ' TaxesBusinessAPrizeCouponText

  Public Overloads Property TaxesBusinessAHideTotalCashIn() As String
    Get
      Return m_taxes_business_a_configuration.hide_cashin
    End Get
    Set(ByVal value As String)
      m_taxes_business_a_configuration.hide_cashin = value
    End Set
  End Property ' TaxesBusinessAHideTotalCashIn

  Public Overloads Property TaxesBusinessAPromoAndCouponTogether() As String
    Get
      Return m_taxes_business_a_configuration.promo_and_coupon_together
    End Get
    Set(ByVal value As String)
      m_taxes_business_a_configuration.promo_and_coupon_together = value
    End Set
  End Property ' TaxesBusinessAPromoAndCouponTogether

  Public Overloads Property TaxesBusinessAPromoAndCouponTogetherText() As String
    Get
      Return m_taxes_business_a_configuration.text_promo_and_coupon_together
    End Get
    Set(ByVal value As String)
      m_taxes_business_a_configuration.text_promo_and_coupon_together = value
    End Set
  End Property ' TaxesBusinessAPromoAndCouponTogetherText

  Public Overloads Property TaxesBusinessAHidePromo() As String
    Get
      Return m_taxes_business_a_configuration.hide_promo
    End Get

    Set(ByVal Value As String)
      m_taxes_business_a_configuration.hide_promo = Value
    End Set

  End Property ' TaxesBusinessAHidePromo

  Public Overloads Property TaxesBusinessAPromotionText() As String
    Get
      Return m_taxes_business_a_configuration.text_promotion
    End Get
    Set(ByVal value As String)
      m_taxes_business_a_configuration.text_promotion = value
    End Set
  End Property ' TaxesBusinessAPromotionText

  Public Overloads Property TaxesBusinessBName() As String
    Get
      Return m_taxes_business_b_configuration.business_name
    End Get

    Set(ByVal Value As String)
      m_taxes_business_b_configuration.business_name = Value
    End Set

  End Property ' TaxesBusinessBName

  Public Overloads Property TaxesBusinessBConcept() As String
    Get
      Return m_taxes_business_b_configuration.concept
    End Get

    Set(ByVal Value As String)
      m_taxes_business_b_configuration.concept = Value
    End Set

  End Property ' TaxesBusinessBConcept

  Public Overloads Property TaxesBusinessBPayTitle() As String
    Get
      Return m_taxes_business_b_configuration.pay_title
    End Get

    Set(ByVal Value As String)
      m_taxes_business_b_configuration.pay_title = Value
    End Set

  End Property ' TaxesBusinessBPayTitle

  Public Overloads Property TaxesBusinessBDevTitle() As String
    Get
      Return m_taxes_business_b_configuration.dev_title
    End Get

    Set(ByVal Value As String)
      m_taxes_business_b_configuration.dev_title = Value
    End Set

  End Property ' TaxesBusinessBDevTitle

  Public Overloads Property TaxesBusinessBCashInPct() As String
    Get
      Return m_taxes_business_b_configuration.cashin_pct
    End Get

    Set(ByVal Value As String)
      m_taxes_business_b_configuration.cashin_pct = Value
    End Set

  End Property ' TaxesBusinessBCashInPct

  Public Overloads Property TaxesBusinessBTaxName() As String
    Get
      Return m_taxes_business_b_configuration.tax_name
    End Get

    Set(ByVal Value As String)
      m_taxes_business_b_configuration.tax_name = Value
    End Set

  End Property ' TaxesBusinessBTaxName

  Public Overloads Property TaxesBusinessBTaxPct() As String
    Get
      Return m_taxes_business_b_configuration.tax_pct
    End Get

    Set(ByVal Value As String)
      m_taxes_business_b_configuration.tax_pct = Value
    End Set

  End Property ' TaxesBusinessBTaxPct

  Public Overloads Property TaxesBusinessBVoucherHeader() As String
    Get
      Return m_taxes_business_b_configuration.voucher_header
    End Get

    Set(ByVal Value As String)
      m_taxes_business_b_configuration.voucher_header = Value
    End Set

  End Property ' TaxesBusinessBVoucherHeader

  Public Overloads Property TaxesBusinessBVoucherFooter() As String
    Get
      Return m_taxes_business_b_configuration.voucher_footer
    End Get

    Set(ByVal Value As String)
      m_taxes_business_b_configuration.voucher_footer = Value
    End Set

  End Property ' TaxesBusinessBVoucherFooter

  Public Overloads Property TaxesBusinessBVoucherFooterCashIn() As String
    Get
      Return m_taxes_business_b_configuration.voucher_footer_cashin
    End Get

    Set(ByVal Value As String)
      m_taxes_business_b_configuration.voucher_footer_cashin = Value
    End Set

  End Property ' TaxesBusinessBVoucherFooterCashIn

  Public Overloads Property TaxesBusinessBVoucherFooterDev() As String
    Get
      Return m_taxes_business_b_configuration.voucher_footer_devolution
    End Get

    Set(ByVal Value As String)
      m_taxes_business_b_configuration.voucher_footer_devolution = Value
    End Set

  End Property ' TaxesBusinessBVoucherFooterDev

  Public Overloads Property TaxesBusinessBVoucherFooterCancel() As String
    Get
      Return m_taxes_business_b_configuration.voucher_footer_cancel
    End Get

    Set(ByVal Value As String)
      m_taxes_business_b_configuration.voucher_footer_cancel = Value
    End Set

  End Property ' TaxesBusinessBVoucherFooterCancel

  Public Overloads Property TaxesBusinessBHideTotalCashIn() As String
    Get
      Return m_taxes_business_b_configuration.hide_cashin
    End Get
    Set(ByVal value As String)
      m_taxes_business_b_configuration.hide_cashin = value
    End Set
  End Property ' TaxesBusinessBHideTotalCashIn

  Public Overloads Property TaxesBusinessBEnable() As String
    Get
      Return m_taxes_business_b_configuration.enabled
    End Get

    Set(ByVal Value As String)
      m_taxes_business_b_configuration.enabled = Value
    End Set

  End Property ' TaxesBusinessBEnable

  Public Overloads Property Taxes1Name() As String
    Get
      Return m_wcp_cashier_configuration.tax_1_name
    End Get

    Set(ByVal Value As String)
      m_wcp_cashier_configuration.tax_1_name = Value
    End Set

  End Property ' Taxes1Name

  Public Overloads Property Taxes2Name() As String
    Get
      Return m_wcp_cashier_configuration.tax_2_name
    End Get

    Set(ByVal Value As String)
      m_wcp_cashier_configuration.tax_2_name = Value
    End Set

  End Property ' Taxes2Name

  Public Overloads Property Taxes3Name() As String
    Get
      Return m_wcp_cashier_configuration.tax_3_name
    End Get

    Set(ByVal Value As String)
      m_wcp_cashier_configuration.tax_3_name = Value
    End Set

  End Property ' Taxes3Name

  Public Overloads Property Taxes1ApplyIf() As String
    Get
      Return m_wcp_cashier_configuration.tax_1_apply_if
    End Get

    Set(ByVal Value As String)
      m_wcp_cashier_configuration.tax_1_apply_if = Value
    End Set

  End Property ' Taxes1ApplyIf

  Public Overloads Property Taxes2ApplyIf() As String
    Get
      Return m_wcp_cashier_configuration.tax_2_apply_if
    End Get

    Set(ByVal Value As String)
      m_wcp_cashier_configuration.tax_2_apply_if = Value
    End Set

  End Property ' Taxes2ApplyIf

  Public Overloads Property Taxes3ApplyIf() As String
    Get
      Return m_wcp_cashier_configuration.tax_3_apply_if
    End Get

    Set(ByVal Value As String)
      m_wcp_cashier_configuration.tax_3_apply_if = Value
    End Set

  End Property ' Taxes3ApplyIf

  Public Overloads Property TaxesPromo1ApplyIf() As String
    Get
      Return m_wcp_cashier_configuration.tax_promo_1_apply_if
    End Get

    Set(ByVal Value As String)
      m_wcp_cashier_configuration.tax_promo_1_apply_if = Value
    End Set

  End Property ' Taxes1ApplyIf

  Public Overloads Property TaxesPromo2ApplyIf() As String
    Get
      Return m_wcp_cashier_configuration.tax_promo_2_apply_if
    End Get

    Set(ByVal Value As String)
      m_wcp_cashier_configuration.tax_promo_2_apply_if = Value
    End Set

  End Property ' Taxes2ApplyIf

  Public Overloads Property TaxesPromo3ApplyIf() As String
    Get
      Return m_wcp_cashier_configuration.tax_promo_3_apply_if
    End Get

    Set(ByVal Value As String)
      m_wcp_cashier_configuration.tax_promo_3_apply_if = Value
    End Set

  End Property ' Taxes3ApplyIf

  Public Overloads Property EvidenceCompanyId1() As String
    Get
      Return Me.m_evidence_configuration.company_id1
    End Get
    Set(ByVal value As String)
      Me.m_evidence_configuration.company_id1 = value
    End Set
  End Property

  Public Overloads Property EvidenceCompanyId2() As String
    Get
      Return Me.m_evidence_configuration.company_id2
    End Get
    Set(ByVal value As String)
      Me.m_evidence_configuration.company_id2 = value
    End Set
  End Property

  Public Overloads Property EvidenceCompanyName() As String
    Get
      Return Me.m_evidence_configuration.company_name
    End Get
    Set(ByVal value As String)
      Me.m_evidence_configuration.company_name = value
    End Set
  End Property

  Public Overloads Property EvidenceRepresentativeId1() As String
    Get
      Return Me.m_evidence_configuration.representative_id1
    End Get
    Set(ByVal value As String)
      Me.m_evidence_configuration.representative_id1 = value
    End Set
  End Property

  Public Overloads Property EvidenceRepresentativeId2() As String
    Get
      Return Me.m_evidence_configuration.representative_id2
    End Get
    Set(ByVal value As String)
      Me.m_evidence_configuration.representative_id2 = value
    End Set
  End Property

  Public Overloads Property EvidenceRepresentativeName() As String
    Get
      Return Me.m_evidence_configuration.representative_name
    End Get
    Set(ByVal value As String)
      Me.m_evidence_configuration.representative_name = value
    End Set
  End Property

  Public Overloads Property EvidenceRepresentativeSign() As Image
    Get
      Return Me.m_evidence_configuration.representative_sign
    End Get
    Set(ByVal value As Image)
      Me.m_evidence_configuration.representative_sign = value
    End Set
  End Property

  Public Overloads Property EvidenceEnabled() As Boolean
    Get
      Return Me.m_evidence_configuration.enabled
    End Get
    Set(ByVal value As Boolean)
      Me.m_evidence_configuration.enabled = value
    End Set
  End Property

  Public Overloads Property EvidenceEditableMinutes() As Integer
    Get
      Return Me.m_evidence_configuration.editable_minutes
    End Get
    Set(ByVal value As Integer)
      Me.m_evidence_configuration.editable_minutes = value
    End Set
  End Property

  Public Overloads Property EvidencePrizeGreaterThan() As Decimal
    Get
      Return Me.m_evidence_configuration.prize_greater_than
    End Get
    Set(ByVal value As Decimal)
      Me.m_evidence_configuration.prize_greater_than = value
    End Set
  End Property

  'LTC 22-JUN-2016
  Public Overloads Property GeneratedWitholdingDocument() As Integer
    Get
      Return Me.m_evidence_configuration.generated_witholding_document
    End Get
    Set(ByVal value As Integer)
      Me.m_evidence_configuration.generated_witholding_document = value
    End Set
  End Property

  Public Property Comments() As String
    Get
      Return Me.m_comments
    End Get
    Set(ByVal value As String)
      Me.m_comments = value
    End Set
  End Property

  Public Property CardPlayerConcept() As String
    Get
      Return Me.m_card_player_concept
    End Get
    Set(ByVal value As String)
      Me.m_card_player_concept = value
    End Set
  End Property


  Public Overloads Property CardPlayerAmountCompanyB() As Boolean
    Get
      Return Me.m_card_player_amount_company_b
    End Get

    Set(ByVal Value As Boolean)
      Me.m_card_player_amount_company_b = Value
    End Set

  End Property ' AmountCompanyB

  Public Overloads Property CFDI_Active() As String
    Get
      Return m_cfdi_cashier_configuration.active
    End Get
    Set(ByVal Value As String)
      m_cfdi_cashier_configuration.active = Value
    End Set
  End Property

  Public Overloads Property CFDI_Url() As String
    Get
      Return m_cfdi_cashier_configuration.url
    End Get
    Set(ByVal Value As String)
      m_cfdi_cashier_configuration.url = Value
    End Set
  End Property

  Public Overloads Property CFDI_User() As String
    Get
      Return m_cfdi_cashier_configuration.user
    End Get
    Set(ByVal Value As String)
      m_cfdi_cashier_configuration.user = Value
    End Set
  End Property

  Public Overloads Property CFDI_Password() As String
    Get
      Return m_cfdi_cashier_configuration.password
    End Get
    Set(ByVal Value As String)
      m_cfdi_cashier_configuration.password = Value
    End Set
  End Property

  Public Overloads Property CFDI_SchedulingLoad() As Integer
    Get
      Return m_cfdi_cashier_configuration.scheduling_load
    End Get
    Set(ByVal Value As Integer)
      m_cfdi_cashier_configuration.scheduling_load = Value
    End Set
  End Property

  Public Overloads Property CFDI_SchedulingProcess() As Integer
    Get
      Return m_cfdi_cashier_configuration.scheduling_process
    End Get
    Set(ByVal Value As Integer)
      m_cfdi_cashier_configuration.scheduling_process = Value
    End Set
  End Property
  Public Overloads Property CFDI_Retries() As Integer
    Get
      Return m_cfdi_cashier_configuration.retries
    End Get
    Set(ByVal Value As Integer)
      m_cfdi_cashier_configuration.retries = Value
    End Set
  End Property

  Public Overloads Property CFDI_Site() As String
    Get
      Return m_cfdi_cashier_configuration.site
    End Get
    Set(ByVal Value As String)
      m_cfdi_cashier_configuration.site = Value
    End Set
  End Property

  Public Overloads ReadOnly Property CFDI_Resume_Current_Month() As TYPE_CFDI_REGISTERS_STATUS
    Get
      Dim _first_day As DateTime
      _first_day = New DateTime(DateTime.Now.Year, DateTime.Now.Month, 1)
      Return GetCFDICountersByStatus(_first_day, _first_day.AddMonths(1))
    End Get
  End Property

  Public Overloads ReadOnly Property CFDI_Resume_Last_Month() As TYPE_CFDI_REGISTERS_STATUS
    Get
      Dim _first_day As DateTime
      _first_day = New DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(-1)
      Return GetCFDICountersByStatus(_first_day, _first_day.AddMonths(1))
    End Get
  End Property

  Public Overloads Property CFDI_RFC_Anonymous() As String
    Get
      Return m_cfdi_cashier_configuration.RFCAnonymous
    End Get
    Set(ByVal Value As String)
      m_cfdi_cashier_configuration.RFCAnonymous = Value
    End Set
  End Property

  Public Overloads Property CFDI_EF_Anonymous() As String
    Get
      Return m_cfdi_cashier_configuration.EFAnonymous
    End Get
    Set(ByVal Value As String)
      m_cfdi_cashier_configuration.EFAnonymous = Value
    End Set
  End Property

  Public Overloads Property CFDI_RFC_Foreign() As String
    Get
      Return m_cfdi_cashier_configuration.RFCForeign
    End Get
    Set(ByVal Value As String)
      m_cfdi_cashier_configuration.RFCForeign = Value
    End Set
  End Property

  Public Overloads Property CFDI_EF_Foreign() As String
    Get
      Return m_cfdi_cashier_configuration.EFForeign
    End Get
    Set(ByVal Value As String)
      m_cfdi_cashier_configuration.EFForeign = Value
    End Set
  End Property

  Public Property PlayerLevelIcons() As List(Of PlayerLevelIcon)
    Get
      Return m_player_level_icons
    End Get
    Set(value As List(Of PlayerLevelIcon))
      m_player_level_icons = value
    End Set
  End Property

#End Region 'Properties

#Region "Overrides"

  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim auditor_data As CLASS_AUDITOR_DATA
    Dim field_name As String
    Dim field_value As String
    Dim idx As Integer
    Dim base_field_name As String
    Dim _info_img_sign As String

    auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_CASHIER_CONFIGURATION)

    auditor_data.SetName(GLB_NLS_GUI_CONFIGURATION.Id(201), "")

    'Cashier configuration parameters
    auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(208), CashierType())                                               ' 208 "Cashier type"

    ' WCP params
    auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(224), GUI_DBNumberToLocalNumber(CardPrice()))                      ' 224 "Card price"
    auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(248), CardRefundable())                                            ' 248 "Personal Card Refundable"
    auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(246), GUI_DBNumberToLocalNumber(PersonalCardPrice()))              ' 246 "Personal Card Price"
    auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(247), GUI_DBNumberToLocalNumber(PersonalCardReplacementPrice()))   ' 247 "Personal Card Replacement Price"
    'auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5055), PersonalCardReplacementPriceInPoints)                    ' 247 "Personal Card Replacement Price In Points"
    auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5056), GUI_DBNumberToLocalNumber(PersonalCardReplacementChargeAfterTimes()))  ' 247 "Personal Card Replacement Charge After Times"

    field_value = ""
    Select Case CashierMode
      Case GLB_NLS_GUI_CONFIGURATION.GetString(234)
        field_value = GLB_NLS_GUI_CONFIGURATION.GetString(231)

      Case GLB_NLS_GUI_CONFIGURATION.GetString(235)
        field_value = GLB_NLS_GUI_CONFIGURATION.GetString(232)
    End Select

    auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(225), field_value)                     ' 225 "Cashier mode"
    auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(226), Language())                      ' 226 "Language"
    auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(284), MinTimeToCloseSession())         ' 284 "Min. Time to Close Session"

    ' 237 "Voucher" 
    ' 228 "Header"
    field_name = GLB_NLS_GUI_CONFIGURATION.GetString(237) & "." & GLB_NLS_GUI_CONFIGURATION.GetString(228)
    Call auditor_data.SetField(0, VoucherHeader, field_name)

    ' 237 "Voucher" 
    ' 229 "Footer"
    field_name = GLB_NLS_GUI_CONFIGURATION.GetString(237) & "." & GLB_NLS_GUI_CONFIGURATION.GetString(229)
    Call auditor_data.SetField(0, VoucherFooter, field_name)

    ' PGJ 26-JUN-2013: Account customization voucher
    ' 237 "Voucher" 
    ' 2192 "Accounting customization footer"
    field_name = GLB_NLS_GUI_CONFIGURATION.GetString(237) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2192)
    Call auditor_data.SetField(0, VoucherFooterAccountsCustomize, field_name)

    ' 272 "CardPrint" 
    ' 273 "PrintNameArea_TopX"
    field_name = GLB_NLS_GUI_CONFIGURATION.GetString(272) & "." & GLB_NLS_GUI_CONFIGURATION.GetString(273)
    Call auditor_data.SetField(0, CardPrintTopX, field_name)

    ' 272 "CardPrint" 
    ' 274 "PrintNameArea_TopY"
    field_name = GLB_NLS_GUI_CONFIGURATION.GetString(272) & "." & GLB_NLS_GUI_CONFIGURATION.GetString(274)
    Call auditor_data.SetField(0, CardPrintTopY, field_name)

    ' 272 "CardPrint" 
    ' 275 "PrintNameArea_BottomX"
    field_name = GLB_NLS_GUI_CONFIGURATION.GetString(272) & "." & GLB_NLS_GUI_CONFIGURATION.GetString(275)
    Call auditor_data.SetField(0, CardPrintBottomX, field_name)

    ' 272 "CardPrint" 
    ' 276 "PrintNameArea_BottomY"
    field_name = GLB_NLS_GUI_CONFIGURATION.GetString(272) & "." & GLB_NLS_GUI_CONFIGURATION.GetString(276)
    Call auditor_data.SetField(0, CardPrintBottomY, field_name)

    ' 272 "CardPrint" 
    ' 277 "PrintNameArea_FontSize"
    field_name = GLB_NLS_GUI_CONFIGURATION.GetString(272) & "." & GLB_NLS_GUI_CONFIGURATION.GetString(277)
    Call auditor_data.SetField(0, CardPrintFontSize, field_name)

    ' 272 "CardPrint" 
    ' 279 "QueueName"
    field_name = GLB_NLS_GUI_CONFIGURATION.GetString(272) & "." & GLB_NLS_GUI_CONFIGURATION.GetString(279)
    Call auditor_data.SetField(0, CardPrinterName, field_name)

    ' 272 "CardPrint" 
    ' 282 "PrintNameArea_Bold"
    field_name = GLB_NLS_GUI_CONFIGURATION.GetString(272) & "." & GLB_NLS_GUI_CONFIGURATION.GetString(282)
    field_value = ""
    Select Case CardPrintBold
      Case 0
        field_value = GLB_NLS_GUI_CONFIGURATION.GetString(281)    ' 281 "Normal Mode"
      Case 1
        field_value = GLB_NLS_GUI_CONFIGURATION.GetString(280)    ' 280 "Bold Mode"
    End Select
    Call auditor_data.SetField(0, field_value, field_name)

    Call auditor_data.SetField(0, HandPayTimePeriod, "Cashier.HandPays.TimePeriod")
    Call auditor_data.SetField(0, HandPayTimeToCancel, "Cashier.HandPays.TimeToCancel")

    Select Case AllowAnon
      Case "0"
        field_value = "Not Allowed"
      Case "1"
        field_value = "Allowed"
    End Select
    Call auditor_data.SetField(0, field_value, "Cashier.AllowAnonymous")

    ' Alesis params
    auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(202), DataBaseIp())
    auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(203), DataBaseName())
    auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(205), VendorId())
    auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(206), Username())
    auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(207), Password())

    For idx = 0 To Me.ConfTerms.Count - 1
      base_field_name = GLB_NLS_GUI_CONFIGURATION.GetString(238) & "." & ConfTerms(idx).external_id & "." & GLB_NLS_GUI_CONFIGURATION.GetString(211)
      Call auditor_data.SetField(0, ConfTerms(idx).machine_id, base_field_name)
    Next

    ' TJG 17-SEP-2010 Player Tracking
    Call auditor_data.SetField(0, PlayerTrackingPointsExpiration, "PlayerTracking.PointsExpirationDays")
    Call auditor_data.SetField(0, PlayerTrackingPointsExpirationDayMonth, "PlayerTracking.PointsExpirationDayMonth")
    Call auditor_data.SetField(0, PlayerTrackingGiftExpiration, "PlayerTracking.GiftExpirationDays")
    Call auditor_data.SetField(0, PlayerTrackingLevelsExpirationDayMonth, "PlayerTracking.Levels.ExpirationDayMonth")
    Call auditor_data.SetField(0, PlayerTrackingLevelsDaysCountingPoints, "PlayerTracking.Levels.DaysCountingPoints")
    Call auditor_data.SetField(0, PlayerTrackingLevelName(0), "PlayerTracking.Level01.Name")
    Call auditor_data.SetField(0, PlayerTrackingLevelName(1), "PlayerTracking.Level02.Name")
    Call auditor_data.SetField(0, PlayerTrackingLevelName(2), "PlayerTracking.Level03.Name")
    Call auditor_data.SetField(0, PlayerTrackingLevelName(3), "PlayerTracking.Level04.Name")
    Call auditor_data.SetField(0, GUI_DBNumberToLocalNumber(PlayerTrackingRedeemPlayedTo1Point(0)), "PlayerTracking.Level01.RedeemablePlayedTo1Point")
    Call auditor_data.SetField(0, GUI_DBNumberToLocalNumber(PlayerTrackingRedeemPlayedTo1Point(1)), "PlayerTracking.Level02.RedeemablePlayedTo1Point")
    Call auditor_data.SetField(0, GUI_DBNumberToLocalNumber(PlayerTrackingRedeemPlayedTo1Point(2)), "PlayerTracking.Level03.RedeemablePlayedTo1Point")
    Call auditor_data.SetField(0, GUI_DBNumberToLocalNumber(PlayerTrackingRedeemPlayedTo1Point(3)), "PlayerTracking.Level04.RedeemablePlayedTo1Point")
    Call auditor_data.SetField(0, GUI_DBNumberToLocalNumber(PlayerTrackingTotalPlayedTo1Point(0)), "PlayerTracking.Level01.TotalPlayedTo1Point")
    Call auditor_data.SetField(0, GUI_DBNumberToLocalNumber(PlayerTrackingTotalPlayedTo1Point(1)), "PlayerTracking.Level02.TotalPlayedTo1Point")
    Call auditor_data.SetField(0, GUI_DBNumberToLocalNumber(PlayerTrackingTotalPlayedTo1Point(2)), "PlayerTracking.Level03.TotalPlayedTo1Point")
    Call auditor_data.SetField(0, GUI_DBNumberToLocalNumber(PlayerTrackingTotalPlayedTo1Point(3)), "PlayerTracking.Level04.TotalPlayedTo1Point")
    Call auditor_data.SetField(0, GUI_DBNumberToLocalNumber(PlayerTrackingRedeemableSpentTo1Point(0)), "PlayerTracking.Level01.RedeemableSpentTo1Point")
    Call auditor_data.SetField(0, GUI_DBNumberToLocalNumber(PlayerTrackingRedeemableSpentTo1Point(1)), "PlayerTracking.Level02.RedeemableSpentTo1Point")
    Call auditor_data.SetField(0, GUI_DBNumberToLocalNumber(PlayerTrackingRedeemableSpentTo1Point(2)), "PlayerTracking.Level03.RedeemableSpentTo1Point")
    Call auditor_data.SetField(0, GUI_DBNumberToLocalNumber(PlayerTrackingRedeemableSpentTo1Point(3)), "PlayerTracking.Level04.RedeemableSpentTo1Point")
    Call auditor_data.SetField(0, PlayerTrackingPointsToEnter(1), "PlayerTracking.Level02.PointsToEnter")
    Call auditor_data.SetField(0, PlayerTrackingPointsToEnter(2), "PlayerTracking.Level03.PointsToEnter")
    Call auditor_data.SetField(0, PlayerTrackingPointsToEnter(3), "PlayerTracking.Level04.PointsToEnter")
    Call auditor_data.SetField(0, PlayerTrackingDaysOnLevel(1), "PlayerTracking.Level02.DaysOnLevel")
    Call auditor_data.SetField(0, PlayerTrackingDaysOnLevel(2), "PlayerTracking.Level03.DaysOnLevel")
    Call auditor_data.SetField(0, PlayerTrackingDaysOnLevel(3), "PlayerTracking.Level04.DaysOnLevel")
    Call auditor_data.SetField(0, PlayerTrackingPointsToKeep(1), "PlayerTracking.Level02.PointsToKeep")
    Call auditor_data.SetField(0, PlayerTrackingPointsToKeep(2), "PlayerTracking.Level03.PointsToKeep")
    Call auditor_data.SetField(0, PlayerTrackingPointsToKeep(3), "PlayerTracking.Level04.PointsToKeep")
    Call auditor_data.SetField(0, PlayerTrackingMaxDaysNoActivity(1), "PlayerTracking.Level02.MaxDaysNoActivity")
    Call auditor_data.SetField(0, PlayerTrackingMaxDaysNoActivity(2), "PlayerTracking.Level03.MaxDaysNoActivity")
    Call auditor_data.SetField(0, PlayerTrackingMaxDaysNoActivity(3), "PlayerTracking.Level04.MaxDaysNoActivity")

    'FBA 06-SEP-2013
    Call auditor_data.SetField(0, PlayerTrackingUpgradeDowngradeAction(0), "PlayerTracking.Level01.UpgradeDowngradeAction")
    Call auditor_data.SetField(0, PlayerTrackingUpgradeDowngradeAction(1), "PlayerTracking.Level02.UpgradeDowngradeAction")
    Call auditor_data.SetField(0, PlayerTrackingUpgradeDowngradeAction(2), "PlayerTracking.Level03.UpgradeDowngradeAction")
    Call auditor_data.SetField(0, PlayerTrackingUpgradeDowngradeAction(3), "PlayerTracking.Level04.UpgradeDowngradeAction")

    Call auditor_data.SetField(0, PlayerTrackingMaxPoints, "PlayerTracking.DoNotAwardPoints.PlaySession.PointsAreGreaterThan")
    Call auditor_data.SetField(0, PlayerTrackingMaxPointsPerMinute, "PlayerTracking.DoNotAwardPoints.PlaySession.PointsPerMinuteAreGreaterThan")
    Call auditor_data.SetField(0, GetRepresentativeValue(PlayerTrackingHasZeroPlays), "PlayerTracking.DoNotAwardPoints.PlaySession.HasNoPlays")
    Call auditor_data.SetField(0, GetRepresentativeValue(PlayerTrackingHasMismatch), "PlayerTracking.DoNotAwardPoints.PlaySession.HasBalanceMismatch")

    If GLB_GuiMode = public_globals.ENUM_GUI.WIGOS_GUI Then
      ' RCI 20-JAN-2012
      For Each _points_mult As DataRow In Me.PlayerTrackingPointsMultiplierProviders.Rows
        'Call auditor_data.SetField(0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(569, _
        '                              GUI_FormatNumber(_points_mult(SQL_COLUMN_POINTS_MULTIPLIER), 2)), _
        '                              _points_mult(SQL_COLUMN_PROVIDER_NAME))
        Call auditor_data.SetField(0, "x " & GUI_FormatNumber(_points_mult(SQL_COLUMN_POINTS_MULTIPLIER), 2), _
                                      _points_mult(SQL_COLUMN_PROVIDER_NAME) & ". " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(569))
      Next
    End If

    ' MBF 28-SEP-2010
    Call auditor_data.SetField(0, TaxesBusinessAName, "Taxes.Business.A.Name")
    Call auditor_data.SetField(0, TaxesBusinessAConcept, "Taxes.Business.A.Concept")
    Call auditor_data.SetField(0, GUI_DBNumberToLocalNumber(TaxesBusinessACashInPct), "Taxes.Business.A.CashInPct")
    Call auditor_data.SetField(0, TaxesBusinessATaxName, "Taxes.Business.A.TaxName")
    Call auditor_data.SetField(0, GUI_DBNumberToLocalNumber(TaxesBusinessATaxPct), "Taxes.Business.A.TaxPct")
    Call auditor_data.SetField(0, TaxesBusinessAVoucherHeader, "Taxes.Business.A.Voucher.Header")
    Call auditor_data.SetField(0, TaxesBusinessAVoucherFooter, "Taxes.Business.A.Voucher.Footer")
    Call auditor_data.SetField(0, TaxesBusinessAVoucherFooterCashIn, "Taxes.Business.A.Voucher.Footer.CashIn")
    Call auditor_data.SetField(0, TaxesBusinessAVoucherFooterDev, "Taxes.Business.A.Voucher.Footer.Dev")
    Call auditor_data.SetField(0, TaxesBusinessAVoucherFooterCancel, "Taxes.Business.A.Voucher.Footer.Cancel")
    Call auditor_data.SetField(0, TaxesBusinessAHidePromo, "Taxes.Business.A.HidePromotion")
    Call auditor_data.SetField(0, TaxesBusinessAPromotionText, "Taxes.Business.A.PromotionText")
    Call auditor_data.SetField(0, TaxesBusinessAPrizeCoupon, "Taxes.Business.A.PrizeCoupon")
    Call auditor_data.SetField(0, TaxesBusinessAPrizeCouponText, "Taxes.Business.A.PrizeCouponText")
    Call auditor_data.SetField(0, TaxesBusinessAHideTotalCashIn, "Taxes.Business.A.HideCashIn")
    ' JAB 21-MAR-2012 Add "Promo and CouponTogether" field
    Call auditor_data.SetField(0, TaxesBusinessAPromoAndCouponTogether, "Taxes.Business.A.PromoAndCouponTogether")
    Call auditor_data.SetField(0, TaxesBusinessAPromoAndCouponTogetherText, "Taxes.Business.A.PromoAndCouponTogether.Text")
    Call auditor_data.SetField(0, TaxesBusinessAPayTitle, "Taxes.Business.A.PayTitle")
    Call auditor_data.SetField(0, TaxesBusinessADevTitle, "Taxes.Business.A.DevTitle")


    Call auditor_data.SetField(0, TaxesBusinessBName, "Taxes.Business.B.Name")
    Call auditor_data.SetField(0, TaxesBusinessBConcept, "Taxes.Business.B.Concept")
    Call auditor_data.SetField(0, GUI_DBNumberToLocalNumber(TaxesBusinessBCashInPct), "Taxes.Business.B.CashInPct")
    Call auditor_data.SetField(0, TaxesBusinessBTaxName, "Taxes.Business.B.TaxName")
    Call auditor_data.SetField(0, GUI_DBNumberToLocalNumber(TaxesBusinessBTaxPct), "Taxes.Business.B.TaxPct")
    Call auditor_data.SetField(0, TaxesBusinessBVoucherHeader, "Taxes.Business.B.Voucher.Header")
    Call auditor_data.SetField(0, TaxesBusinessBVoucherFooter, "Taxes.Business.B.Voucher.Footer")
    Call auditor_data.SetField(0, TaxesBusinessBVoucherFooterCashIn, "Taxes.Business.B.Voucher.Footer.CashIn")
    Call auditor_data.SetField(0, TaxesBusinessBVoucherFooterDev, "Taxes.Business.B.Voucher.Footer.Dev")
    Call auditor_data.SetField(0, TaxesBusinessBVoucherFooterCancel, "Taxes.Business.B.Voucher.Footer.Cancel")
    Call auditor_data.SetField(0, TaxesBusinessBHideTotalCashIn, "Taxes.Business.B.HideCashIn")

    Call auditor_data.SetField(0, TaxesBusinessBPayTitle, "Taxes.Business.B.PayTitle")
    Call auditor_data.SetField(0, TaxesBusinessBDevTitle, "Taxes.Business.B.DevTitle")

    Call auditor_data.SetField(0, TaxesBusinessBEnable, "Taxes.Business.B.Enabled")

    Call auditor_data.SetField(0, Taxes1Name, "Tax.OnPrize.1.Name")
    Call auditor_data.SetField(0, Taxes2Name, "Tax.OnPrize.2.Name")
    Call auditor_data.SetField(0, Taxes3Name, "Tax.OnPrize.3.Name")
    Call auditor_data.SetField(0, GUI_DBNumberToLocalNumber(Taxes1Pct), "Tax.OnPrize.1.Pct")
    Call auditor_data.SetField(0, GUI_DBNumberToLocalNumber(Taxes2Pct), "Tax.OnPrize.2.Pct")
    Call auditor_data.SetField(0, GUI_DBNumberToLocalNumber(Taxes3Pct), "Tax.OnPrize.3.Pct")
    Call auditor_data.SetField(0, GUI_DBNumberToLocalNumber(Taxes1ApplyIf), "Tax.OnPrize.1.Threshold")
    Call auditor_data.SetField(0, GUI_DBNumberToLocalNumber(Taxes2ApplyIf), "Tax.OnPrize.2.Threshold")
    Call auditor_data.SetField(0, GUI_DBNumberToLocalNumber(Taxes3ApplyIf), "Tax.OnPrize.3.Threshold")

    Call auditor_data.SetField(0, GUI_DBNumberToLocalNumber(TaxesPromo1Pct), "Promotions.RE.Tax.1.Pct")
    Call auditor_data.SetField(0, GUI_DBNumberToLocalNumber(TaxesPromo2Pct), "Promotions.RE.Tax.2.Pct")
    Call auditor_data.SetField(0, GUI_DBNumberToLocalNumber(TaxesPromo3Pct), "Promotions.RE.Tax.3.Pct")
    Call auditor_data.SetField(0, GUI_DBNumberToLocalNumber(TaxesPromo1ApplyIf), "Promotions.RE.Tax.1.Threshold")
    Call auditor_data.SetField(0, GUI_DBNumberToLocalNumber(TaxesPromo2ApplyIf), "Promotions.RE.Tax.2.Threshold")
    Call auditor_data.SetField(0, GUI_DBNumberToLocalNumber(TaxesPromo3ApplyIf), "Promotions.RE.Tax.3.Threshold")

    Select Case EnableRedeemPromotionTaxes
      Case 0
        field_value = 0
      Case 1
        field_value = 1
    End Select

    Call auditor_data.SetField(0, field_value, "Promotions.RE.Tax.Enabled")

    Call auditor_data.SetField(0, EvidenceRepresentativeId1, "Witholding.Representative.ID1")
    Call auditor_data.SetField(0, EvidenceRepresentativeId2, "Witholding.Representative.ID2")
    Call auditor_data.SetField(0, EvidenceRepresentativeName, "Witholding.Representative.Name")

    _info_img_sign = "0 x 0 => 0 b."
    If Not IsNothing(EvidenceRepresentativeSign) Then
      _info_img_sign = EvidenceRepresentativeSign.Size.Height & " px."
      _info_img_sign &= " x " & EvidenceRepresentativeSign.Size.Width & " px."
      Using _mm = New IO.MemoryStream()
        Try
          EvidenceRepresentativeSign.Save(_mm, EvidenceRepresentativeSign.RawFormat)
        Catch ex As Exception
        End Try
        _info_img_sign &= " => " & _mm.Length & " b."
      End Using
    End If

    Call auditor_data.SetField(0, _info_img_sign, "Witholding.Representative.Sign")

    Call auditor_data.SetField(0, EvidenceCompanyId1, "Witholding.Business.ID1")
    Call auditor_data.SetField(0, EvidenceCompanyId2, "Witholding.Business.ID2")
    Call auditor_data.SetField(0, EvidenceCompanyName, "Witholding.Business.Name")

    If EvidenceEnabled Then
      Call auditor_data.SetField(0, "1", "Witholding.Enabled")
    Else
      Call auditor_data.SetField(0, "0", "Witholding.Enabled")
    End If

    'LTC 22-JUN-2016
    If GeneratedWitholdingDocument > 0 Then
      Call auditor_data.SetField(0, GeneratedWitholdingDocument.ToString(), "Witholding.GenerateDocument")
    Else
      Call auditor_data.SetField(0, "0", "Witholding.GenerateDocument")
    End If

    Call auditor_data.SetField(0, GUI_FormatNumber(EvidencePrizeGreaterThan, 2), "Witholding.OnPrizeGreaterThan")
    Call auditor_data.SetField(0, EvidenceEditableMinutes, "Witholding.EditableMinutes")

    ' 497 Motivo del cambio
    auditor_data.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(497), Me.Comments)

    ' Concepto de la tarjeta.
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6524), Me.CardPlayerConcept)
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6523), Me.CardPlayerAmountCompanyB)

    'CFDI
    field_name = SetAuditoryLiteralCFDI(7767)
    Call auditor_data.SetField(0, Me.CFDI_Active, field_name)

    field_name = SetAuditoryLiteralCFDI(2697)
    Call auditor_data.SetField(0, Me.CFDI_Password, field_name)

    field_name = SetAuditoryLiteralCFDI(7652)
    Call auditor_data.SetField(0, Me.CFDI_Retries, field_name)

    field_name = SetAuditoryLiteralCFDI(7651)
    Call auditor_data.SetField(0, Me.CFDI_SchedulingLoad, field_name)

    field_name = SetAuditoryLiteralCFDI(7655)
    Call auditor_data.SetField(0, Me.CFDI_SchedulingProcess, field_name)

    field_name = SetAuditoryLiteralCFDI(7653)
    Call auditor_data.SetField(0, Me.CFDI_Site, field_name)

    field_name = SetAuditoryLiteralCFDI(7650)
    Call auditor_data.SetField(0, Me.CFDI_Url, field_name)

    field_name = SetAuditoryLiteralCFDI(2696)
    Call auditor_data.SetField(0, Me.CFDI_User, field_name)

    field_name = SetAuditoryLiteralCFDI(7768)
    Call auditor_data.SetField(0, Me.CFDI_RFC_Anonymous, field_name)

    field_name = SetAuditoryLiteralCFDI(7769)
    Call auditor_data.SetField(0, Me.CFDI_EF_Anonymous, field_name)

    field_name = SetAuditoryLiteralCFDI(7770)
    Call auditor_data.SetField(0, Me.CFDI_RFC_Foreign, field_name)

    field_name = SetAuditoryLiteralCFDI(7771)
    Call auditor_data.SetField(0, Me.CFDI_EF_Foreign, field_name)

    If (PlayerLevelIcons.Count > 0) Then
      field_name = GLB_NLS_GUI_PLAYER_TRACKING.Id(8563)
      Call auditor_data.SetField(0, Me.PlayerLevelIcons.Item(0).Level, field_name)
    End If

    If (PlayerLevelIcons.Count > 1) Then
      field_name = GLB_NLS_GUI_PLAYER_TRACKING.Id(8564)
      Call auditor_data.SetField(0, Me.PlayerLevelIcons.Item(1).Level, field_name)
    End If

    If (PlayerLevelIcons.Count > 2) Then
      field_name = GLB_NLS_GUI_PLAYER_TRACKING.Id(8565)
      Call auditor_data.SetField(0, Me.PlayerLevelIcons.Item(2).Level, field_name)
    End If

    If (PlayerLevelIcons.Count > 3) Then
      field_name = GLB_NLS_GUI_PLAYER_TRACKING.Id(8566)
      Call auditor_data.SetField(0, Me.PlayerLevelIcons.Item(3).Level, field_name)
    End If

    Return auditor_data

  End Function ' AuditorData

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
  End Function ' DB_Delete

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
  End Function ' DB_Insert

  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim rc As Integer

    ' Read Jackpot parameters information
    rc = CashierConf_DbRead(SqlCtx)

    Select Case rc
      Case ENUM_DB_RC.DB_RC_OK
        Return ENUM_STATUS.STATUS_OK

      Case ENUM_DB_RC.DB_RC_ERROR_NOT_FOUND
        Return CLASS_BASE.ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Read

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim rc As Integer

    If GLB_GuiMode = public_globals.ENUM_GUI.MULTISITE_GUI Then
      rc = CashierConf_DbUpdateMultiSite()
    Else
      rc = CashierConf_DbUpdate()
    End If

    Select Case rc
      Case ENUM_DB_RC.DB_RC_OK
        Return ENUM_STATUS.STATUS_OK

      Case ENUM_DB_RC.DB_RC_ERROR_NOT_FOUND
        Return CLASS_BASE.ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Update

  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE
    Dim temp_cashier_config As CLASS_CASHIER_CONFIGURATION
    Dim idx As Integer

    temp_cashier_config = New CLASS_CASHIER_CONFIGURATION
    'global
    temp_cashier_config.m_cashier_configuration.cashier_type = Me.m_cashier_configuration.cashier_type

    ' WCP
    temp_cashier_config.m_wcp_cashier_configuration.card_price = Me.m_wcp_cashier_configuration.card_price
    temp_cashier_config.m_wcp_cashier_configuration.personal_card_price = Me.m_wcp_cashier_configuration.personal_card_price
    temp_cashier_config.m_wcp_cashier_configuration.personal_card_replacement_price = Me.m_wcp_cashier_configuration.personal_card_replacement_price
    temp_cashier_config.m_wcp_cashier_configuration.personal_card_replacement_price_in_points = Me.m_wcp_cashier_configuration.personal_card_replacement_price_in_points
    temp_cashier_config.m_wcp_cashier_configuration.personal_card_replacement_charge_after_times = Me.m_wcp_cashier_configuration.personal_card_replacement_charge_after_times
    temp_cashier_config.m_wcp_cashier_configuration.card_refundable = Me.m_wcp_cashier_configuration.card_refundable
    temp_cashier_config.m_wcp_cashier_configuration.cashier_mode = Me.m_wcp_cashier_configuration.cashier_mode
    temp_cashier_config.m_wcp_cashier_configuration.language = Me.m_wcp_cashier_configuration.language
    temp_cashier_config.m_wcp_cashier_configuration.tax_1_pct = Me.m_wcp_cashier_configuration.tax_1_pct
    temp_cashier_config.m_wcp_cashier_configuration.tax_2_pct = Me.m_wcp_cashier_configuration.tax_2_pct
    temp_cashier_config.m_wcp_cashier_configuration.tax_3_pct = Me.m_wcp_cashier_configuration.tax_3_pct
    temp_cashier_config.m_wcp_cashier_configuration.tax_promo_1_pct = Me.m_wcp_cashier_configuration.tax_promo_1_pct
    temp_cashier_config.m_wcp_cashier_configuration.tax_promo_2_pct = Me.m_wcp_cashier_configuration.tax_promo_2_pct
    temp_cashier_config.m_wcp_cashier_configuration.tax_promo_3_pct = Me.m_wcp_cashier_configuration.tax_promo_3_pct
    temp_cashier_config.m_wcp_cashier_configuration.enable_promo_taxes = Me.m_wcp_cashier_configuration.enable_promo_taxes
    temp_cashier_config.m_wcp_cashier_configuration.min_time_close_session = Me.m_wcp_cashier_configuration.min_time_close_session
    temp_cashier_config.m_wcp_cashier_configuration.voucher_header = Me.m_wcp_cashier_configuration.voucher_header
    temp_cashier_config.m_wcp_cashier_configuration.voucher_footer = Me.m_wcp_cashier_configuration.voucher_footer
    temp_cashier_config.m_wcp_cashier_configuration.topx = Me.m_wcp_cashier_configuration.topx
    temp_cashier_config.m_wcp_cashier_configuration.topy = Me.m_wcp_cashier_configuration.topy
    temp_cashier_config.m_wcp_cashier_configuration.bottomx = Me.m_wcp_cashier_configuration.bottomx
    temp_cashier_config.m_wcp_cashier_configuration.bottomy = Me.m_wcp_cashier_configuration.bottomy
    temp_cashier_config.m_wcp_cashier_configuration.font_size = Me.m_wcp_cashier_configuration.font_size
    temp_cashier_config.m_wcp_cashier_configuration.printer_name = Me.m_wcp_cashier_configuration.printer_name
    temp_cashier_config.m_wcp_cashier_configuration.font_bold = Me.m_wcp_cashier_configuration.font_bold
    temp_cashier_config.m_wcp_cashier_configuration.allow_anon = Me.m_wcp_cashier_configuration.allow_anon
    temp_cashier_config.m_wcp_cashier_configuration.handpay_time_period = Me.m_wcp_cashier_configuration.handpay_time_period
    temp_cashier_config.m_wcp_cashier_configuration.handpay_time_to_cancel = Me.m_wcp_cashier_configuration.handpay_time_to_cancel
    ' PGJ 26-JUN-2013: Account customization voucher
    temp_cashier_config.m_wcp_cashier_configuration.voucher_footer_account_customize = Me.m_wcp_cashier_configuration.voucher_footer_account_customize

    ' Alesis
    temp_cashier_config.m_alesis_cashier_configuration.database_ip = Me.m_alesis_cashier_configuration.database_ip
    temp_cashier_config.m_alesis_cashier_configuration.database_name = Me.m_alesis_cashier_configuration.database_name
    temp_cashier_config.m_alesis_cashier_configuration.vendor_id = Me.m_alesis_cashier_configuration.vendor_id
    temp_cashier_config.m_alesis_cashier_configuration.username = Me.m_alesis_cashier_configuration.username
    temp_cashier_config.m_alesis_cashier_configuration.password = Me.m_alesis_cashier_configuration.password

    For idx = 0 To Me.m_alesis_cashier_configuration.configured_terminals.Count - 1
      Dim temp_terminal As New CLASS_CASHIER_CONFIGURATION.TYPE_TERMINAL

      temp_terminal.id = Me.m_alesis_cashier_configuration.configured_terminals(idx).id
      temp_terminal.name = Me.m_alesis_cashier_configuration.configured_terminals(idx).name
      temp_terminal.external_id = Me.m_alesis_cashier_configuration.configured_terminals(idx).external_id
      temp_terminal.machine_id = Me.m_alesis_cashier_configuration.configured_terminals(idx).machine_id
      temp_terminal.Added = False
      temp_terminal.Deleted = False
      temp_terminal.Updated = False
      temp_terminal.status = Me.m_alesis_cashier_configuration.configured_terminals(idx).status
      temp_cashier_config.m_alesis_cashier_configuration.configured_terminals.Add(temp_terminal)
    Next

    ' TJG 17-SEP-2010 Player Tracking

    temp_cashier_config.m_player_tracking_configuration.is_multisite_member = Me.m_player_tracking_configuration.is_multisite_member
    temp_cashier_config.m_player_tracking_configuration.points_expiration = Me.m_player_tracking_configuration.points_expiration
    temp_cashier_config.m_player_tracking_configuration.points_expiration_day_month = Me.m_player_tracking_configuration.points_expiration_day_month
    temp_cashier_config.m_player_tracking_configuration.gift_expiration = Me.m_player_tracking_configuration.gift_expiration
    temp_cashier_config.m_player_tracking_configuration.levels_days_counting_points = Me.m_player_tracking_configuration.levels_days_counting_points
    temp_cashier_config.m_player_tracking_configuration.levels_expiration_day_month = Me.m_player_tracking_configuration.levels_expiration_day_month

    temp_cashier_config.m_player_tracking_configuration.max_points = Me.m_player_tracking_configuration.max_points
    temp_cashier_config.m_player_tracking_configuration.max_points_per_minute = Me.m_player_tracking_configuration.max_points_per_minute
    temp_cashier_config.m_player_tracking_configuration.has_zero_plays = Me.m_player_tracking_configuration.has_zero_plays
    temp_cashier_config.m_player_tracking_configuration.has_mismatch = Me.m_player_tracking_configuration.has_mismatch

    For idx = 0 To Me.m_player_tracking_configuration.num_levels - 1
      temp_cashier_config.m_player_tracking_configuration.level(idx).name = Me.m_player_tracking_configuration.level(idx).name
      temp_cashier_config.m_player_tracking_configuration.level(idx).factor_redeemable_played_to_1_point = Me.m_player_tracking_configuration.level(idx).factor_redeemable_played_to_1_point
      temp_cashier_config.m_player_tracking_configuration.level(idx).factor_total_played_to_1_point = Me.m_player_tracking_configuration.level(idx).factor_total_played_to_1_point
      temp_cashier_config.m_player_tracking_configuration.level(idx).factor_redeemable_spent_to_1_point = Me.m_player_tracking_configuration.level(idx).factor_redeemable_spent_to_1_point
      temp_cashier_config.m_player_tracking_configuration.level(idx).points_to_enter = Me.m_player_tracking_configuration.level(idx).points_to_enter
      temp_cashier_config.m_player_tracking_configuration.level(idx).days_on_level = Me.m_player_tracking_configuration.level(idx).days_on_level
      temp_cashier_config.m_player_tracking_configuration.level(idx).points_to_keep = Me.m_player_tracking_configuration.level(idx).points_to_keep
      temp_cashier_config.m_player_tracking_configuration.level(idx).max_days_no_activity = Me.m_player_tracking_configuration.level(idx).max_days_no_activity
      temp_cashier_config.m_player_tracking_configuration.level(idx).upgrade_downgrade_action = Me.m_player_tracking_configuration.level(idx).upgrade_downgrade_action
    Next

    If GLB_GuiMode = public_globals.ENUM_GUI.WIGOS_GUI Then
      ' RCI 20-JAN-2012
      temp_cashier_config.m_player_tracking_configuration.points_multiplier_per_provider = Me.m_player_tracking_configuration.points_multiplier_per_provider.Copy()
    End If

    ' MBF 28-SEP-2010
    temp_cashier_config.m_taxes_business_a_configuration.business_name = Me.m_taxes_business_a_configuration.business_name
    temp_cashier_config.m_taxes_business_a_configuration.concept = Me.m_taxes_business_a_configuration.concept
    temp_cashier_config.m_taxes_business_a_configuration.cashin_pct = Me.m_taxes_business_a_configuration.cashin_pct
    temp_cashier_config.m_taxes_business_a_configuration.tax_name = Me.m_taxes_business_a_configuration.tax_name
    temp_cashier_config.m_taxes_business_a_configuration.tax_pct = Me.m_taxes_business_a_configuration.tax_pct
    temp_cashier_config.m_taxes_business_a_configuration.voucher_header = Me.m_taxes_business_a_configuration.voucher_header
    temp_cashier_config.m_taxes_business_a_configuration.voucher_footer = Me.m_taxes_business_a_configuration.voucher_footer
    temp_cashier_config.m_taxes_business_a_configuration.voucher_footer_cashin = Me.m_taxes_business_a_configuration.voucher_footer_cashin
    temp_cashier_config.m_taxes_business_a_configuration.voucher_footer_devolution = Me.m_taxes_business_a_configuration.voucher_footer_devolution
    temp_cashier_config.m_taxes_business_a_configuration.voucher_footer_cancel = Me.m_taxes_business_a_configuration.voucher_footer_cancel
    temp_cashier_config.m_taxes_business_a_configuration.prize_coupon = Me.m_taxes_business_a_configuration.prize_coupon
    temp_cashier_config.m_taxes_business_a_configuration.hide_promo = Me.m_taxes_business_a_configuration.hide_promo
    temp_cashier_config.m_taxes_business_a_configuration.text_promotion = Me.m_taxes_business_a_configuration.text_promotion
    temp_cashier_config.m_taxes_business_a_configuration.prize_coupon_text = Me.m_taxes_business_a_configuration.prize_coupon_text
    temp_cashier_config.m_taxes_business_a_configuration.hide_cashin = Me.m_taxes_business_a_configuration.hide_cashin
    ' JAB 21-MAR-2012 Add "Promo and CouponTogether" field
    temp_cashier_config.m_taxes_business_a_configuration.promo_and_coupon_together = Me.m_taxes_business_a_configuration.promo_and_coupon_together
    temp_cashier_config.m_taxes_business_a_configuration.text_promo_and_coupon_together = Me.m_taxes_business_a_configuration.text_promo_and_coupon_together
    temp_cashier_config.m_taxes_business_a_configuration.pay_title = Me.m_taxes_business_a_configuration.pay_title
    temp_cashier_config.m_taxes_business_a_configuration.dev_title = Me.m_taxes_business_a_configuration.dev_title

    temp_cashier_config.m_taxes_business_b_configuration.business_name = Me.m_taxes_business_b_configuration.business_name
    temp_cashier_config.m_taxes_business_b_configuration.concept = Me.m_taxes_business_b_configuration.concept
    temp_cashier_config.m_taxes_business_b_configuration.cashin_pct = Me.m_taxes_business_b_configuration.cashin_pct
    temp_cashier_config.m_taxes_business_b_configuration.tax_name = Me.m_taxes_business_b_configuration.tax_name
    temp_cashier_config.m_taxes_business_b_configuration.tax_pct = Me.m_taxes_business_b_configuration.tax_pct
    temp_cashier_config.m_taxes_business_b_configuration.voucher_header = Me.m_taxes_business_b_configuration.voucher_header
    temp_cashier_config.m_taxes_business_b_configuration.voucher_footer = Me.m_taxes_business_b_configuration.voucher_footer
    temp_cashier_config.m_taxes_business_b_configuration.voucher_footer_cashin = Me.m_taxes_business_b_configuration.voucher_footer_cashin
    temp_cashier_config.m_taxes_business_b_configuration.voucher_footer_devolution = Me.m_taxes_business_b_configuration.voucher_footer_devolution
    temp_cashier_config.m_taxes_business_b_configuration.voucher_footer_cancel = Me.m_taxes_business_b_configuration.voucher_footer_cancel
    temp_cashier_config.m_taxes_business_b_configuration.hide_cashin = Me.m_taxes_business_b_configuration.hide_cashin
    temp_cashier_config.m_taxes_business_b_configuration.pay_title = Me.m_taxes_business_b_configuration.pay_title
    temp_cashier_config.m_taxes_business_b_configuration.dev_title = Me.m_taxes_business_b_configuration.dev_title

    temp_cashier_config.m_taxes_business_b_configuration.enabled = Me.m_taxes_business_b_configuration.enabled

    temp_cashier_config.m_wcp_cashier_configuration.tax_1_name = Me.m_wcp_cashier_configuration.tax_1_name
    temp_cashier_config.m_wcp_cashier_configuration.tax_2_name = Me.m_wcp_cashier_configuration.tax_2_name
    temp_cashier_config.m_wcp_cashier_configuration.tax_3_name = Me.m_wcp_cashier_configuration.tax_3_name
    temp_cashier_config.m_wcp_cashier_configuration.tax_1_apply_if = Me.m_wcp_cashier_configuration.tax_1_apply_if
    temp_cashier_config.m_wcp_cashier_configuration.tax_2_apply_if = Me.m_wcp_cashier_configuration.tax_2_apply_if
    temp_cashier_config.m_wcp_cashier_configuration.tax_3_apply_if = Me.m_wcp_cashier_configuration.tax_3_apply_if
    temp_cashier_config.m_wcp_cashier_configuration.tax_promo_1_apply_if = Me.m_wcp_cashier_configuration.tax_promo_1_apply_if
    temp_cashier_config.m_wcp_cashier_configuration.tax_promo_2_apply_if = Me.m_wcp_cashier_configuration.tax_promo_2_apply_if
    temp_cashier_config.m_wcp_cashier_configuration.tax_promo_3_apply_if = Me.m_wcp_cashier_configuration.tax_promo_3_apply_if

    temp_cashier_config.m_evidence_configuration.representative_id1 = Me.m_evidence_configuration.representative_id1
    temp_cashier_config.m_evidence_configuration.representative_id2 = Me.m_evidence_configuration.representative_id2
    temp_cashier_config.m_evidence_configuration.representative_name = Me.m_evidence_configuration.representative_name
    temp_cashier_config.m_evidence_configuration.representative_sign = Me.m_evidence_configuration.representative_sign
    temp_cashier_config.m_evidence_configuration.company_id1 = Me.m_evidence_configuration.company_id1
    temp_cashier_config.m_evidence_configuration.company_id2 = Me.m_evidence_configuration.company_id2
    temp_cashier_config.m_evidence_configuration.company_name = Me.m_evidence_configuration.company_name
    temp_cashier_config.m_evidence_configuration.enabled = Me.m_evidence_configuration.enabled
    temp_cashier_config.m_evidence_configuration.editable_minutes = Me.m_evidence_configuration.editable_minutes
    temp_cashier_config.m_evidence_configuration.prize_greater_than = Me.m_evidence_configuration.prize_greater_than

    temp_cashier_config.m_comments = Me.m_comments
    temp_cashier_config.m_card_player_concept = Me.m_card_player_concept
    temp_cashier_config.m_card_player_amount_company_b = Me.m_card_player_amount_company_b

    temp_cashier_config.GeneratedWitholdingDocument = Me.m_evidence_configuration.generated_witholding_document

    'CFDI
    temp_cashier_config.m_cfdi_cashier_configuration.active = Me.m_cfdi_cashier_configuration.active
    temp_cashier_config.m_cfdi_cashier_configuration.password = Me.m_cfdi_cashier_configuration.password
    temp_cashier_config.m_cfdi_cashier_configuration.retries = Me.m_cfdi_cashier_configuration.retries
    temp_cashier_config.m_cfdi_cashier_configuration.scheduling_load = Me.m_cfdi_cashier_configuration.scheduling_load
    temp_cashier_config.m_cfdi_cashier_configuration.scheduling_process = Me.m_cfdi_cashier_configuration.scheduling_process
    temp_cashier_config.m_cfdi_cashier_configuration.site = Me.m_cfdi_cashier_configuration.site
    temp_cashier_config.m_cfdi_cashier_configuration.url = Me.m_cfdi_cashier_configuration.url
    temp_cashier_config.m_cfdi_cashier_configuration.user = Me.m_cfdi_cashier_configuration.user
    temp_cashier_config.m_cfdi_cashier_configuration.EFAnonymous = Me.m_cfdi_cashier_configuration.EFAnonymous
    temp_cashier_config.m_cfdi_cashier_configuration.RFCAnonymous = Me.m_cfdi_cashier_configuration.RFCAnonymous
    temp_cashier_config.m_cfdi_cashier_configuration.EFForeign = Me.m_cfdi_cashier_configuration.EFForeign
    temp_cashier_config.m_cfdi_cashier_configuration.RFCForeign = Me.m_cfdi_cashier_configuration.RFCForeign

    temp_cashier_config.m_player_level_icons = Me.m_player_level_icons.Clone()

    Return temp_cashier_config
  End Function ' Duplicate

#End Region ' Overrides

#Region "Private Functions"

  Private Function CashierConf_DbRead(ByVal Context As Integer) As Integer

    Dim _general_params As WSI.Common.GeneralParam.Dictionary

    Try
      _general_params = WSI.Common.GeneralParam.Dictionary.Create()

      m_cashier_configuration.cashier_type = GetCashlessServerId()

      Dim str_sql As String
      Dim data_table_alesis_parameters As DataTable
      Dim data_table_terminals As DataTable
      Dim data_table_points_mult As DataTable
      Dim idx As Integer
      Dim _concept_name As String
      Dim _slpit_b_enabled As Integer
      Dim _amount_to_b_enabled As Integer

      data_table_points_mult = Nothing

      If GLB_GuiMode = public_globals.ENUM_GUI.WIGOS_GUI Then

        str_sql = "SELECT  AP_SQL_SERVER_IP_ADDRESS " _
                          & ", AP_SQL_DATABASE_NAME " _
                          & ", AP_VENDOR_ID " _
                          & ", AP_SQL_USER " _
                          & ", AP_SQL_USER_PASSWORD " _
                          & " FROM  ALESIS_PARAMETERS "

        data_table_alesis_parameters = GUI_GetTableUsingSQL(str_sql, 1)

        str_sql = " SELECT T.TE_TERMINAL_ID, T.TE_NAME, T.TE_EXTERNAL_ID, AT.AT_MACHINE_ID,  AT.AT_STATUS " & _
                  " FROM TERMINALS AS T LEFT OUTER JOIN " & _
                  " ALESIS_TERMINALS AS AT ON T.TE_TERMINAL_ID = AT.AT_TERMINAL_ID " & _
                  " WHERE T.TE_TYPE = 1 "

        data_table_terminals = GUI_GetTableUsingSQL(str_sql, 5000)

        ' RCI 20-JAN-2012
        str_sql = "SELECT   PV_ID " & _
                  "       , PV_NAME " & _
                  "       , PV_POINTS_MULTIPLIER " & _
                  "  FROM   PROVIDERS " & _
                  " WHERE   PV_HIDE = 0 " & _
                  " ORDER   BY PV_NAME "

        data_table_points_mult = GUI_GetTableUsingSQL(str_sql, 5000)

        If Not IsDBNull(data_table_alesis_parameters.Rows(0).Item("AP_SQL_SERVER_IP_ADDRESS")) Then
          m_alesis_cashier_configuration.database_ip = data_table_alesis_parameters.Rows(0).Item("AP_SQL_SERVER_IP_ADDRESS")
        End If

        If Not IsDBNull(data_table_alesis_parameters.Rows(0).Item("AP_SQL_DATABASE_NAME")) Then
          m_alesis_cashier_configuration.database_name = data_table_alesis_parameters.Rows(0).Item("AP_SQL_DATABASE_NAME")
        End If

        If Not IsDBNull(data_table_alesis_parameters.Rows(0).Item("AP_VENDOR_ID")) Then
          m_alesis_cashier_configuration.vendor_id = data_table_alesis_parameters.Rows(0).Item("AP_VENDOR_ID")
        End If

        If Not IsDBNull(data_table_alesis_parameters.Rows(0).Item("AP_SQL_USER")) Then
          m_alesis_cashier_configuration.username = data_table_alesis_parameters.Rows(0).Item("AP_SQL_USER")
        End If

        If Not IsDBNull(data_table_alesis_parameters.Rows(0).Item("AP_SQL_USER_PASSWORD")) Then
          m_alesis_cashier_configuration.password = data_table_alesis_parameters.Rows(0).Item("AP_SQL_USER_PASSWORD")
        End If

        For idx = 0 To data_table_terminals.Rows.Count() - 1
          Dim terminal As New TYPE_TERMINAL

          If Not IsDBNull(data_table_terminals.Rows(idx).Item("TE_NAME")) Then
            terminal.name = data_table_terminals.Rows(idx).Item("TE_NAME")
          End If

          terminal.id = data_table_terminals.Rows(idx).Item("TE_TERMINAL_ID")

          If Not IsDBNull(data_table_terminals.Rows(idx).Item("TE_EXTERNAL_ID")) Then
            terminal.external_id = data_table_terminals.Rows(idx).Item("TE_EXTERNAL_ID")
          End If

          If Not IsDBNull(data_table_terminals.Rows(idx).Item("AT_MACHINE_ID")) Then
            terminal.machine_id = data_table_terminals.Rows(idx).Item("AT_MACHINE_ID")
            m_alesis_cashier_configuration.configured_terminals.Add(terminal)
          Else
            terminal.machine_id = 0
            m_alesis_cashier_configuration.configured_terminals.Add(terminal)
          End If

          If Not IsDBNull(data_table_terminals.Rows(idx).Item("AT_STATUS")) Then
            terminal.status = data_table_terminals.Rows(idx).Item("AT_STATUS")
          End If

        Next

        m_wcp_cashier_configuration.card_price = GetCashierData("CardPrice")
        m_wcp_cashier_configuration.personal_card_price = GetCashierData("PersonalCardPrice")
        m_wcp_cashier_configuration.personal_card_replacement_price = GetCashierData("PersonalCardReplacementPrice")
        'm_wcp_cashier_configuration.personal_card_replacement_price_in_points = GetCashierData("PersonalCardReplacement.PriceInPoints")
        m_wcp_cashier_configuration.personal_card_replacement_charge_after_times = GetCashierData("PersonalCardReplacement.ChargeAfterTimes")
        m_wcp_cashier_configuration.card_refundable = GetCashierData("CardRefundable")
        m_wcp_cashier_configuration.cashier_mode = GetCashierData("CashierMode")
        m_wcp_cashier_configuration.language = GetCashierData("Language")
        m_wcp_cashier_configuration.min_time_close_session = GetCashierData("MinTimeToCloseSession")
        m_wcp_cashier_configuration.voucher_header = GetCashierVoucherData("Header")
        m_wcp_cashier_configuration.voucher_footer = GetCashierVoucherData("Footer")
        m_wcp_cashier_configuration.topx = GetCashierCardPrintData("PrintNameArea_TopX")
        m_wcp_cashier_configuration.topy = GetCashierCardPrintData("PrintNameArea_TopY")
        m_wcp_cashier_configuration.bottomx = GetCashierCardPrintData("PrintNameArea_BottomX")
        m_wcp_cashier_configuration.bottomy = GetCashierCardPrintData("PrintNameArea_BottomY")
        m_wcp_cashier_configuration.font_size = GetCashierCardPrintData("PrintNameArea_FontSize")
        m_wcp_cashier_configuration.printer_name = GetCashierCardPrintData("QueueName")
        m_wcp_cashier_configuration.font_bold = GetCashierCardPrintData("PrintNameArea_Bold")
        m_wcp_cashier_configuration.allow_anon = GetCashierData("AllowedAnonymous")
        m_wcp_cashier_configuration.handpay_time_period = GetCashierHandPaysData("TimePeriod")
        m_wcp_cashier_configuration.handpay_time_to_cancel = GetCashierHandPaysData("TimeToCancel")
        ' PGJ 26-JUN-2013: Account customization voucher
        m_wcp_cashier_configuration.voucher_footer_account_customize = GetCashierVoucherData("AccountCustomize.Footer")

        m_cfdi_cashier_configuration.active = ReadGeneralParam("CFDI", "Active")
        m_cfdi_cashier_configuration.password = ReadGeneralParam("CFDI", "Password")
        m_cfdi_cashier_configuration.retries = _general_params.GetInt32("CFDI", "Retries")
        m_cfdi_cashier_configuration.scheduling_load = _general_params.GetInt32("CFDI", "Scheduling.Load")
        m_cfdi_cashier_configuration.scheduling_process = _general_params.GetInt32("CFDI", "Scheduling.Process")
        m_cfdi_cashier_configuration.site = ReadGeneralParam("CFDI", "Site")
        m_cfdi_cashier_configuration.url = ReadGeneralParam("CFDI", "Uri")
        m_cfdi_cashier_configuration.user = ReadGeneralParam("CFDI", "User")
        m_cfdi_cashier_configuration.RFCAnonymous = ReadGeneralParam("CFDI", "Anonymous.RFC")
        m_cfdi_cashier_configuration.EFAnonymous = ReadGeneralParam("CFDI", "Anonymous.EntidadFederativa")
        m_cfdi_cashier_configuration.RFCForeign = ReadGeneralParam("CFDI", "Foreign.RFC")
        m_cfdi_cashier_configuration.EFForeign = ReadGeneralParam("CFDI", "Foreign.EntidadFederativa")

      End If

      ' TJG 17-SEP-2010 Player Tracking

      m_player_tracking_configuration.is_multisite_member = GeneralParam.GetBoolean("Site", "MultiSiteMember", False)
      m_player_tracking_configuration.points_expiration = 0 'GetCashierPlayerTrackingData("PointsExpirationDays")
      m_player_tracking_configuration.points_expiration_day_month = GetCashierPlayerTrackingData("PointsExpirationDayMonth")
      m_player_tracking_configuration.gift_expiration = GetCashierPlayerTrackingData("GiftExpirationDays")
      m_player_tracking_configuration.levels_expiration_day_month = GetCashierPlayerTrackingData("Levels.ExpirationDayMonth")
      m_player_tracking_configuration.levels_days_counting_points = GetCashierPlayerTrackingData("Levels.DaysCountingPoints")
      m_player_tracking_configuration.level(0).name = GetCashierPlayerTrackingData("Level01.Name")
      m_player_tracking_configuration.level(1).name = GetCashierPlayerTrackingData("Level02.Name")
      m_player_tracking_configuration.level(2).name = GetCashierPlayerTrackingData("Level03.Name")
      m_player_tracking_configuration.level(3).name = GetCashierPlayerTrackingData("Level04.Name")
      m_player_tracking_configuration.level(0).factor_redeemable_played_to_1_point = GetCashierPlayerTrackingData("Level01.RedeemablePlayedTo1Point")
      m_player_tracking_configuration.level(1).factor_redeemable_played_to_1_point = GetCashierPlayerTrackingData("Level02.RedeemablePlayedTo1Point")
      m_player_tracking_configuration.level(2).factor_redeemable_played_to_1_point = GetCashierPlayerTrackingData("Level03.RedeemablePlayedTo1Point")
      m_player_tracking_configuration.level(3).factor_redeemable_played_to_1_point = GetCashierPlayerTrackingData("Level04.RedeemablePlayedTo1Point")
      m_player_tracking_configuration.level(0).factor_total_played_to_1_point = GetCashierPlayerTrackingData("Level01.TotalPlayedTo1Point")
      m_player_tracking_configuration.level(1).factor_total_played_to_1_point = GetCashierPlayerTrackingData("Level02.TotalPlayedTo1Point")
      m_player_tracking_configuration.level(2).factor_total_played_to_1_point = GetCashierPlayerTrackingData("Level03.TotalPlayedTo1Point")
      m_player_tracking_configuration.level(3).factor_total_played_to_1_point = GetCashierPlayerTrackingData("Level04.TotalPlayedTo1Point")
      m_player_tracking_configuration.level(0).factor_redeemable_spent_to_1_point = GetCashierPlayerTrackingData("Level01.RedeemableSpentTo1Point")
      m_player_tracking_configuration.level(1).factor_redeemable_spent_to_1_point = GetCashierPlayerTrackingData("Level02.RedeemableSpentTo1Point")
      m_player_tracking_configuration.level(2).factor_redeemable_spent_to_1_point = GetCashierPlayerTrackingData("Level03.RedeemableSpentTo1Point")
      m_player_tracking_configuration.level(3).factor_redeemable_spent_to_1_point = GetCashierPlayerTrackingData("Level04.RedeemableSpentTo1Point")
      m_player_tracking_configuration.level(1).points_to_enter = GetCashierPlayerTrackingData("Level02.PointsToEnter")
      m_player_tracking_configuration.level(2).points_to_enter = GetCashierPlayerTrackingData("Level03.PointsToEnter")
      m_player_tracking_configuration.level(3).points_to_enter = GetCashierPlayerTrackingData("Level04.PointsToEnter")
      m_player_tracking_configuration.level(1).days_on_level = GetCashierPlayerTrackingData("Level02.DaysOnLevel")
      m_player_tracking_configuration.level(2).days_on_level = GetCashierPlayerTrackingData("Level03.DaysOnLevel")
      m_player_tracking_configuration.level(3).days_on_level = GetCashierPlayerTrackingData("Level04.DaysOnLevel")

      m_player_tracking_configuration.level(1).points_to_keep = GetCashierPlayerTrackingData("Level02.PointsToKeep")
      If (String.IsNullOrEmpty(m_player_tracking_configuration.level(1).points_to_keep)) Then
        m_player_tracking_configuration.level(1).points_to_keep = "0"
      End If
      m_player_tracking_configuration.level(2).points_to_keep = GetCashierPlayerTrackingData("Level03.PointsToKeep")
      If (String.IsNullOrEmpty(m_player_tracking_configuration.level(2).points_to_keep)) Then
        m_player_tracking_configuration.level(2).points_to_keep = "0"
      End If
      m_player_tracking_configuration.level(3).points_to_keep = GetCashierPlayerTrackingData("Level04.PointsToKeep")
      If (String.IsNullOrEmpty(m_player_tracking_configuration.level(3).points_to_keep)) Then
        m_player_tracking_configuration.level(3).points_to_keep = "0"
      End If

      m_player_tracking_configuration.level(1).max_days_no_activity = GetCashierPlayerTrackingData("Level02.MaxDaysNoActivity")
      m_player_tracking_configuration.level(2).max_days_no_activity = GetCashierPlayerTrackingData("Level03.MaxDaysNoActivity")
      m_player_tracking_configuration.level(3).max_days_no_activity = GetCashierPlayerTrackingData("Level04.MaxDaysNoActivity")

      m_player_tracking_configuration.level(0).upgrade_downgrade_action = GetCashierPlayerTrackingData("Level01.UpgradeDowngradeAction")
      m_player_tracking_configuration.level(1).upgrade_downgrade_action = GetCashierPlayerTrackingData("Level02.UpgradeDowngradeAction")
      m_player_tracking_configuration.level(2).upgrade_downgrade_action = GetCashierPlayerTrackingData("Level03.UpgradeDowngradeAction")
      m_player_tracking_configuration.level(3).upgrade_downgrade_action = GetCashierPlayerTrackingData("Level04.UpgradeDowngradeAction")

      m_player_tracking_configuration.max_points = GetCashierPlayerTrackingDoNotAwardPointsData("PlaySession.PointsAreGreaterThan", "0")
      m_player_tracking_configuration.max_points_per_minute = GetCashierPlayerTrackingDoNotAwardPointsData("PlaySession.PointsPerMinuteAreGreaterThan", "0")
      m_player_tracking_configuration.has_zero_plays = GetCashierPlayerTrackingDoNotAwardPointsData("PlaySession.HasNoPlays", "0")
      m_player_tracking_configuration.has_mismatch = GetCashierPlayerTrackingDoNotAwardPointsData("PlaySession.HasBalanceMismatch", "0")

      If GLB_GuiMode = public_globals.ENUM_GUI.WIGOS_GUI Then
        m_player_tracking_configuration.points_multiplier_per_provider = data_table_points_mult

        ' MBF 28-SEP-2010
        m_taxes_business_a_configuration.business_name = GetCashierData("Split.A.CompanyName")
        m_taxes_business_a_configuration.concept = GetCashierData("Split.A.Name")
        m_taxes_business_a_configuration.cashin_pct = GetCashierData("Split.A.CashInPct")
        m_taxes_business_a_configuration.tax_name = GetCashierData("Split.A.Tax.Name")
        m_taxes_business_a_configuration.tax_pct = GetCashierData("Split.A.Tax.Pct")
        m_taxes_business_a_configuration.hide_cashin = GetCashierData("Split.A.HideTotal")
        m_taxes_business_a_configuration.voucher_header = GetCashierVoucherData("Voucher.A.Header")
        m_taxes_business_a_configuration.voucher_footer = GetCashierVoucherData("Voucher.A.Footer")
        m_taxes_business_a_configuration.voucher_footer_cashin = GetCashierVoucherData("Voucher.A.Footer.CashIn")
        m_taxes_business_a_configuration.voucher_footer_devolution = GetCashierVoucherData("Voucher.A.Footer.Dev")
        m_taxes_business_a_configuration.voucher_footer_cancel = GetCashierVoucherData("Voucher.A.Footer.Cancel")
        m_taxes_business_a_configuration.prize_coupon = GetCashierData("SplitAsWon")
        m_taxes_business_a_configuration.hide_promo = GetCashierData("HidePromotion")
        m_taxes_business_a_configuration.text_promotion = GetCashierData("Promotion.Text")
        ' JAB 21-MAR-2012 Add "Promo and CouponTogether" field
        m_taxes_business_a_configuration.promo_and_coupon_together = GetCashierData("Promo&CouponTogether")
        m_taxes_business_a_configuration.text_promo_and_coupon_together = GetCashierData("Promo&CouponTogether.Text")
        m_taxes_business_a_configuration.pay_title = GetCashierData("Split.A.CashInVoucherTitle")
        m_taxes_business_a_configuration.dev_title = GetCashierData("Split.A.CashOutVoucherTitle")

        ' MPO 21-DIC-2011
        m_taxes_business_a_configuration.prize_coupon_text = GetCashierData("SplitAsWon.Text")

        m_taxes_business_b_configuration.business_name = GetCashierData("Split.B.CompanyName")
        m_taxes_business_b_configuration.concept = GetCashierData("Split.B.Name")
        m_taxes_business_b_configuration.cashin_pct = GetCashierData("Split.B.CashInPct")
        m_taxes_business_b_configuration.tax_name = GetCashierData("Split.B.Tax.Name")
        m_taxes_business_b_configuration.tax_pct = GetCashierData("Split.B.Tax.Pct")
        m_taxes_business_b_configuration.hide_cashin = GetCashierData("Split.B.HideTotal")
        m_taxes_business_b_configuration.voucher_header = GetCashierVoucherData("Voucher.B.Header")
        m_taxes_business_b_configuration.voucher_footer = GetCashierVoucherData("Voucher.B.Footer")
        m_taxes_business_b_configuration.voucher_footer_cashin = GetCashierVoucherData("Voucher.B.Footer.CashIn")
        m_taxes_business_b_configuration.voucher_footer_devolution = GetCashierVoucherData("Voucher.B.Footer.Dev")
        m_taxes_business_b_configuration.voucher_footer_cancel = GetCashierVoucherData("Voucher.B.Footer.Cancel")
        m_taxes_business_b_configuration.pay_title = GetCashierData("Split.B.CashInVoucherTitle")
        m_taxes_business_b_configuration.dev_title = GetCashierData("Split.B.CashOutVoucherTitle")

        m_taxes_business_b_configuration.enabled = GetCashierData("Split.B.Enabled")

        m_wcp_cashier_configuration.tax_1_name = GetCashierData("Tax.OnPrize.1.Name")
        m_wcp_cashier_configuration.tax_2_name = GetCashierData("Tax.OnPrize.2.Name")
        m_wcp_cashier_configuration.tax_3_name = GetCashierData("Tax.OnPrize.3.Name")
        m_wcp_cashier_configuration.tax_1_pct = GetCashierData("Tax.OnPrize.1.Pct")
        m_wcp_cashier_configuration.tax_2_pct = GetCashierData("Tax.OnPrize.2.Pct")
        m_wcp_cashier_configuration.tax_3_pct = GetCashierData("Tax.OnPrize.3.Pct")
        m_wcp_cashier_configuration.tax_1_apply_if = GetCashierData("Tax.OnPrize.1.Threshold")
        m_wcp_cashier_configuration.tax_2_apply_if = GetCashierData("Tax.OnPrize.2.Threshold")
        m_wcp_cashier_configuration.tax_3_apply_if = GetCashierData("Tax.OnPrize.3.Threshold")
        m_wcp_cashier_configuration.tax_promo_1_pct = GetCashierData("Promotions.RE.Tax.1.Pct")
        m_wcp_cashier_configuration.tax_promo_2_pct = GetCashierData("Promotions.RE.Tax.2.Pct")
        m_wcp_cashier_configuration.tax_promo_3_pct = GetCashierData("Promotions.RE.Tax.3.Pct")
        m_wcp_cashier_configuration.tax_promo_1_apply_if = GetCashierData("Promotions.RE.Tax.1.Threshold")
        m_wcp_cashier_configuration.tax_promo_2_apply_if = GetCashierData("Promotions.RE.Tax.2.Threshold")
        m_wcp_cashier_configuration.tax_promo_3_apply_if = GetCashierData("Promotions.RE.Tax.3.Threshold")
        m_wcp_cashier_configuration.enable_promo_taxes = GetCashierData("Promotions.RE.Tax.Enabled")

        m_evidence_configuration.company_id1 = GetWitholdingData("Business.ID1")
        m_evidence_configuration.company_id2 = GetWitholdingData("Business.ID2")
        m_evidence_configuration.company_name = GetWitholdingData("Business.Name")
        m_evidence_configuration.representative_id1 = GetWitholdingData("Representative.ID1")
        m_evidence_configuration.representative_id2 = GetWitholdingData("Representative.ID2")
        m_evidence_configuration.representative_name = GetWitholdingData("Representative.Name")
        m_evidence_configuration.representative_sign = GetRepresentativeSign()
        m_evidence_configuration.editable_minutes = ConvertInt32(GetWitholdingData("EditableMinutes"))
        m_evidence_configuration.prize_greater_than = _general_params.GetDecimal("Witholding.Document", "OnPrizeGreaterThan")
        m_evidence_configuration.enabled = (GetWitholdingData("Enabled") = "1")
        m_evidence_configuration.generated_witholding_document = ConvertInt32(GetWitholdingData("GenerateDocument")) 'LTC 22-JUN-2016
      End If

      m_comments = ""

      _concept_name = ReadGeneralParam("Cashier.PlayerCard", "ConceptName")
      m_card_player_concept = IIf(_concept_name Is String.Empty, WSI.Common.Resource.String("STR_FORMAT_GENERAL_NAME_CARD"), _concept_name)

      Integer.TryParse(GetCashierData("Split.B.Enabled"), _slpit_b_enabled)
      Integer.TryParse(ReadGeneralParam("Cashier.PlayerCard", "AmountToCompanyB"), _amount_to_b_enabled)
      m_card_player_amount_company_b = _slpit_b_enabled And _amount_to_b_enabled

      'Get PlayerLevelIcon Level
      If (GeneralParam.GetBoolean("MultiSite", "IsCenter", False) = False) Then
        m_player_level_icons = New PlayerLevelIcon().GetAllPlayerLevelIcon()
      End If

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

    Catch _ex As Exception
      Return ENUM_STATUS.STATUS_ERROR

    End Try

  End Function ' CashierConf_DbRead

  Private Function CashierConf_DbUpdate() As Integer

    Dim str_sql As String
    Dim commit_trx As Boolean
    Dim sql_command As SqlCommand
    Dim rc As Integer
    Dim result As Integer
    Dim idx As Integer
    Dim _gp_update_list As New List(Of String())

    Try
      Using _db_trx As New DB_TRX()

        If Not GUI_BeginSQLTransaction(_db_trx.SqlTransaction) Then
          Exit Function
        End If

        commit_trx = False
        rc = ENUM_CONFIGURATION_RC.CONFIGURATION_OK

        Select Case m_cashier_configuration.cashier_type
          Case CashlessMode.WIN

            _gp_update_list.Add(GeneralParam.AddToList("Cashier", "CardPrice", m_wcp_cashier_configuration.card_price))
            _gp_update_list.Add(GeneralParam.AddToList("Cashier", "PersonalCardPrice", m_wcp_cashier_configuration.personal_card_price))
            _gp_update_list.Add(GeneralParam.AddToList("Cashier", "PersonalCardReplacementPrice", m_wcp_cashier_configuration.personal_card_replacement_price))
            _gp_update_list.Add(GeneralParam.AddToList("Cashier", "PersonalCardReplacement.ChargeAfterTimes", m_wcp_cashier_configuration.personal_card_replacement_charge_after_times))
            _gp_update_list.Add(GeneralParam.AddToList("Cashier", "CardRefundable", m_wcp_cashier_configuration.card_refundable))
            _gp_update_list.Add(GeneralParam.AddToList("Cashier", "CashierMode", m_wcp_cashier_configuration.cashier_mode))
            _gp_update_list.Add(GeneralParam.AddToList("Cashier", "Language", m_wcp_cashier_configuration.language))
            _gp_update_list.Add(GeneralParam.AddToList("Cashier", "MinTimeToCloseSession", m_wcp_cashier_configuration.min_time_close_session))
            _gp_update_list.Add(GeneralParam.AddToList("Cashier.Voucher", "Header", m_wcp_cashier_configuration.voucher_header))
            _gp_update_list.Add(GeneralParam.AddToList("Cashier.Voucher", "Footer", m_wcp_cashier_configuration.voucher_footer))
            _gp_update_list.Add(GeneralParam.AddToList("Cashier.CardPrint", "PrintNameArea_TopX", m_wcp_cashier_configuration.topx))
            _gp_update_list.Add(GeneralParam.AddToList("Cashier.CardPrint", "PrintNameArea_TopY", m_wcp_cashier_configuration.topy))
            _gp_update_list.Add(GeneralParam.AddToList("Cashier.CardPrint", "PrintNameArea_BottomX", m_wcp_cashier_configuration.bottomx))
            _gp_update_list.Add(GeneralParam.AddToList("Cashier.CardPrint", "PrintNameArea_BottomY", m_wcp_cashier_configuration.bottomy))
            _gp_update_list.Add(GeneralParam.AddToList("Cashier.CardPrint", "PrintNameArea_FontSize", m_wcp_cashier_configuration.font_size))
            _gp_update_list.Add(GeneralParam.AddToList("Cashier.CardPrint", "QueueName", m_wcp_cashier_configuration.printer_name))
            _gp_update_list.Add(GeneralParam.AddToList("Cashier.CardPrint", "PrintNameArea_Bold", m_wcp_cashier_configuration.font_bold))
            _gp_update_list.Add(GeneralParam.AddToList("Cashier", "AllowedAnonymous", m_wcp_cashier_configuration.allow_anon))
            _gp_update_list.Add(GeneralParam.AddToList("Cashier.HandPays", "TimePeriod", m_wcp_cashier_configuration.handpay_time_period))
            _gp_update_list.Add(GeneralParam.AddToList("Cashier.HandPays", "TimeToCancel", m_wcp_cashier_configuration.handpay_time_to_cancel))
            _gp_update_list.Add(GeneralParam.AddToList("Cashier.Voucher", "AccountCustomize.Footer", m_wcp_cashier_configuration.voucher_footer_account_customize))
            _gp_update_list.Add(GeneralParam.AddToList("Witholding.Document", "Business.ID1", m_evidence_configuration.company_id1))
            _gp_update_list.Add(GeneralParam.AddToList("Witholding.Document", "Business.ID2", m_evidence_configuration.company_id2))
            _gp_update_list.Add(GeneralParam.AddToList("Witholding.Document", "Business.Name", m_evidence_configuration.company_name))
            _gp_update_list.Add(GeneralParam.AddToList("Witholding.Document", "Representative.ID1", m_evidence_configuration.representative_id1))
            _gp_update_list.Add(GeneralParam.AddToList("Witholding.Document", "Representative.ID2", m_evidence_configuration.representative_id2))
            _gp_update_list.Add(GeneralParam.AddToList("Witholding.Document", "Representative.Name", m_evidence_configuration.representative_name))
            _gp_update_list.Add(GeneralParam.AddToList("Witholding.Document", "EditableMinutes", m_evidence_configuration.editable_minutes))
            _gp_update_list.Add(GeneralParam.AddToList("Witholding.Document", "OnPrizeGreaterThan", GUI_LocalNumberToDBNumber(m_evidence_configuration.prize_greater_than.ToString())))
            _gp_update_list.Add(GeneralParam.AddToList("Witholding.Document", "OnPrizeGreaterThan.1", GUI_LocalNumberToDBNumber(m_evidence_configuration.prize_greater_than.ToString())))
            _gp_update_list.Add(GeneralParam.AddToList("Witholding.Document", "OnPrizeGreaterThan.2", GUI_LocalNumberToDBNumber(m_evidence_configuration.prize_greater_than.ToString())))
            _gp_update_list.Add(GeneralParam.AddToList("Witholding.Document", "Enabled", IIf(m_evidence_configuration.enabled, 1, 0)))
            _gp_update_list.Add(GeneralParam.AddToList("Witholding.Document", "GenerateDocument", m_evidence_configuration.generated_witholding_document.ToString()))
            _gp_update_list.Add(GeneralParam.AddToList("Cashless", "Server", GLB_NLS_GUI_CONFIGURATION.GetString(220)))

            SetRepresentativeSign(m_evidence_configuration.representative_sign, _db_trx.SqlTransaction)

            For Each _general_param As String() In _gp_update_list
              If (Not GUI_Controls.DB_GeneralParam_Update(_general_param(GeneralParam.COLUMN_NAME.Group),
                                                          _general_param(GeneralParam.COLUMN_NAME.Subject),
                                                          _general_param(GeneralParam.COLUMN_NAME.Value),
                                                          _db_trx.SqlTransaction)) Then

                Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
              End If
            Next

          Case CashlessMode.ALESIS
            If (Not GUI_Controls.DB_GeneralParam_Update("Cashless", "Server", GLB_NLS_GUI_CONFIGURATION.GetString(221), _db_trx.SqlTransaction)) Then

              Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
            End If

            ' UPDATE ALESIS PARAMETERS
            str_sql = "UPDATE   ALESIS_PARAMETERS " & _
                      "    SET  AP_SQL_SERVER_IP_ADDRESS = @p1" & _
                      "      ,  AP_SQL_DATABASE_NAME = @p2" & _
                      "      ,  AP_VENDOR_ID = @p3" & _
                      "      ,  AP_SQL_USER = @p4" & _
                      "      ,  AP_SQL_USER_PASSWORD = @p5"


            sql_command = New SqlCommand(str_sql)
            sql_command.Connection = _db_trx.SqlTransaction.Connection
            sql_command.Transaction = _db_trx.SqlTransaction

            sql_command.Parameters.Add("@p1", SqlDbType.NVarChar, 50).Value = m_alesis_cashier_configuration.database_ip
            sql_command.Parameters.Add("@p2", SqlDbType.NVarChar, 50).Value = m_alesis_cashier_configuration.database_name
            sql_command.Parameters.Add("@p3", SqlDbType.NVarChar, 50).Value = m_alesis_cashier_configuration.vendor_id
            sql_command.Parameters.Add("@p4", SqlDbType.NVarChar, 50).Value = m_alesis_cashier_configuration.username
            sql_command.Parameters.Add("@p5", SqlDbType.NVarChar, 50).Value = m_alesis_cashier_configuration.password
            rc = sql_command.ExecuteNonQuery()

            Dim str_sql_upd As String
            Dim str_sql_ins As String
            Dim str_sql_del As String

            ' UPDATE CASHIER TERMINALS
            str_sql_upd = "UPDATE   ALESIS_TERMINALS " & _
                      "   SET   AT_TERMINAL_ID = @p1 " & _
                      " ,       AT_MACHINE_ID = @p2 " & _
                      " ,       AT_STATUS = @p3 " & _
                      " WHERE   AT_TERMINAL_ID = @p1 "

            ' Delete CASHIER TERMINALS
            str_sql_ins = "INSERT INTO ALESIS_TERMINALS ( AT_TERMINAL_ID, AT_MACHINE_ID, AT_STATUS )" & _
                          " VALUES ( @p1, @p2, @p3 )"


            ' Insert CASHIER TERMINALS
            str_sql_del = "DELETE FROM ALESIS_TERMINALS " & _
                          " WHERE AT_TERMINAL_ID =  @p1"

            For idx = 0 To m_alesis_cashier_configuration.configured_terminals.Count - 1
              If m_alesis_cashier_configuration.configured_terminals(idx).Added Then
                sql_command = New SqlCommand(str_sql_ins)
                sql_command.Connection = _db_trx.SqlTransaction.Connection
                sql_command.Transaction = _db_trx.SqlTransaction

                sql_command.Parameters.Add("@p1", SqlDbType.NVarChar, 20).Value = m_alesis_cashier_configuration.configured_terminals(idx).id
                sql_command.Parameters.Add("@p2", SqlDbType.Int).Value = m_alesis_cashier_configuration.configured_terminals(idx).machine_id
                sql_command.Parameters.Add("@p3", SqlDbType.Int).Value = ENUM_TERMINAL_STATUS.TERMINAL_STATUS_PENDING

                rc = sql_command.ExecuteNonQuery()
              End If

              If m_alesis_cashier_configuration.configured_terminals(idx).Updated Then
                sql_command = New SqlCommand(str_sql_upd)
                sql_command.Connection = _db_trx.SqlTransaction.Connection
                sql_command.Transaction = _db_trx.SqlTransaction

                sql_command.Parameters.Add("@p1", SqlDbType.NVarChar, 20).Value = m_alesis_cashier_configuration.configured_terminals(idx).id
                sql_command.Parameters.Add("@p2", SqlDbType.Int).Value = m_alesis_cashier_configuration.configured_terminals(idx).machine_id
                sql_command.Parameters.Add("@p3", SqlDbType.Int).Value = ENUM_TERMINAL_STATUS.TERMINAL_STATUS_PENDING

                rc = sql_command.ExecuteNonQuery()
              End If

              If m_alesis_cashier_configuration.configured_terminals(idx).Deleted Then
                sql_command = New SqlCommand(str_sql_del)
                sql_command.Connection = _db_trx.SqlTransaction.Connection
                sql_command.Transaction = _db_trx.SqlTransaction

                sql_command.Parameters.Add("@p1", SqlDbType.NVarChar, 20).Value = m_alesis_cashier_configuration.configured_terminals(idx).id

                rc = sql_command.ExecuteNonQuery()
              End If

            Next

            If rc <> 1 Then
              Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
            End If

        End Select

        If Not IsMultisiteMember Or IsMultisiteMember And CommonMultiSite.GetPlayerTrackingMode() = PlayerTracking_Mode.Site Then
          _gp_update_list.Clear()

          _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "PointsExpirationDays", m_player_tracking_configuration.points_expiration))
          _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "PointsExpirationDayMonth", m_player_tracking_configuration.points_expiration_day_month))
          _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "GiftExpirationDays", m_player_tracking_configuration.gift_expiration))
          _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Levels.ExpirationDayMonth", m_player_tracking_configuration.levels_expiration_day_month))
          _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Levels.DaysCountingPoints", m_player_tracking_configuration.levels_days_counting_points))
          _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level01.Name", m_player_tracking_configuration.level(0).name))
          _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level02.Name", m_player_tracking_configuration.level(1).name))
          _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level03.Name", m_player_tracking_configuration.level(2).name))
          _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level04.Name", m_player_tracking_configuration.level(3).name))
          _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level01.RedeemablePlayedTo1Point", m_player_tracking_configuration.level(0).factor_redeemable_played_to_1_point))
          _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level02.RedeemablePlayedTo1Point", m_player_tracking_configuration.level(1).factor_redeemable_played_to_1_point))
          _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level03.RedeemablePlayedTo1Point", m_player_tracking_configuration.level(2).factor_redeemable_played_to_1_point))
          _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level04.RedeemablePlayedTo1Point", m_player_tracking_configuration.level(3).factor_redeemable_played_to_1_point))
          _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level01.TotalPlayedTo1Point", m_player_tracking_configuration.level(0).factor_total_played_to_1_point))
          _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level02.TotalPlayedTo1Point", m_player_tracking_configuration.level(1).factor_total_played_to_1_point))
          _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level03.TotalPlayedTo1Point", m_player_tracking_configuration.level(2).factor_total_played_to_1_point))
          _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level04.TotalPlayedTo1Point", m_player_tracking_configuration.level(3).factor_total_played_to_1_point))
          _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level01.RedeemableSpentTo1Point", m_player_tracking_configuration.level(0).factor_redeemable_spent_to_1_point))
          _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level02.RedeemableSpentTo1Point", m_player_tracking_configuration.level(1).factor_redeemable_spent_to_1_point))
          _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level03.RedeemableSpentTo1Point", m_player_tracking_configuration.level(2).factor_redeemable_spent_to_1_point))
          _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level04.RedeemableSpentTo1Point", m_player_tracking_configuration.level(3).factor_redeemable_spent_to_1_point))
          _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level02.PointsToEnter", m_player_tracking_configuration.level(1).points_to_enter))
          _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level03.PointsToEnter", m_player_tracking_configuration.level(2).points_to_enter))
          _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level04.PointsToEnter", m_player_tracking_configuration.level(3).points_to_enter))
          _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level02.DaysOnLevel", m_player_tracking_configuration.level(1).days_on_level))
          _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level03.DaysOnLevel", m_player_tracking_configuration.level(2).days_on_level))
          _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level04.DaysOnLevel", m_player_tracking_configuration.level(3).days_on_level))
          _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level02.PointsToKeep", m_player_tracking_configuration.level(1).points_to_keep))
          _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level03.PointsToKeep", m_player_tracking_configuration.level(2).points_to_keep))
          _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level04.PointsToKeep", m_player_tracking_configuration.level(3).points_to_keep))
          _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level02.MaxDaysNoActivity", m_player_tracking_configuration.level(1).max_days_no_activity))
          _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level03.MaxDaysNoActivity", m_player_tracking_configuration.level(2).max_days_no_activity))
          _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level04.MaxDaysNoActivity", m_player_tracking_configuration.level(3).max_days_no_activity))
          _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level01.UpgradeDowngradeAction", m_player_tracking_configuration.level(0).upgrade_downgrade_action))
          _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level02.UpgradeDowngradeAction", m_player_tracking_configuration.level(1).upgrade_downgrade_action))
          _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level03.UpgradeDowngradeAction", m_player_tracking_configuration.level(2).upgrade_downgrade_action))
          _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level04.UpgradeDowngradeAction", m_player_tracking_configuration.level(3).upgrade_downgrade_action))
          _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking.DoNotAwardPoints", "PlaySession.PointsAreGreaterThan", m_player_tracking_configuration.max_points))
          _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking.DoNotAwardPoints", "PlaySession.PointsPerMinuteAreGreaterThan", m_player_tracking_configuration.max_points_per_minute))
          _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking.DoNotAwardPoints", "PlaySession.HasNoPlays", IIf(m_player_tracking_configuration.has_zero_plays, "1", "0")))
          _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking.DoNotAwardPoints", "PlaySession.HasBalanceMismatch", IIf(m_player_tracking_configuration.has_mismatch, "1", "0")))

          For Each _general_param As String() In _gp_update_list
            If (Not GUI_Controls.DB_GeneralParam_Update(_general_param(GeneralParam.COLUMN_NAME.Group), _general_param(GeneralParam.COLUMN_NAME.Subject), _general_param(GeneralParam.COLUMN_NAME.Value), _db_trx.SqlTransaction)) Then
              Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
            End If
          Next
        End If

        'MPO 20-JAN-2012

        ' Only update points multiplier in case of WigosGUI mode ( not in multisite master mode )
        Dim _num_modified As Integer
        Dim _num_updated As Integer

        _num_modified = m_player_tracking_configuration.points_multiplier_per_provider.Select("", "", _
                                                          DataViewRowState.ModifiedCurrent).Length
        If _num_modified > 0 Then
          Dim _sql_txt As System.Text.StringBuilder
          _sql_txt = New System.Text.StringBuilder()
          _sql_txt.AppendLine("UPDATE   PROVIDERS")
          _sql_txt.AppendLine("   SET   PV_POINTS_MULTIPLIER = @pPointsMultiplier")
          _sql_txt.AppendLine(" WHERE   PV_ID                = @pProviderId")

          Using _da As New SqlDataAdapter()
            Using _sql_cmd As New SqlCommand(_sql_txt.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
              _sql_cmd.Parameters.Add("@pPointsMultiplier", SqlDbType.Decimal).SourceColumn = "PV_POINTS_MULTIPLIER"
              _sql_cmd.Parameters.Add("@pProviderId", SqlDbType.Int).SourceColumn = "PV_ID"

              _da.UpdateCommand = _sql_cmd
              _num_updated = _da.Update(m_player_tracking_configuration.points_multiplier_per_provider)
            End Using
          End Using

          If _num_modified <> _num_updated Then
            Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
          End If
        End If

        _gp_update_list.Clear()
        ' MBF 28-SEP-2010
        _gp_update_list.Add(GeneralParam.AddToList("Cashier", "Split.A.CompanyName", m_taxes_business_a_configuration.business_name))
        _gp_update_list.Add(GeneralParam.AddToList("Cashier", "Split.A.Name", m_taxes_business_a_configuration.concept))
        _gp_update_list.Add(GeneralParam.AddToList("Cashier", "Split.A.CashInPct", m_taxes_business_a_configuration.cashin_pct))
        _gp_update_list.Add(GeneralParam.AddToList("Cashier", "Split.A.Tax.Name", m_taxes_business_a_configuration.tax_name))
        _gp_update_list.Add(GeneralParam.AddToList("Cashier", "Split.A.Tax.Pct", m_taxes_business_a_configuration.tax_pct))
        _gp_update_list.Add(GeneralParam.AddToList("Cashier", "Split.A.HideTotal", m_taxes_business_a_configuration.hide_cashin))
        _gp_update_list.Add(GeneralParam.AddToList("Cashier.Voucher", "Voucher.A.Header", m_taxes_business_a_configuration.voucher_header))
        _gp_update_list.Add(GeneralParam.AddToList("Cashier.Voucher", "Voucher.A.Footer", m_taxes_business_a_configuration.voucher_footer))
        _gp_update_list.Add(GeneralParam.AddToList("Cashier.Voucher", "Voucher.A.Footer.CashIn", m_taxes_business_a_configuration.voucher_footer_cashin))
        _gp_update_list.Add(GeneralParam.AddToList("Cashier.Voucher", "Voucher.A.Footer.Dev", m_taxes_business_a_configuration.voucher_footer_devolution))
        _gp_update_list.Add(GeneralParam.AddToList("Cashier.Voucher", "Voucher.A.Footer.Cancel", m_taxes_business_a_configuration.voucher_footer_cancel))
        _gp_update_list.Add(GeneralParam.AddToList("Cashier", "SplitAsWon", m_taxes_business_a_configuration.prize_coupon))
        _gp_update_list.Add(GeneralParam.AddToList("Cashier", "SplitAsWon.Text", m_taxes_business_a_configuration.prize_coupon_text))
        _gp_update_list.Add(GeneralParam.AddToList("Cashier", "HidePromotion", m_taxes_business_a_configuration.hide_promo))
        _gp_update_list.Add(GeneralParam.AddToList("Cashier", "Promotion.Text", m_taxes_business_a_configuration.text_promotion))
        ' JAB 21-MAR-2012 Add "Promo and CouponTogether" field
        _gp_update_list.Add(GeneralParam.AddToList("Cashier", "Promo&CouponTogether", m_taxes_business_a_configuration.promo_and_coupon_together))
        _gp_update_list.Add(GeneralParam.AddToList("Cashier", "Promo&CouponTogether.Text", m_taxes_business_a_configuration.text_promo_and_coupon_together))
        _gp_update_list.Add(GeneralParam.AddToList("Cashier", "Split.A.CashInVoucherTitle", m_taxes_business_a_configuration.pay_title))
        _gp_update_list.Add(GeneralParam.AddToList("Cashier", "Split.A.CashOutVoucherTitle", m_taxes_business_a_configuration.dev_title))

        _gp_update_list.Add(GeneralParam.AddToList("Cashier", "Split.B.CompanyName", m_taxes_business_b_configuration.business_name))
        _gp_update_list.Add(GeneralParam.AddToList("Cashier", "Split.B.Name", m_taxes_business_b_configuration.concept))
        _gp_update_list.Add(GeneralParam.AddToList("Cashier", "Split.B.CashInPct", m_taxes_business_b_configuration.cashin_pct))
        _gp_update_list.Add(GeneralParam.AddToList("Cashier", "Split.B.Tax.Name", m_taxes_business_b_configuration.tax_name))
        _gp_update_list.Add(GeneralParam.AddToList("Cashier", "Split.B.Tax.Pct", m_taxes_business_b_configuration.tax_pct))
        _gp_update_list.Add(GeneralParam.AddToList("Cashier", "Split.B.HideTotal", m_taxes_business_b_configuration.hide_cashin))
        _gp_update_list.Add(GeneralParam.AddToList("Cashier.Voucher", "Voucher.B.Header", m_taxes_business_b_configuration.voucher_header))
        _gp_update_list.Add(GeneralParam.AddToList("Cashier.Voucher", "Voucher.B.Footer", m_taxes_business_b_configuration.voucher_footer))
        _gp_update_list.Add(GeneralParam.AddToList("Cashier.Voucher", "Voucher.B.Footer.CashIn", m_taxes_business_b_configuration.voucher_footer_cashin))
        _gp_update_list.Add(GeneralParam.AddToList("Cashier.Voucher", "Voucher.B.Footer.Dev", m_taxes_business_b_configuration.voucher_footer_devolution))
        _gp_update_list.Add(GeneralParam.AddToList("Cashier.Voucher", "Voucher.B.Footer.Cancel", m_taxes_business_b_configuration.voucher_footer_cancel))
        _gp_update_list.Add(GeneralParam.AddToList("Cashier", "Split.B.Enabled", m_taxes_business_b_configuration.enabled))
        _gp_update_list.Add(GeneralParam.AddToList("Cashier", "Split.B.CashInVoucherTitle", m_taxes_business_b_configuration.pay_title))
        _gp_update_list.Add(GeneralParam.AddToList("Cashier", "Split.B.CashOutVoucherTitle", m_taxes_business_b_configuration.dev_title))

        _gp_update_list.Add(GeneralParam.AddToList("Cashier", "Tax.OnPrize.1.Name", m_wcp_cashier_configuration.tax_1_name))
        _gp_update_list.Add(GeneralParam.AddToList("Cashier", "Tax.OnPrize.2.Name", m_wcp_cashier_configuration.tax_2_name))
        _gp_update_list.Add(GeneralParam.AddToList("Cashier", "Tax.OnPrize.3.Name", m_wcp_cashier_configuration.tax_3_name))
        _gp_update_list.Add(GeneralParam.AddToList("Cashier", "Tax.OnPrize.1.Pct", m_wcp_cashier_configuration.tax_1_pct))
        _gp_update_list.Add(GeneralParam.AddToList("Cashier", "Tax.OnPrize.2.Pct", m_wcp_cashier_configuration.tax_2_pct))
        _gp_update_list.Add(GeneralParam.AddToList("Cashier", "Tax.OnPrize.3.Pct", m_wcp_cashier_configuration.tax_3_pct))
        _gp_update_list.Add(GeneralParam.AddToList("Cashier", "Tax.OnPrize.1.Threshold", m_wcp_cashier_configuration.tax_1_apply_if))
        _gp_update_list.Add(GeneralParam.AddToList("Cashier", "Tax.OnPrize.2.Threshold", m_wcp_cashier_configuration.tax_2_apply_if))
        _gp_update_list.Add(GeneralParam.AddToList("Cashier", "Tax.OnPrize.3.Threshold", m_wcp_cashier_configuration.tax_3_apply_if))
        _gp_update_list.Add(GeneralParam.AddToList("Cashier", "Promotions.RE.Tax.1.Pct", m_wcp_cashier_configuration.tax_promo_1_pct))
        _gp_update_list.Add(GeneralParam.AddToList("Cashier", "Promotions.RE.Tax.2.Pct", m_wcp_cashier_configuration.tax_promo_2_pct))
        _gp_update_list.Add(GeneralParam.AddToList("Cashier", "Promotions.RE.Tax.3.Pct", m_wcp_cashier_configuration.tax_promo_3_pct))
        _gp_update_list.Add(GeneralParam.AddToList("Cashier", "Promotions.RE.Tax.1.Threshold", m_wcp_cashier_configuration.tax_promo_1_apply_if))
        _gp_update_list.Add(GeneralParam.AddToList("Cashier", "Promotions.RE.Tax.2.Threshold", m_wcp_cashier_configuration.tax_promo_2_apply_if))
        _gp_update_list.Add(GeneralParam.AddToList("Cashier", "Promotions.RE.Tax.3.Threshold", m_wcp_cashier_configuration.tax_promo_3_apply_if))
        _gp_update_list.Add(GeneralParam.AddToList("Cashier", "Promotions.RE.Tax.Enabled", m_wcp_cashier_configuration.enable_promo_taxes))

        _gp_update_list.Add(GeneralParam.AddToList("Cashier.PlayerCard", "ConceptName", m_card_player_concept))
        _gp_update_list.Add(GeneralParam.AddToList("Cashier.PlayerCard", "AmountToCompanyB", IIf(m_card_player_amount_company_b, 1, 0)))

        _gp_update_list.Add(GeneralParam.AddToList("CFDI", "Active", m_cfdi_cashier_configuration.active))
        _gp_update_list.Add(GeneralParam.AddToList("CFDI", "Password", m_cfdi_cashier_configuration.password))
        _gp_update_list.Add(GeneralParam.AddToList("CFDI", "Retries", m_cfdi_cashier_configuration.retries))
        _gp_update_list.Add(GeneralParam.AddToList("CFDI", "Scheduling.Load", m_cfdi_cashier_configuration.scheduling_load))
        _gp_update_list.Add(GeneralParam.AddToList("CFDI", "Scheduling.Process", m_cfdi_cashier_configuration.scheduling_process))
        _gp_update_list.Add(GeneralParam.AddToList("CFDI", "Site", m_cfdi_cashier_configuration.site))
        _gp_update_list.Add(GeneralParam.AddToList("CFDI", "Uri", m_cfdi_cashier_configuration.url))
        _gp_update_list.Add(GeneralParam.AddToList("CFDI", "User", m_cfdi_cashier_configuration.user))
        _gp_update_list.Add(GeneralParam.AddToList("CFDI", "Anonymous.RFC", m_cfdi_cashier_configuration.RFCAnonymous))
        _gp_update_list.Add(GeneralParam.AddToList("CFDI", "Anonymous.EntidadFederativa", m_cfdi_cashier_configuration.EFAnonymous))
        _gp_update_list.Add(GeneralParam.AddToList("CFDI", "Foreign.RFC", m_cfdi_cashier_configuration.RFCForeign))
        _gp_update_list.Add(GeneralParam.AddToList("CFDI", "Foreign.EntidadFederativa", m_cfdi_cashier_configuration.EFForeign))

        For Each _general_param As String() In _gp_update_list
          If (Not GUI_Controls.DB_GeneralParam_Update(_general_param(GeneralParam.COLUMN_NAME.Group),
                                                      _general_param(GeneralParam.COLUMN_NAME.Subject),
                                                      _general_param(GeneralParam.COLUMN_NAME.Value),
                                                      _db_trx.SqlTransaction)) Then

            Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
          End If
        Next

        Me.PlayerLevelIcons.Save(_db_trx.SqlTransaction)

        _db_trx.Commit()

      End Using

    Catch ex As Exception
      ' Do nothing
      Log.Error("Exception on CashierConf_DbUpdate(): " & ex.Message)
      result = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

    Return result

  End Function ' CashierConf_DbUpdate

  Private Function CashierConf_DbUpdateMultiSite() As Integer

    Using _db_trx As DB_TRX = New DB_TRX()
      Try
        Dim _gp_update_list As New List(Of String())

        _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "PointsExpirationDays", m_player_tracking_configuration.points_expiration))
        _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "PointsExpirationDayMonth", m_player_tracking_configuration.points_expiration_day_month))
        _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "GiftExpirationDays", m_player_tracking_configuration.gift_expiration))
        _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Levels.ExpirationDayMonth", m_player_tracking_configuration.levels_expiration_day_month))
        _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Levels.DaysCountingPoints", m_player_tracking_configuration.levels_days_counting_points))
        _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level01.Name", m_player_tracking_configuration.level(0).name))
        _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level02.Name", m_player_tracking_configuration.level(1).name))
        _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level03.Name", m_player_tracking_configuration.level(2).name))
        _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level04.Name", m_player_tracking_configuration.level(3).name))
        _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level01.RedeemablePlayedTo1Point", m_player_tracking_configuration.level(0).factor_redeemable_played_to_1_point))
        _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level02.RedeemablePlayedTo1Point", m_player_tracking_configuration.level(1).factor_redeemable_played_to_1_point))
        _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level03.RedeemablePlayedTo1Point", m_player_tracking_configuration.level(2).factor_redeemable_played_to_1_point))
        _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level04.RedeemablePlayedTo1Point", m_player_tracking_configuration.level(3).factor_redeemable_played_to_1_point))
        _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level01.TotalPlayedTo1Point", m_player_tracking_configuration.level(0).factor_total_played_to_1_point))
        _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level02.TotalPlayedTo1Point", m_player_tracking_configuration.level(1).factor_total_played_to_1_point))
        _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level03.TotalPlayedTo1Point", m_player_tracking_configuration.level(2).factor_total_played_to_1_point))
        _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level04.TotalPlayedTo1Point", m_player_tracking_configuration.level(3).factor_total_played_to_1_point))
        _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level01.RedeemableSpentTo1Point", m_player_tracking_configuration.level(0).factor_redeemable_spent_to_1_point))
        _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level02.RedeemableSpentTo1Point", m_player_tracking_configuration.level(1).factor_redeemable_spent_to_1_point))
        _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level03.RedeemableSpentTo1Point", m_player_tracking_configuration.level(2).factor_redeemable_spent_to_1_point))
        _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level04.RedeemableSpentTo1Point", m_player_tracking_configuration.level(3).factor_redeemable_spent_to_1_point))
        _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level02.PointsToEnter", m_player_tracking_configuration.level(1).points_to_enter))
        _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level03.PointsToEnter", m_player_tracking_configuration.level(2).points_to_enter))
        _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level04.PointsToEnter", m_player_tracking_configuration.level(3).points_to_enter))
        _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level02.DaysOnLevel", m_player_tracking_configuration.level(1).days_on_level))
        _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level03.DaysOnLevel", m_player_tracking_configuration.level(2).days_on_level))
        _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level04.DaysOnLevel", m_player_tracking_configuration.level(3).days_on_level))
        _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level02.PointsToKeep", m_player_tracking_configuration.level(1).points_to_keep))
        _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level03.PointsToKeep", m_player_tracking_configuration.level(2).points_to_keep))
        _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level04.PointsToKeep", m_player_tracking_configuration.level(3).points_to_keep))
        _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level02.MaxDaysNoActivity", m_player_tracking_configuration.level(1).max_days_no_activity))
        _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level03.MaxDaysNoActivity", m_player_tracking_configuration.level(2).max_days_no_activity))
        _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level04.MaxDaysNoActivity", m_player_tracking_configuration.level(3).max_days_no_activity))
        _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level01.UpgradeDowngradeAction", m_player_tracking_configuration.level(0).upgrade_downgrade_action))
        _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level02.UpgradeDowngradeAction", m_player_tracking_configuration.level(1).upgrade_downgrade_action))
        _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level03.UpgradeDowngradeAction", m_player_tracking_configuration.level(2).upgrade_downgrade_action))
        _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking", "Level04.UpgradeDowngradeAction", m_player_tracking_configuration.level(3).upgrade_downgrade_action))
        _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking.DoNotAwardPoints", "PlaySession.PointsAreGreaterThan", m_player_tracking_configuration.max_points))
        _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking.DoNotAwardPoints", "PlaySession.PointsPerMinuteAreGreaterThan", m_player_tracking_configuration.max_points_per_minute))
        _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking.DoNotAwardPoints", "PlaySession.HasNoPlays", IIf(m_player_tracking_configuration.has_zero_plays, "1", "0")))
        _gp_update_list.Add(GeneralParam.AddToList("PlayerTracking.DoNotAwardPoints", "PlaySession.HasBalanceMismatch", IIf(m_player_tracking_configuration.has_mismatch, "1", "0")))

        For Each _general_param As String() In _gp_update_list
          If (Not GUI_Controls.DB_GeneralParam_Update(_general_param(GeneralParam.COLUMN_NAME.Group),
                                                      _general_param(GeneralParam.COLUMN_NAME.Subject),
                                                      _general_param(GeneralParam.COLUMN_NAME.Value),
                                                      _db_trx.SqlTransaction)) Then
            Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
          End If
        Next
      Catch ex As Exception
        Log.Error("Exception on CashierConf_DbUpdateMultiSite(): " & ex.Message)

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End Try

      _db_trx.SqlTransaction.Commit()

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
    End Using

  End Function ' CashierConf_DbUpdateMultiSite

  Private Function ConvertInt32(ByVal obj As Object) As Int32
    Dim _result As Int32

    If Int32.TryParse(obj.ToString, _result) Then
      Return _result
    Else
      Return 0
    End If

  End Function

  Private Function ConvertDecimal(ByVal obj As Object) As Decimal
    Dim _result As Decimal

    If Decimal.TryParse(obj.ToString, _result) Then
      Return _result
    Else
      Return 0
    End If

  End Function

  Private Function GetRepresentativeSign() As Image
    Dim _doc_img As WSI.Common.DocumentList
    Dim _image As Image

    _doc_img = Nothing
    _image = Nothing

    Using _db_trx As WSI.Common.DB_TRX = New WSI.Common.DB_TRX()
      WSI.Common.TableDocuments.Load(WSI.Common.DOCUMENT_TYPE.SIGNATURE, _doc_img, _db_trx.SqlTransaction)
    End Using

    Try
      If Not IsNothing(_doc_img) AndAlso _doc_img.Count > 0 Then
        _image = Image.FromStream(New IO.MemoryStream(_doc_img.Item(0).Content))
      End If
    Catch ex As Exception

    End Try

    Return _image

  End Function

  Private Sub SetRepresentativeSign(ByVal Image As Image, ByVal SqlTrans As System.Data.SqlClient.SqlTransaction)
    Dim _doc_img As WSI.Common.DocumentList
    Dim _read_only_doc As WSI.Common.ReadOnlyDocument
    Dim _mm As IO.MemoryStream

    If IsNothing(Image) Then
      Return
    End If

    _doc_img = New WSI.Common.DocumentList
    _mm = New IO.MemoryStream()

    Image.Save(_mm, Image.RawFormat)
    _read_only_doc = New WSI.Common.ReadOnlyDocument("", _mm.GetBuffer())
    _doc_img.Add(_read_only_doc)

    WSI.Common.TableDocuments.Save(WSI.Common.DOCUMENT_TYPE.SIGNATURE, _doc_img, SqlTrans)

  End Sub

  Private Function GetRepresentativeValue(ByVal Value As String) As String

    If Value Then
      Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(698) ' "Si"
    Else
      Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(699) ' "No"
    End If

  End Function ' GetRepresentativeValue

  Private Function GetCFDICountersByStatus(FromDate As DateTime, ToDate As DateTime) As TYPE_CFDI_REGISTERS_STATUS
    Dim _str_sql As System.Text.StringBuilder
    Dim _data_table As DataTable
    Dim _cfdi_status_total As TYPE_CFDI_REGISTERS_STATUS

    _str_sql = New System.Text.StringBuilder()

    _str_sql.AppendLine("SELECT    CR_STATUS, COUNT(*) AS TOTAL")
    _str_sql.AppendLine("  FROM    CFDI_REGISTERS")
    _str_sql.AppendLine(" WHERE    CR_UPDATED >= " + GUI_FormatDateDB(FromDate))
    _str_sql.AppendLine("   AND    CR_UPDATED < " + GUI_FormatDateDB(ToDate))
    _str_sql.AppendLine("GROUP BY  CR_STATUS")

    _data_table = GUI_GetTableUsingSQL(_str_sql.ToString(), 4)

    _cfdi_status_total = New TYPE_CFDI_REGISTERS_STATUS()

    For Each _row As DataRow In _data_table.Rows
      If _row.Item("CR_STATUS") = 0 Then 'CFDIStatus.Pending
        _cfdi_status_total.TotalPending = _row.Item("TOTAL")
      End If
      If _row.Item("CR_STATUS") = 1 Then 'CFDIStatus.Send
        _cfdi_status_total.TotalSend = _row.Item("TOTAL")
      End If
      If _row.Item("CR_STATUS") = 2 Then 'CFDIStatus.Retries
        _cfdi_status_total.TotalRetries = _row.Item("TOTAL")
      End If
      If _row.Item("CR_STATUS") = 3 Then 'CFDIStatus.Error
        _cfdi_status_total.TotalError = _row.Item("TOTAL")
      End If
    Next

    Return _cfdi_status_total
  End Function

#End Region

  Private Function SetAuditoryLiteralCFDI(NLSId As Integer) As String
    Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(7649) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(NLSId)
  End Function

End Class
