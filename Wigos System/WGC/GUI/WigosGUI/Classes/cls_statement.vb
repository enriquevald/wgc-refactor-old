'-------------------------------------------------------------------
' Copyright � 2015 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   CLASS_STATEMENT
' DESCRIPTION:   
' AUTHOR:        Samuel Gonz�lez
' CREATION DATE: 23-APR-2015
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 23-APR-2015  SGB    Initial version. Backlog Item 1129
'--------------------------------------------------------------------

Imports GUI_Controls
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports WSI.Common
Imports System.Data.SqlClient
Imports System.Text
Public Class CLASS_STATEMENT
  Inherits CLASS_BASE

#Region " Structure "
  Public Class CONF_STATEMENT

    Public fiscal_year_start As String
    Public enabled As Boolean
    Public paper_size As Int32
    Public paper_size_string As String
    Public title As String
    Public number_prev_year As Int32
    Public header As String
    Public footer As String
    Public logo As Image

  End Class

#End Region ' Structure

#Region " Members "

  Protected m_conf_statement As New CONF_STATEMENT
  Protected m_logo_old As String

#End Region ' Members

#Region " Properties "

  Public Property fiscal_year_start() As String
    Get
      Return m_conf_statement.fiscal_year_start
    End Get

    Set(ByVal Value As String)
      m_conf_statement.fiscal_year_start = Value
    End Set
  End Property

  Public Property enabled() As Boolean
    Get
      Return m_conf_statement.enabled
    End Get

    Set(ByVal Value As Boolean)
      m_conf_statement.enabled = Value
    End Set
  End Property

  Public Property paper_size() As Int32
    Get
      Return m_conf_statement.paper_size
    End Get

    Set(ByVal Value As Int32)
      m_conf_statement.paper_size = Value
    End Set
  End Property

  Public Property paper_size_string() As String
    Get
      Return m_conf_statement.paper_size_string
    End Get

    Set(ByVal Value As String)
      m_conf_statement.paper_size_string = Value
    End Set
  End Property

  Public Property title() As String
    Get
      Return m_conf_statement.title
    End Get

    Set(ByVal Value As String)
      m_conf_statement.title = Value
    End Set
  End Property

  Public Property number_prev_year() As Int32
    Get
      Return m_conf_statement.number_prev_year
    End Get

    Set(ByVal Value As Int32)
      m_conf_statement.number_prev_year = Value
    End Set
  End Property

  Public Property header() As String
    Get
      Return m_conf_statement.header
    End Get

    Set(ByVal Value As String)
      m_conf_statement.header = Value
    End Set
  End Property

  Public Property footer() As String
    Get
      Return m_conf_statement.footer
    End Get

    Set(ByVal Value As String)
      m_conf_statement.footer = Value
    End Set
  End Property

  Public Property logo() As Image
    Get
      Return m_conf_statement.logo
    End Get

    Set(ByVal Value As Image)
      m_conf_statement.logo = Value
    End Set
  End Property

#End Region ' Properties

#Region " Overrides "

  '----------------------------------------------------------------------------
  ' PURPOSE: Returns the related auditor data for the current object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA
    Dim _auditor As CLASS_AUDITOR_DATA
    Dim _yes_text As String
    Dim _no_text As String
    Dim _fiscal_year As String

    _yes_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(278)
    _no_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)

    _auditor = New CLASS_AUDITOR_DATA(AUDIT_NAME_CASHIER_CONFIGURATION)

    Call _auditor.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(6223), "")

    ' Fiscal year start
    _fiscal_year = m_conf_statement.fiscal_year_start.Substring(2, 2) & "-" _
                 & m_conf_statement.fiscal_year_start.Substring(0, 2) & " " _
                 & m_conf_statement.fiscal_year_start.Substring(4, 2) & ":00"
    _auditor.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6226), IIf(String.IsNullOrEmpty(m_conf_statement.fiscal_year_start), AUDIT_NONE_STRING, _fiscal_year))
    ' enabled
    _auditor.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6228), IIf(m_conf_statement.enabled, _yes_text, _no_text))
    ' Paper size
    _auditor.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6231), m_conf_statement.paper_size_string)
    ' Title
    _auditor.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6233), IIf(String.IsNullOrEmpty(m_conf_statement.title), AUDIT_NONE_STRING, m_conf_statement.title))
    ' number of previous year
    _auditor.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6232), IIf(m_conf_statement.number_prev_year < 0, AUDIT_NONE_STRING, GUI_FormatNumber(m_conf_statement.number_prev_year, 0)))
    ' header
    _auditor.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6234), IIf(String.IsNullOrEmpty(m_conf_statement.header), AUDIT_NONE_STRING, m_conf_statement.header.Replace("\n", " ")))
    ' footer
    _auditor.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6287), IIf(String.IsNullOrEmpty(m_conf_statement.footer), AUDIT_NONE_STRING, m_conf_statement.footer.Replace("\n", " ")))
    ' logo
    _auditor.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6242), GetInfoCRC(m_conf_statement.logo))
    Return _auditor
  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE: Deletes the given object from the database.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return ENUM_STATUS.STATUS_ERROR
  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE: Inserts the given object to the database.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return ENUM_STATUS.STATUS_ERROR
  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE: Reads from the database into the given object
  '
  ' PARAMS:
  '   - INPUT:
  '     - ObjectId: An object, it must be 'casted' to the desired KEY.
  '
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim rc As Integer

    rc = Conf_DbRead(SqlCtx)

    Select Case rc
      Case ENUM_DB_RC.DB_RC_OK
        Return ENUM_STATUS.STATUS_OK

      Case ENUM_DB_RC.DB_RC_ERROR_NOT_FOUND
        Return CLASS_BASE.ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select
  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE: Updates the given object to the database.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _rc As Integer

    ' Update pattern data
    _rc = Conf_DbUpdate(SqlCtx)

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
        Return ENUM_STATUS.STATUS_DUPLICATE_KEY

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select
  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE: Returns a duplicate of the given object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim _clone As CLASS_STATEMENT

    _clone = New CLASS_STATEMENT()

    _clone.fiscal_year_start = Me.fiscal_year_start
    _clone.enabled = Me.enabled
    _clone.paper_size = Me.paper_size
    _clone.paper_size_string = Me.paper_size_string
    _clone.title = Me.title
    _clone.number_prev_year = Me.number_prev_year
    _clone.header = Me.header
    _clone.footer = Me.footer
    _clone.logo = Me.logo

    Return _clone
  End Function
#End Region ' Overrides

#Region " Private "

  '----------------------------------------------------------------------------
  ' PURPOSE: It calls the Conf_Statement_DbRead and checks if the database result is ok
  '                                              
  ' PARAMS:
  '   - INPUT: 
  '   - OUTPUT:
  '
  ' RETURNS:
  '
  ' NOTES:
  Private Function Conf_DbRead(ByVal Context As Integer) As Integer

    Dim Item As CONF_STATEMENT = m_conf_statement
    Dim _general_param As GeneralParam.Dictionary

    _general_param = GeneralParam.Dictionary.Create()

    Try

      Item.fiscal_year_start = _general_param.GetString("WinLossStatement", "FiscalYearStart.MMDDHH", "010100")
      Item.enabled = _general_param.GetBoolean("WinLossStatement", "Enabled", False)
      Item.paper_size = _general_param.GetInt32("WinLossStatement", "PaperSize", PageSize.A4)
      Item.title = IIf(_general_param.GetString("WinLossStatement", "Title") = " ", "", _general_param.GetString("WinLossStatement", "Title"))
      Item.number_prev_year = _general_param.GetString("WinLossStatement", "NumberOfPreviousYear", 5)
      Item.header = IIf(_general_param.GetString("WinLossStatement", "Header") = " ", "", _general_param.GetString("WinLossStatement", "Header"))
      Item.footer = IIf(_general_param.GetString("WinLossStatement", "Footer") = " ", "", _general_param.GetString("WinLossStatement", "Footer"))
      Item.paper_size_string = PaperSizeEnumToString(Item.paper_size)


      Using _db_trx As New DB_TRX()
        Call LoadLogo(Item.logo, _db_trx.SqlTransaction)
        m_logo_old = GetInfoCRC(Item.logo)
      End Using

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

    Catch _ex As Exception
      Log.Exception(_ex)

      Return ENUM_STATUS.STATUS_ERROR
    End Try
  End Function ' Conf_DbRead

  Private Function PaperSizeEnumToString(ByVal enum_paper_size As Int32) As String

    Select Case enum_paper_size
      Case PageSize.A4
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(6236)
      Case PageSize.A5
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(6237)
      Case PageSize.Letter
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(6235)
      Case Else
        Return Nothing
    End Select
  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE : Updates the configuration statement object into the DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - Context
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - CONFIGURATION_OK
  '     - CONFIGURATION_ERROR_DB
  '
  ' NOTES :
  Private Function Conf_DbUpdate(ByVal Context As Integer) As Integer
    Dim _sql_tx As System.Data.SqlClient.SqlTransaction = Nothing

    Try
      '   - Update the safe keeping parameters
      Using _db_trx As New DB_TRX()
        If Not DB_GeneralParam_Update("WinLossStatement", "FiscalYearStart.MMDDHH", m_conf_statement.fiscal_year_start, _db_trx.SqlTransaction) Then
          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

        If Not DB_GeneralParam_Update("WinLossStatement", "Enabled", IIf(m_conf_statement.enabled, "1", "0"), _db_trx.SqlTransaction) Then
          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

        If Not DB_GeneralParam_Update("WinLossStatement", "PaperSize", m_conf_statement.paper_size, _db_trx.SqlTransaction) Then
          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

        If Not DB_GeneralParam_Update("WinLossStatement", "Title", IIf(String.IsNullOrEmpty(m_conf_statement.title), "", m_conf_statement.title), _db_trx.SqlTransaction) Then
          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

        If Not DB_GeneralParam_Update("WinLossStatement", "NumberOfPreviousYear", m_conf_statement.number_prev_year, _db_trx.SqlTransaction) Then
          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

        If Not DB_GeneralParam_Update("WinLossStatement", "Header", IIf(String.IsNullOrEmpty(m_conf_statement.header), "", m_conf_statement.header), _db_trx.SqlTransaction) Then
          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

        If Not DB_GeneralParam_Update("WinLossStatement", "Footer", IIf(String.IsNullOrEmpty(m_conf_statement.footer), "", m_conf_statement.footer), _db_trx.SqlTransaction) Then
          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

        If LogoChanged(m_logo_old, GetInfoCRC(m_conf_statement.logo)) Then
          If Not SaveLogo(m_conf_statement.logo, _db_trx.SqlTransaction) Then
            Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
          End If
        End If

        _db_trx.Commit()

      End Using

    Catch ex As Exception
      ' Rollback  
      GUI_EndSQLTransaction(_sql_tx, False)
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
  End Function ' Conf_DbUpdate

  '----------------------------------------------------------------------------
  ' PURPOSE : Check if Logo is changed
  '
  ' PARAMS :
  '     - INPUT :   
  '         - CRC of image old
  '         - CRC of image new
  '     - OUTPUT :  
  '
  ' RETURNS :
  '     - Boolean
  '
  ' NOTES :
  Private Function LogoChanged(ByVal CRCOld As String, ByVal CRCNew As String)
    Return CRCOld <> CRCNew
  End Function
#End Region ' Private

#Region " Publics "

  '----------------------------------------------------------------------------
  ' PURPOSE : Load logo image from documents table in database.
  '
  ' PARAMS :
  '     - INPUT :   Trx As SqlTransaction
  '     - OUTPUT :  ImageLogo = logo info as Image class
  '
  ' RETURNS :
  '     - Boolean
  '
  ' NOTES :
  Public Shared Function LoadLogo(ByRef ImageLogo As Image, ByVal Trx As SqlTransaction) As Boolean

    Dim _doc_img As WSI.Common.DocumentList

    _doc_img = Nothing

    Try

      WSI.Common.TableDocuments.Load(WSI.Common.DOCUMENT_TYPE.STATEMENT_LOGO, _doc_img, Trx)

      If IsNothing(_doc_img) Or _doc_img.Count = 0 Then
        Return False
      End If

      ImageLogo = Image.FromStream(New IO.MemoryStream(_doc_img.Item(0).Content))

      Return True

    Catch _ex As Exception
      ImageLogo = Nothing
      Return False

    End Try

  End Function   ' LoadLogo

  '----------------------------------------------------------------------------
  ' PURPOSE : Save logo image from documents table in database.
  '
  ' PARAMS :
  '     - INPUT :   ImageLogo = logo info as Image class
  '                 Trx As SqlTransaction
  '     - OUTPUT :  None
  '
  ' RETURNS :
  '     - Boolean
  '
  ' NOTES :
  Public Shared Function SaveLogo(ByVal Logo As Image, ByVal Trx As SqlTransaction) As Boolean

    Dim _doc_img As WSI.Common.DocumentList
    Dim _read_only_doc As WSI.Common.ReadOnlyDocument
    Dim _mm As IO.MemoryStream

    _doc_img = New WSI.Common.DocumentList
    _mm = New IO.MemoryStream()

    Try

      If Not IsNothing(Logo) Then
        Logo.Save(_mm, Logo.RawFormat)
      End If

      _read_only_doc = New WSI.Common.ReadOnlyDocument("", _mm.GetBuffer())
      _doc_img.Add(_read_only_doc)

      Return WSI.Common.TableDocuments.Save(WSI.Common.DOCUMENT_TYPE.STATEMENT_LOGO, _doc_img, Trx)

    Catch _ex As Exception
      Log.Error(_ex.Message)
    End Try

    Return False
  End Function   ' SaveLogo
#End Region ' Publics
End Class
