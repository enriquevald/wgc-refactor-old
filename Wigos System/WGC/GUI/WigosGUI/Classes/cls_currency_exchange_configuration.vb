'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : cls_currency_exchange_configuration.vb
'
' DESCRIPTION : Currency Exchange Configuration Data class
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 19-JUN-2013  JBP    Initial version   
' 05-JUl-2013  DLL    Change Select to recive currencies
' 24-JUL-2013  DHA    Added hidden symbol on SAS-HOST option
'--------------------------------------------------------------------
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports WSI.Common
Imports System.Text

Public Class CLS_CURRENCY_EXCHANGE_CONFIGURATION
  Inherits CLASS_BASE

#Region " Constants "

#End Region  ' Constants 

#Region " Structures "
  Public Class TYPE_CURRENCY_EXCHANGE_CONFIGURATION
    Public currencies As DataTable
    Public currency_ISO_Code_GP As String
    Public currencies_accepted_GP As String
    Public symbol As String
    Public hidden_gui As Boolean
    Public hidden_sas_host As Boolean

    Sub New()
      currencies = New DataTable()
      currency_ISO_Code_GP = String.Empty
      currencies_accepted_GP = String.Empty
      symbol = String.Empty
      hidden_gui = False
      hidden_sas_host = False
    End Sub
  End Class

#End Region ' Structures

#Region " Members "
  Protected m_currency_exchange_configuration As New TYPE_CURRENCY_EXCHANGE_CONFIGURATION
#End Region    ' Members

#Region "Properties"

  Public Property Currencies() As DataTable
    Get
      Return m_currency_exchange_configuration.currencies
    End Get
    Set(ByVal value As DataTable)
      m_currency_exchange_configuration.currencies = value
    End Set
  End Property

  Public Property Currencies_Accepted_GP() As String
    Get
      Return m_currency_exchange_configuration.currencies_accepted_GP
    End Get
    Set(ByVal Value As String)
      m_currency_exchange_configuration.currencies_accepted_GP = Value
    End Set
  End Property

  Public Property Currency_ISO_Code_GP() As String
    Get
      Return m_currency_exchange_configuration.currency_ISO_Code_GP
    End Get
    Set(ByVal Value As String)
      m_currency_exchange_configuration.currency_ISO_Code_GP = Value
    End Set
  End Property

  Public Property Symbol() As String
    Get
      Return m_currency_exchange_configuration.symbol
    End Get
    Set(ByVal Value As String)
      m_currency_exchange_configuration.symbol = Value
    End Set
  End Property

  Public Property Hidden_GUI() As Boolean
    Get
      Return m_currency_exchange_configuration.hidden_gui
    End Get
    Set(ByVal Value As Boolean)
      m_currency_exchange_configuration.hidden_gui = Value
    End Set
  End Property

  Public Property Hidden_SAS_HOST() As Boolean
    Get
      Return m_currency_exchange_configuration.hidden_sas_host
    End Get
    Set(ByVal Value As Boolean)
      m_currency_exchange_configuration.hidden_sas_host = Value
    End Set
  End Property

#End Region   ' Properties

#Region "Overrides"

  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _hidden_gui As String
    Dim _hidden_sas_host As String

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_GENERAL_PARAMS)
    _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(2070), "")

    _hidden_gui = IIf(Me.Hidden_GUI, GLB_NLS_GUI_AUDITOR.GetString(336), GLB_NLS_GUI_AUDITOR.GetString(337))
    _hidden_sas_host = IIf(Me.Hidden_SAS_HOST, GLB_NLS_GUI_AUDITOR.GetString(336), GLB_NLS_GUI_AUDITOR.GetString(337))

    Call _auditor_data.SetField(0, Me.Currency_ISO_Code_GP, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2072))
    Call _auditor_data.SetField(0, Me.Currencies_Accepted_GP, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2075))
    Call _auditor_data.SetField(0, Me.Symbol, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2073))
    Call _auditor_data.SetField(0, _hidden_gui, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2080))
    Call _auditor_data.SetField(0, _hidden_sas_host, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2403))

    Return _auditor_data

  End Function   ' AuditorData

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR

  End Function     ' DB_Delete

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR

  End Function     ' DB_Insert

  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim rc As Integer

    ' Read currency exchange configuration data
    rc = CurrencyExchangeConfigurationData_DbRead(SqlCtx)

    Select Case rc
      Case ENUM_DB_RC.DB_RC_OK
        Return ENUM_STATUS.STATUS_OK

      Case ENUM_DB_RC.DB_RC_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function       ' DB_Read

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim rc As Integer

    ' Update currency exchange configuration data
    rc = CurrencyExchangeConfigurationData_DbUpdate(SqlCtx)

    Select Case rc
      Case ENUM_DB_RC.DB_RC_OK
        Return ENUM_STATUS.STATUS_OK

      Case ENUM_DB_RC.DB_RC_ERROR_NOT_FOUND
        Return CLASS_BASE.ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function     ' DB_Update

  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim _target As CLS_CURRENCY_EXCHANGE_CONFIGURATION

    _target = New CLS_CURRENCY_EXCHANGE_CONFIGURATION

    _target.m_currency_exchange_configuration.currencies = Me.m_currency_exchange_configuration.currencies.Copy()
    _target.m_currency_exchange_configuration.currency_ISO_Code_GP = Me.m_currency_exchange_configuration.currency_ISO_Code_GP
    _target.m_currency_exchange_configuration.currencies_accepted_GP = Me.m_currency_exchange_configuration.currencies_accepted_GP
    _target.m_currency_exchange_configuration.symbol = Me.m_currency_exchange_configuration.symbol
    _target.m_currency_exchange_configuration.hidden_gui = Me.m_currency_exchange_configuration.hidden_gui
    _target.m_currency_exchange_configuration.hidden_sas_host = Me.m_currency_exchange_configuration.hidden_sas_host

    Return _target

  End Function     ' Duplicate

#End Region ' Overrides

#Region "Private Functions"

  Private Function CurrencyExchangeConfigurationData_DbRead(ByVal Context As Integer) As Integer

    Dim _str_bld As StringBuilder = New StringBuilder()
    Dim _rc As Integer

    If Not CurrencyExchange.ReadCurrencyExchangeFilter(False, m_currency_exchange_configuration.currencies) Then
      Return ENUM_STATUS.STATUS_ERROR
    End If

    _rc = CurrencyExchangeConf_DbRead(Context)

    If _rc <> ENUM_DB_RC.DB_RC_OK Then
      Return _rc
    End If

    Return ENUM_STATUS.STATUS_OK

  End Function    ' CurrencyExchangeConfigurationData_DbRead


  Private Function CurrencyExchangeConfigurationData_DbUpdate(ByVal Context As Integer) As Integer

    Dim _symbol_gui As String
    Dim _symbol_sas_host As String

    _symbol_gui = IIf(Me.m_currency_exchange_configuration.hidden_gui, String.Empty, Me.m_currency_exchange_configuration.symbol)
    _symbol_sas_host = IIf(Me.m_currency_exchange_configuration.hidden_sas_host, String.Empty, Me.m_currency_exchange_configuration.symbol)

    Try
      Using _db_trx As New DB_TRX()

        If Not Currency_Exchange_Configuration_DbUpdate("RegionalOptions", "CurrencyISOCode", Me.m_currency_exchange_configuration.currency_ISO_Code_GP, _db_trx.SqlTransaction) Then

          Return ENUM_DB_RC.DB_RC_ERROR_DB
        End If

        If Not Currency_Exchange_Configuration_DbUpdate("RegionalOptions", "CurrenciesAccepted", Me.m_currency_exchange_configuration.currencies_accepted_GP, _db_trx.SqlTransaction) Then

          Return ENUM_DB_RC.DB_RC_ERROR_DB
        End If

        If Not Currency_Exchange_Configuration_DbUpdate("WigosGUI", "CurrencySymbol", _symbol_gui, _db_trx.SqlTransaction) Then

          Return ENUM_DB_RC.DB_RC_ERROR_DB
        End If

        If Not Currency_Exchange_Configuration_DbUpdate("SasHost", "CurrencySymbol", _symbol_sas_host, _db_trx.SqlTransaction) Then

          Return ENUM_DB_RC.DB_RC_ERROR_DB
        End If

        _db_trx.Commit()
      End Using

      Return ENUM_DB_RC.DB_RC_OK
    Catch
    End Try

    Return ENUM_DB_RC.DB_RC_ERROR_DB
  End Function 'CurrencyExchangeConfigurationData_DbUpdate

  '----------------------------------------------------------------------------
  ' PURPOSE: Update the database General_Params. If it goes ok the method returns true 
  '                                              if it doesn't go ok return false
  ' PARAMS:
  '   - INPUT: General_Params.Key
  '   - INPUT: General_Params.SubKey 
  '   - INPUT: The value to update
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - It returns True or false, depends if the transaction was fine or not
  '
  ' NOTES:
  Private Function Currency_Exchange_Configuration_DbUpdate(ByVal Key As String, _
                                                   ByVal SubKey As String, _
                                                   ByVal NewValue As String, _
                                                   ByVal Trx As SqlTransaction) As Boolean
    Dim _sb As StringBuilder

    Try
      _sb = New StringBuilder()
      _sb.AppendLine("UPDATE GENERAL_PARAMS ")
      _sb.AppendLine("   SET GP_KEY_VALUE   = @pValue")
      _sb.AppendLine(" WHERE GP_GROUP_KEY   = @pGroup")
      _sb.AppendLine("   AND GP_SUBJECT_KEY = @pSubject")

      Using _cmd As SqlCommand = New SqlCommand(_sb.ToString(), Trx.Connection, Trx)
        _cmd.Parameters.Add("@pValue", SqlDbType.NVarChar).Value = NewValue
        _cmd.Parameters.Add("@pGroup", SqlDbType.NVarChar).Value = Key
        _cmd.Parameters.Add("@pSubject", SqlDbType.NVarChar).Value = SubKey

        Return (_cmd.ExecuteNonQuery() = 1)
      End Using
    Catch
    End Try

    Return False
  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE: It calls the Password_Configuration_DbRead and checks if the database result is ok
  '                                              
  ' PARAMS:
  '   - INPUT: 
  '   - OUTPUT:
  '
  ' RETURNS:
  '
  ' NOTES:
  Private Function CurrencyExchangeConf_DbRead(ByVal Context As Integer) As Integer

    Dim _final_db_result
    Dim _value As String
    Dim _general_params As WSI.Common.GeneralParam.Dictionary
    Dim _properties As CurrencyExchangeProperties

    _value = ""
    _final_db_result = ENUM_DB_RC.DB_RC_OK

    Try
      _general_params = WSI.Common.GeneralParam.Dictionary.Create()

      m_currency_exchange_configuration.currencies_accepted_GP = _general_params.GetString("RegionalOptions", "CurrenciesAccepted")
      m_currency_exchange_configuration.currency_ISO_Code_GP = _general_params.GetString("RegionalOptions", "CurrencyISOCode")

      _properties = CurrencyExchangeProperties.GetProperties(Currency_ISO_Code_GP)

      If Not _properties Is Nothing Then
        m_currency_exchange_configuration.symbol = _properties.Symbol
      End If

      m_currency_exchange_configuration.hidden_gui = False

      ' Set if symbols is hiden in GUI
      ' if current_ISO_Code is defined and symbols = '', the symbol is hidden. 
      If String.IsNullOrEmpty(_general_params.GetString("WigosGUI", "CurrencySymbol")) Then
        m_currency_exchange_configuration.hidden_gui = True
      End If

      m_currency_exchange_configuration.hidden_sas_host = False

      If String.IsNullOrEmpty(_general_params.GetString("SasHost", "CurrencySymbol")) Then
        m_currency_exchange_configuration.hidden_sas_host = True
      End If

      Return _final_db_result

    Catch

    End Try

    Return ENUM_DB_RC.DB_RC_ERROR_DB
  End Function

#End Region


End Class
