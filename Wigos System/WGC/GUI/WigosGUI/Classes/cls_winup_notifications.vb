﻿
'-------------------------------------------------------------------
' Copyright © 2016 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : cls_winup_notifications.vb
'
' DESCRIPTION : notifications winup class
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 19-ENE-2017  PDM    Initial version
'--------------------------------------------------------------------
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports WSI.Common
Imports Newtonsoft.Json.Linq
Imports System.IO
Imports Newtonsoft.Json
Imports System.Text

Public Class CLASS_WINUP_NOTIFICATIONS
  Inherits CLASS_BASE

#Region "Members"

  Private m_id As Integer
  Private m_title As String
  Private m_message As String
  Private m_target As String
  Private m_type_target As Integer
  Private m_isurl As Boolean
  Private m_segmentation As Int32
  Private m_schedule As Int32
  Private m_status As Boolean
  Private m_date_to As Date
  Private m_date_from As Date
  Private m_weekdays As Integer
  Private m_time_from As Integer
  Private m_time_to As Integer
  Private m_id_schedule As Integer
  Private m_id_segmentation As Integer

  'filters
  'Gender
  Private m_gender_filter As Integer

  'Birthday
  Private m_birthday_filter As Integer
  Private m_age_filter As Integer
  Private m_birthday_age_from As Integer
  Private m_birthday_age_to As Integer

  'Account
  Private m_created_account_filter As Integer
  Private m_created_account_anniversary_year_to As Integer
  Private m_created_account_anniversary_year_from As Integer
  Private m_created_account_working_days_included As Integer

  'Level
  Private m_level_filter As Integer
  Private m_is_vip As Boolean

  'Frequency
  Private m_freq_filter_last_days As Integer
  Private m_freq_filter_min_days As Integer
  Private m_freq_filter_min_cash_in As Integer

  'By Customer
  Private m_account_id As Integer

  'ByCustomerGroup

  Private m_accounts_ids As String


  'Image
  Private m_image_id As Integer
  Private m_image_content_type As String
  Private m_image_file_name As String
  Private m_image_data As String
  Private m_image As Image

#End Region

#Region " Enums "

  Public Enum ENUM_GENDER_FILTER
    NOT_SET = 0
    MEN_ONLY = 1
    WOMEN_ONLY = 2
  End Enum

  Public Enum ENUM_BIRTHDAY_FILTER
    NOT_SET = 0
    DAY_ONLY = 1
    WHOLE_MONTH = 2
    INCLUDE_AGE = 3
    EXCLUDE_AGE = 4
  End Enum

  Public Enum ENUM_CREATED_ACCOUNT_DATE_FILTER
    NOT_SET = 0
    WORKING_DAY_ONLY = 1
    WORKING_DAYS_INCLUDED = 2
    ANNIVERSARY_WHOLE_MONTH = 3
    ANNIVERSARY_WORKING_DAY_ONLY = 4
  End Enum

#End Region

#Region "Properties"

  Public Property Image() As Image
    Get
      Return m_image
    End Get
    Set(ByVal value As Image)
      m_image = value
    End Set
  End Property

  Public Property ImageId() As Integer
    Get
      Return m_image_id
    End Get
    Set(ByVal value As Integer)
      m_image_id = value
    End Set
  End Property


  Public Property ImageContentType() As String
    Get
      Return m_image_content_type
    End Get
    Set(ByVal value As String)
      m_image_content_type = value
    End Set
  End Property

  Public Property ImageFileName() As String
    Get
      Return m_image_file_name
    End Get
    Set(ByVal value As String)
      m_image_file_name = value
    End Set
  End Property

  Public Property ImageData() As String
    Get
      Return m_image_data
    End Get
    Set(ByVal value As String)
      m_image_data = value
    End Set
  End Property


  Public ReadOnly Property DefaultDate() As Date
    Get
      Return New Date(1900, 1, 1)
    End Get
  End Property


  Public Property Id() As Integer
    Get
      Return m_id
    End Get
    Set(ByVal value As Integer)
      m_id = value
    End Set
  End Property

  Public Property Title() As String
    Get
      Return m_title
    End Get
    Set(ByVal value As String)
      m_title = value
    End Set
  End Property

  Public Property Message() As String
    Get
      Return m_message
    End Get
    Set(ByVal value As String)
      m_message = value
    End Set
  End Property

  Public Property Target() As String
    Get
      Return m_target
    End Get
    Set(ByVal value As String)
      m_target = value
    End Set
  End Property

  Public Property Target_Type() As Integer
    Get
      Return m_type_target
    End Get
    Set(ByVal value As Integer)
      m_type_target = value
    End Set
  End Property

  Public Property IsUrl() As Boolean
    Get
      Return m_isurl
    End Get
    Set(ByVal value As Boolean)
      m_isurl = value
    End Set
  End Property

  Public Property Status() As Boolean
    Get
      Return m_status
    End Get
    Set(ByVal value As Boolean)
      m_status = value
    End Set
  End Property

  Public Property Segmentation() As Int32
    Get
      Return m_segmentation
    End Get
    Set(ByVal value As Int32)
      m_segmentation = value
    End Set
  End Property

  Public Property Schedule() As Int32
    Get
      Return m_schedule
    End Get
    Set(ByVal value As Int32)
      m_schedule = value
    End Set
  End Property


  Public Property DateFrom() As Date
    Get
      Return m_date_from
    End Get
    Set(ByVal value As Date)
      m_date_from = value
    End Set
  End Property

  Public Property DateTo() As Date
    Get
      Return m_date_to
    End Get
    Set(ByVal value As Date)
      m_date_to = value
    End Set
  End Property

  Public Property Weekdays() As Integer
    Get
      Return m_weekdays
    End Get
    Set(ByVal value As Integer)
      m_weekdays = value
    End Set
  End Property


  Public Property TimeFrom() As Integer
    Get
      Return m_time_from
    End Get
    Set(ByVal value As Integer)
      m_time_from = value
    End Set
  End Property


  Public Property TimeTo() As Integer
    Get
      Return m_time_to
    End Get
    Set(ByVal value As Integer)
      m_time_to = value
    End Set
  End Property


  Public Property IdSchedule() As Integer
    Get
      Return m_id_schedule
    End Get
    Set(ByVal value As Integer)
      m_id_schedule = value
    End Set
  End Property

  Public Property IdSegmentation() As Integer
    Get
      Return m_id_segmentation
    End Get
    Set(ByVal value As Integer)
      m_id_segmentation = value
    End Set
  End Property

  'Gender ========
  Public Property GenderFilter() As Integer
    Get
      Return m_gender_filter
    End Get
    Set(ByVal Value As Integer)
      m_gender_filter = Value
    End Set
  End Property

  'Birthday =======
  Public Property BirthdayFilter() As Integer
    Get
      Return m_birthday_filter
    End Get
    Set(ByVal Value As Integer)
      m_birthday_filter = Value
    End Set
  End Property

  Public Property AgeFilter() As Integer
    Get
      Return m_age_filter
    End Get
    Set(ByVal Value As Integer)
      m_age_filter = Value
    End Set
  End Property

  Public Property BirthdayAgeFrom() As Integer
    Get
      Return m_birthday_age_from
    End Get
    Set(ByVal value As Integer)
      m_birthday_age_from = value
    End Set
  End Property

  Public Property BirthdayAgeTo() As Integer
    Get
      Return m_birthday_age_to
    End Get
    Set(ByVal value As Integer)
      m_birthday_age_to = value
    End Set
  End Property


  'Create Account ======================
  Public Property CreatedAccountFilter() As Integer
    Get
      Return m_created_account_filter
    End Get
    Set(ByVal Value As Integer)
      m_created_account_filter = Value
    End Set
  End Property

  Public Property CreatedAccountWorkingDaysIncluded() As Integer
    Get
      Return m_created_account_working_days_included
    End Get
    Set(ByVal Value As Integer)
      m_created_account_working_days_included = Value
    End Set
  End Property

  Public Property CreatedAccountAnniversaryYearFrom() As Integer
    Get
      Return m_created_account_anniversary_year_from
    End Get
    Set(ByVal Value As Integer)
      m_created_account_anniversary_year_from = Value
    End Set
  End Property

  Public Property CreatedAccountAnniversaryYearTo() As Integer
    Get
      Return m_created_account_anniversary_year_to
    End Get
    Set(ByVal Value As Integer)
      m_created_account_anniversary_year_to = Value
    End Set
  End Property

  'Level==========================
  Public Property LevelFilter() As Integer
    Get
      Return m_level_filter
    End Get
    Set(ByVal Value As Integer)
      m_level_filter = Value
    End Set
  End Property


  Public Property IsVip() As Boolean
    Get
      Return m_is_vip
    End Get
    Set(ByVal value As Boolean)
      m_is_vip = value
    End Set
  End Property


  'Frequency =====================
  Public Property FreqFilterLastDays() As Integer
    Get
      Return m_freq_filter_last_days
    End Get
    Set(ByVal Value As Integer)
      m_freq_filter_last_days = Value
    End Set
  End Property

  Public Property FreqFilterMinDays() As Integer
    Get
      Return m_freq_filter_min_days
    End Get
    Set(ByVal Value As Integer)
      m_freq_filter_min_days = Value
    End Set
  End Property

  Public Property FreqFilterMinCashIn() As Double
    Get
      Return m_freq_filter_min_cash_in
    End Get
    Set(ByVal Value As Double)
      m_freq_filter_min_cash_in = Value
    End Set
  End Property

  'By CUSTOMER
  Public Property AccountId() As Integer
    Get
      Return m_account_id
    End Get
    Set(ByVal value As Integer)
      m_account_id = value
    End Set
  End Property


  'BY CUSTOMER GROUP
  Public Property AccountsIds() As String
    Get
      Return Me.m_accounts_ids
    End Get
    Set(ByVal value As String)
      Me.m_accounts_ids = value
    End Set
  End Property


  Public ReadOnly Property WorkingDaysText(ByVal Days As Integer) As String
    Get
      Dim _str_binary As String
      Dim _bit_array As Char()
      Dim _str_days As String

      _str_days = ""

      _str_binary = Convert.ToString(Days, 2)
      _str_binary = New String("0", 7 - _str_binary.Length) + _str_binary

      _bit_array = _str_binary.ToCharArray()

      If (_bit_array(6) = "1") Then _str_days = _str_days & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(325) '"Monday "
      If (_bit_array(5) = "1") Then _str_days = _str_days & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(326) '"Tuesday "
      If (_bit_array(4) = "1") Then _str_days = _str_days & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(327) '"Wednesday "
      If (_bit_array(3) = "1") Then _str_days = _str_days & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(328) '"Thursday "
      If (_bit_array(2) = "1") Then _str_days = _str_days & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(329) '"Friday "
      If (_bit_array(1) = "1") Then _str_days = _str_days & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(330) '"Saturday "
      If (_bit_array(0) = "1") Then _str_days = _str_days & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(331) '"Sunday "

      Return _str_days.TrimStart

    End Get
  End Property

#End Region

#Region " Overrides functions "
  ' Constructor
  Public Sub New()

  End Sub



  Public Overrides Function AuditorData() As CLASS_AUDITOR_DATA

    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim gender_filter As String
    Dim birthday_filter As String
    Dim _birthday_filter As Integer
    Dim _age_filter As Integer
    Dim _age_filter_string As String
    Dim _yes_text As String
    Dim _no_text As String
    Dim _created_account_filter As Integer
    Dim _created_account_filter_string As String
    Dim string_date As String
    Dim elapsed As TimeSpan

    _yes_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(278)
    _no_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_USER_ACTIVITY)

    _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(7890), m_id.ToString())

    Call _auditor_data.SetField(0, m_title, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7522))
    Call _auditor_data.SetField(0, m_message, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1428))
    Call _auditor_data.SetField(0, m_isurl, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7872))
    Call _auditor_data.SetField(0, m_status, GLB_NLS_GUI_PLAYER_TRACKING.GetString(532))
    Call _auditor_data.SetField(0, m_target, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7867))
    Call _auditor_data.SetField(0, m_type_target, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7868))

    Call _auditor_data.SetField(0, GetInfoCRC(Me.Image), GLB_NLS_GUI_PLAYER_TRACKING.GetString(7561))

    Call _auditor_data.SetField(0, IIf(m_schedule = 0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7870), GLB_NLS_GUI_PLAYER_TRACKING.GetString(7870)), GLB_NLS_GUI_PLAYER_TRACKING.GetString(7869))


    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(299), WorkingDaysText(m_weekdays))

    ' Date Start
    string_date = GUI_FormatDate(m_date_from, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    Call _auditor_data.SetField(0, string_date, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5838))

    ' Date Finish
    string_date = GUI_FormatDate(m_date_to, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    Call _auditor_data.SetField(0, string_date, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5839))

    ' Schedule timetable 
    elapsed = TimeSpan.FromSeconds(m_time_from)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(292), GLB_NLS_GUI_PLAYER_TRACKING.GetString(297) & " " + elapsed.ToString())
    elapsed = TimeSpan.FromSeconds(m_time_to)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(292), GLB_NLS_GUI_PLAYER_TRACKING.GetString(298) & " " + elapsed.ToString())


    Select Case m_segmentation
      Case 0
        Call _auditor_data.SetField(0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7876), GLB_NLS_GUI_PLAYER_TRACKING.GetString(7874))
      Case 1
        Call _auditor_data.SetField(0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7877), GLB_NLS_GUI_PLAYER_TRACKING.GetString(7874))
      Case 2
        Call _auditor_data.SetField(0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7878), GLB_NLS_GUI_PLAYER_TRACKING.GetString(7874))
      Case 3
        Call _auditor_data.SetField(0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7879), GLB_NLS_GUI_PLAYER_TRACKING.GetString(7874))
      Case 4
        Call _auditor_data.SetField(0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7887), GLB_NLS_GUI_PLAYER_TRACKING.GetString(7874))
    End Select

    ' Gender Filter
    gender_filter = AUDIT_NONE_STRING
    Select Case Me.GenderFilter
      Case ENUM_GENDER_FILTER.NOT_SET
        gender_filter = AUDIT_NONE_STRING

      Case ENUM_GENDER_FILTER.MEN_ONLY
        gender_filter = GLB_NLS_GUI_PLAYER_TRACKING.GetString(214)

      Case ENUM_GENDER_FILTER.WOMEN_ONLY
        gender_filter = GLB_NLS_GUI_PLAYER_TRACKING.GetString(215)

    End Select
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(213), gender_filter)

    ' Date of birth Filter
    _birthday_filter = 0
    _age_filter = Me.AgeFilter
    If Me.BirthdayFilter.ToString().Length > 1 Then
      Call WSI.Common.Promotion.DecodeBirthdayFilter(Me.BirthdayFilter, _age_filter, _birthday_filter, Me.BirthdayAgeFrom, Me.BirthdayAgeTo)
    Else
      _birthday_filter = Me.BirthdayFilter
    End If
    birthday_filter = AUDIT_NONE_STRING
    Select Case _birthday_filter
      Case ENUM_BIRTHDAY_FILTER.NOT_SET
        birthday_filter = AUDIT_NONE_STRING

      Case ENUM_BIRTHDAY_FILTER.DAY_ONLY
        birthday_filter = GLB_NLS_GUI_PLAYER_TRACKING.GetString(239)

      Case ENUM_BIRTHDAY_FILTER.WHOLE_MONTH
        birthday_filter = GLB_NLS_GUI_PLAYER_TRACKING.GetString(240)
    End Select

    _age_filter_string = AUDIT_NONE_STRING
    Select Case _age_filter
      Case ENUM_BIRTHDAY_FILTER.INCLUDE_AGE
        _age_filter_string = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2702)

      Case ENUM_BIRTHDAY_FILTER.EXCLUDE_AGE
        _age_filter_string = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2703)
    End Select

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(216), birthday_filter)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2730), _age_filter_string)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2730), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2706) & ": " & Me.BirthdayAgeFrom)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2730), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2707) & ": " & Me.BirthdayAgeTo)

    ' AVZ 01-OCT-2015: Created account date Filter
    _created_account_filter = 0

    If Me.CreatedAccountFilter.ToString().Length > 1 Then
      Call WSI.Common.Promotion.DecodeCreatedAccountFilter(Me.CreatedAccountFilter, _created_account_filter, Me.CreatedAccountWorkingDaysIncluded, Me.CreatedAccountAnniversaryYearFrom, Me.CreatedAccountAnniversaryYearTo)
    Else
      _created_account_filter = Me.CreatedAccountFilter
    End If
    _created_account_filter_string = AUDIT_NONE_STRING
    Select Case _created_account_filter
      Case ENUM_CREATED_ACCOUNT_DATE_FILTER.NOT_SET
        _created_account_filter_string = AUDIT_NONE_STRING
      Case ENUM_CREATED_ACCOUNT_DATE_FILTER.WORKING_DAY_ONLY
        _created_account_filter_string = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6766)
      Case ENUM_CREATED_ACCOUNT_DATE_FILTER.WORKING_DAYS_INCLUDED
        _created_account_filter_string = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6767)
      Case ENUM_CREATED_ACCOUNT_DATE_FILTER.ANNIVERSARY_WHOLE_MONTH
        _created_account_filter_string = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6768)
      Case ENUM_CREATED_ACCOUNT_DATE_FILTER.ANNIVERSARY_WORKING_DAY_ONLY
        _created_account_filter_string = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6769)
    End Select

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6770), _created_account_filter_string)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6770), GLB_NLS_GUI_PLAYER_TRACKING.GetString(6771) & ": " & Me.CreatedAccountWorkingDaysIncluded)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6770), GLB_NLS_GUI_PLAYER_TRACKING.GetString(6772) & ": " & Me.CreatedAccountAnniversaryYearFrom)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6770), GLB_NLS_GUI_PLAYER_TRACKING.GetString(6773) & ": " & Me.CreatedAccountAnniversaryYearTo)

    ' Level Filter
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(404), LevelFilterText(Me.LevelFilter))

    ' Frequency Filter
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(414), GUI_FormatNumber(Me.FreqFilterLastDays, 0))
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(415), GUI_FormatNumber(Me.FreqFilterMinDays, 0))
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(416), GUI_FormatCurrency(Me.FreqFilterMinCashIn))

    Call _auditor_data.SetField(0, m_account_id, GLB_NLS_GUI_PLAYER_TRACKING.GetString(496))

    Call _auditor_data.SetField(0, m_accounts_ids, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1237))

    Return _auditor_data

  End Function

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As CLASS_BASE.ENUM_STATUS
    Dim _sql_trx As SqlTransaction
    Dim _sql_string_builder As StringBuilder
    Dim _command As SqlCommand

    _sql_string_builder = New StringBuilder()

    Try
      Using _db_trx As DB_TRX = New DB_TRX()
        _sql_trx = _db_trx.SqlTransaction

        _sql_string_builder.AppendLine(" DELETE MAPP_NOTIFICATION_SCHEDULE")
        _sql_string_builder.AppendLine(" WHERE NS_ID_NOTIFICATION = @id")

        _command = New SqlCommand( _
            _sql_string_builder.ToString(), _
            _sql_trx.Connection, _sql_trx)

        _command.Parameters.Add("@id", SqlDbType.BigInt).Value = Me.Id
        _command.ExecuteNonQuery()

        _sql_trx.Commit()
      End Using

      Using _db_trx As DB_TRX = New DB_TRX()
        _sql_trx = _db_trx.SqlTransaction

        _sql_string_builder.AppendLine(" DELETE mapp_notification")
        _sql_string_builder.AppendLine(" WHERE no_id_notification= @id")

        _command = New SqlCommand( _
            _sql_string_builder.ToString(), _
            _sql_trx.Connection, _sql_trx)

        _command.Parameters.Add("@id", SqlDbType.BigInt).Value = Me.Id
        _command.ExecuteNonQuery()

        _sql_trx.Commit()
      End Using

      Return ENUM_STATUS.STATUS_OK
    Catch ex As Exception
      Return ENUM_STATUS.STATUS_ERROR
    End Try

  End Function

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As CLASS_BASE.ENUM_STATUS

    Dim _sql_string_builder As System.Text.StringBuilder
    Dim _sql_trx As System.Data.SqlClient.SqlTransaction
    Dim _param_id As SqlParameter
    Dim _row_affected As Integer

    Try

      _sql_string_builder = New StringBuilder()

      Using _db_trx As New DB_TRX()

        _sql_trx = _db_trx.SqlTransaction

        If Not Me.Image Is Nothing Then
          SaveImage()
        End If

        _sql_string_builder.AppendLine(" INSERT INTO mapp_notification (")
        _sql_string_builder.AppendLine(" no_id_application, ")
        _sql_string_builder.AppendLine(" NO_TITLE, ")
        _sql_string_builder.AppendLine(" NO_MESSAGE, ")
        _sql_string_builder.AppendLine(" NO_TARGET_ISURL, ")
        _sql_string_builder.AppendLine(" NO_TARGET, ")
        _sql_string_builder.AppendLine(" NO_TARGET_IDENTIFIER, ")
        _sql_string_builder.AppendLine(" NO_STATUS, ")
        _sql_string_builder.AppendLine(" NO_SCHEDULE, ")
        _sql_string_builder.AppendLine(" NO_SEGMENTATION,")
        _sql_string_builder.AppendLine(" NO_IMAGE_ID,")
        _sql_string_builder.AppendLine(" no_created_date) ")

        _sql_string_builder.AppendLine(" VALUES ")

        _sql_string_builder.AppendLine(" (@no_id_application, ")
        _sql_string_builder.AppendLine(" @title, ")
        _sql_string_builder.AppendLine(" @message, ")
        _sql_string_builder.AppendLine(" @isurl, ")
        _sql_string_builder.AppendLine(" @target, ")
        _sql_string_builder.AppendLine(" @targetype, ")
        _sql_string_builder.AppendLine(" @status, ")
        _sql_string_builder.AppendLine(" @schedule, ")
        _sql_string_builder.AppendLine(" @segmentation,")
        _sql_string_builder.AppendLine(" @image_id,")
        _sql_string_builder.AppendLine(" @no_created_date) ")
        _sql_string_builder.AppendLine("        SET @pId    = SCOPE_IDENTITY();   ")

        Using _cmd As SqlCommand = New SqlCommand(_sql_string_builder.ToString(), _sql_trx.Connection, _sql_trx)

          _cmd.Parameters.Add("@no_id_application", SqlDbType.BigInt).Value = 1
          _cmd.Parameters.Add("@title", SqlDbType.NVarChar).Value = m_title
          _cmd.Parameters.Add("@message", SqlDbType.NVarChar).Value = m_message
          _cmd.Parameters.Add("@isurl", SqlDbType.Bit).Value = m_isurl
          _cmd.Parameters.Add("@target", SqlDbType.NVarChar).Value = m_target
          _cmd.Parameters.Add("@targetype", SqlDbType.BigInt).Value = m_type_target
          _cmd.Parameters.Add("@image_id", SqlDbType.BigInt).Value = m_image_id
          _cmd.Parameters.Add("@status", SqlDbType.SmallInt).Value = IIf(m_status, 1, 0)
          _cmd.Parameters.Add("@schedule", SqlDbType.SmallInt).Value = m_schedule
          _cmd.Parameters.Add("@segmentation", SqlDbType.SmallInt).Value = m_segmentation
          _cmd.Parameters.Add("@no_created_date", SqlDbType.DateTime).Value = DateTime.Now

          _param_id = _cmd.Parameters.Add("@pId", SqlDbType.Int)
          _param_id.Direction = ParameterDirection.Output

          _row_affected = _cmd.ExecuteNonQuery()

          If _row_affected <> 1 Or IsDBNull(_param_id.Value) Then
            Return ENUM_STATUS.STATUS_ERROR
          End If

        End Using

        Me.Id = _param_id.Value

        'Schedule---
        _sql_string_builder = New StringBuilder()
        _sql_string_builder.AppendLine("INSERT INTO MAPP_NOTIFICATION_SCHEDULE(")
        _sql_string_builder.AppendLine("NS_ID_NOTIFICATION, ")
        _sql_string_builder.AppendLine("ns_date_from,")
        _sql_string_builder.AppendLine("ns_date_to, ")
        _sql_string_builder.AppendLine("ns_weekdays,")
        _sql_string_builder.AppendLine("ns_time_from, ")
        _sql_string_builder.AppendLine("ns_time_to,")
        _sql_string_builder.AppendLine("ns_second_time_enabled,")
        _sql_string_builder.AppendLine("ns_second_time_from,")
        _sql_string_builder.AppendLine("ns_second_time_to) ")
        _sql_string_builder.AppendLine(" VALUES (")
        _sql_string_builder.AppendLine("@NS_ID_NOTIFICATION, ")
        _sql_string_builder.AppendLine("@ns_date_from,")
        _sql_string_builder.AppendLine("@ns_date_to, ")
        _sql_string_builder.AppendLine("@ns_weekdays,")
        _sql_string_builder.AppendLine("@ns_time_from, ")
        _sql_string_builder.AppendLine("@ns_time_to,")
        _sql_string_builder.AppendLine("@ns_second_time_enabled,")
        _sql_string_builder.AppendLine("@ns_second_time_from,")
        _sql_string_builder.AppendLine("@ns_second_time_to) ")

        Using _cmd As SqlCommand = New SqlCommand(_sql_string_builder.ToString(), _sql_trx.Connection, _sql_trx)

          _cmd.Parameters.Add("@NS_ID_NOTIFICATION", SqlDbType.BigInt).Value = m_id
          _cmd.Parameters.Add("@ns_date_from", SqlDbType.DateTime).Value = m_date_from
          _cmd.Parameters.Add("@ns_date_to", SqlDbType.DateTime).Value = m_date_to
          _cmd.Parameters.Add("@ns_weekdays", SqlDbType.SmallInt).Value = m_weekdays
          _cmd.Parameters.Add("@ns_time_from", SqlDbType.BigInt).Value = m_time_from
          _cmd.Parameters.Add("@ns_time_to", SqlDbType.BigInt).Value = m_time_to
          _cmd.Parameters.Add("@ns_second_time_enabled", SqlDbType.Bit).Value = False
          _cmd.Parameters.Add("@ns_second_time_from", SqlDbType.BigInt).Value = 0
          _cmd.Parameters.Add("@ns_second_time_to", SqlDbType.BigInt).Value = 0

          _cmd.ExecuteNonQuery()

        End Using

        'Segmentation -------
        _sql_string_builder = New StringBuilder()
        _sql_string_builder.AppendLine("INSERT INTO MAPP_NOTIFICATION_SEGMENTATION(")
        _sql_string_builder.AppendLine("NS_ID_NOTIFICATION,")

        _sql_string_builder.AppendLine("NS_ID_ACCOUNT,")
        _sql_string_builder.AppendLine("NS_IDS_ACCOUNTS,")

        _sql_string_builder.AppendLine("NS_GENDER_FILTER,")
        _sql_string_builder.AppendLine("NS_BIRTHDAY_FILTER,")
        _sql_string_builder.AppendLine("NS_CREATED_ACCOUNT_FILTER,")

        _sql_string_builder.AppendLine("NS_LEVEL_FILTER,")
        _sql_string_builder.AppendLine("NS_IS_VIP,")

        _sql_string_builder.AppendLine("NS_FREQ_FILTER_LAST_DAYS,")
        _sql_string_builder.AppendLine("NS_FREQ_FILTER_MIN_DAYS,")
        _sql_string_builder.AppendLine("NS_FREQ_FILTER_MIN_CASH_IN ")

        _sql_string_builder.AppendLine(") VALUES (")

        _sql_string_builder.AppendLine("@NS_ID_NOTIFICATION,")
        _sql_string_builder.AppendLine("@NS_ID_ACCOUNT,")
        _sql_string_builder.AppendLine("@NS_IDS_ACCOUNTS,")

        _sql_string_builder.AppendLine("@NS_GENDER_FILTER,")
        _sql_string_builder.AppendLine("@NS_BIRTHDAY_FILTER,")
        _sql_string_builder.AppendLine("@NS_CREATED_ACCOUNT_FILTER,")
        _sql_string_builder.AppendLine("@NS_LEVEL_FILTER,")

        _sql_string_builder.AppendLine("@NS_IS_VIP,")

        _sql_string_builder.AppendLine("@NS_FREQ_FILTER_LAST_DAYS,")
        _sql_string_builder.AppendLine("@NS_FREQ_FILTER_MIN_DAYS,")
        _sql_string_builder.AppendLine("@NS_FREQ_FILTER_MIN_CASH_IN ")

        _sql_string_builder.AppendLine(")")

        _sql_string_builder.AppendLine("        SET @Id    = SCOPE_IDENTITY();   ")

        Using _cmd As SqlCommand = New SqlCommand(_sql_string_builder.ToString(), _sql_trx.Connection, _sql_trx)

          _cmd.Parameters.Add("@NS_ID_NOTIFICATION", SqlDbType.BigInt).Value = m_id

          'BY FILTERS
          _cmd.Parameters.Add("@NS_GENDER_FILTER", SqlDbType.Int).Value = m_gender_filter
          _cmd.Parameters.Add("@NS_BIRTHDAY_FILTER", SqlDbType.Int).Value = m_birthday_filter
          _cmd.Parameters.Add("@NS_CREATED_ACCOUNT_FILTER", SqlDbType.Int).Value = m_created_account_filter
          _cmd.Parameters.Add("@NS_LEVEL_FILTER", SqlDbType.Int).Value = m_level_filter
          _cmd.Parameters.Add("@NS_FREQ_FILTER_LAST_DAYS", SqlDbType.Int).Value = m_freq_filter_last_days
          _cmd.Parameters.Add("@NS_FREQ_FILTER_MIN_DAYS", SqlDbType.Int).Value = m_freq_filter_min_days
          _cmd.Parameters.Add("@NS_FREQ_FILTER_MIN_CASH_IN", SqlDbType.Money).Value = m_freq_filter_min_cash_in
          _cmd.Parameters.Add("@NS_IS_VIP", SqlDbType.Bit).Value = m_is_vip

          'BY CUSTOMER
          _cmd.Parameters.Add("@NS_ID_ACCOUNT", SqlDbType.BigInt).Value = m_account_id

          'BY CUSTOMER GROUP
          _cmd.Parameters.Add("@NS_IDS_ACCOUNTS", SqlDbType.NVarChar).Value = m_accounts_ids

          _param_id = _cmd.Parameters.Add("@Id", SqlDbType.Int)
          _param_id.Direction = ParameterDirection.Output

          _row_affected = _cmd.ExecuteNonQuery()

          If _row_affected <> 1 Or IsDBNull(_param_id.Value) Then
            Return ENUM_STATUS.STATUS_ERROR
          End If

          Me.m_id_segmentation = _param_id.Value

        End Using

        ' --- Ok
        _db_trx.Commit()

      End Using

      Return ENUM_STATUS.STATUS_OK
    Catch ex As Exception
      Return ENUM_STATUS.STATUS_ERROR
    End Try

    Return Nothing
  End Function

  Public Overrides Function DB_Read(ObjectId As Object, ByRef SqlCtx As Integer) As CLASS_BASE.ENUM_STATUS

    Dim _sql_cmd As SqlCommand
    Dim _sql_string_builder As System.Text.StringBuilder
    Dim _datatable As DataTable
    Dim _sql_transaction As System.Data.SqlClient.SqlTransaction
    Dim _rc As Integer

    Try

      Me.m_id = ObjectId
      _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      _sql_transaction = Nothing
      _sql_string_builder = New System.Text.StringBuilder()

      With _sql_string_builder

        'NOTIFICATION
        .AppendLine("SELECT   no_id_notification ")
        .AppendLine("       , no_title   ")
        .AppendLine("       , no_message   ")
        .AppendLine("       , no_target   ")
        .AppendLine("     	, no_target_identifier   ")
        .AppendLine("       , no_target_isurl   ")
        .AppendLine("	      , no_status   ")
        .AppendLine("	      , no_schedule   ")
        .AppendLine("	      , no_segmentation    ")
        .AppendLine("	      , ISNULL(no_image_id,0) AS no_image_id ")

        'SCHEDULE
        .AppendLine("		     ,NS.NS_ID AS NO_ID_SCHEDULE ")
        .AppendLine("        ,NS_DATE_FROM   AS NO_DATE_FROM   ")
        .AppendLine("        ,NS_DATE_TO  AS NO_DATE_TO      ")
        .AppendLine("        ,NS_WEEKDAYS AS NO_WEEKDAYS      ")
        .AppendLine("        ,NS_TIME_FROM AS NO_TIME_FROM      ")
        .AppendLine("        ,NS_TIME_TO AS NO_TIME_TO     ")
        .AppendLine("        ,NS_IS_VIP  AS NO_IS_VIP   ")


        'SEGMENTATION

        .AppendLine("       ,NSE.NS_ID AS NO_ID_SEGMENTATION")
        .AppendLine("       ,NS_ID_ACCOUNT AS NO_ID_ACCOUNT")
        .AppendLine("       ,NS_IDS_ACCOUNTS AS NO_IDS_ACCOUNTS")

        .AppendLine("       ,NS_GENDER_FILTER AS NO_GENDER_FILTER")
        .AppendLine("       ,NS_BIRTHDAY_FILTER AS NO_BIRTHDAY_FILTER")
        .AppendLine("       ,NS_CREATED_ACCOUNT_FILTER AS NO_CREATED_ACCOUNT_FILTER")
        .AppendLine("       ,NS_LEVEL_FILTER AS NO_LEVEL_FILTER")
        .AppendLine("       ,NS_FREQ_FILTER_LAST_DAYS AS NO_FREQ_FILTER_LAST_DAYS")
        .AppendLine("       ,NS_FREQ_FILTER_MIN_DAYS AS NO_FREQ_FILTER_MIN_DAYS")
        .AppendLine("       ,NS_FREQ_FILTER_MIN_CASH_IN AS NO_FREQ_FILTER_MIN_CASH_IN ")

        .AppendLine("FROM   mapp_notification")

        .AppendLine("       LEFT JOIN  MAPP_NOTIFICATION_SCHEDULE   ")
        .AppendLine("       NS ON  MAPP_NOTIFICATION.NO_ID_NOTIFICATION = NS.NS_ID_NOTIFICATION ")

        .AppendLine("       LEFT JOIN  MAPP_NOTIFICATION_SEGMENTATION   ")
        .AppendLine("       NSE ON  MAPP_NOTIFICATION.NO_ID_NOTIFICATION = NSE.NS_ID_NOTIFICATION ")

        .AppendLine(" WHERE   no_id_notification = @id ")


      End With


      _sql_cmd = New SqlCommand(_sql_string_builder.ToString())
      _sql_cmd.Parameters.Add("@id", SqlDbType.BigInt).Value = Me.m_id

      _datatable = GUI_GetTableUsingCommand(_sql_cmd, 1)

      If Not IsNothing(_datatable) AndAlso _datatable.Rows.Count = 1 Then

        m_title = _datatable.Rows(0).Item("no_title")
        m_message = _datatable.Rows(0).Item("no_message")
        m_target = _datatable.Rows(0).Item("no_target")
        m_type_target = _datatable.Rows(0).Item("no_target_identifier")
        m_isurl = _datatable.Rows(0).Item("no_target_isurl")
        m_status = _datatable.Rows(0).Item("no_status")
        m_schedule = _datatable.Rows(0).Item("no_schedule")
        m_segmentation = IIf(IsDBNull(_datatable.Rows(0).Item("no_segmentation")), 0, _datatable.Rows(0).Item("no_segmentation"))
        m_image_id = _datatable.Rows(0).Item("no_image_id")

        'SCHEDULE
        m_date_from = IIf(IsDBNull(_datatable.Rows(0).Item("no_date_from")), DefaultDate, _datatable.Rows(0).Item("no_date_from"))
        m_date_to = IIf(IsDBNull(_datatable.Rows(0).Item("no_date_to")), DefaultDate, _datatable.Rows(0).Item("no_date_to"))
        m_weekdays = IIf(IsDBNull(_datatable.Rows(0).Item("no_weekdays")), 0, _datatable.Rows(0).Item("no_weekdays"))
        m_time_from = IIf(IsDBNull(_datatable.Rows(0).Item("no_time_from")), 0, _datatable.Rows(0).Item("no_time_from"))
        m_time_to = IIf(IsDBNull(_datatable.Rows(0).Item("no_time_to")), 0, _datatable.Rows(0).Item("no_time_to"))
        m_id_schedule = IIf(IsDBNull(_datatable.Rows(0).Item("NO_ID_SCHEDULE")), 0, _datatable.Rows(0).Item("NO_ID_SCHEDULE"))

        'SEGMENTATION
        m_id_segmentation = IIf(IsDBNull(_datatable.Rows(0).Item("NO_ID_SEGMENTATION")), 0, _datatable.Rows(0).Item("NO_ID_SEGMENTATION"))

        m_is_vip = IIf(IsDBNull(_datatable.Rows(0).Item("NO_IS_VIP")), False, _datatable.Rows(0).Item("NO_IS_VIP"))

        m_gender_filter = IIf(IsDBNull(_datatable.Rows(0).Item("NO_GENDER_FILTER")), 0, _datatable.Rows(0).Item("NO_GENDER_FILTER"))

        If IsDBNull(_datatable.Rows(0).Item("NO_BIRTHDAY_FILTER")) Then
          m_age_filter = 0
          m_birthday_filter = 0
          m_birthday_age_from = 0
          m_birthday_age_to = 0
        Else
          WSI.Common.Promotion.DecodeBirthdayFilter(_datatable.Rows(0).Item("NO_BIRTHDAY_FILTER"), m_age_filter, m_birthday_filter, m_birthday_age_from, m_birthday_age_to)
        End If

        If IsDBNull(_datatable.Rows(0).Item("NO_CREATED_ACCOUNT_FILTER")) Then
          m_created_account_filter = 0
          m_created_account_working_days_included = 0
          m_created_account_anniversary_year_from = 0
          m_created_account_anniversary_year_to = 0
        Else
          WSI.Common.Promotion.DecodeCreatedAccountFilter(_datatable.Rows(0).Item("NO_CREATED_ACCOUNT_FILTER"), m_created_account_filter, m_created_account_working_days_included, m_created_account_anniversary_year_from, m_created_account_anniversary_year_to)
        End If

        m_level_filter = IIf(IsDBNull(_datatable.Rows(0).Item("NO_LEVEL_FILTER")), 0, _datatable.Rows(0).Item("NO_LEVEL_FILTER"))

        m_freq_filter_last_days = IIf(IsDBNull(_datatable.Rows(0).Item("NO_FREQ_FILTER_LAST_DAYS")), 0, _datatable.Rows(0).Item("NO_FREQ_FILTER_LAST_DAYS"))
        m_freq_filter_min_days = IIf(IsDBNull(_datatable.Rows(0).Item("NO_FREQ_FILTER_MIN_DAYS")), 0, _datatable.Rows(0).Item("NO_FREQ_FILTER_MIN_DAYS"))
        m_freq_filter_min_cash_in = IIf(IsDBNull(_datatable.Rows(0).Item("NO_FREQ_FILTER_MIN_CASH_IN")), 0, _datatable.Rows(0).Item("NO_FREQ_FILTER_MIN_CASH_IN"))

        m_account_id = IIf(IsDBNull(_datatable.Rows(0).Item("NO_ID_ACCOUNT")), 0, _datatable.Rows(0).Item("NO_ID_ACCOUNT"))
        m_accounts_ids = IIf(IsDBNull(_datatable.Rows(0).Item("NO_IDS_ACCOUNTS")), "", _datatable.Rows(0).Item("NO_IDS_ACCOUNTS"))

        If m_image_id > 0 Then
          GetImage()
        End If

        _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return _rc

      End If

      Return ENUM_STATUS.STATUS_OK

    Catch ex As Exception
      Return ENUM_STATUS.STATUS_ERROR
    End Try

  End Function

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As CLASS_BASE.ENUM_STATUS

    Dim _sql_string_builder As System.Text.StringBuilder
    Dim _sql_trx As System.Data.SqlClient.SqlTransaction
    Dim _param_id As SqlParameter
    Dim _row_affected As Integer

    Try

      _sql_string_builder = New StringBuilder()

      Using _db_trx As New DB_TRX()

        _sql_trx = _db_trx.SqlTransaction

        If m_image_id = 0 And Not IsNothing(m_image) Or m_image_id > 0 And Not IsNothing(m_image) And m_image_file_name = String.Empty Then
          SaveImage()
        End If

        If m_image_id > 0 And IsNothing(m_image) Then
          DeleteImage()
          m_image_id = 0
        End If

        _sql_string_builder.AppendLine(" UPDATE mapp_notification SET ")
        _sql_string_builder.AppendLine(" NO_TITLE = @title, ")
        _sql_string_builder.AppendLine(" NO_MESSAGE= @message, ")
        _sql_string_builder.AppendLine(" NO_TARGET_ISURL=@isurl, ")
        _sql_string_builder.AppendLine(" NO_TARGET=@target, ")
        _sql_string_builder.AppendLine(" NO_TARGET_IDENTIFIER=@targetype, ")
        _sql_string_builder.AppendLine(" NO_STATUS=@status, ")
        _sql_string_builder.AppendLine(" NO_SCHEDULE=@schedule, ")
        _sql_string_builder.AppendLine(" NO_IMAGE_ID=@image_id, ")
        _sql_string_builder.AppendLine(" NO_SEGMENTATION=@segmentation ")

        _sql_string_builder.AppendLine(" WHERE no_id_notification= @id")

        Using _cmd As SqlCommand = New SqlCommand(_sql_string_builder.ToString(), _sql_trx.Connection, _sql_trx)

          _cmd.Parameters.Add("@id", SqlDbType.BigInt).Value = m_id
          _cmd.Parameters.Add("@title", SqlDbType.NVarChar).Value = m_title
          _cmd.Parameters.Add("@message", SqlDbType.NVarChar).Value = m_message
          _cmd.Parameters.Add("@isurl", SqlDbType.Bit).Value = m_isurl
          _cmd.Parameters.Add("@target", SqlDbType.NVarChar).Value = m_target
          _cmd.Parameters.Add("@targetype", SqlDbType.BigInt).Value = m_type_target
          _cmd.Parameters.Add("@image_id", SqlDbType.BigInt).Value = m_image_id
          _cmd.Parameters.Add("@status", SqlDbType.SmallInt).Value = IIf(m_status, 1, 0)
          _cmd.Parameters.Add("@schedule", SqlDbType.SmallInt).Value = m_schedule
          _cmd.Parameters.Add("@segmentation", SqlDbType.SmallInt).Value = m_segmentation

          _cmd.ExecuteNonQuery()
        End Using

        If m_id_schedule = 0 Then
          _sql_string_builder = New StringBuilder()
          _sql_string_builder.AppendLine("INSERT INTO MAPP_NOTIFICATION_SCHEDULE(")
          _sql_string_builder.AppendLine("NS_ID_NOTIFICATION, ")
          _sql_string_builder.AppendLine("ns_date_from,")
          _sql_string_builder.AppendLine("ns_date_to, ")
          _sql_string_builder.AppendLine("ns_weekdays,")
          _sql_string_builder.AppendLine("ns_time_from, ")
          _sql_string_builder.AppendLine("ns_time_to,")
          _sql_string_builder.AppendLine("ns_second_time_enabled,")
          _sql_string_builder.AppendLine("ns_second_time_from,")
          _sql_string_builder.AppendLine("ns_second_time_to) ")
          _sql_string_builder.AppendLine(" VALUES (")
          _sql_string_builder.AppendLine("@NS_ID_NOTIFICATION, ")
          _sql_string_builder.AppendLine("@ns_date_from,")
          _sql_string_builder.AppendLine("@ns_date_to, ")
          _sql_string_builder.AppendLine("@ns_weekdays,")
          _sql_string_builder.AppendLine("@ns_time_from, ")
          _sql_string_builder.AppendLine("@ns_time_to,")
          _sql_string_builder.AppendLine("@ns_second_time_enabled,")
          _sql_string_builder.AppendLine("@ns_second_time_from,")
          _sql_string_builder.AppendLine("@ns_second_time_to) ")

          Using _cmd As SqlCommand = New SqlCommand(_sql_string_builder.ToString(), _sql_trx.Connection, _sql_trx)

            _cmd.Parameters.Add("@NS_ID_NOTIFICATION", SqlDbType.BigInt).Value = m_id
            _cmd.Parameters.Add("@ns_date_from", SqlDbType.DateTime).Value = m_date_from
            _cmd.Parameters.Add("@ns_date_to", SqlDbType.DateTime).Value = m_date_to
            _cmd.Parameters.Add("@ns_weekdays", SqlDbType.SmallInt).Value = m_weekdays
            _cmd.Parameters.Add("@ns_time_from", SqlDbType.BigInt).Value = m_time_from
            _cmd.Parameters.Add("@ns_time_to", SqlDbType.BigInt).Value = m_time_to
            _cmd.Parameters.Add("@ns_second_time_enabled", SqlDbType.Bit).Value = False
            _cmd.Parameters.Add("@ns_second_time_from", SqlDbType.BigInt).Value = 0
            _cmd.Parameters.Add("@ns_second_time_to", SqlDbType.BigInt).Value = 0

            _cmd.ExecuteNonQuery()
          End Using

        Else

          _sql_string_builder = New StringBuilder()
          _sql_string_builder.AppendLine("UPDATE MAPP_NOTIFICATION_SCHEDULE SET ")
          _sql_string_builder.AppendLine("ns_date_from=@ns_date_from,")
          _sql_string_builder.AppendLine("ns_date_to=@ns_date_to, ")
          _sql_string_builder.AppendLine("ns_weekdays=@ns_weekdays,")
          _sql_string_builder.AppendLine("ns_time_from=@ns_time_from, ")
          _sql_string_builder.AppendLine("ns_time_to=@ns_time_to,")
          _sql_string_builder.AppendLine("ns_second_time_enabled=@ns_second_time_enabled,")
          _sql_string_builder.AppendLine("ns_second_time_from=@ns_second_time_from,")
          _sql_string_builder.AppendLine("ns_second_time_to=@ns_second_time_to ")

          _sql_string_builder.AppendLine(" WHERE NS_ID=@NS_ID ")

          Using _cmd As SqlCommand = New SqlCommand(_sql_string_builder.ToString(), _sql_trx.Connection, _sql_trx)

            _cmd.Parameters.Add("@NS_ID", SqlDbType.BigInt).Value = m_id_schedule
            _cmd.Parameters.Add("@ns_date_from", SqlDbType.DateTime).Value = m_date_from
            _cmd.Parameters.Add("@ns_date_to", SqlDbType.DateTime).Value = m_date_to
            _cmd.Parameters.Add("@ns_weekdays", SqlDbType.SmallInt).Value = m_weekdays
            _cmd.Parameters.Add("@ns_time_from", SqlDbType.BigInt).Value = m_time_from
            _cmd.Parameters.Add("@ns_time_to", SqlDbType.BigInt).Value = m_time_to
            _cmd.Parameters.Add("@ns_second_time_enabled", SqlDbType.Bit).Value = False
            _cmd.Parameters.Add("@ns_second_time_from", SqlDbType.BigInt).Value = 0
            _cmd.Parameters.Add("@ns_second_time_to", SqlDbType.BigInt).Value = 0

            _cmd.ExecuteNonQuery()
          End Using

        End If


        If m_id_segmentation = 0 Then

          _sql_string_builder = New StringBuilder()
          _sql_string_builder.AppendLine("INSERT INTO MAPP_NOTIFICATION_SEGMENTATION(")
          _sql_string_builder.AppendLine("NS_ID_NOTIFICATION,")

          _sql_string_builder.AppendLine("NS_ID_ACCOUNT,")
          _sql_string_builder.AppendLine("NS_IDS_ACCOUNTS,")

          _sql_string_builder.AppendLine("NS_GENDER_FILTER,")
          _sql_string_builder.AppendLine("NS_BIRTHDAY_FILTER,")
          _sql_string_builder.AppendLine("NS_CREATED_ACCOUNT_FILTER,")
          _sql_string_builder.AppendLine("NS_LEVEL_FILTER,")
          _sql_string_builder.AppendLine("NS_IS_VIP,")
          _sql_string_builder.AppendLine("NS_FREQ_FILTER_LAST_DAYS,")
          _sql_string_builder.AppendLine("NS_FREQ_FILTER_MIN_DAYS,")
          _sql_string_builder.AppendLine("NS_FREQ_FILTER_MIN_CASH_IN ")

          _sql_string_builder.AppendLine(") VALUES (")

          _sql_string_builder.AppendLine("@NS_ID_NOTIFICATION,")
          _sql_string_builder.AppendLine("@NS_ID_ACCOUNT,")
          _sql_string_builder.AppendLine("@NS_IDS_ACCOUNTS,")

          _sql_string_builder.AppendLine("@NS_GENDER_FILTER,")
          _sql_string_builder.AppendLine("@NS_BIRTHDAY_FILTER,")
          _sql_string_builder.AppendLine("@NS_CREATED_ACCOUNT_FILTER,")
          _sql_string_builder.AppendLine("@NS_LEVEL_FILTER,")
          _sql_string_builder.AppendLine("@NS_IS_VIP,")
          _sql_string_builder.AppendLine("@NS_FREQ_FILTER_LAST_DAYS,")
          _sql_string_builder.AppendLine("@NS_FREQ_FILTER_MIN_DAYS,")
          _sql_string_builder.AppendLine("@NS_FREQ_FILTER_MIN_CASH_IN ")

          _sql_string_builder.AppendLine(")")

          _sql_string_builder.AppendLine("        SET @Id    = SCOPE_IDENTITY();   ")

          Using _cmd As SqlCommand = New SqlCommand(_sql_string_builder.ToString(), _sql_trx.Connection, _sql_trx)

            _cmd.Parameters.Add("@NS_ID_NOTIFICATION", SqlDbType.BigInt).Value = m_id

            'BY FILTERS
            _cmd.Parameters.Add("@NS_GENDER_FILTER", SqlDbType.Int).Value = m_gender_filter
            _cmd.Parameters.Add("@NS_BIRTHDAY_FILTER", SqlDbType.Int).Value = m_birthday_filter
            _cmd.Parameters.Add("@NS_CREATED_ACCOUNT_FILTER", SqlDbType.Int).Value = m_created_account_filter
            _cmd.Parameters.Add("@NS_LEVEL_FILTER", SqlDbType.Int).Value = m_level_filter
            _cmd.Parameters.Add("@NS_FREQ_FILTER_LAST_DAYS", SqlDbType.Int).Value = m_freq_filter_last_days
            _cmd.Parameters.Add("@NS_FREQ_FILTER_MIN_DAYS", SqlDbType.Int).Value = m_freq_filter_min_days
            _cmd.Parameters.Add("@NS_FREQ_FILTER_MIN_CASH_IN", SqlDbType.Money).Value = m_freq_filter_min_cash_in
            _cmd.Parameters.Add("@NS_IS_VIP", SqlDbType.Bit).Value = m_is_vip

            'BY CUSTOMER
            _cmd.Parameters.Add("@NS_ID_ACCOUNT", SqlDbType.BigInt).Value = m_account_id

            'BY CUSTOMER GROUP
            _cmd.Parameters.Add("@NS_IDS_ACCOUNTS", SqlDbType.NVarChar).Value = m_accounts_ids


            _param_id = _cmd.Parameters.Add("@Id", SqlDbType.Int)
            _param_id.Direction = ParameterDirection.Output

            _row_affected = _cmd.ExecuteNonQuery()

            If _row_affected <> 1 Or IsDBNull(_param_id.Value) Then
              Return ENUM_STATUS.STATUS_ERROR
            End If

            Me.m_id_segmentation = _param_id.Value

          End Using

        Else

          _sql_string_builder = New StringBuilder()
          _sql_string_builder.AppendLine("UPDATE MAPP_NOTIFICATION_SEGMENTATION SET ")

          _sql_string_builder.AppendLine("NS_ID_ACCOUNT=@NS_ID_ACCOUNT,")
          _sql_string_builder.AppendLine("NS_IDS_ACCOUNTS=@NS_IDS_ACCOUNTS,")

          _sql_string_builder.AppendLine("NS_GENDER_FILTER=@NS_GENDER_FILTER,")
          _sql_string_builder.AppendLine("NS_BIRTHDAY_FILTER=@NS_BIRTHDAY_FILTER,")
          _sql_string_builder.AppendLine("NS_CREATED_ACCOUNT_FILTER=@NS_CREATED_ACCOUNT_FILTER,")
          _sql_string_builder.AppendLine("NS_LEVEL_FILTER=@NS_LEVEL_FILTER,")
          _sql_string_builder.AppendLine("NS_IS_VIP=@NS_IS_VIP,")
          _sql_string_builder.AppendLine("NS_FREQ_FILTER_LAST_DAYS=@NS_FREQ_FILTER_LAST_DAYS,")
          _sql_string_builder.AppendLine("NS_FREQ_FILTER_MIN_DAYS=@NS_FREQ_FILTER_MIN_DAYS,")
          _sql_string_builder.AppendLine("NS_FREQ_FILTER_MIN_CASH_IN=@NS_FREQ_FILTER_MIN_CASH_IN ")

          _sql_string_builder.AppendLine(" WHERE NS_ID=@NS_ID ")

          Using _cmd As SqlCommand = New SqlCommand(_sql_string_builder.ToString(), _sql_trx.Connection, _sql_trx)

            _cmd.Parameters.Add("@NS_ID", SqlDbType.BigInt).Value = m_id_segmentation

            'BY FILTERS
            _cmd.Parameters.Add("@NS_GENDER_FILTER", SqlDbType.Int).Value = m_gender_filter
            _cmd.Parameters.Add("@NS_BIRTHDAY_FILTER", SqlDbType.Int).Value = m_birthday_filter
            _cmd.Parameters.Add("@NS_CREATED_ACCOUNT_FILTER", SqlDbType.Int).Value = m_created_account_filter
            _cmd.Parameters.Add("@NS_LEVEL_FILTER", SqlDbType.Int).Value = m_level_filter
            _cmd.Parameters.Add("@NS_FREQ_FILTER_LAST_DAYS", SqlDbType.Int).Value = m_freq_filter_last_days
            _cmd.Parameters.Add("@NS_FREQ_FILTER_MIN_DAYS", SqlDbType.Int).Value = m_freq_filter_min_days
            _cmd.Parameters.Add("@NS_FREQ_FILTER_MIN_CASH_IN", SqlDbType.Money).Value = m_freq_filter_min_cash_in
            _cmd.Parameters.Add("@NS_IS_VIP", SqlDbType.Bit).Value = m_is_vip

            'BY CUSTOMER
            _cmd.Parameters.Add("@NS_ID_ACCOUNT", SqlDbType.BigInt).Value = m_account_id

            'BY CUSTOMER GROUP
            _cmd.Parameters.Add("@NS_IDS_ACCOUNTS", SqlDbType.NVarChar).Value = m_accounts_ids

            _cmd.ExecuteNonQuery()
          End Using

        End If

        ' --- Ok
        _db_trx.Commit()

      End Using



      Return ENUM_STATUS.STATUS_OK
    Catch ex As Exception
      Return ENUM_STATUS.STATUS_ERROR
    End Try

    Return Nothing
  End Function

  Public Overrides Function Duplicate() As CLASS_BASE

    Dim _notification As CLASS_WINUP_NOTIFICATIONS

    _notification = New CLASS_WINUP_NOTIFICATIONS

    With _notification

      .Title = m_title
      .Message = m_message
      .Target = m_target
      .Target_Type = m_type_target
      .IsUrl = m_isurl
      .Status = m_status
      .Schedule = m_schedule
      .Segmentation = m_segmentation

      .DateFrom = m_date_from
      .DateTo = m_date_to
      .Weekdays = m_weekdays
      .TimeFrom = m_time_from
      .TimeTo = m_time_to

      .GenderFilter = m_gender_filter

      .BirthdayFilter = m_birthday_filter
      .AgeFilter = m_age_filter
      .BirthdayAgeFrom = Me.m_birthday_age_from
      .BirthdayAgeTo = Me.m_birthday_age_to

      .CreatedAccountFilter = Me.m_created_account_filter
      .CreatedAccountWorkingDaysIncluded = Me.m_created_account_working_days_included
      .CreatedAccountAnniversaryYearFrom = Me.m_created_account_anniversary_year_from
      .CreatedAccountAnniversaryYearTo = Me.m_created_account_anniversary_year_to

      .LevelFilter = Me.m_level_filter

      .FreqFilterLastDays = Me.m_freq_filter_last_days
      .FreqFilterMinDays = Me.m_freq_filter_min_days
      .FreqFilterMinCashIn = Me.m_freq_filter_min_cash_in

      .IsVip = m_is_vip

      .AccountId = Me.m_account_id
      .AccountsIds = Me.m_accounts_ids

      .Image = Me.m_image
      .ImageId = Me.ImageId

    End With

    Return _notification
  End Function

#End Region

#Region "Private Functions"

#End Region

#Region "Public Functions"

  Public Function CheckNotificationToSent(ByVal IdNotification As Integer) As Boolean

    Dim _sql_trx As SqlTransaction
    Dim _sql_string_builder As StringBuilder
    Dim _command As SqlCommand

    _sql_string_builder = New StringBuilder()

    Try
      Using _db_trx As DB_TRX = New DB_TRX()
        _sql_trx = _db_trx.SqlTransaction

        _sql_string_builder.AppendLine(" UPDATE mapp_notification SET ")
        _sql_string_builder.AppendLine(" NO_SENT=1 ")
        _sql_string_builder.AppendLine(" WHERE no_id_notification= @id")

        _command = New SqlCommand( _
            _sql_string_builder.ToString(), _
            _sql_trx.Connection, _sql_trx)

        _command.Parameters.Add("@id", SqlDbType.BigInt).Value = IdNotification

        _command.ExecuteNonQuery()

        _sql_trx.Commit()
      End Using

      Return True
    Catch ex As Exception
      Return False
    End Try

  End Function

  Public Function GetSections() As Dictionary(Of Integer, String)

    Dim _list As New Dictionary(Of Integer, String)
    Dim _sql_trx As SqlTransaction
    Dim _sql_str_select As String
    Dim _command As SqlCommand
    Dim _reader As SqlDataReader

    Using _db_trx As DB_TRX = New DB_TRX()
      _sql_trx = _db_trx.SqlTransaction

      _sql_str_select = "SELECT SS_SECTION_SCHEMA_ID, SS_NAME FROM MAPP_SECTION_SCHEMA"

      _command = New SqlCommand( _
          _sql_str_select, _
          _sql_trx.Connection, _sql_trx)

      _reader = _command.ExecuteReader()

      If _reader.HasRows Then
        _list.Add(0, "")
        Do While _reader.Read()
          _list.Add(_reader.GetInt64(0), _reader.GetString(1))
        Loop
      End If

      _reader.Close()

    End Using

    Return _list

  End Function

  Public Function GetTypeSections(ByVal IdSection As Integer) As Dictionary(Of Integer, String)

    Dim _list As New Dictionary(Of Integer, String)
    Dim _sql_trx As SqlTransaction
    Dim _sql_str_select As String
    Dim _command As SqlCommand
    Dim _reader As SqlDataReader

    Using _db_trx As DB_TRX = New DB_TRX()
      _sql_trx = _db_trx.SqlTransaction

      _sql_str_select = "SELECT ad_id, ad_winup_title, ad_name FROM wkt_ads WHERE ad_winup_enabled = 1 AND ad_enabled = 1 AND  ad_target_schemaId = " & IdSection

      _command = New SqlCommand( _
          _sql_str_select, _
          _sql_trx.Connection, _sql_trx)

      _reader = _command.ExecuteReader()

      If _reader.HasRows Then
        _list.Add(0, "")
        Do While _reader.Read()
          Dim _description As String = ""
          If (Not IsDBNull(_reader(1))) Then
            _description = _reader.GetString(1)
          ElseIf (Not IsDBNull(_reader(2))) Then
            _description = _reader.GetString(2)
          End If
          _list.Add(_reader.GetInt64(0), _description)
        Loop
      End If

      _reader.Close()

    End Using

    Return _list

  End Function


  Public Function GetAccountsByFilter(ByVal SqlWhere As String) As DataTable
    Dim _sql_trx As SqlTransaction
    Dim _sql_str_select As String
    Dim _command As SqlCommand
    Dim _table As DataTable
    Dim _sql_data_adapter As SqlDataAdapter

    _table = New DataTable

    Using _db_trx As DB_TRX = New DB_TRX()

      _sql_trx = _db_trx.SqlTransaction

      _sql_str_select = "SELECT ac_account_id, ac_holder_name, ac_holder_email_01, ac_holder_birth_date FROM accounts"

      If SqlWhere <> "" Then
        _sql_str_select = _sql_str_select & " WHERE " & SqlWhere
      End If

      _command = New SqlCommand( _
          _sql_str_select, _
          _sql_trx.Connection, _sql_trx)

      _sql_data_adapter = New SqlDataAdapter(_command)

      _sql_data_adapter.Fill(_table)

    End Using

    Return _table

  End Function

  Public Function GetAccountsByIds(ByVal Ids As String) As DataTable
    Dim _sql_trx As SqlTransaction
    Dim _sql_str_select As String
    Dim _command As SqlCommand
    Dim _table As DataTable
    Dim _sql_data_adapter As SqlDataAdapter

    _table = New DataTable

    Using _db_trx As DB_TRX = New DB_TRX()

      _sql_trx = _db_trx.SqlTransaction

      _sql_str_select = "SELECT ac_account_id as account_id, ac_holder_name as account_holder_name FROM accounts WHERE ac_account_id IN (" & Ids & ")"

      _command = New SqlCommand( _
          _sql_str_select, _
          _sql_trx.Connection, _sql_trx)

      _sql_data_adapter = New SqlDataAdapter(_command)

      _sql_data_adapter.Fill(_table)

    End Using

    Return _table

  End Function

  Private Function InsertImage(ByVal Trx As SqlTransaction) As Integer

    Dim _sql_str_insert As String
    Dim _command As SqlCommand
    Dim _param_id As SqlParameter
    Dim _row_affected As Integer

    _sql_str_insert = "INSERT INTO  mapp_images (im_content_type,im_created_date,im_file_name,im_name,im_updated_date,im_data) VALUES "
    _sql_str_insert += "(@type,@date,@name,@name,@date,@data) "
    _sql_str_insert += "        SET @Id    = SCOPE_IDENTITY();   "

    _command = New SqlCommand( _
        _sql_str_insert, _
        Trx.Connection, Trx)

    _command.Parameters.Add("@type", SqlDbType.NVarChar).Value = Me.m_image_content_type
    _command.Parameters.Add("@date", SqlDbType.DateTime).Value = Date.Now
    _command.Parameters.Add("@name", SqlDbType.NVarChar).Value = Me.m_image_file_name
    _command.Parameters.Add("@data", SqlDbType.NVarChar).Value = Me.m_image_data

    _param_id = _command.Parameters.Add("@Id", SqlDbType.Int)
    _param_id.Direction = ParameterDirection.Output

    _row_affected = _command.ExecuteNonQuery()

    If _row_affected <> 1 Or IsDBNull(_param_id.Value) Then
      Return 0
    End If

    Me.m_image_id = _param_id.Value

    Return Me.m_image_id

  End Function

  Private Function UpdateImage(ByVal Trx As SqlTransaction) As Integer

    Dim _sql_str_insert As String
    Dim _command As SqlCommand
    Dim _row_affected As Integer

    _sql_str_insert = "UPDATE  mapp_images SET im_content_type=@type,im_file_name=@name,im_name=@name,im_updated_date=@date,im_data=@data "

    _sql_str_insert += " WHERE im_image_id = @id "

    _command = New SqlCommand( _
        _sql_str_insert, _
        Trx.Connection, Trx)

    _command.Parameters.Add("@id", SqlDbType.BigInt).Value = Me.m_image_id
    _command.Parameters.Add("@type", SqlDbType.NVarChar).Value = Me.m_image_content_type
    _command.Parameters.Add("@date", SqlDbType.DateTime).Value = Date.Now
    _command.Parameters.Add("@name", SqlDbType.NVarChar).Value = Me.m_image_file_name
    _command.Parameters.Add("@data", SqlDbType.NVarChar).Value = Me.m_image_data

    _row_affected = _command.ExecuteNonQuery()

    Return _row_affected

  End Function

  Private Function GetImage() As Boolean

    Dim _url As String
    Dim _api As CLS_API
    Dim _imageTmpResponse As JObject

    _url = WSI.Common.GeneralParam.GetString("WinUP", "Backend.Url") + "/image/getbyid/" + Me.ImageId.ToString()
    _api = New CLS_API(_url)
    _imageTmpResponse = JObject.Parse(_api.sanitizeData(_api.getData()))
    If Not _imageTmpResponse.Item("imageData") = "" Then
      Dim imgData As Byte() = _imageTmpResponse.Item("imageData")
      Using ms As New MemoryStream(imgData)
        Me.Image = System.Drawing.Image.FromStream(ms)
      End Using
    Else
      Me.Image = Nothing
    End If

    If Not _imageTmpResponse.Item("fileName") = "" Then
      Me.ImageFileName = _imageTmpResponse.Item("fileName")
    End If

  End Function

  Private Function SaveImage() As Boolean

    Dim _url As String
    Dim _api As CLS_API
    Dim response As String
    Dim imgBackgoundData As Byte()
    Dim fileName As String

    Try

      If Not IsNothing(Me.Image) Then

        Dim img As New ImageConverter()
        imgBackgoundData = img.ConvertTo(Me.Image, GetType(Byte()))
        fileName = Me.Title + GetImageType(Me.Image)

        Dim jObjBackground As New JObject
        jObjBackground.Add("Name", Me.Title)
        jObjBackground.Add("fileName", fileName)
        jObjBackground.Add("ContentType", GetMimeType(fileName))
        jObjBackground.Add("Data", imgBackgoundData)

        If Me.ImageId = 0 Then
          _url = WSI.Common.GeneralParam.GetString("WinUP", "Backend.Url") + "/image/SaveImage"
          _api = New CLS_API(_url)
          response = _api.postData(Encoding.UTF8.GetBytes(jObjBackground.ToString()))
        Else
          jObjBackground.Add("ImageId", m_image_id)
          _url = WSI.Common.GeneralParam.GetString("WinUP", "Backend.Url") + "/image/" + m_image_id.ToString()
          _api = New CLS_API(_url)
          response = _api.putData(Encoding.UTF8.GetBytes(jObjBackground.ToString()))
        End If

        Dim jresponse As JObject = JObject.Parse(response)

        Me.ImageId = jresponse.Item("imageId")

        Return True
      End If
    Catch ex As Exception
      Return False
    End Try

  End Function

  Private Function DeleteImage() As Boolean

    Dim _url As String
    Dim _api As CLS_API
    Dim response As String

    Try

      _url = WSI.Common.GeneralParam.GetString("WinUP", "Backend.Url") + "/image/" + m_image_id.ToString()
      _api = New CLS_API(_url)

      response = _api.deleteData()

      Return True

    Catch ex As Exception
      Return False
    End Try

  End Function ' DeleteAds

#End Region

End Class
