'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : cls_gaming_table.vb
'
' DESCRIPTION : gaming table class for user design
'              
' REVISION HISTORY :
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 21-MAY-2014  DHA    Initial version
'-------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports System.Text
Imports WSI.Common

Public Class CLASS_GAMING_TABLE_DESING
  Inherits CLASS_BASE

#Region " Structures "

  Public Class TYPE_GAMING_TABLE_DESIGN

#Region " Constants "

#End Region

#Region " Enums "

#End Region ' Enums 

#Region " Members "

    Public gaming_table_id As Integer
    Public gaming_table_name As String
    Public table_num_seats As Integer
    Public table_design_xml As String
    Public gaming_table_provider_id As Nullable(Of Integer)
    Public gaming_table_bank_id As Integer
    Public gaming_table_floor_id As String
    Public session_opened As Boolean = False
    Public updated As Boolean = False

#End Region

#Region " Properties "

    Public Property GamingTableID() As Integer
      Get
        Return Me.gaming_table_id
      End Get

      Set(ByVal Value As Integer)
        Me.gaming_table_id = Value
      End Set
    End Property

    Public Property GamingTableName() As String
      Get
        Return Me.gaming_table_name
      End Get

      Set(ByVal Value As String)
        Me.gaming_table_name = Value
      End Set
    End Property

    Public Property NumSeats() As Integer
      Get
        Return Me.table_num_seats
      End Get

      Set(ByVal Value As Integer)
        Me.table_num_seats = Value
      End Set
    End Property

    Public Property DesingXml() As String
      Get
        Return Me.table_design_xml
      End Get

      Set(ByVal Value As String)
        Me.table_design_xml = Value
      End Set
    End Property

    Public Property GamingTableProviderID() As Nullable(Of Integer)
      Get
        Return Me.gaming_table_provider_id
      End Get

      Set(ByVal Value As Nullable(Of Integer))
        Me.gaming_table_provider_id = Value
      End Set
    End Property

    Public Property GamingTableBankID() As Integer
      Get
        Return Me.gaming_table_bank_id
      End Get

      Set(ByVal Value As Integer)
        Me.gaming_table_bank_id = Value
      End Set
    End Property

    Public Property GamingTableFloorID() As String
      Get
        Return Me.gaming_table_floor_id
      End Get

      Set(ByVal Value As String)
        Me.gaming_table_floor_id = Value
      End Set
    End Property

#End Region ' Properties 

#Region " Public Functions "

    ' PURPOSE: Clone/duplicate the object
    '
    '    - INPUT:
    '
    '    - OUTPUT:
    '
    ' RETURNS:
    '    - TYPE_POINTS_TO_CREDITS : New instance copy of the actual
    Public Function Clone() As TYPE_GAMING_TABLE_DESIGN
      Dim _clone As TYPE_GAMING_TABLE_DESIGN

      _clone = New TYPE_GAMING_TABLE_DESIGN()
      _clone.GamingTableID = Me.GamingTableID
      _clone.GamingTableName = Me.GamingTableName
      _clone.NumSeats = Me.NumSeats
      _clone.DesingXml = Me.DesingXml
      _clone.GamingTableProviderID = Me.GamingTableProviderID

      Return _clone

    End Function 'Clone

    ' PURPOSE: Audits the class
    '
    '    - INPUT:
    '       - TableNumber: Number of the tabpage that is being audited
    '       - Auditor: Auditor object.
    '
    '    - OUTPUT:
    '
    ' RETURNS:
    '    - 
    Public Sub AuditGamingTableDesing(ByVal TableNumber As String, ByRef Auditor As CLASS_AUDITOR_DATA)

      Dim _xml As System.Xml.XmlDocument
      Dim _xml_nodes As System.Xml.XmlNodeList
      Dim _xml_node As System.Xml.XmlNode
      Dim _xml_seat As System.Xml.XmlNode
      Dim _max_seats As Integer
      Dim _position_seat As Integer
      Dim _empty_field As String
      Dim _property As KeyValuePair(Of String, String)

      _max_seats = GeneralParam.GetInt32("GamingTables", "Seats.MaxAllowedSeats", 10)
      _xml = New System.Xml.XmlDocument
      _position_seat = 0
      _empty_field = "---"

      Call Auditor.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(4997), "")
      Call Auditor.SetField(0, Me.NumSeats, Me.GamingTableName & " - " + GLB_NLS_GUI_PLAYER_TRACKING.GetString(4994))

      If Me.DesingXml Is Nothing Or String.IsNullOrEmpty(Me.DesingXml) Then
        For Each _property In UtilsDesign.GetSerializableProperties(GetType(GamingTableProperties))
          Call Auditor.SetField(0, _empty_field, Me.GamingTableName & " - " & _property.Value)
        Next
        For _idx As Integer = 0 To _max_seats - 1
          _position_seat = _position_seat + 1
          For Each _property In UtilsDesign.GetSerializableProperties(GetType(GamingSeatProperties))
            Call Auditor.SetField(0, _empty_field, Me.GamingTableName & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4998) & _position_seat & "." & _property.Value)
          Next
        Next
      Else
        _xml.LoadXml(Me.DesingXml)
        ' Audit table desing
        _xml_nodes = _xml.GetElementsByTagName("GamingTableProperties")

        If _xml_nodes.Count > 0 Then
          For Each _xml_node In _xml_nodes(0).ChildNodes
            Call Auditor.SetField(0, _xml_node.InnerText, Me.GamingTableName & " - " & UtilsDesign.GetPropertyResource(_xml_node.Name))
          Next
        Else
          For Each _property In UtilsDesign.GetSerializableProperties(GetType(GamingTableProperties))
            Call Auditor.SetField(0, _empty_field, Me.GamingTableName & " - " & _property.Value)
          Next
        End If

        ' Audit seats
        _xml_nodes = _xml.GetElementsByTagName("GamingSeatProperties")

        For Each _xml_seat In _xml_nodes
          _position_seat = _position_seat + 1
          For Each _xml_node In _xml_seat.ChildNodes
            Call Auditor.SetField(0, _xml_node.InnerText, Me.GamingTableName & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4998) & _position_seat & "." & UtilsDesign.GetPropertyResource(_xml_node.Name))
          Next
        Next

        For _idx As Integer = 0 To _max_seats - _xml_nodes.Count - 1
          _position_seat = _position_seat + 1
          For Each _property In UtilsDesign.GetSerializableProperties(GetType(GamingSeatProperties))
            Call Auditor.SetField(0, _empty_field, Me.GamingTableName & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4998) & _position_seat & "." & _property.Value)
          Next
        Next

      End If
    End Sub ' AuditGamingTableDesing


#End Region ' Public Functions

  End Class ' TYPE_GAMING_TABLE_DESIGN

#End Region ' Structures 

#Region "Enum"

  Enum GAMING_TABLE_DESING_MEMBERS
    GTD_GAMING_TABLE_ID = 0
    GTD_GAMING_TABLE_NAME = 1
    GTD_NUM_SEATS = 2
    GTD_DESING_XML = 3
    GTD_GAMING_TABLE_PROVIDER_ID = 4
    GTD_GAMING_TABLE_BANK_ID = 5
    GTD_GAMING_TABLE_FLOOR_ID = 6
  End Enum

#End Region ' Enum

#Region " Members "

  Private m_gaming_table_design As List(Of TYPE_GAMING_TABLE_DESIGN) = New List(Of TYPE_GAMING_TABLE_DESIGN)

#End Region 'Members

#Region " Properties "

  Public Property GamingTablesDesingList() As List(Of TYPE_GAMING_TABLE_DESIGN)
    Set(ByVal value As List(Of TYPE_GAMING_TABLE_DESIGN))
      m_gaming_table_design = value
    End Set
    Get
      Return m_gaming_table_design
    End Get
  End Property


#End Region ' Properties 

#Region " Overrides functions "

#Region " Public Functions "

  Public Function FindItemByGamingTableId(ByVal Id As Integer) As TYPE_GAMING_TABLE_DESIGN

    Dim _item As TYPE_GAMING_TABLE_DESIGN

    For Each _item In m_gaming_table_design
      If Id = _item.GamingTableID Then
        Return _item
      End If
    Next

    Return Nothing
  End Function

#End Region ' Public Functions
  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA
    Dim _auditor As CLASS_AUDITOR_DATA
    Dim _idx_table As Integer

    _auditor = New CLASS_AUDITOR_DATA(AUDIT_CODE_GAMING_TABLES)

    For _idx_table = 0 To Me.GamingTablesDesingList.Count - 1
      Call Me.GamingTablesDesingList(_idx_table).AuditGamingTableDesing(_idx_table.ToString, _auditor)
    Next

    Return _auditor

  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE : Deletes the given object from the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '
  ' NOTES :
  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

  End Function

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

  End Function

  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _rc As ENUM_STATUS

    _rc = ReadGamingTable()

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select
  End Function

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _rc As Integer

    ' Update points to credit
    _rc = UpdateGamingTableDesign(Me.GamingTablesDesingList, SqlCtx)

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
        Return ENUM_STATUS.STATUS_DUPLICATE_KEY

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select
  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE: Returns a duplicate of the given object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim _clone As CLASS_GAMING_TABLE_DESING
    Dim _item_cloned As TYPE_GAMING_TABLE_DESIGN

    _clone = New CLASS_GAMING_TABLE_DESING()

    _clone.GamingTablesDesingList = New List(Of TYPE_GAMING_TABLE_DESIGN)

    For Each _item As TYPE_GAMING_TABLE_DESIGN In Me.GamingTablesDesingList
      _item_cloned = _item.Clone()
      _clone.GamingTablesDesingList.Add(_item_cloned)
    Next

    Return _clone
  End Function ' Duplicate

#End Region ' Overrides functions 

#Region "Private Functions "
  '----------------------------------------------------------------------------
  ' PURPOSE : Reads a Gaming Table object from DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - SqlTrans
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_NOT_FOUND
  '     - STATUS_ERROR
  '
  ' NOTES :

  Private Function ReadGamingTable() As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _sb As StringBuilder
    Dim _sql_trx As SqlClient.SqlTransaction
    Dim _item As TYPE_GAMING_TABLE_DESIGN

    _sb = New StringBuilder()

    _sb.AppendLine("SELECT   GT_GAMING_TABLE_ID			 ")
    _sb.AppendLine("	     , GT_NAME                 ")
    _sb.AppendLine("	     , GT_NUM_SEATS          	 ")
    _sb.AppendLine("       , GT_DESIGN_XML           ")
    _sb.AppendLine("       , GT_PROVIDER_ID          ")
    _sb.AppendLine("       , GT_BANK_ID              ")
    _sb.AppendLine("       , GT_FLOOR_ID             ")
    _sb.AppendLine("  FROM   GAMING_TABLES			     ")

    Try

      Using _db_trx As New DB_TRX()

        _sql_trx = _db_trx.SqlTransaction

        Using _sql_cmd As New SqlCommand(_sb.ToString, _sql_trx.Connection, _sql_trx)

          Using _reader As SqlDataReader = _sql_cmd.ExecuteReader()

            While _reader.Read()
              _item = New TYPE_GAMING_TABLE_DESIGN()
              _item.gaming_table_id = _reader.GetInt32(GAMING_TABLE_DESING_MEMBERS.GTD_GAMING_TABLE_ID)
              _item.gaming_table_name = _reader.GetString(GAMING_TABLE_DESING_MEMBERS.GTD_GAMING_TABLE_NAME)

              If Not _reader.IsDBNull(GAMING_TABLE_DESING_MEMBERS.GTD_GAMING_TABLE_PROVIDER_ID) Then
                _item.GamingTableProviderID = _reader.GetInt32(GAMING_TABLE_DESING_MEMBERS.GTD_GAMING_TABLE_PROVIDER_ID)
              Else
                _item.GamingTableProviderID = Nothing
              End If
              If Not _reader.IsDBNull(GAMING_TABLE_DESING_MEMBERS.GTD_GAMING_TABLE_BANK_ID) Then
                _item.GamingTableBankID = _reader.GetInt32(GAMING_TABLE_DESING_MEMBERS.GTD_GAMING_TABLE_BANK_ID)
              Else
                _item.GamingTableBankID = Nothing
              End If
              If Not _reader.IsDBNull(GAMING_TABLE_DESING_MEMBERS.GTD_GAMING_TABLE_FLOOR_ID) Then
                _item.GamingTableFloorID = _reader.GetString(GAMING_TABLE_DESING_MEMBERS.GTD_GAMING_TABLE_FLOOR_ID)
              Else
                _item.GamingTableFloorID = Nothing
              End If
              If Not _reader.IsDBNull(GAMING_TABLE_DESING_MEMBERS.GTD_NUM_SEATS) Then
                _item.table_num_seats = _reader.GetInt32(GAMING_TABLE_DESING_MEMBERS.GTD_NUM_SEATS)
              End If
              If Not _reader.IsDBNull(GAMING_TABLE_DESING_MEMBERS.GTD_DESING_XML) Then
                _item.table_design_xml = _reader.GetString(GAMING_TABLE_DESING_MEMBERS.GTD_DESING_XML)
              End If


              Me.GamingTablesDesingList.Add(_item)

            End While

            _reader.Close()

          End Using

        End Using

      End Using

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

    Catch ex As Exception

      Log.Exception(ex)
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
  End Function ' ReadGamingTable

  '----------------------------------------------------------------------------
  ' PURPOSE: Updates the given object to the database.
  '
  ' PARAMS:
  '   - INPUT: 
  '       - Items: List of objects that have to be treated
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES:
  Private Function UpdateGamingTableDesign(ByVal Items As List(Of TYPE_GAMING_TABLE_DESIGN), _
                                         ByVal Context As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _item As TYPE_GAMING_TABLE_DESIGN
    Dim _sb As StringBuilder
    Dim _gaming_table_session As GamingTablesSessions

    Try
      _gaming_table_session = Nothing

      For Each _item In Items

        Using _db_trx As New WSI.Common.DB_TRX()

          If _item.updated Then
            Try

              _item.updated = False
              ' Validate if exist a opened session for the gaming table
              If GamingTablesSessions.GetOpenedSession(_item.GamingTableID, _gaming_table_session, _db_trx.SqlTransaction) Then

                If _gaming_table_session Is Nothing Then

                  _sb = New StringBuilder()
                  _sb.AppendLine(" UPDATE   GAMING_TABLES                                  ")
                  _sb.AppendLine("    SET   GT_DESIGN_XML            =   @pXmlDesign       ")
                  _sb.AppendLine("        , GT_NUM_SEATS             =   @pNumSeats        ")
                  _sb.AppendLine("  WHERE   GT_GAMING_TABLE_ID       =   @pGamingTableId   ")

                  Using _sql_cmd As New SqlCommand(_sb.ToString, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
                    _sql_cmd.Parameters.Add("@pGamingTableId", SqlDbType.Int).Value = _item.GamingTableID
                    _sql_cmd.Parameters.Add("@pNumSeats", SqlDbType.Int).Value = _item.NumSeats

                    If _item.DesingXml Is Nothing Then
                      _sql_cmd.Parameters.Add("@pXmlDesign", SqlDbType.NVarChar).Value = DBNull.Value
                    Else
                      _sql_cmd.Parameters.Add("@pXmlDesign", SqlDbType.NVarChar).Value = _item.DesingXml
                    End If

                    If _sql_cmd.ExecuteNonQuery() = 1 Then
                      ' Update Seats
                      If GamingTableBusinessLogic.InsertUpdateTableSeats(_item.GamingTableID, _item.NumSeats, _item.DesingXml, _item.GamingTableProviderID, _item.GamingTableBankID, _item.GamingTableFloorID, _db_trx.SqlTransaction) Then
                        _item.updated = True
                      End If
                    End If

                  End Using

                End If
              End If

            Catch ex As Exception

            End Try
          End If

          If _item.updated Then
            _db_trx.Commit()
          Else
            _db_trx.Rollback()
          End If
        End Using
      Next

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK


    Catch ex As Exception
      Log.Exception(ex)
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

  End Function ' UpdateGamingTableDesign
#End Region ' Private Functions

End Class ' TYPE_GAMING_TABLE
