'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_providers_games.vb
'
' DESCRIPTION:   Providers class for providers edition
'
' AUTHOR:        Daniel Moreno
'
' CREATION DATE: 03-MAY-2013
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 03-MAY-2013  DMR    Initial version.
' 10-MAY-2013  JAB    Added multi site functionality.
' 05-JUN-2013  JCA    Fixed Bug #826: not allowed games with EMPTY NAME
' 06-OCT-2014  LEM    Call TerminalReport.ForceRefresh on update.
'--------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports System.Text
Imports WSI.Common

Public Class CLASS_PROVIDERS_GAMES
  Inherits CLASS_BASE

#Region " Structures "

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_DATA_PROVIDERS
    Public is_multisite_member As Boolean
    Public tables_master_mode As ENUM_TABLES_MASTER_MODE
    Public pv_site_id As Int64
    Public pv_id As Int64
    Public pv_name As String
    Public pv_hide As Boolean
    Public pv_points_multiplier As Currency
    Public pv_3gs As ENUM_3GS_STATUS
    Public pv_3gs_vendor_id As String
    Public pv_3gs_vendor_ip As String
    Public pv_site_jackpot As Boolean
    Public pv_only_redeemable As Boolean
    Public data_providers_games As DataTable

    ' PURPOSE: Init structure. 
    '
    '  PARAMS:
    '     - INPUT:
    '
    '     - OUTPUT:
    '
    ' RETURNS:
    '
    Public Sub New()
      is_multisite_member = False
      tables_master_mode = ENUM_TABLES_MASTER_MODE.MODE_DEFAULT

      data_providers_games = New DataTable()
    End Sub ' New

    ' PURPOSE: Finalize structure. 
    '
    '  PARAMS:
    '     - INPUT:
    '
    '     - OUTPUT:
    '
    ' RETURNS:
    '
    Protected Overrides Sub Finalize()
      MyBase.Finalize()

    End Sub ' Finalize

  End Class

#End Region ' Structures

#Region " Members "

  Protected m_data_providers As New TYPE_DATA_PROVIDERS
  Public m_is_multisite As Boolean
  Private m_table_prefix As String
  Private m_table_name As String

#End Region ' Members

#Region " Const "
  Private Const TABLE_PREFIX_PROVIDER_GAME As String = "PG_"
  Private Const TABLE_PREFIX_GAME As String = "GM_"
  Private Const PG_TABLE_NAME As String = "PROVIDERS_GAMES"
  Private Const GM_TABLE_NAME As String = "GAMES"
#End Region

#Region " Properties "

  Public Overloads ReadOnly Property IsMultisiteMember() As Boolean
    Get
      Return m_data_providers.is_multisite_member
    End Get
  End Property

  Public Overloads ReadOnly Property MasterTablesMode() As ENUM_TABLES_MASTER_MODE
    Get
      Return m_data_providers.tables_master_mode
    End Get
  End Property

  Public Property IsMultisite() As Boolean
    Get
      Return m_is_multisite
    End Get
    Set(ByVal value As Boolean)
      m_is_multisite = value
      If m_is_multisite Then
        m_table_prefix = TABLE_PREFIX_GAME
        m_table_name = GM_TABLE_NAME
      Else
        m_table_prefix = TABLE_PREFIX_PROVIDER_GAME
        m_table_name = PG_TABLE_NAME
      End If
    End Set
  End Property
  Public Property SiteId() As Int64
    Get
      Return m_data_providers.pv_site_id
    End Get
    Set(ByVal value As Int64)
      m_data_providers.pv_site_id = value
    End Set
  End Property

  Public Property ProvidersId() As Int64
    Get
      Return m_data_providers.pv_id
    End Get

    Set(ByVal Value As Int64)
      m_data_providers.pv_id = Value
    End Set
  End Property

  Public Property Name() As String
    Get
      Return m_data_providers.pv_name
    End Get

    Set(ByVal Value As String)
      m_data_providers.pv_name = Value
    End Set
  End Property

  Public Property Hide() As Boolean
    Get
      Return m_data_providers.pv_hide
    End Get

    Set(ByVal Value As Boolean)
      m_data_providers.pv_hide = Value
    End Set
  End Property

  Public Property PointsMultiplier() As Currency
    Get
      Return m_data_providers.pv_points_multiplier
    End Get

    Set(ByVal Value As Currency)
      m_data_providers.pv_points_multiplier = Value
    End Set
  End Property

  Public Property Pv3gs() As ENUM_3GS_STATUS
    Get
      Return m_data_providers.pv_3gs
    End Get

    Set(ByVal Value As ENUM_3GS_STATUS)
      m_data_providers.pv_3gs = Value
    End Set
  End Property

  Public Property Pv3gsVendorId() As String
    Get
      Return m_data_providers.pv_3gs_vendor_id
    End Get

    Set(ByVal Value As String)
      m_data_providers.pv_3gs_vendor_id = Value
    End Set
  End Property

  Public Property Pv3gsVendorIp() As String
    Get
      Return m_data_providers.pv_3gs_vendor_ip
    End Get

    Set(ByVal Value As String)
      m_data_providers.pv_3gs_vendor_ip = Value
    End Set
  End Property
  Public Property SiteJackpot() As Boolean
    Get
      Return m_data_providers.pv_site_jackpot
    End Get

    Set(ByVal Value As Boolean)
      m_data_providers.pv_site_jackpot = Value
    End Set
  End Property

  Public Property OnlyRedeemable() As Boolean
    Get
      Return m_data_providers.pv_only_redeemable
    End Get

    Set(ByVal Value As Boolean)
      m_data_providers.pv_only_redeemable = Value
    End Set
  End Property

  Public Property DataProvidersGames() As DataTable
    Get
      Return m_data_providers.data_providers_games
    End Get
    Set(ByVal Value As DataTable)
      m_data_providers.data_providers_games = Value
    End Set
  End Property

#End Region ' Properties

#Region " Enums "

  Enum ENUM_3GS_STATUS
    STATUS_NO_ACTIVATED
    STATUS_ACTIVATED
  End Enum

#End Region ' Enums

#Region " Overrides functions "

  ' PURPOSE: Audit data. 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim _auditor_data As CLASS_AUDITOR_DATA

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_PROVIDERS)

    'Providor
    Call _auditor_data.SetName(GLB_NLS_GUI_AUDITOR.Id(341), Me.Name)

    ' Provider Name
    Call _auditor_data.SetField(GLB_NLS_GUI_AUDITOR.Id(330), IIf(String.IsNullOrEmpty(Me.Name), AUDIT_NONE_STRING, Me.Name))

    If Not Me.IsMultisite Then
      ' Parameters only can be eddited in site
      ' Multiplier
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1921), Me.PointsMultiplier.ToString)

      ' Site Jackpot
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1918), IIf(Me.SiteJackpot, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)))

      ' Redeemable only
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1919), IIf(Me.OnlyRedeemable, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)))

      ' 3GS Status
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1920), IIf(Me.Pv3gs = ENUM_3GS_STATUS.STATUS_ACTIVATED, GLB_NLS_GUI_PLAYER_TRACKING.GetString(532), GLB_NLS_GUI_PLAYER_TRACKING.GetString(316)))

      ' Ip Address
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1925), IIf(String.IsNullOrEmpty(Me.Pv3gsVendorIp), AUDIT_NONE_STRING, Me.Pv3gsVendorIp))

      ' Provider ID
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2011), IIf(String.IsNullOrEmpty(Me.Pv3gsVendorId), AUDIT_NONE_STRING, Me.Pv3gsVendorId))
    End If
    

    ' Games Data
    For Each _row As DataRow In Me.DataProvidersGames.Rows()
      Call _auditor_data.SetField(GLB_NLS_GUI_STATISTICS.Id(215), _row.Item(m_table_prefix & "GAME_NAME").ToString)
      AuditPayouts(_row, _auditor_data)
    Next

    Return _auditor_data

  End Function ' AuditorData

  ' PURPOSE: DB Delete. 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
  End Function ' DB_Delete

  ' PURPOSE: DB Insert. 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As Integer

    _rc = ProvidersGames_DbInsert(m_data_providers, SqlCtx)

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
        Return ENUM_STATUS.STATUS_DUPLICATE_KEY

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Insert

  ' PURPOSE: DB Read. 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim rc As Integer

    Me.ProvidersId = ObjectId
    ' Read providers and games data
    rc = ProvidersGames_DbRead(m_data_providers, SqlCtx)

    Select Case rc
      Case ENUM_DB_RC.DB_RC_OK
        Return ENUM_STATUS.STATUS_OK

      Case ENUM_DB_RC.DB_RC_ERROR_NOT_FOUND
        Return CLASS_BASE.ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Read

  ' PURPOSE: DB Update. 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rcp As Integer

    ' Update providers_games data
    _rcp = ProvidersGames_DbUpdate(SqlCtx)

    Select Case _rcp
      Case ENUM_DB_RC.DB_RC_OK
        Call TerminalReport.ForceRefresh()
        Return ENUM_STATUS.STATUS_OK

      Case ENUM_DB_RC.DB_RC_ERROR_NOT_FOUND
        Return CLASS_BASE.ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Update

  ' PURPOSE: Duplicate provider data. 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim _temp_providers_data As CLASS_PROVIDERS_GAMES

    _temp_providers_data = New CLASS_PROVIDERS_GAMES(GeneralParam.GetBoolean("MultiSite", "IsCenter", False))

    With _temp_providers_data.m_data_providers

      .pv_3gs = Me.m_data_providers.pv_3gs
      .pv_id = Me.m_data_providers.pv_id
      .pv_3gs_vendor_id = Me.m_data_providers.pv_3gs_vendor_id
      .pv_3gs_vendor_ip = Me.m_data_providers.pv_3gs_vendor_ip
      .pv_hide = Me.m_data_providers.pv_hide
      .pv_name = Me.m_data_providers.pv_name
      .pv_only_redeemable = Me.m_data_providers.pv_only_redeemable
      .pv_points_multiplier = Me.m_data_providers.pv_points_multiplier
      .pv_site_jackpot = Me.m_data_providers.pv_site_jackpot
      .data_providers_games = Me.m_data_providers.data_providers_games.Copy()
      .is_multisite_member = Me.m_data_providers.is_multisite_member
      .tables_master_mode = Me.m_data_providers.tables_master_mode

    End With

    Return _temp_providers_data

  End Function   ' Duplicate

#End Region ' Overrides functions

#Region " Public Functions "

  ' PURPOSE: Initialize data providers games. 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Sub New()

    : Me.New(False)
    
  End Sub ' New

  ' PURPOSE: Initialize data providers games. 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Sub New(ByVal Multisite As Boolean)

    IsMultisite = Multisite
    m_data_providers.data_providers_games = getNewTable()

  End Sub ' New

  ' PURPOSE: Set commands to debug. 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Shared Function DebugCommand(ByVal DtReg As DataTable, ByVal Cmd As SqlCommand) As [String]

    Dim _query As [String]
    Dim _sb_declare As StringBuilder
    Dim _sb_set As StringBuilder

    If Not Debugger.IsAttached Then
      Return ""
    End If

    _sb_declare = New StringBuilder()
    _sb_set = New StringBuilder()

    For Each _sqlparameter As SqlParameter In Cmd.Parameters
      _query = "DECLARE " & _sqlparameter.ParameterName & " AS " & _sqlparameter.SqlDbType.ToString().ToUpper()
      If _sqlparameter.DbType = DbType.AnsiString OrElse _sqlparameter.DbType = DbType.AnsiStringFixedLength OrElse _sqlparameter.DbType = DbType.[String] OrElse _sqlparameter.DbType = DbType.StringFixedLength Then
        _query += " (" & _sqlparameter.Size & ")"
      End If

      _sb_declare.AppendLine(_query)
      If _sqlparameter.SqlValue IsNot Nothing AndAlso Not [String].IsNullOrEmpty(_sqlparameter.SqlValue.ToString()) Then
        _sb_set.AppendLine("SET " & _sqlparameter.ParameterName & " =" & SqlValue(_sqlparameter))
      ElseIf Not [String].IsNullOrEmpty(_sqlparameter.SourceColumn) Then
        _sb_set.AppendLine("SET " & _sqlparameter.ParameterName & " =" & SqlValue(DtReg, _sqlparameter))
      Else
        If _sqlparameter.Value IsNot Nothing Then
          _sb_set.AppendLine((("SET " & _sqlparameter.ParameterName & " =") & _sqlparameter.Value & "--(") + _sqlparameter.SqlDbType & ")--")
        Else
          _sb_set.AppendLine("SET " & _sqlparameter.ParameterName & " =")
        End If
      End If
    Next

    _query = _sb_declare.ToString() + _sb_set.ToString() + Cmd.CommandText

    Return _query

  End Function ' DebugCommand

#End Region ' Public functions

#Region " Private Functions "

  ' PURPOSE: Set queries to insert. 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Function ProvidersGames_DbInsert(ByVal pProvidersGames As TYPE_DATA_PROVIDERS, _
                                           ByVal Context As Integer) As Integer

    Dim _str_pv_sql As String
    Dim _str_pg_sql As String
    Dim _param As SqlClient.SqlParameter
    Dim _providers_id As Int64
    Dim _sql_command As System.Data.SqlClient.SqlCommand = Nothing
    Dim SqlTrans As System.Data.SqlClient.SqlTransaction = Nothing
    Dim _num_rows_inserted As Integer
    Dim _sb As StringBuilder
    Dim _sb_pv As StringBuilder

    Try
      If Not GUI_BeginSQLTransaction(SqlTrans) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      _sb_pv = New StringBuilder

      With _sb_pv

        .AppendLine("INSERT INTO    PROVIDERS                   ")
        .AppendLine("           ( PV_NAME                       ")
        .AppendLine("             , PV_HIDE                     ")
        .AppendLine("             , PV_POINTS_MULTIPLIER        ")
        .AppendLine("             , PV_3GS                      ")
        .AppendLine("             , PV_3GS_VENDOR_ID            ")
        .AppendLine("             , PV_3GS_VENDOR_IP            ")
        .AppendLine("             , PV_SITE_JACKPOT             ")
        .AppendLine("             , PV_ONLY_REDEEMABLE          ")
        If m_is_multisite Then
          .AppendLine("           , PV_SITE_ID                  ")
        End If
        .AppendLine("             )                             ")
        .AppendLine("     VALUES                                ")
        .AppendLine("           ( @pName                        ")
        .AppendLine("             , @pHide                      ")
        .AppendLine("             , @pMultiplier                ")
        .AppendLine("             , @p3gs                       ")
        .AppendLine("             , @p3gs_id                    ")
        .AppendLine("             , @p3gs_ip                    ")
        .AppendLine("             , @pJackpot                   ")
        .AppendLine("             , @pOnlyRedeemable            ")
        If m_is_multisite Then
          .AppendLine("           , @pSiteId                    ")
        End If
        .AppendLine("       )                                   ")
        .AppendLine("SET @pProvidersId = SCOPE_IDENTITY()       ")

      End With

      _str_pv_sql = _sb_pv.ToString()

      _sql_command = New SqlCommand(_str_pv_sql)
      _sql_command.Transaction = SqlTrans
      _sql_command.Connection = SqlTrans.Connection

      With _sql_command.Parameters

        .Add("@pName", SqlDbType.NVarChar, 50).Value = pProvidersGames.pv_name
        .Add("@pMultiplier", SqlDbType.Money).Value = pProvidersGames.pv_points_multiplier.SqlMoney '.Value
        .Add("@pHide", SqlDbType.Bit).Value = pProvidersGames.pv_hide
        .Add("@p3gs", SqlDbType.Bit).Value = pProvidersGames.pv_3gs
        .Add("@pJackpot", SqlDbType.Bit).Value = pProvidersGames.pv_site_jackpot
        .Add("@pOnlyRedeemable", SqlDbType.Bit).Value = pProvidersGames.pv_only_redeemable
        .Add("@p3gs_ip", SqlDbType.NVarChar, 50).Value = IIf(pProvidersGames.pv_3gs_vendor_ip = Nothing, DBNull.Value, pProvidersGames.pv_3gs_vendor_ip)
        .Add("@p3gs_id", SqlDbType.NVarChar, 50).Value = IIf(pProvidersGames.pv_3gs_vendor_id = Nothing, DBNull.Value, pProvidersGames.pv_3gs_vendor_id)
        .Add("@pSiteId", SqlDbType.BigInt).Value = pProvidersGames.pv_site_id

        _param = _sql_command.Parameters.Add("@pProvidersId", SqlDbType.Int)

      End With

      _param.Direction = ParameterDirection.Output

      _num_rows_inserted = _sql_command.ExecuteNonQuery()

      _providers_id = _param.Value

      If _num_rows_inserted <> 1 Then
        GUI_EndSQLTransaction(SqlTrans, False)

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      If Me.m_data_providers.data_providers_games.Rows.Count > 0 Then

        For Each _row As DataRow In Me.m_data_providers.data_providers_games.Rows
          _row(m_table_prefix & "PV_ID") = _providers_id
          If m_is_multisite Then
            _row(m_table_prefix & "SITE_ID") = pProvidersGames.pv_site_id
          End If
        Next

        Try
          'Delete all rows with EMPTY GAME_NAME 
          Dim _rows() As DataRow
          _rows = Me.m_data_providers.data_providers_games.Select(m_table_prefix & "GAME_NAME = ''")
          For i As Integer = 0 To _rows.Length - 1
            Me.m_data_providers.data_providers_games.Rows.Remove(_rows(i))
          Next
        Catch ex As Exception

        End Try

        _sb = New StringBuilder()

        With _sb

          .AppendLine("INSERT INTO     " & m_table_name & "                 ")
          .AppendLine("              ( " & m_table_prefix & "PV_ID          ")
          .AppendLine("              , " & m_table_prefix & "GAME_NAME      ")
          .AppendLine("              , " & m_table_prefix & "PAYOUT_1       ")
          .AppendLine("              , " & m_table_prefix & "PAYOUT_2       ")
          .AppendLine("              , " & m_table_prefix & "PAYOUT_3       ")
          .AppendLine("              , " & m_table_prefix & "PAYOUT_4       ")
          .AppendLine("              , " & m_table_prefix & "PAYOUT_5       ")
          .AppendLine("              , " & m_table_prefix & "PAYOUT_6       ")
          .AppendLine("              , " & m_table_prefix & "PAYOUT_7       ")
          .AppendLine("              , " & m_table_prefix & "PAYOUT_8       ")
          .AppendLine("              , " & m_table_prefix & "PAYOUT_9       ")
          .AppendLine("              , " & m_table_prefix & "PAYOUT_10      ")
          If m_is_multisite Then
            .AppendLine("            , " & m_table_prefix & "SITE_ID        ")
          End If
          .AppendLine("              )                   ")
          .AppendLine("     VALUES                       ")
          .AppendLine("              ( @pPvId            ")
          .AppendLine("              , @pGameName        ")
          .AppendLine("              , @pPayout1         ")
          .AppendLine("              , @pPayout2         ")
          .AppendLine("              , @pPayout3         ")
          .AppendLine("              , @pPayout4         ")
          .AppendLine("              , @pPayout5         ")
          .AppendLine("              , @pPayout6         ")
          .AppendLine("              , @pPayout7         ")
          .AppendLine("              , @pPayout8         ")
          .AppendLine("              , @pPayout9         ")
          .AppendLine("              , @pPayout10        ")
          If m_is_multisite Then
            .AppendLine("            , @pSiteId          ")
          End If
          .AppendLine("              )                   ")

        End With

        _str_pg_sql = _sb.ToString()

        Using _sql_pg_cmd As New SqlCommand(_str_pg_sql)

          With _sql_pg_cmd.Parameters

            .Add("@pPvId", SqlDbType.Int).SourceColumn = m_table_prefix & "PV_ID" '.Value = _providers_id
            .Add("@pGameName", SqlDbType.NVarChar).SourceColumn = m_table_prefix & "GAME_NAME"
            .Add("@pPayout1", SqlDbType.Money).SourceColumn = m_table_prefix & "PAYOUT_1"
            .Add("@pPayout2", SqlDbType.Money).SourceColumn = m_table_prefix & "PAYOUT_2"
            .Add("@pPayout3", SqlDbType.Money).SourceColumn = m_table_prefix & "PAYOUT_3"
            .Add("@pPayout4", SqlDbType.Money).SourceColumn = m_table_prefix & "PAYOUT_4"
            .Add("@pPayout5", SqlDbType.Money).SourceColumn = m_table_prefix & "PAYOUT_5"
            .Add("@pPayout6", SqlDbType.Money).SourceColumn = m_table_prefix & "PAYOUT_6"
            .Add("@pPayout7", SqlDbType.Money).SourceColumn = m_table_prefix & "PAYOUT_7"
            .Add("@pPayout8", SqlDbType.Money).SourceColumn = m_table_prefix & "PAYOUT_8"
            .Add("@pPayout9", SqlDbType.Money).SourceColumn = m_table_prefix & "PAYOUT_9"
            .Add("@pPayout10", SqlDbType.Money).SourceColumn = m_table_prefix & "PAYOUT_10"
            If m_is_multisite Then
              .Add("@pSiteId", SqlDbType.Money).SourceColumn = m_table_prefix & "SITE_ID"
            End If

          End With

          _sql_pg_cmd.Transaction = SqlTrans
          _sql_pg_cmd.Connection = SqlTrans.Connection

          Using _sql_pg As New SqlDataAdapter()

            _sql_pg.InsertCommand = _sql_pg_cmd
            _sql_pg.ContinueUpdateOnError = False
            _sql_pg.UpdateBatchSize = 500
            _sql_pg.InsertCommand.UpdatedRowSource = UpdateRowSource.None

            Try
              _num_rows_inserted = _sql_pg.Update(Me.m_data_providers.data_providers_games)

            Catch ex As Exception
              GUI_EndSQLTransaction(SqlTrans, False)
              Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
            End Try

            If _num_rows_inserted <> Me.m_data_providers.data_providers_games.Rows.Count Then 'pProvidersGames.data_providers_games.Rows.Count Then
              GUI_EndSQLTransaction(SqlTrans, False)

              Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
            End If

          End Using
        End Using

      End If

      ' Commit
      If Not GUI_EndSQLTransaction(SqlTrans, True) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If
    Catch ex As Exception
      GUI_EndSQLTransaction(SqlTrans, False)
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

  End Function ' ProvidersGames_DbInsert

  ' PURPOSE: Set queries to read. 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Function ProvidersGames_DbRead(ByVal pProvidersGames As TYPE_DATA_PROVIDERS, _
                                         ByVal Context As Integer) As Integer

    Dim _data_table_providers As DataTable
    Dim _data_table_providers_games As DataTable
    Dim _sb_pv As StringBuilder
    Dim _sb_pg As StringBuilder
    Dim _sql_cmd As SqlCommand
    Dim _sql_trx As System.Data.SqlClient.SqlTransaction
    Dim _do_commit As Boolean

    _sql_trx = Nothing
    _sb_pv = New StringBuilder()
    _do_commit = False

    Try
      If Not GUI_BeginSQLTransaction(_sql_trx) Then

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      With _sb_pv

        If m_is_multisite Then
          .AppendLine("SELECT   PV_SITE_ID          ")
          .AppendLine("       , PV_ID               ")
        Else
          .AppendLine("SELECT   PV_ID               ")
        End If

        .AppendLine("       , PV_NAME               ")
        .AppendLine("       , PV_HIDE               ")
        .AppendLine("       , PV_POINTS_MULTIPLIER  ")
        .AppendLine("       , PV_3GS                ")
        .AppendLine("       , PV_3GS_VENDOR_ID      ")
        .AppendLine("       , PV_3GS_VENDOR_IP      ")
        .AppendLine("       , PV_SITE_JACKPOT       ")
        .AppendLine("       , PV_ONLY_REDEEMABLE    ")
        .AppendLine(" FROM   PROVIDERS              ")
        .AppendLine(" WHERE PV_ID = @pProvidersId   ")

        If m_is_multisite Then
          .AppendLine("   AND PV_SITE_ID = @pSiteId ")
        End If

      End With

      _sql_cmd = New SqlCommand(_sb_pv.ToString())
      _sql_cmd.Parameters.Add("@pProvidersId", SqlDbType.Int).Value = m_data_providers.pv_id
      _sql_cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = m_data_providers.pv_site_id

      _data_table_providers = GUI_GetTableUsingCommand(_sql_cmd, 5000)

      If IsNothing(_data_table_providers) Or _data_table_providers.Rows.Count() <> 1 Then

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      With pProvidersGames

        If m_is_multisite Then
          .pv_site_id = _data_table_providers.Rows(0).Item("PV_SITE_ID")
        End If

        .pv_id = _data_table_providers.Rows(0).Item("PV_ID")
        .pv_name = _data_table_providers.Rows(0).Item("PV_NAME")
        .pv_hide = _data_table_providers.Rows(0).Item("PV_HIDE")
        .pv_points_multiplier = Double.Parse(_data_table_providers.Rows(0).Item("PV_POINTS_MULTIPLIER").ToString)
        .pv_3gs = IIf(Convert.ToBoolean(_data_table_providers.Rows(0).Item("PV_3GS")), ENUM_3GS_STATUS.STATUS_ACTIVATED, ENUM_3GS_STATUS.STATUS_NO_ACTIVATED)
        .pv_3gs_vendor_id = IIf(_data_table_providers.Rows(0).Item("PV_3GS_VENDOR_ID") Is DBNull.Value, Nothing, _data_table_providers.Rows(0).Item("PV_3GS_VENDOR_ID"))
        .pv_3gs_vendor_ip = IIf(_data_table_providers.Rows(0).Item("PV_3GS_VENDOR_IP") Is DBNull.Value, Nothing, _data_table_providers.Rows(0).Item("PV_3GS_VENDOR_IP"))
        .pv_site_jackpot = _data_table_providers.Rows(0).Item("PV_SITE_JACKPOT")
        .pv_only_redeemable = _data_table_providers.Rows(0).Item("PV_ONLY_REDEEMABLE")

      End With

      _sb_pg = New StringBuilder()

      With _sb_pg

        .AppendLine("SELECT   " & m_table_prefix & "PV_ID ")
        .AppendLine("       , " & m_table_prefix & "GAME_ID  ")
        .AppendLine("       , " & m_table_prefix & "GAME_NAME ")
        .AppendLine("       , " & m_table_prefix & "PAYOUT_1 ")
        .AppendLine("       , " & m_table_prefix & "PAYOUT_2 ")
        .AppendLine("       , " & m_table_prefix & "PAYOUT_3 ")
        .AppendLine("       , " & m_table_prefix & "PAYOUT_4 ")
        .AppendLine("       , " & m_table_prefix & "PAYOUT_5 ")
        .AppendLine("       , " & m_table_prefix & "PAYOUT_6 ")
        .AppendLine("       , " & m_table_prefix & "PAYOUT_7 ")
        .AppendLine("       , " & m_table_prefix & "PAYOUT_8 ")
        .AppendLine("       , " & m_table_prefix & "PAYOUT_9 ")
        .AppendLine("       , " & m_table_prefix & "PAYOUT_10 ")

        If m_is_multisite Then
          .AppendLine("       , " & m_table_prefix & "SITE_ID  ")

        End If

        .AppendLine(" FROM   " & m_table_name)
        .AppendLine(" WHERE  " & m_table_prefix & "PV_ID = @pProvidersId")

        If m_is_multisite Then
          .AppendLine("   AND " & m_table_prefix & "SITE_ID = @pSiteId")
        End If

        .AppendLine(" ORDER BY  " & m_table_prefix & "GAME_NAME ")

      End With

      _sql_cmd = New SqlCommand(_sb_pg.ToString())
      _sql_cmd.Parameters.Add("@pProvidersId", SqlDbType.Int).Value = m_data_providers.pv_id
      _sql_cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = m_data_providers.pv_site_id

      _data_table_providers_games = GUI_GetTableUsingCommand(_sql_cmd, 5000)

      If IsNothing(_data_table_providers_games) Then

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If


      With pProvidersGames

        .data_providers_games = _data_table_providers_games
        .is_multisite_member = GeneralParam.GetBoolean("Site", "MultiSiteMember", False)
        .tables_master_mode = GeneralParam.GetInt32("MultiSite", "TablesMasterMode", 0)

      End With

    Catch ex As Exception
      _do_commit = False
    Finally

      _do_commit = True
      GUI_EndSQLTransaction(_sql_trx, _do_commit)

    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

  End Function ' ProvidersGames_DbRead

  ' PURPOSE: Set queries to update. 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Function ProvidersGames_DbUpdate(ByVal Context As Integer) As Integer

    Dim _sql_trx As System.Data.SqlClient.SqlTransaction = Nothing
    Dim _num_rows_updated As Integer
    Dim _sb As StringBuilder
    Dim _cmd_update As SqlClient.SqlCommand
    Dim _cmd_insert As SqlClient.SqlCommand
    Dim _cmd_delete As SqlClient.SqlCommand

    If Not GUI_BeginSQLTransaction(_sql_trx) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    Try

      _sb = New StringBuilder()

      With _sb

        .AppendLine(" UPDATE   PROVIDERS ")
        .AppendLine("    SET   PV_NAME = @pName ")
        .AppendLine("        , PV_3GS  = @p3gs ")
        .AppendLine("        , PV_3GS_VENDOR_IP = @p3gs_ip ")
        .AppendLine("        , PV_3GS_VENDOR_ID = @p3gs_id ")
        .AppendLine("        , PV_SITE_JACKPOT = @pJackpot ")
        .AppendLine("        , PV_ONLY_REDEEMABLE = @pOnlyRedeemable ")
        .AppendLine("        , PV_POINTS_MULTIPLIER = @pPointsMultipler ")
        .AppendLine("  WHERE   PV_ID = @pProvidersId ")
        If m_is_multisite Then
          .AppendLine("  AND   PV_SITE_ID = @pSiteId ")
        End If

      End With

      Using _cmd As New SqlClient.SqlCommand(_sb.ToString(), _sql_trx.Connection, _sql_trx)

        With _cmd.Parameters

          .Add("@pName", SqlDbType.NVarChar).Value = m_data_providers.pv_name
          .Add("@p3gs", SqlDbType.Bit).Value = IIf(m_data_providers.pv_3gs = ENUM_3GS_STATUS.STATUS_ACTIVATED, True, False)
          .Add("@p3gs_ip", SqlDbType.NVarChar).Value = IIf(String.IsNullOrEmpty(m_data_providers.pv_3gs_vendor_ip), DBNull.Value, m_data_providers.pv_3gs_vendor_ip)
          .Add("@p3gs_id", SqlDbType.NVarChar).Value = IIf(String.IsNullOrEmpty(m_data_providers.pv_3gs_vendor_id), DBNull.Value, m_data_providers.pv_3gs_vendor_id)
          .Add("@pJackpot", SqlDbType.Bit).Value = m_data_providers.pv_site_jackpot
          .Add("@pOnlyRedeemable", SqlDbType.Bit).Value = m_data_providers.pv_only_redeemable
          .Add("@pPointsMultipler", SqlDbType.Money).Value = m_data_providers.pv_points_multiplier.SqlMoney
          .Add("@pProvidersId", SqlDbType.Int).Value = m_data_providers.pv_id
          .Add("@pSiteId", SqlDbType.Int).Value = m_data_providers.pv_site_id

        End With

        _num_rows_updated = _cmd.ExecuteNonQuery()

        If _num_rows_updated <> 1 Then
          GUI_EndSQLTransaction(_sql_trx, False)

          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

      End Using

      If Not Me.m_data_providers.data_providers_games.GetChanges() Is Nothing Then

        Try
          'Delete all rows with EMPTY GAME_NAME 
          Dim _rows() As DataRow
          _rows = Me.m_data_providers.data_providers_games.Select(m_table_prefix & "GAME_NAME = ''")
          For i As Integer = 0 To _rows.Length - 1
            Me.m_data_providers.data_providers_games.Rows.Remove(_rows(i))
          Next
        Catch ex As Exception

        End Try

        With _sb

          .Length = 0
          .AppendLine(" UPDATE   " & m_table_name)
          .AppendLine("    SET   " & m_table_prefix & "GAME_NAME = @pGameName")
          .AppendLine("        , " & m_table_prefix & "PAYOUT_1 = @pPayout1")
          .AppendLine("        , " & m_table_prefix & "PAYOUT_2 = @pPayout2")
          .AppendLine("        , " & m_table_prefix & "PAYOUT_3 = @pPayout3")
          .AppendLine("        , " & m_table_prefix & "PAYOUT_4 = @pPayout4")
          .AppendLine("        , " & m_table_prefix & "PAYOUT_5 = @pPayout5")
          .AppendLine("        , " & m_table_prefix & "PAYOUT_6 = @pPayout6")
          .AppendLine("        , " & m_table_prefix & "PAYOUT_7 = @pPayout7")
          .AppendLine("        , " & m_table_prefix & "PAYOUT_8 = @pPayout8")
          .AppendLine("        , " & m_table_prefix & "PAYOUT_9 = @pPayout9")
          .AppendLine("        , " & m_table_prefix & "PAYOUT_10 = @pPayout10")
          .AppendLine("  WHERE   " & m_table_prefix & "PV_ID = @pProvidersId")
          .AppendLine("    AND   " & m_table_prefix & "GAME_ID = @pGameId")
          If m_is_multisite Then
            .AppendLine("    AND   " & m_table_prefix & "SITE_ID = @pSiteId")
          End If

        End With

        _cmd_update = New SqlCommand(_sb.ToString(), _sql_trx.Connection, _sql_trx)

        With _cmd_update.Parameters

          .Add("@pProvidersId", SqlDbType.Int).Value = Me.m_data_providers.pv_id
          .Add("@pSiteId", SqlDbType.Int).Value = Me.m_data_providers.pv_site_id
          .Add("@pGameId", SqlDbType.Int).SourceColumn = m_table_prefix & "GAME_ID"
          .Add("@pGameName", SqlDbType.NVarChar).SourceColumn = m_table_prefix & "GAME_NAME"
          .Add("@pPayout1", SqlDbType.Money).SourceColumn = m_table_prefix & "PAYOUT_1"
          .Add("@pPayout2", SqlDbType.Money).SourceColumn = m_table_prefix & "PAYOUT_2"
          .Add("@pPayout3", SqlDbType.Money).SourceColumn = m_table_prefix & "PAYOUT_3"
          .Add("@pPayout4", SqlDbType.Money).SourceColumn = m_table_prefix & "PAYOUT_4"
          .Add("@pPayout5", SqlDbType.Money).SourceColumn = m_table_prefix & "PAYOUT_5"
          .Add("@pPayout6", SqlDbType.Money).SourceColumn = m_table_prefix & "PAYOUT_6"
          .Add("@pPayout7", SqlDbType.Money).SourceColumn = m_table_prefix & "PAYOUT_7"
          .Add("@pPayout8", SqlDbType.Money).SourceColumn = m_table_prefix & "PAYOUT_8"
          .Add("@pPayout9", SqlDbType.Money).SourceColumn = m_table_prefix & "PAYOUT_9"
          .Add("@pPayout10", SqlDbType.Money).SourceColumn = m_table_prefix & "PAYOUT_10"

        End With

        _sb.Length = 0
        _sb.AppendLine(" DELETE FROM " & m_table_name)
        _sb.AppendLine("  WHERE " & m_table_prefix & "PV_ID = @pProvidersId")
        _sb.AppendLine("    AND " & m_table_prefix & "GAME_ID = @pGameId")
        If m_is_multisite Then
          _sb.AppendLine("  AND " & m_table_prefix & "SITE_ID = @pSiteId")
        End If

        _cmd_delete = New SqlCommand(_sb.ToString(), _sql_trx.Connection, _sql_trx)

        _cmd_delete.Parameters.Add("@pProvidersId", SqlDbType.Int).Value = Me.m_data_providers.pv_id
        _cmd_delete.Parameters.Add("@pSiteId", SqlDbType.Int).Value = Me.m_data_providers.pv_site_id
        _cmd_delete.Parameters.Add("@pGameId", SqlDbType.Int).SourceColumn = m_table_prefix & "GAME_ID"

        With _sb

          .Length = 0
          .AppendLine("INSERT INTO   " & m_table_name)
          .AppendLine("            ( " & m_table_prefix & "PV_ID        ")
          .AppendLine("            , " & m_table_prefix & "GAME_NAME    ")
          .AppendLine("            , " & m_table_prefix & "PAYOUT_1     ")
          .AppendLine("            , " & m_table_prefix & "PAYOUT_2     ")
          .AppendLine("            , " & m_table_prefix & "PAYOUT_3     ")
          .AppendLine("            , " & m_table_prefix & "PAYOUT_4     ")
          .AppendLine("            , " & m_table_prefix & "PAYOUT_5     ")
          .AppendLine("            , " & m_table_prefix & "PAYOUT_6     ")
          .AppendLine("            , " & m_table_prefix & "PAYOUT_7     ")
          .AppendLine("            , " & m_table_prefix & "PAYOUT_8     ")
          .AppendLine("            , " & m_table_prefix & "PAYOUT_9     ")
          .AppendLine("            , " & m_table_prefix & "PAYOUT_10    ")
          If m_is_multisite Then
            .AppendLine("          , " & m_table_prefix & "SITE_ID      ")
          End If
          .AppendLine("            )                 ")
          .AppendLine("     VALUES                   ")
          .AppendLine("            ( @pPvId          ")
          .AppendLine("            , @pGameName      ")
          .AppendLine("            , @pPayout1       ")
          .AppendLine("            , @pPayout2       ")
          .AppendLine("            , @pPayout3       ")
          .AppendLine("            , @pPayout4       ")
          .AppendLine("            , @pPayout5       ")
          .AppendLine("            , @pPayout6       ")
          .AppendLine("            , @pPayout7       ")
          .AppendLine("            , @pPayout8       ")
          .AppendLine("            , @pPayout9       ")
          .AppendLine("            , @pPayout10      ")
          If m_is_multisite Then
            .AppendLine("          , @pSiteId        ")
          End If
          .AppendLine("            )                 ")

        End With

        _cmd_insert = New SqlCommand(_sb.ToString(), _sql_trx.Connection, _sql_trx)

        With _cmd_insert.Parameters

          .Add("@pPvId", SqlDbType.Int).Value = Me.m_data_providers.pv_id
          .Add("@pSiteId", SqlDbType.Int).Value = Me.m_data_providers.pv_site_id
          .Add("@pGameName", SqlDbType.NVarChar).SourceColumn = m_table_prefix & "GAME_NAME"
          .Add("@pPayout1", SqlDbType.Money).SourceColumn = m_table_prefix & "PAYOUT_1"
          .Add("@pPayout2", SqlDbType.Money).SourceColumn = m_table_prefix & "PAYOUT_2"
          .Add("@pPayout3", SqlDbType.Money).SourceColumn = m_table_prefix & "PAYOUT_3"
          .Add("@pPayout4", SqlDbType.Money).SourceColumn = m_table_prefix & "PAYOUT_4"
          .Add("@pPayout5", SqlDbType.Money).SourceColumn = m_table_prefix & "PAYOUT_5"
          .Add("@pPayout6", SqlDbType.Money).SourceColumn = m_table_prefix & "PAYOUT_6"
          .Add("@pPayout7", SqlDbType.Money).SourceColumn = m_table_prefix & "PAYOUT_7"
          .Add("@pPayout8", SqlDbType.Money).SourceColumn = m_table_prefix & "PAYOUT_8"
          .Add("@pPayout9", SqlDbType.Money).SourceColumn = m_table_prefix & "PAYOUT_9"
          .Add("@pPayout10", SqlDbType.Money).SourceColumn = m_table_prefix & "PAYOUT_10"

        End With

        Using _sql_da As New SqlDataAdapter()

          _sql_da.UpdateCommand = _cmd_update
          _sql_da.InsertCommand = _cmd_insert

          _sql_da.ContinueUpdateOnError = False
          _sql_da.UpdateBatchSize = 500
          _sql_da.InsertCommand.UpdatedRowSource = UpdateRowSource.None
          _sql_da.UpdateCommand.UpdatedRowSource = UpdateRowSource.None
          _num_rows_updated = _sql_da.Update(Me.m_data_providers.data_providers_games.GetChanges())

          If _num_rows_updated <> Me.m_data_providers.data_providers_games.GetChanges().Rows.Count Then
            Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
          End If

        End Using
      End If

      ' Commit
      If Not GUI_EndSQLTransaction(_sql_trx, True) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If
    Catch ex As Exception
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

  End Function  ' Providers_DbUpdate

  ' PURPOSE: Create a new table. 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Function getNewTable() As DataTable

    Dim m_table As DataTable

    m_table = New DataTable("GAMES")

    With m_table.Columns

      .Add(m_table_prefix & "PV_ID")
      .Add(m_table_prefix & "GAME_ID")
      .Add(m_table_prefix & "GAME_NAME")
      .Add(m_table_prefix & "PAYOUT_1", System.Type.GetType("System.Decimal"))
      .Add(m_table_prefix & "PAYOUT_2", System.Type.GetType("System.Decimal"))
      .Add(m_table_prefix & "PAYOUT_3", System.Type.GetType("System.Decimal"))
      .Add(m_table_prefix & "PAYOUT_4", System.Type.GetType("System.Decimal"))
      .Add(m_table_prefix & "PAYOUT_5", System.Type.GetType("System.Decimal"))
      .Add(m_table_prefix & "PAYOUT_6", System.Type.GetType("System.Decimal"))
      .Add(m_table_prefix & "PAYOUT_7", System.Type.GetType("System.Decimal"))
      .Add(m_table_prefix & "PAYOUT_8", System.Type.GetType("System.Decimal"))
      .Add(m_table_prefix & "PAYOUT_9", System.Type.GetType("System.Decimal"))
      .Add(m_table_prefix & "PAYOUT_10", System.Type.GetType("System.Decimal"))
      .Add(m_table_prefix & "SITE_ID")

    End With

    Return m_table

  End Function ' getNewTable

  ' PURPOSE: SqlValue. 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Shared Function SqlValue(ByVal DtReg As DataTable, ByVal SqlPar As SqlParameter) As Object
    If DtReg Is Nothing Then
      Return Nothing
    End If
    If SqlPar.DbType = DbType.AnsiString OrElse SqlPar.DbType = DbType.AnsiStringFixedLength OrElse SqlPar.DbType = DbType.[String] OrElse SqlPar.DbType = DbType.StringFixedLength Then
      Return "'" + DtReg.Rows(0)(SqlPar.SourceColumn) & "'"
    ElseIf SqlPar.DbType = DbType.[Date] OrElse SqlPar.DbType = DbType.DateTime OrElse SqlPar.DbType = DbType.DateTime2 Then
      Return "'" & Convert.ToDateTime(DtReg.Rows(0)(SqlPar.SourceColumn)).ToString("yyyy-MM-dd HH:mm:ss") & "'"
    End If
    Return DtReg.Rows(0)(SqlPar.SourceColumn)
  End Function ' SqlValue

  ' PURPOSE: SqlValue. 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Shared Function SqlValue(ByVal _sqlparameter As SqlParameter) As [String]
    If _sqlparameter.DbType = DbType.AnsiString OrElse _sqlparameter.DbType = DbType.AnsiStringFixedLength OrElse _sqlparameter.DbType = DbType.[String] OrElse _sqlparameter.DbType = DbType.StringFixedLength Then

      Return "'" & _sqlparameter.SqlValue.ToString() & "'"

    ElseIf _sqlparameter.DbType = DbType.[Date] OrElse _sqlparameter.DbType = DbType.DateTime OrElse _sqlparameter.DbType = DbType.DateTime2 Then

      Return "'" & Convert.ToDateTime(_sqlparameter.SqlValue.ToString()).ToString("yyyy-MM-dd HH:mm:ss") & "'"

    End If
    Return _sqlparameter.SqlValue.ToString()
  End Function ' SqlValue

  Private Sub AuditPayouts(ByVal Row As DataRow, ByVal AuditorData As CLASS_AUDITOR_DATA)
    Dim _game_payouts As StringBuilder
    Dim _strpayout As String

    _game_payouts = New StringBuilder
    _game_payouts.Append(GLB_NLS_GUI_STATISTICS.GetString(215) & " " & Row.Item(m_table_prefix & "GAME_NAME").ToString())

    For x As Integer = 1 To 10

      _strpayout = AUDIT_NONE_STRING
      If Not String.IsNullOrEmpty(Row.Item(m_table_prefix & "PAYOUT_" & x.ToString()).ToString()) Then
        _strpayout = String.Format("{0:n2}", Convert.ToDecimal(Row.Item(m_table_prefix & "PAYOUT_" & x.ToString())))
      End If

      _game_payouts.Append(" " & x.ToString() & " - [")
      _game_payouts.Append(_strpayout)
      _game_payouts.Append("%]")
    Next

    Call AuditorData.SetField(0, _game_payouts.ToString())
    _game_payouts.Length = 0
  End Sub ' AuditPayouts

#End Region ' Private Functions

End Class


