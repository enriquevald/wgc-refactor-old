'-------------------------------------------------------------------
' Copyright � 2016 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_about_us
' DESCRIPTION:   About Us class
' AUTHOR:        Gustavo Al�
' CREATION DATE: 07-SEP-2016
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 07-SEP-2016  GDA    Initial version
'--------------------------------------------------------------------
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports WSI.Common.MEDIA_TYPE
Imports System.Text
Imports System.IO
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
Imports System.Net


Public Class CLS_ABOUT_US
  Inherits CLASS_BASE

#Region " Constants "

  Protected Const AUDIT_LABEL_TITLE As Integer = 7593
  Protected Const AUDIT_LABEL_ABOUT_US As Integer = 7592
  Protected Const AUDIT_LABEL_CONTENT_TEXT As Integer = 7594
  Private Const SPANISH_LANG As Integer = 1
#End Region

#Region " Enums "

#End Region

#Region " Structures "
  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_ABOUT_US
    Public _id As String
    Public _title As String
    Public _content_text As String

  End Class
#End Region

#Region " Members "
  Protected m_about_us As New TYPE_ABOUT_US
#End Region

#Region " Properties "

  Public Property ID() As Integer
    Get
      Return m_about_us._id
    End Get
    Set(ByVal value As Integer)
      m_about_us._id = value
    End Set
  End Property
  Public Property Title() As String
    Get
      Return m_about_us._title
    End Get
    Set(ByVal value As String)
      m_about_us._title = value
    End Set
  End Property

  Public Property ContentText() As String
    Get
      Return m_about_us._content_text
    End Get
    Set(ByVal value As String)
      m_about_us._content_text = value
    End Set
  End Property
#End Region

#Region " Override Functions "

  '----------------------------------------------------------------------------
  ' PURPOSE : Reads from the database into the given object
  '
  ' PARAMS :
  '     - INPUT :
  '         - ObjectId: An object, it must be 'casted' to the desired KEY.
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '
  ' NOTES :

  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As Integer

    Me.ID = ObjectId

    ' Read ads data
    _rc = ReadAboutUs(m_about_us, SqlCtx)

    Select Case _rc

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE : Updates the given object to the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES :

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As Integer

    ' Update ads data
    _rc = UpdateAboutUs(m_about_us, SqlCtx)

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
        Return ENUM_STATUS.STATUS_DUPLICATE_KEY

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select
  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE : Inserts the given object to the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES :

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _rc As Integer

    _rc = InsertAboutUs(m_about_us, SqlCtx)

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
        Return ENUM_STATUS.STATUS_DUPLICATE_KEY

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select
  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE : Deletes the given object from the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DEPENDENCIES
  '
  ' NOTES :

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _rc As Integer

    _rc = DeleteAboutUs(m_about_us, SqlCtx)

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DEPENDENCIES
        Return ENUM_STATUS.STATUS_DEPENDENCIES

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select
  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE : Returns the related auditor data for the current object.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - The newly created audit object
  '
  ' NOTES :

  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim _auditor_data As CLASS_AUDITOR_DATA

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_PLAYER_PROMOTIONS)

    Call _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(AUDIT_LABEL_ABOUT_US), Me.Title)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(AUDIT_LABEL_TITLE), Me.Title)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(AUDIT_LABEL_CONTENT_TEXT), Me.ContentText)

    Return _auditor_data
  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE : Returns a duplicate of the given object.
  '
  ' PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - The newly created object
  '
  ' NOTES :

  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE
    Dim _copy As CLS_ABOUT_US
    _copy = New CLS_ABOUT_US

    _copy.m_about_us._title = Me.m_about_us._title
    _copy.m_about_us._content_text = Me.m_about_us._content_text
    _copy.m_about_us._id = Me.m_about_us._id

    Return _copy
  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE : Returns object info as String ( Mainly for easy debug ) 
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT : 
  '
  ' RETURNS :
  '     - String with instance info
  '
  ' NOTES :

  Public Overrides Function ToString() As String

    Dim _str_bld As System.Text.StringBuilder
    _str_bld = New System.Text.StringBuilder()

    _str_bld.AppendLine("ID =  " & Me.ID)
    _str_bld.AppendLine("Title =  " & Me.Title)
    _str_bld.AppendLine("ContentText = " & Me.ContentText)

    Return _str_bld.ToString()
  End Function

#End Region

#Region " Private functions"

  '----------------------------------------------------------------------------
  ' PURPOSE : Return the properties of the class in JSON format
  '
  ' PARAMS :
  '     - INPUT :
  '         None
  '
  '     - OUTPUT :
  '         None
  '
  ' RETURNS :
  '     - JSON string
  '
  ' NOTES :

  Private Function ToJSON() As String
    Dim _aboutUsSchemaId As New Integer
    Dim _jObj As New JObject
    _jObj.Add("aboutusid", ID)
    _jObj.Add("title", Title)
    _jObj.Add("contenttext", ContentText)
    _jObj.Add("languageid", SPANISH_LANG)

    _aboutUsSchemaId = GetAboutUsId()
    _jObj.Add("SectionSchemaId", _aboutUsSchemaId)
    Return _jObj.ToString()
  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE : Reads a advertisement object from DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - TYPE_ADS_STEPS
  '         - Context
  '
  '     - OUTPUT :
  '         - Item
  '
  ' RETURNS :
  '     - CONFIGURATION_OK
  '     - CONFIGURATION_ERROR_DB
  '
  ' NOTES :

  Private Function ReadAboutUs(ByVal Item As TYPE_ABOUT_US, _
                           ByVal Context As Integer) As Integer

    Dim _id As Integer
    Dim _datatable As DataTable
    Dim _sql_transaction As System.Data.SqlClient.SqlTransaction
    Dim _rc As Integer
    Dim _do_commit As Boolean
    Dim _url As String
    Dim _api As CLS_API

    _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    _sql_transaction = Nothing
    _id = Item._id
    _do_commit = False

    Try

      If Not GUI_BeginSQLTransaction(_sql_transaction) Then

        _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        Return _rc
      End If

      _url = WSI.Common.GeneralParam.GetString("WinUP", "Backend.Url") + "/aboutus/getbylanguage/" + _id.ToString()
      _api = New CLS_API(_url)

      _datatable = JsonConvert.DeserializeObject(Of DataTable)(_api.responseToArray(_api.sanitizeData(_api.getData())))

      If Not IsNothing(_datatable) AndAlso _datatable.Rows.Count = 1 Then

        With Item
          ._title = _datatable.Rows(0).Item("title")
          ._content_text = _datatable.Rows(0).Item("contenttext")
        End With

        _do_commit = True
        _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return _rc

      Else

        _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        Return _rc

      End If
    Catch ex As Exception
      _do_commit = False
    Finally

      GUI_EndSQLTransaction(_sql_transaction, _do_commit)

    End Try

    Return _rc

  End Function

  Private Function GetAboutUsId() As Integer

    Dim _datatable As DataTable
    Dim _rc As Integer
    Dim _url As String
    Dim _api As CLS_API


    Try
      _url = WSI.Common.GeneralParam.GetString("WinUP", "Backend.Url") + "/sectionsschema/AboutUs"
      _api = New CLS_API(_url)

      _datatable = JsonConvert.DeserializeObject(Of DataTable)(_api.responseToArray(_api.sanitizeData(_api.getData())))

      If Not IsNothing(_datatable) AndAlso _datatable.Rows.Count = 1 Then

        _rc = _datatable.Rows(0).Item("sectionSchemaId")
        
      End If

    Catch ex As Exception

    End Try

    Return _rc

  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE : Deletes a advertisement object from DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - TYPE_ADS_STEPS
  '         - Context
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - CONFIGURATION_OK
  '     - CONFIGURATION_ERROR_DB
  '
  ' NOTES :

  Private Function DeleteAboutUs(ByVal Item As TYPE_ABOUT_US, _
                             ByVal Context As Integer) As Integer

    Dim _code As String
    Dim _sql_transaction As System.Data.SqlClient.SqlTransaction
    Dim _row_affected As Integer
    Dim _do_commit As Boolean
    Dim _url As String
    Dim _api As CLS_API
    Dim response As String

    _sql_transaction = Nothing
    _row_affected = 0
    _code = Item._id
    _do_commit = False

    Try

      If Not GUI_BeginSQLTransaction(_sql_transaction) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      _url = WSI.Common.GeneralParam.GetString("WinUP", "Backend.Url") + "/aboutus/" + Item._id.ToString()
      _api = New CLS_API(_url)

      response = _api.deleteData()

      _row_affected = 1
      If _row_affected <> 1 Then
        _do_commit = False
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      Else
        _do_commit = True
      End If



    Catch ex As Exception

      _do_commit = False

    Finally

      GUI_EndSQLTransaction(_sql_transaction, _do_commit)
    End Try

    If _do_commit Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
    Else
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

  End Function ' DeleteAds

  '----------------------------------------------------------------------------
  ' PURPOSE : Adds a advertisement object into DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - TYPE_ADS_STEPS
  '         - Context
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - CONFIGURATION_OK
  '     - CONFIGURATION_ERROR_DB
  '
  ' NOTES :

  Private Function InsertAboutUs(ByVal Item As TYPE_ABOUT_US, _
                             ByVal Context As Integer) As Integer

    Dim _sql_transaction As System.Data.SqlClient.SqlTransaction
    Dim _row_affected As Integer
    Dim _do_commit As Boolean
    Dim _rc As Integer
    Dim _url As String
    Dim _api As CLS_API
    Dim response As String

    _sql_transaction = Nothing
    _row_affected = 0
    _do_commit = False
    _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

    Try

      If Not GUI_BeginSQLTransaction(_sql_transaction) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If


      _url = WSI.Common.GeneralParam.GetString("WinUP", "Backend.Url") + "/aboutus"
      _api = New CLS_API(_url)

      response = _api.postData(Encoding.UTF8.GetBytes(Me.ToJSON()))

      _row_affected = 1

      If _row_affected <> 1 Then
        _do_commit = False
        _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        Return _rc
      Else
        _do_commit = True
        _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_OK
      End If

    Catch ex As Exception
      _do_commit = False
      _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

    Finally

      GUI_EndSQLTransaction(_sql_transaction, _do_commit)

    End Try

    Return _rc

  End Function ' InsertArea

  '----------------------------------------------------------------------------
  ' PURPOSE : Updates the given advertisement object into the DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - TYPE_ADS_STEPS
  '         - Context
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - CONFIGURATION_OK
  '     - CONFIGURATION_ERROR_DB
  '
  ' NOTES :

  Private Function UpdateAboutUs(ByVal Item As TYPE_ABOUT_US, _
                             ByVal Context As Integer) As Integer

    Dim _sql_transaction As System.Data.SqlClient.SqlTransaction
    Dim _row_affected As Integer
    Dim _do_commit As Boolean
    Dim _rc As Integer
    Dim _url As String
    Dim _api As CLS_API
    Dim response As String

    _sql_transaction = Nothing
    _row_affected = 0
    _do_commit = False

    Try

      If Not GUI_BeginSQLTransaction(_sql_transaction) Then
        _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

        Return _rc
      End If

      _url = WSI.Common.GeneralParam.GetString("WinUP", "Backend.Url") + "/aboutus/" + Item._id.ToString()
      _api = New CLS_API(_url)

      response = _api.putData(Encoding.UTF8.GetBytes(Me.ToJSON()))

      '_datatable = JsonConvert.DeserializeObject(Of DataTable)(_api.responseToArray(_api.sanitizeData(_api.getData())))

      _row_affected = 1

      If _row_affected <> 1 Then
        _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        Return _rc
      Else
        _do_commit = True
        _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_OK
      End If

    Catch ex As Exception
      _do_commit = False

    Finally

      GUI_EndSQLTransaction(_sql_transaction, _do_commit)

    End Try

    Return _rc

  End Function ' UpdateArea

#End Region

End Class
