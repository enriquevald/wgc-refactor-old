﻿'-------------------------------------------------------------------
' Copyright © 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_games_category.vb
' DESCRIPTION:   category class for user edition
' AUTHOR:        Sergio Daniel Soria
' CREATION DATE: 31-AUG-2015
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 22-AUG-2016  SDS    Initial version.
' -----------  ------ -----------------------------------------------


Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports WSI.Common
Imports System.Text
Imports System.Type
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports System.IO

Public Class CLS_GAMES_CATEGORY
  Inherits CLASS_BASE


#Region " Enum "

  Private Enum ROWACTION
    NORMAL = 0
    MODIFIED = 1
    DELETED = 2
  End Enum

#End Region 'Enum

#Region " Constants "

  Private Const SQL_COLUMN_CAT_ID As Integer = 0
  Private Const SQL_COLUMN_TITLE As Integer = 1
  Private Const SQL_COLUMN_DESCRIPTION As Integer = 2
  Private Const SQL_COLUMN_ENABLED As Integer = 3
  Private Const SQL_COLUMN_IMAGE As Integer = 4
  Private Const SQL_COLUMN_LASTUPDATE As Integer = 5

#End Region ' Constants 

#Region " Structures "

  Public Class CATEGORY
    Public category_id As Int64
    Public title As String
    Public description As String
    Public enabled As Boolean
    Public image As Image
    Public last_update As Date

    Sub New()
      category_id = 0
      title = String.Empty
      description = String.Empty
      enabled = True
      last_update = WSI.Common.WGDB.Now
    End Sub
  End Class

#End Region ' Structures

#Region " Members "
  Protected m_category As New CATEGORY
#End Region ' Members

#Region "Properties"
  Public Property CategoryId() As Int64
    Get
      Return m_category.category_id
    End Get
    Set(ByVal value As Int64)
      m_category.category_id = value
    End Set
  End Property

  Public Property Title() As String
    Get
      Return m_category.title
    End Get
    Set(ByVal value As String)
      m_category.title = value
    End Set
  End Property


  Public Property Description() As String
    Get
      Return m_category.description
    End Get
    Set(ByVal value As String)
      m_category.description = value
    End Set
  End Property
  Public Property Enabled() As Boolean
    Get
      Return m_category.enabled
    End Get
    Set(ByVal value As Boolean)
      m_category.enabled = value
    End Set
  End Property

  Public Property Image() As Image
    Get
      Return m_category.image
    End Get
    Set(ByVal value As Image)
      m_category.image = value
    End Set
  End Property

  Public Property LastUpdate() As Date
    Get
      Return m_category.last_update
    End Get
    Set(ByVal value As Date)
      m_category.last_update = value
    End Set
  End Property

#End Region ' Properties

#Region "Overrides"

  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA
    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _str_sort As String
    Dim _action_value As ROWACTION
    Dim _yes_text As String
    Dim _no_text As String
    Dim _str_title As String
    Dim _str_description As String


    _yes_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6692)
    _no_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6693)
    _str_sort = String.Empty
    _action_value = ROWACTION.NORMAL
    _str_title = String.Empty
    _str_description = String.Empty

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_CASHIER_CONFIGURATION)

    Call _auditor_data.SetName(GLB_NLS_GUI_AUDITOR.Id(484), " - " & Me.m_category.title)
    Call _auditor_data.SetField(0, m_category.title, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6679)) 'nombre
    Call _auditor_data.SetField(0, m_category.description, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6682)) 'descripcion

    If m_category.enabled Then
      Call _auditor_data.SetField(0, _yes_text, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6694)) 'habilitado
    Else
      Call _auditor_data.SetField(0, _no_text, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6694))
    End If

    ''Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7635), Me.LastUpdate)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7636), GetInfoCRC(Me.Image))

    Return _auditor_data

  End Function ' AuditorData

  '----------------------------------------------------------------------------
  ' PURPOSE : Deletes the given object from the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DEPENDENCIES
  '
  ' NOTES :
  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR

  End Function ' DB_Delete

  '----------------------------------------------------------------------------
  ' PURPOSE : Inserts the given object to the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES :
  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Return Category_Insert(SqlCtx)

  End Function ' DB_Insert

  '----------------------------------------------------------------------------
  ' PURPOSE : Reads from the database into the given object
  '
  ' PARAMS :
  '     - INPUT :
  '         - ObjectId: An object, it must be 'casted' to the desired KEY.
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '
  ' NOTES :
  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Me.CategoryId = ObjectId

    Return ReadCategory()

  End Function ' DB_Read

  '----------------------------------------------------------------------------
  ' PURPOSE : Updates the given object to the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES :
  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Return Category_Update(SqlCtx)

  End Function ' DB_Update

  '----------------------------------------------------------------------------
  ' PURPOSE: Returns a duplicate of the given object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim _target As CLS_GAMES_CATEGORY

    _target = New CLS_GAMES_CATEGORY

    _target.Title = Me.Title
    _target.Enabled = Me.Enabled
    _target.Description = Me.Description

    ''_target.LastUpdate = Me.LastUpdate

    If Not Me.Image Is Nothing Then
      _target.Image = Me.Image.Clone()
    Else
      _target.Image = Nothing
    End If

    Return _target

  End Function ' Duplicate

#End Region ' Overrides

#Region " Private Functions "

  '----------------------------------------------------------------------------
  ' PURPOSE: Reads an root object  from the database.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES:
  Private Function ReadCategory() As Integer

    Dim _sb As StringBuilder
    Dim _table As DataTable
    Try
      _sb = New StringBuilder()

      _sb.AppendLine(" SELECT GEC_GAME_EXTERNAL_CATEGORIES_ID ")
      _sb.AppendLine("         ,GEC_TITLE ")
      _sb.AppendLine("         ,GEC_DESCRIPTION ")
      _sb.AppendLine("         ,GEC_ACTIVE ")
      _sb.AppendLine("         ,GEC_LAST_UPDATE_DATE ")
      _sb.AppendLine("         ,GEC_IMAGE ")
      _sb.AppendLine(" FROM MAPP_GAMES_EXTERNAL_CATEGORIES ")
      _sb.AppendLine(" WHERE   GEC_GAME_EXTERNAL_CATEGORIES_ID = " + Me.m_category.category_id.ToString())

      _table = GUI_GetTableUsingSQL(_sb.ToString(), 5000)

      If IsNothing(_table) Then
        Return ENUM_STATUS.STATUS_ERROR
      End If

      If _table.Rows.Count() > 1 Then
        Return ENUM_STATUS.STATUS_ERROR
      End If

      If _table.Rows.Count() = 1 Then
        Me.CategoryId = IIf(IsDBNull(_table.Rows(0).Item("GEC_GAME_EXTERNAL_CATEGORIES_ID")), 0, _table.Rows(0).Item("GEC_GAME_EXTERNAL_CATEGORIES_ID"))
        Me.Title = IIf(IsDBNull(_table.Rows(0).Item("GEC_TITLE")), "", _table.Rows(0).Item("GEC_TITLE"))
        Me.Description = IIf(IsDBNull(_table.Rows(0).Item("GEC_DESCRIPTION")), "", _table.Rows(0).Item("GEC_DESCRIPTION"))
        Me.Enabled = IIf(IsDBNull(_table.Rows(0).Item("GEC_ACTIVE")), "", _table.Rows(0).Item("GEC_ACTIVE"))
        Me.LastUpdate = IIf(IsDBNull(_table.Rows(0).Item("GEC_LAST_UPDATE_DATE")), DateTime.MinValue, _table.Rows(0).Item("GEC_LAST_UPDATE_DATE"))
        If Not IsDBNull(_table.Rows(0).Item("GEC_IMAGE")) Then
          Dim imgData As Byte() = _table.Rows(0).Item("GEC_IMAGE")
          Using ms As New MemoryStream(imgData)
            Me.Image = Image.FromStream(ms)
          End Using
        Else
          Me.Image = Nothing
        End If
      End If

      Return ENUM_STATUS.STATUS_OK

    Catch ex As Exception
      Return ENUM_STATUS.STATUS_ERROR
    End Try
  End Function ' ReadCatalogs


  '----------------------------------------------------------------------------
  ' PURPOSE: Insert the given object into the database.
  '
  ' PARAMS:
  '   - INPUT:  None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES:
  Private Function Category_Insert(ByVal Context As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _str_sql As String
    Dim _db_count As Integer
    Dim _num_rows_updated As Integer
    Dim _param As SqlClient.SqlParameter

    _db_count = 0

    Try

      Using _db_trx As New DB_TRX()

        ' INSERT Catalog
        _str_sql = "INSERT INTO MAPP_GAMES_EXTERNAL_CATEGORIES " & _
                   "(GEC_TITLE" & _
                   ",GEC_DESCRIPTION" & _
                   ",GEC_LAST_UPDATE_DATE" & _
                   ",GEC_IMAGE" & _
                   ",GEC_ACTIVE)" & _
                              "   VALUES" & _
                              " (  " & _
                              "   @cat_title " & _
                              " , @cat_description " & _
                              " , @cat_last_update " & _
                              " , @cat_image " & _
                              " , @cat_enabled " & _
                             " ) " & _
        "SET @cat_id = SCOPE_IDENTITY()"

        Using _cmd As New SqlClient.SqlCommand(_str_sql, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _cmd.Parameters.Add("@cat_title", SqlDbType.NVarChar).Value = m_category.title
          _cmd.Parameters.Add("@cat_description", SqlDbType.NVarChar).Value = m_category.description
          _cmd.Parameters.Add("@cat_last_update", SqlDbType.DateTime).Value = WSI.Common.WGDB.Now
          _cmd.Parameters.Add("@cat_enabled", SqlDbType.Bit).Value = m_category.enabled

          If Not IsNothing(m_category.image) Then
            Dim img As New ImageConverter()
            Dim imgData As Byte() = img.ConvertTo(m_category.image, GetType(Byte()))
            _cmd.Parameters.Add("@cat_image", SqlDbType.VarBinary).Value = imgData
          Else
            _cmd.Parameters.Add("@cat_image", SqlDbType.VarBinary).Value = DBNull.Value
          End If

          _param = _cmd.Parameters.Add("@cat_id", SqlDbType.BigInt)
          _param.Direction = ParameterDirection.Output

          _num_rows_updated = _cmd.ExecuteNonQuery()
        End Using
          

        If _num_rows_updated <> 1 Then
          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

        If _num_rows_updated > 0 Then
          m_category.category_id = _param.Value
          ' Min one row
          _db_trx.Commit()
        Else
          _db_trx.Rollback()
          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

      End Using '_db_trx

    Catch ex As Exception
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

  End Function ' Catalog_Insert

  '----------------------------------------------------------------------------
  ' PURPOSE: Update the given object into the database.
  '
  ' PARAMS:
  '   - INPUT:  None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES:
  Private Function Category_Update(ByVal Context As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _str_sql As String
    Dim _num_rows_updated As Integer


    Try

      Using _db_trx As New DB_TRX()

        ' UPDATE Catalogs 
        _str_sql = "UPDATE  MAPP_GAMES_EXTERNAL_CATEGORIES " & _
                    "   SET   GEC_TITLE = @cat_title " & _
                    "      , GEC_DESCRIPTION = @cat_description " & _
                    "      , GEC_ACTIVE = @cat_enabled  " & _
                    "      , GEC_IMAGE = @cat_image  " & _
                    "      , GEC_LAST_UPDATE_DATE = @cat_last_update  " & _
                    "  WHERE GEC_GAME_EXTERNAL_CATEGORIES_ID   = @cat_id "

        Using _cmd As New SqlClient.SqlCommand(_str_sql, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _cmd.Parameters.Add("@cat_id", SqlDbType.BigInt).Value = m_category.category_id
          _cmd.Parameters.Add("@cat_title", SqlDbType.NVarChar).Value = m_category.title
          _cmd.Parameters.Add("@cat_description", SqlDbType.NVarChar).Value = m_category.description
          _cmd.Parameters.Add("@cat_last_update", SqlDbType.DateTime).Value = WSI.Common.WGDB.Now
          _cmd.Parameters.Add("@cat_enabled", SqlDbType.Bit).Value = m_category.enabled

          If Not IsNothing(m_category.image) Then
            Dim img As New ImageConverter()
            Dim imgData As Byte() = img.ConvertTo(m_category.image, GetType(Byte()))
            _cmd.Parameters.Add("@cat_image", SqlDbType.VarBinary).Value = imgData
          Else
            _cmd.Parameters.Add("@cat_image", SqlDbType.VarBinary).Value = DBNull.Value
          End If

          _num_rows_updated = _cmd.ExecuteNonQuery()

        End Using

        'UPDATE Catalog Items


        If _num_rows_updated > 0 Then
          ' Min one row
          _db_trx.Commit()
        Else
          _db_trx.Rollback()
          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

      End Using '_db_trx

    Catch ex As Exception
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

  End Function ' Catalog_Update


  ' PURPOSE : Exist name of Catalog
  '
  ' PARAMS :
  '     - INPUT :
  '         - name
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK  -> Not Exists
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DUPLICATE_KEY -> Exist
  '
  ' NOTES :
  Public Function ExistsCategoryName(ByVal categoryTitle As String, ByVal currentId As Long) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _value As Object
    Dim _db_count As Integer
    Dim _str_sql As String

    Using _db_trx As New DB_TRX()

      ' CHECK If name already exists
      _str_sql = "SELECT  COUNT(*) " & _
                " FROM MAPP_GAMES_EXTERNAL_CATEGORIES  " & _
              "  WHERE   GEC_TITLE = @pName AND GEC_GAME_EXTERNAL_CATEGORIES_ID <> @pId"

      Using _cmd As New SqlClient.SqlCommand(_str_sql, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
        _cmd.Parameters.Add("@pName", SqlDbType.NVarChar).Value = categoryTitle
        _cmd.Parameters.Add("@pId", SqlDbType.BigInt).Value = currentId

        _value = _cmd.ExecuteScalar()
      End Using

    End Using

    If _value IsNot Nothing And _value IsNot DBNull.Value Then
      _db_count = CInt(_value)
    Else
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    If _db_count > 0 Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
    Else
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
    End If


  End Function ' ExistsCatalogName

#End Region ' Private Functions

End Class
