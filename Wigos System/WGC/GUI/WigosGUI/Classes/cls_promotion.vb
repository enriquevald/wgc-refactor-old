'-------------------------------------------------------------------
' Copyright � 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_promotion.vb
' DESCRIPTION:   Promotion class for user edition
' AUTHOR:        Raul Cervera
' CREATION DATE: 11-MAY-2010
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 11-MAY-2010  RCI    Initial version.
' 18-JAN-2012  RCI    Add support for ProviderList Filter.
' 23-MAY-2012  MPO    Set or Get the image and icon of the gift. Save/Retrieve of the db.
' 04-JUN-2012  JMM    Add fields and audit changes to support periodical promotions.
' 05-JUN-2012  DDM    Modified enum PROMOTION_TYPE.
' 31-JUL-2012  MPO    Added new field PM_CREDIT_TYPE.
' 20-AUG-2012  JAB    Added Promotion Global Limit.
' 22-AUG-2012  MPO    Promotion preassigned --> Added properties that contains the preassigned accounts 
' 10-SEP-2012  HBB    Added the ticket_footer property to the TYPE_PROMOTION structure. It was also added the corresponding field for update and read methods
' 25-SEP-2012  QMP    Added Promotion Category
' 03-OCT-2012  XCD    Added Promotion Flags
' 12-APR-2013  DHA    Added Expiration Limit
' 23-APR-2013  QMP    Fixed Bug #715: Already awarded promotions can be deleted
' 26-APR-2013  SMN    Hide Player Tracking data if exists an External Loyalty 
' 24-MAY-2013  HBB    The property LevelFilterText was moved to mld_misc to make it accesible from other sites
' 10-JUN-2013  RBG    Fixed Bug #786: Error when selecting a provider with character: '
' 19-JUN-2013  RRR    Fixed Bug #878: Error adding images to copied promotions
' 28-AUG-2013  BBA    Added new fields Daily, Monthly and Global used promotion credit
' 29-AUG-2013  RBG & AMF Change ProviderList Audition And create new field: pm_restricted_to_terminal_list
' 08-OCT-2013  FBA    Added new filters by age range
' 05-NOV-2013  JMN & RMS   Added AwardOnPromoBOX and PromoBOX description.
' 21-NOV-2013  JPJ    Defect WIG-426 Can't create a periodic promotion. 
' 07-APR-2014  JBP    Added IsVip property.
' 02-JUN-2014  XCD    Add comment property
' 11-APR-2014  JCO    Fixed Bug WIG-1072: It doesn't save changes when we are editing system's promotions.
' 09-SEP-2015  AVZ    Backlog Item #3696: Added new filter by created account date
' 01-OCT-2015  AVZ    Backlog Item #4753: Edit filter by created account date - New requirements
' 16-OCT-2015  SDS    Backlog Item 3855: New promotion by played
' 04-NOV-2015  GMV    PBI 5836 - Garcia River - Discernir promos visibles en PromoBOX y Display Touch
' 07-JAN-2016  DLL    Hide Promotions by played. Not implemented
' 28-FEB-2017  AMF    Bug 25152:Egasa Mallorca: sin conexi�n a la base de datos al modificar una promoci�n a�adiendo decimales
'-------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports WSI.Common
Imports System.Runtime.InteropServices
Imports System.Text
Imports System.Data.SqlClient

Public Class CLASS_PROMOTION
  Inherits CLASS_BASE

#Region " Constants "

  ' Fields length
  Public Const MAX_NAME_LEN As Integer = 50

  Public Const PM_TYPE_USER As Integer = 0
  Public Const PM_TYPE_SYSTEM As Integer = 1

#End Region ' Constants

#Region " Enums "

  Public Enum ENUM_GENDER_FILTER
    NOT_SET = 0
    MEN_ONLY = 1
    WOMEN_ONLY = 2
  End Enum

  Public Enum ENUM_BIRTHDAY_FILTER
    NOT_SET = 0
    DAY_ONLY = 1
    WHOLE_MONTH = 2
    INCLUDE_AGE = 3
    EXCLUDE_AGE = 4
  End Enum

  Public Enum ENUM_CREATED_ACCOUNT_DATE_FILTER
    NOT_SET = 0
    WORKING_DAY_ONLY = 1
    WORKING_DAYS_INCLUDED = 2
    ANNIVERSARY_WHOLE_MONTH = 3
    ANNIVERSARY_WORKING_DAY_ONLY = 4
  End Enum

  Public Enum ENUM_EXPIRATION_TYPE
    NOT_SET = 0
    DAYS = 1
    HOURS = 2
  End Enum

  Public Enum ENUM_SPECIAL_PERMISSION
    NOT_SET = 0
    PERMISSION_A = 1
    PERMISSION_B = 2
  End Enum

  Public Enum ENUM_PROMO_SCREEN_MODE
    MODE_EDIT = 0
    MODE_NEW = 1
    MODE_DELETE = 2
  End Enum

#End Region ' Enums 

#Region " GUI_Configuration.dll "

#Region " Structures "

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_PROMOTION
    Public promotion_id As Integer
    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_NAME_LEN + 1)> _
    Public name As String
    Public enabled As Integer
    Public special_permission As Integer
    Public type As WSI.Common.Promotion.PROMOTION_TYPE
    Public date_start As New TYPE_DATE_TIME
    Public date_finish As New TYPE_DATE_TIME
    Public schedule_weekday As Integer
    Public schedule1_time_from As Integer
    Public schedule1_time_to As Integer
    Public schedule2_enabled As Integer
    Public schedule2_time_from As Integer
    Public schedule2_time_to As Integer
    Public gender_filter As Integer
    Public birthday_filter As Integer
    Public age_filter As Integer
    Public birthday_age_from As Integer
    Public birthday_age_to As Integer

    ' AVZ 01-OCT-2015
    Public created_account_filter As Integer
    Public created_account_working_days_included As Integer
    Public created_account_anniversary_year_from As Integer
    Public created_account_anniversary_year_to As Integer

    Public level_filter As Integer
    Public freq_filter_last_days As Integer
    Public freq_filter_min_days As Integer
    Public freq_filter_min_cash_in As Double
    Public expiration_type As Integer
    Public expiration_value As Integer
    Public expiration_limit As New TYPE_DATE_TIME
    Public min_cash_in As Double
    Public min_cash_in_reward As Double
    Public cash_in As Double
    Public cash_in_reward As Double
    Public won_lock_enabled As Boolean
    Public won_lock As Double
    Public num_tokens As Integer
    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_NAME_LEN + 1)> _
    Public token_name As String
    Public token_reward As Double
    Public daily_limit As Double
    Public monthly_limit As Double
    Public global_daily_limit As Double
    Public global_monthly_limit As Double
    Public global_limit As Double
    Public min_spent As Double
    Public min_spent_reward As Double
    Public spent As Double
    Public spent_reward As Double
    Public min_played As Double
    Public min_played_reward As Double
    Public played As Double
    Public played_reward As Double
    Public provider_list As New ProviderList
    Public terminal_list As New TerminalList
    Public terminal_list_restricted As New TerminalList
    Public terminal_list_played As New TerminalList
    Public large_resource_id As Nullable(Of Long)
    Public small_resource_id As Nullable(Of Long)
    Public image As Image
    Public icon As Image
    Public credit_type As WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE
    Public accounts_preassigned As DataTable
    Public ticket_footer As String
    Public category_id As Integer
    Public flags_required As DataTable
    Public flags_awarded As DataTable
    Public promo_screen_mode As Integer
    Public visible_on_PromoBOX As Integer
    Public change_table_name As String
    Public award_on_PromoBOX As Integer
    Public text_on_PromoBOX As String
    Public is_vip As Boolean
    Public apply_tax As Boolean
    Public comments As String
    Public promogame As Long
    Public pyramidal_dist As String
    Public journey_limit As Boolean

  End Class

#End Region ' Structures

#End Region

#Region " Members "

  Protected m_promotion As New TYPE_PROMOTION


#End Region ' Members

#Region " Properties "

  Public Property PromotionId() As Integer
    Get
      Return m_promotion.promotion_id
    End Get
    Set(ByVal Value As Integer)
      m_promotion.promotion_id = Value
    End Set
  End Property

  Public Property Name() As String
    Get
      Return m_promotion.name
    End Get
    Set(ByVal Value As String)
      m_promotion.name = Value
    End Set
  End Property

  Public Property Enabled() As Boolean
    Get
      Return m_promotion.enabled
    End Get
    Set(ByVal Value As Boolean)
      m_promotion.enabled = Value
    End Set
  End Property

  Public Property SpecialPermission() As Integer
    Get
      Return m_promotion.special_permission
    End Get
    Set(ByVal Value As Integer)
      m_promotion.special_permission = Value
    End Set
  End Property

  Public Property Type() As WSI.Common.Promotion.PROMOTION_TYPE
    Get
      Return m_promotion.type
    End Get
    Set(ByVal Value As WSI.Common.Promotion.PROMOTION_TYPE)
      m_promotion.type = Value
    End Set
  End Property

  Public Property DateStart() As Date
    Get
      If m_promotion.date_start.IsNull Then
        Return GUI_GetDate()
      Else
        Return m_promotion.date_start.Value
      End If
    End Get
    Set(ByVal Value As Date)
      If IsNothing(m_promotion.date_start) Then
        m_promotion.date_start = New TYPE_DATE_TIME
      End If
      m_promotion.date_start.Value = Value
    End Set
  End Property

  Public Property DateFinish() As Date
    Get
      If m_promotion.date_finish.IsNull Then
        Return GUI_GetDate()
      Else
        Return m_promotion.date_finish.Value
      End If
    End Get
    Set(ByVal Value As Date)
      If IsNothing(m_promotion.date_finish) Then
        m_promotion.date_finish = New TYPE_DATE_TIME
      End If
      m_promotion.date_finish.Value = Value
    End Set
  End Property

  Public Property ScheduleWeekday() As Integer
    Get
      Return m_promotion.schedule_weekday
    End Get
    Set(ByVal Value As Integer)
      m_promotion.schedule_weekday = Value
    End Set
  End Property

  Public Property Schedule1TimeFrom() As Integer
    Get
      Return m_promotion.schedule1_time_from
    End Get
    Set(ByVal Value As Integer)
      m_promotion.schedule1_time_from = Value
    End Set
  End Property

  Public Property Schedule1TimeTo() As Integer
    Get
      Return m_promotion.schedule1_time_to
    End Get
    Set(ByVal Value As Integer)
      m_promotion.schedule1_time_to = Value
    End Set
  End Property

  Public Property Schedule2Enabled() As Boolean
    Get
      Return m_promotion.schedule2_enabled
    End Get
    Set(ByVal Value As Boolean)
      m_promotion.schedule2_enabled = Value
    End Set
  End Property

  Public Property Schedule2TimeFrom() As Integer
    Get
      Return m_promotion.schedule2_time_from
    End Get
    Set(ByVal Value As Integer)
      m_promotion.schedule2_time_from = Value
    End Set
  End Property

  Public Property Schedule2TimeTo() As Integer
    Get
      Return m_promotion.schedule2_time_to
    End Get
    Set(ByVal Value As Integer)
      m_promotion.schedule2_time_to = Value
    End Set
  End Property

  Public Property GenderFilter() As Integer
    Get
      Return m_promotion.gender_filter
    End Get
    Set(ByVal Value As Integer)
      m_promotion.gender_filter = Value
    End Set
  End Property

  Public Property BirthdayFilter() As Integer
    Get
      Return m_promotion.birthday_filter
    End Get
    Set(ByVal Value As Integer)
      m_promotion.birthday_filter = Value
    End Set
  End Property

  Public Property AgeFilter() As Integer
    Get
      Return m_promotion.age_filter
    End Get
    Set(ByVal Value As Integer)
      m_promotion.age_filter = Value
    End Set
  End Property

  Public Property BirthdayAgeFrom() As Integer
    Get
      Return m_promotion.birthday_age_from
    End Get
    Set(ByVal Value As Integer)
      m_promotion.birthday_age_from = Value
    End Set
  End Property

  Public Property BirthdayAgeTo() As Integer
    Get
      Return m_promotion.birthday_age_to
    End Get
    Set(ByVal Value As Integer)
      m_promotion.birthday_age_to = Value
    End Set
  End Property

  ' AVZ 01-OCT-2015
  Public Property CreatedAccountFilter() As Integer
    Get
      Return m_promotion.created_account_filter
    End Get
    Set(ByVal Value As Integer)
      m_promotion.created_account_filter = Value
    End Set
  End Property

  Public Property CreatedAccountWorkingDaysIncluded() As Integer
    Get
      Return m_promotion.created_account_working_days_included
    End Get
    Set(ByVal Value As Integer)
      m_promotion.created_account_working_days_included = Value
    End Set
  End Property

  Public Property CreatedAccountAnniversaryYearFrom() As Integer
    Get
      Return m_promotion.created_account_anniversary_year_from
    End Get
    Set(ByVal Value As Integer)
      m_promotion.created_account_anniversary_year_from = Value
    End Set
  End Property

  Public Property CreatedAccountAnniversaryYearTo() As Integer
    Get
      Return m_promotion.created_account_anniversary_year_to
    End Get
    Set(ByVal Value As Integer)
      m_promotion.created_account_anniversary_year_to = Value
    End Set
  End Property

  Public Property LevelFilter() As Integer
    Get
      Return m_promotion.level_filter
    End Get
    Set(ByVal Value As Integer)
      m_promotion.level_filter = Value
    End Set
  End Property

  Public Property FreqFilterLastDays() As Integer
    Get
      Return m_promotion.freq_filter_last_days
    End Get
    Set(ByVal Value As Integer)
      m_promotion.freq_filter_last_days = Value
    End Set
  End Property

  Public Property FreqFilterMinDays() As Integer
    Get
      Return m_promotion.freq_filter_min_days
    End Get
    Set(ByVal Value As Integer)
      m_promotion.freq_filter_min_days = Value
    End Set
  End Property

  Public Property FreqFilterMinCashIn() As Double
    Get
      Return m_promotion.freq_filter_min_cash_in
    End Get
    Set(ByVal Value As Double)
      m_promotion.freq_filter_min_cash_in = Value
    End Set
  End Property

  Public Property ExpirationType() As Integer
    Get
      Return m_promotion.expiration_type
    End Get
    Set(ByVal Value As Integer)
      m_promotion.expiration_type = Value
    End Set
  End Property

  Public Property ExpirationValue() As Integer
    Get
      Return m_promotion.expiration_value
    End Get
    Set(ByVal Value As Integer)
      m_promotion.expiration_value = Value
    End Set
  End Property

  Public Property MinCashIn() As Double
    Get
      Return m_promotion.min_cash_in
    End Get
    Set(ByVal Value As Double)
      m_promotion.min_cash_in = Value
    End Set
  End Property

  Public Property MinCashInReward() As Double
    Get
      Return m_promotion.min_cash_in_reward
    End Get
    Set(ByVal Value As Double)
      m_promotion.min_cash_in_reward = Value
    End Set
  End Property

  Public Property CashIn() As Double
    Get
      Return m_promotion.cash_in
    End Get
    Set(ByVal Value As Double)
      m_promotion.cash_in = Value
    End Set
  End Property

  Public Property CashInReward() As Double
    Get
      Return m_promotion.cash_in_reward
    End Get
    Set(ByVal Value As Double)
      m_promotion.cash_in_reward = Value
    End Set
  End Property

  Public Property WonLockEnabled() As Boolean
    Get
      Return m_promotion.won_lock_enabled
    End Get
    Set(ByVal Value As Boolean)
      m_promotion.won_lock_enabled = Value
    End Set
  End Property

  Public Property WonLock() As Double
    Get
      Return m_promotion.won_lock
    End Get
    Set(ByVal Value As Double)
      m_promotion.won_lock = Value
    End Set
  End Property

  Public Property NumTokens() As Integer
    Get
      Return m_promotion.num_tokens
    End Get
    Set(ByVal Value As Integer)
      m_promotion.num_tokens = Value
    End Set
  End Property

  Public Property TokenName() As String
    Get
      Return m_promotion.token_name
    End Get
    Set(ByVal Value As String)
      m_promotion.token_name = Value
    End Set
  End Property

  Public Property TokenReward() As Double
    Get
      Return m_promotion.token_reward
    End Get
    Set(ByVal Value As Double)
      m_promotion.token_reward = Value
    End Set
  End Property

  Public Property DailyLimit() As Double
    Get
      Return m_promotion.daily_limit
    End Get
    Set(ByVal Value As Double)
      m_promotion.daily_limit = Value
    End Set
  End Property

  Public Property MonthlyLimit() As Double
    Get
      Return m_promotion.monthly_limit
    End Get
    Set(ByVal Value As Double)
      m_promotion.monthly_limit = Value
    End Set
  End Property

  Public Property GlobalDailyLimit() As Double
    Get
      Return m_promotion.global_daily_limit
    End Get
    Set(ByVal value As Double)
      m_promotion.global_daily_limit = value
    End Set
  End Property

  Public Property GlobalMonthlyLimit() As Double
    Get
      Return m_promotion.global_monthly_limit
    End Get
    Set(ByVal value As Double)
      m_promotion.global_monthly_limit = value
    End Set
  End Property

  Public Property GlobalLimit() As Double
    Get
      Return m_promotion.global_limit
    End Get
    Set(ByVal value As Double)
      m_promotion.global_limit = value
    End Set
  End Property

  Public Property MinSpent() As Double
    Get
      Return m_promotion.min_spent
    End Get
    Set(ByVal Value As Double)
      m_promotion.min_spent = Value
    End Set
  End Property

  Public Property MinSpentReward() As Double
    Get
      Return m_promotion.min_spent_reward
    End Get
    Set(ByVal Value As Double)
      m_promotion.min_spent_reward = Value
    End Set
  End Property

  Public Property Spent() As Double
    Get
      Return m_promotion.spent
    End Get
    Set(ByVal Value As Double)
      m_promotion.spent = Value
    End Set
  End Property

  Public Property SpentReward() As Double
    Get
      Return m_promotion.spent_reward
    End Get
    Set(ByVal Value As Double)
      m_promotion.spent_reward = Value
    End Set
  End Property

  Public Property MinPlayed() As Double
    Get
      Return m_promotion.min_played
    End Get
    Set(ByVal Value As Double)
      m_promotion.min_played = Value
    End Set
  End Property

  Public Property MinPlayedReward() As Double
    Get
      Return m_promotion.min_played_reward
    End Get
    Set(ByVal Value As Double)
      m_promotion.min_played_reward = Value
    End Set
  End Property

  Public Property Played() As Double
    Get
      Return m_promotion.played
    End Get
    Set(ByVal Value As Double)
      m_promotion.played = Value
    End Set
  End Property

  Public Property PlayedReward() As Double
    Get
      Return m_promotion.played_reward
    End Get
    Set(ByVal Value As Double)
      m_promotion.played_reward = Value
    End Set
  End Property

  Public ReadOnly Property ProviderList() As ProviderList
    Get
      Return m_promotion.provider_list
    End Get
  End Property

  Public ReadOnly Property TerminalList() As TerminalList
    Get
      Return m_promotion.terminal_list
    End Get
  End Property

  ' AMF 29-AUG-2013
  Public ReadOnly Property TerminalListRestricted() As TerminalList
    Get
      Return m_promotion.terminal_list_restricted
    End Get
  End Property

  Public ReadOnly Property TerminalListPlayed() As TerminalList
    Get
      Return m_promotion.terminal_list_played
    End Get
  End Property

  Public Property VisibleOnPromoBOX() As Boolean
    Get
      Return m_promotion.visible_on_PromoBOX
    End Get
    Set(ByVal Value As Boolean)
      m_promotion.visible_on_PromoBOX = Value
    End Set
  End Property

  Public Property AwardOnPromoBOX() As PROMOTION_AWARD_TARGET
    Get
      Return m_promotion.award_on_PromoBOX
    End Get
    Set(ByVal Value As PROMOTION_AWARD_TARGET)
      m_promotion.award_on_PromoBOX = Value
    End Set
  End Property

  ' DHA 09-APR-2013: 'Expiration Limit' field added
  Public Property ExpirationLimit() As Date
    Get
      If m_promotion.expiration_limit.IsNull Then
        Return GUI_GetDate()
      Else
        Return m_promotion.expiration_limit.Value
      End If
    End Get
    Set(ByVal Value As Date)
      If IsNothing(m_promotion.expiration_limit) Then
        m_promotion.expiration_limit = New TYPE_DATE_TIME
      End If
      m_promotion.expiration_limit.Value = Value
    End Set
  End Property

  Public ReadOnly Property WorkingDaysText(ByVal Days As Integer, ByVal Type As Promotion.PROMOTION_TYPE) As String
    Get
      Dim _str_binary As String
      Dim _bit_array As Char()
      Dim _str_days As String

      _str_days = ""

      If Type = Promotion.PROMOTION_TYPE.MANUAL Or Type = Promotion.PROMOTION_TYPE.PREASSIGNED Then
        _str_binary = Convert.ToString(Days, 2)
        _str_binary = New String("0", 7 - _str_binary.Length) + _str_binary

        _bit_array = _str_binary.ToCharArray()

        If (_bit_array(6) = "1") Then _str_days = _str_days & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(325) '"Monday "
        If (_bit_array(5) = "1") Then _str_days = _str_days & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(326) '"Tuesday "
        If (_bit_array(4) = "1") Then _str_days = _str_days & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(327) '"Wednesday "
        If (_bit_array(3) = "1") Then _str_days = _str_days & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(328) '"Thursday "
        If (_bit_array(2) = "1") Then _str_days = _str_days & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(329) '"Friday "
        If (_bit_array(1) = "1") Then _str_days = _str_days & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(330) '"Saturday "
        If (_bit_array(0) = "1") Then _str_days = _str_days & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(331) '"Sunday "

      ElseIf Type = Promotion.PROMOTION_TYPE.PERIODIC_PER_LEVEL Or _
             Type = Promotion.PROMOTION_TYPE.PERIODIC_PER_LEVEL_AND_MIN_PLAYED Then

        _str_days = ScheduleDay.GetMonthDay(Days) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(447) ' 447 "de cada mes"

      ElseIf Type = Promotion.PROMOTION_TYPE.PERIODIC_PER_BIRTHDAY Then

        _str_days = GLB_NLS_GUI_PLAYER_TRACKING.GetString(819) ' 819 "En el cumplea�os del cliente"

      End If

      Return _str_days.TrimStart

    End Get
  End Property

  Public Property TicketFooter() As String
    Get
      Return m_promotion.ticket_footer
    End Get
    Set(ByVal Value As String)
      m_promotion.ticket_footer = Value
    End Set
  End Property

  Public Property Image() As Image
    Get
      Return Me.m_promotion.image
    End Get
    Set(ByVal value As Image)
      Me.m_promotion.image = value
    End Set
  End Property

  Public Property Icon() As Image
    Get
      Return Me.m_promotion.icon
    End Get
    Set(ByVal value As Image)
      Me.m_promotion.icon = value
    End Set
  End Property

  Public Property CreditType() As WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE
    Get
      Return Me.m_promotion.credit_type
    End Get
    Set(ByVal value As WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE)
      Me.m_promotion.credit_type = value
    End Set
  End Property

  Public Property AccountsPreassigned() As DataTable
    Get
      Return Me.m_promotion.accounts_preassigned
    End Get
    Set(ByVal value As DataTable)
      Me.m_promotion.accounts_preassigned = value
    End Set
  End Property

  Public Property CategoryId() As Integer
    Get
      Return m_promotion.category_id
    End Get
    Set(ByVal Value As Integer)
      m_promotion.category_id = Value
    End Set
  End Property

  Public Property FlagsRequired() As DataTable
    Get
      Return m_promotion.flags_required
    End Get
    Set(ByVal Value As DataTable)
      m_promotion.flags_required = Value
    End Set
  End Property

  Public Property FlagsAwarded() As DataTable
    Get
      Return m_promotion.flags_awarded
    End Get
    Set(ByVal Value As DataTable)
      m_promotion.flags_awarded = Value
    End Set
  End Property

  Public Property PromoScreenMode() As Integer
    Get
      Return m_promotion.promo_screen_mode
    End Get
    Set(ByVal value As Integer)
      m_promotion.promo_screen_mode = value
    End Set
  End Property

  Public ReadOnly Property BelongsToRedemptionTable() As Boolean
    Get
      If String.IsNullOrEmpty(Me.m_promotion.change_table_name) Then
        Return False
      Else
        Return m_promotion.change_table_name.Length > 0
      End If
    End Get
  End Property

  Public ReadOnly Property ChangeTableName() As String
    Get
      Return m_promotion.change_table_name
    End Get
  End Property

  Public Property TextOnPromoBOX() As String
    Get
      Return m_promotion.text_on_PromoBOX
    End Get
    Set(ByVal Value As String)
      m_promotion.text_on_PromoBOX = Value
    End Set
  End Property

  Public Property IsVip() As Boolean
    Get
      Return m_promotion.is_vip
    End Get
    Set(ByVal Value As Boolean)
      m_promotion.is_vip = Value
    End Set
  End Property


  Public Property ApplyTax() As Boolean
    Get
      Return m_promotion.apply_tax
    End Get
    Set(ByVal Value As Boolean)
      m_promotion.apply_tax = Value
    End Set
  End Property


  Public Property Comments() As String
    Get
      Return m_promotion.comments
    End Get
    Set(ByVal value As String)
      m_promotion.comments = value
    End Set
  End Property

  Public Property Promogame() As Long
    Get
      Return m_promotion.promogame
    End Get
    Set(ByVal value As Long)
      m_promotion.promogame = value
    End Set
  End Property

  Public Property JourneyLimit() As Boolean
    Get
      Return m_promotion.journey_limit
    End Get
    Set(ByVal value As Boolean)
      m_promotion.journey_limit = value
    End Set
  End Property

  Public Property PyramidalDist() As String
    Get
      Return m_promotion.pyramidal_dist
    End Get
    Set(ByVal value As String)
      m_promotion.pyramidal_dist = value
    End Set
  End Property

#End Region ' Properties

#Region " Overrides functions "
  ' Constructor
  Public Sub New()
    ' Create datatables
    FlagsAwarded = New DataTable()
    FlagsAwarded.Columns.Add("PF_FLAG_ID")
    FlagsAwarded.Columns.Add("PF_FLAG_COUNT")
    FlagsAwarded.PrimaryKey = New DataColumn() {FlagsAwarded.Columns("PF_FLAG_ID")}
    FlagsRequired = New DataTable()
    FlagsRequired.Columns.Add("PF_FLAG_ID")
    FlagsRequired.Columns.Add("PF_FLAG_COUNT")
    FlagsRequired.PrimaryKey = New DataColumn() {FlagsRequired.Columns("PF_FLAG_ID")}

    ' DHA 15-Apr-2013: Initialize expiration limit
    m_promotion.expiration_limit.IsNull = True
    m_promotion.expiration_limit.Value = DateTime.MinValue

  End Sub


  '----------------------------------------------------------------------------
  ' PURPOSE: Reads from the database into the given object
  '
  ' PARAMS:
  '   - INPUT:
  '     - ObjectId: An object, it must be 'casted' to the desired KEY.
  '
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim rc As Integer

    Me.PromotionId = ObjectId

    ' Read promotion data
    rc = ReadPromotion(m_promotion, SqlCtx)

    Select Case rc

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Read

  '----------------------------------------------------------------------------
  ' PURPOSE: Updates the given object to the database.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim rc As Integer

    ' Update promotion data
    rc = UpdatePromotion(m_promotion, SqlCtx)

    Select Case rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_PASSWORD
        Return CLASS_BASE.ENUM_STATUS.STATUS_PASSWORD

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Update

  '----------------------------------------------------------------------------
  ' PURPOSE: Inserts the given object to the database.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim rc As Integer

    ' Insert promotion data
    rc = InsertPromotion(m_promotion, SqlCtx)

    Select Case rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_PASSWORD
        Return CLASS_BASE.ENUM_STATUS.STATUS_PASSWORD

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
        Return ENUM_STATUS.STATUS_DUPLICATE_KEY

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Insert

  '----------------------------------------------------------------------------
  ' PURPOSE: Deletes the given object from the database.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim rc As Integer

    ' Delete promotion data
    rc = DeletePromotion(Me.PromotionId, Me.m_promotion.small_resource_id, Me.m_promotion.large_resource_id, SqlCtx)

    Select Case rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DEPENDENCIES
        Return ENUM_STATUS.STATUS_DEPENDENCIES

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Delete

  '----------------------------------------------------------------------------
  ' PURPOSE: Returns a duplicate of the given object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim temp_promo As CLASS_PROMOTION

    temp_promo = New CLASS_PROMOTION
    temp_promo.m_promotion.promotion_id = Me.m_promotion.promotion_id
    temp_promo.m_promotion.name = Me.m_promotion.name
    temp_promo.m_promotion.enabled = Me.m_promotion.enabled
    temp_promo.m_promotion.special_permission = Me.m_promotion.special_permission
    temp_promo.m_promotion.type = Me.m_promotion.type

    If Me.m_promotion.date_start.IsNull Then
      temp_promo.m_promotion.date_start.IsNull = True
    Else
      temp_promo.m_promotion.date_start.Value = Me.m_promotion.date_start.Value
    End If
    If Me.m_promotion.date_finish.IsNull Then
      temp_promo.m_promotion.date_finish.IsNull = True
    Else
      temp_promo.m_promotion.date_finish.Value = Me.m_promotion.date_finish.Value
    End If

    temp_promo.m_promotion.schedule_weekday = Me.m_promotion.schedule_weekday
    temp_promo.m_promotion.schedule1_time_from = Me.m_promotion.schedule1_time_from
    temp_promo.m_promotion.schedule1_time_to = Me.m_promotion.schedule1_time_to
    temp_promo.m_promotion.schedule2_enabled = Me.m_promotion.schedule2_enabled
    temp_promo.m_promotion.schedule2_time_from = Me.m_promotion.schedule2_time_from
    temp_promo.m_promotion.schedule2_time_to = Me.m_promotion.schedule2_time_to
    temp_promo.m_promotion.gender_filter = Me.m_promotion.gender_filter
    temp_promo.m_promotion.birthday_filter = Me.m_promotion.birthday_filter
    temp_promo.m_promotion.age_filter = Me.m_promotion.age_filter
    temp_promo.m_promotion.birthday_age_from = Me.m_promotion.birthday_age_from
    temp_promo.m_promotion.birthday_age_to = Me.m_promotion.birthday_age_to

    ' AVZ 01-OCT-2015: Created account filter added
    temp_promo.m_promotion.created_account_filter = Me.m_promotion.created_account_filter
    temp_promo.m_promotion.created_account_working_days_included = Me.m_promotion.created_account_working_days_included
    temp_promo.m_promotion.created_account_anniversary_year_from = Me.m_promotion.created_account_anniversary_year_from
    temp_promo.m_promotion.created_account_anniversary_year_to = Me.m_promotion.created_account_anniversary_year_to

    temp_promo.m_promotion.level_filter = Me.m_promotion.level_filter
    temp_promo.m_promotion.freq_filter_last_days = Me.m_promotion.freq_filter_last_days
    temp_promo.m_promotion.freq_filter_min_days = Me.m_promotion.freq_filter_min_days
    temp_promo.m_promotion.freq_filter_min_cash_in = Me.m_promotion.freq_filter_min_cash_in
    temp_promo.m_promotion.expiration_type = Me.m_promotion.expiration_type
    temp_promo.m_promotion.expiration_value = Me.m_promotion.expiration_value
    temp_promo.m_promotion.min_cash_in = Me.m_promotion.min_cash_in
    temp_promo.m_promotion.min_cash_in_reward = Me.m_promotion.min_cash_in_reward
    temp_promo.m_promotion.cash_in = Me.m_promotion.cash_in
    temp_promo.m_promotion.cash_in_reward = Me.m_promotion.cash_in_reward
    temp_promo.m_promotion.won_lock_enabled = Me.m_promotion.won_lock_enabled
    temp_promo.m_promotion.won_lock = Me.m_promotion.won_lock
    temp_promo.m_promotion.num_tokens = Me.m_promotion.num_tokens
    temp_promo.m_promotion.token_name = Me.m_promotion.token_name
    temp_promo.m_promotion.token_reward = Me.m_promotion.token_reward
    temp_promo.m_promotion.daily_limit = Me.m_promotion.daily_limit
    temp_promo.m_promotion.monthly_limit = Me.m_promotion.monthly_limit
    temp_promo.m_promotion.global_daily_limit = Me.m_promotion.global_daily_limit
    temp_promo.m_promotion.global_monthly_limit = Me.m_promotion.global_monthly_limit
    temp_promo.m_promotion.min_spent = Me.m_promotion.min_spent
    temp_promo.m_promotion.min_spent_reward = Me.m_promotion.min_spent_reward
    temp_promo.m_promotion.spent = Me.m_promotion.spent
    temp_promo.m_promotion.spent_reward = Me.m_promotion.spent_reward
    temp_promo.m_promotion.min_played = Me.m_promotion.min_played
    temp_promo.m_promotion.min_played_reward = Me.m_promotion.min_played_reward
    temp_promo.m_promotion.played = Me.m_promotion.played
    temp_promo.m_promotion.played_reward = Me.m_promotion.played_reward

    temp_promo.m_promotion.terminal_list = New TerminalList()
    temp_promo.m_promotion.terminal_list.FromXml(Me.m_promotion.terminal_list.ToXml())

    temp_promo.m_promotion.provider_list = New ProviderList()
    temp_promo.m_promotion.provider_list.FromXml(Me.m_promotion.provider_list.ToXml())
    ' AMF 29-AUG-2013
    temp_promo.m_promotion.terminal_list_restricted = New TerminalList()
    temp_promo.m_promotion.terminal_list_restricted.FromXml(Me.m_promotion.terminal_list_restricted.ToXml())

    temp_promo.m_promotion.terminal_list_played = New TerminalList()
    temp_promo.m_promotion.terminal_list_played.FromXml(Me.m_promotion.terminal_list_played.ToXml())

    temp_promo.m_promotion.small_resource_id = Me.m_promotion.small_resource_id
    temp_promo.m_promotion.large_resource_id = Me.m_promotion.large_resource_id

    If IsNothing(Me.m_promotion.icon) Then
      temp_promo.m_promotion.icon = Nothing
    Else
      temp_promo.m_promotion.icon = Me.m_promotion.icon.Clone
    End If

    If IsNothing(Me.m_promotion.image) Then
      temp_promo.m_promotion.image = Nothing
    Else
      temp_promo.m_promotion.image = Me.m_promotion.image.Clone
    End If

    temp_promo.m_promotion.credit_type = Me.m_promotion.credit_type
    temp_promo.m_promotion.accounts_preassigned = Me.m_promotion.accounts_preassigned

    temp_promo.m_promotion.global_limit = Me.m_promotion.global_limit

    temp_promo.m_promotion.ticket_footer = Me.m_promotion.ticket_footer

    temp_promo.m_promotion.category_id = Me.m_promotion.category_id

    temp_promo.m_promotion.flags_required = Me.m_promotion.flags_required.Copy()
    temp_promo.m_promotion.flags_awarded = Me.m_promotion.flags_awarded.Copy()
    temp_promo.m_promotion.promo_screen_mode = Me.m_promotion.promo_screen_mode

    temp_promo.VisibleOnPromoBOX = Me.m_promotion.visible_on_PromoBOX
    temp_promo.AwardOnPromoBOX = Me.m_promotion.award_on_PromoBOX

    ' DHA 09-APR-2013: 'Expiration Limit' field added
    If Me.m_promotion.expiration_limit.IsNull Then
      temp_promo.m_promotion.expiration_limit.IsNull = True
    Else
      temp_promo.m_promotion.expiration_limit.Value = Me.m_promotion.expiration_limit.Value
    End If

    temp_promo.m_promotion.change_table_name = Me.m_promotion.change_table_name
    temp_promo.m_promotion.text_on_PromoBOX = Me.m_promotion.text_on_PromoBOX
    temp_promo.m_promotion.is_vip = Me.m_promotion.is_vip
    temp_promo.m_promotion.apply_tax = Me.m_promotion.apply_tax
    temp_promo.m_promotion.comments = Me.m_promotion.comments

    temp_promo.m_promotion.promogame = Me.m_promotion.promogame
    temp_promo.m_promotion.pyramidal_dist = Me.m_promotion.pyramidal_dist

    temp_promo.m_promotion.journey_limit = Me.m_promotion.journey_limit

    Return temp_promo

  End Function ' Duplicate

  '----------------------------------------------------------------------------
  ' PURPOSE: Returns the related auditor data for the current object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim auditor_data As CLASS_AUDITOR_DATA
    Dim special_permission As String
    Dim string_date As String
    Dim elapsed As TimeSpan
    Dim gender_filter As String
    Dim birthday_filter As String
    Dim expiration As String
    Dim _str_promotion_type As String
    Dim _birthday_filter As Integer
    Dim _age_filter As Integer
    Dim _age_filter_string As String
    Dim _yes_text As String
    Dim _no_text As String
    ' AVZ 01-OCT-2015
    Dim _created_account_filter As Integer
    Dim _created_account_filter_string As String
    Dim _promogame_string As String
    Dim _pyramidal_dist_string As String

    _yes_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(278)
    _no_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)

    auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_PLAYER_PROMOTIONS)
    Call auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(256), Me.Name)

    ' Name
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(253), Me.Name)

    ' Promotion type

    _str_promotion_type = ""

    Select Case Me.Type
      Case Promotion.PROMOTION_TYPE.PERIODIC_PER_LEVEL
        _str_promotion_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(439)
      Case Promotion.PROMOTION_TYPE.PERIODIC_PER_LEVEL_AND_MIN_PLAYED
        _str_promotion_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(441)
      Case Promotion.PROMOTION_TYPE.PERIODIC_PER_BIRTHDAY
        _str_promotion_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(442)
      Case Else

    End Select

    If _str_promotion_type <> "" Then
      'Field only set on periodical promotions
      Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(254), _str_promotion_type)
    End If

    ' Enabled
    If Me.Enabled = True Then
      Call auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(261), GLB_NLS_GUI_CONFIGURATION.GetString(311))
    Else
      Call auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(261), GLB_NLS_GUI_CONFIGURATION.GetString(328))
    End If

    ' Special Permission
    special_permission = AUDIT_NONE_STRING
    Select Case Me.SpecialPermission
      Case ENUM_SPECIAL_PERMISSION.NOT_SET
        special_permission = AUDIT_NONE_STRING

      Case ENUM_SPECIAL_PERMISSION.PERMISSION_A
        special_permission = GLB_NLS_GUI_PLAYER_TRACKING.GetString(402)

      Case ENUM_SPECIAL_PERMISSION.PERMISSION_B
        special_permission = GLB_NLS_GUI_PLAYER_TRACKING.GetString(403)

    End Select
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(401), special_permission)

    ' Date Start
    string_date = GUI_FormatDate(Me.DateStart, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(289), string_date)

    ' Date Finish
    string_date = GUI_FormatDate(Me.DateFinish, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(290), string_date)

    ' Schedule data
    ' Schedule weekdays
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(299), WorkingDaysText(Me.ScheduleWeekday, Me.Type))

    ' Schedule timetable 
    elapsed = TimeSpan.FromSeconds(Me.Schedule1TimeFrom)
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(292), GLB_NLS_GUI_PLAYER_TRACKING.GetString(297) & " " + elapsed.ToString())
    elapsed = TimeSpan.FromSeconds(Me.Schedule1TimeTo)
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(292), GLB_NLS_GUI_PLAYER_TRACKING.GetString(298) & " " + elapsed.ToString())

    If Me.Schedule2Enabled Then
      elapsed = TimeSpan.FromSeconds(Me.Schedule2TimeFrom)
      Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(293), GLB_NLS_GUI_PLAYER_TRACKING.GetString(297) & " " + elapsed.ToString())
      elapsed = TimeSpan.FromSeconds(Me.Schedule2TimeTo)
      Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(293), GLB_NLS_GUI_PLAYER_TRACKING.GetString(298) & " " + elapsed.ToString())
    Else
      Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(293), GLB_NLS_GUI_PLAYER_TRACKING.GetString(297) & AUDIT_NONE_STRING)
      Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(293), GLB_NLS_GUI_PLAYER_TRACKING.GetString(298) & AUDIT_NONE_STRING)
    End If

    ' Gender Filter
    gender_filter = AUDIT_NONE_STRING
    Select Case Me.GenderFilter
      Case ENUM_GENDER_FILTER.NOT_SET
        gender_filter = AUDIT_NONE_STRING

      Case ENUM_GENDER_FILTER.MEN_ONLY
        gender_filter = GLB_NLS_GUI_PLAYER_TRACKING.GetString(214)

      Case ENUM_GENDER_FILTER.WOMEN_ONLY
        gender_filter = GLB_NLS_GUI_PLAYER_TRACKING.GetString(215)

    End Select
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(213), gender_filter)

    ' Date of birth Filter
    _birthday_filter = 0
    _age_filter = Me.AgeFilter
    If Me.BirthdayFilter.ToString().Length > 1 Then
      Call WSI.Common.Promotion.DecodeBirthdayFilter(Me.BirthdayFilter, _age_filter, _birthday_filter, Me.BirthdayAgeFrom, Me.BirthdayAgeTo)
    Else
      _birthday_filter = Me.BirthdayFilter
    End If
    birthday_filter = AUDIT_NONE_STRING
    Select Case _birthday_filter
      Case ENUM_BIRTHDAY_FILTER.NOT_SET
        birthday_filter = AUDIT_NONE_STRING

      Case ENUM_BIRTHDAY_FILTER.DAY_ONLY
        birthday_filter = GLB_NLS_GUI_PLAYER_TRACKING.GetString(239)

      Case ENUM_BIRTHDAY_FILTER.WHOLE_MONTH
        birthday_filter = GLB_NLS_GUI_PLAYER_TRACKING.GetString(240)
    End Select

    _age_filter_string = AUDIT_NONE_STRING
    Select Case _age_filter
      Case ENUM_BIRTHDAY_FILTER.INCLUDE_AGE
        _age_filter_string = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2702)

      Case ENUM_BIRTHDAY_FILTER.EXCLUDE_AGE
        _age_filter_string = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2703)
    End Select

    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(216), birthday_filter)
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2730), _age_filter_string)
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2730), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2706) & ": " & Me.BirthdayAgeFrom)
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2730), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2707) & ": " & Me.BirthdayAgeTo)

    ' AVZ 01-OCT-2015: Created account date Filter
    _created_account_filter = 0

    If Me.CreatedAccountFilter.ToString().Length > 1 Then
      Call WSI.Common.Promotion.DecodeCreatedAccountFilter(Me.CreatedAccountFilter, _created_account_filter, Me.CreatedAccountWorkingDaysIncluded, Me.CreatedAccountAnniversaryYearFrom, Me.CreatedAccountAnniversaryYearTo)
    Else
      _created_account_filter = Me.CreatedAccountFilter
    End If
    _created_account_filter_string = AUDIT_NONE_STRING
    Select Case _created_account_filter
      Case ENUM_CREATED_ACCOUNT_DATE_FILTER.NOT_SET
        _created_account_filter_string = AUDIT_NONE_STRING
      Case ENUM_CREATED_ACCOUNT_DATE_FILTER.WORKING_DAY_ONLY
        _created_account_filter_string = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6766)
      Case ENUM_CREATED_ACCOUNT_DATE_FILTER.WORKING_DAYS_INCLUDED
        _created_account_filter_string = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6767)
      Case ENUM_CREATED_ACCOUNT_DATE_FILTER.ANNIVERSARY_WHOLE_MONTH
        _created_account_filter_string = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6768)
      Case ENUM_CREATED_ACCOUNT_DATE_FILTER.ANNIVERSARY_WORKING_DAY_ONLY
        _created_account_filter_string = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6769)
    End Select

    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6770), _created_account_filter_string)
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6770), GLB_NLS_GUI_PLAYER_TRACKING.GetString(6771) & ": " & Me.CreatedAccountWorkingDaysIncluded)
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6770), GLB_NLS_GUI_PLAYER_TRACKING.GetString(6772) & ": " & Me.CreatedAccountAnniversaryYearFrom)
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6770), GLB_NLS_GUI_PLAYER_TRACKING.GetString(6773) & ": " & Me.CreatedAccountAnniversaryYearTo)

    ' Level Filter
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(404), LevelFilterText(Me.LevelFilter))

    ' Frequency Filter
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(414), GUI_FormatNumber(Me.FreqFilterLastDays, 0))
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(415), GUI_FormatNumber(Me.FreqFilterMinDays, 0))
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(416), GUI_FormatCurrency(Me.FreqFilterMinCashIn))

    ' Expiration 
    expiration = AUDIT_NONE_STRING
    Select Case Me.ExpirationType
      Case ENUM_EXPIRATION_TYPE.DAYS
        expiration = GLB_NLS_GUI_PLAYER_TRACKING.GetString(258)

      Case ENUM_EXPIRATION_TYPE.HOURS
        expiration = GLB_NLS_GUI_PLAYER_TRACKING.GetString(259)
    End Select
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(223), GUI_FormatNumber(Me.ExpirationValue, 0) & " " & expiration)

    ' Amounts
    ' Minimum Cash in
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(283), GUI_FormatCurrency(Me.MinCashIn))
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(241), GUI_FormatCurrency(Me.MinCashInReward))

    ' Cash in
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(233), GUI_FormatCurrency(Me.CashIn))
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(242), GUI_FormatCurrency(Me.CashInReward))

    ' Minimum Spent
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(424), GUI_FormatCurrency(Me.MinSpent))
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(425), GUI_FormatCurrency(Me.MinSpentReward))

    ' Spent
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(426), GUI_FormatCurrency(Me.Spent))
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(427), GUI_FormatCurrency(Me.SpentReward))

    ' Minimum Played
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(814), GUI_FormatCurrency(Me.MinPlayed))
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(815), GUI_FormatCurrency(Me.MinPlayedReward))

    ' Played
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(816), GUI_FormatCurrency(Me.Played))
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(817), GUI_FormatCurrency(Me.PlayedReward))

    ' Tokens
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(228), GUI_FormatNumber(Me.NumTokens, 0))
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(243), GUI_FormatCurrency(Me.TokenReward))
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(244), Me.TokenName)

    ' Won Lock
    If Me.WonLockEnabled And Me.WonLock >= 0 Then
      Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(245), GUI_FormatCurrency(Me.WonLock))
    Else
      Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(245), GLB_NLS_GUI_CONTROLS.GetString(282))
    End If

    ' Limits
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(230), GUI_FormatCurrency(Me.DailyLimit))
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(235), GUI_FormatCurrency(Me.MonthlyLimit))
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(529), GUI_FormatCurrency(Me.GlobalDailyLimit))
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(530), GUI_FormatCurrency(Me.GlobalMonthlyLimit))
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1247), GUI_FormatCurrency(Me.GlobalLimit))

    'Groups
    ' Type
    Call auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(484), GLB_NLS_GUI_CONTROLS.GetString(486) + ": " + _
                                TerminalList.AuditStringType(GLB_NLS_GUI_CONFIGURATION.GetString(497), _
                                                             GLB_NLS_GUI_CONFIGURATION.GetString(499), _
                                                             GLB_NLS_GUI_CONFIGURATION.GetString(498)))


    ' Group list
    Call auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(484), GLB_NLS_GUI_CONTROLS.GetString(482) + ": " + _
                               TerminalList.AuditStringList(GROUP_ELEMENT_TYPE.GROUP, _
                                                            GLB_NLS_GUI_PLAYER_TRACKING.GetString(501)))

    ' Prviders list
    Call auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(484), GLB_NLS_GUI_CONTROLS.GetString(478) + ": " + _
                               TerminalList.AuditStringList(GROUP_ELEMENT_TYPE.PROV, _
                                                            GLB_NLS_GUI_PLAYER_TRACKING.GetString(501)))

    ' Zones lista
    Call auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(484), GLB_NLS_GUI_CONTROLS.GetString(479) + ": " + _
                               TerminalList.AuditStringList(GROUP_ELEMENT_TYPE.ZONE, _
                                                            GLB_NLS_GUI_PLAYER_TRACKING.GetString(501)))

    ' Areas list
    Call auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(484), GLB_NLS_GUI_CONTROLS.GetString(480) + ": " + _
                               TerminalList.AuditStringList(GROUP_ELEMENT_TYPE.AREA, _
                                                            GLB_NLS_GUI_PLAYER_TRACKING.GetString(501)))

    ' Banks list
    Call auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(484), GLB_NLS_GUI_CONTROLS.GetString(481) + ": " + _
                               TerminalList.AuditStringList(GROUP_ELEMENT_TYPE.BANK, _
                                                            GLB_NLS_GUI_PLAYER_TRACKING.GetString(501)))

    ' Terminals list
    Call auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(484), GLB_NLS_GUI_CONTROLS.GetString(483) + ": " + _
                               TerminalList.AuditStringList(GROUP_ELEMENT_TYPE.TERM, _
                                                            GLB_NLS_GUI_PLAYER_TRACKING.GetString(501)))

    ' Providers Selection (Recurring Promotions)

    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(475), GLB_NLS_GUI_CONTROLS.GetString(478) + ": " + _
                               ProviderList.AuditString(GLB_NLS_GUI_CONFIGURATION.GetString(497), _
                              GLB_NLS_GUI_CONFIGURATION.GetString(499), GLB_NLS_GUI_CONFIGURATION.GetString(498), _
                              GLB_NLS_GUI_PLAYER_TRACKING.GetString(501)))


    'SDS 16-10-2015
    'Group Played
    Call auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(484), GLB_NLS_GUI_CONTROLS.GetString(486) + ": " + _
                                TerminalListPlayed.AuditStringType(GLB_NLS_GUI_CONFIGURATION.GetString(497), _
                                                             GLB_NLS_GUI_CONFIGURATION.GetString(499), _
                                                             GLB_NLS_GUI_CONFIGURATION.GetString(498)))

    ' Group list Played
    Call auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(484), GLB_NLS_GUI_CONTROLS.GetString(482) + ": " + _
                               TerminalListPlayed.AuditStringList(GROUP_ELEMENT_TYPE.GROUP, _
                                                            GLB_NLS_GUI_PLAYER_TRACKING.GetString(501)))

    ' Prviders list Played
    Call auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(484), GLB_NLS_GUI_CONTROLS.GetString(478) + ": " + _
                               TerminalListPlayed.AuditStringList(GROUP_ELEMENT_TYPE.PROV, _
                                                            GLB_NLS_GUI_PLAYER_TRACKING.GetString(501)))

    ' Zones list Played
    Call auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(484), GLB_NLS_GUI_CONTROLS.GetString(479) + ": " + _
                               TerminalListPlayed.AuditStringList(GROUP_ELEMENT_TYPE.ZONE, _
                                                            GLB_NLS_GUI_PLAYER_TRACKING.GetString(501)))

    ' Areas list Played 
    Call auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(484), GLB_NLS_GUI_CONTROLS.GetString(480) + ": " + _
                               TerminalListPlayed.AuditStringList(GROUP_ELEMENT_TYPE.AREA, _
                                                            GLB_NLS_GUI_PLAYER_TRACKING.GetString(501)))

    ' Banks list Played
    Call auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(484), GLB_NLS_GUI_CONTROLS.GetString(481) + ": " + _
                               TerminalListPlayed.AuditStringList(GROUP_ELEMENT_TYPE.BANK, _
                                                            GLB_NLS_GUI_PLAYER_TRACKING.GetString(501)))

    ' Terminals list Played
    Call auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(484), GLB_NLS_GUI_CONTROLS.GetString(483) + ": " + _
                               TerminalListPlayed.AuditStringList(GROUP_ELEMENT_TYPE.TERM, _
                                                          GLB_NLS_GUI_PLAYER_TRACKING.GetString(501)))
    ' AMF 29-AUG-2013
    'Groups Restricted

    ' Type
    Call auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(485), GLB_NLS_GUI_CONTROLS.GetString(486) + ": " + _
                                TerminalListRestricted.AuditStringType(GLB_NLS_GUI_CONFIGURATION.GetString(497), _
                                                             GLB_NLS_GUI_CONFIGURATION.GetString(499), _
                                                             GLB_NLS_GUI_CONFIGURATION.GetString(498)))

    ' Group list
    Call auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(485), GLB_NLS_GUI_CONTROLS.GetString(482) + ": " + _
                               TerminalListRestricted.AuditStringList(GROUP_ELEMENT_TYPE.GROUP, _
                                                            GLB_NLS_GUI_PLAYER_TRACKING.GetString(501)))

    ' Prviders list
    Call auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(485), GLB_NLS_GUI_CONTROLS.GetString(478) + ": " + _
                               TerminalListRestricted.AuditStringList(GROUP_ELEMENT_TYPE.PROV, _
                                                            GLB_NLS_GUI_PLAYER_TRACKING.GetString(501)))

    ' Zones list
    Call auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(485), GLB_NLS_GUI_CONTROLS.GetString(479) + ": " + _
                               TerminalListRestricted.AuditStringList(GROUP_ELEMENT_TYPE.ZONE, _
                                                            GLB_NLS_GUI_PLAYER_TRACKING.GetString(501)))

    ' Areas list
    Call auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(485), GLB_NLS_GUI_CONTROLS.GetString(480) + ": " + _
                               TerminalListRestricted.AuditStringList(GROUP_ELEMENT_TYPE.AREA, _
                                                            GLB_NLS_GUI_PLAYER_TRACKING.GetString(501)))

    ' Banks list
    Call auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(485), GLB_NLS_GUI_CONTROLS.GetString(481) + ": " + _
                               TerminalListRestricted.AuditStringList(GROUP_ELEMENT_TYPE.BANK, _
                                                            GLB_NLS_GUI_PLAYER_TRACKING.GetString(501)))

    ' Terminals list
    Call auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(485), GLB_NLS_GUI_CONTROLS.GetString(483) + ": " + _
                               TerminalListRestricted.AuditStringList(GROUP_ELEMENT_TYPE.TERM, _
                                                            GLB_NLS_GUI_PLAYER_TRACKING.GetString(501)))


    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(719), GetInfoCRC(Me.Icon))
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(687), GetInfoCRC(Me.Image))

    ' 1163 Credit type 
    Select Case Me.CreditType
      Case ACCOUNT_PROMO_CREDIT_TYPE.NR2, ACCOUNT_PROMO_CREDIT_TYPE.NR1
        ' 224 Non-redeemable
        Call auditor_data.SetField(GLB_NLS_GUI_INVOICING.Id(336), GLB_NLS_GUI_INVOICING.GetString(338))
      Case ACCOUNT_PROMO_CREDIT_TYPE.POINT
        ' 354 Points
        Call auditor_data.SetField(GLB_NLS_GUI_INVOICING.Id(336), GLB_NLS_GUI_PLAYER_TRACKING.GetString(354))
      Case ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE
        ' 250 Redeemable
        Call auditor_data.SetField(GLB_NLS_GUI_INVOICING.Id(336), GLB_NLS_GUI_INVOICING.GetString(337))
      Case Else
        ' 786 Unknown
        Call auditor_data.SetField(GLB_NLS_GUI_INVOICING.Id(336), GLB_NLS_GUI_PLAYER_TRACKING.GetString(786))
    End Select

    If Me.Type = Promotion.PROMOTION_TYPE.PREASSIGNED Then

      If Not Me.AccountsPreassigned Is Nothing AndAlso Me.AccountsPreassigned.Rows.Count > 0 Then

        Dim _summary As StringBuilder
        Dim _acc_prev As Int64
        Dim _total_accounts As Int32
        Dim _total_rows As Int32
        Dim _str As String

        _total_accounts = 0
        _total_rows = 0
        _acc_prev = -1
        _summary = New StringBuilder

        For Each _dr_next As DataRow In Me.AccountsPreassigned.Select("", "account_id")

          If Not _dr_next(0).Equals(_acc_prev) Then
            _total_accounts += 1
          End If
          _acc_prev = _dr_next(0)
          _total_rows += 1

        Next
        _str = GLB_NLS_GUI_AUDITOR.GetString(423) ' Total
        _summary.Append(_str & ": " & _total_rows)
        _summary.Append(", ")
        _str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1253) ' Num. cuentas: 
        _summary.Append(_str & _total_accounts)
        _summary.Append(", ")
        _str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1254) ' Total otorgado: 

        If CreditType = ACCOUNT_PROMO_CREDIT_TYPE.POINT Then
          _summary.Append(_str & GUI_FormatNumber(Me.AccountsPreassigned.Compute("SUM(reward)", ""), 0) & " PT ")
        Else
          _summary.Append(_str & GUI_FormatCurrency(Me.AccountsPreassigned.Compute("SUM(reward)", "")))
        End If

        Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1252), _summary.ToString())

      End If

    End If

    'ticket_footer
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1284), Me.m_promotion.ticket_footer)

    ' Category
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1336), PromotionCategories.Value(Me.CategoryId))

    ' Audit changes in promotion flag assignment

    Dim _row As DataRow
    Dim _all_flags As DataTable
    Dim _str_title As String
    Dim _str_header As String
    _all_flags = Nothing

    If (Not GetAllFlags(_all_flags, True)) Then
      Log.Error("WARNING :  Can't auditor flag changes!")
      Return auditor_data
    End If

    ' Is String type to allow set AUDIT_NONE_STRING value ;)
    _all_flags.Columns.Add("COUNT", System.Type.GetType("System.String"))
    _all_flags.PrimaryKey = New DataColumn() {_all_flags.Columns("FL_FLAG_ID")}


    '' BAD!
    If (Me.FlagsAwarded.PrimaryKey.Length = 0) Then
      FlagsAwarded.PrimaryKey = New DataColumn() {FlagsAwarded.Columns(0)}
    End If
    If (Me.FlagsRequired.PrimaryKey.Length = 0) Then
      FlagsRequired.PrimaryKey = New DataColumn() {FlagsRequired.Columns(0)}
    End If

    For Each _flag As DataRow In Me.FlagsAwarded.Rows
      _row = _all_flags.Rows.Find(_flag("PF_FLAG_ID"))
      If (_row Is Nothing) Then
        _row = _all_flags.NewRow()
        _row("FL_FLAG_ID") = _flag("PF_FLAG_ID")
        _row("COUNT") = _flag("PF_FLAG_COUNT")
        _row("FL_NAME") = "##-" + _flag("PF_FLAG_ID").ToString()
        _all_flags.Rows.Add(_row)
      End If
    Next

    For Each _flag As DataRow In _all_flags.Rows
      _row = Me.FlagsAwarded.Rows.Find(_flag("FL_FLAG_ID"))
      If (Not _row Is Nothing) Then
        _flag("COUNT") = _row("PF_FLAG_COUNT")
      Else
        _flag("COUNT") = AUDIT_NONE_STRING
      End If

      ' Otorga / Otorgadas
      _str_header = IIf(IsNumeric(_flag("COUNT")) AndAlso _flag("COUNT") = 1 _
                             , GLB_NLS_GUI_PLAYER_TRACKING.GetString(1413) _
                             , GLB_NLS_GUI_PLAYER_TRACKING.GetString(1414))
      _str_title = _flag("FL_NAME") & "." & vbTab & _str_header

      ' Screen Mode New
      If (Me.PromoScreenMode = ENUM_PROMO_SCREEN_MODE.MODE_NEW Or _
          Me.PromoScreenMode = ENUM_PROMO_SCREEN_MODE.MODE_DELETE) AndAlso Not IsNumeric(_flag("COUNT")) Then
        ' Discard flags without a value
      Else
        Call auditor_data.SetField(0, _flag("COUNT"), _str_title)
      End If
    Next

    For Each _flag As DataRow In Me.FlagsRequired.Rows
      _row = _all_flags.Rows.Find(_flag("PF_FLAG_ID"))
      If (_row Is Nothing) Then
        _row = _all_flags.NewRow()
        _row("FL_FLAG_ID") = _flag("PF_FLAG_ID")
        _row("COUNT") = _flag("PF_FLAG_COUNT")
        _row("FL_NAME") = "##-" + _flag("PF_FLAG_ID").ToString()
        _all_flags.Rows.Add(_row)
      End If
    Next

    For Each _flag As DataRow In _all_flags.Rows
      _row = Me.FlagsRequired.Rows.Find(_flag("FL_FLAG_ID"))
      If (Not _row Is Nothing) Then
        _flag("COUNT") = _row("PF_FLAG_COUNT")
      Else
        _flag("COUNT") = AUDIT_NONE_STRING
      End If

      ' Requiere / Requeridas
      _str_header = IIf(IsNumeric(_flag("COUNT")) AndAlso _flag("COUNT") = 1 _
                             , GLB_NLS_GUI_PLAYER_TRACKING.GetString(1415) _
                             , GLB_NLS_GUI_PLAYER_TRACKING.GetString(1416))
      _str_title = _flag("FL_NAME") & "." & vbTab & _str_header

      If (Me.PromoScreenMode = 1 Or Me.PromoScreenMode = 2) AndAlso Not IsNumeric(_flag("COUNT")) Then
        ' Discard flags without a value
      Else
        Call auditor_data.SetField(0, _flag("COUNT"), _str_title)
      End If
    Next

    ' JMM 14-NOV-2012: 'Visible on PromoBOX' field added
    If Me.VisibleOnPromoBOX = True Then
      Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1434), _yes_text)
    Else
      Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1434), _no_text)
    End If

    ' DHA 09-APR-2013: 'Expiration Limit' field added
    If Me.ExpirationLimit.Date = DateTime.MinValue Then
      string_date = AUDIT_NONE_STRING
    Else
      string_date = GUI_FormatDate(Me.ExpirationLimit, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1903), string_date)

    ' GMV 04-NOV-2015: 'Applicable on PromoBOX'
    If Me.AwardOnPromoBOX >= 0 Then
      Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2869), Me.AwardOnPromoBOX.ToString())
    Else
      Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2869), _no_text)
    End If

    ' JMM 04-NOV-2013: 'TextOnPromoBOX' field added
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(688), Me.TextOnPromoBOX)

    ' JBP 07-APR-2014: Gift is Vip
    auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(4804), IIf(Me.IsVip, _yes_text, _no_text))

    ' JBP 07-APR-2014: Gift is Vip
    auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7143), IIf(Me.ApplyTax, _yes_text, _no_text))

    ' TODO XCD 497 Motivo del cambio
    auditor_data.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(497), Me.Comments)

    ' Promogame
    If Me.Promogame = -1 Then
      _promogame_string = GLB_NLS_GUI_PLAYER_TRACKING.GetString(248)
    Else
      _promogame_string = GetPromogameName(Me.Promogame)
    End If
    auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8541), _promogame_string)

    ' Pyramidal Distribution
    If IsNothing(Me.PyramidalDist) OrElse Me.PyramidalDist.Equals(String.Empty) Then
      _pyramidal_dist_string = GLB_NLS_GUI_PLAYER_TRACKING.GetString(248)
    Else
      _pyramidal_dist_string = Me.PyramidalDist ' TODO - UNRANKED CCG: Ver qu� valor guardar
    End If
    auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8755), _pyramidal_dist_string)

    ' RLO 25-OCT-2017: JOURNEY LIMIT
    auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8745), IIf(Me.JourneyLimit, _yes_text, _no_text))

    Return auditor_data

  End Function ' AuditorData

#End Region ' Overrides Functions

#Region " Private Functions "

  Private Function GetPromogameName(GameId As Long) As String
    Dim _promogame As PromoGame

    _promogame = New PromoGame(GameId)

    Return _promogame.Name
  End Function


  Private Function GetAllFlags(ByRef _data As DataTable, Optional ByVal bShowAutomaticFlags As Boolean = False) As Boolean

    Dim _sql_tx As SqlClient.SqlTransaction
    _sql_tx = Nothing

    If Not GUI_BeginSQLTransaction(_sql_tx) Then
      Return False
    Else

      CLASS_FLAG.GetAllFlags(_sql_tx, _data, bShowAutomaticFlags)

      Call GUI_EndSQLTransaction(_sql_tx, True)

      Return True

    End If


  End Function

  Public Function ReadPromotion(ByVal pPromo As TYPE_PROMOTION, _
                                 ByVal Context As Integer) As Integer
    Dim _str_sql As StringBuilder
    Dim data_table As DataTable
    Dim _xml As String
    Dim _sql_tx As SqlClient.SqlTransaction

    _str_sql = New StringBuilder()
    _sql_tx = Nothing

    ' AVZ 01-OCT-2015: Added column PM_CREATED_ACCOUNT_FILTER

    _str_sql.AppendLine(" SELECT PM_NAME, ")
    _str_sql.AppendLine("        PM_ENABLED, ")
    _str_sql.AppendLine("        PM_TYPE, ")
    _str_sql.AppendLine("        PM_DATE_START, ")
    _str_sql.AppendLine("        PM_DATE_FINISH, ")
    _str_sql.AppendLine("        PM_SCHEDULE_WEEKDAY, ")
    _str_sql.AppendLine("        PM_SCHEDULE1_TIME_FROM, ")
    _str_sql.AppendLine("        PM_SCHEDULE1_TIME_TO, ")
    _str_sql.AppendLine("        PM_SCHEDULE2_ENABLED, ")
    _str_sql.AppendLine("        ISNULL (PM_SCHEDULE2_TIME_FROM, 0) AS PM_SCHEDULE2_TIME_FROM, ")
    _str_sql.AppendLine("        ISNULL (PM_SCHEDULE2_TIME_TO, 86399) AS PM_SCHEDULE2_TIME_TO, ")
    _str_sql.AppendLine("        PM_GENDER_FILTER, ")
    _str_sql.AppendLine("        PM_BIRTHDAY_FILTER, ")
    _str_sql.AppendLine("        ISNULL(PM_CREATED_ACCOUNT_FILTER,0) AS PM_CREATED_ACCOUNT_FILTER, ")
    _str_sql.AppendLine("        PM_EXPIRATION_TYPE, ")
    _str_sql.AppendLine("        PM_EXPIRATION_VALUE, ")
    _str_sql.AppendLine("        PM_MIN_CASH_IN, ")
    _str_sql.AppendLine("        PM_MIN_CASH_IN_REWARD, ")
    _str_sql.AppendLine("        PM_CASH_IN, ")
    _str_sql.AppendLine("        PM_CASH_IN_REWARD, ")
    _str_sql.AppendLine("        PM_WON_LOCK, ")
    _str_sql.AppendLine("        PM_NUM_TOKENS, ")
    _str_sql.AppendLine("        ISNULL (PM_TOKEN_NAME, '') AS PM_TOKEN_NAME, ")
    _str_sql.AppendLine("        PM_TOKEN_REWARD, ")
    _str_sql.AppendLine("        ISNULL (PM_DAILY_LIMIT, 0) AS PM_DAILY_LIMIT, ")
    _str_sql.AppendLine("        ISNULL (PM_MONTHLY_LIMIT, 0) AS PM_MONTHLY_LIMIT, ")
    _str_sql.AppendLine("        ISNULL (PM_GLOBAL_DAILY_LIMIT, 0) AS PM_GLOBAL_DAILY_LIMIT, ")
    _str_sql.AppendLine("        ISNULL (PM_GLOBAL_MONTHLY_LIMIT, 0) AS PM_GLOBAL_MONTHLY_LIMIT, ")
    _str_sql.AppendLine("        ISNULL (PM_GLOBAL_LIMIT, 0) AS PM_GLOBAL_LIMIT, ")
    _str_sql.AppendLine("        PM_LEVEL_FILTER, ")
    _str_sql.AppendLine("        PM_PERMISSION, ")
    _str_sql.AppendLine("        ISNULL (PM_FREQ_FILTER_LAST_DAYS, 0) AS PM_FREQ_FILTER_LAST_DAYS, ")
    _str_sql.AppendLine("        ISNULL (PM_FREQ_FILTER_MIN_DAYS, 0) AS PM_FREQ_FILTER_MIN_DAYS, ")
    _str_sql.AppendLine("        ISNULL (PM_FREQ_FILTER_MIN_CASH_IN, 0) AS PM_FREQ_FILTER_MIN_CASH_IN, ")
    _str_sql.AppendLine("        PM_MIN_SPENT, ")
    _str_sql.AppendLine("        PM_MIN_SPENT_REWARD, ")
    _str_sql.AppendLine("        PM_SPENT, ")
    _str_sql.AppendLine("        PM_SPENT_REWARD, ")
    _str_sql.AppendLine("        PM_PROVIDER_LIST, ")
    _str_sql.AppendLine("        PM_RESTRICTED_TO_TERMINAL_LIST, ")
    _str_sql.AppendLine("        PM_SMALL_RESOURCE_ID , ")
    _str_sql.AppendLine("        PM_LARGE_RESOURCE_ID , ")
    _str_sql.AppendLine("        PM_MIN_PLAYED, ")
    _str_sql.AppendLine("        PM_MIN_PLAYED_REWARD, ")
    _str_sql.AppendLine("        PM_PLAYED, ")
    _str_sql.AppendLine("        PM_PLAYED_REWARD, ")
    _str_sql.AppendLine("        PM_CREDIT_TYPE, ")
    _str_sql.AppendLine("        PM_TICKET_FOOTER, ")
    _str_sql.AppendLine("        PM_CATEGORY_ID, ")
    _str_sql.AppendLine("        PM_VISIBLE_ON_PROMOBOX,")
    _str_sql.AppendLine("        PM_EXPIRATION_LIMIT,")
    _str_sql.AppendLine("        PM_TEXT_ON_PROMOBOX,")
    _str_sql.AppendLine("        PM_AWARD_ON_PROMOBOX, ")
    _str_sql.AppendLine("        PTC_NAME, ")
    _str_sql.AppendLine("        PM_JOURNEY_LIMIT, ")
    _str_sql.AppendLine("        ISNULL(PM_VIP, 0) AS PM_VIP, ")
    _str_sql.AppendLine("        ISNULL(PM_APPLY_TAX, 0) AS PM_APPLY_TAX, ")
    _str_sql.AppendLine("        ISNULL(PM_PROMOGAME_ID, -1) AS PM_PROMOGAME_ID, ")
    _str_sql.AppendLine("        PM_PYRAMIDAL_DIST ")
    _str_sql.AppendLine(" FROM   PROMOTIONS ")
    _str_sql.AppendLine(" LEFT   JOIN POINTS_TO_CREDITS ON PTC_POINTS_TO_CREDITS_ID = PM_POINTS_TO_CREDITS_ID ")
    _str_sql.AppendLine(" WHERE  PM_PROMOTION_ID = " & pPromo.promotion_id)
    '" PM_TICKET_FOOTER")

    data_table = GUI_GetTableUsingSQL(_str_sql.ToString(), 5000)
    If IsNothing(data_table) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    If data_table.Rows.Count() <> 1 Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    pPromo.name = NullTrim(data_table.Rows(0).Item("PM_NAME"))
    pPromo.enabled = data_table.Rows(0).Item("PM_ENABLED")
    pPromo.type = data_table.Rows(0).Item("PM_TYPE")
    pPromo.date_start.Value = data_table.Rows(0).Item("PM_DATE_START")
    pPromo.date_finish.Value = data_table.Rows(0).Item("PM_DATE_FINISH")
    pPromo.schedule_weekday = data_table.Rows(0).Item("PM_SCHEDULE_WEEKDAY")
    pPromo.schedule1_time_from = data_table.Rows(0).Item("PM_SCHEDULE1_TIME_FROM")
    pPromo.schedule1_time_to = data_table.Rows(0).Item("PM_SCHEDULE1_TIME_TO")
    pPromo.schedule2_enabled = data_table.Rows(0).Item("PM_SCHEDULE2_ENABLED")
    pPromo.schedule2_time_from = data_table.Rows(0).Item("PM_SCHEDULE2_TIME_FROM")
    pPromo.schedule2_time_to = data_table.Rows(0).Item("PM_SCHEDULE2_TIME_TO")
    pPromo.gender_filter = data_table.Rows(0).Item("PM_GENDER_FILTER")
    WSI.Common.Promotion.DecodeBirthdayFilter(data_table.Rows(0).Item("PM_BIRTHDAY_FILTER"), pPromo.age_filter, pPromo.birthday_filter, pPromo.birthday_age_from, pPromo.birthday_age_to)
    ' AVZ 01-OCT-2015: Added column PM_CREATED_ACCOUNT_FILTER
    WSI.Common.Promotion.DecodeCreatedAccountFilter(data_table.Rows(0).Item("PM_CREATED_ACCOUNT_FILTER"), pPromo.created_account_filter, pPromo.created_account_working_days_included, pPromo.created_account_anniversary_year_from, pPromo.created_account_anniversary_year_to)
    pPromo.expiration_type = data_table.Rows(0).Item("PM_EXPIRATION_TYPE")
    pPromo.expiration_value = data_table.Rows(0).Item("PM_EXPIRATION_VALUE")
    pPromo.min_cash_in = data_table.Rows(0).Item("PM_MIN_CASH_IN")
    pPromo.min_cash_in_reward = data_table.Rows(0).Item("PM_MIN_CASH_IN_REWARD")
    pPromo.cash_in = data_table.Rows(0).Item("PM_CASH_IN")
    pPromo.cash_in_reward = data_table.Rows(0).Item("PM_CASH_IN_REWARD")
    If data_table.Rows(0).IsNull("PM_WON_LOCK") Then
      pPromo.won_lock_enabled = False
      pPromo.won_lock = 0
    Else
      pPromo.won_lock_enabled = True
      pPromo.won_lock = data_table.Rows(0).Item("PM_WON_LOCK")
    End If
    pPromo.num_tokens = data_table.Rows(0).Item("PM_NUM_TOKENS")
    pPromo.token_name = NullTrim(data_table.Rows(0).Item("PM_TOKEN_NAME"))
    pPromo.token_reward = data_table.Rows(0).Item("PM_TOKEN_REWARD")
    pPromo.daily_limit = data_table.Rows(0).Item("PM_DAILY_LIMIT")
    pPromo.monthly_limit = data_table.Rows(0).Item("PM_MONTHLY_LIMIT")
    pPromo.global_daily_limit = data_table.Rows(0).Item("PM_GLOBAL_DAILY_LIMIT")
    pPromo.global_monthly_limit = data_table.Rows(0).Item("PM_GLOBAL_MONTHLY_LIMIT")
    pPromo.global_limit = data_table.Rows(0).Item("PM_GLOBAL_LIMIT")
    pPromo.level_filter = data_table.Rows(0).Item("PM_LEVEL_FILTER")
    pPromo.special_permission = data_table.Rows(0).Item("PM_PERMISSION")
    pPromo.freq_filter_last_days = data_table.Rows(0).Item("PM_FREQ_FILTER_LAST_DAYS")
    pPromo.freq_filter_min_days = data_table.Rows(0).Item("PM_FREQ_FILTER_MIN_DAYS")
    pPromo.freq_filter_min_cash_in = data_table.Rows(0).Item("PM_FREQ_FILTER_MIN_CASH_IN")
    pPromo.min_spent = data_table.Rows(0).Item("PM_MIN_SPENT")
    pPromo.min_spent_reward = data_table.Rows(0).Item("PM_MIN_SPENT_REWARD")
    pPromo.spent = data_table.Rows(0).Item("PM_SPENT")
    pPromo.spent_reward = data_table.Rows(0).Item("PM_SPENT_REWARD")
    pPromo.min_played = data_table.Rows(0).Item("PM_MIN_PLAYED")
    pPromo.min_played_reward = data_table.Rows(0).Item("PM_MIN_PLAYED_REWARD")
    pPromo.played = data_table.Rows(0).Item("PM_PLAYED")
    pPromo.played_reward = data_table.Rows(0).Item("PM_PLAYED_REWARD")
    pPromo.category_id = data_table.Rows(0).Item("PM_CATEGORY_ID")

    ' PM_PROVIDER_LIST
    _xml = Nothing
    If data_table.Rows(0).Item("PM_PROVIDER_LIST") IsNot DBNull.Value Then
      _xml = data_table.Rows(0).Item("PM_PROVIDER_LIST")
    End If

    If pPromo.type = Promotion.PROMOTION_TYPE.PERIODIC_PER_LEVEL_AND_MIN_PLAYED Then
      pPromo.provider_list.FromXml(_xml)
    Else
      pPromo.terminal_list.FromXml(_xml)
    End If

    ' AMF 29-AUG-2013
    ' PM_RESTRICTED_TO_TERMINAL_LIST
    _xml = Nothing
    If data_table.Rows(0).Item("PM_RESTRICTED_TO_TERMINAL_LIST") IsNot DBNull.Value Then
      _xml = data_table.Rows(0).Item("PM_RESTRICTED_TO_TERMINAL_LIST")
    End If
    pPromo.terminal_list_restricted.FromXml(_xml)

    '_xml = Nothing
    'If Not data_table.Rows(0).IsNull("PM_PLAYED_TERMINAL_LIST") Then
    '  _xml = data_table.Rows(0).Item("PM_PLAYED_TERMINAL_LIST")
    'End If
    'pPromo.terminal_list_played.FromXml(_xml)

    If data_table.Rows(0).IsNull("PM_SMALL_RESOURCE_ID") Then
      pPromo.icon = Nothing
      pPromo.small_resource_id = Nothing
    Else
      pPromo.small_resource_id = data_table.Rows(0).Item("PM_SMALL_RESOURCE_ID")
      If Not GUI_BeginSQLTransaction(_sql_tx) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If
      pPromo.icon = LoadImage(pPromo.small_resource_id.Value, _sql_tx)
      Call GUI_EndSQLTransaction(_sql_tx, False)
    End If

    If data_table.Rows(0).IsNull("PM_LARGE_RESOURCE_ID") Then
      pPromo.image = Nothing
      pPromo.large_resource_id = Nothing
    Else
      pPromo.large_resource_id = data_table.Rows(0).Item("PM_LARGE_RESOURCE_ID")
      If Not GUI_BeginSQLTransaction(_sql_tx) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If
      pPromo.image = LoadImage(pPromo.large_resource_id.Value, _sql_tx)
      Call GUI_EndSQLTransaction(_sql_tx, False)
    End If

    pPromo.credit_type = data_table.Rows(0).Item("PM_CREDIT_TYPE")

    If data_table.Rows(0).IsNull("PM_TICKET_FOOTER") Then
      pPromo.ticket_footer = ""
    Else
      pPromo.ticket_footer = data_table.Rows(0).Item("PM_TICKET_FOOTER")
    End If

    If pPromo.type = Promotion.PROMOTION_TYPE.PREASSIGNED Then

      If Not GUI_BeginSQLTransaction(_sql_tx) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      WSI.Common.AccountsImport.LoadPreassignedAccounts(pPromo.promotion_id, pPromo.accounts_preassigned, _sql_tx)

      Call GUI_EndSQLTransaction(_sql_tx, False)

    End If

    ' XCD PROMO_FLAGS
    If Not GUI_BeginSQLTransaction(_sql_tx) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    Else
      WSI.Common.Promotion.ReadPromotionFlags(pPromo.promotion_id, _sql_tx, pPromo.flags_awarded, pPromo.flags_required)
      Call GUI_EndSQLTransaction(_sql_tx, False)
    End If

    ' JMM 09-NOV-2012: Visible On PromoBOX check
    pPromo.visible_on_PromoBOX = data_table.Rows(0).Item("PM_VISIBLE_ON_PROMOBOX")

    ' DHA 09-APR-2013: Expiration limit check
    If data_table.Rows(0).IsNull("PM_EXPIRATION_LIMIT") Then
      pPromo.expiration_limit.IsNull = True
      pPromo.expiration_limit.Value = DateTime.MinValue
    Else
      pPromo.expiration_limit.Value = data_table.Rows(0).Item("PM_EXPIRATION_LIMIT")
    End If

    ' JML 25-OCT-2013
    If IsDBNull(data_table.Rows(0).Item("PTC_NAME")) Then
      pPromo.change_table_name = ""
    Else
      pPromo.change_table_name = data_table.Rows(0).Item("PTC_NAME")
    End If

    ' JMM 04-NOV-2013: Applicable On PromoBOX check
    pPromo.award_on_PromoBOX = data_table.Rows(0).Item("PM_AWARD_ON_PROMOBOX")

    Try
      ' JMM 04-NOV-2013: TextOnPromoBOX
      If IsDBNull(data_table.Rows(0).Item("PM_TEXT_ON_PROMOBOX")) Then
        pPromo.text_on_PromoBOX = ""
      Else
        pPromo.text_on_PromoBOX = NullTrim(data_table.Rows(0).Item("PM_TEXT_ON_PROMOBOX"))
      End If
    Catch _exception As Exception
      Log.Exception(_exception)
    End Try

    pPromo.is_vip = data_table.Rows(0).Item("PM_VIP")
    pPromo.apply_tax = data_table.Rows(0).Item("PM_APPLY_TAX")
    pPromo.comments = ""

    pPromo.promogame = data_table.Rows(0).Item("PM_PROMOGAME_ID")
    pPromo.pyramidal_dist = IIf(IsDBNull(data_table.Rows(0).Item("PM_PYRAMIDAL_DIST")), String.Empty, data_table.Rows(0).Item("PM_PYRAMIDAL_DIST"))

    pPromo.journey_limit = data_table.Rows(0).Item("PM_JOURNEY_LIMIT")

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
  End Function ' ReadPromotion

  ''' <summary>
  ''' Update promotion by PromotionId
  ''' </summary>
  ''' <param name="pPromo"></param>
  ''' <param name="Context"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function UpdatePromotion(ByVal pPromo As TYPE_PROMOTION, ByVal Context As Integer) As Integer
    Dim _sb As StringBuilder
    Dim _list_xml As String
    Dim _list_restricted_xml As String

    Try

      If pPromo.type = Promotion.PROMOTION_TYPE.PERIODIC_PER_LEVEL_AND_MIN_PLAYED Then
        _list_xml = pPromo.provider_list.ToXml()
      Else
        _list_xml = pPromo.terminal_list.ToXml()
      End If

      If pPromo.credit_type = WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.NR1 OrElse _
         pPromo.credit_type = WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.NR2 Then
        _list_restricted_xml = pPromo.terminal_list_restricted.ToXml()
      Else
        _list_restricted_xml = Nothing
      End If

      _sb = New StringBuilder()
      _sb.AppendLine("UPDATE   PROMOTIONS                                                       ")
      _sb.AppendLine("   SET   PM_NAME = @pName                                                 ")
      _sb.AppendLine("       , PM_ENABLED = @pEnabled                                           ")
      _sb.AppendLine("       , PM_TYPE = @pType                                                 ")
      _sb.AppendLine("       , PM_DATE_START = @pDateStart                                      ")
      _sb.AppendLine("       , PM_DATE_FINISH = @pDateFinish                                    ")
      _sb.AppendLine("       , PM_SCHEDULE_WEEKDAY = @pScheduleWeekday                          ")
      _sb.AppendLine("       , PM_SCHEDULE1_TIME_FROM = @pSchedule1TimeFrom                     ")
      _sb.AppendLine("       , PM_SCHEDULE1_TIME_TO = @pSchedule1TimeTo                         ")
      _sb.AppendLine("       , PM_SCHEDULE2_ENABLED = @pSchedule2Enabled                        ")
      _sb.AppendLine("       , PM_SCHEDULE2_TIME_FROM = @pSchedule2TimeFrom                     ")
      _sb.AppendLine("       , PM_SCHEDULE2_TIME_TO = @pSchedule2TimeTo                         ")
      _sb.AppendLine("       , PM_GENDER_FILTER = @pGenderFilter                                ")
      _sb.AppendLine("       , PM_BIRTHDAY_FILTER = @pBirthdayFilter                            ")
      _sb.AppendLine("       , PM_CREATED_ACCOUNT_FILTER = @pCreatedAccountFilter               ")
      _sb.AppendLine("       , PM_EXPIRATION_TYPE = @pExpirationType                            ")
      _sb.AppendLine("       , PM_EXPIRATION_VALUE = @pExpirationValue                          ")
      _sb.AppendLine("       , PM_MIN_CASH_IN = @pMinCashIn                                     ")
      _sb.AppendLine("       , PM_MIN_CASH_IN_REWARD = @pMinCashInReward                        ")
      _sb.AppendLine("       , PM_CASH_IN = @pCashIn                                            ")
      _sb.AppendLine("       , PM_CASH_IN_REWARD = @pCashInReward                               ")
      _sb.AppendLine("       , PM_WON_LOCK = @pWonLock                                          ")
      _sb.AppendLine("       , PM_NUM_TOKENS = @pNumTokens                                      ")
      _sb.AppendLine("       , PM_TOKEN_NAME = @pTokenName                                      ")
      _sb.AppendLine("       , PM_TOKEN_REWARD = @pTokenReward                                  ")
      _sb.AppendLine("       , PM_DAILY_LIMIT = @pDailyLimit                                    ")
      _sb.AppendLine("       , PM_MONTHLY_LIMIT = @pMonthlyLimit                                ")
      _sb.AppendLine("       , PM_GLOBAL_DAILY_LIMIT = @pGlobalDailyLimit                       ")
      _sb.AppendLine("       , PM_GLOBAL_MONTHLY_LIMIT = @pGlobalMonthlyLimit                   ")
      _sb.AppendLine("       , PM_GLOBAL_LIMIT = @pGlobalLimit                                  ")
      _sb.AppendLine("       , PM_LEVEL_FILTER = @pLevelFilter                                  ")
      _sb.AppendLine("       , PM_PERMISSION = @pSpecialPermission                              ")
      _sb.AppendLine("       , PM_FREQ_FILTER_LAST_DAYS = @pFreqFilterLastDays                  ")
      _sb.AppendLine("       , PM_FREQ_FILTER_MIN_DAYS = @pFreqFilterMinDays                    ")
      _sb.AppendLine("       , PM_FREQ_FILTER_MIN_CASH_IN = @pFreqFilterMinCashIn               ")
      _sb.AppendLine("       , PM_MIN_SPENT = @pMinSpent                                        ")
      _sb.AppendLine("       , PM_MIN_SPENT_REWARD = @pMinSpentReward                           ")
      _sb.AppendLine("       , PM_SPENT = @pSpent                                               ")
      _sb.AppendLine("       , PM_SPENT_REWARD = @pSpentReward                                  ")
      _sb.AppendLine("       , PM_MIN_PLAYED = @pMinPlayed                                      ")
      _sb.AppendLine("       , PM_MIN_PLAYED_REWARD = @pMinPlayedReward                         ")
      _sb.AppendLine("       , PM_PLAYED = @pPlayed                                             ")
      _sb.AppendLine("       , PM_PLAYED_REWARD = @pPlayedReward                                ")
      _sb.AppendLine("       , PM_PROVIDER_LIST = @pProviderListValue                           ")
      _sb.AppendLine("       , PM_RESTRICTED_TO_TERMINAL_LIST = @pProviderListRestrictedValue   ")
      _sb.AppendLine("       , PM_PLAY_RESTRICTED_TO_PROVIDER_LIST = @pRestrictedToProviderList ")
      _sb.AppendLine("       , PM_SMALL_RESOURCE_ID = @pSmallImage                              ")
      _sb.AppendLine("       , PM_LARGE_RESOURCE_ID = @pLargeImage                              ")
      _sb.AppendLine("       , PM_CREDIT_TYPE = @pCreditType                                    ")
      _sb.AppendLine("       , PM_TICKET_FOOTER = @pTicketFooter                                ")
      _sb.AppendLine("       , PM_CATEGORY_ID = @pCategory                                      ")
      _sb.AppendLine("       , PM_VISIBLE_ON_PROMOBOX = @pVisibleOnPromoBOX                     ")
      _sb.AppendLine("       , PM_EXPIRATION_LIMIT = @pExpirationLimit                          ")
      _sb.AppendLine("       , PM_AWARD_ON_PROMOBOX = @pApplicableOnPromoBOX                    ")
      _sb.AppendLine("       , PM_TEXT_ON_PROMOBOX =  @pTextOnPromoBOX                          ")
      _sb.AppendLine("       , PM_VIP =  @pVip                                                  ")
      _sb.AppendLine("       , PM_APPLY_TAX =  @pApplyTax                                       ")
      _sb.AppendLine("       , PM_PROMOGAME_ID =  @pPromoGame                                   ")
      _sb.AppendLine("       , PM_PYRAMIDAL_DIST = @pPyramidalDist                              ")
      _sb.AppendLine("       , PM_JOURNEY_LIMIT =  @pJourneyLimit                               ")
      _sb.AppendLine(" WHERE   PM_PROMOTION_ID = @pPromotionId                                  ")

      Using _db_trx As New DB_TRX()

        SaveImage(pPromo.large_resource_id, pPromo.image, _db_trx.SqlTransaction)
        SaveImage(pPromo.small_resource_id, pPromo.icon, _db_trx.SqlTransaction)

        Using _cmd As New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _cmd.Parameters.Add("@pName", SqlDbType.NVarChar).Value = pPromo.name
          _cmd.Parameters.Add("@pEnabled", SqlDbType.Bit).Value = pPromo.enabled
          _cmd.Parameters.Add("@pType", SqlDbType.Int).Value = pPromo.type
          _cmd.Parameters.Add("@pDateStart", SqlDbType.DateTime).Value = pPromo.date_start.Value
          _cmd.Parameters.Add("@pDateFinish", SqlDbType.DateTime).Value = pPromo.date_finish.Value
          _cmd.Parameters.Add("@pScheduleWeekday", SqlDbType.Int).Value = pPromo.schedule_weekday
          _cmd.Parameters.Add("@pSchedule1TimeFrom", SqlDbType.Int).Value = pPromo.schedule1_time_from
          _cmd.Parameters.Add("@pSchedule1TimeTo", SqlDbType.Int).Value = pPromo.schedule1_time_to
          _cmd.Parameters.Add("@pSchedule2Enabled", SqlDbType.Bit).Value = pPromo.schedule2_enabled
          _cmd.Parameters.Add("@pSchedule2TimeFrom", SqlDbType.Int).Value = pPromo.schedule2_time_from
          _cmd.Parameters.Add("@pSchedule2TimeTo", SqlDbType.Int).Value = pPromo.schedule2_time_to
          _cmd.Parameters.Add("@pGenderFilter", SqlDbType.Int).Value = pPromo.gender_filter
          _cmd.Parameters.Add("@pBirthdayFilter", SqlDbType.Int).Value = pPromo.birthday_filter
          _cmd.Parameters.Add("@pCreatedAccountFilter", SqlDbType.Int).Value = pPromo.created_account_filter
          _cmd.Parameters.Add("@pExpirationType", SqlDbType.Int).Value = pPromo.expiration_type
          _cmd.Parameters.Add("@pExpirationValue", SqlDbType.Int).Value = pPromo.expiration_value
          _cmd.Parameters.Add("@pMinCashIn", SqlDbType.Money).Value = pPromo.min_cash_in
          _cmd.Parameters.Add("@pMinCashInReward", SqlDbType.Money).Value = pPromo.min_cash_in_reward
          _cmd.Parameters.Add("@pCashIn", SqlDbType.Money).Value = pPromo.cash_in
          _cmd.Parameters.Add("@pCashInReward", SqlDbType.Money).Value = pPromo.cash_in_reward
          _cmd.Parameters.Add("@pWonLock", SqlDbType.Money).Value = IIf(pPromo.won_lock_enabled, pPromo.won_lock, DBNull.Value)
          _cmd.Parameters.Add("@pNumTokens", SqlDbType.Int).Value = pPromo.num_tokens
          _cmd.Parameters.Add("@pTokenName", SqlDbType.VarChar).Value = IIf(IsNothing(pPromo.token_name), DBNull.Value, pPromo.token_name)
          _cmd.Parameters.Add("@pTokenReward", SqlDbType.Money).Value = pPromo.token_reward
          _cmd.Parameters.Add("@pDailyLimit", SqlDbType.Money).Value = IIf(pPromo.daily_limit > 0, pPromo.daily_limit, DBNull.Value)
          _cmd.Parameters.Add("@pMonthlyLimit", SqlDbType.Money).Value = IIf(pPromo.monthly_limit > 0, pPromo.monthly_limit, DBNull.Value)
          _cmd.Parameters.Add("@pGlobalDailyLimit", SqlDbType.Money).Value = IIf(pPromo.global_daily_limit > 0, pPromo.global_daily_limit, DBNull.Value)
          _cmd.Parameters.Add("@pGlobalMonthlyLimit", SqlDbType.Money).Value = IIf(pPromo.global_monthly_limit > 0, pPromo.global_monthly_limit, DBNull.Value)
          _cmd.Parameters.Add("@pGlobalLimit", SqlDbType.Money).Value = IIf(pPromo.global_limit > 0, pPromo.global_limit, DBNull.Value)
          _cmd.Parameters.Add("@pLevelFilter", SqlDbType.Int).Value = pPromo.level_filter
          _cmd.Parameters.Add("@pSpecialPermission", SqlDbType.Int).Value = pPromo.special_permission
          _cmd.Parameters.Add("@pFreqFilterLastDays", SqlDbType.Int).Value = pPromo.freq_filter_last_days
          _cmd.Parameters.Add("@pFreqFilterMinDays", SqlDbType.Int).Value = pPromo.freq_filter_min_days
          _cmd.Parameters.Add("@pFreqFilterMinCashIn", SqlDbType.Money).Value = pPromo.freq_filter_min_cash_in
          _cmd.Parameters.Add("@pMinSpent", SqlDbType.Money).Value = pPromo.min_spent
          _cmd.Parameters.Add("@pMinSpentReward", SqlDbType.Money).Value = pPromo.min_spent_reward
          _cmd.Parameters.Add("@pSpent", SqlDbType.Money).Value = pPromo.spent
          _cmd.Parameters.Add("@pSpentReward", SqlDbType.Money).Value = pPromo.spent_reward
          _cmd.Parameters.Add("@pMinPlayed", SqlDbType.Money).Value = pPromo.min_played
          _cmd.Parameters.Add("@pMinPlayedReward", SqlDbType.Money).Value = pPromo.min_played_reward
          _cmd.Parameters.Add("@pPlayed", SqlDbType.Money).Value = pPromo.played
          _cmd.Parameters.Add("@pPlayedReward", SqlDbType.Money).Value = pPromo.played_reward
          _cmd.Parameters.Add("@pProviderListValue", SqlDbType.NVarChar).Value = IIf(IsNothing(_list_xml), DBNull.Value, _list_xml)
          _cmd.Parameters.Add("@pProviderListRestrictedValue", SqlDbType.NVarChar).Value = IIf(IsNothing(_list_restricted_xml), DBNull.Value, _list_restricted_xml)
          _cmd.Parameters.Add("@pRestrictedToProviderList", SqlDbType.Int).Value = IIf(IsNothing(_list_xml), "0", "1")
          _cmd.Parameters.Add("@pSmallImage", SqlDbType.BigInt).Value = IIf(pPromo.small_resource_id.HasValue, pPromo.small_resource_id, DBNull.Value)
          _cmd.Parameters.Add("@pLargeImage", SqlDbType.BigInt).Value = IIf(pPromo.large_resource_id.HasValue, pPromo.large_resource_id, DBNull.Value)
          _cmd.Parameters.Add("@pCreditType", SqlDbType.Int).Value = pPromo.credit_type
          _cmd.Parameters.Add("@pTicketFooter", SqlDbType.NVarChar).Value = pPromo.ticket_footer
          _cmd.Parameters.Add("@pCategory", SqlDbType.Int).Value = pPromo.category_id
          _cmd.Parameters.Add("@pVisibleOnPromoBOX", SqlDbType.Bit).Value = pPromo.visible_on_PromoBOX
          _cmd.Parameters.Add("@pExpirationLimit", SqlDbType.DateTime).Value = IIf(pPromo.expiration_limit.Value = DateTime.MinValue, DBNull.Value, pPromo.expiration_limit.Value)
          _cmd.Parameters.Add("@pApplicableOnPromoBOX", SqlDbType.Int).Value = pPromo.award_on_PromoBOX
          _cmd.Parameters.Add("@pTextOnPromoBOX", SqlDbType.NVarChar).Value = IIf(IsNothing(pPromo.text_on_PromoBOX), DBNull.Value, pPromo.text_on_PromoBOX)
          _cmd.Parameters.Add("@pVip", SqlDbType.Bit).Value = pPromo.is_vip
          _cmd.Parameters.Add("@pApplyTax", SqlDbType.Bit).Value = pPromo.apply_tax
          _cmd.Parameters.Add("@pPromoGame", SqlDbType.BigInt).Value = IIf(pPromo.promogame = -1, DBNull.Value, pPromo.promogame)
          _cmd.Parameters.Add("@pPyramidalDist", SqlDbType.Xml).Value = IIf(pPromo.pyramidal_dist = String.Empty, DBNull.Value, pPromo.pyramidal_dist)
          _cmd.Parameters.Add("@pJourneyLimit", SqlDbType.Bit).Value = pPromo.journey_limit
          _cmd.Parameters.Add("@pPromotionId", SqlDbType.BigInt).Value = pPromo.promotion_id

          If _cmd.ExecuteNonQuery() <> 1 Then
            Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
          End If

          If Not Promotion.SavePromotionFlags(pPromo.promotion_id, pPromo.flags_required, pPromo.flags_awarded, _db_trx.SqlTransaction) Then
            Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
          End If

          _db_trx.Commit()

          Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

        End Using
      End Using

    Catch _ex As Exception
      Log.Error("cls_promotion.UpdatePromotion failed!")
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

  End Function ' UpdatePromotion

  Private Function InsertPromotion(ByVal pPromo As TYPE_PROMOTION, ByVal Context As Integer) As Integer

    Dim _list_xml As String
    Dim _list_restricted_xml As String
    Dim _sql_trx As System.Data.SqlClient.SqlTransaction = Nothing
    Dim _sb As StringBuilder
    Dim _param As SqlClient.SqlParameter
    Dim _promotion_id As Int64

    If Not GUI_BeginSQLTransaction(_sql_trx) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    If pPromo.type = Promotion.PROMOTION_TYPE.PERIODIC_PER_LEVEL_AND_MIN_PLAYED Then
      _list_xml = pPromo.provider_list.ToXml()
    Else
      _list_xml = pPromo.terminal_list.ToXml()
    End If

    ' AMF 29-AUG-2013
    _list_restricted_xml = pPromo.terminal_list_restricted.ToXml()

    SaveImage(pPromo.large_resource_id, pPromo.image, _sql_trx)
    SaveImage(pPromo.small_resource_id, pPromo.icon, _sql_trx)

    Try

      _sb = New StringBuilder()
      ' AVZ 01-OCT-2015: Added column PM_CREATED_ACCOUNT_FILTER
      _sb.AppendLine(" INSERT INTO   PROMOTIONS ")
      _sb.AppendLine("             ( PM_NAME ")
      _sb.AppendLine("             , PM_ENABLED ")
      _sb.AppendLine("             , PM_TYPE ")
      _sb.AppendLine("             , PM_DATE_START ")
      _sb.AppendLine("             , PM_DATE_FINISH ")
      _sb.AppendLine("             , PM_SCHEDULE_WEEKDAY ")
      _sb.AppendLine("             , PM_SCHEDULE1_TIME_FROM ")
      _sb.AppendLine("             , PM_SCHEDULE1_TIME_TO ")
      _sb.AppendLine("             , PM_SCHEDULE2_ENABLED ")
      _sb.AppendLine("             , PM_SCHEDULE2_TIME_FROM ")
      _sb.AppendLine("             , PM_SCHEDULE2_TIME_TO ")
      _sb.AppendLine("             , PM_GENDER_FILTER ")
      _sb.AppendLine("             , PM_BIRTHDAY_FILTER ")
      _sb.AppendLine("             , PM_CREATED_ACCOUNT_FILTER ")
      _sb.AppendLine("             , PM_EXPIRATION_TYPE ")
      _sb.AppendLine("             , PM_EXPIRATION_VALUE ")
      _sb.AppendLine("             , PM_MIN_CASH_IN ")
      _sb.AppendLine("             , PM_MIN_CASH_IN_REWARD ")
      _sb.AppendLine("             , PM_CASH_IN ")
      _sb.AppendLine("             , PM_CASH_IN_REWARD ")
      _sb.AppendLine("             , PM_WON_LOCK ")
      _sb.AppendLine("             , PM_NUM_TOKENS ")
      _sb.AppendLine("             , PM_TOKEN_NAME ")
      _sb.AppendLine("             , PM_TOKEN_REWARD ")
      _sb.AppendLine("             , PM_DAILY_LIMIT ")
      _sb.AppendLine("             , PM_MONTHLY_LIMIT ")
      _sb.AppendLine("             , PM_GLOBAL_DAILY_LIMIT ")
      _sb.AppendLine("             , PM_GLOBAL_MONTHLY_LIMIT ")
      _sb.AppendLine("             , PM_GLOBAL_LIMIT ")
      _sb.AppendLine("             , PM_LEVEL_FILTER ")
      _sb.AppendLine("             , PM_PERMISSION ")
      _sb.AppendLine("             , PM_FREQ_FILTER_LAST_DAYS ")
      _sb.AppendLine("             , PM_FREQ_FILTER_MIN_DAYS ")
      _sb.AppendLine("             , PM_FREQ_FILTER_MIN_CASH_IN ")
      _sb.AppendLine("             , PM_MIN_SPENT ")
      _sb.AppendLine("             , PM_MIN_SPENT_REWARD ")
      _sb.AppendLine("             , PM_SPENT ")
      _sb.AppendLine("             , PM_SPENT_REWARD ")
      _sb.AppendLine("             , PM_MIN_PLAYED ")
      _sb.AppendLine("             , PM_MIN_PLAYED_REWARD ")
      _sb.AppendLine("             , PM_PLAYED ")
      _sb.AppendLine("             , PM_PLAYED_REWARD ")
      _sb.AppendLine("             , PM_PROVIDER_LIST ")
      _sb.AppendLine("             , PM_SMALL_RESOURCE_ID ")
      _sb.AppendLine("             , PM_LARGE_RESOURCE_ID ")
      _sb.AppendLine("             , PM_CREDIT_TYPE ")
      _sb.AppendLine("             , PM_TICKET_FOOTER ")
      _sb.AppendLine("             , PM_CATEGORY_ID ")
      _sb.AppendLine("             , PM_VISIBLE_ON_PROMOBOX ")
      _sb.AppendLine("             , PM_EXPIRATION_LIMIT ")
      _sb.AppendLine("             , PM_RESTRICTED_TO_TERMINAL_LIST ")
      _sb.AppendLine("             , PM_PLAY_RESTRICTED_TO_PROVIDER_LIST ")
      _sb.AppendLine("             , PM_AWARD_ON_PROMOBOX ")
      _sb.AppendLine("             , PM_TEXT_ON_PROMOBOX ")
      _sb.AppendLine("             , PM_VIP ")
      _sb.AppendLine("             , PM_APPLY_TAX ")
      _sb.AppendLine("             , PM_PROMOGAME_ID ")
      _sb.AppendLine("             , PM_PYRAMIDAL_DIST ")
      _sb.AppendLine("             , PM_JOURNEY_LIMIT ")
      '_sb.AppendLine("             , PM_PLAYED_TERMINAL_LIST ")
      _sb.AppendLine("             ) ")
      _sb.AppendLine("               VALUES ")
      _sb.AppendLine("             ( @pName ")
      _sb.AppendLine("             , @pEnabled ")
      _sb.AppendLine("             , @pType ")
      _sb.AppendLine("             , @pDateStart ")
      _sb.AppendLine("             , @pDateFinish ")
      _sb.AppendLine("             , @pScheduleWeekday ")
      _sb.AppendLine("             , @pSchedule1TimeFrom ")
      _sb.AppendLine("             , @pSchedule1TimeTo ")
      _sb.AppendLine("             , @pSchedule2Enabled ")
      _sb.AppendLine("             , @pSchedule2TimeFrom ")
      _sb.AppendLine("             , @pSchedule2TimeTo ")
      _sb.AppendLine("             , @pGenderFilter ")
      _sb.AppendLine("             , @pBirthdayFilter ")
      _sb.AppendLine("             , @pCreatedAccountFilter ")
      _sb.AppendLine("             , @pExpirationType ")
      _sb.AppendLine("             , @pExpirationValue ")
      _sb.AppendLine("             , @pMinCashIn ")
      _sb.AppendLine("             , @pMinCashInReward ")
      _sb.AppendLine("             , @pCashIn ")
      _sb.AppendLine("             , @pCashInReward ")
      _sb.AppendLine("             , @pWonLock ")
      _sb.AppendLine("             , @pNumTokens ")
      _sb.AppendLine("             , @pTokenName ")
      _sb.AppendLine("             , @pTokenReward ")
      _sb.AppendLine("             , @pDailyLimit ")
      _sb.AppendLine("             , @pMonthlyLimit ")
      _sb.AppendLine("             , @pGlobalDailyLimit ")
      _sb.AppendLine("             , @pGlobalMonthlyLimit ")
      _sb.AppendLine("             , @pGlobalLimit ")
      _sb.AppendLine("             , @pLevelFilter ")
      _sb.AppendLine("             , @pSpecialPermission ")
      _sb.AppendLine("             , @pFreqFilterLastDays ")
      _sb.AppendLine("             , @pFreqFilterMinDays ")
      _sb.AppendLine("             , @pFreqFilterMinCashIn ")
      _sb.AppendLine("             , @pMinSpent ")
      _sb.AppendLine("             , @pMinSpentReward ")
      _sb.AppendLine("             , @pSpent ")
      _sb.AppendLine("             , @pSpentReward ")
      _sb.AppendLine("             , @pMinPlayed ")
      _sb.AppendLine("             , @pMinPlayedReward ")
      _sb.AppendLine("             , @pPlayed ")
      _sb.AppendLine("             , @pPlayedReward ")
      _sb.AppendLine("             , @pProviderListValue ")
      _sb.AppendLine("             , @pSmallImage ")
      _sb.AppendLine("             , @pLargeImage ")
      _sb.AppendLine("             , @pCreditType ")
      _sb.AppendLine("             , @pTicketFooter ")
      _sb.AppendLine("             , @pCategory ")
      _sb.AppendLine("             , @pVisibleOnPromoBOX")
      _sb.AppendLine("             , @pExpirationLimit")
      _sb.AppendLine("             , @pProviderListRestrictedValue ")
      _sb.AppendLine("             , @pRestrictedToProviderList")
      _sb.AppendLine("             , @pApplicableOnPromoBOX")
      _sb.AppendLine("             , @pTextOnPromoBOX")
      _sb.AppendLine("             , @pVip")
      _sb.AppendLine("             , @pApplyTax")
      _sb.AppendLine("             , @pPromoGame")
      _sb.AppendLine("             , @pPyramidalDist")
      _sb.AppendLine("             , @pJourneyLimit")
      '_sb.AppendLine("             , @pProviderListPlayedValue")
      _sb.AppendLine("             ) ")

      _sb.AppendLine("SET @pPromotionId = SCOPE_IDENTITY()")

      Using _cmd As New SqlClient.SqlCommand(_sb.ToString(), _sql_trx.Connection, _sql_trx)

        _cmd.Parameters.Add("@pName", SqlDbType.NVarChar).Value = pPromo.name
        _cmd.Parameters.Add("@pEnabled", SqlDbType.Bit).Value = pPromo.enabled
        _cmd.Parameters.Add("@pType", SqlDbType.Int).Value = pPromo.type
        _cmd.Parameters.Add("@pDateStart", SqlDbType.DateTime).Value = pPromo.date_start.Value
        _cmd.Parameters.Add("@pDateFinish", SqlDbType.DateTime).Value = pPromo.date_finish.Value
        _cmd.Parameters.Add("@pScheduleWeekday", SqlDbType.Int).Value = pPromo.schedule_weekday
        _cmd.Parameters.Add("@pSchedule1TimeFrom", SqlDbType.Int).Value = pPromo.schedule1_time_from
        _cmd.Parameters.Add("@pSchedule1TimeTo", SqlDbType.Int).Value = pPromo.schedule1_time_to
        _cmd.Parameters.Add("@pSchedule2Enabled", SqlDbType.Bit).Value = pPromo.schedule2_enabled
        _cmd.Parameters.Add("@pSchedule2TimeFrom", SqlDbType.Int).Value = pPromo.schedule2_time_from
        _cmd.Parameters.Add("@pSchedule2TimeTo", SqlDbType.Int).Value = pPromo.schedule2_time_to
        _cmd.Parameters.Add("@pGenderFilter", SqlDbType.Int).Value = pPromo.gender_filter
        _cmd.Parameters.Add("@pBirthdayFilter", SqlDbType.Int).Value = pPromo.birthday_filter
        _cmd.Parameters.Add("@pCreatedAccountFilter", SqlDbType.Int).Value = pPromo.created_account_filter
        _cmd.Parameters.Add("@pExpirationType", SqlDbType.Int).Value = pPromo.expiration_type
        _cmd.Parameters.Add("@pExpirationValue", SqlDbType.Int).Value = pPromo.expiration_value
        _cmd.Parameters.Add("@pMinCashIn", SqlDbType.Money).Value = pPromo.min_cash_in
        _cmd.Parameters.Add("@pMinCashInReward", SqlDbType.Money).Value = pPromo.min_cash_in_reward
        _cmd.Parameters.Add("@pCashIn", SqlDbType.Money).Value = pPromo.cash_in
        _cmd.Parameters.Add("@pCashInReward", SqlDbType.Money).Value = pPromo.cash_in_reward
        _cmd.Parameters.Add("@pWonLock", SqlDbType.Money).Value = IIf(pPromo.won_lock_enabled, pPromo.won_lock, DBNull.Value)
        _cmd.Parameters.Add("@pNumTokens", SqlDbType.Int).Value = pPromo.num_tokens
        _cmd.Parameters.Add("@pTokenName", SqlDbType.VarChar).Value = IIf(IsNothing(pPromo.token_name), DBNull.Value, pPromo.token_name)
        _cmd.Parameters.Add("@pTokenReward", SqlDbType.Money).Value = pPromo.token_reward
        _cmd.Parameters.Add("@pDailyLimit", SqlDbType.Money).Value = IIf(pPromo.daily_limit > 0, pPromo.daily_limit, DBNull.Value)
        _cmd.Parameters.Add("@pMonthlyLimit", SqlDbType.Money).Value = IIf(pPromo.monthly_limit > 0, pPromo.monthly_limit, DBNull.Value)
        _cmd.Parameters.Add("@pGlobalDailyLimit", SqlDbType.Money).Value = IIf(pPromo.global_daily_limit > 0, pPromo.global_daily_limit, DBNull.Value)
        _cmd.Parameters.Add("@pGlobalMonthlyLimit", SqlDbType.Money).Value = IIf(pPromo.global_monthly_limit > 0, pPromo.global_monthly_limit, DBNull.Value)
        _cmd.Parameters.Add("@pGlobalLimit", SqlDbType.Money).Value = IIf(pPromo.global_limit > 0, pPromo.global_limit, DBNull.Value)
        _cmd.Parameters.Add("@pLevelFilter", SqlDbType.Int).Value = pPromo.level_filter
        _cmd.Parameters.Add("@pSpecialPermission", SqlDbType.Int).Value = pPromo.special_permission
        _cmd.Parameters.Add("@pFreqFilterLastDays", SqlDbType.Int).Value = pPromo.freq_filter_last_days
        _cmd.Parameters.Add("@pFreqFilterMinDays", SqlDbType.Int).Value = pPromo.freq_filter_min_days
        _cmd.Parameters.Add("@pFreqFilterMinCashIn", SqlDbType.Money).Value = pPromo.freq_filter_min_cash_in
        _cmd.Parameters.Add("@pMinSpent", SqlDbType.Money).Value = pPromo.min_spent
        _cmd.Parameters.Add("@pMinSpentReward", SqlDbType.Money).Value = pPromo.min_spent_reward
        _cmd.Parameters.Add("@pSpent", SqlDbType.Money).Value = pPromo.spent
        _cmd.Parameters.Add("@pSpentReward", SqlDbType.Money).Value = pPromo.spent_reward
        _cmd.Parameters.Add("@pMinPlayed", SqlDbType.Money).Value = pPromo.min_played
        _cmd.Parameters.Add("@pMinPlayedReward", SqlDbType.Money).Value = pPromo.min_played_reward
        _cmd.Parameters.Add("@pPlayed", SqlDbType.Money).Value = pPromo.played
        _cmd.Parameters.Add("@pPlayedReward", SqlDbType.Money).Value = pPromo.played_reward
        _cmd.Parameters.Add("@pProviderListValue", SqlDbType.NVarChar).Value = IIf(IsNothing(_list_xml), DBNull.Value, _list_xml)
        _cmd.Parameters.Add("@pSmallImage", SqlDbType.BigInt).Value = IIf(pPromo.small_resource_id.HasValue, pPromo.small_resource_id, DBNull.Value)
        _cmd.Parameters.Add("@pLargeImage", SqlDbType.BigInt).Value = IIf(pPromo.large_resource_id.HasValue, pPromo.large_resource_id, DBNull.Value)
        _cmd.Parameters.Add("@pCreditType", SqlDbType.Int).Value = pPromo.credit_type
        _cmd.Parameters.Add("@pTicketFooter", SqlDbType.NVarChar).Value = pPromo.ticket_footer
        _cmd.Parameters.Add("@pCategory", SqlDbType.Int).Value = pPromo.category_id
        _cmd.Parameters.Add("@pVisibleOnPromoBOX", SqlDbType.Bit).Value = pPromo.visible_on_PromoBOX
        _cmd.Parameters.Add("@pExpirationLimit", SqlDbType.DateTime).Value = IIf(pPromo.expiration_limit.Value = DateTime.MinValue, DBNull.Value, pPromo.expiration_limit.Value)
        _cmd.Parameters.Add("@pProviderListRestrictedValue", SqlDbType.NVarChar).Value = IIf(IsNothing(_list_restricted_xml), DBNull.Value, _list_restricted_xml)
        _cmd.Parameters.Add("@pRestrictedToProviderList", SqlDbType.Int).Value = IIf(IsNothing(_list_xml), "0", "1")
        _cmd.Parameters.Add("@pApplicableOnPromoBOX", SqlDbType.Int).Value = pPromo.award_on_PromoBOX
        ' 21-NOV-2013  JPJ    Defect WIG-426 Can't create a periodic promotion. 
        _cmd.Parameters.Add("@pTextOnPromoBOX", SqlDbType.NVarChar).Value = IIf(IsNothing(pPromo.text_on_PromoBOX), DBNull.Value, pPromo.text_on_PromoBOX)
        _cmd.Parameters.Add("@pVip", SqlDbType.Bit).Value = pPromo.is_vip
        _cmd.Parameters.Add("@pApplyTax", SqlDbType.Bit).Value = pPromo.apply_tax
        _cmd.Parameters.Add("@pPromoGame", SqlDbType.BigInt).Value = IIf(pPromo.promogame = -1, DBNull.Value, pPromo.promogame)
        _cmd.Parameters.Add("@pPyramidalDist", SqlDbType.Xml).Value = IIf(pPromo.pyramidal_dist = String.Empty, DBNull.Value, pPromo.pyramidal_dist)

        _cmd.Parameters.Add("@pJourneyLimit", SqlDbType.Bit).Value = pPromo.journey_limit
        _param = _cmd.Parameters.Add("@pPromotionId", SqlDbType.BigInt)
        _param.Direction = ParameterDirection.Output

        If _cmd.ExecuteNonQuery() <> 1 Then
          GUI_EndSQLTransaction(_sql_trx, False)

          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

        _promotion_id = _param.Value

        If pPromo.type = Promotion.PROMOTION_TYPE.PREASSIGNED Then

          If Not WSI.Common.AccountsImport.InsertForPreassignedPromotion(pPromo.accounts_preassigned, _promotion_id, _sql_trx) Then
            Throw New Exception("")
          End If

        End If

        ' XCD 04-OCT-2012
        If Not WSI.Common.Promotion.SavePromotionFlags(_promotion_id, pPromo.flags_required, pPromo.flags_awarded, _sql_trx) Then
          GUI_EndSQLTransaction(_sql_trx, False)
          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

      End Using

      If Not GUI_EndSQLTransaction(_sql_trx, True) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

    Catch ex As Exception
      If Not GUI_EndSQLTransaction(_sql_trx, False) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

  End Function ' InsertPromotion

  Private Function DeletePromotion(ByVal PromotionId As Integer, _
                                   ByVal SmallResourceId As Nullable(Of Long), _
                                   ByVal LargeResourceId As Nullable(Of Long), _
                                   ByVal Context As Integer) As Integer
    Dim str_sql As String
    Dim db_count As Integer = 0
    Dim SqlTrans As System.Data.SqlClient.SqlTransaction = Nothing
    Dim data_table As DataTable

    If Not GUI_BeginSQLTransaction(SqlTrans) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    ' QMP 23-APR-2013: Check if promotion has been awarded
    str_sql = " SELECT   COUNT(*)           " & _
              "   FROM   ACCOUNT_PROMOTIONS " & _
              "  WHERE   ACP_PROMO_ID =     " & PromotionId

    data_table = GUI_GetTableUsingSQL(str_sql, 5000)
    If IsNothing(data_table) Then
      GUI_EndSQLTransaction(SqlTrans, False)

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    db_count = data_table.Rows(0).Item(0)

    If db_count > 0 Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DEPENDENCIES
    End If

    str_sql = "DELETE  PROMOTIONS " & _
              " WHERE  PM_PROMOTION_ID = " & PromotionId & _
                " AND  PM_TYPE IN    (    " & Promotion.PROMOTION_TYPE.MANUAL _
                                  & "   , " & Promotion.PROMOTION_TYPE.PERIODIC_PER_LEVEL _
                                  & "   , " & Promotion.PROMOTION_TYPE.PERIODIC_PER_LEVEL_AND_MIN_PLAYED _
                                  & "   , " & Promotion.PROMOTION_TYPE.PERIODIC_PER_BIRTHDAY _
                                  & ")"  ' Only user-type & periodical promotions can be deleted

    If Not GUI_SQLExecuteNonQuery(str_sql, SqlTrans) Then
      GUI_EndSQLTransaction(SqlTrans, False)

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    If SmallResourceId.HasValue Then
      If Not WSI.Common.WKTResources.Delete(SmallResourceId, SqlTrans) Then
        ' Rollback  
        GUI_EndSQLTransaction(SqlTrans, False)

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If
    End If

    If LargeResourceId.HasValue Then
      If Not WSI.Common.WKTResources.Delete(LargeResourceId, SqlTrans) Then
        ' Rollback  
        GUI_EndSQLTransaction(SqlTrans, False)

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If
    End If

    ' Delete promotion flags
    str_sql = "DELETE PROMOTION_FLAGS WHERE PF_PROMOTION_ID = " & PromotionId
    If Not GUI_SQLExecuteNonQuery(str_sql, SqlTrans) Then
      GUI_EndSQLTransaction(SqlTrans, False)
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB 'Rollback
    End If

    If Not GUI_EndSQLTransaction(SqlTrans, True) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    ' For auditor purposes
    Me.PromoScreenMode = ENUM_PROMO_SCREEN_MODE.MODE_DELETE 'Delete

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
  End Function ' DeletePromotion

#End Region ' Private Functions

#Region " Public Functions "

  ' PURPOSE: Set every image reference in a new copied promotion to nothing
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub ResetImages()

    Me.Image = Nothing
    Me.Icon = Nothing
    Me.m_promotion.large_resource_id = Nothing
    Me.m_promotion.small_resource_id = Nothing

  End Sub 'ResetImages

  Public Sub ResetIdValues()

    Me.PromotionId = 0
    Me.Name = String.Empty
    Me.ResetImages()

  End Sub

  ''' <summary>
  ''' Function that check if a preasignedpromotions that edit have a status pendingplayed
  ''' </summary>
  ''' <param name="PromotionId"></param>
  ''' <returns> 
  '''       true  : If the promo preasigned is pending to play a promo game
  '''       false : If the promo haven't any pending play or is played 
  ''' </returns>
  ''' <remarks></remarks>
  Public Function IsPromotionWithPromoGamePendingPlayed(ByVal PromotionId As Integer) As Boolean
    Dim _sb As StringBuilder
    Dim _obj As Object
    Dim _result As Boolean

    _sb = New StringBuilder()
    _result = False
    _obj = Nothing

    Try

      _sb.AppendLine("SELECT   ACP_PROMO_ID                    ")
      _sb.AppendLine("  FROM   ACCOUNT_PROMOTIONS              ")
      _sb.AppendLine(" WHERE   ACP_PROMO_ID = @pPromotionId    ")
      _sb.AppendLine("   AND   ACP_STATUS = 12                 ")

      Using _db_trx As New DB_TRX()
        Using _cmd As SqlCommand = New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          _cmd.Parameters.Add("@pPromotionId", SqlDbType.Int).Value = PromotionId

          _obj = _cmd.ExecuteScalar()

          If _obj IsNot Nothing Then
            _result = True
          End If
        End Using
      End Using
    Finally

    End Try

    Return _result

  End Function
#End Region ' Public Functions

End Class
