'-------------------------------------------------------------------
' Copyright © 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_counter_configuration.vb
' DESCRIPTION:   Configure for counters machine
' AUTHOR:        Samuel González Berdugo
' CREATION DATE: 18-AUG-2014
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 18-AUG-2014  SGB    Initial version
' 15-SEP-2014  DRV    Several changes in auditor data
' 30-SEP-2014  DRV    Fixed Bug WIG-1323: Error saving denomination data
' 30-SEP-2014  DRV    Fixed Bug WIG-1328: Multicurrency not supported
' 28-OCT-2014  DRV    Fixed Bug WIG-1575: Denominations should allow modifications.
'--------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports System.Text
Imports WSI.Common
Imports WSI.Common.NoteScanner
Imports System.IO.Ports.StopBits

Public Class CLASS_COUNTER_CONFIGURATION
  Inherits CLASS_BASE

  Public Class TYPE_COUNTER
    Public application_id As Int64
    Public model As String
    Public currency As String
    Public communication_type As Int32
    Public port As NoteCounterPort
    Public baudrate As NoteCounterBaudrate
    Public stopbits As Int32
    Public databits As NoteCounterDatabits
    Public parity As Int32
    Public denomination As DataTable = New DataTable()

  End Class


#Region " Enums "

  Private Enum ENUM_APPLICATIONS
    WigosGUI = 0
  End Enum

  Private Enum ROWACTION
    NORMAL = 0
    MODIFIED = 1
    DELETED = 2
  End Enum

#End Region                                               'Enums

#Region " Constants "

  Private Const AUDIT_STATE_COLUMN_NAME As String = "ACTION"

#End Region                                           'Constants

#Region " Members "

  Protected m_counter As New TYPE_COUNTER()

#End Region                                             'Members

#Region " Properties "

  Private ReadOnly Property ApplicationNameInDB(ByVal Application As ENUM_APPLICATIONS) As String
    Get
      Select Case Application
        Case ENUM_APPLICATIONS.WigosGUI
          Return "WigosGUI"

        Case Else
          Return Nothing

      End Select
    End Get
  End Property   'AplicationNameInDB

  Public Property ApplicationId() As Int64
    Get
      Return m_counter.application_id
    End Get
    Set(ByVal Value As Int64)
      m_counter.application_id = Value
    End Set
  End Property                   'ApplicationId

  Public Property Model() As NoteCounterModel
    Get
      Return m_counter.model
    End Get
    Set(ByVal Value As NoteCounterModel)
      m_counter.model = Value
    End Set
  End Property                           'Model

  Public Property Currency() As String
    Get
      Return m_counter.currency
    End Get
    Set(ByVal Value As String)
      m_counter.currency = Value
    End Set
  End Property                        'Currency

  Public Property CommunicationType() As NoteCounterCommunication
    Get
      Return m_counter.communication_type
    End Get
    Set(ByVal Value As NoteCounterCommunication)
      m_counter.communication_type = Value
    End Set
  End Property               'CommunicationType

  Public Property Port() As NoteCounterPort
    Get
      Return m_counter.port
    End Get
    Set(ByVal Value As NoteCounterPort)
      m_counter.port = Value
    End Set
  End Property                            'Port

  Public Property Baudrate() As NoteCounterBaudrate
    Get
      Return m_counter.baudrate
    End Get
    Set(ByVal Value As NoteCounterBaudrate)
      m_counter.baudrate = Value
    End Set
  End Property                        'Baudrate

  Public Property Stopbits() As Int32
    Get
      Return m_counter.stopbits
    End Get
    Set(ByVal Value As Int32)
      m_counter.stopbits = Value
    End Set
  End Property                        'Stopbits

  Public Property Databits() As NoteCounterDatabits
    Get
      Return m_counter.databits
    End Get
    Set(ByVal Value As NoteCounterDatabits)
      m_counter.databits = Value
    End Set
  End Property                        'Databits

  Public Property Parity() As Int32
    Get
      Return m_counter.parity
    End Get
    Set(ByVal Value As Int32)
      m_counter.parity = Value
    End Set
  End Property                          'Parity

  Public Property Denomination() As DataTable
    Get
      Return m_counter.denomination
    End Get
    Set(ByVal Value As DataTable)
      m_counter.denomination = Value
    End Set
  End Property                    'Denomination
#End Region                                          'Properties

#Region "Overrides"

  ' PURPOSE: Returns the related auditor data for the current object.
  '
  ' PARAMS:
  '   - INPUT: 
  '           - None
  '   - OUTPUT: 
  '           - None
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA
    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _currency_exchange As CurrencyExchangeProperties

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_CASHIER_CONFIGURATION)
    _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(5309), "") 'Configuración de contadora

    '---- configuration counter
    Select Case Me.Model
      Case NoteCounterModel.None
        Call _auditor_data.SetField(0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5439), GLB_NLS_GUI_PLAYER_TRACKING.GetString(5311)) 'Modelo de contadora %1
      Case NoteCounterModel.JetScan4091
        Call _auditor_data.SetField(0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5306), GLB_NLS_GUI_PLAYER_TRACKING.GetString(5311)) 'Modelo de contadora %1
      Case Else
        Call _auditor_data.SetField(0, AUDIT_NONE_STRING, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5311))
    End Select

    Call _auditor_data.SetField(0, IIf(Not String.IsNullOrEmpty(Me.Currency), Me.Currency, AUDIT_NONE_STRING), GLB_NLS_GUI_PLAYER_TRACKING.GetString(5312)) 'Tipo de moneda %1

    If Me.Model <> 0 Then
      Select Case Me.CommunicationType
        Case NoteCounterCommunication.File 'File
          Call _auditor_data.SetField(0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5307), GLB_NLS_GUI_PLAYER_TRACKING.GetString(5313)) 'Tipo de comunicación %1
        Case NoteCounterCommunication.Serial 'Serial
          Call _auditor_data.SetField(0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5308), GLB_NLS_GUI_PLAYER_TRACKING.GetString(5313))
        Case Else
          Call _auditor_data.SetField(0, AUDIT_NONE_STRING, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5313))
      End Select
    Else
      Call _auditor_data.SetField(0, AUDIT_NONE_STRING, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5313))
    End If

    If Me.Model <> 0 Then
      Call _auditor_data.SetField(0, Me.Port.ToString(), GLB_NLS_GUI_PLAYER_TRACKING.GetString(5314) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5315)) ''Configración serial.puerto %1
    Else
      Call _auditor_data.SetField(0, AUDIT_NONE_STRING, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5314) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5315)) ''Configración serial.puerto %1
    End If

    If Me.Model <> 0 Then
      Call _auditor_data.SetField(0, CommonScannerFunctions.ReturnStringBaudrate(Me.Baudrate), GLB_NLS_GUI_PLAYER_TRACKING.GetString(5314) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5316)) 'Configración serial.Baudrate %1
    Else
      Call _auditor_data.SetField(0, AUDIT_NONE_STRING, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5314) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5316)) 'Configración serial.Baudrate %1
    End If

    If Me.Model <> 0 Then
      Select Case Me.Stopbits
        Case One '1
          Call _auditor_data.SetField(0, "1", GLB_NLS_GUI_PLAYER_TRACKING.GetString(5314) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5317)) 'Configración serial.Stopbits %1
        Case OnePointFive '1.5
          Call _auditor_data.SetField(0, "1.5", GLB_NLS_GUI_PLAYER_TRACKING.GetString(5314) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5317))
        Case Two '2
          Call _auditor_data.SetField(0, "2", GLB_NLS_GUI_PLAYER_TRACKING.GetString(5314) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5317))
        Case Else
          Call _auditor_data.SetField(0, AUDIT_NONE_STRING, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5314) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5317))
      End Select
    Else
      Call _auditor_data.SetField(0, AUDIT_NONE_STRING, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5314) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5317))
    End If
    
    If Me.Model <> 0 Then
      Call _auditor_data.SetField(0, CommonScannerFunctions.ReturnStringDatabits(Me.Databits), GLB_NLS_GUI_PLAYER_TRACKING.GetString(5314) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5318))
    Else
      Call _auditor_data.SetField(0, AUDIT_NONE_STRING, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5314) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5318))
    End If

    If Me.Model <> 0 Then
      Select Case Me.Parity
        Case 2 'Even
          Call _auditor_data.SetField(0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5329), GLB_NLS_GUI_PLAYER_TRACKING.GetString(5314) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5319)) 'Configración serial.Parity %1
        Case 1 'Odd
          Call _auditor_data.SetField(0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5330), GLB_NLS_GUI_PLAYER_TRACKING.GetString(5314) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5319))
        Case 0 'None
          Call _auditor_data.SetField(0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5331), GLB_NLS_GUI_PLAYER_TRACKING.GetString(5314) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5319))
        Case 3 'Mark
          Call _auditor_data.SetField(0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5332), GLB_NLS_GUI_PLAYER_TRACKING.GetString(5314) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5319))
        Case 4 'Space
          Call _auditor_data.SetField(0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5333), GLB_NLS_GUI_PLAYER_TRACKING.GetString(5314) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5319))
        Case Else
          Call _auditor_data.SetField(0, AUDIT_NONE_STRING, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5314) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5319))
      End Select
    Else
      Call _auditor_data.SetField(0, AUDIT_NONE_STRING, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5314) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5319))
    End If


    'control first input in form for new machine
    If Not Me.Denomination Is Nothing Then
      If Me.Denomination.Rows.Count > 0 Then

        ' mark each row according to their status
        For Each _dr As DataRow In Me.Denomination.Rows
          Select Case _dr.RowState
            Case DataRowState.Added
              _currency_exchange = CurrencyExchangeProperties.GetProperties(_dr("ISO_CODE"))
              _auditor_data.SetField(0, GLB_NLS_GUI_COMMONMISC.GetString(10), GLB_NLS_GUI_PLAYER_TRACKING.GetString(5322) & " : " & _dr("CurrencyId") & " - " & _currency_exchange.FormatCurrency(_dr("Denomination")) & " - " & _dr("ISO_CODE") & " - " & GUI_FormatNumber(_dr("Limit"), 0), CLASS_AUDITOR_DATA.ENUM_FIELD_TYPE.FIELD_ALWAYS_VISIBLE)

            Case DataRowState.Deleted
              _currency_exchange = CurrencyExchangeProperties.GetProperties(_dr("ISO_CODE", DataRowVersion.Original))
              _auditor_data.SetField(0, GLB_NLS_GUI_COMMONMISC.GetString(9), GLB_NLS_GUI_PLAYER_TRACKING.GetString(5322) & " : " & _dr("CurrencyId", DataRowVersion.Original) & " - " & _currency_exchange.FormatCurrency(_dr("Denomination", DataRowVersion.Original)) & " - " & _dr("ISO_CODE", DataRowVersion.Original) & " - " & GUI_FormatNumber(_dr("Limit", DataRowVersion.Original), 0), CLASS_AUDITOR_DATA.ENUM_FIELD_TYPE.FIELD_ALWAYS_VISIBLE)

            Case DataRowState.Modified
              _currency_exchange = CurrencyExchangeProperties.GetProperties(_dr("ISO_CODE"))
              _auditor_data.SetField(0, _dr("CurrencyId") & " - " & _currency_exchange.FormatCurrency(_dr("Denomination")) & " - " & _dr("ISO_CODE") & " - " & GUI_FormatNumber(_dr("Limit"), 0) & _
                                        " (" & GLB_NLS_GUI_CONFIGURATION.GetString(83) & " " & _dr("CurrencyId", DataRowVersion.Original) & " - " & _currency_exchange.FormatCurrency(_dr("Denomination", DataRowVersion.Original)) & " - " & _dr("ISO_CODE", DataRowVersion.Original) & " - " & GUI_FormatNumber(_dr("Limit", DataRowVersion.Original), 0) & ")" _
                                        , GLB_NLS_GUI_PLAYER_TRACKING.GetString(5322), CLASS_AUDITOR_DATA.ENUM_FIELD_TYPE.FIELD_ALWAYS_VISIBLE)
          End Select
        Next
      End If
    End If

    Return _auditor_data
  End Function           'AuditorData

  ' PURPOSE: Reads from the database into the given object
  '
  ' PARAMS:
  '   - INPUT:
  '     - ObjectId: An object, it must be 'casted' to the desired KEY.
  '
  '   - OUTPUT: 
  '           - None
  ' RETURNS:
  '     - ENUM_STATUS
  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _rc As Integer
    Dim _dt As DataTable = Nothing

    ' Read counter configuration
    _rc = ReadCounter(SqlCtx, _dt)

    Select Case _rc
      Case ENUM_DB_RC.DB_RC_OK

        CommonScannerFunctions.GetApplicationId(Me.ApplicationId)
        Me.Model = _dt.Rows(0).Item("NC_MODEL")
        If Not String.IsNullOrEmpty(_dt.Rows(0).Item("NC_CURRENCY_ISO_CODE").ToString) Then
          Me.Currency = _dt.Rows(0).Item("NC_CURRENCY_ISO_CODE")
        Else
          Me.Currency = ""
        End If
        Me.CommunicationType = _dt.Rows(0).Item("NC_COMMUNICATION_TYPE")
        Me.Port = _dt.Rows(0).Item("NC_PORT")
        Me.Baudrate = _dt.Rows(0).Item("NC_BAUD_RATE")
        Me.Stopbits = _dt.Rows(0).Item("NC_STOP_BITS")
        Me.Databits = _dt.Rows(0).Item("NC_DATA_BITS")
        Me.Parity = _dt.Rows(0).Item("NC_PARITY")
        If Not String.IsNullOrEmpty(_dt.Rows(0).Item("NC_DENOMINATIONS").ToString) Then
          Me.Denomination = NoteScanner.CommonScannerFunctions.GetDataSetCurrenciesFromXml(_dt.Rows(0).Item("NC_DENOMINATIONS")).Tables(0)
          Me.Denomination.AcceptChanges()
        End If

        Return ENUM_STATUS.STATUS_OK

      Case ENUM_DB_RC.DB_RC_ERROR_NOT_FOUND
        Me.Denomination = InitDenominationDataTable()
        Me.Currency = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode")
        Me.Model = NoteCounterModel.None
        Me.CommunicationType = NoteCounterCommunication.Serial
        Me.Port = NoteCounterPort.COM1
        Me.Baudrate = NoteCounterBaudrate.BAUDRATE_9600
        Me.Stopbits = 1
        Me.Databits = NoteCounterDatabits.DATABITS_8
        Me.Parity = 2

        Return ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_ALREADY_EXISTS 'control first input in form for new machine
        'create a new row in DataBase for my machine

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select
  End Function               'DB_Read

  ' PURPOSE: Validate correct services to read database
  '
  ' PARAMS:
  '   - INPUT:
  '     - data_table: Applicated actual data table
  '
  '   - OUTPUT: 
  '           - None
  ' RETURNS:
  '     - ENUM_CONFIGURATION_RC
  Private Function ReadCounter(ByVal Context As Integer, ByRef data_table As DataTable) As Integer

    CommonScannerFunctions.GetApplicationId(Me.ApplicationId)
    data_table = CommonScannerFunctions.GetScannerInfoDataTable(Me.ApplicationId)

    If IsNothing(data_table) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    If data_table.Rows.Count = 0 Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
    End If

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
  End Function                    'ReadCounter

  ' PURPOSE: Updates the given object to the database.
  '
  ' PARAMS:
  '   - INPUT:  
  '           - None
  '   - OUTPUT:  
  '           - None
  ' RETURNS:
  '     - ENUM_STATUS
  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _rc As Integer

    ' Update counter configration
    _rc = InsertOrUpdateCounter(m_counter, SqlCtx)

    Select Case _rc
      Case ENUM_DB_RC.DB_RC_OK
        Return ENUM_STATUS.STATUS_OK

      Case ENUM_DB_RC.DB_RC_ERROR_NOT_FOUND
        Return CLASS_BASE.ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select
  End Function             'DB_Update

  ' PURPOSE: Update & insert in database the object
  '
  ' PARAMS:
  '   - INPUT:  
  '           - TYPE_COUNTER
  '   - OUTPUT:  
  '           - None
  ' RETURNS:
  '     - ENUM_CONFIGURATION_RC
  Private Function InsertOrUpdateCounter(ByVal pCounter As TYPE_COUNTER, _
                                 ByVal Context As Integer) As Integer

    Dim _SqlTrans As System.Data.SqlClient.SqlTransaction = Nothing
    Dim _sb As StringBuilder

    Try
      Using _db_trx As New DB_TRX()

        _sb = New StringBuilder()

        _sb.AppendLine(" IF NOT EXISTS ( SELECT * FROM NOTE_COUNTERS WHERE NC_APPLICATION_ID = @pApplication )")
        _sb.AppendLine("INSERT INTO   NOTE_COUNTERS ")
        _sb.AppendLine("            ( NC_APPLICATION_ID ")
        _sb.AppendLine("            , NC_MODEL  ")
        _sb.AppendLine("            , NC_CURRENCY_ISO_CODE ")
        _sb.AppendLine("            , NC_COMMUNICATION_TYPE ")
        _sb.AppendLine("            , NC_PORT ")
        _sb.AppendLine("            , NC_BAUD_RATE ")
        _sb.AppendLine("            , NC_STOP_BITS ")
        _sb.AppendLine("            , NC_DATA_BITS ")
        _sb.AppendLine("            , NC_PARITY ")
        _sb.AppendLine("            , NC_DENOMINATIONS) ")
        _sb.AppendLine("     VALUES   ")
        _sb.AppendLine("            ( @pApplication ")
        _sb.AppendLine("            , @pModel ")
        _sb.AppendLine("            , @pCurrency ")
        _sb.AppendLine("            , @pCommunication ")
        _sb.AppendLine("            , @pPort ")
        _sb.AppendLine("            , @pBaudrate ")
        _sb.AppendLine("            , @pStopbits ")
        _sb.AppendLine("            , @pDatabits ")
        _sb.AppendLine("            , @pParity ")
        _sb.AppendLine("            , @pDenomination) ")
        _sb.AppendLine(" ELSE                          ")
        _sb.AppendLine(" UPDATE   NOTE_COUNTERS                      ")
        _sb.AppendLine("    SET   NC_MODEL              =  @pModel ")
        _sb.AppendLine("        , NC_CURRENCY_ISO_CODE  =  @pCurrency ")
        _sb.AppendLine("        , NC_COMMUNICATION_TYPE =  @pCommunication ")
        _sb.AppendLine("        , NC_PORT               =  @pPort ")
        _sb.AppendLine("        , NC_BAUD_RATE          =  @pBaudrate ")
        _sb.AppendLine("        , NC_STOP_BITS          =  @pStopbits ")
        _sb.AppendLine("        , NC_DATA_BITS          =  @pDatabits ")
        _sb.AppendLine("        , NC_PARITY             =  @pParity ")
        _sb.AppendLine("        , NC_DENOMINATIONS      =  @pDenomination ")
        _sb.AppendLine("  WHERE   NC_APPLICATION_ID     =  @pApplication ")

        Using _cmd As SqlCommand = New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          _cmd.Parameters.Add("@pModel", SqlDbType.Int).Value = pCounter.model
          _cmd.Parameters.Add("@pCurrency", SqlDbType.NVarChar).Value = pCounter.currency
          _cmd.Parameters.Add("@pCommunication", SqlDbType.Int).Value = pCounter.communication_type
          _cmd.Parameters.Add("@pPort", SqlDbType.Int).Value = pCounter.port
          _cmd.Parameters.Add("@pBaudrate", SqlDbType.Int).Value = pCounter.baudrate
          _cmd.Parameters.Add("@pStopbits", SqlDbType.Int).Value = pCounter.stopbits
          _cmd.Parameters.Add("@pDatabits", SqlDbType.Int).Value = pCounter.databits
          _cmd.Parameters.Add("@pParity", SqlDbType.Int).Value = pCounter.parity
          If pCounter.denomination Is Nothing Or pCounter.model = NoteCounterModel.None Then
            _cmd.Parameters.Add("@pDenomination", SqlDbType.Xml).Value = "<CurrenciesList/>"
          Else
            _cmd.Parameters.Add("@pDenomination", SqlDbType.Xml).Value = pCounter.denomination.DataSet.GetXml()
          End If
          _cmd.Parameters.Add("@pApplication", SqlDbType.Int).Value = Me.ApplicationId

          If _cmd.ExecuteNonQuery() = 0 Then
            Return ENUM_STATUS.STATUS_ERROR
          End If

        End Using

        _db_trx.Commit()
      End Using

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

    Catch _ex As Exception
      Log.Exception(_ex)

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR
    End Try
  End Function                  'UpdateCounter

  ' PURPOSE: Can't delete the given object.
  '
  ' PARAMS:
  '   - INPUT:   
  '           - None
  '   - OUTPUT:   
  '           - None
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
  End Function             'DB_Delete

  ' PURPOSE: Can't insert the given object.
  '
  ' PARAMS:
  '   - INPUT: 
  '           - None
  '   - OUTPUT: 
  '           - None
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
  End Function             'DB_Insert

  ' PURPOSE: Returns a duplicate of the given object.
  '
  ' PARAMS:
  '   - INPUT: 
  '           - None
  '   - OUTPUT: 
  '           - None
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE
    Dim _temp_counter As CLASS_COUNTER_CONFIGURATION

    _temp_counter = New CLASS_COUNTER_CONFIGURATION()

    _temp_counter.ApplicationId = Me.ApplicationId
    _temp_counter.Model = Me.Model
    _temp_counter.Currency = Me.Currency
    _temp_counter.CommunicationType = Me.CommunicationType
    _temp_counter.Port = Me.Port
    _temp_counter.Baudrate = Me.Baudrate
    _temp_counter.Stopbits = Me.Stopbits
    _temp_counter.Databits = Me.Databits
    _temp_counter.Parity = Me.Parity
    If Me.Denomination IsNot Nothing Then
      _temp_counter.Denomination = Me.Denomination.Copy()
    End If

    Return _temp_counter
  End Function             'Duplicate

#End Region                                             'Overrrides

#Region "Private Functions"
  Private Function InitDenominationDataTable() As DataTable
    Dim _table As DataTable
    Dim _ds As DataSet

    _ds = New DataSet("CurrenciesList")
    _table = _ds.Tables.Add("Currencies")
    _table.Columns.Add("ISO_CODE", Type.GetType("System.String"))
    _table.Columns.Add("CurrencyId", Type.GetType("System.Char"))
    _table.Columns.Add("Denomination", Type.GetType("System.Decimal"))
    _table.Columns.Add("Limit", Type.GetType("System.Int32"))

    Return _table
  End Function
#End Region
End Class
