'-------------------------------------------------------------------
' Copyright � 2015 Win Systems Ltd.
'-------------------------------------------------------------------
'
'   MODULE NAME : frm_terminals_block
'   DESCRIPTION : Block specific terminals
'        AUTHOR : Samuel Gonz�lez
' CREATION DATE : 07-JUL-2015
'
'
' REVISION HISTORY :
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 07-JUL-2015  SGB    Initial version. Backlog Item 2665
'--------------------------------------------------------------------
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports System.Text
Imports WSI.Common

Public Class CLASS_TERMINALS_BLOCK
  Inherits CLASS_BASE

#Region " Members "

  Dim m_disable_machine As Boolean
  Dim m_start_trading As Integer
  Dim m_end_trading As Integer
  Dim m_start_trading_after_befoure As Integer
  Dim m_end_trading_after_befoure As Integer
  Dim m_terminals_always_block As New DataTable
  Dim m_terminals_always_unblock As New DataTable
  Dim m_selected_terminals As DataTable
  Dim m_terminal_status_bitmasks As DataTable
  Dim m_block_automatic As Boolean
  Dim m_block_exceptions As Boolean

#End Region ' Members

#Region "Constants"

  Public Const AUDIT_NONE_STRING As String = "---"

#End Region

#Region "Properties"

  Public Property DisableMachine() As Boolean

    Get
      Return m_disable_machine
    End Get

    Set(ByVal Value As Boolean)
      m_disable_machine = Value
    End Set
  End Property ' DisableMachine

  Public Property StartTrading() As Integer
    Get
      Return m_start_trading
    End Get
    Set(ByVal value As Integer)
      m_start_trading = value
    End Set
  End Property 'StartTrading

  Public Property EndTrading() As Integer
    Get
      Return m_end_trading
    End Get

    Set(ByVal Value As Integer)
      m_end_trading = Value
    End Set
  End Property ' EndTrading

  Public Property StartTradingAfterBefoure() As Integer
    Get
      Return m_start_trading_after_befoure
    End Get
    Set(ByVal value As Integer)
      m_start_trading_after_befoure = value
    End Set
  End Property 'StartTrading

  Public Property EndTradingAfterBefoure() As Integer
    Get
      Return m_end_trading_after_befoure
    End Get

    Set(ByVal Value As Integer)
      m_end_trading_after_befoure = Value
    End Set
  End Property ' EndTrading

  Public Property TerminalsAlwaysBlock() As DataTable
    Get
      Return m_terminals_always_block
    End Get

    Set(ByVal Value As DataTable)
      m_terminals_always_block = Value
    End Set
  End Property 'TerminalsEverBlock

  Public Property TerminalsAlwaysUnblock() As DataTable
    Get
      Return m_terminals_always_unblock
    End Get

    Set(ByVal Value As DataTable)
      m_terminals_always_unblock = Value
    End Set
  End Property ' TerminalsEverUnblock

  Public Property TerminalsSelected() As DataTable
    Get
      Return m_selected_terminals
    End Get

    Set(ByVal Value As DataTable)
      m_selected_terminals = Value
    End Set
  End Property ' TerminalsSelected

  Public Property BlockAutomatic() As Boolean

    Get
      Return m_block_automatic
    End Get

    Set(ByVal Value As Boolean)
      m_block_automatic = Value
    End Set
  End Property ' BlockAutomatic

  Public Property BlockExceptions() As Boolean

    Get
      Return m_block_exceptions
    End Get

    Set(ByVal Value As Boolean)
      m_block_exceptions = Value
    End Set
  End Property ' BlockExceptions

#End Region ' Properties

#Region " Overrides functions "

  ' PURPOSE: Returns the related auditor data for the current object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim auditor_data As CLASS_AUDITOR_DATA

    auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_CASHIER_CONFIGURATION)

    'auditor_data.IsAuditable = False
    auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(6542), "")

    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6544), _
                            IIf(Me.DisableMachine, GLB_NLS_GUI_ALARMS.GetString(318), _
                                                   GLB_NLS_GUI_ALARMS.GetString(319))) ' disable machine

    Call auditor_data.SetField(0, GUI_ParseNumber(Me.StartTrading) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6547), _
                                  GLB_NLS_GUI_PLAYER_TRACKING.GetString(6545) & " " & Me.EndTrading.ToString & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6547) & _
                                  " " & IIf(Me.StartTradingAfterBefoure = 0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6548).ToLower, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6549).ToLower) & _
                                  " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6550)) ' (x minutos), Bloquear x minutos antes/despu�s del fin de horario

    Call auditor_data.SetField(0, GUI_ParseNumber(Me.EndTrading) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6547), _
                                  GLB_NLS_GUI_PLAYER_TRACKING.GetString(6546) & " " & Me.EndTrading.ToString & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6547) & _
                                  " " & IIf(Me.StartTradingAfterBefoure = 0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6548).ToLower, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6549).ToLower) & _
                                  " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6551)) ' (x minutos), Desbloquear x minutos antes/despu�s del inicio de horario

    Call auditor_data.SetField(0, IIf(Me.StartTradingAfterBefoure = 0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6548), GLB_NLS_GUI_PLAYER_TRACKING.GetString(6549)), _
                                  GLB_NLS_GUI_PLAYER_TRACKING.GetString(6545) & " " & Me.EndTrading.ToString & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6547) & _
                                  " " & IIf(Me.StartTradingAfterBefoure = 0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6548).ToLower, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6549).ToLower) & _
                                  " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6550)) ' (antes/despu�s), Bloquear x minutos antes/despu�s del fin de horario

    Call auditor_data.SetField(0, IIf(Me.EndTradingAfterBefoure = 0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6548), GLB_NLS_GUI_PLAYER_TRACKING.GetString(6549)), _
                                  GLB_NLS_GUI_PLAYER_TRACKING.GetString(6546) & " " & Me.EndTrading.ToString & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6547) & _
                                  " " & IIf(Me.EndTradingAfterBefoure = 0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6548).ToLower, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6549).ToLower) & _
                                  " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6551)) ' (antes/despu�s), Desbloquear x minutos antes/despu�s del inicio de horario

    Call auditor_data.SetField(0, AuditStringList(Me.TerminalsAlwaysBlock), GLB_NLS_GUI_PLAYER_TRACKING.GetString(6553).Substring(0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6553).ToString.Length - 2))

    Call auditor_data.SetField(0, AuditStringList(Me.TerminalsAlwaysUnblock), GLB_NLS_GUI_PLAYER_TRACKING.GetString(6554).Substring(0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6554).ToString.Length - 2))

    Return auditor_data

  End Function ' AuditorData

  ' PURPOSE: Delete into database into the given object
  '                                              
  ' PARAMS:
  '   - INPUT: 
  '   - OUTPUT:
  '
  ' RETURNS:
  '
  ' NOTES:
  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_NOT_SUPPORTED
  End Function ' DB_Delete

  ' PURPOSE: Insert into database into the given object
  '                                              
  ' PARAMS:
  '   - INPUT: 
  '   - OUTPUT:
  '
  ' RETURNS:
  '
  ' NOTES:
  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_NOT_SUPPORTED
  End Function ' DB_Insert

  ' PURPOSE: Reads from the database into the given object
  '
  ' PARAMS:
  '   - INPUT:
  '     - ObjectId: An object, it must be 'casted' to the desired KEY.
  '
  '   - OUTPUT: ENUM_STATUS
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim rc As Integer

    Using _db_trx As New DB_TRX()

      rc = ReadConfig(_db_trx.SqlTransaction)

      Select Case rc
        Case ENUM_DB_RC.DB_RC_OK
          Return ENUM_STATUS.STATUS_OK

        Case ENUM_DB_RC.DB_RC_ERROR_NOT_FOUND
          Return CLASS_BASE.ENUM_STATUS.STATUS_NOT_FOUND

        Case Else
          Return ENUM_STATUS.STATUS_ERROR

      End Select

    End Using ' _db_trx

  End Function ' DB_Read

  ' PURPOSE: Updates the given object to the database.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As ENUM_STATUS

    Using _db_trx As New DB_TRX()

      _rc = UpdateConfig(_db_trx.SqlTransaction)

      If _rc <> ENUM_STATUS.STATUS_OK Then
        Call _db_trx.Rollback()
      Else
        _rc = IIf(_db_trx.Commit() = True, ENUM_STATUS.STATUS_OK, ENUM_STATUS.STATUS_ERROR)
      End If

    End Using ' _db_trx

    Return _rc

  End Function ' DB_Update

  ' PURPOSE: Returns a duplicate of the given object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim _temp_terminals_block As CLASS_TERMINALS_BLOCK

    _temp_terminals_block = New CLASS_TERMINALS_BLOCK()

    _temp_terminals_block.DisableMachine = Me.DisableMachine
    _temp_terminals_block.StartTrading = Me.StartTrading
    _temp_terminals_block.EndTrading = Me.EndTrading
    _temp_terminals_block.StartTradingAfterBefoure = Me.StartTradingAfterBefoure
    _temp_terminals_block.EndTradingAfterBefoure = Me.EndTradingAfterBefoure

    'TODO SAM New DataTable
    _temp_terminals_block.m_terminals_always_block = Me.m_terminals_always_block
    _temp_terminals_block.m_terminals_always_unblock = Me.m_terminals_always_unblock

    Return _temp_terminals_block

  End Function ' Duplicate

#End Region 'Overrides 

#Region "Private Functions"

  ' PURPOSE: Update general params of OperationsSchedule
  '                                              
  ' PARAMS:
  '   - INPUT: 
  '         - SqlTransaction
  '   - OUTPUT:
  '
  ' RETURNS:
  '
  ' NOTES:
  Private Function UpdateConfig(ByVal Trx As SqlTransaction) As Integer

    Try

      If Me.BlockAutomatic Then
        If Not DB_GeneralParam_Update("OperationsSchedule", "DisableMachine", (IIf(Me.DisableMachine, "1", "0")), Trx) Then

          Return ENUM_DB_RC.DB_RC_ERROR_DB
        End If

        If (Me.DisableMachine) Then
          If Not DB_GeneralParam_Update("OperationsSchedule", "DisableMachine.EndTrading", IIf(Me.EndTradingAfterBefoure = 0, "-", "") & Me.EndTrading.ToString(), Trx) Then

            Return ENUM_DB_RC.DB_RC_ERROR_DB
          End If

          If Not DB_GeneralParam_Update("OperationsSchedule", "DisableMachine.StartTrading", IIf(Me.StartTradingAfterBefoure = 0, "-", "") & Me.StartTrading.ToString(), Trx) Then

            Return ENUM_DB_RC.DB_RC_ERROR_DB
          End If
        End If
      End If

      If Me.BlockExceptions Then

        Call ReadAllTerminalsToMandatory()

        If Not UpdateBlockTerminals(Trx) Then

          Return ENUM_DB_RC.DB_RC_ERROR_DB
        End If
      End If

      Return ENUM_DB_RC.DB_RC_OK

    Catch _ex As Exception

      Return ENUM_STATUS.STATUS_ERROR

    End Try

  End Function ' UpdateConfig

  ' PURPOSE: Update blockTerminals and using "batch update"
  '                                              
  ' PARAMS:
  '   - INPUT: 
  '         - SqlTransaction
  '   - OUTPUT:
  '
  ' RETURNS:
  '
  ' NOTES:
  Private Function UpdateBlockTerminals(ByVal Trx As SqlTransaction) As Boolean

    Dim _sb_insert As StringBuilder
    Dim _sb_update As StringBuilder
    Dim _sql_cmd As SqlCommand
    Dim _rows As DataRow()
    Dim _dt_changes_terminals As DataTable
    Dim _num_rows_updated As Integer
    Dim _dr_aux As DataRow()
    Dim _row As DataRow

    Dim _firstWhere As Boolean = True
    Dim _dt As DataTable

    _sb_insert = New StringBuilder
    _sb_update = New StringBuilder

    _dt = New DataTable()

    For Each _dr As DataRow In m_terminal_status_bitmasks.Rows
      _dr("TS_MACHINE_FLAGS") = TerminalStatusFlags.UpdateBitmask(_dr("TS_MACHINE_FLAGS"), TerminalStatusFlags.MACHINE_FLAGS.ALWAYS_BLOCKED, False)
      _dr("TS_MACHINE_FLAGS") = TerminalStatusFlags.UpdateBitmask(_dr("TS_MACHINE_FLAGS"), TerminalStatusFlags.MACHINE_FLAGS.ALWAYS_UNBLOCKED, False)
    Next

    For Each _dr As DataRow In m_selected_terminals.Rows
      _dr_aux = m_terminal_status_bitmasks.Select("TS_TERMINAL_ID = " & _dr("TS_TERMINAL_ID"))
      If _dr_aux.Length > 0 Then
        If Not TerminalStatusFlags.IsFlagActived(_dr_aux(0)("TS_MACHINE_FLAGS"), _dr("TS_MACHINE_FLAGS")) Then
          _dr_aux(0)("TS_MACHINE_FLAGS") = TerminalStatusFlags.UpdateBitmask(_dr_aux(0)("TS_MACHINE_FLAGS"), _dr("TS_MACHINE_FLAGS"), True)
        End If
      Else

        _row = m_terminal_status_bitmasks.NewRow
        _row("TS_TERMINAL_ID") = _dr("TS_TERMINAL_ID")
        _row("TS_MACHINE_FLAGS") = _dr("TS_MACHINE_FLAGS")
        m_terminal_status_bitmasks.Rows.Add(_row)
      End If
    Next

    _dt_changes_terminals = m_terminal_status_bitmasks.GetChanges()

    If _dt_changes_terminals Is Nothing Then
      Return False
    End If

    _rows = m_terminal_status_bitmasks.Select("", "", DataViewRowState.ModifiedCurrent Or DataViewRowState.Added)
    Call TrimDataValues(_rows)

    Try

      If _rows.Length > 0 Then
        _sb_insert.AppendLine(" INSERT INTO   TERMINAL_STATUS   ")
        _sb_insert.AppendLine("             ( TS_TERMINAL_ID    ")
        _sb_insert.AppendLine("             , TS_MACHINE_FLAGS  ")
        _sb_insert.AppendLine("             )                   ")
        _sb_insert.AppendLine("      VALUES ( @pTerminalId      ")
        _sb_insert.AppendLine("             , @pMachineFlags    ")
        _sb_insert.AppendLine("             )                   ")

        _sb_update.AppendLine(" UPDATE   TERMINAL_STATUS                   ")
        _sb_update.AppendLine("    SET   TS_MACHINE_FLAGS = @pMachineFlags ")
        _sb_update.AppendLine("  WHERE   TS_TERMINAL_ID = @pTerminalId     ")

        Using _sql_adap As SqlDataAdapter = New SqlDataAdapter()
          With _sql_adap
            .SelectCommand = Nothing
            .DeleteCommand = Nothing

            _sql_cmd = New SqlCommand(_sb_insert.ToString, Trx.Connection, Trx)
            _sql_cmd.Parameters.Add("@pMachineFlags", SqlDbType.Int).SourceColumn = "TS_MACHINE_FLAGS"
            _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "TS_TERMINAL_ID"
            _sql_cmd.UpdatedRowSource = UpdateRowSource.OutputParameters
            .InsertCommand = _sql_cmd

            _sql_cmd = New SqlCommand(_sb_update.ToString, Trx.Connection, Trx)
            _sql_cmd.Parameters.Add("@pMachineFlags", SqlDbType.Int).SourceColumn = "TS_MACHINE_FLAGS"
            _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "TS_TERMINAL_ID"
            _sql_cmd.UpdatedRowSource = UpdateRowSource.OutputParameters
            .UpdateCommand = _sql_cmd

            .UpdateBatchSize = 500

            _num_rows_updated = .Update(_rows)

          End With
        End Using
      End If

      Return True

    Catch _ex As Exception

      Log.Exception(_ex)

    End Try

    Return False

  End Function 'UpdateBlockTerminals

  ' PURPOSE: It calls the TITO_Configuration_DbRead and checks if the database result is ok
  '                                              
  ' PARAMS:
  '   - INPUT: 
  '         - SqlTransaction
  '   - OUTPUT:
  '
  ' RETURNS:
  '
  ' NOTES:
  Private Function ReadConfig(ByVal Trx As SqlTransaction) As Integer

    'Dim _dt_terminals_count As DataTable
    Dim _general_param As GeneralParam.Dictionary
    Dim _gp_endTrading As Int32
    Dim _gp_startTrading As Int32

    Try
      _general_param = GeneralParam.Dictionary.Create()

      'Configuration terminals block
      _gp_endTrading = _general_param.GetInt32("OperationsSchedule", "DisableMachine.EndTrading")
      _gp_startTrading = _general_param.GetInt32("OperationsSchedule", "DisableMachine.StartTrading")

      Me.DisableMachine = _general_param.GetBoolean("OperationsSchedule", "DisableMachine", False)
      Me.StartTrading = Math.Abs(_gp_startTrading)
      Me.EndTrading = Math.Abs(_gp_endTrading)
      Me.StartTradingAfterBefoure = IIf(_gp_startTrading < 0, 0, 1)
      Me.EndTradingAfterBefoure = IIf(_gp_endTrading < 0, 0, 1)

      Me.m_terminals_always_block = ReadTerminalSelectedBBDD(TerminalStatusFlags.MACHINE_FLAGS.ALWAYS_BLOCKED)
      Me.m_terminals_always_unblock = ReadTerminalSelectedBBDD(TerminalStatusFlags.MACHINE_FLAGS.ALWAYS_UNBLOCKED)

      Call ReadAllTerminalsToMandatory()

    Catch _ex As Exception
      Return ENUM_STATUS.STATUS_ERROR
    End Try

    Return ENUM_STATUS.STATUS_OK

  End Function ' ReadConfig

  ' PURPOSE: 
  '                                              
  ' PARAMS:
  '   - INPUT: 
  '         - Machine flag
  '   - OUTPUT:
  '
  ' RETURNS:
  '         - DataTable terminals selected
  '
  ' NOTES:
  Private Function ReadTerminalSelectedBBDD(ByVal MachineFlag As TerminalStatusFlags.MACHINE_FLAGS) As DataTable

    Dim _table As DataTable
    Dim _sb As StringBuilder

    _table = New DataTable()
    _sb = New StringBuilder

    _sb.AppendLine("    SELECT   TS_TERMINAL_ID                                                                    ")
    _sb.AppendLine("           , TS_MACHINE_FLAGS                                                                  ")
    _sb.AppendLine("           , T2.TE_NAME                                                                        ")
    _sb.AppendLine("      FROM   TERMINAL_STATUS                                                                   ")
    _sb.AppendLine("INNER JOIN   TERMINALS T1 ON TS_TERMINAL_ID  = T1.TE_TERMINAL_ID                               ")
    _sb.AppendLine("INNER JOIN   TERMINALS T2 ON T1.TE_MASTER_ID = T2.TE_TERMINAL_ID                               ")
    _sb.AppendLine("       AND   T1.TE_TERMINAL_TYPE IN (" & WSI.Common.Misc.GamingTerminalTypeListToString() & ") ")
    _sb.AppendLine("       AND   T1.TE_STATUS = @pTerminalStatus                                                   ")
    _sb.AppendLine("       AND   T1.TE_TYPE   = 1                                                                  ")
    _sb.AppendLine("     WHERE   TS_MACHINE_FLAGS = TS_MACHINE_FLAGS | @pMachineFlags                              ")

    Try

      Using _db_trx As DB_TRX = New DB_TRX()
        Using _sql_cmd As SqlCommand = New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _sql_cmd.Parameters.Add("@pMachineFlags", SqlDbType.Int).Value = MachineFlag
          _sql_cmd.Parameters.Add("@pTerminalStatus", SqlDbType.Int).Value = TerminalStatus.ACTIVE

          Using _sql_da As New SqlDataAdapter(_sql_cmd)

            _db_trx.Fill(_sql_da, _table)

          End Using
        End Using
      End Using

    Catch _ex As Exception
      Log.Exception(_ex)

    End Try

    Return _table

  End Function 'ReadTerminalSelectedBBDD

  ' PURPOSE: 
  '                                              
  ' PARAMS:
  '   - INPUT: 
  '   - OUTPUT:
  '
  ' RETURNS:
  '
  ' NOTES:
  Private Sub ReadAllTerminalsToMandatory()

    Dim _sb As StringBuilder

    m_terminal_status_bitmasks = New DataTable()
    _sb = New StringBuilder

    _sb.AppendLine("    SELECT   TS_TERMINAL_ID                                                                 ")
    _sb.AppendLine("           , TS_MACHINE_FLAGS                                                               ")
    _sb.AppendLine("      FROM   TERMINAL_STATUS                                                                ")
    _sb.AppendLine("INNER JOIN   TERMINALS ON TS_TERMINAL_ID = TE_TERMINAL_ID                                   ")
    _sb.AppendLine("       AND   TE_TERMINAL_TYPE IN (" & WSI.Common.Misc.GamingTerminalTypeListToString() & ") ")
    _sb.AppendLine("       AND   TE_STATUS = @pTerminalStatus                                                   ")
    _sb.AppendLine("       AND   TE_TYPE   = 1                                                                  ")

    Try

      Using _db_trx As DB_TRX = New DB_TRX()
        Using _sql_cmd As SqlCommand = New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _sql_cmd.Parameters.Add("@pTerminalStatus", SqlDbType.Int).Value = TerminalStatus.ACTIVE

          Using _sql_da As New SqlDataAdapter(_sql_cmd)

            _db_trx.Fill(_sql_da, m_terminal_status_bitmasks)

          End Using
        End Using
      End Using

    Catch _ex As Exception
      Log.Exception(_ex)

    End Try

  End Sub 'ReadBlockTerminal

  ' PURPOSE: 
  '                                              
  ' PARAMS:
  '   - INPUT: 
  '         - DataTable to audit
  '   - OUTPUT:
  '
  ' RETURNS:
  '
  '         - String
  ' NOTES:
  Private Function AuditStringList(Dt As DataTable) As String

    Dim _list As StringBuilder

    _list = New StringBuilder()

    For Each _dr As DataRow In Dt.Rows
      If _list.Length > 0 Then
        _list.Append(", ")
      End If
      _list.Append(_dr(2).ToString())
    Next

    If _list.Length = 0 Then
      Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(2711)
    Else
      Return _list.ToString()
    End If

  End Function ' AuditStringList

#End Region ' Private Functions

#Region " Shared Functions "

  ' PURPOSE: 'Read counts necesari to form
  '                                              
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS: DataTable
  '
  ' NOTES:
  Shared Function ReadCountStatusTerminals() As DataTable
    Dim _table As DataTable
    Dim _sb As StringBuilder

    _table = New DataTable()
    _sb = New StringBuilder()

    _sb.AppendLine("    SELECT   SUM (CASE WHEN TS_MACHINE_FLAGS & @pTerminalBloqued > 0                                                   ")
    _sb.AppendLine("                         OR TS_MACHINE_FLAGS & @pTerminalAutomatically > 0                                             ")
    _sb.AppendLine("                         OR TS_MACHINE_FLAGS & @pTerminalManual > 0                                                    ")
    _sb.AppendLine("                        AND NOT TS_MACHINE_FLAGS & @pTerminalUnbloqued > 0 THEN 1 ELSE 0 END) AS BlockTotal            ")
    _sb.AppendLine("           , SUM (CASE WHEN TS_MACHINE_FLAGS & @pTerminalAutomatically > 0 THEN 1 ELSE 0 END) AS BlockAutomatic        ")
    _sb.AppendLine("           , SUM (CASE WHEN TS_MACHINE_FLAGS & @pTerminalManual > 0 THEN 1 ELSE 0 END) AS BlockManual                  ")
    _sb.AppendLine("           , SUM (CASE WHEN TS_MACHINE_FLAGS & @pTerminalBloqued > 0 THEN 1 ELSE 0 END) AS BlockAlways                 ")
    _sb.AppendLine("           , SUM (CASE WHEN NOT TS_MACHINE_FLAGS & @pTerminalBloqued > 0                                               ")
    _sb.AppendLine("                        AND NOT TS_MACHINE_FLAGS & @pTerminalAutomatically > 0                                         ")
    _sb.AppendLine("                        AND NOT TS_MACHINE_FLAGS & @pTerminalManual > 0                                                ")
    _sb.AppendLine("                         OR TS_MACHINE_FLAGS & @pTerminalUnbloqued > 0 THEN 1 ELSE 0 END) AS UnblockTotal              ")
    _sb.AppendLine("           , SUM (CASE WHEN NOT TS_MACHINE_FLAGS & @pTerminalAutomatically > 0 THEN 1 ELSE 0 END) AS UnblockAutomatic  ")
    _sb.AppendLine("           , SUM (CASE WHEN NOT TS_MACHINE_FLAGS & @pTerminalManual > 0 THEN 1 ELSE 0 END) AS UnblockManual            ")
    _sb.AppendLine("           , SUM (CASE WHEN TS_MACHINE_FLAGS & @pTerminalUnbloqued > 0 THEN 1 ELSE 0 END) AS UnblockAlways             ")
    _sb.AppendLine("           , SUM (CASE WHEN TE_CURRENT_PLAY_SESSION_ID IS NOT NULL THEN 1 ELSE 0 END) AS TerminalConnected             ")
    _sb.AppendLine("           , COUNT(1) AS TerminalCount                                                                                 ")
    _sb.AppendLine("      FROM   TERMINALS                                                                                                 ")
    _sb.AppendLine(" LEFT JOIN   TERMINAL_STATUS ON TS_TERMINAL_ID = TE_TERMINAL_ID                                                        ")
    _sb.AppendLine("     WHERE   TE_STATUS = @pTerminalStatus                                                                              ")
    _sb.AppendLine("       AND   TE_TERMINAL_TYPE IN (" & WSI.Common.Misc.GamingTerminalTypeListToString() & ")                            ")
    _sb.AppendLine("       AND   TE_TYPE   = 1                                                                                             ")

    Try

      Using _db_trx As DB_TRX = New DB_TRX()
        Using _sql_cmd As SqlCommand = New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          _sql_cmd.Parameters.Add("@pTerminalBloqued", SqlDbType.Int).Value = TerminalStatusFlags.MACHINE_FLAGS.ALWAYS_BLOCKED
          _sql_cmd.Parameters.Add("@pTerminalUnbloqued", SqlDbType.Int).Value = TerminalStatusFlags.MACHINE_FLAGS.ALWAYS_UNBLOCKED
          _sql_cmd.Parameters.Add("@pTerminalAutomatically", SqlDbType.Int).Value = TerminalStatusFlags.MACHINE_FLAGS.BLOCKED_AUTOMATICALLY
          _sql_cmd.Parameters.Add("@pTerminalManual", SqlDbType.Int).Value = TerminalStatusFlags.MACHINE_FLAGS.BLOCKED_MANUALLY
          _sql_cmd.Parameters.Add("@pTerminalStatus", SqlDbType.Int).Value = TerminalStatus.ACTIVE
          Using _sql_da As New SqlDataAdapter(_sql_cmd)

            _db_trx.Fill(_sql_da, _table)

          End Using
        End Using
      End Using

      Return _table
    Catch _ex As Exception
      Log.Exception(_ex)

    End Try

    Return _table

  End Function 'ReadCountStatusTerminals

#End Region ' Shared Functions

End Class
