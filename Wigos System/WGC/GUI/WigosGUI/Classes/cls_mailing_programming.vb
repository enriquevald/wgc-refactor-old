'-------------------------------------------------------------------
' Copyright � 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_mailing_programming.vb
'
' DESCRIPTION:   Mailing Programming class
'
' AUTHOR:        Ra�l Cervera
'
' CREATION DATE: 22-DEC-2010
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 22-DEC-2010  RCI    Initial version.
' 29-NOV-2012  RRB    Added Terminals Without Activity mail type.
' 07-OCT-2013  DLL    Added WXP SAS Meters mail type.
' 12-FEB-2015  DRV    Fixed Bug WIG-2055: Configuration is not saved successfully
' 28-JUN-2016  EOR    Product Backlog Item 14627: Generic Reports: Added Mailing
' 15-SEP-2016  ETP    Fixed bugs 17585 - 17607: Generic reports: add permissions config.
' 29-JUN-2017  DHA    PBI 28419:WIGOS-80 Score report - Email configuration - Edition form - Update
'--------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports WSI.Common.GenericReport


Public Class CLASS_MAILING_PROGRAMMING
  Inherits CLASS_BASE

#Region " Constants "

  ' Fields length
  Public Const MAX_NAME_LEN As Integer = 50
  Public Const MAX_ADDRESS_LIST_LEN As Integer = 500
  Public Const MAX_SUBJECT_LEN As Integer = 200

#End Region ' Constants

#Region "Structure"

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_MAILING_PROGRAMMING
    Public prog_id As Integer
    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_NAME_LEN + 1)> _
    Public name As String
    Public enabled As Integer
    Public type As Integer
    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_ADDRESS_LIST_LEN + 1)> _
    Public address_list As String
    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_SUBJECT_LEN + 1)> _
    Public subject As String
    Public schedule_weekday As Integer
    Public schedule_time_from As Integer
    Public schedule_time_to As Integer
    Public schedule_time_step As Integer
  End Class

#End Region ' Structure

#Region "Constructor / Destructor"

  Public Sub New()
    Call MyBase.New()
  End Sub

  Protected Overrides Sub Finalize()
    MyBase.Finalize()

  End Sub

#End Region ' Constructor / Destructor

#Region " Members "

  Protected m_mailing_programming As New TYPE_MAILING_PROGRAMMING

#End Region ' Members

#Region "Properties"

  Public Property MailingProgrammingId() As Integer
    Get
      Return m_mailing_programming.prog_id
    End Get
    Set(ByVal Value As Integer)
      m_mailing_programming.prog_id = Value
    End Set
  End Property

  Public Property Name() As String
    Get
      Return m_mailing_programming.name
    End Get
    Set(ByVal Value As String)
      m_mailing_programming.name = Value
    End Set
  End Property

  Public Property Enabled() As Boolean
    Get
      Return m_mailing_programming.enabled
    End Get

    Set(ByVal Value As Boolean)
      m_mailing_programming.enabled = Value
    End Set

  End Property

  Public Property Type() As Integer
    Get
      Return m_mailing_programming.type
    End Get
    Set(ByVal Value As Integer)
      m_mailing_programming.type = Value
    End Set
  End Property

  Public Property AddressList() As String
    Get
      Return m_mailing_programming.address_list
    End Get
    Set(ByVal Value As String)
      m_mailing_programming.address_list = Value
    End Set
  End Property

  Public Property Subject() As String
    Get
      Return m_mailing_programming.subject
    End Get
    Set(ByVal Value As String)
      m_mailing_programming.subject = Value
    End Set
  End Property

  Public Property ScheduleWeekday() As Integer
    Get
      Return m_mailing_programming.schedule_weekday
    End Get
    Set(ByVal Value As Integer)
      m_mailing_programming.schedule_weekday = Value
    End Set
  End Property

  Public Property ScheduleTimeFrom() As Integer
    Get
      Return m_mailing_programming.schedule_time_from
    End Get
    Set(ByVal Value As Integer)
      m_mailing_programming.schedule_time_from = Value
    End Set
  End Property

  Public Property ScheduleTimeTo() As Integer
    Get
      Return m_mailing_programming.schedule_time_to
    End Get
    Set(ByVal Value As Integer)
      m_mailing_programming.schedule_time_to = Value
    End Set
  End Property

  Public Property ScheduleTimeStep() As Integer
    Get
      Return m_mailing_programming.schedule_time_step
    End Get
    Set(ByVal Value As Integer)
      m_mailing_programming.schedule_time_step = Value
    End Set
  End Property

  Public ReadOnly Property ScheduleDaysText() As String
    Get
      Dim str_binary As String
      Dim bit_array As Char()
      Dim str_days As String

      str_binary = Convert.ToString(m_mailing_programming.schedule_weekday, 2)
      str_binary = New String("0", 7 - str_binary.Length) + str_binary

      bit_array = str_binary.ToCharArray()

      str_days = ""
      If (bit_array(5) = "1") Then str_days = str_days & " " & GLB_NLS_GUI_JACKPOT_MGR.GetString(298) '"Monday "
      If (bit_array(4) = "1") Then str_days = str_days & " " & GLB_NLS_GUI_JACKPOT_MGR.GetString(299) '"Tuesday "
      If (bit_array(3) = "1") Then str_days = str_days & " " & GLB_NLS_GUI_JACKPOT_MGR.GetString(300) '"Wednesday "
      If (bit_array(2) = "1") Then str_days = str_days & " " & GLB_NLS_GUI_JACKPOT_MGR.GetString(301) '"Thursday "
      If (bit_array(1) = "1") Then str_days = str_days & " " & GLB_NLS_GUI_JACKPOT_MGR.GetString(302) '"Friday "
      If (bit_array(0) = "1") Then str_days = str_days & " " & GLB_NLS_GUI_JACKPOT_MGR.GetString(303) '"Saturday "
      If (bit_array(6) = "1") Then str_days = str_days & " " & GLB_NLS_GUI_JACKPOT_MGR.GetString(304) '"Sunday "

      Return str_days.Trim
    End Get
  End Property

#End Region ' Properties

#Region " Overrides functions "

  '----------------------------------------------------------------------------
  ' PURPOSE: Reads from the database into the given object
  '
  ' PARAMS:
  '   - INPUT:
  '     - ObjectId: An object, it must be 'casted' to the desired KEY.
  '
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As ENUM_STATUS

    Dim rc As Integer

    Me.MailingProgrammingId = ObjectId

    ' Read Mailing Programming information
    rc = MailingProg_DbRead(m_mailing_programming, SqlCtx)

    Select Case rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function 'DB_Read

  '----------------------------------------------------------------------------
  ' PURPOSE: Updates the given object to the database.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As ENUM_STATUS

    Dim rc As Integer

    ' Read Mailing Programming information
    rc = MailingProg_DbUpdate(m_mailing_programming, SqlCtx)

    Select Case rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select
  End Function 'DB_Update

  '----------------------------------------------------------------------------
  ' PURPOSE: Inserts the given object to the database.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As ENUM_STATUS

    Dim rc As Integer

    ' Insert mailing programming data
    rc = MailingProg_DbInsert(m_mailing_programming, SqlCtx)

    Select Case rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
        Return ENUM_STATUS.STATUS_DUPLICATE_KEY

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Insert

  '----------------------------------------------------------------------------
  ' PURPOSE: Deletes the given object from the database.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As ENUM_STATUS

    Dim rc As Integer

    ' Delete mailing programming data
    rc = MailingProg_DbDelete(Me.MailingProgrammingId, SqlCtx)

    Select Case rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DEPENDENCIES
        Return ENUM_STATUS.STATUS_DEPENDENCIES

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Delete

  '----------------------------------------------------------------------------
  ' PURPOSE: Returns a duplicate of the given object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim temp_prog As CLASS_MAILING_PROGRAMMING

    temp_prog = New CLASS_MAILING_PROGRAMMING
    temp_prog.m_mailing_programming.prog_id = Me.m_mailing_programming.prog_id
    temp_prog.m_mailing_programming.name = Me.m_mailing_programming.name
    temp_prog.m_mailing_programming.enabled = Me.m_mailing_programming.enabled
    temp_prog.m_mailing_programming.type = Me.m_mailing_programming.type
    temp_prog.m_mailing_programming.address_list = Me.m_mailing_programming.address_list
    temp_prog.m_mailing_programming.subject = Me.m_mailing_programming.subject
    temp_prog.m_mailing_programming.schedule_weekday = Me.m_mailing_programming.schedule_weekday
    temp_prog.m_mailing_programming.schedule_time_from = Me.m_mailing_programming.schedule_time_from
    temp_prog.m_mailing_programming.schedule_time_to = Me.m_mailing_programming.schedule_time_to
    temp_prog.m_mailing_programming.schedule_time_step = Me.m_mailing_programming.schedule_time_step

    Return temp_prog

  End Function 'Duplicate 

  '----------------------------------------------------------------------------
  ' PURPOSE: Returns the related auditor data for the current object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim auditor_data As CLASS_AUDITOR_DATA
    Dim str_aux As String
    Dim str_aux2 As String
    Dim seconds As Integer
    Dim hours As Integer
    Dim minutes As Integer
    Dim days As Integer
    Dim date_time As Date
    Dim subject As String

    auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_MAILING_PROGRAMMING)
    Call auditor_data.SetName(GLB_NLS_GUI_STATISTICS.Id(432), Me.Name)

    ' Name
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(253), Me.Name)

    ' Enabled
    If Me.Enabled Then
      Call auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(281), GLB_NLS_GUI_CONTROLS.GetString(283))
    Else
      Call auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(281), GLB_NLS_GUI_CONTROLS.GetString(284))
    End If

    ' Type
    Select Case Me.Type
      Case WSI.Common.MAILING_PROGRAMMING_TYPE.CASH_INFORMATION
        Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(343), GLB_NLS_GUI_STATISTICS.GetString(427))

      Case WSI.Common.MAILING_PROGRAMMING_TYPE.WSI_STATISTICS
        Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(343), GLB_NLS_GUI_STATISTICS.GetString(439))

      Case WSI.Common.MAILING_PROGRAMMING_TYPE.CONNECTED_TERMINALS
        Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(343), GLB_NLS_GUI_STATISTICS.GetString(440))

      Case WSI.Common.MAILING_PROGRAMMING_TYPE.TERMINALS_WITHOUT_ACTIVITY
        Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(343), GLB_NLS_GUI_INVOICING.GetString(361))

      Case WSI.Common.MAILING_PROGRAMMING_TYPE.WXP_MACHINE_METERS
        Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(343), GLB_NLS_GUI_INVOICING.GetString(2692))

      Case WSI.Common.MAILING_PROGRAMMING_TYPE.TITO_TICKET_OUT_REPORT
        Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(343), GLB_NLS_GUI_PLAYER_TRACKING.GetString(5805))

      Case WSI.Common.MAILING_PROGRAMMING_TYPE.MACHINE_AND_GAME_REPORT
        Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(343), GLB_NLS_GUI_PLAYER_TRACKING.GetString(5820))

      Case WSI.Common.MAILING_PROGRAMMING_TYPE.TITO_HOLD_VS_WIN
        Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(343), GLB_NLS_GUI_PLAYER_TRACKING.GetString(7193))

      Case WSI.Common.MAILING_PROGRAMMING_TYPE.SCORE_REPORT
        Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(343), GLB_NLS_GUI_PLAYER_TRACKING.GetString(8458))

        'EOR 28-JUN-2018
      Case Else
        If Type > 100 And Type < 200 Then
          Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(343), GetTypeOfMailing(Type - 100))
        Else
          Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(343), GLB_NLS_GUI_CONTROLS.GetString(408))
        End If
    End Select

    ' Address List
    Call auditor_data.SetField(GLB_NLS_GUI_STATISTICS.Id(429), Me.AddressList)

    ' Subject
    If String.IsNullOrEmpty(Me.Subject) Then
      subject = ""
    Else
      subject = Me.Subject.Replace("'", "''")
    End If
    Call auditor_data.SetField(GLB_NLS_GUI_STATISTICS.Id(436), subject)

    ' Schedule Weekdays
    auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(299), ScheduleDaysText())

    ' Schedule timetable 
    date_time = GUI_GetDateTime()
    Call Format_SecondsToDdHhMmSs(Me.ScheduleTimeFrom, days, hours, minutes, seconds)
    str_aux2 = GUI_FormatTime(New DateTime(date_time.Year, date_time.Month, date_time.Day, hours, minutes, 0), ModuleDateTimeFormats.ENUM_FORMAT_TIME.FORMAT_HHMM)
    str_aux = GLB_NLS_GUI_JACKPOT_MGR.GetString(230) & "." & GLB_NLS_GUI_JACKPOT_MGR.GetString(204)
    auditor_data.SetField(0, str_aux2, str_aux)

    date_time = GUI_GetDateTime()
    Call Format_SecondsToDdHhMmSs(Me.ScheduleTimeTo, days, hours, minutes, seconds)
    str_aux2 = GUI_FormatTime(New DateTime(date_time.Year, date_time.Month, date_time.Day, hours, minutes, 0), ModuleDateTimeFormats.ENUM_FORMAT_TIME.FORMAT_HHMM)
    str_aux = GLB_NLS_GUI_JACKPOT_MGR.GetString(230) & "." & GLB_NLS_GUI_JACKPOT_MGR.GetString(205)
    auditor_data.SetField(0, str_aux2, str_aux)

    date_time = GUI_GetDateTime()
    Call Format_SecondsToDdHhMmSs(Me.ScheduleTimeStep, days, hours, minutes, seconds)
    str_aux2 = GUI_FormatTime(New DateTime(date_time.Year, date_time.Month, date_time.Day, hours, minutes, 0), ModuleDateTimeFormats.ENUM_FORMAT_TIME.FORMAT_HHMM)
    str_aux = GLB_NLS_GUI_JACKPOT_MGR.GetString(230) & "." & GLB_NLS_GUI_STATISTICS.GetString(431)
    auditor_data.SetField(0, str_aux2, str_aux)

    Return auditor_data

  End Function 'AuditorData

#End Region 'Overrides functions

#Region "DB Functions"

  Private Function MailingProg_DbRead(ByVal MailingProg As TYPE_MAILING_PROGRAMMING, _
                                      ByVal Context As Integer) As Integer

    Dim str_sql As String
    Dim data_table As DataTable

    str_sql = "SELECT   MP_NAME " & _
              "       , MP_ENABLED " & _
              "       , MP_TYPE " & _
              "       , MP_ADDRESS_LIST " & _
              "       , MP_SUBJECT " & _
              "       , MP_SCHEDULE_WEEKDAY " & _
              "       , MP_SCHEDULE_TIME_FROM " & _
              "       , MP_SCHEDULE_TIME_TO " & _
              "       , MP_SCHEDULE_TIME_STEP " & _
              "  FROM   MAILING_PROGRAMMING " & _
              " WHERE   MP_PROG_ID = " & MailingProg.prog_id

    data_table = GUI_GetTableUsingSQL(str_sql, 5000)
    If IsNothing(data_table) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    If data_table.Rows.Count() <> 1 Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    MailingProg.name = NullTrim(data_table.Rows(0).Item("MP_NAME"))
    MailingProg.enabled = data_table.Rows(0).Item("MP_ENABLED")
    MailingProg.type = data_table.Rows(0).Item("MP_TYPE")
    MailingProg.address_list = NullTrim(data_table.Rows(0).Item("MP_ADDRESS_LIST"))
    MailingProg.subject = NullTrim(data_table.Rows(0).Item("MP_SUBJECT"))
    MailingProg.schedule_weekday = data_table.Rows(0).Item("MP_SCHEDULE_WEEKDAY")
    MailingProg.schedule_time_from = data_table.Rows(0).Item("MP_SCHEDULE_TIME_FROM")
    MailingProg.schedule_time_to = data_table.Rows(0).Item("MP_SCHEDULE_TIME_TO")
    MailingProg.schedule_time_step = data_table.Rows(0).Item("MP_SCHEDULE_TIME_STEP")

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

  End Function ' MailingProg_DbRead

  Private Function MailingProg_DbUpdate(ByVal MailingProg As TYPE_MAILING_PROGRAMMING, _
                                        ByVal Context As Integer) As Integer

    Dim SqlTrans As SqlTransaction = Nothing
    Dim sql_command As SqlCommand
    Dim str_sql As String
    Dim num_rows_updated As Integer

    Try

      If Not GUI_BeginSQLTransaction(SqlTrans) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      str_sql = "UPDATE   MAILING_PROGRAMMING " & _
                  " SET   MP_NAME               = @pName " & _
                      " , MP_ENABLED            = @pEnabled " & _
                      " , MP_TYPE               = @pType " & _
                      " , MP_ADDRESS_LIST       = @pAddressList " & _
                      " , MP_SUBJECT            = @pSubject " & _
                      " , MP_SCHEDULE_WEEKDAY   = @pWeekday " & _
                      " , MP_SCHEDULE_TIME_FROM = @pTimeFrom " & _
                      " , MP_SCHEDULE_TIME_TO   = @pTimeTo " & _
                      " , MP_SCHEDULE_TIME_STEP = @pTimeStep " & _
                " WHERE   MP_PROG_ID            = @pProgId "

      sql_command = New SqlCommand(str_sql)
      sql_command.Connection = SqlTrans.Connection
      sql_command.Transaction = SqlTrans

      sql_command.Parameters.Add("@pName", SqlDbType.NVarChar).Value = MailingProg.name
      sql_command.Parameters.Add("@pEnabled", SqlDbType.Int).Value = MailingProg.enabled
      sql_command.Parameters.Add("@pType", SqlDbType.Int).Value = MailingProg.type
      sql_command.Parameters.Add("@pAddressList", SqlDbType.NVarChar).Value = MailingProg.address_list
      sql_command.Parameters.Add("@pSubject", SqlDbType.NVarChar).Value = MailingProg.subject
      sql_command.Parameters.Add("@pWeekday", SqlDbType.Int).Value = MailingProg.schedule_weekday
      sql_command.Parameters.Add("@pTimeFrom", SqlDbType.Int).Value = MailingProg.schedule_time_from
      sql_command.Parameters.Add("@pTimeTo", SqlDbType.Int).Value = MailingProg.schedule_time_to
      sql_command.Parameters.Add("@pTimeStep", SqlDbType.Int).Value = MailingProg.schedule_time_step

      sql_command.Parameters.Add("@pProgId", SqlDbType.BigInt).Value = MailingProg.prog_id

      num_rows_updated = sql_command.ExecuteNonQuery()

      If num_rows_updated <> 1 Then
        GUI_EndSQLTransaction(SqlTrans, False)

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      If Not GUI_EndSQLTransaction(SqlTrans, True) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

    Catch ex As Exception
      GUI_EndSQLTransaction(SqlTrans, False)

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

  End Function ' MailingProg_DbUpdate

  Private Function MailingProg_DbInsert(ByVal MailingProg As TYPE_MAILING_PROGRAMMING, _
                                        ByVal Context As Integer) As Integer
    Dim SqlTrans As SqlTransaction = Nothing
    Dim sql_command As SqlCommand
    Dim str_sql As String
    Dim num_rows_updated As Integer

    Try

      If Not GUI_BeginSQLTransaction(SqlTrans) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      str_sql = "INSERT INTO   MAILING_PROGRAMMING " & _
                "            ( MP_NAME " & _
                "            , MP_ENABLED " & _
                "            , MP_TYPE " & _
                "            , MP_ADDRESS_LIST " & _
                "            , MP_SUBJECT " & _
                "            , MP_SCHEDULE_WEEKDAY " & _
                "            , MP_SCHEDULE_TIME_FROM " & _
                "            , MP_SCHEDULE_TIME_TO " & _
                "            , MP_SCHEDULE_TIME_STEP " & _
                "            ) " & _
                "     VALUES " & _
                "           ( @pName " & _
                "           , @pEnabled " & _
                "           , @pType " & _
                "           , @pAddressList " & _
                "           , @pSubject " & _
                "           , @pWeekday " & _
                "           , @pTimeFrom " & _
                "           , @pTimeTo " & _
                "           , @pTimeStep " & _
                "           )"

      sql_command = New SqlCommand(str_sql)
      sql_command.Transaction = SqlTrans
      sql_command.Connection = SqlTrans.Connection

      sql_command.Parameters.Add("@pName", SqlDbType.NVarChar).Value = MailingProg.name
      sql_command.Parameters.Add("@pEnabled", SqlDbType.Int).Value = MailingProg.enabled
      sql_command.Parameters.Add("@pType", SqlDbType.Int).Value = MailingProg.type
      sql_command.Parameters.Add("@pAddressList", SqlDbType.NVarChar).Value = MailingProg.address_list
      sql_command.Parameters.Add("@pSubject", SqlDbType.NVarChar).Value = MailingProg.subject
      sql_command.Parameters.Add("@pWeekday", SqlDbType.Int).Value = MailingProg.schedule_weekday
      sql_command.Parameters.Add("@pTimeFrom", SqlDbType.Int).Value = MailingProg.schedule_time_from
      sql_command.Parameters.Add("@pTimeTo", SqlDbType.Int).Value = MailingProg.schedule_time_to
      sql_command.Parameters.Add("@pTimeStep", SqlDbType.Int).Value = MailingProg.schedule_time_step

      num_rows_updated = sql_command.ExecuteNonQuery()

      If num_rows_updated <> 1 Then
        GUI_EndSQLTransaction(SqlTrans, False)

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      If Not GUI_EndSQLTransaction(SqlTrans, True) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

    Catch ex As Exception
      GUI_EndSQLTransaction(SqlTrans, False)

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

  End Function ' MailingProg_DbInsert

  Private Function MailingProg_DbDelete(ByVal ProgId As Integer, _
                                        ByVal Context As Integer) As Integer
    Dim SqlTrans As SqlTransaction = Nothing
    Dim sql_command As SqlCommand
    Dim str_sql As String
    Dim num_rows_updated As Integer

    Try

      If Not GUI_BeginSQLTransaction(SqlTrans) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      str_sql = "DELETE   MAILING_PROGRAMMING " & _
                " WHERE   MP_PROG_ID = @pProgId "

      sql_command = New SqlCommand(str_sql)
      sql_command.Transaction = SqlTrans
      sql_command.Connection = SqlTrans.Connection

      sql_command.Parameters.Add("@pProgId", SqlDbType.BigInt).Value = ProgId

      num_rows_updated = sql_command.ExecuteNonQuery()

      If num_rows_updated <> 1 Then
        GUI_EndSQLTransaction(SqlTrans, False)

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      If Not GUI_EndSQLTransaction(SqlTrans, True) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

    Catch ex As Exception
      GUI_EndSQLTransaction(SqlTrans, False)

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

  End Function ' MailingProg_DbDelete

#End Region ' DB Functions

#Region "Public Functions"

  'EOR 28-JUN-2016
  Public Function GetTypeOfMailing(ByVal IdReport As Integer) As String
    Dim _service As IReportToolConfigService
    Dim _reponse As ReportToolConfigResponse

    _service = New ReportToolConfigService()
    _reponse = New ReportToolConfigResponse

    _reponse = _service.GetReportToolConfig(IdReport)

    If (_reponse.Success) Then
      For Each item As ReportToolConfigDTO In _reponse.ReportToolConfigs
        If item.ReportID = IdReport Then
          Return item.ReportName
        End If
      Next
    End If

    Return GLB_NLS_GUI_CONTROLS.GetString(408)
  End Function

#End Region 'Public Functions

End Class ' CLASS_MAILING_PROGRAMMING
