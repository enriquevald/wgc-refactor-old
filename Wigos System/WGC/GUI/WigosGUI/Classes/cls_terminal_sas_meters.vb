'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : cls_terminal_sas_meters.vb
'
' DESCRIPTION : 
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 11-NOV-2014  DLL    Initial version
' 16-DEC-2014  DLL    Fixed Bug WIG-1838: Different errors
' 30-MAR-2015  DLL    Fixed Bug WIG-2186: Incorrect message
' 08-ABR-2015  FOS    TASK 974: Show only period of terminals is active
'--------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports System.Text
Imports WSI.Common

Public Class cls_terminal_sas_meters
  Inherits CLASS_BASE

#Region "Enum"
  Public Enum FilterType
    All = 0
    WithoutValue = 1
    WithValue = 2
  End Enum

#End Region

#Region "Members"
  Private m_date_from As DateTime
  Private m_date_to As DateTime
  Private m_terminal_sas_meters As DataTable
  Private m_meter_list As List(Of Meter)
  Private m_group_meter_list As List(Of MetersGroup)
  Private m_selected_terminal As Long()
  Private m_meter_selected As String
  Private m_meter_name As Dictionary(Of Int32, String)
  Private m_filter_type As FilterType
  Private m_meter_max_value As Dictionary(Of Int32, List(Of MaxMeter))

#End Region

  Private Const EVENT_START_BASE As Int32 = &H10000
  Private Const NLS_EVENTS As Int32 = 4000

  Public Class MaxMeter
    Public Code As Int32
    Public MaxValue As Long
  End Class

  Public Class Meter
    Public Code As Int32
    Public Description As String
    Public HexCode As String
    Public CatalogName As String
  End Class

  Public Class MetersGroup
    Public Id As Int32
    Public GroupName As String
    Public Type As String
    Public MeterList As List(Of Meter)
  End Class

#Region "Properties"
  Public Property DateFrom() As DateTime
    Get
      Return m_date_from
    End Get

    Set(ByVal Value As DateTime)
      m_date_from = Value
    End Set
  End Property ' DateFrom

  Public Property DateTo() As DateTime
    Get
      Return m_date_to
    End Get

    Set(ByVal Value As DateTime)
      m_date_to = Value
    End Set
  End Property ' DateTo

  Public Property TerminalSasMeters() As DataTable
    Get
      Return m_terminal_sas_meters
    End Get

    Set(ByVal Value As DataTable)
      m_terminal_sas_meters = Value
    End Set
  End Property ' TerminalSasMeters

  Public Property TerminalMaxMeters() As Dictionary(Of Int32, List(Of MaxMeter))
    Get
      Return m_meter_max_value
    End Get

    Set(ByVal Value As Dictionary(Of Int32, List(Of MaxMeter)))
      m_meter_max_value = Value
    End Set
  End Property ' TerminalSasMeters

  Public Property MeterList() As List(Of Meter)
    Get
      Return m_meter_list
    End Get

    Set(ByVal Value As List(Of Meter))
      m_meter_list = Value
    End Set
  End Property ' MeterList

  Public Property GroupMeterList() As List(Of MetersGroup)
    Get
      Return m_group_meter_list
    End Get

    Set(ByVal Value As List(Of MetersGroup))
      m_group_meter_list = Value
    End Set
  End Property ' GroupMeterList

  Public Property TerminalsSelected() As Long()
    Get
      Return m_selected_terminal
    End Get

    Set(ByVal Value As Long())
      m_selected_terminal = Value
    End Set
  End Property ' TerminalsSelected

  Public Property MetersSelected() As String
    Get
      Return m_meter_selected
    End Get

    Set(ByVal Value As String)
      m_meter_selected = Value
    End Set
  End Property ' MetersSelected

  Public Property MeterName() As Dictionary(Of Int32, String)
    Get
      Return m_meter_name
    End Get

    Set(ByVal Value As Dictionary(Of Int32, String))
      m_meter_name = Value
    End Set
  End Property ' MeterName

  Public Property FilterTypeValue() As FilterType
    Get
      Return m_filter_type
    End Get

    Set(ByVal Value As FilterType)
      m_filter_type = Value
    End Set
  End Property ' MeterName
#End Region

  Public Sub New()
    LoadMeters()
  End Sub



#Region "Overrides"
  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA
    Return Nothing
  End Function

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR

  End Function

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR

  End Function

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

  End Function

  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE
    Return Nothing
  End Function

  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

  End Function
#End Region

#Region "Private Functions"

  Private Function TerminalSasMeters_DbRead() As DataTable
    Dim _dt_terminal_sas_meters As DataTable
    Dim _sb As StringBuilder

    _dt_terminal_sas_meters = New DataTable()
    _sb = New StringBuilder()

    _sb.AppendLine(CreateTimeTable())

    ' TERMINAL_SAS_METERS_HISTORY
    _sb.AppendLine("     SELECT  ''               ")
    _sb.AppendLine("           , TSMH_DATETIME  AS DATATIMELONG ")
    _sb.AppendLine("           , TSMH_DATETIME    ")
    _sb.AppendLine("           , TSMH_TERMINAL_ID ")
    _sb.AppendLine("           , ''               AS TE_PROVIDER ")
    _sb.AppendLine("           , ''               AS TE_NAME ")
    _sb.AppendLine("           , ''               AS TE_AREA")
    _sb.AppendLine("           , ''               AS TE_BANK")
    _sb.AppendLine("           , ''               AS TE_POSITION")

    For Each _meter As Meter In m_meter_list
      _sb.AppendFormat("     , A{0} ", _meter.Code) 'Initial
      _sb.AppendFormat("     , B{0} ", _meter.Code) 'Final
      _sb.AppendFormat("     , C{0} ", _meter.Code) 'Increment
    Next
    _sb.AppendLine("       FROM                   ")
    _sb.AppendLine("(    SELECT  TIME_DATE AS TSMH_DATETIME   ")
    _sb.AppendLine("           , TT_TERMINAL_ID   AS TSMH_TERMINAL_ID ")
    _sb.AppendLine("           , ''               AS TE_PROVIDER ")
    _sb.AppendLine("           , ''               AS TE_NAME ")
    _sb.AppendLine("           , ''               AS TE_AREA")
    _sb.AppendLine("           , ''               AS TE_BANK")
    _sb.AppendLine("           , ''               AS TE_POSITION")
    For Each _meter As Meter In m_meter_list
      _sb.AppendFormat("     , SUM(CASE WHEN (TSMH_METER_CODE={0}) THEN TSMH_METER_INI_VALUE ELSE NULL END) AS A{0} ", _meter.Code)
      _sb.AppendFormat("     , SUM(CASE WHEN (TSMH_METER_CODE={0}) THEN TSMH_METER_FIN_VALUE ELSE NULL END) AS B{0} ", _meter.Code)
      _sb.AppendFormat("     , SUM(CASE WHEN (TSMH_METER_CODE={0}) THEN TSMH_METER_INCREMENT ELSE NULL END) AS C{0} ", _meter.Code)
    Next

    _sb.AppendLine("      FROM   #TimeTable ")
    If m_filter_type = FilterType.All OrElse m_filter_type = FilterType.WithoutValue Then
      _sb.AppendLine("      LEFT   JOIN  TERMINAL_SAS_METERS_HISTORY ON CONVERT(VARCHAR, TSMH_DATETIME,104) = CONVERT(VARCHAR, TIME_DATE,104)  AND TSMH_TERMINAL_ID = TT_TERMINAL_ID " + GetSqlWhere())
    Else
      _sb.AppendLine("      LEFT   JOIN  TERMINAL_SAS_METERS_HISTORY ON CONVERT(VARCHAR, TSMH_DATETIME,104) = CONVERT(VARCHAR, TIME_DATE,104)  AND TSMH_TERMINAL_ID = TT_TERMINAL_ID")
    End If

    If m_filter_type = FilterType.WithValue Then
      _sb.AppendLine(" WHERE " + GetSqlWhere())
    End If

    If m_filter_type = FilterType.WithoutValue Then
      _sb.AppendLine(" WHERE TSMH_DATETIME IS NULL ")
    End If

    _sb.AppendLine("  GROUP BY   TIME_DATE, TT_TERMINAL_ID ) AS T_SAS_METERS ")

    _sb.AppendLine("ORDER BY   TSMH_DATETIME DESC ")

    _sb.AppendLine(" IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID('#TimeTable') AND type in (N'U')) ")
    _sb.AppendLine("    DROP TABLE #TimeTable ")

    _dt_terminal_sas_meters = GUI_GetTableUsingSQL(_sb.ToString(), 5000)

    If _dt_terminal_sas_meters Is Nothing Then
      _dt_terminal_sas_meters = New DataTable()
    End If

    Return _dt_terminal_sas_meters

  End Function

  Private Function TerminalMaxMeters_DbRead(ByVal DbTrx As DB_TRX) As DataTable
    Dim _dt_terminal_max_meters As DataTable
    Dim _str_terminals As String
    Dim _sb As StringBuilder

    _dt_terminal_max_meters = New DataTable()
    _sb = New StringBuilder()
    _str_terminals = ""

    ' TERMINAL_SAS_METERS_HISTORY
    _sb.AppendLine("     SELECT  TSM_TERMINAL_ID    ")

    For Each _meter As Meter In m_meter_list
      _sb.AppendFormat("     , A{0} ", _meter.Code) 'max
    Next
    _sb.AppendLine("       FROM                   ")
    _sb.AppendLine("(    SELECT  TSM_TERMINAL_ID   ")
    For Each _meter As Meter In m_meter_list
      _sb.AppendFormat("     , SUM(CASE WHEN (TSM_METER_CODE={0}) THEN TSM_METER_MAX_VALUE ELSE NULL END) AS A{0} ", _meter.Code)
    Next

    _sb.AppendLine("      FROM   TERMINAL_SAS_METERS ")

    If Me.TerminalsSelected.Length > 0 Then
      For Each _terminal_id As Long In Me.TerminalsSelected
        _str_terminals &= _terminal_id & ","
      Next
      _str_terminals = _str_terminals.Substring(0, (_str_terminals.Length - 1))
      _sb.AppendLine(" WHERE   TSM_TERMINAL_ID IN (" + _str_terminals + ") ")
    End If

    _sb.AppendLine("  GROUP BY  TSM_TERMINAL_ID ) AS T_SAS_METERS ")

    _sb.AppendLine("ORDER BY   TSM_TERMINAL_ID DESC ")

    Using _cmd As SqlCommand = New SqlCommand(_sb.ToString(), DbTrx.SqlTransaction.Connection, DbTrx.SqlTransaction)
      Using _sql_adapter As SqlDataAdapter = New SqlDataAdapter(_cmd)
        DbTrx.Fill(_sql_adapter, _dt_terminal_max_meters)
      End Using
    End Using
    Return _dt_terminal_max_meters

  End Function

  Private Function CreateTimeTable() As String
    Dim _sb As StringBuilder
    Dim _str_terminals As String

    _str_terminals = ""
    For Each _terminal_id As Long In Me.TerminalsSelected
      _str_terminals &= _terminal_id & ","
    Next

    _sb = New StringBuilder()

    _sb.AppendLine(" DECLARE   @pDateFrom AS DATETIME ")
    _sb.AppendLine(" DECLARE   @pDateTo AS DATETIME ")
    _sb.AppendLine(" DECLARE   @_DAY_VAR AS DATETIME ")
    _sb.AppendLine(" DECLARE   @TerminalList AS NVARCHAR(MAX) ")
    _sb.AppendLine(" DECLARE   @pos AS INT ")
    _sb.AppendLine(" DECLARE   @len AS INT ")
    _sb.AppendLine(" DECLARE   @terminal_id AS INT ")
    _sb.AppendLine(" DECLARE   @pTerminalActivationDate AS DATETIME")
    _sb.AppendLine(" DECLARE   @pTerminalRetiredDate AS DATETIME")



    _sb.AppendLine(" IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID('#TimeTable') AND type in (N'U')) ")
    _sb.AppendLine("    DROP TABLE #TimeTable ")

    _sb.AppendLine(" CREATE TABLE #TimeTable (TIME_DATE DATETIME, TT_TERMINAL_ID INT)  ")

    _sb.AppendLine(" SET @pDateFrom = " + GUI_FormatDateDB(m_date_from))
    _sb.AppendLine(" SET @pDateTo = " + GUI_FormatDateDB(m_date_to))
    _sb.AppendLine(" SET @TerminalList = '" + _str_terminals + "' ")
    _sb.AppendLine(" SET @pos = 0 ")
    _sb.AppendLine(" SET @len = 0 ")

    _sb.AppendLine(" WHILE CHARINDEX(',', @TerminalList, @pos+1)>0 ")
    _sb.AppendLine(" BEGIN ")

    _sb.AppendLine("   SET @len = CHARINDEX(',', @TerminalList, @pos+1) - @pos ")
    _sb.AppendLine("   SET @terminal_id = SUBSTRING(@TerminalList, @pos, @len) ")

    _sb.AppendLine("   SET @_DAY_VAR = @pDateFrom ")

    _sb.AppendLine("   WHILE @_DAY_VAR < @pDateTo ")
    _sb.AppendLine("   BEGIN ")
    _sb.AppendLine("     SET @pTerminalActivationDate = (select te_activation_date from terminals where te_terminal_id = @terminal_id)")
    _sb.AppendLine("     SET @pTerminalRetiredDate = (select te_retirement_date from terminals where te_terminal_id = @terminal_id)")
    _sb.AppendLine("        if (  ")
    _sb.AppendLine("             (convert(varchar(10), @pTerminalActivationDate, 120) <=  convert(varchar(10), @_DAY_VAR, 120))  ")
    _sb.AppendLine("             AND ")
    _sb.AppendLine("             (")
    _sb.AppendLine("               (")
    _sb.AppendLine("               convert(varchar(10), @pTerminalRetiredDate, 120) >=  convert(varchar(10), @_DAY_VAR, 120))")
    _sb.AppendLine("                OR ")
    _sb.AppendLine("               @pTerminalRetiredDate IS NULL")
    _sb.AppendLine("              )")
    _sb.AppendLine("           )")
    _sb.AppendLine("          BEGIN ")
    _sb.AppendLine("            INSERT INTO   #TimeTable (TIME_DATE, TT_TERMINAL_ID) VALUES (@_DAY_VAR, @terminal_id)")
    _sb.AppendLine("          END   ")
    _sb.AppendLine("      SET @_DAY_VAR =  DATEADD(day,1,@_DAY_VAR)")
    _sb.AppendLine("   END ")
    _sb.AppendLine(" set @pos = CHARINDEX(',', @TerminalList, @pos+@len) +1 ")
    _sb.AppendLine(" END ")


    Return _sb.ToString()
  End Function

  ' PURPOSE: Get the sql 'where' statement
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - an string with the 'where' statement
  Private Function GetSqlWhere() As String

    Dim _str_where As String
    Dim _str_terminals As String
    Dim _str_counters_types As String
    Dim _lst_counter_types As List(Of Integer)

    _str_where = String.Empty
    _str_terminals = String.Empty
    _str_counters_types = String.Empty
    _lst_counter_types = New List(Of Integer)

    ' Terminal filter
    If Me.TerminalsSelected.Length > 0 Then
      For Each _terminal_id As Long In Me.TerminalsSelected
        _str_terminals &= _terminal_id & ","
      Next
    End If

    _str_where = String.Format(" TSMH_TYPE = {0} ", CType(ENUM_SAS_METER_HISTORY.TSMH_MINCETUR_DAILY_REPORT, Integer))

    If m_filter_type = FilterType.All OrElse m_filter_type = FilterType.WithoutValue Then
      _str_where = String.Format(" AND {0} ", _str_where)
    End If

    ' Terminal filter
    If Me.TerminalsSelected.Length > 0 Then
      For Each _terminal_id As Long In Me.TerminalsSelected
        _str_terminals &= _terminal_id & ","
      Next
      _str_terminals = _str_terminals.Substring(0, (_str_terminals.Length - 1))
      _str_where = String.Format(" {0} AND TSMH_TERMINAL_ID IN ({1})", _str_where, _str_terminals)
    End If

    Return _str_where

  End Function ' GetSqlWhere

  Private Function TerminalSasMeters_DbUpdate(ByVal DbTrx As DB_TRX, ByVal ModifiedRows As DataTable) As Boolean
    Dim _sb As StringBuilder
    Dim _num_rows_inserted As Int32

    _sb = New StringBuilder()

    _sb.AppendLine("  IF NOT EXISTS   (SELECT 1 FROM TERMINAL_SAS_METERS_HISTORY ")
    _sb.AppendLine("  WHERE   TSMH_TERMINAL_ID = @terminal_id ")
    _sb.AppendLine("    AND   TSMH_METER_CODE = @meter_code ")
    _sb.AppendLine("    AND   TSMH_GAME_ID = 0 ")
    _sb.AppendLine("    AND   TSMH_DENOMINATION = 0 ")
    _sb.AppendLine("    AND   TSMH_TYPE = 20 ")
    _sb.AppendLine("    AND   TSMH_DATETIME = @time) ")
    _sb.AppendLine("  BEGIN ")
    _sb.AppendLine(" INSERT   INTO TERMINAL_SAS_METERS_HISTORY ")
    _sb.AppendLine("        ( TSMH_TERMINAL_ID ")
    _sb.AppendLine("        , TSMH_METER_CODE ")
    _sb.AppendLine("        , TSMH_GAME_ID ")
    _sb.AppendLine("        , TSMH_DENOMINATION ")
    _sb.AppendLine("        , TSMH_TYPE ")
    _sb.AppendLine("        , TSMH_DATETIME ")
    _sb.AppendLine("        , TSMH_METER_INI_VALUE ")
    _sb.AppendLine("        , TSMH_METER_FIN_VALUE ")
    _sb.AppendLine("        , TSMH_METER_INCREMENT ")
    _sb.AppendLine("        , TSMH_RAW_METER_INCREMENT ")
    _sb.AppendLine("      ) ")
    _sb.AppendLine(" VALUES ")
    _sb.AppendLine("        ( @terminal_id ")
    _sb.AppendLine("        , @meter_code ")
    _sb.AppendLine("        , 0 ")
    _sb.AppendLine("        , 0 ")
    _sb.AppendLine("        , 20 ")
    _sb.AppendLine("        , @time ")
    _sb.AppendLine("        , CASE WHEN (@initial IS NULL) THEN 0 ELSE @initial END  ")
    _sb.AppendLine("        , CASE WHEN (@final IS NULL) THEN 0 ELSE @final END ")
    _sb.AppendLine("        , CASE WHEN (@increment IS NULL) THEN 0 ELSE @increment END ")
    _sb.AppendLine("        , 0 ")
    _sb.AppendLine("      ) ")
    _sb.AppendLine("   END ")

    _sb.AppendLine("   ELSE ")
    _sb.AppendLine("  BEGIN ")
    _sb.AppendLine(" UPDATE   TERMINAL_SAS_METERS_HISTORY ")
    _sb.AppendLine("    SET   TSMH_METER_INI_VALUE = CASE WHEN (@initial IS NULL) THEN TSMH_METER_INI_VALUE ELSE @initial END ")
    _sb.AppendLine("        , TSMH_METER_FIN_VALUE = CASE WHEN (@final IS NULL) THEN TSMH_METER_FIN_VALUE ELSE @final END ")
    _sb.AppendLine("        , TSMH_METER_INCREMENT = CASE WHEN (@increment IS NULL) THEN TSMH_METER_INCREMENT ELSE @increment END ")
    _sb.AppendLine("  WHERE   TSMH_TERMINAL_ID = @terminal_id ")
    _sb.AppendLine("    AND   TSMH_METER_CODE = @meter_code ")
    _sb.AppendLine("    AND   TSMH_GAME_ID = 0 ")
    _sb.AppendLine("    AND   TSMH_DENOMINATION = 0 ")
    _sb.AppendLine("    AND   TSMH_TYPE = 20 ")
    _sb.AppendLine("    AND   TSMH_DATETIME = @time ")
    _sb.AppendLine("    END ")

    '_sb.AppendLine("    IF (@increment IS NOT NULL AND @meter_code IN (0, 1, 5, 6)) ")
    '_sb.AppendLine("    BEGIN ")

    '' Update machine_stats_per_hour
    '_sb.AppendLine("      IF NOT EXISTS   (SELECT 1 FROM MACHINE_STATS_PER_HOUR ")
    '_sb.AppendLine("      WHERE   MSH_TERMINAL_ID = @terminal_id ")
    '_sb.AppendLine("        AND   MSH_BASE_HOUR = @time) ")
    '_sb.AppendLine("      BEGIN ")
    '_sb.AppendLine("       INSERT   INTO MACHINE_STATS_PER_HOUR ")
    '_sb.AppendLine("              ( MSH_BASE_HOUR ")
    '_sb.AppendLine("              , MSH_TERMINAL_ID ")
    '_sb.AppendLine("              , MSH_PLAYED_COUNT ")
    '_sb.AppendLine("              , MSH_PLAYED_AMOUNT ")
    '_sb.AppendLine("              , MSH_WON_COUNT ")
    '_sb.AppendLine("              , MSH_WON_AMOUNT ")
    '_sb.AppendLine("              , MSH_TO_GM_COUNT ")
    '_sb.AppendLine("              , MSH_TO_GM_AMOUNT ")
    '_sb.AppendLine("              , MSH_FROM_GM_COUNT ")
    '_sb.AppendLine("              , MSH_FROM_GM_AMOUNT ")
    '_sb.AppendLine("            ) ")
    '_sb.AppendLine("       VALUES ")
    '_sb.AppendLine("              ( @time ")
    '_sb.AppendLine("              , @terminal_id ")
    '_sb.AppendLine("              , CASE WHEN (@meter_code = 5 AND @increment IS NOT NULL) THEN @increment ELSE 0 END ")
    '_sb.AppendLine("              , CASE WHEN (@meter_code = 0 AND @increment IS NOT NULL) THEN @increment ELSE 0 END ")
    '_sb.AppendLine("              , CASE WHEN (@meter_code = 6 AND @increment IS NOT NULL) THEN @increment ELSE 0 END ")
    '_sb.AppendLine("              , CASE WHEN (@meter_code = 1 AND @increment IS NOT NULL) THEN @increment ELSE 0 END ")
    '_sb.AppendLine("              , 0 ")
    '_sb.AppendLine("              , 0 ")
    '_sb.AppendLine("              , 0 ")
    '_sb.AppendLine("              , 0 ")
    '_sb.AppendLine("            ) ")
    '_sb.AppendLine("         END ")
    '_sb.AppendLine("         ELSE ")
    '_sb.AppendLine("        BEGIN ")
    '_sb.AppendLine("       UPDATE   MACHINE_STATS_PER_HOUR ")
    '_sb.AppendLine("          SET   MSH_PLAYED_COUNT = CASE WHEN (@meter_code = 5  AND @increment IS NOT NULL) THEN @increment ELSE MSH_PLAYED_COUNT END ")
    '_sb.AppendLine("              , MSH_PLAYED_AMOUNT = CASE WHEN (@meter_code = 0  AND @increment IS NOT NULL) THEN @increment ELSE MSH_PLAYED_AMOUNT END ")
    '_sb.AppendLine("              , MSH_WON_COUNT = CASE WHEN (@meter_code = 6  AND @increment IS NOT NULL) THEN @increment ELSE MSH_WON_COUNT END ")
    '_sb.AppendLine("              , MSH_WON_AMOUNT = CASE WHEN (@meter_code = 1  AND @increment IS NOT NULL) THEN @increment ELSE MSH_WON_AMOUNT END ")
    '_sb.AppendLine("        WHERE   MSH_TERMINAL_ID = @terminal_id ")
    '_sb.AppendLine("          AND   MSH_BASE_HOUR = @time ")
    '_sb.AppendLine("      END ")
    '_sb.AppendLine("    END ")

    Try
      Using _cmd As SqlCommand = New SqlCommand(_sb.ToString(), DbTrx.SqlTransaction.Connection, DbTrx.SqlTransaction)
        _cmd.Parameters.Add("@terminal_id", SqlDbType.Int).SourceColumn = "TERMINAL_ID"
        _cmd.Parameters.Add("@meter_code", SqlDbType.Int).SourceColumn = "METER_CODE"
        _cmd.Parameters.Add("@time", SqlDbType.DateTime).SourceColumn = "BASE_TIME"
        _cmd.Parameters.Add("@initial", SqlDbType.BigInt).SourceColumn = "INIT_VALUE"
        _cmd.Parameters.Add("@final", SqlDbType.BigInt).SourceColumn = "FINAL_VALUE"
        _cmd.Parameters.Add("@increment", SqlDbType.BigInt).SourceColumn = "INCREMENT_VALUE"

        Using _ad As SqlDataAdapter = New SqlDataAdapter()

          _ad.InsertCommand = _cmd
          _ad.UpdateBatchSize = 500
          _ad.InsertCommand.UpdatedRowSource = UpdateRowSource.None

          _num_rows_inserted = _ad.Update(ModifiedRows)
          If _num_rows_inserted = ModifiedRows.Rows.Count Then

            Return True
          End If
        End Using

      End Using

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return False

  End Function

  Private Sub DB_LoadGroupMeters()
    Dim _str_sql As StringBuilder
    Dim _dt_counters As DataTable
    Dim _group_id As Integer
    Dim _new_meter As Meter
    Dim _new_meters_group As MetersGroup

    _str_sql = New StringBuilder()
    _dt_counters = New DataTable()
    _group_id = 0

    Try

      Using _db_trx As DB_TRX = New DB_TRX()

        _str_sql.AppendLine("  SELECT   DISTINCT SMCG_GROUP_ID                       ")
        _str_sql.AppendLine("         , SMCG_METER_CODE                     ")
        _str_sql.AppendLine("         , SMC_DESCRIPTION                     ")
        _str_sql.AppendLine("         , SMG_NAME                            ")
        _str_sql.AppendLine("         , ISNULL(SMC_NAME, '') AS CATALOG_NAME")
        _str_sql.AppendLine("    FROM   SAS_METERS_CATALOG_PER_GROUP ")
        _str_sql.AppendLine("   INNER   JOIN SAS_METERS_CATALOG ON SMC_METER_CODE = SMCG_METER_CODE ")
        _str_sql.AppendLine("   INNER   JOIN SAS_METERS_GROUPS ON SMCG_GROUP_ID = SMG_GROUP_ID ")
        _str_sql.AppendLine("   WHERE   SMCG_GROUP_ID > 10000 ")
        _str_sql.AppendLine("     AND   SMCG_METER_CODE IN ")
        _str_sql.AppendLine("           ( ")
        _str_sql.AppendLine("           SELECT   SMCG_METER_CODE  ")
        _str_sql.AppendLine("             FROM   SAS_METERS_CATALOG_PER_GROUP ")
        _str_sql.AppendLine("            WHERE   SMCG_GROUP_ID = 3 ")
        _str_sql.AppendLine("           ) ")
        _str_sql.AppendLine("ORDER BY   SMCG_GROUP_ID ")

        m_group_meter_list = New List(Of MetersGroup)
        m_meter_name = New Dictionary(Of Int32, String)

        Using _sql_cmd As SqlCommand = New SqlCommand(_str_sql.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          Using _sql_adapter As SqlDataAdapter = New SqlDataAdapter(_sql_cmd)

            If _sql_adapter.Fill(_dt_counters) > 0 Then
              For Each _row As DataRow In _dt_counters.Rows

                If _group_id <> CInt(_row(0)) Then
                  _group_id = _row(0) ' GroupId

                  _new_meters_group = New MetersGroup()
                  _new_meters_group.MeterList = New List(Of Meter)
                  _new_meters_group.Id = _group_id
                  _new_meters_group.GroupName = _row(3) 'GroupName
                  _new_meters_group.Type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3482) ' Meter

                  m_group_meter_list.Add(_new_meters_group)
                End If

                _new_meter = New Meter()
                _new_meter.Code = _row(1) ' MeterCode
                _new_meter.Description = _row(2) ' MeterDescription
                _new_meter.HexCode = Hex(_new_meter.Code)

                If _new_meter.Code > EVENT_START_BASE Then
                  _new_meter.CatalogName = IIf(String.IsNullOrEmpty(_row(4)), GLB_NLS_GUI_PLAYER_TRACKING.GetString((_new_meter.Code - EVENT_START_BASE) + NLS_EVENTS), _row(4)) ' Catalog Name
                  m_group_meter_list(m_group_meter_list.Count - 1).Type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3483) ' Event
                Else
                  _new_meter.CatalogName = IIf(String.IsNullOrEmpty(_row(4)), WSI.Common.Resource.String(String.Format("STR_SAS_METER_DESC_{0:00000}", _new_meter.Code)), _row(4))
                End If

                m_group_meter_list(m_group_meter_list.Count - 1).MeterList.Add(_new_meter)
                m_meter_name.Add(_new_meter.Code, _new_meter.CatalogName)

              Next
            End If
          End Using
        End Using
      End Using

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try
  End Sub

  Private Sub DB_LoadMeters()
    Dim _dt_counters As DataTable
    Dim _group_id As Integer
    Dim _new_meter As Meter

    _dt_counters = New DataTable()
    _group_id = 0
    m_meter_list = New List(Of Meter)

    Try

      Using _db_trx As DB_TRX = New DB_TRX()

        _dt_counters = WSI.Common.TerminalMetersInfo.ReadSASMeters(_db_trx.SqlTransaction, Me.MetersSelected)

        If _dt_counters.Rows.Count > 0 Then

          For Each _row As DataRow In _dt_counters.Rows

            _new_meter = New Meter()
            _new_meter.Code = _row(1) ' MeterCode
            _new_meter.Description = _row(2) ' MeterDescription
            _new_meter.HexCode = Hex(_new_meter.Code)

            If _new_meter.Code > EVENT_START_BASE Then
              _new_meter.CatalogName = IIf(String.IsNullOrEmpty(_row(4)), GLB_NLS_GUI_PLAYER_TRACKING.GetString((_new_meter.Code - EVENT_START_BASE) + NLS_EVENTS), _row(4)) ' Catalog Name
              m_group_meter_list(m_group_meter_list.Count - 1).Type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3483) ' Event
            Else
              _new_meter.CatalogName = IIf(String.IsNullOrEmpty(_row(4)),
                                          WSI.Common.Resource.String(String.Format("STR_SAS_METER_DESC_{0:00000}", _new_meter.Code)), _row(4))
            End If

            m_meter_list.Add(_new_meter)

          Next

        End If

      End Using

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try
  End Sub

  Private Function BD_GetAlarms(ByVal TerminalId As Long, ByVal BaseHour As DateTime) As DataTable
    Dim _table As DataTable
    Dim _str_sql As StringBuilder

    _str_sql = New StringBuilder()
    _table = New DataTable()

    _str_sql.AppendLine("    SELECT   AL_DATETIME                       ")
    _str_sql.AppendLine("           , ALC_NAME                     ")
    _str_sql.AppendLine("           , AL_ALARM_DESCRIPTION                     ")
    _str_sql.AppendLine("      FROM   ALARMS ")
    _str_sql.AppendLine("INNER JOIN   ALARM_CATEGORIES ON ALC_CATEGORY_ID = AL_SOURCE_CODE")
    _str_sql.AppendLine("     WHERE   AL_SOURCE_ID = '" & TerminalId & "'")
    _str_sql.AppendLine("       AND   AL_DATETIME >= " & GUI_FormatDateDB(BaseHour.AddDays(-1)) & " ")
    _str_sql.AppendLine("       AND   AL_DATETIME < " & GUI_FormatDateDB(BaseHour.AddDays(1)) & " ")
    _str_sql.AppendLine("       AND   ALC_LANGUAGE_ID = " & Resource.LanguageId & " ")
    _str_sql.AppendLine("     ORDER   BY   AL_DATETIME DESC ")

    Using _db_trx As DB_TRX = New DB_TRX()
      Using _sql_cmd As SqlCommand = New SqlCommand(_str_sql.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
        Using _sql_adapter As SqlDataAdapter = New SqlDataAdapter(_sql_cmd)
          _sql_adapter.Fill(_table)
        End Using
      End Using

    End Using

    Return _table
  End Function

#End Region


#Region "Public Functions"
  Public Sub ReadTerminalSasMeters()
    Dim _terminal_max_meters As DataTable
    Dim _max_meter As MaxMeter
    Dim _list_max_meters As List(Of MaxMeter)
    Dim _terminal_id As Int32
    Dim _idx As Int32

    Using _db_trx As New DB_TRX()
      Me.m_terminal_sas_meters = TerminalSasMeters_DbRead()
      _terminal_max_meters = TerminalMaxMeters_DbRead(_db_trx)
    End Using

    m_meter_max_value = New Dictionary(Of Int32, List(Of MaxMeter))
    For Each _row As DataRow In _terminal_max_meters.Rows
      _list_max_meters = New List(Of MaxMeter)
      _terminal_id = _row(0)
      _idx = 1
      For Each _meter As Meter In m_meter_list
        _max_meter = New MaxMeter()
        _max_meter.Code = _meter.Code
        _max_meter.MaxValue = IIf(_row(_idx) Is DBNull.Value, 0, _row(_idx))
        _list_max_meters.Add(_max_meter)
        _idx += 1
      Next

      m_meter_max_value.Add(_terminal_id, _list_max_meters)
    Next

  End Sub

  Public Function Db_UpdateTerminalSasMeters(ByVal ModifiedRows As DataTable) As Boolean
    Using _db_trx As New DB_TRX()
      If TerminalSasMeters_DbUpdate(_db_trx, ModifiedRows) Then
        _db_trx.Commit()

        Return True
      End If

      Return False
    End Using
  End Function

  Public Sub LoadGroupMeters()

    DB_LoadGroupMeters()

  End Sub

  Public Sub LoadMeters()
    DB_LoadMeters()
  End Sub

  Public Function GetAlarms(ByVal TerminalId As Long, ByVal BaseHour As DateTime) As DataTable

    Return BD_GetAlarms(TerminalId, BaseHour)
  End Function

#End Region
End Class
