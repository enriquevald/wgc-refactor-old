'-------------------------------------------------------------------
' Copyright � 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : cls_dealer_copy_configuration.vb
'
' DESCRIPTION : Dealer copy configuration data class
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------- ------ -----------------------------------------------
' 05-JUL-2017 RAB    Initial version
' 06-JUL-2017 RAB    PBI 28421: WIGOS-1425 MES10 Ticket validation - Dealer copy tickets configuration
'--------------------------------------------------------------------

Imports GUI_CommonOperations
Imports GUI_Controls
Imports WSI.Common
Imports System.Data.SqlClient
Imports System.Diagnostics
Imports GUI_Controls.CLASS_GUI_USER

Public Class CLASS_DEALER_COPY_CONFIGURATION
  Inherits CLASS_BASE

#Region " Structures "

  Public Class TYPE_DEALER_COPY_CONFIGURATION
    Public cage_collection_enabled As Boolean
    Public ticket_report_enabled As Boolean
    Public tickets_audit_enabled As Boolean
    Public soft_count_enabled As Boolean
    Public redeem_tickets_enabled As Boolean
    Public validation_enabled As Boolean
    Public offline_enabled As Boolean
    Public reconcile_enabled As Boolean

    Sub New()
      Me.cage_collection_enabled = False
      Me.ticket_report_enabled = False
      Me.tickets_audit_enabled = False
      Me.soft_count_enabled = False
      Me.redeem_tickets_enabled = False
      Me.validation_enabled = False
      Me.offline_enabled = False
    End Sub
  End Class

#End Region ' Structures

#Region " Members "

  Protected m_dealer_copy_configuration As New TYPE_DEALER_COPY_CONFIGURATION

#End Region    ' Members

#Region " Properties "

  Public Property CageCollectionEnabled() As Boolean
    Get
      Return m_dealer_copy_configuration.cage_collection_enabled
    End Get
    Set(value As Boolean)
      m_dealer_copy_configuration.cage_collection_enabled = value
    End Set
  End Property

  Public Property TicketReportEnabled() As Boolean
    Get
      Return m_dealer_copy_configuration.ticket_report_enabled
    End Get
    Set(value As Boolean)
      m_dealer_copy_configuration.ticket_report_enabled = value
    End Set
  End Property

  Public Property TicketsAuditEnabled() As Boolean
    Get
      Return m_dealer_copy_configuration.tickets_audit_enabled
    End Get
    Set(value As Boolean)
      m_dealer_copy_configuration.tickets_audit_enabled = value
    End Set
  End Property

  Public Property SoftCountEnabled() As Boolean
    Get
      Return m_dealer_copy_configuration.soft_count_enabled
    End Get
    Set(value As Boolean)
      m_dealer_copy_configuration.soft_count_enabled = value
    End Set
  End Property

  Public Property RedeemTicketsEnabled() As Boolean
    Get
      Return m_dealer_copy_configuration.redeem_tickets_enabled
    End Get
    Set(value As Boolean)
      m_dealer_copy_configuration.redeem_tickets_enabled = value
    End Set
  End Property

  Public Property ValidationEnabled() As Boolean
    Get
      Return m_dealer_copy_configuration.validation_enabled
    End Get
    Set(value As Boolean)
      m_dealer_copy_configuration.validation_enabled = value
    End Set
  End Property

  Public Property OfflineEnabled() As Boolean
    Get
      Return m_dealer_copy_configuration.offline_enabled
    End Get
    Set(value As Boolean)
      m_dealer_copy_configuration.offline_enabled = value
    End Set
  End Property

  Public Property ReconcileEnabled() As Boolean
    Get
      Return m_dealer_copy_configuration.reconcile_enabled
    End Get
    Set(value As Boolean)
      m_dealer_copy_configuration.reconcile_enabled = value
    End Set
  End Property

#End Region   ' Properties

#Region " Overrides "

  ''' <summary>
  ''' Returns the related auditor data for the current object
  ''' </summary>
  ''' <returns>The newly created audit object</returns>
  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA
    Dim _auditor_data As CLASS_AUDITOR_DATA

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_CASHIER_CONFIGURATION)
    _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(8460), "")

    Call _auditor_data.SetField(0, IIf(Me.CageCollectionEnabled, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)),
                                GLB_NLS_GUI_PLAYER_TRACKING.GetString(8461) + "." + GLB_NLS_GUI_PLAYER_TRACKING.GetString(1819) + "." + GLB_NLS_GUI_PLAYER_TRACKING.GetString(8462))

    Call _auditor_data.SetField(0, IIf(Me.TicketReportEnabled, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)),
                                GLB_NLS_GUI_PLAYER_TRACKING.GetString(8461) + "." + GLB_NLS_GUI_PLAYER_TRACKING.GetString(1819) + "." + GLB_NLS_GUI_PLAYER_TRACKING.GetString(2124))

    Call _auditor_data.SetField(0, IIf(Me.TicketsAuditEnabled, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)),
                                GLB_NLS_GUI_PLAYER_TRACKING.GetString(8461) + "." + GLB_NLS_GUI_PLAYER_TRACKING.GetString(1819) + "." + GLB_NLS_GUI_PLAYER_TRACKING.GetString(7674))

    Call _auditor_data.SetField(0, IIf(Me.SoftCountEnabled, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)),
                                GLB_NLS_GUI_PLAYER_TRACKING.GetString(8461) + "." + GLB_NLS_GUI_PLAYER_TRACKING.GetString(1819) + "." + GLB_NLS_GUI_PLAYER_TRACKING.GetString(5638))

    Call _auditor_data.SetField(0, IIf(Me.RedeemTicketsEnabled, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)),
                                GLB_NLS_GUI_PLAYER_TRACKING.GetString(8461) + "." + GLB_NLS_GUI_PLAYER_TRACKING.GetString(6616) + "." + GLB_NLS_GUI_PLAYER_TRACKING.GetString(8463))

    Call _auditor_data.SetField(0, IIf(Me.ValidationEnabled, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)),
                                GLB_NLS_GUI_PLAYER_TRACKING.GetString(8461) + "." + GLB_NLS_GUI_PLAYER_TRACKING.GetString(6616) + "." + GLB_NLS_GUI_PLAYER_TRACKING.GetString(2710))

    Call _auditor_data.SetField(0, IIf(Me.OfflineEnabled, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)),
                                GLB_NLS_GUI_PLAYER_TRACKING.GetString(8461) + "." + GLB_NLS_GUI_PLAYER_TRACKING.GetString(6616) + "." + GLB_NLS_GUI_PLAYER_TRACKING.GetString(3327))

    Call _auditor_data.SetField(0, IIf(Me.ReconcileEnabled, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)),
                                GLB_NLS_GUI_PLAYER_TRACKING.GetString(4600) + "." + GLB_NLS_GUI_PLAYER_TRACKING.GetString(1819) + "." + GLB_NLS_GUI_PLAYER_TRACKING.GetString(6525))

    Return _auditor_data
  End Function ' AuditorData

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
  End Function ' DB_Delete

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
  End Function ' DB_Insert

  ''' <summary>
  ''' Reads from the database into the given object
  ''' </summary>
  ''' <param name="ObjectId">An object, it must be 'casted' to the desired KEY</param>
  ''' <param name="SqlCtx">Sql Transaction</param>
  ''' <returns>
  '''    - STATUS_OK
  '''    - STATUS_ERROR
  ''' </returns>
  ''' <remarks></remarks>
  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _rc As ENUM_STATUS
    _rc = ENUM_STATUS.STATUS_ERROR

    If DealerCopyConfiguration_DbRead() Then
      _rc = ENUM_STATUS.STATUS_OK
    End If

    Return _rc
  End Function ' DB_Read

  ''' <summary>
  ''' Updates the given object to the database
  ''' </summary>
  ''' <param name="SqlCtx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _rc As ENUM_STATUS
    _rc = ENUM_STATUS.STATUS_ERROR

    If DealerCopyConfiguration_DbUpdate() Then
      _rc = ENUM_STATUS.STATUS_OK
    End If

    Return _rc
  End Function ' DB_Update

  ''' <summary>
  ''' Returns a duplicate of the given object
  ''' </summary>
  ''' <returns>The newly created object</returns>
  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE
    Dim _target As CLASS_DEALER_COPY_CONFIGURATION
    _target = New CLASS_DEALER_COPY_CONFIGURATION

    _target.m_dealer_copy_configuration.cage_collection_enabled = Me.CageCollectionEnabled
    _target.m_dealer_copy_configuration.ticket_report_enabled = Me.TicketReportEnabled
    _target.m_dealer_copy_configuration.tickets_audit_enabled = Me.TicketsAuditEnabled
    _target.m_dealer_copy_configuration.soft_count_enabled = Me.SoftCountEnabled
    _target.m_dealer_copy_configuration.redeem_tickets_enabled = Me.RedeemTicketsEnabled
    _target.m_dealer_copy_configuration.validation_enabled = Me.ValidationEnabled
    _target.m_dealer_copy_configuration.offline_enabled = Me.OfflineEnabled
    _target.m_dealer_copy_configuration.reconcile_enabled = Me.ReconcileEnabled

    Return _target
  End Function ' Duplicate

#End Region ' Overrides

#Region " Private Functions "
  ''' <summary>
  ''' Update the database General_Params
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function DealerCopyConfiguration_DbUpdate() As Boolean
    Try
      Using _db_trx As New DB_TRX

        'General Params cage collection
        If Not DB_GeneralParam_Update("DealerCopy.Configuration", "CageCollection", IIf(Me.CageCollectionEnabled, 1, 0), _db_trx.SqlTransaction) Then
          Return False
        End If

        'General Params ticket report
        If Not DB_GeneralParam_Update("DealerCopy.Configuration", "TicketReport", IIf(Me.TicketReportEnabled, 1, 0), _db_trx.SqlTransaction) Then
          Return False
        End If

        'General Params tickets audit report
        If Not DB_GeneralParam_Update("DealerCopy.Configuration", "TicketsAuditReport", IIf(Me.TicketsAuditEnabled, 1, 0), _db_trx.SqlTransaction) Then
          Return False
        End If

        'General Params soft count
        If Not DB_GeneralParam_Update("DealerCopy.Configuration", "SoftCount", IIf(Me.SoftCountEnabled, 1, 0), _db_trx.SqlTransaction) Then
          Return False
        End If

        'General Params redeem ticket
        If Not DB_GeneralParam_Update("DealerCopy.Configuration", "Redeem", IIf(Me.RedeemTicketsEnabled, 1, 0), _db_trx.SqlTransaction) Then
          Return False
        End If

        'General Params validation ticket
        If Not DB_GeneralParam_Update("DealerCopy.Configuration", "Validation", IIf(Me.ValidationEnabled, 1, 0), _db_trx.SqlTransaction) Then
          Return False
        End If

        'General Params offline ticket
        If Not DB_GeneralParam_Update("DealerCopy.Configuration", "Offline", IIf(Me.OfflineEnabled, 1, 0), _db_trx.SqlTransaction) Then
          Return False
        End If

        'General Params reconcile ticket
        If Not DB_GeneralParam_Update("DealerCopy.Configuration", "Reconcile", IIf(Me.ReconcileEnabled, 1, 0), _db_trx.SqlTransaction) Then
          Return False
        End If

        _db_trx.Commit()
      End Using

      Return True
    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return False
  End Function ' DealerCopyConfiguration_DbUpdate

  ''' <summary>
  ''' Get all dealer copy configuration general params
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function DealerCopyConfiguration_DbRead() As Boolean
    Dim _general_params As WSI.Common.GeneralParam.Dictionary

    Try
      _general_params = WSI.Common.GeneralParam.Dictionary.Create()

      Me.CageCollectionEnabled = _general_params.GetBoolean("DealerCopy.Configuration", "CageCollection", False)
      Me.TicketReportEnabled = _general_params.GetBoolean("DealerCopy.Configuration", "TicketReport", False)
      Me.TicketsAuditEnabled = _general_params.GetBoolean("DealerCopy.Configuration", "TicketsAuditReport", False)
      Me.SoftCountEnabled = _general_params.GetBoolean("DealerCopy.Configuration", "SoftCount", False)
      Me.RedeemTicketsEnabled = _general_params.GetBoolean("DealerCopy.Configuration", "Redeem", False)
      Me.ValidationEnabled = _general_params.GetBoolean("DealerCopy.Configuration", "Validation", False)
      Me.OfflineEnabled = _general_params.GetBoolean("DealerCopy.Configuration", "Offline", False)
      Me.ReconcileEnabled = _general_params.GetBoolean("DealerCopy.Configuration", "Reconcile", False)

      Return True
    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return False
  End Function ' DealerCopyConfiguration_DbRead

#End Region

End Class
