'-------------------------------------------------------------------
' Copyright � 2014 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : cls_window_menu.vb
'
' DESCRIPTION : Class inherits CLASS_MENU_ITEM special for "Window" menu feature.
' AUTHOR:        Luis Mesa
' CREATION DATE: 25-NOV-2014
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 25-NOV-2014  LEM    Initial version
' 12-FEB-2015  FJC    New GUI Menu
' 30-NOV-2017  RGR    PBI 30932:WIGOS-6393 GUI - Menu: User Interface improvements
'--------------------------------------------------------------------

Imports GUI_Controls

Public Class CLASS_WINDOW_MENU
  Inherits CLASS_MENU_ITEM

#Region "Constants"

  Private Const TAG_POSITION_STRIP = "PS"

#End Region

#Region "Members"

  Private m_static_items_count As Int32
  Private m_click_event As EventHandler
  Private m_previous_windows_state As FormWindowState

#End Region

#Region "Constructor / Destructor"

  Public Sub New(ByVal PopupEvent As EventHandler, _
                 ByVal ClickEvent As EventHandler, _
                 Optional ByVal ImageItem As Image = Nothing)

    MyBase.New(ENUM_MENU_ITEM_TYPE.ROOT_ITEM)

    m_click_event = ClickEvent

    MenuItem.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5755)
    MenuItem.Name = CLASS_MENU_CUSTOMIZE.KEY_ID_MENU_ITEM
    MenuItem.Enabled = True

    If Not CLASS_MENU_CUSTOMIZE.m_font Is Nothing Then
      MenuItem.Font = CLASS_MENU_CUSTOMIZE.m_font
    End If

    MenuItem.TextImageRelation = TextImageRelation.ImageAboveText

    If Not ImageItem Is Nothing Then
      MenuItem.Tag = ImageItem
      If CLASS_MENU_CUSTOMIZE.m_show_icons Then
        MenuItem.Image = ImageItem
      End If
    End If

    'FJC 30-12-2014
    ''AddHandler Menu.Popup, PopupEvent
    AddHandler MenuItem.DropDownOpening, PopupEvent

    Call CreateStaticItems()

  End Sub
#End Region

#Region "Public Methods"

  Public Sub OnPopup(ByVal MainForm As Form)
    Dim _screens() As Form
    Dim _id As Int32

    _screens = MainForm.MdiChildren

    If m_static_items_count = 0 Then
      Call CreateStaticItems()
    End If

    Call ResetMenu(_screens.Length > 0)

    If _screens.Length > 0 Then
      Me.AddBreakItem()

      _id = 1
      For Each _item As frm_base In _screens
        AddMenuItem(_id & " " & _item.FormTitle, True)
        _id += 1
      Next

    End If
  End Sub

  Public Sub OnClick(ByVal ClickedIndex As Int32, ByVal MainForm As Form)
    Dim _screens() As Form
    Dim _idx As Int32
    Dim _first_idx As Int32

    _screens = MainForm.MdiChildren
    _idx = ClickedIndex

    'FJC 30-12-2014
    '_first_idx = Me.Menu.MenuItems.Count - _screens.Length
    _first_idx = Me.MenuItem.DropDownItems.Count - _screens.Length

    If _idx >= _first_idx Then
      If _screens(_idx - _first_idx).WindowState = FormWindowState.Minimized Then
        _screens(_idx - _first_idx).WindowState = FormWindowState.Normal
      End If
      _screens(_idx - _first_idx).BringToFront()
    Else
      Select Case _idx
        Case 0 ' Close All
          For Each _frm As frm_base In _screens
            _frm.Close()
          Next

        Case 1 ' Minimize All
          For Each _frm As frm_base In _screens
            m_previous_windows_state = _frm.WindowState
            _frm.WindowState = FormWindowState.Minimized
          Next

        Case 2 ' Restore All
          For Each _frm As frm_base In _screens
            _frm.WindowState = m_previous_windows_state
          Next

        Case 4 ' Cascade
          Call ShowCascadeLayout(MainForm)

        Case 5 ' Parallel
          Call ShowParallelLayout(MainForm)

      End Select

    End If
  End Sub

#End Region

#Region "Private Methods"

  Private Sub CreateStaticItems()
    'FJC 30-12-2014
    ''Me.Menu.MenuItems.Clear()
    Me.MenuItem.DropDownItems.Clear()

    ' Close all
    Me.AddMenuItem(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5756), False)

    ' Minmize all
    Me.AddMenuItem(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5757), False)

    ' Restore all
    Me.AddMenuItem(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5758), False)

    ' Break
    Me.AddBreakItem()

    ' Cascade
    Me.AddMenuItem(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5759), False)

    ' Parallel
    Me.AddMenuItem(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5760), False)

    'FJC 30-12-2014
    'm_static_items_count = Me.Menu.MenuItems.Count
    m_static_items_count = Me.MenuItem.DropDownItems.Count

  End Sub

  Private Sub ResetMenu(ByVal Enabled As Boolean)
    'FJC 30-12-2014
    'While Me.Menu.MenuItems.Count > m_static_items_count
    '  Me.Menu.MenuItems.RemoveAt(Me.Menu.MenuItems.Count - 1)
    'End While

    'For Each _item As MenuItem In Me.Menu.MenuItems
    '  _item.Enabled = Enabled
    'Next

    While Me.MenuItem.DropDownItems.Count > m_static_items_count
      Me.MenuItem.DropDownItems.RemoveAt(Me.MenuItem.DropDownItems.Count - 1)
    End While

    For Each _item As Object In Me.MenuItem.DropDownItems
      If TypeOf _item Is ToolStripDropDownItem Then
        If _item.tag <> TAG_POSITION_STRIP Then
          _item.Enabled = Enabled
        End If
      End If
    Next

  End Sub

  Private Sub AddBreakItem()
    Dim _item_sep As ToolStripSeparator

    _item_sep = New ToolStripSeparator


    'FJC 30-12-2014
    '_item.Menu.Text = "-"
    'Me.Menu.MenuItems.Add(_item.Menu)

    If Not IsNothing(MenuItem) Then
      Me.MenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {_item_sep})
    End If


  End Sub

  Private Sub AddMenuItem(ByVal Title As String, _
                          ByVal Enabled As Boolean, _
                          ByVal Tag As String)

    Dim _item As CLASS_MENU_ITEM

    _item = New CLASS_MENU_ITEM(ENUM_MENU_ITEM_TYPE.NORMAL_ITEM)

    _item.MenuItem.Text = Title
    _item.MenuItem.Enabled = Enabled
    _item.MenuItem.Tag = Tag

    AddHandler _item.MenuItem.Click, m_click_event

    'FJC 30-12-2014
    'Me.Menu.MenuItems.Add(_item.Menu)
    Me.MenuItem.DropDownItems.Add(_item.MenuItem)



  End Sub

  Private Sub AddMenuItem(ByVal Title As String, _
                          ByVal Enabled As Boolean)

    Me.AddMenuItem(Title, Enabled, String.Empty)
  End Sub

  Private Sub ShowCascadeLayout(ByVal MainForm As Form)
    Dim _size As Size
    Dim _top As Int32
    Dim _left As Int32
    Dim _active As Form

    _size = New Size(1100, 700)
    _top = 10
    _left = 10

    _active = MainForm.ActiveMdiChild
    _active.WindowState = FormWindowState.Normal
    _active.Size = _size

    For Each _frm As frm_base In MainForm.MdiChildren
      If _frm.Name <> _active.Name Then
        _frm.WindowState = FormWindowState.Normal
        _frm.Size = _size
        _frm.Top = _top
        _frm.Left = _left
        _frm.BringToFront()

        _top += 30
        _left += 25
      End If
    Next

    _active.Top = _top
    _active.Left = _left

    _active.BringToFront()
  End Sub

  Private Sub ShowParallelLayout(ByVal MainForm As Form)
    Dim _screens() As Form
    Dim _columns As Int32
    Dim _rows As Int32
    Dim _size As Size
    Dim _row As Int32
    Dim _column As Int32
    Dim _remain As Int32

    Dim _size_menu_strip As Size
    Dim _position_menu_strip As DockStyle
    Dim _off_set_width As Integer
    Dim _off_set_height As Integer

    _screens = MainForm.MdiChildren
    _columns = Math.Ceiling(Math.Sqrt(_screens.Length))
    _rows = IIf((_columns - 1) * _columns >= _screens.Length, _columns - 1, _columns)

    'Get Size and Position of the MenuStrip
    GetSizeMenuStripPosition(MainForm, _size_menu_strip, _position_menu_strip)

    'TODO
    _off_set_width = 25
    _off_set_height = 48
    '_off_set_width = (System.Windows.Forms.SystemInformation.Border3DSize.Width * 4) + _
    '                     (MainForm.Padding.Horizontal * 4)
    '_off_set_height = (System.Windows.Forms.SystemInformation.Border3DSize.Height * 4) + _
    '                         (MainForm.Padding.Vertical * 4)

    Select Case _position_menu_strip
      Case DockStyle.Top, _
           DockStyle.Bottom

        _size = New Size(Math.Floor((MainForm.Width - _off_set_width) / _columns), _
                         Math.Floor((MainForm.Height - _size_menu_strip.Height - _off_set_height) / _rows))
      Case DockStyle.Left, _
           DockStyle.Right

        _size = New Size(Math.Floor((MainForm.Width - _size_menu_strip.Width - _off_set_width) / _columns), _
                         Math.Floor((MainForm.Height - _off_set_height) / _rows))
    End Select

    _row = 0
    _column = 0
    _remain = _screens.Length

    MainForm.ActiveMdiChild.Top = 0
    MainForm.ActiveMdiChild.Left = 0

    For Each _frm As frm_base In _screens
      If _column >= _columns Then
        _row += 1
        _column = 0
        If _remain < _columns Then
          Select Case _position_menu_strip
            Case DockStyle.Top, _
                 DockStyle.Bottom

              _size.Width = (MainForm.Width - _off_set_width) / _remain

            Case DockStyle.Left, _
                 DockStyle.Right

              _size.Width = (MainForm.Width - _size_menu_strip.Width - _off_set_width) / _remain
          End Select
        End If
      End If

      _frm.Top = _row * _size.Height
      _frm.Left = _column * _size.Width

      _column += 1

      _frm.WindowState = FormWindowState.Normal
      _frm.Size = _size

      _remain -= 1
    Next
  End Sub

  ' PURPOSE: Function for getting Size and Position of a MenuStrip
  '
  '  PARAMS:
  '     - INPUT: 
  '               MainForm:  Form
  '     - OUTPUT:
  '               Size:      (Height and Width)
  '               Position:  Windows.Forms.DockStyle (Top, Bottom, Left, Right)
  '
  ' RETURNS:
  Private Sub GetSizeMenuStripPosition(ByVal MainForm As Form, _
                                       ByRef SizeMenuStrip As System.Drawing.Size, _
                                       ByRef PositionMenuStrip As DockStyle)

    For Each _ctrl As Control In MainForm.Controls
      If TypeOf _ctrl Is MenuStrip Then
        SizeMenuStrip = _ctrl.Size
        PositionMenuStrip = _ctrl.Dock

        Exit For
      End If
    Next
  End Sub

#End Region

  Protected Overrides Sub Finalize()
    MyBase.Finalize()
  End Sub
End Class
