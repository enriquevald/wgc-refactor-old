'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : cls_users_assignatin.vb
'
' DESCRIPTION : area class for user edition
'              
' REVISION HISTORY :
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 21-FEB-2012  SSC    Initial version
'-------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports System.Text
Imports WSI.Common

Public Class CLASS_USERS_ASSIGNATION
  Inherits CLASS_BASE

#Region " Constants "

  ' Fields length
  Public Const MAX_NAME_LEN As Integer = 50

#End Region

#Region " Enums "


#End Region ' Enums 

#Region " Structures "

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_USERS_ASSIGNATION
    Public user_id As Integer
    Public name As String
  End Class

#End Region

#Region " Members "

  Protected m_area As New TYPE_USERS_ASSIGNATION

#End Region

#Region " Properties "

  Public Property UserId() As Integer
    Get
      Return m_area.user_id

    End Get

    Set(ByVal Value As Integer)
      m_area.user_id = Value
    End Set
  End Property

  Public Property Name() As String
    Get
      Return m_area.name
    End Get

    Set(ByVal Value As String)
      m_area.name = Value
    End Set
  End Property

#End Region ' Structures

#Region " Overrides functions "

  '----------------------------------------------------------------------------
  ' PURPOSE : Reads from the database into the given object
  '
  ' PARAMS :
  '     - INPUT :
  '         - ObjectId: An object, it must be 'casted' to the desired KEY.
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '
  ' NOTES :

  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As Integer

    Me.UserId = ObjectId

    ' Read area data
    _rc = ReadRunners(m_area, SqlCtx)

    Select Case _rc

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

    Return CLASS_BASE.ENUM_STATUS.STATUS_OK

  End Function ' DB_Read

  '----------------------------------------------------------------------------
  ' PURPOSE : Updates the given object to the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES :

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    'Dim _rc As Integer

    '' Update area data
    '_rc = UpdateArea(m_area, SqlCtx)

    'Select Case _rc
    '  Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
    '    Call TerminalReport.ForceRefresh()
    '    Return CLASS_BASE.ENUM_STATUS.STATUS_OK

    '  Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
    '    Return ENUM_STATUS.STATUS_NOT_FOUND

    '  Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
    '    Return ENUM_STATUS.STATUS_DUPLICATE_KEY

    '  Case Else
    '    Return ENUM_STATUS.STATUS_ERROR

    'End Select

    Return CLASS_BASE.ENUM_STATUS.STATUS_OK

  End Function ' DB_Update

  '----------------------------------------------------------------------------
  ' PURPOSE : Inserts the given object to the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES :

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    'Dim _rc As Integer

    '' Insert area data
    '_rc = InsertArea(m_area, SqlCtx)

    'Select Case _rc
    '  Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
    '    Return CLASS_BASE.ENUM_STATUS.STATUS_OK

    '  Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
    '    Return ENUM_STATUS.STATUS_DUPLICATE_KEY

    '  Case Else
    '    Return ENUM_STATUS.STATUS_ERROR

    'End Select

    Return CLASS_BASE.ENUM_STATUS.STATUS_OK

  End Function ' DB_Insert

  '----------------------------------------------------------------------------
  ' PURPOSE : Deletes the given object from the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DEPENDENCIES
  '
  ' NOTES :

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    'Dim _rc As Integer

    '' Delete area data
    '_rc = DeleteArea(Me.UserId, SqlCtx)

    'Select Case _rc
    '  Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
    '    Return CLASS_BASE.ENUM_STATUS.STATUS_OK

    '  Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
    '    Return ENUM_STATUS.STATUS_NOT_FOUND

    '  Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DEPENDENCIES
    '    Return ENUM_STATUS.STATUS_DEPENDENCIES

    '  Case Else
    '    Return ENUM_STATUS.STATUS_ERROR

    'End Select

    Return CLASS_BASE.ENUM_STATUS.STATUS_OK

  End Function ' DB_Delete

  '----------------------------------------------------------------------------
  ' PURPOSE : Returns a duplicate of the given object.
  '
  ' PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - The newly created object
  '
  ' NOTES :

  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE
    Dim temp_runners As CLASS_USERS_ASSIGNATION

    temp_runners = New CLASS_USERS_ASSIGNATION
    temp_runners.UserId = Me.UserId
    temp_runners.Name = Me.Name

    Return temp_runners

  End Function ' Duplicate

  '----------------------------------------------------------------------------
  ' PURPOSE : Returns the related auditor data for the current object.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - The newly created audit object
  '
  ' NOTES :

  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim _auditor_data As CLASS_AUDITOR_DATA

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_USERS_ASSIGNATION)
    Call _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(8768), String.Empty)

    'Runners
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8767), IIf(Me.Name = "", AUDIT_NONE_STRING, Me.Name))

    Return _auditor_data

  End Function ' AuditorData

#End Region

#Region " Private Functions "

  '----------------------------------------------------------------------------
  ' PURPOSE : Reads a area object from DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - Item.area_id
  '         - Context
  '
  '     - OUTPUT :
  '         - Item
  '
  ' RETURNS :
  '     - CONFIGURATION_OK
  '     - CONFIGURATION_ERROR_DB
  '
  ' NOTES :

  Private Function ReadRunners(ByVal Item As TYPE_USERS_ASSIGNATION, ByVal Context As Integer) As Integer

    Dim _sql_query As String
    Dim _data_table As DataTable

    '_sql_query = "SELECT AR_NAME" _
    '                + ", AR_SMOKING" _
    '            + " FROM AREAS" _
    '            + " WHERE AR_AREA_ID = " + Item.user_id.ToString()

    '_data_table = GUI_GetTableUsingSQL(_sql_query, 5000)
    'If IsNothing(_data_table) Then
    '  Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    'End If

    'If _data_table.Rows.Count() <> 1 Then
    '  Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    'End If

    Item.name = "AA"


  End Function ' ReadArea

  '----------------------------------------------------------------------------
  ' PURPOSE : Deletes a Area object from DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - AreaId
  '         - Context
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - CONFIGURATION_OK
  '     - CONFIGURATION_ERROR_DB
  '
  ' NOTES :

  Private Function DeleteArea(ByVal AreaId As Integer, _
                              ByVal Context As Integer) As Integer
    'Dim _sql_query As String
    'Dim db_count As Integer = 0
    'Dim _sql_tx As System.Data.SqlClient.SqlTransaction = Nothing
    'Dim data_table As DataTable

    ''     - Remove the Area item from the AREAS table
    'Try
    '  If Not GUI_BeginSQLTransaction(_sql_tx) Then
    '    Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    '  End If

    '  ' Before removing, check dependecies with other tables
    '  _sql_query = " SELECT  COUNT(*) " & _
    '               " FROM  BANKS " & _
    '               " WHERE  BK_AREA_ID = " & AreaId.ToString()

    '  data_table = GUI_GetTableUsingSQL(_sql_query, 5000)
    '  If IsNothing(data_table) Then
    '    GUI_EndSQLTransaction(_sql_tx, False)

    '    Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    '  End If

    '  db_count = data_table.Rows(0).Item(0)

    '  If db_count > 0 Then
    '    Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DEPENDENCIES
    '  End If

    '  _sql_query = "DELETE AREAS" _
    '             + " WHERE AR_AREA_ID = " + AreaId.ToString()

    '  If Not GUI_SQLExecuteNonQuery(_sql_query, _sql_tx) Then
    '    ' Rollback  
    '    GUI_EndSQLTransaction(_sql_tx, False)

    '    Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    '  End If

    '  ' Commit
    '  If Not GUI_EndSQLTransaction(_sql_tx, True) Then
    '    Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    '  End If

    'Catch ex As Exception
    '  ' Rollback  
    '  GUI_EndSQLTransaction(_sql_tx, False)

    '  Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    'End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

  End Function ' DeleteArea

  '----------------------------------------------------------------------------
  ' PURPOSE : Adds a Area object into DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - Item
  '         - Context
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - CONFIGURATION_OK
  '     - CONFIGURATION_ERROR_DB
  '
  ' NOTES :

  Private Function InsertArea(ByVal Item As TYPE_USERS_ASSIGNATION, _
                              ByVal Context As Integer) As Integer
    'Dim _sql_query As String
    'Dim _num_rows_inserted As Integer
    'Dim _sql_tx As System.Data.SqlClient.SqlTransaction = Nothing
    'Dim _sql_command As System.Data.SqlClient.SqlCommand = Nothing
    'Dim _data_table As DataTable

    '' Steps
    ''     - Check whether there is no other Area with the same name 
    ''     - Insert the Area item to the AREAS table

    'Try
    '  '     - Check whether there is no other area with the same name 
    '  _sql_query = "SELECT COUNT (*)" _
    '              + " FROM AREAS" _
    '              + " WHERE AR_NAME = '" + Item.name.Replace("'", "''") + "'"

    '  _data_table = GUI_GetTableUsingSQL(_sql_query, 5000)
    '  If Not IsNothing(_data_table) Then
    '    If _data_table.Rows.Count() > 0 Then
    '      If _data_table.Rows(0).Item(0) > 0 Then
    '        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
    '      End If
    '    End If
    '  End If

    '  If Not GUI_BeginSQLTransaction(_sql_tx) Then
    '    Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    '  End If

    '  '     - Insert the area item to the AREAS table
    '  _sql_query = "INSERT INTO AREAS" _
    '                      + " ( AR_NAME" _
    '                       + ", AR_SMOKING" _
    '                       + ")" _
    '                 + " VALUES" _
    '                     + " ( @name" _
    '                      + " ,@smoking" _
    '                      + ")"

    '  _sql_command = New SqlCommand(_sql_query)
    '  _sql_command.Transaction = _sql_tx
    '  _sql_command.Connection = _sql_tx.Connection

    '  _sql_command.Parameters.Add("@name", SqlDbType.NVarChar).Value = IIf(Item.name = "", DBNull.Value, Item.name)

    '  _num_rows_inserted = _sql_command.ExecuteNonQuery()

    '  If _num_rows_inserted <> 1 Then
    '    GUI_EndSQLTransaction(_sql_tx, False)

    '    Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    '  End If

    '  ' Commit
    '  If Not GUI_EndSQLTransaction(_sql_tx, True) Then
    '    Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    '  End If

    'Catch ex As Exception
    '  ' Rollback  
    '  GUI_EndSQLTransaction(_sql_tx, False)

    '  Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    'End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

  End Function ' InsertArea

  '----------------------------------------------------------------------------
  ' PURPOSE : Updates the given Area object into the DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - Item
  '         - Context
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - CONFIGURATION_OK
  '     - CONFIGURATION_ERROR_DB
  '
  ' NOTES :

  Private Function UpdateArea(ByVal Item As TYPE_USERS_ASSIGNATION, _
                              ByVal Context As Integer) As Integer
    'Dim _sql_query As String
    'Dim _sql_tx As System.Data.SqlClient.SqlTransaction = Nothing
    'Dim _sql_command As System.Data.SqlClient.SqlCommand = Nothing
    'Dim _num_rows_updated As Integer
    'Dim _data_table As DataTable

    '' Steps
    ''     - Check whether there is no other Area with the same name 
    ''     - Update the Area's data 

    'Try
    '  '   - Check whether there is no other Area with the same name 
    '  _sql_query = "SELECT COUNT (*)" _
    '              + " FROM AREAS" _
    '             + " WHERE AR_NAME = '" + Item.name.Replace("'", "''") + "'" _
    '               + " AND AR_AREA_ID <> " + Item.user_id.ToString()

    '  _data_table = GUI_GetTableUsingSQL(_sql_query, 5000)
    '  If Not IsNothing(_data_table) Then
    '    If _data_table.Rows.Count() > 0 Then
    '      If _data_table.Rows(0).Item(0) > 0 Then
    '        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
    '      End If
    '    End If
    '  End If

    '  '   - Update the area's data 
    '  If Not GUI_BeginSQLTransaction(_sql_tx) Then
    '    Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    '  End If

    '  _sql_query = "UPDATE AREAS" _
    '               + " SET AR_NAME = @name" _
    '                  + ", AR_SMOKING = @smoking" _
    '             + " WHERE AR_AREA_ID = @area_id"

    '  _sql_command = New SqlCommand(_sql_query)
    '  _sql_command.Transaction = _sql_tx
    '  _sql_command.Connection = _sql_tx.Connection

    '  _sql_command.Parameters.Add("@name", SqlDbType.NVarChar).Value = IIf(Item.name = "", DBNull.Value, Item.name)

    '  _sql_command.Parameters.Add("@area_id", SqlDbType.BigInt).Value = Item.user_id

    '  _num_rows_updated = _sql_command.ExecuteNonQuery()

    '  If _num_rows_updated <> 1 Then
    '    GUI_EndSQLTransaction(_sql_tx, False)

    '    Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    '  End If

    '  ' Commit
    '  If Not GUI_EndSQLTransaction(_sql_tx, True) Then
    '    Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    '  End If

    'Catch ex As Exception
    '  ' Rollback  
    '  GUI_EndSQLTransaction(_sql_tx, False)
    '  Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    'End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
  End Function ' UpdateArea

#End Region ' Private Functions

End Class
