'-------------------------------------------------------------------
' Copyright � 2015 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_terminal_meter_delta.vb
'
' DESCRIPTION:   Terminal Meter Delta Class (dynamic fields)
'
' AUTHOR:        Fernando Jim�nez
'
' CREATION DATE: 30-MAR-2015
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 26-MAR-2015  FJC    Initial version.
' 18-OCT-2016  ESE    Bug 6116: Do not show SQL fiel modified, show the form label
'--------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports WSI.Common
Imports System.Text

Public Class CLASS_TERMINAL_METER_DELTA
  Inherits CLASS_BASE

#Region " Const "

#End Region

#Region " Structures "

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_TERMINAL_METER_DELTA
    Public tmd_id As Int64
    Public tmd_description As String
    Public tmd_sas_meter_big_inc_data_table As DataTable
    Public tmd_screen_mode As Integer
  End Class

#End Region

#Region " Enums "

  Public Enum ENUM_TERMINAL_METER_DELTA_SCREEN_MODE
    MODE_EDIT = 0
    MODE_NEW = 1
  End Enum

  Public Enum ENUM_TERMINAL_METER_DELTA_GROUP
    GROUP_CENTS = 0
    GROUP_QUANTITY = 1

  End Enum
#End Region

#Region " Members "

  Protected m_terminal_meter_delta As TYPE_TERMINAL_METER_DELTA

#End Region

#Region " Properties "

  Property TmdId() As Int64
    Get
      Return m_terminal_meter_delta.tmd_id
    End Get
    Set(ByVal Value As Int64)
      m_terminal_meter_delta.tmd_id = Value
    End Set
  End Property ' TmdId

  Property TmdDescription() As String
    Get
      Return m_terminal_meter_delta.tmd_description
    End Get
    Set(ByVal Value As String)
      m_terminal_meter_delta.tmd_description = Value
    End Set
  End Property ' TmdDescription

  Property TmdBigIncDataTable() As DataTable
    Get
      Return m_terminal_meter_delta.tmd_sas_meter_big_inc_data_table
    End Get
    Set(ByVal Value As DataTable)
      m_terminal_meter_delta.tmd_sas_meter_big_inc_data_table = Value
    End Set
  End Property ' TmdBigIncDataTable

  Public Property TmdScreenMode() As Integer
    Get
      Return m_terminal_meter_delta.tmd_screen_mode
    End Get
    Set(ByVal Value As Integer)
      m_terminal_meter_delta.tmd_screen_mode = Value
    End Set
  End Property ' TmdScreenMode

#End Region

#Region " Constructor / Destructor "

  Public Sub New()
    Call MyBase.New()
    Me.m_terminal_meter_delta = New TYPE_TERMINAL_METER_DELTA

  End Sub ' New

  Protected Overrides Sub Finalize()
    MyBase.Finalize()
  End Sub ' Finalize

#End Region

#Region " Overrides "

  '----------------------------------------------------------------------------
  ' PURPOSE: Returns the related auditor data for the current object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  '
  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _aux_field_name As String
    Dim _temp As String

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_TERMINALS)
    _temp = String.Empty
    _aux_field_name = String.Empty

    _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(6061), Me.TmdDescription)
    _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6061), Me.TmdDescription)

    'Big Incremenets (dinamically)
    If Me.TmdBigIncDataTable.Rows.Count > 0 Then
      For Each _row As DataRow In Me.TmdBigIncDataTable.Rows

        'ESE 18-OCT-2016 Do not show SQL fiel modified, show the form label
        _aux_field_name = _row("NAMETEXT").ToString()
        _auditor_data.SetField(0, _row("VALUE").ToString, _aux_field_name)
      Next
    End If

    Return _auditor_data
  End Function ' AuditorData

  '----------------------------------------------------------------------------
  ' PURPOSE: Can't delete the given object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  '
  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
  End Function 'DB_Delete

  '----------------------------------------------------------------------------
  ' PURPOSE: Can't insert the given object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  '
  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As ENUM_STATUS
    Dim _rc As Integer

    ' Insert Terminal Meter Delta Group
    _rc = InsertGroupTerminalMeterDelta()

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
        Return ENUM_STATUS.STATUS_DUPLICATE_KEY

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select
  End Function 'DB_Insert

  '----------------------------------------------------------------------------
  ' PURPOSE: Reads from the database into the given object
  '
  ' PARAMS:
  '   - INPUT:
  '     - ObjectId: An object, it must be 'casted' to the desired KEY.
  '
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  '
  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As ENUM_STATUS

    Dim _rc As Integer

    ' Read Group terminal meter delta information
    If ObjectId <> -1 Then
      _rc = GetGroupTerminalMeterDelta(ObjectId)
    End If

    Select Case _rc
      Case ENUM_DB_RC.DB_RC_OK
        Return ENUM_STATUS.STATUS_OK

      Case ENUM_DB_RC.DB_RC_ERROR_NOT_FOUND
        Return CLASS_BASE.ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function   'DB_Read

  '----------------------------------------------------------------------------
  ' PURPOSE: Updates the given object to the database.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  '
  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _rc As Integer

    ' Update Terminal Delta Meter Group
    _rc = Me.UpdateGroupTerminalMeterDelta()

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
        Return ENUM_STATUS.STATUS_DUPLICATE_KEY

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return CLASS_BASE.ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function 'DB_Update

  '----------------------------------------------------------------------------
  ' PURPOSE: Returns a duplicate of the given object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim _temp As CLASS_TERMINAL_METER_DELTA

    _temp = New CLASS_TERMINAL_METER_DELTA

    _temp.TmdId = Me.m_terminal_meter_delta.tmd_id
    _temp.TmdDescription = Me.m_terminal_meter_delta.tmd_description
    _temp.TmdBigIncDataTable = Me.m_terminal_meter_delta.tmd_sas_meter_big_inc_data_table.Copy
    
    Return _temp

  End Function 'Duplicate 

#End Region

#Region " Private "

  '----------------------------------------------------------------------------
  ' PURPOSE: Insert the Terminal Meter Delta information.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  '
  Private Function GetGroupTerminalMeterDelta(ByVal ItemTmdId As Integer) As Integer
    Dim _sb_tmd As StringBuilder
    Dim _dt_tmd As DataTable
    Dim _dt_tmd_description As DataTable
    Dim _dt_tmd_return As DataTable
    Dim _sql_trans As System.Data.SqlClient.SqlTransaction = Nothing

    Try

      If Not GUI_BeginSQLTransaction(_sql_trans) Then
        'TODO JMM 28-NOV-2012: Return an empty object
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      If Me.TmdScreenMode = ENUM_TERMINAL_METER_DELTA_SCREEN_MODE.MODE_NEW Then
        ItemTmdId = -1
      End If

      _sb_tmd = New StringBuilder()

      ' TERMINAL_METER_DELTA
      _sb_tmd.AppendLine("      SELECT   *                        ")
      _sb_tmd.AppendLine("        FROM   TERMINAL_METER_DELTA     ")
      _sb_tmd.AppendLine("       WHERE   TMD_ID = " & ItemTmdId)

      _dt_tmd = GUI_GetTableUsingSQL(_sb_tmd.ToString(), 1)
      If IsNothing(_dt_tmd) Then
        Return CLASS_BASE.ENUM_STATUS.STATUS_NOT_FOUND
      End If

      ' TERMINAL_METER_DELTA_DESCRIPTION
      _sb_tmd = New StringBuilder()
      _sb_tmd.AppendLine("      SELECT   *                                ")
      _sb_tmd.AppendLine("        FROM   TERMINAL_METER_DELTA_DESCRIPTION ")
      _sb_tmd.AppendLine("    ORDER BY   TMDD_GROUP, TMDD_ORDER           ")
      _dt_tmd_description = GUI_GetTableUsingSQL(_sb_tmd.ToString(), Integer.MaxValue)

      If IsNothing(_dt_tmd_description) Then
        Return CLASS_BASE.ENUM_STATUS.STATUS_NOT_FOUND
      End If

      ' Create DataTable big increments dinamically (depending of the fileds in DB)
      _dt_tmd_return = CreateDataTableBigInc(_dt_tmd, _dt_tmd_description)

      If Not IsNothing(_dt_tmd_return) Then
        Me.TmdBigIncDataTable = _dt_tmd_return.Copy()
      Else
        Return CLASS_BASE.ENUM_STATUS.STATUS_NOT_FOUND
      End If

      If _dt_tmd.Rows.Count > 0 Then

        Me.TmdId = ItemTmdId
        Me.TmdDescription = NullTrim(_dt_tmd.Rows(0).Item("TMD_DESCRIPTION"))
      End If

    Catch _ex As Exception
      Log.Exception(_ex)
      GUI_EndSQLTransaction(_sql_trans, False)

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

    Return CLASS_BASE.ENUM_STATUS.STATUS_OK
  End Function ' GetGroupTerminalMeterDelta

  '----------------------------------------------------------------------------
  ' PURPOSE: Create a DataTable with dynamic fields
  '
  ' PARAMS:
  '   - INPUT: 
  '       
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  '
  Private Function CreateDataTableBigInc(ByVal DtTmd As DataTable, _
                                         ByVal DtTmdDescription As DataTable) As DataTable
    Dim _dt_tmd_return As DataTable
    Dim _dt_row As DataRow

    Try

      ' MEMORY DATATABLE CREATION (BigIncrements)
      _dt_tmd_return = New DataTable
      _dt_tmd_return.Columns.Add("FIELDNAME", Type.GetType("System.String"))
      _dt_tmd_return.Columns.Add("NAMETEXT", Type.GetType("System.String"))
      _dt_tmd_return.Columns.Add("GROUP", Type.GetType("System.Int32"))
      _dt_tmd_return.Columns.Add("ORDER", Type.GetType("System.Int32"))
      _dt_tmd_return.Columns.Add("VALUE", Type.GetType("System.Int64"))

      For Each _dt_tmd_row As DataRow In DtTmdDescription.Rows                      ' Description
        For _idx As Integer = 0 To DtTmd.Columns.Count                              ' Teminal Meter
          If _dt_tmd_row.Item("TMDD_FIELD_NAME") = DtTmd.Columns(_idx).ColumnName Then
            _dt_row = _dt_tmd_return.NewRow()
            _dt_row("FIELDNAME") = _dt_tmd_row.Item("TMDD_FIELD_NAME")
            _dt_row("NAMETEXT") = _dt_tmd_row.Item("TMDD_FIELD_DESCRIPTION")
            _dt_row("GROUP") = _dt_tmd_row("TMDD_GROUP")
            _dt_row("ORDER") = _dt_tmd_row("TMDD_ORDER")
            If DtTmd.Rows.Count <= 0 Then

              _dt_row("VALUE") = -1
            Else

              _dt_row("VALUE") = DtTmd.Rows(0).Item(_idx)
            End If

            _dt_tmd_return.Rows.Add(_dt_row)

            Exit For
          End If
        Next
      Next

    Catch _ex As Exception
      Log.Exception(_ex)

      Return Nothing
    End Try

    Return _dt_tmd_return
  End Function ' CreateDataTableBigInc

  '----------------------------------------------------------------------------
  ' PURPOSE: Update the group terminal meter delta information.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  '
  Private Function UpdateGroupTerminalMeterDelta() As Integer
    Dim _sb_tmd As StringBuilder
    Dim _sb_tmd_fields As StringBuilder
    Dim _sql_trans As System.Data.SqlClient.SqlTransaction
    Dim _sql_command As System.Data.SqlClient.SqlCommand
    Dim _num_rows_updated As Integer
    Dim _norepeat As List(Of String)

    _sql_trans = Nothing
    _sql_command = Nothing

    Try
      ' Terminal Meter Delta Data
      _sb_tmd = New StringBuilder()
      _sb_tmd_fields = New StringBuilder()


      If Not GUI_BeginSQLTransaction(_sql_trans) Then
        'TODO JMM 28-NOV-2012: Return an empty object
      End If
      _norepeat = New List(Of String)

      'Get Fields from DataSet
      For Each _row As DataRow In Me.TmdBigIncDataTable.Rows
        If Not _norepeat.Contains(_row("FIELDNAME").ToString().ToUpper()) Then

          ' Fields Name & Values
          If _sb_tmd_fields.ToString <> String.Empty Then
            _sb_tmd_fields.AppendLine(" , ")
          End If
          _sb_tmd_fields.AppendLine(_row("FIELDNAME") & "=" & "@p" & _row("FIELDNAME"))
          _norepeat.Add(_row("FIELDNAME").ToString().ToUpper())
        Else
          Log.Message("duplicate meter delta sent to database, ignored")
        End If
      Next

      'Generate SQL String
      With _sb_tmd
        .AppendLine("   UPDATE  TERMINAL_METER_DELTA              ")
        .AppendLine("      SET  TMD_DESCRIPTION = @pTmdGroupName  ")
        .AppendLine("          ,                         ")
        .AppendLine(_sb_tmd_fields.ToString)
        .AppendLine("  WHERE   TMD_ID = @pTmdId               ")
      End With

      _sql_command = New SqlCommand(_sb_tmd.ToString())
      _sql_command.Transaction = _sql_trans
      _sql_command.Connection = _sql_trans.Connection

      With _sql_command.Parameters
        'Set GroupName
        .Add("@pTmdGroupName", SqlDbType.NVarChar, 100).Value = Me.TmdDescription

        _norepeat = New List(Of String)

        'Get Values from DataSet
        For Each _row As DataRow In Me.TmdBigIncDataTable.Rows
          If Not _norepeat.Contains(_row("FIELDNAME").ToString().ToUpper()) Then
            .Add("@p" & _row("FIELDNAME"), SqlDbType.BigInt).Value = _row("VALUE")
            _norepeat.Add(_row("FIELDNAME").ToString().ToUpper())
          Else
            Log.Message("duplicate meter delta sent to database, ignored")
          End If
        Next

        'Set Id TMD
        .Add("@pTmdId", SqlDbType.BigInt).Value = Me.TmdId
      End With

      _num_rows_updated = _sql_command.ExecuteNonQuery()

      If _num_rows_updated <= 0 Then
        GUI_EndSQLTransaction(_sql_trans, False)

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      ' Commit
      If Not GUI_EndSQLTransaction(_sql_trans, True) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

    Catch _ex As Exception

      Log.Exception(_ex)
      GUI_EndSQLTransaction(_sql_trans, False)
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

    Return CLASS_BASE.ENUM_STATUS.STATUS_OK

  End Function ' UpdateGroupTerminalMeterDelta

  '----------------------------------------------------------------------------
  ' PURPOSE: Insert a new group terminal meter delta.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  '
  Private Function InsertGroupTerminalMeterDelta() As Integer
    Dim _sb_tmd As StringBuilder
    Dim _sb_tmd_fields As StringBuilder
    Dim _sb_tmd_fields_values As StringBuilder
    Dim _sql_trans As System.Data.SqlClient.SqlTransaction
    Dim _sql_command As System.Data.SqlClient.SqlCommand
    Dim _num_rows_inserted As Integer
    Dim _norepeat As List(Of String)
    _sql_trans = Nothing

    Try
      ' Terminal Meter Delta Data
      _sb_tmd = New StringBuilder()
      _sb_tmd_fields = New StringBuilder()
      _sb_tmd_fields_values = New StringBuilder()


      If Not GUI_BeginSQLTransaction(_sql_trans) Then
        'TODO JMM 28-NOV-2012: Return an empty object
      End If
      _norepeat = New List(Of String)

      'Get Fields from DataSet
      For Each _row As DataRow In Me.TmdBigIncDataTable.Rows
        If Not _norepeat.Contains(_row("FIELDNAME").ToString().ToUpper()) Then
          Debug.Print(_row.ItemArray(1))
          ' Fields Name
          If _sb_tmd_fields.ToString <> String.Empty Then
            _sb_tmd_fields.AppendLine(" , ")
          End If
          _sb_tmd_fields.AppendLine(_row("FIELDNAME"))

          ' Fields Values
          If _sb_tmd_fields_values.ToString <> String.Empty Then
            _sb_tmd_fields_values.AppendLine(" , ")
          End If
          _sb_tmd_fields_values.AppendLine("@p" & _row("FIELDNAME"))
          _norepeat.Add(_row("FIELDNAME").ToString().ToUpper())
        Else
          Log.Message("duplicate meter delta sent to database, ignored")
        End If
      Next

      'Generate SQL String
      With _sb_tmd
        .AppendLine(" INSERT INTO     TERMINAL_METER_DELTA( ")
        .AppendLine("                 TMD_DESCRIPTION       ")
        .AppendLine("               ,                       ")
        .AppendLine(_sb_tmd_fields.ToString)
        .AppendLine(")                                      ")
        .AppendLine("   VALUES(                             ")
        .AppendLine("                 @pTmdGroupName        ")
        .AppendLine("               ,                       ")
        .AppendLine(_sb_tmd_fields_values.ToString)
        .AppendLine(")                                      ")
      End With

      _sql_command = New SqlCommand(_sb_tmd.ToString())
      _sql_command.Transaction = _sql_trans
      _sql_command.Connection = _sql_trans.Connection

      With _sql_command.Parameters
        .Add("@pTmdGroupName", SqlDbType.NVarChar, 100).Value = Me.TmdDescription
        _norepeat = New List(Of String)
        'Get Values from DataSet
        For Each _row As DataRow In Me.TmdBigIncDataTable.Rows
          If Not _norepeat.Contains(_row("FIELDNAME").ToString().ToUpper()) Then
            .Add("@p" & _row("FIELDNAME"), SqlDbType.BigInt).Value = _row("VALUE")
            _norepeat.Add(_row("FIELDNAME").ToString().ToUpper())
          Else
            Log.Message("duplicate meter delta sent to database, ignored")
          End If
        Next

      End With

      _num_rows_inserted = _sql_command.ExecuteNonQuery()
      If _num_rows_inserted <= 0 Then
        GUI_EndSQLTransaction(_sql_trans, False)

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      ' Commit
      If Not GUI_EndSQLTransaction(_sql_trans, True) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

    Catch _ex As Exception

      Log.Exception(_ex)
      GUI_EndSQLTransaction(_sql_trans, False)
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

    Return CLASS_BASE.ENUM_STATUS.STATUS_OK

  End Function ' InsertGroupTerminalMeterDelta

#End Region

#Region " Public "
  Public Function CheckNameAlreadyExists(ByVal Name As String, ByVal Id As Int64) As Boolean
    Dim _sql_query As StringBuilder
    Dim _sql_command As SqlCommand
    Dim _sql_tx As System.Data.SqlClient.SqlTransaction = Nothing
    Dim _data_table As DataTable

    If String.IsNullOrEmpty(Name) Then
      Return False
    End If
    Try
      _sql_query = New StringBuilder

      _sql_query.AppendLine("SELECT   COUNT(*)                    ")
      _sql_query.AppendLine("  FROM   terminal_meter_delta        ")
      _sql_query.AppendLine(" WHERE   tmd_description like @pName ")
      _sql_query.AppendLine("   AND   tmd_id <> @pId              ")

      _sql_command = New SqlCommand(_sql_query.ToString())
      _sql_command.Parameters.Add("@pName", SqlDbType.NVarChar, 100).Value = Name
      _sql_command.Parameters.Add("@pId", SqlDbType.BigInt).Value = Id
      _data_table = GUI_GetTableUsingCommand(_sql_command, 10)

      If Not IsNothing(_data_table) Then
        If _data_table.Rows.Count() > 0 Then
          If _data_table.Rows(0).Item(0) > 0 Then
            Return True
          End If
        End If
      End If

    Catch ex As Exception
      ' Rollback  
      GUI_EndSQLTransaction(_sql_tx, False)

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

    Return False

  End Function 'CheckNameAlreadyExists
#End Region

End Class

