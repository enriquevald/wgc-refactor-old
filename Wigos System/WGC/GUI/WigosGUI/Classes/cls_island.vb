'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : cls_island.vb
'
' DESCRIPTION : island class for user edition
'              
' REVISION HISTORY :
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 20-FEB-2012  SSC    Initial version
' 06-OCT-2014  LEM    Call TerminalReport.ForceRefresh on update.
'-------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports WSI.Common

Public Class CLASS_ISLAND
  Inherits CLASS_BASE

#Region " Constants "

  ' Fields length
  Public Const MAX_NAME_LEN As Integer = 50

#End Region

#Region " Enums "


#End Region ' Enums 

#Region " Structures "

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_ISLAND
    Public bank_id As Int64
    Public name As String
    Public area_id As Int32
    Public area_name As String
  End Class

#End Region

#Region " Members "

  Protected m_island As New TYPE_ISLAND

#End Region

#Region " Properties "

  Public Property BankId() As Int64
    Get
      Return m_island.bank_id
    End Get

    Set(ByVal Value As Int64)
      m_island.bank_id = Value
    End Set
  End Property

  Public Property Name() As String
    Get
      Return m_island.name
    End Get

    Set(ByVal Value As String)
      m_island.name = Value
    End Set
  End Property

  Public Property AreaId() As Int32
    Get
      Return m_island.area_id
    End Get

    Set(ByVal Value As Int32)
      m_island.area_id = Value
    End Set
  End Property

  Public Property AreaName() As String
    Get
      Return m_island.area_name
    End Get

    Set(ByVal Value As String)
      m_island.area_name = Value
    End Set
  End Property

#End Region ' Structures

#Region " Overrides functions "

  '----------------------------------------------------------------------------
  ' PURPOSE : Reads from the database into the given object
  '
  ' PARAMS :
  '     - INPUT :
  '         - ObjectId: An object, it must be 'casted' to the desired KEY.
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '
  ' NOTES :

  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As Integer

    Me.BankId = ObjectId

    ' Read Bank data
    _rc = ReadIsland(m_island, SqlCtx)

    Select Case _rc

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Read

  '----------------------------------------------------------------------------
  ' PURPOSE : Updates the given object to the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES :

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As Integer

    ' Update bank data
    _rc = UpdateIsland(m_island, SqlCtx)

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Call TerminalReport.ForceRefresh()
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
        Return ENUM_STATUS.STATUS_DUPLICATE_KEY

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Update

  '----------------------------------------------------------------------------
  ' PURPOSE : Inserts the given object to the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES :

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As Integer

    ' Insert bank data
    _rc = InsertIsland(m_island, SqlCtx)

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
        Return ENUM_STATUS.STATUS_DUPLICATE_KEY

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Insert

  '----------------------------------------------------------------------------
  ' PURPOSE : Deletes the given object from the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DEPENDENCIES
  '
  ' NOTES :

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As Integer

    ' Delete bank data
    _rc = DeleteIsland(Me.BankId, SqlCtx)

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DEPENDENCIES
        Return ENUM_STATUS.STATUS_DEPENDENCIES

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Delete

  '----------------------------------------------------------------------------
  ' PURPOSE : Returns a duplicate of the given object.
  '
  ' PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - The newly created object
  '
  ' NOTES :

  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim temp_island As CLASS_ISLAND

    temp_island = New CLASS_ISLAND
    temp_island.m_island.bank_id = Me.m_island.bank_id
    temp_island.m_island.name = Me.m_island.name
    temp_island.m_island.area_id = Me.m_island.area_id
    temp_island.m_island.area_name = Me.m_island.area_name

    Return temp_island

  End Function ' Duplicate

  '----------------------------------------------------------------------------
  ' PURPOSE : Returns the related auditor data for the current object.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - The newly created audit object
  '
  ' NOTES :

  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim _auditor_data As CLASS_AUDITOR_DATA

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_TERMINALS)

    Call _auditor_data.SetName(GLB_NLS_GUI_CONFIGURATION.Id(431), Me.Name)        ' 431 "Isla"
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(353), Me.Name)       ' 353 "Nombre"

    If Me.AreaName = "" Then 'NOTHING
      Call _auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(435), "---")         ' 435 "Area"
    Else
      Call _auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(435), Me.AreaName)
    End If

    Return _auditor_data

  End Function ' AuditorData

#End Region

#Region " Private Functions "

  '----------------------------------------------------------------------------
  ' PURPOSE : Reads a island object from DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - Item.bank_id
  '         - Context
  '
  '     - OUTPUT :
  '         - Item
  '
  ' RETURNS :
  '     - CONFIGURATION_OK
  '     - CONFIGURATION_ERROR_DB
  '
  ' NOTES :

  Private Function ReadIsland(ByVal Item As TYPE_ISLAND, _
                            ByVal Context As Integer) As Integer

    Dim _sql_query As String
    Dim _data_table As DataTable

    _sql_query = "SELECT BK_NAME" _
                    + ", BK_AREA_ID" _
                    + ", AR_NAME" _
                + " FROM BANKS, AREAS" _
                + " WHERE BK_AREA_ID = AR_AREA_ID" _
               + " AND BK_BANK_ID = " + Item.bank_id.ToString()

    _data_table = GUI_GetTableUsingSQL(_sql_query, 5000)
    If IsNothing(_data_table) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    If _data_table.Rows.Count() <> 1 Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    Item.name = _data_table.Rows(0).Item("BK_NAME")
    Item.area_id = _data_table.Rows(0).Item("BK_AREA_ID")
    Item.area_name = _data_table.Rows(0).Item("AR_NAME")
  

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

  End Function ' ReadIsland

  '----------------------------------------------------------------------------
  ' PURPOSE : Deletes a island object from DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - BankId
  '         - Context
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - CONFIGURATION_OK
  '     - CONFIGURATION_ERROR_DB
  '
  ' NOTES :

  Private Function DeleteIsland(ByVal BankId As Integer, _
                              ByVal Context As Integer) As Integer
    Dim _sql_query As String
    Dim db_count As Integer = 0
    Dim _sql_tx As System.Data.SqlClient.SqlTransaction = Nothing
    Dim _sql_command As System.Data.SqlClient.SqlCommand = Nothing
    Dim _num_rows_updated As Integer
    Dim _data_table As DataTable
    Dim _bank_name As String = ""
    Dim _bank_name_default As String = ""

    '     - Remove the island item from the BANKS table
    Try
      If Not GUI_BeginSQLTransaction(_sql_tx) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If


      ' Before removing, check dependecies with other tables
      _sql_query = " SELECT  COUNT(*) " & _
                   " FROM  TERMINALS " & _
                   " WHERE TE_BANK_ID = " & BankId.ToString()

      _data_table = GUI_GetTableUsingSQL(_sql_query, 5000)
      If IsNothing(_data_table) Then
        GUI_EndSQLTransaction(_sql_tx, False)

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      db_count = _data_table.Rows(0).Item(0)

      'If there are terminals in this island
      If db_count > 0 Then
        'Island name
        _sql_query = "SELECT BK_NAME" _
               + " FROM BANKS" _
               + " WHERE BK_BANK_ID = " + BankId.ToString()

        _data_table = GUI_GetTableUsingSQL(_sql_query, 5000)
        If IsNothing(_data_table) Then
          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

        If _data_table.Rows.Count() > 0 Then
          _bank_name = _data_table.Rows(0).Item("BK_NAME")
        End If

        'Island default name (BankId = 0)
        _sql_query = "SELECT BK_NAME" _
               + " FROM BANKS" _
               + " WHERE BK_BANK_ID = 0"

        _data_table = GUI_GetTableUsingSQL(_sql_query, 5000)
        If IsNothing(_data_table) Then
          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

        If _data_table.Rows.Count() > 0 Then
          _bank_name_default = _data_table.Rows(0).Item("BK_NAME")
        End If

        ' 149 "Todos los terminales de la isla %1 se mover�n a la isla %2. �Desea continuar?"
        If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(149), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_YES_NO, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _bank_name, _bank_name_default) = ENUM_MB_RESULT.MB_RESULT_NO Then

          Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

        Else

          _sql_query = "UPDATE TERMINALS" _
                   + " SET TE_BANK_ID = 0" _
                   + " WHERE TE_BANK_ID = " & BankId.ToString()

          _sql_command = New SqlCommand(_sql_query)
          _sql_command.Transaction = _sql_tx
          _sql_command.Connection = _sql_tx.Connection

          _num_rows_updated = _sql_command.ExecuteNonQuery()

          If _num_rows_updated = 0 Then
            GUI_EndSQLTransaction(_sql_tx, False)
            Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
          End If

        End If

      End If

      'Delete Bank
      _sql_query = "DELETE BANKS" _
                 + " WHERE BK_BANK_ID = " + BankId.ToString()

      If Not GUI_SQLExecuteNonQuery(_sql_query, _sql_tx) Then
        ' Rollback  
        GUI_EndSQLTransaction(_sql_tx, False)

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      ' Commit
      If Not GUI_EndSQLTransaction(_sql_tx, True) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

    Catch ex As Exception
      ' Rollback  
      GUI_EndSQLTransaction(_sql_tx, False)

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

  End Function ' DeleteIsland

  '----------------------------------------------------------------------------
  ' PURPOSE : Adds a island object into DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - Item
  '         - Context
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - CONFIGURATION_OK
  '     - CONFIGURATION_ERROR_DB
  '
  ' NOTES :

  Private Function InsertIsland(ByVal Item As TYPE_ISLAND, _
                              ByVal Context As Integer) As Integer
    Dim _sql_query As String
    Dim _sql_tx As System.Data.SqlClient.SqlTransaction = Nothing
    Dim _data_table As DataTable

    ' Steps
    '     - Check whether there is no other island with the same name 
    '     - Insert the island item to the BANKS table

    Try
      '     - Check whether there is no other island with the same name 
      _sql_query = "SELECT COUNT (*)" _
                  + " FROM BANKS" _
                 + " WHERE BK_NAME = '" + Item.name + "'"

      _data_table = GUI_GetTableUsingSQL(_sql_query, 5000)
      If Not IsNothing(_data_table) Then
        If _data_table.Rows.Count() > 0 Then
          If _data_table.Rows(0).Item(0) > 0 Then
            Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
          End If
        End If
      End If

      If Not GUI_BeginSQLTransaction(_sql_tx) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      '     - Insert the bank item to the BANKS table
      _sql_query = "INSERT INTO BANKS" _
                          + " ( BK_NAME" _
                           + ", BK_AREA_ID" _
                           + ")" _
                     + " VALUES" _
                         + " ( '" + Item.name + "'" _
                         + " , " + Item.area_id.ToString _
                         + ")"

      If Not GUI_SQLExecuteNonQuery(_sql_query, _sql_tx) Then
        ' Rollback  
        GUI_EndSQLTransaction(_sql_tx, False)

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      ' Commit
      If Not GUI_EndSQLTransaction(_sql_tx, True) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

    Catch ex As Exception
      ' Rollback  
      GUI_EndSQLTransaction(_sql_tx, False)

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

  End Function ' InsertIsland

  '----------------------------------------------------------------------------
  ' PURPOSE : Updates the given island object into the DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - Item
  '         - Context
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - CONFIGURATION_OK
  '     - CONFIGURATION_ERROR_DB
  '
  ' NOTES :

  Private Function UpdateIsland(ByVal Item As TYPE_ISLAND, _
                              ByVal Context As Integer) As Integer
    Dim _sql_query As String
    Dim _sql_tx As System.Data.SqlClient.SqlTransaction = Nothing
    Dim _data_table As DataTable

    ' Steps
    '     - Check whether there is no other island with the same name 
    '     - Update the island's data 

    Try
      '   - Check whether there is no other island with the same name 
      _sql_query = "SELECT COUNT (*)" _
                  + " FROM BANKS" _
                 + " WHERE BK_NAME = '" + Item.name + "'" _
                   + " AND BK_BANK_ID <> " + Item.bank_id.ToString()

      _data_table = GUI_GetTableUsingSQL(_sql_query, 5000)
      If Not IsNothing(_data_table) Then
        If _data_table.Rows.Count() > 0 Then
          If _data_table.Rows(0).Item(0) > 0 Then
            Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
          End If
        End If
      End If

      '   - Update the island's data 
      If Not GUI_BeginSQLTransaction(_sql_tx) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      _sql_query = "UPDATE BANKS" _
                   + " SET BK_NAME = '" + Item.name + "'" _
                      + ", BK_AREA_ID = " + Item.area_id.ToString _
                 + " WHERE BK_BANK_ID = " + Item.bank_id.ToString()

      If Not GUI_SQLExecuteNonQuery(_sql_query, _sql_tx) Then
        ' Rollback  
        GUI_EndSQLTransaction(_sql_tx, False)

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      ' Commit
      If Not GUI_EndSQLTransaction(_sql_tx, True) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

    Catch ex As Exception
      ' Rollback  
      GUI_EndSQLTransaction(_sql_tx, False)
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
  End Function ' UpdateIsland

#End Region ' Private Functions

End Class
