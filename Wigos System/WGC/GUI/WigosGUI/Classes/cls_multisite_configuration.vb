'-------------------------------------------------------------------
' Copyright © 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_multisite_configuration.vb
' DESCRIPTION:   
' AUTHOR:        Javier Molina Moral
' CREATION DATE: 26-FEB-2013
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------------------------
' 26-FEB-2013  JMM    Initial version
' 22-APR-2013  ANG    Disallow update MemberCardsId
' 20-DEC-2013  JBC    Fixed Bug WIG-495: Insert GROUP_KEY param on the update query
' 29-SEP-2014  FJC    Add SAS_FLAG: SPECIAL_PROGRESSIVE_METER
' 11-OCT-2016  RAB    PBI 18098: General params: Automatically add the GP to the system
' 16-JAN-2016  JMM    PBI 19744:Pago automático de handpays
' 16-FEB-2016  FAV    PBI 24711: Machine on reserve.
' -------------------------------------------------------------------------------------

Option Explicit On

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports WSI.Common
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports System.Text

Public Class CLS_MULTISITE_CONFIGURATION
  Inherits CLASS_BASE

#Region " Enums "

  Enum ENUM_MULTISITE_STATUS
    STATUS_NO_ACTIVATED
    STATUS_ACTIVATED
  End Enum

  Enum ENUM_SAS_PROMOTION_FLAGS
    SYSTEM_DEFINED = 0
    MACHINE_DEFINED = 1
  End Enum

#End Region ' Constants

#Region " Structures "

  Public Class TYPE_MULTISITE_CONFIG
    Public site_configured As Int32
    Public status As ENUM_MULTISITE_STATUS
    Public site_id As String
    Public site_name As String
    Public members_card_id As String
    Public center_address_1 As String
    Public center_address_2 As String
    Public members_card_id_enable As Boolean

    Sub New()
      site_configured = 0
      status = ENUM_MULTISITE_STATUS.STATUS_NO_ACTIVATED
      site_id = ""
      site_name = ""
      members_card_id = ""
      center_address_1 = ""
      center_address_2 = ""
    End Sub
  End Class

#End Region ' Structures

#Region " Members "

  Protected m_multisite_config As TYPE_MULTISITE_CONFIG
  Private m_extended_counters As Boolean
  Private m_promotional_credits As ENUM_SAS_PROMOTION_FLAGS
  Private m_handPays As ENUM_SAS_FLAGS
  Private m_progressive_meters As Boolean
  Private m_enable_disable_note_acceptor As Boolean
  Private m_reserve_terminal As Boolean
  Private m_lock_egm_on_reserve As Boolean

#End Region ' Members

#Region " Properties "

  Public Property SiteConfigured() As Int32
    Get
      Return m_multisite_config.site_configured
    End Get
    Set(ByVal Value As Int32)
      m_multisite_config.site_configured = Value
    End Set
  End Property

  Public Property Status() As ENUM_MULTISITE_STATUS
    Get
      Return m_multisite_config.status
    End Get
    Set(ByVal Value As ENUM_MULTISITE_STATUS)
      m_multisite_config.status = Value
    End Set
  End Property

  Public Property SiteID() As String
    Get
      Return m_multisite_config.site_id
    End Get
    Set(ByVal Value As String)
      m_multisite_config.site_id = Value
    End Set
  End Property

  Public Property SiteName() As String
    Get
      Return m_multisite_config.site_name
    End Get
    Set(ByVal Value As String)
      m_multisite_config.site_name = Value
    End Set
  End Property

  Public Property MembersCardID() As String
    Get
      Return m_multisite_config.members_card_id
    End Get
    Set(ByVal Value As String)
      m_multisite_config.members_card_id = Value
    End Set
  End Property

  Public Property CenterAddress1() As String
    Get
      Return m_multisite_config.center_address_1
    End Get
    Set(ByVal Value As String)
      m_multisite_config.center_address_1 = Value
    End Set
  End Property

  Public Property CenterAddress2() As String
    Get
      Return m_multisite_config.center_address_2
    End Get
    Set(ByVal Value As String)
      m_multisite_config.center_address_2 = Value
    End Set
  End Property

  Public Property MembersCardIDEnabled() As String
    Get
      Return m_multisite_config.members_card_id_enable
    End Get
    Set(ByVal Value As String)
      m_multisite_config.members_card_id_enable = Value
    End Set
  End Property

  Public Property ExtendedMeters() As Boolean
    Get
      Return Me.m_extended_counters
    End Get
    Set(ByVal Value As Boolean)
      Me.m_extended_counters = Value
    End Set
  End Property

  Public Property PromotionalCredits() As ENUM_SAS_PROMOTION_FLAGS
    Get
      Return Me.m_promotional_credits
    End Get
    Set(ByVal Value As ENUM_SAS_PROMOTION_FLAGS)
      Me.m_promotional_credits = Value
    End Set
  End Property

  Public Property Cmd1BHandPays() As ENUM_SAS_FLAGS
    Get
      Return Me.m_handPays
    End Get
    Set(ByVal Value As ENUM_SAS_FLAGS)
      Me.m_handPays = Value
    End Set
  End Property


  Public Property ProgressiveMeters() As Boolean
    Get
      Return Me.m_progressive_meters
    End Get
    Set(ByVal Value As Boolean)
      Me.m_progressive_meters = Value
    End Set
  End Property

  Public Property EnableDisableNoteAcceptor() As Boolean
    Get
      Return Me.m_enable_disable_note_acceptor
    End Get
    Set(ByVal Value As Boolean)
      Me.m_enable_disable_note_acceptor = Value
    End Set
  End Property

  Public Property ReserveTerminal() As Boolean
    Get
      Return Me.m_reserve_terminal
    End Get
    Set(ByVal Value As Boolean)
      Me.m_reserve_terminal = Value
    End Set
  End Property

  Public Property LockEgmOnReserve() As Boolean
    Get
      Return Me.m_lock_egm_on_reserve
    End Get
    Set(ByVal Value As Boolean)
      Me.m_lock_egm_on_reserve = Value
    End Set
  End Property


#End Region ' Properties

#Region " Overrides function "

  '----------------------------------------------------------------------------
  ' PURPOSE : Returns the related auditor data for the current object.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - The newly created audit object
  '
  ' NOTES :
  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA
    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _value As String

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_MULTISITE)

    Call _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(1697), "")

    ' Site Configured
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1698), Me.SiteConfigured)

    ' Site ID
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1407), Me.SiteID)

    ' Site Name
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1410), Me.SiteName)

    ' Status
    Select Case Me.Status
      Case CLS_MULTISITE_CONFIGURATION.ENUM_MULTISITE_STATUS.STATUS_NO_ACTIVATED
        _value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1694) '1694 "Not activated"

      Case CLS_MULTISITE_CONFIGURATION.ENUM_MULTISITE_STATUS.STATUS_ACTIVATED
        _value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1696) '1696 "Activated"

      Case Else
        _value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(786) '786 "Unknown"

    End Select

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(262), _value)

    ' Members Card ID
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1408), Me.MembersCardID)

    ' Center Address 1
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1690), Me.CenterAddress1)

    ' Center Address 2
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1691), Me.CenterAddress2)

    ' Extended Meters
    If Me.ExtendedMeters Then
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2099), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1696))
    Else
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2099), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1694))
    End If

    ' Promotinal Credits
    If Me.PromotionalCredits = ENUM_SAS_PROMOTION_FLAGS.MACHINE_DEFINED Then
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(4434), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1158))
    Else
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(4434), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1157))
    End If

    ' Hand Pays
    If Me.Cmd1BHandPays = ENUM_SAS_FLAGS.ACTIVATED Then
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(4433), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1696))
    ElseIf Me.Cmd1BHandPays = ENUM_SAS_FLAGS.ACTIVATED_AND_AUTOMATICALLY Then
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(4433), GLB_NLS_GUI_PLAYER_TRACKING.GetString(7808)) '7808 "Activated & Automatic"
    Else
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(4433), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1694))
    End If

    ' Special Progresive Meter
    If Me.ProgressiveMeters Then
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5590), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1696))
    Else
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5590), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1694))
    End If

    If Me.EnableDisableNoteAcceptor Then
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7637), GLB_NLS_GUI_PLAYER_TRACKING.GetString(278))
    Else
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7637), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279))
    End If

    If Me.LockEgmOnReserve Then
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7913), GLB_NLS_GUI_PLAYER_TRACKING.GetString(278))
    Else
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7913), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279))
    End If

    Return _auditor_data
  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE : Deletes the given object from the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DEPENDENCIES
  '
  ' NOTES :
  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE : Inserts the given object to the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES :
  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE : Reads from the database into the given object
  '
  ' PARAMS :
  '     - INPUT :
  '         - ObjectId: An object, it must be 'casted' to the desired KEY.
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '
  ' NOTES :
  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _rc As Integer

    _rc = GetConfigurationData(Me.m_multisite_config)

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select
  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE : Updates the given object to the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES :
  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _rc As Integer

    _rc = Me.UpdateConfiguration(Me.m_multisite_config)

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
        Return ENUM_STATUS.STATUS_DUPLICATE_KEY

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE : Returns a duplicate of the given object.
  '
  ' PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - The newly created object
  '
  ' NOTES :
  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE
    Dim _target As CLS_MULTISITE_CONFIGURATION

    _target = New CLS_MULTISITE_CONFIGURATION

    _target.m_multisite_config.site_configured = Me.m_multisite_config.site_configured
    _target.m_multisite_config.status = Me.m_multisite_config.status
    _target.m_multisite_config.site_id = Me.m_multisite_config.site_id
    _target.m_multisite_config.site_name = Me.m_multisite_config.site_name
    _target.m_multisite_config.members_card_id = Me.m_multisite_config.members_card_id
    _target.m_multisite_config.center_address_1 = Me.m_multisite_config.center_address_1
    _target.m_multisite_config.center_address_2 = Me.m_multisite_config.center_address_2
    _target.ExtendedMeters = Me.m_extended_counters
    _target.PromotionalCredits = Me.m_promotional_credits
    _target.Cmd1BHandPays = Me.m_handPays
    _target.ProgressiveMeters = Me.ProgressiveMeters
    _target.EnableDisableNoteAcceptor = Me.EnableDisableNoteAcceptor
    _target.LockEgmOnReserve = Me.LockEgmOnReserve

    Return _target

  End Function

#End Region ' Overrides Function

#Region " Private functions "

  '----------------------------------------------------------------------------
  ' PURPOSE : Reads MultiSite configuration status from General Params.
  '
  ' PARAMS :
  '     - INPUT : 
  '
  '     - OUTPUT : 
  '
  ' RETURNS :
  '     - ENUM_MULTISITE_STATUS
  '
  ' NOTES :
  Function GetStatus(ByVal Trx As SqlTransaction) As ENUM_MULTISITE_STATUS
    Dim _multisite_member As Int32
    Dim _multisite_member_str As String

    _multisite_member_str = ReadGeneralParam("Site", "MultiSiteMember", Trx)

    If Not Int32.TryParse(_multisite_member_str, _multisite_member) Then
      Return ENUM_MULTISITE_STATUS.STATUS_NO_ACTIVATED
    End If

    If _multisite_member = 1 Then
      Return ENUM_MULTISITE_STATUS.STATUS_ACTIVATED
    End If

    Return ENUM_MULTISITE_STATUS.STATUS_NO_ACTIVATED
  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE : Fill TYPE_MULTISITE_CONFIG.
  '
  ' PARAMS :
  '     - INPUT : An instance of TYPE_MULTISITE_CONFIG object
  '
  '     - OUTPUT : An instance of TYPE_MULTISITE_CONFIG with data filled
  '
  ' RETURNS :
  '     - ENUM_CONFIGURATION_RC
  '
  ' NOTES :
  Private Function GetConfigurationData(ByRef multisite_config As CLS_MULTISITE_CONFIGURATION.TYPE_MULTISITE_CONFIG) As Integer
    Dim _rc As Integer
    Dim _sql_transaction As SqlTransaction
    Dim _site_configured_str As String
    Dim _sas_flags As Integer

    _sas_flags = 0

    _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_OK

    _sql_transaction = Nothing

    Try

      If Not GUI_BeginSQLTransaction(_sql_transaction) Then
        _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

        Return _rc
      End If

      'Site Configured
      _site_configured_str = ReadGeneralParam("Site", "Configured", _sql_transaction)

      If Not Int32.TryParse(_site_configured_str, multisite_config.site_configured) Then
        _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

        Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, "cls_multisite_configuration", "GetConfigurationData", "Invalid value for General Param 'Site.Configured'")

        Return _rc
      End If

      multisite_config.status = GetStatus(_sql_transaction)
      multisite_config.site_id = GetSiteId()
      multisite_config.site_name = GetSiteName()

      If multisite_config.status = ENUM_MULTISITE_STATUS.STATUS_ACTIVATED Then
        multisite_config.members_card_id = ReadGeneralParam("MultiSite", "MembersCardId", _sql_transaction)
        multisite_config.center_address_1 = ReadGeneralParam("MultiSite", "CenterAddress1", _sql_transaction)
        multisite_config.center_address_2 = ReadGeneralParam("MultiSite", "CenterAddress2", _sql_transaction)
      End If

      _sas_flags = ReadGeneralParam("SasHost", "SystemDefaultSasFlags", _sql_transaction)
      m_extended_counters = WSI.Common.Terminal.SASFlagActived(_sas_flags, SAS_FLAGS.USE_EXTENDED_METERS)
      m_promotional_credits = IIf(WSI.Common.Terminal.SASFlagActived(_sas_flags, SAS_FLAGS.CREDITS_PLAY_MODE_SAS_MACHINE), ENUM_SAS_PROMOTION_FLAGS.MACHINE_DEFINED, ENUM_SAS_PROMOTION_FLAGS.SYSTEM_DEFINED)

      If WSI.Common.Terminal.SASFlagActived(_sas_flags, SAS_FLAGS.USE_HANDPAY_INFORMATION) Then
        m_handPays = ENUM_SAS_FLAGS.ACTIVATED

        If WSI.Common.Terminal.SASFlagActived(_sas_flags, SAS_FLAGS.SAS_FLAGS_AUTOMATIC_HANDPAY_PAYMENT) Then
          m_handPays = ENUM_SAS_FLAGS.ACTIVATED_AND_AUTOMATICALLY
        End If
      Else
        m_handPays = ENUM_SAS_FLAGS.NOT_ACTIVATED
      End If

      m_progressive_meters = WSI.Common.Terminal.SASFlagActived(_sas_flags, SAS_FLAGS.SPECIAL_PROGRESSIVE_METER)
      m_enable_disable_note_acceptor = WSI.Common.Terminal.SASFlagActived(_sas_flags, SAS_FLAGS.SAS_FLAGS_ENABLE_DISABLE_NOTE_ACCEPTOR)
      m_reserve_terminal = WSI.Common.Terminal.SASFlagActived(_sas_flags, SAS_FLAGS.SAS_FLAGS_RESERVE_TERMINAL)
      m_lock_egm_on_reserve = WSI.Common.Terminal.SASFlagActived(_sas_flags, SAS_FLAGS.SAS_FLAGS_LOCK_EGM_ON_RESERVE)

    Catch _ex As Exception
      _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, "cls_multisite_configuration", "GetConfigurationData", _ex.Message)
    End Try

    Return _rc

  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE : Read a General Param directly from db.
  '
  ' PARAMS :
  '          - GroupKey
  '          - SubjectKey
  '
  ' RETURNS :
  '     - ENUM_CONFIGURATION_RC
  '
  ' NOTES :
  Private Function ReadGeneralParam(ByVal GroupKey As String, ByVal SubjectKey As String, ByVal Trx As SqlTransaction)
    Dim _str_bld As StringBuilder
    Dim _value As String

    _value = ""

    _str_bld = New StringBuilder()
    _str_bld.AppendLine("SELECT   GP_KEY_VALUE                  ")
    _str_bld.AppendLine("  FROM   GENERAL_PARAMS                ")
    _str_bld.AppendLine(" WHERE   GP_GROUP_KEY = @pGroupKey     ")
    _str_bld.AppendLine("   AND   GP_SUBJECT_KEY = @pSubjectKey ")

    Using _sql_cmd As New SqlCommand(_str_bld.ToString())
      _sql_cmd.Connection = Trx.Connection
      _sql_cmd.Transaction = Trx

      _sql_cmd.Parameters.Add("@pGroupKey", SqlDbType.NVarChar).Value = GroupKey
      _sql_cmd.Parameters.Add("@pSubjectKey", SqlDbType.NVarChar).Value = SubjectKey

      _value = _sql_cmd.ExecuteScalar()
    End Using

    Return _value
  End Function

  ' PURPOSE: UpdateConfiguration
  '                  
  '  PARAMS:         
  '     - INPUT:     
  '           - 
  '     - OUTPUT:    
  '           -  LocalData As TYPE_ADS_CONFIGURATION, 
  '                  
  ' RETURNS:         
  '     - Integer
  '                  
  Private Function UpdateConfiguration(ByRef LocalData As TYPE_MULTISITE_CONFIG) As Integer
    Dim _do_commit As Boolean
    Dim _rc As Integer
    Dim _status_gp_value As Int32
    Dim _sas_flags As Integer

    _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    _do_commit = False
    _status_gp_value = 0
    _sas_flags = SAS_FLAGS.NONE

    Using _db_trx As New DB_TRX()
      Try
        ' MultiSite Member
        If Me.m_multisite_config.status = ENUM_MULTISITE_STATUS.STATUS_ACTIVATED Then
          _status_gp_value = 1
        End If

        ' SAS FLAGS
        If m_extended_counters Then
          _sas_flags = _sas_flags Or SAS_FLAGS.USE_EXTENDED_METERS
        End If

        If m_promotional_credits = ENUM_SAS_PROMOTION_FLAGS.MACHINE_DEFINED Then
          _sas_flags = _sas_flags Or SAS_FLAGS.CREDITS_PLAY_MODE_SAS_MACHINE
        End If

        If m_handPays = ENUM_SAS_FLAGS.ACTIVATED Then
          _sas_flags = _sas_flags Or SAS_FLAGS.USE_HANDPAY_INFORMATION
        ElseIf m_handPays = ENUM_SAS_FLAGS.ACTIVATED_AND_AUTOMATICALLY Then
          _sas_flags = _sas_flags Or SAS_FLAGS.USE_HANDPAY_INFORMATION
          _sas_flags = _sas_flags Or SAS_FLAGS.SAS_FLAGS_AUTOMATIC_HANDPAY_PAYMENT
        End If

        If m_progressive_meters Then
          _sas_flags = _sas_flags Or SAS_FLAGS.SPECIAL_PROGRESSIVE_METER
        End If

        If m_enable_disable_note_acceptor Then
          _sas_flags = _sas_flags Or SAS_FLAGS.SAS_FLAGS_ENABLE_DISABLE_NOTE_ACCEPTOR
        End If

        If m_reserve_terminal Then
          _sas_flags = _sas_flags Or SAS_FLAGS.SAS_FLAGS_RESERVE_TERMINAL
        End If

        If m_lock_egm_on_reserve Then
          _sas_flags = _sas_flags Or SAS_FLAGS.SAS_FLAGS_LOCK_EGM_ON_RESERVE
        End If

        Dim _gp_update_list As New List(Of String())

        _gp_update_list.Add(GeneralParam.AddToList("Site", "Configured", Me.m_multisite_config.site_configured))
        _gp_update_list.Add(GeneralParam.AddToList("Site", "Identifier", Me.m_multisite_config.site_id))
        _gp_update_list.Add(GeneralParam.AddToList("Site", "Name", Me.m_multisite_config.site_name))
        _gp_update_list.Add(GeneralParam.AddToList("Site", "MultiSiteMember", _status_gp_value))
        _gp_update_list.Add(GeneralParam.AddToList("MultiSite", "CenterAddress1", Me.m_multisite_config.center_address_1))
        _gp_update_list.Add(GeneralParam.AddToList("MultiSite", "CenterAddress2", Me.m_multisite_config.center_address_2))
        _gp_update_list.Add(GeneralParam.AddToList("SasHost", "SystemDefaultSasFlags", _sas_flags))

        ' Now MembersCardId it gets from Center
        ' and isn't editable in the site if Site is member of a Multisite.
        ' it is editable if site is a single site

        ' MultiSite Members Card ID
        If Me.m_multisite_config.members_card_id_enable Then
          _gp_update_list.Add(GeneralParam.AddToList("MultiSite", "MembersCardId", Me.m_multisite_config.members_card_id))
        End If

        For Each _general_param As String() In _gp_update_list
          If (Not GUI_Controls.DB_GeneralParam_Update(_general_param(GeneralParam.COLUMN_NAME.Group),
                                                      _general_param(GeneralParam.COLUMN_NAME.Subject),
                                                      _general_param(GeneralParam.COLUMN_NAME.Value),
                                                      _db_trx.SqlTransaction)) Then

            Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
            Exit For
          End If
        Next

        ' Activate MultiSite Triggers
        If Me.m_multisite_config.site_configured = 1 Then
          'Call MultiSiteTriggersEnable SP
          Dim _str_bld As StringBuilder
          Dim _sql_command As SqlCommand

          _str_bld = New StringBuilder

          _str_bld.AppendLine("EXECUTE  MultiSiteTriggersEnable ")
          _str_bld.AppendLine("         @pEnable")

          _sql_command = New SqlCommand(_str_bld.ToString())
          _sql_command.Connection = _db_trx.SqlTransaction.Connection
          _sql_command.Transaction = _db_trx.SqlTransaction
          'TODO Defect
          'm_multisite_config.status: 0 is not multisite member
          '                           1 is multisite member
          _sql_command.Parameters.Add("@pEnable", SqlDbType.Bit).Value = CType(Me.m_multisite_config.status, Integer)

          _sql_command.ExecuteNonQuery()
        End If

        ' No error
        _do_commit = True

        _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_OK

      Catch _ex As Exception
        _do_commit = False
        _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                              "cls_multisite_configuration", _
                              "UpdateConfiguration", _
                              _ex.Message)
      Finally

        GUI_EndSQLTransaction(_db_trx.SqlTransaction, _do_commit)
      End Try
    End Using

    Return _rc

  End Function 'UpdateConfigurationDataTable

#End Region ' Private functions 

#Region " Public functions"

  ' PURPOSE: New
  '                  
  '  PARAMS:         
  '     - INPUT:     
  '           - None
  '     - OUTPUT:    
  '           - None
  '                  
  ' RETURNS:         
  '     -None
  '                  
  Public Sub New()
    Me.m_multisite_config = New TYPE_MULTISITE_CONFIG()
  End Sub 'New

#End Region ' Public functions 

End Class
