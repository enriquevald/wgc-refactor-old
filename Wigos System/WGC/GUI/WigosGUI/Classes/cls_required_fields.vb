'------------------------------------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'------------------------------------------------------------------------------------------------
'
' MODULE NAME : cls_required_fields_configuration.vb
'
' DESCRIPTION : 
'
' REVISION HISTORY:
' 
' Date         Author Description
' -----------  ------ ---------------------------------------------------------------------------
' 28-OCT-2013  ACM    Fixed Bug #WIG-322 Wrong auditor code and change AccountCustomize audit message.
' 12-FEB-2014  FBA    Added account holder id type control via CardData class IdentificationTypes
' 14-JUL-2014  JBC    New class
' 12-AUG-2014  RCI    Fixed Bug WIG-1180: Wrong audit message when changing Surname2.
' 03-OCT-2017  AMF    WIGOS-5478 Accounts - Change literals
' 13-FEB-2018  EOR    Bug 31515: WIGOS-8032 [Ticket #12229] Formulario de inscripci�n de jugadores en registro por l�mite de pago 
' -----------  ------ ---------------------------------------------------------------------------

#Region " Imports "

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports WSI.Common
Imports WSI.Common.OperationsSchedule
Imports System.Text
Imports System.Data.SqlClient
Imports GUI_Controls.Misc

#End Region ' Imports

Public Class CLS_REQUIRED_FIELDS
  Inherits CLASS_BASE

  ' New type to store data from General Param
  Public Class Config_Param
    Public Visible As Boolean = False
    Public Required As Boolean = False
    Public AML As Boolean = False
  End Class

  Public Class CLS_ACCOUNT_REQUIRED_FIELD

#Region " Members "

    'principal
    Private m_account_customize_enabled As String
    Private m_name1 As New Config_Param
    Private m_name2 As Config_Param
    Private m_name3 As Config_Param
    Private m_name4 As Config_Param
    Private m_document As Config_Param
    Private m_doc_scan As Config_Param
    Private m_birthdate As Config_Param
    Private m_occupation As Config_Param
    Private m_gender As Config_Param
    Private m_nationality As Config_Param
    Private m_birthcountry As Config_Param
    Private m_marital_status As Config_Param
    Private m_wedding_date As Config_Param
    Private m_photo As Config_Param
    Private m_doc_expiration As Config_Param

    'contact
    Private m_phone1 As Config_Param
    Private m_phone2 As Config_Param
    Private m_email1 As Config_Param
    Private m_email2 As Config_Param
    Private m_twitter_account As Config_Param

    'address
    Private m_address01 As Config_Param
    Private m_extnum As Config_Param
    Private m_address02 As Config_Param
    Private m_address03 As Config_Param
    Private m_city As Config_Param
    Private m_zip As Config_Param
    Private m_address_country As Config_Param
    Private m_fed_entity As Config_Param

    Private m_document_type_list_required As String
    Private m_doc_scan_type_list_required As String

    Private m_document_type_list_AML As String
    Private m_doc_scan_type_list_AML As String

    Private m_document_type_list_Visible As String
    Private m_doc_scan_type_list_Visible As String

#End Region

#Region " Properties "

    Public Property AccountCustomizeEnabled() As String
      Get
        Return m_account_customize_enabled
      End Get
      Set(ByVal value As String)
        m_account_customize_enabled = value
      End Set
    End Property

    Public ReadOnly Property Name1() As Config_Param
      Get
        Return Me.m_name1
      End Get
    End Property

    Public ReadOnly Property Name2() As Config_Param
      Get
        Return Me.m_name2
      End Get
    End Property

    Public ReadOnly Property Name3() As Config_Param
      Get
        Return Me.m_name3
      End Get
    End Property

    Public ReadOnly Property Name4() As Config_Param
      Get
        Return Me.m_name4
      End Get
    End Property

    Public ReadOnly Property Document() As Config_Param
      Get
        Return m_document
      End Get
    End Property

    Public ReadOnly Property DocScan() As Config_Param
      Get
        Return Me.m_doc_scan
      End Get
    End Property

    Public ReadOnly Property Birthdate() As Config_Param
      Get
        Return m_birthdate
      End Get
    End Property

    Public ReadOnly Property Occupation() As Config_Param
      Get
        Return Me.m_occupation
      End Get
    End Property

    Public ReadOnly Property Gender() As Config_Param
      Get
        Return Me.m_gender
      End Get
    End Property

    Public ReadOnly Property Nationality() As Config_Param
      Get
        Return Me.m_nationality
      End Get
    End Property

    Public ReadOnly Property BirthCountry() As Config_Param
      Get
        Return m_birthcountry
      End Get
    End Property

    Public ReadOnly Property MaritalStatus() As Config_Param
      Get
        Return Me.m_marital_status
      End Get
    End Property

    Public ReadOnly Property WeddingDate() As Config_Param
      Get
        Return Me.m_wedding_date
      End Get
    End Property

    Public ReadOnly Property Photo() As Config_Param
      Get
        Return Me.m_photo
      End Get
    End Property

    Public ReadOnly Property ExpirationDocumentDate() As Config_Param
      Get
        Return Me.m_doc_expiration
      End Get
    End Property



    Public ReadOnly Property Phone1() As Config_Param
      Get
        Return Me.m_phone1
      End Get
    End Property

    Public ReadOnly Property Phone2() As Config_Param
      Get
        Return Me.m_phone2
      End Get
    End Property

    Public ReadOnly Property Email1() As Config_Param
      Get
        Return Me.m_email1
      End Get
    End Property

    Public ReadOnly Property Email2() As Config_Param
      Get
        Return Me.m_email2
      End Get
    End Property

    Public ReadOnly Property TwitterAccount() As Config_Param
      Get
        Return Me.m_twitter_account
      End Get
    End Property


    Public ReadOnly Property Address01() As Config_Param
      Get
        Return m_address01
      End Get
    End Property

    Public ReadOnly Property Address02() As Config_Param
      Get
        Return m_address02
      End Get
    End Property

    Public ReadOnly Property Address03() As Config_Param
      Get
        Return m_address03
      End Get
    End Property

    Public ReadOnly Property ExtNum() As Config_Param
      Get
        Return Me.m_extnum
      End Get
    End Property

    Public ReadOnly Property City() As Config_Param
      Get
        Return Me.m_city
      End Get
    End Property

    Public ReadOnly Property Zip() As Config_Param
      Get
        Return Me.m_zip
      End Get
    End Property

    Public ReadOnly Property AddressCountry() As Config_Param
      Get
        Return Me.m_address_country
      End Get
    End Property

    Public ReadOnly Property FedEntity() As Config_Param
      Get
        Return Me.m_fed_entity
      End Get
    End Property


    Public Property DocumentTypeList() As String
      Get
        Return m_document_type_list_required
      End Get
      Set(ByVal value As String)
        m_document_type_list_required = value
      End Set
    End Property

    Public Property DocScanTypeList() As String
      Get
        Return Me.m_doc_scan_type_list_required
      End Get
      Set(ByVal value As String)
        Me.m_doc_scan_type_list_required = value
      End Set
    End Property

    Public Property AMLDocScanTypeList() As String
      Get
        Return Me.m_doc_scan_type_list_AML
      End Get
      Set(ByVal value As String)
        Me.m_doc_scan_type_list_AML = value
      End Set
    End Property

    Public Property AMLDocumentTypeList() As String
      Get
        Return Me.m_document_type_list_AML
      End Get
      Set(ByVal value As String)
        Me.m_document_type_list_AML = value
      End Set
    End Property

    Public Property DocScanTypeListVisible() As String
      Get
        Return Me.m_doc_scan_type_list_AML
      End Get
      Set(ByVal value As String)
        Me.m_doc_scan_type_list_AML = value
      End Set
    End Property

    Public Property DocumentTypeListVisible() As String
      Get
        Return Me.m_document_type_list_Visible
      End Get
      Set(ByVal value As String)
        Me.m_document_type_list_Visible = value
      End Set
    End Property

#End Region

    Public Sub New()
      m_name1 = New Config_Param()
      m_name2 = New Config_Param()
      m_name3 = New Config_Param()
      m_name4 = New Config_Param()
      m_document = New Config_Param()
      m_doc_scan = New Config_Param()
      m_birthdate = New Config_Param()
      m_occupation = New Config_Param()
      m_gender = New Config_Param()
      m_nationality = New Config_Param()
      m_birthcountry = New Config_Param()
      m_marital_status = New Config_Param()
      m_wedding_date = New Config_Param()
      m_photo = New Config_Param()
      m_doc_expiration = New Config_Param()

      m_phone1 = New Config_Param()
      m_phone2 = New Config_Param()
      m_email1 = New Config_Param()
      m_email2 = New Config_Param()
      m_twitter_account = New Config_Param()

      m_address01 = New Config_Param()
      m_extnum = New Config_Param()
      m_address02 = New Config_Param()
      m_address03 = New Config_Param()
      m_city = New Config_Param()
      m_zip = New Config_Param()
      m_address_country = New Config_Param()
      m_fed_entity = New Config_Param()
    End Sub

  End Class

  Public Class CLS_BENEFICIARY_REQUIRED_FIELD

#Region " Members "

    'principal
    Private m_name1 As Config_Param
    Private m_name2 As Config_Param
    Private m_name3 As Config_Param
    Private m_document As Config_Param
    Private m_doc_scan As Config_Param
    Private m_birthdate As Config_Param
    Private m_occupation As Config_Param
    Private m_gender As Config_Param

    Private m_document_type_list_required As String
    Private m_doc_scan_type_list_required As String

    Private m_document_type_list_AML As String
    Private m_doc_scan_type_list_AML As String

    Private m_document_type_list_Visible As String
    Private m_doc_scan_type_list_Visible As String

#End Region

#Region " Properties "
    Public ReadOnly Property Name1() As Config_Param
      Get
        Return Me.m_name1
      End Get
    End Property

    Public ReadOnly Property Name2() As Config_Param
      Get
        Return Me.m_name2
      End Get
    End Property

    Public ReadOnly Property Name3() As Config_Param
      Get
        Return Me.m_name3
      End Get
    End Property


    Public ReadOnly Property Document() As Config_Param
      Get
        Return m_document
      End Get
    End Property

    Public ReadOnly Property DocScan() As Config_Param
      Get
        Return Me.m_doc_scan
      End Get
    End Property

    Public ReadOnly Property Birthdate() As Config_Param
      Get
        Return m_birthdate
      End Get
    End Property

    Public ReadOnly Property Occupation() As Config_Param
      Get
        Return Me.m_occupation
      End Get
    End Property

    Public ReadOnly Property Gender() As Config_Param
      Get
        Return Me.m_gender
      End Get
    End Property

    Public Property DocumentTypeList() As String
      Get
        Return m_document_type_list_required
      End Get
      Set(ByVal value As String)
        m_document_type_list_required = value
      End Set
    End Property

    Public Property DocScanTypeList() As String
      Get
        Return Me.m_doc_scan_type_list_required
      End Get
      Set(ByVal value As String)
        Me.m_doc_scan_type_list_required = value
      End Set
    End Property

    Public Property AMLDocScanTypeList() As String
      Get
        Return Me.m_doc_scan_type_list_AML
      End Get
      Set(ByVal value As String)
        Me.m_doc_scan_type_list_AML = value
      End Set
    End Property

    Public Property AMLDocumentTypeList() As String
      Get
        Return Me.m_document_type_list_AML
      End Get
      Set(ByVal value As String)
        Me.m_document_type_list_AML = value
      End Set
    End Property

    Public Property DocScanTypeListVisible() As String
      Get
        Return Me.m_doc_scan_type_list_AML
      End Get
      Set(ByVal value As String)
        Me.m_doc_scan_type_list_AML = value
      End Set
    End Property

    Public Property DocumentTypeListVisible() As String
      Get
        Return Me.m_document_type_list_Visible
      End Get
      Set(ByVal value As String)
        Me.m_document_type_list_Visible = value
      End Set
    End Property

#End Region

    Public Sub New()
      m_name1 = New Config_Param()
      m_name2 = New Config_Param()
      m_name3 = New Config_Param()
      m_document = New Config_Param()
      m_doc_scan = New Config_Param()
      m_birthdate = New Config_Param()
      m_occupation = New Config_Param()
      m_gender = New Config_Param()
    End Sub
  End Class

#Region " Members "

  Dim m_account_required_field As CLS_ACCOUNT_REQUIRED_FIELD = New CLS_ACCOUNT_REQUIRED_FIELD()
  Dim m_beneficiary_required_field As CLS_BENEFICIARY_REQUIRED_FIELD = New CLS_BENEFICIARY_REQUIRED_FIELD()

  Private m_dt_identification_types As DataTable
  Private m_dic_countries As Dictionary(Of String, String)

  Private m_default_document As String
  Private m_default_country As String
  Private m_visible_documents As String

#End Region

#Region " Properties "

  Public ReadOnly Property AccountRequiredField() As CLS_ACCOUNT_REQUIRED_FIELD
    Get
      Return Me.m_account_required_field
    End Get
  End Property

  Public ReadOnly Property BeneficiaryRequiredField() As CLS_BENEFICIARY_REQUIRED_FIELD
    Get
      Return Me.m_beneficiary_required_field
    End Get
  End Property

  Public Property IdentificationTypes() As DataTable
    Get
      Return Me.m_dt_identification_types
    End Get
    Set(ByVal value As DataTable)
      Me.m_dt_identification_types = value
    End Set
  End Property

  Public Property DefaultDocument() As String
    Get
      Return Me.m_default_document
    End Get
    Set(ByVal value As String)
      m_default_document = value
    End Set
  End Property

  Public Property DefaultCountry() As String
    Get
      Return Me.m_default_country
    End Get
    Set(ByVal value As String)
      Me.m_default_country = value
    End Set
  End Property

  Public ReadOnly Property ListOfCountries() As Dictionary(Of String, String)
    Get
      Return Me.m_dic_countries
    End Get
  End Property

  Public Property VisibleDocuments() As String
    Get
      Return Me.m_visible_documents
    End Get
    Set(ByVal value As String)
      Me.m_visible_documents = value
    End Set
  End Property

#End Region

#Region " Overrides "

  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim _auditor_data As CLASS_AUDITOR_DATA

    ' TODO: AUDIT CODE
    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_CASHIER_CONFIGURATION)
    ' TODO: SETNAME
    Call _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(2477), "")

    Call FillAuditorData(_auditor_data, Me)

    Return _auditor_data

  End Function

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Return ENUM_STATUS.STATUS_NOT_SUPPORTED

  End Function

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Return ENUM_STATUS.STATUS_NOT_SUPPORTED

  End Function

  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As ENUM_STATUS

    Using _db_trx As New DB_TRX()

      _rc = Me.Read(_db_trx.SqlTransaction)

    End Using ' _db_trx

    Return _rc

  End Function

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _rc As ENUM_STATUS

    _rc = ENUM_STATUS.STATUS_ERROR

    Using _db_trx As New DB_TRX()

      _rc = Me.Update(_db_trx.SqlTransaction)

      If _rc <> ENUM_STATUS.STATUS_OK Then

        Return _rc
      End If

      If _db_trx.Commit() Then

        Return ENUM_STATUS.STATUS_OK
      End If

    End Using ' _db_trx

    Return _rc

  End Function

  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim _clone As CLS_REQUIRED_FIELDS

    _clone = New CLS_REQUIRED_FIELDS()

    _clone.DefaultCountry = Me.m_default_country
    _clone.DefaultDocument = Me.m_default_document
    _clone.AccountRequiredField.AccountCustomizeEnabled = Me.m_account_required_field.AccountCustomizeEnabled

    _clone.m_account_required_field.Name1.Visible = Me.m_account_required_field.Name1.Visible
    _clone.m_account_required_field.Name1.Required = Me.m_account_required_field.Name1.Required
    _clone.m_account_required_field.Name1.AML = Me.m_account_required_field.Name1.AML

    _clone.m_account_required_field.Name2.Visible = Me.m_account_required_field.Name2.Visible
    _clone.m_account_required_field.Name2.Required = Me.m_account_required_field.Name2.Required
    _clone.m_account_required_field.Name2.AML = Me.m_account_required_field.Name2.AML

    _clone.m_account_required_field.Name3.Visible = Me.m_account_required_field.Name3.Visible
    _clone.m_account_required_field.Name3.Required = Me.m_account_required_field.Name3.Required
    _clone.m_account_required_field.Name3.AML = Me.m_account_required_field.Name3.AML

    _clone.m_account_required_field.Name4.Visible = Me.m_account_required_field.Name4.Visible
    _clone.m_account_required_field.Name4.Required = Me.m_account_required_field.Name4.Required
    _clone.m_account_required_field.Name4.AML = Me.m_account_required_field.Name4.AML

    _clone.m_account_required_field.Document.Visible = Me.m_account_required_field.Document.Visible
    _clone.m_account_required_field.Document.Required = Me.m_account_required_field.Document.Required
    _clone.m_account_required_field.Document.AML = Me.m_account_required_field.Document.AML

    _clone.m_account_required_field.DocScan.Visible = Me.m_account_required_field.DocScan.Visible
    _clone.m_account_required_field.DocScan.Required = Me.m_account_required_field.DocScan.Required
    _clone.m_account_required_field.DocScan.AML = Me.m_account_required_field.DocScan.AML

    _clone.m_account_required_field.Birthdate.Visible = Me.m_account_required_field.Birthdate.Visible
    _clone.m_account_required_field.Birthdate.Required = Me.m_account_required_field.Birthdate.Required
    _clone.m_account_required_field.Birthdate.AML = Me.m_account_required_field.Birthdate.AML

    _clone.m_account_required_field.Occupation.Visible = Me.m_account_required_field.Occupation.Visible
    _clone.m_account_required_field.Occupation.Required = Me.m_account_required_field.Occupation.Required
    _clone.m_account_required_field.Occupation.AML = Me.m_account_required_field.Occupation.AML

    _clone.m_account_required_field.Gender.Visible = Me.m_account_required_field.Gender.Visible
    _clone.m_account_required_field.Gender.Required = Me.m_account_required_field.Gender.Required
    _clone.m_account_required_field.Gender.AML = Me.m_account_required_field.Gender.AML

    _clone.m_account_required_field.Nationality.Visible = Me.m_account_required_field.Nationality.Visible
    _clone.m_account_required_field.Nationality.Required = Me.m_account_required_field.Nationality.Required
    _clone.m_account_required_field.Nationality.AML = Me.m_account_required_field.Nationality.AML

    _clone.m_account_required_field.BirthCountry.Visible = Me.m_account_required_field.BirthCountry.Visible
    _clone.m_account_required_field.BirthCountry.Required = Me.m_account_required_field.BirthCountry.Required
    _clone.m_account_required_field.BirthCountry.AML = Me.m_account_required_field.BirthCountry.AML

    _clone.m_account_required_field.MaritalStatus.Visible = Me.m_account_required_field.MaritalStatus.Visible
    _clone.m_account_required_field.MaritalStatus.Required = Me.m_account_required_field.MaritalStatus.Required
    _clone.m_account_required_field.MaritalStatus.AML = Me.m_account_required_field.MaritalStatus.AML

    _clone.m_account_required_field.WeddingDate.Visible = Me.m_account_required_field.WeddingDate.Visible
    _clone.m_account_required_field.WeddingDate.Required = Me.m_account_required_field.WeddingDate.Required
    _clone.m_account_required_field.WeddingDate.AML = Me.m_account_required_field.WeddingDate.AML

    _clone.m_account_required_field.Photo.Visible = Me.m_account_required_field.Photo.Visible
    _clone.m_account_required_field.Photo.Required = Me.m_account_required_field.Photo.Required
    _clone.m_account_required_field.Photo.AML = Me.m_account_required_field.Photo.AML

    _clone.m_account_required_field.ExpirationDocumentDate.Visible = Me.m_account_required_field.ExpirationDocumentDate.Visible
    _clone.m_account_required_field.ExpirationDocumentDate.Required = Me.m_account_required_field.ExpirationDocumentDate.Required
    _clone.m_account_required_field.ExpirationDocumentDate.AML = Me.m_account_required_field.ExpirationDocumentDate.AML

    _clone.m_account_required_field.Phone1.Visible = Me.m_account_required_field.Phone1.Visible
    _clone.m_account_required_field.Phone1.Required = Me.m_account_required_field.Phone1.Required
    _clone.m_account_required_field.Phone1.AML = Me.m_account_required_field.Phone1.AML

    _clone.m_account_required_field.Phone2.Visible = Me.m_account_required_field.Phone2.Visible
    _clone.m_account_required_field.Phone2.Required = Me.m_account_required_field.Phone2.Required
    _clone.m_account_required_field.Phone2.AML = Me.m_account_required_field.Phone2.AML

    _clone.m_account_required_field.Email1.Visible = Me.m_account_required_field.Email1.Visible
    _clone.m_account_required_field.Email1.Required = Me.m_account_required_field.Email1.Required
    _clone.m_account_required_field.Email1.AML = Me.m_account_required_field.Email1.AML

    _clone.m_account_required_field.Email2.Visible = Me.m_account_required_field.Email2.Visible
    _clone.m_account_required_field.Email2.Required = Me.m_account_required_field.Email2.Required
    _clone.m_account_required_field.Email2.AML = Me.m_account_required_field.Email2.AML

    _clone.m_account_required_field.TwitterAccount.Visible = Me.m_account_required_field.TwitterAccount.Visible
    _clone.m_account_required_field.TwitterAccount.Required = Me.m_account_required_field.TwitterAccount.Required
    _clone.m_account_required_field.TwitterAccount.AML = Me.m_account_required_field.TwitterAccount.AML

    _clone.m_account_required_field.Address01.Visible = Me.m_account_required_field.Address01.Visible
    _clone.m_account_required_field.Address01.Required = Me.m_account_required_field.Address01.Required
    _clone.m_account_required_field.Address01.AML = Me.m_account_required_field.Address01.AML

    _clone.m_account_required_field.Address02.Visible = Me.m_account_required_field.Address02.Visible
    _clone.m_account_required_field.Address02.Required = Me.m_account_required_field.Address02.Required
    _clone.m_account_required_field.Address02.AML = Me.m_account_required_field.Address02.AML

    _clone.m_account_required_field.Address03.Visible = Me.m_account_required_field.Address03.Visible
    _clone.m_account_required_field.Address03.Required = Me.m_account_required_field.Address03.Required
    _clone.m_account_required_field.Address03.AML = Me.m_account_required_field.Address03.AML

    _clone.m_account_required_field.ExtNum.Visible = Me.m_account_required_field.ExtNum.Visible
    _clone.m_account_required_field.ExtNum.Required = Me.m_account_required_field.ExtNum.Required
    _clone.m_account_required_field.ExtNum.AML = Me.m_account_required_field.ExtNum.AML

    _clone.m_account_required_field.City.Visible = Me.m_account_required_field.City.Visible
    _clone.m_account_required_field.City.Required = Me.m_account_required_field.City.Required
    _clone.m_account_required_field.City.AML = Me.m_account_required_field.City.AML

    _clone.m_account_required_field.Zip.Visible = Me.m_account_required_field.Zip.Visible
    _clone.m_account_required_field.Zip.Required = Me.m_account_required_field.Zip.Required
    _clone.m_account_required_field.Zip.AML = Me.m_account_required_field.Zip.AML

    _clone.m_account_required_field.AddressCountry.Visible = Me.m_account_required_field.AddressCountry.Visible
    _clone.m_account_required_field.AddressCountry.Required = Me.m_account_required_field.AddressCountry.Required
    _clone.m_account_required_field.AddressCountry.AML = Me.m_account_required_field.AddressCountry.AML

    _clone.m_account_required_field.FedEntity.Visible = Me.m_account_required_field.FedEntity.Visible
    _clone.m_account_required_field.FedEntity.Required = Me.m_account_required_field.FedEntity.Required
    _clone.m_account_required_field.FedEntity.AML = Me.m_account_required_field.FedEntity.AML

    _clone.m_account_required_field.DocumentTypeList = Me.m_account_required_field.DocumentTypeList
    _clone.m_account_required_field.DocScanTypeList = Me.m_account_required_field.DocScanTypeList

    _clone.m_account_required_field.AMLDocumentTypeList = Me.m_account_required_field.AMLDocumentTypeList
    _clone.m_account_required_field.AMLDocScanTypeList = Me.m_account_required_field.AMLDocScanTypeList

    ' BENEFICIARY
    _clone.m_beneficiary_required_field.Name1.Required = Me.m_beneficiary_required_field.Name1.Required
    _clone.m_beneficiary_required_field.Name1.AML = Me.m_beneficiary_required_field.Name1.AML

    _clone.m_beneficiary_required_field.Name2.Required = Me.m_beneficiary_required_field.Name2.Required
    _clone.m_beneficiary_required_field.Name2.AML = Me.m_beneficiary_required_field.Name2.AML

    _clone.m_beneficiary_required_field.Name3.Required = Me.m_beneficiary_required_field.Name3.Required
    _clone.m_beneficiary_required_field.Name3.AML = Me.m_beneficiary_required_field.Name3.AML

    _clone.m_beneficiary_required_field.Document.Required = Me.m_beneficiary_required_field.Document.Required
    _clone.m_beneficiary_required_field.Document.AML = Me.m_beneficiary_required_field.Document.AML

    _clone.m_beneficiary_required_field.DocScan.Required = Me.m_beneficiary_required_field.DocScan.Required
    _clone.m_beneficiary_required_field.DocScan.AML = Me.m_beneficiary_required_field.DocScan.AML

    _clone.m_beneficiary_required_field.Birthdate.Required = Me.m_beneficiary_required_field.Birthdate.Required
    _clone.m_beneficiary_required_field.Birthdate.AML = Me.m_beneficiary_required_field.Birthdate.AML

    _clone.m_beneficiary_required_field.Occupation.Required = Me.m_beneficiary_required_field.Occupation.Required
    _clone.m_beneficiary_required_field.Occupation.AML = Me.m_beneficiary_required_field.Occupation.AML

    _clone.m_beneficiary_required_field.Gender.Required = Me.m_beneficiary_required_field.Gender.Required
    _clone.m_beneficiary_required_field.Gender.AML = Me.m_beneficiary_required_field.Gender.AML

    _clone.m_beneficiary_required_field.DocumentTypeList = Me.m_beneficiary_required_field.DocumentTypeList
    _clone.m_beneficiary_required_field.DocScanTypeList = Me.m_beneficiary_required_field.DocScanTypeList

    _clone.m_beneficiary_required_field.AMLDocumentTypeList = Me.m_beneficiary_required_field.AMLDocumentTypeList
    _clone.m_beneficiary_required_field.AMLDocScanTypeList = Me.m_beneficiary_required_field.AMLDocScanTypeList
    _clone.m_dic_countries = Me.m_dic_countries
    _clone.m_dt_identification_types = Me.m_dt_identification_types.Copy()
    _clone.VisibleDocuments = Me.VisibleDocuments

    Return _clone

  End Function

#End Region

#Region " Public Functions "

  Public Sub New()

    Me.m_dt_identification_types = New DataTable()

    m_dic_countries = New Dictionary(Of String, String)

  End Sub ' New

  Public Function Read(ByVal Trx As SqlTransaction) As ENUM_STATUS

    Dim _gp As GeneralParam.Dictionary
    Dim _identification_types As List(Of String)
    Dim _ident_value As Integer

    'Populate General Params dictionary with the current database values
    _gp = GeneralParam.Dictionary.Create(Trx)

    _identification_types = New List(Of String)

    Try

      Me.m_account_required_field.Name1.Visible = _gp.GetBoolean("Account.VisibleField", "Name1")
      Me.m_account_required_field.Name1.Required = _gp.GetBoolean("Account.RequestedField", "Name1")
      Me.m_account_required_field.Name1.AML = _gp.GetBoolean("Account.RequestedField", "AntiMoneyLaundering.Name1")

      Me.m_account_required_field.Name2.Visible = _gp.GetBoolean("Account.VisibleField", "Name2")
      Me.m_account_required_field.Name2.Required = _gp.GetBoolean("Account.RequestedField", "Name2")
      Me.m_account_required_field.Name2.AML = _gp.GetBoolean("Account.RequestedField", "AntiMoneyLaundering.Name2")

      Me.m_account_required_field.Name3.Visible = _gp.GetBoolean("Account.VisibleField", "Name3")
      Me.m_account_required_field.Name3.Required = _gp.GetBoolean("Account.RequestedField", "Name3")
      Me.m_account_required_field.Name3.AML = _gp.GetBoolean("Account.RequestedField", "AntiMoneyLaundering.Name3")

      Me.m_account_required_field.Name4.Visible = _gp.GetBoolean("Account.VisibleField", "Name4")
      Me.m_account_required_field.Name4.Required = _gp.GetBoolean("Account.RequestedField", "Name4")
      Me.m_account_required_field.Name4.AML = _gp.GetBoolean("Account.RequestedField", "AntiMoneyLaundering.Name4")

      Me.m_account_required_field.Document.Visible = _gp.GetBoolean("Account.VisibleField", "Document")
      Me.m_account_required_field.Document.Required = _gp.GetBoolean("Account.RequestedField", "Document")
      Me.m_account_required_field.Document.AML = _gp.GetBoolean("Account.RequestedField", "AntiMoneyLaundering.Document")

      Me.m_account_required_field.DocScan.Visible = _gp.GetBoolean("Account.VisibleField", "DocScan")
      Me.m_account_required_field.DocScan.Required = _gp.GetBoolean("Account.RequestedField", "DocScan")
      Me.m_account_required_field.DocScan.AML = _gp.GetBoolean("Account.RequestedField", "AntiMoneyLaundering.DocScan")

      Me.m_account_required_field.Birthdate.Visible = _gp.GetBoolean("Account.VisibleField", "BirthDate")
      Me.m_account_required_field.Birthdate.Required = _gp.GetBoolean("Account.RequestedField", "BirthDate")
      Me.m_account_required_field.Birthdate.AML = _gp.GetBoolean("Account.RequestedField", "AntiMoneyLaundering.BirthDate")

      Me.m_account_required_field.Occupation.Visible = _gp.GetBoolean("Account.VisibleField", "Occupation")
      Me.m_account_required_field.Occupation.Required = _gp.GetBoolean("Account.RequestedField", "Occupation")
      Me.m_account_required_field.Occupation.AML = _gp.GetBoolean("Account.RequestedField", "AntiMoneyLaundering.Occupation")

      Me.m_account_required_field.Gender.Visible = _gp.GetBoolean("Account.VisibleField", "Gender")
      Me.m_account_required_field.Gender.Required = _gp.GetBoolean("Account.RequestedField", "Gender")
      Me.m_account_required_field.Gender.AML = _gp.GetBoolean("Account.RequestedField", "AntiMoneyLaundering.Gender")

      Me.m_account_required_field.Nationality.Visible = _gp.GetBoolean("Account.VisibleField", "Nationality")
      Me.m_account_required_field.Nationality.Required = _gp.GetBoolean("Account.RequestedField", "Nationality")
      Me.m_account_required_field.Nationality.AML = _gp.GetBoolean("Account.RequestedField", "AntiMoneyLaundering.Nationality")

      Me.m_account_required_field.BirthCountry.Visible = _gp.GetBoolean("Account.VisibleField", "BirthCountry")
      Me.m_account_required_field.BirthCountry.Required = _gp.GetBoolean("Account.RequestedField", "BirthCountry")
      Me.m_account_required_field.BirthCountry.AML = _gp.GetBoolean("Account.RequestedField", "AntiMoneyLaundering.BirthCountry")

      Me.m_account_required_field.MaritalStatus.Visible = _gp.GetBoolean("Account.VisibleField", "MaritalStatus")
      Me.m_account_required_field.MaritalStatus.Required = _gp.GetBoolean("Account.RequestedField", "MaritalStatus")
      Me.m_account_required_field.MaritalStatus.AML = _gp.GetBoolean("Account.RequestedField", "AntiMoneyLaundering.MaritalStatus")

      Me.m_account_required_field.WeddingDate.Visible = _gp.GetBoolean("Account.VisibleField", "WeddingDate")
      Me.m_account_required_field.WeddingDate.Required = _gp.GetBoolean("Account.RequestedField", "WeddingDate")
      Me.m_account_required_field.WeddingDate.AML = _gp.GetBoolean("Account.RequestedField", "AntiMoneyLaundering.WeddingDate")

      Me.m_account_required_field.Photo.Visible = _gp.GetBoolean("Account.VisibleField", "Photo", True)
      Me.m_account_required_field.Photo.Required = _gp.GetBoolean("Account.RequestedField", "Photo", False)
      Me.m_account_required_field.Photo.AML = _gp.GetBoolean("Account.RequestedField", "AntiMoneyLaundering.Photo", False)

      Me.m_account_required_field.ExpirationDocumentDate.Visible = _gp.GetBoolean("Account.VisibleField", "ExpirationDocumentDate", True)
      Me.m_account_required_field.ExpirationDocumentDate.Required = _gp.GetBoolean("Account.RequestedField", "ExpirationDocumentDate", False)
      Me.m_account_required_field.ExpirationDocumentDate.AML = _gp.GetBoolean("Account.RequestedField", "AntiMoneyLaundering.ExpirationDocumentDate", False)

      Me.m_account_required_field.Phone1.Visible = _gp.GetBoolean("Account.VisibleField", "Phone1")
      Me.m_account_required_field.Phone1.Required = _gp.GetBoolean("Account.RequestedField", "Phone1")
      Me.m_account_required_field.Phone1.AML = _gp.GetBoolean("Account.RequestedField", "AntiMoneyLaundering.Phone1")

      Me.m_account_required_field.Phone2.Visible = _gp.GetBoolean("Account.VisibleField", "Phone2")
      Me.m_account_required_field.Phone2.Required = _gp.GetBoolean("Account.RequestedField", "Phone2")
      Me.m_account_required_field.Phone2.AML = _gp.GetBoolean("Account.RequestedField", "AntiMoneyLaundering.Phone2")

      Me.m_account_required_field.Email1.Visible = _gp.GetBoolean("Account.VisibleField", "Email1")
      Me.m_account_required_field.Email1.Required = _gp.GetBoolean("Account.RequestedField", "Email1")
      Me.m_account_required_field.Email1.AML = _gp.GetBoolean("Account.RequestedField", "AntiMoneyLaundering.Email1")

      Me.m_account_required_field.Email2.Visible = _gp.GetBoolean("Account.VisibleField", "Email2")
      Me.m_account_required_field.Email2.Required = _gp.GetBoolean("Account.RequestedField", "Email2")
      Me.m_account_required_field.Email2.AML = _gp.GetBoolean("Account.RequestedField", "AntiMoneyLaundering.Email2")

      Me.m_account_required_field.TwitterAccount.Visible = _gp.GetBoolean("Account.VisibleField", "TwitterAccount")
      Me.m_account_required_field.TwitterAccount.Required = _gp.GetBoolean("Account.RequestedField", "TwitterAccount")
      Me.m_account_required_field.TwitterAccount.AML = _gp.GetBoolean("Account.RequestedField", "AntiMoneyLaundering.TwitterAccount")

      Me.m_account_required_field.Address01.Visible = _gp.GetBoolean("Account.VisibleField", "Address")
      Me.m_account_required_field.Address01.Required = _gp.GetBoolean("Account.RequestedField", "Address")
      Me.m_account_required_field.Address01.AML = _gp.GetBoolean("Account.RequestedField", "AntiMoneyLaundering.Address")

      Me.m_account_required_field.Address02.Visible = _gp.GetBoolean("Account.VisibleField", "Address02")
      Me.m_account_required_field.Address02.Required = _gp.GetBoolean("Account.RequestedField", "Address02")
      Me.m_account_required_field.Address02.AML = _gp.GetBoolean("Account.RequestedField", "AntiMoneyLaundering.Address02")

      Me.m_account_required_field.Address03.Visible = _gp.GetBoolean("Account.VisibleField", "Address03")
      Me.m_account_required_field.Address03.Required = _gp.GetBoolean("Account.RequestedField", "Address03")
      Me.m_account_required_field.Address03.AML = _gp.GetBoolean("Account.RequestedField", "AntiMoneyLaundering.Address03")

      Me.m_account_required_field.ExtNum.Visible = _gp.GetBoolean("Account.VisibleField", "ExtNum")
      Me.m_account_required_field.ExtNum.Required = _gp.GetBoolean("Account.RequestedField", "ExtNum")
      Me.m_account_required_field.ExtNum.AML = _gp.GetBoolean("Account.RequestedField", "AntiMoneyLaundering.ExtNum")

      Me.m_account_required_field.City.Visible = _gp.GetBoolean("Account.VisibleField", "City")
      Me.m_account_required_field.City.Required = _gp.GetBoolean("Account.RequestedField", "City")
      Me.m_account_required_field.City.AML = _gp.GetBoolean("Account.RequestedField", "AntiMoneyLaundering.City")

      Me.m_account_required_field.Zip.Visible = _gp.GetBoolean("Account.VisibleField", "Zip")
      Me.m_account_required_field.Zip.Required = _gp.GetBoolean("Account.RequestedField", "Zip")
      Me.m_account_required_field.Zip.AML = _gp.GetBoolean("Account.RequestedField", "AntiMoneyLaundering.Zip")

      Me.m_account_required_field.AddressCountry.Visible = _gp.GetBoolean("Account.VisibleField", "AddressCountry")
      Me.m_account_required_field.AddressCountry.Required = _gp.GetBoolean("Account.RequestedField", "AddressCountry")
      Me.m_account_required_field.AddressCountry.AML = _gp.GetBoolean("Account.RequestedField", "AntiMoneyLaundering.AddressCountry")

      Me.m_account_required_field.FedEntity.Visible = _gp.GetBoolean("Account.VisibleField", "FedEntity")
      Me.m_account_required_field.FedEntity.Required = _gp.GetBoolean("Account.RequestedField", "FedEntity")
      Me.m_account_required_field.FedEntity.AML = _gp.GetBoolean("Account.RequestedField", "AntiMoneyLaundering.FedEntity")

      Me.m_account_required_field.DocumentTypeList = _gp.GetString("Account.RequestedField", "DocumentTypeList")
      Me.m_account_required_field.DocScanTypeList = _gp.GetString("Account.RequestedField", "DocScanTypeList")

      Me.m_account_required_field.AMLDocumentTypeList = _gp.GetString("Account.RequestedField", "AntiMoneyLaundering.DocumentTypeList")
      Me.m_account_required_field.AMLDocScanTypeList = _gp.GetString("Account.RequestedField", "AntiMoneyLaundering.DocScanTypeList")

      Me.m_account_required_field.AccountCustomizeEnabled = _gp.GetString("Cashier.Voucher", "AccountCustomize.Enabled")


      ' BENEFICIARY
      Me.m_beneficiary_required_field.Name1.Required = _gp.GetBoolean("Beneficiary.RequestedField", "Name1")
      Me.m_beneficiary_required_field.Name1.AML = _gp.GetBoolean("Beneficiary.RequestedField", "AntiMoneyLaundering.Name1")

      Me.m_beneficiary_required_field.Name2.Required = _gp.GetBoolean("Beneficiary.RequestedField", "Name2")
      Me.m_beneficiary_required_field.Name2.AML = _gp.GetBoolean("Beneficiary.RequestedField", "AntiMoneyLaundering.Name2")

      Me.m_beneficiary_required_field.Name3.Required = _gp.GetBoolean("Beneficiary.RequestedField", "Name3")
      Me.m_beneficiary_required_field.Name3.AML = _gp.GetBoolean("Beneficiary.RequestedField", "AntiMoneyLaundering.Name3")

      Me.m_beneficiary_required_field.Document.Required = _gp.GetBoolean("Beneficiary.RequestedField", "Document")
      Me.m_beneficiary_required_field.Document.AML = _gp.GetBoolean("Beneficiary.RequestedField", "AntiMoneyLaundering.Document")

      Me.m_beneficiary_required_field.DocScan.Required = _gp.GetBoolean("Beneficiary.RequestedField", "DocScan")
      Me.m_beneficiary_required_field.DocScan.AML = _gp.GetBoolean("Beneficiary.RequestedField", "AntiMoneyLaundering.DocScan")

      Me.m_beneficiary_required_field.Birthdate.Required = _gp.GetBoolean("Beneficiary.RequestedField", "BirthDate")
      Me.m_beneficiary_required_field.Birthdate.AML = _gp.GetBoolean("Beneficiary.RequestedField", "AntiMoneyLaundering.BirthDate")

      Me.m_beneficiary_required_field.Occupation.Required = _gp.GetBoolean("Beneficiary.RequestedField", "Occupation")
      Me.m_beneficiary_required_field.Occupation.AML = _gp.GetBoolean("Beneficiary.RequestedField", "AntiMoneyLaundering.Occupation")

      Me.m_beneficiary_required_field.Gender.Required = _gp.GetBoolean("Beneficiary.RequestedField", "Gender")
      Me.m_beneficiary_required_field.Gender.AML = _gp.GetBoolean("Beneficiary.RequestedField", "AntiMoneyLaundering.Gender")

      Me.m_beneficiary_required_field.DocumentTypeList = _gp.GetString("Beneficiary.RequestedField", "DocumentTypeList")
      Me.m_beneficiary_required_field.DocScanTypeList = _gp.GetString("Beneficiary.RequestedField", "DocScanTypeList")

      Me.m_beneficiary_required_field.AMLDocumentTypeList = _gp.GetString("Beneficiary.RequestedField", "AntiMoneyLaundering.DocumentTypeList")
      Me.m_beneficiary_required_field.AMLDocScanTypeList = _gp.GetString("Beneficiary.RequestedField", "AntiMoneyLaundering.DocScanTypeList")

      Call Trx_LoadIdentificationTypes(Trx)

      Call Trx_LoadCountries(Trx)


      Me.m_default_document = _gp.GetString("Account.DefaultValues", "DocumentType")
      Me.m_default_country = _gp.GetString("Account.DefaultValues", "Country")

      For Each _dr As DataRow In Me.m_dt_identification_types.Rows
        If Convert.ToBoolean(_dr(1)) Then
          _ident_value = _dr(0)
          _identification_types.Add(_ident_value.ToString("000"))
        End If
      Next

      If _identification_types.Count > 0 Then
        Me.m_visible_documents = String.Join(",", _identification_types.ToArray())
      Else
        Me.m_visible_documents = String.Empty
      End If

      Return ENUM_STATUS.STATUS_OK

    Catch ex As Exception
      Log.Exception(ex)
    End Try

    Return ENUM_STATUS.STATUS_ERROR

  End Function

  Public Function Update(ByVal Trx As SqlTransaction) As ENUM_STATUS

    Dim _dic_key As Dictionary(Of String, String)

    Try
      _dic_key = New Dictionary(Of String, String)

      _dic_key.Add("Name1", ToStr(Me.m_account_required_field.Name1.Visible))
      _dic_key.Add("Name2", ToStr(Me.m_account_required_field.Name2.Visible))
      _dic_key.Add("Name3", ToStr(Me.m_account_required_field.Name3.Visible))
      _dic_key.Add("Name4", ToStr(Me.m_account_required_field.Name4.Visible))
      _dic_key.Add("Document", ToStr(Me.m_account_required_field.Document.Visible))
      _dic_key.Add("DocScan", ToStr(Me.m_account_required_field.DocScan.Visible))

      _dic_key.Add("BirthDate", ToStr(Me.m_account_required_field.Birthdate.Visible))
      _dic_key.Add("Occupation", ToStr(Me.m_account_required_field.Occupation.Visible))
      _dic_key.Add("Gender", ToStr(Me.m_account_required_field.Gender.Visible))
      _dic_key.Add("Nationality", ToStr(Me.m_account_required_field.Nationality.Visible))
      _dic_key.Add("BirthCountry", ToStr(Me.m_account_required_field.BirthCountry.Visible))
      _dic_key.Add("MaritalStatus", ToStr(Me.m_account_required_field.MaritalStatus.Visible))
      _dic_key.Add("WeddingDate", ToStr(Me.m_account_required_field.WeddingDate.Visible))
      _dic_key.Add("Photo", ToStr(Me.m_account_required_field.Photo.Visible))
      _dic_key.Add("ExpirationDocumentDate", ToStr(Me.m_account_required_field.ExpirationDocumentDate.Visible))
      _dic_key.Add("Phone1", ToStr(Me.m_account_required_field.Phone1.Visible))
      _dic_key.Add("Phone2", ToStr(Me.m_account_required_field.Phone2.Visible))
      _dic_key.Add("Email1", ToStr(Me.m_account_required_field.Email1.Visible))
      _dic_key.Add("Email2", ToStr(Me.m_account_required_field.Email2.Visible))
      _dic_key.Add("TwitterAccount", ToStr(Me.m_account_required_field.TwitterAccount.Visible))
      _dic_key.Add("Address", ToStr(Me.m_account_required_field.Address01.Visible))
      _dic_key.Add("Address02", ToStr(Me.m_account_required_field.Address02.Visible))
      _dic_key.Add("Address03", ToStr(Me.m_account_required_field.Address03.Visible))
      _dic_key.Add("ExtNum", ToStr(Me.m_account_required_field.ExtNum.Visible))
      _dic_key.Add("Zip", ToStr(Me.m_account_required_field.Zip.Visible))
      _dic_key.Add("City", ToStr(Me.m_account_required_field.City.Visible))
      _dic_key.Add("AddressCountry", ToStr(Me.m_account_required_field.AddressCountry.Visible))
      _dic_key.Add("FedEntity", ToStr(Me.m_account_required_field.FedEntity.Visible))

      For Each _dic As KeyValuePair(Of String, String) In _dic_key
        ' 9/2/2016 SJA DB_GeneralParam_AddOrUpdate instead of DB_GeneralParam_Update incase genparam does not exist yet in db
        If Not DB_GeneralParam_Update("Account.VisibleField", _dic.Key, _dic.Value, Trx) Then
          Return ENUM_STATUS.STATUS_ERROR
        End If
      Next

      _dic_key = New Dictionary(Of String, String)

      _dic_key.Add("AntiMoneyLaundering.Name1", ToStr(Me.m_account_required_field.Name1.AML))
      _dic_key.Add("AntiMoneyLaundering.Name2", ToStr(Me.m_account_required_field.Name2.AML))
      _dic_key.Add("AntiMoneyLaundering.Name3", ToStr(Me.m_account_required_field.Name3.AML))
      _dic_key.Add("AntiMoneyLaundering.Document", ToStr(Me.m_account_required_field.Document.AML))
      _dic_key.Add("AntiMoneyLaundering.DocScan", ToStr(Me.m_account_required_field.DocScan.AML))
      _dic_key.Add("AntiMoneyLaundering.BirthDate", ToStr(Me.m_account_required_field.Birthdate.AML))
      _dic_key.Add("AntiMoneyLaundering.AddressCountry", ToStr(Me.m_account_required_field.AddressCountry.AML))
      _dic_key.Add("AntiMoneyLaundering.FedEntity", ToStr(Me.m_account_required_field.FedEntity.AML))
      _dic_key.Add("AntiMoneyLaundering.Zip", ToStr(Me.m_account_required_field.Zip.AML))
      _dic_key.Add("AntiMoneyLaundering.City", ToStr(Me.m_account_required_field.City.AML))
      _dic_key.Add("AntiMoneyLaundering.ExtNum", ToStr(Me.m_account_required_field.ExtNum.AML))
      _dic_key.Add("AntiMoneyLaundering.Address03", ToStr(Me.m_account_required_field.Address03.AML))
      _dic_key.Add("AntiMoneyLaundering.Address02", ToStr(Me.m_account_required_field.Address02.AML))
      _dic_key.Add("AntiMoneyLaundering.Address", ToStr(Me.m_account_required_field.Address01.AML))
      _dic_key.Add("AntiMoneyLaundering.TwitterAccount", ToStr(Me.m_account_required_field.TwitterAccount.AML))
      _dic_key.Add("AntiMoneyLaundering.Email2", ToStr(Me.m_account_required_field.Email2.AML))
      _dic_key.Add("AntiMoneyLaundering.Email1", ToStr(Me.m_account_required_field.Email1.AML))
      _dic_key.Add("AntiMoneyLaundering.Phone2", ToStr(Me.m_account_required_field.Phone2.AML))
      _dic_key.Add("AntiMoneyLaundering.Phone1", ToStr(Me.m_account_required_field.Phone1.AML))
      _dic_key.Add("AntiMoneyLaundering.WeddingDate", ToStr(Me.m_account_required_field.WeddingDate.AML))
      _dic_key.Add("AntiMoneyLaundering.MaritalStatus", ToStr(Me.m_account_required_field.MaritalStatus.AML))
      _dic_key.Add("AntiMoneyLaundering.BirthCountry", ToStr(Me.m_account_required_field.BirthCountry.AML))
      _dic_key.Add("AntiMoneyLaundering.Nationality", ToStr(Me.m_account_required_field.Nationality.AML))
      _dic_key.Add("AntiMoneyLaundering.Gender", ToStr(Me.m_account_required_field.Gender.AML))
      _dic_key.Add("AntiMoneyLaundering.Occupation", ToStr(Me.m_account_required_field.Occupation.AML))
      _dic_key.Add("Name1", ToStr(Me.m_account_required_field.Name1.Required))
      _dic_key.Add("Name2", ToStr(Me.m_account_required_field.Name2.Required))
      _dic_key.Add("Name3", ToStr(Me.m_account_required_field.Name3.Required))
      _dic_key.Add("Name4", ToStr(Me.m_account_required_field.Name4.Required))
      _dic_key.Add("Document", ToStr(Me.m_account_required_field.Document.Required))
      _dic_key.Add("DocScan", ToStr(Me.m_account_required_field.DocScan.Required))
      _dic_key.Add("Birthdate", ToStr(Me.m_account_required_field.Birthdate.Required))
      _dic_key.Add("Occupation", ToStr(Me.m_account_required_field.Occupation.Required))
      _dic_key.Add("Gender", ToStr(Me.m_account_required_field.Gender.Required))
      _dic_key.Add("Nationality", ToStr(Me.m_account_required_field.Nationality.Required))
      _dic_key.Add("BirthCountry", ToStr(Me.m_account_required_field.BirthCountry.Required))
      _dic_key.Add("MaritalStatus", ToStr(Me.m_account_required_field.MaritalStatus.Required))
      _dic_key.Add("WeddingDate", ToStr(Me.m_account_required_field.WeddingDate.Required))
      _dic_key.Add("Photo", ToStr(Me.m_account_required_field.Photo.Required))
      _dic_key.Add("ExpirationDocumentDate", ToStr(Me.m_account_required_field.ExpirationDocumentDate.Required))
      _dic_key.Add("Phone1", ToStr(Me.m_account_required_field.Phone1.Required))
      _dic_key.Add("Phone2", ToStr(Me.m_account_required_field.Phone2.Required))
      _dic_key.Add("Email1", ToStr(Me.m_account_required_field.Email1.Required))
      _dic_key.Add("Email2", ToStr(Me.m_account_required_field.Email2.Required))
      _dic_key.Add("TwitterAccount", ToStr(Me.m_account_required_field.TwitterAccount.Required))
      _dic_key.Add("Address", ToStr(Me.m_account_required_field.Address01.Required))
      _dic_key.Add("Address02", ToStr(Me.m_account_required_field.Address02.Required))
      _dic_key.Add("Address03", ToStr(Me.m_account_required_field.Address03.Required))
      _dic_key.Add("ExtNum", ToStr(Me.m_account_required_field.ExtNum.Required))
      _dic_key.Add("Zip", ToStr(Me.m_account_required_field.Zip.Required))
      _dic_key.Add("City", ToStr(Me.m_account_required_field.City.Required))
      _dic_key.Add("AddressCountry", ToStr(Me.m_account_required_field.AddressCountry.Required))
      _dic_key.Add("FedEntity", ToStr(Me.m_account_required_field.FedEntity.Required))
      _dic_key.Add("DocumentTypeList", Me.m_account_required_field.DocumentTypeList)
      _dic_key.Add("DocScanTypeList", Me.m_account_required_field.DocScanTypeList)
      _dic_key.Add("AntiMoneyLaundering.DocumentTypeList", Me.m_account_required_field.AMLDocumentTypeList)
      _dic_key.Add("AntiMoneyLaundering.DocScanTypeList", Me.m_account_required_field.AMLDocScanTypeList)

      For Each _dic As KeyValuePair(Of String, String) In _dic_key
        ' 9/2/2016 SJA DB_GeneralParam_AddOrUpdate instead of DB_GeneralParam_Update incase genparam does not exist yet in db
        If Not DB_GeneralParam_Update("Account.RequestedField", _dic.Key, _dic.Value, Trx) Then
          Return ENUM_STATUS.STATUS_ERROR
        End If
      Next

      ' BENEFICIARY

      _dic_key = New Dictionary(Of String, String)

      _dic_key.Add("Name1", ToStr(Me.m_beneficiary_required_field.Name1.Required))
      _dic_key.Add("AntiMoneyLaundering.Name1", ToStr(Me.m_beneficiary_required_field.Name1.AML))
      _dic_key.Add("Gender", ToStr(Me.m_beneficiary_required_field.Gender.Required))
      _dic_key.Add("AntiMoneyLaundering.Gender", ToStr(Me.m_beneficiary_required_field.Gender.AML))
      _dic_key.Add("Name2", ToStr(Me.m_beneficiary_required_field.Name2.Required))
      _dic_key.Add("AntiMoneyLaundering.Name2", ToStr(Me.m_beneficiary_required_field.Name2.AML))
      _dic_key.Add("Name3", ToStr(Me.m_beneficiary_required_field.Name3.Required))
      _dic_key.Add("AntiMoneyLaundering.Name3", ToStr(Me.m_beneficiary_required_field.Name3.AML))
      _dic_key.Add("Document", ToStr(Me.m_beneficiary_required_field.Document.Required))
      _dic_key.Add("AntiMoneyLaundering.Document", ToStr(Me.m_beneficiary_required_field.Document.AML))
      _dic_key.Add("DocScan", ToStr(Me.m_beneficiary_required_field.DocScan.Required))
      _dic_key.Add("AntiMoneyLaundering.DocScan", ToStr(Me.m_beneficiary_required_field.DocScan.AML))
      _dic_key.Add("Birthdate", ToStr(Me.m_beneficiary_required_field.Birthdate.Required))
      _dic_key.Add("AntiMoneyLaundering.Birthdate", ToStr(Me.m_beneficiary_required_field.Birthdate.AML))
      _dic_key.Add("Occupation", ToStr(Me.m_beneficiary_required_field.Occupation.Required))
      _dic_key.Add("AntiMoneyLaundering.Occupation", ToStr(Me.m_beneficiary_required_field.Occupation.AML))
      _dic_key.Add("DocumentTypeList", Me.m_beneficiary_required_field.DocumentTypeList)
      _dic_key.Add("DocScanTypeList", Me.m_beneficiary_required_field.DocScanTypeList)
      _dic_key.Add("AntiMoneyLaundering.DocumentTypeList", Me.m_beneficiary_required_field.AMLDocumentTypeList)
      _dic_key.Add("AntiMoneyLaundering.DocScanTypeList", Me.m_beneficiary_required_field.AMLDocScanTypeList)

      For Each _dic As KeyValuePair(Of String, String) In _dic_key
        ' 9/2/2016 SJA DB_GeneralParam_AddOrUpdate instead of DB_GeneralParam_Update incase genparam does not exist yet in db
        If Not DB_GeneralParam_Update("Beneficiary.RequestedField", _dic.Key, _dic.Value, Trx) Then
          Return ENUM_STATUS.STATUS_ERROR
        End If
      Next

      ' 9/2/2016 SJA DB_GeneralParam_AddOrUpdate instead of DB_GeneralParam_Update incase genparam does not exist yet in db
      If Not DB_GeneralParam_Update("Cashier.Voucher", "AccountCustomize.Enabled", Me.m_account_required_field.AccountCustomizeEnabled, Trx) Then
        Return ENUM_STATUS.STATUS_ERROR
      End If

      Call Trx_UpdateIdentificationTypes(Trx)

      Call Trx_UpdateDefaultParams(Trx)

      Return ENUM_STATUS.STATUS_OK

    Catch ex As Exception
      Log.Exception(ex)
    End Try

    Return ENUM_STATUS.STATUS_ERROR

  End Function

  Public Shared Sub FillAuditorData(ByVal AuditorData As CLASS_AUDITOR_DATA, ByVal RequiredFields As CLS_REQUIRED_FIELDS)
    Dim _account As CLS_ACCOUNT_REQUIRED_FIELD
    Dim _beneficiary As CLS_BENEFICIARY_REQUIRED_FIELD
    Dim _document_type_list As String
    Dim AML_Name As String
    Dim _dummy As Integer

    Dim _yes_text As String
    Dim _no_text As String

    AML_Name = Accounts.getAMLName()

    _account = RequiredFields.m_account_required_field
    _beneficiary = RequiredFields.m_beneficiary_required_field

    _yes_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(278)
    _no_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)

    If _account.AccountCustomizeEnabled = "0" Then
      ' Ticket Print
      Call AuditorData.SetField(0 _
                                , _no_text _
                                , GLB_NLS_GUI_PLAYER_TRACKING.GetString(491) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1728))

      ' AskToPrint
      Call AuditorData.SetField(0 _
                                , _no_text _
                                , GLB_NLS_GUI_PLAYER_TRACKING.GetString(491) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(839))
    ElseIf _account.AccountCustomizeEnabled = "1" Then
      ' Ticket Print
      Call AuditorData.SetField(0 _
                                , _yes_text _
                                , GLB_NLS_GUI_PLAYER_TRACKING.GetString(491) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1728))
      ' AskToPrint
      Call AuditorData.SetField(0 _
                                , _no_text _
                                , GLB_NLS_GUI_PLAYER_TRACKING.GetString(491) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(839))
    Else
      ' Ticket Print
      Call AuditorData.SetField(0 _
                                , _yes_text _
                                , GLB_NLS_GUI_PLAYER_TRACKING.GetString(491) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1728))
      ' AskToPrint
      Call AuditorData.SetField(0 _
                                , _yes_text _
                                , GLB_NLS_GUI_PLAYER_TRACKING.GetString(491) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(839))
    End If


    Call SetAuditorValue(AuditorData, _account.Name3, AccountFields.GetName(), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1713))
    Call SetAuditorValue(AuditorData, _account.Name4, AccountFields.GetMiddleName(), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1713)) ' "Name4"
    Call SetAuditorValue(AuditorData, _account.Name1, AccountFields.GetSurname1(), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1713)) '"Name1"
    Call SetAuditorValue(AuditorData, _account.Name2, AccountFields.GetSurname2(), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1713)) 'Name2
    Call SetAuditorValue(AuditorData, _account.Document, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1566), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1713)) 'Document
    Call SetAuditorValue(AuditorData, _account.DocScan, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5092), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1713)) 'Documentos Escaneados
    Call SetAuditorValue(AuditorData, _account.Birthdate, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1725), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1713)) 'Fecha nacimiento
    Call SetAuditorValue(AuditorData, _account.Occupation, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2058), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1713)) 'Ocupaci�n
    Call SetAuditorValue(AuditorData, _account.Gender, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1723), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1713)) 'Sexo
    Call SetAuditorValue(AuditorData, _account.Nationality, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2089), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1713)) 'Nacionalidad
    Call SetAuditorValue(AuditorData, _account.BirthCountry, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2088), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1713)) 'Pa�s nacimiento
    Call SetAuditorValue(AuditorData, _account.MaritalStatus, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1724), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1713)) 'Estado civil
    Call SetAuditorValue(AuditorData, _account.WeddingDate, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1726), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1713)) 'Fecha boda
    Call SetAuditorValue(AuditorData, _account.Photo, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7103), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1713)) 'Photo
    Call SetAuditorValue(AuditorData, _account.ExpirationDocumentDate, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7950), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1713)) 'ExpirationDocumentDate
    Call SetAuditorValue(AuditorData, _account.Address01, AccountFields.GetAddressName(), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1713)) 'Calle
    Call SetAuditorValue(AuditorData, _account.ExtNum, AccountFields.GetNumExtName(), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1713)) 'NumExt
    Call SetAuditorValue(AuditorData, _account.Address02, AccountFields.GetColonyName(), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1713)) 'Colonia
    Call SetAuditorValue(AuditorData, _account.Address03, AccountFields.GetDelegationName(), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1713)) 'Delegacion
    Call SetAuditorValue(AuditorData, _account.City, AccountFields.GetTownshipName(), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1713)) 'Municipio
    Call SetAuditorValue(AuditorData, _account.Zip, AccountFields.GetZipName(), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1713)) 'CP
    Call SetAuditorValue(AuditorData, _account.AddressCountry, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5090), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1713)) 'Pais
    Call SetAuditorValue(AuditorData, _account.FedEntity, AccountFields.GetStateName(), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1713)) 'Entidad federativa
    Call SetAuditorValue(AuditorData, _account.Phone1, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1727, "1"), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1713)) 'Email1
    Call SetAuditorValue(AuditorData, _account.Phone2, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1727, "2"), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1713)) 'Email2
    Call SetAuditorValue(AuditorData, _account.Email1, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1729, "1"), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1713)) 'Phone1
    Call SetAuditorValue(AuditorData, _account.Email2, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1729, "2"), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1713)) 'Phone2
    Call SetAuditorValue(AuditorData, _account.TwitterAccount, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1730), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1713)) 'Twitter



    Call SetAuditorValue(AuditorData, _beneficiary.Name3, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4399), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2060))
    Call SetAuditorValue(AuditorData, _beneficiary.Name1, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5087), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2060))
    Call SetAuditorValue(AuditorData, _beneficiary.Name2, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1710), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2060))
    Call SetAuditorValue(AuditorData, _beneficiary.Document, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1566), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2060))
    Call SetAuditorValue(AuditorData, _beneficiary.DocScan, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5092), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2060))
    Call SetAuditorValue(AuditorData, _beneficiary.Birthdate, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1725), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2060))
    Call SetAuditorValue(AuditorData, _beneficiary.Occupation, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2058), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2060))
    Call SetAuditorValue(AuditorData, _beneficiary.Gender, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1723), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2060))

    ' Types Visible
    If String.IsNullOrEmpty(RequiredFields.VisibleDocuments) Then
      _document_type_list = AUDIT_NONE_STRING
    Else
      _document_type_list = WSI.Common.IdentificationTypes.DocIdTypeStringList(RequiredFields.VisibleDocuments)
    End If

    Call AuditorData.SetField(0, _document_type_list, GLB_NLS_GUI_PLAYER_TRACKING.GetString(496) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5089) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(754)) ' Visible

    ' Required
    If String.IsNullOrEmpty(_account.DocumentTypeList) Then
      _document_type_list = AUDIT_NONE_STRING
    Else
      _document_type_list = WSI.Common.IdentificationTypes.DocIdTypeStringList(_account.DocumentTypeList)
    End If

    Call AuditorData.SetField(0, _document_type_list, GLB_NLS_GUI_PLAYER_TRACKING.GetString(496) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5089) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(739)) ' Account.Document.Required


    If String.IsNullOrEmpty(_account.DocScanTypeList) Then
      _document_type_list = AUDIT_NONE_STRING
    Else
      _document_type_list = WSI.Common.IdentificationTypes.DocIdTypeStringList(_account.DocScanTypeList)
    End If

    '' "Player mandatory fields"."Scanned Doc."
    Call AuditorData.SetField(0, _document_type_list, GLB_NLS_GUI_PLAYER_TRACKING.GetString(496) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5089) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5091)) 'Account.Document.Scan

    If String.IsNullOrEmpty(_account.AMLDocumentTypeList) Then
      _document_type_list = AUDIT_NONE_STRING
    Else
      _document_type_list = WSI.Common.IdentificationTypes.DocIdTypeStringList(_account.AMLDocumentTypeList)
    End If

    '' "Player mandatory fields"."Scanned Doc."
    Call AuditorData.SetField(0, _document_type_list, GLB_NLS_GUI_PLAYER_TRACKING.GetString(496) & "." & AML_Name & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(739)) 'Account.AML.Required

    If String.IsNullOrEmpty(_account.AMLDocScanTypeList) Then
      _document_type_list = AUDIT_NONE_STRING
    Else
      _document_type_list = WSI.Common.IdentificationTypes.DocIdTypeStringList(_account.AMLDocScanTypeList)
    End If

    '' "Player mandatory fields"."Scanned Doc."
    Call AuditorData.SetField(0, _document_type_list, GLB_NLS_GUI_PLAYER_TRACKING.GetString(496) & "." & AML_Name & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5091)) 'Account.AML.SCAN

    If String.IsNullOrEmpty(_beneficiary.DocumentTypeList) Then
      _document_type_list = AUDIT_NONE_STRING
    Else
      _document_type_list = WSI.Common.IdentificationTypes.DocIdTypeStringList(_beneficiary.DocumentTypeList)
    End If

    '' "Player mandatory fields"."Scanned Doc."
    Call AuditorData.SetField(0, _document_type_list, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2060) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5089) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(739)) 'BENEFICIARY.Document.Required

    If String.IsNullOrEmpty(_beneficiary.DocScanTypeList) Then
      _document_type_list = AUDIT_NONE_STRING
    Else
      _document_type_list = WSI.Common.IdentificationTypes.DocIdTypeStringList(_beneficiary.DocScanTypeList)
    End If

    ' Beneficiarty Requeried Fields"."Scanned Doc."
    Call AuditorData.SetField(0, _document_type_list, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2060) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5089) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5091)) 'BENEFICIARY.Document.scan

    If String.IsNullOrEmpty(_beneficiary.AMLDocumentTypeList) Then
      _document_type_list = AUDIT_NONE_STRING
    Else
      _document_type_list = WSI.Common.IdentificationTypes.DocIdTypeStringList(_beneficiary.AMLDocumentTypeList)
    End If

    '' "Player mandatory fields"."Scanned Doc."
    Call AuditorData.SetField(0, _document_type_list, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2060) & "." & AML_Name & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(739)) 'BENEFICIARY.aml.required

    If String.IsNullOrEmpty(_beneficiary.AMLDocScanTypeList) Then
      _document_type_list = AUDIT_NONE_STRING
    Else
      _document_type_list = WSI.Common.IdentificationTypes.DocIdTypeStringList(_beneficiary.AMLDocScanTypeList)
    End If

    '' "Player mandatory fields"."Scanned Doc."
    Call AuditorData.SetField(0, _document_type_list, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2060) & "." & AML_Name & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5091)) ''BENEFICIARY.aml.SCAN



    If Not String.IsNullOrEmpty(RequiredFields.m_default_country) Then
      If RequiredFields.m_default_country.Length <= 2 Then

        For Each _key As KeyValuePair(Of String, String) In RequiredFields.m_dic_countries
          If _key.Value = RequiredFields.m_default_country Then
            RequiredFields.m_default_country = _key.Key
          End If
        Next

        Call AuditorData.SetField(0, RequiredFields.m_default_country, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5088) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5090))
      Else
        Call AuditorData.SetField(0, RequiredFields.m_default_country, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5088) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5090))
      End If
    Else
      Call AuditorData.SetField(0, AUDIT_NONE_STRING, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5088) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5090))
    End If

    If String.IsNullOrEmpty(RequiredFields.m_default_document) Then
      _document_type_list = AUDIT_NONE_STRING
    Else
      If RequiredFields.m_default_document.Length <= 3 And Integer.TryParse(RequiredFields.m_default_document, _dummy) Then
        For Each _dr As DataRow In RequiredFields.m_dt_identification_types.Rows
          Dim _document_id As Integer
          _document_id = _dr(0)

          If RequiredFields.m_default_document = _document_id.ToString("000") Then
            _document_type_list = _dr(2)
          End If
        Next
      Else
        _document_type_list = RequiredFields.m_default_document
      End If
    End If

    '' "Player mandatory fields"."Scanned Doc."
    Call AuditorData.SetField(0, _document_type_list, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5088) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5089)) ''BENEFICIARY.aml.SCAN
  End Sub

  Public Shared Sub SetAuditorValue(ByVal AuditorData As CLASS_AUDITOR_DATA, ByVal ConfigParam As Config_Param, ByVal Name As String, ByVal Player As String)
    Dim _yes_text As String
    Dim _no_text As String

    _yes_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(278)
    _no_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)

    If Not Player = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2060) Then 'Beneficiary
      If ConfigParam.Visible Then
        Call AuditorData.SetField(0, _yes_text, Player & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(754) & "." & Name)
      Else
        Call AuditorData.SetField(0, _no_text, Player & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(754) & "." & Name)
      End If
    End If

    If ConfigParam.Required Then
      Call AuditorData.SetField(0, _yes_text, Player & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(739) & "." & Name)
    Else
      Call AuditorData.SetField(0, _no_text, Player & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(739) & "." & Name)
    End If

    If Not Name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5087) Then ' Name4
      If ConfigParam.AML Then
        Call AuditorData.SetField(0, _yes_text, Player & "." & Accounts.getAMLName() & "." & Name)
      Else
        Call AuditorData.SetField(0, _no_text, Player & "." & Accounts.getAMLName() & "." & Name)
      End If
    End If

  End Sub

  ' PURPOSE:  Returns a Dictionary of All Documents from DB
  '
  '  PARAMS:
  '     - OUTPUT:
  '           - Dic
  '
  Public Sub GetDictionaryOfDocuments(ByRef Dic As Dictionary(Of String, String))
    Dim _str_value As Integer

    For Each _dr As DataRow In Me.m_dt_identification_types.Rows
      _str_value = _dr(0)
      Dic.Add(_str_value.ToString("000"), _dr(2))
    Next

  End Sub

#End Region

#Region " Private Function "

  Private Function ToStr(ByVal Value As Boolean) As String

    If Value Then
      Return "1"
    End If

    Return "0"

  End Function

  ' PURPOSE:  Load Identification types from DB filtering by ISO CODE
  '
  '  PARAMS:
  '     - INPUT:
  '           - Trx : Transaction
  '
  Private Sub Trx_LoadIdentificationTypes(ByVal Trx As SqlTransaction)
    Dim _str_query As StringBuilder

    Try
      _str_query = New StringBuilder()

      _str_query.AppendLine("   SELECT   IDT_ID,  IDT_ENABLED,  IDT_NAME  ")
      _str_query.AppendLine("     FROM   IDENTIFICATION_TYPES             ")
      _str_query.AppendLine("    WHERE   IDT_COUNTRY_ISO_CODE2=@pISOCODE ")
      _str_query.AppendLine(" ORDER BY   IDT_ORDER                        ")

      Using _sql_cmd As New SqlCommand(_str_query.ToString(), Trx.Connection, Trx)
        _sql_cmd.Parameters.Add("@pISOCODE", SqlDbType.NVarChar).Value = WSI.Common.Resource.CountryISOCode2()
        Using _sql_da As New SqlDataAdapter(_sql_cmd)
          _sql_da.Fill(Me.m_dt_identification_types)
        End Using
      End Using

    Catch ex As Exception
      Log.Exception(ex)
    End Try

  End Sub

  ' PURPOSE:  Load Countries from DB filtering by LANGUAGE
  '
  '  PARAMS:
  '     - INPUT:
  '           - Trx : Transaction
  '
  Private Sub Trx_LoadCountries(ByVal Trx As SqlTransaction)
    Dim _str_query As StringBuilder

    Try
      _str_query = New StringBuilder()

      _str_query.AppendLine("   SELECT   CO_NAME,  CO_ISO2        ")
      _str_query.AppendLine("     FROM   COUNTRIES                ")
      _str_query.AppendLine("    WHERE CO_LANGUAGE_ID=@pLanguage  ")
      _str_query.AppendLine(" ORDER BY   CO_NAME ASC              ")

      Using _sql_cmd As New SqlCommand(_str_query.ToString(), Trx.Connection, Trx)
        _sql_cmd.Parameters.Add("@pLanguage", SqlDbType.BigInt).Value = WSI.Common.Resource.LanguageId
        Using _reader As SqlClient.SqlDataReader = _sql_cmd.ExecuteReader()
          While (_reader.Read())
            m_dic_countries.Add(_reader(0), _reader(1))
          End While
          _reader.Close()
        End Using
      End Using

    Catch ex As Exception
      Log.Exception(ex)
    End Try

  End Sub

  ' PURPOSE:  Sets enabled value of identification types 
  '
  '  PARAMS:
  '     - INPUT:
  '           - Trx : Transaction
  '
  Private Function Trx_UpdateIdentificationTypes(ByVal Trx As SqlTransaction) As Boolean
    Dim _str_query As StringBuilder
    Try
      _str_query = New StringBuilder()

      _str_query.AppendLine("    UPDATE   IDENTIFICATION_TYPES ")
      _str_query.AppendLine("       SET   IDT_ENABLED =        ")
      If m_visible_documents.Length >= 1 Then
        _str_query.AppendLine(" CASE WHEN   IDT_ID IN (" & m_visible_documents & ") ")
        _str_query.AppendLine("      THEN   1 ELSE 0 END")
      Else
        _str_query.AppendLine("0")
      End If

      If Not GUI_SQLExecuteNonQuery(_str_query.ToString(), Trx) Then

        Return False
      End If

      Return True
    Catch _ex As Exception
      Log.Exception(_ex)

      Return False
    End Try
  End Function

  ' PURPOSE:  Update Default Params
  '
  '  PARAMS:
  '     - INPUT:
  '           - Trx : Transaction
  '
  Private Function Trx_UpdateDefaultParams(ByVal Trx As SqlTransaction) As Boolean
    Dim _str_country As String
    Dim _str_document_type As Integer
    Dim _str_value As String
    Dim _dr_value() As DataRow
    Dim rowfilter As String

    Try
      ' UPDATE COUNTRY PARAM
      If Me.m_dic_countries.ContainsKey(Me.m_default_country) Then
        _str_country = Me.m_dic_countries(Me.m_default_country)
      Else
        _str_country = String.Empty
      End If

      If Not DB_GeneralParam_Update("Account.DefaultValues", "Country", _str_country, Trx) Then
        Return ENUM_STATUS.STATUS_ERROR
      End If

      _str_value = "IDT_NAME = '{0}'"

      rowfilter = "IDT_NAME = '" & Me.m_default_document.Replace("'", "''") & "'"

      'UPDATE DOCUMENT TYPE PARAM
      _dr_value = Me.m_dt_identification_types.Select(rowfilter)

      If _dr_value Is Nothing OrElse _dr_value.Length <= 0 Then
        If Not DB_GeneralParam_Update("Account.DefaultValues", "DocumentType", String.Empty, Trx) Then
          Return ENUM_STATUS.STATUS_ERROR
        End If
      Else
        _str_document_type = Convert.ToInt32(_dr_value(0).Item(0))
        If Not DB_GeneralParam_Update("Account.DefaultValues", "DocumentType", _str_document_type.ToString("000"), Trx) Then
          Return ENUM_STATUS.STATUS_ERROR
        End If
      End If

      Return True
    Catch _ex As Exception
      Log.Exception(_ex)

      Return False
    End Try
  End Function

#End Region

End Class
