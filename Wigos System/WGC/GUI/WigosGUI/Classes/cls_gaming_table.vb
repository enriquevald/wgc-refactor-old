'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : cls_gaming_table.vb
'
' DESCRIPTION : gaming table class for user edition
'              
' REVISION HISTORY :
'
' Date         Author  Description
' -----------  ------  -----------------------------------------------
' 16-DEC-2013  JBP     Initial version
' 08-JUL-2014  DHA     Added gambling table design parameters
' 15-SEP-2014  DHA     Added functionality to create a new gaming table based on other gaming table
' 30-SEP-2014  DHA     Fixed Bug #WIG-1348: error when created new gaming table
' 30-SEP-2014  DCS&DHA Fixed Bug #WIG-1355: error when edit gaming table
' 25-JAN-2016  RGR     Product Backlog Item 4951:Tables: Groups and MD: Groups of chips on the table
' 05-MAY-2016  RAB     PBI 9848: Management drop box (Phase 1)
' 29-JUN-2016  FOS     Product Backlog Item 13403: Tables: GUI modifications (Phase 1) 
' 06-JUL-2016  FOS     Product Backlog Item 15070: Tables: GUI configuration (Phase 4)
' 18-AUG-2016  RAB     Bug 16862: Gaming Tables - GUI: No is saving minimum and maximum bet information or the type of operations to create a new table
' 08-SEP-2016  FOS     PBI 16373: Tables: Split PlayerTracking to GamingTables (Phase 5)
' 30-SEP-2016  FOS     PBI 18374: Gaming table provider is saved empty (Without player tracking).
' 16-DEC-2016  FOS     Fixed Bug 1689: Add Cashier name in the field of auditor data
' 19-DEC-2016  DHA     Fixed bug 21730: error on creating gaming tables, seats are not inserted.
' 05-SEP-2017  DHA     PBI 28629:WIGOS-3350 MES22 - Linked gaming table configuration
' 01-MAR-2018  JML     Fixed Bug 31768:WIGOS-7947 [Ticket #12063] mesas de Bacarat no estan aperturando con la banca de dia anterior
' 21-MAR-2018  AGS     Bug 32007: WIGOS-9153 Error when opening gambling tables - Exception error: invalid column name
'-------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports System.Text
Imports WSI.Common
Imports WSI.Common.ClosingStocks
Imports System.IO

Public Class CLASS_GAMING_TABLE
  Inherits CLASS_BASE

#Region " Constants "

#End Region ' Constants

#Region " Enums "


#End Region ' Enums 

#Region " Structures "

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_GAMING_TABLE
    Public gaming_table_id As Integer
    Public code As String
    Public type_id As Int64
    Public type_name As String
    Public name As String
    Public description As String
    Public area_id As Integer
    Public bank_id As Integer
    Public floor_id As String
    Public area_name As String
    Public bank_name As String
    Public enabled As Boolean = True ' Initialite new gaming table to true.
    Public integrated_cashier As Boolean
    Public cashier_id As Integer
    Public exist_cage_movements As Boolean
    Public created As DateTime
    Public gaming_session_id As Integer
    Public table_speed_normal As Integer
    Public table_speed_slow As Integer
    Public table_speed_fast As Integer
    Public table_num_seats As Integer
    Public table_bet_min As Decimal
    Public table_bet_max As Decimal
    Public table_design_xml As String
    Public provider_id As Integer
    Public table_idle_plays As Integer
    Public provider_name As String
    Public theoric_hold As Decimal
    Public chips_allowed As DataTable
    Public table_bet_min_2 As Decimal
    Public table_bet_max_2 As Decimal
    Public drop_box_enabled As Boolean
    Public closing_stock As ClosingStocks
    Public bet_currencies As DataTable
    Public cashier_name As String
    Public linked_cashier_id As Integer
  End Class


  Public Class TYPE_BET_CURRENCIES
    Public max_bet As Decimal
    Public min_bet As Decimal
    Public iso_code As String
    Public status As Integer
    Public gamingTableId As Integer
  End Class

#End Region ' Structures

#Region " Members "

  Protected m_gaming_table As New TYPE_GAMING_TABLE
  Protected m_dt_bet_by_currency As DataTable

#End Region ' Members

#Region " Properties "

  Public Property GamingTableID() As Integer
    Get
      Return m_gaming_table.gaming_table_id
    End Get

    Set(ByVal Value As Integer)
      m_gaming_table.gaming_table_id = Value
    End Set
  End Property

  Public Property Code() As String
    Get
      Return m_gaming_table.code
    End Get

    Set(ByVal Value As String)
      m_gaming_table.code = Value
    End Set
  End Property

  Public Property TypeID() As Int64
    Get
      Return m_gaming_table.type_id
    End Get

    Set(ByVal Value As Int64)
      m_gaming_table.type_id = Value
    End Set
  End Property

  Public Property TypeName() As String
    Get
      Return m_gaming_table.type_name
    End Get

    Set(ByVal Value As String)
      m_gaming_table.type_name = Value
    End Set
  End Property

  Public Property Name() As String
    Get
      Return m_gaming_table.name
    End Get

    Set(ByVal Value As String)
      m_gaming_table.name = Value
    End Set
  End Property

  Public Property Description() As String
    Get
      Return m_gaming_table.description
    End Get

    Set(ByVal Value As String)
      m_gaming_table.description = Value
    End Set
  End Property

  Public Property AreaID() As Integer
    Get
      Return m_gaming_table.area_id
    End Get

    Set(ByVal Value As Integer)
      m_gaming_table.area_id = Value
    End Set
  End Property

  Public Property BankID() As Integer
    Get
      Return m_gaming_table.bank_id
    End Get

    Set(ByVal Value As Integer)
      m_gaming_table.bank_id = Value
    End Set
  End Property

  Public Property FloorID() As String
    Get
      Return m_gaming_table.floor_id
    End Get

    Set(ByVal Value As String)
      m_gaming_table.floor_id = Value
    End Set
  End Property

  Public Property AreaName() As String
    Get
      Return m_gaming_table.area_name
    End Get

    Set(ByVal Value As String)
      m_gaming_table.area_name = Value
    End Set
  End Property

  Public Property BankName() As String
    Get
      Return m_gaming_table.bank_name
    End Get

    Set(ByVal Value As String)
      m_gaming_table.bank_name = Value
    End Set
  End Property

  Public Property Enabled() As Boolean
    Get
      Return m_gaming_table.enabled
    End Get

    Set(ByVal Value As Boolean)
      m_gaming_table.enabled = Value
    End Set
  End Property

  Public Property IntegratedCashier() As Boolean
    Get
      Return m_gaming_table.integrated_cashier
    End Get

    Set(ByVal Value As Boolean)
      m_gaming_table.integrated_cashier = Value
    End Set
  End Property

  Public Property CashierID() As Integer
    Get
      Return m_gaming_table.cashier_id
    End Get

    Set(ByVal Value As Integer)
      m_gaming_table.cashier_id = Value
    End Set
  End Property

  Public Property TableSpeedNormal() As Integer
    Get
      Return m_gaming_table.table_speed_normal
    End Get

    Set(ByVal Value As Integer)
      m_gaming_table.table_speed_normal = Value
    End Set
  End Property

  Public Property TableSpeedSlow() As Integer
    Get
      Return m_gaming_table.table_speed_slow
    End Get

    Set(ByVal Value As Integer)
      m_gaming_table.table_speed_slow = Value
    End Set
  End Property

  Public Property TableSpeedFast() As Integer
    Get
      Return m_gaming_table.table_speed_fast
    End Get

    Set(ByVal Value As Integer)
      m_gaming_table.table_speed_fast = Value
    End Set
  End Property

  Public Property TableNumSeats() As Integer
    Get
      Return m_gaming_table.table_num_seats
    End Get

    Set(ByVal Value As Integer)
      m_gaming_table.table_num_seats = Value
    End Set
  End Property

  Public Property TableBetMin() As Decimal
    Get
      Return m_gaming_table.table_bet_min
    End Get

    Set(ByVal Value As Decimal)
      m_gaming_table.table_bet_min = Value
    End Set
  End Property

  Public Property TableBetMax() As Decimal
    Get
      Return m_gaming_table.table_bet_max
    End Get

    Set(ByVal Value As Decimal)
      m_gaming_table.table_bet_max = Value
    End Set
  End Property

  Public Property TableDesignXml() As String
    Get
      Return m_gaming_table.table_design_xml
    End Get

    Set(ByVal Value As String)
      m_gaming_table.table_design_xml = Value
    End Set
  End Property

  Public Property ProviderID() As Integer
    Get
      Return m_gaming_table.provider_id
    End Get

    Set(ByVal Value As Integer)
      m_gaming_table.provider_id = Value
    End Set
  End Property

  Public Property TableIdlePlays() As Integer
    Get
      Return m_gaming_table.table_idle_plays
    End Get

    Set(ByVal Value As Integer)
      m_gaming_table.table_idle_plays = Value
    End Set
  End Property

  Public Property ProviderName() As String
    Get
      Return m_gaming_table.provider_name
    End Get

    Set(ByVal Value As String)
      m_gaming_table.provider_name = Value
    End Set
  End Property

  Public Property TheoricHold() As Decimal
    Get
      Return m_gaming_table.theoric_hold
    End Get

    Set(ByVal Value As Decimal)
      m_gaming_table.theoric_hold = Value
    End Set
  End Property

  Public Property ChipsAllowed() As DataTable
    Get
      Return m_gaming_table.chips_allowed
    End Get

    Set(ByVal Value As DataTable)
      m_gaming_table.chips_allowed = Value
    End Set
  End Property

  Public Property TableBetMin2() As Decimal
    Get
      Return m_gaming_table.table_bet_min_2
    End Get

    Set(ByVal Value As Decimal)
      m_gaming_table.table_bet_min_2 = Value
    End Set
  End Property

  Public Property TableBetMax2() As Decimal
    Get
      Return m_gaming_table.table_bet_max_2
    End Get

    Set(ByVal Value As Decimal)
      m_gaming_table.table_bet_max_2 = Value
    End Set
  End Property

  Public Property DropBoxEnabled As Boolean
    Get
      Return m_gaming_table.drop_box_enabled
    End Get
    Set(ByVal Value As Boolean)
      m_gaming_table.drop_box_enabled = Value
    End Set
  End Property

  Public Property ClosingStock() As ClosingStocks
    Get
      Return m_gaming_table.closing_stock
    End Get
    Set(ByVal value As ClosingStocks)
      m_gaming_table.closing_stock = value
    End Set
  End Property

  Public Property BetByCurrencies() As DataTable
    Get
      Return m_gaming_table.bet_currencies
    End Get
    Set(ByVal value As DataTable)
      m_gaming_table.bet_currencies = value
    End Set
  End Property

  Public Property CashierName() As String
    Get
      Return m_gaming_table.cashier_name
    End Get

    Set(ByVal Value As String)
      m_gaming_table.cashier_name = Value
    End Set
  End Property

  Public Property LinkedCashierid() As String
    Get
      Return m_gaming_table.linked_cashier_id
    End Get

    Set(ByVal Value As String)
      m_gaming_table.linked_cashier_id = Value
    End Set
  End Property

#End Region ' Structures

#Region " Public Functions "

#End Region ' Public Functions

#Region " Constructor "

  Public Sub New()
    ' Create datatables
    ChipsAllowed = New DataTable()
    ChipsAllowed.Columns.Add("GTCS_GAMING_TABLE_ID")
    ChipsAllowed.Columns.Add("GTCS_CHIPS_SET_ID")

    ClosingStock = New ClosingStocks(0)
    ClosingStock.CurrenciesStocks = New DataTable("CLOSING_STOCKS")
    ClosingStock.CurrenciesStocks.Columns.Add("ISO_CODE", GetType(System.String))
    ClosingStock.CurrenciesStocks.Columns.Add("DENOMINATION", GetType(System.Decimal))
    ClosingStock.CurrenciesStocks.Columns.Add("CAGE_CURRENCY_TYPE", GetType(CageCurrencyType))
    ClosingStock.CurrenciesStocks.Columns.Add("CHIP_ID", GetType(System.Int64))
    ClosingStock.CurrenciesStocks.Columns.Add("QUANTITY", GetType(System.Int32))
    ClosingStock.CurrenciesStocks.Columns.Add("TOTAL", GetType(System.Decimal))

    BetByCurrencies = New DataTable("Table")
    BetByCurrencies.Columns.Add("GTB_ISO_CODE", GetType(System.String))
    BetByCurrencies.Columns.Add("GTB_MAX_BET", GetType(System.Decimal))
    BetByCurrencies.Columns.Add("GTB_MIN_BET", GetType(System.Decimal))
  End Sub

  '----------------------------------------------------------------------------
  ' PURPOSE : Reads from the database into the given object
  '
  ' PARAMS :
  '     - INPUT :
  '         - ObjectId: An object, it must be 'casted' to the desired KEY.
  '         - SqlCtx: SqlTransaction.
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '
  ' NOTES :

  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As ENUM_STATUS

    Me.GamingTableID = ObjectId
    _rc = ENUM_STATUS.STATUS_ERROR

    Using _db_trx As New DB_TRX()
      ' Read terminal data
      _rc = ReadGamingTable(_db_trx.SqlTransaction)
    End Using

    Return _rc

  End Function ' DB_Read

  '----------------------------------------------------------------------------
  ' PURPOSE : Updates the given object to the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_DEPENDENCIES
  '     - STATUS_DUPLICATE_KEY
  '     - STATUS_NOT_SUPPORTED
  '     - STATUS_NOT_FOUND
  '     - STATUS_ERROR
  '
  ' NOTES :

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As ENUM_STATUS

    _rc = ENUM_STATUS.STATUS_ERROR

    Using _db_trx As New DB_TRX()
      ' Update terminal data
      _rc = UpdateGamingTable(_db_trx.SqlTransaction)
    End Using

    Return _rc

  End Function ' DB_Update

  '----------------------------------------------------------------------------
  ' PURPOSE : Inserts the given object to the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES :

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As ENUM_STATUS

    _rc = ENUM_STATUS.STATUS_ERROR

    Using _db_trx As New DB_TRX()
      ' Insert terminal data
      _rc = InsertGamingTable(_db_trx.SqlTransaction)

    End Using

    Return _rc

  End Function ' DB_Insert

  '----------------------------------------------------------------------------
  ' PURPOSE : Deletes the given object from the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '
  ' NOTES :

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As ENUM_STATUS

    _rc = ENUM_STATUS.STATUS_ERROR

    Using _db_trx As New DB_TRX()
      ' Delete terminal data
      _rc = DeleteGamingTable(_db_trx.SqlTransaction)
    End Using

    Return _rc

  End Function ' DB_Delete

  '----------------------------------------------------------------------------
  ' PURPOSE : Returns a duplicate of the given object.
  '
  ' PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - The newly created object
  '
  ' NOTES :

  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim temp_object As CLASS_GAMING_TABLE

    temp_object = New CLASS_GAMING_TABLE
    temp_object.m_gaming_table.gaming_table_id = Me.m_gaming_table.gaming_table_id
    temp_object.m_gaming_table.code = Me.m_gaming_table.code
    temp_object.m_gaming_table.type_id = Me.m_gaming_table.type_id
    temp_object.m_gaming_table.type_name = Me.m_gaming_table.type_name
    temp_object.m_gaming_table.name = Me.m_gaming_table.name
    temp_object.m_gaming_table.description = Me.m_gaming_table.description
    temp_object.m_gaming_table.area_id = Me.m_gaming_table.area_id
    temp_object.m_gaming_table.bank_id = Me.m_gaming_table.bank_id
    temp_object.m_gaming_table.floor_id = Me.m_gaming_table.floor_id
    temp_object.m_gaming_table.area_name = Me.m_gaming_table.area_name
    temp_object.m_gaming_table.bank_name = Me.m_gaming_table.bank_name
    temp_object.m_gaming_table.enabled = Me.m_gaming_table.enabled
    temp_object.m_gaming_table.integrated_cashier = Me.m_gaming_table.integrated_cashier
    temp_object.m_gaming_table.cashier_id = Me.m_gaming_table.cashier_id
    temp_object.m_gaming_table.created = Me.m_gaming_table.created
    temp_object.m_gaming_table.gaming_session_id = Me.m_gaming_table.gaming_session_id
    temp_object.m_gaming_table.table_speed_normal = Me.m_gaming_table.table_speed_normal
    temp_object.m_gaming_table.table_speed_slow = Me.m_gaming_table.table_speed_slow
    temp_object.m_gaming_table.table_speed_fast = Me.m_gaming_table.table_speed_fast
    temp_object.m_gaming_table.table_num_seats = Me.m_gaming_table.table_num_seats
    temp_object.m_gaming_table.table_bet_min = Me.m_gaming_table.table_bet_min
    temp_object.m_gaming_table.table_bet_max = Me.m_gaming_table.table_bet_max
    temp_object.m_gaming_table.provider_id = Me.m_gaming_table.provider_id
    temp_object.m_gaming_table.table_design_xml = Me.m_gaming_table.table_design_xml
    temp_object.m_gaming_table.table_idle_plays = Me.m_gaming_table.table_idle_plays
    temp_object.m_gaming_table.provider_name = Me.m_gaming_table.provider_name
    temp_object.m_gaming_table.theoric_hold = Me.m_gaming_table.theoric_hold
    temp_object.m_gaming_table.chips_allowed = Me.m_gaming_table.chips_allowed.Copy()
    temp_object.m_gaming_table.table_bet_min_2 = Me.m_gaming_table.table_bet_min_2
    temp_object.m_gaming_table.table_bet_max_2 = Me.m_gaming_table.table_bet_max_2
    temp_object.m_gaming_table.drop_box_enabled = Me.m_gaming_table.drop_box_enabled
    temp_object.m_gaming_table.closing_stock = Me.m_gaming_table.closing_stock.Clone()
    temp_object.m_gaming_table.bet_currencies = Me.m_gaming_table.bet_currencies.Copy()
    temp_object.m_gaming_table.cashier_name = Me.m_gaming_table.cashier_name
    temp_object.m_gaming_table.linked_cashier_id = Me.m_gaming_table.linked_cashier_id

    Return temp_object

  End Function ' Duplicate

  '----------------------------------------------------------------------------
  ' PURPOSE : Returns the related auditor data for the current object.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - The newly created audit object
  '
  ' NOTES :

  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _field_value As String
    Dim _chipsSet As DataTable
    Dim _search As DataRow()
    Dim _dv_closing_stocks As DataView
    Dim _dt_closing_stocks As DataTable
    Dim _dv_bet_currencies As DataView
    Dim _dt_bet_currencies As DataTable
    Dim _quantity As Integer
    Dim _description As String
    Dim _chips_max_bet As Integer
    Dim _chips_min_bet As Integer

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_CASHIER_CONFIGURATION)

    Call _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(3416), Me.Code)               ' 3416 "Mesas de Juego"

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(3418), Me.Code)              ' 3418 "C�digo"
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(3419), Me.Name)              ' 3419 "Nombre"
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(3420), Me.TypeName)          ' 3420 "Tipo de Mesa"   

    If m_gaming_table.enabled Then
      _field_value = GLB_NLS_GUI_CONFIGURATION.GetString(264)
    Else
      _field_value = GLB_NLS_GUI_CONFIGURATION.GetString(265)
    End If

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(3422), _field_value)         ' 3421 "Habilitada"

    Call _auditor_data.SetField(0, Me.TableSpeedSlow, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4954) & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5021))        ' 4954 "Velocidad mesa" - Slow

    Call _auditor_data.SetField(0, Me.TableSpeedNormal, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4954) & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5022))        ' 4954 "Velocidad mesa" - Normal

    Call _auditor_data.SetField(0, Me.TableSpeedFast, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4954) & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5023))        ' 4954 "Velocidad mesa" - Fast

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(4955), Me.TableNumSeats)     ' 4955 "N�m. asientos"

    Call _auditor_data.SetField(GLB_NLS_GUI_AUDITOR.Id(341), Me.ProviderName)         ' 341 "Proveedor"

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5070), GUI_FormatCurrency(Me.TableBetMin, 2))     ' 4955 "Apuesta m�nima"

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5071), GUI_FormatCurrency(Me.TableBetMax, 2))     ' 4955 "Apuesta m�xima"

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5070), GUI_FormatCurrency(Me.TableBetMin2, 2), "2")     ' 4955 "Apuesta m�nima 2"

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5071), GUI_FormatCurrency(Me.TableBetMax2, 2), "2")     ' 4955 "Apuesta m�xima 2"

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(3423), Me.Description)       ' 3421 "Descripci�n"

    If m_gaming_table.integrated_cashier Then
      _field_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4339)
    ElseIf m_gaming_table.linked_cashier_id > 0 Then
      _field_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8482)
    Else
      _field_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4340)
    End If

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(3479), _field_value)         ' 3421 "Cajero"

    If m_gaming_table.integrated_cashier Or m_gaming_table.linked_cashier_id > 0 Then
      _field_value = "'" & Me.CashierName & "'"
    Else
      _field_value = "---"
    End If

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(3479), _field_value)         ' 3421 "Cajero"

    If Me.BankName = String.Empty Then 'NOTHING
      Call _auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(431), "---")
    Else
      Call _auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(431), Me.BankName + " (" + Me.AreaName + ")") ' 431 "Isla" (435 "Area")
    End If

    Call _auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(432), Me.FloorID)  ' 432 "Id en planta"

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5587), Me.TableIdlePlays)  ' "Table idle plays"

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5629), Me.TheoricHold)  ' "Theoric hold %"

    If Me.DropBoxEnabled Then
      _field_value = GLB_NLS_GUI_CONFIGURATION.GetString(264)
    Else
      _field_value = GLB_NLS_GUI_CONFIGURATION.GetString(265)
    End If

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7298), _field_value)    ' 7298 "Gesti�n drop box"

    If WSI.Common.GamingTableBusinessLogic.IsGamingTablesEnabled() Then
      AuditGamingTableDesign(_auditor_data)
    End If

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7360), Me.ClosingStock.Type)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7361), Me.ClosingStock.SleepsOnTable)

    If (Not String.IsNullOrEmpty(Me.ClosingStock.CurrenciesStocks.TableName)) Then
      _dv_closing_stocks = Me.ClosingStock.CurrenciesStocks.DefaultView
      _dv_closing_stocks.Sort = "ISO_CODE,DENOMINATION" & " DESC"
      _dt_closing_stocks = _dv_closing_stocks.ToTable()

      For Each _row As DataRow In _dt_closing_stocks.Rows
        _quantity = 0
        If Not String.IsNullOrEmpty(_row("QUANTITY").ToString()) Then
          _quantity = _row("QUANTITY")
        End If
        _description = "(" & GUI_FormatCurrency(_row("DENOMINATION"), 2) & "/" & _row("ISO_CODE") & ")"
        Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5052), String.Format("{0} - {1}", _description, _quantity))
      Next
    End If

    'Bet currencies
    If (Not String.IsNullOrEmpty(Me.BetByCurrencies.TableName)) Then
      _dv_bet_currencies = Me.BetByCurrencies.DefaultView
      _dv_bet_currencies.Sort = "GTB_ISO_CODE" & " DESC"
      _dt_bet_currencies = _dv_bet_currencies.ToTable()

      For Each _row As DataRow In _dt_bet_currencies.Rows
        _chips_max_bet = 0
        _chips_min_bet = 0

        _description = _row("GTB_ISO_CODE")
        If Not String.IsNullOrEmpty(_row("GTB_MAX_BET").ToString()) Then
          _chips_max_bet = _row("GTB_MAX_BET")
        End If
        Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7391), String.Format("{0} - {1}", _description, _chips_max_bet))

        If Not String.IsNullOrEmpty(_row("GTB_MIN_BET").ToString()) Then
          _chips_min_bet = _row("GTB_MIN_BET")
        End If
        Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7392), String.Format("{0} - {1}", _description, _chips_min_bet))
      Next
    End If

    _chipsSet = ReadChipsIds()
    For Each _item As DataRow In Me.m_gaming_table.chips_allowed.Rows
      If _item.RowState <> DataRowState.Deleted Then
        _search = _chipsSet.Select(String.Format("CHS_CHIP_SET_ID = {0}", _item("GTCS_CHIPS_SET_ID")))
        If _search.Length > 0 Then
          Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5059), String.Format("{0} - {1}", _search(0)("CHS_NAME"), _search(0)("CHS_ISO_CODE")))
        End If
      End If
    Next


    Return _auditor_data

  End Function ' AuditorData

#End Region ' Constructor

#Region " Private Functions "

  '----------------------------------------------------------------------------
  ' PURPOSE : Reads a Gaming Table object from DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - SqlTrans
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_NOT_FOUND
  '     - STATUS_ERROR
  '
  ' NOTES :

  Private Function ReadGamingTable(ByVal SqlTrans As SqlTransaction) As Integer

    Dim _sb As StringBuilder

    Dim _data_table As DataTable

    Try

      _sb = New StringBuilder()

      _sb.AppendLine("SELECT   GT_GAMING_TABLE_ID			                ")
      _sb.AppendLine("	     , GT_CODE					                      ")
      _sb.AppendLine("	     , GT_TYPE_ID				                      ")
      _sb.AppendLine("	     , GT_NAME					                      ")
      _sb.AppendLine("	     , GT_DESCRIPTION			                    ")
      _sb.AppendLine("	     , GT_AREA_ID 				                    ")
      _sb.AppendLine("	     , AR_NAME     				                    ")
      _sb.AppendLine("	     , GT_BANK_ID 				                    ")
      _sb.AppendLine("	     , BK_NAME				                        ")
      _sb.AppendLine("	     , GT_FLOOR_ID				                    ")
      _sb.AppendLine("	     , GT_ENABLED				                      ")
      _sb.AppendLine("	     , GT_HAS_INTEGRATED_CASHIER	            ")
      _sb.AppendLine("	     , GT_CASHIER_ID	                        ")
      _sb.AppendLine("	     , GTT_NAME             	                ")
      _sb.AppendLine("	     , GT_CREATED             	              ")
      _sb.AppendLine("	     , GT_TABLE_SPEED_NORMAL        	        ")
      _sb.AppendLine("	     , GT_TABLE_SPEED_SLOW          	        ")
      _sb.AppendLine("	     , GT_TABLE_SPEED_FAST          	        ")
      _sb.AppendLine("	     , GT_NUM_SEATS          	                ")
      _sb.AppendLine("       , GT_NUM_SEATS                           ")
      _sb.AppendLine("       , GT_BET_MIN                             ")
      _sb.AppendLine("       , GT_BET_MAX                             ")
      _sb.AppendLine("       , GT_DESIGN_XML                          ")
      _sb.AppendLine("       , GT_PROVIDER_ID                         ")
      _sb.AppendLine("       , GT_TABLE_IDLE_PLAYS                    ")
      _sb.AppendLine("       , PV_NAME                                ")
      _sb.AppendLine("       , GT_THEORIC_HOLD                        ")
      _sb.AppendLine("       , GT_BET_MIN_CURR_2                      ")
      _sb.AppendLine("       , GT_BET_MAX_CURR_2                      ")
      _sb.AppendLine("       , GT_DROPBOX_ENABLED                     ")
      _sb.AppendLine("       , GT_LINKED_CASHIER_ID                   ")
      _sb.AppendLine("  FROM   GAMING_TABLES			                    ")
      _sb.AppendLine("  LEFT   JOIN GAMING_TABLES_TYPES		          	")
      _sb.AppendLine("    ON   GTT_GAMING_TABLE_TYPE_ID = GT_TYPE_ID  ")
      _sb.AppendLine("  LEFT   JOIN AREAS                             ")
      _sb.AppendLine("    ON   GT_AREA_ID = AR_AREA_ID                ")
      _sb.AppendLine("  LEFT   JOIN BANKS                             ")
      _sb.AppendLine("    ON   GT_BANK_ID = BK_BANK_ID                ")
      _sb.AppendLine("  LEFT   JOIN PROVIDERS                         ")
      _sb.AppendLine("    ON   PV_ID = GT_PROVIDER_ID                 ")

      _sb.AppendLine(" WHERE GT_GAMING_TABLE_ID = " + Me.GamingTableID.ToString())

      _data_table = GUI_GetTableUsingSQL(_sb.ToString(), 5000)

      If IsNothing(_data_table) Then
        Return ENUM_STATUS.STATUS_ERROR
      End If

      If _data_table.Rows.Count() = 0 Then
        Return ENUM_STATUS.STATUS_NOT_FOUND
      End If

      If _data_table.Rows.Count() <> 1 Then
        Return ENUM_STATUS.STATUS_ERROR
      End If

      Me.Code = IIf(IsDBNull(_data_table.Rows(0).Item("GT_CODE")), "", _data_table.Rows(0).Item("GT_CODE"))
      Me.Name = IIf(IsDBNull(_data_table.Rows(0).Item("GT_NAME")), "", _data_table.Rows(0).Item("GT_NAME"))
      Me.TypeID = IIf(IsDBNull(_data_table.Rows(0).Item("GT_TYPE_ID")), 0, _data_table.Rows(0).Item("GT_TYPE_ID"))
      Me.TypeName = IIf(IsDBNull(_data_table.Rows(0).Item("GTT_NAME")), "", _data_table.Rows(0).Item("GTT_NAME"))
      Me.Description = IIf(IsDBNull(_data_table.Rows(0).Item("GT_DESCRIPTION")), "", _data_table.Rows(0).Item("GT_DESCRIPTION"))
      Me.AreaID = IIf(IsDBNull(_data_table.Rows(0).Item("GT_AREA_ID")), 0, _data_table.Rows(0).Item("GT_AREA_ID"))
      Me.AreaName = IIf(IsDBNull(_data_table.Rows(0).Item("AR_NAME")), "", _data_table.Rows(0).Item("AR_NAME"))
      Me.BankID = IIf(IsDBNull(_data_table.Rows(0).Item("GT_BANK_ID")), -1I, _data_table.Rows(0).Item("GT_BANK_ID"))
      Me.BankName = IIf(IsDBNull(_data_table.Rows(0).Item("BK_NAME")), "", _data_table.Rows(0).Item("BK_NAME"))
      Me.FloorID = IIf(IsDBNull(_data_table.Rows(0).Item("GT_FLOOR_ID")), 0, _data_table.Rows(0).Item("GT_FLOOR_ID"))
      Me.Enabled = IIf(IsDBNull(_data_table.Rows(0).Item("GT_ENABLED")), False, _data_table.Rows(0).Item("GT_ENABLED"))
      Me.IntegratedCashier = IIf(IsDBNull(_data_table.Rows(0).Item("GT_HAS_INTEGRATED_CASHIER")), False, _data_table.Rows(0).Item("GT_HAS_INTEGRATED_CASHIER"))
      Me.CashierID = IIf(IsDBNull(_data_table.Rows(0).Item("GT_CASHIER_ID")), Nothing, _data_table.Rows(0).Item("GT_CASHIER_ID"))
      Me.TableSpeedNormal = IIf(IsDBNull(_data_table.Rows(0).Item("GT_TABLE_SPEED_NORMAL")), Nothing, _data_table.Rows(0).Item("GT_TABLE_SPEED_NORMAL"))
      Me.TableSpeedSlow = IIf(IsDBNull(_data_table.Rows(0).Item("GT_TABLE_SPEED_SLOW")), Nothing, _data_table.Rows(0).Item("GT_TABLE_SPEED_SLOW"))
      Me.TableSpeedFast = IIf(IsDBNull(_data_table.Rows(0).Item("GT_TABLE_SPEED_FAST")), Nothing, _data_table.Rows(0).Item("GT_TABLE_SPEED_FAST"))
      Me.TableNumSeats = IIf(IsDBNull(_data_table.Rows(0).Item("GT_NUM_SEATS")), Nothing, _data_table.Rows(0).Item("GT_NUM_SEATS"))
      Me.TableBetMin = IIf(IsDBNull(_data_table.Rows(0).Item("GT_BET_MIN")), Nothing, _data_table.Rows(0).Item("GT_BET_MIN"))
      Me.TableBetMax = IIf(IsDBNull(_data_table.Rows(0).Item("GT_BET_MAX")), Nothing, _data_table.Rows(0).Item("GT_BET_MAX"))
      Me.TableDesignXml = IIf(IsDBNull(_data_table.Rows(0).Item("GT_DESIGN_XML")), Nothing, _data_table.Rows(0).Item("GT_DESIGN_XML"))
      Me.ProviderID = IIf(IsDBNull(_data_table.Rows(0).Item("GT_PROVIDER_ID")), -1, _data_table.Rows(0).Item("GT_PROVIDER_ID"))
      Me.TableIdlePlays = IIf(IsDBNull(_data_table.Rows(0).Item("GT_TABLE_IDLE_PLAYS")), Nothing, _data_table.Rows(0).Item("GT_TABLE_IDLE_PLAYS"))
      Me.ProviderName = IIf(IsDBNull(_data_table.Rows(0).Item("PV_NAME")), "", _data_table.Rows(0).Item("PV_NAME"))
      Me.TheoricHold = IIf(IsDBNull(_data_table.Rows(0).Item("GT_THEORIC_HOLD")), 0, _data_table.Rows(0).Item("GT_THEORIC_HOLD"))
      Me.TableBetMin2 = IIf(IsDBNull(_data_table.Rows(0).Item("GT_BET_MIN_CURR_2")), Nothing, _data_table.Rows(0).Item("GT_BET_MIN_CURR_2"))
      Me.TableBetMax2 = IIf(IsDBNull(_data_table.Rows(0).Item("GT_BET_MAX_CURR_2")), Nothing, _data_table.Rows(0).Item("GT_BET_MAX_CURR_2"))
      Me.DropBoxEnabled = _data_table.Rows(0).Item("GT_DROPBOX_ENABLED")
      Me.LinkedCashierid = IIf(IsDBNull(_data_table.Rows(0).Item("GT_LINKED_CASHIER_ID")), -1, _data_table.Rows(0).Item("GT_LINKED_CASHIER_ID"))

      Me.ClosingStock = New ClosingStocks(Me.CashierID)
      Me.BetByCurrencies = ReadCurrenciesBet(Me.GamingTableID)
      Me.ClosingStock.CashierIdOld = Me.CashierID

      If (Me.IntegratedCashier) Then
        Me.CashierName = ReadCashierName(Me.ClosingStock.CashierId, SqlTrans)
      ElseIf Me.LinkedCashierid > 0 Then
        Me.CashierName = ReadCashierName(Me.LinkedCashierid, SqlTrans)
      Else
        Me.CashierName = String.Empty
      End If

      ' Create the table chips sets
      If ReadChipsSets(SqlTrans) <> ENUM_STATUS.STATUS_OK Then
        Return ENUM_STATUS.STATUS_ERROR
      End If

      Return ENUM_STATUS.STATUS_OK

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return ENUM_STATUS.STATUS_ERROR

  End Function ' ReadGamingTable

  '----------------------------------------------------------------------------
  ' PURPOSE : Deletes a Gaming Table object from DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - SqlTrans
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_ERROR
  '
  ' NOTES :

  Private Function DeleteGamingTable(ByVal SqlTrans As SqlTransaction) As Integer
    Return ENUM_STATUS.STATUS_ERROR
  End Function ' DeleteGamingTable

  '----------------------------------------------------------------------------
  ' PURPOSE : Adds a Gaming Table object into DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - SqlTrans
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_DEPENDENCIES
  '     - STATUS_DUPLICATE_KEY
  '     - STATUS_ERROR
  '
  ' NOTES :

  Private Function InsertGamingTable(ByVal SqlTrans As SqlTransaction) As Integer

    Dim _sb As StringBuilder
    Dim _param_id As SqlParameter
    Dim _row_affected As Integer
    Dim _rc As ENUM_STATUS

    Try

      ' Check if name is valid. 
      _rc = ExistName(SqlTrans)

      If _rc <> ENUM_STATUS.STATUS_OK Then
        Return _rc
      End If

      ' Check if table type is valid. 
      _rc = ExistTableType(SqlTrans)

      If _rc <> ENUM_STATUS.STATUS_OK Then
        Return _rc
      End If

      _sb = New StringBuilder()

      _sb.AppendLine("INSERT INTO GAMING_TABLES					        ")
      _sb.AppendLine("           (  GT_CODE						          ")
      _sb.AppendLine("           	, GT_TYPE_ID					        ")
      _sb.AppendLine("           	, GT_NAME						          ")
      _sb.AppendLine("           	, GT_DESCRIPTION				      ")
      _sb.AppendLine("           	, GT_AREA_ID					        ")
      _sb.AppendLine("           	, GT_BANK_ID					        ")
      _sb.AppendLine("           	, GT_FLOOR_ID					        ")
      _sb.AppendLine("           	, GT_ENABLED					        ")
      _sb.AppendLine("           	, GT_HAS_INTEGRATED_CASHIER 	")
      _sb.AppendLine("           	, GT_CASHIER_ID             	")
      _sb.AppendLine("           	, GT_CREATED                	")
      _sb.AppendLine("           	, GT_TABLE_SPEED_NORMAL       ")
      _sb.AppendLine("           	, GT_TABLE_SPEED_SLOW         ")
      _sb.AppendLine("           	, GT_TABLE_SPEED_FAST         ")
      _sb.AppendLine("           	, GT_TABLE_SPEED_SELECTED     ")
      _sb.AppendLine("           	, GT_NUM_SEATS              	")
      _sb.AppendLine("           	, GT_BET_MIN                  ")
      _sb.AppendLine("           	, GT_BET_MAX                  ")
      _sb.AppendLine("           	, GT_DESIGN_XML              	")
      _sb.AppendLine("           	, GT_PROVIDER_ID             	")
      _sb.AppendLine("           	, GT_TABLE_IDLE_PLAYS        	")
      _sb.AppendLine("           	, GT_THEORIC_HOLD           	")
      _sb.AppendLine("           	, GT_BET_MIN_CURR_2           ")
      _sb.AppendLine("           	, GT_BET_MAX_CURR_2           ")
      _sb.AppendLine("            , GT_DROPBOX_ENABLED          ")
      _sb.AppendLine("            , GT_LINKED_CASHIER_ID      ) ")
      _sb.AppendLine("     VALUES									              ")
      _sb.AppendLine("           (  @pCode						          ")
      _sb.AppendLine("           	, @pTypeId						        ")
      _sb.AppendLine("           	, @pName					            ")
      _sb.AppendLine("           	, @pDescription					      ")
      _sb.AppendLine("          	, @pAreaId						        ")
      _sb.AppendLine("           	, @pBankId						        ")
      _sb.AppendLine("          	, @pFloorId						        ")
      _sb.AppendLine("          	, @pEnabled						        ")
      _sb.AppendLine("          	, @pHasIntegratedCashier      ")
      _sb.AppendLine("           	, @pCashierId  		            ")
      _sb.AppendLine("           	, @pCreated    		            ")
      _sb.AppendLine("           	, @pGameSpeedNormal           ")
      _sb.AppendLine("           	, @pGameSpeedSlow             ")
      _sb.AppendLine("           	, @pGameSpeedFast             ")
      _sb.AppendLine("           	, @pGameSpeedSelected         ")
      _sb.AppendLine("           	, @pNumSeats  		            ")
      _sb.AppendLine("           	, @pBetMin     		            ")
      _sb.AppendLine("           	, @pBetMax     		            ")
      _sb.AppendLine("           	, @pDesignXml  		            ")
      _sb.AppendLine("           	, @pProviderID   	            ")
      _sb.AppendLine("           	, @pTableIdlePlays            ")
      _sb.AppendLine("           	, @pTheoricHold               ")
      _sb.AppendLine("           	, @pBetMin2     		          ")
      _sb.AppendLine("           	, @pBetMax2     		          ")
      _sb.AppendLine("            , @pDropBoxEnabled            ")
      _sb.AppendLine("            , @pLinkedCashierId         ) ")
      _sb.AppendLine("        SET @pId    = SCOPE_IDENTITY();   ")

      Using _cmd As New SqlCommand(_sb.ToString(), SqlTrans.Connection, SqlTrans)

        _cmd.Parameters.Add("@pCode", SqlDbType.NVarChar).Value = Me.Code
        _cmd.Parameters.Add("@pTypeId", SqlDbType.BigInt).Value = Me.TypeID
        _cmd.Parameters.Add("@pName", SqlDbType.NVarChar).Value = Me.Name
        _cmd.Parameters.Add("@pDescription", SqlDbType.NVarChar).Value = Me.Description
        _cmd.Parameters.Add("@pAreaId", SqlDbType.Int).Value = Me.AreaID
        _cmd.Parameters.Add("@pBankId", SqlDbType.Int).Value = Me.BankID
        _cmd.Parameters.Add("@pFloorId", SqlDbType.NVarChar).Value = Me.FloorID
        _cmd.Parameters.Add("@pEnabled", SqlDbType.Bit).Value = Me.Enabled
        _cmd.Parameters.Add("@pHasIntegratedCashier", SqlDbType.Bit).Value = Me.IntegratedCashier
        _cmd.Parameters.Add("@pCashierId", SqlDbType.Int).Value = Me.CashierID
        _cmd.Parameters.Add("@pCreated", SqlDbType.DateTime).Value = WGDB.Now
        _cmd.Parameters.Add("@pGameSpeedNormal", SqlDbType.BigInt).Value = Me.TableSpeedNormal
        _cmd.Parameters.Add("@pGameSpeedSlow", SqlDbType.BigInt).Value = Me.TableSpeedSlow
        _cmd.Parameters.Add("@pGameSpeedFast", SqlDbType.BigInt).Value = Me.TableSpeedFast
        _cmd.Parameters.Add("@pGameSpeedSelected", SqlDbType.BigInt).Value = GTPlayerTrackingSpeed.Medium
        _cmd.Parameters.Add("@pNumSeats", SqlDbType.Int).Value = Me.TableNumSeats
        _cmd.Parameters.Add("@pBetMin", SqlDbType.Money).Value = Me.TableBetMin
        _cmd.Parameters.Add("@pBetMax", SqlDbType.Money).Value = Me.TableBetMax

        If Me.TableDesignXml <> Nothing Then
          _cmd.Parameters.Add("@pDesignXml", SqlDbType.Xml).Value = Me.TableDesignXml
        Else
          _cmd.Parameters.Add("@pDesignXml", SqlDbType.Xml).Value = DBNull.Value
        End If

        If GamingTableBusinessLogic.IsGamingTablesEnabled() Then
          _cmd.Parameters.Add("@pProviderID", SqlDbType.Int).Value = Me.ProviderID
        Else
          _cmd.Parameters.Add("@pProviderID", SqlDbType.Int).Value = DBNull.Value
        End If

        _cmd.Parameters.Add("@pTableIdlePlays", SqlDbType.Money).Value = Me.TableIdlePlays

        _cmd.Parameters.Add("@pTheoricHold", SqlDbType.Decimal).Value = Me.TheoricHold

        _cmd.Parameters.Add("@pBetMin2", SqlDbType.Money).Value = Me.TableBetMin2
        _cmd.Parameters.Add("@pBetMax2", SqlDbType.Money).Value = Me.TableBetMax2
        _cmd.Parameters.Add("@pDropBoxEnabled", SqlDbType.Bit).Value = Me.DropBoxEnabled

        If Me.LinkedCashierid = -1 Then
          _cmd.Parameters.Add("@pLinkedCashierId", SqlDbType.Int).Value = DBNull.Value
        Else
          _cmd.Parameters.Add("@pLinkedCashierId", SqlDbType.Int).Value = Me.LinkedCashierid
        End If

        _param_id = _cmd.Parameters.Add("@pId", SqlDbType.Int)
        _param_id.Direction = ParameterDirection.Output

        _row_affected = _cmd.ExecuteNonQuery()

        If _row_affected <> 1 Or IsDBNull(_param_id.Value) Then
          Return ENUM_STATUS.STATUS_ERROR
        End If

        Me.GamingTableID = _param_id.Value

        If GamingTableBusinessLogic.IsGamingTablesEnabled() Then
          If Not WSI.Common.GamingTableBusinessLogic.InsertUpdateTableSeats(Me.GamingTableID, Me.TableNumSeats, Me.TableDesignXml, Me.ProviderID, Me.BankID, Me.FloorID, SqlTrans) Then
            Return ENUM_STATUS.STATUS_ERROR
          End If
        End If

        ' Needed update name first and second get the name 
        ' If not integrated Cashier, first update and second get terminal id. 
        ' This protect if name is changed. ReadTerminalId() gets terminalId and update name of terminal in CASHIER_TERMINALS. 
        If Not Me.IntegratedCashier Then
          If UpdateCashierId(SqlTrans) <> ENUM_STATUS.STATUS_OK Then
            Return ENUM_STATUS.STATUS_ERROR
          End If
        End If

        'Apply changes in table chips sets in BD 
        UpdateChipsSets(SqlTrans)

        'Insert BetCurrencies
        Me.Save(m_gaming_table.bet_currencies, m_gaming_table.gaming_table_id, SqlTrans)

        SqlTrans.Commit()

        Using _db_trx As New DB_TRX()
          Dim _type As ClosingStockType
          Dim _sleeps_on_table As Boolean
          Dim _currencies_stocks As DataTable

          _type = Me.m_gaming_table.closing_stock.Type
          _sleeps_on_table = Me.m_gaming_table.closing_stock.SleepsOnTable
          _currencies_stocks = Me.m_gaming_table.closing_stock.CurrenciesStocks.Copy()

          ReadGamingTable(_db_trx.SqlTransaction)

          'Insert templates
          Me.ClosingStock.Type = _type
          Me.ClosingStock.SleepsOnTable = _sleeps_on_table
          Me.ClosingStock.CurrenciesStocks = _currencies_stocks
          ClosingStock.Save(_db_trx.SqlTransaction)

          _db_trx.SqlTransaction.Commit()

        End Using

        Return ENUM_STATUS.STATUS_OK

      End Using

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return ENUM_STATUS.STATUS_ERROR

  End Function ' InsertGamingTable

  '----------------------------------------------------------------------------
  ' PURPOSE : Is data Ok
  '
  ' PARAMS :
  '     - INPUT :
  '         - SqlTrans
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_DEPENDENCIES
  '     - STATUS_DUPLICATE_KEY
  '     - STATUS_NOT_SUPPORTED
  '     - STATUS_NOT_FOUND
  '     - STATUS_ALREADY_EXISTS
  '     - STATUS_ERROR
  '
  ' NOTES :
  '
  Private Function IsDataOk(ByVal SqlTrans As SqlTransaction) As Integer

    Dim _rc As ENUM_STATUS

    ' Check if name is valid. 
    _rc = ExistName(SqlTrans)

    If _rc <> ENUM_STATUS.STATUS_OK Then
      Return _rc
    End If

    ' Check if table type is valid. 
    _rc = ExistTableType(SqlTrans)

    If _rc <> ENUM_STATUS.STATUS_OK Then
      Return _rc
    End If

    ' Only check if disable table. 
    If Not Me.Enabled Then
      ' Check if table have cage pending movements. 
      _rc = ExistCagePendingMovements(SqlTrans)
    End If

    Return _rc

  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE : Update a Gaming Table object into DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - SqlTrans
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_DEPENDENCIES
  '     - STATUS_DUPLICATE_KEY
  '     - STATUS_NOT_SUPPORTED
  '     - STATUS_NOT_FOUND
  '     - STATUS_ALREADY_EXISTS
  '     - STATUS_ERROR
  '
  ' NOTES :

  Private Function UpdateGamingTable(ByVal SqlTrans As SqlTransaction) As Integer

    Dim _sb As StringBuilder
    Dim _rc As ENUM_STATUS
    Dim _area_id As Integer
    Dim _bank_id As Integer
    Dim _floor_id As String
    Dim _integrated_cashier As Boolean
    Dim _cashier_id As Integer
    Dim _num_seats As Integer
    Dim _design_xml As String
    Dim _provider_id As Integer
    Dim _player_tracking_enabled As Boolean
    Dim _session_id As Int64

    _area_id = 0
    _bank_id = 0
    _floor_id = ""
    _integrated_cashier = False
    _cashier_id = 0
    _num_seats = 0
    _design_xml = ""
    _provider_id = 0
    _player_tracking_enabled = False
    _player_tracking_enabled = GamingTableBusinessLogic.IsEnabledGTPlayerTracking()

    Try

      _rc = IsDataOk(SqlTrans)

      If _rc <> ENUM_STATUS.STATUS_OK Then
        Return _rc
      End If

      _sb = New StringBuilder()

      ' UPDATE GAMING_TABLES
      _sb.AppendLine("  UPDATE   GAMING_TABLES							  	                ")
      _sb.AppendLine("     SET   GT_CODE                    = @pCode			      ")
      _sb.AppendLine("         , GT_TYPE_ID                 = @pType			      ")
      _sb.AppendLine("         , GT_NAME                    = @pName			      ")
      _sb.AppendLine("         , GT_DESCRIPTION             = @pDescription	    ")
      _sb.AppendLine("         , GT_AREA_ID                 = @pAreaId			    ")
      _sb.AppendLine("         , GT_BANK_ID                 = @pBankId			    ")
      _sb.AppendLine("         , GT_FLOOR_ID                = @pFloorId			    ")
      _sb.AppendLine("         , GT_ENABLED                 = @pEnabled			    ")
      _sb.AppendLine("         , GT_HAS_INTEGRATED_CASHIER  = @pIntegrated      ")
      _sb.AppendLine("         , GT_CASHIER_ID              = @pCashierId       ")
      _sb.AppendLine("         , GT_TABLE_SPEED_NORMAL      = @pGameSpeedNormal ")
      _sb.AppendLine("         , GT_TABLE_SPEED_SLOW        = @pGameSpeedSlow   ")
      _sb.AppendLine("         , GT_TABLE_SPEED_FAST        = @pGameSpeedFast   ")
      _sb.AppendLine("         , GT_NUM_SEATS               = @pNumSeats        ")
      _sb.AppendLine("         , GT_BET_MIN                 = @pBetMin          ")
      _sb.AppendLine("         , GT_BET_MAX                 = @pBetMax          ")
      _sb.AppendLine("         , GT_DESIGN_XML              =	@pDesignXml       ")
      _sb.AppendLine("         , GT_PROVIDER_ID             = IsNull(@pProviderID, GT_PROVIDER_ID) ")
      _sb.AppendLine("         , GT_TABLE_SPEED_SELECTED    = isNull(GT_TABLE_SPEED_SELECTED, @pTableSpeedSelected)      ")
      _sb.AppendLine("         , GT_TABLE_IDLE_PLAYS        = @pTableIdlePlays  ")
      _sb.AppendLine("         , GT_THEORIC_HOLD            = @pTheoricHold     ")
      _sb.AppendLine("         , GT_BET_MIN_CURR_2          = @pBetMin2         ")
      _sb.AppendLine("         , GT_BET_MAX_CURR_2          = @pBetMax2         ")
      _sb.AppendLine("         , GT_DROPBOX_ENABLED         = @pDropBoxEnabled  ")
      _sb.AppendLine("         , GT_LINKED_CASHIER_ID       = @pLinkedCashierId ")
      _sb.AppendLine(" OUTPUT    DELETED.GT_AREA_ID                             ")
      _sb.AppendLine("         , DELETED.GT_BANK_ID                             ")
      _sb.AppendLine("         , DELETED.GT_FLOOR_ID                            ")
      _sb.AppendLine("         , DELETED.GT_HAS_INTEGRATED_CASHIER              ")
      _sb.AppendLine("         , DELETED.GT_CASHIER_ID                          ")
      _sb.AppendLine("         , DELETED.GT_NUM_SEATS                           ")
      _sb.AppendLine("         , DELETED.GT_DESIGN_XML                          ")
      _sb.AppendLine("         , DELETED.GT_PROVIDER_ID                         ")
      _sb.AppendLine("   WHERE   GT_GAMING_TABLE_ID         = @pId				      ")

      Using _cmd As New SqlCommand(_sb.ToString(), SqlTrans.Connection, SqlTrans)
        _cmd.Parameters.Add("@pCode", SqlDbType.NVarChar).Value = Me.Code
        _cmd.Parameters.Add("@pType", SqlDbType.BigInt).Value = Me.TypeID
        _cmd.Parameters.Add("@pName", SqlDbType.NVarChar).Value = Me.Name
        _cmd.Parameters.Add("@pDescription", SqlDbType.NVarChar).Value = Me.Description
        _cmd.Parameters.Add("@pAreaId", SqlDbType.Int).Value = Me.AreaID
        _cmd.Parameters.Add("@pBankId", SqlDbType.Int).Value = Me.BankID
        _cmd.Parameters.Add("@pFloorId", SqlDbType.NVarChar).Value = Me.FloorID
        _cmd.Parameters.Add("@pEnabled", SqlDbType.Bit).Value = Me.Enabled
        _cmd.Parameters.Add("@pIntegrated", SqlDbType.Bit).Value = Me.IntegratedCashier
        _cmd.Parameters.Add("@pCashierId", SqlDbType.Int).Value = Me.CashierID
        _cmd.Parameters.Add("@pGameSpeedNormal", SqlDbType.BigInt).Value = Me.TableSpeedNormal
        _cmd.Parameters.Add("@pGameSpeedSlow", SqlDbType.BigInt).Value = Me.TableSpeedSlow
        _cmd.Parameters.Add("@pGameSpeedFast", SqlDbType.BigInt).Value = Me.TableSpeedFast
        _cmd.Parameters.Add("@pNumSeats", SqlDbType.Int).Value = Me.TableNumSeats
        _cmd.Parameters.Add("@pBetMin", SqlDbType.Money).Value = Me.TableBetMin
        _cmd.Parameters.Add("@pBetMax", SqlDbType.Money).Value = Me.TableBetMax
        _cmd.Parameters.Add("@pId", SqlDbType.BigInt).Value = Me.GamingTableID
        If Me.TableDesignXml <> Nothing Then
          _cmd.Parameters.Add("@pDesignXml", SqlDbType.Xml).Value = Me.TableDesignXml
        Else
          _cmd.Parameters.Add("@pDesignXml", SqlDbType.Xml).Value = DBNull.Value
        End If

        If GamingTableBusinessLogic.IsGamingTablesEnabled() Then
          _cmd.Parameters.Add("@pProviderID", SqlDbType.Int).Value = Me.ProviderID
        Else
          _cmd.Parameters.Add("@pProviderID", SqlDbType.Int).Value = DBNull.Value
        End If

        _cmd.Parameters.Add("@pTableSpeedSelected", SqlDbType.Int).Value = GTPlayerTrackingSpeed.Medium
        _cmd.Parameters.Add("@pTableIdlePlays", SqlDbType.Int).Value = Me.TableIdlePlays

        _cmd.Parameters.Add("@pTheoricHold", SqlDbType.Decimal).Value = Me.TheoricHold

        _cmd.Parameters.Add("@pBetMin2", SqlDbType.Decimal).Value = Me.TableBetMin2
        _cmd.Parameters.Add("@pBetMax2", SqlDbType.Decimal).Value = Me.TableBetMax2
        _cmd.Parameters.Add("@pDropBoxEnabled", SqlDbType.Bit).Value = Me.DropBoxEnabled

        If Me.LinkedCashierid = -1 Then
          _cmd.Parameters.Add("@pLinkedCashierId", SqlDbType.Int).Value = DBNull.Value
        Else
          _cmd.Parameters.Add("@pLinkedCashierId", SqlDbType.Int).Value = Me.LinkedCashierid
        End If

        Using _reader As SqlDataReader = _cmd.ExecuteReader()
          If Not _reader.Read() Then

            Return ENUM_STATUS.STATUS_NOT_FOUND
          End If

          If Not _reader.IsDBNull(0) Then
            _area_id = _reader.GetInt32(0)
          End If

          If Not _reader.IsDBNull(1) Then
            _bank_id = _reader.GetInt32(1)
          End If

          If Not _reader.IsDBNull(2) Then
            _floor_id = _reader.GetString(2)
          End If

          If Not _reader.IsDBNull(3) Then
            _integrated_cashier = _reader.GetBoolean(3)
          End If

          If Not _reader.IsDBNull(4) Then
            _cashier_id = _reader.GetInt32(4)
          End If

          If Not _reader.IsDBNull(5) Then
            _num_seats = _reader.GetInt32(5)
          End If

          If Not _reader.IsDBNull(6) Then
            _design_xml = _reader.GetString(6)
          End If

          If Not _reader.IsDBNull(7) Then
            _provider_id = _reader.GetInt32(7)
          End If

        End Using
      End Using

      ' Validate if there is the gaming table session opened
      If Cashier.ExistCashierTerminalSessionOpen(Me.CashierID, _session_id, SqlTrans) Then

        If Me.AreaID <> _area_id Or Me.BankID <> _bank_id Or Me.FloorID <> _floor_id Or _
          Me.IntegratedCashier <> _integrated_cashier Or Me.CashierID <> _cashier_id Or _
          _player_tracking_enabled And Me.TableNumSeats <> _num_seats Or _
          _player_tracking_enabled And Me.TableDesignXml <> _design_xml Or _
          _player_tracking_enabled And Me.ProviderID <> _provider_id Then
          Return ENUM_STATUS.STATUS_ERROR
        End If

      End If

      If GamingTableBusinessLogic.IsGamingTablesEnabled() Then
        If Not WSI.Common.GamingTableBusinessLogic.InsertUpdateTableSeats(Me.GamingTableID, Me.TableNumSeats, Me.TableDesignXml, Me.ProviderID, Me.BankID, Me.FloorID, SqlTrans) Then
          Return ENUM_STATUS.STATUS_ERROR
        End If
      End If

      ' Needed update name first and second get the name 
      ' If not integrated Cashier, first update and second get terminal id. 
      ' This protect if name is changed. ReadTerminalId() gets terminalId and update name of terminal in CASHIER_TERMINALS. 
      If Not Me.IntegratedCashier Then
        If UpdateCashierId(SqlTrans) <> ENUM_STATUS.STATUS_OK Then
          Return ENUM_STATUS.STATUS_ERROR
        End If
      End If

      'Apply changes in table chips sets in BD 
      UpdateChipsSets(SqlTrans)

      'Insert templates
      Me.ClosingStock.Type = Me.m_gaming_table.closing_stock.Type
      Me.ClosingStock.SleepsOnTable = Me.m_gaming_table.closing_stock.SleepsOnTable
      Me.ClosingStock.CurrenciesStocks = Me.m_gaming_table.closing_stock.CurrenciesStocks.Copy()

      'Reset database values to prevent duplicates
      ClosingStock.Reset(SqlTrans, Me.m_gaming_table.closing_stock.Type <> ClosingStockType.ROLLING)

      'Save ClosingStock values
      ClosingStock.Save(SqlTrans, Me.m_gaming_table.closing_stock.Type <> ClosingStockType.ROLLING)

      'Save  BetCurrencies
      Me.Save(m_gaming_table.bet_currencies, m_gaming_table.gaming_table_id, SqlTrans)

      SqlTrans.Commit()
      Return ENUM_STATUS.STATUS_OK

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return ENUM_STATUS.STATUS_ERROR

  End Function ' UpdateGamingTable

  '----------------------------------------------------------------------------
  ' PURPOSE : Check if exists Game table name or cashier terminal name.
  '
  ' PARAMS :
  '     - INPUT :
  '         - SqlTrans
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_DEPENDENCIES
  '     - STATUS_DUPLICATE_KEY
  '     - STATUS_ERROR
  '
  ' NOTES :

  Private Function ExistName(ByVal SqlTrans As SqlTransaction) As ENUM_STATUS
    Dim _sb As StringBuilder
    Dim _data As Object

    Try

      ' Check Gaming table names
      _sb = New StringBuilder()

      _sb.AppendLine("  SELECT  GT_GAMING_TABLE_ID	        ")
      _sb.AppendLine("    FROM  GAMING_TABLES               ")
      _sb.AppendLine("   WHERE  GT_NAME = @pName	          ")
      _sb.AppendLine("     AND  GT_GAMING_TABLE_ID != @pId  ")

      Using _cmd As New SqlCommand(_sb.ToString(), SqlTrans.Connection, SqlTrans)
        _cmd.Parameters.Add("@pName", SqlDbType.NVarChar).Value = Me.Name
        _cmd.Parameters.Add("@pId", SqlDbType.Int).Value = Me.GamingTableID

        _data = _cmd.ExecuteScalar()

        If Not _data Is Nothing Then
          Return ENUM_STATUS.STATUS_DUPLICATE_KEY ' Is not really (This diference error between Cashier Terminals and Gaming Tables)
        End If

      End Using

      ' Check Cashier terminals names
      _sb = New StringBuilder()

      _sb.AppendLine("  SELECT  CT_CASHIER_ID	                    ")
      _sb.AppendLine("    FROM  CASHIER_TERMINALS                 ")
      _sb.AppendLine("   WHERE  CT_NAME = @pName	                ")
      _sb.AppendLine("     AND  (   CT_GAMING_TABLE_ID IS NULL    ")
      _sb.AppendLine("           OR CT_GAMING_TABLE_ID != @pId )  ")

      Using _cmd As New SqlCommand(_sb.ToString(), SqlTrans.Connection, SqlTrans)
        _cmd.Parameters.Add("@pName", SqlDbType.NVarChar).Value = Me.Name
        _cmd.Parameters.Add("@pId", SqlDbType.Int).Value = Me.GamingTableID

        _data = _cmd.ExecuteScalar()

        If Not _data Is Nothing Then
          Return ENUM_STATUS.STATUS_DEPENDENCIES
        End If

        Return ENUM_STATUS.STATUS_OK

      End Using

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return ENUM_STATUS.STATUS_ERROR

  End Function ' ExistName

  '----------------------------------------------------------------------------
  ' PURPOSE : Check if exists table type.
  '
  ' PARAMS :
  '     - INPUT :
  '         - SqlTrans
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_NOT_SUPPORTED
  '     - STATUS_ERROR
  '
  ' NOTES :

  Private Function ExistTableType(ByVal SqlTrans As SqlTransaction) As ENUM_STATUS
    Dim _sb As StringBuilder
    Dim _data As Object

    Try

      ' Check Gaming table names
      _sb = New StringBuilder()

      _sb.AppendLine("  SELECT    GTT_GAMING_TABLE_TYPE_ID        ")
      _sb.AppendLine("    FROM    GAMING_TABLES_TYPES             ")
      _sb.AppendLine("   WHERE    GTT_GAMING_TABLE_TYPE_ID = @pId ")
      _sb.AppendLine("     AND    GTT_ENABLED = 1                 ")

      Using _cmd As New SqlCommand(_sb.ToString(), SqlTrans.Connection, SqlTrans)
        _cmd.Parameters.Add("@pId", SqlDbType.Int).Value = Me.TypeID

        _data = _cmd.ExecuteScalar()

        If _data Is Nothing Then
          Return ENUM_STATUS.STATUS_NOT_SUPPORTED ' Is not really (The type now is not valid)
        End If

        Return ENUM_STATUS.STATUS_OK

      End Using

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return ENUM_STATUS.STATUS_ERROR

  End Function ' ExistTableType

  '----------------------------------------------------------------------------
  ' PURPOSE : Check if exists cage pending movements.
  '
  ' PARAMS :
  '     - INPUT :
  '         - SqlTrans
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ALREADY_EXISTS
  '     - STATUS_ERROR
  '
  ' NOTES :
  '
  Public Function ExistCagePendingMovements(ByVal SqlTrans As SqlTransaction) As ENUM_STATUS
    Dim _sb As StringBuilder
    Dim _data As Object

    Try

      If Me.GamingTableID > 0 Then

        ' Check Gaming table names
        _sb = New StringBuilder()

        _sb.AppendLine("  SELECT	CPM_MOVEMENT_ID                                                   ")
        _sb.AppendLine("    FROM	CAGE_MOVEMENTS			                                              ")
        _sb.AppendLine("   INNER	JOIN CAGE_PENDING_MOVEMENTS ON CGM_MOVEMENT_ID = CPM_MOVEMENT_ID  ")
        _sb.AppendLine("   WHERE	CGM_GAMING_TABLE_ID = @pGamingTableId                             ")

        Using _cmd As New SqlCommand(_sb.ToString(), SqlTrans.Connection, SqlTrans)
          _cmd.Parameters.Add("@pGamingTableID", SqlDbType.Int).Value = Me.GamingTableID

          _data = _cmd.ExecuteScalar()

          If Not _data Is Nothing Then
            Return ENUM_STATUS.STATUS_ALREADY_EXISTS ' Is not really (The cashier have pending cage movements)
          End If

        End Using

      End If

      Return ENUM_STATUS.STATUS_OK

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return ENUM_STATUS.STATUS_ERROR

  End Function ' ExistCagePendingMovements

  '----------------------------------------------------------------------------
  ' PURPOSE : Update cashier terminal id in gaming tables 
  '
  ' PARAMS :
  '     - INPUT :
  '         - SqlTrans
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_DEPENDENCIES
  '     - STATUS_ERROR
  '
  ' NOTES :

  Private Function UpdateCashierId(ByVal SqlTrans As SqlTransaction) As ENUM_STATUS
    Dim _cashier_id As Int32
    Dim _sb As StringBuilder

    ' Get TerminalId and Update Cashier Terminal Name 
    _cashier_id = Cashier.ReadTerminalId(SqlTrans, Me.Name, GU_USER_TYPE.SYS_GAMING_TABLE, False)

    ' If _cashier_id is 0, a cashier terminal already exists with then name Me.Name.
    If _cashier_id = 0 Then
      Return ENUM_STATUS.STATUS_DEPENDENCIES
    End If

    _sb = New StringBuilder()

    ' UPDATE GT_CASHIER_ID IN GAMING_TABLES
    _sb.AppendLine("  UPDATE   GAMING_TABLES							  	  ")
    _sb.AppendLine("     SET   GT_CASHIER_ID      = @pCashierId ")
    _sb.AppendLine("   WHERE   GT_GAMING_TABLE_ID = @pId				")

    Using _cmd As New SqlCommand(_sb.ToString(), SqlTrans.Connection, SqlTrans)
      _cmd.Parameters.Add("@pCashierId", SqlDbType.Int).Value = _cashier_id
      _cmd.Parameters.Add("@pId", SqlDbType.BigInt).Value = Me.GamingTableID

      If _cmd.ExecuteNonQuery() = 1 Then
        Return ENUM_STATUS.STATUS_OK
      End If

    End Using

    Return ENUM_STATUS.STATUS_ERROR

  End Function ' UpdateCashierId

  Public Sub ResetIdValues()

    Me.GamingTableID = 0
    Me.Code = String.Empty
    Me.Name = String.Empty
    Me.FloorID = String.Empty
    Me.IntegratedCashier = False
    Me.CashierID = 0

  End Sub

  ' PURPOSE: Audits the class
  '
  '    - INPUT:
  '       - TableNumber: Number of the tabpage that is being audited
  '       - Auditor: Auditor object.
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '    - 
  Public Sub AuditGamingTableDesign(ByRef Auditor As CLASS_AUDITOR_DATA)

    Dim _xml As System.Xml.XmlDocument
    Dim _xml_nodes As System.Xml.XmlNodeList
    Dim _xml_node As System.Xml.XmlNode
    Dim _xml_seat As System.Xml.XmlNode
    Dim _max_seats As Integer
    Dim _position_seat As Integer
    Dim _empty_field As String
    Dim _property As KeyValuePair(Of String, String)

    _max_seats = GeneralParam.GetInt32("GamingTables", "Seats.MaxAllowedSeats", 10)
    _xml = New System.Xml.XmlDocument
    _position_seat = 0
    _empty_field = "---"

    'Call Auditor.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(4997), "")
    'Call Auditor.SetField(0, Me.NumSeats, Me.GamingTableName & " - " + GLB_NLS_GUI_PLAYER_TRACKING.GetString(4994))

    If Me.TableDesignXml Is Nothing Or String.IsNullOrEmpty(Me.TableDesignXml) Then
      For Each _property In UtilsDesign.GetSerializableProperties(GetType(GamingTableProperties))
        Call Auditor.SetField(0, _empty_field, Me.Name & " - " & _property.Value)
      Next
      For _idx As Integer = 0 To _max_seats - 1
        _position_seat = _position_seat + 1
        For Each _property In UtilsDesign.GetSerializableProperties(GetType(GamingSeatProperties))
          Call Auditor.SetField(0, _empty_field, Me.Name & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4998) & _position_seat & "." & _property.Value)
        Next
      Next
    Else
      _xml.LoadXml(Me.TableDesignXml)
      ' Audit table desing
      _xml_nodes = _xml.GetElementsByTagName("GamingTableProperties")

      If _xml_nodes.Count > 0 Then
        For Each _xml_node In _xml_nodes(0).ChildNodes
          Call Auditor.SetField(0, _xml_node.InnerText, Me.Name & " - " & UtilsDesign.GetPropertyResource(_xml_node.Name))
        Next
      Else
        For Each _property In UtilsDesign.GetSerializableProperties(GetType(GamingTableProperties))
          Call Auditor.SetField(0, _empty_field, Me.Name & " - " & _property.Value)
        Next
      End If

      ' Audit seats
      _xml_nodes = _xml.GetElementsByTagName("GamingSeatProperties")

      For Each _xml_seat In _xml_nodes
        _position_seat = _position_seat + 1
        For Each _xml_node In _xml_seat.ChildNodes
          Call Auditor.SetField(0, _xml_node.InnerText, Me.Name & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4998) & _position_seat & "." & UtilsDesign.GetPropertyResource(_xml_node.Name))
        Next
      Next

      For _idx As Integer = 0 To _max_seats - _xml_nodes.Count - 1
        _position_seat = _position_seat + 1
        For Each _property In UtilsDesign.GetSerializableProperties(GetType(GamingSeatProperties))
          Call Auditor.SetField(0, _empty_field, Me.Name & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4998) & _position_seat & "." & _property.Value)
        Next
      Next

    End If
  End Sub ' AuditGamingTableDesing

  '----------------------------------------------------------------------------
  ' PURPOSE : Read chips allowed in gaming tables 
  '
  ' PARAMS :
  '     - INPUT :
  '         - SqlTrans
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_DEPENDENCIES
  '     - STATUS_ERROR
  '
  ' NOTES :
  Private Function ReadChipsSets(ByVal SqlTrans As SqlTransaction) As ENUM_STATUS
    Dim _sb As StringBuilder
    Dim _dt As DataTable
    Dim _chips_types As List(Of String)

    Try

      _sb = New StringBuilder()
      _chips_types = New List(Of String)()

      _sb.AppendLine("     SELECT   GTCS_GAMING_TABLE_ID     ")
      _sb.AppendLine("            , GTCS_CHIPS_SET_ID        ")
      _sb.AppendLine("       FROM   GAMING_TABLE_CHIPS_SETS  ")
      _sb.AppendLine(" INNER JOIN   CHIPS_SETS ON GTCS_CHIPS_SET_ID = CHS_CHIP_SET_ID  ")
      _sb.AppendLine("   WHERE   GTCS_GAMING_TABLE_ID = " & Me.GamingTableID)

      If Not FeatureChips.IsChipsTypeEnabledGP(FeatureChips.ChipType.RE) And _
        Not FeatureChips.IsChipsTypeEnabledGP(FeatureChips.ChipType.NR) And _
        Not FeatureChips.IsChipsTypeEnabledGP(FeatureChips.ChipType.COLOR) Then
        _chips_types.Add(FeatureChips.ChipType.RE)
        _chips_types.Add(FeatureChips.ChipType.NR)
        _chips_types.Add(FeatureChips.ChipType.COLOR)
        _sb.AppendLine("   AND CHS_CHIP_TYPE NOT IN ( " & String.Join(",", _chips_types.ToArray()) & ")")
      Else
        If FeatureChips.IsChipsTypeEnabledGP(FeatureChips.ChipType.RE) Then
          _chips_types.Add(FeatureChips.ChipType.RE)
        End If

        If FeatureChips.IsChipsTypeEnabledGP(FeatureChips.ChipType.NR) Then
          _chips_types.Add(FeatureChips.ChipType.NR)
        End If

        If FeatureChips.IsChipsTypeEnabledGP(FeatureChips.ChipType.COLOR) Then
          _chips_types.Add(FeatureChips.ChipType.COLOR)
        End If
        _sb.AppendLine("   AND CHS_CHIP_TYPE IN ( " & String.Join(",", _chips_types.ToArray()) & ")")
      End If

      _dt = GUI_GetTableUsingSQL(_sb.ToString(), 500, , SqlTrans)

      If IsNothing(_dt) Then
        Return ENUM_STATUS.STATUS_ERROR
      End If

      Me.m_gaming_table.chips_allowed = _dt

      Return ENUM_STATUS.STATUS_OK

    Catch ex As Exception
      Return ENUM_STATUS.STATUS_ERROR
    End Try

  End Function ' ReadChipsSets

  '----------------------------------------------------------------------------
  ' PURPOSE : Read master chips table for audit object
  '
  ' PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - datatble (chip_id, chip_name)
  '
  ' NOTES :
  Private Function ReadChipsIds() As DataTable
    Dim _sb As StringBuilder
    Dim _table As DataTable

    Try

      _sb = New StringBuilder()

      'Table for audit elements allowed / unallowed
      _sb.AppendLine("SELECT   CHS_CHIP_SET_ID      ")
      _sb.AppendLine("       , CHS_NAME             ")
      _sb.AppendLine("       , CHS_ISO_CODE         ")
      _sb.AppendLine("  FROM   CHIPS_SETS           ")
      _sb.AppendLine(" WHERE   CHS_ALLOWED = 1      ")
      _sb.AppendLine(" ORDER   BY CHS_CHIP_SET_ID   ")

      _table = GUI_GetTableUsingSQL(_sb.ToString(), 500)

      If IsNothing(_table) Then
        Return Nothing
      Else
        Return _table
      End If

    Catch ex As Exception
      Return Nothing
    End Try

  End Function ' ReadChipsIds

  '----------------------------------------------------------------------------
  ' PURPOSE : Update chips allowed in gaming tables 
  '
  ' PARAMS :
  '     - INPUT :
  '         - SqlTrans
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - Num rows updated (only insert or deleted) in database
  '
  ' NOTES :
  Private Function UpdateChipsSets(ByVal SqlTrans As SqlTransaction) As Integer
    Dim _sb_insert As StringBuilder
    Dim _sb_delete As StringBuilder
    Dim _num_rows As Integer
    Dim _parameter As SqlParameter

    _sb_insert = New StringBuilder()
    _sb_delete = New StringBuilder()
    _num_rows = 0

    _sb_insert.AppendLine("INSERT INTO   GAMING_TABLE_CHIPS_SETS  ")
    _sb_insert.AppendLine("            ( GTCS_GAMING_TABLE_ID ")
    _sb_insert.AppendLine("            , GTCS_CHIPS_SET_ID  ")
    _sb_insert.AppendLine("            ) ")
    _sb_insert.AppendLine("     VALUES             ")
    _sb_insert.AppendLine("            ( @pTableId ")
    _sb_insert.AppendLine("            , @pSetsId  ")
    _sb_insert.AppendLine("            ) ")

    _sb_delete.AppendLine("DELETE FROM GAMING_TABLE_CHIPS_SETS  ")
    _sb_delete.AppendLine("       WHERE GTCS_GAMING_TABLE_ID  = @pTableId ")
    _sb_delete.AppendLine("        AND GTCS_CHIPS_SET_ID  = @pSetsId  ")

    Using _cmd_insert As SqlCommand = New SqlCommand(_sb_insert.ToString(), SqlTrans.Connection, SqlTrans)
      Using _cmd_delete As SqlCommand = New SqlCommand(_sb_delete.ToString(), SqlTrans.Connection, SqlTrans)
        _cmd_insert.Parameters.Add("@pTableId", SqlDbType.BigInt).Value = Me.GamingTableID
        _cmd_insert.Parameters.Add("@pSetsId", SqlDbType.BigInt).SourceColumn = "GTCS_CHIPS_SET_ID"

        _parameter = _cmd_delete.Parameters.Add("@pTableId", SqlDbType.BigInt)
        _parameter.SourceColumn = "GTCS_GAMING_TABLE_ID"
        _parameter.SourceVersion = DataRowVersion.Original

        _parameter = _cmd_delete.Parameters.Add("@pSetsId", SqlDbType.BigInt)
        _parameter.SourceColumn = "GTCS_CHIPS_SET_ID"
        _parameter.SourceVersion = DataRowVersion.Original

        Using _da As SqlDataAdapter = New SqlDataAdapter()
          _da.InsertCommand = _cmd_insert
          _da.DeleteCommand = _cmd_delete
          _da.UpdateCommand = Nothing
          _da.SelectCommand = Nothing
          _num_rows = _da.Update(Me.m_gaming_table.chips_allowed)
        End Using
      End Using
    End Using

    Return _num_rows

  End Function ' UpdateChipsSets


  ''' <summary>
  ''' Save all bet by currency
  ''' </summary>
  ''' <param name="dt_currencies_from_form">datatable with form bet values</param>
  ''' <param name="GamingTableId">Id of gaming table</param>
  ''' <param name="Trx">Transaction to commit the changes</param>
  ''' <returns>true if save / false has an error</returns>
  ''' <remarks></remarks>
  Private Function Save(ByRef dt_currencies_from_form As DataTable, ByVal GamingTableId As Integer, ByVal Trx As SqlTransaction) As Boolean

    Dim _rc As ENUM_STATUS
    Dim _is_update As Boolean
    Dim _type_from_database As TYPE_BET_CURRENCIES
    Dim _type_from_form As TYPE_BET_CURRENCIES

    m_dt_bet_by_currency = ReadCurrenciesBet(GamingTableId)

    If m_dt_bet_by_currency.Rows.Count > 0 Then

      For Each actually_item As DataRow In dt_currencies_from_form.Rows
        _type_from_form = New TYPE_BET_CURRENCIES()
        _type_from_form.iso_code = actually_item(0)
        _type_from_form.max_bet = actually_item(1)
        _type_from_form.min_bet = actually_item(2)
        _type_from_form.gamingTableId = GamingTableId
        _type_from_form.status = 0
        _is_update = False

        For Each item As DataRow In m_dt_bet_by_currency.Rows
          _type_from_database = New TYPE_BET_CURRENCIES()
          _type_from_database.iso_code = item(0)
          _type_from_database.max_bet = item(1)
          _type_from_database.min_bet = item(2)
          _type_from_database.gamingTableId = GamingTableId
          _type_from_database.status = 0

          If (_type_from_form.iso_code = _type_from_database.iso_code) Then
            _is_update = True
            Exit For
          End If
        Next

        _rc = Me.Save(_type_from_form, Trx, _is_update)
      Next
    Else
      _is_update = False

      For Each item As DataRow In Me.BetByCurrencies.Rows
        _type_from_database = New TYPE_BET_CURRENCIES()
        _type_from_database.iso_code = item(0)
        _type_from_database.max_bet = item(1)
        _type_from_database.min_bet = item(2)
        _type_from_database.gamingTableId = GamingTableId
        _type_from_database.status = 0

        _rc = Me.Save(_type_from_database, Trx, _is_update)
      Next
    End If

    Return _rc
    Return True
  End Function

  ''' <summary>
  ''' Override function to save all changes in bet form
  ''' </summary>
  ''' <param name="gamingTablesBet"></param>
  ''' <param name="Trx"></param>
  ''' <param name="isUpdate"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function Save(ByRef gamingTablesBet As TYPE_BET_CURRENCIES, ByVal Trx As SqlTransaction, ByVal isUpdate As Boolean) As Boolean
    Dim _sb As StringBuilder

    _sb = New StringBuilder()
    If (isUpdate) Then
      _sb.AppendLine("     UPDATE  GAMING_TABLE_BET    ")
      _sb.AppendLine("     SET      GTB_MAX_BET             = @pMaxBet  ")
      _sb.AppendLine("	          , GTB_MIN_BET             = @pMinBet  ")
      _sb.AppendLine("	          , GTB_STATUS              = @pStatus  ")
      _sb.AppendLine("	  WHERE     GTB_ISO_CODE            = @pIsoCode ")
      _sb.AppendLine("	    AND     GTB_GAMING_TABLE_ID     = @pGamingtableId ")
    Else
      _sb.AppendLine(" INSERT INTO  GAMING_TABLE_BET    ")
      _sb.AppendLine("            ( GTB_MAX_BET      ")
      _sb.AppendLine("            , GTB_MIN_BET         ")
      _sb.AppendLine("            , GTB_ISO_CODE                  ")
      _sb.AppendLine("            , GTB_GAMING_TABLE_ID           ")
      _sb.AppendLine("            , GTB_STATUS                    ")
      _sb.AppendLine("            )                               ")
      _sb.AppendLine("     VALUES ( @pMaxBet                      ")
      _sb.AppendLine("	          , @pMinBet                      ")
      _sb.AppendLine("	          , @pIsoCode                     ")
      _sb.AppendLine("	          , @pGamingtableId               ")
      _sb.AppendLine("	          , @pStatus                      ")
      _sb.AppendLine("	          );                              ")
    End If

    Try

      Using _sql_cmd = New SqlCommand(_sb.ToString(), Trx.Connection, Trx)
        _sql_cmd.Parameters.Add("@pMaxBet", SqlDbType.Decimal).Value = gamingTablesBet.max_bet
        _sql_cmd.Parameters.Add("@pMinBet", SqlDbType.Decimal).Value = gamingTablesBet.min_bet
        _sql_cmd.Parameters.Add("@pIsoCode", SqlDbType.NVarChar).Value = gamingTablesBet.iso_code
        _sql_cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = 0
        _sql_cmd.Parameters.Add("@pGamingtableId", SqlDbType.Int).Value = gamingTablesBet.gamingTableId

        If _sql_cmd.ExecuteNonQuery() <> 1 Then
          Return ENUM_STATUS.STATUS_ERROR
        End If

      End Using


    Catch ex As Exception
      Return False
    End Try


  End Function

  ''' <summary>
  ''' Function that return CurrenciesBet in DataBase
  ''' </summary>
  ''' <param name="gamingTableId"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function ReadCurrenciesBet(ByVal gamingTableId As Integer) As DataTable
    Dim _sb As StringBuilder
    Dim _table As DataTable
    Dim _currenciesAccepted As String


    _currenciesAccepted = GeneralParam.GetString("RegionalOptions", "CurrenciesAccepted").Replace(";", "','")

    Try

      _sb = New StringBuilder()

      'Table for audit elements allowed / unallowed
      _sb.AppendLine(" SELECT  GTB_ISO_CODE")
      _sb.AppendLine("        ,GTB_MIN_BET")
      _sb.AppendLine("        ,GTB_MAX_BET")
      _sb.AppendLine(" FROM    GAMING_TABLE_BET")
      _sb.Append("    WHERE   GTB_ISO_CODE IN ('")
      _sb.Append(_currenciesAccepted)
      _sb.AppendLine("')")
      _sb.AppendLine("  AND    GTB_ISO_CODE <> 'X01'")
      _sb.AppendLine("  AND    GTB_GAMING_TABLE_ID =")
      _sb.Append(gamingTableId)

      _table = GUI_GetTableUsingSQL(_sb.ToString(), 500)

      If IsNothing(_table) Then
        Return Nothing
      Else
        Return _table
      End If

    Catch ex As Exception
      Return Nothing
    End Try

  End Function ' ReadCurrenciesBet

  ''' <summary>
  ''' Function to get the name of the cashier
  ''' </summary>
  ''' <param name="cashierId"></param>
  ''' <param name="Trx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function ReadCashierName(ByVal cashierId As Integer, ByVal Trx As SqlTransaction) As String
    Dim _sb As StringBuilder
    Dim _name As String

    Try

      _sb = New StringBuilder()


      _sb.AppendLine("SELECT CT_NAME          ")
      _sb.AppendLine(" FROM  CASHIER_TERMINALS")
      _sb.AppendLine("WHERE  CT_CASHIER_ID =  @pCashierId")


      Using _sql_cmd = New SqlCommand(_sb.ToString(), Trx.Connection, Trx)

        _sql_cmd.Parameters.Add("@pCashierId", SqlDbType.Int).Value = cashierId



        _name = _sql_cmd.ExecuteScalar()

        Return _name
      End Using


    Catch ex As Exception
      Return String.Empty
    End Try

  End Function

#End Region ' Private Functions

End Class