'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_flag.vb
' DESCRIPTION:   Flag class for flag edition
' AUTHOR:        Xavier Cots
' CREATION DATE: 27-AUG-2012
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 27-AUG-2012  XCD    Initial version.
' 08-OCT-2012  ANG    Refactor ReadFlag using SqlParametersm,SqlCommand and Reader
'-------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Text
Imports WSI.Common
Imports System.Data.SqlClient


Public Class CLASS_FLAG
  Inherits CLASS_BASE

#Region " Constants "

  ' Fields length
  Public Const MAX_NAME_LEN As Integer = 20 ' In DB nvarchar(50)
  Public Const MAX_DESC_LEN As Integer = 240 ' In DB nvarchar(250)

  Public Const DATE_NULL As String = "1900/01/01 00:00:00"
  Public Const AUDIT_NONE_STRING As String = "---"

  ' Private Const
  Private Const MAX_SQL_ROWS As Integer = 5000

#End Region

#Region " GUI_Configuration.dll "

#Region " Structures "
  <StructLayout(LayoutKind.Sequential)> _
   Public Class TYPE_FLAG
    Public control_block As Integer
    Public flag_id As Int64
    Public flag_type As AccountFlag.FlagTypes
    Public status As Integer
    Public expiration_type As Integer
    Public expiration_date As New TYPE_DATE_TIME
    Public expiration_interval As Integer
    Public flag_created_date As New TYPE_DATE_TIME
    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_NAME_LEN + 1)> _
    Public name As String
    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_DESC_LEN + 1)> _
    Public description As String
    Public color As Integer
    Public related_id As Int64
    Public related_type As AccountFlag.RelatedTypes

    Public Sub New()

      Me.related_id = -1
      Me.related_type = AccountFlag.RelatedTypes.NONE
    End Sub

  End Class

#End Region

#Region " Enums "

  Public Enum STATUS
    ENABLED = 0
    DISABLED = 1
  End Enum

#End Region

#End Region

#Region " Members "

  Protected m_flag As New TYPE_FLAG

#End Region

#Region " Properties "

  Public Property FlagId() As Int64
    Get
      Return m_flag.flag_id
    End Get
    Set(ByVal Value As Int64)
      m_flag.flag_id = Value
    End Set
  End Property

  Public Property FlagType() As AccountFlag.FlagTypes
    Get
      Return m_flag.flag_type
    End Get
    Set(ByVal Value As AccountFlag.FlagTypes)
      m_flag.flag_type = Value
    End Set
  End Property

  Public Property FlagStatus() As Integer
    Get
      Return m_flag.status
    End Get
    Set(ByVal Value As Integer)
      m_flag.status = Value
    End Set
  End Property

  Public Property ExpirationType() As Integer
    Get
      Return m_flag.expiration_type
    End Get
    Set(ByVal Value As Integer)
      m_flag.expiration_type = Value
    End Set
  End Property

  Public Property ExpirationDate() As Date
    Get
      If m_flag.expiration_date.IsNull() Then
        m_flag.expiration_date.Value = DATE_NULL
      End If
      Return m_flag.expiration_date.Value
    End Get
    Set(ByVal Value As Date)
      If IsNothing(m_flag.expiration_date) Then
        m_flag.expiration_date = New TYPE_DATE_TIME
      End If
      m_flag.expiration_date.Value = Value
    End Set
  End Property

  Public Property ExpirationInterval() As Integer
    Get
      Return m_flag.expiration_interval
    End Get
    Set(ByVal Value As Integer)
      m_flag.expiration_interval = Value
    End Set
  End Property

  Public Property FlagCreated() As Date
    Get
      If m_flag.flag_created_date.IsNull Then
        Return GUI_GetDate()
      Else
        Return m_flag.flag_created_date.Value
      End If
    End Get
    Set(ByVal Value As Date)
      If IsNothing(m_flag.flag_created_date) Then
        m_flag.flag_created_date = New TYPE_DATE_TIME
      End If
      m_flag.flag_created_date.Value = Value
    End Set
  End Property

  Public Property FlagName() As String
    Get
      Return m_flag.name
    End Get
    Set(ByVal Value As String)
      m_flag.name = Value
    End Set
  End Property

  Public Property FlagDescription() As String
    Get
      Return m_flag.description
    End Get
    Set(ByVal Value As String)
      m_flag.description = Value
    End Set
  End Property

  Public Property FlagColor() As Integer
    Get
      Return m_flag.color
    End Get
    Set(ByVal Value As Integer)
      m_flag.color = Value
    End Set
  End Property

  Public Property RelatedId() As Int64
    Get
      Return m_flag.related_id
    End Get
    Set(ByVal Value As Int64)
      m_flag.related_id = Value
    End Set
  End Property

  Public Property RelatedType() As AccountFlag.RelatedTypes
    Get
      Return m_flag.related_type
    End Get
    Set(ByVal Value As AccountFlag.RelatedTypes)
      m_flag.related_type = Value
    End Set
  End Property

#End Region

#Region " Overrides functions "

  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As Integer

    Me.FlagId = ObjectId

    m_flag.control_block = Marshal.SizeOf(m_flag)

    ' Read flag data
    _rc = ReadFlag(m_flag, SqlCtx)

    Select Case _rc

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Read

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As Integer

    m_flag.control_block = Marshal.SizeOf(m_flag)

    ' Update flag data
    _rc = UpdateFlag(m_flag, SqlCtx)

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_ALREADY_EXISTS
        Return CLASS_BASE.ENUM_STATUS.STATUS_ALREADY_EXISTS

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Update

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As Integer

    m_flag.control_block = Marshal.SizeOf(m_flag)

    ' Insert user data
    _rc = InsertFlag()

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_ALREADY_EXISTS
        Return CLASS_BASE.ENUM_STATUS.STATUS_ALREADY_EXISTS

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Insert

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As Integer

    ' Delete user data
    _rc = DeleteFlag(Me.FlagId, SqlCtx)

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DEPENDENCIES
        Return ENUM_STATUS.STATUS_DEPENDENCIES

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_CONTEXT
        Return ENUM_STATUS.STATUS_NOT_SUPPORTED

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Delete

  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _string_exp_interval As String

    ' Create a new auditor_code for flag manipulation
    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_PLAYER_PROMOTIONS)
    Call _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(1286), IIf(String.IsNullOrEmpty(Me.FlagName), AUDIT_NONE_STRING, Me.FlagName))

    ' Flag Name
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1286), IIf(String.IsNullOrEmpty(Me.FlagName), AUDIT_NONE_STRING, Me.FlagName))
    ' Status
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(262), IIf(Me.FlagStatus = STATUS.ENABLED, _
                                                                         GLB_NLS_GUI_PLAYER_TRACKING.GetString(260), _
                                                                         GLB_NLS_GUI_PLAYER_TRACKING.GetString(248)))

    ' Description
    Call _auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(312), IIf(String.IsNullOrEmpty(Me.FlagDescription), AUDIT_NONE_STRING, Me.FlagDescription))

    ' Color
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1291), Hex(Me.FlagColor))

    ' Flag expiration
    Select Case (Me.ExpirationType)
      Case FLAG_EXPIRATION_TYPE.NEVER
        _string_exp_interval = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1287)
      Case FLAG_EXPIRATION_TYPE.MINUTES
        _string_exp_interval = Me.ExpirationInterval & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1351)
      Case FLAG_EXPIRATION_TYPE.HOURS
        _string_exp_interval = Me.ExpirationInterval & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(259)
      Case FLAG_EXPIRATION_TYPE.DAYS
        _string_exp_interval = Me.ExpirationInterval & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(258)
      Case FLAG_EXPIRATION_TYPE.MONTH
        _string_exp_interval = Me.ExpirationInterval & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1289)
      Case FLAG_EXPIRATION_TYPE.DATETIME
        _string_exp_interval = GUI_FormatDate(Me.ExpirationDate, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
      Case FLAG_EXPIRATION_TYPE.UNKNOWN
        _string_exp_interval = Me.ExpirationInterval & " " & AUDIT_NONE_STRING
      Case Else
        _string_exp_interval = AUDIT_NONE_STRING
    End Select

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1288), _string_exp_interval)

    Return _auditor_data

  End Function ' AuditorData

  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim _temp_flag As CLASS_FLAG

    _temp_flag = New CLASS_FLAG
    _temp_flag.m_flag.control_block = Me.m_flag.control_block
    _temp_flag.m_flag.flag_id = Me.m_flag.flag_id
    _temp_flag.m_flag.flag_type = Me.m_flag.flag_type
    _temp_flag.m_flag.status = Me.m_flag.status
    _temp_flag.m_flag.expiration_type = Me.m_flag.expiration_type
    If Me.m_flag.expiration_date.IsNull Then
      _temp_flag.m_flag.expiration_date.IsNull = True
    Else
      _temp_flag.m_flag.expiration_date.Value = Me.m_flag.expiration_date.Value
    End If

    _temp_flag.m_flag.expiration_interval = Me.m_flag.expiration_interval
    _temp_flag.m_flag.flag_created_date.Value = Me.m_flag.flag_created_date.Value
    _temp_flag.m_flag.name = Me.m_flag.name
    _temp_flag.m_flag.description = Me.m_flag.description
    _temp_flag.m_flag.color = Me.m_flag.color

    Return _temp_flag

  End Function ' Duplicate

#End Region

#Region " Private Functions "

  Public Function ReadFlag(ByVal pFlag As TYPE_FLAG, _
                            ByVal Context As Integer) As Integer

    Dim _str_bld As StringBuilder
    Dim _do_commit As Boolean
    Dim _sql_trx As System.Data.SqlClient.SqlTransaction

    _sql_trx = Nothing
    _do_commit = False

    Try

      If Not GUI_BeginSQLTransaction(_sql_trx) Then

        _do_commit = False
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_DB_NOT_CONNECT

      End If

      _str_bld = New StringBuilder()

      _str_bld.AppendLine("SELECT   FL_FLAG_ID                                                              ")
      _str_bld.AppendLine("       , FL_TYPE                                                                 ")
      _str_bld.AppendLine("       , FL_STATUS                                                               ")
      _str_bld.AppendLine("       , FL_EXPIRATION_TYPE                                                      ")
      _str_bld.AppendLine("       , ISNULL (FL_EXPIRATION_DATE,@pDateNull) AS FL_EXPIRATION_DATE            ")
      _str_bld.AppendLine("       , FL_EXPIRATION_INTERVAL                                                  ")
      _str_bld.AppendLine("       , FL_CREATED                                                              ")
      _str_bld.AppendLine("       , FL_NAME                                                                 ")
      _str_bld.AppendLine("       , ISNULL (FL_DESCRIPTION, ' ') AS FL_DESCRIPTION                          ")
      _str_bld.AppendLine("       , FL_COLOR                                                                ")
      _str_bld.AppendLine("  FROM   FLAGS                                                                   ")
      _str_bld.AppendLine(" WHERE   FL_FLAG_ID = @pFlagId                                                   ")


      Using _sql_cmd As New SqlCommand(_str_bld.ToString(), _sql_trx.Connection, _sql_trx)

        _sql_cmd.Parameters.Add("@pFlagId", SqlDbType.Int).Value = pFlag.flag_id
        _sql_cmd.Parameters.Add("@pDateNull", SqlDbType.DateTime).Value = DATE_NULL

        Using _reader As SqlDataReader = _sql_cmd.ExecuteReader()

          If (Not _reader.Read) Then

            _do_commit = False
            Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND

          End If

          '' FILL FLAG OBJECT
          With pFlag
            .flag_id = _reader("FL_FLAG_ID")
            .flag_type = _reader("FL_TYPE")
            .status = _reader("FL_STATUS")
            .expiration_type = _reader("FL_EXPIRATION_TYPE")
            .expiration_date.Value = _reader("FL_EXPIRATION_DATE")
            .expiration_interval = _reader("FL_EXPIRATION_INTERVAL")
            .flag_created_date.Value = _reader("FL_CREATED")
            .name = _reader("FL_NAME")
            .description = _reader("FL_DESCRIPTION")
            .color = _reader("FL_COLOR")
          End With

        End Using

      End Using

      _do_commit = True
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK


    Catch ex As Exception

      Log.Error(ex.Message)
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

    Finally

      GUI_EndSQLTransaction(_sql_trx, _do_commit)

    End Try

  End Function ' ReadFlag

  Private Function UpdateFlag(ByVal pFlag As TYPE_FLAG, _
                              ByVal Context As Integer) As Integer

    Dim _sql_trx As System.Data.SqlClient.SqlTransaction = Nothing
    Dim _sb As StringBuilder
    Dim _nls_param1 As String
    Dim _nls_param2 As String

    If Not GUI_BeginSQLTransaction(_sql_trx) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    Try
      _sb = New StringBuilder()
      _sb.AppendLine(" SELECT  FL_NAME FROM FLAGS WHERE  FL_FLAG_ID = @fId ")

      Using _cmd As New SqlClient.SqlCommand(_sb.ToString(), _sql_trx.Connection, _sql_trx)

        _cmd.Parameters.Add("@fId", SqlDbType.BigInt).Value = pFlag.flag_id

        If pFlag.name.Equals(_cmd.ExecuteScalar().ToString()) Then
          ' Flag name don't changed.
          _cmd.Parameters.Add("@fName", SqlDbType.NVarChar).Value = pFlag.name
        Else
          ' Flag name changed. Check if there are flags with same name
          _sb.Length = 0 'Reset stringBuilder
          _sb.AppendLine(" SELECT  COUNT(*) FROM  FLAGS WHERE  FL_NAME = @fName AND FL_FLAG_ID != @fId ")

          ' Assign query parameters
          _cmd.Parameters.Add("@fName", SqlDbType.NVarChar).Value = pFlag.name

          ' Assign current command text
          _cmd.CommandText = _sb.ToString()

          ' Check if flag name already exists
          If (_cmd.ExecuteScalar() > 0) Then
            _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1286) ' Bandera
            _nls_param2 = pFlag.name
            ' Ask user if want to continue inserting flag
            If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1352), ENUM_MB_TYPE.MB_TYPE_WARNING, _
                          ENUM_MB_BTN.MB_BTN_YES_NO, , _nls_param1, _nls_param2) _
                        = ENUM_MB_RESULT.MB_RESULT_NO Then
              ' Return to edit flag
              GUI_EndSQLTransaction(_sql_trx, False)
              Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_ALREADY_EXISTS
            End If
          End If
        End If

        ' Insert to DB
        _sb.Length = 0 'Reset stringBuilder

        _sb.AppendLine(" UPDATE FLAGS                                         ")
        _sb.AppendLine(" SET   FL_TYPE = @fType                               ")
        _sb.AppendLine("     , FL_STATUS = @fStatus                           ")
        _sb.AppendLine("     , FL_EXPIRATION_TYPE = @fExpirationType          ")
        _sb.AppendLine("     , FL_EXPIRATION_DATE = @fExpirationDate          ")
        _sb.AppendLine("     , FL_EXPIRATION_INTERVAL =  @fExpirationInterval ")
        _sb.AppendLine("     , FL_NAME = @fName                               ")
        _sb.AppendLine("     , FL_DESCRIPTION = @fDescription                 ")
        _sb.AppendLine("     , FL_COLOR = @fColor                             ")
        _sb.AppendLine(" WHERE FL_FLAG_ID = @fId                              ")

        ' Assign current command text
        _cmd.CommandText = _sb.ToString()

        ' Assign query parameters - @fName an @fId had been asigned before
        _cmd.Parameters.Add("@fType", SqlDbType.Int).Value = 0
        _cmd.Parameters.Add("@fStatus", SqlDbType.Int).Value = pFlag.status
        _cmd.Parameters.Add("@fExpirationType", SqlDbType.Int).Value = pFlag.expiration_type
        _cmd.Parameters.Add("@fExpirationDate", SqlDbType.DateTime).Value = IIf(pFlag.expiration_date.Value = DATE_NULL, DBNull.Value, pFlag.expiration_date.Value())
        _cmd.Parameters.Add("@fExpirationInterval", SqlDbType.Int).Value = pFlag.expiration_interval
        _cmd.Parameters.Add("@fDescription", SqlDbType.NText).Value = pFlag.description
        _cmd.Parameters.Add("@fColor", SqlDbType.Int).Value = pFlag.color

        If _cmd.ExecuteNonQuery() <> 1 Then
          GUI_EndSQLTransaction(_sql_trx, False)

          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

        If Not GUI_EndSQLTransaction(_sql_trx, True) Then
          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

      End Using

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

    Catch ex As Exception
      If Not GUI_EndSQLTransaction(_sql_trx, False) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

  End Function ' UpdateFlag

  Private Function DeleteFlag(ByVal FlagId As Int64, _
                              ByVal Context As Integer) As Integer
    'Dim _sql_query As String
    Dim _db_count As Integer = 0
    Dim _str_bld As StringBuilder
    Dim _sql_tx As System.Data.SqlClient.SqlTransaction = Nothing
    Dim _data_table As DataTable

    _str_bld = New StringBuilder()
    '     - Remove the FLAG item from the FLAGS table
    Try
      If Not GUI_BeginSQLTransaction(_sql_tx) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      ' Before removing, check dependencies with other tables

      ' Table account flags
      _str_bld.AppendLine("SELECT   COUNT(*)      ")
      _str_bld.AppendLine("  FROM   ACCOUNT_FLAGS ")
      _str_bld.AppendLine(" WHERE   AF_FLAG_ID = " & FlagId.ToString())

      _data_table = GUI_GetTableUsingSQL(_str_bld.ToString(), 5000)

      If IsNothing(_data_table) Then
        GUI_EndSQLTransaction(_sql_tx, False)
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      _db_count = _data_table.Rows(0).Item(0)

      If _db_count > 0 Then
        GUI_EndSQLTransaction(_sql_tx, False)
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DEPENDENCIES
      End If

      ' Table promotion flags
      _str_bld.Length = 0
      _str_bld.AppendLine("SELECT   COUNT(*)      ")
      _str_bld.AppendLine("  FROM   PROMOTION_FLAGS ")
      _str_bld.AppendLine(" WHERE   PF_FLAG_ID = " & FlagId.ToString())

      _data_table = GUI_GetTableUsingSQL(_str_bld.ToString(), 5000)

      If IsNothing(_data_table) Then
        GUI_EndSQLTransaction(_sql_tx, False)
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      _db_count = _data_table.Rows(0).Item(0)

      If _db_count > 0 Then
        GUI_EndSQLTransaction(_sql_tx, False)
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_CONTEXT
      End If

      ' Delete flag
      _str_bld.Length = 0
      _str_bld.AppendLine("DELETE FLAGS")
      _str_bld.AppendLine("WHERE FL_FLAG_ID = " & FlagId.ToString())

      If Not GUI_SQLExecuteNonQuery(_str_bld.ToString(), _sql_tx) Then
        ' Rollback  
        GUI_EndSQLTransaction(_sql_tx, False)
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      ' Commit
      If Not GUI_EndSQLTransaction(_sql_tx, True) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

    Catch ex As Exception
      ' Rollback  
      GUI_EndSQLTransaction(_sql_tx, False)

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
  End Function ' DeleteFlag

#End Region

#Region "Shared Functions"

  ''' <summary>
  ''' Get all flags
  ''' </summary>
  ''' <param name="SqlTrx"></param>
  ''' <param name="Flags"></param>
  ''' <param name="bShowAutomaticFlags"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function GetAllFlags(ByVal SqlTrx As SqlClient.SqlTransaction, ByRef Flags As DataTable, Optional ByVal bShowAutomaticFlags As Boolean = False) As Boolean

    Dim _str_bld As StringBuilder

    _str_bld = New StringBuilder()
    Flags = New DataTable()

    Try

      _str_bld.AppendLine("SELECT   FL_FLAG_ID                                 ")
      _str_bld.AppendLine("	      , FL_NAME                                    ")
      _str_bld.AppendLine("	      , FL_COLOR                                   ")
      _str_bld.AppendLine("	      , FL_EXPIRATION_TYPE                         ")
      _str_bld.AppendLine("	      , FL_EXPIRATION_DATE                         ")
      _str_bld.AppendLine("	      , FL_STATUS                                  ")
      _str_bld.AppendLine("	      , FL_TYPE                                    ")
      _str_bld.AppendLine("	      , FL_RELATED_ID                              ")
      _str_bld.AppendLine("	      , FL_RELATED_TYPE                            ")
      _str_bld.AppendLine("  FROM   FLAGS                                      ")
      _str_bld.AppendLine(" WHERE   1 = 1                                      ")

      If Not bShowAutomaticFlags Then
        _str_bld.AppendLine("   AND   FL_TYPE = @pType                           ")
      End If

      _str_bld.AppendLine(" ORDER BY FL_TYPE ASC, FL_NAME ASC, FL_FLAG_ID ASC ")

      Using _sql_cmd As New SqlCommand(_str_bld.ToString(), SqlTrx.Connection, SqlTrx)

        _sql_cmd.Parameters.Add("@pStatusActive", SqlDbType.Int).Value = ACCOUNT_FLAG_STATUS.ACTIVE
        _sql_cmd.Parameters.Add("@pExpirationType", SqlDbType.Int).Value = FLAG_EXPIRATION_TYPE.DATETIME

        If Not bShowAutomaticFlags Then
          _sql_cmd.Parameters.Add("@pType", SqlDbType.Int).Value = AccountFlag.FlagTypes.Manual
        End If

        Using _sql_da As New SqlDataAdapter(_sql_cmd)
          _sql_da.Fill(Flags)
        End Using
      End Using

    Catch ex As Exception
      Log.Error(ex.Message)
      Return False
    End Try

    Return True

  End Function ' GetAllFlags

  ''' <summary>
  ''' Get all flags
  ''' </summary>
  ''' <param name="bShowAutomaticFlags"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function GetAllFlags(Optional ByVal bShowAutomaticFlags As Boolean = False) As DataTable

    Dim _dt_flags As DataTable
    Dim _sql_trx As System.Data.SqlClient.SqlTransaction = Nothing

    Try

      _dt_flags = New DataTable()

      If Not GUI_BeginSQLTransaction(_sql_trx) Then
        Return Nothing
      End If

      GetAllFlags(_sql_trx, _dt_flags, bShowAutomaticFlags)

      If Not GUI_EndSQLTransaction(_sql_trx, True) Then
        Return Nothing
      End If

      Return _dt_flags

    Catch ex As Exception
      Log.Error(ex.Message)
      GUI_EndSQLTransaction(_sql_trx, False)
    End Try

    Return Nothing

  End Function ' GetAllFlags

#End Region

#Region " Public Method "

  Public Function InsertFlag(Trx As SqlTransaction, Optional MostrarMensajeDuplicada As Boolean = True) As Integer

    Dim _sql_trx As System.Data.SqlClient.SqlTransaction = Nothing
    Dim _sb As StringBuilder
    Dim nls_param1 As String
    Dim nls_param2 As String
    Dim _param_id_flag As SqlParameter

    Try

      _sb = New StringBuilder()

      _sb.AppendLine("SELECT   COUNT(*)         ")
      _sb.AppendLine("  FROM   FLAGS            ")
      _sb.AppendLine(" WHERE   FL_NAME = @fName ")

      Using _cmd As New SqlClient.SqlCommand(_sb.ToString(), Trx.Connection, Trx)

        ' Assign query parameters
        _cmd.Parameters.Add("@fName", SqlDbType.NVarChar).Value = m_flag.name

        If MostrarMensajeDuplicada Then
          ' Check if flag name already exists
          If (_cmd.ExecuteScalar() > 0) Then

            nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1286) ' Bandera
            nls_param2 = m_flag.name

            ' Ask user if want to continue inserting flag
            If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1352), ENUM_MB_TYPE.MB_TYPE_WARNING, _
                          ENUM_MB_BTN.MB_BTN_YES_NO, , nls_param1, nls_param2) _
                        = ENUM_MB_RESULT.MB_RESULT_NO Then
              ' Return to edit flag
              GUI_EndSQLTransaction(_sql_trx, False)
              Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_ALREADY_EXISTS

            End If
          End If

        End If

        ' Insert to DB
        _sb.Length = 0 'Reset stringBuilder

        _sb.AppendLine(" INSERT INTO   FLAGS                       ")
        _sb.AppendLine("             ( FL_TYPE                     ")
        _sb.AppendLine("             , FL_STATUS                   ")
        _sb.AppendLine("             , FL_EXPIRATION_TYPE          ")
        _sb.AppendLine("             , FL_EXPIRATION_DATE          ")
        _sb.AppendLine("             , FL_EXPIRATION_INTERVAL      ")
        _sb.AppendLine("             , FL_CREATED                  ")
        _sb.AppendLine("             , FL_NAME                     ")
        _sb.AppendLine("             , FL_DESCRIPTION              ")
        _sb.AppendLine("             , FL_COLOR                    ")
        _sb.AppendLine("             , FL_RELATED_ID               ")
        _sb.AppendLine("             , FL_RELATED_TYPE )           ")
        _sb.AppendLine("      VALUES                               ")
        _sb.AppendLine("             ( @fType                      ")
        _sb.AppendLine("             , @fStatus                    ")
        _sb.AppendLine("             , @fExpirationType            ")
        _sb.AppendLine("             , @fExpirationDate            ")
        _sb.AppendLine("             , @fExpirationInterval        ")
        _sb.AppendLine("             , @fCreated                   ")
        _sb.AppendLine("             , @fName                      ")
        _sb.AppendLine("             , @fDescription               ")
        _sb.AppendLine("             , @fColor                     ")
        _sb.AppendLine("             , @fRelatedId                 ")
        _sb.AppendLine("             , @fRelatedType )             ")
        _sb.AppendLine(" SET @pID = SCOPE_IDENTITY();              ")

        ' Assign current command text
        _cmd.CommandText = _sb.ToString

        ' Assign query parameters - @fName had been asigned before
        _cmd.Parameters.Add("@fType", SqlDbType.Int).Value = m_flag.flag_type
        _cmd.Parameters.Add("@fStatus", SqlDbType.Int).Value = m_flag.status
        _cmd.Parameters.Add("@fExpirationType", SqlDbType.Int).Value = m_flag.expiration_type
        _cmd.Parameters.Add("@fExpirationDate", SqlDbType.DateTime).Value = IIf(m_flag.expiration_date.Value = DATE_NULL, DBNull.Value, m_flag.expiration_date.Value)
        _cmd.Parameters.Add("@fExpirationInterval", SqlDbType.Int).Value = m_flag.expiration_interval
        _cmd.Parameters.Add("@fCreated", SqlDbType.DateTime).Value = m_flag.flag_created_date.Value()
        _cmd.Parameters.Add("@fDescription", SqlDbType.NVarChar).Value = m_flag.description
        _cmd.Parameters.Add("@fColor", SqlDbType.Int).Value = m_flag.color
        _cmd.Parameters.Add("@fRelatedId", SqlDbType.Int).Value = IIf(m_flag.related_id = -1, DBNull.Value, m_flag.related_id)
        _cmd.Parameters.Add("@fRelatedType", SqlDbType.Int).Value = IIf(m_flag.related_type = AccountFlag.RelatedTypes.NONE, DBNull.Value, m_flag.related_type)

        _param_id_flag = _cmd.Parameters.Add("@pID", SqlDbType.BigInt)
        _param_id_flag.Direction = ParameterDirection.Output

        If _cmd.ExecuteNonQuery() <> 1 Then
          GUI_EndSQLTransaction(_sql_trx, False)

          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

        m_flag.flag_id = _param_id_flag.Value

      End Using

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

    Catch ex As Exception
      If Not GUI_EndSQLTransaction(_sql_trx, False) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

  End Function ' InsertFlag

  ''' <summary>
  ''' Insert a new flag
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function InsertFlag() As Integer

    Dim _sql_trx As System.Data.SqlClient.SqlTransaction = Nothing

    Try

      If Not GUI_BeginSQLTransaction(_sql_trx) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      If InsertFlag(_sql_trx) <> ENUM_CONFIGURATION_RC.CONFIGURATION_OK Then

        If Not GUI_EndSQLTransaction(_sql_trx, False) Then
          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

      Else

        If Not GUI_EndSQLTransaction(_sql_trx, True) Then
          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

      End If

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

    Catch ex As Exception
      If Not GUI_EndSQLTransaction(_sql_trx, False) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

  End Function ' InsertFlag

#End Region

End Class
