
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports WSI.Common.CollectionImport
Imports WSI.Common

Public Class CLASS_COLLECTION_FILE_IMPORT
  Inherits CLASS_BASE

#Region "Members"
  Dim m_soft_count As ImportedSoftCounts
  Dim m_file1_name As String
  Dim m_file2_name As String
  Dim m_name As String
  Dim m_working_day As DateTime
  Dim m_xml_soft_count As String
  Dim m_soft_count_id As Int64
#End Region

#Region "Properties"

  Public Property SoftCounts() As ImportedSoftCounts
    Get
      Return m_soft_count
    End Get
    Set(ByVal value As ImportedSoftCounts)
      m_soft_count = value
    End Set
  End Property

  Public Property File1Name() As String
    Get
      Return m_file1_name
    End Get
    Set(ByVal value As String)
      m_file1_name = value
    End Set
  End Property

  Public Property File2Name() As String
    Get
      Return m_file2_name
    End Get
    Set(ByVal value As String)
      m_file2_name = value
    End Set
  End Property

  Public Property Name() As String
    Get
      Return m_name
    End Get
    Set(ByVal value As String)
      m_name = value
    End Set
  End Property

  Public Property WorkingDay() As DateTime
    Get
      Return m_working_day
    End Get
    Set(ByVal value As DateTime)
      m_working_day = value
    End Set
  End Property

  Public Property XmlSoftCount() As String
    Get
      Return m_xml_soft_count
    End Get
    Set(ByVal value As String)
      m_xml_soft_count = value
    End Set
  End Property

  Public Property SoftCountID() As String
    Get
      Return m_soft_count_id
    End Get
    Set(ByVal value As String)
      m_soft_count_id = value
    End Set
  End Property
#End Region


  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA
    Dim _auditor_data As CLASS_AUDITOR_DATA

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_CAGE)

    ' Audit data
    _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(6489), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2617))

    '     - jornaly import
    _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6298), GUI_FormatDate(m_working_day, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT))

    '     - import file bills
    _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6295), IIf(String.IsNullOrEmpty(m_file1_name), AUDIT_NONE_STRING, m_file1_name))

    '     - import file tickets
    _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6296), IIf(String.IsNullOrEmpty(m_file2_name), AUDIT_NONE_STRING, m_file2_name))

    '     - name import
    _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6297), IIf(String.IsNullOrEmpty(m_name), AUDIT_NONE_STRING, m_name))

    
    Return _auditor_data

  End Function


  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

  End Function


  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Using _db_trx As New DB_TRX()

      m_soft_count.m_working_day = m_working_day
      m_soft_count.m_name = m_name
      m_soft_count.m_file1_name = System.IO.Path.GetFileName(m_file1_name)
      m_soft_count.m_file2_name = System.IO.Path.GetFileName(m_file2_name)

      If Not m_soft_count.SaveImported(_db_trx.SqlTransaction, XmlSoftCount) Then

        Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
      End If

      SoftCountID = m_soft_count.m_imported_soft_count_id
      _db_trx.Commit()

      Return ENUM_STATUS.STATUS_OK
    End Using

  End Function

  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

  End Function

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

  End Function

  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE
    Dim _temp As CLASS_COLLECTION_FILE_IMPORT

    _temp = New CLASS_COLLECTION_FILE_IMPORT()

    _temp.m_file1_name = m_file1_name
    _temp.m_file2_name = m_file2_name
    _temp.m_name = m_name
    _temp.m_working_day = m_working_day

    Return _temp
  End Function

  Public Sub New()
    Me.SoftCounts = New ImportedSoftCounts()
  End Sub
End Class
