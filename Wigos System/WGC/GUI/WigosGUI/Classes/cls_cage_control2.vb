Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports GUI_CommonOperations
Imports GUI_Controls
Imports GUI_CommonMisc
Imports WSI.Common
Imports System.Text
Imports Wigos.GUI.Adapter.Sessions.Commands
Imports Wigos.Cage.Proxy.Sessions

Partial Public Class CLASS_CAGE_CONTROL

#Region "Members"

  Private m_cage_session_id As Long
  Private m_cage_session_status As CASHIER_SESSION_STATUS
  Private m_open_datetime As DateTime
  Private m_close_datetime As DateTime
  Public m_total_imbalance As DataTable
  Private m_chips_circulate As Decimal
  Private m_Loaded_Stock As DataTable
  Private m_IsStockError As Boolean

#End Region ' Members

#Region "Properties"

  ''' <summary>
  ''' Circulate chips
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>

  Public Property ChipsCirculate() As Decimal
    Get
      Return m_chips_circulate
    End Get
    Set(ByVal value As Decimal)
      m_chips_circulate = value
    End Set
  End Property


  Public Property CageSessionId() As Long
    Get
      Return m_cage_session_id
    End Get
    Set(ByVal Value As Long)
      m_cage_session_id = Value
    End Set
  End Property

  Public ReadOnly Property CageSessionStatus() As CASHIER_SESSION_STATUS
    Get
      Return m_cage_session_status
    End Get
  End Property

  Public Property OpenDateTime() As DateTime
    Get
      Return m_open_datetime
    End Get
    Set(ByVal Value As DateTime)
      m_open_datetime = Value
    End Set
  End Property

  Public Property CloseDateTime() As DateTime
    Get
      Return m_close_datetime
    End Get
    Set(ByVal Value As DateTime)
      m_close_datetime = Value
    End Set
  End Property

  Public Property LoadedStock() As DataTable
    Get
      Return m_Loaded_Stock
    End Get
    Set(ByVal Value As DataTable)
      m_Loaded_Stock = Value
    End Set
  End Property

  Public Property IsStockError() As Boolean
    Get
      Return m_IsStockError
    End Get
    Set(ByVal value As Boolean)
      m_IsStockError = value
    End Set
  End Property

#End Region ' Properties 

#Region "Private"

  ' PURPOSE: CountCage
  '
  ' PARAMS:
  '
  ' RETURNS:
  '     - db result
  '
  ' NOTES:
  '
  Private Function CountCage() As Integer
    Dim _result As ENUM_CONFIGURATION_RC
    Dim _commit_done As Boolean

    _result = ENUM_CONFIGURATION_RC.CONFIGURATION_OK
    _commit_done = False

    Try

      Using _db_trx As New DB_TRX()

        ' Inserts movement
        If _result = ENUM_CONFIGURATION_RC.CONFIGURATION_OK AndAlso Not InsertMovement(_db_trx.SqlTransaction) Then
          _result = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

        ' Inserts movement details
        If _result = ENUM_CONFIGURATION_RC.CONFIGURATION_OK AndAlso Not InsertMovementDetails(_db_trx.SqlTransaction, Me.TableCurrencies) Then
          If (Me.IsStockError) Then
            _result = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_STOCK_CHANGED
          Else
            _result = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
          End If

        End If
        Me.RelatedMovementId = Me.MovementID

        ' create quadrature movements
        If _result = ENUM_CONFIGURATION_RC.CONFIGURATION_OK AndAlso Not QuadratureMovements(_db_trx.SqlTransaction) Then
          _result = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

        ' updates Global Stock
        If _result = ENUM_CONFIGURATION_RC.CONFIGURATION_OK AndAlso Not ReSetsGlobalStock(_db_trx.SqlTransaction, Me.TableCurrencies) Then
          _result = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

        If _result = ENUM_CONFIGURATION_RC.CONFIGURATION_OK Then
          Call _db_trx.Commit()
          _commit_done = True
        Else
          Call _db_trx.Rollback()
          _commit_done = False
        End If

      End Using

      If _commit_done Then
        'NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4354), ENUM_MB_TYPE.MB_TYPE_INFO)
      Else
        If Not IsStockError Then
          NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4355), ENUM_MB_TYPE.MB_TYPE_WARNING)
        End If
      End If

    Catch ex As Exception
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4355), ENUM_MB_TYPE.MB_TYPE_WARNING)
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "cls_cage_control2", _
                            "CountCage", _
                            ex.Message)

      _result = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

    Return _result

  End Function ' CountCage

  ' PARAMS:
  '   - INPUT:  sql transaction
  '
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - True if everything went OK. False if there was an error
  '
  ' NOTES:
  '
  Public Function OpenCageSessionDB(ByVal SessionName As String, ByVal WorkingDay As Date, ByVal SqlTrx As SqlTransaction) As Boolean

    Dim openSessionCommand As OpenSessionCommand
    Dim sessionAdapter As OpenSessionCommandHandler
    Dim result As Wigos.Core.Entity.Result
    Dim sessionProxy As SessionProxy

    sessionProxy = cls_global_params.Instance.GUIContainer.Resolve(GetType(SessionProxy), "SessionProxy") ' TODO: Try to resolve using DI
    sessionAdapter = New OpenSessionCommandHandler(sessionProxy)
    openSessionCommand = New OpenSessionCommand(SessionName, WorkingDay)
    result = sessionAdapter.Execute(openSessionCommand)

    Me.OpenDateTime = WGDB.Now()

    If (result.Status = Wigos.Core.Entity.Result.StatusResult.Failure) Then
      Log.Error("OpenCageSessionDb: Could not open cage session.")
      Return False
    Else
      Return True
    End If

  End Function ' OpenCageSessionDB

  ' PURPOSE: Updates movement to Cancel status
  '
  ' PARAMS:
  '   - INPUT:  sql transaction
  '
  ' RETURNS:
  '     - _result Boolean
  '
  ' NOTES
  '
  Public Function CloseCageSessionDB(ByVal SqlTrx As SqlTransaction) As Boolean
    Dim _sb As StringBuilder
    Dim _result As Boolean

    _result = True
    Try
      _sb = New StringBuilder

      _sb.AppendLine("UPDATE   CAGE_SESSIONS ")
      _sb.AppendLine("   SET   CGS_CLOSE_DATETIME  = @pCloseDatetime ")
      _sb.AppendLine("       , CGS_CLOSE_USER_ID   = @pCloseUserId ")
      _sb.AppendLine(" WHERE   CGS_CAGE_SESSION_ID = @pSessionId ")

      Using _cmd As New SqlClient.SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx)

        Me.CloseDateTime = WSI.Common.WGDB.Now
        _cmd.Parameters.Add("@pCloseDatetime", SqlDbType.DateTime).Value = Me.CloseDateTime
        _cmd.Parameters.Add("@pCloseUserId", SqlDbType.Int).Value = CurrentUser.Id
        _cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = Me.CageSessionId

        If _cmd.ExecuteNonQuery() <> 1 Then
          _result = False
        Else
          Me.OperationType = Cage.CageOperationType.CloseCage
          Me.Status = Cage.CageStatus.FinishedOpenCloseCage
          If Not InsertMovement(SqlTrx) Then
            _result = False
          End If
        End If
      End Using

      Return _result

    Catch ex As Exception
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "cls_cage_control2", _
                            "CloseCageSessionDB", _
                            ex.Message)
      Return False
    End Try

  End Function ' CloseCage

  ' PURPOSE: Updates movement to Cancel status
  '
  ' PARAMS:
  '   - INPUT:  sql transaction
  '
  ' RETURNS:
  '     - _result Boolean
  '
  ' NOTES
  '
  Public Function CloseCageSession(ByVal SqlTrx As SqlTransaction) As Boolean
    Dim _chips_circulate As Decimal
    Dim _chips_circulate_account_balance As Decimal
    Dim _result_meters As Decimal
    Dim _result_session_meters As Decimal
    Dim _national_currency As String

    ' If Cage Meters is enabled, then calculate chips circulating
    If Cage.IsCageEnabled() Then
      If Not GetChipsCirculateFromSession(_chips_circulate) Then

        Return False
      End If

      Me.ChipsCirculate = _chips_circulate

      _national_currency = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode")

      ' TODO JML 22-Abr-2016: add Cage currency type
      If Not CageMeters.UpdateCageMeters(_chips_circulate_account_balance _
                                        , 0 _
                                        , Me.CageSessionId _
                                        , CageMeters.CageSystemSourceTarget.System _
                                        , CageMeters.CageConceptId.ChipsCirculating _
                                        , _national_currency _
                                        , 0 _
                                        , CageMeters.UpdateCageMetersOperationType.Increment _
                                        , CageMeters.UpdateCageMetersgetGetSessionMode.LastOpened _
                                        , _result_meters, _result_session_meters, SqlTrx) Then

        Return False
      End If
    End If

    ' Close Session
    ' RRR 28-OCT-2014
    If Not CloseCageSessionDB(SqlTrx) Then
      Return False
    Else
      If Not CageMeters.CageSessionClose_SaveStock(Me.CageSessionId, SqlTrx) Then
        Return False
      End If
    End If

    Return True

  End Function ' CloseCageSession

  ' PURPOSE: Delte zeros from currencies datatable 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub DeleteZerosFromDT()
    Dim _row As DataRow

    For Each _row In Me.TableCurrencies.Rows
      Call CheckForZero(_row)
    Next
  End Sub ' DeleteZerosFromDT

  ' PURPOSE: creates movements with negative and positive values for quadrature
  '
  ' PARAMS:
  '   - INPUT:  
  '       - SqlTrx: SqlTransaction
  '
  ' RETURNS:
  '       - _result: Boolean
  '
  ' NOTES:
  '
  Private Function QuadratureMovements(ByVal SqlTrx As SqlTransaction) As Boolean
    Dim _table_lost As DataTable
    Dim _table_gain As DataTable
    Dim _result As Boolean

    _result = True

    Try
      _table_lost = Me.TableCurrencies.Copy
      _table_gain = Me.TableCurrencies.Copy

      Call KeepOnlyPositivesNegatives(_table_gain, _table_lost)
      If Not InsertQuadratureMovements(_table_gain, _table_lost, SqlTrx) Then
        _result = False
      End If

      Return _result

    Catch _ex As Exception
      Call Trace.WriteLine(_ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "cls_cage_control2", _
                            "QuadratureMovements", _
                            _ex.Message)
      Return False
    End Try

  End Function ' QuadratureMovements

  ' PURPOSE: modify tables to only keep positive and negative difference values
  '
  ' PARAMS:
  '   - INPUT:  
  '     - TableGain:  DataTable
  '     - TableLost:  DataTable
  '
  ' RETURNS:
  '
  ' NOTES:
  '
  Private Sub KeepOnlyPositivesNegatives(ByRef TableGain As DataTable, ByRef TableLost As DataTable)
    Dim _row As DataRow

    For Each _row In TableGain.Rows
      If _row("QUANTITY_DIFFERENCE") IsNot DBNull.Value AndAlso _row("TOTAL_DIFFERENCE") IsNot DBNull.Value _
      AndAlso _row("QUANTITY") IsNot DBNull.Value Then
        _row("QUANTITY") = IIf(_row("QUANTITY_DIFFERENCE") > 0, _row("QUANTITY_DIFFERENCE"), DBNull.Value)
        _row("TOTAL") = IIf(_row("TOTAL_DIFFERENCE") > 0, _row("TOTAL_DIFFERENCE"), DBNull.Value)
      End If
    Next

    For Each _row In TableLost.Rows
      If _row("QUANTITY_DIFFERENCE") IsNot DBNull.Value AndAlso _row("TOTAL_DIFFERENCE") IsNot DBNull.Value _
      AndAlso _row("QUANTITY") IsNot DBNull.Value Then
        _row("QUANTITY") = IIf(_row("QUANTITY_DIFFERENCE") < 0, -(_row("QUANTITY_DIFFERENCE")), DBNull.Value)
        _row("TOTAL") = IIf(_row("TOTAL_DIFFERENCE") < 0, -(_row("TOTAL_DIFFERENCE")), DBNull.Value)
      End If
    Next

  End Sub ' KeepOnlyPositivesNegatives

  ' PURPOSE: Insert quadrature movements when closing cage session
  '
  ' PARAMS:
  '   - INPUT:  
  '       - TableGain:  DataTable
  '       - TableLost:  DataTable
  '
  ' RETURNS:
  '     - true if ok
  '
  ' NOTES:
  '
  Private Function InsertQuadratureMovements(ByRef TableGain As DataTable, ByRef TableLost As DataTable, ByVal SqlTrx As SqlTransaction) As Boolean
    Dim _aux_operation_type As Cage.CageOperationType
    Dim _result As Boolean

    _result = True

    Try
      _aux_operation_type = Me.OperationType
      Me.OperationType = Cage.CageOperationType.GainQuadrature
      If Not InsertMovement(SqlTrx) Then
        _result = False
      End If
      If _result AndAlso Not InsertMovementDetails(SqlTrx, TableGain) Then
        _result = False
      End If
      Me.OperationType = Cage.CageOperationType.LostQuadrature
      If _result AndAlso Not InsertMovement(SqlTrx) Then
        _result = False
      End If
      If _result AndAlso Not InsertMovementDetails(SqlTrx, TableLost) Then
        _result = False
      End If
      Me.OperationType = _aux_operation_type

    Catch _ex As Exception
      Call Trace.WriteLine(_ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "cls_cage_control2", _
                            "InsertQuadratureMovements", _
                            _ex.Message)
      _result = False
    End Try

    Return _result

  End Function ' InsertQuadratureMovements

  ' PURPOSE: gets values from quadrature movement (count & close)
  '
  ' PARAMS:
  '
  ' RETURNS:
  '     - true if ok
  '
  ' NOTES:
  Private Function GetCloseCageDetail() As Boolean
    Dim _sb As StringBuilder
    Dim _table As DataTable
    Dim _result As Boolean

    _sb = New StringBuilder
    _table = New DataTable
    _result = True

    Try
      _sb.AppendLine("     SELECT   CGM_TYPE ")
      _sb.AppendLine("            , CASE WHEN CMD_ISO_CODE = '" & Cage.CHIPS_ISO_CODE & "' THEN '" & CurrencyExchange.GetNationalCurrency & "' ELSE CMD_ISO_CODE END AS CMD_ISO_CODE ")
      _sb.AppendLine("            , CMD_DENOMINATION ")
      _sb.AppendLine("            , CMD_QUANTITY ")
      _sb.AppendLine("            , CASE WHEN CMD_ISO_CODE = 'X01' THEN 1001 ELSE CASE WHEN CMD_QUANTITY >= 0 THEN ISNULL(CMD_CAGE_CURRENCY_TYPE,0) ELSE 99 END END AS TYPE ")
      _sb.AppendLine("            , ISNULL(CMD_CHIP_ID, 0) AS CMD_CHIP_ID ")
      _sb.AppendLine("            , ISNULL(CSC_SET_ID, 0) AS SET_ID ")
      _sb.AppendLine("       FROM   CAGE_MOVEMENTS ")
      _sb.AppendLine(" INNER JOIN   CAGE_MOVEMENT_DETAILS ON CGM_MOVEMENT_ID = CMD_MOVEMENT_ID  ")
      _sb.AppendLine("  LEFT JOIN   CHIPS_SETS_CHIPS ON CHIPS_SETS_CHIPS.CSC_CHIP_ID = CAGE_MOVEMENT_DETAILS.CMD_CHIP_ID ")
      _sb.AppendLine("      WHERE   CGM_TYPE IN(" & Me.OperationType & ", " & Cage.CageOperationType.GainQuadrature & ", " & Cage.CageOperationType.LostQuadrature & ")  ")
      If Me.OperationType = Cage.CageOperationType.CountCage Then
        _sb.AppendLine("        AND   CGM_MOVEMENT_ID = " & Me.MovementID & " OR CGM_RELATED_MOVEMENT_ID = " & Me.MovementID)
      End If
      _sb.AppendLine("        AND   CGM_CAGE_SESSION_ID = " & Me.CageSessionId)
      _sb.AppendLine("   ORDER BY   CGM_TYPE ")
      _sb.AppendLine("            , CASE WHEN CMD_ISO_CODE = '" & Cage.CHIPS_ISO_CODE & "' THEN '" & CurrencyExchange.GetNationalCurrency & "' ELSE CMD_ISO_CODE END ")
      _sb.AppendLine("            , CASE WHEN CMD_ISO_CODE = 'X01' THEN 1001 ELSE CASE WHEN CMD_QUANTITY >= 0 THEN ISNULL(CMD_CAGE_CURRENCY_TYPE,0) ELSE 99 END END ")
      _sb.AppendLine("            , CMD_DENOMINATION  ASC ")

      _table = GUI_GetTableUsingSQL(_sb.ToString, Integer.MaxValue)

      If _table Is Nothing OrElse _table.Rows.Count = 0 Then
        _result = False
      Else
        Call Me.InitCurrencyData()
        Call SetCloseQuadratureValues(_table)
      End If

      Return _result

    Catch ex As Exception
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "cls_cage_control2", _
                            "GetCloseCageDetail", _
                            ex.Message)
      Return False
    End Try

  End Function ' GetQuadratureValues

  ' PURPOSE: sets table currencies with values
  '
  ' PARAMS:
  '
  ' RETURNS:
  '
  ' NOTES:
  Private Sub SetCloseQuadratureValues(ByRef TableQuadrature As DataTable)
    Dim _row As DataRow
    Dim _row_money As DataRow()
    Dim _real_quantity As Decimal
    Dim _screen_quantity As Decimal
    Dim _iso_code As String
    Dim _denomination As Decimal
    Dim _total As Decimal
    Dim _currency_type As CageCurrencyType
    Dim _str_filter As String
    Dim _dataview As DataView
    Dim _show_denominations As Cage.ShowDenominationsMode
    Dim _chip_id As Int64
    Dim _chip_set_id As Int64

    _show_denominations = GeneralParam.GetInt32("Cage", "ShowDenominations", Cage.ShowDenominationsMode.ShowAllDenominations)

    For Each _row In TableQuadrature.Rows
      _real_quantity = _row("CMD_QUANTITY")
      _screen_quantity = _real_quantity
      _denomination = _row("CMD_DENOMINATION")
      _iso_code = _row("CMD_ISO_CODE")
      _currency_type = _row("TYPE")
      _chip_id = _row("CMD_CHIP_ID")
      _chip_set_id = _row("SET_ID")
      _str_filter = String.Format("ISO_CODE = '{0}' AND TYPE = {1} AND CHIP_ID = {2} AND DENOMINATION = ", _iso_code, CInt(_currency_type), _chip_id)

      If _real_quantity < 0 Then
        If _real_quantity <> Cage.TICKETS_CODE Then
          _total = _denomination
          _screen_quantity = 1
        Else
          _total = 0
          _screen_quantity = _denomination
        End If
        _denomination = _real_quantity
      Else
        If _currency_type <> CageCurrencyType.ChipsColor Then
          _total = _real_quantity * _denomination
        Else
          _total = _real_quantity
        End If
      End If

      _row_money = Me.TableCurrencies.Select(_str_filter & GUI_LocalNumberToDBNumber(_denomination.ToString))

      If _row_money.Length = 0 Then

        _row_money = New DataRow() {InitializeNewRow(_iso_code, _denomination, True, _currency_type, _chip_id, _chip_set_id)}
      End If

      If _row_money.Length > 0 Then
        Select Case _row("CGM_TYPE")
          Case Cage.CageOperationType.CloseCage, Cage.CageOperationType.CountCage
            _row_money(0).Item("CASHIER_QUANTITY") = _screen_quantity
            _row_money(0).Item("CASHIER_TOTAL") = _total
            _row_money(0).Item("QUANTITY") = _screen_quantity
            _row_money(0).Item("TOTAL") = _total
            _row_money(0).Item("QUANTITY_DIFFERENCE") = 0
            _row_money(0).Item("TOTAL_DIFFERENCE") = 0

          Case Cage.CageOperationType.LostQuadrature
            _row_money(0).Item("CASHIER_TOTAL") = GetRowValue(_row_money(0).Item("CASHIER_TOTAL")) + _total
            _row_money(0).Item("TOTAL_DIFFERENCE") = GetRowValue(_row_money(0).Item("TOTAL")) - GetRowValue(_row_money(0).Item("CASHIER_TOTAL"))

            If _denomination < 0 AndAlso _denomination <> Cage.TICKETS_CODE Then
              _row_money(0).Item("CASHIER_QUANTITY") = _screen_quantity
              _row_money(0).Item("QUANTITY_DIFFERENCE") = _screen_quantity
            Else
              _row_money(0).Item("CASHIER_QUANTITY") = GetRowValue(_row_money(0).Item("CASHIER_QUANTITY")) + _screen_quantity
              _row_money(0).Item("QUANTITY_DIFFERENCE") = GetRowValue(_row_money(0).Item("QUANTITY")) - GetRowValue(_row_money(0).Item("CASHIER_QUANTITY"))
            End If

          Case Cage.CageOperationType.GainQuadrature
            _row_money(0).Item("CASHIER_TOTAL") = GetRowValue(_row_money(0).Item("CASHIER_TOTAL")) - _total
            _row_money(0).Item("TOTAL_DIFFERENCE") = GetRowValue(_row_money(0).Item("TOTAL")) - GetRowValue(_row_money(0).Item("CASHIER_TOTAL"))

            If _denomination < 0 AndAlso _denomination <> Cage.TICKETS_CODE Then
              _row_money(0).Item("CASHIER_QUANTITY") = IIf(GetRowValue(_row_money(0).Item("TOTAL")) = 0, 0, _screen_quantity)
              _row_money(0).Item("QUANTITY_DIFFERENCE") = IIf(GetRowValue(_row_money(0).Item("TOTAL")) = 0, _screen_quantity, 0)
            Else
              _row_money(0).Item("CASHIER_QUANTITY") = GetRowValue(_row_money(0).Item("CASHIER_QUANTITY")) - _screen_quantity
              _row_money(0).Item("QUANTITY_DIFFERENCE") = GetRowValue(_row_money(0).Item("QUANTITY")) - GetRowValue(_row_money(0).Item("CASHIER_QUANTITY"))
            End If

        End Select

      End If
    Next

    _dataview = Me.TableCurrencies.DefaultView
    _dataview.Sort = "ISO_CODE ASC, TYPE ASC, CHIP_SET_NAME ASC, DENOMINATION DESC, CHIP_ID ASC "
    Me.TableCurrencies = _dataview.ToTable()
    AddExpectedColumnInCurrencies()
    Call DeleteZerosFromDT()

  End Sub ' GetQuadratureValues

  ' PURPOSE: ' Set with the new cage session Id the movements that were Sent in the last Cage Sessions
  '
  ' PARAMS:
  '
  ' RETURNS:
  '     - true if ok
  '
  ' NOTES:
  Private Function SetNewCageSessionIdSentMovements(ByVal SqlTrx As SqlTransaction) As Boolean
    Dim _sb As StringBuilder
    Dim _result As Boolean

    _result = True
    _sb = New StringBuilder

    _sb.AppendLine(" UPDATE   CAGE_MOVEMENTS ")
    _sb.AppendLine("    SET   CGM_CAGE_SESSION_ID = @pCageSessionId ")
    _sb.AppendLine("  WHERE   CGM_CAGE_SESSION_ID = -1 ")
    _sb.AppendLine("    AND   CGM_STATUS = @pStatus ")

    Try

      Using _cmd As New SqlClient.SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx)

        _cmd.Parameters.Add("@pCageSessionId", SqlDbType.BigInt).Value = Me.CageSessionId
        _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = Cage.CageStatus.Sent

        Call _cmd.ExecuteNonQuery()
      End Using

    Catch ex As Exception
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "cls_cage_control2", _
                            "SetNewCageSessionIdSentMovements", _
                            ex.Message)
      Return False
    End Try

    Return _result
  End Function ' SetNewCageSessionIdSentMovements

  ' PURPOSE: ' Creates collection alarms if status is imbalanced
  '
  ' PARAMS:
  '
  ' RETURNS:
  '     - true if ok
  '
  ' NOTES:

  Private Sub CreateCollectionAlarms()

    Using _db_trx As DB_TRX = New DB_TRX()

      Call Me.CreateCollectionAlarms(_db_trx.SqlTransaction)

      Call _db_trx.Commit()

    End Using

  End Sub

  Private Sub CreateCollectionAlarms(ByVal SqlTrx As SqlTransaction)
    Dim _source_name As String
    Dim _description As String
    Dim _row As DataRow

    Try

      _source_name = GLB_CurrentUser.Name & "@" & Environment.MachineName

      If Me.Status = Cage.CageStatus.OkAmountImbalance AndAlso Not Me.m_total_imbalance Is Nothing Then
        For Each _row In Me.m_total_imbalance.Rows
          _description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4721, Me.m_type_new_collection.terminal_name & " " & _row("ISO_CODE") _
            & " " & Me.FormatAmount(_row("TOTAL"), False, _row("ISO_CODE")) _
            , GUI_FormatDate(Me.MovementDatetime, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS))

          Call Alarm.Register(AlarmSourceCode.User, GLB_CurrentUser.Id, _source_name, _
                              AlarmCode.User_DenominationCollectionUnbalanced, _
                              _description, AlarmSeverity.Warning, DateTime.MinValue, SqlTrx)
        Next

      ElseIf Me.Status = Cage.CageStatus.OkDenominationImbalance Then
        _description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4720, GUI_FormatDate(Me.MovementDatetime, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS))

        Call Alarm.Register(AlarmSourceCode.User, GLB_CurrentUser.Id, _source_name, _
                            AlarmCode.User_DenominationCollectionUnbalanced, _
                            _description, AlarmSeverity.Info, DateTime.MinValue, SqlTrx)
      End If

      'tickets
      If Not String.IsNullOrEmpty(m_type_new_collection.alarm_message) Then
        Call Alarm.Register(AlarmSourceCode.User, GLB_CurrentUser.Id, _source_name, _
                            AlarmCode.User_DenominationCollectionUnbalanced, _
                            m_type_new_collection.alarm_message, _
                            AlarmSeverity.Warning, DateTime.MinValue, SqlTrx)
      End If

    Catch _ex As Exception
      Call Trace.WriteLine(_ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "cls_cage_control2", _
                            "CreateCollectionAlarms", _
                            _ex.Message)
    End Try

  End Sub ' CreateCollectionAlarms


  ' PURPOSE: Updates movement to Cancel status
  '
  ' PARAMS:
  '   - INPUT:  sql transaction
  '
  ' RETURNS:
  '     - _result Boolean
  '
  ' NOTES
  '
  Public Function ReOpenCageSessionDB(ByVal SqlTrx As SqlTransaction) As Boolean
    Dim _sb As StringBuilder
    Dim _result As Boolean

    _result = True
    Try
      _sb = New StringBuilder

      _sb.AppendLine("UPDATE   CAGE_SESSIONS ")
      _sb.AppendLine("   SET   CGS_CLOSE_DATETIME  = NULL ")
      _sb.AppendLine("       , CGS_CLOSE_USER_ID   = NULL ")
      _sb.AppendLine("       , CGS_CAGE_STOCK      = NULL ")
      _sb.AppendLine(" WHERE   CGS_CAGE_SESSION_ID = @pSessionId ")

      Using _cmd As New SqlClient.SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx)

        Me.CloseDateTime = WSI.Common.WGDB.Now

        _cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = Me.CageSessionId
        _result = False
        If _cmd.ExecuteNonQuery() = 1 Then

          If CreateNewCageMeters(SqlTrx) Then

            Me.OperationType = Cage.CageOperationType.ReOpenCage
            Me.Status = Cage.CageStatus.FinishedOpenCloseCage
            If InsertMovement(SqlTrx) Then

              _result = True
            End If
          End If
        End If
      End Using

      Return _result

    Catch ex As Exception
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "cls_cage_control2", _
                            "ReOpenCageSessionDB", _
                            ex.Message)
      Return False
    End Try

  End Function ' ReOpenCageSessionDB

  ' PURPOSE: Get Chips circulate from a cage session
  '
  ' PARAMS:
  '   - INPUT:  sql transaction
  '
  ' RETURNS:
  '     - _result Boolean
  '
  ' NOTES
  '
  Private Function GetChipsCirculateFromSession(ByRef ChipsCirculate As Currency) As Boolean
    Dim _sb As StringBuilder

    ChipsCirculate = 0
    Try
      _sb = New StringBuilder

      _sb.AppendLine(" select isnull(")
      _sb.AppendLine("		sum(case when cgm_type >= 0 and cgm_type <= 99 then  ")
      _sb.AppendLine("		                                               case when cmd_cage_currency_type = @pIsoTypeColor then cmd_quantity ")
      _sb.AppendLine("		                                                    else cmd_denomination * cmd_quantity end ")
      _sb.AppendLine("		         when cgm_type >= 100 and cgm_type <= 199 then -1 * ")
      _sb.AppendLine("		                                               case when cmd_cage_currency_type = @pIsoTypeColor then cmd_quantity ")
      _sb.AppendLine("		                                                    else cmd_denomination * cmd_quantity end ")
      _sb.AppendLine("		         else 0 ")
      _sb.AppendLine("		    end), 0 ) as chips_circulate ")
      _sb.AppendLine(" from cage_movements ")
      _sb.AppendLine("    inner join cage_movement_details on cgm_movement_id = cmd_movement_id ")
      _sb.AppendLine(" where cgm_cage_session_id = @pSessionId ")
      _sb.AppendLine("    and cmd_cage_currency_type in (@pIsoTypeValue, @pIsoTypeNoValue, @pIsoTypeColor) ")

      Using _db_trx As DB_TRX = New DB_TRX()
        Using _cmd As New SqlClient.SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          _cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = Me.CageSessionId
          _cmd.Parameters.Add("@pIsoTypeValue", SqlDbType.Int).Value = FeatureChips.ChipType.RE
          _cmd.Parameters.Add("@pIsoTypeNoValue", SqlDbType.Int).Value = FeatureChips.ChipType.NR
          _cmd.Parameters.Add("@pIsoTypeColor", SqlDbType.Int).Value = FeatureChips.ChipType.COLOR

          Using _reader As SqlClient.SqlDataReader = _db_trx.ExecuteReader(_cmd)
            If _reader.Read() Then
              ChipsCirculate = _reader.GetDecimal(0)
            End If

          End Using

        End Using
      End Using

      Return True

    Catch ex As Exception
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "cls_cage_control2", _
                            "GetChipsCirculateFromSession", _
                            ex.Message)
      Return False

    End Try

  End Function ' GetChipsCirculateFromSession


  ' PURPOSE: Create new cage meters when reopen session
  '
  ' PARAMS:
  '   - INPUT:  sql transaction
  '
  ' RETURNS:
  '     - _result Boolean
  '
  ' NOTES
  '
  Private Function CreateNewCageMeters(ByVal SqlTrx As SqlTransaction) As Boolean
    'LMRS 02/10/2014
    Dim _sb As StringBuilder
    Dim _table As DataTable

    Try



      _sb = New StringBuilder

      _sb.AppendLine(" SELECT   CSTC_CONCEPT_ID, CSTC_SOURCE_TARGET_ID ")
      _sb.AppendLine("   FROM CAGE_SOURCE_TARGET_CONCEPTS               ")

      _table = GUI_GetTableUsingSQL(_sb.ToString, Integer.MaxValue)

      For Each _el As DataRow In _table.Rows
        Call CageMeters.CreateCageMeters(_el("CSTC_SOURCE_TARGET_ID"), _
                                         _el("CSTC_CONCEPT_ID"), _
                                         Me.CageSessionId, _
                                         CageMeters.CreateCageMetersOption.CageSessionMeters, _
                                         SqlTrx)
      Next
      Return True
    Catch ex As Exception
      Return False
    End Try
  End Function

  Private Function GetQuantity(Row As System.Data.DataRow) As Double
    Dim _q1 As Double
    Dim _q2 As Double

    _q1 = Me.GetRowValue(Row(DT_COLUMN_CURRENCY.QUANTITY))
    _q2 = Me.GetRowValue(Row(DT_COLUMN_CURRENCY.QUANTITY_DIFFERENCE))

    If _q2 < _q1 Then
      Return Math.Min(_q1, _q2)
    ElseIf _q2 = _q1 Then
      Return _q1
    Else
      Return Math.Max(_q1, _q2)
    End If

  End Function
#End Region ' Private

#Region "Public"

  '----------------------------------------------------------------------------
  ' PURPOSE: Gets the cage session
  '
  ' PARAMS:
  '
  ' RETURNS:
  '     - CageMovementId
  '
  ' NOTES:
  Public Function GetCageSessionIdByMovement(ByVal CageMovementId As Long) As Long
    Dim _sb As StringBuilder
    Dim _obj As Object
    Dim _cage_session_id As Long

    _cage_session_id = -1
    Try
      _sb = New StringBuilder

      _sb.AppendLine("SELECT    CGM_CAGE_SESSION_ID ")
      _sb.AppendLine("  FROM    CAGE_MOVEMENTS ")
      _sb.AppendLine(" WHERE    CGM_MOVEMENT_ID = @pCageMovementId ")

      Using _db_trx As New DB_TRX()
        Using _cmd As New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          _cmd.Parameters.Add("@pCageMovementId", SqlDbType.BigInt).Value = CageMovementId

          _obj = _cmd.ExecuteScalar()

          If _obj IsNot Nothing AndAlso _obj IsNot DBNull.Value Then
            _cage_session_id = CLng(_obj)
          End If

        End Using
      End Using

    Catch ex As Exception
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "cls_cage_control2", _
                            "GetCageSessionIdByMovement", _
                            ex.Message)
      _cage_session_id = -1
    End Try

    Return _cage_session_id

  End Function ' GetCageSessionIdByMovement

  '----------------------------------------------------------------------------
  ' PURPOSE: Gets the cage session
  '
  ' PARAMS:
  '
  ' RETURNS:
  '     - CageMovementId
  '
  ' NOTES:
  Public Function GetMoneyCollectionIdByMovement(ByVal CageMovementId As Long, ByRef CollectionId As Int64, ByRef CollectionStatus As Int32) As Boolean
    Dim _sb As StringBuilder

    _sb = New StringBuilder()

    _sb.AppendLine("     SELECT   CGM_MC_COLLECTION_ID               ")
    _sb.AppendLine("            , MC_STATUS                          ")
    _sb.AppendLine("       FROM   CAGE_MOVEMENTS                     ")
    _sb.AppendLine(" INNER JOIN   MONEY_COLLECTIONS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID ")
    _sb.AppendLine("      WHERE   CGM_MOVEMENT_ID = @pCageMovementId ")

    Try
      Using _db_trx As New DB_TRX()
        Using _cmd As New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          _cmd.Parameters.Add("@pCageMovementId", SqlDbType.BigInt).Value = CageMovementId
          Using _reader As SqlClient.SqlDataReader = _db_trx.ExecuteReader(_cmd)
            If _reader.Read() Then
              CollectionId = _reader(0)
              CollectionStatus = _reader(1)

              Return True
            End If
          End Using

        End Using
      End Using
    Catch ex As Exception
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "cls_cage_control2", _
                            "GetMoneyCollectionIdByMovement", _
                            ex.Message)
    End Try

    Return False

  End Function ' GetMoneyCollectionIdByMovement

  '----------------------------------------------------------------------------
  ' PURPOSE: Gets the cage session
  '
  ' PARAMS:
  '
  ' RETURNS:
  '     - MoneyCollectionId
  '
  ' NOTES:
  Public Function GetCageSessionByMoneyCollection(ByVal MoneyCollectionId As Long) As Long
    Dim _sb As StringBuilder
    Dim _obj As Object
    Dim _cage_session_id As Long

    _cage_session_id = -1

    Try
      _sb = New StringBuilder

      _sb.AppendLine("SELECT    CGM_CAGE_SESSION_ID ")
      _sb.AppendLine("  FROM    CAGE_MOVEMENTS ")
      _sb.AppendLine(" WHERE    CGM_MC_COLLECTION_ID = @pMoneyCollectionId ")

      Using _db_trx As New DB_TRX()
        Using _cmd As New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          _cmd.Parameters.Add("@pMoneyCollectionId", SqlDbType.BigInt).Value = MoneyCollectionId

          _obj = _cmd.ExecuteScalar()

          If _obj IsNot Nothing AndAlso _obj IsNot DBNull.Value Then
            _cage_session_id = CLng(_obj)
          End If

        End Using
      End Using

    Catch ex As Exception
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "cls_cage_control2", _
                            "GetCageSessionByMoneyCollection", _
                            ex.Message)
      _cage_session_id = -1
    End Try

    Return _cage_session_id

  End Function ' GetCageSessionByMoneyCollection

  ' PURPOSE: Formats amount
  '
  '  PARAMS:
  '     - INPUT:
  '           - Amount
  '           - IsDenomination
  '           - IsoCode
  '           - ShowZeros
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - FormatAmount
  Public Function FormatAmount(ByVal Amount As Decimal _
                              , ByVal IsDenomination As Boolean _
                              , ByVal IsoCode As String _
                              , Optional ByVal ShowZeros As Boolean = True) As String

    Dim _currency_exchange As WSI.Common.CurrencyExchangeProperties
    Dim _nfi As Globalization.NumberFormatInfo
    Dim _formatted_amount As String

    _nfi = System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat
    _formatted_amount = String.Empty

    If Amount = 0 And ShowZeros = False Then
      Return _formatted_amount
    End If

    If Not Amount = Nothing Or Amount = 0 Then
      _currency_exchange = WSI.Common.CurrencyExchangeProperties.GetProperties(IsoCode)

      If Not _currency_exchange Is Nothing Then
        If Amount < 0 Then
          _formatted_amount = "-" & _currency_exchange.FormatCurrency(Amount * -1) ' s'hauria d'implementar a currency exchange la possiblitat de -$n
        Else
          _formatted_amount = _currency_exchange.FormatCurrency(Amount)
        End If

      Else
        _formatted_amount = GUI_FormatNumber(Amount)
      End If
    End If

    Return _formatted_amount
  End Function ' FormatAmount

#End Region ' Public

#Region " Virtual Gaming Tables"

  ' PURPOSE: Collects movement from gaming tables
  '
  ' PARAMS:
  '     - Transaction
  '
  ' RETURNS:
  '     - true if ok
  '
  ' NOTES:
  Private Function InsertMovementVirtualGamingTable(ByVal SqlTrx As SqlTransaction) As Boolean
    Dim _session_stats As New WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS
    Dim _total_collected As Currency
    Dim _cashier_session As CashierSessionInfo
    Dim _terminal_name As String
    Dim _gaming_table_session As GamingTablesSessions
    Dim _chips_amount As New SortedDictionary(Of CurrencyIsoType, Decimal)(New CurrencyIsoType.SortComparer)
    Dim _result As Boolean
    Dim _gt_update_type As GTS_UPDATE_TYPE

    _gaming_table_session = Nothing
    _result = True
    Try
      _terminal_name = GetGamingTableName()
      'primero recuperar una sesion de caja v�lida
      _cashier_session = Cashier.GetSystemCashierSessionInfo(GU_USER_TYPE.SYS_GAMING_TABLE, SqlTrx, _terminal_name)
      '_chips_amount = New SortedDictionary(Of CurrencyIsoType, Decimal)(New CurrencyIsoType.SortComparer)


      If _result AndAlso _cashier_session Is Nothing Then
        _result = False
      End If

      If _result AndAlso Not GamingTablesSessions.GetOrOpenSession(_gaming_table_session, Me.GamingTableId, _cashier_session.CashierSessionId, SqlTrx) Then
        _result = False
      End If

      'Insertar movimientos de "Deposito" (Igual que se hace en el cajero) cage+totales
      If _result AndAlso Not InsertCageCashierMovements(SqlTrx, Me.TableCurrencies, _cashier_session, CASHIER_MOVEMENT.CAGE_FILLER_IN, _chips_amount, _gaming_table_session.GamingTableSessionId) Then
        _result = False
      End If

      'Actualizar tabla de sesiones de mesa 
      _gt_update_type = GTS_UPDATE_TYPE.FillIn
      If Me.CashierSessionId <= 0 Then
        _gt_update_type = GTS_UPDATE_TYPE.OpenFillIn
      End If

      If _result AndAlso Not _gaming_table_session.UpdateSession(_gt_update_type, _chips_amount, SqlTrx) Then
        _result = False
      End If

      ' JBP 19-FEB-2014: Actualizar Collected amount de la sesi�n de mesa de juego 
      If _result Then
        _total_collected = 0

        ' Read Cashier session data to update collected
        Call Cashier.ReadCashierSessionData(SqlTrx, _gaming_table_session.CashierSessionId, _session_stats)
        Call GamingTablesSessions.GetCollectedAmount(_session_stats, _chips_amount, _total_collected, SqlTrx)

        If Not _gaming_table_session.UpdateSession(GTS_UPDATE_TYPE.CollectedAmount, _total_collected, SqlTrx) Then
          _result = False
        End If

        If Not _gaming_table_session.UpdateSession(GTS_UPDATE_TYPE.CollectedAmount, _chips_amount, SqlTrx) Then
          _result = False
        End If
      End If

    Catch ex As Exception
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "cls_cage_control2", _
                            "InsertMovementVirtualGamingTable", _
                            ex.Message)
      _result = False
    End Try

    Return _result
  End Function 'InsertMovementVirtualGamingTable

  ' PURPOSE: Collects movement from gaming tables
  '
  ' PARAMS:
  '     - CloseSession: marks if fillout or close movement
  '     - Transaction
  '
  ' RETURNS:
  '     - true if ok
  '
  ' NOTES:
  Private Function CollectMovementVirtualGamingTable(ByVal CloseSession As Boolean, ByVal SqlTrx As SqlTransaction) As Boolean
    Dim _cashier_session As CashierSessionInfo
    Dim _cashier_session_id As Long
    Dim _gaming_table_session As GamingTablesSessions
    Dim _chips_amount As New SortedDictionary(Of CurrencyIsoType, Decimal)(New CurrencyIsoType.SortComparer)
    Dim _chips_amount_close As Currency
    Dim _total_cash_amount As Currency
    Dim _result As Boolean

    _result = True
    _total_cash_amount = 0
    _chips_amount_close = 0
    _cashier_session_id = 0
    _cashier_session = Nothing
    _gaming_table_session = Nothing

    Try
      'Mirar si existe una sesi�n de caja abierta de esta mesa de juego.
      Call Cashier.ExistCashierTerminalSessionOpen(GetRelatedCashierIdByGamingTableId(), _cashier_session_id, SqlTrx)
      If _cashier_session_id = 0 Then
        Return False
      End If

      'Recuperar una sesion de caja v�lida
      _cashier_session = Cashier.GetSystemCashierSessionInfo(GU_USER_TYPE.SYS_GAMING_TABLE, SqlTrx, GetGamingTableName())
      If _cashier_session Is Nothing Then
        Return False
      End If

      If Not GamingTablesSessions.GetOrOpenSession(_gaming_table_session, Me.GamingTableId, _cashier_session_id, SqlTrx) Then
        Return False
      End If

      ' Generate cashier movements
      If CloseSession Then
        ' Cerrar sesion: movimientos de "CLOSE" (Igual que se hace en el cajero el cierre de caja cage+totales)
        If Not InsertCageCashierMovements(SqlTrx, Me.TableCurrencies, _cashier_session, CASHIER_MOVEMENT.CAGE_CLOSE_SESSION, _chips_amount, _gaming_table_session.GamingTableSessionId, _total_cash_amount) Then
          _result = False
        End If
      Else
        ' Retirar: movimientos de "FILLOUT" (Igual que se hace en el cajero cage+totales)
        If Not InsertCageCashierMovements(SqlTrx, Me.TableCurrencies, _cashier_session, CASHIER_MOVEMENT.CAGE_FILLER_OUT, _chips_amount, _gaming_table_session.GamingTableSessionId, _total_cash_amount) Then
          _result = False
        End If
      End If

      If _result Then
        _result = UpdateGamblingTableSessions(SqlTrx, CloseSession, _cashier_session_id, _chips_amount, _gaming_table_session)
      End If

    Catch ex As Exception
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "cls_cage_control2", _
                            "CollectMovementVirtualGamingTable", _
                            ex.Message)
      _result = False
    End Try

    Return _result

  End Function 'CollectMovementVirtualGamingTable

  Private Function UpdateGamblingTableSessionsDropbox(ByVal TableCurrencies As DataTable, ByVal SqlTrx As SqlTransaction) As Boolean

    Dim _total_collected_amounts As New SortedDictionary(Of CurrencyIsoType, Decimal)(New CurrencyIsoType.SortComparer)
    Dim _total_collected_chips As New SortedDictionary(Of CurrencyIsoType, Decimal)(New CurrencyIsoType.SortComparer)
    Dim _gt_session As GamingTablesSessions
    Dim _total_colleted_national As Decimal
    Dim _total_colleted_chips_national As Decimal
    Dim _total_tickets_collected As Decimal

    _gt_session = Nothing
    _total_colleted_national = 0
    _total_colleted_chips_national = 0
    _total_tickets_collected = 0

    CalculateTotalsAmounts(TableCurrencies, _total_collected_amounts, _total_collected_chips, _total_colleted_national, _total_colleted_chips_national, _total_tickets_collected, SqlTrx)

    If Not GamingTablesSessions.GetOrOpenSession(_gt_session, Me.GamingTableId, Me.CashierSessionId, SqlTrx) Then
      Return False
    End If

    ' Update total collected dropbox in national currency
    If Not _gt_session.UpdateSession(GTS_UPDATE_TYPE.CollectedDropBoxAmount, _total_colleted_national, SqlTrx) Then
      Return False
    End If

    ' Update total collected dropbox by currency
    If Not _gt_session.UpdateSession(GTS_UPDATE_TYPE.CollectedDropBoxAmount, _total_collected_amounts, SqlTrx) Then
      Return False
    End If

    ' Update total collected chips (RE & NR)
    If Not _gt_session.UpdateSession(GTS_UPDATE_TYPE.CollectedDropBoxChips, _total_colleted_chips_national, SqlTrx) Then
      Return False
    End If

    If Not _gt_session.UpdateSession(GTS_UPDATE_TYPE.CollectedDropBoxChips, _total_collected_chips, SqlTrx) Then
      Return False
    End If

    ' Update total tickets collected dropbox 
    If Not _gt_session.UpdateSession(GTS_UPDATE_TYPE.CollectedDropBoxTickets, _total_tickets_collected, SqlTrx) Then
      Return False
    End If

    If Not _gt_session.UpdateSession(GTS_UPDATE_TYPE.Tips, GetChipTips, SqlTrx) Then
      Return False
    End If



    Return True

  End Function

  Private Sub CalculateTotalsAmounts(ByVal TableCurrencies As DataTable, ByRef TotalAmount As SortedDictionary(Of CurrencyIsoType, Decimal), _
                                       ByRef TotalChips As SortedDictionary(Of CurrencyIsoType, Decimal), ByRef TotalAmountNational As Decimal, _
                                       ByRef TotalChipsNational As Decimal, ByRef TotalTicketsAmount As Decimal, ByVal SqlTrx As SqlTransaction)

    Dim _total As Decimal
    Dim _iso_code As String
    Dim _currency_type As CurrencyExchangeType
    Dim _currency_exchange As CurrencyExchange
    Dim _exchange_result As CurrencyExchangeResult

    _currency_exchange = Nothing
    _exchange_result = Nothing
    _iso_code = CurrencyExchange.GetNationalCurrency()

    For Each _row As DataRow In TableCurrencies.Rows
      If _row(DT_COLUMN_CURRENCY.TOTAL) Is DBNull.Value OrElse Not Decimal.TryParse(_row(DT_COLUMN_CURRENCY.TOTAL), _total) Then
        Continue For
      End If

      _currency_type = FeatureChips.ConvertToCurrencyExchangeType(CType(_row(DT_COLUMN_CURRENCY.CURRENCY_TYPE), CageCurrencyType))

      If _row(DT_COLUMN_CURRENCY.ISO_CODE) <> CurrencyExchange.GetNationalCurrency() AndAlso _row(DT_COLUMN_CURRENCY.ISO_CODE) <> _iso_code Then
        CurrencyExchange.ReadCurrencyExchange(_currency_type, _row(DT_COLUMN_CURRENCY.ISO_CODE), _currency_exchange, SqlTrx)
      End If

      _iso_code = _row(DT_COLUMN_CURRENCY.ISO_CODE)

      ' Accumulate tickets TITO
      If (_row(DT_COLUMN_CURRENCY.DENOMINATION) = Cage.TICKETS_CODE AndAlso _row(DT_COLUMN_CURRENCY.QUANTITY) > 0) Then
        TotalTicketsAmount += Me.m_type_new_collection.real_ticket_sum
        Continue For
      End If

      If _total > 0 Then
        If _currency_type <> CurrencyExchangeType.CURRENCY AndAlso Not FeatureChips.IsChipsType(_currency_type) Then
          Continue For
        End If

        If Not TotalAmount.ContainsKey(New CurrencyIsoType(_iso_code, _currency_type)) Then
          TotalAmount.Add(New CurrencyIsoType(_iso_code, _currency_type), 0)
        End If

        ' Currency exchange must to convert to national currency to accumulate
        If _iso_code <> CurrencyExchange.GetNationalCurrency() AndAlso Not FeatureChips.IsChipsType(_currency_type) Then
          _currency_exchange.ApplyExchange(_total, _exchange_result)
          TotalAmountNational += _exchange_result.GrossAmount
        ElseIf Not FeatureChips.IsChipsType(_currency_type) Then
          TotalAmountNational += _total
        ElseIf FeatureChips.IsChipsType(_currency_type) And Not _currency_type = CurrencyExchangeType.CASINO_CHIP_COLOR Then
          ' Acumulate chips
          If Not TotalChips.ContainsKey(New CurrencyIsoType(_iso_code, _currency_type)) Then
            TotalChips.Add(New CurrencyIsoType(_iso_code, _currency_type), 0)
          End If

          ' Currency exchange must to convert to national currency to accumulate
          If _iso_code <> CurrencyExchange.GetNationalCurrency() Then
            _currency_exchange.ApplyExchange(_total, _exchange_result)
            TotalChipsNational += _exchange_result.GrossAmount
          ElseIf FeatureChips.IsChipsType(_currency_type) Then
            TotalChipsNational += _total
          End If

          TotalChips(New CurrencyIsoType(_iso_code, _currency_type)) += _total

        End If

        TotalAmount(New CurrencyIsoType(_iso_code, _currency_type)) += _total

      End If

    Next

  End Sub

  ' PURPOSE: update values from gambling table sessions
  '
  ' PARAMS:
  '
  ' RETURNS:
  '     - true if everything ok
  '
  ' NOTES:
  Private Function UpdateGamblingTableSessions(ByVal SqlTrx As SqlTransaction, ByVal CloseSession As Boolean, ByVal CashierSessionId As Long _
                                             , ByVal ChipsAmount As SortedDictionary(Of CurrencyIsoType, Decimal), ByVal GamingTableSession As GamingTablesSessions) As Boolean
    Dim _total_collected As SortedDictionary(Of CurrencyIsoType, Decimal)
    Dim _session_data As New WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS
    Dim _result As Boolean
    Dim _total_collected_national As Decimal
    Dim _gt_update_type As GTS_UPDATE_TYPE

    _result = True
    _total_collected = New SortedDictionary(Of CurrencyIsoType, Decimal)
    _gt_update_type = GTS_UPDATE_TYPE.FillOut
    Try
      ' Read Cashier session data to update collected
      WSI.Common.Cashier.ReadCashierSessionData(SqlTrx, GamingTableSession.CashierSessionId, _session_data)

      If CloseSession Then
        Call SetCollectedSessionData(_session_data)

        GamingTablesSessions.GetCollectedAmount(_session_data, _total_collected, _total_collected_national, SqlTrx)

        ' Add total collected amounts, exclude chips types
        For Each _total_collected_item As KeyValuePair(Of CurrencyIsoType, Decimal) In _total_collected
          If Not FeatureChips.IsChipsType(_total_collected_item.Key.Type) Then
            ChipsAmount.Add(_total_collected_item.Key, _total_collected_item.Value)
          End If
        Next

        ' Actualizar tabla de sesiones de mesa 
        If _result _
          AndAlso Not GamingTableSession.CloseSession(ChipsAmount, _total_collected_national, Me.GamingTableVisits, GetChipTips, _session_data.collected, SqlTrx) Then
          _result = False
        End If
        ' Tancar cashier session
        If _result AndAlso Not CloseCashierSession(_session_data, SqlTrx, CashierSessionId) Then
          _result = False
        End If
      Else
        GamingTablesSessions.GetCollectedAmount(_session_data, _total_collected, _total_collected_national, SqlTrx)

        If CloseSession Then
          _gt_update_type = GTS_UPDATE_TYPE.CloseFillOut
        End If
        ' Actualizar tabla de sesiones de mesa 
        If _result AndAlso Not GamingTableSession.UpdateSession(_gt_update_type, ChipsAmount, SqlTrx) Then
          _result = False
        End If

        ' Update total collected converted to national amount
        If _result AndAlso Not GamingTableSession.UpdateSession(GTS_UPDATE_TYPE.CollectedAmount, _total_collected_national, SqlTrx) Then
          _result = False
        End If

        ' Update collected amount for each currency
        If _result AndAlso Not GamingTableSession.UpdateSession(GTS_UPDATE_TYPE.CollectedAmount, _total_collected, SqlTrx) Then
          _result = False
        End If

        If _result AndAlso Not GamingTableSession.UpdateSession(GTS_UPDATE_TYPE.Tips, GetChipTips, SqlTrx) Then
          _result = False
        End If
      End If

    Catch ex As Exception
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "cls_cage_control2", _
                            "UpdateGamblingTableSessions", _
                            ex.Message)
      _result = False
    End Try

    Return _result

  End Function ' UpdateGamblingTableSessions

  ' PURPOSE: sets collected amount into session data
  '
  ' PARAMS:
  '
  ' RETURNS:
  '     - session data
  '
  ' NOTES:
  Private Sub SetCollectedSessionData(ByRef SessionData As WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS)
    Dim _iso_type As CurrencyIsoType

    For Each _row As DataRow In Me.TableCurrencies.Rows
      _iso_type = New CurrencyIsoType()
      _iso_type.IsoCode = _row("ISO_CODE")

      If GetRowValue(_row("TOTAL")) <= 0 Then
        Continue For
      End If

      If WSI.Common.FeatureChips.IsChipsType(CType(_row("TYPE"), CurrencyExchangeType)) Then
        _iso_type.Type = CType(_row("TYPE"), CurrencyExchangeType)
      Else
        _iso_type.Type = CurrencyExchangeType.CURRENCY
      End If
      If Not SessionData.collected.ContainsKey(_iso_type) Then
        SessionData.collected.Add(_iso_type, 0)
      End If
      SessionData.collected(_iso_type) += GetRowValue(_row("TOTAL"))
    Next

  End Sub ' SetCollectedSessionData

  ' PURPOSE: Gets tips value for chips
  '
  ' PARAMS:
  '
  ' RETURNS:
  '     - Tips
  '
  ' NOTES:
  Private Function GetChipTips() As SortedDictionary(Of CurrencyIsoType, Decimal)
    Dim _row_tips As DataRow()
    Dim _tips As Decimal
    Dim _tips_currency As SortedDictionary(Of CurrencyIsoType, Decimal)
    Dim _iso_code As String
    Dim _currency_type As CageCurrencyType
    Dim _currency_iso_type As CurrencyIsoType
    Dim _decimal As Decimal
    Dim _dt_currencies_types As DataTable

    _tips_currency = New SortedDictionary(Of CurrencyIsoType, Decimal)(New CurrencyIsoType.SortComparer)
    _tips = 0

    _dt_currencies_types = GetAllowedISOCodeWithoutColor()

    _row_tips = ConceptsData.Select("CONCEPT_ID = '" & CageMeters.CageConceptId.Tips & "'")
    If _row_tips.Length > 0 Then
      For Each _row As DataRow In _row_tips
        _iso_code = _row.Item("CONCEPT_ISO_CODE")
        _currency_type = _row.Item("CONCEPT_CAGE_CURRENCY_TYPE")

        _currency_iso_type = New CurrencyIsoType(_iso_code, _currency_type)

        _decimal = 0
        Try
          If Not String.IsNullOrEmpty(_row.Item("CONCEPT_AMOUNT")) Then
            _decimal = CDec(_row.Item("CONCEPT_AMOUNT"))
          End If
        Catch ex As Exception
        End Try

        If (_decimal <= 0) Then
          Continue For
        End If

        If _tips_currency.ContainsKey(_currency_iso_type) Then
          _tips_currency(_currency_iso_type) += _decimal
        Else
          _tips_currency.Add(_currency_iso_type, _decimal)
        End If
      Next
    End If

    Return _tips_currency
  End Function ' GetChipTips

  ' PURPOSE: Gets name of a gaming table
  '
  ' PARAMS:
  '
  ' RETURNS:
  '     - Gaming Table name
  '
  ' NOTES:
  Private Function GetGamingTableName() As String
    Dim _sb As StringBuilder
    Dim _obj As Object
    Dim _gambling_table_name As String

    _gambling_table_name = String.Empty
    Try
      _sb = New StringBuilder

      _sb.AppendLine("   SELECT   GT_NAME ")
      _sb.AppendLine("     FROM   GAMING_TABLES ")
      _sb.AppendLine("    WHERE   GT_GAMING_TABLE_ID = " & Me.GamingTableId)

      Using _db_trx As New DB_TRX()
        Using _cmd As New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          _obj = _cmd.ExecuteScalar()

          If _obj IsNot Nothing AndAlso _obj IsNot DBNull.Value Then
            _gambling_table_name = CStr(_obj)
          End If

        End Using
      End Using

    Catch ex As Exception
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "cls_cage_control2", _
                            "GetGamingTableName", _
                            ex.Message)
      _gambling_table_name = String.Empty
    End Try

    Return _gambling_table_name

  End Function ' GetGamingTableName

  ' PURPOSE: Gets related cashier
  '
  ' PARAMS:
  '   - INPUT: gaming table id
  '
  ' RETURNS:
  '     - gaming table cashier id
  '
  ' NOTES:
  Private Function GetRelatedCashierIdByGamingTableId(Optional ByVal CurrentGamingTableId As Integer = -1) As Long
    Dim _sb As StringBuilder
    Dim _gaming_table_id As Integer
    Dim _obj As Object
    Dim _cashier_id As Long

    _gaming_table_id = IIf(CurrentGamingTableId <> -1, CurrentGamingTableId, Me.GamingTableId)

    Try
      _cashier_id = 0
      _sb = New StringBuilder

      _sb.AppendLine("   SELECT   GT_CASHIER_ID ")
      _sb.AppendLine("     FROM   GAMING_TABLES ")
      _sb.AppendLine("    WHERE   GT_GAMING_TABLE_ID = " & _gaming_table_id)

      Using _db_trx As New DB_TRX()
        Using _cmd As New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          _obj = _cmd.ExecuteScalar()

          If _obj IsNot Nothing AndAlso _obj IsNot DBNull.Value Then
            _cashier_id = CLng(_obj)
          End If

        End Using
      End Using

    Catch ex As Exception
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "cls_cage_control2", _
                            "GetRelatedCashierIdByGamingTableId", _
                            ex.Message)
      _cashier_id = 0
    End Try

    Return _cashier_id

  End Function ' GetRelatedCashierIdByGamingTableId

  ' PURPOSE: goes over all datatable rows and calls function that inserts CAGE movements 
  '
  ' PARAMS:
  '   - INPUT: sql transaction, table, cashier movement
  '
  ' RETURNS:
  '     - True if everything went OK. False if there was an error
  '
  ' NOTES:
  Private Function InsertCageCashierMovements(ByVal SqlTrx As SqlTransaction, ByVal Table As DataTable, ByVal CashierSession As CashierSessionInfo _
                                            , ByVal CashierMovementType As CASHIER_MOVEMENT, ByRef ChipsAmount As SortedDictionary(Of CurrencyIsoType, Decimal) _
                                            , ByVal GamingTableSessionId As Long, Optional ByRef TotalCashAmount As Decimal = 0) As Boolean
    Dim _row As DataRow
    Dim _cashier_movements As CashierMovementsTable
    Dim _total As Decimal
    Dim _total_denomination As New SortedDictionary(Of CurrencyIsoType, Decimal)(New CurrencyIsoType.SortComparer)
    Dim _denomination As Decimal
    Dim _iso_code As String
    Dim _iso_code_ant As String
    Dim _first_time As Boolean
    Dim _aux_row As DataRow
    Dim _totals As DataTable
    Dim _national_currency As String
    Dim _result As Boolean
    Dim _currency_exchange As CurrencyExchange
    Dim _exchange_result As CurrencyExchangeResult
    Dim _aux_amount As Decimal
    Dim _currency_type As CurrencyExchangeType
    Dim _currency_type_ant As CurrencyExchangeType
    Dim _cage_currency_type As CageCurrencyType
    Dim _operation_id As Int64

    Try
      _totals = New DataTable
      _totals.Columns.Add("ISO_CODE")
      _totals.Columns.Add("TYPE", GetType(CurrencyExchangeType))
      _totals.Columns.Add("TOTAL")

      _currency_exchange = New CurrencyExchange()
      _exchange_result = New CurrencyExchangeResult()

      _cashier_movements = New CashierMovementsTable(CashierSession)
      _first_time = True
      _iso_code_ant = ""
      _currency_type_ant = CurrencyExchangeType.CURRENCY
      _iso_code = ""
      _national_currency = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode")

      _operation_id = 0

      If CashierMovementType = CASHIER_MOVEMENT.CAGE_FILLER_OUT Then
        Operations.DB_InsertOperation(OperationCode.CASH_WITHDRAWAL, 0, Me.CashierSessionId, 0, 0, 0, 0, 0, 0, String.Empty, _operation_id, SqlTrx)
      End If


      _result = False

      For Each _row In Table.Rows
        _iso_code = _row(DT_COLUMN_CURRENCY.ISO_CODE).ToString
        _cage_currency_type = CType(_row(DT_COLUMN_CURRENCY.CURRENCY_TYPE), CageCurrencyType)
        _aux_amount = -1

        If FeatureChips.IsCageCurrencyTypeChips(_cage_currency_type) Then
          _currency_type = FeatureChips.ConvertToCurrencyExchangeType(_cage_currency_type)
        Else
          _currency_type = CurrencyExchangeType.CURRENCY
        End If

        If _first_time Then
          _first_time = False
          _iso_code_ant = _iso_code
          _currency_type_ant = _currency_type
        ElseIf _iso_code_ant <> _iso_code Or _currency_type <> _currency_type_ant Then
          _iso_code_ant = _iso_code
          _currency_type_ant = _currency_type
        End If

        If GetRowValue(_row(DT_COLUMN_CURRENCY.QUANTITY)) > 0 _
            Or GetRowValue(_row(DT_COLUMN_CURRENCY.TOTAL)) > 0 Then ' If quantity or total are 0 , don't insert into BBDD


          If _row(DT_COLUMN_CURRENCY.DENOMINATION) < 0 Then
            _denomination = _row(DT_COLUMN_CURRENCY.DENOMINATION)
            If _denomination = Cage.TICKETS_CODE Then
              _total = _row(DT_COLUMN_CURRENCY.QUANTITY)
            Else
              _total = _row(DT_COLUMN_CURRENCY.TOTAL)
            End If
          Else
            _denomination = _row(DT_COLUMN_CURRENCY.DENOMINATION)
            If _cage_currency_type = CageCurrencyType.ChipsColor Then
              _total = _row(DT_COLUMN_CURRENCY.QUANTITY)
            Else
              _total = _row(DT_COLUMN_CURRENCY.QUANTITY) * _denomination
            End If
          End If

          If _iso_code <> _national_currency AndAlso Not FeatureChips.IsChipsType(_cage_currency_type) Then
            If CashierMovementType = CASHIER_MOVEMENT.CAGE_CLOSE_SESSION Then
              CurrencyExchange.ReadCurrencyExchange(CurrencyExchangeType.CURRENCY, _iso_code, _currency_exchange, SqlTrx)
              _currency_type = _currency_exchange.Type
              _currency_exchange.ApplyExchange(_total, _exchange_result)
              _aux_amount = _exchange_result.GrossAmount
            End If

          End If

          _cashier_movements.Add(_operation_id, CashierMovementType, _total, 0, String.Empty, String.Empty, _iso_code, _denomination, Me.MovementID, 0, _aux_amount, _currency_type, _cage_currency_type, _row("CHIP_ID"))

          If _denomination = Cage.CHECK_CODE Then
            If (Not _total_denomination.ContainsKey(New CurrencyIsoType(_iso_code, CurrencyExchangeType.CHECK))) Then
              _total_denomination.Add(New CurrencyIsoType(_iso_code, CurrencyExchangeType.CHECK), _total)
            Else
              _total_denomination(New CurrencyIsoType(_iso_code, CurrencyExchangeType.CHECK)) += _total
            End If
          ElseIf _denomination = Cage.BANK_CARD_CODE Then
            If (Not _total_denomination.ContainsKey(New CurrencyIsoType(_iso_code, CurrencyExchangeType.CARD))) Then
              _total_denomination.Add(New CurrencyIsoType(_iso_code, CurrencyExchangeType.CARD), _total)
            Else
              _total_denomination(New CurrencyIsoType(_iso_code, CurrencyExchangeType.CARD)) += _total
            End If
          ElseIf _denomination <> Cage.TICKETS_CODE Then
            If (Not _total_denomination.ContainsKey(New CurrencyIsoType(_iso_code, _currency_type))) Then
              _total_denomination.Add(New CurrencyIsoType(_iso_code, _currency_type), _total)
            Else
              _total_denomination(New CurrencyIsoType(_iso_code, _currency_type)) += _total
            End If

          End If
          _iso_code_ant = _iso_code

        End If
      Next

      If _total_denomination.Count > 0 Then
        For Each _item As KeyValuePair(Of CurrencyIsoType, Decimal) In _total_denomination
          _aux_row = _totals.NewRow()
          _aux_row("ISO_CODE") = _item.Key.IsoCode
          _aux_row("TYPE") = CType(_item.Key.Type, CurrencyExchangeType)
          _aux_row("TOTAL") = _item.Value
          _totals.Rows.Add(_aux_row)
        Next
      End If

      If _cashier_movements.Save(SqlTrx) Then
        _result = InsertCashierMovements(SqlTrx, _totals, ChipsAmount, TotalCashAmount, GamingTableSessionId, CashierMovementType, CashierSession, _operation_id)
      Else
        _result = False
      End If

    Catch ex As Exception
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "cls_cage_control2", _
                            "InsertCageCashierMovements", _
                            ex.Message)
      _result = False
    End Try

    Return _result

  End Function ' InsertCageCashierMovements 

  ' PURPOSE: goes over all datatable rows and calls function that inserts cashier movements 
  '
  ' PARAMS:
  '   - INPUT: sql transaction, table, cashier movement
  '
  ' RETURNS:
  '     - True if everything went OK. False if there was an error
  '
  ' NOTES:
  Private Function InsertCashierMovements(ByVal SqlTrx As SqlTransaction, ByVal Totals As DataTable, ByRef ChipsAmount As SortedDictionary(Of CurrencyIsoType, Decimal) _
                                        , ByRef TotalCashAmount As Currency, ByVal GamingTableSessionId As Long _
                                        , ByVal CashierMovementType As CASHIER_MOVEMENT, ByVal CashierSession As CashierSessionInfo, ByVal OperationId As Int64) As Boolean
    Dim _national_currency As String
    Dim _iso_code As String
    Dim _aux_iso As String
    Dim _total As Decimal
    Dim _currency_exchange As CurrencyExchange
    Dim _exchange_result As CurrencyExchangeResult
    Dim _row As DataRow
    Dim _cashier_movement_type As CASHIER_MOVEMENT
    Dim _cashier_movements As CashierMovementsTable
    Dim _result As Boolean
    Dim _aux_amount As Decimal
    Dim _currency_type As CurrencyExchangeType

    Try
      _result = True
      _currency_exchange = New CurrencyExchange
      _exchange_result = New CurrencyExchangeResult

      If CashierMovementType = CASHIER_MOVEMENT.CAGE_FILLER_IN Then
        _cashier_movement_type = CASHIER_MOVEMENT.FILLER_IN

      ElseIf CashierMovementType = CASHIER_MOVEMENT.CAGE_FILLER_OUT Then
        _cashier_movement_type = CASHIER_MOVEMENT.FILLER_OUT

      ElseIf CashierMovementType = CASHIER_MOVEMENT.CAGE_CLOSE_SESSION Then
        _cashier_movement_type = CASHIER_MOVEMENT.CLOSE_SESSION
      End If

      _national_currency = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode")
      _cashier_movements = New CashierMovementsTable(CashierSession)

      If CashierMovementType = CASHIER_MOVEMENT.CAGE_FILLER_OUT Or CashierMovementType = CASHIER_MOVEMENT.CAGE_CLOSE_SESSION Then
        _result = FeatureChips.CreateGameCollectedMovements(CashierSession, Totals, GamingTableSessionId, CashierMovementType = CASHIER_MOVEMENT.CAGE_CLOSE_SESSION, OperationId, SqlTrx)
      End If

      If _result Then
        ' recorre datatable de totales por divisa e inserta movimientos de cajero no-cage
        For Each _row In Totals.Rows
          _iso_code = _row("ISO_CODE")
          _currency_type = _row("Type")
          _total = _row("TOTAL")

          If (_total <= 0) Then
            Continue For
          End If

          _aux_amount = -1
          _aux_iso = _iso_code
          If _aux_iso = _national_currency Then
            _aux_iso = String.Empty
          End If

          If WSI.Common.FeatureChips.IsCurrencyExchangeTypeChips(_currency_type) Then
            Dim _key As CurrencyIsoType
            _key = New CurrencyIsoType
            _key.IsoCode = _iso_code
            _key.Type = _currency_type

            If _currency_type = CurrencyExchangeType.CASINOCHIP Then
              _key.Type = CurrencyExchangeType.CASINO_CHIP_RE
            End If

            If (Not ChipsAmount.ContainsKey(_key)) Then
              ChipsAmount.Add(_key, _total)
            Else
              ChipsAmount(_key) = _total
            End If

          Else
            If _iso_code <> _national_currency Then
              CurrencyExchange.ReadCurrencyExchange(CurrencyExchangeType.CURRENCY, _iso_code, _currency_exchange, SqlTrx)
              _currency_exchange.ApplyExchange(_total, _exchange_result)
              _aux_amount = _exchange_result.GrossAmount
              TotalCashAmount += _exchange_result.GrossAmount
            Else
              TotalCashAmount += _total
            End If
          End If

          If _cashier_movement_type <> CASHIER_MOVEMENT.CLOSE_SESSION Then

            If _currency_type = CurrencyExchangeType.CARD Then
              _cashier_movements.Add(OperationId, _cashier_movement_type, _total, 0, String.Empty, String.Empty, _iso_code, Cage.BANK_CARD_CODE, 0, GamingTableSessionId, _aux_amount, CurrencyExchangeType.CARD, WSI.Common.FeatureChips.ConvertToCageCurrencyType(_currency_type), Nothing)
            ElseIf _currency_type = CurrencyExchangeType.CHECK Then
              _cashier_movements.Add(OperationId, CASHIER_MOVEMENT.FILLER_OUT_CHECK, _total, 0, String.Empty, String.Empty, _iso_code, Cage.CHECK_CODE, Me.MovementID, 0, _aux_amount, CurrencyExchangeType.CHECK, WSI.Common.FeatureChips.ConvertToCageCurrencyType(_currency_type), Nothing)
            Else
              _cashier_movements.Add(OperationId, _cashier_movement_type, _total, 0, String.Empty, String.Empty, _iso_code, 0, 0, GamingTableSessionId, _aux_amount, _currency_type, WSI.Common.FeatureChips.ConvertToCageCurrencyType(_currency_type), Nothing)
            End If

            If _cashier_movement_type = CASHIER_MOVEMENT.FILLER_IN Then
              If _iso_code = _national_currency Then
                Call Cashier.UpdateSessionBalance(SqlTrx, CashierSession.CashierSessionId, 0)
              Else
                Call Cashier.UpdateSessionBalance(SqlTrx, CashierSession.CashierSessionId, 0, _aux_iso, _currency_type)
              End If
            ElseIf _cashier_movement_type = CASHIER_MOVEMENT.FILLER_OUT Then
              If _iso_code = _national_currency And _currency_type = CurrencyExchangeType.CURRENCY Then
                Call Cashier.UpdateSessionBalance(SqlTrx, CashierSession.CashierSessionId, 0)
              Else
                Call WSI.Common.Cashier.SetCashierSessionsByCurrency(CashierSession.CashierSessionId, _iso_code, _currency_type, _total, _total, SqlTrx)
              End If
            End If
          End If

        Next

        If _cashier_movement_type <> CASHIER_MOVEMENT.CLOSE_SESSION Then
          Return _cashier_movements.Save(SqlTrx)
        Else
          Return True
        End If

      End If

      Return False

    Catch ex As Exception
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "cls_cage_control2", _
                            "InsertCashierMovements", _
                            ex.Message)
      Return False
    End Try

  End Function ' InsertCashierMovements

  ' PURPOSE: Closes cashier session
  '
  ' PARAMS:
  '
  ' RETURNS:
  '     - True if everything went OK. False if there was an error
  '
  ' NOTES:
  Private Function CloseCashierSession(ByVal SessionStats As WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS, _
                                       ByVal SqlTrx As SqlTransaction, ByVal CashierSessionId As Long) As Boolean

    Dim _session_info As CashierSessionInfo
    Dim _vouchers As System.Collections.ArrayList

    Try

      _vouchers = New System.Collections.ArrayList()

      _session_info = CommonCashierInformation.CashierSessionInfo()

      Cashier.CashierSessionClose(SessionStats, _session_info, _session_info.UserId, _vouchers, SqlTrx, 0)

      Call GUI_CloseSessionWriteAuditory(CashierSessionId)

      Return True

    Catch ex As Exception
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "cls_cage_control2", _
                            "CloseCashierSession", _
                            ex.Message)
      Return False
    End Try

  End Function ' CloseCashierSession

  ' PURPOSE: Gets cashier session name 
  '
  ' PARAMS:
  '   INTPUT: Cashier session id
  '
  ' RETURNS:
  '     - cashier session name
  '
  ' NOTES:
  Public Function GetCashierSessionNameById(ByVal CashierSessionId) As String
    Dim _sb As StringBuilder
    Dim _obj As Object
    Dim _cashier_name As String

    _cashier_name = String.Empty
    Try
      _sb = New StringBuilder

      _sb.AppendLine("   SELECT   CS_NAME ")
      _sb.AppendLine("     FROM   CASHIER_SESSIONS ")
      _sb.AppendLine("    WHERE   CS_SESSION_ID = " & CashierSessionId)

      Using _db_trx As New DB_TRX()
        Using _cmd As New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _obj = _cmd.ExecuteScalar()

          If _obj IsNot Nothing AndAlso _obj IsNot DBNull.Value Then
            _cashier_name = CStr(_obj)
          End If

        End Using
      End Using

    Catch ex As Exception
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "cls_cage_control2", _
                            "GetCashierSessionNameById", _
                            ex.Message)
      _cashier_name = String.Empty
    End Try

    Return _cashier_name

  End Function ' GetCashierSessionNameById

  ' PURPOSE: Write auditory changes.
  '
  '  PARAMS:
  '     - INPUT:
  '           - AmountToDeposit
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub GUI_CloseSessionWriteAuditory(ByVal CashierSessionId As Long)
    Dim _cashier_name As String

    _cashier_name = GetCashierSessionNameById(CashierSessionId)
    Me.GUI_CloseSessionWriteAuditory(_cashier_name)
  End Sub
  ' PURPOSE: Write auditory changes.
  '
  '  PARAMS:
  '     - INPUT:
  '           - AmountToDeposit
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_CloseSessionWriteAuditory(ByVal CashierName As String)
    Dim _auditor As CLASS_AUDITOR_DATA

    _auditor = New CLASS_AUDITOR_DATA(AUDIT_NAME_CASHIER_SESSIONS)

    ' Set Parameter
    Call _auditor.SetName(GLB_NLS_GUI_INVOICING.Id(40), GLB_NLS_GUI_INVOICING.GetString(40))
    Call _auditor.SetField(0, CashierName, GLB_NLS_GUI_AUDITOR.GetString(330))

    _auditor.Notify(GLB_CurrentUser.GuiId, GLB_CurrentUser.Id, GLB_CurrentUser.Name, _
                    CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.GENERIC, 0)

  End Sub 'GUI_WriteAuditoryChanges

  ' PURPOSE: Load theoric gaming table values to table currencies
  '
  ' PARAMS:
  '   INTPUT: Gaming table Id
  '
  ' RETURNS:
  '     - True if ok
  '
  ' NOTES:
  Public Function LoadGamingTableValuesToTableCurrencies(ByVal GamingTableId As Integer) As Boolean
    Dim _sb As StringBuilder
    'Dim _table As DataTable
    Dim _cashier_session_id As Long
    Dim _result As Boolean

    _sb = New StringBuilder
    _cashier_session_id = 0
    _result = True
    Try
      'Mirar si existe una sesi�n de caja abierta de esta mesa de juego.
      Using _sql_trx As New DB_TRX()
        Call Cashier.ExistCashierTerminalSessionOpen(GetRelatedCashierIdByGamingTableId(GamingTableId), _cashier_session_id, _sql_trx.SqlTransaction)

        If FeatureChips.Chips.ReadStock(_cashier_session_id, _sql_trx.SqlTransaction) Then
          SetChipsStockToTableCurrencies()
        End If
      End Using

    Catch ex As Exception
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "cls_cage_control2", _
                            "LoadGamingTableValuesToTableCurrencies", _
                            ex.Message)
      _result = False
    End Try

    Return _result
  End Function ' LoadGamingTableValuesToTableCurrencies

  ' PURPOSE: Set Chips Stock To Table Currencies
  '
  ' PARAMS:
  '   INTPUT: Stock
  '
  ' RETURNS:
  '
  ' NOTES:
  Private Sub SetChipsStockToTableCurrencies()
    Dim _rows_money As DataRow()
    Dim _quantity As Integer
    Dim _difference As Integer

    Try
      Call Me.InitCurrencyData(False, True)
      For Each _column As KeyValuePair(Of Int64, FeatureChips.Chips.Chip) In FeatureChips.Chips.GetChips()

        _rows_money = Me.TableCurrencies.Select("(TYPE = '" & FeatureChips.ChipType.RE & "' OR TYPE = '" & FeatureChips.ChipType.NR & "' OR TYPE = '" & FeatureChips.ChipType.COLOR & "') " _
                                             & " AND DENOMINATION = " & GUI_LocalNumberToDBNumber(_column.Key))

        _quantity = _column.Value.Stock
        _difference = (_quantity * _column.Value.Denomination)
        _rows_money(0).Item(DT_COLUMN_CURRENCY.CASHIER_QUANTITY) = _quantity
        _rows_money(0).Item(DT_COLUMN_CURRENCY.CASHIER_TOTAL) = _difference
        _rows_money(0).Item(DT_COLUMN_CURRENCY.QUANTITY_DIFFERENCE) = _quantity * -1
        _rows_money(0).Item(DT_COLUMN_CURRENCY.TOTAL_DIFFERENCE) = _difference * -1

      Next
      Call Me.DeleteZerosFromDT()

    Catch ex As Exception
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "cls_cage_control2", _
                            "SetChipsStockToTableCurrencies", _
                            ex.Message)
    End Try

  End Sub ' SetChipsStockToTableCurrencies

  ' PURPOSE: Gets id of a gaming table
  '
  ' PARAMS:
  '
  ' RETURNS:
  '     - Gaming Table name
  '
  ' NOTES:
  Public Function GetGamingTableIdFromCashierSession(ByVal CashierSessionId As Long) As Long
    Dim _sb As StringBuilder
    Dim _obj As Object
    Dim _gambling_table_id As Long

    _gambling_table_id = 0
    _sb = New StringBuilder

    Try
      _sb.AppendLine("       SELECT   GT_GAMING_TABLE_ID                                                          ")
      _sb.AppendLine("         FROM   GAMING_TABLES                                                               ")
      _sb.AppendLine("   INNER JOIN   GAMING_TABLES_SESSIONS ON GT_GAMING_TABLE_ID = GTS_GAMING_TABLE_ID          ")
      _sb.AppendLine("   INNER JOIN   CASHIER_SESSIONS ON CS_SESSION_ID = GTS_CASHIER_SESSION_ID                  ")
      _sb.AppendLine("        WHERE   CS_SESSION_ID = @pCashierSessionId                                          ")

      Using _db_trx As New DB_TRX()
        Using _cmd As New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          _cmd.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).Value = CashierSessionId
          _obj = _cmd.ExecuteScalar()
          If _obj IsNot Nothing AndAlso _obj IsNot DBNull.Value Then
            _gambling_table_id = CLng(_obj)
          End If

        End Using
      End Using

    Catch ex As Exception
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "cls_cage_control2", _
                            "GetGamingTableIdFromCashierSession", _
                            ex.Message)
      _gambling_table_id = 0
    End Try

    Return _gambling_table_id

  End Function ' GetGamingTableIdFromCashierSession

  ' PURPOSE: Checks if  gambling table is still enabled
  '
  ' PARAMS:
  '
  ' RETURNS:
  '       - True if still enabled
  '
  ' NOTES:
  Public Function CheckIfGamblingTableStillEnabled(ByVal pGamblingTableId As Integer) As Boolean
    Dim _sb As StringBuilder
    Dim _result As Boolean

    _result = False
    _sb = New StringBuilder

    _sb.AppendLine(" SELECT   TOP 1 GT_GAMING_TABLE_ID ")
    _sb.AppendLine("   FROM   GAMING_TABLES ")
    _sb.AppendLine("  WHERE   GT_GAMING_TABLE_ID = @pGamblingTableId ")
    _sb.AppendLine("    AND   GT_ENABLED = 1 ")

    Try

      Using _db_trx As New DB_TRX()
        Using _cmd As New SqlClient.SqlCommand(_sb.ToString())

          _cmd.Parameters.Add("@pGamblingTableId", SqlDbType.BigInt).Value = pGamblingTableId

          Using _sql_reader As SqlDataReader = _db_trx.ExecuteReader(_cmd)

            If _sql_reader.Read() Then
              _result = True
            End If

          End Using
        End Using
      End Using

    Catch ex As Exception
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "cls_cage_control2", _
                            "CheckIfGamblingTableStillEnabled", _
                            ex.Message)
      _result = False
    End Try

    Return _result
  End Function ' CheckIfGamblingTableStillEnabled

  ' PURPOSE: Gets gambling table visits from gambling table ID
  '
  ' PARAMS:
  '   - INPUT: CashierSessionId
  '
  ' RETURNS:
  '     - gambling table visits
  '
  ' NOTES:
  Public Function GetGamblingTableVisits(ByVal GamingTableId As Long, ByVal CashierSesionId As Long) As Decimal
    Dim _sb As StringBuilder
    Dim _data As Object
    Dim _client_visits As Decimal

    _sb = New StringBuilder
    _client_visits = 0
    Try
      _sb.AppendLine(" SELECT   GTS_CLIENT_VISITS                             ")
      _sb.AppendLine("   FROM   GAMING_TABLES_SESSIONS                        ")
      _sb.AppendLine("  WHERE   GTS_CASHIER_SESSION_ID = @pCashierSessionId   ")
      _sb.AppendLine("    AND   GTS_GAMING_TABLE_ID    = @pGamingTableId      ")

      Using _sql_trx As New DB_TRX()
        Using _sql_cmd As New SqlCommand(_sb.ToString(), _sql_trx.SqlTransaction.Connection, _sql_trx.SqlTransaction)
          _sql_cmd.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).Value = CashierSesionId
          _sql_cmd.Parameters.Add("@pGamingTableId", SqlDbType.BigInt).Value = GamingTableId

          _data = _sql_cmd.ExecuteScalar()

          If Not IsNothing(_data) AndAlso IsNumeric(_data) Then
            _client_visits = CType(_data, Decimal)
          Else
            _client_visits = 0
          End If

        End Using
      End Using

    Catch ex As Exception
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "cls_cage_control2", _
                            "GetGamblingTableVisits", _
                            ex.Message)
      _client_visits = 0
    End Try

    Return _client_visits

  End Function ' GetGamblingTableVisits

#End Region

#Region " Global Stock"

  ' PURPOSE: Deletes and resets global stock with count cage values
  '
  ' PARAMS:
  '   - INPUT: sql transaction
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - True if everything went OK. False if there was an error
  '
  ' NOTES:
  Private Function ReSetsGlobalStock(ByVal SqlTrx As SqlTransaction, ByVal Table As DataTable) As Boolean
    Dim _row As DataRow
    Dim _result As Boolean

    _result = True
    Try
      For Each _row In Table.Rows

        If GetRowValue(_row(DT_COLUMN_CURRENCY.QUANTITY)) > 0 _
          Or GetRowValue(_row(DT_COLUMN_CURRENCY.TOTAL)) > 0 Then ' If quantity or total are 0 , don't insert into BBDD

          If Not UpdateCageStock(SqlTrx, _row) Then
            _result = False
            Exit For
          End If

        End If

      Next

    Catch ex As Exception
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "cls_cage_control2", _
                            "ReSetsGlobalStock", _
                            ex.Message)
      _result = False
    End Try

    Return _result

  End Function ' ReSetsGlobalStock 

  ' PURPOSE: Update Cage Stock when a cancel sent/collection is done.
  '
  ' PARAMS:
  '   - INPUT: SqlTrx, Table
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - True if everything went OK. False if there was an error
  '
  ' NOTES:
  Private Function UpdateCageStockWhenCancel(ByVal SqlTrx As SqlTransaction) As Boolean
    Dim _row As DataRow
    Dim _is_cancel_movement As Boolean
    Dim _result As Boolean

    _is_cancel_movement = True
    _result = True

    Try

      ''MS 2017-10-05
      '' When a cage movement is cancelled after sending to a gaming table with a template
      If Me.OperationType = Cage.CageOperationType.ToGamingTable And Not Me.ClosingStocks Is Nothing Then

        For Each _row In Me.TableCurrencies.Select("QUANTITY > 0 OR TOTAL > 0 OR QUANTITY_DIFFERENCE > 0 OR TOTAL_DIFFERENCE > 0")

          If Not UpdateCageStock(SqlTrx, _row, _is_cancel_movement) Then
            _result = False
          End If

        Next
      Else

        For Each _row In Me.TableCurrencies.Select("QUANTITY > 0 OR TOTAL > 0")

          If Not UpdateCageStock(SqlTrx, _row, _is_cancel_movement) Then
            _result = False
          End If

        Next
      End If


    Catch ex As Exception
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "cls_cage_control2", _
                            "UpdateCageStockWhenCancel", _
                            ex.Message)
      _result = False
    End Try

    Return _result
  End Function ' UpdateCageStockWhenCancel

  ' PURPOSE: Updates global stock
  '
  ' PARAMS:
  '   - INPUT: sql transaction
  '            Datatable Row
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - True if everything went OK. False if there was an error
  '
  ' NOTES:
  Private Function UpdateCageStock(ByVal SqlTrx As SqlTransaction, ByVal _row As DataRow, Optional ByVal IsCancellMovement As Boolean = False) As Boolean
    Dim _str_query As String
    Dim _quantity As Decimal
    Dim _cashier_quantity As Decimal
    Dim _result As Boolean
    Dim _num_rows_afected As Int32

    _result = True
    IsStockError = False
    _str_query = String.Empty

    Try
      _str_query = WSI.Common.GamingHall.CageStock.GetSqlCommandUpdateCageStock(FeatureChips.IsCurrencyExchangeTypeChips(_row(DT_COLUMN_CURRENCY.CURRENCY_TYPE)), _
                                                                                _row(DT_COLUMN_CURRENCY.DENOMINATION), _
                                                                                Me.OperationType, _
                                                                                IsCancellMovement)

      Using _cmd As New SqlClient.SqlCommand(_str_query, SqlTrx.Connection, SqlTrx)
        _cmd.Parameters.Add("@pISOCode", SqlDbType.NVarChar, 3).Value = _row(DT_COLUMN_CURRENCY.ISO_CODE).ToString()
        _cmd.Parameters.Add("@pISOType", SqlDbType.Int).Value = _row(DT_COLUMN_CURRENCY.CURRENCY_TYPE)
        _cmd.Parameters.Add("@pDenomination", SqlDbType.Money).Value = _row(DT_COLUMN_CURRENCY.DENOMINATION)
        _cmd.Parameters.Add("@pColor", SqlDbType.Int).Value = _row(DT_COLUMN_CURRENCY.CHIP_COLOR)
        _cmd.Parameters.Add("@pName", SqlDbType.NVarChar, 50).Value = _row(DT_COLUMN_CURRENCY.CHIP_NAME).ToString()
        _cmd.Parameters.Add("@pDrawing", SqlDbType.NVarChar, 50).Value = _row(DT_COLUMN_CURRENCY.CHIP_DRAWING).ToString()
        _cmd.Parameters.Add("@pChipId", SqlDbType.BigInt).Value = IIf(_row(DT_COLUMN_CURRENCY.CHIP_ID) > 0, _row(DT_COLUMN_CURRENCY.CHIP_ID), DBNull.Value)

        If _row(DT_COLUMN_CURRENCY.CURRENCY_TYPE) = CageCurrencyType.Others Then
          _cmd.Parameters.Add("@pCurrencyType", SqlDbType.Int).Value = CInt(CageCurrencyType.Bill)
        Else
          _cmd.Parameters.Add("@pCurrencyType", SqlDbType.Int).Value = _row(DT_COLUMN_CURRENCY.CURRENCY_TYPE).ToString()
        End If

        ''MS 2017-10-05
        '' When a cage movement is cancelled after sending to a gaming table with a template
        If Me.OperationType = Cage.CageOperationType.ToGamingTable And Not Me.ClosingStocks Is Nothing Then
          _quantity = GetQuantity(_row)
          _cashier_quantity = GetRowValue(_row(DT_COLUMN_CURRENCY.CASHIER_QUANTITY))
        ElseIf _row(DT_COLUMN_CURRENCY.DENOMINATION) < 0 AndAlso _row(DT_COLUMN_CURRENCY.DENOMINATION) <> Cage.TICKETS_CODE Then
          _quantity = Me.GetRowValue(_row(DT_COLUMN_CURRENCY.TOTAL))
          _cashier_quantity = Me.GetRowValue(_row(DT_COLUMN_CURRENCY.CASHIER_TOTAL))
        Else
          _quantity = Me.GetRowValue(_row(DT_COLUMN_CURRENCY.QUANTITY))
          _cashier_quantity = GetRowValue(_row(DT_COLUMN_CURRENCY.CASHIER_QUANTITY))
        End If

        Select Case Me.OperationType
          Case Cage.CageOperationType.ToGamingTable _
             , Cage.CageOperationType.ToCustom _
             , Cage.CageOperationType.ToCashier
            ' Transfer Canceled
            If Not IsCancellMovement Then
              _quantity = _quantity * -1
            End If

          Case Cage.CageOperationType.FromGamingTable _
             , Cage.CageOperationType.FromGamingTableClosing _
             , Cage.CageOperationType.FromCashier _
             , Cage.CageOperationType.FromCashierClosing _
             , Cage.CageOperationType.FromTerminal _
             , Cage.CageOperationType.FromCustom _
             , Cage.CageOperationType.FromGamingTableDropbox _
             , Cage.CageOperationType.FromGamingTableDropboxWithExpected
            ' Collection Canceled
            If IsCancellMovement Then
              _quantity = _quantity * -1
            End If

        End Select
        'DLL11111
        If Not FeatureChips.IsCurrencyExchangeTypeChips(_row(DT_COLUMN_CURRENCY.CURRENCY_TYPE)) Then
          _cmd.Parameters.Add("@pQuantity", SqlDbType.Money).Value = _quantity
        Else
          _cmd.Parameters.Add("@pQuantity", SqlDbType.Int).Value = CInt(_quantity)
        End If

        If Me.OperationType = Cage.CageOperationType.CountCage And Not IsCancellMovement Then
          If _row(DT_COLUMN_CURRENCY.ISO_CODE).ToString() <> Cage.CHIPS_ISO_CODE And _row(DT_COLUMN_CURRENCY.ISO_CODE).ToString() <> Cage.CHIPS_COLOR Then
            _cmd.Parameters.Add("@pCashierQuantity", SqlDbType.Money).Value = _cashier_quantity
          Else
            _cmd.Parameters.Add("@pCashierQuantity", SqlDbType.Int).Value = CInt(_cashier_quantity)
          End If
        Else
          _cmd.Parameters.Add("@pCashierQuantity", SqlDbType.Int).Value = GetRowValue(_row(DT_COLUMN_CURRENCY.CASHIER_TOTAL))
        End If

        _num_rows_afected = _cmd.ExecuteNonQuery()

        If _num_rows_afected <> 1 Then

          If Me.OperationType = Cage.CageOperationType.CountCage And Not IsCancellMovement Then
            IsStockError = True
          End If
          _result = False

        Else

          'Update Stock values in local grid.
          If Me.OperationType = Cage.CageOperationType.CountCage And Not IsCancellMovement Then
            If _row(DT_COLUMN_CURRENCY.DENOMINATION) < 0 AndAlso _row(DT_COLUMN_CURRENCY.DENOMINATION) <> Cage.TICKETS_CODE Then
              _row(DT_COLUMN_CURRENCY.CASHIER_TOTAL) = _quantity
            Else
              _row(DT_COLUMN_CURRENCY.CASHIER_QUANTITY) = _quantity
            End If

          End If

        End If
      End Using

    Catch ex As Exception
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "cls_cage_control2", _
                            "UpdateCageStock", _
                            ex.Message)
      _result = False
    End Try

    Return _result

  End Function ' UpdateCageStock

  ' PURPOSE: Gets global stock
  '
  ' PARAMS:
  '
  ' RETURNS:
  '     - true if everything went ok
  '
  ' NOTES:
  Public Function GetGlobalCageStock(ByVal UseTableCurrencies As Boolean, Optional ByVal CashierColumns As Boolean = True _
                                    , Optional ByVal FromStock As Boolean = False, Optional ByVal FromCageStock As Boolean = False) As Boolean
    Dim _result As Boolean
    Dim _is_cage_count As Boolean

    _result = True

    Try

      If FromStock Then
        Me.OperationType = Cage.CageOperationType.FromCashier
      End If

      _is_cage_count = (Me.OperationType = Cage.CageOperationType.CountCage)

      Call Me.InitCurrencyData(_is_cage_count, UseTableCurrencies, , FromCageStock)

      Dim _sql_cmd As SqlCommand

      _sql_cmd = New SqlCommand("CageCurrentStock")
      _sql_cmd.CommandType = CommandType.StoredProcedure

      LoadedStock = GUI_GetTableUsingCommand(_sql_cmd, Integer.MaxValue)

      If LoadedStock Is Nothing OrElse LoadedStock.Rows.Count = 0 OrElse Me.OpenMode = OPEN_MODE.TEMPLATE_MAINTENANCE Then
        _result = False
      Else
        Call SetGlobalCageValues(LoadedStock, UseTableCurrencies, CashierColumns)
      End If

    Catch ex As Exception
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "cls_cage_control2", _
                            "GetGlobalCageStock", _
                            ex.Message)
      _result = False
    End Try

    Return _result
  End Function ' GetGlobalCageStock

  ' PURPOSE: Checks if stock has changed
  '
  ' PARAMS:
  '
  ' RETURNS:
  '     - true if everything went ok
  '
  ' NOTES:
  Public Function CheckChangeStock(OldStock As DataTable, NewStock As DataTable) As Boolean

    Dim _row As DataRow
    Dim _rows As DataRow()
    Dim _query As String
    Dim _filter As String

    Dim _iso_code As String
    Dim _qty As Decimal  ' When denomination < 0, qty can have decimals
    Dim _denomimation As Decimal
    Dim _iso_type As Integer
    Dim _set_id As Integer
    Dim _chip_id As Int64

    _query = "(     CGS_ISO_CODE = '{0}' AND CGS_CAGE_CURRENCY_TYPE = {1} AND CGS_DENOMINATION = {2} AND CGS_QUANTITY = {3} " & _
             " AND (CGS_CAGE_CURRENCY_TYPE NOT IN ({4}; {5}; {6}) or (SET_ID = {7} AND CGS_CHIP_ID = {8}))                 )"


    For Each _row In OldStock.Rows

      Try

        _iso_code = _row.Item("CGS_ISO_CODE")
        _iso_type = _row.Item("CGS_CAGE_CURRENCY_TYPE")
        _denomimation = _row.Item("CGS_DENOMINATION")
        _qty = _row.Item("CGS_QUANTITY")

        _set_id = _row.Item("SET_ID")
        _chip_id = _row.Item("CGS_CHIP_ID")

        _filter = String.Format(_query, _iso_code, _iso_type, _denomimation, _qty _
                                , CInt(CageCurrencyType.ChipsRedimible), CInt(CageCurrencyType.ChipsNoRedimible), CInt(CageCurrencyType.ChipsColor) _
                                , _set_id, _chip_id)
        If System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyGroupSeparator <> "" Then
          _filter = Replace(_filter, System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyGroupSeparator, "")
        End If
        If System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyDecimalSeparator <> "." Then
          _filter = Replace(_filter, System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyDecimalSeparator, ".")
        End If

        _filter = Replace(_filter, ";", ",")
        _rows = NewStock.Select(_filter)

        If _rows.Length <> 1 Then
          Return False
        End If
      Catch ex As Exception
        Log.Exception(ex)
      End Try
    Next

    Return True

  End Function

  ' PURPOSE: sets theoric values
  '
  ' PARAMS:
  '
  ' RETURNS:
  '     - true if ok
  '
  ' NOTES:
  Public Sub SetGlobalCageValues(ByRef TheoricTable As DataTable, ByVal UseTableCurrencies As Boolean, Optional ByVal CashierColumns As Boolean = True)
    Dim _row As DataRow
    Dim _iso_code As String
    Dim _quantity As Decimal
    Dim _denomination As Decimal
    Dim _dataview As DataView
    Dim _currency_type As CageCurrencyType
    Dim _chip_id As Int64
    Dim _set_id As Int64
    Dim _color As Int64
    Dim _name As String
    Dim _drawing As String

    For Each _row In TheoricTable.Rows

      _iso_code = _row.Item("CGS_ISO_CODE")
      _quantity = _row.Item("CGS_QUANTITY")
      _denomination = _row.Item("CGS_DENOMINATION")
      _currency_type = _row.Item("CGS_CAGE_CURRENCY_TYPE")
      _chip_id = _row.Item("CGS_CHIP_ID")
      _set_id = _row.Item("SET_ID")
      _color = _row.Item("COLOR")
      _name = _row.Item("NAME").ToString()
      _drawing = _row.Item("DRAWING").ToString()

      Call TheoricTableUpdateGlobalStock(_iso_code, _denomination, _quantity, UseTableCurrencies, CashierColumns, _currency_type, _chip_id, _set_id, _color, _name, _drawing)

    Next

    If UseTableCurrencies Then
      _dataview = Me.TableCurrencies.DefaultView
      _dataview.Sort = "ISO_CODE ASC, TYPE ASC, CHIP_SET_NAME ASC, DENOMINATION DESC"
      Me.TableCurrencies = _dataview.ToTable()
      AddExpectedColumnInCurrencies()
      Call DeleteZerosFromDT()
    Else
      _dataview = Me.TableTheoricValues.DefaultView
      _dataview.Sort = "ISO_CODE ASC, TYPE ASC, CHIP_SET_NAME ASC, DENOMINATION DESC"
      Me.TableTheoricValues = _dataview.ToTable()
    End If

  End Sub ' SetTheoricValues

  ' PURPOSE: updates values
  '
  ' PARAMS:
  '
  ' RETURNS:
  '
  ' NOTES:
  Private Sub TheoricTableUpdateGlobalStock(ByVal IsoCode As String, ByVal Denomination As Decimal, ByVal Quantity As Decimal _
                                             , ByVal UseTableCurrencies As Boolean, ByVal CashierColumns As Boolean, ByVal CurrencyType As CageCurrencyType, ByVal ChipId As Int64 _
                                             , ByVal ChipSetId As Int64, ByVal Color As Int64, ByVal Name As String, ByVal Drawing As String)
    Dim _rows_money As DataRow()
    Dim _row_money As DataRow
    Dim _quantity_column As Integer
    Dim _quantity_difference_column As Integer
    Dim _total_column As Integer
    Dim _total_difference_column As Integer
    Dim _str_filter As String

    _row_money = Nothing

    If CashierColumns Then
      _quantity_column = DT_COLUMN_CURRENCY.CASHIER_QUANTITY
      _quantity_difference_column = DT_COLUMN_CURRENCY.QUANTITY_DIFFERENCE
      _total_column = DT_COLUMN_CURRENCY.CASHIER_TOTAL
      _total_difference_column = DT_COLUMN_CURRENCY.TOTAL_DIFFERENCE
    Else
      _quantity_column = DT_COLUMN_CURRENCY.QUANTITY
      _total_column = DT_COLUMN_CURRENCY.TOTAL
    End If

    If CurrencyType <> CageCurrencyType.ChipsRedimible And CurrencyType <> CageCurrencyType.ChipsNoRedimible And CurrencyType <> CageCurrencyType.ChipsColor Then
      _str_filter = String.Format("ISO_CODE = '{0}' AND DENOMINATION = {1} AND TYPE = {2}", IsoCode, GUI_LocalNumberToDBNumber(Denomination.ToString), CInt(CurrencyType))
    Else
      _str_filter = String.Format("ISO_CODE = '{0}' AND CHIP_ID = {1} AND TYPE = {2}", IsoCode, ChipId, CInt(CurrencyType))
    End If

    If UseTableCurrencies Then
      _rows_money = Me.TableCurrencies.Select(_str_filter)
    Else
      _rows_money = Me.TableTheoricValues.Select(_str_filter)
    End If

    'JCA En este caso tengo Stock de una denominaci�n que ya no existe, debo a�adir el registro en la tabla.
    If _rows_money.Length = 0 Then
      If Quantity > 0 Then
        _row_money = InitializeNewRow(IsoCode, Denomination, UseTableCurrencies, CurrencyType, ChipId, ChipSetId)
        If FeatureChips.IsChipsType(CurrencyType) Then
          _row_money(DT_COLUMN_CURRENCY.CHIP_COLOR) = Color
          _row_money(DT_COLUMN_CURRENCY.CHIP_DRAWING) = Drawing
          _row_money(DT_COLUMN_CURRENCY.CHIP_NAME) = Name
          _row_money(DT_COLUMN_CURRENCY.CHIP_SET_ID) = ChipSetId
          _row_money(DT_COLUMN_CURRENCY.CHIP_SET_NAME) = " "
        End If
      End If
    Else
      _row_money = _rows_money(0)
    End If

    If _row_money IsNot Nothing Then
      If Denomination < 0 Then
        Select Case Denomination
          Case -200
            _row_money.Item(_quantity_column) = Quantity
            '_row_money.Item(_total_column) = 0
            If CashierColumns Then
              _row_money.Item(_quantity_difference_column) = -Quantity
              '_row_money.Item(_total_difference_column) = 0
            End If

          Case Else
            _row_money.Item(_quantity_column) = IIf(Quantity <> 0, 1, 0)
            _row_money.Item(_total_column) = Quantity
            If CashierColumns Then
              _row_money.Item(_quantity_difference_column) = IIf(Quantity <> 0, -1, 0)
              _row_money.Item(_total_difference_column) = -Quantity
            End If

        End Select
      Else
        _row_money.Item(_quantity_column) = Quantity
        If CurrencyType = CageCurrencyType.ChipsColor Then
          _row_money.Item(_total_column) = Quantity
        Else
          _row_money.Item(_total_column) = Denomination * Quantity
        End If

        If CashierColumns Then
          _row_money.Item(_quantity_difference_column) = -Quantity
          If CurrencyType = CageCurrencyType.ChipsColor Then
            _row_money.Item(_total_difference_column) = -Quantity
          Else
            _row_money.Item(_total_difference_column) = -(Denomination * Quantity)
          End If
        End If
      End If
    End If

  End Sub ' TheoricTableUpdateGlobalStock

  ' PURPOSE: initializes values for a new row in tablecurrencies/tabletheoricvalues
  '
  ' PARAMS:
  '
  ' RETURNS:
  '
  ' NOTES:
  Private Function InitializeNewRow(ByVal IsoCode As String, _
                                    ByVal Denomination As Decimal, _
                                    ByVal UseTableCurrencies As Boolean, _
                                    ByVal CurrencyType As CageCurrencyType, _
                                    ByVal ChipId As Int64, _
                                    ByVal ChipSetId As Int64) As DataRow

    Dim _row_money As DataRow

    If UseTableCurrencies Then
      _row_money = Me.TableCurrencies.NewRow()
    Else
      _row_money = Me.TableTheoricValues.NewRow()
    End If

    _row_money(DT_COLUMN_CURRENCY.ISO_CODE) = IsoCode
    _row_money(DT_COLUMN_CURRENCY.DENOMINATION) = CDec(Denomination)
    _row_money(DT_COLUMN_CURRENCY.CURRENCY_TYPE) = CurrencyType
    _row_money(DT_COLUMN_CURRENCY.CHIP_SET_ID) = ChipSetId
    _row_money(DT_COLUMN_CURRENCY.CHIP_ID) = ChipId
    _row_money(DT_COLUMN_CURRENCY.CHIP_COLOR) = 0
    _row_money(DT_COLUMN_CURRENCY.CHIP_DRAWING) = ""
    _row_money(DT_COLUMN_CURRENCY.CHIP_NAME) = ""
    _row_money(DT_COLUMN_CURRENCY.CHIP_SET_ID) = 0
    _row_money(DT_COLUMN_CURRENCY.CHIP_SET_NAME) = ""

    If Denomination < 0 Then
      _row_money(DT_COLUMN_CURRENCY.DENOMINATION_WITH_SIMBOL) = Me.GetCurrencyDescription(IsoCode, Denomination)
    Else
      _row_money(DT_COLUMN_CURRENCY.DENOMINATION_WITH_SIMBOL) = Denomination
    End If

    _row_money(DT_COLUMN_CURRENCY.TOTAL) = 0
    _row_money(DT_COLUMN_CURRENCY.TOTAL_WITH_SIMBOL) = 0
    _row_money(DT_COLUMN_CURRENCY.CASHIER_QUANTITY) = 0
    _row_money(DT_COLUMN_CURRENCY.CASHIER_TOTAL) = 0
    _row_money(DT_COLUMN_CURRENCY.QUANTITY_DIFFERENCE) = 0
    _row_money(DT_COLUMN_CURRENCY.TOTAL_DIFFERENCE) = 0

    If UseTableCurrencies Then
      Me.TableCurrencies.Rows.Add(_row_money)
    Else
      Me.TableTheoricValues.Rows.Add(_row_money)
    End If

    Return _row_money

  End Function ' InitializeNewRow

#End Region ' Global Stock

End Class


