'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_lcd_configuration.vb
' DESCRIPTION:   LCD Display configuration
' AUTHOR:        Jes�s �ngel Blanco
' CREATION DATE: 22-JUL-2014
'
' REVISION HISTORY:
'
' Date         Author      Description
' -----------  ---------   ------------------------------------------
' 22-JUL-2012  JAB         Initial version.
' 15-OCT-2014  JAB         Fixed Bug #1513: Errors in the LCD Display configuration screen.
' 15-OCT-2014  JAB         Fixed Bug #1292: Inconsistencies in the LCD Display configuration screen.
' 04-NOV-2014  JCA         Add funcionality: Notify awarded Site jackpot and Allow Changes 
' -------------------------------------------------------------------

Option Explicit On

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports WSI.Common
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports System.Text
Imports System.IO

Public Class CLS_LCD_CONFIGURATION
  Inherits CLASS_BASE

#Region " Enums "

  Public Enum RESOURCE_TYPE
    IMAGE_LOGO = 0
    VIDEO_INTRO = 1
  End Enum

#End Region

#Region " Consts "

  Private Const GRID_COLUMN_FUNC_ID As Integer = 0
  Private Const GRID_COLUMN_FUNC_NAME As Integer = 1
  Private Const GRID_COLUMN_FUNC_ENABLED As Integer = 2

#End Region

#Region " Members "

  Protected m_lcd_configuration As TYPE_LCD_CONFIGURATION

#End Region

#Region " Structures "

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_LCD_CONFIGURATION
    Public functionalities As DataTable
    Public image As LCD_Image
    Public video As LCD_Video

    '----------------------------------------------------------------------------
    ' PURPOSE : Constructor.
    '
    ' PARAMS :
    '     - INPUT :
    '     - OUTPUT :
    '
    ' RETURNS :
    '
    ' NOTES :
    '
    Sub New()
      Me.functionalities = New DataTable("lcd_functionalities")
      Me.image = New LCD_Image
      Me.video = New LCD_Video

    End Sub ' New

  End Class ' TYPE_LCD_CONFIGURATION

  <StructLayout(LayoutKind.Sequential)> _
  Public Class LCD_Image
    Implements IEquatable(Of LCD_Image)

    Public resource_type_id As Integer

    Public resource_id As Long
    Public image_cached As Image
    Public image_length As Int64
    Public image_has_changes As Boolean = False
    Public image_deleted As Boolean = False
    Public image_name As String = ""

    '----------------------------------------------------------------------------
    ' PURPOSE : Check if two objects are the same ( has the same ID field ) 
    '           It's mandatory override this function to List.IndexOf() works right
    '
    ' PARAMS :
    '     - INPUT : Other object that same type
    '
    '     - OUTPUT :
    '
    ' RETURNS :
    '     - True or False 
    '
    ' NOTES :
    ' It's mandatory implement Equals() to be able to use List.Contains() and List.IndexOf() functions
    '
    Public Overloads Function Equals(ByVal target As LCD_Image) As Boolean Implements IEquatable(Of LCD_Image).Equals
      If target.resource_type_id = Me.resource_type_id Then
        Return True
      Else
        Return False
      End If
    End Function

  End Class

  <StructLayout(LayoutKind.Sequential)> _
  Public Class LCD_Video
    Implements IEquatable(Of LCD_Video)

    Public resource_type_id As Integer

    Public resource_id As Long
    Public video_cached As IO.MemoryStream
    Public video_length As Int64
    Public video_has_changes As Boolean = False
    Public video_deleted As Boolean = False
    Public video_extension As String = ""
    Public video_name As String = ""

    Public Function Equals_LCDVideo(ByVal target As LCD_Video) As Boolean Implements System.IEquatable(Of LCD_Video).Equals
      If target.resource_type_id = Me.resource_type_id Then
        Return True
      Else
        Return False
      End If
    End Function
  End Class

#End Region

#Region " Properties "

  Public Property Functionalities() As DataTable
    Get
      Return Me.m_lcd_configuration.functionalities
    End Get
    Set(ByVal value As DataTable)
      Me.m_lcd_configuration.functionalities = value
    End Set
  End Property ' Functionalities

  Public Property Image() As LCD_Image
    Get
      Return Me.m_lcd_configuration.image
    End Get
    Set(ByVal value As LCD_Image)
      Me.m_lcd_configuration.image = value
    End Set
  End Property ' Image

  Public Property Video() As LCD_Video
    Get
      Return Me.m_lcd_configuration.video
    End Get
    Set(ByVal value As LCD_Video)
      Me.m_lcd_configuration.video = value
    End Set
  End Property ' Video

#End Region

#Region "Overrrides"

  '----------------------------------------------------------------------------
  ' PURPOSE : Returns the related auditor data for the current object.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - The newly created audit object
  '
  ' NOTES :
  '
  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA
    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _str_value As String
    Dim _title As String
    Dim _image_size As String
    Dim _video_size As String

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_TERMINALS)
    _str_value = ""
    _title = ""
    _image_size = ""
    _video_size = ""

    Call _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(5156), "") ' LKT-LCD Configuraci�n

    For Each _funct As DataRow In Me.Functionalities.Rows

      _title = _funct(GRID_COLUMN_FUNC_NAME)
      _str_value = _funct(GRID_COLUMN_FUNC_ENABLED)
      Call _auditor_data.SetField(0, _str_value, _title)

    Next

    ' Image
    If Not Me.Image.image_cached Is Nothing Then
      _image_size = " (" & GUI_FormatNumber(Me.Image.image_length, 0, ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE) & " bytes)"
    End If
    _str_value = IIf(String.IsNullOrEmpty(Me.Image.image_name), GLB_NLS_GUI_PLAYER_TRACKING.GetString(5219), Me.Image.image_name & _image_size)
    _title = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5192) ' Cambio de logotipo
    Call _auditor_data.SetField(0, _str_value, _title)

    ' Video
    If Not Me.Video.video_cached Is Nothing Then
      _video_size = " (" & GUI_FormatNumber(Me.Video.video_cached.Length, 0, ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE) & " bytes)"
    End If
    _str_value = IIf(String.IsNullOrEmpty(Me.Video.video_name), GLB_NLS_GUI_PLAYER_TRACKING.GetString(5219), Me.Video.video_name & _video_size)
    _title = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5195) ' Cambio de video
    Call _auditor_data.SetField(0, _str_value, _title)

    Return _auditor_data
  End Function ' AuditorData

  '----------------------------------------------------------------------------
  ' PURPOSE : Deletes the given object from the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DEPENDENCIES
  '
  ' NOTES :
  '
  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

  End Function ' DB_Delete

  '----------------------------------------------------------------------------
  ' PURPOSE : Inserts the given object to the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES :
  '
  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

  End Function ' DB_Insert

  '----------------------------------------------------------------------------
  ' PURPOSE : Reads from the database into the given object
  '
  ' PARAMS :
  '     - INPUT :
  '         - ObjectId: An object, it must be 'casted' to the desired KEY.
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '
  ' NOTES :
  '
  Public Overrides Function DB_Read(ByVal ObjectId As Object, _
                                    ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As Integer
    ' Funci�n que devuelva el estatus y que almacene en la variable que se le pasa por referencia
    ' el contenido obtenido en el Read
    _rc = GetConfigurationData(Me.m_lcd_configuration)

    Select Case _rc

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Read

  '----------------------------------------------------------------------------
  ' PURPOSE : Updates the given object to the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES :
  '
  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _rc As Integer

    _rc = Me.UpdateConfigurationDataTable(Me.m_lcd_configuration)

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
        Return ENUM_STATUS.STATUS_DUPLICATE_KEY

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select ' DB_Update
  End Function ' DB_Update

  '----------------------------------------------------------------------------
  ' PURPOSE : Returns a duplicate of the given object.
  '
  ' PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - The newly created object
  '
  ' NOTES :
  '
  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE
    Dim _target As CLS_LCD_CONFIGURATION

    _target = New CLS_LCD_CONFIGURATION()

    ' Duplicate functionalities
    _target.Functionalities = Me.Functionalities.Copy()

    ' Duplicate image logo
    _target.Image = New LCD_Image()
    _target.Image.image_cached = Me.Image.image_cached
    _target.Image.image_length = Me.Image.image_length
    _target.Image.image_deleted = Me.Image.image_deleted
    _target.Image.image_has_changes = Me.Image.image_has_changes
    _target.Image.resource_type_id = Me.Image.resource_type_id
    _target.Image.resource_id = Me.Image.resource_id
    _target.Image.image_name = Me.Image.image_name

    ' Duplicate intro video
    _target.Video = New LCD_Video()
    _target.Video.video_cached = Me.Video.video_cached
    _target.Video.video_length = Me.Video.video_length
    _target.Video.video_deleted = Me.Video.video_deleted
    _target.Video.video_has_changes = Me.Video.video_has_changes
    _target.Video.resource_type_id = Me.Video.resource_type_id
    _target.Video.resource_id = Me.Video.resource_id
    _target.Video.video_extension = Me.Video.video_extension
    _target.Video.video_name = Me.Video.video_name

    Return _target
  End Function ' Duplicate

#End Region

#Region " Public "

  ' PURPOSE: New
  '                  
  '  PARAMS:         
  '     - INPUT:     
  '           - None
  '     - OUTPUT:    
  '           - None
  '                  
  ' RETURNS:         
  '     -None
  '                  
  Public Sub New()
    Me.m_lcd_configuration = New TYPE_LCD_CONFIGURATION()
  End Sub ' New

  ' PURPOSE: Get functionality name
  '                  
  '  PARAMS:         
  '     - INPUT:     
  '           - FunctionalityId: Int32
  '     - OUTPUT:    
  '           - None
  '                  
  ' RETURNS:         
  '     -_result: String
  '                  
  Public Function GetFunctionalityName(ByVal FunctionalityId As Int32) As String
    Dim _result As String

    _result = ""

    Select Case FunctionalityId
      Case TYPE_LCD_FUNCTIONALITIES.ACCOUNT_INFO
        _result = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5630) ' Account Info

      Case TYPE_LCD_FUNCTIONALITIES.RAFFLES
        _result = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5631) ' Raffles

      Case TYPE_LCD_FUNCTIONALITIES.PROMOTIONS
        _result = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5632) ' Promotions

      Case TYPE_LCD_FUNCTIONALITIES.JACKPOT
        _result = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5633) ' Casino Jackpot

      Case TYPE_LCD_FUNCTIONALITIES.LOYALTY_CLUB
        _result = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5634) ' Loyalty Club

      Case TYPE_LCD_FUNCTIONALITIES.EXCHANGES
        _result = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5696) ' Allow Changes

      Case TYPE_LCD_FUNCTIONALITIES.NOTIFY_PASSWORD
        _result = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5697) ' Notify awarded Site jackpot

      Case TYPE_LCD_FUNCTIONALITIES.GAMEGATEWAY
        _result = GeneralParam.GetString("GameGateway", "Name", "GameGateway")

      Case TYPE_LCD_FUNCTIONALITIES.FB
        _result = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7760) ' Food&Beverage

    End Select

    Return _result
  End Function ' GetFunctionalityName

#End Region

#Region " Private "

  ' PURPOSE: Mark all rows in datable as Modifies
  '                  
  '  PARAMS:         
  '     - INPUT:     
  '           - 
  '     - OUTPUT:    
  '           -  table As DataTable, 
  '                  
  ' RETURNS:         
  '     -None
  '                  
  Private Sub MarkAsModified(ByRef table As DataTable)
    Dim _row As DataRow
    table.AcceptChanges()

    For Each _row In table.Rows
      _row.SetModified()
    Next

  End Sub 'MarkAsModified

  '----------------------------------------------------------------------------
  ' PURPOSE : Fill TPYE_ADS_CONFIGURATION with database rows.
  '
  ' PARAMS :
  '     - INPUT : An instance of TYPE_ADS_CONFIGURATION object
  '
  '     - OUTPUT : An instance of TYPE_ADS_CONFIGURATION with rows filled
  '
  ' RETURNS :
  '     - ENUM_CONFIGURATION_RC
  '
  ' NOTES :
  '
  Private Function GetConfigurationData(ByRef lcd_configuration As CLS_LCD_CONFIGURATION.TYPE_LCD_CONFIGURATION) As Integer

    Dim _rc As Integer
    _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_OK

    Try

      lcd_configuration.functionalities = GetFunctionalities()
      'lcd_configuration.image = GetImageLogo()

    Catch _ex As Exception
      _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, "cls_lcd_configuration", "GetConfigurationData", _ex.Message)
    End Try

    Return _rc

  End Function ' GetConfigurationData

  ' PURPOSE: GetFunctionalities
  '                  
  '  PARAMS:         
  '     - INPUT:     
  '           - None
  '     - OUTPUT:    
  '           - None
  '                  
  ' RETURNS:         
  '     - List(Of TYPE_FUNCTIONALITIES)
  '                  
  Private Function GetFunctionalities() As DataTable

    Dim _datatable As DataTable
    Dim _sql_command As SqlCommand
    Dim _sb As StringBuilder

    _sb = New StringBuilder()
    _datatable = New DataTable

    _sb.AppendLine("SELECT   FUN_FUNCTION_ID      ")
    _sb.AppendLine("       , FUN_NAME             ")
    _sb.AppendLine("       , FUN_ENABLED          ")
    _sb.AppendLine("  FROM   LCD_FUNCTIONALITIES  ")
    _sb.AppendLine(GetSqlWhere())

    _sql_command = New SqlCommand(_sb.ToString())
    _datatable = GUI_GetTableUsingCommand(_sql_command, 5000)

    Return _datatable

  End Function ' GetFunctionalities

  ' PURPOSE: UpdateConfigurationDataTable
  '                  
  '  PARAMS:         
  '     - INPUT:     
  '           - None
  '     - OUTPUT:    
  '           - None
  '                  
  ' RETURNS:         
  '     - Integer
  '                  
  Private Function UpdateConfigurationDataTable(ByRef LocalData As TYPE_LCD_CONFIGURATION) As Integer
    Dim _do_commit As Boolean
    Dim _sql_data_adapter As SqlDataAdapter
    Dim _sql_transaction As System.Data.SqlClient.SqlTransaction
    Dim _sql_command As SqlCommand
    Dim _dt As DataTable
    Dim _rows_affected As Integer
    Dim _row As DataRow
    Dim _rc As Integer
    Dim _img As CLS_LCD_CONFIGURATION.LCD_Image
    Dim _vid As CLS_LCD_CONFIGURATION.LCD_Video
    Dim _ms As MemoryStream
    Dim _img_len As Int32
    Dim _vid_len As Int32

    _do_commit = False
    _sql_data_adapter = New SqlDataAdapter()
    _sql_transaction = Nothing
    _rc = 0

    Try

      If Not GUI_BeginSQLTransaction(_sql_transaction) Then
        _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        Return _rc
      End If

      ' UPDATE FUNCTIONALITIES
      _sql_data_adapter = New SqlDataAdapter()
      _dt = Me.GetFunctionalitiesSchemma(_sql_transaction)
      _sql_data_adapter.UpdateCommand = SetUpdateFunctionalities()          ' SET SQL UPDATE COMMAND
      _sql_data_adapter.UpdateCommand.Connection = _sql_transaction.Connection
      _sql_data_adapter.UpdateCommand.Transaction = _sql_transaction

      ' FILL _dt
      For Each _currrent_row As DataRow In Me.Functionalities.Rows
        _row = _dt.NewRow()
        _row("fun_function_id") = _currrent_row(GRID_COLUMN_FUNC_ID)
        _row("fun_name") = _currrent_row(GRID_COLUMN_FUNC_NAME)
        _row("fun_enabled") = _currrent_row(GRID_COLUMN_FUNC_ENABLED)
        _dt.Rows.Add(_row)
      Next

      Call MarkAsModified(_dt)
      _rows_affected = _sql_data_adapter.Update(_dt)

      If (_rows_affected = 0) Then
        _do_commit = False
        _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

        Return _rc
      End If

      ' Update image changes
      _img = Me.Image
      If _img.image_has_changes Then
        If _img.resource_id = 0 Then
          If Not _img.image_deleted Then
            ' INSERT
            Dim _new_image_resid As Nullable(Of Long)
            _new_image_resid = New Nullable(Of Long)
            SaveImageLCD(_new_image_resid, _img.image_cached, _sql_transaction)
            _img.resource_id = _new_image_resid ' NEED TO BE UPDATED IN DB
          Else
            ' DELETE
            ' If picture has been deleted, set resource_id = 0 
            ' fmr_ads_configuration gets the default image from enum instead of wkt_images.
          End If

        Else ' Customed image has changes.
          SaveImageLCD(_img.resource_id, _img.image_cached, _sql_transaction)
        End If

        If _img.image_cached Is Nothing Then
          _img_len = 0
        Else
          _ms = New MemoryStream
          _img.image_cached.Save(_ms, _img.image_cached.RawFormat)
          _img_len = Convert.ToInt32(_ms.Position)
        End If

        _sql_command = SqlUpdateResource(_img.resource_type_id, _img.resource_id, _img_len, _img.image_name)
        _sql_command.Connection = _sql_transaction.Connection
        _sql_command.Transaction = _sql_transaction
        _rows_affected = _sql_command.ExecuteNonQuery()

        If _rows_affected = 0 And Not _img.image_has_changes Then
          _do_commit = False
          _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

          Return _rc
        End If

      End If

      ' Update video changes
      _vid = Me.Video
      If _vid.video_has_changes Then
        If _vid.resource_id = 0 Then
          If Not _vid.video_deleted Then
            ' INSERT
            Dim _new_video_resid As Nullable(Of Long)
            _new_video_resid = New Nullable(Of Long)
            SaveVideoLCD(_new_video_resid, _vid.video_cached, _vid.video_extension, _sql_transaction)
            _vid.resource_id = _new_video_resid ' NEED TO BE UPDATED IN DB

          Else
            ' DELETE
            ' If picture has been deleted, set resource_id = 0 
            ' fmr_ads_configuration gets the default image from enum instead of wkt_images.
            _vid.resource_id = 0
          End If

        Else ' Customed video has changes.
          SaveVideoLCD(_vid.resource_id, _vid.video_cached, _vid.video_extension, _sql_transaction)
        End If

        If _vid.video_cached Is Nothing Then
          _vid_len = 0
        Else
          _vid_len = Convert.ToInt32(_vid.video_cached.Length)
        End If

        _sql_command = SqlUpdateResource(_vid.resource_type_id, _vid.resource_id, _vid_len, _vid.video_name)
        _sql_command.Connection = _sql_transaction.Connection
        _sql_command.Transaction = _sql_transaction
        _rows_affected = _sql_command.ExecuteNonQuery()

        If _rows_affected = 0 And Not _vid.video_has_changes Then
          _do_commit = False
          _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

          Return _rc
        End If

      End If

      ' No error
      _do_commit = True

      Return _rc

    Catch ex As Exception
      _do_commit = False
      _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    Finally

      GUI_EndSQLTransaction(_sql_transaction, _do_commit)
    End Try

  End Function ' UpdateConfigurationDataTable

  ' PURPOSE: GetScheema
  '                  
  '  PARAMS:         
  '     - INPUT:     
  '           -  SqlCmd As SqlCommand, 
  '     - OUTPUT:    
  '           -  Trax As SqlTransaction, 
  '                  
  ' RETURNS:         
  '     - DataTable
  '                  
  Private Function GetSchema(ByVal SqlCmd As SqlCommand, ByRef Trax As SqlTransaction) As DataTable

    Dim _dtable As DataTable

    _dtable = New DataTable()
    SqlCmd.Transaction = Trax
    SqlCmd.Connection = Trax.Connection

    Using _sql_da As New SqlDataAdapter(SqlCmd)
      _sql_da.FillSchema(_dtable, SchemaType.Source)
    End Using

    Return _dtable

  End Function ' GetScheema

  ' PURPOSE: GetFunctionalitiesSchemma
  '                  
  '  PARAMS:         
  '     - INPUT:     
  '           - 
  '     - OUTPUT:    
  '           -  Trax As SqlTransaction, 
  '                  
  ' RETURNS:         
  '     - DataTable
  '                  
  Private Function GetFunctionalitiesSchemma(ByRef Trax As SqlTransaction) As DataTable

    Dim _str_bld As StringBuilder
    _str_bld = New StringBuilder()

    _str_bld.AppendLine("SELECT   FUN_FUNCTION_ID")
    _str_bld.AppendLine("       , FUN_NAME")
    _str_bld.AppendLine("       , FUN_ENABLED ")
    _str_bld.AppendLine("  FROM   WKT_FUNCTIONALITIES")

    Return GetSchema(New SqlCommand(_str_bld.ToString()), Trax)

  End Function ' GetFunctionalitiesSchemma

  ' PURPOSE: SqlUpdateFunctionalities
  '                  
  '  PARAMS:         
  '     - INPUT:     
  '           - None
  '     - OUTPUT:    
  '           - None
  '                  
  ' RETURNS:         
  '     - SqlCommand
  '                  
  Private Function SetUpdateFunctionalities() As SqlCommand

    Dim _sql_update_command As SqlCommand
    Dim _str_bld_update As StringBuilder
    _str_bld_update = New StringBuilder

    _str_bld_update.AppendLine("UPDATE   LCD_FUNCTIONALITIES    ")
    _str_bld_update.AppendLine("   SET   FUN_ENABLED =@pEnabled ")
    _str_bld_update.AppendLine(" WHERE   FUN_FUNCTION_ID=@pId   ")

    _sql_update_command = New SqlCommand(_str_bld_update.ToString())

    _sql_update_command.Parameters.Add("@pEnabled", SqlDbType.Bit, 1, "fun_enabled")
    _sql_update_command.Parameters.Add("@pId", SqlDbType.BigInt, 99, "fun_function_id")
    Return _sql_update_command
  End Function ' SqlUpdateFunctionalities

  ' PURPOSE: SqlUpdateImages
  '                  
  '  PARAMS:         
  '     - INPUT:     
  '           - None
  '     - OUTPUT:    
  '           - None
  '                  
  ' RETURNS:         
  '     - SqlCommand
  '                  
  Private Function SqlUpdateResource(ByVal ResourceTypeId As Long, ByVal ResourceId As Long, ByVal ResourceLength As Int32, Optional ByVal ResourceName As String = "") As SqlCommand
    Dim _sql_command As SqlCommand
    Dim _str_bld_update As StringBuilder
    _sql_command = New SqlCommand()
    _str_bld_update = New StringBuilder

    _str_bld_update.AppendLine("UPDATE   LCD_IMAGES                               ")
    _str_bld_update.AppendLine("   SET   CIM_RESOURCE_ID      = @pResourceId      ")
    _str_bld_update.AppendLine("       , CIM_RESOURCE_LENGTH  = @pResourceLength  ")
    _str_bld_update.AppendLine("       , CIM_RESOURCE_NAME    = @pResourceName    ")
    _str_bld_update.AppendLine(" WHERE   CIM_IMAGE_ID         = @pImageId         ")

    _sql_command = New SqlCommand(_str_bld_update.ToString())

    _sql_command.Parameters.Add("@pResourceId", SqlDbType.Int).Value = ResourceId
    _sql_command.Parameters.Add("@pResourceLength", SqlDbType.Int).Value = ResourceLength
    _sql_command.Parameters.Add("@pResourceName", SqlDbType.VarChar).Value = ResourceName
    _sql_command.Parameters.Add("@pImageId", SqlDbType.Int).Value = ResourceTypeId

    Return _sql_command
  End Function ' SqlUpdateImages

  ''' <summary>
  ''' Get SQL Where
  ''' </summary>
  ''' <remarks></remarks>
  Private Function GetSqlWhere() As String
    Dim _str_where As String

    _str_where = String.Empty

    If GeneralParam.GetInt32("GameGateway", "Enabled", 0) = 0 Then
      _str_where = String.Format("  FUN_FUNCTION_ID != {0} ", Int32.Parse(TYPE_LCD_FUNCTIONALITIES.GAMEGATEWAY))
    End If

    If GeneralParam.GetInt32("FB", "Enabled", 0) = 0 Then
      _str_where = _str_where & IIf(String.IsNullOrEmpty(_str_where), String.Empty, " AND ")
      _str_where = _str_where & String.Format("  FUN_FUNCTION_ID != {0} ", Int32.Parse(TYPE_LCD_FUNCTIONALITIES.FB))
    End If

    If Not String.IsNullOrEmpty(_str_where) Then
      _str_where = " WHERE " & _str_where
    End If

    Return _str_where
  End Function

#End Region


End Class
