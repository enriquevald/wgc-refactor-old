'-------------------------------------------------------------------
' Copyright � 2011 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_site_operator.vb
' DESCRIPTION:   Site Operator class for site operators edition
' AUTHOR:        Agust� Poch
' CREATION DATE: 04-FEB-2011
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 04-FEB-2011  APB    Initial version.
'
'-------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices

Public Class CLASS_SITE_OPERATOR
  Inherits CLASS_BASE

#Region " Constants "

  ' Fields length
  Public Const MAX_OPERATOR_NAME_LEN As Integer = 30
  Public Const MAX_SITE_NAME_LEN As Integer = 30
  Public Const INVALID_OPERATOR_ID As Integer = -1
  Public Const SITE_ARRAY_LENGTH As Integer = 100

#End Region ' Constants

#Region " GUI_Configuration.dll "

#Region " Structures "

  ' Related to SITE_OPERATORS
  <StructLayout(LayoutKind.Sequential)> _
   Public Class TYPE_SITE_OPERATOR
    Public control_block As Integer
    Public operator_id As Integer
    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_OPERATOR_NAME_LEN + 1)> _
    Public operator_name As String
    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=1)> _
    Public filler_0 As String

    Public Sub New()
      MyBase.New()
      control_block = Marshal.SizeOf(GetType(TYPE_SITE_OPERATOR))
    End Sub

  End Class ' TYPE_SITE_OPERATOR

  ' Related to GUI_PROFILE_FORMS and GUI_FORMS
  <StructLayout(LayoutKind.Sequential)> _
   Public Class TYPE_SITE_ITEM
    Public site_id As Integer
    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_SITE_NAME_LEN + 1)> _
    Public site_name As String
    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=1)> _
    Public filler_0 As String

  End Class ' TYPE_SITE_ITEM

  ' Class to pass fixed length arrays of sites
  <StructLayout(LayoutKind.Sequential)> _
   Public Class TYPE_SITES_BY_OPERATOR

    Public control_block As Integer
    Public operator_id As Integer
    Public site_list As New CLASS_VECTOR()

    Public Sub New(Optional ByVal NumberOfItems As Integer = SITE_ARRAY_LENGTH)
      MyBase.New()
      control_block = Marshal.SizeOf(GetType(TYPE_SITES_BY_OPERATOR))
      Call site_list.Init(GetType(TYPE_SITE_ITEM), NumberOfItems)
    End Sub

    Protected Overrides Sub Finalize()
      site_list.Done()
      MyBase.Finalize()
    End Sub

  End Class ' TYPE_SITES_BY_OPERATOR

#End Region ' Structures

#End Region ' GUI_Configuration.dll

#Region " Members "

  Protected m_site_operator As New TYPE_SITE_OPERATOR
  Protected m_site_list() As TYPE_SITE_ITEM

#End Region

#Region " Properties "

  Public Property OperatorId() As Integer
    Get
      Return m_site_operator.operator_id
    End Get
    Set(ByVal Value As Integer)
      m_site_operator.operator_id = Value
    End Set
  End Property

  Public Property OperatorName() As String
    Get
      Return m_site_operator.operator_name
    End Get
    Set(ByVal Value As String)
      m_site_operator.operator_name = Value
    End Set
  End Property

  Public Property SiteList() As TYPE_SITE_ITEM()
    Get
      Return m_site_list
    End Get
    Set(ByVal Value() As TYPE_SITE_ITEM)
      m_site_list = Value
    End Set
  End Property

  Public ReadOnly Property NumSites() As Integer
    Get
      If IsNothing(m_site_list) Then
        Return 0
      Else
        Return m_site_list.Length
      End If
    End Get
  End Property

#End Region ' Properties

#Region " Overrides functions "

  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim rc As Integer
    'Dim profile As New TYPE_SITES_BY_OPERATOR

    Me.OperatorId = ObjectId

    ' Read profile 
    rc = ReadOperator(m_site_operator, SqlCtx)

    ' Read profile forms array
    If rc = ENUM_CONFIGURATION_RC.CONFIGURATION_OK Then
      rc = DB_ReadSitesByOperator(Me.OperatorId, SqlCtx)
    End If

    Select Case rc

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Read

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim rc As Integer
    Dim site_operator As New TYPE_SITE_OPERATOR

    ' Set Information
    site_operator.operator_id = Me.OperatorId
    site_operator.operator_name = Me.OperatorName

    rc = UpdateOperator(site_operator, SqlCtx)

    Select Case rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
        Return ENUM_STATUS.STATUS_DUPLICATE_KEY

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Update

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim rc As Integer
    Dim site_operator As New TYPE_SITE_OPERATOR

    ' Set user Profile Information
    site_operator.operator_id = Me.OperatorId
    site_operator.operator_name = Me.OperatorName

    rc = InsertOperator(site_operator, SqlCtx)

    Select Case rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
        Return ENUM_STATUS.STATUS_DUPLICATE_KEY

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Insert

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim rc As Integer

    ' Delete profile data
    rc = DeleteOperator(Me.OperatorId, SqlCtx)

    Select Case rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Delete

  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim auditor_data As CLASS_AUDITOR_DATA

    ' Create a new auditor_code for sites/operators manipulation
    auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_SITES_OPERATORS)
    Call auditor_data.SetName(GLB_NLS_GUI_CONFIGURATION.Id(381), Me.OperatorName)

    ' Operator Name
    Call auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(210), IIf(Me.OperatorName = "", AUDIT_NONE_STRING, Me.OperatorName))

    Return auditor_data

  End Function ' AuditorData

  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim temp_operator As CLASS_SITE_OPERATOR
    Dim idx_site As Integer

    temp_operator = New CLASS_SITE_OPERATOR

    ' Duplicate operator info
    temp_operator.m_site_operator.control_block = Me.m_site_operator.control_block
    temp_operator.m_site_operator.operator_id = Me.m_site_operator.operator_id
    temp_operator.m_site_operator.operator_name = Me.m_site_operator.operator_name
    temp_operator.m_site_operator.filler_0 = Me.m_site_operator.filler_0

    ' Duplicate array of forms permisions
    If IsNothing(m_site_list) Then
      temp_operator.m_site_list = Nothing
    Else
      ReDim temp_operator.m_site_list(Me.m_site_list.Length - 1)
      For idx_site = 0 To Me.m_site_list.Length - 1
        temp_operator.m_site_list(idx_site) = New TYPE_SITE_ITEM
        temp_operator.m_site_list(idx_site).site_id = Me.m_site_list(idx_site).site_id
        temp_operator.m_site_list(idx_site).site_name = Me.m_site_list(idx_site).site_name
      Next
    End If

    Return temp_operator

  End Function ' Duplicate

#End Region ' Overrides functions

#Region " Public functions "

  ' PURPOSE: Read all forms registers and save they into member array
  '          initializing permisions in member array to true
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - status code returned from Database
  '
  ' NOTE: Setting Profile Id to inexisting value returns all forms with no permisions.
  '
  Public Function DB_ReadSitesByOperator(ByVal OperatorId As Integer, _
                                         ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim rc As Integer
    Dim idx_form As Integer
    Dim site_item As New TYPE_SITE_ITEM
    Dim operator_and_sites As New TYPE_SITES_BY_OPERATOR

    ' Dispose actual array of sites to redim a new one
    m_site_list = Nothing

    ' Set the params values
    operator_and_sites.operator_id = OperatorId

    rc = ReadSiteList(operator_and_sites, SqlCtx)

    Select Case rc

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        ReDim m_site_list(operator_and_sites.site_list.Count - 1)

        ' Copy new items to array 
        For idx_form = 0 To operator_and_sites.site_list.Count - 1
          Call operator_and_sites.site_list.GetItem(idx_form, site_item)

          m_site_list(idx_form) = New TYPE_SITE_ITEM
          m_site_list(idx_form).site_id = site_item.site_id
          m_site_list(idx_form).site_name = site_item.site_name
        Next

        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_ReadSitesByOperator

#End Region ' Public functions

#Region " Private functions "

  Shared Function ReadOperatorName(ByVal SiteOperator As TYPE_SITE_OPERATOR, _
                                   ByVal Context As Integer) As Integer

    Dim str_sql As String
    Dim data_table As DataTable

    SiteOperator.operator_name = " "

    str_sql = "SELECT  ISNULL (SOP_NAME, ' ') AS SOP_NAME " & _
               " FROM  SITE_OPERATORS " & _
              " WHERE  SOP_OPERATOR_ID = " & SiteOperator.operator_id

    data_table = GUI_GetTableUsingSQL(str_sql, 5000)

    If IsNothing(data_table) Or data_table.Rows.Count = 0 Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    If Not IsNothing(data_table) Then
      SiteOperator.operator_name = NullTrim(data_table.Rows.Item(0).Item(0))
    End If

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
  End Function ' ReadOperatorName

  Private Function ReadOperator(ByVal SiteOperator As TYPE_SITE_OPERATOR, _
                                ByVal Context As Integer) As Integer

    Dim str_sql As String
    Dim data_table As DataTable

    str_sql = "SELECT  SOP_OPERATOR_ID " & _
                    ", SOP_NAME " & _
              " FROM  SITE_OPERATORS " & _
             " WHERE  SOP_OPERATOR_ID = " & SiteOperator.operator_id

    data_table = GUI_GetTableUsingSQL(str_sql, 5000)

    If IsNothing(data_table) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    If data_table.Rows.Count() <> 1 Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    SiteOperator.operator_name = data_table.Rows(0).Item("SOP_NAME")

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
  End Function ' ReadOperator

  Private Function ReadSiteList(ByVal SiteList As TYPE_SITES_BY_OPERATOR, _
                                ByVal Context As Integer) As Integer

    Dim site_item As New TYPE_SITE_ITEM
    Dim str_sql As String
    Dim idx_db_row As Integer
    Dim num_db_rows As Integer = 0
    Dim data_table As DataTable

    str_sql = "SELECT  ST_SITE_ID " & _
                    ", ST_NAME " & _
               " FROM  SITES " & _
              " WHERE  ST_OPERATOR_ID = " & SiteList.operator_id & _
           " ORDER BY  ST_SITE_ID "

    data_table = GUI_GetTableUsingSQL(str_sql, 5000)

    If IsNothing(data_table) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    num_db_rows = data_table.Rows.Count()
    For idx_db_row = 0 To num_db_rows - 1

      site_item.site_id = data_table.Rows(idx_db_row).Item("ST_SITE_ID")
      site_item.site_name = data_table.Rows(idx_db_row).Item("ST_NAME")

      SiteList.site_list.SetItem(SiteList.site_list.Count, site_item)
    Next

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
  End Function ' ReadSiteList

  Private Function UpdateOperator(ByVal SiteOperator As TYPE_SITE_OPERATOR, _
                                  ByVal Context As Integer) As Integer

    Dim str_sql As String
    Dim db_count As Integer = 0
    Dim SqlTrans As System.Data.SqlClient.SqlTransaction = Nothing
    Dim data_table As DataTable

    If Not GUI_BeginSQLTransaction(SqlTrans) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    str_sql = "SELECT  COUNT(*) " & _
               " FROM  SITE_OPERATORS " & _
              " WHERE  SOP_NAME = '" & SiteOperator.operator_name & "' " & _
                " AND  SOP_OPERATOR_ID <> " & SiteOperator.operator_id

    data_table = GUI_GetTableUsingSQL(str_sql, 5000, , SqlTrans)
    If IsNothing(data_table) Then
      GUI_EndSQLTransaction(SqlTrans, False)

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    db_count = data_table.Rows(0).Item(0)

    If db_count > 0 Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
    End If

    str_sql = "UPDATE  SITE_OPERATORS " & _
                " SET  SOP_NAME = '" & SiteOperator.operator_name & "' " & _
              " WHERE  SOP_OPERATOR_ID = " & SiteOperator.operator_id

    If Not GUI_SQLExecuteNonQuery(str_sql, SqlTrans) Then
      GUI_EndSQLTransaction(SqlTrans, False)

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    If Not GUI_EndSQLTransaction(SqlTrans, True) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
  End Function ' UpdateOperator

  Private Function InsertOperator(ByVal SiteOperator As TYPE_SITE_OPERATOR, _
                                  ByVal Context As Integer) As Integer

    Dim str_sql As String
    Dim db_count As Integer = 0
    Dim SqlTrans As System.Data.SqlClient.SqlTransaction = Nothing
    Dim data_table As DataTable

    If Not GUI_BeginSQLTransaction(SqlTrans) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    str_sql = "SELECT  COUNT(*) " & _
               " FROM  SITE_OPERATORS " & _
              " WHERE  SOP_NAME = '" & SiteOperator.operator_name & "' "

    data_table = GUI_GetTableUsingSQL(str_sql, 5000)
    If IsNothing(data_table) Then
      GUI_EndSQLTransaction(SqlTrans, False)

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    db_count = data_table.Rows(0).Item(0)

    If db_count > 0 Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
    End If

    str_sql = "INSERT INTO   SITE_OPERATORS " & _
                         " ( SOP_NAME " & _
                         " )" & _
                  " VALUES " & _
                         " ( '" & SiteOperator.operator_name & "' " & _
                         " )"

    If Not GUI_SQLExecuteNonQuery(str_sql, SqlTrans) Then
      GUI_EndSQLTransaction(SqlTrans, False)

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    If Not GUI_EndSQLTransaction(SqlTrans, True) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
  End Function ' InsertOperator

  Private Function DeleteOperator(ByVal OperatorId As Integer, ByVal Context As Integer) As Integer

    Dim str_sql As String
    Dim db_count As Integer = 0
    Dim SqlTrans As System.Data.SqlClient.SqlTransaction = Nothing
    Dim data_table As DataTable

    If Not GUI_BeginSQLTransaction(SqlTrans) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    str_sql = "SELECT  COUNT(*) " & _
               " FROM  SITES " & _
              " WHERE  ST_OPERATOR_ID = " & OperatorId

    data_table = GUI_GetTableUsingSQL(str_sql, 5000)
    If IsNothing(data_table) Then
      GUI_EndSQLTransaction(SqlTrans, False)

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    db_count = data_table.Rows(0).Item(0)

    If db_count > 0 Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DEPENDENCIES
    End If

    str_sql = "DELETE  SITE_OPERATORS " & _
              " WHERE  SOP_OPERATOR_ID = " & OperatorId

    If Not GUI_SQLExecuteNonQuery(str_sql, SqlTrans) Then
      GUI_EndSQLTransaction(SqlTrans, False)

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    If Not GUI_EndSQLTransaction(SqlTrans, True) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
  End Function ' DeleteOperator

#End Region ' Private functions

#Region " Shared Functions "

  ' PURPOSE: Read Database to obtain the prolile name for the given ID
  '
  '  PARAMS:
  '     - INPUT:
  '           - ProfileId
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - ProfileName or INVALID_PROFILE_NAME if error
  '
  Shared Function GetOperatorNameForId(ByVal OperatorId As Integer, ByRef SqlCtx As Integer) As String

    Dim operator_name As String
    Dim operator_info As CLASS_SITE_OPERATOR.TYPE_SITE_OPERATOR
    Dim rc As Integer

    operator_info = New CLASS_SITE_OPERATOR.TYPE_SITE_OPERATOR
    operator_info.operator_id = OperatorId
    rc = ReadOperatorName(operator_info, SqlCtx)
    If rc = ENUM_CONFIGURATION_RC.CONFIGURATION_OK Then
      operator_name = operator_info.operator_name
    Else
      operator_name = ""
    End If

    Return operator_name

  End Function ' GetOperatorNameForId

#End Region ' Shared Functions

End Class
