'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : cls_cage_control.vb
'
' DESCRIPTION : cage class
'              
' REVISION HISTORY :
'
' Date         Author    Description
' -----------  ------    ----------------------------------------------
' 13-NOV-2013 CCG & FBA  First Release.
' 13-FEB-2014 FBA        Fixed Bug WIG-623. 
' 18-FEB-2014 JBA        Added new parameter "ShowEmptyElement" to add empty element in the form's combos.
' 25-FEB-2014 ICS & FBA  Added support for terminal collection with cage movement.
' 12-MAR-2014 CCG        Fixed Bug WIGOSTITO-1110.
' 13-MAR-2014 JCA        Fixed Bug WIG-723.
' 17-MAR-2014 JCA        Fixed Bug WIG-741: Promobox or Sys-Acceptor Collections can not open when is collected.
' 07-APR-2014 FBA        Added logic for money requests from cash desk.
' 28-APR-2014 CCG        Fixed Bug WIG-860: Negative denominations in some forms.
' 30-APR-2014 CCG        Fixed Bug WIG-879 / WIG-879: Show "Amount" depending on the GP:Cage.ShowDenominations.
' 08-MAY-2014 CCG & LEM  Added Logic to cancel stacker collections and cashier with tickets collections.
' 09-MAY-2014 LEM        Fixed Bug WIGOSTITO-1219: Error on collection cancelation without bills.
' 15-MAY-2014 LEM        Fixed Bug WIGOSTITO-1223: Wrong undoing cashier movements on cancel collection operation.
' 20-MAY-2014 HBB        Fixed Bug WIGOSTITO-1043: When a money collection is done with imbalance, it appears as closed and balanced.
' 20-MAY-2014 HBB        Fixed Bug WIG-923: There are wrong denominations(Ckeck, coins AND card) visibles in stacker collections.
' 20-MAY-2014 JCA        Fixed Bug WIG-930: Wrong audit info.
' 20-MAY-2014 JCA        Fixed Bug WIG-935: Can't open cashier request.
' 21-MAY-2014 JAB        Added new columns to cancellation data.
' 21-MAY-2014 JAB        Fixed Bug WIG-941: Local date is used when cage movements are created. The correct is to use the server date.
' 21-MAY-2014 JCA        Fixed Bug WIG-947: Not save the cancelled user when is recollect from terminal.
' 06-JUN-2014 JAB        Added two decimal characters in denominations fields.
' 30-JUN-2014 LEM        Fixed Bug WIG-1058: There are not audit of  RequestOperation cancelation.
' 30-JUN-2014 JBC & JPJ  Added user templates.
' 25-JUL-2014 DLL    Chips are moved to a separate table
' 08-AUG-2014 JAB        Fixed Bug WIG-1176: If there are two card or check movements, only last movement is shown when cage movement is edited.
' 23-SEP-2014 RRR        Added Cage Meters functionalities
' 29-SEP-2014 JBC        Fixed Bug WIG-1338: Error when we save an existing template.
' 30-SEP-2014 LRS        Create enum for Movement Types
' 30-SEP-2014 LRS        Update Cage Meters when receipt from cashier
' 03-OCT-2014 JBC        Fixed Bug WIG-1381: Now audit shows the template used on the send
' 06-OCT-2014 SMN        Verify conversions from null/empty values
' 06-OCT-2014 JBC        Fixed Bug WIG-1416: Function setting denominations on grid is more safety
' 09-OCT-2014 LRS        Fixed bug WIG-1440 WIG-1438: Can't close cage in tito mode
' 09-OCT-2014 LEM        Fixed Bug WIG-1456: Show chips denominations with Cage.ShowDenominations = 0
' 15-OCT-2014 JBC        Fixed Bug WIG-1485: Cards and checks with Automode=True now works. 
' 13-NOV-2014 HBB        Fixed Bug WIG-1670: Denominations that are not defined at money_collection_metter cant be shown in terminal money collection(TITO Mode)
' 19-NOV-2014 DRV        Fixed Bug WIG-1711: Added Massive collection for cashless mode
' 20-NOV-2014 LEM        InitCurrencyData include Bank Card and Check in addition to Coins(Amount) when the option HideAllDenominations is activated
'                        also when ShowOnlyChipsDenominations is activated and IsoCode is not CHIPS_ISO_CODE
' 25-NOV-2014 LEM        Fixed Bug WIG-1609: Wrong denominations in Cage Count
' 09-APR-2015 RMS        Gaming Day
' 20-APR-2015 RMS        Fixed Bug WIG-2192: Cage - New Send to cashier without values
' 20-APR-2015 RMS        Fixed Bug WIG-2227: Cage - Error openning movement: denomination does not exists
' 29-APR-2015 YNM        Fixed Bug WIG-2263: Error closing session with custom recolect movement
' 29-APR-2015 YNM        Fixed Bug WIG-2253: Request operation can be managed by several users
' 30-APR-2015 DLL        Fixed Bug WIG-2262: Error gambling tables. Errors with playertracking and without playertracking
' 24-MAY-2015 YNM        Fixed Bug WIG-2365: Cage Count fixed some errors
' 24-MAY-2015 DLL        Fixed Bug WIG-2398: Error gaming tables with playertracking don't set CashierSessionId
' 28-MAY-2015 MPO        Fixed Bug WIG-2391
' 29-MAY-2015 DLL        Fixed Bug WIG-2389: Duplicate Cage stock when gaming table collection
' 05-JUN-2015 DLL        Fixed Bug WIG-2388: Error when collect gaming table
' 08-JUN-2015 MPO        Fixed Bug WIG-2417
' 09-JUN-2015 RCI        Fixed Bug WIG-2421
' 09-JUN-2015 SGB        Backlog Item 1416
' 16-JUN-2015 DHA        Fixed Bug WIG-2446
' 19-JUN-2015 DHA        Fixed Bug WIG-2475
' 20-AUG-2015 YNM        Fixed Bug TFS-3887: Pending collection from terminal opens collection history screen
' 22-SEP-2015 ETP        Fixed Bug TFS-4269: Added new condition for show all currencies.
' 05-OCT-2015 FAV        Fixed Bug TFS-4269: Change in the condition (checked). The general param doesn't affect the cancel movement.
' 08-SEP-2015 FOS        Backlog Item 3709
' 18-FEB-2016 RAB        Bug 9523:Ref. 16572: en los detalles de movimiento de Boveda a veces no aparece el "Usuario" o "Caja"
' 13-JAN-2016 DHA        Task 8287: Floor Dual Currency
' 03-FEB-2016 JPJ        Bug 8965:FloorDualCurrency: Cage meters totals are not adding up correctly
' 07-MAR-2016 ETP        Fixed Bug 10224: Validate if stock has changed.
' 23-MAR-2016 FAV        Fixed Bug 10911: Error showing Detail movement for imbalance collect
' 05-APR-2016 DDS        Fixed bug 9209: Confusing workflow when cancelling a money collection from GUI
' 05-APR-2016 DDS        Backlog Item 10953: Add CountR terminals toCombo
' 23-MAR-2016 JML        Product Backlog Item 9755:Gaming Tables (Fase 1): Cage
' 22-ABR-2016 JML        Product Backlog Item 9758:Gaming Tables (Fase 1): General cage report
' 28-ABR-2016 JML        Product Backlog Item 10825:Gaming Tables (Fase 1): Items, tickets & chips (Cage)
' 02-MAY-2016 JML        Product Backlog Item 10825:Gaming Tables (Fase 1): Items, tickets & chips (Cage)
' 24-MAY-2016 RAB        PBI 11755: Tables (Phase 1): Upgrading to a new version of chips. Backward compatibility
' 24-MAY-2016 DHA        Product Backlog Item 9848: drop box gaming tables
' 24-MAY-2016 YNM        PBI 13371: CountR - Review changes
' 09-JUN-2016 RAB        PBI 11755: Tables (Phase 1): Correction errors as a result of the backward compatibility of the chips group
' 10-JUN-2016 JMl        Product Backlog Item 13541:UNPLANNED - Temas varios Winions Sprint 25 -  Add closing stock window
' 16-JUN-2016 RAB        Product Backlog Item 9853: GamingTables (Phase 3): Fixed Bank (Phase 1)
' 16-JUN-2016 JMl        Product Backlog Item 14486: Review - Backward compatibility.
' 27-JUN-2016 FOS        Product Backlog Item 14493: Gaming tables (Fase 4): Cage
' 28-JUN-2016 DHA        Product Backlog Item 15903: Gaming tables (Fase 5): Drop box collection on Cashier
' 11-JUL-2016 JBP        Bug 15408:CountR: No se puede cerrar sesi�n de b�veda despu�s de haber operado con una sesi�n de CountR
'                        Bug 15315:CountR: no se puede cerrar una sesi�n de caja de countr teniendo el kiosco deshabilitado
' 15-JUL-2016 JBP        Bug 15409:CountR: No se insertan los registros en "Faltantes y sobrantes"
'                        Bug 15626:CountR: No se actualiza la sesi�n de CountR
' 14-JUL-2016 JCA        Bug 15214:CountR: No se cargan los detalles de movimiento de b�veda despu�s de haber realizado un env�o
' 18-JUL-2016 JCA        Bug 15594:CountR: se puede enviar/recaudar fichas, cheque/tarjeta bancaria
' 17-AGO-2016 FJC        Fixed Bug 16693:CountR: Al recaudar una sesi�n de kiosco, no se puede cerrar la sesi�n de b�veda asociada
' 19-AUG-2016 FJC        Fixed Bug 16884:CountR: error en los env�os de dinero a CountR recien creados.
' 22-AGO-2016 FOS        Fixed Bug 16714:Uncontrolled exception when saves templates
' 30-AUG-2016 JBP        Bug 17105: CountR: No se actualiza la columna "entregado" de la sesi�n de CountR cuando se cancela su recaudaci�n asociada.
' 30-AUG-2016 JBP        Bug 17756:No se puede cerrar b�veda, sale mensaje de error que existen movimientos pendientes cuando realmente no los hay.
' 19-SEP-2016 ETP        Fixed Bug 16866: Cage Count: Expected value is incorrect when auditing
' 20-SEP-2016 FJC        Bug 17669:Anular retiro de caja desde b�veda/Cajero: no deshace el stock y conceptos de b�veda
' 27-SEP-2016 FAV        Fixed Bug 8816: Allow request to cage without cage opened.
' 28-SEP-2016 DHA        PBI 17747: cancel gaming tables operations
' 29-SEP-2016 FJC        Fixed Bug (Reopened) 17669:Anular retiro de caja desde b�veda/Cajero: no deshace el stock y conceptos de b�veda
' 13-OCT-2016 JML        Fixed Bug 18952:GUI: New movement Cage unhandled exception to save a template configured to send bills to cage when the GP GamingTables.TransferCurrency.Enabled = 0
' 20-OCT-2016 JMV        Bug 19345:Auditor�a - Televisa: incorrect amount
' 20-OCT-2016 ATB        Bug 18909:Al realizar una recaudaci�n de b�veda, sale un pop up de env�o
' 22-NOV-2016 DHA        Bug 6550: Errors when audit terminal collection on coins case
' 08-DIC-2016 RGR        Bug 18295: Cage: The general report is not showing the correct data when a withdrawal is canceled
' 09-DEC-2016 XGJ        Bug 20177: Error en el manejo de divisa en b�veda 
' 14-DEC-2016 DHA        Bug 21545: error on sending to gaming table without auto-cage, marks the sent as finalized
' 26-JAN-2017 AMF        Bug 23503:Cage: No se cargan correctamente los terminales al crear un nuevo movimiento
' 30-JAN-2017 DPC        Bug 23503:Cage: No se cargan correctamente los terminales al crear un nuevo movimiento
' 09-FEN-2017 JML        PBI 24378: Reabrir sesiones de caja y mesas de juego: rehacer reapertura y correcci�n de Bugs
' 20-FEB-2017 DHA        Bug 24792: on cancel withdrawals the user is setting wrong
' 21-FEB-2017 JML        PBI 24378: Reabrir sesiones de caja y mesas de juego: rehacer reapertura y correcci�n de Bugs
' 24-MAR-2017 ETP        PBI 25788: Hide Currency Credit Line
' 19-JUN-2017 ETP        WIGOS 2761: The concept coin not visible in the voucher.
' 03-AUG-2017 DPC        PBI 25788: Hide Currency Credit Line
' 26-OCT-2017 DHA        Bug 30436:WIGOS-6165 [Ticket #9844] Registros de fichas desaparecidos tras actualizaci�n
' 28-JUN-2017 FOS        Bug 33411:WIGOS-12936 Gaming tables template - Confirmation pop up should not appear when clicking on Cancel button without changes made.
'-------------------------------------------------------------------

Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports GUI_CommonOperations
Imports GUI_Controls
Imports GUI_CommonMisc
Imports WSI.Common
Imports WSI.Common.CountR
Imports System.Text
Imports WSI.Common.FeatureChips

Partial Public Class CLASS_CAGE_CONTROL
  Inherits CLASS_BASE

#Region "Enums"

  Public Enum ENUM_CAGE_CONTROL_SCREEN_MODE
    MODE_EDIT = 0
    MODE_NEW = 1
    MODE_DELETE = 2
  End Enum

  Public Enum DT_COLUMN_CURRENCY
    ISO_CODE = 0
    DENOMINATION = 1
    CURRENCY_TYPE = 2
    CHIP_COLOR = 3
    CHIP_SET_NAME = 4
    CHIP_NAME = 5
    CHIP_DRAWING = 6
    CHIP_ID = 7
    CHIP_SET_ID = 8
    DENOMINATION_WITH_SIMBOL = 9
    QUANTITY = 10
    TOTAL = 11
    TOTAL_WITH_SIMBOL = 12
    CASHIER_QUANTITY = 13
    CASHIER_TOTAL = 14
    QUANTITY_DIFFERENCE = 15
    TOTAL_DIFFERENCE = 16
    EXPECTED_COLUMN = 17
  End Enum

  Public Enum DT_COLUMN_MOVEMENT
    MOVEMENT_ID = 0
    TERM_CAGE_ID = 1
    TERM_CASHIER_ID = 2
    USER_CAGE_ID = 3
    USER_CASHIER_ID = 4
    OPERATION_TYPE = 5
    MOV_DATETIME = 6
    STATUS = 7
    CASHIER_SESSION_ID = 8
    USER_CUSTOM_ID = 9
    CAGE_SESSION_ID = 10
    TARGET_TYPE = 11
    GAMING_TABLE_ID = 12
    GAMING_TABLE_VISITS = 13
    MONEY_COLLECTION_ID = 14
    USER_CAGE_ID_LAST_MODIFICATION = 15
    MOV_DATETIME_LAST_MODIFICATION = 16
    CAGE_SESSION_STATUS = 17
  End Enum

  Public Enum OPEN_MODE
    NEW_MOVEMENT = 0
    CANCEL_MOVEMENT = 1
    RECOLECT_MOVEMENT = 2
    VIEW_MOVEMENT = 3
    'RESEND_MOVEMENT = 4
    OPEN_CAGE = 5
    CLOSE_CAGE = 6
    COUNT_CAGE = 7
    CLOSE_VIRTUAL = 8
    RECOLECT_FROM_PROMOBOX_OR_ACCEPTOR = 9
    CANCEL_MOVEMENT_FROM_PROMOBOX_OR_ACCEPTOR = 10
    RECOLECT_MASSIVE = 11
    TEMPLATE_MAINTENANCE = 12
    CANCEL_MOVEMENT_FROM_GAMINGTABLES = 13
  End Enum

  Public Enum MOVEMENT_TYPE
    RECEIPT = 0
    SEND = 1
  End Enum

#End Region ' Enums

#Region "Contants"

  Private Const ISO_CODE_COLUMN As Integer = 0
  Private Const DENOMINATION_COLUMN As Integer = 1
  Private Const QUANTITY_COLUMN As Integer = 3
  Private Const CM_ADD_AMOUNT As Integer = 2
  Private Const CM_SUB_AMOUNT As Integer = 3
  Private Const CM_INITIAL_BALANCE As Integer = 4
  Private Const CM_TYPE As Integer = 5
  Private TOTAL_COLUMN As Integer = 0
  Private CURRENCY_TYPE_COLUMN As Integer = 0

  Private Const TABLE_NAME_PREFERENCES As String = "Preferences"
  Private Const TABLE_NAME_CURRENCIES As String = "Currencies"
  Private Const TABLE_COLUMN_OPERATION_TYPE As String = "OperationType"
  Private Const TABLE_COLUMN_TARGET_TYPE As String = "TargetType"
  Private Const TABLE_NAME_MOVEMENT As String = "Movement"

  Private Const MAX_NUMBER_TEMPLATES As Integer = 20
  Public Const COUNTR_ID_OFFSET As Integer = 100000

  Private Const EMPTY_VALUE As String = "---"

#End Region ' Contants 

#Region "Members"

  'movement
  Private m_movement_id As Long
  Private m_terminal_cage_id As Long
  Private m_terminal_cashier_id As Long
  Private m_user_cage_id As String
  Private m_user_cage_id_last_modification As String
  Private m_user_cashier_id As String
  Private m_operation_type As Short
  Private m_movement_datetime As DateTime
  Private m_movement_datetime_last_modification As DateTime
  Private m_status As Short
  Private m_table_currencies As DataTable
  Private m_table_preferences As DataTable
  Private m_table_theoric_values As DataTable
  Private m_name As String
  Private m_open_mode As Int16
  Private m_promobox_or_acceptor As Boolean
  Private m_cashier_session_id As Long
  Private m_user_custom_id As String
  Private m_target_type As Integer
  Private m_related_movement_id As Long
  Private m_gaming_table_id As Integer
  Private m_gaming_table_visits As Decimal
  Private m_money_collection_id As Long

  Private m_is_reception_and_close As Boolean
  Private m_gui_user_type As GU_USER_TYPE

  Private m_white_spaces As Integer

  Private m_template_id As Long 'Stores the template used to send the information
  Private m_template_name As String  'Stores the template used to send the information

  Private m_sct_data As DataTable
  Private m_source_target_id As Int64
  Private m_movement_type As Int32
  Private m_concepts_data As DataTable
  Private m_denominations_in_terminals As List(Of Decimal)

  Private m_cls_countr As CLASS_COUNTR

  ' JBC 03-NOV-2014
  Private m_send_by_template As Boolean = False
  ' SGB 15-MAY-2015
  Public m_table_currencies_copy As DataTable

  ' FOS 08-SEP-2015
  Private m_is_enable_coin_collection As Boolean

  ' DHA 13-JAN-2016
  Private m_terminal_iso_code As String

  Private m_countr_sufix As String = "-[" + GLB_NLS_GUI_PLAYER_TRACKING.GetString(7226) + "]"
  Private m_terminal_name As String
  ' JML 09-jun-2016 
  Private m_cashier_id As Int64
  Private m_cashier_name As String
  Private m_is_template_editable As Boolean
  Private m_closing_stock As ClosingStocks

  Private m_show_without_cage_id As Boolean

  Private m_is_countr As Boolean
  Private m_countr_id As Int32

  Private m_countr_close_session As Boolean

  Private m_national_currency As String

#End Region ' Members

#Region "Properties"

  Public Property MovementID() As Long
    Get
      Return m_movement_id
    End Get

    Set(ByVal Value As Long)
      m_movement_id = Value
    End Set
  End Property ' MovementID

  Public Property TerminalCageID() As Long
    Get
      Return m_terminal_cage_id
    End Get

    Set(ByVal Value As Long)
      m_terminal_cage_id = Value
    End Set
  End Property ' TerminalCageID

  Public Property TerminalCashierID() As Integer
    Get
      Return m_terminal_cashier_id
    End Get

    Set(ByVal Value As Integer)
      m_terminal_cashier_id = Value
    End Set
  End Property ' TerminalCashierID

  Public Property UserCageID() As String
    Get
      Return m_user_cage_id
    End Get

    Set(ByVal Value As String)
      m_user_cage_id = Value
    End Set
  End Property ' UserCageID

  Public Property UserCageIDLastModification() As String
    Get
      Return m_user_cage_id_last_modification
    End Get

    Set(ByVal Value As String)
      m_user_cage_id_last_modification = Value
    End Set
  End Property ' UserCageIDLastModification

  Public Property UserCashierID() As Integer
    Get
      Return m_user_cashier_id
    End Get

    Set(ByVal Value As Integer)
      m_user_cashier_id = Value
    End Set
  End Property ' UserCashierID

  Public Property OperationType() As Short
    Get
      Return m_operation_type
    End Get

    Set(ByVal Value As Short)
      m_operation_type = Value
    End Set
  End Property ' OperationType

  Public Property MovementDatetime() As DateTime
    Get
      Return m_movement_datetime
    End Get

    Set(ByVal Value As DateTime)
      m_movement_datetime = Value
    End Set
  End Property ' MovementDatetime

  Public Property MovementDatetimeLastModification() As DateTime
    Get
      Return m_movement_datetime_last_modification
    End Get

    Set(ByVal Value As DateTime)
      m_movement_datetime_last_modification = Value
    End Set
  End Property ' MovementDatetimeLastModification

  Public Property Status() As Short
    Get
      Return m_status
    End Get

    Set(ByVal Value As Short)
      m_status = Value
    End Set
  End Property ' Status

  Public Property TableCurrencies() As DataTable
    Get
      Return m_table_currencies
    End Get

    Set(ByVal Value As DataTable)
      m_table_currencies = Value
    End Set
  End Property ' TableCurrencies

  Public Property TablePreferences() As DataTable
    Get
      Return m_table_preferences
    End Get

    Set(ByVal Value As DataTable)
      m_table_preferences = Value
    End Set
  End Property ' TableCurrencies

  Public Property TableTheoricValues() As DataTable
    Get
      Return m_table_theoric_values
    End Get

    Set(ByVal Value As DataTable)
      m_table_theoric_values = Value
    End Set
  End Property ' TableTheoricValues

  Public Property Name() As String
    Get
      Return m_name
    End Get
    Set(ByVal Value As String)
      m_name = Value
    End Set
  End Property ' Name

  Public Property OpenMode() As Int16
    Get
      Return m_open_mode
    End Get
    Set(ByVal Value As Int16)
      m_open_mode = Value
    End Set
  End Property ' OpenMode

  Public Property GuiUserType() As GU_USER_TYPE
    Get
      Return m_gui_user_type
    End Get
    Set(ByVal Value As GU_USER_TYPE)
      m_gui_user_type = Value
    End Set
  End Property ' GuiUserType

  Public Property IsPromoBoxOrAcceptor() As Boolean
    Get
      Return m_promobox_or_acceptor
    End Get
    Set(ByVal Value As Boolean)
      m_promobox_or_acceptor = Value
    End Set
  End Property ' IsPromoBoxOrAcceptor

  Public Property CashierSessionId() As Long
    Get
      Return m_cashier_session_id
    End Get
    Set(ByVal Value As Long)
      m_cashier_session_id = Value
    End Set
  End Property ' CashierSessionId

  Public Property CustomUserId() As Long
    Get
      Return m_user_custom_id
    End Get
    Set(ByVal Value As Long)
      m_user_custom_id = Value
    End Set
  End Property ' CustomUserId

  Public Property TargetType() As Long
    Get
      Return m_target_type
    End Get

    Set(ByVal Value As Long)
      m_target_type = Value
    End Set
  End Property ' TargetType

  Public Property RelatedMovementId() As Long
    Get
      Return m_related_movement_id
    End Get
    Set(ByVal value As Long)
      m_related_movement_id = value
    End Set
  End Property ' RelatedMovementId

  Public Property GamingTableId() As Integer
    Get
      Return m_gaming_table_id
    End Get
    Set(ByVal value As Integer)
      m_gaming_table_id = value
    End Set
  End Property ' GamingTableId

  Public Property ReceptionAndClose() As Boolean
    Get
      Return m_is_reception_and_close
    End Get
    Set(ByVal value As Boolean)
      m_is_reception_and_close = value
    End Set
  End Property ' ReceptionAndClose

  Public Property GamingTableVisits() As Decimal
    Get
      Return m_gaming_table_visits
    End Get
    Set(ByVal value As Decimal)
      m_gaming_table_visits = value
    End Set
  End Property ' GamingTableVisits

  Public Property TemplateID() As Long
    Get
      Return m_template_id
    End Get

    Set(ByVal Value As Long)
      m_template_id = Value
    End Set
  End Property ' TemplateID

  Public Property TemplateName() As String
    Get
      Return m_template_name
    End Get

    Set(ByVal Value As String)
      m_template_name = Value
    End Set
  End Property ' TemplateName

  Public Property SourceTargetConceptsData() As DataTable
    Get
      Return m_sct_data
    End Get

    Set(ByVal Value As DataTable)
      m_sct_data = Value
    End Set
  End Property ' SourceTargetConceptsData

  Public Property SourceTargetId() As Int64
    Get
      Return m_source_target_id
    End Get
    Set(ByVal Value As Int64)
      m_source_target_id = Value
    End Set
  End Property ' SourceTargetId

  Public Property MovementType() As Int32
    Get
      Return m_movement_type
    End Get
    Set(ByVal Value As Int32)
      m_movement_type = Value
    End Set
  End Property ' SourceTargetId

  Public Property ConceptsData() As DataTable
    Get
      Return m_concepts_data
    End Get

    Set(ByVal Value As DataTable)
      m_concepts_data = Value
    End Set
  End Property ' ConceptsData

  Public Property SendByTemplate() As Boolean
    Get
      Return m_send_by_template
    End Get

    Set(ByVal Value As Boolean)
      m_send_by_template = Value
    End Set
  End Property ' ConceptsData

  Public Property DenominationAceptedByTerminals() As List(Of Decimal)
    Get
      Return m_denominations_in_terminals
    End Get
    Set(ByVal value As List(Of Decimal))
      m_denominations_in_terminals = value
    End Set
  End Property

  Public Property IsEnableCoinCollection() As Boolean
    Get
      Return m_is_enable_coin_collection
    End Get
    Set(ByVal value As Boolean)
      m_is_enable_coin_collection = value
    End Set
  End Property ' IsEnableCoinCollection

  Public Property TerminalIsoCode() As String
    Get
      Return m_terminal_iso_code
    End Get
    Set(ByVal value As String)
      m_terminal_iso_code = value
    End Set
  End Property ' TerminalIsoCode

  Public Property TerminalName() As String
    Get
      Return m_terminal_name
    End Get
    Set(ByVal value As String)
      m_terminal_name = value
    End Set
  End Property ' TerminalName

  Public Property IsCountR() As Boolean
    Get
      Return m_is_countr
    End Get

    Set(ByVal Value As Boolean)
      m_is_countr = Value
    End Set
  End Property ' IsCountR

  'Public Property CountRId() As Int32
  '  Get
  '    Return m_countr_id
  '  End Get

  '  Set(ByVal Value As Int32)
  '    m_countr_id = Value
  '  End Set
  'End Property ' CountRId

  Public ReadOnly Property IsCountREnabled() As Boolean
    Get
      Return WSI.Common.Misc.IsCountREnabled()
    End Get
  End Property ' IsCountREnabled

  Public Property CountR() As CLASS_COUNTR
    Get
      If m_cls_countr Is Nothing Then
        m_cls_countr = New CLASS_COUNTR()
      End If
      Return m_cls_countr
    End Get

    Set(ByVal Value As CLASS_COUNTR)
      m_cls_countr = Value
    End Set
  End Property ' MovementID

  Public Property CountRCloseSession() As Boolean
    Get
      Return m_countr_close_session
    End Get
    Set(ByVal Value As Boolean)
      m_countr_close_session = Value
    End Set
  End Property ' CountRCloseSession

  Public Property CashierId() As Long
    Get
      Return m_cashier_id
    End Get
    Set(ByVal value As Long)
      m_cashier_id = value
    End Set
  End Property ' CashierId

  Public Property CashierName() As String
    Get
      Return m_cashier_name
    End Get
    Set(ByVal value As String)
      m_cashier_name = value
    End Set
  End Property ' CashierName

  Public Property IsTemplateEditable() As Boolean
    Get
      Return m_is_template_editable
    End Get
    Set(ByVal value As Boolean)
      m_is_template_editable = value
    End Set
  End Property ' CashierName

  Public Property ClosingStocks() As ClosingStocks
    Get
      Return m_closing_stock
    End Get
    Set(value As ClosingStocks)
      m_closing_stock = value
    End Set
  End Property

  Public Property ShowWithoutCageID As Boolean
    Get
      Return m_show_without_cage_id
    End Get
    Set(value As Boolean)
      m_show_without_cage_id = value
    End Set
  End Property

#End Region ' Properties 

#Region "Overrides"

  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As ENUM_STATUS
    Dim _result As Boolean
    Dim _sb As StringBuilder
    Dim _obj As Object
    Dim _mb_user_type As MB_USER_TYPE
    Dim _terminal_type As TerminalTypes

    'Me.MovementID = ObjectId
    'Me.m_type_new_collection.money_collection_id = ObjectId

    If Me.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.COUNT_CAGE Then
      Me.OperationType = Cage.CageOperationType.CountCage
      Call Me.GetGlobalCageStock(True)
    End If

    If Me.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.TEMPLATE_MAINTENANCE Then
      Call Me.GetGlobalCageStock(True)
    End If

    If Me.MovementID = 0 And Me.OperationType = Cage.CageOperationType.FromTerminal Then
      Call Me.ReadMovementIdByMoneyCollection()
    End If

    ' JCA 20-MAY-2014: Fixed Bug WIG-935.
    _result = True
    If Me.MovementID > 0 Then
      If Not ReadMovement() Then
        Return ENUM_STATUS.STATUS_ERROR
      End If
    End If

    _terminal_type = TerminalTypes.SAS_HOST

    'Check if it's Promobox or Acceptor
    If Not TITO.Utils.IsTitoMode() AndAlso Me.OperationType = Cage.CageOperationType.FromTerminal And Not Me.IsPromoBoxOrAcceptor Then

      Using _db_trx As New DB_TRX()

        _sb = New StringBuilder()

        If Me.TerminalCashierID > 0 Then
          _sb.AppendLine(" SELECT   TE_TERMINAL_TYPE ")
          _sb.AppendLine("   FROM   CASHIER_TERMINALS ")
          _sb.AppendLine("  INNER   JOIN TERMINALS  ON TE_TERMINAL_ID = CT_TERMINAL_ID ")
          _sb.AppendLine("  WHERE   CT_CASHIER_ID = @pCashierId ")
        ElseIf Not String.IsNullOrEmpty(Me.m_type_new_collection.floor_id) Then
          _sb.AppendLine(" SELECT   TE_TERMINAL_TYPE ")
          _sb.AppendLine("   FROM   TERMINALS ")
          _sb.AppendLine("  WHERE   TE_FLOOR_ID = @pFloorId ")
        Else
          Return ENUM_STATUS.STATUS_NOT_FOUND
        End If

        Using _cmd As New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _cmd.Parameters.Add("@pCashierId", SqlDbType.NVarChar).Value = Me.TerminalCashierID
          _cmd.Parameters.Add("@pFloorId", SqlDbType.NVarChar).Value = IIf(String.IsNullOrEmpty(Me.m_type_new_collection.floor_id), System.DBNull.Value, Me.m_type_new_collection.floor_id)

          _obj = _cmd.ExecuteScalar()
        End Using

        If _obj IsNot Nothing AndAlso _obj IsNot DBNull.Value Then
          _terminal_type = CShort(_obj)
        Else
          Return ENUM_STATUS.STATUS_ERROR
        End If

      End Using
    End If

    If Me.OperationType = Cage.CageOperationType.FromTerminal Then
      Cashier.GetMobileBankAndGUIUserType(_terminal_type, _mb_user_type, Me.GuiUserType)
      Me.IsPromoBoxOrAcceptor = (Me.GuiUserType = GU_USER_TYPE.SYS_PROMOBOX Or Me.GuiUserType = GU_USER_TYPE.SYS_ACCEPTOR)
    End If

    If (TITO.Utils.IsTitoMode() AndAlso _
        (Me.OperationType = Cage.CageOperationType.FromTerminal OrElse Me.OperationType = Cage.CageOperationType.FromCashier OrElse _
         Me.OperationType = Cage.CageOperationType.FromGamingTableDropbox OrElse Me.OperationType = Cage.CageOperationType.FromGamingTableDropboxWithExpected OrElse _
         Me.OperationType = Cage.CageOperationType.FromCashierClosing OrElse Me.OperationType = Cage.CageOperationType.FromGamingTableClosing)) Or _
        Me.IsPromoBoxOrAcceptor Then

      _result = SelectReadStackerCollectionMode()
    End If

    If _result Then
      Return CLASS_BASE.ENUM_STATUS.STATUS_OK
    Else
      Return ENUM_STATUS.STATUS_ERROR
    End If

  End Function ' DB_Read

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As ENUM_STATUS
    Dim rc As Integer
    Dim _result As Integer
    Dim _m_open_mode As Int16

    _m_open_mode = m_open_mode

    If m_open_mode = OPEN_MODE.NEW_MOVEMENT And m_operation_type = Cage.CageOperationType.FromTerminal Then
      _m_open_mode = OPEN_MODE.RECOLECT_MOVEMENT
    End If


    Me.IsCountR = Me.IsCountRTerminal()

    Select Case _m_open_mode

      Case OPEN_MODE.RECOLECT_MOVEMENT _
         , OPEN_MODE.RECOLECT_MASSIVE

        If m_operation_type = Cage.CageOperationType.RequestOperation Then
          If m_status = Cage.CageStatus.Canceled Then
            'cancel request
            rc = Me.CancelRequest()
          Else
            rc = Me.ProcessRequestOK()
          End If
        ElseIf m_operation_type = Cage.CageOperationType.FromTerminal Then

          rc = Me.UpdateStackerCollection()

          If rc = ENUM_CONFIGURATION_RC.CONFIGURATION_OK Then
            Call Me.CreateCollectionAlarms()
          End If
        ElseIf Me.IsCountR Then
          rc = Me.RecolectCountRMovement()
        Else
          rc = Me.RecolectMovement()
        End If

      Case OPEN_MODE.CANCEL_MOVEMENT _
         , OPEN_MODE.CANCEL_MOVEMENT_FROM_PROMOBOX_OR_ACCEPTOR _
         , OPEN_MODE.CANCEL_MOVEMENT_FROM_GAMINGTABLES

        rc = Me.CancelMovement()

      Case OPEN_MODE.VIEW_MOVEMENT
        rc = Me.ViewMovement()

    End Select

    Select Case rc

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        _result = CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_PASSWORD
        _result = CLASS_BASE.ENUM_STATUS.STATUS_PASSWORD

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
        _result = ENUM_STATUS.STATUS_DUPLICATE_KEY

      Case Else
        _result = ENUM_STATUS.STATUS_ERROR

    End Select

    Return _result
  End Function ' DB_Update

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As ENUM_STATUS
    Dim rc As Integer

    Select Case Me.OperationType
      Case Cage.CageOperationType.ToCashier _
         , Cage.CageOperationType.ToCustom _
         , Cage.CageOperationType.FromCustom _
         , Cage.CageOperationType.ToGamingTable _
         , Cage.CageOperationType.FromGamingTable _
         , Cage.CageOperationType.FromCashier _
         , Cage.CageOperationType.FromGamingTableClosing _
         , Cage.CageOperationType.FromCashierClosing
        rc = InsertMovementAndDetails()

      Case Cage.CageOperationType.CountCage
        rc = CountCage()

    End Select

    Select Case rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_PASSWORD
        Return CLASS_BASE.ENUM_STATUS.STATUS_PASSWORD

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
        Return ENUM_STATUS.STATUS_DUPLICATE_KEY

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_STOCK_CHANGED
        Return ENUM_STATUS.STATUS_STOCK_ERROR

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select
  End Function ' DB_Insert

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

  End Function ' DB_Delete

  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE
    Dim _temp_cage_control As CLASS_CAGE_CONTROL

    _temp_cage_control = New CLASS_CAGE_CONTROL

    _temp_cage_control.m_movement_id = Me.m_movement_id
    _temp_cage_control.m_user_cage_id = Me.m_user_cage_id
    _temp_cage_control.m_user_cage_id_last_modification = Me.m_user_cage_id_last_modification
    _temp_cage_control.m_user_cashier_id = Me.m_user_cashier_id
    _temp_cage_control.m_terminal_cashier_id = Me.m_terminal_cashier_id
    _temp_cage_control.m_operation_type = Me.m_operation_type
    _temp_cage_control.m_movement_datetime = Me.m_movement_datetime
    _temp_cage_control.m_movement_datetime_last_modification = Me.m_movement_datetime_last_modification
    _temp_cage_control.m_status = Me.m_status
    _temp_cage_control.m_cage_session_id = Me.m_cage_session_id
    _temp_cage_control.m_cage_session_status = Me.m_cage_session_status
    _temp_cage_control.m_target_type = Me.m_target_type
    _temp_cage_control.m_gaming_table_id = Me.m_gaming_table_id
    _temp_cage_control.m_user_custom_id = Me.m_user_custom_id
    _temp_cage_control.m_gaming_table_visits = Me.m_gaming_table_visits
    _temp_cage_control.m_type_new_collection = Me.m_type_new_collection
    _temp_cage_control.m_promobox_or_acceptor = Me.m_promobox_or_acceptor
    _temp_cage_control.m_gui_user_type = Me.m_gui_user_type
    _temp_cage_control.m_cashier_session_id = Me.m_cashier_session_id
    _temp_cage_control.m_open_mode = Me.m_open_mode
    _temp_cage_control.m_operation_type = Me.m_operation_type
    _temp_cage_control.m_send_by_template = Me.m_send_by_template
    _temp_cage_control.m_show_without_cage_id = Me.m_show_without_cage_id

    If Not m_table_currencies Is Nothing AndAlso m_table_currencies.Rows.Count > 0 Then
      _temp_cage_control.m_table_currencies = Me.m_table_currencies.Copy()
    End If

    Return _temp_cage_control
  End Function ' Duplicate

  ' PURPOSE: Returns the related auditor data for the current object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA
    Dim auditor_data As CLASS_AUDITOR_DATA

    Dim _status As String
    Dim _row As DataRow
    Dim _iso_code As String
    Dim _denomination As String
    Dim _quantity As Decimal
    Dim _dv As DataView
    Dim _user_name As String
    Dim _custom_user_name As String
    Dim _operation_name As String
    Dim _currency_type_desc As String
    Dim _nls_id As Integer
    Dim _total_delivery As Decimal

    auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_CAGE)
    _currency_type_desc = ""
    _total_delivery = 0

    Select Case Me.OperationType
      Case Cage.CageOperationType.FromTerminal
        Call auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(4612), String.Empty)
        _operation_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4395)

      Case Cage.CageOperationType.CountCage
        Call auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(4352), String.Empty)
        _operation_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4352)

      Case Cage.CageOperationType.ToCashier
        Call auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(4376), String.Empty)
        _operation_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2890)

      Case Cage.CageOperationType.FromCashier
        Call auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(4377), String.Empty)
        _operation_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2889)

      Case Cage.CageOperationType.FromCashierClosing
        Call auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(7897), String.Empty)
        _operation_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7898)

      Case Cage.CageOperationType.ToCustom
        Call auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(3394), String.Empty)
        _operation_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3446)

      Case Cage.CageOperationType.FromCustom
        Call auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(3395), String.Empty)
        _operation_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3447)

      Case Cage.CageOperationType.ToGamingTable
        Call auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(4391), String.Empty)
        _operation_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4394)

      Case Cage.CageOperationType.FromGamingTable
        Call auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(4392), String.Empty)
        _operation_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4395)

      Case Cage.CageOperationType.FromGamingTableClosing
        Call auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(7899), String.Empty)
        _operation_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7900)

      Case Cage.CageOperationType.FromGamingTableDropbox, _
        Cage.CageOperationType.FromGamingTableDropboxWithExpected
        Call auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(7329), String.Empty)
        _operation_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7329)

      Case Cage.CageOperationType.RequestOperation
        Call auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(4785), String.Empty)
        _operation_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4785)

      Case Else
        Call auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(2876), Me.Name)
        _operation_name = ""

    End Select

    If Me.SendByTemplate Then
      Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5052), Me.TemplateName)
    Else
      Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5052), AUDIT_NONE_STRING)
    End If

    ' Status
    _status = AUDIT_NONE_STRING
    Select Case Me.Status
      Case Cage.CageStatus.Sent
        If Me.OpenMode = OPEN_MODE.RECOLECT_MOVEMENT OrElse Me.OpenMode = OPEN_MODE.RECOLECT_MASSIVE Then
          If Me.OperationType = Cage.CageOperationType.RequestOperation Then
            _status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4786)
          Else
            _status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3011)
          End If
        Else
          _status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2891)
        End If

      Case Cage.CageStatus.OK
        If Me.OperationType = Cage.CageOperationType.RequestOperation Then
          _status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4788)
        Else
          _status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3305)
        End If

      Case Cage.CageStatus.Canceled
        _status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1891)

      Case Cage.CageStatus.OkDenominationImbalance
        If Me.OperationType = Cage.CageOperationType.RequestOperation Then
          _status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4788)
        Else
          _status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3021)
        End If

      Case Cage.CageStatus.OkAmountImbalance
        If Me.OperationType = Cage.CageOperationType.RequestOperation Then
          _status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4788)
        Else
          _status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3022)
        End If


      Case Cage.CageStatus.ErrorTotalImbalance
        _status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3023)

      Case Cage.CageStatus.CanceledDueToImbalanceOrOther
        _status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2893)

      Case Cage.CageStatus.SentToCustom
        _status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3393)

      Case Cage.CageStatus.ReceptionFromCustom
        _status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3305)

      Case Cage.CageStatus.FinishedCountCage
        _status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4732)

    End Select

    ' User Name
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(262), _status)

    If Me.UserCageID = AUDIT_NONE_STRING Then
      _user_name = Me.UserCageID
    Else
      _user_name = GetUserFullName(Me.UserCageID)
    End If
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2896), _user_name)

    Select Case Me.OperationType
      Case Cage.CageOperationType.ToCashier
        If Me.TerminalCashierID <> 0 Then
          Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(3450), GetTerminalCashierName(Me.TerminalCashierID))
        Else
          If Me.UserCashierID = Nothing Then
            _user_name = AUDIT_NONE_STRING
          Else
            _user_name = GetUserFullName(Me.UserCashierID)
          End If
          Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2898), _user_name)
        End If

      Case Cage.CageOperationType.ToCustom, Cage.CageOperationType.FromCustom
        If Me.CustomUserId = Nothing Then
          _custom_user_name = AUDIT_NONE_STRING
        Else
          _custom_user_name = GetCustomUserName(Me.CustomUserId)
        End If
        Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(3393), _custom_user_name)

      Case Cage.CageOperationType.ToGamingTable,
           Cage.CageOperationType.FromGamingTable,
           Cage.CageOperationType.FromGamingTableClosing,
           Cage.CageOperationType.FromGamingTableDropbox,
           Cage.CageOperationType.FromGamingTableDropboxWithExpected
        Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(4396), GetGamingTableName())

    End Select

    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(3306), Me.MovementDatetime)  ' Creation Datetime
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(3388), Cage.GetCageSessionName(Me.CageSessionId))  ' Cage session Id
    Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(627), _operation_name) ' Operation Name

    If Not Me.TableCurrencies Is Nothing AndAlso Me.TableCurrencies.Rows.Count > 0 Then
      _dv = Me.TableCurrencies.DefaultView
      _dv.Sort = "ISO_CODE ASC, TYPE ASC, DENOMINATION DESC"
      Me.TableCurrencies = _dv.ToTable()
      AddExpectedColumnInCurrencies()

      For Each _row In Me.TableCurrencies.Select("QUANTITY <> 0 OR (TOTAL <> 0 AND DENOMINATION = -1) OR CHIP_ID <> 0", "ISO_CODE ASC, TYPE ASC, DENOMINATION DESC")
        _iso_code = _row.Item(DT_COLUMN_CURRENCY.ISO_CODE)
        _nls_id = 3301
        _currency_type_desc = GetCurrencyTypeDescription(_row.Item(DT_COLUMN_CURRENCY.CURRENCY_TYPE))

        Select Case _row.Item(DT_COLUMN_CURRENCY.DENOMINATION)
          Case Cage.COINS_CODE
            _denomination = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2952)
            _quantity = Me.GetRowValue(_row.Item(DT_COLUMN_CURRENCY.TOTAL))

          Case Cage.TICKETS_CODE
            _denomination = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3081)
            _quantity = Me.GetRowValue(_row.Item(DT_COLUMN_CURRENCY.QUANTITY))

          Case Cage.BANK_CARD_CODE
            _denomination = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2558)
            _quantity = Me.GetRowValue(_row.Item(DT_COLUMN_CURRENCY.TOTAL))

          Case Cage.CHECK_CODE
            _denomination = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2594)
            _quantity = Me.GetRowValue(_row.Item(DT_COLUMN_CURRENCY.TOTAL))

          Case Else
            _denomination = GUI_FormatCurrency(_row.Item(DT_COLUMN_CURRENCY.DENOMINATION), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
            _quantity = Me.GetRowValue(_row.Item(DT_COLUMN_CURRENCY.QUANTITY))
            _nls_id = 5674

        End Select

        If _nls_id = 5674 Then
          Call auditor_data.SetField(0, _quantity, GLB_NLS_GUI_PLAYER_TRACKING.GetString(_nls_id, _iso_code, _denomination, _currency_type_desc))
        Else
          Call auditor_data.SetField(0, _quantity, GLB_NLS_GUI_PLAYER_TRACKING.GetString(_nls_id, _iso_code, _denomination))
        End If

      Next
    End If

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    If Me.OperationType = Cage.CageOperationType.FromTerminal Then
      Call auditor_data.SetField(0, Me.m_type_new_collection.real_ticket_count.ToString(), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2842)) ' Tickets recaudados
      Call auditor_data.SetField(0, Me.m_type_new_collection.real_bills_count.ToString(), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2784)) ' Cantidad de billetes
      Call auditor_data.SetField(0, GUI_FormatCurrency((Me.m_type_new_collection.real_bills_sum + Me.m_type_new_collection.real_coins_collection_amount).ToString()), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2783)) ' Total entregado
    End If

    Return auditor_data
  End Function ' AuditorData

#End Region ' Overrides

#Region "Private"

  Private Sub SafeDispose(ByRef Obj As Object, Optional ByVal TryDispose As Boolean = False)
    If TryDispose AndAlso Obj IsNot Nothing AndAlso TypeOf Obj Is IDisposable Then
      CType(Obj, IDisposable).Dispose()
    End If
    Obj = Nothing
    System.GC.Collect()
  End Sub


  Public Function FillTableCurrenciesForCancel(ByVal CollectionDetail As NEW_COLLECTION.TYPE_MONEY_COLLECTION_DETAILS _
                                    , ByVal CurrencyIsoCode As String _
                                    , ByVal CurrentMoneyCollection As NEW_COLLECTION.TYPE_NEW_COLLECTION) As Boolean

    Dim _rows_selected As DataRow()
    Dim _ticket_collection_enabled As Boolean
    Dim _gp_name As String
    Dim _param_array(10) As Object
    Dim _dataview As DataView
    Dim _sort As Boolean
    Dim _automatic_data_fill As Boolean
    Dim _ticket_detail As NEW_COLLECTION.TYPE_MONEY_COLLECTION_DETAILS
    Dim _str_filter As String
    'Private m_list_of_theoretical_tickets As List(Of NEW_COLLECTION.TYPE_MONEY_COLLECTION_DETAILS)
    Dim m_list_of_theoretical_tickets As List(Of NEW_COLLECTION.TYPE_MONEY_COLLECTION_DETAILS)

    m_list_of_theoretical_tickets = New List(Of NEW_COLLECTION.TYPE_MONEY_COLLECTION_DETAILS)
    _ticket_detail = New NEW_COLLECTION.TYPE_MONEY_COLLECTION_DETAILS()
    _gp_name = ""
    _str_filter = ""

    _rows_selected = Nothing

    'filling theoretical ticket values and adding those to the theoretical list of tickets
    _ticket_detail.ticket_validation_number = CollectionDetail.ticket_validation_type
    _ticket_detail.ticket_amount = CollectionDetail.ticket_amount
    _ticket_detail.ticket_id = CollectionDetail.ticket_id
    _ticket_detail.ticket_type = CollectionDetail.ticket_type
    m_list_of_theoretical_tickets.Add(_ticket_detail)

    _ticket_collection_enabled = IIf((Me.OperationType = Cage.CageOperationType.FromCashier Or Me.OperationType = Cage.CageOperationType.FromCashierClosing), _
                                    GeneralParam.GetBoolean("TITO", "Cashier.TicketsCollection"), _
                                    GeneralParam.GetBoolean("TITO", "TicketsCollection"))

    _automatic_data_fill = GeneralParam.GetBoolean("Cage", "Collection.SuggestDenominationAmounts", False) _
                            And (Me.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MOVEMENT _
                              Or Me.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_FROM_PROMOBOX_OR_ACCEPTOR _
                                Or Me.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.NEW_MOVEMENT _
                                  Or Me.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MASSIVE)


    If Me.OperationType <> Cage.CageOperationType.FromCashier And Me.OperationType <> Cage.CageOperationType.FromCashierClosing Then

      If CollectionDetail.bill_denomination <> 0 Then 'AndAlso CollectionDetail.bill_theoretical_count > 0 Then
        _str_filter = String.Format("ISO_CODE = '{0}' AND DENOMINATION = {1} AND TYPE = {2} ", CurrencyIsoCode, GUI_LocalNumberToDBNumber(CollectionDetail.bill_denomination.ToString()), CInt(CollectionDetail.currency_type))
        _rows_selected = Me.TableCurrencies.Select(_str_filter)

        If _rows_selected.Length > 0 Then

          If Not Me.m_new_collecion_mode Then
            If CollectionDetail.bill_denomination > 0 Then
              _rows_selected(0).Item("TOTAL") = CollectionDetail.bill_real_count * CollectionDetail.bill_denomination
              _rows_selected(0).Item("QUANTITY") = CollectionDetail.bill_real_count
            ElseIf CollectionDetail.bill_denomination = 0 And CollectionDetail.currency_type = CageCurrencyType.ChipsColor Then
              _rows_selected(0).Item("TOTAL") = CollectionDetail.bill_real_count
              _rows_selected(0).Item("QUANTITY") = CollectionDetail.bill_real_count
            ElseIf CollectionDetail.bill_denomination < 0 AndAlso CollectionDetail.bill_denomination <> Cage.TICKETS_CODE Then 'cardmoney, check or coins
              _rows_selected(0).Item("TOTAL") = CollectionDetail.bill_real_count
              _rows_selected(0).Item("QUANTITY") = 1
            ElseIf CollectionDetail.bill_denomination = Cage.TICKETS_CODE Then 'tickets has -200 as demonitation in database
              _rows_selected(0).Item("QUANTITY") = CollectionDetail.bill_real_count
            End If
          End If

          _rows_selected(0).Item("CASHIER_QUANTITY") = Me.GetRowValue(CollectionDetail.bill_theoretical_count)
          If CollectionDetail.bill_denomination > 0 Then
            _rows_selected(0).Item("CASHIER_TOTAL") = Me.GetRowValue(_rows_selected(0).Item("CASHIER_QUANTITY") * CollectionDetail.bill_denomination)
          ElseIf CollectionDetail.bill_denomination = 0 And CollectionDetail.currency_type = CageCurrencyType.ChipsColor Then
            _rows_selected(0).Item("CASHIER_TOTAL") = Me.GetRowValue(_rows_selected(0).Item("CASHIER_QUANTITY"))
          End If

          ' automatic terminal data fill.
          If _automatic_data_fill And Me.m_new_collecion_mode Then
            _rows_selected(0).Item("QUANTITY") = Me.GetRowValue(_rows_selected(0).Item("CASHIER_QUANTITY"))
            _rows_selected(0).Item("TOTAL") = Me.GetRowValue(_rows_selected(0).Item("CASHIER_TOTAL"))
          End If

        Else ' The denomination doesn't exist in cage denomination
          _param_array(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.ISO_CODE) = CurrencyIsoCode
          _param_array(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.DENOMINATION) = CollectionDetail.bill_denomination
          _param_array(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.CURRENCY_TYPE) = CInt(CollectionDetail.currency_type)
          _param_array(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.DENOMINATION_WITH_SIMBOL) = CollectionDetail.bill_denomination
          _param_array(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.QUANTITY) = CollectionDetail.bill_real_count
          _param_array(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.TOTAL) = CollectionDetail.bill_real_count * CollectionDetail.bill_denomination
          _param_array(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.TOTAL_WITH_SIMBOL) = 0
          _param_array(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.CASHIER_QUANTITY) = CollectionDetail.bill_theoretical_count
          _param_array(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.CASHIER_TOTAL) = CollectionDetail.bill_theoretical_count * CollectionDetail.bill_denomination
          _param_array(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.QUANTITY_DIFFERENCE) = 0
          _param_array(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.TOTAL_DIFFERENCE) = 0
          Me.TableCurrencies.Rows.Add(_param_array)
          _sort = True
        End If

        If _sort Then
          _dataview = Me.TableCurrencies.DefaultView
          _dataview.Sort = "ISO_CODE, TYPE ASC, DENOMINATION DESC"
          Me.TableCurrencies = _dataview.ToTable()
          AddExpectedColumnInCurrencies()
        End If

      ElseIf CollectionDetail.ticket_id > 0 Then
        _str_filter = String.Format("ISO_CODE = '{0}' AND DENOMINATION = {1} ", CurrencyIsoCode, Cage.TICKETS_CODE)
        _rows_selected = Me.TableCurrencies.Select(_str_filter)
        _rows_selected(0).Item("CASHIER_QUANTITY") += 1
        If _rows_selected(0).Item("QUANTITY") Is DBNull.Value Then
          _rows_selected(0).Item("QUANTITY") = 0
        End If
        If (Not _ticket_collection_enabled _
                                           AndAlso Me.m_new_collecion_mode) _
                                           OrElse (CollectionDetail.money_collection_id = CurrentMoneyCollection.money_collection_id _
                                           AndAlso CollectionDetail.ticketd_collected_money_collection = CurrentMoneyCollection.money_collection_id) Then
          _rows_selected(0).Item("QUANTITY") += 1 'JCA tickets + 1
        End If

      End If
    End If

    ' Dispose
    Call SafeDispose(_rows_selected)
    Call SafeDispose(_ticket_detail)

    Return True
  End Function ' GUI_SetupRow

  '----------------------------------------------------------------------------
  ' PURPOSE: Checks if the movement still has the same status
  '
  ' PARAMS:
  '       - MovID
  '
  ' RETURNS:
  '       - True if it has the same status.
  '
  ' NOTES:
  Public Function CheckStatusForCancel(ByVal MovID As Long) As Boolean
    Dim _sb As StringBuilder
    Dim _obj As Object

    _sb = New StringBuilder

    _sb.AppendLine("SELECT    CGM_STATUS ")
    _sb.AppendLine("  FROM    CAGE_MOVEMENTS ")
    _sb.AppendLine(" WHERE    CGM_MOVEMENT_ID = @pMovementId ")
    _sb.AppendLine("   AND   (CGM_STATUS = @pOk")
    _sb.AppendLine("    OR    CGM_STATUS = @pOkAmountImbalance")
    _sb.AppendLine("    OR    CGM_STATUS = @pOkDenominationImbalance)")

    Try

      Using _db_trx As New DB_TRX()
        Using _cmd As New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _cmd.Parameters.Add("@pMovementId", SqlDbType.BigInt).Value = MovID
          _cmd.Parameters.Add("@pOk", SqlDbType.Int).Value = Cage.CageStatus.OK
          _cmd.Parameters.Add("@pOkAmountImbalance", SqlDbType.Int).Value = Cage.CageStatus.OkAmountImbalance
          _cmd.Parameters.Add("@pOkDenominationImbalance", SqlDbType.Int).Value = Cage.CageStatus.OkDenominationImbalance

          _obj = _cmd.ExecuteScalar()

          If _obj IsNot Nothing AndAlso _obj IsNot DBNull.Value Then
            Return True
          End If

        End Using
      End Using

    Catch _ex As Exception
      Call Trace.WriteLine(_ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "cls_cage_control", _
                            "CheckStatusForCancel", _
                            _ex.Message)

    End Try

    Return False

  End Function


  ' PURPOSE: To cancel the movement opened in cancel mode
  '
  ' PARAMS:
  '
  ' RETURNS:
  '     - db result
  '
  ' NOTES:
  Public Function CancelMovementExternalCall(ByVal MoneyCollectionId As Int64, ByVal CashierSessionId As Long) As Integer
    Dim _row As DataRow
    Dim _currency_iso_code As String

    Try

      'Set parameters
      Me.SourceTargetId = CageMeters.CageSystemSourceTarget.Terminals
      Me.OperationType = Cage.CageOperationType.FromTerminal
      Me.TargetType = CageMeters.CageSourceTargetType.CageInput
      Me.m_type_new_collection.money_collection_id = MoneyCollectionId
      Me.m_cashier_session_id = CashierSessionId
      Me.m_new_collecion_mode = False
      Me.OpenMode = OPEN_MODE.CANCEL_MOVEMENT
      Me.Status = WSI.Common.Cage.CageStatus.Canceled
      Me.MovementType = 0

      _currency_iso_code = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode")

      'Set the MovementID
      If Me.ReadMovementIdByMoneyCollection() Then

        'If table_currencies_copy is empty copy to TableCurrencies
        If Me.m_table_currencies_copy Is Nothing OrElse Me.m_table_currencies_copy.Rows.Count <= 0 Then
          InitCurrencyData(True)
          Me.m_table_currencies_copy = New DataTable
          Me.m_table_currencies_copy = TableCurrencies.Copy
        End If
        'And clear TableCurrencies of m_table_currencies_copy and not another fill TableCurrencies
        Me.TableCurrencies = m_table_currencies_copy.Copy

        'Fill collection money
        If Me.SelectReadStackerCollectionMode() Then
          'Check if the recaudation not is cancel
          If Me.CheckStatusForCancel(Me.MovementID) Then
            'Fill collection details
            For Each _detail As NEW_COLLECTION.TYPE_MONEY_COLLECTION_DETAILS In Me.m_type_new_collection.collection_details
              Call FillTableCurrenciesForCancel(_detail, _currency_iso_code, Me.m_type_new_collection)
            Next

            'Change the columns table of table currency '0' to empty
            For Each _row In Me.TableCurrencies.Rows
              Call Me.CheckForZero(_row)
            Next

            'Cancel collection
            Return Me.CancelMovement()
          End If
        End If
      End If


    Catch ex As Exception
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "cls_cage_control", _
                            "CancelMovementExternalCall", _
                            ex.Message)
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR

  End Function ' CancelMovement

  ' PURPOSE: To cancel the movement opened in cancel mode
  '
  ' PARAMS:
  '
  ' RETURNS:
  '     - db result
  '
  ' NOTES:

  Private Function CancelMovement() As Integer
    Dim rc As Integer

    Using _db_trx As New DB_TRX()
      If Me.SourceTargetId = CageMeters.CageSystemSourceTarget.Cashiers And Me.MovementType = 0 Then
        If Not CageMeters.UndoCageMeters(Me.MovementID, True, _db_trx.SqlTransaction) Then
          rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If
      Else
        If Not CageMeters.UndoCageMeters(Me.MovementID, False, _db_trx.SqlTransaction) Then
          rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If
      End If

      If rc <> ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB Then

        If Not Me.UpdateCageStockWhenCancel(_db_trx.SqlTransaction) Then
          rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        Else
          If Me.OperationType = Cage.CageOperationType.FromCashier _
            Or Me.OperationType = Cage.CageOperationType.FromCashierClosing _
            Or Me.OperationType = Cage.CageOperationType.FromTerminal _
            Or Me.OperationType = Cage.CageOperationType.FromGamingTable _
            Or Me.OperationType = Cage.CageOperationType.FromGamingTableClosing _
            Or Me.OperationType = Cage.CageOperationType.FromGamingTableDropbox _
            Or Me.OperationType = Cage.CageOperationType.FromGamingTableDropboxWithExpected Then

            rc = Me.CancelCollectionMovement(_db_trx.SqlTransaction)

            If rc = ENUM_CONFIGURATION_RC.CONFIGURATION_OK Then
              If (Me.IsPromoBoxOrAcceptor OrElse TITO.Utils.IsTitoMode()) And Me.m_type_new_collection.money_collection_id > 0 AndAlso _
                (Me.OperationType <> Cage.CageOperationType.FromGamingTableDropbox And Me.OperationType <> Cage.CageOperationType.FromGamingTableDropboxWithExpected) Then
                rc = Me.CancelMoneyCollection(_db_trx.SqlTransaction)
              End If
            End If

          Else
            rc = Me.UpdateToVoidMovement(_db_trx.SqlTransaction)
          End If
        End If

        ' Check CountR Terminal
        Me.IsCountR = Me.IsCountRTerminal(_db_trx.SqlTransaction)

        ' Insert movements
        If Me.IsCountR Then
          ' JBP: Undo collected amount
          rc = Me.CountR.UndoCollectedAmount(m_cashier_session_id, _db_trx.SqlTransaction)
        End If

        ' Reset drop box values
        If Me.OperationType = Cage.CageOperationType.FromGamingTableDropbox Or Me.OperationType = Cage.CageOperationType.FromGamingTableDropboxWithExpected Then
          If Not GamingTablesSessions.ResetGamblingTableSessionsDropbox(Me.CashierSessionId, _db_trx.SqlTransaction) Then
            rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR
          End If
        End If

        If rc = ENUM_CONFIGURATION_RC.CONFIGURATION_OK Then
          Call _db_trx.Commit()
        Else
          Call _db_trx.Rollback()
        End If

      End If

    End Using
    If rc <> ENUM_CONFIGURATION_RC.CONFIGURATION_OK Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4929), ENUM_MB_TYPE.MB_TYPE_ERROR)
    End If

    Return rc

  End Function ' CancelMovement

  Private Function CancelMoneyCollection(ByVal SqlTrx As SqlTransaction)
    Dim _sb As StringBuilder
    Dim _result As Integer
    Dim _currency_iso_code As String
    Dim _bill_amount As Object
    Dim _bill_count As Object
    Dim _is_tito_mode As Boolean

    _sb = New StringBuilder
    _currency_iso_code = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode")
    _result = ENUM_CONFIGURATION_RC.CONFIGURATION_OK
    _bill_amount = 0
    _bill_count = 0

    ' DHA 13-JAN-2016: get terminal currency
    If WSI.Common.Misc.IsFloorDualCurrencyEnabled() And m_operation_type = Cage.CageOperationType.FromTerminal Then
      _currency_iso_code = Me.TerminalIsoCode
    End If

    _is_tito_mode = TITO.Utils.IsTitoMode()

    _sb.AppendLine(" DELETE FROM   MONEY_COLLECTION_DETAILS ")
    _sb.AppendLine("       WHERE   MCD_COLLECTION_ID = @pMoneyCollectionId ;")

    _sb.AppendLine("DECLARE   @pCollectedTicketAmount        AS MONEY   ")
    _sb.AppendLine("DECLARE   @pCollectedTicketCount         AS INTEGER ")
    _sb.AppendLine("DECLARE   @pCollectedReTicketAmount      AS MONEY   ")
    _sb.AppendLine("DECLARE   @pCollectedReTicketCount       AS INTEGER ")
    _sb.AppendLine("DECLARE   @pCollectedPromoReTicketAmount AS MONEY   ")
    _sb.AppendLine("DECLARE   @pCollectedPromoReTicketCount  AS INTEGER ")
    _sb.AppendLine("DECLARE   @pCollectedPromoNrTicketAmount AS MONEY   ")
    _sb.AppendLine("DECLARE   @pCollectedPromoNrTicketCount  AS INTEGER ")

    _sb.AppendLine("SET       @pCollectedTicketAmount        = 0")
    _sb.AppendLine("SET       @pCollectedTicketCount         = 0")
    _sb.AppendLine("SET       @pCollectedReTicketAmount      = 0")
    _sb.AppendLine("SET       @pCollectedReTicketCount       = 0")
    _sb.AppendLine("SET       @pCollectedPromoReTicketAmount = 0")
    _sb.AppendLine("SET       @pCollectedPromoReTicketCount  = 0")
    _sb.AppendLine("SET       @pCollectedPromoNrTicketAmount = 0")
    _sb.AppendLine("SET       @pCollectedPromoNrTicketCount  = 0")

    If Me.OperationType = Cage.CageOperationType.FromCashier _
      Or Me.OperationType = Cage.CageOperationType.FromCashierClosing _
      Or Me.OperationType = Cage.CageOperationType.FromGamingTable _
      Or Me.OperationType = Cage.CageOperationType.FromGamingTableClosing Then

      If _is_tito_mode Then
        _sb.AppendLine(" SELECT   @pCollectedReTicketAmount = SUM (CASE WHEN TI_TYPE_ID IN (@pCashable, @pHandpay, @pJackpot) OR (TI_TYPE_ID = @pOffline AND TI_STATUS = @pRedeemed) THEN TI_AMOUNT ELSE 0 END) ")
        _sb.AppendLine("  	    , @pCollectedPromoReTicketAmount = SUM (CASE WHEN TI_TYPE_ID = @pPromoRE THEN TI_AMOUNT ELSE 0 END) ")
        _sb.AppendLine("		    , @pCollectedPromoNrTicketAmount = SUM (CASE WHEN TI_TYPE_ID = @pPromoNR OR (TI_TYPE_ID = @pOffline AND TI_STATUS = @pDiscarded) THEN TI_AMOUNT ELSE 0 END) ")
        _sb.AppendLine("		    , @pCollectedTicketAmount = @pCollectedReTicketAmount + @pCollectedPromoReTicketAmount + @pCollectedPromoNrTicketAmount ")
        _sb.AppendLine("		    , @pCollectedReTicketCount = SUM (CASE WHEN TI_TYPE_ID IN (@pCashable, @pHandpay, @pJackpot) OR (TI_TYPE_ID = @pOffline AND TI_STATUS = @pRedeemed) THEN 1 ELSE 0 END) ")
        _sb.AppendLine("		    , @pCollectedPromoReTicketCount = SUM (CASE WHEN TI_TYPE_ID = @pPromoRE THEN 1 ELSE 0 END) ")
        _sb.AppendLine("		    , @pCollectedPromoNrTicketCount = SUM (CASE WHEN TI_TYPE_ID = @pPromoNR OR (TI_TYPE_ID = @pOffline AND TI_STATUS = @pDiscarded) THEN 1 ELSE 0 END) ")
        _sb.AppendLine("		    , @pCollectedTicketCount = @pCollectedReTicketCount + @pCollectedPromoReTicketCount + @pCollectedPromoNrTicketCount ")
        _sb.AppendLine("   FROM   TICKETS ")
        _sb.AppendLine("  WHERE   TI_CAGE_MOVEMENT_ID = @pCageMovementId ;")
      End If

      Call Me.UpdateCancelFromCashier(_sb)

      _bill_amount = Me.TableCurrencies.Compute("Sum(TOTAL)", "ISO_CODE = '" & _currency_iso_code & "' AND ( DENOMINATION > 0 OR DENOMINATION = " & Cage.COINS_CODE & " )")
      If _bill_amount Is Nothing OrElse IsDBNull(_bill_amount) Then
        _bill_amount = 0
      End If
      _bill_count = Me.TableCurrencies.Compute("Sum(QUANTITY)", "ISO_CODE = '" & _currency_iso_code & "' AND DENOMINATION > 0")
      If _bill_count Is Nothing OrElse IsDBNull(_bill_count) Then
        _bill_count = 0
      End If

    ElseIf Me.OperationType = Cage.CageOperationType.FromTerminal Then

      Call Me.UpdateCancelFromMoneyCollection(_sb)

    End If

    If _is_tito_mode Then
      _sb.AppendLine(" UPDATE   TICKETS ")
      _sb.AppendLine("    SET   TI_COLLECTED_MONEY_COLLECTION = NULL ,")
      _sb.AppendLine("          TI_CAGE_MOVEMENT_ID = NULL ")
      _sb.AppendLine("  WHERE   TI_CAGE_MOVEMENT_ID = @pCageMovementId ;")
    End If

    Try

      Using _cmd As New SqlClient.SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx)

        _cmd.Parameters.Add("@pMoneyCollectionId", SqlDbType.BigInt).Value = Me.m_type_new_collection.money_collection_id
        _cmd.Parameters.Add("@pMcStatus", SqlDbType.Int).Value = TITO_MONEY_COLLECTION_STATUS.PENDING
        _cmd.Parameters.Add("@pCageMovementId", SqlDbType.BigInt).Value = Me.MovementID
        _cmd.Parameters.Add("@pBillAmount", SqlDbType.Money).Value = CDec(_bill_amount)
        _cmd.Parameters.Add("@pBillCount", SqlDbType.Int).Value = CInt(_bill_count)
        _cmd.Parameters.Add("@pCashable", SqlDbType.Int).Value = TITO_TICKET_TYPE.CASHABLE
        _cmd.Parameters.Add("@pHandpay", SqlDbType.Int).Value = TITO_TICKET_TYPE.HANDPAY
        _cmd.Parameters.Add("@pJackpot", SqlDbType.Int).Value = TITO_TICKET_TYPE.JACKPOT
        _cmd.Parameters.Add("@pOffline", SqlDbType.Int).Value = TITO_TICKET_TYPE.OFFLINE
        _cmd.Parameters.Add("@pPromoRE", SqlDbType.Int).Value = TITO_TICKET_TYPE.PROMO_REDEEM
        _cmd.Parameters.Add("@pPromoNR", SqlDbType.Int).Value = TITO_TICKET_TYPE.PROMO_NONREDEEM
        _cmd.Parameters.Add("@pRedeemed", SqlDbType.Int).Value = TITO_TICKET_STATUS.REDEEMED
        _cmd.Parameters.Add("@pDiscarded", SqlDbType.Int).Value = TITO_TICKET_STATUS.DISCARDED

        If _cmd.ExecuteNonQuery() = 0 Then
          _result = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR
        Else
          If Me.OperationType = Cage.CageOperationType.FromTerminal Then
            _result = RollbackCashierSessionInfo(SqlTrx)
          End If
        End If

      End Using

    Catch ex As Exception
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "cls_cage_control", _
                            "CancelMoneyCollection", _
                            ex.Message)

      _result = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR
    End Try

    Return _result
  End Function ' CancelMoneyCollection

  Private Sub UpdateCancelFromCashier(ByRef Sb As StringBuilder)

    Sb.AppendLine(" UPDATE   MONEY_COLLECTIONS ")
    Sb.AppendLine("    SET   MC_USER_ID = NULL ,")
    Sb.AppendLine("          MC_COLLECTION_DATETIME = NULL ,")
    Sb.AppendLine("          MC_STATUS = @pMcStatus ,")
    Sb.AppendLine("          MC_COLLECTED_BILL_AMOUNT = MC_COLLECTED_BILL_AMOUNT - @pBillAmount,")
    Sb.AppendLine("          MC_COLLECTED_BILL_COUNT = MC_COLLECTED_BILL_COUNT - @pBillCount,")
    Sb.AppendLine("          MC_COLLECTED_TICKET_AMOUNT = MC_COLLECTED_TICKET_AMOUNT - @pCollectedTicketAmount,")
    Sb.AppendLine("          MC_COLLECTED_TICKET_COUNT = MC_COLLECTED_TICKET_COUNT - @pCollectedTicketCount,")
    Sb.AppendLine("          MC_COLLECTED_RE_TICKET_AMOUNT = MC_COLLECTED_RE_TICKET_AMOUNT - @pCollectedReTicketAmount,")
    Sb.AppendLine("          MC_COLLECTED_RE_TICKET_COUNT = MC_COLLECTED_RE_TICKET_COUNT - @pCollectedReTicketCount,")
    Sb.AppendLine("          MC_COLLECTED_PROMO_RE_TICKET_AMOUNT = MC_COLLECTED_PROMO_RE_TICKET_AMOUNT - @pCollectedPromoReTicketAmount,")
    Sb.AppendLine("          MC_COLLECTED_PROMO_RE_TICKET_COUNT = MC_COLLECTED_PROMO_RE_TICKET_COUNT - @pCollectedPromoReTicketCount,")
    Sb.AppendLine("          MC_COLLECTED_PROMO_NR_TICKET_AMOUNT = MC_COLLECTED_PROMO_NR_TICKET_AMOUNT - @pCollectedPromoNrTicketAmount,")
    Sb.AppendLine("          MC_COLLECTED_PROMO_NR_TICKET_COUNT = MC_COLLECTED_PROMO_NR_TICKET_COUNT - @pCollectedPromoNrTicketCount")
    Sb.AppendLine("  WHERE   MC_COLLECTION_ID = @pMoneyCollectionId ;")

  End Sub ' UpdateCancelFromCashier

  Private Sub UpdateCancelFromMoneyCollection(ByRef Sb As StringBuilder)

    Sb.AppendLine(" UPDATE   MONEY_COLLECTIONS ")
    Sb.AppendLine("    SET   MC_USER_ID = NULL ,")
    Sb.AppendLine("          MC_COLLECTION_DATETIME = NULL ,")
    Sb.AppendLine("          MC_STATUS = @pMcStatus ,")
    Sb.AppendLine("          MC_COLLECTED_BILL_AMOUNT = 0,")
    Sb.AppendLine("          MC_COLLECTED_BILL_COUNT = 0,")
    Sb.AppendLine("          MC_COLLECTED_TICKET_AMOUNT = @pCollectedTicketAmount,")
    Sb.AppendLine("          MC_COLLECTED_TICKET_COUNT = @pCollectedTicketCount,")
    Sb.AppendLine("          MC_COLLECTED_RE_TICKET_AMOUNT = @pCollectedReTicketAmount,")
    Sb.AppendLine("          MC_COLLECTED_RE_TICKET_COUNT = @pCollectedReTicketCount,")
    Sb.AppendLine("          MC_COLLECTED_PROMO_RE_TICKET_AMOUNT = @pCollectedPromoReTicketAmount,")
    Sb.AppendLine("          MC_COLLECTED_PROMO_RE_TICKET_COUNT = @pCollectedPromoReTicketCount,")
    Sb.AppendLine("          MC_COLLECTED_PROMO_NR_TICKET_AMOUNT = @pCollectedPromoNrTicketAmount,")
    Sb.AppendLine("          MC_COLLECTED_PROMO_NR_TICKET_COUNT = @pCollectedPromoNrTicketCount")
    Sb.AppendLine("  WHERE   MC_COLLECTION_ID = @pMoneyCollectionId ;")

  End Sub ' UpdateCancelFromMoneyCollection

  Private Function RollbackCashierSessionInfo(ByVal SqlTrx As SqlTransaction) As ENUM_CONFIGURATION_RC

    Dim _sb As StringBuilder
    Dim _balance As Currency

    _balance = Currency.Zero()
    _sb = New StringBuilder()

    Try
      _sb.AppendLine("DECLARE @pMB_CASH_MovId AS BIGINT                         ")
      _sb.AppendLine("DECLARE @pCLOSE_MovId AS BIGINT                           ")
      _sb.AppendLine("                                                          ")
      _sb.AppendLine("SELECT   @pMB_CASH_MovId = MAX(CM_MOVEMENT_ID)            ")
      _sb.AppendLine("  FROM   CASHIER_MOVEMENTS                                ")
      _sb.AppendLine(" WHERE   CM_SESSION_ID = @pCashierSessionId               ")
      _sb.AppendLine("   AND   CM_TYPE = @MB_CLOSE_TYPE                         ")
      _sb.AppendLine("SELECT   @pCLOSE_MovId = MAX(CM_MOVEMENT_ID)              ")
      _sb.AppendLine("  FROM   CASHIER_MOVEMENTS                                ")
      _sb.AppendLine(" WHERE   CM_SESSION_ID = @pCashierSessionId               ")
      _sb.AppendLine("   AND   CM_TYPE = @CS_CLOSE_TYPE                         ")
      _sb.AppendLine("                                                          ")
      _sb.AppendLine("UPDATE   CASHIER_MOVEMENTS                                ")
      _sb.AppendLine("   SET   CM_UNDO_STATUS = @pUndoStatus                    ")
      _sb.AppendLine(" WHERE   CM_SESSION_ID = @pCashierSessionId               ")
      _sb.AppendLine("   AND   CM_MOVEMENT_ID IN (@pMB_CASH_MovId,@pCLOSE_MovId)")

      _sb.AppendLine(" UPDATE   CASHIER_SESSIONS ")
      _sb.AppendLine("    SET   CS_CLOSING_DATE = NULL ")
      _sb.AppendLine("        , CS_STATUS = @pPendingClosing ")
      _sb.AppendLine("        , CS_COLLECTED_AMOUNT = 0")
      _sb.AppendLine("  WHERE   CS_SESSION_ID = @pCashierSessionId ;")


      Using _cmd As New SqlClient.SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx)

        _cmd.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).Value = Me.CashierSessionId
        _cmd.Parameters.Add("@pDepositCash", SqlDbType.Int).Value = MBMovementType.DepositCash
        _cmd.Parameters.Add("@pPendingClosing", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.PENDING_CLOSING
        _cmd.Parameters.Add("@pUndoStatus", SqlDbType.Int).Value = UndoStatus.Undone
        _cmd.Parameters.Add("@CS_CLOSE_TYPE", SqlDbType.Int).Value = CASHIER_MOVEMENT.CLOSE_SESSION
        _cmd.Parameters.Add("@MB_CLOSE_TYPE", SqlDbType.Int).Value = CASHIER_MOVEMENT.MB_CASH_IN

        If _cmd.ExecuteNonQuery() = 0 Then
          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR

        End If

      End Using

      If m_type_new_collection.real_bills_sum > 0 Then
        If Not mdl_tito.NACardCashDeposit(Me.CashierSessionId, Me.GuiUserType _
                                        , Decimal.Negate(m_type_new_collection.real_bills_sum) _
                                        , Me.m_type_new_collection.terminal_id _
                                        , SqlTrx) Then

          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR
        End If
      End If

      If Not CloseSessionTITO(SqlTrx) Then

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR
      End If

      Using _cmd As New SqlClient.SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx)

        _cmd.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).Value = Me.CashierSessionId
        _cmd.Parameters.Add("@pDepositCash", SqlDbType.Int).Value = MBMovementType.DepositCash
        _cmd.Parameters.Add("@pPendingClosing", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.PENDING_CLOSING
        _cmd.Parameters.Add("@pUndoStatus", SqlDbType.Int).Value = UndoStatus.UndoOperation
        _cmd.Parameters.Add("@CS_CLOSE_TYPE", SqlDbType.Int).Value = CASHIER_MOVEMENT.CLOSE_SESSION
        _cmd.Parameters.Add("@MB_CLOSE_TYPE", SqlDbType.Int).Value = CASHIER_MOVEMENT.MB_CASH_IN

        If _cmd.ExecuteNonQuery() = 0 Then
          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR

        End If

      End Using

    Catch ex As Exception
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "cls_cage_control", _
                            "RollbackCashierSessionInfo", _
                            ex.Message)

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
  End Function

  ' PURPOSE: To cancel the movement opened in view mode
  '
  ' PARAMS:
  '
  ' RETURNS:
  '     - db result
  '
  ' NOTES:
  Private Function ViewMovement() As Integer
    Dim rc As Integer

    Using _db_trx As New DB_TRX()

      If Me.OperationType = Cage.CageOperationType.FromCashier Or Me.OperationType = Cage.CageOperationType.FromCashierClosing Then
        If Me.UpdateMovementStatus(Cage.CageStatus.Sent, _db_trx.SqlTransaction) Then
          rc = ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Else
          rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If
      Else
        rc = Me.UpdateToVoidMovement(_db_trx.SqlTransaction)
      End If

      If rc = ENUM_CONFIGURATION_RC.CONFIGURATION_OK Then
        Call _db_trx.Commit()
      Else
        Call _db_trx.Rollback()
      End If

    End Using

    If rc <> ENUM_CONFIGURATION_RC.CONFIGURATION_OK Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3037), ENUM_MB_TYPE.MB_TYPE_INFO)
    End If

    Return rc
  End Function ' ViewMovement

  ''' <summary>
  ''' Insert movement and details into database
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function InsertMovementAndDetails() As Integer

    Dim _result As ENUM_CONFIGURATION_RC

    Dim _is_collection As Boolean = Me.MovementType
    Dim _nls_id As Integer

    _result = ENUM_CONFIGURATION_RC.CONFIGURATION_OK

    Using _db_trx As New DB_TRX()

      Try

        If Me.CageSessionId = 0 Then
          _result = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

        ' Check CountR Terminal
        Me.IsCountR = Me.IsCountRTerminal(_db_trx.SqlTransaction)

        ' Insert movements
        If Me.IsCountR Then
          _result = Me.CountR.AddMovementAndDetails(Me, _db_trx.SqlTransaction)
        End If

        If _result = ENUM_CONFIGURATION_RC.CONFIGURATION_OK Then
          _result = Me.InsertGenericMovementAndDetails(_db_trx.SqlTransaction)
        End If

        ' Process result
        If _result = ENUM_CONFIGURATION_RC.CONFIGURATION_OK Then

          Call _db_trx.Commit()
          'ATB 20-10-2016
          _nls_id = IIf(_is_collection, GLB_NLS_GUI_PLAYER_TRACKING.Id(3303), GLB_NLS_GUI_PLAYER_TRACKING.Id(3034))
          NLS_MsgBox(_nls_id, ENUM_MB_TYPE.MB_TYPE_INFO)
        Else

          Call _db_trx.Rollback()
          If _result = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND Then
            _nls_id = GLB_NLS_GUI_PLAYER_TRACKING.Id(7402) 'CountR not enabled
          Else
            _nls_id = IIf(_is_collection, GLB_NLS_GUI_PLAYER_TRACKING.Id(3035), GLB_NLS_GUI_PLAYER_TRACKING.Id(3304))
          End If

          NLS_MsgBox(_nls_id, ENUM_MB_TYPE.MB_TYPE_WARNING)
        End If

        Return _result

      Catch ex As Exception

        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3304), ENUM_MB_TYPE.MB_TYPE_WARNING)

        Log.Exception(ex)
        Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                              "cls_cage_control", _
                              "InsertMovementAndDetails", _
                              ex.Message)

        Call _db_trx.Rollback()

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End Try

    End Using
  End Function ' InsertMovementAndDetails

  ''' <summary>
  ''' Insert generic movements and details into database
  ''' </summary>
  ''' <param name="SqlTrx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function InsertGenericMovementAndDetails(ByVal SqlTrx As SqlTransaction) As ENUM_CONFIGURATION_RC
    Dim _cashier_session_id As Long
    Dim _has_integrated_cashier As Boolean

    If Not Me.HasIntegratedCashier(Me.GamingTableId, _has_integrated_cashier) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    If (Me.OperationType = Cage.CageOperationType.ToGamingTable _
        Or Me.OperationType = Cage.CageOperationType.FromGamingTable _
        Or Me.OperationType = Cage.CageOperationType.FromGamingTableClosing) _
       And Not _has_integrated_cashier Then

      'primero recuperar una sesion de caja v�lida
      If (Cashier.ExistCashierTerminalSessionOpen(GetRelatedCashierIdByGamingTableId(), _cashier_session_id, SqlTrx)) Then
        Me.CashierSessionId = _cashier_session_id
      End If
    End If

    ' Inserts movement
    If Not InsertMovement(SqlTrx) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    ' Inserts movement details
    If Not InsertMovementDetails(SqlTrx, Me.TableCurrencies) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    If Not Me.OperationType = Cage.CageOperationType.FromCustom And Not Me.OperationType = Cage.CageOperationType.ToCustom Then 'And Not Me.IsCountR Then
      ' Inserts pending movement
      If ((Me.OperationType = Cage.CageOperationType.ToGamingTable _
          Or Me.OperationType = Cage.CageOperationType.FromGamingTable _
          Or Me.OperationType = Cage.CageOperationType.FromGamingTableClosing) _
         And Not _has_integrated_cashier) Then

        If Me.OperationType = Cage.CageOperationType.ToGamingTable And _
           GamingTableBusinessLogic.GamingTablesMode() <> GamingTableBusinessLogic.GT_MODE.GUI_AND_CASHIER _
        AndAlso Not InsertMovementVirtualGamingTable(SqlTrx) Then

          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

        If (Me.OperationType = Cage.CageOperationType.FromGamingTable _
          Or Me.OperationType = Cage.CageOperationType.FromGamingTableClosing) _
               AndAlso Not CollectMovementVirtualGamingTable(Me.ReceptionAndClose, SqlTrx) Then

          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If
      End If

      If (_has_integrated_cashier OrElse _
           Not _has_integrated_cashier AndAlso _
           GamingTableBusinessLogic.GamingTablesMode() = GamingTableBusinessLogic.GT_MODE.GUI_AND_CASHIER OrElse _
           Me.OperationType = Cage.CageOperationType.ToCashier OrElse _
           Me.OperationType = Cage.CageOperationType.FromCashier OrElse _
           Me.OperationType = Cage.CageOperationType.FromCashierClosing) Then

        If Me.IsCountR AndAlso _
          (Me.OperationType = Cage.CageOperationType.ToCashier OrElse _
           Me.OperationType = Cage.CageOperationType.FromCashier OrElse _
           Me.OperationType = Cage.CageOperationType.FromCashierClosing) Then

          ' We don't insert a pending movement because this, changes status to finished directly.
        Else
          If Not InsertPendingMovement(SqlTrx) Then

            Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
          End If
        End If
      End If
    End If
    ' We update the last time we used the template
    If Me.TemplateID > 0 Then
      If UpdateTemplateLastUsed(Me.TemplateID, SqlTrx) = False Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If
    End If

    ' Avoid to update cage concepts from gamingtable when movement is Sent
    If Not (Me.m_status = Cage.CageStatus.Sent AndAlso (Me.OperationType = Cage.CageOperationType.FromGamingTable Or Me.OperationType = Cage.CageOperationType.FromGamingTableClosing)) Then
      Return Me.UpdateCageMeters(SqlTrx)
    End If

  End Function

  ''' <summary>
  ''' Check if is CountR and set CountRId
  ''' </summary>
  ''' <param name="SqlTrx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function IsCountRTerminal() As Boolean

    Using _db_trx As New DB_TRX()
      Return IsCountRTerminal(_db_trx.SqlTransaction)
    End Using

  End Function ' IsCountRTerminal

  Public Function IsCountRTerminal(ByVal TerminalCashierID As Integer, ByVal SqlTrx As SqlTransaction) As Boolean
    ' Only for terminals selecteds in frm cage control
    Me.TerminalCashierID = TerminalCashierID
    Return IsCountRTerminal(SqlTrx)
  End Function ' IsCountRTerminal

  Public Function IsCountRTerminal(ByVal SqlTrx As SqlTransaction) As Boolean
    Dim _countR As CountR

    _countR = New CountR

    If Not Me.IsCountREnabled Then
      Return False
    End If

    If Me.TerminalCashierID >= COUNTR_ID_OFFSET Then
      Me.CountR.Id = Me.TerminalCashierID - COUNTR_ID_OFFSET

      If Not Cashier.GetCashierTerminalIdFromCountR(Me.CountR.Id, Me.TerminalCashierID, SqlTrx) Then
        'Fixed Bug 16884:CountR: error en los env�os de dinero a CountR recien creados.
        If (WSI.Common.CountR.CountR.DB_GetCountR(Me.CountR.Id, _countR, SqlTrx)) Then                                                 ' Fixed Bug 16884. Get CountR Data
          Me.TerminalCashierID = Cashier.ReadTerminalId(SqlTrx, _countR.Name, GU_USER_TYPE.SYS_REDEMPTION, True, Me.TerminalCashierID) ' Fixed Bug 16884. Insert CountR into Cashier_Terminals (relation between CountR && cashier_terminals)
          If Me.TerminalCashierID > 0 Then

            Return True
          End If
        End If
      Else

        Return True
      End If

      Return False
    End If

    Return CountRBusinessLogic.IsCountRByCashierTerminalId(Me.TerminalCashierID, Me.CountR.Id, SqlTrx)
  End Function

  ' PURPOSE: goes over all datatable rows and calls function that inserts the movement details and updates STOCK
  '
  ' PARAMS:
  '   - INPUT: sql transaction
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - True if everything went OK. False if there was an error
  '
  ' NOTES:
  Private Function InsertMovementDetails(ByVal SqlTrx As SqlTransaction, ByVal Table As DataTable) As Boolean
    Dim _row As DataRow
    Dim _operation_ok As Boolean
    Dim _dt_currencies_types As DataTable

    _operation_ok = True

    _dt_currencies_types = GetAllowedISOCodeWithoutColor()

    Try

      For Each _row In Table.Rows
        If GetRowValue(_row(DT_COLUMN_CURRENCY.QUANTITY)) > 0 _
            Or GetRowValue(_row(DT_COLUMN_CURRENCY.TOTAL)) > 0 _
            Or (Me.OperationType = Cage.CageOperationType.CountCage AndAlso Not _row.IsNull(DT_COLUMN_CURRENCY.QUANTITY)) Then
          If (Me.OperationType <> Cage.CageOperationType.FromGamingTable And Me.OperationType <> Cage.CageOperationType.FromGamingTableClosing) _
            OrElse m_open_mode = OPEN_MODE.RECOLECT_MOVEMENT _
                   AndAlso (Me.OperationType = Cage.CageOperationType.FromGamingTable Or Me.OperationType = Cage.CageOperationType.FromGamingTableClosing) _
            OrElse Not GamingTableBusinessLogic.IsEnabledGTPlayerTracking _
                   And (Me.OperationType = Cage.CageOperationType.FromGamingTable Or Me.OperationType = Cage.CageOperationType.FromGamingTableClosing) _
                   And m_open_mode = OPEN_MODE.NEW_MOVEMENT Then
            If Not InsertDetail(SqlTrx, _row) Then

              Return False
            End If
          End If

          If Me.OperationType <> Cage.CageOperationType.LostQuadrature And Me.OperationType <> Cage.CageOperationType.GainQuadrature Then
            If (Me.OperationType <> Cage.CageOperationType.FromGamingTable And Me.OperationType <> Cage.CageOperationType.FromGamingTableClosing) _
              OrElse m_open_mode = OPEN_MODE.RECOLECT_MOVEMENT _
                   AndAlso (Me.OperationType = Cage.CageOperationType.FromGamingTable Or Me.OperationType = Cage.CageOperationType.FromGamingTableClosing) _
              OrElse Not GamingTableBusinessLogic.IsEnabledGTPlayerTracking _
                   And (Me.OperationType = Cage.CageOperationType.FromGamingTable Or Me.OperationType = Cage.CageOperationType.FromGamingTableClosing) _
                   And m_open_mode = OPEN_MODE.NEW_MOVEMENT Then
              If Not UpdateCageStock(SqlTrx, _row) Then

                Return False
              End If
            End If
          End If
        End If

      Next

      ' Avoid to update cage concepts from gamingtable when movement is Sent
      If Not (Me.m_status = Cage.CageStatus.Sent AndAlso (Me.OperationType = Cage.CageOperationType.FromGamingTable Or Me.OperationType = Cage.CageOperationType.FromGamingTableClosing)) Then
        Call WSI.Common.CageMeters.CreateItemsMovementDetails(Me.MovementID, Me.ConceptsData, Me.TableCurrencies, _dt_currencies_types, Me.m_type_new_collection.real_ticket_sum, SqlTrx)
      End If

    Catch ex As Exception
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "cls_cage_control", _
                            "InsertMovementDetails", _
                            ex.Message)

      Return False
    End Try

    Return _operation_ok
  End Function ' InsertMovementDetails 

  ' PURPOSE: Inserts movement detail
  '
  ' PARAMS:
  '   - INPUT: sql transaction
  '            Datatable Row
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - True if everything went OK. False if there was an error
  '
  ' NOTES:
  Private Function InsertDetail(ByVal SqlTrx As SqlTransaction, ByVal _row As DataRow) As Boolean
    Dim _sb As StringBuilder
    Dim _operation_ok As Boolean
    Dim _dt_chips As DataTable
    Dim _chip_id As Int64
    Dim _iso_code As String
    Dim _currency_type As CageCurrencyType
    Dim _denomination As Decimal

    _chip_id = Nothing

    _dt_chips = New DataTable()

    _iso_code = _row(DT_COLUMN_CURRENCY.ISO_CODE)
    _currency_type = _row(DT_COLUMN_CURRENCY.CURRENCY_TYPE)
    _chip_id = _row(DT_COLUMN_CURRENCY.CHIP_ID)
    _denomination = _row(DT_COLUMN_CURRENCY.DENOMINATION)

    _operation_ok = True
    _sb = New StringBuilder()
    _sb.AppendLine(" INSERT INTO   CAGE_MOVEMENT_DETAILS ")
    _sb.AppendLine("             ( CMD_MOVEMENT_ID ")
    _sb.AppendLine("             , CMD_ISO_CODE ")
    _sb.AppendLine("             , CMD_QUANTITY ")
    _sb.AppendLine("             , CMD_DENOMINATION ")
    _sb.AppendLine("             , CMD_CHIP_ID ")
    _sb.AppendLine("             , CMD_CAGE_CURRENCY_TYPE ")
    _sb.AppendLine("             ) ")
    _sb.AppendLine("               VALUES ")
    _sb.AppendLine("             ( @pMovementID ")
    _sb.AppendLine("             , @pISOCode ")
    _sb.AppendLine("             , @pQuantity ")
    _sb.AppendLine("             , @pAmount ")
    _sb.AppendLine("             , @pChipID ")
    _sb.AppendLine("             , @CageCurrencyType ")
    _sb.AppendLine("             ) ")

    Try

      Using _cmd As New SqlClient.SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx)

        _cmd.Parameters.Add("@pMovementID", SqlDbType.BigInt).Value = Me.MovementID
        _cmd.Parameters.Add("@pISOCode", SqlDbType.NVarChar).Value = _iso_code

        If _denomination < 0 Then
          _cmd.Parameters.Add("@pQuantity", SqlDbType.Int).Value = _denomination

          If _denomination = Cage.TICKETS_CODE Then
            _cmd.Parameters.Add("@pAmount", SqlDbType.Decimal).Value = _row(DT_COLUMN_CURRENCY.QUANTITY)
          Else
            _cmd.Parameters.Add("@pAmount", SqlDbType.Decimal).Value = _row(DT_COLUMN_CURRENCY.TOTAL)
          End If

          _cmd.Parameters.Add("@pChipID", SqlDbType.Int).Value = _chip_id
          _cmd.Parameters.Add("@CageCurrencyType", SqlDbType.Int).Value = _currency_type

        Else
          _cmd.Parameters.Add("@pQuantity", SqlDbType.Int).Value = _row(DT_COLUMN_CURRENCY.QUANTITY)
          _cmd.Parameters.Add("@pAmount", SqlDbType.Decimal).Value = _denomination
          _cmd.Parameters.Add("@pChipID", SqlDbType.Int).Value = _chip_id
          'JPJ11111
          _cmd.Parameters.Add("@CageCurrencyType", SqlDbType.Int).Value = _currency_type
        End If

        If _cmd.ExecuteNonQuery() <> 1 Then
          _operation_ok = False
        End If

      End Using

    Catch ex As Exception
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "cls_cage_control", _
                            "InsertDetail", _
                            ex.Message)

      Return False
    End Try

    Return _operation_ok
  End Function ' InsertDetail

  '----------------------------------------------------------------------------
  ' PURPOSE: Inserts pending movement
  '
  ' PARAMS:
  '   - INPUT: sql transaction
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - True if everything went OK. False if there was an error
  '
  ' NOTES:
  Private Function InsertPendingMovement(ByVal SqlTrx As SqlTransaction) As Boolean
    Dim _sb As StringBuilder
    Dim _operation_ok As Boolean

    _operation_ok = True
    _sb = New StringBuilder()
    _sb.AppendLine(" INSERT INTO   CAGE_PENDING_MOVEMENTS ")
    _sb.AppendLine("             ( CPM_MOVEMENT_ID ")
    _sb.AppendLine("             , CPM_USER_ID ")
    _sb.AppendLine("             , CPM_TYPE ")
    _sb.AppendLine("             ) ")
    _sb.AppendLine("               VALUES ")
    _sb.AppendLine("             ( @pMovementID ")
    _sb.AppendLine("             , @pUserID ")
    _sb.AppendLine("             , @pType ")
    _sb.AppendLine("             ) ")

    Try

      Using _cmd As New SqlClient.SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx)

        _cmd.Parameters.Add("@pMovementID", SqlDbType.BigInt).Value = Me.MovementID

        If Me.OperationType = WSI.Common.Cage.CageOperationType.ToCashier Then

          If Me.TargetType = Cage.PendingMovementType.ToCashierUser Then
            _cmd.Parameters.Add("@pUserID", SqlDbType.Int).Value = Me.UserCashierID
          ElseIf Me.TargetType = Cage.PendingMovementType.ToCashierTerminal Or Me.TargetType = Cage.PendingMovementType.ToGamingTable Then
            _cmd.Parameters.Add("@pUserID", SqlDbType.Int).Value = Me.TerminalCashierID
          End If

        ElseIf Me.OperationType = WSI.Common.Cage.CageOperationType.ToGamingTable Then
          _cmd.Parameters.Add("@pUserID", SqlDbType.Int).Value = Me.TerminalCashierID
        Else
          ' No deberia entrar nunca
          _cmd.Parameters.Add("@pUserID", SqlDbType.Int).Value = -1
        End If

        _cmd.Parameters.Add("@pType", SqlDbType.Int).Value = Me.TargetType

        If _cmd.ExecuteNonQuery() <> 1 Then
          _operation_ok = False
        End If

      End Using

    Catch ex As Exception
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "cls_cage_control", _
                            "InsertPendingMovement", _
                            ex.Message)

      Return False
    End Try

    Return _operation_ok
  End Function ' InsertPendingMovement

  '----------------------------------------------------------------------------
  ' PURPOSE: Insert movement into database
  '
  ' PARAMS:
  '   - INPUT: sql transaction
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - True if everything went OK. False if there was an error
  '
  ' NOTES:
  Private Function ReadMovement() As Boolean
    Dim _sb As StringBuilder
    Dim _table As DataTable
    Dim _operation_ok As Boolean

    _operation_ok = True
    _sb = New StringBuilder
    _table = New DataTable

    _sb.AppendLine(" SELECT   CGM_MOVEMENT_ID")
    _sb.AppendLine("        , CGM_TERMINAL_CAGE_ID")
    _sb.AppendLine("        , CGM_TERMINAL_CASHIER_ID")
    _sb.AppendLine("        , CGM_USER_CAGE_ID")
    _sb.AppendLine("        , CGM_USER_CASHIER_ID")
    _sb.AppendLine("        , CGM_TYPE")
    _sb.AppendLine("        , CGM_MOVEMENT_DATETIME")
    _sb.AppendLine("        , CGM_STATUS")
    _sb.AppendLine("        , CGM_CASHIER_SESSION_ID")
    _sb.AppendLine("        , CGM_SOURCE_TARGET_ID")
    _sb.AppendLine("        , CGM_CAGE_SESSION_ID")
    _sb.AppendLine("        , CGM_TARGET_TYPE")
    _sb.AppendLine("        , CGM_GAMING_TABLE_ID")
    _sb.AppendLine("        , CGM_GAMING_TABLE_VISITS")
    _sb.AppendLine("        , CGM_MC_COLLECTION_ID")
    _sb.AppendLine("        , CGM_CANCELLATION_USER_ID              ")
    _sb.AppendLine("        , CGM_CANCELLATION_DATETIME             ")
    _sb.AppendLine("        , CASE WHEN CGS_CLOSE_DATETIME IS NULL THEN 0 ELSE 1 END AS CGM_CAGE_SESSION_STATUS ") '0=OPEN,1=CLOSED
    _sb.AppendLine("  FROM   CAGE_MOVEMENTS")

    If m_show_without_cage_id Then
      _sb.AppendLine(" LEFT JOIN   CAGE_SESSIONS ON CGM_CAGE_SESSION_ID = CGS_CAGE_SESSION_ID  ")
    Else
      _sb.AppendLine("INNER JOIN   CAGE_SESSIONS ON CGM_CAGE_SESSION_ID = CGS_CAGE_SESSION_ID  ")
    End If

    _sb.AppendLine(" WHERE   CGM_MOVEMENT_ID = " & Me.MovementID)

    Try

      _table = GUI_GetTableUsingSQL(_sb.ToString, Integer.MaxValue)

      If _table Is Nothing OrElse _table.Rows.Count = 0 Then
        _operation_ok = False
      Else
        Me.m_movement_id = _table.Rows(0).Item(DT_COLUMN_MOVEMENT.MOVEMENT_ID)

        If Me.IsCellNotNull(_table.Rows(0).Item(DT_COLUMN_MOVEMENT.TERM_CAGE_ID)) Then
          Me.m_terminal_cage_id = _table.Rows(0).Item(DT_COLUMN_MOVEMENT.TERM_CAGE_ID)
        Else
          Me.m_terminal_cage_id = Nothing
        End If

        If Me.IsCellNotNull(_table.Rows(0).Item(DT_COLUMN_MOVEMENT.TERM_CASHIER_ID)) Then
          Me.m_terminal_cashier_id = _table.Rows(0).Item(DT_COLUMN_MOVEMENT.TERM_CASHIER_ID)
          Using _db_trx As New DB_TRX()
            Me.IsCountR = Me.IsCountRTerminal(_db_trx.SqlTransaction)
          End Using
        Else
          Me.m_terminal_cashier_id = Nothing
        End If

        If Me.IsCellNotNull(_table.Rows(0).Item(DT_COLUMN_MOVEMENT.USER_CAGE_ID)) Then
          Me.m_user_cage_id = _table.Rows(0).Item(DT_COLUMN_MOVEMENT.USER_CAGE_ID)
        Else
          Me.m_user_cage_id = Nothing
        End If

        If Me.IsCellNotNull(_table.Rows(0).Item(DT_COLUMN_MOVEMENT.CASHIER_SESSION_ID)) Then
          Me.m_cashier_session_id = _table.Rows(0).Item(DT_COLUMN_MOVEMENT.CASHIER_SESSION_ID)
        Else
          Me.m_cashier_session_id = Nothing
        End If

        If Not Me.GetPartialCollectionValue() Then

          Return False
        End If

        If Me.IsCellNotNull(_table.Rows(0).Item(DT_COLUMN_MOVEMENT.USER_CASHIER_ID)) Then
          Me.m_user_cashier_id = _table.Rows(0).Item(DT_COLUMN_MOVEMENT.USER_CASHIER_ID)
        Else
          Me.m_user_cashier_id = Nothing
        End If

        If Me.IsCellNotNull(_table.Rows(0).Item(DT_COLUMN_MOVEMENT.USER_CUSTOM_ID)) Then
          Me.m_user_custom_id = _table.Rows(0).Item(DT_COLUMN_MOVEMENT.USER_CUSTOM_ID)
        Else
          Me.m_user_custom_id = Nothing
        End If

        If Me.IsCellNotNull(_table.Rows(0).Item(DT_COLUMN_MOVEMENT.TARGET_TYPE)) Then
          Me.m_target_type = _table.Rows(0).Item(DT_COLUMN_MOVEMENT.TARGET_TYPE)
        Else
          Me.m_target_type = Nothing
        End If

        If Me.IsCellNotNull(_table.Rows(0).Item(DT_COLUMN_MOVEMENT.GAMING_TABLE_ID)) Then
          Me.m_gaming_table_id = _table.Rows(0).Item(DT_COLUMN_MOVEMENT.GAMING_TABLE_ID)
        Else
          Me.m_gaming_table_id = Nothing
        End If

        If Me.IsCellNotNull(_table.Rows(0).Item(DT_COLUMN_MOVEMENT.GAMING_TABLE_VISITS)) Then
          Me.m_gaming_table_visits = _table.Rows(0).Item(DT_COLUMN_MOVEMENT.GAMING_TABLE_VISITS)
        Else
          Me.m_gaming_table_visits = Nothing
        End If

        If Me.IsCellNotNull(_table.Rows(0).Item(DT_COLUMN_MOVEMENT.MONEY_COLLECTION_ID)) Then
          Me.m_type_new_collection.money_collection_id = _table.Rows(0).Item(DT_COLUMN_MOVEMENT.MONEY_COLLECTION_ID)
        End If

        Me.m_operation_type = _table.Rows(0).Item(DT_COLUMN_MOVEMENT.OPERATION_TYPE)
        Me.m_movement_datetime = _table.Rows(0).Item(DT_COLUMN_MOVEMENT.MOV_DATETIME)
        Me.m_status = _table.Rows(0).Item(DT_COLUMN_MOVEMENT.STATUS)
        Me.m_new_collecion_mode = (Me.m_status = Cage.CageStatus.Sent)

        If Me.IsCellNotNull(_table.Rows(0).Item(DT_COLUMN_MOVEMENT.CAGE_SESSION_ID)) Then
          Me.m_cage_session_id = _table.Rows(0).Item(DT_COLUMN_MOVEMENT.CAGE_SESSION_ID)
        End If

        If Me.IsCellNotNull(_table.Rows(0).Item(DT_COLUMN_MOVEMENT.CAGE_SESSION_STATUS)) Then
          Me.m_cage_session_status = _table.Rows(0).Item(DT_COLUMN_MOVEMENT.CAGE_SESSION_STATUS)
        End If

        If Me.IsCellNotNull(_table.Rows(0).Item(DT_COLUMN_MOVEMENT.USER_CAGE_ID_LAST_MODIFICATION)) Then
          Me.m_user_cage_id_last_modification = _table.Rows(0).Item(DT_COLUMN_MOVEMENT.USER_CAGE_ID_LAST_MODIFICATION)
        End If

        If Me.IsCellNotNull(_table.Rows(0).Item(DT_COLUMN_MOVEMENT.MOV_DATETIME_LAST_MODIFICATION)) Then
          Me.m_movement_datetime_last_modification = _table.Rows(0).Item(DT_COLUMN_MOVEMENT.MOV_DATETIME_LAST_MODIFICATION)
        End If

      End If

      If Me.OperationType = Cage.CageOperationType.CountCage Then
        If Not Me.GetCloseCageDetail() Then
          _operation_ok = False
        End If
      Else
        If Me.OperationType <> Cage.CageOperationType.FromTerminal Then
          If Not ReadMovementDetail() Then
            _operation_ok = False
          End If
        End If
      End If

    Catch ex As Exception
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "cls_cage_control", _
                            "ReadMovement", _
                            ex.Message)

      Return False
    End Try

    Return _operation_ok
  End Function ' ReadMovement

  ' PURPOSE: Read movement detail
  '
  ' PARAMS:
  '
  ' RETURNS:
  '     - true if everything went ok
  '
  ' NOTES:
  Private Function ReadMovementDetail() As Boolean
    Dim _sb As StringBuilder
    Dim _table As DataTable
    Dim _row As DataRow
    Dim _rows_select As DataRow()
    Dim _sort_datatable As Boolean
    Dim _dataview As DataView
    Dim _operation_ok As Boolean

    _operation_ok = True
    _sb = New StringBuilder
    _rows_select = Nothing
    _sort_datatable = False

    Call Me.InitCurrencyData()

    _sb.AppendLine("SELECT    CMD_ISO_CODE           AS ISO_CODE")
    _sb.AppendLine("        , CMD_DENOMINATION       AS DENOMINATION")
    _sb.AppendLine("        , CMD_QUANTITY           AS QUANTITY")
    _sb.AppendLine("        , CASE WHEN CMD_QUANTITY > 0 THEN ISNULL(CMD_CAGE_CURRENCY_TYPE,0) ELSE 99 END AS TYPE")
    _sb.AppendLine("        , CMD_CHIP_ID ")
    _sb.AppendLine("        , ISNULL(CMD_CAGE_CURRENCY_TYPE,0) AS CMD_CAGE_CURRENCY_TYPE ")
    _sb.AppendLine("  FROM    CAGE_MOVEMENT_DETAILS")
    _sb.AppendLine(" WHERE    CMD_MOVEMENT_ID = " & Me.MovementID)
    _sb.AppendLine(" ORDER BY  CMD_ISO_CODE, TYPE ASC, CMD_DENOMINATION DESC")

    Try

      _table = GUI_GetTableUsingSQL(_sb.ToString, Integer.MaxValue)

      If _table Is Nothing Then
        _operation_ok = False
      Else

        For Each _row In _table.Rows
          Call SetRowMoneyValue(_row, _sort_datatable)
        Next

        If _sort_datatable Then
          _dataview = Me.TableCurrencies.DefaultView
          _dataview.Sort = "ISO_CODE, TYPE ASC, DENOMINATION DESC"
          Me.TableCurrencies = _dataview.ToTable()
          AddExpectedColumnInCurrencies()
        End If
      End If

      'Entra cuando viene de cajero/mesa de juego o va hacia cajero/mesadejuego y ya se ha recaudado
      If Me.OperationType = Cage.CageOperationType.FromCashier Or Me.OperationType = Cage.CageOperationType.FromGamingTable Or _
        Me.OperationType = Cage.CageOperationType.FromCashierClosing Or Me.OperationType = Cage.CageOperationType.FromGamingTableClosing Or _
        Me.OperationType = Cage.CageOperationType.FromGamingTableDropbox Or Me.OperationType = Cage.CageOperationType.FromGamingTableDropboxWithExpected _
        Or (Me.OperationType = Cage.CageOperationType.ToCashier And Me.Status <> Cage.CageStatus.Sent And Me.Status <> Cage.CageStatus.Canceled) _
        Or (Me.OperationType = Cage.CageOperationType.ToGamingTable And Me.Status <> Cage.CageStatus.Sent And Me.Status <> Cage.CageStatus.Canceled) Then

        _operation_ok = Me.GetCashierCashing()
      ElseIf Me.OperationType = Cage.CageOperationType.RequestOperation And Me.Status <> Cage.CageStatus.Sent And Me.Status <> Cage.CageStatus.Canceled Then
        _operation_ok = Me.GetRequestCashing()
      End If

    Catch ex As Exception
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "cls_cage_control", _
                            "ReadMovementDetail", _
                            ex.Message)

      Return False
    End Try

    Return _operation_ok
  End Function ' ReadMovementDetail

  ' PURPOSE: To show the money row on the datagrid
  '
  ' PARAMS:
  '
  ' RETURNS:
  '
  ' NOTES:
  Private Sub SetRowMoneyValue(ByRef Row As DataRow, ByRef SortDatatable As Boolean)
    Dim _row_money As DataRow()
    Dim _iso_code As String
    Dim _quantity As Decimal
    Dim _denomination As Decimal
    Dim _row_new As DataRow
    Dim _quantity_column As Int16
    Dim _total_column As Int16
    Dim _automatic_data_fill As Boolean
    Dim _type As CageCurrencyType
    Dim _str_filter As String
    Dim _chip_id As Int32
    Dim _cage_currency_type As Int32

    Try
      _iso_code = Row.Item("ISO_CODE")
      _quantity = Me.GetRowValue(Row.Item("QUANTITY"))
      _denomination = Row.Item("DENOMINATION")
      _type = Row.Item("TYPE")
      _chip_id = -1
      If IsCellNotNull(Row.Item("CMD_CHIP_ID")) Then
        _chip_id = Row.Item("CMD_CHIP_ID")
      End If

      _cage_currency_type = Row.Item("CMD_CAGE_CURRENCY_TYPE")

      If _iso_code = Cage.CHIPS_ISO_CODE Then
        _iso_code = CurrencyExchange.GetNationalCurrency()
        _cage_currency_type = FeatureChips.ChipType.RE
        _type = CageCurrencyType.ChipsRedimible
      End If

      If _cage_currency_type = FeatureChips.ChipType.RE Or _cage_currency_type = FeatureChips.ChipType.NR Or _cage_currency_type = FeatureChips.ChipType.COLOR Then
        _str_filter = String.Format("ISO_CODE = '{0}' AND TYPE = {1} AND CHIP_ID = {2} ", _iso_code, CInt(_type), _chip_id)
      Else
        _str_filter = String.Format("ISO_CODE = '{0}' AND TYPE = {1} AND DENOMINATION = ", _iso_code, CInt(_type))
      End If

      If Me.OperationType = Cage.CageOperationType.RequestOperation Then
        _quantity_column = DT_COLUMN_CURRENCY.CASHIER_QUANTITY
        _total_column = DT_COLUMN_CURRENCY.CASHIER_TOTAL
        _automatic_data_fill = GeneralParam.GetBoolean("Cage", "Collection.SuggestDenominationAmounts", False)
      Else
        _quantity_column = DT_COLUMN_CURRENCY.QUANTITY
        _total_column = DT_COLUMN_CURRENCY.TOTAL
        _automatic_data_fill = False
      End If

      If _cage_currency_type = FeatureChips.ChipType.RE Or _cage_currency_type = FeatureChips.ChipType.NR Or _cage_currency_type = FeatureChips.ChipType.COLOR Then
        _row_money = Me.TableCurrencies.Select(_str_filter)
      Else
        If _quantity < 0 Then
          _row_money = Me.TableCurrencies.Select(_str_filter & GUI_LocalNumberToDBNumber(_quantity.ToString))
        Else
          _row_money = Me.TableCurrencies.Select(_str_filter & GUI_LocalNumberToDBNumber(_denomination.ToString))
        End If
      End If


      If _row_money.Length > 0 Then
        If _row_money(0).Item(DT_COLUMN_CURRENCY.DENOMINATION) < 0 Then
          If _row_money(0).Item(DT_COLUMN_CURRENCY.DENOMINATION) = Cage.TICKETS_CODE Then
            _row_money(0).Item(_quantity_column) = _denomination
            _row_money(0).Item(_total_column) = 0
          Else
            _row_money(0).Item(_quantity_column) = 1
            _row_money(0).Item(_total_column) += Row.Item(DT_COLUMN_CURRENCY.DENOMINATION)
          End If
        Else
          _row_money(0).Item(_quantity_column) = Me.GetRowValue(Row.Item("QUANTITY"))
          If _iso_code = Cage.CHIPS_COLOR Then
            _row_money(0).Item(_total_column) = _row_money(0).Item(_quantity_column)
          Else
            _row_money(0).Item(_total_column) = _row_money(0).Item(_quantity_column) * Row.Item(DT_COLUMN_CURRENCY.DENOMINATION)
          End If
        End If

        _row_money(0).Item(DT_COLUMN_CURRENCY.QUANTITY_DIFFERENCE) = _row_money(0).Item(_quantity_column)
        _row_money(0).Item(DT_COLUMN_CURRENCY.TOTAL_DIFFERENCE) = _row_money(0).Item(_total_column)

        If _automatic_data_fill And Me.Status = Cage.CageStatus.Sent Then
          _row_money(0).Item(DT_COLUMN_CURRENCY.QUANTITY) = _row_money(0).Item(DT_COLUMN_CURRENCY.CASHIER_QUANTITY)
          _row_money(0).Item(DT_COLUMN_CURRENCY.TOTAL) = _row_money(0).Item(DT_COLUMN_CURRENCY.CASHIER_TOTAL)
        End If

      Else
        _row_new = Me.TableCurrencies.NewRow()

        _row_new("ISO_CODE") = _iso_code
        _row_new("DENOMINATION") = _denomination
        _row_new("TYPE") = _type
        _row_new(DT_COLUMN_CURRENCY.DENOMINATION_WITH_SIMBOL) = _denomination
        _row_new(_quantity_column) = _quantity
        _row_new(_total_column) = _quantity * _denomination
        _row_new(DT_COLUMN_CURRENCY.TOTAL_WITH_SIMBOL) = _row_new(_total_column)

        If Me.OperationType = Cage.CageOperationType.RequestOperation Then
          _row_new(DT_COLUMN_CURRENCY.QUANTITY) = 0
          _row_new(DT_COLUMN_CURRENCY.TOTAL) = 0
        Else
          _row_new(DT_COLUMN_CURRENCY.CASHIER_QUANTITY) = 0
          _row_new(DT_COLUMN_CURRENCY.CASHIER_TOTAL) = 0
        End If

        _row_new(DT_COLUMN_CURRENCY.QUANTITY_DIFFERENCE) = _row_new(DT_COLUMN_CURRENCY.QUANTITY)
        _row_new(DT_COLUMN_CURRENCY.TOTAL_DIFFERENCE) = _row_new(DT_COLUMN_CURRENCY.TOTAL)
        Me.TableCurrencies.Rows.Add(_row_new)

        SortDatatable = True

        ' JBC 07/10/2014
        Call SetRowMoneyValue(Row, SortDatatable)

      End If

    Catch _ex As Exception

      Log.Message("Cage_Control.SetRowMoneyValue: " & _ex.Message())
    End Try

  End Sub ' SetRowMoneyValue

  ' PURPOSE: Gets movement details from cashdesk
  '
  ' PARAMS:
  '
  ' RETURNS:
  '     - true if ok
  '
  ' NOTES:
  Private Function GetCashierCashing() As Boolean
    Dim _sb As StringBuilder
    Dim _table As DataTable
    Dim _operation_ok As Boolean

    _sb = New StringBuilder
    _table = New DataTable
    _operation_ok = True

    _sb.AppendLine("    SELECT     CASE WHEN CM_CURRENCY_ISO_CODE IS NULL OR CM_CURRENCY_ISO_CODE = '" & Cage.CHIPS_ISO_CODE & "' THEN '" & GeneralParam.GetString("RegionalOptions", "CurrencyISOCode", "MXN") & "'")
    _sb.AppendLine("                    ELSE CM_CURRENCY_ISO_CODE END AS CM_CURRENCY_ISO_CODE ")
    _sb.AppendLine("             , CM_CURRENCY_DENOMINATION ")
    _sb.AppendLine("             , CM_ADD_AMOUNT ")
    _sb.AppendLine("             , CM_SUB_AMOUNT ")
    _sb.AppendLine("             , CM_INITIAL_BALANCE ")
    _sb.AppendLine("             , CM_TYPE ")
    _sb.AppendLine("             , CASE WHEN CM_CURRENCY_ISO_CODE = '" & Cage.CHIPS_ISO_CODE & "' THEN '" & Convert.ToString(CageCurrencyType.ChipsRedimible) & "'")
    _sb.AppendLine("                    WHEN (CM_CURRENCY_DENOMINATION > 0 OR CM_CHIP_ID IS NOT NULL) THEN ISNULL(CM_CAGE_CURRENCY_TYPE,0) ")
    _sb.AppendLine("                    ELSE 99 END AS TYPE ")
    _sb.AppendLine("             , CM_CHIP_ID AS CHIP_ID")
    _sb.AppendLine("       FROM    CASHIER_MOVEMENTS ")
    _sb.AppendLine(" INNER JOIN    CAGE_CASHIER_MOVEMENT_RELATION ")
    _sb.AppendLine("         ON    CASHIER_MOVEMENTS.CM_MOVEMENT_ID = CAGE_CASHIER_MOVEMENT_RELATION.CM_MOVEMENT_ID ")
    _sb.AppendLine("      WHERE    CAGE_CASHIER_MOVEMENT_RELATION.CGM_MOVEMENT_ID = " & Me.MovementID)
    _sb.AppendLine("        AND    CM_TYPE <> " & CASHIER_MOVEMENT.CAGE_CASHIER_COLLECT_DROPBOX_GAMINGTABLE)
    _sb.AppendLine("   ORDER BY    CM_CURRENCY_ISO_CODE ")
    _sb.AppendLine("             , CM_CURRENCY_DENOMINATION ")

    Try

      _table = GUI_GetTableUsingSQL(_sb.ToString, Integer.MaxValue)

      If Not SetCashierCashingData(_table) Then
        _operation_ok = False
      End If

    Catch ex As Exception
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "cls_cage_control", _
                            "GetCashierCashing", _
                            ex.Message)

      Return False
    End Try

    Return _operation_ok
  End Function ' GetCashierCashing

  ' PURPOSE: Gets movement details from request movement 
  '
  ' PARAMS:
  '
  ' RETURNS:
  '     - true if ok
  '
  ' NOTES:
  Private Function GetRequestCashing() As Boolean
    Dim _sb As StringBuilder
    Dim _table As DataTable
    Dim _operation_ok As Boolean

    _sb = New StringBuilder
    _table = New DataTable
    _operation_ok = True

    _sb.AppendLine("    SELECT     CASE WHEN CMD_ISO_CODE IS NULL THEN '" & GeneralParam.GetString("RegionalOptions", "CurrencyISOCode") & "'")
    _sb.AppendLine("                    ELSE CMD_ISO_CODE END AS CMD_ISO_CODE                             ")
    _sb.AppendLine("             , CASE WHEN CMD_QUANTITY < 0 THEN CMD_QUANTITY                           ")
    _sb.AppendLine("                    ELSE CMD_DENOMINATION END AS CMD_DENOMINATION                     ")
    _sb.AppendLine("             , CASE WHEN CMD_QUANTITY < 0 THEN CMD_DENOMINATION                       ")
    _sb.AppendLine("                    ELSE CMD_QUANTITY  END AS CMD_QUANTITY                            ")
    _sb.AppendLine("             , CASE WHEN CMD_QUANTITY < 0 THEN CMD_DENOMINATION                       ")
    _sb.AppendLine("                    WHEN CMD_DENOMINATION = 0 THEN CMD_QUANTITY                       ")
    _sb.AppendLine("                    ELSE CMD_QUANTITY * CMD_DENOMINATION END AS TOTAL                 ")
    _sb.AppendLine("             , CASE WHEN CMD_QUANTITY > 0 THEN ISNULL(CMD_CAGE_CURRENCY_TYPE,0)       ")
    _sb.AppendLine("                    ELSE 99 END AS TYPE                                               ")
    _sb.AppendLine("             , CMD_CHIP_ID AS CHIP_ID                                                 ")
    _sb.AppendLine("       FROM    CAGE_MOVEMENT_DETAILS                                                  ")
    _sb.AppendLine(" INNER JOIN    CAGE_MOVEMENTS                                                         ")
    _sb.AppendLine("         ON    CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID = CAGE_MOVEMENTS.CGM_MOVEMENT_ID ")
    _sb.AppendLine("      WHERE    CAGE_MOVEMENTS.CGM_RELATED_MOVEMENT_ID = " & Me.MovementID)
    _sb.AppendLine("   ORDER BY    CMD_ISO_CODE                                                           ")
    _sb.AppendLine("             , CMD_DENOMINATION                                                       ")

    Try

      _table = GUI_GetTableUsingSQL(_sb.ToString, Integer.MaxValue)

      If Not SetCashierCashingData(_table) Then
        _operation_ok = False
      End If

    Catch ex As Exception
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "cls_cage_control", _
                            "GetRequestCashing", _
                            ex.Message)

      Return False
    End Try

    Return _operation_ok
  End Function ' GetRequestCashing

  ' PURPOSE: Sets cashdesk data
  '
  ' PARAMS:
  '
  ' RETURNS:
  '     - true if ok
  '
  ' NOTES:
  Private Function SetCashierCashingData(ByVal TableCashier As DataTable) As Boolean
    Dim _row_cashing As DataRow
    Dim _rows_currency As DataRow()
    Dim _row_currency As DataRow
    Dim _iso_code_current As String
    Dim _operation_ok As Boolean
    Dim _param_array([Enum].GetValues(GetType(DT_COLUMN_CURRENCY)).Length - 1) As Object
    Dim _dataview As DataView
    Dim _sort As Boolean
    Dim _currency_type As CageCurrencyType

    _operation_ok = False
    _iso_code_current = ""
    _sort = False

    Try

      If TableCashier Is Nothing OrElse TableCashier.Rows.Count <= 0 Then
        _operation_ok = True
      Else
        If Me.OperationType = Cage.CageOperationType.RequestOperation Then
          TOTAL_COLUMN = 3
          CURRENCY_TYPE_COLUMN = 4
        Else
          ' Set column from sql query to get the values
          Select Case TableCashier.Rows(0)(CM_TYPE)
            Case CASHIER_MOVEMENT.CAGE_FILLER_IN
              TOTAL_COLUMN = CM_ADD_AMOUNT
            Case CASHIER_MOVEMENT.CAGE_FILLER_OUT
              TOTAL_COLUMN = CM_SUB_AMOUNT
            Case CASHIER_MOVEMENT.CAGE_CLOSE_SESSION
              TOTAL_COLUMN = CM_INITIAL_BALANCE
            Case CASHIER_MOVEMENT.CAGE_CASHIER_COLLECT_DROPBOX_GAMINGTABLE_DETAIL
              TOTAL_COLUMN = CM_SUB_AMOUNT
          End Select
          CURRENCY_TYPE_COLUMN = 6
        End If

        For Each _row_cashing In TableCashier.Rows

          _currency_type = _row_cashing.Item(CURRENCY_TYPE_COLUMN)

          If _row_cashing(DT_COLUMN_CURRENCY.DENOMINATION) = 0 And _currency_type <> FeatureChips.ChipType.COLOR Then
            Continue For
          End If

          If _iso_code_current <> _row_cashing.Item(DT_COLUMN_CURRENCY.ISO_CODE) Then
            _iso_code_current = _row_cashing.Item(DT_COLUMN_CURRENCY.ISO_CODE)
          End If
          If _currency_type = FeatureChips.ChipType.RE Or _currency_type = FeatureChips.ChipType.NR Or _currency_type = FeatureChips.ChipType.COLOR Then
            _rows_currency = TableCurrencies.Select(String.Format("ISO_CODE = '{0}' AND TYPE = {1} AND CHIP_ID = {2}", _row_cashing.Item(DT_COLUMN_CURRENCY.ISO_CODE), _
                                                                  _row_cashing.Item(CURRENCY_TYPE_COLUMN), _row_cashing.Item("CHIP_ID")))
          Else
            _rows_currency = TableCurrencies.Select(String.Format("ISO_CODE = '{0}' AND DENOMINATION = {1} AND TYPE = {2}", _row_cashing.Item(DT_COLUMN_CURRENCY.ISO_CODE), _
                                                                  GUI_LocalNumberToDBNumber(_row_cashing.Item(DT_COLUMN_CURRENCY.DENOMINATION)), _row_cashing.Item(CURRENCY_TYPE_COLUMN)))
          End If

          If _rows_currency.Length > 0 Then
            Call Me.UpdateTableCurrencies(_rows_currency(0), _row_cashing)
          Else ' The denomination doesn't exist in cage denominations.
            _param_array(DT_COLUMN_CURRENCY.ISO_CODE) = _row_cashing(DT_COLUMN_CURRENCY.ISO_CODE)
            _param_array(DT_COLUMN_CURRENCY.DENOMINATION) = _row_cashing(DT_COLUMN_CURRENCY.DENOMINATION)
            _param_array(DT_COLUMN_CURRENCY.CURRENCY_TYPE) = _row_cashing(DT_COLUMN_CURRENCY.CURRENCY_TYPE)
            _param_array(DT_COLUMN_CURRENCY.DENOMINATION_WITH_SIMBOL) = _row_cashing(DT_COLUMN_CURRENCY.DENOMINATION)
            _param_array(DT_COLUMN_CURRENCY.QUANTITY) = 0
            _param_array(DT_COLUMN_CURRENCY.TOTAL) = 0
            _param_array(DT_COLUMN_CURRENCY.TOTAL_WITH_SIMBOL) = 0
            _param_array(DT_COLUMN_CURRENCY.CASHIER_QUANTITY) = _row_cashing(DT_COLUMN_CURRENCY.QUANTITY) / _row_cashing(DT_COLUMN_CURRENCY.DENOMINATION)
            _param_array(DT_COLUMN_CURRENCY.CASHIER_TOTAL) = _row_cashing(DT_COLUMN_CURRENCY.QUANTITY)
            _param_array(DT_COLUMN_CURRENCY.QUANTITY_DIFFERENCE) = _param_array(6)
            _param_array(DT_COLUMN_CURRENCY.TOTAL_DIFFERENCE) = _param_array(7)
            Me.TableCurrencies.Rows.Add(_param_array)
            _sort = True
          End If

        Next

        If _sort Then
          _dataview = Me.TableCurrencies.DefaultView
          _dataview.Sort = "ISO_CODE, TYPE ASC, DENOMINATION DESC"
          Me.TableCurrencies = _dataview.ToTable()
          AddExpectedColumnInCurrencies()
        End If

        ' Delete zeros from datagrid
        For Each _row_currency In Me.TableCurrencies.Rows
          Call Me.CheckForZero(_row_currency)
        Next

        _operation_ok = True

      End If

    Catch ex As Exception
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "cls_cage_control", _
                            "SetCashierCashingData", _
                            ex.Message)

      Return False
    End Try

    Return _operation_ok
  End Function ' SetCashierCashingData

  ' PURPOSE: Updates currencies datatable
  '
  ' PARAMS:
  '   - INPUT: 
  '         - RowCurrency
  '         - RowCashier
  '
  ' RETURNS:
  '
  ' NOTES:
  '
  Private Sub UpdateTableCurrencies(ByRef RowCurrency As DataRow, ByRef RowCashier As DataRow)
    Dim _quantity As Decimal
    Dim _total As Decimal
    Dim _cashier_quantity As Decimal
    Dim _tab_curr_cashier_total As Decimal
    Dim _from_cashier_total As Decimal
    Dim _quantity_diff As Decimal
    Dim _total_diff As Decimal
    Dim _automatic_data_fill As Boolean
    Dim _suggest_denomination_amounts As Boolean

    _quantity = 0
    _total = 0
    _cashier_quantity = 0
    _tab_curr_cashier_total = 0
    _from_cashier_total = 0
    _quantity_diff = 0
    _total_diff = 0

    ' FAV: Bug 10911
    _suggest_denomination_amounts = GeneralParam.GetBoolean("Cage", "Collection.SuggestDenominationAmounts", False)
    Select Case Me.OpenMode
      Case CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MOVEMENT, _
           CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_FROM_PROMOBOX_OR_ACCEPTOR
        _automatic_data_fill = _suggest_denomination_amounts

      Case CLASS_CAGE_CONTROL.OPEN_MODE.CANCEL_MOVEMENT, _
           CLASS_CAGE_CONTROL.OPEN_MODE.CANCEL_MOVEMENT_FROM_GAMINGTABLES
        Select Case Me.Status
          Case Cage.CageStatus.OkAmountImbalance, _
               Cage.CageStatus.OkDenominationImbalance, _
               Cage.CageStatus.OK
            _automatic_data_fill = False
          Case Else
            _automatic_data_fill = True
        End Select

      Case Else
        _automatic_data_fill = False
    End Select

    If Me.OperationType = Cage.CageOperationType.RequestOperation Then
      _quantity = Me.GetRowValue(RowCurrency.Item(DT_COLUMN_CURRENCY.CASHIER_QUANTITY))
      _total = Me.GetRowValue(RowCurrency.Item(DT_COLUMN_CURRENCY.CASHIER_TOTAL))
      _from_cashier_total = RowCashier.Item(TOTAL_COLUMN)
    Else
      _quantity = Me.GetRowValue(RowCurrency.Item(DT_COLUMN_CURRENCY.QUANTITY))
      _total = Me.GetRowValue(RowCurrency.Item(DT_COLUMN_CURRENCY.TOTAL))
      _from_cashier_total = RowCashier.Item(TOTAL_COLUMN)
      _tab_curr_cashier_total = RowCurrency.Item(DT_COLUMN_CURRENCY.CASHIER_TOTAL)
    End If

    Select Case RowCashier.Item(DENOMINATION_COLUMN)
      Case Is > 0 ' Bills 
        _cashier_quantity = _from_cashier_total / RowCashier.Item(DENOMINATION_COLUMN)

        If Me.OperationType = Cage.CageOperationType.RequestOperation Then
          _quantity_diff = _cashier_quantity - _quantity
        Else
          _quantity_diff = _quantity - _cashier_quantity
        End If

        If _automatic_data_fill Then

          _quantity = _cashier_quantity
          _total = _from_cashier_total
        End If

        _total_diff = _total - _from_cashier_total

      Case Is = 0 ' Chips color
        _cashier_quantity = _from_cashier_total
        _quantity_diff = _quantity - _cashier_quantity

        If _automatic_data_fill Then

          _quantity = _cashier_quantity
          _total = _from_cashier_total
        End If

        _total_diff = _total - _from_cashier_total

      Case Cage.TICKETS_CODE ' Tickets
        _cashier_quantity = _from_cashier_total + _tab_curr_cashier_total
        _quantity_diff = _quantity - _cashier_quantity
        _from_cashier_total = 0
        _total_diff = 0

        If WSI.Common.GeneralParam.GetInt32("TITO", "Cashier.TicketsCollection") = 0 AndAlso Me.m_new_collecion_mode Then
          _quantity_diff = 0
          _quantity = _cashier_quantity
        End If

      Case Else ' Coins, bank cards, checks
        _cashier_quantity = _from_cashier_total + _tab_curr_cashier_total
        _quantity_diff = _total - _cashier_quantity

        If _automatic_data_fill Then
          _quantity = 1
          _total = _cashier_quantity
        End If

        _cashier_quantity = 1
        _quantity_diff = _quantity - 1
        _total_diff = _quantity_diff

    End Select

    If Me.OperationType = Cage.CageOperationType.RequestOperation Then
      RowCurrency.Item(DT_COLUMN_CURRENCY.QUANTITY) = _cashier_quantity
      RowCurrency.Item(DT_COLUMN_CURRENCY.TOTAL) = _from_cashier_total
    Else
      RowCurrency.Item(DT_COLUMN_CURRENCY.QUANTITY) = _quantity
      RowCurrency.Item(DT_COLUMN_CURRENCY.TOTAL) = _total
      RowCurrency.Item(DT_COLUMN_CURRENCY.CASHIER_QUANTITY) = _cashier_quantity
      RowCurrency.Item(DT_COLUMN_CURRENCY.CASHIER_TOTAL) += _from_cashier_total ' JAB 08-AUG-2014: Fixed bug WIG-1176
    End If
    RowCurrency.Item(DT_COLUMN_CURRENCY.QUANTITY_DIFFERENCE) = IIf(_quantity_diff > 0, "+" & _quantity_diff, _quantity_diff)
    RowCurrency.Item(DT_COLUMN_CURRENCY.TOTAL_DIFFERENCE) = IIf(_total_diff > 0, "+" & _total_diff, _total_diff)

  End Sub ' UpdateTableCurrencies

  ' PURPOSE: Checks if zeroes in a given row, sets blank value
  '
  ' PARAMS:
  '
  ' RETURNS:
  '     - true if ok
  '
  ' NOTES:
  Public Sub CheckForZero(ByRef RowCurrency As DataRow)
    Dim _cage_quantity As Integer
    Dim _cage_total As Decimal
    Dim _cashier_quantity As Integer
    Dim _cashier_total As Decimal

    _cage_quantity = Me.GetRowValue(RowCurrency.Item(DT_COLUMN_CURRENCY.QUANTITY))
    _cage_total = Me.GetRowValue(RowCurrency.Item(DT_COLUMN_CURRENCY.TOTAL))
    _cashier_quantity = Me.GetRowValue(RowCurrency.Item(DT_COLUMN_CURRENCY.CASHIER_QUANTITY))
    _cashier_total = Me.GetRowValue(RowCurrency.Item(DT_COLUMN_CURRENCY.CASHIER_TOTAL))

    If (_cage_quantity + _cage_total + _cashier_quantity + _cashier_total) = 0 Then ' Check if all values are zero
      RowCurrency.Item(DT_COLUMN_CURRENCY.QUANTITY) = DBNull.Value
      RowCurrency.Item(DT_COLUMN_CURRENCY.TOTAL) = DBNull.Value
      RowCurrency.Item(DT_COLUMN_CURRENCY.CASHIER_QUANTITY) = DBNull.Value
      RowCurrency.Item(DT_COLUMN_CURRENCY.CASHIER_TOTAL) = DBNull.Value
      'RowCurrency.Item(DT_COLUMN_CURRENCY.QUANTITY_DIFFERENCE) = DBNull.Value
    Else

      _cashier_total = 0
    End If

  End Sub ' CheckForZero

  ' PURPOSE: checks if cell is not null
  '
  ' PARAMS:
  '
  ' RETURNS:
  '     - true if ok
  '
  ' NOTES:
  Private Function IsCellNotNull(ByVal Value As Object) As Boolean
    Dim _operation_ok As Boolean

    _operation_ok = True

    If Value Is DBNull.Value Then
      _operation_ok = False
    End If

    Return _operation_ok
  End Function ' IsCellNotNull

  ' PURPOSE: updates movement to Cancel status
  '
  ' PARAMS:
  '
  ' RETURNS:
  '     - db result
  '
  ' NOTES:
  Private Function UpdateToVoidMovement(ByVal SqlTrx As SqlTransaction) As Integer
    Dim _sb As StringBuilder
    Dim _result As Integer

    _result = CLASS_BASE.ENUM_STATUS.STATUS_OK
    _sb = New StringBuilder

    _sb.AppendLine("UPDATE    CAGE_MOVEMENTS ")
    _sb.AppendLine("   SET    CGM_STATUS = @pCanceledStatus ")
    _sb.AppendLine("       , CGM_CANCELLATION_USER_ID = @pUserCageIdLastModification  ")
    _sb.AppendLine("       , CGM_CANCELLATION_DATETIME = @pDateTimeLastModification        ")
    _sb.AppendLine(" WHERE    CGM_MOVEMENT_ID = @pMovementId ")

    Try

      Using _cmd As New SqlClient.SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx)

        _cmd.Parameters.Add("@pCanceledStatus", SqlDbType.Int).Value = Cage.CageStatus.Canceled
        _cmd.Parameters.Add("@pMovementId", SqlDbType.BigInt).Value = Me.MovementID

        _cmd.Parameters.Add("@pUserCageIdLastModification", SqlDbType.BigInt).Value = GLB_CurrentUser.Id
        _cmd.Parameters.Add("@pDateTimeLastModification", SqlDbType.DateTime).Value = GUI_FormatDate(WGDB.Now(), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

        If _cmd.ExecuteNonQuery() = 1 Then
          If Me.HasPendingMovementById(Me.MovementID, SqlTrx) AndAlso Not Me.DeleteFromPendingMovement(SqlTrx) Then
            _result = CLASS_BASE.ENUM_STATUS.STATUS_ERROR
          End If
        Else
          _result = CLASS_BASE.ENUM_STATUS.STATUS_ERROR
        End If
      End Using

      If _result = CLASS_BASE.ENUM_STATUS.STATUS_OK Then

        If Me.OperationType = Cage.CageOperationType.FromCustom Then
          NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4931), ENUM_MB_TYPE.MB_TYPE_INFO)
        Else
          NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3036), ENUM_MB_TYPE.MB_TYPE_INFO)
        End If

      Else

        If Me.OperationType = Cage.CageOperationType.FromCustom Then
          NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4929), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Else
          NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3037), ENUM_MB_TYPE.MB_TYPE_WARNING)
        End If

      End If

    Catch ex As Exception
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "cls_cage_control", _
                            "UpdateToVoidMovement", _
                            ex.Message)

      _result = CLASS_BASE.ENUM_STATUS.STATUS_ERROR
    End Try

    Return _result
  End Function ' UpdateToVoidMovement

  ' PURPOSE: Deletes movement from pending movement table
  '
  ' PARAMS:
  '
  ' RETURNS:
  '     - db result
  '
  ' NOTES:
  Private Function DeleteFromPendingMovement(ByVal SqlTrx As SqlTransaction) As Boolean
    Dim _operation_ok As Boolean

    _operation_ok = Cage.DeleteFromPendingMovement(Me.MovementID, SqlTrx)

    Return _operation_ok

  End Function ' DeleteFromPendingMovement

  ' PURPOSE: Collects movement
  '
  ' PARAMS:
  '
  ' RETURNS:
  '     - db result
  '
  ' NOTES:
  Private Function CancelRequest() As Integer
    Dim _result As ENUM_CONFIGURATION_RC

    _result = ENUM_CONFIGURATION_RC.CONFIGURATION_OK

    Using _db_trx As New DB_TRX()

      If _result = ENUM_CONFIGURATION_RC.CONFIGURATION_OK AndAlso Not Me.UpdateMovement(Cage.CageStatus.Canceled, _db_trx.SqlTransaction, False, True) Then
        _result = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      If _result = ENUM_CONFIGURATION_RC.CONFIGURATION_OK Then
        Me.Status = Cage.CageStatus.Canceled

        _db_trx.Commit()
      End If

    End Using

    Return _result
  End Function

  ' PURPOSE: Collects movement
  '
  ' PARAMS:
  '
  ' RETURNS:
  '     - db result
  '
  ' NOTES:
  Private Function ProcessRequestOK() As Integer
    Dim _result As ENUM_CONFIGURATION_RC
    Dim _aux_related_mov As Long
    Dim _aux_operation_type As Long
    Dim _aux_status As Cage.CageStatus

    _result = ENUM_CONFIGURATION_RC.CONFIGURATION_OK

    Using _db_trx As New DB_TRX()

      Try
        Me.UpdateMovement(Me.Status, _db_trx.SqlTransaction, True, True)

        If Me.OperationType = Cage.CageOperationType.RequestOperation AndAlso _
         Not Me.CheckIfSameStatus(Me.MovementID, Cage.CageStatus.Sent) Then
          'Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4834), ENUM_MB_TYPE.MB_TYPE_WARNING)
          _result = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_ALREADY_EXISTS
        End If

        'sets status for the movement
        If _result = ENUM_CONFIGURATION_RC.CONFIGURATION_OK AndAlso Not Me.UpdateMovement(Cage.CageStatus.OK, _db_trx.SqlTransaction, True, True) Then
          _result = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

        _aux_related_mov = Me.RelatedMovementId
        _aux_status = Me.Status
        _aux_operation_type = Me.OperationType

        If _result = ENUM_CONFIGURATION_RC.CONFIGURATION_OK Then
          ' Inserts movement
          Me.RelatedMovementId = Me.MovementID
          Me.OperationType = Cage.CageOperationType.ToCashier
          Me.Status = Cage.CageStatus.Sent
          If _result = ENUM_CONFIGURATION_RC.CONFIGURATION_OK AndAlso Not InsertMovement(_db_trx.SqlTransaction) Then
            _result = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
          End If

          ' Inserts movement details
          If _result = ENUM_CONFIGURATION_RC.CONFIGURATION_OK AndAlso Not InsertMovementDetails(_db_trx.SqlTransaction, Me.TableCurrencies) Then
            _result = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
          End If

          If _result = ENUM_CONFIGURATION_RC.CONFIGURATION_OK AndAlso Not InsertPendingMovement(_db_trx.SqlTransaction) Then
            _result = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
          End If

          Me.RelatedMovementId = _aux_related_mov
          Me.Status = _aux_status
          Me.OperationType = _aux_operation_type

        End If
        If _result = ENUM_CONFIGURATION_RC.CONFIGURATION_OK Then
          _result = UpdateCageMeters(_db_trx.SqlTransaction)
        End If
        If _result = ENUM_CONFIGURATION_RC.CONFIGURATION_OK Then
          Call _db_trx.Commit()
        Else
          Call _db_trx.Rollback()
        End If

      Catch ex As Exception
        Call _db_trx.Rollback()
        Call Trace.WriteLine(ex.ToString())
        Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                              "cls_cage_control", _
                              "ProcessRequestOK", _
                              ex.Message)

        _result = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End Try

    End Using

    If _result = ENUM_CONFIGURATION_RC.CONFIGURATION_OK Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4797), ENUM_MB_TYPE.MB_TYPE_INFO)
    Else
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4798), ENUM_MB_TYPE.MB_TYPE_WARNING)
    End If

    Return _result
  End Function ' ProcessRequestOK

  ' PURPOSE: Collects CountR movement
  '
  ' PARAMS:
  '
  ' RETURNS:
  '     - db result
  '
  ' NOTES:
  Private Function RecolectCountRMovement() As Integer
    Dim _result As ENUM_CONFIGURATION_RC

    _result = ENUM_CONFIGURATION_RC.CONFIGURATION_OK

    Using _db_trx As New DB_TRX()

      Try

        ' Inserts movement details
        If _result = ENUM_CONFIGURATION_RC.CONFIGURATION_OK AndAlso Not Me.InsertMovementDetails(_db_trx.SqlTransaction, Me.TableCurrencies) Then
          _result = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

        ' Deletes pending movement
        If _result = ENUM_CONFIGURATION_RC.CONFIGURATION_OK AndAlso Not Me.DeleteFromPendingMovement(_db_trx.SqlTransaction) Then
          _result = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

        ' Set status for the movement
        If _result = ENUM_CONFIGURATION_RC.CONFIGURATION_OK AndAlso Not Me.UpdateMovement(Me.Status, _db_trx.SqlTransaction, True) Then
          _result = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

        If _result = ENUM_CONFIGURATION_RC.CONFIGURATION_OK Then
          _result = Me.UpdateCageMeters(_db_trx.SqlTransaction)
        End If

        Me.CountRCloseSession = True
        _result = Me.CountR.AddMovementAndDetails(Me, _db_trx.SqlTransaction)

        If _result = ENUM_CONFIGURATION_RC.CONFIGURATION_OK Then
          Call Me.CreateCollectionAlarms(_db_trx.SqlTransaction) ' Creation of the alarms if its exists

          Call _db_trx.Commit()
        Else
          Call _db_trx.Rollback()
        End If

      Catch ex As Exception
        Call _db_trx.Rollback()
        Call Trace.WriteLine(ex.ToString())
        Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                              "cls_cage_control", _
                              "RecolectMovement", _
                              ex.Message)

        _result = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End Try

    End Using

    If _result = ENUM_CONFIGURATION_RC.CONFIGURATION_OK Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3034), ENUM_MB_TYPE.MB_TYPE_INFO)
    Else
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3035), ENUM_MB_TYPE.MB_TYPE_WARNING)
    End If

    Return _result
  End Function ' RecolectCountRMovement

  ' PURPOSE: Collects movement
  '
  ' PARAMS:
  '
  ' RETURNS:
  '     - db result
  '
  ' NOTES:
  Private Function RecolectMovement() As Integer
    Dim _result As ENUM_CONFIGURATION_RC

    _result = ENUM_CONFIGURATION_RC.CONFIGURATION_OK

    Using _db_trx As New DB_TRX()

      Try

        ' Inserts movement details
        If _result = ENUM_CONFIGURATION_RC.CONFIGURATION_OK AndAlso Not Me.InsertMovementDetails(_db_trx.SqlTransaction, Me.TableCurrencies) Then
          _result = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

        'deletes pending movement
        If _result = ENUM_CONFIGURATION_RC.CONFIGURATION_OK AndAlso Not Me.DeleteFromPendingMovement(_db_trx.SqlTransaction) Then
          _result = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

        'sets status for the movement
        If _result = ENUM_CONFIGURATION_RC.CONFIGURATION_OK AndAlso Not Me.UpdateMovement(Me.Status, _db_trx.SqlTransaction, True) Then
          _result = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

        If Not Me.OperationType = Cage.CageOperationType.ToGamingTable _
              AndAlso Not Me.OperationType = Cage.CageOperationType.FromGamingTable _
              AndAlso Not Me.OperationType = Cage.CageOperationType.FromGamingTableClosing _
              AndAlso Not Me.OperationType = Cage.CageOperationType.FromGamingTableDropbox _
              AndAlso Not Me.OperationType = Cage.CageOperationType.FromGamingTableDropboxWithExpected _
              AndAlso Not GeneralParam.GetBoolean("Cage", "GamingTables.TicketsCollection.Enabled", 0) Then
          If _result = ENUM_CONFIGURATION_RC.CONFIGURATION_OK AndAlso Me.UpdateStackerCollection(_db_trx.SqlTransaction) = 1 Then
            _result = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
          End If
        End If

        ' Gaming Table Dropbox --> Update gaming table session
        If _result = ENUM_CONFIGURATION_RC.CONFIGURATION_OK AndAlso _
            (Me.OperationType = Cage.CageOperationType.FromGamingTableDropbox Or Me.OperationType = Cage.CageOperationType.FromGamingTableDropboxWithExpected) AndAlso _
            Not UpdateGamblingTableSessionsDropbox(Me.TableCurrencies, _db_trx.SqlTransaction) Then
          _result = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

        If _result = ENUM_CONFIGURATION_RC.CONFIGURATION_OK Then
          _result = Me.UpdateCageMeters(_db_trx.SqlTransaction)
        End If

        If _result = ENUM_CONFIGURATION_RC.CONFIGURATION_OK Then
          Call Me.CreateCollectionAlarms(_db_trx.SqlTransaction) ' Creation of the alarms if its exists
          Call _db_trx.Commit()
        Else
          Call _db_trx.Rollback()
        End If


      Catch ex As Exception
        Call _db_trx.Rollback()
        Call Trace.WriteLine(ex.ToString())
        Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                              "cls_cage_control", _
                              "RecolectMovement", _
                              ex.Message)

        _result = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End Try

    End Using

    If _result = ENUM_CONFIGURATION_RC.CONFIGURATION_OK Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3034), ENUM_MB_TYPE.MB_TYPE_INFO)
    Else
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3035), ENUM_MB_TYPE.MB_TYPE_WARNING)
    End If

    Return _result
  End Function ' RecolectMovement

  '----------------------------------------------------------------------------
  ' PURPOSE: To set as null the cage user on the collection movement
  '
  ' PARAMS:
  '       - SqlTrx
  '
  ' RETURNS:
  '     - UpdateCageUserToNothing
  '
  ' NOTES:
  Private Function UpdateCageUserToNothing(ByVal SqlTrx As SqlTransaction) As Boolean
    Dim _sb As StringBuilder
    Dim _operation_ok As Boolean

    _sb = New StringBuilder
    _operation_ok = True

    _sb.AppendLine("UPDATE   CAGE_MOVEMENTS ")
    _sb.AppendLine("   SET   CGM_USER_CAGE_ID = @pNothing ")
    _sb.AppendLine(" WHERE   CGM_MOVEMENT_ID = @pMovementId ")

    Try

      Using _cmd As New SqlClient.SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx)
        _cmd.Parameters.Add("@pNothing", SqlDbType.NVarChar).Value = DBNull.Value
        _cmd.Parameters.Add("@pMovementId", SqlDbType.BigInt).Value = Me.MovementID

        If _cmd.ExecuteNonQuery() <> 1 Then
          _operation_ok = False
        End If
      End Using

    Catch ex As Exception
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "cls_cage_control", _
                            "UpdateCageUserToNothing", _
                            ex.Message)

      Return False
    End Try

    Return _operation_ok
  End Function ' UpdateCageUserToNothing

  '----------------------------------------------------------------------------
  ' PURPOSE: To delete the movement details
  '
  ' PARAMS:
  '       - SqlTrx
  '
  ' RETURNS:
  '     - DeleteMovementDetails
  '
  ' NOTES:
  Private Function DeleteMovementDetails(ByVal SqlTrx As SqlTransaction) As Boolean
    Dim _sb As StringBuilder
    Dim _operation_ok As Boolean

    _sb = New StringBuilder
    _operation_ok = True

    _sb.AppendLine("DELETE   FROM CAGE_MOVEMENT_DETAILS ")
    _sb.AppendLine(" WHERE   CMD_MOVEMENT_ID = @pMovementId ")

    Try

      Using _cmd As New SqlClient.SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx)

        _cmd.Parameters.Add("@pMovementId", SqlDbType.BigInt).Value = Me.MovementID

        If _cmd.ExecuteNonQuery() < 0 Then
          _operation_ok = False
        End If

      End Using

    Catch ex As Exception
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "cls_cage_control", _
                            "DeleteMovementDetails", _
                            ex.Message)

      Return False
    End Try

    Return _operation_ok
  End Function ' DeleteMovementDetails

  '----------------------------------------------------------------------------
  ' PURPOSE: To cancel a recolection
  '
  ' PARAMS:
  '       None
  '
  ' RETURNS:
  '     - CancelCollectionMovement
  '
  ' NOTES:
  Private Function CancelCollectionMovement(ByVal SqlTrx As SqlTransaction) As Integer
    Dim _result As Integer

    _result = ENUM_CONFIGURATION_RC.CONFIGURATION_OK

    Try
      ' Update the status movement
      If _result = ENUM_CONFIGURATION_RC.CONFIGURATION_OK AndAlso Not Me.UpdateMovementStatus(Cage.CageStatus.Sent, SqlTrx) Then
        _result = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR
      End If

      ' Delete user cage from the movement
      If _result = ENUM_CONFIGURATION_RC.CONFIGURATION_OK AndAlso Not Me.UpdateCageUserToNothing(SqlTrx) Then
        _result = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR
      End If

      ' Delete movement details
      If _result = ENUM_CONFIGURATION_RC.CONFIGURATION_OK AndAlso Not Me.DeleteMovementDetails(SqlTrx) Then
        _result = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR
      End If

      ' Insert into pending movement
      If _result = ENUM_CONFIGURATION_RC.CONFIGURATION_OK AndAlso Not Me.InsertPendingMovement(SqlTrx) Then
        _result = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR
      End If


    Catch ex As Exception
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "cls_cage_control", _
                            "CancelCollectionMovement", _
                            ex.Message)

      _result = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

    Return _result
  End Function ' CancelCollectionMovement

  '----------------------------------------------------------------------------
  ' PURPOSE: Updates Cage Meters values
  '
  ' PARAMS:
  '       None
  '
  ' RETURNS:
  '
  ' NOTES:
  Private Function UpdateCageMeters(ByVal SqlTrx As SqlTransaction) As ENUM_CONFIGURATION_RC
    Dim _val_in As Decimal
    Dim _val_out As Decimal
    Dim _iso_code As String
    Dim _currency_type As CageCurrencyType
    Dim _res_meters As Decimal
    Dim _res_session_meters As Decimal
    Dim _decimal As Decimal
    Dim _national_currency As String
    Dim _has_tickets As Boolean
    Dim _update_tickets_in_global As Boolean
    Dim _dt_currencies_types As DataTable

    _has_tickets = False
    _update_tickets_in_global = False

    Try

      _national_currency = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode")

      For Each _row As DataRow In Me.TableCurrencies.Rows

        _iso_code = GetRowValue(_row(DT_COLUMN_CURRENCY.ISO_CODE))
        _currency_type = GetRowValue(_row(DT_COLUMN_CURRENCY.CURRENCY_TYPE))

        If GetRowValue(_row(DT_COLUMN_CURRENCY.DENOMINATION)) = -200 Then
          _has_tickets = True
          If CType(GetRowValue(_row(DT_COLUMN_CURRENCY.TOTAL)), Decimal) = 0 Then
            _update_tickets_in_global = True
          End If
        End If


        ' Update System Concepts [id = 0]
        If GetRowValue(_row(DT_COLUMN_CURRENCY.TOTAL)) > 0 And ((_currency_type <> FeatureChips.ChipType.RE And _currency_type <> FeatureChips.ChipType.NR And _currency_type <> FeatureChips.ChipType.COLOR) Or CType(GetRowValue(_row(DT_COLUMN_CURRENCY.DENOMINATION)), Decimal) <> -100) Then

          ' 06-OCT-2014 SMN
          _decimal = 0
          Try

            If _row(DT_COLUMN_CURRENCY.TOTAL) IsNot DBNull.Value AndAlso _
               Not String.IsNullOrEmpty(GetRowValue(_row(DT_COLUMN_CURRENCY.TOTAL))) Then
              _decimal = CType(GetRowValue(_row(DT_COLUMN_CURRENCY.TOTAL)), Decimal)
            End If
          Catch ex As Exception
          End Try

          If Me.MovementType = 0 Then
            _val_in = _decimal
            _val_out = 0
          Else
            _val_in = 0
            _val_out = _decimal
          End If

          CageMeters.UpdateCageMeters(_val_in _
                                    , _val_out _
                                    , Me.m_cage_session_id _
                                    , Me.SourceTargetId _
                                    , 0 _
                                    , _iso_code _
                                    , _currency_type _
                                    , CageMeters.UpdateCageMetersOperationType.Increment _
                                    , CageMeters.UpdateCageMetersgetGetSessionMode.FromParameter _
                                    , _res_meters _
                                    , _res_session_meters _
                                    , SqlTrx)


        End If

      Next

      _dt_currencies_types = GetAllowedISOCodeWithoutColor()

      ' Update Tickets values if necessary
      If _update_tickets_in_global Then
        If WSI.Common.Misc.IsFloorDualCurrencyEnabled() Then

          For Each _row As DataRow In Me.ConceptsData.Rows

            _iso_code = _row("CONCEPT_ISO_CODE")
            _currency_type = _row("CONCEPT_CAGE_CURRENCY_TYPE")

            If _row("CONCEPT_ID") = CInt(CageMeters.CageConceptId.Tickets) Then

              If _row("CONCEPT_AMOUNT") <> Currency.Zero Then
                ' 06-OCT-2014 SMN
                _decimal = CType(_row("CONCEPT_AMOUNT"), Decimal)

                If Me.MovementType = 0 Then
                  _val_in = _decimal
                  _val_out = 0
                Else
                  _val_in = 0
                  _val_out = _decimal
                End If

                CageMeters.UpdateCageMeters(_val_in _
                                           , _val_out _
                                           , Me.m_cage_session_id _
                                           , Me.SourceTargetId _
                                           , CInt(CageMeters.CageConceptId.Global) _
                                       , _iso_code _
                                       , _currency_type _
                                           , CageMeters.UpdateCageMetersOperationType.Increment _
                                           , CageMeters.UpdateCageMetersgetGetSessionMode.FromParameter _
                                           , _res_meters _
                                           , _res_session_meters _
                                           , SqlTrx)

              End If
            End If

          Next

        Else
          If Me.MovementType = 0 Then
            _val_in = Me.m_type_new_collection.real_ticket_sum
            _val_out = 0
          Else
            _val_in = 0
            _val_out = Me.m_type_new_collection.real_ticket_sum
          End If

          CageMeters.UpdateCageMeters(_val_in _
                                     , _val_out _
                                     , Me.m_cage_session_id _
                                     , Me.SourceTargetId _
                                     , 0 _
                                     , _national_currency _
                                     , 0 _
                                     , CageMeters.UpdateCageMetersOperationType.Increment _
                                     , CageMeters.UpdateCageMetersgetGetSessionMode.FromParameter _
                                     , _res_meters _
                                     , _res_session_meters _
                                     , SqlTrx)
        End If
      End If

      ' Update No-System Concepts [id != 0]

      For Each _row As DataRow In Me.ConceptsData.Rows

        _iso_code = _row("CONCEPT_ISO_CODE")
        _currency_type = _row("CONCEPT_CAGE_CURRENCY_TYPE")

        If Not (Me.SourceTargetId = CageMeters.CageSystemSourceTarget.Cashiers And Me.MovementType = 0) Or _
    _row("CONCEPT_ID") = CInt(CageMeters.CageConceptId.Tickets) Then

          If _row("CONCEPT_AMOUNT") <> Currency.Zero Then
            ' 06-OCT-2014 SMN
            _decimal = CType(_row("CONCEPT_AMOUNT"), Decimal)

            If Me.MovementType = 0 Then
              _val_in = _decimal
              _val_out = 0
            Else
              _val_in = 0
              _val_out = _decimal
            End If

            CageMeters.UpdateCageMeters(_val_in _
                                      , _val_out _
                                      , Me.m_cage_session_id _
                                      , Me.SourceTargetId _
                                      , CType(_row("CONCEPT_ID"), Int64) _
                                      , _iso_code _
                                      , _currency_type _
                                      , CageMeters.UpdateCageMetersOperationType.Increment _
                                      , CageMeters.UpdateCageMetersgetGetSessionMode.FromParameter _
                                      , _res_meters _
                                      , _res_session_meters _
                                      , SqlTrx)

          End If
        End If

      Next

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

    Catch ex As Exception
      Log.Error(ex.Message())
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR
    End Try

  End Function ' UpdateCageMeters

  '----------------------------------------------------------------------------
  ' PURPOSE: Returns if cashier session is open
  '
  ' PARAMS:
  '       SqlTrx
  '
  ' RETURNS:
  '
  ' NOTES:
  Friend Function IsCashierSessionOpen(ByVal SqlTrx As SqlTransaction) As Boolean

    Dim _sb As StringBuilder
    Dim _obj As Object

    _sb = New StringBuilder()

    _sb.AppendLine("SELECT COUNT(CS_SESSION_ID)           ")
    _sb.AppendLine("  FROM   CASHIER_SESSIONS             ")
    _sb.AppendLine(" WHERE   CS_SESSION_ID = @pSessionId  ")
    _sb.AppendLine("   AND   CS_STATUS = @pStatus         ")

    Using _cmd As New SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx)
      _cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = Me.CashierSessionId
      _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.OPEN

      _obj = _cmd.ExecuteScalar()
      If _obj IsNot Nothing AndAlso _obj = 1 Then
        Return True
      End If

    End Using

    Return False
  End Function ' IsCashierSessionOpen

  '----------------------------------------------------------------------------
  ' PURPOSE: Get cashier terminal id by cage movement id
  '
  ' PARAMS:
  '       CageMovementId
  '       Transaction
  '
  ' RETURNS:
  '       Termianl Id
  ' NOTES:
  Private Function GetCashierTerminalIdByCageMovId(ByVal CageMovementId As Int64, ByVal SqlTrx As SqlTransaction) As Int64
    Dim _sb As StringBuilder
    Dim _obj As Object
    Dim _result As Int64

    _sb = New StringBuilder()
    _obj = Nothing
    _result = 0

    _sb.AppendLine("SELECT   CGM_TERMINAL_CASHIER_ID             ")
    _sb.AppendLine("  FROM   CAGE_MOVEMENTS                      ")
    _sb.AppendLine(" WHERE   CGM_MOVEMENT_ID = @pCageMovementId  ")

    Try
      Using _cmd As SqlCommand = New SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx)
        _cmd.Parameters.Add("@pCageMovementId", SqlDbType.BigInt).Value = CageMovementId
        _obj = _cmd.ExecuteScalar()

        If _obj IsNot Nothing AndAlso CInt(_obj) > 0 Then
          _result = CInt(_obj)
        End If
      End Using
    Catch _ex As Exception

    End Try
    Return _result
  End Function ' GetCashierTerminalIdByCageMovId

  ''' <summary>
  ''' Get SQL with the CountR terminals
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function getSQLCountRComboTerminals() As String

    Dim _sb_CountR As New StringBuilder
    _sb_CountR.AppendLine("                        SELECT   DISTINCT(CT_CASHIER_ID)                                             ") ' CountR's dados de alta en Cashier_Terminals
    _sb_CountR.AppendLine(String.Format( _
                          "                               , CT_NAME + '{0}' AS CT_NAME                                          ", Me.m_countr_sufix))
    _sb_CountR.AppendLine("                          FROM   CASHIER_TERMINALS CT				      	                                ")
    _sb_CountR.AppendLine("                    INNER JOIN   COUNTR CR ON CR.CR_COUNTR_ID = CT_COUNTR_ID AND CR.CR_ENABLED = 1   ")
    _sb_CountR.AppendLine("                         WHERE   CT.CT_COUNTR_ID IS NOT NULL                                         ")
    _sb_CountR.AppendLine("                         UNION                                                                       ")
    _sb_CountR.AppendLine(String.Format( _
                          "                        SELECT   DISTINCT(CR_COUNTR_ID) + {0:D}                                      ", COUNTR_ID_OFFSET)) ' CountR's NO dados de alta en Cashier_Terminals . Sumar 100.000 al ID
    _sb_CountR.AppendLine(String.Format( _
                          "                               , CR_NAME + '{0}' AS CT_NAME                                          ", Me.m_countr_sufix))
    _sb_CountR.AppendLine("                          FROM   COUNTR CR            			      	                                  ")
    _sb_CountR.AppendLine("               LEFT OUTER JOIN   CASHIER_TERMINALS CT ON CR.CR_COUNTR_ID = CT_COUNTR_ID              ")
    _sb_CountR.AppendLine("                         WHERE   CR.CR_ENABLED = 1 AND CT.CT_COUNTR_ID IS NULL                       ")

    Return _sb_CountR.ToString()

  End Function ' getSQLCountRComboTerminals

  ''' <summary>
  ''' Get SQL with the Cashier terminals
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function getSQLCashiersComboTerminals() As String

    Dim _sb_cashiers As New StringBuilder
    Dim _only_cashier_is_open As Boolean = False

    _only_cashier_is_open = GeneralParam.GetBoolean("Cage", "Transfer.OnlyCashierIsOpen", False)

    _sb_cashiers.AppendLine("                        SELECT   DISTINCT(CT_CASHIER_ID)                                                             ")
    _sb_cashiers.AppendLine("                               , CT_NAME	                                                                            ")
    _sb_cashiers.AppendLine("                          FROM   CASHIER_TERMINALS CT 	                                                              ")

    If _only_cashier_is_open Then
      _sb_cashiers.AppendLine("                    INNER JOIN   CASHIER_SESSIONS CS ON CS.CS_CASHIER_ID = CT.CT_CASHIER_ID                        ")
      _sb_cashiers.AppendLine("                    INNER JOIN   GUI_USERS        GU ON   GU.GU_USER_ID = CS.CS_USER_ID                            ")
    End If

    _sb_cashiers.AppendLine("                         WHERE   CT.CT_COUNTR_ID IS NULL				                                                      ")

    If (OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.NEW_MOVEMENT) Then
      _sb_cashiers.AppendLine("                           AND   CT.CT_TERMINAL_ID IS NULL				                                                ")
      _sb_cashiers.AppendLine("                           AND   CT.CT_GAMING_TABLE_ID IS NULL			                                              ")
    End If

    If _only_cashier_is_open Then
      _sb_cashiers.AppendLine(String.Format("                           AND   CS.CS_STATUS = {0}       ", Convert.ToInt32(WSI.Common.CASHIER_SESSION_STATUS.OPEN)))
      _sb_cashiers.AppendLine(String.Format("                           AND   GU.GU_BLOCK_REASON = {0} ", Convert.ToInt32(WSI.Common.GUI_USER_BLOCK_REASON.NONE)))
    End If

    Return _sb_cashiers.ToString()

  End Function ' getSQLCashiersComboTerminals

#End Region ' Private

#Region "Public"

  '----------------------------------------------------------------------------
  ' PURPOSE: Gets gui_user fullname
  '
  ' PARAMS:
  '       - movement ID
  '
  ' RETURNS:
  '     - userfullname
  '
  ' NOTES:
  Public Function GetUserFullName(ByVal UserId As String) As String
    Dim _sb As StringBuilder
    Dim _obj As Object
    Dim _user_name As String

    _user_name = AUDIT_NONE_STRING

    If UserId <> String.Empty Then

      _sb = New StringBuilder

      _sb.AppendLine(" SELECT   GU_FULL_NAME ")
      _sb.AppendLine("   FROM   GUI_USERS ")
      _sb.AppendLine("  WHERE   GU_USER_ID = @pUserId ")

      Try

        Using _db_trx As New DB_TRX()
          Using _cmd As New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

            _cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = UserId

            _obj = _cmd.ExecuteScalar()

            If _obj IsNot Nothing AndAlso _obj IsNot DBNull.Value Then
              _user_name = _obj
            End If

          End Using
        End Using

      Catch ex As Exception
        Call Trace.WriteLine(ex.ToString())
        Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                              "cls_cage_control", _
                              "GetUserFullName", _
                              ex.Message)

        Return AUDIT_NONE_STRING
      End Try

    End If

    Return _user_name
  End Function ' GetUserFullName

  '----------------------------------------------------------------------------
  ' PURPOSE: Gets row values
  '
  ' PARAMS:
  '       - Value
  '       - Optional ReturnNothing
  '
  ' RETURNS:
  '     - 
  '
  ' NOTES:
  Public Function GetRowValue(ByVal Value As Object, Optional ByVal ReturnNothing As Boolean = False) As Object
    Dim _row_value As Object

    _row_value = 0

    If ReturnNothing Then
      _row_value = Nothing
    End If

    If Not Value Is DBNull.Value AndAlso Not String.IsNullOrEmpty(Value) Then
      _row_value = Value
    End If

    Return _row_value
  End Function ' GetRowValue

  '----------------------------------------------------------------------------
  ' PURPOSE: Returns Allowed ISO CODE
  '
  ' PARAMS:
  '       - Datatable
  '
  ' RETURNS:
  '     - 
  '
  ' NOTES:
  Public Function GetAllowedISOCode(Optional ByVal IsoCode As String = "") As DataTable
    Dim _sql_curr As StringBuilder
    Dim _sql_chip As StringBuilder
    Dim _table_curr As DataTable
    Dim _table_chip As DataTable
    Dim _row_curr As DataRow
    Dim _table_temp As DataTable
    Dim _row_chips As DataRow()
    Dim _row_chip As DataRow
    Dim _num_row As Int32

    m_national_currency = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode")
    _table_chip = Nothing

    _sql_curr = New StringBuilder
    _sql_chip = New StringBuilder

    _sql_curr.AppendLine("SELECT CE_CURRENCY_ORDER AS N ") '1
    _sql_curr.AppendLine("     , VA.CGC_ISO_CODE ")  '2                           
    _sql_curr.AppendLine("     , VA.TYPE ")          '3
    _sql_curr.AppendLine("     , VA.NAME ")          '4
    ' XGJ 09-DEC-2016
    _sql_curr.AppendLine("     , CGC_ALLOWED ")          '4
    _sql_curr.AppendLine("  FROM (SELECT  DISTINCT ISNULL(CE_CURRENCY_ORDER , CASE WHEN CGC_ISO_CODE = '" & m_national_currency & "' THEN 0 ELSE 99999999 END) AS CE_CURRENCY_ORDER ")
    _sql_curr.AppendLine("              , (CGC_ISO_CODE)  AS CGC_ISO_CODE  ")
    _sql_curr.AppendLine("              , 0 AS TYPE ")
    _sql_curr.AppendLine("              , CASE WHEN CGC_ISO_CODE = '" & m_national_currency & "' THEN 0 ELSE 1 END AS N_CURR ")
    _sql_curr.AppendLine("              , '' AS NAME ")
    ' XGJ 09-DEC-2016
    _sql_curr.AppendLine("              , CGC_ALLOWED ")
    _sql_curr.AppendLine("          FROM  CAGE_CURRENCIES  ")
    _sql_curr.AppendLine("     LEFT JOIN  CURRENCY_EXCHANGE ON CE_CURRENCY_ISO_CODE = CGC_ISO_CODE AND CE_TYPE = 0 ) AS VA ")

    If Not String.IsNullOrEmpty(IsoCode) Then
      _sql_curr.AppendLine("         WHERE VA.CGC_ISO_CODE = '" & IsoCode & "' ")
      ' XGJ 09-DEC-2016
      _sql_curr.AppendLine("           AND CGC_ALLOWED = 1 ")
    Else
      ' XGJ 09-DEC-2016
      _sql_curr.AppendLine("         WHERE CGC_ALLOWED = 1 ")
    End If

    _sql_curr.AppendLine("         ORDER  BY 1, 2, 3 ")

    If String.IsNullOrEmpty(IsoCode) Then
      _sql_chip.AppendLine("    SELECT  DISTINCT ISNULL(CE_CURRENCY_ORDER , CASE WHEN CHS_ISO_CODE = '" & m_national_currency & "' THEN 0 ELSE 99999999 END) AS CE_CURRENCY_ORDER ")
      _sql_chip.AppendLine("          , CHS_ISO_CODE  ")
      _sql_chip.AppendLine("          , ISNULL(CHS_CHIP_TYPE, 0) AS TYPE ")
      _sql_chip.AppendLine("          , CHS_NAME ")
      _sql_chip.AppendLine("          , CHS_NAME ")
      _sql_chip.AppendLine("      FROM  CHIPS_SETS  ")
      _sql_chip.AppendLine(" LEFT JOIN  CURRENCY_EXCHANGE ON CE_CURRENCY_ISO_CODE = CHS_ISO_CODE AND CE_TYPE = 0 ")
      _sql_chip.AppendLine("     WHERE  CHS_ALLOWED = 1 ")
      _sql_chip.AppendLine("     ORDER  BY 1, 2, 3 ")
    End If


    Try

      _table_curr = GUI_GetTableUsingSQL(_sql_curr.ToString, Integer.MaxValue)
      If String.IsNullOrEmpty(IsoCode) Then
        _table_chip = GUI_GetTableUsingSQL(_sql_chip.ToString, Integer.MaxValue)
      End If

      _num_row = _table_curr.Rows.Count()

      For Each _row As DataRow In _table_curr.Rows
        If _row("NAME") = "" Then
          _row("NAME") = GetCurrencyDescription(_row("CGC_ISO_CODE"), _row("TYPE"))
        End If

      Next

      _table_temp = _table_curr.Copy()

      For Each _row As DataRow In _table_temp.Rows

        If IsFeatureChipsEnabled And Not _table_chip Is Nothing Then
          If IsChipsValueTypeEnabled Then
            _row_curr = _table_curr.NewRow()
            _row_chips = _table_chip.Select(" CHS_ISO_CODE = '" & _row("CGC_ISO_CODE") & "' AND TYPE = '" & FeatureChips.ChipType.RE & "' ")
            If _row_chips.Length > 0 Then
              _row_chip = _row_chips(0)
              _row_curr("N") = _num_row
              _row_curr("CGC_ISO_CODE") = _row_chip("CHS_ISO_CODE")
              _row_curr("TYPE") = FeatureChips.ChipType.RE
              _row_curr("NAME") = FeatureChips.GetChipTypeDescription(FeatureChips.ChipType.RE, _row_chip("CHS_ISO_CODE"))

              _table_curr.Rows.Add(_row_curr)
              _num_row += 1
            End If
          End If

          If IsChipsNonRedeemableTypeEnabled Then
            _row_curr = _table_curr.NewRow()
            _row_chips = _table_chip.Select(" CHS_ISO_CODE = '" & _row("CGC_ISO_CODE") & "' AND TYPE = '" & FeatureChips.ChipType.NR & "' ")
            If _row_chips.Length > 0 Then
              _row_chip = _row_chips(0)
              _row_curr("N") = _num_row
              _row_curr("CGC_ISO_CODE") = _row_chip("CHS_ISO_CODE")
              _row_curr("TYPE") = FeatureChips.ChipType.NR
              _row_curr("NAME") = FeatureChips.GetChipTypeDescription(FeatureChips.ChipType.NR, _row_chip("CHS_ISO_CODE"))

              _table_curr.Rows.Add(_row_curr)
              _num_row += 1
            End If
          End If

        End If
      Next

      If IsChipsColorTypeEnabled And Not _table_chip Is Nothing Then
        _row_curr = _table_curr.NewRow()
        _row_chips = _table_chip.Select(" CHS_ISO_CODE = '" & Cage.CHIPS_COLOR & "' AND TYPE = '" & FeatureChips.ChipType.COLOR & "' ")
        If _row_chips.Length > 0 Then
          _row_chip = _row_chips(0)
          _row_curr("N") = _num_row
          _row_curr("CGC_ISO_CODE") = _row_chip("CHS_ISO_CODE")
          _row_curr("TYPE") = FeatureChips.ChipType.COLOR
          _row_curr("NAME") = FeatureChips.GetChipTypeDescription(FeatureChips.ChipType.COLOR, _row_chip("CHS_ISO_CODE"))

          _table_curr.Rows.Add(_row_curr)
          _num_row += 1
        End If
      End If

    Catch ex As Exception
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "cls_cage_control", _
                            "GetCageAmounts", _
                            ex.Message)

      Return New DataTable
    End Try

    Return _table_curr
  End Function ' GetAllowedISOCode

  '----------------------------------------------------------------------------
  ' PURPOSE: Returns Allowed ISO CODE
  '
  ' PARAMS:
  '       - Datatable
  '
  ' RETURNS:
  '     - 
  '
  ' NOTES:
  Public Function GetAllowedChipSets(GamingTableId As Int64) As String
    Dim _sql_chip_sets As StringBuilder
    Dim _table_chip As DataTable
    Dim _chips_sets As String

    _sql_chip_sets = New StringBuilder

    _chips_sets = String.Empty

    _sql_chip_sets.AppendLine(" SELECT  DISTINCT  ")
    _sql_chip_sets.AppendLine("         GTCS_CHIPS_SET_ID  ")
    _sql_chip_sets.AppendLine("   FROM  GAMING_TABLE_CHIPS_SETS  ")
    _sql_chip_sets.AppendLine("  WHERE  GTCS_GAMING_TABLE_ID = " & GamingTableId.ToString() & " ")
    _sql_chip_sets.AppendLine("  ORDER  BY GTCS_CHIPS_SET_ID ")

    _table_chip = GUI_GetTableUsingSQL(_sql_chip_sets.ToString, Integer.MaxValue)

    For Each _row As DataRow In _table_chip.Rows
      _chips_sets &= _row("GTCS_CHIPS_SET_ID").ToString() & ", "
    Next

    If _chips_sets.Length > 2 Then
      Return _chips_sets.Substring(0, _chips_sets.Length() - 2)
    Else
      Return _chips_sets
    End If

  End Function


  Public Function GetAllowedISOCodeWithoutColor() As DataTable
    Dim _dt_currencies_types As DataTable
    Dim _row_to_delete As DataRow()

    _dt_currencies_types = GetAllowedISOCode()

    Try
      For Each _dr As DataRow In _dt_currencies_types.Copy().Rows
        If _dr("TYPE") = FeatureChips.ChipType.COLOR Then
          _row_to_delete = _dt_currencies_types.Select("CGC_ISO_CODE = '" & _dr("CGC_ISO_CODE") & "' AND TYPE = " & _dr("TYPE") & " ")
          _row_to_delete(0).Delete()
        End If
      Next

    Catch ex As Exception
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_USER_MESSAGE, _
                      "cls_cage_control", _
                      "GetAllowedISOCodeWithoutColor", _
                      ex.Message)
    End Try

    Return _dt_currencies_types

  End Function  ' GetAllowedISOCodeWithoutColor

  '----------------------------------------------------------------------------
  ' PURPOSE: Returns amounts for collect movements
  '
  ' PARAMS:
  '       - Datatable
  '
  ' RETURNS:
  '     - 
  '
  ' NOTES:
  Public Function GetCageAmountsCollect() As DataTable
    Dim _sql As StringBuilder
    Dim _table As DataTable

    _sql = New StringBuilder

    _sql.AppendLine("   SELECT      ROW_NUMBER() OVER (ORDER BY CM_CURRENCY_ISO_CODE) n, ")
    _sql.AppendLine("               VA.CM_CURRENCY_ISO_CODE ")
    _sql.AppendLine("  FROM (SELECT  DISTINCT (CM_CURRENCY_ISO_CODE) ")
    _sql.AppendLine("          FROM  CASHIER_MOVEMENTS ")
    _sql.AppendLine("    INNER JOIN    CAGE_CASHIER_MOVEMENT_RELATION ")
    _sql.AppendLine("            ON    CASHIER_MOVEMENTS.CM_MOVEMENT_ID = CAGE_CASHIER_MOVEMENT_RELATION.CM_MOVEMENT_ID ")
    _sql.AppendLine("         WHERE    CAGE_CASHIER_MOVEMENT_RELATION.CGM_MOVEMENT_ID = " & Me.MovementID & ") AS VA")

    Try

      _table = GUI_GetTableUsingSQL(_sql.ToString, Integer.MaxValue)

    Catch ex As Exception
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "cls_cage_control", _
                            "GetCageAmountsCollect", _
                            ex.Message)

      Return New DataTable
    End Try

    Return _table
  End Function ' GetCageAmountsCollect

  '----------------------------------------------------------------------------
  ' PURPOSE: Gets all allowed denominations
  '
  ' PARAMS:
  '
  ' RETURNS:
  '       - Datatable
  '
  ' NOTES:
  Public Function GetAllowedDenominations() As DataTable
    Dim _sql As StringBuilder
    Dim _table As DataTable

    _sql = New StringBuilder

    _sql.AppendLine("   SELECT   CGC_ISO_CODE AS ISO ")
    _sql.AppendLine("          , CGC_DENOMINATION AS DENOMINATION ")
    _sql.AppendLine("          , CASE WHEN CGC_DENOMINATION > 0 THEN ISNULL(CGC_CAGE_CURRENCY_TYPE,0) ELSE 99 END AS TYPE ")
    _sql.AppendLine("          , -1 AS CHIP_ID ")
    _sql.AppendLine("     FROM   CAGE_CURRENCIES ")
    _sql.AppendLine("    WHERE   CGC_ALLOWED = 1 ")
    _sql.AppendLine("    UNION ")
    _sql.AppendLine("   SELECT   CHS_ISO_CODE AS ISO ")
    _sql.AppendLine("          , CH_DENOMINATION AS DENOMINATION ")
    '_sql.AppendLine("          , 0 AS TYPE ")
    _sql.AppendLine("          , ISNULL(CHS_CHIP_TYPE,0) AS TYPE ")
    _sql.AppendLine("          , CH_CHIP_ID AS CHIP_ID ")
    _sql.AppendLine("     FROM   CHIPS_SETS ")
    _sql.AppendLine("     LEFT   JOIN CHIPS_SETS_CHIPS ON CSC_SET_ID = CHS_CHIP_SET_ID ")
    _sql.AppendLine("     LEFT   JOIN CHIPS ON CSC_CHIP_ID = CH_CHIP_ID ")
    _sql.AppendLine("                      AND CH_ISO_CODE = CHS_ISO_CODE ")
    _sql.AppendLine("                      AND CH_CHIP_TYPE = CHS_CHIP_TYPE ")
    _sql.AppendLine("    WHERE   CHS_ALLOWED = 1 ")
    _sql.AppendLine("      AND   CH_ALLOWED = 1 ")

    Try

      _table = GUI_GetTableUsingSQL(_sql.ToString, Integer.MaxValue)

    Catch ex As Exception
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "cls_cage_control", _
                            "GetAllowedDenominations", _
                            ex.Message)

      Return New DataTable
    End Try

    Return _table
  End Function ' GetAllowedDenominations

  '----------------------------------------------------------------------------
  ' PURPOSE: gets details from cashier
  '
  ' PARAMS:
  '       - UserId
  '       - EntryFieldOpening
  '       - EntryFieldCashier
  '
  ' RETURNS:
  '
  ' NOTES:
  Public Sub SetCashierData(ByVal UserId As Integer _
                          , ByRef EntryFieldOpening As uc_entry_field _
                          , ByRef EntryFieldCashier As uc_entry_field)

    Dim _sb As StringBuilder
    Dim _date As String
    Dim _cashier As String

    _sb = New StringBuilder

    _date = AUDIT_NONE_STRING
    _cashier = AUDIT_NONE_STRING

    If (Me.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.CANCEL_MOVEMENT And Me.OperationType = Cage.CageOperationType.FromCashier) _
        OrElse (Me.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.CANCEL_MOVEMENT And Me.OperationType = Cage.CageOperationType.FromCashierClosing) _
        OrElse (Me.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.VIEW_MOVEMENT And Me.OperationType = Cage.CageOperationType.ToCashier) Then

      _sb.AppendLine("SELECT    CM_DATE      ")
      _sb.AppendLine("        , CM_CASHIER_ID")
      _sb.AppendLine("        , CM_CASHIER_NAME")
      _sb.AppendLine("  FROM    CASHIER_MOVEMENTS ")
      _sb.AppendLine(" INNER  JOIN CAGE_CASHIER_MOVEMENT_RELATION ON CAGE_CASHIER_MOVEMENT_RELATION.CM_MOVEMENT_ID = CASHIER_MOVEMENTS.CM_MOVEMENT_ID")
      _sb.AppendLine(" WHERE    CGM_MOVEMENT_ID = @pMovementID")
    Else


      _sb.AppendLine("SELECT    TOP 1 CS_OPENING_DATE ")
      _sb.AppendLine("        , CS_CASHIER_ID ")
      _sb.AppendLine("        , CT_NAME")
      _sb.AppendLine("  FROM    CASHIER_SESSIONS ")
      _sb.AppendLine("        , CASHIER_TERMINALS ")
      _sb.AppendLine(" WHERE    CS_STATUS = @pStatusOpen ")
      _sb.AppendLine("   AND    CS_USER_ID = @pUserId ")
      _sb.AppendLine("   AND    CS_CASHIER_ID = CT_CASHIER_ID ")
    End If

    Try

      Using _db_trx As New DB_TRX()
        Using _sql_cmd As SqlCommand = New SqlCommand(_sb.ToString())

          _sql_cmd.Parameters.Add("@pStatusOpen", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.OPEN
          _sql_cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = UserId
          _sql_cmd.Parameters.Add("@pMovementID", SqlDbType.BigInt).Value = Me.MovementID
          Using _sql_reader As SqlDataReader = _db_trx.ExecuteReader(_sql_cmd)

            If _sql_reader.Read() Then
              _date = GUI_FormatDate(_sql_reader.GetDateTime(0), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
              _cashier = Computer.Alias(_sql_reader.GetString(2))
            End If

          End Using
        End Using
      End Using

    Catch ex As Exception
      _date = AUDIT_NONE_STRING
      _cashier = AUDIT_NONE_STRING
    End Try

    EntryFieldOpening.TextValue = _date
    EntryFieldCashier.TextValue = _cashier

  End Sub ' SetCashierData

  Public Sub AddExpectedColumnInCurrencies()

    If Not IsNothing(Me.m_table_currencies) AndAlso Not Me.m_table_currencies.Columns.Contains("EXPECTED_COLUMN") Then
      Dim _expected_column As DataColumn = New DataColumn
      With _expected_column
        .DataType = System.Type.GetType("System.Decimal")
        .ColumnName = "EXPECTED_COLUMN"
      End With
      ' Add columns to DataTable
      With Me.m_table_currencies.Columns
        .Add(_expected_column)
      End With
    End If

    If Me.m_table_currencies.Columns.Contains("EXPECTED_COLUMN") Then
      If CurrentUser.Permissions(ENUM_FORM.FORM_CAGE_SHOW_EXPECTED_AMOUNTS).Write Then
        Me.TableCurrencies.Columns(DT_COLUMN_CURRENCY.EXPECTED_COLUMN).Expression = "IIF( QUANTITY IS NULL OR (DENOMINATION = -100 AND QUANTITY = 0 AND TOTAL = 0 AND TOTAL_DIFFERENCE <> 0) OR (DENOMINATION = " & Cage.COINS_COLLECTION_CODE.ToString() & " AND QUANTITY = 0 AND TOTAL = 0 AND TOTAL_DIFFERENCE <> 0) , CASHIER_TOTAL, TOTAL )"
      Else
        Me.TableCurrencies.Columns(DT_COLUMN_CURRENCY.EXPECTED_COLUMN).Expression = "IIF( QUANTITY IS NULL OR (DENOMINATION = -100 AND QUANTITY = 0 AND TOTAL = 0 AND TOTAL_DIFFERENCE <> 0)  OR (DENOMINATION = " & Cage.COINS_COLLECTION_CODE.ToString() & " AND QUANTITY = 0 AND TOTAL = 0 AND TOTAL_DIFFERENCE <> 0), 0, TOTAL )"
      End If
    End If


  End Sub


  '----------------------------------------------------------------------------
  ' PURPOSE: Initializes currency data
  '
  ' PARAMS:
  '     - IsNew
  '     - UseTableCurrencies
  '     - IsoCode
  '
  ' RETURNS:
  '
  ' NOTES:
  Public Sub InitCurrencyData(Optional ByVal IsNew As Boolean = False _
                            , Optional ByVal UseTableCurrencies As Boolean = True _
                            , Optional ByVal IsoCode As String = "" _
                            , Optional ByVal FromCageStock As Boolean = False)

    Dim _sb As StringBuilder
    Dim _sb_chips As StringBuilder
    Dim _row As DataRow
    Dim _aux_datatable As DataTable
    Dim _show_denominations As Cage.ShowDenominationsMode
    Dim _str_monto As String

    m_national_currency = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode")

    _sb = New StringBuilder
    _sb_chips = New StringBuilder
    _aux_datatable = New DataTable
    _str_monto = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3372)

    If Me.OperationType = Cage.CageOperationType.FromTerminal Then
      _show_denominations = Cage.ShowDenominationsMode.ShowAllDenominations
    Else
      _show_denominations = GeneralParam.GetInt32("Cage", "ShowDenominations", Cage.ShowDenominationsMode.ShowAllDenominations)
    End If

    _sb.AppendLine("    SELECT   CGC_ISO_CODE AS ISO_CODE")
    _sb.AppendLine("           , CGC_DENOMINATION AS DENOMINATION")
    _sb.AppendLine("           , CASE WHEN CGC_DENOMINATION >= 0 THEN ISNULL(CGC_CAGE_CURRENCY_TYPE,0) ELSE 99 END AS TYPE ")
    _sb.AppendLine("           , '' AS COLOR ")
    _sb.AppendLine("           , '' AS CHIP_SET_NAME ")
    _sb.AppendLine("           , '' AS NAME ")
    _sb.AppendLine("           , '' AS DRAWING ")
    _sb.AppendLine("           , '' AS CHIP_ID ")
    _sb.AppendLine("           , '' AS CHIP_SET_ID ")
    _sb.AppendLine("           , ISNULL(CE_CURRENCY_ORDER, CASE WHEN CGC_ISO_CODE = '" & m_national_currency & "' THEN 0 ELSE 99999999 END) AS N ")
    _sb.AppendLine("      FROM   CAGE_CURRENCIES ")
    _sb.AppendLine(" LEFT JOIN  CURRENCY_EXCHANGE ON CE_CURRENCY_ISO_CODE = CGC_ISO_CODE AND CE_TYPE = 0")
    _sb.AppendLine("     WHERE  CGC_CAGE_VISIBLE = 1") ' Show visible denominations.

    _sb_chips.AppendLine("    SELECT   CHS_ISO_CODE AS ISO_CODE ")
    _sb_chips.AppendLine("           , CH_DENOMINATION AS DENOMINATION ")
    _sb_chips.AppendLine("           , ISNULL(CHS_CHIP_TYPE,0)  AS TYPE ")
    _sb_chips.AppendLine("           , ISNULL(CH_COLOR, 0) AS COLOR ")
    _sb_chips.AppendLine("           , ISNULL(CHS_NAME, '') AS CHIP_SET_NAME ")
    _sb_chips.AppendLine("           , ISNULL(CH_NAME, '') AS NAME ")
    _sb_chips.AppendLine("           , ISNULL(CH_DRAWING, '') AS DRAWING ")
    _sb_chips.AppendLine("           , CH_CHIP_ID AS CHIP_ID ")
    _sb_chips.AppendLine("           , ISNULL(CHS_CHIP_SET_ID, 0) AS CHIP_SET_ID ")
    _sb_chips.AppendLine("           , ISNULL(CE_CURRENCY_ORDER, CASE WHEN CHS_ISO_CODE = '" & m_national_currency & "' THEN 0 ELSE 99999999 END) AS N ")
    _sb_chips.AppendLine("      FROM   CHIPS_SETS ")
    _sb_chips.AppendLine("      LEFT   JOIN CHIPS_SETS_CHIPS ON CSC_SET_ID = CHS_CHIP_SET_ID")
    _sb_chips.AppendLine("      LEFT   JOIN CHIPS ON CH_CHIP_ID = CSC_CHIP_ID")
    _sb_chips.AppendLine("                       AND CH_ISO_CODE = CHS_ISO_CODE ")
    _sb_chips.AppendLine("                       AND CH_CHIP_TYPE = CHS_CHIP_TYPE ")
    _sb_chips.AppendLine(" LEFT JOIN   CURRENCY_EXCHANGE ON CE_CURRENCY_ISO_CODE = CHS_ISO_CODE AND CE_TYPE = 0 ")
    _sb_chips.AppendLine("     WHERE   1 = 1 ")

    If Not String.IsNullOrEmpty(IsoCode) Then
      _sb.AppendLine("  AND   CGC_ISO_CODE = '" + IsoCode + "'")
      _sb_chips.AppendLine("  AND   CHS_ISO_CODE = '" + IsoCode + "'")
    End If

    If Not TITO.Utils.IsTitoMode() Then
      _sb.AppendLine("  AND   CGC_DENOMINATION <> " + Cage.TICKETS_CODE.ToString())
    End If

    If Me.OperationType = Cage.CageOperationType.FromTerminal Then
      ' if the collection came from a terminal the coins, bank card and ckecks cant be shown 
      _sb.AppendLine("  AND   CGC_DENOMINATION <> " + Cage.BANK_CARD_CODE.ToString())
      _sb.AppendLine("  AND   CGC_DENOMINATION <> " + Cage.CHECK_CODE.ToString())
    Else
      If _show_denominations = Cage.ShowDenominationsMode.HideAllDenominations Then
        If Me.OperationType = Cage.CageOperationType.CountCage Then
          _sb.AppendLine("  AND   CGC_DENOMINATION IN (" & Cage.COINS_CODE & "," & Cage.BANK_CARD_CODE & "," & Cage.CHECK_CODE & ")")
          _sb.AppendLine("  OR CGC_DENOMINATION = " & Cage.TICKETS_CODE & " AND CGC_ISO_CODE = '" & GeneralParam.GetString("RegionalOptions", "CurrencyISOCode", "MXN") & "'")
        Else
          _sb.AppendLine("  AND   CGC_DENOMINATION = " & Cage.COINS_CODE)
        End If

        _sb_chips.AppendLine("   AND ( CH_CHIP_ID IN (           ")
        _sb_chips.AppendLine("         SELECT MIN(B.CH_CHIP_ID) ")
        _sb_chips.AppendLine("         FROM CHIPS AS B  ")
        _sb_chips.AppendLine("         INNER JOIN CHIPS_SETS_CHIPS AS J ON B.CH_CHIP_ID= J.CSC_CHIP_ID ")
        _sb_chips.AppendLine("         INNER JOIN ( SELECT MIN(C.CH_DENOMINATION) AS DENOMINATION, C.CH_ISO_CODE, C.CH_CHIP_TYPE ")   ' , D.CSC_SET_ID
        _sb_chips.AppendLine("                 FROM CHIPS AS C  ")
        _sb_chips.AppendLine("                 INNER JOIN CHIPS_SETS_CHIPS AS D ON C.CH_CHIP_ID= D.CSC_CHIP_ID ")
        _sb_chips.AppendLine("                 INNER JOIN CHIPS_SETS AS E ON E.CHS_CHIP_SET_ID = D.CSC_SET_ID ")
        _sb_chips.AppendLine("                 WHERE (C.CH_DENOMINATION >= 1 OR C.CH_ISO_CODE = '" & Cage.CHIPS_COLOR & "') ")
        _sb_chips.AppendLine("                 AND E.CHS_ALLOWED = 1  ")
        _sb_chips.AppendLine("                 GROUP BY C.CH_ISO_CODE, C.CH_CHIP_TYPE ")      ' , D.CSC_SET_ID
        _sb_chips.AppendLine("                 ) AS H ON H.CH_ISO_CODE = B.CH_ISO_CODE AND H.CH_CHIP_TYPE = B.CH_CHIP_TYPE AND H.DENOMINATION = B.CH_DENOMINATION ")  ' AND H.CSC_SET_ID = J.CSC_SET_ID 
        _sb_chips.AppendLine("         GROUP BY B.CH_ISO_CODE, B.CH_CHIP_TYPE, B.CH_DENOMINATION ")     '  , J.CSC_SET_ID
        _sb_chips.AppendLine("          ) )")
      Else
        If Me.OperationType <> Cage.CageOperationType.CountCage Then
          _sb_chips.AppendLine("   AND   CHS_ALLOWED = 1 ")
          _sb_chips.AppendLine("   AND   CH_ALLOWED = 1 ")
        End If
      End If

      If _show_denominations = Cage.ShowDenominationsMode.ShowOnlyChipsDenominations Then
        If Me.OperationType = Cage.CageOperationType.CountCage Then
          _sb.AppendLine("  AND   CGC_DENOMINATION IN (" & Cage.COINS_CODE & "," & Cage.BANK_CARD_CODE & "," & Cage.CHECK_CODE & ")")
          _sb.AppendLine("  OR CGC_DENOMINATION = " & Cage.TICKETS_CODE & " AND CGC_ISO_CODE = '" & GeneralParam.GetString("RegionalOptions", "CurrencyISOCode", "MXN") & "'")
        Else
          _sb.AppendLine("  AND   CGC_DENOMINATION = " & Cage.COINS_CODE)
        End If
      End If

      If _show_denominations <> Cage.ShowDenominationsMode.ShowAllDenominations AndAlso _
         (Me.OperationType = Cage.CageOperationType.FromCashier Or Me.OperationType = Cage.CageOperationType.FromCashierClosing) Then
        _sb.AppendLine("   OR ( CGC_ISO_CODE = '" & GeneralParam.GetString("RegionalOptions", "CurrencyISOCode", "MXN") & "'")
        _sb.AppendLine("  AND   CGC_DENOMINATION < 0")
        _sb.AppendLine(String.Format("  AND   CGC_DENOMINATION <> {0} )", Cage.CREDIT_LINES))
      End If
    End If

    _sb_chips.AppendLine(" ORDER BY N ASC, ISO_CODE ASC, TYPE ASC, CHIP_SET_ID ASC, DENOMINATION DESC, CHIP_ID ASC")

    Try
      _sb.AppendLine(" UNION ")
      _sb.AppendLine(_sb_chips.ToString())
      _aux_datatable = GUI_GetTableUsingSQL(_sb.ToString, Integer.MaxValue)
      _aux_datatable.Columns.Remove("N")

      _aux_datatable.Columns.Add(DT_COLUMN_CURRENCY.DENOMINATION_WITH_SIMBOL.ToString, GetType(String))
      _aux_datatable.Columns.Add(DT_COLUMN_CURRENCY.QUANTITY.ToString, GetType(Int32))
      _aux_datatable.Columns.Add(DT_COLUMN_CURRENCY.TOTAL.ToString, GetType(Decimal))
      _aux_datatable.Columns.Add(DT_COLUMN_CURRENCY.TOTAL_WITH_SIMBOL.ToString, GetType(String))
      _aux_datatable.Columns.Add(DT_COLUMN_CURRENCY.CASHIER_QUANTITY.ToString, GetType(Int32))
      _aux_datatable.Columns.Add(DT_COLUMN_CURRENCY.CASHIER_TOTAL.ToString, GetType(Decimal))
      _aux_datatable.Columns.Add(DT_COLUMN_CURRENCY.QUANTITY_DIFFERENCE.ToString, GetType(String))
      _aux_datatable.Columns.Add(DT_COLUMN_CURRENCY.TOTAL_DIFFERENCE.ToString, GetType(Decimal))

      For Each _row In _aux_datatable.Rows
        _row(DT_COLUMN_CURRENCY.DENOMINATION_WITH_SIMBOL) = _row(DT_COLUMN_CURRENCY.DENOMINATION)

        ' Data configuration for General Param Cage.ShowDenominations
        If _show_denominations = Cage.ShowDenominationsMode.HideAllDenominations Then

          If FeatureChips.IsCurrencyExchangeTypeChips(_row(DT_COLUMN_CURRENCY.CURRENCY_TYPE)) Then
            _row(DT_COLUMN_CURRENCY.DENOMINATION) = 1D

            If _row(DT_COLUMN_CURRENCY.DENOMINATION) = 1 Then
              _row(DT_COLUMN_CURRENCY.DENOMINATION_WITH_SIMBOL) = _str_monto.PadLeft(_str_monto.Length + m_white_spaces) ' Monto
              m_white_spaces += 1

            ElseIf _row(DT_COLUMN_CURRENCY.DENOMINATION) < 0 Then
              _row(DT_COLUMN_CURRENCY.DENOMINATION_WITH_SIMBOL) = _
                  Me.GetCurrencyDescription(_row(DT_COLUMN_CURRENCY.ISO_CODE), _row(DT_COLUMN_CURRENCY.DENOMINATION))
            End If

          ElseIf _row(DT_COLUMN_CURRENCY.DENOMINATION) < 0 Then

            If _row(DT_COLUMN_CURRENCY.DENOMINATION) = Cage.COINS_CODE Then
              _row(DT_COLUMN_CURRENCY.DENOMINATION_WITH_SIMBOL) = _str_monto.PadLeft(_str_monto.Length + m_white_spaces) ' Monto
              m_white_spaces += 1
            Else
              _row(DT_COLUMN_CURRENCY.DENOMINATION_WITH_SIMBOL) = _
                                Me.GetCurrencyDescription(_row(DT_COLUMN_CURRENCY.ISO_CODE), _row(DT_COLUMN_CURRENCY.DENOMINATION))
            End If

          End If

        ElseIf _show_denominations = Cage.ShowDenominationsMode.ShowOnlyChipsDenominations Then

          If Not FeatureChips.IsCurrencyExchangeTypeChips(_row(DT_COLUMN_CURRENCY.CURRENCY_TYPE)) _
            AndAlso _row(DT_COLUMN_CURRENCY.DENOMINATION) = Cage.COINS_CODE Then
            _row(DT_COLUMN_CURRENCY.DENOMINATION_WITH_SIMBOL) = _str_monto.PadLeft(_str_monto.Length + m_white_spaces) ' Monto
            m_white_spaces += 1
          Else
            If _row(DT_COLUMN_CURRENCY.DENOMINATION) < 0 Then
              _row(DT_COLUMN_CURRENCY.DENOMINATION_WITH_SIMBOL) = _
                  Me.GetCurrencyDescription(_row(DT_COLUMN_CURRENCY.ISO_CODE), _row(DT_COLUMN_CURRENCY.DENOMINATION))
            End If
          End If
        Else ' Show all denominations
          If Not _row(DT_COLUMN_CURRENCY.DENOMINATION) Is DBNull.Value AndAlso _row(DT_COLUMN_CURRENCY.DENOMINATION) < 0 Then
            _row(DT_COLUMN_CURRENCY.DENOMINATION_WITH_SIMBOL) = _
                             Me.GetCurrencyDescription(_row(DT_COLUMN_CURRENCY.ISO_CODE), _row(DT_COLUMN_CURRENCY.DENOMINATION))
          End If
        End If
        'End of Data configuration for General Param Cage.ShowDenominations

        If Not IsNew Then
          _row(DT_COLUMN_CURRENCY.QUANTITY) = 0
        End If

        _row(DT_COLUMN_CURRENCY.TOTAL) = 0
        _row(DT_COLUMN_CURRENCY.TOTAL_WITH_SIMBOL) = _row(DT_COLUMN_CURRENCY.TOTAL)
        _row(DT_COLUMN_CURRENCY.QUANTITY_DIFFERENCE) = 0
        _row(DT_COLUMN_CURRENCY.CASHIER_QUANTITY) = 0
        _row(DT_COLUMN_CURRENCY.CASHIER_TOTAL) = 0
        _row(DT_COLUMN_CURRENCY.QUANTITY_DIFFERENCE) = 0
        _row(DT_COLUMN_CURRENCY.TOTAL_DIFFERENCE) = 0
      Next

      If UseTableCurrencies Then
        Me.TableCurrencies = New DataTable
        Me.TableCurrencies = _aux_datatable.Copy
      Else
        Me.TableTheoricValues = New DataTable
        Me.TableTheoricValues = _aux_datatable.Copy
      End If

      Call Me.AddExpectedColumnInCurrencies()

    Catch ex As Exception
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "cls_cage_control", _
                            "InitCurrencyData", _
                            ex.Message)
    End Try

  End Sub ' InitCurrencyData

  '----------------------------------------------------------------------------
  ' PURPOSE: Checks if there is a pending movement created by ID
  '
  ' PARAMS:
  '       - Movement ID
  '
  ' RETURNS:
  '     - True if it doesn't exists a pending movement.
  '
  ' NOTES:
  Public Function HasPendingMovementById(ByVal MovementId As Long, ByRef SqlTrx As SqlTransaction) As Boolean
    Dim _sb As StringBuilder
    Dim _has_pending_movement As Boolean
    Dim _obj As Object

    _has_pending_movement = True
    _sb = New StringBuilder

    _sb.AppendLine(" SELECT   CPM_MOVEMENT_ID ")
    _sb.AppendLine("   FROM   CAGE_PENDING_MOVEMENTS ")
    _sb.AppendLine("  WHERE   CPM_MOVEMENT_ID = @pMovementId ")

    Try

      Using _cmd As New SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx)

        _cmd.Parameters.Add("@pMovementId", SqlDbType.BigInt).Value = MovementId
        _obj = _cmd.ExecuteScalar()

        If _obj Is Nothing OrElse _obj Is DBNull.Value Then
          _has_pending_movement = False
        End If

      End Using

    Catch ex As Exception
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "cls_cage_control", _
                            "HasPendingMovementById", _
                            ex.Message)

      Return True
    End Try

    Return _has_pending_movement
  End Function ' HasPendingMovementById

  '----------------------------------------------------------------------------
  ' PURPOSE: Checks if there is a pending movement created for an user ID
  '
  ' PARAMS:
  '       - UserId
  '
  ' RETURNS:
  '     - True if it doesn't exists a pending movement.
  '
  ' NOTES:
  Public Function CheckPendingMovement(ByVal UserId As Long, Optional ByVal WorkingDay As DateTime = Nothing) As Boolean
    Dim _sb As StringBuilder
    Dim _has_pending_movement As Boolean
    Dim _obj As Object

    _has_pending_movement = False
    _sb = New StringBuilder

    _sb.AppendLine(" SELECT   CPM_MOVEMENT_ID ")
    _sb.AppendLine("   FROM   CAGE_PENDING_MOVEMENTS ")

    If WorkingDay <> Nothing Then
      _sb.AppendLine("INNER   JOIN CAGE_MOVEMENTS ON CGM_MOVEMENT_ID     = CPM_MOVEMENT_ID ")
      _sb.AppendLine("INNER   JOIN CAGE_SESSIONS  ON CGM_CAGE_SESSION_ID = CGS_CAGE_SESSION_ID ")
    End If

    _sb.AppendLine("  WHERE   CPM_USER_ID = @pUserId ")

    If WorkingDay <> Nothing Then
      _sb.AppendLine("AND   CGS_WORKING_DAY = " + GUI_FormatDateDB(WorkingDay))
    End If

    Try

      Using _db_trx As New DB_TRX()
        Using _cmd As New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = UserId
          _obj = _cmd.ExecuteScalar()

          If _obj IsNot Nothing AndAlso _obj IsNot DBNull.Value Then
            _has_pending_movement = True
          End If

        End Using
      End Using

    Catch ex As Exception
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "cls_cage_control", _
                            "CheckPendingMovement", _
                            ex.Message)

      _has_pending_movement = True
    End Try

    Return _has_pending_movement
  End Function ' CheckPendingMovement

  '----------------------------------------------------------------------------
  ' PURPOSE: Checks if the movement still has the same status
  '
  ' PARAMS:
  '       - MovID
  '       - Status
  '
  ' RETURNS:
  '       - True if it has the same status.
  '
  ' NOTES:
  Public Function CheckIfSameStatus(ByVal MovID As Long, ByVal Status As Cage.CageStatus) As Boolean
    Dim _sb As StringBuilder
    Dim _obj As Object
    Dim _same_status As Boolean

    _same_status = False

    _sb = New StringBuilder

    _sb.AppendLine("SELECT    CGM_STATUS ")
    _sb.AppendLine("  FROM    CAGE_MOVEMENTS ")
    _sb.AppendLine(" WHERE    CGM_MOVEMENT_ID = @pMovementId ")
    _sb.AppendLine("   AND    CGM_STATUS = @pStatus")

    Try

      Using _db_trx As New DB_TRX()
        Using _cmd As New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _cmd.Parameters.Add("@pMovementId", SqlDbType.BigInt).Value = MovID
          _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = Status

          _obj = _cmd.ExecuteScalar()

          If _obj IsNot Nothing AndAlso _obj IsNot DBNull.Value Then
            _same_status = True
          End If

        End Using
      End Using

    Catch ex As Exception
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "cls_cage_control", _
                            "CheckIfSameStatus", _
                            ex.Message)

      _same_status = False
    End Try

    Return _same_status
  End Function ' CheckIfSameStatus

  '----------------------------------------------------------------------------
  ' PURPOSE: Gets the currency description by iso code
  '
  ' PARAMS:
  '       - ISOCode
  '       - Type
  '
  ' RETURNS:
  '       - the currency description
  '
  ' NOTES:
  Public Function GetCurrencyDescription(ByVal ISOCode As String, ByVal Type As Integer, Optional currencyType As Integer = 0) As String
    Dim _sb As StringBuilder
    Dim _currency_description As String
    Dim _show_denominations As Cage.ShowDenominationsMode

    If Me.OperationType = Cage.CageOperationType.FromTerminal Then
      _show_denominations = Cage.ShowDenominationsMode.ShowAllDenominations
    Else
      _show_denominations = GeneralParam.GetInt32("Cage", "ShowDenominations", Cage.ShowDenominationsMode.ShowAllDenominations)
    End If

    _currency_description = String.Empty

    If Type = Cage.COINS_CODE Then
      If _show_denominations = Cage.ShowDenominationsMode.HideAllDenominations Then
        _currency_description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3372) ' Monto
      Else
        _currency_description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2952) ' Resto
      End If

      If FeatureChips.IsCurrencyExchangeTypeChips(Type) Then
        _currency_description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3401) ' Propinas
      End If

    ElseIf Type = Cage.TICKETS_CODE Then
      _currency_description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3365) ' Tickets TITO

    ElseIf Type = Cage.COINS_COLLECTION_CODE AndAlso currencyType = CageCurrencyType.Others Then
      _currency_description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5558) ' Monedas 

    ElseIf Type = Cage.COINS_COLLECTION_CODE AndAlso currencyType = CageCurrencyType.Bill Then
      _currency_description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2952) ' Resto 

    Else
      _currency_description = CurrencyExchange.GetDescription((Type * -1), ISOCode, String.Empty)

      If String.IsNullOrEmpty(_currency_description) Then
        _sb = New StringBuilder()

        _sb.AppendLine("SELECT    ISNULL(CE_DESCRIPTION,'') AS CE_DESCRIPTION ")
        _sb.AppendLine("  FROM    CURRENCY_EXCHANGE ")
        _sb.AppendLine(" WHERE    CE_CURRENCY_ISO_CODE = @pISOCode ")
        _sb.AppendLine("   AND    CE_TYPE = @pType")

        Try

          Using _db_trx As New DB_TRX()
            Using _cmd As New SqlClient.SqlCommand(_sb.ToString())

              _cmd.Parameters.Add("@pISOCode", SqlDbType.NVarChar).Value = ISOCode
              _cmd.Parameters.Add("@pType", SqlDbType.Int).Value = Type * -1

              Using _sql_reader As SqlDataReader = _db_trx.ExecuteReader(_cmd)

                If _sql_reader.Read() Then
                  _currency_description = _sql_reader.GetString(0)
                Else
                  _currency_description = String.Empty
                End If

              End Using
            End Using
          End Using

        Catch ex As Exception
          _currency_description = String.Empty
        End Try

      End If

    End If

    Return _currency_description
  End Function ' GetCurrencyDescription

  '----------------------------------------------------------------------------
  ' PURPOSE: To fill combo custom
  '
  ' PARAMS:
  '       - IsSend
  '       - Combo
  '       - ShowEmptyElement
  '
  ' RETURNS:
  '       - None
  '
  ' NOTES:
  Public Sub SetComboCustom(ByVal IsSend As Boolean, ByRef Combo As uc_combo, Optional ByVal ShowEmptyElement As Boolean = False)
    Dim _table As DataTable
    Dim _sb As StringBuilder

    _sb = New StringBuilder

    _sb.AppendLine("SELECT   CST_SOURCE_TARGET_ID ")
    _sb.AppendLine("       , CST_SOURCE_TARGET_NAME ")
    _sb.AppendLine("  FROM   CAGE_SOURCE_TARGET ")

    If IsSend Then
      _sb.AppendLine(" WHERE   CST_TARGET = 1")
    Else
      _sb.AppendLine(" WHERE   CST_SOURCE = 1")
    End If
    _sb.AppendLine(" AND   CST_SOURCE_TARGET_ID > 999")

    _sb.AppendLine(" ORDER BY   CST_SOURCE_TARGET_NAME ")

    Try

      _table = GUI_GetTableUsingSQL(_sb.ToString(), Integer.MaxValue)

    Catch ex As Exception
      _table = New DataTable
    End Try

    Call Combo.Clear()

    If ShowEmptyElement Then
      Combo.Add(-1, String.Empty)
    End If

    Call Combo.Add(_table)
    Combo.TextValue = " "

  End Sub ' SetComboCustom

  '----------------------------------------------------------------------------
  ' PURPOSE: To get the custom user name
  '
  ' PARAMS:
  '       - UserId
  '
  ' RETURNS:
  '      - GetCustomUserName
  '
  ' NOTES:
  Public Function GetCustomUserName(ByVal UserId As Long) As String
    Dim _sb As StringBuilder
    Dim _custom_user_name As String
    Dim _obj As Object

    _custom_user_name = AUDIT_NONE_STRING
    _sb = New StringBuilder

    _sb.AppendLine("SELECT   CST_SOURCE_TARGET_NAME ")
    _sb.AppendLine("  FROM   CAGE_SOURCE_TARGET ")
    _sb.AppendLine(" WHERE   CST_SOURCE_TARGET_ID = " & UserId)

    Try

      Using _db_trx As New DB_TRX()
        Using _cmd As New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _obj = _cmd.ExecuteScalar()

          If _obj IsNot Nothing AndAlso _obj IsNot DBNull.Value Then
            _custom_user_name = _obj
          End If

        End Using
      End Using

    Catch ex As Exception
      _custom_user_name = AUDIT_NONE_STRING
    End Try

    Return _custom_user_name
  End Function ' GetCustomUserName

  '----------------------------------------------------------------------------
  ' PURPOSE: Gets movement id for new request
  '
  ' PARAMS:
  '       - UserId
  '
  ' RETURNS:
  '      - GetCustomUserName
  '
  ' NOTES:
  Public Function GetNewRequest() As Int64
    Dim _sb As StringBuilder
    Dim _movement_id As String
    Dim _obj As Object

    _movement_id = 0
    _sb = New StringBuilder

    _sb.AppendLine("SELECT   CGM_MOVEMENT_ID                  ")
    _sb.AppendLine("  FROM   CAGE_MOVEMENTS                   ")
    _sb.AppendLine(" WHERE   CGM_TYPE            = " & Cage.CageOperationType.RequestOperation)
    _sb.AppendLine("   AND   CGM_USER_CASHIER_ID = " & Me.UserCashierID)
    _sb.AppendLine("   AND   CGM_STATUS          = " & Cage.CageStatus.Sent)

    Try

      Using _db_trx As New DB_TRX()
        Using _cmd As New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _obj = _cmd.ExecuteScalar()

          If _obj IsNot Nothing AndAlso _obj IsNot DBNull.Value Then
            _movement_id = _obj
          End If

        End Using
      End Using

    Catch ex As Exception
      _movement_id = 0
    End Try

    Return _movement_id
  End Function ' GetNewRequest

  ''' <summary>
  ''' Set the terminals list into combo
  ''' </summary>
  ''' <param name="Combo"></param>
  ''' <param name="IsCollecting"></param>
  ''' <remarks></remarks>
  Public Sub SetComboTerminals(ByRef Combo As uc_combo, Optional ByVal IsCollecting As Boolean = False)

    Dim _sb As StringBuilder
    _sb = New StringBuilder

    If Me.IsCountREnabled Then
      'CountR
      _sb.Append(getSQLCountRComboTerminals())
    End If

    If Not Cage.IsCageAutoMode Then
      If Not IsCollecting Then
        If Me.IsCountREnabled Then
          _sb.AppendLine("                         UNION         ")
        End If

        _sb.Append(getSQLCashiersComboTerminals())
      End If
    End If

    If _sb.Length > 0 Then
      _sb.AppendLine("                         ORDER   BY CT_NAME ")

      Call SetCombo(Combo, _sb.ToString, True)
    Else
      'If no sql delete items
      Combo.Clear()
    End If

    If _sb.Length = 0 Then
      Call SetComboCashierAlias(Combo, False, True, _sb.ToString)
    End If

  End Sub ' SetComboTerminals

  ' PURPOSE: Apply union with cashier terminals
  '
  ' PARAMS:
  '       - None
  '
  ' RETURNS:
  '       - None
  '
  ' NOTES:
  Public Function GetUnionWithCashierTerminals(Optional ByVal IsCollecting As Boolean = False) As String

    Dim _sb As StringBuilder
    _sb = New StringBuilder

    ' DDS 05-APR-2016
    If Not Cage.IsCageAutoMode Or IsCollecting Then
      _sb.AppendLine("UNION                                                                                   ")
    End If
    _sb.AppendLine("     SELECT   DISTINCT(CT_CASHIER_ID)                                                   ") ' CountR's dados de alta en Cashier_Terminals
    _sb.AppendLine("            , CT_NAME	+ '" + Me.m_countr_sufix + "' AS CT_NAME                          ")
    _sb.AppendLine("       FROM   CASHIER_TERMINALS CT				      	                                      ")
    _sb.AppendLine("       INNER JOIN COUNTR CR ON CR.CR_COUNTR_ID = CT_COUNTR_ID AND CR.CR_ENABLED = 1     ")
    _sb.AppendLine("       WHERE CT.CT_COUNTR_ID IS NOT NULL                                                ")
    _sb.AppendLine("UNION                                                                                   ")
    _sb.AppendLine("     SELECT   DISTINCT(CR_COUNTR_ID) + " + String.Format("{0:D}", COUNTR_ID_OFFSET) + " ") ' CountR's NO dados de alta en Cashier_Terminals . Sumar 100.000 al ID
    _sb.AppendLine("            , CR_NAME	+ '" + Me.m_countr_sufix + "' AS CT_NAME                          ")
    _sb.AppendLine("       FROM   COUNTR CR            			      	                                        ")
    _sb.AppendLine("       LEFT OUTER JOIN CASHIER_TERMINALS CT ON CR.CR_COUNTR_ID = CT_COUNTR_ID           ")
    _sb.AppendLine("       WHERE CR.CR_ENABLED = 1 AND CT.CT_COUNTR_ID IS NULL                              ")

    Return _sb.ToString()

  End Function

  ' PURPOSE: To set the gaming table combo box
  '
  ' PARAMS:
  '       - Combo
  '
  ' RETURNS:
  '       - None
  '
  ' NOTES:
  Public Sub SetComboGamingTable(ByRef Combo As uc_combo _
                               , ByVal VirtualCashier As Boolean _
                               , Optional ByVal ShowEmptyElement As Boolean = False _
                               , Optional ByVal LoadOnlyVirtual As Boolean = False)

    Dim _sb As StringBuilder
    Dim _table As DataTable

    _sb = New StringBuilder

    _sb.AppendLine("   SELECT   GT_GAMING_TABLE_ID ")
    _sb.AppendLine("          , GT_NAME + ' - [' + GT_CODE + ']' AS NAME ")
    _sb.AppendLine("     FROM   GAMING_TABLES ")

    If VirtualCashier Then
      _sb.AppendLine(" INNER JOIN   CASHIER_SESSIONS ON GT_CASHIER_ID = CS_CASHIER_ID ")
    End If

    _sb.AppendLine("    WHERE   GT_ENABLED = 1 ")
    _sb.AppendLine("      AND   GT_CASHIER_ID IS NOT NULL ")

    ' For GP: Cage.AutoMode = True
    If LoadOnlyVirtual Then
      _sb.AppendLine("      AND   GT_HAS_INTEGRATED_CASHIER = 0")
    End If

    If VirtualCashier Then
      _sb.AppendLine("      AND   GT_HAS_INTEGRATED_CASHIER = 0")
      _sb.AppendLine("      AND   CS_STATUS = 0  ")
    End If

    _sb.AppendLine(" ORDER BY   GT_NAME ASC ")

    Try

      _table = GUI_GetTableUsingSQL(_sb.ToString(), Integer.MaxValue)

    Catch ex As Exception
      _table = New DataTable
    End Try

    If _table.Rows.Count() <> Combo.Count() Then

      Call Combo.Clear()

      If ShowEmptyElement Then
        Combo.Add(-1, String.Empty)
      End If

      Call Combo.Add(_table)
    End If
  End Sub ' SetComboGamingTable

  ' PURPOSE: To get the integrated cashier ID of a gaming table
  '
  ' PARAMS:
  '       - GamingTableId
  '
  ' RETURNS:
  '       - None
  '
  ' NOTES:
  Public Function GetIntegratedCashierGamingTableId(ByVal GamingTableId As Integer) As Integer
    Dim _sb As StringBuilder
    Dim _integrated_cashier As Integer
    Dim _obj As Object

    _integrated_cashier = -1 ' default value
    _sb = New StringBuilder

    _sb.AppendLine(" SELECT   GT_CASHIER_ID ")
    _sb.AppendLine("   FROM   GAMING_TABLES ")
    _sb.AppendLine("  WHERE   GT_GAMING_TABLE_ID = " & GamingTableId)

    Try

      Using _db_trx As New DB_TRX()
        Using _cmd As New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _obj = _cmd.ExecuteScalar()

          If _obj IsNot Nothing AndAlso _obj IsNot DBNull.Value Then
            _integrated_cashier = _obj
          End If

        End Using
      End Using

    Catch ex As Exception
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "cls_cage_control", _
                            "GetIntegratedCashierGamingTableId", _
                            ex.Message)

      Return -1
    End Try

    Return _integrated_cashier
  End Function ' GetIntegratedCashierGamingTableId

  ' PURPOSE: gets if the gambling table has an integrated cashier
  '
  ' PARAMS:
  '       - GamingTableId
  '
  ' RETURNS:
  '       - HasIntegratedCashier
  '
  ' NOTES:
  Public Function HasIntegratedCashier(ByVal GamingTableId As Integer, ByRef HasIntCashier As Boolean) As Boolean
    Dim _sb As StringBuilder
    Dim _operation_ok As Boolean
    Dim _obj As Object

    HasIntCashier = False
    _operation_ok = True
    _sb = New StringBuilder

    _sb.AppendLine(" SELECT   GT_HAS_INTEGRATED_CASHIER ")
    _sb.AppendLine("   FROM   GAMING_TABLES ")
    _sb.AppendLine("  WHERE   GT_GAMING_TABLE_ID = " & GamingTableId)

    Try

      Using _db_trx As New DB_TRX()
        Using _cmd As New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _obj = _cmd.ExecuteScalar()

          If _obj IsNot Nothing AndAlso _obj IsNot DBNull.Value Then
            HasIntCashier = _obj
          End If

        End Using
      End Using

    Catch ex As Exception

      _operation_ok = False
    End Try

    Return _operation_ok
  End Function ' HasIntegratedCashier

  ' PURPOSE: Checks if  custom source/target Still exists 
  '
  ' PARAMS:
  '       - CustomUserId
  '
  ' RETURNS:
  '       - True if still exist
  '
  ' NOTES:
  Public Function CheckIfStillExistCustom(ByVal CustomUserId As Long) As Boolean
    Dim _sb As StringBuilder
    Dim _still_exist As Boolean
    Dim _obj As Object

    _still_exist = False
    _sb = New StringBuilder

    _sb.AppendLine(" SELECT   TOP 1 CST_SOURCE_TARGET_ID ")
    _sb.AppendLine("   FROM   CAGE_SOURCE_TARGET ")
    _sb.AppendLine("  WHERE   CST_SOURCE_TARGET_ID = @pCustomId ")

    Try

      Using _db_trx As New DB_TRX()
        Using _cmd As New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _cmd.Parameters.Add("@pCustomId", SqlDbType.BigInt).Value = CustomUserId

          _obj = _cmd.ExecuteScalar()

          If _obj IsNot Nothing AndAlso _obj IsNot DBNull.Value Then
            _still_exist = True
          End If

        End Using
      End Using

    Catch ex As Exception

      Return False
    End Try

    Return _still_exist
  End Function ' CheckIfStillExistCustom

  ' PURPOSE: To set the cage sessions combo box
  '
  ' PARAMS:
  '       - Combo
  '       - ShowEmptyElement
  '
  ' RETURNS:
  '       - None
  '
  ' NOTES:
  Public Sub SetComboCageSessions(ByRef Combo As uc_combo, Optional ByVal ShowEmptyElement As Boolean = False)
    Dim _sb As StringBuilder
    Dim _table As DataTable

    _sb = New StringBuilder

    _sb.AppendLine("    SELECT   CGS_CAGE_SESSION_ID  ")
    _sb.AppendLine("           , CGS_SESSION_NAME ")
    _sb.AppendLine("      FROM   CAGE_SESSIONS ")
    _sb.AppendLine("INNER JOIN   GUI_USERS ")
    _sb.AppendLine("        ON   CGS_OPEN_USER_ID = GU_USER_ID ")
    _sb.AppendLine("     WHERE   CGS_CLOSE_DATETIME IS NULL ")
    _sb.AppendLine("  ORDER BY   CGS_OPEN_DATETIME ASC ")

    Try

      _table = GUI_GetTableUsingSQL(_sb.ToString(), Integer.MaxValue)

    Catch ex As Exception
      _table = New DataTable
    End Try

    Call Combo.Clear()

    If ShowEmptyElement Then
      Combo.Add(-1, String.Empty)
    End If

    Call Combo.Add(_table)
    Combo.TextValue = String.Empty
    Combo.SelectedIndex = -1
  End Sub ' SetComboCageSessions

  ' PURPOSE: To set the cage source/target concepts relation
  '
  ' PARAMS:
  '       - Combo
  '       - ShowEmptyElement
  '
  ' RETURNS:
  '       - None
  '
  ' NOTES:
  Public Function GetSourceTargetConceptsData() As DataTable

    Dim _sb As StringBuilder
    Dim _table As DataTable

    _sb = New StringBuilder

    _sb.AppendLine(" SELECT  CSTC.CSTC_SOURCE_TARGET_ID ")
    _sb.AppendLine("       , CSTC.CSTC_CONCEPT_ID ")
    _sb.AppendLine(" 		   , CC.CC_DESCRIPTION ")
    _sb.AppendLine(" 		   , CSTC.CSTC_TYPE ")
    _sb.AppendLine(" 		   , CSTC.CSTC_ONLY_NATIONAL_CURRENCY ")

    _sb.AppendLine(" 	 FROM  CAGE_SOURCE_TARGET_CONCEPTS AS CSTC ")
    _sb.AppendLine(" 		     INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CSTC.CSTC_CONCEPT_ID ")

    _sb.AppendLine("  WHERE  CSTC.CSTC_ENABLED = 1 ")
    _sb.AppendLine("    AND  CC.CC_ENABLED = 1 ")
    _sb.AppendLine("    AND  (CC.CC_CONCEPT_ID >= 1000 OR CC.CC_CONCEPT_ID = 6)") ' Personalizado Or Propinas


    Try

      _table = GUI_GetTableUsingSQL(_sb.ToString(), Integer.MaxValue)

    Catch ex As Exception
      _table = New DataTable
    End Try

    Return _table

  End Function ' GetSourceTargetConceptsData

  ' PURPOSE: Custom Auditor Generator.
  '
  ' PARAMS:
  '   - INPUT: 
  '           - dtTemplate
  '           - IsNew
  '           - TemplateName
  '   - OUTPUT: None
  '
  ' NOTES:
  Public Sub AuditTemplate(ByVal dtTemplate As DataTable, ByVal IsNew As Boolean, ByVal TemplateName As String, ByVal ColumnType As String)
    Dim _status As frm_cage_control.TEMPLATE_CHANGES_STATUS
    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _old_auditor_data As CLASS_AUDITOR_DATA
    Dim _dt_templates() As DataRow
    Dim _operation_id As Integer
    Dim _str_audit_denom_name As String = ""
    Dim _str_audit_denomination As String = ""
    Dim _str_type_description As String

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_CAGE)
    _old_auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_CAGE)

    TemplateName = Chr(34) & TemplateName & Chr(34) 'quotes

    If IsNew Then
      _operation_id = CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.INSERT
      _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(5052), TemplateName)
      _dt_templates = dtTemplate.Select("NEW_VALUE IS NOT NULL", "ISO_CODE ASC, DENOMINATION ASC") 'hide not set values
    Else
      _operation_id = CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.UPDATE
      _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(5052), TemplateName)
      _dt_templates = dtTemplate.Select("", "ISO_CODE ASC, DENOMINATION ASC")
    End If

    For Each _dr As DataRow In _dt_templates

      _status = _dr("STATUS")

      'Audit Operation and Target Type
      Select Case _status
        Case frm_cage_control.TEMPLATE_CHANGES_STATUS.NONE
          If (_dr("ISO_CODE") = "OperationType") Then
            _auditor_data.SetField(0, GetNLSFromType(_dr("NEW_VALUE"), 1), GLB_NLS_GUI_PLAYER_TRACKING.GetString(3396))
            _old_auditor_data.SetField(0, GetNLSFromType(_dr("OLD_VALUE"), 1), GLB_NLS_GUI_PLAYER_TRACKING.GetString(3396))
          End If

          If (_dr("ISO_CODE") = "TargetType") Then
            _auditor_data.SetField(0, GetNLSFromType(_dr("NEW_VALUE"), 2), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1181))
            _old_auditor_data.SetField(0, GetNLSFromType(_dr("OLD_VALUE"), 2), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1181))
          End If
      End Select

      'Hide values when template is new
      If _dr("NEW_VALUE") > 0 OrElse _dr("OLD_VALUE") > 0 Then

        _str_type_description = String.Empty
        If _dr(ColumnType) IsNot DBNull.Value Then
          _str_type_description = " " & GetCurrencyTypeDescription(_dr(ColumnType))
        End If
        If (_dr("ISO_CODE") <> "OperationType") Then
          If FeatureChips.IsCageCurrencyTypeChips(_dr(ColumnType)) Then
            If _dr(ColumnType) = CageCurrencyType.ChipsColor Then
              _str_audit_denom_name = " " & _dr("CHIP_GROUP") & " " & _dr("DENOM_NAME") & " - " & _dr("CHIP_NAME") & " " & _dr("CHIP_DRAW") & " "
            Else
              _str_audit_denom_name = " " & _dr("CHIP_GROUP") & "(" & _dr("ISO_CODE") & ") " & _dr("DENOM_NAME") & " - " & _dr("CHIP_NAME") & " " & _dr("CHIP_DRAW") & " "
            End If
          Else
            _str_audit_denom_name = " " & _dr("ISO_CODE") & " - " & _dr("DENOM_NAME") & _str_type_description
          End If

          If FeatureChips.IsCageCurrencyTypeChips(_dr(ColumnType)) Then
            If _dr(ColumnType) = CageCurrencyType.ChipsColor Then
              _str_audit_denomination = " " & _dr("CHIP_GROUP") & " " & _dr("DENOM_NAME") & " - " & _dr("CHIP_NAME") & " " & _dr("CHIP_DRAW") & " "
            Else
              _str_audit_denomination = " " & _dr("CHIP_GROUP") & "(" & _dr("ISO_CODE") & ") " & _dr("DENOM_NAME") & " - " & _dr("CHIP_NAME") & " " & _dr("CHIP_DRAW") & " "
            End If
          Else
            _str_audit_denomination = " " & _dr("ISO_CODE") & " - " & GUI_FormatCurrency(_dr("DENOMINATION")) & _str_type_description
          End If
        End If

        Select Case _status
          'NEW
          Case frm_cage_control.TEMPLATE_CHANGES_STATUS.ADDED
            If _dr("DENOMINATION") < 0 Then
              _auditor_data.SetField(0, GUI_FormatCurrency(_dr("NEW_VALUE")), GLB_NLS_GUI_INVOICING.GetString(215) & _str_audit_denom_name)
              _old_auditor_data.SetField(0, AUDIT_NONE_STRING, GLB_NLS_GUI_INVOICING.GetString(215) & _str_audit_denom_name)
            Else
              _auditor_data.SetField(0, _dr("NEW_VALUE"), GLB_NLS_GUI_INVOICING.GetString(215) & _str_audit_denomination)
              _old_auditor_data.SetField(0, AUDIT_NONE_STRING, GLB_NLS_GUI_INVOICING.GetString(215) & _str_audit_denomination)
            End If

            'UPDATED
          Case frm_cage_control.TEMPLATE_CHANGES_STATUS.UPDATED
            If _dr("DENOMINATION") < 0 Then
              _old_auditor_data.SetField(0, GUI_FormatCurrency(_dr("OLD_VALUE")), GLB_NLS_GUI_SW_DOWNLOAD.GetString(220) & _str_audit_denom_name)
              _auditor_data.SetField(0, GUI_FormatCurrency(_dr("NEW_VALUE")), GLB_NLS_GUI_SW_DOWNLOAD.GetString(220) & _str_audit_denom_name)
            Else
              _old_auditor_data.SetField(0, _dr("OLD_VALUE"), GLB_NLS_GUI_SW_DOWNLOAD.GetString(220) & _str_audit_denomination)
              _auditor_data.SetField(0, _dr("NEW_VALUE"), GLB_NLS_GUI_SW_DOWNLOAD.GetString(220) & _str_audit_denomination)
            End If

            'DELETED
          Case frm_cage_control.TEMPLATE_CHANGES_STATUS.DELETED
            If _dr("DENOMINATION") < 0 Then
              _auditor_data.SetField(0, AUDIT_NONE_STRING, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4567) & _str_audit_denom_name)
              _old_auditor_data.SetField(0, String.Empty, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4567) & _str_audit_denom_name)
            Else
              _auditor_data.SetField(0, AUDIT_NONE_STRING, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4567) & _str_audit_denomination)
              _old_auditor_data.SetField(0, String.Empty, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4567) & _str_audit_denomination)
            End If

          Case Else

        End Select
      End If
    Next

    If Not _auditor_data.Notify(GLB_CurrentUser.GuiId, _
                    GLB_CurrentUser.Id, _
                    GLB_CurrentUser.Name, _
                    _operation_id, 0, _
                    _old_auditor_data) Then
    End If

  End Sub

  Public Shared Function GetCurrencyTypeDescription(ByVal Type As CageCurrencyType) As String
    Dim _return_value As String
    _return_value = Type.ToString()

    Select Case Type
      Case CageCurrencyType.Bill
        _return_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5675)
      Case CageCurrencyType.Coin
        _return_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5676)
      Case Else
        _return_value = String.Empty
    End Select

    Return _return_value
  End Function

  Public Function GetCurrencyTypeEnum(ByVal Type As String) As CageCurrencyType
    Dim _return_value As CageCurrencyType
    _return_value = CageCurrencyType.Bill

    Select Case Type
      Case GLB_NLS_GUI_PLAYER_TRACKING.GetString(5675)
        _return_value = CageCurrencyType.Bill
      Case GLB_NLS_GUI_PLAYER_TRACKING.GetString(5676)
        _return_value = CageCurrencyType.Coin
      Case Else
        _return_value = CageCurrencyType.Others
    End Select

    Return _return_value
  End Function

  Private Function GetNLSFromType(ByVal Id As Integer, ByVal Type As Integer) As String
    Dim _return_value As String

    _return_value = String.Empty

    Select Case Type
      Case 1 ' Operation type
        Select Case Id
          Case Cage.CageOperationType.ToCashier
            _return_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1818)
          Case Cage.CageOperationType.ToCustom
            _return_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3393)
          Case Cage.CageOperationType.ToGamingTable
            _return_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4474)
          Case Cage.CageOperationType.ToTerminal
            _return_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4473)
        End Select

      Case 2 ' Target Type
        Select Case Id
          Case Cage.PendingMovementType.ToCashierUser
            _return_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3449)
          Case Cage.PendingMovementType.ToCashierTerminal
            _return_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3450)
          Case Cage.PendingMovementType.ToGamingTable
            _return_value = AUDIT_NONE_STRING
        End Select

    End Select

    Return _return_value
  End Function

  Public Sub GetDenominationAcceptedByTerminals()
    Dim _sb As StringBuilder
    Dim _table As DataTable
    Dim _denomination As Decimal


    m_denominations_in_terminals = New List(Of Decimal)
    _sb = New StringBuilder()
    _table = New DataTable()

    _sb.AppendLine("SELECT   COLUMN_NAME                             ")
    _sb.AppendLine("  FROM   INFORMATION_SCHEMA.COLUMNS              ")
    _sb.AppendLine(" WHERE   TABLE_NAME = 'MONEY_COLLECTION_METERS'  ")

    Using _db_trx As New DB_TRX()
      Using _cmd As New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
        Using _sql_da As New SqlDataAdapter(_cmd)
          _db_trx.Fill(_sql_da, _table)
        End Using
      End Using
    End Using

    If _table.Rows.Count > 0 Then
      For Each _row As DataRow In _table.Rows
        Call GetDenominationFromColumnName(_row(0).ToString(), _denomination)
        If _denomination > 0 Then
          m_denominations_in_terminals.Add(_denomination)
        End If
      Next
    End If

  End Sub

  Public Function GetCountRIsoCode(ByVal Id As Integer) As String
    Dim _sb As StringBuilder

    _sb = New StringBuilder()
    Using _table As DataTable = New DataTable()

      _sb.AppendLine("       SELECT   CR_CURRENT_ISOCODE        ")
      _sb.AppendLine("       FROM     COUNTR                    ")

      If Id < COUNTR_ID_OFFSET Then ' Id is CASHIER_TERMINALS.CT_CASHIER_ID 
        _sb.AppendLine("     INNER JOIN CASHIER_TERMINALS ON CT_COUNTR_ID = CR_COUNTR_ID")
        _sb.AppendLine("     WHERE   CT_CASHIER_ID = @pId")
      Else ' Id is COUNTR.CR_COUNTR_ID 
        ' Nothing
        Id = Id - COUNTR_ID_OFFSET
        _sb.AppendLine("     WHERE   CR_COUNTR_ID = @pId")
      End If


      Using _db_trx As New DB_TRX()
        Using _cmd As New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          _cmd.Parameters.Add("@pId", SqlDbType.Int).Value = Id
          Using _sql_da As New SqlDataAdapter(_cmd)
            _db_trx.Fill(_sql_da, _table)
          End Using
        End Using
      End Using

      If _table.Rows.Count = 0 Then
        Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_UNEXPECTED_ERROR, _
                         "cls_cage_control", _
                         "GetCountRIsoCode", _
                         "CountR Terminal Not found")
        Return Nothing
      End If

      Return _table.Rows(0)(0).ToString()
    End Using
  End Function

  Private Function GetDenominationFromColumnName(ByVal Value As String, ByRef Denomination As Decimal) As Decimal

    Dim _substring As String

    _substring = ""

    For _idx As Int32 = 0 To Value.Length - 1
      If IsNumeric(Value.Chars(_idx)) Then
        _substring = _substring & Value.Chars(_idx)
      End If
    Next

    If _substring.Length > 0 Then
      Decimal.TryParse(_substring, Denomination)
    End If

  End Function

#Region "Cage movement template"

  ' PURPOSE: Insert movement into database
  '
  ' PARAMS:
  '   - INPUT: 
  '         - SqlTrx
  '   - OUTPUT: 
  '         - None
  '
  ' RETURNS:
  '     - True if everything went OK. False if there was an error
  '
  ' NOTES:
  Public Function InsertMovement(ByVal SqlTrx As SqlTransaction) As Boolean
    Dim _sb As StringBuilder
    Dim _param As SqlClient.SqlParameter
    Dim _operation_ok As Boolean

    _operation_ok = True

    _sb = New StringBuilder()
    _sb.AppendLine(" INSERT INTO   CAGE_MOVEMENTS ")
    _sb.AppendLine("              ( CGM_USER_CAGE_ID ")
    _sb.AppendLine("               ,CGM_USER_CASHIER_ID ")
    _sb.AppendLine("               ,CGM_TERMINAL_CASHIER_ID ")
    _sb.AppendLine("               ,CGM_TYPE ")
    _sb.AppendLine("               ,CGM_MOVEMENT_DATETIME ")
    _sb.AppendLine("               ,CGM_STATUS ")
    _sb.AppendLine("               ,CGM_CAGE_SESSION_ID ")
    _sb.AppendLine("               ,CGM_SOURCE_TARGET_ID ")

    If Me.RelatedMovementId <> 0 Then
      _sb.AppendLine("               ,CGM_RELATED_MOVEMENT_ID ")
    End If

    _sb.AppendLine("               ,CGM_TARGET_TYPE ")
    _sb.AppendLine("               ,CGM_GAMING_TABLE_ID ")
    _sb.AppendLine("               ,CGM_GAMING_TABLE_VISITS ")
    _sb.AppendLine("               ,CGM_CASHIER_SESSION_ID ")
    _sb.AppendLine("               ,CGM_MC_COLLECTION_ID ")
    _sb.AppendLine("              ) ")
    _sb.AppendLine("               VALUES (")
    _sb.AppendLine("                @pUserCage ")
    _sb.AppendLine("               ,@pUserCashier ")
    _sb.AppendLine("               ,@pTerminalCashier ")
    _sb.AppendLine("               ,@pOperationType ")
    _sb.AppendLine("               ,@pMovementDateTime ")
    _sb.AppendLine("               ,@pStatus ")
    _sb.AppendLine("               ,@pCageSessionId ")
    _sb.AppendLine("               ,@pSourceTargetId ")

    If Me.RelatedMovementId <> 0 Then
      _sb.AppendLine("               ,@pRelatedMovementId ")
    End If

    _sb.AppendLine("               ,@pTargetType ")
    _sb.AppendLine("               ,@pGamingTableId ")
    _sb.AppendLine("               ,@pGamingTableVisits ")
    _sb.AppendLine("               ,@pCashierSessionId ")
    _sb.AppendLine("               ,@pMcCollectionId ")
    _sb.AppendLine("              ) ")
    _sb.AppendLine("SET @pMovementId = SCOPE_IDENTITY()")

    Try

      Using _cmd As New SqlClient.SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx)
        _cmd.Parameters.Add("@pUserCage", SqlDbType.BigInt).Value = CurrentUser.Id

        If Me.OperationType = Cage.CageOperationType.ToCashier Then

          If Me.TargetType = Cage.PendingMovementType.ToCashierUser Or Me.TargetType = Cage.PendingMovementType.ToGamingTable Then ' To cashier user or to gaming table
            _cmd.Parameters.Add("@pUserCashier", SqlDbType.Int).Value = Me.UserCashierID
            _cmd.Parameters.Add("@pTerminalCashier", SqlDbType.BigInt).Value = DBNull.Value
          ElseIf Me.TargetType = Cage.PendingMovementType.ToCashierTerminal Then ' To cashier terminal
            _cmd.Parameters.Add("@pUserCashier", SqlDbType.Int).Value = DBNull.Value
            _cmd.Parameters.Add("@pTerminalCashier", SqlDbType.Int).Value = Me.TerminalCashierID
          End If

          _cmd.Parameters.Add("@pTargetType", SqlDbType.Int).Value = Me.TargetType
          _cmd.Parameters.Add("@pSourceTargetId", SqlDbType.BigInt).Value = DBNull.Value
          If Me.TargetType = Cage.PendingMovementType.ToGamingTable Then
            _cmd.Parameters.Add("@pGamingTableId", SqlDbType.Int).Value = Me.GamingTableId
          Else
            _cmd.Parameters.Add("@pGamingTableId", SqlDbType.Int).Value = DBNull.Value
          End If

          _cmd.Parameters.Add("@pGamingTableVisits", SqlDbType.Decimal).Value = DBNull.Value
          _cmd.Parameters.Add("@pMcCollectionId", SqlDbType.BigInt).Value = DBNull.Value

        ElseIf Me.OperationType = Cage.CageOperationType.ToCustom _
               Or Me.OperationType = Cage.CageOperationType.FromCustom Then ' Custom

          _cmd.Parameters.Add("@pUserCashier", SqlDbType.BigInt).Value = DBNull.Value
          _cmd.Parameters.Add("@pSourceTargetId", SqlDbType.BigInt).Value = Me.CustomUserId
          _cmd.Parameters.Add("@pTerminalCashier", SqlDbType.Int).Value = DBNull.Value
          _cmd.Parameters.Add("@pTargetType", SqlDbType.Int).Value = DBNull.Value
          _cmd.Parameters.Add("@pGamingTableId", SqlDbType.Int).Value = DBNull.Value
          _cmd.Parameters.Add("@pGamingTableVisits", SqlDbType.Decimal).Value = DBNull.Value
          _cmd.Parameters.Add("@pMcCollectionId", SqlDbType.BigInt).Value = DBNull.Value

        ElseIf Me.OperationType = Cage.CageOperationType.ToGamingTable Then ' To Gaming Table
          _cmd.Parameters.Add("@pUserCashier", SqlDbType.BigInt).Value = DBNull.Value
          _cmd.Parameters.Add("@pSourceTargetId", SqlDbType.BigInt).Value = DBNull.Value
          _cmd.Parameters.Add("@pTerminalCashier", SqlDbType.Int).Value = Me.TerminalCashierID
          _cmd.Parameters.Add("@pTargetType", SqlDbType.Int).Value = Me.TargetType
          _cmd.Parameters.Add("@pGamingTableId", SqlDbType.Int).Value = Me.GamingTableId
          _cmd.Parameters.Add("@pGamingTableVisits", SqlDbType.Decimal).Value = DBNull.Value
          _cmd.Parameters.Add("@pMcCollectionId", SqlDbType.BigInt).Value = DBNull.Value

        ElseIf Me.OperationType = Cage.CageOperationType.FromGamingTable _
          Or Me.OperationType = Cage.CageOperationType.FromGamingTableClosing Then ' From Gaming Table
          _cmd.Parameters.Add("@pUserCashier", SqlDbType.BigInt).Value = DBNull.Value
          _cmd.Parameters.Add("@pSourceTargetId", SqlDbType.BigInt).Value = DBNull.Value
          _cmd.Parameters.Add("@pTerminalCashier", SqlDbType.Int).Value = Me.TerminalCashierID
          _cmd.Parameters.Add("@pTargetType", SqlDbType.Int).Value = Me.TargetType
          _cmd.Parameters.Add("@pGamingTableId", SqlDbType.Int).Value = Me.GamingTableId
          If Me.ReceptionAndClose Then
            _cmd.Parameters.Add("@pGamingTableVisits", SqlDbType.Decimal).Value = Me.GamingTableVisits
          Else
            _cmd.Parameters.Add("@pGamingTableVisits", SqlDbType.Decimal).Value = DBNull.Value
          End If
          _cmd.Parameters.Add("@pMcCollectionId", SqlDbType.BigInt).Value = DBNull.Value

        ElseIf Me.OperationType = Cage.CageOperationType.FromTerminal Then ' From Money Collection
          _cmd.Parameters.Add("@pUserCashier", SqlDbType.BigInt).Value = Me.m_type_new_collection.user_id
          _cmd.Parameters.Add("@pSourceTargetId", SqlDbType.BigInt).Value = DBNull.Value
          If Not Me.IsPromoBoxOrAcceptor Then
            _cmd.Parameters.Add("@pTerminalCashier", SqlDbType.Int).Value = Me.m_type_new_collection.terminal_id
          Else
            _cmd.Parameters.Add("@pTerminalCashier", SqlDbType.Int).Value = Me.TerminalCashierID
          End If
          _cmd.Parameters.Add("@pTargetType", SqlDbType.Int).Value = DBNull.Value
          _cmd.Parameters.Add("@pGamingTableId", SqlDbType.Int).Value = DBNull.Value
          _cmd.Parameters.Add("@pGamingTableVisits", SqlDbType.Decimal).Value = DBNull.Value
          _cmd.Parameters.Add("@pMcCollectionId", SqlDbType.BigInt).Value = Me.m_type_new_collection.money_collection_id

        ElseIf Me.OperationType = Cage.CageOperationType.FromCashier _
          Or Me.OperationType = Cage.CageOperationType.FromCashierClosing Then
          _cmd.Parameters.Add("@pUserCashier", SqlDbType.Int).Value = DBNull.Value
          _cmd.Parameters.Add("@pTerminalCashier", SqlDbType.Int).Value = Me.TerminalCashierID
          _cmd.Parameters.Add("@pTargetType", SqlDbType.Int).Value = Me.TargetType
          _cmd.Parameters.Add("@pSourceTargetId", SqlDbType.BigInt).Value = DBNull.Value
          _cmd.Parameters.Add("@pGamingTableId", SqlDbType.Int).Value = DBNull.Value
          _cmd.Parameters.Add("@pGamingTableVisits", SqlDbType.Decimal).Value = DBNull.Value
          _cmd.Parameters.Add("@pMcCollectionId", SqlDbType.BigInt).Value = DBNull.Value

        Else
          _cmd.Parameters.Add("@pUserCashier", SqlDbType.BigInt).Value = DBNull.Value
          _cmd.Parameters.Add("@pSourceTargetId", SqlDbType.BigInt).Value = DBNull.Value
          _cmd.Parameters.Add("@pTerminalCashier", SqlDbType.Int).Value = DBNull.Value
          _cmd.Parameters.Add("@pTargetType", SqlDbType.Int).Value = DBNull.Value
          _cmd.Parameters.Add("@pGamingTableId", SqlDbType.Int).Value = DBNull.Value
          _cmd.Parameters.Add("@pGamingTableVisits", SqlDbType.Decimal).Value = DBNull.Value
          _cmd.Parameters.Add("@pMcCollectionId", SqlDbType.BigInt).Value = DBNull.Value
        End If

        _cmd.Parameters.Add("@pOperationType", SqlDbType.Int).Value = Me.OperationType
        Me.MovementDatetime = GUI_FormatDate(WGDB.Now(), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
        _cmd.Parameters.Add("@pMovementDateTime", SqlDbType.DateTime).Value = Me.MovementDatetime
        _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = Me.Status
        _cmd.Parameters.Add("@pCageSessionId", SqlDbType.BigInt).Value = Me.CageSessionId
        _cmd.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).Value = Me.CashierSessionId

        If Me.RelatedMovementId <> 0 Then
          _cmd.Parameters.Add("@pRelatedMovementId", SqlDbType.BigInt).Value = Me.RelatedMovementId
        End If

        _param = _cmd.Parameters.Add("@pMovementId", SqlDbType.BigInt)
        _param.Direction = ParameterDirection.Output

        If _cmd.ExecuteNonQuery() <> 1 Then
          _operation_ok = False
        Else
          Me.MovementID = _param.Value
        End If

      End Using

    Catch ex As Exception
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "cls_cage_control", _
                            "InsertMovement", _
                            ex.Message)

      Return False
    End Try

    Return _operation_ok
  End Function ' InsertMovement

  ' PURPOSE: Updates movement
  '
  ' PARAMS:
  '     - Status
  '     - Trx
  '
  ' RETURNS:
  '     - db result
  '
  ' NOTES:
  Public Function UpdateMovementStatus(ByVal Status As Cage.CageStatus, ByVal Trx As SqlTransaction) As Boolean
    Dim _sb As StringBuilder
    Dim _result As Boolean

    _result = True
    _sb = New StringBuilder

    _sb.AppendLine("UPDATE    CAGE_MOVEMENTS")
    _sb.AppendLine("   SET    CGM_STATUS = @pStatus")

    If Me.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.CANCEL_MOVEMENT Or _
      Me.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.CANCEL_MOVEMENT_FROM_GAMINGTABLES Then
      _sb.AppendLine("       , CGM_CANCELLATION_USER_ID = @pUserCageIdLastModification  ")
      _sb.AppendLine("       , CGM_CANCELLATION_DATETIME = @pDateTimeLastModification   ")
    End If

    _sb.AppendLine(" WHERE    CGM_MOVEMENT_ID = @pMovementId")

    Try
      Using _cmd As New SqlClient.SqlCommand(_sb.ToString(), Trx.Connection, Trx)
        _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = Status
        _cmd.Parameters.Add("@pMovementId", SqlDbType.BigInt).Value = Me.MovementID
        _cmd.Parameters.Add("@pUserCageIdLastModification", SqlDbType.BigInt).Value = GLB_CurrentUser.Id
        _cmd.Parameters.Add("@pDateTimeLastModification", SqlDbType.DateTime).Value = GUI_FormatDate(WGDB.Now(), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

        If _cmd.ExecuteNonQuery() <> 1 Then
          _result = False
        End If

      End Using

    Catch ex As Exception
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "cls_cage_control", _
                            "UpdateMovementStatus", _
                            ex.Message)

      _result = False
    End Try

    Return _result
  End Function ' UpdateMovementStatus

  ' PURPOSE: Updates movement
  '
  ' PARAMS:
  '
  ' RETURNS:
  '     - db result
  '
  ' NOTES:
  Public Function UpdateMovement(ByVal Status As Cage.CageStatus, ByVal SqlTrx As SqlTransaction _
                               , Optional ByVal UserCage As Boolean = False, Optional ByVal CageSession As Boolean = False) As Boolean
    Dim _sb As StringBuilder
    Dim _operation_ok As Boolean

    _operation_ok = True
    _sb = New StringBuilder

    _sb.AppendLine("UPDATE    CAGE_MOVEMENTS ")
    _sb.AppendLine("   SET    CGM_STATUS = @pStatus ")

    If UserCage Then
      _sb.AppendLine("         ,CGM_USER_CAGE_ID = @pUserCage ")
    End If

    If CageSession Then
      _sb.AppendLine("         ,CGM_CAGE_SESSION_ID = @pCageSession ")
    End If

    If Me.m_type_new_collection.money_collection_id > 0 Then
      _sb.AppendLine("               ,CGM_MC_COLLECTION_ID = @pMcCollectionId")
    End If

    If Status = Cage.CageStatus.Canceled Then
      _sb.AppendLine("        , CGM_CANCELLATION_USER_ID = @pCageIdLastModification        ")
      _sb.AppendLine("        , CGM_CANCELLATION_DATETIME = @pDatetimeLastModification     ")
    End If

    _sb.AppendLine(" WHERE    CGM_MOVEMENT_ID = @pMovementId ")

    Try

      Using _cmd As New SqlClient.SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx)

        _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = Status
        If UserCage Then
          _cmd.Parameters.Add("@pUserCage", SqlDbType.BigInt).Value = CurrentUser.Id
        End If
        If CageSession Then
          _cmd.Parameters.Add("@pCageSession", SqlDbType.BigInt).Value = Me.CageSessionId
        End If
        _cmd.Parameters.Add("@pMovementId", SqlDbType.BigInt).Value = Me.MovementID
        If Me.m_type_new_collection.money_collection_id > 0 Then
          _cmd.Parameters.Add("@pMcCollectionId", SqlDbType.BigInt).Value = Me.m_type_new_collection.money_collection_id
        End If
        If Status = Cage.CageStatus.Canceled Then
          _cmd.Parameters.Add("@pCageIdLastModification", SqlDbType.Int).Value = GLB_CurrentUser.Id
          _cmd.Parameters.Add("@pDatetimeLastModification", SqlDbType.DateTime).Value = GUI_FormatDate(WGDB.Now(), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
        End If

        If _cmd.ExecuteNonQuery() <> 1 Then
          _operation_ok = False
        End If

      End Using

    Catch ex As Exception
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "cls_cage_control", _
                            "UpdateMovement", _
                            ex.Message)

      Return False
    End Try

    Return _operation_ok
  End Function ' UpdateMovement

  '----------------------------------------------------------------------------
  ' PURPOSE: Gets gui_user fullname
  '
  ' PARAMS:
  '       - movement ID
  '
  ' RETURNS:
  '     - userfullname
  '
  ' NOTES:
  Public Function GetTerminalCashierName(ByVal TerminalCashierId As Long) As String
    Dim _sb As StringBuilder
    Dim _cashier_name As String
    Dim _obj As Object

    _cashier_name = AUDIT_NONE_STRING

    _sb = New StringBuilder

    _sb.AppendLine("SELECT    CT_NAME ")
    _sb.AppendLine("  FROM    CASHIER_TERMINALS ")
    _sb.AppendLine(" WHERE    CT_CASHIER_ID = @pTerminalCashierId ")

    Try

      Using _db_trx As New DB_TRX()
        Using _cmd As New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _cmd.Parameters.Add("@pTerminalCashierId", SqlDbType.NVarChar).Value = TerminalCashierId

          _obj = _cmd.ExecuteScalar()

          If _obj IsNot Nothing AndAlso _obj IsNot DBNull.Value Then
            _cashier_name = _obj
          End If

        End Using
      End Using

    Catch ex As Exception
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "cls_cage_control", _
                            "GetTerminalCashierName", _
                            ex.Message)

      Return AUDIT_NONE_STRING
    End Try

    Return _cashier_name
  End Function ' GetTerminalCashierName

  ' PURPOSE: To Write audit
  '
  ' PARAMS:
  '
  ' RETURNS:
  '     - db result
  '
  ' NOTES:
  Public Function WriteAudit(ByVal OriginalClass As CLASS_CAGE_CONTROL) As Boolean
    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _status As String
    Dim _row As DataRow
    Dim _iso_code As String
    Dim _denomination As String
    Dim _value As String
    Dim _expected_value As String
    Dim _dv As DataView
    Dim _user_name As String
    Dim _custom_user_name As String
    Dim _operation_name As String
    Dim _nls_id As Integer
    Dim _currency_type_desc As String

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_CAGE)
    _currency_type_desc = ""

    Select Case Me.OperationType
      Case Cage.CageOperationType.FromTerminal
        _nls_id = (4612)
        _operation_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3308)

      Case Cage.CageOperationType.CountCage
        _nls_id = (4352)
        _operation_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4352)

      Case Cage.CageOperationType.ToCashier
        _nls_id = (4376)
        _operation_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2890)

      Case Cage.CageOperationType.FromCashier
        _nls_id = (4377)
        _operation_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2889)

      Case Cage.CageOperationType.FromCashierClosing
        _nls_id = (7898)
        _operation_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7898)

      Case Cage.CageOperationType.ToCustom
        _nls_id = (3394)
        _operation_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3446)

      Case Cage.CageOperationType.FromCustom
        _nls_id = (3395)
        _operation_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3447)

      Case Cage.CageOperationType.ToGamingTable
        _nls_id = (4391)
        _operation_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4394)

      Case Cage.CageOperationType.FromGamingTable
        _nls_id = (4392)
        _operation_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4395)

      Case Cage.CageOperationType.FromGamingTableClosing
        _nls_id = (7899)
        _operation_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7900)

      Case Cage.CageOperationType.FromGamingTableDropbox, _
        Cage.CageOperationType.FromGamingTableDropboxWithExpected
        _nls_id = (7329)
        _operation_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7329)

      Case Cage.CageOperationType.RequestOperation
        _nls_id = (4785)
        _operation_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4785)

      Case Else
        _nls_id = (2876)
        _operation_name = ""

    End Select

    If Me.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.CANCEL_MOVEMENT _
      OrElse (Me.OperationType = Cage.CageOperationType.RequestOperation AndAlso Me.Status = Cage.CageStatus.Canceled) Then
      _auditor_data.SetName(GLB_NLS_GUI_CLASS_II.Id(2876), GLB_NLS_GUI_PLAYER_TRACKING.GetString(_nls_id) & " (" + GLB_NLS_GUI_PLAYER_TRACKING.GetString(2908) + ")")
    Else
      _auditor_data.SetName(GLB_NLS_GUI_CLASS_II.Id(2876), GLB_NLS_GUI_PLAYER_TRACKING.GetString(_nls_id))
    End If

    ' Status
    _status = AUDIT_NONE_STRING
    Select Case Me.Status
      Case Cage.CageStatus.Sent
        If Me.OpenMode = OPEN_MODE.RECOLECT_MOVEMENT OrElse Me.OpenMode = OPEN_MODE.RECOLECT_MASSIVE Then
          If Me.OperationType = Cage.CageOperationType.RequestOperation Then
            _status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4786)
          Else
            _status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3011)
          End If
        Else
          _status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2891)
        End If

      Case Cage.CageStatus.OK
        If Me.OperationType = Cage.CageOperationType.RequestOperation Then
          _status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4788)
        Else
          _status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3305)
        End If

      Case Cage.CageStatus.Canceled
        _status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1891)

      Case Cage.CageStatus.OkDenominationImbalance
        If Me.OperationType = Cage.CageOperationType.RequestOperation Then
          _status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4788)
        Else
          _status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3021)
        End If

      Case Cage.CageStatus.OkAmountImbalance
        If Me.OperationType = Cage.CageOperationType.RequestOperation Then
          _status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4788)
        Else
          _status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3022)
        End If


      Case Cage.CageStatus.ErrorTotalImbalance
        _status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3023)

      Case Cage.CageStatus.CanceledDueToImbalanceOrOther
        _status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2893)

      Case Cage.CageStatus.SentToCustom
        _status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3393)

      Case Cage.CageStatus.ReceptionFromCustom
        _status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3305)

      Case Cage.CageStatus.FinishedCountCage
        _status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4732)

    End Select

    ' User Name
    _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(262), _status)


    If Me.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.CANCEL_MOVEMENT Then
      _user_name = GetUserFullName(GLB_CurrentUser.Id)
      _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(4613), _user_name)
    End If

    If Me.UserCageID = AUDIT_NONE_STRING Then
      _user_name = Me.UserCageID
    Else
      _user_name = GetUserFullName(Me.UserCageID)
    End If
    _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2896), _user_name)

    If Me.OpenMode = OPEN_MODE.CANCEL_MOVEMENT Or Me.OpenMode = OPEN_MODE.CANCEL_MOVEMENT_FROM_PROMOBOX_OR_ACCEPTOR Then
      If Me.OperationType = Cage.CageOperationType.FromCashier Or Me.OperationType = Cage.CageOperationType.FromCashierClosing Then
        Me.UserCashierID = OriginalClass.UserCashierID

        If Me.UserCashierID = Nothing Then
          _user_name = AUDIT_NONE_STRING
        Else
          _user_name = GetUserFullName(Me.UserCashierID)
        End If
        _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2898), _user_name)
      End If
    End If

    Select Case Me.OperationType
      Case Cage.CageOperationType.ToCashier, _
           Cage.CageOperationType.FromCashier, _
           Cage.CageOperationType.FromCashierClosing
        If Me.TerminalCashierID <> 0 Then
          _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(3450), GetTerminalCashierName(Me.TerminalCashierID))
        Else
          If Me.UserCashierID = Nothing Then
            _user_name = AUDIT_NONE_STRING
          Else
            _user_name = GetUserFullName(Me.UserCashierID)
          End If
          _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2898), _user_name)
        End If

      Case Cage.CageOperationType.ToCustom, Cage.CageOperationType.FromCustom
        If Me.CustomUserId = Nothing Then
          _custom_user_name = AUDIT_NONE_STRING
        Else
          _custom_user_name = GetCustomUserName(Me.CustomUserId)
        End If
        _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(3393), _custom_user_name)

      Case Cage.CageOperationType.ToGamingTable, _
        Cage.CageOperationType.FromGamingTable, _
        Cage.CageOperationType.FromGamingTableClosing, _
        Cage.CageOperationType.FromGamingTableDropbox, _
        Cage.CageOperationType.FromGamingTableDropboxWithExpected
        _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(4396), GetGamingTableName())

    End Select

    _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(3306), Me.MovementDatetime)  ' Creation Datetime
    _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(3388), Cage.GetCageSessionName(Me.CageSessionId))  ' Cage session Id
    _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(627), _operation_name) ' Operation Name

    If Not Me.TableCurrencies Is Nothing AndAlso Me.TableCurrencies.Rows.Count > 0 Then
      _dv = Me.TableCurrencies.DefaultView
      _dv.Sort = "ISO_CODE ASC, TYPE ASC, DENOMINATION DESC"
      Me.TableCurrencies = _dv.ToTable()
      AddExpectedColumnInCurrencies()

      For Each _row In Me.TableCurrencies.Select("QUANTITY <> 0 OR (TOTAL <> 0 AND DENOMINATION = -1) OR (QUANTITY_DIFFERENCE <> '0')", "ISO_CODE ASC, TYPE ASC, DENOMINATION DESC")
        _iso_code = _row.Item(DT_COLUMN_CURRENCY.ISO_CODE)

        Select Case _row.Item(DT_COLUMN_CURRENCY.DENOMINATION)
          Case Cage.COINS_CODE
            _denomination = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2952)
            _value = GUI_FormatCurrency(Me.GetRowValue(_row.Item(DT_COLUMN_CURRENCY.TOTAL)), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
            _expected_value = GUI_FormatCurrency(Me.GetRowValue(_row.Item(DT_COLUMN_CURRENCY.CASHIER_TOTAL)) - Me.GetRowValue(_row.Item(DT_COLUMN_CURRENCY.TOTAL_DIFFERENCE)), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
            _nls_id = 1985

          Case Cage.TICKETS_CODE
            _denomination = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3081)
            _value = GUI_FormatNumber(Me.GetRowValue(_row.Item(DT_COLUMN_CURRENCY.QUANTITY)), ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
            _expected_value = GUI_FormatNumber(Me.GetRowValue(_row.Item(DT_COLUMN_CURRENCY.CASHIER_QUANTITY)) - Me.GetRowValue(_row.Item(DT_COLUMN_CURRENCY.QUANTITY_DIFFERENCE)), ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
            _nls_id = 3301

          Case Cage.BANK_CARD_CODE
            _denomination = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2558)
            _value = GUI_FormatCurrency(Me.GetRowValue(_row.Item(DT_COLUMN_CURRENCY.TOTAL)), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
            _expected_value = GUI_FormatCurrency(Me.GetRowValue(_row.Item(DT_COLUMN_CURRENCY.CASHIER_TOTAL)) - Me.GetRowValue(_row.Item(DT_COLUMN_CURRENCY.TOTAL_DIFFERENCE)), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
            _nls_id = 1985

          Case Cage.CHECK_CODE
            _denomination = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2594)
            _value = GUI_FormatCurrency(Me.GetRowValue(_row.Item(DT_COLUMN_CURRENCY.TOTAL)), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
            _expected_value = GUI_FormatCurrency(Me.GetRowValue(_row.Item(DT_COLUMN_CURRENCY.CASHIER_TOTAL)) - Me.GetRowValue(_row.Item(DT_COLUMN_CURRENCY.TOTAL_DIFFERENCE)), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
            _nls_id = 1985

          Case Else
            _denomination = GUI_FormatNumber(_row.Item(DT_COLUMN_CURRENCY.DENOMINATION), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
            _value = GUI_FormatNumber(Me.GetRowValue(_row.Item(DT_COLUMN_CURRENCY.QUANTITY)), ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
            If (m_open_mode = OPEN_MODE.RECOLECT_MOVEMENT) Then
              If Me.OperationType = Cage.CageOperationType.FromTerminal And Me.GetRowValue(_row.Item(DT_COLUMN_CURRENCY.CURRENCY_TYPE)) = CageCurrencyType.Coin Then
                _expected_value = EMPTY_VALUE
              Else
                _expected_value = GUI_FormatNumber(Me.GetRowValue(_row.Item(DT_COLUMN_CURRENCY.CASHIER_QUANTITY)), ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
              End If
            Else
              _expected_value = GUI_FormatNumber(Me.GetRowValue(_row.Item(DT_COLUMN_CURRENCY.CASHIER_QUANTITY)) - Me.GetRowValue(_row.Item(DT_COLUMN_CURRENCY.QUANTITY_DIFFERENCE)), ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
            End If
            _currency_type_desc = GetCurrencyTypeDescription(_row.Item(DT_COLUMN_CURRENCY.CURRENCY_TYPE))
            _nls_id = 5674

        End Select

        _value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1984, _value, _expected_value)

        If _nls_id = 5674 Then
          _auditor_data.SetField(0, _value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(_nls_id, _iso_code, _denomination, _currency_type_desc))
        Else
          _auditor_data.SetField(0, _value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(_nls_id, _iso_code, _denomination))
        End If

      Next
    End If

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    If Me.OperationType = Cage.CageOperationType.FromTerminal Then
      _auditor_data.SetField(0, Me.m_type_new_collection.real_ticket_count.ToString(), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2842)) ' Tickets recaudados
      _auditor_data.SetField(0, Me.m_type_new_collection.real_bills_count.ToString(), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2784)) ' Cantidad de billetes
      _auditor_data.SetField(0, GUI_FormatCurrency((Me.m_type_new_collection.real_bills_sum + Me.m_type_new_collection.real_coins_collection_amount).ToString()), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2783)) ' Total entregado

      ''the stacker id is 0 in this scope just when the collection has not been inicialized
      ''so if you try to close a non started collection, the form has to close
      'If Not Me.m_type_new_collection.collection_date Is Nothing And Not Me.m_type_new_collection.already_collected Then
      '  _auditor_data.SetField(0, Me.m_type_new_collection.collection_date.Value.ToString(), GLB_NLS_GUI_PLAYER_TRACKING.GetString(3009))
      'End If

      'For Each _details As TYPE_MONEY_COLLECTION_DETAILS In Me.m_type_new_collection.collection_details
      '  If _details.bill_real_count > 0 Then
      '    _auditor_data.SetField(0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2785, _details.bill_real_count.ToString(), GUI_FormatCurrency(_details.bill_denomination * _details.bill_real_count)), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2782, GUI_FormatCurrency(_details.bill_denomination))) ' Total =
      '  End If
      'Next
    End If

    'Template has to be audited
    If (Me.OperationType = Cage.CageOperationType.ToTerminal OrElse Me.OperationType = Cage.CageOperationType.ToGamingTable OrElse _
       Me.OperationType = Cage.CageOperationType.ToCashier) AndAlso m_open_mode = 0 AndAlso Me.TemplateID > 0 Then
      _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5052), Me.TemplateName)  ' TemplateID
    End If

    If Not _auditor_data.Notify(GLB_CurrentUser.GuiId, _
             GLB_CurrentUser.Id, _
             GLB_CurrentUser.Name, _
             CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.GENERIC, _
             0) Then
      ' Logger message 
    End If
    _auditor_data = Nothing
  End Function

  ' PURPOSE: To save the Cage movement template
  '
  ' PARAMS:
  '       - TemplateName
  '       - CageTemplate
  '       - out TemplateID
  ' RETURNS:
  '       - Boolean
  '
  ' NOTES:
  Public Function SaveTemplate(ByVal TemplateName As String, ByVal CageTemplate As CLASS_CAGE_CONTROL, ByRef TemplateID As Long, ByRef IsNew As Boolean) As Boolean
    Dim _sb As StringBuilder
    Dim _denominations_xml As String
    Dim _data As Object
    Dim _ds As DataSet
    Dim _dtpreferences As DataTable
    Dim _row As DataRow
    Dim _dtcurrencies As DataTable

    _sb = New StringBuilder
    _ds = New DataSet(TABLE_NAME_MOVEMENT)

    'Adding preferences
    _dtpreferences = New DataTable(TABLE_NAME_PREFERENCES)

    _dtpreferences.Columns.Add(TABLE_COLUMN_OPERATION_TYPE, Type.GetType("System.Int32"))
    _dtpreferences.Columns.Add(TABLE_COLUMN_TARGET_TYPE, Type.GetType("System.Int32"))
    _row = _dtpreferences.NewRow

    _row(0) = CageTemplate.OperationType()
    _row(1) = CageTemplate.TargetType
    _dtpreferences.Rows.Add(_row)

    _ds.Tables.Add(_dtpreferences)
    m_table_preferences = _dtpreferences.Copy

    'Adding currencies
    _dtcurrencies = CageTemplate.TableCurrencies.Copy
    _dtcurrencies.TableName = TABLE_NAME_CURRENCIES
    _ds.Tables.Add(_dtcurrencies)

    Using _sw As New IO.StringWriter()

      _ds.WriteXml(_sw, XmlWriteMode.WriteSchema)

      _denominations_xml = _sw.ToString()
    End Using

    Try

      Using _db_trx As New DB_TRX()
        If TemplateID > 0 Then
          UpdateTemplate(TemplateID, _denominations_xml, _db_trx.SqlTransaction)
        Else
          _sb = New StringBuilder
          _sb.AppendLine("   SELECT   CMT_ID ")
          _sb.AppendLine("     FROM   CAGE_MOVEMENT_TEMPLATE ")
          _sb.AppendLine("    WHERE   CMT_NAME = @pTemplateName")

          Using _sql_cmd As New SqlCommand(_sb.ToString, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

            _sql_cmd.Parameters.Add("@pTemplateName", SqlDbType.NVarChar).Value = TemplateName

            _data = _sql_cmd.ExecuteScalar()

            If Not IsNothing(_data) Then
              TemplateID = CType(_data, Long)
              'Item already exists in database
              UpdateTemplate(TemplateID, _denominations_xml, _db_trx.SqlTransaction)
              IsNew = False
            Else
              InsertTemplate(TemplateName, _denominations_xml, TemplateID, _db_trx.SqlTransaction)
              IsNew = True
            End If
          End Using
        End If

        _db_trx.Commit()

      End Using

      Return True

    Catch ex As Exception
      Log.Exception(ex)
    End Try

    Return False

  End Function ' SaveTemplate

  ' PURPOSE: To Insert the Cage movement template
  '
  ' PARAMS:
  '       - TemplateName
  '       - CageTemplate
  '       - out TemplateID
  ' RETURNS:
  '       - Boolean
  '
  ' NOTES:
  Public Function InsertTemplate(ByVal TemplateName As String, ByVal XMLDenominations As String, ByRef TemplateID As Long, ByVal Trx As SqlTransaction) As Boolean
    Dim _sb As StringBuilder
    Dim _param As SqlClient.SqlParameter

    Try

      _sb = New StringBuilder()
      _sb.AppendLine("   INSERT INTO   CAGE_MOVEMENT_TEMPLATE")
      _sb.AppendLine("               ( CMT_NAME ")
      _sb.AppendLine("               , CMT_DENOMINATIONS")
      _sb.AppendLine("               , CMT_CREATION")
      _sb.AppendLine("               ) ")
      _sb.AppendLine("                 VALUES")
      _sb.AppendLine("               ( @pName")
      _sb.AppendLine("               , @pInfo")
      _sb.AppendLine("               , @pCreation")
      _sb.AppendLine("               ) ")
      _sb.AppendLine("   SET @pTemplateId = SCOPE_IDENTITY()")

      Using _cmd As New SqlClient.SqlCommand(_sb.ToString(), Trx.Connection, Trx)

        _cmd.Parameters.Add("@pName", SqlDbType.NVarChar).Value = TemplateName
        _cmd.Parameters.Add("@pInfo", SqlDbType.NVarChar).Value = XMLDenominations
        _cmd.Parameters.Add("@pCreation", SqlDbType.DateTime).Value = WGDB.Now()
        _param = _cmd.Parameters.Add("@pTemplateId", SqlDbType.BigInt)
        _param.Direction = ParameterDirection.Output

        If _cmd.ExecuteNonQuery() <> 1 Or IsDBNull(_param.Value) Then

          Return False
        Else
          TemplateID = _param.Value
        End If
      End Using

      Return True

    Catch ex As Exception
      Log.Exception(ex)
    End Try

    Return False

  End Function ' InsertTemplate

  ' PURPOSE: To update the Cage movement template
  '
  ' PARAMS:
  '       - TemplateName
  '       - CageTemplate
  '       - out TemplateID
  ' RETURNS:
  '       - Boolean
  '
  ' NOTES:
  Public Function UpdateTemplate(ByVal TemplateID As Long, _
                                 ByVal XMLDenominations As String, _
                                 ByVal Trx As SqlTransaction) As Boolean
    Dim _sb As StringBuilder

    Try

      _sb = New StringBuilder()
      _sb.AppendLine("   UPDATE       CAGE_MOVEMENT_TEMPLATE ")
      _sb.AppendLine("      SET       CMT_DENOMINATIONS = @pInfo")
      _sb.AppendLine("              , CMT_CREATION = @pCreation")
      _sb.AppendLine("    WHERE       CMT_ID = @pID")

      Using _cmd As New SqlClient.SqlCommand(_sb.ToString(), Trx.Connection, Trx)

        _cmd.Parameters.Add("@pInfo", SqlDbType.NVarChar).Value = XMLDenominations
        _cmd.Parameters.Add("@pCreation", SqlDbType.DateTime).Value = WGDB.Now()
        _cmd.Parameters.Add("@pID", SqlDbType.BigInt).Value = TemplateID

        If _cmd.ExecuteNonQuery() = 0 Then

          Return False
        End If
      End Using

      Return True

    Catch ex As Exception
      Log.Exception(ex)
    End Try

    Return False

  End Function ' UpdateTemplate

  ' PURPOSE: To update the last time the template was used
  '
  ' PARAMS:
  '       - TemplateName
  '       - CageTemplate
  '       - out TemplateID
  ' RETURNS:
  '       - Boolean
  '
  ' NOTES:
  Public Function UpdateTemplateLastUsed(ByVal TemplateID As Long, _
                                         ByVal Trx As SqlTransaction) As Boolean
    Dim _sb As StringBuilder

    Try

      _sb = New StringBuilder()
      _sb.AppendLine("   UPDATE       CAGE_MOVEMENT_TEMPLATE ")
      _sb.AppendLine("      SET       CMT_LAST_USED = @pLastUsed")
      _sb.AppendLine("    WHERE       CMT_ID = @pID")

      Using _cmd As New SqlClient.SqlCommand(_sb.ToString(), Trx.Connection, Trx)
        _cmd.Parameters.Add("@pLastUsed", SqlDbType.DateTime).Value = WGDB.Now()
        _cmd.Parameters.Add("@pID", SqlDbType.BigInt).Value = TemplateID

        If _cmd.ExecuteNonQuery() = 0 Then

          Return False
        End If
      End Using

      Return True

    Catch ex As Exception
      Log.Exception(ex)
    End Try

    Return False

  End Function ' UpdateTemplateLastUsed

  ' PURPOSE: To set the template cage sessions combo box
  '
  ' PARAMS:
  '       - Combo
  '       - ShowEmptyElement
  '
  ' RETURNS:
  '       - None
  '
  ' NOTES:
  Public Sub SetComboTemplateCageSessions(ByRef Combo As uc_combo, Optional ByVal ShowEmptyElement As Boolean = False)
    Dim _sb As StringBuilder
    Dim _table As DataTable

    _sb = New StringBuilder

    _sb.AppendLine("   SELECT   CMT_ID, CMT_NAME ")
    _sb.AppendLine("     FROM   CAGE_MOVEMENT_TEMPLATE ")
    _sb.AppendLine(" ORDER BY   CMT_LAST_USED DESC, CMT_NAME ASC ")

    Try

      _table = GUI_GetTableUsingSQL(_sb.ToString(), Integer.MaxValue)

    Catch ex As Exception
      _table = New DataTable
    End Try

    Call Combo.Clear()

    'If the number of rows exceed the general param we don't let the user create new templates
    If _table.Rows.Count <= WSI.Common.GeneralParam.GetInt32("Cage", "MaxNumberTemplates", MAX_NUMBER_TEMPLATES) Then
      If ShowEmptyElement Then
        Combo.Add(-1, String.Empty)
      End If
    End If

    Call Combo.Add(_table, "CMT_ID", "CMT_NAME")

    Combo.TextValue = String.Empty
    Combo.SelectedIndex = -1

  End Sub ' SetComboCageSessions

  '----------------------------------------------------------------------------
  ' PURPOSE : Read object (points to credit) from the DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - PointsToCreditId: Points to credit primary key
  '         - DbTrx: Transaction
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - Datatable:
  '
  ' NOTES :
  Public Function LoadCageTemplate(ByVal TemplateID As Int64, ByRef DSTemplate As DataSet) As Boolean
    Dim _sb As StringBuilder
    Dim _xml As String
    Dim _obj As Object

    DSTemplate = New DataSet()
    _sb = New StringBuilder()

    Try
      _sb.AppendLine("   SELECT   CMT_DENOMINATIONS ")
      _sb.AppendLine("     FROM   CAGE_MOVEMENT_TEMPLATE ")
      _sb.AppendLine("    WHERE   CMT_ID = @pID")

      Using _db_trx As New DB_TRX()
        Using _sql_cmd As New SqlCommand(_sb.ToString, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          _sql_cmd.Parameters.Add("@pID", SqlDbType.BigInt).Value = TemplateID

          _obj = _sql_cmd.ExecuteScalar()

          If _obj IsNot Nothing And _obj IsNot DBNull.Value Then
            _xml = CType(_obj, String)

            Using _sr As New IO.StringReader(_xml)
              DSTemplate.ReadXml(_sr, XmlReadMode.Auto)
            End Using
          End If
        End Using
      End Using

      Return True

    Catch ex As Exception
      Log.Exception(ex)
    End Try

    Return False
  End Function ' GetPointsToCreditGifts



#End Region ' Cage movement template

  Public Function WithdrawalCancelation() As ENUM_STATUS

    Dim _input_undo_operation As OperationUndo.InputUndoOperation
    Dim _card_data As CardData
    Dim _voucher_list As ArrayList
    Dim _result As ENUM_STATUS
    Dim _gaming_table_session As GamingTablesSessions
    Dim _total_amount As New SortedDictionary(Of CurrencyIsoType, Decimal)(New CurrencyIsoType.SortComparer)
    Dim _total_chips As New SortedDictionary(Of CurrencyIsoType, Decimal)(New CurrencyIsoType.SortComparer)
    Dim _total_amount_national As Decimal
    Dim _total_chips_national As Decimal
    Dim _total_tickets_amount As Decimal
    Dim _session_data As New WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS
    Dim _total_collected As New SortedDictionary(Of CurrencyIsoType, Decimal)(New CurrencyIsoType.SortComparer)
    Dim _total_collected_national As Decimal

    _input_undo_operation = New OperationUndo.InputUndoOperation()
    _card_data = New CardData()
    _result = ENUM_STATUS.STATUS_OK

    _input_undo_operation.CardData = _card_data
    _input_undo_operation.CashierSessionInfo = CommonCashierInformation.CashierSessionInfo()
    _input_undo_operation.CodeOperation = OperationCode.CASH_WITHDRAWAL
    _input_undo_operation.GenerateVoucherForUndo = True
    _input_undo_operation.OperationId = 0
    _voucher_list = New ArrayList()
    _gaming_table_session = Nothing
    _total_amount_national = 0
    _total_chips_national = 0
    _total_tickets_amount = 0
    _total_collected_national = 0

    Using _db_trx As New DB_TRX()
      ' check if cashier session is open 
      If IsCashierSessionOpen(_db_trx.SqlTransaction) Then

        ' Set session information
        _input_undo_operation.OperationIdForUndo = Cage.GetAccountOperatioByCageMovement(Me.MovementID, _db_trx.SqlTransaction)
        _input_undo_operation.CashierSessionInfo.CashierSessionId = Me.CashierSessionId
        _input_undo_operation.CashierSessionInfo.TerminalId = GetCashierTerminalIdByCageMovId(Me.MovementID, _db_trx.SqlTransaction)
        _input_undo_operation.CashierSessionInfo.TerminalName = Me.GetCashierSessionNameById(Me.CashierSessionId)
        _input_undo_operation.CashierSessionInfo.AuthorizedByUserName = GLB_CurrentUser.Name
        _input_undo_operation.CashierSessionInfo.UserId = GLB_CurrentUser.Id

        If OperationUndo.UndoOperation(_input_undo_operation, True, _voucher_list, _db_trx.SqlTransaction) AndAlso _
                                       Cage.UpdateCageMovementStatus(Me.MovementID, Cage.CageStatus.CanceledUncollectedCashierRetirement, _input_undo_operation.CashierSessionInfo.UserId, _input_undo_operation.CashierSessionInfo.CashierSessionId, _db_trx.SqlTransaction) Then

          ' MPO 08-JUN-2015: Don't check if DELETE is ok. Without cage, there is no pending movements.
          Me.DeleteFromPendingMovement(_db_trx.SqlTransaction)

          ' FJC 20-SEP-2016 Bug 17669:Anular retiro de caja desde b�veda/Cajero: no deshace el stock y conceptos de b�veda
          If Cage.IsCageAutoMode Then
            If Not Me.UpdateCageStockWhenCancel(_db_trx.SqlTransaction) Then
              Return ENUM_STATUS.STATUS_ERROR
            End If

            If Not CageMeters.UndoCageMeters(Me.MovementID, False, _db_trx.SqlTransaction) Then
              Return ENUM_STATUS.STATUS_ERROR
            End If

          End If

          If (Me.OperationType = Cage.CageOperationType.FromGamingTable Or Me.OperationType = Cage.CageOperationType.FromGamingTableClosing) Then

            If Not GamingTablesSessions.GetOrOpenSession(_gaming_table_session, Me.GamingTableId, Me.CashierSessionId, _db_trx.SqlTransaction) Then
              Return ENUM_STATUS.STATUS_ERROR
            End If

            CalculateTotalsAmounts(Me.TableCurrencies, _total_amount, _total_chips, _total_amount_national, _total_chips_national, _total_tickets_amount, _db_trx.SqlTransaction)

            WSI.Common.Cashier.ReadCashierSessionData(_db_trx.SqlTransaction, _gaming_table_session.CashierSessionId, _session_data)

            GamingTablesSessions.GetCollectedAmount(_session_data, _total_collected, _total_collected_national, _db_trx.SqlTransaction)

            If Not _gaming_table_session.UpdateSession(GTS_UPDATE_TYPE.CollectedAmount, _total_collected, _db_trx.SqlTransaction) Then
              Return ENUM_STATUS.STATUS_ERROR
            End If

            If Not _gaming_table_session.UpdateSession(GTS_UPDATE_TYPE.CollectedAmount, _total_collected_national, _db_trx.SqlTransaction) Then
              Return ENUM_STATUS.STATUS_ERROR
            End If

            For Each _total_chip As KeyValuePair(Of CurrencyIsoType, Decimal) In _total_chips
              If Not _gaming_table_session.UpdateSession(GTS_UPDATE_TYPE.FillOut, -_total_chip.Value, _total_chip.Key, _db_trx.SqlTransaction) Then
                Return ENUM_STATUS.STATUS_ERROR
              End If
            Next
          End If

          _db_trx.Commit()
        Else
          _db_trx.Rollback()
          _result = ENUM_STATUS.STATUS_ERROR
        End If
      Else
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5854), ENUM_MB_TYPE.MB_TYPE_ERROR, , , , , )
      End If
    End Using

    Return _result
  End Function

#End Region ' Public

End Class
