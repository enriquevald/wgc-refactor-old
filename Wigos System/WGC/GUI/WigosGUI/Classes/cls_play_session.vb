'-------------------------------------------------------------------
' Copyright � 2009 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_play_session.vb
' DESCRIPTION:   Play Session class
' AUTHOR:        Miquel Beltran Febrer
' CREATION DATE: 14-APR-2009
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 14-APR-2009  MBF    Initial version
'--------------------------------------------------------------------
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient

Imports WSI.Common


Public Class CLASS_PLAY_SESSION
  Inherits CLASS_BASE

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_PLAY_SESSION
    Public status As Integer
    Public session_id As Int64
    Public inactivity As Integer
    Public account_id As Integer
    Public final_balance As Double
    Public terminal_id As Integer
    Public terminal_name As String
    Public terminal_type As Integer

  End Class

#Region " Constants "

  ' Status
  Private Const STATUS_OPENED As Integer = 0
  Private Const STATUS_CLOSED As Integer = 1
  Private Const STATUS_ABANDONED As Integer = 2
  Private Const STATUS_CLOSED_MANUALLY As Integer = 3

  Private Const MOV_TYPE_DB_MANUAL_END_SESSION As Integer = 32

#End Region ' Constants

#Region " Members "

  Protected m_play_session As New TYPE_PLAY_SESSION

#End Region 'Members

#Region "Properties"

  Public Property Status() As Integer
    Get
      Return m_play_session.status
    End Get

    Set(ByVal Value As Integer)
      m_play_session.status = Value
    End Set

  End Property

  Public Property SessionId() As Integer
    Get
      Return m_play_session.session_id
    End Get

    Set(ByVal Value As Integer)
      m_play_session.session_id = Value
    End Set

  End Property

  Public Property Inactivity() As Integer
    Get
      Return m_play_session.inactivity
    End Get

    Set(ByVal Value As Integer)
      m_play_session.inactivity = Value
    End Set

  End Property

  Public Property AccountId() As Integer
    Get
      Return m_play_session.account_id
    End Get

    Set(ByVal Value As Integer)
      m_play_session.account_id = Value
    End Set

  End Property

  Public Property FinalBalance() As Double
    Get
      Return m_play_session.final_balance
    End Get

    Set(ByVal Value As Double)
      m_play_session.final_balance = Value
    End Set

  End Property

  Public Property TerminalId() As Integer
    Get
      Return m_play_session.terminal_id
    End Get

    Set(ByVal Value As Integer)
      m_play_session.terminal_id = Value
    End Set

  End Property

  Public Property TerminalName() As String
    Get
      Return m_play_session.terminal_name
    End Get

    Set(ByVal Value As String)
      m_play_session.terminal_name = Value
    End Set

  End Property

  Public Property TerminalType() As Integer
    Get
      Return m_play_session.terminal_type
    End Get

    Set(ByVal Value As Integer)
      m_play_session.terminal_type = Value
    End Set

  End Property



#End Region ' Properties

#Region "Overrides"
  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim auditor_data As CLASS_AUDITOR_DATA
    Dim str_status As String = ""

    ' Create a new auditor_code
    auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_TERMINALS)

    ' Session Id
    Call auditor_data.SetName(GLB_NLS_GUI_STATISTICS.Id(393), SessionId())

    ' Status
    Select Case Status()
      Case STATUS_OPENED
        str_status = GLB_NLS_GUI_STATISTICS.GetString(400)

      Case STATUS_CLOSED
        str_status = GLB_NLS_GUI_STATISTICS.GetString(401)

      Case STATUS_ABANDONED
        str_status = GLB_NLS_GUI_STATISTICS.GetString(402)

      Case STATUS_CLOSED_MANUALLY
        str_status = GLB_NLS_GUI_STATISTICS.GetString(404)

    End Select
    Call auditor_data.SetField(GLB_NLS_GUI_STATISTICS.Id(394), str_status)

    Return auditor_data
  End Function ' AuditorData

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
  End Function ' DB_Delete

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
  End Function ' DB_Insert

  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim rc As Integer

    Me.SessionId = ObjectId

    rc = PlaySession_DbRead(m_play_session)

    Select Case rc
      Case ENUM_DB_RC.DB_RC_OK
        Return ENUM_STATUS.STATUS_OK

      Case ENUM_DB_RC.DB_RC_ERROR_NOT_FOUND
        Return CLASS_BASE.ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Read

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
  End Function ' DB_Update

  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim temp_play_session As CLASS_PLAY_SESSION

    temp_play_session = New CLASS_PLAY_SESSION

    temp_play_session.SessionId = Me.SessionId
    temp_play_session.Status = Me.Status
    temp_play_session.Inactivity = Me.Inactivity
    temp_play_session.AccountId = Me.AccountId

    temp_play_session.FinalBalance = Me.FinalBalance
    temp_play_session.TerminalId = Me.TerminalId
    temp_play_session.TerminalName = Me.TerminalName
    temp_play_session.TerminalType = Me.TerminalType

    Return temp_play_session
  End Function ' Duplicate

#End Region ' Overrides

#Region " Private Functions "

  Private Function PlaySession_DbRead(ByVal pPlaySession As TYPE_PLAY_SESSION) As Integer

    Dim str_sql As String
    Dim data_table As DataTable

    str_sql = "  SELECT   PS_ACCOUNT_ID " & _
              "         , PS_PLAY_SESSION_ID " & _
              "         , PS_STATUS " & _
              "         , PS_ACCOUNT_ID " & _
              "         , PS_FINAL_BALANCE " & _
              "         , PS_TERMINAL_ID " & _
              "         , TE_NAME " & _
              "         , TE_TERMINAL_TYPE " & _
              "         , ISNULL(DATEDIFF (MINUTE, MAX(PL_DATETIME), GETDATE()), DATEDIFF (MINUTE, PS_STARTED, GETDATE())) AS INACTIVITY " & _
              "    FROM   PLAY_SESSIONS " & _
              "    LEFT   OUTER JOIN PLAYS ON PS_PLAY_SESSION_ID = PL_PLAY_SESSION_ID " & _
              "    LEFT   OUTER JOIN TERMINALS ON TE_TERMINAL_ID = PS_TERMINAL_ID " & _
              "   WHERE   PS_PLAY_SESSION_ID = " & pPlaySession.session_id & " " & _
              "GROUP BY   PS_ACCOUNT_ID " & _
              "         , PS_PLAY_SESSION_ID " & _
              "         , PS_STATUS " & _
              "         , PS_ACCOUNT_ID " & _
              "         , PS_FINAL_BALANCE " & _
              "         , PS_TERMINAL_ID " & _
              "         , TE_NAME " & _
              "         , TE_TERMINAL_TYPE " & _
              "         , PS_STARTED "

    data_table = GUI_GetTableUsingSQL(str_sql, 50)
    If IsNothing(data_table) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    If data_table.Rows.Count() <> 1 Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    pPlaySession.status = data_table.Rows(0).Item("PS_STATUS")
    pPlaySession.inactivity = data_table.Rows(0).Item("INACTIVITY")
    pPlaySession.account_id = data_table.Rows(0).Item("PS_ACCOUNT_ID")
    pPlaySession.session_id = data_table.Rows(0).Item("PS_PLAY_SESSION_ID")
    pPlaySession.final_balance = data_table.Rows(0).Item("PS_FINAL_BALANCE")
    pPlaySession.terminal_id = data_table.Rows(0).Item("PS_TERMINAL_ID")
    pPlaySession.terminal_name = data_table.Rows(0).Item("TE_NAME")
    pPlaySession.terminal_type = data_table.Rows(0).Item("TE_TERMINAL_TYPE")

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

  End Function ' PlaySession_DbRead

  Private Function PlaySessionClose_DBUpdate(ByVal pPlaySession As TYPE_PLAY_SESSION, ByVal ClosingReason As String) As Integer

    Try
      If WSI.Common.CardData.DB_PlaySessionCloseManually(pPlaySession.account_id, _
                                                         pPlaySession.session_id, _
                                                         pPlaySession.final_balance, _
                                                         pPlaySession.terminal_id, _
                                                         pPlaySession.terminal_name, _
                                                         pPlaySession.terminal_type, _
                                                         CurrentUser.Name + "@" + Environment.MachineName, _
                                                         False, _
                                                         0, ClosingReason) Then

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
      End If

    Catch ex As Exception
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

  End Function ' PlaySessionClose_DBUpdate

#End Region ' Private Functions

#Region " Public Functions "

  ' PURPOSE: Close the play session if is active
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Public Function PlaySessionClose(ByVal ClosingReason As String) As Integer

    Dim rc As Integer

    If Me.Status <> 0 Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    rc = PlaySessionClose_DBUpdate(m_play_session, ClosingReason)
    PlaySession_DbRead(m_play_session)

    Return rc

  End Function ' PlaySessionClose

#End Region ' Public Functions

End Class