'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_user.vb
' DESCRIPTION:   User class for user edition
' AUTHOR:        Jaume Sala
' CREATION DATE: 04-JUL-2002
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 04-JUL-2002  JSV    Initial version.
' 07-AUG-2012  JCM    Added Cashier Sesion Sales Limit, Cashier Sesion all MB Sales Limit.
' 07-SEP-2012  JAB    Can created terminated users.
' 01-OCT-2012  JAB    Fixed bug, when the user�s full name has a single quote fails.
' 16-OCT-2012  RRB    Fixed bug, when insert/update with Sales Limit or MB Sales Limit (not parsing).
' 07-FEB-2013  JCM    Added Functionality Locked by inactivity
' 13-FEB-2014  LJM    Added Employee code to user
' 22-SEP-2014  HBB & XIT Added Change Stacker card code to user
' 25-SEP-2014  SGB    Added Change Technician card code to user
' 25-SEP-2014  SGB    Fixed Bug #WIG-1277 & #WIG-1279: multisite control input because Stacker card & Technician card columns do not exist in BD
' 08-OCT-2014  HBB    Fixed Bug #WIG-1233 : New user creation: the data aren't save correctly
' 30-APR-2015  YNM    Fixed Bug #WIG-2153 : When password is changed, the change on next login check isn't saved.
' 15-OCT-2015  JBC    Added SmartFloor Manager 
' 02-NOV-2016  OVS    Added Employee Card treatment
' 28-NOV-2016  FAV    PBI 20374:EGASA: Logic card assignment
'-------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports WSI.Common
Imports System.Data.SqlClient
Imports System.Text

Public Class CLASS_USER
  Inherits CLASS_BASE

#Region " Constants "

  ' Fields length
  Public Const MAX_USERNAME_LEN As Integer = 10
  Public Const MAX_FULL_NAME_LEN As Integer = 20
  Public Const MAX_PASSWORD_LEN As Integer = 16

  Public Const USER_STATE_ENABLE As Integer = 1
  Public Const USER_STATE_DISABLE As Integer = 0

  Public Const USER_PASSWORD_REQUIRED As Integer = 1
  Public Const USER_PASSWORD_NOT_REQUIRED As Integer = 0

  Public Const USER_HIERARCHY_TYPE_MANAGER As Integer = 1
  Public Const USER_HIERARCHY_TYPE_OTHER As Integer = 0

  Public Const UNDEFINED_PWD_STRING As String = "__INVALID_PWD__"
  Public Const AUDIT_NONE_STRING As String = "---"

#End Region

#Region " GUI_Configuration.dll "

#Region " Structures "
  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_USER
    Public control_block As Integer
    Public user_id As Integer
    Public profile_id As Integer
    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_USERNAME_LEN + 1)> _
    Public username As String
    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=1)> _
    Public filler_0 As String
    ''XCD 14-Aug-2012 State Deprecated
    'Public state As Integer
    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_PASSWORD_LEN + 1)> _
    Public password As String
    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=3)> _
    Public filler_1 As String
    Public password_date As New TYPE_DATE_TIME
    Public password_exp As Integer
    Public password_req As Integer
    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_FULL_NAME_LEN + 1)> _
    Public full_name As String
    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=3)> _
    Public filler_2 As String
    Public sales_limit_cashier As String
    Public sales_limit_mb As String
    Public block_reason As Integer
    Public gu_master As Boolean
    Public gu_master_id As Integer
    Public employee_code As String

    Public intellia_manager As Boolean
    Public intellia_runner As Boolean

    Public card_number As String
    Public card_technician As String
    Public card_employee As String

    Public mobile_bank As cls_mobile_bank

    ''' <summary>
    ''' Constructor
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub New()
      Me.mobile_bank = New cls_mobile_bank()
    End Sub
  End Class
#End Region

#End Region

#Region " Members "
  Protected m_user As New TYPE_USER
  Protected m_original_block_reason As Int32
  Protected m_profile_name As String
  Protected m_pwd_policy As PasswordPolicy
#End Region

#Region " Constructor "

  Public Sub New()
    m_pwd_policy = New PasswordPolicy()
  End Sub

#End Region

#Region " Properties "
  Public Property UserId() As Integer
    Get
      Return m_user.user_id
    End Get
    Set(ByVal Value As Integer)
      m_user.user_id = Value
    End Set
  End Property

  Public Property ProfileName() As String
    Get
      Return m_profile_name
    End Get
    Set(ByVal Value As String)
      m_profile_name = Value
    End Set
  End Property

  Public Property UserName() As String
    Get
      Return m_user.username
    End Get
    Set(ByVal Value As String)
      m_user.username = Value
    End Set
  End Property

  Public Property FullName() As String
    Get
      Return m_user.full_name
    End Get
    Set(ByVal Value As String)
      m_user.full_name = Value
    End Set
  End Property

  Public Property ProfileId() As Integer
    Get
      Return m_user.profile_id
    End Get
    Set(ByVal Value As Integer)
      m_user.profile_id = Value
    End Set
  End Property

  Public Property Password() As String
    Get
      Return m_pwd_policy.ParsePassword(m_user.password)
    End Get
    Set(ByVal Value As String)
      m_user.password = m_pwd_policy.ParsePassword(Value)
    End Set
  End Property

  Public Property PasswordDate() As Date
    Get
      If m_user.password_date.IsNull Then
        Return GUI_GetDate()
      Else
        Return m_user.password_date.Value
      End If
    End Get
    Set(ByVal Value As Date)
      If IsNothing(m_user.password_date) Then
        m_user.password_date = New TYPE_DATE_TIME
      End If
      m_user.password_date.Value = Value
    End Set
  End Property

  Public Property PasswordExp() As Integer
    Get
      Return m_user.password_exp
    End Get
    Set(ByVal Value As Integer)
      m_user.password_exp = Value
    End Set
  End Property

  Public Property PasswordReq() As Integer
    Get
      Return m_user.password_req
    End Get
    Set(ByVal Value As Integer)
      m_user.password_req = Value
    End Set
  End Property

  Public Property SalesLimitCashier() As String
    Get
      Return m_user.sales_limit_cashier
    End Get
    Set(ByVal Value As String)
      m_user.sales_limit_cashier = Value
    End Set
  End Property

  Public Property SalesLimitMB() As String
    Get
      Return m_user.sales_limit_mb
    End Get
    Set(ByVal Value As String)
      m_user.sales_limit_mb = Value
    End Set
  End Property

  Public Property BlockReason() As Integer
    Get
      Return m_user.block_reason
    End Get
    Set(ByVal Value As Integer)
      m_user.block_reason = Value
    End Set
  End Property

  Public Property GupMaster() As Boolean
    Get
      Return m_user.gu_master
    End Get
    Set(ByVal Value As Boolean)
      m_user.gu_master = Value
    End Set
  End Property

  Public Property GupMasterId() As Integer
    Get
      Return m_user.gu_master_id
    End Get
    Set(ByVal Value As Integer)
      m_user.gu_master_id = Value
    End Set
  End Property

  Public Property EmployeeCode() As String
    Get
      Return m_user.employee_code
    End Get
    Set(ByVal value As String)
      m_user.employee_code = value
    End Set
  End Property

  Public Property CardNumber() As String
    Get
      Return m_user.card_number
    End Get
    Set(ByVal value As String)
      m_user.card_number = value
    End Set
  End Property

  Public Property CardTechnician() As String
    Get
      Return m_user.card_technician
    End Get
    Set(ByVal value As String)
      m_user.card_technician = value
    End Set
  End Property

  Public Property CardEmployee() As String
    Get
      Return m_user.card_employee
    End Get
    Set(ByVal value As String)
      m_user.card_employee = value
    End Set
  End Property

  Public Property IntelliaManager() As Boolean
    Get
      Return m_user.intellia_manager
    End Get
    Set(ByVal value As Boolean)
      m_user.intellia_manager = value
    End Set
  End Property

  Public Property IntelliaRunner() As Boolean
    Get
      Return m_user.intellia_runner
    End Get
    Set(ByVal value As Boolean)
      m_user.intellia_runner = value
    End Set
  End Property

  Public Property MobileBank() As cls_mobile_bank
    Get
      Return m_user.mobile_bank
    End Get
    Set(ByVal value As cls_mobile_bank)
      m_user.mobile_bank = value
    End Set
  End Property


#End Region

#Region " Overrides functions "

  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim rc As Integer

    Me.UserId = ObjectId

    m_user.control_block = Marshal.SizeOf(m_user)

    ' Read user data (except password)
    rc = ReadUser(m_user, SqlCtx)

    ' Initialize password
    m_user.password = String.empty

    Select Case rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        ' Profile name
        Me.ProfileName = CLASS_PROFILE.GetProfileNameForId(Me.ProfileId, SqlCtx)

        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim rc As Integer

    m_user.control_block = Marshal.SizeOf(m_user)

    ' Update user data
    rc = UpdateUser(m_user, SqlCtx)

    Select Case rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        ' Profile name
        Me.ProfileName = CLASS_PROFILE.GetProfileNameForId(Me.ProfileId, SqlCtx)

        'Dim _old_user As CLASS_USER = TryCast(DbReadObject, CLASS_USER)
        'Dim _new_user As CLASS_USER = TryCast(m_user, CLASS_USER)

        'If Not ((_old_user Is Nothing) And (_new_user Is Nothing)) Then
        '  If (Not _old_user.MobileBank.Equals(_new_user.MobileBank)) Then
        '    _new_user.DB_Update()
        '  End If

        '  Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
        '  If Me.CloseOnOkClick Then
        '    Call Me.Close()
        '  End If
        'End If

        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_PASSWORD
        Return CLASS_BASE.ENUM_STATUS.STATUS_PASSWORD

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim rc As Integer

    m_user.control_block = Marshal.SizeOf(m_user)

    ' Insert user data
    rc = InsertUser(m_user, SqlCtx)

    Select Case rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        ' Profile name
        Me.ProfileName = CLASS_PROFILE.GetProfileNameForId(Me.ProfileId, SqlCtx)

        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_PASSWORD
        Return CLASS_BASE.ENUM_STATUS.STATUS_PASSWORD

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
        Return ENUM_STATUS.STATUS_DUPLICATE_KEY

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim rc As Integer

    m_user.control_block = Marshal.SizeOf(m_user)

    ' Delete user data
    rc = DeleteUser(m_user, SqlCtx)

    Select Case rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        ' Profile name
        Me.ProfileName = CLASS_PROFILE.GetProfileNameForId(Me.ProfileId, SqlCtx)

        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DEPENDENCIES
        Return ENUM_STATUS.STATUS_DEPENDENCIES

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function

  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _string_date As String

    Dim _text_yes As String = GLB_NLS_GUI_PLAYER_TRACKING.GetString(698)  ' Yes
    Dim _text_no As String = GLB_NLS_GUI_PLAYER_TRACKING.GetString(699)   ' No
    'XVV Variable not use Dim str_hierarchy As String

    ' Create a new auditor_code for user/profile manipulation
    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_USERS)
    Call _auditor_data.SetName(GLB_NLS_GUI_CONFIGURATION.Id(326), Me.UserName)

    'Username
    Call _auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(326), IIf(Me.UserName = "", AUDIT_NONE_STRING, Me.UserName))

    'EmployeeCode
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(4703), IIf(String.IsNullOrEmpty(Me.EmployeeCode), AUDIT_NONE_STRING, Me.EmployeeCode))

    'CardNumber
    Call _auditor_data.SetField(0, IIf(String.IsNullOrEmpty(Me.CardNumber), AUDIT_NONE_STRING, Me.CardNumber) _
                                , GLB_NLS_GUI_PLAYER_TRACKING.GetString(5467) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5468))

    'CardTechnician
    Call _auditor_data.SetField(0, IIf(String.IsNullOrEmpty(Me.CardTechnician), AUDIT_NONE_STRING, Me.CardTechnician) _
                                , GLB_NLS_GUI_PLAYER_TRACKING.GetString(5467) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5574))

    'CardEmployee
    Call _auditor_data.SetField(0, IIf(String.IsNullOrEmpty(Me.CardEmployee), AUDIT_NONE_STRING, Me.CardEmployee) _
                                , GLB_NLS_GUI_PLAYER_TRACKING.GetString(5467) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2224))

    'Fullname
    Call _auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(327), IIf(Me.FullName = "", AUDIT_NONE_STRING, Me.FullName))

    'Master
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2087), IIf(Me.GupMaster, _text_yes, _text_no))

    ' State - Block reasons XCD 16-Aug-201
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1220), _
                                IIf((Me.BlockReason And GUI_USER_BLOCK_REASON.DISABLED).Equals(GUI_USER_BLOCK_REASON.DISABLED), _
                                  _text_yes, _
                                  _text_no))

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1221), _
                                IIf((Me.BlockReason And GUI_USER_BLOCK_REASON.WRONG_PASSWORD).Equals(GUI_USER_BLOCK_REASON.WRONG_PASSWORD), _
                                  _text_yes, _
                                  _text_no))

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1222), _
                                IIf((Me.BlockReason And GUI_USER_BLOCK_REASON.INACTIVITY).Equals(GUI_USER_BLOCK_REASON.INACTIVITY), _
                                  _text_yes, _
                                  _text_no))

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1223), _
                                IIf((Me.BlockReason And GUI_USER_BLOCK_REASON.UNSUBSCRIBED).Equals(GUI_USER_BLOCK_REASON.UNSUBSCRIBED), _
                                  _text_yes, _
                                  _text_no))

    ' Profile name
    Call _auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(319), IIf(Me.ProfileName = "", AUDIT_NONE_STRING, Me.ProfileName))

    ' Password
    Call _auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(305), Me.Password, , CLASS_AUDITOR_DATA.ENUM_FIELD_TYPE.FIELD_NO_DATA)

    ' Password date
    If Me.PasswordDate = Nothing Then
      _string_date = AUDIT_NONE_STRING
    Else
      _string_date = GUI_FormatDate(Me.PasswordDate, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT)
    End If

    Call _auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(307), _string_date)

    ' Valid days
    If Me.PasswordExp = 0 Then
      Call _auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(308), GLB_NLS_GUI_CONFIGURATION.GetString(309))
    Else
      Call _auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(308), Me.PasswordExp)
    End If

    ' Require change
    If Me.PasswordReq = USER_PASSWORD_REQUIRED Then
      Call _auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(317), GLB_NLS_GUI_CONFIGURATION.GetString(264))
    Else
      Call _auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(317), GLB_NLS_GUI_CONFIGURATION.GetString(265))
    End If

    ' Limit Sales Cashier
    If Me.SalesLimitCashier = "" Then
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1176), AUDIT_NONE_STRING)
    Else
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1176), GUI_FormatCurrency(Me.SalesLimitCashier, 2))
    End If

    ' Limit Sales Mobile bank
    If Me.SalesLimitMB = "" Then
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1178), AUDIT_NONE_STRING)
    Else
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1178), GUI_FormatCurrency(Me.SalesLimitMB, 2))
    End If

    ' Intellia manager/runner
    If WSI.Common.Misc.IsIntelliaEnabled() Then
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6799), IIf(Me.IntelliaManager, _text_yes, _text_no))
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6933), IIf(Me.IntelliaRunner, _text_yes, _text_no))
    End If

    'Add Mobile bank auditor data to user auditor data
    AddMobileBankAuditorData(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1182), _auditor_data)

    Return _auditor_data
  End Function

  ''' <summary>
  ''' Adds to the user auditor data the mobile bank auditori data
  ''' </summary>
  ''' <param name="NamePreffix"></param>
  ''' <param name="Auditor"></param>
  ''' <remarks></remarks>
  Public Sub AddMobileBankAuditorData(ByVal NamePreffix As String, ByRef Auditor As CLASS_AUDITOR_DATA)
    Dim _mb_au As CLASS_AUDITOR_DATA

    Dim _idx As Integer

    Dim _field_name As String
    Dim _field_value As String

    If ((Auditor Is Nothing) OrElse (Me.MobileBank Is Nothing)) Then
      Return
    End If

    'Get MB auditor data
    _mb_au = Me.MobileBank.AuditorData
    If (_mb_au Is Nothing) Then
      Return
    End If

    For _idx = 0 To (_mb_au.FieldCount - 1)
      _field_name = _mb_au.FieldName(_idx)
      _field_value = _mb_au.FieldValue(_idx)

      'Add MB auditor data to USER auditor data
      Auditor.SetField(0, _field_value, NamePreffix & "." & _field_name)
    Next
  End Sub

  ''' <summary>
  ''' Duplicates a user info
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE
    Dim temp_user As CLASS_USER

    temp_user = New CLASS_USER()

    temp_user.m_user.control_block = Me.m_user.control_block
    temp_user.m_user.user_id = Me.m_user.user_id
    temp_user.m_user.profile_id = Me.m_user.profile_id
    temp_user.m_user.username = Me.m_user.username
    temp_user.m_user.filler_0 = Me.m_user.filler_0
    'XCD 14-Aug-2012 State Deprecated
    'temp_user.m_user.state = Me.m_user.state
    temp_user.m_user.block_reason = Me.m_user.block_reason
    m_original_block_reason = Me.m_user.block_reason
    temp_user.m_user.password = m_pwd_policy.ParsePassword(Me.m_user.password)
    temp_user.m_user.filler_1 = Me.m_user.filler_1

    If Me.m_user.password_date.IsNull Then
      temp_user.m_user.password_date.IsNull = True
    Else
      temp_user.m_user.password_date.Value = Me.m_user.password_date.Value
    End If

    temp_user.m_user.password_exp = Me.m_user.password_exp
    temp_user.m_user.password_req = Me.m_user.password_req
    temp_user.m_user.full_name = Me.m_user.full_name
    temp_user.m_user.filler_2 = Me.m_user.filler_1
    temp_user.m_profile_name = Me.m_profile_name
    temp_user.m_user.sales_limit_cashier = Me.m_user.sales_limit_cashier
    temp_user.m_user.sales_limit_mb = Me.m_user.sales_limit_mb
    temp_user.m_user.gu_master = Me.m_user.gu_master
    temp_user.m_user.gu_master_id = Me.m_user.gu_master_id
    temp_user.m_user.employee_code = Me.m_user.employee_code
    temp_user.m_user.card_number = Me.m_user.card_number
    temp_user.m_user.card_technician = Me.m_user.card_technician
    temp_user.m_user.card_employee = Me.m_user.card_employee
    temp_user.m_user.intellia_manager = Me.m_user.intellia_manager
    temp_user.m_user.intellia_runner = Me.m_user.intellia_runner

    temp_user.MobileBank = Me.MobileBank.Duplicate()

    Return temp_user
  End Function

#End Region

#Region " Private Functions "

  Private Function ReadUser(ByVal pUser As TYPE_USER, _
                            ByVal Context As Integer) As Integer
    Dim _sb As StringBuilder
    Dim data_table As DataTable
    Dim date_exp As Date
    Dim _data_row As DataRow

    _sb = New StringBuilder()

    ' XCD 14-Aug-2012 GU_ENABLED DEPRECATED
    _sb.AppendLine(" SELECT   GU_PROFILE_ID ")
    _sb.AppendLine("        , GU_USERNAME ")
    _sb.AppendLine("        , GU_NOT_VALID_BEFORE ")
    _sb.AppendLine("        , ISNULL(GU_PASSWORD_EXP, '1900/01/01 00:00:00') AS GU_PASSWORD_EXP ")
    _sb.AppendLine("        , GU_PWD_CHG_REQ ")
    _sb.AppendLine("        , ISNULL(GU_FULL_NAME, ' ') AS GU_FULL_NAME ")
    _sb.AppendLine("        , GU_SALES_LIMIT ")
    _sb.AppendLine("        , GU_MB_SALES_LIMIT ")
    _sb.AppendLine("        , GU_BLOCK_REASON ")
    _sb.AppendLine("        , CASE WHEN GU_MASTER_ID IS NULL THEN 0 ELSE 1 END AS GU_MASTER ")
    _sb.AppendLine("        , ISNULL(GU_MASTER_ID, 0) AS GU_MASTER_ID ")
    _sb.AppendLine("        , ISNULL(GU_EMPLOYEE_CODE, '') AS GU_EMPLOYEE_CODE ")

    If WSI.Common.Misc.IsIntelliaEnabled() Then
      _sb.AppendLine("        , LC_IS_MANAGER ")
      _sb.AppendLine("        , LC_IS_RUNNER ")
    End If

    ' SGB 25-SEP-2014 These columns do not exist in multisite
    If Not GeneralParam.GetBoolean("MultiSite", "IsCenter") AndAlso _
    Not GeneralParam.GetBoolean("CashDesk.Draw", "IsCashDeskDraw") Then
      _sb.AppendLine("        , TECHNICIAN.CA_TRACKDATA AS CA_TECHNICIAN ")
      _sb.AppendLine("        , STACKER_CHANGE.CA_TRACKDATA AS CA_STACKER_CHANGE ")
      _sb.AppendLine("        , EMPLOYEE.CA_TRACKDATA AS CA_EMPLOYEE")
    End If

    _sb.AppendLine("   FROM   GUI_USERS ")

    If Not GeneralParam.GetBoolean("MultiSite", "IsCenter") AndAlso _
    Not GeneralParam.GetBoolean("CashDesk.Draw", "IsCashDeskDraw") Then
      _sb.AppendLine(" LEFT JOIN  CARDS AS TECHNICIAN ")
      _sb.AppendLine("        ON  TECHNICIAN.CA_LINKED_ID = GU_USER_ID")
      _sb.AppendLine("       AND  TECHNICIAN.CA_LINKED_TYPE = 10")
      _sb.AppendLine(" LEFT JOIN  CARDS AS STACKER_CHANGE")
      _sb.AppendLine("        ON  STACKER_CHANGE.CA_LINKED_ID = GU_USER_ID")
      _sb.AppendLine("       AND  STACKER_CHANGE.CA_LINKED_TYPE = 12")
      _sb.AppendLine(" LEFT JOIN  CARDS AS EMPLOYEE")
      _sb.AppendLine("        ON  EMPLOYEE.CA_LINKED_ID = GU_USER_ID")
      _sb.AppendLine("       AND  EMPLOYEE.CA_LINKED_TYPE = 14")
    End If

    If WSI.Common.Misc.IsIntelliaEnabled() Then
      _sb.AppendLine(" LEFT JOIN  LAYOUT_USERS_CONFIGURATION AS LUC ")
      _sb.AppendLine("        ON  LUC.LC_USER_ID = GU_USER_ID")
    End If

    _sb.AppendLine("  WHERE   GU_USER_ID = " & pUser.user_id)

    data_table = GUI_GetTableUsingSQL(_sb.ToString(), 5000)
    If IsNothing(data_table) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    If data_table.Rows.Count() <> 1 Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    'Get user data row
    _data_row = data_table.Rows(0)

    pUser.profile_id = _data_row.Item("GU_PROFILE_ID")
    pUser.username = NullTrim(_data_row.Item("GU_USERNAME"))
    pUser.employee_code = _data_row.Item("GU_EMPLOYEE_CODE")

    '' XCD 14-Aug-2012 GU_ENABLED DEPRECATED
    'pUser.state = IIf(_data_row.Item("GU_ENABLED"), 1, 0)
    pUser.password_date.Value = _data_row.Item("GU_NOT_VALID_BEFORE")
    ' XCD 14-Aug-2012
    pUser.block_reason = _data_row.Item("GU_BLOCK_REASON")

    date_exp = _data_row.Item("GU_PASSWORD_EXP")
    If date_exp = "1900/01/01 00:00:00" Then
      pUser.password_exp = 0
    Else
      pUser.password_exp = date_exp.Subtract(pUser.password_date.Value).Days
    End If

    pUser.password_req = IIf(_data_row.Item("GU_PWD_CHG_REQ"), 1, 0)
    pUser.full_name = NullTrim(_data_row.Item("GU_FULL_NAME"))

    If (_data_row.IsNull("GU_SALES_LIMIT")) Then
      pUser.sales_limit_cashier = ""
    Else
      pUser.sales_limit_cashier = _data_row.Item("GU_SALES_LIMIT")
    End If

    If (_data_row.IsNull("GU_MB_SALES_LIMIT")) Then
      pUser.sales_limit_mb = ""
    Else
      pUser.sales_limit_mb = _data_row.Item("GU_MB_SALES_LIMIT")
    End If

    pUser.gu_master = _data_row.Item("GU_MASTER")
    pUser.gu_master_id = _data_row.Item("GU_MASTER_ID")

    pUser.card_number = String.Empty
    pUser.card_technician = String.Empty
    pUser.card_employee = String.Empty

    ' SGB 25-SEP-2014 These columns do not exist in multisite
    If Not GeneralParam.GetBoolean("MultiSite", "IsCenter") And _
    Not GeneralParam.GetBoolean("CashDesk.Draw", "IsCashDeskDraw") Then
      'CHANGE STACKER CARD
      If Not IsDBNull(_data_row.Item("CA_STACKER_CHANGE")) Then
        pUser.card_number = NullTrim(_data_row.Item("CA_STACKER_CHANGE"))
      End If

      'CHANGE TECHNICIAN
      If Not IsDBNull(_data_row.Item("CA_TECHNICIAN")) Then
        pUser.card_technician = NullTrim(_data_row.Item("CA_TECHNICIAN"))
      End If

      'EMPLOYEE
      If Not IsDBNull(_data_row.Item("CA_EMPLOYEE")) Then
        pUser.card_employee = NullTrim(_data_row.Item("CA_EMPLOYEE"))
      End If
    End If

    'SMARTFLOOR MANAGER
    If WSI.Common.Misc.IsIntelliaEnabled() Then
      If Not IsDBNull(_data_row.Item("LC_IS_MANAGER")) Then
        pUser.intellia_manager = _data_row.Item("LC_IS_MANAGER")
      End If

      If Not IsDBNull(_data_row.Item("LC_IS_RUNNER")) Then
        pUser.intellia_runner = _data_row.Item("LC_IS_RUNNER")
      End If
    End If

    'Password is never returned
    pUser.password = ""

    'Mobile Bank
    pUser.mobile_bank = New cls_mobile_bank()

    If (WSI.Common.MobileBankConfig.IsMobileBankLinkedToADevice) Then
      'Get MB data
      Dim _mb_id As Int64 = WSI.Common.MobileBank.DB_FindMBId(pUser.user_id)

      'Not found. It does not exists. Assign user id
      If (pUser.mobile_bank.DB_Read(_mb_id, Context) <> ENUM_STATUS.STATUS_OK) Then
        pUser.mobile_bank.UserId = pUser.user_id
      End If
    End If

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
  End Function

  ''' <summary>
  ''' Update user
  ''' </summary>
  ''' <param name="pUser"></param>
  ''' <param name="Context"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function UpdateUser(ByVal pUser As TYPE_USER, ByVal Context As Integer) As Integer
    Dim _sb As StringBuilder
    Dim _rc As Integer
    Dim _date_exp_string As String

    'XVV 16/04/2007
    'Assign Nothing to variable because compiler generate a warning
    Dim SqlTrans As System.Data.SqlClient.SqlTransaction = Nothing

    If Not GUI_BeginSQLTransaction(SqlTrans) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    _date_exp_string = GetExpirationDateAsString(pUser)

    _sb = New StringBuilder()

    ' XCD 14-Aug-2012 GU_ENABLED DEPRECATED. Inserted GU_BLOCK_REASON
    _sb.AppendLine("UPDATE   GUI_USERS ")
    _sb.AppendLine("   SET   GU_PROFILE_ID = " & pUser.profile_id)
    _sb.AppendLine("       , GU_NOT_VALID_BEFORE = " & GUI_FormatDateDB(pUser.password_date.Value))
    _sb.AppendLine("       , GU_NOT_VALID_AFTER = NULL")
    _sb.AppendLine("       , GU_PASSWORD_EXP = " & _date_exp_string)
    _sb.AppendLine("       , GU_PWD_CHG_REQ = " & pUser.password_req)
    _sb.AppendLine("       , GU_FULL_NAME = '" & pUser.full_name.Replace("'", "''") & "' ")
    _sb.AppendLine("       , GU_SALES_LIMIT = " & IIf(String.IsNullOrEmpty(pUser.sales_limit_cashier), "NULL", GUI_ParseCurrency(pUser.sales_limit_cashier)))
    _sb.AppendLine("       , GU_MB_SALES_LIMIT = " & IIf(String.IsNullOrEmpty(pUser.sales_limit_mb), "NULL", GUI_ParseCurrency(pUser.sales_limit_mb)))
    _sb.AppendLine("       , GU_BLOCK_REASON = " & pUser.block_reason)
    _sb.AppendLine("       , GU_EMPLOYEE_CODE = '" & pUser.employee_code & "' ")

    If m_original_block_reason <> WSI.Common.GUI_USER_BLOCK_REASON.NONE _
         And pUser.block_reason = WSI.Common.GUI_USER_BLOCK_REASON.NONE Then
      _sb.AppendLine("       , GU_LAST_ACTIVITY = GETDATE() ")
      _sb.AppendLine("       , GU_LAST_ACTION   = '" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1650) & "'")
    End If

    If pUser.gu_master Then
      _sb.AppendLine("       , GU_MASTER_ID  = " & pUser.user_id)
    Else
      _sb.AppendLine("       , GU_MASTER_ID  = NULL ")
    End If

    _sb.AppendLine(" WHERE   GU_USER_ID = " & pUser.user_id)

    'Can link cards?
    If (Not CheckCardsCanBeLinked(pUser, Context, SqlTrans)) Then
      GUI_EndSQLTransaction(SqlTrans, False)

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    If Len(pUser.password) > 0 Then
      If m_pwd_policy.PasswordRecentlyUsed(pUser.user_id, pUser.password, SqlTrans) Then
        _rc = ENUM_COMMON_RC.COMMON_ERROR_PASSWORD
      Else
        _rc = ENUM_COMMON_RC.COMMON_OK
      End If

      If _rc <> CInt(ENUM_COMMON_RC.COMMON_OK) Then
        GUI_EndSQLTransaction(SqlTrans, False)

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_PASSWORD
      End If

      If (Not String.IsNullOrEmpty(pUser.password)) Then
        If (Not m_pwd_policy.ChangeUserPassword(pUser.user_id, pUser.password, SqlTrans)) Then
          GUI_EndSQLTransaction(SqlTrans, False)

          GLB_NLS_GUI_CONFIGURATION.MsgBox(104, mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_PASSWORD
        End If
      End If
    End If

    If Not GUI_SQLExecuteNonQuery(_sb.ToString(), SqlTrans) Then
      GUI_EndSQLTransaction(SqlTrans, False)

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    If WSI.Common.Misc.IsIntelliaEnabled() Then
      If Not Trx_SaveUserIntelliaInfo(pUser, SqlTrans) Then

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If
    End If

    'Save mobile bank
    If (WSI.Common.MobileBankConfig.IsMobileBankLinkedToADevice) Then
      If (Not SaveMobileBankForUser(pUser, Context)) Then
        GUI_EndSQLTransaction(SqlTrans, False)

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If
    End If

    If Not GUI_EndSQLTransaction(SqlTrans, True) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
  End Function

  ''' <summary>
  ''' Save mobile bank data for user
  ''' </summary>
  ''' <param name="pUser"></param>
  ''' <param name="pContext"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function SaveMobileBankForUser(ByRef pUser As TYPE_USER, ByVal pContext As Integer) As Boolean
    Dim _status As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    If (pUser.mobile_bank Is Nothing) Then
      Return True
    End If

    pUser.mobile_bank.HolderName = pUser.full_name
    pUser.mobile_bank.EmployeeCode = pUser.employee_code

    If (pUser.mobile_bank.ID = 0) Then
      If (pUser.mobile_bank.UserId = 0) Then
        pUser.mobile_bank.UserId = pUser.user_id
      End If

      _status = pUser.mobile_bank.DB_Insert(pContext)
    Else
      _status = pUser.mobile_bank.DB_Update(pContext)
    End If

    Return (_status = ENUM_STATUS.STATUS_OK)
  End Function

  ''' <summary>
  ''' Delete mobile bank data for user
  ''' </summary>
  ''' <param name="pUser"></param>
  ''' <param name="pContext"></param>
  ''' <param name="pSqlTrans"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function DeleteMobileBankForUser(ByRef pUser As TYPE_USER, ByVal pContext As Integer) As Boolean
    Dim _status As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    If ((pUser.mobile_bank Is Nothing) OrElse (pUser.mobile_bank.ID = 0)) Then
      Return True
    End If

    _status = pUser.mobile_bank.DB_Delete(pContext)

    Return (_status = ENUM_STATUS.STATUS_OK)
  End Function

  ''' <summary>
  ''' Get expiration date as string
  ''' </summary>
  ''' <param name="pUser"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetExpirationDateAsString(ByVal pUser As TYPE_USER) As String
    Dim date_exp As Date

    If (pUser.password_exp > 0) Then
      date_exp = pUser.password_date.Value
      date_exp = date_exp.AddDays(pUser.password_exp)
      date_exp = date_exp.Date

      Return GUI_FormatDateDB(date_exp)
    Else
      Return "NULL"
    End If
  End Function

  ''' <summary>
  ''' Can link cards?
  ''' </summary>
  ''' <param name="pUser"></param>
  ''' <param name="Context"></param>
  ''' <param name="SqlTrans"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function CheckCardsCanBeLinked(ByVal pUser As TYPE_USER, _
                                         ByVal Context As Integer, _
                                         ByVal SqlTrans As System.Data.SqlClient.SqlTransaction) As Boolean
    ' SGB 25-SEP-2014 These columns do not exist in multisite
    If (Not GeneralParam.GetBoolean("MultiSite", "IsCenter")) And _
       (Not GeneralParam.GetBoolean("CashDesk.Draw", "IsCashDeskDraw")) Then
      Dim _current_user As New TYPE_USER
      Dim _rc As Integer

      _current_user.user_id = pUser.user_id
      _rc = ReadUser(_current_user, Context)

      If ((_current_user.card_number <> pUser.card_number) AndAlso (Not CardData.DB_LinkCard(pUser.card_number, WCP_CardTypes.CARD_TYPE_CHANGE_STACKER, pUser.user_id, SqlTrans))) Then
        Return False
      End If

      If ((_current_user.card_technician <> pUser.card_technician) AndAlso (Not CardData.DB_LinkCard(pUser.card_technician, WCP_CardTypes.CARD_TYPE_TECH, pUser.user_id, SqlTrans))) Then
        Return False
      End If

      If ((_current_user.card_employee <> pUser.card_employee) AndAlso (Not CardData.DB_LinkCard(pUser.card_employee, WCP_CardTypes.CARD_TYPE_EMPLOYEE, pUser.user_id, SqlTrans))) Then
        Return False
      End If
    End If

    Return True
  End Function

  Private Function Trx_SaveUserIntelliaInfo(User As CLASS_USER.TYPE_USER, Trx As SqlTransaction)
    Try
      Using _cmd As New SqlCommand("Layout_SaveUser", Trx.Connection, Trx)

        With _cmd
          .CommandType = CommandType.StoredProcedure
          .CommandTimeout = 2 * 60 '2 min
          .Parameters.Add("@pUserId", SqlDbType.Int).Value = User.user_id
          .Parameters.Add("@pIsManager", SqlDbType.Bit).Value = User.intellia_manager
          .Parameters.Add("@pIsRunner", SqlDbType.Bit).Value = User.intellia_runner
        End With

        If Not _cmd.ExecuteNonQuery() > 0 Then

          Return False
        End If
      End Using

      Return True
    Catch _ex As Exception

      Log.Message(_ex.Message)
    End Try

    Return False
  End Function

  Private Function InsertUser(ByVal pUser As TYPE_USER, _
                              ByVal Context As Integer) As Integer
    Dim _sb As StringBuilder
    Dim _num_rows_inserted As Integer
    Dim _rc As Integer
    Dim _db_count As Integer = 0
    Dim _db_user_id As Integer

    'XVV 16/04/2007
    'Assign Nothing to variable because compiler generate a warning
    Dim _sql_trans As System.Data.SqlClient.SqlTransaction = Nothing

    Dim _date_exp As Date
    Dim _data_table As DataTable

    Try
      If Not GUI_BeginSQLTransaction(_sql_trans) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      _sb = New StringBuilder()
      _sb.AppendLine(" SELECT   COUNT(*) ")
      _sb.AppendLine("   FROM   GUI_USERS ")
      _sb.AppendLine("  WHERE   GU_USERNAME = '" & pUser.username & "'")

      _data_table = GUI_GetTableUsingSQL(_sb.ToString(), 5000)
      If IsNothing(_data_table) Then
        GUI_EndSQLTransaction(_sql_trans, False)

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      _db_count = _data_table.Rows(0).Item(0)

      If _db_count > 0 Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
      End If

      _sb = New StringBuilder()
      _sb.AppendLine(" SELECT   MAX(ISNULL(GU_USER_ID, 0)) + 1 ")
      _sb.AppendLine("   FROM   GUI_USERS")

      _data_table = GUI_GetTableUsingSQL(_sb.ToString(), 5000)
      If IsNothing(_data_table) Then
        GUI_EndSQLTransaction(_sql_trans, False)

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      _db_user_id = _data_table.Rows(0).Item(0)
      pUser.user_id = _db_user_id

      ' HBB 08/10/2014
      '1- if there are not specified expiration days, expiration date is not created
      '2- the expiration date is created from the validation number + expiration days
      If pUser.password_exp > 0 Then
        _date_exp = pUser.password_date.Value.AddDays(pUser.password_exp)
      End If

      ' XCD 16-Aug-2012 GU_ENABLED is deprecated but is necessary to insert user to database.
      _sb = New StringBuilder()
      _sb.AppendLine("INSERT INTO   GUI_USERS ")
      _sb.AppendLine(" ( ")
      _sb.AppendLine("   GU_USER_ID ")
      _sb.AppendLine(" , GU_PROFILE_ID ")
      _sb.AppendLine(" , GU_USERNAME ")
      _sb.AppendLine(" , GU_ENABLED ")
      _sb.AppendLine(" , GU_PASSWORD ")
      _sb.AppendLine(" , GU_NOT_VALID_BEFORE ")
      _sb.AppendLine(" , GU_NOT_VALID_AFTER ")
      _sb.AppendLine(" , GU_LAST_CHANGED ")
      _sb.AppendLine(" , GU_PASSWORD_EXP ")
      _sb.AppendLine(" , GU_PWD_CHG_REQ ")
      _sb.AppendLine(" , GU_FULL_NAME ")
      _sb.AppendLine(" , GU_LOGIN_FAILURES ")
      _sb.AppendLine(" , GU_SALES_LIMIT ")
      _sb.AppendLine(" , GU_MB_SALES_LIMIT ")
      _sb.AppendLine(" , GU_BLOCK_REASON ")
      _sb.AppendLine(" , GU_LAST_ACTIVITY ")
      _sb.AppendLine(" , GU_LAST_ACTION ")
      _sb.AppendLine(" , GU_MASTER_ID ")
      _sb.AppendLine(" , GU_EMPLOYEE_CODE ")
      _sb.AppendLine(" ) ")
      _sb.AppendLine(" VALUES ")
      _sb.AppendLine(" ( ")
      _sb.AppendLine("   @pUserId")
      _sb.AppendLine(" , @pUserProfileId")
      _sb.AppendLine(" , @pUserName")
      _sb.AppendLine(" , @pEnabled")
      _sb.AppendLine(" , CAST('00' AS binary(40))")
      _sb.AppendLine(" , @pUserPasswordDate")
      _sb.AppendLine(" , NULL")
      _sb.AppendLine(" , GETDATE() ")
      _sb.AppendLine(" , @pDateExp")
      _sb.AppendLine(" , @pUserPasswordReq")
      _sb.AppendLine(" , @pUserFullName")
      _sb.AppendLine(" , @pLoginFailures")
      _sb.AppendLine(" , @pUserSalesLimitCashier")
      _sb.AppendLine(" , @pUserSalesLimitMb")
      _sb.AppendLine(" , @pUserBlockReason")
      _sb.AppendLine(" , GETDATE() ")
      _sb.AppendLine(" , @pNLS")
      _sb.AppendLine(" , @pUserMaster")
      _sb.AppendLine(" , @pUserEmpolyeeCode")
      _sb.AppendLine(" )")

      Using _sql_command As SqlCommand = New SqlCommand(_sb.ToString(), _sql_trans.Connection, _sql_trans)
        _sql_command.Parameters.Add("@pUserId", SqlDbType.Int).Value = pUser.user_id
        _sql_command.Parameters.Add("@pUserProfileId", SqlDbType.Int).Value = pUser.profile_id
        _sql_command.Parameters.Add("@pUserName", SqlDbType.NVarChar).Value = pUser.username
        _sql_command.Parameters.Add("@pEnabled", SqlDbType.Bit).Value = 0
        _sql_command.Parameters.Add("@pUserPasswordDate", SqlDbType.DateTime).Value = pUser.password_date.Value
        _sql_command.Parameters.Add("@pDateExp", SqlDbType.DateTime).Value = IIf(pUser.password_exp = 0, DBNull.Value, _date_exp)
        _sql_command.Parameters.Add("@pUserPasswordReq", SqlDbType.Bit).Value = pUser.password_req
        _sql_command.Parameters.Add("@pUserFullName", SqlDbType.NVarChar).Value = pUser.full_name
        _sql_command.Parameters.Add("@pLoginFailures", SqlDbType.Int).Value = 0
        _sql_command.Parameters.Add("@pUserSalesLimitCashier", SqlDbType.Money).Value = IIf(String.IsNullOrEmpty(pUser.sales_limit_cashier), DBNull.Value, GUI_ParseCurrency(pUser.sales_limit_cashier))
        _sql_command.Parameters.Add("@pUserSalesLimitMb", SqlDbType.Money).Value = IIf(String.IsNullOrEmpty(pUser.sales_limit_mb), DBNull.Value, GUI_ParseCurrency(pUser.sales_limit_mb))
        _sql_command.Parameters.Add("@pUserBlockReason", SqlDbType.Int).Value = pUser.block_reason
        _sql_command.Parameters.Add("@pNLS", SqlDbType.NVarChar).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1653)
        _sql_command.Parameters.Add("@pUserMaster", SqlDbType.Int).Value = IIf(pUser.gu_master, pUser.user_id, DBNull.Value)
        _sql_command.Parameters.Add("@pUserEmpolyeeCode", SqlDbType.NVarChar).Value = IIf(String.IsNullOrEmpty(pUser.employee_code), DBNull.Value, pUser.employee_code)

        ' SGB 25-SEP-2014 These columns do not exist in multisite
        If Not GeneralParam.GetBoolean("MultiSite", "IsCenter") AndAlso _
           Not GeneralParam.GetBoolean("CashDesk.Draw", "IsCashDeskDraw") Then

          If Not String.IsNullOrEmpty(pUser.card_number) Then
            If Not CardData.DB_LinkCard(pUser.card_number, WCP_CardTypes.CARD_TYPE_CHANGE_STACKER, pUser.user_id, _sql_trans) Then
              Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
            End If
          End If

          If Not String.IsNullOrEmpty(pUser.card_technician) Then
            If Not CardData.DB_LinkCard(pUser.card_technician, WCP_CardTypes.CARD_TYPE_TECH, pUser.user_id, _sql_trans) Then
              Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
            End If
          End If

          If Not String.IsNullOrEmpty(pUser.card_employee) Then
            If Not CardData.DB_LinkCard(pUser.card_employee, WCP_CardTypes.CARD_TYPE_EMPLOYEE, pUser.user_id, _sql_trans) Then
              Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
            End If
          End If
        End If

        _num_rows_inserted = _sql_command.ExecuteNonQuery()
      End Using

      If _num_rows_inserted <> 1 Then
        GUI_EndSQLTransaction(_sql_trans, False)

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      _rc = CLASS_GUI_USER.Common_SetNewPassword(pUser.user_id, m_pwd_policy.ParsePassword(pUser.password), _sql_trans)
      If _rc <> CInt(ENUM_COMMON_RC.COMMON_OK) Then
        'package or database error, password inserting fails
        GLB_NLS_GUI_CONFIGURATION.MsgBox(104, mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        GUI_EndSQLTransaction(_sql_trans, False)

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_PASSWORD
      End If

      If WSI.Common.Misc.IsIntelliaEnabled() Then
        If Not Trx_SaveUserIntelliaInfo(pUser, _sql_trans) Then

          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If
      End If

      'Save mobile bank
      If (WSI.Common.MobileBankConfig.IsMobileBankLinkedToADevice) Then
        If (Not SaveMobileBankForUser(pUser, Context)) Then
          GUI_EndSQLTransaction(_sql_trans, False)

          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If
      End If

      If Not GUI_EndSQLTransaction(_sql_trans, True) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

    Catch ex As Exception
      ' Rollback  
      GUI_EndSQLTransaction(_sql_trans, False)

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
  End Function

  Private Function DeleteUser(ByVal pUser As TYPE_USER, _
                              ByVal Context As Integer) As Integer
    Dim _sb As StringBuilder
    Dim db_count As Integer = 0

    'XVV 16/04/2007
    'Assign Nothing to variable because compiler generate a warning
    Dim _sql_trans As System.Data.SqlClient.SqlTransaction = Nothing

    Dim data_table As DataTable

    If Not GUI_BeginSQLTransaction(_sql_trans) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    'Search if user has been used
    _sb = New StringBuilder()
    _sb.AppendLine(" SELECT   COUNT(*) ")
    _sb.AppendLine("   FROM   GUI_AUDIT ")
    _sb.AppendLine("  WHERE   GA_GUI_USER_ID = " & pUser.user_id)

    data_table = GUI_GetTableUsingSQL(_sb.ToString(), 5000)
    If IsNothing(data_table) Then
      GUI_EndSQLTransaction(_sql_trans, False)

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    db_count = data_table.Rows(0).Item(0)

    'User has been used?
    If (db_count > 0) Then
      GUI_EndSQLTransaction(_sql_trans, False)

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DEPENDENCIES
    End If

    _sb = New StringBuilder()
    _sb.AppendLine(" DELETE   GUI_USERS ")
    _sb.AppendLine("  WHERE   GU_USER_ID = " & pUser.user_id)

    If Not GUI_SQLExecuteNonQuery(_sb.ToString(), _sql_trans) Then
      GUI_EndSQLTransaction(_sql_trans, False)

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    If WSI.Common.Misc.IsIntelliaEnabled() Then
      _sb = New StringBuilder()
      _sb.AppendLine(" DELETE   FROM LAYOUT_USERS_CONFIGURATION ")
      _sb.AppendLine("  WHERE   LC_USER_ID = " & pUser.user_id)

      If Not GUI_SQLExecuteNonQuery(_sb.ToString(), _sql_trans) Then
        GUI_EndSQLTransaction(_sql_trans, False)

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If
    End If

    'Delete mobile bank
    If (WSI.Common.MobileBankConfig.IsMobileBankLinkedToADevice) Then
      If (Not DeleteMobileBankForUser(pUser, Context)) Then
        GUI_EndSQLTransaction(_sql_trans, False)

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If
    End If

    If Not GUI_EndSQLTransaction(_sql_trans, True) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
  End Function

  Private Function getHolderNameByLinkedIDAndType(LinkedID As Int64, CardType As WCP_CardTypes) As String
    Dim _sb As StringBuilder
    Dim _data As Object

    _sb = New StringBuilder()

    _sb.Append("SELECT GU_FULL_NAME FROM GUI_USERS WHERE GU_USER_ID = @pId")

    Using _db_trx As New DB_TRX()
      Using _sql_cmd As New SqlClient.SqlCommand(_sb.ToString, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
        _sql_cmd.Parameters.Add("@pId", SqlDbType.BigInt).Value = LinkedID

        _data = _sql_cmd.ExecuteScalar()

        If Not IsNothing(_data) Then
          Return CType(_data, String)
        End If

      End Using
    End Using

    Return String.Empty
  End Function
#End Region

End Class
