'-------------------------------------------------------------------
' Copyright � 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_mailing_parameters.vb
'
' DESCRIPTION:   Mailing Programming class
'
' AUTHOR:        Ra�l Cervera
'
' CREATION DATE: 04-JAN-2011
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 04-JAN-2011  RCI    Initial version.
' 14-OCT-2016  RAB    PBI 18098: General params: Automatically add the GP to the system
'--------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient

Public Class CLASS_MAILING_PARAMETERS
  Inherits CLASS_BASE

#Region " Constants "

#End Region ' Constants

#Region "Structure"

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_MAILING_PARAMETERS
    Public authorized_server_list As String
    Public enabled As Boolean
    Public smtp_enabled As Boolean
    Public smtp_server As String
    Public smtp_port As Integer
    Public smtp_secured As Boolean
    Public smtp_username As String
    Public smtp_password As String
    Public smtp_domain As String
    Public smtp_email_from As String
  End Class

#End Region ' Structure

#Region "Constructor / Destructor"

  Public Sub New()
    Call MyBase.New()
  End Sub

  Protected Overrides Sub Finalize()
    MyBase.Finalize()

  End Sub

#End Region ' Constructor / Destructor

#Region " Members "

  Protected m_mailing_parameters As New TYPE_MAILING_PARAMETERS

#End Region ' Members

#Region "Properties"

  Public Overloads Property AuthorizedServerList() As String
    Get
      Return m_mailing_parameters.authorized_server_list
    End Get

    Set(ByVal Value As String)
      m_mailing_parameters.authorized_server_list = Value
    End Set

  End Property

  Public Overloads Property MailingEnabled() As Boolean
    Get
      Return m_mailing_parameters.enabled
    End Get

    Set(ByVal Value As Boolean)
      m_mailing_parameters.enabled = Value
    End Set

  End Property

  Public Overloads Property SmtpEnabled() As Boolean
    Get
      Return m_mailing_parameters.smtp_enabled
    End Get

    Set(ByVal Value As Boolean)
      m_mailing_parameters.smtp_enabled = Value
    End Set

  End Property

  Public Overloads Property SmtpServer() As String
    Get
      Return m_mailing_parameters.smtp_server
    End Get

    Set(ByVal Value As String)
      m_mailing_parameters.smtp_server = Value
    End Set

  End Property

  Public Overloads Property SmtpPort() As Integer
    Get
      Return m_mailing_parameters.smtp_port
    End Get

    Set(ByVal Value As Integer)
      m_mailing_parameters.smtp_port = Value
    End Set

  End Property

  Public Overloads Property SmtpSecured() As Boolean
    Get
      Return m_mailing_parameters.smtp_secured
    End Get

    Set(ByVal Value As Boolean)
      m_mailing_parameters.smtp_secured = Value
    End Set

  End Property

  Public Overloads Property SmtpUsername() As String
    Get
      Return m_mailing_parameters.smtp_username
    End Get

    Set(ByVal Value As String)
      m_mailing_parameters.smtp_username = Value
    End Set

  End Property

  Public Overloads Property SmtpPassword() As String
    Get
      Return m_mailing_parameters.smtp_password
    End Get

    Set(ByVal Value As String)
      m_mailing_parameters.smtp_password = Value
    End Set

  End Property

  Public Overloads Property SmtpDomain() As String
    Get
      Return m_mailing_parameters.smtp_domain
    End Get

    Set(ByVal Value As String)
      m_mailing_parameters.smtp_domain = Value
    End Set

  End Property

  Public Overloads Property SmtpEmailFrom() As String
    Get
      Return m_mailing_parameters.smtp_email_from
    End Get

    Set(ByVal Value As String)
      m_mailing_parameters.smtp_email_from = Value
    End Set

  End Property

#End Region ' Properties

#Region " Overrides functions "

  '----------------------------------------------------------------------------
  ' PURPOSE: Reads from the database into the given object
  '
  ' PARAMS:
  '   - INPUT:
  '     - ObjectId: An object, it must be 'casted' to the desired KEY.
  '
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As ENUM_STATUS

    Dim rc As Integer

    ' Read Mailing Parameters
    rc = MailingParams_DbRead(SqlCtx)

    Select Case rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function 'DB_Read

  '----------------------------------------------------------------------------
  ' PURPOSE: Updates the given object to the database.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As ENUM_STATUS

    Dim rc As Integer

    ' Update Mailing Parameters
    rc = MailingParams_DbUpdate()

    Select Case rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select
  End Function 'DB_Update

  '----------------------------------------------------------------------------
  ' PURPOSE: Can't insert.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
  End Function ' DB_Insert

  '----------------------------------------------------------------------------
  ' PURPOSE: Can't delete.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
  End Function ' DB_Delete

  '----------------------------------------------------------------------------
  ' PURPOSE: Returns a duplicate of the given object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim temp_params As CLASS_MAILING_PARAMETERS

    temp_params = New CLASS_MAILING_PARAMETERS
    temp_params.m_mailing_parameters.authorized_server_list = Me.m_mailing_parameters.authorized_server_list
    temp_params.m_mailing_parameters.enabled = Me.m_mailing_parameters.enabled
    temp_params.m_mailing_parameters.smtp_enabled = Me.m_mailing_parameters.smtp_enabled
    temp_params.m_mailing_parameters.smtp_server = Me.m_mailing_parameters.smtp_server
    temp_params.m_mailing_parameters.smtp_port = Me.m_mailing_parameters.smtp_port
    temp_params.m_mailing_parameters.smtp_secured = Me.m_mailing_parameters.smtp_secured
    temp_params.m_mailing_parameters.smtp_username = Me.m_mailing_parameters.smtp_username
    temp_params.m_mailing_parameters.smtp_password = Me.m_mailing_parameters.smtp_password
    temp_params.m_mailing_parameters.smtp_domain = Me.m_mailing_parameters.smtp_domain
    temp_params.m_mailing_parameters.smtp_email_from = Me.m_mailing_parameters.smtp_email_from

    Return temp_params

  End Function 'Duplicate 

  '----------------------------------------------------------------------------
  ' PURPOSE: Returns the related auditor data for the current object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim auditor_data As CLASS_AUDITOR_DATA
    Dim field_name As String
    Dim field_value As String

    auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_MAILING_PROGRAMMING)

    auditor_data.SetName(GLB_NLS_GUI_STATISTICS.Id(438), "")

    field_name = GLB_NLS_GUI_CONFIGURATION.GetString(412)
    Call auditor_data.SetField(0, AuthorizedServerList, field_name)

    field_value = ""
    Select Case MailingEnabled
      Case False
        field_value = GLB_NLS_GUI_CONFIGURATION.GetString(265)
      Case True
        field_value = GLB_NLS_GUI_CONFIGURATION.GetString(264)
    End Select
    Call auditor_data.SetField(0, field_value, GLB_NLS_GUI_CONFIGURATION.GetString(396))

    field_value = ""
    Select Case SmtpEnabled
      Case False
        field_value = GLB_NLS_GUI_CONFIGURATION.GetString(265)
      Case True
        field_value = GLB_NLS_GUI_CONFIGURATION.GetString(264)
    End Select
    Call auditor_data.SetField(0, field_value, GLB_NLS_GUI_CONFIGURATION.GetString(397))

    field_name = GLB_NLS_GUI_CONFIGURATION.GetString(407) & "." & GLB_NLS_GUI_CONFIGURATION.GetString(400)
    Call auditor_data.SetField(0, SmtpServer, field_name)

    field_name = GLB_NLS_GUI_CONFIGURATION.GetString(407) & "." & GLB_NLS_GUI_CONFIGURATION.GetString(401)
    Call auditor_data.SetField(0, SmtpPort, field_name)

    field_name = GLB_NLS_GUI_CONFIGURATION.GetString(407) & "." & GLB_NLS_GUI_CONFIGURATION.GetString(402)
    field_value = ""
    Select Case SmtpSecured
      Case False
        field_value = GLB_NLS_GUI_CONFIGURATION.GetString(265)
      Case True
        field_value = GLB_NLS_GUI_CONFIGURATION.GetString(264)
    End Select
    Call auditor_data.SetField(0, field_value, field_name)

    field_name = GLB_NLS_GUI_CONFIGURATION.GetString(407) & "." & GLB_NLS_GUI_CONFIGURATION.GetString(403)
    Call auditor_data.SetField(0, SmtpUsername, field_name)

    field_name = GLB_NLS_GUI_CONFIGURATION.GetString(407) & "." & GLB_NLS_GUI_CONFIGURATION.GetString(404)
    Call auditor_data.SetField(0, WSI.Common.Misc.StringToHex(SmtpPassword), field_name)

    field_name = GLB_NLS_GUI_CONFIGURATION.GetString(407) & "." & GLB_NLS_GUI_CONFIGURATION.GetString(405)
    Call auditor_data.SetField(0, SmtpDomain, field_name)

    field_name = GLB_NLS_GUI_CONFIGURATION.GetString(407) & "." & GLB_NLS_GUI_CONFIGURATION.GetString(406)
    Call auditor_data.SetField(0, SmtpEmailFrom, field_name)

    Return auditor_data

  End Function 'AuditorData

#End Region 'Overrides functions

#Region "DB Functions"

  Private Function MailingParams_DbRead(ByVal Context As Integer) As Integer
    Dim _general_params As WSI.Common.GeneralParam.Dictionary
    Dim _password_hex As String

    ' Populate General Params dictionary with database values
    _general_params = WSI.Common.GeneralParam.Dictionary.Create()

    m_mailing_parameters.authorized_server_list = _general_params.GetString("Mailing", "AuthorizedServerList")
    m_mailing_parameters.enabled = _general_params.GetBoolean("Mailing", "Enabled")
    m_mailing_parameters.smtp_enabled = _general_params.GetBoolean("Mailing", "SMTP.Enabled")
    m_mailing_parameters.smtp_server = _general_params.GetString("Mailing", "SMTP.Connection.ServerAddress")
    m_mailing_parameters.smtp_port = _general_params.GetInt32("Mailing", "SMTP.Connection.ServerPort", 25)
    m_mailing_parameters.smtp_secured = _general_params.GetBoolean("Mailing", "SMTP.Connection.Secured")
    m_mailing_parameters.smtp_username = _general_params.GetString("Mailing", "SMTP.Credentials.UserName")

    _password_hex = _general_params.GetString("Mailing", "SMTP.Credentials.Password")
    Try
      m_mailing_parameters.smtp_password = WSI.Common.Misc.HexToString(_password_hex)
    Catch
      m_mailing_parameters.smtp_password = ""
    End Try

    m_mailing_parameters.smtp_domain = _general_params.GetString("Mailing", "SMTP.Credentials.Domain")
    m_mailing_parameters.smtp_email_from = _general_params.GetString("Mailing", "SMTP.Credentials.EMailAddress")

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

  End Function ' MailingParams_DbRead

  Private Function MailingParams_DbUpdate() As Integer

    Try
      Using _db_trx As New WSI.Common.DB_TRX()

        If Not GUI_BeginSQLTransaction(_db_trx.SqlTransaction) Then

          _db_trx.SqlTransaction.Rollback()
          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

        Dim _gp_update_list As New List(Of String())

        _gp_update_list.Add(WSI.Common.GeneralParam.AddToList("Mailing", "AuthorizedServerList", m_mailing_parameters.authorized_server_list))
        _gp_update_list.Add(WSI.Common.GeneralParam.AddToList("Mailing", "Enabled", IIf(m_mailing_parameters.enabled, "1", "0")))
        _gp_update_list.Add(WSI.Common.GeneralParam.AddToList("Mailing", "SMTP.Enabled", IIf(m_mailing_parameters.smtp_enabled, "1", "0")))
        _gp_update_list.Add(WSI.Common.GeneralParam.AddToList("Mailing", "SMTP.Connection.ServerAddress", m_mailing_parameters.smtp_server))
        _gp_update_list.Add(WSI.Common.GeneralParam.AddToList("Mailing", "SMTP.Connection.ServerPort", m_mailing_parameters.smtp_port.ToString()))
        _gp_update_list.Add(WSI.Common.GeneralParam.AddToList("Mailing", "SMTP.Connection.Secured", IIf(m_mailing_parameters.smtp_secured, "1", "0")))
        _gp_update_list.Add(WSI.Common.GeneralParam.AddToList("Mailing", "SMTP.Credentials.UserName", m_mailing_parameters.smtp_username))
        _gp_update_list.Add(WSI.Common.GeneralParam.AddToList("Mailing", "SMTP.Credentials.Password", WSI.Common.Misc.StringToHex(m_mailing_parameters.smtp_password)))
        _gp_update_list.Add(WSI.Common.GeneralParam.AddToList("Mailing", "SMTP.Credentials.Domain", m_mailing_parameters.smtp_domain))
        _gp_update_list.Add(WSI.Common.GeneralParam.AddToList("Mailing", "SMTP.Credentials.EMailAddress", m_mailing_parameters.smtp_email_from))

        For Each _general_param As String() In _gp_update_list
          If (Not GUI_Controls.DB_GeneralParam_Update(_general_param(WSI.Common.GeneralParam.COLUMN_NAME.Group),
                                                      _general_param(WSI.Common.GeneralParam.COLUMN_NAME.Subject),
                                                      _general_param(WSI.Common.GeneralParam.COLUMN_NAME.Value),
                                                      _db_trx.SqlTransaction)) Then

            Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
          End If
        Next

        If Not GUI_EndSQLTransaction(_db_trx.SqlTransaction, True) Then
          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If
      End Using

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

    Catch ex As Exception

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

  End Function ' MailingParams_DbUpdate


#End Region ' DB Functions

End Class ' CLASS_MAILING_PARAMETERS
