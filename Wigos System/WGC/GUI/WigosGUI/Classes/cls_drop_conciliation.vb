﻿'-------------------------------------------------------------------
' Copyright © 2015 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_drop_conciliation
' DESCRIPTION:   Conciliation Drop
' AUTHOR:        José Martínez
' CREATION DATE: 24-JUN-2014
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 23-JUN-2014  JML    Initial version   
' 06-APR-2017  RAB    Bug 26621: Delete chips conciliation error on WigosGUI
' 20-APR-2017  DHA    PBI 26808: MES10 Ticket validation - Ticket conciliation
' 26-APR-2017  JML    PBI 26808: MES10 Ticket validation - Ticket conciliation
' 26-APR-2017  JML    PBI 26810: MES10 Ticket validation - Ticket conciliation action (without gaming table validation)
' 02-MAY-2017  JML    Fixed Bug 27159:Cancel conciliation when the tickets have been scanned, the Chip conciliation form is closed without the pop-up showed "are you sure cancel..."
' 03-MAY-2017  JML    PBI 27170: MES10 Ticket validation - Retrocompatibility
' 08-MAY-2017  JML    PBI 27174: Drop gaming table information - Conciliation parameter
' 07-AUG-2017  RAB    Bug 29200: WIGOS-3981 Tables - Conciliation: The description of the status on alarm action triggered is missing when dealer copy ticket is not valid
' 04-MAY-2018  AGS    Bug 32552:WIGOS-8179 [Ticket #12450] Reporte de Compra Venta de Fichas Duplica ventas en Caja V03.06.0035
' 05-JUN-2018  AGS		Bug 32889:WIGOS-12142, WIGOS-12143, WIGOS-12146, WIGOS-12148 - Drop mesas
' 16-JUN-2018  AGS    Bug 33092:WIGOS-13017 Gambling tables: It is not possible to conciliate the dealer copy tickets in gambling table session when previously it has had the deleted conciliation.
'--------------------------------------------------------------------

#Region "Imports "

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports WSI.Common
Imports System.Text
Imports System.IO
Imports System.Xml.Serialization
Imports System.ComponentModel
Imports System.Globalization

#End Region

Public Class CLASS_DROP_CONCILIATION
  Inherits CLASS_BASE

#Region " Enum "
  Public Enum CONCILIATION_STATUS
    OK = 0
    REPEATED_IN_THIS_RECONCILIATION = 1
    REPEATED_IN_OTHER_RECONCILIATION = 2
    TICKET_CANCELLED = 3
    NOT_EXISTS = 4
    PENDING = 5
    OK_OVER_AMOUNT = 6
  End Enum
#End Region       ' Enum

#Region " Constants "

  'SQL Query Columns
  Private Const SQL_COL_VALID_TICKET_TICKET_ID As Integer = 0
  Private Const SQL_COL_VALID_TICKET_VALIDATION_NUM As Integer = 1
  Private Const SQL_COL_VALID_TICKET_AMOUNT As Integer = 2
  Private Const SQL_COL_VALID_TICKET_AMOUNT_ISSUED As Integer = 3
  Private Const SQL_COL_VALID_TICKET_ISOCODE_ISSUED As Integer = 4
  Private Const SQL_COL_VALID_TICKET_CREATION_DATE As Integer = 5
  Private Const SQL_COL_VALID_TICKET_COLLECTED As Integer = 6

#End Region  ' Constants 

#Region " Structures "

  <StructLayout(LayoutKind.Sequential), XmlType(TypeName:="VOUCHER_RECONCILIATION")> _
  Public Class TYPE_VOUCHER_RECONCILIATION
    Private table_session_id As Int64
    Private table_name As String
    Private sales_amount_list As New List(Of TYPE_TOTAL_CURRENCIES)
    Private open_sesion_date As Date
    Private has_conciliate As Boolean
    Private vouchers_list As New List(Of TYPE_RECONCILED_VOUCHER)
    Private validated_tickets_list As New List(Of TYPE_VALIDATED_TICKETS)

    ' Constructor
    Sub New()
      Me.table_session_id = 0
      Me.table_name = ""
      Me.sales_amount_list = New List(Of TYPE_TOTAL_CURRENCIES)
      Me.open_sesion_date = Date.MinValue
      Me.has_conciliate = False
      Me.vouchers_list = New List(Of TYPE_RECONCILED_VOUCHER)
      Me.validated_tickets_list = New List(Of TYPE_VALIDATED_TICKETS)
    End Sub

#Region " Properties "

    <XmlElement(ElementName:="gt_session_id")> _
    Public Property TableSessionId() As Int64
      Get
        Return table_session_id
      End Get
      Set(ByVal value As Int64)
        table_session_id = value
      End Set
    End Property

    <XmlElement(ElementName:="gt_name")> _
    Public Property TableName() As String
      Get
        Return table_name
      End Get
      Set(ByVal value As String)
        table_name = value
      End Set
    End Property

    <XmlElement(ElementName:="gt_sales")> _
    Public Property SalesAmounts() As List(Of TYPE_TOTAL_CURRENCIES)
      Get
        Return sales_amount_list
      End Get
      Set(ByVal value As List(Of TYPE_TOTAL_CURRENCIES))
        sales_amount_list = value
      End Set
    End Property

    <XmlElement(ElementName:="gt_session_date")> _
    Public Property OpenSessionDate() As Date
      Get
        Return open_sesion_date
      End Get
      Set(ByVal value As Date)
        open_sesion_date = value
      End Set
    End Property

    <System.Xml.Serialization.XmlIgnore> _
    Public Property HasConciliate() As Boolean
      Get
        Return has_conciliate
      End Get
      Set(ByVal value As Boolean)
        has_conciliate = value
      End Set
    End Property

    Public Property Vouchers() As List(Of TYPE_RECONCILED_VOUCHER)
      Get
        Return vouchers_list
      End Get

      Set(ByVal value As List(Of TYPE_RECONCILED_VOUCHER))
        vouchers_list = value
      End Set

    End Property

    <System.Xml.Serialization.XmlIgnore> _
    Public Property ValidatedTickets() As List(Of TYPE_VALIDATED_TICKETS)
      Get
        Return validated_tickets_list
      End Get

      Set(ByVal value As List(Of TYPE_VALIDATED_TICKETS))
        validated_tickets_list = value
      End Set

    End Property

#End Region ' Properties

  End Class

  <StructLayout(LayoutKind.Sequential), XmlType(TypeName:="ReconciledVoucher")> _
  Public Class TYPE_RECONCILED_VOUCHER
    Private operation_id As Int64
    Private voucher_barcode As String
    Private cashier_movement_id As Int64
    Private issued_value_amount As Decimal
    Private issued_iso_code As String
    Private value_amount As Decimal
    Private creation_date As Date
    Private voucher_status As CONCILIATION_STATUS
    Private status_description As String
    Private reconciled_in_session_id As Int64
    Private ticket_id As Int64
    Private ticket_status As TITO_TICKET_STATUS
    Private is_over_amount As Boolean

    ' Constructor
    Sub New()
      Me.operation_id = 0
      Me.voucher_barcode = String.Empty
      Me.cashier_movement_id = 0
      Me.issued_value_amount = 0
      Me.issued_iso_code = String.Empty
      Me.value_amount = 0
      Me.creation_date = WGDB.Now()
      Me.voucher_status = CONCILIATION_STATUS.OK
      Me.status_description = String.Empty
      Me.reconciled_in_session_id = 0
      Me.ticket_id = 0
      Me.ticket_status = TITO_TICKET_STATUS.VALID
    End Sub

#Region " Properties "

    <XmlElement(ElementName:="OperationId")> _
    Public Property OperationId() As Int64
      Get
        Return operation_id
      End Get
      Set(ByVal value As Int64)
        operation_id = value
      End Set
    End Property

    <XmlElement(ElementName:="Barcode")> _
    Public Property OperationBarcode() As String
      Get
        Return voucher_barcode
      End Get
      Set(ByVal value As String)
        voucher_barcode = value
      End Set
    End Property

    <XmlElement(ElementName:="CashierMovementId")> _
    Public Property CashierMovementId() As Int64
      Get
        Return cashier_movement_id
      End Get
      Set(ByVal value As Int64)
        cashier_movement_id = value
      End Set
    End Property

    <XmlElement(ElementName:="IssuedAmount")> _
    Public Property IssuedValueAmount() As Decimal
      Get
        Return issued_value_amount
      End Get
      Set(ByVal value As Decimal)
        issued_value_amount = value
      End Set
    End Property

    <XmlElement(ElementName:="IssuedIsoCode")> _
    Public Property IssuedIsoCode() As String
      Get
        Return issued_iso_code
      End Get
      Set(ByVal value As String)
        issued_iso_code = value
      End Set
    End Property

    <XmlElement(ElementName:="Amount")> _
    Public Property ValueAmount() As Decimal
      Get
        Return value_amount
      End Get
      Set(ByVal value As Decimal)
        value_amount = value
      End Set
    End Property

    <XmlElement(ElementName:="Creation_Date")> _
    Public Property CreationDate() As Date
      Get
        Return creation_date
      End Get
      Set(ByVal value As Date)
        creation_date = value
      End Set
    End Property

    <XmlIgnore> _
    Public Property VoucherStatus() As CONCILIATION_STATUS
      Get
        Return voucher_status
      End Get
      Set(ByVal value As CONCILIATION_STATUS)
        voucher_status = value
      End Set
    End Property

    <XmlElement(ElementName:="Status")>
    Public Property StatusInt32() As Int32
      Get
        Return CInt(VoucherStatus)
      End Get
      Set(ByVal value As Int32)
        VoucherStatus = DirectCast(value, CONCILIATION_STATUS)
      End Set
    End Property

    <System.Xml.Serialization.XmlIgnore> _
    Public Property StatusDescription() As String
      Get
        Return status_description
      End Get
      Set(ByVal value As String)
        status_description = value
      End Set
    End Property

    <System.Xml.Serialization.XmlIgnore> _
    Public Property ReconciledInSessionId() As Int64
      Get
        Return reconciled_in_session_id
      End Get
      Set(ByVal value As Int64)
        reconciled_in_session_id = value
      End Set
    End Property

    <XmlElement(ElementName:="Ticket_Id")>
    Public Property TickedId() As Int64
      Get
        Return ticket_id
      End Get
      Set(ByVal value As Int64)
        ticket_id = value
      End Set
    End Property

    <System.Xml.Serialization.XmlIgnore> _
    Public Property TickedStatus() As TITO_TICKET_STATUS
      Get
        Return ticket_status
      End Get
      Set(ByVal value As TITO_TICKET_STATUS)
        ticket_status = value
      End Set
    End Property

    <System.Xml.Serialization.XmlIgnore> _
    Public Property IsOverAmount() As Boolean
      Get
        Return is_over_amount
      End Get
      Set(ByVal value As Boolean)
        is_over_amount = value
      End Set
    End Property

#End Region ' Properties

  End Class

  <StructLayout(LayoutKind.Sequential), XmlType(TypeName:="gt_sales")> _
  Public Class TYPE_TOTAL_CURRENCIES
    Private total_issued_amount As Decimal
    Private total_issued_iso_code As String
    Private total_amount As Decimal

    ' Constructor
    Sub New()
      Me.total_issued_amount = 0
      Me.total_issued_iso_code = String.Empty
      Me.total_amount = 0
    End Sub

#Region " Properties "

    <XmlElement(ElementName:="TotalIssuedAmount")> _
    Public Property TotalIssuedAmount() As Decimal
      Get
        Return total_issued_amount
      End Get
      Set(ByVal value As Decimal)
        total_issued_amount = value
      End Set
    End Property

    <XmlElement(ElementName:="TotalCurrencyIsoCode")> _
    Public Property TotalCurrencyIsoCode() As String
      Get
        Return total_issued_iso_code
      End Get
      Set(ByVal value As String)
        total_issued_iso_code = value
      End Set
    End Property

    <XmlElement(ElementName:="TotalAmount")> _
    Public Property TotalAmount() As Decimal
      Get
        Return total_amount
      End Get
      Set(ByVal value As Decimal)
        total_amount = value
      End Set
    End Property

#End Region ' Properties

  End Class

  <StructLayout(LayoutKind.Sequential), XmlType(TypeName:="gt_validated_tickets")> _
  Public Class TYPE_VALIDATED_TICKETS
    Private ticket_id As Int64
    Private validation_number As String
    Private ticket_amount As Decimal
    Private ticket_amount_issued As Decimal
    Private ticket_isocode_issued As String
    Private ticket_creation_date As DateTime
    Private ticket_collected As Boolean

    ' Constructor
    Sub New()
      Me.ticket_id = 0
      Me.validation_number = String.Empty
      Me.ticket_amount = 0
      Me.ticket_amount_issued = 0
      Me.ticket_isocode_issued = CurrencyExchange.GetNationalCurrency()
      Me.ticket_creation_date = New DateTime()
      Me.ticket_collected = False
    End Sub



#Region " Properties "

    <XmlElement(ElementName:="TicketId")> _
    Public Property TicketId() As Int64
      Get
        Return ticket_id
      End Get
      Set(ByVal value As Int64)
        ticket_id = value
      End Set
    End Property

    <XmlElement(ElementName:="ValidationNumber")> _
    Public Property ValidationNumber() As String
      Get
        Return validation_number
      End Get
      Set(ByVal value As String)
        validation_number = value
      End Set
    End Property

    <XmlElement(ElementName:="TicketAmount")> _
    Public Property TicketAmount() As Decimal
      Get
        Return ticket_amount
      End Get
      Set(ByVal value As Decimal)
        ticket_amount = value
      End Set
    End Property

    <XmlElement(ElementName:="TicketAmountIssued")> _
    Public Property TicketAmountIssued() As Decimal
      Get
        Return ticket_amount_issued
      End Get
      Set(ByVal value As Decimal)
        ticket_amount_issued = value
      End Set
    End Property

    <XmlElement(ElementName:="TicketIsocodeIssued")> _
    Public Property TicketIsocodeIssued() As String
      Get
        Return ticket_isocode_issued
      End Get
      Set(ByVal value As String)
        ticket_isocode_issued = value
      End Set
    End Property

    <XmlElement(ElementName:="TicketCreationDate")> _
    Public Property TicketCreationDate() As DateTime
      Get
        Return ticket_creation_date
      End Get
      Set(ByVal value As DateTime)
        ticket_creation_date = value
      End Set
    End Property

    <XmlElement(ElementName:="TicketCollected")> _
    Public Property TicketCollected() As Boolean
      Get
        Return ticket_collected
      End Get
      Set(ByVal value As Boolean)
        ticket_collected = value
      End Set
    End Property

#End Region ' Properties

  End Class

#End Region ' Structures

#Region " Members "
  Public m_type_voucher As TYPE_VOUCHER_RECONCILIATION
  Sub New()
    m_type_voucher = New TYPE_VOUCHER_RECONCILIATION()
  End Sub

#End Region    ' Members

#Region "Overrides"

  '----------------------------------------------------------------------------
  ' PURPOSE : Returns the related auditor data for the current object.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - The newly created audit object
  '
  ' NOTES :
  Public Overrides Function AuditorData() As CLASS_AUDITOR_DATA
    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _total_amount As Decimal

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_CASHIER_CONFIGURATION)

    _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(6498), Me.m_type_voucher.TableName & " - " & GUI_FormatDate(m_type_voucher.OpenSessionDate, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM))

    _total_amount = 0

    If m_type_voucher.Vouchers.Count > 0 Then
      For Each _voucher As TYPE_RECONCILED_VOUCHER In m_type_voucher.Vouchers
        If _voucher.TickedId.ToString().Length > 0 Then
          _total_amount += _voucher.ValueAmount
        End If
      Next
      Call _auditor_data.SetField(0, GUI_FormatCurrency(_total_amount), GLB_NLS_GUI_PLAYER_TRACKING.GetString(6527))
    End If

    Return _auditor_data

  End Function ' AuditorData

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As CLASS_BASE.ENUM_STATUS
    Dim _rc As ENUM_STATUS

    _rc = DropConcilition_DbDelete(m_type_voucher)

    Select Case _rc
      Case ENUM_DB_RC.DB_RC_OK
        Return ENUM_STATUS.STATUS_OK

      Case ENUM_DB_RC.DB_RC_ERROR_NOT_FOUND
        Return CLASS_BASE.ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE : Reads from the database into the given object
  '
  ' PARAMS :
  '     - INPUT :
  '         - ObjectId: An object, it must be 'casted' to the desired KEY.
  '         - SqlCtx: SqlTransaction.
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '
  ' NOTES :
  '
  Public Overrides Function DB_Read(ObjectId As Object, ByRef SqlCtx As Integer) As CLASS_BASE.ENUM_STATUS
    Dim _rc As ENUM_DB_RC

    _rc = DropConcilition_DbRead(ObjectId)

    Select Case _rc
      Case ENUM_DB_RC.DB_RC_OK
        Return ENUM_STATUS.STATUS_OK

      Case ENUM_DB_RC.DB_RC_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_OK

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Read

  '----------------------------------------------------------------------------
  ' PURPOSE : Insert the given object to the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '
  ' NOTES :
  '
  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As CLASS_BASE.ENUM_STATUS
    Dim _rc As ENUM_STATUS

    _rc = DropConcilition_DbInsert(m_type_voucher)

    Select Case _rc
      Case ENUM_DB_RC.DB_RC_OK
        Return ENUM_STATUS.STATUS_OK

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Insert

  '----------------------------------------------------------------------------
  ' PURPOSE : Updates the given object to the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '
  ' NOTES :
  '
  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As CLASS_BASE.ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR

  End Function ' DB_Update

  '----------------------------------------------------------------------------
  ' PURPOSE : Returns a duplicate of the given object.
  '
  ' PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - The newly created object
  '
  ' NOTES :
  '
  Public Overrides Function Duplicate() As CLASS_BASE
    Dim _temp_mc As CLASS_DROP_CONCILIATION
    Dim _idx As Integer
    Dim _item As TYPE_RECONCILED_VOUCHER
    Dim _sales As TYPE_TOTAL_CURRENCIES

    _temp_mc = New CLASS_DROP_CONCILIATION

    _temp_mc.m_type_voucher.TableSessionId = m_type_voucher.TableSessionId
    _temp_mc.m_type_voucher.TableName = m_type_voucher.TableName
    '_temp_mc.m_type_voucher.SalesAmounts = m_type_voucher.SalesAmounts
    For Each _sales In m_type_voucher.SalesAmounts
      _temp_mc.m_type_voucher.SalesAmounts.Add(_sales)
    Next
    _temp_mc.m_type_voucher.OpenSessionDate = m_type_voucher.OpenSessionDate
    _temp_mc.m_type_voucher.Vouchers = New List(Of TYPE_RECONCILED_VOUCHER)
    For _idx = 0 To m_type_voucher.Vouchers.Count - 1

      _item = New TYPE_RECONCILED_VOUCHER
      _item.OperationBarcode = m_type_voucher.Vouchers(_idx).OperationBarcode
      _item.OperationId = m_type_voucher.Vouchers(_idx).OperationId
      _item.CashierMovementId = m_type_voucher.Vouchers(_idx).CashierMovementId
      _item.ValueAmount = m_type_voucher.Vouchers(_idx).ValueAmount
      _item.CreationDate = m_type_voucher.Vouchers(_idx).CreationDate
      _item.VoucherStatus = m_type_voucher.Vouchers(_idx).VoucherStatus
      _item.StatusDescription = m_type_voucher.Vouchers(_idx).StatusDescription
      _item.ReconciledInSessionId = m_type_voucher.Vouchers(_idx).ReconciledInSessionId

      _temp_mc.m_type_voucher.Vouchers.Add(_item)
    Next

    Return _temp_mc

  End Function ' Duplicate

#End Region ' Overrides

#Region "Private Functions"

  '----------------------------------------------------------------------------
  ' PURPOSE: Get all conciliated tickets for this table session
  '                                              
  ' PARAMS:
  '   - INPUT: 
  '   - OUTPUT:
  '
  ' RETURNS:
  '   - True
  '   - False
  '
  ' NOTES:
  '
  Private Function DropConcilition_DbRead(ByVal MCParams As CLASS_DROP_CONCILIATION) As ENUM_DB_RC
    Dim _str_sql As StringBuilder
    Dim _data_table As DataTable
    Dim _dt_validated_tickets As DataTable
    Dim _ticket As TYPE_RECONCILED_VOUCHER
    Dim _other_conciliation As String
    Dim _xml As String
    Dim _xml_child As System.Xml.XmlDocument
    Dim _natinonal_iso_code As String
    Dim _gt_sales_for_old_xml As Decimal
    Dim _row_validated_ticket As DataRow

    _natinonal_iso_code = CurrencyExchange.GetNationalCurrency()
    _gt_sales_for_old_xml = 0

    MCParams.m_type_voucher.Vouchers = New List(Of TYPE_RECONCILED_VOUCHER)
    _other_conciliation = ""
    _str_sql = New StringBuilder

    _xml = ""
    _xml_child = New System.Xml.XmlDocument()

    _str_sql.AppendLine("SELECT   GTSC_SESSION_ID        ")
    _str_sql.AppendLine("       , GTSC_TYPE              ")
    _str_sql.AppendLine("       , GTSC_DATA              ")
    _str_sql.AppendLine("       , GTSC_USER_ID           ")
    _str_sql.AppendLine("       , GTSC_CREATED           ")
    _str_sql.AppendLine("  FROM   GT_SESSIONS_CONCILIATE ")
    _str_sql.AppendLine(" WHERE   GTSC_SESSION_ID =  " & MCParams.m_type_voucher.TableSessionId.ToString())

    _data_table = GUI_GetTableUsingSQL(_str_sql.ToString, 5000)

    ' Read Validated Tickets
    _str_sql = New StringBuilder

    _str_sql.AppendLine("SELECT   TI_TICKET_ID                                  ")
    _str_sql.AppendLine("       , TI_VALIDATION_NUMBER                          ")
    _str_sql.AppendLine("       , TI_AMOUNT                                     ")
    _str_sql.AppendLine("       , TI_AMT0                                       ")
    _str_sql.AppendLine("       , TI_CUR0                                       ")
    _str_sql.AppendLine("       , TI_CREATED_DATETIME                           ")
    _str_sql.AppendLine("       , ISNULL(TI_COLLECTED, 0)                       ")
    _str_sql.AppendLine("  FROM   GT_COPY_DEALER_VALIDATED                      ")
    _str_sql.AppendLine(" INNER   JOIN TICKETS ON GTCD_TICKET_ID = TI_TICKET_ID ")
    _str_sql.AppendLine(" WHERE   GTCD_GAMING_TABLE_SESSION_ID =  " & MCParams.m_type_voucher.TableSessionId.ToString())
    _str_sql.AppendLine("   AND   GTCD_VALIDATION_SOURCE =  " & WSI.Common.GamingTableBusinessLogic.GT_COPY_DEALER_VALIDATED_SOURCE.GAMING_TABLE)

    _dt_validated_tickets = GUI_GetTableUsingSQL(_str_sql.ToString, 5000)

    If IsNothing(_data_table) Then
      Return ENUM_DB_RC.DB_RC_ERROR
    End If

    ' Load validated tickets
    If Not IsNothing(_dt_validated_tickets) OrElse _dt_validated_tickets.Rows.Count > 0 Then
      MCParams.m_type_voucher.ValidatedTickets = New List(Of TYPE_VALIDATED_TICKETS)
      Dim _validated_ticket As TYPE_VALIDATED_TICKETS

      For Each _row_validated_ticket In _dt_validated_tickets.Rows
        _validated_ticket = New TYPE_VALIDATED_TICKETS()

        _validated_ticket.TicketId = _row_validated_ticket(SQL_COL_VALID_TICKET_TICKET_ID)
        If (_row_validated_ticket(SQL_COL_VALID_TICKET_VALIDATION_NUM).ToString().Length > 8) Then
          _validated_ticket.ValidationNumber = _row_validated_ticket(SQL_COL_VALID_TICKET_VALIDATION_NUM).ToString().PadLeft(18, "0")
        Else
          _validated_ticket.ValidationNumber = _row_validated_ticket(SQL_COL_VALID_TICKET_VALIDATION_NUM)
        End If

        _validated_ticket.TicketAmount = _row_validated_ticket(SQL_COL_VALID_TICKET_AMOUNT)
        _validated_ticket.TicketAmountIssued = _row_validated_ticket(SQL_COL_VALID_TICKET_AMOUNT_ISSUED)
        _validated_ticket.TicketIsocodeIssued = _row_validated_ticket(SQL_COL_VALID_TICKET_ISOCODE_ISSUED)
        _validated_ticket.TicketCreationDate = _row_validated_ticket(SQL_COL_VALID_TICKET_CREATION_DATE)
        _validated_ticket.TicketCollected = _row_validated_ticket(SQL_COL_VALID_TICKET_COLLECTED)

        MCParams.m_type_voucher.ValidatedTickets.Add(_validated_ticket)
      Next
    End If

    If _data_table.Rows.Count() = 0 Or _data_table.Rows.Count() > 0 AndAlso IsDBNull(_data_table.Rows(0).Item("GTSC_DATA")) Then
      Return ENUM_DB_RC.DB_RC_ERROR_NOT_FOUND
    End If

    _xml = _data_table.Rows(0).Item("GTSC_DATA")

    If Not WSI.Common.UtilsDesign.DeserializeXml(_xml, m_type_voucher) Then
      Return ENUM_DB_RC.DB_RC_ERROR
    ElseIf m_type_voucher.SalesAmounts.Count = 1 AndAlso m_type_voucher.SalesAmounts(0).TotalCurrencyIsoCode = String.Empty Then
      For Each _voucher As TYPE_RECONCILED_VOUCHER In m_type_voucher.Vouchers
        If _voucher.IssuedIsoCode = String.Empty Then
          _voucher.IssuedIsoCode = _natinonal_iso_code
          _voucher.IssuedValueAmount = _voucher.ValueAmount
          _gt_sales_for_old_xml += _voucher.ValueAmount
        End If
      Next
      Dim _gt_sales As TYPE_TOTAL_CURRENCIES
      _gt_sales = New TYPE_TOTAL_CURRENCIES()
      _gt_sales.TotalIssuedAmount = _gt_sales_for_old_xml
      _gt_sales.TotalCurrencyIsoCode = _natinonal_iso_code
      _gt_sales.TotalAmount = _gt_sales_for_old_xml
      m_type_voucher.SalesAmounts = New List(Of TYPE_TOTAL_CURRENCIES)
      m_type_voucher.SalesAmounts.Add(_gt_sales)
    End If

    m_type_voucher.HasConciliate = True

    For Each _ticket In m_type_voucher.Vouchers

      _ticket.StatusDescription = GetStatusDescription(_ticket.VoucherStatus, _other_conciliation)

    Next

    MCParams.m_type_voucher = m_type_voucher

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

  End Function ' DropConcilition_DbRead

  '----------------------------------------------------------------------------
  ' PURPOSE: Update the database conciliation for drop
  '                                             
  ' PARAMS:
  '   - INPUT: Transaction
  '   - OUTPUT: None
  '
  ' RETURNS:
  '   - True
  '   - False
  '
  ' NOTES:
  Private Function DropConcilition_DbInsert(ByVal MCParams As TYPE_VOUCHER_RECONCILIATION) As ENUM_STATUS
    Dim _sb As StringBuilder
    Dim _sql_trx As System.Data.SqlClient.SqlTransaction = Nothing
    Dim _ticket As TYPE_RECONCILED_VOUCHER
    Dim _xml As String
    Dim _xml_child As System.Xml.XmlDocument

    Dim _gaming_table_sesion As GamingTablesSessions
    Dim _currency_iso_type As CurrencyIsoType

    Dim _source_name As String
    Dim _description As String
    Dim _ticket_status_description As String

    _xml = ""
    _xml_child = New System.Xml.XmlDocument()

    _source_name = GLB_CurrentUser.Name & "@" & Environment.MachineName

    Try

      If WSI.Common.UtilsDesign.ObjectSerializableToXml(m_type_voucher, _xml) Then
        ' Load xml
        _xml_child.LoadXml(_xml)

        ' Remove XML declaration
        If _xml_child.FirstChild.NodeType = System.Xml.XmlNodeType.XmlDeclaration Then
          _xml_child.RemoveChild(_xml_child.FirstChild)
        End If

        _xml = _xml_child.InnerXml().ToString()
      Else
        _xml = ""
      End If

      Using _db_trx As New WSI.Common.DB_TRX()

        _gaming_table_sesion = Nothing
        GamingTablesSessions.GetSession(_gaming_table_sesion, MCParams.TableSessionId, _db_trx.SqlTransaction)

        For Each _ticket In MCParams.Vouchers
          _sb = New StringBuilder()
          _sb.AppendLine(" UPDATE   CASHIER_MOVEMENTS ")
          _sb.AppendLine("    SET   CM_GAMING_TABLE_SESSION_ID = CASE WHEN CM_GAMING_TABLE_SESSION_ID IS NULL ")
          _sb.AppendLine("                                            THEN @pGamingTableSessionId             ")
          _sb.AppendLine("                                            WHEN CM_GAMING_TABLE_SESSION_ID = 0     ")
          _sb.AppendLine("                                            THEN @pGamingTableSessionId             ")
          _sb.AppendLine("                                            ELSE CM_GAMING_TABLE_SESSION_ID END     ")
          _sb.AppendLine("  WHERE   CM_MOVEMENT_ID = @pMovementId       ")
          _sb.AppendLine("    AND   CM_TYPE IN (@pType, @pTypeExchange) ")
          _sb.AppendLine("    AND   CM_UNDO_STATUS IS NULL              ")

          _sb.AppendLine(" UPDATE   TICKETS ")
          _sb.AppendLine("    SET   TI_STATUS = CASE WHEN TI_STATUS = @pTicketStatusValid ")
          _sb.AppendLine("                           THEN @pTicketStatusCancelled         ")
          _sb.AppendLine("                           ELSE TI_STATUS                       ")
          _sb.AppendLine("                           END                                  ")
          _sb.AppendLine("        , TI_COLLECTED = 1                                      ")
          _sb.AppendLine("  WHERE   TI_TICKET_ID = @pTicketid ")

          _sb.AppendLine(" IF NOT EXISTS (SELECT 1 FROM GT_COPY_DEALER_VALIDATED WHERE GTCD_TICKET_ID = @pTicketid ) AND @pTicketid > 0 ")
          _sb.AppendLine(" BEGIN ")
          _sb.AppendLine("    INSERT INTO   GT_COPY_DEALER_VALIDATED     ")
          _sb.AppendLine("                ( GTCD_GAMING_TABLE_SESSION_ID ")
          _sb.AppendLine("                , GTCD_TICKET_ID               ")
          _sb.AppendLine("                , GTCD_VALIDATION_SOURCE       ")
          _sb.AppendLine("                , GTCD_VALIDATION_DATETIME )   ")
          _sb.AppendLine("        VALUES                                 ")
          _sb.AppendLine("               ( @pGamingTableSessionId        ")
          _sb.AppendLine("               , @pTicketid                    ")
          _sb.AppendLine("               , @pValidationSource            ")
          _sb.AppendLine("               , GETDATE()                 )   ")
          _sb.AppendLine(" END   ")

          Using _cmd As SqlCommand = New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
            _cmd.Parameters.Add("@pGamingTableSessionId", SqlDbType.BigInt).Value = MCParams.TableSessionId
            _cmd.Parameters.Add("@pMovementId", SqlDbType.Int).Value = _ticket.CashierMovementId
            _cmd.Parameters.Add("@pType", SqlDbType.Int).Value = CASHIER_MOVEMENT.CHIPS_SALE_TOTAL
            _cmd.Parameters.Add("@pTypeExchange", SqlDbType.Int).Value = CASHIER_MOVEMENT.CHIPS_SALE_TOTAL_EXCHANGE

            _cmd.Parameters.Add("@pTicketStatusValid", SqlDbType.Int).Value = TITO_TICKET_STATUS.VALID
            _cmd.Parameters.Add("@pTicketStatusCancelled", SqlDbType.Int).Value = TITO_TICKET_STATUS.CANCELED
            _cmd.Parameters.Add("@pTicketid", SqlDbType.BigInt).Value = _ticket.TickedId
            _cmd.Parameters.Add("@pValidationSource", SqlDbType.Int).Value = GamingTableBusinessLogic.GT_COPY_DEALER_VALIDATED_SOURCE.GUI

            _cmd.ExecuteNonQuery()

          End Using

          If _ticket.VoucherStatus = CONCILIATION_STATUS.OK Or _ticket.VoucherStatus = CONCILIATION_STATUS.OK_OVER_AMOUNT Then

            _currency_iso_type = New CurrencyIsoType()
            _currency_iso_type.IsoCode = _ticket.IssuedIsoCode
            _currency_iso_type.Type = CurrencyExchangeType.CASINO_CHIP_RE

          End If

          If _ticket.IsOverAmount And Not GeneralParam.GetBoolean("GamingTables", "Conciliation.SuggestTickets", True) Then
            _description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8195, _ticket.OperationBarcode, GUI_FormatNumber(_ticket.ValueAmount.ToString()))

            Call Alarm.Register(AlarmSourceCode.User, GLB_CurrentUser.Id, _source_name, _
                                AlarmCode.User_ReconciledTicketNotExpected, _
                                _description, AlarmSeverity.Warning, WGDB.Now(), _db_trx.SqlTransaction)

          End If

          If _ticket.VoucherStatus = CONCILIATION_STATUS.NOT_EXISTS Then
            _ticket.TickedStatus = TITO_TICKET_STATUS.DISCARDED
            _ticket_status_description = _ticket.StatusDescription
          Else
            _ticket_status_description = TicketStatusAsString(_ticket.TickedStatus, False)
          End If
          If _ticket.TickedStatus <> TITO_TICKET_STATUS.CANCELED And GeneralParam.GetBoolean("GamingTables", "Conciliation.SuggestTickets", True) Then
            _description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8198, _ticket.OperationBarcode, _ticket_status_description, GUI_FormatNumber(_ticket.ValueAmount.ToString()))

            Call Alarm.Register(AlarmSourceCode.User, GLB_CurrentUser.Id, _source_name, _
                                AlarmCode.User_ReconciledTicketNotValid, _
                                _description, AlarmSeverity.Warning, WGDB.Now(), _db_trx.SqlTransaction)

          End If

        Next

        _sb = New StringBuilder()
        _sb.AppendLine("INSERT   INTO GT_SESSIONS_CONCILIATE ")
        _sb.AppendLine("       ( GTSC_SESSION_ID             ")
        _sb.AppendLine("       , GTSC_DATA                   ")
        _sb.AppendLine("       , GTSC_USER_ID                ")
        _sb.AppendLine("       ) ")
        _sb.AppendLine("VALUES ( @pGamingTableSessionId ")
        _sb.AppendLine("       , @pXML                  ")
        _sb.AppendLine("       , @pUserId               ")
        _sb.AppendLine("       )                        ")

        Using _cmd As SqlCommand = New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          _cmd.Parameters.Add("@pGamingTableSessionId", SqlDbType.BigInt).Value = MCParams.TableSessionId
          _cmd.Parameters.Add("@pXML", SqlDbType.NVarChar).Value = _xml
          _cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = GLB_CurrentUser.GuiId

          _cmd.ExecuteNonQuery()

        End Using

        _db_trx.Commit()
        Return ENUM_STATUS.STATUS_OK

      End Using
    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return ENUM_STATUS.STATUS_ERROR

  End Function ' DropConcilition_DbUpdate

  '----------------------------------------------------------------------------
  ' PURPOSE: Delete the database conciliation for drop
  '                                             
  ' PARAMS:
  '   - INPUT: Transaction
  '   - OUTPUT: None
  '
  ' RETURNS:
  '   - True
  '   - False
  '
  ' NOTES:
  Private Function DropConcilition_DbDelete(ByVal MCParams As TYPE_VOUCHER_RECONCILIATION) As ENUM_STATUS
    Dim _sb As StringBuilder
    Dim _sql_trx As System.Data.SqlClient.SqlTransaction = Nothing
    Dim _ticket As TYPE_RECONCILED_VOUCHER

    Try
      Using _db_trx As New WSI.Common.DB_TRX()

        For Each _ticket In MCParams.Vouchers
          _sb = New StringBuilder()

          _sb.AppendLine("   UPDATE   CASHIER_MOVEMENTS                                   ")
          _sb.AppendLine("      SET   CM_GAMING_TABLE_SESSION_ID = 0                      ")
          _sb.AppendLine("    WHERE   CM_MOVEMENT_ID             = @pMovementId           ")
          _sb.AppendLine("      AND   CM_GAMING_TABLE_SESSION_ID = @pGamingTableSessionId ")
          _sb.AppendLine("      AND   CM_UNDO_STATUS             IS NULL                  ")

          _sb.AppendLine(" IF EXISTS (SELECT 1 FROM GT_COPY_DEALER_VALIDATED WHERE  GTCD_TICKET_ID = @pTicketid                      ")
          _sb.AppendLine("                                                     AND (GTCD_VALIDATION_SOURCE = @pValidationSourceGUI   ")
          _sb.AppendLine("                                                      OR  GTCD_VALIDATION_SOURCE = @pValidationSourceGT )) ")
          _sb.AppendLine(" BEGIN ")

          _sb.AppendLine("   UPDATE   TICKETS ")
          _sb.AppendLine("      SET   TI_STATUS = CASE WHEN TI_STATUS = @pTicketStatusCancelled ")
          _sb.AppendLine("                             THEN @pTicketStatusValid                 ")
          _sb.AppendLine("                             ELSE TI_STATUS                           ")
          _sb.AppendLine("                             END                                      ")
          _sb.AppendLine("          , TI_COLLECTED = NULL                                       ")
          _sb.AppendLine("    WHERE   TI_TICKET_ID = @pTicketid ")

          _sb.AppendLine("   DELETE   GT_COPY_DEALER_VALIDATED                      ")
          _sb.AppendLine("    WHERE   GTCD_TICKET_ID = @pTicketid                   ")
          _sb.AppendLine("      AND   GTCD_VALIDATION_SOURCE = @pValidationSourceGUI   ")

          _sb.AppendLine(" END   ")


          Using _cmd As SqlCommand = New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
            _cmd.Parameters.Add("@pGamingTableSessionId", SqlDbType.Int).Value = Me.m_type_voucher.TableSessionId
            _cmd.Parameters.Add("@pMovementId", SqlDbType.Int).Value = _ticket.CashierMovementId

            _cmd.Parameters.Add("@pTicketStatusValid", SqlDbType.Int).Value = TITO_TICKET_STATUS.VALID
            _cmd.Parameters.Add("@pTicketStatusCancelled", SqlDbType.Int).Value = TITO_TICKET_STATUS.CANCELED
            _cmd.Parameters.Add("@pValidationSourceGUI", SqlDbType.Int).Value = GamingTableBusinessLogic.GT_COPY_DEALER_VALIDATED_SOURCE.GUI
            _cmd.Parameters.Add("@pValidationSourceGT", SqlDbType.Int).Value = GamingTableBusinessLogic.GT_COPY_DEALER_VALIDATED_SOURCE.GAMING_TABLE
            _cmd.Parameters.Add("@pTicketid", SqlDbType.BigInt).Value = _ticket.TickedId

            _cmd.ExecuteNonQuery()

          End Using
        Next

        _sb = New StringBuilder()
        _sb.AppendLine("UPDATE   GAMING_TABLES_SESSIONS                                ")
        _sb.AppendLine("   SET   GTS_EXTERNAL_SALES_AMOUNT = 0                         ")
        _sb.AppendLine("     ,   GTS_TOTAL_SALES_AMOUNT = GTS_OWN_SALES_AMOUNT         ")
        _sb.AppendLine(" WHERE   GTS_GAMING_TABLE_SESSION_ID = @pGamingTableSessionId  ")

        _sb.AppendLine("UPDATE   GAMING_TABLES_SESSIONS_BY_CURRENCY                    ")
        _sb.AppendLine("   SET   GTSC_EXTERNAL_SALES_AMOUNT = 0                        ")
        _sb.AppendLine("     ,   GTSC_TOTAL_SALES_AMOUNT = GTSC_OWN_SALES_AMOUNT       ")
        _sb.AppendLine(" WHERE   GTSC_GAMING_TABLE_SESSION_ID = @pGamingTableSessionId ")

        _sb.AppendLine("DELETE   GT_SESSIONS_CONCILIATE                                ")
        _sb.AppendLine(" WHERE   GTSC_SESSION_ID = @pGamingTableSessionId              ")

        Using _cmd As SqlCommand = New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          _cmd.Parameters.Add("@pGamingTableSessionId", SqlDbType.Int).Value = Me.m_type_voucher.TableSessionId

          _cmd.ExecuteNonQuery()

        End Using

        _db_trx.Commit()
        Return ENUM_STATUS.STATUS_OK

      End Using
    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return ENUM_STATUS.STATUS_ERROR

  End Function

#End Region ' Private Functions

#Region " Public Functions "

  Public Function GetStatusDescription(ticket_status As CLASS_DROP_CONCILIATION.CONCILIATION_STATUS, Optional _other_conciliation As String = "") As String
    Dim _mensaje As String

    _mensaje = String.Empty

    Select Case (ticket_status)
      Case CLASS_DROP_CONCILIATION.CONCILIATION_STATUS.OK
        _mensaje = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6529)
      Case CLASS_DROP_CONCILIATION.CONCILIATION_STATUS.REPEATED_IN_THIS_RECONCILIATION
        _mensaje = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6530)
      Case CLASS_DROP_CONCILIATION.CONCILIATION_STATUS.REPEATED_IN_OTHER_RECONCILIATION
        _mensaje = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6531, _other_conciliation)
      Case CLASS_DROP_CONCILIATION.CONCILIATION_STATUS.TICKET_CANCELLED
        _mensaje = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6532)
      Case CONCILIATION_STATUS.NOT_EXISTS
        _mensaje = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6533)
      Case CONCILIATION_STATUS.PENDING
        _mensaje = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8165)
      Case CLASS_DROP_CONCILIATION.CONCILIATION_STATUS.OK_OVER_AMOUNT
        _mensaje = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8187)
    End Select

    Return _mensaje

  End Function

#End Region  ' Public Functions

End Class
