'-------------------------------------------------------------------
' Copyright © 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : cls_cage_control_TITO.vb
'
' DESCRIPTION : cage class
'              
' REVISION HISTORY :
'
' Date         Author Description
' -----------  ------ ----------------------------------------------
' 30-JAN-2014 CCG & FBA & HBB    First Release
' 25-FEB-2014 ICS & FBA          Added support for terminal collection with cage movement
' 27-FEB-2014 LEM                Autoupdate collection status from tickets on cashier collection (theoretical vs real)
' 13-MAR-2014 JCA        Fixed Bug WIG-723
' 20-MAY-2014 LEM & DRV  Fixed Bug WIG-927 Couldn't load a collection that has been cancelled and collected again
' 21-MAY-2014 RCI & HBB  Fixed Bug WIGOSTITO-1225 Error trying to open a collection that was closed from cashier session details form
' 22-JUL-2014 DRV & HBB  Added floor Id in queries
' 18-AUG-2014 HBB        Added the collection events. A new column in money collections in xml format that contains  the events thrown during the note counter reading
' 29-OCT-2014 DRV  Fixed Bug WIGOSTITO-1256: Denominations lower than 1 are not saved
' 19-NOV-2014 DRV  Fixed Bug WIG-1711: Added Massive collection for cashless mode
' 28-MAY-2015 YNM  Fixed Bug WIG-2385: Stacker Change does not show the ID Stacker 
' 07-JUL-2015 DRV  Fixed Bug WIG-2545: Error on massive collections.
' 18-AUG-2015 YNM  Fixed Bug TFS-3788: Unhandled exception on massive collections. System out of memory 
' 20-AUG-2015 YNM  Fixed Bug TFS-3804: Unhandled exception opening a collection from cloned terminal
' 08-SEP-2015 FOS  Backlog Item 3709
' 22-SEP-2015 FOS  Task 4458:Recaudación de monedas: Mejoras recaudación
' 07-OCT-2015 YNM  Fixed Bug TFS-5058: GUI: error cancelling Collection with Tickets
' 13-OCT-2015 ETP  Fixed Bug TFS-5146: GUI: Added new columns in UNION queries.
' 15-OCT-2015 YNM  Fixed Bug TFS-5287: Terminal Collection with Tickets is always unbalanced
' 27-OCT-2015 FOS  Fixed Bug TFS-5287: GUI: Added null column in union Queries, coin collection
' 05-NOV-2015 FOS & JPJ  Fixed Bug TFS-6007, 6235, 6238, 6242: GUI: error in total
' 09-NOV-2015 FOS & JPJ  Fixed Bug TFS-6235 We can not cancel a cashier withdraw
' 18-NOV-2015 DCS  Fixed Bug TFS-6751 & TFS-6764 & TFS-6765 Udate CAGE_MOVEMENT_DETAILS when collect stackers
' 30-NOV-2015 JPJ  Fixed Bug TFS-7135 Duplicated elements.
' 13-JAN-2016 DHA  Task 8287: Floor Dual Currency
' 01-FEB-2016 DHA  Product Backlog Item 8351:Floor Dual Currency: Movimientos de tickets TITO en M.N.
' 10-FEB-2016 DHA  Bug 9181: Dual currency error mapping ticket amount
' 18-FEB-2016 DHA  Bug 9633: Dual currency: in cashless mode when note acceptor activated system shows an error.
' 03-MAR-2016 JPJ  Bug 10255: While introducing and incorrect Stacker ID.
' 08-MAR-2016 FAV  Fixed Bug in SELECT sentence to calculate theoretical sum of bills and real sum of bills
' 28-ABR-2016 JML  Product Backlog Item 10825:Gaming Tables (Fase 1): Items, tickets & chips (Cage)
' 24-MAY-2016 DHA  Product Backlog Item 9848: drop box gaming tables
' 28-JUN-2016 DHA  Product Backlog Item 15903: Gaming tables (Fase 5): Drop box collection on Cashier
' 28-DIC-2016 FJC  Bug 21984:CountR: no se recaudan los tickets pagados
' 11-JAN-2017 FAV  Bug 7151: Floor ID is showing empty
' 09-FEN-2017 JML  PBI 24378: Reabrir sesiones de caja y mesas de juego: rehacer reapertura y corrección de Bugs
'-------------------------------------------------------------------
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports GUI_CommonOperations
Imports GUI_Controls
Imports GUI_CommonMisc
Imports WSI.Common
Imports WSI.Common.TITO
Imports System.Text
' Tito's imports
Imports WigosGUI.NEW_COLLECTION

Partial Public Class CLASS_CAGE_CONTROL

#Region "Members"
  ' members
  Public m_type_new_collection As New NEW_COLLECTION.TYPE_NEW_COLLECTION()
  Public m_session_id As Int64
  Public m_new_collecion_mode As Boolean = False
  Public m_is_fill_out_movement As Boolean
  Public m_load_cancelled_by_user As Boolean

#End Region ' Members

#Region "Contants"

  'SQL COLUMNS FOR COLLECTION IN EDIT MODE
  Const SQL_COLUMN_COLLECTION_TERMINAL_ID_EDIT_MODE As Int32 = 0
  Const SQL_COLUMN_COLLECTION_TERMINAL_NAME_EDIT_MODE As Int32 = 1
  Const SQL_COLUMN_COLLECTION_INSERTED_DATE_TIME_EDIT_MODE As Int32 = 2
  Const SQL_COLUMN_COLLECTION_EXTRACTION_DATE_TIME_EDIT_MODE As Int32 = 3
  Const SQL_COLUMN_COLLECTION_TICKETS_COUNT_EDIT_MODE As Int32 = 4
  Const SQL_COLUMN_COLLECTION_AMOUNT_IN_TICKETS_EDIT_MODE As Int32 = 5
  Const SQL_COLUMN_COLLECTION_BILLS_COUNT_EDIT_MODE As Int32 = 6
  Const SQL_COLUMN_COLLECTION_AMOUNT_IN_BILLS_EDIT_MODE As Int32 = 7
  Const SQL_COLUMN_COLLECTION_COLLECTION_ID_EDIT_MODE As Int32 = 8
  Const SQL_COLUMN_COLLECTION_STATUS_EDIT_MODE As Int32 = 9
  Const SQL_COLUMN_COLLECTION_STACKER_ID_EDIT_MODE As Int32 = 10
  Const SQL_COLUMN_COLLECTION_EXPECTED_COIN_COLLECTION_EDIT_MODE As Int32 = 11
  Const SQL_COLUMN_COLLECTION_COIN_COLLECTION_EDIT_MODE As Int32 = 12
  Const SQL_COLUMN_COLLECTION_TERMINAL_ID_ByPROMOBOX_EDIT_MODE As Int32 = 13
  Const SQL_COLUMN_COLLECTION_CASHIER_SESSION_EDIT_MODE As Int32 = 14
  Const SQL_COLUMN_COLLECTION_TERMINAL_ISO_CODE_EDIT_MODE As Int32 = 15
  Const SQL_COLUMN_COLLECTION_TERMINAL_FLOOR_ID_EDIT_MODE As Int32 = 16


  'SQL COLUMNS COLLECTION DETAILS IN EDIT MODE
  Const SQL_COLUMN_TICKET_OR_NOTE_EDIT_MODE As Int32 = 0
  Const SQL_COLUMN_TICKET_NUMBER_EDIT_MODE As Int32 = 1
  Const SQL_COLUMN_TICKET_THEORETICAL_VALUE_EDIT_MODE As Int32 = 2
  Const SQL_COLUMN_DENOMINATION_EDIT_MODE As Int32 = 3
  Const SQL_COLUMN_THEORETICAL_QUANTITY_EDIT_MODE As Int32 = 4
  Const SQL_COLUMN_TICKET_THEORETICAL_MONEY_COLLECTION_ID As Int32 = 5
  Const SQL_COLUMN_TICKET_VALIDATION_TYPE_EDIT_MODE As Int32 = 6
  Const SQL_COLUMN_TICKET_ID As Int32 = 7
  Const SQL_COLUMN_TICKET_REAL_MONEY_COLLECTION_ID = 8
  Const SQL_COLUMN_TICKET_TYPE = 9
  Const SQL_COLUMN_TICKET_STATUS = 10
  Const SQL_COLUMN_TICKET_CAGE_MOVEMENT_ID = 11
  Const SQL_COLUMN_TICKET_AMOUNT_ISSUE = 12
  Const SQL_COLUMN_TICKET_CURRENCY_ISSUE = 13


  'SQL COLUMNS FOR COLLECTION FIELDS QUERY
  Const SQL_COLUMN_COLLECTION_USER_NAME As Int32 = 0
  Const SQL_COLUMN_COLLECTION_TERMINAL_NAME As Int32 = 1
  Const SQL_COLUMN_COLLECTION_THEORETICAL_TICKET_SUM As Int32 = 2
  Const SQL_COLUMN_COLLECTION_THEORETICAL_BILLS_SUM As Int32 = 3
  Const SQL_COLUMN_COLLECTION_REAL_BILLS_SUM As Int32 = 4
  Const SQL_COLUMN_COLLECTION_REAL_BILLS_COUNT As Int32 = 5
  Const SQL_COLUMN_COLLECTION_REAL_TICKET_SUM As Int32 = 6
  Const SQL_COLUMN_COLLECTION_COLLECTION_ID As Int32 = 7
  Const SQL_COLUMN_COLLECTION_INSERTED_DATE_TIME As Int32 = 8
  Const SQL_COLUMN_COLLECTION_TERMINAL_ID As Int32 = 9
  Const SQL_COLUMN_COLLECTION_USER_ID As Int32 = 10
  Const SQL_COLUMN_COLLECTION_NOTES As Int32 = 11
  Const SQL_COLUMN_COLLECTION_STACKER_ID As Int32 = 12
  Const SQL_COLUMN_COLLECTION_USER_INSERTION_ID As Int32 = 13
  Const SQL_COLUMN_COLLECTION_COLLECTED_DATE_TIME As Int32 = 14
  Const SQL_COLUMN_COLLECTION_EXTRACTION_DATE_TIME As Int32 = 15
  Const SQL_COLUMN_COLLECTION_STATUS As Int32 = 16
  Const SQL_COLUMN_COLLECTION_CASHIER_SESSION As Int32 = 17
  Const SQL_COLUMN_COLLECTION_EXPECTED_COINS_COLLECTION = 18
  Const SQL_COLUMN_COLLECTION_COINS_COLLECTION = 19
  Const SQL_COLUMN_COLLECTION_EVENTS = 20
  Const SQL_COLUMN_COLLECTION_TERMINAL_ISO_CODE = 21
  Const SQL_COLUMN_COLLECTION_TERMINAL_FLOOR_ID = 22


  'SQL COLUMNS FOR COLLECTION DETAILS QUERY 
  Const SQL_COLUMN_COLLECTION_DETAILS_TYPE As Int32 = 0
  Const SQL_COLUMN_COLLECTION_DETAILS_MONEY_COLLECTION_ID As Int32 = 1
  Const SQL_COLUMN_COLLECTION_DETAILS_TICKET_AMOUNT As Int32 = 2
  Const SQL_COLUMN_COLLECTION_DETAILS_BILL_DENOM As Int32 = 3
  Const SQL_COLUMN_COLLECTION_DETAILS_BILLS_REAL_COUNT As Int32 = 4
  Const SQL_COLUMN_COLLECTION_DETAILS_BILLS_THEO_COUNT As Int32 = 5
  Const SQL_COLUMN_COLLECTION_DETAILS_BILLS_REAL_VS_THEO_COUNT As Int32 = 6
  Const SQL_COLUMN_COLLECTION_DETAILS_TICKET_VALIDATION_NUMBER As Int32 = 7
  Const SQL_COLUMN_COLLECTION_DETAILS_COLLECTED_TICKET_COLLECTION As Int32 = 8
  Const SQL_COLUMN_COLLECTION_DETAILS_TICKET_VALIDATION_TYPE As Int32 = 9
  Const SQL_COLUMN_COLLECTION_DETAILS_TICKET_CM_ID = 10
  Const SQL_COLUMN_COLLECTION_DETAILS_CURRENCY_TYPE = 11
  Const SQL_COLUMN_COLLECTION_DETAILS_TICKET_AMOUNT_ISSUE = 12
  Const SQL_COLUMN_COLLECTION_DETAILS_TICKET_CURRENCY_ISSUE = 13

#End Region

#Region "Public"

  Public Function ReadTheoreticalCollectionNewMode(Optional ByVal CollectionId As Int64 = -1, Optional ByVal SessionId As Int64 = -1) As Boolean

    Dim _sb As StringBuilder
    Dim _table As DataTable
    Dim _where As String

    _sb = New StringBuilder()
    _table = New DataTable()
    _where = ""

    Try
      Using _db_trx As New DB_TRX()

        If m_operation_type = Cage.CageOperationType.FromTerminal Then

          If CollectionId > 0 Then
            _where = "MC_COLLECTION_ID = @pCollectionId"
          ElseIf SessionId > 0 Then
            _where = "MC_CASHIER_SESSION_ID = @pSessionId"
          Else
            Return False
          End If

          _sb.AppendLine("    SELECT  TOP 1                                                    ")
          _sb.AppendLine("             MC_TERMINAL_ID                                          ")
          _sb.AppendLine("            ,TE_NAME                                                 ")
          _sb.AppendLine("            ,MC_DATETIME                                             ")
          _sb.AppendLine("            ,MC_EXTRACTION_DATETIME                                  ")
          _sb.AppendLine("            ,MC_EXPECTED_TICKET_COUNT                                ")
          _sb.AppendLine("            ,MC_EXPECTED_TICKET_AMOUNT                               ")
          _sb.AppendLine("            ,MC_EXPECTED_BILL_COUNT                                  ")
          _sb.AppendLine("            ,MC_EXPECTED_BILL_AMOUNT                                 ")
          _sb.AppendLine("            ,MC_COLLECTION_ID                                        ")
          _sb.AppendLine("            ,MC_STATUS                                               ")
          _sb.AppendLine("            ,MC_STACKER_ID                                           ")
          _sb.AppendLine("            ,MC_EXPECTED_COIN_AMOUNT                                 ")
          _sb.AppendLine("            ,TE_COIN_COLLECTION                                      ")

          If Not Me.m_promobox_or_acceptor Then
            _sb.AppendLine("           , 0                                                     ") 'CT_CASHIER_ID
            _sb.AppendLine("           , MC_CASHIER_SESSION_ID                                 ")
            ' DHA 13-JAN-2016: get terminal currency
            _sb.AppendLine("           ,TE_ISO_CODE                                            ")
            _sb.AppendLine("           ,TE_FLOOR_ID                                            ")
            _sb.AppendLine("      FROM   MONEY_COLLECTIONS                                     ")
            _sb.AppendLine("INNER JOIN   TERMINALS ON TE_TERMINAL_ID = MC_TERMINAL_ID          ")
            _sb.AppendLine("     WHERE " & _where)
            _sb.AppendLine("       AND   MC_STATUS = @pStatus                                  ")
          Else
            _sb.AppendLine("           , CT_CASHIER_ID                                         ")
            _sb.AppendLine("           , MC_CASHIER_SESSION_ID                                 ")
            _sb.AppendLine("           , NULL                                                  ")
            _sb.AppendLine("           , TE_FLOOR_ID                                           ")
            _sb.AppendLine("      FROM   MONEY_COLLECTIONS                                     ")
            _sb.AppendLine("INNER JOIN   TERMINALS ON TE_TERMINAL_ID = MC_TERMINAL_ID          ")
            _sb.AppendLine("INNER JOIN   CASHIER_TERMINALS ON TE_TERMINAL_ID = CT_TERMINAL_ID  ")
            _sb.AppendLine("     WHERE " & _where)
            _sb.AppendLine("       AND   MC_STATUS = @pStatus                                  ")
          End If

          Using _sql_cmd As New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
            _sql_cmd.Parameters.Add("@pCollectionId", SqlDbType.BigInt).Value = CollectionId
            _sql_cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = SessionId
            _sql_cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = TITO_MONEY_COLLECTION_STATUS.PENDING
            Using _sql_da As New SqlDataAdapter(_sql_cmd)
              _db_trx.Fill(_sql_da, _table)

            End Using
          End Using

        ElseIf m_operation_type = Cage.CageOperationType.FromCashier Or m_operation_type = Cage.CageOperationType.FromGamingTable Or _
          m_operation_type = Cage.CageOperationType.FromCashierClosing Or m_operation_type = Cage.CageOperationType.FromGamingTableClosing Or _
          m_operation_type = Cage.CageOperationType.FromGamingTableDropbox Or m_operation_type = Cage.CageOperationType.FromGamingTableDropboxWithExpected Then
          ' 09-NOV-2015 FOS & JPJ  Fixed Bug TFS-6235 We can not cancel a cashier withdraw
          _sb.AppendLine("     SELECT   TOP 1                                                           ")
          _sb.AppendLine("              MC_CASHIER_ID                                                   ")
          _sb.AppendLine("            , CT_NAME                                                         ")
          _sb.AppendLine("            , MC_DATETIME                                                     ")
          _sb.AppendLine("            , MC_EXTRACTION_DATETIME                                          ")
          'LEM 18-03-2014: Ticket info will be filled in FillTheoreticalDetailsInEditMode
          _sb.AppendLine("            , 0                                                               ") 'MC_EXPECTED_TICKET_COUNT  
          _sb.AppendLine("            , 0                                                               ") 'MC_EXPECTED_TICKET_AMOUNT 
          _sb.AppendLine("            , 0                                                               ") 'MC_EXPECTED_BILL_COUNT    
          _sb.AppendLine("            , 0                                                               ") 'MC_EXPECTED_BILL_AMOUNT   
          _sb.AppendLine("            , MC_COLLECTION_ID                                                ")
          _sb.AppendLine("            , MC_STATUS                                                       ")
          _sb.AppendLine("            , 0                                                               ") 'MC_STACKER_ID
          _sb.AppendLine("            , 0                                                               ") 'CT_CASHIER_ID
          _sb.AppendLine("            , MC_EXPECTED_COIN_AMOUNT                                         ")
          _sb.AppendLine("            , 0                                                               ")
          _sb.AppendLine("            , @pCashierSessionId                                              ") '
          _sb.AppendLine("            , NULL                                                            ")
          _sb.AppendLine("            , NULL                                                            ")
          '_sb.AppendLine("            , 0                                                               ") 'CGM_MOVEMENT_ID
          _sb.AppendLine("       FROM   MONEY_COLLECTIONS                                               ")
          _sb.AppendLine(" INNER JOIN   CASHIER_TERMINALS ON MC_CASHIER_ID          = CT_CASHIER_ID     ")
          _sb.AppendLine(" INNER JOIN   CASHIER_SESSIONS  ON MC_CASHIER_SESSION_ID  = CS_SESSION_ID     ")
          _sb.AppendLine("      WHERE   CS_SESSION_ID = @pCashierSessionId                              ")

          _sb.AppendLine("   GROUP BY   MC_CASHIER_ID, CT_NAME, MC_DATETIME, MC_STATUS                  ")
          _sb.AppendLine("            , MC_EXTRACTION_DATETIME, MC_COLLECTION_ID                        ")
          _sb.AppendLine("            , MC_EXPECTED_COIN_AMOUNT                                         ")

          Using _sql_cmd As New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
            _sql_cmd.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).Value = SessionId
            Using _sql_da As New SqlDataAdapter(_sql_cmd)
              _db_trx.Fill(_sql_da, _table)

            End Using
          End Using
        End If

        If _table.Rows.Count <= 0 Then
          If m_operation_type = Cage.CageOperationType.FromTerminal Then
            Me.m_type_new_collection.stacker_id = 0
            Me.m_type_new_collection.floor_id = Nothing
          End If

          Return False
        End If

        If _table.Rows(0).Item(SQL_COLUMN_COLLECTION_TERMINAL_ID_EDIT_MODE) IsNot DBNull.Value Then
          Me.m_type_new_collection.terminal_id = _table.Rows(0).Item(SQL_COLUMN_COLLECTION_TERMINAL_ID_EDIT_MODE)
        Else
          Me.m_type_new_collection.terminal_id = 0
        End If

        If _table.Rows(0).Item(SQL_COLUMN_COLLECTION_AMOUNT_IN_BILLS_EDIT_MODE) IsNot DBNull.Value Then
          Me.m_type_new_collection.theoretical_bills_sum = _table.Rows(0).Item(SQL_COLUMN_COLLECTION_AMOUNT_IN_BILLS_EDIT_MODE)
        Else
          Me.m_type_new_collection.theoretical_bills_sum = 0
        End If

        If _table.Rows(0).Item(5) IsNot DBNull.Value Then
          Me.m_type_new_collection.theoretical_ticket_sum = _table.Rows(0).Item(SQL_COLUMN_COLLECTION_AMOUNT_IN_TICKETS_EDIT_MODE)
        Else
          Me.m_type_new_collection.theoretical_ticket_sum = 0
        End If

        If _table.Rows(0).Item(SQL_COLUMN_COLLECTION_BILLS_COUNT_EDIT_MODE) IsNot DBNull.Value Then
          Me.m_type_new_collection.theoretical_bills_count = _table.Rows(0).Item(SQL_COLUMN_COLLECTION_BILLS_COUNT_EDIT_MODE)
        Else
          Me.m_type_new_collection.theoretical_bills_count = 0
        End If

        If _table.Rows(0).Item(SQL_COLUMN_COLLECTION_TICKETS_COUNT_EDIT_MODE) IsNot DBNull.Value Then
          Me.m_type_new_collection.theoretical_ticket_count = _table.Rows(0).Item(SQL_COLUMN_COLLECTION_TICKETS_COUNT_EDIT_MODE)
        Else
          Me.m_type_new_collection.theoretical_ticket_count = 0
        End If

        If _table.Rows(0).Item(SQL_COLUMN_COLLECTION_INSERTED_DATE_TIME_EDIT_MODE) IsNot DBNull.Value Then
          Me.m_type_new_collection.insertion_date.Value = _table.Rows(0).Item(SQL_COLUMN_COLLECTION_INSERTED_DATE_TIME_EDIT_MODE)
        End If

        If _table.Rows(0).Item(SQL_COLUMN_COLLECTION_EXTRACTION_DATE_TIME_EDIT_MODE) IsNot DBNull.Value Then
          Me.m_type_new_collection.extraction_date.Value = _table.Rows(0).Item(SQL_COLUMN_COLLECTION_EXTRACTION_DATE_TIME_EDIT_MODE)
        End If

        If _table.Rows(0).Item(SQL_COLUMN_COLLECTION_TERMINAL_NAME_EDIT_MODE) IsNot DBNull.Value Then
          Me.m_type_new_collection.terminal_name = _table.Rows(0).Item(SQL_COLUMN_COLLECTION_TERMINAL_NAME_EDIT_MODE)
        End If

        If _table.Rows(0).Item(SQL_COLUMN_COLLECTION_COLLECTION_ID_EDIT_MODE) IsNot DBNull.Value Then
          Me.m_type_new_collection.money_collection_id = _table.Rows(0).Item(SQL_COLUMN_COLLECTION_COLLECTION_ID_EDIT_MODE)
        End If

        If _table.Rows(0).Item(SQL_COLUMN_COLLECTION_STACKER_ID_EDIT_MODE) IsNot DBNull.Value Then
          Me.m_type_new_collection.stacker_id = _table.Rows(0).Item(SQL_COLUMN_COLLECTION_STACKER_ID_EDIT_MODE)
        End If

        If _table.Rows(0).Item(SQL_COLUMN_COLLECTION_COIN_COLLECTION_EDIT_MODE) IsNot DBNull.Value Then
          Me.m_type_new_collection.is_coin_collection_enabled = _table.Rows(0).Item(SQL_COLUMN_COLLECTION_COIN_COLLECTION_EDIT_MODE)
          If Not Me.m_type_new_collection.is_coin_collection_enabled AndAlso Not IsDBNull(_table.Rows(0).Item(SQL_COLUMN_COLLECTION_EXPECTED_COIN_COLLECTION_EDIT_MODE)) Then
            Me.m_type_new_collection.is_coin_collection_enabled = _table.Rows(0).Item(SQL_COLUMN_COLLECTION_EXPECTED_COIN_COLLECTION_EDIT_MODE) > 0
          End If
        End If

        If _table.Rows(0).Item(SQL_COLUMN_COLLECTION_TERMINAL_ID_ByPROMOBOX_EDIT_MODE) IsNot DBNull.Value Then
          If Not Me.IsCountR OrElse Me.TerminalCashierID = 0 Then
            Me.TerminalCashierID = _table.Rows(0).Item(SQL_COLUMN_COLLECTION_TERMINAL_ID_ByPROMOBOX_EDIT_MODE)
          End If
        End If

        If _table.Rows(0).Item(SQL_COLUMN_COLLECTION_CASHIER_SESSION_EDIT_MODE) IsNot DBNull.Value Then
          Me.CashierSessionId = _table.Rows(0).Item(SQL_COLUMN_COLLECTION_CASHIER_SESSION_EDIT_MODE)
        End If

        ' DHA 13-JAN-2016: get terminal currency
        Me.TerminalIsoCode = CurrencyExchange.GetNationalCurrency()
        If WSI.Common.Misc.IsFloorDualCurrencyEnabled() AndAlso m_operation_type = Cage.CageOperationType.FromTerminal Then
          If _table.Rows(0).Item(SQL_COLUMN_COLLECTION_TERMINAL_ISO_CODE_EDIT_MODE) IsNot DBNull.Value Then
            Me.TerminalIsoCode = _table.Rows(0).Item(SQL_COLUMN_COLLECTION_TERMINAL_ISO_CODE_EDIT_MODE)
          End If
        End If

        If _table.Rows(0).Item(SQL_COLUMN_COLLECTION_TERMINAL_FLOOR_ID_EDIT_MODE) IsNot DBNull.Value Then
          Me.m_type_new_collection.floor_id = _table.Rows(0).Item(SQL_COLUMN_COLLECTION_TERMINAL_FLOOR_ID_EDIT_MODE)
        End If

        If Not ReadCollectionDetailsEditionMode(_db_trx.SqlTransaction) Then
          Return False
        End If

        _db_trx.Commit()

        Return True
      End Using

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return False

  End Function ' ReadTheoreticalCollectionNewMode

  ''' <summary>
  ''' 
  ''' </summary>
  ''' <param name="IsCashierCollection"></param>
  ''' <param name="Trx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function InsertStackerCollection(ByVal IsCashierCollection As Boolean, ByVal Trx As SqlTransaction) As Boolean

    For Each _detail As TYPE_MONEY_COLLECTION_DETAILS In Me.m_type_new_collection.collection_details
      _detail.money_collection_id = Me.m_type_new_collection.money_collection_id
      If _detail.type = 2 AndAlso Not IsCashierCollection Then ' Bills
        If Not InsertStackerCollectionDetails(Me.m_type_new_collection.money_collection_id, Trx, _detail, False) Then
          ' Bills
          Return False
        End If
      ElseIf (_detail.ticket_cage_movement_id = Me.MovementID Or _detail.ticket_cage_movement_id = 0) Then
        If Not InsertStackerCollectionDetails(Me.m_type_new_collection.money_collection_id, Trx, _detail, True) Then
          ' Tickets
          Return False
        End If
      End If
    Next

    Return True
  End Function


#End Region

#Region " Private "

  Private Function ReadCollectionDetailsEditionMode(ByVal Trx As SqlTransaction) As Boolean

    Dim _sb As StringBuilder
    Dim _table As DataTable
    Dim _row As DataRow
    Dim _row_index As Int32
    Dim _where As String
    Dim _ticket_where As String

    If m_is_fill_out_movement AndAlso Not GeneralParam.GetBoolean("TITO", "Cashier.TicketsCollection") Then

      Return True
    End If

    _where = ""
    _ticket_where = ""
    _sb = New StringBuilder()
    _table = New DataTable()
    _row_index = 0

    If Me.m_type_new_collection.money_collection_id > 0 Then
      _where = "MC_COLLECTION_ID = @pCollectionId"
      _ticket_where = "(TI_COLLECTED_MONEY_COLLECTION = @pCollectionId OR TI_MONEY_COLLECTION_ID = @pCollectionId)"
    Else
      _where = "MC_CASHIER_SESSION_ID = @pSessionId"
      _ticket_where = "MC_CASHIER_SESSION_ID = @pSessionId"
    End If

    _sb.AppendLine("    SELECT   X.*                                                                   ")
    _sb.AppendLine("      INTO   #TMP                                                                  ")
    _sb.AppendLine("      FROM   (                                                                     ")
    _sb.AppendLine("        SELECT   0                               AS TYPE                           ")
    _sb.AppendLine("               , TI_VALIDATION_NUMBER            AS TI_VALIDATION_NUMBER           ")

    ' DHA 13-JAN-2016: get ticket amount converted to currency machine
    If WSI.Common.Misc.IsFloorDualCurrencyEnabled() And m_operation_type = Cage.CageOperationType.FromTerminal Then
      _sb.AppendLine("              , CASE WHEN TI_AMT1 IS NULL AND @pTerminalISOCode = @pNationalCurrency THEN TI_AMOUNT    ")
      _sb.AppendLine("                    WHEN TI_AMT1 IS NULL AND @pTerminalISOCode <> @pNationalCurrency THEN 0           ")
      _sb.AppendLine("                    WHEN TI_CUR1 <> @pTerminalISOCode AND @pTerminalISOCode = @pNationalCurrency    THEN TI_AMOUNT   ")
      _sb.AppendLine("                    WHEN TI_CUR1 <> @pTerminalISOCode AND @pTerminalISOCode <> @pNationalCurrency   THEN 0           ")
      _sb.AppendLine("                    ELSE TI_AMT1                                                                      ")
      _sb.AppendLine("               END AS TI_AMOUNT                                                                       ")
    Else
      _sb.AppendLine("               , TI_AMOUNT                       AS TI_AMOUNT                      ")
    End If

    _sb.AppendLine("               , NULL                            AS CTM_DENOMINATION               ")
    _sb.AppendLine("               , NULL                            AS CTM_QUANTITY                   ")
    _sb.AppendLine("               , TI_MONEY_COLLECTION_ID          AS TI_MONEY_COLLECTION_ID         ")
    _sb.AppendLine("               , TI_VALIDATION_TYPE              AS TI_VALIDATION_TYPE             ")
    _sb.AppendLine("               , TI_TICKET_ID                    AS ID                             ")
    _sb.AppendLine("               , TI_COLLECTED_MONEY_COLLECTION   AS TI_COLLECTED_MONEY_COLLECTION  ")
    _sb.AppendLine("               , TI_TYPE_ID                      AS TI_TICKET_TYPE                 ")
    _sb.AppendLine("               , TI_STATUS                       AS TICKET_STATUS                  ")
    _sb.AppendLine("               , TI_CAGE_MOVEMENT_ID             AS TI_CAGE_MOVEMENT_ID            ")
    _sb.AppendLine("               , TI_AMT0                         AS TI_AMT0                        ")
    _sb.AppendLine("               , TI_CUR0                         AS TI_CUR0                        ")
    _sb.AppendLine("          FROM   TICKETS                                                           ")
    _sb.AppendLine("    INNER JOIN   MONEY_COLLECTIONS ON TI_MONEY_COLLECTION_ID = MC_COLLECTION_ID    ")
    _sb.AppendLine("                                   OR TI_COLLECTED_MONEY_COLLECTION = MC_COLLECTION_ID ")
    _sb.AppendLine("         WHERE   " & _ticket_where)

    If m_operation_type = Cage.CageOperationType.FromCashier Or m_operation_type = Cage.CageOperationType.FromCashierClosing Then
      _sb.AppendLine("           AND  (TI_COLLECTED_MONEY_COLLECTION IS NULL OR TI_COLLECTED_MONEY_COLLECTION <> MC_COLLECTION_ID) ")
      _sb.AppendLine("           AND  (TI_CAGE_MOVEMENT_ID IS NULL OR TI_CAGE_MOVEMENT_ID <> @pMovementId)  ")
      _sb.AppendLine("           AND  TI_LAST_ACTION_DATETIME <= @pCageMovementDate                   ")

    ElseIf m_operation_type = Cage.CageOperationType.FromTerminal Then
      _sb.AppendLine("         UNION                                                                     ")
      If Not Me.m_promobox_or_acceptor Then
        _sb.AppendLine("        SELECT   2                       AS TIPO                                   ")
        _sb.AppendLine("               , NULL                    AS NULL1                                  ")
        _sb.AppendLine("               , NULL                    AS NULL2                                  ")
        _sb.AppendLine("               , CTM_DENOMINATION        AS CTM_DENOMINATION                       ")
        _sb.AppendLine("               , CTM_QUANTITY            AS CTM_QUANTITY                           ")
        _sb.AppendLine("               , NULL                    AS NULL3                                  ")
        _sb.AppendLine("               , NULL                    AS TI_VALIDATION_TYPE                     ")
        _sb.AppendLine("               , NULL                    AS NULL0                                  ")
        _sb.AppendLine("               , NULL                    AS NULL4                                  ")
        _sb.AppendLine("               , NULL                    AS NULL5                                  ")
        _sb.AppendLine("               , NULL                    AS NULL6                                  ")
        _sb.AppendLine("               , NULL                    AS NULL7                                  ")
        _sb.AppendLine("               , NULL                    AS NULL8                                  ")
        _sb.AppendLine("               , NULL                    AS NULL9                                  ")

        _sb.AppendLine("          FROM   CASHIER_TERMINAL_MONEY                                            ")
        _sb.AppendLine("     LEFT JOIN   MONEY_COLLECTIONS ON  CTM_MONEY_COLLECTION_ID = MC_COLLECTION_ID  ")
        _sb.AppendLine("         WHERE   (" & _where)
        _sb.AppendLine("           AND   MC_STATUS = @pStatus)                                             ")
        _sb.AppendLine("            OR   CTM_MONEY_COLLECTION_ID IS NULL                                   ")
        _sb.AppendLine("      GROUP BY   CTM_DENOMINATION, CTM_QUANTITY, CTM_MONEY_COLLECTION_ID           ")
      Else
        _sb.AppendLine("        SELECT   2                       AS TIPO                                   ")
        _sb.AppendLine("               , NULL                    AS NULL1                                  ")
        _sb.AppendLine("               , NULL                    AS NULL2                                  ")
        _sb.AppendLine("               , TM_AMOUNT               AS CTM_DENOMINATION                       ")
        _sb.AppendLine("               , COUNT(TM_AMOUNT)        AS CTM_QUANTITY                           ")
        _sb.AppendLine("               , NULL                    AS NULL3                                  ")
        _sb.AppendLine("               , NULL                    AS TI_VALIDATION_TYPE                     ")
        _sb.AppendLine("               , NULL                    AS NULL0                                  ")
        _sb.AppendLine("               , NULL                    AS NULL4                                  ")
        _sb.AppendLine("               , NULL                    AS NULL5                                  ")
        _sb.AppendLine("               , NULL                    AS NULL6                                  ")
        _sb.AppendLine("               , NULL                    AS NULL7                                  ")
        _sb.AppendLine("               , NULL                    AS NULL8                                  ")
        _sb.AppendLine("               , NULL                    AS NULL9                                  ")
        _sb.AppendLine("          FROM   TERMINAL_MONEY                                                    ")
        _sb.AppendLine("         WHERE  (TM_CASHIER_SESSION_ID = @pSessionId)                              ")
        _sb.AppendLine("      GROUP BY   TM_AMOUNT                                                         ")

      End If
    End If

    _sb.AppendLine("                   ) AS X                                                          ")
    _sb.AppendLine("    SELECT   *                                                                     ")
    _sb.AppendLine("      FROM   #TMP                                                                  ")

    If m_operation_type = Cage.CageOperationType.FromTerminal Then
      _sb.AppendLine(" UNION ALL                                                                         ")
      _sb.AppendLine("    SELECT   2                            AS TIPO                                  ")
      _sb.AppendLine("           , NULL                         AS NULL1                                 ")
      _sb.AppendLine("           , NULL                         AS NULL2                                 ")
      _sb.AppendLine("           , CUD_DENOMINATION             AS CTM_DENOMINATION                      ")
      _sb.AppendLine("           , NULL                         AS CTM_QUANTITY                          ")
      _sb.AppendLine("           , NULL                         AS NULL3                                 ")
      _sb.AppendLine("           , NULL                         AS TI_VALIDATION_TYPE                    ")
      _sb.AppendLine("           , NULL                         AS NULL0                                 ")
      _sb.AppendLine("           , NULL                         AS NULL4                                 ")
      _sb.AppendLine("           , NULL                         AS NULL5                                 ")
      _sb.AppendLine("           , NULL                         AS NULL6                                 ")
      _sb.AppendLine("           , NULL                         AS NULL7                                 ")
      _sb.AppendLine("           , NULL                         AS NULL8                                 ")
      _sb.AppendLine("           , NULL                         AS NULL9                                 ")
      _sb.AppendLine("      FROM   CURRENCY_DENOMINATIONS                                                ")
      _sb.AppendLine("     WHERE   CUD_ISO_CODE = @pTerminalISOCode                                      ")
      _sb.AppendLine("       AND   CUD_DENOMINATION NOT IN(                                              ")
      _sb.AppendLine("             SELECT   ISNULL(CTM_DENOMINATION,0)                                   ")
      _sb.AppendLine("               FROM   #TMP)                                                        ")

      'coins collection
      _sb.AppendLine(" UNION                                                                            ")
      _sb.AppendLine("   SELECT   3                               AS TYPE                               ")
      _sb.AppendLine("          , NULL                            AS NULL1                              ")
      _sb.AppendLine("          , MC_expected_coin_amount         AS MC_AMOUNT                          ")
      _sb.AppendLine("          , -50                             AS CTM_DENOMINATION                   ")
      _sb.AppendLine("          , NULL                            AS NULL2                              ")
      _sb.AppendLine("          , NULL                            AS NULL3                              ")
      _sb.AppendLine("          , NULL   AS NULL4                                                       ")
      _sb.AppendLine("          , NULL   AS NULL5                                                       ")
      _sb.AppendLine("          , NULL   AS NULL6                                                       ")
      _sb.AppendLine("          , NULL   AS NULL7                                                       ")
      _sb.AppendLine("          , NULL   AS NULL8                                                       ")
      _sb.AppendLine("          , NULL   AS NULL9                                                       ")
      _sb.AppendLine("          , NULL   AS NULL10                                                      ")
      _sb.AppendLine("          , NULL   AS NULL11                                                      ")
      _sb.AppendLine("     FROM   MONEY_COLLECTIONS                                                     ")
      _sb.AppendLine("    WHERE   MC_COLLECTION_ID = @pCollectionId")
      _sb.AppendLine("  ORDER BY   CTM_DENOMINATION")

    End If

    _sb.AppendLine("      DROP   TABLE #TMP ")

    Try

      Using _sql_cmd As New SqlCommand(_sb.ToString(), Trx.Connection, Trx)
        _sql_cmd.Parameters.Add("@pStackerId", SqlDbType.BigInt).Value = Me.m_type_new_collection.stacker_id
        _sql_cmd.Parameters.Add("@pCollectionId", SqlDbType.BigInt).Value = Me.m_type_new_collection.money_collection_id
        If Not Me.IsPromoBoxOrAcceptor Then
          _sql_cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = Me.m_session_id
        Else
          _sql_cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = Me.CashierSessionId
        End If
        _sql_cmd.Parameters.Add("@pStatus", SqlDbType.Bit).Value = TITO_MONEY_COLLECTION_STATUS.PENDING

        ' DHA 13-JAN-2016: get currency terminal only
        If WSI.Common.Misc.IsFloorDualCurrencyEnabled() Then
          _sql_cmd.Parameters.Add("@pTerminalISOCode", SqlDbType.VarChar).Value = Me.TerminalIsoCode
          _sql_cmd.Parameters.Add("@pNationalCurrency", SqlDbType.VarChar).Value = CurrencyExchange.GetNationalCurrency()
        Else
          _sql_cmd.Parameters.Add("@pTerminalISOCode", SqlDbType.VarChar).Value = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode")
        End If

        If m_operation_type = Cage.CageOperationType.FromCashier Or m_operation_type = Cage.CageOperationType.FromCashierClosing Then
          _sql_cmd.Parameters.Add("@pCageMovementDate", SqlDbType.DateTime).Value = m_movement_datetime
        End If
        _sql_cmd.Parameters.Add("@pMovementId", SqlDbType.BigInt).Value = Me.MovementID
        Using _sql_da As New SqlDataAdapter(_sql_cmd)
          _sql_da.Fill(_table)
        End Using
      End Using

      For Each _row In _table.Rows
        Call FillTheoreticalDetailsInEditMode(_row_index, _row)
        _row_index = _row_index + 1
      Next

      Return True

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return False

  End Function ' ReadCollectionDetailsEditionMode

  Private Sub FillTheoreticalDetailsInEditMode(ByVal RowIndex As Int32, ByVal DbRow As DataRow)

    Dim _collection_detail As TYPE_MONEY_COLLECTION_DETAILS

    _collection_detail = New TYPE_MONEY_COLLECTION_DETAILS()

    If Not IsDBNull(DbRow.Item(SQL_COLUMN_DENOMINATION_EDIT_MODE)) _
      AndAlso DbRow.Item(SQL_COLUMN_DENOMINATION_EDIT_MODE) = Cage.COINS_COLLECTION_CODE _
      AndAlso Not IsDBNull(DbRow.Item(SQL_COLUMN_TICKET_THEORETICAL_VALUE_EDIT_MODE)) Then

      'TOTAL GRID
      _collection_detail.coin_total_from_terminals = DbRow.Item(SQL_COLUMN_TICKET_THEORETICAL_VALUE_EDIT_MODE)

    Else


      If Not IsDBNull(DbRow.Item(SQL_COLUMN_TICKET_OR_NOTE_EDIT_MODE)) Then
        If DbRow.Item(SQL_COLUMN_TICKET_OR_NOTE_EDIT_MODE) = 0 AndAlso WSI.Common.GeneralParam.GetInt32("TITO", "TicketsCollection") = 0 Then
          _collection_detail.type = 1
        Else
          _collection_detail.type = 0
        End If
      End If

      If Not IsDBNull(DbRow.Item(SQL_COLUMN_TICKET_NUMBER_EDIT_MODE)) Then
        _collection_detail.ticket_validation_number = DbRow.Item(SQL_COLUMN_TICKET_NUMBER_EDIT_MODE)
      End If


      If Not IsDBNull(DbRow.Item(SQL_COLUMN_DENOMINATION_EDIT_MODE)) Then
        _collection_detail.bill_denomination = DbRow.Item(SQL_COLUMN_DENOMINATION_EDIT_MODE)
      End If


      If Not IsDBNull(DbRow.Item(SQL_COLUMN_TICKET_THEORETICAL_VALUE_EDIT_MODE)) Then
        _collection_detail.ticket_amount = DbRow.Item(SQL_COLUMN_TICKET_THEORETICAL_VALUE_EDIT_MODE)
      End If


      If Not IsDBNull(DbRow.Item(SQL_COLUMN_THEORETICAL_QUANTITY_EDIT_MODE)) Then
        _collection_detail.bill_theoretical_count = DbRow.Item(SQL_COLUMN_THEORETICAL_QUANTITY_EDIT_MODE)
        _collection_detail.bill_real_vs_theo_count_diff = -_collection_detail.bill_theoretical_count
      End If

      If Not IsDBNull(DbRow.Item(SQL_COLUMN_TICKET_THEORETICAL_MONEY_COLLECTION_ID)) Then
        _collection_detail.money_collection_id = DbRow.Item(SQL_COLUMN_TICKET_THEORETICAL_MONEY_COLLECTION_ID)
      End If

      If Not IsDBNull(DbRow.Item(SQL_COLUMN_TICKET_VALIDATION_TYPE_EDIT_MODE)) Then
        _collection_detail.ticket_validation_type = DbRow.Item(SQL_COLUMN_TICKET_VALIDATION_TYPE_EDIT_MODE)
      End If

      If Not IsDBNull(DbRow.Item(SQL_COLUMN_TICKET_ID)) Then
        _collection_detail.ticket_id = DbRow.Item(SQL_COLUMN_TICKET_ID)
      Else
        _collection_detail.ticket_id = -1
      End If

      If Not IsDBNull(DbRow.Item(SQL_COLUMN_TICKET_REAL_MONEY_COLLECTION_ID)) Then
        _collection_detail.ticketd_collected_money_collection = DbRow.Item(SQL_COLUMN_TICKET_REAL_MONEY_COLLECTION_ID)
      End If

      If Not IsDBNull(DbRow.Item(SQL_COLUMN_TICKET_CAGE_MOVEMENT_ID)) Then
        _collection_detail.ticket_cage_movement_id = DbRow.Item(SQL_COLUMN_TICKET_CAGE_MOVEMENT_ID)
      End If

      If Not IsDBNull(DbRow.Item(SQL_COLUMN_TICKET_TYPE)) Then
        _collection_detail.ticket_type = DbRow.Item(SQL_COLUMN_TICKET_TYPE)
      End If

      If Not IsDBNull(DbRow.Item(SQL_COLUMN_TICKET_STATUS)) Then
        _collection_detail.ticket_status = DbRow.Item(SQL_COLUMN_TICKET_STATUS)
      End If

      ' DHA 13-JAN-2016: Set ticket issue amount/ISO Code
      If Not IsDBNull(DbRow.Item(SQL_COLUMN_TICKET_AMOUNT_ISSUE)) Then
        _collection_detail.ticket_amount_issue = DbRow.Item(SQL_COLUMN_TICKET_AMOUNT_ISSUE)
      End If

      If Not IsDBNull(DbRow.Item(SQL_COLUMN_TICKET_CURRENCY_ISSUE)) Then
        _collection_detail.ticket_currency_issue = DbRow.Item(SQL_COLUMN_TICKET_CURRENCY_ISSUE)
      End If

    End If

    Me.m_type_new_collection.collection_details.Add(_collection_detail)

    If (m_operation_type = Cage.CageOperationType.FromCashier Or m_operation_type = Cage.CageOperationType.FromCashierClosing) _
      AndAlso _collection_detail.ticket_amount > 0 Then
      If _collection_detail.ticketd_collected_money_collection = m_type_new_collection.money_collection_id Or WSI.Common.GeneralParam.GetInt32("TITO", "Cashier.TicketsCollection") = 0 Then
        'real
        m_type_new_collection.real_ticket_count += 1
        m_type_new_collection.real_ticket_sum += _collection_detail.ticket_amount
      End If
      If _collection_detail.money_collection_id = m_type_new_collection.money_collection_id Then
        'expected
        m_type_new_collection.theoretical_ticket_count += 1
        m_type_new_collection.theoretical_ticket_sum += _collection_detail.ticket_amount
      End If
    ElseIf m_operation_type = Cage.CageOperationType.FromTerminal AndAlso _collection_detail.ticket_amount > 0 AndAlso _collection_detail.type = 1 Then
      'real
      m_type_new_collection.real_ticket_count += 1
      m_type_new_collection.real_ticket_sum += _collection_detail.ticket_amount
    End If

  End Sub ' FillTheoreticalDetailsInEditMode

  ' PURPOSE: Update Money Collection
  '
  ' PARAMS:
  '
  ' RETURNS:
  '     - 0: Ok 
  '     - 1: Error 
  '
  ' NOTES:
  Private Function UpdateStackerCollection() As Int32
    Dim _result As Int32
    Dim _dt_currencies_types As DataTable

    _dt_currencies_types = GetAllowedISOCodeWithoutColor()

    Using _db_trx As New DB_TRX()
      _result = UpdateStackerCollection(_db_trx.SqlTransaction)

      If _result = 0 Then
        If Me.UpdateCageMeters(_db_trx.SqlTransaction) = ENUM_CONFIGURATION_RC.CONFIGURATION_OK Then
          If WSI.Common.CageMeters.CreateItemsMovementDetails(Me.MovementID, Me.ConceptsData, Me.TableCurrencies, _dt_currencies_types, Me.m_type_new_collection.real_ticket_sum, _db_trx.SqlTransaction) Then
            _db_trx.Commit()
          End If
        End If
      End If

    End Using

    Return _result

  End Function

  Private Function UpdateStackerCollection(ByVal Trx As SqlTransaction) As Int32
    Dim _sb As StringBuilder
    Dim _table As DataTable
    Dim _is_cashier_collection As Boolean
    Dim _terminal_name As String
    Dim _terminal_id As Int64
    Dim _session_data As New WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS
    Dim _row As DataRow

    _sb = New StringBuilder()
    _table = New DataTable()
    _is_cashier_collection = (Me.OperationType = Cage.CageOperationType.FromCashier Or Me.OperationType = Cage.CageOperationType.FromCashierClosing Or _
                              Me.OperationType = Cage.CageOperationType.FromGamingTable Or Me.OperationType = Cage.CageOperationType.FromGamingTableClosing Or _
                              Me.OperationType = Cage.CageOperationType.FromGamingTableDropbox Or Me.OperationType = Cage.CageOperationType.FromGamingTableDropboxWithExpected)
    _terminal_id = Me.m_type_new_collection.terminal_id

    If Not Utils.IsTitoMode() And Not Me.IsPromoBoxOrAcceptor Then
      Me.m_type_new_collection.already_collected = True

      Return 0
    End If

    If Me.m_type_new_collection.money_collection_id <= 0 Then

      Return 1
    End If

    If Not _is_cashier_collection Then
      If Me.MovementID > 0 Then
        If Not Me.UpdateMovement(Me.Status, Trx, True) Then

          Return 1
        End If
      Else
        If Not Me.InsertMovement(Trx) Then

          Return 1
        End If
      End If
      ' Update Cage Stock.
      For Each _row In Me.TableCurrencies.Select("QUANTITY > 0 OR TOTAL > 0")

        If Not UpdateCageStock(Trx, _row) Then

          Return 1
        End If

      Next
    End If

    For Each _detail As TYPE_MONEY_COLLECTION_DETAILS In Me.m_type_new_collection.collection_details
      _detail.money_collection_id = Me.m_type_new_collection.money_collection_id
      If _detail.type = 2 AndAlso Not _is_cashier_collection Then ' Bills
        If Not InsertStackerCollectionDetails(Me.m_type_new_collection.money_collection_id, Trx, _detail, False) Then
          ' Bills
          Return 1
        End If
      ElseIf (_detail.ticket_cage_movement_id = Me.MovementID Or _detail.ticket_cage_movement_id = 0) Then
        If Not InsertStackerCollectionDetails(Me.m_type_new_collection.money_collection_id, Trx, _detail, True) Then
          ' Tickets
          Return 1
        End If
      End If

    Next

    Try

      If _is_cashier_collection AndAlso Not m_is_fill_out_movement Then
        _sb.AppendLine(" DECLARE   @pPreviousStatus AS INT                        ")
        _sb.AppendLine("  SELECT   @pPreviousStatus = CASE WHEN MC_COLLECTED_TICKET_AMOUNT = MC_EXPECTED_TICKET_AMOUNT ")
        _sb.AppendLine("                                    AND MC_COLLECTED_TICKET_COUNT  = MC_EXPECTED_TICKET_COUNT  ")
        _sb.AppendLine("                              THEN @pBalanced             ")
        _sb.AppendLine("                              ELSE @pImbalanced           ")
        _sb.AppendLine("                         END                              ")
        _sb.AppendLine("    FROM   MONEY_COLLECTIONS                              ")
        _sb.AppendLine("   WHERE   MC_COLLECTION_ID = @pCollectionId              ")
        _sb.AppendLine(" SELECT @pNewStatus = CASE WHEN @pPreviousStatus = @pNewStatus THEN @pNewStatus ELSE @pImbalanced END ")
      End If

      _sb.AppendLine("  UPDATE   MONEY_COLLECTIONS                                           ")
      _sb.AppendLine("     SET   MC_USER_ID = @pUserId                                       ")
      If Not m_is_fill_out_movement Then
        _sb.AppendLine("         , MC_COLLECTION_DATETIME = @pCollectionDateTime               ")
        _sb.AppendLine("         , MC_NOTES = @pNotes                                          ")
        _sb.AppendLine("         , MC_STATUS = @pNewStatus                                     ")
        _sb.AppendLine("         , MC_NOTE_COUNTER_EVENTS = @pNoteCounterEvent                 ")
      End If
      _sb.AppendLine("         , MC_COLLECTED_BILL_AMOUNT = @pBillAmount                                ")
      _sb.AppendLine("         , MC_COLLECTED_BILL_COUNT = @pBillCount                                  ")
      _sb.AppendLine("         , MC_COLLECTED_TICKET_AMOUNT = ISNULL(MC_COLLECTED_TICKET_AMOUNT,0) + @pTicketAmount                          ")
      _sb.AppendLine("         , MC_COLLECTED_TICKET_COUNT = ISNULL(MC_COLLECTED_TICKET_COUNT,0) + @pTicketCount                             ")
      _sb.AppendLine("         , MC_COLLECTED_RE_TICKET_AMOUNT =  ISNULL(MC_COLLECTED_RE_TICKET_AMOUNT,0) + @pReTicketAmount                 ")
      _sb.AppendLine("         , MC_COLLECTED_RE_TICKET_COUNT = ISNULL(MC_COLLECTED_RE_TICKET_COUNT,0) + @pReTicketCount                     ")
      _sb.AppendLine("         , MC_COLLECTED_PROMO_RE_TICKET_AMOUNT = ISNULL(MC_COLLECTED_PROMO_RE_TICKET_AMOUNT,0) + @pPromoReTicketAmount ")
      _sb.AppendLine("         , MC_COLLECTED_PROMO_RE_TICKET_COUNT = ISNULL(MC_COLLECTED_PROMO_RE_TICKET_COUNT,0) + @pPromoReTicketCount    ")
      _sb.AppendLine("         , MC_COLLECTED_PROMO_NR_TICKET_AMOUNT = ISNULL(MC_COLLECTED_PROMO_NR_TICKET_AMOUNT,0) + @pPromoNRTicketAmount ")
      _sb.AppendLine("         , MC_COLLECTED_PROMO_NR_TICKET_COUNT = ISNULL(MC_COLLECTED_PROMO_NR_TICKET_COUNT,0) + @pPromoNRTicketCount    ")
      _sb.AppendLine("         , MC_COLLECTED_COIN_AMOUNT  =  @pCoinCollectionAmount        ")

      _sb.AppendLine("  OUTPUT   INSERTED.MC_COLLECTION_ID                      ")
      If _is_cashier_collection Then
        _sb.AppendLine("         , ISNULL(INSERTED.MC_TERMINAL_ID, 0)           ")
      Else
        _sb.AppendLine("         , INSERTED.MC_TERMINAL_ID                      ")
      End If
      _sb.AppendLine("         , INSERTED.MC_CASHIER_SESSION_ID                 ")
      _sb.AppendLine("   WHERE   MC_COLLECTION_ID = @pCollectionId              ")
      If Not Me.IsPromoBoxOrAcceptor And Not m_is_fill_out_movement Then
        _sb.AppendLine("     AND   MC_STATUS = @pOldStatus                        ")
      End If

      Using _cmd As SqlCommand = New SqlCommand(_sb.ToString(), Trx.Connection, Trx)

        _cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = Me.m_type_new_collection.user_id
        _cmd.Parameters.Add("@pCollectionDateTime", SqlDbType.DateTime).Value = Me.m_type_new_collection.collection_date.Value
        _cmd.Parameters.Add("@pNotes", SqlDbType.NVarChar, 200).Value = Me.m_type_new_collection.notes
        _cmd.Parameters.Add("@pStackerId", SqlDbType.BigInt).Value = Me.m_type_new_collection.stacker_id
        _cmd.Parameters.Add("@pCollectionId", SqlDbType.BigInt).Value = Me.m_type_new_collection.money_collection_id
        _cmd.Parameters.Add("@pNewStatus", SqlDbType.Int).Value = IIf(Me.m_type_new_collection.balanced, TITO_MONEY_COLLECTION_STATUS.CLOSED_BALANCED, TITO_MONEY_COLLECTION_STATUS.CLOSED_IN_IMBALANCE)
        _cmd.Parameters.Add("@pOldStatus", SqlDbType.Int).Value = TITO_MONEY_COLLECTION_STATUS.PENDING
        _cmd.Parameters.Add("@pBalanced", SqlDbType.Int).Value = TITO_MONEY_COLLECTION_STATUS.CLOSED_BALANCED
        _cmd.Parameters.Add("@pImbalanced", SqlDbType.Int).Value = TITO_MONEY_COLLECTION_STATUS.CLOSED_IN_IMBALANCE
        _cmd.Parameters.Add("@pBillAmount", SqlDbType.Decimal).Value = Me.m_type_new_collection.real_bills_sum
        _cmd.Parameters.Add("@pBillCount", SqlDbType.Int).Value = Me.m_type_new_collection.real_bills_count
        _cmd.Parameters.Add("@pTicketAmount", SqlDbType.Decimal).Value = Me.m_type_new_collection.real_ticket_sum
        _cmd.Parameters.Add("@pTicketCount", SqlDbType.Int).Value = Me.m_type_new_collection.real_ticket_count
        _cmd.Parameters.Add("@pReTicketAmount", SqlDbType.Decimal).Value = Me.m_type_new_collection.real_redeem_ticket_amount
        _cmd.Parameters.Add("@pReTicketCount", SqlDbType.Int).Value = Me.m_type_new_collection.real_redeem_ticket_count
        _cmd.Parameters.Add("@pPromoReTicketAmount", SqlDbType.Decimal).Value = Me.m_type_new_collection.real_promo_redeem_ticket_amount
        _cmd.Parameters.Add("@pPromoReTicketCount", SqlDbType.Int).Value = Me.m_type_new_collection.real_promo_redeem_ticket_count
        _cmd.Parameters.Add("@pPromoNRTicketAmount", SqlDbType.Decimal).Value = Me.m_type_new_collection.real_non_redeem_ticket_amount
        _cmd.Parameters.Add("@pPromoNRTicketCount", SqlDbType.Int).Value = Me.m_type_new_collection.real_non_redeem_ticket_count
        _cmd.Parameters.Add("@pNoteCounterEvent", SqlDbType.Xml).Value = IIf(String.IsNullOrEmpty(Me.m_type_new_collection.note_counter_events), DBNull.Value, Me.m_type_new_collection.note_counter_events)
        _cmd.Parameters.Add("@pCoinCollectionAmount", SqlDbType.Decimal).Value = Me.m_type_new_collection.real_coins_collection_amount

        Using _adapter As New SqlDataAdapter(_cmd)
          _adapter.Fill(_table)
        End Using

        If _table.Rows.Count = 0 _
           OrElse _table.Rows(0).Item(0) Is DBNull.Value _
                  OrElse _table.Rows(0).Item(1) Is DBNull.Value _
                         OrElse _table.Rows(0).Item(1) Is DBNull.Value Then

          Return 1
        End If

        Me.m_type_new_collection.money_collection_id = _table.Rows(0).Item(0)
        Me.m_type_new_collection.terminal_id = _table.Rows(0).Item(1)
        Me.m_session_id = _table.Rows(0).Item(2)
        Me.CashierSessionId = Me.m_session_id

      End Using

      If Me.m_type_new_collection.money_collection_id > 0 Then

        If Not _is_cashier_collection Then
          'TODO: Quitar comentario. esta puesto porque hay problemas con
          If m_type_new_collection.real_bills_sum > 0 Then
            If Not mdl_tito.NACardCashDeposit(Me.CashierSessionId, Me.GuiUserType _
                                                      , m_type_new_collection.real_bills_sum + Me.m_type_new_collection.real_coins_collection_amount _
                                                      , Me.m_type_new_collection.terminal_id _
                                                      , Trx) Then

              Return 1
            End If

          End If
        End If

        If Not _is_cashier_collection Then
          If Me.MovementID > 0 Then
            If Not Me.UpdateMovement(Me.Status, Trx, True) Then
              Return 1
            End If
          Else
            If Not Me.InsertMovement(Trx) Then
              Return 1
            End If
          End If
        End If

      End If

      Me.m_type_new_collection.already_collected = True

      _terminal_name = Environment.MachineName
      _terminal_id = Cashier.ReadTerminalId(Trx, _terminal_name, GU_USER_TYPE.USER)
      WSI.Common.Cashier.ReadCashierSessionData(Trx, m_session_id, _session_data)

      If Me.OperationType = Cage.CageOperationType.FromTerminal AndAlso Not CloseSessionTITO(Trx) Then

        Return 1
      End If

      Call Me.DeleteFromPendingMovement(Trx)

    Catch _ex As Exception

      Return 1
    Finally

    End Try

    Return 0
  End Function ' UpdateStackerCollection

  ''' <summary>
  ''' Initialize m_type_new_collection
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub InitializeTypeNewCollection()
    Me.m_type_new_collection = New NEW_COLLECTION.TYPE_NEW_COLLECTION()
    Me.m_type_new_collection.collection_date = New TYPE_DATE_TIME()
    Me.m_type_new_collection.insertion_date = New TYPE_DATE_TIME()
    Me.m_type_new_collection.extraction_date = New TYPE_DATE_TIME()
    Me.m_type_new_collection.collection_details = New List(Of NEW_COLLECTION.TYPE_MONEY_COLLECTION_DETAILS)()
    Me.m_is_fill_out_movement = False
  End Sub ' InitializeTypeNewCollection

  Private Function InsertStackerCollectionDetails(ByVal StackerCollectionId As Int64, ByVal Trx As SqlTransaction, ByVal Detail As TYPE_MONEY_COLLECTION_DETAILS, ByVal IsTicket As Boolean) As Boolean

    Dim _sb As StringBuilder

    _sb = New StringBuilder()
    Try
      Using _cmd As SqlCommand = New SqlCommand("", Trx.Connection, Trx)
        If IsTicket Then
          ' hacer el update de la tabla ticket
          _sb.AppendLine("UPDATE   TICKETS                                                   ")
          _sb.AppendLine("   SET   TI_COLLECTED_MONEY_COLLECTION = @CollectedMoneyCollection ")
          _sb.AppendLine("       , TI_CAGE_MOVEMENT_ID = @pMovementId                        ")
          _sb.AppendLine(" WHERE   TI_TICKET_ID = @pTicketId                                 ")
          ' _sb.AppendLine("   AND   TI_MONEY_COLLECTION_ID = @pCollectionId    ")

          _cmd.CommandText = _sb.ToString()
          _cmd.Parameters.Add("@CollectedMoneyCollection", SqlDbType.BigInt).Value = Detail.money_collection_id
          _cmd.Parameters.Add("@pTicketId", SqlDbType.BigInt).Value = Detail.ticket_id
          _cmd.Parameters.Add("@pCollectionId", SqlDbType.BigInt).Value = Detail.money_collection_id
          _cmd.Parameters.Add("@pMovementId", SqlDbType.BigInt).Value = m_movement_id

        Else
          _sb.AppendLine("INSERT INTO   MONEY_COLLECTION_DETAILS  ")
          _sb.AppendLine("            ( MCD_COLLECTION_ID         ")
          _sb.AppendLine("            , MCD_FACE_VALUE            ")
          _sb.AppendLine("            , MCD_NUM_COLLECTED         ")
          _sb.AppendLine("            , MCD_NUM_EXPECTED          ")
          _sb.AppendLine("            , MCD_CAGE_CURRENCY_TYPE )  ")
          _sb.AppendLine("     VALUES                             ")
          _sb.AppendLine("            ( @pCollectionId            ")
          _sb.AppendLine("            , @pFaceValue               ")
          _sb.AppendLine("            , @pNumCollected            ")
          _sb.AppendLine("            , @pNumExpected             ")
          _sb.AppendLine("            , @pCurrencyType )          ")

          _cmd.CommandText = _sb.ToString()
          _cmd.Parameters.Add("@pCollectionId", SqlDbType.BigInt).Value = Detail.money_collection_id
          _cmd.Parameters.Add("@pFaceValue", SqlDbType.Money).Value = Detail.bill_denomination
          _cmd.Parameters.Add("@pNumCollected", SqlDbType.Money).Value = Detail.bill_real_count

          If Detail.currency_type = CageCurrencyType.Coin Then
            _cmd.Parameters.Add("@pNumExpected", SqlDbType.Money).Value = Detail.bill_real_count
          Else
            _cmd.Parameters.Add("@pNumExpected", SqlDbType.Money).Value = Detail.bill_theoretical_count
          End If

          If Detail.currency_type = CageCurrencyType.Others Then
            _cmd.Parameters.Add("@pCurrencyType", SqlDbType.Int).Value = CInt(CageCurrencyType.Bill)
          Else
            _cmd.Parameters.Add("@pCurrencyType", SqlDbType.Int).Value = CInt(Detail.currency_type)
          End If

        End If

        If _cmd.ExecuteNonQuery() = 1 Then
          Return True
        End If

      End Using
    Catch _ex As Exception

    End Try

    Return False

  End Function ' InsertStackerCollectionDetails

  Private Function UpdateStackerStatus(ByVal Trx As SqlTransaction) As Boolean
    Dim _sb As StringBuilder

    _sb = New StringBuilder()

    'if StackerId = -1 it means that the collection is being done without stacker
    If Me.m_type_new_collection.stacker_id < 1 Then
      Return True
    End If

    Try
      _sb.AppendLine("UPDATE   STACKERS                    ")
      _sb.AppendLine("   SET   ST_INSERTED = NULL          ")
      _sb.AppendLine(" WHERE   ST_STACKER_ID = @pStackerId ")

      Using _cmd As SqlCommand = New SqlCommand(_sb.ToString(), Trx.Connection, Trx)
        _cmd.Parameters.Add("@pStackerId", SqlDbType.BigInt).Value = m_type_new_collection.stacker_id

        Return _cmd.ExecuteNonQuery() = 1

      End Using
    Catch ex As Exception
    End Try

    Return False
  End Function ' UpdateStackerStatus

  Private Function ReadMoneyCollection(ByVal MoneyCollectionId As Int64, Optional ByVal CashierSessionId As Int64 = -1) As Boolean
    Dim _result As Boolean
    Dim _sb As StringBuilder
    Dim _table As DataTable

    _sb = New StringBuilder()
    _table = New DataTable()
    _result = True

    _sb.AppendLine("     SELECT   GU_USERNAME                                                        ")
    If m_operation_type = Cage.CageOperationType.FromTerminal Then
      _sb.AppendLine("            , TE_NAME                                                          ")
    ElseIf m_operation_type = Cage.CageOperationType.FromCashier _
      Or m_operation_type = Cage.CageOperationType.FromCashierClosing _
      Or m_operation_type = Cage.CageOperationType.FromGamingTableDropbox _
      Or m_operation_type = Cage.CageOperationType.FromGamingTableDropboxWithExpected Then
      _sb.AppendLine("            , CT_NAME                                                          ")
    End If
    _sb.AppendLine("            , ISNULL(MC_EXPECTED_TICKET_AMOUNT, 0) AS THEO_TICKET_SUM            ")
    _sb.AppendLine("            , ISNULL(MC_EXPECTED_BILL_AMOUNT, 0) + ISNULL(MC_EXPECTED_COIN_AMOUNT, 0) AS THEO_BILLS_SUM   ")
    _sb.AppendLine("            , ISNULL(MC_COLLECTED_BILL_AMOUNT, 0) + ISNULL(MC_COLLECTED_COIN_AMOUNT, 0) AS REAL_BILLS_SUM  ")
    _sb.AppendLine("            , ISNULL(MC_COLLECTED_BILL_COUNT, 0) AS REAL_BILLS_COUNT             ")
    _sb.AppendLine("            , ISNULL(MC_COLLECTED_TICKET_AMOUNT, 0)  AS REAL_TICKET_SUM          ")
    _sb.AppendLine("            , MC_COLLECTION_ID                                                   ")
    _sb.AppendLine("            , MC_DATETIME                                                        ")
    _sb.AppendLine("            , MC_TERMINAL_ID                                                     ")
    _sb.AppendLine("            , MC_USER_ID                                                         ")
    _sb.AppendLine("            , MC_NOTES                                                           ")
    _sb.AppendLine("            , MC_STACKER_ID                                                      ")
    _sb.AppendLine("            , MC_INSERTED_USER_ID                                                ")
    _sb.AppendLine("            , MC_COLLECTION_DATETIME                                             ")
    _sb.AppendLine("            , MC_EXTRACTION_DATETIME                                             ")
    _sb.AppendLine("            , MC_STATUS                                                          ")
    _sb.AppendLine("            , MC_CASHIER_SESSION_ID                                              ")
    _sb.AppendLine("            , MC_EXPECTED_COIN_AMOUNT                                            ")
    _sb.AppendLine("            , TE_COIN_COLLECTION                                                 ")
    _sb.AppendLine("            , ISNULL(MC_NOTE_COUNTER_EVENTS, '')                                 ")

    ' DHA 13-JAN-2016: get terminal currency
    If WSI.Common.Misc.IsFloorDualCurrencyEnabled() And m_operation_type = Cage.CageOperationType.FromTerminal Then
      _sb.AppendLine("          , TE_ISO_CODE                                                       ")
    Else
      _sb.AppendLine("          , NULL                                                              ")
    End If

    If m_operation_type = Cage.CageOperationType.FromTerminal Then
      _sb.AppendLine("            , TE_FLOOR_ID                                                     ")
    Else
      _sb.AppendLine("            , NULL                                                            ")
    End If

    _sb.AppendLine("       FROM   MONEY_COLLECTIONS                                                 ")

    Try

      Using _db_trx As New DB_TRX()

        If m_operation_type = Cage.CageOperationType.FromTerminal Then

          _sb.AppendLine(" LEFT JOIN   GUI_USERS ON GU_USER_ID = MC_USER_ID                      ")
          _sb.AppendLine(" LEFT JOIN   TERMINALS ON TE_TERMINAL_ID = MC_TERMINAL_ID              ")
          _sb.AppendLine("     WHERE   MC_COLLECTION_ID = @pStackerCollectionId                  ")

          Using _sql_cmd As New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
            _sql_cmd.Parameters.Add("@pStackerCollectionId", SqlDbType.BigInt).Value = MoneyCollectionId

            Using _sql_da As New SqlDataAdapter(_sql_cmd)
              _db_trx.Fill(_sql_da, _table)
            End Using

          End Using

        ElseIf m_operation_type = Cage.CageOperationType.FromCashier _
          Or m_operation_type = Cage.CageOperationType.FromCashierClosing _
          Or m_operation_type = Cage.CageOperationType.FromGamingTableDropbox _
          Or m_operation_type = Cage.CageOperationType.FromGamingTableDropboxWithExpected Then
          ' 05-NOV-2015 FOS & JPJ  Fixed Bug TFS-6007, 6235, 6238, 6242: GUI: error in total
          _sb.AppendLine(" INNER JOIN   CASHIER_SESSIONS  ON MC_CASHIER_SESSION_ID  = CS_SESSION_ID        ")
          _sb.AppendLine("  LEFT JOIN   CASHIER_TERMINALS ON MC_CASHIER_ID          = CT_CASHIER_ID        ")
          _sb.AppendLine("  LEFT JOIN   GUI_USERS ON GU_USER_ID = MC_USER_ID                               ")
          _sb.AppendLine("  LEFT JOIN   TERMINALS ON TE_TERMINAL_ID = MC_TERMINAL_ID                       ")
          _sb.AppendLine("      WHERE   MC_CASHIER_SESSION_ID    = @pSessionId                             ")

          Using _sql_cmd As New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
            _sql_cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = CashierSessionId

            Using _sql_da As New SqlDataAdapter(_sql_cmd)
              _db_trx.Fill(_sql_da, _table)
            End Using

          End Using

        End If

        If _table.Rows.Count > 0 Then

          If _table.Rows(0).Item(SQL_COLUMN_COLLECTION_TERMINAL_NAME) IsNot DBNull.Value Then
            Me.m_type_new_collection.terminal_name = _table.Rows(0).Item(SQL_COLUMN_COLLECTION_TERMINAL_NAME)
          Else
            Me.m_type_new_collection.terminal_name = ""
          End If

          If _table.Rows(0).Item(SQL_COLUMN_COLLECTION_THEORETICAL_BILLS_SUM) IsNot DBNull.Value Then
            Me.m_type_new_collection.theoretical_bills_sum = _table.Rows(0).Item(SQL_COLUMN_COLLECTION_THEORETICAL_BILLS_SUM)
          Else
            Me.m_type_new_collection.theoretical_bills_sum = 0
          End If

          If _table.Rows(0).Item(SQL_COLUMN_COLLECTION_THEORETICAL_TICKET_SUM) IsNot DBNull.Value Then
            Me.m_type_new_collection.theoretical_ticket_sum = _table.Rows(0).Item(SQL_COLUMN_COLLECTION_THEORETICAL_TICKET_SUM)
          Else
            Me.m_type_new_collection.theoretical_ticket_sum = 0
          End If

          If _table.Rows(0).Item(SQL_COLUMN_COLLECTION_INSERTED_DATE_TIME) IsNot DBNull.Value Then
            Me.m_type_new_collection.insertion_date.Value = mdl_tito.GetFormatedDate(_table.Rows(0).Item(SQL_COLUMN_COLLECTION_INSERTED_DATE_TIME))

          End If

          If _table.Rows(0).Item(SQL_COLUMN_COLLECTION_EXTRACTION_DATE_TIME) IsNot DBNull.Value Then
            Me.m_type_new_collection.extraction_date.Value = mdl_tito.GetFormatedDate(_table.Rows(0).Item(SQL_COLUMN_COLLECTION_EXTRACTION_DATE_TIME))

          End If

          If _table.Rows(0).Item(SQL_COLUMN_COLLECTION_COLLECTED_DATE_TIME) IsNot DBNull.Value Then
            Me.m_type_new_collection.collection_date.Value = mdl_tito.GetFormatedDate(_table.Rows(0).Item(SQL_COLUMN_COLLECTION_COLLECTED_DATE_TIME))
          End If

          If _table.Rows(0).Item(SQL_COLUMN_COLLECTION_STACKER_ID) IsNot DBNull.Value Then
            Me.m_type_new_collection.stacker_id = _table.Rows(0).Item(SQL_COLUMN_COLLECTION_STACKER_ID)
          End If

          If _table.Rows(0).Item(SQL_COLUMN_COLLECTION_NOTES) IsNot DBNull.Value Then
            Me.m_type_new_collection.notes = _table.Rows(0).Item(SQL_COLUMN_COLLECTION_NOTES)
          End If

          If _table.Rows(0).Item(SQL_COLUMN_COLLECTION_REAL_TICKET_SUM) IsNot DBNull.Value Then
            Me.m_type_new_collection.real_ticket_sum = _table.Rows(0).Item(SQL_COLUMN_COLLECTION_REAL_TICKET_SUM)
          End If

          If _table.Rows(0).Item(SQL_COLUMN_COLLECTION_REAL_BILLS_SUM) IsNot DBNull.Value Then
            Me.m_type_new_collection.real_bills_sum = _table.Rows(0).Item(SQL_COLUMN_COLLECTION_REAL_BILLS_SUM)
          End If

          If _table.Rows(0).Item(SQL_COLUMN_COLLECTION_REAL_BILLS_COUNT) IsNot DBNull.Value Then
            Me.m_type_new_collection.real_bills_count = _table.Rows(0).Item(SQL_COLUMN_COLLECTION_REAL_BILLS_COUNT)
          End If

          If _table.Rows(0).Item(SQL_COLUMN_COLLECTION_TERMINAL_ID) IsNot DBNull.Value Then
            Me.m_type_new_collection.terminal_id = _table.Rows(0).Item(SQL_COLUMN_COLLECTION_TERMINAL_ID)
          End If

          If _table.Rows(0).Item(SQL_COLUMN_COLLECTION_COLLECTION_ID) IsNot DBNull.Value Then
            Me.m_type_new_collection.user_id = _table.Rows(0).Item(SQL_COLUMN_COLLECTION_COLLECTION_ID)
          End If

          If _table.Rows(0).Item(SQL_COLUMN_COLLECTION_USER_NAME) IsNot DBNull.Value Then
            Me.m_type_new_collection.user_name = _table.Rows(0).Item(SQL_COLUMN_COLLECTION_USER_NAME)
          End If

          If _table.Rows(0).Item(SQL_COLUMN_COLLECTION_USER_INSERTION_ID) IsNot DBNull.Value Then
            Me.m_type_new_collection.user_insertion_id = _table.Rows(0).Item(SQL_COLUMN_COLLECTION_USER_INSERTION_ID)
          End If

          If _table.Rows(0).Item(SQL_COLUMN_COLLECTION_COLLECTION_ID) IsNot DBNull.Value Then
            Me.m_type_new_collection.money_collection_id = _table.Rows(0).Item(SQL_COLUMN_COLLECTION_COLLECTION_ID)
          End If

          If _table.Rows(0).Item(SQL_COLUMN_COLLECTION_STATUS) IsNot DBNull.Value Then
            Me.m_type_new_collection.already_collected = (_table.Rows(0).Item(SQL_COLUMN_COLLECTION_STATUS) = TITO_MONEY_COLLECTION_STATUS.CLOSED_BALANCED OrElse _
                                                          _table.Rows(0).Item(SQL_COLUMN_COLLECTION_STATUS) = TITO_MONEY_COLLECTION_STATUS.CLOSED_IN_IMBALANCE)
          End If

          If _table.Rows(0).Item(SQL_COLUMN_COLLECTION_CASHIER_SESSION) IsNot DBNull.Value Then
            Me.CashierSessionId = _table.Rows(0).Item(SQL_COLUMN_COLLECTION_CASHIER_SESSION)
          End If
          If _table.Rows(0).Item(SQL_COLUMN_COLLECTION_EVENTS) IsNot DBNull.Value Then
            Me.m_type_new_collection.note_counter_events = _table.Rows(0).Item(SQL_COLUMN_COLLECTION_EVENTS)
          End If

          If _table.Rows(0).Item(SQL_COLUMN_COLLECTION_COINS_COLLECTION) IsNot DBNull.Value Then
            Me.m_type_new_collection.is_coin_collection_enabled = _table.Rows(0).Item(SQL_COLUMN_COLLECTION_COINS_COLLECTION)
            If Not Me.m_type_new_collection.is_coin_collection_enabled AndAlso _table.Rows(0).Item(SQL_COLUMN_COLLECTION_EXPECTED_COINS_COLLECTION) IsNot DBNull.Value Then
              Me.m_type_new_collection.is_coin_collection_enabled = _table.Rows(0).Item(SQL_COLUMN_COLLECTION_EXPECTED_COINS_COLLECTION) > 0
            End If
          End If

          ' DHA 13-JAN-2016: get terminal currency
          If WSI.Common.Misc.IsFloorDualCurrencyEnabled() And m_operation_type = Cage.CageOperationType.FromTerminal Then
            If _table.Rows(0).Item(SQL_COLUMN_COLLECTION_TERMINAL_ISO_CODE) IsNot DBNull.Value Then
              Me.TerminalIsoCode = _table.Rows(0).Item(SQL_COLUMN_COLLECTION_TERMINAL_ISO_CODE)
            End If
          End If

          If _table.Rows(0).Item(SQL_COLUMN_COLLECTION_TERMINAL_FLOOR_ID) IsNot DBNull.Value Then
            Me.m_type_new_collection.floor_id = _table.Rows(0).Item(SQL_COLUMN_COLLECTION_TERMINAL_FLOOR_ID)
          Else
            Me.m_type_new_collection.floor_id = ""
          End If

        End If

        'read collection details
        If m_type_new_collection.money_collection_id > 0 _
            AndAlso Not ReadCollectionDetails(m_type_new_collection.money_collection_id, _db_trx) Then

          _result = False
        End If

      End Using

    Catch _ex As Exception

      _result = False
    End Try

    Return _result
  End Function ' ReadMoneyCollection

  Private Function ReadCollectionDetails(ByVal StackerCollectionId As Int64, ByVal DbTrx As DB_TRX) As Boolean
    Dim _result As Boolean
    Dim _sb As StringBuilder
    Dim _table As DataTable
    Dim _row As DataRow
    Dim _row_index As Int32
    Dim _sql_where As String

    Me.m_type_new_collection.collection_details = New List(Of TYPE_MONEY_COLLECTION_DETAILS)
    _sb = New StringBuilder()
    _table = New DataTable()
    _row_index = 0
    _sql_where = ""
    _result = True

    'Making the where statement
    If m_operation_type = Cage.CageOperationType.FromTerminal Then
      _sql_where = " WHERE  TI_MONEY_COLLECTION_ID = @pStackerCollectionId  OR " & _
                           "TI_COLLECTED_MONEY_COLLECTION = @pStackerCollectionId "

    ElseIf m_operation_type = Cage.CageOperationType.FromCashier _
      Or m_operation_type = Cage.CageOperationType.FromCashierClosing _
      Or m_operation_type = Cage.CageOperationType.FromGamingTableDropbox _
      Or m_operation_type = Cage.CageOperationType.FromGamingTableDropboxWithExpected Then
      _sql_where = "WHERE TI_CAGE_MOVEMENT_ID = @pCageMovementId "

      If Not m_is_fill_out_movement Then
        _sql_where = _sql_where & " OR (TI_MONEY_COLLECTION_ID = @pStackerCollectionId " & _
                                  " AND (TI_COLLECTED_MONEY_COLLECTION <> @pStackerCollectionId " & _
                                  "      OR TI_COLLECTED_MONEY_COLLECTION IS NULL) )"
      End If
    End If

    _sb.AppendLine("  SELECT   0 AS ORDER_NUM                                               ")
    _sb.AppendLine("         , TI_MONEY_COLLECTION_ID                                       ")

    ' DHA 13-JAN-2016: get ticket amount converted to currency machine
    If WSI.Common.Misc.IsFloorDualCurrencyEnabled() And m_operation_type = Cage.CageOperationType.FromTerminal Then
      _sb.AppendLine("              , CASE WHEN TI_AMT1 IS NULL AND @pTerminalISOCode = @pNationalCurrency THEN TI_AMOUNT    ")
      _sb.AppendLine("                    WHEN TI_AMT1 IS NULL AND @pTerminalISOCode <> @pNationalCurrency THEN 0           ")
      _sb.AppendLine("                    WHEN TI_CUR1 <> @pTerminalISOCode AND @pTerminalISOCode = @pNationalCurrency    THEN TI_AMOUNT   ")
      _sb.AppendLine("                    WHEN TI_CUR1 <> @pTerminalISOCode AND @pTerminalISOCode <> @pNationalCurrency   THEN 0           ")
      _sb.AppendLine("                    ELSE TI_AMT1                                                                      ")
      _sb.AppendLine("               END AS TI_AMOUNT                                                                       ")
    Else
      _sb.AppendLine("               , TI_AMOUNT                       AS TI_AMOUNT                      ")
    End If

    _sb.AppendLine("         , NULL AS DENOMINATION                                         ")
    _sb.AppendLine("         , NULL AS REAL_AMOUNT                                          ")
    _sb.AppendLine("         , NULL AS THEO_AMOUNT                                          ")
    _sb.AppendLine("         , NULL AS REAL_THEO_DIFFERENCE                                 ")
    _sb.AppendLine("         , TI_VALIDATION_NUMBER                                         ")
    _sb.AppendLine("         , TI_COLLECTED_MONEY_COLLECTION                                ")
    _sb.AppendLine("         , TI_VALIDATION_TYPE                                           ")
    _sb.AppendLine("         , TI_CAGE_MOVEMENT_ID                                          ")
    _sb.AppendLine("         , 99 AS TYPE                                                   ")
    ' DHA 13-JAN-2016: get ticket issue amount and currency
    _sb.AppendLine("         , TI_AMT0 AS TI_AMOUNT_ISSUE                                   ")
    _sb.AppendLine("         , TI_CUR0 AS TI_CURRENCY_ISSUE                                 ")
    _sb.AppendLine("    FROM   TICKETS                                                      ")
    _sb.AppendLine(_sql_where)
    _sb.AppendLine("     UNION                                                                ")
    _sb.AppendLine("    SELECT   1 AS ORDER_NUM                                               ")
    _sb.AppendLine("           , MCD_COLLECTION_ID                                            ")
    _sb.AppendLine("           , NULL                                                         ")
    _sb.AppendLine("           , MCD_FACE_VALUE                                               ")
    _sb.AppendLine("           , CASE WHEN MCD_FACE_VALUE = -50 THEN MC_COLLECTED_COIN_AMOUNT  ELSE MCD_NUM_COLLECTED END AS  MCD_NUM_COLLECTED")
    _sb.AppendLine("           , CASE WHEN MCD_FACE_VALUE = -50 THEN MC_EXPECTED_COIN_AMOUNT  ELSE MCD_NUM_EXPECTED END AS  MCD_NUM_EXPECTED")
    _sb.AppendLine("           , CASE WHEN MCD_FACE_VALUE = -50 THEN MC_COLLECTED_COIN_AMOUNT - MC_EXPECTED_COIN_AMOUNT ")
    _sb.AppendLine("             ELSE   MCD_NUM_COLLECTED -  MCD_NUM_EXPECTED END      AS REAL_THEO_DIFFERENCE ")
    _sb.AppendLine("           , '0'                                                          ")
    _sb.AppendLine("           , NULL                                                         ")
    _sb.AppendLine("           , NULL                                                         ")
    _sb.AppendLine("           , NULL                                                         ")
    _sb.AppendLine("           , CASE WHEN MCD_FACE_VALUE > 0 THEN ISNULL(MCD_CAGE_CURRENCY_TYPE,0) ELSE 99 END AS TYPE ")
    ' DHA 13-JAN-2016: added fields for dual currency
    _sb.AppendLine("           , NULL                                                         ")
    _sb.AppendLine("           , NULL                                                         ")
    _sb.AppendLine("      FROM   MONEY_COLLECTION_DETAILS                                     ")
    _sb.AppendLine("INNER JOIN   MONEY_COLLECTIONS                                            ")
    _sb.AppendLine("        ON   MCD_COLLECTION_ID = MC_COLLECTION_ID                         ")
    _sb.AppendLine("     WHERE   MCD_COLLECTION_ID = @pStackerCollectionId                    ")
    _sb.AppendLine("  ORDER BY   ORDER_NUM                                                    ")

    Try

      Using _sql_cmd As New SqlCommand(_sb.ToString(), DbTrx.SqlTransaction.Connection, DbTrx.SqlTransaction)
        _sql_cmd.Parameters.Add("@pStackerCollectionId", SqlDbType.BigInt).Value = StackerCollectionId
        _sql_cmd.Parameters.Add("@pCageMovementId", SqlDbType.BigInt).Value = m_movement_id

        If WSI.Common.Misc.IsFloorDualCurrencyEnabled() And Me.OperationType = Cage.CageOperationType.FromTerminal Then
          _sql_cmd.Parameters.Add("@pTerminalISOCode", SqlDbType.NVarChar).Value = Me.TerminalIsoCode
          _sql_cmd.Parameters.Add("@pNationalCurrency", SqlDbType.NVarChar).Value = CurrencyExchange.GetNationalCurrency()
        End If

        Using _sql_da As New SqlDataAdapter(_sql_cmd)
          DbTrx.Fill(_sql_da, _table)
        End Using

      End Using

      For Each _row In _table.Rows
        Call FillTheoreticalDetails(_row_index, _row)
        _row_index = _row_index + 1
      Next

    Catch _ex As Exception

      _result = False
    End Try

    Return _result
  End Function ' ReadCollectionDetails

  Private Sub FillTheoreticalDetails(ByVal RowIndex As Int32, ByVal DbRow As DataRow)

    Dim _collection_detail As TYPE_MONEY_COLLECTION_DETAILS

    _collection_detail = New TYPE_MONEY_COLLECTION_DETAILS()

    If Not IsDBNull(DbRow.Item(SQL_COLUMN_COLLECTION_DETAILS_TYPE)) Then
      _collection_detail.type = DbRow.Item(SQL_COLUMN_COLLECTION_DETAILS_TYPE)
    End If

    If Not IsDBNull(DbRow.Item(SQL_COLUMN_COLLECTION_DETAILS_TICKET_VALIDATION_NUMBER)) Then
      _collection_detail.ticket_validation_number = DbRow.Item(SQL_COLUMN_COLLECTION_DETAILS_TICKET_VALIDATION_NUMBER)
    End If

    If Not IsDBNull(DbRow.Item(SQL_COLUMN_COLLECTION_DETAILS_MONEY_COLLECTION_ID)) Then
      _collection_detail.money_collection_id = DbRow.Item(SQL_COLUMN_COLLECTION_DETAILS_MONEY_COLLECTION_ID)
    End If

    If Not IsDBNull(DbRow.Item(SQL_COLUMN_COLLECTION_DETAILS_TICKET_AMOUNT)) Then
      _collection_detail.ticket_amount = DbRow.Item(SQL_COLUMN_COLLECTION_DETAILS_TICKET_AMOUNT)
    End If

    If Not IsDBNull(DbRow.Item(SQL_COLUMN_COLLECTION_DETAILS_BILL_DENOM)) Then
      _collection_detail.bill_denomination = DbRow.Item(SQL_COLUMN_COLLECTION_DETAILS_BILL_DENOM)
    End If

    If Not IsDBNull(DbRow.Item(SQL_COLUMN_COLLECTION_DETAILS_BILLS_REAL_COUNT)) Then
      _collection_detail.bill_real_count = DbRow.Item(SQL_COLUMN_COLLECTION_DETAILS_BILLS_REAL_COUNT)
    End If

    If Not IsDBNull(DbRow.Item(SQL_COLUMN_COLLECTION_DETAILS_BILLS_THEO_COUNT)) Then
      _collection_detail.bill_theoretical_count = DbRow.Item(SQL_COLUMN_COLLECTION_DETAILS_BILLS_THEO_COUNT)
    End If

    If Not IsDBNull(DbRow.Item(SQL_COLUMN_COLLECTION_DETAILS_BILLS_REAL_VS_THEO_COUNT)) Then
      _collection_detail.bill_real_vs_theo_count_diff = DbRow.Item(SQL_COLUMN_COLLECTION_DETAILS_BILLS_REAL_VS_THEO_COUNT)
    End If

    If Not IsDBNull(DbRow.Item(SQL_COLUMN_COLLECTION_DETAILS_COLLECTED_TICKET_COLLECTION)) Then
      _collection_detail.ticketd_collected_money_collection = DbRow.Item(SQL_COLUMN_COLLECTION_DETAILS_COLLECTED_TICKET_COLLECTION)
    End If

    If Not IsDBNull(DbRow.Item(SQL_COLUMN_COLLECTION_DETAILS_TICKET_VALIDATION_TYPE)) Then
      _collection_detail.ticket_validation_type = DbRow.Item(SQL_COLUMN_COLLECTION_DETAILS_TICKET_VALIDATION_TYPE)
    End If

    If Not IsDBNull(DbRow.Item(SQL_COLUMN_COLLECTION_DETAILS_TICKET_CM_ID)) Then
      _collection_detail.ticket_cage_movement_id = DbRow.Item(SQL_COLUMN_COLLECTION_DETAILS_TICKET_CM_ID)
    End If

    If Not IsDBNull(DbRow.Item(SQL_COLUMN_COLLECTION_DETAILS_CURRENCY_TYPE)) Then
      _collection_detail.currency_type = DbRow.Item(SQL_COLUMN_COLLECTION_DETAILS_CURRENCY_TYPE)
    End If

    ' DHA 13-JAN-2016: set ticket issue amount and currency
    If Not IsDBNull(DbRow.Item(SQL_COLUMN_COLLECTION_DETAILS_TICKET_AMOUNT_ISSUE)) Then
      _collection_detail.ticket_amount_issue = DbRow.Item(SQL_COLUMN_COLLECTION_DETAILS_TICKET_AMOUNT_ISSUE)
    End If

    If Not IsDBNull(DbRow.Item(SQL_COLUMN_COLLECTION_DETAILS_TICKET_CURRENCY_ISSUE)) Then
      _collection_detail.ticket_currency_issue = DbRow.Item(SQL_COLUMN_COLLECTION_DETAILS_TICKET_CURRENCY_ISSUE)
    End If

    Me.m_type_new_collection.collection_details.Add(_collection_detail)

  End Sub ' FillTheoreticalDetails


  Private Function SelectReadStackerCollectionMode() As Boolean

    If Me.m_new_collecion_mode Then
      Return ReadTheoreticalCollectionNewMode(Me.m_type_new_collection.money_collection_id, Me.m_cashier_session_id)
    Else
      Return ReadMoneyCollection(Me.m_type_new_collection.money_collection_id, Me.m_cashier_session_id)
    End If

  End Function ' SelectReadStackerCollectionMode

  Private Function CloseSessionTITO(ByVal TRX As DB_TRX) As Boolean
    Return CloseSessionTITO(TRX.SqlTransaction)
  End Function

  Private Function CloseSessionTITO(ByVal Transaction As SqlTransaction) As Boolean
    Dim _terminal_id As Int32
    Dim _terminal_name As String
    Dim _session_info As CashierSessionInfo
    Dim _cashier_movements As CashierMovementsTable

    Try

      'Get terminal information to set cashier information used for Vouchers.
      _terminal_name = Environment.MachineName
      _terminal_id = Cashier.ReadTerminalId(Transaction, _terminal_name, GU_USER_TYPE.USER)

      If _terminal_id = 0 Then
        Return False
      End If

      ' Set the information as the following:
      '  - Machine: is the PC where the GUI is executed.
      '  - User: is the GUI user, not the Cashier user.
      CommonCashierInformation.SetCashierInformation(m_cashier_session_id, _
                                                     GLB_CurrentUser.Id, GLB_CurrentUser.Name, _terminal_id, _terminal_name)
      If _terminal_id = 0 Then
        Return False

      End If
      ' Set session information
      _session_info = CommonCashierInformation.CashierSessionInfo()
      ' Set user type
      _session_info.UserType = GU_USER_TYPE.SYS_TITO

      _cashier_movements = New CashierMovementsTable(_session_info)

      ' DHA 01-FEB-2016: set terminal ISO Code when session is closed
      If WSI.Common.Misc.IsFloorDualCurrencyEnabled() And _session_info.UserType = GU_USER_TYPE.SYS_TITO Then
        If String.IsNullOrEmpty(Me.TerminalIsoCode) Then
          Me.TerminalIsoCode = CurrencyExchange.GetNationalCurrency()
        End If
        _cashier_movements.Add(0, CASHIER_MOVEMENT.CLOSE_SESSION, 0, 0, "", "", Me.TerminalIsoCode)
      Else
        _cashier_movements.Add(0, CASHIER_MOVEMENT.CLOSE_SESSION, 0, 0, "")
      End If


      If Not _cashier_movements.Save(Transaction) Then
        Return False
      End If

      ' Close cashier session of system mobile bank
      If Not Cashier.CashierSessionCloseById(m_cashier_session_id, _terminal_id, _terminal_name, Transaction) Then
        Return False
      End If

      Call GUI_CloseSessionWriteAuditory(_terminal_name)

    Catch _ex As Exception
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, GLB_ApplicationName, "CloseSession", _ex.Message)
      Return False

    End Try

    Return True
  End Function

  Private Function ReadMovementIdByMoneyCollection() As Boolean
    Dim _result As Boolean
    Dim _sb As StringBuilder
    Dim _obj As Object

    _sb = New StringBuilder
    _result = True

    If Me.m_type_new_collection.money_collection_id = 0 Then
      If (Not GetPendingMoneyCollectionID(Me.m_type_new_collection.stacker_id, Me.m_type_new_collection.floor_id)) Then

        Return False
      End If
    End If
    _sb.AppendLine(" IF @pMoneyCollection IS NOT NULL ")
    _sb.AppendLine("   SELECT   CGM_MOVEMENT_ID ")
    _sb.AppendLine("     FROM   CAGE_MOVEMENTS ")
    _sb.AppendLine("    WHERE   CGM_MC_COLLECTION_ID = @pMoneyCollection ")

    Try

      Using _db_trx As New DB_TRX()

        Using _sql_cmd As New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          _sql_cmd.Parameters.Add("@pMoneyCollection", SqlDbType.BigInt).Value = Me.m_type_new_collection.money_collection_id

          _obj = _sql_cmd.ExecuteScalar()

          If Not IsNothing(_obj) Then
            Me.MovementID = CInt(_obj)
          Else
            _result = False
          End If

        End Using

      End Using

    Catch _ex As Exception

      _result = False
    End Try

    Return _result
  End Function

  Private Function GetPendingMoneyCollectionID(ByVal StackerId As Int64, ByVal FloorId As String) As Boolean
    Dim _sb As StringBuilder
    Dim _money_collection_id As Int32
    Dim _obj As Object
    Dim _frm As frm_stacker_collections_sel
    Dim _sb_where As StringBuilder

    If StackerId < 0 AndAlso String.IsNullOrEmpty(FloorId) Then

      Return False
    End If


    _money_collection_id = -1
    _sb_where = New StringBuilder()
    If (String.IsNullOrEmpty(FloorId)) Then
      If StackerId > 0 Then
        _sb_where.AppendLine("  WHERE   MC_STACKER_ID = @pStackerId ")
      Else
        _sb_where.AppendLine("  WHERE   MC_STACKER_ID IS NULL ")
      End If
    Else
      _sb_where.AppendLine("   LEFT JOIN   TERMINALS ")
      _sb_where.AppendLine("          ON   TE_TERMINAL_ID = MC_TERMINAL_ID ")
      _sb_where.AppendLine("       WHERE   TE_FLOOR_ID = @pFloorId ")
    End If
    _sb_where.AppendLine(" AND MC_STATUS = @pStatus ")


    _sb = New StringBuilder()
    _sb.AppendLine(" IF ( SELECT   COUNT(1) ")
    _sb.AppendLine("       FROM   MONEY_COLLECTIONS ")
    _sb.AppendLine(_sb_where.ToString())
    _sb.AppendLine("    ) > 1")
    _sb.AppendLine(" BEGIN ")
    _sb.AppendLine("    SELECT -1 ")
    _sb.AppendLine(" END ")
    _sb.AppendLine(" ELSE ")
    _sb.AppendLine(" BEGIN ")
    _sb.AppendLine("   SELECT   MC_COLLECTION_ID ")
    _sb.AppendLine("     FROM   MONEY_COLLECTIONS ")
    _sb.AppendLine(_sb_where.ToString())
    _sb.AppendLine(" END ")

    Try
      Using _db_trx As New DB_TRX
        Using _sql_cmd As New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          _sql_cmd.Parameters.Add("@pStackerId", SqlDbType.BigInt).Value = StackerId
          _sql_cmd.Parameters.Add("@pFloorId", SqlDbType.NVarChar).Value = IIf(String.IsNullOrEmpty(FloorId), System.DBNull.Value, FloorId)
          _sql_cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = TITO_MONEY_COLLECTION_STATUS.PENDING

          _obj = _sql_cmd.ExecuteScalar()

          If _obj IsNot Nothing AndAlso _obj IsNot DBNull.Value Then
            _money_collection_id = _obj
          Else
            Me.m_type_new_collection.stacker_id = 0
            Me.m_type_new_collection.floor_id = Nothing

            Return False
          End If
        End Using
      End Using

    Catch _ex As Exception
      Log.Exception(_ex)

      Return False
    End Try

    If _money_collection_id > 0 Then
      Me.m_type_new_collection.money_collection_id = _money_collection_id

      Return True
    Else
      Me.m_load_cancelled_by_user = False

      _frm = New frm_stacker_collections_sel()
      _frm.ShowForView(FloorId, StackerId)
      _frm.Close()

      Me.m_type_new_collection.money_collection_id = _frm.m_selected_money_collection
      If Me.m_type_new_collection.money_collection_id > 0 Then

        Return True
      Else
        Me.m_load_cancelled_by_user = True

        Return False
      End If
    End If
  End Function


#End Region 'Tito's functions

#Region "Public"

  Public Sub New()
    Me.IsCountR = False
    Me.InitializeTypeNewCollection()
  End Sub

  Public Function GetPartialCollectionValue() As Boolean
    Dim _sb As StringBuilder
    Dim _cashier_session_status As CASHIER_SESSION_STATUS
    Dim _last_movement_id As Int64

    _sb = New StringBuilder()
    m_is_fill_out_movement = False

    _sb.AppendLine("      SELECT   MAX(CGM_MOVEMENT_ID) ")
    _sb.AppendLine("             , CS_STATUS            ")
    _sb.AppendLine("        FROM   CAGE_MOVEMENTS       ")
    _sb.AppendLine("  INNER JOIN   CASHIER_SESSIONS     ")
    _sb.AppendLine("          ON   CGM_CASHIER_SESSION_ID = CS_SESSION_ID      ")
    _sb.AppendLine("       WHERE   CGM_CASHIER_SESSION_ID = @pCashierSessionId ")
    _sb.AppendLine("    GROUP BY   CS_STATUS            ")

    Try

      Using _db_trx As New DB_TRX()
        Using _cmd As New SqlCommand(_sb.ToString, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _cmd.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).Value = m_cashier_session_id

          Using _reader As SqlDataReader = _cmd.ExecuteReader()

            If _reader.Read() Then
              _last_movement_id = _reader.GetInt64(0)
              _cashier_session_status = _reader.GetInt32(1)

              m_is_fill_out_movement = (m_movement_id < _last_movement_id OrElse _cashier_session_status = CASHIER_SESSION_STATUS.OPEN)
            End If

            Return True
          End Using

        End Using
      End Using

    Catch _ex As Exception

      Return False
    End Try
  End Function 'IsPartialCollection

#End Region

End Class
