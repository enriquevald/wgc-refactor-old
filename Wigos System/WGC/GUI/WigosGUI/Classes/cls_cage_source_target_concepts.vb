'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_cage_source_target_concepts.vb
' DESCRIPTION:   Cage Source Target Concepts class for Cage Source Target Concepts Selection
' AUTHOR:        Omar P�rez
' CREATION DATE: 16-SEP-2014
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 16-SEP-2014  OPC    Initial version.
'-------------------------------------------------------------------

#Region "Imports"

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Text
Imports WSI.Common
Imports System.Data.SqlClient

#End Region

Public Class cls_cage_source_target_concepts
  Inherits CLASS_BASE

#Region "Constants"

  Public Const MAX_DESCRIPTION_LEN As Integer = 50
  Public Const MAX_TITLE_LEN As Integer = 20
  Public Const AUDIT_NONE_STRING As String = "---"

  Private Const MAX_SQL_ROWS As Integer = 5000

#End Region

#Region "Enum"

  Public Enum STATUS
    ENABLED = 1
    DISABLED = 0
  End Enum

#End Region

#Region "Class"

  Public Class TYPE_CONCEPT
    Public concept_id As Int64
    Public description As String
    Public is_provision As Boolean
    Public show_in_report As Boolean
    Public unit_price As Decimal
    Public name As String
    Public type As Int32
    Public enabled As Boolean
  End Class

#Region "Properties"

  Public Property Concept_Id() As Int64
    Get
      Return Me.m_type_cage_concept.concept_id
    End Get
    Set(ByVal value As Int64)
      Me.m_type_cage_concept.concept_id = value
    End Set
  End Property

  Public Property Description() As String
    Get
      Return Me.m_type_cage_concept.description
    End Get
    Set(ByVal value As String)
      Me.m_type_cage_concept.description = value
    End Set
  End Property

  Public Property Is_Provision() As Boolean
    Get
      Return Me.m_type_cage_concept.is_provision
    End Get
    Set(ByVal value As Boolean)
      Me.m_type_cage_concept.is_provision = value
    End Set
  End Property

  Public Property Show_In_Report() As Boolean
    Get
      Return Me.m_type_cage_concept.show_in_report
    End Get
    Set(ByVal value As Boolean)
      Me.m_type_cage_concept.show_in_report = value
    End Set
  End Property

  Public Property Unit_Price() As Decimal
    Get
      Return Me.m_type_cage_concept.unit_price
    End Get
    Set(ByVal value As Decimal)
      Me.m_type_cage_concept.unit_price = value
    End Set
  End Property

  Public Property Name() As String
    Get
      Return Me.m_type_cage_concept.name
    End Get
    Set(ByVal value As String)
      Me.m_type_cage_concept.name = value
    End Set
  End Property

  Public Property Type() As Int32
    Get
      Return Me.m_type_cage_concept.type
    End Get
    Set(ByVal value As Int32)
      Me.m_type_cage_concept.type = value
    End Set
  End Property

  Public Property Enabled() As Boolean
    Get
      Return Me.m_type_cage_concept.enabled
    End Get
    Set(ByVal value As Boolean)
      Me.m_type_cage_concept.enabled = value
    End Set
  End Property

#End Region

#End Region

#Region "Members"

  Protected m_type_cage_concept As New TYPE_CONCEPT

#End Region

#Region "Overrides functions"

  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As ENUM_CONFIGURATION_RC

    Me.m_type_cage_concept.concept_id = ObjectId

    _rc = ReadSourceTargetConcepts(m_type_cage_concept)

    Select Case _rc

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As ENUM_CONFIGURATION_RC

    _rc = InsertSourceTargetConcepts(m_type_cage_concept)

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_ALREADY_EXISTS
        Return CLASS_BASE.ENUM_STATUS.STATUS_ALREADY_EXISTS

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As ENUM_CONFIGURATION_RC

    _rc = DeleteCageSourceTargetConcept(Me.Concept_Id)

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DEPENDENCIES
        Return ENUM_STATUS.STATUS_DEPENDENCIES

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_CONTEXT
        Return ENUM_STATUS.STATUS_NOT_SUPPORTED

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As ENUM_CONFIGURATION_RC

    _rc = UpdateSourceTargetConcept(Me.Concept_Id)

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_ALREADY_EXISTS
        Return CLASS_BASE.ENUM_STATUS.STATUS_ALREADY_EXISTS

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function

  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim _auditor_data As CLASS_AUDITOR_DATA

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_CASHIER_CONFIGURATION)
    _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(5495), "")

    _auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(210), Me.Name)  ' Name
    _auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(62), Me.Description)  ' Description
    _auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(259), IIf(Me.Enabled, _
                                                              GLB_NLS_GUI_CONFIGURATION.GetString(311), _
                                                              GLB_NLS_GUI_CONFIGURATION.GetString(328)))  ' Enabled
    _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2268), Me.Type)  ' System
    _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5476), GUI_FormatCurrency(Me.Unit_Price))  ' Unit Price
    _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5474), Me.Is_Provision)  ' Provision
    _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5475), Me.Show_In_Report)  ' Show in Report

    Return _auditor_data

  End Function

  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim _temp_cage_source_concepts As cls_cage_source_target_concepts

    _temp_cage_source_concepts = New cls_cage_source_target_concepts

    _temp_cage_source_concepts.m_type_cage_concept.concept_id = Me.m_type_cage_concept.concept_id
    _temp_cage_source_concepts.m_type_cage_concept.description = Me.m_type_cage_concept.description
    _temp_cage_source_concepts.m_type_cage_concept.enabled = Me.m_type_cage_concept.enabled
    _temp_cage_source_concepts.m_type_cage_concept.is_provision = Me.m_type_cage_concept.is_provision
    _temp_cage_source_concepts.m_type_cage_concept.name = Me.m_type_cage_concept.name
    _temp_cage_source_concepts.m_type_cage_concept.show_in_report = Me.m_type_cage_concept.show_in_report
    _temp_cage_source_concepts.m_type_cage_concept.type = Me.m_type_cage_concept.type
    _temp_cage_source_concepts.m_type_cage_concept.unit_price = Me.m_type_cage_concept.unit_price

    Return _temp_cage_source_concepts

  End Function

#End Region

#Region "Public Functions / Methods"

  Public Shared Function GetConceptRelation(ByVal Concept_Id As Int64) As Boolean
    Dim _sw_Exist As Boolean = False

    Try

      Using _db_trx As New DB_TRX()
        Dim _sb As StringBuilder
        _sb = New StringBuilder()

        _sb.AppendLine("SELECT   COUNT(*)")
        _sb.AppendLine("  FROM   CAGE_SOURCE_TARGET_CONCEPTS")
        _sb.AppendLine(" WHERE   CSTC_CONCEPT_ID = @pConceptId")

        Using _sql_cmd As New SqlCommand(_sb.ToString, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _sql_cmd.Parameters.Add("@pConceptId", SqlDbType.BigInt).Value = Concept_Id

          If (_sql_cmd.ExecuteScalar() > 0) Then
            _sw_Exist = True
          End If

        End Using

      End Using

    Catch ex As Exception
      Log.Exception(ex)

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

    Return _sw_Exist
  End Function ' GetConceptRelation

#End Region

#Region "Private Functions / Methods"

  Private Function ReadSourceTargetConcepts(ByVal Concept As TYPE_CONCEPT) As ENUM_CONFIGURATION_RC

    Dim _sb As StringBuilder

    _sb = New StringBuilder()

    Try

      Using _db_trx As New DB_TRX()

        _sb.AppendLine("SELECT   CC_CONCEPT_ID")
        _sb.AppendLine("       , CC_DESCRIPTION")
        _sb.AppendLine("       , CC_IS_PROVISION")
        _sb.AppendLine("       , CC_SHOW_IN_REPORT")
        _sb.AppendLine("       , CC_UNIT_PRICE")
        _sb.AppendLine("       , CC_NAME")
        _sb.AppendLine("       , CC_TYPE")
        _sb.AppendLine("       , CC_ENABLED")
        _sb.AppendLine(" FROM    CAGE_CONCEPTS")
        _sb.AppendLine(" WHERE   CC_CONCEPT_ID = @pConceptId")

        Using _sql_cmd As New SqlCommand(_sb.ToString, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _sql_cmd.Parameters.Add("pConceptId", SqlDbType.Int).Value = Concept.concept_id

          Using _sql_reader As SqlDataReader = _sql_cmd.ExecuteReader()

            If Not _sql_reader.Read() Then
              Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
            End If

            With Concept
              .concept_id = _sql_reader.GetInt64(0)           ' CC_CONCEPT_ID
              If Not IsDBNull(_sql_reader(1)) Then
                .description = _sql_reader.GetString(1)       ' CC_DESCRIPTION
              Else
                .description = String.Empty
              End If
              .is_provision = _sql_reader.GetBoolean(2)       ' CC_IS_PROVISION
              .show_in_report = _sql_reader.GetSqlBoolean(3)  ' CC_SHOW_IN_REPORT
              If Not IsDBNull(_sql_reader(4)) Then
                .unit_price = _sql_reader.GetDecimal(4)       ' CC_UNIT_PRICE
              Else
                .unit_price = GUI_ParseCurrency("0")
              End If
              .name = _sql_reader.GetString(5)                ' CC_NAME
              .type = _sql_reader.GetInt32(6)                 ' CC_TYPE
              .enabled = _sql_reader.GetBoolean(7)            ' CC_ENABLED
            End With

            Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

          End Using

        End Using

      End Using

    Catch ex As Exception
      Log.Exception(ex)

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

  End Function 'ReadSourceTargetConcepts

  Private Function InsertSourceTargetConcepts(ByVal Concept As TYPE_CONCEPT) As ENUM_CONFIGURATION_RC

    Dim _sb As StringBuilder

    _sb = New StringBuilder()

    Try

      Using _db_trx As New DB_TRX()

        _sb.AppendLine("SELECT   COUNT(*)")
        _sb.AppendLine(" FROM    CAGE_CONCEPTS")
        _sb.AppendLine(" WHERE   CC_NAME = @pName")

        Using _sql_cmd As New SqlCommand(_sb.ToString, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _sql_cmd.Parameters.Add("pName", SqlDbType.NVarChar).Value = Concept.name

          If (_sql_cmd.ExecuteScalar() > 0) Then
            Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_ALREADY_EXISTS
          End If

          _sb.Length = 0

          _sb.AppendLine("DECLARE   @p_concept_id AS INTEGER")
          _sb.AppendLine(" SELECT    @p_concept_id = ")
          _sb.AppendLine("          CASE WHEN ISNULL(MAX(CC_CONCEPT_ID + 1),1000) < 1000 THEN ")
          _sb.AppendLine("          1000 ")
          _sb.AppendLine("          ELSE ")
          _sb.AppendLine("          MAX(CC_CONCEPT_ID + 1) ")
          _sb.AppendLine("          END ")
          _sb.AppendLine("          FROM CAGE_CONCEPTS")
          _sb.AppendLine(" INSERT INTO  CAGE_CONCEPTS")
          _sb.AppendLine("            ( cc_concept_id")
          _sb.AppendLine("            , cc_description")
          _sb.AppendLine("            , cc_is_provision")
          _sb.AppendLine("            , cc_show_in_report")
          _sb.AppendLine("            , cc_unit_price")
          _sb.AppendLine("            , cc_name")
          _sb.AppendLine("            , cc_type")
          _sb.AppendLine("            , cc_enabled )")
          _sb.AppendLine("              VALUES ")
          _sb.AppendLine("            ( @p_concept_id")
          _sb.AppendLine("            , @p_description")
          _sb.AppendLine("            , @p_is_provision")
          _sb.AppendLine("            , @p_show_in_report")
          _sb.AppendLine("            , @p_unit_price")
          _sb.AppendLine("            , @p_name")
          _sb.AppendLine("            , @p_type")
          _sb.AppendLine("            , @p_enabled )")

          _sql_cmd.CommandText = _sb.ToString()

          _sql_cmd.Parameters.Add("@p_description", SqlDbType.NVarChar).Value = Concept.description
          _sql_cmd.Parameters.Add("@p_is_provision", SqlDbType.Bit).Value = Concept.is_provision
          _sql_cmd.Parameters.Add("@p_show_in_report", SqlDbType.Bit).Value = Concept.show_in_report
          _sql_cmd.Parameters.Add("@p_unit_price", SqlDbType.Money).Value = Concept.unit_price
          _sql_cmd.Parameters.Add("@p_name", SqlDbType.NVarChar).Value = Concept.name
          _sql_cmd.Parameters.Add("@p_type", SqlDbType.Int).Value = Concept.type
          _sql_cmd.Parameters.Add("@p_enabled", SqlDbType.Bit).Value = Concept.enabled

          If _sql_cmd.ExecuteNonQuery() <> 1 Then

            Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
          End If

          _db_trx.Commit()

        End Using

      End Using

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

    Catch ex As Exception
      Log.Exception(ex)

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

  End Function ' InsertSourceTargetConcepts

  Private Function DeleteCageSourceTargetConcept(ByVal Concept_Id As Int64) As ENUM_CONFIGURATION_RC

    Dim _sb As StringBuilder

    _sb = New StringBuilder()

    Try

      Using _db_trx As New DB_TRX()

        _sb.Length = 0
        _sb.AppendLine("DELETE FROM   CAGE_CONCEPTS")
        _sb.AppendLine("      WHERE   CC_CONCEPT_ID = @pConcept_Id")

        Using _sql_command As New SqlCommand(_sb.ToString, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _sql_command.Parameters.Add("@pConcept_Id", SqlDbType.BigInt).Value = Me.Concept_Id

          _sql_command.ExecuteNonQuery()

          _db_trx.Commit()

          Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

        End Using

      End Using

    Catch ex As Exception
      Log.Exception(ex)

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

  End Function ' DeleteCageSourceTargetConcept

  Private Function UpdateSourceTargetConcept(ByVal Concept_Id As Int64) As ENUM_CONFIGURATION_RC

    Dim _sb As StringBuilder

    _sb = New StringBuilder()

    Try

      Using _db_trx As New DB_TRX()

        _sb.Length = 0
        _sb.AppendLine("SELECT COUNT(*) FROM CAGE_CONCEPTS WHERE CC_NAME = @pName AND CC_CONCEPT_ID <> @pConceptId")

        Using _sql_command As New SqlCommand(_sb.ToString, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _sql_command.Parameters.Add("@pName", SqlDbType.NVarChar).Value = Me.Name
          _sql_command.Parameters.Add("@pConceptId", SqlDbType.BigInt).Value = Me.Concept_Id

          If _sql_command.ExecuteScalar() <> 0 Then
            Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_ALREADY_EXISTS
          End If

        End Using

        _sb.Length = 0
        _sb.AppendLine("UPDATE   CAGE_CONCEPTS")
        _sb.AppendLine("   SET   CC_DESCRIPTION = @pDescription")
        _sb.AppendLine("	     , CC_IS_PROVISION = @pIsProvision")
        _sb.AppendLine("	     , CC_SHOW_IN_REPORT = @pShowInReport")
        _sb.AppendLine("	     , CC_UNIT_PRICE = @pUnitPrice")
        _sb.AppendLine("	     , CC_NAME = @pName")
        _sb.AppendLine("	     , CC_ENABLED = @pEnabled")
        _sb.AppendLine(" WHERE   CC_CONCEPT_ID = @pConceptId")

        Using _sql_command As New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection(), _db_trx.SqlTransaction())

          _sql_command.Parameters.Add("@pDescription", SqlDbType.NVarChar).Value = IIf(String.IsNullOrEmpty(Me.Description), String.Empty, Me.Description)
          _sql_command.Parameters.Add("@pIsProvision", SqlDbType.Bit).Value = Me.Is_Provision
          _sql_command.Parameters.Add("@pShowInReport", SqlDbType.Bit).Value = Me.Show_In_Report
          If Me.Unit_Price <> 0 Then
            _sql_command.Parameters.Add("@pUnitPrice", SqlDbType.Money).Value = GUI_ParseCurrency(Me.Unit_Price.ToString())
          Else
            _sql_command.Parameters.Add("@pUnitPrice", SqlDbType.Money).Value = DBNull.Value
          End If
          _sql_command.Parameters.Add("@pName", SqlDbType.NVarChar).Value = Me.Name
          _sql_command.Parameters.Add("@pEnabled", SqlDbType.Bit).Value = Me.Enabled
          _sql_command.Parameters.Add("@pConceptId", SqlDbType.BigInt).Value = Concept_Id

          _sql_command.ExecuteNonQuery()

          _db_trx.Commit()

        End Using

      End Using

    Catch ex As Exception
      Log.Exception(ex)

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

  End Function ' UpdateSourceTargetConcept

#End Region

End Class
