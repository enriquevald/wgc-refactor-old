﻿'-------------------------------------------------------------------
' Copyright © 2016 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : Class to manage Customer Entrance Price
'
' DESCRIPTION : Customer Entrance Price
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 06-MAY-2016  SMN    Initial version Product Backlog Item 12929:Visitas / Recepción: BlackList - Ampliar funcionalidad
' 02-AGO-2016  FJC    Bug 16156:Recepción: Error en el log al crear "Tipo de lista de prohibidos "
'--------------------------------------------------------------------

Imports WSI.Common
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports System.Text

Public Class cls_blacklist_import_type
  Inherits CLASS_BASE

  Public Const SQL_COLUMN_TYPE_ID As Integer = 0
  Public Const SQL_COLUMN_NAME As Integer = 1
  Public Const SQL_COLUMN_MESSAGE As Integer = 2
  Public Const SQL_COLUMN_ENTRANCE_ALLOWED As Integer = 3
  Public Const SQL_COLUMN_IS_DELETED As Integer = 4

  Public Const SQL_COLUMN_DESC_TYPE_ID As String = "BKLT_ID_TYPE"

#Region "Memebers"
  Private m_datatable_blacklist_import_type As New DataTable

#End Region

#Region "Properties"

  Public Property BlackListImportType() As DataTable
    Get
      Return m_datatable_blacklist_import_type
    End Get
    Set(ByVal value As DataTable)
      m_datatable_blacklist_import_type = value
    End Set
  End Property


#End Region

#Region "Overrides"

  ''' <summary>
  ''' 
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function AuditorData() As CLASS_AUDITOR_DATA
    Dim auditor_data As CLASS_AUDITOR_DATA
    Dim _idx As Integer

    Using DbTrx As New DB_TRX()

      auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_CASHIER_CONFIGURATION)
      auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(7125), "")

      Try
        For _idx = 0 To Me.BlackListImportType.Rows.Count - 1
          'Description
          Call auditor_data.SetField(0, Me.BlackListImportType.Rows(_idx)(cls_blacklist_import_type.SQL_COLUMN_NAME), GLB_NLS_GUI_CONTROLS.GetString(323))

          ' Level
          Call auditor_data.SetField(0, Me.BlackListImportType.Rows(_idx)(cls_blacklist_import_type.SQL_COLUMN_MESSAGE), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1428))

          ' Price
          Call auditor_data.SetField(0, Me.BlackListImportType.Rows(_idx)(cls_blacklist_import_type.SQL_COLUMN_ENTRANCE_ALLOWED), GLB_NLS_GUI_PLAYER_TRACKING.GetString(7303))

        Next

      Catch ex As Exception

        Debug.Print(ex.Message)
      End Try
    End Using

    Return auditor_data
  End Function

  ''' <summary>
  ''' 
  ''' </summary>
  ''' <param name="SqlCtx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As CLASS_BASE.ENUM_STATUS

  End Function

  ''' <summary>
  ''' 
  ''' </summary>
  ''' <param name="SqlCtx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As CLASS_BASE.ENUM_STATUS

  End Function

  ''' <summary>
  ''' 
  ''' </summary>
  ''' <param name="ObjectId"></param>
  ''' <param name="SqlCtx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function DB_Read(ObjectId As Object, ByRef SqlCtx As Integer) As CLASS_BASE.ENUM_STATUS

    m_datatable_blacklist_import_type = GetTypes()

  End Function

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As CLASS_BASE.ENUM_STATUS
    Dim _datatable_blacklist_import_type_copy As New DataTable

    _datatable_blacklist_import_type_copy = BlackListImportType.Copy()

    ' Delete rows that are marked as Deleted. It DataTable is used in Audit control.
    DeleteRowsByColumnIsDeleted(BlackListImportType)

    Try

      Using _db_trx As New DB_TRX()
        Using _sql_da As New SqlDataAdapter()
          ' Create all SqlCommand
          _sql_da.UpdateCommand = CreateUpdateCommand(_db_trx.SqlTransaction)
          _sql_da.InsertCommand = CreateInsertCommand(_db_trx.SqlTransaction)
          _sql_da.DeleteCommand = CreateDeleteCommand(_db_trx.SqlTransaction) ' Only delete Types that not are not related to any file imported.

          ' Execute data adapter
          _sql_da.Update(_datatable_blacklist_import_type_copy)
        End Using

        _db_trx.Commit()
      End Using

    Catch _ex_concurrency As DBConcurrencyException

      If _datatable_blacklist_import_type_copy.Select(String.Empty, String.Empty, DataViewRowState.Deleted).Length > 0 Then

        Return ENUM_STATUS.STATUS_DEPENDENCIES
      End If

      Return ENUM_STATUS.STATUS_ERROR

    Catch ex As Exception

      Return ENUM_STATUS.STATUS_ERROR
    End Try

    Return ENUM_STATUS.STATUS_OK

  End Function ' DB_Update

  ''' <summary>
  ''' Get a datatable with import files types [BLACKLIST_FILE_IMPORTED_TYPE]
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function GetTypes() As DataTable
    Dim _sb As StringBuilder
    Dim _datatable As DataTable

    _datatable = New DataTable()

    Try
      _sb = New StringBuilder()

      _sb.AppendLine("   SELECT   BKLT_ID_TYPE                 ")
      _sb.AppendLine("          , BKLT_NAME                    ")
      _sb.AppendLine("          , BKLT_MESSAGE                 ")
      _sb.AppendLine("          , BKLT_ENTRANCE_ALLOWED        ")
      _sb.AppendLine("          , 0 AS IS_DELETED              ") ' uses for manage delete rows
      _sb.AppendLine("     FROM   BLACKLIST_FILE_IMPORTED_TYPE ")
      _sb.AppendLine(" ORDER BY   BKLT_NAME                    ")

      _datatable = GUI_GetTableUsingSQL(_sb.ToString(), 5000, "BLACKLIST_FILE_IMPORTED_TYPE")

    Catch ex As Exception
      Log.Exception(ex)
    End Try

    Return _datatable

  End Function


  ''' <summary>
  ''' Returns a duplicate of the given object.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE
    Dim temp_blacklist_import_type As cls_blacklist_import_type

    temp_blacklist_import_type = New cls_blacklist_import_type

    temp_blacklist_import_type.m_datatable_blacklist_import_type = Me.m_datatable_blacklist_import_type.Copy()

    Return temp_blacklist_import_type
  End Function     ' Duplicate
#End Region

#Region "Private"

  ''' <summary>
  ''' Create sql update command
  ''' </summary>
  ''' <param name="SqlTrx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function CreateUpdateCommand(ByVal SqlTrx As System.Data.SqlClient.SqlTransaction) As SqlCommand
    Dim _sql_query As StringBuilder
    Dim _sql_command As SqlCommand

    Try
      _sql_query = New StringBuilder()
      _sql_query.AppendLine(" UPDATE  BLACKLIST_FILE_IMPORTED_TYPE                    ")
      _sql_query.AppendLine("    SET  BKLT_NAME = @pBKLT_NAME                         ")
      _sql_query.AppendLine("      ,  BKLT_MESSAGE = @pBKLT_MESSAGE                   ")
      _sql_query.AppendLine("      ,  BKLT_ENTRANCE_ALLOWED = @pBKLT_ENTRANCE_ALLOWED ")
      _sql_query.AppendLine("  WHERE  BKLT_ID_TYPE = @pBKLT_ID_TYPE                   ")


      _sql_command = New SqlCommand(_sql_query.ToString(), SqlTrx.Connection, SqlTrx)
      _sql_command.Parameters.Add("@pBKLT_NAME", SqlDbType.NVarChar, 50, "BKLT_NAME")
      _sql_command.Parameters.Add("@pBKLT_MESSAGE", SqlDbType.NVarChar, 50, "BKLT_MESSAGE")
      _sql_command.Parameters.Add("@pBKLT_ENTRANCE_ALLOWED", SqlDbType.Bit, 1, "BKLT_ENTRANCE_ALLOWED")
      _sql_command.Parameters.Add("@pBKLT_ID_TYPE", SqlDbType.BigInt, 9, "BKLT_ID_TYPE")

      Return _sql_command

    Catch ex As Exception

      Throw
    End Try

  End Function ' CreateUpdateCommand

  ''' <summary>
  ''' Create sql insert command
  ''' </summary>
  ''' <param name="SqlTrx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function CreateInsertCommand(ByVal SqlTrx As System.Data.SqlClient.SqlTransaction) As SqlCommand
    Dim _sql_query As StringBuilder
    Dim _sql_command As SqlCommand

    Try

      _sql_query = New StringBuilder()
      _sql_query.AppendLine(" INSERT INTO   BLACKLIST_FILE_IMPORTED_TYPE ")
      _sql_query.AppendLine("             ( BKLT_NAME                    ")
      _sql_query.AppendLine("             , BKLT_MESSAGE                 ")
      _sql_query.AppendLine("             , BKLT_ENTRANCE_ALLOWED )      ")
      _sql_query.AppendLine("      VALUES ( @pBKLT_NAME                  ")
      _sql_query.AppendLine("             , @pBKLT_MESSAGE               ")
      _sql_query.AppendLine("             , @pBKLT_ENTRANCE_ALLOWED )    ")

      _sql_command = New SqlCommand(_sql_query.ToString(), SqlTrx.Connection, SqlTrx)
      _sql_command.Parameters.Add("@pBKLT_NAME", SqlDbType.NVarChar, 50, "BKLT_NAME")
      _sql_command.Parameters.Add("@pBKLT_MESSAGE", SqlDbType.NVarChar, 50, "BKLT_MESSAGE")
      _sql_command.Parameters.Add("@pBKLT_ENTRANCE_ALLOWED", SqlDbType.Bit, 1, "BKLT_ENTRANCE_ALLOWED")

      Return _sql_command

    Catch ex As Exception

      Throw
    End Try

  End Function ' CreateInsertCommand


  ''' <summary>
  ''' Create sql delete command. Only delete Types that not are not related to any file imported. 
  ''' </summary>
  ''' <param name="SqlTrx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function CreateDeleteCommand(ByVal SqlTrx As System.Data.SqlClient.SqlTransaction) As SqlCommand
    Dim _sql_query As StringBuilder
    Dim _sql_command As SqlCommand

    Try
      _sql_query = New StringBuilder()
      _sql_query.AppendLine(" DELETE FROM BLACKLIST_FILE_IMPORTED_TYPE WHERE BKLT_ID_TYPE = @pBKLT_ID_TYPE AND NOT EXISTS(SELECT TOP 1 * FROM BLACKLIST_FILE_IMPORTED WHERE BLKF_ID_TYPE = @pBKLT_ID_TYPE) ")
      _sql_command = New SqlCommand(_sql_query.ToString(), SqlTrx.Connection, SqlTrx)
      _sql_command.Parameters.Add("@pBKLT_ID_TYPE", SqlDbType.BigInt, 9, "BKLT_ID_TYPE")

      Return _sql_command

    Catch ex As Exception

      Throw
    End Try

  End Function ' CreateDeleteCommand

  ''' <summary>
  ''' Delete rows whose "is_deleted" column = 1
  ''' </summary>
  ''' <param name="Dt"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function DeleteRowsByColumnIsDeleted(ByRef Dt As DataTable) As Boolean
    Dim dr As DataRow

    Try
      For _idx As Integer = Dt.Rows.Count - 1 To 0 Step -1
        dr = Dt.Rows(_idx)
        If Not dr.IsNull(SQL_COLUMN_IS_DELETED) AndAlso dr(SQL_COLUMN_IS_DELETED) = 1 Then

          dr.Delete()
        End If
      Next

      Return True
    Catch ex As Exception
      Log.Exception(ex)

    End Try

    Return False
  End Function

#End Region ' Private

End Class
