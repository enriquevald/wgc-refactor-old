﻿'-------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_booking_terminal_config.vb
' DESCRIPTION:   Terminal booking configuration class
' AUTHOR:        Toni Téllez
' CREATION DATE: 24-JUL-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 24-JUL-2017  ATB    Initial version (PBI 28710: EGM Reserve – Settings(WIGOS-893))
' 08-AUG-2017  ATB    PBI 28710: EGM Reserve – Settings
' 10-AUG-2017  ATB    PBI 28710: EGM Reserve – Settings (WIGOS-4297)
' 03-APR-2018  JGC    Bug 32138:[WIGOS-9605]: Terminal Reservation Configuration - Changes to Interval field doesn't been saved
' 06-APR-2018  RLO    WIGOS-9605 Terminal Reservation Configuration - Changes to Interval field doesn't been saved
' 19-APR-2018  EOR    Bug 32394: WIGOS-10118 A warning message is not appear indicating that the changes will be lost 
'-------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports WSI.Common
Imports System.Runtime.InteropServices
Imports System.Text
Imports System.Data.SqlClient
Imports WSI.Common.Utilities
Imports System.Xml

Public Class CLASS_BOOKING_TERMINAL_CONFIG
  Inherits CLASS_BASE

#Region " Structures "

  Public Class TYPE_BOOKING_TERMINAL_CONFIG

    Public reservation_enabled As Boolean
    Public allow_card As Boolean
    Public reservation_level_01 As Boolean
    Public reservation_level_02 As Boolean
    Public reservation_level_03 As Boolean
    Public reservation_level_04 As Boolean
    Public time_level_01 As Integer
    Public time_level_02 As Integer
    Public time_level_03 As Integer
    Public time_level_04 As Integer
    Public terminals_level_01 As List(Of String)
    Public terminals_level_02 As List(Of String)
    Public terminals_level_03 As List(Of String)
    Public terminals_level_04 As List(Of String)
    Public coin_level_01 As Decimal
    Public coin_level_02 As Decimal
    Public coin_level_03 As Decimal
    Public coin_level_04 As Decimal
    Public type_level_01 As Type_Level
    Public type_level_02 As Type_Level
    Public type_level_03 As Type_Level
    Public type_level_04 As Type_Level
    Public terminal_interval As Integer
    Public terminals_01 As List(Of Integer)
    Public terminals_02 As List(Of Integer)
    Public terminals_03 As List(Of Integer)
    Public terminals_04 As List(Of Integer)
    Public account_time_level_01 As ReservedTerminal.AccountTimeLevel
    Public account_time_level_02 As ReservedTerminal.AccountTimeLevel
    Public account_time_level_03 As ReservedTerminal.AccountTimeLevel
    Public account_time_level_04 As ReservedTerminal.AccountTimeLevel
    Public terminal_list_01 As String
    Public terminal_list_02 As String
    Public terminal_list_03 As String
    Public terminal_list_04 As String

  End Class

#End Region ' Structures

#Region " Enums "
  Enum Type_Level
    All = 0
    Selected = 1
    AllBut = 2
  End Enum
#End Region ' Enums 

#Region " Members "

  Protected m_terminal_configuration As New TYPE_BOOKING_TERMINAL_CONFIG()

#End Region ' Members

#Region " Properties "

  Public Property ReservationEnabled() As Boolean
    Get
      Return m_terminal_configuration.reservation_enabled
    End Get
    Set(ByVal Value As Boolean)
      m_terminal_configuration.reservation_enabled = Value
    End Set
  End Property

  Public Property AllowCard() As Boolean
    Get
      Return m_terminal_configuration.allow_card
    End Get
    Set(ByVal Value As Boolean)
      m_terminal_configuration.allow_card = Value
    End Set
  End Property

  Public Property ReservationLevel01() As Boolean
    Get
      Return m_terminal_configuration.reservation_level_01
    End Get
    Set(ByVal value As Boolean)
      m_terminal_configuration.reservation_level_01 = value
    End Set
  End Property

  Public Property ReservationLevel02() As Boolean
    Get
      Return m_terminal_configuration.reservation_level_02
    End Get
    Set(ByVal value As Boolean)
      m_terminal_configuration.reservation_level_02 = value
    End Set
  End Property

  Public Property ReservationLevel03() As Boolean
    Get
      Return m_terminal_configuration.reservation_level_03
    End Get
    Set(ByVal value As Boolean)
      m_terminal_configuration.reservation_level_03 = value
    End Set
  End Property

  Public Property ReservationLevel04() As Boolean
    Get
      Return m_terminal_configuration.reservation_level_04
    End Get
    Set(ByVal value As Boolean)
      m_terminal_configuration.reservation_level_04 = value
    End Set
  End Property

  Public Property TimeLevel01() As Integer
    Get
      Return m_terminal_configuration.time_level_01
    End Get
    Set(ByVal value As Integer)
      m_terminal_configuration.time_level_01 = value
    End Set
  End Property

  Public Property TimeLevel02() As Integer
    Get
      Return m_terminal_configuration.time_level_02
    End Get
    Set(ByVal value As Integer)
      m_terminal_configuration.time_level_02 = value
    End Set
  End Property

  Public Property TimeLevel03() As Integer
    Get
      Return m_terminal_configuration.time_level_03
    End Get
    Set(ByVal value As Integer)
      m_terminal_configuration.time_level_03 = value
    End Set
  End Property

  Public Property TimeLevel04() As Integer
    Get
      Return m_terminal_configuration.time_level_04
    End Get
    Set(ByVal value As Integer)
      m_terminal_configuration.time_level_04 = value
    End Set
  End Property

  Public Property TerminalsLevel01() As List(Of String)
    Get
      Return m_terminal_configuration.terminals_level_01
    End Get
    Set(ByVal value As List(Of String))
      m_terminal_configuration.terminals_level_01 = value
    End Set
  End Property

  Public Property TerminalsLevel02() As List(Of String)
    Get
      Return m_terminal_configuration.terminals_level_02
    End Get
    Set(ByVal value As List(Of String))
      m_terminal_configuration.terminals_level_02 = value
    End Set
  End Property

  Public Property TerminalsLevel03() As List(Of String)
    Get
      Return m_terminal_configuration.terminals_level_03
    End Get
    Set(ByVal value As List(Of String))
      m_terminal_configuration.terminals_level_03 = value
    End Set
  End Property

  Public Property TerminalsLevel04() As List(Of String)
    Get
      Return m_terminal_configuration.terminals_level_04
    End Get
    Set(ByVal value As List(Of String))
      m_terminal_configuration.terminals_level_04 = value
    End Set
  End Property

  Public Property CoinInLevel01() As Decimal
    Get
      Return m_terminal_configuration.coin_level_01
    End Get
    Set(ByVal value As Decimal)
      m_terminal_configuration.coin_level_01 = value
    End Set
  End Property

  Public Property CoinInLevel02() As Decimal
    Get
      Return m_terminal_configuration.coin_level_02
    End Get
    Set(ByVal value As Decimal)
      m_terminal_configuration.coin_level_02 = value
    End Set
  End Property

  Public Property CoinInLevel03() As Decimal
    Get
      Return m_terminal_configuration.coin_level_03
    End Get
    Set(ByVal value As Decimal)
      m_terminal_configuration.coin_level_03 = value
    End Set
  End Property

  Public Property CoinInLevel04() As Decimal
    Get
      Return m_terminal_configuration.coin_level_04
    End Get
    Set(ByVal value As Decimal)
      m_terminal_configuration.coin_level_04 = value
    End Set
  End Property

  Public Property TypeLevel01() As Type_Level
    Get
      Return m_terminal_configuration.type_level_01
    End Get
    Set(value As Type_Level)
      m_terminal_configuration.type_level_01 = value
    End Set
  End Property

  Public Property TypeLevel02() As Type_Level
    Get
      Return m_terminal_configuration.type_level_02
    End Get
    Set(value As Type_Level)
      m_terminal_configuration.type_level_02 = value
    End Set
  End Property

  Public Property TypeLevel03() As Type_Level
    Get
      Return m_terminal_configuration.type_level_03
    End Get
    Set(value As Type_Level)
      m_terminal_configuration.type_level_03 = value
    End Set
  End Property

  Public Property TypeLevel04() As Type_Level
    Get
      Return m_terminal_configuration.type_level_04
    End Get
    Set(value As Type_Level)
      m_terminal_configuration.type_level_04 = value
    End Set
  End Property

  Public Property TerminalInterval() As Integer
    Get
      Return m_terminal_configuration.terminal_interval
    End Get
    Set(value As Integer)
      m_terminal_configuration.terminal_interval = value
    End Set
  End Property

  Public Property Terminals01() As List(Of Integer)
    Get
      Return m_terminal_configuration.terminals_01
    End Get
    Set(value As List(Of Integer))
      m_terminal_configuration.terminals_01 = value
    End Set
  End Property

  Public Property Terminals02() As List(Of Integer)
    Get
      Return m_terminal_configuration.terminals_02
    End Get
    Set(value As List(Of Integer))
      m_terminal_configuration.terminals_02 = value
    End Set
  End Property

  Public Property Terminals03() As List(Of Integer)
    Get
      Return m_terminal_configuration.terminals_03
    End Get
    Set(value As List(Of Integer))
      m_terminal_configuration.terminals_03 = value
    End Set
  End Property

  Public Property Terminals04() As List(Of Integer)
    Get
      Return m_terminal_configuration.terminals_04
    End Get
    Set(value As List(Of Integer))
      m_terminal_configuration.terminals_04 = value
    End Set
  End Property

  Public Property AccountTimeLevel01 As ReservedTerminal.AccountTimeLevel
    Get
      Return m_terminal_configuration.account_time_level_01
    End Get
    Set(value As ReservedTerminal.AccountTimeLevel)
      m_terminal_configuration.account_time_level_01 = value
    End Set
  End Property

  Public Property AccountTimeLevel02 As ReservedTerminal.AccountTimeLevel
    Get
      Return m_terminal_configuration.account_time_level_02
    End Get
    Set(value As ReservedTerminal.AccountTimeLevel)
      m_terminal_configuration.account_time_level_02 = value
    End Set
  End Property

  Public Property AccountTimeLevel03 As ReservedTerminal.AccountTimeLevel
    Get
      Return m_terminal_configuration.account_time_level_03
    End Get
    Set(value As ReservedTerminal.AccountTimeLevel)
      m_terminal_configuration.account_time_level_03 = value
    End Set
  End Property

  Public Property AccountTimeLevel04 As ReservedTerminal.AccountTimeLevel
    Get
      Return m_terminal_configuration.account_time_level_04
    End Get
    Set(value As ReservedTerminal.AccountTimeLevel)
      m_terminal_configuration.account_time_level_04 = value
    End Set
  End Property

  Public Property TerminalList01 As String
    Get
      Return m_terminal_configuration.terminal_list_01
    End Get
    Set(value As String)
      m_terminal_configuration.terminal_list_01 = value
    End Set
  End Property

  Public Property TerminalList02 As String
    Get
      Return m_terminal_configuration.terminal_list_02
    End Get
    Set(value As String)
      m_terminal_configuration.terminal_list_02 = value
    End Set
  End Property

  Public Property TerminalList03 As String
    Get
      Return m_terminal_configuration.terminal_list_03
    End Get
    Set(value As String)
      m_terminal_configuration.terminal_list_03 = value
    End Set
  End Property

  Public Property TerminalList04 As String
    Get
      Return m_terminal_configuration.terminal_list_04
    End Get
    Set(value As String)
      m_terminal_configuration.terminal_list_04 = value
    End Set
  End Property

#End Region ' Properties

#Region " Overrides functions "

  ''' <summary>
  ''' Constructor
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub New()
    TerminalsLevel01 = New List(Of String)()
    TerminalsLevel02 = New List(Of String)()
    TerminalsLevel03 = New List(Of String)()
    TerminalsLevel04 = New List(Of String)()

    Terminals01 = New List(Of Integer)()
    Terminals02 = New List(Of Integer)()
    Terminals03 = New List(Of Integer)()
    Terminals04 = New List(Of Integer)()

    AccountTimeLevel01 = New ReservedTerminal.AccountTimeLevel()
    AccountTimeLevel02 = New ReservedTerminal.AccountTimeLevel()
    AccountTimeLevel03 = New ReservedTerminal.AccountTimeLevel()
    AccountTimeLevel04 = New ReservedTerminal.AccountTimeLevel()
  End Sub

  ''' <summary>
  ''' Update the General Params with the new data
  ''' </summary>
  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return ExecuteInsertOrUpdate()
  End Function ' DB_Update

  ''' <summary>
  ''' Inserts the given object to the database.
  ''' </summary>
  ''' <param name="SqlCtx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return ExecuteInsertOrUpdate()
  End Function ' DB_Insert

  ''' <summary>
  ''' Deletes the given object from the database.
  ''' </summary>
  ''' <param name="SqlCtx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_OK
  End Function ' DB_Delete

  ''' <summary>
  ''' Returns the related auditor data for the current object
  ''' </summary>
  ''' 
  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim _auditor_data As CLASS_AUDITOR_DATA
    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_TERMINALS_BOOKING)

    Dim _gp_level_01 As String
    Dim _gp_level_02 As String
    Dim _gp_level_03 As String
    Dim _gp_level_04 As String

    _gp_level_01 = " (" & GeneralParam.GetString("PlayerTracking", "Level01.Name") & ") "
    _gp_level_02 = " (" & GeneralParam.GetString("PlayerTracking", "Level02.Name") & ") "
    _gp_level_03 = " (" & GeneralParam.GetString("PlayerTracking", "Level03.Name") & ") "
    _gp_level_04 = " (" & GeneralParam.GetString("PlayerTracking", "Level04.Name") & ") "

    Call _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(8524), "") ' 8366 "EGM Reserve Settings"

    ' Enabled
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8527), Me.ReservationEnabled) ' 8527 "Enabled"

    ' Allow
    Call _auditor_data.SetField(0, Me.AllowCard, NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(8694)))

    ' Reservation Level 01
    Call _auditor_data.SetField(0, Me.ReservationLevel01, _gp_level_01 & GLB_NLS_GUI_PLAYER_TRACKING.GetString(260))

    ' Reservation Level 02
    Call _auditor_data.SetField(0, Me.ReservationLevel02, _gp_level_02 & GLB_NLS_GUI_PLAYER_TRACKING.GetString(260))

    ' Reservation Level 03
    Call _auditor_data.SetField(0, Me.ReservationLevel03, _gp_level_03 & GLB_NLS_GUI_PLAYER_TRACKING.GetString(260))

    ' Reservation Level 04
    Call _auditor_data.SetField(0, Me.ReservationLevel04, _gp_level_04 & GLB_NLS_GUI_PLAYER_TRACKING.GetString(260))

    ' TimeLevel01
    Call _auditor_data.SetField(0, Me.TimeLevel01, _gp_level_01 & GLB_NLS_GUI_PLAYER_TRACKING.GetString(8528))   ' 8528 "Time"

    ' TimeLevel02
    Call _auditor_data.SetField(0, Me.TimeLevel02, _gp_level_02 & GLB_NLS_GUI_PLAYER_TRACKING.GetString(8528))   ' 8528 "Time"

    ' TimeLevel03
    Call _auditor_data.SetField(0, Me.TimeLevel03, _gp_level_03 & GLB_NLS_GUI_PLAYER_TRACKING.GetString(8528))   ' 8528 "Time"

    ' TimeLevel04
    Call _auditor_data.SetField(0, Me.TimeLevel04, _gp_level_04 & GLB_NLS_GUI_PLAYER_TRACKING.GetString(8528))   ' 8528 "Time"

    ' TerminalsLevel01
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(4895), Me.TerminalsLevel01.ToArray(), ": " & _gp_level_01 & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4895))   ' 4895 "Terminals"

    ' TerminalsLevel02
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(4895), Me.TerminalsLevel02.ToArray(), ": " & _gp_level_02 & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4895))   ' 4895 "Terminals"

    ' TerminalsLevel03
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(4895), Me.TerminalsLevel03.ToArray(), ": " & _gp_level_03 & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4895))   ' 4895 "Terminals"

    ' TerminalsLevel04
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(4895), Me.TerminalsLevel04.ToArray(), ": " & _gp_level_04 & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4895))   ' 4895 "Terminals"

    ' CoinInLevel01
    Call _auditor_data.SetField(0, FormatNumber(Me.CoinInLevel01, 2), _gp_level_01 & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2661))   ' 2661 "Coin-In"

    ' CoinInLevel02
    Call _auditor_data.SetField(0, FormatNumber(Me.CoinInLevel02, 2), _gp_level_02 & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2661))   ' 2661 "Coin-In"

    ' CoinInLevel03
    Call _auditor_data.SetField(0, FormatNumber(Me.CoinInLevel03, 2), _gp_level_03 & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2661))   ' 2661 "Coin-In"

    ' CoinInLevel04
    Call _auditor_data.SetField(0, FormatNumber(Me.CoinInLevel04, 2), _gp_level_04 & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2661))   ' 2661 "Coin-In"

    ' Type Level 01
    Call _auditor_data.SetField(0, Me.TypeLevel01.ToString(), _gp_level_01 & GLB_NLS_GUI_PLAYER_TRACKING.GetString(8548)) '8541 Type Level

    ' Type Level 02
    Call _auditor_data.SetField(0, Me.TypeLevel02.ToString(), _gp_level_02 & GLB_NLS_GUI_PLAYER_TRACKING.GetString(8548)) '8541 Type Level

    ' Type Level 03
    Call _auditor_data.SetField(0, Me.TypeLevel03.ToString(), _gp_level_03 & GLB_NLS_GUI_PLAYER_TRACKING.GetString(8548)) '8541 Type Level

    ' Type Level 04
    Call _auditor_data.SetField(0, Me.TypeLevel04.ToString(), _gp_level_04 & GLB_NLS_GUI_PLAYER_TRACKING.GetString(8548)) '8541 Type Level

    ' Terminal Interval
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8693), Me.TerminalInterval.ToString())

    Return _auditor_data

  End Function ' AuditorData

  ''' <summary>
  ''' Duplicate the tournament configuration object
  ''' </summary>
  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _service As WSI.Common.ReservedTerminal.IReservedTerminalConfigurationService
    Dim _read_base As WSI.Common.ReservedTerminal.ReservedTerminalConfigurationResponse
    Dim _xml_level_type As XmlDocument
    Dim _sql As StringBuilder
    Dim _data_table As DataTable

    Try
      _sql = New StringBuilder()
      _sql.AppendLine(" SELECT gp_subject_key,  gp_key_value")
      _sql.AppendLine(" FROM general_params                                                 ")
      _sql.AppendLine(" WHERE gp_group_key = 'Terminal'                                     ")
      _sql.AppendLine(" 	AND	gp_subject_key IN('Reservation.MonitorRefreshInterval'        ")
      _sql.AppendLine("                       , 'Reservation.Enabled'                       ")
      _sql.AppendLine("                       , 'AllowStartCardSessionWithAnotherReserve')  ")

      _data_table = GUI_GetTableUsingSQL(_sql.ToString(), 3)

      If _data_table IsNot Nothing Then
        For Each _dr As DataRow In _data_table.Rows
          Select Case _dr(0).ToString()
            Case "Reservation.MonitorRefreshInterval"
              TerminalInterval = CInt(_dr(1))
            Case "Reservation.Enabled"
              ReservationEnabled = CBool(_dr(1))
            Case "AllowStartCardSessionWithAnotherReserve"
              AllowCard = CBool(_dr(1))
          End Select
        Next
      End If

      _service = New WSI.Common.ReservedTerminal.ReservedTerminalConfigurationService
      _read_base = _service.Read()
      _xml_level_type = New XmlDocument()

      For Each _account_level As WSI.Common.ReservedTerminal.AccountTimeLevel In _read_base.Configurations.TimeLevels
        _account_level.TerminalList = IIf(String.IsNullOrEmpty(_account_level.TerminalList), "<ProviderList><ListType><Type>0</Type></ListType></ProviderList>", _account_level.TerminalList)
        _xml_level_type.LoadXml(_account_level.TerminalList)

        Select Case _account_level.LevelType
          Case HolderLevelType.A
            AccountTimeLevel01 = _account_level
            ReservationLevel01 = _account_level.Enabled
            TimeLevel01 = _account_level.Minutes
            CoinInLevel01 = _account_level.CoinIn
            TerminalsLevel01.AddRange(ReadTerminalsName(IIf(IsNothing(_account_level.Terminals), New List(Of Integer), _account_level.Terminals)))
            TypeLevel01 = _xml_level_type.SelectSingleNode("ProviderList").SelectSingleNode("ListType").SelectSingleNode("Type").InnerText
            Terminals01 = IIf(IsNothing(_account_level.Terminals), New List(Of Integer), _account_level.Terminals)
            TerminalList01 = _account_level.TerminalList
          Case HolderLevelType.B
            AccountTimeLevel02 = _account_level
            ReservationLevel02 = _account_level.Enabled
            TimeLevel02 = _account_level.Minutes
            CoinInLevel02 = _account_level.CoinIn
            TerminalsLevel02.AddRange(ReadTerminalsName(IIf(IsNothing(_account_level.Terminals), New List(Of Integer), _account_level.Terminals)))
            TypeLevel02 = _xml_level_type.SelectSingleNode("ProviderList").SelectSingleNode("ListType").SelectSingleNode("Type").InnerText
            Terminals02 = IIf(IsNothing(_account_level.Terminals), New List(Of Integer), _account_level.Terminals)
            TerminalList02 = _account_level.TerminalList
          Case HolderLevelType.C
            AccountTimeLevel03 = _account_level
            ReservationLevel03 = _account_level.Enabled
            TimeLevel03 = _account_level.Minutes
            CoinInLevel03 = _account_level.CoinIn
            TerminalsLevel03.AddRange(ReadTerminalsName(IIf(IsNothing(_account_level.Terminals), New List(Of Integer), _account_level.Terminals)))
            TypeLevel03 = _xml_level_type.SelectSingleNode("ProviderList").SelectSingleNode("ListType").SelectSingleNode("Type").InnerText
            Terminals03 = IIf(IsNothing(_account_level.Terminals), New List(Of Integer), _account_level.Terminals)
            TerminalList03 = _account_level.TerminalList
          Case HolderLevelType.D
            AccountTimeLevel04 = _account_level
            ReservationLevel04 = _account_level.Enabled
            TimeLevel04 = _account_level.Minutes
            CoinInLevel04 = _account_level.CoinIn
            TerminalsLevel04.AddRange(ReadTerminalsName(IIf(IsNothing(_account_level.Terminals), New List(Of Integer), _account_level.Terminals)))
            TypeLevel04 = _xml_level_type.SelectSingleNode("ProviderList").SelectSingleNode("ListType").SelectSingleNode("Type").InnerText
            Terminals04 = IIf(IsNothing(_account_level.Terminals), New List(Of Integer), _account_level.Terminals)
            TerminalList04 = _account_level.TerminalList
        End Select

      Next

      Return ENUM_STATUS.STATUS_OK
    Catch ex As Exception
      Return ENUM_STATUS.STATUS_ERROR
    End Try
  End Function ' Read

  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim _target As CLASS_BOOKING_TERMINAL_CONFIG

    _target = New CLASS_BOOKING_TERMINAL_CONFIG

    _target.AccountTimeLevel01 = Me.AccountTimeLevel01
    _target.AccountTimeLevel02 = Me.AccountTimeLevel02
    _target.AccountTimeLevel03 = Me.AccountTimeLevel03
    _target.AccountTimeLevel04 = Me.AccountTimeLevel04
    _target.AllowCard = Me.AllowCard
    _target.CoinInLevel01 = Me.CoinInLevel01
    _target.CoinInLevel02 = Me.CoinInLevel02
    _target.CoinInLevel03 = Me.CoinInLevel03
    _target.CoinInLevel04 = Me.CoinInLevel04
    _target.ReservationEnabled = Me.ReservationEnabled
    _target.ReservationLevel01 = Me.ReservationLevel01
    _target.ReservationLevel02 = Me.ReservationLevel02
    _target.ReservationLevel03 = Me.ReservationLevel03
    _target.ReservationLevel04 = Me.ReservationLevel04
    _target.TerminalInterval = Me.TerminalInterval
    _target.TerminalList01 = Me.TerminalList01
    _target.TerminalList02 = Me.TerminalList02
    _target.TerminalList03 = Me.TerminalList03
    _target.TerminalList04 = Me.TerminalList04
    _target.Terminals01 = Me.Terminals01
    _target.Terminals02 = Me.Terminals02
    _target.Terminals03 = Me.Terminals03
    _target.Terminals04 = Me.Terminals04
    _target.TerminalsLevel01 = Me.TerminalsLevel01
    _target.TerminalsLevel02 = Me.TerminalsLevel02
    _target.TerminalsLevel03 = Me.TerminalsLevel03
    _target.TerminalsLevel04 = Me.TerminalsLevel04
    _target.TimeLevel01 = Me.TimeLevel01
    _target.TimeLevel02 = Me.TimeLevel02
    _target.TimeLevel03 = Me.TimeLevel03
    _target.TimeLevel04 = Me.TimeLevel04
    _target.TypeLevel01 = Me.TypeLevel01
    _target.TypeLevel02 = Me.TypeLevel02
    _target.TypeLevel03 = Me.TypeLevel03
    _target.TypeLevel04 = Me.TypeLevel04


    Return _target

  End Function ' Duplicate

#End Region ' Overrides Functions

#Region " Private Functions "

  Private Function ReadTerminalsName(ByVal Terminals As List(Of Integer)) As List(Of String)
    Dim _cmd As SqlCommand
    Dim _sql As StringBuilder
    Dim _terminals_raw As String
    Dim _str_var_list As List(Of String)
    Dim _str_terminals As List(Of String)
    Dim _m_db_trx As DB_TRX
    Dim m_adapter As New SqlDataAdapter()
    _m_db_trx = New DB_TRX()
    _str_terminals = New List(Of String)()
    _str_var_list = New List(Of String)()

    _sql = New StringBuilder()
    _sql.Append("SELECT     TE_NAME                         ")
    _sql.Append("FROM       TERMINALS                       ")
    _sql.Append("WHERE      TE_TERMINAL_ID IN ({0})         ")
    _sql.Append("ORDER BY   TE_TERMINAL_ID                  ")

    For i As Integer = 0 To Terminals.Count - 1
      _str_var_list.Add("@pTerminal_" & i.ToString())
    Next
    _terminals_raw = String.Join(", ", _str_var_list.ToArray())
    If (_terminals_raw = "") Then
      _terminals_raw = "0"
    End If
    _cmd = New SqlCommand(String.Format(_sql.ToString(), _terminals_raw), _m_db_trx.SqlTransaction.Connection, _m_db_trx.SqlTransaction)

    For i As Integer = 0 To Terminals.Count - 1
      _cmd.Parameters.AddWithValue("@pTerminal_" & i, Terminals(i))
    Next

    Using _reader As SqlClient.SqlDataReader = _cmd.ExecuteReader()
      While (_reader.Read())
        _str_terminals.Add(_reader("TE_NAME"))
      End While
    End Using
    Return _str_terminals
  End Function

  Private Function ExecuteInsertOrUpdate() As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _response As ReservedTerminal.ReservedTerminalResponseBase
    Dim _service As ReservedTerminal.IReservedTerminalConfigurationService
    Dim _request As ReservedTerminal.ReservedTerminalSaveRequest
    Dim _aux_item As ReservedTerminal.AccountTimeLevel
    Dim _gp_update_list As List(Of String())

    Try

      Using _db_trx As New DB_TRX()
        _gp_update_list = New List(Of String())

        _gp_update_list.Add(GeneralParam.AddToList("Terminal", "Reservation.MonitorRefreshInterval", m_terminal_configuration.terminal_interval.ToString()))
        _gp_update_list.Add(GeneralParam.AddToList("Terminal", "Reservation.Enabled", IIf(m_terminal_configuration.reservation_enabled, "1", "0")))
        _gp_update_list.Add(GeneralParam.AddToList("Terminal", "AllowStartCardSessionWithAnotherReserve", IIf(m_terminal_configuration.allow_card, "1", "0")))

        For Each _general_param As String() In _gp_update_list
          If (Not GUI_Controls.DB_GeneralParam_Update(_general_param(GeneralParam.COLUMN_NAME.Group),
                                                      _general_param(GeneralParam.COLUMN_NAME.Subject),
                                                      _general_param(GeneralParam.COLUMN_NAME.Value),
                                                      _db_trx.SqlTransaction)) Then


            Return ENUM_DB_RC.DB_RC_ERROR_DB
          End If
        Next

        _db_trx.SqlTransaction.Commit()
      End Using


      _service = New ReservedTerminal.ReservedTerminalConfigurationService()
      _request = New ReservedTerminal.ReservedTerminalSaveRequest()
      _request.TimeLevels = New List(Of ReservedTerminal.AccountTimeLevel)

      _aux_item = New ReservedTerminal.AccountTimeLevel()
      _aux_item.LevelType = HolderLevelType.A
      _aux_item.Minutes = m_terminal_configuration.time_level_01
      _aux_item.TerminalList = m_terminal_configuration.terminal_list_01
      _aux_item.Terminals = m_terminal_configuration.terminals_01
      _aux_item.Enabled = m_terminal_configuration.reservation_level_01
      _aux_item.CoinIn = m_terminal_configuration.coin_level_01
      _request.TimeLevels.Add(_aux_item)

      _aux_item = New ReservedTerminal.AccountTimeLevel()
      _aux_item.LevelType = HolderLevelType.B
      _aux_item.Minutes = m_terminal_configuration.time_level_02
      _aux_item.TerminalList = m_terminal_configuration.terminal_list_02
      _aux_item.Terminals = m_terminal_configuration.terminals_02
      _aux_item.Enabled = m_terminal_configuration.reservation_level_02
      _aux_item.CoinIn = m_terminal_configuration.coin_level_02
      _request.TimeLevels.Add(_aux_item)

      _aux_item = New ReservedTerminal.AccountTimeLevel()
      _aux_item.LevelType = HolderLevelType.C
      _aux_item.Minutes = m_terminal_configuration.time_level_03
      _aux_item.TerminalList = m_terminal_configuration.terminal_list_03
      _aux_item.Terminals = m_terminal_configuration.terminals_03
      _aux_item.Enabled = m_terminal_configuration.reservation_level_03
      _aux_item.CoinIn = m_terminal_configuration.coin_level_03
      _request.TimeLevels.Add(_aux_item)

      _aux_item = New ReservedTerminal.AccountTimeLevel()
      _aux_item.LevelType = HolderLevelType.D
      _aux_item.Minutes = m_terminal_configuration.time_level_04
      _aux_item.TerminalList = m_terminal_configuration.terminal_list_04
      _aux_item.Terminals = m_terminal_configuration.terminals_04
      _aux_item.Enabled = m_terminal_configuration.reservation_level_04
      _aux_item.CoinIn = m_terminal_configuration.coin_level_04
      _request.TimeLevels.Add(_aux_item)

      _response = _service.Save(_request)

      If Not _response.Success Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR
      End If

    Catch
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR
    End Try

    Return ENUM_DB_RC.DB_RC_OK
  End Function

#End Region ' Private Functions

End Class
