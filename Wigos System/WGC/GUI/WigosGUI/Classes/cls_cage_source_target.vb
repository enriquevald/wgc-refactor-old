'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : cls_cage_amounts.vb
'
' DESCRIPTION : Cage Amounts class
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 06-NOV-2013  XMS    Initial version
' 18-NOV-2013  JPJ    Restricted amounts
' 20-DEC-2013  XMS    Fixed Bug #907: correct message that pop-up when don't write info in column Name
' 20-DEC-2013  XMS    Fixed Bug #909: the rows are white after save the row with info.
' 16-SEP-2014  OPC    Concepts: SQL Insert edited the System add.
'--------------------------------------------------------------------

Imports WSI.Common
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports System.Text


Public Class cls_cage_source_target
  Inherits CLASS_BASE

#Region " Constants "

  Public Const SQL_COLUMN_CST_ID As Integer = 0
  Public Const SQL_COLUMN_CST_NAME As Integer = 1
  Public Const SQL_COLUMN_CST_SOURCE As Integer = 2
  Public Const SQL_COLUMN_CST_TARGET As Integer = 3
  Private Const MARK_DELETED As Integer = 8

  Private Const DUMMY_ROWS As Int16 = 30
  Private Const CST_COLUMN_ACTION_NAME As String = "ACTION"
  Private Const CST_COLUMN_NAME As String = "CST_SOURCE_TARGET_NAME"
  Private Const CST_COLUMN_EDITABLE_NAME As String = "EDITABLE_ROW"

  ' OPC 17-SEP-2014
  Private Const CCM_CONCEPT_ID As Int64 = 0
  Private Const CCSTC_CONCEPT_ID As Int64 = 0
  Private Const CCSTC_CURRENCY As Boolean = False
  Private Const CCSTC_ENABLED As Boolean = True
  Private Const CCSTC_PRICE_FACTOR As Decimal = 1

#End Region       ' Constants 

#Region " Structures "

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_DATA_CAGE_SOURCE_TARGET
    Public data_table_cst As DataTable

    ' Init structure
    Public Sub New()
      data_table_cst = New DataTable()
    End Sub

  End Class

#End Region      ' Structures

#Region " Members "

  Protected m_data_cage_source_target As New TYPE_DATA_CAGE_SOURCE_TARGET

#End Region         ' Members

#Region " Enum "

  Private Enum ROWACTION
    NORMAL = 0
    MODIFIED = 1
    DELETED = 2
    ADDED = 3
  End Enum

  Private Enum ROWORDER
    NO = 0
    YES = 1
  End Enum

#End Region            ' Enum

#Region "Properties"

  Public Overloads Property DataCageSourceTarget() As DataTable
    Get
      Return m_data_cage_source_target.data_table_cst
    End Get

    Set(ByVal Value As DataTable)
      m_data_cage_source_target.data_table_cst = Value
    End Set

  End Property             ' DataCageSourceTarget

#End Region        ' Properties

#Region "Overrides"

  '----------------------------------------------------------------------------
  ' PURPOSE : Returns the related auditor data for the current object.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - The newly created audit object
  '
  ' NOTES :
  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim auditor_data As CLASS_AUDITOR_DATA
    Dim _dt As DataTable
    Dim _dv As DataView
    Dim _row As DataRow
    Dim _row2 As DataRow
    Dim _idx_row As Int64
    Dim _idx_blanks As Int64
    Dim _literal As String

    auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_CASHIER_CONFIGURATION)
    _dt = New DataTable
    auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(3376), "")

    Try

      ' m�ximo
      If Not Me.DataCageSourceTarget Is Nothing Then
        _dt = Me.DataCageSourceTarget.Copy
        _dt.Columns.Add(CST_COLUMN_ACTION_NAME, Type.GetType("System.Int16"))

        For Each _row In _dt.Rows
          Select Case _row.RowState
            Case DataRowState.Added
              _row(CST_COLUMN_ACTION_NAME) = ROWORDER.YES
              _row(CST_COLUMN_EDITABLE_NAME) = ROWACTION.ADDED

            Case DataRowState.Deleted
              _row.RejectChanges()
              _row(CST_COLUMN_ACTION_NAME) = ROWORDER.NO
              _row(CST_COLUMN_EDITABLE_NAME) = ROWACTION.DELETED

            Case DataRowState.Modified
              _row(CST_COLUMN_ACTION_NAME) = ROWORDER.NO
              _row(CST_COLUMN_EDITABLE_NAME) = ROWACTION.MODIFIED

            Case Else
              _row(CST_COLUMN_ACTION_NAME) = ROWORDER.NO
              _row(CST_COLUMN_EDITABLE_NAME) = ROWACTION.NORMAL
          End Select
        Next

        _dv = _dt.DefaultView
        _dv.Sort = "ACTION ASC"
        _dt = _dv.ToTable()
      End If

      For Each _row In _dt.Rows
        _idx_row = 0
        For Each _row2 In _dt.Select(CST_COLUMN_NAME & " = '" & _row.Item(CST_COLUMN_NAME).ToString & "'")
          If _row2(CST_COLUMN_EDITABLE_NAME) = ROWACTION.DELETED Then
            ' ELIMINADO
            Call auditor_data.SetField(0, AUDIT_NONE_STRING & " " & _
                                GLB_NLS_GUI_COMMONMISC.GetString(9), _
                               _row2(SQL_COLUMN_CST_NAME))

            _idx_row = _idx_row + 1

            Continue For
          End If

          ' MODIFICADO y A�ADIDO
          _literal = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3419) & ":" & _row2(SQL_COLUMN_CST_NAME) & "; " & _
                    GLB_NLS_GUI_PLAYER_TRACKING.GetString(3427) & ":" & IIf(_row2(SQL_COLUMN_CST_SOURCE), GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)) & "; " & _
                    GLB_NLS_GUI_PLAYER_TRACKING.GetString(3396) & ":" & IIf(_row2(SQL_COLUMN_CST_TARGET), GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)) & " "

          If _row2(CST_COLUMN_EDITABLE_NAME) = ROWACTION.ADDED Then
            _literal += GLB_NLS_GUI_COMMONMISC.GetString(10)
          End If

          Call auditor_data.SetField(0, _
                              _literal, _
                              _row2(SQL_COLUMN_CST_NAME))

          _idx_row = _idx_row + 1
        Next

        For _idx_blanks = _idx_row To DUMMY_ROWS
          Call auditor_data.SetField(0, AUDIT_NONE_STRING, _row.Item(CST_COLUMN_NAME).ToString)
        Next
      Next

    Catch ex As Exception

      Debug.Print(ex.Message)
    End Try

    Return auditor_data

  End Function   ' AuditorData

  '----------------------------------------------------------------------------
  ' PURPOSE : Deletes the given object from the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DEPENDENCIES
  '
  ' NOTES :
  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
  End Function     ' DB_Delete

  '----------------------------------------------------------------------------
  ' PURPOSE : Inserts the given object to the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES :
  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
  End Function     ' DB_Insert

  '----------------------------------------------------------------------------
  ' PURPOSE : Reads from the database into the given object
  '
  ' PARAMS :
  '     - INPUT :
  '         - ObjectId: An object, it must be 'casted' to the desired KEY.
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '
  ' NOTES :
  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim rc As Integer

    ' Read currency data
    rc = DataCageSourceTarget_DbRead(SqlCtx)

    Select Case rc
      Case ENUM_DB_RC.DB_RC_OK
        Return ENUM_STATUS.STATUS_OK

      Case ENUM_DB_RC.DB_RC_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function       ' DB_Read

  '----------------------------------------------------------------------------
  ' PURPOSE : Updates the given object to the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES :
  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim rc As Integer

    ' Update currency data
    rc = DataCageAmounts_DbUpdate(SqlCtx)

    Select Case rc
      Case ENUM_DB_RC.DB_RC_OK
        Return ENUM_STATUS.STATUS_OK

      Case ENUM_DB_RC.DB_RC_ERROR_NOT_FOUND
        Return CLASS_BASE.ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function     ' DB_Update

  '----------------------------------------------------------------------------
  ' PURPOSE: Returns a duplicate of the given object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE
    Dim temp_source_target_data As cls_cage_source_target

    temp_source_target_data = New cls_cage_source_target

    temp_source_target_data.m_data_cage_source_target.data_table_cst = Me.m_data_cage_source_target.data_table_cst.Copy()

    Return temp_source_target_data
  End Function     ' Duplicate

#End Region         ' Overrides

#Region "Private Functions"

  '----------------------------------------------------------------------------
  ' PURPOSE: Reads an object from the database.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES:
  Private Function DataCageSourceTarget_DbRead(ByVal Context As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Try
      Using _db_trx As New DB_TRX()

        'Target table
        m_data_cage_source_target.data_table_cst = GetCageSourceTarget(_db_trx)

      End Using

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

    Catch ex As Exception

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR
    End Try

  End Function      ' DataCageAmounts_DbRead

  '----------------------------------------------------------------------------
  ' PURPOSE: Updates the given object into the database.
  '
  ' PARAMS:
  '   - INPUT:  None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES:
  Private Function DataCageAmounts_DbUpdate(ByVal Context As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _sql_trx As System.Data.SqlClient.SqlTransaction
    Dim _num_of_rows As Integer
    Dim _cst_source_target_to_del() As DataRow
    Dim _id As Int64
    Dim _name As String
    Dim _source As Boolean
    Dim _target As Boolean
    Dim _sb As StringBuilder
    Dim _cls_cage_control As New CLASS_CAGE_CONTROL
    Dim _currencies_accepted_GP As String

    _currencies_accepted_GP = String.Empty

    Try
      _sql_trx = Nothing

      Using _db_trx As New DB_TRX()

        _sql_trx = _db_trx.SqlTransaction
        _sb = New StringBuilder()

        'Bucle para recorrer los iso_codes que se han modificado
        _num_of_rows = m_data_cage_source_target.data_table_cst.Select("", "", DataViewRowState.CurrentRows).Length

        _currencies_accepted_GP = _currencies_accepted_GP.Replace(";;", ";")
        _currencies_accepted_GP = _currencies_accepted_GP.TrimStart(";")

        'UPDATE
        _num_of_rows = m_data_cage_source_target.data_table_cst.Select("", "", DataViewRowState.ModifiedCurrent).Length
        For _idx_row_to As Integer = 0 To _num_of_rows - 1
          _name = m_data_cage_source_target.data_table_cst.Select("", "", DataViewRowState.ModifiedCurrent)(_idx_row_to).Item(SQL_COLUMN_CST_NAME)
          _source = m_data_cage_source_target.data_table_cst.Select("", "", DataViewRowState.ModifiedCurrent)(_idx_row_to).Item(SQL_COLUMN_CST_SOURCE)
          _target = m_data_cage_source_target.data_table_cst.Select("", "", DataViewRowState.ModifiedCurrent)(_idx_row_to).Item(SQL_COLUMN_CST_TARGET)
          _id = m_data_cage_source_target.data_table_cst.Select("", "", DataViewRowState.ModifiedCurrent)(_idx_row_to).Item(SQL_COLUMN_CST_ID)


          _sb.Length = 0
          _sb.AppendLine("UPDATE  CAGE_SOURCE_TARGET                      ")
          _sb.AppendLine(" SET    CST_SOURCE_TARGET_NAME = @pCstName,     ")
          _sb.AppendLine("        CST_SOURCE             = @pCstSource,   ")
          _sb.AppendLine("        CST_TARGET             = @pCstTarget    ")
          _sb.AppendLine("        WHERE CST_SOURCE_TARGET_ID   = @pCstId        ")

          Using _cmd As SqlCommand = New SqlCommand(_sb.ToString(), _sql_trx.Connection, _sql_trx)
            _cmd.Parameters.Add("@pCstName", SqlDbType.NVarChar).Value = _name
            _cmd.Parameters.Add("@pCstSource", SqlDbType.NVarChar).Value = _source
            _cmd.Parameters.Add("@pCstTarget", SqlDbType.NVarChar).Value = _target
            _cmd.Parameters.Add("@pCstId", SqlDbType.BigInt).Value = _id

            _cmd.ExecuteNonQuery()
          End Using
        Next


        ' Now is possible delete the row with the mark for more simple update of DB.
        _cst_source_target_to_del = m_data_cage_source_target.data_table_cst.Select("EDITABLE_ROW = 8", "")
        For _idx_row_to_del As Integer = 0 To _cst_source_target_to_del.Length - 1
          _cst_source_target_to_del(_idx_row_to_del).Delete()
        Next

        'DELETE
        _num_of_rows = m_data_cage_source_target.data_table_cst.Select("", "", DataViewRowState.Deleted).Length
        For _idx_row_to As Integer = 0 To _num_of_rows - 1
          _id = m_data_cage_source_target.data_table_cst.Select("", "", DataViewRowState.Deleted)(_idx_row_to).Item(SQL_COLUMN_CST_ID, DataRowVersion.Original)
          _name = m_data_cage_source_target.data_table_cst.Select("", "", DataViewRowState.Deleted)(_idx_row_to).Item(SQL_COLUMN_CST_NAME, DataRowVersion.Original)
          _source = m_data_cage_source_target.data_table_cst.Select("", "", DataViewRowState.Deleted)(_idx_row_to).Item(SQL_COLUMN_CST_SOURCE, DataRowVersion.Original)
          _target = m_data_cage_source_target.data_table_cst.Select("", "", DataViewRowState.Deleted)(_idx_row_to).Item(SQL_COLUMN_CST_TARGET, DataRowVersion.Original)

          If Not _cls_cage_control.CheckIfStillExistCustom(CLng(_id)) Then
            NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4456), ENUM_MB_TYPE.MB_TYPE_WARNING, , , _name) ' Custom was deleted
          Else
            'Has movements?
            If HasMovements(_id) Then
              Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4455), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
            Else

              _sb.Length = 0
              _sb.AppendLine("DELETE FROM  CAGE_SOURCE_TARGET ")
              _sb.AppendLine(" WHERE       CST_SOURCE_TARGET_NAME = @pCstName ")
              _sb.AppendLine("             AND CST_SOURCE = @pCstSource ")
              _sb.AppendLine("             AND CST_TARGET = @pCstTarget ")

              Using _cmd As SqlCommand = New SqlCommand(_sb.ToString(), _sql_trx.Connection, _sql_trx)
                _cmd.Parameters.Add("@pCstName", SqlDbType.NVarChar).Value = _name
                _cmd.Parameters.Add("@pCstSource", SqlDbType.NVarChar).Value = _source
                _cmd.Parameters.Add("@pCstTarget", SqlDbType.NVarChar).Value = _target

                _cmd.ExecuteNonQuery()
              End Using

              Call CageMeters.DeleteCageMeters(_id, _sql_trx)
              Call CageMeters.DeleteCageSourceTargetConcepts(_id, _sql_trx)
            End If
          End If

        Next

        ' --- Currency denomination
        'INSERT
        _num_of_rows = m_data_cage_source_target.data_table_cst.Select("", "", DataViewRowState.Added).Length
        For _idx_row_to As Integer = 0 To _num_of_rows - 1
          _id = GetMaxId(_sql_trx)
          _name = m_data_cage_source_target.data_table_cst.Select("", "", DataViewRowState.Added)(_idx_row_to).Item(SQL_COLUMN_CST_NAME)
          _source = m_data_cage_source_target.data_table_cst.Select("", "", DataViewRowState.Added)(_idx_row_to).Item(SQL_COLUMN_CST_SOURCE)
          _target = m_data_cage_source_target.data_table_cst.Select("", "", DataViewRowState.Added)(_idx_row_to).Item(SQL_COLUMN_CST_TARGET)

          _sb.Length = 0
          _sb.AppendLine(" INSERT INTO CAGE_SOURCE_TARGET")
          _sb.AppendLine("            (CST_SOURCE_TARGET_ID, CST_SOURCE_TARGET_NAME, CST_SOURCE, CST_TARGET)")
          _sb.AppendLine(" VALUES     (@pCstId, @pCstName, @pCstSource, @pCstTarget)")

          Using _cmd As SqlCommand = New SqlCommand(_sb.ToString(), _sql_trx.Connection, _sql_trx)
            _cmd.Parameters.Add("@pCstId", SqlDbType.BigInt).Value = _id
            _cmd.Parameters.Add("@pCstName", SqlDbType.NVarChar).Value = _name
            _cmd.Parameters.Add("@pCstSource", SqlDbType.NVarChar).Value = _source
            _cmd.Parameters.Add("@pCstTarget", SqlDbType.NVarChar).Value = _target

            _cmd.ExecuteNonQuery()
          End Using

          ' OPC 17-SEP-2014
          Call CageMeters.CreateCageMeters(_id, _
                                      CCM_CONCEPT_ID, _
                                      Nothing, _
                                      CageMeters.CreateCageMetersOption.CageMeters_CageAllOpenSessionMeters, _
                                      _sql_trx)

          Call CageMeters.CreateCageSourceTargetConcepts(CCSTC_CONCEPT_ID, _
                                                    _id, _
                                                    CageMeters.CageSourceTargetType.CageInputOutput, _
                                                    CCSTC_CURRENCY, _
                                                    Nothing, _
                                                    CCSTC_ENABLED, _
                                                    CCSTC_PRICE_FACTOR, _
                                                    _sql_trx)

        Next

        ' --- Ok
        _db_trx.Commit()

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
      End Using

    Catch ex As Exception

      Log.Exception(ex)
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

  End Function    ' CurrencyData_DbUpdate

  ' PURPOSE: Gets the Max concept_id +1
  '
  '    - INPUT:
  '      _sql_trx: Sql Transaction
  '    - OUTPUT:
  '
  ' RETURNS:
  '    - Sufix : Returns an Integer with the new Concept Id.
  Private Function GetMaxId(ByVal _sql_trx As SqlTransaction) As Integer

    Dim _id As Integer = 0
    Dim _obj As Object
    Dim _sb As New StringBuilder

    _sb.AppendLine(" SELECT CASE WHEN ISNULL(MAX(CST_SOURCE_TARGET_ID + 1),1000) < 1000 THEN 1000 ELSE MAX(CST_SOURCE_TARGET_ID + 1) END FROM CAGE_SOURCE_TARGET")

    Using _cmd As SqlCommand = New SqlCommand(_sb.ToString(), _sql_trx.Connection, _sql_trx)

      _obj = _cmd.ExecuteScalar()

      If Not _obj Is Nothing AndAlso Not IsDBNull(_obj) Then
        _id = GUI_ParseNumber(_obj.ToString())
      End If

    End Using

    Return _id

  End Function

  ' PURPOSE: Gets the representive value
  '
  '    - INPUT:
  '      Value: true or false
  '    - OUTPUT:
  '
  ' RETURNS:
  '    - Sufix : Contains the sufix for the audit
  Private Function GetRepresentativeValue(ByVal Value As String) As String

    If Value Then
      Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(698) ' "Si"
    Else
      Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(699) ' "No"
    End If

  End Function       ' GetRepresentativeValue


  '----------------------------------------------------------------------------
  ' PURPOSE : Read object (amount movements) from the DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - DbTrx: Transaction
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - Datatable:
  '
  ' NOTES :
  Private Function GetCageSourceTarget(ByVal DbTrx As DB_TRX) As DataTable
    Dim _table As DataTable
    Dim _sb As StringBuilder

    _table = New DataTable()
    _sb = New StringBuilder()

    Try

      _sb.AppendLine("    SELECT cst_source_target_id ")
      _sb.AppendLine("    , cst_source_target_name    ")
      _sb.AppendLine("    , cst_source                ")
      _sb.AppendLine("    , cst_target                ")
      _sb.AppendLine("    , 0 AS EDITABLE_ROW         ")
      _sb.AppendLine("    FROM cage_source_target     ")
      _sb.AppendLine("    ORDER BY CST_SOURCE_TARGET_NAME        ")

      Using _sql_cmd As New SqlCommand(_sb.ToString)

        Using _sql_da As New SqlDataAdapter(_sql_cmd)

          DbTrx.Fill(_sql_da, _table)

        End Using

      End Using

    Catch ex As Exception

      Log.Exception(ex)
    End Try

    Return _table
  End Function              ' GetCageSourceTarget

#End Region ' Private Functions

#Region "Public Functions"

  '----------------------------------------------------------------------------
  ' PURPOSE : Read the movement of SOURCE or TARGET
  '
  ' PARAMS :
  '     - INPUT :
  '         - DbTrx: Transaction
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - Datatable:
  '
  ' NOTES :
  Public Function IsMovementsSourceTarget(ByVal idx As Integer) As DataTable
    Dim _table As DataTable
    Dim _sb As StringBuilder

    _table = New DataTable()
    _sb = New StringBuilder()

    Try
      Using _db_trx As New DB_TRX()
        _sb.AppendLine("    SELECT CGM_SOURCE_TARGET_ID ")
        _sb.AppendLine("    FROM CAGE_MOVEMENTS CGM    ")
        _sb.AppendLine("    INNER JOIN CAGE_PENDING_MOVEMENTS CPM ON CGM.CGM_MOVEMENT_ID = CPM.CPM_MOVEMENT_ID  ")
        _sb.AppendLine("    WHERE CGM_SOURCE_TARGET_ID = @pIdCageSourceTarget ")

        Using _sql_cmd As New SqlCommand(_sb.ToString)
          _sql_cmd.Parameters.Add("@pIdCageSourceTarget", SqlDbType.Int).Value = idx

          Using _sql_da As New SqlDataAdapter(_sql_cmd)

            _db_trx.Fill(_sql_da, _table)

          End Using

        End Using

      End Using

    Catch ex As Exception

      Log.Exception(ex)
    End Try

    Return _table
  End Function              ' IsMovementsSourceTarget

  ' PURPOSE : To get if that source/target has some movements
  '
  ' PARAMS :
  '     - INPUT :
  '         - CustomId
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '         - True if has movements
  '
  Public Function HasMovements(ByVal CustomId As Long) As Boolean
    Dim _sb As StringBuilder

    HasMovements = True
    _sb = New StringBuilder

    _sb.AppendLine(" SELECT   TOP 1 CGM_MOVEMENT_ID ")
    _sb.AppendLine("   FROM   CAGE_MOVEMENTS ")
    _sb.AppendLine("  WHERE   CGM_SOURCE_TARGET_ID = @pCustomId ")

    Try

      Using _db_trx As New DB_TRX()
        Using _cmd As SqlCommand = New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _cmd.Parameters.Add("@pCustomId", SqlDbType.BigInt).Value = CustomId

          Using _sql_reader As SqlDataReader = _db_trx.ExecuteReader(_cmd)

            If Not _sql_reader.Read() Then
              HasMovements = False
            End If

          End Using
        End Using
      End Using

    Catch ex As Exception
      Return False
    End Try

    Return HasMovements
  End Function ' HasMovements

  '----------------------------------------------------------------------------
  ' PURPOSE : Determines if the sources/target have any concept asociated.
  '
  ' PARAMS :
  '     - INPUT :
  '         - TargetId: the index of sources/target
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - Boolean
  '
  ' NOTES :
  Public Shared Function InUse(ByVal TargetId As Int64) As Boolean
    Dim _sw_Exist As Boolean = False

    Using _db_trx As New DB_TRX()

      Dim _sb As StringBuilder
      _sb = New StringBuilder()

      _sb.AppendLine("SELECT   COUNT(*)")
      _sb.AppendLine("  FROM   CAGE_SOURCE_TARGET_CONCEPTS")
      _sb.AppendLine(" WHERE   CSTC_SOURCE_TARGET_ID = @pTargetId")
      _sb.AppendLine("         AND CSTC_CONCEPT_ID <> 0 ")

      Using _sql_cmd As New SqlCommand(_sb.ToString, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

        _sql_cmd.Parameters.Add("@pTargetId", SqlDbType.BigInt).Value = TargetId

        If _sql_cmd.ExecuteScalar > 0 Then
          _sw_Exist = True
        End If

      End Using

    End Using

    Return _sw_Exist
  End Function ' InUse


#End Region ' Public Functions

End Class
