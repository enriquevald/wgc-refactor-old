'-------------------------------------------------------------------
' Copyright � 2016 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_asection_schema
' DESCRIPTION:   About Us class
' AUTHOR:        Sergio Soria
' CREATION DATE: 20-SEPT-2016
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 20-SEPT-2016  SDS    Initial version
'--------------------------------------------------------------------
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports WSI.Common.MEDIA_TYPE
Imports System.Text
Imports System.IO
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
Imports System.Net
Imports WSI.Common


Public Class CLS_SECTION_SCHEMA
  Inherits CLASS_BASE

#Region " Constants "

  Protected Const AUDIT_LABEL_TITLE As Integer = 7601
  Protected Const AUDIT_LABEL_NAME As Integer = 7602
  Protected Const AUDIT_LABEL_SUBTITLE As Integer = 7603
  Protected Const AUDIT_LABEL_MAINTITLE As Integer = 7604
  Protected Const AUDIT_LABEL_MAINSUBTITLE As Integer = 7605
  Protected Const AUDIT_LABEL_ABSTRACT As Integer = 7606
  Protected Const AUDIT_LABEL_DESCRIPTION As Integer = 7607
  Protected Const AUDIT_LABEL_HEADLINE As Integer = 7608
  Protected Const AUDIT_LABEL_BUTTON As Integer = 7609
  Protected Const AUDIT_LABEL_ACTIVE As Integer = 7599
  Protected Const AUDIT_LABEL_DELETED As Integer = 7600
  Protected Const AUDIT_LABEL_ORDER As Integer = 5778
  Protected Const AUDIT_LABEL_LASTUPDATE As Integer = 7660
  Protected Const AUDIT_LABEL_BACKGROUNDIMAGE As Integer = 7610
  Protected Const AUDIT_LABEL_ICONIMAGE As Integer = 7611
  Protected Const AUDIT_LABEL_PARENTID As Integer = 7817
  Protected Const AUDIT_LABEL_SECTIONSCHEMAID As Integer = 7596
  Protected Const AUDIT_LABEL_TEMPLATE As Integer = 8137
  Protected Const AUDIT_LABEL_LAYOUT As Integer = 8138
  Protected Const AUDIT_LABEL_LAYOUT_ITEM As Integer = 8139

  Private Const SPANISH_LANG As Integer = 1
  Private Const SECTION_SCHEMA_ID = 6

  Private _language = WSI.Common.GeneralParam.Value("WigosGUI", "Language")

#End Region

#Region " Enums "

  Public Enum ENUM_TYPE
    SLIDE
    BLOCK
  End Enum

  Public Enum ENUM_REFERENCE
    SLIDE
    SLIDE_DETAIL
    BLOCK
    BLOCK_DETAIL
  End Enum

  Public Enum ENUM_IMAGE_REFERENCE
    SECTION_SCHEMA_SLIDE
    SECTION_SCHEMA_SLIDE_DETAIL
    SECTION_SCHEMA_BLOCK
    SECTION_SCHEMA_BLOCK_DETAIL
  End Enum

  Public Enum ENUM_IMAGE_LAYOUT_REFERENCE
    HOME
  End Enum

#End Region

#Region " Structures "
  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_SECTION_SCHEMA
    Public sectionSchema_id As Long
    Public parentId As Nullable(Of Long)
    Public name As String
    Public title As String
    Public subTitle As String
    Public description As String
    Public abstract As String
    Public mainTitle As String
    Public mainSubtitle As String
    Public headLine As String
    Public button As String
    Public active As Boolean
    Public deleted As Boolean
    Public order As Nullable(Of Integer)
    Public lastUpdate As Date
    Public backgroundImageId As Long
    Public backgroundImage As Image
    Public iconImageId As Long
    Public iconImage As Image
    Public type As Integer

    Public layoutId As Integer
    Public templateId As Integer
    Public layoutItemId As Integer

    Public parentLayoutId As Integer

    Sub New()
      sectionSchema_id = 0
      name = String.Empty
      title = String.Empty
      subTitle = String.Empty
      description = String.Empty
      abstract = String.Empty
      mainTitle = String.Empty
      mainSubtitle = String.Empty
      headLine = String.Empty
      button = String.Empty
      active = False
      deleted = False
      name = String.Empty
      order = 0
      lastUpdate = Date.Now
      parentId = 0


    End Sub
  End Class
#End Region

#Region "Class"

  Public Class ParentData

    Private layout_Id As Integer
    Public Property LayoutId() As Integer
      Get
        Return layout_Id
      End Get
      Set(ByVal value As Integer)
        layout_Id = value
      End Set
    End Property

    Private layout_Code As String
    Public Property LayoutCode() As String
      Get
        Return layout_Code
      End Get
      Set(ByVal value As String)
        layout_Code = value
      End Set
    End Property

    Private template_id As Integer
    Public Property TemplateId() As Integer
      Get
        Return template_id
      End Get
      Set(ByVal value As Integer)
        template_id = value
      End Set
    End Property

    Private template_code As String
    Public Property TemplateCode() As String
      Get
        Return template_code
      End Get
      Set(ByVal value As String)
        template_code = value
      End Set
    End Property

    Private title_mandatory As Boolean
    Public Property TitleMandatory() As Boolean
      Get
        Return title_mandatory
      End Get
      Set(ByVal value As Boolean)
        title_mandatory = value
      End Set
    End Property

    Private icon_mandatory As Boolean
    Public Property IconMandatory() As Boolean
      Get
        Return icon_mandatory
      End Get
      Set(ByVal value As Boolean)
        icon_mandatory = value
      End Set
    End Property

    Private abstract_mandatory As Boolean
    Public Property AbstractMandatory() As Boolean
      Get
        Return abstract_mandatory
      End Get
      Set(ByVal value As Boolean)
        abstract_mandatory = value
      End Set
    End Property

  End Class

#End Region


#Region " Members "
  Protected m_section_schema As New TYPE_SECTION_SCHEMA

#End Region

#Region " Properties "

  Private m_template_codes As Dictionary(Of Integer, String)
  Public Property TemplateCodes() As Dictionary(Of Integer, String)
    Get
      Return m_template_codes
    End Get
    Set(ByVal value As Dictionary(Of Integer, String))
      m_template_codes = value
    End Set
  End Property


  Private m_layout_codes As Dictionary(Of Integer, String)
  Public Property LayoutCodes() As Dictionary(Of Integer, String)
    Get
      Return m_layout_codes
    End Get
    Set(ByVal value As Dictionary(Of Integer, String))
      m_layout_codes = value
    End Set
  End Property


  Public Property SectionSchemaId() As Long
    Get
      Return m_section_schema.sectionSchema_id
    End Get
    Set(ByVal value As Long)
      m_section_schema.sectionSchema_id = value
    End Set
  End Property
  Public Property ParentId() As Long
    Get
      Return m_section_schema.parentId
    End Get
    Set(ByVal value As Long)
      m_section_schema.parentId = value
    End Set
  End Property
  Public Property Name() As String
    Get
      Return m_section_schema.name
    End Get
    Set(ByVal value As String)
      m_section_schema.name = value
    End Set
  End Property

  Public Property Title() As String
    Get
      Return m_section_schema.title
    End Get
    Set(ByVal value As String)
      m_section_schema.title = value
    End Set
  End Property


  Public Property SubTitle() As String
    Get
      Return m_section_schema.subTitle
    End Get
    Set(ByVal value As String)
      m_section_schema.subTitle = value
    End Set
  End Property

  Public Property Description() As String
    Get
      Return m_section_schema.description
    End Get
    Set(ByVal value As String)
      m_section_schema.description = value
    End Set
  End Property

  Public Property Abstract() As String
    Get
      Return m_section_schema.abstract
    End Get
    Set(ByVal value As String)
      m_section_schema.abstract = value
    End Set
  End Property

  Public Property MainTitle() As String
    Get
      Return m_section_schema.mainTitle
    End Get
    Set(ByVal value As String)
      m_section_schema.mainTitle = value
    End Set
  End Property

  Public Property MainSubTitle() As String
    Get
      Return m_section_schema.mainSubtitle
    End Get
    Set(ByVal value As String)
      m_section_schema.mainSubtitle = value
    End Set
  End Property

  Public Property HeadLine() As String
    Get
      Return m_section_schema.headLine
    End Get
    Set(ByVal value As String)
      m_section_schema.headLine = value
    End Set
  End Property

  Public Property Button() As String
    Get
      Return m_section_schema.button
    End Get
    Set(ByVal value As String)
      m_section_schema.button = value
    End Set
  End Property

  Public Property Active() As Boolean
    Get
      Return m_section_schema.active
    End Get
    Set(ByVal value As Boolean)
      m_section_schema.active = value
    End Set
  End Property

  Public Property Deleted() As Boolean
    Get
      Return m_section_schema.deleted
    End Get
    Set(ByVal value As Boolean)
      m_section_schema.deleted = value
    End Set
  End Property
  Public Property Order() As Nullable(Of Integer)
    Get
      Return m_section_schema.order
    End Get
    Set(ByVal value As Nullable(Of Integer))
      m_section_schema.order = value
    End Set
  End Property
  Public Property LastUpdate() As Date
    Get
      Return m_section_schema.lastUpdate
    End Get
    Set(ByVal value As Date)
      m_section_schema.lastUpdate = value
    End Set
  End Property

  Public Property BackGroundImageId() As Long
    Get
      Return m_section_schema.backgroundImageId
    End Get
    Set(ByVal value As Long)
      m_section_schema.backgroundImageId = value
    End Set
  End Property

  Public Property IconImageId() As Long
    Get
      Return m_section_schema.iconImageId
    End Get
    Set(ByVal value As Long)
      m_section_schema.iconImageId = value
    End Set
  End Property

  Public Property BackGroundImage() As Image
    Get
      Return m_section_schema.backgroundImage
    End Get
    Set(ByVal value As Image)
      m_section_schema.backgroundImage = value
    End Set
  End Property

  Public Property IconImage() As Image
    Get
      Return m_section_schema.iconImage
    End Get
    Set(ByVal value As Image)
      m_section_schema.iconImage = value
    End Set
  End Property

  Public Property Type() As Integer
    Get
      Return m_section_schema.type
    End Get
    Set(ByVal value As Integer)
      m_section_schema.type = value
    End Set
  End Property

  Public Property LayoutId() As Integer
    Get
      Return m_section_schema.layoutId
    End Get
    Set(ByVal value As Integer)
      m_section_schema.layoutId = value
    End Set
  End Property

  Public Property TemplateId() As Integer
    Get
      Return m_section_schema.templateId
    End Get
    Set(ByVal value As Integer)
      m_section_schema.templateId = value
    End Set
  End Property

  Public Property LayoutItemId() As Integer
    Get
      Return m_section_schema.layoutItemId
    End Get
    Set(ByVal value As Integer)
      m_section_schema.layoutItemId = value
    End Set
  End Property

  Public Property ParentLayoutId() As Integer
    Get
      Return m_section_schema.parentLayoutId
    End Get
    Set(ByVal value As Integer)
      m_section_schema.parentLayoutId = value
    End Set
  End Property

  'Private parent_Data As ParentData
  'Public Property Parent() As ParentData
  '  Get
  '    Return parent_Data
  '  End Get
  '  Set(ByVal value As ParentData)
  '    parent_Data = value
  '  End Set
  'End Property

#End Region

#Region " Override Functions "

  '----------------------------------------------------------------------------
  ' PURPOSE : Reads from the database into the given object
  '
  ' PARAMS :
  '     - INPUT :
  '         - ObjectId: An object, it must be 'casted' to the desired KEY.
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '
  ' NOTES :

  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As Integer

    Me.SectionSchemaId = ObjectId

    ' Read ads data
    _rc = ReadSectionSchema(m_section_schema, SqlCtx)

    Select Case _rc

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE : Updates the given object to the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES :

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As Integer

    ' Update section_schema data
    _rc = UpdateSectionSchema(m_section_schema, SqlCtx)

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
        Return ENUM_STATUS.STATUS_DUPLICATE_KEY

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select
  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE : Inserts the given object to the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES :

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _rc As Integer

    _rc = InsertSectionSchema(m_section_schema, SqlCtx)

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
        Return ENUM_STATUS.STATUS_DUPLICATE_KEY

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select
  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE : Deletes the given object from the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DEPENDENCIES
  '
  ' NOTES :

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _rc As Integer

    _rc = DeleteSectionSchema(m_section_schema, SqlCtx)

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DEPENDENCIES
        Return ENUM_STATUS.STATUS_DEPENDENCIES

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select
  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE : Returns the related auditor data for the current object.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - The newly created audit object
  '
  ' NOTES :

  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim _auditor_data As CLASS_AUDITOR_DATA

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_PLAYER_PROMOTIONS)

    Call _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(AUDIT_LABEL_NAME), Me.Name)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(AUDIT_LABEL_TITLE), Me.Title)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(AUDIT_LABEL_SUBTITLE), Me.SubTitle)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(AUDIT_LABEL_MAINTITLE), Me.MainTitle)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(AUDIT_LABEL_MAINSUBTITLE), Me.MainSubTitle)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(AUDIT_LABEL_ABSTRACT), Me.Abstract)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(AUDIT_LABEL_DESCRIPTION), Me.Description)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(AUDIT_LABEL_HEADLINE), Me.HeadLine)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(AUDIT_LABEL_BUTTON), Me.Button)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(AUDIT_LABEL_ACTIVE), Me.Active)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(AUDIT_LABEL_DELETED), Me.Deleted)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(AUDIT_LABEL_BACKGROUNDIMAGE), GetInfoCRC(Me.BackGroundImage))
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(AUDIT_LABEL_ICONIMAGE), GetInfoCRC(Me.IconImage))
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(AUDIT_LABEL_ORDER), Me.Order)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(AUDIT_LABEL_PARENTID), Me.ParentId)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(AUDIT_LABEL_SECTIONSCHEMAID), Me.SectionSchemaId)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(AUDIT_LABEL_TEMPLATE), Me.TemplateId)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(AUDIT_LABEL_LAYOUT), Me.LayoutId)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(AUDIT_LABEL_LAYOUT_ITEM), Me.LayoutItemId)

    Return _auditor_data

  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE : Returns a duplicate of the given object.
  '
  ' PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - The newly created object
  '
  ' NOTES :

  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim _copy As CLS_SECTION_SCHEMA
    _copy = New CLS_SECTION_SCHEMA

    _copy.m_section_schema.abstract = Me.m_section_schema.abstract
    _copy.m_section_schema.active = Me.m_section_schema.active
    _copy.m_section_schema.backgroundImage = Me.m_section_schema.backgroundImage
    _copy.m_section_schema.backgroundImageId = Me.m_section_schema.backgroundImageId
    _copy.m_section_schema.button = Me.m_section_schema.button
    _copy.m_section_schema.deleted = Me.m_section_schema.deleted
    _copy.m_section_schema.description = Me.m_section_schema.description
    _copy.m_section_schema.headLine = Me.m_section_schema.headLine
    _copy.m_section_schema.iconImage = Me.m_section_schema.iconImage
    _copy.m_section_schema.iconImageId = Me.m_section_schema.iconImageId
    _copy.m_section_schema.mainSubtitle = Me.m_section_schema.mainSubtitle
    _copy.m_section_schema.mainTitle = Me.m_section_schema.mainTitle
    _copy.m_section_schema.name = Me.m_section_schema.name
    _copy.m_section_schema.parentId = Me.m_section_schema.parentId
    _copy.m_section_schema.subTitle = Me.m_section_schema.subTitle
    _copy.m_section_schema.title = Me.m_section_schema.title
    _copy.m_section_schema.order = Me.m_section_schema.order
    _copy.m_section_schema.type = Me.m_section_schema.type
    _copy.m_section_schema.sectionSchema_id = Me.m_section_schema.sectionSchema_id
    _copy.m_section_schema.templateId = Me.m_section_schema.templateId
    _copy.m_section_schema.layoutItemId = Me.m_section_schema.layoutItemId
    _copy.m_section_schema.layoutId = Me.m_section_schema.layoutId

    Return _copy
  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE : Returns object info as String ( Mainly for easy debug ) 
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT : 
  '
  ' RETURNS :
  '     - String with instance info
  '
  ' NOTES :

  Public Overrides Function ToString() As String

    Dim _str_bld As System.Text.StringBuilder
    _str_bld = New System.Text.StringBuilder()

    _str_bld.AppendLine("SectionSchemaId =  " & Me.SectionSchemaId.ToString())
    _str_bld.AppendLine("Name =  " & Me.Title)
    _str_bld.AppendLine("Ttle = " & Me.Title)

    Return _str_bld.ToString()
  End Function

#End Region

#Region " Private functions"
  '----------------------------------------------------------------------------
  ' PURPOSE : Return the image type extension of the image 
  '
  ' PARAMS :
  '     - INPUT :
  '         None
  '
  '     - OUTPUT :
  '         String
  '
  ' RETURNS :
  '     - string
  '
  ' NOTES :
  Private Function GetImageType(img As Image) As String
    Dim _result As String

    _result = ""
    If (img.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Jpeg)) Then
      _result = ".jpg"
    End If
    If (img.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Bmp)) Then
      _result = ".bmp"
    End If
    If (img.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Png)) Then
      _result = ".png"
    End If
    If (img.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Gif)) Then
      _result = ".gif"
    End If

    Return _result
  End Function
  '----------------------------------------------------------------------------
  ' PURPOSE : Return the properties of the class in JSON format
  '
  ' PARAMS :
  '     - INPUT :
  '         None
  '
  '     - OUTPUT :
  '         None
  '
  ' RETURNS :
  '     - JSON string
  '
  ' NOTES :

  Private Function ToJSON() As String
    Dim _jObj As New JObject
    _jObj.Add("languageId", 1)
    _jObj.Add("sectionSchemaId", SectionSchemaId)
    _jObj.Add("parentId", ParentId)
    _jObj.Add("name", Name)
    _jObj.Add("mainTitle", MainTitle)
    _jObj.Add("mainSubTitle", MainSubTitle)
    _jObj.Add("title", Title)
    _jObj.Add("subTitle", SubTitle)
    _jObj.Add("abstract", Abstract)
    _jObj.Add("description", Description)
    _jObj.Add("headlineText", HeadLine)
    _jObj.Add("buttonText", Button)
    _jObj.Add("active", Active)
    _jObj.Add("deleted", Deleted)
    _jObj.Add("order", Order)
    _jObj.Add("lastUpdate", WSI.Common.WGDB.Now)
    _jObj.Add("type", Type)
    _jObj.Add("templateId", TemplateId)
    _jObj.Add("layoutId", LayoutId)
    _jObj.Add("layoutItemId", LayoutItemId)

    If Not BackGroundImageId > 0 Then
      Dim imgBackgoundData As Byte()
      If Not IsNothing(BackGroundImage) Then
        Dim img As New ImageConverter()
        Dim _url As String
        imgBackgoundData = img.ConvertTo(BackGroundImage, GetType(Byte()))

        Dim jObjBackground As New JObject
        jObjBackground.Add("Name", Name)
        jObjBackground.Add("fileName", Name + GetImageType(BackGroundImage))
        jObjBackground.Add("ContentType", "image/jpeg")
        jObjBackground.Add("Data", imgBackgoundData)

        _url = WSI.Common.GeneralParam.GetString("WinUP", "Backend.Url") + "/image/SaveImage"
        Dim _api = New CLS_API(_url)

        Dim response As String = _api.postData(Encoding.UTF8.GetBytes(jObjBackground.ToString()))
        Dim jresponse As JObject = JObject.Parse(response)
        _jObj.Add("backgroundImageId", jresponse.Item("imageId"))

      End If
    Else
      _jObj.Add("backgroundImageId", BackGroundImageId)
    End If
    '   _jObj.Add("backgroundImage", BackGroundImage)
    '  _jObj.Add("iconImage", IconImage)
    If Not IconImageId > 0 Then
      Dim imgIconData As Byte()
      If Not IsNothing(IconImage) Then
        Dim img As New ImageConverter()
        Dim _url As String
        imgIconData = img.ConvertTo(IconImage, GetType(Byte()))

        Dim jObjIcon As New JObject
        jObjIcon.Add("Name", Name)
        jObjIcon.Add("fileName", Name + GetImageType(IconImage))
        jObjIcon.Add("ContentType", "image/jpeg")
        jObjIcon.Add("Data", imgIconData)

        _url = WSI.Common.GeneralParam.GetString("WinUP", "Backend.Url") + "/image/SaveImage"
        Dim _api = New CLS_API(_url)

        Dim response As String = _api.postData(Encoding.UTF8.GetBytes(jObjIcon.ToString()))
        Dim jresponse As JObject = JObject.Parse(response)
        'jresponse = jresponse.Item("data")
        Dim rta As String = jresponse.Item("imageId")
        _jObj.Add("iconImageId", rta)
      End If
    Else
      _jObj.Add("iconImageId", IconImageId)

    End If
    Return _jObj.ToString()
  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE : Reads a advertisement object from DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - TYPE_ADS_STEPS
  '         - Context
  '
  '     - OUTPUT :
  '         - Item
  '
  ' RETURNS :
  '     - CONFIGURATION_OK
  '     - CONFIGURATION_ERROR_DB
  '
  ' NOTES :

  Private Function ReadSectionSchema(ByVal Item As TYPE_SECTION_SCHEMA, _
                           ByVal Context As Integer) As Integer

    Dim _id As Integer
    'Dim _datatable As DataTable
    Dim _response As String
    Dim _arraySchema As JObject
    Dim _sql_transaction As System.Data.SqlClient.SqlTransaction
    Dim _rc As Integer
    Dim _do_commit As Boolean
    Dim _url As String
    Dim _api As CLS_API

    _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    _sql_transaction = Nothing
    _id = Item.sectionSchema_id
    _do_commit = False

    Try

      If Not GUI_BeginSQLTransaction(_sql_transaction) Then

        _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        Return _rc
      End If

      _url = WSI.Common.GeneralParam.GetString("WinUP", "Backend.Url") + "/sectionsschema/getbylanguage/" + _id.ToString() + "/1"

      _api = New CLS_API(_url)
      _response = _api.getData()
      _arraySchema = JObject.Parse(_api.sanitizeData(_response))
      If _arraySchema.Item("backgroundImageId").ToString() <> "" Then
        Dim _imageTmpResponse As JObject
        _url = WSI.Common.GeneralParam.GetString("WinUP", "Backend.Url") + "/image/getbyid/" + _arraySchema.Item("backgroundImageId").ToString()
        _api = New CLS_API(_url)
        _imageTmpResponse = JObject.Parse(_api.sanitizeData(_api.getData()))
        If Not _imageTmpResponse.Item("imageData") = "" Then
          Dim imgData As Byte() = _imageTmpResponse.Item("imageData")
          Using ms As New MemoryStream(imgData)
            Item.backgroundImage = System.Drawing.Image.FromStream(ms)
          End Using
        Else
          Item.backgroundImage = Nothing
        End If

      End If
      If _arraySchema.Item("iconImageId").ToString() <> "" Then
        Dim _imageTmpResponse As JObject
        _url = WSI.Common.GeneralParam.GetString("WinUP", "Backend.Url") + "/image/getbyid/" + _arraySchema.Item("iconImageId").ToString()
        _api = New CLS_API(_url)
        _imageTmpResponse = JObject.Parse(_api.sanitizeData(_api.getData()))
        If Not _imageTmpResponse.Item("imageData") = "" Then
          Dim imgData As Byte() = _imageTmpResponse.Item("imageData")
          Using ms As New MemoryStream(imgData)
            Item.iconImage = System.Drawing.Image.FromStream(ms)
          End Using
        Else
          Item.backgroundImage = Nothing
        End If
      End If
      '_datatable = JsonConvert.DeserializeObject(Of DataTable)(_api.responseToArray(_api.sanitizeData(_api.getData())))



      If Not IsNothing(_arraySchema.Count = 0) Then

        With Item
          .sectionSchema_id = _arraySchema.Item("sectionSchemaId")
          If _arraySchema.Item("parentId").ToString() <> "" Then
            .parentId = _arraySchema.Item("parentId")
          End If
          If _arraySchema.Item("name").ToString() <> "" Then
            .name = _arraySchema.Item("name")
          End If
          If _arraySchema.Item("mainTitle").ToString() <> "" Then
            .mainTitle = _arraySchema.Item("mainTitle")
          End If
          If _arraySchema.Item("mainSubTitle").ToString() <> "" Then
            .mainSubtitle = _arraySchema.Item("mainSubTitle")
          End If
          If _arraySchema.Item("title").ToString() <> "" Then
            .title = _arraySchema.Item("title")
          End If
          If _arraySchema.Item("subTitle").ToString() <> "" Then
            .subTitle = _arraySchema.Item("subTitle")
          End If
          If _arraySchema.Item("abstract").ToString() <> "" Then
            .abstract = _arraySchema.Item("abstract")
          End If
          If _arraySchema.Item("description").ToString() <> "" Then
            .description = _arraySchema.Item("description")
          End If
          If _arraySchema.Item("headlineText").ToString() <> "" Then
            .headLine = _arraySchema.Item("headlineText")
          End If
          If _arraySchema.Item("buttonText").ToString() <> "" Then
            .button = _arraySchema.Item("buttonText")
          End If
          If _arraySchema.Item("active").ToString() <> "" Then
            .active = _arraySchema.Item("active")
          End If
          If _arraySchema.Item("deleted").ToString() <> "" Then
            .deleted = _arraySchema.Item("deleted")
          End If
          If _arraySchema.Item("order").ToString() <> "" Then
            .order = _arraySchema.Item("order")
          End If
          If _arraySchema.Item("type").ToString() <> "" Then
            .type = _arraySchema.Item("type")
          End If

          .lastUpdate = Date.Now 'siempre actualiza fecha

          If _arraySchema.Item("backgroundImageId").ToString() <> "" Then
            .backgroundImageId = _arraySchema.Item("backgroundImageId")
          End If

          If _arraySchema.Item("iconImageId").ToString() <> "" Then
            .iconImageId = _arraySchema.Item("iconImageId")
          End If

          .templateId = _arraySchema.Item("templateId")
          .layoutId = _arraySchema.Item("layoutId")

          If _arraySchema.Item("layoutItemId").ToString() <> "" Then
            .layoutItemId = _arraySchema.Item("layoutItemId")
          End If


        End With

        _do_commit = True
        _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return _rc

      Else

        _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        Return _rc

      End If
    Catch ex As Exception
      _do_commit = False
    Finally

      GUI_EndSQLTransaction(_sql_transaction, _do_commit)

    End Try

    Return _rc

  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE : Deletes a advertisement object from DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - TYPE_ADS_STEPS
  '         - Context
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - CONFIGURATION_OK
  '     - CONFIGURATION_ERROR_DB
  '
  ' NOTES :

  Private Function DeleteSectionSchema(ByVal Item As TYPE_SECTION_SCHEMA, _
                             ByVal Context As Integer) As Integer

    Dim _code As String
    Dim _sql_transaction As System.Data.SqlClient.SqlTransaction
    Dim _row_affected As Integer
    Dim _do_commit As Boolean
    Dim _url As String
    Dim _api As CLS_API
    Dim response As String

    _sql_transaction = Nothing
    _row_affected = 0
    _code = Item.sectionSchema_id
    _do_commit = False

    Try

      If Not GUI_BeginSQLTransaction(_sql_transaction) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      _url = WSI.Common.GeneralParam.GetString("WinUP", "Backend.Url") + "/sectionsschema/" + Item.sectionSchema_id.ToString()
      _api = New CLS_API(_url)

      response = _api.deleteData()

      _row_affected = 1
      If _row_affected <> 1 Then
        _do_commit = False
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      Else
        _do_commit = True
      End If



    Catch ex As Exception

      _do_commit = False

    Finally

      GUI_EndSQLTransaction(_sql_transaction, _do_commit)
    End Try

    If _do_commit Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
    Else
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

  End Function ' DeleteAds

  '----------------------------------------------------------------------------
  ' PURPOSE : Adds a advertisement object into DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - TYPE_ADS_STEPS
  '         - Context
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - CONFIGURATION_OK
  '     - CONFIGURATION_ERROR_DB
  '
  ' NOTES :

  Private Function InsertSectionSchema(ByVal Item As TYPE_SECTION_SCHEMA, _
                             ByVal Context As Integer) As Integer

    Dim _sql_transaction As System.Data.SqlClient.SqlTransaction
    Dim _row_affected As Integer
    Dim _do_commit As Boolean
    Dim _rc As Integer
    Dim _url As String
    Dim _api As CLS_API
    Dim response As String

    _sql_transaction = Nothing
    _row_affected = 0
    _do_commit = False
    _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

    Try

      If Not GUI_BeginSQLTransaction(_sql_transaction) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      _url = WSI.Common.GeneralParam.GetString("WinUP", "Backend.Url") + "/sectionsschema"
      _api = New CLS_API(_url)
      response = _api.postData(Encoding.UTF8.GetBytes(Me.ToJSON()))

      _row_affected = 1

      If _row_affected <> 1 Then
        _do_commit = False
        _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        Return _rc
      Else
        _do_commit = True
        _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_OK
      End If

    Catch ex As Exception
      _do_commit = False
      _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

    Finally

      GUI_EndSQLTransaction(_sql_transaction, _do_commit)

    End Try

    Return _rc

  End Function ' InsertArea

  '----------------------------------------------------------------------------
  ' PURPOSE : Updates the given advertisement object into the DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - TYPE_ADS_STEPS
  '         - Context
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - CONFIGURATION_OK
  '     - CONFIGURATION_ERROR_DB
  '
  ' NOTES :

  Private Function UpdateSectionSchema(ByVal Item As TYPE_SECTION_SCHEMA, _
                             ByVal Context As Integer) As Integer

    Dim _sql_transaction As System.Data.SqlClient.SqlTransaction
    Dim _row_affected As Integer
    Dim _do_commit As Boolean
    Dim _rc As Integer
    Dim _url As String
    Dim _api As CLS_API
    Dim response As String

    _sql_transaction = Nothing
    _row_affected = 0
    _do_commit = False

    Try

      If Not GUI_BeginSQLTransaction(_sql_transaction) Then
        _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

        Return _rc
      End If

      _url = WSI.Common.GeneralParam.GetString("WinUP", "Backend.Url") + "/sectionsschema/" + Item.sectionSchema_id.ToString()
      _api = New CLS_API(_url)

      response = _api.putData(Encoding.UTF8.GetBytes(Me.ToJSON()))

      '_datatable = JsonConvert.DeserializeObject(Of DataTable)(_api.responseToArray(_api.sanitizeData(_api.getData())))

      _row_affected = 1

      If _row_affected <> 1 Then
        _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        Return _rc
      Else
        _do_commit = True
        _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_OK
      End If

    Catch ex As Exception
      _do_commit = False

    Finally

      GUI_EndSQLTransaction(_sql_transaction, _do_commit)

    End Try

    Return _rc

  End Function ' UpdateArea

#End Region


#Region "Public Functions"

  Public Function DeleteSection(ByVal SectionId As Integer)

    Dim _rc As Integer
    Dim _section As TYPE_SECTION_SCHEMA

    _section = New TYPE_SECTION_SCHEMA
    _section.sectionSchema_id = SectionId

    _rc = DeleteSectionSchema(_section, Nothing)

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return True

      Case Else
        Return False

    End Select

  End Function

  Public Function GetSections(ByVal ShowFeature As Boolean) As Dictionary(Of Integer, String)

    Dim _list As New Dictionary(Of Integer, String)
    Dim _sql_trx As SqlTransaction
    Dim _sql_str_select As String
    Dim _command As SqlCommand
    Dim _reader As SqlDataReader

    Using _db_trx As DB_TRX = New DB_TRX()

      _sql_trx = _db_trx.SqlTransaction

      _sql_str_select = " SELECT SS_SECTION_SCHEMA_ID, ML.lo_value AS  SS_NAME FROM MAPP_SECTION_SCHEMA SS "
      _sql_str_select += " LEFT JOIN mapp_section_template ST on ST.st_id = SS.ss_template_id "
      _sql_str_select += " LEFT JOIN mapp_localization ML on ML.lo_entity_item_id = SS.ss_section_schema_id and ML.lo_resource_id='MainTitle'"
      _sql_str_select += " WHERE ST.st_name = 'SectionsListTemplate'"
      If Not ShowFeature Then
        _sql_str_select += " AND SS.ss_type=1"
      End If


      'donde template = 'SectionsListTemplate'

      _command = New SqlCommand( _
          _sql_str_select, _
          _sql_trx.Connection, _sql_trx)

      _reader = _command.ExecuteReader()

      If _reader.HasRows Then
        Do While _reader.Read()
          _list.Add(_reader.GetInt64(0), _reader.GetString(1))
        Loop
      End If

      _reader.Close()

    End Using

    Return _list

  End Function

  Public Function GetTemplates() As Dictionary(Of Integer, String)

    Dim _list As New Dictionary(Of Integer, String)
    Dim _sql_trx As SqlTransaction
    Dim _sql_str_select As String
    Dim _command As SqlCommand
    Dim _reader As SqlDataReader

    m_template_codes = New Dictionary(Of Integer, String)

    Using _db_trx As DB_TRX = New DB_TRX()
      _sql_trx = _db_trx.SqlTransaction

      _sql_str_select = "SELECT st_id, st_name_nls_id, st_name FROM mapp_section_template where st_isitem =0"

      _command = New SqlCommand( _
          _sql_str_select, _
          _sql_trx.Connection, _sql_trx)

      _reader = _command.ExecuteReader()

      _list.Add(0, "---")

      If _reader.HasRows Then
        Do While _reader.Read()
          m_template_codes.Add(_reader.GetInt64(0), _reader.GetString(2))
          _list.Add(_reader.GetInt64(0), GLB_NLS_GUI_PLAYER_TRACKING.GetString(_reader.GetInt32(1)))
        Loop
      End If

      _reader.Close()

    End Using

    Return _list

  End Function

  Public Function GetLayouts(ByVal TemplateId As Integer, Optional ByVal IsItems As Boolean = False) As Dictionary(Of Integer, String)

    Dim _list As New Dictionary(Of Integer, String)
    Dim _sql_trx As SqlTransaction
    Dim _sql_str_select As String
    Dim _command As SqlCommand
    Dim _reader As SqlDataReader

    If Not IsItems Then
      m_layout_codes = New Dictionary(Of Integer, String)
    End If

    Using _db_trx As DB_TRX = New DB_TRX()
      _sql_trx = _db_trx.SqlTransaction

      If IsItems Then
        _sql_str_select = "SELECT stli_id, stli_name_nls_id FROM mapp_section_template_layout_item WHERE stli_section_template_id = (SELECT st_id FROM mapp_section_template WHERE st_isitem=1) "
      Else
        _sql_str_select = "SELECT stl_id,  stl_name_nls_id, stl_name FROM mapp_section_template_layout WHERE stl_section_template_id = " + TemplateId.ToString()
      End If

      _command = New SqlCommand( _
          _sql_str_select, _
          _sql_trx.Connection, _sql_trx)

      _reader = _command.ExecuteReader()

      If IsItems Then
        _list.Add(0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8241))
      End If

      If _reader.HasRows Then

        Do While _reader.Read()

          If Not IsItems Then
            m_layout_codes.Add(_reader.GetInt64(0), _reader.GetString(2))
          End If

          _list.Add(_reader.GetInt64(0), GLB_NLS_GUI_PLAYER_TRACKING.GetString(_reader.GetInt32(1)))
        Loop
      End If

      _reader.Close()

    End Using

    Return _list

  End Function

  Public Shared Function SetListImageLayout() As Dictionary(Of Integer, String)
    Dim _list As New Dictionary(Of Integer, String)
    Dim _sql_trx As SqlTransaction
    Dim _sql_str_select As String
    Dim _reader As SqlDataReader
    Dim _command As SqlCommand

    Try

      Using _db_trx As DB_TRX = New DB_TRX()
        _sql_trx = _db_trx.SqlTransaction

        _sql_str_select = " SELECT Layout.stl_id AS LayoutId, Template.st_name + '_' + Layout.stl_name As Name "
        _sql_str_select += " FROM mapp_section_template AS Template, "
        _sql_str_select += " mapp_section_template_layout AS Layout "
        _sql_str_select += " WHERE Template.st_id = Layout.stl_section_template_id "

        _command = New SqlCommand( _
            _sql_str_select, _
            _sql_trx.Connection, _sql_trx)

        _reader = _command.ExecuteReader()

        If _reader.HasRows Then
          Do While _reader.Read()
            ' (0) LayoutId, (1) Name
            _list.Add(_reader.GetInt64(0), _reader.GetString(1))
          Loop
        End If

        _reader.Close()

      End Using

    Catch ex As Exception

    End Try

    Return _list

  End Function

  Public Shared Function SetListImageLayoutItem() As Dictionary(Of Integer, String)
    Dim _list As New Dictionary(Of Integer, String)
    Dim _sql_trx As SqlTransaction
    Dim _sql_str_select As String
    Dim _reader As SqlDataReader
    Dim _command As SqlCommand

    Try

      Using _db_trx As DB_TRX = New DB_TRX()
        _sql_trx = _db_trx.SqlTransaction

        _sql_str_select = " SELECT Layout.stli_id AS LayoutId, Template.st_name + '_' + Layout.stli_name AS Name "
        _sql_str_select += " FROM mapp_section_template AS Template, "
        _sql_str_select += " mapp_section_template_layout_item AS Layout "
        _sql_str_select += " WHERE Template.st_id = Layout.stli_section_template_id "

        _command = New SqlCommand( _
            _sql_str_select, _
            _sql_trx.Connection, _sql_trx)

        _reader = _command.ExecuteReader()

        If _reader.HasRows Then
          Do While _reader.Read()
            ' (0) LayoutId, (1) Name
            _list.Add(_reader.GetInt64(0), _reader.GetString(1))
          Loop
        End If

        _reader.Close()

      End Using

    Catch ex As Exception

    End Try

    Return _list

  End Function

  Public Function GetImageResource(ByVal list As Dictionary(Of Integer, String), ByVal detailSufix As String, ByVal LayoutId As Integer) As Image

    Dim image_name As String
    image_name = ""

    If list.ContainsKey(LayoutId) Then

      Dim pair As KeyValuePair(Of Integer, String)

      For Each pair In list
        If pair.Key.Equals(LayoutId) Then
          image_name = pair.Value
          Exit For
        End If
      Next

      If detailSufix = String.Empty Then
        Return CLASS_GUI_RESOURCES.GetImage(image_name + "_" + _language.ToString)
      Else
        Return CLASS_GUI_RESOURCES.GetImage(image_name + detailSufix + _language.ToString)
      End If
    Else
      Return Nothing
    End If

  End Function

  Public Function GetImageLayout(ByVal list As Dictionary(Of Integer, String), ByVal detailSufix As String, ByVal LayoutId As Integer) As Image

    Return GetImageResource(list, detailSufix, LayoutId)

  End Function


  Public Function GetImageLayoutItem(ByVal list As Dictionary(Of Integer, String), ByVal detailSufix As String, ByVal LayoutId As Integer) As Image

    Return GetImageResource(list, detailSufix, LayoutId)

  End Function

  Public Function GetParentData(ByVal ParentId As Integer) As ParentData

    Dim _sql_trx As SqlTransaction
    Dim _sql_str_select As String
    Dim _command As SqlCommand
    Dim _data As ParentData

    _data = Nothing

    Try

      Using _db_trx As DB_TRX = New DB_TRX()
        _sql_trx = _db_trx.SqlTransaction

        _sql_str_select = " SELECT ss_template_id AS TemplateId "
        _sql_str_select += " ,ss_layout_id as LayoutId "
        _sql_str_select += "  , ST.st_name as TemplateCode "
        _sql_str_select += "  ,SL.stl_name as LayoutCode "
        _sql_str_select += "  FROM mapp_section_schema SS "
        _sql_str_select += "  LEFT JOIN mapp_section_template ST on ST.st_id = SS.ss_template_id "
        _sql_str_select += "  LEFT JOIN mapp_section_template_layout SL on SL.stl_id = SS.ss_layout_id "
        _sql_str_select += "  WHERE ss_section_schema_id = " + ParentId.ToString()

        _command = New SqlCommand( _
            _sql_str_select, _
            _sql_trx.Connection, _sql_trx)

        Using _reader As SqlDataReader = _command.ExecuteReader()
          While (_reader.Read())
            _data = New ParentData
            _data.LayoutId = _reader.GetInt64(1)
            _data.TemplateId = _reader.GetInt64(0)
            _data.TemplateCode = _reader.GetString(2)
            _data.LayoutCode = _reader.GetString(3)

            If _data.LayoutCode = "IconTitleDescriptionList" Then
              _data.TitleMandatory = True
              _data.AbstractMandatory = True
              _data.IconMandatory = True
            ElseIf _data.LayoutCode = "IconTitleList" Then
              _data.TitleMandatory = True
              _data.AbstractMandatory = False
              _data.IconMandatory = True
            ElseIf _data.LayoutCode = "Carrousel" Then
              _data.TitleMandatory = True
              _data.AbstractMandatory = False
              _data.IconMandatory = False
            End If
          End While
        End Using

      End Using

    Catch ex As Exception

    End Try

    Return _data

  End Function

  Public Function GetImageParentLayout(ByVal list As Dictionary(Of Integer, String), ByVal LayoutId As Integer) As Image

    Return GetImageResource(list, "", LayoutId)

  End Function

  Public Function GetImageHome() As Image

    Return CLASS_GUI_RESOURCES.GetImage(ENUM_IMAGE_LAYOUT_REFERENCE.HOME.ToString.ToLower + "_" + _language.ToString)

  End Function

  Public Function GetNextOrderByParent(ByVal ParentId As Integer) As Integer
    Dim _sql_trx As SqlTransaction
    Dim _sql_str_select As String
    Dim _command As SqlCommand
    Dim _order As Integer

    _order = 1

    Try

      Using _db_trx As DB_TRX = New DB_TRX()
        _sql_trx = _db_trx.SqlTransaction

        _sql_str_select = "SELECT MAX(ss_order) FROM mapp_section_schema WHERE ss_parent_id = " + ParentId.ToString() + ""

        _command = New SqlCommand( _
            _sql_str_select, _
            _sql_trx.Connection, _sql_trx)

        Dim _value As Object = _command.ExecuteScalar()

        If Not _value Is Nothing Then
          _order = Convert.ToInt32(_value) + 1
        End If

      End Using

    Catch ex As Exception

    End Try

    Return _order

  End Function

  Public Function IsOrderValid(ByVal Order As Integer, ByVal ParentId As Integer, ByVal Id As Integer) As Boolean
    Dim _sql_trx As SqlTransaction
    Dim _sql_str_select As String
    Dim _command As SqlCommand
    Dim _valid As Boolean

    _valid = True

    Try

      Using _db_trx As DB_TRX = New DB_TRX()
        _sql_trx = _db_trx.SqlTransaction


        _sql_str_select = "SELECT COUNT(*) FROM mapp_section_schema WHERE ss_parent_id = " + ParentId.ToString() + " AND ss_order = " + Order.ToString()

        If Id > 0 Then
          _sql_str_select += " AND ss_section_schema_id <> " + Id.ToString()
        End If

        _command = New SqlCommand( _
            _sql_str_select, _
            _sql_trx.Connection, _sql_trx)

        Dim _value As Object = _command.ExecuteScalar()

        If Not _value Is Nothing Then
          If Convert.ToInt32(_value) > 0 Then
            _valid = False
          End If
        End If

      End Using

    Catch ex As Exception

    End Try

    Return _valid

  End Function

  Public Function GetSectionsListByParentId(ByVal ParentId As Integer) As DataTable


    Dim _url As String
    Dim _response, _responseSanitized As String
    Dim _data As DataTable
    Dim _jarray As JArray
    Dim _items() As String = {"backgroundImage", "iconImage", "parent", "childs"}
    _url = WSI.Common.GeneralParam.GetString("WinUP", "Backend.Url") + "/sectionsschema/GetByParentId/" + ParentId.ToString() + "/1"

    Dim _api As New CLS_API(_url)

    _response = _api.getData()
    _responseSanitized = _api.sanitizeData(_response)
    _jarray = _api.removeItems(JArray.Parse(_responseSanitized), _items)
    _data = JsonConvert.DeserializeObject(Of DataTable)(_jarray.ToString())

    Return _data

  End Function ' SeccionSchemaComboFill

#End Region

End Class
