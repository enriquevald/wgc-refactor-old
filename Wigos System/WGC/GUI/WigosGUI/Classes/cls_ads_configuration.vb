'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_ads_configuration.vb
' DESCRIPTION:   
' AUTHOR:        Marcos Piedra Osuna
' CREATION DATE: 22-MAY-2012
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 22-MAY-2012  MPO    Initial version
' 23-MAY-2012  ANG    Fill data from database
' 09-OCT-2012  JMM    Messages setup added
' 18-OCT-2012  JMM    Schedule setup added
' 27-NOV-2012  JMM    Level images & gift split added
' 14-OCT-2016  RAB    PBI 18098: General params: Automatically add the GP to the system
' 02-NOV-2017  DHA    Bug 30536:WIGOS-6246 Missing string in the screen "Configuracion de Promobox"
' -------------------------------------------------------------------

Option Explicit On

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports WSI.Common
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports System.Text

Public Class CLS_ADS_CONFIGURATION
  Inherits CLASS_BASE

#Region " Constants "

  Public Const PROMOBOX_GP_GROUP_KEY = "WigosKiosk"

#End Region ' Constants

#Region " Structures "

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_FUNCTIONALITY
    Implements IEquatable(Of TYPE_FUNCTIONALITY)

    Private Const NLS_FUNCTIONALITY_BASE As Integer = (948 - 1)
    Private Const NLS_FUNCTIONALITY_DESC_BASE As Integer = (998 - 1)

    Public function_id As Integer
    Public group_id As Integer

    Public enabled As Boolean

    Public Shared Function GetName(ByVal FunctionalityId As Integer) As String

      Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(GetNlsId(FunctionalityId))

    End Function

    Public Shared Function GetDescription(ByVal FunctionalityId As Integer) As String

      Return NLS_FUNCTIONALITY_DESC_BASE + FunctionalityId

    End Function

    Public Shared Function GetNlsId(ByVal FunctionalityId As Integer) As Integer

      Return NLS_FUNCTIONALITY_BASE + FunctionalityId

    End Function

    '----------------------------------------------------------------------------
    ' PURPOSE : Check if two objects are the same ( has the same ID field ) 
    '           It's mandatory override this function to .ListIndexOf() works right
    ' PARAMS :
    '     - INPUT : Other object that same type
    '
    '     - OUTPUT :
    '
    ' RETURNS :
    '     - True or False 
    '
    ' NOTES :
    ' It's mandatory implement Equals() to be able to use List.Contains() and List.IndexOf() functions
    Public Overloads Function Equals(ByVal target As TYPE_FUNCTIONALITY) As Boolean Implements IEquatable(Of TYPE_FUNCTIONALITY).Equals

      If target.function_id = Me.function_id Then
        Return True
      Else
        Return False
      End If
    End Function


  End Class

  <StructLayout(LayoutKind.Sequential)> _
  Public Class WKT_Image
    Implements IEquatable(Of WKT_Image)

    Public image_id As Integer
    Private Const NLS_NAMES_BASE As Integer = 847
    Private Const NLS_DESCR_BASE As Integer = 897

    Public resource_id As Long
    Public image_cached As Image
    Public image_has_changes As Boolean = False
    Public image_deleted As Boolean = False


    Public ReadOnly Property Name() As String
      Get
        Return GetImageName(image_id)
      End Get
    End Property

    Private Shared Function GetPointsLevelNameFromImageId(ByVal ImageId As Integer) As String

      Dim _level_name As String

      _level_name = ""

      Select Case ImageId
        Case 2
          _level_name = GetCashierPlayerTrackingData("Level01.Name")

        Case 3
          _level_name = GetCashierPlayerTrackingData("Level02.Name")

        Case 4
          _level_name = GetCashierPlayerTrackingData("Level03.Name")

        Case 5
          _level_name = GetCashierPlayerTrackingData("Level04.Name")

        Case Else
          _level_name = ""

      End Select

      Return _level_name

    End Function

    Public Shared Function GetImageName(ByVal ImageId As Integer) As String

      Select Case ImageId
        Case 2, 3, 4, 5

          'Points Levels images
          Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(GetImageNlsId(ImageId), GetPointsLevelNameFromImageId(ImageId))

        Case Else

          ' Other PromoBOX images
          Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(GetImageNlsId(ImageId))

      End Select

      Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(GetImageNlsId(ImageId))

    End Function

    Public Shared Function GetDescription(ByVal ImageId As Integer) As String

      'Points Levels images
      Select Case ImageId
        Case 2, 3, 4, 5

          'Points Levels images
          Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(NLS_DESCR_BASE + ImageId, GetPointsLevelNameFromImageId(ImageId))
        
        Case Else

          ' Other PromoBOX images
          Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(NLS_DESCR_BASE + ImageId)

      End Select

      Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(NLS_DESCR_BASE + ImageId)

    End Function

    Public Shared Function GetImageNlsId(ByVal ImageId As Integer) As Integer

      Return NLS_NAMES_BASE + ImageId

    End Function

    '----------------------------------------------------------------------------
    ' PURPOSE : Check if two objects are the same ( has the same ID field ) 
    '           It's mandatory override this function to List.IndexOf() works right
    '
    ' PARAMS :
    '     - INPUT : Other object that same type
    '
    '     - OUTPUT :
    '
    ' RETURNS :
    '     - True or False 
    '
    ' NOTES :
    ' It's mandatory implement Equals() to be able to use List.Contains() and List.IndexOf() functions
    Public Overloads Function Equals(ByVal target As WKT_Image) As Boolean Implements IEquatable(Of WKT_Image).Equals
      If target.image_id = Me.image_id Then
        Return True
      Else
        Return False
      End If

    End Function


  End Class

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_FIELD_INFO
    Implements IEquatable(Of TYPE_FIELD_INFO)
    Public field_id As Integer
    Public shown As Boolean
    Public user_editable As Boolean
    Public system_editable As Boolean
    Public type As Integer
    Public min_length As Short
    Public max_length As Short
    Public order As Short
    Private Const NLS_NAMES_BASE As Integer = 833

    Public Shared Function GetName(ByVal FieldId As Integer) As String

      Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(GetNlsId(FieldId))

    End Function

    ' Map between FieldId with Field Name
    Public Shared Function GetNlsId(ByVal FieldId As Integer) As Integer

      Dim _id_nls As Long

      Select Case FieldId
        Case 1 : _id_nls = 758 ' FIELD NAME
        Case 2 : _id_nls = 762 ' CITY
        Case 3 : _id_nls = 763 ' ZIP
        Case 4 : _id_nls = 770 ' PIN
        Case 10 To 19 ' ADDRESS
          _id_nls = 759 + (FieldId - 10) ' NLS are sequencial
        Case 20 To 29 ' EMAIL 
          _id_nls = 766 + (FieldId - 20) ' NLS are sequencial
        Case 30 To 39 ' PHONE NUMBER
          _id_nls = 768 + (FieldId - 30)  ' NLS are sequencial
        Case 40 To 49 ' ID1
          _id_nls = 764 + (FieldId - 40)  ' NLS are sequencial
        Case 50 To 52 ' SURNAME1 , SURNAME2 and NAME
          _id_nls = 1184 + (FieldId - 50)  ' NLS are sequencial
        Case 53 : _id_nls = 5087 ' MIDDLE NAME
        Case 60 ' Birthday
          _id_nls = 1187
      End Select
      Return _id_nls

    End Function


    '----------------------------------------------------------------------------
    ' PURPOSE : Check if two objects are the same ( has the same ID field ) 
    '           It's mandatory override this function to List.IndexOf() works right
    '
    ' PARAMS :
    '     - INPUT : Other object that same type
    '
    '     - OUTPUT :
    '
    ' RETURNS :
    '     - True or False 
    '
    ' NOTES :
    ' It's mandatory implement Equals() to be able to use List.Contains() and List.IndexOf() functions
    Public Overloads Function Equals(ByVal target As TYPE_FIELD_INFO) As Boolean Implements IEquatable(Of TYPE_FIELD_INFO).Equals

      If target.field_id = Me.field_id Then
        Return True
      Else
        Return False
      End If
    End Function

    Public Sub New()

    End Sub

    Protected Overrides Sub Finalize()
      MyBase.Finalize()
    End Sub
  End Class

  Public Class TYPE_GIFT_TO_FUTURE
    Public percentage As Nullable(Of Integer)
    Public min_to_show As Nullable(Of Integer)
    Public max_to_show As Nullable(Of Integer)

  End Class

  Public Class TYPE_GIFTS_SPLIT
    Public enabled As Boolean
    Public category_1_name As String
    Public category_1_short As String
    Public category_2_name As String
    Public category_2_short As String
    Public category_3_name As String
    Public category_3_short As String
  End Class

  Public Class TYPE_GIFTS_CONFIG
    Public next_gifts_config As TYPE_GIFT_TO_FUTURE
    Public gifts_split As TYPE_GIFTS_SPLIT

    Public Const GIFTS_MAX_LENGTH As Integer = 30

    Sub New()
      Me.next_gifts_config = New TYPE_GIFT_TO_FUTURE
      Me.gifts_split = New TYPE_GIFTS_SPLIT
    End Sub
  End Class

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_MESSAGE
    Implements IEquatable(Of TYPE_MESSAGE)

    Public msg_id As Integer
    Public text As String
    Public Const SUB_KEY_PREFIX As String = "Msg."
    Private Const NLS_MESSAGES_BASE As Integer = 450
    Private Const NLS_MESSAGES_HELP_BASE As Integer = 500
    Public Const MSG_MAX_LENGTH As Integer = 100

    Public Shared Function GetMsgId(ByVal GeneralParamsSubjectKey As String) As Integer

      Return GUI_ParseNumber(GeneralParamsSubjectKey.Substring(SUB_KEY_PREFIX.Length))
    End Function

    Public Shared Function GetMessageName(ByVal MessageId As Integer) As String

      Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(NLS_MESSAGES_BASE + MessageId)
    End Function

    Public Shared Function GetMessageHelp(ByVal MessageId As Integer) As String

      Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(NLS_MESSAGES_HELP_BASE + MessageId)
    End Function

    '----------------------------------------------------------------------------
    ' PURPOSE : Check if two objects are the same ( has the same ID ) 
    '           It's mandatory override this function to List.IndexOf() works right
    ' PARAMS :
    '     - INPUT : Other object that same type
    '
    '     - OUTPUT :
    '
    ' RETURNS :
    '     - True or False 
    '
    ' NOTES :
    ' It's mandatory implement Equals() to be able to use List.Contains() and List.IndexOf() functions
    Public Overloads Function Equals(ByVal target As TYPE_MESSAGE) As Boolean Implements IEquatable(Of TYPE_MESSAGE).Equals

      If target.msg_id = Me.msg_id Then
        Return True
      Else
        Return False
      End If
    End Function

  End Class

  Public Class TYPE_SCHEDULE
    Public from_time As String
    Public to_time As String

    Public Const SUB_KEY_FROM As String = "Schedule.From"
    Public Const SUB_KEY_TO As String = "Schedule.To"

    Public Const SCHEDULE_MAX_LENGTH As Integer = 4
  End Class

  Public Class TYPE_RECHARGES_VOUCHER_CONF
    Public header As String
    Public footer As String

    Public Const SUB_KEY_HEADER As String = "Voucher.Recharges.Header"
    Public Const SUB_KEY_FOOTER As String = "Voucher.Recharges.Footer"

    Public Const VOUCHER_CONF_MAX_LENGTH As Integer = 500
  End Class

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_ADS_CONFIGURATION
    Public functionalities As List(Of TYPE_FUNCTIONALITY)
    Public images As List(Of WKT_Image)
    Public fields As List(Of TYPE_FIELD_INFO)
    Public gifts As TYPE_GIFTS_CONFIG
    Public messages As List(Of TYPE_MESSAGE)
    Public schedule As TYPE_SCHEDULE
    Public recharges_voucher_conf As TYPE_RECHARGES_VOUCHER_CONF

    Sub New()
      Me.functionalities = New List(Of TYPE_FUNCTIONALITY)
      Me.images = New List(Of WKT_Image)
      Me.fields = New List(Of TYPE_FIELD_INFO)
      Me.gifts = New TYPE_GIFTS_CONFIG
      Me.messages = New List(Of TYPE_MESSAGE)
      Me.schedule = New TYPE_SCHEDULE
      Me.recharges_voucher_conf = New TYPE_RECHARGES_VOUCHER_CONF
    End Sub

  End Class

#End Region ' Structures

#Region " Members "

  Protected m_ads_configuration As TYPE_ADS_CONFIGURATION

#End Region

#Region " Properties "

  Public Property Functionalities() As List(Of TYPE_FUNCTIONALITY)
    Get
      Return Me.m_ads_configuration.functionalities
    End Get
    Set(ByVal value As List(Of TYPE_FUNCTIONALITY))
      Me.m_ads_configuration.functionalities = value
    End Set
  End Property

  Public Property Images() As List(Of WKT_Image)
    Get
      Return Me.m_ads_configuration.images
    End Get
    Set(ByVal value As List(Of WKT_Image))
      Me.m_ads_configuration.images = value
    End Set
  End Property

  Public Property Fields() As List(Of TYPE_FIELD_INFO)
    Get
      Return Me.m_ads_configuration.fields
    End Get
    Set(ByVal value As List(Of TYPE_FIELD_INFO))
      Me.m_ads_configuration.fields = value
    End Set
  End Property

  Public Property Gifts() As TYPE_GIFTS_CONFIG
    Get
      Return Me.m_ads_configuration.gifts
    End Get
    Set(ByVal value As TYPE_GIFTS_CONFIG)
      Me.m_ads_configuration.gifts = value
    End Set
  End Property

  Public Property Messages() As List(Of TYPE_MESSAGE)
    Get
      Return Me.m_ads_configuration.messages
    End Get
    Set(ByVal value As List(Of TYPE_MESSAGE))
      Me.m_ads_configuration.messages = value
    End Set
  End Property

  Public Property Schedule() As TYPE_SCHEDULE
    Get
      Return Me.m_ads_configuration.schedule
    End Get
    Set(ByVal value As TYPE_SCHEDULE)
      Me.m_ads_configuration.schedule = value
    End Set
  End Property

  Public Property RechargesVoucherConf() As TYPE_RECHARGES_VOUCHER_CONF
    Get
      Return Me.m_ads_configuration.recharges_voucher_conf
    End Get
    Set(ByVal value As TYPE_RECHARGES_VOUCHER_CONF)
      Me.m_ads_configuration.recharges_voucher_conf = value
    End Set
  End Property

#End Region

#Region " Overrides function"

  ' PURPOSE : Returns Auditor header
  '
  '
  ' Example : Retur String like "User Fields . Surname Editable "

  Private Function GetAuditorHeader(ByVal FieldGroup As String, ByVal FieldName As String, ByVal FieldProperty As String) As String

    Dim _double_quotes As String
    _double_quotes = String.Empty ' Chr(34)
    Return String.Format("{0}{1}{0}.{0}{2}{0} {3}", _double_quotes, FieldGroup, FieldName, FieldProperty)
  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE : Returns the related auditor data for the current object.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - The newly created audit object
  '
  ' NOTES :
  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _id_nls As Integer
    Dim _label_editable As String
    Dim _label_show As String
    Dim _label_enabled As String
    Dim _label_min_gifts_to_show As String
    Dim _label_max_gifts_to_show As String
    Dim _label_type As String
    Dim _label_order As String
    Dim _label_changed As String
    Dim _extra_points As String
    Dim _min_count As String
    Dim _max_count As String
    Dim _str_field_name As String

    Dim _title As String
    Dim _str_functionality_name As String
    Dim _str_section As String
    Dim _str_value As String

    Dim _label_yes As String
    Dim _label_no As String


    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_TERMINALS)
    _label_editable = GLB_NLS_GUI_PLAYER_TRACKING.GetString(755)
    _label_show = GLB_NLS_GUI_PLAYER_TRACKING.GetString(754)
    _label_enabled = GLB_NLS_GUI_PLAYER_TRACKING.GetString(757)
    _label_min_gifts_to_show = GLB_NLS_GUI_PLAYER_TRACKING.GetString(772)
    _label_max_gifts_to_show = GLB_NLS_GUI_PLAYER_TRACKING.GetString(773)
    _label_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1205)
    _label_order = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1206)
    _label_changed = GLB_NLS_GUI_PLAYER_TRACKING.GetString(774)
    _label_yes = GLB_NLS_GUI_PLAYER_TRACKING.GetString(698)
    _label_no = GLB_NLS_GUI_PLAYER_TRACKING.GetString(699)

    Call _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(722), "")

    'FIELDS
    For Each _field As TYPE_FIELD_INFO In Me.Fields

      _id_nls = TYPE_FIELD_INFO.GetNlsId(_field.field_id)
      _str_field_name = TYPE_FIELD_INFO.GetName(_field.field_id)
      _str_section = GLB_NLS_GUI_PLAYER_TRACKING.GetString(753) ' Player Info

      ' Changed Value, Field Title
      _title = GetAuditorHeader(_str_section, _str_field_name, _label_editable)
      _str_value = IIf(_field.user_editable, _label_yes, _label_no)
      Call _auditor_data.SetField(0, _str_value, _title)

      _title = GetAuditorHeader(_str_section, _str_field_name, _label_show)
      _str_value = IIf(_field.shown, _label_yes, _label_no)
      Call _auditor_data.SetField(0, _str_value, _title)

      _title = GetAuditorHeader(_str_section, _str_field_name, _label_order)
      Call _auditor_data.SetField(0, _field.order, _title)

    Next

    'FUNCTIONALITIES
    _str_section = GLB_NLS_GUI_PLAYER_TRACKING.GetString(736) ' Functionalities

    For Each _funct As TYPE_FUNCTIONALITY In Me.Functionalities
      _id_nls = TYPE_FUNCTIONALITY.GetNlsId(_funct.function_id)
      _str_functionality_name = TYPE_FUNCTIONALITY.GetName(_funct.function_id)

      _title = GetAuditorHeader(_str_section, _str_functionality_name, _label_enabled)
      _str_value = IIf(_funct.enabled, _label_yes, _label_no)
      Call _auditor_data.SetField(0, _str_value, _title)

    Next

    'IMAGES
    For Each _image As WKT_Image In Me.Images

      Dim _str_image_name As String
      _str_section = GLB_NLS_GUI_PLAYER_TRACKING.GetString(742) ' Images

      _id_nls = WKT_Image.GetImageNlsId(_image.image_id)
      _str_image_name = WKT_Image.GetImageName(_image.image_id)


      '' TO DO : 
      '' PROBLEM : Replaced image isn't loaded in DbReadObject ( only has ResourceId ) 
      ''           ImageCRC from DbReadObject alawys returns "--"
      '_img_crc = IIf(Not _image.image_cached Is Nothing, mdl_misc.GetInfoCRC(_image.image_cached), "--")

      _title = GetAuditorHeader(_str_section, _str_image_name, _label_changed)

      _str_value = IIf(_image.image_has_changes, _label_yes, _label_no)
      Call _auditor_data.SetField(0, _str_value, _title)

    Next

    'GIFTS

    '   - Next Gifts.Percentage
    If Me.Gifts.next_gifts_config.percentage.HasValue Then
      _extra_points = Me.Gifts.next_gifts_config.percentage.Value
    Else
      _extra_points = "''"
    End If

    _title = GetAuditorHeader(GLB_NLS_GUI_PLAYER_TRACKING.GetString(738), _
                              GLB_NLS_GUI_PLAYER_TRACKING.GetString(799), _
                              String.Empty)

    Call _auditor_data.SetField(0, _extra_points, _title)

    '   - Next Gifts.Min2show
    If Me.Gifts.next_gifts_config.min_to_show.HasValue Then
      _min_count = Me.Gifts.next_gifts_config.min_to_show.Value
    Else
      _min_count = "''"
    End If

    _title = GetAuditorHeader(GLB_NLS_GUI_PLAYER_TRACKING.GetString(738), _
                              GLB_NLS_GUI_PLAYER_TRACKING.GetString(800), _
                              String.Empty)

    Call _auditor_data.SetField(0, _min_count, _title)

    '   - Next Gifts.Percentage
    If Me.Gifts.next_gifts_config.max_to_show.HasValue Then
      _max_count = Me.Gifts.next_gifts_config.max_to_show.Value
    Else
      _max_count = "''"
    End If

    _title = GetAuditorHeader(GLB_NLS_GUI_PLAYER_TRACKING.GetString(738), _
                              GLB_NLS_GUI_PLAYER_TRACKING.GetString(801), _
                              String.Empty)

    Call _auditor_data.SetField(0, _max_count, _title)

    '   - Gifts split 
    '        - Enabled
    '        - Category 1 name
    '        - Category 1 short
    '        - Category 2 name
    '        - Category 2 short
    '        - Category 3 name
    '        - Category 3 short

    '        - Enabled
    _title = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1445)

    _str_value = IIf(Me.Gifts.gifts_split.enabled, _label_yes, _label_no)
    Call _auditor_data.SetField(0, _str_value, _title)

    '        - Category 1 name
    _title = GetAuditorHeader(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1445), _
                              GLB_NLS_GUI_PLAYER_TRACKING.GetString(1446), _
                              String.Empty)
    Call _auditor_data.SetField(0, Me.Gifts.gifts_split.category_1_name, _title)

    '        - Category 1 short
    _title = GetAuditorHeader(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1445), _
                              GLB_NLS_GUI_PLAYER_TRACKING.GetString(1449), _
                              String.Empty)
    Call _auditor_data.SetField(0, Me.Gifts.gifts_split.category_1_short, _title)

    '        - Category 2 name
    _title = GetAuditorHeader(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1445), _
                              GLB_NLS_GUI_PLAYER_TRACKING.GetString(1447), _
                              String.Empty)
    Call _auditor_data.SetField(0, Me.Gifts.gifts_split.category_2_name, _title)

    '        - Category 2 short
    _title = GetAuditorHeader(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1445), _
                              GLB_NLS_GUI_PLAYER_TRACKING.GetString(1551), _
                              String.Empty)
    Call _auditor_data.SetField(0, Me.Gifts.gifts_split.category_2_short, _title)

    '        - Category 3 name
    _title = GetAuditorHeader(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1445), _
                              GLB_NLS_GUI_PLAYER_TRACKING.GetString(1448), _
                              String.Empty)
    Call _auditor_data.SetField(0, Me.Gifts.gifts_split.category_3_name, _title)

    '        - Category 3 short
    _title = GetAuditorHeader(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1445), _
                              GLB_NLS_GUI_PLAYER_TRACKING.GetString(1552), _
                              String.Empty)
    Call _auditor_data.SetField(0, Me.Gifts.gifts_split.category_3_short, _title)

    'MESSAGES
    For Each _message As TYPE_MESSAGE In Me.Messages

      _str_section = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1422) ' Messages

      _title = String.Format("{0} - {1}", _str_section, TYPE_MESSAGE.GetMessageName(_message.msg_id))

      Call _auditor_data.SetField(0, _message.text, _title)

    Next

    'SCHEDULE
    _str_section = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1429) '1429 Schedule

    _title = String.Format("{0} - {1}", _str_section, GLB_NLS_GUI_PLAYER_TRACKING.GetString(297))
    Call _auditor_data.SetField(0, Me.Schedule.from_time, _title)

    _title = String.Format("{0} - {1}", _str_section, GLB_NLS_GUI_PLAYER_TRACKING.GetString(298))
    Call _auditor_data.SetField(0, Me.Schedule.to_time, _title)

    'RECHARGES VOUCHER CONF
    _str_section = GLB_NLS_GUI_CONFIGURATION.GetString(237)        ' 237 "Voucher"

    _title = String.Format("{0} - {1}", _str_section, GLB_NLS_GUI_CONFIGURATION.GetString(228))
    Call _auditor_data.SetField(0, Me.RechargesVoucherConf.header, _title)

    _title = String.Format("{0} - {1}", _str_section, GLB_NLS_GUI_CONFIGURATION.GetString(229))
    Call _auditor_data.SetField(0, Me.RechargesVoucherConf.footer, _title)

    Return _auditor_data

  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE : Deletes the given object from the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DEPENDENCIES
  '
  ' NOTES :
  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE : Inserts the given object to the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES :
  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE : Reads from the database into the given object
  '
  ' PARAMS :
  '     - INPUT :
  '         - ObjectId: An object, it must be 'casted' to the desired KEY.
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '
  ' NOTES :
  Public Overrides Function DB_Read(ByVal ObjectId As Object, _
                                    ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As Integer
    _rc = GetConfigurationData(Me.m_ads_configuration)

    Select Case _rc

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE : Updates the given object to the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES :
  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _rc As Integer

    _rc = Me.UpdateConfigurationDataTable(Me.m_ads_configuration)

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
        Return ENUM_STATUS.STATUS_DUPLICATE_KEY

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select
  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE : Returns a duplicate of the given object.
  '
  ' PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - The newly created object
  '
  ' NOTES :
  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE
    Dim _target As CLS_ADS_CONFIGURATION
    Dim _img_copy As CLS_ADS_CONFIGURATION.WKT_Image

    _target = New CLS_ADS_CONFIGURATION()

    _target.Images = New List(Of CLS_ADS_CONFIGURATION.WKT_Image)

    For Each _img As CLS_ADS_CONFIGURATION.WKT_Image In Me.Images
      _img_copy = New CLS_ADS_CONFIGURATION.WKT_Image
      _img_copy.image_id = _img.image_id
      _img_copy.resource_id = _img.resource_id
      If (Not IsNothing(_img.image_cached)) Then
        _img_copy.image_cached = _img.image_cached.Clone()
      End If
      _img_copy.image_has_changes = _img.image_has_changes
      _target.Images.Add(_img_copy)
    Next

    _target.Fields = New List(Of CLS_ADS_CONFIGURATION.TYPE_FIELD_INFO)
    For Each _field As CLS_ADS_CONFIGURATION.TYPE_FIELD_INFO In Me.Fields
      _target.Fields.Add(_field)
    Next

    _target.Functionalities = New List(Of CLS_ADS_CONFIGURATION.TYPE_FUNCTIONALITY)
    For Each _funct As CLS_ADS_CONFIGURATION.TYPE_FUNCTIONALITY In Me.Functionalities
      _target.Functionalities.Add(_funct)
    Next

    _target.Gifts.next_gifts_config.min_to_show = Me.Gifts.next_gifts_config.min_to_show
    _target.Gifts.next_gifts_config.percentage = Me.Gifts.next_gifts_config.percentage
    _target.Gifts.next_gifts_config.max_to_show = Me.Gifts.next_gifts_config.max_to_show
    _target.Gifts.gifts_split.enabled = Me.Gifts.gifts_split.enabled
    _target.Gifts.gifts_split.category_1_name = Me.Gifts.gifts_split.category_1_name
    _target.Gifts.gifts_split.category_1_short = Me.Gifts.gifts_split.category_1_short
    _target.Gifts.gifts_split.category_2_name = Me.Gifts.gifts_split.category_2_name
    _target.Gifts.gifts_split.category_2_short = Me.Gifts.gifts_split.category_2_short
    _target.Gifts.gifts_split.category_3_name = Me.Gifts.gifts_split.category_3_name
    _target.Gifts.gifts_split.category_3_short = Me.Gifts.gifts_split.category_3_short

    For Each _msg As CLS_ADS_CONFIGURATION.TYPE_MESSAGE In Me.Messages
      _target.Messages.Add(_msg)
    Next

    _target.Schedule.from_time = Me.Schedule.from_time
    _target.Schedule.to_time = Me.Schedule.to_time

    _target.RechargesVoucherConf.header = Me.RechargesVoucherConf.header
    _target.RechargesVoucherConf.footer = Me.RechargesVoucherConf.footer

    Return _target
  End Function


#End Region

#Region " Private functions "

#Region " Update data "


  ' PURPOSE: UpdateConfigurationDataTable
  '                  
  '  PARAMS:         
  '     - INPUT:     
  '           - 
  '     - OUTPUT:    
  '           -  LocalData As TYPE_ADS_CONFIGURATION, 
  '                  
  ' RETURNS:         
  '     - Integer
  '                  
  Private Function UpdateConfigurationDataTable(ByRef LocalData As TYPE_ADS_CONFIGURATION) As Integer
    Dim _do_commit As Boolean
    Dim _sql_data_adapter As SqlDataAdapter

    Dim _rows_affected As Integer
    Dim _dtable As DataTable
    Dim _row As DataRow

    _do_commit = False
    _sql_data_adapter = New SqlDataAdapter()

    Try
      Using _db_trx As New DB_TRX()

        ' UPDATE FUNCTIONALITIES
        _sql_data_adapter = New SqlDataAdapter()
        _dtable = Me.GetFunctionalitiesSchemma(_db_trx.SqlTransaction)
        _sql_data_adapter.UpdateCommand = SqlUpdateFunctionalities()          ' SET SQL UPDATE COMMAND
        _sql_data_adapter.UpdateCommand.Connection = _db_trx.SqlTransaction.Connection
        _sql_data_adapter.UpdateCommand.Transaction = _db_trx.SqlTransaction

        For Each _field As TYPE_FUNCTIONALITY In LocalData.functionalities
          _row = _dtable.NewRow()
          _row("fun_function_id") = _field.function_id
          _row("fun_enabled") = _field.enabled
          _row("fun_name") = " " ' Don't allow null values
          _dtable.Rows.Add(_row)
        Next

        Call MarkAsModified(_dtable)
        _rows_affected = _sql_data_adapter.Update(_dtable)

        If (_rows_affected = 0) Then
          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

        ' UPDATE FIELDS
        _sql_data_adapter = New SqlDataAdapter()
        _dtable = Me.GetFieldsSchemma(_db_trx.SqlTransaction)
        _sql_data_adapter.UpdateCommand = SqlUpdateFields()                 ' SET SQL UPDATE COMMAND
        _sql_data_adapter.UpdateCommand.Connection = _db_trx.SqlTransaction.Connection
        _sql_data_adapter.UpdateCommand.Transaction = _db_trx.SqlTransaction

        For Each _field As TYPE_FIELD_INFO In LocalData.fields
          _row = _dtable.NewRow()
          _row("PIF_FIELD_ID") = _field.field_id
          _row("PIF_SHOWN") = _field.shown
          _row("PIF_USER_ALLOW_EDIT") = _field.user_editable
          _row("PIF_TYPE") = _field.type
          _row("PIF_MIN_LENGTH") = _field.min_length
          _row("PIF_MAX_LENGTH") = _field.max_length
          _row("PIF_NAME") = " "
          _row("PIF_EDITABLE") = False ' FAKE VALUE , DON'T UPDATE! (( �� LLEIG !! ))
          _row("PIF_ORDER") = _field.order
          _dtable.Rows.Add(_row)
        Next

        Call MarkAsModified(_dtable)

        _rows_affected = _sql_data_adapter.Update(_dtable)

        If (_rows_affected = 0) Then
          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

        ' UPDATE GIFTS
        Dim _gp_update_list As New List(Of String())

        _gp_update_list.Add(GeneralParam.AddToList(PROMOBOX_GP_GROUP_KEY, "FutureGifts.ExtraPointsPct", IIf(LocalData.gifts.next_gifts_config.percentage.HasValue, LocalData.gifts.next_gifts_config.percentage, "")))
        _gp_update_list.Add(GeneralParam.AddToList(PROMOBOX_GP_GROUP_KEY, "FutureGifts.MinCount", IIf(LocalData.gifts.next_gifts_config.min_to_show.HasValue, LocalData.gifts.next_gifts_config.min_to_show, "")))
        _gp_update_list.Add(GeneralParam.AddToList(PROMOBOX_GP_GROUP_KEY, "FutureGifts.MaxCount", IIf(LocalData.gifts.next_gifts_config.max_to_show.HasValue, LocalData.gifts.next_gifts_config.max_to_show, "")))
        _gp_update_list.Add(GeneralParam.AddToList(PROMOBOX_GP_GROUP_KEY, "Gifts.SplitCategories", IIf(LocalData.gifts.gifts_split.enabled, "1", "0")))
        _gp_update_list.Add(GeneralParam.AddToList(PROMOBOX_GP_GROUP_KEY, "Gifts.CategoryName01", LocalData.gifts.gifts_split.category_1_name))
        _gp_update_list.Add(GeneralParam.AddToList(PROMOBOX_GP_GROUP_KEY, "Gifts.CategoryShort01", LocalData.gifts.gifts_split.category_1_short))
        _gp_update_list.Add(GeneralParam.AddToList(PROMOBOX_GP_GROUP_KEY, "Gifts.CategoryName02", LocalData.gifts.gifts_split.category_2_name))
        _gp_update_list.Add(GeneralParam.AddToList(PROMOBOX_GP_GROUP_KEY, "Gifts.CategoryShort02", LocalData.gifts.gifts_split.category_2_short))
        _gp_update_list.Add(GeneralParam.AddToList(PROMOBOX_GP_GROUP_KEY, "Gifts.CategoryName03", LocalData.gifts.gifts_split.category_3_name))
        _gp_update_list.Add(GeneralParam.AddToList(PROMOBOX_GP_GROUP_KEY, "Gifts.CategoryShort03", LocalData.gifts.gifts_split.category_3_short))

        For Each _general_param As String() In _gp_update_list
          If (Not GUI_Controls.DB_GeneralParam_Update(_general_param(GeneralParam.COLUMN_NAME.Group),
                                                      _general_param(GeneralParam.COLUMN_NAME.Subject),
                                                      _general_param(GeneralParam.COLUMN_NAME.Value),
                                                      _db_trx.SqlTransaction)) Then

            Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
          End If
        Next

        Dim _sql_command As SqlCommand

        For Each _img As CLS_ADS_CONFIGURATION.WKT_Image In LocalData.images
          If _img.image_has_changes Then
            If _img.resource_id = 0 Then
              If Not _img.image_deleted Then
                ' INSERT
                Dim _new_resid As Nullable(Of Long)
                _new_resid = New Nullable(Of Long)
                SaveImage(_new_resid, _img.image_cached, _db_trx.SqlTransaction)
                _img.resource_id = _new_resid ' NEED TO BE UPDATED IN DB

              Else
                ' DELETE
                ' If picture has been deleted, set resource_id = 0 
                ' fmr_ads_configuration gets the default image from enum instead of wkt_images.
              End If

              _sql_command = SqlUpdateImages(_img.image_id, _img.resource_id)
              _sql_command.Connection = _db_trx.SqlTransaction.Connection
              _sql_command.Transaction = _db_trx.SqlTransaction
              _rows_affected = _sql_command.ExecuteNonQuery()

              If _rows_affected = 0 Then
                Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
              End If
            Else ' Customed image has changes.
              SaveImage(_img.resource_id, _img.image_cached, _db_trx.SqlTransaction)
            End If
          End If
        Next

        'MESSAGES
        For Each _message As TYPE_MESSAGE In LocalData.messages
          If (Not GUI_Controls.DB_GeneralParam_Update(PROMOBOX_GP_GROUP_KEY,
                                                  TYPE_MESSAGE.SUB_KEY_PREFIX & _message.msg_id,
                                                  _message.text,
                                                  _db_trx.SqlTransaction)) Then

            Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
          End If
        Next

        'SCHEDULE
        If LocalData.schedule.from_time.Length <> 5 _
          Or LocalData.schedule.to_time.Length <> 5 Then

          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

        _gp_update_list.Clear()

        _gp_update_list.Add(GeneralParam.AddToList(PROMOBOX_GP_GROUP_KEY, TYPE_SCHEDULE.SUB_KEY_FROM, LocalData.schedule.from_time.Substring(0, 2) + LocalData.schedule.from_time.Substring(3, 2)))
        _gp_update_list.Add(GeneralParam.AddToList(PROMOBOX_GP_GROUP_KEY, TYPE_SCHEDULE.SUB_KEY_TO, LocalData.schedule.to_time.Substring(0, 2) + LocalData.schedule.to_time.Substring(3, 2)))

        For Each _general_param As String() In _gp_update_list
          If (Not GUI_Controls.DB_GeneralParam_Update(_general_param(GeneralParam.COLUMN_NAME.Group),
                                                      _general_param(GeneralParam.COLUMN_NAME.Subject),
                                                      _general_param(GeneralParam.COLUMN_NAME.Value),
                                                      _db_trx.SqlTransaction)) Then

            Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
          End If
        Next

        'RECHARGES VOUCHER CONF
        _gp_update_list.Clear()

        _gp_update_list.Add(GeneralParam.AddToList(PROMOBOX_GP_GROUP_KEY, TYPE_RECHARGES_VOUCHER_CONF.SUB_KEY_HEADER, LocalData.recharges_voucher_conf.header))
        _gp_update_list.Add(GeneralParam.AddToList(PROMOBOX_GP_GROUP_KEY, TYPE_RECHARGES_VOUCHER_CONF.SUB_KEY_FOOTER, LocalData.recharges_voucher_conf.footer))

        For Each _general_param As String() In _gp_update_list
          If (Not GUI_Controls.DB_GeneralParam_Update(_general_param(GeneralParam.COLUMN_NAME.Group), _general_param(GeneralParam.COLUMN_NAME.Subject), _general_param(GeneralParam.COLUMN_NAME.Value), _db_trx.SqlTransaction)) Then
            _rows_affected = False

            Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
          End If
        Next

        ' No error
        _do_commit = True
        GUI_EndSQLTransaction(_db_trx.SqlTransaction, _do_commit)
      End Using

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

    Catch ex As Exception
      _do_commit = False
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

  End Function 'UpdateConfigurationDataTable

  ' PURPOSE: SqlUpdateFunctionalities
  '                  
  '  PARAMS:         
  '     - INPUT:     
  '           - None
  '     - OUTPUT:    
  '           - None
  '                  
  ' RETURNS:         
  '     - SqlCommand
  '                  
  Private Function SqlUpdateFunctionalities() As SqlCommand

    Dim _sql_update_command As SqlCommand
    Dim _str_bld_update As StringBuilder
    _str_bld_update = New StringBuilder

    _str_bld_update.AppendLine("UPDATE   WKT_FUNCTIONALITIES    ")
    _str_bld_update.AppendLine("   SET   FUN_ENABLED =@pEnabled ")
    _str_bld_update.AppendLine(" WHERE   FUN_FUNCTION_ID=@pId   ")

    _sql_update_command = New SqlCommand(_str_bld_update.ToString())

    _sql_update_command.Parameters.Add("@pEnabled", SqlDbType.Bit, 1, "fun_enabled")
    _sql_update_command.Parameters.Add("@pId", SqlDbType.BigInt, 99, "fun_function_id")
    Return _sql_update_command
  End Function 'SqlUpdateFunctionalities

  ' PURPOSE: SqlUpdateFields
  '                  
  '  PARAMS:         
  '     - INPUT:     
  '           - None
  '     - OUTPUT:    
  '           - None
  '                  
  ' RETURNS:         
  '     - SqlCommand
  '                  
  Private Function SqlUpdateFields() As SqlCommand
    Dim _sql_update_command As SqlCommand
    Dim _str_bld_update As StringBuilder
    _str_bld_update = New StringBuilder

    _str_bld_update.AppendLine("UPDATE   WKT_PLAYER_INFO_FIELDS           ")
    _str_bld_update.AppendLine("   SET   PIF_SHOWN = @pShown              ")
    _str_bld_update.AppendLine("       , PIF_USER_ALLOW_EDIT = @pEditable ")
    _str_bld_update.AppendLine("       , PIF_TYPE =  @pType               ")
    _str_bld_update.AppendLine("       , PIF_MIN_LENGTH = @pMinLength     ")
    _str_bld_update.AppendLine("       , PIF_MAX_LENGTH = @pMaxLength     ")
    _str_bld_update.AppendLine("       , PIF_ORDER = @pOrder              ")
    _str_bld_update.AppendLine(" WHERE   PIF_FIELD_ID = @pId              ")

    _sql_update_command = New SqlCommand(_str_bld_update.ToString())

    _sql_update_command.Parameters.Add("@pShown", SqlDbType.Bit, 1, "PIF_SHOWN")
    _sql_update_command.Parameters.Add("@pEditable", SqlDbType.Bit, 1, "PIF_USER_ALLOW_EDIT")
    _sql_update_command.Parameters.Add("@pType", SqlDbType.Int, 99, "PIF_TYPE")
    _sql_update_command.Parameters.Add("@pMinLength", SqlDbType.Int, 99, "PIF_MIN_LENGTH")
    _sql_update_command.Parameters.Add("@pMaxLength", SqlDbType.Int, 99, "PIF_MAX_LENGTH")
    _sql_update_command.Parameters.Add("@pId", SqlDbType.BigInt, 99, "PIF_FIELD_ID")
    _sql_update_command.Parameters.Add("@pOrder", SqlDbType.Int, 99, "PIF_ORDER")

    Return _sql_update_command

  End Function 'SqlUpdateFields


  Private Function SqlUpdateImages(ByVal imageId As Long, ByVal resourceId As Long) As SqlCommand
    Dim _sql_command As SqlCommand
    Dim _str_bld_update As StringBuilder
    _sql_command = New SqlCommand()
    _str_bld_update = New StringBuilder

    _str_bld_update.AppendLine("UPDATE   WKT_IMAGES ")
    _str_bld_update.AppendLine("   SET   CIM_RESOURCE_ID = @pResourceId ")
    _str_bld_update.AppendLine(" WHERE   CIM_IMAGE_ID = @pImageId")

    _sql_command = New SqlCommand(_str_bld_update.ToString())

    _sql_command.Parameters.Add("@pResourceId", SqlDbType.Int).Value = resourceId
    _sql_command.Parameters.Add("@pImageId", SqlDbType.Int).Value = imageId

    Return _sql_command
  End Function

  ' PURPOSE: GetScheema
  '                  
  '  PARAMS:         
  '     - INPUT:     
  '           -  SqlCmd As SqlCommand, 
  '     - OUTPUT:    
  '           -  Trax As SqlTransaction, 
  '                  
  ' RETURNS:         
  '     - DataTable
  '                  
  Private Function GetSchema(ByVal SqlCmd As SqlCommand, ByRef Trax As SqlTransaction) As DataTable

    Dim _dtable As DataTable

    _dtable = New DataTable()
    SqlCmd.Transaction = Trax
    SqlCmd.Connection = Trax.Connection

    Using _sql_da As New SqlDataAdapter(SqlCmd)
      _sql_da.FillSchema(_dtable, SchemaType.Source)
    End Using

    Return _dtable

  End Function 'GetScheema


  ' PURPOSE: GetFunctionalitiesSchemma
  '                  
  '  PARAMS:         
  '     - INPUT:     
  '           - 
  '     - OUTPUT:    
  '           -  Trax As SqlTransaction, 
  '                  
  ' RETURNS:         
  '     - DataTable
  '                  
  Private Function GetFunctionalitiesSchemma(ByRef Trax As SqlTransaction) As DataTable

    Dim _str_bld As StringBuilder
    _str_bld = New StringBuilder()

    _str_bld.AppendLine("SELECT   FUN_FUNCTION_ID")
    _str_bld.AppendLine("       , FUN_NAME")
    _str_bld.AppendLine("       , FUN_ENABLED ")
    _str_bld.AppendLine("  FROM   WKT_FUNCTIONALITIES")

    Return GetSchema(New SqlCommand(_str_bld.ToString()), Trax)

  End Function 'GetFunctionalitiesSchemma

  ' PURPOSE: GetFieldsSchemma
  '                  
  '  PARAMS:         
  '     - INPUT:     
  '           - 
  '     - OUTPUT:    
  '           -  Trax As SqlTransaction, 
  '                  
  ' RETURNS:         
  '     - DataTable
  '                  
  Private Function GetFieldsSchemma(ByRef Trax As SqlTransaction) As DataTable

    Dim _str_bld As StringBuilder
    _str_bld = New StringBuilder()

    _str_bld.AppendLine("SELECT   PIF_FIELD_ID          ")
    _str_bld.AppendLine("       , PIF_NAME,PIF_SHOWN    ")
    _str_bld.AppendLine("       , PIF_USER_ALLOW_EDIT   ")
    _str_bld.AppendLine("       , PIF_EDITABLE          ")
    _str_bld.AppendLine("       , PIF_TYPE              ")
    _str_bld.AppendLine("       , PIF_MIN_LENGTH        ")
    _str_bld.AppendLine("       , PIF_MAX_LENGTH        ")
    _str_bld.AppendLine("       , PIF_ORDER             ")
    _str_bld.AppendLine("  FROM   WKT_PLAYER_INFO_FIELDS")

    Return GetSchema(New SqlCommand(_str_bld.ToString()), Trax)

  End Function 'GetFieldsSchemma

  ' PURPOSE: Mark all rows in datable as Modifies
  '                  
  '  PARAMS:         
  '     - INPUT:     
  '           - 
  '     - OUTPUT:    
  '           -  table As DataTable, 
  '                  
  ' RETURNS:         
  '     -None
  '                  
  Private Sub MarkAsModified(ByRef table As DataTable)
    Dim _row As DataRow
    table.AcceptChanges()

    For Each _row In table.Rows
      _row.SetModified()
    Next

  End Sub 'MarkAsModified

#End Region

#Region " Read data"

  '----------------------------------------------------------------------------
  ' PURPOSE : Fill TPYE_ADS_CONFIGURATION with database rows.
  '
  ' PARAMS :
  '     - INPUT : An instance of TYPE_ADS_CONFIGURATION object
  '
  '     - OUTPUT : An instance of TYPE_ADS_CONFIGURATION with rows filled
  '
  ' RETURNS :
  '     - ENUM_CONFIGURATION_RC
  '
  ' NOTES :
  Private Function GetConfigurationData(ByRef ads_configuration As CLS_ADS_CONFIGURATION.TYPE_ADS_CONFIGURATION) As Integer

    Dim _rc As Integer
    _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_OK

    Try

      ads_configuration.functionalities = GetFunctionalities()
      ads_configuration.fields = GetFieldsInfo()
      ads_configuration.images = GetImages()
      ads_configuration.gifts = GetGiftConfiguration()
      ads_configuration.messages = GetMessages()
      ads_configuration.schedule = GetSchedule()
      ads_configuration.recharges_voucher_conf = GetRechargesVoucherConf()

    Catch _ex As Exception
      _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, "cls_ads_configuration", "GetConfigurationData", _ex.Message)
    End Try

    Return _rc

  End Function

  ' PURPOSE: GetGiftConfiguration
  '                  
  '  PARAMS:         
  '     - INPUT:     
  '           - None
  '     - OUTPUT:    
  '           - None
  '                  
  ' RETURNS:         
  '     - TYPE_GIFTS_TO_FUTURE
  '                  
  Private Function GetGiftConfiguration() As TYPE_GIFTS_CONFIG

    Dim _sql_cmd As SqlCommand
    Dim _sql_transaction As System.Data.SqlClient.SqlTransaction

    Dim _target As CLS_ADS_CONFIGURATION.TYPE_GIFTS_CONFIG
    Dim _extra_points As Nullable(Of Integer)
    Dim _min_count As Nullable(Of Integer)
    Dim _max_count As Nullable(Of Integer)
    Dim _gifts_split_enabled As Boolean
    Dim _category_1_name As String
    Dim _category_1_short As String
    Dim _category_2_name As String
    Dim _category_2_short As String
    Dim _category_3_name As String
    Dim _category_3_short As String
    Dim _data As Object
    Dim _str_bld As StringBuilder

    _target = New CLS_ADS_CONFIGURATION.TYPE_GIFTS_CONFIG
    _sql_transaction = Nothing

    _gifts_split_enabled = False
    _category_1_name = ""
    _category_1_short = ""
    _category_2_name = ""
    _category_2_short = ""
    _category_3_name = ""
    _category_3_short = ""

    Try

      If Not GUI_BeginSQLTransaction(_sql_transaction) Then
        'TODO JMM 28-NOV-2012: Return an empty object
      End If

      ' Extra Points %
      _str_bld = New StringBuilder()
      _str_bld.AppendLine("SELECT   GP_KEY_VALUE                  ")
      _str_bld.AppendLine("  FROM   GENERAL_PARAMS                ")
      _str_bld.AppendLine(" WHERE   GP_GROUP_KEY = @pGroupKey     ")
      _str_bld.AppendLine("   AND   GP_SUBJECT_KEY = @pSubjectKey ")

      _sql_cmd = New SqlCommand(_str_bld.ToString())
      _sql_cmd.Connection = _sql_transaction.Connection
      _sql_cmd.Transaction = _sql_transaction

      _sql_cmd.Parameters.Add("@pGroupKey", SqlDbType.NVarChar).Value = PROMOBOX_GP_GROUP_KEY
      _sql_cmd.Parameters.Add("@pSubjectKey", SqlDbType.NVarChar).Value = "FutureGifts.ExtraPointsPct"

      _data = _sql_cmd.ExecuteScalar()

      If Not IsNothing(_data) AndAlso IsNumeric(_data) Then
        _extra_points = CType(_data, Integer)
      Else
        _extra_points = Nothing
      End If

      ' Min gifts to show
      _sql_cmd.Parameters.Clear()
      _sql_cmd.Parameters.Add("@pGroupKey", SqlDbType.NVarChar).Value = PROMOBOX_GP_GROUP_KEY
      _sql_cmd.Parameters.Add("@pSubjectKey", SqlDbType.NVarChar).Value = "FutureGifts.MinCount"

      _data = _sql_cmd.ExecuteScalar()

      If Not IsNothing(_data) AndAlso IsNumeric(_data) Then
        _min_count = CType(_data, Integer)
      Else
        _min_count = Nothing
      End If

      ' Max gifts to show
      _sql_cmd.Parameters.Clear()
      _sql_cmd.Parameters.Add("@pGroupKey", SqlDbType.NVarChar).Value = PROMOBOX_GP_GROUP_KEY
      _sql_cmd.Parameters.Add("@pSubjectKey", SqlDbType.NVarChar).Value = "FutureGifts.MaxCount"

      _data = _sql_cmd.ExecuteScalar()

      If Not IsNothing(_data) AndAlso IsNumeric(_data) Then
        _max_count = CType(_data, Integer)
      Else
        _max_count = Nothing
      End If

      ' Split gifts enabled
      _sql_cmd.Parameters.Clear()
      _sql_cmd.Parameters.Add("@pGroupKey", SqlDbType.NVarChar).Value = PROMOBOX_GP_GROUP_KEY
      _sql_cmd.Parameters.Add("@pSubjectKey", SqlDbType.NVarChar).Value = "Gifts.SplitCategories"

      _data = _sql_cmd.ExecuteScalar()

      If Not IsNothing(_data) Then
        _gifts_split_enabled = CType(_data, Boolean)
      End If

      ' Split gifts - Category 1 name
      _sql_cmd.Parameters.Clear()
      _sql_cmd.Parameters.Add("@pGroupKey", SqlDbType.NVarChar).Value = PROMOBOX_GP_GROUP_KEY
      _sql_cmd.Parameters.Add("@pSubjectKey", SqlDbType.NVarChar).Value = "Gifts.CategoryName01"

      _data = _sql_cmd.ExecuteScalar()

      If Not IsNothing(_data) Then
        _category_1_name = CType(_data, String)
      End If

      ' Split gifts - Category 1 short
      _sql_cmd.Parameters.Clear()
      _sql_cmd.Parameters.Add("@pGroupKey", SqlDbType.NVarChar).Value = PROMOBOX_GP_GROUP_KEY
      _sql_cmd.Parameters.Add("@pSubjectKey", SqlDbType.NVarChar).Value = "Gifts.CategoryShort01"

      _data = _sql_cmd.ExecuteScalar()

      If Not IsNothing(_data) Then
        _category_1_short = CType(_data, String)
      End If

      ' Split gifts - Category 2 name
      _sql_cmd.Parameters.Clear()
      _sql_cmd.Parameters.Add("@pGroupKey", SqlDbType.NVarChar).Value = PROMOBOX_GP_GROUP_KEY
      _sql_cmd.Parameters.Add("@pSubjectKey", SqlDbType.NVarChar).Value = "Gifts.CategoryName02"

      _data = _sql_cmd.ExecuteScalar()

      If Not IsNothing(_data) Then
        _category_2_name = CType(_data, String)
      End If

      ' Split gifts - Category 2 short
      _sql_cmd.Parameters.Clear()
      _sql_cmd.Parameters.Add("@pGroupKey", SqlDbType.NVarChar).Value = PROMOBOX_GP_GROUP_KEY
      _sql_cmd.Parameters.Add("@pSubjectKey", SqlDbType.NVarChar).Value = "Gifts.CategoryShort02"

      _data = _sql_cmd.ExecuteScalar()

      If Not IsNothing(_data) Then
        _category_2_short = CType(_data, String)
      End If

      ' Split gifts -  Category 3 name
      _sql_cmd.Parameters.Clear()
      _sql_cmd.Parameters.Add("@pGroupKey", SqlDbType.NVarChar).Value = PROMOBOX_GP_GROUP_KEY
      _sql_cmd.Parameters.Add("@pSubjectKey", SqlDbType.NVarChar).Value = "Gifts.CategoryName03"

      _data = _sql_cmd.ExecuteScalar()

      If Not IsNothing(_data) Then
        _category_3_name = CType(_data, String)
      End If

      ' Split gifts - Category 3 name
      _sql_cmd.Parameters.Clear()
      _sql_cmd.Parameters.Add("@pGroupKey", SqlDbType.NVarChar).Value = PROMOBOX_GP_GROUP_KEY
      _sql_cmd.Parameters.Add("@pSubjectKey", SqlDbType.NVarChar).Value = "Gifts.CategoryShort03"

      _data = _sql_cmd.ExecuteScalar()

      If Not IsNothing(_data) Then
        _category_3_short = CType(_data, String)
      End If

    Catch ex As Exception

    Finally
      GUI_EndSQLTransaction(_sql_transaction, True)
    End Try

    _target.next_gifts_config.percentage = _extra_points
    _target.next_gifts_config.min_to_show = _min_count
    _target.next_gifts_config.max_to_show = _max_count

    _target.gifts_split.enabled = _gifts_split_enabled
    _target.gifts_split.category_1_name = _category_1_name
    _target.gifts_split.category_1_short = _category_1_short
    _target.gifts_split.category_2_name = _category_2_name
    _target.gifts_split.category_2_short = _category_2_short
    _target.gifts_split.category_3_name = _category_3_name
    _target.gifts_split.category_3_short = _category_3_short

    Return _target

  End Function 'GetGiftConfiguration


  ' PURPOSE: GetFunctionalities
  '                  
  '  PARAMS:         
  '     - INPUT:     
  '           - None
  '     - OUTPUT:    
  '           - None
  '                  
  ' RETURNS:         
  '     - List(Of TYPE_FUNCTIONALITIES)
  '                  
  Private Function GetFunctionalities() As List(Of TYPE_FUNCTIONALITY)

    Dim _list As List(Of TYPE_FUNCTIONALITY)
    Dim _datatable As DataTable
    Dim _sql_command As SqlCommand
    Dim _ob As TYPE_FUNCTIONALITY
    Dim _str_bld As StringBuilder

    _str_bld = New StringBuilder()
    _list = New List(Of TYPE_FUNCTIONALITY)
    _datatable = New DataTable

    _str_bld.AppendLine("SELECT   FUN_FUNCTION_ID    ")
    _str_bld.AppendLine("       , FUN_NAME           ")
    _str_bld.AppendLine("       , FUN_ENABLED        ")
    _str_bld.AppendLine("  FROM   WKT_FUNCTIONALITIES")

    _sql_command = New SqlCommand(_str_bld.ToString())
    _datatable = GUI_GetTableUsingCommand(_sql_command, 5000)

    For Each _row As DataRow In _datatable.Rows

      _ob = New TYPE_FUNCTIONALITY
      _ob.function_id = _row("fun_function_id")
      _ob.enabled = _row("fun_enabled")
      _list.Add(_ob)

    Next

    Return _list

  End Function 'GetFunctionalities


  ' PURPOSE: GetFieldsInfo
  '                  
  '  PARAMS:         
  '     - INPUT:     
  '           - None
  '     - OUTPUT:    
  '           - None
  '                  
  ' RETURNS:         
  '     - List(Of TYPE_FIELD_INFO)
  '                  
  Private Function GetFieldsInfo() As List(Of TYPE_FIELD_INFO)

    Dim _list As List(Of TYPE_FIELD_INFO)
    Dim _datatable As DataTable
    Dim _sql_command As SqlCommand
    Dim _ob As TYPE_FIELD_INFO
    Dim _str_bld As StringBuilder

    _str_bld = New StringBuilder()
    _list = New List(Of TYPE_FIELD_INFO)
    _datatable = New DataTable

    _str_bld.AppendLine("  SELECT   PIF_FIELD_ID                  ")
    _str_bld.AppendLine("         , PIF_NAME                      ")
    _str_bld.AppendLine("         , PIF_SHOWN                     ")
    _str_bld.AppendLine("         , PIF_USER_ALLOW_EDIT           ")
    _str_bld.AppendLine("         , PIF_EDITABLE                  ")
    _str_bld.AppendLine("         , PIF_TYPE                      ")
    _str_bld.AppendLine("         , PIF_MIN_LENGTH                ")
    _str_bld.AppendLine("         , PIF_MAX_LENGTH                ")
    _str_bld.AppendLine("         , PIF_ORDER                     ")
    _str_bld.AppendLine("    FROM   WKT_PLAYER_INFO_FIELDS        ")
    _str_bld.AppendLine("ORDER BY   PIF_ORDER ASC,PIF_FIELD_ID ASC")

    _sql_command = New SqlCommand(_str_bld.ToString())

    _datatable = GUI_GetTableUsingCommand(_sql_command, 5000)

    For Each _row As DataRow In _datatable.Rows

      _ob = New TYPE_FIELD_INFO()
      _ob.field_id = _row("PIF_FIELD_ID")
      _ob.shown = _row("PIF_SHOWN")
      _ob.user_editable = _row("PIF_USER_ALLOW_EDIT")
      _ob.system_editable = _row("PIF_EDITABLE")
      _ob.type = _row("PIF_TYPE")
      _ob.min_length = _row("PIF_MIN_LENGTH")
      _ob.max_length = _row("PIF_MAX_LENGTH")
      _ob.order = _row("PIF_ORDER")
      _list.Add(_ob)
    Next

    Return _list

  End Function 'GetFieldsInfo


  ' PURPOSE: GetImages
  '                  
  '  PARAMS:         
  '     - INPUT:     
  '           - None
  '     - OUTPUT:    
  '           - None
  '                  
  ' RETURNS:         
  '     - List(Of TYPE_IMAGES)
  '                  
  Private Function GetImages() As List(Of WKT_Image)

    Dim _list As List(Of WKT_Image)
    Dim _datatable As DataTable
    Dim _sql_command As SqlCommand
    Dim _ob As WKT_Image
    Dim _str_bld As StringBuilder

    _str_bld = New StringBuilder()
    _list = New List(Of WKT_Image)
    _datatable = New DataTable

    _str_bld.AppendLine("SELECT   CIM_IMAGE_ID    ")
    _str_bld.AppendLine("       , CIM_NAME        ")
    _str_bld.AppendLine("       , CIM_RESOURCE_ID ")
    _str_bld.AppendLine("  FROM   WKT_IMAGES      ")


    _sql_command = New SqlCommand(_str_bld.ToString())

    _datatable = GUI_GetTableUsingCommand(_sql_command, 5000)

    For Each _row As DataRow In _datatable.Rows

      _ob = New WKT_Image
      _ob.image_id = _row("CIM_IMAGE_ID")
      _ob.resource_id = _row("CIM_RESOURCE_ID")

      _list.Add(_ob)
    Next

    Return _list

  End Function 'GetImages

  ' PURPOSE: GetMessages
  '                  
  '  PARAMS:         
  '     - INPUT:     
  '           - None
  '     - OUTPUT:    
  '           - None
  '                  
  ' RETURNS:         
  '     - List(Of TYPE_MESSAGE)
  '                  
  Private Function GetMessages() As List(Of TYPE_MESSAGE)

    Dim _str_bld As StringBuilder
    Dim _list As List(Of TYPE_MESSAGE)
    Dim _ob As TYPE_MESSAGE
    Dim _messages As DataTable

    _list = New List(Of TYPE_MESSAGE)

    _str_bld = New StringBuilder()

    Try
      _str_bld.AppendLine("SELECT   GP_SUBJECT_KEY                                            ")
      _str_bld.AppendLine("       , GP_KEY_VALUE                                              ")
      _str_bld.AppendLine("  FROM   GENERAL_PARAMS                                            ")
      _str_bld.AppendLine(" WHERE   GP_GROUP_KEY = '" & PROMOBOX_GP_GROUP_KEY & "'            ")
      _str_bld.AppendLine("   AND   GP_SUBJECT_KEY like '" & TYPE_MESSAGE.SUB_KEY_PREFIX & "%'")

      _messages = GUI_GetTableUsingSQL(_str_bld.ToString(), 1000)

      For Each _dr As DataRow In _messages.Rows
        _ob = New TYPE_MESSAGE()

        _ob.msg_id = CLS_ADS_CONFIGURATION.TYPE_MESSAGE.GetMsgId(_dr.Item("GP_SUBJECT_KEY"))
        _ob.text = _dr.Item("GP_KEY_VALUE")

        _list.Add(_ob)
      Next

    Catch ex As Exception

    Finally

    End Try

    Return _list

  End Function

  ' PURPOSE: GetSchedule
  '                  
  '  PARAMS:         
  '     - INPUT:     
  '           - None
  '     - OUTPUT:    
  '           - None
  '                  
  ' RETURNS:         
  '     - TYPE_SCHEDULE
  '                  
  Private Function GetSchedule() As TYPE_SCHEDULE

    Dim _str_bld As StringBuilder
    Dim _schedule As TYPE_SCHEDULE
    Dim _obj As Object
    Dim _from_time As String
    Dim _to_time As String

    _str_bld = New StringBuilder()
    _schedule = New TYPE_SCHEDULE

    _from_time = ""
    _to_time = ""

    Try

      _str_bld.AppendLine("SELECT   GP_KEY_VALUE              ")
      _str_bld.AppendLine("  FROM   GENERAL_PARAMS            ")
      _str_bld.AppendLine(" WHERE   GP_GROUP_KEY = @pGroup    ")
      _str_bld.AppendLine("   AND   GP_SUBJECT_KEY = @pSubject")

      Using _db_trx As New DB_TRX()
        Using _cmd As SqlCommand = New SqlCommand(_str_bld.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          _cmd.Parameters.Add("@pGroup", SqlDbType.NVarChar).Value = PROMOBOX_GP_GROUP_KEY
          _cmd.Parameters.Add("@pSubject", SqlDbType.NVarChar).Value = TYPE_SCHEDULE.SUB_KEY_FROM

          _obj = _cmd.ExecuteScalar()

          If _obj IsNot Nothing AndAlso _obj IsNot DBNull.Value Then
            _from_time = CStr(_obj)
          End If

          Call _cmd.Parameters.Clear()

          _cmd.Parameters.Add("@pGroup", SqlDbType.NVarChar).Value = PROMOBOX_GP_GROUP_KEY
          _cmd.Parameters.Add("@pSubject", SqlDbType.NVarChar).Value = TYPE_SCHEDULE.SUB_KEY_TO

          _obj = _cmd.ExecuteScalar()

          If _obj IsNot Nothing AndAlso _obj IsNot DBNull.Value Then
            _to_time = CStr(_obj)
          End If

        End Using
      End Using

      If _from_time.Length = 4 Then
        _schedule.from_time = _from_time.Substring(0, 2) + ":" + _from_time.Substring(2, 2)
      End If

      If _to_time.Length = 4 Then
        _schedule.to_time = _to_time.Substring(0, 2) + ":" + _to_time.Substring(2, 2)
      End If

    Finally

    End Try

    Return _schedule
  End Function

  ' PURPOSE: GetRechargesVoucherConf
  '                  
  '  PARAMS:         
  '     - INPUT:     
  '           - None
  '     - OUTPUT:    
  '           - None
  '                  
  ' RETURNS:         
  '     - TYPE_RECHARGES_VOUCHER_CONF
  '                  
  Private Function GetRechargesVoucherConf() As TYPE_RECHARGES_VOUCHER_CONF
    Dim _str_bld As StringBuilder
    Dim _voucher_conf As TYPE_RECHARGES_VOUCHER_CONF
    Dim _obj As Object
    Dim _header As String
    Dim _footer As String

    _str_bld = New StringBuilder()
    _voucher_conf = New TYPE_RECHARGES_VOUCHER_CONF

    _header = ""
    _footer = ""

    Try

      _str_bld.AppendLine("SELECT   GP_KEY_VALUE              ")
      _str_bld.AppendLine("  FROM   GENERAL_PARAMS            ")
      _str_bld.AppendLine(" WHERE   GP_GROUP_KEY = @pGroup    ")
      _str_bld.AppendLine("   AND   GP_SUBJECT_KEY = @pSubject")

      Using _db_trx As New DB_TRX()
        Using _cmd As SqlCommand = New SqlCommand(_str_bld.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          _cmd.Parameters.Add("@pGroup", SqlDbType.NVarChar).Value = PROMOBOX_GP_GROUP_KEY
          _cmd.Parameters.Add("@pSubject", SqlDbType.NVarChar).Value = TYPE_RECHARGES_VOUCHER_CONF.SUB_KEY_HEADER

          _obj = _cmd.ExecuteScalar()

          If _obj IsNot Nothing AndAlso _obj IsNot DBNull.Value Then
            _header = CStr(_obj)
          End If

          Call _cmd.Parameters.Clear()

          _cmd.Parameters.Add("@pGroup", SqlDbType.NVarChar).Value = PROMOBOX_GP_GROUP_KEY
          _cmd.Parameters.Add("@pSubject", SqlDbType.NVarChar).Value = TYPE_RECHARGES_VOUCHER_CONF.SUB_KEY_FOOTER

          _obj = _cmd.ExecuteScalar()

          If _obj IsNot Nothing AndAlso _obj IsNot DBNull.Value Then
            _footer = CStr(_obj)
          End If

        End Using
      End Using

      _voucher_conf.header = _header
      _voucher_conf.footer = _footer

    Finally

    End Try

    Return _voucher_conf

  End Function

#End Region

#End Region

#Region " Public functions"

  ' PURPOSE: New
  '                  
  '  PARAMS:         
  '     - INPUT:     
  '           - None
  '     - OUTPUT:    
  '           - None
  '                  
  ' RETURNS:         
  '     -None
  '                  
  Public Sub New()
    Me.m_ads_configuration = New TYPE_ADS_CONFIGURATION()
  End Sub 'New

#End Region

End Class