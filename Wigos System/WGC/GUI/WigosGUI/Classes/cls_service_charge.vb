'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : cls_service_charge.vb
'
' DESCRIPTION : 
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 13-SEP-2012  HBB    Initial version
' 08-JUL-2013  QMP    Fixed Bug #893: Service charge decimal numbers are not correctly stored on DB
' 28-OCT-2013  ACM    Fixed Bug #WIG-322 Wrong auditor code.
' 22-SEP-2016  EOR    PBI 17963: Televisa - Service Charge: Modifications Voucher / Report / Settings
' 07-OCT-2016  EOR    Fixed Bug 18719: Audit: Service Charge: Change Attributed Company text string not found
' 14-OCT-2016  RAB    PBI 18098: General params: Automatically add the GP to the system
'--------------------------------------------------------------------
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports System.Text
Imports WSI.Common

Public Class SERVICE_CHARGE
  Inherits CLASS_BASE

#Region "Structure"
  Public Class TYPE_SERVICE_CHARGE

    Public service_charge_enabled As Boolean
    Public concept As String
    Public percent_over_lower_devolution As Decimal
    Public percent_over_greater_devlution As Decimal
    Public percent_over_award As Decimal
    Public calculation_of_award As Integer
    Public amount_attributed_company As Char    'EOR 22-SEP-2016

  End Class
#End Region

#Region " Members "

  Protected m_service_charge As New TYPE_SERVICE_CHARGE()

#End Region

#Region "Properties"

  Public Property ServiceChargeEnabled() As Boolean
    Get
      Return m_service_charge.service_charge_enabled
    End Get
    Set(ByVal Value As Boolean)
      m_service_charge.service_charge_enabled = Value
    End Set
  End Property

  Public Property Concept() As String
    Get
      Return m_service_charge.concept
    End Get
    Set(ByVal Value As String)
      m_service_charge.concept = Value
    End Set
  End Property

  Public Property PercentOverLowerDevolution() As Decimal
    Get
      Return m_service_charge.percent_over_lower_devolution
    End Get
    Set(ByVal Value As Decimal)
      m_service_charge.percent_over_lower_devolution = Value
    End Set
  End Property

  Public Property PercentOverGreaterDevolution() As Decimal
    Get
      Return m_service_charge.percent_over_greater_devlution
    End Get
    Set(ByVal Value As Decimal)
      m_service_charge.percent_over_greater_devlution = Value
    End Set
  End Property

  Public Property PercentOverAward() As Decimal
    Get
      Return m_service_charge.percent_over_award
    End Get
    Set(ByVal Value As Decimal)
      m_service_charge.percent_over_award = Value
    End Set
  End Property

  Public Property CalculationOfAward() As Integer
    Get
      Return m_service_charge.calculation_of_award
    End Get
    Set(ByVal Value As Integer)
      m_service_charge.calculation_of_award = Value
    End Set
  End Property

  'EOR 22-SEP-2016
  Public Property AmountAttributedCompany() As Char
    Get
      Return m_service_charge.amount_attributed_company
    End Get
    Set(ByVal Value As Char)
      m_service_charge.amount_attributed_company = Value
    End Set
  End Property

#End Region

#Region "Overrides"

  '----------------------------------------------------------------------------
  ' PURPOSE: Returns the related auditor data for the current object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA
    Dim auditor_data As CLASS_AUDITOR_DATA

    auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_CASHIER_CONFIGURATION)
    auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(1294), "")

    Call auditor_data.SetField(0, IIf(Me.ServiceChargeEnabled, 1, 0), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1295))
    Call auditor_data.SetField(0, Me.Concept, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1296))
    Call auditor_data.SetField(0, GUI_FormatNumber(Me.PercentOverLowerDevolution, 2), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1304) & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1297))
    Call auditor_data.SetField(0, GUI_FormatNumber(Me.PercentOverGreaterDevolution, 2), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1303) & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1297))
    Call auditor_data.SetField(0, GUI_FormatNumber(Me.PercentOverAward, 2), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1303) & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1298))
    Call auditor_data.SetField(0, Me.CalculationOfAward, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1316))
    Call auditor_data.SetField(0, Me.AmountAttributedCompany, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7623) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1285).ToLower())  'EOR 22-SEP-2016

    Return auditor_data

  End Function

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
  End Function

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE: Reads from the database into the given object
  '
  ' PARAMS:
  '   - INPUT:
  '     - ObjectId: An object, it must be 'casted' to the desired KEY.
  '
  '   - OUTPUT: ENUM_STATUS
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim rc As Integer

    rc = Service_Charge_DbRead(SqlCtx)

    Select Case rc
      Case ENUM_DB_RC.DB_RC_OK
        Return ENUM_STATUS.STATUS_OK

      Case ENUM_DB_RC.DB_RC_ERROR_NOT_FOUND
        Return CLASS_BASE.ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim rc As Integer

    rc = Service_Charge_DbUpdate()

    Select Case rc
      Case ENUM_DB_RC.DB_RC_OK
        Return ENUM_STATUS.STATUS_OK

      Case ENUM_DB_RC.DB_RC_ERROR_NOT_FOUND
        Return CLASS_BASE.ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR
    End Select

  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE: Returns a duplicate of the given object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim _temp_service_charge As SERVICE_CHARGE

    _temp_service_charge = New SERVICE_CHARGE()

    _temp_service_charge.Concept = Me.Concept
    _temp_service_charge.PercentOverAward = Me.PercentOverAward
    _temp_service_charge.PercentOverGreaterDevolution = Me.PercentOverGreaterDevolution
    _temp_service_charge.PercentOverLowerDevolution = Me.PercentOverLowerDevolution
    _temp_service_charge.ServiceChargeEnabled = Me.ServiceChargeEnabled
    _temp_service_charge.CalculationOfAward = Me.CalculationOfAward
    _temp_service_charge.AmountAttributedCompany = Me.AmountAttributedCompany   'EOR 22-SEP-2016

    Return _temp_service_charge
  End Function

#End Region 'Override

#Region "Private methods"

  Private Function General_Param_DbRead(ByVal Key As String, _
                                                ByVal SubKey As String, ByRef Value As String, _
                                                ByVal Trx As SqlTransaction) As Boolean
    Dim _sb As StringBuilder
    Dim _obj As Object

    Value = ""

    Try
      _sb = New StringBuilder()
      _sb.AppendLine("SELECT   GP_KEY_VALUE ")
      _sb.AppendLine("  FROM   GENERAL_PARAMS ")
      _sb.AppendLine(" WHERE   GP_GROUP_KEY   = @pGroup")
      _sb.AppendLine("   AND   GP_SUBJECT_KEY = @pSubject")

      Using _cmd As SqlCommand = New SqlCommand(_sb.ToString(), Trx.Connection, Trx)
        _cmd.Parameters.Add("@pGroup", SqlDbType.NVarChar).Value = Key
        _cmd.Parameters.Add("@pSubject", SqlDbType.NVarChar).Value = SubKey
        _obj = _cmd.ExecuteScalar()

        If _obj IsNot Nothing AndAlso _obj IsNot DBNull.Value Then
          Value = CStr(_obj)
        End If

        Return True
      End Using
    Catch
    End Try

    Return False
  End Function

  Private Function Service_Charge_DbRead(ByVal Context As Integer) As Integer

    Dim _value As String

    Dim _general_param As GeneralParam.Dictionary

    _value = ""
    Try
      _general_param = GeneralParam.Dictionary.Create()
      Me.ServiceChargeEnabled = _general_param.GetBoolean("Cashier.ServiceCharge", "Enabled", False)
      Me.CalculationOfAward = _general_param.GetInt32("Cashier.ServiceCharge", "Mode", 1)
      Me.Concept = _general_param.GetString("Cashier.ServiceCharge", "Text")
      Me.PercentOverLowerDevolution = _general_param.GetDecimal("Cashier.ServiceCharge", "OnLoser.RefundPct", -1)
      Me.PercentOverGreaterDevolution = _general_param.GetDecimal("Cashier.ServiceCharge", "OnWinner.RefundPct", -1)
      Me.PercentOverAward = _general_param.GetDecimal("Cashier.ServiceCharge", "OnWinner.PrizePct", -1)
      Me.AmountAttributedCompany = _general_param.GetString("Cashier.ServiceCharge", "AttributedToCompany", "B")    'EOR 22-SEP-2016

      Return ENUM_DB_RC.DB_RC_OK

    Catch
      Return ENUM_DB_RC.DB_RC_ERROR_DB
    End Try

  End Function

  Private Function Service_Charge_DbUpdate() As Integer

    Try
      Using _db_trx As New DB_TRX()

        Dim _gp_update_list As New List(Of String())

        _gp_update_list.Add(GeneralParam.AddToList("Cashier.ServiceCharge", "Enabled", IIf(Me.ServiceChargeEnabled, 1, 0)))
        _gp_update_list.Add(GeneralParam.AddToList("Cashier.ServiceCharge", "Text", Me.Concept))
        _gp_update_list.Add(GeneralParam.AddToList("Cashier.ServiceCharge", "OnLoser.RefundPct", GUI_LocalNumberToDBNumber(Me.PercentOverLowerDevolution)))
        _gp_update_list.Add(GeneralParam.AddToList("Cashier.ServiceCharge", "OnWinner.RefundPct", GUI_LocalNumberToDBNumber(Me.PercentOverGreaterDevolution)))
        _gp_update_list.Add(GeneralParam.AddToList("Cashier.ServiceCharge", "OnWinner.PrizePct", GUI_LocalNumberToDBNumber(Me.PercentOverAward)))
        _gp_update_list.Add(GeneralParam.AddToList("Cashier.ServiceCharge", "Mode", Me.CalculationOfAward))
        'EOR 22-SEP-2016
        _gp_update_list.Add(GeneralParam.AddToList("Cashier.ServiceCharge", "AttributedToCompany", Me.AmountAttributedCompany))



        For Each _general_param As String() In _gp_update_list
          If (Not GUI_Controls.DB_GeneralParam_Update(_general_param(GeneralParam.COLUMN_NAME.Group),
                                                      _general_param(GeneralParam.COLUMN_NAME.Subject),
                                                      _general_param(GeneralParam.COLUMN_NAME.Value),
                                                      _db_trx.SqlTransaction)) Then


            Return ENUM_DB_RC.DB_RC_ERROR_DB
          End If
        Next

        _db_trx.SqlTransaction.Commit()
        Return ENUM_DB_RC.DB_RC_OK
      End Using
    Catch
    End Try
  End Function

#End Region
End Class
