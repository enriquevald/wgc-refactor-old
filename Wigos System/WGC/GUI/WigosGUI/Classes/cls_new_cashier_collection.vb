'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : cls_new_cashier_collection.vb
'
' DESCRIPTION : gets or sets cashier money collections
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 27-NOV-2013  DRV    Initial version   
'--------------------------------------------------------------------
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports System.Text
Imports WSI.Common


Public Class NEW_CASHIER_COLLECTION
  Inherits CLASS_BASE

  Public Class TYPE_MONEY_COLLECTION_DETAILS

    Public ticket_validation_number As Int64
    Public ticket_validation_type As Int32
    Public ticket_amount As Double
    Public money_collection_id As Int64
    Public matched As Boolean               ' theoretical match with real
    Public ticket_collected As Boolean
  End Class


  Public Class TYPE_CASHIER_COLLECTION

    ' Table variables 
    Public money_collection_id As Int64
    Public user_id As Int32
    Public cashier_session_id As Int32
    Public cashier_terminal_id As Int64
    Public notes As String
    Public collection_date As New TYPE_DATE_TIME
    Public collection_status As Int32
    Public balanced As Boolean
    Public cashier_terminal_name As String
    Public user_name As String
    Public real_ticket_count As Integer
    Public real_ticket_sum As Double
    Public theoretical_ticket_count As Integer
    Public theoretical_ticket_sum As Double
    Public collection_details As List(Of TYPE_MONEY_COLLECTION_DETAILS)
    Public already_collected As Boolean

  End Class

#Region " Constants "

  Const SQL_USER_NAME As Int32 = 0
  Const SQL_CASHIER_TERMINAL_NAME As Int32 = 1
  Const SQL_THEO_TICKET_SUM As Int32 = 2
  Const SQL_REAL_TICKET_SUM As Int32 = 3
  Const SQL_MONEY_COLLECTION_ID As Int32 = 4
  Const SQL_TERMINAL_ID As Int32 = 5
  Const SQL_USER_ID As Int32 = 6
  Const SQL_NOTES As Int32 = 7
  Const SQL_COLLECTION_DATETIME As Int32 = 8
  Const SQL_COLLECTION_STATUS As Int32 = 9
  Const SQL_REAL_TICKET_COUNT As Int32 = 10

  Const SQL_COLUMN_DETAILS_MONEY_COLLECTION_ID As Int32 = 0
  Const SQL_COLUMN_DETAILS_AMOUNT As Int32 = 1
  Const SQL_COLUMN_DETAILS_VALIDATION_NUMBER As Int32 = 2
  Const SQL_COLUMN_DETAILS_COLLECTED As Int32 = 3
  Const SQL_COLUMN_DETAILS_VALIDATION_TYPE As Int32 = 4
#End Region

#Region " Members "

  Protected m_cashier_collection As New TYPE_CASHIER_COLLECTION()
  Protected m_is_new_collection As Boolean

#End Region

#Region "Properties"

  Public Property MoneyCollectionId() As Int64
    Get
      Return m_cashier_collection.money_collection_id
    End Get
    Set(ByVal value As Int64)
      m_cashier_collection.money_collection_id = value
    End Set
  End Property

  Public Property UserId() As Int32
    Get
      Return m_cashier_collection.user_id
    End Get
    Set(ByVal value As Int32)
      m_cashier_collection.user_id = value
    End Set
  End Property

  Public Property CashierSessionId() As Int32
    Get
      Return m_cashier_collection.cashier_session_id
    End Get
    Set(ByVal value As Int32)
      m_cashier_collection.cashier_session_id = value
    End Set
  End Property

  Public Property CashierTerminalId() As Int64
    Get
      Return m_cashier_collection.cashier_terminal_id
    End Get
    Set(ByVal value As Int64)
      m_cashier_collection.cashier_terminal_id = value
    End Set
  End Property

  Public Property Notes() As String
    Get
      Return m_cashier_collection.notes
    End Get
    Set(ByVal value As String)
      m_cashier_collection.notes = value
    End Set
  End Property

  Public Property CollectionDate() As TYPE_DATE_TIME
    Get
      Return m_cashier_collection.collection_date
    End Get
    Set(ByVal value As TYPE_DATE_TIME)
      m_cashier_collection.collection_date = value
    End Set
  End Property

  Public Property Status() As Int32
    Get
      Return m_cashier_collection.collection_status
    End Get
    Set(ByVal value As Int32)
      m_cashier_collection.collection_status = value
    End Set
  End Property

  Public Property Balanced() As Boolean
    Get
      Return m_cashier_collection.balanced
    End Get
    Set(ByVal value As Boolean)
      m_cashier_collection.balanced = value
    End Set
  End Property

  Public Property CashierName() As String
    Get
      Return m_cashier_collection.cashier_terminal_name
    End Get
    Set(ByVal value As String)
      m_cashier_collection.cashier_terminal_name = value
    End Set
  End Property

  Public Property UserName() As String
    Get
      Return m_cashier_collection.user_name
    End Get
    Set(ByVal value As String)
      m_cashier_collection.user_name = value
    End Set
  End Property

  Public Property RealTicketCount() As Integer
    Get
      Return m_cashier_collection.real_ticket_count
    End Get
    Set(ByVal value As Integer)
      m_cashier_collection.real_ticket_count = value
    End Set
  End Property

  Public Property RealTicketSum() As Double
    Get
      Return m_cashier_collection.real_ticket_sum
    End Get
    Set(ByVal value As Double)
      m_cashier_collection.real_ticket_sum = value
    End Set
  End Property

  Public Property TheoreticalTicketCount() As Integer
    Get
      Return m_cashier_collection.theoretical_ticket_count
    End Get
    Set(ByVal value As Integer)
      m_cashier_collection.theoretical_ticket_count = value
    End Set
  End Property

  Public Property TheoreticalTicketSum() As Double
    Get
      Return m_cashier_collection.theoretical_ticket_sum
    End Get
    Set(ByVal value As Double)
      m_cashier_collection.theoretical_ticket_sum = value
    End Set
  End Property

  Public Property CollectionDetails() As List(Of TYPE_MONEY_COLLECTION_DETAILS)
    Get
      Return m_cashier_collection.collection_details
    End Get
    Set(ByVal value As List(Of TYPE_MONEY_COLLECTION_DETAILS))
      m_cashier_collection.collection_details = value
    End Set
  End Property

  Public Property AlreadyCollected() As Boolean
    Get
      Return m_cashier_collection.already_collected
    End Get
    Set(ByVal value As Boolean)
      m_cashier_collection.already_collected = value
    End Set
  End Property

  Public Property IsNewCollection() As Boolean
    Get
      Return m_is_new_collection
    End Get
    Set(ByVal value As Boolean)
      m_is_new_collection = value
    End Set
  End Property
#End Region

#Region "Overrides Functions"

  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim auditor_data As CLASS_AUDITOR_DATA


    auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_ACCEPTORS)
    auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(2222), "")

    Call auditor_data.SetField(0, Me.RealTicketCount.ToString(), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2842))
    Call auditor_data.SetField(0, GUI_FormatCurrency(Me.RealTicketSum.ToString()), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2783))

    If Not Me.AlreadyCollected Then
      If Me.CollectionDate.IsNull Then
        Call auditor_data.SetField(0, "---", GLB_NLS_GUI_PLAYER_TRACKING.GetString(3009))
      Else
        Call auditor_data.SetField(0, Me.CollectionDate.Value.ToString(), GLB_NLS_GUI_PLAYER_TRACKING.GetString(3009))
      End If
    End If
    Return auditor_data
  End Function

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

  End Function

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

  End Function ' DB_Insert

  '----------------------------------------------------------------------------
  ' PURPOSE: Reads from the database into the given object
  '
  ' PARAMS:
  '   - INPUT:
  '     - ObjectId: An object, it must be 'casted' to the desired KEY.
  '
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim rc As ENUM_CONFIGURATION_RC

    Me.m_cashier_collection.cashier_session_id = ObjectId

    ' Read cashier collection data

    rc = ReadCashierCollection()

    Select Case rc

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function 'DB_Read

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim rc As ENUM_CONFIGURATION_RC

    ' Update draw data
    rc = UpdateCashierCollection()

    Select Case rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
        Return ENUM_STATUS.STATUS_DUPLICATE_KEY

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select
  End Function

  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim _temp_type_new_collection As New NEW_CASHIER_COLLECTION()

    _temp_type_new_collection.MoneyCollectionId = Me.MoneyCollectionId
    _temp_type_new_collection.UserId = Me.UserId
    _temp_type_new_collection.CashierSessionId = Me.CashierSessionId
    _temp_type_new_collection.Notes = Me.Notes
    If Not Me.CollectionDate.IsNull Then
      _temp_type_new_collection.CollectionDate.Value = Me.CollectionDate.Value
    Else
      _temp_type_new_collection.CollectionDate = New TYPE_DATE_TIME()
    End If
    _temp_type_new_collection.Status = Me.Status
    _temp_type_new_collection.Balanced = Me.Balanced
    _temp_type_new_collection.CashierName = Me.CashierName
    _temp_type_new_collection.UserName = Me.UserName
    _temp_type_new_collection.RealTicketCount = Me.RealTicketCount
    _temp_type_new_collection.RealTicketSum = Me.RealTicketSum
    _temp_type_new_collection.TheoreticalTicketCount = Me.TheoreticalTicketCount
    _temp_type_new_collection.TheoreticalTicketSum = Me.TheoreticalTicketSum
    _temp_type_new_collection.CollectionDetails = Me.CollectionDetails
    _temp_type_new_collection.AlreadyCollected = Me.AlreadyCollected
    _temp_type_new_collection.m_is_new_collection = Me.m_is_new_collection

    Return _temp_type_new_collection

  End Function

#End Region

#Region "Private functions"

  Private Function ReadCashierCollection() As ENUM_CONFIGURATION_RC
    Return ReadTheoreticalCashierCollection()
  End Function

  Private Function ReadTheoreticalCashierCollection() As ENUM_CONFIGURATION_RC
    Dim _sb As StringBuilder
    Dim _table As DataTable

    _sb = New StringBuilder()
    _table = New DataTable()

    _sb.AppendLine("   SELECT   GU_USERNAME                                                        ")
    _sb.AppendLine("          , CT_NAME                                                            ")
    _sb.AppendLine("          , ISNULL(ST_TABLE.TI_TICKETS_SUM, 0) AS THEO_TICKET_SUM              ")
    _sb.AppendLine("          , ISNULL(TI_TABLE.TI_TICKET_SUM, 0)  AS REAL_TICKET_SUM              ")
    _sb.AppendLine("          , MC_COLLECTION_ID                                                   ")
    _sb.AppendLine("          , MC_TERMINAL_ID                                                     ")
    _sb.AppendLine("          , MC_USER_ID                                                         ")
    _sb.AppendLine("          , MC_NOTES                                                           ")
    _sb.AppendLine("          , MC_COLLECTION_DATETIME                                             ")
    _sb.AppendLine("          , MC_STATUS                                                          ")
    _sb.AppendLine("          , TI_TABLE.TI_TICKET_COUNT                                           ")
    _sb.AppendLine("     FROM   MONEY_COLLECTIONS                                                  ")
    _sb.AppendLine("LEFT JOIN   GUI_USERS ON GU_USER_ID = MC_USER_ID                               ")
    _sb.AppendLine("LEFT JOIN   CASHIER_TERMINALS ON CT_CASHIER_ID = MC_CASHIER_ID                 ")
    '-- SUM OF TICKETS COLLECTED ACCORDING TO SYSTEM
    _sb.AppendLine("LEFT JOIN (  SELECT   SUM(TI_AMOUNT) AS TI_TICKETS_SUM                         ")
    _sb.AppendLine("                    , TI_MONEY_COLLECTION_ID                                   ")
    _sb.AppendLine("               FROM   TICKETS                                                  ")
    _sb.AppendLine("           GROUP BY   TICKETS.TI_MONEY_COLLECTION_ID                           ")
    _sb.AppendLine("          )ST_TABLE                                                            ")
    _sb.AppendLine("      ON   ST_TABLE.TI_MONEY_COLLECTION_ID = MC_COLLECTION_ID                  ")
    ' -- NUMBER AND SUM OF TICKETS COLLECTED ACCORDING TO USER
    _sb.AppendLine("LEFT JOIN (  SELECT   SUM(TI_AMOUNT) AS TI_TICKET_SUM                          ")
    _sb.AppendLine("                    , TI_MONEY_COLLECTION_ID                                   ")
    _sb.AppendLine("                    , COUNT(*) AS TI_TICKET_COUNT                              ")
    _sb.AppendLine("               FROM   TICKETS WITH(INDEX(IX_ti_collected_money_collection_id)) ")
    _sb.AppendLine("              WHERE   TICKETS.TI_COLLECTED = 1                                 ")
    _sb.AppendLine("           GROUP BY   TICKETS.TI_MONEY_COLLECTION_ID                           ")
    _sb.AppendLine("         ) TI_TABLE                                                            ")
    _sb.AppendLine("       ON  TI_TABLE.TI_MONEY_COLLECTION_ID = MC_COLLECTION_ID                  ")
    _sb.AppendLine("    WHERE  MC_CASHIER_SESSION_ID = @pCashierSessionId                          ")

    Try

      Using _db_trx As New DB_TRX()
        Using _sql_cmd As New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          _sql_cmd.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).Value = CashierSessionId
          Using _sql_da As New SqlDataAdapter(_sql_cmd)
            _db_trx.Fill(_sql_da, _table)

          End Using
        End Using
      End Using

    Catch _ex As Exception


      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

    If _table.Rows.Count > 0 Then

      If _table.Rows(0).Item(SQL_USER_NAME) IsNot DBNull.Value Then
        Me.UserName = _table.Rows(0).Item(SQL_USER_NAME)
      Else
        Me.UserName = ""
      End If

      If _table.Rows(0).Item(SQL_THEO_TICKET_SUM) IsNot DBNull.Value Then
        Me.TheoreticalTicketSum = _table.Rows(0).Item(SQL_THEO_TICKET_SUM)
      Else
        Me.TheoreticalTicketSum = 0
      End If

      If _table.Rows(0).Item(SQL_COLLECTION_DATETIME) IsNot DBNull.Value Then
        Me.CollectionDate.Value = mdl_tito.GetFormatedDate(_table.Rows(0).Item(SQL_COLLECTION_DATETIME))
        Me.AlreadyCollected = True
      Else
        Me.AlreadyCollected = False
      End If

      If _table.Rows(0).Item(SQL_NOTES) IsNot DBNull.Value Then
        Me.Notes = _table.Rows(0).Item(SQL_NOTES)
      End If

      If _table.Rows(0).Item(SQL_REAL_TICKET_SUM) IsNot DBNull.Value Then
        Me.RealTicketSum = _table.Rows(0).Item(SQL_REAL_TICKET_SUM)
      End If

      If _table.Rows(0).Item(SQL_TERMINAL_ID) IsNot DBNull.Value Then
        Me.CashierTerminalId = _table.Rows(0).Item(SQL_TERMINAL_ID)
      End If

      If _table.Rows(0).Item(SQL_MONEY_COLLECTION_ID) IsNot DBNull.Value Then
        Me.MoneyCollectionId = _table.Rows(0).Item(SQL_MONEY_COLLECTION_ID)
      End If

      If _table.Rows(0).Item(SQL_USER_ID) IsNot DBNull.Value Then
        Me.UserId = _table.Rows(0).Item(SQL_USER_ID)
      End If

      If _table.Rows(0).Item(SQL_COLLECTION_STATUS) IsNot DBNull.Value Then
        Me.Status = _table.Rows(0).Item(SQL_COLLECTION_STATUS)
        If Me.Status = TITO_MONEY_COLLECTION_STATUS.CLOSED_BALANCED Then
          Me.Balanced = True
        Else
          Me.Balanced = False
        End If
      End If

      If _table.Rows(0).Item(SQL_CASHIER_TERMINAL_NAME) IsNot DBNull.Value Then
        Me.CashierName = _table.Rows(0).Item(SQL_CASHIER_TERMINAL_NAME)
      End If

      If _table.Rows(0).Item(SQL_REAL_TICKET_COUNT) IsNot DBNull.Value Then
        Me.RealTicketCount = _table.Rows(0).Item(SQL_REAL_TICKET_COUNT)
      End If

    Else
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
    End If

    'read collection details
    Return ReadCollectionDetails(MoneyCollectionId)

  End Function

  Private Function ReadCollectionDetails(ByVal MoneyCollectionId) As ENUM_CONFIGURATION_RC

    Dim _sb As StringBuilder
    Dim _table As DataTable
    Dim _row As DataRow

    CollectionDetails = New List(Of TYPE_MONEY_COLLECTION_DETAILS)
    _sb = New StringBuilder()
    _table = New DataTable()

    _sb.AppendLine("  SELECT   TI_MONEY_COLLECTION_ID                                       ")
    _sb.AppendLine("         , TI_AMOUNT                                                    ")
    _sb.AppendLine("         , TI_VALIDATION_NUMBER                                         ")
    _sb.AppendLine("         , TI_COLLECTED                                                 ")
    _sb.AppendLine("         , TI_VALIDATION_TYPE                                           ")
    _sb.AppendLine("    FROM   TICKETS                                                      ")
    _sb.AppendLine("   WHERE   TI_MONEY_COLLECTION_ID = @pCollectionId               ")


    Try
      Using _db_trx As New DB_TRX()
        Using _sql_cmd As New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          _sql_cmd.Parameters.Add("@pCollectionId", SqlDbType.BigInt).Value = MoneyCollectionId
          Using _sql_da As New SqlDataAdapter(_sql_cmd)
            _db_trx.Fill(_sql_da, _table)

          End Using
        End Using
      End Using

      For Each _row In _table.Rows
        FillTheoreticalDetails(_row)
      Next

    Catch _ex As Exception


      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR
    End Try


    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

  End Function

  Private Sub FillTheoreticalDetails(ByVal DbRow As DataRow)

    Dim _collection_detail As TYPE_MONEY_COLLECTION_DETAILS

    _collection_detail = New TYPE_MONEY_COLLECTION_DETAILS()

    If Not IsDBNull(DbRow.Item(SQL_COLUMN_DETAILS_VALIDATION_NUMBER)) Then
      _collection_detail.ticket_validation_number = DbRow.Item(SQL_COLUMN_DETAILS_VALIDATION_NUMBER)
    End If

    If Not IsDBNull(DbRow.Item(SQL_COLUMN_DETAILS_AMOUNT)) Then
      _collection_detail.ticket_amount = DbRow.Item(SQL_COLUMN_DETAILS_AMOUNT)
    End If

    If Not IsDBNull(DbRow.Item(SQL_COLUMN_DETAILS_COLLECTED)) Then
      _collection_detail.ticket_collected = DbRow.Item(SQL_COLUMN_DETAILS_COLLECTED)
    End If

    If Not IsDBNull(DbRow.Item(SQL_COLUMN_DETAILS_VALIDATION_TYPE)) Then
      _collection_detail.ticket_validation_type = DbRow.Item(SQL_COLUMN_DETAILS_VALIDATION_TYPE)
    End If

    Me.TheoreticalTicketCount += 1

    CollectionDetails.Add(_collection_detail)
  End Sub

  Private Function UpdateCashierCollection() As ENUM_CONFIGURATION_RC
    Dim _sb As StringBuilder
    Dim _reader As SqlDataReader

    _sb = New StringBuilder()
    _reader = Nothing

    'it is posible when the collection is been doing from the Collections Form
    'If Me.SessionId = -1 Then
    '  Me.SessionId = GetSessionIdFromStackerId(Me.StackerId)
    'End If

    _sb.AppendLine("UPDATE   MONEY_COLLECTIONS                              ")
    _sb.AppendLine("   SET   MC_USER_ID = @pUserId                          ")
    _sb.AppendLine("       , MC_COLLECTION_DATETIME = @pCollectionDateTime  ")
    _sb.AppendLine("       , MC_NOTES = @pNotes                             ")
    _sb.AppendLine("       , MC_STATUS = @pNewStatus                        ")
    _sb.AppendLine("OUTPUT   INSERTED.MC_COLLECTION_ID                      ")
    _sb.AppendLine("      ,  INSERTED.MC_CASHIER_ID                        ")
    _sb.AppendLine(" WHERE   MC_CASHIER_SESSION_ID = @pSessionId            ")
    _sb.AppendLine("   AND   MC_STATUS = @pOldStatus                        ")

    Try
      Using _db_trx As New DB_TRX()
        Using _cmd As SqlCommand = New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          _cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = Me.UserId
          _cmd.Parameters.Add("@pCollectionDateTime", SqlDbType.DateTime).Value = Me.CollectionDate.Value
          _cmd.Parameters.Add("@pNotes", SqlDbType.VarChar).Value = Me.Notes
          _cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = Me.CashierSessionId
          _cmd.Parameters.Add("@pNewStatus", SqlDbType.Int).Value = IIf(Me.Balanced, TITO_MONEY_COLLECTION_STATUS.CLOSED_BALANCED, TITO_MONEY_COLLECTION_STATUS.CLOSED_IN_IMBALANCE)
          _cmd.Parameters.Add("@pOldStatus", SqlDbType.Int).Value = TITO_MONEY_COLLECTION_STATUS.PENDING

          _reader = _cmd.ExecuteReader()

          If Not _reader.Read() _
              Or _reader.IsDBNull(0) _
              Or _reader.IsDBNull(1) Then

            Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR
          End If

          Me.m_cashier_collection.money_collection_id = _reader.GetInt64(0)
          Me.m_cashier_collection.cashier_terminal_id = _reader.GetInt32(1)

          _reader.Close()
        End Using

        If Me.m_cashier_collection.money_collection_id > 0 Then
          For Each _detail As TYPE_MONEY_COLLECTION_DETAILS In Me.CollectionDetails
            _detail.money_collection_id = Me.m_cashier_collection.money_collection_id
            If InsertStackerCollectionDetails(Me.m_cashier_collection.money_collection_id, _db_trx, _detail, True) <> ENUM_CONFIGURATION_RC.CONFIGURATION_OK Then
              Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR

            End If
          Next
        End If

        _db_trx.Commit()
        Me.AlreadyCollected = True
      End Using

    Catch _ex As Exception

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR
    Finally
      If _reader IsNot Nothing AndAlso Not _reader.IsClosed Then
        _reader.Close()
        _reader = Nothing
      End If
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
  End Function

  Private Function InsertStackerCollectionDetails(ByVal StackerCollectionId As Int64, ByVal Transaction As DB_TRX, ByVal Detail As TYPE_MONEY_COLLECTION_DETAILS, ByVal IsTicket As Boolean) As ENUM_CONFIGURATION_RC

    Dim _sb As StringBuilder

    _sb = New StringBuilder()
    Try
      Using _cmd As SqlCommand = New SqlCommand("", Transaction.SqlTransaction.Connection, Transaction.SqlTransaction)
        ' hacer el update de la tabla ticket
        _sb.AppendLine("UPDATE   TICKETS                                    ")
        _sb.AppendLine("   SET   TI_COLLECTED = @pCollected                 ")
        _sb.AppendLine(" WHERE   TI_VALIDATION_NUMBER = @pValidationNumber  ")
        _sb.AppendLine("   AND   TI_MONEY_COLLECTION_ID = @pCollectionId    ")

        _cmd.CommandText = _sb.ToString()
        _cmd.Parameters.Add("@pCollected", SqlDbType.Bit).Value = True
        _cmd.Parameters.Add("@pValidationNumber", SqlDbType.BigInt).Value = Detail.ticket_validation_number
        _cmd.Parameters.Add("@pCollectionId", SqlDbType.BigInt).Value = Detail.money_collection_id

        If _cmd.ExecuteNonQuery() = 1 Then
          Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        End If

      End Using
    Catch _ex As Exception

    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR

  End Function

#End Region

End Class

