﻿'-------------------------------------------------------------------
' Copyright © 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_catalog.vb
' DESCRIPTION:   Draw class for user edition
' AUTHOR:        Sergio Daniel Soria
' CREATION DATE: 31-AUG-2015
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 31-AUG-2015  SDS    Initial version.
' 11-DEC-2015  GMV    Product Backlog Item 6857:Review Iteración 15: Catalogos
' 21-DEC-2015  DLL    Fixed Bug TFS-7814: Catalogs always create disable
' -----------  ------ -----------------------------------------------


Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports WSI.Common
Imports System.Text
Imports System.Type
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient

Public Class CLS_CATALOG
  Inherits CLASS_BASE


#Region " Enum "

  Private Enum ROWACTION
    NORMAL = 0
    MODIFIED = 1
    DELETED = 2
  End Enum

#End Region 'Enum

#Region " Constants "

  Public Const SQL_COLUMN_CAT_ID As Integer = 0
  Public Const SQL_COLUMN_TYPE As Integer = 1
  Public Const SQL_COLUMN_NAME As Integer = 2
  Public Const SQL_COLUMN_DESCRIPTION As Integer = 3
  Public Const SQL_COLUMN_ENABLED As Integer = 4

  Public Const SQL_COLUMN_CAI_CAT_ID As Integer = 0
  Public Const SQL_COLUMN_CAI_ID As Integer = 1
  Public Const SQL_COLUMN_CAI_NAME As Integer = 2
  Public Const SQL_COLUMN_CAI_DESCRIPTION As Integer = 3
  Public Const SQL_COLUMN_CAI_ENABLED As Integer = 4

  Private Const CATALOG_TYPE_USER As Integer = 0
  Private Const CATALOG_TYPE_SYSTEM As Integer = 1
  Private Const CATALOG_TYPE_SYSTEM_SEALED As Integer = 2
  Private Const CATALOG_TYPE_SYSTEM_READONLY As Integer = 4

  Private Const AUDIT_STATE_COLUMN_NAME As String = "ACTION"
#End Region ' Constants 

#Region " Structures "

  Public Class CATALOG
    Public catalog_id As Int32
    Public name As String
    Public type As Integer
    Public description As String
    Public enabled As Boolean
    Public catalog_item As DataTable

    Sub New()
      catalog_id = 0
      name = String.Empty
      type = CATALOG_TYPE_USER  'only type user permitted
      description = String.Empty
      enabled = True
      catalog_item = New DataTable()
    End Sub
  End Class

#End Region ' Structures

#Region " Members "
  Protected m_catalog As New CATALOG
#End Region ' Members

#Region "Properties"
  Public Property CatalogId() As Int32
    Get
      Return m_catalog.catalog_id
    End Get
    Set(ByVal value As Int32)
      m_catalog.catalog_id = value
    End Set
  End Property

  Public Property Name() As String
    Get
      Return m_catalog.name
    End Get
    Set(ByVal value As String)
      m_catalog.name = value
    End Set
  End Property

  Public Property Type() As Integer
    Get
      Return m_catalog.type
    End Get
    Set(ByVal value As Integer)
      m_catalog.type = value
    End Set
  End Property

  Public Property Description() As String
    Get
      Return m_catalog.description
    End Get
    Set(ByVal value As String)
      m_catalog.description = value
    End Set
  End Property
  Public Property Enabled() As Boolean
    Get
      Return m_catalog.enabled
    End Get
    Set(ByVal value As Boolean)
      m_catalog.enabled = value
    End Set
  End Property

  Public Property CatalogItems() As DataTable
    Get
      Return m_catalog.catalog_item
    End Get
    Set(ByVal value As DataTable)
      m_catalog.catalog_item = value
    End Set
  End Property

#End Region ' Properties

#Region "Overrides"

  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA
    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _dt As DataTable
    Dim _str_sort As String
    Dim _dv As DataView
    Dim _action_value As ROWACTION
    Dim _yes_text As String
    Dim _no_text As String
    Dim _str_name As String
    Dim _str_description As String


    _yes_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6692)
    _no_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6693)
    _str_sort = String.Empty
    _action_value = ROWACTION.NORMAL
    _dt = New DataTable()
    _str_name = String.Empty
    _str_description = String.Empty

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_CASHIER_CONFIGURATION)

    Call _auditor_data.SetName(GLB_NLS_GUI_AUDITOR.Id(484), " - " & Me.m_catalog.name)
    Call _auditor_data.SetField(0, m_catalog.name, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6679)) 'nombre
    Call _auditor_data.SetField(0, m_catalog.description, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6682)) 'descripcion

    If m_catalog.enabled Then
      Call _auditor_data.SetField(0, _yes_text, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6694)) 'habilitado
    Else
      Call _auditor_data.SetField(0, _no_text, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6694))
    End If


    If Not Me.CatalogItems Is Nothing And Me.CatalogItems.Rows.Count > 0 Then

      _dt = Me.CatalogItems.Copy
      _dt.Columns.Add(AUDIT_STATE_COLUMN_NAME, GetType(System.Int16))

      ' ordenamos por ID
      _str_sort = "CAI_ID DESC"

      ' marcamos cada row segun su estado
      For Each _dr As DataRow In _dt.Rows
        Select Case _dr.RowState
          Case DataRowState.Added, DataRowState.Modified
            _dr(AUDIT_STATE_COLUMN_NAME) = ROWACTION.MODIFIED
          Case DataRowState.Deleted
            _dr.RejectChanges()
            _dr(AUDIT_STATE_COLUMN_NAME) = ROWACTION.DELETED
          Case Else
            _dr(AUDIT_STATE_COLUMN_NAME) = ROWACTION.NORMAL
        End Select
      Next

      ' ordenamos el nuevo DT
      _dv = _dt.DefaultView
      _dv.Sort = _str_sort
      _dt = _dv.ToTable()

      For Each _dr As DataRow In _dt.Rows

        _action_value = _dr(AUDIT_STATE_COLUMN_NAME)

        _str_name = _dr(SQL_COLUMN_CAI_NAME)

        Select Case _action_value
          Case ROWACTION.MODIFIED, ROWACTION.NORMAL

            If IsDBNull(_dr(SQL_COLUMN_CAI_NAME)) Then
              _auditor_data.SetField(0, AUDIT_NONE_STRING, _str_name & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6679))
            Else
              _auditor_data.SetField(0, _dr(SQL_COLUMN_CAI_NAME), _str_name & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6679))
            End If

            If IsDBNull(_dr(SQL_COLUMN_CAI_DESCRIPTION)) Then
              _auditor_data.SetField(0, AUDIT_NONE_STRING, _str_name & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6682))
            Else
              _auditor_data.SetField(0, _dr(SQL_COLUMN_CAI_DESCRIPTION), _str_name & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6682))
            End If

            If IsDBNull(_dr(SQL_COLUMN_CAI_ENABLED)) Then
              _auditor_data.SetField(0, AUDIT_NONE_STRING, _str_name & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6694))
            Else
              If _dr(SQL_COLUMN_CAI_ENABLED) = True Then
                _auditor_data.SetField(0, _yes_text, _str_name & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6694))
              Else
                _auditor_data.SetField(0, _no_text, _str_name & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6694))
              End If
            End If

          Case Else
        End Select
      Next
    End If

    Return _auditor_data

  End Function ' AuditorData

  '----------------------------------------------------------------------------
  ' PURPOSE : Deletes the given object from the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DEPENDENCIES
  '
  ' NOTES :
  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR

  End Function ' DB_Delete

  '----------------------------------------------------------------------------
  ' PURPOSE : Inserts the given object to the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES :
  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Return Catalog_Insert(SqlCtx)

  End Function ' DB_Insert

  '----------------------------------------------------------------------------
  ' PURPOSE : Reads from the database into the given object
  '
  ' PARAMS :
  '     - INPUT :
  '         - ObjectId: An object, it must be 'casted' to the desired KEY.
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '
  ' NOTES :
  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Me.CatalogId = ObjectId

    Return ReadCatalog()

  End Function ' DB_Read

  '----------------------------------------------------------------------------
  ' PURPOSE : Updates the given object to the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES :
  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Return Catalog_Update(SqlCtx)

  End Function ' DB_Update

  '----------------------------------------------------------------------------
  ' PURPOSE: Returns a duplicate of the given object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim _target As CLS_CATALOG

    _target = New CLS_CATALOG

    _target.Name = Me.Name
    _target.Type = Me.Type
    _target.m_catalog.catalog_item = Me.m_catalog.catalog_item.Copy
    _target.Enabled = Me.Enabled
    _target.Description = Me.Description
    Return _target

  End Function ' Duplicate

#End Region ' Overrides

#Region " Private Functions "

  '----------------------------------------------------------------------------
  ' PURPOSE: Reads an root object  from the database.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES:
  Private Function ReadCatalog() As Integer

    Dim _sb As StringBuilder
    Dim _table As DataTable
    Try
      _sb = New StringBuilder()

      _sb.AppendLine("SELECT   CAT_TYPE ")
      _sb.AppendLine("       , CAT_NAME ")
      _sb.AppendLine("       , CAT_DESCRIPTION ")
      _sb.AppendLine("       , CAT_ENABLED ")
      _sb.AppendLine("  FROM   CATALOGS    ")
      _sb.AppendLine(" WHERE   CAT_ID = " + Me.m_catalog.catalog_id.ToString())

      _table = GUI_GetTableUsingSQL(_sb.ToString(), 5000)

      If IsNothing(_table) Then
        Return ENUM_STATUS.STATUS_ERROR
      End If

      If _table.Rows.Count() > 1 Then
        Return ENUM_STATUS.STATUS_ERROR
      End If

      If _table.Rows.Count() = 1 Then
        Me.Type = IIf(IsDBNull(_table.Rows(0).Item("CAT_TYPE")), "", _table.Rows(0).Item("CAT_TYPE"))
        Me.Name = IIf(IsDBNull(_table.Rows(0).Item("CAT_NAME")), "", _table.Rows(0).Item("CAT_NAME"))
        Me.Description = IIf(IsDBNull(_table.Rows(0).Item("CAT_DESCRIPTION")), "", _table.Rows(0).Item("CAT_DESCRIPTION"))
        Me.Enabled = IIf(IsDBNull(_table.Rows(0).Item("CAT_ENABLED")), "", _table.Rows(0).Item("CAT_ENABLED"))
      End If

      Call ReadCatalogItems()

      Return ENUM_STATUS.STATUS_OK

    Catch ex As Exception
      Return ENUM_STATUS.STATUS_ERROR
    End Try
  End Function ' ReadCatalogs

  '----------------------------------------------------------------------------
  ' PURPOSE: Reads an child object  from the database.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  '
  ' NOTES:
  Public Sub ReadCatalogItems()
    Dim _sb As StringBuilder

    _sb = New StringBuilder()

    _sb.AppendLine("SELECT   CAI_CATALOG_ID    ")
    _sb.AppendLine("       , CAI_ID     ")
    _sb.AppendLine("       , ISNULL(CAI_NAME, '') AS CAI_NAME  ")
    _sb.AppendLine("       , ISNULL(CAI_DESCRIPTION, '') AS CAI_DESCRIPTION   ")
    _sb.AppendLine("       , CAI_ENABLED   ")
    _sb.AppendLine("  FROM   CATALOG_ITEMS    ")
    _sb.AppendLine(" WHERE   CAI_CATALOG_ID = " + Me.m_catalog.catalog_id.ToString())
    '_sb.AppendLine("    ORDER BY  ")

    m_catalog.catalog_item = GUI_GetTableUsingSQL(_sb.ToString(), 5000)

    If m_catalog.catalog_item.Rows.Count > 0 Then
      m_catalog.catalog_id = m_catalog.catalog_item.Rows(0)(SQL_COLUMN_CAI_CAT_ID)
    End If
  End Sub

  '----------------------------------------------------------------------------
  ' PURPOSE: Insert the given object into the database.
  '
  ' PARAMS:
  '   - INPUT:  None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES:
  Private Function Catalog_Insert(ByVal Context As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _str_sql As String
    Dim _db_count As Integer
    Dim _num_rows_updated As Integer
    Dim _param As SqlClient.SqlParameter

    _db_count = 0

    Try

      Using _db_trx As New DB_TRX()

        ' INSERT Catalog
        _str_sql = "INSERT INTO   CATALOGS " & _
                              " ( " & _
                              "  CAT_TYPE " & _
                              " , CAT_NAME " & _
                              " , CAT_DESCRIPTION" & _
                              " , CAT_ENABLED" & _
                              " ) " & _
                              "   VALUES" & _
                              " (  " & _
                              "  @cat_type " & _
                              " , @cat_name " & _
                              " , @cat_description " & _
                              " , @cat_enabled " & _
                              " ) " & _
                            " SET @pCatalogID = SCOPE_IDENTITY() "

        Using _cmd As New SqlClient.SqlCommand(_str_sql, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _cmd.Parameters.Add("@cat_type", SqlDbType.Int).Value = m_catalog.type
          _cmd.Parameters.Add("@cat_name", SqlDbType.NVarChar).Value = m_catalog.name
          _cmd.Parameters.Add("@cat_description", SqlDbType.NVarChar).Value = m_catalog.description
          _cmd.Parameters.Add("@cat_enabled", SqlDbType.Bit).Value = m_catalog.enabled

          _param = _cmd.Parameters.Add("@pCatalogID", SqlDbType.Int)
          _param.Direction = ParameterDirection.Output

          _num_rows_updated = _cmd.ExecuteNonQuery()
        End Using


        If _num_rows_updated <> 1 Then
          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

        _num_rows_updated = UpdateCatalog_items(_db_trx, _param.Value)

        If _num_rows_updated > 0 Then
          ' Min one row
          _db_trx.Commit()
        Else
          _db_trx.Rollback()
          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

      End Using '_db_trx

    Catch ex As Exception
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

  End Function ' Catalog_Insert

  '----------------------------------------------------------------------------
  ' PURPOSE: Insert the given object into the database.
  '
  ' PARAMS:
  '   - INPUT:  Catalog ID
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - number of rows updated
  '
  ' NOTES:
  Private Function UpdateCatalog_items(ByVal DbTrx As DB_TRX, ByVal CatalogId As Integer) As Integer
    Dim _sb_update As StringBuilder
    Dim _sb_insert As StringBuilder
    Dim _sb_delete As StringBuilder
    Dim _num_rows As Integer

    _sb_update = New StringBuilder()
    _sb_insert = New StringBuilder()
    _sb_delete = New StringBuilder()
    _num_rows = 0

    _sb_insert.AppendLine("INSERT INTO   CATALOG_ITEMS  ")
    _sb_insert.AppendLine("            ( CAI_CATALOG_ID ")
    '_sb_insert.AppendLine("            , CAI_ID ")
    _sb_insert.AppendLine("            , CAI_NAME  ")
    _sb_insert.AppendLine("            , CAI_DESCRIPTION ")
    _sb_insert.AppendLine("            , CAI_ENABLED ")
    _sb_insert.AppendLine("            ) ")
    _sb_insert.AppendLine("              VALUES ")
    _sb_insert.AppendLine("            ( @cai_catalog_id ")
    '_sb_insert.AppendLine("            , @cai_id ")
    _sb_insert.AppendLine("            , @cai_name ")
    _sb_insert.AppendLine("            , @cai_description ")
    _sb_insert.AppendLine("            , @cai_enabled ")
    _sb_insert.AppendLine("            ) ")

    _sb_update.AppendLine("UPDATE  CATALOG_ITEMS        ")
    _sb_update.AppendLine("   SET   CAI_CATALOG_ID      = @cai_catalog_id     ")
    _sb_update.AppendLine("       , CAI_NAME            = @cai_name    ")
    _sb_update.AppendLine("       , CAI_DESCRIPTION     = @cai_description    ")
    _sb_update.AppendLine("       , CAI_ENABLED         = @cai_enabled    ")
    _sb_update.AppendLine(" WHERE   CAI_ID              = @cai_id  ")


    Using _cmd_update As SqlCommand = New SqlCommand(_sb_update.ToString(), DbTrx.SqlTransaction.Connection, DbTrx.SqlTransaction)
      Using _cmd_insert As SqlCommand = New SqlCommand(_sb_insert.ToString(), DbTrx.SqlTransaction.Connection, DbTrx.SqlTransaction)

        _cmd_update.Parameters.Add("@cai_catalog_id", SqlDbType.BigInt).Value = CatalogId ' Param is Catalog root
        _cmd_update.Parameters.Add("@cai_id", SqlDbType.BigInt).SourceColumn = "CAI_ID"
        _cmd_update.Parameters.Add("@cai_name", SqlDbType.NVarChar).SourceColumn = "CAI_NAME"
        _cmd_update.Parameters.Add("@cai_description", SqlDbType.NVarChar).SourceColumn = "CAI_DESCRIPTION"
        _cmd_update.Parameters.Add("@cai_enabled", SqlDbType.Bit).SourceColumn = "CAI_ENABLED"

        _cmd_insert.Parameters.Add("@cai_catalog_id", SqlDbType.Int).Value = CatalogId ' Param is Catalog root
        _cmd_insert.Parameters.Add("@cai_name", SqlDbType.NVarChar).SourceColumn = "CAI_NAME"
        _cmd_insert.Parameters.Add("@cai_description", SqlDbType.NVarChar).SourceColumn = "CAI_DESCRIPTION"
        _cmd_insert.Parameters.Add("@cai_enabled", SqlDbType.Bit).SourceColumn = "CAI_ENABLED"

        Using _da As SqlDataAdapter = New SqlDataAdapter()
          _da.UpdateCommand = _cmd_update
          _da.InsertCommand = _cmd_insert

          _num_rows = _da.Update(m_catalog.catalog_item.GetChanges)
        End Using
      End Using
    End Using

    Return _num_rows
  End Function ' UpdateCatalogs

  '----------------------------------------------------------------------------
  ' PURPOSE: Update the given object into the database.
  '
  ' PARAMS:
  '   - INPUT:  None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES:
  Private Function Catalog_Update(ByVal Context As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _str_sql As String
    Dim _num_rows_updated As Integer
    Dim _num_rows_catalog_updated As Integer



    Try

      Using _db_trx As New DB_TRX()

        ' UPDATE Catalogs 
        _str_sql = "UPDATE  CATALOGS " & _
                    "   SET  CAT_TYPE = @cat_type " & _
                    "      , CAT_NAME = @cat_name " & _
                    "      , CAT_DESCRIPTION = @cat_descripcion " & _
                    "      , CAT_ENABLED = @cat_enabled " & _
                    "  WHERE CAT_ID   = @cat_id "

        Using _cmd As New SqlClient.SqlCommand(_str_sql, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _cmd.Parameters.Add("@cat_type", SqlDbType.Int).Value = m_catalog.type
          _cmd.Parameters.Add("@cat_name", SqlDbType.NVarChar).Value = m_catalog.name
          _cmd.Parameters.Add("@cat_descripcion", SqlDbType.NVarChar).Value = m_catalog.description
          _cmd.Parameters.Add("@cat_enabled", SqlDbType.Bit).Value = m_catalog.enabled
          _cmd.Parameters.Add("@cat_id", SqlDbType.BigInt).Value = m_catalog.catalog_id

          _num_rows_updated = _cmd.ExecuteNonQuery()

        End Using

        'UPDATE Catalog Items

        _num_rows_catalog_updated = UpdateCatalog_items(_db_trx, m_catalog.catalog_id)

        If _num_rows_catalog_updated > 0 Or _num_rows_updated > 0 Then
          ' Min one row
          _db_trx.Commit()
        Else
          _db_trx.Rollback()
          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

      End Using '_db_trx

    Catch ex As Exception
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

  End Function ' Catalog_Update


  ' PURPOSE : Exist name of Catalog
  '
  ' PARAMS :
  '     - INPUT :
  '         - name
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK  -> Not Exists
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DUPLICATE_KEY -> Exist
  '
  ' NOTES :
  Public Function ExistsCatalogName(ByVal catalogName As String) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _value As Object
    Dim _db_count As Integer
    Dim _str_sql As String

    Using _db_trx As New DB_TRX()

      ' CHECK If name already exists
      _str_sql = "SELECT  COUNT(*) " & _
                " FROM   CATALOGS " & _
              "  WHERE   CAT_NAME = @pName "

      Using _cmd As New SqlClient.SqlCommand(_str_sql, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
        _cmd.Parameters.Add("@pName", SqlDbType.NVarChar).Value = catalogName

        _value = _cmd.ExecuteScalar()
      End Using

    End Using

    If _value IsNot Nothing And _value IsNot DBNull.Value Then
      _db_count = CInt(_value)
    Else
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    If _db_count > 0 Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
    Else
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
    End If


  End Function ' ExistsCatalogName

#End Region ' Private Functions

End Class
