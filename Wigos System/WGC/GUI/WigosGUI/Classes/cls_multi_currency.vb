'-------------------------------------------------------------------
' Copyright � 2015 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_multi_currency.vb
'
' DESCRIPTION:   Currency rate management class
'
' AUTHOR:        Alex de Nicol�s
'
' CREATION DATE: 23-APR-2015
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 23-APR-2015  ANM    Initial version.
'--------------------------------------------------------------------

Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports WSI.Common
Imports System.Text
Imports System.Globalization
Imports System.Threading.Thread

Public Class CLASS_MULTI_CURRENCY
  Inherits CLASS_BASE

#Region "Structure"

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_MULTI_CURRENCY
    Public working_day As Date
    Public national_iso_currency As String
    Public currencies As New List(Of TYPE_MULTI_CURRENCY_INSTANCE)
  End Class

#Region "Currency Instance"

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_MULTI_CURRENCY_INSTANCE
    Public iso_code As String
    Public iso_code_view As String
    Public description As String
    Public change As Decimal
  End Class

#End Region ' Currency Instance

#End Region ' Structure

#Region "Constructor / Destructor"

  Public Sub New()
    Call MyBase.New()

    m_multi_currency.working_day = WGDB.Now

  End Sub

  Protected Overrides Sub Finalize()
    MyBase.Finalize()
  End Sub

#End Region ' Constructor / Destructor

#Region " Members "

  Protected m_multi_currency As New TYPE_MULTI_CURRENCY

#End Region ' Members

#Region "Properties"

  Public Property Currencies() As List(Of TYPE_MULTI_CURRENCY_INSTANCE)
    Get
      Return m_multi_currency.currencies
    End Get

    Set(ByVal value As List(Of TYPE_MULTI_CURRENCY_INSTANCE))
      m_multi_currency.currencies = value
    End Set

  End Property

  Public Property NationalIsoCurrency() As String
    Get
      Return m_multi_currency.national_iso_currency
    End Get

    Set(ByVal value As String)
      m_multi_currency.national_iso_currency = value
    End Set

  End Property

  Public Property WorkingDay() As Date
    Get
      Return m_multi_currency.working_day
    End Get

    Set(ByVal Value As Date)
      m_multi_currency.working_day = Value
    End Set

  End Property

#End Region ' Properties

#Region " Overrides functions "

  '----------------------------------------------------------------------------
  ' PURPOSE: Reads from the database into the given object
  '
  ' PARAMS:
  '   - INPUT:
  '     - ObjectId: An object, it must be 'casted' to the desired KEY.
  '
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As ENUM_STATUS

    Dim _rc As Integer

    _rc = ReadMultiCurrency(m_multi_currency)

    Select Case _rc
      Case ENUM_DB_RC.DB_RC_OK
        Return ENUM_STATUS.STATUS_OK

      Case ENUM_DB_RC.DB_RC_ERROR_NOT_FOUND
        Return CLASS_BASE.ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function   'DB_Read

  '----------------------------------------------------------------------------
  ' PURPOSE: Updates the given object to the database.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As Integer

    _rc = UpdateMultiCurrency(m_multi_currency)

    Select Case _rc
      Case ENUM_DB_RC.DB_RC_OK
        Return ENUM_STATUS.STATUS_OK

      Case ENUM_DB_RC.DB_RC_ERROR_NOT_FOUND
        Return CLASS_BASE.ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select
  End Function 'DB_Update

  '----------------------------------------------------------------------------
  ' PURPOSE: Can't insert the given object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
  End Function 'DB_Insert

  '----------------------------------------------------------------------------
  ' PURPOSE: Can't delete the given object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
  End Function 'DB_Delete

  '----------------------------------------------------------------------------
  ' PURPOSE: Returns a duplicate of the given object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim _temp_mc As CLASS_MULTI_CURRENCY
    Dim _idx As Integer
    Dim _item As TYPE_MULTI_CURRENCY_INSTANCE

    _temp_mc = New CLASS_MULTI_CURRENCY

    _temp_mc.WorkingDay = m_multi_currency.working_day
    _temp_mc.NationalIsoCurrency = m_multi_currency.national_iso_currency
    _temp_mc.m_multi_currency.currencies = New List(Of TYPE_MULTI_CURRENCY_INSTANCE)
    For _idx = 0 To m_multi_currency.currencies.Count - 1

      _item = New TYPE_MULTI_CURRENCY_INSTANCE
      _item.iso_code = m_multi_currency.currencies(_idx).iso_code
      _item.iso_code_view = m_multi_currency.currencies(_idx).iso_code_view
      _item.description = m_multi_currency.currencies(_idx).description
      _item.change = m_multi_currency.currencies(_idx).change

      _temp_mc.m_multi_currency.currencies.Add(_item)
    Next

    Return _temp_mc

  End Function 'Duplicate 

  '----------------------------------------------------------------------------
  ' PURPOSE: Returns the related auditor data for the current object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA
    Dim _idx As Integer
    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _field As String

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_CASHIER_CONFIGURATION)

    Call _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(6239), GLB_NLS_GUI_PLAYER_TRACKING.GetString(6286) & " " & _
        GUI_FormatDate(m_multi_currency.working_day, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)) ''"Currency rate at date xx"

    For _idx = 0 To m_multi_currency.currencies.Count - 1

      ''1 %1 --> xxxx %2
      _field = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2399, m_multi_currency.currencies(_idx).iso_code_view)
      Call _auditor_data.SetField(0, Me.m_multi_currency.currencies(_idx).change & " " & m_multi_currency.national_iso_currency, _field)

    Next

    Return _auditor_data

  End Function 'AuditorData

#End Region 'Overrides functions

#Region "DB Functions"

  Private Function ReadMultiCurrency(ByVal MCParams As TYPE_MULTI_CURRENCY) As Integer
    Dim _str_sql As StringBuilder
    Dim _data_table_instances As DataTable
    Dim _full As Boolean
    Dim _dr As DataRow
    Dim _currency As TYPE_MULTI_CURRENCY_INSTANCE

    MCParams.currencies = New List(Of TYPE_MULTI_CURRENCY_INSTANCE)
    _full = True

    _str_sql = New StringBuilder
    _str_sql.AppendLine(" SELECT   CUR_ISO_CODE     ")
    _str_sql.AppendLine("        , CUR_DESCRIPTION  ")
    _str_sql.AppendLine("        , ER_CHANGE        ")
    _str_sql.AppendLine("   FROM   CURRENCIES       ")
    _str_sql.AppendLine("   LEFT   JOIN EXCHANGE_RATES ON CURRENCIES.CUR_ISO_CODE = EXCHANGE_RATES.ER_ISO_CODE   ")
    _str_sql.AppendLine("    AND   (ER_WORKING_DAY iS NULL ")
    _str_sql.AppendLine("     OR   (ER_WORKING_DAY >= DATEADD(dd, 0, " & GUI_FormatDayDB(MCParams.working_day) & ") ")
    _str_sql.AppendLine("    AND   ER_WORKING_DAY  <  DATEADD(dd, 1, " & GUI_FormatDayDB(MCParams.working_day) & "))) ")
    _str_sql.AppendLine("  WHERE   CUR_ISO_CODE   <> '" & MCParams.national_iso_currency & "'")

    _data_table_instances = GUI_GetTableUsingSQL(_str_sql.ToString, 5000)

    'can be empty
    If _data_table_instances.Rows.Count() = 0 Then
      _data_table_instances = ReadOnlyCurrencies()
      _full = False
    End If

    If IsNothing(_data_table_instances) Then

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    For Each _dr In _data_table_instances.Rows
      _currency = New TYPE_MULTI_CURRENCY_INSTANCE

      _currency.iso_code = _dr.Item("CUR_ISO_CODE")
      _currency.iso_code_view = "1 " & _dr.Item("CUR_ISO_CODE")
      _currency.description = _dr.Item("CUR_DESCRIPTION")
      If _full AndAlso Not IsDBNull(_dr.Item("ER_CHANGE")) Then
        _currency.change = _dr.Item("ER_CHANGE")
      End If

      MCParams.currencies.Add(_currency)

    Next

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

  End Function

  Private Function ReadOnlyCurrencies() As DataTable
    Dim _str_sql As StringBuilder

    _str_sql = New StringBuilder
    _str_sql.AppendLine("SELECT   CUR_ISO_CODE    ")
    _str_sql.AppendLine("       , CUR_DESCRIPTION ")
    _str_sql.AppendLine("  FROM   CURRENCIES      ")

    Return GUI_GetTableUsingSQL(_str_sql.ToString, 5000)

  End Function

  Private Function UpdateMultiCurrency(ByVal MCParams As TYPE_MULTI_CURRENCY) As Integer
    Dim _str_sql As StringBuilder
    Dim _sql_trx As System.Data.SqlClient.SqlTransaction = Nothing
    Dim _rc As Integer
    Dim _currency As TYPE_MULTI_CURRENCY_INSTANCE

    Try

      Using _db_trx As New WSI.Common.DB_TRX()

        For Each _currency In MCParams.currencies
          _str_sql = New StringBuilder
          _str_sql.AppendLine("IF NOT EXISTS ( SELECT 1 FROM EXCHANGE_RATES         ")
          _str_sql.AppendLine("               WHERE ER_ISO_CODE     = @pIsoCode     ")
          _str_sql.AppendLine("                 AND ER_WORKING_DAY  = @pWorkingDay) ")
          _str_sql.AppendLine("   INSERT INTO EXCHANGE_RATES")
          _str_sql.AppendLine("     (                       ")
          _str_sql.AppendLine("     ER_ISO_CODE             ")
          _str_sql.AppendLine("   , ER_WORKING_DAY          ")
          _str_sql.AppendLine("   , ER_CHANGE               ")
          _str_sql.AppendLine("   , ER_DATE_UPDATED         ")
          _str_sql.AppendLine("     )                       ")
          _str_sql.AppendLine("     VALUES                  ")
          _str_sql.AppendLine("     (                       ")
          _str_sql.AppendLine("     @pIsoCode               ")
          _str_sql.AppendLine("   , @pWorkingDay            ")
          _str_sql.AppendLine("   , @pChange                ")
          _str_sql.AppendLine("   , @pDate                  ")
          _str_sql.AppendLine("     )                       ")
          _str_sql.AppendLine("ELSE                         ")
          _str_sql.AppendLine("   UPDATE EXCHANGE_RATES     ")
          _str_sql.AppendLine("     SET  ER_CHANGE       = @pChange     ")
          _str_sql.AppendLine("        , ER_DATE_UPDATED = @pDate       ")
          _str_sql.AppendLine("   WHERE  ER_ISO_CODE     = @pIsoCode    ")
          _str_sql.AppendLine("     AND  ER_WORKING_DAY  = @pWorkingDay ")
          Using _sql_command As New SqlCommand(_str_sql.ToString, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
            _sql_command.Parameters.Add("@pIsoCode", SqlDbType.NVarChar).Value = _currency.iso_code
            _sql_command.Parameters.Add("@pWorkingDay", SqlDbType.Date).Value = GUI_FormatDate(MCParams.working_day, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
            _sql_command.Parameters.Add("@pChange", SqlDbType.Decimal).Value = _currency.change
            _sql_command.Parameters.Add("@pDate", SqlDbType.DateTime).Value = WGDB.Now
            _rc = _sql_command.ExecuteNonQuery()

            If _rc <> 1 Then

              Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
            End If
          End Using

        Next

        _db_trx.Commit()
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
      End Using

    Catch ex As Exception
      Log.Exception(ex)

    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

  End Function

  Public Function NationalCurrency() As String
    Dim _str_sql As StringBuilder
    Dim _dt As DataTable
    Dim _iso_desc As String

    _iso_desc = String.Empty

    Try

      _str_sql = New StringBuilder
      _str_sql.AppendLine("SELECT   CUR_DESCRIPTION ")
      _str_sql.AppendLine("  FROM   CURRENCIES      ")
      _str_sql.AppendLine(" WHERE   CUR_ISO_CODE = '" & m_multi_currency.national_iso_currency & "'")

      _dt = GUI_GetTableUsingSQL(_str_sql.ToString, 5000)

      If _dt.Rows.Count() > 0 Then
        _iso_desc = _dt.Rows(0).Item("CUR_DESCRIPTION")
      Else
        'try at last to get the iso description
        _iso_desc = CurrencyExchange.GetDescription(CurrencyExchangeType.CURRENCY, m_multi_currency.national_iso_currency, "")
      End If

    Catch ex As Exception
      Log.Exception(ex)

    End Try

    Return _iso_desc
  End Function

#End Region ' DB Functions

End Class
