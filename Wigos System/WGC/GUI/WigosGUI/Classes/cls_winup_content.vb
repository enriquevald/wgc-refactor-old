﻿'-------------------------------------------------------------------
' Copyright © 2016 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : cls_winup_content.vb
'
' DESCRIPTION : content winup class
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 24-FEB-2017  PDM    Initial version
'--------------------------------------------------------------------
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports WSI.Common
Imports Newtonsoft.Json.Linq
Imports System.IO
Imports Newtonsoft.Json
Imports System.Text

Public Class CLASS_WINUP_CONTENT
  Inherits CLASS_BASE

#Region "Enums"

  'Public Enum SECTION_SCHEMA_TYPE
  '  CasinoOffers = 3
  '  Dinning = 4
  '  RecentWinners = 5
  '  Slot = 14
  '  Poker = 16
  '  TableGames = 17
  '  TheCasino = 19
  'End Enum

  Public Enum TEMPLATE
    SectionsListTemplate = 1
    PromotionalContentList = 2
    AdvertisingCampaignsList = 3
    SectionDetail = 4
    AdvertisingPromotionalItem = 5
  End Enum

#End Region

#Region "Members"

  Private m_id As Integer
  Private m_title As String
  Private m_description As String
  Private m_description_details As String
  Private m_order As Integer
  Private m_status As Boolean
  Private m_list_image_id As Integer
  Private m_list_image As Image
  Private m_list_image_name As String
  Private m_detail_image As Image
  Private m_detail_image_name As String
  Private m_section_schema_id As Integer
  Private m_detail_image_id As Integer

#End Region

#Region "Properties"

  Public Property Id() As Integer
    Get
      Return m_id
    End Get
    Set(ByVal value As Integer)
      m_id = value
    End Set
  End Property

  Public Property Title() As String
    Get
      Return m_title
    End Get
    Set(ByVal value As String)
      m_title = value
    End Set
  End Property

  Public Property Description() As String
    Get
      Return m_description
    End Get
    Set(ByVal value As String)
      m_description = value
    End Set
  End Property

  Public Property DescriptionDetails() As String
    Get
      Return m_description_details
    End Get
    Set(ByVal value As String)
      m_description_details = value
    End Set
  End Property

  Public Property Order() As Integer
    Get
      Return m_order
    End Get
    Set(ByVal value As Integer)
      m_order = value
    End Set
  End Property

  Public Property Status() As Boolean
    Get
      Return m_status
    End Get
    Set(ByVal value As Boolean)
      m_status = value
    End Set
  End Property

  Public Property ListImageId() As Integer
    Get
      Return m_list_image_id
    End Get
    Set(ByVal value As Integer)
      m_list_image_id = value
    End Set
  End Property

  Public Property ListImage() As Image
    Get
      Return m_list_image
    End Get
    Set(ByVal value As Image)
      m_list_image = value
    End Set
  End Property

  Public Property ListImageName() As String
    Get
      Return m_list_image_name
    End Get
    Set(ByVal value As String)
      m_list_image_name = value
    End Set
  End Property

  Public Property DetailImageId() As Integer
    Get
      Return m_detail_image_id
    End Get
    Set(ByVal value As Integer)
      m_detail_image_id = value
    End Set
  End Property

  Public Property DetailImage() As Image
    Get
      Return m_detail_image
    End Get
    Set(ByVal value As Image)
      m_detail_image = value
    End Set
  End Property

  Public Property DetailImageName() As String
    Get
      Return m_detail_image_name
    End Get
    Set(ByVal value As String)
      m_detail_image_name = value
    End Set
  End Property

  Public Property SectionSchemaId() As Integer
    Get
      Return m_section_schema_id
    End Get
    Set(ByVal value As Integer)
      m_section_schema_id = value
    End Set
  End Property

#End Region

  Public Overrides Function AuditorData() As CLASS_AUDITOR_DATA

    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _yes_text As String
    Dim _no_text As String

    _yes_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(278)
    _no_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_USER_ACTIVITY)

    _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(7890), m_id.ToString())

    Call _auditor_data.SetField(0, m_title, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7522))
    Call _auditor_data.SetField(0, m_status, GLB_NLS_GUI_PLAYER_TRACKING.GetString(532))
    Call _auditor_data.SetField(0, m_description, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7935))
    Call _auditor_data.SetField(0, m_order, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7449))
    Call _auditor_data.SetField(0, GetInfoCRC(Me.DetailImage), GLB_NLS_GUI_PLAYER_TRACKING.GetString(7451))
    Call _auditor_data.SetField(0, GetInfoCRC(Me.ListImage), GLB_NLS_GUI_PLAYER_TRACKING.GetString(7450))
    Call _auditor_data.SetField(0, m_description_details, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7930))

    Return _auditor_data

  End Function

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As CLASS_BASE.ENUM_STATUS
    Dim _rc As Integer

    _rc = DeleteContent()

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK

        If m_list_image_id > 0 Then
          DeleteImage(m_list_image_id)
        End If

        If m_detail_image_id > 0 Then
          DeleteImage(m_detail_image_id)
        End If

        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DEPENDENCIES
        Return ENUM_STATUS.STATUS_DEPENDENCIES

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select
  End Function

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As CLASS_BASE.ENUM_STATUS
    Dim _rc As Integer

    _rc = InsertContent()

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
        Return ENUM_STATUS.STATUS_DUPLICATE_KEY

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select
  End Function

  Public Overrides Function DB_Read(ObjectId As Object, ByRef SqlCtx As Integer) As CLASS_BASE.ENUM_STATUS

    Dim _rc As Integer

    Me.Id = ObjectId

    ' Read ads data
    _rc = ReadContent()

    Select Case _rc

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As CLASS_BASE.ENUM_STATUS
    Dim _rc As Integer

    ' Read ads data
    _rc = UpdateContent()

    Select Case _rc

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select
  End Function

  Public Overrides Function Duplicate() As CLASS_BASE

    Dim _content As CLASS_WINUP_CONTENT

    _content = New CLASS_WINUP_CONTENT

    With _content

      .Title = m_title
      .Description = m_description
      .DescriptionDetails = m_description_details
      .Order = m_order
      .Status = m_status
      .DetailImageId = m_detail_image_id
      .ListImageId = m_list_image_id
      .SectionSchemaId = m_section_schema_id
      .DetailImage = m_detail_image
      .ListImage = m_list_image

    End With

    Return _content

  End Function


#Region "Private Functions"


  Private Function ReadContent() As Integer

    Dim _id As Integer
    'Dim _datatable As DataTable
    Dim _response As String
    Dim _arrayContent As JObject
    Dim _sql_transaction As System.Data.SqlClient.SqlTransaction
    Dim _rc As Integer
    Dim _do_commit As Boolean
    Dim _url As String
    Dim _api As CLS_API

    _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    _sql_transaction = Nothing
    _id = Me.Id
    _do_commit = False

    Try

      If Not GUI_BeginSQLTransaction(_sql_transaction) Then

        _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        Return _rc
      End If

      _url = WSI.Common.GeneralParam.GetString("WinUP", "Backend.Url") + "/content/getById/" + _id.ToString()

      _api = New CLS_API(_url)
      _response = _api.getData()
      _arrayContent = JObject.Parse(_api.sanitizeData(_response))

      ''Image List
      If _arrayContent.Item("listImageId").ToString() <> "" Then
        m_list_image_id = _arrayContent.Item("listImageId")

        If m_list_image_id > 0 Then

          Dim _imageTmpResponse As JObject
          _url = WSI.Common.GeneralParam.GetString("WinUP", "Backend.Url") + "/image/getbyid/" + _arrayContent.Item("listImageId").ToString()
          _api = New CLS_API(_url)
          _imageTmpResponse = JObject.Parse(_api.sanitizeData(_api.getData()))
          If Not _imageTmpResponse.Item("imageData") = "" Then
            Dim imgData As Byte() = _imageTmpResponse.Item("imageData")
            Using ms As New MemoryStream(imgData)
              m_list_image = System.Drawing.Image.FromStream(ms)
            End Using
          Else
            m_list_image = Nothing
          End If

          If Not _imageTmpResponse.Item("fileName") = "" Then
            m_list_image_name = _imageTmpResponse.Item("fileName")
          End If

        End If

      End If

      ''Image Detail
      If _arrayContent.Item("detailImageId").ToString() <> "" Then
        m_detail_image_id = _arrayContent.Item("detailImageId")

        If m_detail_image_id > 0 Then

          Dim _imageTmpResponse As JObject
          _url = WSI.Common.GeneralParam.GetString("WinUP", "Backend.Url") + "/image/getbyid/" + _arrayContent.Item("detailImageId").ToString()
          _api = New CLS_API(_url)
          _imageTmpResponse = JObject.Parse(_api.sanitizeData(_api.getData()))
          If Not _imageTmpResponse.Item("imageData") = "" Then
            Dim imgData As Byte() = _imageTmpResponse.Item("imageData")
            Using ms As New MemoryStream(imgData)
              m_detail_image = System.Drawing.Image.FromStream(ms)
            End Using
          Else
            m_detail_image = Nothing
          End If

          If Not _imageTmpResponse.Item("fileName") = "" Then
            m_detail_image_name = _imageTmpResponse.Item("fileName")
          End If

        End If
      End If


      If Not IsNothing(_arrayContent.Count = 0) Then

        If _arrayContent.Item("title").ToString() <> "" Then
          m_title = _arrayContent.Item("title")
        End If

        If _arrayContent.Item("description").ToString() <> "" Then
          m_description = _arrayContent.Item("description")
        End If

        If _arrayContent.Item("order").ToString() <> "" Then
          m_order = _arrayContent.Item("order")
        End If

        If _arrayContent.Item("descriptionDetail").ToString() <> "" Then
          m_description_details = _arrayContent.Item("descriptionDetail")
        End If

        If _arrayContent.Item("status").ToString() <> "" Then
          m_status = _arrayContent.Item("status")
        End If

        If _arrayContent.Item("sectionSchemaId").ToString() <> "" Then
          m_section_schema_id = Convert.ToInt32(_arrayContent.Item("sectionSchemaId"))
        End If

        _do_commit = True
        _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return _rc

      Else

        _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        Return _rc

      End If
    Catch ex As Exception
      _do_commit = False
    Finally

      GUI_EndSQLTransaction(_sql_transaction, _do_commit)

    End Try

    Return _rc
  End Function


  '----------------------------------------------------------------------------
  ' PURPOSE : Adds a content object into DB
  '
  ' PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - CONFIGURATION_OK
  '     - CONFIGURATION_ERROR_DB
  '
  ' NOTES :

  Private Function InsertContent() As Integer

    Dim _sql_transaction As System.Data.SqlClient.SqlTransaction
    Dim _row_affected As Integer
    Dim _do_commit As Boolean
    Dim _rc As Integer
    Dim _url As String
    Dim _api As CLS_API
    Dim response As String

    _sql_transaction = Nothing
    _row_affected = 0
    _do_commit = False
    _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

    Try

      If Not GUI_BeginSQLTransaction(_sql_transaction) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      If Not m_list_image Is Nothing Then
        m_list_image_id = SaveImage(m_list_image, 0)
      End If

      If Not m_detail_image Is Nothing Then
        m_detail_image_id = SaveImage(m_detail_image, 0)
      End If

      _url = WSI.Common.GeneralParam.GetString("WinUP", "Backend.Url") + "/content"
      _api = New CLS_API(_url)
      response = _api.postData(Encoding.UTF8.GetBytes(Me.ToJSON()))

      _row_affected = 1

      If _row_affected <> 1 Then
        _do_commit = False
        _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        Return _rc
      Else
        _do_commit = True
        _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_OK
      End If

    Catch ex As Exception
      _do_commit = False
      _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

    Finally

      GUI_EndSQLTransaction(_sql_transaction, _do_commit)

    End Try

    Return _rc

  End Function ' InsertArea

  '----------------------------------------------------------------------------
  ' PURPOSE : Update a content object into DB
  '
  ' PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - CONFIGURATION_OK
  '     - CONFIGURATION_ERROR_DB
  '
  ' NOTES :

  Private Function UpdateContent() As Integer
    Dim _sql_transaction As System.Data.SqlClient.SqlTransaction
    Dim _row_affected As Integer
    Dim _do_commit As Boolean
    Dim _rc As Integer
    Dim _url As String
    Dim _api As CLS_API
    Dim response As String

    _sql_transaction = Nothing
    _row_affected = 0
    _do_commit = False

    Try

      If Not GUI_BeginSQLTransaction(_sql_transaction) Then
        _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        Return _rc
      End If

      If m_list_image_id = 0 And Not IsNothing(m_list_image) Or m_list_image_id > 0 And Not IsNothing(m_list_image) And m_list_image_name = String.Empty Then
        m_list_image_id = SaveImage(m_list_image, m_list_image_id)
      End If

      If m_detail_image_id = 0 And Not IsNothing(m_detail_image) Or m_detail_image_id > 0 And Not IsNothing(m_detail_image) And m_detail_image_name = String.Empty Then
        m_detail_image_id = SaveImage(m_detail_image, m_detail_image_id)
      End If

      If m_list_image_id > 0 And IsNothing(m_list_image) Then
        DeleteImage(m_list_image_id)
        m_list_image_id = 0
      End If

      If m_detail_image_id > 0 And IsNothing(m_detail_image) Then
        DeleteImage(m_detail_image_id)
        m_detail_image_id = 0
      End If

      _url = WSI.Common.GeneralParam.GetString("WinUP", "Backend.Url") + "/content/" + Me.Id.ToString()
      _api = New CLS_API(_url)

      response = _api.putData(Encoding.UTF8.GetBytes(Me.ToJSON()))

      _row_affected = 1

      If _row_affected <> 1 Then
        _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        Return _rc
      Else
        _do_commit = True
        _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_OK
      End If

    Catch ex As Exception
      _do_commit = False

    Finally

      GUI_EndSQLTransaction(_sql_transaction, _do_commit)

    End Try

    Return _rc
  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE : Delete a content object from DB
  '
  ' PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - CONFIGURATION_OK
  '     - CONFIGURATION_ERROR_DB
  '
  ' NOTES :

  Private Function DeleteContent() As Integer

    Dim _sql_transaction As System.Data.SqlClient.SqlTransaction
    Dim _row_affected As Integer
    Dim _do_commit As Boolean
    Dim _url As String
    Dim _api As CLS_API
    Dim response As String

    _sql_transaction = Nothing
    _row_affected = 0
    _do_commit = False

    Try

      If Not GUI_BeginSQLTransaction(_sql_transaction) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      _url = WSI.Common.GeneralParam.GetString("WinUP", "Backend.Url") + "/content/" + Me.Id.ToString()
      _api = New CLS_API(_url)

      response = _api.deleteData()

      _row_affected = 1
      If _row_affected <> 1 Then
        _do_commit = False
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      Else
        _do_commit = True
      End If

    Catch ex As Exception

      _do_commit = False

    Finally

      GUI_EndSQLTransaction(_sql_transaction, _do_commit)
    End Try

    If _do_commit Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
    Else
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

  End Function ' DeleteAds


  Private Function DeleteImage(ByVal ImageId As Integer) As Boolean

    Dim _url As String
    Dim _api As CLS_API
    Dim response As String

    Try

      _url = WSI.Common.GeneralParam.GetString("WinUP", "Backend.Url") + "/image/" + ImageId.ToString()
      _api = New CLS_API(_url)

      response = _api.deleteData()

      Return True

    Catch ex As Exception
      Return False
    End Try

  End Function ' DeleteAds

  Private Function SaveImage(ByVal Image As Image, ByVal ImageId As Integer) As Integer

    Dim _url As String
    Dim _api As CLS_API
    Dim response As String
    Dim imgBackgoundData As Byte()
    Dim fileName As String
    Dim _title As String

    Try

      If Not IsNothing(Image) Then

        _title = DateTime.Now.ToString("yyyyMMddHHmmssffff")
        Dim img As New ImageConverter()
        imgBackgoundData = img.ConvertTo(Image, GetType(Byte()))
        fileName = _title + GetImageType(Image)

        Dim jObjBackground As New JObject
        jObjBackground.Add("Name", _title)
        jObjBackground.Add("fileName", fileName)
        jObjBackground.Add("ContentType", GetMimeType(fileName))
        jObjBackground.Add("Data", imgBackgoundData)

        If ImageId = 0 Then
          _url = WSI.Common.GeneralParam.GetString("WinUP", "Backend.Url") + "/image/SaveImage"
          _api = New CLS_API(_url)
          response = _api.postData(Encoding.UTF8.GetBytes(jObjBackground.ToString()))
        Else
          jObjBackground.Add("ImageId", ImageId)
          _url = WSI.Common.GeneralParam.GetString("WinUP", "Backend.Url") + "/image/" + ImageId.ToString()
          _api = New CLS_API(_url)
          response = _api.putData(Encoding.UTF8.GetBytes(jObjBackground.ToString()))
        End If

        Dim jresponse As JObject = JObject.Parse(response)

        Return jresponse.Item("imageId")

      End If
    Catch ex As Exception
      Return 0
    End Try

  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE : Return the properties of the class in JSON format
  '
  ' PARAMS :
  '     - INPUT :
  '         None
  '
  '     - OUTPUT :
  '         None
  '
  ' RETURNS :
  '     - JSON string
  '
  ' NOTES :

  Private Function ToJSON() As String
    Dim _jObj As New JObject

    _jObj.Add("id", Id)
    _jObj.Add("title", Title)
    _jObj.Add("description", Description)
    _jObj.Add("sectionSchemaId", SectionSchemaId)
    _jObj.Add("order", Order)
    _jObj.Add("status", Status)
    _jObj.Add("descriptionDetail", DescriptionDetails)
    _jObj.Add("detailImageId", DetailImageId)
    _jObj.Add("listImageId", ListImageId)

    Return _jObj.ToString()
  End Function

  Public Shared Function GetSectionSchemaNameById(ByVal SectionSchemaId As Integer) As String


    Dim _sql_trx As SqlTransaction
    Dim _sql_str_select As String
    Dim _command As SqlCommand
    Dim _result As String

    Try

      Using _db_trx As DB_TRX = New DB_TRX()
        _sql_trx = _db_trx.SqlTransaction

        _sql_str_select = " SELECT ML.lo_value AS SS_NAME FROM MAPP_SECTION_SCHEMA SS "
        _sql_str_select += " LEFT JOIN mapp_localization ML on ML.lo_entity_item_id = SS.ss_section_schema_id and ML.lo_resource_id='MainTitle'"
        _sql_str_select += " WHERE SS_SECTION_SCHEMA_ID = " + SectionSchemaId.ToString()


        _command = New SqlCommand( _
            _sql_str_select, _
            _sql_trx.Connection, _sql_trx)

        _result = Convert.ToString(_command.ExecuteScalar)

      End Using

    Catch ex As Exception
      Return String.Empty
    End Try

    Return _result


  End Function

  Public Shared Function GetSectionsByTemplate(ByVal TemplateId As Integer, _
                                               ByVal Method As String) As DataTable

    Dim _url As String
    Dim _api As CLS_API
    Dim dtSectionTemplate As DataTable
    dtSectionTemplate = Nothing
    Dim _response, _responseSanitized As String
    Dim _items() As String = {"backgroundImage", "iconImage", "parent", "childs"}
    Dim _jarray As JArray

    Try

      _url = WSI.Common.GeneralParam.GetString("WinUP", "Backend.Url") + "/sectionsschema/" + Method + "/" + TemplateId.ToString
      _api = New CLS_API(_url)
      _response = _api.getData()

      _responseSanitized = _api.sanitizeData(_response)
      _jarray = _api.removeItems(JArray.Parse(_responseSanitized), _items)
      dtSectionTemplate = JsonConvert.DeserializeObject(Of DataTable)(_jarray.ToString())

    Catch
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7662) + " " + GLB_NLS_GUI_PLAYER_TRACKING.Id(7661), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(7662), GLB_NLS_GUI_PLAYER_TRACKING.GetString(7661))

    End Try

    Return dtSectionTemplate

  End Function ' GetSectionsByTemplate

  Public Shared Function GetSectionsByParentID(ByVal ParentId As Integer) As DataTable

    Dim _url As String
    Dim _api As CLS_API
    Dim dtSectionTemplate As DataTable
    dtSectionTemplate = Nothing
    Dim _response, _responseSanitized As String
    Dim _items() As String = {"backgroundImage", "iconImage", "parent", "childs"}
    Dim _jarray As JArray

    Try

      _url = WSI.Common.GeneralParam.GetString("WinUP", "Backend.Url") + "/sectionsschema/GetByParentId/" + ParentId.ToString
      _api = New CLS_API(_url)
      _response = _api.getData()

      If _response = "error" Then
        Return dtSectionTemplate
      End If
      _responseSanitized = _api.sanitizeData(_response)
      _jarray = _api.removeItems(JArray.Parse(_responseSanitized), _items)
      dtSectionTemplate = JsonConvert.DeserializeObject(Of DataTable)(_jarray.ToString())

    Catch
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7662) + " " + GLB_NLS_GUI_PLAYER_TRACKING.Id(7661), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(7662), GLB_NLS_GUI_PLAYER_TRACKING.GetString(7661))

    End Try
    Return dtSectionTemplate

  End Function ' GetSectionsByTemplate
  Public Shared Function GetSectionsByParentId(ByVal ParentId As String, ByVal Sections As DataTable) As DataTable
    Dim dtSections As DataTable = New DataTable()
    Try
      dtSections = Sections.Select("ParentId = " + ParentId).ListToDataTable
    Catch ex As Exception
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7662) + " " + GLB_NLS_GUI_PLAYER_TRACKING.Id(7661), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(7662), GLB_NLS_GUI_PLAYER_TRACKING.GetString(7661))

    End Try

    Return dtSections

  End Function ' GetSectionsByTemplate
  Public Shared Function GetSectionsByParentName(ByVal ParentName As String) As DataTable
    Dim dtSectionTemplate As DataTable = New DataTable()

    Try
      Dim _sql_command As SqlCommand = New SqlCommand()

      _sql_command.CommandText = "GetSectionCanHaveContent"
      _sql_command.CommandType = CommandType.StoredProcedure
      _sql_command.Parameters.Add("@pSectionName", SqlDbType.NVarChar).Value = ParentName

      dtSectionTemplate = GUI_GetTableUsingCommand(_sql_command, Integer.MaxValue)


    Catch
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7662) + " " + GLB_NLS_GUI_PLAYER_TRACKING.Id(7661), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(7662), GLB_NLS_GUI_PLAYER_TRACKING.GetString(7661))

    End Try

    Return dtSectionTemplate

  End Function ' GetSectionsByTemplate
  Public Shared Function HasChildPromotionalTemplateByParentId(ByVal parentId As Integer) As Boolean

    Dim _url As String
    Dim _api As CLS_API
    Dim _response As String
    Dim result As Boolean = False

    Try

      _url = WSI.Common.GeneralParam.GetString("WinUP", "Backend.Url") + "/sectionsschema/HasChildPromotionalTemplateByParentId/" + parentId.ToString
      _api = New CLS_API(_url)
      _response = _api.getData()


      If (_response = "true") Then
        result = True
      End If

    Catch
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7662) + " " + GLB_NLS_GUI_PLAYER_TRACKING.Id(7661), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(7662), GLB_NLS_GUI_PLAYER_TRACKING.GetString(7661))

    End Try

    Return result

  End Function ' HasChildPromotionalTemplateByParentId

#End Region

End Class
