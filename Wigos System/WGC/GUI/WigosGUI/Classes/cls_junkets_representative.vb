﻿'-------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_junket_representative.vb
' DESCRIPTION:   Junket Class
' AUTHOR:        David Perelló
' CREATION DATE: 30-MAR-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 30-MAR-2017  DPC    Initial Version
' 27-JUL-2017  DPC    WIGOS-4050: Junkets: commission amount is not showing the real value
'--------------------------------------------------------------------
Imports GUI_CommonOperations
Imports System.Runtime.InteropServices
Imports System.Xml
Imports WSI.Common
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Text
Imports System.Data.SqlClient

Public Class CLASS_JUNKETS_REPRESENTATIVE
  Inherits CLASS_BASE

#Region " Structures "

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_JUNKET_REPRESENTATIVE
    Public id As Int64
    Public code As String
    Public name As String
    Public internal_employee As Boolean
    Public enabled As Boolean
    Public deposit_amount As Decimal
    Public comments As String
    Public creation As Date
    Public update As Date

    Public Sub New()
      Me.deposit_amount = 0
      Me.creation = GUI_FormatDate(WGDB.Now, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT)
      Me.update = GUI_FormatDate(WGDB.Now, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT)
    End Sub
  End Class

#End Region ' Structures

#Region " Members "

  Protected m_junket As New TYPE_JUNKET_REPRESENTATIVE

#End Region ' Members

#Region " Enums "

  Public Enum FilterOption
    Yes = 0
    No = 1
    Both = 2
  End Enum

#End Region ' Enums

#Region " Properties "

  Public Property Id() As String
    Get
      Return m_junket.id
    End Get
    Set(ByVal value As String)
      m_junket.id = value
    End Set
  End Property

  Public Property Code() As String
    Get
      Return m_junket.code
    End Get
    Set(ByVal value As String)
      m_junket.code = value
    End Set
  End Property

  Public Property Name() As String
    Get
      Return m_junket.name
    End Get
    Set(ByVal value As String)
      m_junket.name = value
    End Set
  End Property

  Public Property InternalEmployee() As Boolean
    Get
      Return m_junket.internal_employee
    End Get
    Set(ByVal value As Boolean)
      m_junket.internal_employee = value
    End Set
  End Property

  Public Property Enabled() As Boolean
    Get
      Return m_junket.enabled
    End Get
    Set(ByVal value As Boolean)
      m_junket.enabled = value
    End Set
  End Property

  Public Property DepositAmount() As Decimal
    Get
      Return m_junket.deposit_amount
    End Get
    Set(ByVal value As Decimal)
      m_junket.deposit_amount = value
    End Set
  End Property

  Public Property Comments() As String
    Get
      Return m_junket.comments
    End Get
    Set(ByVal value As String)
      m_junket.comments = value
    End Set
  End Property

  Public Property Creation() As DateTime
    Get
      Return m_junket.creation
    End Get
    Set(ByVal value As DateTime)
      m_junket.creation = value
    End Set
  End Property

  Public Property Update() As DateTime
    Get
      Return m_junket.update
    End Get
    Set(ByVal value As DateTime)
      m_junket.update = value
    End Set
  End Property

#End Region ' Properties

#Region " Overrides "

  Public Overrides Function AuditorData() As CLASS_AUDITOR_DATA

    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _yes_text As String
    Dim _no_text As String

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_JUNKETS)

    _yes_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(278)
    _no_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)

    Call _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(8089), Me.Code)

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5710), Code)

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(4399), Name)

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8000), IIf(InternalEmployee, _yes_text, _no_text))

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(3008), IIf(Enabled, _yes_text, _no_text))

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(628), GUI_FormatCurrency(DepositAmount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT))

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1716), IIf(String.IsNullOrEmpty(Comments), AUDIT_NONE_STRING, Comments))

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7985), GUI_FormatDate(Creation, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT))

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7986), GUI_FormatDate(Update, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT))

    Return _auditor_data

  End Function

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As CLASS_BASE.ENUM_STATUS

  End Function

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As CLASS_BASE.ENUM_STATUS
    Return InsertRepresentativeJunket()
  End Function

  Public Overrides Function DB_Read(ObjectId As Object, ByRef SqlCtx As Integer) As CLASS_BASE.ENUM_STATUS
    Return GetRepresentativeJunket(ObjectId)
  End Function

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As CLASS_BASE.ENUM_STATUS
    Return UpdateRepresentativeJunket()
  End Function

  Public Overrides Function Duplicate() As CLASS_BASE

    Dim _tmp_junket As CLASS_JUNKETS_REPRESENTATIVE

    _tmp_junket = New CLASS_JUNKETS_REPRESENTATIVE

    _tmp_junket.Id = m_junket.id
    _tmp_junket.Code = m_junket.code
    _tmp_junket.Name = m_junket.name
    _tmp_junket.InternalEmployee = m_junket.internal_employee
    _tmp_junket.Enabled = m_junket.enabled
    _tmp_junket.DepositAmount = m_junket.deposit_amount
    _tmp_junket.Comments = m_junket.comments
    _tmp_junket.Creation = m_junket.creation
    _tmp_junket.Update = m_junket.update

    Return _tmp_junket

  End Function

#End Region

#Region " DB Actions "

  ''' <summary>
  ''' Insert a new junket
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function InsertRepresentativeJunket() As Integer

    Dim _sb As StringBuilder
    Dim _exist_code As Boolean
    Dim _sql_trx As SqlTransaction = Nothing

    Try

      If Not GUI_BeginSQLTransaction(_sql_trx) Then

        Return ENUM_STATUS.STATUS_ERROR
      End If

      _sb = New StringBuilder
      _sb.AppendLine(" SELECT   COUNT(1)                      ")
      _sb.AppendLine("   FROM   JUNKETS_REPRESENTATIVES       ")
      _sb.AppendLine("  WHERE   JR_CODE = @pCode              ")

      Using _cmd As New SqlClient.SqlCommand(_sb.ToString(), _sql_trx.Connection, _sql_trx)

        _cmd.Parameters.Add("@pCode", SqlDbType.NVarChar).Value = m_junket.code
        _exist_code = (_cmd.ExecuteScalar() > 0)

        If _exist_code Then
          Return ENUM_STATUS.STATUS_DUPLICATE_KEY
        End If

        _sb = New StringBuilder
        _sb.AppendLine(" INSERT INTO   JUNKETS_REPRESENTATIVES           ")
        _sb.AppendLine("             ( JR_CODE                           ")
        _sb.AppendLine("             , JR_NAME                           ")
        _sb.AppendLine("             , JR_INTERNAL                       ")
        _sb.AppendLine("             , JR_STATUS                         ")
        _sb.AppendLine("             , JR_DEPOSIT_AMOUNT                 ")
        _sb.AppendLine("             , JR_COMMENT                        ")
        _sb.AppendLine("             , JR_CREATION                       ")
        _sb.AppendLine("             , JR_UPDATE )                       ")
        _sb.AppendLine("      VALUES                                     ")
        _sb.AppendLine("             ( @pCode                            ")
        _sb.AppendLine("             , @pName                            ")
        _sb.AppendLine("             , @pInternal                        ")
        _sb.AppendLine("             , @pStatus                          ")
        _sb.AppendLine("             , @pDeporitAmount                   ")
        _sb.AppendLine("             , @pComment                         ")
        _sb.AppendLine("             , @pCreation                        ")
        _sb.AppendLine("             , @pUpdate )                        ")

        _cmd.CommandText = _sb.ToString()

        _cmd.Parameters.Add("@pName", SqlDbType.NVarChar).Value = m_junket.name
        _cmd.Parameters.Add("@pInternal", SqlDbType.Bit).Value = m_junket.internal_employee
        _cmd.Parameters.Add("@pStatus", SqlDbType.Bit).Value = m_junket.enabled
        _cmd.Parameters.Add("@pDeporitAmount", SqlDbType.Money).Value = m_junket.deposit_amount
        _cmd.Parameters.Add("@pComment", SqlDbType.NVarChar).Value = m_junket.comments
        _cmd.Parameters.Add("@pCreation", SqlDbType.DateTime).Value = GUI_FormatDate(WGDB.Now, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
        _cmd.Parameters.Add("@pUpdate", SqlDbType.DateTime).Value = GUI_FormatDate(WGDB.Now, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

        If _cmd.ExecuteNonQuery() <> 1 Then
          GUI_EndSQLTransaction(_sql_trx, False)

          Return ENUM_STATUS.STATUS_ERROR
        End If

        If Not GUI_EndSQLTransaction(_sql_trx, True) Then
          Return ENUM_STATUS.STATUS_ERROR
        End If

      End Using

    Catch _ex As Exception
      '  ' Rollback  
      GUI_EndSQLTransaction(_sql_trx, False)

      Return ENUM_STATUS.STATUS_ERROR
    End Try

    Return ENUM_STATUS.STATUS_OK

  End Function ' InsertJunket

  ''' <summary>
  ''' Get a representative junket
  ''' </summary>
  ''' <param name="ObjectId"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function GetRepresentativeJunket(Code As String) As Integer

    Dim _sb As StringBuilder

    Try

      _sb = New StringBuilder
      _sb.AppendLine("    SELECT   JR_ID                                                                                                                     ")
      _sb.AppendLine("           , JR_CODE                                                                                                                   ")
      _sb.AppendLine("           , JR_NAME                                                                                                                   ")
      _sb.AppendLine("           , JR_INTERNAL                                                                                                               ")
      _sb.AppendLine("           , JR_STATUS                                                                                                                 ")
      _sb.AppendLine("           , ISNULL(SUM(JCM_AMOUNT),0) AS JR_DEPOSIT_AMOUNT                                                                            ")
      _sb.AppendLine("           , JR_DEPOSIT_AMOUNT                                                                                                         ")
      _sb.AppendLine("           , JR_COMMENT                                                                                                                ")
      _sb.AppendLine("           , JR_CREATION                                                                                                               ")
      _sb.AppendLine("           , JR_UPDATE                                                                                                                 ")
      _sb.AppendLine("      FROM   JUNKETS_REPRESENTATIVES JR                                                                                                ")
      _sb.AppendLine(" LEFT JOIN   JUNKETS J ON J.JU_REPRESENTATIVE_ID = JR.JR_ID                                                                            ")
      _sb.AppendLine(" LEFT JOIN   JUNKETS_FLYERS JF ON JF.JF_JUNKET_ID = J.JU_ID                                                                            ")
      _sb.AppendLine(" LEFT JOIN   JUNKETS_COMMISSIONS_MOVEMENTS JCM ON JCM.JCM_FLYER_ID = JF_ID                                                             ")
      _sb.AppendLine(" LEFT JOIN   ACCOUNTS AC ON JCM_ACCOUNT_ID = AC_ACCOUNT_ID                                                                             ")
      _sb.AppendLine("     WHERE   JR_CODE = @pCode                                                                                                          ")
      _sb.AppendLine("       AND   ISNULL(AC_TYPE, 0) NOT IN(@pAccountVirtualCashier, @AccountVirtualTerminal)                                               ")
      _sb.AppendLine("  GROUP BY   JR.JR_ID,JR.JR_CODE,JR.JR_NAME,JR.JR_INTERNAL,JR.JR_STATUS,JR.JR_DEPOSIT_AMOUNT,JR.JR_COMMENT,JR.JR_CREATION,JR.JR_UPDATE ")

      Using _db_trx As New WSI.Common.DB_TRX()
        Using _cmd As New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _cmd.Parameters.Add("@pCode", SqlDbType.NVarChar).Value = Code
          _cmd.Parameters.Add("@pAccountVirtualCashier", SqlDbType.Int).Value = AccountType.ACCOUNT_VIRTUAL_CASHIER
          _cmd.Parameters.Add("@AccountVirtualTerminal", SqlDbType.Int).Value = AccountType.ACCOUNT_VIRTUAL_TERMINAL

          Using _reader As SqlDataReader = _cmd.ExecuteReader()

            If (Not _reader.Read) Then
              Return ENUM_STATUS.STATUS_NOT_FOUND
            Else

              m_junket.id = _reader("JR_ID")
              m_junket.code = _reader("JR_CODE")
              m_junket.name = _reader("JR_NAME")
              m_junket.internal_employee = _reader("JR_INTERNAL")
              m_junket.enabled = _reader("JR_STATUS")
              m_junket.deposit_amount = _reader("JR_DEPOSIT_AMOUNT")
              m_junket.comments = _reader("JR_COMMENT")
              m_junket.creation = _reader("JR_CREATION")
              m_junket.update = _reader("JR_UPDATE")

            End If
          End Using
        End Using
      End Using

    Catch _ex As Exception

      Return ENUM_STATUS.STATUS_ERROR
    End Try

    Return ENUM_STATUS.STATUS_OK

  End Function ' GetRepresentativeJunket

  ''' <summary>
  ''' Update a representative junket
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function UpdateRepresentativeJunket() As Integer

    Dim _sb As StringBuilder
    Dim _sql_trx As System.Data.SqlClient.SqlTransaction = Nothing

    Try

      If Not GUI_BeginSQLTransaction(_sql_trx) Then
        Return ENUM_STATUS.STATUS_ERROR
      End If

      _sb = New StringBuilder
      _sb.AppendLine("  UPDATE   JUNKETS_REPRESENTATIVES                  ")
      _sb.AppendLine("     SET   JR_NAME = @pName                         ")
      _sb.AppendLine("         , JR_INTERNAL = @pInternal                 ")
      _sb.AppendLine("         , JR_STATUS = @pStatus                     ")
      _sb.AppendLine("         , JR_DEPOSIT_AMOUNT = @pDeporitAmount      ")
      _sb.AppendLine("         , JR_COMMENT = @pComment                   ")
      _sb.AppendLine("         , JR_UPDATE = @pUpdate                     ")
      _sb.AppendLine("   WHERE   JR_CODE = @pCode                         ")

      Using _cmd As New SqlClient.SqlCommand(_sb.ToString(), _sql_trx.Connection, _sql_trx)

        _cmd.Parameters.Add("@pCode", SqlDbType.NVarChar).Value = Code
        _cmd.Parameters.Add("@pName", SqlDbType.NVarChar).Value = m_junket.name
        _cmd.Parameters.Add("@pInternal", SqlDbType.Bit).Value = m_junket.internal_employee
        _cmd.Parameters.Add("@pStatus", SqlDbType.Bit).Value = m_junket.enabled
        _cmd.Parameters.Add("@pDeporitAmount", SqlDbType.Money).Value = m_junket.deposit_amount
        _cmd.Parameters.Add("@pComment", SqlDbType.NVarChar).Value = m_junket.comments
        _cmd.Parameters.Add("@pUpdate", SqlDbType.DateTime).Value = GUI_FormatDate(WGDB.Now, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

        If _cmd.ExecuteNonQuery() <> 1 Then
          GUI_EndSQLTransaction(_sql_trx, False)

          Return ENUM_STATUS.STATUS_ERROR
        End If

        If Not GUI_EndSQLTransaction(_sql_trx, True) Then
          Return ENUM_STATUS.STATUS_ERROR
        End If

      End Using

    Catch _ex As Exception
      '  ' Rollback  
      GUI_EndSQLTransaction(_sql_trx, False)

      Return ENUM_STATUS.STATUS_ERROR
    End Try

    Return ENUM_STATUS.STATUS_OK

  End Function ' UpdateRepresentativeJunket

  ''' <summary>
  ''' Check if representative junket exist by code
  ''' </summary>
  ''' <param name="Code"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ExistRepresentativeJunket(Code As String) As Integer

    Dim _sb As StringBuilder
    Dim _exist_code As Boolean
    Dim _sql_trx As SqlTransaction = Nothing

    Try

      _sb = New StringBuilder
      _sb.AppendLine(" SELECT   COUNT(1)                      ")
      _sb.AppendLine("   FROM   JUNKETS_REPRESENTATIVES       ")
      _sb.AppendLine("  WHERE   JR_CODE = @pCode              ")

      Using _db_trx As New WSI.Common.DB_TRX()
        Using _cmd As New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _cmd.Parameters.Add("@pCode", SqlDbType.NVarChar).Value = Code
          _exist_code = (_cmd.ExecuteScalar() > 0)

          If _exist_code Then
            Return ENUM_STATUS.STATUS_DUPLICATE_KEY
          End If

        End Using
      End Using

    Catch _ex As Exception

      Return ENUM_STATUS.STATUS_ERROR
    End Try

    Return ENUM_STATUS.STATUS_OK

  End Function ' ExistRepresentativeJunket

  ''' <summary>
  ''' Check if the representative junket have a junkets associated
  ''' </summary>
  ''' <param name="Code"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function HaveJunketsAssociated(Code As Int64, ByRef HaveJunkets As Boolean) As Boolean

    Dim _sb As StringBuilder
    Dim _sql_trx As SqlTransaction = Nothing

    Try

      _sb = New StringBuilder
      _sb.AppendLine("   SELECT   COUNT(1)                                        ")
      _sb.AppendLine("     FROM   JUNKETS                                         ")
      _sb.AppendLine("    WHERE   JU_REPRESENTATIVE_ID = @pRepresentativeId       ")
      _sb.AppendLine("      AND   GETDATE() BETWEEN JU_DATE_FROM AND JU_DATE_TO   ")

      Using _db_trx As New WSI.Common.DB_TRX()
        Using _cmd As New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _cmd.Parameters.Add("@pRepresentativeId", SqlDbType.BigInt).Value = Code

          HaveJunkets = (_cmd.ExecuteScalar() > 0)

          Return True

        End Using
      End Using

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return False

  End Function ' HaveJunketsAssociated

  ''' <summary>
  ''' Check if the representative is active
  ''' </summary>
  ''' <param name="Code"></param>
  ''' <param name="Enabled"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function IsEnabled(Code As Int64, ByRef Enabled As Boolean) As Boolean

    Dim _sb As StringBuilder
    Dim _sql_trx As SqlTransaction = Nothing

    Try

      _sb = New StringBuilder
      _sb.AppendLine("   SELECT   JR_STATUS                    ")
      _sb.AppendLine("     FROM   JUNKETS_REPRESENTATIVES      ")
      _sb.AppendLine("    WHERE   JR_ID = @pRepresentativeId   ")

      Using _db_trx As New WSI.Common.DB_TRX()
        Using _cmd As New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _cmd.Parameters.Add("@pRepresentativeId", SqlDbType.BigInt).Value = Code

          Enabled = (_cmd.ExecuteScalar())

          Return True

        End Using
      End Using

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return False

  End Function ' HaveJunketsAssociated

#End Region

#Region " Public Methods "

  ''' <summary>
  ''' String Query for get the junkets representatives
  ''' </summary>
  ''' <param name="Code"></param>
  ''' <param name="Name"></param>
  ''' <param name="Internal"></param>
  ''' <param name="Status"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function GetRepresentatives(Code As String, Name As String, Internal As FilterOption, Status As FilterOption) As String

    Dim _sql_str

    _sql_str = New StringBuilder()
    _sql_str.AppendLine("    SELECT   JR_CODE                                                       ")
    _sql_str.AppendLine("           , JR_NAME                                                       ")
    _sql_str.AppendLine("           , JR_INTERNAL                                                   ")
    _sql_str.AppendLine("           , JR_STATUS                                                     ")
    _sql_str.AppendLine("           , ISNULL(SUM(JCM_AMOUNT),0) AS JR_DEPOSIT_AMOUNT                ")
    _sql_str.AppendLine("           , JR_CREATION                                                   ")
    _sql_str.AppendLine("           , JR_ID                                                         ")
    _sql_str.AppendLine("      FROM   JUNKETS_REPRESENTATIVES AS JR                                 ")
    _sql_str.AppendLine(" LEFT JOIN   JUNKETS J ON J.JU_REPRESENTATIVE_ID = JR.JR_ID                ")
    _sql_str.AppendLine(" LEFT JOIN   JUNKETS_FLYERS JF ON JF.JF_JUNKET_ID = J.JU_ID                ")
    _sql_str.AppendLine(" LEFT JOIN   JUNKETS_COMMISSIONS_MOVEMENTS JCM ON JCM.JCM_FLYER_ID = JF_ID ")
    _sql_str.AppendLine(" LEFT JOIN   ACCOUNTS AC ON JCM_ACCOUNT_ID = AC_ACCOUNT_ID                 ")
    _sql_str.AppendLine("     WHERE   1 = 1                                                         ")
    _sql_str.AppendLine(String.Format("       AND   ISNULL(AC_TYPE, 0) NOT IN({0}, {1})                           ",
                                      Int32.Parse(AccountType.ACCOUNT_VIRTUAL_CASHIER), Int32.Parse(AccountType.ACCOUNT_VIRTUAL_TERMINAL)))

    If Not String.IsNullOrEmpty(Code) Then
      _sql_str.AppendLine(String.Format("       AND   {0}", GUI_FilterField("JR_CODE", Code, False, False, True)))
    End If

    If Not String.IsNullOrEmpty(Name) Then
      _sql_str.AppendLine(String.Format("       AND   {0}", GUI_FilterField("JR_NAME", Name, False, False, True)))
    End If

    Select Case Internal
      Case FilterOption.Yes
        _sql_str.AppendLine("       AND   JR_INTERNAL = 1 ")
      Case FilterOption.No
        _sql_str.AppendLine("       AND   JR_INTERNAL = 0 ")
    End Select

    Select Case Status
      Case FilterOption.Yes
        _sql_str.AppendLine("       AND   JR_STATUS = 1 ")
      Case FilterOption.No
        _sql_str.AppendLine("       AND   JR_STATUS = 0 ")
    End Select

    _sql_str.AppendLine("  GROUP BY   JR.JR_CODE, JR.JR_NAME, JR.JR_INTERNAL, JR.JR_STATUS, JR.JR_DEPOSIT_AMOUNT, JR.JR_CREATION, JR.JR_ID ")
    _sql_str.AppendLine("     ORDER   BY JR_CODE ")

    Return _sql_str.ToString

  End Function ' GetRepresentatives

#End Region ' Public Methods

End Class
