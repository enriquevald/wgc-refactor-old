'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : cls_stacker.vb
'
' DESCRIPTION : stacker class for user edition
'              
' REVISION HISTORY :
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 26-JUN-2012  NMR    Initial version
' 21-AUG-2013  JRM    Refactoring to company style
' 03-DEC-2013  AMF    status collection
' 30-DEC-2013  JRM    Fixed Bug WIGOSTITO-919: Auditing error
' 07-JAN-2014  DRV    Data not saved in the database as NULL value also added unassign stacker
'-------------------------------------------------------------------

Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports GUI_CommonOperations
Imports GUI_Controls
Imports GUI_CommonMisc
Imports WSI.Common
Imports System.Text


Public Class CLASS_STACKER
  Inherits CLASS_BASE

#Region " Structures "

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_STACKER
    Public stacker_id As Int64
    Public model As String
    Public status As TITO_STACKER_STATUS
    Public status_collection As TITO_MONEY_COLLECTION_STATUS
    Public inserted_terminal As Int64
    Public preassigned_terminal As Int64
    Public notes As String

    Public Sub New()
      stacker_id = 0
      status = TITO_STACKER_STATUS.ACTIVE
      status_collection = TITO_MONEY_COLLECTION_STATUS.NONE
    End Sub

  End Class

#End Region 'Structures

#Region " Members "

  Protected m_stacker As New TYPE_STACKER

#End Region

#Region " Properties "

  Public Property StackerId() As Int64
    Get
      Return m_stacker.stacker_id
    End Get

    Set(ByVal Value As Int64)
      m_stacker.stacker_id = Value
    End Set
  End Property

  Public Property Model() As String
    Get
      Return m_stacker.model
    End Get

    Set(ByVal Value As String)
      m_stacker.model = Value
    End Set
  End Property

  Public Property Status() As TITO_STACKER_STATUS
    Get
      Return m_stacker.status
    End Get

    Set(ByVal Value As TITO_STACKER_STATUS)
      m_stacker.status = Value
    End Set
  End Property

  Public Property StatusCollection() As TITO_MONEY_COLLECTION_STATUS
    Get
      Return m_stacker.status_collection
    End Get

    Set(ByVal Value As TITO_MONEY_COLLECTION_STATUS)
      m_stacker.status_collection = Value
    End Set
  End Property

  Public Property InsertedTerminal() As Int64
    Get
      Return m_stacker.inserted_terminal
    End Get

    Set(ByVal Value As Int64)
      m_stacker.inserted_terminal = Value
    End Set
  End Property

  Public Property PreassignedTerminal() As Int64
    Get
      Return m_stacker.preassigned_terminal
    End Get

    Set(ByVal Value As Int64)
      m_stacker.preassigned_terminal = Value
    End Set
  End Property

  Public Property Notes() As String
    Get
      Return m_stacker.notes
    End Get

    Set(ByVal Value As String)
      m_stacker.notes = Value
    End Set
  End Property

#End Region

#Region " Overrides functions "

  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As ENUM_STATUS

    Return ReadStacker(ObjectId, SqlCtx)

  End Function ' DB_Read

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As ENUM_STATUS

    Return UpdateStacker(SqlCtx)

  End Function ' DB_Update

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As ENUM_STATUS
    Dim _rc As Integer

    ' Make sure that stacker with same id does not already exist
    If ExistsDuplicateStakers(1) Then

      Return ENUM_STATUS.STATUS_DUPLICATE_KEY
    End If

    ' Insert stacker data
    _rc = InsertStacker(SqlCtx)

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_PASSWORD
        Return CLASS_BASE.ENUM_STATUS.STATUS_PASSWORD

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
        Return ENUM_STATUS.STATUS_DUPLICATE_KEY

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Insert

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

  End Function ' DB_Delete

  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim _tmp_stacker As CLASS_STACKER

    _tmp_stacker = New CLASS_STACKER
    _tmp_stacker.m_stacker.stacker_id = m_stacker.stacker_id
    _tmp_stacker.m_stacker.model = m_stacker.model
    _tmp_stacker.m_stacker.status = m_stacker.status
    _tmp_stacker.m_stacker.status_collection = m_stacker.status_collection
    _tmp_stacker.m_stacker.inserted_terminal = m_stacker.inserted_terminal
    _tmp_stacker.m_stacker.preassigned_terminal = m_stacker.preassigned_terminal
    _tmp_stacker.m_stacker.notes = m_stacker.notes

    Return _tmp_stacker

  End Function ' Duplicate

  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim _auditor_data As CLASS_AUDITOR_DATA

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_TERMINALS)

    Call _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(2199), m_stacker.stacker_id)       '"StackerId"
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2180), m_stacker.stacker_id)       '"Identificador"

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2181), m_stacker.model)    '"Modelo"
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2117), mdl_tito.StackerStatusAsString(m_stacker.status))    '"Estado"
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2822), IIf(m_stacker.preassigned_terminal > 0, mdl_tito.GetTerminalName(m_stacker.preassigned_terminal), AUDIT_NONE_STRING))  '"Terminal Preasignado"
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2185), IIf(m_stacker.inserted_terminal > 0, mdl_tito.GetTerminalName(m_stacker.inserted_terminal), AUDIT_NONE_STRING))    '"Insertado"
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2188), IIf(String.IsNullOrEmpty(m_stacker.notes), AUDIT_NONE_STRING, m_stacker.notes))    '"Notas"

    Return _auditor_data

  End Function ' AuditorData

#End Region

#Region " Private Functions and Methods "

  ' PURPOSE: Check if in the database exists another stacker with the same Key proerties
  '
  ' PARAMS:
  '   - INPUT:
  '     - Model: Model name
  '     - ObjectId: An object reference
  '
  ' RETURNS:
  '     - True: duplicate exists
  '     - False: there aren't duplicates
  Private Function ExistsDuplicateStakers(ByVal Allowed As Int32) As Boolean

    Dim _sql_query As StringBuilder
    Dim _sql_command As SqlCommand
    Dim _data_table As DataTable

    _sql_query = New StringBuilder()

    ' Check whether there is no other stacker with the same name or there is another stacker but it is deleted
    _sql_query.AppendLine(" SELECT   COUNT(ST_STACKER_ID)         ")
    _sql_query.AppendLine("   FROM   STACKERS                     ")
    _sql_query.AppendLine("  WHERE   ST_STACKER_ID = @pStackerId  ")
    _sql_query.AppendLine("    AND   ST_STATUS <> @pStatusDisabled")

    _sql_command = New SqlCommand(_sql_query.ToString())
    _sql_command.Parameters.Add("@pStackerId", SqlDbType.BigInt).Value = m_stacker.stacker_id
    _sql_command.Parameters.Add("@pStatusDisabled", SqlDbType.Int).Value = TITO_STACKER_STATUS.DISABLED
    _data_table = GUI_GetTableUsingCommand(_sql_command, Allowed + 1)

    If IsNothing(_data_table) Then

      Return True
    End If

    If _data_table.Rows.Count() > 0 AndAlso _data_table.Rows(0).Item(0) >= Allowed Then

      Return True
    End If

    Return False

  End Function

  Private Function InsertStacker(ByVal Context As Int32) As ENUM_STATUS
    Dim _sql_trx As SqlTransaction = Nothing
    Dim _sb As StringBuilder

    If Not GUI_BeginSQLTransaction(_sql_trx) Then

      Return ENUM_STATUS.STATUS_ERROR
    End If

    Try
      _sb = New StringBuilder()

      _sb.AppendLine("IF( NOT EXISTS (SELECT   1                                          ")
      _sb.AppendLine("                  FROM   STACKERS WHERE ST_STACKER_ID = @pStackerId ")
      _sb.AppendLine("                   AND   ST_STATUS = @pStatusDisabled))             ")
      _sb.AppendLine("  INSERT INTO STACKERS ( ST_STACKER_ID           ")
      _sb.AppendLine("                       , ST_MODEL                ")
      _sb.AppendLine("                       , ST_STATUS               ")
      _sb.AppendLine("                       , ST_INSERTED             ")
      _sb.AppendLine("                       , ST_TERMINAL_PREASSIGNED ")
      _sb.AppendLine("                       , ST_NOTES )              ")
      _sb.AppendLine("                VALUES ( @pStackerId             ")
      _sb.AppendLine("                       , @pModel                 ")
      _sb.AppendLine("                       , @pStatus                ")
      _sb.AppendLine("                       , @pInserted              ")
      _sb.AppendLine("                       , @pTermPreassigned       ")
      _sb.AppendLine("                       , @pNotes )               ")
      _sb.AppendLine("ELSE                                             ")
      _sb.AppendLine("  UPDATE   STACKERS                                       ")
      _sb.AppendLine("     SET   ST_MODEL                 = @pModel             ")
      _sb.AppendLine("         , ST_STATUS                = @pStatusActive      ")
      _sb.AppendLine("         , ST_INSERTED              = @pInserted          ")
      _sb.AppendLine("         , ST_TERMINAL_PREASSIGNED  = @pTermPreassigned   ")
      _sb.AppendLine("         , ST_NOTES                 = @pNotes             ")
      _sb.AppendLine("   WHERE   ST_STACKER_ID            = @pStackerId         ")

      Using _cmd As New SqlClient.SqlCommand(_sb.ToString(), _sql_trx.Connection, _sql_trx)

        _cmd.Parameters.Add("@pStackerId", SqlDbType.BigInt).Value = m_stacker.stacker_id
        _cmd.Parameters.Add("@pModel", SqlDbType.NVarChar).Value = m_stacker.model
        _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = m_stacker.status
        _cmd.Parameters.Add("@pInserted", SqlDbType.Int).Value = IIf(m_stacker.inserted_terminal > 0, m_stacker.inserted_terminal, DBNull.Value)
        _cmd.Parameters.Add("@pTermPreassigned", SqlDbType.Int).Value = IIf(m_stacker.preassigned_terminal > 0, m_stacker.preassigned_terminal, DBNull.Value)
        _cmd.Parameters.Add("@pNotes", SqlDbType.NVarChar).Value = IIf(m_stacker.notes = "", DBNull.Value, m_stacker.notes)
        _cmd.Parameters.Add("@pStatusDisabled", SqlDbType.Int).Value = TITO_STACKER_STATUS.DISABLED
        _cmd.Parameters.Add("@pStatusActive", SqlDbType.Int).Value = TITO_STACKER_STATUS.ACTIVE

        If _cmd.ExecuteNonQuery() <> 1 Then
          GUI_EndSQLTransaction(_sql_trx, False)

          Return ENUM_STATUS.STATUS_ERROR
        End If

      End Using

      If Not GUI_EndSQLTransaction(_sql_trx, True) Then

        Return ENUM_STATUS.STATUS_ERROR
      End If

    Catch _ex As Exception
      GUI_EndSQLTransaction(_sql_trx, False)

      Return ENUM_STATUS.STATUS_ERROR
    End Try

    Return ENUM_STATUS.STATUS_OK
  End Function ' InsertStacker

  Private Function UpdateStacker(ByVal Context As Int32) As ENUM_STATUS
    Dim _sql_trx As SqlTransaction = Nothing
    Dim _sb As StringBuilder

    If Not GUI_BeginSQLTransaction(_sql_trx) Then

      Return ENUM_STATUS.STATUS_ERROR
    End If
    Try

      _sb = New StringBuilder()

      _sb.AppendLine("UPDATE   STACKERS                                       ")
      _sb.AppendLine("   SET   ST_MODEL                 = @pModel             ")
      _sb.AppendLine("       , ST_STATUS                = @pStatus            ")
      _sb.AppendLine("       , ST_INSERTED              = @pInserted          ")
      _sb.AppendLine("       , ST_TERMINAL_PREASSIGNED  = @pTermPreassigned   ")
      _sb.AppendLine("       , ST_NOTES                 = @pNotes             ")
      _sb.AppendLine(" WHERE   ST_STACKER_ID            = @pStackerId         ")

      Using _cmd As New SqlClient.SqlCommand(_sb.ToString(), _sql_trx.Connection, _sql_trx)

        _cmd.Parameters.Add("@pStackerId", SqlDbType.BigInt).Value = m_stacker.stacker_id
        _cmd.Parameters.Add("@pModel", SqlDbType.NVarChar).Value = m_stacker.model
        _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = m_stacker.status
        _cmd.Parameters.Add("@pInserted", SqlDbType.Int).Value = IIf(m_stacker.inserted_terminal > 0, m_stacker.inserted_terminal, DBNull.Value)
        _cmd.Parameters.Add("@pTermPreassigned", SqlDbType.Int).Value = IIf(m_stacker.preassigned_terminal > 0, m_stacker.preassigned_terminal, DBNull.Value)
        _cmd.Parameters.Add("@pNotes", SqlDbType.NVarChar).Value = IIf(m_stacker.notes = "", DBNull.Value, m_stacker.notes)

        If m_stacker.inserted_terminal = 0 And m_stacker.status_collection <> TITO_MONEY_COLLECTION_STATUS.NONE Then
          If Trx_UnassignStacker(_sql_trx) <> ENUM_STATUS.STATUS_OK Then
            GUI_EndSQLTransaction(_sql_trx, False)

            Return ENUM_STATUS.STATUS_ERROR
          End If
        End If

        If _cmd.ExecuteNonQuery() = 0 Then


          GUI_EndSQLTransaction(_sql_trx, False)

          Return ENUM_STATUS.STATUS_NOT_FOUND
        End If

      End Using

      If Not GUI_EndSQLTransaction(_sql_trx, True) Then

        Return ENUM_STATUS.STATUS_ERROR
      End If

    Catch _ex As Exception
      If Not GUI_EndSQLTransaction(_sql_trx, False) Then

        Return ENUM_STATUS.STATUS_ERROR
      End If
    End Try

    Return ENUM_STATUS.STATUS_OK
  End Function

  Private Function ReadStacker(ByVal ObjectId As Int64, _
                               ByVal Context As Int32) As ENUM_STATUS
    Dim _sb As StringBuilder

    Try

      _sb = New StringBuilder()

      _sb.AppendLine("    SELECT   ST_MODEL")
      _sb.AppendLine("           , ST_STATUS ")
      _sb.AppendLine("           , MC_TERMINAL_ID")
      _sb.AppendLine("           , ST_TERMINAL_PREASSIGNED")
      _sb.AppendLine("           , ST_NOTES")
      _sb.AppendLine("           , MC_STATUS")
      _sb.AppendLine("      FROM   STACKERS ")
      _sb.AppendLine(" LEFT JOIN   MONEY_COLLECTIONS ON MC_STACKER_ID = ST_STACKER_ID AND MC_STATUS IN (@pStatusOpen,@pStatusPending)")
      _sb.AppendLine("     WHERE   ST_STACKER_ID = @pStackerId")
      _sb.AppendLine("  ORDER BY   MC_STATUS ASC ")


      Using _db_trx As New WSI.Common.DB_TRX()
        Using _sql_cmd As New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          _sql_cmd.Parameters.Add("@pStatusOpen", SqlDbType.Int).Value = TITO_MONEY_COLLECTION_STATUS.OPEN
          _sql_cmd.Parameters.Add("@pStatusPending", SqlDbType.Int).Value = TITO_MONEY_COLLECTION_STATUS.PENDING
          _sql_cmd.Parameters.Add("@pStackerId", SqlDbType.BigInt).Value = ObjectId
          Using _reader As SqlDataReader = _sql_cmd.ExecuteReader()
            If (Not _reader.Read) Then

              Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
            Else
              m_stacker.stacker_id = ObjectId
              m_stacker.model = IIf(IsDBNull(_reader("ST_MODEL")), String.Empty, _reader("ST_MODEL"))
              m_stacker.inserted_terminal = mdl_tito.GetDefaultNumber(_reader("MC_TERMINAL_ID"))
              m_stacker.preassigned_terminal = mdl_tito.GetDefaultNumber(_reader("ST_TERMINAL_PREASSIGNED"))
              m_stacker.status = _reader("ST_STATUS")
              m_stacker.status_collection = IIf(IsDBNull(_reader("MC_STATUS")), TITO_MONEY_COLLECTION_STATUS.NONE, _reader("MC_STATUS"))
              m_stacker.notes = IIf(IsDBNull(_reader("ST_NOTES")), String.Empty, _reader("ST_NOTES"))
            End If
          End Using
        End Using
      End Using

    Catch _ex As Exception

      Return ENUM_STATUS.STATUS_ERROR
    End Try

    Return ENUM_STATUS.STATUS_OK
  End Function ' ReadStacker

  Private Function Trx_UnassignStacker(ByRef _db_trx As SqlTransaction) As ENUM_STATUS
    Dim _sb As StringBuilder

    Try
      _sb = New StringBuilder()

      _sb.AppendLine(" UPDATE   MONEY_COLLECTIONS                           ")
      _sb.AppendLine("    SET   MC_STACKER_ID = NULL                        ")
      _sb.AppendLine("  WHERE   MC_STACKER_ID = @pStackerId                 ")
      _sb.AppendLine("    AND   MC_STATUS IN (@pStatusOpen) ")

      Using _sql_cmd As New SqlCommand(_sb.ToString(), _db_trx.Connection, _db_trx)
        _sql_cmd.Parameters.Add("@pStatusOpen", SqlDbType.Int).Value = TITO_MONEY_COLLECTION_STATUS.OPEN
        _sql_cmd.Parameters.Add("@pStackerId", SqlDbType.BigInt).Value = m_stacker.stacker_id

        If (_sql_cmd.ExecuteNonQuery() = 1) Then
          Return ENUM_STATUS.STATUS_OK
        Else
          Return ENUM_STATUS.STATUS_NOT_FOUND
        End If
      End Using

    Catch ex As Exception
      Return ENUM_STATUS.STATUS_ERROR
    End Try

  End Function

#End Region

End Class
