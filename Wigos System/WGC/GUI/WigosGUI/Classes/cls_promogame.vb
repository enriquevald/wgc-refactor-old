﻿'------------------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'------------------------------------------------------------------------------
' 
'   MODULE NAME: PromoGame.cs
' 
'   DESCRIPTION: Miscellaneous utilities for PromoGame.
' 
'        AUTHOR: Rubén Lama
' 
' CREATION DATE: 20-JUL-2017
' 
' REVISION HISTORY:
' 
' Date        Author Description
' ----------- ------ ----------------------------------------------------------
' 20-JUL-2017 RLO    First release.
'------------------------------------------------------------------------------
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports System.Text
Imports WSI.Common
Imports WSI.Common.PromoGame

Public Class CLASS_PROMOGAME
  Inherits CLASS_BASE

#Region " Constants "

#End Region ' Constants

#Region " Enums "

#End Region ' Enums 

#Region " Structures "

#End Region ' Structures

#Region " Members "

  Protected m_promo_game As New PromoGame

#End Region ' Members

#Region " Properties "

  Public Property PromoGame() As PromoGame
    Get
      Return m_promo_game
    End Get
    Set(value As PromoGame)
      m_promo_game = value
    End Set
  End Property


#End Region ' Properties

#Region " Public Functions "

  Public Sub New()


  End Sub

  ''' <summary>
  ''' Returns the related auditor data for the current object.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function AuditorData() As CLASS_AUDITOR_DATA
    Dim _auditor_data As CLASS_AUDITOR_DATA

    ' Set In Touch Promo Game  auditor data
    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_PROMOGAME)
    _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(8494), " - " + Me.PromoGame.Name)

    ' In Touch PromoGame
    Call AuditorData_PromoGameSettings(_auditor_data)

    Return _auditor_data

  End Function ' AuditorData

  ''' <summary>
  ''' Deletes the given object from the database.
  ''' </summary>
  ''' <param name="SqlCtx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As CLASS_BASE.ENUM_STATUS

    Try

      Using _db_trx As New DB_TRX()

        ' Save PromoGame
        If Not Me.PromoGame.Delete(_db_trx.SqlTransaction) Then
          Return ENUM_STATUS.STATUS_ERROR
        End If

        _db_trx.Commit()

      End Using

    Catch ex As Exception
      Log.Error(String.Format("PromoGame DB_Delete: There was an error deleting PromoGameId: {0}", Me.PromoGame.Id))
      Log.Exception(ex)

      Return ENUM_STATUS.STATUS_ERROR
    End Try

    Return ENUM_STATUS.STATUS_OK

  End Function

  ''' <summary>
  ''' Reads from the database into the given object
  ''' </summary>
  ''' <param name="ObjectId"></param>
  ''' <param name="SqlCtx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function DB_Read(ObjectId As Object, ByRef SqlCtx As Integer) As CLASS_BASE.ENUM_STATUS
    Dim _promogame As PromoGame

    Try

      Using _db_trx As New DB_TRX()

        _promogame = New PromoGame(ObjectId, _db_trx.SqlTransaction)

        If _promogame Is Nothing Then
          Return ENUM_STATUS.STATUS_ERROR
        End If

        Me.m_promo_game = _promogame

      End Using

    Catch ex As Exception
      Log.Error(String.Format("PromoGame DB_Read: There was an error getting PromoGameId:{0}", ObjectId))
      Log.Exception(ex)

      Return ENUM_STATUS.STATUS_ERROR

    End Try

    Return ENUM_STATUS.STATUS_OK

  End Function ' DB_Read

  ''' <summary>
  ''' Inserts the given object to the database.
  ''' </summary>
  ''' <param name="SqlCtx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As CLASS_BASE.ENUM_STATUS

    Try

      Using _db_trx As New DB_TRX()

        ' Save PromoGame
        If Not Me.PromoGame.Save(_db_trx.SqlTransaction) Then
          Return ENUM_STATUS.STATUS_ERROR
        End If

        _db_trx.Commit()

      End Using

    Catch ex As Exception
      Log.Error(String.Format("PromoGame DB_Insert: There was an error getting PromoGameId:{0}", Me.PromoGame.Id))
      Log.Exception(ex)

      Return ENUM_STATUS.STATUS_ERROR
    End Try

    Return ENUM_STATUS.STATUS_OK

  End Function ' DB_Insert

  ''' <summary>
  ''' Updates the given object to the database.
  ''' </summary>
  ''' <param name="SqlCtx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As CLASS_BASE.ENUM_STATUS

    Try

      ' Save PromoGame
      If Not Me.PromoGame.Save() Then
        Return ENUM_STATUS.STATUS_ERROR
      End If

    Catch ex As Exception

      Log.Error(String.Format("PromoGame DB_Update: There was an error getting PromoGameId:{0}", Me.PromoGame.Id))
      Log.Exception(ex)

      Return ENUM_STATUS.STATUS_ERROR
    End Try

    Return ENUM_STATUS.STATUS_OK

  End Function ' DB_Update

  ''' <summary>
  ''' Returns a duplicate of the given object.
  ''' Not includes ID
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function Duplicate() As CLASS_BASE

    Dim _temp_object As CLASS_PROMOGAME

    _temp_object = New CLASS_PROMOGAME

    If Not Me.PromoGame Is Nothing Then
      _temp_object.PromoGame = Me.PromoGame.Clone()
    End If

    Return _temp_object

  End Function ' Duplicate


#End Region ' Public Functions

#Region " Private Functions "


  Private Function BooleanToAuditString(ByVal Value As Boolean) As String
    Return IIf(Value, GLB_NLS_GUI_AUDITOR.GetString(336), GLB_NLS_GUI_AUDITOR.GetString(337))
  End Function

#Region " Auditor Data "

  ''' <summary>
  ''' Add In Touch PromoGame settings fields to auditor data
  ''' </summary>
  ''' <param name="AuditorData"></param>
  ''' <remarks></remarks>
  Private Sub AuditorData_PromoGameSettings(ByRef AuditorData As CLASS_AUDITOR_DATA)
    Call AuditorData.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8494), IIf(String.IsNullOrEmpty(Me.PromoGame.Name), AUDIT_NONE_STRING, Me.PromoGame.Name))
    Call AuditorData.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8495), Me.PromoGame.Type) ' Type
    Call AuditorData.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8496), GUI_FormatNumber(Me.PromoGame.Price, 2)) ' Price
    Call AuditorData.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8497), Me.PromoGame.PriceUnits) ' Price Units
    Call AuditorData.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8499), Me.PromoGame.ReturnUnits) ' Return Units
    Call AuditorData.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8500), Me.PromoGame.PercentatgeCostReturn) ' PercentatgeCostReturn
    Call AuditorData.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8501), Me.PromoGame.GameURL) ' Game URL
    Call AuditorData.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8502), Me.BooleanToAuditString(Me.PromoGame.MandatoryIC))   ' Mandatory Inser Card
    Call AuditorData.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8503), Me.BooleanToAuditString(Me.PromoGame.ShowBuyDialog))   ' Show buy dialog    
    Call AuditorData.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8504), Me.BooleanToAuditString(Me.PromoGame.PlayerCanCancel))   ' Player Can Cancel
    Call AuditorData.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8505), Me.PromoGame.AutoCancel) ' AutoCancel
    Call AuditorData.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8506), Me.BooleanToAuditString(Me.PromoGame.TransferScreen))   ' Transfer Screen    
    Call AuditorData.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8507), Me.PromoGame.TransferTimeOut) ' Transfer TimeOut
    Call AuditorData.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8743), Me.PromoGame.ReturnPrice) ' Return cost
  End Sub ' AuditorData_PromoGameSettings

#End Region ' Auditor Data

#End Region ' Private Functions

End Class
