'-------------------------------------------------------------------
' Copyright � 2014 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_progressive_provision.vb
'
' DESCRIPTION:   Progressive Jackpot Provision class
'
' AUTHOR:        Javier Molina
'
' CREATION DATE: 07-AUG-2014
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 07-AUG-2014  JMM    Initial version.
'--------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports WSI.Common
Imports System.Text

Public Class CLASS_PROGRESSIVE_PROVISION
  Inherits CLASS_BASE

#Region " Constants "

  ' Fields length
  Public Const MAX_PROGRESSIVE_NAME_LEN As Integer = 50

#End Region ' Constants

#Region "Structure"

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_PROGRESSIVE_PROVISION
    Public provision_id As Int64
    Public progressive_id As Int64
    Public progressive_name As String
    Public creation_date As DateTime
    Public hour_from As DateTime
    Public hour_to As DateTime
    Public amount As Decimal
    Public theoretical_amount As Decimal
    Public levels As DataTable
    Public terminals As DataTable
    Public cage_session_id As Int64
    Public total_cage_provision As Decimal
    Public total_termianls_provision As Decimal
    Public total_termianls_played As Decimal
    Public progressive_last_provisioned_hour As DateTime
    Public status As Integer
    Public gui_user_id As Integer
    Public gui_user_name As String
    Public current_amount As Decimal
  End Class

#End Region ' Structure

#Region "Constructor / Destructor"

  Public Sub New()
    Call MyBase.New()
  End Sub

  Protected Overrides Sub Finalize()
    MyBase.Finalize()

  End Sub

#End Region ' Constructor / Destructor

#Region " Members "

  Protected m_progressive_provision As New TYPE_PROGRESSIVE_PROVISION

#End Region ' Members

#Region "Properties"

  Property ProvisionId() As Int64
    Get
      Return m_progressive_provision.provision_id
    End Get
    Set(ByVal Value As Int64)
      m_progressive_provision.provision_id = Value
    End Set
  End Property

  Property ProgressiveId() As Int64
    Get
      Return m_progressive_provision.progressive_id
    End Get
    Set(ByVal Value As Int64)
      m_progressive_provision.progressive_id = Value
    End Set
  End Property

  Property ProgressiveName() As String
    Get
      Return m_progressive_provision.progressive_name
    End Get
    Set(ByVal Value As String)
      m_progressive_provision.progressive_name = Value
    End Set
  End Property

  Property CreationDate() As DateTime
    Get
      Return m_progressive_provision.creation_date
    End Get
    Set(ByVal Value As DateTime)
      m_progressive_provision.creation_date = Value
    End Set
  End Property

  Property HourFrom() As DateTime
    Get
      Return m_progressive_provision.hour_from
    End Get
    Set(ByVal Value As DateTime)
      m_progressive_provision.hour_from = Value
    End Set
  End Property

  Property HourTo() As DateTime
    Get
      Return m_progressive_provision.hour_to
    End Get
    Set(ByVal Value As DateTime)
      m_progressive_provision.hour_to = Value
    End Set
  End Property

  Property Amount() As Decimal
    Get
      Return m_progressive_provision.amount
    End Get
    Set(ByVal Value As Decimal)
      m_progressive_provision.amount = Value
    End Set
  End Property

  Property TheoreticalAmount() As Decimal
    Get
      Return m_progressive_provision.theoretical_amount
    End Get
    Set(ByVal Value As Decimal)
      m_progressive_provision.theoretical_amount = Value
    End Set
  End Property

  Property Levels() As DataTable
    Get
      Return m_progressive_provision.levels
    End Get
    Set(ByVal Value As DataTable)
      m_progressive_provision.levels = Value
    End Set
  End Property

  Property Terminals() As DataTable
    Get
      Return m_progressive_provision.terminals
    End Get
    Set(ByVal Value As DataTable)
      m_progressive_provision.terminals = Value
    End Set
  End Property

  Property CageSessionId() As Int64
    Get
      Return m_progressive_provision.cage_session_id
    End Get
    Set(ByVal Value As Int64)
      m_progressive_provision.cage_session_id = Value
    End Set
  End Property

  Property TotalCageProvision() As Decimal
    Get
      Return m_progressive_provision.total_cage_provision
    End Get
    Set(ByVal Value As Decimal)
      m_progressive_provision.total_cage_provision = Value
    End Set
  End Property

  Property TotalTerminalsProvision() As Decimal
    Get
      Return m_progressive_provision.total_termianls_provision
    End Get
    Set(ByVal Value As Decimal)
      m_progressive_provision.total_termianls_provision = Value
    End Set
  End Property

  Property ProgressiveLastProvisionedHour() As DateTime
    Get
      Return m_progressive_provision.progressive_last_provisioned_hour
    End Get
    Set(ByVal Value As DateTime)
      m_progressive_provision.progressive_last_provisioned_hour = Value
    End Set
  End Property

  Property TotalTerminalsPlayed() As Decimal
    Get
      Return m_progressive_provision.total_termianls_played
    End Get
    Set(ByVal Value As Decimal)
      m_progressive_provision.total_termianls_played = Value
    End Set
  End Property

  Property Status() As Progressives.PROGRESSIVE_PROVISION_STATUS
    Get
      Return m_progressive_provision.status
    End Get
    Set(ByVal Value As Progressives.PROGRESSIVE_PROVISION_STATUS)
      m_progressive_provision.status = Value
    End Set
  End Property

  Property GuiUserId() As Int32
    Get
      Return m_progressive_provision.gui_user_id
    End Get
    Set(ByVal Value As Int32)
      m_progressive_provision.gui_user_id = Value
    End Set
  End Property

  Property GuiUserName() As String
    Get
      Return m_progressive_provision.gui_user_name
    End Get
    Set(ByVal Value As String)
      m_progressive_provision.gui_user_name = Value
    End Set
  End Property

  Property CurrentAmount() As Decimal
    Get
      Return m_progressive_provision.current_amount
    End Get
    Set(ByVal Value As Decimal)
      m_progressive_provision.current_amount = Value
    End Set
  End Property

#End Region ' Properties

#Region " Overrides functions "

  '----------------------------------------------------------------------------
  ' PURPOSE: Reads from the database into the given object
  '
  ' PARAMS:
  '   - INPUT:
  '     - ObjectId: An object, it must be 'casted' to the desired KEY.
  '
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As ENUM_STATUS

    Dim _rc As Integer

    ' Read Progressive Provision information
    _rc = ProgrProv_DbRead(m_progressive_provision, SqlCtx)

    Select Case _rc
      Case ENUM_DB_RC.DB_RC_OK

        Return ENUM_STATUS.STATUS_OK
      Case ENUM_DB_RC.DB_RC_ERROR_NOT_FOUND

        Return ENUM_STATUS.STATUS_NOT_FOUND
      Case Else

        Return ENUM_STATUS.STATUS_ERROR
    End Select

  End Function   'DB_Read

  '----------------------------------------------------------------------------
  ' PURPOSE: Updates the given object to the database.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return ENUM_STATUS.STATUS_ERROR
  End Function 'DB_Update

  '----------------------------------------------------------------------------
  ' PURPOSE: Can't insert the given object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As ENUM_STATUS
    Dim _rc As Integer

    ' Insert bank data
    _rc = ProgrProv_DbInsert(m_progressive_provision, SqlCtx)

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK

        Return ENUM_STATUS.STATUS_OK
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY

        Return ENUM_STATUS.STATUS_DUPLICATE_KEY
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK_MORE

        Return ENUM_STATUS.STATUS_DEPENDENCIES
      Case Else

        Return ENUM_STATUS.STATUS_ERROR
    End Select
  End Function 'DB_Insert

  '----------------------------------------------------------------------------
  ' PURPOSE: Can't delete the given object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As ENUM_STATUS
    Return ENUM_STATUS.STATUS_ERROR
  End Function 'DB_Delete

  '----------------------------------------------------------------------------
  ' PURPOSE: Returns a duplicate of the given object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim _temp As CLASS_PROGRESSIVE_PROVISION

    _temp = New CLASS_PROGRESSIVE_PROVISION

    _temp.ProvisionId = Me.m_progressive_provision.provision_id
    _temp.CageSessionId = Me.m_progressive_provision.cage_session_id
    _temp.ProgressiveId = Me.m_progressive_provision.progressive_id
    _temp.ProgressiveName = Me.m_progressive_provision.progressive_name
    _temp.CreationDate = Me.m_progressive_provision.creation_date
    _temp.HourFrom = Me.m_progressive_provision.hour_from
    _temp.HourTo = Me.m_progressive_provision.hour_to
    _temp.Amount = Me.m_progressive_provision.amount
    _temp.TheoreticalAmount = Me.m_progressive_provision.theoretical_amount
    _temp.TotalCageProvision = Me.m_progressive_provision.total_cage_provision
    _temp.CageSessionId = Me.m_progressive_provision.cage_session_id
    _temp.TotalTerminalsProvision = Me.m_progressive_provision.total_termianls_provision
    _temp.Levels = Me.m_progressive_provision.levels
    _temp.Terminals = Me.m_progressive_provision.terminals
    _temp.ProgressiveLastProvisionedHour = Me.ProgressiveLastProvisionedHour
    _temp.GuiUserId = Me.m_progressive_provision.gui_user_id
    _temp.GuiUserName = Me.m_progressive_provision.gui_user_name
    _temp.CurrentAmount = Me.m_progressive_provision.current_amount

    Return _temp

  End Function 'Duplicate 

  '----------------------------------------------------------------------------
  ' PURPOSE: Returns the related auditor data for the current object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA
    Dim _str_aux As String
    Dim _str_array() As String

    Dim _auditor_data As CLASS_AUDITOR_DATA

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_PROGRESSIVE_PROVISION)

    _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(5204), "")

    ' Progressive Jackpot Name
    _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5340), Me.ProgressiveName) ' 5340 "Progressive jackpot"

    ' Hour From
    _str_aux = GUI_FormatDate(Me.HourFrom, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT) + " " + GUI_FormatTime(Me.HourFrom, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(311), _str_aux) ' 311 "Start date"

    ' Hour To
    _str_aux = GUI_FormatDate(Me.HourTo, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT) + " " + GUI_FormatTime(Me.HourTo, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(312), _str_aux) ' 312 "End date"

    ' Amount
    _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5509), GUI_FormatCurrency(Me.Amount, 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE, True)) ' 5509 "Provision Amount"

    ' Theoretical Amount
    _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5510), GUI_FormatCurrency(Me.TheoreticalAmount, 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE, True)) ' 5510 "Theoretical Amount"

    ' Levels
    _str_array = Nothing
    Array.Resize(_str_array, 0)
    If Not Levels Is Nothing Then
      If Levels.Rows.Count > 0 Then
        For Each _row As DataRow In Levels.Rows
          Array.Resize(_str_array, _str_array.Length + 1)
          _str_array(_str_array.Length - 1) = _row("LEVEL_NAME") + ": " + GUI_FormatCurrency(_row("PROVISION_AMOUNT"), 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE, True)
        Next
      End If
    End If
    _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5210), _str_array) ' 5210 "Levels"

    ' Terminals
    _str_array = Nothing
    Array.Resize(_str_array, 0)
    If Not Terminals Is Nothing Then
      If Terminals.Rows.Count > 0 Then
        For Each _row As DataRow In Terminals.Rows
          Array.Resize(_str_array, _str_array.Length + 1)
          _str_array(_str_array.Length - 1) = _row("TERMINAL_NAME") + ": " + GUI_FormatCurrency(_row("PROVISION_AMOUNT"), 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE, True)
        Next
      End If
    End If
    _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5220), _str_array) ' 5220 "Terminals"

    ' Cage Session Id
    _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(3388), Cage.GetCageSessionName(Me.CageSessionId)) ' 3388 "Cash cage session"

    ' Total Cage Provision
    _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5511), GUI_FormatCurrency(Me.TotalCageProvision, 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE, True)) ' 5511 "Provision to cage amount"

    Return _auditor_data

  End Function 'AuditorData

#End Region 'Overrides functions

#Region "DB Functions"

  Private Function ProgrProv_DbRead(ByVal ProgrProv As TYPE_PROGRESSIVE_PROVISION, _
                                    ByVal Context As Integer) As Integer

    Dim _sql_query As StringBuilder
    Dim _data_table As DataTable

    ' Table PROGRESSIVES_PROVISIONS
    _sql_query = New StringBuilder
    _sql_query.AppendLine("    SELECT   PGP_PROVISION_ID                                        ")
    _sql_query.AppendLine("           , PGP_PROGRESSIVE_ID                                      ")
    _sql_query.AppendLine("           , PGS_NAME                                                ")
    _sql_query.AppendLine("           , PGP_CREATED                                             ")
    _sql_query.AppendLine("           , PGP_HOUR_FROM                                           ")
    _sql_query.AppendLine("           , PGP_HOUR_TO                                             ")
    _sql_query.AppendLine("           , PGP_AMOUNT                                              ")
    _sql_query.AppendLine("           , PGP_THEORETICAL_AMOUNT                                  ")
    _sql_query.AppendLine("           , PGP_CAGE_SESSION_ID                                     ")
    _sql_query.AppendLine("           , PGP_CAGE_AMOUNT                                         ")
    _sql_query.AppendLine("           , PGS_LAST_PROVISIONED_HOUR                               ")
    _sql_query.AppendLine("           , PGP_STATUS                                              ")
    _sql_query.AppendLine("           , PGP_GUI_USER_ID                                         ")
    _sql_query.AppendLine("           , GU_USERNAME                                             ")
    _sql_query.AppendLine("      FROM   PROGRESSIVES_PROVISIONS                                 ")
    _sql_query.AppendLine("INNER JOIN   PROGRESSIVES ON PGP_PROGRESSIVE_ID = PGS_PROGRESSIVE_ID ")
    _sql_query.AppendLine(" LEFT JOIN   GUI_USERS ON PGP_GUI_USER_ID = GU_USER_ID               ")
    _sql_query.AppendLine("     WHERE   PGP_PROVISION_ID = " + ProgrProv.provision_id.ToString)

    _data_table = GUI_GetTableUsingSQL(_sql_query.ToString(), 5000)
    If IsNothing(_data_table) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    If _data_table.Rows.Count() <> 1 Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    ProgrProv.provision_id = _data_table.Rows(0).Item("PGP_PROVISION_ID")
    ProgrProv.progressive_id = _data_table.Rows(0).Item("PGP_PROGRESSIVE_ID")
    ProgrProv.progressive_name = _data_table.Rows(0).Item("PGS_NAME")
    ProgrProv.creation_date = _data_table.Rows(0).Item("PGP_CREATED")
    ProgrProv.hour_from = _data_table.Rows(0).Item("PGP_HOUR_FROM")
    ProgrProv.hour_to = _data_table.Rows(0).Item("PGP_HOUR_TO")
    ProgrProv.amount = _data_table.Rows(0).Item("PGP_AMOUNT")
    ProgrProv.theoretical_amount = _data_table.Rows(0).Item("PGP_THEORETICAL_AMOUNT")
    ProgrProv.cage_session_id = _data_table.Rows(0).Item("PGP_CAGE_SESSION_ID")
    ProgrProv.total_cage_provision = _data_table.Rows(0).Item("PGP_CAGE_AMOUNT")

    If Not _data_table.Rows(0).IsNull("PGS_LAST_PROVISIONED_HOUR") Then
      ProgrProv.progressive_last_provisioned_hour = _data_table.Rows(0).Item("PGS_LAST_PROVISIONED_HOUR")
    End If

    ProgrProv.status = _data_table.Rows(0).Item("PGP_STATUS")

    If Not _data_table.Rows(0).IsNull("PGP_GUI_USER_ID") Then
      ProgrProv.gui_user_id = _data_table.Rows(0).Item("PGP_GUI_USER_ID")
    End If

    If Not _data_table.Rows(0).IsNull("GU_USERNAME") Then
      ProgrProv.gui_user_name = _data_table.Rows(0).Item("GU_USERNAME")
    End If

    ' Table PROGRESSIVES_PROVISIONS_LEVELS
    _sql_query = New StringBuilder
    _sql_query.AppendLine("    SELECT   PPL_LEVEL_ID              AS LEVEL_ID                         ")
    _sql_query.AppendLine("           , PGL_NAME                  AS LEVEL_NAME                       ")
    _sql_query.AppendLine("           , PGL_CONTRIBUTION_PCT      AS CONTRIBUTION_PCT                 ")
    _sql_query.AppendLine("           , PPL_AMOUNT                AS LEVEL_AMOUNT                     ")
    _sql_query.AppendLine("           , 0.0                       AS THEORETICAL_AMOUNT               ")
    _sql_query.AppendLine("           , PPL_AMOUNT                AS PROVISION_AMOUNT                 ")
    _sql_query.AppendLine("      FROM   PROGRESSIVES_PROVISIONS_LEVELS                                ")
    _sql_query.AppendLine("INNER JOIN   PROGRESSIVES_PROVISIONS ON PPL_PROVISION_ID = PGP_PROVISION_ID")
    _sql_query.AppendLine("INNER JOIN   PROGRESSIVES_LEVELS ON PGL_LEVEL_ID = PPL_LEVEL_ID            ")
    _sql_query.AppendLine("       AND   PGL_PROGRESSIVE_ID = PGP_PROGRESSIVE_ID                       ")
    _sql_query.AppendLine("     WHERE   PPL_PROVISION_ID = " + ProgrProv.provision_id.ToString)

    ProgrProv.levels = GUI_GetTableUsingSQL(_sql_query.ToString(), 5000)
    If IsNothing(ProgrProv.levels) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    ' Table PROGRESSIVES_PROVISIONS_TERMINALS
    _sql_query = New StringBuilder
    _sql_query.AppendLine("    SELECT   PPT_TERMINAL_ID AS TERMINAL_ID                ")
    _sql_query.AppendLine("           , PV_NAME         AS PROVIDER_NAME              ")
    _sql_query.AppendLine("           , TE_NAME         AS TERMINAL_NAME              ")
    _sql_query.AppendLine("           , 0.0             AS TERMINAL_PLAYED_AMOUNT     ")
    _sql_query.AppendLine("           , 0.0             AS EQUALLY_AMOUNT             ")
    _sql_query.AppendLine("           , 0.0             AS AS_PLAYED_AMOUNT           ")
    _sql_query.AppendLine("           , PPT_AMOUNT      AS PROVISION_AMOUNT           ")
    _sql_query.AppendLine("      FROM   PROGRESSIVES_PROVISIONS_TERMINALS             ")
    _sql_query.AppendLine("INNER JOIN   TERMINALS ON PPT_TERMINAL_ID = TE_TERMINAL_ID ")
    _sql_query.AppendLine("INNER JOIN   PROVIDERS ON TE_PROV_ID = PV_ID               ")
    _sql_query.AppendLine("     WHERE   PPT_PROVISION_ID = " + ProgrProv.provision_id.ToString)
    _sql_query.AppendLine("  ORDER BY   PV_NAME, TE_NAME                              ")

    ProgrProv.terminals = GUI_GetTableUsingSQL(_sql_query.ToString(), 5000)
    If IsNothing(ProgrProv.terminals) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

  End Function

  Private Function ProgrProv_DbInsert(ByVal ProgrProv As TYPE_PROGRESSIVE_PROVISION, _
                                      ByVal Context As Integer) As Integer
    Dim _progressive_provision As Progressives.ProgressiveProvision
    Dim _errors_on_terminals As List(Of String)
    Dim _terminals_list As String
    Dim _terminals_counter As String

    _progressive_provision = Me.GetCommonProgressiveProvision()
    _errors_on_terminals = New List(Of String)

    Using _db_trx As New DB_TRX()
      Try
        If Not Progressives.GenerateProvision(_progressive_provision, _db_trx.SqlTransaction, _errors_on_terminals) Then
          'An error occurred generating provision: rollback
          _db_trx.Rollback()

          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

        If _errors_on_terminals.Count > 0 Then
          _terminals_list = String.Empty
          _terminals_counter = 0

          For Each _terminal_name As String In _errors_on_terminals
            _terminals_list &= vbCrLf & "  - " & _terminal_name

            _terminals_counter += 1
            If _terminals_counter = 5 And _errors_on_terminals.Count > 5 Then
              _terminals_list &= vbCrLf & "  ..."

              Exit For
            End If
          Next

          '5624 "If progressive jackpot is provisioned, statistics for the following terminals will not be updated because they have noy yet reported game meters:\n%1\n\nDo you want to continue?"
          If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5624), _
                        ENUM_MB_TYPE.MB_TYPE_WARNING, _
                        ENUM_MB_BTN.MB_BTN_YES_NO, _
                        , _
                        _terminals_list) = ENUM_MB_RESULT.MB_RESULT_NO Then

            Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK_MORE
          End If
        End If

        ' Commit
        _db_trx.Commit()

        ' Everything went ok.  Update class object
        Me.UpdateFromCommonProgressiveProvision(_progressive_provision)

      Catch _ex As Exception
        ' Rollback  
        _db_trx.Rollback()

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End Try
    End Using

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

  End Function ' ProgrProv_DbUpdate

#End Region ' DB Functions

#Region " Public Functions "

  Public Function GetCommonProgressiveProvision() As Progressives.ProgressiveProvision
    Dim _progressive_provision As Progressives.ProgressiveProvision

    _progressive_provision = New Progressives.ProgressiveProvision

    _progressive_provision.ProvisionId = Me.ProvisionId
    _progressive_provision.CageSessionId = Me.CageSessionId
    _progressive_provision.ProgressiveId = Me.ProgressiveId
    _progressive_provision.CreationDate = Me.CreationDate
    _progressive_provision.HourFrom = Me.HourFrom
    _progressive_provision.HourTo = Me.HourTo
    _progressive_provision.Amount = Me.Amount
    _progressive_provision.TheoreticalAmount = Me.TheoreticalAmount
    _progressive_provision.Levels = Me.Levels
    _progressive_provision.Terminals = Me.Terminals
    _progressive_provision.TotalCageProvision = Me.TotalCageProvision
    _progressive_provision.TotalTerminalsProvision = Me.TotalTerminalsProvision
    _progressive_provision.ProgressiveLastProvisionedHour = Me.ProgressiveLastProvisionedHour
    _progressive_provision.TotalTerminalsPlayed = Me.TotalTerminalsPlayed
    _progressive_provision.Status = Me.Status
    _progressive_provision.GuiUserId = Me.GuiUserId
    _progressive_provision.CurrentAmount = Me.CurrentAmount

    Return _progressive_provision
  End Function ' GetCommonProgressiveProvision

  Public Sub UpdateFromCommonProgressiveProvision(ByVal ProgressiveProvision As Progressives.ProgressiveProvision)

    Me.ProvisionId = ProgressiveProvision.ProvisionId
    Me.CageSessionId = ProgressiveProvision.CageSessionId
    Me.ProgressiveId = ProgressiveProvision.ProgressiveId
    Me.CreationDate = ProgressiveProvision.CreationDate
    Me.HourFrom = ProgressiveProvision.HourFrom
    Me.HourTo = ProgressiveProvision.HourTo
    Me.Amount = ProgressiveProvision.Amount
    Me.TheoreticalAmount = ProgressiveProvision.TheoreticalAmount
    Me.Levels = ProgressiveProvision.Levels
    Me.Terminals = ProgressiveProvision.Terminals
    Me.TotalCageProvision = ProgressiveProvision.TotalCageProvision
    Me.TotalTerminalsProvision = ProgressiveProvision.TotalTerminalsProvision
    Me.ProgressiveLastProvisionedHour = ProgressiveProvision.ProgressiveLastProvisionedHour
    Me.TotalTerminalsPlayed = ProgressiveProvision.TotalTerminalsPlayed
    Me.Status = ProgressiveProvision.Status
    Me.CurrentAmount = ProgressiveProvision.CurrentAmount

  End Sub ' UpdateFromCommonProgressiveProvision

#End Region ' Public Functions

End Class ' CLASS_PROGRESSIVE_PROVISION
