'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   CLASS_PAYMENT_ORDER
' DESCRIPTION:   Payment order class
' AUTHOR:        Luis Ernesto Mesa  
' CREATION DATE: 17-JAN-2013
' 
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 17-JAN-2013  LEM    Initial version
' 16-MAR-2017  ATB    PBI 25737:Third TAX - Report columns
'--------------------------------------------------------------------

#Region " Imports "

Option Explicit On
Option Strict Off

Imports GUI_Controls
Imports GUI_CommonOperations
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_CommonMisc
Imports System.IO
Imports System.Drawing.Printing
Imports WSI.Common
Imports System.Data.SqlClient
Imports System.Text

#End Region

Public Class CLASS_PAYMENT_ORDER
  Inherits CLASS_BASE

#Region " Members "

  Dim m_operation_id As Int64
  Dim m_account_id As Int64
  Dim m_order_date As Date
  Dim m_player_name1 As String
  Dim m_player_name2 As String
  Dim m_player_name3 As String
  Dim m_player_name4 As String
  Dim m_player_doc_type1 As String
  Dim m_player_doc_id1 As String
  Dim m_player_doc_type2 As String
  Dim m_player_doc_id2 As String
  Dim m_authorization_title1 As String
  Dim m_authorization_name1 As String
  Dim m_authorization_title2 As String
  Dim m_authorization_name2 As String
  Dim m_effective_days As String
  Dim m_application_date As DateTime
  Dim m_payment_type As String
  Dim m_bank_account_id As Int64
  Dim m_bank_name As String
  Dim m_bank_customer_number As String
  Dim m_bank_account_number As String
  Dim m_account_balance As Double
  Dim m_return_balance As Double
  Dim m_prize As Double
  Dim m_witholding_tax1 As Double
  Dim m_witholding_tax2 As Double
  Dim m_witholding_tax3 As Double
  Dim m_full_payment As Double
  Dim m_cash_payment As Double
  Dim m_check_payment As Double
  Dim m_document_id As Int64
  Dim m_document_list As DocumentList
  Dim m_temp_files() As String
  Dim m_temp_folder As String

#End Region     ' Members

#Region " Properties "

  Public Property OperationId() As Int64
    Get
      Return m_operation_id
    End Get
    Set(ByVal value As Int64)
      m_operation_id = value
    End Set
  End Property

  Public Property AccountId() As Int64
    Get
      Return m_account_id
    End Get
    Set(ByVal value As Int64)
      m_account_id = value
    End Set
  End Property

  Public Property OrderDate() As Date
    Get
      Return m_order_date
    End Get
    Set(ByVal value As Date)
      m_order_date = value
    End Set
  End Property

  Public Property AuthorizationTitle1() As String
    Get
      Return m_authorization_title1
    End Get
    Set(ByVal value As String)
      m_authorization_title1 = value
    End Set
  End Property

  Public Property AuthorizationName1() As String
    Get
      Return m_authorization_name1
    End Get
    Set(ByVal value As String)
      m_authorization_name1 = value
    End Set
  End Property

  Public Property AuthorizationName2() As String
    Get
      Return m_authorization_name2
    End Get
    Set(ByVal value As String)
      m_authorization_name2 = value
    End Set
  End Property

  Public Property AuthorizationTitle2() As String
    Get
      Return m_authorization_title2
    End Get
    Set(ByVal value As String)
      m_authorization_title2 = value
    End Set
  End Property

  Public Property PlayerName1() As String
    Get
      Return m_player_name1
    End Get
    Set(ByVal value As String)
      m_player_name1 = value
    End Set
  End Property

  Public Property PlayerName2() As String
    Get
      Return m_player_name2
    End Get
    Set(ByVal value As String)
      m_player_name2 = value
    End Set
  End Property

  Public Property PlayerName3() As String
    Get
      Return m_player_name3
    End Get
    Set(ByVal value As String)
      m_player_name3 = value
    End Set
  End Property

  Public Property PlayerName4() As String
    Get
      Return m_player_name4
    End Get
    Set(ByVal value As String)
      m_player_name4 = value
    End Set
  End Property

  Public Property PlayerDocType1() As String
    Get
      Return m_player_doc_type1
    End Get
    Set(ByVal value As String)
      m_player_doc_type1 = value
    End Set
  End Property

  Public Property PlayerDocId1() As String
    Get
      Return m_player_doc_id1
    End Get
    Set(ByVal value As String)
      m_player_doc_id1 = value
    End Set
  End Property

  Public Property PlayerDocType2() As String
    Get
      Return m_player_doc_type2
    End Get
    Set(ByVal value As String)
      m_player_doc_type2 = value
    End Set
  End Property

  Public Property PlayerDocId2() As String
    Get
      Return m_player_doc_id2
    End Get
    Set(ByVal value As String)
      m_player_doc_id2 = value
    End Set
  End Property

  Public Property DocumentId() As Int64
    Get
      Return m_document_id
    End Get
    Set(ByVal value As Int64)
      m_document_id = value
    End Set
  End Property

  Public Property DocumentList() As WSI.Common.DocumentList
    Get
      Return m_document_list
    End Get
    Set(ByVal value As WSI.Common.DocumentList)
      m_document_list = value
    End Set
  End Property

  Public Property EffectiveDays() As String
    Get
      Return m_effective_days
    End Get
    Set(ByVal value As String)
      m_effective_days = value
    End Set
  End Property

  Public Property ApplicationDate() As DateTime
    Get
      Return m_application_date
    End Get
    Set(ByVal value As DateTime)
      m_application_date = New DateTime(value.Year, value.Month, value.Day)
    End Set
  End Property

  Public Property PaymentType() As String
    Get
      Return m_payment_type
    End Get
    Set(ByVal value As String)
      m_payment_type = value
    End Set
  End Property

  Public Property BankAccountId() As Int64
    Get
      Return m_bank_account_id
    End Get
    Set(ByVal value As Int64)
      m_bank_account_id = value
    End Set
  End Property

  Public Property BankName() As String
    Get
      Return m_bank_name
    End Get
    Set(ByVal value As String)
      m_bank_name = value
    End Set
  End Property

  Public Property BankCustomerNumber() As String
    Get
      Return m_bank_customer_number
    End Get
    Set(ByVal value As String)
      m_bank_customer_number = value
    End Set
  End Property

  Public Property BankAccountNumber() As String
    Get
      Return m_bank_account_number
    End Get
    Set(ByVal value As String)
      m_bank_account_number = value
    End Set
  End Property

  Public Property AccountBalance() As Decimal
    Get
      Return m_account_balance
    End Get
    Set(ByVal value As Decimal)
      m_account_balance = value
    End Set
  End Property

  Public Property ReturnBalance() As Decimal
    Get
      Return m_return_balance
    End Get
    Set(ByVal value As Decimal)
      m_return_balance = value
    End Set
  End Property

  Public Property Prize() As Decimal
    Get
      Return m_prize
    End Get
    Set(ByVal value As Decimal)
      m_prize = value
    End Set
  End Property

  Public Property WitholdingTax1() As Decimal
    Get
      Return m_witholding_tax1
    End Get
    Set(ByVal value As Decimal)
      m_witholding_tax1 = value
    End Set
  End Property

  Public Property WitholdingTax2() As Decimal
    Get
      Return m_witholding_tax2
    End Get
    Set(ByVal value As Decimal)
      m_witholding_tax2 = value
    End Set
  End Property

  Public Property WitholdingTax3() As Decimal
    Get
      Return m_witholding_tax3
    End Get
    Set(ByVal value As Decimal)
      m_witholding_tax3 = value
    End Set
  End Property

  Public Property FullPayment() As Decimal
    Get
      Return m_full_payment
    End Get
    Set(ByVal value As Decimal)
      m_full_payment = value
    End Set
  End Property

  Public Property CashPayment() As Decimal
    Get
      Return m_cash_payment
    End Get
    Set(ByVal value As Decimal)
      m_cash_payment = value
    End Set
  End Property

  Public Property CheckPayment() As Decimal
    Get
      Return m_check_payment
    End Get
    Set(ByVal value As Decimal)
      m_check_payment = value
    End Set
  End Property

  Public Property TempFiles() As String()
    Get
      Return m_temp_files
    End Get
    Set(ByVal value() As String)
      m_temp_files = value
    End Set
  End Property

  Public Property TempFolder() As String
    Get
      Return m_temp_folder
    End Get
    Set(ByVal value As String)
      m_temp_folder = value
    End Set
  End Property

#End Region  ' Properties

#Region " Overrides "

  '----------------------------------------------------------------------------
  ' PURPOSE : Returns the related auditor data for the current object.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - The newly created audit object
  '
  ' NOTES :
  Public Overrides Function AuditorData() As CLASS_AUDITOR_DATA
    Dim _doc_type_id As Int32
    Dim _auditor_data As CLASS_AUDITOR_DATA

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_ACCOUNT)

    Call _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(1587), _
                               GUI_FormatDate(Me.m_order_date, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS))

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1575), IIf(String.IsNullOrEmpty(Me.m_player_name1), AUDIT_NONE_STRING, Me.m_player_name1))
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1576), IIf(String.IsNullOrEmpty(Me.m_player_name2), AUDIT_NONE_STRING, Me.m_player_name2))
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1614), IIf(String.IsNullOrEmpty(Me.m_player_name3), AUDIT_NONE_STRING, Me.m_player_name3))
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5087), IIf(String.IsNullOrEmpty(Me.m_player_name4), AUDIT_NONE_STRING, Me.m_player_name4))

    If Int32.TryParse(Me.m_player_doc_type1, _doc_type_id) Then
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1589), IIf(String.IsNullOrEmpty(IdentificationTypes.DocIdTypeString(_doc_type_id)), AUDIT_NONE_STRING, IdentificationTypes.DocIdTypeString(_doc_type_id)))
    Else
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1589), AUDIT_NONE_STRING)
    End If
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1565), Me.m_player_doc_id1)
    If Int32.TryParse(Me.m_player_doc_type2, _doc_type_id) Then
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1590), IIf(String.IsNullOrEmpty(IdentificationTypes.DocIdTypeString(_doc_type_id)), AUDIT_NONE_STRING, IdentificationTypes.DocIdTypeString(_doc_type_id)))
    Else
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1590), AUDIT_NONE_STRING)
    End If
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1566), Me.m_player_doc_id2)

    Call _auditor_data.SetField(0, Me.m_authorization_title1, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1605, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1613)))
    Call _auditor_data.SetField(0, Me.m_authorization_name1, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1605, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1614)))
    Call _auditor_data.SetField(0, Me.m_authorization_title2, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1606, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1613)))
    Call _auditor_data.SetField(0, Me.m_authorization_name2, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1606, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1614)))
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1556), Me.m_effective_days)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1596), GUI_FormatDate(Me.m_application_date, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE))
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1595), Me.m_payment_type)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1554), Me.m_bank_name)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1557), Me.m_bank_customer_number)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1558), Me.m_bank_account_number)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1627), GUI_FormatCurrency(Me.m_account_balance, 2))
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1593), GUI_FormatCurrency(Me.m_return_balance, 2))
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1592), GUI_FormatCurrency(Me.m_prize, 2))
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1571), GUI_FormatCurrency(Me.m_witholding_tax1, 2))
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1572), GUI_FormatCurrency(Me.m_witholding_tax2, 2))
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7957), GUI_FormatCurrency(Me.m_witholding_tax3, 2))
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1573), GUI_FormatCurrency(Me.m_full_payment, 2))
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1570), GUI_FormatCurrency(Me.m_cash_payment, 2))
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1569), GUI_FormatCurrency(Me.m_check_payment, 2))

    Return _auditor_data

  End Function ' AuditorData

  '----------------------------------------------------------------------------
  ' PURPOSE: Reads from the database into the given object
  '
  ' PARAMS:
  '   - INPUT:
  '     - ObjectId: An object, it must be 'casted' to the desired KEY.
  '
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As ENUM_STATUS

    Dim _rc As ENUM_STATUS

    Using _db_trx As New DB_TRX()

      Me.m_operation_id = ObjectId
      _rc = Me.Read(_db_trx.SqlTransaction)

      If _rc <> ENUM_STATUS.STATUS_OK Then
        Call _db_trx.Rollback()
      Else
        _rc = IIf(_db_trx.Commit() = True, ENUM_STATUS.STATUS_OK, ENUM_STATUS.STATUS_ERROR)
      End If

    End Using ' _db_trx

    Return _rc

  End Function     ' DB_Read

  '----------------------------------------------------------------------------
  ' PURPOSE: Updates the given object to the database.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As ENUM_STATUS

    Dim _rc As ENUM_STATUS

    Using _db_trx As New DB_TRX()

      _rc = Me.Update(_db_trx.SqlTransaction)

      If _rc <> ENUM_STATUS.STATUS_OK Then
        Call _db_trx.Rollback()
      Else
        _rc = IIf(_db_trx.Commit() = True, ENUM_STATUS.STATUS_OK, ENUM_STATUS.STATUS_ERROR)
      End If

    End Using

    Return _rc

  End Function   ' DB_Update

  '----------------------------------------------------------------------------
  ' PURPOSE: Returns a duplicate of the given object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  Public Overrides Function Duplicate() As CLASS_BASE

    Dim _clone As CLASS_PAYMENT_ORDER

    _clone = New CLASS_PAYMENT_ORDER()

    _clone.m_operation_id = Me.m_operation_id
    _clone.m_account_id = Me.m_account_id
    _clone.m_order_date = Me.m_order_date
    _clone.m_player_name1 = Me.m_player_name1
    _clone.m_player_name2 = Me.m_player_name2
    _clone.m_player_name3 = Me.m_player_name3
    _clone.m_player_name4 = Me.m_player_name4
    _clone.m_player_doc_type1 = Me.m_player_doc_type1
    _clone.m_player_doc_id1 = Me.m_player_doc_id1
    _clone.m_player_doc_type2 = Me.m_player_doc_type2
    _clone.m_player_doc_id2 = Me.m_player_doc_id2
    _clone.m_authorization_title1 = Me.m_authorization_title1
    _clone.m_authorization_name1 = Me.m_authorization_name1
    _clone.m_authorization_title2 = Me.m_authorization_title2
    _clone.m_authorization_name2 = Me.m_authorization_name2
    _clone.m_effective_days = Me.m_effective_days
    _clone.m_application_date = Me.m_application_date
    _clone.m_payment_type = Me.m_payment_type
    _clone.m_bank_account_id = Me.m_bank_account_id
    _clone.m_bank_name = Me.m_bank_name
    _clone.m_bank_customer_number = Me.m_bank_customer_number
    _clone.m_bank_account_number = Me.m_bank_account_number
    _clone.m_account_balance = Me.m_account_balance
    _clone.m_return_balance = Me.m_return_balance
    _clone.m_prize = Me.m_prize
    _clone.m_witholding_tax1 = Me.m_witholding_tax1
    _clone.m_witholding_tax2 = Me.m_witholding_tax2
    _clone.m_witholding_tax3 = Me.m_witholding_tax3
    _clone.m_full_payment = Me.m_full_payment
    _clone.m_cash_payment = Me.m_cash_payment
    _clone.m_check_payment = Me.m_check_payment
    _clone.m_document_id = Me.m_document_id

    Return _clone

  End Function   ' Duplicate

  '----------------------------------------------------------------------------
  ' PURPOSE : Deletes the given object from the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DEPENDENCIES
  '
  ' NOTES :
  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As ENUM_STATUS
    Return ENUM_STATUS.STATUS_ERROR
  End Function   ' DB_Delete

  '----------------------------------------------------------------------------
  ' PURPOSE : Inserts the given object to the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES :
  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As ENUM_STATUS
    Return ENUM_STATUS.STATUS_ERROR
  End Function   ' DB_Insert

#End Region   ' Overrides

#Region " Privates "

  '----------------------------------------------------------------------------
  ' PURPOSE: Reads a payment order object from the database, identified
  '          by the property Id of this same instance.
  '
  ' PARAMS:
  '   - INPUT: Trx As SqlTransaction
  '
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Private Function Read(ByVal Trx As SqlTransaction) As ENUM_STATUS

    Dim _sb As StringBuilder

    Try
      _sb = New StringBuilder()

      _sb.AppendLine(" SELECT   APO_OPERATION_ID          ")
      _sb.AppendLine("        , APO_ACCOUNT_ID            ")
      _sb.AppendLine("        , APO_DATETIME              ")
      _sb.AppendLine("        , APO_PLAYER_NAME1          ")
      _sb.AppendLine("        , APO_PLAYER_DOC_TYPE1      ")
      _sb.AppendLine("        , APO_PLAYER_DOC_ID1        ")
      _sb.AppendLine("        , APO_PLAYER_DOC_TYPE2      ")
      _sb.AppendLine("        , APO_PLAYER_DOC_ID2        ")
      _sb.AppendLine("        , APO_EFFECTIVE_DAYS        ")
      _sb.AppendLine("        , APO_APPLICATION_DATE      ")
      _sb.AppendLine("        , APO_PAYMENT_TYPE          ")
      _sb.AppendLine("        , APO_BANK_ID               ")
      _sb.AppendLine("        , APO_BANK_NAME             ")
      _sb.AppendLine("        , APO_BANK_CUSTOMER_NUMBER  ")
      _sb.AppendLine("        , APO_BANK_ACCOUNT_NUMBER   ")
      _sb.AppendLine("        , APO_ACCOUNT_BALANCE       ")
      _sb.AppendLine("        , APO_RETURN_BALANCE        ")
      _sb.AppendLine("        , APO_PRIZE                 ")
      _sb.AppendLine("        , APO_TAX1                  ")
      _sb.AppendLine("        , APO_TAX2                  ")
      _sb.AppendLine("        , APO_TAX3                  ")
      _sb.AppendLine("        , APO_TOTAL_PAYMENT         ")
      _sb.AppendLine("        , APO_CASH_PAYMENT          ")
      _sb.AppendLine("        , APO_CHECK_PAYMENT         ")
      _sb.AppendLine("        , APO_DOCUMENT_ID           ")
      _sb.AppendLine("        , APO_PLAYER_NAME2          ")
      _sb.AppendLine("        , APO_PLAYER_NAME3          ")
      _sb.AppendLine("        , APO_PLAYER_NAME4          ")
      _sb.AppendLine("        , APO_AUTHORIZED_BY_TITLE1  ")
      _sb.AppendLine("        , APO_AUTHORIZED_BY_TITLE2  ")
      _sb.AppendLine("        , APO_AUTHORIZED_BY_NAME1  ")
      _sb.AppendLine("        , APO_AUTHORIZED_BY_NAME2  ")
      _sb.AppendLine("   FROM   ACCOUNT_PAYMENT_ORDERS    ")
      _sb.AppendLine("  WHERE   APO_OPERATION_ID = @pOperationId ")

      Using _cmd As New SqlCommand(_sb.ToString(), Trx.Connection, Trx)

        _cmd.Parameters.Add("@pOperationId", SqlDbType.Int).Value = Me.m_operation_id

        Using _reader As SqlDataReader = _cmd.ExecuteReader()

          If Not _reader.HasRows() Then
            Return ENUM_STATUS.STATUS_NOT_FOUND
          End If

          While _reader.Read()

            Me.m_account_id = _reader.GetInt64(1)
            Me.m_order_date = _reader.GetDateTime(2)
            Me.m_player_name1 = _reader.GetString(3)
            Me.m_player_doc_type1 = _reader.GetString(4)
            Me.m_player_doc_id1 = _reader.GetString(5)
            Me.m_player_doc_type2 = _reader.GetString(6)
            Me.m_player_doc_id2 = _reader.GetString(7)
            Me.m_effective_days = _reader.GetString(8)
            Me.m_application_date = _reader.GetDateTime(9)
            Me.m_payment_type = _reader.GetString(10)
            Me.m_bank_account_id = _reader.GetInt64(11)
            If Not _reader.IsDBNull(12) Then
              Me.m_bank_name = _reader.GetString(12)
            End If
            If Not _reader.IsDBNull(13) Then
              Me.m_bank_customer_number = _reader.GetString(13)
            End If
            If Not _reader.IsDBNull(14) Then
              Me.m_bank_account_number = _reader.GetString(14)
            End If
            Me.m_account_balance = _reader.GetDecimal(15)
            Me.m_return_balance = _reader.GetDecimal(16)
            Me.m_prize = _reader.GetDecimal(17)
            If Not _reader.IsDBNull(18) Then
              Me.m_witholding_tax1 = _reader.GetDecimal(18)
            End If
            If Not _reader.IsDBNull(19) Then
              Me.m_witholding_tax2 = _reader.GetDecimal(19)
            End If
            If Not _reader.IsDBNull(20) Then
              Me.m_witholding_tax3 = _reader.GetDecimal(20)
            End If
            Me.m_full_payment = _reader.GetDecimal(21)
            Me.m_cash_payment = _reader.GetDecimal(22)
            Me.m_check_payment = _reader.GetDecimal(23)
            If Not _reader.IsDBNull(24) Then
              Me.m_document_id = _reader.GetInt64(24)
            End If
            If Not _reader.IsDBNull(25) Then
              Me.m_player_name2 = _reader.GetString(25)
            End If
            If Not _reader.IsDBNull(26) Then
              Me.m_player_name3 = _reader.GetString(26)
            End If
            If Not _reader.IsDBNull(27) Then
              Me.m_player_name4 = _reader.GetString(27)
            End If
            If Not _reader.IsDBNull(28) Then
              Me.m_authorization_title1 = _reader.GetString(28)
            End If
            If Not _reader.IsDBNull(29) Then
              Me.m_authorization_title2 = _reader.GetString(29)
            End If
            If Not _reader.IsDBNull(30) Then
              Me.m_authorization_name1 = _reader.GetString(30)
            End If
            If Not _reader.IsDBNull(31) Then
              Me.m_authorization_name2 = _reader.GetString(31)
            End If

          End While

          _reader.Close()

        End Using '_reader

      End Using '_cmd

      Return ENUM_STATUS.STATUS_OK

    Catch _ex As Exception

      Return ENUM_STATUS.STATUS_ERROR

    End Try

  End Function   ' Read

  '----------------------------------------------------------------------------
  ' PURPOSE: Updates an payment order object from the database, 
  '          depending on the property Id of this same instance. 
  '
  ' PARAMS:
  '   - INPUT: Trx As SqlTransaction
  '
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Private Function Update(ByVal Trx As SqlTransaction) As ENUM_STATUS

    Dim _sb As StringBuilder

    Try

      _sb = New StringBuilder()

      _sb.AppendLine(" UPDATE   ACCOUNT_PAYMENT_ORDERS                          ")
      _sb.AppendLine("    SET   APO_ACCOUNT_ID           = @pAccountId          ")
      _sb.AppendLine("        , APO_DATETIME             = @pOrderDate          ")
      _sb.AppendLine("        , APO_PLAYER_NAME1         = @pPlayerName1        ")
      _sb.AppendLine("        , APO_PLAYER_NAME2         = @pPlayerName2        ")
      _sb.AppendLine("        , APO_PLAYER_NAME3         = @pPlayerName3        ")
      _sb.AppendLine("        , APO_PLAYER_NAME4         = @pPlayerName4        ")
      _sb.AppendLine("        , APO_PLAYER_DOC_TYPE1     = @pPlayerDocType1     ")
      _sb.AppendLine("        , APO_PLAYER_DOC_ID1       = @pPlayerDocId1       ")
      _sb.AppendLine("        , APO_PLAYER_DOC_TYPE2     = @pPlayerDocType2     ")
      _sb.AppendLine("        , APO_PLAYER_DOC_ID2       = @pPlayerDocId2       ")
      _sb.AppendLine("        , APO_AUTHORIZED_BY_TITLE1 = @pAuthorization1Title")
      _sb.AppendLine("        , APO_AUTHORIZED_BY_NAME1  = @pAuthorization1Name ")
      _sb.AppendLine("        , APO_AUTHORIZED_BY_TITLE2 = @pAuthorization2Title")
      _sb.AppendLine("        , APO_AUTHORIZED_BY_NAME2  = @pAuthorization2Name ")
      _sb.AppendLine("        , APO_EFFECTIVE_DAYS       = @pEffectiveDays      ")
      _sb.AppendLine("        , APO_APPLICATION_DATE     = @pApplicationDate    ")
      _sb.AppendLine("        , APO_PAYMENT_TYPE         = @pPaymentType        ")
      _sb.AppendLine("        , APO_BANK_ID              = @pBankAccountId      ")
      _sb.AppendLine("        , APO_BANK_NAME            = @pBankName           ")
      _sb.AppendLine("        , APO_BANK_CUSTOMER_NUMBER = @pBankCustomerNumber ")
      _sb.AppendLine("        , APO_BANK_ACCOUNT_NUMBER  = @pBankAccountNumber  ")
      _sb.AppendLine("        , APO_ACCOUNT_BALANCE      = @pAccountBalance     ")
      _sb.AppendLine("        , APO_RETURN_BALANCE       = @pReturnBalance      ")
      _sb.AppendLine("        , APO_PRIZE                = @pPrize              ")
      _sb.AppendLine("        , APO_TAX1                 = @pWithHoldingTax1    ")
      _sb.AppendLine("        , APO_TAX2                 = @pWithHoldingTax2    ")
      _sb.AppendLine("        , APO_TAX3                 = @pWithHoldingTax3    ")
      _sb.AppendLine("        , APO_TOTAL_PAYMENT        = @pFullPayment        ")
      _sb.AppendLine("        , APO_CASH_PAYMENT         = @pCashPayment        ")
      _sb.AppendLine("        , APO_CHECK_PAYMENT        = @pCheckPayment       ")
      _sb.AppendLine("  WHERE   APO_OPERATION_ID         = @pOperationId        ")

      Using _cmd As New SqlCommand(_sb.ToString, Trx.Connection, Trx)

        _cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = Me.m_operation_id
        _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = Me.m_account_id
        _cmd.Parameters.Add("@pOrderDate", SqlDbType.DateTime).Value = Me.m_order_date
        _cmd.Parameters.Add("@pPlayerName1", SqlDbType.VarChar, 50).Value = Me.m_player_name1
        _cmd.Parameters.Add("@pPlayerName2", SqlDbType.VarChar, 50).Value = Me.m_player_name2
        _cmd.Parameters.Add("@pPlayerName3", SqlDbType.VarChar, 50).Value = Me.m_player_name3
        _cmd.Parameters.Add("@pPlayerName4", SqlDbType.VarChar, 50).Value = Me.m_player_name4
        _cmd.Parameters.Add("@pPlayerDocType1", SqlDbType.VarChar).Value = Me.m_player_doc_type1
        _cmd.Parameters.Add("@pPlayerDocId1", SqlDbType.VarChar).Value = Me.m_player_doc_id1
        _cmd.Parameters.Add("@pPlayerDocType2", SqlDbType.VarChar).Value = Me.m_player_doc_type2
        _cmd.Parameters.Add("@pPlayerDocId2", SqlDbType.VarChar).Value = Me.m_player_doc_id2
        _cmd.Parameters.Add("@pAuthorization1Title", SqlDbType.VarChar).Value = Me.m_authorization_title1
        _cmd.Parameters.Add("@pAuthorization1Name", SqlDbType.VarChar).Value = Me.m_authorization_name1
        _cmd.Parameters.Add("@pAuthorization2Title", SqlDbType.VarChar).Value = Me.m_authorization_title2
        _cmd.Parameters.Add("@pAuthorization2Name", SqlDbType.VarChar).Value = Me.m_authorization_name2
        _cmd.Parameters.Add("@pEffectiveDays", SqlDbType.VarChar).Value = Me.m_effective_days
        _cmd.Parameters.Add("@pApplicationDate", SqlDbType.DateTime).Value = New DateTime(Me.m_application_date.Year, Me.m_application_date.Month, Me.m_application_date.Day)
        _cmd.Parameters.Add("@pPaymentType", SqlDbType.VarChar).Value = Me.m_payment_type
        _cmd.Parameters.Add("@pBankAccountId", SqlDbType.BigInt).Value = Me.m_bank_account_id
        _cmd.Parameters.Add("@pBankName", SqlDbType.VarChar).Value = Me.m_bank_name
        _cmd.Parameters.Add("@pBankCustomerNumber", SqlDbType.VarChar).Value = Me.m_bank_customer_number
        _cmd.Parameters.Add("@pBankAccountNumber", SqlDbType.VarChar).Value = Me.m_bank_account_number
        _cmd.Parameters.Add("@pAccountBalance", SqlDbType.Money).Value = Me.m_account_balance
        _cmd.Parameters.Add("@pReturnBalance", SqlDbType.Money).Value = Me.m_return_balance
        _cmd.Parameters.Add("@pPrize", SqlDbType.Money).Value = Me.m_prize
        _cmd.Parameters.Add("@pWithHoldingTax1", SqlDbType.Money).Value = Me.m_witholding_tax1
        _cmd.Parameters.Add("@pWithHoldingTax2", SqlDbType.Money).Value = Me.m_witholding_tax2
        _cmd.Parameters.Add("@pWithHoldingTax3", SqlDbType.Money).Value = Me.m_witholding_tax3
        _cmd.Parameters.Add("@pFullPayment", SqlDbType.Money).Value = Me.m_full_payment
        _cmd.Parameters.Add("@pCashPayment", SqlDbType.Money).Value = Me.m_cash_payment
        _cmd.Parameters.Add("@pCheckPayment", SqlDbType.Money).Value = Me.m_check_payment

        If _cmd.ExecuteNonQuery() <> 1 Then
          Return ENUM_STATUS.STATUS_ERROR
        End If

      End Using

      Return ENUM_STATUS.STATUS_OK

    Catch _ex As Exception

      Return ENUM_STATUS.STATUS_ERROR

    End Try

  End Function ' Update

#End Region    ' Privates

#Region " Publics "

  '----------------------------------------------------------------------------
  ' PURPOSE: Reads the document associated from the database, identified
  '          by the document Id.
  '
  ' PARAMS:
  '   - INPUT: None
  '
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  '    TRUE : if the document load went ok
  '    FALSE : if not
  Public Function LoadDocument() As Boolean

    If m_document_id > 0 Then
      Using _db_trx As New DB_TRX()

        m_document_list = New DocumentList()

        Return TableDocuments.Load(m_document_id, DOCUMENT_TYPE.PAYMENT_ORDER, m_document_list, _db_trx.SqlTransaction)

      End Using
    End If

  End Function    ' LoadDocuments

  '----------------------------------------------------------------------------
  ' PURPOSE: Updates the document associated on the database, 
  '          depending on the document Id. 
  '
  ' PARAMS:
  '   - INPUT: None
  '
  '   - OUTPUT: None
  '
  ' RETURNS:
  '    TRUE : if the document update went ok
  '    FALSE : if not
  '
  ' NOTES:
  Public Function UpdateDocument() As Boolean

    Dim _sql_trx As SqlTransaction
    Dim _commit_trx As Boolean

    _commit_trx = False
    _sql_trx = Nothing

    Try

      If Not GUI_BeginSQLTransaction(_sql_trx) Then
        Exit Function
      End If

      If PaymentOrder.DB_CreatePaymentOrderDoc(m_operation_id, _sql_trx) Then

        _commit_trx = True
      End If

      Return False

    Catch ex As Exception
      ' Do nothing
      Debug.WriteLine(ex.Message)

    Finally
      ' Either commit or rollback
      If Not (_sql_trx.Connection Is Nothing) Then
        If _commit_trx Then
          _sql_trx.Commit()
        Else
          _sql_trx.Rollback()
        End If
      End If

      _sql_trx.Dispose()
      _sql_trx = Nothing

    End Try

    Return True

  End Function  ' UpdateDocument

#End Region     ' Publics

End Class
