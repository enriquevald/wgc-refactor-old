﻿'-------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_junkets_flyer.vb
' DESCRIPTION:   Flyer Class
' AUTHOR:        Mark Stansfield
' CREATION DATE: 04-APR-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 04-APR-2017  MS     Initial Version
'--------------------------------------------------------------------
Imports GUI_CommonOperations
Imports System.Runtime.InteropServices
Imports System.Xml
Imports WSI.Common
Imports GUI_CommonMisc
Imports System.Text
Imports System.Data.SqlClient
Imports GUI_Controls

Public Class CLASS_JUNKETS_FLYER
  Inherits CLASS_BASE

#Region " Structures "

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_FLYER
    Public id As Int64
    Public junket_id As Int64
    Public promotion As TYPE_PROMOTION
    Public flags As List(Of TYPE_FLAG)
    Public code As String
    Public num_created As Int32
    Public allow_reuse As Boolean
    Public comment As String
    Public show_pop_up As Boolean
    Public text_pop_up As String
    Public print_voucher As Boolean
    Public text_voucher As String
    Public title_voucher As String
    Public print_text_promotional_voucher As Boolean
    Public text_promotional_voucher As String
    Public creation As DateTime
    Public update As DateTime
    Public junket As CLASS_JUNKETS.TYPE_JUNKET

    Public Sub New(LoadFlags As Boolean)
      Me.creation = GUI_FormatDate(WGDB.Now, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
      Me.update = GUI_FormatDate(WGDB.Now, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
      Me.promotion = New TYPE_PROMOTION
      Me.junket = New CLASS_JUNKETS.TYPE_JUNKET

      If LoadFlags Then
        Me.flags = GetAllFlags()
      End If

    End Sub

  End Class

  Public Class InfoFormFlyer
    Public flyer_id As Int64
    Public flyer_code As String
    Public junket_id As Int64
    Public junket_name As String
    Public junket_code As String
    Public junket_expiration_date As DateTime
  End Class

  Public Class TYPE_PROMOTION
    Public id As Int64
    Public name As String
  End Class

  Public Class TYPE_FLAG
    Public flag_id As Int64
    Public count As Int32
    Public name As String
    Public type As AccountFlag.FlagTypes
  End Class

#End Region ' Structures

#Region " Members "

  Protected m_flyer As New TYPE_FLYER(True)
  Private Const AcceptedCharacters As String = "ybndrfg8ejkmcpqxot1uwisza345h769"

#End Region ' Members

#Region " Properties "

  Public Property Id() As Int64
    Get
      Return m_flyer.id
    End Get
    Set(ByVal value As Int64)
      m_flyer.id = value
    End Set
  End Property

  Public Property JunketId() As Int64
    Get
      Return m_flyer.junket_id
    End Get
    Set(ByVal value As Int64)
      m_flyer.junket_id = value
    End Set
  End Property

  Public Property Promotion() As TYPE_PROMOTION
    Get
      Return m_flyer.promotion
    End Get
    Set(ByVal value As TYPE_PROMOTION)
      m_flyer.promotion = value
    End Set
  End Property

  Public Property Flags() As List(Of TYPE_FLAG)
    Get
      Return m_flyer.flags
    End Get
    Set(ByVal value As List(Of TYPE_FLAG))
      m_flyer.flags = value
    End Set
  End Property

  Public Property Code() As String
    Get
      Return m_flyer.code
    End Get
    Set(ByVal value As String)
      m_flyer.code = value
    End Set
  End Property

  Public Property NumCreated() As Int32
    Get
      Return m_flyer.num_created
    End Get
    Set(ByVal value As Int32)
      m_flyer.num_created = value
    End Set
  End Property

  Public Property AllowReuse() As Boolean
    Get
      Return m_flyer.allow_reuse
    End Get
    Set(ByVal value As Boolean)
      m_flyer.allow_reuse = value
    End Set
  End Property

  Public Property Comment() As String
    Get
      Return m_flyer.comment
    End Get
    Set(ByVal value As String)
      m_flyer.comment = value
    End Set
  End Property

  Public Property ShowPopUp() As Boolean
    Get
      Return m_flyer.show_pop_up
    End Get
    Set(ByVal value As Boolean)
      m_flyer.show_pop_up = value
    End Set
  End Property

  Public Property TextPopUp() As String
    Get
      Return m_flyer.text_pop_up
    End Get
    Set(ByVal value As String)
      m_flyer.text_pop_up = value
    End Set
  End Property

  Public Property PrintVoucher() As Boolean
    Get
      Return m_flyer.print_voucher
    End Get
    Set(ByVal value As Boolean)
      m_flyer.print_voucher = value
    End Set
  End Property

  Public Property TextVoucher() As String
    Get
      Return m_flyer.text_voucher
    End Get
    Set(ByVal value As String)
      m_flyer.text_voucher = value
    End Set
  End Property

  Public Property TitleVoucher() As String
    Get
      Return m_flyer.title_voucher
    End Get
    Set(ByVal value As String)
      m_flyer.title_voucher = value
    End Set
  End Property

  Public Property PrintTextPromotionalVoucher() As String
    Get
      Return m_flyer.print_text_promotional_voucher
    End Get
    Set(ByVal value As String)
      m_flyer.print_text_promotional_voucher = value
    End Set
  End Property

  Public Property TextPromotionalVoucher() As String
    Get
      Return m_flyer.text_promotional_voucher
    End Get
    Set(ByVal value As String)
      m_flyer.text_promotional_voucher = value
    End Set
  End Property

  Public Property Creation() As DateTime
    Get
      Return m_flyer.creation
    End Get
    Set(ByVal value As DateTime)
      m_flyer.creation = value
    End Set
  End Property

  Public Property Update() As DateTime
    Get
      Return m_flyer.update
    End Get
    Set(ByVal value As DateTime)
      m_flyer.update = value
    End Set
  End Property

  Public Property Junket() As CLASS_JUNKETS.TYPE_JUNKET
    Get
      Return m_flyer.junket
    End Get
    Set(ByVal value As CLASS_JUNKETS.TYPE_JUNKET)
      m_flyer.junket = value
    End Set
  End Property

  Private m_is_new As Boolean
  Public Property isNew() As Boolean
    Get
      Return m_is_new
    End Get
    Set(ByVal Value As Boolean)
      m_is_new = value
    End Set
  End Property

#End Region ' Properties

#Region " Overrides "

  ''' <summary>
  '''   AuditorData
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function AuditorData() As CLASS_AUDITOR_DATA

    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _yes_text As String
    Dim _no_text As String

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_JUNKETS)

    _yes_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(278)
    _no_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)

    Call _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(8062), Me.Code)

    Call _auditor_data.SetField(0, IIf(String.IsNullOrEmpty(Code), AUDIT_NONE_STRING, Code), String.Format("{0} {1}", GLB_NLS_GUI_PLAYER_TRACKING.GetString(5710), GLB_NLS_GUI_PLAYER_TRACKING.GetString(8062)))

    Call _auditor_data.SetField(0, IIf(String.IsNullOrEmpty(Me.Junket.code), AUDIT_NONE_STRING, Me.Junket.code), String.Format("{0} {1}", GLB_NLS_GUI_PLAYER_TRACKING.GetString(5710), GLB_NLS_GUI_PLAYER_TRACKING.GetString(8014)))

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8077), IIf(String.IsNullOrEmpty(Me.NumCreated), AUDIT_NONE_STRING, Me.NumCreated))

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8075), IIf(Me.AllowReuse, _yes_text, _no_text))

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(256), IIf(String.IsNullOrEmpty(Me.Promotion.name), AUDIT_NONE_STRING, Me.Promotion.name))

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2715), IIf(Me.ShowPopUp, _yes_text, _no_text))

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5772), IIf(String.IsNullOrEmpty(Me.TextPopUp), AUDIT_NONE_STRING, Me.TextPopUp))

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8076), IIf(Me.PrintVoucher, _yes_text, _no_text))

    Call _auditor_data.SetField(0, IIf(String.IsNullOrEmpty(Me.TitleVoucher), AUDIT_NONE_STRING, Me.TitleVoucher), String.Format("{0} - {1}", GLB_NLS_GUI_PLAYER_TRACKING.GetString(7494), GLB_NLS_GUI_PLAYER_TRACKING.GetString(392)))

    Call _auditor_data.SetField(0, IIf(String.IsNullOrEmpty(Me.TextVoucher), AUDIT_NONE_STRING, Me.TextVoucher), String.Format("{0} - {1}", GLB_NLS_GUI_PLAYER_TRACKING.GetString(7494), GLB_NLS_GUI_PLAYER_TRACKING.GetString(8212)))

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1062), IIf(String.IsNullOrEmpty(Me.Comment), AUDIT_NONE_STRING, Me.Comment))

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8232), IIf(Me.PrintTextPromotionalVoucher, _yes_text, _no_text))

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8233), IIf(String.IsNullOrEmpty(Me.TextPromotionalVoucher), AUDIT_NONE_STRING, Me.TextPromotionalVoucher))

    If Me.Flags.FindAll(Function(_flg) _flg.type = AccountFlag.FlagTypes.Manual).Count > 0 Then
      For Each _flag As TYPE_FLAG In Me.Flags.FindAll(Function(_flg) _flg.type = AccountFlag.FlagTypes.Manual)
        If m_is_new Then
          If _flag.count > 0 Then
            Call _auditor_data.SetField(0, _flag.count, String.Format("{0}: {1}", GLB_NLS_GUI_PLAYER_TRACKING.GetString(8190), _flag.name))
          End If
        Else
          Call _auditor_data.SetField(0, _flag.count, String.Format("{0}: {1}", GLB_NLS_GUI_PLAYER_TRACKING.GetString(8190), _flag.name))
        End If
      Next
    Else
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8190), AUDIT_NONE_STRING)
    End If

    Return _auditor_data

  End Function ' AuditorData

  ''' <summary>
  '''   DB_Delete
  ''' </summary>
  ''' <param name="SqlCtx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As CLASS_BASE.ENUM_STATUS
    Return Nothing
  End Function ' DB_Delete

  ''' <summary>
  '''    DB_Insert
  ''' </summary>
  ''' <param name="SqlCtx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As CLASS_BASE.ENUM_STATUS
    Return InsertFlyer()
  End Function ' DB_Insert

  ''' <summary>
  '''    DB_Read
  ''' </summary>
  ''' <param name="ObjectId"></param>
  ''' <param name="SqlCtx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function DB_Read(ObjectId As Object, ByRef SqlCtx As Integer) As CLASS_BASE.ENUM_STATUS
    Return GetFlyer(ObjectId)
  End Function ' DB_Read

  ''' <summary>
  '''    DB_Update
  ''' </summary>
  ''' <param name="SqlCtx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As CLASS_BASE.ENUM_STATUS
    Return UpdateFlyer()
  End Function ' DB_Update

  ''' <summary>
  '''    Duplicate
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function Duplicate() As CLASS_BASE

    Dim _temp_object As CLASS_JUNKETS_FLYER

    _temp_object = Me.Clone

    Return _temp_object

  End Function ' Duplicate

#End Region ' Overrides

#Region " Private DB "

  ''' <summary>
  '''   InsertFlyer
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function InsertFlyer() As CLASS_BASE.ENUM_STATUS

    Dim _sb As StringBuilder
    Dim _sql_trx As SqlTransaction = Nothing
    Dim _param_id_flyer As SqlParameter

    Try

      If Not GUI_BeginSQLTransaction(_sql_trx) Then
        Return ENUM_STATUS.STATUS_ERROR
      End If

      _sb = New StringBuilder
      _sb.AppendLine(" INSERT INTO   JUNKETS_FLYERS          ")
      _sb.AppendLine("             ( JF_JUNKET_ID            ")
      _sb.AppendLine("             , JF_PROMOTION_ID         ")
      _sb.AppendLine("             , JF_CODE                 ")
      _sb.AppendLine("             , JF_NUM_CREATED          ")
      _sb.AppendLine("             , JF_ALLOW_REUSE          ")
      _sb.AppendLine("             , JF_COMMENT              ")
      _sb.AppendLine("             , JF_SHOW_POP_UP          ")
      _sb.AppendLine("             , JF_TEXT_POP_UP          ")
      _sb.AppendLine("             , JF_PRINT_VOUCHER        ")
      _sb.AppendLine("             , JF_TITLE_VOUCHER        ")
      _sb.AppendLine("             , JF_TEXT_VOUCHER         ")
      _sb.AppendLine("             , JF_PRINT_TEXT_PROMOTION ")
      _sb.AppendLine("             , JF_TEXT_PROMOTION       ")
      _sb.AppendLine("             , JF_CREATION             ")
      _sb.AppendLine("             , JF_UPDATE )             ")
      _sb.AppendLine("      VALUES                           ")
      _sb.AppendLine("             ( @pJunketId              ")
      _sb.AppendLine("             , @pPromotionId           ")
      _sb.AppendLine("             , @pCode                  ")
      _sb.AppendLine("             , @pNumCreated            ")
      _sb.AppendLine("             , @pAllowReuse            ")
      _sb.AppendLine("             , @pComment               ")
      _sb.AppendLine("             , @pShowPopUp             ")
      _sb.AppendLine("             , @pTextPopUp             ")
      _sb.AppendLine("             , @pPrintVoucher          ")
      _sb.AppendLine("             , @pTitleVoucher          ")
      _sb.AppendLine("             , @pTextVoucher           ")
      _sb.AppendLine("             , @pPrintTextPromotion    ")
      _sb.AppendLine("             , @pTextPromotion         ")
      _sb.AppendLine("             , @pCreation              ")
      _sb.AppendLine("             , @pUpdate )              ")
      _sb.AppendLine(" SET @pID = SCOPE_IDENTITY();          ")

      Using _cmd As New SqlClient.SqlCommand(_sb.ToString(), _sql_trx.Connection, _sql_trx)

        _cmd.Parameters.Add("@pJunketId", SqlDbType.BigInt).Value = m_flyer.junket_id
        _cmd.Parameters.Add("@pPromotionId", SqlDbType.BigInt).Value = If(m_flyer.promotion.id = 0, DBNull.Value, m_flyer.promotion.id)
        _cmd.Parameters.Add("@pCode", SqlDbType.NVarChar).Value = m_flyer.code
        _cmd.Parameters.Add("@pNumCreated", SqlDbType.Int).Value = m_flyer.num_created
        _cmd.Parameters.Add("@pAllowReuse", SqlDbType.Bit).Value = m_flyer.allow_reuse
        _cmd.Parameters.Add("@pComment", SqlDbType.NVarChar).Value = m_flyer.comment
        _cmd.Parameters.Add("@pShowPopUp", SqlDbType.Bit).Value = m_flyer.show_pop_up
        _cmd.Parameters.Add("@pPrintVoucher", SqlDbType.Bit).Value = m_flyer.print_voucher
        _cmd.Parameters.Add("@pTitleVoucher", SqlDbType.NVarChar).Value = m_flyer.title_voucher
        _cmd.Parameters.Add("@pTextVoucher", SqlDbType.NVarChar).Value = m_flyer.text_voucher
        _cmd.Parameters.Add("@pTextPopUp", SqlDbType.NVarChar).Value = m_flyer.text_pop_up
        _cmd.Parameters.Add("@pPrintTextPromotion", SqlDbType.Bit).Value = m_flyer.print_text_promotional_voucher
        _cmd.Parameters.Add("@pTextPromotion", SqlDbType.NVarChar).Value = m_flyer.text_promotional_voucher
        _cmd.Parameters.Add("@pCreation", SqlDbType.DateTime).Value = m_flyer.creation
        _cmd.Parameters.Add("@pUpdate", SqlDbType.DateTime).Value = m_flyer.update

        _param_id_flyer = _cmd.Parameters.Add("@pID", SqlDbType.BigInt)
        _param_id_flyer.Direction = ParameterDirection.Output

        If _cmd.ExecuteNonQuery() <> 1 Then
          GUI_EndSQLTransaction(_sql_trx, False)

          Return ENUM_STATUS.STATUS_ERROR
        End If

        m_flyer.id = _param_id_flyer.Value

        If Not InsertAutomaticFlag(_sql_trx) Then
          GUI_EndSQLTransaction(_sql_trx, False)

          Return ENUM_STATUS.STATUS_ERROR
        End If

        If Not InsertFlagsOnFlyer(False, _sql_trx) Then
          GUI_EndSQLTransaction(_sql_trx, False)

          Return ENUM_STATUS.STATUS_ERROR
        End If

        If Not GUI_EndSQLTransaction(_sql_trx, True) Then
          Return ENUM_STATUS.STATUS_ERROR
        End If

      End Using

      Return ENUM_STATUS.STATUS_OK

    Catch ex As Exception
      GUI_EndSQLTransaction(_sql_trx, False)
    End Try

    Return ENUM_STATUS.STATUS_ERROR

  End Function 'InsertFlyer

  ''' <summary>
  ''' Get flayer
  ''' </summary>
  ''' <param name="InfoFormFlyer"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetFlyer(ByRef InfoFormFlyer As CLASS_JUNKETS_FLYER.InfoFormFlyer) As CLASS_BASE.ENUM_STATUS

    Dim _sb As StringBuilder
    Dim _sql_trx As SqlTransaction = Nothing
    Dim _ds As DataSet
    Dim _row_junket_flayer As DataRow
    Dim _flag As TYPE_FLAG

    Try

      _sb = New StringBuilder
      _ds = New DataSet
      _row_junket_flayer = Nothing

      If Not GUI_BeginSQLTransaction(_sql_trx) Then
        Return ENUM_STATUS.STATUS_ERROR
      End If

      _sb.AppendLine("       SELECT   JF_ID                                                   ")
      _sb.AppendLine("              , JF_JUNKET_ID                                            ")
      _sb.AppendLine("              , JF_PROMOTION_ID                                         ")
      _sb.AppendLine("              , JF_CODE                                                 ")
      _sb.AppendLine("              , JF_NUM_CREATED                                          ")
      _sb.AppendLine("              , JF_ALLOW_REUSE                                          ")
      _sb.AppendLine("              , JF_COMMENT                                              ")
      _sb.AppendLine("              , JF_SHOW_POP_UP                                          ")
      _sb.AppendLine("              , JF_TEXT_POP_UP                                          ")
      _sb.AppendLine("              , JF_PRINT_VOUCHER                                        ")
      _sb.AppendLine("              , JF_TITLE_VOUCHER                                        ")
      _sb.AppendLine("              , JF_TEXT_VOUCHER                                         ")
      _sb.AppendLine("              , JF_PRINT_TEXT_PROMOTION                                 ")
      _sb.AppendLine("              , JF_TEXT_PROMOTION                                       ")
      _sb.AppendLine("              , JF_CREATION                                             ")
      _sb.AppendLine("              , JF_UPDATE                                               ")
      _sb.AppendLine("              , PM_NAME                                                 ")
      _sb.AppendLine("         FROM   JUNKETS_FLYERS F                                        ")
      _sb.AppendLine("   LEFT JOIN   PROMOTIONS P ON P.PM_PROMOTION_ID = F.JF_PROMOTION_ID    ")
      _sb.AppendLine("        WHERE   JF_ID = @pId;                                           ")

      _sb.AppendLine("       SELECT   JFF_FLYER_ID                                            ")
      _sb.AppendLine("              , JFF_FLAG_ID                                             ")
      _sb.AppendLine("              , JFF_FLAG_COUNT                                          ")
      _sb.AppendLine("         FROM   JUNKETS_FLYERS_FLAGS FF                                 ")
      _sb.AppendLine("        WHERE   JFF_FLYER_ID = @pId                                     ")

      Using _cmd As New SqlClient.SqlCommand(_sb.ToString(), _sql_trx.Connection, _sql_trx)

        _cmd.Parameters.Add("@pId", SqlDbType.BigInt).Value = InfoFormFlyer.flyer_id
        _cmd.Parameters.Add("@pType", SqlDbType.BigInt).Value = AccountFlag.FlagTypes.Manual

        Using _reader As SqlDataAdapter = New SqlDataAdapter(_cmd)

          _reader.Fill(_ds)

          _ds.Tables(0).TableName = "JUNKETS_FLAYERS"
          _ds.Tables(1).TableName = "JUNKETS_FLAYERS_FLAGS"

          If _ds.Tables("JUNKETS_FLAYERS").Rows.Count > 0 Then

            _row_junket_flayer = _ds.Tables("JUNKETS_FLAYERS").Rows(0)

            m_flyer.id = _row_junket_flayer("JF_ID")
            m_flyer.junket_id = _row_junket_flayer("JF_JUNKET_ID")
            m_flyer.code = _row_junket_flayer("JF_CODE")
            m_flyer.num_created = _row_junket_flayer("JF_NUM_CREATED")
            m_flyer.allow_reuse = _row_junket_flayer("JF_ALLOW_REUSE")
            m_flyer.comment = _row_junket_flayer("JF_COMMENT").ToString
            m_flyer.show_pop_up = _row_junket_flayer("JF_SHOW_POP_UP")
            m_flyer.text_pop_up = _row_junket_flayer("JF_TEXT_POP_UP").ToString
            m_flyer.print_voucher = _row_junket_flayer("JF_PRINT_VOUCHER")
            m_flyer.title_voucher = _row_junket_flayer("JF_TITLE_VOUCHER").ToString
            m_flyer.text_voucher = _row_junket_flayer("JF_TEXT_VOUCHER").ToString
            m_flyer.print_text_promotional_voucher = _row_junket_flayer("JF_PRINT_TEXT_PROMOTION")
            m_flyer.text_promotional_voucher = _row_junket_flayer("JF_TEXT_PROMOTION").ToString
            m_flyer.creation = _row_junket_flayer("JF_CREATION")
            m_flyer.update = _row_junket_flayer("JF_UPDATE")
            m_flyer.promotion.id = IIf(IsDBNull(_row_junket_flayer("JF_PROMOTION_ID")), 0, _row_junket_flayer("JF_PROMOTION_ID"))
            m_flyer.promotion.name = _row_junket_flayer("PM_NAME").ToString

          End If

          If _ds.Tables("JUNKETS_FLAYERS_FLAGS").Rows.Count > 0 Then
            For Each _flg As DataRow In _ds.Tables("JUNKETS_FLAYERS_FLAGS").Rows

              _flag = m_flyer.flags.Find(Function(_f) _f.flag_id = _flg("JFF_FLAG_ID"))

              If Not _flag Is Nothing Then
                _flag.count = _flg("JFF_FLAG_COUNT")
              End If

            Next
          End If

        End Using
      End Using

      Return ENUM_STATUS.STATUS_OK

    Catch ex As Exception
      GUI_EndSQLTransaction(_sql_trx, False)
    End Try

    Return ENUM_STATUS.STATUS_ERROR

  End Function ' GetFlyer

  ''' <summary>
  ''' Update flyer
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function UpdateFlyer() As Integer

    Dim _sb As StringBuilder
    Dim _sql_trx As SqlTransaction = Nothing

    Try

      If Not GUI_BeginSQLTransaction(_sql_trx) Then

        Return ENUM_STATUS.STATUS_ERROR
      End If

      _sb = New StringBuilder
      _sb.AppendLine("   UPDATE   JUNKETS_FLYERS                                 ")
      _sb.AppendLine("      SET   JF_PROMOTION_ID         = @pPromotionId        ")
      _sb.AppendLine("          , JF_NUM_CREATED          = @pNumCreated         ")
      _sb.AppendLine("          , JF_ALLOW_REUSE          = @pAllowReuse         ")
      _sb.AppendLine("          , JF_COMMENT              = @pComment            ")
      _sb.AppendLine("          , JF_SHOW_POP_UP          = @pShowPopUp          ")
      _sb.AppendLine("          , JF_TEXT_POP_UP          = @pTextPopUp          ")
      _sb.AppendLine("          , JF_PRINT_VOUCHER        = @pPrintVoucher       ")
      _sb.AppendLine("          , JF_TITLE_VOUCHER        = @pTitleVoucher       ")
      _sb.AppendLine("          , JF_TEXT_VOUCHER         = @pTextVoucher        ")
      _sb.AppendLine("          , JF_PRINT_TEXT_PROMOTION = @pPrintTextPromotion ")
      _sb.AppendLine("          , JF_TEXT_PROMOTION       = @pTextPromotion      ")
      _sb.AppendLine("          , JF_UPDATE               = @pUpdate             ")
      _sb.AppendLine("    WHERE   JF_ID                   = @pFlyerId            ")

      Using _cmd As New SqlClient.SqlCommand(_sb.ToString(), _sql_trx.Connection, _sql_trx)

        _cmd.Parameters.Add("@pFlyerId", SqlDbType.BigInt).Value = m_flyer.id

        _cmd.Parameters.Add("@pPromotionId", SqlDbType.BigInt).Value = If(m_flyer.promotion.id = 0, DBNull.Value, m_flyer.promotion.id)
        _cmd.Parameters.Add("@pNumCreated", SqlDbType.Int).Value = m_flyer.num_created
        _cmd.Parameters.Add("@pAllowReuse", SqlDbType.Bit).Value = m_flyer.allow_reuse
        _cmd.Parameters.Add("@pComment", SqlDbType.NVarChar).Value = m_flyer.comment
        _cmd.Parameters.Add("@pShowPopUp", SqlDbType.Bit).Value = m_flyer.show_pop_up
        _cmd.Parameters.Add("@pTextPopUp", SqlDbType.NVarChar).Value = m_flyer.text_pop_up
        _cmd.Parameters.Add("@pPrintVoucher", SqlDbType.Bit).Value = m_flyer.print_voucher
        _cmd.Parameters.Add("@pTitleVoucher", SqlDbType.NVarChar).Value = m_flyer.title_voucher
        _cmd.Parameters.Add("@pTextVoucher", SqlDbType.NVarChar).Value = m_flyer.text_voucher
        _cmd.Parameters.Add("@pPrintTextPromotion", SqlDbType.NVarChar).Value = m_flyer.print_text_promotional_voucher
        _cmd.Parameters.Add("@pTextPromotion", SqlDbType.NVarChar).Value = m_flyer.text_promotional_voucher
        _cmd.Parameters.Add("@pUpdate", SqlDbType.DateTime).Value = GUI_GetDateTime()

        If _cmd.ExecuteNonQuery() <> 1 Then
          GUI_EndSQLTransaction(_sql_trx, False)

          Return ENUM_STATUS.STATUS_ERROR
        End If

        If Not DeleteFlagsLinkedFlyer(_sql_trx) Then
          GUI_EndSQLTransaction(_sql_trx, False)

          Return ENUM_STATUS.STATUS_ERROR

        End If

        If Not InsertFlagsOnFlyer(True, _sql_trx) Then
          GUI_EndSQLTransaction(_sql_trx, False)

          Return ENUM_STATUS.STATUS_ERROR
        End If

        If Not GUI_EndSQLTransaction(_sql_trx, True) Then
          Return ENUM_STATUS.STATUS_ERROR
        End If

      End Using

      Return ENUM_STATUS.STATUS_OK

    Catch _ex As Exception
      GUI_EndSQLTransaction(_sql_trx, False)
    End Try

    Return ENUM_STATUS.STATUS_ERROR

  End Function ' UpdateFlyer

#End Region ' Private DB

#Region " Private Methods "

  ''' <summary>
  ''' Clone a object
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function Clone() As CLASS_JUNKETS_FLYER

    Dim _flyer As CLASS_JUNKETS_FLYER
    Dim _promotion As TYPE_PROMOTION
    Dim _junket As CLASS_JUNKETS.TYPE_JUNKET
    Dim _flag As TYPE_FLAG

    _flyer = New CLASS_JUNKETS_FLYER
    _promotion = New TYPE_PROMOTION
    _junket = New CLASS_JUNKETS.TYPE_JUNKET

    _flyer.Id = Me.m_flyer.id
    _flyer.JunketId = Me.m_flyer.junket_id

    _promotion.id = Me.m_flyer.promotion.id
    _promotion.name = Me.m_flyer.promotion.name
    _flyer.Promotion = _promotion

    _flyer.Code = Me.m_flyer.code
    _flyer.NumCreated = Me.m_flyer.num_created
    _flyer.AllowReuse = Me.m_flyer.allow_reuse
    _flyer.Comment = Me.m_flyer.comment
    _flyer.ShowPopUp = Me.m_flyer.show_pop_up
    _flyer.TextPopUp = Me.m_flyer.text_pop_up
    _flyer.PrintVoucher = Me.m_flyer.print_voucher
    _flyer.TitleVoucher = Me.m_flyer.title_voucher
    _flyer.TextVoucher = Me.m_flyer.text_voucher
    _flyer.TextPromotionalVoucher = Me.m_flyer.text_promotional_voucher
    _flyer.PrintTextPromotionalVoucher = Me.m_flyer.print_text_promotional_voucher
    _flyer.Creation = Me.m_flyer.creation

    _junket.id = Me.m_flyer.junket.id
    _junket.code = Me.m_flyer.junket.code
    _junket.name = Me.m_flyer.junket.name
    _junket.date_to = Me.m_flyer.junket.date_to

    _flyer.Junket = _junket

    _flyer.Flags.Clear()

    For Each _flg As TYPE_FLAG In Me.m_flyer.flags

      _flag = New TYPE_FLAG
      _flag.flag_id = _flg.flag_id
      _flag.name = _flg.name
      _flag.type = _flg.type
      _flag.count = _flg.count

      _flyer.Flags.Add(_flag)

    Next

    _flyer.isNew = Me.isNew

    Return _flyer

  End Function ' Clone

  ''' <summary>
  ''' Check if the code is unique
  ''' </summary>
  ''' <param name="Code"></param>
  ''' <param name="IsActiveJunket"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ExistCodeFlyer(Code As String, Optional IsActiveJunket As Boolean = False) As Boolean

    Dim _sb As StringBuilder

    Try

      _sb = New StringBuilder
      _sb.AppendLine("     SELECT   COUNT(1)                                                                         ")
      _sb.AppendLine("       FROM   JUNKETS_FLYERS                                                                   ")

      If IsActiveJunket Then
        _sb.AppendLine(" INNER JOIN   JUNKETS ON JU_ID = JF_JUNKET_ID AND JU_DATE_FROM <= @pNow AND JU_DATE_TO > @pNow ")
      End If

      _sb.AppendLine("      WHERE   JF_CODE = @pCode                                                                 ")

      Using _db_trx As New WSI.Common.DB_TRX()
        Using _cmd As New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _cmd.Parameters.Add("@pCode", SqlDbType.NVarChar).Value = Code
          _cmd.Parameters.Add("@pNow", SqlDbType.DateTime).Value = WGDB.Now

          Return (_cmd.ExecuteScalar() > 0)

        End Using
      End Using

      Return True

    Catch _ex As Exception
      Log.Error(_ex.Message())
    End Try

    Return False

  End Function 'IsUnique

  ''' <summary>
  ''' Insert automatic flag after create a new flyer
  ''' </summary>
  ''' <param name="Trx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function InsertAutomaticFlag(Trx As SqlTransaction) As Boolean

    Dim _flag As CLASS_FLAG
    Dim _auto_flag As TYPE_FLAG

    Try

      _flag = New CLASS_FLAG

      _flag.FlagType = AccountFlag.FlagTypes.Automatic
      _flag.FlagStatus = CLASS_FLAG.STATUS.ENABLED
      _flag.ExpirationType = FLAG_EXPIRATION_TYPE.NEVER
      _flag.ExpirationDate = CLASS_FLAG.DATE_NULL
      _flag.FlagCreated = GUI_FormatDate(WGDB.Now, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
      _flag.FlagName = String.Format("FLYER_{0}_{1}", Me.Id, Me.Code)
      _flag.FlagDescription = Me.Junket.name
      _flag.RelatedId = Me.Id
      _flag.RelatedType = AccountFlag.RelatedTypes.FLYER
      _flag.FlagColor = -1

      If _flag.InsertFlag(Trx, False) <> ENUM_CONFIGURATION_RC.CONFIGURATION_OK Then
        Return False
      End If

      _auto_flag = New TYPE_FLAG

      _auto_flag.flag_id = _flag.FlagId
      _auto_flag.count = 1
      _auto_flag.name = _flag.FlagName
      _auto_flag.type = _flag.FlagType

      m_flyer.flags.Add(_auto_flag)

      Return True

    Catch _ex As Exception
      Log.Error(_ex.Message())
    End Try

    Return False

  End Function ' InsertAutomaticFlag

  ''' <summary>
  ''' Delete flgas linked flyer
  ''' </summary>
  ''' <param name="Trx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function DeleteFlagsLinkedFlyer(Trx As SqlTransaction) As Boolean

    Dim _sb As StringBuilder

    Try

      _sb = New StringBuilder
      _sb.AppendLine("       DELETE   FF                                         ")
      _sb.AppendLine("         FROM   JUNKETS_FLYERS_FLAGS FF                    ")
      _sb.AppendLine("   INNER JOIN   FLAGS F ON F.FL_FLAG_ID = FF.JFF_FLAG_ID   ")
      _sb.AppendLine("        WHERE   FF.JFF_FLYER_ID = @pFlyerId                ")
      _sb.AppendLine("          AND   F.FL_TYPE       = @pType                   ")

      Using _cmd As New SqlClient.SqlCommand(_sb.ToString, Trx.Connection, Trx)

        _cmd.Parameters.Add("@pFlyerId", SqlDbType.BigInt).Value = m_flyer.id
        _cmd.Parameters.Add("@pType", SqlDbType.Int).Value = AccountFlag.FlagTypes.Manual

        _cmd.ExecuteNonQuery()

      End Using

      Return True

    Catch _ex As Exception
      Log.Error(_ex.Message())
    End Try

    Return False

  End Function ' DeleteFlagsLinkedFlyer

  ''' <summary>
  ''' Insert flags linked flyer
  ''' </summary>
  ''' <param name="OnlyManuals"></param>
  ''' <param name="Trx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function InsertFlagsOnFlyer(OnlyManuals As Boolean, Trx As SqlTransaction) As Boolean

    Dim _sb As StringBuilder
    Dim _sql_cmd_upd As SqlCommand
    Dim _num_updated As Integer
    Dim _dt_flags As DataTable

    Try

      _sb = New StringBuilder
      _dt_flags = FlagsToDatatable(True, OnlyManuals)

      _sb.AppendLine("   INSERT INTO   JUNKETS_FLYERS_FLAGS   ")
      _sb.AppendLine("   			       ( JFF_FLYER_ID           ")
      _sb.AppendLine("   			       , JFF_FLAG_ID            ")
      _sb.AppendLine("   			       , JFF_FLAG_COUNT )       ")
      _sb.AppendLine("        VALUES                          ")
      _sb.AppendLine("               ( @pFlyerId              ")
      _sb.AppendLine("   			       , @pFlagId               ")
      _sb.AppendLine("   			       , @pCount )              ")

      Using _da As New SqlDataAdapter()

        _sql_cmd_upd = New SqlCommand(_sb.ToString(), Trx.Connection, Trx)
        _sql_cmd_upd.Parameters.Add("@pFlyerId", SqlDbType.BigInt).SourceColumn = "FLYER_ID"
        _sql_cmd_upd.Parameters.Add("@pFlagId", SqlDbType.BigInt).SourceColumn = "FLAG_ID"
        _sql_cmd_upd.Parameters.Add("@pCount", SqlDbType.Int).SourceColumn = "COUNT"
        _da.InsertCommand = _sql_cmd_upd

        _num_updated = _da.Update(_dt_flags)

      End Using

      If _num_updated <> _dt_flags.Rows.Count Then
        Return False
      End If

      Return True

    Catch _ex As Exception
      Log.Error(_ex.Message())
    End Try

    Return False

  End Function ' InsertFlagsOnFlyer

#End Region ' Private Methods

#Region " Public Methods "

  ''' <summary>
  ''' Convert list flags to datatable
  ''' </summary>
  ''' <param name="OnlyFlagsWithValue"></param>
  ''' <param name="OnlyManuals"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function FlagsToDatatable(OnlyFlagsWithValue As Boolean, OnlyManuals As Boolean) As DataTable

    Dim _dt_flags As DataTable
    Dim _row As DataRow
    Dim _list_flags As List(Of TYPE_FLAG)

    Try

      _list_flags = m_flyer.flags
      _dt_flags = New DataTable

      _dt_flags.Columns.Add("FLYER_ID", GetType(Int64))
      _dt_flags.Columns.Add("FLAG_ID", GetType(Int64))
      _dt_flags.Columns.Add("COUNT", GetType(Int32))
      _dt_flags.Columns.Add("NAME", GetType(String))
      _dt_flags.Columns.Add("TYPE", GetType(AccountFlag.FlagTypes))

      If OnlyFlagsWithValue Then
        _list_flags = _list_flags.FindAll(Function(_flg) _flg.count > 0)
      End If

      If OnlyManuals Then
        _list_flags = _list_flags.FindAll(Function(_flg) _flg.type = AccountFlag.FlagTypes.Manual)
      End If

      For Each _flag As TYPE_FLAG In _list_flags

        _row = _dt_flags.NewRow
        _row("FLYER_ID") = m_flyer.id
        _row("FLAG_ID") = _flag.flag_id
        _row("COUNT") = _flag.count
        _row("NAME") = _flag.name
        _row("TYPE") = _flag.type
        _dt_flags.Rows.Add(_row)

      Next

      Return _dt_flags

    Catch _ex As Exception
      Log.Error(_ex.Message())
    End Try

    Return Nothing

  End Function ' FlagsToDatatable

  ''' <summary>
  ''' Get all flags
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function GetAllFlags() As List(Of TYPE_FLAG)

    Dim _sql_trx As SqlTransaction = Nothing
    Dim _dt As DataTable
    Dim _flag As TYPE_FLAG
    Dim _flags As List(Of TYPE_FLAG)
    Try
      _dt = New DataTable
      _flags = New List(Of TYPE_FLAG)

      If Not GUI_BeginSQLTransaction(_sql_trx) Then

        Return Nothing
      End If

      CLASS_FLAG.GetAllFlags(_sql_trx, _dt, True)

      If _dt.Rows.Count > 0 Then

        For Each _flg As DataRow In _dt.Rows

          _flag = New TYPE_FLAG
          _flag.flag_id = _flg("FL_FLAG_ID")
          _flag.count = 0
          _flag.name = _flg("FL_NAME")
          _flag.type = _flg("FL_TYPE")
          _flags.Add(_flag)

        Next
      End If

      If Not GUI_EndSQLTransaction(_sql_trx, True) Then
        Return Nothing
      End If

      Return _flags

    Catch _ex As Exception
      GUI_EndSQLTransaction(_sql_trx, False)
    End Try

    Return Nothing

  End Function ' GetAllFlags

  ''' <summary>
  ''' Generate code Crockford base32
  ''' </summary>
  ''' <param name="Lenght"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function GenerateCodeCrockfordBase32(Lenght As Int16) As String
    Dim rand As New Random()
    Dim allowableChars() As Char = ("0123456789ABCDEFLJKMNPQRTUVWXYZ").ToCharArray()
    Dim final As String = String.Empty

    For i As Integer = 0 To Lenght - 1
      final += allowableChars(rand.Next(allowableChars.Length - 1))
    Next

    Return final
  End Function ' GenerateCodeCrockfordBase32

  ''' <summary>
  ''' Generate code Crockford base32
  ''' </summary>
  ''' <param name="Prefix"></param>
  ''' <param name="Sufix"></param>
  ''' <param name="Lenght"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function GenerateUniqueCode(Prefix As String, Sufix As String, Lenght As Int16) As String

    Dim _str_code As String

    _str_code = String.Format("{0}{1}{2}", Prefix, GenerateCodeCrockfordBase32(Lenght - Len(Prefix) - Len(Sufix)), Sufix)

    If ExistCodeFlyer(_str_code) Then

      While ExistCodeFlyer(_str_code)
        _str_code = String.Format("{0}{1}{2}", Prefix, GenerateCodeCrockfordBase32(Lenght - Len(Prefix) - Len(Sufix)), Sufix)
      End While

    End If

    Return _str_code.ToUpper

  End Function ' GenerateCodeCrockfordBase32

#End Region ' Public Methods

#Region " Report "

#Region " Public methods "

  ''' <summary>
  ''' String Query for get the flyers report 
  ''' </summary>
  ''' <param name="ListRepresentatives"></param>
  ''' <param name="ListJunkets"></param>
  ''' <param name="FlyerCode"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function GetSQLFilter(ByRef ListRepresentatives As String, ByRef ListJunkets As String, FlyerCode As String) As String

    Dim _sb As StringBuilder

    _sb = New StringBuilder()
    _sb.AppendLine("   SELECT   JU_ID                                                                                 ")
    _sb.AppendLine("          , JU_NAME                                                                               ")
    _sb.AppendLine("          , JF_ID                                                                                 ")
    _sb.AppendLine("          , JF_CODE                                                                               ")
    _sb.AppendLine("          , JF_NUM_CREATED                                                                        ")
    _sb.AppendLine("          , NUMS_USES                                                                             ")
    _sb.AppendLine("   		    , CASE   JF_NUM_CREATED                                                                 ")
    _sb.AppendLine("             WHEN  NULL THEN 0                                                                    ")
    _sb.AppendLine("             WHEN  0 THEN 0                                                                       ")
    _sb.AppendLine("             ELSE  (CAST(NUMS_USES AS FLOAT) / CAST(JF_NUM_CREATED AS FLOAT)) * 100               ")
    _sb.AppendLine("            END AS PCT                                                                            ")
    _sb.AppendLine("          , JR_NAME                                                                               ")
    _sb.AppendLine("          , JU_CODE                                                                               ")
    _sb.AppendLine("     FROM (                                                                                       ")
    _sb.AppendLine("               SELECT   JU_ID                                                                     ")
    _sb.AppendLine("                      , JU_NAME                                                                   ")
    _sb.AppendLine("                      , JF_ID                                                                     ")
    _sb.AppendLine("                      , JF_CODE                                                                   ")
    _sb.AppendLine("                      , ISNULL(JF_NUM_CREATED, 0) AS JF_NUM_CREATED                               ")
    _sb.AppendLine("                      , COUNT(AF_FLAG_ID) AS NUMS_USES                                            ")
    _sb.AppendLine("                      , JR_NAME                                                                   ")
    _sb.AppendLine("                      , JU_CODE                                                                   ")
    _sb.AppendLine("                 FROM   JUNKETS                                                                   ")
    _sb.AppendLine("            LEFT JOIN   JUNKETS_FLYERS ON JF_JUNKET_ID = JU_ID                                    ")
    _sb.AppendLine("            LEFT JOIN   JUNKETS_FLYERS_FLAGS ON JFF_FLYER_ID = JF_ID                              ")
    _sb.AppendLine("          INNER JOIN   FLAGS ON FL_FLAG_ID = JFF_FLAG_ID AND FL_TYPE = 1 AND FL_RELATED_TYPE = 1  ")
    _sb.AppendLine("            LEFT JOIN   ACCOUNT_FLAGS ON AF_FLAG_ID = JFF_FLAG_ID                                 ")
    _sb.AppendLine("            LEFT JOIN   JUNKETS_REPRESENTATIVES ON JR_ID = JU_REPRESENTATIVE_ID                   ")
    _sb.AppendLine("                WHERE   JF_ID IS NOT NULL                                                         ")

    If Not String.IsNullOrEmpty(ListRepresentatives) Then
      _sb.AppendLine(String.Format(" 	                AND   JU_REPRESENTATIVE_ID IN ({0})      ", ListRepresentatives))
    End If

    If Not String.IsNullOrEmpty(ListJunkets) Then
      _sb.AppendLine(String.Format("                  AND   JU_ID IN ({0})                  ", ListJunkets))
    End If

    If Not String.IsNullOrEmpty(FlyerCode) Then
      _sb.AppendLine(String.Format("                  AND   {0}                                ", GUI_FilterField("JF_CODE", FlyerCode, False, False, True)))
    End If

    _sb.AppendLine("             GROUP BY   JU_ID, JU_NAME, JF_ID, JF_CODE, JF_NUM_CREATED, JR_NAME, JU_CODE   ")
    _sb.AppendLine("          ) AS TABLA                                                                       ")
    _sb.AppendLine(" ORDER BY   JU_NAME                                                                        ")

    Return _sb.ToString

  End Function ' GetSQLFilter

  ''' <summary>
  ''' Get results for set a filter grids
  ''' </summary>
  ''' <param name="Result"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function GetDataForFilters(Result As DataSet) As Boolean

    Dim _sb As StringBuilder

    Try

      _sb = New StringBuilder
      _sb.AppendLine(getSQLRepresentatives())
      _sb.AppendLine(getSQLJunkets())

      Using _db_trx As New WSI.Common.DB_TRX()
        Using _cmd As New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          Using _reader As SqlDataAdapter = New SqlDataAdapter(_cmd)

            _reader.Fill(Result)

            Result.Tables(0).TableName = "JUNKETS_REPRESENTATIVES"
            Result.Tables(1).TableName = "JUNKETS"

          End Using
        End Using
      End Using

      Return True

    Catch _ex As Exception
      Log.Error(_ex.Message())
    End Try

    Return False

  End Function ' GetDataForFilters

#End Region ' Public methods

#Region " Private methods "

  ''' <summary>
  ''' Get SQL for get respresentatives junket
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Shared Function getSQLRepresentatives() As String

    Dim _sb As StringBuilder

    _sb = New StringBuilder()
    _sb.AppendLine("   SELECT   JR_ID AS ID             ")
    _sb.AppendLine("          , JR_NAME AS DESCRIPTION  ")
    _sb.AppendLine("     FROM   JUNKETS_REPRESENTATIVES ")
    _sb.AppendLine(" ORDER BY   JR_NAME                 ")

    Return _sb.ToString

  End Function ' getSQLRepresentatives

  ''' <summary>
  ''' Get SQL for get junkets
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Shared Function getSQLJunkets() As String

    Dim _sb As StringBuilder

    _sb = New StringBuilder()
    _sb.AppendLine("   SELECT   JU_ID AS ID             ")
    _sb.AppendLine("          , JU_NAME AS DESCRIPTION  ")
    _sb.AppendLine("     FROM   JUNKETS                 ")
    _sb.AppendLine(" ORDER BY   JU_NAME                 ")

    Return _sb.ToString

  End Function ' getSQLJunkets

#End Region ' Private methods

#End Region ' Report

End Class