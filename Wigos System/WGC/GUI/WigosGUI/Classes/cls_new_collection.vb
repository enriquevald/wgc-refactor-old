'-------------------------------------------------------------------
' Copyright © 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : cls_password_configuration.vb
'
' DESCRIPTION : Configure Password Setting
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 03-AUG-2013  HBB    Initial version   
' 10-OCT-2013  LEM    Create mobile bank movements for bill collection (UpdateStackerCollection)   
' 19-MAR-2014  JFC    Deleted unused code
' 06-AUG-2014  DRV    Added floor id
' 29-OCT-2014  DRV    Fixed Bug WIGOSTITO-1256: Denominations lower than 1 are not saved
' 08-SEP-2015  FOS    Backlog Item 3709
' 22-SEP-2015  FOS    Task 4458:Recaudación de monedas: Mejoras recaudación
' 13-JAN-2016  DHA    Task 8287: Floor Dual Currency
'--------------------------------------------------------------------
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports System.Text
Imports WSI.Common


Public Class NEW_COLLECTION

  Public Structure TYPE_MONEY_COLLECTION_DETAILS

    Public money_collection_id As Int64
    Public type As Int16                    ' 0-note 1-ticket
    Public ticket_validation_number As Int64
    Public ticket_validation_type As Int32
    Public ticket_amount As Double
    Public bill_denomination As Decimal
    Public bill_real_count As Decimal
    Public bill_theoretical_count As Decimal
    Public bill_real_vs_theo_count_diff As Decimal
    Public matched As Boolean               ' theoretical match with real
    Public ticket_collected As Boolean
    Public ticket_id As Int64
    Public ticketd_collected_money_collection As Int64
    Public ticket_type As Int32
    Public ticket_status As TITO_TICKET_STATUS
    Public redeem_ticket_amount As Decimal
    Public redeem_ticket_count As Int32
    Public promo_redeem_ticket_amount As Decimal
    Public promo_redeem_ticket_count As Int32
    Public non_redeem_ticket_amount As Decimal
    Public non_redeem_ticket_count As Int32
    Public ticket_cage_movement_id As Int64
    Public pending_to_especify As Boolean
    Public currency_type As CageCurrencyType
    Public coin_total_from_terminals As Decimal

    ' DHA 13-JAN-2016: floor dual currency
    Public ticket_amount_issue As Decimal
    Public ticket_currency_issue As String

  End Structure

  Public Structure TYPE_NEW_COLLECTION

    ' Table variables 
    Public user_id As Int32
    Public user_insertion_id As Int32
    Public balanced As Boolean
    Public stacker_id As Int64
    Public floor_id As String
    Public money_collection_id As Int64
    Public status As TITO_MONEY_COLLECTION_STATUS
    Public terminal_id As Int32
    Public insertion_date As TYPE_DATE_TIME
    Public extraction_date As TYPE_DATE_TIME
    Public collection_date As TYPE_DATE_TIME
    Public notes As String
    Public is_coin_collection_enabled As Boolean

    ' Helper Variables (Not contained in table)
    Public terminal_name As String
    Public user_name As String
    Public real_ticket_count As Int32
    Public real_ticket_sum As Decimal
    Public real_bills_count As Int32
    Public real_bills_sum As Decimal

    Public real_redeem_ticket_amount As Decimal
    Public real_redeem_ticket_count As Int32
    Public real_promo_redeem_ticket_amount As Decimal
    Public real_promo_redeem_ticket_count As Int32
    Public real_non_redeem_ticket_amount As Decimal
    Public real_non_redeem_ticket_count As Int32

    Public theoretical_bills_count As Int32
    Public theoretical_bills_sum As Decimal
    Public theoretical_ticket_count As Int32
    Public theoretical_ticket_sum As Decimal
    Public collection_details As List(Of TYPE_MONEY_COLLECTION_DETAILS)
    Public already_collected As Boolean
    Public alarm_message As String
    Public note_counter_events As String
    Public real_coins_collection_amount As Decimal

  End Structure

End Class
