'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : cls_cage_session_detail.vb
'
' DESCRIPTION : Cage session detail class
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 18-DEC-2013  JAB    Initial version
' 28-MAY-2015  TPF    Fixed Bug WIG-2394: Change vault session date. Add new member m_session_opening_user_id
'--------------------------------------------------------------------

Imports System.Data.SqlClient
Imports System.Text
Imports WSI.Common
Imports GUI_CommonOperations

Public Class cls_cage_session_detail

#Region " ENUMS "

#End Region

#Region " CONSTANTS "



#End Region

#Region " MEMBERS "

  Public m_session_id As Int64
  Public m_session_status As String
  Public m_session_name As String
  Public m_session_opening_user As String
  Public m_session_opening_user_id As String
  Public m_session_opening_date As String
  Public m_session_closing_user As String
  Public m_session_closing_date As String
  Public m_currency_exchange_iso_codes As List(Of String)
  Public m_currency_exchange_iso_description As List(Of String)
  Public m_session_working_day As String

#End Region

#Region " PROPERTIES "

#End Region

#Region " Public "

  Public Sub New()

    Call LoadIsoCodes()

  End Sub ' cls_cage_session_detail

  ' PURPOSE: Get Iso Codes of Currency Exchange columns
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Function LoadIsoCodes() As Boolean
    Dim _str_sql As StringBuilder
    Dim _national_currency As String

    _str_sql = New StringBuilder()
    m_currency_exchange_iso_codes = New List(Of String)
    m_currency_exchange_iso_description = New List(Of String)
    _national_currency = CurrencyExchange.GetNationalCurrency()

    Try
      Using _db_trx As DB_TRX = New DB_TRX()

        _str_sql.AppendLine("    SELECT    DISTINCT cmd_iso_code                                      ")
        _str_sql.AppendLine("	         , ce_description                                               ")
        _str_sql.AppendLine("      FROM    CAGE_MOVEMENT_DETAILS                                      ")
        _str_sql.AppendLine("INNER JOIN    CURRENCY_EXCHANGE ON cmd_iso_code = ce_currency_iso_code   ")
        _str_sql.AppendLine("INNER JOIN    GENERAL_PARAMS ON ce_currency_iso_code = gp_key_value      ")
        _str_sql.AppendLine("	           AND gp_group_key = 'RegionalOptions'                         ")
        _str_sql.AppendLine("	           AND gp_subject_key = 'CurrencyISOCode'                       ")
        _str_sql.AppendLine("     WHERE    ce_type = 0                                                ")
        _str_sql.AppendLine("     UNION                                                               ")
        _str_sql.AppendLine("    SELECT    DISTINCT cmd_iso_code                                      ")
        _str_sql.AppendLine("	         , ce_description                                               ")
        _str_sql.AppendLine("      FROM    CAGE_MOVEMENT_DETAILS                                      ")
        _str_sql.AppendLine("INNER JOIN    CURRENCY_EXCHANGE ON cmd_iso_code = ce_currency_iso_code   ")
        _str_sql.AppendLine("     WHERE    ce_type = 0                                                ")

        Using _cmd As SqlCommand = New SqlCommand(_str_sql.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          Using _reader As SqlDataReader = _cmd.ExecuteReader()

            While _reader.Read()
              If Not _reader.IsDBNull(0) Then
                m_currency_exchange_iso_codes.Add(_reader.GetString(0))
                m_currency_exchange_iso_description.Add(_reader.GetString(1))
              End If
            End While
          End Using
        End Using
      End Using

    Catch ex As Exception
      Return False
    End Try

    Return True

  End Function ' LoadCurrenciesExchange

  ' PURPOSE: Formats amount
  '
  '  PARAMS:
  '     - INPUT:
  '           - Amount
  '           - IsDenomination
  '           - IsoCode
  '           - ShowZeros
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - FormatAmount
  '
  Public Function FormatAmount(ByVal Amount As Decimal, _
                                ByVal IsoCode As String) As String

    Dim _currency_exchange As WSI.Common.CurrencyExchangeProperties
    FormatAmount = ""

    If String.IsNullOrEmpty(IsoCode) Then
      FormatAmount = GUI_FormatNumber(Amount.ToString())
    Else
      If IsoCode = "-" Then
        FormatAmount = GUI_FormatNumber(Amount.ToString(), ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE, ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE, 0)
      Else
        If Not Amount = Nothing Or Amount = 0 Then
          _currency_exchange = WSI.Common.CurrencyExchangeProperties.GetProperties(IsoCode)

          If Not _currency_exchange Is Nothing Then
            If Amount < 0 Then
              FormatAmount = "-" & _currency_exchange.FormatCurrency(Amount * -1) ' s'hauria d'implementar a currency exchange la possiblitat de -$n
            Else
              FormatAmount = _currency_exchange.FormatCurrency(Amount)
            End If
          Else
            FormatAmount = Currency.Format(Amount, IsoCode)
          End If
        End If
      End If
    End If

    Return FormatAmount
  End Function ' FormatAmount

  ' PURPOSE: Gets session name
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Function GetSessionName() As String
    Dim _str_sql As StringBuilder

    _str_sql = New StringBuilder()
    GetSessionName = ""
    Try
      Using _db_trx As DB_TRX = New DB_TRX()

        _str_sql.AppendLine("    SELECT    CGS_SESSION_NAME                 ")
        _str_sql.AppendLine("      FROM    CAGE_SESSIONS                    ")
        _str_sql.AppendLine("     WHERE    CGS_CAGE_SESSION_ID = " & m_session_id)


        Using _cmd As SqlCommand = New SqlCommand(_str_sql.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          Using _reader As SqlDataReader = _cmd.ExecuteReader()

            _reader.Read()
            If Not _reader.IsDBNull(0) Then
              GetSessionName = _reader.GetString(0)
            End If
          End Using
        End Using
      End Using

      Return GetSessionName

    Catch ex As Exception
      Return ""
    End Try

    Return True

  End Function ' LoadCurrenciesExchange


#End Region

End Class
