'-------------------------------------------------------------------
' Copyright � 2015 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   CLASS_CAGE_SAFE_KEEPING
' DESCRIPTION:   
' AUTHOR:        Samuel Gonz�lez
' CREATION DATE: 09-APR-2015
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 09-APR-2015  SGB    Initial version. Backlog Item 709
' 07-JUL-2015  SGB    Fixed bug WIG-2537: Bloqued new user and not print correct balance.
'--------------------------------------------------------------------

Imports GUI_Controls
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports WSI.Common
Imports System.Data.SqlClient
Imports System.Text

Public Class CLASS_CAGE_SAFE_KEEPING
  Inherits CLASS_BASE

#Region "Structure"
  Public Class TYPE_CAGE_SAFE_KEEPING

    Public safe_keeping_id As Int64 = -1
    Public block_reason As Boolean
    Public owner_document_type As Int32
    Public owner_document_id As String
    Public owner_name As String
    Public owner_name1 As String
    Public owner_name2 As String
    Public owner_name3 As String
    Public owner_name4 As String
    Public balance As Decimal = 0.0
    Public max_balance As Decimal = -1

  End Class
#End Region 'Structure

#Region " Members "

  Protected m_type_cage_safe_keeping As New TYPE_CAGE_SAFE_KEEPING

#End Region ' Members

#Region " Properties "

  Public Property safe_keeping_id() As Int64
    Get
      Return m_type_cage_safe_keeping.safe_keeping_id
    End Get

    Set(ByVal Value As Int64)
      m_type_cage_safe_keeping.safe_keeping_id = Value
    End Set
  End Property

  Public Property block_reason() As Boolean
    Get
      Return m_type_cage_safe_keeping.block_reason
    End Get

    Set(ByVal Value As Boolean)
      m_type_cage_safe_keeping.block_reason = Value
    End Set
  End Property

  Public Property owner_document_type() As Int32
    Get
      Return m_type_cage_safe_keeping.owner_document_type
    End Get

    Set(ByVal Value As Int32)
      m_type_cage_safe_keeping.owner_document_type = Value
    End Set
  End Property

  Public Property owner_document_id() As String
    Get
      Return m_type_cage_safe_keeping.owner_document_id
    End Get

    Set(ByVal Value As String)
      m_type_cage_safe_keeping.owner_document_id = Value
    End Set
  End Property

  Public Property owner_name() As String
    Get
      Return m_type_cage_safe_keeping.owner_name
    End Get

    Set(ByVal Value As String)
      m_type_cage_safe_keeping.owner_name = Value
    End Set
  End Property

  Public Property owner_name1() As String
    Get
      Return m_type_cage_safe_keeping.owner_name1
    End Get

    Set(ByVal Value As String)
      m_type_cage_safe_keeping.owner_name1 = Value
    End Set
  End Property

  Public Property owner_name2() As String
    Get
      Return m_type_cage_safe_keeping.owner_name2
    End Get

    Set(ByVal Value As String)
      m_type_cage_safe_keeping.owner_name2 = Value
    End Set
  End Property

  Public Property owner_name3() As String
    Get
      Return m_type_cage_safe_keeping.owner_name3
    End Get

    Set(ByVal Value As String)
      m_type_cage_safe_keeping.owner_name3 = Value
    End Set
  End Property

  Public Property owner_name4() As String
    Get
      Return m_type_cage_safe_keeping.owner_name4
    End Get

    Set(ByVal Value As String)
      m_type_cage_safe_keeping.owner_name4 = Value
    End Set
  End Property

  Public Property balance() As Decimal
    Get
      Return m_type_cage_safe_keeping.balance
    End Get

    Set(ByVal Value As Decimal)
      m_type_cage_safe_keeping.balance = Value
    End Set
  End Property

  Public Property max_balance() As Decimal
    Get
      Return m_type_cage_safe_keeping.max_balance
    End Get

    Set(ByVal Value As Decimal)
      m_type_cage_safe_keeping.max_balance = Value
    End Set
  End Property

#End Region ' Properties

#Region " Overrides "

  '----------------------------------------------------------------------------
  ' PURPOSE: Returns the related auditor data for the current object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  Public Overrides Function AuditorData() As CLASS_AUDITOR_DATA

    Dim _auditor As CLASS_AUDITOR_DATA
    Dim _yes_text As String
    Dim _no_text As String

    _yes_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(278)
    _no_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)

    _auditor = New CLASS_AUDITOR_DATA(AUDIT_CODE_CAGE)

    Call _auditor.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(6240), "")     ' 6114 "Edici�n de cuenta de custodia"

    ' Name
    _auditor.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1718), IIf(String.IsNullOrEmpty(m_type_cage_safe_keeping.owner_name3), AUDIT_NONE_STRING, m_type_cage_safe_keeping.owner_name3))
    ' Middle name
    _auditor.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5087), IIf(String.IsNullOrEmpty(m_type_cage_safe_keeping.owner_name4), AUDIT_NONE_STRING, m_type_cage_safe_keeping.owner_name4))
    ' Surname 1
    _auditor.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1719), IIf(String.IsNullOrEmpty(m_type_cage_safe_keeping.owner_name1), AUDIT_NONE_STRING, m_type_cage_safe_keeping.owner_name1))
    ' Surname 2
    _auditor.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1720), IIf(String.IsNullOrEmpty(m_type_cage_safe_keeping.owner_name2), AUDIT_NONE_STRING, m_type_cage_safe_keeping.owner_name2))
    ' Doc. Type
    _auditor.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1721), IdentificationTypes.DocIdTypeString(m_type_cage_safe_keeping.owner_document_type))
    ' Document
    _auditor.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1722), IIf(String.IsNullOrEmpty(m_type_cage_safe_keeping.owner_document_id), AUDIT_NONE_STRING, m_type_cage_safe_keeping.owner_document_id))
    ' Bloqued
    _auditor.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6125), IIf(m_type_cage_safe_keeping.block_reason, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)))
    ' Max balance
    _auditor.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6127), IIf(m_type_cage_safe_keeping.max_balance < 0, AUDIT_NONE_STRING, GUI_FormatCurrency(m_type_cage_safe_keeping.max_balance)))
    ' Last operation

    Return _auditor

  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE: Deletes the given object from the database.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return ENUM_STATUS.STATUS_ERROR
  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE: Inserts the given object to the database.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _rc As Integer

    ' Insert pattern data
    _rc = CageSafeKeeping_DbInsert(m_type_cage_safe_keeping, SqlCtx)

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
        Return ENUM_STATUS.STATUS_DUPLICATE_KEY

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select
  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE: Reads from the database into the given object
  '
  ' PARAMS:
  '   - INPUT:
  '     - ObjectId: An object, it must be 'casted' to the desired KEY.
  '
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As ENUM_STATUS

    Dim rc As Integer

    Me.safe_keeping_id = ObjectId

    rc = CageSafeKeeping_DbRead(m_type_cage_safe_keeping, SqlCtx)

    Select Case rc
      Case ENUM_DB_RC.DB_RC_OK
        Return ENUM_STATUS.STATUS_OK

      Case ENUM_DB_RC.DB_RC_ERROR_NOT_FOUND
        Return CLASS_BASE.ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE: Updates the given object to the database.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _rc As Integer

    ' Update pattern data
    _rc = CageSafeKeeping_DbUpdate(m_type_cage_safe_keeping, SqlCtx)

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
        Return ENUM_STATUS.STATUS_DUPLICATE_KEY

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE: Returns a duplicate of the given object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim _clone As CLASS_CAGE_SAFE_KEEPING

    _clone = New CLASS_CAGE_SAFE_KEEPING()

    _clone.owner_name3 = Me.owner_name3
    _clone.owner_name4 = Me.owner_name4
    _clone.owner_name1 = Me.owner_name1
    _clone.owner_name2 = Me.owner_name2
    _clone.owner_name = Me.owner_name
    _clone.safe_keeping_id = Me.safe_keeping_id
    _clone.block_reason = Me.block_reason
    _clone.owner_document_type = Me.owner_document_type
    _clone.owner_document_id = Me.owner_document_id
    _clone.balance = Me.balance
    _clone.max_balance = Me.max_balance

    Return _clone

  End Function

#End Region ' Overrides

#Region "Private"

  '----------------------------------------------------------------------------
  ' PURPOSE: It calls the CageSafeKeeping_DbRead and checks if the database result is ok
  '                                              
  ' PARAMS:
  '   - INPUT: 
  '   - OUTPUT:
  '
  ' RETURNS:
  '
  ' NOTES:
  Private Function CageSafeKeeping_DbRead(ByVal Item As TYPE_CAGE_SAFE_KEEPING, ByVal Context As Integer) As Integer

    Dim _sql_query As StringBuilder
    Dim _data_table As DataTable
    Dim _sql_tx As System.Data.SqlClient.SqlTransaction = Nothing

    Try
      _sql_query = New StringBuilder

      _sql_query.AppendLine("SELECT   SKA_SAFE_KEEPING_ID     ")
      _sql_query.AppendLine("       , SKA_BLOCK_REASON        ")
      _sql_query.AppendLine("       , SKA_OWNER_DOCUMENT_TYPE ")
      _sql_query.AppendLine("       , SKA_OWNER_DOCUMENT_ID   ")
      _sql_query.AppendLine("       , SKA_OWNER_NAME          ")
      _sql_query.AppendLine("       , SKA_OWNER_NAME1         ")
      _sql_query.AppendLine("       , SKA_OWNER_NAME2         ")
      _sql_query.AppendLine("       , SKA_OWNER_NAME3         ")
      _sql_query.AppendLine("       , SKA_OWNER_NAME4         ")
      _sql_query.AppendLine("       , SKA_BALANCE             ")
      _sql_query.AppendLine("       , SKA_MAX_BALANCE         ")
      _sql_query.AppendLine("   FROM  SAFE_KEEPING_ACCOUNTS   ")
      _sql_query.AppendLine("  WHERE  SKA_SAFE_KEEPING_ID = " + Item.safe_keeping_id.ToString())

      _data_table = GUI_GetTableUsingSQL(_sql_query.ToString(), 50)
      If IsNothing(_data_table) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      If _data_table.Rows.Count() <> 1 Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      Item.safe_keeping_id = NullTrim(_data_table.Rows(0).Item("SKA_SAFE_KEEPING_ID"))
      Item.block_reason = _data_table.Rows(0).Item("SKA_BLOCK_REASON")
      Item.owner_document_type = _data_table.Rows(0).Item("SKA_OWNER_DOCUMENT_TYPE")
      Item.owner_document_id = _data_table.Rows(0).Item("SKA_OWNER_DOCUMENT_ID")
      Item.owner_name = _data_table.Rows(0).Item("SKA_OWNER_NAME")
      Item.owner_name1 = _data_table.Rows(0).Item("SKA_OWNER_NAME1")
      Item.balance = _data_table.Rows(0).Item("SKA_BALANCE")
      Item.owner_name3 = _data_table.Rows(0).Item("SKA_OWNER_NAME3")

      If _data_table.Rows(0).Item("SKA_OWNER_NAME2") IsNot DBNull.Value Then
        Item.owner_name2 = _data_table.Rows(0).Item("SKA_OWNER_NAME2")
      End If
      If _data_table.Rows(0).Item("SKA_OWNER_NAME4") IsNot DBNull.Value Then
        Item.owner_name4 = _data_table.Rows(0).Item("SKA_OWNER_NAME4")
      End If
      If _data_table.Rows(0).Item("SKA_MAX_BALANCE") IsNot DBNull.Value Then
        Item.max_balance = _data_table.Rows(0).Item("SKA_MAX_BALANCE")
      Else
        Item.max_balance = -1
      End If

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try
  End Function 'CageSafeKeeping_DbRead

  '----------------------------------------------------------------------------
  ' PURPOSE : Updates the safe keeping account object into the DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - Item
  '         - Context
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - CONFIGURATION_OK
  '     - CONFIGURATION_ERROR_DB
  '
  ' NOTES :

  Private Function CageSafeKeeping_DbUpdate(ByVal Item As TYPE_CAGE_SAFE_KEEPING, ByVal Context As Integer) As Integer
    Dim _sql_query As StringBuilder
    Dim _sql_tx As System.Data.SqlClient.SqlTransaction = Nothing
    Dim _sql_command As SqlCommand
    Dim _rc As Integer

    Try
      '   - Update the safe keeping account 
      If Not GUI_BeginSQLTransaction(_sql_tx) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      _sql_query = New StringBuilder

      _sql_query.AppendLine("UPDATE   SAFE_KEEPING_ACCOUNTS                          ")
      _sql_query.AppendLine("   SET   SKA_BLOCK_REASON         = @pBlockReason       ")
      _sql_query.AppendLine("       , SKA_OWNER_DOCUMENT_TYPE  = @pOwnerDocumentType ")
      _sql_query.AppendLine("       , SKA_OWNER_DOCUMENT_ID    = @pOwnerDocumentId   ")
      _sql_query.AppendLine("       , SKA_OWNER_NAME           = @pOwnerName         ")
      _sql_query.AppendLine("       , SKA_OWNER_NAME1          = @pOwnerName1        ")
      _sql_query.AppendLine("       , SKA_OWNER_NAME2          = @pOwnerName2        ")
      _sql_query.AppendLine("       , SKA_OWNER_NAME3          = @pOwnerName3        ")
      _sql_query.AppendLine("       , SKA_OWNER_NAME4          = @pOwnerName4        ")
      _sql_query.AppendLine("       , SKA_BALANCE              = @pBalance           ")
      _sql_query.AppendLine("       , SKA_MAX_BALANCE          = @pMaxBalance        ")
      _sql_query.AppendLine(" WHERE   SKA_SAFE_KEEPING_ID      = @pSafeKeepingId     ")

      _sql_command = New SqlCommand(_sql_query.ToString(), _sql_tx.Connection, _sql_tx)
      _sql_command.Parameters.Add("@pSafeKeepingId", SqlDbType.BigInt).Value = Item.safe_keeping_id
      _sql_command.Parameters.Add("@pBlockReason", SqlDbType.Bit).Value = Item.block_reason
      _sql_command.Parameters.Add("@pOwnerDocumentType", SqlDbType.Int).Value = Item.owner_document_type
      _sql_command.Parameters.Add("@pOwnerDocumentId", SqlDbType.NVarChar).Value = Item.owner_document_id
      _sql_command.Parameters.Add("@pOwnerName", SqlDbType.NVarChar).Value = CardData.FormatFullName(FULL_NAME_TYPE.ACCOUNT, _
                                                                                                      Item.owner_name3, _
                                                                                                      Item.owner_name4, _
                                                                                                      Item.owner_name1, _
                                                                                                      Item.owner_name2)
      _sql_command.Parameters.Add("@pOwnerName1", SqlDbType.NVarChar).Value = Item.owner_name1
      _sql_command.Parameters.Add("@pOwnerName2", SqlDbType.NVarChar).Value = Item.owner_name2
      _sql_command.Parameters.Add("@pOwnerName3", SqlDbType.NVarChar).Value = Item.owner_name3
      _sql_command.Parameters.Add("@pOwnerName4", SqlDbType.NVarChar).Value = Item.owner_name4
      _sql_command.Parameters.Add("@pBalance", SqlDbType.Money).Value = Item.balance
      _sql_command.Parameters.Add("@pMaxBalance", SqlDbType.Money).Value = IIf(Item.max_balance = -1, DBNull.Value, Item.max_balance)
      _rc = _sql_command.ExecuteNonQuery()

      If _rc <> 1 Then
        ' Rollback  
        GUI_EndSQLTransaction(_sql_tx, False)

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      ' Commit
      If Not GUI_EndSQLTransaction(_sql_tx, True) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

    Catch ex As Exception
      ' Rollback  
      GUI_EndSQLTransaction(_sql_tx, False)
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
  End Function ' CageSafeKeeping_DbUpdate

  '----------------------------------------------------------------------------
  ' PURPOSE : Adds a safe keeping account object into DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - Item
  '         - Context
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - CONFIGURATION_OK
  '     - CONFIGURATION_ERROR_DB
  '
  ' NOTES :

  Private Function CageSafeKeeping_DbInsert(ByVal Item As TYPE_CAGE_SAFE_KEEPING, ByVal Context As Integer) As Integer
    Dim _sql_query As StringBuilder
    Dim _sql_tx As System.Data.SqlClient.SqlTransaction = Nothing
    Dim _sql_command As SqlCommand
    Dim _rc As Integer
    Dim _output_param As SqlParameter
    Dim SafeKeepingId As Int64

    _rc = 0

    Try
      If Not GUI_BeginSQLTransaction(_sql_tx) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      _sql_query = New StringBuilder

      _sql_query.AppendLine("INSERT INTO SAFE_KEEPING_ACCOUNTS ( SKA_BLOCK_REASON        ")
      _sql_query.AppendLine("                                  , SKA_OWNER_DOCUMENT_TYPE ")
      _sql_query.AppendLine("                                  , SKA_OWNER_DOCUMENT_ID   ")
      _sql_query.AppendLine("                                  , SKA_OWNER_NAME          ")
      _sql_query.AppendLine("                                  , SKA_OWNER_NAME1         ")
      _sql_query.AppendLine("                                  , SKA_OWNER_NAME2         ")
      _sql_query.AppendLine("                                  , SKA_OWNER_NAME3         ")
      _sql_query.AppendLine("                                  , SKA_OWNER_NAME4         ")
      _sql_query.AppendLine("                                  , SKA_BALANCE             ")
      _sql_query.AppendLine("                                  , SKA_MAX_BALANCE         ")
      _sql_query.AppendLine("                                  , SKA_LAST_OPERATION      ")
      _sql_query.AppendLine("                                  , SKA_CREATED             ")
      _sql_query.AppendLine("                                  )                         ")
      _sql_query.AppendLine("                           VALUES ( @pBlockReason           ")
      _sql_query.AppendLine("                                  , @pOwnerDocumentType     ")
      _sql_query.AppendLine("                                  , @pOwnerDocumentId       ")
      _sql_query.AppendLine("                                  , @pOwnerName             ")
      _sql_query.AppendLine("                                  , @pOwnerName1            ")
      _sql_query.AppendLine("                                  , @pOwnerName2            ")
      _sql_query.AppendLine("                                  , @pOwnerName3            ")
      _sql_query.AppendLine("                                  , @pOwnerName4            ")
      _sql_query.AppendLine("                                  , @pBalance               ")
      _sql_query.AppendLine("                                  , @pMaxBalance            ")
      _sql_query.AppendLine("                                  , @pLastOperation         ")
      _sql_query.AppendLine("                                  , @pCreated               ")
      _sql_query.AppendLine("                                  )                         ")
      _sql_query.AppendLine(" SET @pSafeKeepingId = SCOPE_IDENTITY()                     ")

      _sql_command = New SqlCommand(_sql_query.ToString(), _sql_tx.Connection, _sql_tx)
      _sql_command.Parameters.Add("@pBlockReason", SqlDbType.Bit).Value = Item.block_reason
      _sql_command.Parameters.Add("@pOwnerDocumentType", SqlDbType.Int).Value = Item.owner_document_type
      _sql_command.Parameters.Add("@pOwnerDocumentId", SqlDbType.NVarChar).Value = Item.owner_document_id
      _sql_command.Parameters.Add("@pOwnerName", SqlDbType.NVarChar).Value = CardData.FormatFullName(FULL_NAME_TYPE.ACCOUNT, _
                                                                                                      Item.owner_name3, _
                                                                                                      Item.owner_name4, _
                                                                                                      Item.owner_name1, _
                                                                                                      Item.owner_name2)
      _sql_command.Parameters.Add("@pOwnerName1", SqlDbType.NVarChar).Value = Item.owner_name1
      If Item.owner_name2 IsNot DBNull.Value Then
        _sql_command.Parameters.Add("@pOwnerName2", SqlDbType.NVarChar).Value = Item.owner_name2
      End If
      _sql_command.Parameters.Add("@pOwnerName3", SqlDbType.NVarChar).Value = Item.owner_name3
      If Item.owner_name4 IsNot DBNull.Value Then
        _sql_command.Parameters.Add("@pOwnerName4", SqlDbType.NVarChar).Value = Item.owner_name4
      End If
      _sql_command.Parameters.Add("@pBalance", SqlDbType.Money).Value = Item.balance
      _sql_command.Parameters.Add("@pMaxBalance", SqlDbType.Money).Value = Item.max_balance
      _sql_command.Parameters.Add("@pLastOperation", SqlDbType.DateTime).Value = System.DBNull.Value
      _sql_command.Parameters.Add("@pCreated", SqlDbType.DateTime).Value = GUI_FormatDate(WGDB.Now(), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

      _output_param = New SqlParameter("@pSafeKeepingId", SqlDbType.BigInt)
      _output_param.Direction = ParameterDirection.Output

      _sql_command.Parameters.Add(_output_param)

      _rc = _sql_command.ExecuteNonQuery()

      If _output_param.Value IsNot DBNull.Value Then
        SafeKeepingId = _output_param.Value
      End If

      If _rc <> 1 Then
        ' Rollback  
        GUI_EndSQLTransaction(_sql_tx, False)

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      ' Commit
      If Not GUI_EndSQLTransaction(_sql_tx, True) Then
        ' Rollback  
        GUI_EndSQLTransaction(_sql_tx, False)

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

    Catch ex As Exception
      ' Rollback  
      GUI_EndSQLTransaction(_sql_tx, False)

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

  End Function ' CageSafeKeeping_DbInsert

#End Region ' Private

#Region " Publics "

  ' PURPOSE: Check if ID is already exists in DB
  ' 
  '  PARAMS:
  '      - INPUT:
  '
  '      - OUTPUT:
  '
  ' RETURNS:
  '    True Id Exists
  '    False Id does not exists
  ' 
  '   NOTES:
  Public Shared Function CheckIdAlreadyExists(ByVal DocumentId As String, ByVal IdType As ACCOUNT_HOLDER_ID_TYPE, ByVal KeepingId As Integer) As Boolean
    Dim _sql_query As StringBuilder
    Dim _sql_command As SqlCommand
    Dim _sql_tx As System.Data.SqlClient.SqlTransaction = Nothing
    Dim _data_table As DataTable

    If String.IsNullOrEmpty(DocumentId) Then
      Return False
    End If
    Try
      _sql_query = New StringBuilder

      _sql_query.AppendLine("SELECT   COUNT(*)                                  ")
      _sql_query.AppendLine("  FROM   SAFE_KEEPING_ACCOUNTS                     ")
      _sql_query.AppendLine(" WHERE   SKA_OWNER_DOCUMENT_ID = @pDocumentId      ")
      _sql_query.AppendLine("   AND   SKA_OWNER_DOCUMENT_TYPE = @pHolderIdType  ")
      _sql_query.AppendLine("   AND   SKA_SAFE_KEEPING_ID <> @pKeepingId        ")

      _sql_command = New SqlCommand(_sql_query.ToString())
      _sql_command.Parameters.Add("@pDocumentId", SqlDbType.NVarChar, 20).Value = DocumentId
      _sql_command.Parameters.Add("@pHolderIdType", SqlDbType.Int).Value = IdType
      _sql_command.Parameters.Add("@pKeepingId", SqlDbType.Int).Value = KeepingId
      _data_table = GUI_GetTableUsingCommand(_sql_command, 10)

      If Not IsNothing(_data_table) Then
        If _data_table.Rows.Count() > 0 Then
          If _data_table.Rows(0).Item(0) > 0 Then
            Return True
          End If
        End If
      End If

    Catch ex As Exception
      ' Rollback  
      GUI_EndSQLTransaction(_sql_tx, False)

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

    Return False
  End Function

  ' PURPOSE: Check if ID is already exists in DB
  ' 
  '  PARAMS:
  '      - INPUT:
  '
  '      - OUTPUT:
  '
  ' RETURNS:
  '    True Id Exists
  '    False Id does not exists
  ' 
  '   NOTES:
  Public Sub UpdateBalance()
    Dim _sql_query As StringBuilder
    Dim _sql_command As SqlCommand
    Dim _sql_tx As System.Data.SqlClient.SqlTransaction = Nothing
    Dim _data_table As DataTable

    Try
      _sql_query = New StringBuilder

      _sql_query.AppendLine("SELECT   SKA_BALANCE                       ")
      _sql_query.AppendLine("  FROM   SAFE_KEEPING_ACCOUNTS             ")
      _sql_query.AppendLine(" WHERE   SKA_SAFE_KEEPING_ID = @pKeepingId ")

      _sql_command = New SqlCommand(_sql_query.ToString())
      _sql_command.Parameters.Add("@pKeepingId", SqlDbType.Int).Value = Me.safe_keeping_id
      _data_table = GUI_GetTableUsingCommand(_sql_command, 10)

      If _data_table.Rows.Count > 0 Then
        Me.balance = _data_table.Rows(0).Item(0)
      End If


    Catch _ex As Exception
      ' Rollback  
      GUI_EndSQLTransaction(_sql_tx, False)
      Log.Exception(_ex)

    End Try
  End Sub


#End Region ' Publics

End Class
