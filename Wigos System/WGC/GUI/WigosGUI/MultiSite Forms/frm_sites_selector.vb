'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_site_selector.vb
' DESCRIPTION:   Allow user select with site connect from list
' AUTHOR:        Artur Nebot Garrig�
' CREATION DATE: 10-JUN-2013
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 10-JUN-2013  ANG    Initial version
' -------------------------------------------------------------------
Imports System.Data.SqlClient
Imports WSI.Common
Imports GUI_Controls
Imports GUI_CommonMisc

Public Class frm_sites_selector
  Inherits GUI_Controls.frm_base

#Region "Private Properties"
  Protected m_selected_site_config As String
  Protected m_user_cancelled As Boolean
  Protected m_site_data As DataTable
#End Region

#Region "Public Properties"

  Public ReadOnly Property UserCancelled() As Boolean
    Get
      Return m_user_cancelled
    End Get
  End Property

  Public ReadOnly Property SelectedSiteConfig() As String
    Get
      Return m_selected_site_config
    End Get
  End Property

#End Region

#Region "Class constructor"

  ' PURPOSE: Form class constructor
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  ' NOTE : Constructor is PRIVATE, should call ShowSelector to instance form
  Private Sub New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

  End Sub ' New()


  ' PURPOSE: Handle all Selected Site form lifecycle
  '  PARAMS:
  '     - INPUT:
  '           - Wigos Language
  '           - Sites data table with sites name and config.
  '     - OUTPUT:
  '          - If user selected Multisite 
  '          - Site Config settings of user selected site.
  ' RETURNS:
  '       FALSE  If user cancell site selection.
  Public Shared Function ShowSelector(ByVal InitLanguage As ENUM_NLS_LANGUAGE, ByRef Sites As DataTable, ByRef IsMultiSiteSelected As Boolean, ByRef SelectedConfig As DbConfig) As Boolean

    Dim _user_cancelled As Boolean

    Using _form As frm_sites_selector = FormSiteSelectorBuilder(InitLanguage, Sites)

      IsMultiSiteSelected = String.IsNullOrEmpty(_form.SelectedSiteConfig)
      _user_cancelled = _form.UserCancelled

      If Not IsMultiSiteSelected Then
        SelectedConfig = DbConfig.CreateFromXml(_form.SelectedSiteConfig)
      End If

    End Using

    Return _user_cancelled

  End Function
  ' PURPOSE: Implement static constructor that 
  '          returns an configured instance of form.
  '  PARAMS:
  '     - INPUT:
  '           - Wigos Language
  '           - Sites data table with sites name and config.
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Shared Function FormSiteSelectorBuilder(ByVal InitLanguage As ENUM_NLS_LANGUAGE, ByRef Sites As DataTable) As frm_sites_selector

    Dim _form As New frm_sites_selector()

    '' Try to initialize language...
    If Not NLS_SetLanguage(InitLanguage) Then
      ' Something went wrong and the application can not be started
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_GENERIC_ERROR, _
                            public_globals.GUI_ModuleName(ENUM_GUI.WIGOS_GUI), _
                            "Main", _
                            "", _
                            "NLS_SetLanguage")

      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(117), _
                              ENUM_MB_TYPE.MB_TYPE_ERROR, _
                              ENUM_MB_BTN.MB_BTN_OK, , _
                              String.Empty, _
                              String.Empty)


      '' Trick to close app 
      _form.m_user_cancelled = True
      Return _form

    End If

    _form.m_site_data = Sites

    _form.ShowDialog()
    Return _form

  End Function ' ShowSelector()

#End Region

#Region "Overrides"

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2053) ' Site selector

    Me.lblSiteId.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1773)       '' Site
    Me.btnConnect.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2052)      '' Connect
    Me.btn_rdp.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2057)         '' Remote Desktop
    Me.btnExit.Text = GLB_NLS_GUI_CONTROLS.GetString(10)                  '' Exit

    '' Add MultiSite Row
    m_site_data.Rows.Add(New Object() {0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1791), String.Empty})


    '' Fill data into list box control
    m_site_data.Columns.Add("ST_FULL_SITE_NAME" _
                           , Type.GetType("System.String") _
                           , "IIF(ST_SITE_ID=0,ST_NAME,CONVERT(ST_SITE_ID,'System.String')+' - '+ST_NAME)")

    m_site_data.DefaultView.Sort = "ST_SITE_ID asc"

    Me.lst_sites.DataSource = m_site_data.DefaultView
    Me.lst_sites.DisplayMember = "ST_FULL_SITE_NAME"

  End Sub ' GUI_InitControls

  ' PURPOSE: Given a type, the function returns if it needs to be audited
  '
  '  PARAMS:
  '     - INPUT:
  '         - AuditType: AUDIT_FLAGS that wants to be audited
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Function GUI_HasToBeAudited(ByVal AuditType As AUDIT_FLAGS) As Boolean

    Return False

  End Function
#End Region

#Region "Private functions"
  ' PURPOSE: Filter SitesListBox
  '  PARAMS:
  '     - INPUT:
  '         FilterValue
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub FilterSitesListBox(ByVal FilterValue As String)

    Dim _filter_format As String
    Dim _filter_view As DataView

    _filter_format = String.Format("ST_FULL_SITE_NAME like '%{0}%'", FilterValue)

    _filter_view = m_site_data.DefaultView
    _filter_view.RowFilter = _filter_format

    Me.lst_sites.DataSource = _filter_view


  End Sub ' FilterSitesListBox

#End Region

#Region "Events"

  ' PURPOSE:On siteId text field value changed..
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub txtSiteId_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSiteId.TextChanged

    Dim _target_txtbox As TextBox
    _target_txtbox = CType(sender, TextBox)

    Call txtSiteId_Validating(sender, Nothing)

    Call FilterSitesListBox(_target_txtbox.Text)

    '' Not stop tab in listbox if is empty
    lst_sites.TabStop = lst_sites.Items.Count > 1

    '' Disable button if no items in list
    btnConnect.Enabled = Not lst_sites.Items.Count = 0
    btn_rdp.Enabled = Not lst_sites.Items.Count = 0

    '' Call to selected value changed to check if site
    '' has connection info...
    lst_sites_SelectedValueChanged(lst_sites, e)

  End Sub ' txtSiteId_TextChanged

  ' PURPOSE: On click to Connect button
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub btnConnect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConnect.Click

    Dim _selected_row As DataRow

    If Me.lst_sites.SelectedItem Is Nothing Then

      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2054), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK)

      Return

    End If

    _selected_row = CType(Me.lst_sites.SelectedItem, DataRowView).Row

    If Not _selected_row.IsNull("ST_CONNECTION_STRING") Then
      m_selected_site_config = _selected_row("ST_CONNECTION_STRING")
    Else
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2055), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK) ' No conection info

      m_selected_site_config = String.Empty
      Return
    End If

    Me.Hide()


  End Sub 'btnConnect_Click

  ' PURPOSE: On double click to list, enter direct to site
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub lst_sites_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lst_sites.DoubleClick

    Call btnConnect_Click(sender, e)

  End Sub 'lst_sites_DoubleClick

  ' PURPOSE: On press enter into list enter direct to site
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub lst_sites_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles lst_sites.KeyDown

    If e.KeyCode = Keys.Enter Then
      Call btnConnect_Click(sender, e)
    End If

  End Sub 'lst_sites_KeyDown

  ' PURPOSE: Handles click on btnExit
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click

    m_user_cancelled = True
    Me.Hide()

  End Sub 'btnExit_Click

  ' PURPOSE: On form clouse
  '           Separate when user close form to exit 
  '           either than program close form...
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub frm_sites_selector_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
    If e.CloseReason = CloseReason.UserClosing Then

      m_user_cancelled = True
      Me.Close()

    End If
  End Sub 'frm_sites_selector_FormClosed

  ' PURPOSE: On key down event in form.
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub frm_sites_selector_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown

    If e.KeyCode = Keys.Escape Then

      Call btnExit_Click(sender, e)
    End If
  End Sub 'frm_sites_selector_KeyDown


  ' PURPOSE: lst_sites_SelectedValueChanged
  '          Disallow connect button if no site information.
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub lst_sites_SelectedValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lst_sites.SelectedValueChanged

    Dim _has_config_info As Boolean
    Dim _has_rdp_info As Boolean
    Dim _lst_sites As ListBox
    _lst_sites = CType(sender, ListBox)


    If Not _lst_sites.SelectedItem Is Nothing Then

      _has_config_info = Not CType(_lst_sites.SelectedItem, DataRowView).Row.IsNull("ST_CONNECTION_STRING")
      _has_rdp_info = Not CType(_lst_sites.SelectedItem, DataRowView).Row.IsNull("ST_REMOTE_DESKTOP")

      Me.btnConnect.Enabled = _has_config_info
      Me.btn_rdp.Enabled = _has_rdp_info
    End If

  End Sub

  ' PURPOSE: lst_sites_SelectedValueChanged
  '          Disallow connect button if no site information.
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub btn_rdp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_rdp.Click

    Dim _site_row As DataRow

    If (Me.lst_sites.SelectedItem Is Nothing) Then
      Return
    End If

    _site_row = CType(lst_sites.SelectedItem, DataRowView).Row

    If Not _site_row.Table.Columns.Contains("ST_REMOTE_DESKTOP") _
        OrElse _site_row.IsNull("ST_REMOTE_DESKTOP") Then
      '"No Remote Desktop connection config for this site"
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2056), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK)

      Return
    End If


    'Process.Start("Cmd.exe", String.Format("/C mstsc.exe /v:{0}", _site_row("ST_REMOTE_DESKTOP")))

    Shell(String.Format("mstsc /v:{0}", _site_row("ST_REMOTE_DESKTOP")))

  End Sub

  ' PURPOSE: Validate TextField User Input
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub txtSiteId_Validating(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtSiteId.Validating

    Dim _target_textbox As TextBox
    _target_textbox = CType(sender, TextBox)

    Dim _accepted_chars As New List(Of Char)(New Char() {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "�", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"})

    For Each _char As Char In _target_textbox.Text.ToCharArray
      If Not _accepted_chars.Contains(Char.ToLower(_char)) Then
        _target_textbox.Text = _target_textbox.Text.Replace(_char, String.Empty)
      End If
    Next

  End Sub

#End Region


End Class