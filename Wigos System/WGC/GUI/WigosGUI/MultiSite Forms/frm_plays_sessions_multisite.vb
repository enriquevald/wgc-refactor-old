'-------------------------------------------------------------------
' Copyright � 2007 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_plays_sessions_multisite
' DESCRIPTION:   This screen allows to view the sessions between:
'                           - two dates 
'                           - for all terminals or one terminal
'                           - for all status or one status
' AUTHOR:        Merc� Torrelles Minguella
' CREATION DATE: 13-APR-2007
' 
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 25-APR-2007  MTM    Initial version
' 15-APR-2009  MBF    Close play session
' 03-AUG-2011  RCI    Group by provider or account and more
' 31-JAN-2011  AJQ & RCI    Computed duration at DB level
' 17-JUL-2012  MPO    Added string -> new type of handpays
' 10-OCT-2012  RRB    Added index in the query (IX_ps_started_terminal_id)
' 12-FEB-2013  JMM    uc_account_sel.ValidateFormat added on FilterCheck
' 28-MAR-2013  ICS    Added new columns to grid (redeemable, non-redeemable, plays)
' 17-APR-2013  SMN    Fixed Bug #722: The total of the initial column doesn't contain the recharges
' 25-APR-2013  RBG    #746 It makes no sense to work in this form with multiselection
' 02-MAY-2013  MPO    Duplicate form to Multisite
' 03-MAY-2013  ANG    Add Site column in grid and Site GUI Filter
' 21-MAY-2013  AMF    Fixed Bug #796: Remove Terminals Filter from the impresion
' 12-JUN-2013  AMF    Fixed Bug #841: Session open don't show duration
' 06-MAY-2014  JBP    Added Vip client filter at reports.
' 22-JUL-2014  AMF    Tito Columns.
' 29-JUL-2014  JBC    Fixed Bug WIG-1124: Column name changed TOTAL_CASH_IN/TOTAL_CASH_OUT
' 09-OCT-2014  RCI    Fixed Bug WIG-1457: PlaySessions with virtual accounts return NULL Account values that must be controlled
' 04-NOV-2014  FJC    Play Sessions: 'Progressive Provision' and 'Progressive Reserve' should appear with a new NLS and another color.
'                     Also, if we check 'solo sesiones descuadradas', 'Progressive Provision' and 'Progressive Reserve' 
'                      should not appear on the report.
' 04-NOV-2014  FJC    Fixed Bug WIG-1631: When check 'show only gap sessions', appear gap sessions and not gap sessions also."
' 02-DIC-2014  DCS    Modify date filter, adding end date
' 30-JAN-2015  FJC    Sum columns ps_aux_ft_re_cash_in and ps_aux_ft_nr_cash_in to column Cash-IN in the Grid (Redeemable and Not Redeemable) 
' 07-MAY-2015  MPO    WIG-2292
' 25-MAY-2015  ANM    Add multicurrency control
' 17-JUN-2015  FOS    Fixed Bug WIG 2458 
' 20-SEP-2016  XGJ    Prepare accounts for PariPlay 
' 19-JAN-2017  RAB    Bug 15724: MultiSite GUI - Play sessions: values rounded halls of the same currency as the MultiSite GUI room.
' 03-MAY-2017  AMF    Bug 27117:MS GUI: Pantalla de sesiones de juego no muestra resultados
' 26-SEP-2017  ETP    Fixed Bug 29921:[WIGOS-4564] Multisite: wrong site number in grids
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports WSI.Common
Imports System.Text

Public Class frm_plays_sessions_multisite
  Inherits frm_base_sel_edit

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents gb_status As System.Windows.Forms.GroupBox
  Friend WithEvents opt_several_status As System.Windows.Forms.RadioButton
  Friend WithEvents opt_all_status As System.Windows.Forms.RadioButton
  Friend WithEvents cmb_status As GUI_Controls.uc_combo
  Friend WithEvents gb_options As System.Windows.Forms.GroupBox
  Friend WithEvents chk_grouped As System.Windows.Forms.CheckBox
  Friend WithEvents opt_provider As System.Windows.Forms.RadioButton
  Friend WithEvents opt_account As System.Windows.Forms.RadioButton
  Friend WithEvents chk_subtotal As System.Windows.Forms.CheckBox
  Friend WithEvents chk_show_sessions As System.Windows.Forms.CheckBox
  Friend WithEvents chk_only_gap_sessions As System.Windows.Forms.CheckBox
  Friend WithEvents lbl_handpay As System.Windows.Forms.Label
  Friend WithEvents lbl_recharges As System.Windows.Forms.Label
  Friend WithEvents uc_site_select As GUI_Controls.uc_sites_sel
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents rb_finish_date As System.Windows.Forms.RadioButton
  Friend WithEvents rb_ini_date As System.Windows.Forms.RadioButton
  Friend WithEvents rb_historic As System.Windows.Forms.RadioButton
  Friend WithEvents rb_open_sessions As System.Windows.Forms.RadioButton
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents uc_multi_currency_select As GUI_Controls.uc_multi_currency_sel
  Friend WithEvents uc_account_sel1 As GUI_Controls.uc_account_sel

  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_plays_sessions_multisite))
    Me.gb_status = New System.Windows.Forms.GroupBox()
    Me.opt_several_status = New System.Windows.Forms.RadioButton()
    Me.opt_all_status = New System.Windows.Forms.RadioButton()
    Me.cmb_status = New GUI_Controls.uc_combo()
    Me.uc_account_sel1 = New GUI_Controls.uc_account_sel()
    Me.gb_options = New System.Windows.Forms.GroupBox()
    Me.chk_only_gap_sessions = New System.Windows.Forms.CheckBox()
    Me.chk_show_sessions = New System.Windows.Forms.CheckBox()
    Me.chk_subtotal = New System.Windows.Forms.CheckBox()
    Me.opt_account = New System.Windows.Forms.RadioButton()
    Me.opt_provider = New System.Windows.Forms.RadioButton()
    Me.chk_grouped = New System.Windows.Forms.CheckBox()
    Me.lbl_handpay = New System.Windows.Forms.Label()
    Me.lbl_recharges = New System.Windows.Forms.Label()
    Me.uc_site_select = New GUI_Controls.uc_sites_sel()
    Me.gb_date = New System.Windows.Forms.GroupBox()
    Me.rb_finish_date = New System.Windows.Forms.RadioButton()
    Me.rb_ini_date = New System.Windows.Forms.RadioButton()
    Me.rb_historic = New System.Windows.Forms.RadioButton()
    Me.rb_open_sessions = New System.Windows.Forms.RadioButton()
    Me.dtp_to = New GUI_Controls.uc_date_picker()
    Me.dtp_from = New GUI_Controls.uc_date_picker()
    Me.uc_multi_currency_select = New GUI_Controls.uc_multi_currency_sel()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_status.SuspendLayout()
    Me.gb_options.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.uc_multi_currency_select)
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Controls.Add(Me.uc_site_select)
    Me.panel_filter.Controls.Add(Me.lbl_recharges)
    Me.panel_filter.Controls.Add(Me.lbl_handpay)
    Me.panel_filter.Controls.Add(Me.gb_options)
    Me.panel_filter.Controls.Add(Me.uc_account_sel1)
    Me.panel_filter.Controls.Add(Me.gb_status)
    Me.panel_filter.Size = New System.Drawing.Size(1207, 231)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_status, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_account_sel1, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_options, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_handpay, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_recharges, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_site_select, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_multi_currency_select, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 235)
    Me.panel_data.Size = New System.Drawing.Size(1207, 474)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1201, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1201, 4)
    '
    'gb_status
    '
    Me.gb_status.Controls.Add(Me.opt_several_status)
    Me.gb_status.Controls.Add(Me.opt_all_status)
    Me.gb_status.Controls.Add(Me.cmb_status)
    Me.gb_status.Location = New System.Drawing.Point(557, 137)
    Me.gb_status.Name = "gb_status"
    Me.gb_status.Size = New System.Drawing.Size(304, 72)
    Me.gb_status.TabIndex = 3
    Me.gb_status.TabStop = False
    Me.gb_status.Text = "xStatus"
    '
    'opt_several_status
    '
    Me.opt_several_status.Location = New System.Drawing.Point(24, 14)
    Me.opt_several_status.Name = "opt_several_status"
    Me.opt_several_status.Size = New System.Drawing.Size(72, 24)
    Me.opt_several_status.TabIndex = 0
    Me.opt_several_status.Text = "xOne"
    '
    'opt_all_status
    '
    Me.opt_all_status.Location = New System.Drawing.Point(24, 40)
    Me.opt_all_status.Name = "opt_all_status"
    Me.opt_all_status.Size = New System.Drawing.Size(64, 24)
    Me.opt_all_status.TabIndex = 2
    Me.opt_all_status.Text = "xAll"
    '
    'cmb_status
    '
    Me.cmb_status.AllowUnlistedValues = False
    Me.cmb_status.AutoCompleteMode = False
    Me.cmb_status.IsReadOnly = False
    Me.cmb_status.Location = New System.Drawing.Point(107, 13)
    Me.cmb_status.Name = "cmb_status"
    Me.cmb_status.SelectedIndex = -1
    Me.cmb_status.Size = New System.Drawing.Size(184, 24)
    Me.cmb_status.SufixText = "Sufix Text"
    Me.cmb_status.SufixTextVisible = True
    Me.cmb_status.TabIndex = 1
    Me.cmb_status.TextCombo = Nothing
    Me.cmb_status.TextVisible = False
    Me.cmb_status.TextWidth = 0
    '
    'uc_account_sel1
    '
    Me.uc_account_sel1.Account = ""
    Me.uc_account_sel1.AccountText = ""
    Me.uc_account_sel1.Anon = False
    Me.uc_account_sel1.AutoSize = True
    Me.uc_account_sel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.uc_account_sel1.BirthDate = New Date(CType(0, Long))
    Me.uc_account_sel1.DisabledHolder = False
    Me.uc_account_sel1.Font = New System.Drawing.Font("Verdana", 8.25!)
    Me.uc_account_sel1.Holder = ""
    Me.uc_account_sel1.Location = New System.Drawing.Point(554, 3)
    Me.uc_account_sel1.MassiveSearchNumbers = ""
    Me.uc_account_sel1.MassiveSearchNumbersToEdit = ""
    Me.uc_account_sel1.MassiveSearchType = 0
    Me.uc_account_sel1.Name = "uc_account_sel1"
    Me.uc_account_sel1.SearchTrackDataAsInternal = True
    Me.uc_account_sel1.ShowMassiveSearch = False
    Me.uc_account_sel1.ShowVipClients = True
    Me.uc_account_sel1.Size = New System.Drawing.Size(307, 134)
    Me.uc_account_sel1.TabIndex = 2
    Me.uc_account_sel1.Telephone = ""
    Me.uc_account_sel1.TrackData = ""
    Me.uc_account_sel1.Vip = False
    Me.uc_account_sel1.WeddingDate = New Date(CType(0, Long))
    '
    'gb_options
    '
    Me.gb_options.Controls.Add(Me.chk_only_gap_sessions)
    Me.gb_options.Controls.Add(Me.chk_show_sessions)
    Me.gb_options.Controls.Add(Me.chk_subtotal)
    Me.gb_options.Controls.Add(Me.opt_account)
    Me.gb_options.Controls.Add(Me.opt_provider)
    Me.gb_options.Controls.Add(Me.chk_grouped)
    Me.gb_options.Location = New System.Drawing.Point(869, 6)
    Me.gb_options.Name = "gb_options"
    Me.gb_options.Size = New System.Drawing.Size(195, 166)
    Me.gb_options.TabIndex = 4
    Me.gb_options.TabStop = False
    Me.gb_options.Text = "xOptions"
    '
    'chk_only_gap_sessions
    '
    Me.chk_only_gap_sessions.AutoSize = True
    Me.chk_only_gap_sessions.Location = New System.Drawing.Point(6, 20)
    Me.chk_only_gap_sessions.Name = "chk_only_gap_sessions"
    Me.chk_only_gap_sessions.Size = New System.Drawing.Size(140, 17)
    Me.chk_only_gap_sessions.TabIndex = 0
    Me.chk_only_gap_sessions.Text = "xOnly Gap Sessions"
    Me.chk_only_gap_sessions.UseVisualStyleBackColor = True
    '
    'chk_show_sessions
    '
    Me.chk_show_sessions.AutoSize = True
    Me.chk_show_sessions.Location = New System.Drawing.Point(20, 138)
    Me.chk_show_sessions.Name = "chk_show_sessions"
    Me.chk_show_sessions.Size = New System.Drawing.Size(116, 17)
    Me.chk_show_sessions.TabIndex = 5
    Me.chk_show_sessions.Text = "xShow sessions"
    '
    'chk_subtotal
    '
    Me.chk_subtotal.AutoSize = True
    Me.chk_subtotal.Location = New System.Drawing.Point(20, 115)
    Me.chk_subtotal.Name = "chk_subtotal"
    Me.chk_subtotal.Size = New System.Drawing.Size(80, 17)
    Me.chk_subtotal.TabIndex = 4
    Me.chk_subtotal.Text = "xSubtotal"
    '
    'opt_account
    '
    Me.opt_account.AutoSize = True
    Me.opt_account.Location = New System.Drawing.Point(20, 66)
    Me.opt_account.Name = "opt_account"
    Me.opt_account.Size = New System.Drawing.Size(77, 17)
    Me.opt_account.TabIndex = 2
    Me.opt_account.Text = "xAccount"
    '
    'opt_provider
    '
    Me.opt_provider.AutoSize = True
    Me.opt_provider.Location = New System.Drawing.Point(20, 89)
    Me.opt_provider.Name = "opt_provider"
    Me.opt_provider.Size = New System.Drawing.Size(80, 17)
    Me.opt_provider.TabIndex = 3
    Me.opt_provider.Text = "xProvider"
    '
    'chk_grouped
    '
    Me.chk_grouped.AutoSize = True
    Me.chk_grouped.Location = New System.Drawing.Point(6, 43)
    Me.chk_grouped.Name = "chk_grouped"
    Me.chk_grouped.Size = New System.Drawing.Size(82, 17)
    Me.chk_grouped.TabIndex = 1
    Me.chk_grouped.Text = "xGrouped"
    '
    'lbl_handpay
    '
    Me.lbl_handpay.AutoSize = True
    Me.lbl_handpay.ForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_handpay.Location = New System.Drawing.Point(11, 178)
    Me.lbl_handpay.Name = "lbl_handpay"
    Me.lbl_handpay.Size = New System.Drawing.Size(90, 13)
    Me.lbl_handpay.TabIndex = 11
    Me.lbl_handpay.Text = "xHP.: Handpay"
    '
    'lbl_recharges
    '
    Me.lbl_recharges.AutoSize = True
    Me.lbl_recharges.ForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_recharges.Location = New System.Drawing.Point(11, 209)
    Me.lbl_recharges.Name = "lbl_recharges"
    Me.lbl_recharges.Size = New System.Drawing.Size(81, 13)
    Me.lbl_recharges.TabIndex = 12
    Me.lbl_recharges.Text = "xRecharges*"
    '
    'uc_site_select
    '
    Me.uc_site_select.Location = New System.Drawing.Point(6, 3)
    Me.uc_site_select.MultiSelect = True
    Me.uc_site_select.Name = "uc_site_select"
    Me.uc_site_select.ShowIsoCode = False
    Me.uc_site_select.ShowMultisiteRow = True
    Me.uc_site_select.Size = New System.Drawing.Size(286, 168)
    Me.uc_site_select.TabIndex = 0
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.rb_finish_date)
    Me.gb_date.Controls.Add(Me.rb_ini_date)
    Me.gb_date.Controls.Add(Me.rb_historic)
    Me.gb_date.Controls.Add(Me.rb_open_sessions)
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Controls.Add(Me.dtp_from)
    Me.gb_date.Location = New System.Drawing.Point(297, 6)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(251, 161)
    Me.gb_date.TabIndex = 1
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xDate"
    '
    'rb_finish_date
    '
    Me.rb_finish_date.AutoCheck = False
    Me.rb_finish_date.Location = New System.Drawing.Point(58, 76)
    Me.rb_finish_date.Name = "rb_finish_date"
    Me.rb_finish_date.Size = New System.Drawing.Size(144, 24)
    Me.rb_finish_date.TabIndex = 3
    Me.rb_finish_date.Text = "xFinishDate"
    '
    'rb_ini_date
    '
    Me.rb_ini_date.AutoCheck = False
    Me.rb_ini_date.Location = New System.Drawing.Point(58, 55)
    Me.rb_ini_date.Name = "rb_ini_date"
    Me.rb_ini_date.Size = New System.Drawing.Size(144, 24)
    Me.rb_ini_date.TabIndex = 2
    Me.rb_ini_date.Text = "xIniDate"
    '
    'rb_historic
    '
    Me.rb_historic.AutoCheck = False
    Me.rb_historic.Location = New System.Drawing.Point(8, 35)
    Me.rb_historic.Name = "rb_historic"
    Me.rb_historic.Size = New System.Drawing.Size(144, 24)
    Me.rb_historic.TabIndex = 1
    Me.rb_historic.Text = "xHistoric"
    '
    'rb_open_sessions
    '
    Me.rb_open_sessions.AutoCheck = False
    Me.rb_open_sessions.Location = New System.Drawing.Point(8, 14)
    Me.rb_open_sessions.Name = "rb_open_sessions"
    Me.rb_open_sessions.Size = New System.Drawing.Size(144, 24)
    Me.rb_open_sessions.TabIndex = 0
    Me.rb_open_sessions.Text = "xOpenSessions"
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(6, 128)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(240, 25)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 5
    Me.dtp_to.TextWidth = 50
    Me.dtp_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(6, 100)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = True
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(240, 25)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 4
    Me.dtp_from.TextWidth = 50
    Me.dtp_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'uc_multi_currency_select
    '
    Me.uc_multi_currency_select.BackColor = System.Drawing.SystemColors.Control
    Me.uc_multi_currency_select.CurrenciesTable = Nothing
    Me.uc_multi_currency_select.DateFrom = New Date(CType(0, Long))
    Me.uc_multi_currency_select.DateTo = New Date(CType(0, Long))
    Me.uc_multi_currency_select.GetAverageCurrencyList = Nothing
    Me.uc_multi_currency_select.GetSitesChecked = Nothing
    Me.uc_multi_currency_select.HasSitesWithoutCurrency = False
    Me.uc_multi_currency_select.IsGridApplyChanges = False
    Me.uc_multi_currency_select.IsoCodeSelected = Nothing
    Me.uc_multi_currency_select.IsSitesChecked = False
    Me.uc_multi_currency_select.Location = New System.Drawing.Point(297, 171)
    Me.uc_multi_currency_select.MultiSelect = False
    Me.uc_multi_currency_select.MultiSiteCurrency = Nothing
    Me.uc_multi_currency_select.Name = "uc_multi_currency_select"
    Me.uc_multi_currency_select.NumRowsChecked = 0
    Me.uc_multi_currency_select.ShowIsoCode = True
    Me.uc_multi_currency_select.ShowMultisiteRow = False
    Me.uc_multi_currency_select.Size = New System.Drawing.Size(251, 47)
    Me.uc_multi_currency_select.TabIndex = 5
    Me.uc_multi_currency_select.Visible = False
    '
    'frm_plays_sessions_multisite
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(1215, 713)
    Me.Name = "frm_plays_sessions_multisite"
    Me.Text = "frm_plays_sessions_multisite"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_status.ResumeLayout(False)
    Me.gb_options.ResumeLayout(False)
    Me.gb_options.PerformLayout()
    Me.gb_date.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Constants "

  Private Const SQL_COLUMN_SESSION_ID As Integer = 0
  Private Const SQL_COLUMN_STARTED As Integer = 1
  Private Const SQL_COLUMN_FINISHED As Integer = 2
  Private Const SQL_COLUMN_TERMINAL_NAME As Integer = 3
  Private Const SQL_COLUMN_TOTAL_CASH_IN As Integer = 4
  Private Const SQL_COLUMN_PLAYED_AMOUNT As Integer = 5
  Private Const SQL_COLUMN_WON_AMOUNT As Integer = 6
  Private Const SQL_COLUMN_PLAYED_COUNT As Integer = 7
  Private Const SQL_COLUMN_WON_COUNT As Integer = 8
  Private Const SQL_COLUMN_CASH_IN_PLAY_SESSION As Integer = 9
  Private Const SQL_COLUMN_TOTAL_CASH_OUT As Integer = 10
  Private Const SQL_COLUMN_STATUS As Integer = 11
  Private Const SQL_COLUMN_ACCOUNT_ID As Integer = 12
  Private Const SQL_COLUMN_ACCOUNT_TYPE As Integer = 13
  Private Const SQL_COLUMN_CARD_ID As Integer = 14
  Private Const SQL_COLUMN_PROMOTIONAL As Integer = 15
  Private Const SQL_COLUMN_HOLDER_NAME As Integer = 16
  Private Const SQL_COLUMN_PROVIDER_NAME As Integer = 17
  Private Const SQL_COLUMN_DURATION As Integer = 18
  Private Const SQL_COLUMN_TERMINAL_TYPE As Integer = 19
  Private Const SQL_COLUMN_TYPE As Integer = 20
  Private Const SQL_COLUMN_RE_CASH_IN As Integer = 21
  Private Const SQL_COLUMN_RE_PLAYED_AMOUNT As Integer = 22
  Private Const SQL_COLUMN_RE_WON_AMOUNT As Integer = 23
  Private Const SQL_COLUMN_RE_CASH_OUT As Integer = 24
  Private Const SQL_COLUMN_NRE_CASH_IN As Integer = 25
  Private Const SQL_COLUMN_NRE_PLAYED_AMOUNT As Integer = 26
  Private Const SQL_COLUMN_NRE_WON_AMOUNT As Integer = 27
  Private Const SQL_COLUMN_NRE_CASH_OUT As Integer = 28
  Private Const SQL_COLUMN_SITE_ID As Integer = 29
  Private Const SQL_COLUMN_RE_TICKET_IN As Integer = 30
  Private Const SQL_COLUMN_PROMO_RE_TICKET_IN As Integer = 31
  Private Const SQL_COLUMN_BILLS_IN_AMOUNT As Integer = 32
  Private Const SQL_COLUMN_PROMO_RE_TICKET_OUT As Integer = 33
  Private Const SQL_COLUMN_PROMO_NONRE_TICKET_IN As Integer = 34
  Private Const SQL_COLUMN_PROMO_NONRE_TICKET_OUT As Integer = 35

  'FJC 30/01/2015
  Private Const SQL_COLUMN_AUX_FT_RE_CASH_IN As Integer = 36
  Private Const SQL_COLUMN_AUX_FT_NR_CASH_IN As Integer = 37
  Private Const SQL_MULTI_CURRENCY_COLUMN_ISO_CODE As Integer = 38

  Private Const GRID_COLUMN_GROUPED_A1 As Integer = 2
  Private Const GRID_COLUMN_GROUPED_A2 As Integer = 3
  Private Const GRID_COLUMN_GROUPED_A3 As Integer = 4
  Private Const GRID_COLUMN_GROUPED_A4 As Integer = 5
  Private Const GRID_COLUMN_GROUPED_A5 As Integer = 6
  Private Const GRID_COLUMN_GROUPED_A6 As Integer = 7
  Private Const GRID_COLUMN_GROUPED_A7 As Integer = 8
  Private Const GRID_COLUMN_GROUPED_A8 As Integer = 9
  Private Const GRID_COLUMN_GROUPED_A9 As Integer = 10

  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_SITE_ID As Integer = 1
  ' Not Const because they can change.
  Private GRID_COLUMN_PROVIDER_ID As Integer = GRID_COLUMN_GROUPED_A1
  Private GRID_COLUMN_TERMINAL_ID As Integer = GRID_COLUMN_GROUPED_A2
  Private GRID_COLUMN_TERMINAL_TYPE_NAME As Integer = GRID_COLUMN_GROUPED_A3
  Private GRID_COLUMN_STARTED As Integer = GRID_COLUMN_GROUPED_A4
  Private GRID_COLUMN_STATUS As Integer = GRID_COLUMN_GROUPED_A5
  Private GRID_COLUMN_TIME As Integer = GRID_COLUMN_GROUPED_A6
  Private GRID_COLUMN_FINISHED As Integer = GRID_COLUMN_GROUPED_A7
  Private GRID_COLUMN_ACCOUNT_ID As Integer = GRID_COLUMN_GROUPED_A8
  Private GRID_COLUMN_HOLDER_NAME As Integer = GRID_COLUMN_GROUPED_A9
  Private Const GRID_COLUMN_RE_INITIAL_BALANCE As Integer = 11
  Private Const GRID_COLUMN_RE_PLAYED_AMOUNT As Integer = 12
  Private Const GRID_COLUMN_RE_WON_AMOUNT As Integer = 13
  Private Const GRID_COLUMN_RE_FINAL_BALANCE As Integer = 14
  Private Const GRID_COLUMN_RE_NETWIN As Integer = 15
  Private Const GRID_COLUMN_REDEEMABLE_TICKET_IN As Integer = 16
  Private Const GRID_COLUMN_PROMO_RE_TICKET_IN As Integer = 17
  Private Const GRID_COLUMN_BILLS_IN_AMOUNT As Integer = 18
  Private Const GRID_COLUMN_REDEEMABLE_TICKET_OUT As Integer = 19
  Private Const GRID_COLUMN_NRE_INITIAL_BALANCE As Integer = 20
  Private Const GRID_COLUMN_NRE_PLAYED_AMOUNT As Integer = 21
  Private Const GRID_COLUMN_NRE_WON_AMOUNT As Integer = 22
  Private Const GRID_COLUMN_NRE_FINAL_BALANCE As Integer = 23
  Private Const GRID_COLUMN_NRE_NETWIN As Integer = 24
  Private Const GRID_COLUMN_PROMO_NONRE_TICKET_IN As Integer = 25
  Private Const GRID_COLUMN_PROMO_NONRE_TICKET_OUT As Integer = 26
  Private Const GRID_COLUMN_TOTAL_CASH_IN As Integer = 27
  Private Const GRID_COLUMN_PLAYED_AMOUNT As Integer = 28
  Private Const GRID_COLUMN_WON_AMOUNT As Integer = 29
  Private Const GRID_COLUMN_TOTAL_CASH_OUT As Integer = 30
  Private Const GRID_COLUMN_NETWIN As Integer = 31
  Private Const GRID_COLUMN_TOTAL_TICKET_IN As Integer = 32
  Private Const GRID_COLUMN_BILLS_IN_AMOUNT_TOTAL As Integer = 33
  Private Const GRID_COLUMN_TOTAL_TICKET_OUT As Integer = 34
  Private Const GRID_COLUMN_CASH_IN_PLAY_SESSION As Integer = 35
  Private Const GRID_COLUMN_SESSION_ID As Integer = 36
  Private Const GRID_COLUMN_CARD_ID As Integer = 37
  Private Const GRID_COLUMN_TERMINAL_TYPE As Integer = 38
  Private Const GRID_COLUMN_PLAYED_COUNT As Integer = 39
  Private Const GRID_COLUMN_WON_COUNT As Integer = 40
  Private Const GRID_COLUMN_PROMOTIONAL As Integer = 41

  Private Const GRID_COLUMNS As Integer = 42
  Private Const GRID_HEADER_ROWS As Integer = 2

  ' Width
  Private Const GRID_WIDTH_DATE As Integer = 1900
  Private Const GRID_WIDTH_TIME As Integer = 1100
  Private Const GRID_WIDTH_AMOUNT As Integer = 1200
  Private Const GRID_WIDTH_AMOUNT_LONG As Integer = 1650
  Private Const GRID_WIDTH_PROMO As Integer = 700
  Private Const GRID_WIDTH_PROVIDER As Integer = 1850
  Private Const GRID_WIDTH_TERMINAL As Integer = 1710
  Private Const GRID_WIDTH_CARD_ACCOUNT As Integer = 1000
  Private Const GRID_WIDTH_CARD_ID As Integer = 2250
  Private Const GRID_WIDTH_PLAYS As Integer = 1000

  ' Excel columns
  Private Const EXCEL_COLUMN_SITE_ID As Integer = 0

  ' Timeout to allow closing a session (in minutes)
  Private Const INACTIVITY_TIMEOUT As Integer = 60


#End Region ' Constants

#Region "Enums"

  Private Enum ENUM_GROUPED
    NOT_GROUPED = 0
    BY_PROVIDER = 1
    BY_ACCOUNT = 2
  End Enum

  Private Enum ENUM_GRID_ROW_TYPE
    NORMAL = 0
    TOTAL_PROVIDER = 1
    TOTAL_TERMINAL = 2
    TOTAL_ACCOUNT = 3
    TOTAL = 4
  End Enum

#End Region ' Enums

#Region " Structures "

  Private Structure TYPE_TOTAL_COUNTERS
    Dim key As String
    Dim played_amount As Double
    Dim won_amount As Double
    Dim initial_balance As Double
    Dim cash_in_play_session As Double
    Dim fin_balance As Double
    Dim re_initial_balance As Double
    Dim re_played_amount As Double
    Dim re_won_amount As Double
    Dim re_fin_balance As Double
    Dim nre_initial_balance As Double
    Dim nre_played_amount As Double
    Dim nre_won_amount As Double
    Dim nre_fin_balance As Double
    Dim played_count As Integer
    Dim won_count As Integer
    Dim redeemable_ticket_in As Double
    Dim redeemable_ticket_in_promo As Double
    Dim bill_in As Double
    Dim redeemable_ticket_out As Double
    Dim not_redeemable_ticket_in As Double
    Dim not_redeemable_ticket_out As Double
    Dim total_ticket_in As Double
    Dim total_bill_in As Double
    Dim total_ticket_out As Double
  End Structure

#End Region ' Structures

#Region " Members "

  Private m_refreshing_grid As Boolean

  ' For report filters 
  Private m_date_from As String
  Private m_date_filter As String
  Private m_date_open_sessions As Boolean
  Private m_date_to As String
  Private m_status As String
  Private m_card_account As String
  Private m_card_track_data As String
  Private m_card_holder_name As String
  Private m_card_holder_is_vip As String
  Private m_options_value As String

  ' For totals
  Private m_first_time As Boolean
  Private m_grouped_by As ENUM_GROUPED
  Private m_terminal_total As TYPE_TOTAL_COUNTERS
  Private m_provider_total As TYPE_TOTAL_COUNTERS
  Private m_account_total As TYPE_TOTAL_COUNTERS

  Private m_global_total As TYPE_TOTAL_COUNTERS
  Private m_last_grouped_by As ENUM_GROUPED
  Private m_is_tito_mode As Boolean

  ' For multi currency
  Private m_is_multi_currency As Boolean = GeneralParam.GetBoolean("MultiSite.Multicurrency", "Enabled", False)
  Private m_site As String
  Private m_currency_option As String

#End Region ' Members

#Region " OVERRIDES "

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_PLAYS_SESSIONS
    Call MyBase.GUI_SetFormId()
  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.m_is_tito_mode = TITO.Utils.IsTitoMode

    Me.Text = GLB_NLS_GUI_STATISTICS.GetString(357)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_STATISTICS.GetString(2)

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_INFO).Visible = False ' Button Plays
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = False ' Button Close session

    ' Date
    Me.gb_date.Text = GLB_NLS_GUI_INVOICING.GetString(201) ' 201 - Date
    Me.rb_open_sessions.Text = GLB_NLS_GUI_STATISTICS.GetString(345) ' 345 - Open Sessions
    Me.rb_historic.Text = GLB_NLS_GUI_CONTROLS.GetString(331) ' 331 - History
    Me.rb_ini_date.Text = GLB_NLS_GUI_STATISTICS.GetString(381) ' 381 - Started
    Me.rb_finish_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5704) ' 5704 - Finished
    Me.dtp_from.Text = GLB_NLS_GUI_AUDITOR.GetString(257)
    Me.dtp_to.Text = GLB_NLS_GUI_AUDITOR.GetString(258)
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    AddHandler rb_open_sessions.Click, AddressOf rb_date_CheckedChanged
    AddHandler rb_historic.Click, AddressOf rb_date_CheckedChanged
    AddHandler rb_ini_date.Click, AddressOf rb_date_CheckedChanged
    AddHandler rb_finish_date.Click, AddressOf rb_date_CheckedChanged

    ' Status
    Me.gb_status.Text = GLB_NLS_GUI_STATISTICS.GetString(394)
    Me.opt_several_status.Text = GLB_NLS_GUI_ALARMS.GetString(276)
    Me.opt_all_status.Text = GLB_NLS_GUI_ALARMS.GetString(277)

    ' RCI 13-DEC-2010: Only Gap Sessions
    Me.chk_only_gap_sessions.Text = GLB_NLS_GUI_STATISTICS.GetString(424)

    ' RCI 02-AUG-2011: Grouped by provider or account
    Me.gb_options.Text = GLB_NLS_GUI_INVOICING.GetString(369)
    Me.chk_grouped.Text = GLB_NLS_GUI_INVOICING.GetString(458)
    Me.opt_provider.Text = GLB_NLS_GUI_INVOICING.GetString(421)
    Me.opt_account.Text = GLB_NLS_GUI_INVOICING.GetString(422)
    Me.chk_subtotal.Text = GLB_NLS_GUI_INVOICING.GetString(371, GLB_NLS_GUI_INVOICING.GetString(424))
    Me.chk_show_sessions.Text = GLB_NLS_GUI_INVOICING.GetString(425)

    Me.lbl_handpay.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1153)
    Me.lbl_recharges.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1817)

    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

    'adapt header controls to the multi currency filter (because is greater than the currency filter)
    If m_is_multi_currency Then
      Call InitializeMultiSiteControls()
    End If

    Me.MultiSiteCurrencies = uc_multi_currency_select.CurrenciesTable()

    For Each status As Integer In System.Enum.GetValues(GetType(WSI.Common.PlaySessionStatus))
      Me.cmb_status.Add(status, PlaySessionStatusName(status, Integer.MinValue))
    Next

    uc_site_select.ShowMultisiteRow = False
    uc_site_select.Init()
    uc_site_select.SetDefaultValues(False)

    Me.cmb_status.Enabled = False
    Me.ColumnIsoCode = SQL_MULTI_CURRENCY_COLUMN_ISO_CODE
    Me.ColumnsChangeCurrency = GetColumnsToChangeCurrency()

  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_BeforeFirstRow()

    If m_grouped_by <> m_last_grouped_by Then
      GUI_StyleSheet()
    End If

    m_first_time = True

    ResetTotalCounters(m_provider_total, "")
    ResetTotalCounters(m_terminal_total, "")
    ResetTotalCounters(m_account_total, "")

    ResetTotalCounters(m_global_total, "")

  End Sub ' GUI_BeforeFirsRow

  ' PURPOSE: Perform final processing for the grid data (totalisator row)
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow()
    Select Case m_grouped_by
      Case ENUM_GROUPED.BY_PROVIDER
        If Not String.IsNullOrEmpty(m_terminal_total.key) Then
          DrawTotalRow(m_terminal_total, ENUM_GRID_ROW_TYPE.TOTAL_TERMINAL)
        End If
        If Not String.IsNullOrEmpty(m_provider_total.key) Then
          DrawTotalRow(m_provider_total, ENUM_GRID_ROW_TYPE.TOTAL_PROVIDER)
        End If

      Case ENUM_GROUPED.BY_ACCOUNT
        If Not String.IsNullOrEmpty(m_provider_total.key) Then
          DrawTotalRow(m_provider_total, ENUM_GRID_ROW_TYPE.TOTAL_PROVIDER)
        End If
        If Not String.IsNullOrEmpty(m_account_total.key) Then
          DrawTotalRow(m_account_total, ENUM_GRID_ROW_TYPE.TOTAL_ACCOUNT)
        End If

      Case ENUM_GROUPED.NOT_GROUPED
      Case Else
    End Select

    DrawTotalRow(m_global_total, ENUM_GRID_ROW_TYPE.TOTAL)

  End Sub ' GUI_AfterLastRow

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  '
  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Dates selection 
    If Me.dtp_from.Checked And Me.dtp_to.Checked Then
      If Me.dtp_from.Value > Me.dtp_to.Value Then
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.dtp_to.Focus()

        Return False
      End If
    End If

    ' Check fields on uc_account_sel are ok
    If Not Me.uc_account_sel1.ValidateFormat Then

      Return False
    End If

    ' Sites selection
    If m_is_multi_currency Then
      Me.ApplyFormatCurrency = uc_multi_currency_select.IsGridApplyChanges()
      UpdateSiteCurrenciesList()
      Me.MultiSiteCurrencies = uc_multi_currency_select.CurrenciesTable()
      If Not uc_multi_currency_select.FilterCheckSites() Then

        Return False
      End If
    Else
      If Not uc_site_select.FilterCheckSites() Then

        Return False
      End If
    End If

    Return True
  End Function ' GUI_FilterCheck

  ' PURPOSE: Set a different value for the maximum number of rows that can be showed
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Function GUI_MaxRows() As Integer
    Return Int32.MaxValue
  End Function ' GUI_MaxRows

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  '
  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim _str_sql As StringBuilder
    Dim _order_by As String

    _str_sql = New StringBuilder()

    ' Get Select and from
    _str_sql.AppendLine("SELECT   PS_PLAY_SESSION_ID ")
    _str_sql.AppendLine("       , PS_STARTED ")
    _str_sql.AppendLine("       , ISNULL(PS_FINISHED, GETDATE()) ")
    _str_sql.AppendLine("       , TE_NAME ")
    _str_sql.AppendLine("       , PS_TOTAL_CASH_IN ")
    _str_sql.AppendLine("       , PS_PLAYED_AMOUNT ")
    _str_sql.AppendLine("       , PS_WON_AMOUNT ")
    _str_sql.AppendLine("       , PS_PLAYED_COUNT ")
    _str_sql.AppendLine("       , PS_WON_COUNT ")
    _str_sql.AppendLine("       , PS_CASH_IN ")
    _str_sql.AppendLine("       , PS_TOTAL_CASH_OUT ")
    _str_sql.AppendLine("       , PS_STATUS ")
    _str_sql.AppendLine("       , AC_ACCOUNT_ID ")
    _str_sql.AppendLine("       , AC_TYPE ")
    _str_sql.AppendLine("       , AC_TRACK_DATA ")
    _str_sql.AppendLine("       , PS_PROMO ")
    _str_sql.AppendLine("       , AC_HOLDER_NAME ")
    _str_sql.AppendLine("       , TE_PROVIDER_ID ")
    _str_sql.AppendLine("       , DATEDIFF(SECOND, PS_STARTED, ISNULL(PS_FINISHED, GETDATE())) PS_DURATION ")
    _str_sql.AppendLine("       , TE_TERMINAL_TYPE ")
    _str_sql.AppendLine("       , PS_TYPE ")
    _str_sql.AppendLine("       , PS_REDEEMABLE_CASH_IN ")
    _str_sql.AppendLine("       , PS_REDEEMABLE_PLAYED ")
    _str_sql.AppendLine("       , PS_REDEEMABLE_WON ")
    _str_sql.AppendLine("       , PS_REDEEMABLE_CASH_OUT ")
    _str_sql.AppendLine("       , PS_NON_REDEEMABLE_CASH_IN ")
    _str_sql.AppendLine("       , PS_NON_REDEEMABLE_PLAYED ")
    _str_sql.AppendLine("       , PS_NON_REDEEMABLE_WON ")
    _str_sql.AppendLine("       , PS_NON_REDEEMABLE_CASH_OUT ")
    _str_sql.AppendLine("       , PS_SITE_ID ")

    If Me.m_is_tito_mode Then
      _str_sql.AppendLine("        , ISNULL(PS_RE_TICKET_IN,0) ")
      _str_sql.AppendLine("        , ISNULL(PS_PROMO_RE_TICKET_IN,0) ")
      _str_sql.AppendLine("        , ISNULL(PS_BILLS_IN_AMOUNT,0) ")
      _str_sql.AppendLine("        , ISNULL(PS_RE_TICKET_OUT,0) ")
      _str_sql.AppendLine("        , ISNULL(PS_PROMO_NR_TICKET_IN,0) ")
      _str_sql.AppendLine("        , ISNULL(PS_PROMO_NR_TICKET_OUT,0) ")
      _str_sql.AppendLine("        , ISNULL(PS_AUX_FT_RE_CASH_IN,0) ")
      _str_sql.AppendLine("        , ISNULL(PS_AUX_FT_NR_CASH_IN,0) ")

    Else
      _str_sql.AppendLine("        , 0 AS PS_RE_TICKET_IN ")
      _str_sql.AppendLine("        , 0 AS PS_PROMO_RE_TICKET_IN ")
      _str_sql.AppendLine("        , 0 AS PS_BILLS_IN_AMOUNT ")
      _str_sql.AppendLine("        , 0 AS PS_RE_TICKET_OUT ")
      _str_sql.AppendLine("        , 0 AS PS_PROMO_NR_TICKET_IN ")
      _str_sql.AppendLine("        , 0 AS PS_PROMO_NR_TICKET_OUT ")
      _str_sql.AppendLine("        , 0 AS PS_AUX_FT_RE_CASH_IN   ")
      _str_sql.AppendLine("        , 0 AS PS_AUX_FT_NR_CASH_IN   ")
    End If

    If m_is_multi_currency AndAlso uc_multi_currency_select.HasSitesWithoutCurrency = False Then
      _str_sql.AppendLine("	      , SC_ISO_CODE")
    Else
      _str_sql.AppendLine(String.Format("	      , '{0}' AS SC_ISO_CODE", GeneralParam.GetString("RegionalOptions", "CurrencyISOCode")))
    End If

    _str_sql.AppendLine("  FROM ( ")

    _str_sql.AppendLine(GetSqlWhere())

    If Not Me.rb_finish_date.Checked Then
      _order_by = "PS_STARTED DESC"
    Else
      _order_by = "PS_FINISHED DESC"
    End If

    m_last_grouped_by = m_grouped_by
    m_grouped_by = ENUM_GROUPED.NOT_GROUPED

    If Me.chk_grouped.Checked Then
      If Me.opt_provider.Checked Then
        _order_by = "TE_PROVIDER_ID, TE_NAME, AC_ACCOUNT_ID, " + _order_by
        m_grouped_by = ENUM_GROUPED.BY_PROVIDER

        GRID_COLUMN_PROVIDER_ID = GRID_COLUMN_GROUPED_A1
        GRID_COLUMN_TERMINAL_ID = GRID_COLUMN_GROUPED_A2
        GRID_COLUMN_TERMINAL_TYPE_NAME = GRID_COLUMN_GROUPED_A3
        GRID_COLUMN_STARTED = GRID_COLUMN_GROUPED_A4
        GRID_COLUMN_STATUS = GRID_COLUMN_GROUPED_A5
        GRID_COLUMN_TIME = GRID_COLUMN_GROUPED_A6
        GRID_COLUMN_FINISHED = GRID_COLUMN_GROUPED_A7
        GRID_COLUMN_ACCOUNT_ID = GRID_COLUMN_GROUPED_A8
        GRID_COLUMN_HOLDER_NAME = GRID_COLUMN_GROUPED_A9

      ElseIf Me.opt_account.Checked Then
        _order_by = "AC_ACCOUNT_ID, TE_PROVIDER_ID, TE_NAME, " + _order_by
        m_grouped_by = ENUM_GROUPED.BY_ACCOUNT

        GRID_COLUMN_ACCOUNT_ID = GRID_COLUMN_GROUPED_A1
        GRID_COLUMN_HOLDER_NAME = GRID_COLUMN_GROUPED_A2
        GRID_COLUMN_STARTED = GRID_COLUMN_GROUPED_A3
        GRID_COLUMN_STATUS = GRID_COLUMN_GROUPED_A4
        GRID_COLUMN_TIME = GRID_COLUMN_GROUPED_A5
        GRID_COLUMN_FINISHED = GRID_COLUMN_GROUPED_A6
        GRID_COLUMN_PROVIDER_ID = GRID_COLUMN_GROUPED_A7
        GRID_COLUMN_TERMINAL_ID = GRID_COLUMN_GROUPED_A8
        GRID_COLUMN_TERMINAL_TYPE_NAME = GRID_COLUMN_GROUPED_A9
      End If
    Else
      GRID_COLUMN_PROVIDER_ID = GRID_COLUMN_GROUPED_A1
      GRID_COLUMN_TERMINAL_ID = GRID_COLUMN_GROUPED_A2
      GRID_COLUMN_TERMINAL_TYPE_NAME = GRID_COLUMN_GROUPED_A3
      GRID_COLUMN_STARTED = GRID_COLUMN_GROUPED_A4
      GRID_COLUMN_STATUS = GRID_COLUMN_GROUPED_A5
      GRID_COLUMN_TIME = GRID_COLUMN_GROUPED_A6
      GRID_COLUMN_FINISHED = GRID_COLUMN_GROUPED_A7
      GRID_COLUMN_ACCOUNT_ID = GRID_COLUMN_GROUPED_A8
      GRID_COLUMN_HOLDER_NAME = GRID_COLUMN_GROUPED_A9
    End If

    _str_sql.AppendLine(" ) X ORDER BY " & _order_by)

    Return _str_sql.ToString()

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE : Checks out the values of a db row before adding it to the grid
  '           We use it here to add total data before adding "normal" data.
  '
  '  PARAMS :
  '     - INPUT :
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the db row should be added to the grid
  '     - False: the db row should NOT be added to the grid
  '
  Public Overrides Function GUI_CheckOutRowBeforeAdd(ByVal DbRow As CLASS_DB_ROW) As Boolean

    ' If ACCOUNT_ID is null, the account is virtual, so don't take them in account when groupping by account
    If Me.chk_grouped.Checked AndAlso Me.opt_account.Checked AndAlso DbRow.IsNull(SQL_COLUMN_ACCOUNT_ID) Then

      Return False
    End If

    Call ProcessCounters(DbRow)

    ' Have to show sessions?
    Return Me.chk_show_sessions.Checked Or Not Me.chk_grouped.Checked
  End Function ' GUI_CheckOutRowBeforeAdd

  ' PURPOSE : Get string of play session
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - String: 
  '
  Private Function PlaySessionStatusName(ByVal Status As WSI.Common.PlaySessionStatus, ByVal Type As WSI.Common.PlaySessionType) As String

    Dim _str_status As String
    Dim _int_value As Int32
    Dim _handpay As Boolean
    Dim _nls_handpay As Int16

    _handpay = False
    _nls_handpay = -1
    _str_status = ""

    Select Case Status
      Case WSI.Common.PlaySessionStatus.Opened
        _str_status = GLB_NLS_GUI_STATISTICS.GetString(400) ' Opened
      Case WSI.Common.PlaySessionStatus.Closed
        _str_status = GLB_NLS_GUI_STATISTICS.GetString(401) ' Closed
      Case WSI.Common.PlaySessionStatus.Abandoned
        _str_status = GLB_NLS_GUI_STATISTICS.GetString(402) ' Abandoned
      Case WSI.Common.PlaySessionStatus.ManualClosed
        _str_status = GLB_NLS_GUI_STATISTICS.GetString(404) ' Manual Closed
      Case WSI.Common.PlaySessionStatus.Cancelled
        _str_status = GLB_NLS_GUI_STATISTICS.GetString(406) ' Cancelled
      Case WSI.Common.PlaySessionStatus.HandpayPayment
        If Type = Integer.MinValue Then
          _str_status = GLB_NLS_GUI_STATISTICS.GetString(407) ' HandpayPayment
        Else
          _handpay = True
          _nls_handpay = 1151
        End If
      Case WSI.Common.PlaySessionStatus.HandpayCancelPayment
        If Type = Integer.MinValue Then
          _str_status = GLB_NLS_GUI_STATISTICS.GetString(408) ' HandpayCancelPayment
        Else
          _handpay = True
          _nls_handpay = 1152
        End If
      Case WSI.Common.PlaySessionStatus.DrawWinner
        _str_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2606) ' DrawWinner
      Case WSI.Common.PlaySessionStatus.DrawLoser
        _str_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2607) ' DrawLoser
      Case WSI.Common.PlaySessionStatus.ProgressiveProvision
        _str_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5410) ' Progressive provision
      Case WSI.Common.PlaySessionStatus.ProgressiveReserve
        _str_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5411) ' Progressive reserve
      Case WSI.Common.PlaySessionStatus.GameGatewayBet
        _str_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7052, GeneralParam.GetString("GameGateway", "Name", "BonoPlay")) 'GameGatewayBet
      Case WSI.Common.PlaySessionStatus.GameGatewayPrize
        _str_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7053, GeneralParam.GetString("GameGateway", "Name", "BonoPlay")) 'GameGatewayPrize
      Case WSI.Common.PlaySessionStatus.GameGatewayBetRollback
        _str_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7064, GeneralParam.GetString("GameGateway", "Name", "BonoPlay")) 'GameGatewayBetRollback
      Case WSI.Common.PlaySessionStatus.PariPlayBet
        _str_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7052, GeneralParam.GetString("PariPlay", "Name", "PariPlay"))     'PariPlayBet
      Case WSI.Common.PlaySessionStatus.PariPlayPrize
        _str_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7053, GeneralParam.GetString("PariPlay", "Name", "PariPlay"))     'PariPlayPrize
      Case WSI.Common.PlaySessionStatus.PariPlayBetRollback
        _str_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7064, GeneralParam.GetString("PariPlay", "Name", "PariPlay"))     'PariPlayRollback
      Case Else
        _int_value = Status
        _str_status = "(" & _int_value.ToString() & ") - " & Status.ToString()
    End Select

    If _handpay Then

      Select Case Type
        Case WSI.Common.PlaySessionType.HANDPAY_CANCELLED_CREDITS
          _str_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(_nls_handpay, " - " & GLB_NLS_GUI_INVOICING.GetString(320))
        Case WSI.Common.PlaySessionType.HANDPAY_JACKPOT
          _str_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(_nls_handpay, " - " & GLB_NLS_GUI_INVOICING.GetString(321))
        Case WSI.Common.PlaySessionType.HANDPAY_MANUAL
          _str_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(_nls_handpay, " - " & GLB_NLS_GUI_INVOICING.GetString(330))
        Case WSI.Common.PlaySessionType.HANDPAY_ORPHAN_CREDITS
          _str_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(_nls_handpay, " - " & GLB_NLS_GUI_INVOICING.GetString(322))
        Case WSI.Common.PlaySessionType.HANDPAY_SITE_JACKPOT
          _str_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(_nls_handpay, " - " & GLB_NLS_GUI_INVOICING.GetString(323))
        Case Else
          _str_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(_nls_handpay, "")
      End Select

    End If

    Return _str_status

  End Function

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As CLASS_DB_ROW) As Boolean

    Dim _time As TimeSpan
    Dim _status As Integer
    Dim _str_status As String = ""
    Dim _played_amount As Double
    Dim _won_amount As Double
    Dim _initial_balance As Double
    Dim _cash_in_play_session As Double
    Dim _final_balance As Double
    Dim _is_promo As Boolean = False
    Dim _played_count As Integer
    Dim _won_count As Integer
    Dim _re_ticket_in As Double
    Dim _re_promo_ticket_in As Double
    Dim _re_bill_in As Double
    Dim _re_ticket_out As Double
    Dim _nr_promo_ticket_in As Double
    Dim _nr_promo_ticket_out As Double
    Dim _total_ticket_in As Double
    Dim _total_bills_in As Double
    Dim _total_ticket_out As Double

    _played_amount = 0
    _won_amount = 0
    _initial_balance = 0
    _cash_in_play_session = 0
    _final_balance = 0
    _played_count = 0
    _won_count = 0
    _re_ticket_in = 0
    _re_promo_ticket_in = 0
    _re_bill_in = 0
    _re_ticket_out = 0
    _nr_promo_ticket_in = 0
    _nr_promo_ticket_out = 0
    _total_ticket_in = 0
    _total_bills_in = 0
    _total_ticket_out = 0

    ' Started 
    Me.Grid.Cell(RowIndex, GRID_COLUMN_STARTED).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_STARTED), _
                                                                       ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                       ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    ' Status
    _status = DbRow.Value(SQL_COLUMN_STATUS)
    _str_status = PlaySessionStatusName(_status, DbRow.Value(SQL_COLUMN_TYPE))

    Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = _str_status

    ' Time
    _time = New TimeSpan(0, 0, DbRow.Value(SQL_COLUMN_DURATION))
    If _time.TotalSeconds > 0 And _status <> WSI.Common.PlaySessionStatus.Opened Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TIME).Value = TimeSpanToString(_time)
    End If

    ' Finished 
    If _status <> WSI.Common.PlaySessionStatus.Opened And _time.TotalSeconds > 0 Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_FINISHED).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_FINISHED), _
                                                                             ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                             ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    ' Account Id
    If Not DbRow.IsNull(SQL_COLUMN_ACCOUNT_ID) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_ID).Value = DbRow.Value(SQL_COLUMN_ACCOUNT_ID)
    End If

    ' Holder Name
    If Not DbRow.IsNull(SQL_COLUMN_HOLDER_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_NAME).Value = DbRow.Value(SQL_COLUMN_HOLDER_NAME)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_NAME).Value = " "
    End If

    ' Card Id
    If Not DbRow.IsNull(SQL_COLUMN_ACCOUNT_TYPE) AndAlso Not DbRow.IsNull(SQL_COLUMN_CARD_ID) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CARD_ID).Value = TrackDataToExternal(DbRow.Value(SQL_COLUMN_ACCOUNT_TYPE), _
                                                                                DbRow.Value(SQL_COLUMN_CARD_ID))
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CARD_ID).Value = ""
    End If

    ' Provider Name
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PROVIDER_ID).Value = DbRow.Value(SQL_COLUMN_PROVIDER_NAME)

    ' Terminal Name
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_ID).Value = DbRow.Value(SQL_COLUMN_TERMINAL_NAME)

    ' Terminal Type
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_TYPE).Value = DbRow.Value(SQL_COLUMN_TERMINAL_TYPE)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_TYPE_NAME).Value = GetTerminalTypeNls(DbRow.Value(SQL_COLUMN_TERMINAL_TYPE))

    ' Redeemable Initial Balance
    _initial_balance = DbRow.Value(SQL_COLUMN_RE_CASH_IN)
    If Me.m_is_tito_mode Then
      If Not DbRow.IsNull(SQL_COLUMN_AUX_FT_RE_CASH_IN) Then

        _initial_balance += DbRow.Value(SQL_COLUMN_AUX_FT_RE_CASH_IN)
      End If
    End If

    Me.Grid.Cell(RowIndex, GRID_COLUMN_RE_INITIAL_BALANCE).Value = GUI_MultiCurrencyValue_Format(_initial_balance)

    ' Redeemable Played amount
    _played_amount = DbRow.Value(SQL_COLUMN_RE_PLAYED_AMOUNT)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_RE_PLAYED_AMOUNT).Value = GUI_MultiCurrencyValue_Format(_played_amount)

    ' Redeemable Won Amount
    _won_amount = DbRow.Value(SQL_COLUMN_RE_WON_AMOUNT)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_RE_WON_AMOUNT).Value = GUI_MultiCurrencyValue_Format(_won_amount)

    ' Redeemable Final Balance
    _final_balance = DbRow.Value(SQL_COLUMN_RE_CASH_OUT)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_RE_FINAL_BALANCE).Value = GUI_MultiCurrencyValue_Format(_final_balance)

    ' Redeemable Netwin
    Me.Grid.Cell(RowIndex, GRID_COLUMN_RE_NETWIN).Value = GUI_MultiCurrencyValue_Format(_initial_balance - _final_balance)

    If Me.m_is_tito_mode Then
      ' Redeemable - Ticket In
      If Not DbRow.IsNull(SQL_COLUMN_RE_TICKET_IN) Then
        _re_ticket_in = DbRow.Value(SQL_COLUMN_RE_TICKET_IN)
      End If
      Me.Grid.Cell(RowIndex, GRID_COLUMN_REDEEMABLE_TICKET_IN).Value = GUI_MultiCurrencyValue_Format(_re_ticket_in)

      ' Redeemable - Promo Redeemable Ticket In
      If Not DbRow.IsNull(SQL_COLUMN_PROMO_RE_TICKET_IN) Then
        _re_promo_ticket_in = DbRow.Value(SQL_COLUMN_PROMO_RE_TICKET_IN)
      End If
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMO_RE_TICKET_IN).Value = GUI_MultiCurrencyValue_Format(_re_promo_ticket_in)

      ' Redeemable - Bill In
      If Not DbRow.IsNull(SQL_COLUMN_BILLS_IN_AMOUNT) Then
        _re_bill_in = DbRow.Value(SQL_COLUMN_BILLS_IN_AMOUNT)
      End If
      Me.Grid.Cell(RowIndex, GRID_COLUMN_BILLS_IN_AMOUNT).Value = GUI_MultiCurrencyValue_Format(_re_bill_in)

      ' Redeemable - Promo Redeemable Ticket Out
      If Not DbRow.IsNull(SQL_COLUMN_PROMO_RE_TICKET_OUT) Then
        _re_ticket_out = DbRow.Value(SQL_COLUMN_PROMO_RE_TICKET_OUT)
      End If
      Me.Grid.Cell(RowIndex, GRID_COLUMN_REDEEMABLE_TICKET_OUT).Value = GUI_MultiCurrencyValue_Format(_re_ticket_out)
    End If

    ' Not Redeemable Initial Balance
    _initial_balance = DbRow.Value(SQL_COLUMN_NRE_CASH_IN)
    If Me.m_is_tito_mode Then
      If Not DbRow.IsNull(SQL_COLUMN_AUX_FT_NR_CASH_IN) Then

        _initial_balance += DbRow.Value(SQL_COLUMN_AUX_FT_NR_CASH_IN)
      End If
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NRE_INITIAL_BALANCE).Value = GUI_MultiCurrencyValue_Format(_initial_balance)

    ' Not Redeemable Played amount
    _played_amount = DbRow.Value(SQL_COLUMN_NRE_PLAYED_AMOUNT)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NRE_PLAYED_AMOUNT).Value = GUI_MultiCurrencyValue_Format(_played_amount)

    ' Not Redeemable Won Amount
    _won_amount = DbRow.Value(SQL_COLUMN_NRE_WON_AMOUNT)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NRE_WON_AMOUNT).Value = GUI_MultiCurrencyValue_Format(_won_amount)

    ' Not Redeemable Final Balance
    _final_balance = DbRow.Value(SQL_COLUMN_NRE_CASH_OUT)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NRE_FINAL_BALANCE).Value = GUI_MultiCurrencyValue_Format(_final_balance)

    ' Not Redeemable Netwin
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NRE_NETWIN).Value = GUI_MultiCurrencyValue_Format(_initial_balance - _final_balance)

    If Me.m_is_tito_mode Then
      ' Not Redeemable - Promo NR Ticket In
      If Not DbRow.IsNull(SQL_COLUMN_PROMO_NONRE_TICKET_IN) Then
        _nr_promo_ticket_in = DbRow.Value(SQL_COLUMN_PROMO_NONRE_TICKET_IN)
      End If
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMO_NONRE_TICKET_IN).Value = GUI_MultiCurrencyValue_Format(_nr_promo_ticket_in)

      ' Not Redeemable - Promo NR Ticket Out
      If Not DbRow.IsNull(SQL_COLUMN_PROMO_NONRE_TICKET_OUT) Then
        _nr_promo_ticket_out = DbRow.Value(SQL_COLUMN_PROMO_NONRE_TICKET_OUT)
      End If
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMO_NONRE_TICKET_OUT).Value = GUI_MultiCurrencyValue_Format(_nr_promo_ticket_out)
    End If

    ' Played amount
    _played_amount = DbRow.Value(SQL_COLUMN_PLAYED_AMOUNT)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PLAYED_AMOUNT).Value = GUI_MultiCurrencyValue_Format(_played_amount)

    ' Won Amount
    _won_amount = DbRow.Value(SQL_COLUMN_WON_AMOUNT)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_WON_AMOUNT).Value = GUI_MultiCurrencyValue_Format(_won_amount)


    ' Initial Balance
    _initial_balance = DbRow.Value(SQL_COLUMN_TOTAL_CASH_IN)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_CASH_IN).Value = GUI_MultiCurrencyValue_Format(_initial_balance)

    ' Final Balance
    If Not DbRow.IsNull(SQL_COLUMN_TOTAL_CASH_OUT) Then
      _final_balance = DbRow.Value(SQL_COLUMN_TOTAL_CASH_OUT)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_CASH_OUT).Value = GUI_MultiCurrencyValue_Format(_final_balance)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_CASH_OUT).Value = ""
    End If

    ' Netwin
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NETWIN).Value = GUI_MultiCurrencyValue_Format(_initial_balance - _final_balance)

    If Me.m_is_tito_mode Then
      ' TOTAL - Ticket In
      _total_ticket_in = _re_ticket_in
      _total_ticket_in += _re_promo_ticket_in
      _total_ticket_in += _nr_promo_ticket_in
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_TICKET_IN).Value = GUI_MultiCurrencyValue_Format(_total_ticket_in)

      ' TOTAL - Bill In
      _total_bills_in = _re_bill_in
      Me.Grid.Cell(RowIndex, GRID_COLUMN_BILLS_IN_AMOUNT_TOTAL).Value = GUI_MultiCurrencyValue_Format(_total_bills_in)

      ' TOTAL - Ticket Out
      _total_ticket_out = _re_ticket_out
      _total_ticket_out += _nr_promo_ticket_out
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_TICKET_OUT).Value = GUI_MultiCurrencyValue_Format(_total_ticket_out)
    Else
      ' Cash In Play Session
      _cash_in_play_session = DbRow.Value(SQL_COLUMN_CASH_IN_PLAY_SESSION)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CASH_IN_PLAY_SESSION).Value = GUI_MultiCurrencyValue_Format(_cash_in_play_session)
    End If

    ' Session Id
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SESSION_ID).Value = DbRow.Value(SQL_COLUMN_SESSION_ID)

    ' Type
    Select Case DbRow.Value(SQL_COLUMN_TYPE)
      Case WSI.Common.PlaySessionType.PROGRESSIVE_PROVISION, _
           WSI.Common.PlaySessionType.PROGRESSIVE_RESERVE

        ' Set Colors
        Me.Grid.Row(RowIndex).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_OCHRE_00)
      Case Else

        If DbRow.Value(SQL_COLUMN_STATUS) = WSI.Common.PlaySessionStatus.HandpayPayment _
        Or DbRow.Value(SQL_COLUMN_STATUS) = WSI.Common.PlaySessionStatus.HandpayCancelPayment Then

          Me.Grid.Row(RowIndex).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_OCHRE_00)
        Else

          If Math.Round((_played_amount - _won_amount), 2) <> Math.Round((_initial_balance - _final_balance), 2) Then
            Me.Grid.Row(RowIndex).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_RED_01)
          End If

          ' Promotional
          _is_promo = DbRow.Value(SQL_COLUMN_PROMOTIONAL)

          If _is_promo Then
            Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMOTIONAL).Value = GLB_NLS_GUI_STATISTICS.GetString(392)
            Me.Grid.Row(RowIndex).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_BLUE_01)
          Else
            Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMOTIONAL).Value = ""
          End If

        End If
    End Select

    ' Played Count
    _played_count = DbRow.Value(SQL_COLUMN_PLAYED_COUNT)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PLAYED_COUNT).Value = GUI_FormatNumber(_played_count, 0)

    ' Won Count
    _won_count = DbRow.Value(SQL_COLUMN_WON_COUNT)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_WON_COUNT).Value = GUI_FormatNumber(_won_count, 0)

    ' Site Id
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SITE_ID).Value = String.Format("{0:000}", DbRow.Value(SQL_COLUMN_SITE_ID))

    Return True
  End Function ' GUI_SetupRow

  ' PURPOSE: Set focus to a control when first entering the form
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.uc_site_select
  End Sub ' GUI_SetInitialFocus

  ' PURPOSE: Set the tool tip text for a given row and column
  '
  '  PARAMS:
  '     - INPUT:
  '           - RowIndex As Integer
  '           - ColIndex As Integer
  '     - OUTPUT:
  '           - ToolTipTxt As String
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_SetToolTipText(ByVal RowIndex As Integer, _
                                             ByVal ColIndex As Integer, _
                                             ByRef ToolTipTxt As String)
    Dim _str_holder_name As String
    Dim _str_card_id As String
    Dim _str_account_id As String

    If RowIndex < 0 Or RowIndex >= (Me.Grid.NumRows - 1) Then

      Return
    End If

    _str_holder_name = Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_NAME).Value
    _str_card_id = Me.Grid.Cell(RowIndex, GRID_COLUMN_CARD_ID).Value
    _str_account_id = Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_ID).Value

    If String.IsNullOrEmpty(_str_account_id) Then

      Return
    End If

    If ColIndex = GRID_COLUMN_HOLDER_NAME Or ColIndex = GRID_COLUMN_ACCOUNT_ID Then
      ToolTipTxt = GLB_NLS_GUI_STATISTICS.GetString(397, _str_account_id, _str_card_id, _str_holder_name)
    End If
  End Sub ' GUI_SetToolTipText

  ' PURPOSE: Process clicks on data grid (double-clicks) and select button
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Call MyBase.GUI_ButtonClick(ButtonId)
  End Sub ' GUI_ButtonClick

#End Region  ' Overrides

#Region " GUI Reports "

  ' PURPOSE: Set form specific requirements/parameters for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '           - FirstColIndex
  '     - OUTPUT:
  '
  ' RETURNS:

  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    With Me.Grid

      .Column(GRID_COLUMN_RE_INITIAL_BALANCE).IsColumnPrintable = False
      .Column(GRID_COLUMN_RE_PLAYED_AMOUNT).IsColumnPrintable = False
      .Column(GRID_COLUMN_RE_WON_AMOUNT).IsColumnPrintable = False
      .Column(GRID_COLUMN_RE_FINAL_BALANCE).IsColumnPrintable = False
      .Column(GRID_COLUMN_RE_NETWIN).IsColumnPrintable = False
      .Column(GRID_COLUMN_NRE_INITIAL_BALANCE).IsColumnPrintable = False
      .Column(GRID_COLUMN_NRE_PLAYED_AMOUNT).IsColumnPrintable = False
      .Column(GRID_COLUMN_NRE_WON_AMOUNT).IsColumnPrintable = False
      .Column(GRID_COLUMN_NRE_FINAL_BALANCE).IsColumnPrintable = False
      .Column(GRID_COLUMN_NRE_NETWIN).IsColumnPrintable = False

    End With

    Call MyBase.GUI_ReportParams(PrintData)

  End Sub ' GUI_ReportParams

  ' PURPOSE: Set form specific requirements/parameters for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '           - FirstColIndex
  '     - OUTPUT:
  '
  ' RETURNS:

  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_EXCEL_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    With Me.Grid
      .Column(GRID_COLUMN_RE_INITIAL_BALANCE).IsColumnPrintable = True
      .Column(GRID_COLUMN_RE_PLAYED_AMOUNT).IsColumnPrintable = True
      .Column(GRID_COLUMN_RE_WON_AMOUNT).IsColumnPrintable = True
      .Column(GRID_COLUMN_RE_FINAL_BALANCE).IsColumnPrintable = True
      .Column(GRID_COLUMN_RE_NETWIN).IsColumnPrintable = True
      .Column(GRID_COLUMN_NRE_INITIAL_BALANCE).IsColumnPrintable = True
      .Column(GRID_COLUMN_NRE_PLAYED_AMOUNT).IsColumnPrintable = True
      .Column(GRID_COLUMN_NRE_WON_AMOUNT).IsColumnPrintable = True
      .Column(GRID_COLUMN_NRE_FINAL_BALANCE).IsColumnPrintable = True
      .Column(GRID_COLUMN_NRE_NETWIN).IsColumnPrintable = True

    End With

    Call MyBase.GUI_ReportParams(PrintData)

    ' Set specific column formats.
    PrintData.SetColumnFormat(EXCEL_COLUMN_SITE_ID, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT)

  End Sub ' GUI_ReportParams

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    Dim _info As String()
    Dim _info0 As String
    Dim _info1 As String

    _info = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1817).Split(":")
    _info0 = "* "
    _info1 = _info(0)
    If _info.Length > 1 Then
      _info0 = _info(0)
      _info1 = _info(1)
    End If

    If m_date_open_sessions Then
      PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(261), GLB_NLS_GUI_STATISTICS.GetString(345))
    Else
      PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(261), GLB_NLS_GUI_CONTROLS.GetString(331) & " - " & m_date_filter)
      PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(257), m_date_from)
      PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(258), m_date_to)
      PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(394), m_status)
    End If

    If m_is_multi_currency Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1771), m_site)
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6282), uc_multi_currency_select.GetTextCurrenciesSelected(m_currency_option))
      PrintExchangeCurrency(PrintData)
    Else
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1771), uc_site_select.GetSitesIdListSelectedToPrint()) '' SiteID
    End If

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(230), m_card_account)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(212), m_card_track_data)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(235), m_card_holder_name)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4802), m_card_holder_is_vip)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(369), m_options_value)
    PrintData.SetFilter(_info0, _info1)

    PrintData.FilterValueWidth(1) = 1800
    PrintData.FilterHeaderWidth(2) = 800
    PrintData.FilterValueWidth(2) = 2500
    PrintData.FilterHeaderWidth(3) = 1000
    PrintData.FilterValueWidth(3) = 2500
    PrintData.FilterHeaderWidth(4) = 2000
    PrintData.FilterValueWidth(4) = 3000



  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_date_from = ""
    m_date_to = ""
    m_status = ""
    m_card_account = ""
    m_card_track_data = ""
    m_card_holder_name = ""
    m_card_holder_is_vip = ""
    m_options_value = ""
    m_site = ""
    m_currency_option = ""

    ' Site
    If Not String.IsNullOrEmpty(uc_site_select.GetSitesIdListSelected()) Then
      m_site = uc_site_select.GetSitesIdListSelectedToPrint()
    End If

    'Site- MultiCurrency
    If CurrencyMultisite.IsEnabledMultiCurrency Then
      m_site = uc_multi_currency_select.GetSitesIdListSelectedToPrint()
      m_currency_option = uc_multi_currency_select.IsGridApplyChanges
    End If

    'Date 
    m_date_open_sessions = Me.rb_open_sessions.Checked
    If Me.rb_ini_date.Checked Then
      m_date_filter = GLB_NLS_GUI_STATISTICS.GetString(381)
    End If
    If Me.rb_finish_date.Checked Then
      m_date_filter = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5704)
    End If
    If Me.dtp_from.Checked Then
      m_date_from = GUI_FormatDate(dtp_from.Value, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If
    If Me.dtp_to.Checked Then
      m_date_to = GUI_FormatDate(dtp_to.Value, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    ' Status
    If Me.opt_all_status.Checked Then
      m_status = Me.opt_all_status.Text
    Else
      m_status = Me.cmb_status.TextValue
    End If

    ' Account
    m_card_account = Me.uc_account_sel1.Account()
    m_card_track_data = Me.uc_account_sel1.TrackData()
    m_card_holder_name = Me.uc_account_sel1.Holder()
    m_card_holder_is_vip = Me.uc_account_sel1.HolderIsVip()

    ' Options
    ' Only Gap Sessions
    m_options_value = chk_only_gap_sessions.Text & ": "
    If chk_only_gap_sessions.Checked Then
      m_options_value = m_options_value & GLB_NLS_GUI_INVOICING.GetString(479)
    Else
      m_options_value = m_options_value & GLB_NLS_GUI_INVOICING.GetString(480)
    End If
    ' Grouped
    m_options_value = m_options_value & ", " & chk_grouped.Text & ": "
    If Me.chk_grouped.Checked Then
      ' By provider or account
      If Me.opt_provider.Checked Then
        m_options_value = m_options_value & Me.opt_provider.Text
      ElseIf Me.opt_account.Checked Then
        m_options_value = m_options_value & Me.opt_account.Text
      End If
      ' Show subtotal
      m_options_value = m_options_value & ", " & chk_subtotal.Text & ": "
      If chk_subtotal.Checked Then
        m_options_value = m_options_value & GLB_NLS_GUI_INVOICING.GetString(479)
      Else
        m_options_value = m_options_value & GLB_NLS_GUI_INVOICING.GetString(480)
      End If
      ' Show sessions
      m_options_value = m_options_value & ", " & chk_show_sessions.Text & ": "
      If chk_show_sessions.Checked Then
        m_options_value = m_options_value & GLB_NLS_GUI_INVOICING.GetString(479)
      Else
        m_options_value = m_options_value & GLB_NLS_GUI_INVOICING.GetString(480)
      End If
    Else
      m_options_value = m_options_value & GLB_NLS_GUI_INVOICING.GetString(480)
    End If

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()
    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' INDEX
      .Column(GRID_COLUMN_INDEX).Header(0).Text = " "
      .Column(GRID_COLUMN_INDEX).Header(1).Text = " "
      .Column(GRID_COLUMN_INDEX).Width = 200
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Session Id
      .Column(GRID_COLUMN_SESSION_ID).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(391)
      .Column(GRID_COLUMN_SESSION_ID).Header(1).Text = ""
      .Column(GRID_COLUMN_SESSION_ID).Width = 0

      ' Started date
      .Column(GRID_COLUMN_STARTED).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(393)
      .Column(GRID_COLUMN_STARTED).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(381)
      .Column(GRID_COLUMN_STARTED).Width = GRID_WIDTH_DATE
      .Column(GRID_COLUMN_STARTED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Status
      .Column(GRID_COLUMN_STATUS).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(393)
      .Column(GRID_COLUMN_STATUS).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(394)
      .Column(GRID_COLUMN_STATUS).Width = 2300
      .Column(GRID_COLUMN_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Time
      .Column(GRID_COLUMN_TIME).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(393)
      .Column(GRID_COLUMN_TIME).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(382)
      .Column(GRID_COLUMN_TIME).Width = GRID_WIDTH_TIME
      .Column(GRID_COLUMN_TIME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Finish date
      .Column(GRID_COLUMN_FINISHED).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(393)
      .Column(GRID_COLUMN_FINISHED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5704)
      .Column(GRID_COLUMN_FINISHED).Width = GRID_WIDTH_DATE
      .Column(GRID_COLUMN_FINISHED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Account Id
      .Column(GRID_COLUMN_ACCOUNT_ID).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_ACCOUNT_ID).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(231)
      .Column(GRID_COLUMN_ACCOUNT_ID).Width = 1150
      .Column(GRID_COLUMN_ACCOUNT_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Holder Name
      .Column(GRID_COLUMN_HOLDER_NAME).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_HOLDER_NAME).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(235)
      .Column(GRID_COLUMN_HOLDER_NAME).Width = GRID_WIDTH_CARD_ID
      .Column(GRID_COLUMN_HOLDER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Card Id
      .Column(GRID_COLUMN_CARD_ID).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(391)
      .Column(GRID_COLUMN_CARD_ID).Header(1).Text = ""
      .Column(GRID_COLUMN_CARD_ID).Width = 0

      '  Provider Id
      .Column(GRID_COLUMN_PROVIDER_ID).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(232)
      .Column(GRID_COLUMN_PROVIDER_ID).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(315)
      .Column(GRID_COLUMN_PROVIDER_ID).Width = GRID_WIDTH_PROVIDER
      .Column(GRID_COLUMN_PROVIDER_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      '  Terminal Id
      .Column(GRID_COLUMN_TERMINAL_ID).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(232)
      .Column(GRID_COLUMN_TERMINAL_ID).Header(1).Text = GLB_NLS_GUI_CONTROLS.GetString(323)
      .Column(GRID_COLUMN_TERMINAL_ID).Width = GRID_WIDTH_TERMINAL
      .Column(GRID_COLUMN_TERMINAL_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      '  Terminal Type
      .Column(GRID_COLUMN_TERMINAL_TYPE).Header(0).Text = ""
      .Column(GRID_COLUMN_TERMINAL_TYPE).Header(1).Text = ""
      .Column(GRID_COLUMN_TERMINAL_TYPE).Width = 0

      '  Terminal Type Name
      .Column(GRID_COLUMN_TERMINAL_TYPE_NAME).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(232)
      .Column(GRID_COLUMN_TERMINAL_TYPE_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1113)
      .Column(GRID_COLUMN_TERMINAL_TYPE_NAME).Width = 0 'GRID_WIDTH_TERMINAL
      .Column(GRID_COLUMN_TERMINAL_TYPE_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Redeemable Initial Balance
      .Column(GRID_COLUMN_RE_INITIAL_BALANCE).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(337)
      .Column(GRID_COLUMN_RE_INITIAL_BALANCE).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(384)
      .Column(GRID_COLUMN_RE_INITIAL_BALANCE).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_RE_INITIAL_BALANCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Redeemable Played Amount
      .Column(GRID_COLUMN_RE_PLAYED_AMOUNT).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(337)
      .Column(GRID_COLUMN_RE_PLAYED_AMOUNT).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(385)
      .Column(GRID_COLUMN_RE_PLAYED_AMOUNT).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_RE_PLAYED_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Redeemable Won Amount
      .Column(GRID_COLUMN_RE_WON_AMOUNT).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(337)
      .Column(GRID_COLUMN_RE_WON_AMOUNT).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(386)
      .Column(GRID_COLUMN_RE_WON_AMOUNT).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_RE_WON_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Redeemable Final Balance
      .Column(GRID_COLUMN_RE_FINAL_BALANCE).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(337)
      .Column(GRID_COLUMN_RE_FINAL_BALANCE).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(389)
      .Column(GRID_COLUMN_RE_FINAL_BALANCE).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_RE_FINAL_BALANCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Redeemable Netwin
      .Column(GRID_COLUMN_RE_NETWIN).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(337)
      .Column(GRID_COLUMN_RE_NETWIN).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(390)
      .Column(GRID_COLUMN_RE_NETWIN).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_RE_NETWIN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Redeemable Ticket In
      .Column(GRID_COLUMN_REDEEMABLE_TICKET_IN).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(337)
      .Column(GRID_COLUMN_REDEEMABLE_TICKET_IN).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2921)
      .Column(GRID_COLUMN_REDEEMABLE_TICKET_IN).Width = IIf(m_is_tito_mode, GRID_WIDTH_AMOUNT, 0)
      .Column(GRID_COLUMN_REDEEMABLE_TICKET_IN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Redeemable Promotional Ticket In
      .Column(GRID_COLUMN_PROMO_RE_TICKET_IN).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(337)
      .Column(GRID_COLUMN_PROMO_RE_TICKET_IN).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2922)
      .Column(GRID_COLUMN_PROMO_RE_TICKET_IN).Width = IIf(m_is_tito_mode, GRID_WIDTH_AMOUNT_LONG, 0)
      .Column(GRID_COLUMN_PROMO_RE_TICKET_IN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Bill In
      .Column(GRID_COLUMN_BILLS_IN_AMOUNT).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(337)
      .Column(GRID_COLUMN_BILLS_IN_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2923)
      .Column(GRID_COLUMN_BILLS_IN_AMOUNT).Width = IIf(m_is_tito_mode, GRID_WIDTH_AMOUNT, 0)
      .Column(GRID_COLUMN_BILLS_IN_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Redeemable Ticket Out
      .Column(GRID_COLUMN_REDEEMABLE_TICKET_OUT).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(337)
      .Column(GRID_COLUMN_REDEEMABLE_TICKET_OUT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2924)
      .Column(GRID_COLUMN_REDEEMABLE_TICKET_OUT).Width = IIf(m_is_tito_mode, GRID_WIDTH_AMOUNT, 0)
      .Column(GRID_COLUMN_REDEEMABLE_TICKET_OUT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Not Redeemable Initial Balance
      .Column(GRID_COLUMN_NRE_INITIAL_BALANCE).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(338)
      .Column(GRID_COLUMN_NRE_INITIAL_BALANCE).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(384)
      .Column(GRID_COLUMN_NRE_INITIAL_BALANCE).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_NRE_INITIAL_BALANCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Not Redeemable Played Amount
      .Column(GRID_COLUMN_NRE_PLAYED_AMOUNT).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(338)
      .Column(GRID_COLUMN_NRE_PLAYED_AMOUNT).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(385)
      .Column(GRID_COLUMN_NRE_PLAYED_AMOUNT).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_NRE_PLAYED_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Not Redeemable Won Amount
      .Column(GRID_COLUMN_NRE_WON_AMOUNT).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(338)
      .Column(GRID_COLUMN_NRE_WON_AMOUNT).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(386)
      .Column(GRID_COLUMN_NRE_WON_AMOUNT).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_NRE_WON_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Not Redeemable Final Balance
      .Column(GRID_COLUMN_NRE_FINAL_BALANCE).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(338)
      .Column(GRID_COLUMN_NRE_FINAL_BALANCE).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(389)
      .Column(GRID_COLUMN_NRE_FINAL_BALANCE).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_NRE_FINAL_BALANCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Not Redeemable Netwin
      .Column(GRID_COLUMN_NRE_NETWIN).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(338)
      .Column(GRID_COLUMN_NRE_NETWIN).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(390)
      .Column(GRID_COLUMN_NRE_NETWIN).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_NRE_NETWIN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Not Redeemable Promo Ticket In
      .Column(GRID_COLUMN_PROMO_NONRE_TICKET_IN).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(338)
      .Column(GRID_COLUMN_PROMO_NONRE_TICKET_IN).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2922)
      .Column(GRID_COLUMN_PROMO_NONRE_TICKET_IN).Width = IIf(m_is_tito_mode, GRID_WIDTH_AMOUNT_LONG, 0)
      .Column(GRID_COLUMN_PROMO_NONRE_TICKET_IN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Not Redeemable Promo Ticket Out
      .Column(GRID_COLUMN_PROMO_NONRE_TICKET_OUT).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(338)
      .Column(GRID_COLUMN_PROMO_NONRE_TICKET_OUT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2925)
      .Column(GRID_COLUMN_PROMO_NONRE_TICKET_OUT).Width = IIf(m_is_tito_mode, GRID_WIDTH_AMOUNT_LONG, 0)
      .Column(GRID_COLUMN_PROMO_NONRE_TICKET_OUT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Total Initial Balance
      .Column(GRID_COLUMN_TOTAL_CASH_IN).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(391)
      .Column(GRID_COLUMN_TOTAL_CASH_IN).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1905) ' Initial*
      .Column(GRID_COLUMN_TOTAL_CASH_IN).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_TOTAL_CASH_IN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Total Played Amount
      .Column(GRID_COLUMN_PLAYED_AMOUNT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(391)
      .Column(GRID_COLUMN_PLAYED_AMOUNT).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(385)
      .Column(GRID_COLUMN_PLAYED_AMOUNT).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_PLAYED_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Total Won Amount
      .Column(GRID_COLUMN_WON_AMOUNT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(391)
      .Column(GRID_COLUMN_WON_AMOUNT).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(386)
      .Column(GRID_COLUMN_WON_AMOUNT).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_WON_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Total Final Balance
      .Column(GRID_COLUMN_TOTAL_CASH_OUT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(391)
      .Column(GRID_COLUMN_TOTAL_CASH_OUT).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(389)
      .Column(GRID_COLUMN_TOTAL_CASH_OUT).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_TOTAL_CASH_OUT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Total Netwin
      .Column(GRID_COLUMN_NETWIN).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(391)
      .Column(GRID_COLUMN_NETWIN).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(390)
      .Column(GRID_COLUMN_NETWIN).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_NETWIN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Total Ticket In
      .Column(GRID_COLUMN_TOTAL_TICKET_IN).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(391)
      .Column(GRID_COLUMN_TOTAL_TICKET_IN).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2921)
      .Column(GRID_COLUMN_TOTAL_TICKET_IN).Width = IIf(m_is_tito_mode, GRID_WIDTH_AMOUNT, 0)
      .Column(GRID_COLUMN_TOTAL_TICKET_IN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Bill In
      .Column(GRID_COLUMN_BILLS_IN_AMOUNT_TOTAL).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(391)
      .Column(GRID_COLUMN_BILLS_IN_AMOUNT_TOTAL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2923)
      .Column(GRID_COLUMN_BILLS_IN_AMOUNT_TOTAL).Width = IIf(m_is_tito_mode, GRID_WIDTH_AMOUNT, 0)
      .Column(GRID_COLUMN_BILLS_IN_AMOUNT_TOTAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Total Ticket Out
      .Column(GRID_COLUMN_TOTAL_TICKET_OUT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(391)
      .Column(GRID_COLUMN_TOTAL_TICKET_OUT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2924)
      .Column(GRID_COLUMN_TOTAL_TICKET_OUT).Width = IIf(m_is_tito_mode, GRID_WIDTH_AMOUNT, 0)
      .Column(GRID_COLUMN_TOTAL_TICKET_OUT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Cash In on Play Session
      .Column(GRID_COLUMN_CASH_IN_PLAY_SESSION).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(391)
      .Column(GRID_COLUMN_CASH_IN_PLAY_SESSION).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(387)
      .Column(GRID_COLUMN_CASH_IN_PLAY_SESSION).Width = IIf(m_is_tito_mode, 0, 1350)
      .Column(GRID_COLUMN_CASH_IN_PLAY_SESSION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Promotional
      .Column(GRID_COLUMN_PROMOTIONAL).Header(0).Text = "" ' GLB_NLS_GUI_STATISTICS.GetString(391)
      .Column(GRID_COLUMN_PROMOTIONAL).Header(1).Text = "" ' GLB_NLS_GUI_STATISTICS.GetString(392)
      .Column(GRID_COLUMN_PROMOTIONAL).Width = 0 ' GRID_WIDTH_PROMO
      .Column(GRID_COLUMN_PROMOTIONAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Played Count
      .Column(GRID_COLUMN_PLAYED_COUNT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(304)
      .Column(GRID_COLUMN_PLAYED_COUNT).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(304)
      .Column(GRID_COLUMN_PLAYED_COUNT).Width = GRID_WIDTH_PLAYS
      .Column(GRID_COLUMN_PLAYED_COUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Won Count
      .Column(GRID_COLUMN_WON_COUNT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(304)
      .Column(GRID_COLUMN_WON_COUNT).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(305)
      .Column(GRID_COLUMN_WON_COUNT).Width = GRID_WIDTH_PLAYS
      .Column(GRID_COLUMN_WON_COUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' SiteId
      .Column(GRID_COLUMN_SITE_ID).Header(0).Text = ""
      .Column(GRID_COLUMN_SITE_ID).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1927) ' SiteId
      .Column(GRID_COLUMN_SITE_ID).Width = 850
      .Column(GRID_COLUMN_SITE_ID).PrintWidth = 850
      .Column(GRID_COLUMN_SITE_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    End With
  End Sub ' GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()
    Dim _today_opening As Date

    If m_is_multi_currency Then
      uc_multi_currency_select.SetDefaultValues(False)
    End If

    _today_opening = WSI.Common.Misc.TodayOpening()

    Me.dtp_to.Value = _today_opening.AddDays(1)
    Me.dtp_to.Checked = True

    Me.dtp_from.Value = _today_opening
    Me.dtp_from.Checked = True

    Me.rb_historic.Checked = True
    Me.rb_finish_date.Checked = True
    Me.rb_open_sessions.Checked = False
    Me.rb_ini_date.Checked = False
    Me.dtp_from.Enabled = True
    Me.dtp_to.Enabled = True
    Me.gb_status.Enabled = True

    Me.cmb_status.Enabled = False
    Me.opt_all_status.Checked = True

    Me.chk_only_gap_sessions.Checked = False
    Me.chk_grouped.Checked = False
    Me.opt_account.Checked = True
    Me.chk_subtotal.Checked = True
    Me.chk_show_sessions.Checked = False
    chk_grouped_CheckedChanged(Nothing, Nothing)

    m_grouped_by = ENUM_GROUPED.NOT_GROUPED
    m_last_grouped_by = ENUM_GROUPED.NOT_GROUPED

    Me.uc_account_sel1.Clear()

    If m_is_multi_currency Then
      Me.uc_multi_currency_select.SetDefaultValues(False)
    End If

  End Sub ' SetDefaultValues

  ' PURPOSE: Get Sql WHERE to buil SQL Query
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetSqlWhere() As String
    Dim _str_where As StringBuilder
    Dim _str_filter_ac As String
    Dim _str_filter_site As String
    Dim _str_index As String

    _str_where = New StringBuilder()
    _str_filter_ac = String.Empty
    _str_filter_site = String.Empty
    _str_index = GetSQLIndex()

    _str_where.AppendLine("SELECT   PS_PLAY_SESSION_ID ")
    _str_where.AppendLine("       , PS_STARTED ")
    _str_where.AppendLine("       , PS_FINISHED ")
    _str_where.AppendLine("       , TE_PROVIDER_ID ")
    _str_where.AppendLine("       , TE_NAME ")
    _str_where.AppendLine("       , PS_TERMINAL_ID ")
    _str_where.AppendLine("       , PS_TOTAL_CASH_IN ")
    _str_where.AppendLine("       , PS_PLAYED_AMOUNT ")
    _str_where.AppendLine("       , PS_WON_AMOUNT ")
    _str_where.AppendLine("       , PS_PLAYED_COUNT ")
    _str_where.AppendLine("       , PS_WON_COUNT ")
    _str_where.AppendLine("       , PS_CASH_IN ")
    _str_where.AppendLine("       , PS_TOTAL_CASH_OUT ")
    _str_where.AppendLine("       , PS_STATUS ")
    _str_where.AppendLine("       , AC_ACCOUNT_ID ")
    _str_where.AppendLine("       , AC_TYPE ")
    _str_where.AppendLine("       , AC_TRACK_DATA ")
    _str_where.AppendLine("       , PS_PROMO ")
    _str_where.AppendLine("       , AC_HOLDER_NAME ")
    _str_where.AppendLine("       , TE_TERMINAL_TYPE ")
    _str_where.AppendLine("       , PS_TYPE ")
    _str_where.AppendLine("       , PS_REDEEMABLE_CASH_IN ")
    _str_where.AppendLine("       , PS_REDEEMABLE_PLAYED ")
    _str_where.AppendLine("       , PS_REDEEMABLE_WON ")
    _str_where.AppendLine("       , PS_REDEEMABLE_CASH_OUT ")
    _str_where.AppendLine("       , PS_NON_REDEEMABLE_CASH_IN ")
    _str_where.AppendLine("       , PS_NON_REDEEMABLE_PLAYED ")
    _str_where.AppendLine("       , PS_NON_REDEEMABLE_WON ")
    _str_where.AppendLine("       , PS_NON_REDEEMABLE_CASH_OUT ")
    _str_where.AppendLine("       , PS_SITE_ID ")
    If m_is_multi_currency AndAlso uc_multi_currency_select.HasSitesWithoutCurrency = False Then
      _str_where.AppendLine("	      , SC_ISO_CODE")
    End If

    If Me.m_is_tito_mode Then
      _str_where.AppendLine("       , PS_RE_TICKET_IN ")
      _str_where.AppendLine("       , PS_PROMO_RE_TICKET_IN ")
      _str_where.AppendLine("       , PS_CASH_IN AS PS_BILLS_IN_AMOUNT ")
      _str_where.AppendLine("       , PS_RE_TICKET_OUT ")
      _str_where.AppendLine("       , PS_PROMO_NR_TICKET_IN ")
      _str_where.AppendLine("       , PS_PROMO_NR_TICKET_OUT ")
      _str_where.AppendLine("       , PS_AUX_FT_RE_CASH_IN ")
      _str_where.AppendLine("       , PS_AUX_FT_NR_CASH_IN ")
    End If

    _str_where.AppendLine("  FROM   PLAY_SESSIONS " & _str_index)
    _str_where.AppendLine("  LEFT   JOIN ACCOUNTS ON PS_ACCOUNT_ID  = AC_ACCOUNT_ID ")
    _str_where.AppendLine("  INNER  JOIN TERMINALS ON TE_TERMINAL_ID = PS_TERMINAL_ID AND PS_SITE_ID = TE_SITE_ID    ")
    If m_is_multi_currency AndAlso uc_multi_currency_select.HasSitesWithoutCurrency = False Then
      _str_where.AppendLine("	    LEFT   JOIN SITE_CURRENCIES   ON   play_sessions.ps_site_id = site_currencies.sc_site_id ")
      _str_where.AppendLine("	                                       AND   SC_TYPE = 1   ")
    End If


    ' Filter by Siteid
    If m_is_multi_currency Then
      _str_filter_site = uc_multi_currency_select.GetSitesIdListSelectedAll()
    Else
      _str_filter_site = uc_site_select.GetSitesIdListSelected()
    End If
    If _str_filter_site <> String.Empty Then
      _str_where.AppendLine(" WHERE PS_SITE_ID IN (" & _str_filter_site & ") ")
    End If

    ' Filter Open Sessions / Historic sessions
    If Me.rb_open_sessions.Checked Then
      _str_where.AppendLine(" AND PS_STATUS = " & WSI.Common.PlaySessionStatus.Opened)
    Else
      ' Filter Dates
      If Me.rb_ini_date.Checked Then
        If Me.dtp_from.Checked = True Then
          _str_where.AppendLine(" AND (PS_STARTED >= " & GUI_FormatDateDB(dtp_from.Value) & ") ")
        End If

        If Me.dtp_to.Checked = True Then
          _str_where.AppendLine(" AND (PS_STARTED < " & GUI_FormatDateDB(dtp_to.Value) & ") ")
        End If
      Else
        If Me.dtp_from.Checked = True Then
          _str_where.AppendLine(" AND (PS_FINISHED >= " & GUI_FormatDateDB(dtp_from.Value) & ") ")
        End If

        If Me.dtp_to.Checked = True Then
          _str_where.AppendLine(" AND (PS_FINISHED < " & GUI_FormatDateDB(dtp_to.Value) & ") ")
        End If
      End If
    End If

    ' Filter Status
    If Me.opt_several_status.Checked Then
      _str_where.AppendLine(" AND PS_STATUS = " & Me.cmb_status.Value)
    End If

    ' RCI 13-DEC-2010: Only Gap Sessions
    If Me.chk_only_gap_sessions.Checked Then
      ' FJC 04-NOV-2014  Fixed Bug WIG-1631
      '_str_where.AppendLine(" AND ROUND (PS_PLAYED_AMOUNT -  PS_WON_AMOUNT, 2) <> ROUND ( PS_INITIAL_BALANCE + PS_CASH_IN - PS_FINAL_BALANCE, 2) ")
      _str_where.AppendLine(" AND ROUND (PS_PLAYED_AMOUNT -  PS_WON_AMOUNT, 2) <> ROUND (PS_TOTAL_CASH_IN - PS_TOTAL_CASH_OUT, 2)")

      _str_where.AppendLine(" AND PS_TYPE NOT IN(" & WSI.Common.PlaySessionType.PROGRESSIVE_PROVISION & ",")
      _str_where.AppendLine(WSI.Common.PlaySessionType.PROGRESSIVE_RESERVE & ") ")
    End If

    _str_filter_ac = Me.uc_account_sel1.GetFilterSQL()
    If _str_filter_ac <> "" Then
      _str_where.AppendLine(" AND " & _str_filter_ac)
    End If

    Return _str_where.ToString()
  End Function ' GetSqlWhere

  ' PURPOSE: Check conditions to set the "better" index for the query
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - The string to use the index for the SQL query
  Private Function GetSQLIndex() As String
    Dim _str_index As String

    _str_index = " WITH (INDEX(IX_ps_started_terminal_id_site_id)) "

    If Me.rb_finish_date.Checked Then
      _str_index = " WITH (INDEX(IX_ps_finished_terminal_id_site_id)) "
    End If

    Return _str_index
  End Function


  ' PURPOSE: Checks whether the specified row has valid data or not
  '
  '  PARAMS:
  '     - INPUT:
  '           - IdxRow: row to check
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - TRUE: selected row contains valid data
  '     - FALSE: selected row does not exist or contains no valid data
  '
  Private Function IsValidDataRow(ByVal IdxRow As Integer) As Boolean

    If IdxRow >= 0 And IdxRow < Me.Grid.NumRows Then
      ' All rows with data have a non-empty value in column SESSION_ID.

      Return Not String.IsNullOrEmpty(Me.Grid.Cell(IdxRow, GRID_COLUMN_SESSION_ID).Value)
    End If

    Return False
  End Function ' IsValidDataRow

  ' PURPOSE : Convert the CardNumber into a encrypted track data if account type is WIN.
  '
  '  PARAMS :
  '     - INPUT : 
  '           - AccountType As Integer
  '           - CardNumber As String
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - String
  '
  Private Function TrackDataToExternal(ByVal AccountType As Integer, ByVal CardNumber As String) As String
    Dim _track_data As String

    ' Screen must always show the encrypted card number
    ' Internal Identifier must be converted prior to showing it
    _track_data = ""
    ' WIN accounts must follow encryption formats
    If AccountType = ACCOUNT_TYPE_WIN Then
      If Not WSI.Common.CardNumber.TrackDataToExternal(_track_data, CardNumber, MAGNETIC_CARD_TYPES.CARD_TYPE_PLAYER) Then
        _track_data = "* * * * * * * *"
      End If
    Else
      _track_data = CardNumber
    End If

    Return _track_data
  End Function ' TrackDataToExternal

  ' PURPOSE : Process total counters: Draw row of totals, update and reset total counters.
  '
  '  PARAMS :
  '     - INPUT : 
  '           - DbRow As CLASS_DB_ROW
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '
  Private Sub ProcessCounters(ByVal DbRow As CLASS_DB_ROW)
    Dim _terminal As String
    Dim _terminal_type As String
    Dim _provider As String
    Dim _provider_terminal As String
    Dim _account As String
    Dim _track_data As String

    UpdateTotalCounters(m_global_total, DbRow)

    If m_grouped_by = ENUM_GROUPED.NOT_GROUPED Then

      Return
    End If

    _account = " "
    _track_data = " "
    ' Account Id - Track Data - Holder Name
    ' RCI 09-OCT-2014: Take in account NULL Account values
    If Not DbRow.IsNull(SQL_COLUMN_HOLDER_NAME) Then
      _account = DbRow.Value(SQL_COLUMN_HOLDER_NAME)
    End If
    If Not DbRow.IsNull(SQL_COLUMN_ACCOUNT_TYPE) AndAlso Not DbRow.IsNull(SQL_COLUMN_CARD_ID) Then
      _track_data = TrackDataToExternal(DbRow.Value(SQL_COLUMN_ACCOUNT_TYPE), DbRow.Value(SQL_COLUMN_CARD_ID))
      If Not DbRow.IsNull(SQL_COLUMN_ACCOUNT_ID) Then
        _account = DbRow.Value(SQL_COLUMN_ACCOUNT_ID) & vbCr & _track_data & vbCr & _account
      End If
    End If

    ' Terminal and Provider Name
    _terminal = DbRow.Value(SQL_COLUMN_TERMINAL_NAME)
    _terminal_type = GetTerminalTypeNls(DbRow.Value(SQL_COLUMN_TERMINAL_TYPE))
    _provider = DbRow.Value(SQL_COLUMN_PROVIDER_NAME)
    _provider = _provider.ToUpper()

    ' Add Provider to Terminal
    _provider_terminal = _provider & vbCr & _terminal & vbCr & _terminal_type

    If m_grouped_by = ENUM_GROUPED.BY_ACCOUNT Then
      ' Add Account to Provider if grouping by account
      _provider = _provider & vbCr & _account
    End If

    If m_first_time Then
      m_first_time = False
      ResetTotalCounters(m_terminal_total, _provider_terminal)
      ResetTotalCounters(m_provider_total, _provider)
      ResetTotalCounters(m_account_total, _account)
    End If

    Select Case m_grouped_by
      Case ENUM_GROUPED.BY_PROVIDER
        If Not m_provider_total.key.Equals(_provider) Then
          DrawTotalRow(m_terminal_total, ENUM_GRID_ROW_TYPE.TOTAL_TERMINAL)
          DrawTotalRow(m_provider_total, ENUM_GRID_ROW_TYPE.TOTAL_PROVIDER)
          ' It's a new provider... Reset totals for provider and for terminal.
          ResetTotalCounters(m_terminal_total, _provider_terminal)
          ResetTotalCounters(m_provider_total, _provider)
        ElseIf Not m_terminal_total.key.Equals(_provider_terminal) Then
          DrawTotalRow(m_terminal_total, ENUM_GRID_ROW_TYPE.TOTAL_TERMINAL)
          ' It's a new terminal... Reset totals.
          ResetTotalCounters(m_terminal_total, _provider_terminal)
        End If

        UpdateTotalCounters(m_terminal_total, DbRow)
        UpdateTotalCounters(m_provider_total, DbRow)

      Case ENUM_GROUPED.BY_ACCOUNT
        If Not m_account_total.key.Equals(_account) Then
          DrawTotalRow(m_provider_total, ENUM_GRID_ROW_TYPE.TOTAL_PROVIDER)
          DrawTotalRow(m_account_total, ENUM_GRID_ROW_TYPE.TOTAL_ACCOUNT)
          ' It's a new account... Reset totals for account and for provider.
          ResetTotalCounters(m_provider_total, _provider)
          ResetTotalCounters(m_account_total, _account)

        ElseIf Not m_provider_total.key.Equals(_provider) Then
          DrawTotalRow(m_provider_total, ENUM_GRID_ROW_TYPE.TOTAL_PROVIDER)
          ' It's a new provider... Reset totals.
          ResetTotalCounters(m_provider_total, _provider)
        End If

        UpdateTotalCounters(m_provider_total, DbRow)
        UpdateTotalCounters(m_account_total, DbRow)

      Case ENUM_GROUPED.NOT_GROUPED
      Case Else
    End Select

  End Sub ' ProcessCounters

  ' PURPOSE : Add and draw a total row.
  '
  '  PARAMS :
  '     - INPUT : 
  '           - Total As TYPE_TOTAL_COUNTERS
  '           - Type As ENUM_GRID_ROW_TYPE
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '
  Private Sub DrawTotalRow(ByVal Total As TYPE_TOTAL_COUNTERS, ByVal Type As ENUM_GRID_ROW_TYPE)
    Dim _idx_row As Integer
    Dim _str_key As String
    Dim _str_key_2 As String
    Dim _idx_key As String
    Dim _idx_key_2 As String
    Dim _str_split As String()
    Dim _color As ENUM_GUI_COLOR

    If Not Me.chk_subtotal.Checked Then
      Select Case m_grouped_by
        Case ENUM_GROUPED.BY_PROVIDER
          If Type = ENUM_GRID_ROW_TYPE.TOTAL_TERMINAL Then

            Return
          End If

        Case ENUM_GROUPED.BY_ACCOUNT
          If Type = ENUM_GRID_ROW_TYPE.TOTAL_PROVIDER Then

            Return
          End If
      End Select
    End If

    _str_key = ""
    _idx_key = 0
    _str_key_2 = ""
    _idx_key_2 = 0

    Me.Grid.AddRow()
    _idx_row = Me.Grid.NumRows - 1

    Select Case Type
      Case ENUM_GRID_ROW_TYPE.TOTAL_PROVIDER
        _idx_key = GRID_COLUMN_PROVIDER_ID

        Select Case m_grouped_by
          Case ENUM_GROUPED.BY_PROVIDER
            _str_key = Total.key
            _color = ENUM_GUI_COLOR.GUI_COLOR_GREEN_01

          Case ENUM_GROUPED.BY_ACCOUNT
            _str_split = Total.key.Split(vbCr)
            _str_key = _str_split(0)
            If _str_split.Length > 3 Then
              Me.Grid.Cell(_idx_row, GRID_COLUMN_ACCOUNT_ID).Value = _str_split(1)
              Me.Grid.Cell(_idx_row, GRID_COLUMN_CARD_ID).Value = _str_split(2)
              Me.Grid.Cell(_idx_row, GRID_COLUMN_HOLDER_NAME).Value = _str_split(3)
            End If
            _color = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01
        End Select

      Case ENUM_GRID_ROW_TYPE.TOTAL_TERMINAL
        _str_split = Total.key.Split(vbCr)
        If _str_split.Length > 1 Then
          Me.Grid.Cell(_idx_row, GRID_COLUMN_PROVIDER_ID).Value = _str_split(0)
          _str_key = _str_split(1)
          _str_key_2 = _str_split(2)
        Else
          _str_key = _str_split(0)
        End If
        _idx_key = GRID_COLUMN_TERMINAL_ID
        _idx_key_2 = GRID_COLUMN_TERMINAL_TYPE_NAME
        _color = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01

      Case ENUM_GRID_ROW_TYPE.TOTAL_ACCOUNT
        _str_split = Total.key.Split(vbCr)
        If _str_split.Length > 2 Then
          _str_key = _str_split(2)
        Else
          _str_key = _str_split(0)
        End If
        _idx_key = GRID_COLUMN_HOLDER_NAME
        _color = ENUM_GUI_COLOR.GUI_COLOR_GREEN_01
        If _str_split.Length > 1 Then
          Me.Grid.Cell(_idx_row, GRID_COLUMN_ACCOUNT_ID).Value = _str_split(0)
          Me.Grid.Cell(_idx_row, GRID_COLUMN_CARD_ID).Value = _str_split(1)
        End If

      Case ENUM_GRID_ROW_TYPE.TOTAL
        _str_key = GLB_NLS_GUI_INVOICING.GetString(205)
        _idx_key = GRID_COLUMN_PROVIDER_ID
        _color = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00
    End Select

    Me.Grid.Row(_idx_row).BackColor = GetColor(_color)

    Me.Grid.Cell(_idx_row, _idx_key).Value = _str_key
    If _str_key_2 <> "" Then
      Me.Grid.Cell(_idx_row, _idx_key_2).Value = _str_key_2
    End If

    Me.Grid.Cell(_idx_row, GRID_COLUMN_PLAYED_AMOUNT).Value = GUI_MultiCurrencyValue_Format(Total.played_amount)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_WON_AMOUNT).Value = GUI_MultiCurrencyValue_Format(Total.won_amount)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_TOTAL_CASH_IN).Value = GUI_MultiCurrencyValue_Format(Total.initial_balance)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_TOTAL_CASH_OUT).Value = GUI_MultiCurrencyValue_Format(Total.fin_balance)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_NETWIN).Value = GUI_MultiCurrencyValue_Format(Total.initial_balance - Total.fin_balance)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_RE_INITIAL_BALANCE).Value = GUI_MultiCurrencyValue_Format(Total.re_initial_balance)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_RE_PLAYED_AMOUNT).Value = GUI_MultiCurrencyValue_Format(Total.re_played_amount)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_RE_WON_AMOUNT).Value = GUI_MultiCurrencyValue_Format(Total.re_won_amount)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_RE_FINAL_BALANCE).Value = GUI_MultiCurrencyValue_Format(Total.re_fin_balance)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_RE_NETWIN).Value = GUI_MultiCurrencyValue_Format(Total.re_initial_balance - Total.re_fin_balance)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_NRE_INITIAL_BALANCE).Value = GUI_MultiCurrencyValue_Format(Total.nre_initial_balance)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_NRE_PLAYED_AMOUNT).Value = GUI_MultiCurrencyValue_Format(Total.nre_played_amount)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_NRE_WON_AMOUNT).Value = GUI_MultiCurrencyValue_Format(Total.nre_won_amount)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_NRE_FINAL_BALANCE).Value = GUI_MultiCurrencyValue_Format(Total.nre_fin_balance)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_NRE_NETWIN).Value = GUI_MultiCurrencyValue_Format(Total.nre_initial_balance - Total.nre_fin_balance)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_PLAYED_COUNT).Value = GUI_FormatNumber(Total.played_count, ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_WON_COUNT).Value = GUI_FormatNumber(Total.won_count, ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)

    If Me.m_is_tito_mode Then
      Me.Grid.Cell(_idx_row, GRID_COLUMN_REDEEMABLE_TICKET_IN).Value = GUI_MultiCurrencyValue_Format(Total.redeemable_ticket_in)
      Me.Grid.Cell(_idx_row, GRID_COLUMN_PROMO_RE_TICKET_IN).Value = GUI_MultiCurrencyValue_Format(Total.redeemable_ticket_in_promo)
      Me.Grid.Cell(_idx_row, GRID_COLUMN_BILLS_IN_AMOUNT).Value = GUI_MultiCurrencyValue_Format(Total.bill_in)
      Me.Grid.Cell(_idx_row, GRID_COLUMN_REDEEMABLE_TICKET_OUT).Value = GUI_MultiCurrencyValue_Format(Total.redeemable_ticket_out)
      Me.Grid.Cell(_idx_row, GRID_COLUMN_PROMO_NONRE_TICKET_IN).Value = GUI_MultiCurrencyValue_Format(Total.not_redeemable_ticket_in)
      Me.Grid.Cell(_idx_row, GRID_COLUMN_PROMO_NONRE_TICKET_OUT).Value = GUI_MultiCurrencyValue_Format(Total.not_redeemable_ticket_out)
      Me.Grid.Cell(_idx_row, GRID_COLUMN_TOTAL_TICKET_IN).Value = GUI_MultiCurrencyValue_Format(Total.total_ticket_in)
      Me.Grid.Cell(_idx_row, GRID_COLUMN_BILLS_IN_AMOUNT_TOTAL).Value = GUI_MultiCurrencyValue_Format(Total.total_bill_in)
      Me.Grid.Cell(_idx_row, GRID_COLUMN_TOTAL_TICKET_OUT).Value = GUI_MultiCurrencyValue_Format(Total.total_ticket_out)
    Else
      Me.Grid.Cell(_idx_row, GRID_COLUMN_CASH_IN_PLAY_SESSION).Value = GUI_MultiCurrencyValue_Format(Total.cash_in_play_session)
    End If

  End Sub ' DrawTotalRow

  ' PURPOSE : Reset Total Counters variable
  '
  '  PARAMS :
  '     - INPUT : 
  '           - Total As TYPE_TOTAL_COUNTERS
  '           - Key As String
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '
  Private Sub ResetTotalCounters(ByRef Total As TYPE_TOTAL_COUNTERS, ByVal Key As String)
    Total.key = Key
    Total.played_amount = 0
    Total.won_amount = 0
    Total.initial_balance = 0
    Total.cash_in_play_session = 0
    Total.fin_balance = 0
    Total.re_initial_balance = 0
    Total.re_played_amount = 0
    Total.re_won_amount = 0
    Total.re_fin_balance = 0
    Total.nre_initial_balance = 0
    Total.nre_played_amount = 0
    Total.nre_won_amount = 0
    Total.nre_fin_balance = 0
    Total.played_count = 0
    Total.won_count = 0
    Total.redeemable_ticket_in = 0
    Total.redeemable_ticket_in_promo = 0
    Total.bill_in = 0
    Total.redeemable_ticket_out = 0
    Total.not_redeemable_ticket_in = 0
    Total.not_redeemable_ticket_out = 0
    Total.total_ticket_in = 0
    Total.total_bill_in = 0
    Total.total_ticket_out = 0
  End Sub ' ResetTotalCounters

  ' PURPOSE : Update total counters.
  '
  '  PARAMS :
  '     - INPUT :
  '           - Total As TYPE_TOTAL_COUNTERS
  '           - Amount As Decimal
  '           - IsPending As Boolean
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '
  Private Sub UpdateTotalCounters(ByRef Total As TYPE_TOTAL_COUNTERS, ByVal DbRow As CLASS_DB_ROW)

    With Total
      .played_amount = .played_amount + DbRow.Value(SQL_COLUMN_PLAYED_AMOUNT)
      .won_amount = .won_amount + DbRow.Value(SQL_COLUMN_WON_AMOUNT)
      .initial_balance = .initial_balance + DbRow.Value(SQL_COLUMN_TOTAL_CASH_IN)
      .cash_in_play_session = .cash_in_play_session + DbRow.Value(SQL_COLUMN_CASH_IN_PLAY_SESSION)
      .fin_balance = .fin_balance + DbRow.Value(SQL_COLUMN_TOTAL_CASH_OUT)
      .re_initial_balance = .re_initial_balance + DbRow.Value(SQL_COLUMN_RE_CASH_IN)
      .re_played_amount = .re_played_amount + DbRow.Value(SQL_COLUMN_RE_PLAYED_AMOUNT)
      .re_won_amount = .re_won_amount + DbRow.Value(SQL_COLUMN_RE_WON_AMOUNT)
      .re_fin_balance = .re_fin_balance + DbRow.Value(SQL_COLUMN_RE_CASH_OUT)
      .nre_initial_balance = .nre_initial_balance + DbRow.Value(SQL_COLUMN_NRE_CASH_IN)
      .nre_played_amount = .nre_played_amount + DbRow.Value(SQL_COLUMN_NRE_PLAYED_AMOUNT)
      .nre_won_amount = .nre_won_amount + DbRow.Value(SQL_COLUMN_NRE_WON_AMOUNT)
      .nre_fin_balance = .nre_fin_balance + DbRow.Value(SQL_COLUMN_NRE_CASH_OUT)
      .played_count = .played_count + DbRow.Value(SQL_COLUMN_PLAYED_COUNT)
      .won_count = .won_count + DbRow.Value(SQL_COLUMN_WON_COUNT)
      .redeemable_ticket_in += IIf(DbRow.IsNull(SQL_COLUMN_RE_TICKET_IN), 0, DbRow.Value(SQL_COLUMN_RE_TICKET_IN))
      .redeemable_ticket_in_promo += IIf(DbRow.IsNull(SQL_COLUMN_PROMO_RE_TICKET_IN), 0, DbRow.Value(SQL_COLUMN_PROMO_RE_TICKET_IN))
      .bill_in += IIf(DbRow.IsNull(SQL_COLUMN_BILLS_IN_AMOUNT), 0, DbRow.Value(SQL_COLUMN_BILLS_IN_AMOUNT))
      .redeemable_ticket_out += IIf(DbRow.IsNull(SQL_COLUMN_PROMO_RE_TICKET_OUT), 0, DbRow.Value(SQL_COLUMN_PROMO_RE_TICKET_OUT))
      .not_redeemable_ticket_in += IIf(DbRow.IsNull(SQL_COLUMN_PROMO_NONRE_TICKET_IN), 0, DbRow.Value(SQL_COLUMN_PROMO_NONRE_TICKET_IN))
      .not_redeemable_ticket_out += IIf(DbRow.IsNull(SQL_COLUMN_PROMO_NONRE_TICKET_OUT), 0, DbRow.Value(SQL_COLUMN_PROMO_NONRE_TICKET_OUT))
      .total_ticket_in = .redeemable_ticket_in + .redeemable_ticket_in_promo + .not_redeemable_ticket_in
      .total_bill_in = .bill_in
      .total_ticket_out = .not_redeemable_ticket_out + .redeemable_ticket_out
    End With

  End Sub ' UpdateTotalCounters

  Private Function GetTerminalTypeNls(ByRef TerminalType As WSI.Common.TerminalTypes) As String

    Select Case TerminalType
      Case WSI.Common.TerminalTypes.WIN
        Return GLB_NLS_GUI_AUDITOR.GetString(379)
      Case WSI.Common.TerminalTypes.T3GS
        Return GLB_NLS_GUI_AUDITOR.GetString(381)
      Case WSI.Common.TerminalTypes.SAS_HOST
        Return GLB_NLS_GUI_AUDITOR.GetString(380)
      Case WSI.Common.TerminalTypes.SITE
        Return GLB_NLS_GUI_AUDITOR.GetString(384)
      Case WSI.Common.TerminalTypes.SITE_JACKPOT
        Return GLB_NLS_GUI_AUDITOR.GetString(382)
      Case WSI.Common.TerminalTypes.MOBILE_BANK
        Return GLB_NLS_GUI_AUDITOR.GetString(383)
      Case WSI.Common.TerminalTypes.MOBILE_BANK_IMB
        Return GLB_NLS_GUI_AUDITOR.GetString(385)
      Case WSI.Common.TerminalTypes.ISTATS
        Return GLB_NLS_GUI_AUDITOR.GetString(386)
      Case WSI.Common.TerminalTypes.PROMOBOX
        Return GLB_NLS_GUI_AUDITOR.GetString(387)
      Case WSI.Common.TerminalTypes.UNKNOWN
        Return "..."
      Case Else
        Return "---"
    End Select

  End Function

  Private Sub UpdateSiteCurrenciesList()

    If dtp_from.Checked Then
      uc_multi_currency_select.DateFrom = GUI_FormatDate(dtp_from.Value)
    Else
      uc_multi_currency_select.DateFrom = GUI_FormatDate(New Date(2007, 1, 1)) ' min date value
    End If

    If dtp_to.Checked Then
      uc_multi_currency_select.DateTo = GUI_FormatDate(dtp_to.Value)
    Else
      uc_multi_currency_select.DateTo = GUI_FormatDate(WGDB.Now)
    End If

  End Sub ' UpdateSiteCurrenciesList



  ' PURPOSE: Get all the columns that apply currency format
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetColumnsToChangeCurrency() As ArrayList
    Dim _columns_change_currency As ArrayList

    _columns_change_currency = New ArrayList
    With _columns_change_currency
      .Add(SQL_COLUMN_RE_CASH_IN)
      .Add(SQL_COLUMN_RE_PLAYED_AMOUNT)
      .Add(SQL_COLUMN_RE_WON_AMOUNT)
      .Add(SQL_COLUMN_RE_CASH_OUT)
      .Add(SQL_COLUMN_RE_TICKET_IN)
      .Add(SQL_COLUMN_NRE_CASH_IN)
      .Add(SQL_COLUMN_NRE_PLAYED_AMOUNT)
      .Add(SQL_COLUMN_NRE_WON_AMOUNT)
      .Add(SQL_COLUMN_NRE_CASH_OUT)
      .Add(SQL_COLUMN_PLAYED_AMOUNT)
      .Add(SQL_COLUMN_WON_AMOUNT)
      .Add(SQL_COLUMN_TOTAL_CASH_IN)
      .Add(SQL_COLUMN_TOTAL_CASH_OUT)
      .Add(SQL_COLUMN_CASH_IN_PLAY_SESSION)

      If Me.m_is_tito_mode Then
        .Add(SQL_COLUMN_AUX_FT_RE_CASH_IN)
        .Add(SQL_COLUMN_AUX_FT_NR_CASH_IN)
        .Add(SQL_COLUMN_RE_TICKET_IN)
        .Add(SQL_COLUMN_PROMO_RE_TICKET_IN)
        .Add(SQL_COLUMN_BILLS_IN_AMOUNT)
        .Add(SQL_COLUMN_PROMO_RE_TICKET_OUT)
        .Add(SQL_COLUMN_AUX_FT_NR_CASH_IN)
        .Add(SQL_COLUMN_PROMO_NONRE_TICKET_IN)
        .Add(SQL_COLUMN_PROMO_NONRE_TICKET_OUT)
      End If
    End With

    Return _columns_change_currency

  End Function ' GetColumnsToChangeCurrency

  ' PURPOSE: Print the exchange average in the excel sheet
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub PrintExchangeCurrency(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)
    Dim _is_multi_currency As Boolean

    _is_multi_currency = (Not String.IsNullOrEmpty(m_currency_option) AndAlso m_currency_option.Equals("True"))

    If _is_multi_currency Then
      Dim _exchange_currency As String
      Dim _exchange_currency_list As String()
      _exchange_currency_list = uc_multi_currency_select.GetAverageCurrencyList.Split(New Char() {";"c})

      For Each _exchange_currency In _exchange_currency_list
        PrintData.SetFilter(" ", _exchange_currency)
      Next

    End If

  End Sub ' PrintExchangeCurrency

  ' PURPOSE: Initialize controls when form show multisite, not multicurrency
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub InitializeMultiSiteControls()
    uc_site_select.Visible = False
    uc_multi_currency_select.Visible = True
    uc_multi_currency_select.DateFrom = dtp_from.Value
    uc_multi_currency_select.DateTo = dtp_to.Value
    uc_multi_currency_select.Init()
    uc_multi_currency_select.Location = New Point(uc_site_select.Location.X, uc_site_select.Location.Y)
    uc_multi_currency_select.Width = 480
    uc_multi_currency_select.Height = 170
    uc_multi_currency_select.TabIndex = 0
    gb_date.Location = New Point(gb_date.Location.X + 200, gb_date.Location.Y)
    gb_options.Location = New Point(gb_options.Location.X - 110, gb_options.Location.Y)
    gb_options.Height = gb_date.Height
    gb_options.TabIndex = 3
    uc_account_sel1.Location = New Point(gb_options.Location.X - 3, uc_account_sel1.Location.Y + 165)
    uc_account_sel1.TabIndex = 4
    gb_status.Width = gb_date.Width
    gb_status.Location = New Point(gb_date.Location.X, gb_status.Location.Y + 35)
    gb_status.TabIndex = 2
    opt_several_status.Location = New Point(8, opt_several_status.Location.Y)
    opt_all_status.Location = New Point(8, opt_all_status.Location.Y)
    cmb_status.Width = cmb_status.Width - 15
    cmb_status.Location = New Point(cmb_status.Location.X - 30, cmb_status.Location.Y)
    lbl_recharges.MaximumSize = New Size(470, 0)
    panel_filter.Height = 305

    AddHandler dtp_from.DatePickerCloseUp, AddressOf UpdateSiteCurrenciesList
    AddHandler dtp_to.DatePickerCloseUp, AddressOf UpdateSiteCurrenciesList
    AddHandler dtp_from.DatePickerValueChanged, AddressOf UpdateSiteCurrenciesList
    AddHandler dtp_to.DatePickerValueChanged, AddressOf UpdateSiteCurrenciesList

    Call UpdateSiteCurrenciesList()   'it calls initially to get the rates with the initial dates
  End Sub ' InitializeMultiSiteControls

#End Region  ' Private Functions

#Region "Events"

  Private Sub opt_several_status_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_several_status.Click
    Me.cmb_status.Enabled = True
  End Sub

  Private Sub opt_all_status_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_all_status.Click
    Me.cmb_status.Enabled = False
  End Sub

  Private Sub chk_grouped_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_grouped.CheckedChanged
    Me.opt_provider.Enabled = Me.chk_grouped.Checked
    Me.opt_account.Enabled = Me.chk_grouped.Checked
    Me.chk_subtotal.Enabled = Me.chk_grouped.Checked
    Me.chk_show_sessions.Enabled = Me.chk_grouped.Checked
  End Sub

  Private Sub opt_provider_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_provider.CheckedChanged
    If Me.opt_provider.Checked Then
      Me.chk_subtotal.Text = GLB_NLS_GUI_INVOICING.GetString(371, GLB_NLS_GUI_INVOICING.GetString(424))
    End If
  End Sub

  Private Sub opt_account_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_account.CheckedChanged
    If Me.opt_account.Checked Then
      Me.chk_subtotal.Text = GLB_NLS_GUI_INVOICING.GetString(371, GLB_NLS_GUI_INVOICING.GetString(423))
    End If
  End Sub

  ' PURPOSE: Get all events for dates filter. Filter is set depending on the selected option
  '
  '  PARAMS:
  '     - INPUT:
  '           - Sender
  '           - EventArgs
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '           - None
  Private Sub rb_date_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    Dim _rb As RadioButton = sender

    RemoveHandler rb_open_sessions.Click, AddressOf rb_date_CheckedChanged
    RemoveHandler rb_historic.Click, AddressOf rb_date_CheckedChanged
    RemoveHandler rb_ini_date.Click, AddressOf rb_date_CheckedChanged
    RemoveHandler rb_finish_date.Click, AddressOf rb_date_CheckedChanged

    Select Case _rb.Text
      Case GLB_NLS_GUI_STATISTICS.GetString(345) ' 345 - Open Sessions
        Me.rb_open_sessions.Checked = True
        Me.rb_finish_date.Checked = False
        Me.rb_historic.Checked = False
        Me.rb_ini_date.Checked = False
        Me.gb_status.Enabled = False
        Me.opt_all_status.Checked = True
        Me.dtp_from.Enabled = False
        Me.dtp_to.Enabled = False
      Case GLB_NLS_GUI_CONTROLS.GetString(331) ' 331 - History
        Me.rb_historic.Checked = True
        Me.rb_open_sessions.Checked = False
        Me.rb_ini_date.Checked = True
        Me.rb_finish_date.Checked = False
        Me.gb_status.Enabled = True
        Me.dtp_to.Enabled = True
        Me.dtp_from.Enabled = True
      Case GLB_NLS_GUI_STATISTICS.GetString(381) ' 381 - Started
        Me.rb_ini_date.Checked = True
        Me.rb_finish_date.Checked = False
        Me.rb_historic.Checked = True
        Me.rb_open_sessions.Checked = False
        Me.gb_status.Enabled = True
        Me.dtp_to.Enabled = True
        Me.dtp_from.Enabled = True
      Case GLB_NLS_GUI_PLAYER_TRACKING.GetString(5704) ' 5704 - Finished
        Me.rb_finish_date.Checked = True
        Me.rb_historic.Checked = True
        Me.rb_open_sessions.Checked = False
        Me.rb_ini_date.Checked = False
        Me.gb_status.Enabled = True
        Me.dtp_from.Enabled = True
        Me.dtp_to.Enabled = True
    End Select

    AddHandler rb_open_sessions.Click, AddressOf rb_date_CheckedChanged
    AddHandler rb_historic.Click, AddressOf rb_date_CheckedChanged
    AddHandler rb_ini_date.Click, AddressOf rb_date_CheckedChanged
    AddHandler rb_finish_date.Click, AddressOf rb_date_CheckedChanged

  End Sub ' rb_date_CheckedChanged

#End Region ' Events
End Class
