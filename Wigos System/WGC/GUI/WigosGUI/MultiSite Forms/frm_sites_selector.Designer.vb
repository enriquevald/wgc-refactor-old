<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_sites_selector
  Inherits GUI_Controls.frm_base

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.lst_sites = New System.Windows.Forms.ListBox
    Me.lblSiteId = New System.Windows.Forms.Label
    Me.txtSiteId = New System.Windows.Forms.TextBox
    Me.btnConnect = New System.Windows.Forms.Button
    Me.btnExit = New System.Windows.Forms.Button
    Me.btn_rdp = New System.Windows.Forms.Button
    Me.SuspendLayout()
    '
    'lst_sites
    '
    Me.lst_sites.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.lst_sites.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lst_sites.FormattingEnabled = True
    Me.lst_sites.ItemHeight = 18
    Me.lst_sites.Location = New System.Drawing.Point(6, 41)
    Me.lst_sites.Name = "lst_sites"
    Me.lst_sites.Size = New System.Drawing.Size(295, 220)
    Me.lst_sites.TabIndex = 1
    '
    'lblSiteId
    '
    Me.lblSiteId.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblSiteId.Location = New System.Drawing.Point(3, 9)
    Me.lblSiteId.Name = "lblSiteId"
    Me.lblSiteId.Size = New System.Drawing.Size(51, 18)
    Me.lblSiteId.TabIndex = 0
    Me.lblSiteId.Text = "xSiteId"
    '
    'txtSiteId
    '
    Me.txtSiteId.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.txtSiteId.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.txtSiteId.Location = New System.Drawing.Point(60, 6)
    Me.txtSiteId.MaxLength = 50
    Me.txtSiteId.Name = "txtSiteId"
    Me.txtSiteId.Size = New System.Drawing.Size(241, 27)
    Me.txtSiteId.TabIndex = 0
    '
    'btnConnect
    '
    Me.btnConnect.Location = New System.Drawing.Point(6, 267)
    Me.btnConnect.Name = "btnConnect"
    Me.btnConnect.Size = New System.Drawing.Size(91, 33)
    Me.btnConnect.TabIndex = 2
    Me.btnConnect.Text = "xConectar"
    Me.btnConnect.UseVisualStyleBackColor = True
    '
    'btnExit
    '
    Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.btnExit.Location = New System.Drawing.Point(208, 267)
    Me.btnExit.Name = "btnExit"
    Me.btnExit.Size = New System.Drawing.Size(91, 33)
    Me.btnExit.TabIndex = 4
    Me.btnExit.Text = "xSalir"
    Me.btnExit.UseVisualStyleBackColor = True
    '
    'btn_rdp
    '
    Me.btn_rdp.Location = New System.Drawing.Point(106, 267)
    Me.btn_rdp.Name = "btn_rdp"
    Me.btn_rdp.Size = New System.Drawing.Size(91, 33)
    Me.btn_rdp.TabIndex = 3
    Me.btn_rdp.Text = "xRemote"
    Me.btn_rdp.UseVisualStyleBackColor = True
    '
    'frm_sites_selector
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(307, 307)
    Me.Controls.Add(Me.btn_rdp)
    Me.Controls.Add(Me.btnExit)
    Me.Controls.Add(Me.btnConnect)
    Me.Controls.Add(Me.txtSiteId)
    Me.Controls.Add(Me.lblSiteId)
    Me.Controls.Add(Me.lst_sites)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.KeyPreview = True
    Me.MaximizeBox = False
    Me.MinimizeBox = False
    Me.Name = "frm_sites_selector"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "xSiteSelector"
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents lst_sites As System.Windows.Forms.ListBox
  Friend WithEvents lblSiteId As System.Windows.Forms.Label
  Friend WithEvents txtSiteId As System.Windows.Forms.TextBox
  Friend WithEvents btnConnect As System.Windows.Forms.Button
  Friend WithEvents btnExit As System.Windows.Forms.Button
  Friend WithEvents btn_rdp As System.Windows.Forms.Button
End Class
