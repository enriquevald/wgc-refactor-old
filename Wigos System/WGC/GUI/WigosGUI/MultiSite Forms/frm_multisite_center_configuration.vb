'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_multisite_center_configuration.vb
' DESCRIPTION:   Multisite Center configuration
' AUTHOR:        Artur Nebot Garrig�
' CREATION DATE: 15-MAR-2013
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 15-MAR-2013  ANG    Initial version
' 04-APR-2013  ANG    Support NULL values in SP_INTERVAL_SECONDS
' 05-APR-2013  ANG    Suppport set task cell as dissable
' 10-APR-2013  ANG    Fixed bug #698 
' 18-APR-2013  ANG    Refactor and optimize code.
' 02-MAY-2013  ANG    Hide Start Hour grid column.
' 06-MAY-2013  ANG    Move GetTaskFieldProperties here.
' 21-MAY-2013  AMF    New Column Name in Sites
' 15-APR-2014  RMS    Fixed Bug WIG-819: System does not allow to set task intervals greater than 999
' 23-APR-2015  ANM    New Column Currency in Sites
' -------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports WSI.Common
Imports GUI_CommonOperations.CLASS_BASE
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data

Public Class frm_multisite_center_configuration
  Inherits GUI_Controls.frm_base_edit

#Region " Constants "

  Private Const GRID_TASKS_HEADER_ROWS As Byte = 1
  Private Const GRID_TASKS_NUM_COLUMNS As Byte = 6
  Private Const GRID_TASKS_COLUMN_TASK_ID As Byte = 0
  Private Const GRID_TASKS_COLUMN_TASK_NAME As Byte = 1
  Private Const GRID_TASKS_COLUMN_ENABLED As Byte = 2
  Private Const GRID_TASKS_COLUMN_INTERVAL_SECONDS As Byte = 3
  Private Const GRID_TASKS_COLUMN_START_HOUR As Byte = 4
  Private Const GRID_TASKS_COLUMN_MAX_ROWS2UPLOAD As Byte = 5

  Private Const TASKS_INTERVAL_MAX_DIGITS As Byte = 9
  Private Const TASKS_START_HOUR_MAX_DIGITS As Byte = 2

  Private Const GRID_SITES_HEADER_ROWS As Byte = 2
  Private Const SITE_ID_MAX_DIGITS = 3
  Private Const SITE_NAME_MAX_DIGITS = 50
  Private Const SITE_HOST_MAX_DIGITS = 50

  Private Const GRID_SITES_NUM_COLUMNS As Byte = 7
  Private Const GRID_SITES_COLUMN_SITEID As Byte = 0
  Private Const GRID_SITES_COLUMN_ENABLED As Byte = 1
  Private Const GRID_SITES_COLUMN_NAME As Byte = 2
  Private Const GRID_SITES_COLUMN_DB_PRINCIPAL As Byte = 3
  Private Const GRID_SITES_COLUMN_DB_MIRROR As Byte = 4
  Private Const GRID_SITES_COLUMN_ELP As Byte = 5
  Private Const GRID_SITES_COLUMN_ISO_CODE As Byte = 6

  Private Const HEADER_TITLE_EMPTY = "   "

#End Region ' Constants

#Region " Members "

  Dim m_grid_tasks_disable_dependencies As Dictionary(Of TYPE_MULTISITE_SITE_TASKS, List(Of TYPE_MULTISITE_SITE_TASKS))
  Dim m_grid_tasks_enable_dependencies As Dictionary(Of TYPE_MULTISITE_SITE_TASKS, List(Of TYPE_MULTISITE_SITE_TASKS))
  Dim m_task_field_properties As Dictionary(Of TYPE_MULTISITE_SITE_TASKS, mdl_multisite.TYPE_MULTISITE_TASK_FIELD_PROPERTIES())
  Dim m_not_editable_color As Color = Color.FromArgb(240, 240, 240)
  Dim m_disabled_color As Color = Color.FromArgb(240, 240, 240)
  Dim m_disabled_value As String = "-" 'String.Empty
  Dim m_new_row_color As Color = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)
  Dim m_new_sites_added As New List(Of Integer)()

#End Region ' Members

#Region " Overrides"

  ' PURPOSE: Sets the proper form identifier
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_MULTISITE_CENTER_CONFIG

    Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

  ' PURPOSE: Initializes form controls
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()
    ' Initialize parent control, required
    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1793) '"Multisite Configuration"

    ' Enable / Disable controls
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Enabled = False
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Visible = False

    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Visible = True
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Enabled = True

    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Visible = True
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Enabled = True

    ' Configuration tab
    Me.tb_cards.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1822)

    ' Directions tab
    Me.tb_directions.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1823)

    ' Tasks tab
    Me.tb_tasks.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1797)

    ' Sites tab
    Me.tb_sites.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1771)

    ' Head groupbox
    Me.gb_head.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1907)

    ' Site ID
    Me.lbl_site_id.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1407) '1407 "Site ID"
    Me.ef_site_id.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 3)
    Me.ef_site_id.IsReadOnly = True

    ' Site Name
    Me.lbl_site_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1410) '1410 "Nombre de sala"
    Me.ef_site_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)
    Me.ef_site_name.IsReadOnly = True

    ' Member CardsId
    Me.lbl_members_card_id.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1408) '1408 "Members Card ID"
    Me.ef_member_card_id.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)

    ' Center Address 1
    Me.lbl_center_address_1.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1690) '1690 "Center Address 1"
    Me.ef_center_address_1.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 500)

    ' Center Address 2
    Me.lbl_center_address_2.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1691) '1691 "Center Address 2"
    Me.ef_center_address_2.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 500)

    ' Button add site
    Me.btnAddSite.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1684)
    Me.btnAddSite.Enabled = Me.Permissions.Write


    ' Label description
    Me.gbSites.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1975)


    ' Set Task grid columns and style
    Call Me.GUI_StyleTaskGrid()

    'Set Sites grid columns and style
    Call GUI_StyleSitesGrid()

    ' Set field properties ( allow edit, min and max values... )
    Me.m_task_field_properties = mdl_multisite.GetTaskFieldProperties()

    ' Configure dependencies between tasks
    Call ConfigureTasksDependencies()

  End Sub 'GUI_InitControls

  ' PURPOSE: Set initial data on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Dim _multisite_center_config As CLS_MULTISITE_CENTER_CONFIGURATION
    _multisite_center_config = CType(DbReadObject, CLS_MULTISITE_CENTER_CONFIGURATION)

    Me.ef_site_id.Value = _multisite_center_config.SiteID
    Me.ef_site_name.Value = _multisite_center_config.SiteName
    Me.ef_member_card_id.Value = _multisite_center_config.MembersCardID
    Me.ef_center_address_1.Value = _multisite_center_config.CenterAddress1
    Me.ef_center_address_2.Value = _multisite_center_config.CenterAddress2

    '' Set data into grid
    FillTasksIntoGrid(_multisite_center_config.Tasks, Me.dg_tasks)

    FillSitesIntoGrid(_multisite_center_config.Sites, Me.dg_sites)

    Me.dg_tasks.CurrentCol = 0

  End Sub 'GUI_SetScreenData

  ' PURPOSE: Get data from the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_GetScreenData()
    Dim _multisite_center_config As CLS_MULTISITE_CENTER_CONFIGURATION
    Dim _site_id As Int32

    _multisite_center_config = CType(DbEditedObject, CLS_MULTISITE_CENTER_CONFIGURATION)

    _multisite_center_config.MembersCardID = Me.ef_member_card_id.Value
    _multisite_center_config.CenterAddress1 = Me.ef_center_address_1.Value
    _multisite_center_config.CenterAddress2 = Me.ef_center_address_2.Value

    _multisite_center_config.Tasks = GetTasksFromGrid(Me.dg_tasks)

    _multisite_center_config.Sites = GetSitesFromGrid(Me.dg_sites)

    '' Detect new Sites...
    For Each _site_row As DataRow In CType(DbEditedObject, CLS_MULTISITE_CENTER_CONFIGURATION).Sites.Rows
      _site_id = _site_row("ST_SITE_ID")
      If CType(DbReadObject, CLS_MULTISITE_CENTER_CONFIGURATION).Sites.Rows.Find(_site_id) Is Nothing Then
        CType(DbEditedObject, CLS_MULTISITE_CENTER_CONFIGURATION).NewSites.Add(_site_id)
      End If
    Next

  End Sub 'GUI_GetScreenData

  ' PURPOSE: Database overridable operations. 
  '          Define specific DB operation for this form.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)
    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE

        Me.DbEditedObject = New CLS_MULTISITE_CENTER_CONFIGURATION
        Me.DbStatus = ENUM_STATUS.STATUS_OK

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(105), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(106), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If
      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_UPDATE
        Call MyBase.GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_UPDATE)
      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)
    End Select

  End Sub 'GUI_DB_Operation

  ' PURPOSE : Validate the data presented on the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    Dim _properties() As mdl_multisite.TYPE_MULTISITE_TASK_FIELD_PROPERTIES
    Dim _idx_grid_row As UInt16 = 0
    Dim _task As TYPE_MULTISITE_SITE_TASKS
    Dim _configuration_interval_value As Int32
    Dim _int_val As Int32
    Dim _str_value As String
    Dim _list_sites_id As New List(Of Integer)
    _properties = Nothing

    '' Check tasks
    If Me.dg_tasks.NumRows > 0 Then
      For _idx_grid_row = 0 To Me.dg_tasks.NumRows - 1
        _task = CType(Me.dg_tasks.Cell(_idx_grid_row, GRID_TASKS_COLUMN_TASK_ID).Value, TYPE_MULTISITE_SITE_TASKS)

        '' Get field properties for this task ( witch columns allow edit and min and max values

        If Not m_task_field_properties.TryGetValue(_task, _properties) Then
          _properties = m_task_field_properties.Item(TYPE_MULTISITE_SITE_TASKS.Unknown)
        End If

        '' Works beacuse Configuration ( 1 ) is the first row.. LLEIG!
        If (_task = TYPE_MULTISITE_SITE_TASKS.DownloadConfiguration) Then
          _configuration_interval_value = Convert.ToInt32(Me.dg_tasks.Cell(_idx_grid_row, GRID_TASKS_COLUMN_INTERVAL_SECONDS).Value)
        End If

        If Not CheckScreenDataTaskProperty(_idx_grid_row, _task, GRID_TASKS_COLUMN_INTERVAL_SECONDS, Me.dg_tasks, _configuration_interval_value) Then

          Return False
        End If

        If Not CheckScreenDataTaskProperty(_idx_grid_row, _task, GRID_TASKS_COLUMN_START_HOUR, Me.dg_tasks, _configuration_interval_value) Then

          Return False
        End If

        If Not CheckScreenDataTaskProperty(_idx_grid_row, _task, GRID_TASKS_COLUMN_MAX_ROWS2UPLOAD, Me.dg_tasks, _configuration_interval_value) Then

          Return False
        End If

      Next
    End If


    '' Check tasks rows
    If dg_sites.NumRows > 0 Then
      For _idx_grid_row = 0 To dg_sites.NumRows - 1

        _str_value = Me.dg_sites.Cell(_idx_grid_row, GRID_SITES_COLUMN_SITEID).Value

        If String.IsNullOrEmpty(_str_value) OrElse Not Int32.TryParse(GUI_LocalNumberToDBNumber(_str_value), _int_val) Then

          Me.tb_panel.SelectedTab = Me.tb_sites
          Call SelectRow(dg_sites, _idx_grid_row)

          NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1976) _
                     , ENUM_MB_TYPE.MB_TYPE_ERROR _
                     , ENUM_MB_BTN.MB_BTN_OK _
                     , ENUM_MB_DEF_BTN.MB_DEF_BTN_1)
          Return False


          Return False
        End If

        If (_int_val <= 0) Then

          Me.tb_panel.SelectedTab = Me.tb_sites
          Call SelectRow(dg_sites, _idx_grid_row)

          NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1976) _
                     , ENUM_MB_TYPE.MB_TYPE_ERROR _
                     , ENUM_MB_BTN.MB_BTN_OK _
                     , ENUM_MB_DEF_BTN.MB_DEF_BTN_1)

          Return False
        End If

        If _list_sites_id.Contains(_int_val) Then

          Me.tb_panel.SelectedTab = Me.tb_sites
          Call SelectRow(dg_sites, _idx_grid_row)

          NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1978) _
                             , ENUM_MB_TYPE.MB_TYPE_ERROR _
                             , ENUM_MB_BTN.MB_BTN_OK _
                             , ENUM_MB_DEF_BTN.MB_DEF_BTN_1 _
                             , _int_val)

          Return False
        End If

        _list_sites_id.Add(_int_val)

      Next
    End If



    Return True

  End Function 'GUI_IsScreenDataOk()

  ' PURPOSE: Handle Form buttons cliks
  '
  '  PARAMS:
  '     - INPUT: Clicked button object
  '         -  
  '
  '     - OUTPUT:
  '
  ' RETURNS: 
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_CUSTOM_0

        ' Save without close form
        MyBase.CloseOnOkClick = False
        Call MyBase.GUI_ButtonClick(ENUM_BUTTON.BUTTON_OK)
        MyBase.CloseOnOkClick = True

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

  ' PURPOSE: Perform any clean-up , release used memory in image cache
  '
  '  PARAMS:
  '     - INPUT:
  '         -  
  '
  '     - OUTPUT:
  '
  ' RETURNS: 
  '
  Protected Overrides Sub GUI_Exit()
    Call MyBase.GUI_Exit()
  End Sub ' GUI_Exit

#End Region ' Overrides

#Region " Public Functions "

  ' PURPOSE: Init form in edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - mdiparent
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Overloads Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT
    Me.DbObjectId = Nothing
    Dim _multisite_center_config As CLS_MULTISITE_CENTER_CONFIGURATION

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)
    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      _multisite_center_config = CType(DbEditedObject, CLS_MULTISITE_CENTER_CONFIGURATION)

      Call Me.Display(True)
    End If

  End Sub ' ShowForEdit

  ' PURPOSE: Class constructor
  '
  '  PARAMS:
  '     - INPUT:
  '         -  
  '
  '     - OUTPUT:
  '
  Public Sub New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.

  End Sub ' New

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE:  Set column properties to Tasks Grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleTaskGrid()

    With Me.dg_tasks
      Call .Init(GRID_TASKS_NUM_COLUMNS, GRID_TASKS_HEADER_ROWS, True) ' Columns , Header , Editable

      With .Column(GRID_TASKS_COLUMN_TASK_ID)
        .Header.Text = "xTaskId"
        .Width = 0
      End With

      With .Column(GRID_TASKS_COLUMN_TASK_NAME)
        .Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1798) ' Task
        .Width = 7400
      End With

      With .Column(GRID_TASKS_COLUMN_ENABLED)
        .Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1799) ' Enabled
        .Width = 1380
        .Editable = True
        .EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX
      End With

      With .Column(GRID_TASKS_COLUMN_INTERVAL_SECONDS)
        .Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1800) ' Interval
        .Width = 1800
        .Editable = True
        .EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_ENTRY_FIELD
        .EditionControl.EntryField.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, TASKS_INTERVAL_MAX_DIGITS)
      End With

      With .Column(GRID_TASKS_COLUMN_START_HOUR)
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1821) ' Starting hour
        .Width = 0 ' Hide column!
        '.Width = 1200 ''
        .EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_ENTRY_FIELD
        .EditionControl.EntryField.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, TASKS_START_HOUR_MAX_DIGITS)
        .Editable = True
      End With

      With .Column(GRID_TASKS_COLUMN_MAX_ROWS2UPLOAD)
        .Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1820) ' Max items to upload
        .Width = 1400
        .EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_ENTRY_FIELD
        .EditionControl.EntryField.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, TASKS_INTERVAL_MAX_DIGITS)
        .Editable = True
      End With

    End With
  End Sub 'GUI_StyleTaskGrid

  ' PURPOSE:  Set column properties to Sites Grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSitesGrid()

    With Me.dg_sites
      Call .Init(GRID_SITES_NUM_COLUMNS, GRID_SITES_HEADER_ROWS, True) ' Columns , Header , Editable

      With .Column(GRID_SITES_COLUMN_SITEID)
        .Header(0).Text = HEADER_TITLE_EMPTY
        .Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1927) '' SiteID
        .Width = 920
        .EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_ENTRY_FIELD
        .EditionControl.EntryField.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, SITE_ID_MAX_DIGITS)
        .Editable = True
      End With

      With .Column(GRID_SITES_COLUMN_ENABLED)
        .Header(0).Text = HEADER_TITLE_EMPTY
        .Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1799) '' Enabled
        .Width = 1000
        .Editable = True
        .EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX
      End With

      With .Column(GRID_SITES_COLUMN_NAME)
        .Header(0).Text = HEADER_TITLE_EMPTY
        .Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1410) '' Site Name
        .Width = 2675
        .Editable = True
        .EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_ENTRY_FIELD
        .EditionControl.EntryField.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, SITE_NAME_MAX_DIGITS)
      End With

      With .Column(GRID_SITES_COLUMN_DB_PRINCIPAL)
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2044)       '' Connection String
        .Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2045)       '' Servidor 1
        .Width = 2000
        .Editable = True
        .EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_ENTRY_FIELD
        .EditionControl.EntryField.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, SITE_HOST_MAX_DIGITS)
      End With

      With .Column(GRID_SITES_COLUMN_DB_MIRROR)
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2044)       '' Connection String
        .Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2046)       '' Servidor 2
        .Width = 2000
        .Editable = True
        .EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_ENTRY_FIELD
        .EditionControl.EntryField.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, SITE_HOST_MAX_DIGITS)
      End With

      With .Column(GRID_SITES_COLUMN_ELP)
        .Header(0).Text = HEADER_TITLE_EMPTY
        .Header(1).Text = GeneralParam.GetString("ExternalLoyaltyProgram.Mode01", "Name") ''Enable External Loyalty Program
        .Width = 1000
        .Editable = True
        .EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX
      End With

      With .Column(GRID_SITES_COLUMN_ISO_CODE)
        .Header(0).Text = HEADER_TITLE_EMPTY
        .Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1057)   '' Currency
        .Width = 850
        .Editable = False
        .EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_ENTRY_FIELD
      End With

    End With
  End Sub

  ' PURPOSE:  Get Data from DataRow Column with Null check
  '
  '  PARAMS:
  '     - INPUT:
  '           - Row as DataRow
  '           - Column Name as String
  '           - Optional BlankValue as String 
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Cell value.
  Private Function GetDataRowColumnValue(ByRef Row As DataRow, ByVal ColumnName As String, Optional ByVal BlankValue As Object = "") As Object

    Return IIf(Row.IsNull(ColumnName), BlankValue, Row(ColumnName))

  End Function 'GetDataRowColumnValue

  ' PURPOSE:  Cast from boolean to uc_grid checked
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function BoolToGridValue(ByVal enabled As Boolean) As String
    If (enabled) Then
      Return uc_grid.GRID_CHK_CHECKED
    Else
      Return uc_grid.GRID_CHK_UNCHECKED
    End If
  End Function 'BoolToGridValue

  ' PURPOSE:  Cast from uc_grid checked to boolean
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GridValueToBool(ByVal enabled As String) As Boolean

    If (enabled.Equals(uc_grid.GRID_CHK_CHECKED) Or enabled.Equals(uc_grid.GRID_CHK_CHECKED_DISABLED)) Then
      Return True
    Else
      Return False
    End If

  End Function 'GridValueToBool

  ' PURPOSE: Fill a new DataTable with data present in Tasks grid
  '
  '    - INPUT:
  '       - TasksGrid : Source to get tasks data
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '    - DataTable : Datatable with tasks data
  Private Function GetTasksFromGrid(ByRef TasksGrid As uc_grid) As DataTable
    Dim _task_data As DataTable = New DataTable()
    Dim _task_row As DataRow
    Dim _idx_row As UInt16
    Dim _val As Object
    Dim _task_properties As mdl_multisite.TYPE_MULTISITE_TASK_FIELD_PROPERTIES()
    Dim _task_id As Byte
    _task_data = CType(Me.DbReadObject, CLS_MULTISITE_CENTER_CONFIGURATION).Tasks.Clone()
    _task_properties = Nothing

    For _idx_row = 0 To TasksGrid.NumRows() - 1
      _task_row = _task_data.NewRow
      _task_id = TasksGrid.Cell(_idx_row, GRID_TASKS_COLUMN_TASK_ID).Value

      If Not m_task_field_properties.TryGetValue(_task_id, _task_properties) Then
        _task_properties = m_task_field_properties.Item(TYPE_MULTISITE_SITE_TASKS.Unknown)
      End If



      _task_row("ST_TASK_ID") = _task_id
      _task_row("ST_ENABLED") = GridValueToBool(TasksGrid.Cell(_idx_row, GRID_TASKS_COLUMN_ENABLED).Value)

      If (_task_properties(GRID_TASKS_COLUMN_INTERVAL_SECONDS).Enabled) Then
        _val = TasksGrid.Cell(_idx_row, GRID_TASKS_COLUMN_INTERVAL_SECONDS).Value
        If (_val <> String.Empty) Then
          _val = GUI_LocalNumberToDBNumber(_val)
        Else
          _val = DBNull.Value
        End If
        _task_row("ST_INTERVAL_SECONDS") = _val
      Else
        _task_row("ST_INTERVAL_SECONDS") = DBNull.Value
      End If

      If (_task_properties(GRID_TASKS_COLUMN_MAX_ROWS2UPLOAD).Enabled _
            AndAlso TasksGrid.Cell(_idx_row, GRID_TASKS_COLUMN_MAX_ROWS2UPLOAD).Value <> String.Empty) Then
        _task_row("ST_MAX_ROWS_TO_UPLOAD") = GUI_LocalNumberToDBNumber(TasksGrid.Cell(_idx_row, GRID_TASKS_COLUMN_MAX_ROWS2UPLOAD).Value)
      Else
        _task_row("ST_MAX_ROWS_TO_UPLOAD") = 0 '' Column don't allow null values!
      End If

      If (_task_properties(GRID_TASKS_COLUMN_START_HOUR).Enabled _
            AndAlso TasksGrid.Cell(_idx_row, GRID_TASKS_COLUMN_START_HOUR).Value <> String.Empty) Then
        _task_row("ST_START_TIME") = GUI_LocalNumberToDBNumber(TasksGrid.Cell(_idx_row, GRID_TASKS_COLUMN_START_HOUR).Value)
      Else
        _task_row("ST_START_TIME") = 0 '' Column don't allow null values!
      End If

      _task_data.Rows.Add(_task_row)

    Next

    Return _task_data


  End Function  'GetTasksFromGrid

  ' PURPOSE:  Fill a new DataTable with data present in Sites grid
  '
  '    - INPUT:
  '       - TasksGrid : Source to get tasks data
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '    - DataTable : Datatable with tasks data
  Private Function GetSitesFromGrid(ByRef SitesGrid As uc_grid) As DataTable

    Dim _sites_data As New DataTable()
    Dim _site_row As DataRow
    Dim _idx_row As UInt16
    Dim _fake_idx As Int32

    _fake_idx = 0
    _sites_data = CType(Me.DbReadObject, CLS_MULTISITE_CENTER_CONFIGURATION).Sites.Clone()

    If SitesGrid.NumRows() <= 0 Then
      Return _sites_data
    End If

    For _idx_row = 0 To SitesGrid.NumRows() - 1
      _site_row = _sites_data.NewRow

      If (Not String.IsNullOrEmpty(SitesGrid.Cell(_idx_row, GRID_SITES_COLUMN_SITEID).Value)) Then
        _site_row("ST_SITE_ID") = GUI_LocalNumberToDBNumber(SitesGrid.Cell(_idx_row, GRID_SITES_COLUMN_SITEID).Value)
      Else
        '' Only  here when user closes form trought "Cancel" button..
        '' Mark to MinusOne to detect changes...
        _fake_idx = _fake_idx - 1
        _site_row("ST_SITE_ID") = _fake_idx
      End If

      If (Not String.IsNullOrEmpty(SitesGrid.Cell(_idx_row, GRID_SITES_COLUMN_ENABLED).Value)) Then
        _site_row("ST_STATE") = GridValueToBool(SitesGrid.Cell(_idx_row, GRID_SITES_COLUMN_ENABLED).Value)
      Else
        '' Only  here when user closes form trought "Cancel" button..
        _site_row("ST_STATE") = False
      End If

      If (Not String.IsNullOrEmpty(SitesGrid.Cell(_idx_row, GRID_SITES_COLUMN_NAME).Value)) Then
        _site_row("ST_NAME") = SitesGrid.Cell(_idx_row, GRID_SITES_COLUMN_NAME).Value
      Else
        '' Only  here when user closes form trought "Cancel" button..
        _site_row("ST_NAME") = ""
      End If

      If Not String.IsNullOrEmpty(SitesGrid.Cell(_idx_row, GRID_SITES_COLUMN_DB_PRINCIPAL).Value) Then
        _site_row("DBPrincipal") = SitesGrid.Cell(_idx_row, GRID_SITES_COLUMN_DB_PRINCIPAL).Value
      Else
        _site_row("DBPrincipal") = String.Empty
      End If

      If Not String.IsNullOrEmpty(SitesGrid.Cell(_idx_row, GRID_SITES_COLUMN_DB_MIRROR).Value) Then
        _site_row("DBMirror") = SitesGrid.Cell(_idx_row, GRID_SITES_COLUMN_DB_MIRROR).Value
      Else
        _site_row("DBMirror") = String.Empty
      End If

      If (Not String.IsNullOrEmpty(SitesGrid.Cell(_idx_row, GRID_SITES_COLUMN_ELP).Value)) Then
        _site_row("ST_ELP") = GridValueToBool(SitesGrid.Cell(_idx_row, GRID_SITES_COLUMN_ELP).Value)
      Else
        '' Only  here when user closes form trought "Cancel" button..
        _site_row("ST_ELP") = False
      End If

      If _sites_data.Rows.Find(_site_row("ST_SITE_ID")) IsNot Nothing Then

        '' Trick ;)
        '' Only can be duplicated values in case that user closes form with 'Cancel' button
        '' in this case, we can ignore data ;)
        Continue For

      End If
      _sites_data.Rows.Add(_site_row)

    Next


    Return _sites_data

  End Function 'GetSitesFromGrid

  ' PURPOSE: Set data from DataTable to Sites Grid.
  '
  '    - INPUT:
  '       - Tasks DataTable 
  '       - TasksGrid : Source to get tasks data
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Private Sub FillSitesIntoGrid(ByRef Sites As DataTable, ByRef dg_tasks As uc_grid)

    Dim _idx_row As Int16
    Dim _is_task_enabled As Boolean
    Dim _is_elp_enabled As Boolean

    For Each _row As DataRow In Sites.Rows
      _idx_row = dg_sites.AddRow()

      dg_sites.Cell(_idx_row, GRID_SITES_COLUMN_SITEID).Value = GUI_FormatNumber(GetDataRowColumnValue(_row, "ST_SITE_ID"), 0)
      dg_sites.Cell(_idx_row, GRID_SITES_COLUMN_SITEID).BackColor = m_not_editable_color

      _is_task_enabled = IIf(GetDataRowColumnValue(_row, "ST_STATE"), True, False)
      dg_sites.Cell(_idx_row, GRID_SITES_COLUMN_ENABLED).Value = BoolToGridValue(_is_task_enabled)

      dg_sites.Cell(_idx_row, GRID_SITES_COLUMN_NAME).Value = GetDataRowColumnValue(_row, "ST_NAME")

      dg_sites.Cell(_idx_row, GRID_SITES_COLUMN_DB_PRINCIPAL).Value = GetDataRowColumnValue(_row, "DBPrincipal")
      dg_sites.Cell(_idx_row, GRID_SITES_COLUMN_DB_MIRROR).Value = GetDataRowColumnValue(_row, "DBMirror")

      _is_elp_enabled = IIf(GetDataRowColumnValue(_row, "ST_ELP"), True, False)
      dg_sites.Cell(_idx_row, GRID_SITES_COLUMN_ELP).Value = BoolToGridValue(_is_elp_enabled)

      dg_sites.Cell(_idx_row, GRID_SITES_COLUMN_ISO_CODE).Value = GetDataRowColumnValue(_row, "SC_ISO_CODE")

    Next

    Call GUI_AdjustGridScrollbars(Me.dg_sites, GRID_SITES_COLUMN_NAME, GRID_SITES_HEADER_ROWS)

  End Sub 'FillSitesIntoGrid

  ' PURPOSE: Set data from grid to DataTable
  '
  '    - INPUT:
  '       - Tasks DataTable 
  '       - TasksGrid : Source to get tasks data
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Private Sub FillTasksIntoGrid(ByRef Tasks As DataTable, ByRef dg_tasks As uc_grid)

    Dim _idx_row As Int16
    Dim _task_id As Byte
    Dim _task_name As String
    Dim _is_enabled As Boolean
    Dim _task_properties As mdl_multisite.TYPE_MULTISITE_TASK_FIELD_PROPERTIES()
    _task_name = String.Empty
    _task_properties = Nothing

    For Each _row As DataRow In Tasks.Rows
      _idx_row = dg_tasks.AddRow()

      _task_id = CType(GetDataRowColumnValue(_row, "ST_TASK_ID"), TYPE_MULTISITE_SITE_TASKS)
      _task_name = TranslateMultiSiteProcessID(_task_id)

      dg_tasks.Cell(_idx_row, GRID_TASKS_COLUMN_TASK_ID).Value = _task_id.ToString()
      dg_tasks.Cell(_idx_row, GRID_TASKS_COLUMN_TASK_NAME).Value = _task_name

      _is_enabled = GetDataRowColumnValue(_row, "ST_ENABLED")

      If Not m_task_field_properties.TryGetValue(_task_id, _task_properties) Then
        _task_properties = m_task_field_properties.Item(TYPE_MULTISITE_SITE_TASKS.Unknown)
      End If

      If _task_id = TYPE_MULTISITE_SITE_TASKS.DownloadPoints_All And CommonMultiSite.GetPlayerTrackingMode() = PlayerTracking_Mode.Site And Not _is_enabled Then
        _task_properties(PROPERTY_TASKS_ENABLED).Enabled = False
        _task_properties(PROPERTY_TASKS_INTERVAL_SECONDS).Enabled = False
        _task_properties(PROPERTY_TASKS_MAX_ROWS2UPLOAD).Enabled = False

        _task_properties(PROPERTY_TASKS_ENABLED).Editable = False
        _task_properties(PROPERTY_TASKS_INTERVAL_SECONDS).Editable = False
        _task_properties(PROPERTY_TASKS_MAX_ROWS2UPLOAD).Editable = False
      End If

      '' Enable or disable CheckBox 
      If (_task_properties(GRID_TASKS_COLUMN_ENABLED).Editable) Then
        dg_tasks.Cell(_idx_row, GRID_TASKS_COLUMN_ENABLED).Value = BoolToGridValue(_is_enabled)
      Else
        dg_tasks.Cell(_idx_row, GRID_TASKS_COLUMN_ENABLED).Value = IIf(_is_enabled, uc_grid.GRID_CHK_CHECKED_DISABLED, uc_grid.GRID_CHK_UNCHECKED_DISABLED)
      End If

      ''' Set Inverval 
      FillTaskPropertyIntoGrid(_task_properties, _idx_row, GRID_TASKS_COLUMN_INTERVAL_SECONDS, dg_tasks, "ST_INTERVAL_SECONDS", _row)

      '' Set Max Rows to upload
      FillTaskPropertyIntoGrid(_task_properties, _idx_row, GRID_TASKS_COLUMN_MAX_ROWS2UPLOAD, dg_tasks, "ST_MAX_ROWS_TO_UPLOAD", _row)

      '' Set Starting hour
      FillTaskPropertyIntoGrid(_task_properties, _idx_row, GRID_TASKS_COLUMN_START_HOUR, dg_tasks, "ST_START_TIME", _row)


    Next

    Call GUI_AdjustGridScrollbars(Me.dg_tasks, GRID_TASKS_COLUMN_TASK_NAME, GRID_TASKS_HEADER_ROWS)

  End Sub ' FillTasksIntoGrid

  ' PURPOSE:  Configure task dependencies 
  '           Allow to automatically disable all depedency tasks when disallow a tasks
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  ' 
  Private Sub ConfigureTasksDependencies()

    m_grid_tasks_disable_dependencies = New Dictionary(Of TYPE_MULTISITE_SITE_TASKS, List(Of TYPE_MULTISITE_SITE_TASKS))
    m_grid_tasks_enable_dependencies = New Dictionary(Of TYPE_MULTISITE_SITE_TASKS, List(Of TYPE_MULTISITE_SITE_TASKS))

    m_grid_tasks_disable_dependencies.Add(TYPE_MULTISITE_SITE_TASKS.DownloadPoints_All, _
                                  New List(Of TYPE_MULTISITE_SITE_TASKS)(New TYPE_MULTISITE_SITE_TASKS() _
                                  {TYPE_MULTISITE_SITE_TASKS.UploadPointsMovements}))


    m_grid_tasks_disable_dependencies.Add(TYPE_MULTISITE_SITE_TASKS.DownloadPersonalInfo_All, _
                                  New List(Of TYPE_MULTISITE_SITE_TASKS)(New TYPE_MULTISITE_SITE_TASKS() _
                                  {TYPE_MULTISITE_SITE_TASKS.UploadPersonalInfo}))

    '' Disable 'MasterUsers' when disable 'MasterProfiles'
    m_grid_tasks_disable_dependencies.Add(TYPE_MULTISITE_SITE_TASKS.DownloadMasterProfiles, _
                                      New List(Of TYPE_MULTISITE_SITE_TASKS)(New TYPE_MULTISITE_SITE_TASKS() _
                                  {TYPE_MULTISITE_SITE_TASKS.DownloadCorporateUsers}))

    '' Enable 'MasterProfiles' when enable 'MasterUsers'.
    m_grid_tasks_enable_dependencies.Add(TYPE_MULTISITE_SITE_TASKS.DownloadCorporateUsers, _
                                      New List(Of TYPE_MULTISITE_SITE_TASKS)(New TYPE_MULTISITE_SITE_TASKS() _
                                  {TYPE_MULTISITE_SITE_TASKS.DownloadMasterProfiles}))

  End Sub 'ConfigureTasksDependencies

  ' PURPOSE:  Set tasks name into cell and also set their properties
  '  PARAMS:
  '     - INPUT:
  '           - Row Index
  '           - Column Index
  '           - DataTable Column Name
  '           - Task Properties
  '           - Row with data
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  ' 
  Private Sub FillTaskPropertyIntoGrid(ByRef TaskProperties As mdl_multisite.TYPE_MULTISITE_TASK_FIELD_PROPERTIES(), _
                                ByVal GridRow As Int32, _
                                ByVal GridColumn As Int32, _
                                ByRef Grid As uc_grid, _
                                ByVal DataTableColumnName As String, _
                                ByRef Row As DataRow)

    If (TaskProperties(GridColumn).Enabled) Then
      If (Not TaskProperties(GridColumn).Editable) Then
        Grid.Cell(GridRow, GridColumn).BackColor = m_not_editable_color
      End If

      Grid.Cell(GridRow, GridColumn).Value = GetDataRowColumnValue(Row, DataTableColumnName)
    Else
      Grid.Cell(GridRow, GridColumn).BackColor = m_disabled_color
      Grid.Cell(GridRow, GridColumn).Value = m_disabled_value
      Grid.Cell(GridRow, GridColumn).Alignment = uc_grid.CLASS_CELL_DATA.ENUM_ALIGN.ALIGN_CENTER
    End If
  End Sub 'FillTaskPropertyIntoGrid

  ' PURPOSE:  Check Property value is right for this task.
  '  PARAMS:
  '     - INPUT:
  '           - TaskId ( Is a TYPE_MULTISITE_SITE_TASKS )
  '           - Property ( Id from Column )
  '           - uc_grid ( to allow select row )
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  ' 
  Private Function CheckScreenDataTaskProperty(ByVal IdxGridRow As UInt16, _
                                               ByVal TaskId As TYPE_MULTISITE_SITE_TASKS, _
                                               ByVal GridColumn As Byte, _
                                               ByRef Grid As uc_grid, _
                                               ByVal MinIntervalValue As UInt16) As Boolean
    Dim _task_properties As mdl_multisite.TYPE_MULTISITE_TASK_FIELD_PROPERTIES()
    Dim _min_value As UInt32
    Dim _max_value As UInt32
    Dim _value As UInt32
    Dim _field_nls As UInt32
    Dim _is_real_time_task As Boolean
    _task_properties = Nothing
    _is_real_time_task = False

    Select Case GridColumn
      Case GRID_TASKS_COLUMN_INTERVAL_SECONDS
        _field_nls = 1800
      Case GRID_TASKS_COLUMN_MAX_ROWS2UPLOAD
        _field_nls = 1820
      Case GRID_TASKS_COLUMN_START_HOUR
        _field_nls = 1821
    End Select

    If Not m_task_field_properties.TryGetValue(TaskId, _task_properties) Then
      _task_properties = m_task_field_properties.Item(TYPE_MULTISITE_SITE_TASKS.Unknown)
    End If

    If (_task_properties(GridColumn).Enabled) Then
      _min_value = _task_properties(GridColumn).MinValue
      _max_value = _task_properties(GridColumn).MaxValue
      _is_real_time_task = _task_properties(GridColumn).RealTimeInterval

      ' Allow values with thousand separator characters to parse in a value.
      If Not UInt32.TryParse(dg_tasks.Cell(IdxGridRow, GridColumn).Value, Globalization.NumberStyles.AllowThousands, System.Threading.Thread.CurrentThread.CurrentCulture, _value) OrElse _value < 1 Then

        ' 1899 "Should set value for field '%1' in '%2' task"
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1899) _
                    , ENUM_MB_TYPE.MB_TYPE_ERROR _
                    , ENUM_MB_BTN.MB_BTN_OK _
                    , ENUM_MB_DEF_BTN.MB_DEF_BTN_1 _
                    , GLB_NLS_GUI_PLAYER_TRACKING.GetString(_field_nls) _
                    , TranslateMultiSiteProcessID(TaskId))
        Return False

      End If

      If _value < _min_value Or _value > _max_value Then

        Me.tb_panel.SelectedTab = Me.tb_tasks
        Call SelectRow(Grid, IdxGridRow)

        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1824) _
                    , ENUM_MB_TYPE.MB_TYPE_ERROR _
                    , ENUM_MB_BTN.MB_BTN_OK _
                    , ENUM_MB_DEF_BTN.MB_DEF_BTN_1 _
                    , TranslateMultiSiteProcessID(TaskId) _
                    , GLB_NLS_GUI_PLAYER_TRACKING.GetString(_field_nls) _
                    , _min_value _
                    , _max_value)

        Return False
      End If

      '' In not real time tasks check task interval
      '' not lower than Config task interval
      If (Not _is_real_time_task _
             AndAlso GridColumn = GRID_TASKS_COLUMN_INTERVAL_SECONDS _
             AndAlso _value < MinIntervalValue) Then

        Me.tb_panel.SelectedTab = Me.tb_tasks
        Call SelectRow(Grid, IdxGridRow)

        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1900) _
          , ENUM_MB_TYPE.MB_TYPE_ERROR _
          , ENUM_MB_BTN.MB_BTN_OK _
          , ENUM_MB_DEF_BTN.MB_DEF_BTN_1 _
          , TranslateMultiSiteProcessID(TaskId) _
          , TranslateMultiSiteProcessID(TYPE_MULTISITE_SITE_TASKS.DownloadConfiguration))

        Return False
      End If
    End If


    Return True

  End Function ' CheckScreenDataTaskProperty

#End Region ' Private Functions

#Region " Events "

  ' PURPOSE:  Handle OnKeydown Event
  ' Accept changes in grid when user press Intro
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  ' 
  Private Sub Form_OnKeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown

    Select Case e.KeyCode
      Case Keys.Enter
        If Me.ActiveControl.Equals(Me.dg_tasks) Then
          e.Handled = True
          Me.dg_tasks.Validate()

          Return
        End If

      Case Keys.Escape
        If Me.ActiveControl.Equals(Me.dg_tasks) Then
          e.Handled = True
          Me.dg_tasks.Validate()

          Return
        End If

        Me.Close()
    End Select
  End Sub 'Form_OnKeyDown

  ' PURPOSE: Before start editing grid
  ' To alow edit or not edit cells as defined in m_field_properties
  '   PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  ' 
  Private Sub dg_tasks_BeforeStartEditionEvent(ByVal Row As System.Int32, ByVal Column As System.Int32, ByRef EditionCanceled As System.Boolean) Handles dg_tasks.BeforeStartEditionEvent

    Dim _task As TYPE_MULTISITE_SITE_TASKS
    Dim _params() As mdl_multisite.TYPE_MULTISITE_TASK_FIELD_PROPERTIES
    _params = Nothing

    _task = CType(dg_tasks.Cell(Row, GRID_TASKS_COLUMN_TASK_ID).Value, TYPE_MULTISITE_SITE_TASKS)

    If (Not m_task_field_properties.ContainsKey(_task)) Then

      Return
    End If

    If Not m_task_field_properties.TryGetValue(_task, _params) Then
      _params = m_task_field_properties.Item(TYPE_MULTISITE_SITE_TASKS.Unknown)
    End If

    EditionCanceled = Not _params(Column).Editable Or Not _params(Column).Enabled
    If (EditionCanceled) Then

      Exit Sub
    End If

  End Sub 'dg_tasks_BeforeStartEditionEvent

  ' PURPOSE: Highlight row and focus row in frid
  '
  '  PARAMS:
  '     - INPUT:
  '         -  Grid object
  '         -  Row to hightlight
  '
  '     - OUTPUT:
  '
  ' RETURNS: 
  '
  Private Sub SelectRow(ByRef Grid As uc_grid, ByVal RowIdx As UInt16)

    '' Previous unselect current selected rows
    If (Not Grid.SelectedRows Is Nothing) Then
      For Each _idx As UInt16 In Grid.SelectedRows
        Grid.IsSelected(_idx) = False
      Next
    End If

    Grid.IsSelected(RowIdx) = True
    '' Focus & scroll down up to last row
    Grid.TopRow = RowIdx


  End Sub 'SelectRow

  ' PURPOSE: Handle Form Key events
  '
  '  PARAMS:
  '     - INPUT:
  '         -  
  '
  '     - OUTPUT:
  '
  ' RETURNS: True / False 
  '
  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean

    If Me.dg_sites.ContainsFocus Then
      Me.dg_sites.KeyPressed(sender, e)
    End If

    Return True  ' Handled true , and don't trigger default behavior
  End Function 'GUI_KeyPress

  ' PURPOSE: Controll CellDataChanged Event in Tasks Grid
  '          Allow to check or uncheck cell combo box.
  '  PARAMS:
  '     - INPUT:
  '         -  
  '
  '     - OUTPUT:
  '
  Private Sub dg_tasks_CellDataChangedEvent(ByVal Row As System.Int32, ByVal Column As System.Int32) Handles dg_tasks.CellDataChangedEvent
    Dim _changed_cell As uc_grid.CLASS_CELL_DATA
    Dim _to_checked As Boolean

    If (Column = GRID_TASKS_COLUMN_ENABLED) Then

      _changed_cell = Me.dg_tasks.Cell(Row, Column)
      _to_checked = GridValueToBool(_changed_cell.Value)

      Call dg_tasks_TaskCellCheckedChange(Row, _to_checked)

    End If

  End Sub 'dg_tasks_CellDataChangedEvent

  ' PURPOSE: Disable Tasks dependencies by disabled task
  '
  '  PARAMS:
  '     - INPUT:
  '         -  
  '
  '     - OUTPUT:
  '
  Private Sub dg_tasks_TaskCellCheckedChange(ByVal Row As UInt16, ByVal ToChecked As Boolean)

    Dim _task_id As TYPE_MULTISITE_SITE_TASKS
    Dim _dependencies As List(Of TYPE_MULTISITE_SITE_TASKS)
    Dim _idx As UInt16

    _task_id = Me.dg_tasks.Cell(Row, GRID_TASKS_COLUMN_TASK_ID).Value

    If ToChecked Then
      If Not m_grid_tasks_enable_dependencies.ContainsKey(_task_id) Then
        Exit Sub
      End If
      _dependencies = m_grid_tasks_enable_dependencies(_task_id)
    Else
      If Not m_grid_tasks_disable_dependencies.ContainsKey(_task_id) Then
        Exit Sub
      End If
      _dependencies = m_grid_tasks_disable_dependencies(_task_id)
    End If

    For _idx = 0 To Me.dg_tasks.NumRows - 1
      If (_dependencies.Contains(Me.dg_tasks.Cell(_idx, GRID_TASKS_COLUMN_TASK_ID).Value)) Then
        Me.dg_tasks.Cell(_idx, GRID_TASKS_COLUMN_ENABLED).Value = BoolToGridValue(ToChecked)
      End If
    Next

  End Sub 'dg_tasks_CellEnabled

  ' PURPOSE: Add new row to Sites Grid
  '
  '  PARAMS:
  '     - INPUT:
  '         -  
  '
  '     - OUTPUT:
  '
  Private Sub btnAddSite_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddSite.Click
    Dim _idx_row As Integer
    Const _GRID_ROW_HEIGHT As Byte = 16

    _idx_row = dg_sites.AddRow()
    m_new_sites_added.Add(_idx_row)

    Call SelectRow(dg_sites, _idx_row)
    Me.dg_sites.Row(_idx_row).BackColor = m_new_row_color



    If dg_sites.NumRows = Math.Truncate(dg_sites.Height / _GRID_ROW_HEIGHT - GRID_SITES_HEADER_ROWS) + 1 Then

      Call GUI_AdjustGridScrollbars(Me.dg_sites, GRID_SITES_COLUMN_NAME, GRID_SITES_HEADER_ROWS)
    End If

  End Sub 'btnAdd_Click

  ' PURPOSE: Control start edition event, allow to edit only SiteId task
  '  PARAMS:
  '     - INPUT:
  '         -  
  '
  '     - OUTPUT:
  '
  Private Sub dg_sites_BeforeStartEditionEvent(ByVal Row As System.Int32, ByVal Column As System.Int32, ByRef EditionCanceled As System.Boolean) Handles dg_sites.BeforeStartEditionEvent
    If Column = GRID_SITES_COLUMN_SITEID Then
      If (Not m_new_sites_added.Contains(Row)) Then

        EditionCanceled = True
        Exit Sub
      End If
    End If
  End Sub 'dg_sites_BeforeStartEditionEvent

#End Region ' Events

End Class