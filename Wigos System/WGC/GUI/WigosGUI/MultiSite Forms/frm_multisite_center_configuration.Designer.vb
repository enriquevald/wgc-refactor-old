<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_multisite_center_configuration
  Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.tb_panel = New System.Windows.Forms.TabControl
    Me.tb_cards = New System.Windows.Forms.TabPage
    Me.lbl_members_card_id = New System.Windows.Forms.Label
    Me.ef_member_card_id = New GUI_Controls.uc_entry_field
    Me.tb_directions = New System.Windows.Forms.TabPage
    Me.lbl_center_address_2 = New System.Windows.Forms.Label
    Me.lbl_center_address_1 = New System.Windows.Forms.Label
    Me.ef_center_address_1 = New GUI_Controls.uc_entry_field
    Me.ef_center_address_2 = New GUI_Controls.uc_entry_field
    Me.tb_tasks = New System.Windows.Forms.TabPage
    Me.dg_tasks = New GUI_Controls.uc_grid
    Me.tb_sites = New System.Windows.Forms.TabPage
    Me.gbSites = New System.Windows.Forms.GroupBox
    Me.btnAddSite = New System.Windows.Forms.Button
    Me.dg_sites = New GUI_Controls.uc_grid
    Me.gb_head = New System.Windows.Forms.GroupBox
    Me.lbl_site_id = New System.Windows.Forms.Label
    Me.ef_site_id = New GUI_Controls.uc_entry_field
    Me.lbl_site_name = New System.Windows.Forms.Label
    Me.ef_site_name = New GUI_Controls.uc_entry_field
    Me.panel_data.SuspendLayout()
    Me.tb_panel.SuspendLayout()
    Me.tb_cards.SuspendLayout()
    Me.tb_directions.SuspendLayout()
    Me.tb_tasks.SuspendLayout()
    Me.tb_sites.SuspendLayout()
    Me.gbSites.SuspendLayout()
    Me.gb_head.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.gb_head)
    Me.panel_data.Controls.Add(Me.tb_panel)
    Me.panel_data.Location = New System.Drawing.Point(5, 4)
    Me.panel_data.Size = New System.Drawing.Size(820, 507)
    '
    'tb_panel
    '
    Me.tb_panel.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.tb_panel.Controls.Add(Me.tb_cards)
    Me.tb_panel.Controls.Add(Me.tb_directions)
    Me.tb_panel.Controls.Add(Me.tb_tasks)
    Me.tb_panel.Controls.Add(Me.tb_sites)
    Me.tb_panel.Location = New System.Drawing.Point(3, 120)
    Me.tb_panel.Name = "tb_panel"
    Me.tb_panel.SelectedIndex = 0
    Me.tb_panel.Size = New System.Drawing.Size(811, 387)
    Me.tb_panel.TabIndex = 8
    '
    'tb_cards
    '
    Me.tb_cards.Controls.Add(Me.lbl_members_card_id)
    Me.tb_cards.Controls.Add(Me.ef_member_card_id)
    Me.tb_cards.Location = New System.Drawing.Point(4, 22)
    Me.tb_cards.Name = "tb_cards"
    Me.tb_cards.Padding = New System.Windows.Forms.Padding(3)
    Me.tb_cards.Size = New System.Drawing.Size(803, 329)
    Me.tb_cards.TabIndex = 0
    Me.tb_cards.Text = "xCards"
    Me.tb_cards.UseVisualStyleBackColor = True
    '
    'lbl_members_card_id
    '
    Me.lbl_members_card_id.AutoSize = True
    Me.lbl_members_card_id.Location = New System.Drawing.Point(11, 13)
    Me.lbl_members_card_id.Name = "lbl_members_card_id"
    Me.lbl_members_card_id.Size = New System.Drawing.Size(108, 13)
    Me.lbl_members_card_id.TabIndex = 8
    Me.lbl_members_card_id.Text = "xMembersCardID"
    '
    'ef_member_card_id
    '
    Me.ef_member_card_id.DoubleValue = 0
    Me.ef_member_card_id.IntegerValue = 0
    Me.ef_member_card_id.IsReadOnly = False
    Me.ef_member_card_id.Location = New System.Drawing.Point(8, 29)
    Me.ef_member_card_id.Name = "ef_member_card_id"
    Me.ef_member_card_id.Size = New System.Drawing.Size(368, 24)
    Me.ef_member_card_id.SufixText = "Sufix Text"
    Me.ef_member_card_id.TabIndex = 9
    Me.ef_member_card_id.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_member_card_id.TextValue = ""
    Me.ef_member_card_id.TextVisible = False
    Me.ef_member_card_id.TextWidth = 0
    Me.ef_member_card_id.Value = ""
    '
    'tb_directions
    '
    Me.tb_directions.Controls.Add(Me.lbl_center_address_2)
    Me.tb_directions.Controls.Add(Me.lbl_center_address_1)
    Me.tb_directions.Controls.Add(Me.ef_center_address_1)
    Me.tb_directions.Controls.Add(Me.ef_center_address_2)
    Me.tb_directions.Location = New System.Drawing.Point(4, 22)
    Me.tb_directions.Name = "tb_directions"
    Me.tb_directions.Padding = New System.Windows.Forms.Padding(3)
    Me.tb_directions.Size = New System.Drawing.Size(803, 329)
    Me.tb_directions.TabIndex = 2
    Me.tb_directions.Text = "xDirections"
    Me.tb_directions.UseVisualStyleBackColor = True
    '
    'lbl_center_address_2
    '
    Me.lbl_center_address_2.AutoSize = True
    Me.lbl_center_address_2.Location = New System.Drawing.Point(11, 63)
    Me.lbl_center_address_2.Name = "lbl_center_address_2"
    Me.lbl_center_address_2.Size = New System.Drawing.Size(106, 13)
    Me.lbl_center_address_2.TabIndex = 16
    Me.lbl_center_address_2.Text = "xCenterAddress2"
    '
    'lbl_center_address_1
    '
    Me.lbl_center_address_1.AutoSize = True
    Me.lbl_center_address_1.Location = New System.Drawing.Point(11, 13)
    Me.lbl_center_address_1.Name = "lbl_center_address_1"
    Me.lbl_center_address_1.Size = New System.Drawing.Size(106, 13)
    Me.lbl_center_address_1.TabIndex = 14
    Me.lbl_center_address_1.Text = "xCenterAddress1"
    '
    'ef_center_address_1
    '
    Me.ef_center_address_1.DoubleValue = 0
    Me.ef_center_address_1.IntegerValue = 0
    Me.ef_center_address_1.IsReadOnly = False
    Me.ef_center_address_1.Location = New System.Drawing.Point(8, 29)
    Me.ef_center_address_1.Name = "ef_center_address_1"
    Me.ef_center_address_1.Size = New System.Drawing.Size(368, 24)
    Me.ef_center_address_1.SufixText = "Sufix Text"
    Me.ef_center_address_1.TabIndex = 15
    Me.ef_center_address_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_center_address_1.TextValue = ""
    Me.ef_center_address_1.TextVisible = False
    Me.ef_center_address_1.TextWidth = 0
    Me.ef_center_address_1.Value = ""
    '
    'ef_center_address_2
    '
    Me.ef_center_address_2.DoubleValue = 0
    Me.ef_center_address_2.IntegerValue = 0
    Me.ef_center_address_2.IsReadOnly = False
    Me.ef_center_address_2.Location = New System.Drawing.Point(8, 79)
    Me.ef_center_address_2.Name = "ef_center_address_2"
    Me.ef_center_address_2.Size = New System.Drawing.Size(368, 24)
    Me.ef_center_address_2.SufixText = "Sufix Text"
    Me.ef_center_address_2.TabIndex = 17
    Me.ef_center_address_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_center_address_2.TextValue = ""
    Me.ef_center_address_2.TextVisible = False
    Me.ef_center_address_2.TextWidth = 0
    Me.ef_center_address_2.Value = ""
    '
    'tb_tasks
    '
    Me.tb_tasks.Controls.Add(Me.dg_tasks)
    Me.tb_tasks.Location = New System.Drawing.Point(4, 22)
    Me.tb_tasks.Name = "tb_tasks"
    Me.tb_tasks.Padding = New System.Windows.Forms.Padding(3)
    Me.tb_tasks.Size = New System.Drawing.Size(803, 361)
    Me.tb_tasks.TabIndex = 1
    Me.tb_tasks.Text = "xTasks"
    Me.tb_tasks.UseVisualStyleBackColor = True
    '
    'dg_tasks
    '
    Me.dg_tasks.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.dg_tasks.CurrentCol = -1
    Me.dg_tasks.CurrentRow = -1
    Me.dg_tasks.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_tasks.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_tasks.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_tasks.Location = New System.Drawing.Point(1, 0)
    Me.dg_tasks.Name = "dg_tasks"
    Me.dg_tasks.PanelRightVisible = False
    Me.dg_tasks.Redraw = True
    Me.dg_tasks.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_tasks.Size = New System.Drawing.Size(802, 358)
    Me.dg_tasks.Sortable = False
    Me.dg_tasks.SortAscending = True
    Me.dg_tasks.SortByCol = 0
    Me.dg_tasks.TabIndex = 0
    Me.dg_tasks.ToolTipped = True
    Me.dg_tasks.TopRow = -2
    '
    'tb_sites
    '
    Me.tb_sites.Controls.Add(Me.gbSites)
    Me.tb_sites.Location = New System.Drawing.Point(4, 22)
    Me.tb_sites.Name = "tb_sites"
    Me.tb_sites.Padding = New System.Windows.Forms.Padding(3)
    Me.tb_sites.Size = New System.Drawing.Size(803, 361)
    Me.tb_sites.TabIndex = 3
    Me.tb_sites.Text = "xSites"
    Me.tb_sites.UseVisualStyleBackColor = True
    '
    'gbSites
    '
    Me.gbSites.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.gbSites.Controls.Add(Me.btnAddSite)
    Me.gbSites.Controls.Add(Me.dg_sites)
    Me.gbSites.Location = New System.Drawing.Point(8, 4)
    Me.gbSites.Name = "gbSites"
    Me.gbSites.Size = New System.Drawing.Size(787, 351)
    Me.gbSites.TabIndex = 4
    Me.gbSites.TabStop = False
    Me.gbSites.Text = "xAlgo"
    '
    'btnAddSite
    '
    Me.btnAddSite.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.btnAddSite.Location = New System.Drawing.Point(707, 321)
    Me.btnAddSite.Name = "btnAddSite"
    Me.btnAddSite.Size = New System.Drawing.Size(63, 21)
    Me.btnAddSite.TabIndex = 4
    Me.btnAddSite.Text = "xAdd"
    Me.btnAddSite.UseVisualStyleBackColor = True
    '
    'dg_sites
    '
    Me.dg_sites.CurrentCol = -1
    Me.dg_sites.CurrentRow = -1
    Me.dg_sites.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_sites.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_sites.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_sites.Location = New System.Drawing.Point(4, 17)
    Me.dg_sites.Name = "dg_sites"
    Me.dg_sites.PanelRightVisible = False
    Me.dg_sites.Redraw = True
    Me.dg_sites.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_sites.Size = New System.Drawing.Size(697, 325)
    Me.dg_sites.Sortable = False
    Me.dg_sites.SortAscending = True
    Me.dg_sites.SortByCol = 0
    Me.dg_sites.TabIndex = 3
    Me.dg_sites.ToolTipped = True
    Me.dg_sites.TopRow = -2
    '
    'gb_head
    '
    Me.gb_head.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.gb_head.Controls.Add(Me.lbl_site_id)
    Me.gb_head.Controls.Add(Me.ef_site_id)
    Me.gb_head.Controls.Add(Me.lbl_site_name)
    Me.gb_head.Controls.Add(Me.ef_site_name)
    Me.gb_head.Location = New System.Drawing.Point(3, 3)
    Me.gb_head.Name = "gb_head"
    Me.gb_head.Size = New System.Drawing.Size(812, 111)
    Me.gb_head.TabIndex = 9
    Me.gb_head.TabStop = False
    Me.gb_head.Text = "xMultisite"
    '
    'lbl_site_id
    '
    Me.lbl_site_id.AutoSize = True
    Me.lbl_site_id.Location = New System.Drawing.Point(8, 17)
    Me.lbl_site_id.Name = "lbl_site_id"
    Me.lbl_site_id.Size = New System.Drawing.Size(50, 13)
    Me.lbl_site_id.TabIndex = 8
    Me.lbl_site_id.Text = "xSiteID"
    '
    'ef_site_id
    '
    Me.ef_site_id.DoubleValue = 0
    Me.ef_site_id.IntegerValue = 0
    Me.ef_site_id.IsReadOnly = False
    Me.ef_site_id.Location = New System.Drawing.Point(5, 33)
    Me.ef_site_id.Name = "ef_site_id"
    Me.ef_site_id.Size = New System.Drawing.Size(53, 24)
    Me.ef_site_id.SufixText = "Sufix Text"
    Me.ef_site_id.SufixTextVisible = True
    Me.ef_site_id.TabIndex = 9
    Me.ef_site_id.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_site_id.TextValue = ""
    Me.ef_site_id.TextWidth = 0
    Me.ef_site_id.Value = ""
    '
    'lbl_site_name
    '
    Me.lbl_site_name.AutoSize = True
    Me.lbl_site_name.Location = New System.Drawing.Point(8, 62)
    Me.lbl_site_name.Name = "lbl_site_name"
    Me.lbl_site_name.Size = New System.Drawing.Size(69, 13)
    Me.lbl_site_name.TabIndex = 10
    Me.lbl_site_name.Text = "xSiteName"
    '
    'ef_site_name
    '
    Me.ef_site_name.DoubleValue = 0
    Me.ef_site_name.IntegerValue = 0
    Me.ef_site_name.IsReadOnly = False
    Me.ef_site_name.Location = New System.Drawing.Point(5, 78)
    Me.ef_site_name.Name = "ef_site_name"
    Me.ef_site_name.Size = New System.Drawing.Size(368, 24)
    Me.ef_site_name.SufixText = "Sufix Text"
    Me.ef_site_name.SufixTextVisible = True
    Me.ef_site_name.TabIndex = 11
    Me.ef_site_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_site_name.TextValue = ""
    Me.ef_site_name.TextWidth = 0
    Me.ef_site_name.Value = ""
    '
    'frm_multisite_center_configuration
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(924, 516)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_multisite_center_configuration"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_multisite_center_configuration"
    Me.panel_data.ResumeLayout(False)
    Me.tb_panel.ResumeLayout(False)
    Me.tb_cards.ResumeLayout(False)
    Me.tb_cards.PerformLayout()
    Me.tb_directions.ResumeLayout(False)
    Me.tb_directions.PerformLayout()
    Me.tb_tasks.ResumeLayout(False)
    Me.tb_sites.ResumeLayout(False)
    Me.gbSites.ResumeLayout(False)
    Me.gb_head.ResumeLayout(False)
    Me.gb_head.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents tb_panel As System.Windows.Forms.TabControl
  Friend WithEvents tb_cards As System.Windows.Forms.TabPage
  Friend WithEvents tb_tasks As System.Windows.Forms.TabPage
  Friend WithEvents dg_tasks As GUI_Controls.uc_grid
  Friend WithEvents lbl_members_card_id As System.Windows.Forms.Label
  Friend WithEvents ef_member_card_id As GUI_Controls.uc_entry_field
  Friend WithEvents tb_directions As System.Windows.Forms.TabPage
  Friend WithEvents lbl_center_address_2 As System.Windows.Forms.Label
  Friend WithEvents lbl_center_address_1 As System.Windows.Forms.Label
  Friend WithEvents ef_center_address_1 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_center_address_2 As GUI_Controls.uc_entry_field
  Friend WithEvents gb_head As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_site_id As System.Windows.Forms.Label
  Friend WithEvents ef_site_id As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_site_name As System.Windows.Forms.Label
  Friend WithEvents ef_site_name As GUI_Controls.uc_entry_field
  Friend WithEvents tb_sites As System.Windows.Forms.TabPage
  Friend WithEvents gbSites As System.Windows.Forms.GroupBox
  Friend WithEvents btnAddSite As System.Windows.Forms.Button
  Friend WithEvents dg_sites As GUI_Controls.uc_grid
End Class
