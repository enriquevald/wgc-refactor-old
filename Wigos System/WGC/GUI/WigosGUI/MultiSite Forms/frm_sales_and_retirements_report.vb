﻿'-------------------------------------------------------------------
' Copyright © 2015 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_sales_and_retirements_report.vb
' DESCRIPTION:   Sales and retirements report
' AUTHOR:        José Martínez  
' CREATION DATE: 29-SEP-2015
'
' REVISION HISTORY:
'
' Date         Author   Description
' -----------  ------   -----------------------------------------------
' 29-SEP-2015  JML      First Release
'----------------------------------------------------------------------
Option Explicit On
Option Strict Off

#Region " Imports "

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports WSI.Common
Imports System.Text
Imports System.Data
Imports System.Data.SqlClient
Imports System.Text.RegularExpressions

#End Region


Public Class frm_sales_and_retirements_report
  Inherits frm_base_sel

#Region " Constants "

  ' SQL Columnds
  Private Const SQL_COLUMN_SITE_ID As Integer = 0
  Private Const SQL_COLUMN_OPERATION_ID As Integer = 1
  Private Const SQL_COLUMN_ACCOUNT_NAME As Integer = 2
  Private Const SQL_COLUMN_ACCOUNT_ID As Integer = 3
  Private Const SQL_COLUMN_EXTERNAL_CARD_NUMBER As Integer = 4
  Private Const SQL_COLUMN_DATETIME As Integer = 5
  Private Const SQL_COLUMN_OPERATION_TYPE As Integer = 6
  Private Const SQL_COLUMN_TOTAL_CASH_IN As Integer = 7
  Private Const SQL_COLUMN_CASH_IN_SPLIT1 As Integer = 8
  Private Const SQL_COLUMN_CASH_IN_SPLIT2 As Integer = 9
  Private Const SQL_COLUMN_CASH_IN_TAX1 As Integer = 10
  Private Const SQL_COLUMN_CASH_IN_TAX2 As Integer = 11
  Private Const SQL_COLUMN_PAYMENT_TYPE_CODE As Integer = 12
  Private Const SQL_COLUMN_TOTAL_CASH_OUT As Integer = 13
  Private Const SQL_COLUMN_DEVOLUTION As Integer = 14
  Private Const SQL_COLUMN_PRIZE As Integer = 15
  Private Const SQL_COLUMN_ISR1 As Integer = 16
  Private Const SQL_COLUMN_ISR2 As Integer = 17
  Private Const SQL_COLUMN_SERVICE_CHARGE As Integer = 18
  Private Const SQL_COLUMN_DECIMAL_ROUNDING As Integer = 19
  Private Const SQL_COLUMN_RETENTION_ROUNDING As Integer = 20
  Private Const SQL_COLUMN_EVIDENCE As Integer = 21
  Private Const SQL_COLUMN_CASH_OUT_MONEY As Integer = 22
  Private Const SQL_COLUMN_CASH_OUT_CHECK As Integer = 23

  ' Grid Columns
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_SITE_ID As Integer = 1
  Private Const GRID_COLUMN_OPERATION_ID As Integer = 2
  Private Const GRID_COLUMN_DATETIME As Integer = 3
  Private Const GRID_COLUMN_OPERATION_TYPE As Integer = 4
  Private Const GRID_COLUMN_ACCOUNT_ID As Integer = 5
  Private Const GRID_COLUMN_EXTERNAL_CARD_NUMBER As Integer = 6
  Private Const GRID_COLUMN_ACCOUNT_NAME As Integer = 7
  Private Const GRID_COLUMN_TOTAL_CASH_IN As Integer = 8
  Private Const GRID_COLUMN_CASH_IN_SPLIT1 As Integer = 9
  Private Const GRID_COLUMN_CASH_IN_SPLIT2 As Integer = 10
  Private Const GRID_COLUMN_CASH_IN_TAX1 As Integer = 11
  Private Const GRID_COLUMN_CASH_IN_TAX2 As Integer = 12
  Private Const GRID_COLUMN_PAYMENT_TYPE_CODE As Integer = 13
  Private Const GRID_COLUMN_TOTAL_CASH_OUT As Integer = 14
  Private Const GRID_COLUMN_DEVOLUTION As Integer = 15
  Private Const GRID_COLUMN_PRIZE As Integer = 16
  Private Const GRID_COLUMN_ISR1 As Integer = 17
  Private Const GRID_COLUMN_ISR2 As Integer = 18
  Private Const GRID_COLUMN_SERVICE_CHARGE As Integer = 19
  Private Const GRID_COLUMN_DECIMAL_ROUNDING As Integer = 20
  Private Const GRID_COLUMN_RETENTION_ROUNDING As Integer = 21
  Private Const GRID_COLUMN_EVIDENCE As Integer = 22
  Private Const GRID_COLUMN_CASH_OUT_MONEY As Integer = 23
  Private Const GRID_COLUMN_CASH_OUT_CHECK As Integer = 24

  Private Const GRID_COLUMNS As Integer = 25
  Private Const GRID_HEADER_ROWS As Integer = 2

  Private Const GRID_WIDTH_INDEX As Integer = 200
  Private Const GRID_WIDTH_SITE As Integer = 800
  Private Const GRID_WIDTH_OPERATION_ID As Integer = 1500
  Private Const GRID_WIDTH_OPERATION As Integer = 2000
  Private Const GRID_WIDTH_ACCOUNT As Integer = 2000
  Private Const GRID_WIDTH_ACCOUNT_NAME As Integer = 3000
  Private Const GRID_WIDTH_CARD_NUMBER As Integer = 3000
  Private Const GRID_WIDTH_DATE As Integer = 2000
  Private Const GRID_WIDTH_AMOUNT As Integer = 2200


#End Region ' Constants

#Region " Enums "

#End Region ' Enums

#Region " Members "

  ' Filter variables for report
  Private m_filter_sites As String
  Private m_date_from As String
  Private m_date_to As String
  Private m_operations As String
  Private m_pay_mode As String

#End Region

#Region " OVERRIDES "

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_SALES_AND_RETIREMENTS_REPORT
    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  Protected Overrides Function GUI_MaxRows() As Integer
    Return 10000
  End Function 'GUI_MaxRows

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()
    MyBase.GUI_InitControls()

    ' Set form Name
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6736)

    ' Buttons not needed
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False

    ' Site filter
    Me.uc_sites_sel.ShowMultisiteRow = False
    Me.uc_sites_sel.MultiSelect = False
    Me.uc_sites_sel.Init()
    Me.uc_sites_sel.Height = 130
    Me.uc_sites_sel.SetDefaultValues(False)

    ' Date time filter
    Me.uc_daily_session_selector.Init(GLB_NLS_GUI_INVOICING.Id(201))

    ' Operations
    Me.uc_checked_list_operations.GroupBoxText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6685)
    Me.uc_checked_list_operations.ColumnWidth(uc_checked_list.GRID_COLUMN_DESC, 3000)
    Me.uc_checked_list_operations.btn_check_all.Width = 120
    Me.uc_checked_list_operations.btn_uncheck_all.Width = 120
    Me.uc_checked_list_operations.m_resize_width = 290
    Me.uc_checked_list_operations.SetLevels = 2

    ' Payment mode
    Me.uc_checked_list_payment_type.GroupBoxText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6780)
    Me.uc_checked_list_payment_type.ColumnWidth(uc_checked_list.GRID_COLUMN_DESC, 3000)
    Me.uc_checked_list_payment_type.btn_check_all.Width = 120
    Me.uc_checked_list_payment_type.btn_uncheck_all.Width = 120
    Me.uc_checked_list_payment_type.m_resize_width = 290
    Me.uc_checked_list_payment_type.Visible = False

    ' Operations & Payment mode
    Call FillTableTypeFilterGrid()

    Call SetDefaultValues()

    Call GUI_StyleSheet()
  End Sub

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  ' PURPOSE: To check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Site selection
    If Not Me.uc_sites_sel.FilterCheckSites() Then
      Return False
    End If

    ' Dates selection 
    If Me.uc_daily_session_selector.FromDateSelected And Me.uc_daily_session_selector.ToDateSelected Then
      If Me.uc_daily_session_selector.FromDate > Me.uc_daily_session_selector.ToDate Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.uc_daily_session_selector.Focus()

        Return False
      End If
    End If

    Return True
  End Function ' GUI_FilterCheck

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim _str_sql As StringBuilder
    Dim _str_where As String

    _str_where = GetSqlWhere()

    _str_sql = New StringBuilder()
    _str_sql.AppendLine(" SELECT   TWD_SITE_ID              AS VenueCod ")
    _str_sql.AppendLine("        , TWD_OPERATION_ID         AS TicketNumber ")
    _str_sql.AppendLine("        , AC_HOLDER_NAME           AS HolderName ")
    _str_sql.AppendLine("        , TWD_ACCOUNT_ID           AS AccountId ")
    _str_sql.AppendLine("        , TWD_EXTERNAL_CARD_NUMBER AS IncCardNumber ")
    _str_sql.AppendLine("        , TWD_DATETIME             AS FechaHoraEvento ")
    _str_sql.AppendLine("        , TWD_OPERATION_TYPE       AS TipoEvento ")
    _str_sql.AppendLine("        , TWD_TOTAL_CASH_IN        AS RecargaTotal ")
    _str_sql.AppendLine("        , TWD_CASH_IN_SPLIT1       AS RecargaEmpresaA ")
    _str_sql.AppendLine("        , TWD_CASH_IN_SPLIT2       AS RecargaEmpresaB ")
    _str_sql.AppendLine("        , TWD_CASH_IN_TAX1         AS RecargaImpuesto1 ")
    _str_sql.AppendLine("        , TWD_CASH_IN_TAX2         AS RecargaImpuesto2 ")
    _str_sql.AppendLine("        , TWD_PAYMENT_TYPE_CODE    AS RecargaModoDePago ")
    _str_sql.AppendLine("        , TWD_TOTAL_CASH_OUT       AS RetiroTotal ")
    _str_sql.AppendLine("        , TWD_DEVOLUTION           AS RetiroDevolucion ")
    _str_sql.AppendLine("        , TWD_PRIZE                AS RetiroPremio ")
    _str_sql.AppendLine("        , TWD_ISR1                 AS RetiroImpuesto1 ")
    _str_sql.AppendLine("        , TWD_ISR2                 AS RetiroImpuesto2 ")
    _str_sql.AppendLine("        , TWD_SERVICE_CHARGE       AS RetiroCargoPorServicio ")
    _str_sql.AppendLine("        , TWD_DECIMAL_ROUNDING     AS RetiroRedondeoPorDecimales ")
    _str_sql.AppendLine("        , TWD_RETENTION_ROUNDING   AS RetiroRedondeoPorRetencion ")
    _str_sql.AppendLine("        , TWD_EVIDENCE             AS RetiroConstancia ")
    _str_sql.AppendLine("        , TWD_CASH_OUT_MONEY       AS RetiroTotalPagadoEnEfectivo ")
    _str_sql.AppendLine("        , TWD_CASH_OUT_CHECK       AS RetiroTotalPagadoEnCheque ")
    _str_sql.AppendLine("   FROM   TASK68_WORK_DATA WITH( INDEX(IDX_TWD_DATETIME_TWD_SITE_ID_TWD_OPERATION_ID)) ")
    _str_sql.AppendLine("   LEFT   JOIN ACCOUNTS    ON AC_ACCOUNT_ID = TWD_ACCOUNT_ID ")
    _str_sql.AppendLine(_str_where)
    _str_sql.AppendLine("  ORDER   BY TWD_SITE_ID ")
    _str_sql.AppendLine("        , TWD_OPERATION_ID ")

    Return _str_sql.ToString()

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE: Set focus.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.uc_daily_session_selector
  End Sub ' GUI_SetInitialFocus

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  '
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _gross_amount As Decimal
    Dim _is_anulation As Boolean
    Dim _anulation_prefix As String
    Dim _operation_type_str As String
    Dim _pay_mode As String
    Dim _track_data As String

    _is_anulation = False
    _anulation_prefix = String.Empty
    _operation_type_str = String.Empty

    ' SITE ID
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SITE_ID).Value = DbRow.Value(SQL_COLUMN_SITE_ID)

    ' OPERATION_ID
    If Not DbRow.Value(SQL_COLUMN_OPERATION_ID) Is DBNull.Value Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_OPERATION_ID).Value = DbRow.Value(SQL_COLUMN_OPERATION_ID)
    End If

    ' DATETIME
    If Not DbRow.Value(SQL_COLUMN_DATETIME) Is DBNull.Value Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DATETIME).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_DATETIME), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    ' ACCOUNT_ID 
    If Not DbRow.Value(SQL_COLUMN_ACCOUNT_ID) Is DBNull.Value Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_ID).Value = DbRow.Value(SQL_COLUMN_ACCOUNT_ID)
    End If

    ' EXTERNAL_CARD_NUMBER
    _track_data = ""
    If Not DbRow.Value(SQL_COLUMN_EXTERNAL_CARD_NUMBER) Is DBNull.Value Then
      _track_data = DbRow.Value(SQL_COLUMN_EXTERNAL_CARD_NUMBER).ToString()
    End If
    Do While _track_data.Length < 20 And _track_data.Length > 0
      _track_data = "0" + _track_data
    Loop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_EXTERNAL_CARD_NUMBER).Value = _track_data

    ' ACCOUNT_NAME
    If Not DbRow.Value(SQL_COLUMN_ACCOUNT_NAME) Is DBNull.Value Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_NAME).Value = DbRow.Value(SQL_COLUMN_ACCOUNT_NAME)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_NAME).Value = "- - -"
    End If

    'TOTAL_CASH_IN 
    If Not DbRow.Value(SQL_COLUMN_TOTAL_CASH_IN) Is DBNull.Value Then
      _gross_amount = DbRow.Value(SQL_COLUMN_TOTAL_CASH_IN)
      If _gross_amount < 0 Then
        _is_anulation = True
      End If
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_CASH_IN).Value = GUI_FormatCurrency(_gross_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    ' CASH_IN_SPLIT1 
    If Not DbRow.Value(SQL_COLUMN_CASH_IN_SPLIT1) Is DBNull.Value Then
      _gross_amount = DbRow.Value(SQL_COLUMN_CASH_IN_SPLIT1)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CASH_IN_SPLIT1).Value = GUI_FormatCurrency(_gross_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    ' CASH_IN_SPLIT2 
    If Not DbRow.Value(SQL_COLUMN_CASH_IN_SPLIT2) Is DBNull.Value Then
      _gross_amount = DbRow.Value(SQL_COLUMN_CASH_IN_SPLIT2)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CASH_IN_SPLIT2).Value = GUI_FormatCurrency(_gross_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    ' CASH_IN_TAX1 
    If Not DbRow.Value(SQL_COLUMN_CASH_IN_TAX1) Is DBNull.Value Then
      _gross_amount = DbRow.Value(SQL_COLUMN_CASH_IN_TAX1)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CASH_IN_TAX1).Value = GUI_FormatCurrency(_gross_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    ' CASH_IN_TAX2 
    If Not DbRow.Value(SQL_COLUMN_CASH_IN_TAX2) Is DBNull.Value Then
      _gross_amount = DbRow.Value(SQL_COLUMN_CASH_IN_TAX2)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CASH_IN_TAX2).Value = GUI_FormatCurrency(_gross_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    ' PAYMENT_TYPE_CODE  
    If Not DbRow.Value(SQL_COLUMN_PAYMENT_TYPE_CODE) Is DBNull.Value Then
      _pay_mode = TranslatePayMode(DbRow.Value(SQL_COLUMN_PAYMENT_TYPE_CODE))
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PAYMENT_TYPE_CODE).Value = _pay_mode
    End If

    ' TOTAL_CASH_OUT 
    If Not DbRow.Value(SQL_COLUMN_TOTAL_CASH_OUT) Is DBNull.Value Then
      _gross_amount = DbRow.Value(SQL_COLUMN_TOTAL_CASH_OUT)
      If _gross_amount < 0 Then
        _is_anulation = True
      End If
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_CASH_OUT).Value = GUI_FormatCurrency(_gross_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    ' DEVOLUTION 
    If Not DbRow.Value(SQL_COLUMN_DEVOLUTION) Is DBNull.Value Then
      _gross_amount = DbRow.Value(SQL_COLUMN_DEVOLUTION)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DEVOLUTION).Value = GUI_FormatCurrency(_gross_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    ' PRIZE  
    If Not DbRow.Value(SQL_COLUMN_PRIZE) Is DBNull.Value Then
      _gross_amount = DbRow.Value(SQL_COLUMN_PRIZE)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PRIZE).Value = GUI_FormatCurrency(_gross_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    ' ISR1  
    If Not DbRow.Value(SQL_COLUMN_ISR1) Is DBNull.Value Then
      _gross_amount = DbRow.Value(SQL_COLUMN_ISR1)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ISR1).Value = GUI_FormatCurrency(_gross_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    ' ISR2  
    If Not DbRow.Value(SQL_COLUMN_ISR2) Is DBNull.Value Then
      _gross_amount = DbRow.Value(SQL_COLUMN_ISR2)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ISR2).Value = GUI_FormatCurrency(_gross_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    ' SERVICE_CHARGE  
    If Not DbRow.Value(SQL_COLUMN_SERVICE_CHARGE) Is DBNull.Value Then
      _gross_amount = DbRow.Value(SQL_COLUMN_SERVICE_CHARGE)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SERVICE_CHARGE).Value = GUI_FormatCurrency(_gross_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    ' DECIMAL_ROUNDING  
    If Not DbRow.Value(SQL_COLUMN_DECIMAL_ROUNDING) Is DBNull.Value Then
      _gross_amount = DbRow.Value(SQL_COLUMN_DECIMAL_ROUNDING)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DECIMAL_ROUNDING).Value = GUI_FormatCurrency(_gross_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    ' RETENTION_ROUNDING  
    If Not DbRow.Value(SQL_COLUMN_RETENTION_ROUNDING) Is DBNull.Value Then
      _gross_amount = DbRow.Value(SQL_COLUMN_RETENTION_ROUNDING)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_RETENTION_ROUNDING).Value = GUI_FormatCurrency(_gross_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    ' EVIDENCE  
    If Not DbRow.Value(SQL_COLUMN_EVIDENCE) Is DBNull.Value Then
      _gross_amount = DbRow.Value(SQL_COLUMN_EVIDENCE)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_EVIDENCE).Value = GUI_FormatCurrency(_gross_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    ' CASH_OUT_MONEY 
    If Not DbRow.Value(SQL_COLUMN_CASH_OUT_MONEY) Is DBNull.Value Then
      _gross_amount = DbRow.Value(SQL_COLUMN_CASH_OUT_MONEY)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CASH_OUT_MONEY).Value = GUI_FormatCurrency(_gross_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    ' CASH_OUT_CHECK  
    If Not DbRow.Value(SQL_COLUMN_CASH_OUT_CHECK) Is DBNull.Value Then
      _gross_amount = DbRow.Value(SQL_COLUMN_CASH_OUT_CHECK)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CASH_OUT_CHECK).Value = GUI_FormatCurrency(_gross_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    ' OPERATION_TYPE 
    If _is_anulation = True Then
      _anulation_prefix = "(" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2786) & ") "
    End If
    _operation_type_str = TranslateOperationType(DbRow.Value(SQL_COLUMN_OPERATION_TYPE).ToString())         ' TranslateOperationType(DbRow.Value(SQL_COLUMN_OPERATION_TYPE).ToString())

    If Not DbRow.Value(SQL_COLUMN_OPERATION_TYPE) Is DBNull.Value Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_OPERATION_TYPE).Value = _anulation_prefix & _operation_type_str
    End If

    Return True

  End Function ' GUI_SetupRow


#End Region ' OVERRIDES

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " GUI Reports "

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(202), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(203), m_date_to)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1803), m_filter_sites)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6685), m_operations)

  End Sub ' GUI_ReportFilter

  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(PrintData)
    PrintData.Params.Title = GLB_NLS_GUI_INVOICING.GetString(204)
    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_PORTRAIT

  End Sub ' GUI_ReportParams

  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_date_from = ""
    m_date_to = ""
    m_filter_sites = ""
    m_operations = ""

    'Sites
    If Not String.IsNullOrEmpty(uc_sites_sel.GetSitesIdListSelected()) Then
      m_filter_sites = Me.uc_sites_sel.GetSitesIdListSelectedToPrint()
    Else
      m_filter_sites = GLB_NLS_GUI_ALARMS.GetString(282)
    End If

    'Date 
    If Me.uc_daily_session_selector.FromDateSelected Then
      m_date_from = GUI_FormatDate(Me.uc_daily_session_selector.FromDate.AddHours(Me.uc_daily_session_selector.ClosingTime), _
                                   ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                   ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    If Me.uc_daily_session_selector.ToDateSelected Then
      m_date_to = GUI_FormatDate(Me.uc_daily_session_selector.ToDate.AddHours(Me.uc_daily_session_selector.ClosingTime), _
                                 ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                 ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    ' Operations 
    If uc_checked_list_operations.Count > 0 Then
      m_operations = uc_checked_list_operations.SelectedValuesList
    End If

    ' Pay mode 
    If uc_checked_list_payment_type.Count > 0 Then
      m_pay_mode = uc_checked_list_payment_type.SelectedValuesList
    End If

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#Region " Private Functions "

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()

    Dim from_date As DateTime
    Dim closing_time As Integer

    closing_time = GetDefaultClosingTime()

    from_date = New DateTime(Now.Year, Now.Month, Now.Day)
    Me.uc_daily_session_selector.FromDate = from_date.AddDays(-1)
    Me.uc_daily_session_selector.FromDateSelected = True

    Me.uc_daily_session_selector.ToDate = Me.uc_daily_session_selector.FromDate
    Me.uc_daily_session_selector.ToDateSelected = False

    Me.uc_daily_session_selector.ClosingTime = closing_time

    Me.uc_sites_sel.SetDefaultValues(False)

  End Sub ' SetDefaultValues

  ' PURPOSE: Fill the filter Grid with data
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  ' NOTES:
  '    TipoEvento Descripción
  '1:  Recarga()
  '2:  Retiro()
  '3	Recarga con promoción
  '4	Recarga Banco Móvil
  '10	Recarga por puntos
  '14	Venta de fichas con recarga
  '16	Compra de fichas con reintegro
  '109	Recarga en validador de billetes
  Private Sub FillTableTypeFilterGrid()

    'Operations
    Call Me.uc_checked_list_operations.Clear()
    Call Me.uc_checked_list_operations.ReDraw(False)

    ' sales
    Me.uc_checked_list_operations.Add(1, 0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6764), True)


    If TITO.Utils.IsTitoMode() Then
      Me.uc_checked_list_operations.Add(2, WSI.Common.OperationCode.CASH_IN, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2687), True)
    Else
      Me.uc_checked_list_operations.Add(2, WSI.Common.OperationCode.CASH_IN, GLB_NLS_GUI_PLAYER_TRACKING.GetString(607), True)
    End If

    Me.uc_checked_list_operations.Add(2, WSI.Common.OperationCode.PROMOTION, GLB_NLS_GUI_PLAYER_TRACKING.GetString(609), True)
    Me.uc_checked_list_operations.Add(2, WSI.Common.OperationCode.MB_CASH_IN, GLB_NLS_GUI_PLAYER_TRACKING.GetString(610), True)
    Me.uc_checked_list_operations.Add(2, WSI.Common.OperationCode.GIFT_REDEEMABLE_AS_CASHIN, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2510), True)
    Me.uc_checked_list_operations.Add(2, WSI.Common.OperationCode.NA_CASH_IN, GLB_NLS_GUI_PLAYER_TRACKING.GetString(623), True)

    'Me.uc_checked_list_operations.Add(2, WSI.Common.OperationCode.CHIPS_SALE, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4311), True)
    Me.uc_checked_list_operations.Add(2, WSI.Common.OperationCode.CHIPS_SALE_WITH_RECHARGE, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4312), True)
    'Me.uc_checked_list_operations.Add(2, WSI.Common.OperationCode.CHIPS_SALE, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6600), True)
    'Me.uc_checked_list_operations.Add(2, WSI.Common.OperationCode.CHIPS_SALE_WITH_RECHARGE, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6607), True)


    ' purchases
    Me.uc_checked_list_operations.Add(1, 1, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6765), True)

    If TITO.Utils.IsTitoMode() Then
      Me.uc_checked_list_operations.Add(2, WSI.Common.OperationCode.CASH_OUT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4607), True)
    Else
      Me.uc_checked_list_operations.Add(2, WSI.Common.OperationCode.CASH_OUT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5865), True)
    End If

    'Me.uc_checked_list_operations.Add(2, WSI.Common.OperationCode.CHIPS_PURCHASE, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4310), True)
    Me.uc_checked_list_operations.Add(2, WSI.Common.OperationCode.CHIPS_PURCHASE_WITH_CASH_OUT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4313), True)
    'Me.uc_checked_list_operations.Add(2, WSI.Common.OperationCode.CHIPS_PURCHASE, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6610), True)
    'Me.uc_checked_list_operations.Add(2, WSI.Common.OperationCode.CHIPS_PURCHASE_WITH_CASH_OUT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6611), True)


    If Me.uc_checked_list_operations.Count() > 0 Then
      Call Me.uc_checked_list_operations.CurrentRow(0)
    End If

    Call Me.uc_checked_list_operations.ReDraw(True)
    Call Me.uc_checked_list_operations.CollapseAll()

    'Pay type
    Call Me.uc_checked_list_payment_type.Clear()
    Call Me.uc_checked_list_payment_type.ReDraw(False)

    Me.uc_checked_list_payment_type.Add(0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6775))
    Me.uc_checked_list_payment_type.Add(1, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6776))
    Me.uc_checked_list_payment_type.Add(2, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6777))
    Me.uc_checked_list_payment_type.Add(3, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6778))
    Me.uc_checked_list_payment_type.Add(4, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6779))

    If Me.uc_checked_list_payment_type.Count() > 0 Then
      Call Me.uc_checked_list_payment_type.CurrentRow(0)
    End If

    Call Me.uc_checked_list_payment_type.ReDraw(True)

  End Sub 'FillTableTypeFilterGrid

  ' PURPOSE: Gets the movement name and options of each kind of movement type
  '
  '  PARAMS:
  '     - INPUT:
  '           - operation_type: the Code of the operation
  '     - OUTPUT:
  '           - unrecognized_movement: true if the movement doesn't have a personalized title
  '
  ' RETURNS:
  '     - A string with the name of the operation type
  Public Function TranslateOperationType(ByVal operation_type As Integer) As String
    Dim _str_type As String

    _str_type = ""

    Select Case operation_type

      Case WSI.Common.OperationCode.NOT_SET
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(670)

        'Case WSI.Common.OperationCode.CHIPS_SALE
        '  _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6600)

        'Case WSI.Common.OperationCode.CHIPS_SALE_WITH_RECHARGE
        '  _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6607)

        'Case WSI.Common.OperationCode.CHIPS_PURCHASE
        '  _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6610)

        'Case WSI.Common.OperationCode.CHIPS_PURCHASE_WITH_CASH_OUT
        '  _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6611)

      Case WSI.Common.OperationCode.CASH_IN
        If TITO.Utils.IsTitoMode() Then
          _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2687)
        Else
          _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(607)
        End If

      Case WSI.Common.OperationCode.CASH_OUT
        If TITO.Utils.IsTitoMode() Then
          _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4607)
        Else
          _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5865)
        End If

      Case WSI.Common.OperationCode.PROMOTION
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(609)

      Case WSI.Common.OperationCode.MB_CASH_IN
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(610)

      Case WSI.Common.OperationCode.MB_PROMOTION
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(611)

      Case WSI.Common.OperationCode.HANDPAY
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1834)

      Case WSI.Common.OperationCode.GIFT_REQUEST
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(612)

      Case WSI.Common.OperationCode.GIFT_DELIVERY
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(613)

      Case WSI.Common.OperationCode.CANCEL_GIFT_INSTANCE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1788)

      Case WSI.Common.OperationCode.DRAW_TICKET
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(614)

      Case WSI.Common.OperationCode.GIFT_DRAW_TICKET
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(625)

      Case WSI.Common.OperationCode.GIFT_REDEEMABLE_AS_CASHIN
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2510)

      Case WSI.Common.OperationCode.ACCOUNT_PIN_RANDOM
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(621)

      Case WSI.Common.OperationCode.NA_CASH_IN
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(623)

      Case WSI.Common.OperationCode.NA_PROMOTION
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(624)

      Case WSI.Common.OperationCode.PROMOBOX_TOTAL_RECHARGES_PRINT
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1433)

      Case WSI.Common.OperationCode.CHIPS_PURCHASE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4310)

      Case WSI.Common.OperationCode.CHIPS_PURCHASE_WITH_CASH_OUT
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4313)

      Case WSI.Common.OperationCode.CHIPS_SALE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4311)

      Case WSI.Common.OperationCode.CHIPS_SALE_WITH_RECHARGE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4312)

      Case WSI.Common.OperationCode.TRANSFER_CREDIT_OUT
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4604)

      Case WSI.Common.OperationCode.CASH_ADVANCE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5461)

      Case WSI.Common.OperationCode.CASH_WITHDRAWAL
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5855)

      Case WSI.Common.OperationCode.CASH_DEPOSIT
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5859)

      Case WSI.Common.OperationCode.TITO_OFFLINE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3366)

      Case WSI.Common.OperationCode.TITO_TICKET_VALIDATION
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4716)

      Case WSI.Common.OperationCode.MB_DEPOSIT
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5862)

      Case WSI.Common.OperationCode.HANDPAY_CANCELLATION
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1835)

      Case WSI.Common.OperationCode.HANDPAY_VALIDATION
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6687)

        ' LA 12-04-2016
      Case WSI.Common.OperationCode.PROMOTION_WITH_TAXES
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7272)

      Case WSI.Common.OperationCode.PRIZE_PAYOUT
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7273)

      Case WSI.Common.OperationCode.PRIZE_PAYOUT_AND_RECHARGE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7274)


      Case Else
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(670)

    End Select
    Return _str_type
  End Function 'TranslateOperationType


  ' PURPOSE: Get Sql WHERE to buil SQL Query
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None  Private Function GetSqlWhere() As String
  Private Function GetSqlWhere() As String

    Dim _str_where As String
    Dim _date_from As Date
    Dim _date_to As Date
    Dim _rgx As Regex
    Dim _sites As String
    Dim _operations As String

    _str_where = String.Empty
    _operations = String.Empty

    ' Filter Siteid
    _sites = uc_sites_sel.GetSitesIdListSelected()
    If _sites <> String.Empty Then
      _str_where = _str_where & " AND TWD_SITE_ID IN (" & _sites & ") "
    End If

    ' Filter Dates
    _date_from = Me.uc_daily_session_selector.FromDate.AddHours(Me.uc_daily_session_selector.ClosingTime)
    _date_to = Me.uc_daily_session_selector.ToDate.AddHours(Me.uc_daily_session_selector.ClosingTime)
    If Me.uc_daily_session_selector.FromDateSelected = True Then
      _str_where = _str_where & " AND TWD_DATETIME >= " & GUI_FormatDateDB(_date_from) & " "
    End If

    If Me.uc_daily_session_selector.ToDateSelected = True Then
      _str_where = _str_where & " AND TWD_DATETIME < " & GUI_FormatDateDB(_date_to) & " "
    End If

    If uc_checked_list_operations.SelectedIndexesListLevel2.Length > 0 Then
      _str_where = _str_where & " AND TWD_OPERATION_TYPE in (" & uc_checked_list_operations.SelectedIndexesListLevel2 & ") "
    End If

    ' Filter Payment mode
    If uc_checked_list_payment_type.SelectedIndexes.Length > 0 Then
      _str_where = _str_where & " AND TWD_PAYMENT_TYPE_CODE IN (" & uc_checked_list_payment_type.SelectedIndexesList & ") "
    End If

    ' "AND" first occurrence is replaced by "WHERE"
    If _str_where <> String.Empty Then
      _rgx = New Regex("^\s*AND\b", RegexOptions.IgnoreCase)
      _str_where = _rgx.Replace(_str_where, " WHERE")
    End If

    Return _str_where
  End Function ' GetSqlWhere

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()

    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)

      ' Index
      .Column(GRID_COLUMN_INDEX).Header(0).Text = " "
      .Column(GRID_COLUMN_INDEX).Header(1).Text = " "
      .Column(GRID_COLUMN_INDEX).Width = GRID_WIDTH_INDEX
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' SITE ID
      .Column(GRID_COLUMN_SITE_ID).Header(0).Text = ""
      .Column(GRID_COLUMN_SITE_ID).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6739)
      .Column(GRID_COLUMN_SITE_ID).Width = GRID_WIDTH_SITE
      .Column(GRID_COLUMN_SITE_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' OPERATION_ID
      .Column(GRID_COLUMN_OPERATION_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6621)
      .Column(GRID_COLUMN_OPERATION_ID).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6740)
      .Column(GRID_COLUMN_OPERATION_ID).Width = GRID_WIDTH_OPERATION_ID
      .Column(GRID_COLUMN_OPERATION_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' DATETIME
      .Column(GRID_COLUMN_DATETIME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6621)
      .Column(GRID_COLUMN_DATETIME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6744)
      .Column(GRID_COLUMN_DATETIME).Width = GRID_WIDTH_DATE
      .Column(GRID_COLUMN_DATETIME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' OPERATION_TYPE 
      .Column(GRID_COLUMN_OPERATION_TYPE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6621)
      .Column(GRID_COLUMN_OPERATION_TYPE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6745)
      .Column(GRID_COLUMN_OPERATION_TYPE).Width = GRID_WIDTH_OPERATION
      .Column(GRID_COLUMN_OPERATION_TYPE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' ACCOUNT_NAME
      .Column(GRID_COLUMN_ACCOUNT_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6763)
      .Column(GRID_COLUMN_ACCOUNT_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6741)
      .Column(GRID_COLUMN_ACCOUNT_NAME).Width = GRID_WIDTH_ACCOUNT_NAME
      .Column(GRID_COLUMN_ACCOUNT_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' ACCOUNT_ID 
      .Column(GRID_COLUMN_ACCOUNT_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6763)
      .Column(GRID_COLUMN_ACCOUNT_ID).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6742)
      .Column(GRID_COLUMN_ACCOUNT_ID).Width = GRID_WIDTH_ACCOUNT
      .Column(GRID_COLUMN_ACCOUNT_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' EXTERNAL_CARD_NUMBER
      .Column(GRID_COLUMN_EXTERNAL_CARD_NUMBER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6763)
      .Column(GRID_COLUMN_EXTERNAL_CARD_NUMBER).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6743)
      .Column(GRID_COLUMN_EXTERNAL_CARD_NUMBER).Width = GRID_WIDTH_CARD_NUMBER
      .Column(GRID_COLUMN_EXTERNAL_CARD_NUMBER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      'TOTAL_CASH_IN 
      .Column(GRID_COLUMN_TOTAL_CASH_IN).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6764)
      .Column(GRID_COLUMN_TOTAL_CASH_IN).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6746)
      .Column(GRID_COLUMN_TOTAL_CASH_IN).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_TOTAL_CASH_IN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' CASH_IN_SPLIT1 
      .Column(GRID_COLUMN_CASH_IN_SPLIT1).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6764)
      .Column(GRID_COLUMN_CASH_IN_SPLIT1).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6747)
      .Column(GRID_COLUMN_CASH_IN_SPLIT1).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_CASH_IN_SPLIT1).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' CASH_IN_SPLIT2 
      .Column(GRID_COLUMN_CASH_IN_SPLIT2).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6764)
      .Column(GRID_COLUMN_CASH_IN_SPLIT2).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6748)
      .Column(GRID_COLUMN_CASH_IN_SPLIT2).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_CASH_IN_SPLIT2).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' CASH_IN_TAX1 
      .Column(GRID_COLUMN_CASH_IN_TAX1).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6764)
      .Column(GRID_COLUMN_CASH_IN_TAX1).Header(1).Text = "" ' GLB_NLS_GUI_PLAYER_TRACKING.GetString(6749)
      .Column(GRID_COLUMN_CASH_IN_TAX1).Width = 0 ' GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_CASH_IN_TAX1).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' CASH_IN_TAX2 
      .Column(GRID_COLUMN_CASH_IN_TAX2).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6764)
      .Column(GRID_COLUMN_CASH_IN_TAX2).Header(1).Text = "" ' GLB_NLS_GUI_PLAYER_TRACKING.GetString(6750)
      .Column(GRID_COLUMN_CASH_IN_TAX2).Width = 0 ' GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_CASH_IN_TAX2).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' PAYMENT_TYPE_CODE  
      .Column(GRID_COLUMN_PAYMENT_TYPE_CODE).Header(0).Text = ""
      .Column(GRID_COLUMN_PAYMENT_TYPE_CODE).Header(1).Text = "" ' GLB_NLS_GUI_PLAYER_TRACKING.GetString(6751)
      .Column(GRID_COLUMN_PAYMENT_TYPE_CODE).Width = 0 ' GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_PAYMENT_TYPE_CODE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' TOTAL_CASH_OUT 
      .Column(GRID_COLUMN_TOTAL_CASH_OUT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6765)
      .Column(GRID_COLUMN_TOTAL_CASH_OUT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6752)
      .Column(GRID_COLUMN_TOTAL_CASH_OUT).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_TOTAL_CASH_OUT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' DEVOLUTION 
      .Column(GRID_COLUMN_DEVOLUTION).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6765)
      .Column(GRID_COLUMN_DEVOLUTION).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6753)
      .Column(GRID_COLUMN_DEVOLUTION).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_DEVOLUTION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' PRIZE  
      .Column(GRID_COLUMN_PRIZE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6765)
      .Column(GRID_COLUMN_PRIZE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6754)
      .Column(GRID_COLUMN_PRIZE).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_PRIZE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' ISR1  
      .Column(GRID_COLUMN_ISR1).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6765)
      .Column(GRID_COLUMN_ISR1).Header(1).Text = WSI.Common.Misc.ReadGeneralParams("Cashier", "Tax.OnPrize.1.Name")
      .Column(GRID_COLUMN_ISR1).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_ISR1).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' ISR2  
      .Column(GRID_COLUMN_ISR2).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6765)
      .Column(GRID_COLUMN_ISR2).Header(1).Text = WSI.Common.Misc.ReadGeneralParams("Cashier", "Tax.OnPrize.2.Name")
      .Column(GRID_COLUMN_ISR2).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_ISR2).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' SERVICE_CHARGE  
      .Column(GRID_COLUMN_SERVICE_CHARGE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6765)
      .Column(GRID_COLUMN_SERVICE_CHARGE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6757) ' WSI.Common.Misc.ReadGeneralParams("Cashier.ServiceCharge", "Text")
      .Column(GRID_COLUMN_SERVICE_CHARGE).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_SERVICE_CHARGE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' DECIMAL_ROUNDING  
      .Column(GRID_COLUMN_DECIMAL_ROUNDING).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6765)
      .Column(GRID_COLUMN_DECIMAL_ROUNDING).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6758)
      .Column(GRID_COLUMN_DECIMAL_ROUNDING).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_DECIMAL_ROUNDING).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' RETENTION_ROUNDING  
      .Column(GRID_COLUMN_RETENTION_ROUNDING).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6765)
      .Column(GRID_COLUMN_RETENTION_ROUNDING).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6759)
      .Column(GRID_COLUMN_RETENTION_ROUNDING).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_RETENTION_ROUNDING).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' EVIDENCE  
      .Column(GRID_COLUMN_EVIDENCE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6765)
      .Column(GRID_COLUMN_EVIDENCE).Header(1).Text = "" ' GLB_NLS_GUI_PLAYER_TRACKING.GetString(6760)
      .Column(GRID_COLUMN_EVIDENCE).Width = 0 ' GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_EVIDENCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' CASH_OUT_MONEY 
      .Column(GRID_COLUMN_CASH_OUT_MONEY).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6765)
      .Column(GRID_COLUMN_CASH_OUT_MONEY).Header(1).Text = "" ' GLB_NLS_GUI_PLAYER_TRACKING.GetString(6761)
      .Column(GRID_COLUMN_CASH_OUT_MONEY).Width = 0 ' GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_CASH_OUT_MONEY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' CASH_OUT_CHECK  
      .Column(GRID_COLUMN_CASH_OUT_CHECK).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6765)
      .Column(GRID_COLUMN_CASH_OUT_CHECK).Header(1).Text = "" ' GLB_NLS_GUI_PLAYER_TRACKING.GetString(6762)
      .Column(GRID_COLUMN_CASH_OUT_CHECK).Width = 0 ' GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_CASH_OUT_CHECK).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT


    End With

  End Sub ' GUI_StyleSheet

  Private Function TranslatePayMode(ByVal PayMode As Integer) As String
    Dim _str As String
    _str = ""

    Select Case PayMode
      Case 0
        _str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6775)
      Case 1
        _str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6776)
      Case 2
        _str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6777)
      Case 3
        _str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6778)
      Case 4
        _str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6779)
    End Select

    Return _str

  End Function    ' TranslatePayMode

#End Region  ' Private Functions


End Class