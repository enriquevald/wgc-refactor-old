'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_sites_monitor.vb
' DESCRIPTION:   Sites monitor
' AUTHOR:        Javier Molina Moral
' CREATION DATE: 05-MAR-2013
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 05-MAR-2013  JMM    Initial version
' 17-APR-2013  JMM    Force ReSync modified to allow multiple tasks at once
' 18-APR-2013  JMM    Form redesign.  Mark tasks that are not reporting.
' 25-MAY-2013  ANG    Some code refactoring to improve perfomance
'                     - Initialize grid events after fill data
'                     - Share the same transaction
' 06-JUN-2013  ANG    Fix bug #822
' 11-MAR-2014  DLL    Change timer to thread and refactor read methods
' 31-MAR-2014  DLL    Fixed Bug WIG-779: incorrect site refresh
' 20-JAN-2015  FJC    Set features form (size and autoscroll panels) when size's form exceeds desktop area
' 29-MAR-2016  LTC    Bug 10375:Mulstisite - Site's State
' -------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports WSI.Common
Imports GUI_CommonOperations.CLASS_BASE
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient

Public Class frm_sites_monitor
  Inherits GUI_Controls.frm_base

#Region " Constants "

  ' GRID SITES DEFINITIONS
  Private Const GRID_NUM_COL_SITES As Integer = 6
  Private Const GRID_COLUMN_SITES_SENTINEL As Integer = 0
  Private Const GRID_COLUMN_SITES_SITE_ID As Integer = 1
  Private Const GRID_COLUMN_SITES_SITE_NAME As Integer = 2
  Private Const GRID_COLUMN_SITES_STATUS_ID As Integer = 3
  Private Const GRID_COLUMN_SITES_STATUS As Integer = 4
  Private Const GRID_COLUMN_SITES_TIME_OFFLINE As Integer = 5
  Private Const GRID_COLUMN_SITES_STATUS_WIDTH As Integer = 3285

  ' GRID SITE TASKS DEFINITIONS
  Private Const GRID_NUM_COL_TASKS As Integer = 9
  Private Const GRID_COLUMN_TASKS_SENTINEL As Integer = 0
  Private Const GRID_COLUMN_TASKS_SITE_ID As Integer = 1
  Private Const GRID_COLUMN_TASKS_SITE_NAME As Integer = 2
  Private Const GRID_COLUMN_TASKS_SITE_STATUS As Integer = 3
  Private Const GRID_COLUMN_TASKS_SITE_STATUS_ID As Integer = 4
  Private Const GRID_COLUMN_TASKS_ID As Integer = 5
  Private Const GRID_COLUMN_TASKS_NAME As Integer = 6
  Private Const GRID_COLUMN_PENDING_CHANGES As Integer = 7
  Private Const GRID_COLUMN_TASKS_LAST_SYNC As Integer = 8
  Private Const GRID_COLUMN_TASKS_SITE_STATUS_WIDTH As Integer = 3030

  ' QUERY WWP STATUS
  Private Const SQL_COLUMN_RUNNING As Integer = 0
  Private Const SQL_COLUMN_LAST_ACCESS As Integer = 1

  ' QUERY NUM SITES CONNECTED
  Private Const SQL_COLUMN_WWS_STATUS As Integer = 0
  Private Const SQL_COLUMN_WWS_LAST_RCVD_MSG As Integer = 1

  ' QUERY SITE TASKS
  Private Const SQL_COLUMN_TASKS_SITE_ID As Integer = 0
  Private Const SQL_COLUMN_TASKS_SITE_NAME As Integer = 1
  Private Const SQL_COLUMN_TASKS_SITE_STATUS As Integer = 2
  Private Const SQL_COLUMN_TASKS_LAST_RECEIVED_MSG As Integer = 3
  Private Const SQL_COLUMN_TASKS_TASK_ID As Integer = 4
  Private Const SQL_COLUMN_TASKS_RUNNING As Integer = 5
  Private Const SQL_COLUMN_TASKS_LAST_SYNC As Integer = 6
  Private Const SQL_COLUMN_TASKS_SYNC_INTERVAL As Integer = 7
  Private Const SQL_COLUMN_TASKS_NUM_PENDING_SYNC As Integer = 8

  Private Const GRID_SCROLLBAR_WIDTH As Int16 = 235
  Private Enum ENUM_SITE_STATUS
    Synchronize = 0
    ConnectedNotSynchronize = 1
    Disconected = 2
  End Enum

  Private Enum ENUM_READ_TYPE
    ALL = 0
    TASKS = 1
    SITETASKS = 2
  End Enum

  Private Enum ENUM_MULTISITE_TASK_TYPE
    Upload = 0
    Download = 1
    Other = 99
  End Enum

  Private Const UNDEFINED_VALUE_STR = "---"

  ' Timer interval to refresh multisite status (in milliseconds)
  Private Const REFRESH_INTERVAL = 5000

  ' Grid counters
  Private Const COUNTER_SYNCHRONIZE As Integer = 1
  Private Const COUNTER_DISCONNECTED As Integer = 2
  Private Const COUNTER_CONNECTED_NOT_SYNCHRONIZE As Integer = 3

  ' Maximum amount of seconds elapsed since last refresh to consider that a service is alive
  Private Const MAX_SERVICE_TIMEOUT = 10

  ' No reporting constants
  Private Const NO_REPORTING_MIN_ELAPSED_SEC = 60
  Private Const NO_REPORTING_MARGIN = 2

  ' Task dictionary propierties
  'Private Const DIC_PROPERTY_TASKS_TASK_ID As Byte = 0
  'Private Const DIC_PROPERTY_TASKS_TASK_NAME As Byte = 1
  'Private Const DIC_PROPERTY_TASKS_ENABLED As Byte = 2
  Private Const DIC_PROPERTY_TASKS_INTERVAL_SECONDS As Byte = 3
  'Private Const DIC_PROPERTY_TASKS_START_HOUR As Byte = 4
  'Private Const DIC_PROPERTY_TASKS_MAX_ROWS2UPLOAD As Byte = 5

  Private Const SECONDS_TO_MILLISECONDS As Integer = 1000

  Private Const GRID_SITE_TASKS As Integer = 0
  Private Const GRID_TASKS As Integer = 1

#End Region ' Constants

#Region " Delegates "

  Private Delegate Sub DelegatesRefreshValues(ByVal values As DataSet, ByVal TypeRead As ENUM_READ_TYPE)

#End Region

#Region " Structures "

  Public Structure TYPE_RESYNCABLE_TASKS
    Dim TaskId As TYPE_MULTISITE_SITE_TASKS
    Dim TaskName As String
    Dim SiteId As Int32
    Dim SiteName As String
  End Structure


#End Region

#Region " Members "

  Private m_selected_row As Integer
  Private m_selected_process As Integer

  Private m_window_color As Color = System.Drawing.SystemColors.Window
  Private m_window_text_color As Color = System.Drawing.SystemColors.WindowText
  Private m_white_color As Color = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
  Private m_red_color As Color = GetColor(ENUM_GUI_COLOR.GUI_COLOR_RED_02)
  Private m_green_color As Color = Color.FromArgb(0, 192, 0)
  Private m_yellow_color As Color = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
  Private m_black_color As Color = GetColor(ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
  Private m_dummy_color As Color = Color.FromArgb(130, 242, 194)

  Dim m_task_field_properties As Dictionary(Of TYPE_MULTISITE_SITE_TASKS, mdl_multisite.TYPE_MULTISITE_TASK_FIELD_PROPERTIES())

  Dim m_thread As Thread
  Private m_selected_tab_index As Integer
  Private m_start_refresh As New System.Threading.AutoResetEvent(False)
  Private m_read_type As ENUM_READ_TYPE

#End Region ' Members

#Region " Overrides"

  ' PURPOSE: Sets the proper form identifier
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_SITES_MONITOR

    Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId


  Public Overrides Sub GUI_Closing(ByRef CloseCanceled As Boolean)

    m_thread.Abort()

    CloseCanceled = False
  End Sub

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' - Form title
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1760) '1760 "Sites Status"

    Call GUI_StyleSitesGrid()

    StyleTasksGrid(Me.dg_site_tasks, True) '' SiteTaskGrid

    StyleTasksGrid(Me.dg_tasks, True) '' TaskGrid

    ' Set field properties ( allow edit, min and max values... )
    Me.m_task_field_properties = mdl_multisite.GetTaskFieldProperties()
    ' - Buttons
    btn_exit.Text = GLB_NLS_GUI_CONTROLS.GetString(10)
    btn_refresh.Text = GLB_NLS_GUI_CONTROLS.GetString(8)
    btn_force_sync.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1759) '1759 "Force Update"
    btn_force_sync.Enabled = Permissions.Execute

    ' WWW_Status Text Field
    Me.lbl_wwp_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1789) '1789 "Service"
    Me.lbl_stopped.ForeColor = m_yellow_color
    Me.lbl_stopped.BackColor = m_red_color
    Me.lbl_connected_sites.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1801) '1801 "Connected Sites"
    Me.lbl_disconnected_sites.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1802) '1802 "Disconnected Sites"
    Me.lbl_num_disconnected_sites.ForeColor = m_yellow_color
    Me.lbl_num_disconnected_sites.BackColor = m_red_color

    ' Sites tab
    Me.tab_sites.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1771) '1771 "Sites"

    ' Site filter control
    Me.lbl_site_id.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1407) '1407 "Site ID"
    Me.ef_site.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 3)

    ' Sites Tab Status filter
    Me.gb_sites_tab_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1752) '1752 "Status"  
    Me.opt_sites_tab_all_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(287) '287 "All"
    Me.opt_sites_tab_all_status.Checked = True
    Me.lbl_sites_tab_color_all.BackColor = Color.Black
    Me.opt_sites_tab_synchronize.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2003) '' Connected ( Updated ) 
    Me.lbl_sites_tab_color_synchronize.BackColor = Color.White
    Me.opt_sites_tab_connected_not_synchronize.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2005) '' Connected (Not Updated ) 
    Me.lbl_sites_tab_color_conected.BackColor = m_yellow_color
    Me.opt_sites_tab_disconnected.Text = GLB_NLS_GUI_CONTROLS.GetString(329) '329 "Disconnected"
    Me.lbl_sites_tab_color_disconected.BackColor = m_red_color

    'Grid Labels
    Me.lbl_sites.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1771) '1771 "Sites"
    Me.lbl_tasks.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1772) '1772 "Tasks"

    ' Tasks Tab
    Me.tab_processes.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1772) '1772 "Tasks"

    ' Processes combo
    Me.lbl_tasks_taks.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1772) '1772 "Tasks"


    ' Tasks Tab Status filter
    Me.gb_tasks_tab_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1752) '1752 "Status"  
    Me.opt_tasks_tab_all_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(287) '287 "All"
    Me.opt_tasks_tab_all_status.Checked = True
    Me.lbl_tasks_tab_color_all.BackColor = Color.Black
    Me.opt_tasks_tab_synchronize.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2001) '' Updated
    Me.lbl_tasks_tab_color_synchronize.BackColor = Color.White
    Me.opt_tasks_tab_connected_not_synchronize.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2002) ' Not updated
    Me.lbl_tasks_tab_color_conected.BackColor = m_yellow_color
    Me.opt_tasks_tab_disconnected.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2002, "(" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2004) & ")") ' Not Updated ( Disconected )
    Me.lbl_tasks_tab_color_disconected.BackColor = m_red_color

    If Me.dg_sites.NumRows > 0 Then
      Me.dg_sites.SelectFirstRow(False)
    End If

    m_selected_tab_index = 0

    m_thread = New Thread(AddressOf DataRefresh)
    m_thread.Start()

    '' Add handler to event after fill data ;)
    AddHandler dg_sites.RowSelectedEvent, AddressOf dg_sites_RowSelectedEvent



  End Sub ' GUI_InitControls

#End Region ' Overrides

#Region " Public Functions "

  ' PURPOSE: Init form in edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - mdiparent
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Overloads Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    ' Sets the screen mode
    Me.MdiParent = MdiParent

    m_selected_row = -1
    m_selected_process = -1

    Call Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE:  Set the columns for sites grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSitesGrid()

    With Me.dg_sites
      Call .Init(GRID_NUM_COL_SITES, 1, False)

      .PanelRightVisible = True

      .Counter(COUNTER_SYNCHRONIZE).Visible = True
      .Counter(COUNTER_SYNCHRONIZE).BackColor = m_window_color
      .Counter(COUNTER_SYNCHRONIZE).ForeColor = m_window_text_color

      .Counter(COUNTER_DISCONNECTED).Visible = True
      .Counter(COUNTER_DISCONNECTED).BackColor = m_red_color
      .Counter(COUNTER_DISCONNECTED).ForeColor = m_yellow_color

      .Counter(COUNTER_CONNECTED_NOT_SYNCHRONIZE).Visible = True
      .Counter(COUNTER_CONNECTED_NOT_SYNCHRONIZE).BackColor = m_yellow_color
      .Counter(COUNTER_CONNECTED_NOT_SYNCHRONIZE).ForeColor = m_black_color

      ' Sentinel
      .Column(GRID_COLUMN_SITES_SENTINEL).Header.Text = ""
      .Column(GRID_COLUMN_SITES_SENTINEL).Width = 200
      .Column(GRID_COLUMN_SITES_SENTINEL).HighLightWhenSelected = False

      ' Site ID
      .Column(GRID_COLUMN_SITES_SITE_ID).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1407) '1407 "Site ID"
      .Column(GRID_COLUMN_SITES_SITE_ID).Width = 750
      .Column(GRID_COLUMN_SITES_SITE_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Site Name
      .Column(GRID_COLUMN_SITES_SITE_NAME).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1410) '1410 "Site name"
      .Column(GRID_COLUMN_SITES_SITE_NAME).Width = 5130
      .Column(GRID_COLUMN_SITES_SITE_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Status ID
      .Column(GRID_COLUMN_SITES_STATUS_ID).Header.Text = ""
      .Column(GRID_COLUMN_SITES_STATUS_ID).Width = 0
      .Column(GRID_COLUMN_SITES_STATUS_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Status
      .Column(GRID_COLUMN_SITES_STATUS).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1752) '1752 "Status"  
      .Column(GRID_COLUMN_SITES_STATUS).Width = GRID_COLUMN_SITES_STATUS_WIDTH
      .Column(GRID_COLUMN_SITES_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_SITES_STATUS).HighLightWhenSelected = False

      ' Time Offline
      .Column(GRID_COLUMN_SITES_TIME_OFFLINE).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1792) '1792 "Time Offline"
      .Column(GRID_COLUMN_SITES_TIME_OFFLINE).Width = 2200
      .Column(GRID_COLUMN_SITES_TIME_OFFLINE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      .SelectFirstRow(False)
    End With
  End Sub 'GUI_StyleDownloadGrid

  ' PURPOSE:  Set the columns for any processes grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - TaskGrid , uc_grid to set columns.
  '           - Keep Space For Scrollbar. True of False
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub StyleTasksGrid(ByVal TasksGrid As uc_grid, ByVal KeepSpaceForScrollBar As Boolean)

    With TasksGrid
      Call .Init(GRID_NUM_COL_TASKS, 1)

      .PanelRightVisible = True

      .Counter(COUNTER_SYNCHRONIZE).Visible = True
      .Counter(COUNTER_SYNCHRONIZE).BackColor = m_window_color
      .Counter(COUNTER_SYNCHRONIZE).ForeColor = m_window_text_color

      .Counter(COUNTER_DISCONNECTED).Visible = True
      .Counter(COUNTER_DISCONNECTED).BackColor = m_red_color
      .Counter(COUNTER_DISCONNECTED).ForeColor = m_yellow_color

      .Counter(COUNTER_CONNECTED_NOT_SYNCHRONIZE).Visible = True
      .Counter(COUNTER_CONNECTED_NOT_SYNCHRONIZE).BackColor = m_yellow_color
      .Counter(COUNTER_CONNECTED_NOT_SYNCHRONIZE).ForeColor = m_black_color

      ' Sentinel
      .Column(GRID_COLUMN_TASKS_SENTINEL).Header.Text = ""
      .Column(GRID_COLUMN_TASKS_SENTINEL).Width = 200
      .Column(GRID_COLUMN_TASKS_SENTINEL).HighLightWhenSelected = False

      ' Site ID
      .Column(GRID_COLUMN_TASKS_SITE_ID).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1407) '1407 "Site ID"
      .Column(GRID_COLUMN_TASKS_SITE_ID).Width = 550
      .Column(GRID_COLUMN_TASKS_SITE_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Site Name
      .Column(GRID_COLUMN_TASKS_SITE_NAME).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1410) '1410 "Site name"

      .Column(GRID_COLUMN_TASKS_SITE_NAME).Width = 2210

      If KeepSpaceForScrollBar Then
        .Column(GRID_COLUMN_TASKS_SITE_NAME).Width = .Column(GRID_COLUMN_TASKS_SITE_NAME).Width - GRID_SCROLLBAR_WIDTH
      End If

      .Column(GRID_COLUMN_TASKS_SITE_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Site Status ID
      .Column(GRID_COLUMN_TASKS_SITE_STATUS_ID).Header.Text = ""
      .Column(GRID_COLUMN_TASKS_SITE_STATUS_ID).Width = 0
      .Column(GRID_COLUMN_TASKS_SITE_STATUS_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Site Status
      .Column(GRID_COLUMN_TASKS_SITE_STATUS).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1747) '1747 "Status"
      .Column(GRID_COLUMN_TASKS_SITE_STATUS).Width = GRID_COLUMN_TASKS_SITE_STATUS_WIDTH
      .Column(GRID_COLUMN_TASKS_SITE_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_TASKS_SITE_STATUS).HighLightWhenSelected = False

      ' Task ID
      .Column(GRID_COLUMN_TASKS_ID).Header.Text = ""
      .Column(GRID_COLUMN_TASKS_ID).Width = 0
      .Column(GRID_COLUMN_TASKS_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Task Name
      .Column(GRID_COLUMN_TASKS_NAME).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1751) '1751 "Task"
      .Column(GRID_COLUMN_TASKS_NAME).Width = 5340
      .Column(GRID_COLUMN_TASKS_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Pending Changes 
      .Column(GRID_COLUMN_PENDING_CHANGES).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1753) ' Pending Changes
      .Column(GRID_COLUMN_PENDING_CHANGES).Width = 2025
      .Column(GRID_COLUMN_PENDING_CHANGES).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Last Message
      .Column(GRID_COLUMN_TASKS_LAST_SYNC).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1754) '1754 "Last Message"
      .Column(GRID_COLUMN_TASKS_LAST_SYNC).Width = 2000
      .Column(GRID_COLUMN_TASKS_LAST_SYNC).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
    End With
  End Sub 'StyleProcessesGrid

  ' PURPOSE: Checks if any task from the site is currently no reporting
  '
  '  PARAMS:
  '     - INPUT:
  '           - SiteID
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '   
  Private Function AnyTaskNoReporting(ByVal SiteID As Int32) As Boolean
    Dim _str_bld As StringBuilder
    Dim _sql_transaction As SqlTransaction
    Dim _seconds_since_last_sync As Int64
    Dim _task_id As TYPE_MULTISITE_SITE_TASKS
    Dim _properties As mdl_multisite.TYPE_MULTISITE_TASK_FIELD_PROPERTIES()
    Dim _max_value As UInt32
    Dim _last_run As Date
    Dim _interval_seconds As Int32

    _properties = Nothing

    _sql_transaction = Nothing

    Try
      If GUI_BeginSQLTransaction(_sql_transaction) Then
        _str_bld = New StringBuilder()

        _str_bld.AppendLine("   SELECT   ST_TASK_ID            ")
        _str_bld.AppendLine("          , ST_LAST_RUN           ")
        _str_bld.AppendLine("          , ST_INTERVAL_SECONDS   ")
        _str_bld.AppendLine("     FROM   MS_SITE_TASKS         ")
        _str_bld.AppendLine("    WHERE   ST_SITE_ID = @pSiteID ")
        _str_bld.AppendLine("      AND   ST_ENABLED = 1        ")
        _str_bld.AppendLine(" ORDER BY   ST_TASK_ID            ")

        Using _sql_cmd As SqlCommand = New SqlCommand(_str_bld.ToString(), _sql_transaction.Connection, _sql_transaction)
          _sql_cmd.Parameters.Add("@pSiteID", SqlDbType.Int).Value = SiteID
          Using _sql_reader As SqlDataReader = _sql_cmd.ExecuteReader()

            While _sql_reader.Read()
              _task_id = _sql_reader.GetInt32(0)
              _properties = m_task_field_properties.Item(_task_id)
              _interval_seconds = IIf(_sql_reader.IsDBNull(2), 0, _sql_reader.GetInt32(2))
              If Not _sql_reader.IsDBNull(1) Then
                _last_run = _sql_reader.GetDateTime(1)
              Else
                _last_run = Date.MinValue
              End If
              If _properties(DIC_PROPERTY_TASKS_INTERVAL_SECONDS).Editable Then
                _max_value = _properties(DIC_PROPERTY_TASKS_INTERVAL_SECONDS).MaxValue
              Else
                If Not _sql_reader.IsDBNull(2) Then
                  _max_value = _sql_reader.GetInt32(2)
                Else
                  _max_value = NO_REPORTING_MIN_ELAPSED_SEC
                End If

              End If

              _seconds_since_last_sync = GUI_GetDBDateTime().Subtract(_last_run).TotalSeconds

              If _seconds_since_last_sync > (Math.Max(_max_value, _interval_seconds) * NO_REPORTING_MARGIN) Then
                Return False
              End If

            End While

          End Using
        End Using

      End If

    Catch ex As Exception
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                      "frm_sites_monitor", _
                      "AnyTaskNoReporting", _
                      ex.Message)
    End Try

    Return True

  End Function 'AnyTaskNoReporting

  ' PURPOSE: Update or insert data in sites grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - DataRow
  '           - RowIndex
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '   
  Private Sub UpdateSiteRow(ByVal Row As DataRow, ByVal RowIndex As Integer, ByVal WWPActive As Boolean)
    Dim _site_status_str As String
    Dim _site_status As ENUM_SITE_STATUS
    Dim _time_offline As String
    Dim _date_diff As TimeSpan
    Dim _back_color As Color
    Dim _fore_color As Color
    Dim _days_offline As Int32
    Dim _site_id As Int32
    Dim _sentinel_value As Int32

    _time_offline = UNDEFINED_VALUE_STR
    _site_status = ENUM_SITE_STATUS.Disconected

    ' Site ID
    _site_id = Row.Item("st_site_id")
    Me.dg_sites.Cell(RowIndex, GRID_COLUMN_SITES_SITE_ID).Value = _site_id

    ' Site Name
    Me.dg_sites.Cell(RowIndex, GRID_COLUMN_SITES_SITE_NAME).Value = Row.Item("st_name")

    If Not Row.IsNull("wws_status") _
      And Not Row.IsNull("wws_last_rcvd_msg") Then
      _date_diff = GUI_GetDBDateTime().Subtract(Row.Item("wws_last_rcvd_msg"))

      ' Status
      If WWPActive And Row.Item("wws_status") = WWP_SessionStatus.Opened Then
        'Check if last message is old enough to consider connection lost
        If _date_diff.TotalMilliseconds < CommonMultiSite.WwpConnectionTimeout Then
          _site_status = ENUM_SITE_STATUS.Synchronize
        End If
      Else
        ' Time Offline
        If _date_diff.TotalDays >= 1 Then
          _days_offline = Math.Truncate(_date_diff.TotalDays)
          _time_offline = _days_offline.ToString & "d " & String.Format("{0:HH:mm:ss}", New DateTime(_date_diff.Ticks))
        Else
          _time_offline = String.Format("{0:HH:mm:ss}", New DateTime(_date_diff.Ticks))
        End If
      End If
    End If

    _site_status_str = GLB_NLS_GUI_CONTROLS.GetString(329) '329 "Disconnected"
    _back_color = m_red_color
    _fore_color = m_yellow_color

    If WWPActive And _site_status = ENUM_SITE_STATUS.Synchronize Then
      _site_status_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2003) '' Connectada (Actualizada)

      If Not AnyTaskNoReporting(_site_id) Then
        _back_color = m_yellow_color
        _fore_color = m_black_color
        _site_status_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2005) ' Conectada ( No Actualizada )
        _site_status = ENUM_SITE_STATUS.ConnectedNotSynchronize

        _sentinel_value = COUNTER_CONNECTED_NOT_SYNCHRONIZE
      Else
        _back_color = m_window_color
        _fore_color = m_window_text_color

        _sentinel_value = COUNTER_SYNCHRONIZE
      End If
    Else
      _sentinel_value = COUNTER_DISCONNECTED
    End If

    Me.dg_sites.Cell(RowIndex, GRID_COLUMN_SITES_STATUS_ID).Value = _site_status
    Me.dg_sites.Cell(RowIndex, GRID_COLUMN_SITES_TIME_OFFLINE).Value = _time_offline

    ' Paint all uncolored cells cause row is defined with a wrong default fore color
    If Not Me.dg_sites.IsSelected(RowIndex) Then
      Me.dg_sites.Cell(RowIndex, GRID_COLUMN_SITES_SITE_ID).ForeColor = Me.m_window_text_color
      Me.dg_sites.Cell(RowIndex, GRID_COLUMN_SITES_SITE_NAME).ForeColor = Me.m_window_text_color
      Me.dg_sites.Cell(RowIndex, GRID_COLUMN_SITES_STATUS_ID).ForeColor = Me.m_window_text_color
      Me.dg_sites.Cell(RowIndex, GRID_COLUMN_SITES_TIME_OFFLINE).ForeColor = Me.m_window_text_color
    End If

    Me.dg_sites.Cell(RowIndex, GRID_COLUMN_SITES_STATUS).Value = _site_status_str
    Me.dg_sites.Cell(RowIndex, GRID_COLUMN_SITES_STATUS).BackColor = _back_color
    Me.dg_sites.Cell(RowIndex, GRID_COLUMN_SITES_STATUS).ForeColor = _fore_color

    Me.dg_sites.Cell(RowIndex, GRID_COLUMN_SITES_SENTINEL).BackColor = _back_color
    Me.dg_sites.Cell(RowIndex, GRID_COLUMN_SITES_SENTINEL).ForeColor = _back_color

    Me.dg_sites.Counter(_sentinel_value).Value += 1
  End Sub ' UpdateSiteRow

  ' PURPOSE: Set information from sites connection
  '
  '  PARAMS:
  '     - INPUT:
  '           - DataTable
  '           - WWPActive
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '  
  Private Sub SetSitesConnectionStatus(ByVal Values As DataTable, ByVal WWPActive As Boolean)
    Dim _idx_row As Integer
    Dim _data_row As DataRow
    Dim _num_updated As Integer
    Dim _num_outdated As Integer
    Dim _filter_status As ENUM_SITE_STATUS
    Dim _selected_site_id As Int32

    Me.dg_sites.Redraw = False

    If IsNothing(Values) Then
      Exit Sub
    End If

    _num_updated = 0
    _num_outdated = 0

    _idx_row = 0

    Me.dg_sites.Counter(COUNTER_SYNCHRONIZE).Value = 0
    Me.dg_sites.Counter(COUNTER_DISCONNECTED).Value = 0
    Me.dg_sites.Counter(COUNTER_CONNECTED_NOT_SYNCHRONIZE).Value = 0

    _selected_site_id = GetSelectedSiteID()

    ' Remove RowSelectedEvent in Sites grid to avoid unecessary database queries
    RemoveHandler dg_sites.RowSelectedEvent, AddressOf dg_sites_RowSelectedEvent

    ' Add all rows on the DataTable
    For Each _data_row In Values.Rows

      'Don't show sites that haven't init a session on MultiSite
      If _data_row.IsNull("wws_status") Then
        Continue For
      End If

      If _idx_row + 1 > Me.dg_sites.NumRows Then
        Me.dg_sites.AddRow()
      End If

      ' Wrong default fore color to avoid a issue on returning colored rows to a original color
      Me.dg_sites.Row(_idx_row).ForeColor = Me.m_dummy_color

      UpdateSiteRow(_data_row, _idx_row, WWPActive)

      _idx_row += 1
    Next

    ' Delete leftover rows if there are
    While _idx_row < Me.dg_sites.NumRows
      Me.dg_sites.DeleteRow(_idx_row)
    End While

    ' Apply status filter 
    If Not Me.opt_sites_tab_all_status.Checked Then
      If Me.opt_sites_tab_synchronize.Checked Then
        _filter_status = ENUM_SITE_STATUS.Synchronize
      End If

      If Me.opt_sites_tab_connected_not_synchronize.Checked Then
        _filter_status = ENUM_SITE_STATUS.ConnectedNotSynchronize
      End If

      If Me.opt_sites_tab_disconnected.Checked Then
        _filter_status = ENUM_SITE_STATUS.Disconected
      End If

      _idx_row = 0

      While _idx_row < Me.dg_sites.NumRows
        If dg_sites.Cell(_idx_row, GRID_COLUMN_SITES_STATUS_ID).Value <> _filter_status Then

          ' Site status doesn't match filter's one: detete row
          If Me.dg_sites.Cell(_idx_row, GRID_COLUMN_SITES_SITE_ID).Value = _selected_site_id Then
            Me.dg_sites.IsSelected(_idx_row) = False
            m_selected_row = -1
            Me.dg_site_tasks.Clear()
          End If

          Me.dg_sites.Counter(dg_sites.Cell(_idx_row, GRID_COLUMN_SITES_SENTINEL).Value).Value -= 1
          Me.dg_sites.DeleteRowFast(_idx_row)
        Else
          _idx_row += 1
        End If
      End While
    End If

    ' When only one site, load automatically its processes
    If Me.dg_sites.NumRows = 1 Then
      m_selected_row = 0
    Else
      _idx_row = 0

      While _idx_row < Me.dg_sites.NumRows
        ' Restore previously selected site
        If Me.dg_sites.Cell(_idx_row, GRID_COLUMN_SITES_SITE_ID).Value = _selected_site_id Then
          m_selected_row = _idx_row
        End If

        _idx_row += 1
      End While
    End If

    AddHandler dg_sites.RowSelectedEvent, AddressOf dg_sites_RowSelectedEvent

    Call GUI_AdjustGridScrollbars(Me.dg_sites, GRID_COLUMN_SITES_STATUS, 1, GRID_COLUMN_SITES_STATUS_WIDTH)

    Me.dg_sites.Redraw = True
  End Sub 'SetSitesConnectionStatus

  ' PURPOSE: Set information from site tasks
  '
  '  PARAMS:
  '     - INPUT:
  '           - DataTable
  '           - WWPActive
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '  
  Private Sub SetSiteTasksStatus(ByVal Values As DataTable, ByVal WWPActive As Boolean)
    Dim _idx_row As Integer
    Dim _data_row As DataRow
    Dim _num_tasks_connected As Int32
    Dim _num_tasks_disconnected As Int32

    _num_tasks_connected = 0
    _num_tasks_disconnected = 0

    dg_site_tasks.Redraw = False

    _idx_row = 0

    dg_site_tasks.Counter(COUNTER_CONNECTED_NOT_SYNCHRONIZE).Value = 0
    dg_site_tasks.Counter(COUNTER_SYNCHRONIZE).Value = 0
    dg_site_tasks.Counter(COUNTER_DISCONNECTED).Value = 0

    ' New rows to add
    For Each _data_row In Values.Rows
      'For Each _data_row In _sorted_view.ToTable().Rows

      If _idx_row + 1 > Me.dg_site_tasks.NumRows Then
        Me.dg_site_tasks.AddRow()
      End If


      ' Wrong default fore color to avoid a issue on returning colored rows to a original color
      Me.dg_site_tasks.Row(_idx_row).ForeColor = Me.m_dummy_color

      UpdateTaskRow(_data_row, _idx_row, dg_site_tasks, WWPActive)

      _idx_row += 1
    Next

    ' Delete leftover rows if there are
    While _idx_row < Me.dg_site_tasks.NumRows
      Me.dg_site_tasks.DeleteRow(_idx_row)
    End While

    Call GUI_AdjustGridScrollbars(Me.dg_site_tasks, GRID_COLUMN_TASKS_SITE_STATUS, 1, GRID_COLUMN_TASKS_SITE_STATUS_WIDTH)

    dg_site_tasks.Redraw = True
  End Sub 'SetSiteTasksStatus

  ' PURPOSE:  Format the data read from database to the site tasks grid cols
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub UpdateTaskRow(ByVal Row As DataRow, ByVal RowIndex As Integer, ByVal Grid As uc_grid, ByVal WWPActive As Boolean)
    Dim _site_id As Integer
    Dim _site_name As String
    Dim _task_id As Integer
    Dim _task_name As String
    Dim _reporting As Boolean
    Dim _reporting_status As String
    Dim _last_sync As String
    Dim _last_sync_date_time As DateTime
    Dim _sync_interval As String
    Dim _num_pending_sync As String
    Dim _site_status As ENUM_SITE_STATUS
    Dim _site_status_str As String
    Dim _date_diff As TimeSpan
    Dim _back_color As Color
    Dim _fore_color As Color
    Dim _seconds_since_last_sync As Int32
    Dim _sentinel_value As Int32
    Dim _properties As mdl_multisite.TYPE_MULTISITE_TASK_FIELD_PROPERTIES()
    Dim _max_value As Int32

    _reporting_status = UNDEFINED_VALUE_STR
    _last_sync = UNDEFINED_VALUE_STR
    _last_sync_date_time = Nothing
    _sync_interval = UNDEFINED_VALUE_STR
    _num_pending_sync = UNDEFINED_VALUE_STR
    _site_status_str = UNDEFINED_VALUE_STR

    _site_id = Row.Item(SQL_COLUMN_TASKS_SITE_ID)
    _site_name = Row.Item(SQL_COLUMN_TASKS_SITE_NAME)

    _task_id = Row.Item(SQL_COLUMN_TASKS_TASK_ID)

    _task_name = mdl_multisite.TranslateMultiSiteProcessID(_task_id)

    If Not m_task_field_properties.ContainsKey(_task_id) Then
      _task_id = TYPE_MULTISITE_SITE_TASKS.Unknown
    End If

    _properties = m_task_field_properties.Item(_task_id)

    ' Site Status
    _site_status = ENUM_SITE_STATUS.Disconected

    If Not Row.IsNull(SQL_COLUMN_TASKS_LAST_SYNC) Then
      _last_sync_date_time = Row.Item(SQL_COLUMN_TASKS_LAST_SYNC)
      _last_sync = GUI_FormatDate(_last_sync_date_time, _
                                  ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                  ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    If Not Row.IsNull(SQL_COLUMN_TASKS_SITE_STATUS) _
      And Not Row.IsNull(SQL_COLUMN_TASKS_LAST_RECEIVED_MSG) Then

      If Row.Item(SQL_COLUMN_TASKS_SITE_STATUS) = WWP_SessionStatus.Opened Then
        'Check if last message is old enough to consider connection lost
        _date_diff = GUI_GetDBDateTime().Subtract(Row.Item(SQL_COLUMN_TASKS_LAST_RECEIVED_MSG))
        If _date_diff.TotalMilliseconds < WSI.Common.CommonMultiSite.WwpConnectionTimeout Then

          _site_status = ENUM_SITE_STATUS.ConnectedNotSynchronize

          If _last_sync_date_time <> Nothing Then
            _seconds_since_last_sync = GUI_GetDBDateTime().Subtract(_last_sync_date_time).TotalSeconds

            If _properties(DIC_PROPERTY_TASKS_INTERVAL_SECONDS).Editable Then
              _max_value = _properties(DIC_PROPERTY_TASKS_INTERVAL_SECONDS).MaxValue
            Else
              If Not Row.IsNull(SQL_COLUMN_TASKS_SYNC_INTERVAL) Then
                _max_value = Row.Item(SQL_COLUMN_TASKS_SYNC_INTERVAL)
              End If
            End If

            'Inactivity time exceeds 50% the interval defined and is, at least, bigger than one minute
            If _seconds_since_last_sync < (Math.Max(_max_value, Row.Item(SQL_COLUMN_TASKS_SYNC_INTERVAL)) * NO_REPORTING_MARGIN) _
                Or _seconds_since_last_sync < (NO_REPORTING_MIN_ELAPSED_SEC) Then

              _site_status = ENUM_SITE_STATUS.Synchronize

            End If
          End If

        End If
      End If
    End If

    ' Task Status
    _reporting_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(548) '548 "No activity"

    If _site_status = ENUM_SITE_STATUS.Synchronize _
      Or _site_status = ENUM_SITE_STATUS.ConnectedNotSynchronize Then
      If Not Row.IsNull(SQL_COLUMN_TASKS_RUNNING) Then
        _reporting = Row.Item(SQL_COLUMN_TASKS_RUNNING)

        If _reporting Then
          _reporting_status = GLB_NLS_GUI_SYSTEM_MONITOR.GetString(213) '213 "Synchronizing"
        End If
      End If
    End If

    If Not Row.IsNull(SQL_COLUMN_TASKS_SYNC_INTERVAL) Then
      _sync_interval = Row.Item(SQL_COLUMN_TASKS_SYNC_INTERVAL)
    End If

    If Not Row.IsNull(SQL_COLUMN_TASKS_NUM_PENDING_SYNC) Then
      _num_pending_sync = Row.Item(SQL_COLUMN_TASKS_NUM_PENDING_SYNC)
    End If

    _site_status_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2002, "(" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2004) & ")") ' Not Updated ( Disconected )   
    _back_color = m_red_color
    _fore_color = m_yellow_color

    If WWPActive And _site_status = ENUM_SITE_STATUS.Synchronize Then

      _back_color = m_window_color
      _fore_color = m_window_text_color
      _site_status_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2001) ' Updated 

      _sentinel_value = COUNTER_SYNCHRONIZE

    ElseIf WWPActive And _site_status = ENUM_SITE_STATUS.ConnectedNotSynchronize Then
      _site_status_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2002) ' Not Updated

      _back_color = m_yellow_color
      _fore_color = m_black_color

      _sentinel_value = COUNTER_CONNECTED_NOT_SYNCHRONIZE
    Else
      _sentinel_value = COUNTER_DISCONNECTED
    End If

    With Grid
      ' Paint all uncolored cells cause row is defined with a wrong default fore color
      If Not .IsSelected(RowIndex) Then
        .Cell(RowIndex, GRID_COLUMN_TASKS_SITE_ID).ForeColor = Me.m_window_text_color
        .Cell(RowIndex, GRID_COLUMN_TASKS_SITE_NAME).ForeColor = Me.m_window_text_color
        .Cell(RowIndex, GRID_COLUMN_TASKS_SITE_STATUS_ID).ForeColor = Me.m_window_text_color
        .Cell(RowIndex, GRID_COLUMN_TASKS_ID).ForeColor = Me.m_window_text_color
        .Cell(RowIndex, GRID_COLUMN_TASKS_NAME).ForeColor = Me.m_window_text_color
        .Cell(RowIndex, GRID_COLUMN_PENDING_CHANGES).ForeColor = Me.m_window_text_color
        .Cell(RowIndex, GRID_COLUMN_TASKS_LAST_SYNC).ForeColor = Me.m_window_text_color
      End If

      .Cell(RowIndex, GRID_COLUMN_TASKS_SENTINEL).BackColor = _back_color
      .Cell(RowIndex, GRID_COLUMN_TASKS_SENTINEL).ForeColor = _back_color
      .Cell(RowIndex, GRID_COLUMN_TASKS_SITE_ID).Value = _site_id
      .Cell(RowIndex, GRID_COLUMN_TASKS_SITE_NAME).Value = _site_name
      .Cell(RowIndex, GRID_COLUMN_TASKS_SITE_STATUS_ID).Value = _site_status
      .Cell(RowIndex, GRID_COLUMN_TASKS_SITE_STATUS).Value = _site_status_str
      .Cell(RowIndex, GRID_COLUMN_TASKS_SITE_STATUS).BackColor = _back_color
      .Cell(RowIndex, GRID_COLUMN_TASKS_SITE_STATUS).ForeColor = _fore_color
      .Cell(RowIndex, GRID_COLUMN_TASKS_ID).Value = _task_id
      .Cell(RowIndex, GRID_COLUMN_TASKS_NAME).Value = _task_name
      .Cell(RowIndex, GRID_COLUMN_PENDING_CHANGES).Value = _num_pending_sync
      .Cell(RowIndex, GRID_COLUMN_TASKS_LAST_SYNC).Value = _last_sync

      .Counter(_sentinel_value).Value += 1
    End With
  End Sub 'UpdateTaskRow

  ' PURPOSE: Set information from tasks
  '
  '  PARAMS:
  '     - INPUT:
  '           - DataTable
  '           - WWPActive
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '  
  Private Sub SetTasksStatus(ByVal Values As DataTable, ByVal WWPActive As Boolean)
    Dim _idx_row As Integer
    Dim _data_row As DataRow
    Dim _num_tasks_connected As Int32
    Dim _num_tasks_disconnected As Int32
    Dim _filter_status As ENUM_SITE_STATUS

    _num_tasks_connected = 0
    _num_tasks_disconnected = 0

    dg_tasks.Redraw = False

    _idx_row = 0

    dg_tasks.Counter(COUNTER_CONNECTED_NOT_SYNCHRONIZE).Value = 0
    dg_tasks.Counter(COUNTER_SYNCHRONIZE).Value = 0
    dg_tasks.Counter(COUNTER_DISCONNECTED).Value = 0

    ' New rows to add
    For Each _data_row In Values.Rows

      If _idx_row + 1 > Me.dg_tasks.NumRows Then
        Me.dg_tasks.AddRow()
      End If

      ' Wrong default fore color to avoid a issue on returning colored rows to a original color
      Me.dg_tasks.Row(_idx_row).ForeColor = Me.m_dummy_color

      UpdateTaskRow(_data_row, _idx_row, dg_tasks, WWPActive)

      _idx_row += 1
    Next

    ' Delete leftover rows if there are
    While _idx_row < Me.dg_tasks.NumRows
      Me.dg_tasks.DeleteRow(_idx_row)
    End While

    ' Apply status filter 
    If Not Me.opt_tasks_tab_all_status.Checked Then
      If Me.opt_tasks_tab_synchronize.Checked Then
        _filter_status = ENUM_SITE_STATUS.Synchronize
      End If

      If Me.opt_tasks_tab_connected_not_synchronize.Checked Then
        _filter_status = ENUM_SITE_STATUS.ConnectedNotSynchronize
      End If

      If Me.opt_tasks_tab_disconnected.Checked Then
        _filter_status = ENUM_SITE_STATUS.Disconected
      End If

      _idx_row = 0

      While _idx_row < Me.dg_tasks.NumRows
        If dg_tasks.Cell(_idx_row, GRID_COLUMN_TASKS_SITE_STATUS_ID).Value <> _filter_status Then

          ' Site status doesn't match filter's one: detete row
          dg_tasks.Counter(dg_tasks.Cell(_idx_row, GRID_COLUMN_TASKS_SENTINEL).Value).Value -= 1
          dg_tasks.DeleteRowFast(_idx_row)
        Else
          _idx_row += 1
        End If
      End While
    End If

    Call GUI_AdjustGridScrollbars(Me.dg_tasks, GRID_COLUMN_TASKS_SITE_STATUS, 20, GRID_COLUMN_TASKS_SITE_STATUS_WIDTH)

    dg_tasks.Redraw = True
  End Sub 'SetProcessesStatus

  ' PURPOSE: Set information from WWP
  '
  '  PARAMS:
  '     - INPUT:
  '           - DataTable
  '           - WWPActive
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '  
  Private Function SetWWPStatus(ByVal Values As DataTable) As Boolean
    Dim _wwp_status As String
    Dim _wwp_running As Int32
    Dim _wwp_last_access As Int32
    Dim _num_wwp_active As Int32
    Dim _num_wwp_inactive As Int32
    Dim _fore_color As Color
    Dim _back_color As Color
    Dim _wwp_active As Boolean
    Dim _wwp_stopped As String

    _wwp_status = UNDEFINED_VALUE_STR
    _num_wwp_active = 0
    _num_wwp_inactive = 0
    _fore_color = m_yellow_color
    _back_color = m_red_color
    _wwp_active = False
    _wwp_stopped = ""

    For Each _row As DataRow In Values.Rows
      _wwp_running = _row(SQL_COLUMN_RUNNING)
      _wwp_last_access = _row(SQL_COLUMN_LAST_ACCESS)

      If _wwp_running = 0 Or _wwp_last_access >= MAX_SERVICE_TIMEOUT Then
        _num_wwp_inactive += 1
      Else
        _num_wwp_active += 1
      End If
    Next

    _wwp_active = (_num_wwp_active > 0)

    If _wwp_active Then
      _wwp_status = GLB_NLS_GUI_SYSTEM_MONITOR.GetString(206) '206 "Active"

      If _num_wwp_inactive > 0 Then
        If _num_wwp_inactive = 1 Then
          '1794 " (%1 stopped)."  
          _wwp_stopped = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1794, _num_wwp_inactive.ToString())
        Else
          '1790 " (%1 stopped)."  
          _wwp_stopped = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1790, _num_wwp_inactive.ToString())
        End If
      End If

      _fore_color = m_white_color
      _back_color = m_green_color
    Else
      _wwp_status = GLB_NLS_GUI_SYSTEM_MONITOR.GetString(208) '208 "Inactive"
      _fore_color = m_yellow_color
      _back_color = m_red_color
    End If


    Me.lbl_wwp_status_value.Text = _wwp_status
    Me.lbl_wwp_status_value.ForeColor = _fore_color
    Me.lbl_wwp_status_value.BackColor = _back_color
    Me.lbl_stopped.Text = _wwp_stopped
    Me.lbl_stopped.Location = New Point((Me.lbl_wwp_status_value.Location.X + Me.lbl_wwp_status_value.Width + 10), Me.lbl_stopped.Location.Y)

    Return _wwp_active

  End Function

  ' PURPOSE: Set number of connected sites
  '
  '  PARAMS:
  '     - INPUT:
  '           - DataTable
  '           - WWPActive
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '  
  Private Sub SetNumConnectedSites(ByVal Values As DataTable, ByVal WWPActive As Boolean)
    Dim _wws_status As WWP_SessionStatus
    Dim _date_diff As TimeSpan
    Dim _num_sites_connected As Int32
    Dim _num_sites_disconnected As Int32

    _num_sites_connected = 0
    _num_sites_disconnected = 0

    For Each _row As DataRow In Values.Rows
      'Don't count sites that haven't init a session on MultiSite
      If _row.IsNull(SQL_COLUMN_WWS_STATUS) Then
        Continue For
      End If

      _wws_status = _row(SQL_COLUMN_WWS_STATUS)

      _date_diff = GUI_GetDBDateTime().Subtract(_row(SQL_COLUMN_WWS_LAST_RCVD_MSG))

      ' Status
      If _wws_status = WWP_SessionStatus.Opened And WWPActive Then
        'Check if last message is old enough to consider connection lost
        If _date_diff.TotalMilliseconds < CommonMultiSite.WwpConnectionTimeout Then
          _num_sites_connected += 1
        Else
          _num_sites_disconnected += 1
        End If
      Else
        _num_sites_disconnected += 1
      End If
    Next

    If _num_sites_disconnected > 0 Then
      lbl_disconnected_sites.Visible = True
      lbl_num_disconnected_sites.Visible = True
    Else
      lbl_disconnected_sites.Visible = False
      lbl_num_disconnected_sites.Visible = False
    End If
    Me.lbl_connected_sites.Location = New Point((Me.lbl_stopped.Location.X + Me.lbl_stopped.Width + 10), Me.lbl_connected_sites.Location.Y)

    Me.lbl_num_connected_sites.Location = New Point((Me.lbl_connected_sites.Location.X + Me.lbl_connected_sites.Width + 10), Me.lbl_num_connected_sites.Location.Y)
    Me.lbl_num_connected_sites.Text = _num_sites_connected

    Me.lbl_disconnected_sites.Location = New Point((Me.lbl_num_connected_sites.Location.X + Me.lbl_num_connected_sites.Width + 10), Me.lbl_disconnected_sites.Location.Y)

    Me.lbl_num_disconnected_sites.Location = New Point((Me.lbl_disconnected_sites.Location.X + Me.lbl_disconnected_sites.Width + 10), Me.lbl_num_disconnected_sites.Location.Y)
    Me.lbl_num_disconnected_sites.Text = _num_sites_disconnected
  End Sub 'SetNumConnectedSites

  ' PURPOSE: Read information for all
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '  
  Private Sub ExecuteQuery()
    Dim _sb As StringBuilder
    Dim _dataset As DataSet
    Dim _read_type As ENUM_READ_TYPE

    _sb = New StringBuilder()
    _dataset = New DataSet()

    ' Save the actual value of m_read_type to use in this function and RefreshData. Events can change it!
    SyncLock Me
      _read_type = m_read_type
    End SyncLock

    'WWPStatus
    _sb.AppendLine("SELECT   case when SVC_STATUS = 'RUNNING' then 1 else 0 end as RUNNING")
    _sb.AppendLine("		   , DATEDIFF (second, SVC_LAST_ACCESS, GETDATE())")
    _sb.AppendLine("  FROM   SERVICES ")
    _sb.AppendLine(" WHERE   SVC_PROTOCOL = 'WWP CENTER' ")

    If _read_type = ENUM_READ_TYPE.ALL Then
      'NumConnectedSites
      _sb.AppendLine("SELECT   WWS_STATUS  ")
      _sb.AppendLine("       , WWS_LAST_RCVD_MSG   ")
      _sb.AppendLine("  FROM (   ")
      _sb.AppendLine("         SELECT   MAX (WWS_SESSION_ID) as WWS_SESSION_ID  ")
      _sb.AppendLine("           FROM   SITES  ")
      _sb.AppendLine("           LEFT   OUTER JOIN   WWP_SESSIONS ON ST_SITE_ID = WWS_SITE_ID")
      _sb.AppendLine("       GROUP BY   ST_SITE_ID  ")
      _sb.AppendLine("       ) as X  ")
      _sb.AppendLine("  LEFT JOIN   WWP_SESSIONS Y ON X.WWS_SESSION_ID = Y.WWS_SESSION_ID ")


      'SitesConnectionStatus
      _sb.AppendLine("SELECT   ST_SITE_ID ")
      _sb.AppendLine("       , ST_NAME    ")
      _sb.AppendLine("       , WWS_STATUS ")
      _sb.AppendLine("       , WWS_LAST_RCVD_MSG ")
      _sb.AppendLine("  FROM (  ")
      _sb.AppendLine("         SELECT   ST_SITE_ID ")
      _sb.AppendLine("                , ST_NAME    ")
      _sb.AppendLine("                , MAX (WWS_SESSION_ID) as WWS_SESSION_ID  ")
      _sb.AppendLine("           FROM   SITES ")
      _sb.AppendLine("           LEFT   OUTER JOIN   WWP_SESSIONS ON ST_SITE_ID = WWS_SITE_ID")
      _sb.AppendLine("       GROUP BY   ST_SITE_ID, ST_NAME ")
      _sb.AppendLine("       ) as X  ")
      _sb.AppendLine("  LEFT JOIN   WWP_SESSIONS Y ON X.WWS_SESSION_ID = Y.WWS_SESSION_ID     ")
      If Me.ef_site.Value <> "" Then
        _sb.AppendLine("WHERE ST_SITE_ID = " & Me.ef_site.Value & " ")
      End If
      _sb.AppendLine("ORDER BY ST_SITE_ID ")
    End If

    'Tasks
    _sb.AppendLine("SELECT   T.ST_SITE_ID ")
    _sb.AppendLine("       , ST_NAME ")
    _sb.AppendLine("       , WWS_STATUS           AS SITE_STATUS ")
    _sb.AppendLine("       , WWS_LAST_RCVD_MSG    AS LAST_RCVD_MSG ")
    _sb.AppendLine("       , ST_TASK_ID ")
    _sb.AppendLine("       , ST_RUNNING ")
    _sb.AppendLine("       , ST_LAST_RUN ")
    _sb.AppendLine("       , ST_INTERVAL_SECONDS ")
    _sb.AppendLine("       , ST_NUM_PENDING ")
    _sb.AppendLine("  FROM   MS_SITE_TASKS T ")
    _sb.AppendLine(" INNER   JOIN   SITES S ON T.ST_SITE_ID = S.ST_SITE_ID ")
    _sb.AppendLine("  LEFT   OUTER JOIN ( ")
    _sb.AppendLine("                      SELECT   WWS_SITE_ID ")
    _sb.AppendLine("                             , MAX(WWS_LAST_RCVD_MSG) AS WWS_LAST_RCVD_MSG")
    _sb.AppendLine("                             , WWS_STATUS ")
    _sb.AppendLine("                        FROM   WWP_SESSIONS  WHERE WWS_STATUS = 0 ")
    _sb.AppendLine("                    GROUP BY   WWS_SITE_ID, WWS_STATUS ")
    _sb.AppendLine("                    ) LAST_WWP_SESSIONS ON T.ST_SITE_ID = WWS_SITE_ID ")
    _sb.AppendLine(" WHERE   ST_ENABLED = 1 ")

    Select Case m_selected_tab_index
      Case 0 'Sites Tab
        'SiteTasksStatus
        _sb.AppendLine(" AND   T.ST_SITE_ID = @pSiteID ")

      Case 1 'Tasks Tab
        'TasksStatus
        _sb.AppendLine(" AND   ST_TASK_ID = @pTaskID ")
        _sb.AppendLine(" AND   T.ST_SITE_ID <> 0 ")
    End Select
    _sb.AppendLine(" ORDER BY   ST_TASK_ID ")

    Try
      Using _db_trx As New DB_TRX()
        Using _cmd As SqlCommand = New SqlCommand(_sb.ToString())
          _cmd.Parameters.Add("@pSiteID", SqlDbType.Int).Value = GetSelectedSiteID()
          _cmd.Parameters.Add("@pTaskID", SqlDbType.Int).Value = m_selected_process
          Using _da As New SqlClient.SqlDataAdapter(_cmd)
            _db_trx.Fill(_da, _dataset)
          End Using
        End Using
      End Using

      If Me.tf_last_update.InvokeRequired Then
        Call Me.Invoke(New DelegatesRefreshValues(AddressOf RefreshValues), _dataset, _read_type)
      Else
        Call RefreshValues(_dataset, _read_type)
      End If
    Catch

    Finally
      SyncLock Me
        m_read_type = ENUM_READ_TYPE.ALL
      End SyncLock
    End Try
  End Sub

  ' PURPOSE: Refresh values
  '
  '  PARAMS:
  '     - INPUT:
  '           - DataSet
  '           - ENUM_READ_TYPE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '  
  Private Sub RefreshValues(ByVal Values As DataSet, ByVal TypeRead As ENUM_READ_TYPE)
    Dim _wwp_status As Boolean
    Dim _idx_table As Int32

    _idx_table = 0

    'WWPStatus
    _wwp_status = SetWWPStatus(Values.Tables(_idx_table))

    If TypeRead = ENUM_READ_TYPE.ALL Then
      'NumConnectedSites
      _idx_table += 1
      Call SetNumConnectedSites(Values.Tables(_idx_table), _wwp_status)

      _idx_table += 1
      'SitesConnectionStatus
      Call SetSitesConnectionStatus(Values.Tables(_idx_table), _wwp_status)
    End If

    _idx_table += 1
    'Tasks
    Select Case m_selected_tab_index
      Case 0 'Sites Tab
        'SiteTasksStatus
        Call SetSiteTasksStatus(Values.Tables(_idx_table), _wwp_status)

      Case 1 'Tasks Tab
        'TasksStatus
        Call SetTasksStatus(Values.Tables(_idx_table), _wwp_status)
    End Select

    Me.tf_last_update.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(324)
    Me.tf_last_update.Value = GUI_FormatDate(GUI_GetDBDateTime, _
                                             ENUM_FORMAT_DATE.FORMAT_DATE_NONE, _
                                             ENUM_FORMAT_TIME.FORMAT_HHMMSS)
  End Sub

  ' PURPOSE: Gets the site id from the selected row
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     
  Private Function GetSelectedSiteID() As Int32
    Dim _site_id As Int32

    _site_id = -1

    If m_selected_row >= 0 And Me.dg_sites.NumRows > m_selected_row Then
      _site_id = Me.dg_sites.Cell(m_selected_row, GRID_COLUMN_SITES_SITE_ID).Value
    End If

    Return _site_id
  End Function

  ' PURPOSE: Fill the processes combo box
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - none
  Private Sub FillProcessesCombo(ByRef SqlTrx As SqlTransaction)
    Dim _str_bld As StringBuilder
    Dim _task_id As Int32


    cmb_processes.Clear()

    cmb_processes.Add(-1, "") 'Empty combo entry

    Try

      _str_bld = New StringBuilder()

      _str_bld.AppendLine("  SELECT   ST_TASK_ID    ")
      _str_bld.AppendLine("    FROM   MS_SITE_TASKS ")
      _str_bld.AppendLine("   WHERE   ST_SITE_ID = 0")
      _str_bld.AppendLine("     AND   ST_ENABLED = 1")
      _str_bld.AppendLine("ORDER BY   ST_TASK_ID    ")

      Using _sql_cmd As SqlCommand = New SqlCommand(_str_bld.ToString(), SqlTrx.Connection, SqlTrx)
        Using _sql_reader As SqlDataReader = _sql_cmd.ExecuteReader()

          While _sql_reader.Read()
            _task_id = _sql_reader.GetInt32(0)

            cmb_processes.Add(_task_id, mdl_multisite.TranslateMultiSiteProcessID(_task_id))
          End While
        End Using
      End Using
    Catch _ex As Exception
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                      "frm_sites_monitor", _
                      "FillProcessesCombo", _
                      _ex.Message)
    End Try

  End Sub 'FillProcessesCombo

  ' PURPOSE: Force the selected site's task to totally resync
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '   
  Private Sub ForceReSync()
    Dim _selected_rows() As Integer
    Dim _selected_row As Integer
    Dim _task_id As TYPE_MULTISITE_SITE_TASKS
    Dim _resyncable_tasks() As TYPE_RESYNCABLE_TASKS
    Dim _resyncable_task As TYPE_RESYNCABLE_TASKS
    Dim _resyncable_description_tasks As String
    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _str_bld As StringBuilder
    Dim _sql_command As SqlCommand
    Dim _sql_transaction As System.Data.SqlClient.SqlTransaction
    Dim _num_rows_updated As Integer
    Dim _commit As Boolean
    Dim _ctx As Integer
    Dim _grid As uc_grid
    Dim _previous_exists As Boolean

    _sql_transaction = Nothing
    _commit = False
    _grid = Nothing
    _resyncable_tasks = Nothing
    _resyncable_description_tasks = vbCrLf

    Try

      Select Case TabControl.SelectedIndex
        Case 0 'Sites Tab
          _grid = Me.dg_site_tasks
        Case 1 'Tasks Tab
          _grid = Me.dg_tasks
      End Select

      If _grid.SelectedRows() Is Nothing Then
        '1782 "You should select one of following tasks to force its update:\n%1"
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1782), _
                        mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, _
                        mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, _
                        , _
                        mdl_multisite.GetReSyncableTasksList())

        Return
      End If

      _selected_rows = _grid.SelectedRows
      Array.Resize(_resyncable_tasks, 0)

      For Each _selected_row In _selected_rows
        _task_id = _grid.Cell(_selected_row, GRID_COLUMN_TASKS_ID).Value()

        If Array.IndexOf(ReSyncTasks, _task_id) <> -1 Then

          Array.Resize(_resyncable_tasks, _resyncable_tasks.Length + 1)
          _resyncable_tasks(_resyncable_tasks.Length - 1).TaskId = _task_id
          _resyncable_tasks(_resyncable_tasks.Length - 1).TaskName = mdl_multisite.TranslateMultiSiteProcessID(_task_id)
          _resyncable_tasks(_resyncable_tasks.Length - 1).SiteId = _grid.Cell(_selected_row, GRID_COLUMN_TASKS_SITE_ID).Value()
          _resyncable_tasks(_resyncable_tasks.Length - 1).SiteName = _grid.Cell(_selected_row, GRID_COLUMN_TASKS_SITE_NAME).Value()

          _resyncable_description_tasks &= vbCrLf & " - " & _resyncable_tasks(_resyncable_tasks.Length - 1).TaskName & _
                                        " (" & _resyncable_tasks(_resyncable_tasks.Length - 1).SiteName & ")"

          If _task_id = TYPE_MULTISITE_SITE_TASKS.DownloadCorporateUsers Then
            _previous_exists = False
            For Each _task As TYPE_RESYNCABLE_TASKS In _resyncable_tasks
              If _task.TaskId = TYPE_MULTISITE_SITE_TASKS.DownloadMasterProfiles Then
                _previous_exists = True
                Exit For
              End If
            Next

            If Not _previous_exists Then

              Array.Resize(_resyncable_tasks, _resyncable_tasks.Length + 1)
              _resyncable_tasks(_resyncable_tasks.Length - 1).TaskId = TYPE_MULTISITE_SITE_TASKS.DownloadMasterProfiles
              _resyncable_tasks(_resyncable_tasks.Length - 1).TaskName = mdl_multisite.TranslateMultiSiteProcessID(TYPE_MULTISITE_SITE_TASKS.DownloadMasterProfiles)
              _resyncable_tasks(_resyncable_tasks.Length - 1).SiteId = _grid.Cell(_selected_row, GRID_COLUMN_TASKS_SITE_ID).Value()
              _resyncable_tasks(_resyncable_tasks.Length - 1).SiteName = _grid.Cell(_selected_row, GRID_COLUMN_TASKS_SITE_NAME).Value()

              _resyncable_description_tasks &= vbCrLf & " - " & _resyncable_tasks(_resyncable_tasks.Length - 1).TaskName & _
                                            " (" & _resyncable_tasks(_resyncable_tasks.Length - 1).SiteName & ")"

            End If

          End If
        End If
      Next

      If _resyncable_tasks.Length = 0 Then
        '1782 "You should select one of following tasks to force its update:\n%1"
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1782), _
                        mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, _
                        mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, _
                        , _
                        mdl_multisite.GetReSyncableTasksList())

        Return
      End If

      '1837 "Are you sure you want to force update of following tasks? %1"
      If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1837), _
                    ENUM_MB_TYPE.MB_TYPE_INFO, _
                    ENUM_MB_BTN.MB_BTN_YES_NO, _
                    , _
                    _resyncable_description_tasks) = ENUM_MB_RESULT.MB_RESULT_NO Then
        Return
      End If

      If Not GUI_BeginSQLTransaction(_sql_transaction) Then
        Return
      End If

      For Each _resyncable_task In _resyncable_tasks

        _str_bld = New StringBuilder()

        _str_bld.AppendLine("UPDATE MS_SITE_TASKS         ")
        _str_bld.AppendLine("   SET ST_FORCE_RESYNCH = 1  ")
        _str_bld.AppendLine(" WHERE ST_TASK_ID = @pTaskId ")
        _str_bld.AppendLine("   AND ST_SITE_ID = @pSiteId ")

        _sql_command = New SqlCommand(_str_bld.ToString(), _sql_transaction.Connection, _sql_transaction)

        _sql_command.Parameters.Add("@pTaskId", SqlDbType.Int).Value = _resyncable_task.TaskId
        _sql_command.Parameters.Add("@pSiteId", SqlDbType.Int).Value = _resyncable_task.SiteId

        _num_rows_updated = _sql_command.ExecuteNonQuery()

        If _num_rows_updated <> 1 Then
          Return
        End If

        _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_MULTISITE)

        Call _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(1697), _
                                   GLB_NLS_GUI_PLAYER_TRACKING.GetString(1783)) '1783 "Update started for task %1."

        Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1773), _
                                    _resyncable_task.SiteName)

        Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1784), _
                                    _resyncable_task.TaskName)

        If Not _auditor_data.Notify(CurrentUser.GuiId, _
                                    CurrentUser.Id, _
                                    CurrentUser.Name, _
                                    CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.GENERIC, _
                                    _ctx) Then
          Return
        End If

      Next

      '1785 "Update was forced for the following tasks: %1"
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1785), _
                      mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO, _
                      mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, _
                      , _
                      _resyncable_description_tasks)

      _commit = True

    Catch ex As Exception
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "frm_multisite_monitor", _
                            "ForceReSync", _
                            ex.Message)
    Finally
      GUI_EndSQLTransaction(_sql_transaction, _commit)
    End Try

  End Sub 'ForceReSync

  ' PURPOSE: Thread method to update information
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '  
  Private Sub DataRefresh()
    m_start_refresh = New AutoResetEvent(False)
    m_read_type = ENUM_READ_TYPE.ALL
    While True

      Call ExecuteQuery()

      m_start_refresh.Reset()

      m_start_refresh.WaitOne(REFRESH_INTERVAL, False)

    End While
  End Sub


#End Region ' Private Functions

#Region " Events "

  ' PURPOSE: Execute proper actions when user presses the 'Exit' button
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     
  Private Sub btn_cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_exit.Click
    Me.Close()
  End Sub

  Private Sub dg_sites_RowSelectedEvent(ByVal SelectedRow As System.Int32)
    If m_selected_row = SelectedRow Then
      Return
    End If

    m_selected_row = SelectedRow
    SyncLock Me
      m_read_type = ENUM_READ_TYPE.SITETASKS
    End SyncLock
    m_start_refresh.Set()

  End Sub

  Private Sub cmb_processes_ValueChangedEvent()
    Try
      Windows.Forms.Cursor.Current = Cursors.WaitCursor

      SyncLock Me
        m_read_type = ENUM_READ_TYPE.TASKS
      End SyncLock

      m_selected_process = cmb_processes.Value
      m_start_refresh.Set()

    Finally
      Windows.Forms.Cursor.Current = Cursors.Default
    End Try
  End Sub

  Private Sub btn_refresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_refresh.Click
    Try
      Windows.Forms.Cursor.Current = Cursors.WaitCursor

      SyncLock Me
        m_read_type = ENUM_READ_TYPE.ALL
      End SyncLock

      m_start_refresh.Set()

    Finally
      Windows.Forms.Cursor.Current = Cursors.Default
    End Try
  End Sub

  Private Sub TabControl_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TabControl.SelectedIndexChanged

    Dim _current_selected_task As Byte

    m_selected_tab_index = TabControl.SelectedIndex

    If TabControl.SelectedTab.Name = Me.tab_processes.Name Then

      If (Me.cmb_processes.Count = 0) Then
        Using _db_trx As New DB_TRX
          Call FillProcessesCombo(_db_trx.SqlTransaction)
        End Using

        '' Add handler to event after fill data ;)
        AddHandler cmb_processes.ValueChangedEvent, AddressOf cmb_processes_ValueChangedEvent
      End If

      If Not dg_site_tasks.SelectedRows Is Nothing _
          AndAlso dg_site_tasks.SelectedRows().Length = 1 Then
        _current_selected_task = Me.dg_site_tasks.Cell(dg_site_tasks.SelectedRows(0), GRID_COLUMN_TASKS_ID).Value
        cmb_processes.Value = _current_selected_task

      Else
        cmb_processes.SelectedIndex = -1
      End If
    End If

    SyncLock Me
      m_read_type = ENUM_READ_TYPE.ALL
    End SyncLock

    m_start_refresh.Set()
  End Sub

  Private Sub btn_force_sync_MouseClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btn_force_sync.MouseClick
    ForceReSync()
  End Sub

  Private Sub opt_sites_tab_all_status_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_sites_tab_all_status.CheckedChanged
    If Me.opt_sites_tab_all_status.Checked Then
      SyncLock Me
        m_read_type = ENUM_READ_TYPE.ALL
      End SyncLock
      Me.dg_sites.ClearSelection()
      m_selected_row = -1
      m_start_refresh.Set()
    End If
  End Sub

  Private Sub opt_sites_tab_synchronize_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_sites_tab_synchronize.CheckedChanged
    If Me.opt_sites_tab_synchronize.Checked Then
      SyncLock Me
        m_read_type = ENUM_READ_TYPE.ALL
      End SyncLock
      Me.dg_sites.ClearSelection()
      m_selected_row = -1
      m_start_refresh.Set()
    End If
  End Sub

  Private Sub opt_sites_tab_connected_not_synchronize_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_sites_tab_connected_not_synchronize.CheckedChanged
    If Me.opt_sites_tab_connected_not_synchronize.Checked Then
      SyncLock Me
        m_read_type = ENUM_READ_TYPE.ALL
      End SyncLock
      Me.dg_sites.ClearSelection()
      m_selected_row = -1
      m_start_refresh.Set()
    End If
  End Sub

  Private Sub opt_sites_tab_disconnected_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_sites_tab_disconnected.CheckedChanged
    If Me.opt_sites_tab_disconnected.Checked Then
      SyncLock Me
        m_read_type = ENUM_READ_TYPE.ALL
      End SyncLock
      Me.dg_sites.ClearSelection()
      m_selected_row = -1
      m_start_refresh.Set()
    End If
  End Sub

  Private Sub opt_tasks_tab_all_status_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_tasks_tab_all_status.CheckedChanged
    If Me.opt_tasks_tab_all_status.Checked Then
      SyncLock Me
        m_read_type = ENUM_READ_TYPE.ALL
      End SyncLock
      m_start_refresh.Set()
    End If
  End Sub

  Private Sub opt_tasks_tab_synchronize_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_tasks_tab_synchronize.CheckedChanged
    If Me.opt_tasks_tab_synchronize.Checked Then
      SyncLock Me
        m_read_type = ENUM_READ_TYPE.ALL
      End SyncLock
      m_start_refresh.Set()
    End If
  End Sub

  Private Sub opt_tasks_tab_connected_not_synchronize_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_tasks_tab_connected_not_synchronize.CheckedChanged
    If Me.opt_tasks_tab_connected_not_synchronize.Checked Then
      SyncLock Me
        m_read_type = ENUM_READ_TYPE.ALL
      End SyncLock
      m_start_refresh.Set()
    End If
  End Sub

  Private Sub opt_tasks_tab_disconnected_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_tasks_tab_disconnected.CheckedChanged
    If Me.opt_tasks_tab_disconnected.Checked Then
      SyncLock Me
        m_read_type = ENUM_READ_TYPE.ALL
      End SyncLock
      m_start_refresh.Set()
    End If
  End Sub

#End Region ' Events

End Class

