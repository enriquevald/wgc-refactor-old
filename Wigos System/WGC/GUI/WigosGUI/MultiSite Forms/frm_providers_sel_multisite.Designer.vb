<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_providers_sel_multisite
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_providers_sel_multisite))
    Me.ef_providers_games_name = New GUI_Controls.uc_entry_field()
    Me.uc_site_select = New GUI_Controls.uc_sites_sel()
    Me.chk_games = New System.Windows.Forms.CheckBox()
    Me.chk_3gs = New System.Windows.Forms.CheckBox()
    Me.panel_filter.SuspendLayout
    Me.panel_data.SuspendLayout
    Me.pn_separator_line.SuspendLayout
    Me.SuspendLayout
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.chk_games)
    Me.panel_filter.Controls.Add(Me.chk_3gs)
    Me.panel_filter.Controls.Add(Me.uc_site_select)
    Me.panel_filter.Controls.Add(Me.ef_providers_games_name)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(800, 135)
    Me.panel_filter.Controls.SetChildIndex(Me.ef_providers_games_name, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_site_select, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_3gs, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_games, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(5, 139)
    Me.panel_data.Size = New System.Drawing.Size(800, 478)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(794, 10)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(794, 10)
    '
    'ef_providers_games_name
    '
    Me.ef_providers_games_name.AccessibleRole = System.Windows.Forms.AccessibleRole.IpAddress
    Me.ef_providers_games_name.DoubleValue = 0.0R
    Me.ef_providers_games_name.IntegerValue = 0
    Me.ef_providers_games_name.IsReadOnly = False
    Me.ef_providers_games_name.Location = New System.Drawing.Point(293, 17)
    Me.ef_providers_games_name.Name = "ef_providers_games_name"
    Me.ef_providers_games_name.PlaceHolder = Nothing
    Me.ef_providers_games_name.ShortcutsEnabled = True
    Me.ef_providers_games_name.Size = New System.Drawing.Size(232, 24)
    Me.ef_providers_games_name.SufixText = "Sufix Text"
    Me.ef_providers_games_name.SufixTextVisible = True
    Me.ef_providers_games_name.TabIndex = 11
    Me.ef_providers_games_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_providers_games_name.TextValue = ""
    Me.ef_providers_games_name.TextWidth = 70
    Me.ef_providers_games_name.Value = ""
    Me.ef_providers_games_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'uc_site_select
    '
    Me.uc_site_select.Location = New System.Drawing.Point(6, 6)
    Me.uc_site_select.MultiSelect = True
    Me.uc_site_select.Name = "uc_site_select"
    Me.uc_site_select.ShowIsoCode = False
    Me.uc_site_select.ShowMultisiteRow = True
    Me.uc_site_select.Size = New System.Drawing.Size(288, 126)
    Me.uc_site_select.TabIndex = 12
    '
    'chk_games
    '
    Me.chk_games.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.chk_games.Location = New System.Drawing.Point(386, 47)
    Me.chk_games.Name = "chk_games"
    Me.chk_games.Size = New System.Drawing.Size(139, 17)
    Me.chk_games.TabIndex = 20
    Me.chk_games.Text = "xGames"
    Me.chk_games.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.chk_games.UseVisualStyleBackColor = true
    '
    'chk_3gs
    '
    Me.chk_3gs.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.chk_3gs.Location = New System.Drawing.Point(293, 47)
    Me.chk_3gs.Name = "chk_3gs"
    Me.chk_3gs.Size = New System.Drawing.Size(139, 17)
    Me.chk_3gs.TabIndex = 19
    Me.chk_3gs.Text = "x3gs"
    Me.chk_3gs.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.chk_3gs.UseVisualStyleBackColor = true
    '
    'frm_providers_sel_multisite
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7!, 13!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(810, 621)
    Me.Icon = CType(resources.GetObject("$this.Icon"),System.Drawing.Icon)
    Me.Name = "frm_providers_sel_multisite"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_providers_sel"
    Me.panel_filter.ResumeLayout(false)
    Me.panel_data.ResumeLayout(false)
    Me.pn_separator_line.ResumeLayout(false)
    Me.ResumeLayout(false)

End Sub
  Friend WithEvents ef_providers_games_name As GUI_Controls.uc_entry_field
  Friend WithEvents uc_site_select As GUI_Controls.uc_sites_sel
  Friend WithEvents chk_games As System.Windows.Forms.CheckBox
  Friend WithEvents chk_3gs As System.Windows.Forms.CheckBox
End Class
