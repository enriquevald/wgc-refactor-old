'-------------------------------------------------------------------
' Copyright � 2007-2009 Win Systems International Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_account_movements
'
' DESCRIPTION:   This screen allows to view account movements:
'                           - between two dates 
'                           - for one or all cashiers
'                           - for one or all users
'                           - for one or all movements types
'                           - for one or all cashier sessions
' AUTHOR:        Agust� Poch
' CREATION DATE: 06-SEP-2007
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 06-SEP-2007  APB    Initial version
' 21-OCT-2011  JCA    Added new movement (Card Adjustment)
' 22-NOV-2011  JMM    Added new movements (PIN Change & PIN Generation)
' 17-FEB-2012  RCI    Added new movements (CashIn CoverCoupon & Not Redeemable 2 Expiration)
' 24-APR-2012  JCM    Added new movement (Recycle Card)
' 04-JUN-2012  JAB    Eliminate top clause of sql query
' 05-JUL-2012  DDM    Added new movement (MOV_TYPE_DB_REDEEMABLE). Modified some names of movements
' 31-JUL-2012  DDM    Added new movement (Promo. Per Points & Promo. Per Points Cancelled). 
' 27-AUG-2012  RRB    Added new information for account adjustment (Reasons and Details).
' 06-FEB-2013  RRB    Added new movement (PointsStatusChanged).
' 12-FEB-2013  JMM    uc_account_sel.ValidateFormat added on FilterCheck
' 06-MAR-2013  MPO    Copy frm_account_movements to frm_account_movements_multisite and move to MultiSite Forms
' 27-MAR-2013  AMF    Fixed Bug #672: Failed to activate filter Date To
' 27-MAR-2013  AMF    Add user control uc_sitel_sel to filter
' 18-SEP-2013  JML    No view balance for Cash draw participation
' 22-OCT-2013  LEM    Show TrackData movement info instead TrackData account info
' 31-OCT-2013  LEM    Fixed Bug WIG-363: Show (AC_TRACK_DATA*) for movements without AM_TRACK_DATA
' 06-MAY-2014  JBP    Added Vip client filter at reports.
' 25-JUN-2015  FOS    Created new parameters to amount input company b
' 30-MAY-2017  JML    Fixed Bug 27731: Multisite does not take into account points redeemed in external system
' 19-JUL-2017  ETP    Fixed Bug 28846: Exception in GUI Account Movements multisite.
' 02-AUG-2017  DPC    WIGOS-4138: Creditline: Accounts movements do not appear in MS
' 26-SEP-2017  ETP    Fixed Bug 29922:[WIGOS-4563] Multisite: wrong site number when exporting data to Excel or Print
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data

Public Class frm_account_movements_multisite
  Inherits GUI_Controls.frm_base_sel

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    ' RRB 27-AUG-2012 Set FormId
    Me.FormId = ENUM_FORM.FORM_ACCOUNT_MOVEMENTS
    Me.m_account_movements_type_mode = ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE.TYPE_NORMAL

    Call MyBase.GUI_SetFormId()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  Public Sub New(ByVal FormId As ENUM_FORM)
    MyBase.New()

    Me.FormId = FormId
    If FormId = ENUM_FORM.FORM_ACCOUNT_MOVEMENTS Then
      Me.m_account_movements_type_mode = ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE.TYPE_NORMAL
    Else
      Me.m_account_movements_type_mode = ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE.TYPE_DETAILS
    End If

    Call MyBase.GUI_SetFormId()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents uc_account_sel1 As GUI_Controls.uc_account_sel
  Friend WithEvents gb_type As System.Windows.Forms.GroupBox
  Friend WithEvents opt_level_change As System.Windows.Forms.RadioButton
  Friend WithEvents opt_points_accreditations As System.Windows.Forms.RadioButton
  Friend WithEvents uc_account_mov_sites_sel As GUI_Controls.uc_sites_sel
  Friend WithEvents lbl_trackdata_note As GUI_Controls.uc_text_field
  Friend WithEvents opt_points_status As System.Windows.Forms.RadioButton
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_account_movements_multisite))
    Me.gb_date = New System.Windows.Forms.GroupBox()
    Me.dtp_to = New GUI_Controls.uc_date_picker()
    Me.dtp_from = New GUI_Controls.uc_date_picker()
    Me.uc_account_sel1 = New GUI_Controls.uc_account_sel()
    Me.gb_type = New System.Windows.Forms.GroupBox()
    Me.opt_points_status = New System.Windows.Forms.RadioButton()
    Me.opt_level_change = New System.Windows.Forms.RadioButton()
    Me.opt_points_accreditations = New System.Windows.Forms.RadioButton()
    Me.uc_account_mov_sites_sel = New GUI_Controls.uc_sites_sel()
    Me.lbl_trackdata_note = New GUI_Controls.uc_text_field()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.gb_type.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.lbl_trackdata_note)
    Me.panel_filter.Controls.Add(Me.uc_account_mov_sites_sel)
    Me.panel_filter.Controls.Add(Me.uc_account_sel1)
    Me.panel_filter.Controls.Add(Me.gb_type)
    Me.panel_filter.Controls.Add(Me.gb_date)
    resources.ApplyResources(Me.panel_filter, "panel_filter")
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_type, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_account_sel1, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_account_mov_sites_sel, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_trackdata_note, 0)
    '
    'panel_data
    '
    resources.ApplyResources(Me.panel_data, "panel_data")
    '
    'pn_separator_line
    '
    resources.ApplyResources(Me.pn_separator_line, "pn_separator_line")
    '
    'pn_line
    '
    resources.ApplyResources(Me.pn_line, "pn_line")
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Controls.Add(Me.dtp_from)
    resources.ApplyResources(Me.gb_date, "gb_date")
    Me.gb_date.Name = "gb_date"
    Me.gb_date.TabStop = False
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    resources.ApplyResources(Me.dtp_to, "dtp_to")
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TextWidth = 42
    Me.dtp_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    resources.ApplyResources(Me.dtp_from, "dtp_from")
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = False
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TextWidth = 42
    Me.dtp_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'uc_account_sel1
    '
    Me.uc_account_sel1.Account = ""
    Me.uc_account_sel1.AccountText = ""
    Me.uc_account_sel1.Anon = False
    resources.ApplyResources(Me.uc_account_sel1, "uc_account_sel1")
    Me.uc_account_sel1.BirthDate = New Date(CType(0, Long))
    Me.uc_account_sel1.DisabledHolder = False
    Me.uc_account_sel1.Holder = ""
    Me.uc_account_sel1.MassiveSearchNumbers = ""
    Me.uc_account_sel1.MassiveSearchNumbersToEdit = ""
    Me.uc_account_sel1.MassiveSearchType = 0
    Me.uc_account_sel1.Name = "uc_account_sel1"
    Me.uc_account_sel1.SearchTrackDataAsInternal = True
    Me.uc_account_sel1.ShowMassiveSearch = False
    Me.uc_account_sel1.ShowVipClients = True
    Me.uc_account_sel1.Telephone = ""
    Me.uc_account_sel1.TrackData = ""
    Me.uc_account_sel1.Vip = False
    Me.uc_account_sel1.WeddingDate = New Date(CType(0, Long))
    '
    'gb_type
    '
    Me.gb_type.Controls.Add(Me.opt_points_status)
    Me.gb_type.Controls.Add(Me.opt_level_change)
    Me.gb_type.Controls.Add(Me.opt_points_accreditations)
    resources.ApplyResources(Me.gb_type, "gb_type")
    Me.gb_type.Name = "gb_type"
    Me.gb_type.TabStop = False
    '
    'opt_points_status
    '
    resources.ApplyResources(Me.opt_points_status, "opt_points_status")
    Me.opt_points_status.Name = "opt_points_status"
    Me.opt_points_status.UseVisualStyleBackColor = True
    '
    'opt_level_change
    '
    resources.ApplyResources(Me.opt_level_change, "opt_level_change")
    Me.opt_level_change.Name = "opt_level_change"
    Me.opt_level_change.UseVisualStyleBackColor = True
    '
    'opt_points_accreditations
    '
    resources.ApplyResources(Me.opt_points_accreditations, "opt_points_accreditations")
    Me.opt_points_accreditations.Checked = True
    Me.opt_points_accreditations.Name = "opt_points_accreditations"
    Me.opt_points_accreditations.TabStop = True
    Me.opt_points_accreditations.UseVisualStyleBackColor = True
    '
    'uc_account_mov_sites_sel
    '
    resources.ApplyResources(Me.uc_account_mov_sites_sel, "uc_account_mov_sites_sel")
    Me.uc_account_mov_sites_sel.MultiSelect = True
    Me.uc_account_mov_sites_sel.Name = "uc_account_mov_sites_sel"
    Me.uc_account_mov_sites_sel.ShowIsoCode = False
    Me.uc_account_mov_sites_sel.ShowMultisiteRow = True
    '
    'lbl_trackdata_note
    '
    Me.lbl_trackdata_note.IsReadOnly = True
    Me.lbl_trackdata_note.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_trackdata_note.LabelForeColor = System.Drawing.Color.Blue
    resources.ApplyResources(Me.lbl_trackdata_note, "lbl_trackdata_note")
    Me.lbl_trackdata_note.Name = "lbl_trackdata_note"
    Me.lbl_trackdata_note.SufixText = "Sufix Text"
    Me.lbl_trackdata_note.SufixTextVisible = True
    Me.lbl_trackdata_note.TextVisible = False
    Me.lbl_trackdata_note.TextWidth = 0
    Me.lbl_trackdata_note.Value = "* TrackData Note"
    '
    'frm_account_movements_multisite
    '
    resources.ApplyResources(Me, "$this")
    Me.Name = "frm_account_movements_multisite"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_date.ResumeLayout(False)
    Me.gb_type.ResumeLayout(False)
    Me.gb_type.PerformLayout()
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Constants "

  ' DB Columns
  Private Const SQL_COLUMN_SITE_ID As Integer = 0
  Private Const SQL_COLUMN_SITE_NAME As Integer = 1
  Private Const SQL_COLUMN_ACCT_NUMBER As Integer = 2
  Private Const SQL_COLUMN_ACCOUNT_TYPE As Integer = 3
  Private Const SQL_COLUMN_CARD_TRACK As Integer = 4
  Private Const SQL_COLUMN_HOLDER_NAME As Integer = 5
  Private Const SQL_COLUMN_DATE As Integer = 6
  Private Const SQL_COLUMN_MACHINE_ID As Integer = 7
  Private Const SQL_COLUMN_MACHINE_NAME As Integer = 8
  Private Const SQL_COLUMN_TYPE_ID As Integer = 9
  Private Const SQL_COLUMN_INIT_BALANCE As Integer = 10
  Private Const SQL_COLUMN_SUB_AMOUNT As Integer = 11
  Private Const SQL_COLUMN_ADD_AMOUNT As Integer = 12
  Private Const SQL_COLUMN_FINAL_BALANCE As Integer = 13
  Private Const SQL_COLUMN_AM_TRACKDATA As Integer = 14
  Private Const SQL_COLUMN_DETAILS As Integer = 15
  Private Const SQL_COLUMN_REASONS As Integer = 16



  ' Grid Columns
  Private Const GRID_COLUMN_SITE_ID As Integer = 0
  Private Const GRID_COLUMN_SITE_NAME As Integer = 1
  Private Const GRID_COLUMN_ACCT_NUMBER As Integer = 2
  Private Const GRID_COLUMN_ACCOUNT_TYPE As Integer = 3
  Private Const GRID_COLUMN_CARD_TRACK As Integer = 4
  Private Const GRID_COLUMN_HOLDER_NAME As Integer = 5
  Private Const GRID_COLUMN_DATE As Integer = 6
  Private Const GRID_COLUMN_TYPE_ID As Integer = 7
  Private Const GRID_COLUMN_TYPE_NAME As Integer = 8
  Private GRID_COLUMN_TERMINAL_ID As Integer = 9
  Private Const GRID_COLUMN_TERMINAL_NAME As Integer = 10
  Private Const GRID_COLUMN_INIT_BALANCE As Integer = 11
  Private Const GRID_COLUMN_SUB_AMOUNT As Integer = 12
  Private GRID_COLUMN_ADD_AMOUNT As Integer = 13
  Private Const GRID_COLUMN_FINAL_BALANCE As Integer = 14
  Private Const GRID_COLUMN_REASONS As Integer = 15
  Private Const GRID_COLUMN_DETAILS As Integer = 16

  Private Const GRID_COLUMNS_NORMAL As Integer = 15
  Private Const GRID_COLUMNS_DETAILS As Integer = 17
  Private Const GRID_HEADER_ROWS As Integer = 2

  ' Width
  Private Const GRID_WIDTH_DATE As Integer = 2000
  Private Const GRID_WIDTH_TIME As Integer = 1300
  Private Const GRID_WIDTH_TERMINAL As Integer = 1700
  Private Const GRID_WIDTH_AMOUNT_SHORT As Integer = 1100
  Private Const GRID_WIDTH_AMOUNT_LONG As Integer = 1400
  Private Const GRID_WIDTH_MOVEMENT_TYPE_NAME As Integer = 2030
  Private Const GRID_WIDTH_CARD_HOLDER_NAME As Integer = 2150
  Private Const GRID_WIDTH_CARD_TRACK_DATA As Integer = 2300
  Private Const GRID_WIDTH_REASONS As Integer = 4000
  Private Const GRID_WIDTH_DETAILS As Integer = 10000

  ' Excel columns
  Private Const EXCEL_COLUMN_SITE_ID As Integer = 0


  ' Database codes
  Private Const MOV_TYPE_DB_PLAY As Integer = 0
  Private Const MOV_TYPE_DB_CASH_IN As Integer = 1
  Private Const MOV_TYPE_DB_CASH_OUT As Integer = 2
  Private Const MOV_TYPE_DB_DEVOLUTION As Integer = 3
  Private Const MOV_TYPE_DB_FEDERAL_TAXES As Integer = 4
  Private Const MOV_TYPE_DB_START_SESSION As Integer = 5
  Private Const MOV_TYPE_DB_END_SESSION As Integer = 6
  Private Const MOV_TYPE_DB_ACTIVATE As Integer = 7
  Private Const MOV_TYPE_DB_DEACTIVATE As Integer = 8
  Private Const MOV_TYPE_DB_NO_REDEEMABLE As Integer = 9
  Private Const MOV_TYPE_DB_CARD_DEPOSIT_IN As Integer = 10
  Private Const MOV_TYPE_DB_CARD_DEPOSIT_OUT As Integer = 11
  Private Const MOV_TYPE_DB_CARD_NO_REDEEMABLE_CANCEL As Integer = 12
  Private Const MOV_TYPE_DB_STATE_TAXES As Integer = 13
  ' = 14
  ' = 15
  ' = 16
  ' = 17
  ' = 18
  ' = 19
  Private Const MOV_TYPE_DB_PROMO_CREDITS As Integer = 20
  Private Const MOV_TYPE_DB_PROMO_TO_REDEEMABLE As Integer = 21
  Private Const MOV_TYPE_DB_PROMO_EXPIRED As Integer = 22
  Private Const MOV_TYPE_DB_PROMO_CANCEL As Integer = 23
  Private Const MOV_TYPE_DB_PROMO_START_SESSION As Integer = 24
  Private Const MOV_TYPE_DB_PROMO_END_SESSION As Integer = 25

  Private Const MOV_TYPE_DB_CREDITS_EXPIRED As Integer = 26
  Private Const MOV_TYPE_DB_CREDITS_NOT_REDEEMABLE_EXPIRED As Integer = 27

  Private Const MOV_TYPE_DB_CARD_REPLACEMENT As Integer = 28

  Private Const MOV_TYPE_DB_CARD_DRAW_TICKET_PRINT As Integer = 29
  Private Const MOV_TYPE_DB_CARD_HANDPAY As Integer = 30
  Private Const MOV_TYPE_DB_CARD_HANDPAY_CANCELLATION As Integer = 31

  Private Const MOV_TYPE_DB_MANUAL_END_SESSION As Integer = 32
  Private Const MOV_TYPE_DB_PROMO_MANUAL_END_SESSION As Integer = 33

  Private Const MOV_TYPE_DB_CARD_MANUAL_HANDPAY As Integer = 34
  Private Const MOV_TYPE_DB_CARD_MANUAL_HANDPAY_CANCELLATION As Integer = 35

  Private Const MOV_TYPE_DB_CARD_POINTS_AWARDED As Integer = 36
  Private Const MOV_TYPE_DB_CARD_POINTS_TO_GIFT_REQUEST As Integer = 37
  Private Const MOV_TYPE_DB_CARD_POINTS_TO_NOT_REDEEMABLE As Integer = 38
  Private Const MOV_TYPE_DB_CARD_POINTS_GIFT_DELIVERY As Integer = 39
  Private Const MOV_TYPE_DB_CARD_POINTS_EXPIRED As Integer = 40

  Private Const MOV_TYPE_DB_CARD_POINTS_TO_DRAW_TICKET_PRINT As Integer = 41

  Private Const MOV_TYPE_DB_CARD_POINTS_GIFT_SERVICES As Integer = 42

  Private Const MOV_TYPE_DB_CARD_HOLDER_LEVEL_CHANGED As Integer = 43

  Private Const MOV_TYPE_DB_CARD_PRIZE_COUPON As Integer = 44

  Private Const MOV_TYPE_DB_CARD_REDEEMABLE_SPENT_USED As Integer = 45

  Private Const MOV_TYPE_DB_CARD_POINTS_TO_REDEEMABLE As Integer = 46

  Private Const MOV_TYPE_DB_PRIZE_EXPIRED As Integer = 47

  ' MBF 11-OCT-2011
  Private Const MOV_TYPE_DB_CARD_CREATION As Integer = 48
  Private Const MOV_TYPE_DB_CARD_PERSONALIZATED As Integer = 49

  ' JCA 21-OCT-2011
  'Private Const MOV_TYPE_DB_CARD_ADJUSTMENT As Integer = 50

  ' JMM 22-NOV-2011
  Private Const MOV_TYPE_DB_CARD_PIN_CHANGE As Integer = 51
  Private Const MOV_TYPE_DB_CARD_PIN_RANDOM As Integer = 52

  ' RCI 17-FEB-2012
  Private Const MOV_TYPE_DB_CREDITS_NOT_REDEEMABLE2_EXPIRED As Integer = 53
  Private Const MOV_TYPE_DB_COVER_COUPON As Integer = 54
  Private Const MOV_TYPE_DB_NO_REDEEMABLE2 As Integer = 55 ' For future promotions

  Private Const MOV_TYPE_DB_CANCEL_START_SESSION As Integer = 56

  ' JCM  24-APR-2012
  Private Const MOV_TYPE_DB_CARD_RECYCLED As Integer = 57

  ' DDM  05-JUL-2012
  Private Const MOV_TYPE_DB_PROMOTION_REDEEMABLE As Integer = 58
  Private Const MOV_TYPE_DB_CANCEL_PROMOTION_REDEEMABLE As Integer = 59

  ' DDM  31-JUL-2012
  Private Const MOV_TYPE_DB_PROMOTION_POINT As Integer = 60
  Private Const MOV_TPYE_DB_CANCEL_PROMOTION_POINT As Integer = 61

  ' RRB 29-AUG-2012
  Private Const MOV_TYPE_DB_CARD_MANUAL_HOLDER_LEVEL_CHANGED = 62

  ' JML 30-MAY-2017
  Private Const MOV_TYPE_DB_MANUALLY_ADDED_POINTS_FOR_LEVEL = 68

  ' FOS 25-JUN-2015
  Private Const MOV_TYPE_DB_CARD_REPLACEMENT_CHECK As Integer = 206
  Private Const MOV_TYPE_DB_CARD_REPLACEMENT_CASH_CURRENCY As Integer = 207
  Private Const MOV_TYPE_DB_CARD_REPLACEMENT_CARD_CREDIT As Integer = 208
  Private Const MOV_TYPE_DB_CARD_REPLACEMENT_CARD_DEBIT As Integer = 209
  Private Const MOV_TYPE_DB_CARD_REPLACEMENT_CARD_GENERIC As Integer = 210
  Private Const MOV_TYPE_DB_CARD_CREATION_CHECK As Integer = 211
  Private Const MOV_TYPE_DB_CARD_CREATION_CASH_CURRENCY As Integer = 212
  Private Const MOV_TYPE_DB_CARD_CREATION_CARD_CREDIT As Integer = 213
  Private Const MOV_TYPE_DB_CARD_CREATION_CARD_DEBIT As Integer = 214
  Private Const MOV_TYPE_DB_CARD_CREATION_CARD_GENERIC As Integer = 215

  Private Const FORMAT_ID As String = "00#"

#End Region ' Constants

#Region " Enums "

  ' RRB 27-AUG-2012 
  Public Enum ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE

    TYPE_NORMAL = 0
    TYPE_DETAILS = 1

  End Enum

#End Region ' Enums

#Region " Members "

  ' RRB 27-AUG-2012 Type of account movements shown
  Private m_account_movements_type_mode As ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE

  ' Used to avoid data-changed recursive calls when re-painting a data grid
  Private m_refreshing_grid As Boolean

  ' For report filters 
  Private m_account As String
  Private m_track_data As String
  Private m_holder As String
  Private m_holder_is_vip As String
  Private m_date_from As String
  Private m_date_to As String
  Private m_terminal As String
  Private m_type As String

  ' For calls from other screens
  Private m_initial_account As Long

  Private m_prize_taxes_1_name As String
  Private m_prize_taxes_2_name As String
  Private m_prize_taxes_3_name As String

  Private m_level_name() As String
  Private m_split_a_name_str As String

  ' For grid totalizators
  Dim m_total_points As Double

#End Region ' Members

#Region " OVERRIDES "

  ' PURPOSE: Set a different value for the maximum number of rows that can be showed
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  Protected Overrides Function GUI_MaxRows() As Integer
    Return 10000
  End Function ' GUI_MaxRows

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Public Overrides Sub GUI_SetFormId()
    ' Leave this sub empty, as the FormId is set now in the Constructor of the class.

  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' RRB 27-AUG-2012 Form name
    Select Case Me.m_account_movements_type_mode
      Case ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE.TYPE_NORMAL
        Me.Text = GLB_NLS_GUI_INVOICING.GetString(228) ' Movimientos de cuentas

      Case ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE.TYPE_DETAILS
        Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1257) ' Informe de Asignaciones Manuales

      Case Else
        Me.Text = GLB_NLS_GUI_INVOICING.GetString(228) ' Movimientos de cuentas

    End Select

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(3)

    ' Date
    Me.gb_date.Text = GLB_NLS_GUI_INVOICING.GetString(201)
    Me.dtp_from.Text = GLB_NLS_GUI_INVOICING.GetString(202)
    Me.dtp_to.Text = GLB_NLS_GUI_INVOICING.GetString(203)
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    Me.lbl_trackdata_note.Value = "* " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2862)
    Me.lbl_trackdata_note.Visible = False

    ' Type Account Movement (ManualHolderLevelChanged & CardAdjustment)
    If Me.m_account_movements_type_mode = ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE.TYPE_NORMAL Then
      Me.gb_type.Visible = False
    Else
    End If
    Me.gb_type.Text = GLB_NLS_GUI_INVOICING.GetString(211)
    Me.opt_points_accreditations.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1276)
    Me.opt_level_change.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(545)
    Me.opt_points_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1645)

    'Sites
    Me.uc_account_mov_sites_sel.Init()

    ' Grid
    Call GUI_StyleSheet()

    m_prize_taxes_1_name = GetCashierData("Tax.OnPrize.1.Name")
    m_prize_taxes_2_name = GetCashierData("Tax.OnPrize.2.Name")
    m_prize_taxes_3_name = GetCashierData("Tax.OnPrize.3.Name")

    ' Set filter default values
    Call SetDefaultValues()

    ' Init level name
    ReDim m_level_name(5)
    m_level_name(0) = "---"
    m_level_name(1) = WSI.Common.Misc.ReadGeneralParams("PlayerTracking", "Level01.Name")
    m_level_name(2) = WSI.Common.Misc.ReadGeneralParams("PlayerTracking", "Level02.Name")
    m_level_name(3) = WSI.Common.Misc.ReadGeneralParams("PlayerTracking", "Level03.Name")
    m_level_name(4) = WSI.Common.Misc.ReadGeneralParams("PlayerTracking", "Level04.Name")

    m_split_a_name_str = WSI.Common.Misc.ReadGeneralParams("Cashier", "Split.A.Name")

  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted

  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Date selection 
    If Me.dtp_from.Checked And Me.dtp_to.Checked Then
      If Me.dtp_from.Value > Me.dtp_to.Value Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.dtp_to.Focus()

        Return False
      End If
    End If

    ' Check fields on uc_account_sel are ok
    If Not Me.uc_account_sel1.ValidateFormat() Then
      Return False
    End If

    ' Check Sites
    If Not Me.uc_account_mov_sites_sel.FilterCheckSites() Then
      Return False
    End If

    Return True
  End Function ' GUI_FilterCheck

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database

  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim str_sql As String
    Dim _with_index As String

    _with_index = ""

    If m_account_movements_type_mode = ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE.TYPE_DETAILS Then
      _with_index = " WITH (INDEX(IX_type_date_account)) "
    Else
      If Me.uc_account_sel1.GetFilterSQL() <> "" Then
        _with_index = " WITH (INDEX(IX_movements_account_date)) "
      Else
        _with_index = " WITH (INDEX(IX_am_datetime)) "
      End If
    End If

    ' Get Select and from
    str_sql = "SELECT " & _
                  " AM_SITE_ID," & _
                  " ST_NAME," & _
                  " AM_ACCOUNT_ID, " & _
                  " AC_TYPE, " & _
                  " AC_TRACK_DATA, " & _
                  " AC_HOLDER_NAME, " & _
                  " AM_DATETIME, " & _
                  " CASE WHEN AM_TERMINAL_ID IS NOT NULL THEN AM_TERMINAL_ID " & _
                       " WHEN AM_CASHIER_ID IS NOT NULL THEN AM_CASHIER_ID " & _
                       " ELSE NULL " & _
                  " END, " & _
                  " CASE WHEN AM_TERMINAL_ID IS NOT NULL THEN AM_TERMINAL_NAME" & _
                       " WHEN AM_CASHIER_ID IS NOT NULL THEN AM_CASHIER_NAME" & _
                       " WHEN AM_TERMINAL_ID IS NULL AND AM_CASHIER_ID IS NULL THEN AM_CASHIER_NAME" & _
                       " ELSE NULL " & _
                  " END, " & _
                  " AM_TYPE, " & _
                  " AM_INITIAL_BALANCE, " & _
                  " AM_SUB_AMOUNT, " & _
                  " AM_ADD_AMOUNT, " & _
                  " AM_FINAL_BALANCE, " & _
                  " AM_TRACK_DATA, "

    ' RRB 27-AUG-2012
    If m_account_movements_type_mode = ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE.TYPE_DETAILS Then
      str_sql = str_sql & " AM_DETAILS, " & _
                          " AM_REASONS "
    Else
      str_sql = str_sql & " AM_DETAILS "
    End If

    str_sql = str_sql & " FROM ACCOUNT_MOVEMENTS " & _with_index & _
                        " INNER JOIN ACCOUNTS ON AC_ACCOUNT_ID = AM_ACCOUNT_ID "
    str_sql = str_sql & " LEFT OUTER JOIN SITES ON ST_SITE_ID = AM_SITE_ID "

    str_sql = str_sql & GetSqlWhere()
    str_sql = str_sql & " ORDER BY AM_DATETIME DESC, AM_MOVEMENT_ID DESC "

    Return str_sql

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_BeforeFirstRow()
    Me.m_total_points = 0

    Call GUI_StyleSheet()

  End Sub ' GUI_BeforeFirstRow

  ' PURPOSE: Print totalizator row in the data grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  Protected Overrides Sub GUI_AfterLastRow()

    If Me.m_account_movements_type_mode = ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE.TYPE_DETAILS _
       And Me.opt_points_accreditations.Checked _
       And Me.Grid.NumRows > 0 Then

      Dim idx_row As Integer = Me.Grid.NumRows

      Me.Grid.AddRow()

      ' Label - TOTAL:
      Me.Grid.Cell(idx_row, GRID_COLUMN_ACCT_NUMBER).Value = GLB_NLS_GUI_INVOICING.GetString(205)

      ' Total - Points
      Me.Grid.Cell(idx_row, GRID_COLUMN_ADD_AMOUNT).Value = GUI_FormatNumber(Me.m_total_points)

      ' Color Row
      Me.Grid.Row(idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
    End If

  End Sub ' GUI_AfterLastRow

  ' PURPOSE : Sets the values of a row in the data grid
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the row could be added
  '     - False: the row could not be added

  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim mov_type As Integer
    Dim str_type As String = ""
    Dim _track_data As String
    Dim _currency_format As Boolean
    Dim _blank_row As Boolean
    Dim _blank_balance As Boolean

    ' Site ID  Case Site Id = 0, then show "Center"
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SITE_ID).Value = IIf(DbRow.IsNull(SQL_COLUMN_SITE_ID), "", IIf(DbRow.Value(SQL_COLUMN_SITE_ID) = 0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1791), Format(DbRow.Value(SQL_COLUMN_SITE_ID), FORMAT_ID)))

    ' Site Name
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SITE_NAME).Value = IIf(DbRow.IsNull(SQL_COLUMN_SITE_NAME), "", DbRow.Value(SQL_COLUMN_SITE_NAME))

    ' Account type
    Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_TYPE).Value = DbRow.Value(SQL_COLUMN_ACCOUNT_TYPE)

    ' Card Track Data
    _track_data = ""
    ' WIN accounts must follow encryption formats
    If Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_TYPE).Value = ACCOUNT_TYPE_WIN Then
      If Not DbRow.IsNull(SQL_COLUMN_AM_TRACKDATA) AndAlso Not String.IsNullOrEmpty(DbRow.Value(SQL_COLUMN_AM_TRACKDATA)) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_CARD_TRACK).Value = DbRow.Value(SQL_COLUMN_AM_TRACKDATA)
      Else
        If Not DbRow.IsNull(SQL_COLUMN_CARD_TRACK) Then
          If WSI.Common.CardNumber.TrackDataToExternal(_track_data, DbRow.Value(SQL_COLUMN_CARD_TRACK), MAGNETIC_CARD_TYPES.CARD_TYPE_PLAYER) Then
            Me.Grid.Cell(RowIndex, GRID_COLUMN_CARD_TRACK).Value = _track_data & "*"
            Me.lbl_trackdata_note.Visible = True
          Else
            Me.Grid.Cell(RowIndex, GRID_COLUMN_CARD_TRACK).Value = "* * * * * * * *"
          End If
        Else
          Me.Grid.Cell(RowIndex, GRID_COLUMN_CARD_TRACK).Value = ""
        End If
      End If
    Else
      If Not DbRow.IsNull(SQL_COLUMN_CARD_TRACK) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_CARD_TRACK).Value = DbRow.Value(SQL_COLUMN_CARD_TRACK)
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_CARD_TRACK).Value = ""
      End If
    End If

    ' Card Account Number
    Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCT_NUMBER).Value = DbRow.Value(SQL_COLUMN_ACCT_NUMBER)

    ' Card Holder Name
    If Not DbRow.IsNull(SQL_COLUMN_HOLDER_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_NAME).Value = DbRow.Value(SQL_COLUMN_HOLDER_NAME)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_NAME).Value = ""
    End If

    ' Date
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_DATE), _
                                                                                ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                                ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    ' Movement type
    mov_type = DbRow.Value(SQL_COLUMN_TYPE_ID)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE_ID).Value = DbRow.Value(SQL_COLUMN_TYPE_ID)

    _currency_format = True
    _blank_row = False
    _blank_balance = False

    str_type = TranslateMovementType(mov_type, _currency_format, _blank_row, _blank_balance)

    ' Movement type names
    Select Case mov_type
      Case MOV_TYPE_DB_PLAY
        str_type = GLB_NLS_GUI_INVOICING.GetString(41)

      Case MOV_TYPE_DB_CASH_IN
        str_type = m_split_a_name_str

      Case MOV_TYPE_DB_CASH_OUT
        str_type = GLB_NLS_GUI_INVOICING.GetString(43)

      Case MOV_TYPE_DB_DEVOLUTION
        'str_type = GLB_NLS_GUI_INVOICING.GetString(489)        
        str_type = GLB_NLS_GUI_INVOICING.GetString(44)

      Case MOV_TYPE_DB_FEDERAL_TAXES
        str_type = m_prize_taxes_1_name

      Case MOV_TYPE_DB_STATE_TAXES
        str_type = m_prize_taxes_2_name

      Case MOV_TYPE_DB_START_SESSION
        str_type = GLB_NLS_GUI_INVOICING.GetString(46)

      Case MOV_TYPE_DB_CANCEL_START_SESSION
        str_type = GLB_NLS_GUI_INVOICING.GetString(96)

      Case MOV_TYPE_DB_END_SESSION
        str_type = GLB_NLS_GUI_INVOICING.GetString(47)

      Case MOV_TYPE_DB_ACTIVATE
        str_type = GLB_NLS_GUI_INVOICING.GetString(48)

      Case MOV_TYPE_DB_DEACTIVATE
        str_type = GLB_NLS_GUI_INVOICING.GetString(49)

      Case MOV_TYPE_DB_NO_REDEEMABLE
        If DbRow.IsNull(SQL_COLUMN_DETAILS) Then
          str_type = GLB_NLS_GUI_INVOICING.GetString(50)
        Else
          str_type = GLB_NLS_GUI_INVOICING.GetString(50) + " (" + DbRow.Value(SQL_COLUMN_DETAILS) + ")"
        End If

      Case MOV_TYPE_DB_MANUALLY_ADDED_POINTS_FOR_LEVEL        '= 68, Points added manually
        str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1860)

      Case MOV_TYPE_DB_COVER_COUPON
        str_type = GLB_NLS_GUI_INVOICING.GetString(38)

      Case MOV_TYPE_DB_CARD_NO_REDEEMABLE_CANCEL
        str_type = GLB_NLS_GUI_INVOICING.GetString(53)

      Case MOV_TYPE_DB_CARD_DEPOSIT_IN
        str_type = GLB_NLS_GUI_INVOICING.GetString(51)

      Case MOV_TYPE_DB_CARD_DEPOSIT_OUT
        str_type = GLB_NLS_GUI_INVOICING.GetString(52)

      Case MOV_TYPE_DB_CARD_REPLACEMENT
        str_type = GLB_NLS_GUI_INVOICING.GetString(70)

      Case MOV_TYPE_DB_CARD_REPLACEMENT_CHECK
        str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(206)

      Case MOV_TYPE_DB_CARD_REPLACEMENT_CASH_CURRENCY
        str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(207)

      Case MOV_TYPE_DB_CARD_REPLACEMENT_CARD_CREDIT
        str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(208)

      Case MOV_TYPE_DB_CARD_REPLACEMENT_CARD_DEBIT
        str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(209)

      Case MOV_TYPE_DB_CARD_REPLACEMENT_CARD_GENERIC
        str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(210)

      Case MOV_TYPE_DB_PROMO_CREDITS
        str_type = GLB_NLS_GUI_INVOICING.GetString(64)

      Case MOV_TYPE_DB_PROMO_TO_REDEEMABLE
        str_type = GLB_NLS_GUI_INVOICING.GetString(65)

      Case MOV_TYPE_DB_PROMO_EXPIRED
        str_type = GLB_NLS_GUI_INVOICING.GetString(66)

      Case MOV_TYPE_DB_PROMO_CANCEL
        str_type = GLB_NLS_GUI_INVOICING.GetString(67)

      Case MOV_TYPE_DB_PROMO_START_SESSION
        str_type = GLB_NLS_GUI_INVOICING.GetString(68)

      Case MOV_TYPE_DB_PROMO_END_SESSION
        str_type = GLB_NLS_GUI_INVOICING.GetString(69)

      Case MOV_TYPE_DB_CREDITS_EXPIRED
        str_type = GLB_NLS_GUI_INVOICING.GetString(477)

      Case MOV_TYPE_DB_PRIZE_EXPIRED
        str_type = GLB_NLS_GUI_INVOICING.GetString(89)

      Case MOV_TYPE_DB_CREDITS_NOT_REDEEMABLE_EXPIRED
        str_type = GLB_NLS_GUI_INVOICING.GetString(478)

      Case MOV_TYPE_DB_CREDITS_NOT_REDEEMABLE2_EXPIRED
        str_type = GLB_NLS_GUI_INVOICING.GetString(95)

      Case MOV_TYPE_DB_CARD_DRAW_TICKET_PRINT
        str_type = GLB_NLS_GUI_INVOICING.GetString(71)

      Case MOV_TYPE_DB_CARD_POINTS_TO_DRAW_TICKET_PRINT
        str_type = GLB_NLS_GUI_INVOICING.GetString(83)

      Case MOV_TYPE_DB_CARD_HANDPAY
        str_type = GLB_NLS_GUI_CONFIGURATION.GetString(364)

      Case MOV_TYPE_DB_CARD_HANDPAY_CANCELLATION
        str_type = GLB_NLS_GUI_CONFIGURATION.GetString(365)

      Case MOV_TYPE_DB_CARD_MANUAL_HANDPAY
        str_type = GLB_NLS_GUI_CONFIGURATION.GetString(366)

      Case MOV_TYPE_DB_CARD_MANUAL_HANDPAY_CANCELLATION
        str_type = GLB_NLS_GUI_CONFIGURATION.GetString(367)

      Case MOV_TYPE_DB_MANUAL_END_SESSION
        str_type = GLB_NLS_GUI_INVOICING.GetString(74)

      Case MOV_TYPE_DB_PROMO_MANUAL_END_SESSION
        str_type = GLB_NLS_GUI_INVOICING.GetString(75)

      Case MOV_TYPE_DB_CARD_POINTS_AWARDED
        str_type = GLB_NLS_GUI_INVOICING.GetString(76)
        _currency_format = False

      Case MOV_TYPE_DB_CARD_POINTS_TO_GIFT_REQUEST
        str_type = GLB_NLS_GUI_INVOICING.GetString(77)
        _currency_format = False

      Case MOV_TYPE_DB_CARD_POINTS_TO_NOT_REDEEMABLE
        str_type = GLB_NLS_GUI_INVOICING.GetString(78)

      Case MOV_TYPE_DB_CARD_POINTS_TO_REDEEMABLE
        str_type = GLB_NLS_GUI_INVOICING.GetString(88)

      Case MOV_TYPE_DB_CARD_POINTS_GIFT_DELIVERY
        str_type = GLB_NLS_GUI_INVOICING.GetString(79)
        _currency_format = False

      Case MOV_TYPE_DB_CARD_POINTS_GIFT_SERVICES
        str_type = GLB_NLS_GUI_INVOICING.GetString(84)
        _currency_format = False

      Case MOV_TYPE_DB_CARD_POINTS_EXPIRED
        str_type = GLB_NLS_GUI_INVOICING.GetString(81)
        _currency_format = False

      Case MOV_TYPE_DB_CARD_HOLDER_LEVEL_CHANGED, _
           MOV_TYPE_DB_CARD_MANUAL_HOLDER_LEVEL_CHANGED
        Dim _new_level As String
        Dim _old_level As String

        _old_level = m_level_name(DbRow.Value(SQL_COLUMN_SUB_AMOUNT))
        _new_level = m_level_name(DbRow.Value(SQL_COLUMN_ADD_AMOUNT))

        str_type = IIf(mov_type = MOV_TYPE_DB_CARD_HOLDER_LEVEL_CHANGED, _
                       GLB_NLS_GUI_INVOICING.GetString(85, _new_level, _old_level), _
                       GLB_NLS_GUI_PLAYER_TRACKING.GetString(1123, _new_level, _old_level))

        _blank_row = True
        _blank_balance = True

      Case MOV_TYPE_DB_CARD_PRIZE_COUPON
        str_type = GLB_NLS_GUI_INVOICING.GetString(86)

      Case MOV_TYPE_DB_CARD_CREATION
        str_type = GLB_NLS_GUI_INVOICING.GetString(90)

      Case MOV_TYPE_DB_CARD_CREATION_CHECK
        str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(211)

      Case MOV_TYPE_DB_CARD_CREATION_CASH_CURRENCY
        str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(212)

      Case MOV_TYPE_DB_CARD_CREATION_CARD_CREDIT
        str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(213)

      Case MOV_TYPE_DB_CARD_CREATION_CARD_DEBIT
        str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(214)

      Case MOV_TYPE_DB_CARD_CREATION_CARD_GENERIC
        str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(215)

      Case MOV_TYPE_DB_CARD_PERSONALIZATED
        str_type = GLB_NLS_GUI_INVOICING.GetString(91)

      Case MOV_TYPE_DB_CARD_REDEEMABLE_SPENT_USED
        str_type = GLB_NLS_GUI_INVOICING.GetString(87)

        ' JCA 21-OCT-2011 
      Case WSI.Common.MovementType.ManuallyAddedPointsOnlyForRedeem
        Dim _old_points As String
        Dim _new_points As String
        Dim _add_points As String

        _old_points = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_INIT_BALANCE))
        _add_points = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_ADD_AMOUNT))
        _new_points = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_FINAL_BALANCE))

        If DbRow.Value(SQL_COLUMN_SUB_AMOUNT) = 0 Then
          str_type = IIf(m_account_movements_type_mode = ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE.TYPE_NORMAL, _
                         GLB_NLS_GUI_INVOICING.GetString(92), _
                         GLB_NLS_GUI_PLAYER_TRACKING.GetString(1125, _add_points, _new_points, _old_points))
        Else
          str_type = IIf(m_account_movements_type_mode = ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE.TYPE_NORMAL, _
                         GLB_NLS_GUI_INVOICING.GetString(92), _
                         GLB_NLS_GUI_PLAYER_TRACKING.GetString(1124, _new_points, _old_points))
        End If

        _currency_format = False

        ' JMM 22-NOV-2011
      Case MOV_TYPE_DB_CARD_PIN_CHANGE
        str_type = GLB_NLS_GUI_INVOICING.GetString(93)

      Case MOV_TYPE_DB_CARD_PIN_RANDOM
        str_type = GLB_NLS_GUI_INVOICING.GetString(94)

        ' JCM 24-APR-2012 Card Recycle Movement
      Case MOV_TYPE_DB_CARD_RECYCLED
        str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(654)

        ' DDM 05-JUL-2012
      Case MOV_TYPE_DB_PROMOTION_REDEEMABLE
        str_type = GLB_NLS_GUI_INVOICING.GetString(100)
        If Not DbRow.IsNull(SQL_COLUMN_DETAILS) AndAlso Not String.IsNullOrEmpty(DbRow.Value(SQL_COLUMN_DETAILS)) Then
          str_type &= " (" + DbRow.Value(SQL_COLUMN_DETAILS) + ")"
        End If

      Case MOV_TYPE_DB_CANCEL_PROMOTION_REDEEMABLE
        str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1120)

      Case MOV_TYPE_DB_PROMOTION_POINT
        str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1121)
        If Not DbRow.IsNull(SQL_COLUMN_DETAILS) AndAlso Not String.IsNullOrEmpty(DbRow.Value(SQL_COLUMN_DETAILS)) Then
          str_type &= " (" + DbRow.Value(SQL_COLUMN_DETAILS) + ")"
        End If
        _currency_format = False

      Case MOV_TPYE_DB_CANCEL_PROMOTION_POINT
        str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1122)
        _currency_format = False


      Case WSI.Common.MovementType.TaxReturningOnPrizeCoupon
        str_type = GLB_NLS_GUI_INVOICING.GetString(483)

        ' RRB 13-SEP-2012
      Case WSI.Common.MovementType.DecimalRounding
        str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1126)

      Case WSI.Common.MovementType.ServiceCharge
        str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1127)

      Case WSI.Common.MovementType.CancelGiftInstance
        str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1129)
        _currency_format = False

      Case WSI.Common.MovementType.PointsStatusChanged
        Dim _new_points_status As WSI.Common.ACCOUNT_POINTS_STATUS = DbRow.Value(SQL_COLUMN_ADD_AMOUNT)
        Dim _old_points_status As WSI.Common.ACCOUNT_POINTS_STATUS = DbRow.Value(SQL_COLUMN_SUB_AMOUNT)
        Dim _new_pt_status As String = ""
        Dim _old_pt_status As String = ""

        Select Case _new_points_status
          Case WSI.Common.ACCOUNT_POINTS_STATUS.REDEEM_ALLOWED
            _new_pt_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1643)
          Case WSI.Common.ACCOUNT_POINTS_STATUS.REDEEM_NOT_ALLOWED
            _new_pt_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1644)
        End Select

        Select Case _old_points_status
          Case WSI.Common.ACCOUNT_POINTS_STATUS.REDEEM_ALLOWED
            _old_pt_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1643)
          Case WSI.Common.ACCOUNT_POINTS_STATUS.REDEEM_NOT_ALLOWED
            _old_pt_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1644)
        End Select

        str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1646, _new_pt_status, _old_pt_status)

        _blank_row = True
        _currency_format = False

      Case WSI.Common.MovementType.ManuallyAddedPointsForLevel  '= 68, Points added manually  
        str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1860)
        _currency_format = False

      Case WSI.Common.MovementType.ImportedAccount              '= 70, Imported(account) 
        str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1150)

      Case WSI.Common.MovementType.ImportedPointsOnlyForRedeem  '= 71, Imported points (not accounted for the level) 
        str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1861)
        _currency_format = False

      Case WSI.Common.MovementType.ImportedPointsForLevel       '= 72, Imported(points) 
        str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1862)
        _currency_format = False

      Case WSI.Common.MovementType.ImportedPointsHistory        '= 73, History points imported 
        str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1863)
        _currency_format = False

      Case WSI.Common.MovementType.AccountBlocked               '= 75, Account blocked
        str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1133)
        _currency_format = False

      Case WSI.Common.MovementType.AccountUnblocked             '= 76, Account unblocked
        str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1134)
        _currency_format = False

      Case WSI.Common.MovementType.AccountInBlacklist           '= 240, Account added in blacklist
        str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7452)
        _currency_format = False

      Case WSI.Common.MovementType.AccountNotInBlacklist        '= 241, Account removed from blacklist
        str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7453)
        _currency_format = False

      Case WSI.Common.MovementType.MultiSiteCurrentLocalPoints  '= 101, Multisite - Current points in site, similar ManuallyAddedPointsOnlyForRedeem 
        str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1864)
        _currency_format = False

      Case WSI.Common.MovementType.CashdeskDrawParticipation    '= 200, 
        _blank_balance = True

    End Select

    Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE_NAME).Value = str_type

    ' Allowed combinations:
    '
    ' Machine Id      NOT NULL            NULL          NULL
    ' Machine Name    NOT NULL            NOT NULL      NULL
    ' ------------    ----------------    ---------     ----------
    ' What is it?     Terminal/Cashier    GUI           No Machine

    ' Machine Id
    If Not DbRow.IsNull(SQL_COLUMN_MACHINE_NAME) Then
      If Not DbRow.IsNull(SQL_COLUMN_MACHINE_ID) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_ID).Value = DbRow.Value(SQL_COLUMN_MACHINE_ID)
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_ID).Value = ""
      End If
    Else
    End If

    ' Terminal Name
    If Not DbRow.IsNull(SQL_COLUMN_MACHINE_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_NAME).Value = DbRow.Value(SQL_COLUMN_MACHINE_NAME)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_NAME).Value = ""
    End If

    ' Initial balance
    If Not _blank_balance Then
      If _currency_format Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_INIT_BALANCE).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_INIT_BALANCE))
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_INIT_BALANCE).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_INIT_BALANCE))
      End If
    End If

    ' Subtract amount
    If Not DbRow.IsNull(SQL_COLUMN_SUB_AMOUNT) And Not _blank_row Then
      If _currency_format Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_SUB_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_SUB_AMOUNT))
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_SUB_AMOUNT).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_SUB_AMOUNT))
      End If
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SUB_AMOUNT).Value = ""
    End If

    ' Add amount
    If Not DbRow.IsNull(SQL_COLUMN_ADD_AMOUNT) And Not _blank_row Then
      If _currency_format Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_ADD_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_ADD_AMOUNT))
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_ADD_AMOUNT).Value = IIf(Me.m_account_movements_type_mode = ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE.TYPE_NORMAL, _
                                                                   GUI_FormatNumber(DbRow.Value(SQL_COLUMN_ADD_AMOUNT)), _
                                                                   GUI_FormatNumber(GUI_FormatNumber(DbRow.Value(SQL_COLUMN_ADD_AMOUNT)) - GUI_FormatNumber(DbRow.Value(SQL_COLUMN_SUB_AMOUNT))))
        Me.m_total_points += GUI_FormatNumber(GUI_FormatNumber(DbRow.Value(SQL_COLUMN_ADD_AMOUNT)) - GUI_FormatNumber(DbRow.Value(SQL_COLUMN_SUB_AMOUNT)))
      End If
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ADD_AMOUNT).Value = ""
    End If

    ' Final balance
    If Not _blank_balance Then
      If _currency_format Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_FINAL_BALANCE).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_FINAL_BALANCE))
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_FINAL_BALANCE).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_FINAL_BALANCE))
      End If
    End If

    ' RRB 27-AUG-2012
    If m_account_movements_type_mode = ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE.TYPE_DETAILS Then
      ' Reasons
      If Not DbRow.IsNull(SQL_COLUMN_REASONS) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_REASONS).Value = DbRow.Value(SQL_COLUMN_REASONS)
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_REASONS).Value = ""
      End If
      ' Details
      If Not DbRow.IsNull(SQL_COLUMN_DETAILS) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAILS).Value = DbRow.Value(SQL_COLUMN_DETAILS)
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAILS).Value = ""
      End If
    End If



    Return True

  End Function ' GUI_SetupRow

  ' PURPOSE: Set focus to a control when first entering the form
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.uc_account_sel1
  End Sub ' GUI_SetInitialFocus


#Region " GUI Reports "

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(230), m_account)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(212), m_track_data)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(235), m_holder)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4802), m_holder_is_vip)

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(202), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(203), m_date_to)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1927), uc_account_mov_sites_sel.GetSitesIdListSelectedToPrint()) '' SiteID


    If Me.m_account_movements_type_mode = ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE.TYPE_NORMAL Then
    Else
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(211), m_type)
    End If

    If lbl_trackdata_note.Visible Then
      PrintData.SetFilter("*", GLB_NLS_GUI_PLAYER_TRACKING.GetString(2862))
    Else
      PrintData.SetFilter("", "", True)
    End If

    ' Allow enough space for terminal type labels in the report
    PrintData.FilterHeaderWidth(3) = 2000
  End Sub ' GUI_ReportFilter


  ' PURPOSE: Get report parameters and headers
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Protected Overrides Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA, Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(ExcelData)

    ' Set specific column formats.
    ExcelData.SetColumnFormat(EXCEL_COLUMN_SITE_ID, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT)

  End Sub

  ' PURPOSE: Set form specific requirements/parameters forthe report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '           - FirstColIndex
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    ' Prevent date/time column from being squeezed too much and drop the time part to the following line
    If m_account_movements_type_mode = ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE.TYPE_DETAILS Then
      PrintData.Settings.Columns(GRID_COLUMN_DATE).IsWidthFixed = True
      PrintData.Settings.Columns(GRID_COLUMN_CARD_TRACK).IsWidthFixed = True

      With Me.Grid
        .Column(GRID_COLUMN_TYPE_NAME).PrintWidth = 3000
        .Column(GRID_COLUMN_REASONS).PrintWidth = 2000
        .Column(GRID_COLUMN_DETAILS).PrintWidth = 2000
      End With

    Else
      PrintData.Settings.Columns(GRID_COLUMN_DATE).IsWidthFixed = True
      PrintData.Settings.Columns(GRID_COLUMN_CARD_TRACK).IsWidthFixed = True
    End If

    Call MyBase.GUI_ReportParams(PrintData)
    PrintData.Params.Title = IIf(m_account_movements_type_mode = ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE.TYPE_NORMAL, GLB_NLS_GUI_INVOICING.GetString(228), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1257))

    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_LANDSCAPE



  End Sub ' GUI_ReportParams

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_account = ""
    m_track_data = ""
    m_holder = ""
    m_holder_is_vip = ""
    m_date_from = ""
    m_date_to = ""
    m_terminal = ""

    m_account = Me.uc_account_sel1.Account()
    m_track_data = Me.uc_account_sel1.TrackData()
    m_holder = Me.uc_account_sel1.Holder()
    m_holder_is_vip = Me.uc_account_sel1.HolderIsVip()

    Me.lbl_trackdata_note.Visible = False

    'Date 
    If Me.dtp_from.Checked Then
      m_date_from = GUI_FormatDate(dtp_from.Value, _
                                   ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    If Me.dtp_to.Checked Then
      m_date_to = GUI_FormatDate(dtp_to.Value, _
                                 ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    ' Type Account Movement
    If m_account_movements_type_mode = ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE.TYPE_DETAILS Then
      If Me.opt_points_accreditations.Checked Then
        m_type = Me.opt_points_accreditations.Text
      ElseIf Me.opt_level_change.Checked Then
        m_type = Me.opt_level_change.Text
      Else
        m_type = Me.opt_points_status.Text
      End If
    End If
  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region ' Overrides

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    m_initial_account = 0
    Me.Display(False)

  End Sub ' ShowForEdit

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window, ByVal account_id As Int64)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING

    Me.m_initial_account = account_id
    uc_account_sel1.Account = m_initial_account

    Me.MdiParent = MdiParent
    Me.Display(False)

    ' For Show window before filter apply 
    ' RRB 28-AUG-2012 Not search if don't have permissions - coming from card summary (Avoid popup: Not found)
    If Me.Permissions.Read Then
      Call Application.DoEvents()
      System.Threading.Thread.Sleep(100)
      Call Application.DoEvents()

      Me.GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)
    End If

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub GUI_StyleSheet()
    With Me.Grid
      If m_account_movements_type_mode = ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE.TYPE_DETAILS Then
        GRID_COLUMN_TERMINAL_ID = 13
        GRID_COLUMN_ADD_AMOUNT = 9
        Call .Init(GRID_COLUMNS_DETAILS, GRID_HEADER_ROWS)
      Else
        GRID_COLUMN_TERMINAL_ID = 9
        GRID_COLUMN_ADD_AMOUNT = 13
        Call .Init(GRID_COLUMNS_NORMAL, GRID_HEADER_ROWS)
      End If

      ' Site number
      .Column(GRID_COLUMN_SITE_ID).Header(0).Text = ""
      .Column(GRID_COLUMN_SITE_ID).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1927) ' SiteId
      .Column(GRID_COLUMN_SITE_ID).Width = 850
      .Column(GRID_COLUMN_SITE_ID).PrintWidth = 850
      .Column(GRID_COLUMN_SITE_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Site Name
      .Column(GRID_COLUMN_SITE_NAME).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(447)
      .Column(GRID_COLUMN_SITE_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1718)
      .Column(GRID_COLUMN_SITE_NAME).Width = 0
      .Column(GRID_COLUMN_SITE_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Account number
      .Column(GRID_COLUMN_ACCT_NUMBER).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_ACCT_NUMBER).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(231)
      .Column(GRID_COLUMN_ACCT_NUMBER).Width = 1150
      .Column(GRID_COLUMN_ACCT_NUMBER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Account type
      .Column(GRID_COLUMN_ACCOUNT_TYPE).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_ACCOUNT_TYPE).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(231)
      .Column(GRID_COLUMN_ACCOUNT_TYPE).Width = 0

      ' Card Track
      .Column(GRID_COLUMN_CARD_TRACK).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_CARD_TRACK).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(212)
      .Column(GRID_COLUMN_CARD_TRACK).Width = GRID_WIDTH_CARD_TRACK_DATA
      .Column(GRID_COLUMN_CARD_TRACK).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Holder Name
      .Column(GRID_COLUMN_HOLDER_NAME).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_HOLDER_NAME).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(235)
      .Column(GRID_COLUMN_HOLDER_NAME).Width = GRID_WIDTH_CARD_HOLDER_NAME
      .Column(GRID_COLUMN_HOLDER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Date
      .Column(GRID_COLUMN_DATE).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(236)
      .Column(GRID_COLUMN_DATE).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(201)
      .Column(GRID_COLUMN_DATE).Width = GRID_WIDTH_DATE
      .Column(GRID_COLUMN_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Type Id
      .Column(GRID_COLUMN_TYPE_ID).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(236)
      .Column(GRID_COLUMN_TYPE_ID).Header(1).Text = ""
      .Column(GRID_COLUMN_TYPE_ID).Width = 0
      .Column(GRID_COLUMN_TYPE_ID).IsColumnPrintable = False

      ' Operation Type Name
      .Column(GRID_COLUMN_TYPE_NAME).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(236)
      .Column(GRID_COLUMN_TYPE_NAME).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(211)
      .Column(GRID_COLUMN_TYPE_NAME).Width = IIf(m_account_movements_type_mode = ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE.TYPE_NORMAL, 2200, 4800)
      .Column(GRID_COLUMN_TYPE_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Terminal Id
      .Column(GRID_COLUMN_TERMINAL_ID).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(236)
      .Column(GRID_COLUMN_TERMINAL_ID).Header(1).Text = ""
      .Column(GRID_COLUMN_TERMINAL_ID).Width = 0
      .Column(GRID_COLUMN_TERMINAL_ID).IsColumnPrintable = False

      ' Terminal Name
      .Column(GRID_COLUMN_TERMINAL_NAME).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(236)
      .Column(GRID_COLUMN_TERMINAL_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1270)
      .Column(GRID_COLUMN_TERMINAL_NAME).Width = 2600
      .Column(GRID_COLUMN_TERMINAL_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Initial balance
      .Column(GRID_COLUMN_INIT_BALANCE).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(236)
      .Column(GRID_COLUMN_INIT_BALANCE).Header(1).Text = IIf(m_account_movements_type_mode = ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE.TYPE_NORMAL, GLB_NLS_GUI_INVOICING.GetString(213), "")
      .Column(GRID_COLUMN_INIT_BALANCE).Width = IIf(m_account_movements_type_mode = ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE.TYPE_NORMAL, GRID_WIDTH_AMOUNT_LONG, 0)
      .Column(GRID_COLUMN_INIT_BALANCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Sub amount
      .Column(GRID_COLUMN_SUB_AMOUNT).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(236)
      .Column(GRID_COLUMN_SUB_AMOUNT).Header(1).Text = IIf(m_account_movements_type_mode = ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE.TYPE_NORMAL, GLB_NLS_GUI_INVOICING.GetString(214), "")
      .Column(GRID_COLUMN_SUB_AMOUNT).Width = IIf(m_account_movements_type_mode = ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE.TYPE_NORMAL, GRID_WIDTH_AMOUNT_SHORT, 0)
      .Column(GRID_COLUMN_SUB_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Add amount
      ' RRB
      .Column(GRID_COLUMN_ADD_AMOUNT).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(236)
      If Me.m_account_movements_type_mode = ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE.TYPE_NORMAL Then
        .Column(GRID_COLUMN_ADD_AMOUNT).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(215)
        .Column(GRID_COLUMN_ADD_AMOUNT).Width = GRID_WIDTH_AMOUNT_SHORT
        .Column(GRID_COLUMN_ADD_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      Else
        If Me.opt_points_accreditations.Checked Then
          .Column(GRID_COLUMN_ADD_AMOUNT).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(305)
          .Column(GRID_COLUMN_ADD_AMOUNT).Width = GRID_WIDTH_AMOUNT_SHORT
          .Column(GRID_COLUMN_ADD_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
        Else
          .Column(GRID_COLUMN_ADD_AMOUNT).Header(1).Text = ""
          .Column(GRID_COLUMN_ADD_AMOUNT).Width = 0
          .Column(GRID_COLUMN_ADD_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
        End If
      End If

      ' Final balance
      .Column(GRID_COLUMN_FINAL_BALANCE).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(236)
      .Column(GRID_COLUMN_FINAL_BALANCE).Header(1).Text = IIf(m_account_movements_type_mode = ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE.TYPE_NORMAL, GLB_NLS_GUI_INVOICING.GetString(216), "")
      .Column(GRID_COLUMN_FINAL_BALANCE).Width = IIf(m_account_movements_type_mode = ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE.TYPE_NORMAL, GRID_WIDTH_AMOUNT_LONG, 0)
      .Column(GRID_COLUMN_FINAL_BALANCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' RRB 27-AUG-2012
      If m_account_movements_type_mode = ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE.TYPE_DETAILS Then
        ' Reasons
        .Column(GRID_COLUMN_REASONS).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(236)
        .Column(GRID_COLUMN_REASONS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1262)
        .Column(GRID_COLUMN_REASONS).Width = GRID_WIDTH_REASONS
        .Column(GRID_COLUMN_REASONS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

        ' Details
        .Column(GRID_COLUMN_DETAILS).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(236)
        .Column(GRID_COLUMN_DETAILS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1263)
        .Column(GRID_COLUMN_DETAILS).Width = GRID_WIDTH_DETAILS
        .Column(GRID_COLUMN_DETAILS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      End If

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  Private Sub SetDefaultValues()

    Dim _closing_time As Integer
    Dim _initial_time As Date
    Dim _now As Date

    _now = WSI.Common.WGDB.Now

    Me.uc_account_sel1.Clear()

    If m_initial_account <> 0 Then
      Me.uc_account_sel1.Account = m_initial_account
    End If

    _closing_time = GetDefaultClosingTime()
    _initial_time = New DateTime(_now.Year, _now.Month, _now.Day, _closing_time, 0, 0)
    If _initial_time > _now Then
      _initial_time = _initial_time.AddDays(-1)
    End If

    Me.dtp_from.Value = _initial_time
    Me.dtp_from.Checked = True

    Me.dtp_to.Value = _initial_time.AddDays(1)
    Me.dtp_to.Checked = False

    Me.uc_account_mov_sites_sel.SetDefaultValues()

    If Me.m_account_movements_type_mode = ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE.TYPE_NORMAL Then
    Else
      Me.opt_points_accreditations.Checked = True
      Me.opt_level_change.Checked = False
    End If

  End Sub ' SetDefaultValues

  ' PURPOSE: Build the variable part of the WHERE clause (the one that depends on filter values) for the
  '          main SQL Query.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  Private Function GetSqlWhere() As String

    Dim str_where As String = ""
    Dim str_where_account As String = ""
    Dim _str_where_site As String

    _str_where_site = ""

    ' Filter Dates
    If Me.dtp_from.Checked = True Then
      str_where = IIf(String.IsNullOrEmpty(str_where), " ", " AND ") & " (AM_DATETIME >= " & GUI_FormatDateDB(dtp_from.Value) & ") "
    End If

    If Me.dtp_to.Checked = True Then
      str_where += IIf(String.IsNullOrEmpty(str_where), " ", " AND ") & " (AM_DATETIME < " & GUI_FormatDateDB(dtp_to.Value) & ") "
    End If

    str_where_account = Me.uc_account_sel1.GetFilterSQL()

    If str_where_account <> "" Then
      str_where += IIf(String.IsNullOrEmpty(str_where), " ", " AND ") & str_where_account
    End If

    ' RRB 27-AUG-2012 Filter Movement type (Change level or Add/fix points)
    If m_account_movements_type_mode = ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE.TYPE_DETAILS Then
      If Me.opt_points_accreditations.Checked Then
        str_where = IIf(String.IsNullOrEmpty(str_where), " ", " AND ") & " (AM_TYPE IN ( " & WSI.Common.MovementType.ManuallyAddedPointsOnlyForRedeem & " )) "
      ElseIf Me.opt_level_change.Checked Then
        str_where = IIf(String.IsNullOrEmpty(str_where), " ", " AND ") & " (AM_TYPE IN ( " & WSI.Common.MovementType.ManualHolderLevelChanged & " )) "
      Else
        str_where = IIf(String.IsNullOrEmpty(str_where), " ", " AND ") & " (AM_TYPE IN ( " & WSI.Common.MovementType.PointsStatusChanged & " )) "
      End If
    End If

    _str_where_site = Me.uc_account_mov_sites_sel.GetSitesIdListSelected()

    If Not String.IsNullOrEmpty(_str_where_site) Then
      str_where += IIf(String.IsNullOrEmpty(str_where), " ", " AND ") & " ( AM_SITE_ID IN (" & _str_where_site & "))"
    End If

    Return IIf(String.IsNullOrEmpty(str_where), "", " WHERE ") & str_where
  End Function ' GetSqlWhere

#End Region ' Private Functions

#Region " Events "

#End Region ' Events


  ' PURPOSE: Gets the movement name and options of each kind of movement type
  '
  '  PARAMS:
  '     - INPUT:
  '           - mov_type: the Code of the movement
  '     - OUTPUT:
  '           - currency_format: True if the balance has to be formatted as currency
  '           - blank_row: True if the row doesn't have to show the add and sub cells
  '           - blank_balance: True if the row doesn't have to show the initial and final balance cells
  '           - unrecognized_movement: true if the movement doesn't have a personalized title
  '
  ' RETURNS:
  '     - A string with the name of the movement type
  Private Function TranslateMovementType(ByVal mov_type As Integer, Optional ByRef currency_format As Boolean = False, Optional ByRef blank_row As Boolean = False, Optional ByRef blank_balance As Boolean = False, Optional ByRef unrecognized_movement As Boolean = False) As String
    Dim _str_type As String
    Dim _buckets_forms As WSI.Common.BucketsForm
    Dim _name As String

    _str_type = ""
    currency_format = True
    blank_row = False
    blank_balance = False
    unrecognized_movement = False

    _buckets_forms = New WSI.Common.BucketsForm()
    _name = ""

    Select Case mov_type

      Case WSI.Common.MovementType.Play
        _str_type = GLB_NLS_GUI_INVOICING.GetString(41)

      Case WSI.Common.MovementType.CashIn
        _str_type = m_split_a_name_str

      Case WSI.Common.MovementType.CashOut
        If WSI.Common.Misc.IsVoucherModeTV Then
          _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7681)
        Else
          _str_type = GLB_NLS_GUI_INVOICING.GetString(43)
        End If

      Case WSI.Common.MovementType.Devolution
        _str_type = GLB_NLS_GUI_INVOICING.GetString(44)

      Case WSI.Common.MovementType.TaxOnPrize1
        _str_type = m_prize_taxes_1_name

      Case WSI.Common.MovementType.TaxOnPrize2
        _str_type = m_prize_taxes_2_name

      Case WSI.Common.MovementType.TaxOnPrize3
        _str_type = m_prize_taxes_3_name

      Case WSI.Common.MovementType.PromotionRedeemableFederalTax
        _str_type = String.Format("{0} - {1}", GLB_NLS_GUI_INVOICING.GetString(358), m_prize_taxes_1_name)

      Case WSI.Common.MovementType.PromotionRedeemableStateTax
        _str_type = String.Format("{0} - {1}", GLB_NLS_GUI_INVOICING.GetString(358), m_prize_taxes_2_name)

      Case WSI.Common.MovementType.PromotionRedeemableCouncilTax
        _str_type = String.Format("{0} - {1}", GLB_NLS_GUI_INVOICING.GetString(358), m_prize_taxes_3_name)

      Case WSI.Common.MovementType.StartCardSession
        _str_type = GLB_NLS_GUI_INVOICING.GetString(46)

      Case WSI.Common.MovementType.CancelStartSession
        _str_type = GLB_NLS_GUI_INVOICING.GetString(96)

      Case WSI.Common.MovementType.EndCardSession
        _str_type = GLB_NLS_GUI_INVOICING.GetString(47)

      Case WSI.Common.MovementType.Activate
        _str_type = GLB_NLS_GUI_INVOICING.GetString(48)

      Case WSI.Common.MovementType.Deactivate
        _str_type = GLB_NLS_GUI_INVOICING.GetString(49)

      Case WSI.Common.MovementType.PromotionNotRedeemable
        _str_type = GLB_NLS_GUI_INVOICING.GetString(50)

      Case WSI.Common.MovementType.CashInCoverCoupon
        _str_type = GLB_NLS_GUI_INVOICING.GetString(38)

      Case WSI.Common.MovementType.CancelNotRedeemable
        _str_type = GLB_NLS_GUI_INVOICING.GetString(53)

      Case WSI.Common.MovementType.DepositIn
        _str_type = GLB_NLS_GUI_INVOICING.GetString(51)

      Case WSI.Common.MovementType.DepositOut
        _str_type = GLB_NLS_GUI_INVOICING.GetString(52)

      Case WSI.Common.MovementType.CardReplacement
        _str_type = GLB_NLS_GUI_INVOICING.GetString(70)

      Case WSI.Common.MovementType.PromoCredits
        _str_type = GLB_NLS_GUI_INVOICING.GetString(64)

      Case WSI.Common.MovementType.PromoToRedeemable
        _str_type = GLB_NLS_GUI_INVOICING.GetString(65)

      Case WSI.Common.MovementType.PromoExpired
        _str_type = GLB_NLS_GUI_INVOICING.GetString(66)

      Case WSI.Common.MovementType.PromoCancel
        _str_type = GLB_NLS_GUI_INVOICING.GetString(67)

      Case WSI.Common.MovementType.PromoStartSession
        _str_type = GLB_NLS_GUI_INVOICING.GetString(68)

      Case WSI.Common.MovementType.PromoEndSession
        _str_type = GLB_NLS_GUI_INVOICING.GetString(69)

      Case WSI.Common.MovementType.CreditsExpired
        _str_type = GLB_NLS_GUI_INVOICING.GetString(477)

      Case WSI.Common.MovementType.PrizeExpired
        _str_type = GLB_NLS_GUI_INVOICING.GetString(89)

      Case WSI.Common.MovementType.CreditsNotRedeemableExpired
        _str_type = GLB_NLS_GUI_INVOICING.GetString(478)

      Case WSI.Common.MovementType.CreditsNotRedeemable2Expired
        _str_type = GLB_NLS_GUI_INVOICING.GetString(95)

      Case WSI.Common.MovementType.DrawTicketPrint
        currency_format = False
        blank_row = True
        blank_balance = True
        _str_type = GLB_NLS_GUI_INVOICING.GetString(71)

      Case WSI.Common.MovementType.PointsToDrawTicketPrint
        currency_format = False
        _str_type = GLB_NLS_GUI_INVOICING.GetString(83)

      Case WSI.Common.MovementType.Handpay
        _str_type = GLB_NLS_GUI_CONFIGURATION.GetString(364)

      Case WSI.Common.MovementType.HandpayCancellation
        _str_type = GLB_NLS_GUI_CONFIGURATION.GetString(365)

      Case WSI.Common.MovementType.ManualHandpay
        _str_type = GLB_NLS_GUI_CONFIGURATION.GetString(366)

      Case WSI.Common.MovementType.ManualHandpayCancellation
        _str_type = GLB_NLS_GUI_CONFIGURATION.GetString(367)

      Case WSI.Common.MovementType.ManualEndSession
        _str_type = GLB_NLS_GUI_INVOICING.GetString(74)

      Case WSI.Common.MovementType.PromoManualEndSession
        _str_type = GLB_NLS_GUI_INVOICING.GetString(75)

      Case WSI.Common.MovementType.PointsAwarded
        currency_format = False
        _str_type = GLB_NLS_GUI_INVOICING.GetString(76)

      Case WSI.Common.MovementType.PointsToGiftRequest
        currency_format = False
        _str_type = GLB_NLS_GUI_INVOICING.GetString(77)

      Case WSI.Common.MovementType.PointsToNotRedeemable
        currency_format = False
        _str_type = GLB_NLS_GUI_INVOICING.GetString(78)

      Case WSI.Common.MovementType.PointsToRedeemable
        currency_format = False
        _str_type = GLB_NLS_GUI_INVOICING.GetString(88)

      Case WSI.Common.MovementType.PointsGiftDelivery
        currency_format = False
        _str_type = GLB_NLS_GUI_INVOICING.GetString(79)

      Case WSI.Common.MovementType.PointsGiftServices
        currency_format = False
        _str_type = GLB_NLS_GUI_INVOICING.GetString(84)

      Case WSI.Common.MovementType.PointsExpired
        currency_format = False
        _str_type = GLB_NLS_GUI_INVOICING.GetString(81)

      Case WSI.Common.MovementType.HolderLevelChanged
        currency_format = False
        blank_row = True
        blank_balance = True
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(545)

      Case WSI.Common.MovementType.ManualHolderLevelChanged
        currency_format = False
        blank_row = True
        blank_balance = True
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1866)

      Case WSI.Common.MovementType.PrizeCoupon
        _str_type = GLB_NLS_GUI_INVOICING.GetString(86)

      Case WSI.Common.MovementType.CardCreated
        currency_format = False
        blank_row = True
        blank_balance = True
        _str_type = GLB_NLS_GUI_INVOICING.GetString(90)

      Case WSI.Common.MovementType.AccountPersonalization
        currency_format = False
        blank_row = True
        blank_balance = True
        _str_type = GLB_NLS_GUI_INVOICING.GetString(91)

      Case WSI.Common.MovementType.DEPRECATED_RedeemableSpentUsedInPromotions
        _str_type = GLB_NLS_GUI_INVOICING.GetString(87)

        ' JCA 21-OCT-2011 
      Case WSI.Common.MovementType.ManuallyAddedPointsOnlyForRedeem
        currency_format = False
        _str_type = GLB_NLS_GUI_INVOICING.GetString(92)
        ' JMM 22-NOV-2011
      Case WSI.Common.MovementType.AccountPINChange
        currency_format = False
        blank_row = True
        blank_balance = True
        _str_type = GLB_NLS_GUI_INVOICING.GetString(93)

      Case WSI.Common.MovementType.AccountPINRandom
        currency_format = False
        blank_row = True
        blank_balance = True
        _str_type = GLB_NLS_GUI_INVOICING.GetString(94)

        ' JCM 24-APR-2012 Card Recycle Movement
      Case WSI.Common.MovementType.CardRecycled
        currency_format = False
        blank_row = True
        blank_balance = True
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(654)

        ' DDM 05-JUL-2012
      Case WSI.Common.MovementType.PromotionRedeemable
        If WSI.Common.Misc.IsVoucherModeTV Then
          _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7682)
        Else
          _str_type = GLB_NLS_GUI_INVOICING.GetString(100)
        End If

      Case WSI.Common.MovementType.CancelPromotionRedeemable
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1120)

      Case WSI.Common.MovementType.PromotionPoint
        currency_format = False
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1121)

      Case WSI.Common.MovementType.CancelPromotionPoint
        currency_format = False
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1122)

      Case WSI.Common.MovementType.TaxReturningOnPrizeCoupon
        _str_type = GLB_NLS_GUI_INVOICING.GetString(483)

        ' RRB 13-SEP-2012
      Case WSI.Common.MovementType.DecimalRounding
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1126)

      Case WSI.Common.MovementType.ServiceCharge
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1127)

      Case WSI.Common.MovementType.CancelGiftInstance
        currency_format = False
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1129)

      Case WSI.Common.MovementType.PointsStatusChanged
        currency_format = False
        blank_row = True
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1645)

      Case WSI.Common.MovementType.ManuallyAddedPointsForLevel  '= 68, Points added manually  
        currency_format = False
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1860)

      Case WSI.Common.MovementType.ImportedAccount              '= 70, Imported(account) 
        blank_row = True
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1150)

      Case WSI.Common.MovementType.ImportedPointsOnlyForRedeem  '= 71, Imported points (not accounted for the level) 
        currency_format = False
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1861)

      Case WSI.Common.MovementType.ImportedPointsForLevel       '= 72, Imported(points) 
        currency_format = False
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1862)

      Case WSI.Common.MovementType.ImportedPointsHistory        '= 73, History points imported 
        currency_format = False
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1863)

      Case WSI.Common.MovementType.AccountBlocked               '= 75, Account blocked
        currency_format = False
        blank_row = True
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1133)

      Case WSI.Common.MovementType.AccountUnblocked             '= 76, Account unblocked
        currency_format = False
        blank_row = True
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1134)

      Case WSI.Common.MovementType.AccountInBlacklist           '= 240, Account added in blacklist
        currency_format = False
        blank_row = True
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7452)

      Case WSI.Common.MovementType.AccountNotInBlacklist        '= 241, Account removed from blacklist
        currency_format = False
        blank_row = True
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7453)

        'FBA 12-AUG-2013 new movement ID 77. Deposit Cancelation
      Case WSI.Common.MovementType.Cancellation                 '= 77, Deposit Cancelation
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2503)

        ' DHA 25-APR-2014 modified/added TITO tickets movements
      Case WSI.Common.MovementType.TITO_TicketCashierPrintedCashable
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2545)

      Case WSI.Common.MovementType.TITO_TicketCashierPrintedPromoRedeemable
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2655)

      Case WSI.Common.MovementType.TITO_TicketCashierPrintedPromoNotRedeemable
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4571)

      Case WSI.Common.MovementType.TITO_TicketMachinePrintedCashable
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4880)

      Case WSI.Common.MovementType.TITO_TicketMachinePrintedPromoNotRedeemable
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4881)

      Case WSI.Common.MovementType.TITO_TicketCashierPaidCashable
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2634)

      Case WSI.Common.MovementType.TITO_TicketCashierPaidPromoRedeemable
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4882)

      Case WSI.Common.MovementType.TITO_TicketMachinePlayedCashable
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2841)

      Case WSI.Common.MovementType.TITO_TicketMachinePlayedPromoRedeemable
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4883)

      Case WSI.Common.MovementType.TITO_ticketMachinePlayedPromoNotRedeemable
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4884)

      Case WSI.Common.MovementType.TITO_TicketCashierPaidJackpot
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4599)

      Case WSI.Common.MovementType.TITO_TicketCashierPaidHandpay
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4598)

      Case WSI.Common.MovementType.TITO_TicketMachinePrintedHandpay
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4583)

      Case WSI.Common.MovementType.TITO_TicketMachinePrintedJackpot
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4584)

      Case WSI.Common.MovementType.TITO_TicketReissue
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2997)

      Case WSI.Common.MovementType.TITO_AccountToTerminalCredit
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3398)

      Case WSI.Common.MovementType.TITO_TerminalToAccountCredit
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3399)

      Case WSI.Common.MovementType.MultiSiteCurrentLocalPoints  '= 101, Multisite - Current points in site, similar ManuallyAddedPointsOnlyForRedeem 
        currency_format = False
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1864)

      Case WSI.Common.MovementType.CashdeskDrawParticipation
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2570)
        blank_row = True

      Case WSI.Common.MovementType.CashdeskDrawPlaySession
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2596)

        ' LJM 18-DEC-2013 For Chip movements
      Case WSI.Common.MovementType.ChipsPurchaseTotal
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1870)

      Case WSI.Common.MovementType.ChipsSaleTotal
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1869)

      Case WSI.Common.MovementType.ChipsSaleRemainingAmount
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8417)

      Case WSI.Common.MovementType.ChipsSaleConsumedRemainingAmount
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8475)

      Case WSI.Common.MovementType.ChipsSaleDevolutionForTito
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1871)
        ' LJM

        ' DLL 31-DEC-2013 Tax deposit
      Case WSI.Common.MovementType.CashInTax
        _str_type = WSI.Common.GeneralParam.GetString("Cashier", "CashInTax.Name", GLB_NLS_GUI_PLAYER_TRACKING.GetString(4562))

      Case WSI.Common.MovementType.TaxCustody
        _str_type = WSI.Common.Misc.TaxCustodyName()

      Case WSI.Common.MovementType.TaxReturnCustody
        _str_type = WSI.Common.Misc.TaxCustodyRefundName()

        ' DHA 02-MAR-2016 Tax provisions
      Case WSI.Common.MovementType.TaxProvisions
        _str_type = WSI.Common.GeneralParam.GetString("Cashier.TaxProvisions", "Voucher.Title", GLB_NLS_GUI_PLAYER_TRACKING.GetString(7151))

      Case WSI.Common.MovementType.TITO_TicketOffline
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4470)

      Case WSI.Common.MovementType.CashdeskDrawParticipation
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4572)

        ' RMS 21-JAN-2014
      Case WSI.Common.MovementType.TransferCreditIn
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4479)

      Case WSI.Common.MovementType.TransferCreditOut
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4480)

        'HBB 29-MAY-2014
        'Case WSI.Common.MovementType.ChipsSaleWithCashIn
        '  _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5007)
        'Case WSI.Common.MovementType.ChipsPurchaseWithCashOut
        '  _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5008)

      Case WSI.Common.MovementType.CardReplacementForFree
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5079)

      Case WSI.Common.MovementType.CardReplacementInPoints
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5080)

      Case WSI.Common.MovementType.CashAdvance
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5461)

        ' OPC 04-NOV-2014
      Case WSI.Common.MovementType.ExternalSystemPointsSubstract
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5694)
        currency_format = False

      Case WSI.Common.MovementType.WinLossStatementRequest
        currency_format = False
        blank_row = True
        blank_balance = True
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6253)

        ' DHA 09-JUL-2015
      Case WSI.Common.MovementType.CardDepositInCheck
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6513)

      Case WSI.Common.MovementType.CardDepositInCurrencyExchange
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6514)

      Case WSI.Common.MovementType.CardDepositInCardCredit
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6515)

      Case WSI.Common.MovementType.CardDepositInCardDebit
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6516)

      Case WSI.Common.MovementType.CardDepositInCardGeneric
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6517)

      Case WSI.Common.MovementType.CardReplacementCheck
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6508)

      Case WSI.Common.MovementType.CardReplacementCardCredit
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6510)

      Case WSI.Common.MovementType.CardReplacementCardDebit
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6511)

      Case WSI.Common.MovementType.CardReplacementCardGeneric
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6512)

        'BonoPlay
      Case WSI.Common.MovementType.GameGatewayBet
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7052, WSI.Common.GeneralParam.GetString("GameGateway", "Name", "BonoPlay"))

      Case WSI.Common.MovementType.GameGatewayPrize
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7053, WSI.Common.GeneralParam.GetString("GameGateway", "Name", "BonoPlay"))

      Case WSI.Common.MovementType.GameGatewayBetRollback
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7064, WSI.Common.GeneralParam.GetString("GameGateway", "Name", "BonoPlay"))

      Case WSI.Common.MovementType.GameGatewayReserveCredit
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6966)

      Case WSI.Common.MovementType.CurrencyExchangeCashierIn
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7323)

      Case WSI.Common.MovementType.CurrencyExchangeCashierOut
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7324)

      Case WSI.Common.MovementType.MULTIPLE_BUCKETS_Expired To WSI.Common.MovementType.MULTIPLE_BUCKETS_ExpiredLast ' [1100-1199]
        If (_buckets_forms.GetBucketName(CType(mov_type, WSI.Common.MovementType), _name)) Then
          _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7074, _name)
        Else
          _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2504, mov_type.ToString) ' Default value if error.
          unrecognized_movement = True
        End If
        currency_format = False

      Case WSI.Common.MovementType.MULTIPLE_BUCKETS_EndSession To WSI.Common.MovementType.MULTIPLE_BUCKETS_EndSessionLast ' [1200-1299]
        If (_buckets_forms.GetBucketName(CType(mov_type, WSI.Common.MovementType), _name)) Then
          _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7075, _name)
        Else
          _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2504, mov_type.ToString) ' Default value if error.
          unrecognized_movement = True
        End If
        currency_format = False

      Case WSI.Common.MovementType.MULTIPLE_BUCKETS_Manual_Add To WSI.Common.MovementType.MULTIPLE_BUCKETS_Manual_Add_Last ' [1300-1399]
        If (_buckets_forms.GetBucketName(CType(mov_type, WSI.Common.MovementType), _name)) Then
          _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7065, _name)
        Else
          _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2504, mov_type.ToString) ' Default value if error.
          unrecognized_movement = True
        End If
        currency_format = False

      Case WSI.Common.MovementType.MULTIPLE_BUCKETS_Manual_Sub To WSI.Common.MovementType.MULTIPLE_BUCKETS_Manual_Sub_Last ' [1400-1499]
        If (_buckets_forms.GetBucketName(CType(mov_type, WSI.Common.MovementType), _name)) Then
          _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7066, _name)
        Else
          _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2504, mov_type.ToString) ' Default value if error.
          unrecognized_movement = True
        End If
        currency_format = False

      Case WSI.Common.MovementType.MULTIPLE_BUCKETS_Manual_Set To WSI.Common.MovementType.MULTIPLE_BUCKETS_Manual_Set_Last ' [1300-1399]
        If (_buckets_forms.GetBucketName(CType(mov_type, WSI.Common.MovementType), _name)) Then
          _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7076, _name)
        Else
          _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2504, mov_type.ToString) ' Default value if error.
          unrecognized_movement = True
        End If
        currency_format = False

        ' XGJ 07-set-2016
      Case WSI.Common.MovementType.Admission
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7583)

        'ATB 18-oct-2016
      Case WSI.Common.MovementType.CardAssociate
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7403)

        'FBA 12-AUG-2013 default movement ID

        ' XGJ 19-SEPT-2016 
        ' Pariplay
      Case WSI.Common.MovementType.PariPlayBet
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7052, WSI.Common.GeneralParam.GetString("PariPlay", "Name", "PariPlay"))

      Case WSI.Common.MovementType.PariPlayPrize
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7053, WSI.Common.GeneralParam.GetString("PariPlay", "Name", "PariPlay"))

      Case WSI.Common.MovementType.PariPlayBetRollback
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7064, WSI.Common.GeneralParam.GetString("PariPlay", "Name", "PariPlay"))

      Case WSI.Common.MovementType.TITO_TicketCountRPrintedCashable
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7289)

      Case WSI.Common.MovementType.CreditLineCreate
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7964)

      Case WSI.Common.MovementType.CreditLineUpdate
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7965)

      Case WSI.Common.MovementType.CreditLineApproved
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7966)

      Case WSI.Common.MovementType.CreditLineNotApproved
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7967)

      Case WSI.Common.MovementType.CreditLineSuspended
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7968)

      Case WSI.Common.MovementType.CreditLineExpired
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7969)

      Case WSI.Common.MovementType.CreditLineGetMarker
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7970)

      Case WSI.Common.MovementType.CreditLinePayback
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7971)

      Case WSI.Common.MovementType.CreditLineGetMarkerCardReplacement
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7989)

      Case Else  'Movement not denifed -> Movement (XX)
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2504, mov_type.ToString)
        unrecognized_movement = True

    End Select
    Return _str_type
  End Function


End Class
