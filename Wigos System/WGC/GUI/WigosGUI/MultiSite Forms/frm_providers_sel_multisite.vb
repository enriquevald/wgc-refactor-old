'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_providers_sel_multisite.vb
'
' DESCRIPTION:   Provider selection form to multisite.
'
' AUTHOR:        Jes�s �ngel Blanco
'
' CREATION DATE: 30-APR-2013
'
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 09-MAY-2013  JAB    Initial version.
' 10-JUN-2013  JAB    Add PayOut column.
' 12-FEB-2018  RGR    Bug 31495:WIGOS-6131 Incorrect scroll bar displayed in the screen "Proveedores" from the MultisiteGUI 
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

#Region " Imports "

Imports GUI_CommonMisc
Imports GUI_Controls
Imports GUI_CommonOperations
Imports System.Data.SqlClient
Imports System.Text
Imports WSI.Common

#End Region ' Imports

Public Class frm_providers_sel_multisite
  Inherits GUI_Controls.frm_base_sel

#Region " Members "
  Private m_table_master_mode As Integer

  Private m_row_color As System.Drawing.Color
  Private m_row_color2 As System.Drawing.Color
  Private m_last_row_color As System.Drawing.Color
  Private m_previous_provider As String
  Private m_current_provider As String

  Private m_filter_providers_game As String
  Private m_filter_sites As String
  Private m_filter_game_name As String

  Private m_str_pay_out As String

#End Region ' Members

#Region " Constants "

  ' DB Columns
  Private Const SQL_COLUMN_SITE_ID As Integer = 0
  Private Const SQL_COLUMN_ID As Integer = 1
  Private Const SQL_COLUMN_NAME As Integer = 2
  Private Const SQL_COLUMN_JACKPOT As Integer = 3
  Private Const SQL_COLUMN_MULTIPLIER As Integer = 4
  Private Const SQL_COLUMN_REDEEMABLE_ONLY As Integer = 5
  Private Const SQL_COLUMN_3GS As Integer = 6
  Private Const SQL_COLUMN_IP As Integer = 7
  Private Const SQL_COLUMN_GAME_NAME As Integer = 8
  Private Const SQL_COLUMN_PAYOUT_1 As Integer = 9
  Private Const SQL_COLUMN_PAYOUT_2 As Integer = 10
  Private Const SQL_COLUMN_PAYOUT_3 As Integer = 11
  Private Const SQL_COLUMN_PAYOUT_4 As Integer = 12
  Private Const SQL_COLUMN_PAYOUT_5 As Integer = 13
  Private Const SQL_COLUMN_PAYOUT_6 As Integer = 14
  Private Const SQL_COLUMN_PAYOUT_7 As Integer = 15
  Private Const SQL_COLUMN_PAYOUT_8 As Integer = 16
  Private Const SQL_COLUMN_PAYOUT_9 As Integer = 17
  Private Const SQL_COLUMN_PAYOUT_10 As Integer = 18

  ' Grid Columns
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_SITE_ID As Integer = 1
  Private Const GRID_COLUMN_ID As Integer = 2
  Private Const GRID_COLUMN_NAME As Integer = 3
  Private Const GRID_COLUMN_GAME_NAME As Integer = 4
  Private Const GRID_COLUMN_JACKPOT As Integer = 5
  Private Const GRID_COLUMN_MULTIPLIER As Integer = 6
  Private Const GRID_COLUMN_REDEEMABLE_ONLY As Integer = 7
  Private Const GRID_COLUMN_3GS As Integer = 8
  Private Const GRID_COLUMN_IP As Integer = 9
  Private Const GRID_COLUMN_PAYOUTS As Integer = 10

  Private Const GRID_COLUMNS As Integer = 11
  Private Const GRID_HEADER_ROWS As Integer = 1

  ' Grid Columns Width
  Private Const GRID_WIDTH_HIDE As Integer = 0
  Private Const GRID_WIDTH_INDEX As Integer = 150
  Private Const GRID_WIDTH_ID As Integer = 0 ' 150
  Private Const GRID_WIDTH_SITE_ID As Integer = 0 '850
  Private Const GRID_WIDTH_NAME As Integer = 4580
  Private Const GRID_WIDTH_JACKPOT As Integer = 0 '1700
  Private Const GRID_WIDTH_MULTIPLIER As Integer = 0 '2300
  Private Const GRID_WIDTH_REDEEMABLE_ONLY As Integer = 0 '1620
  Private Const GRID_WIDTH_3GS As Integer = 0 '650
  Private Const GRID_WIDTH_IP As Integer = 0 '1600
  Private Const GRID_WIDTH_GAME_NAME As Integer = 4580
  Private Const GRID_WIDTH_PAYOUTS As Integer = 7800

#End Region ' Constants

#Region " Overrides "

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()
    Me.FormId = ENUM_FORM.FORM_PROVIDERS_GAMES_SEL
    Call MyBase.GUI_SetFormId()
  End Sub ' GUI_SetFormId

  ' PURPOSE : Sets the values of a row in the data grid
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the row could be added
  '     - False: the row could not be added
  '
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Call MyBase.GUI_SetupRow(RowIndex, DbRow)

    ' Id
    Me.Grid.Cell(RowIndex, GRID_COLUMN_ID).Value = DbRow.Value(SQL_COLUMN_ID)

    ' Site Id
    If DbRow.Value(SQL_COLUMN_SITE_ID) = 0 Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SITE_ID).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1791)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SITE_ID).Value = DbRow.Value(SQL_COLUMN_SITE_ID)
    End If

    ' Name
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NAME).Value = DbRow.Value(SQL_COLUMN_NAME)

    'Game Name
    Me.Grid.Cell(RowIndex, GRID_COLUMN_GAME_NAME).Value = DbRow.Value(SQL_COLUMN_GAME_NAME).ToString()

    ' Multiplier
    Me.Grid.Cell(RowIndex, GRID_COLUMN_MULTIPLIER).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_MULTIPLIER), 2)

    ' Jackpot
    If DbRow.Value(SQL_COLUMN_JACKPOT) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_JACKPOT).Value = Me.Grid.Column(GRID_COLUMN_JACKPOT).Header.Text
    End If

    ' Redeemable only
    If DbRow.Value(SQL_COLUMN_REDEEMABLE_ONLY) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_REDEEMABLE_ONLY).Value = Me.Grid.Column(GRID_COLUMN_REDEEMABLE_ONLY).Header.Text
    End If

    ' 3GS
    If DbRow.Value(SQL_COLUMN_3GS) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_3GS).Value = Me.Grid.Column(GRID_COLUMN_3GS).Header.Text
    End If

    If Not DbRow.Value(SQL_COLUMN_IP) Is DBNull.Value Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_IP).Value = DbRow.Value(SQL_COLUMN_IP)
    End If

    If m_previous_provider <> DbRow.Value(SQL_COLUMN_NAME) Then
      If m_current_provider Mod 2 Then
        m_last_row_color = m_row_color
      Else
        m_last_row_color = m_row_color2
      End If
      m_current_provider = m_current_provider + 1
      m_previous_provider = DbRow.Value(SQL_COLUMN_NAME)
    End If

    ' PayOuts
    m_str_pay_out = ""
    If Me.chk_games.Checked Then

      For i As Integer = SQL_COLUMN_PAYOUT_1 To SQL_COLUMN_PAYOUT_10
        GetPayOut(DbRow.Value(i).ToString(), i)
      Next

      Me.Grid.Cell(RowIndex, GRID_COLUMN_PAYOUTS).Value = m_str_pay_out

    End If

    Me.Grid.Row(RowIndex).BackColor = m_last_row_color

    Return True

  End Function ' GUI_SetupRow

  ' PURPOSE: Actions to do before insert first row.
  '
  '  PARAMS:
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  Protected Overrides Sub GUI_BeforeFirstRow()
    m_current_provider = 0
    MyBase.GUI_BeforeFirstRow()
  End Sub

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    If m_table_master_mode = 1 Then

      uc_site_select.Visible = False
      Me.panel_filter.Height = 76
      ef_providers_games_name.Location = New System.Drawing.Point(20, 26)

      ' Posiciones en el caso de usar 3gs y games
      '''chk_3gs.Location = New System.Drawing.Point(ef_providers_games_name.Location.X + 43, chk_3gs.Location.Y + 9)
      '''chk_games.Location = New System.Drawing.Point(ef_providers_games_name.Location.X + 143, chk_games.Location.Y + 9)

      chk_games.Location = New System.Drawing.Point(ef_providers_games_name.Location.X - 53, chk_3gs.Location.Y + 9)

      If Me.Permissions.Write Then
        Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = True
      Else
        Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = False
      End If
    Else
      Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = False
    End If
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_CONTROLS.GetString(6)

    ' - Form title
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1916)

    ' - Buttons
    GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(10)

    GUI_Button(ENUM_BUTTON.BUTTON_EXCEL).Visible = True
    GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False

    Me.uc_site_select.ShowMultisiteRow = True
    Me.uc_site_select.MultiSelect = False
    Me.uc_site_select.Init()
    Me.uc_site_select.SetDefaultValues(False)

    Me.ef_providers_games_name.Text = GLB_NLS_GUI_AUDITOR.GetString(341)
    Me.ef_providers_games_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 64)

    Me.chk_3gs.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1920)
    Me.chk_games.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2012)

    Me.chk_3gs.Visible = False


    Call Me.Grid.Clear()

    m_row_color = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)
    m_row_color2 = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)

    Call GUI_StyleSheet()

  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  ' PURPOSE: Filter Check
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Function GUI_FilterCheck() As Boolean

    If m_table_master_mode <> 1 Then
      If Not Me.uc_site_select.FilterCheckSites() Then
        Return False
      End If
    End If

    Return True
  End Function ' GUI_FilterCheck

  ' PURPOSE: Return query type.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - ENUM_QUERY_TYPE
  '
  Protected Overrides Function GUI_GetQueryType() As ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.QUERY_COMMAND
  End Function ' GUI_GetQueryType

  ' PURPOSE: Set focus.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.ef_providers_games_name
  End Sub ' GUI_SetInitialFocus

  ' PURPOSE: Set sql command.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SqlCommand
  '
  Protected Overrides Function GUI_GetSqlCommand() As SqlCommand

    Dim _sql_cmd As SqlCommand

    Dim _str_sql As StringBuilder
    _str_sql = New StringBuilder()

    _str_sql.AppendLine("   SELECT   RD.PV_SITE_ID                                                              ")
    _str_sql.AppendLine("          , RD.PV_ID                                                                   ")
    _str_sql.AppendLine("          , RD.PV_NAME                                                                 ")
    _str_sql.AppendLine("          , RD.PV_SITE_JACKPOT                                                         ")
    _str_sql.AppendLine("          , RD.PV_POINTS_MULTIPLIER                                                    ")
    _str_sql.AppendLine("          , RD.PV_ONLY_REDEEMABLE                                                      ")
    _str_sql.AppendLine("          , RD.PV_3GS 	                                                                ")
    _str_sql.AppendLine("          , RD.PV_3GS_VENDOR_IP         ")

    If Me.chk_games.Checked Then
      _str_sql.AppendLine("        , ISNULL (GM_GAME_NAME,'') AS GM_GAME_NAME                                   ")
      _str_sql.AppendLine("        , GM_PAYOUT_1                                                                ")
      _str_sql.AppendLine("        , GM_PAYOUT_2                                                                ")
      _str_sql.AppendLine("        , GM_PAYOUT_3                                                                ")
      _str_sql.AppendLine("        , GM_PAYOUT_4                                                                ")
      _str_sql.AppendLine("        , GM_PAYOUT_5                                                                ")
      _str_sql.AppendLine("        , GM_PAYOUT_6                                                                ")
      _str_sql.AppendLine("        , GM_PAYOUT_7                                                                ")
      _str_sql.AppendLine("        , GM_PAYOUT_8                                                                ")
      _str_sql.AppendLine("        , GM_PAYOUT_9                                                                ")
      _str_sql.AppendLine("        , GM_PAYOUT_10                                                               ")
      Me.Grid.Column(GRID_COLUMN_GAME_NAME).Width = GRID_WIDTH_GAME_NAME
    Else
      _str_sql.AppendLine("        , NULL AS GM_GAME_NAME")
    End If

    _str_sql.AppendLine("     FROM   PROVIDERS RD                                                               ")

    If Me.chk_games.Checked Then
      _str_sql.AppendLine("  LEFT JOIN GAMES ON PV_ID = GM_PV_ID ")
    End If

    '_str_sql.AppendLine("    WHERE   RD.PV_NAME like '%'+@pPvName+'%'                                           ")
    _str_sql.AppendLine("    WHERE   " & GUI_FilterField("RD.PV_NAME", Me.ef_providers_games_name.Value, False, False, True))

    If Me.chk_3gs.Checked Then
      _str_sql.AppendLine("    AND  RD.PV_3GS = 1                                                               ")
    End If
    _str_sql.AppendLine(" AND  RD.PV_NAME <> 'UNKNOWN'                    ")
    _str_sql.AppendLine(" ORDER BY RD.PV_NAME                             ")
    If Me.chk_games.Checked Then
      _str_sql.AppendLine("       ,GAMES.GM_GAME_NAME                                                 ")
    End If

    _sql_cmd = New SqlCommand(_str_sql.ToString())
    '_sql_cmd.Parameters.Add("@pPvName", SqlDbType.NVarChar).Value = Me.ef_providers_games_name.Value

    Return _sql_cmd

  End Function ' GUI_GetSqlCommand

  ' PURPOSE: Set sql command.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SqlCommand
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    'Select Case ButtonId
    '  Case ENUM_BUTTON.BUTTON_CUSTOM_0
    '    'Case ENUM_BUTTON.BUTTON_NEW
    '    Call EditNewUser()
    '  Case ENUM_BUTTON.BUTTON_SELECT
    '    Call GUI_EditSelectedItem()

    '  Case Else
    '    Call MyBase.GUI_ButtonClick(ButtonId)
    'End Select

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_CUSTOM_0
        Call EditNewUser()
      Case ENUM_BUTTON.BUTTON_SELECT
        Call GUI_EditSelectedItem()
      Case ENUM_BUTTON.BUTTON_FILTER_APPLY
        Call GUI_StyleSheet()
        Me.Grid.Update()
        Call MyBase.GUI_ButtonClick(ButtonId)
      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

  ' PURPOSE: Edit Item.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_EditSelectedItem()

    Dim _idx_row As Int32
    Dim _prov_site_id As Integer
    Dim _prov_id As Integer
    Dim _prov_name As String
    Dim _prov_jackpot As String
    Dim _prov_multiplier As String
    Dim _prov_redeemable_only As String
    Dim _prov_3gs As String
    Dim _frm_edit As Object

    ' Search the first row selected
    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then
        Exit For
      End If
    Next

    If _idx_row = Me.Grid.NumRows Then
      Return
    End If

    ' Get the Provider Values, and launch the editor
    _prov_site_id = IIf(Me.Grid.Cell(_idx_row, GRID_COLUMN_SITE_ID).Value.ToString = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1791), 0, Me.Grid.Cell(_idx_row, GRID_COLUMN_SITE_ID).Value)
    _prov_id = Me.Grid.Cell(_idx_row, GRID_COLUMN_ID).Value
    _prov_name = Me.Grid.Cell(_idx_row, GRID_COLUMN_NAME).Value
    _prov_jackpot = Me.Grid.Cell(_idx_row, GRID_COLUMN_JACKPOT).Value
    _prov_multiplier = Me.Grid.Cell(_idx_row, GRID_COLUMN_MULTIPLIER).Value
    _prov_redeemable_only = Me.Grid.Cell(_idx_row, GRID_COLUMN_REDEEMABLE_ONLY).Value
    _prov_3gs = Me.Grid.Cell(_idx_row, GRID_COLUMN_3GS).Value

    _frm_edit = New frm_providers_edit_multisite

    Call _frm_edit.ShowEditItem(_prov_id, _prov_site_id)

    _frm_edit = Nothing

    Call Me.Grid.Focus()

  End Sub ' GUI_EditSelectedItem

#End Region ' Overrides

#Region " Private Functions "

  ' PURPOSE: Edit new user. 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub EditNewUser()

    Dim idx_row As Short
    'XVV Variable not use Dim row_type As Integer
    Dim frm_edit As Object

    ' Search the first row selected
    For idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(idx_row).IsSelected Then
        Exit For
      End If
    Next

    frm_edit = New frm_providers_edit_multisite
    Call frm_edit.ShowNewItem()

    frm_edit = Nothing
    Call Me.Grid.Focus()

  End Sub ' EditNewUser

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub GUI_StyleSheet()

    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)
      .Sortable = False
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' Index
      .Column(GRID_COLUMN_INDEX).Header(0).Text = ""
      .Column(GRID_COLUMN_INDEX).Header(1).Text = ""
      .Column(GRID_COLUMN_INDEX).Width = GRID_WIDTH_INDEX
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False
      .Column(GRID_COLUMN_INDEX).Editable = False

      ' ID
      .Column(GRID_COLUMN_ID).Width = GRID_WIDTH_HIDE
      .Column(GRID_COLUMN_ID).HighLightWhenSelected = False
      .Column(GRID_COLUMN_ID).IsColumnPrintable = False

      ' SITE ID
      If m_table_master_mode Then
        .Column(GRID_COLUMN_SITE_ID).Width = GRID_WIDTH_HIDE
      Else
        .Column(GRID_COLUMN_SITE_ID).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1927) ' "ID Sala"
        .Column(GRID_COLUMN_SITE_ID).Width = GRID_WIDTH_SITE_ID
        .Column(GRID_COLUMN_SITE_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      End If

      ' NAME
      .Column(GRID_COLUMN_NAME).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1922) ' "Nombre"
      .Column(GRID_COLUMN_NAME).Width = GRID_WIDTH_NAME
      .Column(GRID_COLUMN_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_NAME).IsMerged = True

      ' GAME_NAME
      .Column(GRID_COLUMN_GAME_NAME).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2008) ' "Proveedor"
      .Column(GRID_COLUMN_GAME_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      If Me.chk_games.Checked Then
        .Column(GRID_COLUMN_GAME_NAME).Width = GRID_WIDTH_GAME_NAME
      Else
        .Column(GRID_COLUMN_GAME_NAME).Width = GRID_WIDTH_HIDE
      End If

      ' JACKPOT
      .Column(GRID_COLUMN_JACKPOT).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1918) ' "Jackpot"
      .Column(GRID_COLUMN_JACKPOT).Width = GRID_WIDTH_JACKPOT
      .Column(GRID_COLUMN_JACKPOT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' MULTIPLIER
      .Column(GRID_COLUMN_MULTIPLIER).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1921) ' "Multiplier"
      .Column(GRID_COLUMN_MULTIPLIER).Width = GRID_WIDTH_MULTIPLIER
      .Column(GRID_COLUMN_MULTIPLIER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' REDEEMABLE ONLY
      .Column(GRID_COLUMN_REDEEMABLE_ONLY).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1919) ' "Redeemable only"
      .Column(GRID_COLUMN_REDEEMABLE_ONLY).Width = GRID_WIDTH_REDEEMABLE_ONLY
      .Column(GRID_COLUMN_REDEEMABLE_ONLY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' 3GS
      .Column(GRID_COLUMN_3GS).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1920) ' "3GS"
      .Column(GRID_COLUMN_3GS).Width = GRID_WIDTH_3GS
      .Column(GRID_COLUMN_3GS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' IP
      .Column(GRID_COLUMN_IP).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1925) ' "IP address"
      .Column(GRID_COLUMN_IP).Width = GRID_WIDTH_IP
      .Column(GRID_COLUMN_IP).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' PAYOUTS
      .Column(GRID_COLUMN_PAYOUTS).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2048) ' "Payout"
      .Column(GRID_COLUMN_PAYOUTS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      If Me.chk_games.Checked Then
        .Column(GRID_COLUMN_PAYOUTS).Width = GRID_WIDTH_PAYOUTS
      Else
        .Column(GRID_COLUMN_PAYOUTS).Width = GRID_WIDTH_HIDE
      End If

    End With

  End Sub ' GUI_StyleSheet

  Private Sub GetPayOut(ByVal PayOut As String, ByVal PayOutOrder As Integer)

    If Not String.IsNullOrEmpty(PayOut) Then
      m_str_pay_out += IIf(PayOutOrder = SQL_COLUMN_PAYOUT_1 Or String.IsNullOrEmpty(m_str_pay_out), GUI_FormatNumber(PayOut, 2).ToString & "%", ", " & GUI_FormatNumber(PayOut, 2).ToString & "%")
    End If

  End Sub

  ' PURPOSE : Sets default values.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '
  Private Sub SetDefaultValues()
    ' Reset values 
    Me.ef_providers_games_name.Value = ""
    Me.uc_site_select.SetDefaultValues(False)
    Me.chk_3gs.Checked = False
    Me.chk_games.Checked = False
    Call ef_providers_games_name.Focus()

  End Sub ' SetDefaultValues

#End Region  'Private Functions

#Region " Public Functions "

  ' PURPOSE : Opens dialog with default settings for edit mode
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    m_table_master_mode = GeneralParam.GetInt32("MultiSite", "TablesMasterMode", 0)
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region 'Public Functions

#Region " GUI Reports "

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(341), m_filter_providers_game)
    If GeneralParam.GetInt32("MultiSite", "TablesMasterMode", 0) <> 1 Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1803), m_filter_sites)
    End If
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2008), m_filter_game_name)

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Protected Overrides Sub GUI_ReportUpdateFilters()
    m_filter_providers_game = ""
    m_filter_sites = ""
    m_filter_game_name = ""

    If Not String.IsNullOrEmpty(Me.ef_providers_games_name.Value) Then
      m_filter_providers_game = Me.ef_providers_games_name.Value
    Else
      m_filter_providers_game = GLB_NLS_GUI_ALARMS.GetString(277)
    End If

    If m_table_master_mode <> 1 Then
      If Not String.IsNullOrEmpty(uc_site_select.GetSitesIdListSelected()) Then
        m_filter_sites = Me.uc_site_select.GetSitesIdListSelectedToPrint()
      Else
        m_filter_sites = GLB_NLS_GUI_ALARMS.GetString(282)
      End If
    End If

    ' Solo cuando se use 3Gs - ocultar por ahora
    ''If Me.chk_3gs.Checked Then
    ''  PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1920), GLB_NLS_GUI_AUDITOR.GetString(336))
    ''Else
    ''  PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1920), GLB_NLS_GUI_AUDITOR.GetString(337))
    ''End If

    If Me.chk_games.Checked Then
      m_filter_game_name = GLB_NLS_GUI_AUDITOR.GetString(336)
    Else
      m_filter_game_name = GLB_NLS_GUI_AUDITOR.GetString(337)
    End If

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

End Class