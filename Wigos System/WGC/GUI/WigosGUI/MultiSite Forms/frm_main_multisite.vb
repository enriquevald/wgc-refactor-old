'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_main_multisite.vb
' DESCRIPTION:   Supercenter main.
' AUTHOR:        Marcos Piedra Osuna
' CREATION DATE: 04-MAR-2013
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 04-MAR-2013  MPO    Initial version
' 17-APR-2013  AMF    New Menu Configuration
' 04-JUN-2013  ANG    Fix bug #818
' 12-SEP-2013  ANG    Fix bug WIG-199
' 12-SEP-2013  MMG    Modified PlayerEditConfigClick(it was opening an incorrect form)
' 05-DEC-2013  RMS    Added version to form title
' 04-DEC-2014  DHA    Added LCD message form
' 08-JAN-2015  DCS    Added Jackpot site form
' 12-FEB-2015  FJC    New GUI Menu
' 22-APR-2015  ANM    Added Multicurrency form
' 18-MAY-2015  JML    Backlog Item 1749: Multisite-Multicurrency permisions
' 14-DEC-2015  FOS    Backlog Item 1858: MultiSite MultiCurrency:Accounts Information
' 13-JAN-2016  JML    Product Backlog Item 2541: MultiSite Multicurrency PHASE3: Loyalty program in the sites.
' 10-MAR-2016  JRC    PBI 7191 Multiple Buckets
' 30-MAR-2016  JRC    PBI 10792 Multiple Buckets: Desactivar Acumulaci�n de Buckets si SPACE is enabled
' 20-OCT-2016  ESE    Bug 10200: "Sales and retirements report" do not show correctly permissions
' 03-APR-2017  ETP    WIGOS - 116: CreditLines - Multisite.
' 19-SEP-2017  ETP    PBI 29621: AGG Data transfer [WIGOS-4458]
' 29-OCT-2017  LTC    [WIGOS-6134] Multisite Terminal meter adjustment
' 17-NOV-2017  LTC    Product Backlog Item 30849:Multisite: Reporte gen�rico terminales
' 21-NOV-2017  JML    Product Backlog Item 29621:AGG Data transfer [WIGOS-4458]
' 30-NOV-2017  RGR    PBI 30932:WIGOS-6393 GUI - Menu: User Interface improvements
' 14-DEC-2017  DPC    PBI 31072:[WIGOS-7111] AGG - EGM connection control
' 08-JAN-2018  RGR    PBI 30932:WIGOS-6393 GUI - Menu: User Interface improvements 
' 02-JUL-2018  FOS    Bug 33441:WIGOS-12444 [Ticket #14705] Bajar categor�a socio
' -------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports WSI.Common

Public Class frm_main_multisite
  Inherits GUI_Controls.frm_base_mdi

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub
  Friend WithEvents lbl_main_title As GUI_Controls.uc_text_field

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.lbl_main_title = New GUI_Controls.uc_text_field
    Me.SuspendLayout()
    '
    'lbl_main_title
    '
    Me.lbl_main_title.IsReadOnly = True
    Me.lbl_main_title.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_main_title.LabelForeColor = System.Drawing.Color.Blue
    Me.lbl_main_title.Location = New System.Drawing.Point(282, 19)
    Me.lbl_main_title.Name = "lbl_main_title"
    Me.lbl_main_title.Size = New System.Drawing.Size(66, 24)
    Me.lbl_main_title.SufixText = "Sufix Text"
    Me.lbl_main_title.SufixTextVisible = True
    Me.lbl_main_title.TabIndex = 1
    Me.lbl_main_title.Visible = False
    '
    'frm_main
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(496, 272)
    Me.Controls.Add(Me.lbl_main_title)
    Me.Name = "frm_main"
    Me.Text = "xWigos GUI"
    Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
    Me.Controls.SetChildIndex(Me.lbl_main_title, 0)
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Delegates "

  Private Delegate Sub DelegatesNoParams()

#End Region

#Region " Members "

#Region "Menu - System"

  Dim m_profiles_item As CLASS_MENU_ITEM
  Dim m_terminals_menu_item As CLASS_MENU_ITEM
  Dim m_general_params_item As CLASS_MENU_ITEM
  Dim m_cashier_configuration_item As CLASS_MENU_ITEM
  Dim m_svc_monitor As CLASS_MENU_ITEM
  Dim m_multisite_center_configuration As CLASS_MENU_ITEM
  Dim m_configuration_menu As CLASS_MENU_ITEM
  Dim m_providers_games As CLASS_MENU_ITEM
  Dim m_accounts_import As CLASS_MENU_ITEM
  Dim m_multi_currency As CLASS_MENU_ITEM
  Dim m_handpays As CLASS_MENU_ITEM
  Dim TerminalsTimeDisconectedMenuItem As CLASS_MENU_ITEM

  ' RRR 06-AUG-2013
  Dim m_money_laundering_config As CLASS_MENU_ITEM

  ' AMF 13-AUG-2013
  Dim m_player_edit_config As CLASS_MENU_ITEM

  ' JBC 17-SEP-2013: Points import.
  Dim m_points_import As CLASS_MENU_ITEM

  ' JMM 19-MAY-2014
  Dim m_patterns_sel As CLASS_MENU_ITEM

  ' DHA 01-DEC-2014
  Dim m_lcd_message_sel As CLASS_MENU_ITEM

  ' JRC 10-MAR-2016
  Dim m_bucket_sel As CLASS_MENU_ITEM

  ' ETP  31-MAR-2017
  Dim Creditline_menu As CLASS_MENU_ITEM
  Dim Form_Creditline_sel As CLASS_MENU_ITEM
  Dim Form_Creditline_report As CLASS_MENU_ITEM

  'RGR 29-NOV-2017
  Dim m_software As CLASS_MENU_ITEM

#End Region

#Region "Menu - Terminals"
  'LTC  29-OCT-2017
  Dim m_history_meters_edit As CLASS_MENU_ITEM
  Dim m_terminal_status As CLASS_MENU_ITEM

#End Region

#Region "Menu - Audit"

  Dim m_auditor_item As CLASS_MENU_ITEM
  Dim m_alarms As CLASS_MENU_ITEM
  Dim m_sites_alarms As CLASS_MENU_ITEM

#End Region

#Region "Menu - Statistics"

  Dim m_sales_date_terminal As CLASS_MENU_ITEM
  Dim m_terminal_sas_meters_sel As CLASS_MENU_ITEM

#End Region

#Region "Menu - Accounting"

  Private m_cash_summary As CLASS_MENU_ITEM
  Private m_sales_and_retirements_report As CLASS_MENU_ITEM
  ' JBM 18-DEC-2017
  Dim m_billing_menu As CLASS_MENU_ITEM
  Dim m_lottery_meters_adjustment_working_day As CLASS_MENU_ITEM
  Dim m_report_profit_day As CLASS_MENU_ITEM
  Dim m_report_profit_month As CLASS_MENU_ITEM
  Dim m_report_profit_machine_and_day As CLASS_MENU_ITEM

#End Region

#Region "Menu - Customers"

  Dim m_card_summary_item As CLASS_MENU_ITEM
  Dim m_card_movements_item As CLASS_MENU_ITEM
  Dim m_plays_sessions As CLASS_MENU_ITEM

#End Region

#Region "Menu - Monitor"

  Dim m_sites_status As CLASS_MENU_ITEM

#End Region

#Region "Menu - Jackpots Site"
  ' DCS 08-JAN-2015
  Dim m_site_jackpot_history As CLASS_MENU_ITEM

#End Region

#Region "Menu - SW DOWNLOAD"

  Dim m_sw_download_versions As CLASS_MENU_ITEM
  Dim m_misc_sw_build As CLASS_MENU_ITEM
  Dim m_licences As CLASS_MENU_ITEM

#End Region

#Region "Menu - Window"

  Dim m_window_menu As CLASS_WINDOW_MENU

#End Region

#End Region

#Region "Menu - Generic Report"

  'LTC 17-NOV-2017
  Dim m_generic_reports_system As CLASS_MENU_ITEM
  Dim m_generic_reports_terminals As CLASS_MENU_ITEM
  Dim m_generic_reports_audit As CLASS_MENU_ITEM
  Dim m_generic_reports_statistics As CLASS_MENU_ITEM
  Dim m_generic_reports_accounting As CLASS_MENU_ITEM
  Dim m_generic_reports_customers As CLASS_MENU_ITEM
  Dim m_generic_reports_monitor As CLASS_MENU_ITEM
  Dim m_generic_reports_jackpot As CLASS_MENU_ITEM
  Dim m_generic_reports_software As CLASS_MENU_ITEM
  Dim _obj_report_tool As GenericReport.IReportToolConfigService

#End Region

#Region " Must Override Methods: frm_base_mdi "

  ' PURPOSE: Set initial GUI permissions.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '       - Perm: Permissions set.
  '
  ' RETURNS:
  Protected Overrides Sub GUI_Permissions(ByRef Perm As GUI_Controls.CLASS_GUI_USER.TYPE_PERMISSIONS)
    Perm.Read = True
    Perm.Write = False
    Perm.Delete = False
    Perm.Execute = False

  End Sub ' GUI_Permissions

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_MAIN

    ' TJG 01-FEB-2005
    ' Set the form icon from the embedded resource (ICO file must be built in the project as "Embedded Resource")
    ' Call could be made as GetManifestResourceStream("GUI_Auditor.GUI_Auditor.ico"))

    '------------------------------------------------
    ' XVV 13/04/2007
    ' Translate tu BASE Form
    ' Me.Icon = New Icon(System.Reflection.Assembly.GetExecutingAssembly.GetManifestResourceStream(Me.GetType(), "WigosGUI.ico"))
    '------------------------------------------------

    '------------------------------------------------
    'XVV 13/04/2007
    'Call Base Form proc
    Call MyBase.GUI_SetFormId()
    '------------------------------------------------

  End Sub ' GUI_SetFormId

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()
    ' Required by the base class
    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_AUDITOR.GetString(87)

    ' Initialize control to keep form title. A control is used to manage event update
    lbl_main_title.Value = ""

    AddHandler WGDB.OnStatusChanged, AddressOf DatabaseStatusChanged
    ' RCI & MPO 02-APR-2012
    AddHandler Users.AutoLogout, AddressOf OnAutoLogout

    ' Add profiles for all forms on the GUI
    m_profiles_item.Status = CurrentUser.Permissions(m_profiles_item.FormId).Read

    If (Not CommonMultiSite.GetPlayerTrackingMode() = PlayerTracking_Mode.Site) Then
      ' SMN 03-MAY-2013
      m_cashier_configuration_item.Status = CurrentUser.Permissions(m_cashier_configuration_item.FormId).Read
    End If

    ' JAB 09-MAY-2013
    m_providers_games.Status = CurrentUser.Permissions(m_providers_games.FormId).Read

    m_general_params_item.Status = CurrentUser.Permissions(m_general_params_item.FormId).Read

    'LTC  29-OCT-2017
    If (WSI.Common.Misc.IsAGGEnabled()) Then
      m_history_meters_edit.Status = CurrentUser.Permissions(m_history_meters_edit.FormId).Read
      m_terminal_sas_meters_sel.Status = CurrentUser.Permissions(m_terminal_sas_meters_sel.FormId).Read
      m_terminal_status.Status = CurrentUser.Permissions(m_terminal_status.FormId).Read
      m_handpays.Status = CurrentUser.Permissions(m_handpays.FormId).Read
    End If

    m_auditor_item.Status = CurrentUser.Permissions(m_auditor_item.FormId).Read
    m_card_movements_item.Status = CurrentUser.Permissions(m_card_movements_item.FormId).Read
    m_card_summary_item.Status = CurrentUser.Permissions(m_card_summary_item.FormId).Read
    m_sites_status.Status = CurrentUser.Permissions(m_sites_status.FormId).Read
    m_multisite_center_configuration.Status = CurrentUser.Permissions(m_multisite_center_configuration.FormId).Read

    '' ANG 04-JUN-2013
    m_svc_monitor.Status = CurrentUser.Permissions(m_svc_monitor.FormId).Read

    m_accounts_import.Status = CurrentUser.Permissions(m_accounts_import.FormId).Read
    m_alarms.Status = CurrentUser.Permissions(m_alarms.FormId).Read

    m_sites_alarms.Status = CurrentUser.Permissions(m_sites_alarms.FormId).Read

    m_sales_date_terminal.Status = CurrentUser.Permissions(m_sales_date_terminal.FormId).Read

    m_cash_summary.Status = CurrentUser.Permissions(m_cash_summary.FormId).Read

    m_plays_sessions.Status = CurrentUser.Permissions(m_plays_sessions.FormId).Read


    m_sw_download_versions.Status = CurrentUser.Permissions(m_sw_download_versions.FormId).Read
    m_licences.Status = CurrentUser.Permissions(m_licences.FormId).Read
    m_misc_sw_build.Status = CurrentUser.Permissions(m_misc_sw_build.FormId).Read

    If (Not WSI.Common.CurrencyMultisite.IsEnabledMultiCurrency) Then
      ' RRR 06-AUG-2013
      m_money_laundering_config.Status = CurrentUser.Permissions(m_money_laundering_config.FormId).Read
    End If

    ' AMF 13-AUG-2013
    If (Not WSI.Common.CurrencyMultisite.IsEnabledMultiCurrency) Then
      m_player_edit_config.Status = CurrentUser.Permissions(m_player_edit_config.FormId).Read
    End If

    ' JBC 17-SEP-2013
    m_points_import.Status = CurrentUser.Permissions(m_points_import.FormId).Read

    ' JMM 19-MAY-2014
    ' JPJ 02-JUN-2014 They are only active when it's enabled
    If (GeneralParam.GetBoolean("Pattern", "Enabled", True)) Then
      m_patterns_sel.Status = CurrentUser.Permissions(m_patterns_sel.FormId).Read
    End If

    ' DHA 01-DEC-2014
    m_lcd_message_sel.Status = CurrentUser.Permissions(m_lcd_message_sel.FormId).Read

    ' DCS 08-JAN-2015
    m_site_jackpot_history.Status = CurrentUser.Permissions(m_site_jackpot_history.FormId).Read

    If (WSI.Common.CurrencyMultisite.IsEnabledMultiCurrency) Then
      ' ANM 24-APR-2015
      m_multi_currency.Status = CurrentUser.Permissions(m_multi_currency.FormId).Read
    End If

    ' ESE 20-OCT-2016 Add the read permissions
    If (GeneralParam.GetInt32("PlayerTracking.ExternalLoyaltyProgram", "Mode", 0) = 1) Then
      m_sales_and_retirements_report.Status = CurrentUser.Permissions(m_sales_and_retirements_report.FormId).Read
    End If

    If (GeneralParam.GetInt32("PlayerTracking.ExternalLoyaltyProgram", "Mode", 0) = 0) Then  'Buckets SPACE
      m_bucket_sel.Status = CurrentUser.Permissions(m_bucket_sel.FormId).Read
    End If

    If (GeneralParam.GetBoolean("CreditLine", "Enabled", False)) Then
      Form_Creditline_sel.Status = CurrentUser.Permissions(Form_Creditline_sel.FormId).Read

      Form_Creditline_report.Status = CurrentUser.Permissions(Form_Creditline_report.FormId).Read
    End If

    ' JBM 20-DEC-2017
    ' AGG Lottery
    If (WSI.Common.Misc.IsAGGEnabled()) Then
      m_lottery_meters_adjustment_working_day.Status = CurrentUser.Permissions(m_lottery_meters_adjustment_working_day.FormId).Read
      m_report_profit_day.Status = CurrentUser.Permissions(m_report_profit_day.FormId).Read
      m_report_profit_month.Status = CurrentUser.Permissions(m_report_profit_month.FormId).Read
      m_report_profit_machine_and_day.Status = CurrentUser.Permissions(m_report_profit_machine_and_day.FormId).Read
    End If

    'LTC 17-NOV-2017
    For Each _location_menu As GenericReport.LocationMenuMultiSite In [Enum].GetValues(GetType(GenericReport.LocationMenuMultiSite))
      If _obj_report_tool.ExistLocationMenu(_location_menu) Then
        Select Case _location_menu
          Case GenericReport.LocationMenuMultiSite.SystemMenu
            If Not m_generic_reports_system Is Nothing Then
              m_generic_reports_system.Status = CurrentUser.Permissions(m_generic_reports_system.FormId).Read
            End If
            If m_generic_reports_system.Status And Not _obj_report_tool.GetChildReportsPermissions(_location_menu) Then
              m_generic_reports_system.Status = False
            End If
          Case GenericReport.LocationMenuMultiSite.TerminalsMenu
            If Not m_generic_reports_terminals Is Nothing Then
              m_generic_reports_terminals.Status = CurrentUser.Permissions(m_generic_reports_terminals.FormId).Read
            End If
            If m_generic_reports_terminals.Status And Not _obj_report_tool.GetChildReportsPermissions(_location_menu) Then
              m_generic_reports_terminals.Status = False
            End If
          Case GenericReport.LocationMenuMultiSite.AuditMenu
            If Not m_generic_reports_audit Is Nothing Then
              m_generic_reports_audit.Status = CurrentUser.Permissions(m_generic_reports_audit.FormId).Read
            End If
            If m_generic_reports_audit.Status And Not _obj_report_tool.GetChildReportsPermissions(_location_menu) Then
              m_generic_reports_audit.Status = False
            End If
          Case GenericReport.LocationMenuMultiSite.StatisticsMenu
            If Not m_generic_reports_statistics Is Nothing Then
              m_generic_reports_statistics.Status = CurrentUser.Permissions(m_generic_reports_statistics.FormId).Read
            End If
            If m_generic_reports_statistics.Status And Not _obj_report_tool.GetChildReportsPermissions(_location_menu) Then
              m_generic_reports_statistics.Status = False
            End If
          Case GenericReport.LocationMenuMultiSite.AccountingMenu
            If Not m_generic_reports_accounting Is Nothing Then
              m_generic_reports_accounting.Status = CurrentUser.Permissions(m_generic_reports_accounting.FormId).Read
            End If
            If m_generic_reports_accounting.Status And Not _obj_report_tool.GetChildReportsPermissions(_location_menu) Then
              m_generic_reports_accounting.Status = False
            End If
          Case GenericReport.LocationMenuMultiSite.CustomerMenu
            If Not m_generic_reports_customers Is Nothing Then
              m_generic_reports_customers.Status = CurrentUser.Permissions(m_generic_reports_customers.FormId).Read
            End If
            If m_generic_reports_customers.Status And Not _obj_report_tool.GetChildReportsPermissions(_location_menu) Then
              m_generic_reports_customers.Status = False
            End If
          Case GenericReport.LocationMenuMultiSite.MonitorMenu
            If Not m_generic_reports_monitor Is Nothing Then
              m_generic_reports_monitor.Status = CurrentUser.Permissions(m_generic_reports_monitor.FormId).Read
            End If
            If m_generic_reports_monitor.Status And Not _obj_report_tool.GetChildReportsPermissions(_location_menu) Then
              m_generic_reports_monitor.Status = False
            End If
          Case GenericReport.LocationMenuMultiSite.JackpotsMenu
            If Not m_generic_reports_jackpot Is Nothing Then
              m_generic_reports_jackpot.Status = CurrentUser.Permissions(m_generic_reports_jackpot.FormId).Read
            End If
            If m_generic_reports_jackpot.Status And Not _obj_report_tool.GetChildReportsPermissions(_location_menu) Then
              m_generic_reports_jackpot.Status = False
            End If
        End Select
      End If
    Next

    m_window_menu.Status = True

  End Sub ' GUI_InitControls

  ' PURPOSE: First screen activation actions:
  '           - Set form title
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_FirstActivation()

    ' Retrieve Site Identifier and Name from database just once
    'GLB_SiteId = GetSiteId()
    'GLB_SiteName = GetSiteName()

    SetMainTitle()

    Me.KeyPreview = True

  End Sub ' GUI_FirstActivation

  Private Function GetEnterImage(ByVal menu As CLASS_MENU.ENUM_MENU_ICON_NAMES) As Image

    Return CLASS_GUI_RESOURCES.GetImage(String.Format("enter_{0}", menu.ToString.ToLower))

  End Function

  ' PURPOSE: Menu creation
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_CreateMenu()
    Dim _cls_menu As CLASS_MENU_ITEM
    Dim _import_menu As CLASS_MENU_ITEM
    Dim _cons_request As GenericReport.ConstructorRequest

    _cons_request = New GenericReport.ConstructorRequest()
    _cons_request.ProfileID = CurrentUser.ProfileId
    _cons_request.ShowAll = CurrentUser.IsSuperUser
    _cons_request.OnlyReportsMailing = False
    _cons_request.ModeType = GetVisibleProfileForms(CurrentUser.GuiId)

    _obj_report_tool = New GenericReport.ReportToolConfigService(_cons_request) 'Create new object and read the table REPORT_TOOL_CONFIG

    '
    'WIGOS_LOGO
    ' 
    _cls_menu = m_mnu.AddItem(String.Empty, ENUM_FORM.FORM_MAIN, , CLASS_GUI_RESOURCES.GetImage(CLASS_MENU.ENUM_MENU_ICON_NAMES.WIGOS_LOGO.ToString.ToLower), , True)

    '
    ' SYSTEM MENU
    '
    _cls_menu = m_mnu.AddItem(GLB_NLS_GUI_AUDITOR.Id(453), ENUM_FORM.FORM_MAIN, , CLASS_GUI_RESOURCES.GetImage(CLASS_MENU.ENUM_MENU_ICON_NAMES.SYSTEM.ToString.ToLower), , True)
    _cls_menu.AddEvents(GetEnterImage(CLASS_MENU.ENUM_MENU_ICON_NAMES.SYSTEM))
    m_profiles_item = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_CONFIGURATION.Id(467), ENUM_FORM.FORM_USER_PROFILE_SEL, AddressOf ClickProfiles)

    ' JAB 09-MAY-2013
    m_providers_games = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(1916), ENUM_FORM.FORM_PROVIDERS_GAMES_SEL, AddressOf ClickProvidersGames)

    ' DHA 01-DEC-2014
    m_lcd_message_sel = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(5770), ENUM_FORM.FORM_LCD_MESSAGE, AddressOf m_Click_lcd_message)

    m_configuration_menu = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(1063), ENUM_FORM.FORM_CASHIER_CONFIGURATION, Nothing)

    If (Not CommonMultiSite.GetPlayerTrackingMode() = PlayerTracking_Mode.Site) Then
      ' SMN 03-MAY-2013
      m_cashier_configuration_item = m_mnu.AddItem(m_configuration_menu, GLB_NLS_GUI_CONFIGURATION.Id(292), ENUM_FORM.FORM_CASHIER_CONFIGURATION, AddressOf CashierConfigurationClick)
    End If

    If (GeneralParam.GetInt32("PlayerTracking.ExternalLoyaltyProgram", "Mode", 0) = 0) Then  'Buckets SPACE
      m_bucket_sel = m_mnu.AddItem(m_configuration_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(6967), ENUM_FORM.FORM_BUCKET_SEL, AddressOf m_bucket_sel_click)
    End If

    m_multisite_center_configuration = m_mnu.AddItem(m_configuration_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(1907), ENUM_FORM.FORM_MULTISITE_CENTER_CONFIG, AddressOf SiteConfigurationClick)

    If (Not WSI.Common.CurrencyMultisite.IsEnabledMultiCurrency) Then
      ' RRR 06-AUG-2013
      m_money_laundering_config = m_mnu.AddItem(m_configuration_menu, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2399, Accounts.getAMLName()), ENUM_FORM.FORM_MONEY_LAUNDERING_CONFIG, AddressOf MoneyLaunderingConfigClick)
    End If

    ' AMF 13-AUG-2013
    If (Not WSI.Common.CurrencyMultisite.IsEnabledMultiCurrency) Then
      m_player_edit_config = m_mnu.AddItem(m_configuration_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(2478), ENUM_FORM.FORM_PLAYER_EDIT_CONFIG, AddressOf PlayerEditConfigClick)
    End If

    ' JMM 07-MAY-2014
    ' JPJ 02-JUN-2014 They are only active when it's enabled
    If (GeneralParam.GetBoolean("Pattern", "Enabled", True)) Then
      m_patterns_sel = m_mnu.AddItem(m_configuration_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(4924), ENUM_FORM.FORM_PATTERNS_SEL, AddressOf m_Patterns_click)
    End If

    If (WSI.Common.CurrencyMultisite.IsEnabledMultiCurrency) Then
      ' ANM 23-APR-2015
      m_multi_currency = m_mnu.AddItem(m_configuration_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(6239), ENUM_FORM.FORM_MULTI_CURRENCY, AddressOf Multisite_multicurrency_click)
    End If

    m_general_params_item = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_AUDITOR.Id(460), ENUM_FORM.FORM_GENERAL_PARAMS, AddressOf ClickGeneralParams)
    m_svc_monitor = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_SYSTEM_MONITOR.Id(451), ENUM_FORM.FORM_SERVICE_MONITOR, AddressOf SvcMonitorClick)
    _import_menu = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(1238), ENUM_FORM.FORM_CURRENCIES, Nothing)
    m_accounts_import = m_mnu.AddItem(_import_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(1237), ENUM_FORM.FORM_ACCOUNTS_IMPORT, AddressOf AccountsImportClick)

    m_mnu.AddBreak(_cls_menu)

    ' 
    ' SW DOWNLOAD MENU
    '
    m_software = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_SW_DOWNLOAD.Id(456), ENUM_FORM.FORM_MAIN)
    m_sw_download_versions = m_mnu.AddItem(m_software, GLB_NLS_GUI_SW_DOWNLOAD.Id(452), ENUM_FORM.FORM_SW_DOWNLOAD_VERSIONS, AddressOf SwDownloadVersionsClick)
    m_misc_sw_build = m_mnu.AddItem(m_software, GLB_NLS_GUI_SW_DOWNLOAD.Id(454), ENUM_FORM.FORM_MISC_SW_BY_BUILD, AddressOf MiscSwBuildClick)
    m_licences = m_mnu.AddItem(m_software, GLB_NLS_GUI_SW_DOWNLOAD.Id(455), ENUM_FORM.FORM_LICENCE_VERSIONS, AddressOf LicenceClick)

    m_mnu.AddBreak(_cls_menu)

    If Not GeneralParam.GetInt32("MultiSite", "PlayerTracking.Mode") = WSI.Common.PlayerTracking_Mode.Site Then
      m_points_import = m_mnu.AddItem(_import_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(205), ENUM_FORM.FORM_POINTS_IMPORT, AddressOf PointsImportClick)
    End If

    If GLB_StartedInSelectSiteMode Then
      m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(2051), 0, AddressOf m_DisconnectClick)
    End If

    m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_AUDITOR.Id(452), 0, AddressOf ExitClick)

    'LTC 17-NOV-2017
    'Check if we need to add a new item in the menu
    If _obj_report_tool.ExistLocationMenu(GenericReport.LocationMenuMultiSite.SystemMenu) Then
      m_mnu.AddBreak(_cls_menu)
      m_generic_reports_system = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(7333), ENUM_FORM.FORM_REPORT_TOOL_MULTISITE_SYSTEM_MENU, AddressOf m_report_tool_multisite_Click)
    End If

    '
    ' TERMINALS MENU
    '
    If (WSI.Common.Misc.IsAGGEnabled()) Then
      _cls_menu = m_mnu.AddItem(GLB_NLS_GUI_AUDITOR.Id(458), ENUM_FORM.FORM_MAIN, , CLASS_GUI_RESOURCES.GetImage(CLASS_MENU.ENUM_MENU_ICON_NAMES.TERMINALS.ToString.ToLower), , True)
      _cls_menu.AddEvents(GetEnterImage(CLASS_MENU.ENUM_MENU_ICON_NAMES.TERMINALS))

      m_terminal_sas_meters_sel = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(2973), ENUM_FORM.FORM_TERMINAL_SAS_METERS_SEL, AddressOf TerminalSasMetersClick)
      ' History meters edit
      'LTC  29-OCT-2017
      m_history_meters_edit = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(6144), ENUM_FORM.FORM_TERMINAL_SAS_METERS_EDIT, AddressOf HistoryMetersEditClick)
      'JML 20-NOV-2017
      ' Terminal status
      m_terminal_status = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_SW_DOWNLOAD.Id(453), ENUM_FORM.FORM_TERMINALS_STATUS, AddressOf m_terminal_status_Click)

      TerminalsTimeDisconectedMenuItem = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(8812), ENUM_FORM.FORM_TERMINALS_TIME_DISCONNECTED, AddressOf m_Click_Terminals_Time_Disconnected_View)
    End If

    'LTC 17-NOV-2017
    'Check if we need to add a new item in the menu
    If _obj_report_tool.ExistLocationMenu(GenericReport.LocationMenuMultiSite.TerminalsMenu) Then
      m_mnu.AddBreak(_cls_menu)
      m_generic_reports_terminals = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(7338), ENUM_FORM.FORM_REPORT_TOOL_MULTISITE_TERMINALS_MENU, AddressOf m_report_tool_multisite_Click)
    End If

    '
    ' AUDIT MENU
    '
    _cls_menu = m_mnu.AddItem(GLB_NLS_GUI_AUDITOR.Id(459), ENUM_FORM.FORM_MAIN, , CLASS_GUI_RESOURCES.GetImage(CLASS_MENU.ENUM_MENU_ICON_NAMES.AUDIT.ToString.ToLower), , True)
    _cls_menu.AddEvents(GetEnterImage(CLASS_MENU.ENUM_MENU_ICON_NAMES.AUDIT))
    m_auditor_item = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_AUDITOR.Id(451), ENUM_FORM.FORM_GUI_AUDITOR, AddressOf AuditorClick)

    'LTC 17-NOV-2017
    'Check if we need to add a new item in the menu
    If _obj_report_tool.ExistLocationMenu(GenericReport.LocationMenuMultiSite.AuditMenu) Then
      m_mnu.AddBreak(_cls_menu)
      m_generic_reports_audit = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(8775), ENUM_FORM.FORM_REPORT_TOOL_MULTISITE_AUDIT_MENU, AddressOf m_report_tool_multisite_Click)
    End If

    '
    ' EVENTS MENU
    '
    _cls_menu = m_mnu.AddItem(GLB_NLS_GUI_AUDITOR.Id(455), ENUM_FORM.FORM_MAIN, , CLASS_GUI_RESOURCES.GetImage(CLASS_MENU.ENUM_MENU_ICON_NAMES.EVENTS.ToString.ToLower), , True)
    _cls_menu.AddEvents(GetEnterImage(CLASS_MENU.ENUM_MENU_ICON_NAMES.EVENTS))
    m_alarms = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_ALARMS.Id(454), ENUM_FORM.FORM_ALARMS, AddressOf AlarmsClick)
    m_sites_alarms = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_ALARMS.Id(455), ENUM_FORM.FORM_SITES_ALARMS, AddressOf SitesAlarmsClick)

    '
    ' MONITOR MENU 
    '
    _cls_menu = m_mnu.AddItem(GLB_NLS_GUI_STATISTICS.Id(356), ENUM_FORM.FORM_MAIN, , CLASS_GUI_RESOURCES.GetImage(CLASS_MENU.ENUM_MENU_ICON_NAMES.MONITOR.ToString.ToLower), , True)
    _cls_menu.AddEvents(GetEnterImage(CLASS_MENU.ENUM_MENU_ICON_NAMES.MONITOR))

    m_sites_status = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(1760), ENUM_FORM.FORM_SITES_MONITOR, AddressOf SitesMonitorClick)

    'LTC 17-NOV-2017
    'Check if we need to add a new item in the menu
    If _obj_report_tool.ExistLocationMenu(GenericReport.LocationMenuMultiSite.MonitorMenu) Then
      m_mnu.AddBreak(_cls_menu)
      m_generic_reports_monitor = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(7339), ENUM_FORM.FORM_REPORT_TOOL_MULTISITE_MONITOR_MENU, AddressOf m_report_tool_multisite_Click)
    End If

    '
    ' STATISTICS MENU
    '
    _cls_menu = m_mnu.AddItem(GLB_NLS_GUI_STATISTICS.Id(201), ENUM_FORM.FORM_MAIN, , CLASS_GUI_RESOURCES.GetImage(CLASS_MENU.ENUM_MENU_ICON_NAMES.STATISTICS.ToString.ToLower), , True)
    _cls_menu.AddEvents(GetEnterImage(CLASS_MENU.ENUM_MENU_ICON_NAMES.STATISTICS))

    m_sales_date_terminal = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_INVOICING.Id(457), ENUM_FORM.FORM_DATE_TERMINAL, AddressOf SalesDateTerminalClick)

    'LTC 17-NOV-2017
    'Check if we need to add a new item in the menu
    If _obj_report_tool.ExistLocationMenu(GenericReport.LocationMenuMultiSite.StatisticsMenu) Then
      m_mnu.AddBreak(_cls_menu)
      m_generic_reports_statistics = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(7341), ENUM_FORM.FORM_REPORT_TOOL_MULTISITE_STATISTICS_MENU, AddressOf m_report_tool_multisite_Click)
    End If

    '
    ' ACCOUNTING MENU
    '
    ' JPJ 17-MAR-2014
    _cls_menu = m_mnu.AddItem(GLB_NLS_GUI_INVOICING.Id(451), ENUM_FORM.FORM_MAIN, , CLASS_GUI_RESOURCES.GetImage(CLASS_MENU.ENUM_MENU_ICON_NAMES.ACCOUNTING.ToString.ToLower), , True)
    _cls_menu.AddEvents(GetEnterImage(CLASS_MENU.ENUM_MENU_ICON_NAMES.ACCOUNTING))
    m_cash_summary = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(1632), ENUM_FORM.FORM_CASH_SUMMARY, AddressOf CashSummaryClick)

    ' JML 19-JUL-2016 : Bug 15673
    If (GeneralParam.GetInt32("PlayerTracking.ExternalLoyaltyProgram", "Mode", 0) = 1) Then
      m_sales_and_retirements_report = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(6737), ENUM_FORM.FORM_SALES_AND_RETIREMENTS_REPORT, AddressOf m_sales_and_retirements_report_Click)
    End If

    'LTC 17-NOV-2017
    'Check if we need to add a new item in the menu
    If _obj_report_tool.ExistLocationMenu(GenericReport.LocationMenuMultiSite.AccountingMenu) Then
      m_mnu.AddBreak(_cls_menu)
      m_generic_reports_accounting = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(7344), ENUM_FORM.FORM_REPORT_TOOL_MULTISITE_ACCOUNTING_MENU, AddressOf m_report_tool_multisite_Click)
    End If

    ' FGB 07-DEC-2017 Handpays
    If (WSI.Common.Misc.IsAGGEnabled()) Then
      m_handpays = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_INVOICING.Id(481), ENUM_FORM.FORM_HANDPAYS, AddressOf m_handpays_Click)
      m_billing_menu = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(8932), ENUM_FORM.FORM_MAIN, Nothing)
      m_lottery_meters_adjustment_working_day = m_mnu.AddItem(m_billing_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(8932), ENUM_FORM.FORM_LOTTERY_METERS_ADJUSTMENT_WORKING_DAY, AddressOf m_lottery_meters_adjustment_working_day_Click)
      m_mnu.AddBreak(m_billing_menu)
      m_report_profit_day = m_mnu.AddItem(m_billing_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(8955), ENUM_FORM.FORM_LOTTERY_REPORT_PROFIT_DAY, AddressOf m_report_profit_day_Click)
      m_report_profit_month = m_mnu.AddItem(m_billing_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(8956), ENUM_FORM.FORM_LOTTERY_REPORT_PROFIT_MONTH, AddressOf m_report_profit_month_Click)
      m_report_profit_machine_and_day = m_mnu.AddItem(m_billing_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(8976), ENUM_FORM.FORM_LOTTERY_REPORT_PROFIT_MACHINE_DAY, AddressOf m_report_profit_machine_and_day_Click)
    End If

    '
    ' CUSTOMER MENU 
    '
    _cls_menu = m_mnu.AddItem(GLB_NLS_GUI_AUDITOR.Id(472), ENUM_FORM.FORM_ACCOUNT_SUMMARY, , CLASS_GUI_RESOURCES.GetImage(CLASS_MENU.ENUM_MENU_ICON_NAMES.CUSTOMERS.ToString.ToLower), , True)
    _cls_menu.AddEvents(GetEnterImage(CLASS_MENU.ENUM_MENU_ICON_NAMES.CUSTOMERS))

    m_card_summary_item = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_INVOICING.Id(455), ENUM_FORM.FORM_ACCOUNT_SUMMARY, AddressOf CardSummaryClick)
    m_card_movements_item = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_INVOICING.Id(456), ENUM_FORM.FORM_ACCOUNT_MOVEMENTS, AddressOf CardMovementsClick)
    m_plays_sessions = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_STATISTICS.Id(357), ENUM_FORM.FORM_PLAYS_SESSIONS, AddressOf PlaysSessionsClick)

    If (GeneralParam.GetBoolean("CreditLine", "Enabled", False)) Then
      ' MS  14-MAR-2017
      m_mnu.AddBreak(_cls_menu)

      'Credit Line
      Creditline_menu = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(7945), ENUM_FORM.FORM_CAGE)

      ' Creditline Selection
      Form_Creditline_sel = m_mnu.AddItem(Creditline_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(7945), ENUM_FORM.FORM_CREDITLINE_SEL, AddressOf m_FormCreditlineSel_Click) 'AddressOf m_FormCreditlineSel_Click)
      Form_Creditline_report = m_mnu.AddItem(Creditline_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(7994), ENUM_FORM.FORM_CREDITLINE_MOVEMENTS, AddressOf m_FormCreditlineMovementReport_Click) 'AddressOf m_FormCreditlineMovementReport_Click)
    End If

    'LTC 17-NOV-2017
    'Check if we need to add a new item in the menu
    If _obj_report_tool.ExistLocationMenu(GenericReport.LocationMenuMultiSite.CustomerMenu) Then
      m_mnu.AddBreak(_cls_menu)
      m_generic_reports_customers = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(7345), ENUM_FORM.FORM_REPORT_TOOL_MULTISITE_CUSTOMER_MENU, AddressOf m_report_tool_multisite_Click)
    End If

    '
    ' SITE JACKPOT MENU 
    '
    ' DCS 08-JAN-2015
    _cls_menu = m_mnu.AddItem(GLB_NLS_GUI_PLAYER_TRACKING.Id(5849), ENUM_FORM.FORM_MAIN, , CLASS_GUI_RESOURCES.GetImage(CLASS_MENU.ENUM_MENU_ICON_NAMES.JACKPOT.ToString.ToLower), , True)
    _cls_menu.AddEvents(GetEnterImage(CLASS_MENU.ENUM_MENU_ICON_NAMES.JACKPOT))

    m_site_jackpot_history = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(5850), ENUM_FORM.FORM_SITE_JACKPOT_HISTORY, AddressOf SiteJackpotHistoryClick)

    'LTC 17-NOV-2017
    'Check if we need to add a new item in the menu
    If _obj_report_tool.ExistLocationMenu(GenericReport.LocationMenuMultiSite.JackpotsMenu) Then
      m_mnu.AddBreak(_cls_menu)
      m_generic_reports_jackpot = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(7347), ENUM_FORM.FORM_REPORT_TOOL_MULTISITE_JACKPOTS_MENU, AddressOf m_report_tool_multisite_Click)
    End If

    m_window_menu = New CLASS_WINDOW_MENU(AddressOf m_open_forms_menu_Click, AddressOf m_open_forms_item_Click, CLASS_GUI_RESOURCES.GetImage(CLASS_MENU.ENUM_MENU_ICON_NAMES.WINDOW.ToString.ToLower))
    m_mnu.AddPopupItem(m_window_menu)
    m_window_menu.AddEvents(GetEnterImage(CLASS_MENU.ENUM_MENU_ICON_NAMES.WINDOW))

    m_mnu.SetHeight(85)

  End Sub 'GUI_CreateMenu

  ' PURPOSE: GUI Exit actions.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_Exit()

    MyBase.GUI_Exit()

    'logout the user
    Call GLB_CurrentUser.Logout()

    ' Exit All threads ...
    Environment.Exit(0)

  End Sub ' GUI_Exit

  ' PURPOSE: Given a type, the function returns if it needs to be audited
  '
  '  PARAMS:
  '     - INPUT:
  '         - AuditType: AUDIT_FLAGS that wants to be audited
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Function GUI_HasToBeAudited(ByVal AuditType As AUDIT_FLAGS) As Boolean

    Return False

  End Function

#End Region

#Region " Private Functions "

  ' PURPOSE : Called when the database status has changed.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  ' NOTES:
  Private Sub DatabaseStatusChanged()

    Try
      If GLB_DbServerName <> WGDB.DataSource Then

        GLB_DbServerName = WGDB.DataSource

        If lbl_main_title.InvokeRequired Then
          Call Me.Invoke(New DelegatesNoParams(AddressOf ChangeMainTitle))
        End If
      End If
    Catch ex As Exception
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "frm_main", _
                            "DatabaseStatusChanged", _
                            "", _
                            "", _
                            "", _
                            ex.Message)
    End Try

  End Sub ' DatabaseStatusChanged

  ' PURPOSE : Build main form title.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub SetMainTitle()

    lbl_main_title.Value = GLB_NLS_GUI_AUDITOR.GetString(87) & Space(2) & "[" & WGDB.GetAppVersion() & "]"

    If CurrentUser.IsLogged Then
      lbl_main_title.Value = lbl_main_title.Value & " - " & CurrentUser.Name & "@" & GLB_DbServerName
      If GLB_DbServerName <> "" Then
        lbl_main_title.Value = lbl_main_title.Value & " - @" & GLB_DbServerName
      End If
    End If

    Me.Text = lbl_main_title.Value

  End Sub ' SetMainTitle

  ' PURPOSE : Modify main form title when a database status change.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub ChangeMainTitle()

    SetMainTitle()

  End Sub ' ChangeMainTitle

  Private Sub frm_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
    Dim _selector As frm_screen_selector

    Select Case e.KeyCode
      Case frm_screen_selector.HotKeyCode
        If e.Control AndAlso MdiChildren.Length > 0 Then
          _selector = New frm_screen_selector()
          Call _selector.Open(Me, Not e.Shift)
        End If

    End Select

  End Sub

#End Region

#Region " Menu Selection Events "

  Private Sub ClickProfiles(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_user_profiles_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor
    _frm = New frm_user_profiles_sel
    Call _frm.ShowForEdit(Me)
    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub ClickGeneralParams(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_genaral_params

    Windows.Forms.Cursor.Current = Cursors.WaitCursor
    _frm = New frm_genaral_params
    Call _frm.ShowForEdit(Me)
    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub AuditorClick(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_auditor

    Windows.Forms.Cursor.Current = Cursors.WaitCursor
    _frm = New frm_auditor
    Call _frm.ShowForEdit(Me)
    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  'LTC  29-OCT-2017
  Private Sub HistoryMetersEditClick(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_meters_history_edit

    Windows.Forms.Cursor.Current = Cursors.WaitCursor
    _frm = New frm_meters_history_edit
    Call _frm.ShowForEdit(Me)
    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_terminal_status_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_terminal_status

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_terminal_status()
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_DisconnectClick(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _is_closure_canceled As Boolean

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    '' Try to close all children forms.
    '' Use GUI_Closing instead of Close() to allow 
    '' to user cancell restart if has unsaved changes
    GUI_Closing(_is_closure_canceled)

    'XVV 16/04/2007
    'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
    Windows.Forms.Cursor.Current = Cursors.Default

    If Not _is_closure_canceled Then
      '' Dispose current form to avoid restart 
      '' two instances of the same app
      Call Me.Dispose()

      Call HtmlPrinter.Stop()
      '' uninterruptible threads :'(
      ''Call FtpVersions.Stop()
      ''Call GeneralParam.Stop()
      ''Call WGDB.Stop()
      Application.Restart()
    End If

  End Sub

  Private Sub ExitClick(ByVal sender As Object, ByVal e As System.EventArgs)

    Windows.Forms.Cursor.Current = Cursors.WaitCursor
    Me.Close()
    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub AlarmsClick(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_alarms

    Windows.Forms.Cursor.Current = Cursors.WaitCursor
    _frm = New frm_alarms
    _frm.ShowForEdit(Me)
    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub SitesAlarmsClick(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_alarms

    Windows.Forms.Cursor.Current = Cursors.WaitCursor
    _frm = New frm_alarms(ENUM_FORM.FORM_SITES_ALARMS)
    _frm.ShowForEdit(Me)
    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub CashSummaryClick(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_reports

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    frm = New frm_reports(ENUM_FORM.FORM_CASH_SUMMARY)

    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_sales_and_retirements_report_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_sales_and_retirements_report

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    frm = New frm_sales_and_retirements_report

    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub CashierConfigurationClick(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_cashier_configuration

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_cashier_configuration
    _frm.ShowEditItem(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_bucket_sel_click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_bucket_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_bucket_sel
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_FormCreditlineSel_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_creditline_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_creditline_sel()
    Call _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

    _frm = Nothing

  End Sub

  Private Sub m_FormCreditlineMovementReport_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_creditline_movements_report

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_creditline_movements_report()
    Call _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

    _frm = Nothing

  End Sub

  Private Sub CardSummaryClick(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_account_summary_multisite

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_account_summary_multisite
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub CardMovementsClick(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_account_movements_multisite

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_account_movements_multisite
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub SitesMonitorClick(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_sites_monitor

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_sites_monitor
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub SwDownloadVersionsClick(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_sw_package_versions

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_sw_package_versions
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub MiscSwBuildClick(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_misc_sw_version

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_misc_sw_version
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub LicenceClick(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_licence_versions

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_licence_versions
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub SvcMonitorClick(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_service_monitor

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_service_monitor
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub SiteConfigurationClick(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_multisite_center_configuration

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_multisite_center_configuration
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub SalesDateTerminalClick(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_sales_date_terminal_multisite

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_sales_date_terminal_multisite
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub TerminalSasMetersClick(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_terminal_sas_meters_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_terminal_sas_meters_sel()
    _frm.ShowForEdit(Me, frm_terminal_sas_meters_sel.ENUM_FORM_MODE.ADVANCED_MODE)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  Private Sub PlaysSessionsClick(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_plays_sessions_multisite

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_plays_sessions_multisite
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  ' PURPOSE: Create and show a form's instance.
  '
  '  PARAMS:
  '     - INPUT:
  '         - sender: Object.
  '         - e:      System.EventArgs.
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub ClickProvidersGames(ByVal sender As Object, ByVal e As System.EventArgs)
    ' JAB 30-MAY-2013
    Dim frm As frm_providers_sel_multisite

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance 
    frm = New frm_providers_sel_multisite()
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub AccountsImportClick(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_accounts_import

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_accounts_import
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  ' RRR 06-AUG-2013
  Private Sub MoneyLaunderingConfigClick(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As Object

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    If GeneralParam.GetBoolean("AntiMoneyLaundering", "ExternalControl.Enabled", False) Then
      frm = New frm_money_laundering_external_config()
    Else
      frm = New frm_money_laundering_config()
    End If

    frm.ShowEditItem(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  ' AMF 13-AUG-2013
  Private Sub PlayerEditConfigClick(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_player_edit_config

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_player_edit_config()

    frm.ShowEditItem(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  'JBC 17-09-2013
  Private Sub PointsImportClick(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_points_import

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_points_import
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  Private Sub m_Patterns_click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_patterns_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_patterns_sel()
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  Private Sub Multisite_multicurrency_click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_multisite_multicurrency

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_multisite_multicurrency()
    _frm.ShowEditItem(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  Private Sub m_Click_lcd_message(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_lcd_message_sel

    'XVV 16/04/2007
    'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance 
    frm = New frm_lcd_message_sel
    Call frm.ShowForEdit(Me)

    'XVV 16/04/2007
    'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  ' DCS 08-JAN-2015
  Private Sub SiteJackpotHistoryClick(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_site_jackpot_history

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_site_jackpot_history()
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  Private Sub m_open_forms_menu_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    m_window_menu.OnPopup(Me)
  End Sub

  Private Sub m_open_forms_item_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _tool_strip_item As ToolStripMenuItem
    Dim _tool_strip_menu_item As ToolStripMenuItem
    Dim _idx As Int32

    _tool_strip_menu_item = CType(sender, ToolStripMenuItem)
    _tool_strip_item = _tool_strip_menu_item.OwnerItem

    _idx = _tool_strip_item.DropDownItems.IndexOf(_tool_strip_menu_item)
    m_window_menu.OnClick(_idx, Me)
  End Sub

  'LTC 17-NOV-2017
  Private Sub m_report_tool_multisite_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    Dim _aux_title_form As String
    Dim _aux_enum_form As ENUM_FORM
    Dim _aux_tool_strip As System.Windows.Forms.ToolStripMenuItem
    Dim _aux_request As GenericReport.ReportToolConfigRequest
    Dim _frm As frm_generic_report_sel

    _aux_tool_strip = sender
    _aux_request = New GenericReport.ReportToolConfigRequest()
    _aux_title_form = ""

    Select Case _aux_tool_strip.Text
      Case NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(7333))
        _aux_request.LocationMenuMultiSite = GenericReport.LocationMenuMultiSite.SystemMenu
        _aux_title_form = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(7333))
        _aux_enum_form = ENUM_FORM.FORM_REPORT_TOOL_MULTISITE_SYSTEM_MENU

      Case NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(7338))
        _aux_request.LocationMenuMultiSite = GenericReport.LocationMenuMultiSite.TerminalsMenu
        _aux_title_form = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(7338))
        _aux_enum_form = ENUM_FORM.FORM_REPORT_TOOL_MULTISITE_TERMINALS_MENU

      Case NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(8775))
        _aux_request.LocationMenuMultiSite = GenericReport.LocationMenuMultiSite.AuditMenu
        _aux_title_form = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(8775))
        _aux_enum_form = ENUM_FORM.FORM_REPORT_TOOL_MULTISITE_AUDIT_MENU

      Case NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(7341))
        _aux_request.LocationMenuMultiSite = GenericReport.LocationMenuMultiSite.StatisticsMenu
        _aux_title_form = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(7341))
        _aux_enum_form = ENUM_FORM.FORM_REPORT_TOOL_MULTISITE_STATISTICS_MENU

      Case NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(7344))
        _aux_request.LocationMenuMultiSite = GenericReport.LocationMenuMultiSite.AccountingMenu
        _aux_title_form = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(7344))
        _aux_enum_form = ENUM_FORM.FORM_REPORT_TOOL_MULTISITE_ACCOUNTING_MENU

      Case NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(7345))
        _aux_request.LocationMenuMultiSite = GenericReport.LocationMenuMultiSite.CustomerMenu
        _aux_title_form = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(7345))
        _aux_enum_form = ENUM_FORM.FORM_REPORT_TOOL_MULTISITE_CUSTOMER_MENU

      Case NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(7339))
        _aux_request.LocationMenuMultiSite = GenericReport.LocationMenuMultiSite.MonitorMenu
        _aux_title_form = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(7339))
        _aux_enum_form = ENUM_FORM.FORM_REPORT_TOOL_MULTISITE_MONITOR_MENU

      Case NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(7347))
        _aux_request.LocationMenuMultiSite = GenericReport.LocationMenuMultiSite.JackpotsMenu
        _aux_title_form = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(7347))
        _aux_enum_form = ENUM_FORM.FORM_REPORT_TOOL_MULTISITE_JACKPOTS_MENU

    End Select

    _frm = New frm_generic_report_sel(_obj_report_tool.GetMenuConfiguration(_aux_request), _aux_enum_form)
    _frm.Text = _aux_title_form
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  Private Sub m_handpays_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_handpays

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    Try
      ' Create instance
      frm = New frm_handpays
      frm.ShowForEdit(Me)

    Finally
      Windows.Forms.Cursor.Current = Cursors.Default
    End Try
  End Sub

  Private Sub m_Click_Terminals_Time_Disconnected_View(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_terminals_time_disconnected

    Windows.Forms.Cursor.Current = Cursors.WaitCursor
    _frm = New frm_terminals_time_disconnected
    _frm.ShowForEdit(Me)
    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_lottery_meters_adjustment_working_day_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Try
      Dim _frm As frm_lottery_meters_adjustment_working_day
      Windows.Forms.Cursor.Current = Cursors.WaitCursor

      ' Create instance
      _frm = New frm_lottery_meters_adjustment_working_day
      Call _frm.ShowForEdit(Me)

      Windows.Forms.Cursor.Current = Cursors.Default
    Catch ex As Exception

    End Try
  End Sub

  Private Sub m_report_profit_day_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Try
      Dim _frm As frm_report_profit_day_and_month
      Windows.Forms.Cursor.Current = Cursors.WaitCursor

      ' Create instance
      _frm = New frm_report_profit_day_and_month(frm_report_profit_day_and_month.ENUM_TYPE_FORM.FOR_DAY)
      Call _frm.ShowForEdit(Me)

      Windows.Forms.Cursor.Current = Cursors.Default
    Catch ex As Exception

    End Try
  End Sub

  Private Sub m_report_profit_month_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Try
      Dim _frm As frm_report_profit_day_and_month
      Windows.Forms.Cursor.Current = Cursors.WaitCursor

      ' Create instance
      _frm = New frm_report_profit_day_and_month(frm_report_profit_day_and_month.ENUM_TYPE_FORM.FOR_MONTH)
      Call _frm.ShowForEdit(Me)

      Windows.Forms.Cursor.Current = Cursors.Default
    Catch ex As Exception

    End Try
  End Sub

  Private Sub m_report_profit_machine_and_day_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Try
      Dim _frm As frm_report_profit_machine_and_day
      Windows.Forms.Cursor.Current = Cursors.WaitCursor

      ' Create instance
      _frm = New frm_report_profit_machine_and_day
      Call _frm.ShowForEdit(Me)

      Windows.Forms.Cursor.Current = Cursors.Default
    Catch ex As Exception

    End Try
  End Sub

#End Region

#Region " Public Functions "

  Public Sub ShowMainWindow()

    Me.Display(True)

  End Sub

  Public Sub OnAutoLogout(ByVal Title As String, ByVal Message As String)

    If Me.InvokeRequired Then
      Me.Invoke(New Users.AutoLogoutEventHandler(AddressOf OnAutoLogout), New Object() {Title, Message})
    Else
      If frm_message.ShowTimer(Me, 60, Message, Title) = Windows.Forms.DialogResult.None Then

        Users.SetUserLoggedOff(Users.EXIT_CODE.EXPIRED)
        WSI.Common.Log.Close()
        Environment.Exit(0)
      Else
        WSI.Common.Users.SetLastAction("Continue", "")
      End If
    End If

  End Sub

#End Region

End Class