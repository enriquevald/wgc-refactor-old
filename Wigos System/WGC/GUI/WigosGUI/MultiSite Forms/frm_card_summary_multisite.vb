'-------------------------------------------------------------------
' Copyright � 2007-2009 Win Systems International Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_card_summary
' DESCRIPTION:   This screen allows to view cashier sessions:
'                           - blocked / non-blocked
'                           - for an account 
'                           - for a card number
' AUTHOR:        Agust� Poch
' CREATION DATE: 06-SEP-2007
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 06-SEP-2007  APB    Initial version
' 20-FEB-2009  RRT    Added new column into grid. (Balance/Blocked)
' 17-OCT-2011  JCA    Added new button (Adjust)
' 03-NOV-2011  RCI    Changed label "Cumplea�os" to "Fecha Nacimiento"
' 03-NOV-2011  RCI    Limit the number of rows to 10,000. With more than 13,750, the FlexGrid runs out of memory.
' 27-FEB-2012  JMM    Use non_redeemable2 when calculating account BalanceParts.
' 26-JUN-2012  JAB    Added AC_RE_BALANCE, AC_PROMO_RE_BALANCE and AC_PROMO_NR_BALANCE columns to GUI_FilterGetSqlQuery function.
' 29-JUN-2012  RXM    Added AC_RE_BALANCE and AC_PROMO_RE_BALANCE columns into grid as "Redimible" and "Promo redimible".
' 06-FEB-2013  RRB    Added new column into grid. (Redimir/Redeem)
' 07-FEB-2013  ICS    Added fields for extended search and new columns (phone1/ phone2)
' 12-FEB-2013  JMM    uc_account_sel.ValidateFormat added on FilterCheck
' 14-FEB-2013  ANG    Add support for account.ac_holder_wedding_date
' 06-MAR-2013  MPO    Copy frm_account_summary to frm_account_summary_multisite and move to MultiSite Forms
' 25-MAR-2013  ANG    Add Account points filter.
' 10-APR-2013  ANG    Fixed Bug #695 , #703 : Points filter
' 30-APR-2013  LEM    Fixed Bug #654: Change point search filter to non strict comparison
' 06-MAY-2013  ACC    Hide Player Tracking data if exists an External Loyalty 
' 17-MAY-2013  JBP    Delete BlockAccount and added RequestBlockAccount. RequestBlockAccount calls frm_account_block
' 21-MAY-2013  AMF    Fixed Bug #796: Remove Balance Filter from the impression
' 06-JUN-2013  JBP    Fixed Bug #829: Exception to lock if not selected rows
' 11-JUN-2013  RBG    Fixed Bug #834: Force format in excel columns
' 12-JUN-2013  RCI    Fixed Bug #843: Block account: No permission
' 19-JUN-2013  RBG    Fixed Bug #883: Correct column numbers to force format and correct column alignment. 
' 21-AUG-2013  JMM    Fixed Defect WIG-141: Several filters errors
' 26-SEP-2013  CCG    Added Multitype massive search
' 07-OCT-2013  CCG    Added last activity filters.
' 30-OCT-2013  JBC    Added new account control to GroupByMode.
' 06-MAY-2014  JBP    Added Vip client filter at reports.
' 22-OCT-2014  SMN    Added new functionality: if block reason is EXTERNAL_SYSTEM_CANCELED cannot unlock account
' 28-JAN-2015  DHA    In TITO mode anonymous accounts don't displayed
' 21-MAY-2015  RCI    Fixed Bug WIG-2375: Don't allow to unlock accounts if they are locked by External PLD.
' 21-AGO-2015  FJC    Product BackLog Item: 3702
' 13-JAN-2016  JML    Product Backlog Item 2541: MultiSite Multicurrency PHASE3: Loyalty program in the sites.
' 19-JUL-2016  JPJ    Bug 15603:MultiSite GUI - No results are shown after the search button is clicked
' 30-MAY-2017  JML    Bug 27551:Error en resumen de cuentas al marcar el check "Mostrar datos de buckets" en Multisite
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.Text
Imports WSI.Common

Public Class frm_account_summary_multisite
  Inherits GUI_Controls.frm_base_sel

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents fra_card_state As System.Windows.Forms.GroupBox
  Friend WithEvents chk_card_not_blocked As System.Windows.Forms.CheckBox
  Friend WithEvents uc_account_sel1 As GUI_Controls.uc_account_sel
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents gb_gender As System.Windows.Forms.GroupBox
  Friend WithEvents gb_level As System.Windows.Forms.GroupBox
  Friend WithEvents chk_level_04 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_level_03 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_level_02 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_level_01 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_gender_female As System.Windows.Forms.CheckBox
  Friend WithEvents chk_gender_male As System.Windows.Forms.CheckBox
  Friend WithEvents chk_only_anonymous As System.Windows.Forms.CheckBox
  Friend WithEvents gb_points As System.Windows.Forms.GroupBox
  Friend WithEvents ef_points_to As GUI_Controls.uc_entry_field
  Friend WithEvents ef_points_from As GUI_Controls.uc_entry_field
  Friend WithEvents gb_last_activity As System.Windows.Forms.GroupBox
  Friend WithEvents rb_todos As System.Windows.Forms.RadioButton
  Friend WithEvents rb_last_activity_by_date As System.Windows.Forms.RadioButton
  Friend WithEvents rb_last_activity_inactive As System.Windows.Forms.RadioButton
  Friend WithEvents rb_last_activity_active As System.Windows.Forms.RadioButton
  Protected WithEvents lbl_days_active As System.Windows.Forms.Label
  Friend WithEvents dtp_last_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_last_from As GUI_Controls.uc_date_picker
  Friend WithEvents uc_account_mov_sites_sel As GUI_Controls.uc_sites_sel
  Friend WithEvents chk_holder_data As System.Windows.Forms.CheckBox
  Friend WithEvents chk_show_bucket As System.Windows.Forms.CheckBox
  Friend WithEvents chk_card_blocked As System.Windows.Forms.CheckBox
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.fra_card_state = New System.Windows.Forms.GroupBox()
    Me.chk_card_not_blocked = New System.Windows.Forms.CheckBox()
    Me.chk_card_blocked = New System.Windows.Forms.CheckBox()
    Me.uc_account_sel1 = New GUI_Controls.uc_account_sel()
    Me.gb_date = New System.Windows.Forms.GroupBox()
    Me.dtp_to = New GUI_Controls.uc_date_picker()
    Me.dtp_from = New GUI_Controls.uc_date_picker()
    Me.gb_gender = New System.Windows.Forms.GroupBox()
    Me.chk_gender_female = New System.Windows.Forms.CheckBox()
    Me.chk_gender_male = New System.Windows.Forms.CheckBox()
    Me.gb_level = New System.Windows.Forms.GroupBox()
    Me.chk_level_04 = New System.Windows.Forms.CheckBox()
    Me.chk_level_03 = New System.Windows.Forms.CheckBox()
    Me.chk_level_02 = New System.Windows.Forms.CheckBox()
    Me.chk_level_01 = New System.Windows.Forms.CheckBox()
    Me.chk_only_anonymous = New System.Windows.Forms.CheckBox()
    Me.gb_points = New System.Windows.Forms.GroupBox()
    Me.ef_points_to = New GUI_Controls.uc_entry_field()
    Me.ef_points_from = New GUI_Controls.uc_entry_field()
    Me.gb_last_activity = New System.Windows.Forms.GroupBox()
    Me.uc_account_mov_sites_sel = New GUI_Controls.uc_sites_sel()
    Me.rb_todos = New System.Windows.Forms.RadioButton()
    Me.rb_last_activity_by_date = New System.Windows.Forms.RadioButton()
    Me.rb_last_activity_inactive = New System.Windows.Forms.RadioButton()
    Me.rb_last_activity_active = New System.Windows.Forms.RadioButton()
    Me.lbl_days_active = New System.Windows.Forms.Label()
    Me.dtp_last_to = New GUI_Controls.uc_date_picker()
    Me.dtp_last_from = New GUI_Controls.uc_date_picker()
    Me.chk_holder_data = New System.Windows.Forms.CheckBox()
    Me.chk_show_bucket = New System.Windows.Forms.CheckBox()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.fra_card_state.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.gb_gender.SuspendLayout()
    Me.gb_level.SuspendLayout()
    Me.gb_points.SuspendLayout()
    Me.gb_last_activity.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.AutoSize = True
    Me.panel_filter.Controls.Add(Me.chk_show_bucket)
    Me.panel_filter.Controls.Add(Me.chk_holder_data)
    Me.panel_filter.Controls.Add(Me.gb_last_activity)
    Me.panel_filter.Controls.Add(Me.uc_account_sel1)
    Me.panel_filter.Controls.Add(Me.gb_level)
    Me.panel_filter.Controls.Add(Me.gb_points)
    Me.panel_filter.Controls.Add(Me.fra_card_state)
    Me.panel_filter.Controls.Add(Me.gb_gender)
    Me.panel_filter.Controls.Add(Me.chk_only_anonymous)
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Size = New System.Drawing.Size(1208, 266)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_only_anonymous, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_gender, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.fra_card_state, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_points, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_level, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_account_sel1, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_last_activity, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_holder_data, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_show_bucket, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 270)
    Me.panel_data.Size = New System.Drawing.Size(1208, 337)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1202, 10)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1202, 4)
    '
    'fra_card_state
    '
    Me.fra_card_state.Controls.Add(Me.chk_card_not_blocked)
    Me.fra_card_state.Controls.Add(Me.chk_card_blocked)
    Me.fra_card_state.Location = New System.Drawing.Point(630, 187)
    Me.fra_card_state.Name = "fra_card_state"
    Me.fra_card_state.Size = New System.Drawing.Size(130, 56)
    Me.fra_card_state.TabIndex = 6
    Me.fra_card_state.TabStop = False
    Me.fra_card_state.Text = "xState"
    '
    'chk_card_not_blocked
    '
    Me.chk_card_not_blocked.Location = New System.Drawing.Point(12, 35)
    Me.chk_card_not_blocked.Name = "chk_card_not_blocked"
    Me.chk_card_not_blocked.Size = New System.Drawing.Size(109, 16)
    Me.chk_card_not_blocked.TabIndex = 1
    Me.chk_card_not_blocked.Text = "xNot Blocked"
    '
    'chk_card_blocked
    '
    Me.chk_card_blocked.Location = New System.Drawing.Point(12, 18)
    Me.chk_card_blocked.Name = "chk_card_blocked"
    Me.chk_card_blocked.Size = New System.Drawing.Size(109, 16)
    Me.chk_card_blocked.TabIndex = 0
    Me.chk_card_blocked.Text = "xBlocked"
    '
    'uc_account_sel1
    '
    Me.uc_account_sel1.Account = ""
    Me.uc_account_sel1.AccountText = ""
    Me.uc_account_sel1.Anon = False
    Me.uc_account_sel1.AutoSize = True
    Me.uc_account_sel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.uc_account_sel1.BirthDate = New Date(CType(0, Long))
    Me.uc_account_sel1.DisabledHolder = False
    Me.uc_account_sel1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.uc_account_sel1.Holder = ""
    Me.uc_account_sel1.Location = New System.Drawing.Point(6, 3)
    Me.uc_account_sel1.MassiveSearchNumbers = ""
    Me.uc_account_sel1.MassiveSearchNumbersToEdit = ""
    Me.uc_account_sel1.MassiveSearchType = 0
    Me.uc_account_sel1.Name = "uc_account_sel1"
    Me.uc_account_sel1.SearchTrackDataAsInternal = True
    Me.uc_account_sel1.ShowMassiveSearch = True
    Me.uc_account_sel1.ShowVipClients = True
    Me.uc_account_sel1.Size = New System.Drawing.Size(307, 134)
    Me.uc_account_sel1.TabIndex = 0
    Me.uc_account_sel1.Telephone = ""
    Me.uc_account_sel1.TrackData = ""
    Me.uc_account_sel1.Vip = False
    Me.uc_account_sel1.WeddingDate = New Date(CType(0, Long))
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Controls.Add(Me.dtp_from)
    Me.gb_date.Location = New System.Drawing.Point(763, 187)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(306, 56)
    Me.gb_date.TabIndex = 7
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xCreationDate"
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(70, 30)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(230, 24)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 1
    Me.dtp_to.TextWidth = 42
    Me.dtp_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(70, 8)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = True
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(230, 24)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 0
    Me.dtp_from.TextWidth = 42
    Me.dtp_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'gb_gender
    '
    Me.gb_gender.Controls.Add(Me.chk_gender_female)
    Me.gb_gender.Controls.Add(Me.chk_gender_male)
    Me.gb_gender.Location = New System.Drawing.Point(504, 187)
    Me.gb_gender.Name = "gb_gender"
    Me.gb_gender.Size = New System.Drawing.Size(122, 56)
    Me.gb_gender.TabIndex = 5
    Me.gb_gender.TabStop = False
    Me.gb_gender.Text = "xGender"
    '
    'chk_gender_female
    '
    Me.chk_gender_female.Location = New System.Drawing.Point(11, 35)
    Me.chk_gender_female.Name = "chk_gender_female"
    Me.chk_gender_female.Size = New System.Drawing.Size(109, 16)
    Me.chk_gender_female.TabIndex = 1
    Me.chk_gender_female.Text = "xWomen"
    '
    'chk_gender_male
    '
    Me.chk_gender_male.Location = New System.Drawing.Point(11, 18)
    Me.chk_gender_male.Name = "chk_gender_male"
    Me.chk_gender_male.Size = New System.Drawing.Size(109, 16)
    Me.chk_gender_male.TabIndex = 0
    Me.chk_gender_male.Text = "xMen"
    '
    'gb_level
    '
    Me.gb_level.Controls.Add(Me.chk_level_04)
    Me.gb_level.Controls.Add(Me.chk_level_03)
    Me.gb_level.Controls.Add(Me.chk_level_02)
    Me.gb_level.Controls.Add(Me.chk_level_01)
    Me.gb_level.Location = New System.Drawing.Point(319, 94)
    Me.gb_level.Name = "gb_level"
    Me.gb_level.Size = New System.Drawing.Size(179, 93)
    Me.gb_level.TabIndex = 2
    Me.gb_level.TabStop = False
    Me.gb_level.Text = "xLevel"
    '
    'chk_level_04
    '
    Me.chk_level_04.AutoSize = True
    Me.chk_level_04.Location = New System.Drawing.Point(11, 19)
    Me.chk_level_04.MaximumSize = New System.Drawing.Size(150, 17)
    Me.chk_level_04.Name = "chk_level_04"
    Me.chk_level_04.Size = New System.Drawing.Size(81, 17)
    Me.chk_level_04.TabIndex = 0
    Me.chk_level_04.Text = "xLevel 04"
    Me.chk_level_04.UseVisualStyleBackColor = True
    '
    'chk_level_03
    '
    Me.chk_level_03.AutoSize = True
    Me.chk_level_03.Location = New System.Drawing.Point(11, 36)
    Me.chk_level_03.MaximumSize = New System.Drawing.Size(150, 17)
    Me.chk_level_03.Name = "chk_level_03"
    Me.chk_level_03.Size = New System.Drawing.Size(81, 17)
    Me.chk_level_03.TabIndex = 1
    Me.chk_level_03.Text = "xLevel 03"
    Me.chk_level_03.UseVisualStyleBackColor = True
    '
    'chk_level_02
    '
    Me.chk_level_02.AutoSize = True
    Me.chk_level_02.Location = New System.Drawing.Point(11, 54)
    Me.chk_level_02.MaximumSize = New System.Drawing.Size(150, 17)
    Me.chk_level_02.Name = "chk_level_02"
    Me.chk_level_02.Size = New System.Drawing.Size(81, 17)
    Me.chk_level_02.TabIndex = 2
    Me.chk_level_02.Text = "xLevel 02"
    Me.chk_level_02.UseVisualStyleBackColor = True
    '
    'chk_level_01
    '
    Me.chk_level_01.AutoSize = True
    Me.chk_level_01.Location = New System.Drawing.Point(11, 71)
    Me.chk_level_01.MaximumSize = New System.Drawing.Size(150, 17)
    Me.chk_level_01.Name = "chk_level_01"
    Me.chk_level_01.Size = New System.Drawing.Size(81, 17)
    Me.chk_level_01.TabIndex = 3
    Me.chk_level_01.Text = "xLevel 01"
    Me.chk_level_01.UseVisualStyleBackColor = True
    '
    'chk_only_anonymous
    '
    Me.chk_only_anonymous.AutoSize = True
    Me.chk_only_anonymous.Location = New System.Drawing.Point(330, 190)
    Me.chk_only_anonymous.Name = "chk_only_anonymous"
    Me.chk_only_anonymous.Size = New System.Drawing.Size(130, 17)
    Me.chk_only_anonymous.TabIndex = 3
    Me.chk_only_anonymous.Text = "xOnly Anonymous"
    Me.chk_only_anonymous.UseVisualStyleBackColor = True
    '
    'gb_points
    '
    Me.gb_points.Controls.Add(Me.ef_points_to)
    Me.gb_points.Controls.Add(Me.ef_points_from)
    Me.gb_points.Location = New System.Drawing.Point(319, 6)
    Me.gb_points.Name = "gb_points"
    Me.gb_points.Size = New System.Drawing.Size(179, 80)
    Me.gb_points.TabIndex = 1
    Me.gb_points.TabStop = False
    Me.gb_points.Text = "xPoints"
    '
    'ef_points_to
    '
    Me.ef_points_to.DoubleValue = 0.0R
    Me.ef_points_to.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_points_to.IntegerValue = 0
    Me.ef_points_to.IsReadOnly = False
    Me.ef_points_to.Location = New System.Drawing.Point(6, 47)
    Me.ef_points_to.Margin = New System.Windows.Forms.Padding(3, 3, 3, 2)
    Me.ef_points_to.Name = "ef_points_to"
    Me.ef_points_to.OnlyUpperCase = True
    Me.ef_points_to.PlaceHolder = Nothing
    Me.ef_points_to.ShortcutsEnabled = True
    Me.ef_points_to.Size = New System.Drawing.Size(165, 25)
    Me.ef_points_to.SufixText = "xDesde"
    Me.ef_points_to.SufixTextVisible = True
    Me.ef_points_to.TabIndex = 1
    Me.ef_points_to.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_points_to.TextValue = ""
    Me.ef_points_to.TextWidth = 50
    Me.ef_points_to.Value = ""
    Me.ef_points_to.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_points_from
    '
    Me.ef_points_from.DoubleValue = 0.0R
    Me.ef_points_from.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_points_from.IntegerValue = 0
    Me.ef_points_from.IsReadOnly = False
    Me.ef_points_from.Location = New System.Drawing.Point(6, 19)
    Me.ef_points_from.Margin = New System.Windows.Forms.Padding(3, 3, 3, 2)
    Me.ef_points_from.Name = "ef_points_from"
    Me.ef_points_from.OnlyUpperCase = True
    Me.ef_points_from.PlaceHolder = Nothing
    Me.ef_points_from.ShortcutsEnabled = True
    Me.ef_points_from.Size = New System.Drawing.Size(165, 25)
    Me.ef_points_from.SufixText = "xDesde"
    Me.ef_points_from.SufixTextVisible = True
    Me.ef_points_from.TabIndex = 0
    Me.ef_points_from.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_points_from.TextValue = ""
    Me.ef_points_from.TextWidth = 50
    Me.ef_points_from.Value = ""
    Me.ef_points_from.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_last_activity
    '
    Me.gb_last_activity.Controls.Add(Me.uc_account_mov_sites_sel)
    Me.gb_last_activity.Controls.Add(Me.rb_todos)
    Me.gb_last_activity.Controls.Add(Me.rb_last_activity_by_date)
    Me.gb_last_activity.Controls.Add(Me.rb_last_activity_inactive)
    Me.gb_last_activity.Controls.Add(Me.rb_last_activity_active)
    Me.gb_last_activity.Controls.Add(Me.lbl_days_active)
    Me.gb_last_activity.Controls.Add(Me.dtp_last_to)
    Me.gb_last_activity.Controls.Add(Me.dtp_last_from)
    Me.gb_last_activity.Location = New System.Drawing.Point(504, 6)
    Me.gb_last_activity.Name = "gb_last_activity"
    Me.gb_last_activity.Size = New System.Drawing.Size(565, 179)
    Me.gb_last_activity.TabIndex = 4
    Me.gb_last_activity.TabStop = False
    Me.gb_last_activity.Text = "xLastActivity"
    '
    'uc_account_mov_sites_sel
    '
    Me.uc_account_mov_sites_sel.Location = New System.Drawing.Point(262, 8)
    Me.uc_account_mov_sites_sel.MultiSelect = True
    Me.uc_account_mov_sites_sel.Name = "uc_account_mov_sites_sel"
    Me.uc_account_mov_sites_sel.ShowIsoCode = False
    Me.uc_account_mov_sites_sel.ShowMultisiteRow = True
    Me.uc_account_mov_sites_sel.Size = New System.Drawing.Size(297, 166)
    Me.uc_account_mov_sites_sel.TabIndex = 6
    '
    'rb_todos
    '
    Me.rb_todos.AutoSize = True
    Me.rb_todos.Location = New System.Drawing.Point(14, 147)
    Me.rb_todos.Name = "rb_todos"
    Me.rb_todos.Size = New System.Drawing.Size(70, 17)
    Me.rb_todos.TabIndex = 5
    Me.rb_todos.Text = "x Todos"
    Me.rb_todos.UseVisualStyleBackColor = True
    '
    'rb_last_activity_by_date
    '
    Me.rb_last_activity_by_date.AutoSize = True
    Me.rb_last_activity_by_date.Location = New System.Drawing.Point(14, 73)
    Me.rb_last_activity_by_date.Name = "rb_last_activity_by_date"
    Me.rb_last_activity_by_date.Size = New System.Drawing.Size(74, 17)
    Me.rb_last_activity_by_date.TabIndex = 2
    Me.rb_last_activity_by_date.Text = "xByDate"
    Me.rb_last_activity_by_date.UseVisualStyleBackColor = True
    '
    'rb_last_activity_inactive
    '
    Me.rb_last_activity_inactive.AutoSize = True
    Me.rb_last_activity_inactive.Location = New System.Drawing.Point(14, 49)
    Me.rb_last_activity_inactive.Name = "rb_last_activity_inactive"
    Me.rb_last_activity_inactive.Size = New System.Drawing.Size(78, 17)
    Me.rb_last_activity_inactive.TabIndex = 1
    Me.rb_last_activity_inactive.Text = "xInactive"
    Me.rb_last_activity_inactive.UseVisualStyleBackColor = True
    '
    'rb_last_activity_active
    '
    Me.rb_last_activity_active.AutoSize = True
    Me.rb_last_activity_active.Checked = True
    Me.rb_last_activity_active.Location = New System.Drawing.Point(14, 25)
    Me.rb_last_activity_active.Name = "rb_last_activity_active"
    Me.rb_last_activity_active.Size = New System.Drawing.Size(67, 17)
    Me.rb_last_activity_active.TabIndex = 0
    Me.rb_last_activity_active.TabStop = True
    Me.rb_last_activity_active.Text = "xActive"
    Me.rb_last_activity_active.UseVisualStyleBackColor = True
    '
    'lbl_days_active
    '
    Me.lbl_days_active.AutoSize = True
    Me.lbl_days_active.ForeColor = System.Drawing.Color.Blue
    Me.lbl_days_active.Location = New System.Drawing.Point(137, 34)
    Me.lbl_days_active.Name = "lbl_days_active"
    Me.lbl_days_active.Size = New System.Drawing.Size(72, 13)
    Me.lbl_days_active.TabIndex = 7
    Me.lbl_days_active.Text = "xLastXdays"
    '
    'dtp_last_to
    '
    Me.dtp_last_to.Checked = True
    Me.dtp_last_to.IsReadOnly = False
    Me.dtp_last_to.Location = New System.Drawing.Point(15, 119)
    Me.dtp_last_to.Name = "dtp_last_to"
    Me.dtp_last_to.ShowCheckBox = True
    Me.dtp_last_to.ShowUpDown = False
    Me.dtp_last_to.Size = New System.Drawing.Size(240, 24)
    Me.dtp_last_to.SufixText = "Sufix Text"
    Me.dtp_last_to.SufixTextVisible = True
    Me.dtp_last_to.TabIndex = 4
    Me.dtp_last_to.TextWidth = 42
    Me.dtp_last_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_last_from
    '
    Me.dtp_last_from.Checked = False
    Me.dtp_last_from.IsReadOnly = False
    Me.dtp_last_from.Location = New System.Drawing.Point(15, 91)
    Me.dtp_last_from.Name = "dtp_last_from"
    Me.dtp_last_from.ShowCheckBox = False
    Me.dtp_last_from.ShowUpDown = False
    Me.dtp_last_from.Size = New System.Drawing.Size(240, 24)
    Me.dtp_last_from.SufixText = "Sufix Text"
    Me.dtp_last_from.SufixTextVisible = True
    Me.dtp_last_from.TabIndex = 3
    Me.dtp_last_from.TextWidth = 42
    Me.dtp_last_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'chk_holder_data
    '
    Me.chk_holder_data.AutoSize = True
    Me.chk_holder_data.Location = New System.Drawing.Point(330, 213)
    Me.chk_holder_data.Name = "chk_holder_data"
    Me.chk_holder_data.Size = New System.Drawing.Size(128, 17)
    Me.chk_holder_data.TabIndex = 12
    Me.chk_holder_data.Text = "xShowHolderData"
    Me.chk_holder_data.UseVisualStyleBackColor = True
    '
    'chk_show_bucket
    '
    Me.chk_show_bucket.AutoSize = True
    Me.chk_show_bucket.Location = New System.Drawing.Point(856, 246)
    Me.chk_show_bucket.Name = "chk_show_bucket"
    Me.chk_show_bucket.Size = New System.Drawing.Size(109, 17)
    Me.chk_show_bucket.TabIndex = 12
    Me.chk_show_bucket.Text = "xShowBuckets"
    Me.chk_show_bucket.UseVisualStyleBackColor = True
    '
    'frm_account_summary_multisite
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(1216, 611)
    Me.Name = "frm_account_summary_multisite"
    Me.Text = "frm_account_summary"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.fra_card_state.ResumeLayout(False)
    Me.gb_date.ResumeLayout(False)
    Me.gb_gender.ResumeLayout(False)
    Me.gb_level.ResumeLayout(False)
    Me.gb_level.PerformLayout()
    Me.gb_points.ResumeLayout(False)
    Me.gb_last_activity.ResumeLayout(False)
    Me.gb_last_activity.PerformLayout()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub

#End Region

#Region " Function Prototypes "


#End Region ' Function Prototypes

#Region " Constants "

  ' Num grid columns / headers
  Private Const GRID_HEADER_ROWS As Integer = 2

  Private Const SQL_COLUMN_CARD_ACCOUNT As Integer = 0
  Private Const SQL_COLUMN_CARD_TRACK As Integer = 1
  Private Const SQL_COLUMN_HOLDER_NAME As Integer = 2
  Private Const SQL_COLUMN_CARD_BLOCKED As Integer = 3

  Private Const SQL_COLUMN_RE_BALANCE As Integer = 4
  Private Const SQL_COLUMN_PROMO_RE_BALANCE As Integer = 5
  Private Const SQL_COLUMN_PROMO_NR_BALANCE As Integer = 6

  Private Const SQL_COLUMN_BALANCE_TOTAL As Integer = 7
  Private Const SQL_COLUMN_BALANCE_POINTS As Integer = 8
  Private Const SQL_COLUMN_CARD_LAST_ACTIVITY As Integer = 9
  Private Const SQL_COLUMN_CARD_CREATED As Integer = 10
  Private Const SQL_COLUMN_LEVEL As Integer = 11
  Private Const SQL_COLUMN_HOLDER_LEVEL As Integer = 12
  Private Const SQL_COLUMN_LEVEL_ENTERED As Integer = 13
  Private Const SQL_COLUMN_LEVEL_EXPIRATION As Integer = 14
  Private Const SQL_COLUMN_LEVEL_POINTS_STATUS As Integer = 15
  Private Const SQL_COLUMN_BLOCK_REASON As Integer = 16
  Private Const SQL_INIT_COLUMNS_HOLDER_DATA As Integer = 17

  ' Grid Columns
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_ACCOUNT As Integer = 1
  Private Const GRID_COLUMN_TRACK_DATA As Integer = 2
  Private Const GRID_COLUMN_HOLDER_NAME As Integer = 3
  Private Const GRID_COLUMN_CREATED As Integer = 4
  Private Const GRID_COLUMN_BLOCKED As Integer = 5
  Private Const GRID_COLUMN_LAST_ACTIVITY As Integer = 6
  Private Const GRID_COLUMN_BALANCE_POINTS As Integer = 7
  Private Const GRID_COLUMN_LEVEL As Integer = 8
  Private Const GRID_COLUMN_LEVEL_DAYS As Integer = 9
  Private Const GRID_COLUMN_LEVEL_ENTERED As Integer = 10
  Private Const GRID_COLUMN_LEVEL_EXPIRATION As Integer = 11
  Private Const GRID_COLUMN_LEVEL_POINTS_STATUS As Integer = 12

  Private Const GRID_COLUMN_RE_BALANCE As Integer = 13
  Private Const GRID_COLUMN_PROMO_RE_BALANCE As Integer = 14
  Private Const GRID_COLUMN_TOTAL_REDEEMABLE_BALANCE As Integer = 15
  Private Const GRID_COLUMN_PROMO_NR_BALANCE As Integer = 16
  Private Const GRID_COLUMN_TOTAL_BALANCE As Integer = 17

  Private Const GRID_COLUMN_BLOCKED_INT As Integer = 18

  Private Const GRID_COLUMN_BLOCK_REASON As Integer = 19

  Private Const GRID_COLUMN_INIT_HOLDER_DATA As Integer = 20
  Private Const GRID_COLUMNS As Integer = GRID_COLUMN_INIT_HOLDER_DATA + mdl_account_for_report.GRID_NUM_COLUMNS_HOLDER_DATA + 1

  '' HIDDEN COLUMN TO ALOW MODIFY USER
  Private Const GRID_COLUMN_HOLDER_LEVEL As Integer = GRID_COLUMN_INIT_HOLDER_DATA + mdl_account_for_report.GRID_NUM_COLUMNS_HOLDER_DATA

  ' Width
  Private Const GRID_WIDTH_INDEX As Integer = 150
  Private Const GRID_WIDTH_DATE As Integer = 1650
  Private Const GRID_WIDTH_DATE_SHORT As Integer = 1100
  Private Const GRID_WIDTH_ACCOUNT As Integer = 1150
  Private Const GRID_WIDTH_AMOUNT_SHORT As Integer = 1100
  Private Const GRID_WIDTH_AMOUNT_LONG As Integer = 1550
  Private Const GRID_WIDTH_CARD_HOLDER_NAME As Integer = 2500
  Private Const GRID_WIDTH_CARD_TRACK_DATA As Integer = 2300
  Private Const GRID_WIDTH_CARD_BLOCKED As Integer = 300
  Private Const GRID_WIDTH_POINTS As Integer = 800
  Private Const GRID_WIDTH_LEVEL As Integer = 1500
  Private Const GRID_WIDTH_HOLDER_GENDER As Integer = 770
  Private Const GRID_WIDTH_HOLDER_AGE As Integer = 480
  Private Const GRID_WIDTH_PHONE As Integer = 1450
  Private Const GRID_WIDTH_POINTS_STATUS As Integer = 1300

  ' Type of card for where clause
  Private Const CARD_TYPE As Integer = 2

  ' Text format columns
  Private Const EXCEL_COLUMN_TELEPHONE_NUMBER_1 As Integer = 17
  Private Const EXCEL_COLUMN_TELEPHONE_NUMBER_2 As Integer = 18
  Private Const EXCEL_COLUMN_POSTAL_CODE As Integer = 23

#End Region ' Constants

#Region " Members "

  ' For report filters 
  Private m_account As String
  Private m_track_data As String
  Private m_holder As String
  Private m_holder_is_vip As String
  Private m_holder_data_filter As String
  Private m_bucket_filter As String
  Private m_holder_data As Boolean
  Private m_bucket_data As Boolean

  Private m_date_from As String
  Private m_date_to As String
  Private m_date_last_from As String
  Private m_date_last_to As String
  Private m_points_from As String
  Private m_points_to As String

  Private m_level As String
  Private m_gender As String
  Private m_only_anonymous As String

  Private m_state As String
  Private m_sites As String

  Private m_birth_date As String
  Private m_wedding_date As String
  Private m_telephone As String

  ' For amount grid

  Private m_total_re_balance As Double
  Private m_total_promo_re_balance As Double
  Private m_total_total_redeemable_balance As Double
  Private m_total_promo_nr_balance As Double
  Private m_total_total_balance As Double

  Private m_total_balance_points As Double
  Private m_closing_time As Integer
  Private m_allow_account_update As Boolean

  Public m_days_without_activity As Int32
  Private m_allow_customize As Boolean

#End Region ' Members

#Region " OVERRIDES "

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_ACCOUNT_SUMMARY

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '

  Protected Overrides Sub GUI_InitControls()

    Dim _temp_point As Drawing.Point

    Call MyBase.GUI_InitControls()

    ' Account Summary
    Me.Text = GLB_NLS_GUI_INVOICING.GetString(229)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(3)

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_INFO).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_INFO).Text = GLB_NLS_GUI_INVOICING.GetString(2)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_INFO).Enabled = False

    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_ALARMS.GetString(301) ' "Bloquear"
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = True

    ' Accounts adjustment
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Text = GLB_NLS_GUI_CONFIGURATION.GetString(69)
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = False

    ' Edit Player Info
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Visible = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1712) ' Editar
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Enabled = False

    ' Creation Date
    Me.gb_date.Text = GLB_NLS_GUI_INVOICING.GetString(468)
    Me.dtp_from.Text = GLB_NLS_GUI_INVOICING.GetString(202)
    Me.dtp_to.Text = GLB_NLS_GUI_INVOICING.GetString(203)
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)

    ' Last Activity Date
    Me.gb_last_activity.Text = GLB_NLS_GUI_INVOICING.GetString(467)
    Me.dtp_last_from.Text = GLB_NLS_GUI_INVOICING.GetString(202)
    Me.dtp_last_to.Text = GLB_NLS_GUI_INVOICING.GetString(203)
    Me.dtp_last_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    Me.dtp_last_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    Me.rb_last_activity_active.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2424)
    Me.rb_last_activity_inactive.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2425)
    Me.rb_last_activity_by_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2427)

    m_days_without_activity = WSI.Common.GeneralParam.GetInt32("PlayerTracking", "MaxDaysWithoutActivity", 90)
    If m_days_without_activity = 0 Then
      m_days_without_activity = 90
    End If

    Me.lbl_days_active.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2428, m_days_without_activity.ToString())
    Me.rb_todos.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1353)

    'Sites
    Me.uc_account_mov_sites_sel.ShowMultisiteRow = False
    Call Me.uc_account_mov_sites_sel.Init()

    ' Gender
    Me.gb_gender.Text = GLB_NLS_GUI_INVOICING.GetString(403)
    Me.chk_gender_male.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(521)
    Me.chk_gender_female.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(522)

    ' Level
    gb_level.Text = GLB_NLS_GUI_INVOICING.GetString(381)
    chk_level_01.Text = GetCashierPlayerTrackingData("Level01.Name")
    If chk_level_01.Text = "" Then
      chk_level_01.Text = GLB_NLS_GUI_CONFIGURATION.GetString(289)
    End If
    chk_level_02.Text = GetCashierPlayerTrackingData("Level02.Name")
    If chk_level_02.Text = "" Then
      chk_level_02.Text = GLB_NLS_GUI_CONFIGURATION.GetString(290)
    End If
    chk_level_03.Text = GetCashierPlayerTrackingData("Level03.Name")
    If chk_level_03.Text = "" Then
      chk_level_03.Text = GLB_NLS_GUI_CONFIGURATION.GetString(291)
    End If
    chk_level_04.Text = GetCashierPlayerTrackingData("Level04.Name")
    If chk_level_04.Text = "" Then
      chk_level_04.Text = GLB_NLS_GUI_CONFIGURATION.GetString(332)
    End If

    If CommonMultiSite.GetPlayerTrackingMode() = PlayerTracking_Mode.Site Then
      gb_level.Visible = False
      gb_points.Visible = False

      Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = False

      _temp_point = chk_only_anonymous.Location
      _temp_point.Y -= (gb_level.Height + gb_points.Height)
      Me.chk_only_anonymous.Location = _temp_point

      _temp_point = chk_holder_data.Location
      _temp_point.Y -= (gb_level.Height + gb_points.Height)
      Me.chk_holder_data.Location = _temp_point

    End If

    ' Only anonymous
    chk_only_anonymous.Text = GLB_NLS_GUI_CONFIGURATION.GetString(66)
    If WSI.Common.TITO.Utils.IsTitoMode() Then
      Me.chk_only_anonymous.Visible = False
    End If

    ' Show Holder Data
    Me.chk_holder_data.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(552)

    ' Show buckets
    Me.chk_show_bucket.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6970)

    ' State
    Me.fra_card_state.Text = GLB_NLS_GUI_INVOICING.GetString(238)
    ' Blocked
    Me.chk_card_blocked.Text = GLB_NLS_GUI_INVOICING.GetString(238)
    ' Not Blocked
    Me.chk_card_not_blocked.Text = GLB_NLS_GUI_INVOICING.GetString(239)

    Me.m_closing_time = GetDefaultClosingTime()
    Me.m_allow_account_update = CurrentUser.Permissions(ENUM_FORM.FORM_ACCOUNT_ADJUSTMENT).Read _
                   And CurrentUser.Permissions(ENUM_FORM.FORM_ACCOUNT_ADJUSTMENT).Write

    ' Points
    Me.gb_points.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(205) ' Points
    Me.ef_points_from.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(297) '  From
    Me.ef_points_from.IsReadOnly = False
    Call Me.ef_points_to.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 8)
    Me.ef_points_to.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(298) '  To
    Me.ef_points_to.IsReadOnly = False
    Call Me.ef_points_from.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 8)



    ' Grid
    Call GUI_StyleSheet()

    ' ICS 07-FEB-2013: Add fields for extended search
    Me.uc_account_sel1.InitExtendedSearch()

    ' Set filter default values
    Call SetDefaultValues()

  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  Protected Overrides Sub GUI_BeforeFirstRow()

    m_total_balance_points = 0

    m_total_re_balance = 0
    m_total_promo_re_balance = 0
    m_total_total_redeemable_balance = 0
    m_total_promo_nr_balance = 0
    m_total_total_balance = 0

  End Sub ' GUI_BeforeFirsRow

  ' PURPOSE: Perform final processing for the grid data (totalisator row)
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:

  Protected Overrides Sub GUI_AfterLastRow()

    Me.Grid.AddRow()

    If (Me.Grid.NumRows > 0) Then
      GUI_RowSelectedEvent(0)
    Else
      GUI_RowSelectedEvent(-1)
    End If

    Dim idx_row As Integer = Me.Grid.NumRows - 1

    ' Label - TOTAL:
    Me.Grid.Cell(idx_row, GRID_COLUMN_ACCOUNT).Value = GLB_NLS_GUI_INVOICING.GetString(205)

    ' Balance - Points
    Me.Grid.Cell(idx_row, GRID_COLUMN_BALANCE_POINTS).Value = GUI_FormatNumber(m_total_balance_points, 0)
    ' Balance - redeemable
    Me.Grid.Cell(idx_row, GRID_COLUMN_RE_BALANCE).Value = GUI_MultiCurrencyValue_Format(m_total_re_balance, True)
    ' Balance - promo redeemable
    Me.Grid.Cell(idx_row, GRID_COLUMN_PROMO_RE_BALANCE).Value = GUI_MultiCurrencyValue_Format(m_total_promo_re_balance, True)
    ' Balance - Redeemable Total       
    Me.Grid.Cell(idx_row, GRID_COLUMN_TOTAL_REDEEMABLE_BALANCE).Value = GUI_MultiCurrencyValue_Format(m_total_total_redeemable_balance, True)
    ' Balance - Non-redeemable amount (PROMO_NR_BALANCE)
    Me.Grid.Cell(idx_row, GRID_COLUMN_PROMO_NR_BALANCE).Value = GUI_MultiCurrencyValue_Format(m_total_promo_nr_balance, True)
    ' Balance - Total
    Me.Grid.Cell(idx_row, GRID_COLUMN_TOTAL_BALANCE).Value = GUI_MultiCurrencyValue_Format(m_total_total_balance, True)

    Me.Grid.Row(idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)

    mdl_bucket_for_report.BucketsAfterLastRow(Me.Grid, idx_row, GRID_COLUMNS, True)
  End Sub ' GUI_AfterLastRow


  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted

  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Account selection
    If Not Me.uc_account_sel1.ValidateFormat() Then
      Return False
    End If

    ' Creation date selection 
    If Me.dtp_from.Checked And Me.dtp_to.Checked Then
      If Me.dtp_from.Value > Me.dtp_to.Value Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.dtp_to.Focus()

        Return False
      End If
    End If

    ' Last Activity date selection 
    If Me.dtp_last_from.Checked And Me.dtp_last_to.Checked Then
      If Me.dtp_last_from.Value > Me.dtp_last_to.Value Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.dtp_last_to.Focus()

        Return False
      End If
    End If

    If Me.ef_points_from.Value <> String.Empty And Me.ef_points_to.Value <> String.Empty Then
      If CInt(Me.ef_points_from.Value) > CInt(Me.ef_points_to.Value) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1896), _
                        ENUM_MB_TYPE.MB_TYPE_WARNING, _
                        ENUM_MB_BTN.MB_BTN_OK, _
                        ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                        GLB_NLS_GUI_PLAYER_TRACKING.GetString(377), _
                        GLB_NLS_GUI_PLAYER_TRACKING.GetString(378))

        Call Me.ef_points_from.Focus()

        Return False
      End If
    End If

    Return True
  End Function ' GUI_FilterCheck


  ' PURPOSE: Set a different value for the maximum number of rows that can be showed
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  Protected Overrides Function GUI_MaxRows() As Integer
    Return 10000
  End Function ' GUI_MaxRows

  ' PURPOSE : Manage permissions.
  '
  '  PARAMS :
  '     - INPUT :
  '           - AndPerm TYPE_PERMISSIONS
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_Permissions(ByRef AndPerm As GUI_Controls.CLASS_GUI_USER.TYPE_PERMISSIONS)

    MyBase.GUI_Permissions(AndPerm)

    ' GroupBox Permissions
    Me.chk_holder_data.Enabled = CurrentUser.Permissions(ENUM_FORM.FORM_SHOW_HOLDER_DATA).Read


  End Sub ' GUI_Permissions

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database

  Protected Overrides Function GUI_FilterGetSqlQuery() As String
    Dim _sb As StringBuilder

    _sb = New StringBuilder()

    ' CCG 23-SEP-2013: Account massive search
    If Not String.IsNullOrEmpty(Me.uc_account_sel1.MassiveSearchNumbers) Then
      _sb.AppendLine(Me.uc_account_sel1.CreateAndInsertAccountData())
    End If

    _sb.AppendLine("SELECT  DISTINCT AC_ACCOUNT_ID                                                                            ")
    _sb.AppendLine("       , AC_TRACK_DATA                                                                                    ")
    _sb.AppendLine("       , AC_HOLDER_NAME                                                                                   ")
    _sb.AppendLine("       , AC_BLOCKED                                                                                       ")
    _sb.AppendLine("       , AC_RE_BALANCE                                                                                    ")
    _sb.AppendLine("       , AC_PROMO_RE_BALANCE                                                                              ")
    _sb.AppendLine("       , AC_PROMO_NR_BALANCE                                                                              ")
    _sb.AppendLine("       , AC_BALANCE                                                                                       ")

    If m_bucket_data Then
      _sb.AppendLine("       , ISNULL([1], 0) AS CBU_VALUE              ")
    Else
      _sb.AppendLine("       , DBO.GETBUCKETVALUE(" & Buckets.BucketType.RedemptionPoints & ",AC_ACCOUNT_ID) AS CBU_VALUE ")
    End If

    _sb.AppendLine("       , AC_LAST_ACTIVITY                                                                                 ")
    _sb.AppendLine("       , AC_CREATED                                                                                       ")
    _sb.AppendLine("       , (SELECT   GP_KEY_VALUE                                                                           ")
    _sb.AppendLine("            FROM   GENERAL_PARAMS                                                                         ")
    _sb.AppendLine("           WHERE   GP_GROUP_KEY   = 'PlayerTracking'                                                      ")
    _sb.AppendLine("             AND   GP_SUBJECT_KEY = 'Level' + RIGHT('0' + CAST(AC_HOLDER_LEVEL AS NVARCHAR), 2) + '.Name' ")
    _sb.AppendLine("          ) AS AC_LEVEL                                                                                   ")
    _sb.AppendLine("      , AC_HOLDER_LEVEL                                                                                   ")
    _sb.AppendLine("      , AC_HOLDER_LEVEL_ENTERED                                                                           ")
    _sb.AppendLine("      , AC_HOLDER_LEVEL_EXPIRATION                                                                        ")
    _sb.AppendLine("      , AC_POINTS_STATUS                                                                                  ")
    _sb.AppendLine("      , AC_BLOCK_REASON                                                                                   ")
    _sb.AppendLine(mdl_account_for_report.AccountFieldsSql())
    _sb.AppendLine(mdl_bucket_for_report.BucketsFieldsSql(m_bucket_data))
    _sb.AppendLine("FROM ACCOUNTS                                                                                             ")
    _sb.AppendLine("LEFT JOIN CUSTOMER_BUCKET ON CBU_CUSTOMER_ID = AC_ACCOUNT_ID AND CBU_BUCKET_ID = " & Buckets.BucketType.RedemptionPoints & " ")

    _sb.AppendLine(mdl_bucket_for_report.BucketsJoinSql(m_bucket_data))


    If uc_account_sel1.BirthDateIsChecked And uc_account_sel1.BirthDateIsVisible Then
      _sb.AppendLine("WITH(INDEX(IX_ac_holder_birth_date)) ")
    End If

    ' CCG 23-SEP-2013: Account massive search
    If Not String.IsNullOrEmpty(Me.uc_account_sel1.MassiveSearchNumbers) Then
      If Me.uc_account_sel1.MassiveSearchType = GUI_Controls.uc_account_sel.ENUM_MASSIVE_SEARCH_TYPE.ID_ACCOUNT Then
        _sb.AppendLine(Me.uc_account_sel1.GetInnerJoinAccountMassiveSearch("AC_ACCOUNT_ID"))
      Else
        _sb.AppendLine(Me.uc_account_sel1.GetInnerJoinAccountMassiveSearch("AC_TRACK_DATA"))
      End If
    End If

    ' CCG 07-OCT-2013: Last activity filter
    If Not Me.uc_account_sel1.DisabledHolder Then
      If Me.rb_last_activity_active.Checked And Me.uc_account_mov_sites_sel.GetSitesIdListSelectedAll <> "" Then
        _sb.AppendLine("INNER JOIN LAST_ACCOUNT_ACTIVITY ON LAST_ACCOUNT_ACTIVITY.LAA_ACCOUNT_ID = AC_ACCOUNT_ID  ")
      End If
    End If

    _sb.AppendLine(Me.GetSqlWhere())
    _sb.AppendLine(Me.GetSqlWhereLastActivity())

    _sb.AppendLine(" ORDER BY AC_ACCOUNT_ID")

    'CCG 23-SEP-2013: Account massive search
    If Not String.IsNullOrEmpty(Me.uc_account_sel1.MassiveSearchNumbers) Then
      _sb.AppendLine(Me.uc_account_sel1.DropTableAccountMassiveSearch())
    End If

    Return _sb.ToString()
  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE : Sets the values of a row in the data grid
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the row could be added
  '     - False: the row could not be added

  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim _track_data As String

    Dim _promo_re_balance As Decimal
    Dim _re_balance As Decimal
    Dim _total_redeemable_balance As Decimal
    Dim _promo_nr_balance As Decimal

    Dim _permanency_days As TimeSpan
    Dim _now As Date
    Dim _date As Date
    Dim _base_sql_column_for_buckets As Integer

    _base_sql_column_for_buckets = 0


    ' JAB 26-JUN-2012: Get balance parts: Redeemable and NotRedeemable.
    _re_balance = DbRow.Value(SQL_COLUMN_RE_BALANCE)
    _promo_re_balance = DbRow.Value(SQL_COLUMN_PROMO_RE_BALANCE)
    _total_redeemable_balance = DbRow.Value(SQL_COLUMN_RE_BALANCE) + DbRow.Value(SQL_COLUMN_PROMO_RE_BALANCE)
    _promo_nr_balance = DbRow.Value(SQL_COLUMN_PROMO_NR_BALANCE)

    ' Card Account
    Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT).Value = DbRow.Value(SQL_COLUMN_CARD_ACCOUNT)

    ' Card Track Data
    If Not DbRow.IsNull(SQL_COLUMN_CARD_TRACK) Then
      ' Screen must always show the encrypted card number
      ' Internal Identifier must be converted prior to showing it
      _track_data = ""
      If WSI.Common.CardNumber.TrackDataToExternal(_track_data, DbRow.Value(SQL_COLUMN_CARD_TRACK), MAGNETIC_CARD_TYPES.CARD_TYPE_PLAYER) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_TRACK_DATA).Value = _track_data
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_TRACK_DATA).Value = "* * * * * * * *"
      End If
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TRACK_DATA).Value = ""
    End If

    ' Last Activity
    If Not DbRow.IsNull(SQL_COLUMN_CARD_LAST_ACTIVITY) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_LAST_ACTIVITY).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_CARD_LAST_ACTIVITY), _
                                                                               ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                               ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    ' Created
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CREATED).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_CARD_CREATED), _
                                                                       ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                       ENUM_FORMAT_TIME.FORMAT_HHMM)

    ' Holder Name
    If Not DbRow.IsNull(SQL_COLUMN_HOLDER_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_NAME).Value = DbRow.Value(SQL_COLUMN_HOLDER_NAME)
    End If

    ' Card State
    If DbRow.Value(SQL_COLUMN_CARD_BLOCKED) = 0 Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_BLOCKED).Value = "" 'GLB_NLS_GUI_INVOICING.GetString(239)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_BLOCKED).Value = GLB_NLS_GUI_INVOICING.GetString(419)
    End If

    Me.Grid.Cell(RowIndex, GRID_COLUMN_BLOCKED_INT).Value = DbRow.Value(SQL_COLUMN_CARD_BLOCKED)

    ' Level
    If Not DbRow.IsNull(SQL_COLUMN_LEVEL) Then

      Me.Grid.Cell(RowIndex, GRID_COLUMN_LEVEL).Value = DbRow.Value(SQL_COLUMN_LEVEL)

      ' Points
      Me.Grid.Cell(RowIndex, GRID_COLUMN_BALANCE_POINTS).Value = GUI_FormatNumber(Math.Truncate(DbRow.Value(SQL_COLUMN_BALANCE_POINTS)), _
                                                                                  0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      m_total_balance_points += Math.Truncate(DbRow.Value(SQL_COLUMN_BALANCE_POINTS))

      ' Level Entered date
      If Not DbRow.IsNull(SQL_COLUMN_LEVEL_ENTERED) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_LEVEL_ENTERED).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_LEVEL_ENTERED), _
                                                                                 ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                                 ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
      End If

      ' Level Expiration date
      If Not DbRow.IsNull(SQL_COLUMN_LEVEL_EXPIRATION) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_LEVEL_EXPIRATION).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_LEVEL_EXPIRATION), _
                                                                                    ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                                    ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
      End If

      ' Permanency days
      If Not DbRow.IsNull(SQL_COLUMN_LEVEL_ENTERED) Then
        _now = WSI.Common.Misc.TodayOpening
        _date = DbRow.Value(SQL_COLUMN_LEVEL_ENTERED)
        _date = New Date(_date.Year, _date.Month, _date.Day, Me.m_closing_time, 0, 0)
        _permanency_days = _now.Subtract(_date)
        Me.Grid.Cell(RowIndex, GRID_COLUMN_LEVEL_DAYS).Value = GUI_FormatNumber(Math.Max(0, Math.Truncate(_permanency_days.TotalHours / 24)), 0)
        'Me.Grid.Cell(RowIndex, GRID_COLUMN_LEVEL_DAYS).Value = GUI_FormatNumber(Math.Max(0, _permanency_days.Days), 0)
      End If

    End If ' If Not DbRow.IsNull(SQL_COLUMN_LEVEL)

    ' Points Status
    If Not DbRow.IsNull(SQL_COLUMN_LEVEL_POINTS_STATUS) Then
      Dim _status As WSI.Common.ACCOUNT_POINTS_STATUS
      _status = DbRow.Value(SQL_COLUMN_LEVEL_POINTS_STATUS)
      Select Case _status
        Case WSI.Common.ACCOUNT_POINTS_STATUS.REDEEM_ALLOWED
          Me.Grid.Cell(RowIndex, GRID_COLUMN_LEVEL_POINTS_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1643) ' Permitir / Allowed

        Case WSI.Common.ACCOUNT_POINTS_STATUS.REDEEM_NOT_ALLOWED
          Me.Grid.Cell(RowIndex, GRID_COLUMN_LEVEL_POINTS_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1644) ' No Permitir / Not Allowed

        Case Else
          Me.Grid.Cell(RowIndex, GRID_COLUMN_LEVEL_POINTS_STATUS).Value = ""

      End Select
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_LEVEL_POINTS_STATUS).Value = ""
    End If

    ' Balance - redeemable amount
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_REDEEMABLE_BALANCE).Value = GUI_MultiCurrencyValue_Format(_total_redeemable_balance, True)
    m_total_total_redeemable_balance += _total_redeemable_balance

    ' Balance - Non redeemable
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMO_NR_BALANCE).Value = GUI_MultiCurrencyValue_Format(_promo_nr_balance, True)
    m_total_promo_nr_balance += _promo_nr_balance

    ' promo redeemable
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMO_RE_BALANCE).Value = GUI_MultiCurrencyValue_Format(_promo_re_balance, True)
    m_total_promo_re_balance += _promo_re_balance

    ' Redeemable
    Me.Grid.Cell(RowIndex, GRID_COLUMN_RE_BALANCE).Value = GUI_MultiCurrencyValue_Format(_re_balance, True)
    m_total_re_balance += _re_balance

    ' Balance - Total
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_BALANCE).Value = GUI_MultiCurrencyValue_Format(DbRow.Value(SQL_COLUMN_BALANCE_TOTAL), True)
    m_total_total_balance += DbRow.Value(SQL_COLUMN_BALANCE_TOTAL)

    '' HOLDER DATA
    mdl_account_for_report.SetupRowHolderData(Me.Grid, DbRow, RowIndex, GRID_COLUMN_INIT_HOLDER_DATA, SQL_INIT_COLUMNS_HOLDER_DATA)

    _base_sql_column_for_buckets = SQL_INIT_COLUMNS_HOLDER_DATA + mdl_account_for_report.GRID_NUM_COLUMNS_HOLDER_DATA - 8
    ' 8 son las pseudo columnas de secondary address que son solo 1 en sql y 8 en grilla
    If m_bucket_data Then
      mdl_bucket_for_report.SetupRowBucketData(Me.Grid, DbRow, RowIndex, GRID_COLUMNS, _base_sql_column_for_buckets - 1)
    End If


    ' Holder level
    If Not DbRow.IsNull(SQL_COLUMN_HOLDER_LEVEL) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_LEVEL).Value = DbRow.Value(SQL_COLUMN_HOLDER_LEVEL)
    End If

    ' Block Reason
    If Not DbRow.IsNull(SQL_COLUMN_BLOCK_REASON) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_BLOCK_REASON).Value = DbRow.Value(SQL_COLUMN_BLOCK_REASON)
    End If

    If m_bucket_data Then
      mdl_bucket_for_report.UpdateTotalizers(DbRow, _base_sql_column_for_buckets - 1)
    End If
    Return True

  End Function ' GUI_SetupRow

  ' PURPOSE: Set focus to a control when first entering the form
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.uc_account_sel1

  End Sub ' GUI_SetInitialFocus

  ' PURPOSE: Set the tool tip text for a given row and column
  '
  '  PARAMS:
  '     - INPUT:
  '           - RowIndex As Integer
  '           - ColIndex As Integer
  '     - OUTPUT:
  '           - ToolTipTxt As String
  '
  ' RETURNS:

  Protected Overrides Sub GUI_SetToolTipText(ByVal RowIndex As Integer, _
                                             ByVal ColIndex As Integer, _
                                             ByRef ToolTipTxt As String)

    If RowIndex = -2 And ColIndex = GRID_COLUMN_LEVEL_DAYS Then
      ToolTipTxt = GLB_NLS_GUI_INVOICING.GetString(417)
    End If

    If ColIndex = GRID_COLUMN_BLOCKED Then
      If RowIndex = -2 Then
        ToolTipTxt = GLB_NLS_GUI_INVOICING.GetString(238)
      ElseIf IsValidDataRow(RowIndex) Then
        If Me.Grid.Cell(RowIndex, GRID_COLUMN_BLOCKED_INT).Value Then
          ToolTipTxt = GLB_NLS_GUI_INVOICING.GetString(238)
        Else
          ToolTipTxt = GLB_NLS_GUI_INVOICING.GetString(239)
        End If
      End If
    End If

  End Sub ' GUI_SetToolTipText

  ' PURPOSE: Process clicks on data grid (double-clicks) and select button
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId

      Case frm_base_sel.ENUM_BUTTON.BUTTON_FILTER_APPLY
        m_holder_data = chk_holder_data.Checked
        m_bucket_data = chk_show_bucket.Checked

        Call GUI_StyleSheet()

        Call MyBase.GUI_ButtonClick(ButtonId)

      Case frm_base_sel.ENUM_BUTTON.BUTTON_GRID_INFO
        Call GUI_ShowSelectedItem()

      Case frm_base_sel.ENUM_BUTTON.BUTTON_SELECT
        Call GUI_ShowSelectedItem()

      Case frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0
        Call RequestBlockAccount()

        ' JCA 17-OCT-2011
      Case frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1
        Call AccountAdjustment()

      Case frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2
        Call GUI_EditSelectedItem()

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)

    End Select

    'Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = m_allow_account_update

  End Sub ' GUI_ButtonClick

  ' PURPOSE: Enable button in selected row
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  Protected Overrides Sub GUI_RowSelectedEvent(ByVal SelectedRow As Integer)

    If Not IsValidDataRow(SelectedRow) Then
      Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = False
      Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = False
      Me.GUI_Button(ENUM_BUTTON.BUTTON_INFO).Enabled = False
      Me.GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Enabled = False
      Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Enabled = False

      Return
    Else
      Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = True
      Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = True
      Me.GUI_Button(ENUM_BUTTON.BUTTON_INFO).Enabled = True
      Me.GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Enabled = True
      Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Enabled = CurrentUser.Permissions(ENUM_FORM.FORM_PLAYER_EDIT).Read
    End If

    ' Get the complete account number and launch select form 
    If Me.Grid.Cell(SelectedRow, GRID_COLUMN_BLOCKED_INT).Value = False Then
      Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_ALARMS.GetString(301)
    Else
      Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_AUDITOR.GetString(467)
    End If

    ' Add condition to enable button that depends on External Loyalty
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = m_allow_account_update And Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled _
                                                         And CurrentUser.Permissions(ENUM_FORM.FORM_ACCOUNT_BLOCK).Read

  End Sub ' GUI_RowSelectedEvent

  Protected Overrides Sub GUI_EditSelectedItem()

    Dim _idx_row As Int32
    Dim _account_number As Int64
    Dim _frm As frm_player_edit

    ' Search the first row selected
    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then
        Exit For
      End If
    Next

    If Not IsValidDataRow(_idx_row) Then

      Return
    End If

    If Me.Grid.Cell(_idx_row, GRID_COLUMN_HOLDER_LEVEL).Value = 0 And m_allow_customize = False Then
      NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2839), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return
    End If

    ' Get the complete account number and launch select form 
    If Me.Grid.Cell(_idx_row, GRID_COLUMN_ACCOUNT).Value = "" Then
      _account_number = 0
    Else
      _account_number = Me.Grid.Cell(_idx_row, GRID_COLUMN_ACCOUNT).Value
    End If

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_player_edit

    _frm.ShowEditItem(_account_number)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  ' PURPOSE: Open additional form to show details for the select row
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Protected Overrides Sub GUI_ShowSelectedItem()

    Dim idx_row As Int32
    Dim account_number As Int64
    Dim _frm As frm_account_movements_multisite

    ' Search the first row selected
    For idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(idx_row).IsSelected Then
        Exit For
      End If
    Next

    If Not IsValidDataRow(idx_row) Then
      Return
    End If

    ' Get the complete account number and launch select form 
    If Me.Grid.Cell(idx_row, GRID_COLUMN_ACCOUNT).Value = "" Then
      account_number = 0
    Else
      account_number = Me.Grid.Cell(idx_row, GRID_COLUMN_ACCOUNT).Value
    End If

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_account_movements_multisite

    _frm.ShowForEdit(Me.MdiParent, account_number)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub ' GUI_ShowSelectedItem

#Region " GUI Reports "

  ' PURPOSE: Get report parameters and headers
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Protected Overrides Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA, Optional ByVal FirstColIndex As Integer = 0)
    With Me.Grid
      Call mdl_account_for_report.HolderColumnsPrintable(Me.Grid, GRID_COLUMNS, True)
      Call mdl_bucket_for_report.BucketsColumnsPrintable(Me.Grid, GRID_COLUMNS + mdl_bucket_for_report.GRID_NUM_COLUMNS_BUCKETS, True)
    End With

    Call MyBase.GUI_ReportParams(ExcelData)

    ' Set specific column formats.
    ExcelData.SetColumnFormat(EXCEL_COLUMN_TELEPHONE_NUMBER_1, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT)
    ExcelData.SetColumnFormat(EXCEL_COLUMN_TELEPHONE_NUMBER_2, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT)
    ExcelData.SetColumnFormat(EXCEL_COLUMN_POSTAL_CODE, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT)

  End Sub

  ' PURPOSE: Set form specific requirements/parameters forthe report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '           - FirstColIndex
  '     - OUTPUT:
  '
  ' RETURNS:

  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    With Me.Grid
      .Column(GRID_COLUMN_ACCOUNT).PrintWidth = GRID_WIDTH_ACCOUNT
      .Column(GRID_COLUMN_CREATED).PrintWidth = GRID_WIDTH_DATE
      .Column(GRID_COLUMN_LAST_ACTIVITY).PrintWidth = GRID_WIDTH_DATE
      .Column(GRID_COLUMN_BLOCKED).PrintWidth = GRID_WIDTH_CARD_BLOCKED
      .Column(GRID_COLUMN_LEVEL_ENTERED).PrintWidth = GRID_WIDTH_DATE_SHORT
      .Column(GRID_COLUMN_LEVEL_EXPIRATION).PrintWidth = GRID_WIDTH_DATE_SHORT

      .Column(GRID_COLUMN_TRACK_DATA).IsColumnPrintable = False
      .Column(GRID_COLUMN_LEVEL_POINTS_STATUS).IsColumnPrintable = False
      .Column(GRID_COLUMN_LEVEL_DAYS).IsColumnPrintable = False

      ' Trick to set visible Gender and Ages ( first two columns )  and hide the rest

      Call mdl_account_for_report.HolderColumnsPrintable(Me.Grid, GRID_COLUMN_INIT_HOLDER_DATA + 2, False)
      Call mdl_bucket_for_report.BucketsColumnsPrintable(Me.Grid, GRID_COLUMNS, False)

    End With

    Call MyBase.GUI_ReportParams(PrintData)

  End Sub ' GUI_ReportParams

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '
  ' RETURNS:

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(230), m_account)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(212), m_track_data)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(235), m_holder)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4802), m_holder_is_vip)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(552), m_holder_data)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6970), m_bucket_filter)

    If Me.uc_account_sel1.BirthDateIsVisible Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1651), m_birth_date)
    End If
    If Me.uc_account_sel1.WeddingDateIsVisible Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1652), m_wedding_date)
    End If
    If Me.uc_account_sel1.TelephoneIsVisible Then
      PrintData.SetFilter(GLB_NLS_GUI_CONTROLS.GetString(360), m_telephone)
    End If

    ' Points from - to
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(377), m_points_from)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(378), m_points_to)

    If Me.chk_only_anonymous.Checked Then
      PrintData.SetFilter(GLB_NLS_GUI_CONFIGURATION.GetString(66), m_only_anonymous)
    Else
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(381), m_level)
    End If

    If Me.rb_last_activity_active.Checked Then
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(467) & " ", GLB_NLS_GUI_PLAYER_TRACKING.GetString(2424))
    End If
    If Me.rb_last_activity_inactive.Checked Then
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(467) & " ", GLB_NLS_GUI_PLAYER_TRACKING.GetString(2425))
    End If
    If Me.rb_todos.Checked Then
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(467) & " ", GLB_NLS_GUI_PLAYER_TRACKING.GetString(1671))
    End If
    If Me.rb_last_activity_by_date.Checked Then
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(467) & " " & GLB_NLS_GUI_INVOICING.GetString(202), m_date_last_from)
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(467) & " " & GLB_NLS_GUI_INVOICING.GetString(203), m_date_last_to)
    End If

    If Not Me.chk_only_anonymous.Checked Then
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(403), m_gender)
    End If


    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(238), m_state)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1803), m_sites)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(468) & " " & GLB_NLS_GUI_INVOICING.GetString(202), m_date_from)

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(468) & " " & GLB_NLS_GUI_INVOICING.GetString(203), m_date_to)

    PrintData.FilterHeaderWidth(1) = 1000
    PrintData.FilterValueWidth(1) = 2300  '3000
    PrintData.FilterHeaderWidth(2) = 1800
    PrintData.FilterValueWidth(2) = 1400
    PrintData.FilterHeaderWidth(3) = 2000
    PrintData.FilterValueWidth(3) = 3300
    PrintData.FilterValueWidth(4) = 1900

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_date_from = ""
    m_date_to = ""
    m_date_last_from = ""
    m_date_last_to = ""

    m_level = ""
    m_gender = ""
    m_only_anonymous = ""

    m_sites = ""
    m_state = ""

    m_account = ""
    m_track_data = ""
    m_holder = ""
    m_holder_is_vip = ""
    m_birth_date = ""
    m_wedding_date = ""
    m_telephone = ""
    m_points_from = ""
    m_points_to = ""

    ' Card Account number
    m_account = Me.uc_account_sel1.Account()
    m_track_data = Me.uc_account_sel1.TrackData()
    m_holder = Me.uc_account_sel1.Holder()
    m_holder_is_vip = Me.uc_account_sel1.HolderIsVip()

    'Extended search
    If Me.uc_account_sel1.BirthDateIsChecked And Me.uc_account_sel1.BirthDateIsVisible Then
      m_birth_date = GUI_FormatDate(Me.uc_account_sel1.BirthDate, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT)
    End If
    If Me.uc_account_sel1.WeddingDateIsChecked And Me.uc_account_sel1.WeddingDateIsVisible Then
      m_wedding_date = GUI_FormatDate(Me.uc_account_sel1.WeddingDate, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT)
    End If
    If uc_account_sel1.TelephoneIsVisible Then
      m_telephone = uc_account_sel1.Telephone
    End If

    ' Date created
    If Me.dtp_from.Checked Then
      m_date_from = GUI_FormatDate(dtp_from.Value, _
                                   ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If
    If Me.dtp_to.Checked Then
      m_date_to = GUI_FormatDate(dtp_to.Value, _
                                 ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    ' Date last activity
    If Me.dtp_last_from.Checked Then
      m_date_last_from = GUI_FormatDate(dtp_last_from.Value, _
                                        ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If
    If Me.dtp_last_to.Checked Then
      m_date_last_to = GUI_FormatDate(dtp_last_to.Value, _
                                      ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    ' Level
    m_level = GetLevelsSelected(False)

    ' Gender
    m_gender = GetGendersSelected(False)

    ' Only anonymous
    m_only_anonymous = IIf(Me.chk_only_anonymous.Checked, GLB_NLS_GUI_PLAYER_TRACKING.GetString(359), GLB_NLS_GUI_PLAYER_TRACKING.GetString(360))

    ' Card state
    If Me.chk_card_blocked.Checked Then
      m_state = GLB_NLS_GUI_INVOICING.GetString(238)
    End If

    If Not String.IsNullOrEmpty(Me.uc_account_mov_sites_sel.GetSitesIdListSelected()) Then
      m_sites = Me.uc_account_mov_sites_sel.GetSitesIdListSelected()
    Else
      m_sites = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1337)
    End If

    If Me.chk_card_not_blocked.Checked Then
      If m_state.Length > 0 Then
        m_state = m_state & ", "
      End If
      m_state = m_state & GLB_NLS_GUI_INVOICING.GetString(239)
    End If

    ' Points
    If Me.ef_points_from.Value <> String.Empty Then
      m_points_from = Me.ef_points_from.Value
    End If

    If Me.ef_points_to.Value <> String.Empty Then
      m_points_to = Me.ef_points_to.Value
    End If

    ' Holder Data
    If Me.m_holder_data Then
      m_holder_data_filter = GLB_NLS_GUI_PLAYER_TRACKING.GetString(278)
    Else
      m_holder_data_filter = GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)
    End If

    If Me.m_bucket_data Then
      m_bucket_filter = GLB_NLS_GUI_PLAYER_TRACKING.GetString(278)
    Else
      m_bucket_filter = GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)
    End If
  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region ' Overrides

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Adjust accounts by points or level
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Private Sub AccountAdjustment()

    Dim _idx_row As Int32
    Dim _account_ids As New List(Of Int64)
    Dim _frm As frm_accounts_adjustment_multisite
    Dim _number_anonymous_accounts As Integer = 0

    If Not m_allow_account_update Then
      ' 109 "User doesn't have permission for this operation."
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(109), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)
      Return
    End If

    ' Search the rows selected
    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then
        ' Only not anonym accounts
        If Me.Grid.Cell(_idx_row, GRID_COLUMN_HOLDER_LEVEL).Value <> "" And Me.Grid.Cell(_idx_row, GRID_COLUMN_HOLDER_LEVEL).Value <> "0" Then
          _account_ids.Add(Me.Grid.Cell(_idx_row, GRID_COLUMN_ACCOUNT).Value)
        End If
      End If
    Next

    If _account_ids.Count < 1 Then
      Return
    End If

    ' Create instance
    _frm = New frm_accounts_adjustment_multisite
    _frm.ShowNewItem(_account_ids)

    ' Refresh the grid
    'Call MyBase.GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)

  End Sub ' GUI_AccountAdjustment

  ' PURPOSE: Request Block account
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub RequestBlockAccount()

    Dim idx_row As Int32
    Dim account_id As Int64
    Dim _frm_account_block As frm_account_block
    Dim blocked_new_value As Boolean
    Dim _block_reason_found As AccountBlockReason
    Dim _msg_id As Integer

    ' Search the first row selected
    For idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(idx_row).IsSelected Then
        Exit For
      End If
    Next

    If Not IsValidDataRow(idx_row) Then
      Return
    End If

    account_id = Me.Grid.Cell(idx_row, GRID_COLUMN_ACCOUNT).Value

    If Accounts.ExistsBlockReasonInMask(New List(Of AccountBlockReason)(New AccountBlockReason() _
                                                   {AccountBlockReason.EXTERNAL_SYSTEM_CANCELED, AccountBlockReason.EXTERNAL_AML}), _
                                    Accounts.ConvertOldBlockReason(CType(Me.Grid.Cell(idx_row, GRID_COLUMN_BLOCK_REASON).Value, Int32)), _
                                    _block_reason_found) Then

      _msg_id = 0
      Select Case _block_reason_found
        Case AccountBlockReason.EXTERNAL_SYSTEM_CANCELED
          _msg_id = GLB_NLS_GUI_PLAYER_TRACKING.Id(5648)
        Case AccountBlockReason.EXTERNAL_AML
          _msg_id = GLB_NLS_GUI_PLAYER_TRACKING.Id(6365)
      End Select

      If _msg_id > 0 Then
        Call NLS_MsgBox(_msg_id, ENUM_MB_TYPE.MB_TYPE_INFO)
      End If

      Return
    End If

    ' Create instance 
    _frm_account_block = New frm_account_block(account_id)
    _frm_account_block.Display(True)

    If _frm_account_block.DialogResult <> DialogResult.OK Then
      Return
    End If

    ' Get the complete account number and launch select form 
    If Me.Grid.Cell(idx_row, GRID_COLUMN_BLOCKED_INT).Value = True Then
      ' Unblock
      blocked_new_value = False
    Else
      'Block
      blocked_new_value = True
    End If

    ' Refresh grid
    Me.Grid.Cell(idx_row, GRID_COLUMN_BLOCKED_INT).Value = blocked_new_value

    If Not blocked_new_value Then
      Me.Grid.Cell(idx_row, GRID_COLUMN_BLOCKED).Value = "" ' GLB_NLS_GUI_INVOICING.GetString(239)
    Else
      Me.Grid.Cell(idx_row, GRID_COLUMN_BLOCKED).Value = GLB_NLS_GUI_INVOICING.GetString(419)
    End If

    ' Refresh button
    GUI_RowSelectedEvent(idx_row)

  End Sub


  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  Private Sub GUI_StyleSheet()

    'SGB 29-DEC-2015: Integrate buckets in grid
    mdl_bucket_for_report.CountColumnsBuckets(m_bucket_data)

    With Me.Grid
      Call .Init(GRID_COLUMNS + mdl_bucket_for_report.GRID_NUM_COLUMNS_BUCKETS, GRID_HEADER_ROWS)

      ' Index
      .Column(GRID_COLUMN_INDEX).Header(0).Text = ""
      .Column(GRID_COLUMN_INDEX).Header(1).Text = ""
      .Column(GRID_COLUMN_INDEX).Width = GRID_WIDTH_INDEX
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Account - Number
      '  Account
      .Column(GRID_COLUMN_ACCOUNT).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      ' Number
      .Column(GRID_COLUMN_ACCOUNT).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(231)
      .Column(GRID_COLUMN_ACCOUNT).Width = GRID_WIDTH_ACCOUNT
      .Column(GRID_COLUMN_ACCOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Account - Card
      ' Account
      .Column(GRID_COLUMN_TRACK_DATA).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      ' Card
      .Column(GRID_COLUMN_TRACK_DATA).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(212)
      .Column(GRID_COLUMN_TRACK_DATA).Width = GRID_WIDTH_CARD_TRACK_DATA
      .Column(GRID_COLUMN_TRACK_DATA).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Account - Holder Name
      ' Account
      .Column(GRID_COLUMN_HOLDER_NAME).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      ' Holder Name
      .Column(GRID_COLUMN_HOLDER_NAME).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(235)
      .Column(GRID_COLUMN_HOLDER_NAME).Width = GRID_WIDTH_CARD_HOLDER_NAME
      .Column(GRID_COLUMN_HOLDER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Account - Status (Blocked/Non Blocked)
      ' Account
      .Column(GRID_COLUMN_BLOCKED).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      ' Status
      .Column(GRID_COLUMN_BLOCKED).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(418)
      .Column(GRID_COLUMN_BLOCKED).Width = GRID_WIDTH_CARD_BLOCKED
      .Column(GRID_COLUMN_BLOCKED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' RCI 15-SEP-2010: If Width = 0, Header(1).Text must be "", else a strange char is showed in the column.
      '                  Also is necessary to define Header(0).
      .Column(GRID_COLUMN_BLOCKED_INT).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(240)
      .Column(GRID_COLUMN_BLOCKED_INT).Header(1).Text = ""
      .Column(GRID_COLUMN_BLOCKED_INT).Width = 0

      ' Account - Created
      ' Account
      .Column(GRID_COLUMN_CREATED).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      ' Created
      .Column(GRID_COLUMN_CREATED).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(468)
      .Column(GRID_COLUMN_CREATED).Width = GRID_WIDTH_DATE
      .Column(GRID_COLUMN_CREATED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Account - Last Activity
      ' Account
      .Column(GRID_COLUMN_LAST_ACTIVITY).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      ' Last Activity
      .Column(GRID_COLUMN_LAST_ACTIVITY).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(467)
      .Column(GRID_COLUMN_LAST_ACTIVITY).Width = GRID_WIDTH_DATE
      .Column(GRID_COLUMN_LAST_ACTIVITY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Player Tracking - Level
      ' Player Tracking
      .Column(GRID_COLUMN_LEVEL).Header(0).Text = GLB_NLS_GUI_CONFIGURATION.GetString(292)
      ' Level 
      .Column(GRID_COLUMN_LEVEL).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(381)
      .Column(GRID_COLUMN_LEVEL).Width = GRID_WIDTH_LEVEL
      .Column(GRID_COLUMN_LEVEL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Player Tracking - Points
      ' Player Tracking
      .Column(GRID_COLUMN_BALANCE_POINTS).Header(0).Text = GLB_NLS_GUI_CONFIGURATION.GetString(292)
      ' Points 
      .Column(GRID_COLUMN_BALANCE_POINTS).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(305)
      .Column(GRID_COLUMN_BALANCE_POINTS).Width = GRID_WIDTH_POINTS
      .Column(GRID_COLUMN_BALANCE_POINTS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Balance - reembled amount
      ' Balance
      .Column(GRID_COLUMN_TOTAL_REDEEMABLE_BALANCE).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(240)
      ' Blocked amount 
      .Column(GRID_COLUMN_TOTAL_REDEEMABLE_BALANCE).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(341)
      .Column(GRID_COLUMN_TOTAL_REDEEMABLE_BALANCE).Width = 1700
      .Column(GRID_COLUMN_TOTAL_REDEEMABLE_BALANCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      Call HideColumn(GRID_COLUMN_TOTAL_REDEEMABLE_BALANCE)

      ' Balance - Non-redeemable
      ' Balance
      .Column(GRID_COLUMN_PROMO_NR_BALANCE).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(240)
      ' Non-redeemable
      .Column(GRID_COLUMN_PROMO_NR_BALANCE).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(241)
      .Column(GRID_COLUMN_PROMO_NR_BALANCE).Width = 1600
      .Column(GRID_COLUMN_PROMO_NR_BALANCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      Call HideColumn(GRID_COLUMN_PROMO_NR_BALANCE)

      ' Balance - Promo-redeemable
      ' Balance
      .Column(GRID_COLUMN_PROMO_RE_BALANCE).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(240)
      ' Promo-Non-redeemable
      .Column(GRID_COLUMN_PROMO_RE_BALANCE).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(342)
      '.Column(GRID_COLUMN_PROMO_RE_BALANCE).Width = 1800
      .Column(GRID_COLUMN_PROMO_RE_BALANCE).WidthFixed = 0
      .Column(GRID_COLUMN_PROMO_RE_BALANCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      Call HideColumn(GRID_COLUMN_PROMO_RE_BALANCE)

      ' Balance - redeemable
      ' Balance
      .Column(GRID_COLUMN_RE_BALANCE).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(240)
      ' redeemable
      .Column(GRID_COLUMN_RE_BALANCE).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(337)
      'GLB_NLS_GUI_INVOICING.GetString(337)
      '.Column(GRID_COLUMN_RE_BALANCE).Width = GRID_WIDTH_AMOUNT_LONG
      .Column(GRID_COLUMN_RE_BALANCE).WidthFixed = 0
      .Column(GRID_COLUMN_RE_BALANCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      Call HideColumn(GRID_COLUMN_RE_BALANCE)

      ' Balance - Total
      ' Balance
      .Column(GRID_COLUMN_TOTAL_BALANCE).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(240)
      ' Total
      .Column(GRID_COLUMN_TOTAL_BALANCE).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(488)
      '.Column(GRID_COLUMN_TOTAL_BALANCE).Width = GRID_WIDTH_AMOUNT_LONG
      .Column(GRID_COLUMN_TOTAL_BALANCE).WidthFixed = 0
      .Column(GRID_COLUMN_TOTAL_BALANCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      Call HideColumn(GRID_COLUMN_TOTAL_BALANCE)

      ' Player Tracking - Holder Level Entered date
      ' Player Tracking
      .Column(GRID_COLUMN_LEVEL_ENTERED).Header(0).Text = GLB_NLS_GUI_CONFIGURATION.GetString(292)
      ' Holder Level Entered date
      .Column(GRID_COLUMN_LEVEL_ENTERED).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(412)
      .Column(GRID_COLUMN_LEVEL_ENTERED).Width = GRID_WIDTH_DATE_SHORT
      .Column(GRID_COLUMN_LEVEL_ENTERED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Player Tracking - Holder Level Expiration date
      ' Player Tracking
      .Column(GRID_COLUMN_LEVEL_EXPIRATION).Header(0).Text = GLB_NLS_GUI_CONFIGURATION.GetString(292)
      ' Holder Level Expiration date
      .Column(GRID_COLUMN_LEVEL_EXPIRATION).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(416)
      .Column(GRID_COLUMN_LEVEL_EXPIRATION).Width = GRID_WIDTH_DATE_SHORT
      .Column(GRID_COLUMN_LEVEL_EXPIRATION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      ' Points Status
      .Column(GRID_COLUMN_LEVEL_POINTS_STATUS).Header(0).Text = GLB_NLS_GUI_CONFIGURATION.GetString(292)
      .Column(GRID_COLUMN_LEVEL_POINTS_STATUS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1642) ' Redimir / Redeem
      .Column(GRID_COLUMN_LEVEL_POINTS_STATUS).Width = GRID_WIDTH_POINTS_STATUS
      .Column(GRID_COLUMN_LEVEL_POINTS_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Player Tracking - Holder Level permanency days
      ' Player Tracking
      .Column(GRID_COLUMN_LEVEL_DAYS).Header(0).Text = GLB_NLS_GUI_CONFIGURATION.GetString(292)
      ' Holder Level permanency days
      .Column(GRID_COLUMN_LEVEL_DAYS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(299)
      .Column(GRID_COLUMN_LEVEL_DAYS).Width = 540
      .Column(GRID_COLUMN_LEVEL_DAYS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      If CommonMultiSite.GetPlayerTrackingMode() = PlayerTracking_Mode.Site Then
        If Not chk_holder_data.Checked Then
          .Column(GRID_COLUMN_HOLDER_NAME).Width = 8700
        End If
        ' Player Tracking - Level
        Call HideColumn(GRID_COLUMN_LEVEL)
        ' Player Tracking - Points
        Call HideColumn(GRID_COLUMN_BALANCE_POINTS)
        ' Player Tracking - Holder Level Entered date
        Call HideColumn(GRID_COLUMN_LEVEL_ENTERED)
        ' Player Tracking - Holder Level Expiration date
        Call HideColumn(GRID_COLUMN_LEVEL_EXPIRATION)
        ' Points Status
        Call HideColumn(GRID_COLUMN_LEVEL_POINTS_STATUS)
        ' Player Tracking - Holder Level permanency days
        Call HideColumn(GRID_COLUMN_LEVEL_DAYS)
      End If

      ' Account holder Data
      Call mdl_account_for_report.StyleSheetHolderData(Grid, GRID_COLUMN_INIT_HOLDER_DATA, , m_holder_data)

      If m_bucket_data Then
        Call mdl_bucket_for_report.StyleSheetBucketData(Grid, GRID_COLUMNS, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6968), m_bucket_data)
      End If

      '' Holder Data- Level
      '' Holder Data
      .Column(GRID_COLUMN_HOLDER_LEVEL).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(420)
      .Column(GRID_COLUMN_HOLDER_LEVEL).Header(1).Text = ""
      .Column(GRID_COLUMN_HOLDER_LEVEL).Width = 0

      ' Block Reason
      .Column(GRID_COLUMN_BLOCK_REASON).IsColumnPrintable = False
      .Column(GRID_COLUMN_BLOCK_REASON).Width = 0

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Private Sub SetDefaultValues()
    Dim initial_time As Date
    Dim _select_all_sites As Boolean

    initial_time = WSI.Common.Misc.TodayOpening()
    _select_all_sites = True

    Me.dtp_from.Value = initial_time
    Me.dtp_from.Checked = False
    Me.dtp_to.Value = Me.dtp_from.Value.AddDays(1)
    Me.dtp_to.Checked = False

    Me.dtp_last_from.Value = initial_time.AddMonths(-1)
    Me.dtp_last_from.Checked = False
    Me.dtp_last_to.Value = initial_time.AddDays(-1)
    Me.dtp_last_to.Checked = False

    Me.chk_card_blocked.Checked = False
    Me.chk_card_not_blocked.Checked = False
    Me.uc_account_sel1.Clear()

    Me.chk_level_01.Checked = False
    Me.chk_level_02.Checked = False
    Me.chk_level_03.Checked = False
    Me.chk_level_04.Checked = False

    Me.chk_gender_male.Checked = False
    Me.chk_gender_female.Checked = False

    Me.chk_only_anonymous.Checked = False

    Me.ef_points_from.Value = String.Empty
    Me.ef_points_to.Value = String.Empty

    Me.rb_last_activity_active.Checked = True

    Me.m_allow_customize = IIf(GeneralParam.GetInt32("AntiMoneyLaundering", "GroupByMode", 0) = 1, False, True)

    Me.chk_show_bucket.Checked = False

    Call Me.uc_account_mov_sites_sel.SetDefaultValues(_select_all_sites)

  End Sub ' SetDefaultValues

  ' PURPOSE: Build the variable part of the WHERE clause (the one that depends on filter values) for the main SQL Query
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Private Function GetSqlWhere() As String
    Dim str_where As String = ""
    Dim str_where_account As String = ""
    Dim _date As Date

    _date = WSI.Common.Misc.TodayOpening()

    str_where = " WHERE AC_TYPE = " & CARD_TYPE
    str_where_account = Me.uc_account_sel1.GetFilterSQL()

    If str_where_account <> "" Then
      str_where = str_where & " AND " & str_where_account
    End If

    If Not Me.uc_account_sel1.DisabledHolder Then
      ' Filter Dates - Creation
      If Me.dtp_from.Checked = True Then
        str_where = str_where & " AND (AC_CREATED >= " & GUI_FormatDateDB(dtp_from.Value) & ") "
      End If

      If Me.dtp_to.Checked = True Then
        str_where = str_where & " AND (AC_CREATED < " & GUI_FormatDateDB(dtp_to.Value) & ") "
      End If

      ' Level
      If Me.chk_only_anonymous.Checked Then
        ' Only anonym accounts
        str_where = str_where & " AND ( AC_HOLDER_LEVEL = 0 )"
      Else
        ' Any kind of accounts
        If Me.chk_level_01.Checked Or _
           Me.chk_level_02.Checked Or _
           Me.chk_level_03.Checked Or _
           Me.chk_level_04.Checked Then

          str_where = str_where & " AND AC_HOLDER_LEVEL IN (" & GetLevelsSelected() & ") "
        End If

        ' Gender
        If Me.chk_gender_male.Checked Or _
           Me.chk_gender_female.Checked Then

          str_where = str_where & " AND AC_HOLDER_GENDER IN (" & GetGendersSelected() & ") "
        End If
      End If

      ' Filter State - Blocked 
      If Me.chk_card_blocked.Checked And Not Me.chk_card_not_blocked.Checked Then
        str_where = str_where & " AND (AC_BLOCKED = 1) "
      End If

      ' Filter State - Not Blocked 
      If Me.chk_card_not_blocked.Checked And Not Me.chk_card_blocked.Checked Then
        str_where = str_where & " AND (AC_BLOCKED = 0) "
      End If

      ' Points filter
      If Me.ef_points_from.Value <> String.Empty Then
        str_where += " AND AC_HOLDER_LEVEL > 0 AND FLOOR(ISNULL(AC_POINTS,0)) >= " & Me.ef_points_from.Value   '' Integer value don't work!!!
      End If

      If Me.ef_points_to.Value <> String.Empty Then
        str_where += " AND AC_HOLDER_LEVEL > 0 AND FLOOR(ISNULL(AC_POINTS,0)) <= " & Me.ef_points_to.Value
      End If

    End If

    Return str_where

  End Function ' GetSqlWhere

  ' PURPOSE: Checks whether the specified row contains data or not
  '
  '  PARAMS:
  '     - INPUT:
  '           - IdxRow: row to check
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: selected row contains data
  '     - FALSE: selected row does not exist or contains no data

  Private Function IsValidDataRow(ByVal IdxRow As Integer) As Boolean

    If IdxRow >= 0 And IdxRow < Me.Grid.NumRows Then
      ' All rows with data have a non-empty value in column BLOCKED_INT.
      Return Not String.IsNullOrEmpty(Me.Grid.Cell(IdxRow, GRID_COLUMN_BLOCKED_INT).Value)
    End If

    Return False
  End Function ' IsValidDataRow

  ' PURPOSE: Get Level list for selected levels
  '          Format list to build query: X, X, X
  '          X is the Id if GetIds = True
  '          otherwise, X is the Name of the control
  '  PARAMS:
  '     - INPUT:
  '           - GetIds As Boolean
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String
  Private Function GetLevelsSelected(Optional ByVal GetIds As Boolean = True) As String
    Dim list_type As String

    list_type = ""

    If Me.chk_level_01.Checked Then
      list_type = IIf(GetIds, "1", Me.chk_level_01.Text)
    End If

    If Me.chk_level_02.Checked Then
      If list_type.Length > 0 Then
        list_type = list_type & ", "
      End If
      list_type = list_type & IIf(GetIds, "2", Me.chk_level_02.Text)
    End If

    If Me.chk_level_03.Checked Then
      If list_type.Length > 0 Then
        list_type = list_type & ", "
      End If
      list_type = list_type & IIf(GetIds, "3", Me.chk_level_03.Text)
    End If

    If Me.chk_level_04.Checked Then
      If list_type.Length > 0 Then
        list_type = list_type & ", "
      End If
      list_type = list_type & IIf(GetIds, "4", Me.chk_level_04.Text)
    End If

    Return list_type
  End Function ' GetLevelsSelected

  ' PURPOSE: Get Gender list for selected genders
  '          Format list to build query: X, X, X
  '          X is the Id if GetIds = True
  '          otherwise, X is the Name of the control
  '  PARAMS:
  '     - INPUT:
  '           - GetIds As Boolean
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - String

  Private Function GetGendersSelected(Optional ByVal GetIds As Boolean = True) As String
    Dim list_type As String

    list_type = ""
    If chk_gender_male.Checked Then
      list_type = IIf(GetIds, "1", Me.chk_gender_male.Text)
    End If
    If chk_gender_female.Checked Then
      If list_type.Length > 0 Then
        list_type = list_type & ", "
      End If
      list_type = list_type & IIf(GetIds, "2", Me.chk_gender_female.Text)
    End If

    Return list_type
  End Function ' GetGendersSelected



  ' PURPOSE: Hide the column of the grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - IndexColumn:
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Private Sub HideColumn(ByVal IndexColumn As Integer)

    Me.Grid.Column(IndexColumn).WidthFixed = 0
    Me.Grid.Column(IndexColumn).Width = 0
    Me.Grid.Column(IndexColumn).IsColumnPrintable = False

  End Sub

  ' PURPOSE: Return the Last activity filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '     - OUTPUT:
  '           - String with the conditions
  '
  ' RETURNS:
  '     - String
  Private Function GetSqlWhereLastActivity() As String
    Dim _sb As StringBuilder
    Dim _date As Date

    _sb = New StringBuilder
    _date = WSI.Common.Misc.TodayOpening()
    _date = _date.AddDays(-1 * m_days_without_activity)

    If Not Me.uc_account_sel1.DisabledHolder Then
      If Not Me.rb_todos.Checked Then

        If Me.rb_last_activity_active.Checked And Me.uc_account_mov_sites_sel.GetSitesIdListSelectedAll <> "" Then
          _sb.AppendLine("    AND LAA_SITE_ID IN (" & Me.uc_account_mov_sites_sel.GetSitesIdListSelectedAll() & ")  ")
          _sb.AppendLine("    AND (LAA_LAST_ACTIVITY >= " & GUI_FormatDateDB(_date) & ")                            ")
        Else
          If Me.rb_last_activity_inactive.Checked Then
            _sb.AppendLine("    AND (AC_LAST_ACTIVITY < " & GUI_FormatDateDB(_date) & ")                            ")
          ElseIf Me.rb_last_activity_by_date.Checked Then
            _sb.AppendLine("    AND (AC_LAST_ACTIVITY >= " & GUI_FormatDateDB(dtp_last_from.Value) & ")             ")
            If Me.dtp_last_to.Checked Then
              _sb.AppendLine("    AND (AC_LAST_ACTIVITY < " & GUI_FormatDateDB(dtp_last_to.Value) & ")              ")
            End If
          End If
        End If
      End If
    End If

    Return _sb.ToString
  End Function ' GetSqlWhereLastActivity


#End Region ' Private Functions

#Region " Events "

  Private Sub chk_only_anonymous_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_only_anonymous.CheckedChanged
    gb_level.Enabled = Not chk_only_anonymous.Checked
    gb_gender.Enabled = Not chk_only_anonymous.Checked
    If chk_only_anonymous.Checked Then
      chk_level_01.Checked = False
      chk_level_02.Checked = False
      chk_level_03.Checked = False
      chk_level_04.Checked = False

      chk_gender_male.Checked = False
      chk_gender_female.Checked = False
    End If
  End Sub

  Private Sub uc_account_sel1_OneAccountFilterChanged() Handles uc_account_sel1.OneAccountFilterChanged
    Me.gb_date.Enabled = Not uc_account_sel1.DisabledHolder
    Me.gb_last_activity.Enabled = Not uc_account_sel1.DisabledHolder
    Me.gb_level.Enabled = Not uc_account_sel1.DisabledHolder
    Me.gb_gender.Enabled = Not uc_account_sel1.DisabledHolder
    Me.chk_only_anonymous.Enabled = Not uc_account_sel1.DisabledHolder
    Me.gb_points.Enabled = Not uc_account_sel1.DisabledHolder
    Me.fra_card_state.Enabled = Not uc_account_sel1.DisabledHolder

    If Me.rb_last_activity_active.Checked Then
      Call Me.uc_account_mov_sites_sel.EnableControl(Not Me.uc_account_sel1.DisabledHolder)
    End If
  End Sub

  Private Sub rb_last_activity_active_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rb_last_activity_active.CheckedChanged
    If Me.rb_last_activity_active.Checked Then
      Call Me.uc_account_mov_sites_sel.EnableControl(True)
    Else
      Call Me.uc_account_mov_sites_sel.EnableControl(False)
    End If
  End Sub ' rb_last_activity_active_CheckedChanged

#End Region ' Events

End Class
