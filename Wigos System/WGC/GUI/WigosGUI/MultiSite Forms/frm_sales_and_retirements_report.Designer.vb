﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_sales_and_retirements_report
  Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.uc_sites_sel = New GUI_Controls.uc_sites_sel()
    Me.uc_daily_session_selector = New GUI_Controls.uc_daily_session_selector()
    Me.uc_checked_list_operations = New GUI_Controls.uc_checked_list()
    Me.uc_checked_list_payment_type = New GUI_Controls.uc_checked_list()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.uc_checked_list_payment_type)
    Me.panel_filter.Controls.Add(Me.uc_checked_list_operations)
    Me.panel_filter.Controls.Add(Me.uc_daily_session_selector)
    Me.panel_filter.Controls.Add(Me.uc_sites_sel)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(1241, 178)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_sites_sel, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_daily_session_selector, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_checked_list_operations, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_checked_list_payment_type, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(5, 182)
    Me.panel_data.Size = New System.Drawing.Size(1241, 399)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1235, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1235, 4)
    '
    'uc_sites_sel
    '
    Me.uc_sites_sel.Location = New System.Drawing.Point(6, 5)
    Me.uc_sites_sel.MultiSelect = True
    Me.uc_sites_sel.Name = "uc_sites_sel"
    Me.uc_sites_sel.ShowIsoCode = False
    Me.uc_sites_sel.ShowMultisiteRow = True
    Me.uc_sites_sel.Size = New System.Drawing.Size(286, 130)
    Me.uc_sites_sel.TabIndex = 0
    '
    'uc_daily_session_selector
    '
    Me.uc_daily_session_selector.ClosingTime = 0
    Me.uc_daily_session_selector.ClosingTimeEnabled = True
    Me.uc_daily_session_selector.FromDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_daily_session_selector.FromDateSelected = True
    Me.uc_daily_session_selector.Location = New System.Drawing.Point(298, 8)
    Me.uc_daily_session_selector.Name = "uc_daily_session_selector"
    Me.uc_daily_session_selector.ShowBorder = True
    Me.uc_daily_session_selector.Size = New System.Drawing.Size(282, 128)
    Me.uc_daily_session_selector.TabIndex = 1
    Me.uc_daily_session_selector.ToDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_daily_session_selector.ToDateSelected = True
    '
    'uc_checked_list_operations
    '
    Me.uc_checked_list_operations.GroupBoxText = "xOperation Type"
    Me.uc_checked_list_operations.Location = New System.Drawing.Point(587, 8)
    Me.uc_checked_list_operations.m_resize_width = 290
    Me.uc_checked_list_operations.multiChoice = True
    Me.uc_checked_list_operations.Name = "uc_checked_list_operations"
    Me.uc_checked_list_operations.SelectedIndexes = New Integer(-1) {}
    Me.uc_checked_list_operations.SelectedIndexesList = ""
    Me.uc_checked_list_operations.SelectedIndexesListLevel2 = ""
    Me.uc_checked_list_operations.SelectedValuesArray = New String(-1) {}
    Me.uc_checked_list_operations.SelectedValuesList = ""
    Me.uc_checked_list_operations.SetLevels = 2
    Me.uc_checked_list_operations.Size = New System.Drawing.Size(290, 165)
    Me.uc_checked_list_operations.TabIndex = 2
    Me.uc_checked_list_operations.ValuesArray = New String(-1) {}
    '
    'uc_checked_list_payment_type
    '
    Me.uc_checked_list_payment_type.GroupBoxText = "xPayment type"
    Me.uc_checked_list_payment_type.Location = New System.Drawing.Point(970, 8)
    Me.uc_checked_list_payment_type.m_resize_width = 148
    Me.uc_checked_list_payment_type.multiChoice = True
    Me.uc_checked_list_payment_type.Name = "uc_checked_list_payment_type"
    Me.uc_checked_list_payment_type.SelectedIndexes = New Integer(-1) {}
    Me.uc_checked_list_payment_type.SelectedIndexesList = ""
    Me.uc_checked_list_payment_type.SelectedIndexesListLevel2 = ""
    Me.uc_checked_list_payment_type.SelectedValuesArray = New String(-1) {}
    Me.uc_checked_list_payment_type.SelectedValuesList = ""
    Me.uc_checked_list_payment_type.SetLevels = 2
    Me.uc_checked_list_payment_type.Size = New System.Drawing.Size(148, 165)
    Me.uc_checked_list_payment_type.TabIndex = 3
    Me.uc_checked_list_payment_type.ValuesArray = New String(-1) {}
    '
    'frm_sales_and_retirements_report
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1251, 585)
    Me.Name = "frm_sales_and_retirements_report"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_sales_and_retirements_report"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents uc_checked_list_payment_type As GUI_Controls.uc_checked_list
  Friend WithEvents uc_daily_session_selector As GUI_Controls.uc_daily_session_selector
  Friend WithEvents uc_sites_sel As GUI_Controls.uc_sites_sel
  Private WithEvents uc_checked_list_operations As GUI_Controls.uc_checked_list
End Class
