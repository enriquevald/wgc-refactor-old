<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_sites_monitor
  Inherits GUI_Controls.frm_base

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container
    Me.tf_last_update = New GUI_Controls.uc_text_field
    Me.TabControl = New System.Windows.Forms.TabControl
    Me.tab_sites = New System.Windows.Forms.TabPage
    Me.lbl_site_id = New System.Windows.Forms.Label
    Me.gb_sites_tab_status = New System.Windows.Forms.GroupBox
    Me.lbl_sites_tab_color_all = New System.Windows.Forms.Label
    Me.opt_sites_tab_connected_not_synchronize = New System.Windows.Forms.RadioButton
    Me.opt_sites_tab_disconnected = New System.Windows.Forms.RadioButton
    Me.opt_sites_tab_synchronize = New System.Windows.Forms.RadioButton
    Me.opt_sites_tab_all_status = New System.Windows.Forms.RadioButton
    Me.lbl_sites_tab_color_conected = New System.Windows.Forms.Label
    Me.lbl_sites_tab_color_synchronize = New System.Windows.Forms.Label
    Me.lbl_sites_tab_color_disconected = New System.Windows.Forms.Label
    Me.lbl_tasks = New System.Windows.Forms.Label
    Me.ef_site = New GUI_Controls.uc_entry_field
    Me.lbl_sites = New System.Windows.Forms.Label
    Me.dg_site_tasks = New GUI_Controls.uc_grid
    Me.dg_sites = New GUI_Controls.uc_grid
    Me.tab_processes = New System.Windows.Forms.TabPage
    Me.lbl_tasks_taks = New System.Windows.Forms.Label
    Me.gb_tasks_tab_status = New System.Windows.Forms.GroupBox
    Me.lbl_tasks_tab_color_all = New System.Windows.Forms.Label
    Me.opt_tasks_tab_connected_not_synchronize = New System.Windows.Forms.RadioButton
    Me.opt_tasks_tab_disconnected = New System.Windows.Forms.RadioButton
    Me.opt_tasks_tab_synchronize = New System.Windows.Forms.RadioButton
    Me.opt_tasks_tab_all_status = New System.Windows.Forms.RadioButton
    Me.lbl_tasks_tab_color_disconected = New System.Windows.Forms.Label
    Me.lbl_tasks_tab_color_conected = New System.Windows.Forms.Label
    Me.lbl_tasks_tab_color_synchronize = New System.Windows.Forms.Label
    Me.cmb_processes = New GUI_Controls.uc_combo
    Me.dg_tasks = New GUI_Controls.uc_grid
    Me.timer = New System.Windows.Forms.Timer(Me.components)
    Me.btn_exit = New System.Windows.Forms.Button
    Me.btn_refresh = New System.Windows.Forms.Button
    Me.lbl_wwp_status = New System.Windows.Forms.Label
    Me.lbl_wwp_status_value = New System.Windows.Forms.Label
    Me.lbl_stopped = New System.Windows.Forms.Label
    Me.lbl_connected_sites = New System.Windows.Forms.Label
    Me.lbl_num_connected_sites = New System.Windows.Forms.Label
    Me.lbl_num_disconnected_sites = New System.Windows.Forms.Label
    Me.lbl_disconnected_sites = New System.Windows.Forms.Label
    Me.btn_force_sync = New System.Windows.Forms.Button
    Me.TabControl.SuspendLayout()
    Me.tab_sites.SuspendLayout()
    Me.gb_sites_tab_status.SuspendLayout()
    Me.tab_processes.SuspendLayout()
    Me.gb_tasks_tab_status.SuspendLayout()
    Me.SuspendLayout()
    '
    'tf_last_update
    '
    Me.tf_last_update.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.tf_last_update.IsReadOnly = True
    Me.tf_last_update.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_last_update.LabelForeColor = System.Drawing.Color.Blue
    Me.tf_last_update.Location = New System.Drawing.Point(849, -5)
    Me.tf_last_update.Name = "tf_last_update"
    Me.tf_last_update.Size = New System.Drawing.Size(242, 24)
    Me.tf_last_update.SufixText = "xSeconds"
    Me.tf_last_update.SufixTextVisible = True
    Me.tf_last_update.TabIndex = 0
    Me.tf_last_update.TabStop = False
    Me.tf_last_update.TextWidth = 180
    Me.tf_last_update.Value = "x99.999"
    '
    'TabControl
    '
    Me.TabControl.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.TabControl.Controls.Add(Me.tab_sites)
    Me.TabControl.Controls.Add(Me.tab_processes)
    Me.TabControl.Location = New System.Drawing.Point(7, 35)
    Me.TabControl.Name = "TabControl"
    Me.TabControl.SelectedIndex = 0
    Me.TabControl.Size = New System.Drawing.Size(1083, 603)
    Me.TabControl.TabIndex = 8
    '
    'tab_sites
    '
    Me.tab_sites.Controls.Add(Me.lbl_site_id)
    Me.tab_sites.Controls.Add(Me.gb_sites_tab_status)
    Me.tab_sites.Controls.Add(Me.lbl_tasks)
    Me.tab_sites.Controls.Add(Me.ef_site)
    Me.tab_sites.Controls.Add(Me.lbl_sites)
    Me.tab_sites.Controls.Add(Me.dg_site_tasks)
    Me.tab_sites.Controls.Add(Me.dg_sites)
    Me.tab_sites.Location = New System.Drawing.Point(4, 22)
    Me.tab_sites.Name = "tab_sites"
    Me.tab_sites.Padding = New System.Windows.Forms.Padding(3)
    Me.tab_sites.Size = New System.Drawing.Size(1075, 577)
    Me.tab_sites.TabIndex = 0
    Me.tab_sites.Text = "xSites"
    Me.tab_sites.UseVisualStyleBackColor = True
    '
    'lbl_site_id
    '
    Me.lbl_site_id.AutoSize = True
    Me.lbl_site_id.Location = New System.Drawing.Point(7, 130)
    Me.lbl_site_id.Name = "lbl_site_id"
    Me.lbl_site_id.Size = New System.Drawing.Size(50, 13)
    Me.lbl_site_id.TabIndex = 0
    Me.lbl_site_id.Text = "xSiteID"
    '
    'gb_sites_tab_status
    '
    Me.gb_sites_tab_status.Controls.Add(Me.lbl_sites_tab_color_all)
    Me.gb_sites_tab_status.Controls.Add(Me.opt_sites_tab_connected_not_synchronize)
    Me.gb_sites_tab_status.Controls.Add(Me.opt_sites_tab_disconnected)
    Me.gb_sites_tab_status.Controls.Add(Me.opt_sites_tab_synchronize)
    Me.gb_sites_tab_status.Controls.Add(Me.opt_sites_tab_all_status)
    Me.gb_sites_tab_status.Controls.Add(Me.lbl_sites_tab_color_conected)
    Me.gb_sites_tab_status.Controls.Add(Me.lbl_sites_tab_color_synchronize)
    Me.gb_sites_tab_status.Controls.Add(Me.lbl_sites_tab_color_disconected)
    Me.gb_sites_tab_status.Location = New System.Drawing.Point(6, 6)
    Me.gb_sites_tab_status.Name = "gb_sites_tab_status"
    Me.gb_sites_tab_status.Size = New System.Drawing.Size(218, 116)
    Me.gb_sites_tab_status.TabIndex = 2
    Me.gb_sites_tab_status.TabStop = False
    Me.gb_sites_tab_status.Text = "xStatus"
    '
    'lbl_sites_tab_color_all
    '
    Me.lbl_sites_tab_color_all.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_sites_tab_color_all.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_sites_tab_color_all.Location = New System.Drawing.Point(6, 21)
    Me.lbl_sites_tab_color_all.Name = "lbl_sites_tab_color_all"
    Me.lbl_sites_tab_color_all.Size = New System.Drawing.Size(16, 16)
    Me.lbl_sites_tab_color_all.TabIndex = 9
    '
    'opt_sites_tab_connected_not_synchronize
    '
    Me.opt_sites_tab_connected_not_synchronize.AutoSize = True
    Me.opt_sites_tab_connected_not_synchronize.Location = New System.Drawing.Point(28, 66)
    Me.opt_sites_tab_connected_not_synchronize.Name = "opt_sites_tab_connected_not_synchronize"
    Me.opt_sites_tab_connected_not_synchronize.Size = New System.Drawing.Size(182, 17)
    Me.opt_sites_tab_connected_not_synchronize.TabIndex = 8
    Me.opt_sites_tab_connected_not_synchronize.TabStop = True
    Me.opt_sites_tab_connected_not_synchronize.Text = "xConnectedNotSynchronize"
    Me.opt_sites_tab_connected_not_synchronize.UseVisualStyleBackColor = True
    '
    'opt_sites_tab_disconnected
    '
    Me.opt_sites_tab_disconnected.AutoSize = True
    Me.opt_sites_tab_disconnected.Location = New System.Drawing.Point(28, 89)
    Me.opt_sites_tab_disconnected.Name = "opt_sites_tab_disconnected"
    Me.opt_sites_tab_disconnected.Size = New System.Drawing.Size(108, 17)
    Me.opt_sites_tab_disconnected.TabIndex = 2
    Me.opt_sites_tab_disconnected.TabStop = True
    Me.opt_sites_tab_disconnected.Text = "xDisconnected"
    Me.opt_sites_tab_disconnected.UseVisualStyleBackColor = True
    '
    'opt_sites_tab_synchronize
    '
    Me.opt_sites_tab_synchronize.AutoSize = True
    Me.opt_sites_tab_synchronize.Location = New System.Drawing.Point(28, 43)
    Me.opt_sites_tab_synchronize.Name = "opt_sites_tab_synchronize"
    Me.opt_sites_tab_synchronize.Size = New System.Drawing.Size(102, 17)
    Me.opt_sites_tab_synchronize.TabIndex = 1
    Me.opt_sites_tab_synchronize.TabStop = True
    Me.opt_sites_tab_synchronize.Text = "xSynchronize"
    Me.opt_sites_tab_synchronize.UseVisualStyleBackColor = True
    '
    'opt_sites_tab_all_status
    '
    Me.opt_sites_tab_all_status.AutoSize = True
    Me.opt_sites_tab_all_status.Location = New System.Drawing.Point(28, 20)
    Me.opt_sites_tab_all_status.Name = "opt_sites_tab_all_status"
    Me.opt_sites_tab_all_status.Size = New System.Drawing.Size(46, 17)
    Me.opt_sites_tab_all_status.TabIndex = 0
    Me.opt_sites_tab_all_status.TabStop = True
    Me.opt_sites_tab_all_status.Text = "xAll"
    Me.opt_sites_tab_all_status.UseVisualStyleBackColor = True
    '
    'lbl_sites_tab_color_conected
    '
    Me.lbl_sites_tab_color_conected.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_sites_tab_color_conected.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_sites_tab_color_conected.Location = New System.Drawing.Point(6, 67)
    Me.lbl_sites_tab_color_conected.Name = "lbl_sites_tab_color_conected"
    Me.lbl_sites_tab_color_conected.Size = New System.Drawing.Size(16, 16)
    Me.lbl_sites_tab_color_conected.TabIndex = 6
    '
    'lbl_sites_tab_color_synchronize
    '
    Me.lbl_sites_tab_color_synchronize.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_sites_tab_color_synchronize.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_sites_tab_color_synchronize.Location = New System.Drawing.Point(6, 44)
    Me.lbl_sites_tab_color_synchronize.Name = "lbl_sites_tab_color_synchronize"
    Me.lbl_sites_tab_color_synchronize.Size = New System.Drawing.Size(16, 16)
    Me.lbl_sites_tab_color_synchronize.TabIndex = 5
    '
    'lbl_sites_tab_color_disconected
    '
    Me.lbl_sites_tab_color_disconected.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_sites_tab_color_disconected.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_sites_tab_color_disconected.Location = New System.Drawing.Point(6, 90)
    Me.lbl_sites_tab_color_disconected.Name = "lbl_sites_tab_color_disconected"
    Me.lbl_sites_tab_color_disconected.Size = New System.Drawing.Size(16, 16)
    Me.lbl_sites_tab_color_disconected.TabIndex = 7
    '
    'lbl_tasks
    '
    Me.lbl_tasks.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.lbl_tasks.AutoSize = True
    Me.lbl_tasks.Location = New System.Drawing.Point(11, 297)
    Me.lbl_tasks.Name = "lbl_tasks"
    Me.lbl_tasks.Size = New System.Drawing.Size(47, 13)
    Me.lbl_tasks.TabIndex = 5
    Me.lbl_tasks.Text = "xTasks"
    '
    'ef_site
    '
    Me.ef_site.DoubleValue = 0
    Me.ef_site.IntegerValue = 0
    Me.ef_site.IsReadOnly = False
    Me.ef_site.Location = New System.Drawing.Point(6, 146)
    Me.ef_site.Name = "ef_site"
    Me.ef_site.Size = New System.Drawing.Size(62, 24)
    Me.ef_site.SufixText = "Sufix Text"
    Me.ef_site.SufixTextVisible = True
    Me.ef_site.TabIndex = 1
    Me.ef_site.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_site.TextValue = ""
    Me.ef_site.TextVisible = False
    Me.ef_site.TextWidth = 0
    Me.ef_site.Value = ""
    Me.ef_site.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_sites
    '
    Me.lbl_sites.AutoSize = True
    Me.lbl_sites.Location = New System.Drawing.Point(230, 6)
    Me.lbl_sites.Name = "lbl_sites"
    Me.lbl_sites.Size = New System.Drawing.Size(42, 13)
    Me.lbl_sites.TabIndex = 3
    Me.lbl_sites.Text = "xSites"
    '
    'dg_site_tasks
    '
    Me.dg_site_tasks.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.dg_site_tasks.CurrentCol = -1
    Me.dg_site_tasks.CurrentRow = -1
    Me.dg_site_tasks.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_site_tasks.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_site_tasks.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_site_tasks.Location = New System.Drawing.Point(0, 315)
    Me.dg_site_tasks.Name = "dg_site_tasks"
    Me.dg_site_tasks.PanelRightVisible = False
    Me.dg_site_tasks.Redraw = True
    Me.dg_site_tasks.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_site_tasks.Size = New System.Drawing.Size(1075, 260)
    Me.dg_site_tasks.Sortable = False
    Me.dg_site_tasks.SortAscending = True
    Me.dg_site_tasks.SortByCol = 0
    Me.dg_site_tasks.TabIndex = 6
    Me.dg_site_tasks.ToolTipped = True
    Me.dg_site_tasks.TopRow = -2
    '
    'dg_sites
    '
    Me.dg_sites.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.dg_sites.CurrentCol = -1
    Me.dg_sites.CurrentRow = -1
    Me.dg_sites.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_sites.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_sites.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_sites.Location = New System.Drawing.Point(233, 24)
    Me.dg_sites.Name = "dg_sites"
    Me.dg_sites.PanelRightVisible = False
    Me.dg_sites.Redraw = True
    Me.dg_sites.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
    Me.dg_sites.Size = New System.Drawing.Size(840, 273)
    Me.dg_sites.Sortable = False
    Me.dg_sites.SortAscending = True
    Me.dg_sites.SortByCol = 1
    Me.dg_sites.TabIndex = 4
    Me.dg_sites.ToolTipped = True
    Me.dg_sites.TopRow = -2
    '
    'tab_processes
    '
    Me.tab_processes.Controls.Add(Me.lbl_tasks_taks)
    Me.tab_processes.Controls.Add(Me.gb_tasks_tab_status)
    Me.tab_processes.Controls.Add(Me.cmb_processes)
    Me.tab_processes.Controls.Add(Me.dg_tasks)
    Me.tab_processes.Location = New System.Drawing.Point(4, 22)
    Me.tab_processes.Name = "tab_processes"
    Me.tab_processes.Padding = New System.Windows.Forms.Padding(3)
    Me.tab_processes.Size = New System.Drawing.Size(911, 577)
    Me.tab_processes.TabIndex = 1
    Me.tab_processes.Text = "xProcesses"
    Me.tab_processes.UseVisualStyleBackColor = True
    '
    'lbl_tasks_taks
    '
    Me.lbl_tasks_taks.AutoSize = True
    Me.lbl_tasks_taks.Location = New System.Drawing.Point(263, 10)
    Me.lbl_tasks_taks.Name = "lbl_tasks_taks"
    Me.lbl_tasks_taks.Size = New System.Drawing.Size(47, 13)
    Me.lbl_tasks_taks.TabIndex = 0
    Me.lbl_tasks_taks.Text = "xTasks"
    '
    'gb_tasks_tab_status
    '
    Me.gb_tasks_tab_status.Controls.Add(Me.lbl_tasks_tab_color_all)
    Me.gb_tasks_tab_status.Controls.Add(Me.opt_tasks_tab_connected_not_synchronize)
    Me.gb_tasks_tab_status.Controls.Add(Me.opt_tasks_tab_disconnected)
    Me.gb_tasks_tab_status.Controls.Add(Me.opt_tasks_tab_synchronize)
    Me.gb_tasks_tab_status.Controls.Add(Me.opt_tasks_tab_all_status)
    Me.gb_tasks_tab_status.Controls.Add(Me.lbl_tasks_tab_color_disconected)
    Me.gb_tasks_tab_status.Controls.Add(Me.lbl_tasks_tab_color_conected)
    Me.gb_tasks_tab_status.Controls.Add(Me.lbl_tasks_tab_color_synchronize)
    Me.gb_tasks_tab_status.Location = New System.Drawing.Point(6, 6)
    Me.gb_tasks_tab_status.Name = "gb_tasks_tab_status"
    Me.gb_tasks_tab_status.Size = New System.Drawing.Size(239, 114)
    Me.gb_tasks_tab_status.TabIndex = 2
    Me.gb_tasks_tab_status.TabStop = False
    Me.gb_tasks_tab_status.Text = "xStatus"
    '
    'lbl_tasks_tab_color_all
    '
    Me.lbl_tasks_tab_color_all.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_tasks_tab_color_all.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_tasks_tab_color_all.Location = New System.Drawing.Point(6, 21)
    Me.lbl_tasks_tab_color_all.Name = "lbl_tasks_tab_color_all"
    Me.lbl_tasks_tab_color_all.Size = New System.Drawing.Size(16, 16)
    Me.lbl_tasks_tab_color_all.TabIndex = 10
    '
    'opt_tasks_tab_connected_not_synchronize
    '
    Me.opt_tasks_tab_connected_not_synchronize.AutoSize = True
    Me.opt_tasks_tab_connected_not_synchronize.Location = New System.Drawing.Point(28, 66)
    Me.opt_tasks_tab_connected_not_synchronize.Name = "opt_tasks_tab_connected_not_synchronize"
    Me.opt_tasks_tab_connected_not_synchronize.Size = New System.Drawing.Size(182, 17)
    Me.opt_tasks_tab_connected_not_synchronize.TabIndex = 8
    Me.opt_tasks_tab_connected_not_synchronize.TabStop = True
    Me.opt_tasks_tab_connected_not_synchronize.Text = "xConnectedNotSynchronize"
    Me.opt_tasks_tab_connected_not_synchronize.UseVisualStyleBackColor = True
    '
    'opt_tasks_tab_disconnected
    '
    Me.opt_tasks_tab_disconnected.AutoSize = True
    Me.opt_tasks_tab_disconnected.Location = New System.Drawing.Point(28, 89)
    Me.opt_tasks_tab_disconnected.Name = "opt_tasks_tab_disconnected"
    Me.opt_tasks_tab_disconnected.Size = New System.Drawing.Size(108, 17)
    Me.opt_tasks_tab_disconnected.TabIndex = 2
    Me.opt_tasks_tab_disconnected.TabStop = True
    Me.opt_tasks_tab_disconnected.Text = "xDisconnected"
    Me.opt_tasks_tab_disconnected.UseVisualStyleBackColor = True
    '
    'opt_tasks_tab_synchronize
    '
    Me.opt_tasks_tab_synchronize.AutoSize = True
    Me.opt_tasks_tab_synchronize.Location = New System.Drawing.Point(28, 43)
    Me.opt_tasks_tab_synchronize.Name = "opt_tasks_tab_synchronize"
    Me.opt_tasks_tab_synchronize.Size = New System.Drawing.Size(102, 17)
    Me.opt_tasks_tab_synchronize.TabIndex = 1
    Me.opt_tasks_tab_synchronize.TabStop = True
    Me.opt_tasks_tab_synchronize.Text = "xSynchronize"
    Me.opt_tasks_tab_synchronize.UseVisualStyleBackColor = True
    '
    'opt_tasks_tab_all_status
    '
    Me.opt_tasks_tab_all_status.AutoSize = True
    Me.opt_tasks_tab_all_status.Location = New System.Drawing.Point(28, 20)
    Me.opt_tasks_tab_all_status.Name = "opt_tasks_tab_all_status"
    Me.opt_tasks_tab_all_status.Size = New System.Drawing.Size(46, 17)
    Me.opt_tasks_tab_all_status.TabIndex = 0
    Me.opt_tasks_tab_all_status.TabStop = True
    Me.opt_tasks_tab_all_status.Text = "xAll"
    Me.opt_tasks_tab_all_status.UseVisualStyleBackColor = True
    '
    'lbl_tasks_tab_color_disconected
    '
    Me.lbl_tasks_tab_color_disconected.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_tasks_tab_color_disconected.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_tasks_tab_color_disconected.Location = New System.Drawing.Point(6, 90)
    Me.lbl_tasks_tab_color_disconected.Name = "lbl_tasks_tab_color_disconected"
    Me.lbl_tasks_tab_color_disconected.Size = New System.Drawing.Size(16, 16)
    Me.lbl_tasks_tab_color_disconected.TabIndex = 7
    '
    'lbl_tasks_tab_color_conected
    '
    Me.lbl_tasks_tab_color_conected.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_tasks_tab_color_conected.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_tasks_tab_color_conected.Location = New System.Drawing.Point(6, 67)
    Me.lbl_tasks_tab_color_conected.Name = "lbl_tasks_tab_color_conected"
    Me.lbl_tasks_tab_color_conected.Size = New System.Drawing.Size(16, 16)
    Me.lbl_tasks_tab_color_conected.TabIndex = 6
    '
    'lbl_tasks_tab_color_synchronize
    '
    Me.lbl_tasks_tab_color_synchronize.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_tasks_tab_color_synchronize.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_tasks_tab_color_synchronize.Location = New System.Drawing.Point(6, 44)
    Me.lbl_tasks_tab_color_synchronize.Name = "lbl_tasks_tab_color_synchronize"
    Me.lbl_tasks_tab_color_synchronize.Size = New System.Drawing.Size(16, 16)
    Me.lbl_tasks_tab_color_synchronize.TabIndex = 5
    '
    'cmb_processes
    '
    Me.cmb_processes.AllowUnlistedValues = False
    Me.cmb_processes.AutoCompleteMode = False
    Me.cmb_processes.IsReadOnly = False
    Me.cmb_processes.Location = New System.Drawing.Point(260, 26)
    Me.cmb_processes.Name = "cmb_processes"
    Me.cmb_processes.SelectedIndex = -1
    Me.cmb_processes.Size = New System.Drawing.Size(330, 24)
    Me.cmb_processes.SufixText = "Sufix Text"
    Me.cmb_processes.TabIndex = 1
    Me.cmb_processes.TextVisible = False
    Me.cmb_processes.TextWidth = 0
    '
    'dg_tasks
    '
    Me.dg_tasks.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.dg_tasks.CurrentCol = -1
    Me.dg_tasks.CurrentRow = -1
    Me.dg_tasks.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_tasks.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_tasks.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_tasks.Location = New System.Drawing.Point(0, 137)
    Me.dg_tasks.Name = "dg_tasks"
    Me.dg_tasks.PanelRightVisible = False
    Me.dg_tasks.Redraw = True
    Me.dg_tasks.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_tasks.Size = New System.Drawing.Size(910, 441)
    Me.dg_tasks.Sortable = False
    Me.dg_tasks.SortAscending = True
    Me.dg_tasks.SortByCol = 0
    Me.dg_tasks.TabIndex = 3
    Me.dg_tasks.ToolTipped = True
    Me.dg_tasks.TopRow = -2
    '
    'btn_exit
    '
    Me.btn_exit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.btn_exit.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btn_exit.Location = New System.Drawing.Point(1096, 612)
    Me.btn_exit.Name = "btn_exit"
    Me.btn_exit.Size = New System.Drawing.Size(84, 26)
    Me.btn_exit.TabIndex = 11
    Me.btn_exit.Text = "xExit"
    Me.btn_exit.UseVisualStyleBackColor = True
    '
    'btn_refresh
    '
    Me.btn_refresh.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.btn_refresh.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btn_refresh.Location = New System.Drawing.Point(1096, 58)
    Me.btn_refresh.Name = "btn_refresh"
    Me.btn_refresh.Size = New System.Drawing.Size(84, 26)
    Me.btn_refresh.TabIndex = 9
    Me.btn_refresh.Text = "xRefresh"
    Me.btn_refresh.UseVisualStyleBackColor = True
    '
    'lbl_wwp_status
    '
    Me.lbl_wwp_status.Font = New System.Drawing.Font("Verdana", 10.0!, System.Drawing.FontStyle.Bold)
    Me.lbl_wwp_status.Location = New System.Drawing.Point(8, 17)
    Me.lbl_wwp_status.Name = "lbl_wwp_status"
    Me.lbl_wwp_status.Size = New System.Drawing.Size(180, 18)
    Me.lbl_wwp_status.TabIndex = 1
    Me.lbl_wwp_status.Text = "xWWPStatus"
    Me.lbl_wwp_status.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'lbl_wwp_status_value
    '
    Me.lbl_wwp_status_value.AutoSize = True
    Me.lbl_wwp_status_value.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
    Me.lbl_wwp_status_value.Location = New System.Drawing.Point(196, 14)
    Me.lbl_wwp_status_value.MinimumSize = New System.Drawing.Size(0, 25)
    Me.lbl_wwp_status_value.Name = "lbl_wwp_status_value"
    Me.lbl_wwp_status_value.Size = New System.Drawing.Size(172, 25)
    Me.lbl_wwp_status_value.TabIndex = 2
    Me.lbl_wwp_status_value.Text = "xWWPStatusValue"
    Me.lbl_wwp_status_value.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_stopped
    '
    Me.lbl_stopped.AutoSize = True
    Me.lbl_stopped.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_stopped.Location = New System.Drawing.Point(374, 14)
    Me.lbl_stopped.MinimumSize = New System.Drawing.Size(0, 25)
    Me.lbl_stopped.Name = "lbl_stopped"
    Me.lbl_stopped.Size = New System.Drawing.Size(84, 25)
    Me.lbl_stopped.TabIndex = 3
    Me.lbl_stopped.Text = "xStopped"
    Me.lbl_stopped.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_connected_sites
    '
    Me.lbl_connected_sites.AutoSize = True
    Me.lbl_connected_sites.Font = New System.Drawing.Font("Verdana", 10.0!, System.Drawing.FontStyle.Bold)
    Me.lbl_connected_sites.Location = New System.Drawing.Point(464, 19)
    Me.lbl_connected_sites.Name = "lbl_connected_sites"
    Me.lbl_connected_sites.Size = New System.Drawing.Size(137, 17)
    Me.lbl_connected_sites.TabIndex = 4
    Me.lbl_connected_sites.Text = "xConnectedSites"
    '
    'lbl_num_connected_sites
    '
    Me.lbl_num_connected_sites.AutoSize = True
    Me.lbl_num_connected_sites.Font = New System.Drawing.Font("Verdana", 10.0!, System.Drawing.FontStyle.Bold)
    Me.lbl_num_connected_sites.Location = New System.Drawing.Point(607, 15)
    Me.lbl_num_connected_sites.MinimumSize = New System.Drawing.Size(0, 25)
    Me.lbl_num_connected_sites.Name = "lbl_num_connected_sites"
    Me.lbl_num_connected_sites.Size = New System.Drawing.Size(29, 25)
    Me.lbl_num_connected_sites.TabIndex = 5
    Me.lbl_num_connected_sites.Text = "---"
    Me.lbl_num_connected_sites.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_num_disconnected_sites
    '
    Me.lbl_num_disconnected_sites.AutoSize = True
    Me.lbl_num_disconnected_sites.Font = New System.Drawing.Font("Verdana", 10.0!, System.Drawing.FontStyle.Bold)
    Me.lbl_num_disconnected_sites.Location = New System.Drawing.Point(807, 15)
    Me.lbl_num_disconnected_sites.MinimumSize = New System.Drawing.Size(0, 25)
    Me.lbl_num_disconnected_sites.Name = "lbl_num_disconnected_sites"
    Me.lbl_num_disconnected_sites.Size = New System.Drawing.Size(29, 25)
    Me.lbl_num_disconnected_sites.TabIndex = 7
    Me.lbl_num_disconnected_sites.Text = "---"
    Me.lbl_num_disconnected_sites.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_disconnected_sites
    '
    Me.lbl_disconnected_sites.AutoSize = True
    Me.lbl_disconnected_sites.Font = New System.Drawing.Font("Verdana", 10.0!, System.Drawing.FontStyle.Bold)
    Me.lbl_disconnected_sites.Location = New System.Drawing.Point(642, 18)
    Me.lbl_disconnected_sites.Name = "lbl_disconnected_sites"
    Me.lbl_disconnected_sites.Size = New System.Drawing.Size(157, 17)
    Me.lbl_disconnected_sites.TabIndex = 6
    Me.lbl_disconnected_sites.Text = "xDisconnectedSites"
    '
    'btn_force_sync
    '
    Me.btn_force_sync.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.btn_force_sync.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btn_force_sync.Location = New System.Drawing.Point(1096, 535)
    Me.btn_force_sync.Name = "btn_force_sync"
    Me.btn_force_sync.Size = New System.Drawing.Size(84, 26)
    Me.btn_force_sync.TabIndex = 10
    Me.btn_force_sync.Text = "xForceSync"
    Me.btn_force_sync.UseVisualStyleBackColor = True
    '
    'frm_sites_monitor
    '
    Me.AcceptButton = Me.btn_refresh
    Me.CancelButton = Me.btn_exit
    Me.ClientSize = New System.Drawing.Size(1184, 646)
    Me.Controls.Add(Me.btn_force_sync)
    Me.Controls.Add(Me.lbl_num_disconnected_sites)
    Me.Controls.Add(Me.lbl_disconnected_sites)
    Me.Controls.Add(Me.lbl_num_connected_sites)
    Me.Controls.Add(Me.lbl_connected_sites)
    Me.Controls.Add(Me.lbl_stopped)
    Me.Controls.Add(Me.lbl_wwp_status_value)
    Me.Controls.Add(Me.lbl_wwp_status)
    Me.Controls.Add(Me.btn_refresh)
    Me.Controls.Add(Me.btn_exit)
    Me.Controls.Add(Me.tf_last_update)
    Me.Controls.Add(Me.TabControl)
    Me.MinimumSize = New System.Drawing.Size(1012, 582)
    Me.Name = "frm_sites_monitor"
    Me.Text = "xSitesMonitor"
    Me.TabControl.ResumeLayout(False)
    Me.tab_sites.ResumeLayout(False)
    Me.tab_sites.PerformLayout()
    Me.gb_sites_tab_status.ResumeLayout(False)
    Me.gb_sites_tab_status.PerformLayout()
    Me.tab_processes.ResumeLayout(False)
    Me.tab_processes.PerformLayout()
    Me.gb_tasks_tab_status.ResumeLayout(False)
    Me.gb_tasks_tab_status.PerformLayout()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents tf_last_update As GUI_Controls.uc_text_field
  Friend WithEvents TabControl As System.Windows.Forms.TabControl
  Friend WithEvents tab_sites As System.Windows.Forms.TabPage
  Friend WithEvents ef_site As GUI_Controls.uc_entry_field
  Friend WithEvents gb_sites_tab_status As System.Windows.Forms.GroupBox
  Friend WithEvents opt_sites_tab_disconnected As System.Windows.Forms.RadioButton
  Friend WithEvents opt_sites_tab_synchronize As System.Windows.Forms.RadioButton
  Friend WithEvents opt_sites_tab_all_status As System.Windows.Forms.RadioButton
  Friend WithEvents dg_site_tasks As GUI_Controls.uc_grid
  Friend WithEvents dg_sites As GUI_Controls.uc_grid
  Friend WithEvents tab_processes As System.Windows.Forms.TabPage
  Friend WithEvents cmb_processes As GUI_Controls.uc_combo
  Friend WithEvents dg_tasks As GUI_Controls.uc_grid
  Friend WithEvents timer As System.Windows.Forms.Timer
  Friend WithEvents btn_exit As System.Windows.Forms.Button
  Friend WithEvents btn_refresh As System.Windows.Forms.Button
  Friend WithEvents lbl_tasks As System.Windows.Forms.Label
  Friend WithEvents lbl_sites As System.Windows.Forms.Label
  Friend WithEvents lbl_wwp_status As System.Windows.Forms.Label
  Friend WithEvents lbl_wwp_status_value As System.Windows.Forms.Label
  Friend WithEvents lbl_tasks_taks As System.Windows.Forms.Label
  Friend WithEvents gb_tasks_tab_status As System.Windows.Forms.GroupBox
  Friend WithEvents opt_tasks_tab_disconnected As System.Windows.Forms.RadioButton
  Friend WithEvents opt_tasks_tab_synchronize As System.Windows.Forms.RadioButton
  Friend WithEvents opt_tasks_tab_all_status As System.Windows.Forms.RadioButton
  Friend WithEvents lbl_stopped As System.Windows.Forms.Label
  Friend WithEvents lbl_connected_sites As System.Windows.Forms.Label
  Friend WithEvents lbl_num_connected_sites As System.Windows.Forms.Label
  Friend WithEvents lbl_num_disconnected_sites As System.Windows.Forms.Label
  Friend WithEvents lbl_disconnected_sites As System.Windows.Forms.Label
  Friend WithEvents btn_force_sync As System.Windows.Forms.Button
  Friend WithEvents lbl_site_id As System.Windows.Forms.Label
  Friend WithEvents lbl_sites_tab_color_synchronize As System.Windows.Forms.Label
  Friend WithEvents lbl_sites_tab_color_conected As System.Windows.Forms.Label
  Friend WithEvents lbl_sites_tab_color_disconected As System.Windows.Forms.Label
  Friend WithEvents lbl_tasks_tab_color_synchronize As System.Windows.Forms.Label
  Friend WithEvents lbl_tasks_tab_color_conected As System.Windows.Forms.Label
  Friend WithEvents lbl_tasks_tab_color_disconected As System.Windows.Forms.Label
  Friend WithEvents opt_sites_tab_connected_not_synchronize As System.Windows.Forms.RadioButton
  Friend WithEvents opt_tasks_tab_connected_not_synchronize As System.Windows.Forms.RadioButton
  Friend WithEvents lbl_sites_tab_color_all As System.Windows.Forms.Label
  Friend WithEvents lbl_tasks_tab_color_all As System.Windows.Forms.Label
End Class
