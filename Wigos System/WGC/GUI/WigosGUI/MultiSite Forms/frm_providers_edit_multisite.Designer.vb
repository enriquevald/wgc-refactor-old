<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_providers_edit_multisite
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.panel_games_data = New System.Windows.Forms.Panel
    Me.btn_delete_game = New System.Windows.Forms.Button
    Me.btn_add_game = New System.Windows.Forms.Button
    Me.dg_games = New GUI_Controls.uc_grid
    Me.ef_providers_games_name = New GUI_Controls.uc_entry_field
    Me.ef_providers_games_ip = New GUI_Controls.uc_entry_field
    Me.gb_provider = New System.Windows.Forms.GroupBox
    Me.gb_site = New System.Windows.Forms.GroupBox
    Me.ef_providers_games_multiplier = New GUI_Controls.uc_entry_field
    Me.chk_jackpot = New System.Windows.Forms.CheckBox
    Me.chk_onlyredeemable = New System.Windows.Forms.CheckBox
    Me.gb_3gs = New System.Windows.Forms.GroupBox
    Me.cmb_3gs_status = New GUI_Controls.uc_combo
    Me.lbl_3gs_status = New System.Windows.Forms.Label
    Me.panel_data.SuspendLayout()
    Me.panel_games_data.SuspendLayout()
    Me.gb_provider.SuspendLayout()
    Me.gb_site.SuspendLayout()
    Me.gb_3gs.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.panel_games_data)
    Me.panel_data.Controls.Add(Me.gb_3gs)
    Me.panel_data.Controls.Add(Me.gb_provider)
    Me.panel_data.Location = New System.Drawing.Point(5, 4)
    Me.panel_data.Size = New System.Drawing.Size(944, 377)
    '
    'panel_games_data
    '
    Me.panel_games_data.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.panel_games_data.AutoSize = True
    Me.panel_games_data.Controls.Add(Me.btn_delete_game)
    Me.panel_games_data.Controls.Add(Me.btn_add_game)
    Me.panel_games_data.Controls.Add(Me.dg_games)
    Me.panel_games_data.Location = New System.Drawing.Point(257, 4)
    Me.panel_games_data.Name = "panel_games_data"
    Me.panel_games_data.Size = New System.Drawing.Size(684, 367)
    Me.panel_games_data.TabIndex = 21
    '
    'btn_delete_game
    '
    Me.btn_delete_game.Location = New System.Drawing.Point(385, 333)
    Me.btn_delete_game.Name = "btn_delete_game"
    Me.btn_delete_game.Size = New System.Drawing.Size(63, 21)
    Me.btn_delete_game.TabIndex = 26
    Me.btn_delete_game.Text = "btn"
    Me.btn_delete_game.UseVisualStyleBackColor = True
    '
    'btn_add_game
    '
    Me.btn_add_game.Location = New System.Drawing.Point(216, 333)
    Me.btn_add_game.Name = "btn_add_game"
    Me.btn_add_game.Size = New System.Drawing.Size(63, 21)
    Me.btn_add_game.TabIndex = 25
    Me.btn_add_game.Text = "btn"
    Me.btn_add_game.UseVisualStyleBackColor = True
    '
    'dg_games
    '
    Me.dg_games.CurrentCol = -1
    Me.dg_games.CurrentRow = -1
    Me.dg_games.Dock = System.Windows.Forms.DockStyle.Top
    Me.dg_games.Location = New System.Drawing.Point(0, 0)
    Me.dg_games.Name = "dg_games"
    Me.dg_games.PanelRightVisible = False
    Me.dg_games.Redraw = True
    Me.dg_games.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
    Me.dg_games.Size = New System.Drawing.Size(684, 324)
    Me.dg_games.Sortable = False
    Me.dg_games.SortAscending = True
    Me.dg_games.SortByCol = 0
    Me.dg_games.TabIndex = 24
    Me.dg_games.ToolTipped = True
    Me.dg_games.TopRow = -2
    '
    'ef_providers_games_name
    '
    Me.ef_providers_games_name.DoubleValue = 0
    Me.ef_providers_games_name.IntegerValue = 0
    Me.ef_providers_games_name.IsReadOnly = False
    Me.ef_providers_games_name.Location = New System.Drawing.Point(6, 20)
    Me.ef_providers_games_name.Name = "ef_providers_games_name"
    Me.ef_providers_games_name.Size = New System.Drawing.Size(232, 24)
    Me.ef_providers_games_name.SufixText = "Sufix Text"
    Me.ef_providers_games_name.SufixTextVisible = True
    Me.ef_providers_games_name.TabIndex = 12
    Me.ef_providers_games_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_providers_games_name.TextValue = ""
    Me.ef_providers_games_name.TextVisible = False
    Me.ef_providers_games_name.TextWidth = 70
    Me.ef_providers_games_name.Value = ""
    '
    'ef_providers_games_ip
    '
    Me.ef_providers_games_ip.DoubleValue = 0
    Me.ef_providers_games_ip.IntegerValue = 0
    Me.ef_providers_games_ip.IsReadOnly = False
    Me.ef_providers_games_ip.Location = New System.Drawing.Point(6, 47)
    Me.ef_providers_games_ip.Name = "ef_providers_games_ip"
    Me.ef_providers_games_ip.Size = New System.Drawing.Size(232, 24)
    Me.ef_providers_games_ip.SufixText = "Sufix Text"
    Me.ef_providers_games_ip.SufixTextVisible = True
    Me.ef_providers_games_ip.TabIndex = 20
    Me.ef_providers_games_ip.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_providers_games_ip.TextValue = ""
    Me.ef_providers_games_ip.TextVisible = False
    Me.ef_providers_games_ip.TextWidth = 100
    Me.ef_providers_games_ip.Value = ""
    '
    'gb_provider
    '
    Me.gb_provider.Controls.Add(Me.gb_site)
    Me.gb_provider.Controls.Add(Me.ef_providers_games_name)
    Me.gb_provider.Location = New System.Drawing.Point(4, 4)
    Me.gb_provider.Name = "gb_provider"
    Me.gb_provider.Size = New System.Drawing.Size(247, 159)
    Me.gb_provider.TabIndex = 1
    Me.gb_provider.TabStop = False
    Me.gb_provider.Text = "xProvider"
    '
    'gb_site
    '
    Me.gb_site.Controls.Add(Me.ef_providers_games_multiplier)
    Me.gb_site.Controls.Add(Me.chk_jackpot)
    Me.gb_site.Controls.Add(Me.chk_onlyredeemable)
    Me.gb_site.Location = New System.Drawing.Point(9, 50)
    Me.gb_site.Name = "gb_site"
    Me.gb_site.Size = New System.Drawing.Size(229, 100)
    Me.gb_site.TabIndex = 18
    Me.gb_site.TabStop = False
    Me.gb_site.Text = "xSite"
    '
    'ef_providers_games_multiplier
    '
    Me.ef_providers_games_multiplier.DoubleValue = 0
    Me.ef_providers_games_multiplier.IntegerValue = 0
    Me.ef_providers_games_multiplier.IsReadOnly = False
    Me.ef_providers_games_multiplier.Location = New System.Drawing.Point(6, 20)
    Me.ef_providers_games_multiplier.Name = "ef_providers_games_multiplier"
    Me.ef_providers_games_multiplier.Size = New System.Drawing.Size(212, 24)
    Me.ef_providers_games_multiplier.SufixText = "Sufix Text"
    Me.ef_providers_games_multiplier.SufixTextVisible = True
    Me.ef_providers_games_multiplier.TabIndex = 13
    Me.ef_providers_games_multiplier.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_providers_games_multiplier.TextValue = ""
    Me.ef_providers_games_multiplier.TextVisible = False
    Me.ef_providers_games_multiplier.TextWidth = 155
    Me.ef_providers_games_multiplier.Value = ""
    '
    'chk_jackpot
    '
    Me.chk_jackpot.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.chk_jackpot.Location = New System.Drawing.Point(6, 51)
    Me.chk_jackpot.Name = "chk_jackpot"
    Me.chk_jackpot.Size = New System.Drawing.Size(212, 17)
    Me.chk_jackpot.TabIndex = 16
    Me.chk_jackpot.Text = "xJackpot"
    Me.chk_jackpot.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.chk_jackpot.UseVisualStyleBackColor = True
    '
    'chk_onlyredeemable
    '
    Me.chk_onlyredeemable.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.chk_onlyredeemable.Location = New System.Drawing.Point(6, 74)
    Me.chk_onlyredeemable.Name = "chk_onlyredeemable"
    Me.chk_onlyredeemable.Size = New System.Drawing.Size(212, 17)
    Me.chk_onlyredeemable.TabIndex = 17
    Me.chk_onlyredeemable.Text = "xOnlyredeemable"
    Me.chk_onlyredeemable.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.chk_onlyredeemable.UseVisualStyleBackColor = True
    '
    'gb_3gs
    '
    Me.gb_3gs.Controls.Add(Me.cmb_3gs_status)
    Me.gb_3gs.Controls.Add(Me.lbl_3gs_status)
    Me.gb_3gs.Controls.Add(Me.ef_providers_games_ip)
    Me.gb_3gs.Location = New System.Drawing.Point(4, 169)
    Me.gb_3gs.Name = "gb_3gs"
    Me.gb_3gs.Size = New System.Drawing.Size(247, 78)
    Me.gb_3gs.TabIndex = 2
    Me.gb_3gs.TabStop = False
    Me.gb_3gs.Text = "x3gs"
    '
    'cmb_3gs_status
    '
    Me.cmb_3gs_status.IsReadOnly = False
    Me.cmb_3gs_status.Location = New System.Drawing.Point(107, 17)
    Me.cmb_3gs_status.Name = "cmb_3gs_status"
    Me.cmb_3gs_status.SelectedIndex = -1
    Me.cmb_3gs_status.Size = New System.Drawing.Size(131, 24)
    Me.cmb_3gs_status.SufixText = "Sufix Text"
    Me.cmb_3gs_status.SufixTextVisible = True
    Me.cmb_3gs_status.TabIndex = 23
    Me.cmb_3gs_status.TextWidth = 0
    '
    'lbl_3gs_status
    '
    Me.lbl_3gs_status.Location = New System.Drawing.Point(6, 23)
    Me.lbl_3gs_status.Name = "lbl_3gs_status"
    Me.lbl_3gs_status.Size = New System.Drawing.Size(98, 13)
    Me.lbl_3gs_status.TabIndex = 22
    Me.lbl_3gs_status.Text = "xStatus"
    Me.lbl_3gs_status.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'frm_providers_edit_multisite
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1046, 415)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.MaximumSize = New System.Drawing.Size(1052, 443)
    Me.MinimumSize = New System.Drawing.Size(1052, 443)
    Me.Name = "frm_providers_edit_multisite"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_providers_edit"
    Me.panel_data.ResumeLayout(False)
    Me.panel_data.PerformLayout()
    Me.panel_games_data.ResumeLayout(False)
    Me.gb_provider.ResumeLayout(False)
    Me.gb_site.ResumeLayout(False)
    Me.gb_3gs.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents panel_games_data As System.Windows.Forms.Panel
  Friend WithEvents gb_3gs As System.Windows.Forms.GroupBox
  Friend WithEvents ef_providers_games_ip As GUI_Controls.uc_entry_field
  Friend WithEvents gb_provider As System.Windows.Forms.GroupBox
  Friend WithEvents ef_providers_games_name As GUI_Controls.uc_entry_field
  Friend WithEvents cmb_3gs_status As GUI_Controls.uc_combo
  Friend WithEvents lbl_3gs_status As System.Windows.Forms.Label
  Friend WithEvents dg_games As GUI_Controls.uc_grid
  Friend WithEvents btn_delete_game As System.Windows.Forms.Button
  Friend WithEvents btn_add_game As System.Windows.Forms.Button
  Friend WithEvents gb_site As System.Windows.Forms.GroupBox
  Friend WithEvents ef_providers_games_multiplier As GUI_Controls.uc_entry_field
  Friend WithEvents chk_jackpot As System.Windows.Forms.CheckBox
  Friend WithEvents chk_onlyredeemable As System.Windows.Forms.CheckBox
End Class
