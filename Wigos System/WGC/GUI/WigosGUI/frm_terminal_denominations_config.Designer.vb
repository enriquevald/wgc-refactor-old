<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_terminal_denominations_config
  Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container
    Me.gb_SAS = New System.Windows.Forms.GroupBox
    Me.tlp_SAS = New System.Windows.Forms.TableLayoutPanel
    Me.btn_create = New GUI_Controls.uc_button
    Me.btn_remove = New GUI_Controls.uc_button
    Me.lv_single = New System.Windows.Forms.ListView
    Me.lv_multi = New System.Windows.Forms.ListView
    Me.pnl_buttons = New System.Windows.Forms.Panel
    Me.flp_main = New System.Windows.Forms.FlowLayoutPanel
    Me.pnl_list = New System.Windows.Forms.Panel
    Me.lbl_multi = New System.Windows.Forms.Label
    Me.lbl_single = New System.Windows.Forms.Label
    Me.tmr_highlighting = New System.Windows.Forms.Timer(Me.components)
    Me.panel_data.SuspendLayout()
    Me.gb_SAS.SuspendLayout()
    Me.pnl_buttons.SuspendLayout()
    Me.flp_main.SuspendLayout()
    Me.pnl_list.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.flp_main)
    Me.panel_data.Size = New System.Drawing.Size(904, 377)
    '
    'gb_SAS
    '
    Me.gb_SAS.Controls.Add(Me.tlp_SAS)
    Me.gb_SAS.Location = New System.Drawing.Point(3, 3)
    Me.gb_SAS.Name = "gb_SAS"
    Me.gb_SAS.Size = New System.Drawing.Size(362, 371)
    Me.gb_SAS.TabIndex = 7
    Me.gb_SAS.TabStop = False
    Me.gb_SAS.Text = "xSAS"
    '
    'tlp_SAS
    '
    Me.tlp_SAS.AutoScroll = True
    Me.tlp_SAS.ColumnCount = 1
    Me.tlp_SAS.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
    Me.tlp_SAS.Dock = System.Windows.Forms.DockStyle.Fill
    Me.tlp_SAS.Location = New System.Drawing.Point(3, 17)
    Me.tlp_SAS.Name = "tlp_SAS"
    Me.tlp_SAS.RowCount = 1
    Me.tlp_SAS.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
    Me.tlp_SAS.Size = New System.Drawing.Size(356, 351)
    Me.tlp_SAS.TabIndex = 0
    '
    'btn_create
    '
    Me.btn_create.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_create.Location = New System.Drawing.Point(3, 144)
    Me.btn_create.Name = "btn_create"
    Me.btn_create.Size = New System.Drawing.Size(63, 21)
    Me.btn_create.TabIndex = 9
    Me.btn_create.ToolTipped = False
    Me.btn_create.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.SMALL
    '
    'btn_remove
    '
    Me.btn_remove.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_remove.Location = New System.Drawing.Point(3, 178)
    Me.btn_remove.Name = "btn_remove"
    Me.btn_remove.Size = New System.Drawing.Size(63, 21)
    Me.btn_remove.TabIndex = 10
    Me.btn_remove.ToolTipped = False
    Me.btn_remove.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.SMALL
    '
    'lv_single
    '
    Me.lv_single.BackColor = System.Drawing.SystemColors.Window
    Me.lv_single.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lv_single.FullRowSelect = True
    Me.lv_single.Location = New System.Drawing.Point(5, 17)
    Me.lv_single.Name = "lv_single"
    Me.lv_single.Size = New System.Drawing.Size(94, 348)
    Me.lv_single.TabIndex = 12
    Me.lv_single.UseCompatibleStateImageBehavior = False
    Me.lv_single.View = System.Windows.Forms.View.List
    '
    'lv_multi
    '
    Me.lv_multi.BackColor = System.Drawing.SystemColors.Window
    Me.lv_multi.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lv_multi.FullRowSelect = True
    Me.lv_multi.Location = New System.Drawing.Point(105, 17)
    Me.lv_multi.Name = "lv_multi"
    Me.lv_multi.Size = New System.Drawing.Size(347, 348)
    Me.lv_multi.TabIndex = 14
    Me.lv_multi.UseCompatibleStateImageBehavior = False
    Me.lv_multi.View = System.Windows.Forms.View.List
    '
    'pnl_buttons
    '
    Me.pnl_buttons.Controls.Add(Me.btn_create)
    Me.pnl_buttons.Controls.Add(Me.btn_remove)
    Me.pnl_buttons.Location = New System.Drawing.Point(371, 3)
    Me.pnl_buttons.Name = "pnl_buttons"
    Me.pnl_buttons.Size = New System.Drawing.Size(68, 368)
    Me.pnl_buttons.TabIndex = 16
    '
    'flp_main
    '
    Me.flp_main.AutoScroll = True
    Me.flp_main.AutoSize = True
    Me.flp_main.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.flp_main.Controls.Add(Me.gb_SAS)
    Me.flp_main.Controls.Add(Me.pnl_buttons)
    Me.flp_main.Controls.Add(Me.pnl_list)
    Me.flp_main.Dock = System.Windows.Forms.DockStyle.Fill
    Me.flp_main.Location = New System.Drawing.Point(0, 0)
    Me.flp_main.Name = "flp_main"
    Me.flp_main.Size = New System.Drawing.Size(904, 377)
    Me.flp_main.TabIndex = 17
    Me.flp_main.WrapContents = False
    '
    'pnl_list
    '
    Me.pnl_list.Controls.Add(Me.lbl_multi)
    Me.pnl_list.Controls.Add(Me.lv_multi)
    Me.pnl_list.Controls.Add(Me.lv_single)
    Me.pnl_list.Controls.Add(Me.lbl_single)
    Me.pnl_list.Location = New System.Drawing.Point(445, 3)
    Me.pnl_list.Name = "pnl_list"
    Me.pnl_list.Size = New System.Drawing.Size(455, 368)
    Me.pnl_list.TabIndex = 17
    '
    'lbl_multi
    '
    Me.lbl_multi.Location = New System.Drawing.Point(105, 0)
    Me.lbl_multi.Name = "lbl_multi"
    Me.lbl_multi.Size = New System.Drawing.Size(120, 13)
    Me.lbl_multi.TabIndex = 15
    Me.lbl_multi.Text = "xMulti"
    '
    'lbl_single
    '
    Me.lbl_single.Location = New System.Drawing.Point(5, 0)
    Me.lbl_single.Name = "lbl_single"
    Me.lbl_single.Size = New System.Drawing.Size(90, 13)
    Me.lbl_single.TabIndex = 0
    Me.lbl_single.Text = "xSingle"
    '
    'tmr_highlighting
    '
    '
    'frm_terminal_denominations_config
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1006, 388)
    Me.Name = "frm_terminal_denominations_config"
    Me.Text = "xTerminal denominations"
    Me.panel_data.ResumeLayout(False)
    Me.panel_data.PerformLayout()
    Me.gb_SAS.ResumeLayout(False)
    Me.pnl_buttons.ResumeLayout(False)
    Me.flp_main.ResumeLayout(False)
    Me.pnl_list.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_SAS As System.Windows.Forms.GroupBox
  Friend WithEvents btn_remove As GUI_Controls.uc_button
  Friend WithEvents btn_create As GUI_Controls.uc_button
  Friend WithEvents lv_single As System.Windows.Forms.ListView
  Friend WithEvents lv_multi As System.Windows.Forms.ListView
  Friend WithEvents flp_main As System.Windows.Forms.FlowLayoutPanel
  Friend WithEvents pnl_buttons As System.Windows.Forms.Panel
  Friend WithEvents pnl_list As System.Windows.Forms.Panel
  Friend WithEvents lbl_multi As System.Windows.Forms.Label
  Friend WithEvents lbl_single As System.Windows.Forms.Label
  Friend WithEvents tlp_SAS As System.Windows.Forms.TableLayoutPanel
  Friend WithEvents tmr_highlighting As System.Windows.Forms.Timer
End Class
