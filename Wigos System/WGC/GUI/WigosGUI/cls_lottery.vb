﻿'-------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : cls_lottery class
'
' DESCRIPTION : Class to manage lottery actions
'
' CREATION DATE : 22-JAN-2018
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 22-JAN-2018  DPC    Initial version.
'--------------------------------------------------------------------

Imports System.Text
Imports System.Data.SqlClient
Imports WSI.Common

Public Class cls_lottery

#Region " Public functions "

  ''' <summary>
  ''' Get the meters
  ''' </summary>
  ''' <param name="DatetimeFrom"></param>
  ''' <param name="Status"></param>
  ''' <param name="SiteId"></param>
  ''' <param name="SqlTrans"></param>
  ''' <param name="DataTableMeters"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function GetMetersPeriod(ByVal DatetimeFrom As DateTime, ByVal Status As Int32, ByVal SiteId As Int32, SqlTrans As SqlTransaction, ByRef DataTableMeters As DataTable) As Boolean

    Dim _sb As StringBuilder

    Try

      _sb = New StringBuilder
      _sb.AppendLine("     SELECT   EMP_WORKING_DAY                                            ")
      _sb.AppendLine("            , EMP_SITE_ID                							                   ")
      _sb.AppendLine("            , EMP_TERMINAL_ID            							                   ")
      _sb.AppendLine("            , EMP_GAME_ID                							                   ")
      _sb.AppendLine("            , EMP_DENOMINATION           							                   ")
      _sb.AppendLine("            , EMP_DATETIME               							                   ")
      _sb.AppendLine("            , EMP_RECORD_TYPE            							                   ")
      _sb.AppendLine("            , EMP_SAS_ACCOUNTING_DENOM   							                   ")
      _sb.AppendLine("            , EMP_LAST_CREATED_DATETIME  							                   ")
      _sb.AppendLine("            , EMP_LAST_REPORTED_DATETIME 							                   ")
      _sb.AppendLine("            , EMP_USER_IGNORED           							                   ")
      _sb.AppendLine("            , EMP_STATUS                 							                   ")
      _sb.AppendLine("            , EMP_USER_IGNORED_DATETIME                                  ")
      _sb.AppendLine("            , EMP_LAST_UPDATED_USER_ID                                   ")
      _sb.AppendLine("            , EMP_LAST_UPDATED_USER_DATETIME                             ")
      _sb.AppendLine("            , EMP_MC_0000                							                   ")
      _sb.AppendLine("            , EMP_MC_0001                							                   ")
      _sb.AppendLine("            , EMP_MC_0002                							                   ")
      _sb.AppendLine("            , EMP_MC_0005                							                   ")
      _sb.AppendLine("            , EMP_MC_0000_INCREMENT      							                   ")
      _sb.AppendLine("            , EMP_MC_0001_INCREMENT      							                   ")
      _sb.AppendLine("            , EMP_MC_0002_INCREMENT      							                   ")
      _sb.AppendLine("            , EMP_MC_0005_INCREMENT      							                   ")
      _sb.AppendLine("       FROM   EGM_METERS_BY_PERIOD                                       ")
      _sb.AppendLine(" INNER JOIN   EGM_DAILY                                                  ")
      _sb.AppendLine("         ON   ED_WORKING_DAY = EMP_WORKING_DAY                           ")
      _sb.AppendLine("        AND   ED_STATUS = @pStatus                                       ")
      _sb.AppendLine("      WHERE   EMP_DATETIME >= @pDatetimeFrom   				                   ")
      _sb.AppendLine("        AND   EMP_DATETIME < @pDatetimeTo  				                       ")
      _sb.AppendLine("        AND   dbo.GamingDayFromDateTime(EMP_DATETIME) <> EMP_WORKING_DAY ")
      _sb.AppendLine("        AND   EMP_SITE_ID = @pSiteId                                     ")

      Using _cmd As New SqlCommand(_sb.ToString, SqlTrans.Connection, SqlTrans)
        With _cmd.Parameters
          .Add("@pDatetimeFrom", SqlDbType.DateTime).Value = DatetimeFrom
          .Add("@pDatetimeTo", SqlDbType.DateTime).Value = DatetimeFrom.AddDays(1)
          .Add("@pStatus", SqlDbType.Int).Value = Status
          .Add("@pSiteId", SqlDbType.Int).Value = SiteId
        End With

        Using _sql_da As SqlDataAdapter = New SqlDataAdapter(_cmd)
          _sql_da.Fill(DataTableMeters)
        End Using
      End Using

      With DataTableMeters
        .PrimaryKey = New DataColumn() {.Columns("emp_datetime"), .Columns("EMP_WORKING_DAY"), .Columns("emp_terminal_id"), .Columns("emp_site_id"), .Columns("emp_denomination"), .Columns("emp_game_id")}
      End With

      Return True

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return False

  End Function ' GetMetersPeriod

  ''' <summary>
  ''' Save Pending Changes (Batch Update)
  ''' </summary>
  ''' <param name="DataTableMeters"></param>
  ''' <param name="SqlTrans"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function DB_SavePendingChangesByPeriod(ByVal DataTableMeters As DataTable, SqlTrans As SqlTransaction, Optional ByVal RecordTypeCut As Int32 = -1) As Boolean

    Dim _sb_insert As StringBuilder
    Dim _sb_update As StringBuilder
    Dim _sb_delete As StringBuilder
    Dim _num_rows_affected As Long

    Try
      ' Insert
      _sb_insert = New StringBuilder()
      _sb_insert.AppendLine(" INSERT INTO   EGM_METERS_BY_PERIOD          ")
      _sb_insert.AppendLine("             ( EMP_WORKING_DAY               ")
      _sb_insert.AppendLine("             , EMP_SITE_ID                   ")
      _sb_insert.AppendLine("             , EMP_TERMINAL_ID               ")
      _sb_insert.AppendLine("             , EMP_GAME_ID                   ")
      _sb_insert.AppendLine("             , EMP_DENOMINATION              ")
      _sb_insert.AppendLine("             , EMP_DATETIME                  ")
      _sb_insert.AppendLine("             , EMP_RECORD_TYPE               ")
      _sb_insert.AppendLine("             , EMP_SAS_ACCOUNTING_DENOM      ")
      _sb_insert.AppendLine("             , EMP_LAST_CREATED_DATETIME     ")
      _sb_insert.AppendLine("             , EMP_LAST_REPORTED_DATETIME    ")
      _sb_insert.AppendLine("             , EMP_USER_IGNORED              ")
      _sb_insert.AppendLine("             , EMP_STATUS                    ")
      _sb_insert.AppendLine("             , EMP_MC_0000                   ")
      _sb_insert.AppendLine("             , EMP_MC_0001                   ")
      _sb_insert.AppendLine("             , EMP_MC_0002                   ")
      _sb_insert.AppendLine("             , EMP_MC_0005                   ")
      _sb_insert.AppendLine("             , EMP_MC_0000_INCREMENT         ")
      _sb_insert.AppendLine("             , EMP_MC_0001_INCREMENT         ")
      _sb_insert.AppendLine("             , EMP_MC_0002_INCREMENT         ")
      _sb_insert.AppendLine("             , EMP_MC_0005_INCREMENT         ")
      _sb_insert.AppendLine("             , EMP_USER_IGNORED_DATETIME     ")
      _sb_insert.AppendLine("             , EMP_LAST_UPDATED_USER_ID      ")
      _sb_insert.AppendLine("             , EMP_LAST_UPDATED_USER_DATETIME")
      _sb_insert.AppendLine("             )                               ")
      _sb_insert.AppendLine("      VALUES ( @pWorkingDay                  ")
      _sb_insert.AppendLine("             , @pSiteId                      ")
      _sb_insert.AppendLine("             , @pTerminalId                  ")
      _sb_insert.AppendLine("             , @pGameId                      ")
      _sb_insert.AppendLine("             , @pDenomination                ")
      _sb_insert.AppendLine("             , @pDateTime                    ")
      _sb_insert.AppendLine("             , @pRecordType                  ")
      _sb_insert.AppendLine("             , @pSasAccountDenom             ")
      _sb_insert.AppendLine("             , GETDATE()                     ")
      _sb_insert.AppendLine("             , @pLastReportedDateTime        ")
      _sb_insert.AppendLine("             , 0                             ")
      _sb_insert.AppendLine("             , 0                             ")
      _sb_insert.AppendLine("             , @pMc_0000                     ")
      _sb_insert.AppendLine("             , @pMc_0001                     ")
      _sb_insert.AppendLine("             , @pMc_0002                     ")
      _sb_insert.AppendLine("             , 0                             ")
      _sb_insert.AppendLine("             , @pMc_0000_increment           ")
      _sb_insert.AppendLine("             , @pMc_0001_increment           ")
      _sb_insert.AppendLine("             , @pMc_0002_increment           ")
      _sb_insert.AppendLine("             , 0                             ")
      _sb_insert.AppendLine("             , @pUserIgnoredDatetime         ")
      _sb_insert.AppendLine("             , @pLastUpdatedUserId           ")
      _sb_insert.AppendLine("             , @pNow                         ")
      _sb_insert.AppendLine("             )                               ")

      ' Update
      _sb_update = New StringBuilder()
      _sb_update.AppendLine(" UPDATE   EGM_METERS_BY_PERIOD                                                                                             ")
      _sb_update.AppendLine("    SET   EMP_MC_0000                    = CASE WHEN EMP_RECORD_TYPE = @pRecordTypeCut THEN @pMc_0000 ELSE EMP_MC_0000 END ")
      _sb_update.AppendLine("        , EMP_MC_0001                    = CASE WHEN EMP_RECORD_TYPE = @pRecordTypeCut THEN @pMc_0001 ELSE EMP_MC_0001 END ")
      _sb_update.AppendLine("        , EMP_MC_0002                    = CASE WHEN EMP_RECORD_TYPE = @pRecordTypeCut THEN @pMc_0002 ELSE EMP_MC_0002 END ")
      _sb_update.AppendLine("        , EMP_MC_0000_INCREMENT          = @pMc_0000_increment                                                             ")
      _sb_update.AppendLine("        , EMP_MC_0001_INCREMENT          = @pMc_0001_increment                                                             ")
      _sb_update.AppendLine("        , EMP_MC_0002_INCREMENT          = @pMc_0002_increment                                                             ")
      _sb_update.AppendLine("        , EMP_USER_IGNORED               = @pUserIgnored                                                                   ")
      _sb_update.AppendLine("        , EMP_USER_IGNORED_DATETIME      = @pUserIgnoredDatetime                                                           ")
      _sb_update.AppendLine("        , EMP_LAST_UPDATED_USER_ID       = @pLastUpdatedUserId                                                             ")
      _sb_update.AppendLine("        , EMP_LAST_UPDATED_USER_DATETIME = @pNow                                                                           ")
      _sb_update.AppendLine("  WHERE   EMP_WORKING_DAY                = @pWorkingDay                                                                    ")
      _sb_update.AppendLine("    AND   EMP_SITE_ID                    = @pSiteId                                                                        ")
      _sb_update.AppendLine("    AND   EMP_TERMINAL_ID                = @pTerminalId                                                                    ")
      _sb_update.AppendLine("    AND   EMP_GAME_ID                    = @pGameId                                                                        ")
      _sb_update.AppendLine("    AND   EMP_DENOMINATION               = @pDenomination                                                                  ")
      _sb_update.AppendLine("    AND   EMP_DATETIME                   = @pDateTime                                                                      ")
      _sb_update.AppendLine("    AND   EMP_RECORD_TYPE                = @pRecordType                                                                    ")

      ' Delete
      _sb_delete = New StringBuilder()
      _sb_delete.AppendLine(" DELETE                                      ")
      _sb_delete.AppendLine("   FROM   EGM_METERS_BY_PERIOD               ")
      _sb_delete.AppendLine("  WHERE   EMP_WORKING_DAY  = @pWorkingDay    ")
      _sb_delete.AppendLine("    AND   EMP_SITE_ID      = @pSiteId        ")
      _sb_delete.AppendLine("    AND   EMP_TERMINAL_ID  = @pTerminalId    ")
      _sb_delete.AppendLine("    AND   EMP_GAME_ID      = @pGameId        ")
      _sb_delete.AppendLine("    AND   EMP_DENOMINATION = @pDenomination  ")
      _sb_delete.AppendLine("    AND   EMP_DATETIME     = @pDateTime      ")
      _sb_delete.AppendLine("    AND   EMP_RECORD_TYPE  = @pRecordTypeCut ")

      Using _sql_adap As SqlDataAdapter = New SqlDataAdapter()
        With _sql_adap
          .SelectCommand = Nothing

          ' Delete
          Using _sql_cmd = New SqlCommand(_sb_delete.ToString, SqlTrans.Connection, SqlTrans)
            _sql_cmd.Parameters.Add("@pworkingDay", SqlDbType.Int).SourceColumn = "EMP_WORKING_DAY"
            _sql_cmd.Parameters.Add("@pSiteId", SqlDbType.Int).SourceColumn = "EMP_SITE_ID"
            _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "EMP_TERMINAL_ID"
            _sql_cmd.Parameters.Add("@pGameId", SqlDbType.Int).SourceColumn = "EMP_GAME_ID"
            _sql_cmd.Parameters.Add("@pDenomination", SqlDbType.Money).SourceColumn = "EMP_DENOMINATION"
            _sql_cmd.Parameters.Add("@pDateTime", SqlDbType.DateTime).SourceColumn = "EMP_DATETIME"

            If RecordTypeCut = -1 Then
              _sql_cmd.Parameters.Add("@pRecordTypeCut", SqlDbType.Int).SourceColumn = "EMP_RECORD_TYPE"
            Else
              _sql_cmd.Parameters.Add("@pRecordTypeCut", SqlDbType.Int).Value = RecordTypeCut
            End If

            _sql_cmd.UpdatedRowSource = UpdateRowSource.OutputParameters
            .DeleteCommand = _sql_cmd
          End Using

          ' Update
          Using _sql_cmd = New SqlCommand(_sb_update.ToString, SqlTrans.Connection, SqlTrans)
            _sql_cmd.Parameters.Add("@pMc_0000", SqlDbType.BigInt).SourceColumn = "EMP_MC_0000"
            _sql_cmd.Parameters.Add("@pMc_0001", SqlDbType.BigInt).SourceColumn = "EMP_MC_0001"
            _sql_cmd.Parameters.Add("@pMc_0002", SqlDbType.BigInt).SourceColumn = "EMP_MC_0002"
            _sql_cmd.Parameters.Add("@pMc_0000_increment", SqlDbType.Decimal).SourceColumn = "EMP_MC_0000_INCREMENT"
            _sql_cmd.Parameters.Add("@pMc_0001_increment", SqlDbType.Decimal).SourceColumn = "EMP_MC_0001_INCREMENT"
            _sql_cmd.Parameters.Add("@pMc_0002_increment", SqlDbType.Decimal).SourceColumn = "EMP_MC_0002_INCREMENT"
            _sql_cmd.Parameters.Add("@pUserIgnored", SqlDbType.Bit).SourceColumn = "EMP_USER_IGNORED"
            _sql_cmd.Parameters.Add("@pUserIgnoredDatetime", SqlDbType.DateTime).SourceColumn = "EMP_USER_IGNORED_DATETIME"
            _sql_cmd.Parameters.Add("@pLastUpdatedUserId", SqlDbType.Int).SourceColumn = "EMP_LAST_UPDATED_USER_ID"
            _sql_cmd.Parameters.Add("@pNow", SqlDbType.DateTime).Value = WGDB.Now
            _sql_cmd.Parameters.Add("@pWorkingDay", SqlDbType.Int).SourceColumn = "EMP_WORKING_DAY"
            _sql_cmd.Parameters.Add("@pSiteId", SqlDbType.Int).SourceColumn = "EMP_SITE_ID"
            _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "EMP_TERMINAL_ID"
            _sql_cmd.Parameters.Add("@pGameId", SqlDbType.Int).SourceColumn = "EMP_GAME_ID"
            _sql_cmd.Parameters.Add("@pDenomination", SqlDbType.Money).SourceColumn = "EMP_DENOMINATION"
            _sql_cmd.Parameters.Add("@pDateTime", SqlDbType.DateTime).SourceColumn = "EMP_DATETIME"
            _sql_cmd.Parameters.Add("@pRecordType", SqlDbType.Int).SourceColumn = "EMP_RECORD_TYPE"

            If RecordTypeCut = -1 Then
              _sql_cmd.Parameters.Add("@pRecordTypeCut", SqlDbType.Int).SourceColumn = "EMP_RECORD_TYPE"
            Else
              _sql_cmd.Parameters.Add("@pRecordTypeCut", SqlDbType.Int).Value = RecordTypeCut
            End If

            _sql_cmd.UpdatedRowSource = UpdateRowSource.OutputParameters
            .UpdateCommand = _sql_cmd
          End Using

          ' Insert
          Using _sql_cmd = New SqlCommand(_sb_insert.ToString, SqlTrans.Connection, SqlTrans)
            _sql_cmd.Parameters.Add("@pWorkingDay", SqlDbType.Int).SourceColumn = "EMP_WORKING_DAY"
            _sql_cmd.Parameters.Add("@pSiteId", SqlDbType.Int).SourceColumn = "EMP_SITE_ID"
            _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "EMP_TERMINAL_ID"
            _sql_cmd.Parameters.Add("@pGameId", SqlDbType.Int).SourceColumn = "EMP_GAME_ID"
            _sql_cmd.Parameters.Add("@pDenomination", SqlDbType.Money).SourceColumn = "EMP_DENOMINATION"
            _sql_cmd.Parameters.Add("@pDateTime", SqlDbType.DateTime).SourceColumn = "EMP_DATETIME"
            _sql_cmd.Parameters.Add("@pRecordType", SqlDbType.Int).SourceColumn = "EMP_RECORD_TYPE"
            _sql_cmd.Parameters.Add("@pSasAccountDenom", SqlDbType.Money).SourceColumn = "EMP_SAS_ACCOUNTING_DENOM"
            _sql_cmd.Parameters.Add("@pLastReportedDateTime", SqlDbType.DateTime).SourceColumn = "EMP_LAST_REPORTED_DATETIME"
            _sql_cmd.Parameters.Add("@pStatus", SqlDbType.Int).SourceColumn = "EMP_STATUS"
            _sql_cmd.Parameters.Add("@pMc_0000", SqlDbType.BigInt).SourceColumn = "EMP_MC_0000"
            _sql_cmd.Parameters.Add("@pMc_0001", SqlDbType.BigInt).SourceColumn = "EMP_MC_0001"
            _sql_cmd.Parameters.Add("@pMc_0002", SqlDbType.BigInt).SourceColumn = "EMP_MC_0002"
            _sql_cmd.Parameters.Add("@pMc_0000_increment", SqlDbType.Decimal).SourceColumn = "EMP_MC_0000_INCREMENT"
            _sql_cmd.Parameters.Add("@pMc_0001_increment", SqlDbType.Decimal).SourceColumn = "EMP_MC_0001_INCREMENT"
            _sql_cmd.Parameters.Add("@pMc_0002_increment", SqlDbType.Decimal).SourceColumn = "EMP_MC_0002_INCREMENT"
            _sql_cmd.Parameters.Add("@pUserIgnoredDatetime", SqlDbType.DateTime).SourceColumn = "EMP_USER_IGNORED_DATETIME"
            _sql_cmd.Parameters.Add("@pLastUpdatedUserId", SqlDbType.Int).SourceColumn = "EMP_LAST_UPDATED_USER_ID"
            _sql_cmd.Parameters.Add("@pNow", SqlDbType.DateTime).Value = WGDB.Now

            _sql_cmd.UpdatedRowSource = UpdateRowSource.OutputParameters
            .InsertCommand = _sql_cmd

          End Using

          .UpdateBatchSize = 500

          _num_rows_affected = .Update(DataTableMeters)

          Return (_num_rows_affected > 0) ' Debug
        End With
      End Using
    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return False
  End Function ' DB_SavePendingChanges

  ''' <summary>
  ''' Save Pending Changes (Batch Update)
  ''' </summary>
  ''' <param name="DataTableMeters"></param>
  ''' <param name="SqlTrans"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function DB_SavePendingChangesByTerminal(ByVal DataTableMeters As DataTable, _
                                                         ByVal SiteId As Integer, _
                                                         ByVal WorkingDay As Integer, _
                                                         ByVal TerminalId As Integer, _
                                                         SqlTrans As SqlTransaction) As Boolean
    Dim _sb_update As StringBuilder
    Dim _increment_0000 As Object
    Dim _increment_0001 As Object
    Dim _increment_0002 As Object

    Try

      'Calculate Increments
      _increment_0000 = DataTableMeters.Compute("SUM(EMP_MC_0000_INCREMENT)", "EMP_RECORD_TYPE IN(" & ENUM_SAS_METER_HISTORY.TSMH_HOURLY & "," & _
                                                                                                      EGMMeterAdjustment.EGMMeterAdjustmentConstants.RECORD_TYPE_CUT & _
                                                                                                      ") AND EMP_USER_IGNORED = 0 AND EMP_WORKING_DAY = " & WorkingDay)

      _increment_0001 = DataTableMeters.Compute("SUM(EMP_MC_0001_INCREMENT)", "EMP_RECORD_TYPE IN(" & ENUM_SAS_METER_HISTORY.TSMH_HOURLY & "," & _
                                                                                                      EGMMeterAdjustment.EGMMeterAdjustmentConstants.RECORD_TYPE_CUT & _
                                                                                                      ") AND EMP_USER_IGNORED = 0 AND EMP_WORKING_DAY = " & WorkingDay)

      _increment_0002 = DataTableMeters.Compute("SUM(EMP_MC_0002_INCREMENT)", "EMP_RECORD_TYPE IN(" & ENUM_SAS_METER_HISTORY.TSMH_HOURLY & "," & _
                                                                                                      EGMMeterAdjustment.EGMMeterAdjustmentConstants.RECORD_TYPE_CUT & _
                                                                                                      ") AND EMP_USER_IGNORED = 0 AND EMP_WORKING_DAY = " & WorkingDay)


      _sb_update = New StringBuilder()
      _sb_update.AppendLine(" UPDATE   EGM_METERS_BY_DAY                                     ")
      _sb_update.AppendLine("    SET   EMD_MC_0000_INCREMENT          = @pMc_0000_increment  ")
      _sb_update.AppendLine("        , EMD_MC_0001_INCREMENT          = @pMc_0001_increment  ")
      _sb_update.AppendLine("        , EMD_MC_0002_INCREMENT          = @pMc_0002_increment  ")
      _sb_update.AppendLine("  WHERE   EMD_WORKING_DAY                = @pWorkingDay         ")
      _sb_update.AppendLine("    AND   EMD_SITE_ID                    = @pSiteId             ")
      _sb_update.AppendLine("    AND   EMD_TERMINAL_ID                = @pTerminalId         ")

      Using _sql_cmd = New SqlCommand(_sb_update.ToString, SqlTrans.Connection, SqlTrans)
        _sql_cmd.Parameters.Add("@pMc_0000_increment", SqlDbType.Decimal).value = IIf(IsDBNull(_increment_0000), 0, _increment_0000)
        _sql_cmd.Parameters.Add("@pMc_0001_increment", SqlDbType.Decimal).value = IIf(IsDBNull(_increment_0001), 0, _increment_0001)
        _sql_cmd.Parameters.Add("@pMc_0002_increment", SqlDbType.Decimal).value = IIf(IsDBNull(_increment_0002), 0, _increment_0002)
        _sql_cmd.Parameters.Add("@pSiteId", SqlDbType.Int).value = SiteId
        _sql_cmd.Parameters.Add("@pWorkingDay", SqlDbType.Int).value = WorkingDay
        _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).value = TerminalId

        Return _sql_cmd.ExecuteNonQuery() = 1
      End Using

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return False
  End Function ' DB_SavePendingChangesByTerminal


  ''' <summary>
  ''' Recalculate meters by working day
  ''' </summary>
  ''' <param name="Day"></param>
  ''' <param name="SqlTrans"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function RecalculatedMetersDay(ByVal Day As Int64(), SqlTrans As SqlTransaction) As Boolean

    Dim _sb As StringBuilder
    Dim _dt As DataTable
    Dim _dr As DataRow

    Try

      _dt = New DataTable

      _sb = New StringBuilder
      _sb.AppendLine(" SELECT   SUM(EMP_MC_0000_INCREMENT) AS EMP_MC_0000_INCREMENT ")
      _sb.AppendLine("        , SUM(EMP_MC_0001_INCREMENT) AS EMP_MC_0001_INCREMENT ")
      _sb.AppendLine(" 	      , SUM(EMP_MC_0002_INCREMENT) AS EMP_MC_0002_INCREMENT ")
      _sb.AppendLine(" 	      , SUM(EMP_MC_0005_INCREMENT) AS EMP_MC_0005_INCREMENT ")
      _sb.AppendLine(" 	      , SUM(EMP_MC_0000) AS EMP_MC_0000                     ")
      _sb.AppendLine(" 	      , SUM(EMP_MC_0001) AS EMP_MC_0001                     ")
      _sb.AppendLine(" 	      , SUM(EMP_MC_0002) AS EMP_MC_0002                     ")
      _sb.AppendLine(" 	      , SUM(EMP_MC_0005) AS EMP_MC_0005                     ")
      _sb.AppendLine("   FROM ( SELECT   EMP_MC_0000_INCREMENT                      ")
      _sb.AppendLine("                 , EMP_MC_0001_INCREMENT                      ")
      _sb.AppendLine("                 , EMP_MC_0002_INCREMENT                      ")
      _sb.AppendLine("                 , EMP_MC_0005_INCREMENT                      ")
      _sb.AppendLine("                 , 0 EMP_MC_0000                              ")
      _sb.AppendLine("                 , 0 EMP_MC_0001                              ")
      _sb.AppendLine("                 , 0 EMP_MC_0002                              ")
      _sb.AppendLine("                 , 0 EMP_MC_0005                              ")
      _sb.AppendLine("            FROM   EGM_METERS_BY_PERIOD                       ")
      _sb.AppendLine("           WHERE   EMP_WORKING_DAY = @pWorkingDay             ")
      _sb.AppendLine("             AND   EMP_SITE_ID = @pSiteId                     ")
      _sb.AppendLine("             AND   EMP_TERMINAL_ID = @pTerminalId             ")
      _sb.AppendLine("       UNION ALL                                              ")
      _sb.AppendLine("          SELECT   TOP 1                                      ")
      _sb.AppendLine("                   0 EMP_MC_0000_INCREMENT                    ")
      _sb.AppendLine("                 , 0 EMP_MC_0001_INCREMENT                    ")
      _sb.AppendLine("                 , 0 EMP_MC_0002_INCREMENT                    ")
      _sb.AppendLine("                 , 0 EMP_MC_0005_INCREMENT                    ")
      _sb.AppendLine("                 , EMP_MC_0000                                ")
      _sb.AppendLine("                 , EMP_MC_0001                                ")
      _sb.AppendLine("                 , EMP_MC_0002                                ")
      _sb.AppendLine("                 , EMP_MC_0005                                ")
      _sb.AppendLine("            FROM   EGM_METERS_BY_PERIOD                       ")
      _sb.AppendLine("           WHERE   EMP_WORKING_DAY = @pWorkingDay             ")
      _sb.AppendLine("             AND   EMP_SITE_ID = @pSiteId                     ")
      _sb.AppendLine("             AND   EMP_TERMINAL_ID = @pTerminalId             ")
      _sb.AppendLine("        ORDER BY   EMP_DATETIME DESC                          ")
      _sb.AppendLine(" 	      ) AS T                                                ")

      Using _cmd As New SqlCommand(_sb.ToString, SqlTrans.Connection, SqlTrans)

        _cmd.Parameters.Add("@pWorkingDay", SqlDbType.BigInt).Value = Day(0)
        _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = Day(1)
        _cmd.Parameters.Add("@pSiteId", SqlDbType.BigInt).Value = Day(2)

        Using _sql_da As SqlDataAdapter = New SqlDataAdapter(_cmd)
          _sql_da.Fill(_dt)
        End Using

      End Using

      _sb = New StringBuilder
      _sb.AppendLine(" UPDATE   EGM_METERS_BY_DAY                       ")
      _sb.AppendLine("    SET   EMD_MC_0000_INCREMENT = @pIncrement0000 ")
      _sb.AppendLine("        , EMD_MC_0001_INCREMENT = @pIncrement0001 ")
      _sb.AppendLine("        , EMD_MC_0002_INCREMENT = @pIncrement0002 ")
      _sb.AppendLine("        , EMD_MC_0005_INCREMENT = @pIncrement0005 ")
      _sb.AppendLine("        , EMD_MC_0000           = @pIni0000       ")
      _sb.AppendLine("        , EMD_MC_0001           = @pIni0001       ")
      _sb.AppendLine("        , EMD_MC_0002           = @pIni0002       ")
      _sb.AppendLine("        , EMD_MC_0005           = @pIni0005       ")
      _sb.AppendLine(" WHERE    EMD_WORKING_DAY = @pWorkingDay          ")
      _sb.AppendLine("   AND    EMD_TERMINAL_ID = @pTerminalId          ")
      _sb.AppendLine("   AND    EMD_SITE_ID     = @pSiteId              ")

      Using _cmd As New SqlCommand(_sb.ToString, SqlTrans.Connection, SqlTrans)

        If _dt.Rows.Count > 0 Then

          _dr = _dt.Rows(0)

          _cmd.Parameters.Add("@pIncrement0000", SqlDbType.BigInt).Value = _dr.Item("EMP_MC_0000_INCREMENT")
          _cmd.Parameters.Add("@pIncrement0001", SqlDbType.BigInt).Value = _dr.Item("EMP_MC_0001_INCREMENT")
          _cmd.Parameters.Add("@pIncrement0002", SqlDbType.BigInt).Value = _dr.Item("EMP_MC_0002_INCREMENT")
          _cmd.Parameters.Add("@pIncrement0005", SqlDbType.BigInt).Value = _dr.Item("EMP_MC_0005_INCREMENT")
          _cmd.Parameters.Add("@pIni0000", SqlDbType.BigInt).Value = _dr.Item("EMP_MC_0000")
          _cmd.Parameters.Add("@pIni0001", SqlDbType.BigInt).Value = _dr.Item("EMP_MC_0001")
          _cmd.Parameters.Add("@pIni0002", SqlDbType.BigInt).Value = _dr.Item("EMP_MC_0002")
          _cmd.Parameters.Add("@pIni0005", SqlDbType.BigInt).Value = _dr.Item("EMP_MC_0005")
          _cmd.Parameters.Add("@pWorkingDay", SqlDbType.BigInt).Value = Day(0)
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = Day(1)
          _cmd.Parameters.Add("@pSiteId", SqlDbType.BigInt).Value = Day(2)

          If _cmd.ExecuteNonQuery <> 1 Then

            Return False
          End If

        End If

      End Using

      Return True

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return False

  End Function ' RecalculatedMetersDay

  ''' <summary>
  ''' Update status by working day
  ''' </summary>
  ''' <param name="Status"></param>
  ''' <param name="User"></param>
  ''' <param name="WorkingDay"></param>
  ''' <param name="SiteId"></param>
  ''' <param name="SqlTrans"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function UpdateStatusWorkingDay(ByVal Status As Int32, ByVal User As String, ByVal WorkingDay As Int64, ByVal SiteId As Int64, ByVal NumTerminals As Int32, SqlTrans As SqlTransaction) As Boolean

    Dim _sb As StringBuilder

    Try

      _sb = New StringBuilder
      _sb.AppendLine("  UPDATE    EGM_DAILY                                ")
      _sb.AppendLine("     SET    ED_STATUS                     = @pStatus ")
      _sb.AppendLine("          , ED_LAST_UPDATED_USER          = @pUser   ")
      _sb.AppendLine("          , ED_LAST_UPDATED_USER_DATETIME = @pNow    ")
      _sb.AppendLine("          , ED_TERMINALS_CONNECTED = @NumTerminals   ")
      _sb.AppendLine("   WHERE    ED_WORKING_DAY = @pWorkingDay            ")
      _sb.AppendLine("    AND     ED_SITE_ID     = @pSiteId                ")

      Using _cmd As New SqlCommand(_sb.ToString, SqlTrans.Connection, SqlTrans)

        _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = Status
        _cmd.Parameters.Add("@pUser", SqlDbType.NVarChar).Value = User
        _cmd.Parameters.Add("@pNow", SqlDbType.DateTime).Value = WGDB.Now
        _cmd.Parameters.Add("@pWorkingDay", SqlDbType.BigInt).Value = WorkingDay
        _cmd.Parameters.Add("@pSiteId", SqlDbType.BigInt).Value = SiteId
        _cmd.Parameters.Add("@NumTerminals", SqlDbType.Int).Value = NumTerminals

        If _cmd.ExecuteNonQuery() <> 1 Then

          Return False
        End If

      End Using

      Return True

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return False

  End Function ' UpdateStatusWorkingDay

  ''' <summary>
  ''' Convert date to big int
  ''' </summary>
  ''' <param name="DateValue"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function DateToBigInt(ByVal DateValue As Date) As Int64
    Return Int64.Parse(DateValue.Year.ToString().PadLeft(4, "0") & DateValue.Month.ToString().PadLeft(2, "0") & DateValue.Day.ToString().PadLeft(2, "0"))
  End Function ' DateToBigInt

  ''' <summary>
  ''' Convert big int to date
  ''' </summary>
  ''' <param name="DateValue"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function BigIntToDate(ByVal DateValue As Int64) As DateTime

    Dim _date As String

    _date = DateValue.ToString()

    Return New DateTime(_date.Substring(0, 4), _date.Substring(4, 2), _date.Substring(6, 2))
  End Function ' BigIntToDate

#End Region

End Class
