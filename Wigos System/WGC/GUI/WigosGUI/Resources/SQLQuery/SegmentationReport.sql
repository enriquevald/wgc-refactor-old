--------------------------------------------------------------------------------
-- Copyright � 2012 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: SegmentationReport.sql
--   DESCRIPTION: Query for the segmentation report
--        AUTHOR: Daniel Dom�nguez
-- CREATION DATE: 12-APR-2012
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 12-APR-2012 DDM    First release.
-- 18-APR-2012 DDM    Fixed Bug #273: Not rounding value
-- 23-APR-2012 DDM    Fixed Bug #279: The calculation from years old is modified
-- 25-APR-2012 DDM    Fixed Bug #282: Take @DefaultHold from the GENERAL_PARAMS
-- 26-APR-2012 RCI    Fixed Bug #283: Default type can't be "Mesas de Juego" (4). Default type is now "---" (0).
--                                    In case all game types have 0 values, the choosen type will be 0.
--                                    In case there are, p.ex, 1 play in game type 1, 4 plays in type 2, 4 plays in type 3, 0 plays in type 4,
--                                    the choosen type will be 2.
--------------------------------------------------------------------------------

--
-- Parameters. Uncomment if neeeded to test locally.
-- 
-- DECLARE @pIniDate as Datetime
--
-- SET @pIniDate   = '03/01/2012 10:00'
--
-- END of Parameters
--

DECLARE @IniDate  as DateTime
DECLARE @FinDate  as DateTime
DECLARE @DefaultHold as Money

SET @IniDate   = @pIniDate
SET @FinDate   = DATEADD (MONTH, 1, @IniDate)

SET @DefaultHold = (SELECT   1 - CAST (GP_KEY_VALUE AS MONEY) / 100
                      FROM   GENERAL_PARAMS
                     WHERE   GP_GROUP_KEY = 'PlayerTracking'
                       AND   GP_SUBJECT_KEY = 'TerminalDefaultPayout')


--------------------------------------------------------------------------
-- TEMPORARY TABLES
--------------------------------------------------------------------------
--
-- #ACCOUNT_DAY
--

SELECT   PS_ACCOUNT_ID        
       , DATEADD (DAY, DATEDIFF (HOUR, @IniDate, PS_STARTED) / 24, @IniDate) DAY 
       , SUM (PS_PLAYED_AMOUNT * ISNULL(TE_THEORETICAL_HOLD, @DefaultHold)) THEORETICAL_HOLD
       , SUM (CASE WHEN TE_GAME_TYPE = 0 THEN PS_PLAYED_COUNT ELSE 0 END) TD_GAME_TYPE_PLAYED_0
       , SUM (CASE WHEN TE_GAME_TYPE = 1 THEN PS_PLAYED_COUNT ELSE 0 END) TD_GAME_TYPE_PLAYED_1
       , SUM (CASE WHEN TE_GAME_TYPE = 2 THEN PS_PLAYED_COUNT ELSE 0 END) TD_GAME_TYPE_PLAYED_2
       , SUM (CASE WHEN TE_GAME_TYPE = 3 THEN PS_PLAYED_COUNT ELSE 0 END) TD_GAME_TYPE_PLAYED_3
       , SUM (CASE WHEN TE_GAME_TYPE = 4 THEN PS_PLAYED_COUNT ELSE 0 END) TD_GAME_TYPE_PLAYED_4
  INTO   #ACCOUNT_DAY
  FROM   PLAY_SESSIONS 
 INNER   JOIN TERMINALS   ON  PS_TERMINAL_ID = TE_TERMINAL_ID
 WHERE   PS_STARTED >= @IniDate
   AND   PS_STARTED <  @FinDate
 GROUP   BY PS_ACCOUNT_ID,   DATEADD (DAY, DATEDIFF (HOUR, @IniDate, PS_STARTED) / 24, @IniDate)
ORDER BY PS_ACCOUNT_ID, DAY 

--
-- #ACCOUNT_VISITS1
--
SELECT   PS_ACCOUNT_ID
       , SUM(1) AS PS_VISITS       
       , SUM(THEORETICAL_HOLD) AS THEORETICAL_HOLD
       , SUM(THEORETICAL_HOLD)/SUM(1) AS ADT_VALUE
       , SUM(TD_GAME_TYPE_PLAYED_0) AS TD_GAME_TYPE_PLAYED_0
       , SUM(TD_GAME_TYPE_PLAYED_1) AS TD_GAME_TYPE_PLAYED_1
       , SUM(TD_GAME_TYPE_PLAYED_2) AS TD_GAME_TYPE_PLAYED_2
       , SUM(TD_GAME_TYPE_PLAYED_3) AS TD_GAME_TYPE_PLAYED_3
       , SUM(TD_GAME_TYPE_PLAYED_4) AS TD_GAME_TYPE_PLAYED_4
  INTO   #ACCOUNT_VISITS1
  FROM   #ACCOUNT_DAY
GROUP BY PS_ACCOUNT_ID
ORDER BY PS_ACCOUNT_ID 


--
-- #ACCOUNT_VISITS
--
SELECT   PS_ACCOUNT_ID
       , PS_VISITS
       , AC_HOLDER_LEVEL 
       , CASE WHEN (TD_GAME_TYPE_PLAYED_0 = 0 AND TD_GAME_TYPE_PLAYED_1  = 0
                                              AND TD_GAME_TYPE_PLAYED_2  = 0
                                              AND TD_GAME_TYPE_PLAYED_3  = 0
                                              AND TD_GAME_TYPE_PLAYED_4  = 0) THEN 0 ELSE
           CASE WHEN (TD_GAME_TYPE_PLAYED_1 >= TD_GAME_TYPE_PLAYED_0 AND TD_GAME_TYPE_PLAYED_1 >= TD_GAME_TYPE_PLAYED_2 
                                                                     AND TD_GAME_TYPE_PLAYED_1 >= TD_GAME_TYPE_PLAYED_3 
                                                                     AND TD_GAME_TYPE_PLAYED_1 >= TD_GAME_TYPE_PLAYED_4) THEN 1 ELSE
           CASE WHEN (TD_GAME_TYPE_PLAYED_2 >= TD_GAME_TYPE_PLAYED_0 AND TD_GAME_TYPE_PLAYED_2 >= TD_GAME_TYPE_PLAYED_1 
                                                                     AND TD_GAME_TYPE_PLAYED_2 >= TD_GAME_TYPE_PLAYED_3 
                                                                     AND TD_GAME_TYPE_PLAYED_2 >= TD_GAME_TYPE_PLAYED_4) THEN 2 ELSE
           CASE WHEN (TD_GAME_TYPE_PLAYED_3 >= TD_GAME_TYPE_PLAYED_0 AND TD_GAME_TYPE_PLAYED_3 >= TD_GAME_TYPE_PLAYED_1 
                                                                     AND TD_GAME_TYPE_PLAYED_3 >= TD_GAME_TYPE_PLAYED_2 
                                                                     AND TD_GAME_TYPE_PLAYED_3 >= TD_GAME_TYPE_PLAYED_4) THEN 3 ELSE
           CASE WHEN (TD_GAME_TYPE_PLAYED_4 >= TD_GAME_TYPE_PLAYED_0 AND TD_GAME_TYPE_PLAYED_4 >= TD_GAME_TYPE_PLAYED_1 
                                                                     AND TD_GAME_TYPE_PLAYED_4 >= TD_GAME_TYPE_PLAYED_2 
                                                                     AND TD_GAME_TYPE_PLAYED_4 >= TD_GAME_TYPE_PLAYED_3) THEN 4 ELSE
                                                                                                                              0 END END END END END AS GAME_TYPE
       , AC_HOLDER_GENDER
       , CASE WHEN (DATEDIFF( month, AC_HOLDER_BIRTH_DATE,  @IniDate ) IS NULL) THEN 0 ELSE
          CASE WHEN (DATEDIFF( month, AC_HOLDER_BIRTH_DATE,  @IniDate ) < 18*12) THEN 1 ELSE
          CASE WHEN (DATEDIFF( month, AC_HOLDER_BIRTH_DATE,  @IniDate ) < 25*12) THEN 2 ELSE
          CASE WHEN (DATEDIFF( month, AC_HOLDER_BIRTH_DATE,  @IniDate ) < 35*12) THEN 3 ELSE
          CASE WHEN (DATEDIFF( month, AC_HOLDER_BIRTH_DATE,  @IniDate ) < 45*12) THEN 4 ELSE
          CASE WHEN (DATEDIFF( month, AC_HOLDER_BIRTH_DATE,  @IniDate ) < 55*12) THEN 5 ELSE
          CASE WHEN (DATEDIFF( month, AC_HOLDER_BIRTH_DATE,  @IniDate ) < 65*12) THEN 6 ELSE
          CASE WHEN (DATEDIFF( month, AC_HOLDER_BIRTH_DATE,  @IniDate ) < 75*12) THEN 7 ELSE
          CASE WHEN (DATEDIFF( month, AC_HOLDER_BIRTH_DATE,  @IniDate ) < 85*12) THEN 8 ELSE
                                                                                        9 END END END END END END END END END AS YEARS_OLD
       , CASE WHEN (ADT_VALUE <   50) THEN 0 ELSE
          CASE WHEN (ADT_VALUE <  100) THEN 1 ELSE
          CASE WHEN (ADT_VALUE <  150) THEN 2 ELSE
          CASE WHEN (ADT_VALUE <  250) THEN 3 ELSE
          CASE WHEN (ADT_VALUE <  400) THEN 4 ELSE
          CASE WHEN (ADT_VALUE < 1000) THEN 5 ELSE
                                            6 END END END END END END AS ADT_GROUP
       , THEORETICAL_HOLD
       , ADT_VALUE
  INTO   #ACCOUNT_VISITS
  FROM   #ACCOUNT_VISITS1
 INNER   JOIN ACCOUNTS    ON PS_ACCOUNT_ID = AC_ACCOUNT_ID
ORDER BY PS_ACCOUNT_ID

--------------------------------------------------------------------------
-- AFORO, THEORETICAL, VISITS, ADT
--------------------------------------------------------------------------
SELECT * FROM (
  --
  -- HOLDER_LEVEL
  --
  SELECT  
          'HOLDER_LEVEL' AS TD_TABLE
         , AC_HOLDER_LEVEL TD_AGROUP         
         , COUNT(PS_VISITS)  TD_AFORO
         , SUM(THEORETICAL_HOLD) TD_THEORETICAL_HOLD
         , SUM(PS_VISITS) TD_VISITS
         , SUM(THEORETICAL_HOLD) / SUM(PS_VISITS) TD_ADT
  FROM     #ACCOUNT_VISITS
  GROUP BY AC_HOLDER_LEVEL

  UNION ALL

  --
  -- GAME_TYPE
  --
  SELECT  
           'GAME_TYPE' AS TD_TABLE      
         , GAME_TYPE AS TD_AGROUP  
         , COUNT(PS_VISITS)  TD_AFORO
         , SUM(THEORETICAL_HOLD) TD_THEORETICAL_HOLD
         , SUM(PS_VISITS) TD_VISITS
         , SUM(THEORETICAL_HOLD) / SUM(PS_VISITS) TD_ADT
         
    FROM   #ACCOUNT_VISITS 
  GROUP BY GAME_TYPE

  UNION ALL

  --
  --  HOLDER_GENDER
  --
  SELECT
           'HOLDER_GENDER' AS TD_TABLE
         , ISNULL(AC_HOLDER_GENDER,0) TD_AGROUP
         , COUNT(PS_VISITS)  TD_AFORO
         , SUM(THEORETICAL_HOLD) TD_THEORETICAL_HOLD
         , SUM(PS_VISITS) TD_VISITS
         , SUM(THEORETICAL_HOLD) / SUM(PS_VISITS) TD_ADT
    FROM   #ACCOUNT_VISITS
  GROUP BY AC_HOLDER_GENDER

  UNION ALL

  --
  --  YEARS_OLD
  --
  SELECT
           'YEARS_OLD' AS TD_TABLE   
         , YEARS_OLD TD_AGROUP
         , COUNT(PS_VISITS)  TD_AFORO
         , SUM(THEORETICAL_HOLD) TD_THEORETICAL_HOLD
         , SUM(PS_VISITS) TD_VISITS
         , SUM(THEORETICAL_HOLD) / SUM(PS_VISITS) TD_ADT
    FROM   #ACCOUNT_VISITS
  GROUP BY YEARS_OLD

  UNION ALL

  --
  --  ADT_GROUP                                                
  --
  SELECT  
           'ADT_GROUP' AS TD_TABLE
         , ADT_GROUP TD_AGROUP
         , COUNT(PS_VISITS)  TD_AFORO
         , SUM(THEORETICAL_HOLD) TD_THEORETICAL_HOLD
         , SUM(PS_VISITS) TD_VISITS
         , SUM(THEORETICAL_HOLD) / SUM(PS_VISITS) TD_ADT
    FROM   #ACCOUNT_VISITS 
  GROUP BY ADT_GROUP
) X


--
-- DROP TABLES
--
DROP TABLE #ACCOUNT_DAY
DROP TABLE #ACCOUNT_VISITS
DROP TABLE #ACCOUNT_VISITS1