--------------------------------------------------------------------------------
-- Copyright � 2012 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: VoucherBrowserByVoucherID.sql
--   DESCRIPTION: Query for the voucher browser filtering by voucher ID, returns the voucher or the operation if exist
--        AUTHOR: RAFA XANDRI
-- CREATION DATE: 03-JUL-2012
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 03-JUL-2012 RXM    First release.
-- 19-AUG-2013 FBA    Added new operation (10).
--------------------------------------------------------------------------------
--
-- Parameters. Uncomment if neeeded to test locally.
--
--DECLARE @pVoucherId AS bigint 
--SET @pVoucherId  = 1
--
-- END of Parameters
--

DECLARE @pSql_operation AS NVARCHAR(MAX)
SET @pSql_operation = 'SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9 UNION SELECT 10 UNION SELECT 107 UNION SELECT 109 UNION SELECT 110 UNION SELECT 0'

CREATE TABLE #OPERATIONS_TEMP (OPERATION_ID INT)
INSERT INTO #OPERATIONS_TEMP EXEC (@pSql_operation)

CREATE TABLE #ACCOUNT_TEMP (AC_ACCOUNT_ID_T BIGINT, AC_HOLDER_NAME_T NVARCHAR(200), AC_TRACK_DATA_T NVARCHAR(50), AC_TYPE INT)
INSERT INTO #ACCOUNT_TEMP EXEC ('SELECT AC_ACCOUNT_ID, AC_HOLDER_NAME, AC_TRACK_DATA, AC_TYPE FROM ACCOUNTS')

CREATE TABLE #USER_TEMP (CV_USER_ID_T BIGINT)
INSERT INTO #USER_TEMP EXEC ('SELECT GU_USER_ID FROM GUI_USERS ')


 DECLARE @CV_OPERATION_ID AS  NVARCHAR(MAX)
 SET @CV_OPERATION_ID = (SELECT ISNULL(CV_OPERATION_ID,0) FROM   CASHIER_VOUCHERS where CV_VOUCHER_ID = @pVoucherId)


IF (@CV_OPERATION_ID = 0 AND @CV_OPERATION_ID <> '')
BEGIN
SELECT   SOURCE
       , ID
       , DATE
       , CODE
       , AMOUNT
       , CS_ID
       , CS_NAME
       , ACCOUNT_ID
       , AC_HOLDER_NAME_T
       , AC_TRACK_DATA_T
	     , AC_TYPE
  FROM ( 
  SELECT          2                       SOURCE               
                , CV_VOUCHER_ID			  ID
                , CV_DATETIME			  DATE
                , 0	                      CODE
                , CV_AMOUNT				  AMOUNT
                , CV_SESSION_ID			  CS_ID
                , CV_ACCOUNT_ID			  ACCOUNT_ID
           FROM   CASHIER_VOUCHERS
          WHERE CV_VOUCHER_ID = @pVoucherId) X
 LEFT    JOIN #ACCOUNT_TEMP    ON AC_ACCOUNT_ID_T = ACCOUNT_ID 
 INNER   JOIN CASHIER_SESSIONS ON CS_SESSION_ID   = CS_ID
 INNER   JOIN #USER_TEMP       ON CV_USER_ID_T    = CS_USER_ID
 WHERE   CODE IN (SELECT OPERATION_ID FROM #OPERATIONS_TEMP)
   AND   ((ACCOUNT_ID IS NULL) OR (ACCOUNT_ID IN (SELECT AC_ACCOUNT_ID_T FROM #ACCOUNT_TEMP)))
   AND   CS_USER_ID IN (SELECT CV_USER_ID_T FROM #USER_TEMP)
ORDER BY DATE DESC



END
ELSE
BEGIN
SELECT   SOURCE
       , ID
       , DATE
       , CODE
       , AMOUNT
       , CS_ID
       , CS_NAME
       , ACCOUNT_ID
       , AC_HOLDER_NAME_T
       , AC_TRACK_DATA_T
	     , AC_TYPE
  FROM ( SELECT            1                      SOURCE
                , AO_OPERATION_ID        ID
                , AO_DATETIME            DATE
                , AO_CODE                CODE
                , AO_AMOUNT              AMOUNT
                , AO_CASHIER_SESSION_ID  CS_ID
                , AO_ACCOUNT_ID          ACCOUNT_ID
           FROM   ACCOUNT_OPERATIONS
WHERE AO_OPERATION_ID = @CV_OPERATION_ID) X
 LEFT    JOIN #ACCOUNT_TEMP    ON AC_ACCOUNT_ID_T = ACCOUNT_ID 
 INNER   JOIN CASHIER_SESSIONS ON CS_SESSION_ID   = CS_ID
 INNER   JOIN #USER_TEMP       ON CV_USER_ID_T    = CS_USER_ID
 WHERE   CODE IN (SELECT OPERATION_ID FROM #OPERATIONS_TEMP)
   AND   ((ACCOUNT_ID IS NULL) OR (ACCOUNT_ID IN (SELECT AC_ACCOUNT_ID_T FROM #ACCOUNT_TEMP)))
   AND   CS_USER_ID IN (SELECT CV_USER_ID_T FROM #USER_TEMP)
ORDER BY DATE DESC

END



DROP TABLE #OPERATIONS_TEMP
DROP TABLE #ACCOUNT_TEMP
DROP TABLE #USER_TEMP
