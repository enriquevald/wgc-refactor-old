---------------------------------------------------------------------
-- Copyright � 2012 Win Systems Ltd.
---------------------------------------------------------------------
--
-- MODULE NAME:   DetailedPrizes.sql
-- DESCRIPTION:   The account related with the terminal. It shows where it won and when it paid.
-- AUTHOR:        Marcos Piedra Osuna
-- CREATION DATE: 04-APR-2012
--
-- REVISION HISTORY:
--
-- Date         Author Description
-- -----------  ------ -----------------------------------------------
-- 04-APR-2012  MPO    Initial version
-- 17-APR-2012  JCM    Convert "Multigame" literal string in parameter
-- 25-JUL-2012  MPO    FixedBug #322. It doesn't order by account id. 
-- 03-SEP-2014  LEM    Add TE_TERMINAL_ID column.
-- -------------------------------------------------------------------

--
-- Parameters. Uncomment if neeeded to test locally.
-- 
--DECLARE @pInitDate    AS DATETIME
--DECLARE @pFinDate    AS DATETIME
--DECLARE @pMultiGame  AS VARCHAR(10) 

--SET @pInitDate  = '2012/03/01 10:00:00'
--SET @pFinDate   = '2012/04/01 10:00:00'
--SET @pMultiGame = 'Multijuego'
--
-- END of Parameters
--

DECLARE @IniDate    AS DATETIME
DECLARE @FinDate    AS DATETIME
DECLARE @Filter     AS INTEGER
DECLARE @sqlGamingTerminalTypeList AS NVARCHAR(MAX)
DECLARE @LanguageId AS INT

SET @IniDate    = @pInitDate
SET @FinDate    = @pFinDate
SET @LanguageId = @pLanguageId
--
-- Gaming Terminals Type
--
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'#TEMP_GAMING_TERMINALS_TYPES') AND type in (N'U'))
BEGIN                                
  DROP TABLE #TEMP_GAMING_TERMINALS_TYPES  
END                                  
CREATE  TABLE #TEMP_GAMING_TERMINALS_TYPES ( TT_TYPE   INT )     
SET @sqlGamingTerminalTypeList = 'INSERT INTO #TEMP_GAMING_TERMINALS_TYPES SELECT TT_TYPE FROM TERMINAL_TYPES WHERE TT_TYPE IN (' + @pGamingTerminalTypeList + ') '
EXEC (@sqlGamingTerminalTypeList)



SELECT   1 LVL_ID
       , GP_KEY_VALUE AS LVL_NAME 
  INTO   #LEVELS 
  FROM   GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PlayerTracking' AND GP_SUBJECT_KEY = 'Level01.Name'
 UNION ALL
SELECT   2 LVL_ID
       , GP_KEY_VALUE AS LVL_NAME 
  FROM   GENERAL_PARAMS 
 WHERE   GP_GROUP_KEY = 'PlayerTracking' AND GP_SUBJECT_KEY = 'Level02.Name'
 UNION ALL
SELECT   3 LVL_ID
       , GP_KEY_VALUE AS LVL_NAME 
  FROM   GENERAL_PARAMS 
 WHERE   GP_GROUP_KEY = 'PlayerTracking' AND GP_SUBJECT_KEY = 'Level03.Name'
 UNION ALL
SELECT   4 LVL_ID
       , GP_KEY_VALUE AS LVL_NAME 
  FROM   GENERAL_PARAMS 
 WHERE   GP_GROUP_KEY = 'PlayerTracking' AND GP_SUBJECT_KEY = 'Level04.Name'
UNION ALL
SELECT   0 LVL_ID
       , NULL AS LVL_NAME 
ORDER BY 1 DESC


--
-- TERMINAL-GAME
--
--SELECT   TGT_TERMINAL_ID AS TG_TERMINAL_ID
--       , ISNULL (GM_NAME, @pMultiGame) AS TG_GAME
--  INTO   #TERMINAL_GAME
--  FROM ( SELECT   TGT_TERMINAL_ID
--                , CASE WHEN MIN(TGT_TARGET_GAME_ID) = MAX(TGT_TARGET_GAME_ID) THEN MIN(TGT_TARGET_GAME_ID) ELSE NULL END GAME_ID
--           FROM   TERMINAL_GAME_TRANSLATION
--          GROUP   BY TGT_TERMINAL_ID 
--       ) AS XXX LEFT JOIN GAMES ON GM_GAME_ID = XXX.GAME_ID 
       
SELECT   TGT_TERMINAL_ID                AS TG_TERMINAL_ID
       , ISNULL (PG_GAME_NAME, @pMultigame)  AS TG_GAME
  INTO   #TERMINAL_GAME
  FROM ( SELECT   TGT_TERMINAL_ID
                , CASE WHEN MIN(TGT_TRANSLATED_GAME_ID) = MAX(TGT_TRANSLATED_GAME_ID) THEN MIN(TGT_TRANSLATED_GAME_ID) ELSE NULL END GAME_ID
           FROM   TERMINAL_GAME_TRANSLATION
         GROUP BY TGT_TERMINAL_ID 
       ) AS XXX
  LEFT   JOIN PROVIDERS_GAMES ON PG_GAME_ID = XXX.GAME_ID 

--
-- TERMINALS
--
SELECT   TE_TERMINAL_ID
       , TE_NAME
       , TG_GAME
       , PV_NAME	
       , BK_NAME
       , AR_SMOKING
       , BK_NAME AS B
       , PV_NAME AS A
       , 0 TERM_X_DAY
  INTO   #TERMINALS
  FROM   TERMINALS
 INNER   JOIN PROVIDERS  		  ON TE_PROV_ID     = PV_ID
 INNER   JOIN GAME_TYPES 		  ON TE_GAME_TYPE   = GT_GAME_TYPE AND GT_LANGUAGE_ID = @LanguageId
 INNER   JOIN BANKS      		  ON TE_BANK_ID     = BK_BANK_ID
 INNER   JOIN AREAS      		  ON BK_AREA_ID     = AR_AREA_ID
  LEFT   JOIN #TERMINAL_GAME  ON TE_TERMINAL_ID = TG_TERMINAL_ID
 WHERE   TE_TYPE = 1 
   AND   TE_TERMINAL_TYPE IN (SELECT   TT_TYPE 
                                FROM   #TEMP_GAMING_TERMINALS_TYPES )

--
-- #ACCOUNT_TERMINAL_PRIZES
--
SELECT   PS_ACCOUNT_ID                             			AS ATP_ACCOUNT_ID
       , 0                                                  AS ATP_TYPE
       , PS_TERMINAL_ID                            			AS ATP_TERMINAL_ID 
       , SUM(1)												AS ATP_NUM_WON
       , SUM(PS_TOTAL_CASH_OUT - PS_TOTAL_CASH_IN)			AS ATP_SUM_WON
       , NULL                                               AS ATP_NUM_PAID
       , NULL                                               AS ATP_SUM_PAID
  INTO   #ACCOUNT_TERMINAL_PRIZES
  FROM PLAY_SESSIONS WITH ( INDEX ( IX_PS_FINISHED_STATUS ) )
  LEFT JOIN HANDPAYS ON HP_PLAY_SESSION_ID = PS_PLAY_SESSION_ID AND HP_TYPE IN (1, 2) -- JACKPOT, ORPHAN_CREDITS
    AND HP_AMOUNT = PS_TOTAL_CASH_OUT - PS_TOTAL_CASH_IN
  WHERE PS_FINISHED >= @IniDate 
    AND PS_FINISHED <  @FinDate
    AND PS_STATUS <> 0
    AND PS_TOTAL_CASH_OUT > PS_TOTAL_CASH_IN
    AND (
         (PS_FINISHED > PS_STARTED AND HP_PLAY_SESSION_ID IS NULL)
     OR  (PS_FINISHED = PS_STARTED AND HP_PLAY_SESSION_ID IS NOT NULL)
        )
 GROUP BY PS_ACCOUNT_ID, PS_TERMINAL_ID

--
-- #ACCOUNT_COLLECTED_PRIZES
--
SELECT   AM_ACCOUNT_ID                  AS ACP_ACCOUNT_ID
       , 1								AS ACP_TYPE
       , NULL                         	AS ACP_TERMINAL_ID 
       , NULL                           AS ACP_NUM_WON
       , NULL                           AS ACP_SUM_WON
       , SUM(1)							AS ACP_NUM_PAID
       , SUM(AM_SUB_AMOUNT)				AS ACP_SUM_PAID
  INTO   #ACCOUNT_COLLECTED_PRIZES
  FROM   ACCOUNT_MOVEMENTS WITH (INDEX (IX_type_date_account)) 
  WHERE  AM_TYPE      = 2
    AND  (AM_DATETIME >= @IniDate AND AM_DATETIME < @FinDate)
  GROUP  BY AM_ACCOUNT_ID

--
-- RESULT
--
SELECT   AC_ACCOUNT_ID
       , AC_HOLDER_NAME
       , LVL_NAME
       , PV_NAME
       , TE_NAME
       , TG_GAME
       , ATP_NUM_WON
       , ATP_SUM_WON
       , ATP_NUM_PAID
       , ATP_SUM_PAID              
       , ISNULL((SELECT COUNT(*) FROM #ACCOUNT_COLLECTED_PRIZES WHERE ACP_ACCOUNT_ID = AC_ACCOUNT_ID),0) AS CONTAIN_PAID
       , ISNULL((SELECT COUNT(*) FROM #ACCOUNT_TERMINAL_PRIZES WHERE ATP_ACCOUNT_ID = AC_ACCOUNT_ID),0) AS CONTAIN_WON
       , ATP_TERMINAL_ID
  FROM ( SELECT   * 
           FROM   #ACCOUNT_TERMINAL_PRIZES
          UNION ALL
         SELECT   * 
           FROM   #ACCOUNT_COLLECTED_PRIZES ) ACCOUNT_DETAILS
 INNER   JOIN ACCOUNTS   ON AC_ACCOUNT_ID   = ATP_ACCOUNT_ID
 INNER   JOIN #LEVELS    ON LVL_ID          = AC_HOLDER_LEVEL  
  LEFT   JOIN #TERMINALS ON TE_TERMINAL_ID  = ATP_TERMINAL_ID
ORDER BY AC_HOLDER_LEVEL DESC, AC_HOLDER_NAME ASC, AC_ACCOUNT_ID ASC, ATP_TYPE ASC, PV_NAME ASC, TE_NAME ASC  
 
DROP TABLE #LEVELS
DROP TABLE #TERMINAL_GAME
DROP TABLE #TERMINALS
DROP TABLE #ACCOUNT_TERMINAL_PRIZES
DROP TABLE #ACCOUNT_COLLECTED_PRIZES

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'#TEMP_GAMING_TERMINALS_TYPES') AND type in (N'U'))
BEGIN                                
  DROP TABLE #TEMP_GAMING_TERMINALS_TYPES  
END                                  
