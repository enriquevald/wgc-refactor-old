--DECLARE @pAccountId AS INT
--DECLARE @pAccountCard AS VARCHAR(20)
--DECLARE @pMultigame AS VARCHAR(15)
--DECLARE @pFechaIni AS DATETIME
--DECLARE @pFechaFin AS DATETIME	
--DECLARE @pRefOpening AS DATETIME
--DECLARE @pSQLaccount AS NVARCHAR(MAX)	

--SET @pAccountId = 7
--SET @pAccountCard = ''
--SET @pMultigame = 'Multijuego'
--SET @pFechaIni = CAST('2012-01-07 10:00:00' AS DATETIME)
--SET @pFechaFin = CAST('2013-01-07 10:00:00' AS DATETIME)
--SET @pRefOpening = CAST('2013-01-07 10:00:00' AS DATETIME)
--------------------------------------------------------------------------------
CREATE TABLE #SELECTED_ACCOUNT 
(
	        SA_ACCOUNT_ID INT,
	        SA_ACCOUNT_CARD NVARCHAR(50),
	        SA_ACCOUNT_NAME NVARCHAR(200),
			    SA_ACCOUNT_LEVEL NVARCHAR(50),   
			    SA_ACCOUNT_POINTS MONEY,                                  
			    SA_ACCOUNT_LAST_ACTIVITY DATETIME,   
			    SA_ACCOUNT_BIRTH_DATE DATETIME,  
			    SA_ACCOUNT_GENDER INT,
          SA_USER_TYPE INT,               
          SA_HOLDER_LEVEL_ENTERED DATETIME,
          SA_ACCOUNT_TYPE INT    
			    
)
INSERT INTO #SELECTED_ACCOUNT EXEC (@pSQLaccount)
--------------------------------------------------------------------------------
SELECT      *
FROM        #SELECTED_ACCOUNT 
--------------------------------------------------------------------------------
SELECT	    PS_PLAY_SESSION_ID SS_PLAY_SESSION_ID,
		        SA_ACCOUNT_ID SS_ACCOUNT_ID,
	          SA_ACCOUNT_CARD SS_ACCOUNT_CARD,
	          SA_ACCOUNT_NAME SS_ACCOUNT_NAME
INTO        #SELECTED_SESSIONS
FROM        PLAY_SESSIONS
INNER JOIN  #SELECTED_ACCOUNT ON PS_ACCOUNT_ID = SA_ACCOUNT_ID	
WHERE       PS_STARTED >= @pFechaIni
  AND       PS_STARTED <= @pFechaFin  
--------------------------------------------------------------------------------
SELECT      TGT_TERMINAL_ID TG_TERMINAL_ID,
            TE_NAME TG_NAME,
            TE_DENOMINATION TG_DENOMINATION,
            TE_MULTI_DENOMINATION TG_MULTI_DENOMINATION,
            PV_NAME TG_PROVIDER,
            --ISNULL(GM_NAME, @pMultigame) TG_GAME
            ISNULL(PG_GAME_NAME, @pMultigame) TG_GAME
  INTO      #TERMINAL_GAME
  FROM  ( 
            SELECT      TGT_TERMINAL_ID, TE_NAME, TE_DENOMINATION, TE_MULTI_DENOMINATION, PV_NAME,
                        --CASE WHEN MIN(TGT_TARGET_GAME_ID) = MAX(TGT_TARGET_GAME_ID) THEN MIN(TGT_TARGET_GAME_ID) ELSE NULL END GAME_ID
                        CASE WHEN MIN(TGT_TRANSLATED_GAME_ID) = MAX(TGT_TRANSLATED_GAME_ID) THEN MIN(TGT_TRANSLATED_GAME_ID) ELSE NULL END GAME_ID
            FROM        TERMINAL_GAME_TRANSLATION
            INNER JOIN  TERMINALS ON TE_TERMINAL_ID = TGT_TERMINAL_ID
            INNER JOIN  PROVIDERS ON PV_ID = TE_PROV_ID
            GROUP BY    TGT_TERMINAL_ID,TE_NAME,TE_DENOMINATION,TE_MULTI_DENOMINATION,PV_NAME 
        ) 
        AS TEMP
  --LEFT JOIN GAMES ON GM_GAME_ID = TEMP.GAME_ID 
    LEFT JOIN PROVIDERS_GAMES ON PG_GAME_ID = TEMP.GAME_ID 
-------------------------------------------------------------------------------- 
--SESSIONS UNGROUP
SELECT	    DATEADD(dd, DATEDIFF(hh, @pRefOpening , PS_STARTED)/24, @pRefOpening) SE_DATE,
		        TG_PROVIDER SE_PROVIDER,
		        TG_NAME SE_NAME,                                             
		        PS_TOTAL_CASH_IN SE_CASH_IN,                        
		        PS_PLAYED_AMOUNT SE_PLAYED,                   
		        PS_WON_AMOUNT SE_WON,                         
		        PS_TOTAL_CASH_OUT SE_CASH_OUT,                      
		        DATEDIFF(mi,PS_STARTED,PS_FINISHED) SE_TIME,  
		        TG_DENOMINATION SE_DENOMINATION,
		        TG_MULTI_DENOMINATION SE_MULTI_DENOMINATION,
		        TG_GAME SE_GAME	   
INTO        #SESSIONS_UNGROUP
FROM        PLAY_SESSIONS PS
INNER JOIN  #SELECTED_SESSIONS SS ON SS_PLAY_SESSION_ID = PS_PLAY_SESSION_ID
INNER JOIN  #TERMINAL_GAME ON TG_TERMINAL_ID = PS_TERMINAL_ID   
----------------------------------------------------------------------------------
--TERMINAL GROUP
SELECT
	          SUM(SE_CASH_IN) SE_CASH_IN,                        
	          SUM(SE_PLAYED) SE_PLAYED,                   
	          SUM(SE_WON) SE_WON,                         
	          SUM(SE_CASH_OUT) SE_CASH_OUT,                      
	          SUM(SE_TIME) SE_TIME,  
	          COUNT(*) SE_COUNT,
	          SE_PROVIDER SE_PROVIDER, 
	          SE_NAME SE_NAME,
	          SE_GAME SE_GAME,
	          SE_DENOMINATION SE_DENOMINATION,
	          SE_MULTI_DENOMINATION SE_MULTI_DENOMINATION,
	          0 SE_DATE   
FROM        #SESSIONS_UNGROUP
GROUP BY    SE_NAME, SE_PROVIDER, SE_DENOMINATION, SE_MULTI_DENOMINATION, SE_GAME
ORDER BY    SE_PLAYED DESC, SE_TIME DESC
----------------------------------------------------------------------------------
--DETAIL
SELECT 	                                             
	          SUM(SE_CASH_IN) SE_CASH_IN,                        
	          SUM(SE_PLAYED) SE_PLAYED,                   
	          SUM(SE_WON) SE_WON,                         
	          SUM(SE_CASH_OUT) SE_CASH_OUT,                      
	          SUM(SE_TIME) SE_TIME,  
	          COUNT(*) SE_COUNT,
	          SE_PROVIDER SE_PROVIDER, 
	          SE_NAME SE_NAME,
	          SE_GAME SE_GAME,
	          SE_DENOMINATION SE_DENOMINATION,
	          SE_MULTI_DENOMINATION SE_MULTI_DENOMINATION,
	          SE_DATE SE_DATE
FROM        #SESSIONS_UNGROUP
GROUP BY    SE_DATE, SE_PROVIDER, SE_NAME,	SE_DENOMINATION, SE_MULTI_DENOMINATION, SE_GAME
ORDER BY    SE_DATE DESC,SE_PLAYED DESC, SE_TIME DESC
----------------------------------------------------------------------------------
DROP TABLE #SELECTED_ACCOUNT
DROP TABLE #SELECTED_SESSIONS  
DROP TABLE #TERMINAL_GAME
DROP TABLE #SESSIONS_UNGROUP