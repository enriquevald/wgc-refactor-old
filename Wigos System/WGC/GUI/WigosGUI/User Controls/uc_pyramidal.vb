﻿Public Class uc_pyramidal
  Inherits System.Windows.Forms.UserControl

#Region " Enums "

#End Region ' Enums

#Region " Members "

  Private m_frm_pyramization As frm_pyramization
  Private m_xml As String = String.Empty

#End Region ' Members "

#Region " Constants "

  Public XmlDefaultValue As String = "<piramization><level><probability>95</probability><fix>0</fix><recharge>0</recharge></level><level><probability>2</probability><fix>0</fix><recharge>0</recharge></level><level><probability>2</probability><fix>0</fix><recharge>0</recharge></level><level><probability>1</probability><fix>0</fix><recharge>0</recharge></level></piramization>"

#End Region  ' Constants "

#Region " Properties "

  Public Property Xml() As String
    Get
      If Me.IsEnabled() AndAlso Me.m_xml Is Nothing Then
        m_xml = XmlDefaultValue
      ElseIf Me.IsEnabled() AndAlso Me.m_xml.Equals(String.Empty) Then
        m_xml = XmlDefaultValue
      End If

      Return m_xml
    End Get
    Set(ByVal value As String)
      m_xml = value
    End Set
  End Property

  Public ReadOnly Property IsEnabled() As Boolean
    Get
      Return Me.chk_apply_pyramization.Checked
    End Get
  End Property

#End Region ' Properties

#Region " Public "

  Public Sub Init()
    Me.chk_apply_pyramization.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8753)
    Me.btn_definition_values.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8754)
  End Sub ' Init

  ''' <summary>
  ''' To Set uc screen data
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub SetScreemData()
    Dim _is_enabled As Boolean

    _is_enabled = False

    If Not IsNothing(Me.m_xml) Then
      _is_enabled = Not Me.m_xml.Equals(String.Empty)
    End If

    Me.chk_apply_pyramization.Checked = _is_enabled
    Me.btn_definition_values.Enabled = _is_enabled
  End Sub

#End Region ' Public

#Region " Private "

#End Region ' Private

#Region " Events "

  Private Sub btn_definition_values_Click(sender As Object, e As EventArgs) Handles btn_definition_values.Click
    Me.m_frm_pyramization = New frm_pyramization(Me)
    Me.m_frm_pyramization.Xml = Me.Xml()
    Call Me.m_frm_pyramization.SetFormInfo()

    Me.m_frm_pyramization.ShowDialog(Me)
  End Sub

  Private Sub chk_apply_pyramization_CheckedChanged(sender As Object, e As EventArgs) Handles chk_apply_pyramization.CheckedChanged
    Me.btn_definition_values.Enabled = Me.chk_apply_pyramization.Checked
  End Sub

#End Region ' Events

End Class
