'-------------------------------------------------------------------
' Copyright � 2007-2013 Win Systems Ltd.
'-------------------------------------------------------------------
' 
' MODULE NAME:   uc_player_edit
' DESCRIPTION:   User control for player data edition
' AUTHOR:        Luis Ernesto Mesa
' CREATION DATE: 28-FEB-2013
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 28-FEB-2013  LEM    Initial version.
' 28-MAR-2013  LEM    Added new field Twitter Account.
' 17-MAY-2013  JMM    Added Block tab.
' 13-JUN-2013  ACM    Added three new documents types, new controls(occupation, birthPlace, nationality, externalNumeber, etc), Document Image and Beneficiary Tab.
' 14-AGO-2013  ANG    Relocate DocId documents.
' 04-SEP-2013  DRV    Fixed Bug 185 - RelocateDocId documents don't works properly in some cases.
' 16-OCT-2013  JAB    Added new tab to show information about points program.
' 24-OCT-2013  JAB    Fixed Bug WIG-308: Format dates with "GUI_FormatDate()" function ("lbl_level_expiration" and "lbl_level_entered").
' 25-OCT-2013  JAB    Fixed Bug #WIG-319: Error in points program tab, error in format values.
' 25-OCT-2013  JAB    Fixed Bug #WIG-321: Error in points program tab, error in format values.
' 25-OCT-2013  JAB    Fixed Bug #WIG-323: Error in points program tab, error in format values.
' 12-FEB-2014  FBA    Added account holder id type control via CardData class IdentificationTypes
' 13-MAR-2014  DLL    Fixed Bug #WIG-730: Error validating zip code.
' 17-MAR-2014  JRM    Added new functionality. Description contained in RequestPinCodeDUT.docx DUT
' 07-APR-2014  JBP    Added only VIP property.
' 02-JUN-2014  RRR    Fixed Bug WIG-959: PIN required in anonymous accounts when general param disables it
' 24-JUL-2014  SGB    Change text value of emails and phone numbers in contact part if use only one of type contact
' 09-JAN-2015  RCI    Added functionality for record card track data in twitter field (Logrand)
' 09-FEB-2015  DRV    Fixed Bug WIG-2028: Personalization is not allowed
' 12-FEB-2015  YNM    Fixed Bug WIG-1087: When unblocking card, the unlocking reason was never show
' 13-AUG-2015  AMF    Backlog Item 3296: Add address country in account
' 25-AUG-2015  SGB    Backlog Item 3297: Check ZIP and autoselection in combobox
' 27-AUG-2015  ETP    Bug 4055 Fixed - Selecting default values for Document type combo
' 15-SEP-2015  SGB    Bug 4438 Fixed - Don't save city value
' 15-SEP-2015  SGB    Bug 4502 Fixed - Don't show combos
' 16-SEP-2015  SGB    Bug 4512 Fixed - Error uncheck freemode
' 28-SEP-2015  DLL    Bug 4592 Fixed - Error with postal code
' 06-OCT-2015  SGB    Fixed Bug TFS-4914: Error selected empty federal entity
' 06-OCT-2015  SGB    Fixed Bug TFS-5076: Error if selected country
' 08-OCT-2015  SGB    Fixed Bug TFS-5101: Start event if change property object in moment no controler
' 13-JAN-2016  JML    Product Backlog Item 2541: MultiSite Multicurrency PHASE3: Loyalty program in the sites.
' 22-FEB-2016  FAV    Fixed Bug 9713: unhandled exception error on Accounts
' 30-MAR-2016  SMN    Product Backlog Item 11210:Multiple Buckets: Modificar "programa de puntos" del GUI y SP
' 30-MAR-2016  SMN    Fixed Bug 14685: Mallorca: NOMBRE - APELLIDO cambio de orden
' 04-AGO-2016  FJC    Bug 16410:Gui: Excepci�n no controlada al personalizar cuenta
' 03-OCT-2017  AMF    WIGOS-5478 Accounts - Change literals
' 13-FEB-2018  EOR    Bug 31515: WIGOS-8032 [Ticket #12229] Formulario de inscripci�n de jugadores en registro por l�mite de pago 
' 04-JUL-2018  DLM    Bug 33475: WIGOS-13382 Losing changes warning message appear without any change in the screen
'--------------------------------------------------------------------

Imports GUI_Controls
Imports GUI_CommonMisc
Imports WSI.Common
Imports GUI_CommonOperations
Imports System.Text
Imports System.Drawing

Public Class uc_player_edit

#Region " CONSTANTS "

  Private Const BUTTON_DOCUMENT_HEIGHT As Integer = 24
  Private Const BUTTON_DOCUMENT_WIDTH As Integer = 180
  Private Const MAX_RFC_LEN As Integer = 20
  Private Const MAX_CURP_LEN As Integer = 20
  Private Const COLUMN_NAME As Integer = 0
  Private Const COLUMN_ID As Integer = 1

#End Region

#Region " Members "

  Dim m_player_data As CLASS_PLAYER_DATA
  Dim m_read_birthdate As DateTime
  Dim m_read_ben_birthdate As DateTime
  Dim m_pin_required As Boolean
  Dim m_empty_pin As Boolean
  Dim m_beneficiary_data As Collection
  Dim m_tab_points_first_load As Boolean
  Dim m_address_original As Zip.Address
  Dim m_address_modified As Zip.Address

  Dim WithEvents m_parent As Form

#End Region ' Members

#Region " Publics "

  Public Sub New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.

  End Sub ' New

  ' PURPOSE: Initialize controls
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS: 
  Public Sub Init()

    Call SetTextControls()
    Call SetFormatControls()
    Call LoadDataCombos()
    Call ChangeNameOrder()
    Call VisibleData()

  End Sub ' InitControls

  ' PURPOSE: Set initial data on the user control.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS: 
  Public Sub SetData(ByVal PlayerData As CLASS_PLAYER_DATA)

    Dim _is_vip_index As Integer
    Dim _coma As String

    _coma = String.Empty
    m_player_data = PlayerData

    'Main
    ef_name.TextValue = m_player_data.CardData.PlayerTracking.HolderName3
    ef_middle_name.TextValue = m_player_data.CardData.PlayerTracking.HolderName4
    ef_surname_1.TextValue = m_player_data.CardData.PlayerTracking.HolderName1
    ef_surname_2.TextValue = m_player_data.CardData.PlayerTracking.HolderName2

    If m_player_data.CardData.PlayerTracking.HolderIdType = ACCOUNT_HOLDER_ID_TYPE.RFC Then
      ef_doc_id.TextValue = String.Empty
      ef_doc_id1.TextValue = m_player_data.CardData.PlayerTracking.HolderId
      ef_doc_id2.TextValue = m_player_data.CardData.PlayerTracking.HolderId2
    ElseIf m_player_data.CardData.PlayerTracking.HolderIdType = ACCOUNT_HOLDER_ID_TYPE.CURP Then
      ef_doc_id.TextValue = String.Empty
      ef_doc_id2.TextValue = m_player_data.CardData.PlayerTracking.HolderId
      ef_doc_id1.TextValue = m_player_data.CardData.PlayerTracking.HolderId1
    Else
      ef_doc_id.TextValue = m_player_data.CardData.PlayerTracking.HolderId
      ef_doc_id1.TextValue = m_player_data.CardData.PlayerTracking.HolderId1
      ef_doc_id2.TextValue = m_player_data.CardData.PlayerTracking.HolderId2
    End If

    If (m_player_data.CardData.PlayerTracking.HolderIdType = ACCOUNT_HOLDER_ID_TYPE.RFC) Or _
       (m_player_data.CardData.PlayerTracking.HolderIdType = ACCOUNT_HOLDER_ID_TYPE.CURP) Then

      Call SelectItemCombo(ACCOUNT_HOLDER_ID_TYPE.UNKNOWN, cb_doc_type)
    Else
      'ETP BUG-4055 Selecting default values for Document type combo
      If (cb_doc_type.Count > 0) Then
        cb_doc_type.SelectedIndex = 0
      End If

      If (m_player_data.CardData.PlayerTracking.CardLevel = 0) Then
        Call SelectItemCombo(GeneralParam.GetInt32("Account.DefaultValues", "DocumentType"), cb_doc_type)

      Else
        Call SelectItemCombo(m_player_data.CardData.PlayerTracking.HolderIdType, cb_doc_type)
      End If

    End If

    Call SelectItemCombo(m_player_data.CardData.PlayerTracking.HolderGender, cb_gender)
    Call SelectItemCombo(m_player_data.CardData.PlayerTracking.HolderMaritalStatus, cb_marital_status)
    Call SelectItemCombo(m_player_data.CardData.PlayerTracking.HolderNationality, cb_nationality)
    Call SelectItemCombo(m_player_data.CardData.PlayerTracking.HolderBirthCountry, cb_birthplace)

    Occupations.Update(cb_occupation.DataSource, m_player_data.CardData.PlayerTracking.HolderOccupationId, 1)
    Call SelectItemCombo(m_player_data.CardData.PlayerTracking.HolderOccupationId, cb_occupation)

    If m_player_data.CardData.PlayerTracking.HolderBirthDate <> DateTime.MinValue Then
      dtp_birthdate.Value = New Date(m_player_data.CardData.PlayerTracking.HolderBirthDate.Year, _
                                     m_player_data.CardData.PlayerTracking.HolderBirthDate.Month, _
                                     m_player_data.CardData.PlayerTracking.HolderBirthDate.Day)
    End If
    m_read_birthdate = dtp_birthdate.Value

    If m_player_data.CardData.PlayerTracking.HolderWeddingDate <> DateTime.MinValue Then
      dtp_wedding.Value = New Date(m_player_data.CardData.PlayerTracking.HolderWeddingDate.Year, _
                                   m_player_data.CardData.PlayerTracking.HolderWeddingDate.Month, _
                                   m_player_data.CardData.PlayerTracking.HolderWeddingDate.Day)
      dtp_wedding.Checked = True
    Else
      dtp_wedding.Checked = False
    End If

    'Holder Scanned Documents button
    If String.IsNullOrEmpty(m_player_data.CardData.PlayerTracking.HolderId3Type) Then
      btn_document.Enabled = False
    Else
      btn_document.Enabled = True
    End If

    'Beneficiary Scanned Documents button
    If String.IsNullOrEmpty(m_player_data.CardData.PlayerTracking.BeneficiaryId3Type) Then
      btn_ben_document.Enabled = False
    Else
      btn_ben_document.Enabled = True
    End If

    'Contact
    ef_phone_1.TextValue = m_player_data.CardData.PlayerTracking.HolderPhone01
    ef_phone_2.TextValue = m_player_data.CardData.PlayerTracking.HolderPhone02
    ef_email_1.TextValue = m_player_data.CardData.PlayerTracking.HolderEmail01
    ef_email_2.TextValue = m_player_data.CardData.PlayerTracking.HolderEmail02
    ef_twitter.TextValue = m_player_data.CardData.PlayerTracking.HolderTwitter

    'Address
    ef_street.TextValue = m_player_data.CardData.PlayerTracking.HolderAddress01
    ef_colony.TextValue = m_player_data.CardData.PlayerTracking.HolderAddress02
    ef_delegation.TextValue = m_player_data.CardData.PlayerTracking.HolderAddress03
    ef_city.TextValue = m_player_data.CardData.PlayerTracking.HolderCity
    ef_zip.TextValue = m_player_data.CardData.PlayerTracking.HolderZip
    ef_external_number.TextValue = m_player_data.CardData.PlayerTracking.HolderExtNumber
    Call SelectItemCombo(m_player_data.CardData.PlayerTracking.HolderAddressCountry, cb_address_country)
    Call SelectItemCombo(m_player_data.CardData.PlayerTracking.HolderFedEntity, cb_federal_entity)

    RemoveHandler cb_as_address_country.ValueChangedEvent, AddressOf cb_as_address_country_ValueChangedEvent
    Call SelectItemCombo(m_player_data.CardData.PlayerTracking.HolderAddressCountry, cb_as_address_country)
    AddHandler cb_as_address_country.ValueChangedEvent, AddressOf cb_as_address_country_ValueChangedEvent

    lbl_info.Text = CardData.GetHolderAddressValidationString(m_player_data.CardData.PlayerTracking.HolderAddressValidation)
    FillStructAddress(m_address_original)
    FillStructAddress(m_address_modified)

    'Comments
    txt_comments.Text = m_player_data.CardData.PlayerTracking.HolderComments
    chk_show_comments.Checked = m_player_data.CardData.PlayerTracking.ShowCommentsOnCashier

    ' VIP
    _is_vip_index = IIf(m_player_data.CardData.PlayerTracking.HolderIsVIP, 1, 0)
    Call SelectItemCombo(_is_vip_index, cb_is_vip)

    'Block
    If m_player_data.CardData.Blocked Then
      lbl_blocked.Text = GLB_NLS_GUI_INVOICING.GetString(238) & " "
      For Each _block_reason_ As KeyValuePair(Of AccountBlockReason, String) In Accounts.GetBlockReason(CType(m_player_data.CardData.BlockReason, Int32))
        lbl_blocked.Text &= _coma & _block_reason_.Value()
        _coma = ", "
      Next
      lbl_block_description.Visible = True
      txt_block_description.Visible = True
      txt_block_description.Text = m_player_data.CardData.BlockDescription
    ElseIf Not String.IsNullOrEmpty(m_player_data.CardData.BlockDescription) Then
      lbl_blocked.Text = GLB_NLS_GUI_INVOICING.GetString(239) & "."
      lbl_block_description.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5896)
      lbl_block_description.Visible = True
      txt_block_description.Visible = True
      txt_block_description.Text = m_player_data.CardData.BlockDescription
    Else
      lbl_blocked.Text = GLB_NLS_GUI_INVOICING.GetString(239) & "."
      lbl_block_description.Visible = False
      txt_block_description.Visible = False
      txt_block_description.Text = ""
    End If

    'Pin
    m_empty_pin = String.IsNullOrEmpty(m_player_data.CardData.PlayerTracking.Pin)

    ef_new_pin.TextValue = ef_new_pin.TextValue.PadLeft(WSI.Common.Accounts.PIN_MIN_LENGTH, "0")
    ef_confirm_pin.TextValue = ef_confirm_pin.TextValue.PadLeft(WSI.Common.Accounts.PIN_MIN_LENGTH, "9")


    'Beneficiary
    chk_ben_holder_data.Checked = m_player_data.CardData.PlayerTracking.HolderHasBeneficiary
    If Not chk_ben_holder_data.Checked Then
      Call DisableAndCleanBeneficiaryData()
    End If

    m_beneficiary_data = New Collection()

    m_beneficiary_data.Add(m_player_data.CardData.PlayerTracking.BeneficiaryName3, "name")
    m_beneficiary_data.Add(m_player_data.CardData.PlayerTracking.BeneficiaryName1, "surname_1")
    m_beneficiary_data.Add(m_player_data.CardData.PlayerTracking.BeneficiaryName2, "surname_2")
    m_beneficiary_data.Add(m_player_data.CardData.PlayerTracking.BeneficiaryId1, "id1")
    m_beneficiary_data.Add(m_player_data.CardData.PlayerTracking.BeneficiaryId2, "id2")
    m_beneficiary_data.Add(m_player_data.CardData.PlayerTracking.BeneficiaryOccupationId, "occupation")
    m_beneficiary_data.Add(m_player_data.CardData.PlayerTracking.BeneficiaryGender, "gender")
    m_beneficiary_data.Add(m_player_data.CardData.PlayerTracking.BeneficiaryBirthDate, "birthdate")

    ' defect value for this check
    chk_FreeMode.Checked = True

  End Sub ' SetData

  Private Sub GetNextLevelInfo()

    Dim _next_level_info As AccountNextLevel
    Dim _generated As WSI.Common.Points
    Dim _discretionaries As WSI.Common.Points
    Dim _level_points As Decimal 'WSI.Common.Points
    Dim _required As WSI.Common.Points
    Dim _maintain As WSI.Common.Points

    _next_level_info = New AccountNextLevel(m_player_data.CardData.AccountId)

    If _next_level_info.LevelId >= 0 And Not WSI.Common.GeneralParam.GetBoolean("Site", "MultiSiteMember", False) Then
      _generated = Math.Truncate(_next_level_info.PointsGenerated)
      _discretionaries = Math.Truncate(_next_level_info.PointsDiscretionaries)
      _level_points = _generated
      _level_points += _discretionaries
      _maintain = WSI.Common.GeneralParam.GetInt32("PlayerTracking", "Level0" & m_player_data.CardData.PlayerTracking.CardLevel & ".PointsToKeep")
      _maintain = Math.Max(_maintain - (_level_points), 0)
      lbl_points_to_maintain.Text = IIf(m_player_data.CardData.PlayerTracking.CardLevel <= 1, "---", _maintain.ToString())
      lbl_points_to_maintain.TextAlign = IIf(m_player_data.CardData.PlayerTracking.CardLevel <= 1, ContentAlignment.MiddleLeft, ContentAlignment.MiddleRight)
      _required = WSI.Common.GeneralParam.GetInt32("PlayerTracking", "Level0" & _next_level_info.LevelId & ".PointsToEnter")
      lbl_points_discretionaries.Text = IIf(CDec(_discretionaries) <= 0, "---", _discretionaries.ToString())
      lbl_points_generated.Text = IIf(CDec(_generated) <= 0, "---", _generated.ToString())
      lbl_points_level.Text = IIf(CDec(_level_points) <= 0, "---", GUI_FormatCurrency(_level_points, 0, ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE, False))

      If _next_level_info.LevelId <> m_player_data.CardData.PlayerTracking.CardLevel Then
        lbl_next_level_name.Text = _next_level_info.LevelName
        lbl_points_to_next_level.Text = _next_level_info.PointsToReach.ToString()
        lbl_points_req.Text = _required.ToString()
      End If

    Else
      gb_next_level.Visible = False
    End If

  End Sub

  ' PURPOSE: Fill the controls of "Beneficiary" Tab.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS: none
  Private Sub EnableAndFillBeneficiaryData()

    'enable controls
    ef_ben_name.Enabled = True
    ef_ben_surname_1.Enabled = True
    ef_ben_surname_2.Enabled = True
    ef_ben_doc_id1.Enabled = True
    ef_ben_doc_id2.Enabled = True
    cb_ben_occupation.Enabled = True
    cb_ben_gender.Enabled = True
    dtp_ben_birthdate.Enabled = True

    If Not String.IsNullOrEmpty(m_player_data.CardData.PlayerTracking.BeneficiaryId3Type) Then
      btn_ben_document.Enabled = True
    End If

    m_read_ben_birthdate = dtp_ben_birthdate.Value


    'fill controls
    If IsNothing(m_beneficiary_data) Then

      ef_ben_name.TextValue = m_player_data.CardData.PlayerTracking.BeneficiaryName3
      ef_ben_surname_1.TextValue = m_player_data.CardData.PlayerTracking.BeneficiaryName1
      ef_ben_surname_2.TextValue = m_player_data.CardData.PlayerTracking.BeneficiaryName2
      ef_ben_doc_id1.TextValue = m_player_data.CardData.PlayerTracking.BeneficiaryId1
      ef_ben_doc_id2.TextValue = m_player_data.CardData.PlayerTracking.BeneficiaryId2

      Occupations.Update(cb_ben_occupation.DataSource, m_player_data.CardData.PlayerTracking.BeneficiaryOccupationId, 1)
      Call SelectItemCombo(m_player_data.CardData.PlayerTracking.BeneficiaryOccupationId, cb_ben_occupation)

      Call SelectItemCombo(m_player_data.CardData.PlayerTracking.BeneficiaryGender, cb_ben_gender)

      If m_player_data.CardData.PlayerTracking.BeneficiaryBirthDate <> DateTime.MinValue Then
        dtp_ben_birthdate.Value = New Date(m_player_data.CardData.PlayerTracking.BeneficiaryBirthDate.Year, _
                                       m_player_data.CardData.PlayerTracking.BeneficiaryBirthDate.Month, _
                                       m_player_data.CardData.PlayerTracking.BeneficiaryBirthDate.Day)
        dtp_ben_birthdate.Checked = True
      Else
        dtp_ben_birthdate.Checked = False
      End If

    Else

      ef_ben_name.TextValue = m_beneficiary_data.Item("name")
      ef_ben_surname_1.TextValue = m_beneficiary_data.Item("surname_1")
      ef_ben_surname_2.TextValue = m_beneficiary_data.Item("surname_2")
      ef_ben_doc_id1.TextValue = m_beneficiary_data.Item("id1")
      ef_ben_doc_id2.TextValue = m_beneficiary_data.Item("id2")
      Call SelectItemCombo(m_beneficiary_data.Item("occupation"), cb_ben_occupation)

      Call SelectItemCombo(m_beneficiary_data.Item("gender"), cb_ben_gender)

      If m_beneficiary_data.Item("birthDate") <> DateTime.MinValue Then
        dtp_ben_birthdate.Value = m_beneficiary_data.Item("birthdate")
        dtp_ben_birthdate.Checked = True
      Else
        dtp_ben_birthdate.Checked = False
      End If

    End If

  End Sub
  ' PURPOSE: Block some controls of "Beneficiary" Tab.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS: none
  Private Sub DisableAndCleanBeneficiaryData()

    'disable controls
    ef_ben_name.Enabled = False
    ef_ben_surname_1.Enabled = False
    ef_ben_surname_2.Enabled = False
    ef_ben_doc_id1.Enabled = False
    ef_ben_doc_id2.Enabled = False
    cb_ben_occupation.Enabled = False
    cb_ben_gender.Enabled = False
    dtp_ben_birthdate.Enabled = False
    btn_ben_document.Enabled = False

    'clean controls
    ef_ben_name.TextValue = String.Empty
    ef_ben_surname_1.TextValue = String.Empty
    ef_ben_surname_2.TextValue = String.Empty
    ef_ben_doc_id1.TextValue = String.Empty
    ef_ben_doc_id2.TextValue = String.Empty
    cb_ben_occupation.SelectedIndex = 0
    cb_ben_occupation.SelectedIndex = -1
    cb_ben_gender.SelectedIndex = 0
    cb_ben_gender.SelectedIndex = -1
    dtp_ben_birthdate.Checked = False
  End Sub

  ' PURPOSE: Get data from the user control.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:CLASS_PLAYER_DATA
  Public Function GetData() As CLASS_PLAYER_DATA

    If Not chk_FreeMode.Checked Then
      ReplicateAddress()
    End If

    FillStructAddress(m_address_modified)

    m_player_data.CardData.PlayerTracking.HolderName3 = ef_name.TextValue
    m_player_data.CardData.PlayerTracking.HolderName4 = ef_middle_name.TextValue
    m_player_data.CardData.PlayerTracking.HolderName1 = ef_surname_1.TextValue
    m_player_data.CardData.PlayerTracking.HolderName2 = ef_surname_2.TextValue
    m_player_data.CardData.PlayerTracking.HolderName = CardData.FormatFullName(FULL_NAME_TYPE.ACCOUNT, ef_name.TextValue, ef_middle_name.TextValue, ef_surname_1.TextValue, ef_surname_2.TextValue)

    m_player_data.CardData.PlayerTracking.HolderIdType = cb_doc_type.Value(cb_doc_type.SelectedIndex)
    m_player_data.CardData.PlayerTracking.HolderId = ef_doc_id.TextValue

    m_player_data.CardData.PlayerTracking.HolderId1 = ef_doc_id1.TextValue
    m_player_data.CardData.PlayerTracking.HolderId1Type = ACCOUNT_HOLDER_ID_TYPE.RFC

    m_player_data.CardData.PlayerTracking.HolderId2 = ef_doc_id2.TextValue
    m_player_data.CardData.PlayerTracking.HolderId2Type = ACCOUNT_HOLDER_ID_TYPE.CURP

    '' Trick
    '' Trick to apply login with HolderId,HolderId1,HolderId2 members
    '' and avoid detect changes when not done.
    Call WSI.Common.CardData.FillHolderDocumentFields(m_player_data.CardData.PlayerTracking)

    m_player_data.CardData.PlayerTracking.HolderGender = cb_gender.Value(cb_gender.SelectedIndex)
    m_player_data.CardData.PlayerTracking.HolderIsVIP = cb_is_vip.SelectedIndex  ' 0 = No. 1 = Yes. 
    m_player_data.CardData.PlayerTracking.HolderMaritalStatus = cb_marital_status.Value(cb_marital_status.SelectedIndex)
    If dtp_birthdate.Value <> m_read_birthdate Then
      m_player_data.CardData.PlayerTracking.HolderBirthDate = dtp_birthdate.Value
    End If
    If dtp_wedding.Checked Then
      m_player_data.CardData.PlayerTracking.HolderWeddingDate = dtp_wedding.Value
    Else
      m_player_data.CardData.PlayerTracking.HolderWeddingDate = DateTime.MinValue
    End If
    m_player_data.CardData.PlayerTracking.HolderPhone01 = ef_phone_1.TextValue
    m_player_data.CardData.PlayerTracking.HolderPhone02 = ef_phone_2.TextValue
    m_player_data.CardData.PlayerTracking.HolderEmail01 = ef_email_1.TextValue
    m_player_data.CardData.PlayerTracking.HolderEmail02 = ef_email_2.TextValue
    m_player_data.CardData.PlayerTracking.HolderTwitter = ef_twitter.TextValue
    m_player_data.CardData.PlayerTracking.HolderAddress01 = ef_street.TextValue
    m_player_data.CardData.PlayerTracking.HolderAddress02 = ef_colony.TextValue
    m_player_data.CardData.PlayerTracking.HolderAddress03 = ef_delegation.TextValue
    m_player_data.CardData.PlayerTracking.HolderCity = ef_city.TextValue
    m_player_data.CardData.PlayerTracking.HolderZip = ef_zip.TextValue
    m_player_data.CardData.PlayerTracking.HolderComments = txt_comments.Text
    m_player_data.CardData.PlayerTracking.ShowCommentsOnCashier = chk_show_comments.Checked
    m_player_data.CardData.PlayerTracking.HolderNationality = cb_nationality.Value(cb_nationality.SelectedIndex)
    m_player_data.CardData.PlayerTracking.HolderAddressCountry = cb_address_country.Value(cb_address_country.SelectedIndex)

    ' 04-JUL-2018  DLM    Bug : WIGOS-13382 Losing changes warning message appear without any change in the screen
    'm_player_data.CardData.PlayerTracking.HolderAddressValidation = CardData.GetHolderAddressValidationValue(m_address_original, m_address_modified, chk_FreeMode.Checked, m_player_data.CardData.PlayerTracking.HolderAddressValidation, m_player_data.CardData.PlayerTracking.HolderAddressCountry)
    Dim _original_holder_address_validation As ADDRESS_VALIDATION = m_player_data.CardData.PlayerTracking.HolderAddressValidation 'DLM TO DEL 
    Dim _holder_address_validation = CardData.GetHolderAddressValidationValue(m_address_original, m_address_modified, chk_FreeMode.Checked, m_player_data.CardData.PlayerTracking.HolderAddressValidation, m_player_data.CardData.PlayerTracking.HolderAddressCountry)

    If _holder_address_validation = ADDRESS_VALIDATION.VALIDATION_SKIPPED Then
      m_player_data.CardData.PlayerTracking.HolderAddressValidation = _original_holder_address_validation
    Else
      m_player_data.CardData.PlayerTracking.HolderAddressValidation = _holder_address_validation
    End If
    ' 04-JUL-2018  DLM    Bug : WIGOS-13382 Losing changes warning message appear without any change in the screen

    m_player_data.CardData.PlayerTracking.HolderFedEntity = cb_federal_entity.Value(cb_federal_entity.SelectedIndex)
    m_player_data.CardData.PlayerTracking.HolderBirthCountry = cb_birthplace.Value(cb_birthplace.SelectedIndex)
    m_player_data.CardData.PlayerTracking.HolderOccupationId = cb_occupation.Value(cb_occupation.SelectedIndex)
    m_player_data.CardData.PlayerTracking.HolderExtNumber = ef_external_number.TextValue

    'beneficiary
    m_player_data.CardData.PlayerTracking.HolderHasBeneficiary = chk_ben_holder_data.Checked
    m_player_data.CardData.PlayerTracking.BeneficiaryName3 = ef_ben_name.TextValue
    m_player_data.CardData.PlayerTracking.BeneficiaryName1 = ef_ben_surname_1.TextValue
    m_player_data.CardData.PlayerTracking.BeneficiaryName2 = ef_ben_surname_2.TextValue
    m_player_data.CardData.PlayerTracking.BeneficiaryId1 = ef_ben_doc_id1.TextValue
    m_player_data.CardData.PlayerTracking.BeneficiaryId2 = ef_ben_doc_id2.TextValue
    m_player_data.CardData.PlayerTracking.BeneficiaryGender = cb_ben_gender.Value(cb_ben_gender.SelectedIndex)
    m_player_data.CardData.PlayerTracking.BeneficiaryOccupationId = cb_ben_occupation.Value(cb_ben_occupation.SelectedIndex)
    m_player_data.CardData.PlayerTracking.BeneficiaryName = CardData.FormatFullName(FULL_NAME_TYPE.ACCOUNT, ef_ben_name.TextValue, ef_ben_surname_1.TextValue, ef_ben_surname_2.TextValue)

    If dtp_ben_birthdate.Checked Then
      m_player_data.CardData.PlayerTracking.BeneficiaryBirthDate = dtp_ben_birthdate.Value
    Else
      m_player_data.CardData.PlayerTracking.BeneficiaryBirthDate = DateTime.MinValue
    End If

    Return m_player_data

  End Function ' GetData

  ' PURPOSE: Set federal entity Id for Autoselection
  '
  '  PARAMS:
  '     - INPUT:
  '         - FederalEntity selected in combo
  '
  '     - OUTPUT:
  '
  ' RETURNS: Federal entity Id
  Private Function SetFederalEntityId(ByVal FederalEntity As String) As Integer
    Dim _dr() As DataRow
    Try
      _dr = WSI.Common.CardData.GetFedStatesList().Select("FS_NAME = '" + FederalEntity + "'")
      If _dr.Length > 0 Then
        Return _dr(0).Item("FS_STATE_ID")
      End If

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try
    Return Nothing
  End Function

  ' PURPOSE: Check control data
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS: True if not empty fields
  Public Function IsControlDataOk() As Boolean

    If Not CheckRequiredEmptyData() Then

      Return False
    End If

    If Not ValidateFormat() Then

      Return False
    End If

    If Not CheckDuplicatePlayerData() Then

      Return False
    End If

    Return True

  End Function ' IsControlDataOk

  ' PURPOSE: Check required fields
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS: True if not empty fields
  Public Function CheckRequiredEmptyData() As Boolean

    Dim _controls_to_check As RequiredData
    Dim _focus_control As ENUM_CARDDATA_FIELD
    Dim _label_control As String

    _controls_to_check = New RequiredData(PLAYER_DATA_TO_CHECK.ACCOUNT)
    _label_control = ""

    'GENERAL PARAMS Account Defined Checks

    _controls_to_check.Add(ENUM_CARDDATA_FIELD.NAME3, ef_name.TextValue)
    _controls_to_check.Add(ENUM_CARDDATA_FIELD.NAME4, ef_middle_name.TextValue)
    _controls_to_check.Add(ENUM_CARDDATA_FIELD.NAME1, ef_surname_1.TextValue)
    _controls_to_check.Add(ENUM_CARDDATA_FIELD.NAME2, ef_surname_2.TextValue)

    _controls_to_check.Add(ENUM_CARDDATA_FIELD.ID1, ef_doc_id1.TextValue)
    _controls_to_check.Add(ENUM_CARDDATA_FIELD.ID2, ef_doc_id2.TextValue)
    _controls_to_check.Add(ENUM_CARDDATA_FIELD.ID, ef_doc_id.TextValue)
    _controls_to_check.Add(ENUM_CARDDATA_FIELD.DOC_TYPE, IIf(cb_doc_type.SelectedIndex >= 0, cb_doc_type.Value(cb_doc_type.SelectedIndex), ""))

    _controls_to_check.Add(ENUM_CARDDATA_FIELD.BIRTHDATE, dtp_birthdate.Value)

    _controls_to_check.Add(ENUM_CARDDATA_FIELD.WEDDING_DATE, dtp_wedding.Checked)

    _controls_to_check.Add(ENUM_CARDDATA_FIELD.GENDER, cb_gender.TextValue)
    _controls_to_check.Add(ENUM_CARDDATA_FIELD.NATIONALITY, cb_nationality.TextValue)
    _controls_to_check.Add(ENUM_CARDDATA_FIELD.BIRTH_COUNTRY, cb_birthplace.TextValue)
    _controls_to_check.Add(ENUM_CARDDATA_FIELD.OCCUPATION, cb_occupation.TextValue)
    _controls_to_check.Add(ENUM_CARDDATA_FIELD.MARITAL_STATUS, cb_marital_status.TextValue)

    _controls_to_check.Add(ENUM_CARDDATA_FIELD.PHONE1, ef_phone_1.TextValue)
    _controls_to_check.Add(ENUM_CARDDATA_FIELD.PHONE2, ef_phone_2.TextValue)
    _controls_to_check.Add(ENUM_CARDDATA_FIELD.EMAIL1, ef_email_1.TextValue)
    _controls_to_check.Add(ENUM_CARDDATA_FIELD.EMAIL2, ef_email_2.TextValue)
    _controls_to_check.Add(ENUM_CARDDATA_FIELD.TWITTER, ef_twitter.TextValue)

    If chk_FreeMode.Checked Then
      _controls_to_check.Add(ENUM_CARDDATA_FIELD.STREET, ef_street.TextValue)
      _controls_to_check.Add(ENUM_CARDDATA_FIELD.EXTERNAL_NUMBER, ef_external_number.TextValue)
      _controls_to_check.Add(ENUM_CARDDATA_FIELD.COLONY, ef_colony.TextValue)
      _controls_to_check.Add(ENUM_CARDDATA_FIELD.DELEGATION, ef_delegation.TextValue)
      _controls_to_check.Add(ENUM_CARDDATA_FIELD.CITY, ef_city.TextValue)
      _controls_to_check.Add(ENUM_CARDDATA_FIELD.ZIP, ef_zip.TextValue)
      _controls_to_check.Add(ENUM_CARDDATA_FIELD.ADDRESS_COUNTRY, cb_address_country.TextValue)
      _controls_to_check.Add(ENUM_CARDDATA_FIELD.FEDERAL_ENTITY, cb_federal_entity.TextValue)
      _controls_to_check.Add(ENUM_CARDDATA_FIELD.COMMENTS, txt_comments.Text)
    End If

    If _controls_to_check.IsAnyEmpty(False, _focus_control) Then
      Call SetControlFocus(_focus_control, PLAYER_DATA_TO_CHECK.ACCOUNT, _label_control)
      NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", _label_control), ENUM_MB_TYPE.MB_TYPE_WARNING)
      Return False
    End If

    If Not chk_FreeMode.Checked Then
      If String.IsNullOrEmpty(ef_as_street.TextValue) Then
        Call SetControlFocus(ENUM_CARDDATA_FIELD.STREET, PLAYER_DATA_TO_CHECK.ACCOUNT, _label_control)
        NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", _label_control), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Return False
      End If

      If String.IsNullOrEmpty(ef_as_external_number.TextValue) Then
        Call SetControlFocus(ENUM_CARDDATA_FIELD.EXTERNAL_NUMBER, PLAYER_DATA_TO_CHECK.ACCOUNT, _label_control)
        NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", _label_control), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Return False
      End If
    End If

    _controls_to_check.Clear()

    ' GENERAL PARAMS Beneficiary Defined Checks

    If chk_ben_holder_data.Checked Then

      _controls_to_check = New RequiredData(PLAYER_DATA_TO_CHECK.BENEFICIARY)

      _controls_to_check.Add(ENUM_CARDDATA_FIELD.NAME3, ef_ben_name.TextValue)
      _controls_to_check.Add(ENUM_CARDDATA_FIELD.NAME1, ef_ben_surname_1.TextValue)
      _controls_to_check.Add(ENUM_CARDDATA_FIELD.NAME2, ef_ben_surname_2.TextValue)

      _controls_to_check.Add(ENUM_CARDDATA_FIELD.ID1, ef_ben_doc_id1.TextValue)
      _controls_to_check.Add(ENUM_CARDDATA_FIELD.ID2, ef_ben_doc_id2.TextValue)

      _controls_to_check.Add(ENUM_CARDDATA_FIELD.BIRTHDATE, dtp_ben_birthdate.Checked)
      _controls_to_check.Add(ENUM_CARDDATA_FIELD.GENDER, cb_ben_gender.TextValue)
      _controls_to_check.Add(ENUM_CARDDATA_FIELD.OCCUPATION, cb_ben_occupation.TextValue)

      If _controls_to_check.IsAnyEmpty(False, _focus_control) Then
        Call SetControlFocus(_focus_control, PLAYER_DATA_TO_CHECK.BENEFICIARY, _label_control)
        NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", _label_control), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Return False
      End If

    End If

    If m_pin_required AndAlso m_empty_pin Then
      Call SetControlFocus(ENUM_CARDDATA_FIELD.PIN, PLAYER_DATA_TO_CHECK.PIN, _label_control)
      NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_NO_PIN"), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return False
    End If

    Return True

  End Function


  ' PURPOSE: Check if exist duplicated data on database
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS: True if exist duplicated data, False otherwise
  Public Function CheckDuplicatePlayerData() As Boolean

    Dim _data As CardData
    Dim _allow_duplicate As Boolean
    Dim _message As String

    'check document id
    If Not String.IsNullOrEmpty(ef_doc_id.TextValue) Then
      If CardData.CheckIdAlreadyExists(ef_doc_id.TextValue, m_player_data.CardData.AccountId, cb_doc_type.Value(cb_doc_type.SelectedIndex)) Then
        NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_HOLDER_ID_ALREADY_EXISTS"), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK)

        Return False
      End If
    End If

    'check RFC
    If Not String.IsNullOrEmpty(ef_doc_id1.TextValue) Then
      If CardData.CheckIdAlreadyExists(ef_doc_id1.TextValue, m_player_data.CardData.AccountId, ACCOUNT_HOLDER_ID_TYPE.RFC) Then
        NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_HOLDER_ID_ALREADY_EXISTS"), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK)

        Return False
      End If
    End If

    'check CURP
    If Not String.IsNullOrEmpty(ef_doc_id2.TextValue) Then
      If CardData.CheckIdAlreadyExists(ef_doc_id2.TextValue, m_player_data.CardData.AccountId, ACCOUNT_HOLDER_ID_TYPE.CURP) Then
        NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_HOLDER_ID_ALREADY_EXISTS"), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK)

        Return False
      End If
    End If

    'check card data 
    _data = Me.GetData().CardData
    _message = ""

    If CardData.CheckDuplicateAccounts(_data, _message, _allow_duplicate) Then

      Return True
    End If

    If _allow_duplicate Then
      If NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), _message, ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO) = ENUM_MB_RESULT.MB_RESULT_YES Then

        Return True
      Else

        Return False
      End If
    Else
      NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), _message, ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK)

      Return False
    End If

  End Function ' HasDuplicatePlayerData

  ' PURPOSE: Check the format of fields
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS: True if all fields has valid format
  Public Function ValidateFormat() As Boolean

    Dim _legal_age As Int32
    Dim _label_control As String
    Dim _regular_expression As String

    _label_control = ""
    _regular_expression = ""

    If Not ef_name.ValidateFormat() Then
      Call SetControlFocus(ENUM_CARDDATA_FIELD.NAME3, PLAYER_DATA_TO_CHECK.ACCOUNT, _label_control)
      NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", _label_control), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return False
    End If

    If Not ef_middle_name.ValidateFormat() Then
      Call SetControlFocus(ENUM_CARDDATA_FIELD.NAME4, PLAYER_DATA_TO_CHECK.ACCOUNT, _label_control)
      NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", _label_control), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return False
    End If

    If Not ef_surname_1.ValidateFormat() Then
      Call SetControlFocus(ENUM_CARDDATA_FIELD.NAME1, PLAYER_DATA_TO_CHECK.ACCOUNT, _label_control)
      NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", _label_control), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return False
    End If

    If Not ef_surname_2.ValidateFormat() Then
      Call SetControlFocus(ENUM_CARDDATA_FIELD.NAME2, PLAYER_DATA_TO_CHECK.ACCOUNT, _label_control)
      NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", _label_control), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return False
    End If

    If Not ef_doc_id.ValidateFormat() Then
      Call SetControlFocus(ENUM_CARDDATA_FIELD.ID, PLAYER_DATA_TO_CHECK.ACCOUNT, _label_control)
      NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", _label_control), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return False
    End If

    If Not ef_doc_id1.ValidateFormat() Then
      Call SetControlFocus(ENUM_CARDDATA_FIELD.ID1, PLAYER_DATA_TO_CHECK.ACCOUNT, _label_control)
      NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", _label_control), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return False
    End If

    If Not String.IsNullOrEmpty(ef_doc_id1.TextValue.Trim()) AndAlso WSI.Common.ValidateFormat.WrongRFC(ef_doc_id1.TextValue.Trim()) Then
      Call SetControlFocus(ENUM_CARDDATA_FIELD.ID1, PLAYER_DATA_TO_CHECK.ACCOUNT, _label_control)
      NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5050), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", _label_control), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return False
    End If


    If Not ef_doc_id2.ValidateFormat() Then
      Call SetControlFocus(ENUM_CARDDATA_FIELD.ID2, PLAYER_DATA_TO_CHECK.ACCOUNT, _label_control)
      NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", _label_control), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return False
    End If

    If Not ef_phone_1.ValidateFormat() Then
      Call SetControlFocus(ENUM_CARDDATA_FIELD.PHONE1, PLAYER_DATA_TO_CHECK.ACCOUNT, _label_control)
      NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", _label_control), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return False
    End If

    If Not ef_phone_2.ValidateFormat() Then
      Call SetControlFocus(ENUM_CARDDATA_FIELD.PHONE2, PLAYER_DATA_TO_CHECK.ACCOUNT, _label_control)
      NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", _label_control), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return False
    End If

    If Not String.IsNullOrEmpty(ef_email_1.TextValue) And Not WSI.Common.ValidateFormat.Email(ef_email_1.TextValue) Then
      Call SetControlFocus(ENUM_CARDDATA_FIELD.EMAIL1, PLAYER_DATA_TO_CHECK.ACCOUNT, _label_control)
      NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", _label_control), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return False
    End If

    If Not String.IsNullOrEmpty(ef_email_2.TextValue) And Not WSI.Common.ValidateFormat.Email(ef_email_2.TextValue) Then
      Call SetControlFocus(ENUM_CARDDATA_FIELD.EMAIL2, PLAYER_DATA_TO_CHECK.ACCOUNT, _label_control)
      NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", _label_control), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return False
    End If

    If Not String.IsNullOrEmpty(ef_twitter.TextValue) And Not WSI.Common.ValidateFormat.Twitter(ef_twitter.TextValue) Then
      Call SetControlFocus(ENUM_CARDDATA_FIELD.TWITTER, PLAYER_DATA_TO_CHECK.ACCOUNT, _label_control)
      NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", _label_control), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return False
    End If

    If Not ef_street.ValidateFormat() Then
      Call SetControlFocus(ENUM_CARDDATA_FIELD.STREET, PLAYER_DATA_TO_CHECK.ACCOUNT, _label_control)
      NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", _label_control), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return False
    End If

    If Not ef_colony.ValidateFormat() Then
      Call SetControlFocus(ENUM_CARDDATA_FIELD.COLONY, PLAYER_DATA_TO_CHECK.ACCOUNT, _label_control)
      NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", _label_control), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return False
    End If

    If Not ef_delegation.ValidateFormat() Then
      Call SetControlFocus(ENUM_CARDDATA_FIELD.DELEGATION, PLAYER_DATA_TO_CHECK.ACCOUNT, _label_control)
      NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", _label_control), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return False
    End If

    If Not ef_city.ValidateFormat() Then
      Call SetControlFocus(ENUM_CARDDATA_FIELD.CITY, PLAYER_DATA_TO_CHECK.ACCOUNT, _label_control)
      NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", _label_control), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return False
    End If

    _regular_expression = WSI.Common.GeneralParam.GetString("Account", "ZipCode.ValidationRegex")
    If Not String.IsNullOrEmpty(ef_zip.TextValue) AndAlso Not WSI.Common.ValidateFormat.CheckRegularExpression(ef_zip.TextValue, _regular_expression) Then
      Call SetControlFocus(ENUM_CARDDATA_FIELD.ZIP, PLAYER_DATA_TO_CHECK.ACCOUNT, _label_control)
      NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", _label_control), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return False
    End If

    If Not chk_FreeMode.Checked Then


      If Not ef_as_external_number.ValidateFormat() Then
        Call SetControlFocus(ENUM_CARDDATA_FIELD.EXTERNAL_NUMBER, PLAYER_DATA_TO_CHECK.ACCOUNT, _label_control)
        NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", _label_control), ENUM_MB_TYPE.MB_TYPE_WARNING)

        Return False
      End If

      If Not ef_as_street.ValidateFormat() Then
        Call SetControlFocus(ENUM_CARDDATA_FIELD.STREET, PLAYER_DATA_TO_CHECK.ACCOUNT, _label_control)
        NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", _label_control), ENUM_MB_TYPE.MB_TYPE_WARNING)

        Return False
      End If

      If Not String.IsNullOrEmpty(ef_as_zip.TextValue) AndAlso _
         Not WSI.Common.ValidateFormat.CheckRegularExpression(ef_as_zip.TextValue, _regular_expression) OrElse _
         ef_as_zip.BackColor = Color.Red Then
        Call SetControlFocus(ENUM_CARDDATA_FIELD.ZIP, PLAYER_DATA_TO_CHECK.ACCOUNT, _label_control)
        NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", _label_control), ENUM_MB_TYPE.MB_TYPE_WARNING)

        Return False
      End If

      If cb_as_federal_entity.BackColor = Color.Red Then
        Call SetControlFocus(ENUM_CARDDATA_FIELD.FEDERAL_ENTITY, PLAYER_DATA_TO_CHECK.ACCOUNT, _label_control)
        NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", _label_control), ENUM_MB_TYPE.MB_TYPE_WARNING)

        Return False
      End If

      If cb_as_delegation.BackColor = Color.Red Then
        Call SetControlFocus(ENUM_CARDDATA_FIELD.DELEGATION, PLAYER_DATA_TO_CHECK.ACCOUNT, _label_control)
        NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", _label_control), ENUM_MB_TYPE.MB_TYPE_WARNING)

        Return False
      End If

      If cb_as_colony.BackColor = Color.Red Then
        Call SetControlFocus(ENUM_CARDDATA_FIELD.COLONY, PLAYER_DATA_TO_CHECK.ACCOUNT, _label_control)
        NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", _label_control), ENUM_MB_TYPE.MB_TYPE_WARNING)

        Return False
      End If

      If cb_as_city.BackColor = Color.Red Then
        Call SetControlFocus(ENUM_CARDDATA_FIELD.CITY, PLAYER_DATA_TO_CHECK.ACCOUNT, _label_control)
        NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", _label_control), ENUM_MB_TYPE.MB_TYPE_WARNING)

        Return False
      End If

    End If

    If Not WSI.Common.CardData.IsValidBirthdate(dtp_birthdate.Value, _legal_age) Then
      Call SetControlFocus(ENUM_CARDDATA_FIELD.BIRTHDATE, PLAYER_DATA_TO_CHECK.ACCOUNT, _label_control)
      NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_NOT_ADULT", _legal_age), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return False
    End If

    If dtp_wedding.Checked Then

      If Not WSI.Common.CardData.IsValidWeddingdate(dtp_birthdate.Value, dtp_wedding.Value) Then
        Call SetControlFocus(ENUM_CARDDATA_FIELD.WEDDING_DATE, PLAYER_DATA_TO_CHECK.ACCOUNT, _label_control)
        NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_WEDDING_ERROR"), ENUM_MB_TYPE.MB_TYPE_WARNING)

        Return False
      End If

    End If

    If Not ef_external_number.ValidateFormat() Then
      Call SetControlFocus(ENUM_CARDDATA_FIELD.EXTERNAL_NUMBER, PLAYER_DATA_TO_CHECK.ACCOUNT, _label_control)
      NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", _label_control), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return False
    End If


    If dtp_ben_birthdate.Checked Then
      If Not WSI.Common.CardData.IsValidBirthdate(dtp_ben_birthdate.Value, _legal_age) Then
        Call SetControlFocus(ENUM_CARDDATA_FIELD.BIRTHDATE, PLAYER_DATA_TO_CHECK.BENEFICIARY, _label_control)
        NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_NOT_ADULT_BENEFICIARY", _legal_age), ENUM_MB_TYPE.MB_TYPE_WARNING)

        Return False
      End If
    End If

    If Not ef_ben_name.ValidateFormat() Then
      Call SetControlFocus(ENUM_CARDDATA_FIELD.NAME3, PLAYER_DATA_TO_CHECK.BENEFICIARY, _label_control)
      NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", _label_control), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return False
    End If

    If Not ef_ben_surname_1.ValidateFormat() Then
      Call SetControlFocus(ENUM_CARDDATA_FIELD.NAME1, PLAYER_DATA_TO_CHECK.BENEFICIARY, _label_control)
      NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", _label_control), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return False
    End If

    If Not ef_ben_surname_2.ValidateFormat() Then
      Call SetControlFocus(ENUM_CARDDATA_FIELD.NAME2, PLAYER_DATA_TO_CHECK.BENEFICIARY, _label_control)
      NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", _label_control), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return False
    End If

    If Not ef_ben_doc_id1.ValidateFormat() Then
      Call SetControlFocus(ENUM_CARDDATA_FIELD.ID1, PLAYER_DATA_TO_CHECK.BENEFICIARY, _label_control)
      NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", _label_control), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return False
    End If

    If Not ef_ben_doc_id2.ValidateFormat() Then
      Call SetControlFocus(ENUM_CARDDATA_FIELD.ID2, PLAYER_DATA_TO_CHECK.BENEFICIARY, _label_control)
      NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", _label_control), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return False
    End If

    Return True

  End Function ' ValidateFormat

#End Region ' Publics

#Region " Privates "

  ' PURPOSE: Set focus on control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub SetControlFocus(ByVal Field As ENUM_CARDDATA_FIELD, ByVal TabToCheck As PLAYER_DATA_TO_CHECK, ByRef Label As String)

    Select Case Field

      Case ENUM_CARDDATA_FIELD.NAME1
        If TabToCheck = PLAYER_DATA_TO_CHECK.ACCOUNT Then
          tab_container.SelectedTab = tab_main
          ef_surname_1.Focus()
          Label = ef_surname_1.Text
        ElseIf TabToCheck = PLAYER_DATA_TO_CHECK.BENEFICIARY Then
          tab_container.SelectedTab = tab_beneficiary
          ef_ben_surname_1.Focus()
          Label = ef_ben_surname_1.Text
        End If
      Case ENUM_CARDDATA_FIELD.NAME2
        If TabToCheck = PLAYER_DATA_TO_CHECK.ACCOUNT Then
          tab_container.SelectedTab = tab_main
          ef_surname_2.Focus()
          Label = ef_surname_2.Text
        ElseIf TabToCheck = PLAYER_DATA_TO_CHECK.BENEFICIARY Then
          tab_container.SelectedTab = tab_beneficiary
          ef_ben_surname_2.Focus()
          Label = ef_ben_surname_2.Text
        End If
      Case ENUM_CARDDATA_FIELD.NAME3
        If TabToCheck = PLAYER_DATA_TO_CHECK.ACCOUNT Then
          tab_container.SelectedTab = tab_main
          ef_name.Focus()
          Label = ef_name.Text
        ElseIf TabToCheck = PLAYER_DATA_TO_CHECK.BENEFICIARY Then
          tab_container.SelectedTab = tab_beneficiary
          ef_ben_name.Focus()
          Label = ef_ben_name.Text
        End If
      Case ENUM_CARDDATA_FIELD.NAME4
        tab_container.SelectedTab = tab_main
        ef_middle_name.Focus()
        Label = ef_middle_name.Text
      Case ENUM_CARDDATA_FIELD.ID
        tab_container.SelectedTab = tab_main
        ef_doc_id.Focus()
        Label = ef_doc_id.Text
      Case ENUM_CARDDATA_FIELD.ID1
        If TabToCheck = PLAYER_DATA_TO_CHECK.ACCOUNT Then
          tab_container.SelectedTab = tab_main
          ef_doc_id1.Focus()
          Label = ef_doc_id1.Text
        ElseIf TabToCheck = PLAYER_DATA_TO_CHECK.BENEFICIARY Then
          tab_container.SelectedTab = tab_beneficiary
          ef_ben_doc_id1.Focus()
          Label = ef_ben_doc_id1.Text
        End If
      Case ENUM_CARDDATA_FIELD.ID2
        If TabToCheck = PLAYER_DATA_TO_CHECK.ACCOUNT Then
          tab_container.SelectedTab = tab_main
          ef_doc_id2.Focus()
          Label = ef_doc_id2.Text
        ElseIf TabToCheck = PLAYER_DATA_TO_CHECK.BENEFICIARY Then
          tab_container.SelectedTab = tab_beneficiary
          ef_ben_doc_id2.Focus()
          Label = ef_ben_doc_id2.Text
        End If
      Case ENUM_CARDDATA_FIELD.GENDER
        If TabToCheck = PLAYER_DATA_TO_CHECK.ACCOUNT Then
          tab_container.SelectedTab = tab_main
          cb_gender.Focus()
          Label = cb_gender.Text
        ElseIf TabToCheck = PLAYER_DATA_TO_CHECK.BENEFICIARY Then
          tab_container.SelectedTab = tab_beneficiary
          cb_ben_gender.Focus()
          Label = cb_ben_gender.Text
        End If
      Case ENUM_CARDDATA_FIELD.DOC_TYPE
        tab_container.SelectedTab = tab_main
        cb_doc_type.Focus()
        Label = cb_doc_type.Text
      Case ENUM_CARDDATA_FIELD.MARITAL_STATUS
        tab_container.SelectedTab = tab_main
        cb_marital_status.Focus()
        Label = cb_marital_status.Text
      Case ENUM_CARDDATA_FIELD.BIRTHDATE
        If TabToCheck = PLAYER_DATA_TO_CHECK.ACCOUNT Then
          tab_container.SelectedTab = tab_main
          dtp_birthdate.Focus()
          Label = dtp_birthdate.Text
        ElseIf TabToCheck = PLAYER_DATA_TO_CHECK.BENEFICIARY Then
          tab_container.SelectedTab = tab_beneficiary
          dtp_ben_birthdate.Focus()
          Label = dtp_ben_birthdate.Text
        End If
      Case ENUM_CARDDATA_FIELD.WEDDING_DATE
        tab_container.SelectedTab = tab_main
        dtp_wedding.Focus()
        Label = dtp_wedding.Text
      Case ENUM_CARDDATA_FIELD.PHONE1
        tab_container.SelectedTab = tab_contact
        ef_phone_1.Focus()
        Label = ef_phone_1.Text
      Case ENUM_CARDDATA_FIELD.PHONE2
        tab_container.SelectedTab = tab_contact
        ef_phone_2.Focus()
        Label = ef_phone_2.Text
      Case ENUM_CARDDATA_FIELD.EMAIL1
        tab_container.SelectedTab = tab_contact
        ef_email_1.Focus()
        Label = ef_email_1.Text
      Case ENUM_CARDDATA_FIELD.EMAIL2
        tab_container.SelectedTab = tab_contact
        ef_email_2.Focus()
        Label = ef_email_2.Text
      Case ENUM_CARDDATA_FIELD.TWITTER
        tab_container.SelectedTab = tab_contact
        ef_twitter.Focus()
        Label = ef_twitter.Text
      Case ENUM_CARDDATA_FIELD.PIN
        tab_container.SelectedTab = tab_pin
        ef_new_pin.Focus()
        Label = ef_new_pin.Text
      Case ENUM_CARDDATA_FIELD.STREET
        tab_container.SelectedTab = tab_address
        ef_street.Focus()
        Label = ef_street.Text
      Case ENUM_CARDDATA_FIELD.COLONY
        tab_container.SelectedTab = tab_address
        ef_colony.Focus()
        Label = ef_colony.Text
      Case ENUM_CARDDATA_FIELD.DELEGATION
        tab_container.SelectedTab = tab_address
        ef_delegation.Focus()
        Label = ef_delegation.Text
      Case ENUM_CARDDATA_FIELD.CITY
        tab_container.SelectedTab = tab_address
        ef_city.Focus()
        Label = ef_city.Text
      Case ENUM_CARDDATA_FIELD.ZIP
        tab_container.SelectedTab = tab_address
        ef_zip.Focus()
        Label = ef_zip.Text
      Case ENUM_CARDDATA_FIELD.EXTERNAL_NUMBER
        tab_container.SelectedTab = tab_address
        ef_external_number.Focus()
        Label = ef_external_number.Text
      Case ENUM_CARDDATA_FIELD.COMMENTS
        tab_container.SelectedTab = tab_comments
        txt_comments.Focus()
        Label = tab_comments.Text
      Case ENUM_CARDDATA_FIELD.OCCUPATION
        If TabToCheck = PLAYER_DATA_TO_CHECK.ACCOUNT Then
          tab_container.SelectedTab = tab_main
          cb_occupation.Focus()
          Label = cb_occupation.Text
        ElseIf TabToCheck = PLAYER_DATA_TO_CHECK.BENEFICIARY Then
          tab_container.SelectedTab = tab_beneficiary
          cb_ben_occupation.Focus()
          Label = cb_ben_occupation.Text
        End If
      Case ENUM_CARDDATA_FIELD.NATIONALITY
        tab_container.SelectedTab = tab_main
        cb_nationality.Focus()
        Label = cb_nationality.Text
      Case ENUM_CARDDATA_FIELD.BIRTH_COUNTRY
        tab_container.SelectedTab = tab_main
        cb_birthplace.Focus()
        Label = cb_birthplace.Text
      Case ENUM_CARDDATA_FIELD.ADDRESS_COUNTRY
        tab_container.SelectedTab = tab_address
        cb_address_country.Focus()
        Label = cb_address_country.Text
      Case ENUM_CARDDATA_FIELD.FEDERAL_ENTITY
        tab_container.SelectedTab = tab_address
        cb_federal_entity.Focus()
        Label = cb_federal_entity.Text
      Case Else
        Label = ""

    End Select

  End Sub ' SetControlFocus

  ' PURPOSE: Set the text label to controls
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS: 
  Private Sub SetTextControls()

    Dim _rfc As String
    Dim _curp As String

    _rfc = IdentificationTypes.DocIdTypeString(ACCOUNT_HOLDER_ID_TYPE.RFC)
    _curp = IdentificationTypes.DocIdTypeString(ACCOUNT_HOLDER_ID_TYPE.CURP)

    'tab_main
    tab_main.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1713)
    ef_name.Text = AccountFields.GetName()
    ef_middle_name.Text = AccountFields.GetMiddleName()
    ef_surname_1.Text = AccountFields.GetSurname1()
    ef_surname_2.Text = AccountFields.GetSurname2()
    cb_is_vip.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4802)
    cb_doc_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1721)
    ef_doc_id.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1722)
    ef_doc_id1.Text = _rfc
    ef_doc_id2.Text = _curp
    cb_gender.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1723)
    cb_marital_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1724)
    dtp_birthdate.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1725)
    dtp_wedding.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1726)
    cb_occupation.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2058)
    btn_document.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(749)
    cb_birthplace.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2088)
    cb_nationality.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2089)

    'tab_contact
    tab_contact.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1714)
    ef_phone_1.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1727, "1")
    ef_phone_2.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1727, "2")
    ef_email_1.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1729, "1")
    ef_email_2.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1729, "2")
    ef_twitter.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1730)

    'tab_address
    tab_address.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1715)
    ef_street.Text = AccountFields.GetAddressName() 'GLB_NLS_GUI_PLAYER_TRACKING.GetString(1731)
    ef_colony.Text = AccountFields.GetColonyName() 'GLB_NLS_GUI_PLAYER_TRACKING.GetString(1732)
    ef_delegation.Text = AccountFields.GetDelegationName() 'GLB_NLS_GUI_PLAYER_TRACKING.GetString(1733)
    ef_city.Text = AccountFields.GetTownshipName() 'GLB_NLS_GUI_PLAYER_TRACKING.GetString(1734)
    ef_zip.Text = AccountFields.GetZipName() 'GLB_NLS_GUI_PLAYER_TRACKING.GetString(1735)
    ef_external_number.Text = AccountFields.GetNumExtName() 'GLB_NLS_GUI_PLAYER_TRACKING.GetString(2091)
    cb_federal_entity.Text = AccountFields.GetStateName()
    cb_address_country.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5090)

    'tab_address autoselection
    ef_as_street.Text = AccountFields.GetAddressName() 'GLB_NLS_GUI_PLAYER_TRACKING.GetString(1731)
    cb_as_colony.Text = AccountFields.GetColonyName() 'GLB_NLS_GUI_PLAYER_TRACKING.GetString(1732)
    cb_as_delegation.Text = AccountFields.GetDelegationName() 'GLB_NLS_GUI_PLAYER_TRACKING.GetString(1733)
    cb_as_city.Text = AccountFields.GetTownshipName() 'GLB_NLS_GUI_PLAYER_TRACKING.GetString(1734)
    ef_as_zip.Text = AccountFields.GetZipName() 'GLB_NLS_GUI_PLAYER_TRACKING.GetString(1735)
    ef_as_external_number.Text = AccountFields.GetNumExtName() 'GLB_NLS_GUI_PLAYER_TRACKING.GetString(2091)
    cb_as_federal_entity.Text = AccountFields.GetStateName()
    cb_as_address_country.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5090)
    chk_FreeMode.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6704)

    'tab block
    tab_block.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1409) '1409 "Block"  
    lbl_block_description.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1955)

    'tab_comments
    tab_comments.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1716)
    chk_show_comments.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5081)

    'tab_pin
    tab_pin.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1717)
    ef_new_pin.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1736)
    ef_confirm_pin.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1737)
    btn_save_pin.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1738)

    'tab_beneficiary
    tab_beneficiary.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2060)
    ef_ben_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1718)
    ef_ben_surname_1.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1719)
    ef_ben_surname_2.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1720)
    ef_ben_doc_id1.Text = _rfc
    ef_ben_doc_id2.Text = _curp
    btn_ben_document.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(749)
    dtp_ben_birthdate.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1725)
    cb_ben_gender.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1723)
    cb_ben_occupation.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2058)
    chk_ben_holder_data.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2217)

    'tab_points_program
    tab_points_program.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2762)
    lbl_level.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2763)
    lbl_entry_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2764)
    lbl_expiration_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2765)
    lbl_points_to_keep_level.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2766)
    lbl_balance.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2767)
    gb_next_level.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2768)
    lbl_points_req_prefix.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2769)
    lbl_points_generated_prefix.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2770)
    lbl_points_discretionaries_prefix.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2771)
    lbl_points_level_prefix.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2772)
    lbl_points_to_next_level_prefix.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2773)
    lbl_points_last_days.Text = "*" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2774, WSI.Common.GeneralParam.GetString("PlayerTracking", "Levels.DaysCountingPoints"))

  End Sub
  ' SetTextControls

  ' PURPOSE: Set format to controls
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS: 
  Private Sub SetFormatControls()

    Dim _min_date As DateTime
    Dim _max_date As DateTime
    Dim _now As DateTime
    Dim _kiosk_enabled As Boolean

    'Dates range are calculated  as the same way as cashier
    _now = WSI.Common.WGDB.Now
    _min_date = New DateTime(_now.Year - WSI.Common.ValidateFormat.YEAR_MAX_INTERVAL_ALLOWED, 1, 1)
    _max_date = New DateTime(_now.Year + 1, 12, 31)


    'tab_main
    ef_name.TextVisible = True
    ef_name.SetFilter(GUI_Controls.CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_BASE, 50)
    ef_middle_name.TextVisible = True
    ef_middle_name.SetFilter(GUI_Controls.CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_BASE, 50)
    ef_surname_1.TextVisible = True
    ef_surname_1.SetFilter(GUI_Controls.CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_BASE, 50)
    ef_surname_2.TextVisible = True
    ef_surname_2.SetFilter(GUI_Controls.CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_BASE, 50)
    ef_doc_id.TextVisible = True
    ef_doc_id.SetFilter(GUI_Controls.CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_EXTENDED, 20)
    ef_doc_id1.TextVisible = True
    ef_doc_id1.SetFilter(GUI_Controls.CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_EXTENDED, MAX_RFC_LEN)
    ef_doc_id2.TextVisible = True
    ef_doc_id2.SetFilter(GUI_Controls.CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_EXTENDED, MAX_CURP_LEN)
    dtp_birthdate.TextVisible = True
    dtp_birthdate.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
    dtp_birthdate.SetRange(_min_date, _max_date)
    dtp_wedding.TextVisible = True
    dtp_wedding.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
    dtp_wedding.SetRange(_min_date, _max_date)

    cb_doc_type.TextVisible = True
    cb_gender.TextVisible = True
    cb_marital_status.TextVisible = True
    cb_occupation.TextVisible = True
    cb_is_vip.TextVisible = True

    btn_document.Type = uc_button.ENUM_BUTTON_TYPE.USER
    btn_document.Height = BUTTON_DOCUMENT_HEIGHT
    btn_document.Width = BUTTON_DOCUMENT_WIDTH
    cb_birthplace.TextVisible = True
    cb_nationality.TextVisible = True
    cb_birthplace.AutoCompleteMode = True

    'tab_contact
    ef_phone_1.TextVisible = True
    ef_phone_1.SetFilter(GUI_Controls.CLASS_FILTER.ENUM_FORMAT.FORMAT_PHONE_NUMBER, 20)
    ef_phone_2.TextVisible = True
    ef_phone_2.SetFilter(GUI_Controls.CLASS_FILTER.ENUM_FORMAT.FORMAT_PHONE_NUMBER, 20)
    ef_email_1.TextVisible = True
    ef_email_1.SetFilter(GUI_Controls.CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)
    ef_email_2.TextVisible = True
    ef_email_2.SetFilter(GUI_Controls.CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)
    ef_twitter.TextVisible = True
    ef_twitter.SetFilter(GUI_Controls.CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)

    'tab_address
    ef_street.TextVisible = True
    ef_street.SetFilter(GUI_Controls.CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_EXTENDED, 50)
    ef_colony.TextVisible = True
    ef_colony.SetFilter(GUI_Controls.CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_EXTENDED, 100)
    ef_delegation.TextVisible = True
    ef_delegation.SetFilter(GUI_Controls.CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_EXTENDED, 50)
    ef_city.TextVisible = True
    ef_city.SetFilter(GUI_Controls.CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_EXTENDED, 50)
    ef_zip.TextVisible = True
    ef_zip.SetFilter(GUI_Controls.CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 10)
    ef_external_number.TextVisible = True
    ef_external_number.SetFilter(GUI_Controls.CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_EXTENDED, 10)
    cb_federal_entity.TextVisible = True
    cb_federal_entity.AutoCompleteMode = True
    cb_address_country.TextVisible = True
    cb_address_country.AutoCompleteMode = True

    'tab_address autoselection
    ef_as_street.TextVisible = True
    ef_as_street.SetFilter(GUI_Controls.CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_EXTENDED, 50)
    cb_as_colony.TextVisible = True
    cb_as_colony.AutoCompleteMode = True
    cb_as_delegation.TextVisible = True
    cb_as_delegation.AutoCompleteMode = True
    cb_as_city.TextVisible = True
    cb_as_city.AutoCompleteMode = True
    ef_as_zip.TextVisible = True
    ef_as_zip.SetFilter(GUI_Controls.CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 10)
    ef_as_external_number.TextVisible = True
    ef_as_external_number.SetFilter(GUI_Controls.CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_EXTENDED, 10)
    cb_as_federal_entity.TextVisible = True
    cb_as_federal_entity.AutoCompleteMode = True
    cb_as_address_country.TextVisible = True
    cb_as_address_country.AutoCompleteMode = True

    'tab_comments
    txt_comments.MaxLength = 100

    'tab_pin
    ef_new_pin.TextVisible = True
    ef_new_pin.Password = True
    ef_new_pin.SetFilter(GUI_Controls.CLASS_FILTER.ENUM_FORMAT.FORMAT_RAW_NUMBER, WSI.Common.Accounts.PIN_MAX_SAVE_LENGTH)
    ef_confirm_pin.TextVisible = True
    ef_confirm_pin.Password = True
    ef_confirm_pin.SetFilter(GUI_Controls.CLASS_FILTER.ENUM_FORMAT.FORMAT_RAW_NUMBER, WSI.Common.Accounts.PIN_MAX_SAVE_LENGTH)
    btn_save_pin.Enabled = CurrentUser.Permissions(ENUM_FORM.FORM_PLAYER_EDIT).Execute

    'tab_beneficiary
    ef_ben_name.TextVisible = True
    ef_ben_name.SetFilter(GUI_Controls.CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_BASE, 50)
    ef_ben_surname_1.TextVisible = True
    ef_ben_surname_1.SetFilter(GUI_Controls.CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_BASE, 50)
    ef_ben_surname_2.TextVisible = True
    ef_ben_surname_2.SetFilter(GUI_Controls.CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_BASE, 50)
    ef_ben_doc_id1.TextVisible = True
    ef_ben_doc_id1.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_EXTENDED, MAX_RFC_LEN)
    ef_ben_doc_id2.TextVisible = True
    ef_ben_doc_id2.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_EXTENDED, MAX_CURP_LEN)
    btn_ben_document.Type = uc_button.ENUM_BUTTON_TYPE.USER
    btn_ben_document.Height = BUTTON_DOCUMENT_HEIGHT
    btn_ben_document.Width = BUTTON_DOCUMENT_WIDTH
    dtp_ben_birthdate.TextVisible = True
    dtp_ben_birthdate.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
    dtp_ben_birthdate.SetRange(_min_date, _max_date)
    cb_ben_gender.TextVisible = True
    cb_ben_occupation.TextVisible = True
    cb_ben_occupation.AutoCompleteMode = True

    'Pin
    m_pin_required = WSI.Common.Accounts.DoPinRequestFlagsExist(False)

    _kiosk_enabled = GeneralParam.GetBoolean("WigosKiosk", "Enabled")

    If Not m_pin_required AndAlso Not _kiosk_enabled Then
      tab_container.TabPages.Remove(tab_pin)
    End If


    chk_show_comments.Visible = (GLB_GuiMode = public_globals.ENUM_GUI.WIGOS_GUI)

  End Sub ' SetFormatControls

  ' PURPOSE: Load data on combos
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS: 
  Private Sub LoadDataCombos()
    'Id param must be the exact value from database

    RemoveHandlerEvents()

    ' Gender
    cb_gender.Add(1, WSI.Common.CardData.GenderString(1))
    cb_gender.Add(2, WSI.Common.CardData.GenderString(2))
    cb_gender.SelectedIndex = -1

    ' VIP
    cb_is_vip.Add(0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(279))
    cb_is_vip.Add(1, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278))
    cb_is_vip.SelectedIndex = 1

    'Marital Status
    cb_marital_status.Add(1, WSI.Common.CardData.MaritalStatusString(ACCOUNT_MARITAL_STATUS.SINGLE))
    cb_marital_status.Add(2, WSI.Common.CardData.MaritalStatusString(ACCOUNT_MARITAL_STATUS.MARRIED))
    cb_marital_status.Add(3, WSI.Common.CardData.MaritalStatusString(ACCOUNT_MARITAL_STATUS.DIVORCED))
    cb_marital_status.Add(4, WSI.Common.CardData.MaritalStatusString(ACCOUNT_MARITAL_STATUS.WIDOWED))
    cb_marital_status.Add(5, WSI.Common.CardData.MaritalStatusString(ACCOUNT_MARITAL_STATUS.UNMARRIED))
    cb_marital_status.Add(8, WSI.Common.CardData.MaritalStatusString(ACCOUNT_MARITAL_STATUS.ANNULED))
    cb_marital_status.Add(6, WSI.Common.CardData.MaritalStatusString(ACCOUNT_MARITAL_STATUS.OTHER))
    cb_marital_status.Add(7, WSI.Common.CardData.MaritalStatusString(ACCOUNT_MARITAL_STATUS.UNSPECIFIED))

    cb_marital_status.SelectedIndex = -1

    'Document Id Type
    cb_doc_type.Add(IdentificationTypes.GetIdentificationTypes(True))
    cb_doc_type.SelectedIndex = -1

    'Occupation
    cb_occupation.Add(-1, String.Empty)
    cb_occupation.Add(WSI.Common.CardData.GetOccupationsList())
    cb_occupation.SelectedIndex = -1

    cb_ben_occupation.Add(-1, String.Empty)
    cb_ben_occupation.Add(WSI.Common.CardData.GetOccupationsList())
    cb_ben_occupation.SelectedIndex = -1

    'Birthplace
    cb_birthplace.Add(-1, String.Empty)
    cb_birthplace.Add(WSI.Common.CardData.GetCountriesList())
    cb_birthplace.SelectedIndex = -1

    'Nationaliy
    cb_nationality.Add(-1, String.Empty)
    cb_nationality.Add(WSI.Common.CardData.GetNationalitiesList())
    cb_nationality.SelectedIndex = -1

    'Address Country
    cb_address_country.Add(WSI.Common.CardData.GetCountriesList())

    'AutoSelection Address Country
    cb_as_address_country.Add(WSI.Common.CardData.GetCountriesList())

    LoadFedEntitiesCombo()

    ' Gender
    cb_ben_gender.Add(1, WSI.Common.CardData.GenderString(1))
    cb_ben_gender.Add(2, WSI.Common.CardData.GenderString(2))

    cb_ben_gender.SelectedIndex = -1

    m_tab_points_first_load = True

    AddHandlerEvent()

  End Sub ' LoadDataCombos

  Private Sub ChangeNameOrder()

    If (GeneralParam.GetBoolean("Account", "InverseNameOrder", False)) Then

      Dim ef_surname_1_position As TableLayoutPanelCellPosition = tlp_principal.GetPositionFromControl(ef_surname_1)
      Dim ef_name_position As TableLayoutPanelCellPosition = tlp_principal.GetPositionFromControl(ef_name)

      Dim ef_surname_2_position As TableLayoutPanelCellPosition = tlp_principal.GetPositionFromControl(ef_surname_2)
      Dim ef_middle_name_position As TableLayoutPanelCellPosition = tlp_principal.GetPositionFromControl(ef_middle_name)

      tlp_principal.SetCellPosition(ef_name, ef_surname_1_position)
      tlp_principal.SetCellPosition(ef_surname_1, ef_name_position)

      tlp_principal.SetCellPosition(ef_middle_name, ef_surname_2_position)
      tlp_principal.SetCellPosition(ef_surname_2, ef_middle_name_position)

      'Change tab order:
      Dim _old_tab_index As Integer

      _old_tab_index = ef_name.TabIndex
      ef_name.TabIndex = ef_surname_1.TabIndex
      ef_surname_1.TabIndex = _old_tab_index

      _old_tab_index = ef_middle_name.TabIndex
      ef_middle_name.TabIndex = ef_surname_2.TabIndex
      ef_surname_2.TabIndex = _old_tab_index

    End If

  End Sub 'ChangeNameOrder

  Private Sub VisibleData()

    Dim _visible_data As VisibleData

    '#If DEBUG Then
    '    WSI.Common.GeneralParam.ReloadGP()
    '#End If

    _visible_data = New VisibleData()
    _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.NAME3, ToList(ef_name))
    _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.NAME4, ToList(ef_middle_name))
    _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.NAME1, ToList(ef_surname_1))
    _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.NAME2, ToList(ef_surname_2))

    _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.ID1, ToList(ef_doc_id1))
    _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.ID2, ToList(ef_doc_id2))
    _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.BIRTHDATE, ToList(dtp_birthdate))
    _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.DOC_SCAN, ToList(btn_document))
    _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.GENDER, ToList(cb_gender))
    _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.NATIONALITY, ToList(cb_nationality))
    _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.BIRTH_COUNTRY, ToList(cb_birthplace))
    _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.MARITAL_STATUS, ToList(cb_marital_status))
    _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.WEDDING_DATE, ToList(dtp_wedding))
    _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.OCCUPATION, ToList(cb_occupation))

    _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.PHONE1, ToList(ef_phone_1))
    _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.PHONE2, ToList(ef_phone_2))
    _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.EMAIL1, ToList(ef_email_1))
    _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.EMAIL2, ToList(ef_email_2))
    _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.TWITTER, ToList(ef_twitter))

    _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.STREET, ToList(ef_street))
    _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.EXTERNAL_NUMBER, ToList(ef_external_number))
    _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.COLONY, ToList(ef_colony))
    _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.DELEGATION, ToList(ef_delegation))
    _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.CITY, ToList(ef_city))
    _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.ZIP, ToList(ef_zip))
    _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.ADDRESS_COUNTRY, ToList(cb_address_country))
    _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.FEDERAL_ENTITY, ToList(cb_federal_entity))

    _visible_data.HideControls()

    If Not WSI.Common.Misc.HasBeneficiaryAndNotAMLExternalControl() Then
      tab_container.TabPages.Remove(tab_beneficiary)
    End If

    If Not WSI.Common.VisibleData.IsContactVisible() Then
      tab_container.TabPages.Remove(tab_contact)
    End If

    If Not WSI.Common.VisibleData.IsAddressVisible() Then
      tab_container.TabPages.Remove(tab_address)
    End If

    If ef_surname_1.Visible And Not ef_surname_2.Visible Then
      ef_surname_1.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5099)
    End If

    If Not WSI.Common.GeneralParam.GetBoolean("Account.VisibleField", "Phone1", True) _
      Or Not WSI.Common.GeneralParam.GetBoolean("Account.VisibleField", "Phone2", True) Then
      If WSI.Common.GeneralParam.GetBoolean("Account.VisibleField", "Phone1", True) Then
        ef_phone_1.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5161)
      End If
      If WSI.Common.GeneralParam.GetBoolean("Account.VisibleField", "Phone2", True) Then
        ef_phone_2.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5161)
      End If
    End If

    If Not WSI.Common.GeneralParam.GetBoolean("Account.VisibleField", "Email1", True) _
      Or Not WSI.Common.GeneralParam.GetBoolean("Account.VisibleField", "Email2", True) Then
      If WSI.Common.GeneralParam.GetBoolean("Account.VisibleField", "Email1", True) Then
        ef_email_1.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5162)
      End If
      If WSI.Common.GeneralParam.GetBoolean("Account.VisibleField", "Email2", True) Then
        ef_email_2.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5162)
      End If
    End If

    If Not WSI.Common.VisibleData.IsAllAddressVisible Then
      chk_FreeMode.Enabled = False
    End If

  End Sub

  Private Function ToList(ByVal ParamArray Controls() As Control) As Control()
    Return Controls
  End Function

  ' PURPOSE: Select an item in combo
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS: 
  Private Sub SelectItemCombo(ByVal Type As Int32, ByVal Combo As uc_combo)

    Dim _idf As Integer

    For _idf = 0 To Combo.Count() - 1

      If Combo.Value(_idf) = Type Then
        Combo.SelectedIndex = _idf

        Return
      End If

    Next

    Combo.SelectedIndex = -1

  End Sub ' AddItemCombo

  ' PURPOSE: Click event for PIN saving
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub SavePinClick() Handles btn_save_pin.ClickEvent

    If Not ef_new_pin.ValidateFormat() Then
      Call SetControlFocus(ENUM_CARDDATA_FIELD.PIN, PLAYER_DATA_TO_CHECK.PIN, "")
      NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", ef_new_pin.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return
    End If

    If Not ef_confirm_pin.ValidateFormat() Then
      Call SetControlFocus(ENUM_CARDDATA_FIELD.PIN, PLAYER_DATA_TO_CHECK.PIN, "")
      NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", ef_confirm_pin.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return
    End If

    If ef_new_pin.TextValue.Length < WSI.Common.Accounts.PIN_MIN_LENGTH Then
      Call SetControlFocus(ENUM_CARDDATA_FIELD.PIN, PLAYER_DATA_TO_CHECK.PIN, "")
      NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_CHANGE_PIN_LENGTH_ERROR", Accounts.PIN_MIN_LENGTH), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return
    End If

    If ef_new_pin.TextValue <> ef_confirm_pin.TextValue Then
      Call SetControlFocus(ENUM_CARDDATA_FIELD.PIN, PLAYER_DATA_TO_CHECK.PIN, "")
      NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_CONFIRMATION_PIN_DIFFERS"), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return
    End If

    If m_player_data.SavePin(ef_confirm_pin.TextValue) Then
      m_empty_pin = False
      gb_pin.Enabled = False
      NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_PIN_CHANGED_CORRECTLY"), ENUM_MB_TYPE.MB_TYPE_INFO)
    Else
      gb_pin.Enabled = True
      NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_ERROR_UPDATING_PIN"), ENUM_MB_TYPE.MB_TYPE_ERROR)
    End If

  End Sub ' SavePinClick

  ' PURPOSE: Set Address for combobox autoselection
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS: 
  Private Sub SetAddressAutoselection()
    Dim _address As Zip.Address

    _address = Nothing

    Me.Cursor = Cursors.WaitCursor

    RemoveHandlerEvents()

    cb_as_federal_entity.Clear()
    cb_as_delegation.Clear()
    cb_as_colony.Clear()
    cb_as_city.Clear()

    FillStructAddress(m_address_modified)

    'Is a anonimous player
    'If not String.IsNullOrEmpty(m_player_data.CardData.PlayerTracking.HolderName) Then

    _address.CountryId = m_address_modified.CountryId
    Call SelectItemCombo(m_address_modified.CountryId, cb_as_address_country)

    'ZIP
    If Not Zip.ValidationZip(_address.CountryId, m_address_modified.Zip) Then
      ef_as_zip.BackColor = Color.Red
      ef_as_zip.ForeColor = Color.Transparent
    Else
      ef_as_zip.BackColor = Color.Transparent
      ef_as_zip.ForeColor = Color.Black
    End If
    _address.Zip = m_address_modified.Zip
    ef_as_zip.TextValue = m_address_modified.Zip

    cb_as_delegation.Enabled = False
    cb_as_colony.Enabled = False
    cb_as_city.Enabled = False

    If Not String.IsNullOrEmpty(ef_as_zip.TextValue) Then

      'Federal entity
      If ManageComboboxAutoselection(cb_as_federal_entity, Zip.CASE_FEDERAL_ENTITY, _address, m_address_modified.FederalEntity) Then
        _address.FederalEntity = cb_as_federal_entity.TextValue

        'Delegation
        If ManageComboboxAutoselection(cb_as_delegation, Zip.CASE_DELEGATION, _address, m_address_modified.Delegation) Then
          _address.Delegation = m_address_modified.Delegation

          'Colony
          If ManageComboboxAutoselection(cb_as_colony, Zip.CASE_COLONY, _address, m_address_modified.Colony) Then
            _address.Colony = m_address_modified.Colony

            'City
            If ManageComboboxAutoselection(cb_as_city, Zip.CASE_CITY, _address, m_address_modified.City) Then
              _address.City = m_address_modified.City
            End If
          End If
        End If
      End If
    Else

      cb_as_federal_entity.BackColor = Color.Red
      cb_as_federal_entity.ForeColor = Color.Transparent
      cb_as_delegation.BackColor = Color.Red
      cb_as_delegation.ForeColor = Color.Transparent
      cb_as_colony.BackColor = Color.Red
      cb_as_colony.ForeColor = Color.Transparent
      cb_as_city.BackColor = Color.Red
      cb_as_city.ForeColor = Color.Transparent
    End If

    ' Street
    ef_as_street.TextValue = m_address_modified.Street

    ' External number
    ef_as_external_number.TextValue = m_address_modified.ExtNum

    'End If

    Me.Cursor = Cursors.Default

    AddHandlerEvent()

  End Sub ' SetAddressAutoselection

  ' PURPOSE: Fill combos autoselection
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS: 
  Private Function ManageComboboxAutoselection(ByRef Combobox As uc_combo, ByVal ParameterFilter As String, ByVal Address As Zip.Address, ByVal ValueCombo As String) As Boolean

    If String.IsNullOrEmpty(ValueCombo) Then
      ValueCombo = ""
    End If

    If FillComboboxAutoselection(Combobox, Address, ParameterFilter) Then
      If Not SelectTextCombo(ValueCombo, Combobox) Then

        Combobox.DataSource.Rows.Add(-1, ValueCombo)

        'If Not ValueCombo = Nothing Then
        'Combobox.Add(ValueCombo)
        'End If

        Combobox.SelectedIndex = Combobox.Count - 1
        Combobox.BackColor = Color.Red
        Combobox.ForeColor = Color.Transparent

        Return False
      Else
        Combobox.Enabled = Combobox.Count > GetNumRowsToCompare(Combobox)
        Combobox.BackColor = Color.Transparent
        Combobox.ForeColor = Color.Black
      End If
    Else
      'If Not ValueCombo = Nothing Then
      Combobox.DataSource.Rows.Add(-1, ValueCombo)
      'End If
      Combobox.SelectedIndex = Combobox.Count - 1
      If Not (String.IsNullOrEmpty(ValueCombo)) Then
        Combobox.BackColor = Color.Red
        Combobox.ForeColor = Color.Transparent
        Combobox.Enabled = Combobox.Count > GetNumRowsToCompare(Combobox)

        Return False
      End If
    End If

    Return True
  End Function ' ManageComboboxAutoselection

  Private Function GetNumRowsToCompare(ByVal Combo As uc_combo) As Integer
    Dim _num As Integer
    Dim _rows As DataRow()

    _num = 0
    _rows = Combo.DataSource.Select("ID >= 0")
    If _rows.Length >= 1 Then
      _num += 1
    End If

    _rows = Combo.DataSource.Select("ID < 0")
    If _rows.Length >= 1 Then
      _num += 1
    End If

    Return _num
  End Function

  ' PURPOSE: AddHandler to Events
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS: 
  Private Sub AddHandlerEvent()

    AddHandler cb_as_address_country.ValueChangedEvent, AddressOf cb_as_address_country_ValueChangedEvent
    AddHandler ef_as_zip.EntryFieldValueChanged, AddressOf ef_as_zip_EntryFieldValueChanged
    AddHandler cb_as_federal_entity.ValueChangedEvent, AddressOf cb_as_federal_entity_ValueChangedEvent
    AddHandler cb_as_delegation.ValueChangedEvent, AddressOf cb_as_delegation_ValueChangedEvent
    AddHandler cb_as_colony.ValueChangedEvent, AddressOf cb_as_colony_ValueChangedEvent
    AddHandler cb_as_city.ValueChangedEvent, AddressOf cb_as_city_ValueChangedEvent
    AddHandler cb_address_country.ValueChangedEvent, AddressOf cb_address_country_ValueChangedEvent

  End Sub ' AddHandlerEvent
  ' PURPOSE: RmoveHandler to Events
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS: 
  Private Sub RemoveHandlerEvents()

    RemoveHandler cb_as_address_country.ValueChangedEvent, AddressOf cb_as_address_country_ValueChangedEvent
    RemoveHandler ef_as_zip.EntryFieldValueChanged, AddressOf ef_as_zip_EntryFieldValueChanged
    RemoveHandler cb_as_federal_entity.ValueChangedEvent, AddressOf cb_as_federal_entity_ValueChangedEvent
    RemoveHandler cb_as_delegation.ValueChangedEvent, AddressOf cb_as_delegation_ValueChangedEvent
    RemoveHandler cb_as_colony.ValueChangedEvent, AddressOf cb_as_colony_ValueChangedEvent
    RemoveHandler cb_as_city.ValueChangedEvent, AddressOf cb_as_city_ValueChangedEvent
    RemoveHandler cb_address_country.ValueChangedEvent, AddressOf cb_address_country_ValueChangedEvent

  End Sub ' RemoveHandlerEvents


  ' PURPOSE: Fill combobox autoselection
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS: 
  Private Function FillComboboxAutoselection(ByRef Combobox As uc_combo, ByRef Address As Zip.Address, ByVal ParameterFilter As String, Optional ByVal ForceZipFilter As Boolean = False) As Boolean
    Dim _dt As DataTable

    Try

      _dt = Zip.GetDataTableToAddress(ParameterFilter, Zip.GetFilterToAddress(Address, ForceZipFilter))

      If Not _dt.Rows.Count = 0 Then
        Combobox.Enabled = True
        Combobox.Clear()
        Combobox.Add(_dt, COLUMN_ID, COLUMN_NAME)

        Return True
      Else
        Combobox.Enabled = False

        Return False
      End If

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

  End Function ' FillComboboxAutoselection

  ' PURPOSE: Select an text in combo
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS: 
  Private Function SelectTextCombo(ByVal Type As String, ByVal Combo As uc_combo) As Boolean

    Dim _idf As Integer

    For _idf = 0 To Combo.Count() - 1

      If Combo.TextValue(_idf) = Type Then
        Combo.SelectedIndex = _idf

        Return True
      End If

    Next

    Return False
  End Function ' SelectTextCombo

  ' PURPOSE: FillStructAddress
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub FillStructAddress(ByRef address As Zip.Address)

    address.Street = ef_street.TextValue
    address.Colony = ef_colony.TextValue
    address.Delegation = ef_delegation.TextValue
    address.City = ef_city.TextValue
    address.Zip = ef_zip.TextValue
    address.ExtNum = ef_external_number.TextValue
    address.FederalEntity = cb_federal_entity.TextValue
    address.CountryId = cb_address_country.Value

  End Sub ' FillStructAddress

  ' PURPOSE: Replicate address
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Private Sub ReplicateAddress()

    ef_street.TextValue = m_address_modified.Street
    ef_colony.TextValue = m_address_modified.Colony
    ef_delegation.TextValue = m_address_modified.Delegation
    ef_city.TextValue = m_address_modified.City
    ef_zip.TextValue = m_address_modified.Zip
    ef_external_number.TextValue = m_address_modified.ExtNum
    Call SelectTextCombo(m_address_modified.FederalEntity, cb_federal_entity)
    Call SelectItemCombo(m_address_modified.CountryId, cb_address_country)

  End Sub ' ReplicateAddress

  ' PURPOSE: Load Federal entities combo
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub LoadFedEntitiesCombo()
    Dim _dt As DataTable
    Dim _name As String = Nothing
    Dim _adj As String = Nothing
    Dim _iso2 As String = Nothing

    _dt = New DataTable

    Countries.GetCountryInfo(cb_address_country.Value, _name, _adj, _iso2)

    States.GetStates(_dt, _iso2)

    cb_federal_entity.Clear()

    cb_federal_entity.Add(_dt)

  End Sub ' LoadFedEntitiesCombo

  Private Sub chk_ben_holder_data_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chk_ben_holder_data.CheckedChanged

    If chk_ben_holder_data.Checked Then

      Call EnableAndFillBeneficiaryData()
    Else

      Call DisableAndCleanBeneficiaryData()
    End If

  End Sub

  Private Sub btn_document_ClickEvent() Handles btn_document.ClickEvent
    Dim _frm As frm_viewer_digitalized_documents

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_viewer_digitalized_documents
    _frm.SetPlayerData = m_player_data.CardData
    _frm.AccountScannedOwner = ACCOUNT_SCANNED_OWNER.HOLDER
    _frm.Display(True)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub btn_ben_document_ClickEvent() Handles btn_ben_document.ClickEvent

    Dim _frm As frm_viewer_digitalized_documents

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_viewer_digitalized_documents
    _frm.SetPlayerData = m_player_data.CardData
    _frm.AccountScannedOwner = ACCOUNT_SCANNED_OWNER.BENEFICIARY
    _frm.Display(True)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub tab_container_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tab_container.SelectedIndexChanged
    If tab_container.TabPages(tab_container.SelectedIndex).Name = tab_points_program.Name And m_tab_points_first_load Then
      'Points program
      lbl_level_value.Text = LoyaltyProgram.LevelName(m_player_data.CardData.PlayerTracking.CardLevel)
      lbl_points.Text = m_player_data.CardData.PlayerTracking.TruncatedPoints.ToString()
      lbl_points_sufix.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2612)
      lbl_level_entered.Text = IIf(m_player_data.CardData.PlayerTracking.HolderLevelEntered = DateTime.MinValue, "---", GUI_FormatDate(m_player_data.CardData.PlayerTracking.HolderLevelEntered, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS))
      lbl_level_expiration.Text = IIf(m_player_data.CardData.PlayerTracking.HolderLevelExpiration = DateTime.MinValue, "---", GUI_FormatDate(m_player_data.CardData.PlayerTracking.HolderLevelExpiration, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS))

      Call GetNextLevelInfo()
      m_tab_points_first_load = False
    End If
  End Sub

  Private Sub FormActivated(ByVal sender As System.Object, ByVal e As System.EventArgs)
    KeyboardMessageFilter.RegisterHandler(Me, BarcodeType.FilterTrackData, AddressOf TrackDataEvent)
  End Sub

  Private Sub FormDeactivated(ByVal sender As System.Object, ByVal e As System.EventArgs)
    KeyboardMessageFilter.UnregisterHandler(Me, AddressOf TrackDataEvent)
  End Sub

  Private Sub uc_account_sel_VisibleChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.VisibleChanged
    If m_parent Is Nothing Then
      m_parent = Me.ParentForm
      KeyboardMessageFilter.Init()
      AddHandler m_parent.Activated, AddressOf Me.FormActivated
      AddHandler m_parent.Deactivate, AddressOf Me.FormDeactivated
    End If
  End Sub

  Private Sub TrackDataEvent(ByVal e As KbdMsgEvent, ByVal Data As Object)
    Try
      If e <> KbdMsgEvent.EventTrackData Then
        Return
      End If
      If Not tab_container.SelectedTab.Equals(tab_contact) Then
        Return
      End If

      ef_twitter.TextValue = CStr(Data)

    Catch
    End Try
  End Sub

  Private Sub chk_FreeMode_CheckedChanged(sender As Object, e As EventArgs) Handles chk_FreeMode.CheckedChanged

    RemoveHandlerEvents()

    If chk_FreeMode.Checked Then
      ReplicateAddress()
    Else

      FillStructAddress(m_address_modified)
      SetAddressAutoselection()

    End If

    tlp_AutoSelection.Visible = Not chk_FreeMode.Checked
    tlp_dir.Visible = chk_FreeMode.Checked

    AddHandlerEvent()
  End Sub

  Private Sub ef_as_external_number_EntryFieldValueChanged() Handles ef_as_external_number.EntryFieldValueChanged

    m_address_modified.ExtNum = ef_as_external_number.TextValue

  End Sub

  Private Sub ef_as_street_EntryFieldValueChanged() Handles ef_as_street.EntryFieldValueChanged

    m_address_modified.Street = ef_as_street.TextValue

  End Sub

  Private Sub cb_as_address_country_ValueChangedEvent() 'Handles cb_as_address_country.ValueChangedEvent

    m_address_modified.CountryId = cb_as_address_country.Value

    Call ef_as_zip_EntryFieldValueChanged()

    cb_address_country.Value = cb_as_address_country.Value

    LoadFedEntitiesCombo()

  End Sub

  Private Sub ef_as_zip_EntryFieldValueChanged() 'Handles ef_as_zip.EntryFieldValueChanged
    Dim _address_copy As Zip.Address
    _address_copy = m_address_modified

    RemoveHandlerEvents()

    cb_as_federal_entity.Clear()
    cb_as_delegation.Clear()
    cb_as_delegation.Enabled = False
    cb_as_colony.Clear()
    cb_as_colony.Enabled = False
    cb_as_city.Clear()
    cb_as_city.Enabled = False

    m_address_modified = _address_copy
    m_address_modified.Zip = ef_as_zip.TextValue

    _address_copy = m_address_modified
    _address_copy.FederalEntity = Nothing
    _address_copy.Delegation = Nothing
    _address_copy.Colony = Nothing
    _address_copy.City = Nothing

    If (String.IsNullOrEmpty(ef_as_zip.TextValue) OrElse _
       Not Zip.ValidationZip(m_address_modified.CountryId, m_address_modified.Zip)) Then
      cb_as_federal_entity.Enabled = False
      ef_as_zip.BackColor = Color.Red
      ef_as_zip.ForeColor = Color.Transparent

    Else
      ef_as_zip.BackColor = Color.Transparent
      ef_as_zip.ForeColor = Color.Black

      If Not FillComboboxAutoselection(cb_as_federal_entity, _address_copy, Zip.CASE_FEDERAL_ENTITY) Then
        cb_as_federal_entity.Enabled = False
        ef_as_zip.BackColor = Color.Red
        ef_as_zip.ForeColor = Color.Transparent
      End If

      If ManageComboboxAutoselection(cb_as_federal_entity, Zip.CASE_FEDERAL_ENTITY, _address_copy, m_address_modified.FederalEntity) Then
        cb_as_federal_entity.Enabled = cb_as_federal_entity.Count > GetNumRowsToCompare(cb_as_federal_entity)
        cb_as_federal_entity_ValueChangedEvent()
      End If
    End If

    RemoveHandlerEvents()
    AddHandlerEvent()

  End Sub

  Private Sub cb_as_federal_entity_ValueChangedEvent() 'Handles cb_as_federal_entity.ValueChangedEvent
    Dim _address_copy As Zip.Address
    _address_copy = m_address_modified

    RemoveHandlerEvents()

    cb_as_delegation.Clear()
    cb_as_colony.Clear()
    cb_as_colony.Enabled = False
    cb_as_city.Clear()
    cb_as_city.Enabled = False

    m_address_modified = _address_copy
    m_address_modified.FederalEntity = cb_as_federal_entity.TextValue

    _address_copy = m_address_modified
    _address_copy.Delegation = Nothing
    _address_copy.Colony = Nothing
    _address_copy.City = Nothing

    'If Not String.IsNullOrEmpty(m_address_modified.FederalEntity) Then

    '  cb_as_delegation.TextCombo = m_address_modified.Delegation
    '  cb_as_colony.TextCombo = m_address_modified.Colony
    '  cb_as_city.TextCombo = m_address_modified.City
    '  cb_as_federal_entity.Enabled = cb_as_federal_entity.Count > 1
    '  cb_as_federal_entity.BackColor = Color.Transparent
    '  cb_as_federal_entity.ForeColor = Color.Black

    'If delegation is more than 0 is federal entity selection is OK
    RemoveHandlerEvents()
    If ManageComboboxAutoselection(cb_as_delegation, Zip.CASE_DELEGATION, _address_copy, m_address_modified.Delegation) Then
      cb_as_delegation_ValueChangedEvent()
    End If

    If cb_as_delegation.Count > 0 Then
      RemoveHandlerEvents()
      cb_as_federal_entity.Enabled = False
      cb_as_federal_entity.BackColor = Color.Transparent
      cb_as_federal_entity.ForeColor = Color.Black
    End If
    'End If

    RemoveHandlerEvents()
    AddHandlerEvent()
  End Sub

  Private Sub cb_as_delegation_ValueChangedEvent() 'Handles cb_as_delegation.ValueChangedEvent
    Dim _address_copy As Zip.Address
    _address_copy = m_address_modified

    RemoveHandlerEvents()

    cb_as_colony.Clear()
    cb_as_city.Clear()
    cb_as_city.Enabled = False

    m_address_modified = _address_copy
    m_address_modified.Delegation = cb_as_delegation.TextValue

    _address_copy = m_address_modified
    _address_copy.Colony = Nothing
    _address_copy.City = Nothing

    If Not String.IsNullOrEmpty(m_address_modified.Delegation) Then

      cb_as_colony.TextCombo = m_address_modified.Colony
      cb_as_city.TextCombo = m_address_modified.City
      cb_as_delegation.Enabled = cb_as_delegation.Count > GetNumRowsToCompare(cb_as_delegation)
      cb_as_delegation.BackColor = Color.Transparent
      cb_as_delegation.ForeColor = Color.Black

      'If colony is more than 0 is delegation selection is OK
      RemoveHandlerEvents()
      If ManageComboboxAutoselection(cb_as_colony, Zip.CASE_COLONY, _address_copy, m_address_modified.Colony) Then
        cb_as_colony_ValueChangedEvent()
      End If
    End If

    RemoveHandlerEvents()
    AddHandlerEvent()

  End Sub

  Private Sub cb_as_colony_ValueChangedEvent() 'Handles cb_as_colony.ValueChangedEvent
    Dim _address_copy As Zip.Address
    _address_copy = m_address_modified

    RemoveHandlerEvents()

    cb_as_city.Clear()

    m_address_modified = _address_copy
    m_address_modified.Colony = cb_as_colony.TextValue

    _address_copy = m_address_modified
    _address_copy.City = Nothing

    If Not String.IsNullOrEmpty(m_address_modified.Colony) Then
      cb_as_city.TextCombo = m_address_modified.City

      cb_as_colony.Enabled = cb_as_colony.Count > GetNumRowsToCompare(cb_as_colony)
      cb_as_colony.BackColor = Color.Transparent
      cb_as_colony.ForeColor = Color.Black

      'If city is more than 0 is colony selection is OK
      RemoveHandlerEvents()
      If ManageComboboxAutoselection(cb_as_city, Zip.CASE_CITY, _address_copy, m_address_modified.City) Then
        cb_as_city_ValueChangedEvent()
      End If
    End If

    RemoveHandlerEvents()
    AddHandlerEvent()
  End Sub

  Private Sub cb_as_city_ValueChangedEvent() 'Handles cb_as_city.ValueChangedEvent

    m_address_modified.City = cb_as_city.TextValue

    RemoveHandlerEvents()

    ManageComboboxAutoselection(cb_as_city, Zip.CASE_CITY, m_address_modified, m_address_modified.City)

    RemoveHandlerEvents()
    AddHandlerEvent()

  End Sub

  Private Sub cb_address_country_ValueChangedEvent() ' Handles cb_address_country.ValueChangedEvent
    LoadFedEntitiesCombo()
  End Sub

#End Region

  Private Sub cb_doc_type_ValueChangedEvent() Handles cb_doc_type.ValueChangedEvent
    Dim _val As String = ""
    If Not m_player_data Is Nothing Then

      If m_player_data.Doctypes.ContainsKey(cb_doc_type.Value) Then
        _val = m_player_data.Doctypes(cb_doc_type.Value)
      End If
      For Each dataRow As DataRow In cb_doc_type.DataSource.Rows
        Dim txt As String = dataRow(1)
        Dim idx = txt.IndexOf(" (", StringComparison.Ordinal)
        If idx > 0 Then
          txt = txt.Substring(0, idx)
        End If
        If Not dataRow(0) = cb_doc_type.Value Then
          If m_player_data.Doctypes.ContainsKey(dataRow(0)) Then
            txt = txt + " (" + m_player_data.Doctypes(dataRow(0)) + ")"
          End If
        End If
        DataRow(1) = txt
      Next

      ef_doc_id.TextValue = _val
    End If
  End Sub

  Private Sub ef_doc_id_Leave(sender As Object, e As EventArgs) Handles ef_doc_id.Leave
    If Not m_player_data Is Nothing Then
      If m_player_data.Doctypes.ContainsKey(cb_doc_type.Value) Then
        m_player_data.Doctypes(cb_doc_type.Value) = ef_doc_id.TextValue
      Else
        m_player_data.Doctypes.Add(cb_doc_type.Value, ef_doc_id.TextValue)

      End If
    End If
  End Sub
End Class

