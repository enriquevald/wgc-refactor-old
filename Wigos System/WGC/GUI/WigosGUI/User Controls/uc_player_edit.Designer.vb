<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uc_player_edit
  Inherits System.Windows.Forms.UserControl

  'UserControl overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing AndAlso components IsNot Nothing Then
      components.Dispose()
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(uc_player_edit))
    Me.tab_container = New System.Windows.Forms.TabControl()
    Me.tab_main = New System.Windows.Forms.TabPage()
    Me.gb_main = New System.Windows.Forms.GroupBox()
    Me.tlp_principal = New System.Windows.Forms.TableLayoutPanel()
    Me.ef_middle_name = New GUI_Controls.uc_entry_field()
    Me.ef_name = New GUI_Controls.uc_entry_field()
    Me.ef_surname_2 = New GUI_Controls.uc_entry_field()
    Me.ef_surname_1 = New GUI_Controls.uc_entry_field()
    Me.tlp_prin_left = New System.Windows.Forms.TableLayoutPanel()
    Me.cb_is_vip = New GUI_Controls.uc_combo()
    Me.ef_doc_id1 = New GUI_Controls.uc_entry_field()
    Me.cb_doc_type = New GUI_Controls.uc_combo()
    Me.ef_doc_id = New GUI_Controls.uc_entry_field()
    Me.dtp_birthdate = New GUI_Controls.uc_date_picker()
    Me.ef_doc_id2 = New GUI_Controls.uc_entry_field()
    Me.tlp_prin_right = New System.Windows.Forms.TableLayoutPanel()
    Me.dtp_wedding = New GUI_Controls.uc_date_picker()
    Me.cb_birthplace = New GUI_Controls.uc_combo()
    Me.cb_nationality = New GUI_Controls.uc_combo()
    Me.cb_gender = New GUI_Controls.uc_combo()
    Me.cb_marital_status = New GUI_Controls.uc_combo()
    Me.btn_document = New GUI_Controls.uc_button()
    Me.cb_occupation = New GUI_Controls.uc_combo()
    Me.tab_contact = New System.Windows.Forms.TabPage()
    Me.gb_contact = New System.Windows.Forms.GroupBox()
    Me.tlp_contact = New System.Windows.Forms.TableLayoutPanel()
    Me.ef_phone_1 = New GUI_Controls.uc_entry_field()
    Me.ef_twitter = New GUI_Controls.uc_entry_field()
    Me.ef_phone_2 = New GUI_Controls.uc_entry_field()
    Me.ef_email_2 = New GUI_Controls.uc_entry_field()
    Me.ef_email_1 = New GUI_Controls.uc_entry_field()
    Me.tab_address = New System.Windows.Forms.TabPage()
    Me.gb_address = New System.Windows.Forms.GroupBox()
    Me.lbl_info = New System.Windows.Forms.Label()
    Me.chk_FreeMode = New System.Windows.Forms.CheckBox()
    Me.tlp_dir = New System.Windows.Forms.TableLayoutPanel()
    Me.cb_address_country = New GUI_Controls.uc_combo()
    Me.ef_street = New GUI_Controls.uc_entry_field()
    Me.cb_federal_entity = New GUI_Controls.uc_combo()
    Me.ef_external_number = New GUI_Controls.uc_entry_field()
    Me.ef_zip = New GUI_Controls.uc_entry_field()
    Me.ef_colony = New GUI_Controls.uc_entry_field()
    Me.ef_city = New GUI_Controls.uc_entry_field()
    Me.ef_delegation = New GUI_Controls.uc_entry_field()
    Me.tlp_AutoSelection = New System.Windows.Forms.TableLayoutPanel()
    Me.cb_as_colony = New GUI_Controls.uc_combo()
    Me.cb_as_delegation = New GUI_Controls.uc_combo()
    Me.cb_as_city = New GUI_Controls.uc_combo()
    Me.cb_as_address_country = New GUI_Controls.uc_combo()
    Me.ef_as_street = New GUI_Controls.uc_entry_field()
    Me.cb_as_federal_entity = New GUI_Controls.uc_combo()
    Me.ef_as_external_number = New GUI_Controls.uc_entry_field()
    Me.ef_as_zip = New GUI_Controls.uc_entry_field()
    Me.tab_beneficiary = New System.Windows.Forms.TabPage()
    Me.gb_beneficiary = New System.Windows.Forms.GroupBox()
    Me.tlp_beneficiary = New System.Windows.Forms.TableLayoutPanel()
    Me.cb_ben_occupation = New GUI_Controls.uc_combo()
    Me.ef_ben_name = New GUI_Controls.uc_entry_field()
    Me.ef_ben_surname_1 = New GUI_Controls.uc_entry_field()
    Me.tlp_ben_left = New System.Windows.Forms.TableLayoutPanel()
    Me.ef_ben_doc_id1 = New GUI_Controls.uc_entry_field()
    Me.ef_ben_doc_id2 = New GUI_Controls.uc_entry_field()
    Me.dtp_ben_birthdate = New GUI_Controls.uc_date_picker()
    Me.ef_ben_surname_2 = New GUI_Controls.uc_entry_field()
    Me.tlp_ben_right = New System.Windows.Forms.TableLayoutPanel()
    Me.btn_ben_document = New GUI_Controls.uc_button()
    Me.cb_ben_gender = New GUI_Controls.uc_combo()
    Me.chk_ben_holder_data = New System.Windows.Forms.CheckBox()
    Me.tab_points_program = New System.Windows.Forms.TabPage()
    Me.lbl_points_sufix = New System.Windows.Forms.Label()
    Me.gb_next_level = New System.Windows.Forms.GroupBox()
    Me.lbl_points_last_days = New System.Windows.Forms.Label()
    Me.lbl_points_discretionaries_prefix = New System.Windows.Forms.Label()
    Me.lbl_points_discretionaries = New System.Windows.Forms.Label()
    Me.lbl_points_generated_prefix = New System.Windows.Forms.Label()
    Me.lbl_points_generated = New System.Windows.Forms.Label()
    Me.lbl_points_to_next_level = New System.Windows.Forms.Label()
    Me.lbl_points_to_next_level_prefix = New System.Windows.Forms.Label()
    Me.lbl_points_level_prefix = New System.Windows.Forms.Label()
    Me.lbl_points_level = New System.Windows.Forms.Label()
    Me.lbl_points_req_prefix = New System.Windows.Forms.Label()
    Me.lbl_points_req = New System.Windows.Forms.Label()
    Me.lbl_next_level_name = New System.Windows.Forms.Label()
    Me.lbl_points = New System.Windows.Forms.Label()
    Me.lbl_points_to_maintain = New System.Windows.Forms.Label()
    Me.lbl_level_expiration = New System.Windows.Forms.Label()
    Me.lbl_level_entered = New System.Windows.Forms.Label()
    Me.lbl_level_value = New System.Windows.Forms.Label()
    Me.lbl_balance = New System.Windows.Forms.Label()
    Me.lbl_points_to_keep_level = New System.Windows.Forms.Label()
    Me.lbl_expiration_date = New System.Windows.Forms.Label()
    Me.lbl_entry_date = New System.Windows.Forms.Label()
    Me.lbl_level = New System.Windows.Forms.Label()
    Me.tab_block = New System.Windows.Forms.TabPage()
    Me.gb_block = New System.Windows.Forms.GroupBox()
    Me.txt_block_description = New System.Windows.Forms.TextBox()
    Me.lbl_block_description = New System.Windows.Forms.Label()
    Me.lbl_blocked = New System.Windows.Forms.Label()
    Me.tab_comments = New System.Windows.Forms.TabPage()
    Me.gb_extra_info = New System.Windows.Forms.GroupBox()
    Me.chk_show_comments = New System.Windows.Forms.CheckBox()
    Me.txt_comments = New System.Windows.Forms.TextBox()
    Me.tab_pin = New System.Windows.Forms.TabPage()
    Me.gb_pin = New System.Windows.Forms.GroupBox()
    Me.btn_save_pin = New GUI_Controls.uc_button()
    Me.ef_new_pin = New GUI_Controls.uc_entry_field()
    Me.ef_confirm_pin = New GUI_Controls.uc_entry_field()
    Me.TabPage1 = New System.Windows.Forms.TabPage()
    Me.GroupBox1 = New System.Windows.Forms.GroupBox()
    Me.Uc_entry_field3 = New GUI_Controls.uc_entry_field()
    Me.Uc_date_picker1 = New GUI_Controls.uc_date_picker()
    Me.Uc_date_picker2 = New GUI_Controls.uc_date_picker()
    Me.Uc_combo1 = New GUI_Controls.uc_combo()
    Me.Uc_entry_field4 = New GUI_Controls.uc_entry_field()
    Me.Uc_combo2 = New GUI_Controls.uc_combo()
    Me.Uc_combo3 = New GUI_Controls.uc_combo()
    Me.Uc_entry_field5 = New GUI_Controls.uc_entry_field()
    Me.Uc_entry_field6 = New GUI_Controls.uc_entry_field()
    Me.Uc_entry_field7 = New GUI_Controls.uc_entry_field()
    Me.TabPage2 = New System.Windows.Forms.TabPage()
    Me.GroupBox2 = New System.Windows.Forms.GroupBox()
    Me.Uc_entry_field8 = New GUI_Controls.uc_entry_field()
    Me.Uc_entry_field9 = New GUI_Controls.uc_entry_field()
    Me.Uc_entry_field10 = New GUI_Controls.uc_entry_field()
    Me.Uc_entry_field11 = New GUI_Controls.uc_entry_field()
    Me.Uc_entry_field12 = New GUI_Controls.uc_entry_field()
    Me.TabPage3 = New System.Windows.Forms.TabPage()
    Me.GroupBox3 = New System.Windows.Forms.GroupBox()
    Me.Uc_entry_field13 = New GUI_Controls.uc_entry_field()
    Me.Uc_entry_field14 = New GUI_Controls.uc_entry_field()
    Me.Uc_entry_field15 = New GUI_Controls.uc_entry_field()
    Me.Uc_entry_field16 = New GUI_Controls.uc_entry_field()
    Me.Uc_entry_field17 = New GUI_Controls.uc_entry_field()
    Me.TabPage4 = New System.Windows.Forms.TabPage()
    Me.GroupBox4 = New System.Windows.Forms.GroupBox()
    Me.TextBox1 = New System.Windows.Forms.TextBox()
    Me.Label1 = New System.Windows.Forms.Label()
    Me.Label2 = New System.Windows.Forms.Label()
    Me.TabPage5 = New System.Windows.Forms.TabPage()
    Me.GroupBox5 = New System.Windows.Forms.GroupBox()
    Me.TextBox2 = New System.Windows.Forms.TextBox()
    Me.TabPage6 = New System.Windows.Forms.TabPage()
    Me.GroupBox6 = New System.Windows.Forms.GroupBox()
    Me.Uc_button2 = New GUI_Controls.uc_button()
    Me.Uc_entry_field18 = New GUI_Controls.uc_entry_field()
    Me.Uc_entry_field19 = New GUI_Controls.uc_entry_field()
    Me.TabPage7 = New System.Windows.Forms.TabPage()
    Me.GroupBox7 = New System.Windows.Forms.GroupBox()
    Me.Uc_entry_field20 = New GUI_Controls.uc_entry_field()
    Me.Uc_date_picker3 = New GUI_Controls.uc_date_picker()
    Me.Uc_date_picker4 = New GUI_Controls.uc_date_picker()
    Me.Uc_combo4 = New GUI_Controls.uc_combo()
    Me.Uc_entry_field21 = New GUI_Controls.uc_entry_field()
    Me.Uc_combo5 = New GUI_Controls.uc_combo()
    Me.Uc_combo6 = New GUI_Controls.uc_combo()
    Me.Uc_entry_field22 = New GUI_Controls.uc_entry_field()
    Me.Uc_entry_field23 = New GUI_Controls.uc_entry_field()
    Me.Uc_entry_field24 = New GUI_Controls.uc_entry_field()
    Me.TabPage8 = New System.Windows.Forms.TabPage()
    Me.GroupBox8 = New System.Windows.Forms.GroupBox()
    Me.Uc_entry_field25 = New GUI_Controls.uc_entry_field()
    Me.Uc_entry_field26 = New GUI_Controls.uc_entry_field()
    Me.Uc_entry_field27 = New GUI_Controls.uc_entry_field()
    Me.Uc_entry_field28 = New GUI_Controls.uc_entry_field()
    Me.Uc_entry_field29 = New GUI_Controls.uc_entry_field()
    Me.TabPage9 = New System.Windows.Forms.TabPage()
    Me.GroupBox9 = New System.Windows.Forms.GroupBox()
    Me.Uc_entry_field30 = New GUI_Controls.uc_entry_field()
    Me.Uc_entry_field31 = New GUI_Controls.uc_entry_field()
    Me.Uc_entry_field32 = New GUI_Controls.uc_entry_field()
    Me.Uc_entry_field33 = New GUI_Controls.uc_entry_field()
    Me.Uc_entry_field34 = New GUI_Controls.uc_entry_field()
    Me.TabPage10 = New System.Windows.Forms.TabPage()
    Me.GroupBox10 = New System.Windows.Forms.GroupBox()
    Me.TextBox3 = New System.Windows.Forms.TextBox()
    Me.Label3 = New System.Windows.Forms.Label()
    Me.Label4 = New System.Windows.Forms.Label()
    Me.TabPage11 = New System.Windows.Forms.TabPage()
    Me.GroupBox11 = New System.Windows.Forms.GroupBox()
    Me.TextBox4 = New System.Windows.Forms.TextBox()
    Me.TabPage12 = New System.Windows.Forms.TabPage()
    Me.GroupBox12 = New System.Windows.Forms.GroupBox()
    Me.Uc_button3 = New GUI_Controls.uc_button()
    Me.Uc_entry_field35 = New GUI_Controls.uc_entry_field()
    Me.Uc_entry_field36 = New GUI_Controls.uc_entry_field()
    Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
    Me.Uc_combo7 = New GUI_Controls.uc_combo()
    Me.Uc_entry_field37 = New GUI_Controls.uc_entry_field()
    Me.Uc_button1 = New GUI_Controls.uc_button()
    Me.Uc_entry_field1 = New GUI_Controls.uc_entry_field()
    Me.Uc_entry_field2 = New GUI_Controls.uc_entry_field()
    Me.Uc_combo8 = New GUI_Controls.uc_combo()
    Me.tab_container.SuspendLayout()
    Me.tab_main.SuspendLayout()
    Me.gb_main.SuspendLayout()
    Me.tlp_principal.SuspendLayout()
    Me.tlp_prin_left.SuspendLayout()
    Me.tlp_prin_right.SuspendLayout()
    Me.tab_contact.SuspendLayout()
    Me.gb_contact.SuspendLayout()
    Me.tlp_contact.SuspendLayout()
    Me.tab_address.SuspendLayout()
    Me.gb_address.SuspendLayout()
    Me.tlp_dir.SuspendLayout()
    Me.tlp_AutoSelection.SuspendLayout()
    Me.tab_beneficiary.SuspendLayout()
    Me.gb_beneficiary.SuspendLayout()
    Me.tlp_beneficiary.SuspendLayout()
    Me.tlp_ben_left.SuspendLayout()
    Me.tlp_ben_right.SuspendLayout()
    Me.tab_points_program.SuspendLayout()
    Me.gb_next_level.SuspendLayout()
    Me.tab_block.SuspendLayout()
    Me.gb_block.SuspendLayout()
    Me.tab_comments.SuspendLayout()
    Me.gb_extra_info.SuspendLayout()
    Me.tab_pin.SuspendLayout()
    Me.gb_pin.SuspendLayout()
    Me.TabPage1.SuspendLayout()
    Me.GroupBox1.SuspendLayout()
    Me.TabPage2.SuspendLayout()
    Me.GroupBox2.SuspendLayout()
    Me.TabPage3.SuspendLayout()
    Me.GroupBox3.SuspendLayout()
    Me.TabPage4.SuspendLayout()
    Me.GroupBox4.SuspendLayout()
    Me.TabPage5.SuspendLayout()
    Me.GroupBox5.SuspendLayout()
    Me.TabPage6.SuspendLayout()
    Me.GroupBox6.SuspendLayout()
    Me.TabPage7.SuspendLayout()
    Me.GroupBox7.SuspendLayout()
    Me.TabPage8.SuspendLayout()
    Me.GroupBox8.SuspendLayout()
    Me.TabPage9.SuspendLayout()
    Me.GroupBox9.SuspendLayout()
    Me.TabPage10.SuspendLayout()
    Me.GroupBox10.SuspendLayout()
    Me.TabPage11.SuspendLayout()
    Me.GroupBox11.SuspendLayout()
    Me.TabPage12.SuspendLayout()
    Me.GroupBox12.SuspendLayout()
    Me.TableLayoutPanel2.SuspendLayout()
    Me.SuspendLayout()
    '
    'tab_container
    '
    Me.tab_container.Controls.Add(Me.tab_main)
    Me.tab_container.Controls.Add(Me.tab_contact)
    Me.tab_container.Controls.Add(Me.tab_address)
    Me.tab_container.Controls.Add(Me.tab_beneficiary)
    Me.tab_container.Controls.Add(Me.tab_points_program)
    Me.tab_container.Controls.Add(Me.tab_block)
    Me.tab_container.Controls.Add(Me.tab_comments)
    Me.tab_container.Controls.Add(Me.tab_pin)
    Me.tab_container.Location = New System.Drawing.Point(0, 0)
    Me.tab_container.Name = "tab_container"
    Me.tab_container.SelectedIndex = 0
    Me.tab_container.Size = New System.Drawing.Size(635, 355)
    Me.tab_container.TabIndex = 0
    '
    'tab_main
    '
    Me.tab_main.Controls.Add(Me.gb_main)
    Me.tab_main.Location = New System.Drawing.Point(4, 22)
    Me.tab_main.Name = "tab_main"
    Me.tab_main.Padding = New System.Windows.Forms.Padding(3)
    Me.tab_main.Size = New System.Drawing.Size(627, 329)
    Me.tab_main.TabIndex = 0
    Me.tab_main.Text = "xPrincipal"
    Me.tab_main.UseVisualStyleBackColor = True
    '
    'gb_main
    '
    Me.gb_main.Controls.Add(Me.tlp_principal)
    Me.gb_main.Dock = System.Windows.Forms.DockStyle.Fill
    Me.gb_main.Location = New System.Drawing.Point(3, 3)
    Me.gb_main.Name = "gb_main"
    Me.gb_main.Size = New System.Drawing.Size(621, 323)
    Me.gb_main.TabIndex = 0
    Me.gb_main.TabStop = False
    '
    'tlp_principal
    '
    Me.tlp_principal.ColumnCount = 2
    Me.tlp_principal.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
    Me.tlp_principal.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
    Me.tlp_principal.Controls.Add(Me.ef_middle_name, 0, 1)
    Me.tlp_principal.Controls.Add(Me.ef_name, 0, 0)
    Me.tlp_principal.Controls.Add(Me.ef_surname_2, 0, 3)
    Me.tlp_principal.Controls.Add(Me.ef_surname_1, 0, 2)
    Me.tlp_principal.Controls.Add(Me.tlp_prin_left, 0, 4)
    Me.tlp_principal.Controls.Add(Me.tlp_prin_right, 1, 4)
    Me.tlp_principal.Controls.Add(Me.cb_occupation, 1, 5)
    Me.tlp_principal.Location = New System.Drawing.Point(3, 13)
    Me.tlp_principal.Margin = New System.Windows.Forms.Padding(2)
    Me.tlp_principal.Name = "tlp_principal"
    Me.tlp_principal.RowCount = 7
    Me.tlp_principal.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.tlp_principal.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.tlp_principal.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.tlp_principal.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.tlp_principal.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.tlp_principal.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.tlp_principal.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.tlp_principal.Size = New System.Drawing.Size(613, 306)
    Me.tlp_principal.TabIndex = 1
    '
    'ef_middle_name
    '
    Me.tlp_principal.SetColumnSpan(Me.ef_middle_name, 2)
    Me.ef_middle_name.DoubleValue = 0.0R
    Me.ef_middle_name.IntegerValue = 0
    Me.ef_middle_name.IsReadOnly = False
    Me.ef_middle_name.Location = New System.Drawing.Point(2, 30)
    Me.ef_middle_name.Margin = New System.Windows.Forms.Padding(2)
    Me.ef_middle_name.Name = "ef_middle_name"
    Me.ef_middle_name.OnlyUpperCase = True
    Me.ef_middle_name.PlaceHolder = Nothing
    Me.ef_middle_name.Size = New System.Drawing.Size(605, 24)
    Me.ef_middle_name.SufixText = "Sufix Text"
    Me.ef_middle_name.TabIndex = 1
    Me.ef_middle_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_middle_name.TextValue = ""
    Me.ef_middle_name.TextVisible = False
    Me.ef_middle_name.TextWidth = 117
    Me.ef_middle_name.Value = ""
    Me.ef_middle_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_name
    '
    Me.tlp_principal.SetColumnSpan(Me.ef_name, 2)
    Me.ef_name.DoubleValue = 0.0R
    Me.ef_name.IntegerValue = 0
    Me.ef_name.IsReadOnly = False
    Me.ef_name.Location = New System.Drawing.Point(2, 2)
    Me.ef_name.Margin = New System.Windows.Forms.Padding(2)
    Me.ef_name.Name = "ef_name"
    Me.ef_name.OnlyUpperCase = True
    Me.ef_name.PlaceHolder = Nothing
    Me.ef_name.Size = New System.Drawing.Size(605, 24)
    Me.ef_name.SufixText = "Sufix Text"
    Me.ef_name.TabIndex = 0
    Me.ef_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_name.TextValue = ""
    Me.ef_name.TextVisible = False
    Me.ef_name.TextWidth = 117
    Me.ef_name.Value = ""
    Me.ef_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_surname_2
    '
    Me.tlp_principal.SetColumnSpan(Me.ef_surname_2, 2)
    Me.ef_surname_2.DoubleValue = 0.0R
    Me.ef_surname_2.IntegerValue = 0
    Me.ef_surname_2.IsReadOnly = False
    Me.ef_surname_2.Location = New System.Drawing.Point(2, 86)
    Me.ef_surname_2.Margin = New System.Windows.Forms.Padding(2)
    Me.ef_surname_2.Name = "ef_surname_2"
    Me.ef_surname_2.OnlyUpperCase = True
    Me.ef_surname_2.PlaceHolder = Nothing
    Me.ef_surname_2.Size = New System.Drawing.Size(605, 24)
    Me.ef_surname_2.SufixText = "Sufix Text"
    Me.ef_surname_2.TabIndex = 3
    Me.ef_surname_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_surname_2.TextValue = ""
    Me.ef_surname_2.TextVisible = False
    Me.ef_surname_2.TextWidth = 117
    Me.ef_surname_2.Value = ""
    Me.ef_surname_2.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_surname_1
    '
    Me.tlp_principal.SetColumnSpan(Me.ef_surname_1, 2)
    Me.ef_surname_1.DoubleValue = 0.0R
    Me.ef_surname_1.IntegerValue = 0
    Me.ef_surname_1.IsReadOnly = False
    Me.ef_surname_1.Location = New System.Drawing.Point(2, 58)
    Me.ef_surname_1.Margin = New System.Windows.Forms.Padding(2)
    Me.ef_surname_1.Name = "ef_surname_1"
    Me.ef_surname_1.OnlyUpperCase = True
    Me.ef_surname_1.PlaceHolder = Nothing
    Me.ef_surname_1.Size = New System.Drawing.Size(605, 24)
    Me.ef_surname_1.SufixText = "Sufix Text"
    Me.ef_surname_1.TabIndex = 2
    Me.ef_surname_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_surname_1.TextValue = ""
    Me.ef_surname_1.TextVisible = False
    Me.ef_surname_1.TextWidth = 117
    Me.ef_surname_1.Value = ""
    Me.ef_surname_1.ValueForeColor = System.Drawing.Color.Blue
    '
    'tlp_prin_left
    '
    Me.tlp_prin_left.AutoSize = True
    Me.tlp_prin_left.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.tlp_prin_left.ColumnCount = 1
    Me.tlp_prin_left.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
    Me.tlp_prin_left.Controls.Add(Me.cb_is_vip, 0, 0)
    Me.tlp_prin_left.Controls.Add(Me.ef_doc_id1, 0, 4)
    Me.tlp_prin_left.Controls.Add(Me.cb_doc_type, 0, 1)
    Me.tlp_prin_left.Controls.Add(Me.ef_doc_id, 0, 2)
    Me.tlp_prin_left.Controls.Add(Me.dtp_birthdate, 0, 6)
    Me.tlp_prin_left.Controls.Add(Me.ef_doc_id2, 0, 5)
    Me.tlp_prin_left.Location = New System.Drawing.Point(0, 112)
    Me.tlp_prin_left.Margin = New System.Windows.Forms.Padding(0)
    Me.tlp_prin_left.Name = "tlp_prin_left"
    Me.tlp_prin_left.RowCount = 7
    Me.tlp_prin_left.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.tlp_prin_left.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.tlp_prin_left.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.tlp_prin_left.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.tlp_prin_left.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.tlp_prin_left.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.tlp_prin_left.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.tlp_prin_left.Size = New System.Drawing.Size(303, 168)
    Me.tlp_prin_left.TabIndex = 4
    '
    'cb_is_vip
    '
    Me.cb_is_vip.AllowUnlistedValues = False
    Me.cb_is_vip.AutoCompleteMode = False
    Me.cb_is_vip.IsReadOnly = False
    Me.cb_is_vip.Location = New System.Drawing.Point(2, 2)
    Me.cb_is_vip.Margin = New System.Windows.Forms.Padding(2)
    Me.cb_is_vip.Name = "cb_is_vip"
    Me.cb_is_vip.SelectedIndex = -1
    Me.cb_is_vip.Size = New System.Drawing.Size(183, 24)
    Me.cb_is_vip.SufixText = "Sufix Text"
    Me.cb_is_vip.TabIndex = 0
    Me.cb_is_vip.TextCombo = Nothing
    Me.cb_is_vip.TextVisible = False
    Me.cb_is_vip.TextWidth = 117
    '
    'ef_doc_id1
    '
    Me.ef_doc_id1.DoubleValue = 0.0R
    Me.ef_doc_id1.IntegerValue = 0
    Me.ef_doc_id1.IsReadOnly = False
    Me.ef_doc_id1.Location = New System.Drawing.Point(2, 86)
    Me.ef_doc_id1.Margin = New System.Windows.Forms.Padding(2)
    Me.ef_doc_id1.Name = "ef_doc_id1"
    Me.ef_doc_id1.OnlyUpperCase = True
    Me.ef_doc_id1.PlaceHolder = Nothing
    Me.ef_doc_id1.Size = New System.Drawing.Size(299, 24)
    Me.ef_doc_id1.SufixText = "Sufix Text"
    Me.ef_doc_id1.TabIndex = 3
    Me.ef_doc_id1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_doc_id1.TextValue = ""
    Me.ef_doc_id1.TextVisible = False
    Me.ef_doc_id1.TextWidth = 117
    Me.ef_doc_id1.Value = ""
    Me.ef_doc_id1.ValueForeColor = System.Drawing.Color.Blue
    '
    'cb_doc_type
    '
    Me.cb_doc_type.AllowUnlistedValues = False
    Me.cb_doc_type.AutoCompleteMode = False
    Me.cb_doc_type.IsReadOnly = False
    Me.cb_doc_type.Location = New System.Drawing.Point(2, 30)
    Me.cb_doc_type.Margin = New System.Windows.Forms.Padding(2)
    Me.cb_doc_type.Name = "cb_doc_type"
    Me.cb_doc_type.SelectedIndex = -1
    Me.cb_doc_type.Size = New System.Drawing.Size(299, 24)
    Me.cb_doc_type.SufixText = "Sufix Text"
    Me.cb_doc_type.TabIndex = 1
    Me.cb_doc_type.TextCombo = Nothing
    Me.cb_doc_type.TextVisible = False
    Me.cb_doc_type.TextWidth = 117
    '
    'ef_doc_id
    '
    Me.ef_doc_id.DoubleValue = 0.0R
    Me.ef_doc_id.IntegerValue = 0
    Me.ef_doc_id.IsReadOnly = False
    Me.ef_doc_id.Location = New System.Drawing.Point(2, 58)
    Me.ef_doc_id.Margin = New System.Windows.Forms.Padding(2)
    Me.ef_doc_id.Name = "ef_doc_id"
    Me.ef_doc_id.OnlyUpperCase = True
    Me.ef_doc_id.PlaceHolder = Nothing
    Me.ef_doc_id.Size = New System.Drawing.Size(299, 24)
    Me.ef_doc_id.SufixText = "Sufix Text"
    Me.ef_doc_id.TabIndex = 2
    Me.ef_doc_id.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_doc_id.TextValue = ""
    Me.ef_doc_id.TextVisible = False
    Me.ef_doc_id.TextWidth = 117
    Me.ef_doc_id.Value = ""
    Me.ef_doc_id.ValueForeColor = System.Drawing.Color.Blue
    '
    'dtp_birthdate
    '
    Me.dtp_birthdate.Checked = True
    Me.dtp_birthdate.IsReadOnly = False
    Me.dtp_birthdate.Location = New System.Drawing.Point(2, 142)
    Me.dtp_birthdate.Margin = New System.Windows.Forms.Padding(2)
    Me.dtp_birthdate.Name = "dtp_birthdate"
    Me.dtp_birthdate.ShowCheckBox = False
    Me.dtp_birthdate.ShowUpDown = False
    Me.dtp_birthdate.Size = New System.Drawing.Size(299, 24)
    Me.dtp_birthdate.SufixText = "Sufix Text"
    Me.dtp_birthdate.TabIndex = 5
    Me.dtp_birthdate.TextVisible = False
    Me.dtp_birthdate.TextWidth = 117
    Me.dtp_birthdate.Value = New Date(2013, 3, 1, 0, 0, 0, 0)
    '
    'ef_doc_id2
    '
    Me.ef_doc_id2.DoubleValue = 0.0R
    Me.ef_doc_id2.IntegerValue = 0
    Me.ef_doc_id2.IsReadOnly = False
    Me.ef_doc_id2.Location = New System.Drawing.Point(2, 114)
    Me.ef_doc_id2.Margin = New System.Windows.Forms.Padding(2)
    Me.ef_doc_id2.Name = "ef_doc_id2"
    Me.ef_doc_id2.OnlyUpperCase = True
    Me.ef_doc_id2.PlaceHolder = Nothing
    Me.ef_doc_id2.Size = New System.Drawing.Size(299, 24)
    Me.ef_doc_id2.SufixText = "Sufix Text"
    Me.ef_doc_id2.TabIndex = 4
    Me.ef_doc_id2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_doc_id2.TextValue = ""
    Me.ef_doc_id2.TextVisible = False
    Me.ef_doc_id2.TextWidth = 117
    Me.ef_doc_id2.Value = ""
    Me.ef_doc_id2.ValueForeColor = System.Drawing.Color.Blue
    '
    'tlp_prin_right
    '
    Me.tlp_prin_right.AutoSize = True
    Me.tlp_prin_right.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.tlp_prin_right.ColumnCount = 1
    Me.tlp_prin_right.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
    Me.tlp_prin_right.Controls.Add(Me.dtp_wedding, 0, 6)
    Me.tlp_prin_right.Controls.Add(Me.cb_birthplace, 0, 4)
    Me.tlp_prin_right.Controls.Add(Me.cb_nationality, 0, 2)
    Me.tlp_prin_right.Controls.Add(Me.cb_gender, 0, 1)
    Me.tlp_prin_right.Controls.Add(Me.cb_marital_status, 0, 5)
    Me.tlp_prin_right.Controls.Add(Me.btn_document, 0, 0)
    Me.tlp_prin_right.Location = New System.Drawing.Point(306, 112)
    Me.tlp_prin_right.Margin = New System.Windows.Forms.Padding(0)
    Me.tlp_prin_right.Name = "tlp_prin_right"
    Me.tlp_prin_right.RowCount = 7
    Me.tlp_prin_right.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.tlp_prin_right.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.tlp_prin_right.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.tlp_prin_right.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.tlp_prin_right.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.tlp_prin_right.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.tlp_prin_right.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.tlp_prin_right.Size = New System.Drawing.Size(303, 174)
    Me.tlp_prin_right.TabIndex = 5
    '
    'dtp_wedding
    '
    Me.dtp_wedding.Checked = True
    Me.dtp_wedding.IsReadOnly = False
    Me.dtp_wedding.Location = New System.Drawing.Point(2, 148)
    Me.dtp_wedding.Margin = New System.Windows.Forms.Padding(2)
    Me.dtp_wedding.Name = "dtp_wedding"
    Me.dtp_wedding.ShowCheckBox = True
    Me.dtp_wedding.ShowUpDown = False
    Me.dtp_wedding.Size = New System.Drawing.Size(299, 24)
    Me.dtp_wedding.SufixText = "Sufix Text"
    Me.dtp_wedding.TabIndex = 4
    Me.dtp_wedding.TextVisible = False
    Me.dtp_wedding.TextWidth = 117
    Me.dtp_wedding.Value = New Date(2013, 3, 1, 0, 0, 0, 0)
    '
    'cb_birthplace
    '
    Me.cb_birthplace.AllowUnlistedValues = False
    Me.cb_birthplace.AutoCompleteMode = False
    Me.cb_birthplace.IsReadOnly = False
    Me.cb_birthplace.Location = New System.Drawing.Point(2, 92)
    Me.cb_birthplace.Margin = New System.Windows.Forms.Padding(2)
    Me.cb_birthplace.Name = "cb_birthplace"
    Me.cb_birthplace.SelectedIndex = -1
    Me.cb_birthplace.Size = New System.Drawing.Size(299, 24)
    Me.cb_birthplace.SufixText = "Sufix Text"
    Me.cb_birthplace.TabIndex = 2
    Me.cb_birthplace.TextCombo = Nothing
    Me.cb_birthplace.TextVisible = False
    Me.cb_birthplace.TextWidth = 117
    '
    'cb_nationality
    '
    Me.cb_nationality.AllowUnlistedValues = False
    Me.cb_nationality.AutoCompleteMode = False
    Me.cb_nationality.IsReadOnly = False
    Me.cb_nationality.Location = New System.Drawing.Point(2, 64)
    Me.cb_nationality.Margin = New System.Windows.Forms.Padding(2)
    Me.cb_nationality.Name = "cb_nationality"
    Me.cb_nationality.SelectedIndex = -1
    Me.cb_nationality.Size = New System.Drawing.Size(299, 24)
    Me.cb_nationality.SufixText = "Sufix Text"
    Me.cb_nationality.TabIndex = 1
    Me.cb_nationality.TextCombo = Nothing
    Me.cb_nationality.TextVisible = False
    Me.cb_nationality.TextWidth = 117
    '
    'cb_gender
    '
    Me.cb_gender.AllowUnlistedValues = False
    Me.cb_gender.AutoCompleteMode = False
    Me.cb_gender.IsReadOnly = False
    Me.cb_gender.Location = New System.Drawing.Point(2, 36)
    Me.cb_gender.Margin = New System.Windows.Forms.Padding(2)
    Me.cb_gender.Name = "cb_gender"
    Me.cb_gender.SelectedIndex = -1
    Me.cb_gender.Size = New System.Drawing.Size(299, 24)
    Me.cb_gender.SufixText = "Sufix Text"
    Me.cb_gender.TabIndex = 0
    Me.cb_gender.TextCombo = Nothing
    Me.cb_gender.TextVisible = False
    Me.cb_gender.TextWidth = 117
    '
    'cb_marital_status
    '
    Me.cb_marital_status.AllowUnlistedValues = False
    Me.cb_marital_status.AutoCompleteMode = False
    Me.cb_marital_status.IsReadOnly = False
    Me.cb_marital_status.Location = New System.Drawing.Point(2, 120)
    Me.cb_marital_status.Margin = New System.Windows.Forms.Padding(2)
    Me.cb_marital_status.Name = "cb_marital_status"
    Me.cb_marital_status.SelectedIndex = -1
    Me.cb_marital_status.Size = New System.Drawing.Size(299, 24)
    Me.cb_marital_status.SufixText = "Sufix Text"
    Me.cb_marital_status.TabIndex = 3
    Me.cb_marital_status.TextCombo = Nothing
    Me.cb_marital_status.TextVisible = False
    Me.cb_marital_status.TextWidth = 117
    '
    'btn_document
    '
    Me.btn_document.Anchor = System.Windows.Forms.AnchorStyles.Right
    Me.btn_document.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_document.Location = New System.Drawing.Point(209, 2)
    Me.btn_document.Margin = New System.Windows.Forms.Padding(2, 2, 4, 2)
    Me.btn_document.Name = "btn_document"
    Me.btn_document.Size = New System.Drawing.Size(90, 30)
    Me.btn_document.TabIndex = 0
    Me.btn_document.ToolTipped = False
    Me.btn_document.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.USER
    '
    'cb_occupation
    '
    Me.cb_occupation.AllowUnlistedValues = False
    Me.cb_occupation.AutoCompleteMode = False
    Me.tlp_principal.SetColumnSpan(Me.cb_occupation, 2)
    Me.cb_occupation.IsReadOnly = False
    Me.cb_occupation.Location = New System.Drawing.Point(0, 286)
    Me.cb_occupation.Margin = New System.Windows.Forms.Padding(0)
    Me.cb_occupation.Name = "cb_occupation"
    Me.cb_occupation.Padding = New System.Windows.Forms.Padding(1, 0, 0, 0)
    Me.cb_occupation.SelectedIndex = -1
    Me.cb_occupation.Size = New System.Drawing.Size(607, 24)
    Me.cb_occupation.SufixText = "Sufix Text"
    Me.cb_occupation.TabIndex = 6
    Me.cb_occupation.TextCombo = Nothing
    Me.cb_occupation.TextVisible = False
    Me.cb_occupation.TextWidth = 117
    '
    'tab_contact
    '
    Me.tab_contact.Controls.Add(Me.gb_contact)
    Me.tab_contact.Location = New System.Drawing.Point(4, 22)
    Me.tab_contact.Name = "tab_contact"
    Me.tab_contact.Padding = New System.Windows.Forms.Padding(3)
    Me.tab_contact.Size = New System.Drawing.Size(627, 329)
    Me.tab_contact.TabIndex = 1
    Me.tab_contact.Text = "xContacto"
    Me.tab_contact.UseVisualStyleBackColor = True
    '
    'gb_contact
    '
    Me.gb_contact.Controls.Add(Me.tlp_contact)
    Me.gb_contact.Dock = System.Windows.Forms.DockStyle.Fill
    Me.gb_contact.Location = New System.Drawing.Point(3, 3)
    Me.gb_contact.Name = "gb_contact"
    Me.gb_contact.Size = New System.Drawing.Size(621, 323)
    Me.gb_contact.TabIndex = 1
    Me.gb_contact.TabStop = False
    '
    'tlp_contact
    '
    Me.tlp_contact.ColumnCount = 1
    Me.tlp_contact.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
    Me.tlp_contact.Controls.Add(Me.ef_phone_1, 0, 0)
    Me.tlp_contact.Controls.Add(Me.ef_twitter, 0, 4)
    Me.tlp_contact.Controls.Add(Me.ef_phone_2, 0, 1)
    Me.tlp_contact.Controls.Add(Me.ef_email_2, 0, 3)
    Me.tlp_contact.Controls.Add(Me.ef_email_1, 0, 2)
    Me.tlp_contact.Location = New System.Drawing.Point(3, 13)
    Me.tlp_contact.Name = "tlp_contact"
    Me.tlp_contact.RowCount = 6
    Me.tlp_contact.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.tlp_contact.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.tlp_contact.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.tlp_contact.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.tlp_contact.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.tlp_contact.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
    Me.tlp_contact.Size = New System.Drawing.Size(613, 147)
    Me.tlp_contact.TabIndex = 5
    '
    'ef_phone_1
    '
    Me.ef_phone_1.DoubleValue = 0.0R
    Me.ef_phone_1.IntegerValue = 0
    Me.ef_phone_1.IsReadOnly = False
    Me.ef_phone_1.Location = New System.Drawing.Point(2, 2)
    Me.ef_phone_1.Margin = New System.Windows.Forms.Padding(2)
    Me.ef_phone_1.Name = "ef_phone_1"
    Me.ef_phone_1.PlaceHolder = Nothing
    Me.ef_phone_1.Size = New System.Drawing.Size(605, 24)
    Me.ef_phone_1.SufixText = "Sufix Text"
    Me.ef_phone_1.TabIndex = 0
    Me.ef_phone_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_phone_1.TextValue = ""
    Me.ef_phone_1.TextVisible = False
    Me.ef_phone_1.TextWidth = 117
    Me.ef_phone_1.Value = ""
    Me.ef_phone_1.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_twitter
    '
    Me.ef_twitter.DoubleValue = 0.0R
    Me.ef_twitter.IntegerValue = 0
    Me.ef_twitter.IsReadOnly = False
    Me.ef_twitter.Location = New System.Drawing.Point(2, 114)
    Me.ef_twitter.Margin = New System.Windows.Forms.Padding(2)
    Me.ef_twitter.Name = "ef_twitter"
    Me.ef_twitter.PlaceHolder = Nothing
    Me.ef_twitter.Size = New System.Drawing.Size(605, 24)
    Me.ef_twitter.SufixText = "Sufix Text"
    Me.ef_twitter.TabIndex = 4
    Me.ef_twitter.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_twitter.TextValue = ""
    Me.ef_twitter.TextVisible = False
    Me.ef_twitter.TextWidth = 117
    Me.ef_twitter.Value = ""
    Me.ef_twitter.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_phone_2
    '
    Me.ef_phone_2.DoubleValue = 0.0R
    Me.ef_phone_2.IntegerValue = 0
    Me.ef_phone_2.IsReadOnly = False
    Me.ef_phone_2.Location = New System.Drawing.Point(2, 30)
    Me.ef_phone_2.Margin = New System.Windows.Forms.Padding(2)
    Me.ef_phone_2.Name = "ef_phone_2"
    Me.ef_phone_2.PlaceHolder = Nothing
    Me.ef_phone_2.Size = New System.Drawing.Size(605, 24)
    Me.ef_phone_2.SufixText = "Sufix Text"
    Me.ef_phone_2.TabIndex = 1
    Me.ef_phone_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_phone_2.TextValue = ""
    Me.ef_phone_2.TextVisible = False
    Me.ef_phone_2.TextWidth = 117
    Me.ef_phone_2.Value = ""
    Me.ef_phone_2.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_email_2
    '
    Me.ef_email_2.DoubleValue = 0.0R
    Me.ef_email_2.IntegerValue = 0
    Me.ef_email_2.IsReadOnly = False
    Me.ef_email_2.Location = New System.Drawing.Point(2, 86)
    Me.ef_email_2.Margin = New System.Windows.Forms.Padding(2)
    Me.ef_email_2.Name = "ef_email_2"
    Me.ef_email_2.PlaceHolder = Nothing
    Me.ef_email_2.Size = New System.Drawing.Size(605, 24)
    Me.ef_email_2.SufixText = "Sufix Text"
    Me.ef_email_2.TabIndex = 3
    Me.ef_email_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_email_2.TextValue = ""
    Me.ef_email_2.TextVisible = False
    Me.ef_email_2.TextWidth = 117
    Me.ef_email_2.Value = ""
    Me.ef_email_2.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_email_1
    '
    Me.ef_email_1.DoubleValue = 0.0R
    Me.ef_email_1.IntegerValue = 0
    Me.ef_email_1.IsReadOnly = False
    Me.ef_email_1.Location = New System.Drawing.Point(2, 58)
    Me.ef_email_1.Margin = New System.Windows.Forms.Padding(2)
    Me.ef_email_1.Name = "ef_email_1"
    Me.ef_email_1.PlaceHolder = Nothing
    Me.ef_email_1.Size = New System.Drawing.Size(605, 24)
    Me.ef_email_1.SufixText = "Sufix Text"
    Me.ef_email_1.TabIndex = 2
    Me.ef_email_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_email_1.TextValue = ""
    Me.ef_email_1.TextVisible = False
    Me.ef_email_1.TextWidth = 117
    Me.ef_email_1.Value = ""
    Me.ef_email_1.ValueForeColor = System.Drawing.Color.Blue
    '
    'tab_address
    '
    Me.tab_address.Controls.Add(Me.gb_address)
    Me.tab_address.Location = New System.Drawing.Point(4, 22)
    Me.tab_address.Name = "tab_address"
    Me.tab_address.Padding = New System.Windows.Forms.Padding(3)
    Me.tab_address.Size = New System.Drawing.Size(627, 329)
    Me.tab_address.TabIndex = 2
    Me.tab_address.Text = "xDirecci�n"
    Me.tab_address.UseVisualStyleBackColor = True
    '
    'gb_address
    '
    Me.gb_address.Controls.Add(Me.lbl_info)
    Me.gb_address.Controls.Add(Me.chk_FreeMode)
    Me.gb_address.Controls.Add(Me.tlp_dir)
    Me.gb_address.Controls.Add(Me.tlp_AutoSelection)
    Me.gb_address.Dock = System.Windows.Forms.DockStyle.Fill
    Me.gb_address.Location = New System.Drawing.Point(3, 3)
    Me.gb_address.Name = "gb_address"
    Me.gb_address.Size = New System.Drawing.Size(621, 323)
    Me.gb_address.TabIndex = 2
    Me.gb_address.TabStop = False
    '
    'lbl_info
    '
    Me.lbl_info.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.lbl_info.Location = New System.Drawing.Point(188, 298)
    Me.lbl_info.Name = "lbl_info"
    Me.lbl_info.RightToLeft = System.Windows.Forms.RightToLeft.Yes
    Me.lbl_info.Size = New System.Drawing.Size(427, 17)
    Me.lbl_info.TabIndex = 10
    Me.lbl_info.Text = "xInfo"
    Me.lbl_info.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'chk_FreeMode
    '
    Me.chk_FreeMode.AutoSize = True
    Me.chk_FreeMode.Checked = True
    Me.chk_FreeMode.CheckState = System.Windows.Forms.CheckState.Checked
    Me.chk_FreeMode.Location = New System.Drawing.Point(10, 298)
    Me.chk_FreeMode.Name = "chk_FreeMode"
    Me.chk_FreeMode.Size = New System.Drawing.Size(88, 17)
    Me.chk_FreeMode.TabIndex = 8
    Me.chk_FreeMode.Text = "xFreeMode"
    Me.chk_FreeMode.UseVisualStyleBackColor = True
    '
    'tlp_dir
    '
    Me.tlp_dir.ColumnCount = 2
    Me.tlp_dir.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
    Me.tlp_dir.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
    Me.tlp_dir.Controls.Add(Me.cb_address_country, 0, 6)
    Me.tlp_dir.Controls.Add(Me.ef_street, 0, 0)
    Me.tlp_dir.Controls.Add(Me.cb_federal_entity, 1, 6)
    Me.tlp_dir.Controls.Add(Me.ef_external_number, 0, 1)
    Me.tlp_dir.Controls.Add(Me.ef_zip, 0, 2)
    Me.tlp_dir.Controls.Add(Me.ef_colony, 0, 3)
    Me.tlp_dir.Controls.Add(Me.ef_city, 0, 5)
    Me.tlp_dir.Controls.Add(Me.ef_delegation, 0, 4)
    Me.tlp_dir.Location = New System.Drawing.Point(3, 13)
    Me.tlp_dir.Name = "tlp_dir"
    Me.tlp_dir.RowCount = 8
    Me.tlp_dir.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.tlp_dir.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.tlp_dir.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.tlp_dir.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.tlp_dir.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.tlp_dir.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.tlp_dir.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.tlp_dir.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.tlp_dir.Size = New System.Drawing.Size(613, 225)
    Me.tlp_dir.TabIndex = 7
    '
    'cb_address_country
    '
    Me.cb_address_country.AllowUnlistedValues = False
    Me.cb_address_country.AutoCompleteMode = False
    Me.cb_address_country.IsReadOnly = False
    Me.cb_address_country.Location = New System.Drawing.Point(2, 170)
    Me.cb_address_country.Margin = New System.Windows.Forms.Padding(2)
    Me.cb_address_country.Name = "cb_address_country"
    Me.cb_address_country.SelectedIndex = -1
    Me.cb_address_country.Size = New System.Drawing.Size(299, 24)
    Me.cb_address_country.SufixText = "Sufix Text"
    Me.cb_address_country.TabIndex = 6
    Me.cb_address_country.TextCombo = Nothing
    Me.cb_address_country.TextVisible = False
    Me.cb_address_country.TextWidth = 117
    '
    'ef_street
    '
    Me.tlp_dir.SetColumnSpan(Me.ef_street, 2)
    Me.ef_street.DoubleValue = 0.0R
    Me.ef_street.IntegerValue = 0
    Me.ef_street.IsReadOnly = False
    Me.ef_street.Location = New System.Drawing.Point(2, 2)
    Me.ef_street.Margin = New System.Windows.Forms.Padding(2)
    Me.ef_street.Name = "ef_street"
    Me.ef_street.PlaceHolder = Nothing
    Me.ef_street.Size = New System.Drawing.Size(605, 24)
    Me.ef_street.SufixText = "Sufix Text"
    Me.ef_street.TabIndex = 0
    Me.ef_street.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_street.TextValue = ""
    Me.ef_street.TextVisible = False
    Me.ef_street.TextWidth = 117
    Me.ef_street.Value = ""
    Me.ef_street.ValueForeColor = System.Drawing.Color.Blue
    '
    'cb_federal_entity
    '
    Me.cb_federal_entity.AllowUnlistedValues = False
    Me.cb_federal_entity.AutoCompleteMode = False
    Me.cb_federal_entity.IsReadOnly = False
    Me.cb_federal_entity.Location = New System.Drawing.Point(308, 170)
    Me.cb_federal_entity.Margin = New System.Windows.Forms.Padding(2)
    Me.cb_federal_entity.Name = "cb_federal_entity"
    Me.cb_federal_entity.SelectedIndex = -1
    Me.cb_federal_entity.Size = New System.Drawing.Size(299, 24)
    Me.cb_federal_entity.SufixText = "Sufix Text"
    Me.cb_federal_entity.TabIndex = 7
    Me.cb_federal_entity.TextCombo = Nothing
    Me.cb_federal_entity.TextVisible = False
    Me.cb_federal_entity.TextWidth = 117
    '
    'ef_external_number
    '
    Me.tlp_dir.SetColumnSpan(Me.ef_external_number, 2)
    Me.ef_external_number.DoubleValue = 0.0R
    Me.ef_external_number.IntegerValue = 0
    Me.ef_external_number.IsReadOnly = False
    Me.ef_external_number.Location = New System.Drawing.Point(2, 30)
    Me.ef_external_number.Margin = New System.Windows.Forms.Padding(2)
    Me.ef_external_number.Name = "ef_external_number"
    Me.ef_external_number.PlaceHolder = Nothing
    Me.ef_external_number.Size = New System.Drawing.Size(299, 24)
    Me.ef_external_number.SufixText = "Sufix Text"
    Me.ef_external_number.TabIndex = 1
    Me.ef_external_number.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_external_number.TextValue = ""
    Me.ef_external_number.TextVisible = False
    Me.ef_external_number.TextWidth = 117
    Me.ef_external_number.Value = ""
    Me.ef_external_number.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_zip
    '
    Me.ef_zip.DoubleValue = 0.0R
    Me.ef_zip.IntegerValue = 0
    Me.ef_zip.IsReadOnly = False
    Me.ef_zip.Location = New System.Drawing.Point(2, 58)
    Me.ef_zip.Margin = New System.Windows.Forms.Padding(2)
    Me.ef_zip.Name = "ef_zip"
    Me.ef_zip.PlaceHolder = Nothing
    Me.ef_zip.Size = New System.Drawing.Size(299, 24)
    Me.ef_zip.SufixText = "Sufix Text"
    Me.ef_zip.TabIndex = 2
    Me.ef_zip.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_zip.TextValue = ""
    Me.ef_zip.TextVisible = False
    Me.ef_zip.TextWidth = 117
    Me.ef_zip.Value = ""
    Me.ef_zip.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_colony
    '
    Me.tlp_dir.SetColumnSpan(Me.ef_colony, 2)
    Me.ef_colony.DoubleValue = 0.0R
    Me.ef_colony.IntegerValue = 0
    Me.ef_colony.IsReadOnly = False
    Me.ef_colony.Location = New System.Drawing.Point(2, 86)
    Me.ef_colony.Margin = New System.Windows.Forms.Padding(2)
    Me.ef_colony.Name = "ef_colony"
    Me.ef_colony.PlaceHolder = Nothing
    Me.ef_colony.Size = New System.Drawing.Size(605, 24)
    Me.ef_colony.SufixText = "Sufix Text"
    Me.ef_colony.TabIndex = 3
    Me.ef_colony.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_colony.TextValue = ""
    Me.ef_colony.TextVisible = False
    Me.ef_colony.TextWidth = 117
    Me.ef_colony.Value = ""
    Me.ef_colony.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_city
    '
    Me.tlp_dir.SetColumnSpan(Me.ef_city, 2)
    Me.ef_city.DoubleValue = 0.0R
    Me.ef_city.IntegerValue = 0
    Me.ef_city.IsReadOnly = False
    Me.ef_city.Location = New System.Drawing.Point(2, 142)
    Me.ef_city.Margin = New System.Windows.Forms.Padding(2)
    Me.ef_city.Name = "ef_city"
    Me.ef_city.PlaceHolder = Nothing
    Me.ef_city.Size = New System.Drawing.Size(605, 24)
    Me.ef_city.SufixText = "Sufix Text"
    Me.ef_city.TabIndex = 5
    Me.ef_city.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_city.TextValue = ""
    Me.ef_city.TextVisible = False
    Me.ef_city.TextWidth = 117
    Me.ef_city.Value = ""
    Me.ef_city.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_delegation
    '
    Me.tlp_dir.SetColumnSpan(Me.ef_delegation, 2)
    Me.ef_delegation.DoubleValue = 0.0R
    Me.ef_delegation.IntegerValue = 0
    Me.ef_delegation.IsReadOnly = False
    Me.ef_delegation.Location = New System.Drawing.Point(2, 114)
    Me.ef_delegation.Margin = New System.Windows.Forms.Padding(2)
    Me.ef_delegation.Name = "ef_delegation"
    Me.ef_delegation.PlaceHolder = Nothing
    Me.ef_delegation.Size = New System.Drawing.Size(605, 24)
    Me.ef_delegation.SufixText = "Sufix Text"
    Me.ef_delegation.TabIndex = 4
    Me.ef_delegation.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_delegation.TextValue = ""
    Me.ef_delegation.TextVisible = False
    Me.ef_delegation.TextWidth = 117
    Me.ef_delegation.Value = ""
    Me.ef_delegation.ValueForeColor = System.Drawing.Color.Blue
    '
    'tlp_AutoSelection
    '
    Me.tlp_AutoSelection.ColumnCount = 2
    Me.tlp_AutoSelection.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
    Me.tlp_AutoSelection.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
    Me.tlp_AutoSelection.Controls.Add(Me.cb_as_colony, 0, 3)
    Me.tlp_AutoSelection.Controls.Add(Me.cb_as_delegation, 0, 4)
    Me.tlp_AutoSelection.Controls.Add(Me.cb_as_city, 0, 5)
    Me.tlp_AutoSelection.Controls.Add(Me.cb_as_address_country, 0, 6)
    Me.tlp_AutoSelection.Controls.Add(Me.ef_as_street, 0, 0)
    Me.tlp_AutoSelection.Controls.Add(Me.cb_as_federal_entity, 1, 6)
    Me.tlp_AutoSelection.Controls.Add(Me.ef_as_external_number, 0, 1)
    Me.tlp_AutoSelection.Controls.Add(Me.ef_as_zip, 0, 2)
    Me.tlp_AutoSelection.Location = New System.Drawing.Point(3, 13)
    Me.tlp_AutoSelection.Name = "tlp_AutoSelection"
    Me.tlp_AutoSelection.RowCount = 8
    Me.tlp_AutoSelection.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.tlp_AutoSelection.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.tlp_AutoSelection.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.tlp_AutoSelection.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.tlp_AutoSelection.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.tlp_AutoSelection.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.tlp_AutoSelection.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.tlp_AutoSelection.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.tlp_AutoSelection.Size = New System.Drawing.Size(613, 225)
    Me.tlp_AutoSelection.TabIndex = 9
    Me.tlp_AutoSelection.Visible = False
    '
    'cb_as_colony
    '
    Me.cb_as_colony.AllowUnlistedValues = False
    Me.cb_as_colony.AutoCompleteMode = False
    Me.tlp_AutoSelection.SetColumnSpan(Me.cb_as_colony, 2)
    Me.cb_as_colony.Enabled = False
    Me.cb_as_colony.IsReadOnly = False
    Me.cb_as_colony.Location = New System.Drawing.Point(2, 86)
    Me.cb_as_colony.Margin = New System.Windows.Forms.Padding(2)
    Me.cb_as_colony.Name = "cb_as_colony"
    Me.cb_as_colony.SelectedIndex = -1
    Me.cb_as_colony.Size = New System.Drawing.Size(605, 24)
    Me.cb_as_colony.SufixText = "Sufix Text"
    Me.cb_as_colony.TabIndex = 10
    Me.cb_as_colony.TextCombo = Nothing
    Me.cb_as_colony.TextVisible = False
    Me.cb_as_colony.TextWidth = 117
    '
    'cb_as_delegation
    '
    Me.cb_as_delegation.AllowUnlistedValues = False
    Me.cb_as_delegation.AutoCompleteMode = False
    Me.tlp_AutoSelection.SetColumnSpan(Me.cb_as_delegation, 2)
    Me.cb_as_delegation.Enabled = False
    Me.cb_as_delegation.IsReadOnly = False
    Me.cb_as_delegation.Location = New System.Drawing.Point(2, 114)
    Me.cb_as_delegation.Margin = New System.Windows.Forms.Padding(2)
    Me.cb_as_delegation.Name = "cb_as_delegation"
    Me.cb_as_delegation.SelectedIndex = -1
    Me.cb_as_delegation.Size = New System.Drawing.Size(605, 24)
    Me.cb_as_delegation.SufixText = "Sufix Text"
    Me.cb_as_delegation.TabIndex = 9
    Me.cb_as_delegation.TextCombo = Nothing
    Me.cb_as_delegation.TextVisible = False
    Me.cb_as_delegation.TextWidth = 117
    '
    'cb_as_city
    '
    Me.cb_as_city.AllowUnlistedValues = True
    Me.cb_as_city.AutoCompleteMode = False
    Me.tlp_AutoSelection.SetColumnSpan(Me.cb_as_city, 2)
    Me.cb_as_city.Enabled = False
    Me.cb_as_city.IsReadOnly = False
    Me.cb_as_city.Location = New System.Drawing.Point(2, 142)
    Me.cb_as_city.Margin = New System.Windows.Forms.Padding(2)
    Me.cb_as_city.Name = "cb_as_city"
    Me.cb_as_city.SelectedIndex = -1
    Me.cb_as_city.Size = New System.Drawing.Size(605, 24)
    Me.cb_as_city.SufixText = "Sufix Text"
    Me.cb_as_city.TabIndex = 8
    Me.cb_as_city.TextCombo = Nothing
    Me.cb_as_city.TextVisible = False
    Me.cb_as_city.TextWidth = 117
    '
    'cb_as_address_country
    '
    Me.cb_as_address_country.AllowUnlistedValues = False
    Me.cb_as_address_country.AutoCompleteMode = False
    Me.cb_as_address_country.IsReadOnly = False
    Me.cb_as_address_country.Location = New System.Drawing.Point(2, 170)
    Me.cb_as_address_country.Margin = New System.Windows.Forms.Padding(2)
    Me.cb_as_address_country.Name = "cb_as_address_country"
    Me.cb_as_address_country.SelectedIndex = -1
    Me.cb_as_address_country.Size = New System.Drawing.Size(299, 24)
    Me.cb_as_address_country.SufixText = "Sufix Text"
    Me.cb_as_address_country.TabIndex = 6
    Me.cb_as_address_country.TextCombo = Nothing
    Me.cb_as_address_country.TextVisible = False
    Me.cb_as_address_country.TextWidth = 117
    '
    'ef_as_street
    '
    Me.tlp_AutoSelection.SetColumnSpan(Me.ef_as_street, 2)
    Me.ef_as_street.DoubleValue = 0.0R
    Me.ef_as_street.IntegerValue = 0
    Me.ef_as_street.IsReadOnly = False
    Me.ef_as_street.Location = New System.Drawing.Point(2, 2)
    Me.ef_as_street.Margin = New System.Windows.Forms.Padding(2)
    Me.ef_as_street.Name = "ef_as_street"
    Me.ef_as_street.PlaceHolder = Nothing
    Me.ef_as_street.Size = New System.Drawing.Size(605, 24)
    Me.ef_as_street.SufixText = "Sufix Text"
    Me.ef_as_street.TabIndex = 0
    Me.ef_as_street.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_as_street.TextValue = ""
    Me.ef_as_street.TextVisible = False
    Me.ef_as_street.TextWidth = 117
    Me.ef_as_street.Value = ""
    Me.ef_as_street.ValueForeColor = System.Drawing.Color.Blue
    '
    'cb_as_federal_entity
    '
    Me.cb_as_federal_entity.AllowUnlistedValues = False
    Me.cb_as_federal_entity.AutoCompleteMode = False
    Me.cb_as_federal_entity.Enabled = False
    Me.cb_as_federal_entity.IsReadOnly = False
    Me.cb_as_federal_entity.Location = New System.Drawing.Point(308, 170)
    Me.cb_as_federal_entity.Margin = New System.Windows.Forms.Padding(2)
    Me.cb_as_federal_entity.Name = "cb_as_federal_entity"
    Me.cb_as_federal_entity.SelectedIndex = -1
    Me.cb_as_federal_entity.Size = New System.Drawing.Size(299, 24)
    Me.cb_as_federal_entity.SufixText = "Sufix Text"
    Me.cb_as_federal_entity.TabIndex = 7
    Me.cb_as_federal_entity.TextCombo = Nothing
    Me.cb_as_federal_entity.TextVisible = False
    Me.cb_as_federal_entity.TextWidth = 117
    '
    'ef_as_external_number
    '
    Me.tlp_AutoSelection.SetColumnSpan(Me.ef_as_external_number, 2)
    Me.ef_as_external_number.DoubleValue = 0.0R
    Me.ef_as_external_number.IntegerValue = 0
    Me.ef_as_external_number.IsReadOnly = False
    Me.ef_as_external_number.Location = New System.Drawing.Point(2, 30)
    Me.ef_as_external_number.Margin = New System.Windows.Forms.Padding(2)
    Me.ef_as_external_number.Name = "ef_as_external_number"
    Me.ef_as_external_number.PlaceHolder = Nothing
    Me.ef_as_external_number.Size = New System.Drawing.Size(299, 24)
    Me.ef_as_external_number.SufixText = "Sufix Text"
    Me.ef_as_external_number.TabIndex = 1
    Me.ef_as_external_number.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_as_external_number.TextValue = ""
    Me.ef_as_external_number.TextVisible = False
    Me.ef_as_external_number.TextWidth = 117
    Me.ef_as_external_number.Value = ""
    Me.ef_as_external_number.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_as_zip
    '
    Me.ef_as_zip.DoubleValue = 0.0R
    Me.ef_as_zip.IntegerValue = 0
    Me.ef_as_zip.IsReadOnly = False
    Me.ef_as_zip.Location = New System.Drawing.Point(2, 58)
    Me.ef_as_zip.Margin = New System.Windows.Forms.Padding(2)
    Me.ef_as_zip.Name = "ef_as_zip"
    Me.ef_as_zip.PlaceHolder = Nothing
    Me.ef_as_zip.Size = New System.Drawing.Size(299, 24)
    Me.ef_as_zip.SufixText = "Sufix Text"
    Me.ef_as_zip.TabIndex = 2
    Me.ef_as_zip.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_as_zip.TextValue = ""
    Me.ef_as_zip.TextVisible = False
    Me.ef_as_zip.TextWidth = 117
    Me.ef_as_zip.Value = ""
    Me.ef_as_zip.ValueForeColor = System.Drawing.Color.Blue
    '
    'tab_beneficiary
    '
    Me.tab_beneficiary.Controls.Add(Me.gb_beneficiary)
    Me.tab_beneficiary.Location = New System.Drawing.Point(4, 22)
    Me.tab_beneficiary.Name = "tab_beneficiary"
    Me.tab_beneficiary.Padding = New System.Windows.Forms.Padding(3)
    Me.tab_beneficiary.Size = New System.Drawing.Size(627, 329)
    Me.tab_beneficiary.TabIndex = 6
    Me.tab_beneficiary.Text = "xBeneficiario"
    Me.tab_beneficiary.UseVisualStyleBackColor = True
    '
    'gb_beneficiary
    '
    Me.gb_beneficiary.Controls.Add(Me.tlp_beneficiary)
    Me.gb_beneficiary.Controls.Add(Me.chk_ben_holder_data)
    Me.gb_beneficiary.Dock = System.Windows.Forms.DockStyle.Fill
    Me.gb_beneficiary.Location = New System.Drawing.Point(3, 3)
    Me.gb_beneficiary.Name = "gb_beneficiary"
    Me.gb_beneficiary.Size = New System.Drawing.Size(621, 323)
    Me.gb_beneficiary.TabIndex = 0
    Me.gb_beneficiary.TabStop = False
    '
    'tlp_beneficiary
    '
    Me.tlp_beneficiary.ColumnCount = 2
    Me.tlp_beneficiary.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
    Me.tlp_beneficiary.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
    Me.tlp_beneficiary.Controls.Add(Me.cb_ben_occupation, 0, 4)
    Me.tlp_beneficiary.Controls.Add(Me.ef_ben_name, 0, 0)
    Me.tlp_beneficiary.Controls.Add(Me.ef_ben_surname_1, 0, 1)
    Me.tlp_beneficiary.Controls.Add(Me.tlp_ben_left, 0, 3)
    Me.tlp_beneficiary.Controls.Add(Me.ef_ben_surname_2, 0, 2)
    Me.tlp_beneficiary.Controls.Add(Me.tlp_ben_right, 1, 3)
    Me.tlp_beneficiary.Location = New System.Drawing.Point(3, 13)
    Me.tlp_beneficiary.Name = "tlp_beneficiary"
    Me.tlp_beneficiary.RowCount = 8
    Me.tlp_beneficiary.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.tlp_beneficiary.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.tlp_beneficiary.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.tlp_beneficiary.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.tlp_beneficiary.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.tlp_beneficiary.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.tlp_beneficiary.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.tlp_beneficiary.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.tlp_beneficiary.Size = New System.Drawing.Size(613, 225)
    Me.tlp_beneficiary.TabIndex = 10
    '
    'cb_ben_occupation
    '
    Me.cb_ben_occupation.AllowUnlistedValues = False
    Me.cb_ben_occupation.AutoCompleteMode = False
    Me.tlp_beneficiary.SetColumnSpan(Me.cb_ben_occupation, 2)
    Me.cb_ben_occupation.IsReadOnly = False
    Me.cb_ben_occupation.Location = New System.Drawing.Point(2, 170)
    Me.cb_ben_occupation.Margin = New System.Windows.Forms.Padding(2)
    Me.cb_ben_occupation.Name = "cb_ben_occupation"
    Me.cb_ben_occupation.SelectedIndex = -1
    Me.cb_ben_occupation.Size = New System.Drawing.Size(605, 24)
    Me.cb_ben_occupation.SufixText = "Sufix Text"
    Me.cb_ben_occupation.TabIndex = 5
    Me.cb_ben_occupation.TextCombo = Nothing
    Me.cb_ben_occupation.TextVisible = False
    Me.cb_ben_occupation.TextWidth = 117
    '
    'ef_ben_name
    '
    Me.tlp_beneficiary.SetColumnSpan(Me.ef_ben_name, 2)
    Me.ef_ben_name.DoubleValue = 0.0R
    Me.ef_ben_name.IntegerValue = 0
    Me.ef_ben_name.IsReadOnly = False
    Me.ef_ben_name.Location = New System.Drawing.Point(2, 2)
    Me.ef_ben_name.Margin = New System.Windows.Forms.Padding(2)
    Me.ef_ben_name.Name = "ef_ben_name"
    Me.ef_ben_name.OnlyUpperCase = True
    Me.ef_ben_name.PlaceHolder = Nothing
    Me.ef_ben_name.Size = New System.Drawing.Size(605, 24)
    Me.ef_ben_name.SufixText = "Sufix Text"
    Me.ef_ben_name.TabIndex = 0
    Me.ef_ben_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_ben_name.TextValue = ""
    Me.ef_ben_name.TextVisible = False
    Me.ef_ben_name.TextWidth = 117
    Me.ef_ben_name.Value = ""
    Me.ef_ben_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_ben_surname_1
    '
    Me.tlp_beneficiary.SetColumnSpan(Me.ef_ben_surname_1, 2)
    Me.ef_ben_surname_1.DoubleValue = 0.0R
    Me.ef_ben_surname_1.IntegerValue = 0
    Me.ef_ben_surname_1.IsReadOnly = False
    Me.ef_ben_surname_1.Location = New System.Drawing.Point(2, 30)
    Me.ef_ben_surname_1.Margin = New System.Windows.Forms.Padding(2)
    Me.ef_ben_surname_1.Name = "ef_ben_surname_1"
    Me.ef_ben_surname_1.OnlyUpperCase = True
    Me.ef_ben_surname_1.PlaceHolder = Nothing
    Me.ef_ben_surname_1.Size = New System.Drawing.Size(605, 24)
    Me.ef_ben_surname_1.SufixText = "Sufix Text"
    Me.ef_ben_surname_1.TabIndex = 1
    Me.ef_ben_surname_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_ben_surname_1.TextValue = ""
    Me.ef_ben_surname_1.TextVisible = False
    Me.ef_ben_surname_1.TextWidth = 117
    Me.ef_ben_surname_1.Value = ""
    Me.ef_ben_surname_1.ValueForeColor = System.Drawing.Color.Blue
    '
    'tlp_ben_left
    '
    Me.tlp_ben_left.AutoSize = True
    Me.tlp_ben_left.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.tlp_ben_left.ColumnCount = 1
    Me.tlp_ben_left.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
    Me.tlp_ben_left.Controls.Add(Me.ef_ben_doc_id1, 0, 0)
    Me.tlp_ben_left.Controls.Add(Me.ef_ben_doc_id2, 0, 1)
    Me.tlp_ben_left.Controls.Add(Me.dtp_ben_birthdate, 0, 2)
    Me.tlp_ben_left.Location = New System.Drawing.Point(0, 84)
    Me.tlp_ben_left.Margin = New System.Windows.Forms.Padding(0)
    Me.tlp_ben_left.Name = "tlp_ben_left"
    Me.tlp_ben_left.RowCount = 4
    Me.tlp_ben_left.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.tlp_ben_left.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.tlp_ben_left.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.tlp_ben_left.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.tlp_ben_left.Size = New System.Drawing.Size(303, 84)
    Me.tlp_ben_left.TabIndex = 3
    '
    'ef_ben_doc_id1
    '
    Me.ef_ben_doc_id1.DoubleValue = 0.0R
    Me.ef_ben_doc_id1.IntegerValue = 0
    Me.ef_ben_doc_id1.IsReadOnly = False
    Me.ef_ben_doc_id1.Location = New System.Drawing.Point(2, 2)
    Me.ef_ben_doc_id1.Margin = New System.Windows.Forms.Padding(2)
    Me.ef_ben_doc_id1.Name = "ef_ben_doc_id1"
    Me.ef_ben_doc_id1.OnlyUpperCase = True
    Me.ef_ben_doc_id1.PlaceHolder = Nothing
    Me.ef_ben_doc_id1.Size = New System.Drawing.Size(299, 24)
    Me.ef_ben_doc_id1.SufixText = "Sufix Text"
    Me.ef_ben_doc_id1.TabIndex = 0
    Me.ef_ben_doc_id1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_ben_doc_id1.TextValue = ""
    Me.ef_ben_doc_id1.TextVisible = False
    Me.ef_ben_doc_id1.TextWidth = 117
    Me.ef_ben_doc_id1.Value = ""
    Me.ef_ben_doc_id1.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_ben_doc_id2
    '
    Me.ef_ben_doc_id2.DoubleValue = 0.0R
    Me.ef_ben_doc_id2.IntegerValue = 0
    Me.ef_ben_doc_id2.IsReadOnly = False
    Me.ef_ben_doc_id2.Location = New System.Drawing.Point(2, 30)
    Me.ef_ben_doc_id2.Margin = New System.Windows.Forms.Padding(2)
    Me.ef_ben_doc_id2.Name = "ef_ben_doc_id2"
    Me.ef_ben_doc_id2.OnlyUpperCase = True
    Me.ef_ben_doc_id2.PlaceHolder = Nothing
    Me.ef_ben_doc_id2.Size = New System.Drawing.Size(299, 24)
    Me.ef_ben_doc_id2.SufixText = "Sufix Text"
    Me.ef_ben_doc_id2.TabIndex = 1
    Me.ef_ben_doc_id2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_ben_doc_id2.TextValue = ""
    Me.ef_ben_doc_id2.TextVisible = False
    Me.ef_ben_doc_id2.TextWidth = 117
    Me.ef_ben_doc_id2.Value = ""
    Me.ef_ben_doc_id2.ValueForeColor = System.Drawing.Color.Blue
    '
    'dtp_ben_birthdate
    '
    Me.dtp_ben_birthdate.Checked = True
    Me.dtp_ben_birthdate.IsReadOnly = False
    Me.dtp_ben_birthdate.Location = New System.Drawing.Point(2, 58)
    Me.dtp_ben_birthdate.Margin = New System.Windows.Forms.Padding(2)
    Me.dtp_ben_birthdate.Name = "dtp_ben_birthdate"
    Me.dtp_ben_birthdate.ShowCheckBox = True
    Me.dtp_ben_birthdate.ShowUpDown = False
    Me.dtp_ben_birthdate.Size = New System.Drawing.Size(299, 24)
    Me.dtp_ben_birthdate.SufixText = "Sufix Text"
    Me.dtp_ben_birthdate.TabIndex = 2
    Me.dtp_ben_birthdate.TextVisible = False
    Me.dtp_ben_birthdate.TextWidth = 117
    Me.dtp_ben_birthdate.Value = New Date(2013, 3, 1, 0, 0, 0, 0)
    '
    'ef_ben_surname_2
    '
    Me.tlp_beneficiary.SetColumnSpan(Me.ef_ben_surname_2, 2)
    Me.ef_ben_surname_2.DoubleValue = 0.0R
    Me.ef_ben_surname_2.IntegerValue = 0
    Me.ef_ben_surname_2.IsReadOnly = False
    Me.ef_ben_surname_2.Location = New System.Drawing.Point(2, 58)
    Me.ef_ben_surname_2.Margin = New System.Windows.Forms.Padding(2)
    Me.ef_ben_surname_2.Name = "ef_ben_surname_2"
    Me.ef_ben_surname_2.OnlyUpperCase = True
    Me.ef_ben_surname_2.PlaceHolder = Nothing
    Me.ef_ben_surname_2.Size = New System.Drawing.Size(605, 24)
    Me.ef_ben_surname_2.SufixText = "Sufix Text"
    Me.ef_ben_surname_2.TabIndex = 2
    Me.ef_ben_surname_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_ben_surname_2.TextValue = ""
    Me.ef_ben_surname_2.TextVisible = False
    Me.ef_ben_surname_2.TextWidth = 117
    Me.ef_ben_surname_2.Value = ""
    Me.ef_ben_surname_2.ValueForeColor = System.Drawing.Color.Blue
    '
    'tlp_ben_right
    '
    Me.tlp_ben_right.AutoSize = True
    Me.tlp_ben_right.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.tlp_ben_right.ColumnCount = 1
    Me.tlp_ben_right.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
    Me.tlp_ben_right.Controls.Add(Me.btn_ben_document, 0, 0)
    Me.tlp_ben_right.Controls.Add(Me.cb_ben_gender, 0, 1)
    Me.tlp_ben_right.Location = New System.Drawing.Point(306, 84)
    Me.tlp_ben_right.Margin = New System.Windows.Forms.Padding(0)
    Me.tlp_ben_right.Name = "tlp_ben_right"
    Me.tlp_ben_right.RowCount = 4
    Me.tlp_ben_right.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.tlp_ben_right.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.tlp_ben_right.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.tlp_ben_right.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.tlp_ben_right.Size = New System.Drawing.Size(304, 62)
    Me.tlp_ben_right.TabIndex = 4
    '
    'btn_ben_document
    '
    Me.btn_ben_document.Anchor = System.Windows.Forms.AnchorStyles.Right
    Me.btn_ben_document.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_ben_document.Location = New System.Drawing.Point(210, 2)
    Me.btn_ben_document.Margin = New System.Windows.Forms.Padding(2, 2, 4, 2)
    Me.btn_ben_document.Name = "btn_ben_document"
    Me.btn_ben_document.Size = New System.Drawing.Size(90, 30)
    Me.btn_ben_document.TabIndex = 0
    Me.btn_ben_document.ToolTipped = False
    Me.btn_ben_document.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.USER
    '
    'cb_ben_gender
    '
    Me.cb_ben_gender.AllowUnlistedValues = False
    Me.cb_ben_gender.AutoCompleteMode = False
    Me.cb_ben_gender.IsReadOnly = False
    Me.cb_ben_gender.Location = New System.Drawing.Point(2, 36)
    Me.cb_ben_gender.Margin = New System.Windows.Forms.Padding(2)
    Me.cb_ben_gender.Name = "cb_ben_gender"
    Me.cb_ben_gender.SelectedIndex = -1
    Me.cb_ben_gender.Size = New System.Drawing.Size(300, 24)
    Me.cb_ben_gender.SufixText = "Sufix Text"
    Me.cb_ben_gender.TabIndex = 1
    Me.cb_ben_gender.TextCombo = Nothing
    Me.cb_ben_gender.TextVisible = False
    Me.cb_ben_gender.TextWidth = 117
    '
    'chk_ben_holder_data
    '
    Me.chk_ben_holder_data.AutoSize = True
    Me.chk_ben_holder_data.Location = New System.Drawing.Point(10, 298)
    Me.chk_ben_holder_data.Name = "chk_ben_holder_data"
    Me.chk_ben_holder_data.Size = New System.Drawing.Size(155, 17)
    Me.chk_ben_holder_data.TabIndex = 9
    Me.chk_ben_holder_data.Text = "xHolder as Beneficiary"
    Me.chk_ben_holder_data.UseVisualStyleBackColor = True
    '
    'tab_points_program
    '
    Me.tab_points_program.Controls.Add(Me.lbl_points_sufix)
    Me.tab_points_program.Controls.Add(Me.gb_next_level)
    Me.tab_points_program.Controls.Add(Me.lbl_points)
    Me.tab_points_program.Controls.Add(Me.lbl_points_to_maintain)
    Me.tab_points_program.Controls.Add(Me.lbl_level_expiration)
    Me.tab_points_program.Controls.Add(Me.lbl_level_entered)
    Me.tab_points_program.Controls.Add(Me.lbl_level_value)
    Me.tab_points_program.Controls.Add(Me.lbl_balance)
    Me.tab_points_program.Controls.Add(Me.lbl_points_to_keep_level)
    Me.tab_points_program.Controls.Add(Me.lbl_expiration_date)
    Me.tab_points_program.Controls.Add(Me.lbl_entry_date)
    Me.tab_points_program.Controls.Add(Me.lbl_level)
    Me.tab_points_program.Location = New System.Drawing.Point(4, 22)
    Me.tab_points_program.Name = "tab_points_program"
    Me.tab_points_program.Padding = New System.Windows.Forms.Padding(3)
    Me.tab_points_program.Size = New System.Drawing.Size(627, 329)
    Me.tab_points_program.TabIndex = 7
    Me.tab_points_program.Text = "xPointsProgram"
    Me.tab_points_program.UseVisualStyleBackColor = True
    '
    'lbl_points_sufix
    '
    Me.lbl_points_sufix.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold)
    Me.lbl_points_sufix.Location = New System.Drawing.Point(282, 191)
    Me.lbl_points_sufix.Name = "lbl_points_sufix"
    Me.lbl_points_sufix.Size = New System.Drawing.Size(53, 24)
    Me.lbl_points_sufix.TabIndex = 60
    Me.lbl_points_sufix.Text = "xPuntos"
    Me.lbl_points_sufix.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'gb_next_level
    '
    Me.gb_next_level.Controls.Add(Me.lbl_points_last_days)
    Me.gb_next_level.Controls.Add(Me.lbl_points_discretionaries_prefix)
    Me.gb_next_level.Controls.Add(Me.lbl_points_discretionaries)
    Me.gb_next_level.Controls.Add(Me.lbl_points_generated_prefix)
    Me.gb_next_level.Controls.Add(Me.lbl_points_generated)
    Me.gb_next_level.Controls.Add(Me.lbl_points_to_next_level)
    Me.gb_next_level.Controls.Add(Me.lbl_points_to_next_level_prefix)
    Me.gb_next_level.Controls.Add(Me.lbl_points_level_prefix)
    Me.gb_next_level.Controls.Add(Me.lbl_points_level)
    Me.gb_next_level.Controls.Add(Me.lbl_points_req_prefix)
    Me.gb_next_level.Controls.Add(Me.lbl_points_req)
    Me.gb_next_level.Controls.Add(Me.lbl_next_level_name)
    Me.gb_next_level.Font = New System.Drawing.Font("Verdana", 8.25!)
    Me.gb_next_level.Location = New System.Drawing.Point(341, 23)
    Me.gb_next_level.Name = "gb_next_level"
    Me.gb_next_level.Size = New System.Drawing.Size(280, 227)
    Me.gb_next_level.TabIndex = 59
    Me.gb_next_level.TabStop = False
    Me.gb_next_level.Text = "xSiguiente Nivel"
    '
    'lbl_points_last_days
    '
    Me.lbl_points_last_days.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.lbl_points_last_days.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_points_last_days.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_points_last_days.Location = New System.Drawing.Point(3, 204)
    Me.lbl_points_last_days.Name = "lbl_points_last_days"
    Me.lbl_points_last_days.Size = New System.Drawing.Size(274, 20)
    Me.lbl_points_last_days.TabIndex = 68
    Me.lbl_points_last_days.Text = "*xPuntos obtenidos en los �ltimos XX d�as"
    Me.lbl_points_last_days.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_points_discretionaries_prefix
    '
    Me.lbl_points_discretionaries_prefix.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_points_discretionaries_prefix.Location = New System.Drawing.Point(7, 104)
    Me.lbl_points_discretionaries_prefix.Name = "lbl_points_discretionaries_prefix"
    Me.lbl_points_discretionaries_prefix.Size = New System.Drawing.Size(167, 20)
    Me.lbl_points_discretionaries_prefix.TabIndex = 66
    Me.lbl_points_discretionaries_prefix.Text = "xPuntos Discrecionales"
    Me.lbl_points_discretionaries_prefix.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_points_discretionaries
    '
    Me.lbl_points_discretionaries.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_points_discretionaries.Location = New System.Drawing.Point(169, 104)
    Me.lbl_points_discretionaries.Name = "lbl_points_discretionaries"
    Me.lbl_points_discretionaries.Size = New System.Drawing.Size(108, 20)
    Me.lbl_points_discretionaries.TabIndex = 65
    Me.lbl_points_discretionaries.Text = "x10.000"
    Me.lbl_points_discretionaries.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_points_generated_prefix
    '
    Me.lbl_points_generated_prefix.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_points_generated_prefix.Location = New System.Drawing.Point(7, 84)
    Me.lbl_points_generated_prefix.Name = "lbl_points_generated_prefix"
    Me.lbl_points_generated_prefix.Size = New System.Drawing.Size(167, 20)
    Me.lbl_points_generated_prefix.TabIndex = 64
    Me.lbl_points_generated_prefix.Text = "xPuntos Generados"
    Me.lbl_points_generated_prefix.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_points_generated
    '
    Me.lbl_points_generated.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_points_generated.Location = New System.Drawing.Point(169, 84)
    Me.lbl_points_generated.Name = "lbl_points_generated"
    Me.lbl_points_generated.Size = New System.Drawing.Size(108, 20)
    Me.lbl_points_generated.TabIndex = 63
    Me.lbl_points_generated.Text = "x10.000"
    Me.lbl_points_generated.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_points_to_next_level
    '
    Me.lbl_points_to_next_level.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold)
    Me.lbl_points_to_next_level.Location = New System.Drawing.Point(193, 152)
    Me.lbl_points_to_next_level.Name = "lbl_points_to_next_level"
    Me.lbl_points_to_next_level.Size = New System.Drawing.Size(85, 38)
    Me.lbl_points_to_next_level.TabIndex = 62
    Me.lbl_points_to_next_level.Text = "---"
    Me.lbl_points_to_next_level.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_points_to_next_level_prefix
    '
    Me.lbl_points_to_next_level_prefix.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold)
    Me.lbl_points_to_next_level_prefix.Location = New System.Drawing.Point(7, 152)
    Me.lbl_points_to_next_level_prefix.Name = "lbl_points_to_next_level_prefix"
    Me.lbl_points_to_next_level_prefix.Size = New System.Drawing.Size(198, 38)
    Me.lbl_points_to_next_level_prefix.TabIndex = 61
    Me.lbl_points_to_next_level_prefix.Text = "xPuntos Faltantes"
    Me.lbl_points_to_next_level_prefix.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_points_level_prefix
    '
    Me.lbl_points_level_prefix.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold)
    Me.lbl_points_level_prefix.Location = New System.Drawing.Point(7, 114)
    Me.lbl_points_level_prefix.Name = "lbl_points_level_prefix"
    Me.lbl_points_level_prefix.Size = New System.Drawing.Size(167, 38)
    Me.lbl_points_level_prefix.TabIndex = 60
    Me.lbl_points_level_prefix.Text = "xPuntos Nivel*"
    Me.lbl_points_level_prefix.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_points_level
    '
    Me.lbl_points_level.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold)
    Me.lbl_points_level.Location = New System.Drawing.Point(169, 114)
    Me.lbl_points_level.Name = "lbl_points_level"
    Me.lbl_points_level.Size = New System.Drawing.Size(108, 38)
    Me.lbl_points_level.TabIndex = 59
    Me.lbl_points_level.Text = "x10.000"
    Me.lbl_points_level.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_points_req_prefix
    '
    Me.lbl_points_req_prefix.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold)
    Me.lbl_points_req_prefix.Location = New System.Drawing.Point(6, 46)
    Me.lbl_points_req_prefix.Name = "lbl_points_req_prefix"
    Me.lbl_points_req_prefix.Size = New System.Drawing.Size(199, 38)
    Me.lbl_points_req_prefix.TabIndex = 58
    Me.lbl_points_req_prefix.Text = "xPuntos Requeridos"
    Me.lbl_points_req_prefix.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_points_req
    '
    Me.lbl_points_req.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold)
    Me.lbl_points_req.Location = New System.Drawing.Point(193, 46)
    Me.lbl_points_req.Name = "lbl_points_req"
    Me.lbl_points_req.Size = New System.Drawing.Size(85, 38)
    Me.lbl_points_req.TabIndex = 57
    Me.lbl_points_req.Text = "---"
    Me.lbl_points_req.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_next_level_name
    '
    Me.lbl_next_level_name.Dock = System.Windows.Forms.DockStyle.Top
    Me.lbl_next_level_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_next_level_name.Location = New System.Drawing.Point(3, 17)
    Me.lbl_next_level_name.Name = "lbl_next_level_name"
    Me.lbl_next_level_name.Size = New System.Drawing.Size(274, 28)
    Me.lbl_next_level_name.TabIndex = 50
    Me.lbl_next_level_name.Text = "---"
    Me.lbl_next_level_name.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lbl_points
    '
    Me.lbl_points.Location = New System.Drawing.Point(188, 191)
    Me.lbl_points.Name = "lbl_points"
    Me.lbl_points.Size = New System.Drawing.Size(88, 24)
    Me.lbl_points.TabIndex = 9
    Me.lbl_points.Text = "xBalanceValue"
    Me.lbl_points.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_points_to_maintain
    '
    Me.lbl_points_to_maintain.Location = New System.Drawing.Point(188, 157)
    Me.lbl_points_to_maintain.Name = "lbl_points_to_maintain"
    Me.lbl_points_to_maintain.Size = New System.Drawing.Size(88, 24)
    Me.lbl_points_to_maintain.TabIndex = 8
    Me.lbl_points_to_maintain.Text = "---"
    Me.lbl_points_to_maintain.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_level_expiration
    '
    Me.lbl_level_expiration.Location = New System.Drawing.Point(188, 122)
    Me.lbl_level_expiration.Name = "lbl_level_expiration"
    Me.lbl_level_expiration.Size = New System.Drawing.Size(139, 24)
    Me.lbl_level_expiration.TabIndex = 7
    Me.lbl_level_expiration.Text = "xExpirationDateValue"
    Me.lbl_level_expiration.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_level_entered
    '
    Me.lbl_level_entered.Location = New System.Drawing.Point(188, 88)
    Me.lbl_level_entered.Name = "lbl_level_entered"
    Me.lbl_level_entered.Size = New System.Drawing.Size(139, 24)
    Me.lbl_level_entered.TabIndex = 6
    Me.lbl_level_entered.Text = "xEntryDateValue"
    Me.lbl_level_entered.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_level_value
    '
    Me.lbl_level_value.Location = New System.Drawing.Point(188, 54)
    Me.lbl_level_value.Name = "lbl_level_value"
    Me.lbl_level_value.Size = New System.Drawing.Size(139, 24)
    Me.lbl_level_value.TabIndex = 5
    Me.lbl_level_value.Text = "xLevelValue"
    Me.lbl_level_value.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_balance
    '
    Me.lbl_balance.Location = New System.Drawing.Point(0, 191)
    Me.lbl_balance.Name = "lbl_balance"
    Me.lbl_balance.Size = New System.Drawing.Size(182, 24)
    Me.lbl_balance.TabIndex = 4
    Me.lbl_balance.Text = "xBalance"
    Me.lbl_balance.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_points_to_keep_level
    '
    Me.lbl_points_to_keep_level.Location = New System.Drawing.Point(0, 157)
    Me.lbl_points_to_keep_level.Margin = New System.Windows.Forms.Padding(0)
    Me.lbl_points_to_keep_level.Name = "lbl_points_to_keep_level"
    Me.lbl_points_to_keep_level.Size = New System.Drawing.Size(182, 24)
    Me.lbl_points_to_keep_level.TabIndex = 3
    Me.lbl_points_to_keep_level.Text = "xPointsToKeepLevel"
    Me.lbl_points_to_keep_level.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_expiration_date
    '
    Me.lbl_expiration_date.Location = New System.Drawing.Point(0, 122)
    Me.lbl_expiration_date.Name = "lbl_expiration_date"
    Me.lbl_expiration_date.Size = New System.Drawing.Size(182, 24)
    Me.lbl_expiration_date.TabIndex = 2
    Me.lbl_expiration_date.Text = "xExpirationDate"
    Me.lbl_expiration_date.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_entry_date
    '
    Me.lbl_entry_date.Location = New System.Drawing.Point(0, 88)
    Me.lbl_entry_date.Name = "lbl_entry_date"
    Me.lbl_entry_date.Size = New System.Drawing.Size(182, 24)
    Me.lbl_entry_date.TabIndex = 1
    Me.lbl_entry_date.Text = "xEntryDate"
    Me.lbl_entry_date.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_level
    '
    Me.lbl_level.Location = New System.Drawing.Point(0, 54)
    Me.lbl_level.Name = "lbl_level"
    Me.lbl_level.Size = New System.Drawing.Size(182, 24)
    Me.lbl_level.TabIndex = 0
    Me.lbl_level.Text = "xLevel"
    Me.lbl_level.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'tab_block
    '
    Me.tab_block.Controls.Add(Me.gb_block)
    Me.tab_block.Location = New System.Drawing.Point(4, 22)
    Me.tab_block.Name = "tab_block"
    Me.tab_block.Padding = New System.Windows.Forms.Padding(3)
    Me.tab_block.Size = New System.Drawing.Size(627, 329)
    Me.tab_block.TabIndex = 5
    Me.tab_block.Text = "xBloqueo"
    Me.tab_block.UseVisualStyleBackColor = True
    '
    'gb_block
    '
    Me.gb_block.Controls.Add(Me.txt_block_description)
    Me.gb_block.Controls.Add(Me.lbl_block_description)
    Me.gb_block.Controls.Add(Me.lbl_blocked)
    Me.gb_block.Dock = System.Windows.Forms.DockStyle.Fill
    Me.gb_block.Location = New System.Drawing.Point(3, 3)
    Me.gb_block.Name = "gb_block"
    Me.gb_block.Size = New System.Drawing.Size(621, 323)
    Me.gb_block.TabIndex = 4
    Me.gb_block.TabStop = False
    '
    'txt_block_description
    '
    Me.txt_block_description.Location = New System.Drawing.Point(24, 96)
    Me.txt_block_description.Multiline = True
    Me.txt_block_description.Name = "txt_block_description"
    Me.txt_block_description.ReadOnly = True
    Me.txt_block_description.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
    Me.txt_block_description.Size = New System.Drawing.Size(573, 211)
    Me.txt_block_description.TabIndex = 8
    '
    'lbl_block_description
    '
    Me.lbl_block_description.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_block_description.Location = New System.Drawing.Point(27, 72)
    Me.lbl_block_description.Name = "lbl_block_description"
    Me.lbl_block_description.Size = New System.Drawing.Size(508, 13)
    Me.lbl_block_description.TabIndex = 7
    Me.lbl_block_description.Text = "xMotivoBloqueo"
    '
    'lbl_blocked
    '
    Me.lbl_blocked.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_blocked.Location = New System.Drawing.Point(28, 17)
    Me.lbl_blocked.Name = "lbl_blocked"
    Me.lbl_blocked.Size = New System.Drawing.Size(569, 55)
    Me.lbl_blocked.TabIndex = 6
    Me.lbl_blocked.Text = "xBloqueo"
    '
    'tab_comments
    '
    Me.tab_comments.Controls.Add(Me.gb_extra_info)
    Me.tab_comments.Location = New System.Drawing.Point(4, 22)
    Me.tab_comments.Name = "tab_comments"
    Me.tab_comments.Padding = New System.Windows.Forms.Padding(3)
    Me.tab_comments.Size = New System.Drawing.Size(627, 329)
    Me.tab_comments.TabIndex = 3
    Me.tab_comments.Text = "xComentarios"
    Me.tab_comments.UseVisualStyleBackColor = True
    '
    'gb_extra_info
    '
    Me.gb_extra_info.Controls.Add(Me.chk_show_comments)
    Me.gb_extra_info.Controls.Add(Me.txt_comments)
    Me.gb_extra_info.Dock = System.Windows.Forms.DockStyle.Fill
    Me.gb_extra_info.Location = New System.Drawing.Point(3, 3)
    Me.gb_extra_info.Name = "gb_extra_info"
    Me.gb_extra_info.Size = New System.Drawing.Size(621, 323)
    Me.gb_extra_info.TabIndex = 3
    Me.gb_extra_info.TabStop = False
    '
    'chk_show_comments
    '
    Me.chk_show_comments.AutoSize = True
    Me.chk_show_comments.Location = New System.Drawing.Point(6, 301)
    Me.chk_show_comments.Name = "chk_show_comments"
    Me.chk_show_comments.Size = New System.Drawing.Size(277, 17)
    Me.chk_show_comments.TabIndex = 10
    Me.chk_show_comments.Text = "xShow comments when reading player card"
    Me.chk_show_comments.UseVisualStyleBackColor = True
    '
    'txt_comments
    '
    Me.txt_comments.Location = New System.Drawing.Point(3, 17)
    Me.txt_comments.Multiline = True
    Me.txt_comments.Name = "txt_comments"
    Me.txt_comments.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
    Me.txt_comments.Size = New System.Drawing.Size(612, 278)
    Me.txt_comments.TabIndex = 0
    '
    'tab_pin
    '
    Me.tab_pin.Controls.Add(Me.gb_pin)
    Me.tab_pin.Location = New System.Drawing.Point(4, 22)
    Me.tab_pin.Name = "tab_pin"
    Me.tab_pin.Padding = New System.Windows.Forms.Padding(3)
    Me.tab_pin.Size = New System.Drawing.Size(627, 329)
    Me.tab_pin.TabIndex = 4
    Me.tab_pin.Text = "PIN"
    Me.tab_pin.UseVisualStyleBackColor = True
    '
    'gb_pin
    '
    Me.gb_pin.Controls.Add(Me.btn_save_pin)
    Me.gb_pin.Controls.Add(Me.ef_new_pin)
    Me.gb_pin.Controls.Add(Me.ef_confirm_pin)
    Me.gb_pin.Dock = System.Windows.Forms.DockStyle.Fill
    Me.gb_pin.Location = New System.Drawing.Point(3, 3)
    Me.gb_pin.Name = "gb_pin"
    Me.gb_pin.Size = New System.Drawing.Size(621, 323)
    Me.gb_pin.TabIndex = 3
    Me.gb_pin.TabStop = False
    '
    'btn_save_pin
    '
    Me.btn_save_pin.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_save_pin.Location = New System.Drawing.Point(126, 110)
    Me.btn_save_pin.Name = "btn_save_pin"
    Me.btn_save_pin.Size = New System.Drawing.Size(90, 30)
    Me.btn_save_pin.TabIndex = 2
    Me.btn_save_pin.ToolTipped = False
    Me.btn_save_pin.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'ef_new_pin
    '
    Me.ef_new_pin.DoubleValue = 0.0R
    Me.ef_new_pin.IntegerValue = 0
    Me.ef_new_pin.IsReadOnly = False
    Me.ef_new_pin.Location = New System.Drawing.Point(7, 49)
    Me.ef_new_pin.Name = "ef_new_pin"
    Me.ef_new_pin.PlaceHolder = Nothing
    Me.ef_new_pin.Size = New System.Drawing.Size(213, 24)
    Me.ef_new_pin.SufixText = "Sufix Text"
    Me.ef_new_pin.TabIndex = 0
    Me.ef_new_pin.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_new_pin.TextValue = ""
    Me.ef_new_pin.TextVisible = False
    Me.ef_new_pin.TextWidth = 117
    Me.ef_new_pin.Value = ""
    Me.ef_new_pin.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_confirm_pin
    '
    Me.ef_confirm_pin.DoubleValue = 0.0R
    Me.ef_confirm_pin.IntegerValue = 0
    Me.ef_confirm_pin.IsReadOnly = False
    Me.ef_confirm_pin.Location = New System.Drawing.Point(7, 79)
    Me.ef_confirm_pin.Name = "ef_confirm_pin"
    Me.ef_confirm_pin.PlaceHolder = Nothing
    Me.ef_confirm_pin.Size = New System.Drawing.Size(213, 24)
    Me.ef_confirm_pin.SufixText = "Sufix Text"
    Me.ef_confirm_pin.TabIndex = 1
    Me.ef_confirm_pin.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_confirm_pin.TextValue = ""
    Me.ef_confirm_pin.TextVisible = False
    Me.ef_confirm_pin.TextWidth = 117
    Me.ef_confirm_pin.Value = ""
    Me.ef_confirm_pin.ValueForeColor = System.Drawing.Color.Blue
    '
    'TabPage1
    '
    Me.TabPage1.Controls.Add(Me.GroupBox1)
    Me.TabPage1.Location = New System.Drawing.Point(4, 22)
    Me.TabPage1.Name = "TabPage1"
    Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
    Me.TabPage1.Size = New System.Drawing.Size(553, 245)
    Me.TabPage1.TabIndex = 0
    Me.TabPage1.Text = "xPrincipal"
    Me.TabPage1.UseVisualStyleBackColor = True
    '
    'GroupBox1
    '
    Me.GroupBox1.Controls.Add(Me.Uc_entry_field3)
    Me.GroupBox1.Controls.Add(Me.Uc_date_picker1)
    Me.GroupBox1.Controls.Add(Me.Uc_date_picker2)
    Me.GroupBox1.Controls.Add(Me.Uc_combo1)
    Me.GroupBox1.Controls.Add(Me.Uc_entry_field4)
    Me.GroupBox1.Controls.Add(Me.Uc_combo2)
    Me.GroupBox1.Controls.Add(Me.Uc_combo3)
    Me.GroupBox1.Controls.Add(Me.Uc_entry_field5)
    Me.GroupBox1.Controls.Add(Me.Uc_entry_field6)
    Me.GroupBox1.Controls.Add(Me.Uc_entry_field7)
    Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Fill
    Me.GroupBox1.Location = New System.Drawing.Point(3, 3)
    Me.GroupBox1.Name = "GroupBox1"
    Me.GroupBox1.Size = New System.Drawing.Size(547, 239)
    Me.GroupBox1.TabIndex = 0
    Me.GroupBox1.TabStop = False
    '
    'Uc_entry_field3
    '
    Me.Uc_entry_field3.DoubleValue = 0.0R
    Me.Uc_entry_field3.IntegerValue = 0
    Me.Uc_entry_field3.IsReadOnly = False
    Me.Uc_entry_field3.Location = New System.Drawing.Point(271, 169)
    Me.Uc_entry_field3.Name = "Uc_entry_field3"
    Me.Uc_entry_field3.OnlyUpperCase = True
    Me.Uc_entry_field3.PlaceHolder = Nothing
    Me.Uc_entry_field3.Size = New System.Drawing.Size(257, 24)
    Me.Uc_entry_field3.SufixText = "Sufix Text"
    Me.Uc_entry_field3.SufixTextVisible = True
    Me.Uc_entry_field3.TabIndex = 9
    Me.Uc_entry_field3.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.Uc_entry_field3.TextValue = ""
    Me.Uc_entry_field3.TextVisible = False
    Me.Uc_entry_field3.TextWidth = 93
    Me.Uc_entry_field3.Value = ""
    Me.Uc_entry_field3.ValueForeColor = System.Drawing.Color.Blue
    '
    'Uc_date_picker1
    '
    Me.Uc_date_picker1.Checked = True
    Me.Uc_date_picker1.IsReadOnly = False
    Me.Uc_date_picker1.Location = New System.Drawing.Point(7, 199)
    Me.Uc_date_picker1.Name = "Uc_date_picker1"
    Me.Uc_date_picker1.ShowCheckBox = True
    Me.Uc_date_picker1.ShowUpDown = False
    Me.Uc_date_picker1.Size = New System.Drawing.Size(257, 24)
    Me.Uc_date_picker1.SufixText = "Sufix Text"
    Me.Uc_date_picker1.SufixTextVisible = True
    Me.Uc_date_picker1.TabIndex = 8
    Me.Uc_date_picker1.TextVisible = False
    Me.Uc_date_picker1.TextWidth = 117
    Me.Uc_date_picker1.Value = New Date(2013, 3, 1, 0, 0, 0, 0)
    '
    'Uc_date_picker2
    '
    Me.Uc_date_picker2.Checked = True
    Me.Uc_date_picker2.IsReadOnly = False
    Me.Uc_date_picker2.Location = New System.Drawing.Point(7, 169)
    Me.Uc_date_picker2.Name = "Uc_date_picker2"
    Me.Uc_date_picker2.ShowCheckBox = False
    Me.Uc_date_picker2.ShowUpDown = False
    Me.Uc_date_picker2.Size = New System.Drawing.Size(257, 24)
    Me.Uc_date_picker2.SufixText = "Sufix Text"
    Me.Uc_date_picker2.SufixTextVisible = True
    Me.Uc_date_picker2.TabIndex = 7
    Me.Uc_date_picker2.TextVisible = False
    Me.Uc_date_picker2.TextWidth = 117
    Me.Uc_date_picker2.Value = New Date(2013, 3, 1, 0, 0, 0, 0)
    '
    'Uc_combo1
    '
    Me.Uc_combo1.AllowUnlistedValues = False
    Me.Uc_combo1.AutoCompleteMode = False
    Me.Uc_combo1.IsReadOnly = False
    Me.Uc_combo1.Location = New System.Drawing.Point(271, 139)
    Me.Uc_combo1.Name = "Uc_combo1"
    Me.Uc_combo1.SelectedIndex = -1
    Me.Uc_combo1.Size = New System.Drawing.Size(257, 24)
    Me.Uc_combo1.SufixText = "Sufix Text"
    Me.Uc_combo1.SufixTextVisible = True
    Me.Uc_combo1.TabIndex = 6
    Me.Uc_combo1.TextCombo = Nothing
    Me.Uc_combo1.TextVisible = False
    Me.Uc_combo1.TextWidth = 93
    '
    'Uc_entry_field4
    '
    Me.Uc_entry_field4.DoubleValue = 0.0R
    Me.Uc_entry_field4.IntegerValue = 0
    Me.Uc_entry_field4.IsReadOnly = False
    Me.Uc_entry_field4.Location = New System.Drawing.Point(271, 109)
    Me.Uc_entry_field4.Name = "Uc_entry_field4"
    Me.Uc_entry_field4.OnlyUpperCase = True
    Me.Uc_entry_field4.PlaceHolder = Nothing
    Me.Uc_entry_field4.Size = New System.Drawing.Size(257, 24)
    Me.Uc_entry_field4.SufixText = "Sufix Text"
    Me.Uc_entry_field4.SufixTextVisible = True
    Me.Uc_entry_field4.TabIndex = 4
    Me.Uc_entry_field4.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.Uc_entry_field4.TextValue = ""
    Me.Uc_entry_field4.TextVisible = False
    Me.Uc_entry_field4.TextWidth = 93
    Me.Uc_entry_field4.Value = ""
    Me.Uc_entry_field4.ValueForeColor = System.Drawing.Color.Blue
    '
    'Uc_combo2
    '
    Me.Uc_combo2.AllowUnlistedValues = False
    Me.Uc_combo2.AutoCompleteMode = False
    Me.Uc_combo2.IsReadOnly = False
    Me.Uc_combo2.Location = New System.Drawing.Point(7, 139)
    Me.Uc_combo2.Name = "Uc_combo2"
    Me.Uc_combo2.SelectedIndex = -1
    Me.Uc_combo2.Size = New System.Drawing.Size(257, 24)
    Me.Uc_combo2.SufixText = "Sufix Text"
    Me.Uc_combo2.SufixTextVisible = True
    Me.Uc_combo2.TabIndex = 5
    Me.Uc_combo2.TextCombo = Nothing
    Me.Uc_combo2.TextVisible = False
    Me.Uc_combo2.TextWidth = 117
    '
    'Uc_combo3
    '
    Me.Uc_combo3.AllowUnlistedValues = False
    Me.Uc_combo3.AutoCompleteMode = False
    Me.Uc_combo3.IsReadOnly = False
    Me.Uc_combo3.Location = New System.Drawing.Point(7, 109)
    Me.Uc_combo3.Name = "Uc_combo3"
    Me.Uc_combo3.SelectedIndex = -1
    Me.Uc_combo3.Size = New System.Drawing.Size(257, 24)
    Me.Uc_combo3.SufixText = "Sufix Text"
    Me.Uc_combo3.SufixTextVisible = True
    Me.Uc_combo3.TabIndex = 3
    Me.Uc_combo3.TextCombo = Nothing
    Me.Uc_combo3.TextVisible = False
    Me.Uc_combo3.TextWidth = 117
    '
    'Uc_entry_field5
    '
    Me.Uc_entry_field5.DoubleValue = 0.0R
    Me.Uc_entry_field5.IntegerValue = 0
    Me.Uc_entry_field5.IsReadOnly = False
    Me.Uc_entry_field5.Location = New System.Drawing.Point(7, 79)
    Me.Uc_entry_field5.Name = "Uc_entry_field5"
    Me.Uc_entry_field5.OnlyUpperCase = True
    Me.Uc_entry_field5.PlaceHolder = Nothing
    Me.Uc_entry_field5.Size = New System.Drawing.Size(521, 24)
    Me.Uc_entry_field5.SufixText = "Sufix Text"
    Me.Uc_entry_field5.SufixTextVisible = True
    Me.Uc_entry_field5.TabIndex = 2
    Me.Uc_entry_field5.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.Uc_entry_field5.TextValue = ""
    Me.Uc_entry_field5.TextVisible = False
    Me.Uc_entry_field5.TextWidth = 117
    Me.Uc_entry_field5.Value = ""
    Me.Uc_entry_field5.ValueForeColor = System.Drawing.Color.Blue
    '
    'Uc_entry_field6
    '
    Me.Uc_entry_field6.DoubleValue = 0.0R
    Me.Uc_entry_field6.IntegerValue = 0
    Me.Uc_entry_field6.IsReadOnly = False
    Me.Uc_entry_field6.Location = New System.Drawing.Point(7, 49)
    Me.Uc_entry_field6.Name = "Uc_entry_field6"
    Me.Uc_entry_field6.OnlyUpperCase = True
    Me.Uc_entry_field6.PlaceHolder = Nothing
    Me.Uc_entry_field6.Size = New System.Drawing.Size(521, 24)
    Me.Uc_entry_field6.SufixText = "Sufix Text"
    Me.Uc_entry_field6.SufixTextVisible = True
    Me.Uc_entry_field6.TabIndex = 1
    Me.Uc_entry_field6.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.Uc_entry_field6.TextValue = ""
    Me.Uc_entry_field6.TextVisible = False
    Me.Uc_entry_field6.TextWidth = 117
    Me.Uc_entry_field6.Value = ""
    Me.Uc_entry_field6.ValueForeColor = System.Drawing.Color.Blue
    '
    'Uc_entry_field7
    '
    Me.Uc_entry_field7.DoubleValue = 0.0R
    Me.Uc_entry_field7.IntegerValue = 0
    Me.Uc_entry_field7.IsReadOnly = False
    Me.Uc_entry_field7.Location = New System.Drawing.Point(7, 19)
    Me.Uc_entry_field7.Name = "Uc_entry_field7"
    Me.Uc_entry_field7.OnlyUpperCase = True
    Me.Uc_entry_field7.PlaceHolder = Nothing
    Me.Uc_entry_field7.Size = New System.Drawing.Size(521, 24)
    Me.Uc_entry_field7.SufixText = "Sufix Text"
    Me.Uc_entry_field7.SufixTextVisible = True
    Me.Uc_entry_field7.TabIndex = 0
    Me.Uc_entry_field7.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.Uc_entry_field7.TextValue = ""
    Me.Uc_entry_field7.TextVisible = False
    Me.Uc_entry_field7.TextWidth = 117
    Me.Uc_entry_field7.Value = ""
    Me.Uc_entry_field7.ValueForeColor = System.Drawing.Color.Blue
    '
    'TabPage2
    '
    Me.TabPage2.Controls.Add(Me.GroupBox2)
    Me.TabPage2.Location = New System.Drawing.Point(4, 22)
    Me.TabPage2.Name = "TabPage2"
    Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
    Me.TabPage2.Size = New System.Drawing.Size(553, 245)
    Me.TabPage2.TabIndex = 1
    Me.TabPage2.Text = "xContacto"
    Me.TabPage2.UseVisualStyleBackColor = True
    '
    'GroupBox2
    '
    Me.GroupBox2.Controls.Add(Me.Uc_entry_field8)
    Me.GroupBox2.Controls.Add(Me.Uc_entry_field9)
    Me.GroupBox2.Controls.Add(Me.Uc_entry_field10)
    Me.GroupBox2.Controls.Add(Me.Uc_entry_field11)
    Me.GroupBox2.Controls.Add(Me.Uc_entry_field12)
    Me.GroupBox2.Dock = System.Windows.Forms.DockStyle.Fill
    Me.GroupBox2.Location = New System.Drawing.Point(3, 3)
    Me.GroupBox2.Name = "GroupBox2"
    Me.GroupBox2.Size = New System.Drawing.Size(547, 239)
    Me.GroupBox2.TabIndex = 1
    Me.GroupBox2.TabStop = False
    '
    'Uc_entry_field8
    '
    Me.Uc_entry_field8.DoubleValue = 0.0R
    Me.Uc_entry_field8.IntegerValue = 0
    Me.Uc_entry_field8.IsReadOnly = False
    Me.Uc_entry_field8.Location = New System.Drawing.Point(7, 139)
    Me.Uc_entry_field8.Name = "Uc_entry_field8"
    Me.Uc_entry_field8.PlaceHolder = Nothing
    Me.Uc_entry_field8.Size = New System.Drawing.Size(521, 24)
    Me.Uc_entry_field8.SufixText = "Sufix Text"
    Me.Uc_entry_field8.SufixTextVisible = True
    Me.Uc_entry_field8.TabIndex = 4
    Me.Uc_entry_field8.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.Uc_entry_field8.TextValue = ""
    Me.Uc_entry_field8.TextVisible = False
    Me.Uc_entry_field8.TextWidth = 117
    Me.Uc_entry_field8.Value = ""
    Me.Uc_entry_field8.ValueForeColor = System.Drawing.Color.Blue
    '
    'Uc_entry_field9
    '
    Me.Uc_entry_field9.DoubleValue = 0.0R
    Me.Uc_entry_field9.IntegerValue = 0
    Me.Uc_entry_field9.IsReadOnly = False
    Me.Uc_entry_field9.Location = New System.Drawing.Point(7, 19)
    Me.Uc_entry_field9.Name = "Uc_entry_field9"
    Me.Uc_entry_field9.PlaceHolder = Nothing
    Me.Uc_entry_field9.Size = New System.Drawing.Size(362, 24)
    Me.Uc_entry_field9.SufixText = "Sufix Text"
    Me.Uc_entry_field9.SufixTextVisible = True
    Me.Uc_entry_field9.TabIndex = 0
    Me.Uc_entry_field9.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.Uc_entry_field9.TextValue = ""
    Me.Uc_entry_field9.TextVisible = False
    Me.Uc_entry_field9.TextWidth = 117
    Me.Uc_entry_field9.Value = ""
    Me.Uc_entry_field9.ValueForeColor = System.Drawing.Color.Blue
    '
    'Uc_entry_field10
    '
    Me.Uc_entry_field10.DoubleValue = 0.0R
    Me.Uc_entry_field10.IntegerValue = 0
    Me.Uc_entry_field10.IsReadOnly = False
    Me.Uc_entry_field10.Location = New System.Drawing.Point(7, 109)
    Me.Uc_entry_field10.Name = "Uc_entry_field10"
    Me.Uc_entry_field10.PlaceHolder = Nothing
    Me.Uc_entry_field10.Size = New System.Drawing.Size(521, 24)
    Me.Uc_entry_field10.SufixText = "Sufix Text"
    Me.Uc_entry_field10.SufixTextVisible = True
    Me.Uc_entry_field10.TabIndex = 3
    Me.Uc_entry_field10.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.Uc_entry_field10.TextValue = ""
    Me.Uc_entry_field10.TextVisible = False
    Me.Uc_entry_field10.TextWidth = 117
    Me.Uc_entry_field10.Value = ""
    Me.Uc_entry_field10.ValueForeColor = System.Drawing.Color.Blue
    '
    'Uc_entry_field11
    '
    Me.Uc_entry_field11.DoubleValue = 0.0R
    Me.Uc_entry_field11.IntegerValue = 0
    Me.Uc_entry_field11.IsReadOnly = False
    Me.Uc_entry_field11.Location = New System.Drawing.Point(7, 79)
    Me.Uc_entry_field11.Name = "Uc_entry_field11"
    Me.Uc_entry_field11.PlaceHolder = Nothing
    Me.Uc_entry_field11.Size = New System.Drawing.Size(521, 24)
    Me.Uc_entry_field11.SufixText = "Sufix Text"
    Me.Uc_entry_field11.SufixTextVisible = True
    Me.Uc_entry_field11.TabIndex = 2
    Me.Uc_entry_field11.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.Uc_entry_field11.TextValue = ""
    Me.Uc_entry_field11.TextVisible = False
    Me.Uc_entry_field11.TextWidth = 117
    Me.Uc_entry_field11.Value = ""
    Me.Uc_entry_field11.ValueForeColor = System.Drawing.Color.Blue
    '
    'Uc_entry_field12
    '
    Me.Uc_entry_field12.DoubleValue = 0.0R
    Me.Uc_entry_field12.IntegerValue = 0
    Me.Uc_entry_field12.IsReadOnly = False
    Me.Uc_entry_field12.Location = New System.Drawing.Point(7, 49)
    Me.Uc_entry_field12.Name = "Uc_entry_field12"
    Me.Uc_entry_field12.PlaceHolder = Nothing
    Me.Uc_entry_field12.Size = New System.Drawing.Size(362, 24)
    Me.Uc_entry_field12.SufixText = "Sufix Text"
    Me.Uc_entry_field12.SufixTextVisible = True
    Me.Uc_entry_field12.TabIndex = 1
    Me.Uc_entry_field12.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.Uc_entry_field12.TextValue = ""
    Me.Uc_entry_field12.TextVisible = False
    Me.Uc_entry_field12.TextWidth = 117
    Me.Uc_entry_field12.Value = ""
    Me.Uc_entry_field12.ValueForeColor = System.Drawing.Color.Blue
    '
    'TabPage3
    '
    Me.TabPage3.Controls.Add(Me.GroupBox3)
    Me.TabPage3.Location = New System.Drawing.Point(4, 22)
    Me.TabPage3.Name = "TabPage3"
    Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
    Me.TabPage3.Size = New System.Drawing.Size(553, 245)
    Me.TabPage3.TabIndex = 2
    Me.TabPage3.Text = "xDirecci�n"
    Me.TabPage3.UseVisualStyleBackColor = True
    '
    'GroupBox3
    '
    Me.GroupBox3.Controls.Add(Me.Uc_entry_field13)
    Me.GroupBox3.Controls.Add(Me.Uc_entry_field14)
    Me.GroupBox3.Controls.Add(Me.Uc_entry_field15)
    Me.GroupBox3.Controls.Add(Me.Uc_entry_field16)
    Me.GroupBox3.Controls.Add(Me.Uc_entry_field17)
    Me.GroupBox3.Dock = System.Windows.Forms.DockStyle.Fill
    Me.GroupBox3.Location = New System.Drawing.Point(3, 3)
    Me.GroupBox3.Name = "GroupBox3"
    Me.GroupBox3.Size = New System.Drawing.Size(547, 239)
    Me.GroupBox3.TabIndex = 2
    Me.GroupBox3.TabStop = False
    '
    'Uc_entry_field13
    '
    Me.Uc_entry_field13.DoubleValue = 0.0R
    Me.Uc_entry_field13.IntegerValue = 0
    Me.Uc_entry_field13.IsReadOnly = False
    Me.Uc_entry_field13.Location = New System.Drawing.Point(7, 139)
    Me.Uc_entry_field13.Name = "Uc_entry_field13"
    Me.Uc_entry_field13.PlaceHolder = Nothing
    Me.Uc_entry_field13.Size = New System.Drawing.Size(220, 24)
    Me.Uc_entry_field13.SufixText = "Sufix Text"
    Me.Uc_entry_field13.SufixTextVisible = True
    Me.Uc_entry_field13.TabIndex = 4
    Me.Uc_entry_field13.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.Uc_entry_field13.TextValue = ""
    Me.Uc_entry_field13.TextVisible = False
    Me.Uc_entry_field13.TextWidth = 117
    Me.Uc_entry_field13.Value = ""
    Me.Uc_entry_field13.ValueForeColor = System.Drawing.Color.Blue
    '
    'Uc_entry_field14
    '
    Me.Uc_entry_field14.DoubleValue = 0.0R
    Me.Uc_entry_field14.IntegerValue = 0
    Me.Uc_entry_field14.IsReadOnly = False
    Me.Uc_entry_field14.Location = New System.Drawing.Point(7, 19)
    Me.Uc_entry_field14.Name = "Uc_entry_field14"
    Me.Uc_entry_field14.PlaceHolder = Nothing
    Me.Uc_entry_field14.Size = New System.Drawing.Size(521, 24)
    Me.Uc_entry_field14.SufixText = "Sufix Text"
    Me.Uc_entry_field14.SufixTextVisible = True
    Me.Uc_entry_field14.TabIndex = 0
    Me.Uc_entry_field14.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.Uc_entry_field14.TextValue = ""
    Me.Uc_entry_field14.TextVisible = False
    Me.Uc_entry_field14.TextWidth = 117
    Me.Uc_entry_field14.Value = ""
    Me.Uc_entry_field14.ValueForeColor = System.Drawing.Color.Blue
    '
    'Uc_entry_field15
    '
    Me.Uc_entry_field15.DoubleValue = 0.0R
    Me.Uc_entry_field15.IntegerValue = 0
    Me.Uc_entry_field15.IsReadOnly = False
    Me.Uc_entry_field15.Location = New System.Drawing.Point(7, 109)
    Me.Uc_entry_field15.Name = "Uc_entry_field15"
    Me.Uc_entry_field15.PlaceHolder = Nothing
    Me.Uc_entry_field15.Size = New System.Drawing.Size(362, 24)
    Me.Uc_entry_field15.SufixText = "Sufix Text"
    Me.Uc_entry_field15.SufixTextVisible = True
    Me.Uc_entry_field15.TabIndex = 3
    Me.Uc_entry_field15.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.Uc_entry_field15.TextValue = ""
    Me.Uc_entry_field15.TextVisible = False
    Me.Uc_entry_field15.TextWidth = 117
    Me.Uc_entry_field15.Value = ""
    Me.Uc_entry_field15.ValueForeColor = System.Drawing.Color.Blue
    '
    'Uc_entry_field16
    '
    Me.Uc_entry_field16.DoubleValue = 0.0R
    Me.Uc_entry_field16.IntegerValue = 0
    Me.Uc_entry_field16.IsReadOnly = False
    Me.Uc_entry_field16.Location = New System.Drawing.Point(7, 79)
    Me.Uc_entry_field16.Name = "Uc_entry_field16"
    Me.Uc_entry_field16.PlaceHolder = Nothing
    Me.Uc_entry_field16.Size = New System.Drawing.Size(362, 24)
    Me.Uc_entry_field16.SufixText = "Sufix Text"
    Me.Uc_entry_field16.SufixTextVisible = True
    Me.Uc_entry_field16.TabIndex = 2
    Me.Uc_entry_field16.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.Uc_entry_field16.TextValue = ""
    Me.Uc_entry_field16.TextVisible = False
    Me.Uc_entry_field16.TextWidth = 117
    Me.Uc_entry_field16.Value = ""
    Me.Uc_entry_field16.ValueForeColor = System.Drawing.Color.Blue
    '
    'Uc_entry_field17
    '
    Me.Uc_entry_field17.DoubleValue = 0.0R
    Me.Uc_entry_field17.IntegerValue = 0
    Me.Uc_entry_field17.IsReadOnly = False
    Me.Uc_entry_field17.Location = New System.Drawing.Point(7, 49)
    Me.Uc_entry_field17.Name = "Uc_entry_field17"
    Me.Uc_entry_field17.PlaceHolder = Nothing
    Me.Uc_entry_field17.Size = New System.Drawing.Size(521, 24)
    Me.Uc_entry_field17.SufixText = "Sufix Text"
    Me.Uc_entry_field17.SufixTextVisible = True
    Me.Uc_entry_field17.TabIndex = 1
    Me.Uc_entry_field17.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.Uc_entry_field17.TextValue = ""
    Me.Uc_entry_field17.TextVisible = False
    Me.Uc_entry_field17.TextWidth = 117
    Me.Uc_entry_field17.Value = ""
    Me.Uc_entry_field17.ValueForeColor = System.Drawing.Color.Blue
    '
    'TabPage4
    '
    Me.TabPage4.Controls.Add(Me.GroupBox4)
    Me.TabPage4.Location = New System.Drawing.Point(4, 22)
    Me.TabPage4.Name = "TabPage4"
    Me.TabPage4.Padding = New System.Windows.Forms.Padding(3)
    Me.TabPage4.Size = New System.Drawing.Size(553, 245)
    Me.TabPage4.TabIndex = 5
    Me.TabPage4.Text = "xBloqueo"
    Me.TabPage4.UseVisualStyleBackColor = True
    '
    'GroupBox4
    '
    Me.GroupBox4.Controls.Add(Me.TextBox1)
    Me.GroupBox4.Controls.Add(Me.Label1)
    Me.GroupBox4.Controls.Add(Me.Label2)
    Me.GroupBox4.Dock = System.Windows.Forms.DockStyle.Fill
    Me.GroupBox4.Location = New System.Drawing.Point(3, 3)
    Me.GroupBox4.Name = "GroupBox4"
    Me.GroupBox4.Size = New System.Drawing.Size(547, 239)
    Me.GroupBox4.TabIndex = 4
    Me.GroupBox4.TabStop = False
    '
    'TextBox1
    '
    Me.TextBox1.Location = New System.Drawing.Point(24, 63)
    Me.TextBox1.Multiline = True
    Me.TextBox1.Name = "TextBox1"
    Me.TextBox1.ReadOnly = True
    Me.TextBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
    Me.TextBox1.Size = New System.Drawing.Size(516, 166)
    Me.TextBox1.TabIndex = 8
    '
    'Label1
    '
    Me.Label1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label1.Location = New System.Drawing.Point(27, 48)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(508, 13)
    Me.Label1.TabIndex = 7
    Me.Label1.Text = "xMotivoBloqueo"
    '
    'Label2
    '
    Me.Label2.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label2.Location = New System.Drawing.Point(28, 17)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(508, 16)
    Me.Label2.TabIndex = 6
    Me.Label2.Text = "xBloqueo"
    '
    'TabPage5
    '
    Me.TabPage5.Controls.Add(Me.GroupBox5)
    Me.TabPage5.Location = New System.Drawing.Point(4, 22)
    Me.TabPage5.Name = "TabPage5"
    Me.TabPage5.Padding = New System.Windows.Forms.Padding(3)
    Me.TabPage5.Size = New System.Drawing.Size(553, 245)
    Me.TabPage5.TabIndex = 3
    Me.TabPage5.Text = "xComentarios"
    Me.TabPage5.UseVisualStyleBackColor = True
    '
    'GroupBox5
    '
    Me.GroupBox5.Controls.Add(Me.TextBox2)
    Me.GroupBox5.Dock = System.Windows.Forms.DockStyle.Fill
    Me.GroupBox5.Location = New System.Drawing.Point(3, 3)
    Me.GroupBox5.Name = "GroupBox5"
    Me.GroupBox5.Size = New System.Drawing.Size(547, 239)
    Me.GroupBox5.TabIndex = 3
    Me.GroupBox5.TabStop = False
    '
    'TextBox2
    '
    Me.TextBox2.Dock = System.Windows.Forms.DockStyle.Fill
    Me.TextBox2.Location = New System.Drawing.Point(3, 16)
    Me.TextBox2.Multiline = True
    Me.TextBox2.Name = "TextBox2"
    Me.TextBox2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
    Me.TextBox2.Size = New System.Drawing.Size(541, 220)
    Me.TextBox2.TabIndex = 0
    '
    'TabPage6
    '
    Me.TabPage6.Controls.Add(Me.GroupBox6)
    Me.TabPage6.Location = New System.Drawing.Point(4, 22)
    Me.TabPage6.Name = "TabPage6"
    Me.TabPage6.Padding = New System.Windows.Forms.Padding(3)
    Me.TabPage6.Size = New System.Drawing.Size(553, 245)
    Me.TabPage6.TabIndex = 4
    Me.TabPage6.Text = "PIN"
    Me.TabPage6.UseVisualStyleBackColor = True
    '
    'GroupBox6
    '
    Me.GroupBox6.Controls.Add(Me.Uc_button2)
    Me.GroupBox6.Controls.Add(Me.Uc_entry_field18)
    Me.GroupBox6.Controls.Add(Me.Uc_entry_field19)
    Me.GroupBox6.Dock = System.Windows.Forms.DockStyle.Fill
    Me.GroupBox6.Location = New System.Drawing.Point(3, 3)
    Me.GroupBox6.Name = "GroupBox6"
    Me.GroupBox6.Size = New System.Drawing.Size(547, 239)
    Me.GroupBox6.TabIndex = 3
    Me.GroupBox6.TabStop = False
    '
    'Uc_button2
    '
    Me.Uc_button2.DialogResult = System.Windows.Forms.DialogResult.None
    Me.Uc_button2.Location = New System.Drawing.Point(126, 91)
    Me.Uc_button2.Name = "Uc_button2"
    Me.Uc_button2.Size = New System.Drawing.Size(90, 30)
    Me.Uc_button2.TabIndex = 2
    Me.Uc_button2.ToolTipped = False
    Me.Uc_button2.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'Uc_entry_field18
    '
    Me.Uc_entry_field18.DoubleValue = 0.0R
    Me.Uc_entry_field18.IntegerValue = 0
    Me.Uc_entry_field18.IsReadOnly = False
    Me.Uc_entry_field18.Location = New System.Drawing.Point(7, 31)
    Me.Uc_entry_field18.Name = "Uc_entry_field18"
    Me.Uc_entry_field18.PlaceHolder = Nothing
    Me.Uc_entry_field18.Size = New System.Drawing.Size(213, 24)
    Me.Uc_entry_field18.SufixText = "Sufix Text"
    Me.Uc_entry_field18.SufixTextVisible = True
    Me.Uc_entry_field18.TabIndex = 0
    Me.Uc_entry_field18.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.Uc_entry_field18.TextValue = ""
    Me.Uc_entry_field18.TextVisible = False
    Me.Uc_entry_field18.TextWidth = 117
    Me.Uc_entry_field18.Value = ""
    Me.Uc_entry_field18.ValueForeColor = System.Drawing.Color.Blue
    '
    'Uc_entry_field19
    '
    Me.Uc_entry_field19.DoubleValue = 0.0R
    Me.Uc_entry_field19.IntegerValue = 0
    Me.Uc_entry_field19.IsReadOnly = False
    Me.Uc_entry_field19.Location = New System.Drawing.Point(7, 61)
    Me.Uc_entry_field19.Name = "Uc_entry_field19"
    Me.Uc_entry_field19.PlaceHolder = Nothing
    Me.Uc_entry_field19.Size = New System.Drawing.Size(213, 24)
    Me.Uc_entry_field19.SufixText = "Sufix Text"
    Me.Uc_entry_field19.SufixTextVisible = True
    Me.Uc_entry_field19.TabIndex = 1
    Me.Uc_entry_field19.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.Uc_entry_field19.TextValue = ""
    Me.Uc_entry_field19.TextVisible = False
    Me.Uc_entry_field19.TextWidth = 117
    Me.Uc_entry_field19.Value = ""
    Me.Uc_entry_field19.ValueForeColor = System.Drawing.Color.Blue
    '
    'TabPage7
    '
    Me.TabPage7.Controls.Add(Me.GroupBox7)
    Me.TabPage7.Location = New System.Drawing.Point(4, 22)
    Me.TabPage7.Name = "TabPage7"
    Me.TabPage7.Padding = New System.Windows.Forms.Padding(3)
    Me.TabPage7.Size = New System.Drawing.Size(553, 245)
    Me.TabPage7.TabIndex = 0
    Me.TabPage7.Text = "xPrincipal"
    Me.TabPage7.UseVisualStyleBackColor = True
    '
    'GroupBox7
    '
    Me.GroupBox7.Controls.Add(Me.Uc_entry_field20)
    Me.GroupBox7.Controls.Add(Me.Uc_date_picker3)
    Me.GroupBox7.Controls.Add(Me.Uc_date_picker4)
    Me.GroupBox7.Controls.Add(Me.Uc_combo4)
    Me.GroupBox7.Controls.Add(Me.Uc_entry_field21)
    Me.GroupBox7.Controls.Add(Me.Uc_combo5)
    Me.GroupBox7.Controls.Add(Me.Uc_combo6)
    Me.GroupBox7.Controls.Add(Me.Uc_entry_field22)
    Me.GroupBox7.Controls.Add(Me.Uc_entry_field23)
    Me.GroupBox7.Controls.Add(Me.Uc_entry_field24)
    Me.GroupBox7.Dock = System.Windows.Forms.DockStyle.Fill
    Me.GroupBox7.Location = New System.Drawing.Point(3, 3)
    Me.GroupBox7.Name = "GroupBox7"
    Me.GroupBox7.Size = New System.Drawing.Size(547, 239)
    Me.GroupBox7.TabIndex = 0
    Me.GroupBox7.TabStop = False
    '
    'Uc_entry_field20
    '
    Me.Uc_entry_field20.DoubleValue = 0.0R
    Me.Uc_entry_field20.IntegerValue = 0
    Me.Uc_entry_field20.IsReadOnly = False
    Me.Uc_entry_field20.Location = New System.Drawing.Point(271, 169)
    Me.Uc_entry_field20.Name = "Uc_entry_field20"
    Me.Uc_entry_field20.OnlyUpperCase = True
    Me.Uc_entry_field20.PlaceHolder = Nothing
    Me.Uc_entry_field20.Size = New System.Drawing.Size(257, 24)
    Me.Uc_entry_field20.SufixText = "Sufix Text"
    Me.Uc_entry_field20.SufixTextVisible = True
    Me.Uc_entry_field20.TabIndex = 9
    Me.Uc_entry_field20.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.Uc_entry_field20.TextValue = ""
    Me.Uc_entry_field20.TextVisible = False
    Me.Uc_entry_field20.TextWidth = 93
    Me.Uc_entry_field20.Value = ""
    Me.Uc_entry_field20.ValueForeColor = System.Drawing.Color.Blue
    '
    'Uc_date_picker3
    '
    Me.Uc_date_picker3.Checked = True
    Me.Uc_date_picker3.IsReadOnly = False
    Me.Uc_date_picker3.Location = New System.Drawing.Point(7, 199)
    Me.Uc_date_picker3.Name = "Uc_date_picker3"
    Me.Uc_date_picker3.ShowCheckBox = True
    Me.Uc_date_picker3.ShowUpDown = False
    Me.Uc_date_picker3.Size = New System.Drawing.Size(257, 24)
    Me.Uc_date_picker3.SufixText = "Sufix Text"
    Me.Uc_date_picker3.SufixTextVisible = True
    Me.Uc_date_picker3.TabIndex = 8
    Me.Uc_date_picker3.TextVisible = False
    Me.Uc_date_picker3.TextWidth = 117
    Me.Uc_date_picker3.Value = New Date(2013, 3, 1, 0, 0, 0, 0)
    '
    'Uc_date_picker4
    '
    Me.Uc_date_picker4.Checked = True
    Me.Uc_date_picker4.IsReadOnly = False
    Me.Uc_date_picker4.Location = New System.Drawing.Point(7, 169)
    Me.Uc_date_picker4.Name = "Uc_date_picker4"
    Me.Uc_date_picker4.ShowCheckBox = False
    Me.Uc_date_picker4.ShowUpDown = False
    Me.Uc_date_picker4.Size = New System.Drawing.Size(257, 24)
    Me.Uc_date_picker4.SufixText = "Sufix Text"
    Me.Uc_date_picker4.SufixTextVisible = True
    Me.Uc_date_picker4.TabIndex = 7
    Me.Uc_date_picker4.TextVisible = False
    Me.Uc_date_picker4.TextWidth = 117
    Me.Uc_date_picker4.Value = New Date(2013, 3, 1, 0, 0, 0, 0)
    '
    'Uc_combo4
    '
    Me.Uc_combo4.AllowUnlistedValues = False
    Me.Uc_combo4.AutoCompleteMode = False
    Me.Uc_combo4.IsReadOnly = False
    Me.Uc_combo4.Location = New System.Drawing.Point(271, 139)
    Me.Uc_combo4.Name = "Uc_combo4"
    Me.Uc_combo4.SelectedIndex = -1
    Me.Uc_combo4.Size = New System.Drawing.Size(257, 24)
    Me.Uc_combo4.SufixText = "Sufix Text"
    Me.Uc_combo4.SufixTextVisible = True
    Me.Uc_combo4.TabIndex = 6
    Me.Uc_combo4.TextCombo = Nothing
    Me.Uc_combo4.TextVisible = False
    Me.Uc_combo4.TextWidth = 93
    '
    'Uc_entry_field21
    '
    Me.Uc_entry_field21.DoubleValue = 0.0R
    Me.Uc_entry_field21.IntegerValue = 0
    Me.Uc_entry_field21.IsReadOnly = False
    Me.Uc_entry_field21.Location = New System.Drawing.Point(271, 109)
    Me.Uc_entry_field21.Name = "Uc_entry_field21"
    Me.Uc_entry_field21.OnlyUpperCase = True
    Me.Uc_entry_field21.PlaceHolder = Nothing
    Me.Uc_entry_field21.Size = New System.Drawing.Size(257, 24)
    Me.Uc_entry_field21.SufixText = "Sufix Text"
    Me.Uc_entry_field21.SufixTextVisible = True
    Me.Uc_entry_field21.TabIndex = 4
    Me.Uc_entry_field21.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.Uc_entry_field21.TextValue = ""
    Me.Uc_entry_field21.TextVisible = False
    Me.Uc_entry_field21.TextWidth = 93
    Me.Uc_entry_field21.Value = ""
    Me.Uc_entry_field21.ValueForeColor = System.Drawing.Color.Blue
    '
    'Uc_combo5
    '
    Me.Uc_combo5.AllowUnlistedValues = False
    Me.Uc_combo5.AutoCompleteMode = False
    Me.Uc_combo5.IsReadOnly = False
    Me.Uc_combo5.Location = New System.Drawing.Point(7, 139)
    Me.Uc_combo5.Name = "Uc_combo5"
    Me.Uc_combo5.SelectedIndex = -1
    Me.Uc_combo5.Size = New System.Drawing.Size(257, 24)
    Me.Uc_combo5.SufixText = "Sufix Text"
    Me.Uc_combo5.SufixTextVisible = True
    Me.Uc_combo5.TabIndex = 5
    Me.Uc_combo5.TextCombo = Nothing
    Me.Uc_combo5.TextVisible = False
    Me.Uc_combo5.TextWidth = 117
    '
    'Uc_combo6
    '
    Me.Uc_combo6.AllowUnlistedValues = False
    Me.Uc_combo6.AutoCompleteMode = False
    Me.Uc_combo6.IsReadOnly = False
    Me.Uc_combo6.Location = New System.Drawing.Point(7, 109)
    Me.Uc_combo6.Name = "Uc_combo6"
    Me.Uc_combo6.SelectedIndex = -1
    Me.Uc_combo6.Size = New System.Drawing.Size(257, 24)
    Me.Uc_combo6.SufixText = "Sufix Text"
    Me.Uc_combo6.SufixTextVisible = True
    Me.Uc_combo6.TabIndex = 3
    Me.Uc_combo6.TextCombo = Nothing
    Me.Uc_combo6.TextVisible = False
    Me.Uc_combo6.TextWidth = 117
    '
    'Uc_entry_field22
    '
    Me.Uc_entry_field22.DoubleValue = 0.0R
    Me.Uc_entry_field22.IntegerValue = 0
    Me.Uc_entry_field22.IsReadOnly = False
    Me.Uc_entry_field22.Location = New System.Drawing.Point(7, 79)
    Me.Uc_entry_field22.Name = "Uc_entry_field22"
    Me.Uc_entry_field22.OnlyUpperCase = True
    Me.Uc_entry_field22.PlaceHolder = Nothing
    Me.Uc_entry_field22.Size = New System.Drawing.Size(521, 24)
    Me.Uc_entry_field22.SufixText = "Sufix Text"
    Me.Uc_entry_field22.SufixTextVisible = True
    Me.Uc_entry_field22.TabIndex = 2
    Me.Uc_entry_field22.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.Uc_entry_field22.TextValue = ""
    Me.Uc_entry_field22.TextVisible = False
    Me.Uc_entry_field22.TextWidth = 117
    Me.Uc_entry_field22.Value = ""
    Me.Uc_entry_field22.ValueForeColor = System.Drawing.Color.Blue
    '
    'Uc_entry_field23
    '
    Me.Uc_entry_field23.DoubleValue = 0.0R
    Me.Uc_entry_field23.IntegerValue = 0
    Me.Uc_entry_field23.IsReadOnly = False
    Me.Uc_entry_field23.Location = New System.Drawing.Point(7, 49)
    Me.Uc_entry_field23.Name = "Uc_entry_field23"
    Me.Uc_entry_field23.OnlyUpperCase = True
    Me.Uc_entry_field23.PlaceHolder = Nothing
    Me.Uc_entry_field23.Size = New System.Drawing.Size(521, 24)
    Me.Uc_entry_field23.SufixText = "Sufix Text"
    Me.Uc_entry_field23.SufixTextVisible = True
    Me.Uc_entry_field23.TabIndex = 1
    Me.Uc_entry_field23.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.Uc_entry_field23.TextValue = ""
    Me.Uc_entry_field23.TextVisible = False
    Me.Uc_entry_field23.TextWidth = 117
    Me.Uc_entry_field23.Value = ""
    Me.Uc_entry_field23.ValueForeColor = System.Drawing.Color.Blue
    '
    'Uc_entry_field24
    '
    Me.Uc_entry_field24.DoubleValue = 0.0R
    Me.Uc_entry_field24.IntegerValue = 0
    Me.Uc_entry_field24.IsReadOnly = False
    Me.Uc_entry_field24.Location = New System.Drawing.Point(7, 19)
    Me.Uc_entry_field24.Name = "Uc_entry_field24"
    Me.Uc_entry_field24.OnlyUpperCase = True
    Me.Uc_entry_field24.PlaceHolder = Nothing
    Me.Uc_entry_field24.Size = New System.Drawing.Size(521, 24)
    Me.Uc_entry_field24.SufixText = "Sufix Text"
    Me.Uc_entry_field24.SufixTextVisible = True
    Me.Uc_entry_field24.TabIndex = 0
    Me.Uc_entry_field24.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.Uc_entry_field24.TextValue = ""
    Me.Uc_entry_field24.TextVisible = False
    Me.Uc_entry_field24.TextWidth = 117
    Me.Uc_entry_field24.Value = ""
    Me.Uc_entry_field24.ValueForeColor = System.Drawing.Color.Blue
    '
    'TabPage8
    '
    Me.TabPage8.Controls.Add(Me.GroupBox8)
    Me.TabPage8.Location = New System.Drawing.Point(4, 22)
    Me.TabPage8.Name = "TabPage8"
    Me.TabPage8.Padding = New System.Windows.Forms.Padding(3)
    Me.TabPage8.Size = New System.Drawing.Size(553, 245)
    Me.TabPage8.TabIndex = 1
    Me.TabPage8.Text = "xContacto"
    Me.TabPage8.UseVisualStyleBackColor = True
    '
    'GroupBox8
    '
    Me.GroupBox8.Controls.Add(Me.Uc_entry_field25)
    Me.GroupBox8.Controls.Add(Me.Uc_entry_field26)
    Me.GroupBox8.Controls.Add(Me.Uc_entry_field27)
    Me.GroupBox8.Controls.Add(Me.Uc_entry_field28)
    Me.GroupBox8.Controls.Add(Me.Uc_entry_field29)
    Me.GroupBox8.Dock = System.Windows.Forms.DockStyle.Fill
    Me.GroupBox8.Location = New System.Drawing.Point(3, 3)
    Me.GroupBox8.Name = "GroupBox8"
    Me.GroupBox8.Size = New System.Drawing.Size(547, 239)
    Me.GroupBox8.TabIndex = 1
    Me.GroupBox8.TabStop = False
    '
    'Uc_entry_field25
    '
    Me.Uc_entry_field25.DoubleValue = 0.0R
    Me.Uc_entry_field25.IntegerValue = 0
    Me.Uc_entry_field25.IsReadOnly = False
    Me.Uc_entry_field25.Location = New System.Drawing.Point(7, 139)
    Me.Uc_entry_field25.Name = "Uc_entry_field25"
    Me.Uc_entry_field25.PlaceHolder = Nothing
    Me.Uc_entry_field25.Size = New System.Drawing.Size(521, 24)
    Me.Uc_entry_field25.SufixText = "Sufix Text"
    Me.Uc_entry_field25.SufixTextVisible = True
    Me.Uc_entry_field25.TabIndex = 4
    Me.Uc_entry_field25.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.Uc_entry_field25.TextValue = ""
    Me.Uc_entry_field25.TextVisible = False
    Me.Uc_entry_field25.TextWidth = 117
    Me.Uc_entry_field25.Value = ""
    Me.Uc_entry_field25.ValueForeColor = System.Drawing.Color.Blue
    '
    'Uc_entry_field26
    '
    Me.Uc_entry_field26.DoubleValue = 0.0R
    Me.Uc_entry_field26.IntegerValue = 0
    Me.Uc_entry_field26.IsReadOnly = False
    Me.Uc_entry_field26.Location = New System.Drawing.Point(7, 19)
    Me.Uc_entry_field26.Name = "Uc_entry_field26"
    Me.Uc_entry_field26.PlaceHolder = Nothing
    Me.Uc_entry_field26.Size = New System.Drawing.Size(362, 24)
    Me.Uc_entry_field26.SufixText = "Sufix Text"
    Me.Uc_entry_field26.SufixTextVisible = True
    Me.Uc_entry_field26.TabIndex = 0
    Me.Uc_entry_field26.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.Uc_entry_field26.TextValue = ""
    Me.Uc_entry_field26.TextVisible = False
    Me.Uc_entry_field26.TextWidth = 117
    Me.Uc_entry_field26.Value = ""
    Me.Uc_entry_field26.ValueForeColor = System.Drawing.Color.Blue
    '
    'Uc_entry_field27
    '
    Me.Uc_entry_field27.DoubleValue = 0.0R
    Me.Uc_entry_field27.IntegerValue = 0
    Me.Uc_entry_field27.IsReadOnly = False
    Me.Uc_entry_field27.Location = New System.Drawing.Point(7, 109)
    Me.Uc_entry_field27.Name = "Uc_entry_field27"
    Me.Uc_entry_field27.PlaceHolder = Nothing
    Me.Uc_entry_field27.Size = New System.Drawing.Size(521, 24)
    Me.Uc_entry_field27.SufixText = "Sufix Text"
    Me.Uc_entry_field27.SufixTextVisible = True
    Me.Uc_entry_field27.TabIndex = 3
    Me.Uc_entry_field27.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.Uc_entry_field27.TextValue = ""
    Me.Uc_entry_field27.TextVisible = False
    Me.Uc_entry_field27.TextWidth = 117
    Me.Uc_entry_field27.Value = ""
    Me.Uc_entry_field27.ValueForeColor = System.Drawing.Color.Blue
    '
    'Uc_entry_field28
    '
    Me.Uc_entry_field28.DoubleValue = 0.0R
    Me.Uc_entry_field28.IntegerValue = 0
    Me.Uc_entry_field28.IsReadOnly = False
    Me.Uc_entry_field28.Location = New System.Drawing.Point(7, 79)
    Me.Uc_entry_field28.Name = "Uc_entry_field28"
    Me.Uc_entry_field28.PlaceHolder = Nothing
    Me.Uc_entry_field28.Size = New System.Drawing.Size(521, 24)
    Me.Uc_entry_field28.SufixText = "Sufix Text"
    Me.Uc_entry_field28.SufixTextVisible = True
    Me.Uc_entry_field28.TabIndex = 2
    Me.Uc_entry_field28.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.Uc_entry_field28.TextValue = ""
    Me.Uc_entry_field28.TextVisible = False
    Me.Uc_entry_field28.TextWidth = 117
    Me.Uc_entry_field28.Value = ""
    Me.Uc_entry_field28.ValueForeColor = System.Drawing.Color.Blue
    '
    'Uc_entry_field29
    '
    Me.Uc_entry_field29.DoubleValue = 0.0R
    Me.Uc_entry_field29.IntegerValue = 0
    Me.Uc_entry_field29.IsReadOnly = False
    Me.Uc_entry_field29.Location = New System.Drawing.Point(7, 49)
    Me.Uc_entry_field29.Name = "Uc_entry_field29"
    Me.Uc_entry_field29.PlaceHolder = Nothing
    Me.Uc_entry_field29.Size = New System.Drawing.Size(362, 24)
    Me.Uc_entry_field29.SufixText = "Sufix Text"
    Me.Uc_entry_field29.SufixTextVisible = True
    Me.Uc_entry_field29.TabIndex = 1
    Me.Uc_entry_field29.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.Uc_entry_field29.TextValue = ""
    Me.Uc_entry_field29.TextVisible = False
    Me.Uc_entry_field29.TextWidth = 117
    Me.Uc_entry_field29.Value = ""
    Me.Uc_entry_field29.ValueForeColor = System.Drawing.Color.Blue
    '
    'TabPage9
    '
    Me.TabPage9.Controls.Add(Me.GroupBox9)
    Me.TabPage9.Location = New System.Drawing.Point(4, 22)
    Me.TabPage9.Name = "TabPage9"
    Me.TabPage9.Padding = New System.Windows.Forms.Padding(3)
    Me.TabPage9.Size = New System.Drawing.Size(553, 245)
    Me.TabPage9.TabIndex = 2
    Me.TabPage9.Text = "xDirecci�n"
    Me.TabPage9.UseVisualStyleBackColor = True
    '
    'GroupBox9
    '
    Me.GroupBox9.Controls.Add(Me.Uc_entry_field30)
    Me.GroupBox9.Controls.Add(Me.Uc_entry_field31)
    Me.GroupBox9.Controls.Add(Me.Uc_entry_field32)
    Me.GroupBox9.Controls.Add(Me.Uc_entry_field33)
    Me.GroupBox9.Controls.Add(Me.Uc_entry_field34)
    Me.GroupBox9.Dock = System.Windows.Forms.DockStyle.Fill
    Me.GroupBox9.Location = New System.Drawing.Point(3, 3)
    Me.GroupBox9.Name = "GroupBox9"
    Me.GroupBox9.Size = New System.Drawing.Size(547, 239)
    Me.GroupBox9.TabIndex = 2
    Me.GroupBox9.TabStop = False
    '
    'Uc_entry_field30
    '
    Me.Uc_entry_field30.DoubleValue = 0.0R
    Me.Uc_entry_field30.IntegerValue = 0
    Me.Uc_entry_field30.IsReadOnly = False
    Me.Uc_entry_field30.Location = New System.Drawing.Point(7, 139)
    Me.Uc_entry_field30.Name = "Uc_entry_field30"
    Me.Uc_entry_field30.PlaceHolder = Nothing
    Me.Uc_entry_field30.Size = New System.Drawing.Size(220, 24)
    Me.Uc_entry_field30.SufixText = "Sufix Text"
    Me.Uc_entry_field30.SufixTextVisible = True
    Me.Uc_entry_field30.TabIndex = 4
    Me.Uc_entry_field30.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.Uc_entry_field30.TextValue = ""
    Me.Uc_entry_field30.TextVisible = False
    Me.Uc_entry_field30.TextWidth = 117
    Me.Uc_entry_field30.Value = ""
    Me.Uc_entry_field30.ValueForeColor = System.Drawing.Color.Blue
    '
    'Uc_entry_field31
    '
    Me.Uc_entry_field31.DoubleValue = 0.0R
    Me.Uc_entry_field31.IntegerValue = 0
    Me.Uc_entry_field31.IsReadOnly = False
    Me.Uc_entry_field31.Location = New System.Drawing.Point(7, 19)
    Me.Uc_entry_field31.Name = "Uc_entry_field31"
    Me.Uc_entry_field31.PlaceHolder = Nothing
    Me.Uc_entry_field31.Size = New System.Drawing.Size(521, 24)
    Me.Uc_entry_field31.SufixText = "Sufix Text"
    Me.Uc_entry_field31.SufixTextVisible = True
    Me.Uc_entry_field31.TabIndex = 0
    Me.Uc_entry_field31.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.Uc_entry_field31.TextValue = ""
    Me.Uc_entry_field31.TextVisible = False
    Me.Uc_entry_field31.TextWidth = 117
    Me.Uc_entry_field31.Value = ""
    Me.Uc_entry_field31.ValueForeColor = System.Drawing.Color.Blue
    '
    'Uc_entry_field32
    '
    Me.Uc_entry_field32.DoubleValue = 0.0R
    Me.Uc_entry_field32.IntegerValue = 0
    Me.Uc_entry_field32.IsReadOnly = False
    Me.Uc_entry_field32.Location = New System.Drawing.Point(7, 109)
    Me.Uc_entry_field32.Name = "Uc_entry_field32"
    Me.Uc_entry_field32.PlaceHolder = Nothing
    Me.Uc_entry_field32.Size = New System.Drawing.Size(362, 24)
    Me.Uc_entry_field32.SufixText = "Sufix Text"
    Me.Uc_entry_field32.SufixTextVisible = True
    Me.Uc_entry_field32.TabIndex = 3
    Me.Uc_entry_field32.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.Uc_entry_field32.TextValue = ""
    Me.Uc_entry_field32.TextVisible = False
    Me.Uc_entry_field32.TextWidth = 117
    Me.Uc_entry_field32.Value = ""
    Me.Uc_entry_field32.ValueForeColor = System.Drawing.Color.Blue
    '
    'Uc_entry_field33
    '
    Me.Uc_entry_field33.DoubleValue = 0.0R
    Me.Uc_entry_field33.IntegerValue = 0
    Me.Uc_entry_field33.IsReadOnly = False
    Me.Uc_entry_field33.Location = New System.Drawing.Point(7, 79)
    Me.Uc_entry_field33.Name = "Uc_entry_field33"
    Me.Uc_entry_field33.PlaceHolder = Nothing
    Me.Uc_entry_field33.Size = New System.Drawing.Size(362, 24)
    Me.Uc_entry_field33.SufixText = "Sufix Text"
    Me.Uc_entry_field33.SufixTextVisible = True
    Me.Uc_entry_field33.TabIndex = 2
    Me.Uc_entry_field33.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.Uc_entry_field33.TextValue = ""
    Me.Uc_entry_field33.TextVisible = False
    Me.Uc_entry_field33.TextWidth = 117
    Me.Uc_entry_field33.Value = ""
    Me.Uc_entry_field33.ValueForeColor = System.Drawing.Color.Blue
    '
    'Uc_entry_field34
    '
    Me.Uc_entry_field34.DoubleValue = 0.0R
    Me.Uc_entry_field34.IntegerValue = 0
    Me.Uc_entry_field34.IsReadOnly = False
    Me.Uc_entry_field34.Location = New System.Drawing.Point(7, 49)
    Me.Uc_entry_field34.Name = "Uc_entry_field34"
    Me.Uc_entry_field34.PlaceHolder = Nothing
    Me.Uc_entry_field34.Size = New System.Drawing.Size(521, 24)
    Me.Uc_entry_field34.SufixText = "Sufix Text"
    Me.Uc_entry_field34.SufixTextVisible = True
    Me.Uc_entry_field34.TabIndex = 1
    Me.Uc_entry_field34.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.Uc_entry_field34.TextValue = ""
    Me.Uc_entry_field34.TextVisible = False
    Me.Uc_entry_field34.TextWidth = 117
    Me.Uc_entry_field34.Value = ""
    Me.Uc_entry_field34.ValueForeColor = System.Drawing.Color.Blue
    '
    'TabPage10
    '
    Me.TabPage10.Controls.Add(Me.GroupBox10)
    Me.TabPage10.Location = New System.Drawing.Point(4, 22)
    Me.TabPage10.Name = "TabPage10"
    Me.TabPage10.Padding = New System.Windows.Forms.Padding(3)
    Me.TabPage10.Size = New System.Drawing.Size(553, 245)
    Me.TabPage10.TabIndex = 5
    Me.TabPage10.Text = "xBloqueo"
    Me.TabPage10.UseVisualStyleBackColor = True
    '
    'GroupBox10
    '
    Me.GroupBox10.Controls.Add(Me.TextBox3)
    Me.GroupBox10.Controls.Add(Me.Label3)
    Me.GroupBox10.Controls.Add(Me.Label4)
    Me.GroupBox10.Dock = System.Windows.Forms.DockStyle.Fill
    Me.GroupBox10.Location = New System.Drawing.Point(3, 3)
    Me.GroupBox10.Name = "GroupBox10"
    Me.GroupBox10.Size = New System.Drawing.Size(547, 239)
    Me.GroupBox10.TabIndex = 4
    Me.GroupBox10.TabStop = False
    '
    'TextBox3
    '
    Me.TextBox3.Location = New System.Drawing.Point(24, 63)
    Me.TextBox3.Multiline = True
    Me.TextBox3.Name = "TextBox3"
    Me.TextBox3.ReadOnly = True
    Me.TextBox3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
    Me.TextBox3.Size = New System.Drawing.Size(516, 166)
    Me.TextBox3.TabIndex = 8
    '
    'Label3
    '
    Me.Label3.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label3.Location = New System.Drawing.Point(27, 48)
    Me.Label3.Name = "Label3"
    Me.Label3.Size = New System.Drawing.Size(508, 13)
    Me.Label3.TabIndex = 7
    Me.Label3.Text = "xMotivoBloqueo"
    '
    'Label4
    '
    Me.Label4.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label4.Location = New System.Drawing.Point(28, 17)
    Me.Label4.Name = "Label4"
    Me.Label4.Size = New System.Drawing.Size(508, 16)
    Me.Label4.TabIndex = 6
    Me.Label4.Text = "xBloqueo"
    '
    'TabPage11
    '
    Me.TabPage11.Controls.Add(Me.GroupBox11)
    Me.TabPage11.Location = New System.Drawing.Point(4, 22)
    Me.TabPage11.Name = "TabPage11"
    Me.TabPage11.Padding = New System.Windows.Forms.Padding(3)
    Me.TabPage11.Size = New System.Drawing.Size(553, 245)
    Me.TabPage11.TabIndex = 3
    Me.TabPage11.Text = "xComentarios"
    Me.TabPage11.UseVisualStyleBackColor = True
    '
    'GroupBox11
    '
    Me.GroupBox11.Controls.Add(Me.TextBox4)
    Me.GroupBox11.Dock = System.Windows.Forms.DockStyle.Fill
    Me.GroupBox11.Location = New System.Drawing.Point(3, 3)
    Me.GroupBox11.Name = "GroupBox11"
    Me.GroupBox11.Size = New System.Drawing.Size(547, 239)
    Me.GroupBox11.TabIndex = 3
    Me.GroupBox11.TabStop = False
    '
    'TextBox4
    '
    Me.TextBox4.Dock = System.Windows.Forms.DockStyle.Fill
    Me.TextBox4.Location = New System.Drawing.Point(3, 16)
    Me.TextBox4.Multiline = True
    Me.TextBox4.Name = "TextBox4"
    Me.TextBox4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
    Me.TextBox4.Size = New System.Drawing.Size(541, 220)
    Me.TextBox4.TabIndex = 0
    '
    'TabPage12
    '
    Me.TabPage12.Controls.Add(Me.GroupBox12)
    Me.TabPage12.Location = New System.Drawing.Point(4, 22)
    Me.TabPage12.Name = "TabPage12"
    Me.TabPage12.Padding = New System.Windows.Forms.Padding(3)
    Me.TabPage12.Size = New System.Drawing.Size(553, 245)
    Me.TabPage12.TabIndex = 4
    Me.TabPage12.Text = "PIN"
    Me.TabPage12.UseVisualStyleBackColor = True
    '
    'GroupBox12
    '
    Me.GroupBox12.Controls.Add(Me.Uc_button3)
    Me.GroupBox12.Controls.Add(Me.Uc_entry_field35)
    Me.GroupBox12.Controls.Add(Me.Uc_entry_field36)
    Me.GroupBox12.Dock = System.Windows.Forms.DockStyle.Fill
    Me.GroupBox12.Location = New System.Drawing.Point(3, 3)
    Me.GroupBox12.Name = "GroupBox12"
    Me.GroupBox12.Size = New System.Drawing.Size(547, 239)
    Me.GroupBox12.TabIndex = 3
    Me.GroupBox12.TabStop = False
    '
    'Uc_button3
    '
    Me.Uc_button3.DialogResult = System.Windows.Forms.DialogResult.None
    Me.Uc_button3.Location = New System.Drawing.Point(126, 91)
    Me.Uc_button3.Name = "Uc_button3"
    Me.Uc_button3.Size = New System.Drawing.Size(90, 30)
    Me.Uc_button3.TabIndex = 2
    Me.Uc_button3.ToolTipped = False
    Me.Uc_button3.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'Uc_entry_field35
    '
    Me.Uc_entry_field35.DoubleValue = 0.0R
    Me.Uc_entry_field35.IntegerValue = 0
    Me.Uc_entry_field35.IsReadOnly = False
    Me.Uc_entry_field35.Location = New System.Drawing.Point(7, 31)
    Me.Uc_entry_field35.Name = "Uc_entry_field35"
    Me.Uc_entry_field35.PlaceHolder = Nothing
    Me.Uc_entry_field35.Size = New System.Drawing.Size(213, 24)
    Me.Uc_entry_field35.SufixText = "Sufix Text"
    Me.Uc_entry_field35.SufixTextVisible = True
    Me.Uc_entry_field35.TabIndex = 0
    Me.Uc_entry_field35.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.Uc_entry_field35.TextValue = ""
    Me.Uc_entry_field35.TextVisible = False
    Me.Uc_entry_field35.TextWidth = 117
    Me.Uc_entry_field35.Value = ""
    Me.Uc_entry_field35.ValueForeColor = System.Drawing.Color.Blue
    '
    'Uc_entry_field36
    '
    Me.Uc_entry_field36.DoubleValue = 0.0R
    Me.Uc_entry_field36.IntegerValue = 0
    Me.Uc_entry_field36.IsReadOnly = False
    Me.Uc_entry_field36.Location = New System.Drawing.Point(7, 61)
    Me.Uc_entry_field36.Name = "Uc_entry_field36"
    Me.Uc_entry_field36.PlaceHolder = Nothing
    Me.Uc_entry_field36.Size = New System.Drawing.Size(213, 24)
    Me.Uc_entry_field36.SufixText = "Sufix Text"
    Me.Uc_entry_field36.SufixTextVisible = True
    Me.Uc_entry_field36.TabIndex = 1
    Me.Uc_entry_field36.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.Uc_entry_field36.TextValue = ""
    Me.Uc_entry_field36.TextVisible = False
    Me.Uc_entry_field36.TextWidth = 117
    Me.Uc_entry_field36.Value = ""
    Me.Uc_entry_field36.ValueForeColor = System.Drawing.Color.Blue
    '
    'TableLayoutPanel2
    '
    Me.TableLayoutPanel2.AutoSize = True
    Me.TableLayoutPanel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.TableLayoutPanel2.ColumnCount = 1
    Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
    Me.TableLayoutPanel2.Controls.Add(Me.Uc_combo7, 0, 0)
    Me.TableLayoutPanel2.Controls.Add(Me.Uc_entry_field37, 0, 4)
    Me.TableLayoutPanel2.Location = New System.Drawing.Point(0, 0)
    Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
    Me.TableLayoutPanel2.RowCount = 5
    Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
    Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
    Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
    Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
    Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
    Me.TableLayoutPanel2.Size = New System.Drawing.Size(200, 100)
    Me.TableLayoutPanel2.TabIndex = 0
    '
    'Uc_combo7
    '
    Me.Uc_combo7.AllowUnlistedValues = False
    Me.Uc_combo7.AutoCompleteMode = False
    Me.Uc_combo7.IsReadOnly = False
    Me.Uc_combo7.Location = New System.Drawing.Point(2, 2)
    Me.Uc_combo7.Margin = New System.Windows.Forms.Padding(2)
    Me.Uc_combo7.Name = "Uc_combo7"
    Me.Uc_combo7.SelectedIndex = -1
    Me.Uc_combo7.Size = New System.Drawing.Size(183, 24)
    Me.Uc_combo7.SufixText = "Sufix Text"
    Me.Uc_combo7.SufixTextVisible = True
    Me.Uc_combo7.TabIndex = 3
    Me.Uc_combo7.TextCombo = Nothing
    Me.Uc_combo7.TextVisible = False
    Me.Uc_combo7.TextWidth = 117
    '
    'Uc_entry_field37
    '
    Me.Uc_entry_field37.DoubleValue = 0.0R
    Me.Uc_entry_field37.IntegerValue = 0
    Me.Uc_entry_field37.IsReadOnly = False
    Me.Uc_entry_field37.Location = New System.Drawing.Point(2, 82)
    Me.Uc_entry_field37.Margin = New System.Windows.Forms.Padding(2)
    Me.Uc_entry_field37.Name = "Uc_entry_field37"
    Me.Uc_entry_field37.OnlyUpperCase = True
    Me.Uc_entry_field37.PlaceHolder = Nothing
    Me.Uc_entry_field37.Size = New System.Drawing.Size(196, 24)
    Me.Uc_entry_field37.SufixText = "Sufix Text"
    Me.Uc_entry_field37.SufixTextVisible = True
    Me.Uc_entry_field37.TabIndex = 6
    Me.Uc_entry_field37.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.Uc_entry_field37.TextValue = ""
    Me.Uc_entry_field37.TextVisible = False
    Me.Uc_entry_field37.TextWidth = 117
    Me.Uc_entry_field37.Value = ""
    Me.Uc_entry_field37.ValueForeColor = System.Drawing.Color.Blue
    '
    'Uc_button1
    '
    Me.Uc_button1.DialogResult = System.Windows.Forms.DialogResult.None
    Me.Uc_button1.Location = New System.Drawing.Point(126, 91)
    Me.Uc_button1.Name = "Uc_button1"
    Me.Uc_button1.Size = New System.Drawing.Size(90, 30)
    Me.Uc_button1.TabIndex = 2
    Me.Uc_button1.ToolTipped = False
    Me.Uc_button1.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'Uc_entry_field1
    '
    Me.Uc_entry_field1.DoubleValue = 0.0R
    Me.Uc_entry_field1.IntegerValue = 0
    Me.Uc_entry_field1.IsReadOnly = False
    Me.Uc_entry_field1.Location = New System.Drawing.Point(7, 31)
    Me.Uc_entry_field1.Name = "Uc_entry_field1"
    Me.Uc_entry_field1.PlaceHolder = Nothing
    Me.Uc_entry_field1.Size = New System.Drawing.Size(213, 24)
    Me.Uc_entry_field1.SufixText = "Sufix Text"
    Me.Uc_entry_field1.SufixTextVisible = True
    Me.Uc_entry_field1.TabIndex = 0
    Me.Uc_entry_field1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.Uc_entry_field1.TextValue = ""
    Me.Uc_entry_field1.TextVisible = False
    Me.Uc_entry_field1.TextWidth = 117
    Me.Uc_entry_field1.Value = ""
    Me.Uc_entry_field1.ValueForeColor = System.Drawing.Color.Blue
    '
    'Uc_entry_field2
    '
    Me.Uc_entry_field2.DoubleValue = 0.0R
    Me.Uc_entry_field2.IntegerValue = 0
    Me.Uc_entry_field2.IsReadOnly = False
    Me.Uc_entry_field2.Location = New System.Drawing.Point(7, 61)
    Me.Uc_entry_field2.Name = "Uc_entry_field2"
    Me.Uc_entry_field2.PlaceHolder = Nothing
    Me.Uc_entry_field2.Size = New System.Drawing.Size(213, 24)
    Me.Uc_entry_field2.SufixText = "Sufix Text"
    Me.Uc_entry_field2.SufixTextVisible = True
    Me.Uc_entry_field2.TabIndex = 1
    Me.Uc_entry_field2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.Uc_entry_field2.TextValue = ""
    Me.Uc_entry_field2.TextVisible = False
    Me.Uc_entry_field2.TextWidth = 117
    Me.Uc_entry_field2.Value = ""
    Me.Uc_entry_field2.ValueForeColor = System.Drawing.Color.Blue
    '
    'Uc_combo8
    '
    Me.Uc_combo8.AllowUnlistedValues = False
    Me.Uc_combo8.AutoCompleteMode = False
    Me.Uc_combo8.IsReadOnly = False
    Me.Uc_combo8.Location = New System.Drawing.Point(2, 30)
    Me.Uc_combo8.Margin = New System.Windows.Forms.Padding(2)
    Me.Uc_combo8.Name = "Uc_combo8"
    Me.Uc_combo8.SelectedIndex = -1
    Me.Uc_combo8.Size = New System.Drawing.Size(229, 24)
    Me.Uc_combo8.SufixText = "Sufix Text"
    Me.Uc_combo8.SufixTextVisible = True
    Me.Uc_combo8.TabIndex = 4
    Me.Uc_combo8.TextCombo = Nothing
    Me.Uc_combo8.TextVisible = False
    Me.Uc_combo8.TextWidth = 117
    '
    'uc_player_edit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.Controls.Add(Me.tab_container)
    Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Name = "uc_player_edit"
    Me.Size = New System.Drawing.Size(636, 357)
    Me.tab_container.ResumeLayout(False)
    Me.tab_main.ResumeLayout(False)
    Me.gb_main.ResumeLayout(False)
    Me.tlp_principal.ResumeLayout(False)
    Me.tlp_principal.PerformLayout()
    Me.tlp_prin_left.ResumeLayout(False)
    Me.tlp_prin_right.ResumeLayout(False)
    Me.tab_contact.ResumeLayout(False)
    Me.gb_contact.ResumeLayout(False)
    Me.tlp_contact.ResumeLayout(False)
    Me.tab_address.ResumeLayout(False)
    Me.gb_address.ResumeLayout(False)
    Me.gb_address.PerformLayout()
    Me.tlp_dir.ResumeLayout(False)
    Me.tlp_AutoSelection.ResumeLayout(False)
    Me.tab_beneficiary.ResumeLayout(False)
    Me.gb_beneficiary.ResumeLayout(False)
    Me.gb_beneficiary.PerformLayout()
    Me.tlp_beneficiary.ResumeLayout(False)
    Me.tlp_beneficiary.PerformLayout()
    Me.tlp_ben_left.ResumeLayout(False)
    Me.tlp_ben_right.ResumeLayout(False)
    Me.tab_points_program.ResumeLayout(False)
    Me.gb_next_level.ResumeLayout(False)
    Me.tab_block.ResumeLayout(False)
    Me.gb_block.ResumeLayout(False)
    Me.gb_block.PerformLayout()
    Me.tab_comments.ResumeLayout(False)
    Me.gb_extra_info.ResumeLayout(False)
    Me.gb_extra_info.PerformLayout()
    Me.tab_pin.ResumeLayout(False)
    Me.gb_pin.ResumeLayout(False)
    Me.TabPage1.ResumeLayout(False)
    Me.GroupBox1.ResumeLayout(False)
    Me.TabPage2.ResumeLayout(False)
    Me.GroupBox2.ResumeLayout(False)
    Me.TabPage3.ResumeLayout(False)
    Me.GroupBox3.ResumeLayout(False)
    Me.TabPage4.ResumeLayout(False)
    Me.GroupBox4.ResumeLayout(False)
    Me.GroupBox4.PerformLayout()
    Me.TabPage5.ResumeLayout(False)
    Me.GroupBox5.ResumeLayout(False)
    Me.GroupBox5.PerformLayout()
    Me.TabPage6.ResumeLayout(False)
    Me.GroupBox6.ResumeLayout(False)
    Me.TabPage7.ResumeLayout(False)
    Me.GroupBox7.ResumeLayout(False)
    Me.TabPage8.ResumeLayout(False)
    Me.GroupBox8.ResumeLayout(False)
    Me.TabPage9.ResumeLayout(False)
    Me.GroupBox9.ResumeLayout(False)
    Me.TabPage10.ResumeLayout(False)
    Me.GroupBox10.ResumeLayout(False)
    Me.GroupBox10.PerformLayout()
    Me.TabPage11.ResumeLayout(False)
    Me.GroupBox11.ResumeLayout(False)
    Me.GroupBox11.PerformLayout()
    Me.TabPage12.ResumeLayout(False)
    Me.GroupBox12.ResumeLayout(False)
    Me.TableLayoutPanel2.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents tab_container As System.Windows.Forms.TabControl
  Friend WithEvents tab_main As System.Windows.Forms.TabPage
  Friend WithEvents tab_contact As System.Windows.Forms.TabPage
  Friend WithEvents tab_address As System.Windows.Forms.TabPage
  Friend WithEvents tab_comments As System.Windows.Forms.TabPage
  Friend WithEvents tab_pin As System.Windows.Forms.TabPage
  Friend WithEvents gb_main As System.Windows.Forms.GroupBox
  Friend WithEvents ef_name As GUI_Controls.uc_entry_field
  Friend WithEvents ef_surname_1 As GUI_Controls.uc_entry_field
  Friend WithEvents cb_marital_status As GUI_Controls.uc_combo
  Friend WithEvents ef_doc_id As GUI_Controls.uc_entry_field
  Friend WithEvents cb_gender As GUI_Controls.uc_combo
  Friend WithEvents cb_doc_type As GUI_Controls.uc_combo
  Friend WithEvents ef_surname_2 As GUI_Controls.uc_entry_field
  Friend WithEvents gb_contact As System.Windows.Forms.GroupBox
  Friend WithEvents ef_email_1 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_phone_2 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_phone_1 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_email_2 As GUI_Controls.uc_entry_field
  Friend WithEvents gb_address As System.Windows.Forms.GroupBox
  Friend WithEvents ef_zip As GUI_Controls.uc_entry_field
  Friend WithEvents ef_street As GUI_Controls.uc_entry_field
  Friend WithEvents ef_city As GUI_Controls.uc_entry_field
  Friend WithEvents ef_delegation As GUI_Controls.uc_entry_field
  Friend WithEvents ef_colony As GUI_Controls.uc_entry_field
  Friend WithEvents gb_extra_info As System.Windows.Forms.GroupBox
  Friend WithEvents txt_comments As System.Windows.Forms.TextBox
  Friend WithEvents gb_pin As System.Windows.Forms.GroupBox
  Friend WithEvents ef_new_pin As GUI_Controls.uc_entry_field
  Friend WithEvents ef_confirm_pin As GUI_Controls.uc_entry_field
  Friend WithEvents btn_save_pin As GUI_Controls.uc_button
  Friend WithEvents ef_twitter As GUI_Controls.uc_entry_field
  Friend WithEvents tab_block As System.Windows.Forms.TabPage
  Friend WithEvents Uc_button1 As GUI_Controls.uc_button
  Friend WithEvents Uc_entry_field1 As GUI_Controls.uc_entry_field
  Friend WithEvents Uc_entry_field2 As GUI_Controls.uc_entry_field
  Friend WithEvents gb_block As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_blocked As System.Windows.Forms.Label
  Friend WithEvents txt_block_description As System.Windows.Forms.TextBox
  Friend WithEvents lbl_block_description As System.Windows.Forms.Label
  Friend WithEvents tab_beneficiary As System.Windows.Forms.TabPage
  Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
  Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
  Friend WithEvents Uc_entry_field3 As GUI_Controls.uc_entry_field
  Friend WithEvents Uc_date_picker1 As GUI_Controls.uc_date_picker
  Friend WithEvents Uc_date_picker2 As GUI_Controls.uc_date_picker
  Friend WithEvents Uc_combo1 As GUI_Controls.uc_combo
  Friend WithEvents Uc_entry_field4 As GUI_Controls.uc_entry_field
  Friend WithEvents Uc_combo2 As GUI_Controls.uc_combo
  Friend WithEvents Uc_combo3 As GUI_Controls.uc_combo
  Friend WithEvents Uc_entry_field5 As GUI_Controls.uc_entry_field
  Friend WithEvents Uc_entry_field6 As GUI_Controls.uc_entry_field
  Friend WithEvents Uc_entry_field7 As GUI_Controls.uc_entry_field
  Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
  Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
  Friend WithEvents Uc_entry_field8 As GUI_Controls.uc_entry_field
  Friend WithEvents Uc_entry_field9 As GUI_Controls.uc_entry_field
  Friend WithEvents Uc_entry_field10 As GUI_Controls.uc_entry_field
  Friend WithEvents Uc_entry_field11 As GUI_Controls.uc_entry_field
  Friend WithEvents Uc_entry_field12 As GUI_Controls.uc_entry_field
  Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
  Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
  Friend WithEvents Uc_entry_field13 As GUI_Controls.uc_entry_field
  Friend WithEvents Uc_entry_field14 As GUI_Controls.uc_entry_field
  Friend WithEvents Uc_entry_field15 As GUI_Controls.uc_entry_field
  Friend WithEvents Uc_entry_field16 As GUI_Controls.uc_entry_field
  Friend WithEvents Uc_entry_field17 As GUI_Controls.uc_entry_field
  Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
  Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
  Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
  Friend WithEvents Label1 As System.Windows.Forms.Label
  Friend WithEvents Label2 As System.Windows.Forms.Label
  Friend WithEvents TabPage5 As System.Windows.Forms.TabPage
  Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
  Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
  Friend WithEvents TabPage6 As System.Windows.Forms.TabPage
  Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
  Friend WithEvents Uc_button2 As GUI_Controls.uc_button
  Friend WithEvents Uc_entry_field18 As GUI_Controls.uc_entry_field
  Friend WithEvents Uc_entry_field19 As GUI_Controls.uc_entry_field
  Friend WithEvents TabPage7 As System.Windows.Forms.TabPage
  Friend WithEvents GroupBox7 As System.Windows.Forms.GroupBox
  Friend WithEvents Uc_entry_field20 As GUI_Controls.uc_entry_field
  Friend WithEvents Uc_date_picker3 As GUI_Controls.uc_date_picker
  Friend WithEvents Uc_date_picker4 As GUI_Controls.uc_date_picker
  Friend WithEvents Uc_combo4 As GUI_Controls.uc_combo
  Friend WithEvents Uc_entry_field21 As GUI_Controls.uc_entry_field
  Friend WithEvents Uc_combo5 As GUI_Controls.uc_combo
  Friend WithEvents Uc_combo6 As GUI_Controls.uc_combo
  Friend WithEvents Uc_entry_field22 As GUI_Controls.uc_entry_field
  Friend WithEvents Uc_entry_field23 As GUI_Controls.uc_entry_field
  Friend WithEvents Uc_entry_field24 As GUI_Controls.uc_entry_field
  Friend WithEvents TabPage8 As System.Windows.Forms.TabPage
  Friend WithEvents GroupBox8 As System.Windows.Forms.GroupBox
  Friend WithEvents Uc_entry_field25 As GUI_Controls.uc_entry_field
  Friend WithEvents Uc_entry_field26 As GUI_Controls.uc_entry_field
  Friend WithEvents Uc_entry_field27 As GUI_Controls.uc_entry_field
  Friend WithEvents Uc_entry_field28 As GUI_Controls.uc_entry_field
  Friend WithEvents Uc_entry_field29 As GUI_Controls.uc_entry_field
  Friend WithEvents TabPage9 As System.Windows.Forms.TabPage
  Friend WithEvents GroupBox9 As System.Windows.Forms.GroupBox
  Friend WithEvents Uc_entry_field30 As GUI_Controls.uc_entry_field
  Friend WithEvents Uc_entry_field31 As GUI_Controls.uc_entry_field
  Friend WithEvents Uc_entry_field32 As GUI_Controls.uc_entry_field
  Friend WithEvents Uc_entry_field33 As GUI_Controls.uc_entry_field
  Friend WithEvents Uc_entry_field34 As GUI_Controls.uc_entry_field
  Friend WithEvents TabPage10 As System.Windows.Forms.TabPage
  Friend WithEvents GroupBox10 As System.Windows.Forms.GroupBox
  Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
  Friend WithEvents Label3 As System.Windows.Forms.Label
  Friend WithEvents Label4 As System.Windows.Forms.Label
  Friend WithEvents TabPage11 As System.Windows.Forms.TabPage
  Friend WithEvents GroupBox11 As System.Windows.Forms.GroupBox
  Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
  Friend WithEvents TabPage12 As System.Windows.Forms.TabPage
  Friend WithEvents GroupBox12 As System.Windows.Forms.GroupBox
  Friend WithEvents Uc_button3 As GUI_Controls.uc_button
  Friend WithEvents Uc_entry_field35 As GUI_Controls.uc_entry_field
  Friend WithEvents Uc_entry_field36 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_ben_surname_2 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_ben_surname_1 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_ben_name As GUI_Controls.uc_entry_field
  Friend WithEvents gb_beneficiary As System.Windows.Forms.GroupBox
  Friend WithEvents btn_document As GUI_Controls.uc_button
  Friend WithEvents btn_ben_document As GUI_Controls.uc_button
  Friend WithEvents cb_birthplace As GUI_Controls.uc_combo
  Friend WithEvents cb_nationality As GUI_Controls.uc_combo
  Friend WithEvents ef_external_number As GUI_Controls.uc_entry_field
  Friend WithEvents cb_federal_entity As GUI_Controls.uc_combo
  Friend WithEvents dtp_ben_birthdate As GUI_Controls.uc_date_picker
  Friend WithEvents ef_ben_doc_id2 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_ben_doc_id1 As GUI_Controls.uc_entry_field
  Friend WithEvents cb_ben_gender As GUI_Controls.uc_combo
  Friend WithEvents chk_ben_holder_data As System.Windows.Forms.CheckBox
  Friend WithEvents ef_doc_id2 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_doc_id1 As GUI_Controls.uc_entry_field
  Friend WithEvents tab_points_program As System.Windows.Forms.TabPage
  Friend WithEvents lbl_level As System.Windows.Forms.Label
  Friend WithEvents lbl_entry_date As System.Windows.Forms.Label
  Friend WithEvents lbl_expiration_date As System.Windows.Forms.Label
  Friend WithEvents lbl_balance As System.Windows.Forms.Label
  Friend WithEvents lbl_points_to_keep_level As System.Windows.Forms.Label
  Friend WithEvents lbl_points As System.Windows.Forms.Label
  Friend WithEvents lbl_points_to_maintain As System.Windows.Forms.Label
  Friend WithEvents lbl_level_expiration As System.Windows.Forms.Label
  Friend WithEvents lbl_level_entered As System.Windows.Forms.Label
  Friend WithEvents lbl_level_value As System.Windows.Forms.Label
  Private WithEvents gb_next_level As System.Windows.Forms.GroupBox
  Private WithEvents lbl_points_last_days As System.Windows.Forms.Label
  Private WithEvents lbl_points_discretionaries_prefix As System.Windows.Forms.Label
  Private WithEvents lbl_points_discretionaries As System.Windows.Forms.Label
  Private WithEvents lbl_points_generated_prefix As System.Windows.Forms.Label
  Private WithEvents lbl_points_generated As System.Windows.Forms.Label
  Private WithEvents lbl_points_to_next_level As System.Windows.Forms.Label
  Private WithEvents lbl_points_to_next_level_prefix As System.Windows.Forms.Label
  Private WithEvents lbl_points_level_prefix As System.Windows.Forms.Label
  Private WithEvents lbl_points_level As System.Windows.Forms.Label
  Private WithEvents lbl_points_req_prefix As System.Windows.Forms.Label
  Private WithEvents lbl_points_req As System.Windows.Forms.Label
  Private WithEvents lbl_next_level_name As System.Windows.Forms.Label
  Private WithEvents lbl_points_sufix As System.Windows.Forms.Label
  Friend WithEvents cb_occupation As GUI_Controls.uc_combo
  Friend WithEvents cb_ben_occupation As GUI_Controls.uc_combo
  Friend WithEvents cb_is_vip As GUI_Controls.uc_combo
  Friend WithEvents tlp_principal As System.Windows.Forms.TableLayoutPanel
  Friend WithEvents tlp_prin_left As System.Windows.Forms.TableLayoutPanel
  Friend WithEvents tlp_prin_right As System.Windows.Forms.TableLayoutPanel
  Friend WithEvents dtp_birthdate As GUI_Controls.uc_date_picker
  Friend WithEvents ef_middle_name As GUI_Controls.uc_entry_field
  Friend WithEvents dtp_wedding As GUI_Controls.uc_date_picker
  Friend WithEvents tlp_contact As System.Windows.Forms.TableLayoutPanel
  Friend WithEvents tlp_dir As System.Windows.Forms.TableLayoutPanel
  Friend WithEvents tlp_beneficiary As System.Windows.Forms.TableLayoutPanel
  Friend WithEvents tlp_ben_left As System.Windows.Forms.TableLayoutPanel
  Friend WithEvents tlp_ben_right As System.Windows.Forms.TableLayoutPanel
  Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
  Friend WithEvents Uc_combo7 As GUI_Controls.uc_combo
  Friend WithEvents Uc_entry_field37 As GUI_Controls.uc_entry_field
  Friend WithEvents Uc_combo8 As GUI_Controls.uc_combo
  Friend WithEvents chk_show_comments As System.Windows.Forms.CheckBox
  Friend WithEvents cb_address_country As GUI_Controls.uc_combo
  Friend WithEvents chk_FreeMode As System.Windows.Forms.CheckBox
  Friend WithEvents tlp_AutoSelection As System.Windows.Forms.TableLayoutPanel
  Friend WithEvents cb_as_colony As GUI_Controls.uc_combo
  Friend WithEvents cb_as_delegation As GUI_Controls.uc_combo
  Friend WithEvents cb_as_city As GUI_Controls.uc_combo
  Friend WithEvents cb_as_address_country As GUI_Controls.uc_combo
  Friend WithEvents ef_as_street As GUI_Controls.uc_entry_field
  Friend WithEvents cb_as_federal_entity As GUI_Controls.uc_combo
  Friend WithEvents ef_as_external_number As GUI_Controls.uc_entry_field
  Friend WithEvents ef_as_zip As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_info As System.Windows.Forms.Label

End Class
