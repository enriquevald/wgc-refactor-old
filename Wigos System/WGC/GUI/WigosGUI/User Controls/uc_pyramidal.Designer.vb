﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uc_pyramidal
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.GroupBox1 = New System.Windows.Forms.GroupBox()
    Me.btn_definition_values = New System.Windows.Forms.Button()
    Me.chk_apply_pyramization = New System.Windows.Forms.CheckBox()
    Me.GroupBox1.SuspendLayout()
    Me.SuspendLayout()
    '
    'GroupBox1
    '
    Me.GroupBox1.Controls.Add(Me.btn_definition_values)
    Me.GroupBox1.Controls.Add(Me.chk_apply_pyramization)
    Me.GroupBox1.Location = New System.Drawing.Point(1, 4)
    Me.GroupBox1.Name = "GroupBox1"
    Me.GroupBox1.Size = New System.Drawing.Size(165, 45)
    Me.GroupBox1.TabIndex = 0
    Me.GroupBox1.TabStop = False
    '
    'btn_definition_values
    '
    Me.btn_definition_values.Location = New System.Drawing.Point(7, 17)
    Me.btn_definition_values.MaximumSize = New System.Drawing.Size(174, 23)
    Me.btn_definition_values.Name = "btn_definition_values"
    Me.btn_definition_values.Size = New System.Drawing.Size(152, 23)
    Me.btn_definition_values.TabIndex = 1
    Me.btn_definition_values.Text = "xDefinitionOfValues"
    Me.btn_definition_values.UseVisualStyleBackColor = True
    '
    'chk_apply_pyramization
    '
    Me.chk_apply_pyramization.AutoSize = True
    Me.chk_apply_pyramization.Location = New System.Drawing.Point(8, -1)
    Me.chk_apply_pyramization.Name = "chk_apply_pyramization"
    Me.chk_apply_pyramization.Size = New System.Drawing.Size(116, 17)
    Me.chk_apply_pyramization.TabIndex = 0
    Me.chk_apply_pyramization.Text = "xApplyPyramization"
    Me.chk_apply_pyramization.UseVisualStyleBackColor = True
    '
    'uc_pyramidal
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.Controls.Add(Me.GroupBox1)
    Me.MaximumSize = New System.Drawing.Size(196, 50)
    Me.Name = "uc_pyramidal"
    Me.Size = New System.Drawing.Size(172, 50)
    Me.GroupBox1.ResumeLayout(False)
    Me.GroupBox1.PerformLayout()
    Me.ResumeLayout(False)

  End Sub

  Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
  Friend WithEvents chk_apply_pyramization As System.Windows.Forms.CheckBox
  Public WithEvents btn_definition_values As System.Windows.Forms.Button

End Class
