'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
'   MODULE NAME: frm_test.vb
'
'   DESCRIPTION: form for testing purpouses
'
'        AUTHOR: Xavier Perez
'
' CREATION DATE: 27-MAR-2003
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------- ------ -----------------------------------------------
' 27-MAR-2003 XPS    Initial version
'-------------------------------------------------------------------

Option Explicit On 
Imports GUI_CommonMisc

Public Class frm_test
  Inherits System.Windows.Forms.Form

  Public GLB_NLS_GUI_STATISTICS As New CLASS_MODULE_NLS(ENUM_NLS_MODULES.COMMON_MODULE_GUI_STATISTICS)

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents crystal_report_viewer As CrystalDecisions.Windows.Forms.CrystalReportViewer
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.crystal_report_viewer = New CrystalDecisions.Windows.Forms.CrystalReportViewer()
    Me.SuspendLayout()
    '
    'crystal_report_viewer
    '
    Me.crystal_report_viewer.ActiveViewIndex = -1
    Me.crystal_report_viewer.DisplayGroupTree = False
    Me.crystal_report_viewer.Dock = System.Windows.Forms.DockStyle.Fill
    Me.crystal_report_viewer.Name = "crystal_report_viewer"
    Me.crystal_report_viewer.ReportSource = Nothing
    Me.crystal_report_viewer.Size = New System.Drawing.Size(752, 382)
    Me.crystal_report_viewer.TabIndex = 1
    '
    'frm_test
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.ClientSize = New System.Drawing.Size(752, 382)
    Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.crystal_report_viewer})
    Me.Name = "frm_test"
    Me.Text = "Form1"
    Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
    Me.ResumeLayout(False)

  End Sub

#End Region


  Public ReadOnly Property ReportViewer() As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Get
      Return Me.crystal_report_viewer
    End Get
  End Property


  Private Sub frm_test_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
    Me.Text = GLB_NLS_GUI_STATISTICS.GetString(257)
    ' To show GroupTree when it's opened you must set true the 
    ' property DisplayGroupTree in design mode, better
    ' Me.crystal_report_viewer.DisplayGroupTree = False
    ' To show GroupTree button you must set true the 
    ' property DisplayGroupTree
    Me.crystal_report_viewer.ShowGroupTreeButton = False

  End Sub

End Class
