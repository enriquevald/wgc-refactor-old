'-------------------------------------------------------------------
' Copyright � 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : frm_file_exported
'
' DESCRIPTION : Data Export
'
' REVISION HISTORY :
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 22-MAY-2013  JBP    Initial version
' 27-MAY-2013  DHA    Changes in the design of the form
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports System.IO

Public Class frm_file_exported

  Dim m_message As Integer

  Public Sub New(ByVal FilePath As String, ByVal FileName As String, ByVal Message As Integer)

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    m_message = Message

    ' Add any initialization after the InitializeComponent() call.
    InitForm()

    lnk_file.Text = Path.GetFileName(FileName)
    lnk_file.Links(0).LinkData = FilePath
    lnk_file.Visible = True

    FilePath = Path.GetDirectoryName(FilePath).ToString

    lnk_folder.Text = FilePath
    lnk_folder.Links(0).LinkData = FilePath
    lnk_folder.Visible = True

  End Sub

  Private Sub InitForm()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2014)

    lbl_message.Text = GUI_CommonMisc.NLS_GetString(m_message)
    lbl_file.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2015)
    lbl_folder.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2016)

    lnk_file.Visible = False
    lnk_folder.Visible = False

    btn_cerrar.Text = GLB_NLS_GUI_STATISTICS.GetString(1)

  End Sub

  ' PURPOSE: Open the link file
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Private Sub lnk_file_name_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnk_file.LinkClicked
    Dim _target As String

    _target = e.Link.LinkData

    Try
      Process.Start(_target)

    Catch ex As Exception
      'If NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(130), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK, , _
      '           Path.GetFileName(_target)) = ENUM_MB_RESULT.MB_RESULT_OK Then
      '  HideLabels()
      'End If

    End Try

  End Sub ' lnk_file_name_LinkClicked

  ' PURPOSE: Open the link file
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Private Sub lnk_file_path_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnk_folder.LinkClicked
    Dim _target As String

    _target = e.Link.LinkData

    Try
      Process.Start(_target)

    Catch ex As Exception
      'If NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(130), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK, , _
      '           Path.GetFileName(_target)) = ENUM_MB_RESULT.MB_RESULT_OK Then
      '  HideLabels()
      'End If

    End Try

  End Sub ' lnk_file_name_LinkClicked

  ' PURPOSE: Close form.
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Private Sub btn_cerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_cerrar.Click
    Me.Close()

  End Sub
End Class