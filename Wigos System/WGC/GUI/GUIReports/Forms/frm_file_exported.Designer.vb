<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_file_exported
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.lbl_folder = New System.Windows.Forms.Label
    Me.lnk_folder = New System.Windows.Forms.LinkLabel
    Me.lbl_file = New System.Windows.Forms.Label
    Me.lnk_file = New System.Windows.Forms.LinkLabel
    Me.btn_cerrar = New System.Windows.Forms.Button
    Me.lbl_message = New System.Windows.Forms.Label
    Me.SuspendLayout()
    '
    'lbl_folder
    '
    Me.lbl_folder.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_folder.Location = New System.Drawing.Point(16, 46)
    Me.lbl_folder.Name = "lbl_folder"
    Me.lbl_folder.Size = New System.Drawing.Size(71, 13)
    Me.lbl_folder.TabIndex = 0
    Me.lbl_folder.Text = "xFolder"
    Me.lbl_folder.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lnk_folder
    '
    Me.lnk_folder.AutoSize = True
    Me.lnk_folder.Location = New System.Drawing.Point(92, 45)
    Me.lnk_folder.Name = "lnk_folder"
    Me.lnk_folder.Size = New System.Drawing.Size(53, 13)
    Me.lnk_folder.TabIndex = 2
    Me.lnk_folder.TabStop = True
    Me.lnk_folder.Text = "lnk_folder"
    '
    'lbl_file
    '
    Me.lbl_file.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_file.Location = New System.Drawing.Point(20, 68)
    Me.lbl_file.Name = "lbl_file"
    Me.lbl_file.Size = New System.Drawing.Size(67, 13)
    Me.lbl_file.TabIndex = 1
    Me.lbl_file.Text = "xFile"
    Me.lbl_file.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lnk_file
    '
    Me.lnk_file.AutoSize = True
    Me.lnk_file.Location = New System.Drawing.Point(92, 67)
    Me.lnk_file.Name = "lnk_file"
    Me.lnk_file.Size = New System.Drawing.Size(40, 13)
    Me.lnk_file.TabIndex = 3
    Me.lnk_file.TabStop = True
    Me.lnk_file.Text = "lnk_file"
    '
    'btn_cerrar
    '
    Me.btn_cerrar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.btn_cerrar.Location = New System.Drawing.Point(407, 90)
    Me.btn_cerrar.Name = "btn_cerrar"
    Me.btn_cerrar.Size = New System.Drawing.Size(75, 23)
    Me.btn_cerrar.TabIndex = 5
    Me.btn_cerrar.Text = "xClose"
    Me.btn_cerrar.UseVisualStyleBackColor = True
    '
    'lbl_message
    '
    Me.lbl_message.AutoSize = True
    Me.lbl_message.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_message.Location = New System.Drawing.Point(16, 9)
    Me.lbl_message.MaximumSize = New System.Drawing.Size(460, 30)
    Me.lbl_message.Name = "lbl_message"
    Me.lbl_message.Size = New System.Drawing.Size(63, 13)
    Me.lbl_message.TabIndex = 6
    Me.lbl_message.Text = "xMessage"
    Me.lbl_message.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'frm_file_exported
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(494, 122)
    Me.Controls.Add(Me.lbl_message)
    Me.Controls.Add(Me.btn_cerrar)
    Me.Controls.Add(Me.lnk_file)
    Me.Controls.Add(Me.lbl_file)
    Me.Controls.Add(Me.lnk_folder)
    Me.Controls.Add(Me.lbl_folder)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.MaximizeBox = False
    Me.MinimizeBox = False
    Me.Name = "frm_file_exported"
    Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
    Me.Text = "frm_file_exported"
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents lbl_folder As System.Windows.Forms.Label
  Friend WithEvents lnk_folder As System.Windows.Forms.LinkLabel
  Friend WithEvents lbl_file As System.Windows.Forms.Label
  Friend WithEvents lnk_file As System.Windows.Forms.LinkLabel
  Friend WithEvents btn_cerrar As System.Windows.Forms.Button
  Friend WithEvents lbl_message As System.Windows.Forms.Label
End Class
