'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:      frm_print_preview.vb  
'
' DESCRIPTION:      Form to set the printing params.
'
' AUTHOR:           Jes�s �ngel Blanco Blanco
'
' CREATION DATE:    08-AUG-2013
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ ----------------------------------------------
' 08-AUG-2013  JAB    Initial version.
' 05-SEP-2013  JBC    Added code for an unhandled exception when print the document
' 04-OCT-2013  JBC    Fixed Bug WIG-180 - Added code for an unhandled exception when print the document
' 22-APR-2016  FAV    Fixed Bug 12339 - Added code to record errors in Log
'-------------------------------------------------------------------

Imports System.Windows.Forms
Imports System.Drawing.Printing
Imports GUI_CommonMisc

Public Class frm_print_preview

#Region " Members "
  Private m_page_setup As SpreadsheetGear.IPageSetup
  Private m_doc As SpreadsheetGear.Drawing.Printing.WorkbookPrintDocument
#End Region

#Region " Properties "

  Public Property Document() As SpreadsheetGear.Drawing.Printing.WorkbookPrintDocument
    Get
      Return Me.m_doc
    End Get
    Set(ByVal value As SpreadsheetGear.Drawing.Printing.WorkbookPrintDocument)

      If Not Me.m_doc Is Nothing Then
        RemoveHandler Me.m_doc.BeginPrint, New PrintEventHandler(AddressOf Me.DocBeginPrint)
        RemoveHandler Me.m_doc.EndPrint, New PrintEventHandler(AddressOf Me.DocEndPrint)
      End If

      Me.m_doc = value

      If Not Me.m_doc Is Nothing Then
        AddHandler Me.m_doc.BeginPrint, New PrintEventHandler(AddressOf Me.DocBeginPrint)
        AddHandler Me.m_doc.EndPrint, New PrintEventHandler(AddressOf Me.DocEndPrint)
      End If

      If MyBase.Visible Then
        Me.Preview.Document = Me.Document
      End If

    End Set
  End Property ' Document()

#End Region

#Region " Public "

  ' PURPOSE: Constructor.
  '
  '  PARAMS:
  '     - INPUT:
  '       - PageSetup: SpreadsheetGear.IPageSetup.
  '       - PrintDocument: SpreadsheetGear.Drawing.Printing.WorkbookPrintDocument.
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Sub New(ByVal PageSetup As SpreadsheetGear.IPageSetup, ByVal PrintDocument As SpreadsheetGear.Drawing.Printing.WorkbookPrintDocument, _
                 Optional ByVal PageOrientation As SpreadsheetGear.PageOrientation = SpreadsheetGear.PageOrientation.Landscape)

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(486)

    gb_orientation.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2484)
    opt_landscape.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2485)
    opt_portrait.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2486)

    gb_scaling.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2487)
    opt_scaling_no.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2488)
    opt_scaling_one_page.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2489)
    opt_scaling_cols_one_page.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2490)
    opt_scaling_rows_one_page.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2491)
    opt_scaling_custom.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2492)

    lbl_number_copies.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2483)
    lbl_width_pages.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2493)
    lbl_height_pages.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2494)

    btn_print.Text = GLB_NLS_GUI_CONTROLS.GetString(5)
    btn_close.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(600)

    btn_begin.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(269)
    btn_end.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(270)
    btn_Prev.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2495)
    btn_next.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2496)

    lbl_message.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2497, Me.Preview.PageCount.ToString)
    lbl_message.ForeColor = Drawing.Color.Red

    Call InitPageSetup(PageSetup, PageOrientation)

    Document = PrintDocument

  End Sub              ' New()

  ' PURPOSE: Handles Preview.EscKeyPress.
  '
  '  PARAMS:
  '     - INPUT:
  '       - ScrollUp: Boolean.
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Sub EscKeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Preview.EscKeyPress
    Select Case e.KeyChar
      Case Chr(13) ' enter
        btn_imprimir_Click(sender, KeyPressEventArgs.Empty)

      Case Chr(27) ' esc
        Me.Close()

    End Select
  End Sub      ' EscKeyPress

#End Region

#Region " Private "

  ' PURPOSE: Initialize pageSetup and set default settings.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub InitPageSetup(ByVal PageSetup As SpreadsheetGear.IPageSetup, _
                            ByVal PageOrientation As SpreadsheetGear.PageOrientation)

    m_page_setup = PageSetup

    ' Default settings
    m_page_setup.Orientation = PageOrientation
    m_page_setup.FitToPagesWide = 1
    m_page_setup.FitToPagesTall = Int16.MaxValue
    m_page_setup.Order = SpreadsheetGear.Order.OverThenDown ' from left to right & from up to down
    m_page_setup.BottomMargin = 40
    m_page_setup.TopMargin = 40
    m_page_setup.LeftMargin = 35
    m_page_setup.RightMargin = 35

  End Sub                             ' SetDefaultSettings

  ' PURPOSE: Handles opt_landscape.CheckedChanged.
  '
  '  PARAMS:
  '     - INPUT:
  '       - sender: System.Object.
  '       - e: System.EventArgs.
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub opt_landscape_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_landscape.CheckedChanged
    If opt_landscape.Checked And Not m_page_setup Is Nothing Then
      Me.Preview.Document.DefaultPageSettings.Landscape = True
      m_page_setup.Orientation = SpreadsheetGear.PageOrientation.Landscape

      Me.Preview.RefreshPreview()
      Me.Preview.Focus()
    End If
  End Sub              ' opt_landscape_CheckedChanged

  ' PURPOSE: Handles opt_portrait.CheckedChanged.
  '
  '  PARAMS:
  '     - INPUT:
  '       - sender: System.Object.
  '       - e: System.EventArgs.
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub opt_portrait_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_portrait.CheckedChanged
    If opt_portrait.Checked And Not m_page_setup Is Nothing Then
      Me.Preview.Document.DefaultPageSettings.Landscape = False
      m_page_setup.Orientation = SpreadsheetGear.PageOrientation.Portrait

      Me.Preview.RefreshPreview()
      Me.Preview.Focus()
    End If
  End Sub               ' opt_portrait_CheckedChanged

  ' PURPOSE: Handles opt_scaling_one_page.CheckedChanged.
  '
  '  PARAMS:
  '     - INPUT:
  '       - sender: System.Object.
  '       - e: System.EventArgs.
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub opt_scaling_one_page_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_scaling_one_page.CheckedChanged
    If opt_scaling_one_page.Checked And Not m_page_setup Is Nothing Then
      m_page_setup.FitToPagesWide = 1
      m_page_setup.FitToPagesTall = 1

      Me.Preview.RefreshPreview()
      Me.Preview.Focus()
    End If
  End Sub       ' opt_scaling_one_page_CheckedChanged

  ' PURPOSE: Handles opt_scaling_cols_one_page.CheckedChanged.
  '
  '  PARAMS:
  '     - INPUT:
  '       - sender: System.Object.
  '       - e: System.EventArgs.
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub opt_scaling_cols_one_page_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_scaling_cols_one_page.CheckedChanged
    If opt_scaling_cols_one_page.Checked And Not m_page_setup Is Nothing Then
      m_page_setup.FitToPagesWide = 1
      m_page_setup.FitToPagesTall = Int16.MaxValue

      Me.Preview.RefreshPreview()
      Me.Preview.Focus()
    End If
  End Sub  ' opt_scaling_cols_one_page_CheckedChanged

  ' PURPOSE: Handles opt_scaling_rows_one_page.CheckedChanged.
  '
  '  PARAMS:
  '     - INPUT:
  '       - sender: System.Object.
  '       - e: System.EventArgs.
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub opt_scaling_rows_one_page_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_scaling_rows_one_page.CheckedChanged
    If opt_scaling_rows_one_page.Checked And Not m_page_setup Is Nothing Then
      m_page_setup.FitToPagesWide = Int16.MaxValue
      m_page_setup.FitToPagesTall = 1

      Me.Preview.RefreshPreview()
      Me.Preview.Focus()
    End If
  End Sub  ' opt_scaling_rows_one_page_CheckedChanged

  ' PURPOSE: Handles opt_scaling_no.CheckedChanged.
  '
  '  PARAMS:
  '     - INPUT:
  '       - sender: System.Object.
  '       - e: System.EventArgs.
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub opt_scaling_no_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_scaling_no.CheckedChanged
    If opt_scaling_no.Checked And Not m_page_setup Is Nothing Then
      m_page_setup.FitToPagesWide = Int16.MaxValue
      m_page_setup.FitToPagesTall = Int16.MaxValue

      Me.Preview.RefreshPreview()
      Me.Preview.Focus()
    End If
  End Sub             ' opt_scaling_no_CheckedChanged

  ' PURPOSE: Handles opt_scaling_custom.CheckedChanged.
  '
  '  PARAMS:
  '     - INPUT:
  '       - sender: System.Object.
  '       - e: System.EventArgs.
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub opt_scaling_custom_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_scaling_custom.CheckedChanged
    nud_height_pages.Enabled = opt_scaling_custom.Checked
    nud_width_pages.Enabled = opt_scaling_custom.Checked

    If opt_scaling_custom.Checked Then
      m_page_setup.FitToPagesWide = IIf(nud_width_pages.Value = 0, Int16.MaxValue, nud_width_pages.Value)
      m_page_setup.FitToPagesTall = IIf(nud_height_pages.Value = 0, Int16.MaxValue, nud_height_pages.Value)

      Me.Preview.RefreshPreview()
      Me.Preview.Focus()
    End If
  End Sub         ' opt_scaling_custom_CheckedChanged

  ' PURPOSE: Handles nud_width_pages.ValueChanged.
  '
  '  PARAMS:
  '     - INPUT:
  '       - sender: System.Object.
  '       - e: System.EventArgs.
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub nud_width_pages_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles nud_width_pages.ValueChanged
    Call opt_scaling_custom_CheckedChanged(sender, e)
  End Sub              ' nud_width_pages_KeyPress

  ' PURPOSE: Handles nud_height_pages.ValueChanged.
  '
  '  PARAMS:
  '     - INPUT:
  '       - sender: System.Object.
  '       - e: System.EventArgs.
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub nud_height_pages_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles nud_height_pages.ValueChanged
    Call opt_scaling_custom_CheckedChanged(sender, e)
  End Sub             ' nud_width_pages_KeyPress

  ' PURPOSE: Handles nud_width_pages.KeyPress, nud_height_pages.KeyPress.
  '
  '  PARAMS:
  '     - INPUT:
  '       - sender: System.Object.
  '       - e: System.Windows.Forms.KeyPressEventArgs.
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub nud_width_pages_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles nud_width_pages.KeyPress
    Select Case e.KeyChar
      Case Chr(13)
        NUD_SetDefaultValue(nud_width_pages, 0)
    End Select
  End Sub                  ' nud_width_pages_KeyPress

  Private Sub nud_width_pages_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles nud_width_pages.Leave
    NUD_SetDefaultValue(nud_width_pages, 0)
  End Sub

  Private Sub nud_height_pages_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles nud_height_pages.KeyPress
    Select Case e.KeyChar
      Case Chr(13)
        NUD_SetDefaultValue(nud_height_pages, 0)
    End Select
  End Sub                  ' nud_width_pages_KeyPress

  Private Sub nud_height_pages_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles nud_height_pages.Leave
    NUD_SetDefaultValue(nud_height_pages, 0)
  End Sub

  Private Sub nud_number_copies_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles nud_number_copies.KeyPress
    Select Case e.KeyChar
      Case Chr(13)
        NUD_SetDefaultValue(nud_number_copies, 1)
    End Select
  End Sub

  Private Sub nud_number_copies_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles nud_number_copies.Leave
    NUD_SetDefaultValue(nud_number_copies, 1)
  End Sub

  Private Sub NUD_SetDefaultValue(ByVal NUD As NumericUpDown, ByVal Value As Integer)
    If String.IsNullOrEmpty(NUD.Controls.Item(1).Text) Then
      NUD.Controls.Item(1).Text = Value.ToString()
      NUD.Value = Value
    End If
  End Sub
  ' PURPOSE: Handles btn_print.Click.
  '
  '  PARAMS:
  '     - INPUT:
  '       - sender: System.Object.
  '       - e: System.EventArgs.
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub btn_imprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_print.Click
    Dim _dialog As System.Windows.Forms.PrintDialog

    Try
      _dialog = New System.Windows.Forms.PrintDialog()

      _dialog.Document = m_doc
      m_doc.PrinterSettings.Copies = nud_number_copies.Value

      If _dialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
        Using m_doc
          _dialog.Document = m_doc
          m_doc.Print()
        End Using
      End If

    Catch _ex As Exception
      WSI.Common.Log.Exception(_ex)
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2566), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1)
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            GLB_ApplicationName, _
                            "", _
                            "frm_print_preview.btn_imprimir_Click()", _
                            "", _
                            "", _
                            "Error in Excel data Print")
    End Try
  End Sub                        ' btn_imprimir_Click

  ' PURPOSE: Handles btn_close.Click.
  '
  '  PARAMS:
  '     - INPUT:
  '       - sender: System.Object.
  '       - e: System.EventArgs.
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub btn_close_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_close.Click
    MyBase.Close()
  End Sub                           ' btn_close_Click

  ' PURPOSE: Bet label and buttons properties before print.
  '
  '  PARAMS:
  '     - INPUT:
  '       - sender: System.Object.
  '       - e: PrintEventArgs.
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub DocBeginPrint(ByVal sender As Object, ByVal e As PrintEventArgs)
    If Preview.PageCount >= uc_print_preview.CANCEL_PREVIEW_AT_PAGE Then
      lbl_message.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2497, Me.Preview.PageCount.ToString)
      lbl_message.Visible = True
    Else
      lbl_message.Visible = False
    End If

    SetControlsEnabledProperties(False)
  End Sub                             ' DocBeginPrint

  ' PURPOSE: Handles opt_scaling_rows_one_page.CheckedChanged.
  '
  '  PARAMS:
  '     - INPUT:
  '       - sender: System.Object.
  '       - e: System.EventArgs.
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub DocEndPrint(ByVal sender As Object, ByVal e As PrintEventArgs)
    If Preview.PageCount >= uc_print_preview.CANCEL_PREVIEW_AT_PAGE Then
      lbl_message.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2497, Me.Preview.PageCount.ToString)
      lbl_message.Visible = True
    Else
      lbl_message.Visible = False
    End If

    Me.Preview.StartPage = 0
    Me.ef_start_page.Text = 1

    SetControlsEnabledProperties(True)
  End Sub                               ' DocEndPrint

  ' PURPOSE: Handles opt_scaling_rows_one_page.CheckedChanged.
  '
  '  PARAMS:
  '     - INPUT:
  '       - sender: System.Object.
  '       - e: System.EventArgs.
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub preview_StartPageChanged(ByVal sender As Object, ByVal e As EventArgs) Handles Preview.StartPageChanged
    Me.ef_start_page.Text = (Me.Preview.StartPage + 1).ToString
  End Sub                  ' preview_StartPageChanged

  ' PURPOSE: Handles opt_scaling_rows_one_page.CheckedChanged.
  '
  '  PARAMS:
  '     - INPUT:
  '       - sender: System.Object.
  '       - e: System.EventArgs.
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub btnBegin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_begin.Click
    Me.Preview.StartPage = 0
  End Sub                            ' preview_StartPageChanged

  ' PURPOSE: Handles opt_scaling_rows_one_page.CheckedChanged.
  '
  '  PARAMS:
  '     - INPUT:
  '       - sender: System.Object.
  '       - e: System.EventArgs.
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub btnPrev_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Prev.Click
    Me.Preview.StartPage -= 1
  End Sub                             ' btnPrev_Click

  ' PURPOSE: Handles opt_scaling_rows_one_page.CheckedChanged.
  '
  '  PARAMS:
  '     - INPUT:
  '       - sender: System.Object.
  '       - e: System.EventArgs.
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub efStartPage_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ef_start_page.Enter
    Me.ef_start_page.SelectAll()
  End Sub                         ' efStartPage_Enter

  ' PURPOSE: Handles opt_scaling_rows_one_page.CheckedChanged.
  '
  '  PARAMS:
  '     - INPUT:
  '       - sender: System.Object.
  '       - e: System.EventArgs.
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub efStartPage_Validating(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles ef_start_page.Validating
    Me.CommitPageNumber()
  End Sub                    ' efStartPage_Validating

  ' PURPOSE: Handles opt_scaling_rows_one_page.CheckedChanged.
  '
  '  PARAMS:
  '     - INPUT:
  '       - sender: System.Object.
  '       - e: System.EventArgs.
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub efStartPage_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles ef_start_page.KeyPress
    Dim c As Char = e.KeyChar
    If c = ChrW(13) Then
      Me.CommitPageNumber()
      e.Handled = True
    ElseIf Not ((c <= " "c) OrElse Char.IsDigit(c)) Then
      e.Handled = True
    End If
  End Sub                      ' efStartPage_KeyPress

  ' PURPOSE: Handles opt_scaling_rows_one_page.CheckedChanged.
  '
  '  PARAMS:
  '     - INPUT:
  '       - sender: System.Object.
  '       - e: System.EventArgs.
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_next.Click
    Me.Preview.StartPage += 1
  End Sub                             ' btnNext_Click

  ' PURPOSE: Handles opt_scaling_rows_one_page.CheckedChanged.
  '
  '  PARAMS:
  '     - INPUT:
  '       - sender: System.Object.
  '       - e: System.EventArgs.
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub btnEnd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_end.Click
    Me.Preview.StartPage = (Me.Preview.PageCount - 1)
  End Sub                              ' btnEnd_Click

  ' PURPOSE: Handles opt_scaling_rows_one_page.CheckedChanged.
  '
  '  PARAMS:
  '     - INPUT:
  '       - sender: System.Object.
  '       - e: System.EventArgs.
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub CommitPageNumber()
    Dim page As Integer
    If Integer.TryParse(Me.ef_start_page.Text, page) Then
      Me.Preview.StartPage = (page - 1)
    End If
  End Sub                          ' CommitPageNumber

  ' PURPOSE: Handles opt_scaling_rows_one_page.CheckedChanged.
  '
  '  PARAMS:
  '     - INPUT:
  '       - sender: System.Object.
  '       - e: System.EventArgs.
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub Preview_PageCountChanged(ByVal sender As Object, ByVal e As EventArgs) Handles Preview.PageCountChanged
    MyBase.Update()
    Application.DoEvents()
    Me.lbl_page_count.Text = String.Format("of {0}", Me.Preview.PageCount)
  End Sub                  ' Preview_PageCountChanged

  ' PURPOSE: Handles Preview.MouseWheelUpDown.
  '
  '  PARAMS:
  '     - INPUT:
  '       - ScrollUp: Boolean.
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub MouseWheelUpDownHandle(ByVal ScrollUp As Boolean) Handles Preview.MouseWheelUpDown
    If ScrollUp Then
      Me.Preview.StartPage -= 1
    Else
      Me.Preview.StartPage += 1
    End If
  End Sub                    ' MouseWheelUpDownHandle

  ' PURPOSE: Set the value of the enabled property.
  '
  '  PARAMS:
  '     - INPUT:
  '       - ScrollUp: Boolean.
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub SetControlsEnabledProperties(ByVal State As Boolean)
    pnl_function_buttons.Enabled = State
    tls_pager.Enabled = State
  End Sub              ' SetControlsEnabledProperties

#End Region

#Region " Overrides "

  Protected Overrides Sub Finalize()
    MyBase.Finalize()
  End Sub      ' Finalize

  Protected Overrides Sub OnFormClosing(ByVal e As FormClosingEventArgs)
    MyBase.OnFormClosing(e)
    If Not (Not Me.Preview.IsRendering OrElse e.Cancel) Then
      Me.Preview.Cancel()
    End If
  End Sub ' OnFormClosing

  Protected Overrides Sub OnShown(ByVal e As EventArgs)
    Dim _page_orientation As SpreadsheetGear.PageOrientation

    _page_orientation = m_page_setup.Orientation

    Try
      MyBase.OnShown(e)
      Me.Preview.Document = Me.Document
      Me.Preview.Focus()

      If _page_orientation = SpreadsheetGear.PageOrientation.Landscape Then
        opt_landscape.Checked = True
      Else
        opt_portrait.Checked = True
      End If

    Catch _ex As Exception
      WSI.Common.Log.Exception(_ex)
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2566), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1)
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
            GLB_ApplicationName, _
           "", _
           "frm_print_preview.OnShown", _
           "", _
           "", _
          "Error in Print Preview")
      MyBase.Close()
    End Try
  End Sub       ' OnShown

#End Region

End Class
