<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_print_preview
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_print_preview))
    Me.pnl_function_buttons = New System.Windows.Forms.Panel
    Me.btn_close = New System.Windows.Forms.Button
    Me.lbl_number_copies = New System.Windows.Forms.Label
    Me.nud_number_copies = New System.Windows.Forms.NumericUpDown
    Me.gb_scaling = New System.Windows.Forms.GroupBox
    Me.lbl_height_pages = New System.Windows.Forms.Label
    Me.lbl_width_pages = New System.Windows.Forms.Label
    Me.nud_height_pages = New System.Windows.Forms.NumericUpDown
    Me.nud_width_pages = New System.Windows.Forms.NumericUpDown
    Me.opt_scaling_custom = New System.Windows.Forms.RadioButton
    Me.opt_scaling_rows_one_page = New System.Windows.Forms.RadioButton
    Me.opt_scaling_cols_one_page = New System.Windows.Forms.RadioButton
    Me.opt_scaling_one_page = New System.Windows.Forms.RadioButton
    Me.opt_scaling_no = New System.Windows.Forms.RadioButton
    Me.gb_orientation = New System.Windows.Forms.GroupBox
    Me.opt_portrait = New System.Windows.Forms.RadioButton
    Me.opt_landscape = New System.Windows.Forms.RadioButton
    Me.btn_print = New System.Windows.Forms.Button
    Me.tls_pager = New System.Windows.Forms.ToolStrip
    Me.btn_end = New System.Windows.Forms.ToolStripButton
    Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator
    Me.btn_next = New System.Windows.Forms.ToolStripButton
    Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
    Me.lbl_page_count = New System.Windows.Forms.ToolStripLabel
    Me.ef_start_page = New System.Windows.Forms.ToolStripTextBox
    Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator
    Me.btn_Prev = New System.Windows.Forms.ToolStripButton
    Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator
    Me.btn_begin = New System.Windows.Forms.ToolStripButton
    Me.lbl_message = New System.Windows.Forms.ToolStripLabel
    Me.Panel1 = New System.Windows.Forms.Panel
    Me.Preview = New GUI_Reports.uc_print_preview
    Me.pnl_function_buttons.SuspendLayout()
    CType(Me.nud_number_copies, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.gb_scaling.SuspendLayout()
    CType(Me.nud_height_pages, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.nud_width_pages, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.gb_orientation.SuspendLayout()
    Me.tls_pager.SuspendLayout()
    Me.Panel1.SuspendLayout()
    Me.SuspendLayout()
    '
    'pnl_function_buttons
    '
    Me.pnl_function_buttons.Controls.Add(Me.btn_close)
    Me.pnl_function_buttons.Controls.Add(Me.lbl_number_copies)
    Me.pnl_function_buttons.Controls.Add(Me.nud_number_copies)
    Me.pnl_function_buttons.Controls.Add(Me.gb_scaling)
    Me.pnl_function_buttons.Controls.Add(Me.gb_orientation)
    Me.pnl_function_buttons.Controls.Add(Me.btn_print)
    Me.pnl_function_buttons.Dock = System.Windows.Forms.DockStyle.Left
    Me.pnl_function_buttons.Location = New System.Drawing.Point(0, 0)
    Me.pnl_function_buttons.Name = "pnl_function_buttons"
    Me.pnl_function_buttons.Size = New System.Drawing.Size(207, 508)
    Me.pnl_function_buttons.TabIndex = 0
    '
    'btn_close
    '
    Me.btn_close.Location = New System.Drawing.Point(7, 401)
    Me.btn_close.Name = "btn_close"
    Me.btn_close.Size = New System.Drawing.Size(75, 23)
    Me.btn_close.TabIndex = 5
    Me.btn_close.Text = "xClose"
    Me.btn_close.UseVisualStyleBackColor = True
    '
    'lbl_number_copies
    '
    Me.lbl_number_copies.Location = New System.Drawing.Point(100, 18)
    Me.lbl_number_copies.Name = "lbl_number_copies"
    Me.lbl_number_copies.Size = New System.Drawing.Size(49, 20)
    Me.lbl_number_copies.TabIndex = 1
    Me.lbl_number_copies.Text = "xCopies"
    Me.lbl_number_copies.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'nud_number_copies
    '
    Me.nud_number_copies.Location = New System.Drawing.Point(150, 18)
    Me.nud_number_copies.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
    Me.nud_number_copies.Name = "nud_number_copies"
    Me.nud_number_copies.Size = New System.Drawing.Size(42, 20)
    Me.nud_number_copies.TabIndex = 2
    Me.nud_number_copies.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.nud_number_copies.Value = New Decimal(New Integer() {1, 0, 0, 0})
    '
    'gb_scaling
    '
    Me.gb_scaling.Controls.Add(Me.lbl_height_pages)
    Me.gb_scaling.Controls.Add(Me.lbl_width_pages)
    Me.gb_scaling.Controls.Add(Me.nud_height_pages)
    Me.gb_scaling.Controls.Add(Me.nud_width_pages)
    Me.gb_scaling.Controls.Add(Me.opt_scaling_custom)
    Me.gb_scaling.Controls.Add(Me.opt_scaling_rows_one_page)
    Me.gb_scaling.Controls.Add(Me.opt_scaling_cols_one_page)
    Me.gb_scaling.Controls.Add(Me.opt_scaling_one_page)
    Me.gb_scaling.Controls.Add(Me.opt_scaling_no)
    Me.gb_scaling.Location = New System.Drawing.Point(7, 164)
    Me.gb_scaling.Name = "gb_scaling"
    Me.gb_scaling.Size = New System.Drawing.Size(187, 231)
    Me.gb_scaling.TabIndex = 4
    Me.gb_scaling.TabStop = False
    Me.gb_scaling.Text = "xScaling"
    '
    'lbl_height_pages
    '
    Me.lbl_height_pages.AutoSize = True
    Me.lbl_height_pages.Location = New System.Drawing.Point(53, 202)
    Me.lbl_height_pages.Name = "lbl_height_pages"
    Me.lbl_height_pages.Size = New System.Drawing.Size(73, 13)
    Me.lbl_height_pages.TabIndex = 8
    Me.lbl_height_pages.Text = "xHeightPages"
    '
    'lbl_width_pages
    '
    Me.lbl_width_pages.AutoSize = True
    Me.lbl_width_pages.Location = New System.Drawing.Point(53, 175)
    Me.lbl_width_pages.Name = "lbl_width_pages"
    Me.lbl_width_pages.Size = New System.Drawing.Size(70, 13)
    Me.lbl_width_pages.TabIndex = 6
    Me.lbl_width_pages.Text = "xWidthPages"
    Me.lbl_width_pages.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'nud_height_pages
    '
    Me.nud_height_pages.Enabled = False
    Me.nud_height_pages.Location = New System.Drawing.Point(6, 198)
    Me.nud_height_pages.Maximum = New Decimal(New Integer() {1000, 0, 0, 0})
    Me.nud_height_pages.Name = "nud_height_pages"
    Me.nud_height_pages.Size = New System.Drawing.Size(44, 20)
    Me.nud_height_pages.TabIndex = 7
    Me.nud_height_pages.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
    Me.nud_height_pages.UpDownAlign = System.Windows.Forms.LeftRightAlignment.Left
    '
    'nud_width_pages
    '
    Me.nud_width_pages.Enabled = False
    Me.nud_width_pages.Location = New System.Drawing.Point(6, 171)
    Me.nud_width_pages.Maximum = New Decimal(New Integer() {1000, 0, 0, 0})
    Me.nud_width_pages.Name = "nud_width_pages"
    Me.nud_width_pages.Size = New System.Drawing.Size(44, 20)
    Me.nud_width_pages.TabIndex = 5
    Me.nud_width_pages.Tag = ""
    Me.nud_width_pages.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
    Me.nud_width_pages.UpDownAlign = System.Windows.Forms.LeftRightAlignment.Left
    '
    'opt_scaling_custom
    '
    Me.opt_scaling_custom.AutoSize = True
    Me.opt_scaling_custom.Location = New System.Drawing.Point(6, 148)
    Me.opt_scaling_custom.Name = "opt_scaling_custom"
    Me.opt_scaling_custom.Size = New System.Drawing.Size(100, 17)
    Me.opt_scaling_custom.TabIndex = 4
    Me.opt_scaling_custom.Text = "xScalingCustom"
    Me.opt_scaling_custom.UseVisualStyleBackColor = True
    '
    'opt_scaling_rows_one_page
    '
    Me.opt_scaling_rows_one_page.AutoSize = True
    Me.opt_scaling_rows_one_page.Location = New System.Drawing.Point(6, 117)
    Me.opt_scaling_rows_one_page.Name = "opt_scaling_rows_one_page"
    Me.opt_scaling_rows_one_page.Size = New System.Drawing.Size(151, 17)
    Me.opt_scaling_rows_one_page.TabIndex = 3
    Me.opt_scaling_rows_one_page.Text = "xScalingRowsOnOnePage"
    Me.opt_scaling_rows_one_page.UseVisualStyleBackColor = True
    '
    'opt_scaling_cols_one_page
    '
    Me.opt_scaling_cols_one_page.AutoSize = True
    Me.opt_scaling_cols_one_page.Checked = True
    Me.opt_scaling_cols_one_page.Location = New System.Drawing.Point(6, 86)
    Me.opt_scaling_cols_one_page.Name = "opt_scaling_cols_one_page"
    Me.opt_scaling_cols_one_page.Size = New System.Drawing.Size(144, 17)
    Me.opt_scaling_cols_one_page.TabIndex = 2
    Me.opt_scaling_cols_one_page.TabStop = True
    Me.opt_scaling_cols_one_page.Text = "xScalingColsOnOnePage"
    Me.opt_scaling_cols_one_page.UseVisualStyleBackColor = True
    '
    'opt_scaling_one_page
    '
    Me.opt_scaling_one_page.AutoSize = True
    Me.opt_scaling_one_page.Location = New System.Drawing.Point(6, 55)
    Me.opt_scaling_one_page.Name = "opt_scaling_one_page"
    Me.opt_scaling_one_page.Size = New System.Drawing.Size(110, 17)
    Me.opt_scaling_one_page.TabIndex = 1
    Me.opt_scaling_one_page.Text = "xScalingOnePage"
    Me.opt_scaling_one_page.UseVisualStyleBackColor = True
    '
    'opt_scaling_no
    '
    Me.opt_scaling_no.AutoSize = True
    Me.opt_scaling_no.Location = New System.Drawing.Point(6, 24)
    Me.opt_scaling_no.Name = "opt_scaling_no"
    Me.opt_scaling_no.Size = New System.Drawing.Size(79, 17)
    Me.opt_scaling_no.TabIndex = 0
    Me.opt_scaling_no.Text = "xScalingNo"
    Me.opt_scaling_no.UseVisualStyleBackColor = True
    '
    'gb_orientation
    '
    Me.gb_orientation.Controls.Add(Me.opt_portrait)
    Me.gb_orientation.Controls.Add(Me.opt_landscape)
    Me.gb_orientation.Location = New System.Drawing.Point(7, 55)
    Me.gb_orientation.Name = "gb_orientation"
    Me.gb_orientation.Size = New System.Drawing.Size(187, 98)
    Me.gb_orientation.TabIndex = 3
    Me.gb_orientation.TabStop = False
    Me.gb_orientation.Text = "xOrientation"
    '
    'opt_portrait
    '
    Me.opt_portrait.AutoSize = True
    Me.opt_portrait.Location = New System.Drawing.Point(6, 55)
    Me.opt_portrait.Name = "opt_portrait"
    Me.opt_portrait.Size = New System.Drawing.Size(63, 17)
    Me.opt_portrait.TabIndex = 1
    Me.opt_portrait.Text = "xPortrait"
    Me.opt_portrait.UseVisualStyleBackColor = True
    '
    'opt_landscape
    '
    Me.opt_landscape.AutoSize = True
    Me.opt_landscape.Location = New System.Drawing.Point(6, 25)
    Me.opt_landscape.Name = "opt_landscape"
    Me.opt_landscape.Size = New System.Drawing.Size(83, 17)
    Me.opt_landscape.TabIndex = 0
    Me.opt_landscape.Text = "xLandscape"
    Me.opt_landscape.UseVisualStyleBackColor = True
    '
    'btn_print
    '
    Me.btn_print.Location = New System.Drawing.Point(7, 16)
    Me.btn_print.Name = "btn_print"
    Me.btn_print.Size = New System.Drawing.Size(75, 23)
    Me.btn_print.TabIndex = 0
    Me.btn_print.Text = "xPrint"
    Me.btn_print.UseVisualStyleBackColor = True
    '
    'tls_pager
    '
    Me.tls_pager.BackColor = System.Drawing.Color.Transparent
    Me.tls_pager.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
    Me.tls_pager.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.tls_pager.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
    Me.tls_pager.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btn_end, Me.ToolStripSeparator3, Me.btn_next, Me.ToolStripSeparator1, Me.lbl_page_count, Me.ef_start_page, Me.ToolStripSeparator4, Me.btn_Prev, Me.ToolStripSeparator2, Me.btn_begin, Me.lbl_message})
    Me.tls_pager.Location = New System.Drawing.Point(0, 483)
    Me.tls_pager.Name = "tls_pager"
    Me.tls_pager.Size = New System.Drawing.Size(653, 25)
    Me.tls_pager.TabIndex = 1
    Me.tls_pager.Text = "ToolStrip1"
    '
    'btn_end
    '
    Me.btn_end.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
    Me.btn_end.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
    Me.btn_end.Image = CType(resources.GetObject("btn_end.Image"), System.Drawing.Image)
    Me.btn_end.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.btn_end.Name = "btn_end"
    Me.btn_end.Size = New System.Drawing.Size(36, 22)
    Me.btn_end.Text = "xEnd"
    '
    'ToolStripSeparator3
    '
    Me.ToolStripSeparator3.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
    Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
    Me.ToolStripSeparator3.Size = New System.Drawing.Size(6, 25)
    '
    'btn_next
    '
    Me.btn_next.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
    Me.btn_next.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
    Me.btn_next.Image = CType(resources.GetObject("btn_next.Image"), System.Drawing.Image)
    Me.btn_next.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.btn_next.Name = "btn_next"
    Me.btn_next.Size = New System.Drawing.Size(40, 22)
    Me.btn_next.Text = "xNext"
    '
    'ToolStripSeparator1
    '
    Me.ToolStripSeparator1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
    Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
    Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 25)
    '
    'lbl_page_count
    '
    Me.lbl_page_count.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
    Me.lbl_page_count.Name = "lbl_page_count"
    Me.lbl_page_count.Size = New System.Drawing.Size(70, 22)
    Me.lbl_page_count.Text = "xTotalPages"
    '
    'ef_start_page
    '
    Me.ef_start_page.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
    Me.ef_start_page.Name = "ef_start_page"
    Me.ef_start_page.Size = New System.Drawing.Size(32, 25)
    Me.ef_start_page.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Center
    '
    'ToolStripSeparator4
    '
    Me.ToolStripSeparator4.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
    Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
    Me.ToolStripSeparator4.Size = New System.Drawing.Size(6, 25)
    '
    'btn_Prev
    '
    Me.btn_Prev.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
    Me.btn_Prev.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
    Me.btn_Prev.Image = CType(resources.GetObject("btn_Prev.Image"), System.Drawing.Image)
    Me.btn_Prev.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.btn_Prev.Name = "btn_Prev"
    Me.btn_Prev.Size = New System.Drawing.Size(39, 22)
    Me.btn_Prev.Text = "xPrev"
    '
    'ToolStripSeparator2
    '
    Me.ToolStripSeparator2.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
    Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
    Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 25)
    '
    'btn_begin
    '
    Me.btn_begin.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
    Me.btn_begin.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
    Me.btn_begin.Image = CType(resources.GetObject("btn_begin.Image"), System.Drawing.Image)
    Me.btn_begin.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.btn_begin.Name = "btn_begin"
    Me.btn_begin.Size = New System.Drawing.Size(46, 22)
    Me.btn_begin.Text = "xBegin"
    '
    'lbl_message
    '
    Me.lbl_message.Name = "lbl_message"
    Me.lbl_message.Size = New System.Drawing.Size(58, 22)
    Me.lbl_message.Text = "xMessage"
    Me.lbl_message.Visible = False
    '
    'Panel1
    '
    Me.Panel1.Controls.Add(Me.Preview)
    Me.Panel1.Controls.Add(Me.tls_pager)
    Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
    Me.Panel1.Location = New System.Drawing.Point(207, 0)
    Me.Panel1.Name = "Panel1"
    Me.Panel1.Size = New System.Drawing.Size(653, 508)
    Me.Panel1.TabIndex = 3
    '
    'Preview
    '
    Me.Preview.AutoScroll = True
    Me.Preview.Dock = System.Windows.Forms.DockStyle.Fill
    Me.Preview.Document = Nothing
    Me.Preview.Location = New System.Drawing.Point(0, 0)
    Me.Preview.Name = "Preview"
    Me.Preview.Size = New System.Drawing.Size(653, 483)
    Me.Preview.TabIndex = 2
    Me.Preview.ZoomMode = GUI_Reports.ZoomMode.FullPage
    '
    'frm_print_preview
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(860, 508)
    Me.Controls.Add(Me.Panel1)
    Me.Controls.Add(Me.pnl_function_buttons)
    Me.MinimizeBox = False
    Me.Name = "frm_print_preview"
    Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
    Me.Text = "frm_print_preview"
    Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
    Me.pnl_function_buttons.ResumeLayout(False)
    CType(Me.nud_number_copies, System.ComponentModel.ISupportInitialize).EndInit()
    Me.gb_scaling.ResumeLayout(False)
    Me.gb_scaling.PerformLayout()
    CType(Me.nud_height_pages, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.nud_width_pages, System.ComponentModel.ISupportInitialize).EndInit()
    Me.gb_orientation.ResumeLayout(False)
    Me.gb_orientation.PerformLayout()
    Me.tls_pager.ResumeLayout(False)
    Me.tls_pager.PerformLayout()
    Me.Panel1.ResumeLayout(False)
    Me.Panel1.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents Preview As GUI_Reports.uc_print_preview
  Friend WithEvents pnl_function_buttons As System.Windows.Forms.Panel
  Friend WithEvents btn_print As System.Windows.Forms.Button
  Friend WithEvents gb_orientation As System.Windows.Forms.GroupBox
  Friend WithEvents opt_landscape As System.Windows.Forms.RadioButton
  Friend WithEvents opt_portrait As System.Windows.Forms.RadioButton
  Friend WithEvents gb_scaling As System.Windows.Forms.GroupBox
  Friend WithEvents opt_scaling_no As System.Windows.Forms.RadioButton
  Friend WithEvents opt_scaling_one_page As System.Windows.Forms.RadioButton
  Friend WithEvents opt_scaling_cols_one_page As System.Windows.Forms.RadioButton
  Friend WithEvents opt_scaling_custom As System.Windows.Forms.RadioButton
  Friend WithEvents opt_scaling_rows_one_page As System.Windows.Forms.RadioButton
  Friend WithEvents nud_width_pages As System.Windows.Forms.NumericUpDown
  Friend WithEvents lbl_height_pages As System.Windows.Forms.Label
  Friend WithEvents lbl_width_pages As System.Windows.Forms.Label
  Friend WithEvents nud_height_pages As System.Windows.Forms.NumericUpDown
  Friend WithEvents lbl_number_copies As System.Windows.Forms.Label
  Friend WithEvents nud_number_copies As System.Windows.Forms.NumericUpDown
  Friend WithEvents btn_close As System.Windows.Forms.Button
  Friend WithEvents tls_pager As System.Windows.Forms.ToolStrip
  Friend WithEvents ef_start_page As System.Windows.Forms.ToolStripTextBox
  Friend WithEvents lbl_page_count As System.Windows.Forms.ToolStripLabel
  Friend WithEvents btn_end As System.Windows.Forms.ToolStripButton
  Private WithEvents btn_begin As System.Windows.Forms.ToolStripButton
  Private WithEvents btn_Prev As System.Windows.Forms.ToolStripButton
  Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
  Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
  Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
  Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
  Private WithEvents btn_next As System.Windows.Forms.ToolStripButton
  Friend WithEvents Panel1 As System.Windows.Forms.Panel
  Friend WithEvents lbl_message As System.Windows.Forms.ToolStripLabel
End Class
