'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:      frm_excel_progress.vb  
'
' DESCRIPTION:      Progress bar used during export to excel.
'
' AUTHOR:           Andreu Juli�
'
' CREATION DATE:    08-FEB-2011
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ ----------------------------------------------
' 08-FEB-2011  AJQ    Initial version.
' 08-AUG-2013  JAB    Add constructor to indicate the call origin.
'-------------------------------------------------------------------

Imports System.Windows.Forms

Public Class frm_excel_progress

  Private m_cancelled As Boolean = False
  Private m_updated As Integer = 0
  Private m_times_updated As Integer = 0

  Public Property Cancelled()
    Get
      Return m_cancelled
    End Get
    Set(ByVal value)
      m_cancelled = value
    End Set
  End Property

  Public Sub New(Optional ByVal FormName As String = "")

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.
    If Not String.IsNullOrEmpty(FormName) Then
      Me.Text = FormName
    End If

  End Sub

  Public Sub SetValues(ByVal Index As Integer, ByVal Num As Integer)
    Dim _elapsed As Long
    Dim _update As Boolean

    _update = (Index = 0 Or Index = Num)

    If Not _update Then
      _elapsed = GetElapsedTicks(m_updated, Environment.TickCount)
      _update = (_elapsed >= 1000)
    End If

    If _update Then
      Me.ProgressBar1.Minimum = 0
      Me.ProgressBar1.Maximum = Num
      Me.ProgressBar1.Value = Index

      ' Need to refresh to process Windows Events.
      Application.DoEvents()

      m_updated = Environment.TickCount
      m_times_updated += 1
    End If

    If m_times_updated >= 30 Then
      m_times_updated = 0
      WSI.Common.Users.SetLastAction("InProgress", Nothing)
    End If
  End Sub

  Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    Cancelled = True
  End Sub

  Private Shared Function GetElapsedTicks(ByVal FirstTick As Integer, ByVal LastTick As Integer) As Long
    Dim _elapsed As Long

    _elapsed = LastTick
    If LastTick < FirstTick Then
      _elapsed += UInt32.MaxValue
      _elapsed += 1
    End If
    _elapsed -= FirstTick

    If _elapsed < 0 Then
      _elapsed = 0
    End If

    Return _elapsed
  End Function

End Class
