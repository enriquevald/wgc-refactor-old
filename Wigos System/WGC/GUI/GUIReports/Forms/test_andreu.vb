Public Class test_andreu
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
  Friend WithEvents Button1 As System.Windows.Forms.Button
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.Button1 = New System.Windows.Forms.Button()
    Me.SuspendLayout()
    '
    'Button1
    '
    Me.Button1.Location = New System.Drawing.Point(80, 112)
    Me.Button1.Name = "Button1"
    Me.Button1.TabIndex = 0
    Me.Button1.Text = "Button1"
    '
    'test_andreu
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.ClientSize = New System.Drawing.Size(292, 273)
    Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.Button1})
    Me.Name = "test_andreu"
    Me.Text = "test_andreu"
    Me.ResumeLayout(False)

  End Sub

#End Region

  Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
    Dim kk As New CLASS_PRINT_DATA()


    'Dim report As New rpt_0001()
    'Dim dataset As New PrintDataset()
    'Dim table_report_values As New PrintDataset.PrintDataDataTable()
    'Dim table_params As New PrintDataset.PrintParamsDataTable()
    Dim row_report_values As PrintDataset.PrintDataRow
    Dim idx As Integer

    ' Add one row with the params of the report

    Dim print_data As New CLASS_PRINT_DATA()


    ' Fill row
    print_data.Params.Title = "Listado de Ventas"
    print_data.Params.Header01 = "Fecha"
    print_data.Params.Header02 = "Op."
    print_data.Params.Header03 = "Dis."
    print_data.Params.Header04 = "Ag."
    print_data.Params.Header05 = "Juego"
    print_data.Params.Header06 = "Tickets"
    print_data.Params.Header07 = "Importe"

    '''''''''''''''''''''''''''''''''''
    Dim ii As Integer
    For ii = 0 To 10
      'total day
      row_report_values = print_data.NewRowData()
      row_report_values.Column01 = CStr(Now.AddDays(ii))
      row_report_values.Column02 = "2-1"
      row_report_values.Column03 = "Dis." & CStr(idx \ 10)
      row_report_values.Column04 = "Ag." & CStr(idx \ 5)
      row_report_values.Column05 = "Juego " & CStr(idx Mod 4)
      row_report_values.Column06 = CStr(4444444)
      row_report_values.Column07 = Format(999999, "0.00")
      row_report_values.Type = 4
      Call print_data.AddRowData(row_report_values)


      'total oper
      row_report_values = print_data.NewRowData()
      row_report_values.Column01 = CStr(Now.AddDays(ii))
      row_report_values.Column02 = "2-1"
      row_report_values.Column03 = "Dis." & CStr(idx \ 10)
      row_report_values.Column04 = "Ag." & CStr(idx \ 5)
      row_report_values.Column05 = "Juego " & CStr(idx Mod 4)
      row_report_values.Column06 = CStr(333333)
      row_report_values.Column07 = Format(999999, "0.00")
      row_report_values.Type = 3
      Call print_data.AddRowData(row_report_values)

      'total distr
      row_report_values = print_data.NewRowData()
      row_report_values.Column01 = CStr(Now.AddDays(ii))
      row_report_values.Column02 = "2-1"
      row_report_values.Column03 = "Dis." & CStr(idx \ 10)
      row_report_values.Column04 = "Ag." & CStr(idx \ 5)
      row_report_values.Column05 = "Juego " & CStr(idx Mod 4)
      row_report_values.Column06 = CStr(22222)
      row_report_values.Column07 = Format(999999, "0.00")
      row_report_values.Type = 2
      Call print_data.AddRowData(row_report_values)

      'total agency
      row_report_values = print_data.NewRowData()
      row_report_values.Column01 = CStr(Now.AddDays(ii))
      row_report_values.Column02 = "2-1"
      row_report_values.Column03 = "Dis." & CStr(idx \ 10)
      row_report_values.Column04 = "Ag." & CStr(idx \ 5)
      row_report_values.Column05 = "Juego " & CStr(idx Mod 4)
      row_report_values.Column06 = CStr(11111)
      row_report_values.Column07 = Format(999999, "0.00")
      row_report_values.Type = 1
      Call print_data.AddRowData(row_report_values)

      For idx = 1 To 5
        row_report_values = print_data.NewRowData()
        row_report_values.Column01 = CStr(Now.AddDays(ii))
        row_report_values.Column02 = "Op."
        row_report_values.Column03 = "Dis."
        row_report_values.Column04 = "Ag."
        row_report_values.Column05 = "Juego " & CStr(idx)
        row_report_values.Column06 = CStr(idx * 100)
        row_report_values.Column07 = Format(idx * 125.1, "0.00")
        row_report_values.Type = 0
        Call print_data.AddRowData(row_report_values)

      Next
    Next

    Call print_data.Preview(0)

  End Sub
End Class
