'-------------------------------------------------------------------
' Copyright � 2011 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   mdl_globals.vb  
'
' DESCRIPTION:   
'
' AUTHOR:        Miquel Beltran
'
' CREATION DATE: 12-JAN-2011
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 12-JAN-2011  MBF    Initial version
'-------------------------------------------------------------------

Imports GUI_CommonMisc

Module MODULE_GLOBALS
  Public GLB_NLS_GUI_CONTROLS As CLASS_MODULE_NLS = New CLASS_MODULE_NLS(ENUM_NLS_MODULES.COMMON_MODULE_GUI_CONTROLS)
  Public GLB_NLS_GUI_COMMONMISC As New CLASS_MODULE_NLS(ENUM_NLS_MODULES.COMMON_MODULE_GUI_COMMONMISC)
  Public GLB_NLS_GUI_CONFIGURATION As New CLASS_MODULE_NLS(ENUM_NLS_MODULES.COMMON_MODULE_GUI_CONF)
  Public GLB_NLS_GUI_STATISTICS As New CLASS_MODULE_NLS(ENUM_NLS_MODULES.COMMON_MODULE_GUI_STATISTICS)
  Public GLB_NLS_GUI_PLAYER_TRACKING As New CLASS_MODULE_NLS(ENUM_NLS_MODULES.COMMON_MODULE_GUI_PLAYER_TRACKING)

  Public GLB_TRX_AUDIT_NLS As New CLASS_MODULE_NLS(ENUM_NLS_MODULES.COMMON_TRX_AUDIT)

End Module
