'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:      cls_page_image.vb  
'
' DESCRIPTION:      Contains excel document pages in image format.
'
' AUTHOR:           Jes�s �ngel Blanco Blanco
'
' CREATION DATE:    08-AUG-2013
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ ----------------------------------------------
' 08-AUG-2013  JAB    Initial version.
'-------------------------------------------------------------------

Imports System.IO
Imports System.ComponentModel
Imports System.Drawing.Imaging

' use regular generic list
Friend Class cls_page_image_list
  Inherits List(Of System.Drawing.Image)
End Class
