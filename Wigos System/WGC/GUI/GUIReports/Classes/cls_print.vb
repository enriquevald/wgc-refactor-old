'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_print.vb
' DESCRIPTION:   Print
' AUTHOR:        Jorge Oliva
' CREATION DATE: 20-JAN-2005
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 20-JAN-2005  JOR    Initial version
' 18-FEB-2013  JAB    Negative values are not valid for variable "Report.ReportDefinition.ReportObjects.Item(idx_field).Left"
' 21-FEB-2013  JAB    Fixed defect #598: Fails to display the header in print
' 13-NOV-2013  LJM    Added a function to get the constant MAXNUMFILTERS
'-------------------------------------------------------------------

Imports CrystalDecisions.Windows.Forms
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports GUI_CommonMisc.public_globals

'XVV -> Add NameSpace Windows.Forms,solve error with IWin32Window variables definitions
Imports System.Windows.Forms
Imports System.Drawing
'XVV-----------------------------------

Public Class CLASS_PRINT_DATA

#Region " Members "

  Private m_preview_form As frm_test
  Private m_print_dataset As PrintDataset
  Private m_settings As CLASS_SETTINGS



#End Region ' Members

#Region " Constants "

  Private Const PRINTER_MARGIN As Integer = 0 'Letter: 1100 - A4: 350
  Private Const WIDTH_SPACE As Integer = 150
  Private Const APPEND_SUFIX As String = ":"

  Private Const MAX_FILTERS_PER_BLOCK As Integer = 5
  Private Const MAX_FILTER_BLOCKS As Integer = 4
  Private Const MAX_NUM_FILTERS As Integer = MAX_FILTERS_PER_BLOCK * MAX_FILTER_BLOCKS

  Private Const MAX_FOOTERS_PER_BLOCK As Integer = 10
  Private Const MAX_FOOTER_BLOCKS As Integer = 4
  Private Const MAX_NUM_FOOTERS As Integer = MAX_FOOTERS_PER_BLOCK * MAX_FOOTER_BLOCKS

  Public Const MAX_COL_DATA As Integer = 20
  Public Const MAX_HEADER_DATA As Integer = MAX_COL_DATA * 2 ' max two headers per column

#End Region ' Constants

#Region " SubClasses "

  Public Class CLASS_SETTINGS

#Region " Members "

    Private m_column(MAX_COL_DATA) As CLASS_COL_SETTINGS
    Private m_header(MAX_HEADER_DATA) As CLASS_HEADER_SETTINGS
    Private m_total_var_width As Long
    Private m_total_fixed_width As Long

    Private m_orientation As ENUM_ORIENTATION

    Private m_filter_resize_allowed As Boolean
    Private m_report_type As ENUM_REPORT_TYPE

    Private m_num_filters As Integer
    Private m_filter_header_width(MAX_FILTER_BLOCKS) As Long
    Private m_filter_value_width(MAX_FILTER_BLOCKS) As Long

    Private m_num_footers As Integer
    Private m_footer_header_width(MAX_FOOTER_BLOCKS) As Long
    Private m_footer_value_width(MAX_FOOTER_BLOCKS) As Long
    Private m_footer_header_alignment(MAX_NUM_FOOTERS) As ENUM_ORIENTATION
    Private m_footer_value_alignment(MAX_NUM_FOOTERS) As ENUM_ORIENTATION
    Private m_footer_header_font(MAX_NUM_FOOTERS) As Font
    Private m_footer_value_font(MAX_NUM_FOOTERS) As Font

#End Region ' Members

#Region " Variables "

    Private GLB_FooterHeaderFont As New Font("Verdana", 8, FontStyle.Regular)
    Private GLB_FooterValueFont As New Font("Verdana", 8, FontStyle.Bold)

#End Region ' Variables

#Region " Constants "

    Public Enum ENUM_ALIGN
      ALIGN_LEFT = CrystalDecisions.Shared.Alignment.LeftAlign
      ALIGN_RIGHT = CrystalDecisions.Shared.Alignment.RightAlign
      ALIGN_CENTER = CrystalDecisions.Shared.Alignment.HorizontalCenterAlign
    End Enum

    Public Enum ENUM_ORIENTATION
      ORIENTATION_DEFAULT = PaperOrientation.DefaultPaperOrientation
      ORIENTATION_LANDSCAPE = PaperOrientation.Landscape
      ORIENTATION_PORTRAIT = PaperOrientation.Portrait
    End Enum

    Public Enum ENUM_REPORT_TYPE
      REPORT_TYPE_DEFAULT = 1
      REPORT_TYPE_MISC = 2
      REPORT_TYPE_INVOICE = 3
    End Enum

#End Region ' Constants

    Public Sub New()
      Dim idx As Integer

      ' Class initialization
      Me.m_orientation = ENUM_ORIENTATION.ORIENTATION_LANDSCAPE

      Me.m_filter_resize_allowed = True
      Me.m_report_type = ENUM_REPORT_TYPE.REPORT_TYPE_MISC

      ' Subclasses initialization: Header and Data
      For idx = 0 To MAX_COL_DATA - 1
        ' Data subclass initialization
        m_column(idx) = New CLASS_COL_SETTINGS(Me, idx)
        m_column(idx).Width = 0
        m_column(idx).Alignment = ENUM_ALIGN.ALIGN_RIGHT

      Next
      For idx = 0 To MAX_HEADER_DATA - 1
        ' Header subclass initialization
        m_header(idx) = New CLASS_HEADER_SETTINGS(Me, idx)
        m_header(idx).Left = 0
        m_header(idx).Width = 0

      Next

      For idx = 0 To MAX_FILTER_BLOCKS - 1
        m_filter_header_width(idx) = 0
        m_filter_value_width(idx) = 0

      Next

      For idx = 0 To MAX_FOOTER_BLOCKS - 1
        m_footer_header_width(idx) = 0
        m_footer_value_width(idx) = 0

      Next

      For idx = 0 To MAX_NUM_FOOTERS - 1
        m_footer_header_alignment(idx) = ENUM_ALIGN.ALIGN_RIGHT
        m_footer_value_alignment(idx) = ENUM_ALIGN.ALIGN_LEFT
        m_footer_header_font(idx) = GLB_FooterHeaderFont
        m_footer_value_font(idx) = GLB_FooterValueFont

      Next

      m_num_filters = 0
      m_num_footers = 0
      m_total_var_width = 0
      m_total_fixed_width = 0

    End Sub

    Public Function NumTopHeaders() As Integer
      Dim idx_header As Integer

      For idx_header = MAX_COL_DATA To MAX_HEADER_DATA - 1
        If m_header(idx_header).StartCol = -1 Then
          Return (idx_header - MAX_COL_DATA)
        End If

      Next

    End Function

    Public Function NumColumns() As Integer
      Dim idx_col As Integer

      For idx_col = 0 To MAX_COL_DATA - 1
        If m_column(idx_col).IsNull Then
          Return idx_col
        End If

      Next

    End Function

    Public ReadOnly Property Headers(ByVal Index As Integer) As CLASS_HEADER_SETTINGS
      Get
        Return m_header(Index)
      End Get
    End Property

    Public ReadOnly Property Columns(ByVal Index As Integer) As CLASS_COL_SETTINGS
      Get
        Return m_column(Index)
      End Get
    End Property

    Public Property Orientation() As ENUM_ORIENTATION
      Get
        Return m_orientation
      End Get

      Set(ByVal Value As ENUM_ORIENTATION)
        m_orientation = Value
      End Set

    End Property

    Public Property FilterResizeAllowed() As Boolean
      Get
        Return m_filter_resize_allowed
      End Get

      Set(ByVal Value As Boolean)
        m_filter_resize_allowed = Value
      End Set

    End Property

    Public Property ReportType() As ENUM_REPORT_TYPE
      Get
        Return m_report_type
      End Get

      Set(ByVal Value As ENUM_REPORT_TYPE)
        m_report_type = Value
      End Set

    End Property

    Public Property TotalVarWidth() As Long
      Get
        Return m_total_var_width
      End Get
      Set(ByVal Value As Long)
        m_total_var_width = Value
      End Set
    End Property

    Public Property TotalFixedWidth() As Long
      Get
        Return m_total_fixed_width
      End Get
      Set(ByVal Value As Long)
        m_total_fixed_width = Value
      End Set
    End Property

    Public Property NumFilters() As Integer
      Get
        Return m_num_filters
      End Get

      Set(ByVal Value As Integer)
        m_num_filters = Value
      End Set

    End Property

    Public Property FilterHeaderWidth(ByVal Index As Integer) As Long
      Get
        If Index < MAX_FILTER_BLOCKS Then
          Return m_filter_header_width(Index)
        Else
          Return 0
        End If
      End Get
      Set(ByVal Value As Long)
        If Index < MAX_FILTER_BLOCKS Then
          m_filter_header_width(Index) = Value
        End If
      End Set
    End Property

    Public Property FilterValueWidth(ByVal Index As Integer) As Long
      Get
        If Index < MAX_FILTER_BLOCKS Then
          Return m_filter_value_width(Index)
        Else
          Return 0
        End If
      End Get
      Set(ByVal Value As Long)
        If Index < MAX_FILTER_BLOCKS Then
          m_filter_value_width(Index) = Value
        End If
      End Set
    End Property

    Public Property NumFooters() As Integer
      Get
        Return m_num_footers
      End Get

      Set(ByVal Value As Integer)
        m_num_footers = Value
      End Set

    End Property

    Public Property FooterHeaderWidth(ByVal Index As Integer) As Long
      Get
        If Index < MAX_FOOTER_BLOCKS Then
          Return m_footer_header_width(Index)
        Else
          Return 0
        End If
      End Get
      Set(ByVal Value As Long)
        If Index < MAX_FOOTER_BLOCKS Then
          m_footer_header_width(Index) = Value
        End If
      End Set
    End Property

    Public Property FooterValueWidth(ByVal Index As Integer) As Long
      Get
        If Index < MAX_FOOTER_BLOCKS Then
          Return m_footer_value_width(Index)
        Else
          Return 0
        End If
      End Get
      Set(ByVal Value As Long)
        If Index < MAX_FOOTER_BLOCKS Then
          m_footer_value_width(Index) = Value
        End If
      End Set
    End Property

    Public Property FooterHeaderAlignment(ByVal Index As Integer) As ENUM_ALIGN
      Get
        Return m_footer_header_alignment(Index)
      End Get

      Set(ByVal Value As ENUM_ALIGN)
        If Index < MAX_NUM_FOOTERS Then
          m_footer_header_alignment(Index) = Value
        End If
      End Set
    End Property

    Public Property FooterValueAlignment(ByVal Index As Integer) As ENUM_ALIGN
      Get
        Return m_footer_value_alignment(Index)
      End Get

      Set(ByVal Value As ENUM_ALIGN)
        If Index < MAX_NUM_FOOTERS Then
          m_footer_value_alignment(Index) = Value
        End If
      End Set
    End Property

    Public Property FooterHeaderFont(ByVal Index As Integer) As Font
      Get
        Return m_footer_header_font(Index)
      End Get

      Set(ByVal Value As Font)
        If Index < MAX_NUM_FOOTERS Then
          m_footer_header_font(Index) = Value
        End If
      End Set
    End Property

    Public Property FooterValueFont(ByVal Index As Integer) As Font
      Get
        Return m_footer_value_font(Index)
      End Get

      Set(ByVal Value As Font)
        If Index < MAX_NUM_FOOTERS Then
          m_footer_value_font(Index) = Value
        End If
      End Set
    End Property

    ' Subclass header
    Public Class CLASS_HEADER_SETTINGS
      Private m_settings As CLASS_SETTINGS
      Private m_index As Integer
      Private m_left As Long
      Private m_width As Long
      Private m_start_col As Integer
      Private m_end_col As Integer

      Public Sub New(ByVal Settings As CLASS_SETTINGS, ByVal Index As Integer)
        m_settings = Settings
        m_index = Index
        m_start_col = -1
        m_end_col = -1
      End Sub

      Public Property Left() As Long
        Get
          Dim idx As Long
          Dim left_value As Long = 0

          If m_settings.m_header(m_index).m_left > 0 Or m_index >= MAX_COL_DATA Then
            left_value = m_settings.m_header(m_index).m_left
          Else
            For idx = 0 To m_index - 1
              left_value = left_value + m_settings.m_header(m_index).m_width
            Next
          End If
          Return left_value
        End Get

        Set(ByVal Value As Long)
          m_settings.m_header(m_index).m_left = Value
        End Set
      End Property

      Public Property Width() As Long
        Get
          If m_settings.m_header(m_index).m_width > WIDTH_SPACE Then
            Return m_settings.m_header(m_index).m_width - WIDTH_SPACE
          End If
        End Get
        Set(ByVal Value As Long)
          m_settings.m_header(m_index).m_width = Value
        End Set
      End Property

      Public Property StartCol() As Integer
        Get
          Return m_start_col
        End Get
        Set(ByVal Value As Integer)
          m_start_col = Value
        End Set
      End Property

      Public Property EndCol() As Integer
        Get
          Return m_end_col
        End Get
        Set(ByVal Value As Integer)
          m_end_col = Value
        End Set
      End Property

    End Class

    ' Subclass data
    Public Class CLASS_COL_SETTINGS
      Private m_settings As CLASS_SETTINGS
      Private m_index As Long
      Private m_width As Long
      Private m_left As Long
      Private m_align As ENUM_ALIGN
      Private m_fixed As Boolean
      Private m_is_null As Boolean

      Public Sub New(ByVal first As CLASS_SETTINGS, ByVal Index As Integer)
        m_settings = first
        m_index = Index
        m_fixed = False
        m_is_null = True
      End Sub

      Public ReadOnly Property WidthSpecial() As Long
        Get
          Return m_settings.m_column(m_index).m_width
        End Get
      End Property

      Public Property Width() As Long
        Get
          If m_settings.m_column(m_index).m_width > WIDTH_SPACE Then
            Return m_settings.m_column(m_index).m_width - WIDTH_SPACE
          End If
        End Get

        Set(ByVal Value As Long)
          m_settings.m_column(m_index).m_width = Value
          If Value > 0 Then
            m_is_null = False
          End If
        End Set
      End Property

      Public Property Left() As Long
        Get
          Dim idx As Long
          Dim left_value As Long = 0

          If m_settings.m_column(m_index).m_left > 0 Or m_index >= MAX_COL_DATA Then
            left_value = m_settings.m_column(m_index).m_left
          Else
            For idx = 0 To m_index - 1
              left_value = left_value + m_settings.m_column(idx).m_width
            Next
          End If
          Return left_value

        End Get

        Set(ByVal Value As Long)
          m_settings.m_column(m_index).m_left = Value
        End Set
      End Property

      Public Property Alignment() As ENUM_ALIGN

        Get
          Return m_settings.m_column(m_index).m_align
        End Get

        Set(ByVal Value As ENUM_ALIGN)
          m_settings.m_column(m_index).m_align = Value
        End Set

      End Property

      Public Property IsWidthFixed() As Boolean
        Get
          Return m_settings.m_column(m_index).m_fixed
        End Get

        Set(ByVal Value As Boolean)
          m_settings.m_column(m_index).m_fixed = Value
        End Set

      End Property

      Public ReadOnly Property IsNull() As Boolean
        Get
          Return m_is_null
        End Get
      End Property

    End Class

  End Class

#End Region ' SubClasses

#Region " Public Funcions "

  Public Sub AddRowData(ByVal RowData As PrintDataset.PrintDataRow)
    Call m_print_dataset.PrintData.Rows.Add(RowData)
  End Sub ' AddRowData

  Public Sub ClearRowData()
    Call m_print_dataset.PrintData.Rows.Clear()
  End Sub ' ClearRowData

  ' PURPOSE: Returns the constant MAX_NUM_FILTERS
  '
  '  PARAMS:
  '     - INPUT
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - MAX_NUM_FILTERS
  Public Function GetMaxNumFilters() As Integer
    Return MAX_NUM_FILTERS
  End Function

  ' PURPOSE: Set the given filter into the filters array
  '
  '  PARAMS:
  '     - INPUT
  '           - FilterHeader
  '           - FilterValue
  '           - LeaveBlank - if to leave filter blank on purpose
  '     - OUTPUT:
  '            - none
  '
  ' RETURNS:
  '     - None
  Public Sub SetFilter(ByVal FilterHeader As String, _
                       ByVal FilterValue As String, _
                       Optional ByVal LeaveFilterBlank As Boolean = False, _
                       Optional ByVal LeaveFormatFilter As Boolean = False)

    If FilterValue Is Nothing Then
      FilterValue = ""
    End If

    If LeaveFilterBlank Then
      m_settings.NumFilters = m_settings.NumFilters + 1
      Exit Sub

    ElseIf FilterHeader.Length > 0 Or FilterValue.Length > 0 Then
      Filter.Item(m_settings.NumFilters) = FormatHeader(FilterHeader)
      If Not LeaveFormatFilter Then
        Filter.Item(m_settings.NumFilters + MAX_NUM_FILTERS) = FormatFilterValue(FilterValue)
      Else
        Filter.Item(m_settings.NumFilters + MAX_NUM_FILTERS) = FilterValue
      End If
      m_settings.NumFilters = m_settings.NumFilters + 1

    End If

  End Sub ' SetFilter

  ' PURPOSE: Set a number of filters blank (for appearance purposes)
  '
  '  PARAMS:
  '     - INPUT
  '           - NumBlankFilters - number of filters to leave blank on purpose
  '     - OUTPUT:
  '            - none
  '
  ' RETURNS:
  '     - None
  Public Sub SetBlankFilters(Optional ByVal NumBlankFilters As Integer = MAX_FILTERS_PER_BLOCK)

    m_settings.NumFilters = m_settings.NumFilters + NumBlankFilters

  End Sub ' SetBlankFilters

  ' PURPOSE: Set the given footer into the footers array
  '
  '  PARAMS:
  '     - INPUT
  '           - FooterHeader
  '           - FooterValue
  '           - LeaveBlank - optional, if to leave footer blank on purpose
  '           - FooterHeaderAlignment - optional, change header alignment
  '           - FooterValueAlignment - optional, change value alignment
  '           - FooterHeaderFont - optional, change header font
  '           - FooterValueFont - optional, change value font
  '     - OUTPUT:
  '            - none
  '
  ' RETURNS:
  '     - None
  Public Sub SetFooter(ByVal FooterHeader As String, _
                       ByVal FooterValue As String, _
                       Optional ByVal LeaveFooterBlank As Boolean = False, _
                       Optional ByVal FooterHeaderAlignment As CLASS_SETTINGS.ENUM_ALIGN = CLASS_SETTINGS.ENUM_ALIGN.ALIGN_RIGHT, _
                       Optional ByVal FooterValueAlignment As CLASS_SETTINGS.ENUM_ALIGN = CLASS_SETTINGS.ENUM_ALIGN.ALIGN_LEFT, _
                       Optional ByVal FooterHeaderFont As Font = Nothing, _
                       Optional ByVal FooterValueFont As Font = Nothing)

    ReportWithFooter = True

    If LeaveFooterBlank Then
      m_settings.NumFooters = m_settings.NumFooters + 1
      Return

    ElseIf FooterHeader.Length > 0 Or FooterValue.Length > 0 Then
      Footer.Item(m_settings.NumFooters) = FormatFooterHeader(FooterHeader)
      Footer.Item(m_settings.NumFooters + MAX_NUM_FOOTERS) = FooterValue
      m_settings.FooterHeaderAlignment(m_settings.NumFooters) = FooterHeaderAlignment
      m_settings.FooterValueAlignment(m_settings.NumFooters) = FooterValueAlignment

      If Not (FooterHeaderFont Is Nothing) Then
        m_settings.FooterHeaderFont(m_settings.NumFooters) = FooterHeaderFont
      End If

      If Not (FooterValueFont Is Nothing) Then
        m_settings.FooterValueFont(m_settings.NumFooters) = FooterValueFont
      End If

      m_settings.NumFooters = m_settings.NumFooters + 1

    End If

  End Sub ' SetFooter

  ' PURPOSE: Set a number of footers blank (for appearance purposes)
  '
  '  PARAMS:
  '     - INPUT
  '           - NumBlankFooters - number of footers to leave blank on purpose
  '     - OUTPUT:
  '            - none
  '
  ' RETURNS:
  '     - None
  Public Sub SetBlankFooters(Optional ByVal NumBlankFooters As Integer = MAX_FOOTERS_PER_BLOCK)

    ReportWithFooter = True
    m_settings.NumFooters = m_settings.NumFooters + NumBlankFooters

  End Sub ' SetBlankFooters

  Public Sub New()

    m_settings = New CLASS_SETTINGS()

    m_print_dataset = New PrintDataset()
    ' Creates the row for the filter
    m_print_dataset.PrintFilter.Rows.Add(m_print_dataset.PrintFilter.NewPrintFilterRow)
    ' Creates the row for the params
    m_print_dataset.PrintParams.Rows.Add(m_print_dataset.PrintParams.NewPrintParamsRow)
    ' Creates the row for the footer
    m_print_dataset.PrintFooter.Rows.Add(m_print_dataset.PrintFooter.NewPrintFooterRow)

    ReportWithFooter = False

  End Sub ' New

  Public Sub Preview(Optional ByVal OnlyPrint As Boolean = False)

    Dim frm As New frm_test()
    Dim report As Object
    Dim current_dir As String

    Select Case (m_settings.ReportType)
      Case CLASS_SETTINGS.ENUM_REPORT_TYPE.REPORT_TYPE_MISC
        report = New rpt_misc()

      Case CLASS_SETTINGS.ENUM_REPORT_TYPE.REPORT_TYPE_INVOICE
        report = New rpt_invoice()

      Case Else
        Exit Sub
    End Select

    Call ApplySettings(report)
    report.SetDataSource(Me.m_print_dataset)
    frm.crystal_report_viewer.ReportSource = report

    If OnlyPrint Then
      report.PrintOptions.PrinterName = GLB_PrinterSettings.PrinterName
      report.PrintToPrinter(GLB_PrinterSettings.Copies, False, 0, 0)
    Else
      ' LRS, 14-APR-2004, Save the current Directory to preserve it.
      current_dir = CurDir()
      Call frm.ShowDialog()
      Call ChDir(current_dir)
    End If

  End Sub ' Preview

#End Region ' Public Funcions

#Region " Public Properties "

  Public ReadOnly Property Settings() As CLASS_SETTINGS
    Get
      Return m_settings
    End Get
  End Property

  Public ReadOnly Property NewRowData() As PrintDataset.PrintDataRow
    Get
      Return m_print_dataset.PrintData.NewRow
    End Get
  End Property

  Public ReadOnly Property Filter() As PrintDataset.PrintFilterRow
    Get
      Return m_print_dataset.PrintFilter(0)
    End Get
  End Property

  Public ReadOnly Property Footer() As PrintDataset.PrintFooterRow
    Get
      Return m_print_dataset.PrintFooter(0)
    End Get
  End Property

  Public ReadOnly Property Params() As PrintDataset.PrintParamsRow
    Get
      Return m_print_dataset.PrintParams(0)
    End Get
  End Property

  Public WriteOnly Property ColumnHeader(ByVal ColumnIndex As Integer) As String
    Set(ByVal Value As String)
      Params.Item(ColumnIndex + 1) = Value
    End Set
  End Property

  Public ReadOnly Property DataSet() As PrintDataset
    Get
      Return m_print_dataset
    End Get
  End Property

  Public WriteOnly Property FilterHeaderWidth(ByVal BlockIndex As Integer) As Long
    Set(ByVal Value As Long)
      m_settings.FilterHeaderWidth(BlockIndex - 1) = Value
    End Set
  End Property

  Public WriteOnly Property FilterValueWidth(ByVal BlockIndex As Integer) As Long
    Set(ByVal Value As Long)
      m_settings.FilterValueWidth(BlockIndex - 1) = Value
    End Set
  End Property

  Public WriteOnly Property FooterHeaderWidth(ByVal BlockIndex As Integer) As Long
    Set(ByVal Value As Long)
      m_settings.FooterHeaderWidth(BlockIndex - 1) = Value
    End Set
  End Property

  Public WriteOnly Property FooterValueWidth(ByVal BlockIndex As Integer) As Long
    Set(ByVal Value As Long)
      m_settings.FooterValueWidth(BlockIndex - 1) = Value
    End Set
  End Property

  Public WriteOnly Property ReportWithFooter() As Boolean
    Set(ByVal Value As Boolean)
      m_print_dataset.PrintFooter(0).ReportWithFooter = Value
    End Set
  End Property

#End Region ' Public Properties

#Region " Private Functions "

  ' PURPOSE: Format the given Filter/Footer Header string to contain ":" at the end
  '
  '  PARAMS:
  '     - INPUT:
  '           - Filter/Footer Header string
  '     - OUTPUT:
  '            - None
  '
  ' RETURNS:
  '     - The formatted Header string
  Private Function FormatHeader(ByVal Header As String) As String

    Dim aux_str As String
    Dim last_char As String

    aux_str = Trim(Header)
    If aux_str.Length = 0 Then
      Return Header
    End If

    last_char = aux_str.Chars(aux_str.Length - 1)
    If last_char <> APPEND_SUFIX Then
      aux_str = Header & APPEND_SUFIX
    Else
      aux_str = Header
    End If

    Return aux_str

  End Function ' FormatHeader

  ' PURPOSE: Format the given Filter/Footer Header string to contain ":" at the end
  '
  '  PARAMS:
  '     - INPUT:
  '           - Filter/Footer Header string
  '     - OUTPUT:
  '            - None
  '
  ' RETURNS:
  '     - The formatted Header string
  Private Function FormatFooterHeader(ByVal Header As String) As String

    Dim aux_str As String

    aux_str = Trim(Header)
    If aux_str.Length = 0 Then
      Return Header
    End If

    Return aux_str

  End Function ' FormatFooterHeader

  ' PURPOSE: Format the given Filter Value
  '
  '  PARAMS:
  '     - INPUT:
  '           - Filter Value string
  '     - OUTPUT:
  '            - None
  '
  ' RETURNS:
  '     - If Value is empty, return "---"; otherwise, return given Value string
  Private Function FormatFilterValue(ByVal Value As String) As String
    Dim aux_str As String

    aux_str = Trim(Value)
    If aux_str.Length = 0 Then
      Return "---"
    Else
      Return Value
    End If

  End Function ' FormatFilterValue

  ' PURPOSE: Apply the corrseponding settings to the given report 
  '
  '  PARAMS:
  '     - INPUT/OUTPUT:
  '                 - Report
  '
  ' RETURNS:
  '     - None
  Private Sub ApplySettings(ByRef Report As Object)

    Dim idx_field As Integer
    Dim idx_column As Integer
    Dim idx_block As Integer
    Dim field_name As String
    Dim coeficient_aux As Double
    Dim add_to_fld_top As Long
    Dim add_to_fld_left As Long
    Dim add_to_title_left As Long
    Dim add_to_title_width As Long
    Dim add_to_filter_header_left(MAX_FILTER_BLOCKS) As Long
    Dim add_to_filter_value_left(MAX_FILTER_BLOCKS) As Long
    Dim add_to_footer_header_left(MAX_FOOTER_BLOCKS) As Long
    Dim add_to_footer_value_left(MAX_FOOTER_BLOCKS) As Long
    Dim add_to_site_left As Long
    Dim add_to_datetime_left As Long
    Dim fixed_width As Long
    Dim var_width As Long

    Const COLUMNS As String = "Detail"
    Const HEADER As String = "Header"
    Const HEADER_FILTER As String = "LabelFilter"
    Const VALUE_FILTER As String = "ValueFilter"
    Const HEADER_FOOTER As String = "LabelFooter"
    Const VALUE_FOOTER As String = "ValueFooter"

    Const FILTER_HEIGHT As Long = 192
    Const FILTER_TOP As Long = 1200
    Const WIDTH_BETWEEN_FILTERS As Long = 360

    Const FOOTER_TOP As Long = 560
    Const WIDTH_BETWEEN_FOOTERS As Long = 360

    Const FIRST_HEADERS_TOP As Long = 350
    Const SECOND_HEADERS_TOP As Long = 734
    Const INVOICE_HEADERS_TOP As Long = 150

    Const PORTRAIT_TO_LANDSCAPE_LEFT As Long = 3600
    Const PORTRAIT_TO_LANDSCAPE_TITLE_LEFT As Long = -680
    Const PORTRAIT_TO_LANDSCAPE_TITLE_WIDTH As Long = 3805
    Const LANDSCAPE_TO_PORTRAIT_TOP As Long = 70
    Const PORTRAIT_SITE_ID_LEFT = -3600
    Const PORTRAIT_TITLE_LEFT As Long = -1200
    Const PORTRAIT_PAGE_NUM_LEFT As Long = -1400

    add_to_title_left = 0
    add_to_title_width = 0
    add_to_fld_left = 0
    add_to_fld_top = 0
    add_to_site_left = 0
    add_to_datetime_left = 0

    If m_settings.TotalVarWidth = 0 Then
      Call UpdateTotalWidth()
    End If

    '--------------------------------------------------------------
    'XVV 13/04/2007
    'Apply refresh Report instance, force recover all information
    Report.refresh()
    '--------------------------------------------------------------

    ' Applied general settings
    With Report.PrintOptions
      .PaperOrientation = m_settings.Orientation
      ' Use add-hoc function instead of predefined one as it does not work as apparently should
      CalculateFixedVarWidth(fixed_width, var_width)
      If var_width > 0 Then
        'coeficient_aux = (.PageContentWidth - m_settings.TotalFixedWidth - PRINTER_MARGIN) / m_settings.TotalVarWidth
        coeficient_aux = (.PageContentWidth - fixed_width - PRINTER_MARGIN) / var_width
      Else
        coeficient_aux = 0
      End If
    End With

    ' Update the columns and headers width according to proportion of page
    Call UpdateColumnsWidth(coeficient_aux, Report.PrintOptions.PageContentWidth)
    Call UpdateHeadersWidth()

    ' Set the coordinates for the title, time and number of pages fields according to orientation
    If Report.PrintOptions.PaperOrientation = CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_LANDSCAPE Then
      add_to_fld_left += PORTRAIT_TO_LANDSCAPE_LEFT
      add_to_title_left += PORTRAIT_TO_LANDSCAPE_TITLE_LEFT
      add_to_title_width += PORTRAIT_TO_LANDSCAPE_TITLE_WIDTH
    Else
      add_to_fld_left += PORTRAIT_PAGE_NUM_LEFT
      add_to_fld_top += LANDSCAPE_TO_PORTRAIT_TOP
      add_to_title_left += PORTRAIT_TITLE_LEFT
      add_to_site_left += PORTRAIT_SITE_ID_LEFT
    End If

    ' Calculate amount to add to left value of filters and footers
    GetFilterFooterLeftValue(add_to_filter_header_left, add_to_filter_value_left, True)
    GetFilterFooterLeftValue(add_to_footer_header_left, add_to_footer_value_left, False)

    ' Applied columns and headers settings
    With Report.ReportDefinition.ReportObjects
      For idx_field = 0 To .count - 1

        If TypeOf (.Item(idx_field)) Is FieldObject Then

          field_name = .Item(idx_field).Name

          Select Case Mid(field_name, 1, 6)
            Case COLUMNS
              Try
                idx_column = Mid(field_name, Len(field_name) - 1, 2)

                ' JAB 18-02-2013: Negative values are not valid for variable ".Item(idx_field).Left"
                If m_settings.Columns(idx_column).Left < 0 Then
                  '.Item(idx_field).Left = m_settings.Columns(idx_column).Left * -1
                  .Item(idx_field).Left = 0
                Else
                  .Item(idx_field).Left = m_settings.Columns(idx_column).Left
                End If

                .Item(idx_field).Width = m_settings.Columns(idx_column).Width
                .Item(idx_field).ObjectFormat.HorizontalAlignment = m_settings.Columns(idx_column).Alignment

              Catch ex As Exception

              End Try

            Case HEADER
              Try
                idx_column = Mid(field_name, Len(field_name) - 1, 2)

                ' JAB 18-02-2013: Negative values are not valid for variable ".Item(idx_field).Left"
                If m_settings.Headers(idx_column).Left < 0 Then
                  '.Item(idx_field).Left = m_settings.Headers(idx_column).Left * -1
                  .Item(idx_field).Left = 0
                Else
                  .Item(idx_field).Left = m_settings.Headers(idx_column).Left
                End If

                ' JAB 21-FEB-2013: Fixed defect #598: Fails to display the header in print
                .Item(idx_field).Width = m_settings.Headers(idx_column).Width

                If idx_column >= MAX_COL_DATA Then
                  If m_settings.ReportType = CLASS_SETTINGS.ENUM_REPORT_TYPE.REPORT_TYPE_INVOICE Then
                    .Item(idx_field).top = INVOICE_HEADERS_TOP
                  Else
                    .Item(idx_field).top = FIRST_HEADERS_TOP
                  End If
                ElseIf idx_column < MAX_COL_DATA Then
                  If m_settings.NumTopHeaders > 0 Then
                    .Item(idx_field).top = SECOND_HEADERS_TOP
                  Else
                    If m_settings.ReportType = CLASS_SETTINGS.ENUM_REPORT_TYPE.REPORT_TYPE_INVOICE Then
                      .Item(idx_field).top = INVOICE_HEADERS_TOP
                    Else
                      .Item(idx_field).top = FIRST_HEADERS_TOP
                    End If

                  End If
                End If
              Catch ex As Exception

              End Try


            Case Else

              Select Case Mid(field_name, 1, 11)

                Case HEADER_FILTER

                  If m_settings.FilterResizeAllowed Then

                    idx_column = CInt(Mid(field_name, 12, 2))
                    idx_block = idx_column \ MAX_FILTERS_PER_BLOCK
                    If m_settings.FilterHeaderWidth(idx_block) > 0 Then
                      .Item(idx_field).Width = m_settings.FilterHeaderWidth(idx_block)
                    End If
                    .Item(idx_field).Left = .Item(idx_field).Left + add_to_filter_header_left(idx_block)
                    If Not (Filter.IsNull(idx_column)) Then
                      .Item(idx_field).height = FILTER_HEIGHT
                      .Item(idx_field).top = FILTER_TOP + _
                            (WIDTH_BETWEEN_FILTERS * (idx_column Mod (MAX_FILTERS_PER_BLOCK)))
                    End If

                  End If

                Case VALUE_FILTER

                  If m_settings.FilterResizeAllowed Then

                    idx_column = CInt(Mid(field_name, 12, 2))
                    idx_block = idx_column \ MAX_FILTERS_PER_BLOCK
                    If m_settings.FilterValueWidth(idx_block) > 0 Then
                      .Item(idx_field).Width = m_settings.FilterValueWidth(idx_block)
                    End If
                    .Item(idx_field).Left = .Item(idx_field).Left + add_to_filter_value_left(idx_block)
                    If Not (Filter.IsNull(idx_column + MAX_NUM_FILTERS)) Then
                      .Item(idx_field).height = FILTER_HEIGHT
                      .Item(idx_field).top = FILTER_TOP + _
                            (WIDTH_BETWEEN_FILTERS * (idx_column Mod (MAX_FILTERS_PER_BLOCK)))
                    End If

                  End If

                Case HEADER_FOOTER

                  If m_settings.FilterResizeAllowed Then

                    idx_column = CInt(Mid(field_name, 12, 2))
                    idx_block = idx_column \ MAX_FOOTERS_PER_BLOCK
                    If m_settings.FooterHeaderWidth(idx_block) > 0 Then
                      .Item(idx_field).Width = m_settings.FooterHeaderWidth(idx_block)
                    End If
                    .Item(idx_field).Left = .Item(idx_field).Left + add_to_footer_header_left(idx_block)
                    If Not (Footer.IsNull(idx_column)) Then
                      .Item(idx_field).top = FOOTER_TOP + _
                           (WIDTH_BETWEEN_FOOTERS * (idx_column Mod (MAX_FOOTERS_PER_BLOCK)))
                    End If
                    .Item(idx_field).ObjectFormat.HorizontalAlignment = m_settings.FooterHeaderAlignment(idx_column)
                    .Item(idx_field).ApplyFont(m_settings.FooterHeaderFont(idx_column))

                  End If

                Case VALUE_FOOTER

                  If m_settings.FilterResizeAllowed Then

                    idx_column = CInt(Mid(field_name, 12, 2))
                    idx_block = idx_column \ MAX_FOOTERS_PER_BLOCK
                    If m_settings.FooterValueWidth(idx_block) > 0 Then
                      .Item(idx_field).Width = m_settings.FooterValueWidth(idx_block)
                    End If
                    .Item(idx_field).Left = .Item(idx_field).Left + add_to_footer_value_left(idx_block)
                    If Not (Footer.IsNull(idx_column + MAX_NUM_FOOTERS)) Then
                      .Item(idx_field).top = FOOTER_TOP + _
                            (WIDTH_BETWEEN_FOOTERS * (idx_column Mod (MAX_FOOTERS_PER_BLOCK)))
                    End If
                    .Item(idx_field).ObjectFormat.HorizontalAlignment = m_settings.FooterValueAlignment(idx_column)
                    .Item(idx_field).ApplyFont(m_settings.FooterValueFont(idx_column))

                  End If

              End Select

              If field_name = "FldDateTime" Then
                .Item(idx_field).Top = .Item(idx_field).Top + add_to_fld_top
              End If

              If field_name = "FldPageNum" Or _
                 field_name = "FldSepPage" Or _
                 field_name = "FldTotalPages" Then

                .Item(idx_field).Left = .Item(idx_field).Left + add_to_fld_left
                .Item(idx_field).Top = .Item(idx_field).Top + add_to_fld_top
              End If

              If field_name = "RptTitle" Then
                .Item(idx_field).Left = .Item(idx_field).Left + add_to_title_left
                .Item(idx_field).Width = .Item(idx_field).Width + add_to_title_width
              End If

              If field_name = "Site1" Then
                .Item(idx_field).Left = .Item(idx_field).Left + add_to_site_left
              End If

          End Select

        ElseIf TypeOf (.Item(idx_field)) Is TextObject Then
          field_name = .Item(idx_field).Name

          If field_name = "RepContinued" Then
            .Item(idx_field).Text = Me.Params.Title & "..."
            .Item(idx_field).Width = 10000
          End If

        End If

        ' In order to move all fields to the left, uncomment and supply proper values...
        If Report.PrintOptions.PaperOrientation = CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_PORTRAIT Then
          '.Item(idx_field).Left += 
        Else
          '.Item(idx_field).Left += 
        End If
      Next

    End With



  End Sub ' ApplySettings

  ' PURPOSE: Calculate the total width required by fields with fixed length specified
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub CalculateFixedVarWidth(ByRef FixedWidth As Long, ByRef VarWidth As Long)

    Dim idx_column As Integer

    FixedWidth = 0
    VarWidth = 0
    For idx_column = 0 To m_settings.NumColumns() - 1
      If m_settings.Columns(idx_column).IsWidthFixed Then
        FixedWidth = FixedWidth + m_settings.Columns(idx_column).WidthSpecial
      Else
        VarWidth = VarWidth + m_settings.Columns(idx_column).WidthSpecial
      End If
    Next

  End Sub ' CalculateFixedVarWidth

  ' PURPOSE: Update the total width of the columns filled
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub UpdateTotalWidth()

    Dim idx_column As Integer

    For idx_column = 0 To m_settings.NumColumns() - 1
      If m_settings.Columns(idx_column).IsWidthFixed Then
        m_settings.TotalFixedWidth = m_settings.TotalFixedWidth + m_settings.Columns(idx_column).WidthSpecial
      Else
        m_settings.TotalVarWidth = m_settings.TotalVarWidth + m_settings.Columns(idx_column).WidthSpecial
      End If
    Next
  End Sub ' UpdateTotalWidth

  ' PURPOSE: Update the columns width according to the given size page proportion
  '
  ' PARAMS:
  '    - INPUT:
  '          - ProportionsCoeff (page size/total width of columns)
  '    - OUTPUT:
  '          - None
  '
  ' RETURNS:
  '    - none
  Private Sub UpdateColumnsWidth(ByVal ProportionsCoeff As Double, ByVal MaxWidth As Long)

    Dim idx_column As Integer

    For idx_column = 0 To m_settings.NumColumns - 1
      If Not m_settings.Columns(idx_column).IsWidthFixed Then

        m_settings.Columns(idx_column).Width = _
            (m_settings.Columns(idx_column).WidthSpecial) * ProportionsCoeff

      End If

    Next

    ' To stretch the last column until the end of the page
    If idx_column > 0 Then
      idx_column = idx_column - 1
      'm_settings.Columns(idx_column).Width = m_settings.Columns(idx_column).Width + (WIDTH_SPACE * 2)
      'If m_settings.Columns(idx_column).Width + m_settings.Columns(idx_column).Left > MaxWidth Then
      m_settings.Columns(idx_column).Width = MaxWidth - m_settings.Columns(idx_column).Left + WIDTH_SPACE
      'End If
    End If

  End Sub ' UpdateColumnsWidth

  ' PURPOSE: Update the top headers width according to the columns that have been updated
  '
  ' PARAMS:
  '    - INPUT:
  '          - None
  '    - OUTPUT:
  '          - None
  '
  ' RETURNS:
  '    - none
  Private Sub UpdateHeadersWidth()

    Dim idx_start_col As Integer
    Dim idx_end_col As Integer
    Dim idx_header As Integer
    Dim idx_col As Integer
    Dim width_header As Long

    For idx_header = 0 To MAX_HEADER_DATA - 1
      idx_start_col = m_settings.Headers(idx_header).StartCol
      idx_end_col = m_settings.Headers(idx_header).EndCol
      If idx_start_col > -1 Then
        m_settings.Headers(idx_header).Left = m_settings.Columns(idx_start_col).Left

        width_header = 0
        For idx_col = idx_start_col To idx_end_col
          width_header = width_header + m_settings.Columns(idx_col).WidthSpecial
        Next

        m_settings.Headers(idx_header).Width = width_header
      End If
    Next

  End Sub ' UpdateHeadersWidth

  ' PURPOSE: Calculate the amount to add to the left value of each filter/footer according
  '          to the modified width of the headers and values.
  '
  ' PARAMS:
  '    - INPUT:
  '          - Filter - if to calcualte the filters, otherwise claculate the footers
  '    - OUTPUT:
  '          - AddToHeaderLeft 
  '          - AddToValueLeft
  '
  ' RETURNS:
  '    - none

  Private Sub GetFilterFooterLeftValue(ByRef AddToHeaderLeft() As Long, _
                                       ByRef AddToValueLeft() As Long, _
                                       ByVal Filter As Boolean)
    Dim idx_block As Integer
    Dim idx_aux As Integer

    Const FILTER_HEADER_WIDTH As Long = 1200
    Const FILTER_VALUE_WIDTH As Long = 2450
    Const FOOTER_HEADER_WIDTH As Long = 1200
    Const FOOTER_VALUE_WIDTH As Long = 2450

    ' Filter header and value width
    If Filter Then
      For idx_block = 0 To MAX_FILTER_BLOCKS - 1
        If m_settings.FilterHeaderWidth(idx_block) > 0 Then
          For idx_aux = idx_block To MAX_FILTER_BLOCKS - 1
            AddToHeaderLeft(idx_aux + 1) = AddToHeaderLeft(idx_aux + 1) + _
                              (m_settings.FilterHeaderWidth(idx_block) - FILTER_HEADER_WIDTH)
            AddToValueLeft(idx_aux) = AddToValueLeft(idx_aux) + _
                              (m_settings.FilterHeaderWidth(idx_block) - FILTER_HEADER_WIDTH)

          Next
        End If
        If m_settings.FilterValueWidth(idx_block) > 0 Then
          For idx_aux = idx_block + 1 To MAX_FILTER_BLOCKS - 1
            AddToHeaderLeft(idx_aux) = AddToHeaderLeft(idx_aux) + _
                              (m_settings.FilterValueWidth(idx_block) - FILTER_VALUE_WIDTH)
            AddToValueLeft(idx_aux) = AddToValueLeft(idx_aux) + _
                              (m_settings.FilterValueWidth(idx_block) - FILTER_VALUE_WIDTH)
          Next
        End If
        AddToHeaderLeft(idx_block) = AddToHeaderLeft(idx_block)
        AddToValueLeft(idx_block) = AddToValueLeft(idx_block)
      Next

    Else
      ' Footer header and value width
      For idx_block = 0 To MAX_FOOTER_BLOCKS - 1
        If m_settings.FooterHeaderWidth(idx_block) > 0 Then
          For idx_aux = idx_block To MAX_FOOTER_BLOCKS - 1
            AddToHeaderLeft(idx_aux + 1) = AddToHeaderLeft(idx_aux + 1) + _
                              (m_settings.FooterHeaderWidth(idx_block) - FOOTER_HEADER_WIDTH)
            AddToValueLeft(idx_aux) = AddToValueLeft(idx_aux) + _
                              (m_settings.FooterHeaderWidth(idx_block) - FOOTER_HEADER_WIDTH)

          Next
        End If
        If m_settings.FooterValueWidth(idx_block) > 0 Then
          For idx_aux = idx_block + 1 To MAX_FOOTER_BLOCKS - 1
            AddToHeaderLeft(idx_aux) = AddToHeaderLeft(idx_aux) + _
                              (m_settings.FooterValueWidth(idx_block) - FOOTER_VALUE_WIDTH)
            AddToValueLeft(idx_aux) = AddToValueLeft(idx_aux) + _
                              (m_settings.FooterValueWidth(idx_block) - FOOTER_VALUE_WIDTH)
          Next
        End If

        AddToHeaderLeft(idx_block) = AddToHeaderLeft(idx_block)
        AddToValueLeft(idx_block) = AddToValueLeft(idx_block)
      Next
    End If

  End Sub ' GetFilterFooterLeftValue

#End Region

End Class
