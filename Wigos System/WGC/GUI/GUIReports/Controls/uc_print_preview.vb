'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:      uc_print_preview.vb  
'
' DESCRIPTION:      Visualizes excel document's pages before set custom print.
'
' AUTHOR:           Jes�s �ngel Blanco Blanco
'
' CREATION DATE:    08-AUG-2013
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ ----------------------------------------------
' 08-AUG-2013  JAB    Initial version.
'-------------------------------------------------------------------

Imports System.Drawing.Imaging
Imports System.Drawing.Printing
Imports System.ComponentModel
Imports System.Windows.Forms
Imports System.Drawing
Imports System.Drawing.Drawing2D

#Region " Enums "

Friend Enum ZoomMode
  ActualSize = 0
  Custom = 4
  FullPage = 1
  PageWidth = 2
  TwoPages = 3
End Enum ' ZoomMode

#End Region

Friend Class uc_print_preview
  Inherits UserControl


#Region " Members "

  Private m_first_time As Boolean = True
  Private m_content_size As Rectangle
  Private m_zoom_selected As String
  Private m_back_brush As Brush
  Private m_cancel As Boolean
  Private m_doc As PrintDocument
  Private m_himm2pix As PointF = New PointF(-1.0!, -1.0!)
  Private m_img_list As cls_page_image_list = New cls_page_image_list
  Private m_point_last As Point
  Private m_rendering As Boolean
  Private m_start_page As Integer
  Private m_zoom As Double
  Private m_zoom_mode As ZoomMode
  Private m_preview_print_controller As PreviewPrintController
  Private m_preview_print_controller_with_status_dialog As System.Windows.Forms.PrintControllerWithStatusDialog

#End Region

#Region " Constants "

  Public Const CANCEL_PREVIEW_AT_PAGE As Integer = 5

#End Region

#Region " Events "

  Public Event PageCountChanged As EventHandler
  Public Event StartPageChanged As EventHandler
  Public Event ZoomModeChanged As EventHandler
  Public Event EscKeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
  Public Event MouseWheelUpDown(ByVal ScrollUp As Boolean)

#End Region

#Region " Properties "

  <DefaultValue(GetType(Color), "AppWorkspace")> _
  Public Overrides Property BackColor() As Color
    Get
      Return MyBase.BackColor
    End Get
    Set(ByVal value As Color)
      MyBase.BackColor = value
      Me.m_back_brush = New SolidBrush(value)
    End Set
  End Property         ' BackColor
  Public Property Document() As SpreadsheetGear.Drawing.Printing.WorkbookPrintDocument
    Get
      Return Me.m_doc
    End Get
    Set(ByVal value As SpreadsheetGear.Drawing.Printing.WorkbookPrintDocument)
      If (Not value Is Me.m_doc) Then
        Me.m_doc = value
        If m_first_time Then
          ' Zoom a mano:  Me._doc.DefaultPageSettings.PaperSize = New PaperSize("Custom", 800, 600)
          ' Me.PreferredSize
          Me.m_doc.DefaultPageSettings.Landscape = True
          m_first_time = False
        End If
        Me.RefreshPreview()
      End If
    End Set
  End Property                    ' Document
  <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)> _
  Public ReadOnly Property IsRendering() As Boolean
    Get
      Return Me.m_rendering
    End Get
  End Property        ' IsRendering
  <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)> _
  Public ReadOnly Property PageCount() As Integer
    Get
      Return Me.m_img_list.Count
    End Get
  End Property          ' PageCount
  <Browsable(False)> _
  Public ReadOnly Property PageImages() As cls_page_image_list
    Get
      Return Me.m_img_list
    End Get
  End Property         ' PageImages
  <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)> _
  Public Property StartPage() As Integer
    Get
      Return Me.m_start_page
    End Get
    Set(ByVal value As Integer)
      If (value > (Me.PageCount - 1)) Then
        value = (Me.PageCount - 1)
      End If
      If (value < 0) Then
        value = 0
      End If
      If (value <> Me.m_start_page) Then
        Me.m_start_page = value
        Me.UpdateScrollBars()
        Me.OnStartPageChanged(EventArgs.Empty)
      End If
    End Set
  End Property                   ' StartPage
  <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)> _
  Public Property Zoom() As Double
    Get
      Return Me.m_zoom
    End Get
    Set(ByVal value As Double)
      If ((value <> Me.m_zoom) OrElse (Me.ZoomMode <> ZoomMode.Custom)) Then
        Me.ZoomMode = ZoomMode.Custom
        Me.m_zoom = value
        Me.UpdateScrollBars()
        Me.OnZoomModeChanged(EventArgs.Empty)
      End If
    End Set
  End Property                        ' Zoom
  <DefaultValue(1)> _
  Public Property ZoomMode() As ZoomMode
    Get
      Return Me.m_zoom_mode
    End Get
    Set(ByVal value As ZoomMode)
      If (value <> Me.m_zoom_mode) Then
        Me.m_zoom_mode = value
        Me.UpdateScrollBars()
        Me.OnZoomModeChanged(EventArgs.Empty)
      End If
    End Set
  End Property                    ' ZoomMode

#End Region

#Region " Public "

  Public Sub New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    Me.BackColor = SystemColors.AppWorkspace
    Me.ZoomMode = ZoomMode.FullPage
    Me.StartPage = 0
    MyBase.SetStyle(ControlStyles.OptimizedDoubleBuffer, True)

  End Sub                                ' New
  Public Sub Cancel()
    Me.m_cancel = True
  End Sub                             ' Cancel
  Public Function GetImageSizeInPixels(ByVal Img As Image) As Size
    Dim _size_f As SizeF = Img.PhysicalDimension

    If TypeOf Img Is Metafile Then
      If (Me.m_himm2pix.X < 0.0!) Then
        Using g As Graphics = MyBase.CreateGraphics
          Me.m_himm2pix.X = (g.DpiX / 2540.0!)
          Me.m_himm2pix.Y = (g.DpiY / 2540.0!)
        End Using
      End If
      _size_f.Width = (_size_f.Width * Me.m_himm2pix.X)
      _size_f.Height = (_size_f.Height * Me.m_himm2pix.Y)
    End If
    Return Size.Truncate(_size_f)
  End Function          ' GetImageRectangle
  Public Shared Function SetImageScale(ByVal Img As Image, ByVal ZoomPercentage As String) As Image
    Dim _new_image As Image
    Dim _size_f As SizeF = Img.PhysicalDimension
    Dim _new_width As Integer
    Dim _new_height As Integer

    _size_f.Width = _size_f.Width / 100 * CSng(ZoomPercentage)
    _size_f.Height = _size_f.Height / 100 * CSng(ZoomPercentage)

    _new_width = CInt(_size_f.Width)
    _new_height = CInt(_size_f.Height)

    _new_image = New Bitmap(_new_width, _new_height)

    Using graphicsHandle As Graphics = Graphics.FromImage(_new_image)
      graphicsHandle.InterpolationMode = InterpolationMode.HighQualityBicubic
      graphicsHandle.DrawImage(Img, 0, 0, _new_width, _new_height)
    End Using

    Return _new_image
  End Function          ' SetImageScale
  Public Sub Print()
    Dim _printer_settings As PrinterSettings = Me.m_doc.PrinterSettings
    Dim _first As Integer = (_printer_settings.MinimumPage - 1)
    Dim _last As Integer = (_printer_settings.MaximumPage - 1)
    Dim _document_printer As DocumentPrinter

    Select Case _printer_settings.PrintRange
      Case PrintRange.AllPages
        Me.Document.Print()
        Return

      Case PrintRange.Selection
        _first = Me.StartPage
        _last = Me.StartPage
        If (Me.ZoomMode = ZoomMode.TwoPages) Then
          _last = Math.Min(CInt((_first + 1)), CInt((Me.PageCount - 1)))
        End If
        Exit Select

      Case PrintRange.SomePages
        _first = (_printer_settings.FromPage - 1)
        _last = (_printer_settings.ToPage - 1)
        Exit Select

      Case PrintRange.CurrentPage
        _first = Me.StartPage
        _last = Me.StartPage
        Exit Select

    End Select

    _document_printer = New DocumentPrinter(Me, _first, _last)
    _document_printer.Print()
  End Sub                              ' Print
  Public Sub RefreshPreview()
    If (Not Me.m_doc Is Nothing) Then
      Dim _savePC As PrintController

      Windows.Forms.Cursor.Current = Cursors.WaitCursor

      Me.m_img_list.Clear()
      _savePC = Me.m_doc.PrintController

      Try
        Me.m_cancel = False
        Me.m_rendering = True
        Me.m_doc.PrintController = New PreviewPrintController
        AddHandler Me.m_doc.PrintPage, New PrintPageEventHandler(AddressOf Me.DocPrintPage)
        AddHandler Me.m_doc.EndPrint, New PrintEventHandler(AddressOf Me.DocEndPrint)
        Me.m_doc.Print()

      Finally
        Me.m_cancel = False
        Me.m_rendering = False
        RemoveHandler Me.m_doc.PrintPage, New PrintPageEventHandler(AddressOf Me.DocPrintPage)
        RemoveHandler Me.m_doc.EndPrint, New PrintEventHandler(AddressOf Me.DocEndPrint)
        Me.m_doc.PrintController = _savePC
      End Try
    End If

    Me.OnPageCountChanged(EventArgs.Empty)
    Me.UpdatePreview()
    Me.UpdateScrollBars()

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub                     ' RefreshPreview

#End Region

#Region " Private "

  Private Sub DocEndPrint(ByVal Sender As Object, ByVal E As PrintEventArgs)
    Me.SyncPageImages(True)
  End Sub                       ' DocEndPrint
  Private Sub DocPrintPage(ByVal Sender As Object, ByVal E As PrintPageEventArgs)
    Me.SyncPageImages(False)
    If Me.m_cancel Then
      E.Cancel = True
    End If
  End Sub                      ' DocPrintPage
  Private Sub SetImage(ByVal Page As Integer, ByVal Img As Image)
    If (Page > -1) AndAlso (Page < Me.PageCount) Then
      Me.m_img_list.Item(Page) = Img
    End If
  End Sub                          ' SetImage
  Private Sub RenderPage(ByVal Grap As Graphics, ByVal Img As Image, ByVal Rect As Rectangle)
    Rect.Offset(1, 1)
    Grap.DrawRectangle(Pens.Black, Rect)
    Rect.Offset(-1, -1)
    Grap.FillRectangle(Brushes.White, Rect)
    Grap.DrawImage(Img, Rect)
    Grap.DrawImage(Img, m_content_size)
    Grap.DrawRectangle(Pens.Black, Rect)
    Rect.Width += 1
    Rect.Height += 1
    Grap.ExcludeClip(Rect)
    Rect.Offset(1, 1)
    Grap.ExcludeClip(Rect)
  End Sub                        ' RenderPage
  Private Sub SyncPageImages(ByVal LastPageReady As Boolean)

    m_preview_print_controller = DirectCast(Me.m_doc.PrintController, PreviewPrintController)
    'm_preview_print_controller_with_status_dialog = DirectCast(Me.m_doc.PrintController, System.Windows.Forms.PrintControllerWithStatusDialog)

    If (Not m_preview_print_controller Is Nothing) Then
      Dim _page_info As PreviewPageInfo() = m_preview_print_controller.GetPreviewPageInfo
      Dim _count As Integer = IIf(LastPageReady, _page_info.Length, (_page_info.Length - 1))
      Dim _i As Integer
      Dim _img As Image

      For _i = Me.m_img_list.Count To _count - 1
        ' Cancel Print
        If m_img_list.Count = CANCEL_PREVIEW_AT_PAGE Then
          m_cancel = True
          Exit For
        End If

        _img = _page_info(_i).Image

        Me.m_img_list.Add(_img)

        Me.OnPageCountChanged(EventArgs.Empty)
        If (Me.StartPage < 0) Then
          Me.StartPage = 0
        End If
        If ((_i = Me.StartPage) OrElse (_i = (Me.StartPage + 1))) Then
          Me.Refresh()
        End If

        Application.DoEvents()
      Next _i

    End If
  End Sub                    ' SyncPageImages
  Private Sub UpdatePreview()
    If (Me.m_start_page < 0) Then
      Me.m_start_page = 0
    End If
    If (Me.m_start_page > (Me.PageCount - 1)) Then
      Me.m_start_page = (Me.PageCount - 1)
    End If
    MyBase.Invalidate()
  End Sub                     ' UpdatePreview
  Private Sub UpdateScrollBars()
    Dim _rectangle As Rectangle = Rectangle.Empty
    Dim _image As Image = Me.GetImage(Me.StartPage)
    Dim _scroll_size As Size

    If (Not _image Is Nothing) Then
      _rectangle = Me.GetImageRectangle(_image)
    End If

    _scroll_size = New Size(0, 0)

    Select Case Me.m_zoom_mode
      Case ZoomMode.ActualSize, ZoomMode.Custom
        _scroll_size = New Size((_rectangle.Width + 8), (_rectangle.Height + 8))

        Exit Select
      Case ZoomMode.PageWidth
        _scroll_size = New Size(0, (_rectangle.Height + 8))

        Exit Select
    End Select

    If (_scroll_size <> MyBase.AutoScrollMinSize) Then
      MyBase.AutoScrollMinSize = _scroll_size
    End If

    Me.UpdatePreview()
  End Sub                  ' UpdateScrollBars
  Private Sub UcPrintPreview_KeyPress(ByVal Sender As System.Object, ByVal E As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
    Select Case E.KeyChar
      Case Chr(13), Chr(27) ' enter
        RaiseEvent EscKeyPress(Sender, E)
    End Select
  End Sub           ' UcPrintPreview_KeyPress

  Private Function GetImage(ByVal Page As Integer) As Image
    If (Page > -1) AndAlso (Page < Me.PageCount) Then
      Return Me.m_img_list.Item(Page)
    Else
      Return Nothing
    End If
  End Function                     ' GetImage
  Private Function GetImageRectangle(ByVal Img As Image) As Rectangle
    Dim _size As Size = Me.GetImageSizeInPixels(Img)
    Dim _rectangle As New Rectangle(0, 0, _size.Width, _size.Height)
    Dim _rectangleCli As Rectangle = MyBase.ClientRectangle
    Dim _zoomX As Double
    Dim _zoomY As Double
    Dim _dx As Integer
    Dim _dy As Integer

    Select Case Me.m_zoom_mode
      Case ZoomMode.ActualSize
        Me.m_zoom = 1
        GoTo Label_00FF

      Case ZoomMode.FullPage
        Exit Select

      Case ZoomMode.PageWidth
        Me.m_zoom = IIf((_rectangle.Width > 0), (CDbl(_rectangleCli.Width) / CDbl(_rectangle.Width)), 0)
        GoTo Label_00FF

      Case ZoomMode.TwoPages
        _rectangle.Width = (_rectangle.Width * 2)
        Exit Select

      Case Else
        GoTo Label_00FF

    End Select

    _zoomX = IIf((_rectangle.Width > 0), (CDbl(_rectangleCli.Width) / CDbl(_rectangle.Width)), 0)
    _zoomY = IIf((_rectangle.Height > 0), (CDbl(_rectangleCli.Height) / CDbl(_rectangle.Height)), 0)

    Me.m_zoom = Math.Min(_zoomX, _zoomY)

Label_00FF:
    _rectangle.Width = CInt((_rectangle.Width * Me.m_zoom))
    _rectangle.Height = CInt((_rectangle.Height * Me.m_zoom))

    _dx = ((_rectangleCli.Width - _rectangle.Width) / 2)
    If (_dx > 0) Then
      _rectangle.X = (_rectangle.X + _dx)
    End If

    _dy = ((_rectangleCli.Height - _rectangle.Height) / 2)
    If (_dy > 0) Then
      _rectangle.Y = (_rectangle.Y + _dy)
    End If
    _rectangle.Inflate(-4, -4)
    If (Me.m_zoom_mode = ZoomMode.TwoPages) Then
      _rectangle.Inflate(-2, 0)
    End If

    Return _rectangle
  End Function            ' GetImageRectangle

#End Region
  
#Region " Protected "

  Protected Sub OnPageCountChanged(ByVal E As EventArgs)
    RaiseEvent PageCountChanged(Me, E)
  End Sub              ' OnPageCountChanged
  Protected Sub OnStartPageChanged(ByVal E As EventArgs)
    RaiseEvent StartPageChanged(Me, E)
  End Sub              ' OnStartPageChanged
  Protected Sub OnZoomModeChanged(ByVal E As EventArgs)
    RaiseEvent ZoomModeChanged(Me, E)
  End Sub               ' OnZoomModeChanged
  Protected Overrides Sub OnKeyDown(ByVal e As KeyEventArgs)
    Dim _point As Point

    MyBase.OnKeyDown(e)
    If e.Handled Then Return

    Select Case e.KeyCode
      ' arrow keys scroll or browse, depending on ZoomMode
      Case Keys.Left, Keys.Up, Keys.Right, Keys.Down

        ' browse
        If ZoomMode = ZoomMode.FullPage OrElse ZoomMode = ZoomMode.TwoPages Then
          Select Case e.KeyCode
            Case Keys.Left, Keys.Up
              StartPage -= 1

            Case Keys.Right, Keys.Down
              StartPage += 1
          End Select
        End If

        ' scroll
        _point = AutoScrollPosition
        Select Case e.KeyCode
          Case Keys.Left
            _point.X += 20
          Case Keys.Right
            _point.X -= 20
          Case Keys.Up
            _point.Y += 20
          Case Keys.Down
            _point.Y -= 20
        End Select
        AutoScrollPosition = New Point(-_point.X, -_point.Y)

        ' page up/down browse pages
      Case Keys.PageUp
        StartPage -= 1
      Case Keys.PageDown
        StartPage += 1

        ' home/end 
      Case Keys.Home
        AutoScrollPosition = Point.Empty
        StartPage = 0
      Case Keys.End
        AutoScrollPosition = Point.Empty
        StartPage = PageCount - 1

      Case Else

        Return

    End Select

    ' if we got here, the event was handled
    e.Handled = True
  End Sub             ' OnKeyDown
  Protected Overrides Sub OnMouseDown(ByVal E As MouseEventArgs)
    MyBase.OnMouseDown(E)
    If ((E.Button = MouseButtons.Left) AndAlso (MyBase.AutoScrollMinSize <> Size.Empty)) Then
      Me.Cursor = Cursors.NoMove2D
      Me.m_point_last = New Point(E.X, E.Y)
    End If
  End Sub           ' OnMouseDown
  Protected Overrides Sub OnMouseMove(ByVal E As MouseEventArgs)
    Dim _dx As Integer
    Dim _dy As Integer
    Dim _point As Point

    MyBase.OnMouseMove(E)
    If (Me.Cursor Is Cursors.NoMove2D) Then

      _dx = (E.X - Me.m_point_last.X)
      _dy = (E.Y - Me.m_point_last.Y)
      If ((_dx <> 0) OrElse (_dy <> 0)) Then
        _point = MyBase.AutoScrollPosition
        MyBase.AutoScrollPosition = New Point(-(_point.X + _dx), -(_point.Y + _dy))
        Me.m_point_last = New Point(E.X, E.Y)
      End If
    End If
  End Sub           ' OnMouseMove
  Protected Overrides Sub OnMouseUp(ByVal E As MouseEventArgs)
    MyBase.OnMouseUp(E)
    If ((E.Button = MouseButtons.Left) AndAlso (Me.Cursor Is Cursors.NoMove2D)) Then
      Me.Cursor = Cursors.Default
    End If
  End Sub             ' OnMouseUp
  Protected Overrides Sub OnPaint(ByVal E As PaintEventArgs)
    Dim _image As Image
    Dim _rectangle As Rectangle

    _image = Me.GetImage(Me.StartPage)

    If Not _image Is Nothing Then
      _rectangle = Me.GetImageRectangle(_image)

      If ((_rectangle.Width > 2) AndAlso (_rectangle.Height > 2)) Then
        _rectangle.Offset(MyBase.AutoScrollPosition)

        If (Me.m_zoom_mode <> ZoomMode.TwoPages) Then
          Me.RenderPage(E.Graphics, _image, _rectangle)
        Else
          _rectangle.Width = ((_rectangle.Width - 4) / 2)
          Me.RenderPage(E.Graphics, _image, _rectangle)

          _image = Me.GetImage((Me.StartPage + 1))

          If Not _image Is Nothing And Not _image Is Nothing Then
            _rectangle = Me.GetImageRectangle(_image)
            _rectangle.Width = ((_rectangle.Width - 4) / 2)
            _rectangle.Offset((_rectangle.Width + 4), 0)

            Me.RenderPage(E.Graphics, _image, _rectangle)
          End If
        End If
      End If
    End If
    E.Graphics.FillRectangle(Me.m_back_brush, MyBase.ClientRectangle)
  End Sub               ' OnPaint
  Protected Overrides Sub OnPaintBackground(ByVal E As PaintEventArgs)
  End Sub     ' OnPaintBackground
  Protected Overrides Sub OnSizeChanged(ByVal E As EventArgs)
    Me.UpdateScrollBars()
    MyBase.OnSizeChanged(E)
  End Sub         ' OnSizeChanged
  Protected Overrides Function IsInputKey(ByVal KeyData As Keys) As Boolean

    Select Case KeyData
      Case Keys.Prior, Keys.Next, Keys.End, Keys.Home, Keys.Left, Keys.Up, Keys.Right, Keys.Down
        Return True
    End Select

    Return MyBase.IsInputKey(KeyData)
  End Function       ' IsInputKey

#End Region

  Friend Class DocumentPrinter
    Inherits PrintDocument

#Region " Members "
    Private m_first As Integer
    Private m_imgList As cls_page_image_list
    Private m_index As Integer
    Private m_last As Integer
#End Region

#Region " Public "

    Public Sub New(ByVal Preview As uc_print_preview, ByVal First As Integer, ByVal Last As Integer)
      Me.m_first = First
      Me.m_last = Last
      Me.m_imgList = Preview.PageImages
      MyBase.DefaultPageSettings = Preview.Document.DefaultPageSettings
      MyBase.PrinterSettings = Preview.Document.PrinterSettings
    End Sub                        ' New

#End Region

#Region " Overtides "

    Protected Overrides Sub OnBeginPrint(ByVal e As PrintEventArgs)
      Me.m_index = Me.m_first
    End Sub  ' OnBeginPrint

    Protected Overrides Sub OnPrintPage(ByVal E As PrintPageEventArgs)
      E.Graphics.PageUnit = GraphicsUnit.Display
      E.Graphics.DrawImage(Me.m_imgList.Item(Me.m_index), E.PageBounds)
      Me.m_index = Me.m_index + 1
      E.HasMorePages = (Me.m_index <= Me.m_last)
    End Sub   ' OnPrintPage

#End Region

  End Class

  Private Sub uc_print_preview_MouseWheel(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles MyBase.MouseWheel
    If e.Delta > 0 Then
      ' Scrolled up!
      RaiseEvent MouseWheelUpDown(True)
    Else
      ' Scrolled down!
      RaiseEvent MouseWheelUpDown(False)
    End If
  End Sub
End Class
