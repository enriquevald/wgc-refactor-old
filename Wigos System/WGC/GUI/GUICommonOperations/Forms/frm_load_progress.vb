'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_load_progress.vb  
'
' DESCRIPTION:   
'
' AUTHOR:        Alberto Cuesta
'
' CREATION DATE: 03-JUN-2002
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 03-JUN-2002  AC    Initial version
'--------------------------------------------------------------------
Imports GUI_CommonMisc

Public Class frm_load_progress

  Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents lbl_copyright As System.Windows.Forms.Label
  Friend WithEvents lbl_application_name As System.Windows.Forms.Label
  Friend WithEvents lbl_product_name As System.Windows.Forms.Label
  Friend WithEvents pnl_load_progress As System.Windows.Forms.Panel
  Friend WithEvents lbl_version As System.Windows.Forms.Label
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.pnl_load_progress = New System.Windows.Forms.Panel
    Me.lbl_version = New System.Windows.Forms.Label
    Me.lbl_copyright = New System.Windows.Forms.Label
    Me.lbl_application_name = New System.Windows.Forms.Label
    Me.lbl_product_name = New System.Windows.Forms.Label
    Me.pnl_load_progress.SuspendLayout()
    Me.SuspendLayout()
    '
    'pnl_load_progress
    '
    Me.pnl_load_progress.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
    Me.pnl_load_progress.Controls.Add(Me.lbl_version)
    Me.pnl_load_progress.Controls.Add(Me.lbl_copyright)
    Me.pnl_load_progress.Controls.Add(Me.lbl_application_name)
    Me.pnl_load_progress.Controls.Add(Me.lbl_product_name)
    Me.pnl_load_progress.Location = New System.Drawing.Point(3, 3)
    Me.pnl_load_progress.Name = "pnl_load_progress"
    Me.pnl_load_progress.Size = New System.Drawing.Size(472, 165)
    Me.pnl_load_progress.TabIndex = 5
    '
    'lbl_version
    '
    Me.lbl_version.Font = New System.Drawing.Font("Verdana", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_version.Location = New System.Drawing.Point(16, 63)
    Me.lbl_version.Name = "lbl_version"
    Me.lbl_version.Size = New System.Drawing.Size(440, 24)
    Me.lbl_version.TabIndex = 12
    Me.lbl_version.Text = "(01.001)"
    Me.lbl_version.TextAlign = System.Drawing.ContentAlignment.TopCenter
    '
    'lbl_copyright
    '
    Me.lbl_copyright.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_copyright.Location = New System.Drawing.Point(20, 133)
    Me.lbl_copyright.Name = "lbl_copyright"
    Me.lbl_copyright.Size = New System.Drawing.Size(435, 24)
    Me.lbl_copyright.TabIndex = 11
    Me.lbl_copyright.Text = "Copyright � 2008-2009, WSI"
    Me.lbl_copyright.TextAlign = System.Drawing.ContentAlignment.TopCenter
    '
    'lbl_application_name
    '
    Me.lbl_application_name.Font = New System.Drawing.Font("Verdana", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_application_name.Location = New System.Drawing.Point(15, 27)
    Me.lbl_application_name.Name = "lbl_application_name"
    Me.lbl_application_name.Size = New System.Drawing.Size(440, 24)
    Me.lbl_application_name.TabIndex = 10
    Me.lbl_application_name.Text = "WigosGUI"
    Me.lbl_application_name.TextAlign = System.Drawing.ContentAlignment.TopCenter
    '
    'lbl_product_name
    '
    Me.lbl_product_name.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_product_name.Location = New System.Drawing.Point(51, 105)
    Me.lbl_product_name.Name = "lbl_product_name"
    Me.lbl_product_name.Size = New System.Drawing.Size(368, 24)
    Me.lbl_product_name.TabIndex = 8
    Me.lbl_product_name.Text = "Wigos System"
    Me.lbl_product_name.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'frm_load_progress
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.BackColor = System.Drawing.Color.Black
    Me.ClientSize = New System.Drawing.Size(478, 171)
    Me.ControlBox = False
    Me.Controls.Add(Me.pnl_load_progress)
    Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
    Me.Location = New System.Drawing.Point(273, 105)
    Me.MaximizeBox = False
    Me.MinimizeBox = False
    Me.Name = "frm_load_progress"
    Me.ShowInTaskbar = False
    Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
    Me.Text = "Wigos System"
    Me.TopMost = True
    Me.pnl_load_progress.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Public Methods "

  Public Sub FormInit(ByRef VersionComponent As TYPE_VERSION_COMPONENT)

    'lbl_application_name.Text = GLB_NLS.GetString(7) & " " & GLB_ApplicationName
    lbl_application_name.Text = GLB_ApplicationName
    lbl_version.Text = VersionComponent.version_found
    lbl_product_name.Text = GLB_NLS_GUI_COMMONMISC.GetString(8)
    lbl_copyright.Text = VersionComponent.copyright & ", " & VersionComponent.company_name

    ' Reset the height and width sizes to the correct ones (which are automatically changed in run time)
    Me.Height = 171
    Me.Width = 478

    ' TJG 04-APR-2005
    ' Since Login dialog is screen centered, Banner window can not be positioned in the 
    ' same way (System.Windows.Forms.FormStartPosition.CenterScreen) to avoid an overlap. So 
    ' a manual start position is set vertically centered and horizontally shifted up
    Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
    Me.SetBounds((System.Windows.Forms.Screen.GetBounds(Me).Width - Me.Width) / 2, _
                 (System.Windows.Forms.Screen.GetBounds(Me).Height - Me.Height) / 2 - Me.Height * 1.31, _
                 Me.Width, _
                 Me.Height, _
                 System.Windows.Forms.BoundsSpecified.Location)

    Me.Show()
    Me.Refresh()

  End Sub

  Public Sub FormEnd()
    Me.Hide()
  End Sub

#End Region


End Class
