﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("GUICommonOperations")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("WSI Gaming S.L.")> 
<Assembly: AssemblyProduct("GUICommonOperations")> 
<Assembly: AssemblyCopyright("Copyright © WSI Gaming S.L. 2007")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("1d198fce-8226-4bef-b17f-4b1362ee3863")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("0.1.0.0")> 
<Assembly: AssemblyFileVersion("0.1.0.0")> 
