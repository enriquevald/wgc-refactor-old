'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   mdl_validate_prize_plan.vb
' DESCRIPTION:   
' AUTHOR:        Ignasi Ripoll
' CREATION DATE: 28-03-2003
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------  ------ -----------------------------------------------
' 28-03-2003  IRP    Initial version
'-------------------------------------------------------------------
Option Strict Off
Option Explicit On 

Imports GUI_CommonMisc



Public Module mdl_validate_prize_plan

#Region " NLS "

  'Public GLB_NLS_CONFIG As CLASS_MODULE_NLS = New CLASS_MODULE_NLS(ENUM_NLS_MODULES.COMMON_MODULE_GUI_CONF)

#End Region


#Region " Public "

  ' PURPOSE: Checks if will be possible to generate a serie
  '
  '  PARAMS:
  '     - INPUT: 
  '           - PLTicketsPerBook: Price Level Tickets per Book
  '           - PLBooksPerSeries: Price Level Books per Series
  '           - PLDescription:    Price Level Description
  '           - PrizePlan:        Prize Plan
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - Boolean
  'Public Function AreValidToSeriesGeneration(ByVal PLTicketsPerBook As Integer, _
  '                                           ByVal PLBooksPerSeries As Integer, _
  '                                           ByVal PLDescription As String, _
  '                                           ByVal PrizePlanId As Integer, _
  '                                           ByVal GameId As Integer, _
  '                                           ByVal GameName As String, _
  '                                           ByVal PrizePlanName As String, _
  '                                           Optional ByVal ShowMsgOk As Boolean = True) As Boolean


  '  Dim key As New CLASS_PRIZE_PLAN_KEY()
  '  Dim prize_plan As New CLASS_PRIZE_PLAN()
  '  Dim rc As Integer
  '  Dim nls_param As String
  '  Dim ctx As Integer

  '  key.PrizePlanId = PrizePlanId
  '  key.GameId = GameId

  '  Try
  '    GUI_SqlCtxAlloc(ctx)
  '    rc = prize_plan.DB_Read(key, ctx)
  '    If rc <> CLASS_BASE.ENUM_STATUS.STATUS_OK Then
  '      Call NLS_MsgBox(GLB_NLS_CONFIG.Id(169), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, PrizePlanName)

  '      Return False
  '    End If

  '  Finally
  '    GUI_SqlCtxRelease(ctx)
  '  End Try

  '  Return AreValidToSeriesGeneration(PLTicketsPerBook, PLBooksPerSeries, PLDescription, GameName, prize_plan, ShowMsgOk)

  'End Function

  ' PURPOSE: Checks if will be possible to generate a serie
  '
  '  PARAMS:
  '     - INPUT: 
  '           - PLTicketsPerBook: Price Level Tickets per Book
  '           - PLBooksPerSeries: Price Level Books per Series
  '           - PLDescription:    Price Level Description
  '           - PrizePlan:        Prize Plan
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - Boolean
  'Public Function AreValidToSeriesGeneration(ByVal PLTicketsPerBook As Integer, _
  '                                           ByVal PLBooksPerSeries As Integer, _
  '                                           ByVal PLDescription As String, _
  '                                           ByVal GameName As String, _
  '                                           ByVal PrizePlan As CLASS_PRIZE_PLAN, _
  '                                           Optional ByVal ShowMsgOk As Boolean = True) As Boolean

  '  Dim idx_category As Integer
  '  Dim category_item As New CLASS_PRIZE_PLAN.TYPE_PP_CATEGORY()
  '  Dim category_valid As Boolean
  '  Dim price_level_valid As Boolean

  '  price_level_valid = True

  '  If PrizePlan.Categories.Count = 0 Then
  '    price_level_valid = False
  '  End If

  '  If price_level_valid = True Then

  '    For idx_category = 0 To PrizePlan.Categories.Count - 1

  '      Call PrizePlan.Categories.GetItem(idx_category, category_item)

  '      category_valid = IsCategoryValid(PLTicketsPerBook, _
  '                                       PLBooksPerSeries, _
  '                                       category_item.pct_deviation_cat, _
  '                                       category_item.amt_win_cat, _
  '                                       category_item.pct_win_cat, _
  '                                       category_item.pct_tax_win_cat)

  '      If category_valid = False Then
  '        price_level_valid = False
  '        Exit For
  '      End If

  '    Next

  '  End If

  '  'TODO: 28-FEB-2003 Change Strings from NLS_GuiConfiguration to NLS_GuiCommonMisc
  '  If price_level_valid = False Then

  '    Call NLS_MsgBox(GLB_NLS_CONFIG.Id(162), _
  '                    ENUM_MB_TYPE.MB_TYPE_WARNING, _
  '                    ENUM_MB_BTN.MB_BTN_OK, _
  '                    ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
  '                    PrizePlan.Name, _
  '                    PLDescription, _
  '                    GameName)


  '  Else
  '    If ShowMsgOk = True Then

  '      Call NLS_MsgBox(GLB_NLS_CONFIG.Id(163), _
  '                      ENUM_MB_TYPE.MB_TYPE_INFO, _
  '                      ENUM_MB_BTN.MB_BTN_OK, _
  '                      ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
  '                      PrizePlan.Name, _
  '                      PLDescription, _
  '                      GameName)
  '    End If
  '  End If

  '  Return price_level_valid

  'End Function 'CheckParamsToSeriesGeneration

#End Region

#Region " Private Functions "



  ' PURPOSE: Checks if it's possible to generate a serie with a Category of Prize Plan
  '
  '  PARAMS:
  '     - INPUT: 
  '           - TicketsPerBook: Game
  '           - BooksPerSeries: Game
  '           - Desviation:     Prize Plans
  '           - NetFactor:      Category of Prize Plans
  '           - SalesPct:       Category of Prize Plans
  '           - TaxesPct:       Category of Prize Plans
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - Boolean string
  Public Function IsCategoryValid(ByVal TicketsPerBook As Integer _
                                , ByVal BooksPerSeries As Integer _
                                , ByVal DesviationPct As Double _
                                , ByVal NetFactor As Double _
                                , ByVal SalesPct As Double _
                                , ByVal TaxesPct As Double) As Boolean

    Dim prizes_per_series As Double
    Dim prizes_per_book As Double
    Dim lower_bound As Double
    Dim upper_bound As Double
    Dim max_prizes_per_book As Integer
    Dim min_prizes_per_book As Integer


    If NetFactor = 0 Or BooksPerSeries = 0 Then
      Return False
    End If

    prizes_per_series = ((BooksPerSeries * TicketsPerBook * (SalesPct / 100)) / NetFactor) * (1 - TaxesPct / 100)

    If prizes_per_series < 1 Then
      Return False
    End If

    prizes_per_book = prizes_per_series / BooksPerSeries


    min_prizes_per_book = Math.Floor(prizes_per_book)
    max_prizes_per_book = Math.Ceiling(prizes_per_book)

    ' AJQ, 14-JAN-2003, Should be possible to generate a series with prizes_per_book = 0!!!
    ' This happens when the number of prizes per series is very small
    '
    'If (prizes_per_book = 0) Then
    '  lower_bound = 0.0
    '  upper_bound = 1.0
    'Else
    lower_bound = prizes_per_book * (1.0 - DesviationPct / 100)
    upper_bound = prizes_per_book * (1.0 + DesviationPct / 100)
    'End If

    If (lower_bound <= min_prizes_per_book And max_prizes_per_book <= upper_bound) Then
      Return True
    Else
      Return False
    End If

  End Function 'IsCategoryValid

#End Region

End Module
