'-------------------------------------------------------------------
' Copyright © 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   mdl_registry.vb
' DESCRIPTION:   Registry module for Mannage the Registry definitions from the Common Data
' AUTHOR:        Luis Rodríguez
' CREATION DATE: 17-FEB-2004
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 26-FEB-2003  LRS    Initial version.
' 12-FEB-2015  FJC    New GUI Menu
'
'--------------------------------------------------------------------
Option Explicit On 

Imports GUI_CommonMisc
Imports System.Runtime.InteropServices

Public Module mdl_registry_def

#Region "Constants"
  Public Const MAX_REGISTRY_KEY_NAME = 80
  Public Const MAX_REGISTRY_VALUE_NAME = 80
  Public Const MAX_REGISTRY_VALUE_DATA = 80
  Public Const MAX_REGISTRY_VALUES = 1000

#Region "  KeyNames and KeyValueNames"
  Private Const REG_KEY_TYPE_MACHINE = 0
  Private Const REG_KEY_TYPE_USER = 1

  Public Const REG_KEY_NAME_GUI_INIT = "GUI\Common"
  Public Const REG_KEY_NAME_GUI_INIT_TYPE = REG_KEY_TYPE_USER
  Public Const REG_VALUE_NAME_LAST_USER = "LastUserName"
  Public Const REG_VALUE_NAME_LAST_HSEL = "LastSelectedHierarchy"
  Public Const REG_VALUE_NAME_LAST_HSEL_RESET = "AutomaticallySetLastSelectedHierarchy"

  Public Const REG_KEY_NAME_GUI_LAUNCHBAR = "GUI\LaunchBar"
  Public Const REG_KEY_NAME_GUI_LAUNCHBAR_TYPE = REG_KEY_TYPE_USER
  Public Const REG_VALUE_NAME_AUTOHIDE = "AutoHide"
  Public Const REG_VALUE_NAME_POSITION = "Position"

  'New Menu
  Public Const REG_KEY_NAME_GUI_MENU_STRIP = "GUI\Menu"
  Public Const REG_VALUE_NAME_GUI_MENU_STRIP_SHOW_ICONS = "ShowIcons"
  Public Const REG_VALUE_NAME_GUI_MENU_STRIP_POSITION = "Position"

#End Region

#End Region

  Public Class CLASS_REGISTRY_DEFINITION
    Inherits CLASS_BASE

#Region " Structures "

    <StructLayout(LayoutKind.Sequential)> _
    Public Class TYPE_REGISTRY_ITEM
      Private key_type As Integer
      <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_REGISTRY_KEY_NAME)> _
      Private key_name As String
      <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_REGISTRY_KEY_NAME)> _
      Private value_name As String
      Private value_type As Integer
      <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_REGISTRY_KEY_NAME)> _
      Private value_data As String
      Private auto_create As Boolean

#Region " Properties "

      Public Property KeyType() As Integer
        Get
          Return key_type
        End Get
        Set(ByVal Value As Integer)
          key_type = Value
        End Set
      End Property

      Public Property KeyName() As String
        Get
          Return key_name
        End Get
        Set(ByVal Value As String)
          key_name = Value
        End Set
      End Property

      Public Property ValueType() As Integer
        Get
          Return value_type
        End Get
        Set(ByVal Value As Integer)
          value_type = Value
        End Set
      End Property

      Public Property ValueName() As String
        Get
          Return value_name
        End Get
        Set(ByVal Value As String)
          value_name = Value
        End Set
      End Property

      Public Property ValueData() As String
        Get
          Return value_data
        End Get
        Set(ByVal Value As String)
          value_data = Value
        End Set
      End Property

      Public Property AutoCreate() As Boolean
        Get
          Return auto_create
        End Get
        Set(ByVal Value As Boolean)
          auto_create = Value
        End Set
      End Property

#End Region

      Public Sub New()

      End Sub

      Public Sub New(ByVal myKeyType As Integer, ByVal myKeyName As String, ByVal myValueName As String, ByVal myValueData As String, ByVal myValueType As Integer, ByVal myAutoCreate As Boolean)
        KeyType = myKeyType
        KeyName = myKeyName
        ValueName = myValueName
        ValueData = myValueData
        ValueType = myValueType
        AutoCreate = myAutoCreate
      End Sub

    End Class

#End Region

#Region " Prototypes "

    Public Declare Function Common_RegistryDefsGet Lib "CommonData" (ByVal RegType As Integer, _
                                                                   <[In](), [Out]()> _
                                                                   ByVal RegistryList As CLASS_VECTOR) As Boolean
#End Region

#Region " Members "

    Protected m_registry_def_list As CLASS_VECTOR

#End Region

#Region "Overrides"
    Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    End Function

    Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    End Function

    Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    End Function

    Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    End Function

    Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE
      Return Nothing
    End Function

    Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA
      Return Nothing
    End Function
#End Region

    Public Sub New()
      MyBase.New()
      m_registry_def_list = New CLASS_VECTOR(GetType(TYPE_REGISTRY_ITEM), MAX_REGISTRY_VALUES)
    End Sub

    Public Property RegistryDefsList() As CLASS_VECTOR
      Get
        Return m_registry_def_list
      End Get
      Set(ByVal Value As CLASS_VECTOR)
        m_registry_def_list = Value
      End Set
    End Property

  End Class


End Module
