'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   mdl_mainr.vb
' DESCRIPTION:   Initialization module fot Gui applicatons
' AUTHOR:        Carlos A. Costa
' CREATION DATE: 25-03-2002
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------  ------ -----------------------------------------------
' 25-03-2002  CAC    Initial version
'-------------------------------------------------------------------

Option Strict Off
Option Explicit On 

Imports GUI_CommonMisc
Imports System.Data.OleDb

Public Module mdl_main

#Region " Global variables "

  Private GLB_SqlCtx As Integer = 0
  Private GLB_SqlCtxMutex As New System.Threading.Mutex(False)
  Private GLB_SqlCtxJobId As Integer = 0
  Private GLB_ClientId As Integer

#End Region

#Region " NLS "

  Public GLB_NLS_GUI_COMMONMISC As CLASS_MODULE_NLS = New CLASS_MODULE_NLS(ENUM_NLS_MODULES.COMMON_MODULE_GUI_COMMONMISC)
  Public GLB_NLS_GUI_CONTROLS As CLASS_MODULE_NLS = New CLASS_MODULE_NLS(ENUM_NLS_MODULES.COMMON_MODULE_GUI_CONTROLS)

#End Region

#Region " Constants "
  Private Const MODULE_NAME = "GUI_COMMONOPERATIONS"
  Private Const COMMON_MISC_DLL = "CommonMisc.dll"
#End Region

  ' TODO: move this structures to version module class
#Region " VERSION MODULE Structures "
  ' TODO: move this structures to version module class

  Public Const VERSION_LENGTH_MODULE_NAME As Integer = 40
  Public Const VERSION_LENGTH_VERSION As Integer = 6
  Public Const VERSION_LENGTH_COPYRIGHT As Integer = 128
  Public Const VERSION_LENGTH_COMPANY_NAME As Integer = 128

  Public Structure TYPE_VERSION_COMPONENT
    Public name As String
    Public client_found As Integer
    Public build_found As Integer
    Public version_found As String
    Public client_expected As Integer
    Public build_expected As Integer
    Public version_expected As String
    Public copyright As String
    Public company_name As String
  End Structure

  Public Structure TYPE_VERSION_MODULE
    Public control_block As Integer
    Public number_of_components As Integer
    Public component() As TYPE_VERSION_COMPONENT
  End Structure

#End Region

#Region " Declarations "
  Public Declare Function LoadLibrary Lib "kernel32" Alias "LoadLibraryA" (ByVal lpLibFileName As String) As Integer

#End Region

  ' PURPOSE: Initialize version control structure.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - True

  Function GUI_Versions(ByRef VersionModule As TYPE_VERSION_MODULE) As Boolean
    'XVV Variable not use Const function_name As String = "GUI_ModuleVersion"

    VersionModule.number_of_components = 1
    ReDim VersionModule.component(1)
    With VersionModule.component(0)
      .name = FixedString(VERSION_LENGTH_MODULE_NAME + 1)
      .version_found = FixedString(VERSION_LENGTH_VERSION + 1)
      .version_expected = FixedString(VERSION_LENGTH_VERSION + 1)
      .copyright = FixedString(VERSION_LENGTH_COPYRIGHT + 1)
      .company_name = FixedString(VERSION_LENGTH_COMPANY_NAME + 1)
      .copyright = "Copyright � 2002-2008"
      .company_name = "Win Systems Intl."
      .version_found = "01.001"
    End With

    Return True

  End Function ' GUI_Versions

  Function GUI_GetFileConfiguration() As CLASS_CENTER_CONFIGURATION
    Const function_name As String = "GUI_FileConfiguration"

    Dim config As CLASS_CENTER_CONFIGURATION
    Dim status_config As Integer

    ' Read from the configuration file
    config = New CLASS_CENTER_CONFIGURATION
    status_config = config.DB_BeforeRead(Nothing)
    If status_config <> CLASS_BASE.ENUM_STATUS.STATUS_OK Then
      Common_LoggerMsg(ENUM_LOG_MSG.LOG_GENERIC_ERROR, _
                       MODULE_NAME, _
                       function_name, _
                       status_config, _
                       "config.DB_BeforeRead")

      config = Nothing
    End If

    Return config
  End Function ' GUI_GetFileConfiguration


  Function GUI_GetDBConfiguration(ByRef Config As CLASS_CENTER_CONFIGURATION)
    Const function_name As String = "GUI_Configuration"

    Dim status_config As Integer

    If IsNothing(Config) Then
      Return Config

    End If

    ' Read the configuration from DB
    status_config = Config.DB_Read(Config)
    If status_config <> CLASS_BASE.ENUM_STATUS.STATUS_OK Then
      Common_LoggerMsg(ENUM_LOG_MSG.LOG_GENERIC_ERROR, _
                       MODULE_NAME, _
                       function_name, _
                       status_config, _
                       "config.DB_Read")

      Config = Nothing
    End If

    Return Config
  End Function


  Private GLB_Banner As frm_load_progress

  Public Sub ShowBanner(ByVal VersionComponent As TYPE_VERSION_COMPONENT)

    GLB_Banner = New frm_load_progress
    GLB_Banner.FormInit(VersionComponent)

  End Sub

  Public Sub HideBanner()
    If IsNothing(GLB_Banner) Then
      Exit Sub
    End If

    Call GLB_Banner.FormEnd()
    Call GLB_Banner.Dispose()
    GLB_Banner = Nothing

  End Sub

  '------------------------------------------------------------------------
  'XVV 17/04/2007
  'Comment this functions, because are obsolete (Oracle version)
  '------------------------------------------------------------------------
  'Public Function GUI_SqlCtxAlloc(ByRef SqlCtx As Integer)
  '  GLB_SqlCtxMutex.WaitOne()
  '  GLB_SqlCtx = 0
  '  SqlCtx = GLB_SqlCtx
  'End Function

  'Public Function GUI_SqlCtxRelease(ByRef SqlCtx As Integer)
  '  If SqlCtx = GLB_SqlCtx Then
  '    GLB_SqlCtx = 0
  '  End If
  '  GLB_SqlCtxMutex.ReleaseMutex()
  '  SqlCtx = -1
  'End Function
  '------------------------------------------------------------------------

  Public Function GUI_ClientId() As Integer
    Return GLB_ClientId
  End Function

  Public Sub GUI_SetClientId(ByVal ClientId As Integer)
    GLB_ClientId = ClientId
  End Sub


End Module
