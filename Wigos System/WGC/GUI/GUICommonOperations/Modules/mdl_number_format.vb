'-------------------------------------------------------------------
' Copyright � 2003 Win Systems Ltd.
'-------------------------------------------------------------------
'
'   MODULE NAME: mdl_number_format.vb
'
'   DESCRIPTION: Functions and procedures to format and parse numbers
'
'        AUTHOR: Andreu Juli�
'
' CREATION DATE: 26-MAY-2003
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------- ------ -----------------------------------------------
' 26-MAY-2003 AJQ    Initial version
' 06-FEB-2003 OC     Sperated all currency and number related functions 
' 09-APR-2013 ICS    Fixed Bug #684: Edici�n Usuario GUI: validaci�n incorrecta de campos tipo money.
' 14-JUN-2013 DLL    Fixed Bug #859: Incorrect validation of currency TextBox
' 10-DEC-2014 DRV    Decimal currency formatting: DUT 201411 - Tito Chile - Currency formatting without decimals
' 11-FEB-2015 DRV    Fixed Bug WIG-2034: Some entry fields must allow decimals despite currency doesn't allow it
' 20-MAR-2015 RCI    Fixed Bug WIG-2168: Currency decimal digits can't be 2000.
' 23-MAR-2015 MPO    WIG-2173: Wrong decimal rounding for negative currency.
' 05-JAN-2016 DDS    PBI 7885. Floor Dual Currency: A�adir combo divisa en control de proveedores
' 10-NOV-2017 LTC    WIGOS-6496: warning in log file for MultisiteGUI - Invalid object name 'CURRENCY_EXCHANGE'
'-------------------------------------------------------------------
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Globalization
Imports System.Math
Imports GUI_CommonMisc

Public Module mdl_number_format

  'Public Declare Function Common_DBNumberToLocalNumber Lib "CommonBase" (ByVal pDBNumber As String, _
  '                                                                      <[In](), [Out]()> ByVal pLocaleNumber As String) As Integer

  'Public Declare Function Common_LocalNumberToDBNumber Lib "CommonBase" (ByVal pLocaleNumber As String, _
  '                                                                        <[In](), [Out]()> ByVal pDBNumber As String) As Integer



  Public Const DEFAULT_DECIMAL_DIGITS_NUMBER As Integer = -1
  Public Const DEFAULT_DECIMAL_DIGITS_CURRENCY As Integer = -2000
  ' LTC 10-NOV-2017
  Public Const DECIMAL_DIGITS_NUMBER As Integer = 2
  Public Const DECIMAL_DIGITS_CURRENCY As Integer = 2
  Private Const NATIONAL_CURRENCY As String = "NATIONAL"

  Private BUFFER_SIZE As Integer = 256

  Private m_culture_dictionary As Dictionary(Of String, CultureInfo) = New Dictionary(Of String, CultureInfo)

  Public Enum ENUM_GROUP_DIGITS
    GROUP_DIGITS_FALSE = 0
    GROUP_DIGITS_TRUE = 1
    GROUP_DIGITS_DEFAULT = 2
  End Enum

  Public Function GUI_FormatNumber(ByVal Number As Object, _
                                   Optional ByVal DecimalDigits As Integer = DEFAULT_DECIMAL_DIGITS_NUMBER, _
                                   Optional ByVal GroupDigits As ENUM_GROUP_DIGITS = ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, _
                                   Optional ByVal NumDigits As Integer = 0) As String
    Dim group_digits As TriState
    'XVV Variable not use Dim format_string As String
    'XVV Variable not use Dim len_format As Integer

    If DecimalDigits = DEFAULT_DECIMAL_DIGITS_NUMBER Then
      DecimalDigits = DECIMAL_DIGITS_NUMBER
    End If

    Select Case GroupDigits
      Case ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE
        group_digits = TriState.True

      Case ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE
        group_digits = TriState.False

      Case Else ' GROUP_DIGITS_DEFAULT
        group_digits = TriState.True

    End Select

    Return FormatNumber(Number, _
                        DecimalDigits, _
                        TriState.UseDefault, _
                        TriState.UseDefault, _
                        group_digits)
    'End If

  End Function


  Public Function GUI_FormatCurrency(ByVal Value As Double, _
                                     Optional ByVal DecimalDigits As Integer = DEFAULT_DECIMAL_DIGITS_CURRENCY, _
                                     Optional ByVal GroupDigits As ENUM_GROUP_DIGITS = ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, _
                                     Optional ByVal CurrencySymbol As Boolean = True, _
                                     Optional ByVal CurrencyIsoCode As String = "", _
                                     Optional ByVal IgnoreCurrencyFormat As Boolean = False) As String

    Dim _ci As System.Globalization.CultureInfo
    Dim _key_str As String
    Dim _dec_value As Decimal
    Dim _found As Boolean
    Dim _floor_dual_currency As Boolean


    _key_str = String.Empty

    ' AJQ Make a Writable Culture
    If (System.Threading.Thread.CurrentThread.CurrentCulture.IsReadOnly) Then
      System.Threading.Thread.CurrentThread.CurrentCulture = System.Threading.Thread.CurrentThread.CurrentCulture.Clone()
      System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencySymbol = WSI.Common.GeneralParam.GetString("WigosGUI", "CurrencySymbol")
      System.Threading.Thread.CurrentThread.CurrentUICulture = System.Threading.Thread.CurrentThread.CurrentUICulture.Clone()
      System.Threading.Thread.CurrentThread.CurrentUICulture.NumberFormat.CurrencySymbol = WSI.Common.GeneralParam.GetString("WigosGUI", "CurrencySymbol")
    End If

    If CurrencyIsoCode = WSI.Common.Cage.CHIPS_ISO_CODE Then
      CurrencyIsoCode = ""
    End If

    If DecimalDigits = DEFAULT_DECIMAL_DIGITS_CURRENCY OrElse _
      (DecimalDigits <> 0 AndAlso Not IgnoreCurrencyFormat) Then
      ' LTC 10-NOV-2017
      If (WSI.Common.Misc.IsMultisiteCenter()) Then
        ' In order to avoid currency_exchange validation - No currency exchange in Multisite
        DecimalDigits = GUI_CommonOperations.mdl_number_format.DECIMAL_DIGITS_CURRENCY
      Else
        DecimalDigits = WSI.Common.Format.GetFormattedDecimals(CurrencyIsoCode)
      End If
    End If

    If DecimalDigits < 0 Then
      _found = False
      DecimalDigits = Math.Abs(DecimalDigits)
      _dec_value = Math.Round(CDec(Value), DecimalDigits)

      While Not _found And DecimalDigits >= 0
        _found = (_dec_value * CDec(10 ^ DecimalDigits)) Mod 10 <> 0
        DecimalDigits -= 1
      End While

      DecimalDigits += 1
    End If

    _key_str &= Math.Abs(DecimalDigits).ToString()

    Select Case GroupDigits
      Case ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE
        _key_str &= "0"

      Case ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT
        _key_str &= "1"

    End Select

    'DDS 05-01-2016 If _floor_dual_currency is enabled, the currency symbol must not be shown in any grid
    _floor_dual_currency = WSI.Common.Misc.IsFloorDualCurrencyEnabled()
    If CurrencySymbol And Not _floor_dual_currency Then
      _key_str &= "1"
    Else
      _key_str &= "0"
    End If

    ' Add the culture ID to key
    _key_str &= System.Threading.Thread.CurrentThread.CurrentCulture.LCID.ToString()

    ' Check if this key exist into the dictionary
    If Not m_culture_dictionary.ContainsKey(_key_str) Then
      _ci = System.Threading.Thread.CurrentThread.CurrentCulture.Clone()
      _ci.NumberFormat.CurrencyDecimalDigits = Math.Abs(DecimalDigits)

      'DDS 05-01-2016 If _floor_dual_currency is enabled, the currency symbol must not be shown in any grid
      If Not CurrencySymbol Or _floor_dual_currency Then
        _ci.NumberFormat.CurrencySymbol = String.Empty
        ' No spaces between value and currency symbol
        _ci.NumberFormat.CurrencyPositivePattern = 0
      End If

      If GroupDigits = ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE Then
        _ci.NumberFormat.CurrencyGroupSeparator = String.Empty
      End If

      ' Add the new culture information to the dictionary
      m_culture_dictionary.Add(_key_str, _ci)

    Else
      _ci = m_culture_dictionary(_key_str)
    End If


    Return String.Format(_ci, "{0:C}", Value)

  End Function


  Public Function GUI_ParseNumber(ByVal StringValue As String) As Double
    Dim is_number As Boolean
    Dim result As Double

    'OC-changed to parse only numbers
    is_number = Double.TryParse(StringValue, NumberStyles.Number, Nothing, result)

    If Not is_number Then
      result = 0
    End If

    Return result

  End Function

  Public Function GUI_ParseNumberLong(ByVal StringValue As String) As Long
    Dim is_number As Boolean
    Dim result As Long

    'OC-changed to parse only numbers
    is_number = Long.TryParse(StringValue, NumberStyles.Number, Nothing, result)

    If Not is_number Then
      result = 0
    End If

    Return result

  End Function

  Public Function GUI_ParseNumberDecimal(ByVal StringValue As String) As Decimal
    Dim _result As Decimal

    If Not Decimal.TryParse(StringValue, NumberStyles.AllowDecimalPoint, Nothing, _result) Then
      _result = 0
    End If

    Return _result

  End Function

  Public Function GUI_ParseNumberDecimal(ByVal StringValue As String, ByVal DecimalValue As Integer) As Decimal
    Dim _result As Decimal

    If Not Decimal.TryParse(StringValue, NumberStyles.AllowDecimalPoint, Nothing, _result) Then
      _result = 0
    End If

    Return GUI_FormatNumber(_result, DecimalValue)

  End Function


  Public Function GUI_ParseCurrency(ByVal StringValue As String) As Double

    Dim is_number As Boolean
    Dim result As Double

    ' AJQ Make a Writable Culture
    If (System.Threading.Thread.CurrentThread.CurrentCulture.IsReadOnly) Then
      System.Threading.Thread.CurrentThread.CurrentCulture = System.Threading.Thread.CurrentThread.CurrentCulture.Clone()
      System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencySymbol = WSI.Common.GeneralParam.GetString("WigosGUI", "CurrencySymbol")
      System.Threading.Thread.CurrentThread.CurrentUICulture = System.Threading.Thread.CurrentThread.CurrentUICulture.Clone()
      System.Threading.Thread.CurrentThread.CurrentUICulture.NumberFormat.CurrencySymbol = WSI.Common.GeneralParam.GetString("WigosGUI", "CurrencySymbol")
    End If

    'OC-changed to parse only currency
    is_number = Double.TryParse(StringValue, NumberStyles.Currency, Nothing, result)

    If Not is_number Then
      result = 0
    End If

    Return result

  End Function


  ' PURPOSE: Check if decimal of value is greater than MaxDecimalLen of a number
  '
  '  PARAMS:
  '     - INPUT:
  '           - value: String field that represent a double
  '           - MaxDecimalLen: Maxim Len accepted to the value
  '
  ' RETURNS:
  '     - True if precission is correct
  Private Function GUI_PrecisionNumberIsOk(ByVal value As String, _
                                           ByVal MaxDecimalLen As Integer) As Boolean
    Dim index As Integer
    Dim dec_len As Integer

    index = value.LastIndexOf(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator.ToString())
    If (index = -1) Then
      ' Decimal digits not Found

      Return True
    End If

    dec_len = value.Length - (index + 1)

    If (dec_len <= MaxDecimalLen) Then
      Return True
    Else
      Return False
    End If

  End Function



  ' PURPOSE: Check if decimal of value is greater than MaxDecimalLen of currency
  '
  '  PARAMS:
  '     - INPUT:
  '           - value: String field that represent a double
  '           - MaxDecimalLen: Maxim Len accepted to the value
  '
  ' RETURNS:
  '     - True if precission is correct
  Private Function GUI_PrecisionCurrencyIsOk(ByVal value As String, _
                                             ByVal MaxDecimalLen As Integer) As Boolean
    Dim idx_currency_dec_separator As Integer
    Dim idx_currency_symbol As Integer
    Dim _decimals As String
    'XVV Variable not use Dim _length As Integer
    Dim currency_symbol As String

    With System.Globalization.NumberFormatInfo.CurrentInfo

      currency_symbol = .CurrencySymbol.ToString()
      idx_currency_symbol = value.LastIndexOf(currency_symbol)

      If (idx_currency_symbol <> -1) Then
        value = Trim(value.Remove(idx_currency_symbol, currency_symbol.Length))
      End If

      idx_currency_dec_separator = value.LastIndexOf(.CurrencyDecimalSeparator.ToString())

    End With


    ' Check if Decimal Digits exist

    If (idx_currency_dec_separator = -1) Then
      ' Decimal digits not Found

      Return True
    End If

    _decimals = value.Substring(idx_currency_dec_separator + 1)

    If (_decimals.Length <= MaxDecimalLen) Then
      Return True
    Else
      Return False
    End If

  End Function



  ' PURPOSE: Check maxValue and MaxDecimalLen and show MsgBox with warning for currency
  '
  '  PARAMS:
  '     - INPUT:
  '           - value: String field that represent a double
  '           - MaxValue: Maxim Value of double value
  '           - FieldName: Name of field for show in String for MsgBox
  '           - MaxDecimalLen: 
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - True if format is correct
  Public Function GUI_CheckCurrency(ByVal value As String, _
                                     ByVal MaxValue As Double, _
                                     ByVal FieldName As String, _
                                     ByVal MaxDecimalLen As Integer) As Boolean

    Dim nls As New CLASS_MODULE_NLS(ENUM_NLS_MODULES.COMMON_MODULE_GUI_CONF)
    'XVV Variable not use Dim max_value As Double
    'XVV Variable not use Dim format_value As Double
    'XVV Variable not use Dim parser_number As Double

    'Check value Max
    If GUI_ParseCurrency(value) > MaxValue Then
      Call NLS_MsgBox(nls.Id(113), _
                      mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, _
                      , _
                      , _
                      GUI_FormatNumber(MaxValue), _
                      FieldName)
      Return False
    End If

    'check Num Decimals
    If Not GUI_PrecisionCurrencyIsOk(value, MaxDecimalLen) Then
      'Falta String - El camp no pot tenir + de 3 decimals
      Call NLS_MsgBox(nls.Id(140), _
                      mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, _
                      , _
                      , _
                      FieldName, _
                      MaxDecimalLen)
      Return False
    End If

    Return True

  End Function

  ' PURPOSE: Check maxValue and MaxDecimalLen and show MsgBox with warning for number
  '
  '  PARAMS:
  '     - INPUT:
  '           - value: String field that represent a double
  '           - MaxValue: Maxim Value of double value
  '           - FieldName: Name of field for show in String for MsgBox
  '           - MaxDecimalLen: 
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - True if format is correct
  Public Function GUI_CheckNumber(ByVal value As String, _
                                     ByVal MaxValue As Double, _
                                     ByVal FieldName As String, _
                                     ByVal MaxDecimalLen As Integer) As Boolean

    Dim nls As New CLASS_MODULE_NLS(ENUM_NLS_MODULES.COMMON_MODULE_GUI_CONF)
    'XVV Variable not use Dim max_value As Double
    'XVV Variable not use Dim format_value As Double
    'XVV Variable not use Dim parser_number As Double

    'Check value Max
    If GUI_ParseNumber(value) > MaxValue Then
      Call NLS_MsgBox(nls.Id(113), _
                      mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, _
                      , _
                      , _
                      GUI_FormatNumber(MaxValue), _
                      FieldName)
      Return False
    End If

    'check Num Decimals
    If Not GUI_PrecisionNumberIsOk(value, MaxDecimalLen) Then
      'Falta String - El camp no pot tenir + de 3 decimals
      Call NLS_MsgBox(nls.Id(140), _
                      mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, _
                      , _
                      , _
                      FieldName, _
                      MaxDecimalLen)
      Return False
    End If

    Return True

  End Function


  Public Function GUI_DBNumberToLocalNumber(ByVal CommonNumber As String)

    Return WSI.Common.Format.DBNumberToLocalNumber(CommonNumber)

  End Function

  Public Function GUI_LocalNumberToDBNumber(ByVal LocalNumber As String)

    Return WSI.Common.Format.LocalNumberToDBNumber(LocalNumber)

  End Function


  ' PURPOSE: Format value with currency format 
  '
  '  PARAMS:
  '     - INPUT:
  '           - Value
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - CurrencyFormat
  Public Function GUI_MultiCurrencyValue_Format(ByVal Value As String, Optional ByVal ShowTwoDecimals As Boolean = True) As String

    Dim _format_currency_decimals As Integer
    Dim _format_currency_group_digits As GUI_CommonOperations.ENUM_GROUP_DIGITS
    Dim _format_currency_symbol As Boolean
    Dim _format_currency_iso_code As String
    Dim _format_currency_ignore As Boolean

    If ShowTwoDecimals Then
      _format_currency_decimals = 2
    Else
      _format_currency_decimals = WSI.Common.GeneralParam.GetInt32("MultiSite", "ExchangeAverageDecimals", 4)
    End If

    _format_currency_group_digits = GUI_CommonOperations.ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT
    _format_currency_symbol = True
    _format_currency_iso_code = String.Empty
    _format_currency_ignore = True

    Return GUI_FormatCurrency(Value, _format_currency_decimals, _format_currency_group_digits, _format_currency_symbol, _format_currency_iso_code, _format_currency_ignore) 'GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_AMOUNT), 2)

  End Function

  ''' <summary>
  ''' Gte currency value formatted
  ''' </summary>
  ''' <param name="Value"></param>
  ''' <param name="GroupDigits"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function GUI_GetCurrencyValue_Format(ByVal Value As String, ByVal GroupDigits As ENUM_GROUP_DIGITS) As String
    'Multisite has problems with GUI_FormatCurrency because it not exists table CURRENCY_EXCHANGE
    If WSI.Common.Misc.IsCenter Then
      Return GUI_MultiCurrencyValue_Format(Value, True)
    Else
      Return GUI_FormatCurrency(Value, GroupDigits)
    End If
  End Function

End Module
