'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
'   MODULE NAME : mdl_Misc.vb
'
'   DESCRIPTION : 
'
'        AUTHOR :
'
' CREATION DATE :
'
' REVISION HISTORY :
'
' Date        Author Description
' ----------- ------ -----------------------------------------------
' xx-xxx-xxxx xxx    Initial version
' 05-SEP-2002 JSV    Add rutine to check forms into DB after login (in GUI_Init).
' 04-APR-2012 MPO    Method to do query with SqlCommand --> GUI_GetTableUsingCommand
'-------------------------------------------------------------------

Imports GUI_CommonOperations
Imports GUI_CommonMisc

Imports System.Runtime.InteropServices
Imports System.Threading.Mutex
Imports System.Data.SqlClient

Public Module mdl_sql_database

  Private GLB_SQLMutex As New System.Threading.Mutex(False)

#Region " SQL Connection functions"

  ' PURPOSE: Create new sql connection to database
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - A SQL connection
  '
  Public Function NewSQLConnection() As SqlConnection

    Dim sql_conn As SqlConnection

    '-------------------------------------------------------------------------------------------------
    'XVV 25/04/2007 
    'Add new SQL connection method, Kernel DLL returns SQL Connection are inicilized in Frm_Main.
    '-------------------------------------------------------------------------------------------------
    'Clear SQL Connection
    sql_conn = Nothing

    'Recover SQL Connection
    sql_conn = WSI.Common.WGDB.Connection()

    'If sql_conn, has been inicialize, then return SQL Connection
    If Not (sql_conn Is Nothing) Then
      Return sql_conn
    Else
      Return Nothing
    End If
    '-----------------------------------------------------------------------------------------------

  End Function

  ' PURPOSE: Store the connections
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - 
  '
  Public Function ReleaseSQLConnection(ByVal SqlConn As SqlConnection, ByVal IsConnected As Boolean) As Boolean

    Try

      SqlConn.Close()
      SqlConn.Dispose()

    Catch ex As SqlException

      Debug.Print(ex.Message)

    End Try

    Return True
  End Function

#End Region

#Region " SQL Query/Reader Database Functions"

  ' PURPOSE: Get data from database. Before create connection 
  '
  '  PARAMS:
  '     - INPUT:
  '           - Sql sentence
  '           - Max rows to select
  '           - Table name
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - A datatable with db data
  '
  Public Function GUI_GetTableUsingSQL(ByVal SqlSelect As String, _
                                       ByVal MaxRows As Integer, _
                                       Optional ByVal TableName As String = "Table", _
                                       Optional ByVal SqlTrans As SqlTransaction = Nothing, _
                                       Optional ByVal Timeout As Integer = 0) As DataTable

    'XVV 13/04/2007 
    'Add Nothing value to variable declaration 
    Dim sql_conn As SqlConnection = Nothing
    Dim sql_adap As SqlDataAdapter = Nothing
    Dim table As DataTable = Nothing
    Dim daset As DataSet = Nothing
    Dim connected As Boolean
    Dim idx_try As Integer
    Dim data_retrieved As Boolean
    'XVV Variable not use Dim job_id As Integer
    Dim sql_cmd As SqlCommand
    ''Dim last_error_string As String = ""
    ''Dim param_string(5) As String
    Dim ex_type As Integer = 0

    data_retrieved = False

    For idx_try = 1 To 3

      Try
        connected = False
        sql_adap = Nothing
        table = Nothing

        If Not IsNothing(SqlTrans) Then

          sql_cmd = New SqlCommand(SqlSelect)
          sql_cmd.Connection = SqlTrans.Connection
          sql_cmd.Transaction = SqlTrans
          sql_adap = New SqlDataAdapter(SqlSelect, sql_cmd.Connection)
          sql_adap.SelectCommand = sql_cmd

        Else

          sql_conn = Nothing
          sql_conn = NewSQLConnection()

          If Not (sql_conn Is Nothing) Then
            ' Connected
            If sql_conn.State <> ConnectionState.Open Then
              ' Connecting ..
              sql_conn.Open()
            End If

            If Not sql_conn.State = ConnectionState.Open Then
              Exit Try
            End If
          End If

          sql_adap = New SqlDataAdapter(SqlSelect, sql_conn)

        End If

        If Not (sql_adap Is Nothing) Then
          ' Table
          daset = New DataSet
          table = New DataTable(TableName)
          daset.Tables.Add(table)
          ' Retrieving data ...
          table.BeginLoadData()
          If Timeout <> 0 Then
            sql_adap.SelectCommand.CommandTimeout = Timeout
          End If
          sql_adap.Fill(daset, 0, MaxRows, table.TableName)
          connected = True
          table.EndLoadData()
          daset.Tables.Remove(table)
          data_retrieved = True

          Return table
        End If

        ' SLE: 26-abr-2010
      Catch exsql As SqlException
        Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, GLB_ApplicationName, "GUI_GetTableUsingSQL", _
                              "", "", "", exsql.Message)
        ex_type = exsql.Number
        If (ex_type = -2) Then
          Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(144), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
          Exit For
        End If

      Catch ex As Exception
        ' Do nothing
        Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, GLB_ApplicationName, "GUI_GetTableUsingSQL", _
                              "", "", "", ex.Message)
        Debug.WriteLine(ex.Message)
        ''last_error_string = ex.Message

      Finally

        If Not data_retrieved Then
          If Not (table Is Nothing) Then
            table.Dispose()
          End If
          table = Nothing
        End If

        If Not (daset Is Nothing) Then
          daset.Dispose()
          daset = Nothing
        End If

        If Not (sql_adap Is Nothing) Then
          sql_adap.Dispose()
          sql_adap = Nothing
        End If

        If Not (sql_conn Is Nothing) Then
          ReleaseSQLConnection(sql_conn, connected)
          sql_conn = Nothing
        End If

      End Try

    Next

    ''If last_error_string <> "" Then
    ''  Call NLS_SplitString(last_error_string, param_string(0), param_string(1), param_string(2), param_string(3), param_string(4))
    ''  Call NLS_MsgBox(GLB_NLS.Id(6), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , param_string(0), param_string(1), param_string(2), param_string(3), param_string(4))
    ''End If

    ' SLE: 26-abr-2010
    If ex_type <> -2 Then
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(142), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
    End If

    Return Nothing

  End Function

  Public Function GUI_GetTableUsingCommand(ByVal SqlCmd As SqlCommand, _
                                           ByVal MaxRows As Integer, _
                                           Optional ByVal TableName As String = "Table", _
                                           Optional ByVal Timeout As Integer = 0) As DataTable

    Dim _dataset As DataSet
    Dim _ex_type As Integer = 0
    Dim _datatable As DataTable

    _datatable = Nothing
    _dataset = New DataSet()

    Try
      Using _db_trx As New WSI.Common.DB_TRX()
        SqlCmd.Transaction = _db_trx.SqlTransaction
        SqlCmd.Connection = _db_trx.SqlTransaction.Connection
        If Timeout <> 0 Then
          SqlCmd.CommandTimeout = Timeout
        End If
        Using _sql_da As New SqlClient.SqlDataAdapter(SqlCmd)
          _sql_da.Fill(_dataset, 0, MaxRows, "Table")
        End Using
      End Using

      If _dataset.Tables.Count > 0 Then
        _datatable = _dataset.Tables(0)
      End If

      Return _datatable

    Catch exsql As SqlException
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, GLB_ApplicationName, "GUI_GetTableUsingCommand", _
                            "", "", "", exsql.Message)
      _ex_type = exsql.Number
      If _ex_type = -2 Then
        Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(144), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      End If

    Catch ex As Exception
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, GLB_ApplicationName, "GUI_GetTableUsingCommand", _
                            "", "", "", ex.Message)
      Debug.WriteLine(ex.Message)
    End Try

    If _ex_type <> -2 Then
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(142), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
    End If

    Return Nothing

  End Function

#End Region

#Region " SQL Transactions Database Functions"

  ' PURPOSE: Begin database Transaction. 
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - True / False
  '
  Public Function GUI_BeginSQLTransaction(ByRef SqlTrans As SqlTransaction) As Boolean
    Dim sql_conn As SqlConnection
    Dim sql_trans As SqlTransaction
    Dim idx_try As Integer

    Try
      For idx_try = 1 To 3

        sql_conn = Nothing

        sql_conn = NewSQLConnection()

        If Not (sql_conn Is Nothing) Then
          ' Connected
          If sql_conn.State <> ConnectionState.Open Then
            ' Connecting ..
            sql_conn.Open()
          End If

          If sql_conn.State = ConnectionState.Open Then
            sql_trans = sql_conn.BeginTransaction()
            SqlTrans = sql_trans

            Return True
          End If
        End If
      Next

    Catch ex As Exception
      ' Do nothing
      Debug.WriteLine(ex.Message)

    Finally
      ' Nothing
    End Try

    Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(142), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

    Return False

  End Function

  ' PURPOSE: End database Transaction. 
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - True / False
  '
  Public Function GUI_EndSQLTransaction(ByVal SqlTrans As SqlTransaction, _
                                        ByVal Commit As Boolean) As Boolean

    If Not (SqlTrans Is Nothing) Then
      If Commit Then
        SqlTrans.Commit()
      Else
        SqlTrans.Rollback()
      End If
      SqlTrans.Dispose()
    End If

    SqlTrans = Nothing

    Return True
  End Function

#End Region

#Region " SQL NonQuery Database Functions"

  ' PURPOSE: Execute NonQuery commands (UPDATE, INSERT, DELETE, ...)
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - True / False
  '
  Public Function GUI_SQLExecuteNonQuery(ByVal SqlString As String, _
                                         Optional ByVal SqlTrans As SqlTransaction = Nothing, _
                                         Optional ByRef NumRowsUpdated As Integer = 0) As Boolean

    Dim sql_conn As SqlConnection
    Dim sql_cmd As SqlCommand
    Dim idx_try As Integer

    Try
      For idx_try = 1 To 3

        sql_cmd = New SqlCommand(SqlString)
        sql_cmd.Connection = Nothing

        If Not (SqlTrans Is Nothing) Then

          sql_cmd.Connection = SqlTrans.Connection
          sql_cmd.Transaction = SqlTrans

        Else

          sql_conn = NewSQLConnection()
          If Not (sql_conn Is Nothing) Then
            ' Connected
            If sql_conn.State <> ConnectionState.Open Then
              ' Connecting ..
              sql_conn.Open()
            End If

            If sql_conn.State = ConnectionState.Open Then
              sql_cmd.Connection = sql_conn
            End If
          End If

        End If

        If Not (sql_cmd.Connection Is Nothing) Then
          If sql_cmd.Connection.State = ConnectionState.Open Then
            NumRowsUpdated = sql_cmd.ExecuteNonQuery()

            Return True
          End If
        End If

      Next

    Catch ex As Exception
      ' Do nothing
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, GLB_ApplicationName, "GUI_SQLExecuteNonQuery", _
                            "", "", "", ex.Message)
      Debug.WriteLine(ex.Message)

    Finally
      ' Nothing
    End Try

    Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(142), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

    Return False
  End Function

#End Region

End Module
