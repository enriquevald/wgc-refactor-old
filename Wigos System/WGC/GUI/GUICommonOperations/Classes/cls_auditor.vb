'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_auditor.vb
' DESCRIPTION:   Definition of the auditor class. All the classes that
'                are used by the edition forms MUST be able to return 
'                this object.
' AUTHOR:        Andreu Juli�
' CREATION DATE: 25-03-2002
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------  ------ -----------------------------------------------
' 25-03-2002  AJQ    Initial version.
' 20-09-2012  JAB    Changed the function "Auditor_Flush" (made by MPO) to use sql parameters.
' 17-04-2013  RBG    #725 Computer name inserted incorrectly  
' 25-09-2014  JMM    Added array support to GENEREIC OPERATIONS auditor
' 20-01-2015  AMF    Fixed bug WIG-1948: Check if related columns exists
' 19-07-2016  LTC    Fixed bug 13758 Unhandled exception
' 22-07-2016  ETP    RollBack Fixed bug 13758 Unhandled exception
'-------------------------------------------------------------------

Imports GUI_CommonMisc
Imports System.Runtime.InteropServices

Public Class CLASS_COLLECTION_ID
  Private m_table As DataTable

  Public Sub New(ByVal CollectionName As String)
    m_table = CreateTable(CollectionName)
  End Sub

  Public Function Count() As Integer
    Return m_table.Rows.Count
  End Function

  Public Function Id(ByVal Index As Integer) As Integer
    Return CInt(m_table.Rows.Item(Index).Item(0))
  End Function

  Public Function Name(ByVal Index As Integer) As String
    If Index >= 0 And Index < Me.Count Then
      Return CStr(m_table.Rows.Item(Index).Item(1))
    Else
      Return vbNullString
    End If
  End Function

  Private Function Index(ByVal Id As Integer) As Integer
    Dim idx As Integer
    For idx = 0 To Me.Count - 1
      If Me.Id(idx) = Id Then
        Return idx
      End If
    Next
    Return -1
  End Function

  Public Function NameFromId(ByVal Id As Integer) As String
    Return Me.Name(Me.Index(Id))
  End Function

  Public Function Table() As DataTable
    Return m_table
  End Function

  Public Sub Add(ByVal NewId As Integer, ByVal NewNlsId As Integer)
    Dim row As DataRow

    row = m_table.NewRow()
    row.Item(0) = NewId
    row.Item(1) = NLS_GetString(NewNlsId)
    row.Item(2) = NewNlsId
    m_table.Rows.Add(row)
  End Sub

  Private Function CreateTable(ByVal TableName As String) As DataTable
    Dim table As DataTable
    Dim col As DataColumn

    table = New DataTable(TableName)

    col = table.Columns.Add()
    col.DataType = System.Type.GetType("System.Decimal")
    col.AllowDBNull = False
    col.Caption = "Id"
    col.ColumnName = "Id"
    col.Unique = True
    col.ReadOnly = True

    col = table.Columns.Add()
    col.DataType = System.Type.GetType("System.String")
    col.AllowDBNull = False
    col.Caption = "Name"
    col.ColumnName = "Name"
    col.Unique = True
    col.ReadOnly = True

    col = table.Columns.Add()
    col.DataType = System.Type.GetType("System.Decimal")
    col.AllowDBNull = False
    col.Caption = "NlsId"
    col.ColumnName = "NlsId"
    col.Unique = True
    col.ReadOnly = True
    Return table
  End Function

End Class

Public Class CLASS_AUDITOR_DATA

#Region " API Auditor "

  Private Const MODULE_NAME = "GUI_COMMONOPERATIONS"

  Private Const MAX_AUDITING_LIST As Integer = 1000
  Private Const AUDIT_NONE_STRING As String = "---"
  Private Const AUDIT_IDENT_LIST_STRING As String = "    "
  Private Const AUDIT_ITEM_LENGTH As Integer = 50
  Private Const MAX_AUDIT_ITEM_LENGTH As Integer = 150



  Private Enum ENUM_AUDITOR_RC
    AUDITOR_OK = 0
    AUDITOR_ERROR = 1
    AUDITOR_ERROR_CONTROL_BLOCK = 2
    AUDITOR_ERROR_DB = 3
    AUDITOR_ERROR_SAFEARRAY_ACCESS = 4
    AUDITOR_LIST_OVERFLOW = 5
    ' add to handle over flow in the number of audited items - OC 20-APR-04   
  End Enum

#Region " Structures"
  Public Structure TYPE_AUDITING_ITEM
    Dim control_block As Integer
    Dim audit_code As Integer
    Dim audit_level As Integer
    Dim item_order As Integer
    Dim nls_code As Integer
    Dim nls_param_1 As String
    Dim nls_param_2 As String
    Dim nls_param_3 As String
    Dim nls_param_4 As String
    Dim nls_param_5 As String
    Dim related_type As ENUM_AUDITOR_RELATED
    Dim related_id As Int64
  End Structure

#End Region

#Region " CommonData.dll "

#Region " Structures "

  <StructLayout(LayoutKind.Sequential)> _
  Protected Class TYPE_GUI_NAME
    Public control_clock As Integer
    Public gui_id As Integer
    Public nls_id As Integer

    Public Sub New()
      control_clock = Marshal.SizeOf(GetType(TYPE_GUI_NAME))
    End Sub
  End Class

  <StructLayout(LayoutKind.Sequential)> _
  Protected Class TYPE_AUDIT_NAME
    Public control_clock As Integer
    Public audit_code As Integer
    Public nls_id As Integer
    Public Sub New()
      control_clock = Marshal.SizeOf(GetType(TYPE_AUDIT_NAME))
    End Sub
  End Class

#End Region

#Region " Prototypes "

  Declare Function Common_GetComputerName Lib "CommonBase" (ByVal pComputerName As String, ByVal ComputerNameLen As Integer) As Integer

  Protected Declare Function Common_AuditNameGetNode Lib "CommonData" (ByVal NodeIndex As Integer, _
                                                                       <[In](), [Out]()> _
                                                                       ByVal AuditNode As TYPE_AUDIT_NAME) As Boolean

  Protected Declare Function Common_AuditNameNumNodes Lib "CommonData" () As Integer
  Protected Declare Function Common_GUINameNumNodes Lib "CommonData" () As Integer

  Protected Declare Function Common_GUINameGetNode Lib "CommonData" (ByVal NodeIndex As Integer, _
                                                                     <[In](), [Out]()> _
                                                                     ByVal GUINameNode As TYPE_GUI_NAME) As Boolean

#End Region

#End Region

#End Region

  Private m_auditable As Boolean
  Private m_auditor_code As Integer
  Private m_name As TYPE_FIELD
  Private m_num_fields As Integer
  Private m_field() As TYPE_FIELD
  Private m_guis As CLASS_COLLECTION_ID
  Private m_auditor_codes As CLASS_COLLECTION_ID

  Private m_auditor_list(MAX_AUDITING_LIST) As TYPE_AUDITING_ITEM
  Private m_idx_current_item_list As Integer = 0

  Private m_related_type As ENUM_AUDITOR_RELATED = ENUM_AUDITOR_RELATED.UNKNOWN
  Private m_related_id As Int64 = -1

  Public Enum ENUM_FIELD_TYPE
    FIELD_VISIBLE = 0
    FIELD_HIDDEN = 1
    FIELD_NO_DATA = 2
    'IRP 
    FIELD_ALWAYS_VISIBLE = 3
  End Enum

  Private Structure TYPE_FIELD
    Dim name As String
    Dim value As String
    Dim type As ENUM_FIELD_TYPE
    Dim is_array As Boolean
    Dim array_length As Integer
    Dim array_value() As String
  End Structure

  Public Enum ENUM_AUDITOR_LEVEL
    ONE = 0
    DETAILS = 1
  End Enum

  Public Enum ENUM_AUDITOR_OPERATIONS
    INSERT = 0
    UPDATE = 1
    DELETE = 2
    GENERIC = 3
  End Enum

  'Public Enum ENUM_AUDITOR_CODE
  '  GENERIC = 0
  '  HIERARCHIES = 1
  '  ALARMS = 2
  '  SERIES = 3
  '  LOGIN_LOGOUT = 4
  '  GAMES5 = 5
  '  PRICE_LEVELS = 6
  '  USERS = 7
  '  PROFILES = 8
  '  PRIZE_PLANS = 9
  '  STOCKS = 10
  '  IPC = 11
  '  COMPUTERS = 12
  '  SCHEDULING_CONNECTIONS = 13
  '  MUSIC_PARAMS = 14
  '  COMMUNICATION_MANAGER = 15
  '  BOOKS_MANAGMENT = 16
  '  JACKPOT_MANAGER = 17
  '  DISPLAY_CONFIG = 18
  '  REGIONAL_OPTIONS = 19
  '  GENERAL_PARAMS = 20
  '  SYSTEM_MONITOR = 21
  '  INVOICE_SYSTEM = 22
  '  SOFTWARE_DOWNLOAD = 23
  'End Enum

  Public Enum ENUM_AUDITOR_RELATED
    UNKNOWN = 0
    TERMINAL = 1
  End Enum

  Public ReadOnly Property GUI() As CLASS_COLLECTION_ID
    Get
      Return m_guis
    End Get
  End Property

  Public ReadOnly Property AuditCode() As CLASS_COLLECTION_ID
    Get
      Return m_auditor_codes
    End Get
  End Property

  Public Sub New(ByVal AuditCode As Integer)
    m_guis = New CLASS_COLLECTION_ID("GUIS")
    m_auditor_codes = New CLASS_COLLECTION_ID("AUDIT_CODES")
    Call PopulateGuis()
    Call PopulateAuditCodes()
    m_auditor_code = AuditCode
    m_num_fields = 0
    m_auditable = True
  End Sub

  Private Sub PrivateSetField(ByRef Field As TYPE_FIELD, _
                              ByVal Name As String, _
                              ByVal Value As String, _
                              ByVal NlsId As Integer, _
                              ByVal Type As ENUM_FIELD_TYPE)

    If NlsId <> 0 Then
      Field.name = NLS_GetString(NlsId)
    Else
      Field.name = Name
    End If

    Field.value = Value
    Field.type = Type
    Field.is_array = False

  End Sub

  Public WriteOnly Property IsAuditable() As Boolean
    Set(ByVal Value As Boolean)
      m_auditable = Value
    End Set
  End Property

  Public Sub SetField(ByVal NlsId As Integer, _
                      ByVal Value As String, _
                      Optional ByVal Name As String = vbNullString, _
                      Optional ByVal Type As ENUM_FIELD_TYPE = ENUM_FIELD_TYPE.FIELD_VISIBLE)

    ReDim Preserve m_field(m_num_fields + 1)
    PrivateSetField(m_field(m_num_fields), Name, Value, NlsId, Type)
    m_num_fields = m_num_fields + 1
  End Sub

  Public Sub SetField(ByVal NlsId As Integer, _
                      ByVal Value() As String, _
                      Optional ByVal ElementName As String = "", _
                      Optional ByVal Name As String = vbNullString, _
                      Optional ByVal Type As ENUM_FIELD_TYPE = ENUM_FIELD_TYPE.FIELD_VISIBLE)

    ReDim Preserve m_field(m_num_fields + 1)

    If NlsId <> 0 Then
      m_field(m_num_fields).name = NLS_GetString(NlsId)
    Else
      m_field(m_num_fields).name = Name
    End If

    ' For string Added/Removed 'ElementName' X
    m_field(m_num_fields).value = ElementName
    If IsNothing(Value) Then
      m_field(m_num_fields).array_length = 0
    Else
      m_field(m_num_fields).array_length = Value.Length
    End If
    m_field(m_num_fields).array_value = Value
    m_field(m_num_fields).type = Type
    m_field(m_num_fields).is_array = True

    m_num_fields = m_num_fields + 1
  End Sub

  Public Sub SetName(ByVal NlsId As Integer, _
                     ByVal Value As String, _
                     Optional ByVal Name As String = vbNullString)

    PrivateSetField(m_name, Name, Value, NlsId, ENUM_FIELD_TYPE.FIELD_VISIBLE)
  End Sub

  Public Sub SetRelated(ByVal RelatedType As CLASS_AUDITOR_DATA.ENUM_AUDITOR_RELATED, ByVal RelatedId As Int64)

    m_related_type = RelatedType
    m_related_id = RelatedId

  End Sub

  ' Not used and not adapted to array items
  '
  'Public Sub SetData(ByVal Values As CLASS_AUDITOR_DATA)
  '  Dim idx As Integer
  '  Dim base_name As String

  '  base_name = Values.Name & "."

  '  For idx = 0 To Values.m_num_fields - 1
  '    With Values.m_field(idx)
  '      Me.SetField(base_name & .name, .value)
  '    End With
  '  Next

  'End Sub

  Public ReadOnly Property FieldCount() As Integer
    Get
      Return m_num_fields
    End Get
  End Property

  Public ReadOnly Property Name() As String
    Get
      Return m_name.name
    End Get
  End Property

  Public ReadOnly Property Value() As String
    Get
      Return m_name.value
    End Get
  End Property

  Private ReadOnly Property Field(ByVal Index As Integer) As TYPE_FIELD
    Get
      Dim new_field As TYPE_FIELD
      If Index < FieldCount Then
        Return m_field(Index)
      Else
        new_field = New TYPE_FIELD()
        new_field.is_array = False
        new_field.name = ""
        '--------------------------------------------------
        'XVV 13/04/2007
        'Change ME for CLASS_AUDITOR_DATA
        'new_field.value = Me.AUDIT_NONE_STRING
        new_field.value = CLASS_AUDITOR_DATA.AUDIT_NONE_STRING
        '--------------------------------------------------
        new_field.type = ENUM_FIELD_TYPE.FIELD_VISIBLE
        Return new_field
      End If
    End Get
  End Property

  Public ReadOnly Property FieldName(ByVal Index As Integer) As String
    Get
      Return m_field(Index).name
    End Get
  End Property

  Public ReadOnly Property FieldValue(ByVal Index As Integer) As String
    Get
      If Index < FieldCount Then
        Return m_field(Index).value
      Else
        Return AUDIT_NONE_STRING
      End If
    End Get
  End Property

  Public ReadOnly Property IsEqual(ByVal Target As CLASS_AUDITOR_DATA) As Boolean
    Get
      Dim idx As Integer
      Dim idx_array As Integer

      If Me.m_num_fields <> Target.m_num_fields Then
        Return False
      End If

      ' Check all the field values
      For idx = 0 To m_num_fields - 1
        If Me.m_field(idx).is_array Then
          If Me.m_field(idx).array_length <> Target.m_field(idx).array_length Then
            Return False
          End If
          For idx_array = 0 To Me.m_field(idx).array_length - 1
            If Me.m_field(idx).array_value(idx_array) <> Target.m_field(idx).array_value(idx_array) Then
              Return False
            End If
          Next
        Else
          If Me.m_field(idx).value <> Target.m_field(idx).value Then
            Return False
          End If
        End If
      Next

      Return True

    End Get
  End Property

  Public Function Notify(ByVal GuiId As Integer, _
                         ByVal GuiUserId As Integer, _
                         ByVal GuiUserName As String, _
                         ByVal Operation As ENUM_AUDITOR_OPERATIONS, _
                         ByVal Context As Integer, _
                         Optional ByVal OldData As CLASS_AUDITOR_DATA = Nothing) As Boolean


    Dim idx As Integer
    Dim nls As CLASS_MODULE_NLS
    Dim param_string(5) As String
    Dim nls_id As Integer
    Dim field_name As String
    '------------------------------------------
    'XVV 13/04/2007 
    'Add Nothing value to variable declaration 
    Dim field_value As String = Nothing
    Dim old_field_value As String = Nothing
    '------------------------------------------
    Dim idx_array As Integer
    Dim idx_array_2 As Integer
    Dim not_found As Boolean
    'XVV Variable not use Dim max_array As Integer
    Dim max_num_fields As Integer
    Dim rc As Integer


    nls = New CLASS_MODULE_NLS(mdl_NLS.ENUM_NLS_MODULES.COMMON_MODULE_GUI_COMMONMISC)

    If Not m_auditable Then
      Return True
    End If

    ' Initialize values
    Call Auditor_Clear()

    Select Case Operation
      Case ENUM_AUDITOR_OPERATIONS.INSERT
        For idx = 0 To Me.FieldCount - 1
          Select Case Me.m_field(idx).type
            Case ENUM_FIELD_TYPE.FIELD_HIDDEN
              nls_id = 0

            Case ENUM_FIELD_TYPE.FIELD_NO_DATA
              nls_id = 6
              field_value = ""

            Case ENUM_FIELD_TYPE.FIELD_VISIBLE
              nls_id = 1
              field_value = Me.FieldValue(idx)

            Case ENUM_FIELD_TYPE.FIELD_ALWAYS_VISIBLE
              nls_id = 1
              field_value = Me.FieldValue(idx)
          End Select

          If nls_id > 0 Then
            If Me.m_field(idx).is_array Then
              ' Header of list
              field_value = Me.m_field(idx).array_length

              'OC 20-APR-04 
              'chenged all calls from this point to "Auditor_Add" to handle over flow in the number of audited items
              rc = Auditor_Add(m_auditor_code, _
                             ENUM_AUDITOR_LEVEL.DETAILS, _
                             nls.Id(nls_id), _
                             Me.FieldName(idx), _
                             field_value)
              If rc <> ENUM_AUDITOR_RC.AUDITOR_OK Then
                If rc = ENUM_AUDITOR_RC.AUDITOR_LIST_OVERFLOW Then
                  Exit For
                Else
                  Return False
                End If
              End If

              ' All details of list
              For idx_array = 0 To Me.m_field(idx).array_length - 1
                field_value = AUDIT_IDENT_LIST_STRING & Me.m_field(idx).array_value(idx_array)
                rc = Auditor_Add(m_auditor_code, _
                               ENUM_AUDITOR_LEVEL.DETAILS, _
                               nls.Id(6), _
                               field_value)
                If rc <> ENUM_AUDITOR_RC.AUDITOR_OK Then
                  If rc = ENUM_AUDITOR_RC.AUDITOR_LIST_OVERFLOW Then
                    Exit For
                  Else
                    Return False
                  End If
                End If
              Next

            Else
              rc = Auditor_Add(m_auditor_code, _
                             ENUM_AUDITOR_LEVEL.DETAILS, _
                             nls.Id(nls_id), _
                             Me.FieldName(idx), _
                             field_value)
              If rc <> ENUM_AUDITOR_RC.AUDITOR_OK Then
                If rc = ENUM_AUDITOR_RC.AUDITOR_LIST_OVERFLOW Then
                  Exit For
                Else
                  Return False
                End If
              End If

            End If
          End If
        Next

        rc = Auditor_Add(m_auditor_code, _
                       ENUM_AUDITOR_LEVEL.ONE, _
                       nls.Id(3), _
                       m_name.name, _
                       m_name.value)
        If rc <> ENUM_AUDITOR_RC.AUDITOR_OK And rc <> ENUM_AUDITOR_RC.AUDITOR_LIST_OVERFLOW Then
          Return False
        End If

      Case ENUM_AUDITOR_OPERATIONS.UPDATE
        ' Audit new fields and supresed fields like if inexistent field was an empty field
        If Me.FieldCount >= OldData.FieldCount Then
          max_num_fields = Me.FieldCount
        Else
          max_num_fields = OldData.FieldCount
        End If

        For idx = 0 To max_num_fields - 1

          ' Supresed fields get name from oldData
          If idx < Me.FieldCount Then
            field_name = Me.FieldName(idx)
          Else
            field_name = OldData.FieldName(idx)
          End If
          'IRP 14-MAY-2003 Show this field always
          If Me.Field(idx).type = ENUM_FIELD_TYPE.FIELD_ALWAYS_VISIBLE Then

            field_value = Me.FieldValue(idx)
            rc = Auditor_Add(m_auditor_code, _
                           ENUM_AUDITOR_LEVEL.DETAILS, _
                           nls.Id(1), _
                           Me.FieldName(idx), _
                           Me.FieldValue(idx))
            If rc <> ENUM_AUDITOR_RC.AUDITOR_OK Then
              If rc = ENUM_AUDITOR_RC.AUDITOR_LIST_OVERFLOW Then
                Exit For
              Else
                Return False
              End If
            End If

          ElseIf OldData.FieldValue(idx) <> Me.FieldValue(idx) Or _
                 ChangedArray(Me.Field(idx), OldData.Field(idx)) Then

            Select Case Me.Field(idx).type
              Case ENUM_FIELD_TYPE.FIELD_HIDDEN
                nls_id = 0

              Case ENUM_FIELD_TYPE.FIELD_NO_DATA
                nls_id = 6
                field_value = ""
                old_field_value = ""

              Case ENUM_FIELD_TYPE.FIELD_VISIBLE
                nls_id = 2
                field_value = Me.FieldValue(idx)
                old_field_value = OldData.FieldValue(idx)

            End Select

            If nls_id > 0 Then
              If Me.Field(idx).is_array And OldData.Field(idx).is_array Then

                ' Header of list
                field_value = Me.m_field(idx).array_length
                old_field_value = OldData.m_field(idx).array_length
                rc = Auditor_Add(m_auditor_code, _
                               ENUM_AUDITOR_LEVEL.DETAILS, _
                               nls.Id(nls_id), _
                               field_name, _
                               field_value, _
                               old_field_value)
                If rc <> ENUM_AUDITOR_RC.AUDITOR_OK Then
                  If rc = ENUM_AUDITOR_RC.AUDITOR_LIST_OVERFLOW Then
                    Exit For
                  Else
                    Return False
                  End If
                End If

                ' All details of list
                ' Deleted
                For idx_array = 0 To Me.m_field(idx).array_length - 1
                  not_found = True
                  For idx_array_2 = 0 To OldData.m_field(idx).array_length - 1
                    If Me.m_field(idx).array_value(idx_array) = OldData.m_field(idx).array_value(idx_array_2) Then
                      not_found = False

                      Exit For
                    End If
                  Next

                  If not_found Then
                    field_value = " " & Me.m_field(idx).value & " " & Me.m_field(idx).array_value(idx_array)
                    rc = Auditor_Add(m_auditor_code, _
                                   ENUM_AUDITOR_LEVEL.DETAILS, _
                                   nls.Id(6), _
                                   AUDIT_IDENT_LIST_STRING, _
                                   nls.GetString(10), _
                                   field_value)
                    If rc <> ENUM_AUDITOR_RC.AUDITOR_OK Then
                      If rc = ENUM_AUDITOR_RC.AUDITOR_LIST_OVERFLOW Then
                        Exit For
                      Else
                        Return False
                      End If
                    End If

                  End If
                Next

                ' Added
                For idx_array = 0 To OldData.m_field(idx).array_length - 1
                  not_found = True
                  For idx_array_2 = 0 To Me.m_field(idx).array_length - 1
                    If OldData.m_field(idx).array_value(idx_array) = Me.m_field(idx).array_value(idx_array_2) Then
                      not_found = False

                      Exit For
                    End If
                  Next

                  If not_found Then
                    field_value = " " & OldData.m_field(idx).value & " " & OldData.m_field(idx).array_value(idx_array)
                    rc = Auditor_Add(m_auditor_code, _
                                   ENUM_AUDITOR_LEVEL.DETAILS, _
                                   nls.Id(6), _
                                   AUDIT_IDENT_LIST_STRING, _
                                   nls.GetString(9), _
                                   field_value)
                    If rc <> ENUM_AUDITOR_RC.AUDITOR_OK Then
                      If rc = ENUM_AUDITOR_RC.AUDITOR_LIST_OVERFLOW Then
                        Exit For
                      Else
                        Return False
                      End If
                    End If

                  End If
                Next

              ElseIf Not Me.Field(idx).is_array And Not OldData.Field(idx).is_array Then
                rc = Auditor_Add(m_auditor_code, _
                               ENUM_AUDITOR_LEVEL.DETAILS, _
                               nls.Id(nls_id), _
                               field_name, _
                               field_value, _
                               old_field_value)
                If rc <> ENUM_AUDITOR_RC.AUDITOR_OK Then
                  If rc = ENUM_AUDITOR_RC.AUDITOR_LIST_OVERFLOW Then
                    Exit For
                  Else
                    Return False
                  End If
                End If

              End If

            End If
          End If
        Next

        rc = Auditor_Add(m_auditor_code, _
                       ENUM_AUDITOR_LEVEL.ONE, _
                       nls.Id(4), _
                       m_name.name, _
                       m_name.value)
        If rc <> ENUM_AUDITOR_RC.AUDITOR_OK And rc <> ENUM_AUDITOR_RC.AUDITOR_LIST_OVERFLOW Then
          Return False
        End If

      Case ENUM_AUDITOR_OPERATIONS.DELETE
        For idx = 0 To Me.FieldCount - 1
          Select Case Me.m_field(idx).type
            Case ENUM_FIELD_TYPE.FIELD_HIDDEN
              nls_id = 0

            Case ENUM_FIELD_TYPE.FIELD_NO_DATA
              nls_id = 6
              field_value = ""

            Case ENUM_FIELD_TYPE.FIELD_VISIBLE
              nls_id = 1
              field_value = Me.FieldValue(idx)

            Case ENUM_FIELD_TYPE.FIELD_ALWAYS_VISIBLE
              nls_id = 1
              field_value = Me.FieldValue(idx)
          End Select
          If nls_id > 0 Then
            If Me.m_field(idx).is_array Then
              ' Header of list
              field_value = Me.m_field(idx).array_length
              rc = Auditor_Add(m_auditor_code, _
                             ENUM_AUDITOR_LEVEL.DETAILS, _
                             nls.Id(nls_id), _
                             Me.FieldName(idx), _
                             field_value)
              If rc <> ENUM_AUDITOR_RC.AUDITOR_OK Then
                If rc = ENUM_AUDITOR_RC.AUDITOR_LIST_OVERFLOW Then
                  Exit For
                Else
                  Return False
                End If
              End If

              ' All details of list
              For idx_array = 0 To Me.m_field(idx).array_length - 1
                field_value = AUDIT_IDENT_LIST_STRING & Me.m_field(idx).array_value(idx_array)
                rc = Auditor_Add(m_auditor_code, _
                               ENUM_AUDITOR_LEVEL.DETAILS, _
                               nls.Id(6), _
                               field_value)
                If rc <> ENUM_AUDITOR_RC.AUDITOR_OK Then
                  If rc = ENUM_AUDITOR_RC.AUDITOR_LIST_OVERFLOW Then
                    Exit For
                  Else
                    Return False
                  End If
                End If
              Next

            Else
              rc = Auditor_Add(m_auditor_code, _
                             ENUM_AUDITOR_LEVEL.DETAILS, _
                             nls.Id(nls_id), _
                             Me.FieldName(idx), _
                             field_value)
              If rc <> ENUM_AUDITOR_RC.AUDITOR_OK Then
                If rc = ENUM_AUDITOR_RC.AUDITOR_LIST_OVERFLOW Then
                  Exit For
                Else
                  Return False
                End If
              End If

            End If
          End If
        Next

        rc = Auditor_Add(m_auditor_code, _
                       ENUM_AUDITOR_LEVEL.ONE, _
                       nls.Id(5), _
                       m_name.name, _
                       m_name.value)
        If rc <> ENUM_AUDITOR_RC.AUDITOR_OK And rc <> ENUM_AUDITOR_RC.AUDITOR_LIST_OVERFLOW Then
          Return False
        End If

      Case ENUM_AUDITOR_OPERATIONS.GENERIC
        For idx = 0 To Me.FieldCount - 1
          Select Case Me.m_field(idx).type
            Case ENUM_FIELD_TYPE.FIELD_HIDDEN
              nls_id = 0

            Case ENUM_FIELD_TYPE.FIELD_NO_DATA
              nls_id = 6
              field_value = ""

            Case ENUM_FIELD_TYPE.FIELD_VISIBLE
              nls_id = 1
              field_value = Me.FieldValue(idx)

            Case ENUM_FIELD_TYPE.FIELD_ALWAYS_VISIBLE
              nls_id = 1
              field_value = Me.FieldValue(idx)
          End Select

          If nls_id > 0 Then
            If Me.m_field(idx).is_array Then
              ' Header of list
              field_value = Me.m_field(idx).array_length
              rc = Auditor_Add(m_auditor_code, _
                             ENUM_AUDITOR_LEVEL.DETAILS, _
                             nls.Id(nls_id), _
                             Me.FieldName(idx), _
                             field_value)
              If rc <> ENUM_AUDITOR_RC.AUDITOR_OK Then
                If rc = ENUM_AUDITOR_RC.AUDITOR_LIST_OVERFLOW Then
                  Exit For
                Else
                  Return False
                End If
              End If

              ' All details of list
              For idx_array = 0 To Me.m_field(idx).array_length - 1
                field_value = AUDIT_IDENT_LIST_STRING & Me.m_field(idx).array_value(idx_array)
                rc = Auditor_Add(m_auditor_code, _
                               ENUM_AUDITOR_LEVEL.DETAILS, _
                               nls.Id(6), _
                               field_value)
                If rc <> ENUM_AUDITOR_RC.AUDITOR_OK Then
                  If rc = ENUM_AUDITOR_RC.AUDITOR_LIST_OVERFLOW Then
                    Exit For
                  Else
                    Return False
                  End If
                End If
              Next
            Else
              rc = Auditor_Add(m_auditor_code, _
                             ENUM_AUDITOR_LEVEL.DETAILS, _
                             nls.Id(nls_id), _
                             Me.FieldName(idx), _
                             Me.FieldValue(idx))
              If rc <> ENUM_AUDITOR_RC.AUDITOR_OK Then
                If rc = ENUM_AUDITOR_RC.AUDITOR_LIST_OVERFLOW Then
                  Exit For
                Else
                  Return False
                End If
              End If
            End If
          End If
        Next

        ' Split auditor string 
        Call NLS_SplitString(m_name.value, _
                             param_string(0), _
                             param_string(1), _
                             param_string(2), _
                             param_string(3), _
                             param_string(4))
        rc = Auditor_Add(m_auditor_code, _
                       ENUM_AUDITOR_LEVEL.ONE, _
                       nls.Id(6), _
                       param_string(0), _
                       param_string(1), _
                       param_string(2), _
                       param_string(3), _
                       param_string(4))
        If rc <> ENUM_AUDITOR_RC.AUDITOR_OK And rc <> ENUM_AUDITOR_RC.AUDITOR_LIST_OVERFLOW Then
          Return False
        End If

      Case Else

    End Select

    If Auditor_Flush(GuiId, _
                     GuiUserId, _
                     GuiUserName, _
                     Context) <> ENUM_AUDITOR_RC.AUDITOR_OK Then
      Return False
    End If

    Return True
  End Function

  Public Shared Function ColumnsRelatedExists() As Boolean
    Dim _str_sql As String

    Try
      Using _db_trx As New WSI.Common.DB_TRX()
        _str_sql = "SELECT 1 FROM sys.columns WHERE object_id = object_id(N'[dbo].[GUI_AUDIT]') and name = 'GA_RELATED_TYPE'"

        Using _cmd As New SqlClient.SqlCommand(_str_sql)
          Return _db_trx.ExecuteScalar(_cmd) <> Nothing
        End Using
      End Using
    Catch
    End Try

    Return False

  End Function

  Private Function ChangedArray(ByVal Field As TYPE_FIELD, ByVal OldField As TYPE_FIELD) As Boolean
    'XVV Variable not use Dim max_array As Integer
    Dim idx_array As Integer

    ' Check all the field values
    If Field.is_array Then
      If Field.array_length <> OldField.array_length Then
        Return True
      End If
      For idx_array = 0 To Field.array_length - 1
        If Field.array_value(idx_array) <> OldField.array_value(idx_array) Then
          Return True
        End If
      Next
    End If

    Return False

  End Function

  Private Sub PopulateGuis()
    Dim idx_gui_name As Integer
    Dim num_nodes As Integer
    Dim aux_type_gui_name As New TYPE_GUI_NAME
    'XVV Variable not use Dim rc_bool As Boolean

    num_nodes = Common_GUINameNumNodes()
    For idx_gui_name = 0 To num_nodes - 1
      If (Common_GUINameGetNode(idx_gui_name, aux_type_gui_name) = False) Then
        'Throw Error
      End If
      m_guis.Add(aux_type_gui_name.gui_id, aux_type_gui_name.nls_id)
    Next

  End Sub

  Private Sub PopulateAuditCodes()
    Dim idx_audit_name As Integer
    Dim num_nodes As Integer
    Dim aux_type_audit_name As New TYPE_AUDIT_NAME
    'XVV Variable not use Dim rc_bool As Boolean

    num_nodes = Common_AuditNameNumNodes()
    For idx_audit_name = 0 To num_nodes - 1
      If (Common_AuditNameGetNode(idx_audit_name, aux_type_audit_name) = False) Then
        'Throw Error
      End If
      m_auditor_codes.Add(aux_type_audit_name.audit_code, aux_type_audit_name.nls_id)
    Next

  End Sub


  Private Sub Auditor_Clear()

    m_idx_current_item_list = 0

  End Sub

  Private Function Auditor_Add(ByVal AuditCode As Integer, _
                                ByVal AuditLevel As Integer, _
                                ByVal NlsCode As Integer, _
                                Optional ByVal Param1 As String = "", _
                                Optional ByVal Param2 As String = "", _
                                Optional ByVal Param3 As String = "", _
                                Optional ByVal Param4 As String = "", _
                                Optional ByVal Param5 As String = "") As Integer

    Dim idx_audit As Integer

    If m_idx_current_item_list >= MAX_AUDITING_LIST Then

      Common_LoggerMsg(ENUM_LOG_MSG.LOG_UNEXPECTED_VALUE_ERROR, _
                       MODULE_NAME, _
                       "Auditor_Add", _
                       "idx_current_item_list", _
                       MAX_AUDITING_LIST)

      Return ENUM_AUDITOR_RC.AUDITOR_LIST_OVERFLOW
    End If

    idx_audit = m_idx_current_item_list

    m_auditor_list(idx_audit).audit_code = AuditCode
    m_auditor_list(idx_audit).audit_level = AuditLevel
    m_auditor_list(idx_audit).item_order = idx_audit
    m_auditor_list(idx_audit).nls_code = NlsCode
    m_auditor_list(idx_audit).nls_param_1 = Param1
    m_auditor_list(idx_audit).nls_param_2 = Param2
    m_auditor_list(idx_audit).nls_param_3 = Param3
    m_auditor_list(idx_audit).nls_param_4 = Param4
    m_auditor_list(idx_audit).nls_param_5 = Param5
    m_auditor_list(idx_audit).related_type = m_related_type
    m_auditor_list(idx_audit).related_id = m_related_id

    m_idx_current_item_list = m_idx_current_item_list + 1

    Return ENUM_AUDITOR_RC.AUDITOR_OK
  End Function

  Private Function Auditor_Flush(ByVal GuiId As Short, _
                                 ByVal GuiUserId As Integer, _
                                 ByVal GuiUserName As String, _
                                 ByVal Context As Integer, _
                                 Optional ByVal firstIntent As Boolean = True) As Integer

    Dim str_sql As String
    Dim db_audit_id As Integer
    Dim idx_audit As Integer
    Dim data_table As DataTable
    Dim computer_name As String
    Dim _sql_command As SqlClient.SqlCommand
    Dim _sql_tx As SqlClient.SqlTransaction
    Dim _rc As Integer
    Dim _ga_related_exists As Boolean

    _sql_tx = Nothing
    _rc = 0

    computer_name = Environment.MachineName

    If m_idx_current_item_list = 0 Then
      Return ENUM_AUDITOR_RC.AUDITOR_OK
    End If

    ' Obtain the auditor id
    str_sql = "SELECT  ISNULL(MAX(GA_AUDIT_ID), 0) + 1 " & _
         " FROM    GUI_AUDIT "


    data_table = GUI_GetTableUsingSQL(str_sql, 1)
    If IsNothing(data_table) Then
      db_audit_id = 0
    Else
      db_audit_id = data_table.Rows.Item(0).Item(0)
    End If

    If Not GUI_BeginSQLTransaction(_sql_tx) Then
      Return ENUM_AUDITOR_RC.AUDITOR_ERROR_DB
    End If



    For idx_audit = 0 To m_idx_current_item_list - 1

      m_auditor_list(idx_audit).nls_param_1 = NullTrim(m_auditor_list(idx_audit).nls_param_1)
      m_auditor_list(idx_audit).nls_param_2 = NullTrim(m_auditor_list(idx_audit).nls_param_2)
      m_auditor_list(idx_audit).nls_param_3 = NullTrim(m_auditor_list(idx_audit).nls_param_3)
      m_auditor_list(idx_audit).nls_param_4 = NullTrim(m_auditor_list(idx_audit).nls_param_4)
      m_auditor_list(idx_audit).nls_param_5 = NullTrim(m_auditor_list(idx_audit).nls_param_5)

      ' APB (05-MAR-2009)
      ' Trim strings to make sure they fit in the database
      If computer_name.Length > AUDIT_ITEM_LENGTH Then
        computer_name = computer_name.Substring(0, AUDIT_ITEM_LENGTH)
      End If

      If m_auditor_list(idx_audit).nls_param_1.Length > MAX_AUDIT_ITEM_LENGTH Then
        m_auditor_list(idx_audit).nls_param_1 = m_auditor_list(idx_audit).nls_param_1.Substring(0, MAX_AUDIT_ITEM_LENGTH)
      End If

      If m_auditor_list(idx_audit).nls_param_2.Length > MAX_AUDIT_ITEM_LENGTH Then
        m_auditor_list(idx_audit).nls_param_2 = m_auditor_list(idx_audit).nls_param_2.Substring(0, MAX_AUDIT_ITEM_LENGTH)
      End If

      If m_auditor_list(idx_audit).nls_param_3.Length > MAX_AUDIT_ITEM_LENGTH Then
        m_auditor_list(idx_audit).nls_param_3 = m_auditor_list(idx_audit).nls_param_3.Substring(0, MAX_AUDIT_ITEM_LENGTH)
      End If

      If m_auditor_list(idx_audit).nls_param_4.Length > AUDIT_ITEM_LENGTH Then
        m_auditor_list(idx_audit).nls_param_4 = m_auditor_list(idx_audit).nls_param_4.Substring(0, AUDIT_ITEM_LENGTH)
      End If

      If m_auditor_list(idx_audit).nls_param_5.Length > AUDIT_ITEM_LENGTH Then
        m_auditor_list(idx_audit).nls_param_5 = m_auditor_list(idx_audit).nls_param_5.Substring(0, AUDIT_ITEM_LENGTH)
      End If

      _ga_related_exists = ColumnsRelatedExists()

      str_sql = "INSERT INTO   GUI_AUDIT " & _
                     " ( GA_AUDIT_ID " & _
                     " , GA_GUI_ID " & _
                     " , GA_GUI_USER_ID " & _
                     " , GA_GUI_USERNAME " & _
                     " , GA_COMPUTER_NAME " & _
                     " , GA_DATETIME " & _
                     " , GA_AUDIT_CODE " & _
                     " , GA_ITEM_ORDER " & _
                     " , GA_AUDIT_LEVEL " & _
                     " , GA_NLS_ID " & _
                     " , GA_NLS_PARAM01 " & _
                     " , GA_NLS_PARAM02 " & _
                     " , GA_NLS_PARAM03 " & _
                     " , GA_NLS_PARAM04 " & _
                     " , GA_NLS_PARAM05 "
      If _ga_related_exists Then
        str_sql += " , GA_RELATED_TYPE " & _
                   " , GA_RELATED_ID "
      End If
      str_sql += " ) " & _
                 " VALUES " & _
                 " ( @pAuditId " & _
                 " , @pGUIId " & _
                 " , @pGUIUserId " & _
                 " , @pGUIUserName " & _
                 " , @pComputerName " & _
                 " , GETDATE() " & _
                 " , @pAuditCode " & _
                 " , @pItemOrder " & _
                 " , @pAuditLevel " & _
                 " , @pNLSId " & _
                 " , @pNLSParam01 " & _
                 " , @pNLSParam02 " & _
                 " , @pNLSParam03 " & _
                 " , @pNLSParam04 " & _
                 " , @pNLSParam05 "
      If _ga_related_exists Then
        str_sql += " , @pRelatedType " & _
                   " , @pRelatedId "
      End If
      str_sql += " ) "

      _sql_command = New SqlClient.SqlCommand(str_sql, _sql_tx.Connection, _sql_tx)
      _sql_command.Parameters.Add("@pAuditId", SqlDbType.BigInt).Value = db_audit_id
      _sql_command.Parameters.Add("@pGUIId", SqlDbType.Int).Value = GuiId
      _sql_command.Parameters.Add("@pGUIUserId", SqlDbType.Int).Value = GuiUserId
      _sql_command.Parameters.Add("@pGUIUserName", SqlDbType.NVarChar).Value = GuiUserName
      _sql_command.Parameters.Add("@pComputerName", SqlDbType.NVarChar).Value = computer_name
      _sql_command.Parameters.Add("@pAuditCode", SqlDbType.Int).Value = m_auditor_list(idx_audit).audit_code
      _sql_command.Parameters.Add("@pItemOrder", SqlDbType.Int).Value = m_auditor_list(idx_audit).item_order
      _sql_command.Parameters.Add("@pAuditLevel", SqlDbType.Int).Value = m_auditor_list(idx_audit).audit_level
      _sql_command.Parameters.Add("@pNLSId", SqlDbType.Int).Value = m_auditor_list(idx_audit).nls_code
      _sql_command.Parameters.Add("@pNLSParam01", SqlDbType.NVarChar).Value = m_auditor_list(idx_audit).nls_param_1
      _sql_command.Parameters.Add("@pNLSParam02", SqlDbType.NVarChar).Value = m_auditor_list(idx_audit).nls_param_2
      _sql_command.Parameters.Add("@pNLSParam03", SqlDbType.NVarChar).Value = m_auditor_list(idx_audit).nls_param_3
      _sql_command.Parameters.Add("@pNLSParam04", SqlDbType.NVarChar).Value = m_auditor_list(idx_audit).nls_param_4
      _sql_command.Parameters.Add("@pNLSParam05", SqlDbType.NVarChar).Value = m_auditor_list(idx_audit).nls_param_5
      _sql_command.Parameters.Add("@pRelatedType", SqlDbType.Int).Value = m_auditor_list(idx_audit).related_type
      _sql_command.Parameters.Add("@pRelatedId", SqlDbType.BigInt).Value = IIf(m_auditor_list(idx_audit).related_id = -1, DBNull.Value, m_auditor_list(idx_audit).related_id)

      Try

        _rc = _sql_command.ExecuteNonQuery()

      Catch _exception As SqlClient.SqlException

        If firstIntent Then
          Auditor_Flush(GuiId, GuiUserId, GuiUserName, Context, False)
        End If

      End Try



      If _rc <> 1 Then
        ' Rollback  
        GUI_EndSQLTransaction(_sql_tx, False)

        Return ENUM_AUDITOR_RC.AUDITOR_ERROR_DB
      End If

    Next

    ' Commit
    If Not GUI_EndSQLTransaction(_sql_tx, True) Then
      Return ENUM_AUDITOR_RC.AUDITOR_ERROR_DB
    End If

    Return ENUM_AUDITOR_RC.AUDITOR_OK
  End Function ' Auditor_Flush


End Class
