'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_center_configuration.vb
' DESCRIPTION:   
' AUTHOR:        
' CREATION DATE: 
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------  ------ -----------------------------------------------
'                    Initial version
'-------------------------------------------------------------------

Imports GUI_CommonMisc

Public Class CLASS_CENTER_CONFIGURATION
 
  '------------------------------------------------------------------------
  ' Registry configuration declarations
  '------------------------------------------------------------------------
  Public Const MAX_USER_LEN = 20
  Public Const MAX_PASSWORD_LEN = 20
  Public Const MAX_CONNECT_STRING_LEN = 30
  Public Const MAX_REGISTRY_KEY_LEN = 255
  Public Const MAX_LOG_PATH_LEN = 260

  Private Structure TYPE_CENTER_DATABASE_CONF
    Public user As String
    Public password As String
    Public connect_string As String
  End Structure

  Private Structure TYPE_CENTER_CONFIGURATION
    Public control_block As Integer
    Public database As TYPE_CENTER_DATABASE_CONF
    Public language As Integer
  End Structure

  Private m_configuration As TYPE_CENTER_CONFIGURATION
  Private m_registry_key As String

  Private Declare Function Common_GetConfigurationFile Lib "CommonBase" (ByRef pConfiguration As TYPE_CENTER_CONFIGURATION) As Integer
  Private Declare Function Common_GetDBUserPassword Lib "CommonBase" (ByVal pUserName As String, _
                                                                      ByVal pConnectString As String, _
                                                                      ByVal pPassword As String) As Integer
  'Private Declare Function Common_SaveConfiguration Lib "CommonMisc" (ByRef pConfiguration As TYPE_CENTER_CONFIGURATION) As Integer
  'Private Declare Function Common_CreateFileConfiguration Lib "CommonMisc" () As Integer
  'Private Declare Function Common_GetRegistryPath Lib "CommonMisc" (ByVal RegistryPath As String) As Integer

  Public Property RegistryKey() As String
    Get
      Return m_registry_key
    End Get
    Set(ByVal Value As String)
      m_registry_key = Value
    End Set
  End Property

  Public Property User() As String
    Get
      Return m_configuration.database.user
    End Get
    Set(ByVal Value As String)
      m_configuration.database.user = Value
    End Set
  End Property

  Public Property Password() As String
    Get
      Return m_configuration.database.password
    End Get
    Set(ByVal Value As String)
      m_configuration.database.password = Value
    End Set
  End Property

  Public Property ConnectString() As String
    Get
      Return m_configuration.database.connect_string
    End Get
    Set(ByVal Value As String)
      m_configuration.database.connect_string = Value
    End Set
  End Property

  Public Property Language() As Integer
    Get
      Return m_configuration.language
    End Get
    Set(ByVal Value As Integer)
      m_configuration.language = Value
    End Set
  End Property

  Public Function DB_BeforeRead(ByVal ObjectId As Object) As CLASS_BASE.ENUM_STATUS
    Dim status_configuration As Integer

    m_configuration.control_block = Len(m_configuration)
    m_configuration.database.connect_string = FixedString(MAX_CONNECT_STRING_LEN)
    m_configuration.database.user = FixedString(MAX_USER_LEN)

    status_configuration = Common_GetConfigurationFile(m_configuration)
    
    If status_configuration <> ENUM_COMMON_RC.COMMON_OK Then

      Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
    End If

    m_configuration.database.connect_string = NullTrim(m_configuration.database.connect_string)
    m_configuration.database.user = NullTrim(m_configuration.database.user)

    ' Always return OK, if error edit blank variables
    Return CLASS_BASE.ENUM_STATUS.STATUS_OK

  End Function

  Public Function DB_Read(ByVal ObjectId As Object) As CLASS_BASE.ENUM_STATUS
    Dim status_configuration As Integer

    m_configuration.database.password = FixedString(MAX_PASSWORD_LEN)

    status_configuration = Common_GetDBUserPassword(m_configuration.database.user, m_configuration.database.connect_string, m_configuration.database.password)

    If status_configuration <> ENUM_COMMON_RC.COMMON_OK Then
      Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
    End If

    m_configuration.database.password = NullTrim(m_configuration.database.password)

    ' Always return OK, if error edit blank variables
    Return CLASS_BASE.ENUM_STATUS.STATUS_OK

  End Function

End Class


