'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_prize_plan.vb
' DESCRIPTION:   Prize Plan class
' AUTHOR:        Jaume Sala
' CREATION DATE: 03-SEP-2002
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 03-SEP-2002  JSV    Initial version.
'
'-------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports System.Runtime.InteropServices



'Public Class CLASS_PRIZE_PLAN_KEY
'  Private m_gm_id As Integer
'  Private m_pp_id As Integer

'#Region " Properties "

'  Public Property GameId() As Integer
'    Get
'      Return m_gm_id
'    End Get
'    Set(ByVal Value As Integer)
'      m_gm_id = Value
'    End Set
'  End Property


'  Public Property PrizePlanId() As Integer
'    Get
'      Return m_pp_id
'    End Get
'    Set(ByVal Value As Integer)
'      m_pp_id = Value
'    End Set
'  End Property

'#End Region


'End Class


'Public Class CLASS_PRIZE_PLAN
'  Inherits CLASS_BASE

'#Region " Constants "

'  'Match with constant in CommonData.dll
'  Public AUDIT_NAME_PRIZE_PLANS As Integer = 9


'  ' Fields length
'  Public Const MAX_PP_NAME_LEN As Integer = 20
'  Public Const MAX_PP_DESC_LEN As Integer = 40
'  Public Const MAX_PERCENTAGE_LEN As Integer = 5 + 1
'  Public Const MAX_AMOUNT_LEN As Integer = 12 + 1
'  Public Const MAX_DECIMAL_LEN As Integer = 2

'  Public Const MAX_PERCENTAGE_VALUE As Integer = 100
'  Public Const MAX_AMOUNT_VALUE As Double = 9999999999.99
'  Public Const MAX_PP_CATEGORIES As Integer = 20
'  Public Const MAX_PP_SERIES_STATUS As Integer = 10

'  Public Const MIN_AMOUNT_VALUE As Double = 1.0
'  Public Const MIN_PCT_WIN_VALUE As Double = 0.01

'  Public Const WIN_JACKPOT_YES As Integer = 1
'  Public Const WIN_JACKPOT_NO As Integer = 0

'  Public Const AUDIT_NONE_STRING As String = "---"

'  Public Enum ENUM_CATEGORY_TYPE
'    CATEGORY_TYPE_NON_REFOUNDABLE = 1
'    CATEGORY_TYPE_MINOR = 2
'    CATEGORY_TYPE_MAJOR = 3
'    CATEGORY_TYPE_SPECIAL = 4
'  End Enum

'  Private Enum ENUM_ORDERBY
'    ORDERBY_ID = 0
'    ORDERBY_AMOUNT = 1
'  End Enum

'  Private Enum ENUM_CONFIGURATION_RC
'    CONFIGURATION_OK = 0
'    CONFIGURATION_ERROR = 1
'    CONFIGURATION_ERROR_CONTROL_BLOCK = 2
'    CONFIGURATION_DB_NOT_CONNECT = 3
'    CONFIGURATION_ERROR_DB = 4
'    CONFIGURATION_ERROR_NOT_FOUND = 5
'    CONFIGURATION_ERROR_REGISTRY = 6
'    CONFIGURATION_ERROR_CONTEXT = 7
'    CONFIGURATION_ERROR_DUPLICATE_KEY = 8
'    CONFIGURATION_ERROR_ARRAY_OVERFLOW = 9
'    CONFIGURATION_ERROR_DEPENDENCIES = 10
'    CONFIGURATION_ERROR_DUPLICATE_AS = 11
'    ' JSV 12-JUL-2002
'    ' Add enumeration for password error
'    CONFIGURATION_ERROR_PASSWORD = 12
'    ' JSV 16-JUL-2002
'    ' Add enumeration for more registers to read
'    CONFIGURATION_OK_MORE = 13
'    CONFIGURATION_ERROR_ALREADY_EXISTS = 14
'  End Enum

'#End Region

'#Region " GUI_Configuration.dll "

'#Region " Structures "

'  <StructLayout(LayoutKind.Sequential)> _
'Public Class TYPE_PP_SERIES_STATUS
'    Public status_id As Integer
'    Public num_series As Integer
'  End Class

'  ' Item of category array
'  ' NOTE: Warning: Order is important because Doubles alignment (by increments of 8 bytes?).
'  <StructLayout(LayoutKind.Sequential)> _
'  Public Class TYPE_PP_CATEGORY
'    Public cat_id As Integer
'    Public type_win_cat As Integer
'    Public pct_win_cat As Double
'    Public amt_win_cat As Double
'    Public pct_tax_win_cat As Double
'    Public pct_deviation_cat As Double
'  End Class

'  ' NOTE: Warning: Order is important because Doubles alignment (by increments of 8 bytes?).
'  '       Add a filler_2 at the end because after doubles, apparently, all needs to be 8 bytes multiple. 
'  <StructLayout(LayoutKind.Sequential)> _
'   Public Class TYPE_PRIZE_PLAN
'    Public control_block As Integer
'    Public prize_plan_id As Integer
'    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_PP_NAME_LEN + 1)> _
'    Public name As String
'    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=3)> _
'    Public filler_0 As String
'    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_PP_DESC_LEN + 1)> _
'    Public description As String
'    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=3)> _
'    Public filler_1 As String
'    Public win_jackpot As Integer
'    Public prizes_pct As Double
'    Public jackpot_pct As Double
'    Public categories As New CLASS_VECTOR()
'    Public series_status As New CLASS_VECTOR()
'    Public expiration_days As Integer
'    Public game_id As Integer
'    Public min_cats As Integer
'    Public max_cats As Integer


'    Public Sub New(Optional ByVal NumCatItems As Integer = MAX_PP_CATEGORIES, _
'                   Optional ByVal NumSeriesStatusItems As Integer = MAX_PP_SERIES_STATUS)
'      MyBase.New()
'      control_block = Marshal.SizeOf(GetType(TYPE_PRIZE_PLAN))
'      Call categories.Init(GetType(TYPE_PP_CATEGORY), NumCatItems)
'      Call series_status.Init(GetType(TYPE_PP_SERIES_STATUS), NumSeriesStatusItems)

'    End Sub

'    Protected Overrides Sub Finalize()
'      Call categories.Done()
'      Call series_status.Done()
'      Call MyBase.Finalize()
'    End Sub

'  End Class

'#End Region

'#Region " Prototypes "

'  Protected Declare Function ReadPrizePlan Lib "GUI_Configuration" (<[In](), [Out]()> _
'                                                                    ByVal pPrizePlan As TYPE_PRIZE_PLAN, _
'                                                                    ByVal Context As Integer) As Integer

'  Protected Declare Function UpdatePrizePlan Lib "GUI_Configuration" (<[In](), [Out]()> _
'                                                                      ByVal pPrizePlan As TYPE_PRIZE_PLAN, _
'                                                                      ByVal Context As Integer) As Integer

'  Protected Declare Function InsertPrizePlan Lib "GUI_Configuration" (<[In](), [Out]()> _
'                                                                      ByVal pPrizePlan As TYPE_PRIZE_PLAN, _
'                                                                      ByVal Context As Integer) As Integer

'  Protected Declare Function DeletePrizePlan Lib "GUI_Configuration" (ByVal PrizePlanId As Integer, _
'                                                                      ByVal GameId As Integer, _
'                                                                      ByVal Context As Integer) As Integer

'#End Region

'#End Region

'#Region " Members "

'  Protected m_prize_plan As New TYPE_PRIZE_PLAN()

'#End Region

'#Region " Properties "

'  Public Property PrizePlanId() As Integer
'    Get
'      Return m_prize_plan.prize_plan_id
'    End Get
'    Set(ByVal Value As Integer)
'      m_prize_plan.prize_plan_id = Value
'    End Set
'  End Property

'  Public Property Name() As String
'    Get
'      Return m_prize_plan.name
'    End Get
'    Set(ByVal Value As String)
'      m_prize_plan.name = Value
'    End Set
'  End Property

'  Public Property Description() As String
'    Get
'      Return m_prize_plan.description
'    End Get
'    Set(ByVal Value As String)
'      m_prize_plan.description = Value
'    End Set
'  End Property

'  Public Property PrizesPct() As Double
'    Get
'      Return m_prize_plan.prizes_pct
'    End Get
'    Set(ByVal Value As Double)
'      m_prize_plan.prizes_pct = Value
'    End Set
'  End Property

'  Public Property JackpotPct() As Double
'    Get
'      Return m_prize_plan.jackpot_pct
'    End Get
'    Set(ByVal Value As Double)
'      m_prize_plan.jackpot_pct = Value
'    End Set
'  End Property

'  Public Property WinJackpot() As Integer
'    Get
'      Return m_prize_plan.win_jackpot
'    End Get
'    Set(ByVal Value As Integer)
'      m_prize_plan.win_jackpot = Value
'    End Set
'  End Property

'  Public Property Categories() As CLASS_VECTOR
'    Get
'      Return m_prize_plan.categories
'    End Get
'    Set(ByVal Value As CLASS_VECTOR)
'      m_prize_plan.categories = Value
'    End Set
'  End Property

'  Public ReadOnly Property SeriesStatus() As CLASS_VECTOR
'    Get
'      Return m_prize_plan.series_status
'    End Get
'  End Property

'  Public Property ExpirationDays() As Integer
'    Get
'      Return m_prize_plan.expiration_days
'    End Get
'    Set(ByVal Value As Integer)
'      m_prize_plan.expiration_days = Value
'    End Set
'  End Property

'  Public Property GameId() As Integer
'    Get
'      Return m_prize_plan.game_id
'    End Get
'    Set(ByVal Value As Integer)
'      m_prize_plan.game_id = Value
'    End Set
'  End Property

'  Public ReadOnly Property GameMinCats() As Integer
'    Get
'      Return m_prize_plan.min_cats
'    End Get
'  End Property

'  Public ReadOnly Property GameMaxCats() As Integer
'    Get
'      Return m_prize_plan.max_cats
'    End Get
'  End Property

'#End Region

'#Region " Overrides functions "

'  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

'    Dim rc As Integer
'    Dim key As New CLASS_PRIZE_PLAN_KEY

'    key = ObjectId
'    Me.PrizePlanId = key.PrizePlanId
'    Me.GameId = key.GameId

'    ' Read prize_plan data
'    rc = ReadPrizePlan(m_prize_plan, SqlCtx)

'    'Sort Categories by import (Teorically it is allready sort into table)
'    Me.SortCategories()

'    Select Case rc

'      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
'        m_prize_plan.name = NullTrim(m_prize_plan.name)
'        m_prize_plan.description = NullTrim(m_prize_plan.description)
'        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

'      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK_MORE
'        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

'      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
'        Return ENUM_STATUS.STATUS_NOT_FOUND

'      Case Else
'        Return ENUM_STATUS.STATUS_ERROR

'    End Select

'  End Function

'  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

'    Dim rc As Integer

'    'Sort Categories by import 
'    Me.SortCategories()

'    ' Update prize_plan data
'    rc = UpdatePrizePlan(m_prize_plan, SqlCtx)

'    Select Case rc
'      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
'        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

'      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_ALREADY_EXISTS
'        Return CLASS_BASE.ENUM_STATUS.STATUS_ALREADY_EXISTS

'      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
'        Return ENUM_STATUS.STATUS_NOT_FOUND

'      Case Else
'        Return ENUM_STATUS.STATUS_ERROR

'    End Select

'  End Function

'  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

'    Dim rc As Integer

'    'Sort Categories by import 
'    Me.SortCategories()

'    ' Insert prize_plan data
'    rc = InsertPrizePlan(m_prize_plan, SqlCtx)

'    Select Case rc
'      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
'        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

'      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
'        Return ENUM_STATUS.STATUS_DUPLICATE_KEY

'      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_ALREADY_EXISTS
'        Return CLASS_BASE.ENUM_STATUS.STATUS_ALREADY_EXISTS

'      Case Else
'        Return ENUM_STATUS.STATUS_ERROR

'    End Select

'  End Function

'  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

'    Dim rc As Integer

'    ' Delete prize_plan data
'    rc = DeletePrizePlan(Me.PrizePlanId, Me.GameId, SqlCtx)

'    Select Case rc
'      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
'        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

'      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
'        Return ENUM_STATUS.STATUS_NOT_FOUND

'      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DEPENDENCIES
'        Return ENUM_STATUS.STATUS_DEPENDENCIES

'      Case Else
'        Return ENUM_STATUS.STATUS_ERROR

'    End Select

'  End Function

'  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

'    Dim auditor_data As CLASS_AUDITOR_DATA
'    Dim category_item As New TYPE_PP_CATEGORY
'    Dim idx_category As Integer
'    Dim type_string As String
'    Dim category_string As String
'    Dim last_cat_id As Integer
'    Dim nls_conf As New CLASS_MODULE_NLS(mdl_NLS.ENUM_NLS_MODULES.COMMON_MODULE_GUI_CONF)


'    auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_PRIZE_PLANS)
'    Call auditor_data.SetName(nls_conf.Id(369), Me.Name)

'    'prize_plan Name
'    Call auditor_data.SetField(nls_conf.Id(254), IIf(Me.Name = "", AUDIT_NONE_STRING, Me.Name))

'    'prize_plan Desc
'    Call auditor_data.SetField(nls_conf.Id(347), IIf(Me.Description = "", AUDIT_NONE_STRING, Me.Description))

'    'jackpot pct
'    Call auditor_data.SetField(nls_conf.Id(360), GUI_FormatNumber(Me.JackpotPct, Me.MAX_DECIMAL_LEN))

'    'win jackpot
'    If Me.WinJackpot = Me.WIN_JACKPOT_YES Then
'      Call auditor_data.SetField(nls_conf.Id(356), nls_conf.GetString(264))
'    Else
'      Call auditor_data.SetField(nls_conf.Id(356), nls_conf.GetString(265))
'    End If

'    ' Prizes Expiration Days
'    Call auditor_data.SetField(nls_conf.Id(335), Me.ExpirationDays, "Caducidad de Premios (d�as)")

'    'pct deviation
'    'Call auditor_data.SetField(nls_conf.Id(361), Me.PctDeviation)

'    'Prizes pct
'    Call auditor_data.SetField(nls_conf.Id(368), GUI_FormatNumber(Me.PrizesPct, Me.MAX_DECIMAL_LEN))

'    'Num win cats
'    Call auditor_data.SetField(nls_conf.Id(370), Me.Categories.Count)

'    ' Categories: If Order is modified by an insert, delete or modified row, all next rows will be change because index change.
'    ' To prevent this effect, order by identifier before audit, and audit deleted identifiers as categories without value.
'    Me.SortCategories(ENUM_ORDERBY.ORDERBY_ID)

'    last_cat_id = 0
'    For idx_category = 0 To Categories.Count - 1

'      Call Me.Categories.GetItem(idx_category, category_item)

'      ' Audit deleted identifiers as categories without value
'      While category_item.cat_id > last_cat_id + 1

'        last_cat_id = last_cat_id + 1

'        'Category Type
'        category_string = nls_conf.GetString(371) & " " & (last_cat_id) & ": " & nls_conf.GetString(268)
'        Call auditor_data.SetField(0, AUDIT_NONE_STRING, category_string)

'        'Category amount
'        category_string = nls_conf.GetString(371) & " " & (last_cat_id) & ": " & nls_conf.GetString(362)
'        Call auditor_data.SetField(0, AUDIT_NONE_STRING, category_string)

'        'Category percentage of sales
'        category_string = nls_conf.GetString(371) & " " & (last_cat_id) & ": " & nls_conf.GetString(357)
'        Call auditor_data.SetField(0, AUDIT_NONE_STRING, category_string)

'        'Category percentage of TAX
'        category_string = nls_conf.GetString(371) & " " & (last_cat_id) & ": " & nls_conf.GetString(367)
'        Call auditor_data.SetField(0, AUDIT_NONE_STRING, category_string)

'        'Category percentage of deviation
'        category_string = nls_conf.GetString(371) & " " & (last_cat_id) & ": " & nls_conf.GetString(361)
'        Call auditor_data.SetField(0, AUDIT_NONE_STRING, category_string)

'      End While

'      Select Case category_item.type_win_cat

'        Case Me.ENUM_CATEGORY_TYPE.CATEGORY_TYPE_NON_REFOUNDABLE
'          type_string = nls_conf.GetString(363)

'        Case Me.ENUM_CATEGORY_TYPE.CATEGORY_TYPE_MINOR
'          type_string = nls_conf.GetString(364)

'        Case Me.ENUM_CATEGORY_TYPE.CATEGORY_TYPE_MAJOR
'          type_string = nls_conf.GetString(365)

'        Case Me.ENUM_CATEGORY_TYPE.CATEGORY_TYPE_SPECIAL
'          type_string = nls_conf.GetString(366)

'        Case Else
'          type_string = AUDIT_NONE_STRING

'      End Select

'      'Category Type
'      category_string = nls_conf.GetString(371) & " " & (category_item.cat_id) & ": " & nls_conf.GetString(268)
'      Call auditor_data.SetField(0, IIf(idx_category >= Categories.Count, AUDIT_NONE_STRING, type_string), category_string)

'      'Category amount
'      category_string = nls_conf.GetString(371) & " " & (category_item.cat_id) & ": " & nls_conf.GetString(375) ' 362)
'      Call auditor_data.SetField(0, IIf(idx_category >= Categories.Count, AUDIT_NONE_STRING, GUI_FormatNumber(category_item.amt_win_cat)), category_string)

'      'Category percentage of sales
'      category_string = nls_conf.GetString(371) & " " & (category_item.cat_id) & ": " & nls_conf.GetString(357)
'      Call auditor_data.SetField(0, IIf(idx_category >= Categories.Count, AUDIT_NONE_STRING, GUI_FormatNumber(category_item.pct_win_cat, Me.MAX_DECIMAL_LEN)), category_string)

'      'Category percentage of TAX
'      category_string = nls_conf.GetString(371) & " " & (category_item.cat_id) & ": " & nls_conf.GetString(367)
'      Call auditor_data.SetField(0, IIf(idx_category >= Categories.Count, AUDIT_NONE_STRING, GUI_FormatNumber(category_item.pct_tax_win_cat, Me.MAX_DECIMAL_LEN)), category_string)

'      'Category percentage of deviation
'      category_string = nls_conf.GetString(371) & " " & (category_item.cat_id) & ": " & nls_conf.GetString(361)
'      Call auditor_data.SetField(0, IIf(idx_category >= Categories.Count, AUDIT_NONE_STRING, GUI_FormatNumber(category_item.pct_deviation_cat, Me.MAX_DECIMAL_LEN)), category_string)

'      last_cat_id = category_item.cat_id

'    Next

'    Return auditor_data

'  End Function

'    Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

'        Dim temp_prize_plan As CLASS_PRIZE_PLAN
'        Dim category_item As New TYPE_PP_CATEGORY
'        Dim serie_status_item As New TYPE_PP_SERIES_STATUS
'        Dim idx_category As Integer
'        Dim idx_series_status As Integer

'        temp_prize_plan = New CLASS_PRIZE_PLAN
'        temp_prize_plan.m_prize_plan.prize_plan_id = Me.m_prize_plan.prize_plan_id
'        temp_prize_plan.m_prize_plan.game_id = Me.m_prize_plan.game_id
'        temp_prize_plan.m_prize_plan.name = Me.m_prize_plan.name
'        temp_prize_plan.m_prize_plan.filler_0 = Me.m_prize_plan.filler_0
'        temp_prize_plan.m_prize_plan.description = Me.m_prize_plan.description
'        temp_prize_plan.m_prize_plan.filler_1 = Me.m_prize_plan.filler_1
'        temp_prize_plan.m_prize_plan.prizes_pct = Me.m_prize_plan.prizes_pct
'        temp_prize_plan.m_prize_plan.jackpot_pct = Me.m_prize_plan.jackpot_pct
'        temp_prize_plan.m_prize_plan.win_jackpot = Me.m_prize_plan.win_jackpot
'        temp_prize_plan.m_prize_plan.min_cats = Me.m_prize_plan.min_cats
'        temp_prize_plan.m_prize_plan.max_cats = Me.m_prize_plan.max_cats
'        'temp_prize_plan.m_prize_plan.instances = Me.m_prize_plan.instances


'        ' AJQ, 19-DEC-2002
'        temp_prize_plan.m_prize_plan.expiration_days = Me.m_prize_plan.expiration_days

'        ' Duplicate class vector of category items
'        If Not IsNothing(m_prize_plan.categories) Then

'            For idx_category = 0 To Me.m_prize_plan.categories.Count - 1
'                Call Me.m_prize_plan.categories.GetItem(idx_category, category_item)
'                Call temp_prize_plan.m_prize_plan.categories.SetItem(idx_category, category_item)
'            Next
'        End If

'        ' Duplicate class vector of category items
'        If Not IsNothing(m_prize_plan.series_status) Then

'            For idx_series_status = 0 To Me.m_prize_plan.series_status.Count - 1
'                Call Me.m_prize_plan.series_status.GetItem(idx_series_status, serie_status_item)
'                Call temp_prize_plan.m_prize_plan.series_status.SetItem(idx_series_status, serie_status_item)
'            Next
'        End If

'        Return temp_prize_plan

'    End Function

'#End Region

'#Region " Public Functions "

'  ' PURPOSE: Put in order all categories by contents of field pct_win_cat in descending order
'  '          or by id in ascending order
'  '
'  '  PARAMS:
'  '     - INPUT:
'  '           - CategoryList: vector to sort
'  '     - OUTPUT:
'  '           - categoryList: vector sorted
'  '
'  ' RETURNS:
'  '     - none
'  Private Sub SortCategories(Optional ByVal OrderBy As ENUM_ORDERBY = ENUM_ORDERBY.ORDERBY_AMOUNT)

'    Dim idx As Integer
'    Dim idx_max As Integer

'    For idx = 0 To Categories.Count - 2
'      idx_max = SearchMaxCategory(idx, OrderBy)
'      If idx_max <> idx Then
'        SwapCategories(idx, idx_max)
'      End If
'    Next

'  End Sub

'#End Region

'#Region " Private Functions "

'  ' PURPOSE: search the category with the greater import starding form IdxStart
'  '          or with the lower id
'  '
'  '  PARAMS:
'  '     - INPUT:
'  '           - CategoryList
'  '           - IdxStart:  start search from this point
'  '     - OUTPUT:
'  '           - none
'  '
'  ' RETURNS:
'  '     - index of category found
'  '
'  Private Function SearchMaxCategory(ByVal IdxStart As Integer, Optional ByVal OrderBy As ENUM_ORDERBY = ENUM_ORDERBY.ORDERBY_AMOUNT) As Integer

'    Dim idx As Integer
'    Dim idx_max As Integer
'    Dim max_value As Double
'    Dim min_value As Double
'    Dim item As New TYPE_PP_CATEGORY()

'    idx_max = IdxStart
'    max_value = 0
'    min_value = MAX_PP_CATEGORIES

'    For idx = IdxStart To Categories.Count - 1
'      Call Categories.GetItem(idx, item)
'      Select Case OrderBy

'        Case ENUM_ORDERBY.ORDERBY_ID
'          If item.cat_id < min_value Then
'            min_value = item.cat_id
'            idx_max = idx
'          End If

'        Case ENUM_ORDERBY.ORDERBY_AMOUNT
'          If item.amt_win_cat > max_value Then
'            max_value = item.amt_win_cat
'            idx_max = idx
'          End If

'        Case Else

'      End Select
'    Next

'    Return idx_max

'  End Function

'  ' PURPOSE: exchange contents of items into array
'  '
'  '  PARAMS:
'  '     - INPUT:
'  '           - CategoryList
'  '           - IdxFirst:  index of item to swap
'  '           - IdxSecond: index of item to swap
'  '     - OUTPUT:
'  '           - CategoryList 
'  '
'  ' RETURNS:
'  '     - none
'  Private Sub SwapCategories(ByVal IdxFirst As Integer, ByVal IdxSecond As Integer)

'    Dim item_first As New TYPE_PP_CATEGORY()
'    Dim item_second As New TYPE_PP_CATEGORY()

'    Call Categories.GetItem(IdxFirst, item_first)
'    Call Categories.GetItem(IdxSecond, item_second)
'    Call Categories.SetItem(IdxFirst, item_second)
'    Call Categories.SetItem(IdxSecond, item_first)

'  End Sub

'#End Region


'End Class
