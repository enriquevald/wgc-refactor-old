'-------------------------------------------------------------------
' Copyright © 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_registry.vb  
' DESCRIPTION:   Class for manage the registry functions
' AUTHOR:        Luis Rodríguez
' CREATION DATE: 17-FEB-2004
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 17-FEB-2004  LRS    Initial version.
'--------------------------------------------------------------------

Option Explicit On 

Imports System.Runtime.InteropServices
Imports GUI_CommonMisc
'Imports GUI_Controls


'<StructLayout(LayoutKind.Sequential)> _
Public Class CLASS_REGISTRY_DATA
  Inherits CLASS_BASE

#Region " Constants"
  Public Const MAX_REGISTRY_KEY_NAME = 80
  Public Const MAX_REGISTRY_VALUE_NAME = 80
  Public Const MAX_REGISTRY_VALUE_DATA = 80
  Public Const MAX_REGISTRY_VALUES = 1000

  Public Const REG_TYPE_STRING = 1
  Public Const REG_TYPE_DWORD = 4
  Public Const REG_TYPE_BYNARY = 3

  Public Const REG_RETURN_CODE_OK = 0
  Public Const REG_RETURN_CODE_ERROR = 1
  Public Const REG_RETURN_CODE_NOT_FOUND = 2

#End Region

#Region " Structures"

  <StructLayout(LayoutKind.Sequential)> _
  Public Class CLASS_REGISTRY_DATA_ITEM
    Private key_type As Integer
    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_REGISTRY_KEY_NAME)> _
    Private key_name As String
    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_REGISTRY_VALUE_NAME)> _
    Private value_name As String
    Private value_type As Integer
    Private value_length As Integer
    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_REGISTRY_VALUE_DATA)> _
    Private value_data As String

#Region " Properties "

    Public Property KeyType() As Integer
      Get
        Return key_type
      End Get
      Set(ByVal Value As Integer)
        key_type = Value
      End Set
    End Property

    Public Property KeyName() As String
      Get
        Return key_name
      End Get
      Set(ByVal Value As String)
        key_name = Value
      End Set
    End Property

    Public Property ValueType() As Integer
      Get
        Return value_type
      End Get
      Set(ByVal Value As Integer)
        value_type = Value
      End Set
    End Property

    Public Property ValueName() As String
      Get
        Return value_name
      End Get
      Set(ByVal Value As String)
        value_name = Value
      End Set
    End Property

    Public Property ValueData() As String
      Get
        Return value_data
      End Get
      Set(ByVal Value As String)
        value_data = Value
      End Set
    End Property

    Public Property ValueLength() As Integer
      Get
        Return value_length
      End Get
      Set(ByVal Value As Integer)
        value_length = Value
      End Set
    End Property

#End Region

  End Class

#End Region

#Region "Prototypes "
  Public Declare Function Common_GetRegistryData Lib "CommonBase" (ByVal KeyRoot As String, _
                                                                   ByVal KeyType As Integer, _
                                                                   ByVal KeyName As String, _
                                                                   ByVal RegistryDataList As CLASS_VECTOR) As Integer

  Public Declare Function Common_SetRegistryData Lib "CommonBase" (ByVal KeyRoot As String, _
                                                                   ByVal RegistryDataList As CLASS_VECTOR) As Integer

  Public Declare Function Common_GetRegistryValue Lib "CommonBase" (ByVal KeyRoot As String, _
                                                                    <[In](), [Out]()> _
                                                                    ByVal RegistryDataItem As CLASS_REGISTRY_DATA_ITEM) As Integer
  Public Declare Function Common_GetValue Lib "CommonBase" (ByVal ValueName As String, _
                                                            ByVal RegistryDataList As CLASS_VECTOR, _
                                                            <[In](), [Out]()> _
                                                            ByVal RegistryDataItem As CLASS_REGISTRY_DATA_ITEM) As Integer
#End Region

#Region " Members "

  Protected m_registry_data As CLASS_VECTOR

#End Region

  Public Sub New()
    MyBase.New()
    m_registry_data = New CLASS_VECTOR(GetType(CLASS_REGISTRY_DATA_ITEM), MAX_REGISTRY_VALUES)
  End Sub

  Public Sub DoneVector()
    m_registry_data.Done()
  End Sub

  Public Property RegistryDataList() As CLASS_VECTOR
    Get
      Return m_registry_data
    End Get
    Set(ByVal Value As CLASS_VECTOR)
      m_registry_data = Value
    End Set
  End Property

#Region "Overrides"
  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
  End Function

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
  End Function

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
  End Function

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
  End Function

  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE
    Return Nothing
  End Function

  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA
    Return Nothing
  End Function
#End Region

End Class


