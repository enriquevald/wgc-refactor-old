'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_vector.vb
' DESCRIPTION:   Class to pass vectors between C & VB.
' AUTHOR:        Andreu Juli�
' CREATION DATE: 12-AUG-2002
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 12-AUG-2002  AJQ    Initial version
'-------------------------------------------------------------------
Imports System.Runtime.InteropServices

<StructLayout(LayoutKind.Sequential)> _
Public Class CLASS_VECTOR
  ' C:
  ' typedef struct
  ' {
  '   DWORD control_block;
  '   DWORD control_block_item;
  '   DWORD max_items;
  '   DWORD num_items;
  '   VOID  * p_items;
  '
  ' } TYPE_COMMON_VECTOR;
  '
  ' Constants
  '
  Private Const DEFAULT_MAX_NUM_ITEMS As Integer = 10 ' Default maximum number of elements
  '
  ' Fields:
  '
  Private control_block As Integer      ' Size of the structure
  Private control_block_item As Integer ' Size of an item
  Private max_items As Integer          ' Maximum number of elements
  Private num_items As Integer          ' [In/Out] Actual number of elements
  Private p_items As IntPtr             ' Pointer to the elements

  Public Sub GetItem(ByVal Index As Integer, ByVal Obj As Object)
    Dim ptr As IntPtr

    If Index < 0 Or Index >= num_items Then
      Exit Sub
    End If

    If Marshal.SizeOf(Obj) <> control_block_item Then
      Exit Sub
    End If

    ' Compute address
    ptr = New IntPtr(p_items.ToInt32 + Marshal.SizeOf(Obj.GetType) * Index)
    ' Copy data
    Call Marshal.PtrToStructure(ptr, Obj)

  End Sub

  Public Sub Clear()
    num_items = 0
  End Sub

  Public Property Count() As Integer
    Get
      Return num_items
    End Get
    Set(ByVal Value As Integer)
      num_items = Value
    End Set
  End Property

  Public ReadOnly Property MaxItems() As Integer
    Get
      Return max_items
    End Get
  End Property

  Public Sub SetItem(ByVal Index As Integer, ByVal Obj As Object)
    Dim ptr As IntPtr

    If Index < 0 Or Index >= max_items Then
      Exit Sub
    End If

    If Marshal.SizeOf(Obj) <> control_block_item Then
      Exit Sub
    End If

    If Index >= num_items Then
      num_items = Index + 1
    End If

    ' Compute address
    ptr = New IntPtr(p_items.ToInt32 + Marshal.SizeOf(Obj.GetType) * Index)
    ' Copy data
    Call Marshal.StructureToPtr(Obj, ptr, True)
  End Sub

  Public Sub Init(ByVal ItemType As System.Type, Optional ByVal MaxItems As Integer = DEFAULT_MAX_NUM_ITEMS)
    Call Done()
    control_block = Marshal.SizeOf(GetType(CLASS_VECTOR))
    control_block_item = Marshal.SizeOf(ItemType)
    max_items = MaxItems
    num_items = 0

    'XVV 17/04/2007
    'Change p_items.Zero for InPtr.Zero
    p_items = IntPtr.Zero
    p_items = Marshal.AllocCoTaskMem(Marshal.SizeOf(ItemType) * (MaxItems))  '+1))
  End Sub

  Public Sub Done()

    '----------------------------------------------
    'XVV 17/04/2007
    'Change p_items.Zero for InPtr.Zero
    '----------------------------------------------
    If p_items.Equals(IntPtr.Zero) Then
      Exit Sub
    End If
    Call Marshal.FreeCoTaskMem(p_items)
    p_items = IntPtr.Zero
    '----------------------------------------------

  End Sub

  Public Sub New(ByVal ItemType As System.Type, Optional ByVal MaxItems As Integer = DEFAULT_MAX_NUM_ITEMS)
    Call Init(ItemType, MaxItems)
  End Sub

  Public Sub New()

    'XVV 17/04/2007
    'Change p_items.Zero for InPtr.Zero
    p_items = IntPtr.Zero

  End Sub

End Class
