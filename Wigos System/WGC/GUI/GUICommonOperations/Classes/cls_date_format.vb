'-------------------------------------------------------------------
' Copyright � 2003 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_date_format.vb
' DESCRIPTION:   Date formats
' AUTHOR:        ---
' CREATION DATE: xx-xxx-xxxx
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' xx-xxx-xxxx  ---    Initial version
' 21-MAY-2012  RCI    Fixed Bug #300: GUI_GetDateTime must use WGDB.Now (DB time), not just Now (local PC time).
' 20-FEB-2013  RXM    Added FORMAT_DATE_YEAR_MONTH selection mode.
' 19-JUN-2013  JAB    Fixed bug #857: Elimited ClearCachedData function calls.
'-------------------------------------------------------------------
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Globalization
Imports System.Math

Imports GUI_CommonMisc

<StructLayout(LayoutKind.Sequential)> _
Public Class TYPE_DATE_TIME
  Private m_year As Integer
  Private m_month As Integer
  Private m_day As Integer
  Private m_hour As Integer
  Private m_minute As Integer
  Private m_second As Integer
  Private m_is_null As Integer

  Public Sub New()
    ' AJQ, 27-NOV-2002 On creation all field are zero, so the date is NULL
    m_is_null = 1
  End Sub

  Public Property Value() As Date
    Get
      ' The user should check if the date is null (Property: IsNull)
      Return New System.DateTime(m_year, m_month, m_day, m_hour, m_minute, m_second)
    End Get
    Set(ByVal Value As Date)
      m_year = Value.Year
      m_month = Value.Month
      m_day = Value.Day
      m_hour = Value.Hour
      m_minute = Value.Minute
      m_second = Value.Second
      m_is_null = 0
    End Set
  End Property

  Public Property IsNull() As Boolean
    Get
      Return (m_is_null <> 0)
    End Get
    Set(ByVal Value As Boolean)
      If Value Then
        m_year = 0
        m_month = 0
        m_day = 0
        m_hour = 0
        m_minute = 0
        m_second = 0
        m_is_null = 1
      Else
        ' Non sense do nothing ...
        ' m_is_null = 0
      End If
    End Set
  End Property

End Class

<StructLayout(LayoutKind.Sequential)> _
Public Class TYPE_SYSTEMTIME
  Public year As Short
  Public month As Short
  Public day_of_week As Short
  Public day As Short
  Public hour As Short
  Public minute As Short
  Public second As Short
  Public milliseconds As Short
End Class

<StructLayout(LayoutKind.Sequential)> _
Public Class TYPE_GET_DATE_TIME
  Private Const MAX_SIZE_DATE As Integer = 100
  Private Const SIZE_TIME As Integer = 11

  Public control_block As Integer
  Public utc_date_time As New TYPE_SYSTEMTIME()
  Public current_date_time As New TYPE_SYSTEMTIME()
  <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_SIZE_DATE + 1 + 3)> _
  Public current_date_string As String
  <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=SIZE_TIME + 1 + 4)> _
  Public current_time_string As String

  ' Init structure
  Public Sub New()
    control_block = Marshal.SizeOf(Me)

  End Sub
End Class


Public Module ModuleDateTimeFormats

#Region " PROTOTYPES "

  Declare Function Common_GetDateTime Lib "CommonBase" (<[In](), [Out]()> ByVal pDateTime As TYPE_GET_DATE_TIME) As Integer

#End Region   ' Prototypes

  Public Enum ENUM_FORMAT_DATE
    FORMAT_DATE_NONE = 0
    FORMAT_DATE_SHORT = 1  ' System Short Date (Year,Month,Day)
    FORMAT_DATE_LONG = 2   ' System Long Date (Year,Month,Day and text)

    FORMAT_DATE_WEEK_WW = 10  ' WW
    FORMAT_DATE_WEEK_WWYEAR = 11  ' WW/YYYY 
    FORMAT_DATE_WEEK_WWDATE = 12  ' WW, DD/MM/YYYY
    FORMAT_DATE_MMYEAR = 13  ' MM/YYYY

    ' 20-FEB-2013  RXM 
    FORMAT_DATE_YEAR_MONTH = 14 ' Oct YYYY

    ' 02-JAN-2014  RMS
    FORMAT_DATE_YEAR = 15 ' YYYY
  End Enum

  Public Enum ENUM_FORMAT_TIME
    FORMAT_TIME_NONE = 0
    FORMAT_TIME_SHORT = 1  ' System Short Time (Hour,Minute)
    FORMAT_TIME_LONG = 2   ' System Long Time (Hour,Minute,Second)

    FORMAT_MINUTES = 3 ' MM
    FORMAT_HHMM = 4 ' HH:MM
    FORMAT_HHMMSS = 5 ' HH:MM:SS
    FORMAT_HOURS = 6 ' HH


    MMSS = 10       ' (Minute, Second)
    MMSSFF = 11     ' (Minute, Sencond, HundredSeconds)

  End Enum

  Public Function GUI_PatternTime(ByVal TimeFormat As ENUM_FORMAT_TIME) As String
    Dim time_format As String
    Dim culture_info As CultureInfo
    Dim dt_format_info As DateTimeFormatInfo

    culture_info = System.Threading.Thread.CurrentThread.CurrentCulture
    dt_format_info = culture_info.DateTimeFormat

    Select Case TimeFormat
      Case ENUM_FORMAT_TIME.FORMAT_TIME_LONG
        time_format = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.LongTimePattern

      Case ENUM_FORMAT_TIME.FORMAT_TIME_NONE
        time_format = ""

      Case ENUM_FORMAT_TIME.FORMAT_TIME_SHORT
        'time_format = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortTimePattern
        time_format = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.LongTimePattern
        time_format = Replace(time_format, ":ss", "")

      Case ENUM_FORMAT_TIME.FORMAT_HHMMSS
        time_format = "HH" & dt_format_info.TimeSeparator & _
                      "mm" & dt_format_info.TimeSeparator & _
                      "ss"
      Case ENUM_FORMAT_TIME.FORMAT_HHMM
        time_format = "HH" & dt_format_info.TimeSeparator & _
                      "mm"

      Case ENUM_FORMAT_TIME.MMSS
        time_format = "mm" & dt_format_info.TimeSeparator & _
                      "ss"

      Case ENUM_FORMAT_TIME.MMSSFF
        time_format = "mm" & dt_format_info.TimeSeparator & _
                      "ss" & dt_format_info.TimeSeparator & _
                      "ff"
      Case ENUM_FORMAT_TIME.FORMAT_MINUTES
        time_format = "mm"

      Case ENUM_FORMAT_TIME.FORMAT_HOURS
        ' RCI 29-DEC-2010: Apended constant string ":00" to hour
        time_format = "HH" & dt_format_info.TimeSeparator & _
                      "00"

      Case Else
        time_format = ""

    End Select

    Return time_format

  End Function

  Public Function GUI_PatternDate(ByVal DateFormat As ENUM_FORMAT_DATE, _
                                  Optional ByVal TimeFormat As ENUM_FORMAT_TIME = ENUM_FORMAT_TIME.FORMAT_TIME_NONE) As String
    Dim date_format As String
    'XVV Variable not use Dim time_format As String
    Dim full_format As String
    'XVV Variable not use im pos As Integer

    Dim culture_info As CultureInfo
    Dim dt_format_info As DateTimeFormatInfo

    culture_info = System.Threading.Thread.CurrentThread.CurrentCulture
    dt_format_info = culture_info.DateTimeFormat

    Select Case DateFormat
      Case ENUM_FORMAT_DATE.FORMAT_DATE_LONG
        date_format = dt_format_info.LongDatePattern

      Case ENUM_FORMAT_DATE.FORMAT_DATE_NONE
        date_format = ""

      Case ENUM_FORMAT_DATE.FORMAT_DATE_SHORT
        date_format = dt_format_info.ShortDatePattern

      Case ENUM_FORMAT_DATE.FORMAT_DATE_YEAR_MONTH
        date_format = dt_format_info.YearMonthPattern

      Case Else
        date_format = ""

    End Select

    If TimeFormat <> ENUM_FORMAT_TIME.FORMAT_TIME_NONE Then
      full_format = Trim(date_format & " " & GUI_PatternTime(TimeFormat))

      Return full_format
    End If

    Return date_format

  End Function

  Public Function GUI_FormatDayDB(ByVal Value As Date) As String
    Const FORMAT_DB_DATE_TIME As String = "yyyy/MM/dd"
    Dim str_date As String

    Dim _dtfi As DateTimeFormatInfo = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.Clone

    _dtfi.TimeSeparator = ":"

    str_date = Value.ToString(FORMAT_DB_DATE_TIME, _dtfi)

    str_date = "CAST('" & str_date & "' AS DATETIME)"

    Return str_date

  End Function


  Public Function GUI_FormatDateDB(ByVal Value As Date) As String
    'XVV Variable not use Const FORMAT_DB_DATE_TIME_SQL As String = "yyyy/mm/dd hh24:mi:ss"
    Const FORMAT_DB_DATE_TIME As String = "yyyy/MM/dd HH:mm:ss"
    Dim str_date As String

    Dim _dtfi As DateTimeFormatInfo = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.Clone

    _dtfi.TimeSeparator = ":"

    str_date = Value.ToString(FORMAT_DB_DATE_TIME, _dtfi)

    ' str_date = Format(Value, FORMAT_DB_DATE_TIME)
    str_date = "CAST('" & str_date & "' AS DATETIME)"

    Return str_date

  End Function

  Public Function DB_FormatDate(ByVal Value As Date) As String
    'XVV Variable not use Const FORMAT_DB_DATE_TIME_SQL As String = "yyyy/mm/dd hh24:mi:ss"
    Const FORMAT_DB_DATE_TIME As String = "yyyy/MM/dd HH:mm:ss"
    Dim str_date As String

    str_date = Format(Value, FORMAT_DB_DATE_TIME)

    Return str_date

  End Function

  Public Function DB_DateFormat() As String
    Const FORMAT_DB_DATE_TIME_SQL As String = "yyyy/mm/dd hh24:mi:ss"
    'XVV Variable not use Const FORMAT_DB_DATE_TIME As String = "yyyy/MM/dd HH:mm:ss"
    Dim str_date As String

    str_date = FORMAT_DB_DATE_TIME_SQL

    Return str_date

  End Function

  Public Function DB_DateAndFormat(ByVal Value As Date) As String
    Const FORMAT_DB_DATE_TIME_SQL As String = "yyyy/mm/dd hh24:mi:ss"
    Const FORMAT_DB_DATE_TIME As String = "yyyy/MM/dd HH:mm:ss"
    Dim str_date As String

    str_date = Format(Value, FORMAT_DB_DATE_TIME)
    str_date = "'" & str_date & "','" & FORMAT_DB_DATE_TIME_SQL & "'"

    str_date = str_date

    Return str_date

  End Function

  Public Function GUI_FormatWeekDB(ByVal Value As Date) As String
    Const FORMAT_DB_DATE_TIME_SQL As String = "yyyy/mm/dd hh24:mi:ss"
    Const FORMAT_DB_DATE_TIME As String = "yyyy/MM/dd HH:mm:ss"
    Dim str_date As String

    str_date = Format(Value, FORMAT_DB_DATE_TIME)
    str_date = "lk$dates.date_to_week_number('" & str_date & "', '" & FORMAT_DB_DATE_TIME_SQL & "',1)"

    Return str_date

  End Function

  Public Function GUI_FormatWeekDBField(ByVal Field As String) As String
    Const FORMAT_DB_DATE_TIME_SQL As String = "yyyy/mm/dd hh24:mi:ss"
    'XVV Variable not use Const FORMAT_DB_DATE_TIME As String = "yyyy/MM/dd HH:mm:ss"
    Dim str_date As String

    'str_date = Format(Value, FORMAT_DB_DATE_TIME)
    str_date = "lk$dates.date_to_week_number(TO_CHAR(" & Field & ",'" & FORMAT_DB_DATE_TIME_SQL & "' )" & _
                                            ", '" & FORMAT_DB_DATE_TIME_SQL & "'" & _
                                            ",1)"

    Return str_date

  End Function

  Public Function GUI_FormatDate(ByVal Value As Date, _
                                 Optional ByVal DateFormat As ENUM_FORMAT_DATE = ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                 Optional ByVal TimeFormat As ENUM_FORMAT_TIME = ENUM_FORMAT_TIME.FORMAT_TIME_NONE) As String

    Select Case DateFormat
      Case ENUM_FORMAT_DATE.FORMAT_DATE_WEEK_WW, _
           ENUM_FORMAT_DATE.FORMAT_DATE_WEEK_WWDATE, _
           ENUM_FORMAT_DATE.FORMAT_DATE_WEEK_WWYEAR

        Return GUI_FormatWeek(Value, DateFormat)

      Case ENUM_FORMAT_DATE.FORMAT_DATE_MMYEAR
        Return Format(Value.Month, "00") & "/" & Format(Value.Year, "00")

      Case ENUM_FORMAT_DATE.FORMAT_DATE_YEAR
        Return Format(Value.Year, "0000")

      Case Else
        Return Format(Value, GUI_PatternDate(DateFormat, TimeFormat))

    End Select

  End Function

  Public Function GUI_FormatTime(ByVal Value As Date, _
                                 Optional ByVal TimeFormat As ENUM_FORMAT_TIME = ENUM_FORMAT_TIME.FORMAT_TIME_SHORT) As String

    Return Format(Value, GUI_PatternTime(TimeFormat))

  End Function


  Public Function GUI_ParseDateTime(ByVal StringValue As String, ByVal DateFormat As ENUM_FORMAT_DATE, ByVal TimeFormat As ENUM_FORMAT_TIME) As Date
    Dim pattern As String

    pattern = GUI_PatternDate(DateFormat, TimeFormat)

    Return Date.ParseExact(StringValue, pattern, Nothing)

  End Function

  Public Function GUI_FormatTimeZone(ByVal Minutes As Integer) As String
    Dim utc_format As String
    Dim utc_minutes As Integer
    Dim utc_hours As Integer

    utc_hours = Abs(Minutes) \ 60
    utc_minutes = Abs(Minutes) Mod 60

    utc_format = Format(TimeSerial(utc_hours, utc_minutes, 0), "HH") _
                & ":" _
                & Format(TimeSerial(utc_hours, utc_minutes, 0), "mm")

    If Minutes < 0 Then

      utc_format = "-" & utc_format
    Else
      utc_format = "+" & utc_format
    End If

    Return utc_format
  End Function

  Public Function GUI_ParseTimeZone(ByVal TimeZoneString As String) As Integer
    Dim utc_hour As Integer
    Dim utc_minute As Integer


    If GUI_IsValidTimeZone(TimeZoneString) = False Then
      'If not valid put value out of range
      Return -13 * 60
    End If

    utc_hour = Left(TimeZoneString, 3)
    utc_minute = Right(TimeZoneString, 2)

    If InStr(TimeZoneString, "-"c) Then
      Return utc_hour * 60 - utc_minute
    Else
      Return utc_hour * 60 + utc_minute
    End If

  End Function

  Public Function GUI_IsValidTimeZone(ByVal TimeZoneString As String) As Boolean
    Dim utc_signed As Char
    Dim utc_minutes As Integer
    Dim utc_hours As Integer
    Dim utc_date As Date


    If TimeZoneString.Length <> 6 Then
      Return False
    End If

    If InStr(1, TimeZoneString, ":") <> 4 Then
      Return False
    End If

    utc_signed = Left(TimeZoneString, 1)
    If utc_signed <> "-" _
       And utc_signed <> "+" Then
      Return False
    End If

    utc_hours = Left(Right(TimeZoneString, 5), 2)
    utc_minutes = Right(TimeZoneString, 2)
    utc_date = TimeSerial(utc_hours, utc_minutes, 0)

    If utc_minutes > 59 Then
      Return False
    End If

    If (utc_date > TimeSerial(13, 0, 0) And utc_signed = "+") _
      Or (utc_date > TimeSerial(12, 0, 0) And utc_signed = "-") Then
      Return False
    End If

    Return True

  End Function

  Public Function WeekOfYear(ByVal DateValue As Date) As Integer
    Return DatePart(DateInterval.WeekOfYear, _
                    DateValue, _
                    FirstDayOfWeek.Monday, _
                    FirstWeekOfYear.Jan1)
  End Function


  Public Function FirstWeekDay(ByVal DateValue As Date) As Date
    Dim first_week_day As Date

    ' Current Date
    first_week_day = DateValue

    ' Go back till the first 'Monday'
    While first_week_day.DayOfWeek <> DayOfWeek.Monday
      first_week_day = first_week_day.AddDays(-1)
    End While

    Return first_week_day

  End Function

  Public Function LastWeekDay(ByVal DateValue As Date) As Date
    Dim first_week_day As Date

    ' Next date
    first_week_day = DateValue.AddDays(1)

    ' Go forward till the first 'Monday'
    While first_week_day.DayOfWeek <> DayOfWeek.Monday
      first_week_day = first_week_day.AddDays(+1)
    End While

    Return first_week_day.AddDays(-1)

  End Function

  ' PURPOSE: Get First day of month.
  '
  '  PARAMS:
  '     - INPUT:
  '           - DateValue: Date to Check
  '
  '     - OUTPUT:
  '           - Date with first day of month
  '
  ' RETURNS:
  '     - Date time
  Public Function FirstMonthDay(ByVal DateValue As Date) As Date
    Dim first_month_day As Date

    first_month_day = New Date(DateValue.Year, _
                                DateValue.Month, _
                                1, _
                                DateValue.Hour, _
                                DateValue.Minute, _
                                DateValue.Second, _
                                DateValue.Millisecond)
    
    Return first_month_day

  End Function

  Public Function GUI_FormatWeek(ByVal DateValue As Date, ByVal DateFormat As ENUM_FORMAT_DATE) As String
    Dim first_day As Date
    Dim last_day As Date
    Dim first_week As Integer
    Dim last_week As Integer
    Dim week_str As String


    first_day = FirstWeekDay(DateValue)
    last_day = first_day.AddDays(6)

    first_week = WeekOfYear(first_day)
    last_week = WeekOfYear(last_day)

    If first_week <> last_week Then
      week_str = Format(first_week, "00") & "-" & Format(last_week, "00")
    Else
      week_str = Format(first_week, "00")
    End If

    Select Case DateFormat
      Case ENUM_FORMAT_DATE.FORMAT_DATE_WEEK_WW
        ' Do nothing

      Case ENUM_FORMAT_DATE.FORMAT_DATE_WEEK_WWYEAR
        week_str = week_str & " (" & Format(last_day.Year, "0000") & ")"

      Case ENUM_FORMAT_DATE.FORMAT_DATE_WEEK_WWDATE
        week_str = week_str & " (" & GUI_FormatDate(first_day, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT) & ")"

      Case Else
        week_str = ""

    End Select

    Return week_str

  End Function

  ' PURPOSE: Get date time
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - Date time
  Public Function GUI_GetDateTime() As Date


    'XVV --> Temporal TODUE

    'Dim date_time As New TYPE_GET_DATE_TIME
    'Dim date_time_vb As Date

    'Common_GetDateTime(date_time)

    'date_time_vb = New DateTime(date_time.current_date_time.year, _
    '                            date_time.current_date_time.month, _
    '                            date_time.current_date_time.day, _
    '                            date_time.current_date_time.hour, _
    '                            date_time.current_date_time.minute, _
    '                            date_time.current_date_time.second, _
    '                            date_time.current_date_time.milliseconds)
    'Return date_time_vb

    Return WSI.Common.WGDB.Now

  End Function

  ' PURPOSE: Get date 
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - Date 
  Public Function GUI_GetDate() As Date
    Dim date_time As Date
    Dim aux_date As Date

    date_time = GUI_GetDateTime()
    aux_date = New DateTime(date_time.Year, _
                            date_time.Month, _
                            date_time.Day, _
                            0, _
                            0, _
                            0, _
                            0)
    Return aux_date

  End Function

  
End Module
