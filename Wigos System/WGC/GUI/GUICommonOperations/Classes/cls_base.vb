'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_base.vb
' DESCRIPTION:   Definition of the base class. All the classes that
'                are used by the edition forms MUST derive from this one.
' AUTHOR:        Andreu Juli�
' CREATION DATE: 25-03-2002
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------  ------ -----------------------------------------------
' 25-03-2002  AJQ    Initial version
' 07-MAR-2016 ETP    Fixed Bug 10224: Validate if stock has changed.
'-------------------------------------------------------------------

Public MustInherit Class CLASS_BASE

  Public Enum ENUM_STATUS
    STATUS_OK = 0             ' Success
    STATUS_ERROR = 1          ' Error
    STATUS_DUPLICATE_KEY = 2  ' Duplicate key
    STATUS_ALREADY_EXISTS = 3 ' The file/register already exists
    STATUS_NOT_FOUND = 4      ' The file/register is not 
    STATUS_PASSWORD = 5       ' Password not valid (insert/update) or recently used (update)
    STATUS_DEPENDENCIES = 6   ' Dependencies
    STATUS_NOT_SUPPORTED = 7
    STATUS_STOCK_ERROR = 8    ' Stock error when ok button is pressed.
  End Enum

  '----------------------------------------------------------------------------
  ' PURPOSE: Reads from the database into the given object
  '
  ' PARAMS:
  '   - INPUT:
  '     - ObjectId: An object, it must be 'casted' to the desired KEY.
  '
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:

  Public MustOverride Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As ENUM_STATUS

  '----------------------------------------------------------------------------
  ' PURPOSE: Inserts the given object to the database.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:

  Public MustOverride Function DB_Insert(ByRef SqlCtx As Integer) As ENUM_STATUS

  '----------------------------------------------------------------------------
  ' PURPOSE: Updates the given object to the database.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:

  Public MustOverride Function DB_Update(ByRef SqlCtx As Integer) As ENUM_STATUS

  '----------------------------------------------------------------------------
  ' PURPOSE: Deletes the given object from the database.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:

  Public MustOverride Function DB_Delete(ByRef SqlCtx As Integer) As ENUM_STATUS

  '----------------------------------------------------------------------------
  ' PURPOSE: Returns a duplicate of the given object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:

  Public MustOverride Function Duplicate() As CLASS_BASE

  '----------------------------------------------------------------------------
  ' PURPOSE: Returns the related auditor data for the current object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:

  Public MustOverride Function AuditorData() As CLASS_AUDITOR_DATA

End Class

