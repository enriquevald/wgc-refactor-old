'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_gui.vb
' DESCRIPTION:   
' AUTHOR:        xxx
' CREATION DATE: xx-xxx-xxxx
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' xx-xxx-xxxx  xxx    Initial version
' 05-SEP-2002  JSV    Implement Check() function
' 25-JAN-2012  RCI    Added the funcionality of setting default permissions for forms
' 29-JUN-2012  RCI    Changed MAX_FORMS_BY_GUI from 100 to 200.
' 25-OCT-2013  LJM    Change Request WIG-303: Added the funcionality "Update", now we can change the order and the nls of GUI_Forms 
' 21-NOV-2013  ACM    Fixed Bug: WIG-423. Error updating new version.
' 26-NOV-2013  MMG    - Replaced the name of "LatestVersionAlreadyInstalled" function by "IsLatestVersion".
'                     - Modified functionality of "IsLatestVersion" function. Now it obtains the app version and returns 
'                       true if it is equal than latest version (false if they aren't equals).
'                     - Modified "Check" function. Now it only checks for new functionalities if the app version is equal than
'                       the latest version.
' 11-APR-2014  RCI & DRV    Changed MAX_FORMS_BY_GUI from 200 to 1000.
' 22-AUG-2014  AMF    Fixed Bug WIG-1197: Feature of form
' 20-OCT-2014  AMF    Fixed Bug WIG-1539: Features gaming tables 
' 18-MAY-2015  JML    Multisite-Multicurrency permisions
' 29-OCT-2015  ETP    Product Backlog Item: 4695 Gaming-Hall permissions.
' 21-JUN-2016  ESE    Product Backlog Item 13581:Generic reports: Add Report Tool to GUI
' 15-SEP-2016  ETP    Fixed bugs 17585 - 17607: Generic reports: add permissions config.
'-------------------------------------------------------------------

Option Explicit On
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports WSI.Common
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports System.Text


Public Class CLASS_GUI_FORMS

  Public Const MAX_FORMS_BY_GUI As Integer = 1000

  Public Enum ENUM_CHECK
    CHECK_OK = 0
    CHECK_ERROR = 1
    CHECK_ERROR_DB = 2
  End Enum

  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  '' RCI & AJQ 25-JAN-2012: This Type "overrides" the TYPE_PERMISSIONS in GUIControls - cls_host_user.vb.
  ''                        Take it in account in case of modifcation in any of the two files.
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  Public Structure TYPE_PERMISSIONS

    Public Const BIT_READ As Integer = 1
    Public Const BIT_WRITE As Integer = 2
    Public Const BIT_DELETE As Integer = 4
    Public Const BIT_EXECUTE As Integer = 8

    Public Const NONE As Integer = 0
    Public Const INHERIT As Integer = -1

    Dim Read As Boolean
    Dim Write As Boolean
    Dim Delete As Boolean
    Dim Execute As Boolean

    ' PURPOSE: Create a TYPE_PERMISSIONS Type from an integer representation.
    '
    '  PARAMS:
    '     - INPUT:
    '           - Permission As Integer
    '     - OUTPUT:
    '           - none
    '
    ' RETURNS: 
    '     - TYPE_PERMISSIONS
    '
    Public Shared Function Create(ByVal Permission As Integer) As TYPE_PERMISSIONS
      Dim _perm As TYPE_PERMISSIONS

      _perm = New TYPE_PERMISSIONS()
      _perm.IntegerValue = Permission

      Return _perm
    End Function ' Create

    ' PURPOSE: Create a TYPE_PERMISSIONS Type from all four permission needed (Read, Write, Delete and Execute).
    '
    '  PARAMS:
    '     - INPUT:
    '           - ReadPerm As Boolean
    '           - WritePerm As Boolean
    '           - DeletePerm As Boolean
    '           - ExecutePerm As Boolean
    '     - OUTPUT:
    '           - none
    '
    ' RETURNS:
    '     - TYPE_PERMISSIONS
    '
    Public Shared Function Create(ByVal ReadPerm As Boolean, _
                                  ByVal WritePerm As Boolean, _
                                  ByVal DeletePerm As Boolean, _
                                  ByVal ExecutePerm As Boolean) As TYPE_PERMISSIONS
      Dim _perm As TYPE_PERMISSIONS

      _perm = New TYPE_PERMISSIONS()
      _perm.Read = ReadPerm
      _perm.Write = WritePerm
      _perm.Delete = DeletePerm
      _perm.Execute = ExecutePerm

      Return _perm
    End Function ' Create

    ' PURPOSE: Property that permits to get or set the integer representation of a TYPE_PERMISSIONS Type.
    '
    '  PARAMS:
    '     - INPUT:
    '           - none
    '     - OUTPUT:
    '           - none
    '
    ' RETURNS:
    '     - Integer
    '
    Public Property IntegerValue() As Integer
      Get
        Dim _value As Integer

        _value = 0
        _value += IIf(Read, BIT_READ, 0)
        _value += IIf(Write, BIT_WRITE, 0)
        _value += IIf(Delete, BIT_DELETE, 0)
        _value += IIf(Execute, BIT_EXECUTE, 0)

        Return _value
      End Get

      Set(ByVal value As Integer)
        Read = ((BIT_READ And value) = BIT_READ)
        Write = ((BIT_WRITE And value) = BIT_WRITE)
        Delete = ((BIT_DELETE And value) = BIT_DELETE)
        Execute = ((BIT_EXECUTE And value) = BIT_EXECUTE)
      End Set
    End Property ' IntegerValue

  End Structure
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  '' END of "overriden" Type.
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

  Public Class CLASS_GUI_FORM

    Private m_id As Integer            ' FormId
    Private m_nls_id As Integer        ' NlsId
    Private m_nls As String            ' Nls
    Private m_order As Integer         ' Order
    Private m_feature As ENUM_FEATURES ' Feature

    ' RCI 25-JAN-2012: Properties for Default Permissions
    Private m_dflt_permission_has_default As Boolean         ' HasDefault
    Private m_dflt_permission_inherited_form_id As Integer   ' InheritedFormId
    Private m_dflt_permission_fixed As Integer               ' FixedPermission
    Private m_dflt_permission_proposed As Integer            ' ProposedPermission

    Public Sub New(ByVal Id As Integer, _
                   ByVal NlsId As Integer, _
                   ByVal Order As Integer, _
                   ByVal Feature As ENUM_FEATURES, _
                   Optional ByVal Nls As String = vbNullString)
      Me.m_id = Id
      Me.m_nls_id = NlsId
      Me.m_nls = Nls
      Me.m_order = Order
      Me.m_feature = Feature

      ' m_has_default indicates if the Default Permission is enabled or not.
      Me.m_dflt_permission_has_default = False
      Me.m_dflt_permission_inherited_form_id = 0
      Me.m_dflt_permission_fixed = 0
      Me.m_dflt_permission_proposed = -1
    End Sub

    Public ReadOnly Property Id() As Integer
      Get
        Return m_id
      End Get
    End Property

    Public ReadOnly Property NlsId() As Integer
      Get
        Return m_nls_id
      End Get
    End Property

    Public ReadOnly Property Nls() As String
      Get
        Return m_nls
      End Get
    End Property

    Public ReadOnly Property Order() As Integer
      Get
        Return m_order
      End Get
    End Property

    Public ReadOnly Property Feature() As ENUM_FEATURES
      Get
        Return m_feature
      End Get
    End Property

    Public Property DefltPermissionHasDefault() As Boolean
      Get
        Return m_dflt_permission_has_default
      End Get
      Set(ByVal value As Boolean)
        m_dflt_permission_has_default = value
      End Set
    End Property

    Public Property DefltPermissionInheritedFormId() As Integer
      Get
        Return m_dflt_permission_inherited_form_id
      End Get
      Set(ByVal value As Integer)
        m_dflt_permission_inherited_form_id = value
      End Set
    End Property

    Public Property DefltPermissionFixed() As Integer
      Get
        Return m_dflt_permission_fixed
      End Get
      Set(ByVal value As Integer)
        m_dflt_permission_fixed = value
      End Set
    End Property

    Public Property DefltPermissionProposed() As Integer
      Get
        Return m_dflt_permission_proposed
      End Get
      Set(ByVal value As Integer)
        m_dflt_permission_proposed = value
      End Set
    End Property

  End Class    ' CLASS_GUI_FORM

#Region " CommonMisc.dll "

#Region " Structures "

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_GUI_FORM_ITEM
    Public id As Integer            ' FormId
    Public nls_id As Integer        ' NlsId
    Public order As Integer         ' Order
    Public feature As ENUM_FEATURES ' Feature

    ' RCI 25-JAN-2012: Properties for Default Permissions
    Public dflt_permission_has_default As Boolean         ' HasDefault
    Public dflt_permission_inherited_form_id As Integer   ' InheritedFormId
    Public dflt_permission_fixed As Integer               ' FixedPermission
    Public dflt_permission_proposed As Integer            ' ProposedPermission

    Public Sub New()
      dflt_permission_has_default = False
      dflt_permission_inherited_form_id = 0
      dflt_permission_fixed = 0
      dflt_permission_proposed = -1
    End Sub

  End Class

  ' Class to pass fixed length arrays of forms to Pro*C
  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_GUI_FORMS_DB

    Public control_block As Integer
    Public gui_id As Integer
    Public forms_list As New CLASS_VECTOR()

    Public Sub New(ByVal GuiId As ENUM_GUI, Optional ByVal NumberItems As Integer = MAX_FORMS_BY_GUI)
      MyBase.New()
      control_block = Marshal.SizeOf(GetType(TYPE_GUI_FORMS_DB))
      gui_id = GuiId
      forms_list.Init(GetType(TYPE_GUI_FORM_ITEM), NumberItems)
    End Sub

    Protected Overrides Sub Finalize()
      forms_list.Done()
      MyBase.Finalize()
    End Sub

    Public Function SearchForm(ByVal FormId As Integer) As Integer
      Dim idx As Integer
      Dim form_item As New TYPE_GUI_FORM_ITEM()

      For idx = 0 To forms_list.Count - 1
        forms_list.GetItem(idx, form_item)
        If form_item.id = FormId Then
          Return idx
        End If
      Next

      Return -1

    End Function

    ' PURPOSE: Return the GUI_FORM wich has de given Id
    '
    '  PARAMS:
    '     - INPUT:
    '           - FormId As Integer
    '     - OUTPUT:
    '           - none
    '
    ' RETURNS:
    '     - TYPE_GUI_FORM_ITEM: If the item with given FormId exists
    '     - Nothing: if the item with given FormId does not exist
    '
    Public Function GetFormIfExists(ByVal FormId As Integer) As TYPE_GUI_FORM_ITEM
      Dim idx As Integer
      Dim form_item As New TYPE_GUI_FORM_ITEM()

      For idx = 0 To forms_list.Count - 1
        forms_list.GetItem(idx, form_item)
        If form_item.id = FormId Then
          Return form_item
        End If
      Next

      Return Nothing

    End Function

  End Class

#End Region

#End Region

#Region " Members "

  Private m_forms As New Collection


  ' members to check and update database  
  Private m_read_forms_db As TYPE_GUI_FORMS_DB
  Private m_insert_forms_db As TYPE_GUI_FORMS_DB
  Private m_update_forms_db As TYPE_GUI_FORMS_DB
  Private m_delete_forms_db As TYPE_GUI_FORMS_DB

#End Region

#Region " Private Functions "

  Private Function Common_ReadGuiForms(ByVal pGuiForms As TYPE_GUI_FORMS_DB, ByVal Context As Integer) As Integer
    Dim form_item As New TYPE_GUI_FORM_ITEM
    Dim str_sql As String
    Dim idx_db_row As Integer
    Dim num_db_rows As Integer = 0
    Dim data_table As DataTable

    str_sql = "SELECT GF_FORM_ID " & _
                   ", GF_FORM_ORDER " & _
                   ", GF_NLS_ID " & _
                "FROM GUI_FORMS " & _
               "WHERE GF_GUI_ID = " & pGuiForms.gui_id & _
           " ORDER BY GF_FORM_ID"

    data_table = GUI_GetTableUsingSQL(str_sql, 5000)

    If IsNothing(data_table) Then
      Return ENUM_COMMON_RC.COMMON_ERROR_DB
    End If

    num_db_rows = data_table.Rows.Count()

    For idx_db_row = 0 To num_db_rows - 1

      form_item.id = data_table.Rows(idx_db_row).Item("GF_FORM_ID")
      form_item.order = data_table.Rows(idx_db_row).Item("GF_FORM_ORDER")
      form_item.nls_id = data_table.Rows(idx_db_row).Item("GF_NLS_ID")
      pGuiForms.forms_list.SetItem(pGuiForms.forms_list.Count, form_item)

    Next

    Return ENUM_COMMON_RC.COMMON_OK
  End Function

  Private Function Common_InsertGuiForms(ByVal pGuiForms As TYPE_GUI_FORMS_DB, ByVal Context As Integer) As Integer
    Dim form_item As New TYPE_GUI_FORM_ITEM
    Dim str_sql As String
    Dim idx_item As Integer
    Dim num_items As Integer = 0
    'XVV 16/04/2007
    'Assign nothing to SQL Trans because compiler generate warning for not have value
    Dim SqlTrans As System.Data.SqlClient.SqlTransaction = Nothing
    Dim commit_trx As Boolean

    If Not GUI_BeginSQLTransaction(SqlTrans) Then
      Exit Function
    End If

    commit_trx = True
    num_items = pGuiForms.forms_list.Count
    For idx_item = 0 To num_items - 1

      pGuiForms.forms_list.GetItem(idx_item, form_item)

      ' UPDATE new order
      str_sql = "UPDATE   GUI_FORMS " & _
                   "SET   GF_FORM_ORDER = GF_FORM_ORDER + 1 " & _
                " WHERE   GF_FORM_ORDER >= " & form_item.order & _
                  " AND   GF_GUI_ID      = " & pGuiForms.gui_id

      If Not GUI_SQLExecuteNonQuery(str_sql, SqlTrans) Then
        commit_trx = False

        Exit For
      End If

      str_sql = "INSERT INTO   GUI_FORMS " & _
                            "( GF_GUI_ID " & _
                            ", GF_FORM_ID " & _
                            ", GF_FORM_ORDER " & _
                            ", GF_NLS_ID " & _
                            ") " & _
                    " VALUES " & _
                           " ( " & pGuiForms.gui_id & _
                           " , " & form_item.id & _
                           " , " & form_item.order & _
                           " , " & form_item.nls_id & _
                           " ) "

      If Not GUI_SQLExecuteNonQuery(str_sql, SqlTrans) Then
        commit_trx = False

        Exit For
      End If

    Next

    If Not GUI_EndSQLTransaction(SqlTrans, commit_trx) Then
      Exit Function
    End If

    Return ENUM_COMMON_RC.COMMON_OK
  End Function ' Common_InsertGuiForms

  ' PURPOSE: Obtain the profiles that have at least one permission set in the application defined by GuiId.
  '
  '  PARAMS:
  '     - INPUT:
  '           - GuiId As Integer
  '           - DbTrx As DB_TRX
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - DataTable
  '
  Private Function GetProfilesWithPermissions(ByVal GuiId As Integer, ByVal DbTrx As DB_TRX) As DataTable
    Dim _table As DataTable
    Dim _sql_txt As String

    _table = New DataTable()

    Try
      _sql_txt = "SELECT   PROFILE_ID " & _
                 "  FROM ( " & _
                 "         SELECT   GPF_PROFILE_ID AS PROFILE_ID " & _
                 "                , SUM(CASE WHEN GPF_READ_PERM    = 1 OR " & _
                 "                                GPF_WRITE_PERM   = 1 OR " & _
                 "                                GPF_DELETE_PERM  = 1 OR " & _
                 "                                GPF_EXECUTE_PERM = 1 " & _
                 "                      THEN 1 ELSE 0 END) AS HAS_SOME_PERMISSION " & _
                 "           FROM   GUI_PROFILE_FORMS " & _
                 "          WHERE   GPF_GUI_ID = @pGuiId " & _
                 "         GROUP BY GPF_PROFILE_ID " & _
                 "       ) X " & _
                 " WHERE   HAS_SOME_PERMISSION > 0 " & _
                 "ORDER BY PROFILE_ID "

      Using _sql_cmd As New SqlCommand(_sql_txt)
        _sql_cmd.Parameters.Add("@pGuiId", SqlDbType.Int).Value = GuiId
        Using _sql_da As New SqlDataAdapter(_sql_cmd)
          DbTrx.Fill(_sql_da, _table)
        End Using
      End Using

    Catch ex As Exception
    End Try

    Return _table
  End Function ' GetProfilesWithPermissions

  ' PURPOSE: Insert the default permissions for the NewForms if they have Default Permissions enabled.
  '
  '  PARAMS:
  '     - INPUT:
  '           - NewForms As TYPE_GUI_FORMS_DB
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - Boolean: True if inserted ok, False otherwise.
  '
  Private Function Common_InsertDefaultPermissions(ByVal NewForms As TYPE_GUI_FORMS_DB) As Boolean
    Dim _has_default As Boolean
    Dim _num_forms As Integer
    Dim _idx_form As Integer
    Dim _new_form As New TYPE_GUI_FORM_ITEM
    Dim _sql_txt As String
    Dim _dt_inherited_profiles As DataTable
    Dim _dt_to_insert_profiles_perm As DataTable
    Dim _dt_profiles As DataTable
    Dim _new_row As DataRow
    Dim _inherited_profiles() As DataRow
    Dim _inherited_profile As DataRow
    Dim _num_rows_inserted As Integer
    Dim _new_perm As TYPE_PERMISSIONS
    Dim _inherited_perm As TYPE_PERMISSIONS

    Try
      ' Check if exists any Form that has Default Permissions enabled.
      _has_default = False
      _num_forms = NewForms.forms_list.Count

      For _idx_form = 0 To _num_forms - 1
        NewForms.forms_list.GetItem(_idx_form, _new_form)

        If _new_form.dflt_permission_has_default Then
          _has_default = True
          Exit For
        End If
      Next

      ' There are not forms with default permissions. Can exit now, there is nothing to do.
      If Not _has_default Then
        Return True
      End If

      ' Create a DataTable to save all the Profiles Forms that need to INSERT to DB.
      ' These are the Default Permissions if they are defined with the routine SetDefaultPermissions().
      _dt_to_insert_profiles_perm = New DataTable()
      With _dt_to_insert_profiles_perm
        .Columns.Add("PROFILE_ID", Type.GetType("System.Int32"))
        .Columns.Add("GUI_ID", Type.GetType("System.Int32"))
        .Columns.Add("FORM_ID", Type.GetType("System.Int32"))
        .Columns.Add("READ_PERM", Type.GetType("System.Boolean"))
        .Columns.Add("WRITE_PERM", Type.GetType("System.Boolean"))
        .Columns.Add("DELETE_PERM", Type.GetType("System.Boolean"))
        .Columns.Add("EXECUTE_PERM", Type.GetType("System.Boolean"))

        .PrimaryKey = New DataColumn() {.Columns(0), .Columns(1), .Columns(2)}
      End With

      _dt_inherited_profiles = New DataTable()

      _sql_txt = "SELECT   GPF_PROFILE_ID    AS PROFILE_ID " & _
                 "       , GPF_READ_PERM     AS READ_PERM " & _
                 "       , GPF_WRITE_PERM    AS WRITE_PERM " & _
                 "       , GPF_DELETE_PERM   AS DELETE_PERM " & _
                 "       , GPF_EXECUTE_PERM  AS EXECUTE_PERM " & _
                 "  FROM   GUI_PROFILE_FORMS " & _
                 " WHERE   GPF_FORM_ID = @pInheritedFormId " & _
                 "   AND   GPF_GUI_ID  = @pGuiId " & _
                 "ORDER BY GPF_PROFILE_ID "

      Using _db_trx As New DB_TRX()

        ' Obtain the Profiles that have to be inserted.
        ' These are those with some permission on the application defined by gui_id.
        _dt_profiles = GetProfilesWithPermissions(NewForms.gui_id, _db_trx)

        Using _sql_cmd As New SqlCommand(_sql_txt)
          _sql_cmd.Parameters.Add("@pGuiId", SqlDbType.Int).Value = NewForms.gui_id
          _sql_cmd.Parameters.Add("@pInheritedFormId", SqlDbType.Int)

          Using _sql_da As New SqlDataAdapter(_sql_cmd)

            ' For all the new forms inserted (this is the NewForms.forms_list parameter).
            For _idx_form = 0 To _num_forms - 1
              NewForms.forms_list.GetItem(_idx_form, _new_form)

              ' If new form has Default Permission...
              If _new_form.dflt_permission_has_default Then

                _dt_inherited_profiles.Clear()

                ' Get all the profiles permissions for the InheritedFormId that is the base for the permissions for the new form.
                _sql_cmd.Parameters("@pInheritedFormId").Value = _new_form.dflt_permission_inherited_form_id
                _db_trx.Fill(_sql_da, _dt_inherited_profiles)

                ' For all the profiles that have permissions, save them in the DataTable to insert
                ' the calculated permissions for the new form.
                For Each _profile As DataRow In _dt_profiles.Rows

                  ' Search for the _profile in the read _inherited_profiles.
                  _inherited_profiles = _dt_inherited_profiles.Select("PROFILE_ID = " & _profile("PROFILE_ID"))

                  ' If _profile is found in the read _inherited_profiles, use the permission from the _inherited_profile.
                  If _inherited_profiles.Length > 0 Then
                    _inherited_profile = _inherited_profiles(0)
                    _inherited_perm = TYPE_PERMISSIONS.Create(_inherited_profile("READ_PERM"), _
                                                              _inherited_profile("WRITE_PERM"), _
                                                              _inherited_profile("DELETE_PERM"), _
                                                              _inherited_profile("EXECUTE_PERM"))

                  Else
                    ' If not found, _inherited_perm is NONE.
                    _inherited_perm = TYPE_PERMISSIONS.Create(TYPE_PERMISSIONS.NONE)
                  End If

                  ' Calculate the permissions for the new form, based on the inherited permission.
                  _new_perm = TYPE_PERMISSIONS.Create(_new_form.dflt_permission_fixed Or (_new_form.dflt_permission_proposed And _inherited_perm.IntegerValue))

                  ' Don't insert the profile-form that doesn't have any permission.
                  If _new_perm.IntegerValue = TYPE_PERMISSIONS.NONE Then
                    Continue For
                  End If

                  _new_row = _dt_to_insert_profiles_perm.NewRow()
                  _new_row("PROFILE_ID") = _profile("PROFILE_ID")
                  _new_row("GUI_ID") = NewForms.gui_id
                  _new_row("FORM_ID") = _new_form.id
                  _new_row("READ_PERM") = _new_perm.Read
                  _new_row("WRITE_PERM") = _new_perm.Write
                  _new_row("DELETE_PERM") = _new_perm.Delete
                  _new_row("EXECUTE_PERM") = _new_perm.Execute

                  _dt_to_insert_profiles_perm.Rows.Add(_new_row)
                Next ' For Each _profile
              End If
            Next ' For _idx_form

          End Using
        End Using

        If _dt_to_insert_profiles_perm.Rows.Count = 0 Then
          Return True
        End If

        ' Insert new form profile permissions
        _sql_txt = "INSERT INTO   GUI_PROFILE_FORMS " & _
                   "            ( GPF_PROFILE_ID " & _
                   "            , GPF_GUI_ID " & _
                   "            , GPF_FORM_ID " & _
                   "            , GPF_READ_PERM " & _
                   "            , GPF_WRITE_PERM " & _
                   "            , GPF_DELETE_PERM " & _
                   "            , GPF_EXECUTE_PERM " & _
                   "            ) " & _
                   "     VALUES " & _
                   "            ( @pProfileId " & _
                   "            , @pGuiId " & _
                   "            , @pFormId " & _
                   "            , @pReadPerm " & _
                   "            , @pWritePerm " & _
                   "            , @pDeletePerm " & _
                   "            , @pExecutePerm " & _
                   "            ) "

        Using _sql_cmd As New SqlCommand(_sql_txt)
          _sql_cmd.Parameters.Add("@pProfileId", SqlDbType.Int).SourceColumn = "PROFILE_ID"
          _sql_cmd.Parameters.Add("@pGuiId", SqlDbType.Int).SourceColumn = "GUI_ID"
          _sql_cmd.Parameters.Add("@pFormId", SqlDbType.Int).SourceColumn = "FORM_ID"
          _sql_cmd.Parameters.Add("@pReadPerm", SqlDbType.Bit).SourceColumn = "READ_PERM"
          _sql_cmd.Parameters.Add("@pWritePerm", SqlDbType.Bit).SourceColumn = "WRITE_PERM"
          _sql_cmd.Parameters.Add("@pDeletePerm", SqlDbType.Bit).SourceColumn = "DELETE_PERM"
          _sql_cmd.Parameters.Add("@pExecutePerm", SqlDbType.Bit).SourceColumn = "EXECUTE_PERM"

          Using _sql_da As New SqlDataAdapter()
            _sql_da.InsertCommand = _sql_cmd
            _sql_da.ContinueUpdateOnError = True
            _sql_da.UpdateBatchSize = 500
            _sql_da.InsertCommand.UpdatedRowSource = UpdateRowSource.None

            _num_rows_inserted = _db_trx.Update(_sql_da, _dt_to_insert_profiles_perm)

            If _num_rows_inserted <> _dt_to_insert_profiles_perm.Rows.Count Then
              Return False
            End If

            _db_trx.Commit()

            Return True
          End Using
        End Using

      End Using ' DB_TRX()

    Catch ex As Exception
      Return False
    End Try
  End Function ' Common_InsertDefaultPermissions

  ' PURPOSE: Update the GUI_FORMS rows given. If there's an error, all of the updates are rolled back
  '
  '   PARAMS:
  '     - INPUT:
  '         - GuiForms As TYPE_GUI_FORMS_DB
  '     - OUTPUT:
  '         - None
  '   RETURNS:
  '     - ENUM_COMMON_RC.COMMON_OK: If the mass updating went ok.
  '     - ENUM_COMMON_RC.COMMON_ERROR_DB: If there was an error during the update.
  '
  Private Function Common_UpdateGuiForms(ByVal GuiForms As TYPE_GUI_FORMS_DB) As Integer
    Dim _form_item As New TYPE_GUI_FORM_ITEM
    Dim _sb As Text.StringBuilder
    Dim _idx_item As Integer
    Dim _dt_to_update_perm As DataTable
    Dim _new_row As DataRow
    Dim _num_rows_inserted As Integer

    _dt_to_update_perm = New DataTable()
    With _dt_to_update_perm
      .Columns.Add("GF_FORM_ID", Type.GetType("System.Int32"))
      .Columns.Add("GF_GUI_ID", Type.GetType("System.Int32"))
      .Columns.Add("GF_FORM_ORDER", Type.GetType("System.Int32"))
      .Columns.Add("GF_NLS_ID", Type.GetType("System.Int32"))
      .PrimaryKey = New DataColumn() {.Columns(0), .Columns(1)}
    End With

    For _idx_item = 0 To GuiForms.forms_list.Count - 1

      GuiForms.forms_list.GetItem(_idx_item, _form_item)

      _new_row = _dt_to_update_perm.NewRow()
      _new_row("GF_FORM_ID") = _form_item.id
      _new_row("GF_GUI_ID") = GuiForms.gui_id
      _new_row("GF_FORM_ORDER") = _form_item.order
      _new_row("GF_NLS_ID") = _form_item.nls_id

      _dt_to_update_perm.Rows.Add(_new_row)
    Next

    Using _db_trx As New DB_TRX()

      _sb = New Text.StringBuilder()
      _sb.AppendLine("UPDATE   GUI_FORMS ")
      _sb.AppendLine("   SET   GF_FORM_ORDER = @pOrder")
      _sb.AppendLine("       , GF_NLS_ID     = @pNlsId")
      _sb.AppendLine(" WHERE   GF_FORM_ID    = @pFormId")
      _sb.AppendLine("   AND   GF_GUI_ID     = @pGuiId")

      Using _cmd As New SqlCommand(_sb.ToString())

        _cmd.Parameters.Add("@pOrder", SqlDbType.Int).SourceColumn = "GF_FORM_ORDER"
        _cmd.Parameters.Add("@pNlsId", SqlDbType.Int).SourceColumn = "GF_NLS_ID"
        _cmd.Parameters.Add("@pFormId", SqlDbType.Int).SourceColumn = "GF_FORM_ID"
        _cmd.Parameters.Add("@pGuiId", SqlDbType.Int).SourceColumn = "GF_GUI_ID"

        Using _sql_da As New SqlDataAdapter()
          _sql_da.InsertCommand = _cmd
          _sql_da.ContinueUpdateOnError = True
          _sql_da.UpdateBatchSize = 500
          _sql_da.InsertCommand.UpdatedRowSource = UpdateRowSource.None

          _num_rows_inserted = _db_trx.Update(_sql_da, _dt_to_update_perm)

          If _num_rows_inserted <> _dt_to_update_perm.Rows.Count Then
            Return ENUM_COMMON_RC.COMMON_ERROR_DB
          End If

          _db_trx.Commit()
        End Using
      End Using
    End Using

    Return ENUM_COMMON_RC.COMMON_OK

  End Function ' Common_UpdateGuiForms

  ' PURPOSE: Check if the latest version is already installed in a terminal
  '
  '   PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '   
  '   RETURNS:
  '     - True: If latest version is already installed in a terminal
  '     - False: otherwise
  '
  Private Function IsLatestVersion() As Boolean

    Dim _sql_str As StringBuilder
    Dim _latest_version As String
    Dim _app_name As String
    Dim _app_type As WSI.Common.ENUM_TSV_PACKET
    Dim _app_version As String

    _sql_str = New StringBuilder()
    _app_type = -1
    _app_name = String.Empty
    _latest_version = String.Empty
    _app_version = String.Empty

    Select Case GLB_GuiMode

      Case public_globals.ENUM_GUI.WIGOS_GUI
        _app_type = ENUM_TSV_PACKET.TSV_PACKET_WIN_GUI
        _app_name = "WigosGUI"

      Case public_globals.ENUM_GUI.MULTISITE_GUI
        _app_type = ENUM_TSV_PACKET.TSV_PACKET_WIN_MULTISITE_GUI
        _app_name = "WigosMultiSiteGUI"

      Case public_globals.ENUM_GUI.CASHDESK_DRAWS_GUI
        _app_type = ENUM_TSV_PACKET.TSV_PACKET_WIN_CASHDESK_DRAWS_GUI
        _app_name = "WigosDrawGUI"

    End Select

    If _app_type = -1 Then
      ' Incorrect application type
      Return False
    End If

    ' In debug mode all new permissions or NLS changes need to be installed
#If DEBUG Then
    Return True
#End If

    ' Get the latest version value
    Using _db_trx As DB_TRX = New DB_TRX()

      _sql_str.Length = 0
      _sql_str.AppendLine("  SELECT   TOP 1 TSV_CLIENT_ID ")
      _sql_str.AppendLine("         , TSV_BUILD_ID ")
      _sql_str.AppendLine("    FROM   TERMINAL_SOFTWARE_VERSIONS ")
      _sql_str.AppendLine("   WHERE   TSV_TERMINAL_TYPE =  @pApp_Type ")
      _sql_str.AppendLine("ORDER BY   TSV_INSERTION_DATE DESC ")

      Using _sql_cmd As SqlCommand = New SqlCommand(_sql_str.ToString())
        _sql_cmd.Parameters.Add("@pApp_Type", SqlDbType.SmallInt).Value = CType(_app_type, Integer)
        Using _reader As SqlClient.SqlDataReader = _db_trx.ExecuteReader(_sql_cmd)

          If Not (_reader.Read()) Then

            'Not version found
            Return False
          End If
          _latest_version = String.Format("{0:00}.{1:000}", _reader(0), _reader(1))
        End Using
      End Using

      'Get the local installed version 
      _app_version = WSI.Common.WGDB.GetAppVersion()

      ' Check if the latest version is the same than the app version
      If Not _app_version.Equals(_latest_version) Then

        Return False
      End If

    End Using

    ' Is the latest version
    Return True
  End Function 'IsLatestVersion

#End Region

#Region " Public Functions "

  ' PURPOSE: Return number of elements of the collection
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - Integer: number of elements
  '
  Public Function FormCount() As Integer
    Return m_forms.Count
  End Function ' FormCount

  ' PURPOSE: Return an element from the collection
  '
  '  PARAMS:
  '     - INPUT:
  '           - FormId
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - CLASS_GUI_FORM: collection's element
  '
  Public Function Form(ByVal FormId As Integer) As CLASS_GUI_FORM
    Dim _key As String

    _key = FormId.ToString()
    If m_forms.Contains(_key) Then
      Return m_forms.Item(_key)
    End If

    Return Nothing
  End Function ' Form

  ' PURPOSE: Add a new form id a its nls id to the collection
  '
  '  PARAMS:
  '     - INPUT:
  '           - FormId 
  '           - NlsId 
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  '
  Public Sub Add(ByVal FormId As Integer, _
                 ByVal NlsId As Integer, ByVal Feature As ENUM_FEATURES, _
                 Optional ByVal Nls As String = vbNullString)
    m_forms.Add(New CLASS_GUI_FORM(FormId, NlsId, m_forms.Count, Feature, Nls), FormId)
  End Sub ' Add

  ' PURPOSE: Get Gui Fomr by Form Id
  '
  '  PARAMS:
  '     - INPUT:
  '       - FormId
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - CLASS_GUI_FORM
  '
  Public Function GetFormData(ByVal FormId As Integer) As CLASS_GUI_FORM
    Dim _key As String

    _key = FormId.ToString()

    If m_forms.Contains(_key) Then
      Return m_forms.Item(_key)
    End If

    Return Nothing
  End Function ' GetFormData

  ' PURPOSE: Set the default permissions to the new Form for all profiles based on the permissions of InheritedFormId.
  '
  '  PARAMS:
  '     - INPUT:
  '           - FormId: The new Form.
  '           - InheritedFormId: The inherited form Id used for calculate the permissions for the new FormId.
  '           - Optional FixedPermission: The indicated permissions have to be set.
  '           - Optional ProposedPermission: The desired permissions. They are "AND"ed with the permissions of the inherited FormId.
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Public Sub SetDefaultPermissions(ByVal FormId As Integer, _
                                   ByVal InheritedFormId As Integer, _
                                   Optional ByVal FixedPermission As Integer = TYPE_PERMISSIONS.NONE, _
                                   Optional ByVal ProposedPermission As Integer = TYPE_PERMISSIONS.INHERIT)
    Dim _form As CLASS_GUI_FORM

    _form = Form(FormId)
    If _form Is Nothing Then
      Exit Sub
    End If

    ' If the permissions defined by the parameters are 0, we will not have default permissions in this form.
    _form.DefltPermissionHasDefault = (FixedPermission Or ProposedPermission) <> 0
    If Not _form.DefltPermissionHasDefault Then
      Exit Sub
    End If

    _form.DefltPermissionInheritedFormId = InheritedFormId
    _form.DefltPermissionFixed = FixedPermission
    _form.DefltPermissionProposed = ProposedPermission

  End Sub ' SetDefaultPermissions

  ' PURPOSE: Check if all forms declared at collection have its corresponent register into database
  '
  '  PARAMS:
  '     - INPUT:
  '           - GuiId: Forms in collection are af this gui
  '           - ShowDiff: If true, a msgBox show diferences
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - True. If collection forms are equal to these into database.
  '     - False.
  '
  Public Function Check(ByVal GuiId As ENUM_GUI, ByVal IsSuperUser As Boolean, ByRef SqlCtx As Integer) As Integer

    Dim rc As Integer
    Dim idx As Integer
    Dim form_item As CLASS_GUI_FORM
    Dim form_db_item As New TYPE_GUI_FORM_ITEM
    Dim equal As Boolean

    m_read_forms_db = Nothing
    m_insert_forms_db = Nothing
    m_update_forms_db = Nothing
    m_delete_forms_db = Nothing

    m_read_forms_db = New TYPE_GUI_FORMS_DB(GuiId)
    m_insert_forms_db = New TYPE_GUI_FORMS_DB(GuiId)
    m_update_forms_db = New TYPE_GUI_FORMS_DB(GuiId)
    m_delete_forms_db = New TYPE_GUI_FORMS_DB(GuiId)

    ' Call CommonMisc to get forms that exist in DB for this GUI 
    rc = Common_ReadGuiForms(m_read_forms_db, SqlCtx)

    If rc <> CInt(ENUM_COMMON_RC.COMMON_OK) Then
      Return ENUM_CHECK.CHECK_ERROR_DB
    End If

    ' Check for latest version installed in a terminal
    If Not IsLatestVersion() Then

      Return ENUM_CHECK.CHECK_OK
    End If

    ' Compare Coded Forms with these readed from DB
    equal = True
    For idx = 1 To m_forms.Count

      form_item = m_forms.Item(idx)
      form_db_item = m_read_forms_db.GetFormIfExists(form_item.Id)

      ' If not found append to miss list
      If form_db_item Is Nothing Then
        form_db_item = New TYPE_GUI_FORM_ITEM()
        form_db_item.id = form_item.Id
        form_db_item.nls_id = form_item.NlsId
        form_db_item.order = form_item.Order
        ' RCI 25-JAN-2012: Default properties
        form_db_item.dflt_permission_has_default = form_item.DefltPermissionHasDefault
        form_db_item.dflt_permission_inherited_form_id = form_item.DefltPermissionInheritedFormId
        form_db_item.dflt_permission_fixed = form_item.DefltPermissionFixed
        form_db_item.dflt_permission_proposed = form_item.DefltPermissionProposed

        m_insert_forms_db.forms_list.SetItem(m_insert_forms_db.forms_list.Count, form_db_item)
        equal = False
      End If

    Next

    ' TODO: Detect old forms to be deleted

    If Not equal Then
      Return ENUM_CHECK.CHECK_ERROR
    End If

    Return ENUM_CHECK.CHECK_OK

  End Function ' Check

  ' PURPOSE: If there are diferences all missing forms are inserted.
  '          Also, if Default Permissions are enabled for the missing forms, the default permissions are inserted.
  '
  '  PARAMS:
  '     - INPUT:
  '           - GuiId: Forms in collection are of this gui
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - True. If updated successfully
  '     - False.
  '
  Public Function Update(ByVal GuiId As ENUM_GUI, _
                         ByVal IsSuperUser As Boolean, _
                         ByRef SqlCtx As Integer) As Boolean

    Dim rc As Integer

    If m_read_forms_db Is Nothing Then
      Call Check(GuiId, IsSuperUser, SqlCtx)
    End If

    ' Insert new forms to GUI_FORMS
    If m_insert_forms_db.forms_list.Count > 0 Then
      rc = Common_InsertGuiForms(m_insert_forms_db, SqlCtx)
      If rc <> CInt(ENUM_COMMON_RC.COMMON_OK) Then
        Return False
      End If

      ' RCI 25-JAN-2012: Insert default permissions for all new detected forms.
      '                  Each form indicates if it is using the Default Permissions.
      If Not Common_InsertDefaultPermissions(m_insert_forms_db) Then
        Return False
      End If
    End If

    If m_update_forms_db.forms_list.Count > 0 Then
      rc = Common_UpdateGuiForms(m_update_forms_db)
      If rc <> CInt(ENUM_COMMON_RC.COMMON_OK) Then
        Return False
      End If
    End If

    ' free forms to force next update call to recheck
    m_read_forms_db = Nothing
    m_insert_forms_db = Nothing
    m_update_forms_db = Nothing
    m_delete_forms_db = Nothing

    Return True

  End Function ' Update

#End Region

End Class
