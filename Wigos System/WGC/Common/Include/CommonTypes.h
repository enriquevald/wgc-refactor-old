//------------------------------------------------------------------------------
// Copyright � 2003 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : CommonTypes.h
// 
//   DESCRIPTION : Common types for CENTER modules
// 
//        AUTHOR : Toni Jord�
// 
// CREATION DATE : 13-FEB-2003
// 
// REVISION HISTORY
// 
// Date        Author Description
// ----------- ------ -----------------------------------------------------------
// 13-FEB-2003 TJG    Initial draft.
// 
//------------------------------------------------------------------------------
#ifndef __COMMON_TYPES_H
#define __COMMON_TYPES_H

//------------------------------------------------------------------------------
// INCLUDES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PUBLIC CONSTANTS 
//------------------------------------------------------------------------------
#define COMMON_LEN_LKC_ADDR         16
#define COMMON_MAX_LKC_ADDR         20
#define LEN_DATE                    20    

//------------------------------------------------------------------------------
// PUBLIC DATATYPES 
//------------------------------------------------------------------------------
typedef struct
{
  DWORD         year;
  DWORD         month;
  DWORD         day;
  DWORD         hour;
  DWORD         minute;
  DWORD         second;
  DWORD         is_null;

} TYPE_DATE_TIME;

typedef struct
{
  TYPE_COMMON_BOOK_KEY  book_key;

  WORD              num_tickets;
  BYTE              status;
  BYTE              agency_status;
  WORD              num_sold;
  TYPE_DATE_TIME    date_assigned;
  DWORD             h_inst_assigned;
  BYTE              series_status;      // AJQ 12-FEB-2004, The series status is also returned
  BOOL              reported;
  
} TYPE_BOOK;

typedef struct
{
  TYPE_COMMON_BOOK_KEY  book_key;
  BYTE                  price_level_id;
  float                 ticket_price;
  WORD                  prize_expiration;
  BYTE                  jackpot_flag;
  float                 jackpot_contribution_pct;
  DWORD                 book_enc_key_len;
  BYTE                  book_enc_key [NUM_BYTES_ENC_BOOK_ENC_KEY];
  WORD                  num_categories;

  BYTE                  prize_type [NUM_PRIZE_CATEGORIES];
  double                net_prize_amount [NUM_PRIZE_CATEGORIES];
  double                gross_prize_amount [NUM_PRIZE_CATEGORIES];

  WORD                  num_tickets;
  WORD                  num_sold;
  BYTE                  ticket [MAX_TICKETS][NUM_BYTES_TICKET_PRIZE_INFO];
  //
  WORD                  dwl_segment_id;
  WORD                  dwl_num_segments;
  WORD                  dwl_ticket_index;

} TYPE_DOWNLOAD_BOOK_DATA;

typedef struct
{
  DWORD                           internal_ids [3];
  DWORD                           external_ids [3];
  DWORD                           agency_id;
  TYPE_COMMON_BOOK_KEY            book_key;
  TYPE_COMMON_BOOK_SALES_HISTORY  sales_history;         // NIR, 20-APR-2004:
  WORD                            last_ticket_to_sell;   // 0 if till the last one

} TYPE_SALES_DATA;
 
typedef struct
{
  DWORD64         card_id;
  DWORD           h_inst_id;
  TYPE_DATE_TIME  creation_date;
  TYPE_DATE_TIME  expired_date;
  double          rest_amount;

} TYPE_EXPIRED_CREDIT;

typedef struct
{
  DWORD64         collect_id;
  DWORD           h_inst_id;
  TYPE_DATE_TIME  creation_date;
  TYPE_DATE_TIME  expired_date;
  double          amount;

} TYPE_EXPIRED_COLLECT;

typedef struct
{
  DWORD   h_inst_id;                            // PK
  TCHAR   name [HI_LEVEL3_LEN_NAME + 1];
  TCHAR   address [HI_LEVEL3_LEN_ADDRESS + 1];
  TCHAR   zip_code [HI_LEVEL3_LEN_ZIP + 1];
  TCHAR   phone [HI_LEVEL3_LEN_PHONE + 1];
  TCHAR   phone2[HI_LEVEL3_LEN_PHONE + 1];  
  TCHAR   nif [HI_LEVEL3_LEN_NIF + 1];
  TCHAR   location1 [HI_LEVEL3_LEN_LOCATION + 1];
  TCHAR   location2 [HI_LEVEL3_LEN_LOCATION + 1];
  TCHAR   location3 [HI_LEVEL3_LEN_LOCATION + 1];

  BYTE    state;
  BYTE    sale_limit_flag;
  BYTE    sale_limit_period;
  double  sale_limit_amount;
  DWORD   prize_permissions;
  BYTE    mode;
  BYTE    status;

  BYTE    line_type;
  TCHAR   phys_address [HI_LEVEL3_LEN_PHYS_ADDR + 1];
  TCHAR   add_net_addr [HI_LEVEL3_LEN_ADD_NET_ADDR + 1];

  DWORD    lkas_client;
  DWORD    lkas_build;
  DWORD    lkt_client;
  DWORD    lkt_build;

  DWORD    expected_lkas_client;
  DWORD    expected_lkas_build;
  DWORD    expected_lkt_client;
  DWORD    expected_lkt_build;

  TCHAR   lkas_digest;
  TCHAR   lkt_digest;

  BYTE    auto_logout;
  WORD    max_terminals_allowed;
  BYTE    operative_flag;
  BYTE    audit_level;

  TYPE_DATE_TIME  last_connection_date;

  BYTE    active_mode_type;
  WORD    active_mode_time_on;
  WORD    active_mode_time_off;
  BYTE    inactive_mode_type;
  WORD    inactive_mode_time_on;
  WORD    inactive_mode_time_off;
  DWORD   terminal_active_time;
  DWORD   play_max_time;
  DWORD   step_play_guide_max_time;
  BYTE    autoplay;       // AGENCY_AUTOPLAY_ENABLED or AGENCY_AUTOPLAY_DISABLED
  
  // AJQ 15-APR-2004
  TYPE_COMMON_LKC_ADDR_LIST  lkc_addr_list;

  TYPE_COMMON_ONLINE_PARAMS online_params;

} TYPE_HI_LEVEL3;

typedef struct
{
  WORD  max_terminals;
  BOOL  terminal_not_allowed [1 + COMMON_MAX_KIOSK_LIST]; // +1 Agency Server: Index = 0

} TYPE_REPORTED_DEVICE_STATUS;


typedef struct
{
  // PK
  WORD            game_id;
  DWORD           series_id;
  DWORD           book_id;
  WORD            ticket_id;

  float           price;
  BYTE            status;
  TYPE_DATE_TIME  date_sold;
  DWORD           h_inst_sold;
  BYTE            prize_data [NUM_BYTES_PRIZE_DATA];

} TYPE_TICKET_LK;

typedef struct
{
  // PK
  WORD            game_id;
  DWORD           series_id;
  DWORD           book_id;
  WORD            ticket_id;

  BYTE            cash_status;
  BYTE            prize_type;
  BYTE            win_category;
  double          prize_amount;
  TYPE_DATE_TIME  date_sold;
  DWORD           jackpot_id;
  DWORD           h_inst_sold;
  TYPE_DATE_TIME  cashed_date;
  DWORD           h_inst_cashed;
  WORD            msg_id_cashed;
  TYPE_DATE_TIME  expiration_date;

} TYPE_WINNING_TICKET_LK;

typedef struct
{
  // PK
  WORD            game_id;
  DWORD           series_id;
  DWORD           book_id;

  TYPE_DATE_TIME  activation_date;
  BYTE            enc_key [NUM_BYTES_BOOK_ENC_KEY];

} TYPE_BOOK_KEY;

typedef struct
{
  // PK
  DWORD           h_inst_id;
  BYTE            key_type;

  TYPE_DATE_TIME  activation_date;
  BYTE            enc_key [NUM_BYTES_SEC_ENC_KEY];
  WORD            key_len;

} TYPE_SECURITY_KEY;

typedef struct
{
  // PK
  TCHAR           group_key [GENERAL_PARAMS_LEN_GROUP_KEY + 1];
  TCHAR           subject_key [GENERAL_PARAMS_LEN_SUBJECT_KEY + 1];

  TCHAR           key_value [GENERAL_PARAMS_LEN_KEY_VALUE + 1];
  TCHAR           modified_by [GENERAL_PARAMS_LEN_MODIFIED_BY + 1];
  TYPE_DATE_TIME  modified_date;

} TYPE_GENERAL_PARAM;

typedef struct
{
  // PK
  WORD            game_id;

  TCHAR           name [GAMES_LEN_NAME + 1];
  TCHAR           aux_name [GAMES_LEN_AUX_NAME + 1];
  TYPE_DATE_TIME  date_created;
  BYTE            active_flag;
  TYPE_DATE_TIME  date_enabled;
  TYPE_DATE_TIME  date_disabled;

} TYPE_GAME;

typedef struct
{
  // PK
  DWORD           level_id;

  TCHAR           name [PRICE_LEVELS_LEN_NAME + 1];
  TCHAR           description [PRICE_LEVELS_LEN_DESCRIPTION + 1];
  double          ticket_price;
  TYPE_DATE_TIME  created;
  TYPE_DATE_TIME  last_updated;

} TYPE_PRICE_LEVEL;

typedef struct
{
  // PK
  WORD            game_id;
  DWORD           level_id;
  DWORD           h_inst_id;

  WORD            stock_from_parent;
  DWORD           min_books;
  DWORD           num_books;

} TYPE_HI_STOCK;

typedef struct
{
  BYTE     price_level_id;
  DWORD64  min_ticket_stock_per_kiosk;
  DWORD64  min_online_stock_per_kiosk_req;
  DWORD64  num_tickets;

} TYPE_BTM_PL_DATA;

typedef struct
{
  WORD                        game_id;
  BYTE                        game_type;
  //BYTE                        commission_id;
  float                       sales_pct;                   
  float                       major_prizes_pct;
  float                       minor_prizes_pct;
  TCHAR                       game_name [GAMES_LEN_NAME + 2]; 
  TCHAR                       game_aux_name [GAMES_LEN_AUX_NAME + 2]; 
  BYTE                        number_price_levels;
  TYPE_BTM_PL_DATA            pl_data [MAX_PRICE_LEVELS];
  TYPE_COMMON_RANDOM_GAME_DATA random;

} TYPE_BTM_GAME_DATA;

typedef struct
{
  // PK
  DWORD           user_id;

  DWORD           password;
  BYTE            level;
  WORD            privileges;
  TCHAR           name [LKAS_USER_LEN_NAME + 1];
  
} TYPE_LKAS_USER;

typedef struct
{
  // PK 
  DWORD           jackpot_id;
  
  BYTE            status;
  BYTE            order;
  TYPE_DATE_TIME  creation_date;
  TYPE_DATE_TIME  last_updated;
  double          current_amount;
  TYPE_DATE_TIME  assign_date;
  TYPE_DATE_TIME  expiration_date;
  DWORD           assign_h_inst;
  WORD            game_id;
  DWORD           series_id;
  DWORD           book_id;
  WORD            ticket_id;
  DWORD           jackpot_manager_id;

} TYPE_JACKPOT;

typedef struct
{
  DWORD           h_inst_id;
  BYTE            tree_level;
  DWORD           extern_id;
  DWORD           parent_h_inst;

} TYPE_HIERARCHY_INSTANCE;

//typedef struct
//{
//  // PK
//  DWORD           group_id;
//  DWORD           task_id;
//  DWORD           h_inst_id;
//  TYPE_DATE_TIME  sched_at;
//  BYTE            status;
//  BYTE            task_type;
//  BYTE            task_parameter [NUM_BYTES_SPECIAL_TASK_PARAM];
//  WORD            num_attempts;
//  WORD            max_attempts;
//
//} TYPE_SPECIAL_TASK;

//typedef struct
//{
//  // PK
//  DWORD           terminal_id;
//
//  BYTE            terminal_type;
//  TCHAR           serial_num [TERMINAL_LEN_SERIAL_NUMBER + 1];
//  DWORD           h_inst_id;
//
//} TYPE_TERMINAL;
//
typedef struct
{
  // pk
  DWORD           conn_id;
  
  DWORD           h_inst_id;
  DWORD           frequecy;
  WORD            step;
  TYPE_DATE_TIME  run_at;
  BYTE            enabled;
  TYPE_DATE_TIME  date_from;
  TYPE_DATE_TIME  date_to;
  WORD            num_times_allowed;
  DWORD           retry_delay;
  WORD            retry_max;
  TYPE_DATE_TIME  next_run_at;
  TYPE_DATE_TIME  last_run_at;
  DWORD           lock_id;

  WORD            num_runs;

} TYPE_CONN_SCHEDULING;

typedef struct
{
  // pk
  DWORD           conn_id;
  DWORD           inst_id;
  DWORD           h_inst_id;

  TYPE_DATE_TIME  sched_at;
  TYPE_DATE_TIME  next_run_at;
  WORD            num_retries;
  BYTE            status;
  DWORD           lock_id;

  DWORD           retry_delay;
  WORD            retry_max;

} TYPE_CONN_INST;

typedef struct
{
  // pk
  WORD            address_id;

  BYTE            line_type;                 
  TCHAR           physical_address_data1 [LKC_ADDRESS_LEN_PHYS_ADDR_DATA + 1];
  TCHAR           physical_address_data2 [LKC_ADDRESS_LEN_PHYS_ADDR_DATA + 1];

} TYPE_LKC_ADDRESS;
 
typedef struct
{
  BYTE            channel_ids [NOTE_ACCEPTOR_NUM_CHANNELS];
  double          values [NOTE_ACCEPTOR_NUM_CHANNELS];
  BYTE            active_flags [NOTE_ACCEPTOR_NUM_CHANNELS];
  WORD            mask;

} TYPE_NOTE_ACCEPTOR_CONF;

typedef struct
{
  // pk
  DWORD           h_inst_id;
  WORD            game_id;
  BYTE            price_level_id;
  WORD            local_terminal_id;
  TYPE_DATE_TIME  sale_date;

  DWORD           num_sold_tickets;
  double          sold_amount;
  DWORD           num_no_refund;
  double          no_refund_amount;
  DWORD           num_minor;
  double          minor_amount;
  DWORD           num_major;
  double          major_amount;
  DWORD           num_special;
  double          special_amount;

} TYPE_TERMINAL_SALES;

typedef struct
{
  // pk
  DWORD           sched_id;
  WORD            type;
  WORD            client_id;
  WORD            build_id;
  BYTE            terminal_type;
  DWORD           h_inst_id;

  TYPE_DATE_TIME  scheduled_at;
  BYTE            status;
  WORD            max_attempts;
  WORD            retry_delay;
  DWORD           lock_id;
  TYPE_DATE_TIME  lock_time;

} TYPE_SOFTWARE_DOWNLOAD_SCHEDULING;

typedef struct
{
  // pk
  DWORD           sched_id;
  DWORD           client_id;
  DWORD           build_id;
  DWORD           terminal_type;
  DWORD           h_inst_id;

  TYPE_DATE_TIME  next_run_at;
  TYPE_DATE_TIME  last_run_at;
  DWORD           status;
  DWORD           num_attempts;
  DWORD           lock_id;
  TYPE_DATE_TIME  lock_time;
  double          progress_pct;

} TYPE_SOFTWARE_DOWNLOAD_INSTANCE;

typedef struct
{
  TYPE_SOFTWARE_DOWNLOAD_INSTANCE   sw_download_inst;
  DWORD                             type;
  DWORD                             max_attempts;
  DWORD                             retry_delay;
  DWORD                             from_build_id;
  
  DWORD                             file_size;
  DWORD                             file_datetime;
  DWORD                             file_crc;

  DWORD                             num_segments;
  DWORD                             last_file_segment_id;
  DWORD                             last_segment_acknowledged;
  
  BYTE                              current_phase;  // Download, Unpack.


} TYPE_SW_DOWNLOAD_DATA;

typedef struct
{
  DWORD     control_block;
  DWORD     terminal_type;
  DWORD     client_id;
  DWORD     build_id;
  DWORD     from_build_id;
}
TYPE_TERMINAL_SOFTWARE_VERSION;

/*typedef struct
{
  TYPE_SOFTWARE_DOWNLOAD_INSTANCE   sw_download_inst;
  WORD                        current_client_id;
  WORD                        current_build_id;
  TCHAR                       md5_digest[SW_MD5_DIGEST_LENGTH];

} TYPE_TERMINAL_SW_DATA;*/

// The TYPE_GAME_STOCK structure encapsulates a Stock record
typedef struct
{
  DWORD                 game_id;      // id of game
  TCHAR                 game_name[GAMES_LEN_NAME+2];    // name of the game ==> TO CHANGE
  DWORD                 price_level_id;     // price level
  TCHAR                 conf_desc[GAMES_LEN_AUX_NAME+2];    // description of the game configuration ==> TO_CHANGE
  DWORD64               num_tickets;  // available stock
  DWORD64               min_tickets;  // minimal stock allowed
  BYTE                  active_flag;  // is game activated

} TYPE_GAME_STOCK;

// The TYPE_GAME_STOCK_LIST structure holds a bunch of Stock records
typedef struct
{
  WORD                  game_stock_count;  // no. of stocks in the list
  TYPE_GAME_STOCK       game_stock_list[MAX_STOCKS];  // the stock records

} TYPE_GAME_STOCK_LIST;

typedef struct
{
  WORD      game_id;
  DWORD     series_id;
  DWORD     book_id;
  BOOL      reported;

} TYPE_REPORTED_BOOK;

typedef struct
{
  WORD      game_id;
  DWORD     series_id;
  DWORD     book_id;
  BYTE      series_status;
  BOOL      game_enabled;

} TYPE_ASSIGNED_BOOK;


// Vector
typedef struct
{
  DWORD   control_block;         // Control Block
  DWORD   control_block_item;    // Size of an element
  DWORD   max_items;             // Maximum number of elements
  DWORD   num_items;             // [In, Out] Actual number of elements
  VOID    * p_items;             // Pointer to the elements (Explicit cast requiered)

} TYPE_COMMON_VECTOR;

// Game
typedef struct
{
  DWORD   control_block;
  DWORD   game_id;
  TCHAR   name [MAX_LEN_GM_NAME + 1];
  TCHAR   filler_0 [3];
  TCHAR   aux_name [MAX_LEN_GM_AUX_NAME + 1];
  TCHAR   filler_1 [3];
  DWORD   active_flag;
  TYPE_DATE_TIME date_enabled;
  TYPE_DATE_TIME date_disabled;
  TYPE_COMMON_VECTOR game_price_levels;
  DWORD   series_active;
  DWORD   commission_type;
  TCHAR   commission_name [MAX_LEN_COMMISSION_NAME + 1 + 3];
  DWORD   max_cat_x_pp;
  DWORD   min_cat_x_pp;
  DWORD   num_pp_out_of_range;
  DWORD   num_pp_total;
  DWORD   game_type_id;
  TCHAR   game_type_name [MAX_LEN_GAME_TYPE_NAME + 1 + 3];

  struct 
  {
    struct
    {
      DWORD   default_values;
      float   contrib_percent;
      float   contrib_min_bet;
      DWORD   award_flag;
      float   award_min_bet;
    
    } jackpot;

  } wings;

} TYPE_GUI_GAME;

// Game price levels items
typedef struct
{ 
  DWORD   game_id;
  DWORD   price_level_id;
  TCHAR   description [MAX_LEN_PL_DESCRIPTION + 1];
  TCHAR   filler_0 [3];
  DWORD   num_books_x_series;
  DWORD   num_tickets_x_books;
  DWORD   stock_min_tickets;  
  DOUBLE  ticket_price;  

} TYPE_GAME_PRICE_LEVELS;


#endif // __COMMON_TYPES_H
