//------------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//------------------------------------------------------------------------------
//
//   MODULE NAME: CommonDefAPI.h
//   DESCRIPTION: Constants, types, variables definitions and prototypes
//                for generic APIs
//        AUTHOR: Alberto Cuesta
// CREATION DATE: 25-MAR-2002
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ -----------------------------------------------------------
// 25-MAR-2002 ACC    Initial draft.
//
//------------------------------------------------------------------------------

#ifndef __COMMONDEFAPI_H
#define __COMMONDEFAPI_H

//------------------------------------------------------------------------------
//  INCLUDES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PUBLIC CONSTANTS
//------------------------------------------------------------------------------

// API return codes                           // Return Code Explanation
#define API_STATUS_OK                       0 // API call OK. It means the API received the request correctly, but it is not the status of the execution.
#define API_STATUS_CB_SIZE_ERROR            1 // Control Block structure size error
#define API_STATUS_FUNCTION_CODE_ERROR      2 // Function Code error
#define API_STATUS_SUBFUNCTION_CODE_ERROR   3 // Sub Function Code error
#define API_STATUS_CALL_MODE_ERROR          4 // Call mode not available
#define API_STATUS_EVENT_OBJECT_ERROR       5 // Handle of Event Object not valid
#define API_STATUS_NO_RESOURCES_AVAILABLE   6 // The API has no resources available to handle the request
#define API_STATUS_PARAMETER_ERROR          7 // One or more of the supplied parameters is NULL

// API call modes                        // Call Mode Explanation
typedef enum
{
  API_MODE_SYNC       = 0, // Asynchronous
  API_MODE_ASYNC      = 1, // Synchronous
  API_MODE_NO_WAIT    = 2  // Client no wait

} ENUM_API_MODE;

//------------------------------------------------------------------------------
//  PUBLIC DATATYPES
//------------------------------------------------------------------------------

// API Input Parameters
typedef struct
{
  CONTROL_BLOCK   control_block;
  ENUM_API_MODE   mode;
  HANDLE          event_object;       // (Optional) Event to signal API execution
                                      // end in Async mode. Must be NULL in any other
                                      // mode (sync or no wait).
  WORD            function_code;      // Code to identify the transaction type
  WORD            sub_function_code;  // Optional

} API_INPUT_PARAMS;

// API Input User Data
typedef void API_INPUT_USER_DATA;

// API Output Parameters
typedef struct
{
  CONTROL_BLOCK   control_block;
  WORD            output_1;           // Used as "FunctionCode" or "Status"
  WORD            output_2;            // Used as "SubFunctionCode" or "SubStatus"

} API_OUTPUT_PARAMS;

// API Output User Data
typedef void API_OUTPUT_USER_DATA;

// Empty DLL Model param
typedef struct
{
  CONTROL_BLOCK     control_block;

} TYPE_PARAM_EMPTY;

//------------------------------------------------------------------------------
//  PUBLIC DATA STRUCTURES
//------------------------------------------------------------------------------

// None

//------------------------------------------------------------------------------
//  PUBLIC FUNCTION PROTOTYPES
//------------------------------------------------------------------------------

// None

#endif // __COMMONDEFAPI_H
