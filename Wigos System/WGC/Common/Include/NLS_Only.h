// ------------------------------------------------------------------------------------
// Copyright � 2002 Win Systems
// ------------------------------------------------------------------------------------
// 
//   MODULE NAME : NLS_ClientInternals.h
//
//   DESCRIPTION : Internal NLS definitions
//
//        AUTHOR : Toni Jord�
//
// CREATION DATE : 25-APR-2002
//
// REVISION HISTORY:
// 
// Date         Author Description
// -----------  ------ ------------------------------------------------------------------
// 25-APR-2002  TJG    Initial release
// --------------------------------------------------------------------------------------

#ifndef __NLS_ONLY_MODULES
#define __NLS_ONLY_MODULES

//------------------------------------------------------------------------------------
//  INCLUDES
//------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------
//  PUBLIC CONSTANTS
//------------------------------------------------------------------------------------
// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the PRINTER_API_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// PRINTER_API functions as being imported from a DLL, wheras this DLL sees symbols
// defined with this macro as being exported.

#ifdef NLS_CLIENT_EXPORTS
#define NLS_CLIENT_API __declspec(dllexport)
#else
#define NLS_CLIENT_API __declspec(dllimport)
#endif

//------------------------------------------------------------------------------------
//  PUBLIC DATATYPES
//------------------------------------------------------------------------------------
typedef struct
{
  DWORD     module_id;
  DWORD     nls_base_id;

} TYPE_MODULE_NLS;

//------------------------------------------------------------------------------------
//  PUBLIC DATA STRUCTURES
//------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------
//  PUBLIC FUNCTION PROTOTYPES
//------------------------------------------------------------------------------------
BOOL        GetClientModuleNLSBase (DWORD       ModuleId,
                                    DWORD       * pNLSBaseId);

BOOL        SearchModuleInList (TYPE_MODULE_NLS * pList,
                                DWORD           NumItems,
                                DWORD           ModuleId,
                                DWORD           * pNLSBaseId);

#endif // __NLS_ONLY_MODULES
