// ------------------------------------------------------------------------------------
// Copyright � 2002 Win Systems
// ------------------------------------------------------------------------------------
//
// MODULE NAME:   NLS_API.H
// DESCRIPTION:   Constants, types, variables definitions and prototypes 
//                for NLS_API.CPP
// AUTHOR:        Carlos A. Costa 
// CREATION DATE: 05-03-2002
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------  ------ -------------------------------------------------------------------
// 05-03-2002  CC     Initial release
// 18-03-2002  CC     #define  NLS_MAX_PARAMS added
// 30-04-2002  CC     Add function "NLS_SplitMessage"
// --------------------------------------------------------------------------------------

#ifndef __NLS_API_H
#define __NLS_API_H

// --------------------------------------------------------------------------------------
// MODULE VERSION
// --------------------------------------------------------------------------------------
#define NLS_API_VERSION     0

// --------------------------------------------------------------------------------------
// PUBLIC CONSTANTS 
// --------------------------------------------------------------------------------------

// Number maximun of parameters in string formats
#define NLS_MAX_PARAMS                      5

// Maximum length of each string on the string table
#define NLS_MSG_STRING_MAX_LENGTH           512

// Maximum length of each parameter
#define NLS_MSG_PARAM_MAX_LENGTH            40

typedef TCHAR TCHAR_NLS_PARAM [NLS_MSG_PARAM_MAX_LENGTH + 1];

// Private Messages
// Debug message with 5 parameters, if you use less, 
// the rest of parameters use _T(" ")
#define NLS_PRIVATE_MESSAGE_DEBUG           65001

// Return codes
#define NLS_STATUS_OK                       0
#define NLS_STATUS_ERROR                    1
#define NLS_STATUS_DLL_NOT_LOADED           2
#define NLS_STATUS_STRING_NOT_EXIST         3

// Languages
#define NLS_LANGUAGE_SPANISH                LANG_SPANISH
#define NLS_LANGUAGE_ENGLISH                LANG_ENGLISH
#define NLS_LANGUAGE_ITALIAN                LANG_ITALIAN
#define NLS_LANGUAGE_KOREAN                 LANG_KOREAN

// NLS button types (only related to bitmap)
#define NLS_BUTTON_OK                       1
#define NLS_BUTTON_OK_CANCEL                2
#define NLS_BUTTON_YES_NO                   3
#define NLS_BUTTON_CANCEL                   4
#define NLS_BUTTON_ABORTRETRYIGNORE         5
#define NLS_BUTTON_RETRYCANCEL              6

// NLS default buttons
#define NLS_FIRST_BUTTON                    1
#define NLS_SECOND_BUTTON                   2
#define NLS_THIRD_BUTTON                    3

// NLS Icon message
#define NLS_MSG_WARNING                     1
#define NLS_MSG_ERROR                       2
#define NLS_MSG_QUESTION                    3
#define NLS_MSG_INFORMATION                 4


// Macro to calculate rest of log parameter for Oracle errors
#define NLS_SplitParam(_str) (Common_StringSize(_str) > NLS_MSG_PARAM_MAX_LENGTH ? _str + NLS_MSG_PARAM_MAX_LENGTH + 1 : _T(""))

// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the NLS_API_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// NLS_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifdef NLS_API_EXPORTS
#define NLS_API __declspec(dllexport)
#else
#define NLS_API __declspec(dllimport)
#endif


// --------------------------------------------------------------------------------------
// PUBLIC DATATYPES 
// --------------------------------------------------------------------------------------

// --------------------------------------------------------------------------------------
// PUBLIC DATA STRUCTURES 
// --------------------------------------------------------------------------------------

// --------------------------------------------------------------------------------------
// PUBLIC FUNCTION PROTOTYPES 
// --------------------------------------------------------------------------------------
NLS_API WORD WINAPI NLS_SetLanguage (DWORD LanguageCode);

NLS_API WORD WINAPI NLS_GetString (const DWORD  StringId,
                                   TCHAR        * pResultString,
                                   const TCHAR  * pParam1 = NULL,
                                   const TCHAR  * pParam2 = NULL,
                                   const TCHAR  * pParam3 = NULL,
                                   const TCHAR  * pParam4 = NULL,
                                   const TCHAR  * pParam5 = NULL);


NLS_API WORD WINAPI NLS_DisplayMsgBox (const HWND   hWnd,
                                       const DWORD  StringId,
                                       const WORD   MsgType,
                                       const WORD   ButtonsDisplay,
                                       const WORD   ButtonDefault,
                                       const WORD   * pButtonPressed,
                                       const TCHAR  * pParam1 = NULL,
                                       const TCHAR  * pParam2 = NULL,
                                       const TCHAR  * pParam3 = NULL,
                                       const TCHAR  * pParam4 = NULL,
                                       const TCHAR  * pParam5 = NULL);

NLS_API WORD WINAPI NLS_SplitString (const TCHAR * pInputString,
                                     TCHAR       * pParam1,
                                     TCHAR       * pParam2 = NULL,
                                     TCHAR       * pParam3 = NULL,
                                     TCHAR       * pParam4 = NULL,
                                     TCHAR       * pParam5 = NULL);

#endif // __NLS_API_H