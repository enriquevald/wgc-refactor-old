//-----------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//-----------------------------------------------------------------------------
// 
//   MODULE NAME : CommonDef.h
// 
//   DESCRIPTION : Common includes for all modules. All modules (CPP files) have to
//                 include this file.
// 
//                 This file will include all common header files and those that are 
//                 explicitly defined, for example:
//
//                   #define INCLUDE_COMMON_MISC
//                   #include "Common_Def.h"
//
//        AUTHOR : Alberto Cuesta
// 
// CREATION DATE : 11-MAR-2002
// 
// REVISION HISTORY
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 11-MAR-2002 ACC    Initial draft.
// 
//------------------------------------------------------------------------------
#ifndef __COMMONDEF_H
#define __COMMONDEF_H

//------------------------------------------------------------------------------
// STANDARD INCLUDES
//------------------------------------------------------------------------------
#ifdef INCLUDE_COMMON_CRYPT
  // - INCLUDE_COMMON_CRYPT means include "cryptlib.h" (CommonCrypt.dll)
  // - NOCRYPT means do NOT include "wincrypt.h" (automatically included when 
  //   including "windows.h" <= "CommonDefStd.h").
  // - "cryptlib.h" and "wincrypt.h" can't both be used at the same time due 
  //   to conflicting type names.
  #define NOCRYPT
#endif

#include "CommonDefStd.h"
#include "CommonDefAPI.h"

#include "CommonConstantsCAT.h"
#include "CommonConstants.h"

#include "CommonTypesCAT.h"
#include "CommonTypes.h"

#include "VersionControl.h"
#include "CommonBase.h"

#if defined (INCLUDE_BATCH_TRX_MGR) || defined (INCLUDE_ONLINE_TRX_MGR)
  #define INCLUDE_TRX_MGR
#endif

#ifdef INCLUDE_TRX_MGR
//////////////////////////////////  #define INCLUDE_PROTOCOL_LKC
#endif

// Common API modules
#ifdef INCLUDE_API
  #define INCLUDE_COMMON_API
  #define INCLUDE_QUEUE_API
#endif

// NIR 26-02-03
// Dependencies
#ifdef INCLUDE_PROTOCOL_LKC
  #define INCLUDE_COMMON_CRYPT
#endif

#ifdef INCLUDE_ALARMS
  #define INCLUDE_NLS_API
#endif

#ifdef INCLUDE_COMMON_CRYPT
  #include "CommonCrypt.h"
#endif

#ifdef INCLUDE_VERSION_CONTROL
  #include "VersionControl.h"
#endif

#ifdef INCLUDE_COMMON_DATA
  #include "CommonData.h"
#endif

#ifdef INCLUDE_COMMON_MISC
  #include "CommonMiscDbConstants.h"
  #include "VersionControl.h"
  #include "CommonMisc.h"
#endif

#ifdef INCLUDE_QUEUE_API
  #include "Queue_API.h"
#endif

#ifdef INCLUDE_COMMON_API
  #include "Common_API.h"
#endif

#ifdef INCLUDE_APPLICATION_LOG
  #include "AppLog.h"
#endif

#ifdef INCLUDE_PRIVATE_LOG
  #include "PrivateLog.h"
#endif 

#ifdef INCLUDE_NLS_API
  #include "NLS_Common.h"
  #include "NLS_API.h"
#endif

#ifdef INCLUDE_AUDITOR
  #include "Auditor.h"
#endif

#ifdef INCLUDE_IPC_API
  #include "IPC_API.h"
#endif

#ifdef INCLUDE_LICENSE
  #include "License.h"
#endif

#ifdef INCLUDE_SERVICE
  #include "Service.h"
#endif

#ifdef INCLUDE_SERVICE_TRX_MGR
  #include "Queue_API.h"
  #include "Service.h"
  #include "Protocol_LKC_Trx.h"
  #include "IPC_API.h"
  #include "ServiceTrxMgr.h"

#endif

#ifdef INCLUDE_ALARMS
  #include "Alarms.h"
#endif

#ifdef INCLUDE_JACKPOT
  #include "Jackpot.h"
#endif

#ifdef INCLUDE_TRANSPORT_API
  #include "Transport_API.h"
#endif

#ifdef INCLUDE_WATCHDOG_API
  #include "Watchdog_API.h"
#endif

#ifdef INCLUDE_LKC2AS_API
  #include "LKC2AS_API.h"
#endif

#ifdef INCLUDE_RESOURCE_MONITOR
  #include "ResourceMonitor.h"
#endif

#ifdef INCLUDE_PROTOCOL_LKC
  #include "Protocol_LKC_Trx.h"
#endif

#ifdef INCLUDE_TRX_MGR
  #include "IPC_API.h"
  #include "ServiceTrxMgr.h"
  #include "TrxMgr.h"
#endif

#ifdef INCLUDE_BATCH_TRX_MGR
  #include "BatchTrxMgr.h"
#endif

#ifdef INCLUDE_SOFTWARE_DOWNLOAD_MGR
  #include "SoftwareDownloadMgr.h"
#endif

#ifdef INCLUDE_COMMUNICATIONS
  #include "Communications.h"
#endif

#ifdef INCLUDE_MCW_API
  #include "MCW_API.h"
#endif

#endif // __COMMONDEF_H