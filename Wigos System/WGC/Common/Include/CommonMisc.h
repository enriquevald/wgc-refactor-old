//------------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : CommonMisc.h
// 
//   DESCRIPTION : Constants, types, variables definitions and prototypes 
//                 for CommonMisc
// 
//        AUTHOR : Alberto Cuesta
// 
// CREATION DATE : 08-MAR-2002
// 
// REVISION HISTORY :
// 
// Date        Author Description
// ----------- ------ -----------------------------------------------------------
// 08-MAR-2002 ACC    Initial draft.
// 27-AUG-2002 JSV    Add start_valid_date to host_user.
// 05-SEP-2002 JSV    Add Gui Forms Structs.
// 10-SEP-2002 APB    Added routines and declarations for delaing with message queues.
//
//------------------------------------------------------------------------------

#ifndef __COMMONMISC_H
#define __COMMONMISC_H

//------------------------------------------------------------------------------
//  INCLUDES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PUBLIC CONSTANTS 
//------------------------------------------------------------------------------

// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the COMMONMISC_API_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// COMMONMISC_API_EXPORTS functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.

#ifdef COMMONMISC_API_EXPORTS
#define COMMONMISC_API __declspec(dllexport)
#else
#define COMMONMISC_API __declspec(dllimport)
#endif

#define Common_MB2WC(mb_str, len_mb, wc_str, len_wc) Common_MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, mb_str, (int) len_mb, wc_str, (int) len_wc)
#define Common_WC2MB(wc_str, len_wc, mb_str, len_mb) Common_WideCharToMultiByte(CP_ACP, MB_PRECOMPOSED, wc_str, (int) len_wc, mb_str, (int) len_mb)

// File constants
#define COMMON_BUFFER_READ_SIZE             255

// Get Raw Fields (generic oracle datatype)
#define COMMON_LEN_RAW_FIELD                32

// Configuration constants
#define COMMON_ODBC_STRING_SIZE             80
#define COMMON_DB_PASSWORD_SIZE             20
#define COMMON_DB_FULL_USER_SIZE            21
#define COMMON_DB_DATE_SIZE                 21
#define COMMON_MAX_PATH_SIZE                _MAX_PATH // Visual C++ constant

// User Info
#define COMMON_FULL_NAME_USER_SIZE          20

#define COMMON_MAX_GUI_FORMS                50
#define COMMON_MAX_HIERARCHY_LEVELS         4

#define COMMON_MAX_AGENCY_TERMINALS         100
#define COMMON_MAX_AGENCY_USERS             100

// Indexed nodes
#define IPC_NODE_MAX_INDEX                  100     // Indexed nodes: 00-99

#define MCW_WRITE_MAX_DATA_LENGTH           256
#define MCW_MAX_MODEL_NAME_SIZE              20

// Other stuff
//  - Transaction Managers
//    - The name of any transaction manager is: TRX_MGR_xx, where xx is the TrxType
#define IpcBuildTrxMgrNodeName(TrxType, pNodeName) \
        { _stprintf (pNodeName, _T("%s_%02ld"), IPC_NODE_NAME_TRX_MGR, (DWORD) TrxType); }

//  - Watchdogs
//    - The name of the watchdog is always: WDOG_nnnn, where nnnn is the related node.
#define IpcBuildWatchdogNodeName(pNodeName, pWatchdogName) \
        { _stprintf (pWatchdogName, _T("%s_WDOG"), pNodeName); }

//  - Indexed node names (CommMgr, TrxMgr, ...)
//    - Some of the Ipc Node doesn't have instances
#define IpcBuildNodeName(pBaseNodeName, NodeIndex, pNodeName) \
        { if ( (NodeIndex >= 0) && (NodeIndex < IPC_NODE_MAX_INDEX) ) \
          {\
            _stprintf (pNodeName, _T("%s_%02ld"), pBaseNodeName, (DWORD) NodeIndex); \
          }\
          else \
          {\
            _stprintf (pNodeName, _T("%s_??"), pBaseNodeName); \
          }\
        }

// Macros encapsulating the call to Common_LoggerMsg and formatting its 
// predefined parameters (_log_param1, _log_param2, _log_param3)
#define Common_LoggerParam1(FormatParam, Param)         _stprintf (_log_param1, FormatParam, Param)
#define Common_LoggerParam2(FormatParam, Param)         _stprintf (_log_param2, FormatParam, Param)
#define Common_LoggerParam3(FormatParam, Param)         _stprintf (_log_param3, FormatParam, Param)

#define Common_Logger(MsgId, Param1, Param2, Param3)              Common_LoggerMsg (MsgId, MODULE_NAME, _T(__FUNCTION__), Param1, Param2, Param3, NULL)
#define Common_LoggerEx(MsgId, Param1, Param2, Param3, ExtraMsg)  Common_LoggerMsg (MsgId, MODULE_NAME, _T(__FUNCTION__), Param1, Param2, Param3, ExtraMsg)

// Macros encapsulating the call to Common_LoggerMsg to log an error checking the control block
#define Common_LoggerCB(ControlBlock, ControlBlockType) \
        { \
          TYPE_LOGGER_PARAM     _log_param1; \
          TYPE_LOGGER_PARAM     _log_param2; \
          \
          Common_LoggerParam1 (_T("%hu"), sizeof (ControlBlockType)); \
          Common_LoggerParam2 (_T("%hu"), ControlBlock); \
          Common_Logger (NLS_ID_LOGGER(3), _T("CONTROL_BLOCK"), _log_param1, _log_param2); \
        } 

// Macros to set the control block
#define Common_SetCB(ControlBlock, ControlBlockType) \
        ControlBlock = sizeof(ControlBlockType)

// #define Common_SetCB2(ControlBlock, ControlBlockType1, ControlBlockType2) \
//         ControlBlock = sizeof(ControlBlockType1) + sizeof(ControlBlockType2)

// Macros to check the control block
#define Common_CheckCB(ControlBlock, ControlBlockType, ErrorReturnCode) \
        { \
          if ( ControlBlock != sizeof (ControlBlockType) ) \
          { \
            Common_LoggerCB (ControlBlock, ControlBlockType); \
            return ErrorReturnCode; \
          } \
        } 


enum ENUM_BOOK_AUDIT_OPERATION
{
  BOOK_AUDIT_OP_UNKNOWN         = 0,
  BOOK_AUDIT_OP_SERIES_CREATE   = 11,
  BOOK_AUDIT_OP_SERIES_DELETE   = 12,
  BOOK_AUDIT_OP_SERIES_ACTIVATE = 13,
  BOOK_AUDIT_OP_SERIES_WITHDRAW = 14,
  BOOK_AUDIT_OP_SERIES_BUY      = 15,
  BOOK_AUDIT_OP_BOOK_ASSIGNED   = 21,
  BOOK_AUDIT_OP_BOOK_WITHDRAWN  = 22,
  BOOK_AUDIT_OP_BOOK_SOLD       = 23,
  BOOK_AUDIT_OP_BOOK_DEASSIGNED = 24,
  BOOK_AUDIT_OP_TICKET_SALE     = 31
};

//------------------------------------------------------------------------------
//  PUBLIC DATATYPES 
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
// Get Raw Fields (generic oracle datatype)
typedef struct
{
  unsigned short len;
  unsigned char  arr [COMMON_LEN_RAW_FIELD];

} HOST_RAW_DEF;

typedef struct
{
  TCHAR          user [COMMON_DB_USER_SIZE + 1]; 
  TCHAR          password [COMMON_DB_PASSWORD_SIZE + 1];
  TCHAR          connect_string [COMMON_DB_CONNECT_STRING_SIZE + 1]; 

} TYPE_CENTER_CONFIGURATION_DB_EX;

typedef struct
{
  CONTROL_BLOCK                     control_block;
  TYPE_CENTER_CONFIGURATION_DB_EX   database;
  DWORD                             language;
  
} TYPE_CENTER_CONFIGURATION_EX;

// User Information
typedef struct
{
  DWORD             control_block;
  DWORD             user_id;
  DWORD             profile_id;
  DWORD             enabled;
  DWORD             h_inst_id;
  TYPE_DATE_TIME    start_valid_date;
  TYPE_DATE_TIME    last_changed;
  DWORD             password_exp;
  DWORD             pwd_chg_req;
  DWORD             login_failures;
  DWORD             priv_level;
  TCHAR             full_name [COMMON_FULL_NAME_USER_SIZE + 1];  // 20 + 1
  TCHAR             filler_0 [3];  // 20 + 1 + 3 = 24
  TYPE_DATE_TIME    current_date;   

} TYPE_USER_INFO;

// Hierarchy Information
typedef struct
{
  DWORD    control_block;
  DWORD    instance_id;
  DWORD    extern_id;
  DWORD    tree_level;
  DWORD    parent_h_inst;
  TCHAR    * p_name;           // 20 + 1

} TYPE_HIERARCHY_INFO;

// User Permissions Information
typedef struct
{
  DWORD    control_block;
  DWORD    form_id;
  DWORD    read_perm;
  DWORD    write_perm;
  DWORD    delete_perm;
  DWORD    execute_perm;

} TYPE_USER_PERMISSIONS_INFO;


// FORM
typedef struct
{
  DWORD    id;
  DWORD    nls_id;
  DWORD    order;

} TYPE_GUI_FORM_ITEM;

// GUI FORMS
typedef struct
{
  DWORD               control_block;
  DWORD               gui_id;
  TYPE_COMMON_VECTOR  forms_list;

} TYPE_GUI_FORMS_DB;

// Queues
typedef struct
{
  DWORD       control_block;  // Control Block

  SQL_CONTEXT context;        // Context to use

  // INPUT fields
  TCHAR       queue_name [MAX_QUEUE_NAME_LEN + 1 + 1];          // QueueName    len+null+filler: 30 + 1 + 1 = 32
  TCHAR       sub_queue_name [MAX_SUB_QUEUE_NAME_LEN + 1 + 1];  // SubQueueName len+null+filler: 30 + 1 + 1 = 32
  TCHAR       consumer_name [MAX_CONSUMER_NAME_LEN + 1 + 1];    // ConsumerName len+null+filler: 30 + 1 + 1 = 32

  // OUTPUT fields
  //  - ReturnCode
  //  - Int  Fields
  //  - Text Fields
  //  - Nls  Fields

  //  - ReturnCode
  DWORD       return_code;
  //  - Int  Fields
  DWORD       fld_int0;
  DWORD       fld_int1;
  DWORD       fld_int2;
  DWORD       fld_int3;
  DWORD       fld_int4;
  DWORD       fld_int5;
  DWORD       fld_int6;
  DWORD       fld_int7;
  DWORD       fld_int8;
  DWORD       fld_int9;
  //  - Text Fields
  TCHAR       fld_text0 [MAX_QUEUE_MSG_LEN + 1 + 3];
  TCHAR       fld_text1 [MAX_QUEUE_MSG_LEN + 1 + 3];
  TCHAR       fld_text2 [MAX_QUEUE_MSG_LEN + 1 + 3];
  TCHAR       fld_text3 [MAX_QUEUE_MSG_LEN + 1 + 3];
  TCHAR       fld_text4 [MAX_QUEUE_MSG_LEN + 1 + 3];
  //  - Nls  Fields
  DWORD       fld_nls_id;
  TCHAR       fld_nls_param1 [MAX_QUEUE_MSG_LEN + 1 + 3];
  TCHAR       fld_nls_param2 [MAX_QUEUE_MSG_LEN + 1 + 3];
  TCHAR       fld_nls_param3 [MAX_QUEUE_MSG_LEN + 1 + 3];
  TCHAR       fld_nls_param4 [MAX_QUEUE_MSG_LEN + 1 + 3];
  TCHAR       fld_nls_param5 [MAX_QUEUE_MSG_LEN + 1 + 3];
  
} TYPE_DB_QUEUE_MESSAGE;

typedef struct
{
  DWORD   num_nodes;                            // Number of Nodes (CommMgr's, BatchTrxMgr's, ...)
  DWORD   node_index [IPC_NODE_MAX_INDEX];      // Node Index, equivalient to the CpuId
  DWORD   node_inst_id [IPC_NODE_MAX_INDEX];    // Ipc node instance id
  DWORD   wdog_inst_id [IPC_NODE_MAX_INDEX];    // Ipc wdog instance id
  DWORD   is_local [IPC_NODE_MAX_INDEX];        // 0/1 Flag that indicates whether or not is a local node

} TYPE_IPC_NODE_LIST;

typedef struct
{
  DWORD     control_block;
  DWORD     ci_conn_id;              // 1.Batch   2.GUI
  DWORD     h_agency_inst_id;        // Agency Instance Identifier
  DWORD     ci_inst_id;              // Return conn_inst of row connection_instances created

} TYPE_AGENCY_CONNECT;

typedef struct
{
  DWORD     control_block;
  DWORD     h_agency_inst_id;       // Agency Instance Identifier
  DWORD     ci_conn_id;             
  DWORD     ci_inst_id;             // Return conn_inst of row connection_instances created
  DWORD     ci_status;
  DWORD     ci_num_attempts;
  DWORD     cs_max_attempts;
  DWORD     ci_num_trx_cmd;
  DWORD     ci_trx_msg_id;
  DWORD     ci_locked_books;
  //DOUBLE    running_seconds; 
  DOUBLE    seconds_till_next_run; 
  DWORD     ci_trx_cmd;
  DWORD     ci_downloaded_books;
  DWORD     ci_uploaded_books;
  DWORD     ci_cancelled_books;
  TYPE_DATE_TIME   ci_run_at;

} TYPE_AGENCY_CONNECT_STATUS;

typedef struct
{
  DWORD            control_block;
  DWORD            h_agency_inst_id;       // Agency Instance Identifier
  TYPE_DATE_TIME   hi3_last_connection;

} TYPE_LAST_CONNECTION;


// AJQ 09-OCT-2003
typedef struct
{
  DWORD control_block;
  DWORD client_id;    // Client Id.
  DWORD common_build; // Common DB. Software Version.
  DWORD client_build; // Client DB. Software Version.

} TYPE_DATABASE_VERSION;

typedef struct
{
  DWORD   control_block;

  DWORD   series_id;   // Primary Key
  DWORD   book_id;

  ENUM_BOOK_AUDIT_OPERATION   op_code;     // Operation Code
  DWORD   reason;
  
  DWORD   new_book_status;
  DWORD   old_book_status;
  DWORD   new_num_sold;   
  DWORD   old_num_sold;

  DWORD   ag_internal_id; // Related Ag.

} TYPE_BOOK_AUDIT_OPERATION;


// (CLASS_VECTOR can't operate directly with integers)
typedef struct
{
  DWORD   id;
  TCHAR   name [MCW_MAX_MODEL_NAME_SIZE + 1 + 3]; //20 + 1 + 3 = 24

} TYPE_MCW_MODEL;

typedef struct
{
  DWORD               control_block;

  DWORD               num_models;
  TYPE_COMMON_VECTOR  models;

} TYPE_MCW_MODELS_LIST;


typedef struct
{
  DWORD               control_block;

  WORD                card_type;
  WORD                data1_length;
  TCHAR                data1 [MCW_WRITE_MAX_DATA_LENGTH]; // Track 1
  WORD                data2_length;
  TCHAR                data2 [MCW_WRITE_MAX_DATA_LENGTH]; // Track 2
  WORD                data3_length;
  TCHAR                data3 [MCW_WRITE_MAX_DATA_LENGTH]; // Track 3

} TYPE_MCW_WRITE_DATA;

//------------------------------------------------------------------------------
//  PUBLIC DATA STRUCTURES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PUBLIC FUNCTION PROTOTYPES 
//------------------------------------------------------------------------------

//
//  - General
//
COMMONMISC_API WORD WINAPI Common_HexaToByte (TCHAR * pStringHexa,
                                              WORD  LengthHexa,
                                              BYTE  * pByte,
                                              WORD  LengthByte);

COMMONMISC_API WORD WINAPI Common_ByteToHexa (BYTE  * pByte,
                                              WORD  LengthByte,
                                              TCHAR * pStringHexa,
                                              WORD  LengthHexa);

COMMONMISC_API FARPROC WINAPI Common_GetProcAddress (HMODULE ModuleHandle,
                                                     TCHAR   * pFunctionName);

COMMONMISC_API HMODULE WINAPI Common_LoadLibrary (TCHAR   * pFileName);


COMMONMISC_API DWORD WINAPI Common_GetGestorId (DWORD          * pGestorId,
                                                SQL_CONTEXT    pContext);

// APB, 10-SEP-2002
COMMONMISC_API DWORD WINAPI Common_ReadDbQueue (TYPE_DB_QUEUE_MESSAGE * pQueueMessage);

COMMONMISC_API DWORD WINAPI  Common_PermuteVector (void   * pElement, 
                                                   DWORD  NumElements,
                                                   DWORD  SizeElement);

// Logger functions
COMMONMISC_API void WINAPI Common_LoggerMsg (const WORD   MsgId,    
                                             const TCHAR  * pParam1    = _T(""), 
                                             const TCHAR  * pParam2    = _T(""), 
                                             const TCHAR  * pParam3    = _T(""), 
                                             const TCHAR  * pParam4    = _T(""), 
                                             const TCHAR  * pParam5    = _T(""), 
                                             const TCHAR  * pExtraMsg  = _T(""));

COMMONMISC_API DWORD WINAPI Common_GetConfigurationEx (TYPE_CENTER_CONFIGURATION_EX  * pConfigurationEx);

//
//  - File
//
COMMONMISC_API DWORD WINAPI Common_FileRead  (TCHAR  * pFileName, 
                                              void   * pDefaultValue, 
                                              WORD   DefaultLength, 
                                              void   * pOutputBuffer, 
                                              WORD   * pBufferLength);

COMMONMISC_API DWORD WINAPI Common_FileWrite (TCHAR  * pFileName, 
                                              void   * pInputBuffer,  
                                              WORD   BufferLength);

//
//  - Alarms
//
// AJQ, 14-MAY-2002
// AJQ, 02-SET-2002, Common_Alarm now uses the SqlContext to obtain an AlarmId
COMMONMISC_API VOID WINAPI Common_Alarm  (DWORD       ClientId0,
                                          DWORD       ClientId1,
                                          DWORD       ClientId2,
                                          DWORD       Priority,
                                          BOOL        Activate,
                                          SQL_CONTEXT SqlContext,
                                          DWORD       NlsId     = 0,
                                          TCHAR       * pParam1 = _T(""),
                                          TCHAR       * pParam2 = _T(""),
                                          TCHAR       * pParam3 = _T(""),
                                          TCHAR       * pParam4 = _T(""),
                                          TCHAR       * pParam5 = _T(""));

//
//  - IPC
//
// AJQ, 16-MAY-2002
COMMONMISC_API BOOL WINAPI Common_Ipc (WORD FunctionCode);

COMMONMISC_API BOOL WINAPI Common_IpcNodeInit (TCHAR    * pNodeName, 
                                               DWORD    * pNodeInstanceId, 
                                               DWORD    BufferSize = 0L);

COMMONMISC_API BOOL WINAPI Common_IpcGetNodeInstanceId (TCHAR   * pNodeName, 
                                                        DWORD   * pNodeInstanceId, 
                                                        BOOL    LocalInstance = TRUE);

COMMONMISC_API BOOL WINAPI Common_IpcGetNodeInstanceIdList (TCHAR   * pNodeName,
                                                            DWORD   * pNumInstances,
                                                            DWORD   * pCfgInstId,
                                                            DWORD   * pNodeInstId, 
                                                            BOOL    * pLocalInstance);
// AJQ, 12-AUG-2002
COMMONMISC_API BOOL WINAPI Common_IpcGetLocalNodeInstanceId (TCHAR * pNodeBaseName, 
                                                             TCHAR * pNodeName, 
                                                             DWORD * pNodeIndex, 
                                                             DWORD * pNodeInstanceId);

COMMONMISC_API BOOL WINAPI Common_IpcGetAliveNodeInstanceId (TCHAR  * pNodeBaseName, 
                                                             DWORD  NumNodes, 
                                                             DWORD  * pNodeInstanceId);

COMMONMISC_API BOOL WINAPI Common_IpcGetNodeList (TCHAR * pNodeBaseName, TYPE_IPC_NODE_LIST * pIpcNodeList);

COMMONMISC_API BOOL WINAPI Common_IpcGetInstIdData (DWORD NodeInstId, 
                                                    WORD  * pComputerId,
                                                    WORD  * pNodeId);

//COMMONMISC_API BOOL WINAPI Common_IpcReceive (DWORD NodeInstanceId, IPC_OUD_RECEIVE * pMessage);

//
//  - MCW
//
COMMONMISC_API BOOL WINAPI Common_MCW_ModuleInit (TYPE_MCW_MODELS_LIST   * pModels);

COMMONMISC_API BOOL WINAPI Common_MCW_DeviceInit (WORD Model, WORD PortType, WORD PortNumber);

COMMONMISC_API BOOL WINAPI Common_MCW_DeviceWrite (TYPE_MCW_WRITE_DATA  * pWriteData);

COMMONMISC_API BOOL WINAPI Common_MCW_DeviceCancelWrite ();

COMMONMISC_API BOOL WINAPI Common_MCW_DeviceClose ();
//
//  - Version
//
// ACC, 14-MAR-2006
COMMONMISC_API DWORD WINAPI Common_VersionGetModuleVersion (TCHAR             * pModuleName, 
                                                            TYPE_MODULE_ABOUT * pModuleAbout);
// AJQ, 30-JUL-2002
COMMONMISC_API DWORD WINAPI Common_VersionCheckModuleVersion (TCHAR             * pModuleName, 
                                                              TYPE_MODULE_ABOUT * pModuleAbout);

//
//  - MAC
//
COMMONMISC_API DWORD WINAPI  Common_StringToMacAddress (TCHAR                     * pMacString, 
                                                        TYPE_COMMON_MAC_ADDRESS   * pMacAddress);

COMMONMISC_API DWORD WINAPI  Common_MacAddressToString (TYPE_COMMON_MAC_ADDRESS   * pMacAddress,
                                                        TCHAR                     * pMacString);

#endif // __COMMONMISC_H
