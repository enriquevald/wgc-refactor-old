//------------------------------------------------------------------------------
// Copyright � 2003 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : CommonConstants.h
// 
//   DESCRIPTION : Common constants for CENTER modules
// 
//        AUTHOR : Toni Jord�
// 
// CREATION DATE : 13-FEB-2003
// 
// REVISION HISTORY
// 
// Date        Author Description
// ----------- ------ -----------------------------------------------------------
// 13-FEB-2003 TJG    Initial draft.
// 
//------------------------------------------------------------------------------
#ifndef __COMMON_CONSTANTS_H
#define __COMMON_CONSTANTS_H

//------------------------------------------------------------------------------
// INCLUDES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PUBLIC CONSTANTS 
//------------------------------------------------------------------------------
#define DB_STATUS_OK              0
#define DB_STATUS_ERROR           1
#define DB_STATUS_ALREADY_EXISTS  2
#define DB_STATUS_NOT_FOUND       3

// Table HI_LEVEL3
#define HI_LEVEL3_LEN_NAME                20
#define HI_LEVEL3_LEN_ADDRESS             80
#define HI_LEVEL3_LEN_ZIP                 10
#define HI_LEVEL3_LEN_PHONE               11
#define HI_LEVEL3_LEN_PHYS_ADDR           16
#define HI_LEVEL3_LEN_ADD_NET_ADDR        16
#define HI_LEVEL3_LEN_NIF                 20
#define HI_LEVEL3_LEN_LOCATION            20
#define HI_LEVEL3_DISABLED                0
#define HI_LEVEL3_ENABLED                 1
#define HI_LEVEL3_INOPERATIVE             0
#define HI_LEVEL3_OPERATIVE               1

#define PRIZE_PERMISSIONS_NO_PRIZE        0
#define PRIZE_PERMISSIONS_PAY_MAJOR_PRIZE 1

// Table BOOKS
#define MAX_BOOKS                         512

#define BOOK_STATUS_NOT_ACTIVE            0
#define BOOK_STATUS_READY                 1
#define BOOK_STATUS_ASSIGNED              2
#define BOOK_STATUS_SOLD                  3
#define BOOK_STATUS_RETIRED               4

#define SERIES_STATUS_NOT_ACTIVE              0
#define SERIES_STATUS_READY                   1
#define SERIES_STATUS_PARTIALLY_DISTRIBUTED   2
#define SERIES_STATUS_COMPLETELY_DISTRIBUTED  3 
#define SERIES_STATUS_COMPLETELY_SOLD         4
#define SERIES_STATUS_BEING_RETIRED           5
#define SERIES_STATUS_RETIRED                 6
#define SERIES_STATUS_BEING_BOUGHT            7
#define SERIES_STATUS_BEING_DELETED           8
#define SERIES_STATUS_BEING_GENERATED         9

// Book Data
#define NUM_PRIZE_CATEGORIES              20
#define NUM_BYTES_TICKET_PRIZE_INFO       16

// Table TICKETS_LK
#define MAX_TICKETS                       1000
#define NUM_BYTES_PRIZE_DATA              16
#define TICKET_STATUS_NOT_SOLD            0
#define TICKET_STATUS_SOLD                1
#define TICKET_STATUS_RETIRED             2

// Table WINNING_TICKETS_LK
#define WIN_TKT_CASH_STTS_NOT_CASHED      0
#define WIN_TKT_CASH_STTS_CASHED          1

#define WIN_TKT_PRZ_TYPE_NO_PRIZE             0
#define WIN_TKT_PRZ_TYPE_NON_REFUNDABLE       1
#define WIN_TKT_PRZ_TYPE_MINOR_PRIZE          2
#define WIN_TKT_PRZ_TYPE_MAJOR_PRIZE          3
#define WIN_TKT_PRZ_TYPE_JACKPOT_CONTRIBUTION 4
#define WIN_TKT_PRZ_TYPE_JACKPOT              5

#define WIN_TKT_WIN_CAT_NON_WINNING       0
#define WIN_TKT_WIN_CAT_JACKPOT           99

// Table BOOK_KEYS
#define NUM_BYTES_BOOK_ENC_KEY            24
#define NUM_BYTES_ENC_BOOK_ENC_KEY        350 // Should be the same as the one defined in Protocol!!!

// Table SECURITY_KEYS
#define NUM_BYTES_SEC_ENC_KEY             1280 // Has to be same as LK_CRYPT_RSA_PRIV_KEY_DEFAULT_LENGTH
//#define SECURITY_KEY_TYPE_PRIVATE         0
//#define SECURITY_KEY_TYPE_PUBLIC          1

// Table GENERAL_PARAMS
#define GENERAL_PARAMS_LEN_GROUP_KEY      20
#define GENERAL_PARAMS_LEN_SUBJECT_KEY    20
#define GENERAL_PARAMS_LEN_KEY_VALUE      260
#define GENERAL_PARAMS_LEN_MODIFIED_BY    20

// Table GAMES
#define MAX_GAMES                         COMMON_MAX_GAMES
#define GAME_DISABLED                     0
#define GAME_ENABLED                      1
#define GAMES_LEN_NAME                    20
#define GAMES_LEN_AUX_NAME                40

// Table PRICE_LEVELS
#define MAX_PRICE_LEVELS                  5
#define PRICE_LEVELS_LEN_NAME             20
#define PRICE_LEVELS_LEN_DESCRIPTION      40

// Table HI_STOCKS
//#define MAX_HI_STOCKS                     MAX_GAMES * MAX_PRICE_LEVELS 
#define HI_STOCK_FROM_PARENT_FALSE        0
#define HI_STOCK_FROM_PARENT_TRUE         1

// Table LKAS_USERS
#define MAX_LKAS_USERS                    100
#define LKAS_USER_LEN_NAME                20
#define LKAS_USER_NUM_BYTES_PRIVILEGES    2
#define LKAS_USER_NOT_ENABLED             0
#define LKAS_USER_ENABLED                 1
#define LKAS_USER_LEVEL_USER              0
#define LKAS_USER_LEVEL_OPERATOR          1
#define LKAS_USER_LEVEL_SUPERVISOR        2

// Table JACKPOTS
#define JACKPOT_STATUS_FIRST_TIME         0
#define JACKPOT_STATUS_ACTIVE             1
#define JACKPOT_STATUS_READY              2
#define JACKPOT_STATUS_ASSIGNED           3
#define JACKPOT_STATUS_NOTIFIED           4
#define JACKPOT_STATUS_CONCEDED           5
#define JACKPOT_STATUS_CONFIRMED          6
#define JACKPOT_STATUS_IN_DOUBT           7
#define JACKPOT_STATUS_DEASSIGNED         8
#define JACKPOT_STATUS_REDISTRIBUTED      9

// Table HIERARCHY_INSTANCES
#define H_INSTANCES_LEVEL_GESTOR          0
#define H_INSTANCES_LEVEL_OPERATOR        1
#define H_INSTANCES_LEVEL_DISTRIBUTOR     2
#define H_INSTANCES_LEVEL_AGENCY          3

// Table TERMINALS
//#define MAX_TEMRINALS                       100
#define TERMINAL_LEN_SERIAL_NUMBER          20

// Table TRANSACTION_AUDIT
enum ENUM_TRX_AUDIT_PRIORITY
{
    TRX_AUDIT_PRIORITY_ERROR   = 0
  , TRX_AUDIT_PRIORITY_WARNING = 1
  , TRX_AUDIT_PRIORITY_EVENT   = 2
};


// Table LKC_ADDRESSES
#define MAX_LKC_ADDRESSES                   50
#define LKC_ADDRESS_LEN_PHYS_ADDR_DATA      16

// Table CONNECTION_INSTANCES
#define MAX_CONN_INSTS                      10
#define CONN_INST_STATUS_PENDING            0
#define CONN_INST_STATUS_RUNNING            1
#define CONN_INST_STATUS_SUCCESSFUL         2
#define CONN_INST_STATUS_FAILED             3

// Table NOTE_ACCEPTOR_CONFIGURATION
#define NOTE_ACCEPTOR_NUM_CHANNELS          16

// Table SOFTWARE_DOWNLOAD_SCHEDULING
#define SW_DOWNLOAD_SCHEDULING_STATUS_NOT_ACTIVE            0
#define SW_DOWNLOAD_SCHEDULING_STATUS_PENDING               1
#define SW_DOWNLOAD_SCHEDULING_STATUS_RUNNING               2
#define SW_DOWNLOAD_SCHEDULING_STATUS_PARTIALLY_COMPLETED   3
#define SW_DOWNLOAD_SCHEDULING_STATUS_SUCCESSFUL            4
#define SW_DOWNLOAD_SCHEDULING_STATUS_FAILED                5

#define SW_DOWNLOAD_SCHEDULING_TYPE_AUTOMATIC               0
#define SW_DOWNLOAD_SCHEDULING_TYPE_MANUAL                  1

// Table SOFTWARE_DOWNLOAD_INSTANCES
#define SW_DOWNLOAD_INSTANCE_STATUS_NOT_ACTIVE              0         
#define SW_DOWNLOAD_INSTANCE_STATUS_PENDING                 1
#define SW_DOWNLOAD_INSTANCE_STATUS_RUNNING_DWNLD           2
#define SW_DOWNLOAD_INSTANCE_STATUS_RUNNING_UNPACK          3
#define SW_DOWNLOAD_INSTANCE_STATUS_DOWNLOADED              4
#define SW_DOWNLOAD_INSTANCE_STATUS_SUCCESSFUL              5
#define SW_DOWNLOAD_INSTANCE_STATUS_FAILED                  6

#define SW_DOWNLOAD_MAX_FILES                               50
#define SW_MD5_DIGEST_LENGTH                                16

#define COMMON_MAX_LKC_ADDRESSES                            20

// Table Stocks
#define MAX_STOCKS                                          ((MAX_GAMES) * (MAX_PRICE_LEVELS) * 100)

// Table Trx_Audit
#define TRX_AUDIT_LEN_COMPUTER                              30
#define TRX_AUDIT_LEN_NLS_PARAM                             40

// Node names
#define IPC_NODE_NAME_BASE_PORT         _T("BASE_PORT")
#define IPC_NODE_NAME_APP_LOG           _T("APP_LOG")
#define IPC_NODE_NAME_ALARM_MGR         _T("ALARM_MGR")
#define IPC_NODE_NAME_RESOURCE_MONITOR  _T("RSRC_MGR")
#define IPC_NODE_NAME_GUI               _T("GUI")
#define IPC_NODE_NAME_GUI_COMM_MGR      _T("GUI_COMM_MGR")
#define IPC_NODE_NAME_GUI_ALARMS        _T("GUI_ALARMS")
#define IPC_NODE_NAME_GUI_SYSMON        _T("GUI_SYSMON")
#define IPC_NODE_NAME_TRX_MGR           _T("TRX_MGR")
#define IPC_NODE_NAME_COMM_MGR          _T("COMM_MGR")
#define IPC_NODE_NAME_BATCH             _T("TRX_MGR_00")        // Batch Trx. Mgr.
#define IPC_NODE_NAME_ONLINE            _T("TRX_MGR_01")        // Online Trx. Mgr.
#define IPC_NODE_NAME_JACKPOT           _T("TRX_MGR_02")        // Jackpot Trx. Mgr.
#define IPC_NODE_NAME_SOFTWARE          _T("TRX_MGR_03")        // Software Trx. Mgr.

#define MAX_AGENCIES_SYSTEM_WIDE        10000

// Game constants
#define MAX_LEN_GM_NAME               20
#define MAX_LEN_GM_AUX_NAME           40
#define MAX_LEN_COMMISSION_NAME       20
#define MAX_LEN_GAME_TYPE_NAME        20
#define DB_MAX_LEN_PP_NAME            20
#define DB_MAX_LEN_PP_DESCRIPTION     40
#define DB_MAX_PP_CATEGORIES          20
// Price Levels constants
#define MAX_LEN_PL_NAME               40
#define MAX_LEN_PL_DESCRIPTION        40

// Port Types 
#define COMMON_PORT_TYPE_PARALLEL     1
#define COMMON_PORT_TYPE_SERIAL       2
#define COMMON_PORT_TYPE_USB          3
                            
//------------------------------------------------------------------------------
// PUBLIC DATATYPES 
//------------------------------------------------------------------------------

#endif // __COMMON_CONSTANTS_H

