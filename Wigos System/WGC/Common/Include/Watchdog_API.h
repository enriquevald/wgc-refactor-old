//------------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//------------------------------------------------------------------------------
//
//   MODULE NAME: WATCHDOG_API.h
//   DESCRIPTION: Constants, types, variables definitions and prototypes
//                for the WATCHDOG_API
//        AUTHOR: Andreu Juli�
// CREATION DATE: 11-JUL-2002
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ -----------------------------------------------------------
// 11-JUL-2002 AJQ    Initial draft.
//
//------------------------------------------------------------------------------
#ifndef __WATCHDOG_API_H
#define __WATCHDOG_API_H

//------------------------------------------------------------------------------
//  INCLUDES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PUBLIC CONSTANTS
//------------------------------------------------------------------------------
#ifdef WATCHDOG_API_EXPORTS
#define WATCHDOG_API __declspec(dllexport)
#else
#define WATCHDOG_API __declspec(dllimport)
#endif

// Module function codes
#define WDOG_CODE_INIT                  1
#define WDOG_CODE_STOP                  2
#define WDOG_CODE_QUERY_STATUS          3
#define WDOG_CODE_SET_AUTO_QUERY_STATUS 4
#define WDOG_CODE_GET_AUTO_QUERY_STATUS 5

// Module return codes
#define WDOG_STATUS_OK                  0
#define WDOG_STATUS_ERROR               1

// Node status
#define WDOG_NODE_STATUS_UNKNOWN        0
#define WDOG_NODE_STATUS_OK             1
#define WDOG_NODE_STATUS_NOT_RESPONDING 2


// Module constants
#define WDOG_MAX_NUM_NODES              500

#define WDOG_NODE_NAME_LENGTH           IPC_NODE_NAME_LENGTH

//------------------------------------------------------------------------------
//  PUBLIC DATATYPES
//------------------------------------------------------------------------------
// Module Init
typedef struct
{
  CONTROL_BLOCK  control_block;
  TCHAR          wdog_node_name [WDOG_NODE_NAME_LENGTH];

} WDOG_IUD_INIT;

typedef struct
{
  CONTROL_BLOCK  control_block;
  DWORD          wdog_node_inst_id;

} WDOG_OUD_INIT;

// Status query
typedef struct
{
  CONTROL_BLOCK control_block;
  DWORD         wdog_count;                            // Number of nodes
  DWORD         wdog_node_id     [WDOG_MAX_NUM_NODES]; // Watchdog Node instance id
  DWORD         wdog_node_status [WDOG_MAX_NUM_NODES]; // Node status
  DWORD         wdog_ping_time   [WDOG_MAX_NUM_NODES]; // time_t of Ping
  DWORD         wdog_pong_time   [WDOG_MAX_NUM_NODES]; // time_t of Pong
  DWORD         wdog_ping_delay  [WDOG_MAX_NUM_NODES]; // PingPong time (microseconds)
  DWORD         wdog_no_answer   [WDOG_MAX_NUM_NODES]; // Num Times the node doesn't answered
  
} WDOG_IUD_QUERY_STATUS;

typedef WDOG_IUD_QUERY_STATUS WDOG_OUD_QUERY_STATUS;

// AutoQuery
typedef WDOG_IUD_QUERY_STATUS WDOG_IUD_SET_AUTO_QUERY_STATUS;
typedef WDOG_OUD_QUERY_STATUS WDOG_OUD_GET_AUTO_QUERY_STATUS;

//------------------------------------------------------------------------------
//  PUBLIC DATA STRUCTURES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PUBLIC FUNCTION PROTOTYPES
//------------------------------------------------------------------------------

WATCHDOG_API WORD WINAPI Wdog_API (API_INPUT_PARAMS     * pApiInputParams,
                                   API_INPUT_USER_DATA  * pApiInputUserData,
                                   API_OUTPUT_PARAMS    * pApiOutputParams,
                                   API_OUTPUT_USER_DATA * pApiOutputUserData);

#endif // __WATCHDOG_API_H
