// ------------------------------------------------------------------------------------
// Copyright � 2002 Win Systems
// ------------------------------------------------------------------------------------
//
//   MODULE NAME: PRIVATELOG.H
//
//   DESCRIPTION: Constants, types, variables definitions and prototypes for PrivateLog 
//
//        AUTHOR: Alberto Cuesta 
//
// CREATION DATE: 27-MAR-2002
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ -------------------------------------------------------------------
// 27-MAR-2002 ACC    Initial release
// 04-APR-2002 TJG    Only one log file for all. API encapsulates file name and messages.
//                    Therefore messages are referenced with its ID.
// 01-AUG-2002 APB    Added definition for message number 9.
// 05-SET-2002 AJQ    The message is built using the common functions
// --------------------------------------------------------------------------------------

#ifndef __PRIVATE_LOG_H
#define __PRIVATE_LOG_H

// --------------------------------------------------------------------------------------
// PUBLIC CONSTANTS 
// --------------------------------------------------------------------------------------
// Available messages
//
// Id  Message Text                                     Notes
// --  ------------------------------------------------ --------------- 
//  2  Error=%s calling %s.
//  3  Error=%s. Requested=%s. Received=%s.
//  4  NLS message id=%s not found.                     Exclusive for NLS module
//  5  %s%s%s.                                          To display a string
//  6  Unexpected value %s=%s
//  7  Configuration error line=%s.
//  8  Error=%s calling %s: %s.
//  9  Invalid LK System license.

// --------------------------------------------------------------------------------------
// PUBLIC DATATYPES 
// --------------------------------------------------------------------------------------
#define PRIVATELOG_SIZE_PARAM           40
// Maximum Size for the Header of the Log Messages
#define LOG_MESSAGE_HEADER_SIZE 100

// --------------------------------------------------------------------------------------
// PUBLIC DATA STRUCTURES 
// --------------------------------------------------------------------------------------

// --------------------------------------------------------------------------------------
// PUBLIC FUNCTION PROTOTYPES 
// --------------------------------------------------------------------------------------

DWORD LogMsgPrefixDateTime (const TCHAR  * pMessage,
                            const DWORD  BufferSize,
                            TCHAR        * pLogMsg);

DWORD LogMsgText (const TCHAR   * pComputerName,
                  const TCHAR   * pModuleName,
                  const TCHAR   * pFunctionName,
                  const WORD    MsgId,
                  const TCHAR   * pMessage,
                  const TCHAR   * pExtraMessage,
                  const DWORD   BufferSize,
                  TCHAR         * pLogMsg);

void        PrivateLog (const WORD  MsgId,                      // Message Id
                        const TCHAR * pModuleName   = _T(""),   // Module name
                        const TCHAR * pFunctionName = _T(""),   // Function name
                        const TCHAR * pParam3       = _T(""),   // Message's 1st. parameter
                        const TCHAR * pParam4       = _T(""),   // Message's 2nd. parameter
                        const TCHAR * pParam5       = _T(""),   // Message's 3rd. parameter
                        const TCHAR * pExtraMsg     = _T(""));  // Extra Message (Oracle Error)

#endif // __PRIVATE_LOG_H
