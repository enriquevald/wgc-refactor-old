//------------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//------------------------------------------------------------------------------
//
//   MODULE NAME : LKC_LicenseCreate.cpp
//
//   DESCRIPTION : Functions and Methods to create license.
//
//        AUTHOR : Carles Iglesias
//
// CREATION DATE : 01-AUG-2002
//
// REVISION HISTORY :
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-AUG-2002 CIR    Initial draft.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// INCLUDES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <tchar.h>
#include <wtypes.h>
#include <time.h>

#include "License.h"

//------------------------------------------------------------------------------
// PRIVATE CONSTANTS
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE DATATYPES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE DATA STRUCTURES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE FUNCTION PROTOTYPES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE FUNCTIONS IMPLEMENTATION
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE FUNCTIONS IMPLEMENTATION
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PUBLIC FUNCTIONS IMPLEMENTATION
//------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
// PURPOSE : Create data to license file
//
//  PARAMS :
//      - INPUT :
//          - ExpirationDate : License expiration date
//          - LicenseType : 0 -> Never expires
//                          1 -> Expires on date
//          - LicenseId : License indentification number
//          - ClientId :
//
//      - OUTPUT :
//
// RETURNS :
//      - TRUE : License OK
//      - FALSE : Invalid License creation
//
//   NOTES :

BOOL        CreateLicenseFile (time_t               ExpirationDate, 
                               ENUM_LICENSE_TYPE    LicenseType,
                               DWORD                LicenseId,
                               DWORD                ClientId)
{
  TYPE_LICENSE      _license;

  _license.expiration_date = ExpirationDate;
  _license.type            = LicenseType;
  _license.license_id      = LicenseId;
  _license.client_id       = ClientId;

  // Get generation date (to add a non fixed value to the license)
  time (&_license.generation_date);

  // Initialize seed from rand() function.
  srand ((unsigned) time (NULL));

  // Get random numbers for filler fields.
  _license.random_filler_1 = rand ();
  _license.random_filler_2 = rand ();
  _license.random_filler_3 = rand ();
  _license.random_filler_4 = rand ();
  _license.random_filler_5 = rand ();

  return CreateLicense (&_license);
} // CreateLicenseFile
