//-----------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//-----------------------------------------------------------------------------
//
//   MODULE NAME : LKC_License.cpp
//
//   DESCRIPTION : Functions and Methods to license file.
//
//        AUTHOR : Carles Iglesias
//
// CREATION DATE : 01-AUG-2002
//
// REVISION HISTORY :
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-AUG-2002 CIR    Initial draft.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// INCLUDES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <tchar.h>
#include <wtypes.h>
#include <time.h>

#include "License.h"
#include "WGC_LicenseInternals.h"

//------------------------------------------------------------------------------
// PRIVATE CONSTANTS
//------------------------------------------------------------------------------

#define PARAM_CREATE              _T("CREATE")
#define PARAM_VIEW                _T("VIEW")

#define PARAM_NEVER               _T("NEVER")

#define OPERATION_NONE            0
#define OPERATION_CREATE          1
#define OPERATION_VIEW            2

//------------------------------------------------------------------------------
// PRIVATE DATATYPES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE DATA STRUCTURES
//------------------------------------------------------------------------------
DWORD               GLB_Operation;
time_t              GLB_ExpirationDate;
DWORD               GLB_ClientId;
DWORD               GLB_LicenseId;
ENUM_LICENSE_TYPE   GLB_LicenseType;

//------------------------------------------------------------------------------
// PRIVATE FUNCTION PROTOTYPES
//------------------------------------------------------------------------------
void      PrintHelp ();

BOOL      ParseCommandLine (int ArgCount, TCHAR ** pArg);

void      StartProcess ();

//------------------------------------------------------------------------------
//  PRIVATE FUNCTIONS IMPLEMENTATION
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE : Print help if error parameters.
//
//  PARAMS :
//      - INPUT :
//
//      - OUTPUT :
//
// RETURNS :
//
//   NOTES :

void      PrintHelp ()
{
  _tprintf (_T("\n"));
  _tprintf (_T("LKC_License parameters\n"));
  _tprintf (_T("----------------------\n"));
  _tprintf (_T("\n"));
  _tprintf (_T("   OPERATION=%s | %s\n"), PARAM_CREATE,
                                          PARAM_VIEW);
  _tprintf (_T("\n"));
  _tprintf (_T("  EXPIRATION=dd/mm/yyyy | %s\n"), PARAM_NEVER);
  _tprintf (_T("\n"));
  _tprintf (_T("          ID=license_id\n"));
  _tprintf (_T("\n"));
  _tprintf (_T("          CLIENT=client id\n"));
  _tprintf (_T("\n"));

  return;
} // PrintHelp

//------------------------------------------------------------------------------
// PURPOSE : Parse command line parameters.
//
//  PARAMS :
//      - INPUT :
//          - ArgCount: Number of parameters in command line
//          - pArg: Array of parameters in command line
//
//      - OUTPUT :
//
// RETURNS :
//
//   NOTES :

BOOL      ParseCommandLine (int ArgCount, TCHAR ** pArg)
{
  int       _idx_param;
  TCHAR     * p_param;
  TCHAR     _aux_text [MAX_PATH];
  TCHAR     _param [4][25] = {  _T("OPERATION=")
                              , _T("EXPIRATION=")
                              , _T("ID=")
                              , _T("CLIENT=") };
  BOOL      _client_id_specified;
  BOOL      _license_id_specified;
  BOOL      _expiration_date_specified;
  DWORD     _len_id;
  DWORD     _idx_id;

  GLB_ClientId  = -1;
  GLB_Operation = OPERATION_NONE;
  GLB_LicenseId = 0;

  _client_id_specified       = FALSE;
  _license_id_specified      = FALSE;
  _expiration_date_specified = FALSE;

  for ( _idx_param = 1; _idx_param < ArgCount; _idx_param++ )
  {
    p_param = _strupr (pArg[_idx_param]);
    if ( ! memcmp (p_param, _param[0], _tcslen (_param[0])) )
    {   
      // OPERATION
      if ( ! _tcscmp (p_param + _tcslen (_param[0]), PARAM_CREATE) )
      {
        GLB_Operation = OPERATION_CREATE;
      }
      else if ( ! _tcscmp (p_param + _tcslen (_param[0]), PARAM_VIEW) )
      {
        GLB_Operation = OPERATION_VIEW;
      }
      else
      {
        _tprintf (_T("\n\nInvalid parameter value: OPERATION\n\n"));

        return FALSE;
      }
    }
    else if ( ! memcmp (p_param, _param[1], _tcslen (_param[1])) )
    {
      // EXPIRATION DATE
      _expiration_date_specified = TRUE;
      if ( ! _tcscmp (p_param + _tcslen (_param[1]), PARAM_NEVER) )
      {
        GLB_LicenseType = LICENSE_TYPE_NEVER_EXPIRE;
        GLB_ExpirationDate = 0;
      }
      else
      {
        GLB_LicenseType = LICENSE_TYPE_EXPIRE_ON_DATE;
        _tcscpy (_aux_text, p_param + _tcslen (_param[1]));
        if ( ! StringToUTCTime (_aux_text, &GLB_ExpirationDate) )
        {
          _tprintf (_T("\n\nInvalid parameter value: EXPIRATION\n\n"));

          return FALSE;
        }
      }
    }
    else if ( ! memcmp (p_param, _param[2], _tcslen (_param[2])) )
    {   
      // ID
      _license_id_specified = TRUE;
      _tcscpy (_aux_text, p_param + _tcslen (_param[2]));

      // Check string length
      _len_id = _tcslen (_aux_text);
      if ( _len_id > 9 )
      {
        _tprintf (_T("\n\nInvalid parameter value: ID\n\n"));

        return FALSE;
      }

      for ( _idx_id = 0; _idx_id < _len_id; _idx_id++ )
      {
        if ( ! isdigit (_aux_text [_idx_id]) )
        {
          _tprintf (_T("\n\nInvalid parameter value: ID\n\n"));

          return FALSE;
        }
      }

      GLB_LicenseId = _ttol (_aux_text);
    }
    else if ( ! memcmp (p_param, _param[3], strlen (_param[3])) )
    {   
      // CLIENT
      _client_id_specified = TRUE;
      _tcscpy (_aux_text, p_param + _tcslen (_param[3]));

      // Check string length
      _len_id = _tcslen (_aux_text);
      if ( _len_id > 9 )
      {
        _tprintf ("\n\nInvalid parameter value: CLIENT\n\n");

        return FALSE;
      }

      for ( _idx_id = 0; _idx_id < _len_id; _idx_id++ )
      {
        if ( ! isdigit (_aux_text [_idx_id]) )
        {
          _tprintf ("\n\nInvalid parameter value: CLIENT\n\n");

          return FALSE;
        }
      }

      GLB_ClientId = atol (_aux_text);
    }
    else
    {
      _tprintf (_T("\n\nInvalid parameter: %s\n\n"), p_param);

      return FALSE;
    }
  } // for

  // Check operation.
  if ( GLB_Operation == OPERATION_NONE )
  {
    _tprintf (_T("\n\nNo Operation specified.\n\n"));

    return FALSE;
  }

  if ( GLB_Operation == OPERATION_CREATE )
  {
    // Check expiration date.
    if ( ! _expiration_date_specified )
    {
      _tprintf (_T("\n\nNo expiration date specified.\n\n"));

      return FALSE;
    }

    // Check license id.
    if ( ! _license_id_specified )
    {
      _tprintf (_T("\n\nNo license id specified.\n\n"));

      return FALSE;
    }

    // Check client id.
    if ( ! _client_id_specified )
    {
      _tprintf ("\n\nNo client id specified.\n\n");

      return FALSE;
    }
  }

  return TRUE;
} // ParseCommandLine

//------------------------------------------------------------------------------
// PURPOSE : Start process of the license file.
//
//  PARAMS :
//      - INPUT :
//
//      - OUTPUT :
//
// RETURNS :
//
//   NOTES :

VOID      StartProcess ()
{

  switch ( GLB_Operation )
  {
    case OPERATION_CREATE :
      if ( ! CreateLicenseFile (GLB_ExpirationDate, 
                                GLB_LicenseType, 
                                GLB_LicenseId,
                                GLB_ClientId) )
      {
        _tprintf (_T("\n\nError creating license\n\n"));
      }
      else
      {
        // Show license created.
        _tprintf (_T("\n"));
        if ( ! ViewLicenseFile () )
        {
          _tprintf (_T("\nError creating license\n\n"));
        }
        else
        {
          _tprintf (_T("\nLicense file generated.\n\n"));
        }
      }
    break;

    case OPERATION_VIEW :
      _tprintf (_T("\n"));
      if ( ! ViewLicenseFile () )
      {
        _tprintf (_T("\n\nError reading license\n\n"));
      }
    break;

    default :
    break;
  }
  
  return;
} // StartProcess

//------------------------------------------------------------------------------
// PURPOSE : Main function.
//
//  PARAMS :
//      - INPUT :
//          - argc : Number of parameters in command line
//          - argv : Array of parameters in command line
//
//      - OUTPUT :
//
// RETURNS :
//
//   NOTES :

int       main (int argc, TCHAR * argv[])
{
  if ( ! ParseCommandLine (argc, argv) )
  {
    PrintHelp ();

    return 1;
  }

  StartProcess ();

  return 0;
}
