//------------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//------------------------------------------------------------------------------
//
//   MODULE NAME : LKC_LicenseView.cpp
//
//   DESCRIPTION : Functions and Methods to view license.
//
//        AUTHOR : Carles Iglesias
//
// CREATION DATE : 01-AUG-2002
//
// REVISION HISTORY :
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-AUG-2002 CIR    Initial draft.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// INCLUDES
//------------------------------------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <tchar.h>
#include <wtypes.h>
#include <time.h>

#include "License.h"
#include "WGC_LicenseInternals.h"

//------------------------------------------------------------------------------
// PRIVATE CONSTANTS
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE DATATYPES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE DATA STRUCTURES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE FUNCTION PROTOTYPES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE FUNCTIONS IMPLEMENTATION
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE FUNCTIONS IMPLEMENTATION
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PUBLIC FUNCTIONS IMPLEMENTATION
//------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
// PURPOSE : View data from license file
//
//  PARAMS :
//      - INPUT :
//
//      - OUTPUT :
//
// RETURNS :
//      - TRUE : License OK
//      - FALSE : Invalid License information
//
//   NOTES :

BOOL        ViewLicenseFile ()
{
  TYPE_LICENSE  _license;
  TCHAR         _aux_date [100];
  TCHAR         _aux_path [MAX_PATH];

  _aux_path [0] = _T('\0');
  if ( ! ReadLicense (&_license, _aux_path) )
  {
    return FALSE;
  }

  // License information:
  //    - Client
  //    - Id
  //    - Generation date-time
  //    - Type

  //    - Client
  _tprintf (_T("    Client  %09ld\n"), _license.client_id);

  //    - Id
  _tprintf (_T("    License %09ld\n"), _license.license_id);

  //    - Type
  switch ( _license.type )
  {
    case LICENSE_TYPE_NEVER_EXPIRE :
      _tprintf (_T("    Never expires\n"));
    break;

    case LICENSE_TYPE_EXPIRE_ON_DATE :
      //    - Expiration date-time
      UTCTimeToString (&_license.expiration_date, _aux_date);
      _tprintf (_T("    Valid until %s\n"), _aux_date);
    break;

    default :
    break;
  }

  return TRUE;
} // ViewLicenseFile
