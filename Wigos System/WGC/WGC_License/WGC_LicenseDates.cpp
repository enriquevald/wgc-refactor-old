//------------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//------------------------------------------------------------------------------
//
//   MODULE NAME : LKC_LicenseDates.cpp
//
//   DESCRIPTION : Functions and Methods to view license dates.
//
//        AUTHOR : Carles Iglesias
//
// CREATION DATE : 01-AUG-2002
//
// REVISION HISTORY :
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-AUG-2002 CIR    Initial draft.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// INCLUDES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <tchar.h>
#include <wtypes.h>
#include <time.h>

#include "License.h"

//------------------------------------------------------------------------------
// PRIVATE CONSTANTS
//------------------------------------------------------------------------------
#define LEN_LICENSE_DATE    10

//------------------------------------------------------------------------------
// PRIVATE DATATYPES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE DATA STRUCTURES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE FUNCTION PROTOTYPES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE FUNCTIONS IMPLEMENTATION
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PUBLIC FUNCTIONS IMPLEMENTATION
//------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
// PURPOSE : View data from license file
//
//  PARAMS :
//      - INPUT :
//            - pTime: Date to convert
//
//      - OUTPUT :
//            - pTimeString: output string.
//
// RETURNS :
//      - TRUE : License OK
//      - FALSE : Invalid License information
//
//   NOTES :

void        UTCTimeToString (time_t     * pTime,
                             TCHAR      * pTimeString)
{
  struct tm     * p_time_str;

  p_time_str = gmtime (pTime);

  // Check if CTime date is less than Jan 01 1970 
  if ( p_time_str == NULL )
  {
    _tcscpy (pTimeString,_T("01/01/1970 00:00:00"));

    return;
  }
  
  _stprintf (pTimeString, _T("%02d/%02d/%04d %02d:%02d:%02d"), p_time_str->tm_mday,
                                                               p_time_str->tm_mon + 1,
                                                               p_time_str->tm_year + 1900,
                                                               p_time_str->tm_hour,
                                                               p_time_str->tm_min,
                                                               p_time_str->tm_sec);

  return;
} // TimeToString

//-------------------------------------------------------------------------------
// PURPOSE : 
//
//  PARAMS :
//      - INPUT :
//            - pTimeString: Time string to convert.
//
//      - OUTPUT :
//            - pTime: Time string in UTC time mode
//
// RETURNS :
//      - TRUE : Valid date
//      - FALSE : Invalid date
//
//   NOTES :

BOOL        StringToUTCTime (TCHAR     * pTimeString,
                             time_t    * pTime)
{
  TCHAR       _day [20];
  TCHAR       _month [20];
  TCHAR       _year [20];
  DWORD       _day_number;
  DWORD       _month_number;
  DWORD       _year_number;
  struct tm   _tm;
  int         _aux_tm_mdays;
  int         _aux_tm_mon;

  if ( _tcslen (pTimeString) != LEN_LICENSE_DATE )
  {
    * pTime = 0;

    return FALSE;
  } // if

  _day[0] = pTimeString [0];
  _day[1] = pTimeString [1];
  _day[2] = _T('\0');

  _month[0] = pTimeString [3];
  _month[1] = pTimeString [4];
  _month[2] = _T('\0');

  _year[0] = pTimeString [6];
  _year[1] = pTimeString [7];
  _year[2] = pTimeString [8];
  _year[3] = pTimeString [9];
  _year[4] = _T('\0');

  // Get numbers from string.
  _day_number   = _ttoi (_day);
  _month_number = _ttoi (_month);
  _year_number  = _ttoi (_year);

  if (   _day_number < 1 || _day_number > 31
      || _month_number < 1 || _month_number > 12 
      || _year_number < 1900 )
  {
    * pTime = 0;

    return FALSE;
  } // if
  
  // Fill tm structure
  memset (&_tm, 0, sizeof (struct tm));
  _tm.tm_mday = _day_number;
  _tm.tm_mon = _month_number - 1;
  _tm.tm_year = _year_number - 1900;
  // Daylight saving time: -1 to have the C run-time library code compute whether
  //                       standard time or daylight savings time is in effect.
  _tm.tm_isdst = -1;

  _aux_tm_mdays = _tm.tm_mday;
  _aux_tm_mon = _tm.tm_mon;

  // Get UTC time
  * pTime = mktime (&_tm);

  // Make sure that time is UTC
  * pTime -= _timezone;

  // Check day and month convertion
  // if it is not equal then it is invalid date.
  if (   _aux_tm_mdays != _tm.tm_mday
      || _aux_tm_mon != _tm.tm_mon )
  {
    * pTime = 0;

    return FALSE;
  } // if

  // Check return code
  if ( * pTime == (time_t) -1 )
  {
    * pTime = 0;

    return FALSE;
  } // if

  return TRUE;
} // StringToUTCTime
