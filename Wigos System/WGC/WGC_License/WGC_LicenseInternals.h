//------------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//------------------------------------------------------------------------------
//
//   MODULE NAME: WGC_LicenseInternals.h
//
//   DESCRIPTION: Constant, type and function prototype definitions.
//
//        AUTHOR: Carles IGlesias
//
// CREATION DATE: 01-AUG-2002
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ -----------------------------------------------------------
// 01-AUG-2002 CIR    Initial draft.
//-------------------------------------------------------------------------------

#ifndef __LKLICENSEINTERNALS_H
#define __LKMGLICENSEINTERNALS_H

//------------------------------------------------------------------------------
// PUBLIC CONSTANTS
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PUBLIC DATATYPES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PUBLIC DATA STRUCTURES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PUBLIC FUNCTION PROTOTYPES
//------------------------------------------------------------------------------

void        UTCTimeToString (time_t     * pTime,
                             TCHAR      * pTimeString);

BOOL        StringToUTCTime (TCHAR       * pTimeString,
                             time_t      * pTime);

BOOL        CreateLicenseFile (time_t               ExpirationDate, 
                               ENUM_LICENSE_TYPE    LicenseType,
                               DWORD                LicenseId,
                               DWORD                ClientId);

BOOL        ViewLicenseFile (void);


#endif // __LKLICENSEINTERNALS_H

