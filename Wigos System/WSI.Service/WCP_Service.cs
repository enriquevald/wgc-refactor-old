//------------------------------------------------------------------------------
// Copyright � 2009 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: StandardService.cs
// 
//   DESCRIPTION: Procedures for WCP to run as a service
// 
//        AUTHOR: Agust� Poch (Header only)
// 
// CREATION DATE: 28-APR-2009
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 28-APR-2009 APB    First release (Header).
// 15-MAR-2012 SSC    Added Opening/ Closing CashierSession for MoneyEvent
// 15-NOV-2013 JBP    Added SASMeters thread. 
// 18-FEB-2014 RMS & RCI    Added HistoricalData thread. 
// 11-MAR-2014 SMN    Added historical old cashier sessions thread
// 12-AUG-2015 AMF    Fixed Bug WIG-2635: Only load bigincrement data in WCP service
// 26-JAN-2016 FGB    Added Customer Buckets Expiration by Day/Month processing
// 28-JUN-2016 ETP    Added Pin Pad Undo for cancelling
// 19-SEP-2016 FGB    PBI 15985: Exped System: Cashier: Check if the payment/sale can be done
// 30-NOV-2016 RAB    PBI 21157: Withholding generation - Thread.
// 18-JUL-2017 ETP    WIGOS-3515 New Jackpots option menu appears in wrong place and with wrong description.
// 23-AUG-2017 DPC    WIGOS-4620: WRKP - Service implementation - Create project self host
// 07-SEP-2017 AGP    Bug 29637:Terminal draw. Error calling Web Service  
// 03-OCT-2017 RAB    PBI 29842: MES13 Specific alarms: Gaming table player sitting time
// 17-OCT-2017 ETP    PBI 29415:WRKP TITO - WIGOS Redemption Kiosk Protocol
// 13-NOV-2017 DHA    Bug 30775:WIGOS-6110 [Ticket #9672] MTY I - Plaza Real - PinPad con error y autorizadas
// 02-FEB-2018 OMC    Added new thread to toggle triggers related to services based on GeneralParam enabled.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ServiceProcess;
using System.Configuration.Install;
using System.ComponentModel;
using System.Reflection;
using System.IO;
using System.Diagnostics;
using System.Net;
using System.Security.Cryptography;
using AccountCommon;
using WSI.Common;
using WSI.Entrances.BlackList;
using WSI.WCP;
using CashIO;
using EventInterchange;
using WSI.Common.PinPad;
using WS2S_Listener;
using SessionInterchange;
using WigosEGMConsumer;
using AccountInterchange;
using Common;
using EventCommon;
using MeterCommon;
using MeterInterchange;
using Microsoft.Practices.Unity;
using SessionCommon;
using WigosHubCommon;
using ContainerControlledLifetimeManager = Microsoft.Practices.Unity.ContainerControlledLifetimeManager;
using ILogger = WigosHubCommon.ILogger;
using WSI.PinPad.Service;
using ExpedBridge;
using WSI.Common.ExternalPaymentAndSaleValidation;
using WSI.CFDI;
using System.ServiceModel;
using System.ServiceModel.Description;
using JackpotViewerWS.Listener;
using JackpotViewerWS.Service;
using WSI.Common.Jackpot;
using KioskRedemptionWebApi;
using WSI.Mobibank.Request;

public partial class WCP_Service : CommonService
{
  private const string VERSION = "18.608";
  private const string SVC_NAME = "WCP_Service";
  private const string PROTOCOL_NAME = "WCP";
  private const int SERVICE_PORT = 13000;
  private const Boolean STAND_BY = false;
  private const string LOG_PREFIX = "WCP";
  private const string CONFIG_FILE = "WSI.Configuration";
  private const ENUM_TSV_PACKET TSV_PACKET = ENUM_TSV_PACKET.TSV_PACKET_WCP_SERVICE;
  private List<IDisposable> StaticHosts;

  #region " Members "

    private JackpotViewerListener m_jackpot_viewer_listener;

  #endregion

  public static void Main(String[] Args)
  {
    WCP_Service _this_service;

    RegisterDependenciesInIoC();

    _this_service = new WCP_Service();
    _this_service.Run(Args);
  }

  private static void RegisterDependenciesInIoC()
  {
    IoC.Container.RegisterInstance<ILogger>(new WigosLogger(), new ContainerControlledLifetimeManager());
    IoC.Container.RegisterType<IFileSystem, FileSystem>();
    IoC.Container.RegisterType<IFileSystemDirectory, FileSystemDirectory>();
    IoC.Container.RegisterType<IInterchangeSession, SessionInterchange.SessionInterchange>();
    IoC.Container.RegisterType<IInterchangeEvent, InterchangeEvent>();
    IoC.Container.RegisterType<IInterchangeMeter, InterchangeMeter>();
    IoC.Container.RegisterType<IInterchangeAccount, AccountInterchange.AccountInterchange>();
    WigosEGMConsumer.WigosEGMConsumer.Init(IoC.Container);
  }

  // Main

  protected override void SetParameters()
  {
    m_service_name = SVC_NAME;
    m_protocol_name = PROTOCOL_NAME;
    m_version = VERSION;
    m_default_port = SERVICE_PORT;
    m_stand_by = STAND_BY;
    m_tsv_packet = TSV_PACKET;
    m_log_prefix = LOG_PREFIX;
    m_old_config_file = CONFIG_FILE;
    BigIncrementsData.LoadData = true;
    OperationVoucherParams.LoadData = true;

  } // SetParameters

  public static bool LogException(string message, Exception Ex)
  {
    Log.Error(message);
    Log.Exception(Ex);
    return true;
  }

  /// <summary>
  /// Starts the service
  /// </summary>
  /// <param name="ipe"></param>
  protected override void OnServiceRunning(IPEndPoint Ipe)
  {
    //Hack to kill blacklistinternal
    if (File.Exists("BlackListConnectorInternal.dll"))
    {
      try
      {
        File.Move("BlackListConnectorInternal.dll", "_lackListConnectorInternal._ll");
      }
      catch (Exception)
      {
        Log.Message("Renamed BlackListConnectorInternal.dll to _lackListConnectorInternal._ll failed");
        //swallow
      }
    }
    if (File.Exists("_lackListConnectorInternal._ll"))
    {
      try
      {
        File.Delete("_lackListConnectorInternal._ll");
      }
      catch (Exception)
      {
        Log.Message("Deleting _lackListConnectorInternal._ll failed");
        //swallow
      }
    }
    //end hack to kill blacklist internal
    
    WCP_DirectoryService _ds;
    IXmlSink _executor;
    WCP_Server _server;

    DbCache.Init();
    DbCache.Start();

    //Start Users Service: User Account Expiration, ...
    WCP_Users.Init();
    WCP_TerminalGroups.Init(); // Exploit terminals

    // Start Buckets thread
    WCP_Buckets.Init();

    // Start Play Statistics
    StatisticsPerHour.Init();

    WCP_MonitorData.Init();

    // Init Wcp Commands thread
    WCP_Commands.Init();

    // Start Credits Expiration processing
    WCP_AccountsCreditsExpiration.Init();
    WCP_AccountsCreditsExpiration.Start();

    // Start generation withholding
    WCP_Withholding.Init();

    // Start Customer Buckets Expiration by Day/Month processing
    WCP_BucketsExpirationDayMonth.Init();
    WCP_BucketsExpirationDayMonth.Start();

    if (WCP_BusinessLogic.CashlessMode == CashlessMode.ALESIS)
    {
      Alesis.Init();
    }

    if (Misc.IsPinPadEnabled() && InternetConnection.RequiredInternetConnection)
    {
      WCP_InternetConnection.Init();
      WCP_InternetConnection.Start();
    }

        StaticHosts = new List<IDisposable>();
    try
    {
      //AGP 07/Sep/2017: Modified on Sept, 7 to allow easier debugging

      string[] _swh_folders = GeneralParam.GetString("StaticWebHost", "Folders", "").Split(',');
      string[] _swh_ports = GeneralParam.GetString("StaticWebHost", "Ports", "").Split(',');
      //string[] _swh_folders = GeneralParam.GetString("StaticWebHost", "Folders", "C:\\lotterywebsite\\").Split(',');
      //string[] _swh_ports = GeneralParam.GetString("StaticWebHost", "Ports", "9998").Split(',');
      if (_swh_ports.Length > 0 && _swh_ports.Length == _swh_folders.Length)
      {
        Log.Message("Starting Embedded Webservers...");
        for (int _i = 0; _i < _swh_ports.Length; _i++)
        {
          int _port = 0;
          if (int.TryParse(_swh_ports[_i], out _port))
          {
              WSI.Hosting.StaticHtmlServerinstance myStaticHost;
              IDisposable myStaticHostInterface;
              string strFilePath;
              string strLocalIpAddress;
              Boolean bRestrictSubnet;
              string strSubnetRestrictionMask;

              strFilePath = _swh_folders[_i];
              strLocalIpAddress = Misc.GetLocalIpAddress();
              bRestrictSubnet = GeneralParam.GetBoolean("StaticWebHost", "RestrictSubnet", false);
              strSubnetRestrictionMask = GeneralParam.GetString("StaticWebHost", "SubnetRestrictionMask", "255.255.255.0");

                        myStaticHost = new WSI.Hosting.StaticHtmlServerinstance(strFilePath,  //File path
                                                                       strLocalIpAddress,
                                                                       _port,
                                                                       LogException, //Delegate function that writes a log
                                                                       bRestrictSubnet,
                                                                       strSubnetRestrictionMask
                                                                      );
              if (myStaticHost != null)
              {
                  myStaticHostInterface = myStaticHost.StartServer();
                  if (myStaticHostInterface == null)
                  {
                      Log.Message("Could not start server:");
                      Log.Message("File path:" + strFilePath);
                      Log.Message("IPAddress=" + strLocalIpAddress + " port:" + _port.ToString());
                      Log.Message("Restrict Subnet:" + bRestrictSubnet.ToString());
                      Log.Message("Restrictuion mask:" + strSubnetRestrictionMask);
                  }
                  else
                  {
                      StaticHosts.Add(myStaticHostInterface);
                      Log.Message(string.Format("   ...Started Embedded Webserver for folder {0} at http://{1}:{2}/ ", _swh_folders[_i], Misc.GetLocalIpAddress(), _port));
                  }
              }
              else //Constructor of StaticHost has failed:
              {
                  Log.Message("Constructor of myStaticHost returns null");
                  Log.Message("File path:" + strFilePath);
                  Log.Message("IPAddress=" + strLocalIpAddress + " port:" + _port.ToString());
                  Log.Message("Restrict Subnet:" + bRestrictSubnet.ToString());
                  Log.Message("Restrictuion mask:" + strSubnetRestrictionMask);
              }
            /*************************************
            StaticHosts.Add(
              new WSI.Hosting.StaticHtmlServerinstance(
                _swh_folders[_i],
                Misc.GetLocalIpAddress(),
                _port, LogException,
                GeneralParam.GetBoolean("StaticWebHost", "RestrictSubnet", false),
                GeneralParam.GetString("StaticWebHost", "SubnetRestrictionMask", "255.255.255.0")
                ).StartServer());
            Log.Message(string.Format("   ...Started Embedded Webserver for folder {0} at http://{1}:{2}/ ", _swh_folders[_i], Misc.GetLocalIpAddress(), _port))
             * ***********************************/
            ;
          }
        }
        Log.Message("...Finished.");
      }
    }
    catch (Exception _ex)
    {
      Log.Error("Error in StaticWebHost General Params");
      Log.Error(string.Format("(Folder:{0}),(Ports:{1})", GeneralParam.GetString("StaticWebHost", "Folders", ""), GeneralParam.GetString("StaticWebHost", "Ports", "")));
      Log.Exception(_ex);
    }
    // Init BatchUpdate and Cache modules
    WSI.Common.BatchUpdate.TerminalSession.Init("WCP",
                                                (Int32)WCP_SessionStatus.Opened,
                                                (Int32)WCP_SessionStatus.Abandoned,
                                                (Int32)WCP_SessionStatus.Disconnected,
                                                (Int32)WCP_SessionStatus.Timeout);

    // Start Connected Terminals processing
    WCP_TerminalsConnected.Init();
    WCP_TerminalsConnected.Start();

    PlaySession.Init();
    WCP_BU_GameMeters.Init();
    WCP_BU_UpdateCardSession.Init();
    WCP_AccountManager.Init();
    //Common.BatchUpdate.Messages.Init("WCP");

    WCP_WKT.WKT_Advertisement.Init();

    // RCI 14-JAN-2013
    WCP_WSP.Init();

    // RMS & RCI 18-FEB-2014
    HistoricalData.Init();

    // JPJ 20-MAY-2014
    Patterns.Init();

    // SMN 11-MAR-2014
    HistoricalCashierSessions.Init();

    WCP_MultiSiteRequests.Init();

    SASMeters.Init();

    if (WSI.Common.TITO.Utils.IsTitoMode())
    {
      //Start SASMeters thread
      TITO_TasksThread.Init();
    }

    PlayerTracking_CalculatePlays.Init();
        PlayerTracking_CalculatePlayerSittingTime.Init(SVC_NAME);

    //ExternalPaymentAndSaleValidation (registering transactions)
    WCP_ExternalValidation.Init();

    WCP_WorkerPool.Init();

    _executor = new WCP_XmlSink();

    _server = new WCP_Server(Ipe);

    _server.Sink = _executor;

    WSI.WCP.WCP.Server = _server;

    _server.Start();

    // Start directory Service
    _ds = new WCP_DirectoryService(Ipe);

    Log.Message("");
    Log.Message("WCP_Service listening on: " + Ipe.ToString());
    Log.Message("Directory Service says:   " + Ipe.ToString());
    Log.Message("");
    Log.Message("");

    // Start: Exploid LCD Message
    WSI.Common.LCDMessages.Init();

    // Start: Expire points Day Month
    // AMF 31-AUG-2015: BackLog 3685
    // FGB 10-MAR-2016: Changed the AccountPointExpiration by BucketsExpiration
    //WCP_AccountsPointsExpirationDayMonth.Init();

    //Historified Terminals, Sites and Players
    WCP_HistorificationTasks.Init();

    //Buckets update
    BucketsUpdate.Init();

    try
    {
      //Undo Timeout transactions.
      if (Misc.IsPinPadEnabled())
      {
        Log.Message("Initializing PinPadUndo.Init()...");
        PinPadUndo.Init();
        Log.Message("...Initialized.");
      }
    }
    catch (Exception _ex)
    {
      Log.Message("!!!Error Initializing.");
      Log.Message("----------------------------------------------------------");
      Log.Exception(_ex);
      Log.Message("----------------------------------------------------------");
    }


    // Start: GameGateway start Listening
    // AMF 23-NOV-2015: Feature 4699:LottoRace
    LCD_GameGateway.LCD_WebServer.Init();

    // Start: Black list service 
    // SJA 18-FEB-2016 PBI 9046
    if (Misc.IsReceptionEnabled())
    {
      new BlackListProxyServiceHost().Init();
    }

    // FGB 03-OCT-2016 Start Exped
    if (Misc.IsExpedEnabled())
    {
      new ExpedProxyHost().Init(ExternalPaymentAndSaleValidationCommon.ExpedServerGroupKey);
    }

    if (Misc.IsWS2SEnabled())
    {
      //register ws2s_listener dependencies
      WS2S_Listener.WS2S_Listener.Init(IoC.Container);
      //start services - construction starts hosted services
      IoC.Container.ResolveAll<IWHUBListener>();
    }

        // JBP 03-JUN-207: Start JackpotViewer WS
        if (JackpotBusinessLogic.IsJackpotsEnabled())
        {
            m_jackpot_viewer_listener = new JackpotViewerListener(new JackpotViewerService());
        }

    // Start CashIO
    WSI.Common.CountR.CountR.UpdateAlarmGroupCountR();

    // Occupation Camara Sensor
    if (Misc.IsIntelliaEnabled() && GeneralParam.GetBoolean("Intellia", "Camera.Enabled", false))
    {
      CamaraOcupacion.OccupationManager.Init();
    }

    // Start Buckets thread
    WCP_Buckets.Init();

    // JBP 05-OCT-2016
    CFDIThread.Init();

        //PDM 03-FEB-2017
        if (GeneralParam.GetBoolean("Features", "WinUP"))
        {
            WSI.WinUp.Notifications.NotificationBuilder.Init();
        }

    //DMT 22-NOV-2017

        if (GeneralParam.GetInt32("Movibank", "Device", 0) == 1)
        {
    WSI.Mobibank.Request.RequestBuilder.Init();
        }

        // FBM
        WCP_FBM.Init();

        // Start Generation Cash Out Voucher Thread/s.
        WSI.Common.AutoPrint.BusinessLogic.AutoPrintPendingGenerationThread.Init();

        // Start Print Cash Out Voucher Thread/s.
        WSI.Common.AutoPrint.BusinessLogic.AutoPrintVouchers.Init();


    WCP_GamingTable.Init();
    WCP_GamingTable.Start();
    
    // AMF 13-DEC-2017
    WCP_TerminalsTimeDisconnected.Init();

    // OMC 09-FEB-2018
    WSI.Common.TriggersValidation.TriggersValidation.Init();

  } // OnServiceRunning<

} // WCP_Service

public class WigosLogger : WigosHubCommon.ILogger
{
  public void Log(LogLevel Level, string Message)
  {
    if (Level >= LogLevel)
    {
      WSI.Common.Log.Message(Message);
    }
  }

  public void Exception(Exception _ex)
  {
    WSI.Common.Log.Exception(_ex);
  }

  public void Chain(WigosHubCommon.ILogger SubLogger)
  {
    throw new NotImplementedException();
  }

  public LogLevel LogLevel { get; set; }

  public ILogger Parent { get; set; }
}

[RunInstallerAttribute(true)]
public class ProjectInstaller : Installer
{
  private ServiceInstaller serviceInstaller;
  private ServiceProcessInstaller processInstaller;

  public ProjectInstaller()
  {
    processInstaller = new ServiceProcessInstaller();
    serviceInstaller = new ServiceInstaller();
    // Service will run under network account and will be configured later with a correct username and password.
    processInstaller.Account = ServiceAccount.NetworkService;
    // Service will have Start Type of Manual
    serviceInstaller.StartType = ServiceStartMode.Manual;
    // Here we are going to hook up some custom events prior to the install and uninstall
    BeforeInstall += new InstallEventHandler(BeforeInstallEventHandler);
    BeforeUninstall += new InstallEventHandler(BeforeUninstallEventHandler);
    // serviceInstaller.ServiceName = servicename;
    Installers.Add(serviceInstaller);
    Installers.Add(processInstaller);
  }

  private void BeforeInstallEventHandler(object sender, InstallEventArgs e)
  {
    //The service has a fixed name.
    serviceInstaller.ServiceName = "WCP_Service";
  }

  private void BeforeUninstallEventHandler(object sender, InstallEventArgs e)
  {
    //The service has a fixed name.
    serviceInstaller.ServiceName = "WCP_Service";
  }
}
