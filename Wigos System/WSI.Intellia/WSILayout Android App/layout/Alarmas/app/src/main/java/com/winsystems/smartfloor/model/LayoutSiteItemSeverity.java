package com.winsystems.smartfloor.model;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: LayoutSiteItemSeverity
//
// DESCRIPTION: Implements method for site item severity.
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public enum LayoutSiteItemSeverity {
  LOW(1),
  MEDIUM(2),
  HIGH(3);

  private int value;

  public int getValue()
  {
    return value;
  }

  LayoutSiteItemSeverity(int i) {
    value = i;
  }

  public static LayoutSiteItemSeverity getById(int id){
    switch (id){
      case 1:
        return LOW;
      case 2:
        return MEDIUM;
      case 3:
        return HIGH;
      default:
        return LOW;
    }
  }
}
