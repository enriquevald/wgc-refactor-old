package com.wynsistems.alarmas.repository;

import android.content.Context;

public class RepositoryFactory {
    private static RepositoryFactory instance = null;

    private RepositoryFactory() {}

    public static RepositoryFactory getInstance(){
        if(instance== null){
            instance = new RepositoryFactory();
        }
        return instance;
    }

    public TaskRepository getAlarmRepository(Context context){
        return DefaultTaskRepository.getInstance(context);
    }
}
