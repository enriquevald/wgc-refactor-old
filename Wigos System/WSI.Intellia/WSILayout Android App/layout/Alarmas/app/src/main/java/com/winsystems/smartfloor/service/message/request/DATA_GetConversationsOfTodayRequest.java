package com.winsystems.smartfloor.service.message.request;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.util.Hashtable;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: DATA_GetConversationsOfTodayRequest
//
// DESCRIPTION: Implement methods for get conversation today.
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public class DATA_GetConversationsOfTodayRequest implements KvmSerializable {

  private String session;

  public void setSession(String session) {
    this.session = session;
  }

  @Override
  public Object getProperty(int i) {
    switch (i){
      case 0:
        return session;
      default:
        return null;
    }
  }

  @Override
  public int getPropertyCount() {
    return 1;
  }

  @Override
  public void setProperty(int i, Object o) {

    switch (i){
      case 0:
        session =  o.toString();
        break;
    }

  }

  @Override
  public void getPropertyInfo(int i, Hashtable hashtable, PropertyInfo propertyInfo) {
    switch (i){
      case 0:
        propertyInfo.type = PropertyInfo.STRING_CLASS;
        propertyInfo.name = "SessionId";
        break;
    }
  }
}