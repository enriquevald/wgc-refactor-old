package com.winsystems.smartfloor.view.task;


import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.winsystems.smartfloor.R;
import com.winsystems.smartfloor.common.LayoutNotificationManager;
import com.winsystems.smartfloor.helpers.AlarmResourseHelper;
import com.winsystems.smartfloor.model.LayoutSiteItemSeverity;
import com.winsystems.smartfloor.model.LayoutSiteItemSubCategory;
import com.winsystems.smartfloor.model.LayoutSiteTaskActions;
import com.winsystems.smartfloor.service.DATA_PostActionResponse;
import com.winsystems.smartfloor.service.ResponseCode;
import com.winsystems.smartfloor.service.ServiceFactory;
import com.winsystems.smartfloor.service.alarm.AlarmService;
import com.winsystems.smartfloor.service.login.response.LayoutSiteTask;
import com.winsystems.smartfloor.service.login.response.LayoutSiteTaskTerminal;
import com.winsystems.smartfloor.service.mqtt.PahoMqttService;
import com.winsystems.smartfloor.view.terminal.TerminalInfoMenuActivity;
import com.winsystems.smartfloor.view.utils.Constants;
import com.winsystems.smartfloor.view.utils.KioskModeActivity;
import com.winsystems.smartfloor.view.utils.ToastFactory;

import java.text.Format;
import java.text.SimpleDateFormat;

import static com.google.android.gms.internal.zzhl.runOnUiThread;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: TaskDetailsInfoFragment
//
// DESCRIPTION: Implements the functionality for task detail info.
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public class TaskDetailsInfoFragment extends Fragment {

    private LayoutSiteTask task;
    private Format formatter = new SimpleDateFormat("dd/MM HH:mm");
    private String alarmServerUrl = "";
    private String idAlarm;
    private String userId;
    private LayoutSiteTaskActions action;
    private Integer notificationId;
    private View rootView;
    private String scaleReason = "";
    private boolean taskUnavailable = true;
    private Button resolveButton;
    private Button scaleButton;
    private Button assignButton;
    private IntentFilter intentFilter = new IntentFilter();
    private BroadcastReceiver tasksUpdatedBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            task = ServiceFactory.getInstance().getAlarmService(getActivity().getApplicationContext()).getById(task.getId().toString());
            if(task == null)
                taskUnavailable = false;
            fillLayoutUsingCurrentAlarm(rootView);
        }
    };


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.task_detail_info_fragment, container, false);
        notificationId = getActivity().getIntent().getIntExtra(Constants.NOTIFICATION_ID, 0);
        SharedPreferences settings = getActivity().getSharedPreferences(Constants.USER_ID_PREFERENCE_EDITOR, 0) ;
        userId = settings.getString(Constants.USER_ID, "");
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        alarmServerUrl = prefs.getString("alarm_server_address", alarmServerUrl);

        intentFilter.addAction(Constants.UPDATED_TASKS);
        intentFilter.addAction(Constants.NEW_TASK_ARRIVE);

        return rootView;
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    private void fillLayoutUsingCurrentAlarm(final View activity) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                boolean actionsAvailables = false;
                TextView description = (TextView) activity.findViewById(R.id.alarm_detail_description);
                TextView terminalId = (TextView) activity.findViewById(R.id.terminal_id);
                TextView categoryDescription = (TextView) activity.findViewById(R.id.category_description);
                TextView alarmId = (TextView) activity.findViewById(R.id.alarm_id);
                TextView alarmDate = (TextView) activity.findViewById(R.id.alarm_date);
                LinearLayout location = (LinearLayout) activity.findViewById(R.id.terminal_details);
                FrameLayout imageView = (FrameLayout) activity.findViewById(R.id.alarm_criticality_type);
                ImageView alarmCategoryType = (ImageView) activity.findViewById(R.id.alarm_category_image);
                TextView terminalProvider = (TextView) activity.findViewById(R.id.terminal_provider);
                TextView terminalModel = (TextView) activity.findViewById(R.id.terminal_model);
                TextView terminalArea = (TextView) activity.findViewById(R.id.terminal_area);
                TextView terminalIsland = (TextView) activity.findViewById(R.id.terminal_island);
                Button terminalStatus = (Button) activity.findViewById(R.id.link_terminal_status);
                LinearLayout terminalDetails = (LinearLayout) activity.findViewById(R.id.terminal_details);
                resolveButton = (Button) activity.findViewById(R.id.resolve_button);
                scaleButton   = (Button) activity.findViewById(R.id.scale_button);
                assignButton  = (Button) activity.findViewById(R.id.assign_button);

                task = ((TaskDetailActivity)getActivity()).getTask();
                if(task != null){

                    Integer status = task.getStatus();
                    Integer severity = task.getSeverity();

                    actionsAvailables = (!(status == LayoutSiteTaskActions.GetActionId(LayoutSiteTaskActions.Resolved) ||
                            status == LayoutSiteTaskActions.GetActionId(LayoutSiteTaskActions.Scaled))) &&
                            taskUnavailable;

                    LayoutSiteTaskTerminal terminal = task.getTerminal();

                    if(terminal.getId() != 10000) {
                        categoryDescription.setTextSize(16);
                        terminalId.setText(terminal.getName());
                        terminalProvider.setText(terminal.getProvider());
                        terminalModel.setText(terminal.getModel());
                        terminalArea.setText(terminal.getArea());
                        terminalIsland.setText(terminal.getBank());
                        terminalDetails.setVisibility(View.VISIBLE);
                    }else {
                        categoryDescription.setTextSize(20);
                        terminalId.setVisibility(View.GONE);
                        terminalDetails.setVisibility(View.GONE);
                    }
                    description.setText(task.getDescription());
                    int subCategoryValue = task.getSubcategory();
                    if(LayoutSiteItemSubCategory.isCustomCategory(subCategoryValue))
                        categoryDescription.setText(task.getTitle());
                    else
                        categoryDescription.setText(LayoutSiteItemSubCategory.forCode(task.getSubcategory()).getDescription(getActivity().getApplicationContext()));

                    alarmId.setText(task.getId().toString());
                    alarmDate.setText(formatter.format(task.getCreation()));

                    GradientDrawable bgShape = (GradientDrawable) imageView.getBackground();
                    bgShape.setColor(AlarmResourseHelper.getColorByCriticality(LayoutSiteItemSeverity.getById(severity), getResources()));
                    alarmCategoryType.setImageResource(AlarmResourseHelper.getCategoryImageByType(LayoutSiteItemSubCategory.forCode(task.getSubcategory()).getGroupCategory()));


                    if (status == LayoutSiteTaskActions.GetActionId(LayoutSiteTaskActions.Accepted)) {
                        assignButton.setVisibility(View.GONE);
                        resolveButton.setVisibility(View.VISIBLE);
                    }

                    TextView textStatus = (TextView) activity.findViewById(R.id.task_status);
                    if(LayoutSiteTaskActions.GetActionLayoutSiteTaskAction(task.getStatus()) == LayoutSiteTaskActions.Assigned)
                    {
                        textStatus.setText(R.string.Pending);
                        textStatus.setTextColor(Color.parseColor("#c62e2b"));
                    }else
                    {
                        textStatus.setText(R.string.InProgress);
                        textStatus.setTextColor(Color.parseColor("#ff0057a5"));
                    }
                }

                final boolean finalActionsAvailables = actionsAvailables;
                assignButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(finalActionsAvailables){

                            action = LayoutSiteTaskActions.Accepted;
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    new HttpAssignTask().execute();
                                }
                            });
                            RemoveNotification();
                        }else{
                            ToastFactory.buildSimpleToast(getResources().getString(R.string.no_action_task_text), getActivity().getApplicationContext());
                        }

                    }
                });

                scaleButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (finalActionsAvailables) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setTitle(R.string.scale_button);
                            final EditText input = new EditText(getActivity());
                            input.setInputType(InputType.TYPE_CLASS_TEXT);
                            input.setHint(R.string.scale_reason);
                            builder.setView(input);
                            builder.setPositiveButton(R.string.scale_button, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    enableDisableButtons(resolveButton, scaleButton);
                                    action = LayoutSiteTaskActions.Scaled;
                                    scaleReason = input.getText().toString();
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            new HttpAssignTask().execute();
                                        }
                                    });
                                    RemoveNotification();
                                }
                            });
                            builder.setNegativeButton(R.string.cancel_button, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });
                            builder.show();
                        } else {
                            ToastFactory.buildSimpleToast(getResources().getString(R.string.no_action_task_text), getActivity().getApplicationContext());
                        }
                    }
                });

                resolveButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (finalActionsAvailables) {
                            action = LayoutSiteTaskActions.Resolved;
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    new HttpAssignTask().execute();
                                }
                            });
                            RemoveNotification();
                        } else {
                            ToastFactory.buildSimpleToast(getResources().getString(R.string.no_action_task_text), getActivity().getApplicationContext());
                        }

                    }
                });

                terminalStatus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent terminalStatusIntent = new Intent(getActivity().getApplicationContext(), TerminalInfoMenuActivity.class);
                        LayoutSiteTaskTerminal terminal = task.getTerminal();
                        terminalStatusIntent.putExtra(Constants.TERMINAL_ID, terminal.getId());
                        terminalStatusIntent.putExtra(Constants.TERMINAL_NAME, terminal.getName());
                        startActivity(terminalStatusIntent);
                    }
                });

            }
        });
    }

    private void RemoveNotification() {
        LayoutNotificationManager notificationManager = new LayoutNotificationManager(getActivity().getApplicationContext());
        if(notificationId != 0)
            notificationManager.remove(notificationId);
        else
            notificationManager.remove(task.getId());
    }



    private void enableDisableButtons(Button enabledButton, Button disabledButton) {
        enabledButton.setEnabled(true);
        enabledButton.setAlpha(1F);
        disabledButton.setEnabled(false);
        disabledButton.setAlpha(0.5F);
    }

    @Override
    public void onResume(){
        super.onResume();
        fillLayoutUsingCurrentAlarm(rootView);
        getActivity().registerReceiver(tasksUpdatedBroadcastReceiver, intentFilter);


    }
    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(tasksUpdatedBroadcastReceiver);
    }

    private class HttpAssignTask extends AsyncTask<Void, Void, Boolean> {
        ProgressDialog dialogAssingTask = new ProgressDialog(getActivity());
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialogAssingTask.setTitle(getActivity().getResources().getString(R.string.title_progress_dialog));
            dialogAssingTask.setMessage(getActivity().getResources().getString(R.string.alarm_message_progress_dialog));
            dialogAssingTask.setCanceledOnTouchOutside(false);
            dialogAssingTask.show();
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            final DATA_PostActionResponse response =  ServiceFactory.getInstance().getAlarmService(getActivity().getApplicationContext()).setUrl(alarmServerUrl).assignAlarm(task.getId().toString(), userId, action, scaleReason);
            AlarmService service = ServiceFactory.getInstance().getAlarmService(getActivity().getApplicationContext());
            service.setUrl(alarmServerUrl);
            service.getAll(userId, getActivity().getApplicationContext());
            if(response.getResponseCode() == ResponseCode.DATA_RC_OK)
                return true;
            else{
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast toast = Toast.makeText(getActivity().getApplicationContext(), response.getResponseCodeDescription(), Toast.LENGTH_SHORT);
                        toast.show();

                    }
                });
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (dialogAssingTask.isShowing()) {
                dialogAssingTask.dismiss();
            }
            if(result) {
                Intent broadcastIntent = new Intent();
                broadcastIntent.setAction(Constants.UPDATED_TASKS);
                getActivity().sendBroadcast(broadcastIntent);
                if (action == LayoutSiteTaskActions.Accepted){
                    assignButton.setVisibility(View.GONE);
                    resolveButton.setVisibility(View.VISIBLE);

                    fillLayoutUsingCurrentAlarm(rootView);
                }else
                {
                    getActivity().finish();
                }
            }
        }
    }

    public void setActionsAvailability(boolean state){

        View view = this.getView();

        if(view == null){
            return;
        }
        Button button = (Button) view.findViewById(R.id.scale_button);
        button.setEnabled(state);
        button = (Button) view.findViewById(R.id.resolve_button);
        button.setEnabled(state);
        button = (Button) view.findViewById(R.id.assign_button);
        button.setEnabled(state);
    }

}
