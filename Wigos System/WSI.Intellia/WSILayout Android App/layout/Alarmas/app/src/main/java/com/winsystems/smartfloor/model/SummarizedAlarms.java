package com.winsystems.smartfloor.model;

import com.winsystems.smartfloor.service.login.response.LayoutSiteTask;

import java.io.Serializable;
import java.util.List;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: SummarizedAlarms
//
// DESCRIPTION: Class that contains methods for the alarm summary.
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public class SummarizedAlarms implements Serializable {

  private boolean isSeverity = false;
  private Boolean inProgress;
  private LayoutSiteItemGroupCategory alarmCategoryType;
  private LayoutSiteItemSeverity alarmCriticality;
  private String description;
  private int total;
  private List<LayoutSiteTask> tasks;

  public SummarizedAlarms(LayoutSiteItemSeverity alarmCriticality, String description, int total, List<LayoutSiteTask> tasks) {
    this.alarmCriticality = alarmCriticality;
    this.description = description;
    this.total = total;
    this.tasks = tasks;
    this.isSeverity = true;
  }

  public SummarizedAlarms(LayoutSiteItemGroupCategory alarmCategoryType, String description, int total, List<LayoutSiteTask> tasks) {
    this.alarmCategoryType = alarmCategoryType;
    this.description = description;
    this.total = total;
    this.tasks = tasks;
  }

  public SummarizedAlarms(Boolean inProgress, String description, int total, List<LayoutSiteTask> tasks) {
    this.inProgress = inProgress;
    this.description = description;
    this.total = total;
    this.tasks = tasks;
  }

  public AlarmGroupingCriteria getGroupingCriteria(){
    if(alarmCategoryType != null)
      return AlarmGroupingCriteria.CATEGORY;

    if(alarmCriticality != null)
      return AlarmGroupingCriteria.CRITICALITY;

    return AlarmGroupingCriteria.COMPLETED;
  }

  public LayoutSiteItemSeverity getAlarmCriticality() {
    return alarmCriticality;
  }

  public boolean isSeverity() {
    return isSeverity;
  }

  public void setAlarmCriticality(LayoutSiteItemSeverity alarmCriticality) {
    this.alarmCriticality = alarmCriticality;
  }
  public LayoutSiteItemGroupCategory getAlarmCategoryType() {
    return alarmCategoryType;
  }

  public Boolean getInProgress() {
    return inProgress;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public int getTotal() {
    return total;
  }

  public void setTotal(int total) {
    this.total = total;
  }

  public List<LayoutSiteTask> getTasks() {
    return tasks;
  }
}
