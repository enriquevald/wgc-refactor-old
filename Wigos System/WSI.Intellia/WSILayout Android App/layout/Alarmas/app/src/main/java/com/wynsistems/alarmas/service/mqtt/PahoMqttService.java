package com.wynsistems.alarmas.service.mqtt;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.provider.Settings.Secure;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;
import com.estimote.sdk.Utils;
import com.wynsistems.alarmas.R;
import com.wynsistems.alarmas.common.AndroidLogger;
import com.wynsistems.alarmas.common.IntentExtra;
import com.wynsistems.alarmas.common.LayoutNotification;
import com.wynsistems.alarmas.common.LayoutNotificationManager;
import com.wynsistems.alarmas.common.LayoutNotificationType;
import com.wynsistems.alarmas.helpers.LayoutPendingMessage;
import com.wynsistems.alarmas.helpers.LogHelper;
import com.wynsistems.alarmas.helpers.PendingToReadMessagesHelper;
import com.wynsistems.alarmas.helpers.SaveInformationHelper;
import com.wynsistems.alarmas.model.LayoutSiteItemSubCategory;
import com.wynsistems.alarmas.service.DATA_PostActionResponse;
import com.wynsistems.alarmas.service.ResponseCode;
import com.wynsistems.alarmas.service.ServiceFactory;
import com.wynsistems.alarmas.service.alarm.AlarmService;
import com.wynsistems.alarmas.service.login.response.BeaconAvailable;
import com.wynsistems.alarmas.service.mqtt.request.DATA_BeaconRunnerDistance;
import com.wynsistems.alarmas.service.notification.NotificationHistoryManager;
import com.wynsistems.alarmas.view.message.ConversationActivity;
import com.wynsistems.alarmas.view.task.TaskDetailActivity;
import com.wynsistems.alarmas.view.task.TaskListActivity;
import com.wynsistems.alarmas.view.update.UpdateApplicationActivity;
import com.wynsistems.alarmas.helpers.AlarmResourseHelper;
import com.wynsistems.alarmas.view.utils.Constants;

import org.apache.commons.math3.fitting.leastsquares.LeastSquaresOptimizer;
import org.apache.commons.math3.fitting.leastsquares.LevenbergMarquardtOptimizer;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.eclipse.paho.client.mqttv3.MqttTopic;
import org.eclipse.paho.client.mqttv3.persist.MqttDefaultFilePersistence;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class PahoMqttService extends Service implements MqttCallback
{
    //   application preferences
    public static final String APP_ID = "winsystems.mqtt";

    public static final String 		DEBUG_TAG = "MqttService"; // Debug TAG

    private static final String		MQTT_THREAD_NAME = "MqttService[" + DEBUG_TAG + "]"; // Handler Thread ID
    public static final String      NEW_TASK_ARRIVE = "NEW TASK ARRIVE";
    public static final String      NEW_MESSAGE_FROM_MANAGER_ARRIVE = "NEW MESSAGE FROM MANAGER ARRIVE";

    private static String 	        MQTT_TOPIC = "*"; // Broker URL or IP Address
    private static String 	        MQTT_BROKER = "*"; // Broker URL or IP Address
    private static final int 		MQTT_PORT = 1883;				// Broker Port

    public static final int			MQTT_QOS_0 = 0; // QOS Level 0 ( Delivery Once no confirmation )

    private static final int 		MQTT_KEEP_ALIVE = 3 * 60000; // KeepAlive Interval in MS
    private static final String		MQTT_KEEP_ALIVE_TOPIC_FORAMT = "/users/%s/keepalive"; // Topic format for KeepAlives
    private static final byte[] 	MQTT_KEEP_ALIVE_MESSAGE = { 0 }; // Keep Alive message to send
    private static final int		MQTT_KEEP_ALIVE_QOS = MQTT_QOS_0; // Default Keepalive QOS

    private static final boolean 	MQTT_CLEAN_SESSION = true; // Start a clean session?

    private static final String 	MQTT_URL_FORMAT = "tcp://%s:%d"; // URL Format normally don't change

    private static final String 	ACTION_START 	 = DEBUG_TAG + ".START"; // Action to start
    private static final String 	ACTION_STOP		 = DEBUG_TAG + ".STOP"; // Action to stop
    private static final String 	ACTION_KEEPALIVE = DEBUG_TAG + ".KEEPALIVE"; // Action to keep alive used by alarm manager
    private static final String 	ACTION_RECONNECT = DEBUG_TAG + ".RECONNECT"; // Action to reconnect

    private static String           MESSAGE_NOTIFICATION_TOPIC = "/Messages/";
    private static final String     UPDATE_APPLICATION_NOTIFICATION_TOPIC = "/NewVersion/*";
    private static final String 	DEVICE_ID_FORMAT = "andr_%s"; // Device ID Format, add any prefix you'd like
    // Note: There is a 23 character limit you will get
    // An NPE if you go over that limit
    private boolean mStarted = false; // Is the Client started?
    private boolean mStarting = false; //During the reconnection we must handle that one thread is working on it.
    private String mDeviceId;		  // Device ID, Secure.ANDROID_ID
    private Handler mConnHandler;	  // Seperate Handler thread for networking

    private MqttDefaultFilePersistence mDataStore; // Defaults to FileStore
    private MqttConnectOptions mOpts;			// Connection Options

    private MqttTopic mKeepAliveTopic;			// Instance Variable for Keepalive topic

    private MqttClient mClient;					// Mqtt Client

    private AlarmManager mAlarmManager;			// Alarm manager to perform repeating tasks
    private ConnectivityManager mConnectivityManager; // To check for connectivity changes

    private BeaconManager beaconManager;
    private final Region ALL_ESTIMOTE_BEACONS = new Region("rId", null, null, null);
    private String url = "";
    private String sessionId = "";
    private boolean isRegisteredConnectivity = false;
    private NotificationHistoryManager notificationHistoryManager;
    private static AndroidLogger mLog;
    /**
     * Start MQTT Client
     * @param 'Context context to start the service with
     * @return void
     */
    public static void actionStart(Context ctx) {
        Intent i = new Intent(ctx,PahoMqttService.class);
        i.setAction(ACTION_START);
        ctx.startService(i);
    }

    /**
     * Stop MQTT Client
     * @param 'Context context to start the service with
     * @return void
     */
    public static void actionStop(Context ctx) {
        Intent i = new Intent(ctx,PahoMqttService.class);
        i.setAction(ACTION_STOP);
        ctx.startService(i);
    }
    /**
     * Send a KeepAlive Message
     * @param 'Context context to start the service with
     * @return void
     */
    public static void actionKeepalive(Context ctx) {
        Intent i = new Intent(ctx,PahoMqttService.class);
        i.setAction(ACTION_KEEPALIVE);
        ctx.startService(i);
    }

    /**
     * Initalizes the DeviceId and most instance variables
     * Including the Connection Handler, Datastore, Alarm Manager
     * and ConnectivityManager.
     */
    @Override
    public void onCreate() {
        super.onCreate();
        mLog = LogHelper.getLogger(getApplicationContext());

        SharedPreferences settings = getSharedPreferences(APP_ID, MODE_PRIVATE);
        MQTT_BROKER = settings.getString("broker", "");
        MQTT_TOPIC      = settings.getString("topic", "");
        MESSAGE_NOTIFICATION_TOPIC = settings.getString("message-topic", "");
        String[] urlBroker = MQTT_BROKER.split("//");
        String ip = urlBroker[1].split(":")[0];

        MQTT_BROKER = ip;


        mDeviceId = String.format(DEVICE_ID_FORMAT,
                Secure.getString(getContentResolver(), Secure.ANDROID_ID));

        HandlerThread thread = new HandlerThread(MQTT_THREAD_NAME);
        thread.start();

        mConnHandler = new Handler(thread.getLooper());
        mDataStore = new MqttDefaultFilePersistence(getCacheDir().getAbsolutePath());

        mOpts = new MqttConnectOptions();
        mOpts.setCleanSession(MQTT_CLEAN_SESSION);
        // Do not set keep alive interval on mOpts we keep track of it with alarm's

        mAlarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        mConnectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);


        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            mLog.info("PahoMqttService - Bluetooth is not operative");
            Log.i("Bluetooth", "Bluetooth is not operative");
        } else {
            if (mBluetoothAdapter.isEnabled()) {
                beaconManager = new BeaconManager(getBaseContext());
                beaconManager.setForegroundScanPeriod(5000, 30000);
                beaconManager.setRangingListener(new BeaconManager.RangingListener() {
                    @Override
                    public void onBeaconsDiscovered(Region region, List<Beacon> beacons) {
                        try {

                            if (beacons.size() > 0) {
                                Beacon[] array = new Beacon[]{};
                                new PostBeaconRunnerDistanceInBackground().execute(beacons.toArray(array));
                            }

                            //Save beacons
                        } catch (Exception e) {
                            mLog.error("PahoMqttService - setRangingListener BEACONS - " + e.getMessage(), e);
                            Log.e("BeaconsService", e.getMessage());
                        }
                    }
                });
            }

        }
        notificationHistoryManager = new NotificationHistoryManager(getApplicationContext());
    }
    // set the duration of the scan to be 1.1 seconds


    /**
     * Service onStartCommand
     * Handles the action passed via the Intent
     *
     * @return START_REDELIVER_INTENT
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        SharedPreferences userSettings = this.getSharedPreferences(Constants.USER_ID_PREFERENCE_EDITOR, 0) ;
        sessionId = userSettings.getString(Constants.USER_ID,sessionId);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        url = prefs.getString("alarm_server_address", url);

        if(intent == null)
            return 0;

        String action = intent.getAction();
        Log.i(DEBUG_TAG, "Received action of " + action);

        if(action == null) {
            Log.i(DEBUG_TAG,"Starting service with no action\n Probably from a crash");
        } else {
            if(action.equals(ACTION_START)) {
                Log.i(DEBUG_TAG,"Received ACTION_START");
                start();
            } else if(action.equals(ACTION_STOP)) {
                stop();
            } else if(action.equals(ACTION_KEEPALIVE)) {
                keepAlive();
            } else if(action.equals(ACTION_RECONNECT)) {
                if(isNetworkAvailable()) {
                    reconnectIfNecessary();
                }
            }
        }


        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            Log.i("Bluetooth", "Bluetooth is not operative");
            mLog.info("PahoMqttService - Bluetooth is not operative");
        } else {
            if (mBluetoothAdapter.isEnabled()) {
                beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
                    @Override
                    public void onServiceReady() {
                        try {
                            beaconManager.startRanging(ALL_ESTIMOTE_BEACONS);
                        } catch (RemoteException e) {
                            Log.e("BeaconsService", "Can't start ranging");
                            mLog.error("PahoMqttService - startRanging BEACONS - " + e.getMessage(), e);
                        }
                    }
                });
            }
        }

        return START_STICKY;
    }

    /**
     * Attempts connect to the Mqtt Broker
     * and listen for Connectivity changes
     * via ConnectivityManager.CONNECTVITIY_ACTION BroadcastReceiver
     */
    private synchronized void start() {
        if(mStarted) {
            Log.i(DEBUG_TAG,"Attempt to start while already started");
            return;
        }

        if(hasScheduledKeepAlives()) {
            stopKeepAlives();
        }

        connect();
        try {
            if(!isRegisteredConnectivity) {
                registerReceiver(mConnectivityReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                isRegisteredConnectivity = true;
            }
        }catch(Exception e){
            e.printStackTrace();
            mLog.error(e.getMessage(), e);
        }
    }
    /**
     * Attempts to stop the Mqtt client
     * as well as halting all keep alive messages queued
     * in the alarm manager
     */
    private synchronized void stop() {
        if(!mStarted) {
            Log.i(DEBUG_TAG,"Attemtpign to stop connection that isn't running");
            return;
        }

        if(mClient != null) {
            mConnHandler.post(new Runnable() {
                @Override
                public void run() {
                    try {
                        mClient.disconnect();
                    } catch(MqttException ex) {
                        ex.printStackTrace();
                    }
                    mClient = null;
                    mStarted = false;

                    stopKeepAlives();
                }
            });
        }

        unregisterReceiver(mConnectivityReceiver);
    }
    /**
     * Connects to the broker with the appropriate datastore
     */
    private synchronized void connect() {
        String url = String.format(Locale.US, MQTT_URL_FORMAT, MQTT_BROKER, MQTT_PORT);
        Log.i(DEBUG_TAG, "Connecting with URL: " + url);
        try {
            if(mDataStore != null) {
                Log.i(DEBUG_TAG,"Connecting with DataStore");
                mClient = new MqttClient(url,"AppDevice_" + mDeviceId,mDataStore);
            } else {
                Log.i(DEBUG_TAG,"Connecting with MemStore");
                mClient = new MqttClient(url,"AppDevice_" + mDeviceId);
            }
        } catch(MqttException e) {
            e.printStackTrace();
            mLog.error(e.getMessage(), e);
        }

        mConnHandler.post(new Runnable() {
            @Override
            public void run() {
                try {
                    mClient.connect(mOpts);


                    String[] topics = new String[]{MQTT_TOPIC, MESSAGE_NOTIFICATION_TOPIC, UPDATE_APPLICATION_NOTIFICATION_TOPIC};
                    int[] qos = new int[]{0, 0, 0};

                    mClient.subscribe(topics, qos);

                    mClient.setCallback(PahoMqttService.this);

                    mStarted = true; // Service is now connected

                    Log.i(DEBUG_TAG, "Successfully connected and subscribed starting keep alives");

                    startKeepAlives();
                } catch (MqttException e) {
                    e.printStackTrace();
                    mLog.error(e.getMessage(), e);
                }
            }
        });
    }
    /**
     * Schedules keep alives via a PendingIntent
     * in the Alarm Manager
     */
    private void startKeepAlives() {
        Intent i = new Intent();
        i.setClass(this, PahoMqttService.class);
        i.setAction(ACTION_KEEPALIVE);
        PendingIntent pi = PendingIntent.getService(this, 0, i, 0);
        mAlarmManager.setRepeating(AlarmManager.RTC_WAKEUP,
                System.currentTimeMillis() + MQTT_KEEP_ALIVE,
                MQTT_KEEP_ALIVE, pi);
    }
    /**
     * Cancels the Pending Intent
     * in the alarm manager
     */
    private void stopKeepAlives() {
        Intent i = new Intent();
        i.setClass(this, PahoMqttService.class);
        i.setAction(ACTION_KEEPALIVE);
        PendingIntent pi = PendingIntent.getService(this, 0, i , 0);
        mAlarmManager.cancel(pi);
    }
    /**
     * Publishes a KeepALive to the topic
     * in the broker
     */
    private synchronized void keepAlive() {
        try{
            new KeepALiveSmartfloorWebService().execute();
        }catch (Exception e){
            mLog.error("KeepALive Error - " + e.getMessage(), e);
        }
        if(isConnected()) {
            try {
                sendKeepAlive();
                return;
            } catch(MqttConnectivityException ex) {
                ex.printStackTrace();
                reconnectIfNecessary();
            } catch(MqttPersistenceException ex) {
                ex.printStackTrace();
                stop();
            } catch(MqttException ex) {
                ex.printStackTrace();
                stop();
            }catch(Exception ex) {
                ex.printStackTrace();
                mLog.error(ex.getMessage(),ex);
            }

        }else{
            if(isNetworkAvailable()) {
                start();
            }
        }
    }

    private synchronized void reconnectIfNecessary() {
        if(mStarted && mClient == null) {
            Log.i(DEBUG_TAG,"Reconnecting...");
            connect();
        }

    }
    /**
     * Query's the NetworkInfo via ConnectivityManager
     * to return the current connected state
     * @return boolean true if we are connected false otherwise
     */
    private boolean isNetworkAvailable() {
        NetworkInfo info = mConnectivityManager.getActiveNetworkInfo();

        return (info == null) ? false : info.isConnected() && info.getType() == ConnectivityManager.TYPE_WIFI;
    }
    /**
     * Verifies the client State with our local connected state
     * @return true if its a match we are connected false if we aren't connected
     */
    private boolean isConnected() {
        if(mStarted && mClient != null && !mClient.isConnected()) {
            Log.i(DEBUG_TAG,"Mismatch between what we think is connected and what is connected");
        }

        if(mClient != null) {
            return (mStarted && mClient.isConnected()) ? true : false;
        }

        return false;
    }
    /**
     * Receiver that listens for connectivity chanes
     * via ConnectivityManager
     */
    private final BroadcastReceiver mConnectivityReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            NetworkInfo info = mConnectivityManager.getActiveNetworkInfo();
            if(info != null){
                if(info.isConnected() && info.getType() == ConnectivityManager.TYPE_WIFI)
                {
                    Log.i(DEBUG_TAG,"Wifi Connected");
                    if(mClient == null && !mStarting)
                    {
                        Log.i(DEBUG_TAG,"Client is NULL, so we come from connectionLost");
                        Log.i(DEBUG_TAG,"restarting server");
                        actionStart(context);
                        mStarting = true;
                    }
                }else
                {
                    Log.i(DEBUG_TAG,"Wifi is not available");
                }
            }

        }
    };
    /**
     * Sends a Keep Alive message to the specified topic
     * @see 'MQTT_KEEP_ALIVE_MESSAGE
     * @see 'MQTT_KEEP_ALIVE_TOPIC_FORMAT
     * @return MqttDeliveryToken specified token you can choose to wait for completion
     */
    private synchronized MqttDeliveryToken sendKeepAlive()
            throws MqttConnectivityException, MqttPersistenceException, MqttException {
        if(!isConnected())
            throw new MqttConnectivityException();

        if(mKeepAliveTopic == null) {
            mKeepAliveTopic = mClient.getTopic(
                    String.format(Locale.US, MQTT_KEEP_ALIVE_TOPIC_FORAMT,mDeviceId));
        }

        Log.i(DEBUG_TAG,"Sending Keepalive to " + MQTT_BROKER);

        MqttMessage message = new MqttMessage(MQTT_KEEP_ALIVE_MESSAGE);
        message.setQos(MQTT_KEEP_ALIVE_QOS);

        return mKeepAliveTopic.publish(message);
    }
    /**
     * Query's the AlarmManager to check if there is
     * a keep alive currently scheduled
     * @return true if there is currently one scheduled false otherwise
     */
    private synchronized boolean hasScheduledKeepAlives() {
        Intent i = new Intent();
        i.setClass(this, PahoMqttService.class);
        i.setAction(ACTION_KEEPALIVE);
        PendingIntent pi = PendingIntent.getBroadcast(this, 0, i, PendingIntent.FLAG_NO_CREATE);

        return (pi != null) ? true : false;
    }


    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }
    /**
     * Connectivity Lost from broker
     */
    @Override
    public void connectionLost(Throwable arg0) {
        Log.i(DEBUG_TAG,"Connection Lost");

        mClient = null;
        mStarted = false;

        if(isNetworkAvailable()) {
            reconnectIfNecessary();
        }
    }

    @Override
    public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {
        String[] splitedTopic = topic.split("/");
        String fomattedTopic = "";
        if (splitedTopic.length > 1)
            fomattedTopic = splitedTopic[4];

        String messageBody = new String(mqttMessage.getPayload());

        if (splitedTopic[1].equals(getString(R.string.ChatIdentificator))){
            notifyUserNewMessageArrive(topic, messageBody);
            broadcastReceivedMessage(NEW_MESSAGE_FROM_MANAGER_ARRIVE);
        }else if (splitedTopic[1].equals("NewVersion")) {
            notifyUserNewVersionArrive();
            broadcastReceivedMessage(NEW_MESSAGE_FROM_MANAGER_ARRIVE);

        }else{
            new UpdateLayoutAssignedTaskList().execute(fomattedTopic, messageBody);
        }
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

    }

    private void broadcastReceivedMessage(String constant)
    {
        // pass a message received from the MQTT server on to the Activity UI
        //   (for times when it is running / active) so that it can be displayed
        //   in the app GUI
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(constant);
        sendBroadcast(broadcastIntent);
    }

    private void notifyUserNewVersionArrive()
    {
        //topic /Messages/{runner_id}/{manager_id}/{manager_user_name}
        Intent notificationIntent = new Intent(this, UpdateApplicationActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(this,
                0,
                notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        Bitmap iconImage = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.ic_system_update_alt_white_48dp), 64, 64, false);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        // Gets an instance of the NotificationManager service
        NotificationManager mNotifyMgr =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        Notification notification = builder.build();
        //Setting default device sound.
        notification.defaults |= Notification.DEFAULT_SOUND;
        notification.defaults |= Notification.DEFAULT_LIGHTS;
        notification.ledARGB = Color.BLUE;

        mNotifyMgr.notify(21, notification);
    }

    private void notifyUserNewMessageArrive(String topic, String body)
    {
        final String  MESSAGING = "MESSAGING";
        //topic /Messages/{runner_id}/{manager_id}/{manager_user_name}

        String[] topicData = topic.split("/Messages/")[1].split("/");
        String managerId = topicData[1];
        String managerUserName = topicData[2];

        PendingToReadMessagesHelper pendingMessasgesHelper = new PendingToReadMessagesHelper(getApplicationContext());
        LayoutNotificationManager notificationManager = new LayoutNotificationManager(getApplicationContext());
        LayoutPendingMessage pendingMessage = new LayoutPendingMessage();
        pendingMessage.setManager(managerUserName);
        pendingMessage.setMessage(body);
        pendingMessasgesHelper.addPendingMessage(pendingMessage);

        Intent notificationIntent = new Intent(this, ConversationActivity.class);

        notificationIntent.putExtra(Constants.MANAGER_ID, Long.parseLong(managerId));
        notificationIntent.putExtra(Constants.MANAGER_USERNAME, managerUserName);
        notificationIntent.putExtra(Constants.FROM_NOTIFICATION, true);
        PendingIntent contentIntent = PendingIntent.getActivity(this,
                0,
                notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        Bitmap iconImage = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.ic_message_white_48dp), 64, 64, false);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        LayoutNotification notification = new LayoutNotification();
        notification.setNotificationType(LayoutNotificationType.MESSAGE);
        notification.setIcon(R.drawable.ic_message_white_48dp);

        List<IntentExtra> intentExtras = new ArrayList<>();
        intentExtras.add(new IntentExtra(Constants.MANAGER_ID, managerId));
        intentExtras.add(new IntentExtra(Constants.MANAGER_USERNAME, managerUserName));
        intentExtras.add(new IntentExtra(Constants.FROM_NOTIFICATION, "true"));
        notification.setExtras(intentExtras);

        // if(!notificationHistoryManager.AvailableNotificationsWithSameKey(managerId)) {
        String title = getResources().getString(R.string.new_message_from) + " " + managerUserName;
        builder.setContentTitle(title)
                .setContentText(body)
                .setSubText(managerUserName);
        notification.setTitle(title);
        notification.setContent(body);
        notificationHistoryManager.AddNotification(managerId, body);

        notificationManager.add(notification);
        broadcastReceivedMessage(Constants.NOTIFY_NEW_ICON_MENU_NOTIFICATION);
        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        long[] pattern = {1000, 1000, 1000, 1000};
        v.vibrate(pattern, -1);
    }


    private void notifyUserNewTaskArrive(String title, String body)
    {
        Intent notificationIntent;
        LayoutNotificationManager notificationManager = new LayoutNotificationManager(getApplicationContext());
        boolean haveAlarmId = body.split("_").length > 1;
        List<IntentExtra> intentExtras = new ArrayList<>();

        if(haveAlarmId) {
            String alarmId = body.split("_")[1];
            notificationIntent = new Intent(this, TaskDetailActivity.class);
            intentExtras.add(new IntentExtra(Constants.DOWNLOAD_ALARMS, "true"));
            intentExtras.add(new IntentExtra(Constants.ALARM_ID, alarmId));
        }else
        {
            notificationIntent = new Intent(this, TaskListActivity.class);
        }

        LayoutNotification notification = new LayoutNotification();
        notification.setNotificationType(LayoutNotificationType.TASK);
        notification.setExtras(intentExtras);

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        //el topic tiene la siguiente forma "/Task/Notifications/4e7ee87b8f49b8ad/Intrusion"
        //entonces haciendo un split en "/" se obtienen 5 elementos, de los cuales el 5to es la categoria.
        LayoutSiteItemSubCategory subCategory = LayoutSiteItemSubCategory.forCode(Integer.parseInt(title));
        String category = subCategory.getDescription(getApplicationContext());
        String content = haveAlarmId ? body.split("_")[0] : body;
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(AlarmResourseHelper.getCategoryImageByType(subCategory.getGroupCategory()))
                        .setContentTitle(category)
                                //.setContentText(body)
                        .setSubText(content)
                        .setContentIntent(contentIntent)
                        .setGroup(subCategory.getDescription(getApplicationContext()))
                        .setGroupSummary(true);

        notification.setTitle(category);
        notification.setIcon(AlarmResourseHelper.getCategoryImageByType(subCategory.getGroupCategory()));
        notification.setContent(content);

        notificationManager.add(notification);
        broadcastReceivedMessage(Constants.NOTIFY_NEW_ICON_MENU_NOTIFICATION);
        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        long[] pattern = {1000, 1000, 1000, 1000};
        v.vibrate(pattern, -1);
    }



    /**
     * MqttConnectivityException Exception class
     */
    private class MqttConnectivityException extends Exception {
        private static final long serialVersionUID = -7385866796799469420L;
    }

    private class PostBeaconRunnerDistanceInBackground extends AsyncTask<Beacon,Void, Boolean> {


        @Override
        protected Boolean doInBackground(Beacon... params) {

            try {
                PushBeaconRunnerDistance(params);
                Log.d("BeaconBackgroundThread", "DoInBackground Success");

                return true;
            } catch (Exception e){
                Log.e("BeaconBackgroundThread", "DoInBackground Exception: " + e);
                mLog.error("PahoMqttService - PostBeaconRunnerDistanceInBackground BEACONS - " + e.getMessage(), e);
                return false;
            }
        }

        private void PushBeaconRunnerDistance(Beacon[] params) {
            try
            {
                //Traer los beacons disponibles
                List<BeaconAvailable> beaconAvailables = new SaveInformationHelper<BeaconAvailable>(getApplicationContext()).readList(sessionId + Constants.BEACONS_AVAILABLES);
                //completar con los encontrados la distancia a la cual se encuentra el device
                List<BeaconAvailable> beaconsDetected = new ArrayList<>();
                for(Beacon b : params)
                {
                    //int counter = 0;
                    for(BeaconAvailable ba : beaconAvailables)
                    {
                        if(ba.getMac().equals(b.getMacAddress())) {
//                        if(counter >2)
//                            break;
//                        if(counter > 1)
//                            ba.setDistance(Utils.computeAccuracy(b) + 20);
//                        else

                            ba.setDistance(Utils.computeAccuracy(b));
                            beaconsDetected.add(ba);
                            mLog.info("Beacons - Distances from beacon " + ba.getMac() + " is " + ba.getDistance());
                            //counter++;
                        }
                    }
                }

                double[][] positions = new double[beaconsDetected.size()][2];
                double[] distances= new double[beaconsDetected.size()];

                for (int i = 0; i < beaconsDetected.size(); i++){
                    positions[i] = new double[]{beaconsDetected.get(i).getPositionX(), beaconsDetected.get(i).getPositionY()};
                    distances[i] = beaconsDetected.get(i).getDistance();
                }

                TrilaterationFunction trilaterationFunction = new TrilaterationFunction(positions, distances);
                NonLinearLeastSquaresSolver solver = new NonLinearLeastSquaresSolver(trilaterationFunction, new LevenbergMarquardtOptimizer());
                LeastSquaresOptimizer.Optimum optimum = solver.solve();
                double[] locationPoint = optimum.getPoint().toArray();

                mLog.info("Beacons - Location Point - X: " + locationPoint[0] + " Y: " + locationPoint[1]);

                String mDeviceId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
                final String NAMESPACE = "http://winsystemsintl.com/";
                final String METHOD_NAME = "UpdateBeaconRunnerPosition";
                final String SOAP_ACTION = "http://winsystemsintl.com/IDATAService/UpdateBeaconRunnerPosition";

                SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
                DATA_BeaconRunnerDistance beaconRequest = new DATA_BeaconRunnerDistance();
                beaconRequest.setSessionId(sessionId);
                beaconRequest.setDeviceId(mDeviceId);
                beaconRequest.setPositionX(String.valueOf(locationPoint[0]));
                beaconRequest.setPositionY(String.valueOf(locationPoint[1]));
                beaconRequest.setFloorId(BeaconWithLowestDistance(beaconsDetected).getPositionFloorId().toString());
                DATA_PostActionResponse beaconResponse = new DATA_PostActionResponse();

                PropertyInfo pi = new PropertyInfo();
                pi.setName("beaconInfo");
                pi.setValue(beaconRequest);
                pi.setType(beaconRequest.getClass());

                request.addProperty(pi);

                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                envelope.dotNet = true;
                envelope.setOutputSoapObject(request);
                envelope.addMapping(NAMESPACE, "DATA_BeaconRunnerDistance", beaconRequest.getClass());
                envelope.addMapping(NAMESPACE, "DATA_PostActionResponse", beaconResponse.getClass());

                HttpTransportSE transporte = new HttpTransportSE(url, 60000);


                transporte.call(SOAP_ACTION, envelope);

                SoapObject resSoap =(SoapObject)envelope.getResponse();

                beaconResponse.setResponseCode(Integer.parseInt(resSoap.getProperty(0).toString()));
                beaconResponse.setResponseCodeDescription(resSoap.getProperty(1).toString());
                beaconResponse.setSessionId(resSoap.getProperty(2).toString());

            }
            catch (Exception e)
            {
                Log.e("PahoMqttService", e.getMessage());
                mLog.error("PahoMqttService - PushBeaconRunnerDistance BEACONS - " + e.getMessage(), e);

            }
        }

        private BeaconAvailable BeaconWithLowestDistance(List<BeaconAvailable> beaconsDetected) {
            Double distance = 1000000000.0;
            BeaconAvailable nearBeacon = null;
            for(BeaconAvailable b : beaconsDetected)
            {
                if(b.getDistance() < distance) {
                    distance = b.getDistance();
                    nearBeacon = b;
                }
            }

            return nearBeacon;
        }
    }
    private class UpdateLayoutAssignedTaskList extends AsyncTask<String,Void, Boolean> {
        private String topic;
        private String message;

        @Override
        protected Boolean doInBackground(String... params) {

            try {
                AlarmService service = ServiceFactory.getInstance().getAlarmService(getApplicationContext());
                service.setUrl(url);
                service.getAll(sessionId, getApplicationContext());
                topic   = params[0];
                message = params[1];
                Log.d("BeaconBackgroundThread", "DoInBackground Success");
                return true;
            } catch (Exception e){
                mLog.error("PahoMqttService - UpdateLayoutAssignedTaskList  - " + e.getMessage(), e);
                Log.e("BeaconBackgroundThread", "DoInBackground Exception: " + e);
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean result){
            if(result)
            {
                notifyUserNewTaskArrive(topic, message);
                broadcastReceivedMessage(NEW_TASK_ARRIVE);
            }
        }

    }
    private class KeepALiveSmartfloorWebService extends AsyncTask<String,Void, Boolean> {
        private ResponseCode responseCode;
        @Override
        protected Boolean doInBackground(String... params) {

            try {
                DATA_PostActionResponse service = ServiceFactory.getInstance().getLoginService(getApplicationContext()).keepALive(sessionId, url);
                Log.i("KeepALive", "DoInBackground Success");
                responseCode = service.getResponseCode();
                return responseCode == ResponseCode.DATA_RC_OK;
            } catch (Exception e){
                mLog.error("PahoMqttService - KeepALiveSmartfloorWebService  - ", e);
                Log.e("KeepALive", "DoInBackground Exception: " + e);
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean result){
            if(!result)
            {
                if(responseCode != ResponseCode.DATA_RC_TIMEOUT_EXCEPTION )
                {
                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    String _userName = prefs.getString("userName", "");
                    String _password = prefs.getString("userPassword", "");
                    String deviceId =  Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
                    ServiceFactory.getInstance().getLoginService(getApplicationContext()).loginUser(getApplicationContext(), _userName, _password, url,deviceId);
                }
            }
        }
    }
}
