package com.wynsistems.alarmas.service.message;

import android.content.Context;
import android.util.Log;

import com.wynsistems.alarmas.common.AndroidLogger;
import com.wynsistems.alarmas.helpers.LogHelper;
import com.wynsistems.alarmas.helpers.SaveInformationHelper;
import com.wynsistems.alarmas.model.LayoutSiteManagerAvailable;
import com.wynsistems.alarmas.model.LayoutSiteManagerRunnerConversationItem;
import com.wynsistems.alarmas.service.ResponseCode;
import com.wynsistems.alarmas.service.alarm.ServerEntitiesBuilder;
import com.wynsistems.alarmas.service.message.request.DATA_GetConversationHistoryRequest;
import com.wynsistems.alarmas.service.message.request.DATA_GetConversationsOfTodayRequest;
import com.wynsistems.alarmas.service.message.request.DATA_MessageToRequest;
import com.wynsistems.alarmas.service.message.response.DATA_GetConversationHistoryResponse;
import com.wynsistems.alarmas.service.message.response.DATA_GetConversationsOfTodayResponse;
import com.wynsistems.alarmas.service.message.response.DATA_SendMessageResponse;
import com.wynsistems.alarmas.view.utils.Constants;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

public class DefaultMessageService implements MessageService{

    private static DefaultMessageService instance = null;
    private static Context _context;
    private static AndroidLogger mLog;
    public static MessageService getInstance(Context context) {
        mLog = LogHelper.getLogger(context);
        if(instance == null){
            instance = new DefaultMessageService();
            _context = context;
        }
        return instance;
    }

    @Override
    public DATA_GetConversationHistoryResponse getConversationHistory(String session, long managerId, String url) {

        final String NAMESPACE = "http://winsystemsintl.com/";
        final String METHOD_NAME = "GetConversationHistory";
        final String SOAP_ACTION = "http://winsystemsintl.com/IDATAService/GetConversationHistory";

        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);

        DATA_GetConversationHistoryRequest getConversationHistoryRequest = new DATA_GetConversationHistoryRequest();
        getConversationHistoryRequest.setSession(session);
        getConversationHistoryRequest.setManagerId(managerId);


        DATA_GetConversationHistoryResponse getConversationHistoryResponse = new DATA_GetConversationHistoryResponse();

        PropertyInfo pi = new PropertyInfo();
        pi.setName("getConversationHistory");
        pi.setValue(getConversationHistoryRequest);
        pi.setType(getConversationHistoryRequest.getClass());

        request.addProperty(pi);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;

        envelope.setOutputSoapObject(request);

        envelope.addMapping(NAMESPACE, "DATA_GetConversationHistoryRequest", getConversationHistoryRequest.getClass());
        envelope.addMapping(NAMESPACE, "DATA_GetConversationHistoryResponse", getConversationHistoryResponse.getClass());

        HttpTransportSE transporte = new HttpTransportSE(url, 60000);

        try {
            transporte.call(SOAP_ACTION, envelope);

            SoapObject resSoap = (SoapObject) envelope.getResponse();

            getConversationHistoryResponse.setResponseCode(Integer.parseInt(resSoap.getProperty(0).toString()));
            getConversationHistoryResponse.setResponseCodeDescription(resSoap.getProperty(1).toString());
            getConversationHistoryResponse.setSessionId(resSoap.getProperty(2).toString());
            if(getConversationHistoryResponse.getResponseCode() == ResponseCode.DATA_RC_OK)
                getConversationHistoryResponse.setMessages(ServerEntitiesBuilder.BuildMessageListFromServerResponse((SoapObject) resSoap.getProperty(3)));
        } catch (Exception e) {
            mLog.error("DefaultMessageService - getConversationHistory - ", e);
            Log.e("DefaultMessageService", e.getMessage());
        }

        return getConversationHistoryResponse;
    }

    @Override
    public DATA_GetConversationsOfTodayResponse getConversationsOfToday(String session, String url) {
        final String NAMESPACE = "http://winsystemsintl.com/";
        final String METHOD_NAME = "GetConversationsOfToday";
        final String SOAP_ACTION = "http://winsystemsintl.com/IDATAService/GetConversationsOfToday";

        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        DATA_GetConversationsOfTodayRequest getConversationOfTodayRequest = new DATA_GetConversationsOfTodayRequest();
        getConversationOfTodayRequest.setSession(session);

        DATA_GetConversationsOfTodayResponse getConversationOfTodayResponse = new DATA_GetConversationsOfTodayResponse();

        PropertyInfo pi = new PropertyInfo();
        pi.setName("getConversationOfToday");
        pi.setValue(getConversationOfTodayRequest);
        pi.setType(getConversationOfTodayRequest.getClass());

        request.addProperty(pi);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;

        envelope.setOutputSoapObject(request);

        envelope.addMapping(NAMESPACE, "DATA_GetConversationsOfTodayRequest", getConversationOfTodayRequest.getClass());
        envelope.addMapping(NAMESPACE, "DATA_GetConversationsOfTodayResponse", getConversationOfTodayResponse.getClass());

        HttpTransportSE transporte = new HttpTransportSE(url, 60000);

        try {
            transporte.call(SOAP_ACTION, envelope);

            SoapObject resSoap = (SoapObject) envelope.getResponse();

            getConversationOfTodayResponse.setResponseCode(Integer.parseInt(resSoap.getProperty(0).toString()));
            getConversationOfTodayResponse.setResponseCodeDescription(resSoap.getProperty(1).toString());
            getConversationOfTodayResponse.setSessionId(resSoap.getProperty(2).toString());
            if(getConversationOfTodayResponse.getResponseCode() == ResponseCode.DATA_RC_OK) {
                getConversationOfTodayResponse.setHeaders(ServerEntitiesBuilder.BuildHeadersListFromServerResponse((SoapObject) resSoap.getProperty(3)));
                getConversationOfTodayResponse.setManagers(ServerEntitiesBuilder.BuildManagersListFromServerResponse((SoapObject) resSoap.getProperty(4)));

                SaveInformationHelper<LayoutSiteManagerRunnerConversationItem> saveHeaders = new SaveInformationHelper<>(_context);
                SaveInformationHelper<LayoutSiteManagerAvailable> saveManagers = new SaveInformationHelper<>(_context);

                saveHeaders.writeList(getConversationOfTodayResponse.getHeaders(), Constants.TODAY_MESSAGES);
                saveManagers.writeList(getConversationOfTodayResponse.getManagers(), Constants.MANAGERS_AVAILABLES);
            }

        } catch (Exception e) {
            mLog.error("DefaultMessageService - getConversationsOfToday - ", e);
            Log.e("DefaultMessageService", e.getMessage());
        }

        return getConversationOfTodayResponse;
    }

    @Override
    public DATA_SendMessageResponse sendMessageTo(String session, long managerId, String message, String url) {
        final String NAMESPACE = "http://winsystemsintl.com/";
        final String METHOD_NAME = "SendMessageTo";
        final String SOAP_ACTION = "http://winsystemsintl.com/IDATAService/SendMessageTo";

        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        DATA_MessageToRequest messageToRequest = new DATA_MessageToRequest();
        messageToRequest.setSession(session);
        messageToRequest.setManagerId(managerId);
        messageToRequest.setMessage(message);

        DATA_SendMessageResponse response = new DATA_SendMessageResponse();

        PropertyInfo pi = new PropertyInfo();
        pi.setName("message");
        pi.setValue(messageToRequest);
        pi.setType(messageToRequest.getClass());

        request.addProperty(pi);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;

        envelope.setOutputSoapObject(request);

        envelope.addMapping(NAMESPACE, "DATA_MessageToRequest", messageToRequest.getClass());
        envelope.addMapping(NAMESPACE, "DATA_SendMessageResponse", response.getClass());

        HttpTransportSE transporte = new HttpTransportSE(url, 60000);

        try {
            transporte.call(SOAP_ACTION, envelope);

            SoapObject resSoap = (SoapObject) envelope.getResponse();

            response.setResponseCode(Integer.parseInt(resSoap.getProperty(0).toString()));
            response.setResponseCodeDescription(resSoap.getProperty(1).toString());
            response.setSessionId(resSoap.getProperty(2).toString());
            if(response.getResponseCode() == ResponseCode.DATA_RC_OK)
                response.setMessage(ServerEntitiesBuilder.BuildMessageFromServerResponse((SoapObject) resSoap.getProperty(3)));

        } catch (Exception e) {
            mLog.error("DefaultMessageService - sendMessageTo - ", e);
            Log.e("DefaultMessageService", e.getMessage());
        }

        return response;
    }
}
