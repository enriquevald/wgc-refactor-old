package com.wynsistems.alarmas.view.task;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.wynsistems.alarmas.R;
import com.wynsistems.alarmas.model.LayoutSiteItemSeverity;
import com.wynsistems.alarmas.model.LayoutSiteItemSubCategory;
import com.wynsistems.alarmas.model.LayoutSiteTaskActions;
import com.wynsistems.alarmas.service.login.response.LayoutSiteTask;
import com.wynsistems.alarmas.helpers.AlarmResourseHelper;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.List;

public class TaskByCriticalityListAdapter extends BaseAdapter {

    private List<LayoutSiteTask> tasks;
    private static LayoutInflater inflater = null;
    private Format formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    public TaskByCriticalityListAdapter(Context context, List<LayoutSiteTask> data) {
        Context context1 = context;
        this.tasks = data;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return this.tasks.size();
    }

    @Override
    public Object getItem(int position) {
        return this.tasks.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        if (vi == null)
            vi = inflater.inflate(R.layout.task_by_criticality_item_detail, null);

        LayoutSiteTask alarm = (LayoutSiteTask)getItem(position);

        TextView description = (TextView)vi.findViewById(R.id.alarm_detail_description);
        TextView terminalId = (TextView)vi.findViewById(R.id.terminal_id);
        TextView catergoryDescription = (TextView)vi.findViewById(R.id.category_description);
        TextView date = (TextView)vi.findViewById(R.id.alarm_date);
        if(alarm.getDescription() == null || alarm.getDescription().isEmpty())
            description.setVisibility(View.GONE);
        else {
            description.setText(alarm.getDescription());
            description.setVisibility(View.VISIBLE);
        }
        terminalId.setText(alarm.getTerminal().getName());
        catergoryDescription.setText(LayoutSiteItemSubCategory.forCode(alarm.getSubcategory()).getDescription(vi.getContext()));
        date.setText(formatter.format(alarm.getCreation()));

        FrameLayout imageView = (FrameLayout) vi.findViewById(R.id.alarm_criticality_type);
        GradientDrawable bgShape = (GradientDrawable)imageView.getBackground();
        bgShape.setColor(AlarmResourseHelper.getColorByCriticality(LayoutSiteItemSeverity.getById(alarm.getSeverity()), vi.getResources()));

        ImageView alarmCategoryType = (ImageView) vi.findViewById(R.id.alarm_category_image);
        alarmCategoryType.setImageResource(AlarmResourseHelper.getCategoryImageByType(LayoutSiteItemSubCategory.forCode(alarm.getSubcategory()).getGroupCategory()));

        TextView textStatus = (TextView) vi.findViewById(R.id.task_status);

        if(LayoutSiteTaskActions.GetActionLayoutSiteTaskAction(alarm.getStatus()) == LayoutSiteTaskActions.Assigned)
        {
            textStatus.setText(R.string.Pending);
            textStatus.setBackgroundResource(R.drawable.rectangle_red);
        }else
        {
            textStatus.setText(R.string.InProgress);
            textStatus.setBackgroundResource(R.drawable.rectangle_blue);
        }

        return vi;
    }
}
