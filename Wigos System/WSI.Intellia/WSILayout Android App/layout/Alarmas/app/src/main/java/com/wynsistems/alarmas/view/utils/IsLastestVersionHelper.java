package com.wynsistems.alarmas.view.utils;


import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.wynsistems.alarmas.R;

public class IsLastestVersionHelper {

    public static boolean IsLastestVersion(Context context){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        String lastVersion = preferences.getString("lastVersionApp", "");
        String currentVersion = context.getResources().getString(R.string.version);

        return lastVersion.equals(currentVersion);
    }
}
