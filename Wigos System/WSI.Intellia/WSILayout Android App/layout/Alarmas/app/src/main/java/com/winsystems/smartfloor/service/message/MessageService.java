package com.winsystems.smartfloor.service.message;

import com.winsystems.smartfloor.service.message.response.DATA_GetConversationHistoryResponse;
import com.winsystems.smartfloor.service.message.response.DATA_GetConversationsOfTodayResponse;
import com.winsystems.smartfloor.service.message.response.DATA_SendMessageResponse;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: MessageService
//
// DESCRIPTION: Interfaces and classes.
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public interface MessageService {

  DATA_GetConversationHistoryResponse getConversationHistory(String session, long managerId, String url);

  DATA_GetConversationsOfTodayResponse getConversationsOfToday(String session, String url);

  DATA_SendMessageResponse sendMessageTo(String session, long managerId, String message, String url);
}
