package com.wynsistems.alarmas.view.message;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.util.Log;

import com.wynsistems.alarmas.R;
import com.wynsistems.alarmas.common.LayoutNotificationManager;
import com.wynsistems.alarmas.common.LayoutNotificationType;
import com.wynsistems.alarmas.service.ResponseCode;
import com.wynsistems.alarmas.service.ServiceFactory;
import com.wynsistems.alarmas.service.message.response.DATA_GetConversationsOfTodayResponse;
import com.wynsistems.alarmas.service.mqtt.PahoMqttService;
import com.wynsistems.alarmas.view.utils.Constants;
import com.wynsistems.alarmas.view.utils.KioskModeActivity;
import com.wynsistems.alarmas.view.utils.ToastFactory;

public class MessageActivity extends KioskModeActivity implements  ActionBar.TabListener {
    private SharedPreferences prefs;
    private ProgressDialog dialog;
    private static String alarmServerUrl = "";
    String userId = "";
    private IntentFilter intentFilter = new IntentFilter(PahoMqttService.NEW_MESSAGE_FROM_MANAGER_ARRIVE);

    private ViewPager tabsviewPager;
    private android.support.v7.app.ActionBar mActionBar;
    private TabsMessagesAdapter mTabsAdapter;
    private LayoutNotificationManager notificationManager;
    private BroadcastReceiver messageBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            MessageActivity.this.fillTodayConversationsListView();
        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        fillTodayConversationsListView();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_history_list);

        tabsviewPager = (ViewPager) findViewById(R.id.pager);

        tabsviewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                getSupportActionBar().setSelectedNavigationItem(position);
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
        });


        mActionBar =  getSupportActionBar();
        mActionBar.removeAllTabs();

        mActionBar.setHomeButtonEnabled(false);
        mActionBar.setNavigationMode(android.app.ActionBar.NAVIGATION_MODE_TABS);

        android.support.v7.app.ActionBar.Tab messages = mActionBar.newTab().setIcon(R.drawable.ic_message_white_18dp).setTabListener(this);
        android.support.v7.app.ActionBar.Tab managers = mActionBar.newTab().setIcon(R.drawable.ic_account_circle_white_18dp).setTabListener(this);

        mActionBar.addTab(messages);
        mActionBar.addTab(managers);

        prefs = PreferenceManager.getDefaultSharedPreferences(this);//get the preferences that are allowed to be given
        prefs.registerOnSharedPreferenceChangeListener(this);//set the listener to listen for changes in the preferences

        SharedPreferences settings = MessageActivity.this.getSharedPreferences(Constants.USER_ID_PREFERENCE_EDITOR, 0);
        userId = settings.getString(Constants.USER_ID, userId);

        updatePreferencesAndLanguage();

        registerReceiver(messageBroadcastReceiver, intentFilter);
        notificationManager = new LayoutNotificationManager(getApplicationContext());
        notificationManager.removeNotificationsByType(LayoutNotificationType.MESSAGE);
    }




    @Override
    public void onTabSelected(android.support.v7.app.ActionBar.Tab tab, android.support.v4.app.FragmentTransaction fragmentTransaction) {
        tabsviewPager.setCurrentItem(tab.getPosition()); //update tab position on tap
    }

    @Override
    public void onTabUnselected(android.support.v7.app.ActionBar.Tab tab, android.support.v4.app.FragmentTransaction fragmentTransaction) {
    }

    @Override
    public void onTabReselected(android.support.v7.app.ActionBar.Tab tab, android.support.v4.app.FragmentTransaction fragmentTransaction) {
    }

    @Override
    public void onResume(){
        super.onResume();
        registerReceiver(messageBroadcastReceiver, intentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        this.dialog.dismiss();
        unregisterReceiver(messageBroadcastReceiver);
    }

    @Override
    protected void onStop() {
        this.dialog.dismiss();
        super.onStop();
    }


    private void buildDialog() {
        this.dialog = new ProgressDialog(this);
        this.dialog.setTitle(this.getResources().getString(R.string.title_progress_dialog));
        this.dialog.setMessage(this.getResources().getString(R.string.alarm_message_progress_dialog));
    }

    private void updatePreferencesAndLanguage() {
        alarmServerUrl = prefs.getString("alarm_server_address", alarmServerUrl);
    }


    private void fillTodayConversationsListView() {
        new HttpRequestTask().execute();
    }

    private class HttpRequestTask extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            buildDialog();
            MessageActivity.this.dialog.show();
        }

        @Override
        public Boolean doInBackground(Void... params) {
            try {
                DATA_GetConversationsOfTodayResponse response = ServiceFactory.getInstance().getMessageService(getApplicationContext()).getConversationsOfToday(userId, alarmServerUrl);

                return response.getResponseCode() == ResponseCode.DATA_RC_OK;
            } catch (Exception e) {
                Log.e("MessageActivity", e.getMessage(), e);
                return false;
            }

        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            if(result) {
                mTabsAdapter = new TabsMessagesAdapter(getSupportFragmentManager());
                tabsviewPager.setAdapter(mTabsAdapter);
            }else{
                 ToastFactory.buildSimpleToast(getResources().getString(R.string.error_on_Action), MessageActivity.this);
             }
         }
    }
}
