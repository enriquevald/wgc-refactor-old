package com.winsystems.smartfloor.view.alarms;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.winsystems.smartfloor.R;
import com.winsystems.smartfloor.model.Alarm;
import com.winsystems.smartfloor.model.SummarizedAlarms;
import com.winsystems.smartfloor.view.task.TaskByCriticalityListAdapter;
import com.winsystems.smartfloor.view.utils.Constants;
import com.winsystems.smartfloor.view.utils.KioskModeActivity;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: AlarmByCriticalityActivity
//
// DESCRIPTION: Activity that implements alarm functioanlity.
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public class AlarmByCriticalityActivity extends KioskModeActivity {

  private SummarizedAlarms currentSummarizedAlarms;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_alarm_by_criticality);
    currentSummarizedAlarms = (SummarizedAlarms) getIntent().getSerializableExtra(Constants.SELECTED_GROUP_ALARMS);
    ListView alarmsView =  (ListView) findViewById( R.id.tasks_by_criticality);
    ActionBar actionBar = getSupportActionBar();
    final TaskByCriticalityListAdapter taskByCriticalityListAdapter = new TaskByCriticalityListAdapter(this,currentSummarizedAlarms.getTasks());

    alarmsView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Alarm item = (Alarm) taskByCriticalityListAdapter.getItem(position);

        Intent intent = new Intent(AlarmByCriticalityActivity.this, AlarmDetailActivity.class);
        intent.putExtra(Constants.SELECTED_ALARM, item);
        startActivity(intent);
      }
    });

    actionBar.setTitle(currentSummarizedAlarms.getDescription());
    actionBar.setDisplayUseLogoEnabled(true);

    alarmsView.setAdapter(taskByCriticalityListAdapter);
  }



  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();

    //noinspection SimplifiableIfStatement
    if (id == R.id.action_settings) {
      return true;
    }

    return super.onOptionsItemSelected(item);
  }
}
