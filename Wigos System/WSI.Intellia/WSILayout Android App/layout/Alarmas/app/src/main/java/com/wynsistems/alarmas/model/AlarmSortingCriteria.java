package com.wynsistems.alarmas.model;

public enum AlarmSortingCriteria {
    NEWEST,
    OLDEST, sortPreference;

    public static AlarmSortingCriteria getFromString(String i){
        switch (i){
            case "1":
                return NEWEST;
            case "2":
                return OLDEST;
            default:
                return NEWEST;
        }
    }
}
