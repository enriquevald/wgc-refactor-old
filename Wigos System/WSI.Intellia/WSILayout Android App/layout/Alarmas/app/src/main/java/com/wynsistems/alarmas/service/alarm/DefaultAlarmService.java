package com.wynsistems.alarmas.service.alarm;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;

import com.wynsistems.alarmas.common.AndroidLogger;
import com.wynsistems.alarmas.helpers.LogHelper;
import com.wynsistems.alarmas.model.Alarm;
import com.wynsistems.alarmas.model.LayoutSiteItemGroupCategory;
import com.wynsistems.alarmas.model.LayoutSiteItemSeverity;
import com.wynsistems.alarmas.model.LayoutSiteTaskActions;
import com.wynsistems.alarmas.repository.RepositoryFactory;
import com.wynsistems.alarmas.repository.TaskRepository;
import com.wynsistems.alarmas.service.DATA_PostActionResponse;
import com.wynsistems.alarmas.service.ResponseCode;
import com.wynsistems.alarmas.service.alarm.request.DATA_CreateAlarm;
import com.wynsistems.alarmas.service.alarm.request.DATA_GetAssignedLayoutSiteTasks;
import com.wynsistems.alarmas.service.alarm.request.DATA_UpdateLayoutSiteTaskInfo;
import com.wynsistems.alarmas.service.alarm.response.DATA_LayoutSiteTaskAssignedResponse;
import com.wynsistems.alarmas.service.login.response.LayoutSiteTask;
import com.wynsistems.alarmas.service.login.response.LayoutSiteTaskTerminal;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class DefaultAlarmService implements AlarmService {

    private static DefaultAlarmService instance = null;
    private static TaskRepository taskRepository;
    private static String url;
    private AndroidLogger mLog;

    private DefaultAlarmService(Context context){
        taskRepository = RepositoryFactory.getInstance().getAlarmRepository(context);
        mLog = LogHelper.getLogger(context);
    }

    public static AlarmService getInstance(Context context) {
        if(instance == null){
            instance = new DefaultAlarmService(context);
        }
        return instance;
    }

    @Override
    public AlarmService setUrl(String url) {
        this.url = url;
        return this;
    }

    @Override
    public DATA_PostActionResponse assignAlarm(String taskId, String userId, LayoutSiteTaskActions action) {
        final String NAMESPACE = "http://winsystemsintl.com/";
        final String METHOD_NAME = "UpdateLayoutSiteTaskInfo";
        final String SOAP_ACTION = "http://winsystemsintl.com/IDATAService/UpdateLayoutSiteTaskInfo";

        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);

        DATA_UpdateLayoutSiteTaskInfo updateLayoutSiteTaskInfoRequest = new DATA_UpdateLayoutSiteTaskInfo();
        updateLayoutSiteTaskInfoRequest.setSessionId(userId);
        updateLayoutSiteTaskInfoRequest.setTaskId(taskId);
        updateLayoutSiteTaskInfoRequest.setAction(LayoutSiteTaskActions.GetActionId(action));

        DATA_PostActionResponse updateLayoutSiteTaskInfoResponse = new DATA_PostActionResponse();

        PropertyInfo pi = new PropertyInfo();
        pi.setName("updatedValues");
        pi.setValue(updateLayoutSiteTaskInfoRequest);
        pi.setType(updateLayoutSiteTaskInfoRequest.getClass());

        request.addProperty(pi);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;

        envelope.setOutputSoapObject(request);

        envelope.addMapping(NAMESPACE, "DATA_UpdateLayoutSiteTaskInfo", updateLayoutSiteTaskInfoRequest.getClass());
        envelope.addMapping(NAMESPACE, "DATA_PostActionResponse", updateLayoutSiteTaskInfoResponse.getClass());

        HttpTransportSE transporte = new HttpTransportSE(url, 60000);

        try {
            transporte.call(SOAP_ACTION, envelope);

            SoapObject resSoap = (SoapObject) envelope.getResponse();

            updateLayoutSiteTaskInfoResponse.setResponseCode(Integer.parseInt(resSoap.getProperty(0).toString()));
            updateLayoutSiteTaskInfoResponse.setResponseCodeDescription(resSoap.getProperty(1).toString());
            updateLayoutSiteTaskInfoResponse.setSessionId(resSoap.getProperty(2).toString());

        } catch (Exception e) {
            mLog.error("DefaultAlarmService - assignAlarm - ", e);
            Log.e("DefaultAlarmService", e.getMessage());
        }

        return updateLayoutSiteTaskInfoResponse;
    }

    @Override
    public DATA_PostActionResponse createAlarm(Alarm alarm, String photoToSend, String userId, String url) {

        final String NAMESPACE = "http://winsystemsintl.com/";
        final String METHOD_NAME = "CreateAlarm";
        final String SOAP_ACTION = "http://winsystemsintl.com/IDATAService/CreateAlarm";

        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        DATA_CreateAlarm createAlarmRequest = new DATA_CreateAlarm();

        createAlarmRequest.setSubCategory(alarm.getSiteItemSubCategory().getValue());
        createAlarmRequest.setCategory(alarm.getSiteItemSubCategory().getCategory().getValue());
        createAlarmRequest.setDescription(alarm.getDescription());
        createAlarmRequest.setTerminalId(alarm.getTerminal().getProvider());
        createAlarmRequest.setUserId(userId);
        if(photoToSend != null) {
            String mediaEncoded = Base64.encodeToString(compressMedia(photoToSend), Base64.DEFAULT);
            createAlarmRequest.setImage(mediaEncoded);
        }
        DATA_PostActionResponse createAlarmResponse = new DATA_PostActionResponse();

        PropertyInfo pi = new PropertyInfo();
        pi.setName("alarm");
        pi.setValue(createAlarmRequest);
        pi.setType(createAlarmRequest.getClass());

        request.addProperty(pi);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;

        envelope.setOutputSoapObject(request);

        envelope.addMapping(NAMESPACE, "DATA_CreateAlarm", createAlarmRequest.getClass());
        envelope.addMapping(NAMESPACE, "DATA_PostActionResponse", createAlarmResponse.getClass());

        HttpTransportSE transporte = new HttpTransportSE(url, 60000);

        try
        {
            transporte.call(SOAP_ACTION, envelope);

            SoapObject resSoap =(SoapObject)envelope.getResponse();

            createAlarmResponse.setResponseCode(Integer.parseInt(resSoap.getProperty(0).toString()));
            createAlarmResponse.setResponseCodeDescription(resSoap.getProperty(1).toString());
            createAlarmResponse.setSessionId(resSoap.getProperty(2).toString());
        }
        catch (Exception e)
        {
            createAlarmResponse.setResponseCode(9);
            mLog.error("DefaultAlarmService - createAlarm - ", e);
            Log.e("DefaultAlarmService", e.getMessage());
        }

        return createAlarmResponse;
    }


    @Override
    public LayoutSiteTask getById(String id) {
        return this.taskRepository.getById(id);
    }

    @Override
    public List<LayoutSiteTask> getAll(String sessionId, Context context) {

        final String NAMESPACE = "http://winsystemsintl.com/";
        final String URL = url;
        final String METHOD_NAME = "GetAssignedTask";
        final String SOAP_ACTION = "http://winsystemsintl.com/IDATAService/GetAssignedTask";

        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);

        DATA_GetAssignedLayoutSiteTasks assinedTaskRequest = new DATA_GetAssignedLayoutSiteTasks();
        assinedTaskRequest.setSessionId(sessionId);

        DATA_LayoutSiteTaskAssignedResponse assinedTaskResponse = new DATA_LayoutSiteTaskAssignedResponse();

        PropertyInfo pi = new PropertyInfo();
        pi.setName("session");
        pi.setValue(assinedTaskRequest);
        pi.setType(assinedTaskRequest.getClass());

        request.addProperty(pi);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;

        envelope.setOutputSoapObject(request);

        envelope.addMapping(NAMESPACE, "DATA_GetAssignedLayoutSiteTasks", assinedTaskRequest.getClass());
        envelope.addMapping(NAMESPACE, "DATA_LayoutSiteTaskAssignedResponse", assinedTaskResponse.getClass());

        HttpTransportSE transporte = new HttpTransportSE(URL, 60000);

        try {
            transporte.call(SOAP_ACTION, envelope);

            SoapObject resSoap = (SoapObject) envelope.getResponse();

            assinedTaskResponse.setResponseCode(Integer.parseInt(resSoap.getProperty(0).toString()));
            assinedTaskResponse.setResponseCodeDescription(resSoap.getProperty(1).toString());
            assinedTaskResponse.setSessionId(resSoap.getProperty(2).toString());

            SoapObject tasks = (SoapObject) resSoap.getProperty(3);
            int taskCount = tasks.getPropertyCount();
            List<LayoutSiteTask> availableTasks = new ArrayList<>();
            for (int i = 0; i < taskCount; i++) {
                SoapObject ic = (SoapObject) tasks.getProperty(i);

                LayoutSiteTask task = new LayoutSiteTask();
                task.setAccountId(Integer.parseInt(ic.getProperty(0).toString()));
                task.setCategory(Integer.parseInt(ic.getProperty(1).toString()));
                task.setCreation(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(ic.getProperty(2).toString()));
                task.setDescription(ic.getProperty(3) == null? "" : ic.getProperty(3).toString());
                task.setId(Integer.parseInt(ic.getProperty(5).toString()));
                task.setSeverity(Integer.parseInt(ic.getProperty(6).toString()));
                task.setStatus(Integer.parseInt(ic.getProperty(8).toString()));
                task.setSubcategory(Integer.parseInt(ic.getProperty(9).toString()));
                task.setTerminalId(Integer.parseInt(ic.getProperty(11).toString()));

                SoapObject t = (SoapObject) ic.getProperty(10);
                LayoutSiteTaskTerminal terminal = new LayoutSiteTaskTerminal();
                terminal.setName(t.getProperty(3).toString());
                terminal.setProvider(t.getProperty(4) == null ? "" : t.getProperty(4).toString());
                terminal.setArea(t.getProperty(0) == null ? "" : t.getProperty(0).toString());
                terminal.setBank(t.getProperty(1) == null ? "" : t.getProperty(1).toString());
                terminal.setModel(t.getProperty(2) == null ? "" : t.getProperty(2).toString());

                task.setTerminal(terminal);

                availableTasks.add(task);
            }

            assinedTaskResponse.setAvailableTasks(availableTasks);
        } catch (Exception e) {
            mLog.error("DefaultAlarmService - getAll - ", e);
            Log.e("DefaultAlarmService", e.getMessage());
        }

        if (assinedTaskResponse.getResponseCode() == ResponseCode.DATA_RC_OK)
        {
            taskRepository.saveFromAlarmResponse(assinedTaskResponse.getAvailableTasks(), context);
            return assinedTaskResponse.getAvailableTasks();
        }else
            return null;

    }



    @Override
    public List<LayoutSiteTask> getAlarmByCriticality(LayoutSiteItemSeverity alarmCriticality) {
        return taskRepository.getAlarmByCriticality(alarmCriticality);
    }

    @Override
    public List<LayoutSiteTask> getAlarmByCategory(LayoutSiteItemGroupCategory type) {
        return taskRepository.getAlarmByCategory(type);
    }

    @Override
    public List<LayoutSiteTask> getAlarmsInProgress() {
        return taskRepository.getAlarmsInProgress();
    }

    @Override
    public List<LayoutSiteTask> getAlarmsAssigned() {
        return taskRepository.getAlarmsAssigned();
    }

    private byte[] compressMedia(String imagePath) {
        Bitmap bm = BitmapFactory.decodeFile(imagePath);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 75, stream);
        if(bm!=null)
        {
            bm.recycle();
            bm=null;
        }
        return stream.toByteArray();
    }
}
