package com.wynsistems.alarmas.view.alarms;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.wynsistems.alarmas.R;
import com.wynsistems.alarmas.helpers.SaveInformationHelper;
import com.wynsistems.alarmas.model.LayoutSiteAlarm;
import com.wynsistems.alarmas.view.setting.SettingsActivity;
import com.wynsistems.alarmas.view.terminal.LayoutSiteAlarmListAdapter;
import com.wynsistems.alarmas.view.utils.Constants;
import com.wynsistems.alarmas.view.utils.KioskModeActivity;

public class CreatedAlarmsListActivity extends KioskModeActivity implements SharedPreferences.OnSharedPreferenceChangeListener {

    private SharedPreferences prefs;
    private ProgressDialog dialog;
    private static String alarmServerUrl = "http://10.254.5.102:62475";
    String userId = "";
    private static String loggedUserAlarmServiceAddress;
    private SaveInformationHelper<LayoutSiteAlarm> saveInformationHelper;

    @Override
    protected void onStart() {
        super.onStart();
        fillAlarmListView();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm_list);

        prefs = PreferenceManager.getDefaultSharedPreferences(this);//get the preferences that are allowed to be given
        prefs.registerOnSharedPreferenceChangeListener(this);//set the listener to listen for changes in the preferences

        SharedPreferences settings = CreatedAlarmsListActivity.this.getSharedPreferences(Constants.USER_ID_PREFERENCE_EDITOR, 0);
        userId = settings.getString(Constants.USER_ID, userId);


        TextView menuDescription = (TextView)this.findViewById(R.id.new_alarm_item_description);
        FrameLayout frameLayout = (FrameLayout) this.findViewById(R.id.new_alarm_item_icon);

        menuDescription.setText(this.getResources().getString(R.string.new_alarm_item_text));
        frameLayout.setBackgroundResource(R.drawable.ic_create_alarm);

        frameLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CreatedAlarmsListActivity.this, NewAlarmActivity.class);
                startActivity(intent);
            }
        });

        menuDescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CreatedAlarmsListActivity.this, NewAlarmActivity.class);
                startActivity(intent);
            }
        });

        buildDialog();

        SharedPreferences.Editor editor = settings.edit();
        editor.putString(Constants.LOGGED_USER_CREATED_ALARM_SERVICE_ADDRESS, alarmServerUrl);
        editor.apply();

        loggedUserAlarmServiceAddress = alarmServerUrl;

        saveInformationHelper = new SaveInformationHelper<>(this);
    }


    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent i = new Intent(this, SettingsActivity.class);
            startActivityForResult(i, 1);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStop() {
        this.dialog.dismiss();
        super.onStop();
        prefs.unregisterOnSharedPreferenceChangeListener(this);
    }




    private void buildDialog() {
        this.dialog = new ProgressDialog(CreatedAlarmsListActivity.this);
        this.dialog.setTitle(CreatedAlarmsListActivity.this.getResources().getString(R.string.title_progress_dialog));
        this.dialog.setMessage(CreatedAlarmsListActivity.this.getResources().getString(R.string.alarm_message_progress_dialog));
    }

    private void fillAlarmListView() {
        ListView alarmListView = (ListView) findViewById( R.id.taskDetailList);

        final ListAdapter listAdapter = new LayoutSiteAlarmListAdapter(this, saveInformationHelper.readList(userId + Constants.ALARMS_CREATED_BY_USER));
        alarmListView.setAdapter(listAdapter);
    }
}
