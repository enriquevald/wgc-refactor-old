package com.winsystems.smartfloor.helpers;


import android.content.Context;
import android.util.Log;

import com.winsystems.smartfloor.common.AndroidLogger;
import com.winsystems.smartfloor.view.utils.Constants;

import java.io.File;
import java.io.IOException;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: LogHelper
//
// DESCRIPTION:  Implement method for get or set o create file log.
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public class LogHelper {
  private static SaveInformationHelper<String> _logsFile;

  public static void Configure(Context context){
    _logsFile = new SaveInformationHelper<>(context);
    File myfile = context.getFileStreamPath(Constants.LOG);
    try {
      if (myfile.exists() || myfile.createNewFile())
        Log.i("LogHelper", "Log File created");
    } catch (IOException e) {
      Log.e("LogHelper", e.getMessage());
      e.printStackTrace();
    }
  }

  public static AndroidLogger getLogger(Context context) {
    if(_logsFile == null)
      Configure(context);
    return new AndroidLogger(_logsFile);
  }

}

