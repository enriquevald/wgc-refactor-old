package com.winsystems.smartfloor.service.update.request;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.util.Hashtable;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: DATA_UpdateApplicationRequest.
//
// DESCRIPTION: DATA_UpdateApplicationRequest.
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public class DATA_UpdateApplicationRequest implements KvmSerializable {

  private String sessionId;

  public void setSessionId(String sessionId) {
    this.sessionId = sessionId;
  }

  @Override
  public Object getProperty(int i) {
    return sessionId;
  }

  @Override
  public int getPropertyCount() {
    return 1;
  }

  @Override
  public void setProperty(int i, Object o) {
    sessionId = o.toString();
  }

  @Override
  public void getPropertyInfo(int i, Hashtable hashtable, PropertyInfo propertyInfo) {
    propertyInfo.type = PropertyInfo.STRING_CLASS;
    propertyInfo.name = "SessionId";
  }
}
