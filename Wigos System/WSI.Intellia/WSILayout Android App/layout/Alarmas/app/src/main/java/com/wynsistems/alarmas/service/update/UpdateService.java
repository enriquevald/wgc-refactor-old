package com.wynsistems.alarmas.service.update;

import com.wynsistems.alarmas.service.update.response.DATA_UpdateApplicationResponse;

public interface UpdateService {

    public DATA_UpdateApplicationResponse getLastestApplicationVersion(String url, String sessionId);
}
