package com.winsystems.smartfloor.service.alarm;

import android.content.Context;

import com.winsystems.smartfloor.model.Alarm;
import com.winsystems.smartfloor.model.LayoutSiteItemGroupCategory;
import com.winsystems.smartfloor.model.LayoutSiteItemSeverity;
import com.winsystems.smartfloor.model.LayoutSiteTaskActions;
import com.winsystems.smartfloor.service.DATA_PostActionResponse;
import com.winsystems.smartfloor.service.login.response.LayoutSiteTask;

import java.util.List;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: AlarmService
//
// DESCRIPTION: Interfaces and classes.
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public interface AlarmService {
  LayoutSiteTask getById(String id);
  List<LayoutSiteTask> getAll(String sessionId, Context context);
  List<LayoutSiteTask> getAlarmByCriticality(LayoutSiteItemSeverity alarmCriticality);
  List<LayoutSiteTask> getAlarmByCategory(LayoutSiteItemGroupCategory type);
  List<LayoutSiteTask> getAlarmsInProgress();
  List<LayoutSiteTask> getAlarmsAssigned();
  AlarmService setUrl(String url);

  DATA_PostActionResponse assignAlarm(String id, String userId, LayoutSiteTaskActions action, String scaleReason);
  DATA_PostActionResponse createAlarm(Alarm alarm, String photoToSend, String userId, String url, Boolean isQrInput);

}
