package com.winsystems.smartfloor.model;

import android.content.Context;

import com.winsystems.smartfloor.R;

public enum LayoutSiteItemSubCategory {
  SoporteCallAttendant(19001) {
    @Override
    public LayoutSiteItemGroupCategory getGroupCategory() {
      return LayoutSiteItemGroupCategory.Support;
    }
    @Override
    public String getDescription(Context context) {
      return context.getResources().getString(R.string.SoporteCallAttendant);
    }

    @Override
    public LayoutSiteItemCategory getCategory() {
      return LayoutSiteItemCategory.PLAYER;
    }
  },
  JackpotJackpot(17001) {
    @Override
    public LayoutSiteItemGroupCategory getGroupCategory() {
      return LayoutSiteItemGroupCategory.PayCheck;
    }

    @Override
    public String getDescription(Context context) {
      return context.getResources().getString(R.string.JackpotJackpot);
    }

    @Override
    public LayoutSiteItemCategory getCategory() {
      return LayoutSiteItemCategory.PLAYER;
    }
  },
  JackpotCreditosCancelados(17002){
    @Override
    public LayoutSiteItemGroupCategory getGroupCategory() {
      return LayoutSiteItemGroupCategory.PayCheck;
    }
    @Override
    public String getDescription(Context context) {
      return context.getResources().getString(R.string.JackpotCreditosCancelados);
    }

    @Override
    public LayoutSiteItemCategory getCategory() {
      return LayoutSiteItemCategory.PLAYER;
    }
  },
  HighRoller(17016){
    @Override
    public LayoutSiteItemGroupCategory getGroupCategory() {
      return LayoutSiteItemGroupCategory.PayCheck;
    }
    @Override
    public String getDescription(Context context) {
      return context.getResources().getString(R.string.HighRoller);
    }

    @Override
    public LayoutSiteItemCategory getCategory() {
      return LayoutSiteItemCategory.PLAYER;
    }
  },
  HighRoller_Anonymous(17032){
    @Override
    public LayoutSiteItemGroupCategory getGroupCategory() {
      return LayoutSiteItemGroupCategory.PayCheck;
    }
    @Override
    public String getDescription(Context context) {
      return context.getResources().getString(R.string.HighRoller_Anonymous);
    }

    @Override
    public LayoutSiteItemCategory getCategory() {
      return LayoutSiteItemCategory.PLAYER;
    }
  },

  StackerFull(18002) {
    @Override
    public LayoutSiteItemGroupCategory getGroupCategory() {
      return LayoutSiteItemGroupCategory.JamBill;
    }

    @Override
    public String getDescription(Context context) {
      return context.getResources().getString(R.string.StackerFull);
    }
  },
  StackerAlmostFull(18001) {
    @Override
    public LayoutSiteItemGroupCategory getGroupCategory() {
      return LayoutSiteItemGroupCategory.JamBill;
    }

    @Override
    public String getDescription(Context context) {
      return context.getResources().getString(R.string.StackerAlmostFull);
    }
  },
  PuertaSlotDoor(12016) {
    @Override
    public LayoutSiteItemGroupCategory getGroupCategory() {
      return LayoutSiteItemGroupCategory.Intrusion;
    }

    @Override
    public String getDescription(Context context) {
      return context.getResources().getString(R.string.PuertaSlotDoor);
    }
  },
  PuertaDropDoor(12008) {
    @Override
    public LayoutSiteItemGroupCategory getGroupCategory() {
      return LayoutSiteItemGroupCategory.Intrusion;
    }
    @Override
    public String getDescription(Context context) {
      return context.getResources().getString(R.string.PuertaDropDoor);
    }
  },
  PuertaCardCagedoor(12002) {
    @Override
    public LayoutSiteItemGroupCategory getGroupCategory() {
      return LayoutSiteItemGroupCategory.Intrusion;
    }
    @Override
    public String getDescription(Context context) {
      return context.getResources().getString(R.string.PuertaCardCagedoor);
    }
  },
  PuertaBellyDoor(12001) {
    @Override
    public LayoutSiteItemGroupCategory getGroupCategory() {
      return LayoutSiteItemGroupCategory.Intrusion;
    }

    @Override
    public String getDescription(Context context) {
      return context.getResources().getString(R.string.PuertaBellyDoor);
    }
  },
  PuertaCashboxDoor(12004) {
    @Override
    public LayoutSiteItemGroupCategory getGroupCategory() {
      return LayoutSiteItemGroupCategory.Intrusion;
    }
    @Override
    public String getDescription(Context context) {
      return context.getResources().getString(R.string.PuertaCashboxDoor);
    }
  },
  AceptadorBilletesAtascoBillete(13001) {
    @Override
    public LayoutSiteItemGroupCategory getGroupCategory() {
      return LayoutSiteItemGroupCategory.JamBill;
    }

    @Override
    public String getDescription(Context context) {
      return context.getResources().getString(R.string.AceptadorBilletesAtascoBillete);
    }
  },
  AceptadorBilletesError(13002){
    @Override
    public LayoutSiteItemGroupCategory getGroupCategory() {
      return LayoutSiteItemGroupCategory.JamBill;
    }

    @Override
    public String getDescription(Context context) {
      return context.getResources().getString(R.string.AceptadorBilletesError);
    }
  },
  AceptadorBilletesBilletesFalsos(13004){
    @Override
    public LayoutSiteItemGroupCategory getGroupCategory() {
      return LayoutSiteItemGroupCategory.JamBill;
    }
    @Override
    public String getDescription(Context context) {
      return context.getResources().getString(R.string.AceptadorBilletesBilletesFalsos);
    }
  },
  ImpresoraErrorComunicacion(14002){
    @Override
    public LayoutSiteItemGroupCategory getGroupCategory() {
      return LayoutSiteItemGroupCategory.Printer;
    }
    @Override
    public String getDescription(Context context) {
      return context.getResources().getString(R.string.ImpresoraErrorComunicacion);
    }
  },
  ImpresoraFalloSalida(14008){
    @Override
    public LayoutSiteItemGroupCategory getGroupCategory() {
      return LayoutSiteItemGroupCategory.Printer;
    }
    @Override
    public String getDescription(Context context) {
      return context.getResources().getString(R.string.ImpresoraFalloSalida);
    }
  },
  ImpresoraReemplazoCinta(14032){
    @Override
    public LayoutSiteItemGroupCategory getGroupCategory() {
      return LayoutSiteItemGroupCategory.Printer;
    }
    @Override
    public String getDescription(Context context) {
      return context.getResources().getString(R.string.ImpresoraReemplazoCinta);
    }

  },
  ImpresoraPapelBajo(14004){
    @Override
    public LayoutSiteItemGroupCategory getGroupCategory() {
      return LayoutSiteItemGroupCategory.Printer;
    }

    @Override
    public String getDescription(Context context) {
      return context.getResources().getString(R.string.ImpresoraPapelBajo);
    }
  },
  ImpresoraAtascoCarro(14001){
    @Override
    public LayoutSiteItemGroupCategory getGroupCategory() {
      return LayoutSiteItemGroupCategory.Printer;
    }

    @Override
    public String getDescription(Context context) {
      return context.getResources().getString(R.string.ImpresoraAtascoCarro);
    }
  },
  ImpresoraFalloDealimentacion(14016){
    @Override
    public LayoutSiteItemGroupCategory getGroupCategory() {
      return LayoutSiteItemGroupCategory.Printer;
    }
    @Override
    public String getDescription(Context context) {
      return context.getResources().getString(R.string.ImpresoraFalloDealimentacion);
    }
  },
  JugadoGanadoJugado(16001){
    @Override
    public LayoutSiteItemGroupCategory getGroupCategory() {
      return LayoutSiteItemGroupCategory.MachineFlags;
    }
    @Override
    public String getDescription(Context context) {
      return context.getResources().getString(R.string.JugadoGanadoJugado);
    }
  },
  JugadoGanadoGanado(16002){
    @Override
    public LayoutSiteItemGroupCategory getGroupCategory() {
      return LayoutSiteItemGroupCategory.MachineFlags;
    }
    @Override
    public String getDescription(Context context) {
      return context.getResources().getString(R.string.JugadoGanadoGanado);
    }
  },
  JugadoGanadoPayout(16004){
    @Override
    public LayoutSiteItemGroupCategory getGroupCategory() {
      return LayoutSiteItemGroupCategory.MachineFlags;
    }
    @Override
    public String getDescription(Context context) {
      return context.getResources().getString(R.string.JugadoGanadoPayout);
    }
  },
  JugadoGanadoSinJugadas(16008){
    @Override
    public LayoutSiteItemGroupCategory getGroupCategory() {
      return LayoutSiteItemGroupCategory.MachineFlags;
    }
    @Override
    public String getDescription(Context context) {
      return context.getResources().getString(R.string.JugadoGanadoSinJugadas);
    }
  },
  MaquinaErrorCMOSRAM(20001){
    @Override
    public LayoutSiteItemGroupCategory getGroupCategory() {
      return LayoutSiteItemGroupCategory.MachineFlags;
    }

    @Override
    public String getDescription(Context context) {
      return context.getResources().getString(R.string.MaquinaErrorCMOSRAM);
    }
  },
  MaquinaErrorEEPROM(20002){
    @Override
    public LayoutSiteItemGroupCategory getGroupCategory() {
      return LayoutSiteItemGroupCategory.MachineFlags;
    }
    @Override
    public String getDescription(Context context) {
      return context.getResources().getString(R.string.MaquinaErrorEEPROM);
    }
  },
  MaquinaBateriaBaja(20512){
    @Override
    public LayoutSiteItemGroupCategory getGroupCategory() {
      return LayoutSiteItemGroupCategory.MachineFlags;
    }

    @Override
    public String getDescription(Context context) {
      return context.getResources().getString(R.string.MaquinaBateriaBaja);
    }

  },
  Custom(99000){
    @Override
    public LayoutSiteItemGroupCategory getGroupCategory() {
      return LayoutSiteItemGroupCategory.Custom;
    }

    @Override
    public String getDescription(Context context) {
      return context.getResources().getString(R.string.other_category_title);
    }

  },
  CustomMachine(97000){
    @Override
    public LayoutSiteItemGroupCategory getGroupCategory() {
      return LayoutSiteItemGroupCategory.MachineFlags;
    }

    @Override
    public String getDescription(Context context) {
      return context.getResources().getString(R.string.other_category_title);
    }

  },
  CustomPlayer(98000){
    @Override
    public LayoutSiteItemGroupCategory getGroupCategory() {
      return LayoutSiteItemGroupCategory.Support;
    }

    @Override
    public String getDescription(Context context) {
      return context.getResources().getString(R.string.other_category_title);
    }

  }, Selection(2){
    @Override
    public LayoutSiteItemGroupCategory getGroupCategory() {
      return LayoutSiteItemGroupCategory.Other;
    }

    @Override
    public String getDescription(Context context) {
      return context.getResources().getString(R.string.selection_title);
    }

  };

  private int value;
  LayoutSiteItemSubCategory(int i) {
    value = i;
  }

  public abstract LayoutSiteItemGroupCategory getGroupCategory();
  public abstract String getDescription(Context context);
  public LayoutSiteItemCategory getCategory(){
    switch (this){
      case JackpotCreditosCancelados:
      case JackpotJackpot:
      case SoporteCallAttendant:
      case HighRoller:
        return LayoutSiteItemCategory.PLAYER;
      case Custom:
        return LayoutSiteItemCategory.CUSTOM;
      default:
        return LayoutSiteItemCategory.MACHINE;
    }
  }

  public static LayoutSiteItemSubCategory forCode(int code) {
    for (LayoutSiteItemSubCategory type : LayoutSiteItemSubCategory.values()) {
      if (type.getValue() == code) {
        return type;
      }
    }
    return LayoutSiteItemSubCategory.AceptadorBilletesAtascoBillete;
  }

  public static boolean isCustomCategory(int selectedValue) {
    return selectedValue == LayoutSiteItemSubCategory.CustomMachine.getValue() || selectedValue == LayoutSiteItemSubCategory.CustomPlayer.getValue()|| selectedValue == LayoutSiteItemSubCategory.Custom.getValue();
  }


  public int getValue() {
    return value;
  }
}
