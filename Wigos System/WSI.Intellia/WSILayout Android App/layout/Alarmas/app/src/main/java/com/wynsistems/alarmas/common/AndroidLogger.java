package com.wynsistems.alarmas.common;

import com.wynsistems.alarmas.helpers.SaveInformationHelper;
import com.wynsistems.alarmas.view.utils.Constants;

import java.util.Date;
import java.util.List;

public class AndroidLogger{
    private SaveInformationHelper<String> _logFile;

    public AndroidLogger(SaveInformationHelper<String> logFile)
    {
        _logFile = logFile;
    }

    public void error(String message, Throwable e)
    {
        List<String> logs = _logFile.readList(Constants.LOG);
        String eMessage = e != null? e.getMessage() : "";
        String finalMessageException = eMessage.isEmpty()? "NO MESSAGE TO SHOW" : eMessage;
        logs.add(new Date() + " - ERROR - "+ message + " - Exception: " + finalMessageException);
        _logFile.writeList(logs, Constants.LOG);
    }

    public void info (String message)
    {
        List<String> logs = _logFile.readList(Constants.LOG);
        logs.add(new Date() + " - INFO - "+ message);
        _logFile.writeList(logs, Constants.LOG);
    }
}
