package com.winsystems.smartfloor.model;

public enum LayoutSiteManagerRunnerMessageSource {

  Manager(1),
  Runner(2);

  private int value;

  LayoutSiteManagerRunnerMessageSource(int i) {
    value = i;
  }

  public int getValue() {
    return value;
  }
}
