package com.wynsistems.alarmas.view.task;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wynsistems.alarmas.R;
import com.wynsistems.alarmas.model.AlarmGroupingCriteria;
import com.wynsistems.alarmas.model.SummarizedAlarms;
import com.wynsistems.alarmas.helpers.AlarmResourseHelper;

import java.util.List;

public class SummarizedTaskListAdapter extends BaseAdapter{
    private List<SummarizedAlarms> summarizedAlarmsList;
    private AlarmGroupingCriteria groupPreference;
    private static LayoutInflater inflater = null;

    public SummarizedTaskListAdapter(Context context, List<SummarizedAlarms> data, AlarmGroupingCriteria groupPreference) {
        this.summarizedAlarmsList = data;
        this.groupPreference = groupPreference;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return summarizedAlarmsList.size();
    }

    @Override
    public Object getItem(int position) {
        return summarizedAlarmsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        SummarizedAlarms summarizedAlarms = summarizedAlarmsList.get(position);

        if (vi == null)
        {
            FrameLayout imageView;
            GradientDrawable bgShape;
            switch (groupPreference) {
                case CRITICALITY:
                    vi = inflater.inflate(R.layout.task_item_detail_criticality, null);
                    imageView = (FrameLayout) vi.findViewById(R.id.list_item_icon);
                    bgShape = (GradientDrawable)imageView.getBackground();
                    bgShape.setColor(AlarmResourseHelper.getColorByCriticality(summarizedAlarms.getAlarmCriticality(), vi.getResources()));
                    break;
                case CATEGORY:
                    vi = inflater.inflate(R.layout.task_item_detail_category,null);

                    imageView = (FrameLayout) vi.findViewById(R.id.list_item_icon);
                    bgShape = (GradientDrawable)imageView.getBackground();
                    bgShape.setColor(vi.getResources().getColor(R.color.background_action_bar));

                    ImageView alarmCategoryType = (ImageView) vi.findViewById(R.id.alarm_category_image);
                    alarmCategoryType.setImageResource(AlarmResourseHelper.getCategoryImageByType(summarizedAlarms.getAlarmCategoryType()));
                    break;
                case COMPLETED:
                    vi = inflater.inflate(R.layout.task_item_detail_completed, null);
                    LinearLayout holder = (LinearLayout) vi.findViewById(R.id.status_holder);
                    if(summarizedAlarms.getInProgress())
                        holder.setBackgroundResource(R.drawable.rectangle_blue);
                    else
                        holder.setBackgroundResource(R.drawable.rectangle_red);
                    break;
            }
        }

        TextView text = (TextView) vi.findViewById(R.id.list_item_description);
        text.setText(summarizedAlarms.getDescription());

        TextView textTotal = (TextView) vi.findViewById(R.id.list_item_total);
        textTotal.setText(Integer.toString(summarizedAlarms.getTotal()));

        return vi;
    }
}
