package com.wynsistems.alarmas.model;


import android.content.Context;

import com.wynsistems.alarmas.R;

public enum LayoutSiteItemGroupCategory {
    PayCheck,
    Intrusion,
    JamBill,
    MachineFlags,
    Support,
    Printer,
    Other;


    public static String getTypeDescription(LayoutSiteItemGroupCategory alarmCategoryType, Context context)
    {
        switch (alarmCategoryType) {
            case PayCheck:
                return context.getResources().getString(R.string.paycheck);
            case Intrusion:
                return context.getResources().getString(R.string.intrusion);
            case JamBill:
                return context.getResources().getString(R.string.jambill);
            case MachineFlags:
                return context.getResources().getString(R.string.machine_flags);
            case Support:
                return context.getResources().getString(R.string.support);
            case Printer:
                return context.getResources().getString(R.string.printer);
            case Other:
                return context.getResources().getString(R.string.other);
            default:
                return context.getResources().getString(R.string.printer);
        }
    }


    public static LayoutSiteItemGroupCategory getEnumByDescription(String description, Context context){
        if(context.getResources().getString(R.string.paycheck).equals(description)) return PayCheck;
        if(context.getResources().getString(R.string.intrusion).equals(description)) return Intrusion;
        if(context.getResources().getString(R.string.jambill).equals(description)) return JamBill;
        if(context.getResources().getString(R.string.machine_flags).equals(description)) return MachineFlags;
        if(context.getResources().getString(R.string.support).equals(description)) return Support;
        if(context.getResources().getString(R.string.printer).equals(description)) return Printer;
        return Printer;
    }
}
