package com.wynsistems.alarmas.model;

import com.wynsistems.alarmas.service.login.response.LayoutSiteTaskTerminal;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.io.Serializable;
import java.util.Hashtable;

public class LayoutSiteAlarm implements KvmSerializable, Serializable {

    private int id;
    private int subCategory;
    private int category;
    private int terminalId;
    private LayoutSiteTaskTerminal terminal;

    public int getStatusOfTask() {
        return statusOfTask;
    }

    public void setStatusOfTask(int statusOfTask) {
        this.statusOfTask = statusOfTask;
    }

    private int statusOfTask;

    public void setId(int id) {
        this.id = id;
    }

    public void setSubCategory(int subCategory) {
        this.subCategory = subCategory;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public void setTerminalId(int terminalId) {
        this.terminalId = terminalId;
    }

    public void setTerminal(LayoutSiteTaskTerminal terminal) {
        this.terminal = terminal;
    }

    public LayoutSiteTaskTerminal getTerminal() {
        return terminal;
    }

    public LayoutSiteItemSubCategory getSubCategory() {
        return LayoutSiteItemSubCategory.forCode(subCategory);
    }

    public int getCategory() {
        return category;
    }

    @Override
    public Object getProperty(int i) {
        switch (i){
            case 0:
                return category;
            case 1:
                return id;
            case 2:
                return statusOfTask;
            case 3:
                return subCategory;
            case 4:
                return terminal;
            case 5:
                return terminalId;
            default:
                return null;
        }
    }

    @Override
    public int getPropertyCount() {
        return 7;
    }

    @Override
    public void setProperty(int i, Object o) {
        switch (i){
            case 0:
                category = Integer.parseInt(o.toString());
                break;
            case 1:
                id = Integer.parseInt(o.toString());
                break;
            case 2:
                statusOfTask = Integer.parseInt(o.toString());
                break;
            case 3:
                subCategory = Integer.parseInt(o.toString());
                break;
            case 4:
                terminal = (LayoutSiteTaskTerminal)o;
                break;
            case 5:
                terminalId = Integer.parseInt(o.toString());
                break;

        }
    }

    @Override
    public void getPropertyInfo(int i, Hashtable hashtable, PropertyInfo propertyInfo) {
        switch (i){
            case 0:
                propertyInfo.type = PropertyInfo.INTEGER_CLASS;
                propertyInfo.name = "Category";
                break;
            case 1:
                propertyInfo.type = PropertyInfo.INTEGER_CLASS;
                propertyInfo.name = "Id";
                break;
            case 2:
                propertyInfo.type = PropertyInfo.INTEGER_CLASS;
                propertyInfo.name = "StatusOfTask";
                break;
            case 3:
                propertyInfo.type = PropertyInfo.INTEGER_CLASS;
                propertyInfo.name = "SubCategory";
                break;
            case 4:
                propertyInfo.type = PropertyInfo.OBJECT_CLASS;
                propertyInfo.name = "Terminal";
                break;
            case 5:
                propertyInfo.type = PropertyInfo.INTEGER_CLASS;
                propertyInfo.name = "TerminalId";
                break;

        }
    }
}
