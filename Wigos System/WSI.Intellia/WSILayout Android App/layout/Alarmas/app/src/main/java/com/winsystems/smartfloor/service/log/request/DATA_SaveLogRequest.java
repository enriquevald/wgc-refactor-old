package com.winsystems.smartfloor.service.log.request;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.util.Date;
import java.util.Hashtable;

public class DATA_SaveLogRequest implements KvmSerializable {
    private Date datetime;
    private String source;
    private String deviceId;
    private String userId;
    private String content;
    private String sessionId;

    public String getSessionId() {
        return sessionId;
    }

    @Override
    public Object getProperty(int i) {
        switch (i){
            case 0:
                return content;
            case 1:
                return datetime;
            case 2:
                return deviceId;
            case 3:
                return sessionId;
            case 4:
                return source;
            case 5:
                return userId;
            default:
                return null;
        }
    }

    @Override
    public int getPropertyCount() {
        return 6;
    }

    @Override
    public void setProperty(int i, Object o) {
        switch (i){
            case 0:
                content= o.toString();
                break;
            case 1:
                datetime = (Date)(o);
                break;
            case 2:
                deviceId = o.toString();
                break;
            case 3:
                sessionId = o.toString();
                break;
            case 4:
                source = o.toString();
                break;
            case 5:
                userId = o.toString();
                break;
        }
    }

    @Override
    public void getPropertyInfo(int i, Hashtable hashtable, PropertyInfo propertyInfo) {
          switch (i){
              case 0:
                  propertyInfo.type = PropertyInfo.STRING_CLASS;
                  propertyInfo.name = "Content";
                  break;
              case 1:
                  propertyInfo.type = PropertyInfo.STRING_CLASS;
                  propertyInfo.name = "DateTime";
                  break;
              case 2:
                  propertyInfo.type = PropertyInfo.STRING_CLASS;
                  propertyInfo.name = "DeviceId";
                  break;
              case 3:
                  propertyInfo.type = PropertyInfo.STRING_CLASS;
                  propertyInfo.name = "SessionId";
                  break;
              case 4:
                  propertyInfo.type = PropertyInfo.STRING_CLASS;
                  propertyInfo.name = "Source";
                  break;
              case 5:
                  propertyInfo.type = PropertyInfo.STRING_CLASS;
                  propertyInfo.name = "UserId";
                  break;
        }
    }

    public void setDatetime(Object date) { this.datetime = (Date)(date); }

    public void setSource(String source) { this.source = source; }

    public void setDeviceId(String deviceid) {
        this.deviceId = deviceid;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }
}
