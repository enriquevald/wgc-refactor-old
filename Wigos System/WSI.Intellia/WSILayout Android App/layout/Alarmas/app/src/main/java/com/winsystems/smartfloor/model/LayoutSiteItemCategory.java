package com.winsystems.smartfloor.model;


import java.util.ArrayList;
import java.util.List;

public enum LayoutSiteItemCategory {
  PLAYER(41),
  MACHINE(42),
  CUSTOM(43);

  private int value;

  LayoutSiteItemCategory(int i) {
    value = i;
  }

  public int getValue(){return value;}

  public static List<LayoutSiteItemSubCategory> GetSubCategories(LayoutSiteItemCategory category){
    ArrayList<LayoutSiteItemSubCategory> categoryList = new ArrayList<>();
    switch (category) {
      case PLAYER:
        categoryList.add(LayoutSiteItemSubCategory.JackpotCreditosCancelados);
        categoryList.add(LayoutSiteItemSubCategory.JackpotJackpot);
        categoryList.add(LayoutSiteItemSubCategory.SoporteCallAttendant);
        categoryList.add(LayoutSiteItemSubCategory.HighRoller);
        categoryList.add(LayoutSiteItemSubCategory.HighRoller_Anonymous);
        categoryList.add(LayoutSiteItemSubCategory.CustomPlayer);
        return categoryList;
      case MACHINE:
        categoryList.add(LayoutSiteItemSubCategory.StackerAlmostFull);
        categoryList.add(LayoutSiteItemSubCategory.StackerFull);
        categoryList.add(LayoutSiteItemSubCategory.AceptadorBilletesAtascoBillete);
        categoryList.add(LayoutSiteItemSubCategory.AceptadorBilletesBilletesFalsos);
        categoryList.add(LayoutSiteItemSubCategory.AceptadorBilletesError);
        categoryList.add(LayoutSiteItemSubCategory.ImpresoraAtascoCarro);
        categoryList.add(LayoutSiteItemSubCategory.ImpresoraErrorComunicacion);
        categoryList.add(LayoutSiteItemSubCategory.ImpresoraFalloDealimentacion);
        categoryList.add(LayoutSiteItemSubCategory.ImpresoraFalloSalida);
        categoryList.add(LayoutSiteItemSubCategory.ImpresoraPapelBajo);
        categoryList.add(LayoutSiteItemSubCategory.ImpresoraReemplazoCinta);
        categoryList.add(LayoutSiteItemSubCategory.JugadoGanadoGanado);
        categoryList.add(LayoutSiteItemSubCategory.JugadoGanadoJugado);
        categoryList.add(LayoutSiteItemSubCategory.JugadoGanadoPayout);
        categoryList.add(LayoutSiteItemSubCategory.JugadoGanadoSinJugadas);
        categoryList.add(LayoutSiteItemSubCategory.MaquinaBateriaBaja);
        categoryList.add(LayoutSiteItemSubCategory.MaquinaErrorCMOSRAM);
        categoryList.add(LayoutSiteItemSubCategory.MaquinaErrorEEPROM);
        categoryList.add(LayoutSiteItemSubCategory.PuertaBellyDoor);
        categoryList.add(LayoutSiteItemSubCategory.PuertaCardCagedoor);
        categoryList.add(LayoutSiteItemSubCategory.PuertaCashboxDoor);
        categoryList.add(LayoutSiteItemSubCategory.PuertaDropDoor);
        categoryList.add(LayoutSiteItemSubCategory.PuertaSlotDoor);
        categoryList.add(LayoutSiteItemSubCategory.CustomMachine);
        return categoryList;
      case CUSTOM:
        categoryList.add(LayoutSiteItemSubCategory.Custom);
        return categoryList;
      default:
        return categoryList;
    }
  }
}
