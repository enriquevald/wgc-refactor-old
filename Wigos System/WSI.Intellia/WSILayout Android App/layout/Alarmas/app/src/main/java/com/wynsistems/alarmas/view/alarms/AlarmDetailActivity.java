package com.wynsistems.alarmas.view.alarms;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wynsistems.alarmas.R;
import com.wynsistems.alarmas.model.Alarm;
import com.wynsistems.alarmas.view.task.TerminalDetailsActivity;
import com.wynsistems.alarmas.helpers.AlarmResourseHelper;
import com.wynsistems.alarmas.view.utils.Constants;
import com.wynsistems.alarmas.view.utils.KioskModeActivity;

import java.text.Format;
import java.text.SimpleDateFormat;

public class AlarmDetailActivity extends KioskModeActivity {
    private Alarm currentAlarm;
    private Format formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm_detail);
        SharedPreferences settings = this.getSharedPreferences(Constants.USER_ID_PREFERENCE_EDITOR, 0) ;

        this.currentAlarm = (Alarm) getIntent().getSerializableExtra(Constants.SELECTED_ALARM);

        fillLayoutUsingCurrentAlarm(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_alarm_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void fillLayoutUsingCurrentAlarm(ActionBarActivity activity) {
        final LinearLayout content = (LinearLayout) activity.findViewById(R.id.content_layout);
        TextView description = (TextView) activity.findViewById(R.id.alarm_detail_description);
        TextView terminalId = (TextView) activity.findViewById(R.id.terminal_id);
        TextView categoryDescription = (TextView) activity.findViewById(R.id.category_description);
        TextView alarmId = (TextView) activity.findViewById(R.id.alarm_id);
        TextView alarmDate = (TextView) activity.findViewById(R.id.alarm_date);
        LinearLayout location = (LinearLayout) activity.findViewById(R.id.terminal_details);

        if(currentAlarm.getDescription() == "")
            description.setVisibility(View.GONE);
        else
            description.setText(currentAlarm.getDescription());
        terminalId.setText(currentAlarm.getTerminal().getId());
        categoryDescription.setText(currentAlarm.getSiteItemSubCategory().getDescription(getApplicationContext()));
        alarmId.setText(currentAlarm.getId());
        alarmDate.setText(formatter.format(currentAlarm.getDate()));

        FrameLayout imageView = (FrameLayout) activity.findViewById(R.id.alarm_criticality_type);
        GradientDrawable bgShape = (GradientDrawable) imageView.getBackground();
        bgShape.setColor(AlarmResourseHelper.getColorByCriticality(currentAlarm.getAlarmCriticality(), getResources()));

        ImageView alarmCategoryType = (ImageView) activity.findViewById(R.id.alarm_category_image);
        alarmCategoryType.setImageResource(AlarmResourseHelper.getCategoryImageByType(currentAlarm.getAlarmCategory()));

        ImageView alarmCompleted = (ImageView) activity.findViewById(R.id.alarm_completed);
        alarmCompleted.setImageResource(AlarmResourseHelper.getCompletedImageByState(currentAlarm.getCompleted()));

        location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AlarmDetailActivity.this, TerminalDetailsActivity.class);
                intent.putExtra(Constants.TERMINAL_DETAILS, currentAlarm.getTerminal());
                startActivity(intent);
            }
        });


        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(AlarmResourseHelper.getCriticalityDescriptionByCriticality(currentAlarm.getAlarmCriticality(), activity));
    }


    @Override
    protected void onStop() {
        if(this.dialog != null)
            this.dialog.dismiss();

        super.onStop();
    }

    private void buildDialog() {

        this.dialog = new ProgressDialog(AlarmDetailActivity.this);
        this.dialog.setTitle(AlarmDetailActivity.this.getResources().getString(R.string.title_progress_dialog));
        this.dialog.setMessage(AlarmDetailActivity.this.getResources().getString(R.string.alarm_message_progress_dialog));

    }



}
