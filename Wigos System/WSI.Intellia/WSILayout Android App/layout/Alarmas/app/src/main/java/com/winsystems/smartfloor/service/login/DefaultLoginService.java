package com.winsystems.smartfloor.service.login;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.winsystems.smartfloor.R;
import com.winsystems.smartfloor.common.AndroidLogger;
import com.winsystems.smartfloor.common.TrustManagerManipulator;
import com.winsystems.smartfloor.helpers.LogHelper;
import com.winsystems.smartfloor.helpers.SaveInformationHelper;
import com.winsystems.smartfloor.model.LayoutSiteAlarm;
import com.winsystems.smartfloor.repository.RepositoryFactory;
import com.winsystems.smartfloor.repository.TaskRepository;
import com.winsystems.smartfloor.service.DATA_PostActionResponse;
import com.winsystems.smartfloor.service.ResponseCode;
import com.winsystems.smartfloor.common.ServerEntitiesBuilder;
import com.winsystems.smartfloor.service.login.request.DATA_KeepALive;
import com.winsystems.smartfloor.service.login.request.DATA_LogIn;
import com.winsystems.smartfloor.service.login.response.BeaconAvailable;
import com.winsystems.smartfloor.service.login.response.DATA_LogInResponse;
import com.winsystems.smartfloor.view.utils.Constants;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public class DefaultLoginService implements LoginService{

  private static final String TAG = "LoginService";
  private static DefaultLoginService instance = null;
  private static TaskRepository taskRepository;
  private SaveInformationHelper<LayoutSiteAlarm> saveLayoutSiteAlarms;
  private SaveInformationHelper<Long> saveRoles;
  private SaveInformationHelper<BeaconAvailable> saveBeacons;
  private AndroidLogger mLog;
  public DefaultLoginService (Context context){
    taskRepository = RepositoryFactory.getInstance().getAlarmRepository(context);
    mLog = LogHelper.getLogger(context);
    mLog.trim();
  }

  public static DefaultLoginService getInstance(Context context){
    if(instance == null){
      instance = new DefaultLoginService(context);
    }


    return instance;
  }

  @Override
  public DATA_LogInResponse loginUser(Context appContext, String username, String password, String url, String deviceId) {

    final String NAMESPACE = "http://winsystemsintl.com/";
    final String METHOD_NAME = "LogIn";
    final String SOAP_ACTION = "http://winsystemsintl.com/IDATAService/LogIn";
    saveRoles = new SaveInformationHelper<>(appContext);
    saveLayoutSiteAlarms = new SaveInformationHelper<>(appContext);
    saveBeacons = new SaveInformationHelper<>(appContext);

    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);

    DATA_LogIn loginRequest = new DATA_LogIn();
    loginRequest.setIdOperador("0");
    loginRequest.setPassword(password);
    loginRequest.setLogin(username);
    loginRequest.setDeviceId(deviceId);

    DATA_LogInResponse logInResponse = new DATA_LogInResponse();

    PropertyInfo pi = new PropertyInfo();
    pi.setName("loginInfo");
    pi.setValue(loginRequest);
    pi.setType(loginRequest.getClass());

    request.addProperty(pi);

    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
    envelope.dotNet = true;

    envelope.setOutputSoapObject(request);

    envelope.addMapping(NAMESPACE, "DATA_LogIn", loginRequest.getClass());
    envelope.addMapping(NAMESPACE, "DATA_LogInResponse", logInResponse.getClass());

    HttpTransportSE transporte = new HttpTransportSE(url, 60000);

    try
    {
      TrustManagerManipulator.allowAllSSL();
      transporte.call(SOAP_ACTION, envelope);

      SoapObject resSoap =(SoapObject)envelope.getResponse();

      logInResponse.setResponseCode(Integer.parseInt(resSoap.getProperty(0).toString()));
      logInResponse.setSessionId(resSoap.getProperty(2).toString());

      if(logInResponse.getResponseCode() == ResponseCode.DATA_RC_OK) {
        logInResponse.setResponseCodeDescription(resSoap.getProperty(1).toString());

        SoapObject tasks = (SoapObject) resSoap.getProperty(3);
        logInResponse.setAvailableTasks(ServerEntitiesBuilder.BuildTasksListFromServerResponse(tasks));

        SoapObject roles = (SoapObject) resSoap.getProperty(6);
        int cantRoles = roles.getPropertyCount();
        List<Long> roleList = new ArrayList<>();
        for (int i = 0; i < cantRoles; i++) {
          roleList.add(Long.parseLong(roles.getProperty(i).toString()));
        }

        logInResponse.setRoles(roleList);

        SoapObject alarms = (SoapObject) resSoap.getProperty(7);
        logInResponse.setTerminalAlarmsCreatedInTheDay(ServerEntitiesBuilder.BuildAlarmsListFromServerResponse(alarms));

        SoapObject beacons = (SoapObject) resSoap.getProperty(4);
        logInResponse.setBeaconsAvailables(ServerEntitiesBuilder.BuildBeaconsListFromServerResponse(beacons));

        logInResponse.setUserId(Long.parseLong(resSoap.getProperty(8).toString()));

        logInResponse.setLastVersionApp(resSoap.getProperty(5).toString().trim());
      }

    }catch (ConnectException e)
    {
      mLog.error("", e);
      logInResponse.setResponseCode(9);
      logInResponse.setResponseCodeDescription(appContext.getString(R.string.error_connection));
    }
    catch (SocketTimeoutException e)
    {
      mLog.error("", e);
      logInResponse.setResponseCode(9);
      logInResponse.setResponseCodeDescription(appContext.getString(R.string.error_timeout));

      return logInResponse;
    }
    catch (Exception e)
    {
      mLog.error("", e);
      logInResponse.setResponseCode(9);
      logInResponse.setResponseCodeDescription(appContext.getString(R.string.error_connection));

      return logInResponse;
    }

    if (logInResponse.getResponseCode() == ResponseCode.DATA_RC_OK)
    {
      taskRepository.saveFromAlarmResponse(logInResponse.getAvailableTasks(), appContext);
      saveLayoutSiteAlarms.writeList(logInResponse.getTerminalAlarmsCreatedInTheDay(), logInResponse.getSessionId() + Constants.ALARMS_CREATED_BY_USER);
      saveRoles.writeList(logInResponse.getRoles(), logInResponse.getSessionId() + Constants.ROLES_OF_USER);
      saveBeacons.writeList(logInResponse.getBeaconsAvailables(), logInResponse.getSessionId() + Constants.BEACONS_AVAILABLES);
      SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(appContext);
      SharedPreferences.Editor editor = preferences.edit();
      editor.putString("userId", logInResponse.getUserId().toString());
      editor.putString("lastVersionApp", logInResponse.getLastVersionApp());
      SharedPreferences settings = appContext.getSharedPreferences(Constants.USER_ID_PREFERENCE_EDITOR, 0) ;
      SharedPreferences.Editor edit = settings.edit();
      edit.putString(Constants.USER_ID, logInResponse.getSessionId());
      edit.apply();
      editor.apply();
    }

    return logInResponse;
  }

  @Override
  public DATA_PostActionResponse keepALive(Context appContext, String session,String url) {
    final String NAMESPACE = "http://winsystemsintl.com/";
    final String METHOD_NAME = "KeepALive";
    final String SOAP_ACTION = "http://winsystemsintl.com/IDATAService/KeepALive";

    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
    DATA_KeepALive keepAlive = new DATA_KeepALive();
    keepAlive.setSessionId(session);

    DATA_PostActionResponse response = new DATA_PostActionResponse();

    PropertyInfo pi = new PropertyInfo();
    pi.setName("keepALive");
    pi.setValue(keepAlive);
    pi.setType(keepAlive.getClass());

    request.addProperty(pi);

    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
    envelope.dotNet = true;

    envelope.setOutputSoapObject(request);

    envelope.addMapping(NAMESPACE, "DATA_KeepALive", keepAlive.getClass());
    envelope.addMapping(NAMESPACE, "DATA_PostActionResponse", response.getClass());

    HttpTransportSE transporte = new HttpTransportSE(url, 20000);

    try {
      TrustManagerManipulator.allowAllSSL();
      transporte.call(SOAP_ACTION, envelope);

      SoapObject resSoap = (SoapObject) envelope.getResponse();

      response.setResponseCode(Integer.parseInt(resSoap.getProperty(0).toString()));
      if(response.getResponseCode() == ResponseCode.DATA_RC_OK){
        response.setResponseCodeDescription(resSoap.getProperty(1).toString());
        response.setSessionId(resSoap.getProperty(2).toString());
      }
    }
    catch (ConnectException e)
    {
      mLog.error("", e);
      response.setResponseCode(9);
      response.setResponseCodeDescription(appContext.getString(R.string.error_connection));
    }
    catch (SocketTimeoutException e)
    {
      mLog.error("", e);
      response.setResponseCode(9);
      response.setResponseCodeDescription(appContext.getString(R.string.error_timeout));

      return response;
    }
    catch (Exception e)
    {
      mLog.error("", e);
      response.setResponseCode(9);
      response.setResponseCodeDescription(appContext.getString(R.string.error_unknown));

      return response;
    }

    return response;
  }
}
