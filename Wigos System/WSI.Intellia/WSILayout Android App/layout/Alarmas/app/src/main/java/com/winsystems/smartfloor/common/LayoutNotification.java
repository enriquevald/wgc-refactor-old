package com.winsystems.smartfloor.common;

import java.io.Serializable;
import java.util.List;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: LayoutNotification
//
// DESCRIPTION: Layout Notification
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public class LayoutNotification implements Serializable{
  private String title;
  private String content;
  private LayoutNotificationType type;
  private Integer id;
  private Integer icon;
  private List<IntentExtra> extras;

  public void setNotificationType(LayoutNotificationType type) {
    this.type = type;
  }

  public void setExtras(List<IntentExtra> extras) {
    this.extras = extras;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public void setId(Integer id){ this.id = id; }

  public void setIcon(int icon) {
    this.icon = icon;
  }

  public List<IntentExtra> getIntent() {
    return extras;
  }

  public LayoutNotificationType getType() {
    return type;
  }

  public String getTitle() {
    return title;
  }

  public String getContent() {
    return content;
  }

  public Integer getIcon() {
    return icon;
  }

  public Integer getId() {
    return id;
  }
}
