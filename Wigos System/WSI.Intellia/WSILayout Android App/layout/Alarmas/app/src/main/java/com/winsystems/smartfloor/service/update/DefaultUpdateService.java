package com.winsystems.smartfloor.service.update;

import android.content.Context;
import android.util.Log;

import com.winsystems.smartfloor.R;
import com.winsystems.smartfloor.common.AndroidLogger;
import com.winsystems.smartfloor.common.TrustManagerManipulator;
import com.winsystems.smartfloor.helpers.LogHelper;
import com.winsystems.smartfloor.service.ResponseCode;
import com.winsystems.smartfloor.service.update.request.DATA_UpdateApplicationRequest;
import com.winsystems.smartfloor.service.update.response.DATA_UpdateApplicationResponse;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.net.ConnectException;
import java.net.SocketTimeoutException;

public class DefaultUpdateService implements UpdateService {

  private static DefaultUpdateService instance;
  private static AndroidLogger mLog;
  private static Context appContext;

  @Override
  public DATA_UpdateApplicationResponse getLastestApplicationVersion(String url, String sessionId) {

    final String NAMESPACE = "http://winsystemsintl.com/";
    final String METHOD_NAME = "UpdateApplication";
    final String SOAP_ACTION = "http://winsystemsintl.com/IDATAService/UpdateApplication";

    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
    DATA_UpdateApplicationRequest dataUpdateApplicationRequest = new DATA_UpdateApplicationRequest();
    dataUpdateApplicationRequest.setSessionId(sessionId);

    DATA_UpdateApplicationResponse response = new DATA_UpdateApplicationResponse();
    try {
      PropertyInfo pi = new PropertyInfo();
      pi.setName("update");
      pi.setValue(dataUpdateApplicationRequest);
      pi.setType(dataUpdateApplicationRequest.getClass());

      request.addProperty(pi);

      SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
      envelope.dotNet = true;

      envelope.setOutputSoapObject(request);

      envelope.addMapping(NAMESPACE, "DATA_UpdateApplicationRequest", dataUpdateApplicationRequest.getClass());
      envelope.addMapping(NAMESPACE, "DATA_UpdateApplicationResponse", response.getClass());

      HttpTransportSE transporte = new HttpTransportSE(url, 120000);


      TrustManagerManipulator.allowAllSSL();
      transporte.call(SOAP_ACTION, envelope);

      SoapObject resSoap = (SoapObject) envelope.getResponse();

      response.setResponseCode(Integer.parseInt(resSoap.getProperty(0).toString()));
      if(response.getResponseCode() == ResponseCode.DATA_RC_OK){
        response.setResponseCodeDescription(resSoap.getProperty(1).toString());
        response.setSessionId(resSoap.getProperty(2).toString());
        response.setVersion(resSoap.getProperty(3).toString());
      }
    }catch (ConnectException e){
      mLog.error("", e);
      response.setResponseCode(9);
      response.setResponseCodeDescription(appContext.getString(R.string.error_connection));
    }
    catch (SocketTimeoutException e)
    {
      mLog.error("", e);
      response.setResponseCode(9);
      response.setResponseCodeDescription(appContext.getString(R.string.error_timeout));

      return response;
    }
    catch (Exception e)
    {
      mLog.error("", e);
      response.setResponseCode(9);
      response.setResponseCodeDescription(appContext.getString(R.string.error_unknown));

      return response;
    }

    return response;
  }

  public static DefaultUpdateService getInstance(Context context){
    mLog = LogHelper.getLogger(context);
    if(instance == null){
      instance = new DefaultUpdateService();
      appContext = context;
    }
    return instance;
  }
}
