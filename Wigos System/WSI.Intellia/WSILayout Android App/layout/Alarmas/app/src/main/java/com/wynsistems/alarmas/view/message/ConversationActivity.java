package com.wynsistems.alarmas.view.message;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.wynsistems.alarmas.R;
import com.wynsistems.alarmas.common.LayoutNotificationManager;
import com.wynsistems.alarmas.helpers.LayoutPendingMessage;
import com.wynsistems.alarmas.helpers.PendingToReadMessagesHelper;
import com.wynsistems.alarmas.model.LayoutSiteManagerRunnerMessage;
import com.wynsistems.alarmas.service.ResponseCode;
import com.wynsistems.alarmas.service.ServiceFactory;
import com.wynsistems.alarmas.service.message.response.DATA_GetConversationHistoryResponse;
import com.wynsistems.alarmas.service.message.response.DATA_SendMessageResponse;
import com.wynsistems.alarmas.service.mqtt.PahoMqttService;
import com.wynsistems.alarmas.service.notification.NotificationHistoryManager;
import com.wynsistems.alarmas.view.utils.Constants;
import com.wynsistems.alarmas.view.utils.KioskModeActivity;
import com.wynsistems.alarmas.view.utils.ToastFactory;

import java.util.ArrayList;
import java.util.List;

public class ConversationActivity extends KioskModeActivity {

    private String url;
    private String userId;
    private String userName;
    private Long managerId;
    private String managerUserName;
    private List<LayoutSiteManagerRunnerMessage> messages;
    private MessageListAdapter messageListAdapter;
    ListView messageListView;
    private IntentFilter intentFilter = new IntentFilter(PahoMqttService.NEW_MESSAGE_FROM_MANAGER_ARRIVE);
    private BroadcastReceiver messageBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            ConversationActivity.this.refreshView();
        }
    };

    private void refreshView() {
        new HttpFillConversationTask().execute();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_with_manager);
        SharedPreferences settings = this.getSharedPreferences(Constants.USER_ID_PREFERENCE_EDITOR, 0) ;
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        this.url = prefs.getString("alarm_server_address", "");
        this.userId = settings.getString(Constants.USER_ID, "");
        this.userName = prefs.getString("userName", "");
        this.managerId = Long.parseLong(getIntent().getStringExtra(Constants.MANAGER_ID));
        this.managerUserName = getIntent().getStringExtra(Constants.MANAGER_USERNAME);
        Integer notificationId = getIntent().getIntExtra(Constants.NOTIFICATION_ID,0);
        PendingToReadMessagesHelper pendingMessasgesHelper = new PendingToReadMessagesHelper(getApplicationContext());
        pendingMessasgesHelper.removePendingMessages(this.managerUserName);

        Boolean fromNotifications = Boolean.parseBoolean(getIntent().getStringExtra(Constants.FROM_NOTIFICATION));
        if(fromNotifications) {
            NotificationHistoryManager notificationHistoryManager = new NotificationHistoryManager(getApplicationContext());
            notificationHistoryManager.ClearNotifications(managerId.toString());
        }

        if(notificationId != 0)
        {
            LayoutNotificationManager notificationManager = new LayoutNotificationManager(getApplicationContext());
            notificationManager.remove(notificationId);
        }

        refreshView();

        registerReceiver(messageBroadcastReceiver, intentFilter);

        messageListView = (ListView) findViewById(R.id.messages_list);
        ImageButton sendMessage = (ImageButton) this.findViewById(R.id.btn_sendMessage);
        sendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new HttpSendMessageTask().execute();
            }
        });
    }

    @Override
    public void onResume(){
        super.onResume();
        registerReceiver(messageBroadcastReceiver, intentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(messageBroadcastReceiver);
    }


    private class HttpFillConversationTask extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            messages = new ArrayList<>();
        }

        @Override
        public Boolean doInBackground(Void... params) {
            try {

                DATA_GetConversationHistoryResponse response = ServiceFactory.getInstance().getMessageService(getApplicationContext()).getConversationHistory(userId, managerId, url);

                if (response.getResponseCode() == ResponseCode.DATA_RC_OK){
                    messages = response.getMessages();
                    return true;
                }else
                {
                    ToastFactory.buildSimpleToast(response.getResponseCodeDescription(), ConversationActivity.this);
                    return false;
                }
            } catch (Exception e) {
                Log.e("PhotoActivity", e.getMessage(), e);
                return false;
            }

        }

        @Override
        protected void onPostExecute(Boolean result) {
            if(result){
                runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                messageListAdapter = new MessageListAdapter(getApplicationContext(),messages);
                                messageListView.setAdapter(messageListAdapter);
                                messageListView.setSelection(messages.size()-1);
                            }});
            }
            else
            {
                ToastFactory.buildSimpleToast(getResources().getString(R.string.error_on_Action), ConversationActivity.this);
            }
        }
    }

    private class HttpSendMessageTask extends AsyncTask<Void, Void, Boolean> {
        private String message;
        private TextView messageTextView;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            messageTextView = (TextView) findViewById(R.id.message_text);
        }

        @Override
        public Boolean doInBackground(Void... params) {
            try {

                message = messageTextView.getText().toString();
                DATA_SendMessageResponse response = ServiceFactory.getInstance().getMessageService(getApplicationContext()).sendMessageTo(userId, managerId, message, url);

                if (response.getResponseCode() == ResponseCode.DATA_RC_OK){
                    return true;
                }else
                {
                    ToastFactory.buildSimpleToast(response.getResponseCodeDescription(), ConversationActivity.this);
                    return false;
                }
            } catch (Exception e) {
                Log.e("PhotoActivity", e.getMessage(), e);
                return false;
            }

        }

        @Override
        protected void onPostExecute(Boolean result) {
            /*if (dialog.isShowing()) {
                dialog.dismiss();
            }*/
            if(result){
                new HttpFillConversationTask().execute();
                runOnUiThread(
                        new Runnable() {
                            @Override
                            public void run() {
                                messageListView.setSelection(messages.size() - 1);
                                messageTextView.setText("");
                            }
                        });
            }
            else
            {
                ToastFactory.buildSimpleToast(getResources().getString(R.string.error_on_Action), ConversationActivity.this);
            }

        }
    }
}
