package com.wynsistems.alarmas.service.update.request;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.util.Hashtable;

public class DATA_UpdateApplicationRequest implements KvmSerializable {

    private String sessionId;

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    @Override
    public Object getProperty(int i) {
        return sessionId;
    }

    @Override
    public int getPropertyCount() {
        return 1;
    }

    @Override
    public void setProperty(int i, Object o) {
        sessionId = o.toString();
    }

    @Override
    public void getPropertyInfo(int i, Hashtable hashtable, PropertyInfo propertyInfo) {
        propertyInfo.type = PropertyInfo.STRING_CLASS;
        propertyInfo.name = "SessionId";
    }
}
