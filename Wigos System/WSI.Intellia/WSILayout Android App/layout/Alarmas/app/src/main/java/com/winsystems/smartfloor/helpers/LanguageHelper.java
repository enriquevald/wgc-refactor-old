package com.winsystems.smartfloor.helpers;

import android.preference.Preference;

import com.winsystems.smartfloor.view.setting.SettingsFragment;

import java.util.Locale;

public class LanguageHelper {
  private static String[] availablesLanguages = {"es_ar", "en_us"};

  public static void setCurrentLocale(Locale locale, SettingsFragment settingsFragment) {
    boolean languageFounded = false;
    int i = 0;
    String currentLanguage = locale.getDefault().toString();
    while (!languageFounded && i < availablesLanguages.length) {
      if (availablesLanguages[i].equals(currentLanguage)) {
        Preference language = settingsFragment.findPreference("language");
        language.setDefaultValue(i+1);
        languageFounded = true;
      }
      i++;
    }
  }

  public static String getLanguageByValue(int index){
    String result = "";
    if(!(index > availablesLanguages.length) && !(index < 0)){
      result =availablesLanguages[--index];
    }
    return result;
  }
}
