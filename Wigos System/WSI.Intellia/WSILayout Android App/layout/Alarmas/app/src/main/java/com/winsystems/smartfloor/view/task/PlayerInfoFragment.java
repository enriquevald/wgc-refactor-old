package com.winsystems.smartfloor.view.task;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.winsystems.smartfloor.R;
import com.winsystems.smartfloor.model.LayoutPlayerSession;
import com.winsystems.smartfloor.service.ServiceFactory;
import com.winsystems.smartfloor.service.login.response.LayoutSiteTask;
import com.winsystems.smartfloor.service.login.response.LayoutSiteTaskTerminal;
import com.winsystems.smartfloor.view.utils.Constants;

public class PlayerInfoFragment  extends Fragment {
  private String idAlarm;
  private LayoutSiteTask task;
  private String alarmServerUrl;

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {

    View rootView = inflater.inflate(R.layout.activity_terminal_details_player_info, container, false);

    Boolean downloadAlarms = Boolean.parseBoolean(getActivity().getIntent().getStringExtra(Constants.DOWNLOAD_ALARMS));
    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
    alarmServerUrl = prefs.getString("alarm_server_address", alarmServerUrl);

    if(downloadAlarms){
      idAlarm = getActivity().getIntent().getStringExtra(Constants.ALARM_ID);
      task = ServiceFactory.getInstance().getAlarmService(getActivity().getApplicationContext()).getById(idAlarm);
    }else {
      LayoutSiteTask selectedAlarm = (LayoutSiteTask) getActivity().getIntent().getSerializableExtra(Constants.SELECTED_ALARM);
      task = ServiceFactory.getInstance().getAlarmService(getActivity().getApplicationContext()).getById(selectedAlarm.getId().toString());
    }

    LayoutSiteTaskTerminal terminal = task.getTerminal();
    LayoutPlayerSession playerSession = terminal.getPlayerSession();
    LinearLayout isAnonymous = (LinearLayout) rootView.findViewById(R.id.active_player_session_anonymous);
    LinearLayout isNotAnonymous = (LinearLayout) rootView.findViewById(R.id.active_player_session_register);
    LinearLayout notRegister = (LinearLayout) rootView.findViewById(R.id.no_active_player_session);
    if(playerSession != null){
      if(playerSession.getIsAnonymous())
      {
        isAnonymous.setVisibility(View.VISIBLE);
        isNotAnonymous.setVisibility(View.GONE);
        notRegister.setVisibility(View.GONE);
      }else{
        isAnonymous.setVisibility(View.GONE);
        isNotAnonymous.setVisibility(View.VISIBLE);
        notRegister.setVisibility(View.GONE);

        ImageView genderImage = (ImageView) rootView.findViewById(R.id.player_gender);
        switch (playerSession.getGender()){
          case FEMALE:
            genderImage.setImageResource(R.drawable.ico_photo_female);
            break;
          case MALE:
            genderImage.setImageResource(R.drawable.ico_photo_male);
            break;
        }

        TextView playerName = (TextView) rootView.findViewById(R.id.player_name);
        playerName.setText(playerSession.getName());

        TextView playerAge = (TextView) rootView.findViewById(R.id.player_age);
        playerAge.setText(String.valueOf(playerSession.getAge()));

        TextView playerLevel = (TextView) rootView.findViewById(R.id.player_level);
        playerLevel.setText(playerSession.getLevel().getText());

        FrameLayout isVip = (FrameLayout) rootView.findViewById(R.id.player_isvip);
        isVip.setVisibility(playerSession.getIsVip()? View.VISIBLE : View.GONE);
      }
    }else{
      isAnonymous.setVisibility(View.GONE);
      isNotAnonymous.setVisibility(View.GONE);
      notRegister.setVisibility(View.VISIBLE);
    }
    return rootView;
  }
}