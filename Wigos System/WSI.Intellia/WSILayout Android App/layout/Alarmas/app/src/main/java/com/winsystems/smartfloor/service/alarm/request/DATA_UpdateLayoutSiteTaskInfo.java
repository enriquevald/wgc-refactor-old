package com.winsystems.smartfloor.service.alarm.request;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.util.Hashtable;

public class DATA_UpdateLayoutSiteTaskInfo implements KvmSerializable{
  private String sessionId;
  private String taskId;
  private int action;
  private String reason;

  public void setSessionId(String sessionId) {
    this.sessionId = sessionId;
  }

  public void setTaskId(String taskId) {
    this.taskId = taskId;
  }

  public void setAction(int action) {
    this.action = action;
  }

  @Override
  public Object getProperty(int i) {
    switch (i){
      case 0:
        return action;
      case 1:
        return reason;
      case 2:
        return sessionId;
      case 3:
        return taskId;
      default:
        return null;
    }
  }

  @Override
  public int getPropertyCount() {
    return 4;
  }

  @Override
  public void setProperty(int i, Object o) {
    switch (i){
      case 0:
        action = Integer.parseInt(o.toString());
      case 1:
        reason = o.toString();
      case 2:
        sessionId = o.toString();
      case 3:
        taskId = o.toString();
    }
  }

  @Override
  public void getPropertyInfo(int i, Hashtable hashtable, PropertyInfo propertyInfo) {
    switch (i){
      case 0:
        propertyInfo.type = PropertyInfo.INTEGER_CLASS;
        propertyInfo.name = "Action";
        break;
      case 1:
        propertyInfo.type = PropertyInfo.STRING_CLASS;
        propertyInfo.name = "Reason";
        break;
      case 2:
        propertyInfo.type = PropertyInfo.STRING_CLASS;
        propertyInfo.name = "SessionId";
        break;
      case 3:
        propertyInfo.type = PropertyInfo.STRING_CLASS;
        propertyInfo.name = "TaskId";
        break;
    }
  }

  public void setReason(String reason) {
    this.reason = reason;
  }
}
