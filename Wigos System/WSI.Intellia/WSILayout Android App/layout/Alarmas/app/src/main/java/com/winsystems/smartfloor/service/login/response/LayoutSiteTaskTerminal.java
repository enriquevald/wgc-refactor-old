package com.winsystems.smartfloor.service.login.response;

import com.winsystems.smartfloor.model.LayoutPlayerSession;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.io.Serializable;
import java.util.Hashtable;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: LayoutSiteTaskTerminal
//
// DESCRIPTION: Class that contains methods for the terminal, for set and get session player.
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public class LayoutSiteTaskTerminal implements KvmSerializable, Serializable {

  private String provider;
  private String name;
  private String area;
  private String bank;
  private String model;
  private int id;

  public void setPlayerSession(LayoutPlayerSession playerSession) {
    this.playerSession = playerSession;
  }

  public LayoutPlayerSession getPlayerSession() {
    return playerSession;
  }

  private LayoutPlayerSession playerSession;

  @Override
  public Object getProperty(int i) {
    switch (i){
      case 0:
        return area;
      case 1:
        return bank;
      case 2:
        return id;
      case 3:
        return model;
      case 4:
        return name;
      case 5:
        return provider;
      case 6:
        return playerSession;
      default:
        return null;
    }
  }

  public String getModel() {
    return model;
  }

  public void setModel(String model) {
    this.model = model;
  }

  public String getArea() {
    return area;
  }

  public void setArea(String area) {
    this.area = area;
  }

  public String getBank() {
    return bank;
  }

  public void setBank(String bank) {
    this.bank = bank;
  }

  public String getProvider() {
    return provider;
  }

  public void setProvider(String provider) {
    this.provider = provider;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  @Override
  public int getPropertyCount() {
    return 7;
  }

  @Override
  public void setProperty(int i, Object o) {
    switch (i){
      case 0:
        area = o.toString();
        break;
      case 1:
        bank = o.toString();
        break;
      case 2:
        id = Integer.parseInt(o.toString());
        break;
      case 3:
        model = o.toString();
        break;
      case 4:
        name = o.toString();
        break;
      case 5:
        provider = o.toString();
      case 6:
        playerSession = (LayoutPlayerSession)o;
        break;
    }
  }

  @Override
  public void getPropertyInfo(int i, Hashtable hashtable, PropertyInfo propertyInfo) {
    switch (i){
      case 0:
        propertyInfo.type = PropertyInfo.STRING_CLASS;
        propertyInfo.name = "Area";
        break;
      case 1:
        propertyInfo.type = PropertyInfo.STRING_CLASS;
        propertyInfo.name = "Bank";
        break;
      case 2:
        propertyInfo.type = PropertyInfo.INTEGER_CLASS;
        propertyInfo.name = "Id";
        break;
      case 3:
        propertyInfo.type = PropertyInfo.STRING_CLASS;
        propertyInfo.name = "Model";
        break;
      case 4:
        propertyInfo.type = PropertyInfo.STRING_CLASS;
        propertyInfo.name = "Name";
        break;
      case 5:
        propertyInfo.type = PropertyInfo.STRING_CLASS;
        propertyInfo.name = "Provider";
      case 6:
        propertyInfo.type = PropertyInfo.OBJECT_CLASS;
        propertyInfo.name = "PlayerSession";
        break;

    }
  }
}
