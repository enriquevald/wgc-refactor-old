package com.winsystems.smartfloor.helpers;


import android.content.Context;

import com.winsystems.smartfloor.view.utils.Constants;

import java.util.ArrayList;
import java.util.List;

public class PendingToReadMessagesHelper {
  private SaveInformationHelper<LayoutPendingMessage> _pendingMessages;

  public PendingToReadMessagesHelper(Context context) {
    this._pendingMessages = new SaveInformationHelper<>(context);
  }

  public void addPendingMessage(LayoutPendingMessage message){
    List<LayoutPendingMessage> pendingMessages = _pendingMessages.readList(Constants.PENDING_MESSAGES);
    pendingMessages.add(message);

    _pendingMessages.writeList(pendingMessages, Constants.PENDING_MESSAGES);
  }

  public void removePendingMessages(String manager){
    List<LayoutPendingMessage> pendingMessages = _pendingMessages.readList(Constants.PENDING_MESSAGES);
    List<LayoutPendingMessage> clearPendingList = new ArrayList<>();
    for(LayoutPendingMessage message: pendingMessages) {
      if(!message.getManager().equals(manager)){
        clearPendingList.add(message);
      }
    }
    if(clearPendingList.isEmpty())
      _pendingMessages.clearList(Constants.PENDING_MESSAGES);
    else
      _pendingMessages.writeList(clearPendingList, Constants.PENDING_MESSAGES);
  }

  public int countPendingMessagesOfManager(String manager){
    Integer count = 0;
    List<LayoutPendingMessage> pendingMessages = _pendingMessages.readList(Constants.PENDING_MESSAGES);
    for(LayoutPendingMessage message: pendingMessages) {
      if(message.getManager().equals(manager)){
        count++;
      }
    }

    return count;
  }

  public Boolean havePendingMessages(){
    List<LayoutPendingMessage> pendingMessages = _pendingMessages.readList(Constants.PENDING_MESSAGES);
    return pendingMessages.size() > 0;
  }
}
