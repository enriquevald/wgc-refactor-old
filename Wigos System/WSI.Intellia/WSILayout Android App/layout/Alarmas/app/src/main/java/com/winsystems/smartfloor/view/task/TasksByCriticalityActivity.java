package com.winsystems.smartfloor.view.task;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.winsystems.smartfloor.R;
import com.winsystems.smartfloor.model.SummarizedAlarms;
import com.winsystems.smartfloor.service.ServiceFactory;
import com.winsystems.smartfloor.service.alarm.AlarmService;
import com.winsystems.smartfloor.service.login.response.LayoutSiteTask;
import com.winsystems.smartfloor.service.mqtt.PahoMqttService;
import com.winsystems.smartfloor.helpers.AlarmResourseHelper;
import com.winsystems.smartfloor.view.utils.Constants;
import com.winsystems.smartfloor.view.utils.KioskModeActivity;

import java.util.Collections;
import java.util.List;

public class TasksByCriticalityActivity extends KioskModeActivity {
  private SummarizedAlarms currentSummarizedAlarms;
  private ListView alarmsView;
  private IntentFilter intentFilter = new IntentFilter();
  private TaskByCriticalityListAdapter taskByCriticalityListAdapter;
  private BroadcastReceiver tasksUpdatedBroadcastReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      TasksByCriticalityActivity.this.refreshView();
    }
  };

  private void refreshView() {
    AlarmService service = ServiceFactory.getInstance().getAlarmService(getApplicationContext());
    List<LayoutSiteTask> layoutSiteTasks = currentSummarizedAlarms.getAlarmCategoryType()!= null ? service.getAlarmByCategory(currentSummarizedAlarms.getAlarmCategoryType())
        : service.getAlarmByCriticality(currentSummarizedAlarms.getAlarmCriticality());
    Collections.sort(layoutSiteTasks);
    taskByCriticalityListAdapter = new TaskByCriticalityListAdapter(this, layoutSiteTasks);
    alarmsView.setAdapter(taskByCriticalityListAdapter);

  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_tasks_by_criticality);
    currentSummarizedAlarms = (SummarizedAlarms) getIntent().getSerializableExtra(Constants.SELECTED_GROUP_ALARMS);
    alarmsView =  (ListView) findViewById(R.id.tasks_by_criticality);
    ActionBar actionBar = getSupportActionBar();
    taskByCriticalityListAdapter = new TaskByCriticalityListAdapter(this, currentSummarizedAlarms.getTasks());

    alarmsView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        LayoutSiteTask item = (LayoutSiteTask) taskByCriticalityListAdapter.getItem(position);

        Intent intent = new Intent(TasksByCriticalityActivity.this, TaskDetailActivity.class);
        intent.putExtra(Constants.SELECTED_ALARM, item);
        startActivity(intent);
      }
    });

    actionBar.setTitle(currentSummarizedAlarms.getDescription());
    if(currentSummarizedAlarms.isSeverity())
      actionBar.setBackgroundDrawable(new ColorDrawable(AlarmResourseHelper.getColorByCriticality(currentSummarizedAlarms.getAlarmCriticality(),getResources())));

    alarmsView.setAdapter(taskByCriticalityListAdapter);

    intentFilter.addAction(Constants.UPDATED_TASKS);
    intentFilter.addAction(PahoMqttService.NEW_TASK_ARRIVE);
  }

  @Override
  public void onResume(){
    super.onResume();
    refreshView();
    registerReceiver(tasksUpdatedBroadcastReceiver, intentFilter);


  }
  @Override
  protected void onPause() {
    super.onPause();
    unregisterReceiver(tasksUpdatedBroadcastReceiver);
  }

  @Override
  protected void onStop() {
    super.onStop();
  }
}
