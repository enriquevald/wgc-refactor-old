package com.winsystems.smartfloor.helpers;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Environment;

import com.winsystems.smartfloor.repository.BaseInternalDirectoryFactory;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: CameraSetupHelper
//
// DESCRIPTION: Implements methods to configure images and photographs.
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public class CameraSetupHelper {
  private static final String JPEG_FILE_SUFFIX = ".jpg";
  private static final String JPEG_FILE_PREFIX = "IMG_";
  private static BaseInternalDirectoryFactory albumStorageDirFactory = new BaseInternalDirectoryFactory();
  private static final int IMAGE_MAX_SIZE = 1600;

  public static boolean deletePhotoFile(String photoPath){
    if(photoPath != null)
    {
      File photo = new File(photoPath);
      return photo.delete();
    }

    return true;
  }

  public static File setupFilePathForCameraActivity(){
    File f = null;
    try {
      f = setUpPhotoFile();
    } catch (IOException e) {
      e.printStackTrace();
    }

    return f;
  }

  private static File createImageFile() throws IOException {
    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
    String imageFileName = JPEG_FILE_PREFIX + timeStamp + "_";
    File albumF = getAlbumDir();
    return File.createTempFile(imageFileName, JPEG_FILE_SUFFIX, albumF);
  }

  private static File setUpPhotoFile() throws IOException {
    return createImageFile();
  }

  // Photo album for this application
  private static String getAlbumName() {
    return "WinSystems";
  }


  //Get album directory and checks if media storage is mounted
  private static File getAlbumDir() {
    File storageDir = null;
    if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
      storageDir = albumStorageDirFactory.getAlbumStorageDir(getAlbumName());
      if (! storageDir.mkdirs()) {
        if (! storageDir.exists()){
          return null;
        }
      }

    }
    return storageDir;
  }

  public static Bitmap LoadBitmatOfPhotoTaken(String photoPath) {
    try {

      BitmapFactory.Options bmOptions = new BitmapFactory.Options();
      bmOptions.inJustDecodeBounds = true; // if it is true, decodeFile doesn't return a Bitmap then access to memory is optimized
      BitmapFactory.decodeFile(photoPath, bmOptions);
      int photoW = bmOptions.outWidth;
      int photoH = bmOptions.outHeight;

      // Figure out which way needs to be reduced less
      int scaleFactor = 1;

      if (photoH > IMAGE_MAX_SIZE || photoW> IMAGE_MAX_SIZE) {
        scaleFactor = (int) Math.pow(2, (int) Math.round(Math.log(IMAGE_MAX_SIZE/ (double) Math.max(photoH, photoW)) / Math.log(0.5)));
      }

      // Set bitmap options to scale the image decode target
      bmOptions.inJustDecodeBounds = false;
      bmOptions.inSampleSize = scaleFactor;

      return tryRotateBitmap(photoPath, bmOptions);

    } catch (IOException e) {
      e.printStackTrace();
    }

    return null;
  }

  private static Bitmap tryRotateBitmap(String currentPhotoPath, BitmapFactory.Options options) throws IOException {
    //Get orientation metadata of a picture. If it is necessary it will be rotated
    ExifInterface exif = new ExifInterface(currentPhotoPath);
    int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);

    Bitmap bm = BitmapFactory.decodeFile(currentPhotoPath,options);
    Matrix m = new Matrix();
    if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
      m.postRotate(90);
      bm = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(),
          bm.getHeight(), m, true);
    }
    else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
      m.postRotate(270);
      bm = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(),
          bm.getHeight(), m, true);
    }
    return bm;
  }

}
