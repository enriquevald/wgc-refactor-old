package com.winsystems.smartfloor.view.menu;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: MenuItem
//
// DESCRIPTION: Implements method  corresponding to the menu items of the application.
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public class MenuItem {
  private int urlDrawable;
  private String description;
  private Class<?> targetClass;

  public MenuItem(int urlDrawable, String description, Class<?> targetClass){
    this.urlDrawable = urlDrawable;
    this.description = description;
    this.targetClass = targetClass;
  }

  public int getUrlDrawable() {
    return this.urlDrawable;
  }

  public String getDescription() {
    return this.description;
  }

  public Class<?> getTargetClass(){
    return this.targetClass;
  }
}
