package com.wynsistems.alarmas.service.mqtt.request;


import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.util.Date;
import java.util.Hashtable;

public class DATA_BeaconRunnerDistance implements KvmSerializable{
    private String deviceId;
    private String sessionId;
    private String positionX;
    private String positionY;
    private String floorId;

    public void setFloorId(String floorId) {
        this.floorId = floorId;
    }
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public void setPositionX(String positionX) {
        this.positionX = positionX;
    }

    public void setPositionY(String positionY) {
        this.positionY = positionY;
    }

    @Override
    public Object getProperty(int i) {
        switch (i){
            case 0:
                return deviceId;
            case 1:
                return floorId;
            case 2:
                return  positionX;
            case 3:
                return  positionY;
            case 4:
                return sessionId;
            default:
                return null;
        }
    }

    @Override
    public int getPropertyCount() {
        return 5;
    }

    @Override
    public void setProperty(int i, Object o) {
        switch (i){
            case 0:
                deviceId = o.toString();
                break;
            case 1:
                floorId = o.toString();
                break;
            case 2:
                positionX = o.toString();
                break;
            case 3:
                positionY = o.toString();
                break;
            case 4:
                sessionId = o.toString();
                break;
        }

    }

    @Override
    public void getPropertyInfo(int i, Hashtable hashtable, PropertyInfo propertyInfo) {
        switch (i){
            case 0:
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                propertyInfo.name = "DeviceId";
                break;
            case 1:
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                propertyInfo.name = "FloorId";
                break;
            case 2:
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                propertyInfo.name = "PositionX";
                break;
            case 3:
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                propertyInfo.name = "PositionY";
                break;
            case 4:
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                propertyInfo.name = "SessionId";
                break;
        }
    }
}
