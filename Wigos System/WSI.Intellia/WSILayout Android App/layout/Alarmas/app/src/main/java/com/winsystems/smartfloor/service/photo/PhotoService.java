package com.winsystems.smartfloor.service.photo;

import android.content.Context;

import com.winsystems.smartfloor.service.DATA_PostActionResponse;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: PhotoService
//
// DESCRIPTION: Interfaces and classes.
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public interface PhotoService {

  DATA_PostActionResponse UploadPhoto(Context appContext, String url, String userName, String imagePath, String comment, String externalId, Integer type);
}
