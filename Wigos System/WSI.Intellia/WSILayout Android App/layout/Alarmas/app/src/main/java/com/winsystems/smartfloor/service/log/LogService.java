package com.winsystems.smartfloor.service.log;

import com.winsystems.smartfloor.service.log.response.DATA_SaveLogResponse;

import java.util.Date;

/**
 * Created by maxim on 2/3/2016.
 */
public interface LogService {
    DATA_SaveLogResponse saveLog();
}
