package com.wynsistems.alarmas.view.utils;


import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.wynsistems.alarmas.helpers.KioskModeHelper;
import com.wynsistems.alarmas.helpers.LanguageHelper;
import com.wynsistems.alarmas.model.AlarmGroupingCriteria;
import com.wynsistems.alarmas.model.AlarmSortingCriteria;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class KioskModeActivity extends ActionBarActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
    private SharedPreferences prefs;
    protected AlarmGroupingCriteria groupPreference;
    protected AlarmSortingCriteria sortPreference;
    private String language;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFullScreen();
        prefs = PreferenceManager.getDefaultSharedPreferences(this);//get the preferences that are allowed to be given
        prefs.registerOnSharedPreferenceChangeListener(this);//set the listener to listen for changes in the preferences

        updatePreferencesAndLanguage();

        Locale newLocale = new Locale(LanguageHelper.getLanguageByValue(Integer.valueOf(prefs.getString("language", "1").toString())));
        Locale.setDefault(newLocale);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = newLocale;
        res.updateConfiguration(conf, dm);

    }

    @Override
    public void onResume(){
        super.onResume();
        String defaultLanguage = Locale.getDefault().getDisplayLanguage();
        //updatePreferencesAndLanguage();
        if (!defaultLanguage.equals(language)){
            finish();
            startActivity(getIntent());
        }
    }

    private void updatePreferencesAndLanguage() {
        groupPreference = AlarmGroupingCriteria.getFromString(prefs.getString("grouping", "1"));
        sortPreference = AlarmSortingCriteria.getFromString(prefs.getString("sorting", "1"));
        language = prefs.getString("languageToLoad", Locale.getDefault().getDisplayLanguage());
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if(!hasFocus && KioskModeHelper.isKioskModeActive(getApplicationContext())) {
            // Close every kind of system dialog
            Intent closeDialog = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
            sendBroadcast(closeDialog);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
       if( KioskModeHelper.isKioskModeActive(getApplicationContext())) {
           ActivityManager activityManager = (ActivityManager) getApplicationContext()
                   .getSystemService(Context.ACTIVITY_SERVICE);

           activityManager.moveTaskToFront(getTaskId(), 0);
       }
    }

    @Override
    public void onBackPressed() {
        // nothing to do here
    }

    /**
     * Detects and toggles immersive mode (also known as "hidey bar" mode).
     */
    public void setFullScreen() {
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        View decorView = getWindow().getDecorView();
//        int uiOptions =  View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION;
//                        //| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
//        decorView.setSystemUiVisibility(uiOptions);
    }



    private final List blockedKeys = new ArrayList(Arrays.asList(KeyEvent.KEYCODE_VOLUME_DOWN, KeyEvent.KEYCODE_VOLUME_UP));

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (blockedKeys.contains(event.getKeyCode()) || KioskModeHelper.isKioskModeActive(getApplicationContext())) {
            return true;
        } else {
            return super.dispatchKeyEvent(event);
        }
    }


    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if((keyCode==KeyEvent.KEYCODE_BACK || keyCode==KeyEvent.KEYCODE_APP_SWITCH) && KioskModeHelper.isKioskModeActive(getApplicationContext()))
        {
            if(KioskModeHelper.isKioskModeActive(getApplicationContext()))
                return true;
            else
                return super.onKeyDown(keyCode, event);
        }
        else
            return super.onKeyDown(keyCode, event);
    }


}
