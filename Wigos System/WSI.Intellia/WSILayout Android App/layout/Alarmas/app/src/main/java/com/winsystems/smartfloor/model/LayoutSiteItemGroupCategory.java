package com.winsystems.smartfloor.model;


import android.content.Context;

import com.winsystems.smartfloor.R;

public enum LayoutSiteItemGroupCategory {
  PayCheck,
  Intrusion,
  JamBill,
  MachineFlags,
  Support,
  Printer,
  Custom,
  Other;


  public static String getTypeDescription(LayoutSiteItemGroupCategory alarmCategoryType, Context context)
  {
    switch (alarmCategoryType) {
      case PayCheck:
        return context.getResources().getString(R.string.paycheck);
      case Intrusion:
        return context.getResources().getString(R.string.intrusion);
      case JamBill:
        return context.getResources().getString(R.string.jambill);
      case MachineFlags:
        return context.getResources().getString(R.string.machine_flags);
      case Support:
        return context.getResources().getString(R.string.support);
      case Printer:
        return context.getResources().getString(R.string.printer);
      case Custom:
        return context.getResources().getString(R.string.other_category_title);
      case Other:
        return context.getResources().getString(R.string.other_category_title);
      default:
        return context.getResources().getString(R.string.printer);
    }
  }


}
