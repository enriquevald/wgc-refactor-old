package com.winsystems.smartfloor.service.login.response;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.io.Serializable;
import java.util.Date;
import java.util.Hashtable;

public class LayoutSiteTask implements KvmSerializable,Serializable, Comparable<LayoutSiteTask>{
  private Integer id;
  private Integer status;
  private Date start;
  private Date end;
  private Date creation;
  private Date assigned;
  private Date accepted;
  private Integer category;
  private Integer subcategory;
  private Integer terminalId;
  private Integer accountId;
  private String description;
  private Integer severity;
  private LayoutSiteTaskTerminal terminal;
  private String title;

  public LayoutSiteTask(){}

  public String getDescription() {
    return description;
  }

  public Integer getId() {
    return id;
  }

  public Date getCreation() {
    return creation;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Integer getStatus() {
    return status;
  }

  public void setStatus(Integer status) {
    this.status = status;
  }

  public Date getStart() {
    return start;
  }

  public void setStart(Date start) {
    this.start = start;
  }

  public Date getEnd() {
    return end;
  }

  public void setEnd(Date end) {
    this.end = end;
  }

  public Integer getCategory() {
    return category;
  }

  public void setCategory(Integer category) {
    this.category = category;
  }

  public Integer getSubcategory() {
    return subcategory;
  }

  public void setSubcategory(Integer subcategory) {
    this.subcategory = subcategory;
  }

  public Integer getTerminalId() {
    return terminalId;
  }

  public void setTerminalId(Integer terminalId) {
    this.terminalId = terminalId;
  }

  public Integer getAccountId() {
    return accountId;
  }

  public void setAccountId(Integer accountId) {
    this.accountId = accountId;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Integer getSeverity() {
    return severity;
  }

  public void setCreation(Date creation) {
    this.creation = creation;
  }

  public void setSeverity(Integer severity) {
    this.severity = severity;
  }

  public LayoutSiteTaskTerminal getTerminal() {
    return terminal;
  }

  public void setTerminal(LayoutSiteTaskTerminal terminal) {
    this.terminal = terminal;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public Date getAssigned() {
    return assigned;
  }

  public void setAssigned(Date assigned) {
    this.assigned = assigned;
  }

  public Date getAccepted() {
    return accepted;
  }

  public void setAccepted(Date accepted) {
    this.accepted = accepted;
  }

  @Override
  public int compareTo(LayoutSiteTask another) {
    return getId().compareTo(another.getId());
  }

  @Override
  public Object getProperty(int i) {
    switch (i){
      case 0:
        return accepted;
      case 1:
        return accountId;
      case 2:
        return assigned;
      case 3:
        return category;
      case 4:
        return creation;
      case 5:
        return description;
      case 6:
        return end;
      case 7:
        return id;
      case 8:
        return severity;
      case 9:
        return start;
      case 10:
        return status;
      case 11:
        return subcategory;
      case 12:
        return terminal;
      case 13:
        return terminalId;
      case 14:
        return title;
      default:
        return null;
    }
  }

  @Override
  public int getPropertyCount() {
    return 15;
  }

  @Override
  public void setProperty(int i, Object o) {
    switch (i){
      case 0:
        accepted = (Date) o;
      case 1:
        accountId = Integer.parseInt(o.toString());
      case 2:
        assigned = (Date) o;
      case 3:
        category = Integer.parseInt(o.toString());
      case 4:
        creation = (Date) o;
      case 5:
        description = o.toString();
      case 6:
        end = (Date) o;
      case 7:
        id = Integer.parseInt(o.toString());
      case 8:
        severity = Integer.parseInt(o.toString());
      case 9:
        start = (Date) o;
      case 10:
        status = Integer.parseInt(o.toString());
      case 11:
        subcategory = Integer.parseInt(o.toString());
      case 12:
        terminal = (LayoutSiteTaskTerminal) o;
      case 13:
        terminalId = Integer.parseInt(o.toString());
      case 14:
        title = o.toString();
    }
  }

  @Override
  public void getPropertyInfo(int i, Hashtable hashtable, PropertyInfo propertyInfo) {
    switch (i){
      case 0:
        propertyInfo.type = PropertyInfo.OBJECT_CLASS;
        propertyInfo.name = "Accepted";
      case 1:
        propertyInfo.type = PropertyInfo.INTEGER_CLASS;
        propertyInfo.name = "AccountId";
      case 2:
        propertyInfo.type = PropertyInfo.OBJECT_CLASS;
        propertyInfo.name = "Assigned";
      case 3:
        propertyInfo.type = PropertyInfo.INTEGER_CLASS;
        propertyInfo.name = "Category";
      case 4:
        propertyInfo.type = PropertyInfo.OBJECT_CLASS;
        propertyInfo.name = "Creation";
      case 5:
        propertyInfo.type = PropertyInfo.STRING_CLASS;
        propertyInfo.name = "Description";
      case 6:
        propertyInfo.type = PropertyInfo.OBJECT_CLASS;
        propertyInfo.name = "End";
      case 7:
        propertyInfo.type = PropertyInfo.INTEGER_CLASS;
        propertyInfo.name = "Id";
      case 8:
        propertyInfo.type = PropertyInfo.INTEGER_CLASS;
        propertyInfo.name = "Severity";
      case 9:
        propertyInfo.type = PropertyInfo.OBJECT_CLASS;
        propertyInfo.name = "Start";
      case 10:
        propertyInfo.type = PropertyInfo.INTEGER_CLASS;
        propertyInfo.name = "Status";
      case 11:
        propertyInfo.type = PropertyInfo.INTEGER_CLASS;
        propertyInfo.name = "SubCategory";
      case 12:
        propertyInfo.type = PropertyInfo.OBJECT_CLASS;
        propertyInfo.name = "Terminal";
      case 13:
        propertyInfo.type = PropertyInfo.INTEGER_CLASS;
        propertyInfo.name = "TerminalId";
      case 14:
        propertyInfo.type = PropertyInfo.STRING_CLASS;
        propertyInfo.name = "Title";
    }
  }
}
