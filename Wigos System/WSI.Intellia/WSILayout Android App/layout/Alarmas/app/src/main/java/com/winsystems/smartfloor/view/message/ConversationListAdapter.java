package com.winsystems.smartfloor.view.message;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.winsystems.smartfloor.helpers.DisplayDateHelper;
import com.winsystems.smartfloor.R;
import com.winsystems.smartfloor.helpers.PendingToReadMessagesHelper;
import com.winsystems.smartfloor.model.LayoutSiteManagerRunnerConversationItem;

import java.util.ArrayList;
import java.util.List;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: ConversationListAdapter
//
// DESCRIPTION: Activity that implements the functionality of conversation.
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public class ConversationListAdapter extends BaseAdapter {

  private Context _context;
  LayoutInflater inflater;
  private List<LayoutSiteManagerRunnerConversationItem> _layoutSiteManagerRunnerConversationItems;

  public ConversationListAdapter(Context context, List<LayoutSiteManagerRunnerConversationItem> layoutSiteManagerRunnerConversationItems) {
    _context = context;
    if(layoutSiteManagerRunnerConversationItems != null)
      this._layoutSiteManagerRunnerConversationItems = layoutSiteManagerRunnerConversationItems;
    else
      this._layoutSiteManagerRunnerConversationItems = new ArrayList<>();

    inflater = (LayoutInflater) context
        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
  }

  @Override
  public int getCount() {
    return this._layoutSiteManagerRunnerConversationItems.size();
  }

  @Override
  public Object getItem(int position) {
    return this._layoutSiteManagerRunnerConversationItems.get(position);
  }

  @Override
  public long getItemId(int position) {
    return position;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    convertView = inflater.inflate(R.layout.messages_conversation_list_item, null);

    LayoutSiteManagerRunnerConversationItem item = (LayoutSiteManagerRunnerConversationItem)getItem(position);
    PendingToReadMessagesHelper pendingMessasgesHelper = new PendingToReadMessagesHelper(_context);

    TextView managerUserName = (TextView)convertView.findViewById(R.id.manager_name);
    TextView managerLastMessage = (TextView)convertView.findViewById(R.id.manager_lastMessage);
    TextView managerLastMessageDate = (TextView)convertView.findViewById(R.id.manager_lastMessageDate);
    TextView pendingMessagesCounter = (TextView) convertView.findViewById(R.id.pending_messages_count);
    FrameLayout pendingMessagesBubble = (FrameLayout) convertView.findViewById(R.id.pending_messages);
    Integer pendingMessagesCount = pendingMessasgesHelper.countPendingMessagesOfManager(item.getManagerUserName());
    if(pendingMessagesCount > 0){
      pendingMessagesCounter.setText(pendingMessagesCount.toString());
      pendingMessagesBubble.setVisibility(View.VISIBLE);
    }else{
      pendingMessagesBubble.setVisibility(View.GONE);
    }
    //managerUserName.setTextColor(_context.getResources().getColor(R.color.abc_background_cache_hint_selector_material_dark));
    managerUserName.setText(item.getManagerUserName());
    managerLastMessage.setText(item.getManagerLastMessage());

    managerLastMessageDate.setText(DisplayDateHelper.getDateToShowInMessages((item.getManagerLastMessageDate())));
    return convertView;
  }
}
