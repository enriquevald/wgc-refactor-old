package com.wynsistems.alarmas.view.alarms;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.soundcloud.android.crop.Crop;
import com.wynsistems.alarmas.R;
import com.wynsistems.alarmas.helpers.CameraSetupHelper;
import com.wynsistems.alarmas.model.Alarm;
import com.wynsistems.alarmas.model.LayoutSiteItemCategory;
import com.wynsistems.alarmas.model.LayoutSiteItemSubCategory;
import com.wynsistems.alarmas.model.Terminal;
import com.wynsistems.alarmas.service.DATA_PostActionResponse;
import com.wynsistems.alarmas.service.ResponseCode;
import com.wynsistems.alarmas.service.ServiceFactory;
import com.wynsistems.alarmas.view.camera.CaptureActivityAnyOrientation;
import com.wynsistems.alarmas.helpers.AlarmResourseHelper;
import com.wynsistems.alarmas.view.utils.Constants;
import com.wynsistems.alarmas.view.utils.KioskModeActivity;
import com.wynsistems.alarmas.view.utils.ToastFactory;

import java.io.File;
import java.util.Date;
import java.util.List;

public class NewAlarmActivity extends KioskModeActivity {
    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
    private ProgressDialog dialog;
    private HttpCreateAlarmTask  createAlarmAsynchTask;
    private String userId;
    private String loggedUserCreateAlarmUrl = "";
    private Alarm alarm;
    private ImageView photoContainerView;
    private String photoFromCammera;
    private String cropedPhoto;
    private Uri photoFromCammeraUri;
    private Uri cropedPhotoUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupAlarmFields();
        buildDialog();
    }

    private void dispatchCameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        try {
            File photo = CameraSetupHelper.setupFilePathForCameraActivity();
            photoFromCammeraUri = Uri.fromFile(photo);
            photoFromCammera = photo.getAbsolutePath();
            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoFromCammeraUri);
        } catch (Exception e) {
            e.printStackTrace();
        }
        startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (resultCode == RESULT_OK) {

            switch (requestCode)
            {
                case CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE:
                    File photo = CameraSetupHelper.setupFilePathForCameraActivity();
                    cropedPhotoUri = Uri.fromFile(photo);
                    cropedPhoto = photo.getAbsolutePath();
                    Crop.of(photoFromCammeraUri, cropedPhotoUri).asSquare().withMaxSize(2048, 2048).start(this);
                    break;
                case Crop.REQUEST_CROP:
                    Bitmap bm = CameraSetupHelper.LoadBitmatOfPhotoTaken(cropedPhoto);
                    this.photoContainerView.setImageBitmap(bm);
                    this.photoContainerView.setVisibility(View.VISIBLE);
                    break;
                default:
                    IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
                    if (scanningResult != null) {
                        TextView textView = (TextView)this.findViewById(R.id.new_alarm_terminal_provider_field);
                        textView.setText(scanningResult.getContents());
                    }
                    break;
            }

        }else{
            IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
            if (scanningResult != null) {
                TextView textView = (TextView)this.findViewById(R.id.new_alarm_terminal_provider_field);
                textView.setText(scanningResult.getContents());
            } else {
                Toast toast = Toast.makeText(getApplicationContext(), getString(R.string.no_data_received), Toast.LENGTH_SHORT);
                toast.show();
            }
        }
    }

    private void setupAlarmFields() {
        setContentView(R.layout.activity_new_alarm);
        final LinearLayout content = (LinearLayout) this.findViewById(R.id.content_layout);

        Button createAlarmButton = (Button) this.findViewById(R.id.create_new_alarm_button);
        Button takePictureButton = (Button) this.findViewById(R.id.take_photo_button);
        ImageButton terminalQRCodeButton = (ImageButton) this.findViewById(R.id.terminal_qr_code_button);
        final RadioGroup radioGroup = (RadioGroup) this.findViewById(R.id.radioGroupCategoryType);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                DisplatTerminalPromotionElements(radioGroup);
            }
        });

        DisplatTerminalPromotionElements(radioGroup);


        createAlarmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(NewAlarmActivity.this.isValidAlarm()) {
                    NewAlarmActivity.this.alarm = NewAlarmActivity.this.createAlarmFromScratch();
                    NewAlarmActivity.this.createAlarmAsynchTask = new HttpCreateAlarmTask();
                    NewAlarmActivity.this.createAlarmAsynchTask.execute();
                }
                else{
                    ToastFactory.buildSimpleToast(getResources().getString(R.string.error_field_required_new_alarm), NewAlarmActivity.this);
                }
            }
        });

        takePictureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchCameraIntent();
            }
        });

        terminalQRCodeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentIntegrator scanIntegrator = new IntentIntegrator(NewAlarmActivity.this);
                scanIntegrator.setCaptureActivity(CaptureActivityAnyOrientation.class);
                scanIntegrator.setOrientationLocked(false);
                scanIntegrator.initiateScan();
            }
        });

        SharedPreferences settings = this.getSharedPreferences(Constants.USER_ID_PREFERENCE_EDITOR, 0) ;
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);//get the preferences that are allowed to be given

        this.userId = settings.getString(Constants.USER_ID, userId);
        this.loggedUserCreateAlarmUrl = prefs.getString("alarm_server_address", this.loggedUserCreateAlarmUrl);

        this.photoContainerView = (ImageView)this.findViewById(R.id.photo_container);
        NewAlarmActivity.this.alarm = NewAlarmActivity.this.createAlarmFromScratch();
    }

    private boolean isValidAlarm() {
        EditText editText = ((EditText)this.findViewById(R.id.new_alarm_terminal_provider_field));
        return !editText.getText().toString().equals("");
    }
    private void DisplatTerminalPromotionElements(RadioGroup radioGroup) {

        int id=radioGroup.getCheckedRadioButtonId();
        View radioButton = radioGroup.findViewById(id);
        Spinner alarmCategory = (Spinner) this.findViewById(R.id.new_alarm_category);
        SpinnerItemCategoryAdapter categoryAdapter;
        if(radioButton.getId()==R.id.new_alarm_type_player) {
            categoryAdapter = new SpinnerItemCategoryAdapter(this, LayoutSiteItemCategory.GetSubCategories(LayoutSiteItemCategory.PLAYER));
        }else{
            categoryAdapter = new SpinnerItemCategoryAdapter(this, LayoutSiteItemCategory.GetSubCategories(LayoutSiteItemCategory.MACHINE));
        }

        alarmCategory.setAdapter(categoryAdapter);
    }



    private void buildDialog() {
        this.dialog = new ProgressDialog(NewAlarmActivity.this);
        this.dialog.setTitle(NewAlarmActivity.this.getResources().getString(R.string.title_progress_dialog));
        this.dialog.setMessage(NewAlarmActivity.this.getResources().getString(R.string.alarm_message_progress_dialog));
        this.dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                NewAlarmActivity.this.createAlarmAsynchTask.cancel(true);
                finish();
            }
        });
    }

    private Alarm createAlarmFromScratch() {
        this.alarm = new Alarm();
        Terminal terminal = buildTerminalFromForm();

        this.alarm.setTerminal(terminal);
        this.alarm.setDate(new Date());
        buildAlarmCategoryFromSpinner(alarm);
        this.alarm.setCompleted(false);
        this.alarm.setDescription(buildAlarmDescription());

        return alarm;
    }


    private String buildAlarmDescription() {
        EditText desciption = (EditText)findViewById(R.id.new_alarm_description_field);
        return desciption.getText().toString();
    }

    private void buildAlarmCategoryFromSpinner(Alarm alarm) {
        Spinner spinner = (Spinner)findViewById(R.id.new_alarm_category);
        LayoutSiteItemSubCategory selectedValue = (LayoutSiteItemSubCategory) spinner.getSelectedItem();
        alarm.setSiteItemSubCategory(selectedValue);
    }

    private Terminal buildTerminalFromForm() {
        Terminal terminal = new Terminal();

        if(this.findViewById(R.id.new_alarm_terminal_provider_field) != null){
            terminal.setProvider(((EditText) this.findViewById(R.id.new_alarm_terminal_provider_field)).getText().toString());
        }

        return terminal;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_new_alarm, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class HttpCreateAlarmTask extends AsyncTask<Alarm, Boolean, Boolean> {

        private DATA_PostActionResponse response;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            NewAlarmActivity.this.dialog.show();
        }

        @Override
        protected Boolean doInBackground(Alarm... params) {
            try {
                response = ServiceFactory.getInstance().getAlarmService(getApplicationContext()).createAlarm(NewAlarmActivity.this.alarm, NewAlarmActivity.this.cropedPhoto, NewAlarmActivity.this.userId, NewAlarmActivity.this.loggedUserCreateAlarmUrl);
                ServiceFactory.getInstance().getLayoutSiteAlarmsService(getApplicationContext()).getAlarmsCreatedByUserID(NewAlarmActivity.this.loggedUserCreateAlarmUrl, NewAlarmActivity.this.userId);
                return true;
            } catch (Exception e) {
                Log.e("NewAlarmActivity", e.getMessage(), e);
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean successfulCreate) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            if(successfulCreate){

                if(response.getResponseCode() != ResponseCode.DATA_RC_OK)
                    ToastFactory.buildSimpleToast(response.getResponseCodeDescription(), NewAlarmActivity.this);
                else{

                    if(CameraSetupHelper.deletePhotoFile(cropedPhoto) && CameraSetupHelper.deletePhotoFile(photoFromCammera))
                        Log.i("Photo deleted", "Deleted Success");
                    else
                        Log.i("Photo deleted", "Deleted failed");

                    Intent i = new Intent(NewAlarmActivity.this, CreatedAlarmsListActivity.class);
                    startActivity(i);
                }

            }
            else{
                ToastFactory.buildSimpleToast(getResources().getString(R.string.alarm_non_created_successfully), NewAlarmActivity.this);
            }
        }



    }

    public class SpinnerItemCategoryAdapter extends BaseAdapter{
        private List<LayoutSiteItemSubCategory> layoutSiteItemCategories;
        private LayoutInflater inflater = null;

        public SpinnerItemCategoryAdapter(Context context, List<LayoutSiteItemSubCategory> data) {
            this.layoutSiteItemCategories = data;
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }


        @Override
        public int getCount() {
            return layoutSiteItemCategories.size();
        }

        @Override
        public Object getItem(int position) {
            return layoutSiteItemCategories.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutSiteItemSubCategory itemCategory = layoutSiteItemCategories.get(position);

            FrameLayout imageView;
            GradientDrawable bgShape;

            convertView = inflater.inflate(R.layout.new_alarm_spinner_category_item,null);

            imageView = (FrameLayout) convertView.findViewById(R.id.list_item_icon);
            bgShape = (GradientDrawable)imageView.getBackground();
            bgShape.setColor(convertView.getResources().getColor(R.color.background_action_bar));

            ImageView alarmCategoryType = (ImageView) convertView.findViewById(R.id.alarm_category_image);
            alarmCategoryType.setImageResource(AlarmResourseHelper.getCategoryImageByType(itemCategory.getGroupCategory()));

            TextView text = (TextView) convertView.findViewById(R.id.list_item_description);
            text.setText(itemCategory.getDescription(getApplicationContext()));

            return convertView;
        }
    }
}
