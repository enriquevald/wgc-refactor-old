package com.winsystems.smartfloor.model;

import com.winsystems.smartfloor.service.login.response.LayoutSiteTaskTerminal;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.io.Serializable;
import java.util.Hashtable;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: LayoutSiteAlarm
//
// DESCRIPTION: It represents the alarm entity smart floor.
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public class LayoutSiteAlarm implements KvmSerializable, Serializable {

  private int id;
  private int subCategory;
  private int category;
  private String description;
  private String imageData;
  private int terminalId;
  private LayoutSiteTaskTerminal terminal;
  private String title;

  public int getStatusOfTask() {
    return statusOfTask;
  }

  public void setStatusOfTask(int statusOfTask) {
    this.statusOfTask = statusOfTask;
  }

  private int statusOfTask;

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getImageData() {
    return imageData;
  }

  public void setImageData(String imageData) {
    this.imageData = imageData;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public void setSubCategory(int subCategory) {
    this.subCategory = subCategory;
  }

  public void setCategory(int category) {
    this.category = category;
  }

  public void setTerminalId(int terminalId) {
    this.terminalId = terminalId;
  }

  public void setTerminal(LayoutSiteTaskTerminal terminal) {
    this.terminal = terminal;
  }

  public LayoutSiteTaskTerminal getTerminal() {
    return terminal;
  }

  public LayoutSiteItemSubCategory getSubCategory() {
    return LayoutSiteItemSubCategory.forCode(subCategory);
  }

  public int getCategory() {
    return category;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  @Override
  public Object getProperty(int i) {
    switch (i){
      case 0:
        return category;
      case 1:
        return description;
      case 2:
        return id;
      case 3:
        return imageData;
      case 4:
        return statusOfTask;
      case 5:
        return subCategory;
      case 6:
        return terminal;
      case 7:
        return terminalId;
      case 8:
        return title;
      default:
        return null;
    }
  }

  @Override
  public int getPropertyCount() {
    return 9;
  }

  @Override
  public void setProperty(int i, Object o) {
    switch (i){
      case 0:
        category = Integer.parseInt(o.toString());
        break;
      case 1:
        description = o.toString();
        break;
      case 2:
        id = Integer.parseInt(o.toString());
        break;
      case 3:
        imageData= o.toString();
        break;
      case 4:
        statusOfTask = Integer.parseInt(o.toString());
        break;
      case 5:
        subCategory = Integer.parseInt(o.toString());
        break;
      case 6:
        terminal = (LayoutSiteTaskTerminal)o;
        break;
      case 7:
        terminalId = Integer.parseInt(o.toString());
      case 8:
        title = o.toString();
        break;

    }
  }

  @Override
  public void getPropertyInfo(int i, Hashtable hashtable, PropertyInfo propertyInfo) {
    switch (i){
      case 0:
        propertyInfo.type = PropertyInfo.INTEGER_CLASS;
        propertyInfo.name = "Category";
        break;
      case 1:
        propertyInfo.type = PropertyInfo.STRING_CLASS;
        propertyInfo.name = "Description";
        break;
      case 2:
        propertyInfo.type = PropertyInfo.INTEGER_CLASS;
        propertyInfo.name = "Id";
        break;
      case 3:
        propertyInfo.type = PropertyInfo.INTEGER_CLASS;
        propertyInfo.name = "ImageData";
        break;
      case 4:
        propertyInfo.type = PropertyInfo.INTEGER_CLASS;
        propertyInfo.name = "StatusOfTask";
        break;
      case 5:
        propertyInfo.type = PropertyInfo.INTEGER_CLASS;
        propertyInfo.name = "SubCategory";
        break;
      case 6:
        propertyInfo.type = PropertyInfo.OBJECT_CLASS;
        propertyInfo.name = "Terminal";
        break;
      case 7:
        propertyInfo.type = PropertyInfo.INTEGER_CLASS;
        propertyInfo.name = "TerminalId";
        break;
      case 8:
        propertyInfo.type = PropertyInfo.STRING_CLASS;
        propertyInfo.name = "Title";
        break;

    }
  }
}
