package com.winsystems.smartfloor.service.kiosk;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.winsystems.smartfloor.view.menu.MenuActivity;
import com.winsystems.smartfloor.view.utils.SplashActivity;

public class BootReceiver extends BroadcastReceiver {

  @Override
  public void onReceive(Context context, Intent intent) {
    if (intent.getAction().equals(Intent.ACTION_PACKAGE_ADDED)) {
      Intent serviceIntent = new Intent();
      serviceIntent.setClass(context,MenuActivity.class);
      serviceIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
      context.startActivity(serviceIntent);
    } else if (intent.getAction().equals(Intent.ACTION_PACKAGE_REPLACED)) {
      Intent serviceIntent = new Intent();
      serviceIntent.setClass(context, MenuActivity.class);
      serviceIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
      context.startActivity(serviceIntent);
    }else{
      Intent myIntent = new Intent(context, SplashActivity.class);
      myIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
      context.startActivity(myIntent);
    }
  }
}
