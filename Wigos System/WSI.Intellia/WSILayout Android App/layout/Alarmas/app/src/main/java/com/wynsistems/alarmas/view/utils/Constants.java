package com.wynsistems.alarmas.view.utils;

public class Constants {

    public static final String SELECTED_ALARM = "SelectedAlarm";
    public static final String SELECTED_GROUP_ALARMS ="SelectedGroupAlarms";
    public static final String TERMINAL_DETAILS = "TerminalDetails";

    public static final String SOLVED_ALARM = "Alarma resuelta";
    public static final String SCALED_ALARM = "Alarma escalada";

    public static final String USER_ID_PREFERENCE_EDITOR = "UserId";
    public static final String USER_ID = "UserId";
    public static final String DOWNLOAD_ALARMS = "Download_alarms";
    public static final String ALARM_ID = "ALARM_ID";
    public static final String LOGGED_USER_ALARM_SERVICE_ADDRESS = "LoggedUserAlarmServiceAddress";

    public static final String LOGGED_USER_CREATED_ALARM_SERVICE_ADDRESS = "LoggedUserCreatedAlarmServiceAddress";
    public static final String TERMINAL_ID = "terminalId";
    public static final String TERMINAL_NAME = "terminalName";
    public static final String UPDATED_TASKS = "UpdatedAlarms";

    public static final String ALARMS_CREATED_BY_USER = "_layoutSiteAlarmsOfDay";
    public static final String ROLES_OF_USER = "_rolesOfUser";
    public static final String TERMINAL_LOGS = "_terminal_logs";
    public static final String BEACONS_AVAILABLES = "_beacons_availables";
    public static final String MANAGER_ID = "managerId" ;
    public static final String TODAY_MESSAGES = "_messagesOfDay";
    public static final String MANAGER_USERNAME = "managerUserName";
    public static final String TASKS_ASSIGNED = "assignedTasks";
    public static final String FROM_NOTIFICATION = "fromNotifications";
    public static final String LOG = "log";
    public static final int APP_INSTALL_REQUEST = 1305;
    public static final String MANAGERS_AVAILABLES = "managersAvailables";
    public static final String PENDING_MESSAGES = "pendingMessages";
    public static final String NOTIFICATION_MANAGER_NOTIFICATION_LIST = "notificationManagerNotificationList";
    public static final String NOTIFICATION_TYPE = "NOTIFICATION_TYPE";
    public static final String NOTIFY_NEW_ICON_MENU_NOTIFICATION = "new_icon_menu_notification";
    public static final String NOTIFICATION_ID = "notification_id";
}
