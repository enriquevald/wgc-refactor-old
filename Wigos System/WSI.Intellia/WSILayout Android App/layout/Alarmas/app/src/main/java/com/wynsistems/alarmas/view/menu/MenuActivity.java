package com.wynsistems.alarmas.view.menu;

import android.app.AlertDialog;
import android.app.Notification;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.wynsistems.alarmas.R;
import com.wynsistems.alarmas.common.AndroidLogger;
import com.wynsistems.alarmas.common.LayoutNotification;
import com.wynsistems.alarmas.common.LayoutNotificationManager;
import com.wynsistems.alarmas.common.LayoutNotificationType;
import com.wynsistems.alarmas.helpers.LogHelper;
import com.wynsistems.alarmas.helpers.SaveInformationHelper;
import com.wynsistems.alarmas.service.mqtt.PahoMqttService;
import com.wynsistems.alarmas.view.alarms.CreatedAlarmsListActivity;
import com.wynsistems.alarmas.view.message.MessageActivity;
import com.wynsistems.alarmas.view.notifications.ListNotificationsActivity;
import com.wynsistems.alarmas.view.setting.SettingsActivity;
import com.wynsistems.alarmas.view.task.TaskListActivity;
import com.wynsistems.alarmas.view.terminal.TerminalInfoActivity;
import com.wynsistems.alarmas.view.update.UpdateApplicationActivity;
import com.wynsistems.alarmas.helpers.IsLastestVersionHelper;
import com.wynsistems.alarmas.view.utils.Constants;
import com.wynsistems.alarmas.view.utils.KioskModeActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MenuActivity extends KioskModeActivity implements SharedPreferences.OnSharedPreferenceChangeListener {

    private SharedPreferences prefs;
    private String language;
    private String preferenceBrokerHost = "";
    private String userId = "";
    private static TextView notifCountMessage;
    private static TextView notifCountTasks;
    private static int mNotifCountMessage = 0;
    private static int mNotifCountTask = 0;
    private Menu iconsMenu;
    private IntentFilter intentFilter = new IntentFilter(Constants.NOTIFY_NEW_ICON_MENU_NOTIFICATION);
    private LayoutNotificationManager notificationManager;


    private BroadcastReceiver updateIconMenuBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    refreshIconsMenu(iconsMenu);
                }
            });
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        setTitle(R.string.title_activity_menu);

        prefs = PreferenceManager.getDefaultSharedPreferences(this);//get the preferences that are allowed to be given
        prefs.registerOnSharedPreferenceChangeListener(this);//set the listener to listen for changes in the preferences
        language = prefs.getString("languageToLoad", Locale.getDefault().getDisplayLanguage());
        notificationManager = new LayoutNotificationManager(getApplicationContext());

        Thread t = new Thread(){
            public void run(){
                StarMQTTService();
            }
        };
        t.start();
        //StarMQTTService();


        GridView menuListView = (GridView) findViewById( R.id.list_menu);
        List<com.wynsistems.alarmas.view.menu.MenuItem> menuItemList = this.buildMenuList();
        final ListAdapter listAdapter = new MenuListAdapter(this, menuItemList);
        menuListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                com.wynsistems.alarmas.view.menu.MenuItem item = (com.wynsistems.alarmas.view.menu.MenuItem) listAdapter.getItem(position);
                Intent intent = new Intent(MenuActivity.this, item.getTargetClass());
                startActivity(intent);
            }

        });
        menuListView.setAdapter(listAdapter);
        registerReceiver(updateIconMenuBroadcastReceiver, intentFilter);
    }
    @Override
    public void onStart(){
        super.onStart();

        if(!IsLastestVersionHelper.IsLastestVersion(getApplicationContext()))
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Are you sure you want to exit?")
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Intent updateIntent = new Intent(MenuActivity.this, UpdateApplicationActivity.class);
                            startActivity(updateIntent);
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.setMessage(getString(R.string.new_version_available_text));
            alert.setTitle(getString(R.string.new_version_available));
            alert.setIcon(R.drawable.ic_system_update_alt_black_18dp);
            alert.show();
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        String oldLanguage = language;
        language = prefs.getString("languageToLoad", Locale.getDefault().getDisplayLanguage());
        if (!oldLanguage.equals(language)){
            finish();
            startActivity(getIntent());
        }

        registerReceiver(updateIconMenuBroadcastReceiver, intentFilter);
        refreshIconsMenu(iconsMenu);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(updateIconMenuBroadcastReceiver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        iconsMenu = menu;
        refreshIconsMenu(iconsMenu);

        return super.onCreateOptionsMenu(iconsMenu);
    }

    private void refreshIconsMenu(Menu menu) {
        if(iconsMenu != null) {
            iconsMenu.clear();
            MenuInflater inflater = this.getMenuInflater();
            inflater.inflate(R.menu.menu_menu, iconsMenu);

            MenuItem message_icon = menu.findItem(R.id.message_bar_icon);
            MenuItemCompat.setActionView(message_icon, R.layout.message_update_count);
            View count = message_icon.getActionView();
            notifCountMessage = (TextView) count.findViewById(R.id.notif_count);
            setNotifCountMessage();
            if (mNotifCountMessage > 0) {
                notifCountMessage.setVisibility(View.VISIBLE);
                count.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(MenuActivity.this, ListNotificationsActivity.class);
                        intent.putExtra(Constants.NOTIFICATION_TYPE, LayoutNotificationType.MESSAGE);
                        startActivity(intent);
                    }
                });
            } else {
                notifCountMessage.setVisibility(View.GONE);
            }

            MenuItem tasks_icon = menu.findItem(R.id.tasks_bar_icon);
            MenuItemCompat.setActionView(tasks_icon, R.layout.tasks_update_count);
            View countTasks = tasks_icon.getActionView();
            notifCountTasks = (TextView) countTasks.findViewById(R.id.notif_count_tasks);
            setNotifCountTask();
            if(mNotifCountTask > 0) {
                notifCountTasks.setVisibility(View.VISIBLE);
                countTasks.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(MenuActivity.this, ListNotificationsActivity.class);
                        intent.putExtra(Constants.NOTIFICATION_TYPE, LayoutNotificationType.TASK);
                        startActivity(intent);
                    }
                });
            }else{
                notifCountTasks.setVisibility(View.GONE);
            }
        }

    }

    private void setNotifCountMessage(){
        mNotifCountMessage = readUnReadNotifications(LayoutNotificationType.MESSAGE);
        notifCountMessage.setText(String.valueOf(mNotifCountMessage));
        invalidateOptionsMenu();
    }

    private void setNotifCountTask(){
        mNotifCountTask = readUnReadNotifications(LayoutNotificationType.TASK);
        notifCountTasks.setText(String.valueOf(mNotifCountTask));
        invalidateOptionsMenu();
    }

    private int readUnReadNotifications(LayoutNotificationType type){
        return notificationManager.getCountNotificationsByType(type);
    }

    public void StarMQTTService() {
        String mDeviceId = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        //Setting brokerHost from preferences
        preferenceBrokerHost = prefs.getString("alarm_server_address", preferenceBrokerHost);
        userId = prefs.getString("userId", "");
        //Adding to the preferences the clientId
        SharedPreferences.Editor editorClientId = prefs.edit();
        editorClientId.putString("clientId", mDeviceId);
        editorClientId.apply();

        SharedPreferences settings = getSharedPreferences(PahoMqttService.APP_ID, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("broker", preferenceBrokerHost);
        editor.putString("topic", "/Task/Notifications/" + userId + "/+");
        editor.putString("message-topic", "/Messages/" + userId + "/+");
        editor.apply();
        PahoMqttService.actionStart(this);
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_menu, menu);
        return true;
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                return true;
            case R.id.action_settings:
                Intent i = new Intent(this, SettingsActivity.class);
                startActivityForResult(i, 1);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }




    private List<com.wynsistems.alarmas.view.menu.MenuItem> buildMenuList() {
        List<com.wynsistems.alarmas.view.menu.MenuItem> menuItemList = new ArrayList<>();

        //menuItemList.add(new com.wynsistems.alarmas.view.menu.MenuItem(R.drawable.ic_camera_alt_white_48dp, getResources().getString(R.string.photo_menu_item), PhotoActivity.class));
        menuItemList.add(new com.wynsistems.alarmas.view.menu.MenuItem(R.drawable.ic_settings_white_48dp, getResources().getString(R.string.action_settings), SettingsActivity.class));
        menuItemList.add(new com.wynsistems.alarmas.view.menu.MenuItem(R.drawable.ic_list_white_48dp, getResources().getString(R.string.task_menu_item), TaskListActivity.class));
        menuItemList.add(new com.wynsistems.alarmas.view.menu.MenuItem(R.drawable.ic_add_alert_white_48dp, getResources().getString(R.string.alarm_menu_item), CreatedAlarmsListActivity.class));
        menuItemList.add(new com.wynsistems.alarmas.view.menu.MenuItem(R.drawable.machineflags, getResources().getString(R.string.title_activity_terminal_status), TerminalInfoActivity.class));
        menuItemList.add(new com.wynsistems.alarmas.view.menu.MenuItem(R.drawable.ic_question_answer_white_48dp, getResources().getString(R.string.message_title), MessageActivity.class));
        return menuItemList;
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Intent svc = new Intent(this, PahoMqttService.class);
        stopService(svc);

        StarMQTTService();
    }

    private void setToolbar() {
        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            // Poner ícono del drawer toggle
            ab.setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);
            ab.setDisplayHomeAsUpEnabled(true);
        }

    }


}
