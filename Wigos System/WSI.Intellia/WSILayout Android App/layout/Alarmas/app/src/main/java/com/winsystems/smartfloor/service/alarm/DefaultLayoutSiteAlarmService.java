package com.winsystems.smartfloor.service.alarm;

import android.content.Context;
import android.util.Log;

import com.winsystems.smartfloor.R;
import com.winsystems.smartfloor.common.AndroidLogger;
import com.winsystems.smartfloor.common.ServerEntitiesBuilder;
import com.winsystems.smartfloor.common.TrustManagerManipulator;
import com.winsystems.smartfloor.helpers.LogHelper;
import com.winsystems.smartfloor.helpers.SaveInformationHelper;
import com.winsystems.smartfloor.model.LayoutSiteAlarm;
import com.winsystems.smartfloor.service.ResponseCode;
import com.winsystems.smartfloor.service.alarm.request.DATA_TerminalInfo;
import com.winsystems.smartfloor.service.alarm.response.DATA_TerminalInfoResponse;
import com.winsystems.smartfloor.view.utils.Constants;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.net.ConnectException;
import java.net.SocketTimeoutException;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: DefaultLayoutSiteAlarmService
//
// DESCRIPTION: Implement methods for terminal operation, example: get alarm created, get terminal status, etc.
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public class DefaultLayoutSiteAlarmService implements LayoutSiteAlarmsService {

  private static DefaultLayoutSiteAlarmService instance = null;
  private Context appContext;
  private SaveInformationHelper<LayoutSiteAlarm> saveLayoutSiteAlarms;

  private AndroidLogger mLog;
  private DefaultLayoutSiteAlarmService(Context context){
    mLog = LogHelper.getLogger(context);
    saveLayoutSiteAlarms = new SaveInformationHelper<>(context);
    appContext = context;
  }

  @Override
  public DATA_TerminalInfoResponse getTerminalStatusByTerminalID(String url, String sessionId, String terminalId) {
    final String NAMESPACE = "http://winsystemsintl.com/";
    final String METHOD_NAME = "GetTerminalInfoByTerminalId";
    final String SOAP_ACTION = "http://winsystemsintl.com/IDATAService/GetTerminalInfoByTerminalId";

    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);

    DATA_TerminalInfo terminalInfoRequest = new DATA_TerminalInfo();
    terminalInfoRequest.setSessionId(sessionId);
    terminalInfoRequest.setTerminalId(terminalId);

    DATA_TerminalInfoResponse terminalInfoResponse = new DATA_TerminalInfoResponse();

    PropertyInfo pi = new PropertyInfo();
    pi.setName("terminalInfo");
    pi.setValue(terminalInfoRequest);
    pi.setType(terminalInfoRequest.getClass());

    request.addProperty(pi);

    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
    envelope.dotNet = true;

    envelope.setOutputSoapObject(request);

    envelope.addMapping(NAMESPACE, "DATA_TerminalInfo", terminalInfoRequest.getClass());
    envelope.addMapping(NAMESPACE, "DATA_TerminalInfoResponse", terminalInfoResponse.getClass());

    HttpTransportSE transporte = new HttpTransportSE(url, 60000);

    try {
      TrustManagerManipulator.allowAllSSL();
      transporte.call(SOAP_ACTION, envelope);

      SoapObject resSoap = (SoapObject) envelope.getResponse();

      terminalInfoResponse.setResponseCode(Integer.parseInt(resSoap.getProperty(0).toString()));
      terminalInfoResponse.setResponseCodeDescription(resSoap.getProperty(1).toString());
      terminalInfoResponse.setSessionId(resSoap.getProperty(2).toString());
      if(resSoap.getProperty(3) != null)
        terminalInfoResponse.setAlarms(ServerEntitiesBuilder.BuildAlarmsListFromServerResponse((SoapObject) resSoap.getProperty(3)));

    }catch (ConnectException c){
      mLog.error("", c);
      terminalInfoResponse.setResponseCode(9);
      terminalInfoResponse.setResponseCodeDescription(appContext.getString(R.string.error_connection));
    }
    catch (SocketTimeoutException e)
    {
      mLog.error("", e);
      terminalInfoResponse.setResponseCode(9);
      terminalInfoResponse.setResponseCodeDescription(appContext.getString(R.string.error_timeout));

      return terminalInfoResponse;
    }
    catch (Exception e)
    {
      mLog.error("", e);
      terminalInfoResponse.setResponseCode(9);
      terminalInfoResponse.setResponseCodeDescription(appContext.getString(R.string.error_unknown));

      return terminalInfoResponse;
    }

    if (terminalInfoResponse.getResponseCode() == ResponseCode.DATA_RC_OK)
    {
      saveLayoutSiteAlarms.writeList(terminalInfoResponse.getAlarms(), sessionId + "_layoutSiteAlarms");
      //saveLogs.writeList(terminalInfoResponse.getLog(), sessionId + "_terminalLogs");
      return terminalInfoResponse;
    }else
      return terminalInfoResponse;

  }

  @Override
  public DATA_TerminalInfoResponse getAlarmsCreatedByUserID(String url, String sessionId) {
    final String NAMESPACE = "http://winsystemsintl.com/";
    final String METHOD_NAME = "GetAlarmsCreatedByUser";
    final String SOAP_ACTION = "http://winsystemsintl.com/IDATAService/GetAlarmsCreatedByUser";

    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);

    DATA_TerminalInfo terminalInfoRequest = new DATA_TerminalInfo();
    terminalInfoRequest.setSessionId(sessionId);

    DATA_TerminalInfoResponse terminalInfoResponse = new DATA_TerminalInfoResponse();

    PropertyInfo pi = new PropertyInfo();
    pi.setName("terminalInfo");
    pi.setValue(terminalInfoRequest);
    pi.setType(terminalInfoRequest.getClass());

    request.addProperty(pi);

    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
    envelope.dotNet = true;

    envelope.setOutputSoapObject(request);

    envelope.addMapping(NAMESPACE, "DATA_TerminalInfo", terminalInfoRequest.getClass());
    envelope.addMapping(NAMESPACE, "DATA_TerminalInfoResponse", terminalInfoResponse.getClass());

    HttpTransportSE transporte = new HttpTransportSE(url, 60000);

    try {
      TrustManagerManipulator.allowAllSSL();
      transporte.call(SOAP_ACTION, envelope);

      SoapObject resSoap = (SoapObject) envelope.getResponse();

      terminalInfoResponse.setResponseCode(Integer.parseInt(resSoap.getProperty(0).toString()));
      terminalInfoResponse.setResponseCodeDescription(resSoap.getProperty(1).toString());
      terminalInfoResponse.setSessionId(resSoap.getProperty(2).toString());

      SoapObject tasks = (SoapObject) resSoap.getProperty(3);
      terminalInfoResponse.setAlarms(ServerEntitiesBuilder.BuildAlarmsListFromServerResponse(tasks));
    }catch (ConnectException c){
      mLog.error("", c);
      terminalInfoResponse.setResponseCode(9);
      terminalInfoResponse.setResponseCodeDescription(appContext.getString(R.string.error_connection));
    }
    catch (SocketTimeoutException e)
    {
      mLog.error("", e);
      terminalInfoResponse.setResponseCode(9);
      terminalInfoResponse.setResponseCodeDescription(appContext.getString(R.string.error_timeout));

      return terminalInfoResponse;
    }
    catch (Exception e)
    {
      mLog.error("", e);
      terminalInfoResponse.setResponseCode(9);
      terminalInfoResponse.setResponseCodeDescription(appContext.getString(R.string.error_unknown));

      return terminalInfoResponse;
    }

    if (terminalInfoResponse.getResponseCode() == ResponseCode.DATA_RC_OK)
    {
      saveLayoutSiteAlarms.writeList(terminalInfoResponse.getAlarms(), sessionId + Constants.ALARMS_CREATED_BY_USER);
      return terminalInfoResponse;
    }else
      return null;
  }

  public static LayoutSiteAlarmsService getInstance(Context context) {
    if(instance == null){
      instance = new DefaultLayoutSiteAlarmService(context);
    }
    return instance;
  }
}
