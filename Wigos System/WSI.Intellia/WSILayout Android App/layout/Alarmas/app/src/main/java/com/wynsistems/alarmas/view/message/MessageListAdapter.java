package com.wynsistems.alarmas.view.message;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.wynsistems.alarmas.R;
import com.wynsistems.alarmas.model.LayoutSiteManagerRunnerMessage;
import com.wynsistems.alarmas.model.LayoutSiteManagerRunnerMessageSource;

import java.util.List;

public class MessageListAdapter extends BaseAdapter{

    private List<LayoutSiteManagerRunnerMessage> messages;
    private Context context;
    private static LayoutInflater inflater;
    public MessageListAdapter(Context applicationContext, List<LayoutSiteManagerRunnerMessage> messageList) {
        context = applicationContext;
        messages = messageList;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setListData(List<LayoutSiteManagerRunnerMessage> data){
        messages = data;
    }


    @Override
    public int getCount() {
        return this.messages.size();
    }

    @Override
    public Object getItem(int position) {
        return this.messages.get(position);
    }

    @Override
    public long getItemId(int position) {
        return this.messages.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null)
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        LayoutSiteManagerRunnerMessage message = (LayoutSiteManagerRunnerMessage)getItem(position);
        if(message.getSource() == LayoutSiteManagerRunnerMessageSource.Manager.getValue()){
            convertView = inflater.inflate(R.layout.message_bubble_item_manager, parent, false);

            TextView messageContent = (TextView)convertView.findViewById(R.id.message_content);
            TextView messageOwner = (TextView)convertView.findViewById(R.id.message_owner);
            FrameLayout messageBubble = (FrameLayout) convertView.findViewById(R.id.message_bubble);
            GradientDrawable bgShapeBubble = (GradientDrawable)messageBubble.getBackground();
            FrameLayout messageArrow = (FrameLayout) convertView.findViewById(R.id.message_bubble);
            GradientDrawable bgShapeArrow = (GradientDrawable)messageArrow.getBackground();

            messageOwner.setText(message.getManagerUserName());
            messageContent.setText(message.getMessage());
            bgShapeBubble.setColor(convertView.getResources().getColor(R.color.manager_color));
            bgShapeArrow.setColor(convertView.getResources().getColor(R.color.manager_color));
        }else{
            convertView = inflater.inflate(R.layout.message_bubble_item_runner, parent, false);
            TextView messageContent = (TextView)convertView.findViewById(R.id.message_content);
            TextView messageOwner = (TextView)convertView.findViewById(R.id.message_owner);
            FrameLayout messageBubble = (FrameLayout) convertView.findViewById(R.id.message_bubble);
            GradientDrawable bgShapeBubble = (GradientDrawable)messageBubble.getBackground();
            FrameLayout messageArrow = (FrameLayout) convertView.findViewById(R.id.message_bubble);
            GradientDrawable bgShapeArrow = (GradientDrawable)messageArrow.getBackground();

            messageOwner.setText(message.getRunnerUserName());
            messageContent.setText(message.getMessage());
            bgShapeBubble.setColor(convertView.getResources().getColor(R.color.runner_color));
            bgShapeArrow.setColor(convertView.getResources().getColor(R.color.runner_color));
        }
        return convertView;
    }
}
