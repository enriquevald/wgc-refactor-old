package com.winsystems.smartfloor.view.menu;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: NavigationItem
//
// DESCRIPTION: Implements methods that correspond to the navigation item menu of the application.
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public class NavigationItem {
  private int title;
  private int image;
  private int count;

  public int getTitle() {
    return title;
  }

  public int getImage() {
    return image;
  }

  public int getCount() {
    return count;
  }
}
