package com.winsystems.smartfloor.service.log;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.provider.Settings;

import com.winsystems.smartfloor.R;
import com.winsystems.smartfloor.common.AndroidLogger;
import com.winsystems.smartfloor.common.ServerEntitiesBuilder;
import com.winsystems.smartfloor.common.TrustManagerManipulator;
import com.winsystems.smartfloor.helpers.LogHelper;
import com.winsystems.smartfloor.service.ResponseCode;
import com.winsystems.smartfloor.service.log.request.DATA_SaveLogRequest;
import com.winsystems.smartfloor.service.log.response.DATA_SaveLogResponse;
import com.winsystems.smartfloor.view.utils.Constants;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalDate;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.Date;

/**
 * Created by maxim on 2/3/2016.
 */
public class DefaultLogService implements LogService {


    private static DefaultLogService instance = null;
    private static Context appContext;
    private static AndroidLogger mLog;

    public static LogService getInstance(Context context) {
        mLog = LogHelper.getLogger(context);
        if(instance == null){
            instance = new DefaultLogService();
            appContext = context;
        }
        return instance;
    }

    @Override
    public DATA_SaveLogResponse saveLog() {

        final String NAMESPACE = "http://winsystemsintl.com/";
        final String METHOD_NAME = "SaveLog";
        final String SOAP_ACTION = "http://winsystemsintl.com/IDATAService/SaveLog";

        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);

        SharedPreferences userSettings = appContext.getSharedPreferences(Constants.USER_ID_PREFERENCE_EDITOR, 0) ;
        String sessionId = "";
        sessionId = userSettings.getString(Constants.USER_ID,sessionId);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(appContext);

        String deviceId =  Settings.Secure.getString(appContext.getContentResolver(), Settings.Secure.ANDROID_ID);
        String userId = prefs.getString("userId", "");
        String url = prefs.getString("alarm_server_address", "");
        String source = "LAYOUT";
        String content = mLog.content();
        Date date = new Date();

        DATA_SaveLogRequest saveLogRequest = new DATA_SaveLogRequest();

        saveLogRequest.setDatetime(date);
        saveLogRequest.setSource(source);
        saveLogRequest.setDeviceId(deviceId);
        saveLogRequest.setUserId(userId);
        saveLogRequest.setContent(content);
        saveLogRequest.setSessionId(sessionId);


        DATA_SaveLogResponse saveLogResponse = new DATA_SaveLogResponse();

        PropertyInfo pi = new PropertyInfo();
        pi.setName("request");
        pi.setValue(saveLogRequest);
        pi.setType(saveLogRequest.getClass());

        request.addProperty(pi);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;

        envelope.setOutputSoapObject(request);

        envelope.addMapping(NAMESPACE, "DATA_SaveLogRequest", saveLogRequest.getClass());
        envelope.addMapping(NAMESPACE, "DATA_SaveLogResponse", saveLogResponse.getClass());

        new MarshalDate().register(envelope);

        HttpTransportSE transporte = new HttpTransportSE(url, 60000);

        try {
            TrustManagerManipulator.allowAllSSL();
            transporte.call(SOAP_ACTION, envelope);

            SoapObject resSoap = (SoapObject) envelope.getResponse();

            saveLogResponse.setResponseCode(Integer.parseInt(resSoap.getProperty(0).toString()));
            saveLogResponse.setResponseCodeDescription(resSoap.getProperty(1).toString());
            saveLogResponse.setSessionId(resSoap.getProperty(2).toString());
            if(saveLogResponse.getResponseCode() == ResponseCode.DATA_RC_OK);
                //No actions by now
        }catch (ConnectException e)
        {
            mLog.error("", e);
            saveLogResponse.setResponseCode(9);
            saveLogResponse.setResponseCodeDescription(appContext.getString(R.string.error_connection));
        }
        catch (SocketTimeoutException e)
        {
            mLog.error("", e);
            saveLogResponse.setResponseCode(9);
            saveLogResponse.setResponseCodeDescription(appContext.getString(R.string.error_timeout));

            return saveLogResponse;
        }
        catch (Exception e)
        {
            mLog.error("", e);
            saveLogResponse.setResponseCode(9);
            saveLogResponse.setResponseCodeDescription(appContext.getString(R.string.error_unknown));

            return saveLogResponse;
        }

        return saveLogResponse;

    }
}
