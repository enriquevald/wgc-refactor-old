package com.wynsistems.alarmas.model;

import java.io.Serializable;

public class Terminal implements Serializable {
    private String id;
    private String provider;
    private String model;
    private String area;
    private String bnk;
    private String island;
    private String hall;

    public Terminal(){}

    public Terminal(String id, String provider, String model, String area, String bnk, String island, String hall) {
        this.id = id;
        this.provider = provider;
        this.model = model;
        this.area = area;
        this.bnk = bnk;
        this.island = island;
        this.hall = hall;
    }

    public Terminal(String id, String provider) {
        this.id = id;
        this.provider = provider;
    }

    public String getId() {
        return id;
    }

    public void setProvider(String provider){
        this.provider = provider;
    }

    public String getProvider() {
        return provider;
    }

    public void setModel(String model){

        this.model = model;
    }

    public String getModel() {
        return model;
    }

    public void setArea(String area){
        this.area = area;
    }

    public String getArea() {
        return area;
    }

    public void setBnk(String bnk){
        this.bnk = bnk;
    }

    public String getBnk() {
        return bnk;
    }

    public void setIsland(String island){
        this.island = island;
    }

    public String getIsland() {
        return island;
    }

    public void setHall(String hall){
        this.hall = hall;
    }

    public String getHall() {
        return hall;
    }
}
