package com.wynsistems.alarmas.model;

public enum LayoutSiteItemSeverity {
    LOW(1),
    MEDIUM(2),
    HIGH(3);

    private int value;

    public int getValue()
    {
        return value;
    }

    LayoutSiteItemSeverity(int i) {
        value = i;
    }

    public static LayoutSiteItemSeverity getById(int id){
        switch (id){
            case 1:
                return LOW;
            case 2:
                return MEDIUM;
            case 3:
                return HIGH;
            default:
                return LOW;
        }
    }
}
