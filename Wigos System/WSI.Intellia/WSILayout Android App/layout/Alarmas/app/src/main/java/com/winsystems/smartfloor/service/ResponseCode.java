package com.winsystems.smartfloor.service;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: ResponseCode
//
// DESCRIPTION: Numbering implements methods to identify response codes.
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public enum ResponseCode {
  DATA_RC_OK,
  DATA_RC_SESSION_TIMEOUT,
  DATA_RC_BAD_SEQID,
  DATA_RC_BAD_MESSAGE_TYPE,
  DATA_RC_BAD_CHECKSUM,
  DATA_RC_BAD_MESSAGE_DATA,
  DATA_RC_INCOHERENCE_WITH_DB_DATA,
  DATA_RC_INTERNAL_DATA_ERROR,
  DATA_RC_TIMEOUT_EXCEPTION,
  DATA_RC_BAD_LOGIN,
  DATA_RC_TOO_MANY_RETRYS,
  DATA_RC_PERIOD_EXPIRED,
  DATA_RC_GETTING_TASK;

  public static ResponseCode getResponseCode(int index){
    switch (index)
    {
      case 0:
        return DATA_RC_OK;
      case 1:
        return DATA_RC_SESSION_TIMEOUT;
      case 2:
        return DATA_RC_BAD_SEQID;
      case 3:
        return DATA_RC_BAD_MESSAGE_TYPE;
      case 4:
        return DATA_RC_BAD_CHECKSUM;
      case 5:
        return DATA_RC_BAD_MESSAGE_DATA;
      case 6:
        return DATA_RC_INCOHERENCE_WITH_DB_DATA;
      case 7:
        return DATA_RC_INTERNAL_DATA_ERROR;
      case 8:
        return DATA_RC_TIMEOUT_EXCEPTION;
      case 9:
        return DATA_RC_BAD_LOGIN;
      case 10:
        return DATA_RC_TOO_MANY_RETRYS;
      case 11:
        return DATA_RC_PERIOD_EXPIRED;
      default:
        return DATA_RC_GETTING_TASK;
    }
  }
}
