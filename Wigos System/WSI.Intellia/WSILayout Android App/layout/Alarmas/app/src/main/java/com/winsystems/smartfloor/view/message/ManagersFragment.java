package com.winsystems.smartfloor.view.message;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.winsystems.smartfloor.R;
import com.winsystems.smartfloor.helpers.SaveInformationHelper;
import com.winsystems.smartfloor.model.LayoutSiteManagerAvailable;
import com.winsystems.smartfloor.view.utils.Constants;

import static com.google.android.gms.internal.zzhl.runOnUiThread;


public class ManagersFragment extends Fragment {

  private SaveInformationHelper<LayoutSiteManagerAvailable> saveInformationHelper;
  private String userId;
  private ListAdapter listAdapter;
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {

    FragmentActivity activity = this.getActivity();
    final Context context = activity.getApplicationContext();
    saveInformationHelper = new SaveInformationHelper<>(context);

    SharedPreferences settings = context.getSharedPreferences(Constants.USER_ID_PREFERENCE_EDITOR, 0) ;
    userId = settings.getString(Constants.USER_ID, userId);

    final View rootView = inflater.inflate(R.layout.manager_available_list, container, false);
    runOnUiThread(new Runnable() {
      @Override
      public void run() {
        ListView managersList = (ListView) rootView.findViewById(R.id.managersList);
        listAdapter = new ManagersAvailablesListAdapter(context, saveInformationHelper.readList(Constants.MANAGERS_AVAILABLES));
        managersList.setAdapter(listAdapter);

        managersList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
          @Override
          public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            LayoutSiteManagerAvailable item = (LayoutSiteManagerAvailable) listAdapter.getItem(position);

            Intent intent = new Intent(getActivity(), ConversationActivity.class);
            intent.putExtra(Constants.MANAGER_ID, item.getId().toString());
            intent.putExtra(Constants.MANAGER_USERNAME, item.getUserName());
            startActivity(intent);
          }
        });
      }
    });


    return rootView;
  }
}
