package com.winsystems.smartfloor.view.menu;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.winsystems.smartfloor.R;

import java.util.List;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: MenuListAdapter
//
// DESCRIPTION: Implements methods that correspond to the menu list of the application.
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public class MenuListAdapter extends BaseAdapter {

  private final List<MenuItem> menuItemList;
  private final LayoutInflater inflater;
  private final Context _context;

  public MenuListAdapter(Context context, List<MenuItem> menuItemList) {
    this.menuItemList = menuItemList;
    _context = context;
    inflater = (LayoutInflater) context
        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
  }

  @Override
  public int getCount() {
    return menuItemList.size();
  }

  @Override
  public Object getItem(int position) {
    return menuItemList.get(position);
  }

  @Override
  public long getItemId(int position) {
    return position;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    View vi = convertView;
    if (vi == null)
      vi = inflater.inflate(R.layout.menu_item, null);

    MenuItem menuItem = (MenuItem) this.getItem(position);
    TextView menuDescription = (TextView)vi.findViewById(R.id.menu_item_description);
    FrameLayout frameLayout = (FrameLayout) vi.findViewById(R.id.menu_item_icon);

    String menuText = menuItem.getDescription();
    menuDescription.setText(menuText);
    frameLayout.setBackgroundResource(menuItem.getUrlDrawable());

    return vi;
  }
}
