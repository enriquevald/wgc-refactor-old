package com.winsystems.smartfloor.view.log;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.winsystems.smartfloor.R;

import java.util.List;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: LogListAdapter
//
// DESCRIPTION: Implements list of log.
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public class LogListAdapter extends BaseAdapter {
    private Context _context;
    LayoutInflater inflater;
    private List<String> _logs;


    public LogListAdapter(Context context, List<String> logs) {
        _context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        _logs = logs;
    }

    @Override
    public int getCount() {
        return _logs.size();
    }

    @Override
    public Object getItem(int position) {
        return _logs.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.log_list_item, null);

        TextView logText = (TextView) convertView.findViewById(R.id.logText);
//        logText.setTextColor(Resources.getSystem().getColor(R.color.abc_background_cache_hint_selector_material_dark));
        String message = _logs.get(position);
//        if (!message.isEmpty()){
           if (message.contains("ERROR")) {
               logText.setTextColor(convertView.getResources().getColor(R.color.winsystem_red));
           }else{
               logText.setTextColor(convertView.getResources().getColor(R.color.winsystem_black));
           }
            logText.setText(message);
//        }
        return convertView;
    }
}
