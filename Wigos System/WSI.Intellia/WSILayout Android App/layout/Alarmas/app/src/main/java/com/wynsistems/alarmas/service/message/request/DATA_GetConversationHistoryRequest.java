package com.wynsistems.alarmas.service.message.request;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.util.Hashtable;

public class DATA_GetConversationHistoryRequest implements KvmSerializable{

    private String session;
    private Long managerId;

    public void setSession(String session) {
        this.session = session;
    }

    public void setManagerId(Long managerId) {
        this.managerId = managerId;
    }

    @Override
    public Object getProperty(int i) {
        switch (i){
            case 0:
                return managerId;
            case 1:
                return session;
            default:
                return null;
        }
    }

    @Override
    public int getPropertyCount() {
        return 2;
    }

    @Override
    public void setProperty(int i, Object o) {

        switch (i){
            case 0:
                managerId = Long.parseLong(o.toString());
                break;
            case 1:
                session =  o.toString();
                break;
        }

    }

    @Override
    public void getPropertyInfo(int i, Hashtable hashtable, PropertyInfo propertyInfo) {
        switch (i){
            case 0:
                propertyInfo.type = PropertyInfo.LONG_CLASS;
                propertyInfo.name = "ManagerId";
                break;
            case 1:
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                propertyInfo.name = "SessionId";
                break;
        }

    }
}
