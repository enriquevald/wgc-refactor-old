package com.wynsistems.alarmas.service.alarm.response;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.util.Hashtable;

public class CreateAlarmResponse implements KvmSerializable{
    private Integer responseCode;
    private String responseCodeDescription;


    public void setResponseCode(Integer responseCode) {
        this.responseCode = responseCode;
    }

    public void setResponseCodeDescription(String responseCodeDescription) {
        this.responseCodeDescription = responseCodeDescription;
    }

    @Override
    public Object getProperty(int i) {
        switch (i){
            case 0:
                return responseCode;
            case 1:
                return responseCodeDescription;
            default:
                return null;
        }
    }

    @Override
    public int getPropertyCount() {
        return 2;
    }

    @Override
    public void setProperty(int i, Object o) {
        switch(i)
        {
            case 0:
                responseCode = Integer.parseInt(o.toString());
                break;
            case 1:
                responseCodeDescription = o.toString();
                break;
            default:
                break;
        }
    }

    @Override
    public void getPropertyInfo(int i, Hashtable hashtable, PropertyInfo propertyInfo) {
        switch(i)
        {
            case 0:
                propertyInfo.type = PropertyInfo.INTEGER_CLASS;
                propertyInfo.name = "ResponseCode";
                break;
            case 1:
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                propertyInfo.name = "ResponseCodeDescription";
                break;
            default:break;
        }
    }
}
