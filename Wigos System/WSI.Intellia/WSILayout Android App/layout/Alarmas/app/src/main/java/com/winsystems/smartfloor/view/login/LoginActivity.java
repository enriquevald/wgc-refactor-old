package com.winsystems.smartfloor.view.login;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.ContentResolver;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.support.v7.app.ActionBar;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.winsystems.smartfloor.R;
import com.winsystems.smartfloor.common.AndroidLogger;
import com.winsystems.smartfloor.helpers.LogHelper;
import com.winsystems.smartfloor.service.ServiceFactory;
import com.winsystems.smartfloor.service.login.LoginService;
import com.winsystems.smartfloor.service.login.response.DATA_LogInResponse;
import com.winsystems.smartfloor.service.ResponseCode;
import com.winsystems.smartfloor.service.mqtt.PahoMqttService;
import com.winsystems.smartfloor.view.menu.MenuActivity;
import com.winsystems.smartfloor.view.setting.SettingsActivity;
import com.winsystems.smartfloor.view.utils.EncryptHelper;
import com.winsystems.smartfloor.view.utils.KioskModeActivity;

import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;

import java.util.ArrayList;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: LoginActivity
//
// DESCRIPTION: Implement methods for login to the app.
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public class LoginActivity extends KioskModeActivity implements LoaderCallbacks<Cursor> , SharedPreferences.OnSharedPreferenceChangeListener  {

    private static String urlLogin = ""; //Only to debug on android emulator

    private UserLoginTask authTask = null;

    private AutoCompleteTextView emailView;
    private EditText passwordView;
    private View progressView;
    private View loginFormView;
    private String preferenceBrokerHost = "";
    private String userId = "";
    private SharedPreferences prefs;
    private static AndroidLogger mLog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLog = LogHelper.getLogger(this);
        setContentView(R.layout.activity_login);
        // Set up the login form.
        emailView = (AutoCompleteTextView) findViewById(R.id.email);
        populateAutoComplete();
        prefs = PreferenceManager.getDefaultSharedPreferences(this);//get the preferences that are allowed to be given
        passwordView = (EditText) findViewById(R.id.password);
        passwordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);//get the preferences that are allowed to be given
        prefs.registerOnSharedPreferenceChangeListener(this);//set the listener to listen for changes in the preferences
        urlLogin = prefs.getString("alarm_server_address", urlLogin);
        trySetDefaultUrlValues(prefs);


        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        loginFormView = findViewById(R.id.login_form);
        progressView = findViewById(R.id.login_progress);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(getString(R.string.login_activity_title));

        boolean IsServiceOffline =  this.getIntent().getBooleanExtra("NoService", false);

        if(IsServiceOffline){
            new AlertDialog.Builder(this)
                    .setTitle(R.string.alarms_title)
                    .setMessage(R.string.WEBSERVICE_NO_CONNECTION_DIALOG)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // continue with delete
                        }
                    })
                    .show();
        }

    }

    private void trySetDefaultUrlValues(SharedPreferences prefs) {
        //urlLogin = prefs.getString("login_address", urlLogin);
        if(urlLogin == null || urlLogin.isEmpty()){
            urlLogin = this.getResources().getString(R.string.server_default_address);
            //Adding to the preferences the clientId
            SharedPreferences.Editor editorUrlLoginAddress = prefs.edit();
            editorUrlLoginAddress.putString("alarm_server_address", urlLogin);
            editorUrlLoginAddress.apply();
        }
    }

    private void populateAutoComplete() {
        getLoaderManager().initLoader(0, null, this);
    }


    public void attemptLogin() {
        if (authTask != null) {
            return;
        }

        // Reset errors.
        emailView.setError(null);
        passwordView.setError(null);

        // Store values at the time of the login attempt.
        String email = emailView.getText().toString();
        String password = passwordView.getText().toString();

        boolean cancel = false;
        View focusView = null;


        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(password)) {
            passwordView.setError(getString(R.string.error_invalid_password));
            focusView = passwordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            emailView.setError(getString(R.string.error_field_required));
            focusView = emailView;
            cancel = true;
        }
        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            authTask = new UserLoginTask();
            authTask.execute((Void) null);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent i = new Intent(this, SettingsActivity.class);
            startActivityForResult(i, 1);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            loginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            loginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    loginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            progressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    progressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            loginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(this,
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,

                // Select only email addresses.
                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{ContactsContract.CommonDataKinds.Email
                .CONTENT_ITEM_TYPE},

                // Show primary email addresses first. Note that there won't be
                // a primary email address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        List<String> emails = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            emails.add(cursor.getString(ProfileQuery.ADDRESS));
            cursor.moveToNext();
        }

        addEmailsToAutoComplete(emails);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        urlLogin = sharedPreferences.getString("alarm_server_address", urlLogin);

    }

    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
    }


    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(LoginActivity.this,
                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

        emailView.setAdapter(adapter);
    }

    public void StarMQTTService() {
        String mDeviceId = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        //Setting brokerHost from preferences
        preferenceBrokerHost = prefs.getString("alarm_server_address", preferenceBrokerHost);
        userId = prefs.getString("userId", "");
        //Adding to the preferences the clientId
        SharedPreferences.Editor editorClientId = prefs.edit();
        editorClientId.putString("clientId", mDeviceId);
        editorClientId.apply();

        SharedPreferences settings = getSharedPreferences(PahoMqttService.APP_ID, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("broker", preferenceBrokerHost);
        editor.putString("topic", "/Task/Notifications/" + userId + "/+");
        editor.putString("message-topic", "/Messages/" + userId + "/+");
        editor.putString("getlog-topic", "/GetLog/" + userId + "/+");
        editor.apply();
        PahoMqttService.actionStart(this);
    }
    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user using webservice endpoint.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

        private static final String TAG = "UserLoginTask";
        private String responseMessage;
        private String _username;
        private String _password;

        @Override
        protected void onPreExecute(){
            _username = emailView.getText().toString();
            _password = passwordView.getText().toString();
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                String username = _username;
                String encryptedPassword = EncryptHelper.Encrypt(_password, "LioMessiMascheranoBarca");
                String deviceId =  Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
                LoginService  loginService = ServiceFactory.getInstance().getLoginService(getApplicationContext());
                DATA_LogInResponse loginResponse = loginService.loginUser(getApplicationContext(), username, encryptedPassword, urlLogin, deviceId);

                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                SharedPreferences.Editor edit = preferences.edit();
                edit.putString("userName", _username.toUpperCase());
                edit.putString("userPassword",encryptedPassword);
                edit.apply();
                if(loginResponse.getResponseCode() != ResponseCode.DATA_RC_OK)
                {
                    responseMessage = loginResponse.getResponseCodeDescription();
                    return false;
                }

            } catch (HttpClientErrorException e) {
                mLog.error("",e);
                return false;
            } catch (ResourceAccessException e) {
                mLog.error("", e);
                return false;
            } catch (Exception e) {
                mLog.error("", e);
                return false;
            }

            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            authTask = null;
            //showProgress(false);

            if (success) {

                try {

                    Thread t = new Thread(){
                        public void run(){
                            StarMQTTService();
                        }
                    };
                    t.start();
                    Intent mainIntent = new Intent(LoginActivity.this, MenuActivity.class);
                    LoginActivity.this.startActivity(mainIntent);

                    finish();
                } catch (Exception e) {
                    mLog.error("", e);
                }
            } else {
                showProgress(false);
                String errorMessage;
                if(responseMessage != null)
                {
                    if(responseMessage.isEmpty())
                        errorMessage = getString(R.string.error_incorrect_password);
                    else
                        errorMessage = responseMessage;
                }else
                {
                    errorMessage = getString(R.string.error_incorrect_password);
                }
                mLog.warning(errorMessage,null);
                passwordView.setError(errorMessage);
                passwordView.requestFocus();
            }
        }

        @Override
        protected void onCancelled() {
            authTask = null;
            showProgress(false);
        }


    }


}



