package com.winsystems.smartfloor.service;

import android.content.Context;

import com.winsystems.smartfloor.service.alarm.AlarmService;
import com.winsystems.smartfloor.service.alarm.DefaultAlarmService;
import com.winsystems.smartfloor.service.alarm.DefaultLayoutSiteAlarmService;
import com.winsystems.smartfloor.service.alarm.LayoutSiteAlarmsService;
import com.winsystems.smartfloor.service.log.DefaultLogService;
import com.winsystems.smartfloor.service.log.LogService;
import com.winsystems.smartfloor.service.login.DefaultLoginService;
import com.winsystems.smartfloor.service.login.LoginService;
import com.winsystems.smartfloor.service.message.DefaultMessageService;
import com.winsystems.smartfloor.service.message.MessageService;
import com.winsystems.smartfloor.service.photo.DefaultPhotoService;
import com.winsystems.smartfloor.service.photo.PhotoService;
import com.winsystems.smartfloor.service.update.DefaultUpdateService;
import com.winsystems.smartfloor.service.update.UpdateService;

public class ServiceFactory {
    private static ServiceFactory instance = null;
    private ServiceFactory() {
    }

    public static ServiceFactory getInstance(){
        if(instance== null){
            instance = new ServiceFactory();
        }

        return instance;
    }

    public AlarmService getAlarmService(Context context){
        return DefaultAlarmService.getInstance(context);
    }

    public LoginService getLoginService(Context context){
        return DefaultLoginService.getInstance(context);
    }

    public PhotoService getPhotoService() {return DefaultPhotoService.getInstance();}

    public MessageService getMessageService(Context context) {
        return DefaultMessageService.getInstance(context);
    }

    public LayoutSiteAlarmsService getLayoutSiteAlarmsService  (Context context) {return DefaultLayoutSiteAlarmService.getInstance(context);}

    public UpdateService getUpdateService(Context context) {
        return DefaultUpdateService.getInstance(context);
    }

    public LogService getLogService(Context context) {
        return DefaultLogService.getInstance(context);
    }

}
