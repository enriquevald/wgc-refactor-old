package com.wynsistems.alarmas.view.log;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ListView;

import com.wynsistems.alarmas.R;
import com.wynsistems.alarmas.helpers.SaveInformationHelper;
import com.wynsistems.alarmas.view.utils.Constants;
import com.wynsistems.alarmas.view.utils.KioskModeActivity;

import java.util.List;

public class LogViewer extends KioskModeActivity {

    private SwipeRefreshLayout pullToRefresh;
    private  ListView logListView;
    private List<String> logList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_viewer);
        LoadLogFileToArrayList();
        logListView = (ListView) findViewById(R.id.logListView);
        logListView.setAdapter(new LogListAdapter(getApplicationContext(), logList));
        logListView.setSelection(logList.size() - 1);

        pullToRefresh = (SwipeRefreshLayout) findViewById(R.id.pullToRefresh);
        pullToRefresh.setColorSchemeColors(R.color.winsystem_blue,
                                           R.color.winsystem_red,
                                           R.color.winsystem_yellow);

        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                new UpdateLogAsync().execute();
            }
        });
    }

    private void LoadLogFileToArrayList() {
        logList = new SaveInformationHelper<String>(getApplicationContext()).readList(Constants.LOG);

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    protected void onStop() {
        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.log_activity_actions_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_clear_log:
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        SaveInformationHelper<String> helper = new SaveInformationHelper<String>(getApplicationContext());
                        helper.clearList(Constants.LOG);
                        logList = helper.readList(Constants.LOG);
                        logListView.setAdapter(new LogListAdapter(getApplicationContext(), logList));
                        logListView.setSelection(logList.size() - 1);
                    }
                });
                return true;
            case R.id.action_refresh_log:
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        logListView.setAdapter(new LogListAdapter(getApplicationContext(), logList));
                        logListView.setSelection(logList.size() - 1);
                    }
                });
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private class UpdateLogAsync extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pullToRefresh.setRefreshing(true);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                LoadLogFileToArrayList();
                return true;
            }catch (Exception e)
            {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if(result)
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        logListView.setAdapter(new LogListAdapter(getApplicationContext(), logList));
                        logListView.setSelection(logList.size() - 1);
                    }
                });

            pullToRefresh.setRefreshing(false);
        }


    }
}


