package com.winsystems.smartfloor.model;


import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.io.Serializable;
import java.util.Hashtable;

public class LayoutPlayerSession implements KvmSerializable, Serializable
{
  private int accountId;
  private int age;
  private int gender;
  private Boolean isAnonymous;
  private Boolean isVip;
  private int level;
  private String name;
  private Boolean sessionStatus;

  public int getAccountId() {
    return accountId;
  }

  public void setAccountId(int accountId) {
    this.accountId = accountId;
  }

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public Gender getGender() {
    return gender == 1? Gender.MALE : Gender.FEMALE;
  }

  public void setGender(int gender) {
    this.gender = gender;
  }

  public Boolean getIsAnonymous() {
    return isAnonymous;
  }

  public void setIsAnonymous(Boolean isAnonymous) {
    this.isAnonymous = isAnonymous;
  }

  public Boolean getIsVip() {
    return isVip;
  }

  public void setIsVip(Boolean isVip) {
    this.isVip = isVip;
  }

  public LayoutPlayerSessionLevel getLevel() {
    return LayoutPlayerSessionLevel.LEVEL00;
  }

  public void setLevel(int level) {
    this.level = level;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Boolean getSessionStatus() {
    return sessionStatus;
  }

  public void setSessionStatus(Boolean sessionStatus) {
    this.sessionStatus = sessionStatus;
  }

  @Override
  public Object getProperty(int i) {
    switch (i){
      case 0:
        return accountId;
      case 1:
        return age;
      case 2:
        return gender;
      case 3:
        return isAnonymous;
      case 4:
        return isVip;
      case 5:
        return level;
      case 6:
        return name;
      case 7:
        return sessionStatus;
      default:
        return null;
    }
  }

  @Override
  public int getPropertyCount() {
    return 8;
  }

  @Override
  public void setProperty(int i, Object o) {
    switch (i){
      case 0:
        accountId = Integer.parseInt(o.toString());
      case 1:
        age = Integer.parseInt(o.toString());
      case 2:
        gender = Integer.parseInt(o.toString());
      case 3:
        isAnonymous = Boolean.parseBoolean(o.toString());
      case 4:
        isVip = Boolean.parseBoolean(o.toString());
      case 5:
        level = Integer.parseInt(o.toString());
      case 6:
        name = o.toString();
      case 7:
        sessionStatus  = Boolean.parseBoolean(o.toString());
    }
  }

  @Override
  public void getPropertyInfo(int i, Hashtable hashtable, PropertyInfo propertyInfo) {
    switch (i) {
      case 0:
        propertyInfo.type = PropertyInfo.INTEGER_CLASS;
        propertyInfo.name = "AccountId";
        break;
      case 1:
        propertyInfo.type = PropertyInfo.INTEGER_CLASS;
        propertyInfo.name = "Age";
        break;
      case 2:
        propertyInfo.type = PropertyInfo.INTEGER_CLASS;
        propertyInfo.name = "Gender";
        break;
      case 3:
        propertyInfo.type = PropertyInfo.BOOLEAN_CLASS;
        propertyInfo.name = "IsAnonymous";
        break;
      case 4:
        propertyInfo.type = PropertyInfo.BOOLEAN_CLASS;
        propertyInfo.name = "IsVip";
        break;
      case 5:
        propertyInfo.type = PropertyInfo.INTEGER_CLASS;
        propertyInfo.name = "Level";
        break;
      case 6:
        propertyInfo.type = PropertyInfo.STRING_CLASS;
        propertyInfo.name = "Level";
        break;
      case 7:
        propertyInfo.type = PropertyInfo.BOOLEAN_CLASS;
        propertyInfo.name = "Level";
        break;
    }
  }
}
