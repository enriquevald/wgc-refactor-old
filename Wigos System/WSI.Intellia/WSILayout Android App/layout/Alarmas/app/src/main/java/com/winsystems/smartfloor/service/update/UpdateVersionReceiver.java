package com.winsystems.smartfloor.service.update;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.winsystems.smartfloor.R;
import com.winsystems.smartfloor.view.update.UpdateApplicationActivity;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: UpdateVersionReceiver
//
// DESCRIPTION: Implements methods to update the application service.
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public class UpdateVersionReceiver extends BroadcastReceiver {
  @Override
  public void onReceive(final Context context, Intent intent) {
    if(intent != null) {
      String version = intent.getStringExtra("version");
      if (version != null) {
        if(!version.isEmpty()) {
          SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
          SharedPreferences.Editor editor = preferences.edit();
          editor.putString("lastVersionApp", intent.getStringExtra("version"));
          editor.apply();
        }
      }
    }

    if(!com.winsystems.smartfloor.helpers.IsLastestVersionHelper.IsLastestVersion(context))
    {
      Intent updateIntent = new Intent(context, UpdateApplicationActivity.class);
      updateIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
      context.startActivity(updateIntent);
    }

  }
}
