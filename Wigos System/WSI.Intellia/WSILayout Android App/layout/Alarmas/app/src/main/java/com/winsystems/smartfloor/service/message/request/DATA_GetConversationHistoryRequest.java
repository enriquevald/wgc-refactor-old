package com.winsystems.smartfloor.service.message.request;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.util.Hashtable;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: DATA_GetConversationHistoryRequest
//
// DESCRIPTION: Implement methods for get conversation history.
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public class DATA_GetConversationHistoryRequest implements KvmSerializable{

  private String session;
  private Long managerId;

  public void setSession(String session) {
    this.session = session;
  }

  public void setManagerId(Long managerId) {
    this.managerId = managerId;
  }

  @Override
  public Object getProperty(int i) {
    switch (i){
      case 0:
        return managerId;
      case 1:
        return session;
      default:
        return null;
    }
  }

  @Override
  public int getPropertyCount() {
    return 2;
  }

  @Override
  public void setProperty(int i, Object o) {

    switch (i){
      case 0:
        managerId = Long.parseLong(o.toString());
        break;
      case 1:
        session =  o.toString();
        break;
    }

  }

  @Override
  public void getPropertyInfo(int i, Hashtable hashtable, PropertyInfo propertyInfo) {
    switch (i){
      case 0:
        propertyInfo.type = PropertyInfo.LONG_CLASS;
        propertyInfo.name = "ManagerId";
        break;
      case 1:
        propertyInfo.type = PropertyInfo.STRING_CLASS;
        propertyInfo.name = "SessionId";
        break;
    }

  }
}
