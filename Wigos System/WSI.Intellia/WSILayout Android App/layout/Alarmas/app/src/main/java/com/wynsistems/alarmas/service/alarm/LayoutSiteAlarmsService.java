package com.wynsistems.alarmas.service.alarm;

import com.wynsistems.alarmas.service.alarm.response.DATA_TerminalInfoResponse;

public interface LayoutSiteAlarmsService{

    public DATA_TerminalInfoResponse getTerminalStatusByTerminalID(String url, String sessionId, String terminalId);
    public DATA_TerminalInfoResponse getAlarmsCreatedByUserID(String url, String sessionId);
}
