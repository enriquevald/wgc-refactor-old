package com.wynsistems.alarmas.view.menu;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.wynsistems.alarmas.R;
import com.wynsistems.alarmas.helpers.PendingToReadMessagesHelper;

import java.util.List;

public class MenuListAdapter extends BaseAdapter {

    private final List<MenuItem> menuItemList;
    private final LayoutInflater inflater;
    private final Context _context;

    public MenuListAdapter(Context context, List<MenuItem> menuItemList) {
        this.menuItemList = menuItemList;
        _context = context;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return menuItemList.size();
    }

    @Override
    public Object getItem(int position) {
        return menuItemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        if (vi == null)
            vi = inflater.inflate(R.layout.menu_item, null);

        MenuItem menuItem = (MenuItem) this.getItem(position);
        TextView menuDescription = (TextView)vi.findViewById(R.id.menu_item_description);
        FrameLayout frameLayout = (FrameLayout) vi.findViewById(R.id.menu_item_icon);

        String menuText = menuItem.getDescription();
        menuDescription.setText(menuText);
        frameLayout.setBackgroundResource(menuItem.getUrlDrawable());

        return vi;
    }
}
