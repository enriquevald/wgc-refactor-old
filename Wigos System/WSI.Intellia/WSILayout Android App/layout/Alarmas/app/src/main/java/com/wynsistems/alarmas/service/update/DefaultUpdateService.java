package com.wynsistems.alarmas.service.update;

import android.content.Context;
import android.util.Log;

import com.wynsistems.alarmas.common.AndroidLogger;
import com.wynsistems.alarmas.helpers.LogHelper;
import com.wynsistems.alarmas.service.ResponseCode;
import com.wynsistems.alarmas.service.update.request.DATA_UpdateApplicationRequest;
import com.wynsistems.alarmas.service.update.response.DATA_UpdateApplicationResponse;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

public class DefaultUpdateService implements UpdateService {

    private static DefaultUpdateService instance;
    private static AndroidLogger mLog;

    @Override
    public DATA_UpdateApplicationResponse getLastestApplicationVersion(String url, String sessionId) {
        final String NAMESPACE = "http://winsystemsintl.com/";
        final String METHOD_NAME = "UpdateApplication";
        final String SOAP_ACTION = "http://winsystemsintl.com/IDATAService/UpdateApplication";

        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        DATA_UpdateApplicationRequest dataUpdateApplicationRequest = new DATA_UpdateApplicationRequest();
        dataUpdateApplicationRequest.setSessionId(sessionId);

        DATA_UpdateApplicationResponse response = new DATA_UpdateApplicationResponse();

        PropertyInfo pi = new PropertyInfo();
        pi.setName("update");
        pi.setValue(dataUpdateApplicationRequest);
        pi.setType(dataUpdateApplicationRequest.getClass());

        request.addProperty(pi);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;

        envelope.setOutputSoapObject(request);

        envelope.addMapping(NAMESPACE, "DATA_UpdateApplicationRequest", dataUpdateApplicationRequest.getClass());
        envelope.addMapping(NAMESPACE, "DATA_UpdateApplicationResponse", response.getClass());

        HttpTransportSE transporte = new HttpTransportSE(url, 70000);

        try {
            transporte.call(SOAP_ACTION, envelope);

            SoapObject resSoap = (SoapObject) envelope.getResponse();

            response.setResponseCode(Integer.parseInt(resSoap.getProperty(0).toString()));
            if(response.getResponseCode() == ResponseCode.DATA_RC_OK){
                response.setResponseCodeDescription(resSoap.getProperty(1).toString());
                response.setSessionId(resSoap.getProperty(2).toString());
                response.setVersion(resSoap.getProperty(3).toString());
            }
        } catch (Exception e) {
            String message = !e.getMessage().isEmpty()? e.getMessage(): "";
            mLog.error("DefaultUpdateService - getLastestApplicationVersion - " + message, e);
            Log.e("DefaultUpdateService", message);

            response.setResponseCode(8);
            response.setResponseCodeDescription("The Service is Unavilable.");
        }

        return response;
    }

    public static DefaultUpdateService getInstance(Context context){
        mLog = LogHelper.getLogger(context);
        if(instance == null){
            instance = new DefaultUpdateService();
        }
        return instance;
    }
}
