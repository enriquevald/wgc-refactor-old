package com.winsystems.smartfloor.common;

import android.content.Context;

import com.winsystems.smartfloor.helpers.SaveInformationHelper;
import com.winsystems.smartfloor.view.utils.Constants;

import java.util.ArrayList;
import java.util.List;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: LayoutNotificationManager
//
// DESCRIPTION: Layout Notification Manager
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public class LayoutNotificationManager {

  private Context context;
  private SaveInformationHelper<LayoutNotification> notificationSaver;
  private String notificationManagerNotificationList = Constants.NOTIFICATION_MANAGER_NOTIFICATION_LIST;

  public LayoutNotificationManager(Context applicationContext) {
    this.context = applicationContext;
    this.notificationSaver = new SaveInformationHelper<>(context);
  }

  public void add(LayoutNotification notification) {
    List<LayoutNotification> list = notificationSaver.readList(Constants.NOTIFICATION_MANAGER_NOTIFICATION_LIST);
    if(notification.getId() != 0){
      list = findAndReplaseNotification(notification, list);
    }else{
      notification.setId(list.size()+1);
      list.add(notification);
    }
    notificationSaver.writeList(list, notificationManagerNotificationList);
  }

  private List<LayoutNotification> findAndReplaseNotification(LayoutNotification notification, List<LayoutNotification> list) {
    for (LayoutNotification n:list) {
      if(n.getId().intValue() == notification.getId().intValue()) {
        list.remove(n);
      }
    }
    list.add(notification);
    return list;
  }

  public List<LayoutNotification> getNotificationsByType(LayoutNotificationType type) {
    List<LayoutNotification> list = notificationSaver.readList(Constants.NOTIFICATION_MANAGER_NOTIFICATION_LIST);
    List<LayoutNotification> filtedList = new ArrayList<>();
    for (LayoutNotification ln : list) {
      if(ln.getType() == type)
        filtedList.add(ln);
    }

    return filtedList;
  }

  public int getCountNotificationsByType(LayoutNotificationType type) {
    List<LayoutNotification> list = notificationSaver.readList(Constants.NOTIFICATION_MANAGER_NOTIFICATION_LIST);
    List<LayoutNotification> filtedList = new ArrayList<>();
    for (LayoutNotification ln : list) {
      if(ln.getType() == type)
        filtedList.add(ln);
    }

    return filtedList.size();
  }

  public void remove(Integer notificationId) {
    List<LayoutNotification> list = notificationSaver.readList(Constants.NOTIFICATION_MANAGER_NOTIFICATION_LIST);
    List<LayoutNotification> filtedList = new ArrayList<>();
    for (LayoutNotification ln : list) {
      if(!ln.getId().equals(notificationId))
        filtedList.add(ln);
    }

    if(filtedList.size() == 0)
      notificationSaver.clearList(Constants.NOTIFICATION_MANAGER_NOTIFICATION_LIST);
    else
      notificationSaver.writeList(filtedList, Constants.NOTIFICATION_MANAGER_NOTIFICATION_LIST);
  }

  public void removeNotificationsByType(LayoutNotificationType type) {
    List<LayoutNotification> list = notificationSaver.readList(Constants.NOTIFICATION_MANAGER_NOTIFICATION_LIST);
    List<LayoutNotification> filtedList = new ArrayList<>();
    for (LayoutNotification ln : list) {
      if(ln.getType() != type)
        filtedList.add(ln);
    }

    if(filtedList.size() == 0)
      notificationSaver.clearList(Constants.NOTIFICATION_MANAGER_NOTIFICATION_LIST);
    else
      notificationSaver.writeList(filtedList, Constants.NOTIFICATION_MANAGER_NOTIFICATION_LIST);
  }

  public void deleteNotificationById(String taskId) {
    List<LayoutNotification> list = notificationSaver.readList(Constants.NOTIFICATION_MANAGER_NOTIFICATION_LIST);
    for (LayoutNotification n:list) {
      if(n.getId().intValue() == Integer.parseInt(taskId)) {
        list.remove(n);
      }
    }
    if(list.size() == 0)
      notificationSaver.clearList(Constants.NOTIFICATION_MANAGER_NOTIFICATION_LIST);
    else
      notificationSaver.writeList(list, Constants.NOTIFICATION_MANAGER_NOTIFICATION_LIST);
  }
}
