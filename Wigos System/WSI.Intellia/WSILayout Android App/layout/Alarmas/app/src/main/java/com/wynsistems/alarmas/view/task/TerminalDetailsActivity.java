package com.wynsistems.alarmas.view.task;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;

import com.wynsistems.alarmas.R;
import com.wynsistems.alarmas.view.terminal.TabsPagerAdapter;
import com.wynsistems.alarmas.view.utils.KioskModeActivity;

public class TerminalDetailsActivity extends KioskModeActivity implements ActionBar.TabListener {

    private ViewPager tabsviewPager;
    private ActionBar mActionBar;
    private TerminalInfoTabsPageAdapter mTabsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terminal_details);


        tabsviewPager = (ViewPager) findViewById(R.id.pager);
        mTabsAdapter = new TerminalInfoTabsPageAdapter(getSupportFragmentManager());
        tabsviewPager.setAdapter(mTabsAdapter);
        mActionBar =  getSupportActionBar();
        mActionBar.setHomeButtonEnabled(false);
        mActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        android.support.v7.app.ActionBar.Tab terminalInfo = mActionBar.newTab().setIcon(R.drawable.machineflags).setTabListener(this);
        android.support.v7.app.ActionBar.Tab playerInfo = mActionBar.newTab().setIcon(R.drawable.ic_account_circle_white_18dp).setTabListener(this);

        mActionBar.addTab(terminalInfo);
        mActionBar.addTab(playerInfo);
        tabsviewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                getSupportActionBar().setSelectedNavigationItem(position);
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
        });

    }



    @Override
    public void onTabSelected(ActionBar.Tab tab, android.support.v4.app.FragmentTransaction ft) {
        tabsviewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, android.support.v4.app.FragmentTransaction ft) {

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, android.support.v4.app.FragmentTransaction ft) {

    }
}
