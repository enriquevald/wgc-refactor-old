package com.wynsistems.alarmas.repository;

import android.os.Environment;

import java.io.File;
import java.io.IOException;

public final class BaseInternalDirectoryFactory {

    // Standard storage location for digital camera files
    private static final String CAMERA_DIR = "/room_task/";
    private static final String JSON_DIR = "/json/";

    public File getAlbumStorageDir(String albumName) {
        return new File(
                Environment.getExternalStorageDirectory()
                        + CAMERA_DIR
                        + albumName
        );
    }

    public File getJsonStorageDir(String albumName) throws IOException {
        File directory = new File(Environment.getExternalStorageDirectory() + JSON_DIR + albumName);
        if(!directory.mkdirs()){
            if (!directory.exists()){
                return null;
            }
        }
        return  File.createTempFile(albumName, "json", directory);
    }
}
