package com.wynsistems.alarmas.repository.factory;

import android.content.Context;

import com.wynsistems.alarmas.R;
import com.wynsistems.alarmas.model.Alarm;
import com.wynsistems.alarmas.model.LayoutSiteItemGroupCategory;
import com.wynsistems.alarmas.model.LayoutSiteItemSeverity;
import com.wynsistems.alarmas.model.AlarmSortingCriteria;
import com.wynsistems.alarmas.model.SummarizedAlarms;
import com.wynsistems.alarmas.service.alarm.AlarmService;
import com.wynsistems.alarmas.service.ServiceFactory;
import com.wynsistems.alarmas.service.login.response.LayoutSiteTask;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SummarizedAlarmsFactory {

    private AlarmService alarmService;
    private static SummarizedAlarmsFactory instance = null;

    private SummarizedAlarmsFactory(String alarmServerUrl, Context context) {
        alarmService = ServiceFactory.getInstance().getAlarmService(context).setUrl(alarmServerUrl);
    }

    public static SummarizedAlarmsFactory getInstance(String alarmServerUrl,Context context){
        if(instance == null){
            instance = new SummarizedAlarmsFactory(alarmServerUrl, context);
        }
        return instance;
    }

    public ArrayList<SummarizedAlarms> getSummarizedAlarmsByCriticality(AlarmSortingCriteria sortingCriteria, Context context){

        ArrayList<SummarizedAlarms> summarizedAlarmsList = new ArrayList<>();

        List<LayoutSiteTask> highCriticality = alarmService.getAlarmByCriticality(LayoutSiteItemSeverity.HIGH);
        List<LayoutSiteTask> mediumCriticality = alarmService.getAlarmByCriticality(LayoutSiteItemSeverity.MEDIUM);
        List<LayoutSiteTask> lowCriticality = alarmService.getAlarmByCriticality(LayoutSiteItemSeverity.LOW);

        if(sortingCriteria == AlarmSortingCriteria.NEWEST) {
            Collections.sort(highCriticality);
            Collections.sort(mediumCriticality);
            Collections.sort(lowCriticality);

        }else{
            Collections.reverse(highCriticality);
            Collections.reverse(mediumCriticality);
            Collections.reverse(lowCriticality);

        }

        summarizedAlarmsList.add(new SummarizedAlarms(LayoutSiteItemSeverity.HIGH,context.getResources().getString(R.string.high_level_description),highCriticality.size(), highCriticality));
        summarizedAlarmsList.add(new SummarizedAlarms(LayoutSiteItemSeverity.MEDIUM, context.getResources().getString(R.string.medium_level_description),mediumCriticality.size(),mediumCriticality));
        summarizedAlarmsList.add(new SummarizedAlarms(LayoutSiteItemSeverity.LOW, context.getResources().getString(R.string.low_level_description),lowCriticality.size(),lowCriticality));

        return summarizedAlarmsList;
    }

    public ArrayList<SummarizedAlarms> getSummarizedAlarmsByCategory(AlarmSortingCriteria sortingCriteria, Context context){

        ArrayList<SummarizedAlarms> summarizedAlarmsList = new ArrayList<>();

        List<LayoutSiteTask> intrusion = alarmService.getAlarmByCategory(LayoutSiteItemGroupCategory.Intrusion);
        List<LayoutSiteTask> machineFlags = alarmService.getAlarmByCategory(LayoutSiteItemGroupCategory.MachineFlags);
        List<LayoutSiteTask> support = alarmService.getAlarmByCategory(LayoutSiteItemGroupCategory.Support);
        List<LayoutSiteTask> payCheck = alarmService.getAlarmByCategory(LayoutSiteItemGroupCategory.PayCheck);
        List<LayoutSiteTask> jamBill = alarmService.getAlarmByCategory(LayoutSiteItemGroupCategory.JamBill);
        List<LayoutSiteTask> printer = alarmService.getAlarmByCategory(LayoutSiteItemGroupCategory.Printer);

        if(sortingCriteria == AlarmSortingCriteria.NEWEST) {
            Collections.sort(intrusion);
            Collections.sort(machineFlags);
            Collections.sort(support);
            Collections.sort(payCheck);
            Collections.sort(jamBill);
            Collections.sort(printer);

        }else{
            Collections.reverse(intrusion);
            Collections.reverse(machineFlags);
            Collections.reverse(support);
            Collections.reverse(payCheck);
            Collections.reverse(jamBill);
            Collections.reverse(printer);
        }

        summarizedAlarmsList.add(new SummarizedAlarms(LayoutSiteItemGroupCategory.Intrusion, LayoutSiteItemGroupCategory.getTypeDescription(LayoutSiteItemGroupCategory.Intrusion, context),intrusion.size(), intrusion));
        summarizedAlarmsList.add(new SummarizedAlarms(LayoutSiteItemGroupCategory.JamBill, LayoutSiteItemGroupCategory.getTypeDescription(LayoutSiteItemGroupCategory.JamBill, context),jamBill.size(), jamBill));
        summarizedAlarmsList.add(new SummarizedAlarms(LayoutSiteItemGroupCategory.MachineFlags, LayoutSiteItemGroupCategory.getTypeDescription(LayoutSiteItemGroupCategory.MachineFlags, context),machineFlags.size(), machineFlags));
        summarizedAlarmsList.add(new SummarizedAlarms(LayoutSiteItemGroupCategory.PayCheck, LayoutSiteItemGroupCategory.getTypeDescription(LayoutSiteItemGroupCategory.PayCheck, context),payCheck.size(), payCheck));
        summarizedAlarmsList.add(new SummarizedAlarms(LayoutSiteItemGroupCategory.Support, LayoutSiteItemGroupCategory.getTypeDescription(LayoutSiteItemGroupCategory.Support, context),support.size(), support));
        summarizedAlarmsList.add(new SummarizedAlarms(LayoutSiteItemGroupCategory.Printer, LayoutSiteItemGroupCategory.getTypeDescription(LayoutSiteItemGroupCategory.Printer, context),printer.size(), printer));

        return summarizedAlarmsList;
    }

    public ArrayList<SummarizedAlarms> getSummarizedAlarmsByCompleted(AlarmSortingCriteria sortingCriteria, Context context){

        ArrayList<SummarizedAlarms> summarizedAlarmsList = new ArrayList<>();

        List<LayoutSiteTask> completed = alarmService.getAlarmsInProgress();
        List<LayoutSiteTask> notCompleted = alarmService.getAlarmsAssigned();

        if(sortingCriteria == AlarmSortingCriteria.NEWEST) {
            Collections.sort(completed);
            Collections.sort(notCompleted);

        }else{
            Collections.reverse(notCompleted);
            Collections.reverse(notCompleted);
        }

        summarizedAlarmsList.add(new SummarizedAlarms(true, context.getResources().getString(R.string.accepted),completed.size(), completed));
        summarizedAlarmsList.add(new SummarizedAlarms(false,context.getResources().getString(R.string.assigned),notCompleted.size(), notCompleted));

        return summarizedAlarmsList;
    }
}
