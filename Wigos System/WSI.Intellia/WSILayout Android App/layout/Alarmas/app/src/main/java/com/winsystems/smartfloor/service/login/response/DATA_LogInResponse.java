package com.winsystems.smartfloor.service.login.response;


import com.winsystems.smartfloor.model.LayoutSiteAlarm;
import com.winsystems.smartfloor.service.ResponseCode;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: DATA_LogInResponse
//
// DESCRIPTION: Implement methods for login to the app.
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public class DATA_LogInResponse implements KvmSerializable {
  private String sessionId;
  private Integer responseCode;
  private String responseCodeDescription;
  private List<LayoutSiteTask> availableTasks;
  private List<BeaconAvailable> beaconsAvailables;
  private List<Long> roles;
  private List<LayoutSiteAlarm> terminalAlarmsCreatedInTheDay;
  private Long userId;
  private String lastVersionApp;
  private Boolean qrAvailable;

  public DATA_LogInResponse() {
    availableTasks = new ArrayList<>();
    roles = new ArrayList<>();
    terminalAlarmsCreatedInTheDay = new ArrayList<>();
  }

  public String getSessionId() {
    return sessionId;
  }

  public void setSessionId(String sessionId) {

    this.sessionId = sessionId;
  }

  public Boolean getQrAvailable() {
    return qrAvailable;
  }

  public void setQrAvailable(Boolean qrAvailable) {
    this.qrAvailable = qrAvailable;
  }

  public String getLastVersionApp() {
    return lastVersionApp;
  }

  public void setLastVersionApp(String lastVersionApp) {
    this.lastVersionApp = lastVersionApp;
  }

  public void setResponseCode(Integer responseCode) {
    this.responseCode = responseCode;
  }

  public void setResponseCodeDescription(String responseCodeDescription) {
    this.responseCodeDescription = responseCodeDescription;
  }

  public void setAvailableTasks(List<LayoutSiteTask> availableTasks) {
    this.availableTasks = availableTasks;
  }

  public List<BeaconAvailable> getBeaconsAvailables() {
    return beaconsAvailables;
  }

  public void setBeaconsAvailables(List<BeaconAvailable> beaconsAvailables) {
    this.beaconsAvailables = beaconsAvailables;
  }

  public void setRoles(List<Long> roles) {
    this.roles = roles;
  }


  public void setTerminalAlarmsCreatedInTheDay(List<LayoutSiteAlarm> terminalAlarmsCreatedInTheDay) {
    this.terminalAlarmsCreatedInTheDay = terminalAlarmsCreatedInTheDay;
  }

  public ResponseCode getResponseCode(){
    return ResponseCode.getResponseCode(responseCode);
  }

  public String getResponseCodeDescription() {
    return responseCodeDescription;
  }

  public List<LayoutSiteTask> getAvailableTasks() {
    return availableTasks;
  }

  public List<Long> getRoles() {
    return roles;
  }

  public List<LayoutSiteAlarm> getTerminalAlarmsCreatedInTheDay() {
    return terminalAlarmsCreatedInTheDay;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  @Override
  public Object getProperty(int i) {
    switch (i){
      case 0:
        return responseCode;
      case 1:
        return responseCodeDescription;
      case 2:
        return sessionId;
      case 3:
        return availableTasks;
      case 4:
        return beaconsAvailables;
      case 5:
        return lastVersionApp;
      case 6:
        return roles;
      case 7:
        return terminalAlarmsCreatedInTheDay;
      case 8:
        return userId;

      default:
        return null;
    }
  }

  @Override
  public int getPropertyCount() {
    return 9;
  }

  @Override
  public void setProperty(int i, Object o) {
    switch(i)
    {
      case 0:
        responseCode = Integer.parseInt(o.toString());
        break;
      case 1:
        responseCodeDescription = o.toString();
        break;
      case 2:
        sessionId = o.toString();
        break;
      case 3:
        availableTasks = (List<LayoutSiteTask>)o;
        break;
      case 4:
        beaconsAvailables = (List<BeaconAvailable>)o;
        break;
      case 5:
        lastVersionApp = o.toString();
        break;
      case 6:
        roles = (List<Long>)o;
        break;
      case 7:
        terminalAlarmsCreatedInTheDay = (List<LayoutSiteAlarm>)o;
        break;
      case 8:
        userId = Long.parseLong(o.toString());
        break;

      default:
        break;
    }
  }

  @Override
  public void getPropertyInfo(int i, Hashtable hashtable, PropertyInfo propertyInfo) {
    switch(i)
    {
      case 0:
        propertyInfo.type = PropertyInfo.INTEGER_CLASS;
        propertyInfo.name = "ResponseCode";
        break;
      case 1:
        propertyInfo.type = PropertyInfo.STRING_CLASS;
        propertyInfo.name = "ResponseCodeDescription";
        break;
      case 2:
        propertyInfo.type = PropertyInfo.STRING_CLASS;
        propertyInfo.name = "SessionId";
        break;
      case 3:
        propertyInfo.type = PropertyInfo.VECTOR_CLASS;
        propertyInfo.name = "AssignedTasks";
        break;
      case 4:
        propertyInfo.type = PropertyInfo.VECTOR_CLASS;
        propertyInfo.name = "BeaconsAvailables";
        break;
      case 5:
        propertyInfo.type = PropertyInfo.STRING_CLASS;
        propertyInfo.name = "LastVersionApp";
        break;
      case 6:
        propertyInfo.type = PropertyInfo.VECTOR_CLASS;
        propertyInfo.name = "Roles";
        break;
      case 7:
        propertyInfo.type = PropertyInfo.VECTOR_CLASS;
        propertyInfo.name = "TerminalAlarmsCreatedInTheDay";
        break;
      case 8:
        propertyInfo.type = PropertyInfo.LONG_CLASS;
        propertyInfo.name = "UserId";
        break;
      default:break;
    }
  }
}
