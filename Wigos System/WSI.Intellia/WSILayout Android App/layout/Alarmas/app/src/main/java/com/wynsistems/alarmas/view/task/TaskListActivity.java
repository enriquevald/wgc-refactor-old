package com.wynsistems.alarmas.view.task;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.wynsistems.alarmas.R;
import com.wynsistems.alarmas.model.SummarizedAlarms;
import com.wynsistems.alarmas.repository.factory.SummarizedAlarmsFactory;
import com.wynsistems.alarmas.service.ServiceFactory;
import com.wynsistems.alarmas.service.alarm.AlarmService;
import com.wynsistems.alarmas.service.mqtt.PahoMqttService;
import com.wynsistems.alarmas.view.setting.SettingsActivity;
import com.wynsistems.alarmas.view.utils.Constants;
import com.wynsistems.alarmas.view.utils.KioskModeActivity;

import java.util.List;


public class TaskListActivity extends KioskModeActivity implements SharedPreferences.OnSharedPreferenceChangeListener{

    private SharedPreferences prefs;

    private ProgressDialog dialog;
    private static String alarmServerUrl = "";
    private static String loggedUserAlarmServiceAddress;
    private String userId;
    private SwipeRefreshLayout pullToRefresh;
    private IntentFilter intentFilter = new IntentFilter(PahoMqttService.NEW_TASK_ARRIVE);
    private BroadcastReceiver tasksUpdatedBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            TaskListActivity.this.refreshView();
        }
    };

    private void refreshView() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                fillAlarmListView();
            }
        });
    }
    @Override
    protected void onStart() {
        super.onStart();
        fillAlarmListView();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_list);
        setTitle(R.string.title_activity_task_list);

        prefs = PreferenceManager.getDefaultSharedPreferences(this);//get the preferences that are allowed to be given
        prefs.registerOnSharedPreferenceChangeListener(this);//set the listener to listen for changes in the preferences

        SharedPreferences settings = TaskListActivity.this.getSharedPreferences(Constants.USER_ID_PREFERENCE_EDITOR, 0);
        userId = settings.getString(Constants.USER_ID, "");

        alarmServerUrl = prefs.getString("alarm_server_address", alarmServerUrl);

        buildDialog();

        SharedPreferences.Editor editor = settings.edit();
        editor.putString(Constants.LOGGED_USER_ALARM_SERVICE_ADDRESS, alarmServerUrl);
        editor.apply();

        intentFilter.addAction(Constants.UPDATED_TASKS);
        intentFilter.addAction(PahoMqttService.NEW_TASK_ARRIVE);

        pullToRefresh = (SwipeRefreshLayout) findViewById(R.id.pullToRefresh);
        pullToRefresh.setColorSchemeColors(R.color.winsystem_blue,
                R.color.winsystem_red,
                R.color.winsystem_yellow);

        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                new UpdateTasksAsync().execute();
            }
        });

        loggedUserAlarmServiceAddress = alarmServerUrl;
    }


    @Override
    public void onResume(){
        super.onResume();
        fillAlarmListView();
        registerReceiver(tasksUpdatedBroadcastReceiver, intentFilter);
    }
    @Override
    protected void onPause() {
        this.dialog.dismiss();
        super.onPause();
        prefs.unregisterOnSharedPreferenceChangeListener(this);
        unregisterReceiver(tasksUpdatedBroadcastReceiver);
    }

    @Override
    protected void onStop() {
        this.dialog.dismiss();
        super.onStop();
        prefs.unregisterOnSharedPreferenceChangeListener(this);
    }


    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent i = new Intent(this, SettingsActivity.class);
            startActivityForResult(i, 1);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void buildDialog() {
        this.dialog = new ProgressDialog(TaskListActivity.this);
        this.dialog.setTitle(TaskListActivity.this.getResources().getString(R.string.title_progress_dialog));
        this.dialog.setMessage(TaskListActivity.this.getResources().getString(R.string.alarm_message_progress_dialog));
    }



    private void fillAlarmListView() {
        ListView alarmListView = (ListView) findViewById( R.id.taskDetailList);
        SummarizedAlarmsFactory summarizedAlarmsFactory = SummarizedAlarmsFactory.getInstance(loggedUserAlarmServiceAddress, getApplicationContext());
        List<SummarizedAlarms> listAlarm;

        switch (groupPreference) {
            case CRITICALITY:
                listAlarm = summarizedAlarmsFactory.getSummarizedAlarmsByCriticality(this.sortPreference, this);
                break;
            case CATEGORY:
                listAlarm = summarizedAlarmsFactory.getSummarizedAlarmsByCategory(this.sortPreference, this);
                break;
            default:
                listAlarm = summarizedAlarmsFactory.getSummarizedAlarmsByCompleted(this.sortPreference, this);
                break;
        }

        final ListAdapter listAdapter = new SummarizedTaskListAdapter(this, listAlarm, groupPreference);

        alarmListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SummarizedAlarms item = (SummarizedAlarms) listAdapter.getItem(position);

                Intent intent = new Intent(TaskListActivity.this,TasksByCriticalityActivity.class);
                intent.putExtra(Constants.SELECTED_GROUP_ALARMS, item);
                startActivity(intent);
            }
        });

        alarmListView.setAdapter(listAdapter);
    }

    private class UpdateTasksAsync extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pullToRefresh.setRefreshing(true);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                AlarmService service = ServiceFactory.getInstance().getAlarmService(getApplicationContext());
                service.setUrl(alarmServerUrl);
                service.getAll(userId, getApplicationContext());
                return true;
            }catch (Exception e)
            {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if(result)
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        fillAlarmListView();
                    }
                });

            pullToRefresh.setRefreshing(false);
        }
    }


}
