package com.wynsistems.alarmas.repository;

import android.content.Context;

import com.wynsistems.alarmas.helpers.SaveInformationHelper;
import com.wynsistems.alarmas.model.LayoutSiteItemGroupCategory;
import com.wynsistems.alarmas.model.LayoutSiteItemSeverity;
import com.wynsistems.alarmas.model.LayoutSiteItemSubCategory;
import com.wynsistems.alarmas.model.LayoutSiteTaskActions;
import com.wynsistems.alarmas.service.login.response.LayoutSiteTask;
import com.wynsistems.alarmas.view.utils.Constants;

import java.util.ArrayList;
import java.util.List;

public class DefaultTaskRepository implements TaskRepository {
    private static DefaultTaskRepository instance = null;

    private static List<LayoutSiteTask> layoutSiteTasks;
    private static SaveInformationHelper<LayoutSiteTask> saveLayoutSiteTask;

    private DefaultTaskRepository(Context context){
        this.layoutSiteTasks = new ArrayList<>();
        this.saveLayoutSiteTask  = new SaveInformationHelper<>(context);
    }

    public static TaskRepository getInstance(Context context) {
        if(instance == null){
            instance = new DefaultTaskRepository(context);
            layoutSiteTasks = saveLayoutSiteTask.readList(Constants.TASKS_ASSIGNED);
        }
        return instance;
    }

    @Override
    public LayoutSiteTask getById(String id) {
        List<LayoutSiteTask> alarms = this.layoutSiteTasks;
        int i = 0;
        boolean founded = false;
        LayoutSiteTask currentTasks = null;
        while(i<alarms.size() && !founded){
            currentTasks = alarms.get(i);
            if(currentTasks.getId() == Integer.parseInt(id)){
                founded = true;
            }
            i++;
        }
        if(!founded){
            currentTasks = null;
        }
        return currentTasks;
    }


    @Override
    public void saveFromAlarmResponse(List<LayoutSiteTask> tasks, Context context){

        saveLayoutSiteTask.writeList(tasks, Constants.TASKS_ASSIGNED);
        layoutSiteTasks = tasks;
    }

    @Override
    public List<LayoutSiteTask> getAlarmByCriticality(LayoutSiteItemSeverity severity)
    {
        List<LayoutSiteTask> tasksByCriticality = new ArrayList<>();

        for (LayoutSiteTask t : this.layoutSiteTasks)
        {
            if(LayoutSiteItemSeverity.getById(t.getSeverity()) == severity)
                tasksByCriticality.add(t);
        }

        return tasksByCriticality;
    }

    @Override
    public List<LayoutSiteTask> getAlarmByCategory(LayoutSiteItemGroupCategory type) {

        List<LayoutSiteTask> tasksByCategory = new ArrayList<>();

        for (LayoutSiteTask t : this.layoutSiteTasks)
        {
            if(LayoutSiteItemSubCategory.forCode(t.getSubcategory()).getGroupCategory() == type)
                tasksByCategory.add(t);
        }

        return tasksByCategory;
    }

    @Override
    public List<LayoutSiteTask> getAlarmsInProgress() {
        List<LayoutSiteTask> tasksInProgress = new ArrayList<>();

        for (LayoutSiteTask t : this.layoutSiteTasks)
        {
            if(LayoutSiteTaskActions.GetActionLayoutSiteTaskAction(t.getStatus()) == LayoutSiteTaskActions.Accepted)
                tasksInProgress.add(t);
        }

        return tasksInProgress;
    }

    @Override
    public List<LayoutSiteTask> getAlarmsAssigned() {
        List<LayoutSiteTask> tasksAssigned = new ArrayList<>();

        for (LayoutSiteTask t : this.layoutSiteTasks)
        {
            if(LayoutSiteTaskActions.GetActionLayoutSiteTaskAction(t.getStatus()) == LayoutSiteTaskActions.Assigned)
                tasksAssigned.add(t);
        }

        return tasksAssigned;
    }
}
