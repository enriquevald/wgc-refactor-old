package com.winsystems.smartfloor.view.notifications;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.winsystems.smartfloor.R;
import com.winsystems.smartfloor.common.IntentExtra;
import com.winsystems.smartfloor.common.LayoutNotification;
import com.winsystems.smartfloor.common.LayoutNotificationManager;
import com.winsystems.smartfloor.common.LayoutNotificationType;
import com.winsystems.smartfloor.view.menu.MenuActivity;
import com.winsystems.smartfloor.view.message.ConversationActivity;
import com.winsystems.smartfloor.view.task.TaskDetailActivity;
import com.winsystems.smartfloor.view.utils.Constants;
import com.winsystems.smartfloor.view.utils.KioskModeActivity;

import java.util.List;

public class ListNotificationsActivity extends ActionBarActivity
{
  private  ListView listView;
  private LayoutNotificationType type;
  private ListAdapter listAdapter;
  private LayoutNotificationManager notificationManager;
  private IntentFilter newNotificationFilter = new IntentFilter(Constants.NEW_TASK_ARRIVE);
  private BroadcastReceiver newNotificationBroadcastReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      buildNotificationList();
    }
  };
  @Override
  protected void onCreate(Bundle savedInstanceState) {

    super.onCreate(savedInstanceState);
    setContentView(R.layout.notification_list_activity);
    type = (LayoutNotificationType)getIntent().getSerializableExtra(Constants.NOTIFICATION_TYPE);
    listView = (ListView) findViewById( R.id.notification_list);
    if(type == LayoutNotificationType.MESSAGE)
      setTitle(getString(R.string.messages_notification));
    else
      setTitle(getString(R.string.tasks_notifications));

  }

  @Override
  public void onResume()    {
    super.onResume();
    buildNotificationList();
    registerReceiver(newNotificationBroadcastReceiver, newNotificationFilter);
  }
  @Override
  public void onPause(){
    super.onPause();
    unregisterReceiver(newNotificationBroadcastReceiver);
  }

  private void buildNotificationList() {
    notificationManager = new LayoutNotificationManager(getApplicationContext());
    List<LayoutNotification> notificationItemList = notificationManager.getNotificationsByType(type);

    if(notificationItemList.size() == 0){
      Intent goToMain = new Intent(ListNotificationsActivity.this, MenuActivity.class);
      startActivity(goToMain);
    }else {
      listAdapter = new NotificationListAdapter(this, notificationItemList, type);
      listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
          LayoutNotification item = (LayoutNotification) listAdapter.getItem(position);
          Class<? extends KioskModeActivity> aClass = item.getType() == LayoutNotificationType.MESSAGE ? ConversationActivity.class : TaskDetailActivity.class;
          Intent intent = new Intent(getApplicationContext(), aClass);
          List<IntentExtra> extras = item.getIntent();
          for (IntentExtra e : extras) {
            intent.putExtra(e.getName(), e.getValue());
          }
          intent.putExtra(Constants.NOTIFICATION_ID, item.getId());
          startActivity(intent);
        }
      });

      listView.setAdapter(listAdapter);
    }
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu items for use in the action bar
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.notification_panel_menu, menu);
    return super.onCreateOptionsMenu(menu);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle presses on the action bar items
    switch (item.getItemId()) {
      case R.id.action_clear:
        runOnUiThread(new Runnable() {
          @Override
          public void run() {
            notificationManager.removeNotificationsByType(type);
            listAdapter = new NotificationListAdapter(getApplicationContext(), notificationManager.getNotificationsByType(type), type);
            listView.setAdapter(listAdapter);
          }
        });
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }
}
