package com.winsystems.smartfloor.repository;

import android.content.Context;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: RepositoryFactory
//
// DESCRIPTION: Implements methods for alarm repository factory.
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public class RepositoryFactory {
  private static RepositoryFactory instance = null;

  private RepositoryFactory() {}

  public static RepositoryFactory getInstance(){
    if(instance== null){
      instance = new RepositoryFactory();
    }
    return instance;
  }

  public TaskRepository getAlarmRepository(Context context){
    return DefaultTaskRepository.getInstance(context);
  }
}
