package com.wynsistems.alarmas.service.message.request;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.util.Hashtable;

public class DATA_GetConversationsOfTodayRequest implements KvmSerializable {

    private String session;

    public void setSession(String session) {
        this.session = session;
    }

    @Override
    public Object getProperty(int i) {
        switch (i){
            case 0:
                return session;
            default:
                return null;
        }
    }

    @Override
    public int getPropertyCount() {
        return 1;
    }

    @Override
    public void setProperty(int i, Object o) {

        switch (i){
            case 0:
                session =  o.toString();
                break;
        }

    }

    @Override
    public void getPropertyInfo(int i, Hashtable hashtable, PropertyInfo propertyInfo) {
        switch (i){
            case 0:
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                propertyInfo.name = "SessionId";
                break;
        }
    }
}