package com.winsystems.smartfloor.helpers;

import android.content.Context;

import com.winsystems.smartfloor.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: DisplayDateHelper
//
// DESCRIPTION: Class to handle dates formats.
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public class DisplayDateHelper {

    private static SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    public static String getDateToShowInMessages(Date dateToShow){
            Calendar calendarFromDate = Calendar.getInstance();
            calendarFromDate.setTime(dateToShow);


            Calendar calendarToday = Calendar.getInstance();
            if(calendarFromDate.get(Calendar.DAY_OF_YEAR) == calendarToday.get(Calendar.DAY_OF_YEAR)){
                SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
                return timeFormat.format(calendarFromDate.getTime());
            }else if(calendarFromDate.get(Calendar.WEEK_OF_YEAR) == calendarToday.get(Calendar.WEEK_OF_YEAR)){
                SimpleDateFormat dayAndTimeFormat = new SimpleDateFormat("EEE HH:mm");
                return dayAndTimeFormat.format(calendarFromDate.getTime());
            }else{
                SimpleDateFormat fullDate = new SimpleDateFormat("dd/MM/yyyy HH:mm");
                return fullDate.format(calendarFromDate.getTime());
            }
        }

    public static String getDateToShowInTasks(Date dateToShow, Context _context){

        Calendar calendarFromDate = Calendar.getInstance();
        calendarFromDate.setTime(dateToShow);

        Calendar calendarToday = Calendar.getInstance();
        if(calendarFromDate.get(Calendar.DAY_OF_YEAR) == calendarToday.get(Calendar.DAY_OF_YEAR)){
            if(calendarFromDate.get(Calendar.HOUR) == calendarToday.get(Calendar.HOUR)){
                if(calendarFromDate.get(Calendar.MINUTE) == calendarToday.get(Calendar.MINUTE)){
                    int secondsDiff = calendarToday.get(Calendar.SECOND) - calendarFromDate.get(Calendar.SECOND);
                    return String.format(_context.getResources().getString(R.string.task_seconds_ago),secondsDiff);
                }else{
                    int minutesDiff = calendarToday.get(Calendar.MINUTE) - calendarFromDate.get(Calendar.MINUTE);
                    return String.format(_context.getResources().getString(R.string.task_minutes_ago),minutesDiff);
                }
            }else{
                int hourDiff = calendarToday.get(Calendar.HOUR) - calendarFromDate.get(Calendar.HOUR);
                return String.format(_context.getResources().getString(R.string.task_hours_ago),hourDiff);
            }
        }else if(calendarFromDate.get(Calendar.WEEK_OF_YEAR) == calendarToday.get(Calendar.WEEK_OF_YEAR)){
            int daysDiff = calendarToday.get(Calendar.DAY_OF_WEEK) - calendarFromDate.get(Calendar.DAY_OF_WEEK);
            if(daysDiff > 1)
                return String.format(_context.getResources().getString(R.string.task_days_ago),daysDiff);
            return String.format(_context.getResources().getString(R.string.task_days_ago),daysDiff);
        }else{
            SimpleDateFormat fullDate = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            return fullDate.format(calendarFromDate.getTime());
        }
    }
}

