package com.winsystems.smartfloor.view.update;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Base64;
import android.util.Log;

import com.winsystems.smartfloor.R;
import com.winsystems.smartfloor.common.AndroidLogger;
import com.winsystems.smartfloor.helpers.LogHelper;
import com.winsystems.smartfloor.service.ServiceFactory;
import com.winsystems.smartfloor.service.update.response.DATA_UpdateApplicationResponse;
import com.winsystems.smartfloor.view.menu.MenuActivity;
import com.winsystems.smartfloor.view.utils.Constants;
import com.winsystems.smartfloor.view.utils.KioskModeActivity;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

public class UpdateApplicationActivity extends KioskModeActivity{
    private ProgressDialog dialog;
    private String url;
    private String sessionId;
    private static AndroidLogger mLog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mLog = LogHelper.getLogger(this);

        setContentView(R.layout.activity_update);
        setTitle(R.string.title_activity_update);

        SharedPreferences settings = this.getSharedPreferences(Constants.USER_ID_PREFERENCE_EDITOR, 0);
        sessionId = settings.getString(Constants.USER_ID, "");

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);//get the preferences that are allowed to be given
        url = prefs.getString("alarm_server_address", url);

    }
    @Override
    public void onResume(){
        super.onResume();

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(UpdateApplicationActivity.this);
                builder.setTitle(R.string.new_version_available);
                builder.setMessage(R.string.new_version_available_text);
                builder.setCancelable(false);
                builder.setPositiveButton(R.string.title_activity_update, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        new AsyncUpdateApplication().execute();
                    }
                });
                builder.show();
            }
        });
    }

    private void buildDialog() {
        this.dialog = new ProgressDialog(this);
        this.dialog.setTitle(this.getResources().getString(R.string.new_version_available));
        this.dialog.setMessage(this.getResources().getString(R.string.update_to_lastest_version_title));
        this.dialog.setCanceledOnTouchOutside(false);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case Constants.APP_INSTALL_REQUEST:
                if (resultCode == RESULT_OK) {
                    mLog.info("PhotoActivity // Package Installation Success");
                } else if (resultCode == RESULT_FIRST_USER) {
                    mLog.info("PhotoActivity // Package Installation Cancelled by USER");
                } else {
                    mLog.info("PhotoActivity // Something went wrong - INSTALLATION FAILED");
                }
        }
    }

    private class AsyncUpdateApplication extends AsyncTask<Void, Void, Boolean> {
        String pathToApk;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            buildDialog();
            UpdateApplicationActivity.this.dialog.show();
        }


        @Override
        public Boolean doInBackground(Void... params) {
            Context _context = getApplicationContext();
            //File file = _context.getFileStreamPath("SITE_TASK_22.apk");
            try {
                //if (file.exists() || file.createNewFile()) {
                //  pathToApk = file.getAbsolutePath();
                DATA_UpdateApplicationResponse response = ServiceFactory.getInstance().getUpdateService(getApplicationContext()).getLastestApplicationVersion(url, sessionId);

                String PATH = Environment.getExternalStorageDirectory() + "/download/";
                File file = new File(PATH);
                file.mkdirs();
                File outputFile = new File(file, "app.apk");
                FileOutputStream fos = new FileOutputStream(outputFile);

                InputStream is =  new ByteArrayInputStream(Base64.decode(response.getVersion(), Base64.DEFAULT));;

                byte[] buffer = new byte[1024];
                int len1 = 0;
                while ((len1 = is.read(buffer)) != -1) {
                    fos.write(buffer, 0, len1);
                }
                fos.close();
                is.close();//till here, it works fine - .apk is download to my sdcard in download file

                pathToApk = PATH + "app.apk";
//
//                    byte[] bytes = Base64.decode(response.getVersion(), Base64.DEFAULT);
//                    OutputStream output = new FileOutputStream(pathToApk);
//                    output.write(bytes);
//
//                    output.flush();
//                    output.close();
                return true;
                //}
                //return false;
            }
            catch (Exception e) {
                mLog.error("", e);
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            if(result){
                Intent i = new Intent();
                i.setAction(Intent.ACTION_VIEW);
                i.setDataAndType(Uri.fromFile(new File(pathToApk)), "application/vnd.android.package-archive");
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mLog.info("Lofting // About to install new .apk");
                startActivity(i);
            }
            else
            {
                Intent main = new Intent(UpdateApplicationActivity.this, MenuActivity.class);
                main.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(main);
            }
        }
    }
}
