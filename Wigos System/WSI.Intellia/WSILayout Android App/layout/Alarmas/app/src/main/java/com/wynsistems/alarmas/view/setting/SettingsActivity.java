package com.wynsistems.alarmas.view.setting;

import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceScreen;
import android.support.v7.app.ActionBarActivity;

import com.wynsistems.alarmas.R;
import com.wynsistems.alarmas.view.utils.KioskModeActivity;

import java.util.Locale;

public class SettingsActivity extends KioskModeActivity {


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.title_activity_settigns);
        // Display the fragment as the main content.
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingsFragment())
                .commit();


    }
}
