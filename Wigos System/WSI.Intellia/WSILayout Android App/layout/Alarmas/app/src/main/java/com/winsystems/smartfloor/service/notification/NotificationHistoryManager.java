package com.winsystems.smartfloor.service.notification;

import android.content.Context;

import com.winsystems.smartfloor.helpers.SaveInformationHelper;

import java.util.List;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: NotificationHistoryManager
//
// DESCRIPTION: Implement methods of service for get conversation history and today.
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public class NotificationHistoryManager {

  private SaveInformationHelper<String> notificationRepository;

  public NotificationHistoryManager(Context context){
    notificationRepository = new SaveInformationHelper<>(context);
  }

  public List<String> getNotificationsByKey(String managerId) {
    return notificationRepository.readList("NotificationsFromUser" + managerId);
  }

  public void AddNotification(String managerId, String content) {
    List<String> notifications =  notificationRepository.readList("NotificationsFromUser" + managerId);
    notifications.add(content);
    notificationRepository.writeList(notifications, "NotificationsFromUser" + managerId);
  }

  public void ClearNotifications(String managerId) {
    notificationRepository.clearList("NotificationsFromUser" + managerId);
  }

  public boolean AvailableNotificationsWithSameKey(String managerId) {

    return notificationRepository.readList("NotificationsFromUser" + managerId).size() > 0;
  }
}
