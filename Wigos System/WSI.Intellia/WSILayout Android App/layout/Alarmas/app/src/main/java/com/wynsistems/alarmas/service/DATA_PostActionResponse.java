package com.wynsistems.alarmas.service;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.util.Hashtable;

public class DATA_PostActionResponse implements KvmSerializable{

    private String sessionId;
    private Integer responseCode;
    private String responseCodeDescription;
    private ResponseCode enumResponseCode;

    public DATA_PostActionResponse() {
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {

        this.sessionId = sessionId;
    }

    public void setResponseCode(Integer responseCode) {
        this.responseCode = responseCode;
    }

    public void setResponseCodeDescription(String responseCodeDescription) {
        this.responseCodeDescription = responseCodeDescription;
    }

    public ResponseCode getResponseCode(){
        return ResponseCode.getResponseCode(responseCode);
    }

    public String getResponseCodeDescription() {
        return responseCodeDescription;
    }

    @Override
    public Object getProperty(int i) {
        switch (i){
            case 0:
                return responseCode;
            case 1:
                return responseCodeDescription;
            case 2:
                return sessionId;
            default:
                return null;
        }
    }

    @Override
    public int getPropertyCount() {
        return 3;
    }

    @Override
    public void setProperty(int i, Object o) {
        switch(i)
        {
            case 0:
                responseCode = Integer.parseInt(o.toString());
                break;
            case 1:
                responseCodeDescription = o.toString();
                break;
            case 2:
                sessionId = o.toString();
                break;
            default:
                break;
        }
    }

    @Override
    public void getPropertyInfo(int i, Hashtable hashtable, PropertyInfo propertyInfo) {
        switch(i)
        {
            case 0:
                propertyInfo.type = PropertyInfo.INTEGER_CLASS;
                propertyInfo.name = "ResponseCode";
                break;
            case 1:
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                propertyInfo.name = "ResponseCodeDescription";
                break;
            case 2:
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                propertyInfo.name = "SessionId";
                break;
            default:break;
        }
    }
}
