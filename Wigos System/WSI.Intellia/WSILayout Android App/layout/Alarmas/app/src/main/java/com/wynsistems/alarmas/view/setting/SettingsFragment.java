package com.wynsistems.alarmas.view.setting;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceCategory;
import android.preference.PreferenceFragment;
import android.util.DisplayMetrics;

import com.wynsistems.alarmas.R;
import com.wynsistems.alarmas.helpers.IsLastestVersionHelper;
import com.wynsistems.alarmas.helpers.LanguageHelper;

import java.util.Locale;


public class SettingsFragment extends PreferenceFragment {
    private String lang;
    private SharedPreferences prefs;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.fragment_settings);

        Preference updatePreference = findPreference("update-lastest-version");
        PreferenceCategory preferenceCategory = (PreferenceCategory) findPreference("advanced_settings");
        preferenceCategory.removePreference(updatePreference);

        if(!IsLastestVersionHelper.IsLastestVersion(getActivity().getApplicationContext())) {
            updatePreference.setIcon(R.drawable.ic_system_update_alt_black_18dp);
            preferenceCategory.addPreference(updatePreference);
        }

        LanguageHelper.setCurrentLocale(Locale.getDefault(), this);
        prefs = getActivity().getSharedPreferences("CommonPrefs", Activity.MODE_PRIVATE);
        lang = prefs.getString("languageToLoad", Locale.getDefault().getDisplayLanguage());

        Preference languageButton = findPreference("language");
        languageButton.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                updateLanguageIfNeeded(newValue);
                restartParentActivityIfLanguageChanged();
                return true;
            }
        });
    }

    private void updateLanguageIfNeeded(Object newValue) {
        Locale newLocale = new Locale(LanguageHelper.getLanguageByValue(Integer.valueOf(newValue.toString())));
        Locale.setDefault(newLocale);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = newLocale;
        res.updateConfiguration(conf, dm);
        Preference language = findPreference("language");
        language.setDefaultValue(newValue);
    }

    private void restartParentActivityIfLanguageChanged() {
        String oldLanguage = lang;
        lang = prefs.getString("languageToLoad", Locale.getDefault().getDisplayLanguage());
        if (!oldLanguage.equals(lang)){
            getActivity().finish();
            startActivity(getActivity().getIntent());
        }
    }
}
