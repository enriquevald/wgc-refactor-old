package com.wynsistems.alarmas.model;

public enum AlarmGroupingCriteria {
    CRITICALITY,
    CATEGORY,
    COMPLETED;

    public static AlarmGroupingCriteria getFromString(String i){
        switch (i){
            case "1":
                return CRITICALITY;
            case "2":
                return CATEGORY;
            case "3":
                return COMPLETED;
            default:
                return CRITICALITY;
        }
    }
}
