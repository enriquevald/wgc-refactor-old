package com.winsystems.smartfloor.view.log;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ListView;

import com.winsystems.smartfloor.R;
import com.winsystems.smartfloor.helpers.SaveInformationHelper;
import com.winsystems.smartfloor.service.ServiceFactory;
import com.winsystems.smartfloor.service.log.LogService;
import com.winsystems.smartfloor.view.utils.Constants;
import com.winsystems.smartfloor.view.utils.KioskModeActivity;

import java.util.List;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: LogViewer
//
// DESCRIPTION: Implements list of log.
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public class LogViewer extends KioskModeActivity {

    private SwipeRefreshLayout pullToRefresh;
    private  ListView logListView;
    private List<String> logList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_viewer);
        LoadLogFileToArrayList();
        logListView = (ListView) findViewById(R.id.logListView);
        logListView.setAdapter(new LogListAdapter(getApplicationContext(), logList));
        logListView.setSelection(logList.size() - 1);

        pullToRefresh = (SwipeRefreshLayout) findViewById(R.id.pullToRefresh);
        pullToRefresh.setColorSchemeColors(R.color.winsystem_blue,
                                           R.color.winsystem_red,
                                           R.color.winsystem_yellow);

        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                new UpdateLogAsync().execute();
            }
        });
    }

    private void LoadLogFileToArrayList() {
        logList = new SaveInformationHelper<String>(getApplicationContext()).readList(Constants.LOG);

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    protected void onStop() {
        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.log_activity_actions_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_clear_log:
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        SaveInformationHelper<String> helper = new SaveInformationHelper<String>(getApplicationContext());
                        helper.clearList(Constants.LOG);
                        logList = helper.readList(Constants.LOG);
                        logListView.setAdapter(new LogListAdapter(getApplicationContext(), logList));
                        logListView.setSelection(logList.size() - 1);
                    }
                });
                return true;
            case R.id.action_refresh_log:
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        logListView.setAdapter(new LogListAdapter(getApplicationContext(), logList));
                        logListView.setSelection(logList.size() - 1);
                    }
                });
                return true;
            case R.id.action_send_log:
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            new LogSender().execute();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private class UpdateLogAsync extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pullToRefresh.setRefreshing(true);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                LoadLogFileToArrayList();
                return true;
            }catch (Exception e)
            {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if(result)
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        logListView.setAdapter(new LogListAdapter(getApplicationContext(), logList));
                        logListView.setSelection(logList.size() - 1);
                    }
                });

            pullToRefresh.setRefreshing(false);
        }


    }

    private class LogSender extends AsyncTask<String,Void, Boolean> {
        private String topic;
        private String message;

        @Override
        protected Boolean doInBackground(String... params) {

            try {
                LogService service = ServiceFactory.getInstance().getLogService(getApplicationContext());
                service.saveLog();
                return true;
            } catch (Exception e){
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean result){
            if(result)
            {
                // No actions by now
            }
        }

    }


}


