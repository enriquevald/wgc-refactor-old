package com.winsystems.smartfloor.service.mqtt;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.provider.Settings.Secure;
import android.support.v4.app.NotificationCompat;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;
import com.estimote.sdk.Utils;
import com.winsystems.smartfloor.R;
import com.winsystems.smartfloor.common.AndroidLogger;
import com.winsystems.smartfloor.common.IntentExtra;
import com.winsystems.smartfloor.common.LayoutNotification;
import com.winsystems.smartfloor.common.LayoutNotificationManager;
import com.winsystems.smartfloor.common.LayoutNotificationType;
import com.winsystems.smartfloor.common.TrustManagerManipulator;
import com.winsystems.smartfloor.helpers.AlarmResourseHelper;
import com.winsystems.smartfloor.helpers.LayoutPendingMessage;
import com.winsystems.smartfloor.helpers.LogHelper;
import com.winsystems.smartfloor.helpers.PendingToReadMessagesHelper;
import com.winsystems.smartfloor.helpers.SaveInformationHelper;
import com.winsystems.smartfloor.model.LayoutSiteItemSubCategory;
import com.winsystems.smartfloor.service.DATA_PostActionResponse;
import com.winsystems.smartfloor.service.ResponseCode;
import com.winsystems.smartfloor.service.ServiceFactory;
import com.winsystems.smartfloor.service.alarm.AlarmService;
import com.winsystems.smartfloor.service.log.LogService;
import com.winsystems.smartfloor.service.login.response.BeaconAvailable;
import com.winsystems.smartfloor.service.login.response.LayoutSiteTask;
import com.winsystems.smartfloor.service.mqtt.request.DATA_BeaconRunnerDistance;
import com.winsystems.smartfloor.service.notification.NotificationHistoryManager;
import com.winsystems.smartfloor.view.login.LoginActivity;
import com.winsystems.smartfloor.view.message.ConversationActivity;
import com.winsystems.smartfloor.view.update.UpdateApplicationActivity;
import com.winsystems.smartfloor.view.utils.Constants;

import org.apache.commons.math3.fitting.leastsquares.LeastSquaresOptimizer;
import org.apache.commons.math3.fitting.leastsquares.LevenbergMarquardtOptimizer;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.eclipse.paho.client.mqttv3.MqttTopic;
import org.eclipse.paho.client.mqttv3.persist.MqttDefaultFilePersistence;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: PahoMqttService
//
// DESCRIPTION: Mqtt Client.
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public class PahoMqttService extends Service implements MqttCallback
{
    //   application preferences
    public static final String      APP_ID = "winsystems.mqtt";

    public static final String 		DEBUG_TAG = "MqttService"; // Debug TAG

    private static final String		MQTT_THREAD_NAME = "MqttService[" + DEBUG_TAG + "]"; // Handler Thread ID
    public static final String      NEW_TASK_ARRIVE = Constants.NEW_TASK_ARRIVE;
    public static final String      NEW_MESSAGE_FROM_MANAGER_ARRIVE = "NEW MESSAGE FROM MANAGER ARRIVE";
    public static final String      NEW_VERSION_AVAILABLE = Constants.NEW_VERSION_AVAILABLE;
    public static final String      LOG_REQUESTED = Constants.GETLOG_REQUEST;
    private static String 	        MQTT_TOPIC = "*"; // Broker URL or IP Address
    private static String 	        MQTT_BROKER = "*"; // Broker URL or IP Address
    private static final int 		MQTT_PORT = 1883;				// Broker Port

    public static final int			MQTT_QOS_0 = 0; // QOS Level 0 ( Delivery Once no confirmation )

    private static final int 		MQTT_KEEP_ALIVE = 3 * 60000; // KeepAlive Interval in MS
    private static final String		MQTT_KEEP_ALIVE_TOPIC_FORAMT = "/users/%s/keepalive"; // Topic format for KeepAlives
    private static final byte[] 	MQTT_KEEP_ALIVE_MESSAGE = { 0 }; // Keep Alive message to send
    private static final int		MQTT_KEEP_ALIVE_QOS = MQTT_QOS_0; // Default Keepalive QOS

    private static final boolean 	MQTT_CLEAN_SESSION = true; // Start a clean session?

    private static final String 	MQTT_URL_FORMAT = "tcp://%s:%d"; // URL Format normally don't change

    private static final String 	ACTION_START 	 = DEBUG_TAG + ".START"; // Action to start
    private static final String 	ACTION_STOP		 = DEBUG_TAG + ".STOP"; // Action to stop
    private static final String 	ACTION_KEEPALIVE = DEBUG_TAG + ".KEEPALIVE"; // Action to keep alive used by alarm manager
    private static final String 	ACTION_RECONNECT = DEBUG_TAG + ".RECONNECT"; // Action to reconnect

    private static String           MESSAGE_NOTIFICATION_TOPIC = "/Messages/";
    private static String           MESSAGE_STATUS_CHANGE_TOPIC = "/MessagesStatusChange/";
    private static final String     UPDATE_APPLICATION_NOTIFICATION_TOPIC = "/NewVersion/+";
    private static String           SAVE_LOG_TOPIC = "/GetLog/";

    private static final String 	DEVICE_ID_FORMAT = "andr_%s"; // Device ID Format, add any prefix you'd like
    // Note: There is a 23 character limit you will get
    // An NPE if you go over that limit
    private boolean mStarted = false; // Is the Client started?
    private boolean mStarting = false; //During the reconnection we must handle that one thread is working on it.
    private String mDeviceId;		  // Device ID, Secure.ANDROID_ID
    private Handler mConnHandler;	  // Seperate Handler thread for networking

    private MqttDefaultFilePersistence mDataStore; // Defaults to FileStore
    private MqttConnectOptions mOpts;			// Connection Options

    private MqttTopic mKeepAliveTopic;			// Instance Variable for Keepalive topic

    private MqttClient mClient;					// Mqtt Client

    private AlarmManager mAlarmManager;			// Alarm manager to perform repeating tasks
    private ConnectivityManager mConnectivityManager; // To check for connectivity changes

    private BeaconManager beaconManager;
    private Region region = new Region("ranged region", UUID.fromString("E2C56DB5-DFFB-48D2-B060-D0F5A71096E0"), 0, 0);
    private String url = "";
    private String sessionId = "";
    private boolean isRegisteredConnectivity = false;
    private NotificationHistoryManager notificationHistoryManager;
    private static AndroidLogger mLog;
    private static boolean lastIsConnected = true;
    private SaveInformationHelper<BeaconAvailable> beaconsList;

    /**
     * Start MQTT Client
     * @param 'Context context to start the service with
     * @return void
     */
    public static void actionStart(Context ctx) {
        Intent i = new Intent(ctx,PahoMqttService.class);
        i.setAction(ACTION_START);
        ctx.startService(i);
    }

    /**
     * Stop MQTT Client
     * @param 'Context context to start the service with
     * @return void
     */
    public static void actionStop(Context ctx) {
        Intent i = new Intent(ctx,PahoMqttService.class);
        i.setAction(ACTION_STOP);
        ctx.startService(i);
    }
    /**
     * Send a KeepAlive Message
     * @param 'Context context to start the service with
     * @return void
     */
    public static void actionKeepalive(Context ctx) {
        Intent i = new Intent(ctx,PahoMqttService.class);
        i.setAction(ACTION_KEEPALIVE);
        ctx.startService(i);
    }

    /**
     * Initalizes the DeviceId and most instance variables
     * Including the Connection Handler, Datastore, Alarm Manager
     * and ConnectivityManager.
     */
    @Override
    public void onCreate() {
        super.onCreate();
        mLog = LogHelper.getLogger(getApplicationContext());

        SharedPreferences settings = getSharedPreferences(APP_ID, MODE_PRIVATE);
        MQTT_BROKER = settings.getString("broker", "");
        MQTT_TOPIC      = settings.getString("topic", "");
        MESSAGE_NOTIFICATION_TOPIC = settings.getString("message-topic", "");
        SAVE_LOG_TOPIC = settings.getString("getlog-topic", "");
        String[] urlBroker = MQTT_BROKER.split("//");
        if(urlBroker.length > 1) {
            String ip = urlBroker[1].split(":")[0];

            MQTT_BROKER = ip;


            mDeviceId = String.format(DEVICE_ID_FORMAT,
                    Secure.getString(getContentResolver(), Secure.ANDROID_ID));

            HandlerThread thread = new HandlerThread(MQTT_THREAD_NAME);
            thread.start();

            mConnHandler = new Handler(thread.getLooper());
            mDataStore = new MqttDefaultFilePersistence(getCacheDir().getAbsolutePath());

            mOpts = new MqttConnectOptions();
            mOpts.setCleanSession(MQTT_CLEAN_SESSION);
            // Do not set keep alive interval on mOpts we keep track of it with alarm's

            mAlarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
            mConnectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);


            BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            if (mBluetoothAdapter == null) {
                mLog.info("Bluetooth is not operative");
            } else {
                if (mBluetoothAdapter.isEnabled()) {
                    beaconManager = new BeaconManager(getApplicationContext());
                    beaconManager.setRangingListener(new BeaconManager.RangingListener() {
                        @Override
                        public void onBeaconsDiscovered(Region region, List<Beacon> list) {
                            if (!list.isEmpty()) {
                                Beacon[] array = new Beacon[]{};
                                new PostBeaconRunnerDistanceInBackground().execute(list.toArray(array));
                            }
                        }
                    });
                }
            }
            notificationHistoryManager = new NotificationHistoryManager(getApplicationContext());
        }
    }
    // set the duration of the scan to be 1.1 seconds


    /**
     * Service onStartCommand
     * Handles the action passed via the Intent
     *
     * @return START_REDELIVER_INTENT
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        SharedPreferences userSettings = this.getSharedPreferences(Constants.USER_ID_PREFERENCE_EDITOR, 0) ;
        sessionId = userSettings.getString(Constants.USER_ID,sessionId);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        url = prefs.getString("alarm_server_address", url);

        if(intent == null)
            return 0;

        String action = intent.getAction();
        mLog.info("Received action of " + action);

        if(action == null) {
            mLog.info("Starting service with no action\n Probably from a crash");
        } else {
            if(action.equals(ACTION_START)) {
                mLog.info("Received ACTION_START");
                start();
            } else if(action.equals(ACTION_STOP)) {
                stop();
            } else if(action.equals(ACTION_KEEPALIVE)) {
                keepAlive();
            } else if(action.equals(ACTION_RECONNECT)) {
                if(isNetworkAvailable()) {
                    reconnectIfNecessary();
                }
            }
        }




        return START_STICKY;
    }

    /**
     * Attempts connect to the Mqtt Broker
     * and listen for Connectivity changes
     * via ConnectivityManager.CONNECTVITIY_ACTION BroadcastReceiver
     */
    private synchronized void start() {
        if(mStarted) {
            mLog.info("Attempt to start while already started");
            return;
        }

        if(hasScheduledKeepAlives()) {
            stopKeepAlives();
        }

        connect();
        try {
            if(!isRegisteredConnectivity) {
                registerReceiver(mConnectivityReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                isRegisteredConnectivity = true;
            }
        }catch(Exception e){
            mLog.error("", e);
            e.printStackTrace();

        }
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            mLog.info("Bluetooth is not operative");
        } else {
            if (mBluetoothAdapter.isEnabled()) {
                beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
                    @Override
                    public void onServiceReady() {
                        beaconManager.startRanging(region);
                    }
                });
            }
        }
    }
    /**
     * Attempts to stop the Mqtt client
     * as well as halting all keep alive messages queued
     * in the alarm manager
     */
    private synchronized void stop() {
        if(!mStarted) {
            mLog.info("Attempting to stop connection that isn't running");
            return;
        }

        if(mClient != null) {
            mConnHandler.post(new Runnable() {
                @Override
                public void run() {
                    try {
                        mClient.disconnect();
                    } catch(MqttException ex) {
                        ex.printStackTrace();
                    }
                    mClient = null;
                    mStarted = false;

                    stopKeepAlives();
                }
            });
        }

        unregisterReceiver(mConnectivityReceiver);
        beaconManager.stopRanging(region);
    }
    /**
     * Connects to the broker with the appropriate datastore
     */
    private synchronized void connect() {
        String url = String.format(Locale.US, MQTT_URL_FORMAT, MQTT_BROKER, MQTT_PORT);
        mLog.info("Connecting with URL: " + url);
        try {
            if(mDataStore != null) {
                mLog.info("Connecting with DataStore");
                mClient = new MqttClient(url,"AppDevice_" + mDeviceId,mDataStore);
            } else {
                mLog.info("Connecting with MemStore");
                mClient = new MqttClient(url,"AppDevice_" + mDeviceId);
            }
        } catch(MqttException e) {
            mLog.error("", e);
            e.printStackTrace();

        }

        mConnHandler.post(new Runnable() {
            @Override
            public void run() {
                try {
                    mClient.connect(mOpts);

                    String[] topics = new String[]{MQTT_TOPIC, MESSAGE_NOTIFICATION_TOPIC, UPDATE_APPLICATION_NOTIFICATION_TOPIC, SAVE_LOG_TOPIC, MESSAGE_STATUS_CHANGE_TOPIC};
                    int[] qos = new int[]{0, 0, 0, 0, 0};

                    mClient.subscribe(topics, qos);

                    mClient.setCallback(PahoMqttService.this);

                    mStarted = true; // Service is now connected

                    mLog.info("Successfully connected and subscribed starting keep alives");

                    startKeepAlives();
                } catch (MqttException e) {
                    mLog.error("", e);
                    e.printStackTrace();
                }
            }
        });
    }
    /**
     * Schedules keep alives via a PendingIntent
     * in the Alarm Manager
     */
    private void startKeepAlives() {
        Intent i = new Intent();
        i.setClass(this, PahoMqttService.class);
        i.setAction(ACTION_KEEPALIVE);
        PendingIntent pi = PendingIntent.getService(this, 0, i, 0);
        mAlarmManager.setRepeating(AlarmManager.RTC_WAKEUP,
                System.currentTimeMillis() + MQTT_KEEP_ALIVE,
                MQTT_KEEP_ALIVE, pi);
    }
    /**
     * Cancels the Pending Intent
     * in the alarm manager
     */
    private void stopKeepAlives() {
        Intent i = new Intent();
        i.setClass(this, PahoMqttService.class);
        i.setAction(ACTION_KEEPALIVE);
        PendingIntent pi = PendingIntent.getService(this, 0, i , 0);
        mAlarmManager.cancel(pi);
    }
    /**
     * Publishes a KeepALive to the topic
     * in the broker
     */
    private synchronized void keepAlive() {
        try{
            new KeepALiveSmartfloorWebService().execute();
        }catch (Exception e){
            mLog.error("KeepALive Error - " + e.getMessage(), e);
        }
        if(isConnected()) {
            try {
                sendKeepAlive();
                if(!lastIsConnected)
                {
                    broadcastReceivedMessage(Constants.KEEP_ALIVE_SUCCESS);
                    lastIsConnected = true;
                }

                return;
            } catch(MqttConnectivityException ex) {
                mLog.error("", ex);
                ex.printStackTrace();
                reconnectIfNecessary();
            } catch(MqttPersistenceException ex) {
                mLog.error("", ex);
                ex.printStackTrace();
                stop();
            } catch(MqttException ex) {
                mLog.error("", ex);
                ex.printStackTrace();
                stop();
            } catch(Exception ex) {
                mLog.error("", ex);
                ex.printStackTrace();
            }

        }else{

            if(lastIsConnected)
            {
                broadcastReceivedMessage(Constants.KEEP_ALIVE_FAIL);
                lastIsConnected = false;
            }

            if(isNetworkAvailable()) {
                start();
            }
        }
    }

    private synchronized void reconnectIfNecessary() {
        if(mStarted && mClient == null) {
            mLog.info("Reconnecting...");
            connect();
        }

    }
    /**
     * Query's the NetworkInfo via ConnectivityManager
     * to return the current connected state
     * @return boolean true if we are connected false otherwise
     */
    private boolean isNetworkAvailable() {
        NetworkInfo info = mConnectivityManager.getActiveNetworkInfo();

        return (info == null) ? false : info.isConnected() && info.getType() == ConnectivityManager.TYPE_WIFI;
    }
    /**
     * Verifies the client State with our local connected state
     * @return true if its a match we are connected false if we aren't connected
     */
    private boolean isConnected() {
        if(mStarted && mClient != null && !mClient.isConnected()) {
            mLog.info("Mismatch between what we think is connected and what is connected");
        }

        if(mClient != null) {
            return (mStarted && mClient.isConnected()) ? true : false;
        }

        return false;
    }
    /**
     * Receiver that listens for connectivity chanes
     * via ConnectivityManager
     */
    private final BroadcastReceiver mConnectivityReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            NetworkInfo info = mConnectivityManager.getActiveNetworkInfo();
            if(info != null){
                if(info.isConnected() && info.getType() == ConnectivityManager.TYPE_WIFI)
                {
                    mLog.info("Wifi Connected");
                    if(mClient == null && !mStarting)
                    {
                        mLog.info("Client is NULL, so we come from connectionLost");
                        mLog.info("restarting server");
                        actionStart(context);
                        mStarting = true;
                    }
                }else
                {
                    mLog.info("Wifi is not available");
                }
            }

        }
    };
    /**
     * Sends a Keep Alive message to the specified topic
     * @see 'MQTT_KEEP_ALIVE_MESSAGE
     * @see 'MQTT_KEEP_ALIVE_TOPIC_FORMAT
     * @return MqttDeliveryToken specified token you can choose to wait for completion
     */
    private synchronized MqttDeliveryToken sendKeepAlive()
            throws MqttConnectivityException, MqttPersistenceException, MqttException {
        if(!isConnected())
            throw new MqttConnectivityException();

        if(mKeepAliveTopic == null) {
            mKeepAliveTopic = mClient.getTopic(
                    String.format(Locale.US, MQTT_KEEP_ALIVE_TOPIC_FORAMT,mDeviceId));
        }

        mLog.info("Sending Keepalive to " + MQTT_BROKER);

        MqttMessage message = new MqttMessage(MQTT_KEEP_ALIVE_MESSAGE);
        message.setQos(MQTT_KEEP_ALIVE_QOS);

        return mKeepAliveTopic.publish(message);
    }
    /**
     * Query's the AlarmManager to check if there is
     * a keep alive currently scheduled
     * @return true if there is currently one scheduled false otherwise
     */
    private synchronized boolean hasScheduledKeepAlives() {
        Intent i = new Intent();
        i.setClass(this, PahoMqttService.class);
        i.setAction(ACTION_KEEPALIVE);
        PendingIntent pi = PendingIntent.getBroadcast(this, 0, i, PendingIntent.FLAG_NO_CREATE);

        return (pi != null) ? true : false;
    }


    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }
    /**
     * Connectivity Lost from broker
     */
    @Override
    public void connectionLost(Throwable arg0) {
        mLog.info("Connection Lost");

        mClient = null;
        mStarted = false;

        if(isNetworkAvailable()) {
            reconnectIfNecessary();
        }
    }

    @Override
    public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {
        String[] splitedTopic = topic.split("/");

        String messageBody = new String(mqttMessage.getPayload());
        switch (splitedTopic[1]){
            case "Messages":
                notifyUserNewMessageArrive(topic, messageBody);
                broadcastReceivedMessage(NEW_MESSAGE_FROM_MANAGER_ARRIVE);
                break;
            case "NewVersion":
                notifyUserNewVersionArrive();
                broadcastReceivedMessage(NEW_VERSION_AVAILABLE, messageBody);
                break;
            case "MessagesStatusChange":
                broadcastReceivedMessage(NEW_MESSAGE_FROM_MANAGER_ARRIVE);
                break;
            case "SaveLog":
                notifyUserLogRequested();
                broadcastReceivedMessage(LOG_REQUESTED);
                try{
                    new LogSender().execute();
                }catch (Exception e){
                    mLog.error("", e);
                }
                break;
            default:
                String fomattedTopic = "";
                if (splitedTopic.length > 1)
                    fomattedTopic = splitedTopic[4];
                new UpdateLayoutAssignedTaskList().execute(fomattedTopic, messageBody);
                break;
        }
    }

    private void notifyUserLogRequested() {

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        // Gets an instance of the NotificationManager service
        NotificationManager mNotifyMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        Notification notification = builder.build();
        //Setting default device sound.
        notification.defaults |= Notification.DEFAULT_SOUND;
        notification.defaults |= Notification.DEFAULT_LIGHTS;
        notification.ledARGB = Color.CYAN;

        mNotifyMgr.notify(21, notification);
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

    }

    private void broadcastReceivedMessage(String constant)
    {
        // pass a message received from the MQTT server on to the Activity UI
        //   (for times when it is running / active) so that it can be displayed
        //   in the app GUI
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(constant);
        sendBroadcast(broadcastIntent);
    }

    private void broadcastReceivedMessage(String constant, String extra)
    {
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(constant);
        broadcastIntent.putExtra("version", extra);
        sendBroadcast(broadcastIntent);
    }

    private void notifyUserNewVersionArrive()
    {
        //topic /Messages/{runner_id}/{manager_id}/{manager_user_name}
        Intent notificationIntent = new Intent(this, UpdateApplicationActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(this,
                0,
                notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        Bitmap iconImage = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.ic_system_update_alt_white_48dp), 64, 64, false);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        // Gets an instance of the NotificationManager service
        NotificationManager mNotifyMgr =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        Notification notification = builder.build();
        //Setting default device sound.
        notification.defaults |= Notification.DEFAULT_SOUND;
        notification.defaults |= Notification.DEFAULT_LIGHTS;
        notification.ledARGB = Color.BLUE;

        mNotifyMgr.notify(21, notification);
    }

    private void notifyUserNewMessageArrive(String topic, String body)
    {
        final String  MESSAGING = "MESSAGING";
        //topic /Messages/{runner_id}/{manager_id}/{manager_user_name}

        String[] topicData = topic.split("/Messages/")[1].split("/");
        String managerId = topicData[1];
        String managerUserName = topicData[2];

        PendingToReadMessagesHelper pendingMessasgesHelper = new PendingToReadMessagesHelper(getApplicationContext());
        LayoutNotificationManager notificationManager = new LayoutNotificationManager(getApplicationContext());
        LayoutPendingMessage pendingMessage = new LayoutPendingMessage();
        pendingMessage.setManager(managerUserName);
        pendingMessage.setMessage(body);
        pendingMessasgesHelper.addPendingMessage(pendingMessage);

        Intent notificationIntent = new Intent(this, ConversationActivity.class);

        notificationIntent.putExtra(Constants.MANAGER_ID, Long.parseLong(managerId));
        notificationIntent.putExtra(Constants.MANAGER_USERNAME, managerUserName);
        notificationIntent.putExtra(Constants.FROM_NOTIFICATION, true);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        LayoutNotification notification = new LayoutNotification();
        notification.setId(0);
        notification.setNotificationType(LayoutNotificationType.MESSAGE);
        notification.setIcon(R.drawable.ic_message_white_48dp);

        List<IntentExtra> intentExtras = new ArrayList<>();
        intentExtras.add(new IntentExtra(Constants.MANAGER_ID, managerId));
        intentExtras.add(new IntentExtra(Constants.MANAGER_USERNAME, managerUserName));
        intentExtras.add(new IntentExtra(Constants.FROM_NOTIFICATION, "true"));
        notification.setExtras(intentExtras);

        // if(!notificationHistoryManager.AvailableNotificationsWithSameKey(managerId)) {
        String title = getResources().getString(R.string.new_message_from) + " " + managerUserName;
        builder.setContentTitle(title)
                .setContentText(body)
                .setSubText(managerUserName);
        notification.setTitle(title);
        notification.setContent(body);
        notificationHistoryManager.AddNotification(managerId, body);

        notificationManager.add(notification);
        broadcastReceivedMessage(Constants.NOTIFY_NEW_ICON_MENU_NOTIFICATION);
        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        long[] pattern = {1000, 1000, 1000, 1000};
        v.vibrate(pattern, -1);
    }


    private void notifyUserNewTaskArrive(String title, String body)
    {
        LayoutNotificationManager notificationManager = new LayoutNotificationManager(getApplicationContext());
        boolean haveAlarmId = body.split("_").length > 1;
        List<IntentExtra> intentExtras = new ArrayList<>();
        String taskTitle = "";
        LayoutNotification notification = new LayoutNotification();
        notification.setNotificationType(LayoutNotificationType.TASK);


        if(haveAlarmId) {
            notification.setId(Integer.parseInt(body.split("_")[1]));
            String alarmId = body.split("_")[1];
            intentExtras.add(new IntentExtra(Constants.DOWNLOAD_ALARMS, "true"));
            intentExtras.add(new IntentExtra(Constants.ALARM_ID, alarmId));
            taskTitle = ServiceFactory.getInstance().getAlarmService(getApplicationContext()).getById(alarmId).getTitle();
        }

        notification.setExtras(intentExtras);

        LayoutSiteItemSubCategory subCategory = LayoutSiteItemSubCategory.forCode(Integer.parseInt(title));
        String category = taskTitle.isEmpty()? subCategory.getDescription(getApplicationContext()) : taskTitle;
        String content = haveAlarmId ? body.split("_")[0] : body;

        notification.setTitle(category);
        notification.setIcon(AlarmResourseHelper.getCategoryImageByType(subCategory.getGroupCategory()));
        notification.setContent(content);

        notificationManager.add(notification);
        broadcastReceivedMessage(Constants.NOTIFY_NEW_ICON_MENU_NOTIFICATION);
        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        long[] pattern = {1000, 1000, 1000, 1000};
        v.vibrate(pattern, -1);
    }




    /**
     * MqttConnectivityException Exception class
     */
    private class MqttConnectivityException extends Exception {
        private static final long serialVersionUID = -7385866796799469420L;
    }

    private class PostBeaconRunnerDistanceInBackground extends AsyncTask<Beacon,Void, Boolean> {

        @Override
        protected void onPreExecute(){
            beaconsList = new SaveInformationHelper<>(getApplicationContext());
            beaconsList.clearList(Constants.BEACONS_DETECTED);
        }
        @Override
        protected Boolean doInBackground(Beacon... params) {

            try {
                BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                if (mBluetoothAdapter == null) {
                    mLog.info("Bluetooth is not operative");
                    return false;
                } else {
                    if (mBluetoothAdapter.isEnabled()) {
                        PushBeaconRunnerDistance(params);
                        mLog.info("DoInBackground Success");
                        return true;
                    }
                    return false;
                }
            } catch (Exception e){
                mLog.error("", e);
                return false;
            }
        }

        private void PushBeaconRunnerDistance(Beacon[] params) {

            DATA_PostActionResponse beaconResponse = new DATA_PostActionResponse();
            try
            {
                List<BeaconAvailable> beaconsFounded = new ArrayList<>();
                //Traer los beacons disponibles
                List<BeaconAvailable> beaconAvailables = new SaveInformationHelper<BeaconAvailable>(getApplicationContext()).readList(sessionId + Constants.BEACONS_AVAILABLES);
                List<BeaconAvailable> beaconsDetected = new ArrayList<>();
                for(Beacon b : params)
                {
                    double distance = Utils.computeAccuracy(b);
                    for(BeaconAvailable ba : beaconAvailables)
                    {
                        if(ba.getMac().equals(b.getMacAddress())) {
                            ba.setDistance(distance);
                            mLog.info("Beacons - Distances from beacon " + ba.getMac() + " is " + ba.getDistance());
                        }
                    }
                    BeaconAvailable detected = new BeaconAvailable();
                    detected.setDistance(Utils.computeAccuracy(b));
                    detected.setMac(b.getMacAddress().toString());
                    beaconsFounded.add(detected);
                }
                beaconsList.writeList(beaconsFounded, Constants.BEACONS_DETECTED);
                sendBroadcast(new Intent(Constants.BEACONS_DETECTED));

                double[][] positions = new double[beaconsDetected.size()][2];
                double[] distances= new double[beaconsDetected.size()];

                for (int i = 0; i < beaconsDetected.size(); i++){
                    positions[i] = new double[]{beaconsDetected.get(i).getPositionX(), beaconsDetected.get(i).getPositionY()};
                    distances[i] = beaconsDetected.get(i).getDistance();
                }

                double[] locationPoint = new double[2];

                if(positions.length > 2) {
                    TrilaterationFunction trilaterationFunction = new TrilaterationFunction(positions, distances);
                    NonLinearLeastSquaresSolver solver = new NonLinearLeastSquaresSolver(trilaterationFunction, new LevenbergMarquardtOptimizer());
                    LeastSquaresOptimizer.Optimum optimum = solver.solve();
                    locationPoint = optimum.getPoint().toArray();
                    mLog.info("Beacons - Location Point - X: " + locationPoint[0] + " Y: " + locationPoint[1]);

                }else {
                    locationPoint[0] = 0;
                    locationPoint[0] = 1;
                }
                String mDeviceId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
                final String NAMESPACE = "http://winsystemsintl.com/";
                final String METHOD_NAME = "UpdateBeaconRunnerPosition";
                final String SOAP_ACTION = "http://winsystemsintl.com/IDATAService/UpdateBeaconRunnerPosition";

                BeaconAvailable beaconAvailable = BeaconWithLowestDistance(beaconsDetected);

                SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
                DATA_BeaconRunnerDistance beaconRequest = new DATA_BeaconRunnerDistance();
                beaconRequest.setSessionId(sessionId);
                beaconRequest.setDeviceId(mDeviceId);
                beaconRequest.setPositionX(String.valueOf(locationPoint[0]));
                beaconRequest.setPositionY(String.valueOf(locationPoint[1]));
                beaconRequest.setFloorId(beaconAvailable.getPositionFloorId().toString());
                beaconRequest.setRange(beaconAvailable.distance);
                beaconRequest.setNearBeacon(beaconAvailable.getMac());

                PropertyInfo pi = new PropertyInfo();
                pi.setName("beaconInfo");
                pi.setValue(beaconRequest);
                pi.setType(beaconRequest.getClass());

                request.addProperty(pi);

                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                envelope.dotNet = true;
                envelope.setOutputSoapObject(request);
                envelope.addMapping(NAMESPACE, "DATA_BeaconRunnerDistance", beaconRequest.getClass());
                envelope.addMapping(NAMESPACE, "DATA_PostActionResponse", beaconResponse.getClass());

                HttpTransportSE transporte = new HttpTransportSE(url, 60000);


                TrustManagerManipulator.allowAllSSL();
                transporte.call(SOAP_ACTION, envelope);

                SoapObject resSoap =(SoapObject)envelope.getResponse();

                beaconResponse.setResponseCode(Integer.parseInt(resSoap.getProperty(0).toString()));
                beaconResponse.setResponseCodeDescription(resSoap.getProperty(1).toString());
                beaconResponse.setSessionId(resSoap.getProperty(2).toString());

            }catch (ConnectException c){
                mLog.error("", c);
                beaconResponse.setResponseCode(9);
                beaconResponse.setResponseCodeDescription(getApplicationContext().getString(R.string.error_connection));
            }
            catch (SocketTimeoutException e)
            {
                mLog.error("", e);
                beaconResponse.setResponseCode(9);
                beaconResponse.setResponseCodeDescription(getApplicationContext().getString(R.string.error_timeout));
            }
            catch (Exception e)
            {
                mLog.error("", e);
                beaconResponse.setResponseCode(9);
                beaconResponse.setResponseCodeDescription(getApplicationContext().getString(R.string.error_unknown));
            }
        }

        private BeaconAvailable BeaconWithLowestDistance(List<BeaconAvailable> beaconsDetected) {
            Double distance = 1000000000.0;
            BeaconAvailable nearBeacon = null;
            for(BeaconAvailable b : beaconsDetected)
            {
                if(b.getDistance() < distance) {
                    distance = b.getDistance();
                    nearBeacon = b;
                }
            }

            return nearBeacon;
        }
    }
    private class UpdateLayoutAssignedTaskList extends AsyncTask<String,Void, Boolean> {
        private String topic;
        private String message;

        @Override
        protected Boolean doInBackground(String... params) {

            try {
                AlarmService service = ServiceFactory.getInstance().getAlarmService(getApplicationContext());
                service.setUrl(url);
                service.getAll(sessionId, getApplicationContext());
                topic   = params[0];
                message = params[1];
                mLog.info("DoInBackground Success");
                return true;
            } catch (Exception e){
                mLog.error("", e);
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean result){
            if(result)
            {
                boolean haveAlarmId = message.split("_").length > 1;
                if(haveAlarmId){
                    String taskId = message.split("_")[1];
                    LayoutSiteTask task = ServiceFactory.getInstance().getAlarmService(getApplicationContext()).getById(taskId);
                    if(task == null){
                        LayoutNotificationManager notificationManager = new LayoutNotificationManager(getApplicationContext());
                        notificationManager.deleteNotificationById(taskId);
                    }else {
                        notifyUserNewTaskArrive(topic, message);
                    }
                }else {
                    notifyUserNewTaskArrive(topic, message);
                }

                broadcastReceivedMessage(NEW_TASK_ARRIVE);
            }
        }

    }

    private class LogSender extends AsyncTask<String,Void, Boolean> {
        private String topic;
        private String message;

        @Override
        protected Boolean doInBackground(String... params) {

            try {
                LogService service = ServiceFactory.getInstance().getLogService(getApplicationContext());
                service.saveLog();
                mLog.info("DoInBackground Success");
                return true;
            } catch (Exception e){
                mLog.error("", e);
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean result){
            if(result)
            {
                    // No actions by now
            }
        }

    }

    private class KeepALiveSmartfloorWebService extends AsyncTask<String,Void, Boolean> {
        private ResponseCode responseCode;
        @Override
        protected Boolean doInBackground(String... params) {

            try {
                DATA_PostActionResponse service = ServiceFactory.getInstance().getLoginService(getApplicationContext()).keepALive(getApplicationContext(), sessionId, url);
                mLog.info("DoInBackground Success");
                responseCode = service.getResponseCode();
                return responseCode == ResponseCode.DATA_RC_OK;
            } catch (Exception e){
                mLog.error("", e);
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean result){
            if(!result)
            {
                Intent intent = new Intent(PahoMqttService.this,LoginActivity.class);
                intent.setAction(Intent.ACTION_VIEW);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.putExtra("NoService", true);
                startActivity(intent);
            }
        }
    }
}
