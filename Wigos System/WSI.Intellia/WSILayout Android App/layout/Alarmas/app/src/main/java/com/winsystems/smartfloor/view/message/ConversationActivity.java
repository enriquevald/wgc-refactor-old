package com.winsystems.smartfloor.view.message;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.winsystems.smartfloor.R;
import com.winsystems.smartfloor.common.AndroidLogger;
import com.winsystems.smartfloor.common.LayoutNotificationManager;
import com.winsystems.smartfloor.helpers.LogHelper;
import com.winsystems.smartfloor.helpers.PendingToReadMessagesHelper;
import com.winsystems.smartfloor.model.LayoutSiteManagerRunnerMessage;
import com.winsystems.smartfloor.service.ResponseCode;
import com.winsystems.smartfloor.service.ServiceFactory;
import com.winsystems.smartfloor.service.message.response.DATA_GetConversationHistoryResponse;
import com.winsystems.smartfloor.service.message.response.DATA_SendMessageResponse;
import com.winsystems.smartfloor.service.mqtt.PahoMqttService;
import com.winsystems.smartfloor.service.notification.NotificationHistoryManager;
import com.winsystems.smartfloor.view.utils.Constants;
import com.winsystems.smartfloor.view.utils.KioskModeActivity;
import com.winsystems.smartfloor.view.utils.ToastFactory;

import java.util.ArrayList;
import java.util.List;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: ConversationActivity
//
// DESCRIPTION: Activity that implements the functionality of conversation.
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public class ConversationActivity extends KioskModeActivity {

  private String url;
  private String userId;
  private String userName;
  private Long managerId;
  private String managerUserName;
  private List<LayoutSiteManagerRunnerMessage> messages;
  private MessageListAdapter messageListAdapter;
  ListView messageListView;
  private IntentFilter intentFilter = new IntentFilter(PahoMqttService.NEW_MESSAGE_FROM_MANAGER_ARRIVE);
  private BroadcastReceiver messageBroadcastReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      ConversationActivity.this.refreshView();
    }
  };

  private void refreshView() {
    new HttpFillConversationTask().execute();
  }
  private static AndroidLogger mLog;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    mLog = LogHelper.getLogger(this);
    setContentView(R.layout.activity_message_with_manager);
    SharedPreferences settings = this.getSharedPreferences(Constants.USER_ID_PREFERENCE_EDITOR, 0) ;
    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
    this.url = prefs.getString("alarm_server_address", "");
    this.userId = settings.getString(Constants.USER_ID, "");
    this.userName = prefs.getString("userName", "");
    this.managerId = Long.parseLong(getIntent().getStringExtra(Constants.MANAGER_ID));
    this.managerUserName = getIntent().getStringExtra(Constants.MANAGER_USERNAME);
    Integer notificationId = getIntent().getIntExtra(Constants.NOTIFICATION_ID,0);
    PendingToReadMessagesHelper pendingMessasgesHelper = new PendingToReadMessagesHelper(getApplicationContext());
    pendingMessasgesHelper.removePendingMessages(this.managerUserName);

    Boolean fromNotifications = Boolean.parseBoolean(getIntent().getStringExtra(Constants.FROM_NOTIFICATION));
    if(fromNotifications) {
      NotificationHistoryManager notificationHistoryManager = new NotificationHistoryManager(getApplicationContext());
      notificationHistoryManager.ClearNotifications(managerId.toString());
    }

    if(notificationId != 0)
    {
      LayoutNotificationManager notificationManager = new LayoutNotificationManager(getApplicationContext());
      notificationManager.remove(notificationId);
    }

    refreshView();

    messageListView = (ListView) findViewById(R.id.messages_list);
    ImageButton sendMessage = (ImageButton) this.findViewById(R.id.btn_sendMessage);
    sendMessage.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        TextView messageTextView = (TextView) findViewById(R.id.message_text);
        if (!messageTextView.getText().toString().isEmpty())
          new HttpSendMessageTask().execute();
        else
          ToastFactory.buildSimpleToast(getString(R.string.cant_send_empty_messages), getApplicationContext());
      }
    });

    ActionBar actionBar = getSupportActionBar();
    actionBar.setTitle(this.managerUserName);
  }

  @Override
  public void onResume(){
    super.onResume();
    registerReceiver(messageBroadcastReceiver, intentFilter);
  }

  @Override
  protected void onPause() {
    super.onPause();
    unregisterReceiver(messageBroadcastReceiver);
  }


  private class HttpFillConversationTask extends AsyncTask<Void, Void, Boolean> {
    @Override
    protected void onPreExecute() {
      super.onPreExecute();
      messages = new ArrayList<>();
    }

    @Override
    public Boolean doInBackground(Void... params) {
      try {

        DATA_GetConversationHistoryResponse response = ServiceFactory.getInstance().getMessageService(getApplicationContext()).getConversationHistory(userId, managerId, url);

        if (response.getResponseCode() == ResponseCode.DATA_RC_OK){
          messages = response.getMessages();
          return true;
        }else
        {
          ToastFactory.buildSimpleToast(response.getResponseCodeDescription(), ConversationActivity.this);
          return false;
        }
      } catch (Exception e) {
        mLog.error("PhotoActivity", e);
        return false;
      }

    }

    @Override
    protected void onPostExecute(Boolean result) {
      if(result){
        runOnUiThread(new Runnable() {
          @Override
          public void run() {
            messageListAdapter = new MessageListAdapter(getApplicationContext(),messages);
            messageListView.setAdapter(messageListAdapter);
            messageListView.setSelection(messages.size()-1);
          }});
      }
      else
      {
        ToastFactory.buildSimpleToast(getResources().getString(R.string.error_on_Action), ConversationActivity.this);
      }
    }
  }

  private class HttpSendMessageTask extends AsyncTask<Void, Void, Boolean> {
    private String message;
    private TextView messageTextView;
    @Override
    protected void onPreExecute() {
      super.onPreExecute();
      messageTextView = (TextView) findViewById(R.id.message_text);
      message = messageTextView.getText().toString();
    }

    @Override
    public Boolean doInBackground(Void... params) {
      try {

        DATA_SendMessageResponse response = ServiceFactory.getInstance().getMessageService(getApplicationContext()).sendMessageTo(userId, managerId, message, url);

        if (response.getResponseCode() == ResponseCode.DATA_RC_OK){
          return true;
        }else
        {
          ToastFactory.buildSimpleToast(response.getResponseCodeDescription(), ConversationActivity.this);
          return false;
        }
      } catch (Exception e) {
        mLog.error("PhotoActivity", e);
        return false;
      }

    }

    @Override
    protected void onPostExecute(Boolean result) {
            /*if (dialog.isShowing()) {
                dialog.dismiss();
            }*/
      if(result){
        new HttpFillConversationTask().execute();
        runOnUiThread(
            new Runnable() {
              @Override
              public void run() {
                messageListView.setSelection(messages.size() - 1);
                messageTextView.setText("");
              }
            });
      }
      else
      {
        ToastFactory.buildSimpleToast(getResources().getString(R.string.error_on_Action), ConversationActivity.this);
      }

    }
  }


  @Override
  public void onConnectivityChange(){
    ImageButton button = (ImageButton) this.findViewById(R.id.btn_sendMessage);

    if(button == null)
    {
      return;
    }

    boolean isConnected = getIsConnected();
    if(!isConnected)
    {
      button.setEnabled(false);
    }
    else{
      if(!button.isEnabled()){
        button.setEnabled(true);
      }
    }
  }

  @Override
  public void onKeepAliveChange (int keepAliveType){
    ImageButton button = (ImageButton) this.findViewById(R.id.btn_sendMessage);

    if(button == null)
    {
      return;
    }

    boolean isConnected = keepAliveType == 0;
    if(!isConnected)
    {
      button.setEnabled(false);
    }
    else{
      if(!button.isEnabled()){
        button.setEnabled(true);
      }
    }

  }

}
