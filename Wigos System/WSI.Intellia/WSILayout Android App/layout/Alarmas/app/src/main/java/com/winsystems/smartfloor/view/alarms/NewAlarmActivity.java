package com.winsystems.smartfloor.view.alarms;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.winsystems.smartfloor.R;
import com.winsystems.smartfloor.common.AndroidLogger;
import com.winsystems.smartfloor.helpers.CameraSetupHelper;
import com.winsystems.smartfloor.helpers.LogHelper;
import com.winsystems.smartfloor.model.Alarm;
import com.winsystems.smartfloor.model.LayoutSiteItemCategory;
import com.winsystems.smartfloor.model.LayoutSiteItemSubCategory;
import com.winsystems.smartfloor.model.Terminal;
import com.winsystems.smartfloor.service.DATA_PostActionResponse;
import com.winsystems.smartfloor.service.ResponseCode;
import com.winsystems.smartfloor.service.ServiceFactory;
import com.winsystems.smartfloor.view.camera.CaptureActivityAnyOrientation;
import com.winsystems.smartfloor.helpers.AlarmResourseHelper;
import com.winsystems.smartfloor.view.utils.Constants;
import com.winsystems.smartfloor.view.utils.KioskModeActivity;
import com.winsystems.smartfloor.view.utils.ToastFactory;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import uk.co.deanwild.materialshowcaseview.IShowcaseListener;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseSequence;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;
import uk.co.deanwild.materialshowcaseview.ShowcaseConfig;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: NewAlarmActivity
//
// DESCRIPTION: Activity that implements new and setup alarm functioanlity.
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public class NewAlarmActivity extends KioskModeActivity {
    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
    private ProgressDialog dialog;
    private HttpCreateAlarmTask  createAlarmAsynchTask;
    private String userId;
    private String loggedUserCreateAlarmUrl = "";
    private Alarm alarm;
    private ImageView photoContainerView;
    private String photoFromCammera;
    private Uri photoFromCammeraUri;
    private String qrCode;
    private RadioGroup radioGroup;
    private boolean isQrInput;
    private Button takePictureButton;
    private RelativeLayout photoLayout;
    private EditText titleCategory;
    private Spinner alarmCategory;
    private EditText alarmDescription;
    private ImageButton terminalQRCodeButton;
    private EditText terminalId;
    private Button createAlarmButton;
    private static AndroidLogger mLog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLog = LogHelper.getLogger(this);
        setupAlarmFields();
        buildDialog();
        onConnectivityChange();
        presentShowcaseSequence();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (resultCode == RESULT_OK) {
            switch (requestCode)
            {
                case CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE:
                    photoContainerView.setImageBitmap(CameraSetupHelper.LoadBitmatOfPhotoTaken(photoFromCammera));
                    photoContainerView.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
                    photoContainerView.setAdjustViewBounds(true);
                    photoLayout.setVisibility(View.VISIBLE);
                    takePictureButton.setText(R.string.new_alarm_change_photo);
                    break;
                default:
                    IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
                    if (scanningResult != null) {
                        String result = scanningResult.getContents();
                        String[] splitResults = result.split("#");
                        if(splitResults.length > 1)
                            qrCode = splitResults[1];
                        else
                            qrCode = "";

                        TextView qrResult = (TextView) findViewById(R.id.new_alarm_terminal_provider_field);
                        qrResult.setText(qrCode);
                    }
                    break;
            }

        }else{
            IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
            if (scanningResult != null) {
                TextView textView = (TextView)this.findViewById(R.id.new_alarm_terminal_provider_field);
                textView.setText(scanningResult.getContents());
            } else {
                Toast toast = Toast.makeText(getApplicationContext(), getString(R.string.no_data_received), Toast.LENGTH_SHORT);
                toast.show();
            }
        }
    }

    @Override
    public void onConnectivityChange(){
        Button createAlarmButton = (Button) this.findViewById(R.id.create_new_alarm_button);

        if(createAlarmButton == null)
        {
            return;
        }

        boolean isConnected = getIsConnected();
        if(!isConnected)
        {
            createAlarmButton.setEnabled(false);
        }
        else{
            if(!createAlarmButton.isEnabled()){
                createAlarmButton.setEnabled(true);
            }
        }
    }

    @Override
    public void onKeepAliveChange (int keepAliveType){
        Button createAlarmButton = (Button) this.findViewById(R.id.create_new_alarm_button);

        if(createAlarmButton == null)
        {
            return;
        }

        boolean isConnected = keepAliveType == 0;
        if(!isConnected)
        {
            createAlarmButton.setEnabled(false);
        }
        else{
            if(!createAlarmButton.isEnabled()){
                createAlarmButton.setEnabled(true);
            }
        }

    }

    private void setupAlarmFields() {
        setContentView(R.layout.activity_new_alarm);
        alarmCategory = (Spinner) this.findViewById(R.id.new_alarm_category);
        alarmDescription = (EditText) this.findViewById(R.id.new_alarm_description_field);
        createAlarmButton = (Button) this.findViewById(R.id.create_new_alarm_button);
        terminalId = (EditText) this.findViewById(R.id.new_alarm_terminal_provider_field);
        titleCategory = (EditText) findViewById(R.id.titleCategory);
        takePictureButton = (Button) this.findViewById(R.id.take_photo_button);
        terminalQRCodeButton = (ImageButton) this.findViewById(R.id.terminal_qr_code_button);
        radioGroup = (RadioGroup) this.findViewById(R.id.radioGroupCategoryType);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                DisplayTerminalPromotionElements(radioGroup);
                int idx = getRadioGroupSelectedIndex();
                if (idx == 2)
                    titleCategory.setVisibility(View.VISIBLE);
                else
                    titleCategory.setVisibility(View.GONE);
                if(idx == 1)
                    terminalId.setHint(R.string.new_alarm_terminal_provider_placeholder_field_machine);
                else
                    terminalId.setHint(R.string.new_alarm_terminal_provider_placeholder_field);
            }
        });

        DisplayTerminalPromotionElements(radioGroup);

        createAlarmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getIsConnected()) {

                    if (isValidAlarm()) {
                        alarm = createAlarmFromScratch();
                        createAlarmAsynchTask = new HttpCreateAlarmTask();
                        createAlarmAsynchTask.execute();
                    } else {
                        ToastFactory.buildSimpleToast(getResources().getString(R.string.error_field_required_new_alarm), NewAlarmActivity.this);
                    }

                } else
                {
                    ToastFactory.buildSimpleToast(getResources().getString(R.string.error_field_required_new_alarm), NewAlarmActivity.this);

                }
            }
        });

        takePictureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchCameraIntent();
            }
        });

        terminalQRCodeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentIntegrator scanIntegrator = new IntentIntegrator(NewAlarmActivity.this);
                scanIntegrator.setCaptureActivity(CaptureActivityAnyOrientation.class);
                scanIntegrator.setOrientationLocked(false);
                scanIntegrator.initiateScan();
            }
        });

        SharedPreferences settings = this.getSharedPreferences(Constants.USER_ID_PREFERENCE_EDITOR, 0) ;
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);//get the preferences that are allowed to be given

        this.userId = settings.getString(Constants.USER_ID, userId);
        this.loggedUserCreateAlarmUrl = prefs.getString("alarm_server_address", this.loggedUserCreateAlarmUrl);

        this.photoContainerView = (ImageView)this.findViewById(R.id.photo_container);
        photoContainerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(android.content.Intent.ACTION_VIEW);
                Uri u = photoFromCammeraUri;
                intent.setDataAndType(u, "image/*");
                startActivity(intent);
            }
        });

        alarm = createAlarmFromScratch();

        photoLayout = (RelativeLayout) this.findViewById(R.id.photo_layout);
        Button delete_photo = (Button) this.findViewById(R.id.delete_photo);
        delete_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CameraSetupHelper.deletePhotoFile(photoFromCammera);
                photoFromCammeraUri = null;
                photoFromCammera = null;
                photoLayout.setVisibility(View.GONE);
                takePictureButton.setText(R.string.new_alarm_add_photo);
            }
        });

        alarmCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                LayoutSiteItemSubCategory selectedItem = (LayoutSiteItemSubCategory)alarmCategory.getSelectedItem();
                int selectedValue = selectedItem.getValue();
                titleCategory.setText("");
                if(LayoutSiteItemSubCategory.isCustomCategory(selectedValue))
                    titleCategory.setVisibility(View.VISIBLE);
                else
                    titleCategory.setVisibility(View.GONE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private int getRadioGroupSelectedIndex() {
        int radioButtonID = radioGroup.getCheckedRadioButtonId();
        View radioButton = radioGroup.findViewById(radioButtonID);
        return radioGroup.indexOfChild(radioButton);
    }

    private void dispatchCameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        try {
            File photo = CameraSetupHelper.setupFilePathForCameraActivity();
            photoFromCammeraUri = Uri.fromFile(photo);
            photoFromCammera = photo.getAbsolutePath();
            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoFromCammeraUri);
        } catch (Exception e) {
            e.printStackTrace();
        }
        startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
    }

    private boolean isValidAlarm() {
        boolean identifierIsValid;
        boolean categoryIsValid;
        boolean titleIsCompleted;

        Spinner alarmCategory = (Spinner) this.findViewById(R.id.new_alarm_category);
        int selectedCategoryValue = ((LayoutSiteItemSubCategory) alarmCategory.getSelectedItem()).getValue();
        categoryIsValid = selectedCategoryValue != 2;

        if(LayoutSiteItemSubCategory.isCustomCategory(((LayoutSiteItemSubCategory) alarmCategory.getSelectedItem()).getValue()))
            titleIsCompleted = !titleCategory.getText().toString().isEmpty();
        else
            titleIsCompleted = true;

        if(getRadioGroupSelectedIndex() == 1){
            EditText editText = (EditText)this.findViewById(R.id.new_alarm_terminal_provider_field);
            identifierIsValid = !editText.getText().toString().isEmpty();
        }else{
            identifierIsValid = true;
        }

        return categoryIsValid && identifierIsValid && titleIsCompleted;
    }

    private void DisplayTerminalPromotionElements(RadioGroup radioGroup) {

        int id=radioGroup.getCheckedRadioButtonId();
        View radioButton = radioGroup.findViewById(id);
        alarmCategory = (Spinner) this.findViewById(R.id.new_alarm_category);
        SpinnerItemCategoryAdapter categoryAdapter;
        TextView terminalId = (TextView) findViewById(R.id.new_alarm_terminal_provider_field);

        switch(radioButton.getId()){
            case R.id.new_alarm_type_player:
                categoryAdapter = new SpinnerItemCategoryAdapter(this, LayoutSiteItemCategory.GetSubCategories(LayoutSiteItemCategory.PLAYER), LayoutSiteItemCategory.PLAYER);
                SetVisibilityByCategory(alarmCategory, View.VISIBLE);
                break;
            case R.id.new_alarm_type_machine:
                categoryAdapter = new SpinnerItemCategoryAdapter(this, LayoutSiteItemCategory.GetSubCategories(LayoutSiteItemCategory.MACHINE), LayoutSiteItemCategory.MACHINE);
                SetVisibilityByCategory(alarmCategory, View.VISIBLE);
                break;
            default:
                categoryAdapter = new SpinnerItemCategoryAdapter(this, LayoutSiteItemCategory.GetSubCategories(LayoutSiteItemCategory.CUSTOM), LayoutSiteItemCategory.CUSTOM);
                SetVisibilityByCategory(alarmCategory, View.GONE);
                break;
        }
        terminalId.setText("");
        alarmCategory.setAdapter(categoryAdapter);
        alarmCategory.setSelection(0);
    }

    private void SetVisibilityByCategory(Spinner alarmCategory, int visibility) {

        LinearLayout terminalLayout = (LinearLayout) findViewById(R.id.terminal_layout);
        TextView terminalTitle = (TextView) findViewById(R.id.new_alarm_terminal_field_separator_label);
        TextView categoryTitle = (TextView) findViewById(R.id.new_alarm_category_field_separator_label);

        terminalLayout.setVisibility(visibility);
        alarmCategory.setVisibility(visibility);
        categoryTitle.setVisibility(visibility);
        terminalTitle.setVisibility(visibility);
    }

    private void buildDialog() {
        this.dialog = new ProgressDialog(NewAlarmActivity.this);
        this.dialog.setTitle(NewAlarmActivity.this.getResources().getString(R.string.title_progress_dialog));
        this.dialog.setMessage(NewAlarmActivity.this.getResources().getString(R.string.alarm_message_progress_dialog));
        this.dialog.setCanceledOnTouchOutside(false);
        this.dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                NewAlarmActivity.this.createAlarmAsynchTask.cancel(true);
                finish();
            }
        });
    }

    private Alarm createAlarmFromScratch() {
        this.alarm = new Alarm();
        Terminal terminal = buildTerminalFromForm();

        this.alarm.setTerminal(terminal);
        this.alarm.setDate(new Date());
        buildAlarmCategoryFromSpinner(alarm);
        this.alarm.setCompleted(false);
        this.alarm.setDescription(buildAlarmDescription());
        this.alarm.setTitle(titleCategory.getText().toString());
        return alarm;
    }

    private String buildAlarmDescription() {
        EditText desciption = (EditText)findViewById(R.id.new_alarm_description_field);
        return desciption.getText().toString();
    }

    private void buildAlarmCategoryFromSpinner(Alarm alarm) {
        Spinner spinner = (Spinner)findViewById(R.id.new_alarm_category);
        LayoutSiteItemSubCategory selectedValue = (LayoutSiteItemSubCategory) spinner.getSelectedItem();

        radioGroup = (RadioGroup) this.findViewById(R.id.radioGroupCategoryType);
        int id= radioGroup.getCheckedRadioButtonId();
        View radioButton = radioGroup.findViewById(id);
        switch(radioButton.getId()){
            case R.id.new_alarm_type_player:
            case R.id.new_alarm_type_machine:
                alarm.setSiteItemSubCategory(selectedValue);
                break;
            default:
                alarm.setSiteItemSubCategory(LayoutSiteItemSubCategory.Custom);
                break;
        }
    }

    private Terminal buildTerminalFromForm() {
        Terminal terminal = new Terminal();
        int idType=radioGroup.getCheckedRadioButtonId();
        View radioButtonType = radioGroup.findViewById(idType);
        if(radioButtonType.getId() == R.id.new_alarm_type_custom)
            terminal.setProvider("10000");
        else {
            EditText editText = (EditText) this.findViewById(R.id.new_alarm_terminal_provider_field);
            terminal.setProvider(editText.getText().toString());

        }
        return terminal;
    }

    private void presentShowcaseSequence() {

        MaterialShowcaseSequence sequence = new MaterialShowcaseSequence(this, Constants.CREATE_ALARM_SHOWCASE);
        ShowcaseConfig config = new ShowcaseConfig();
        config.setDelay(500);

        sequence.setConfig(config);
        sequence.addSequenceItem(
                new MaterialShowcaseView.Builder(this)
                        .setTarget(radioGroup)
                        .setDismissOnTouch(true)
                        .setMaskColour(getResources().getColor(R.color.showcase_blue))
                        .setContentText(getString(R.string.new_alarm_showcase_alarm_type))
                        .withRectangleShape(true)
                        .build()
        );
        sequence.addSequenceItem(
                new MaterialShowcaseView.Builder(this)
                        .setTarget(alarmCategory)
                        .setContentText(getString(R.string.new_alarm_showcase_alarm_category))
                        .setDismissOnTouch(true)
                        .setMaskColour(getResources().getColor(R.color.showcase_blue))
                        .withRectangleShape()
                        .build());
        sequence.addSequenceItem(
                new MaterialShowcaseView.Builder(this)
                        .setTarget(alarmDescription)
                        .setContentText(getString(R.string.new_alarm_showcase_alarm_description))
                        .setDismissOnTouch(true)
                        .setMaskColour(getResources().getColor(R.color.showcase_blue))
                        .withRectangleShape()
                        .build()
        );
        sequence.addSequenceItem(
                new MaterialShowcaseView.Builder(this)
                        .setTarget(takePictureButton)
                        .setContentText(getString(R.string.new_alarm_showcase_alarm_photography))
                        .setDismissOnTouch(true)
                        .setMaskColour(getResources().getColor(R.color.showcase_blue))
                        .withRectangleShape()
                        .build()

        );
        sequence.addSequenceItem(
                new MaterialShowcaseView.Builder(this)
                        .setTarget(terminalQRCodeButton)
                        .setDismissOnTouch(true)
                        .setMaskColour(getResources().getColor(R.color.showcase_blue))
                        .setContentText(getString(R.string.new_alarm_showcase_alarm_qr_scan))
                        .withRectangleShape()
                        .build()
        );
        sequence.addSequenceItem(
                new MaterialShowcaseView.Builder(this)
                        .setTarget(terminalId)
                        .setContentText(getString(R.string.new_alarm_showcase_alarm_text_identifier))
                        .setDismissOnTouch(true)
                        .setMaskColour(getResources().getColor(R.color.showcase_blue))
                        .withRectangleShape()
                        .build()
        );
        sequence.addSequenceItem(
                new MaterialShowcaseView.Builder(this)
                        .setTarget(createAlarmButton)
                        .setContentText(getString(R.string.new_alarm_showcase_alarm_create_button))
                        .setDismissOnTouch(true)
                        .setMaskColour(getResources().getColor(R.color.showcase_blue))
                        .withRectangleShape(true)
                        .build()
        );
        sequence.start();
    }

    private class HttpCreateAlarmTask extends AsyncTask<Alarm, Boolean, Boolean> {

        private DATA_PostActionResponse response;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.show();
        }

        @Override
        protected Boolean doInBackground(Alarm... params) {
            try {
                response = ServiceFactory.getInstance().getAlarmService(getApplicationContext()).createAlarm(alarm, photoFromCammera, userId, loggedUserCreateAlarmUrl, isQrInput);
                ServiceFactory.getInstance().getLayoutSiteAlarmsService(getApplicationContext()).getAlarmsCreatedByUserID(loggedUserCreateAlarmUrl, userId);
                return true;
            } catch (Exception e) {
                mLog.error("", e);
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean successfulCreate) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            if(successfulCreate){

                if(response.getResponseCode() != ResponseCode.DATA_RC_OK)
                    ToastFactory.buildSimpleToast(getResources().getString(R.string.alarm_non_created_successfully), NewAlarmActivity.this);
                else{

                    if(CameraSetupHelper.deletePhotoFile(photoFromCammera))
                        mLog.info("Photo deletion // Delete Success");
                    else
                        mLog.info("Photo deletion // Delete failed");

                    Intent i = new Intent(NewAlarmActivity.this, CreatedAlarmsListActivity.class);
                    startActivity(i);
                }

            }
            else{
                ToastFactory.buildSimpleToast(getResources().getString(R.string.alarm_non_created_successfully), NewAlarmActivity.this);
            }
        }



    }
}
