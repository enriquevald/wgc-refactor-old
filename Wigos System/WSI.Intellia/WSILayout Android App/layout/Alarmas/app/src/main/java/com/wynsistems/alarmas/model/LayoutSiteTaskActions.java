package com.wynsistems.alarmas.model;

public enum LayoutSiteTaskActions
{
    None,
    Assigned,
    Accepted,
    Resolved,
    Scaled;

    public static int GetActionId(LayoutSiteTaskActions a){
        switch (a) {
            case None:
                return 0;
            case Assigned:
                return 1;
            case Accepted:
                return 2;
            case Resolved:
                return 3;
            case Scaled:
                return 4;
            default:
                return 0;
        }
    }

    public static LayoutSiteTaskActions GetActionLayoutSiteTaskAction(int a){
        switch (a) {
            case 0:
                return None;
            case 1:
                return Assigned;
            case 2:
                return Accepted;
            case 3:
                return Resolved;
            case 4:
                return Scaled;
            default:
                return None;
        }
    }
}