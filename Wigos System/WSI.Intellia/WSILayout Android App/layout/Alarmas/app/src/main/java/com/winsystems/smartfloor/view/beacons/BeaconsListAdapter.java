package com.winsystems.smartfloor.view.beacons;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.winsystems.smartfloor.R;
import com.winsystems.smartfloor.service.login.response.BeaconAvailable;

import java.util.List;

public class BeaconsListAdapter  extends BaseAdapter{
    private final Context _context;
    private List<BeaconAvailable> beaconAvailables;
    private LayoutInflater inflater;
    public BeaconsListAdapter(Context context, List<BeaconAvailable> beaconAvailables) {
        this.beaconAvailables = beaconAvailables;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        _context = context;
    }

    @Override
    public int getCount() {
        return beaconAvailables.size();
    }

    @Override
    public Object getItem(int i) {
        return beaconAvailables.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflater.inflate(R.layout.beacon_item, null);
        BeaconAvailable beacon = this.beaconAvailables.get(i);
        TextView beaconId = (TextView) view.findViewById(R.id.beacon_id);
        beaconId.setText(String.format("MAC: %s", beacon.getMac()));
        TextView beaconDistance = (TextView) view.findViewById(R.id.beacon_distance);
        beaconDistance.setText(String.format("%s %smts", _context.getResources().getString(R.string.beacon_distance_title), round(beacon.getDistance(), 2)));

        return view;
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }
}
