package com.winsystems.smartfloor.service.mqtt;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: ServiceAutoStarter
//
// DESCRIPTION: an Service for detecting changes in packages integrity.
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public class ServiceAutoStarter extends BroadcastReceiver {
  @Override
  public void onReceive(Context context, Intent intent) {
    context.startService(new Intent(context, PahoMqttService.class));
  }
}
