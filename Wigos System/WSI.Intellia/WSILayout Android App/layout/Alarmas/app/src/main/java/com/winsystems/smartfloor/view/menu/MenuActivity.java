package com.winsystems.smartfloor.view.menu;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v4.view.MenuItemCompat;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.winsystems.smartfloor.R;
import com.winsystems.smartfloor.common.LayoutNotificationManager;
import com.winsystems.smartfloor.common.LayoutNotificationType;
import com.winsystems.smartfloor.service.login.response.BeaconAvailable;
import com.winsystems.smartfloor.service.mqtt.PahoMqttService;
import com.winsystems.smartfloor.view.alarms.CreatedAlarmsListActivity;
import com.winsystems.smartfloor.view.beacons.BeaconsActivity;
import com.winsystems.smartfloor.view.message.MessageActivity;
import com.winsystems.smartfloor.view.notifications.ListNotificationsActivity;
import com.winsystems.smartfloor.view.setting.SettingsActivity;
import com.winsystems.smartfloor.view.task.TaskListActivity;
import com.winsystems.smartfloor.view.terminal.TerminalInfoActivity;
import com.winsystems.smartfloor.view.utils.Constants;
import com.winsystems.smartfloor.view.utils.KioskModeActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MenuActivity extends KioskModeActivity implements SharedPreferences.OnSharedPreferenceChangeListener {

  private SharedPreferences prefs;
  private String language;
  private String preferenceBrokerHost = "";
  private String userId = "";



  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_menu);
    setTitle(R.string.title_activity_menu);

    prefs = PreferenceManager.getDefaultSharedPreferences(this);//get the preferences that are allowed to be given
    prefs.registerOnSharedPreferenceChangeListener(this);//set the listener to listen for changes in the preferences
    language = prefs.getString("languageToLoad", Locale.getDefault().getDisplayLanguage());

    final GridView menuListView = (GridView) findViewById( R.id.list_menu);
    runOnUiThread(new Runnable() {
      @Override
      public void run() {
        List<com.winsystems.smartfloor.view.menu.MenuItem> menuItemList = buildMenuList();
        final ListAdapter listAdapter = new MenuListAdapter(getApplicationContext(), menuItemList);
        menuListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
          @Override
          public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            com.winsystems.smartfloor.view.menu.MenuItem item = (com.winsystems.smartfloor.view.menu.MenuItem) listAdapter.getItem(position);
            Intent intent = new Intent(MenuActivity.this, item.getTargetClass());
            startActivity(intent);
          }

        });
        menuListView.setAdapter(listAdapter);

      }
    });
  }

  @Override
  protected void onStart(){
    super.onStart();

    if(!com.winsystems.smartfloor.helpers.IsLastestVersionHelper.IsLastestVersion(getApplicationContext()))
    {
      Intent broadcastIntent = new Intent();
      broadcastIntent.setAction(Constants.NEW_VERSION_AVAILABLE);
      sendBroadcast(broadcastIntent);
    }
  }

  @Override
  public void onResume(){
    super.onResume();
    String oldLanguage = language;
    language = prefs.getString("languageToLoad", Locale.getDefault().getDisplayLanguage());
    if (!oldLanguage.equals(language)){
      finish();
      startActivity(getIntent());
    }

  }

  @Override
  protected void onPause() {
    super.onPause();
  }


    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_menu, menu);
        return true;
    }*/

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {

    switch (item.getItemId()) {
      case android.R.id.home:
        return true;
      case R.id.action_settings:
        Intent i = new Intent(this, SettingsActivity.class);
        startActivityForResult(i, 1);
        return true;
    }

    return super.onOptionsItemSelected(item);
  }

  private List<com.winsystems.smartfloor.view.menu.MenuItem> buildMenuList() {
    List<com.winsystems.smartfloor.view.menu.MenuItem> menuItemList = new ArrayList<>();
    //menuItemList.add(new com.winsystems.alarmas.view.menu.MenuItem(R.drawable.ic_camera_alt_white_48dp, getResources().getString(R.string.photo_menu_item), PhotoActivity.class));
    menuItemList.add(new com.winsystems.smartfloor.view.menu.MenuItem(R.drawable.ic_list_white_48dp, getResources().getString(R.string.task_menu_item), TaskListActivity.class));
    menuItemList.add(new com.winsystems.smartfloor.view.menu.MenuItem(R.drawable.ic_add_alert_white_48dp, getResources().getString(R.string.alarm_menu_item), CreatedAlarmsListActivity.class));
    menuItemList.add(new com.winsystems.smartfloor.view.menu.MenuItem(R.drawable.machineflags, getResources().getString(R.string.title_activity_terminal_status), TerminalInfoActivity.class));
    menuItemList.add(new com.winsystems.smartfloor.view.menu.MenuItem(R.drawable.ic_question_answer_white_48dp, getResources().getString(R.string.message_title), MessageActivity.class));
    menuItemList.add(new com.winsystems.smartfloor.view.menu.MenuItem(R.drawable.ic_settings_white_48dp, getResources().getString(R.string.action_settings), SettingsActivity.class));
    menuItemList.add(new com.winsystems.smartfloor.view.menu.MenuItem(R.drawable.ic_wifi_tethering_white_48dp, "Beacons", BeaconsActivity.class));
    return menuItemList;
  }

  @Override
  public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

  }
}
