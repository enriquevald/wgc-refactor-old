package com.wynsistems.alarmas.service;

import android.content.Context;

import com.wynsistems.alarmas.service.alarm.AlarmService;
import com.wynsistems.alarmas.service.alarm.DefaultAlarmService;
import com.wynsistems.alarmas.service.alarm.DefaultLayoutSiteAlarmService;
import com.wynsistems.alarmas.service.alarm.LayoutSiteAlarmsService;
import com.wynsistems.alarmas.service.login.DefaultLoginService;
import com.wynsistems.alarmas.service.login.LoginService;
import com.wynsistems.alarmas.service.message.DefaultMessageService;
import com.wynsistems.alarmas.service.message.MessageService;
import com.wynsistems.alarmas.service.photo.DefaultPhotoService;
import com.wynsistems.alarmas.service.photo.PhotoService;
import com.wynsistems.alarmas.service.update.DefaultUpdateService;
import com.wynsistems.alarmas.service.update.UpdateService;
import com.wynsistems.alarmas.service.update.response.DATA_UpdateApplicationResponse;

public class ServiceFactory {
    private static ServiceFactory instance = null;
    private ServiceFactory() {
    }

    public static ServiceFactory getInstance(){
        if(instance== null){
            instance = new ServiceFactory();
        }

        return instance;
    }

    public AlarmService getAlarmService(Context context){
        return DefaultAlarmService.getInstance(context);
    }

    public LoginService getLoginService(Context context){
        return DefaultLoginService.getInstance(context);
    }

    public PhotoService getPhotoService() {return DefaultPhotoService.getInstance();}

    public MessageService getMessageService(Context context) {
        return DefaultMessageService.getInstance(context);
    }

    public LayoutSiteAlarmsService getLayoutSiteAlarmsService  (Context context) {return DefaultLayoutSiteAlarmService.getInstance(context);}

    public UpdateService getUpdateService(Context context) {
        return DefaultUpdateService.getInstance(context);
    }
}
