package com.winsystems.smartfloor.view.terminal;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.winsystems.smartfloor.R;
import com.winsystems.smartfloor.model.LayoutSiteAlarm;
import com.winsystems.smartfloor.helpers.AlarmResourseHelper;
import com.winsystems.smartfloor.model.LayoutSiteItemSubCategory;
import com.winsystems.smartfloor.service.login.response.LayoutSiteTaskTerminal;
import com.winsystems.smartfloor.view.utils.KioskModeActivity;

import java.util.ArrayList;
import java.util.List;

public class LayoutSiteAlarmListAdapter extends BaseAdapter {

  private List<LayoutSiteAlarm> alarms;
  private static LayoutInflater inflater = null;
  private Context _context;
  public LayoutSiteAlarmListAdapter(Context context, List<LayoutSiteAlarm> data) {
    _context = context;
    if(data != null)
      this.alarms = data;
    else
      this.alarms = new ArrayList<>();

    inflater = (LayoutInflater) context
        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
  }

  @Override
  public int getCount() {
    return this.alarms.size();
  }

  @Override
  public Object getItem(int position) {
    return this.alarms.get(position);
  }

  @Override
  public long getItemId(int position) {
    return position;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    convertView = inflater.inflate(R.layout.terminal_status_alarm_list_item, null);

    LayoutSiteAlarm alarm = (LayoutSiteAlarm)getItem(position);

    TextView terminalId = (TextView)convertView.findViewById(R.id.terminal_id);
    TextView catergoryDescription = (TextView)convertView.findViewById(R.id.category_description);
    TextView alarmId = (TextView) convertView.findViewById(R.id.alarm_created_id);
    LayoutSiteTaskTerminal terminal = alarm.getTerminal();

    if(terminal.getId() != 10000) {
      terminalId.setText(terminal.getName());
    }else
      terminalId.setText(alarm.getTitle());

    terminalId.setTextColor(Color.GRAY);
    alarmId.setText("ID: "+String.valueOf(alarm.getId()));
    alarmId.setTextColor(Color.GRAY);
    int subCategoryValue = alarm.getSubCategory().getValue();
    if(LayoutSiteItemSubCategory.isCustomCategory(subCategoryValue))
      catergoryDescription.setText(alarm.getTitle());
    else
      catergoryDescription.setText(alarm.getSubCategory().getDescription(_context));
    catergoryDescription.setTextColor(Color.GRAY);

    FrameLayout imageView = (FrameLayout) convertView.findViewById(R.id.alarm_criticality_type);
    GradientDrawable bgShape = (GradientDrawable)imageView.getBackground();
    bgShape.setColor(convertView.getResources().getColor(R.color.background_action_bar));

    ImageView alarmCategoryType = (ImageView) convertView.findViewById(R.id.alarm_category_image);
    alarmCategoryType.setImageResource(AlarmResourseHelper.getCategoryImageByType(alarm.getSubCategory().getGroupCategory()));

    return convertView;
  }
}
