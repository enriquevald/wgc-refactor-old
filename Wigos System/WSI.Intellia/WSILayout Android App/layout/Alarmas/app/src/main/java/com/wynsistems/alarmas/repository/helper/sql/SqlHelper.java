package com.wynsistems.alarmas.repository.helper.sql;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SqlHelper extends SQLiteOpenHelper{
    private static final String TABLE_NAME = "User";

    private static final String COLUMN_ID = "Id";
    private static final String COLUMN_USER_ID = "UserId";

    private static SqlHelper instance = null;
    private static final String DATABASE_NAME = "spraylogs.db";
    private static final int DATABASE_VERSION = 1;


    public static final String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;

    public static final String SAVE_IT = "CREATE TABLE  "
            + TABLE_NAME + "(" + COLUMN_ID
            + " integer primary key autoincrement, " + COLUMN_USER_ID
            + " text not null );";

    public static final String NEW_TABLE = "CREATE TABLE IF NOT EXISTS "
            + TABLE_NAME + "(" + COLUMN_ID
            + " integer primary key autoincrement, " + COLUMN_USER_ID
            + " text not null );";


    private SqlHelper(Context context) {
        super(context, context.getExternalFilesDir(null).getAbsolutePath() + "/" + DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static SQLiteOpenHelper getInstance(Context context){
        if(instance == null){
            instance = new SqlHelper(context);
        }
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(NEW_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DROP_TABLE);
        onCreate(db);
    }

}
