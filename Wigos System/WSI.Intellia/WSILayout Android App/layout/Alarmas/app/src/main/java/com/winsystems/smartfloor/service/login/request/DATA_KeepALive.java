package com.winsystems.smartfloor.service.login.request;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.util.Hashtable;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: DATA_KeepALive
//
// DESCRIPTION: Implement methods for keep alive the session.
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public class DATA_KeepALive implements KvmSerializable {

  private String sessionId;

  public String getSessionId() {
    return sessionId;
  }

  public void setSessionId(String sessionId) {
    this.sessionId = sessionId;
  }

  @Override
  public Object getProperty(int i) {
    switch (i)
    {
      case 0:
        return sessionId;
      default:
        return null;
    }
  }

  @Override
  public int getPropertyCount() {
    return 1;
  }

  @Override
  public void setProperty(int i, Object o) {
    switch (i)
    {
      case 0:
        sessionId = o.toString();
        break;
    }


  }

  @Override
  public void getPropertyInfo(int i, Hashtable hashtable, PropertyInfo propertyInfo) {
    switch (i)
    {
      case 0:
        propertyInfo.type = PropertyInfo.STRING_CLASS;
        propertyInfo.name = "SessionId";
        break;
    }
  }
}
