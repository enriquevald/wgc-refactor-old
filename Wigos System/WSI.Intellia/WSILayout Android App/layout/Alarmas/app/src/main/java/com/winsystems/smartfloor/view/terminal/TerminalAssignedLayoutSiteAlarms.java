package com.winsystems.smartfloor.view.terminal;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.winsystems.smartfloor.R;
import com.winsystems.smartfloor.helpers.SaveInformationHelper;
import com.winsystems.smartfloor.model.LayoutSiteAlarm;
import com.winsystems.smartfloor.view.utils.Constants;

public class TerminalAssignedLayoutSiteAlarms  extends ActionBarActivity {

  private SaveInformationHelper<LayoutSiteAlarm> saveInformationHelper;
  private String userId;
  @Override
  protected void onStart() {
    super.onStart();
    fillAlarmListView();
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.terminal_status_alarm_list);
    setTitle(getString(R.string.terminal_assigned_alarms_title));

    saveInformationHelper = new SaveInformationHelper<>(getApplicationContext());

    SharedPreferences settings = this.getSharedPreferences(Constants.USER_ID_PREFERENCE_EDITOR, 0) ;
    userId = settings.getString(Constants.USER_ID, userId);
  }

  private void fillAlarmListView() {
    ListView alarmListView = (ListView) findViewById(R.id.terminal_assigned_alarms);
    ListAdapter listAdapter = new LayoutSiteAlarmAdapter(this, saveInformationHelper.readList(userId + "_layoutSiteAlarms"));

    alarmListView.setAdapter(listAdapter);
  }


}

