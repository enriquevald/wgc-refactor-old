package com.wynsistems.alarmas.view.terminal;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wynsistems.alarmas.R;
import com.wynsistems.alarmas.helpers.SaveInformationHelper;
import com.wynsistems.alarmas.view.utils.Constants;

import java.util.List;

public class LogsFragment extends Fragment {
    private SaveInformationHelper<String> saveInformationHelper;
    private String userId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        saveInformationHelper = new SaveInformationHelper<>(this.getActivity().getApplicationContext());

        SharedPreferences settings = this.getActivity().getApplicationContext().getSharedPreferences(Constants.USER_ID_PREFERENCE_EDITOR, 0) ;
        userId = settings.getString(Constants.USER_ID, userId);

        View rootView = inflater.inflate(R.layout.fragment_logs, container, false);
        TextView noLogsToDisplay = (TextView) rootView.findViewById(R.id.no_logs_to_display);
        List<String> data = saveInformationHelper.readList(userId + Constants.TERMINAL_LOGS);
        if(data.size() == 0){
            noLogsToDisplay.setVisibility(View.VISIBLE);
        }else{
            noLogsToDisplay.setVisibility(View.GONE);
        }
        return rootView;
    }
}