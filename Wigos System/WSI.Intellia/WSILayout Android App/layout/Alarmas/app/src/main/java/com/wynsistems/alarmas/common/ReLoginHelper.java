package com.wynsistems.alarmas.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;

import com.wynsistems.alarmas.service.ResponseCode;
import com.wynsistems.alarmas.service.ServiceFactory;
import com.wynsistems.alarmas.service.login.response.DATA_LogInResponse;
import com.wynsistems.alarmas.view.utils.ToastFactory;

import static com.google.android.gms.internal.zzhl.runOnUiThread;

public class ReLoginHelper extends AsyncTask<Void, Void, Boolean> {
    private Context _context;
    private String _url;

    public ReLoginHelper(Context _context, String url) {
        this._context = _context;
        this._url = url;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        try {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(_context);
            String _userName = prefs.getString("userName", "");
            String _password = prefs.getString("userPassword", "");
            DATA_LogInResponse response = ServiceFactory.getInstance().getLoginService(_context).loginUser(_context, _userName, _password, _url);
            return response.getResponseCode() == ResponseCode.DATA_RC_OK;
        }catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    protected void onPostExecute(Boolean result) {
        if(result)
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ToastFactory.buildSimpleToast("Sorry, we lost the connection with the server but is already restored. Please try again.", _context);
                }
            });
    }
}
