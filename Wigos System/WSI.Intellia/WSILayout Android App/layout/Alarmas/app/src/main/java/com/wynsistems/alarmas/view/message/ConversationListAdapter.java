package com.wynsistems.alarmas.view.message;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.wynsistems.alarmas.R;
import com.wynsistems.alarmas.helpers.PendingToReadMessagesHelper;
import com.wynsistems.alarmas.model.LayoutSiteManagerRunnerConversationItem;

import java.util.ArrayList;
import java.util.List;

public class ConversationListAdapter extends BaseAdapter {

    private Context _context;
    LayoutInflater inflater;
    private List<LayoutSiteManagerRunnerConversationItem> _layoutSiteManagerRunnerConversationItems;

    public ConversationListAdapter(Context context, List<LayoutSiteManagerRunnerConversationItem> layoutSiteManagerRunnerConversationItems) {
        _context = context;
        if(layoutSiteManagerRunnerConversationItems != null)
            this._layoutSiteManagerRunnerConversationItems = layoutSiteManagerRunnerConversationItems;
        else
            this._layoutSiteManagerRunnerConversationItems = new ArrayList<>();

        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return this._layoutSiteManagerRunnerConversationItems.size();
    }

    @Override
    public Object getItem(int position) {
        return this._layoutSiteManagerRunnerConversationItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.messages_conversation_list_item, null);

        LayoutSiteManagerRunnerConversationItem item = (LayoutSiteManagerRunnerConversationItem)getItem(position);
        PendingToReadMessagesHelper pendingMessasgesHelper = new PendingToReadMessagesHelper(_context);

        TextView managerUserName = (TextView)convertView.findViewById(R.id.manager_name);
        TextView pendingMessagesCounter = (TextView) convertView.findViewById(R.id.pending_messages_count);
        FrameLayout pendingMessagesBubble = (FrameLayout) convertView.findViewById(R.id.pending_messages);
        Integer pendingMessagesCount = pendingMessasgesHelper.countPendingMessagesOfManager(item.getManagerUserName());
        if(pendingMessagesCount > 0){
            pendingMessagesCounter.setText(pendingMessagesCount.toString());
            pendingMessagesBubble.setVisibility(View.VISIBLE);
        }else{
            pendingMessagesBubble.setVisibility(View.GONE);
        }
        //managerUserName.setTextColor(_context.getResources().getColor(R.color.abc_background_cache_hint_selector_material_dark));
        managerUserName.setText(item.getManagerUserName());

        return convertView;
    }
}
