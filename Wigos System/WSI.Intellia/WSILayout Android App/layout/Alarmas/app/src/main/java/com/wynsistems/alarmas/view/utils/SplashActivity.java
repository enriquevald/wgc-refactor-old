package com.wynsistems.alarmas.view.utils;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.wynsistems.alarmas.R;
import com.wynsistems.alarmas.view.login.LoginActivity;
import com.wynsistems.alarmas.view.menu.MenuActivity;


public class SplashActivity extends KioskModeActivity {

    private static final int SPLASH_TEXT_LENGTH = 1000;
    private static final int SPLASH_DISPLAY_LENGTH = 3000;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);
        final TextView splashScreenText = (TextView)this.findViewById(R.id.splash_text);
        Handler handlerConnection = new Handler();
        /* New Handler to start the Menu-Activity
         * and close this Splash-Screen after some seconds.*/
        handlerConnection.postDelayed(new Runnable() {
            @Override
            public void run() {
                splashScreenText.setText("Updating environment…");
            }
        }, SPLASH_TEXT_LENGTH);
        handlerConnection.postDelayed(new Runnable() {
            @Override
            public void run() {
                splashScreenText.setText("Loading services…");
            }
        },SPLASH_TEXT_LENGTH*2);
        handlerConnection.postDelayed(new Runnable() {
            @Override
            public void run() {
                /* Create an Intent that will start the AlarmsGroupList-Activity. */
                Intent mainIntent = new Intent(SplashActivity.this, LoginActivity.class);
                SplashActivity.this.startActivity(mainIntent);
                SplashActivity.this.finish();
            }
        },SPLASH_DISPLAY_LENGTH);

    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
