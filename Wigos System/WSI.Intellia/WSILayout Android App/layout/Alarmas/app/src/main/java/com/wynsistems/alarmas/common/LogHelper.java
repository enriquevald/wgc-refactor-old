package com.wynsistems.alarmas.common;


import android.content.Context;
import android.util.Log;

import com.wynsistems.alarmas.view.utils.Constants;

import java.io.File;
import java.io.IOException;

public class LogHelper {
    private static SaveInformationHelper<String> _logsFile;

    public static void Configure(Context context){
        _logsFile = new SaveInformationHelper<>(context);
        File myfile = context.getFileStreamPath(Constants.LOG);
        try {
            if (myfile.exists() || myfile.createNewFile())
                Log.i("LogHelper", "Log File created");
        } catch (IOException e) {
            Log.e("LogHelper", e.getMessage());
            e.printStackTrace();
        }
    }

    public static AndroidLogger getLogger() {
        return new AndroidLogger(_logsFile);
    }

}

