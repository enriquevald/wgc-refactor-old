package com.winsystems.smartfloor.view.task;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.winsystems.smartfloor.R;
import com.winsystems.smartfloor.service.ServiceFactory;
import com.winsystems.smartfloor.service.login.response.LayoutSiteTask;
import com.winsystems.smartfloor.service.mqtt.PahoMqttService;
import com.winsystems.smartfloor.view.utils.Constants;
import com.winsystems.smartfloor.view.utils.KioskModeActivity;

public class TaskDetailActivity extends KioskModeActivity implements ActionBar.TabListener {

  private ViewPager tabsviewPager;
  private ActionBar mActionBar;
  private TaskDetailInfoTabsPageAdapter mTabsAdapter;
  private String idAlarm;
  private LayoutSiteTask task;
  private String alarmServerUrl;
  private IntentFilter intentFilter = new IntentFilter();
  private BroadcastReceiver tasksUpdatedBroadcastReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      task = ServiceFactory.getInstance().getAlarmService(getApplicationContext()).getById(task.getId().toString());
    }
  };
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_terminal_details);
    Boolean downloadAlarms = Boolean.parseBoolean(getIntent().getStringExtra(Constants.DOWNLOAD_ALARMS));
    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
    alarmServerUrl = prefs.getString("alarm_server_address", alarmServerUrl);
    if(downloadAlarms){
      idAlarm = getIntent().getStringExtra(Constants.ALARM_ID);
      task = ServiceFactory.getInstance().getAlarmService(getApplicationContext()).getById(idAlarm);
    }else {
      LayoutSiteTask selectedAlarm = (LayoutSiteTask) getIntent().getSerializableExtra(Constants.SELECTED_ALARM);
      if(selectedAlarm != null)
        task = ServiceFactory.getInstance().getAlarmService(getApplicationContext()).getById(selectedAlarm.getId().toString());
    }

    intentFilter.addAction(Constants.UPDATED_TASKS);
    intentFilter.addAction(Constants.NEW_TASK_ARRIVE);

    tabsviewPager = (ViewPager) findViewById(R.id.pager);
    mTabsAdapter = new TaskDetailInfoTabsPageAdapter(getSupportFragmentManager());
    tabsviewPager.setAdapter(mTabsAdapter);
    mActionBar =  getSupportActionBar();
    mActionBar.setHomeButtonEnabled(false);
    mActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
    android.support.v7.app.ActionBar.Tab terminalInfo = mActionBar.newTab().setIcon(R.drawable.machineflags).setTabListener(this);
    android.support.v7.app.ActionBar.Tab playerInfo = mActionBar.newTab().setIcon(R.drawable.ic_account_circle_white_18dp).setTabListener(this);
    mActionBar.setTitle(R.string.task_detail_title);
    mActionBar.addTab(terminalInfo);
    mActionBar.addTab(playerInfo);

    tabsviewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
      @Override
      public void onPageSelected(int position) {
        getSupportActionBar().setSelectedNavigationItem(position);
      }

      @Override
      public void onPageScrolled(int arg0, float arg1, int arg2) {
      }

      @Override
      public void onPageScrollStateChanged(int arg0) {
      }
    });


  }

  @Override
  public void onResume(){
    super.onResume();
    registerReceiver(tasksUpdatedBroadcastReceiver, intentFilter);


  }
  @Override
  public void onPause() {
    super.onPause();
    unregisterReceiver(tasksUpdatedBroadcastReceiver);
  }

  @Override
  public void onTabSelected(ActionBar.Tab tab, android.support.v4.app.FragmentTransaction ft) {
    tabsviewPager.setCurrentItem(tab.getPosition());
  }

  @Override
  public void onTabUnselected(ActionBar.Tab tab, android.support.v4.app.FragmentTransaction ft) {
  }

  @Override
  public void onTabReselected(ActionBar.Tab tab, android.support.v4.app.FragmentTransaction ft) {

  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();

    //noinspection SimplifiableIfStatement
    if (id == R.id.action_settings) {
      return true;
    }

    return super.onOptionsItemSelected(item);
  }

  public LayoutSiteTask getTask() {
    return task;
  }

  @Override
  public void onConnectivityChange(){

    TaskDetailsInfoFragment fragment =  (TaskDetailsInfoFragment)((TaskDetailInfoTabsPageAdapter) tabsviewPager.getAdapter()).getTaskDetailsInfoFragment();

    if(fragment == null)
    {
      return;
    }

    fragment.setActionsAvailability(getIsConnected());

  }

  @Override
  public void onKeepAliveChange (int keepAliveType){

    TaskDetailsInfoFragment fragment =  (TaskDetailsInfoFragment)((TaskDetailInfoTabsPageAdapter) tabsviewPager.getAdapter()).getTaskDetailsInfoFragment();

    if(fragment == null)
    {
      return;
    }

    fragment.setActionsAvailability(keepAliveType == 0);

  }


}
