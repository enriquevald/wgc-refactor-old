package com.winsystems.smartfloor.view.task;


import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.winsystems.smartfloor.R;
import com.winsystems.smartfloor.helpers.AlarmResourseHelper;
import com.winsystems.smartfloor.helpers.DisplayDateHelper;
import com.winsystems.smartfloor.model.LayoutSiteItemSeverity;
import com.winsystems.smartfloor.model.LayoutSiteItemSubCategory;
import com.winsystems.smartfloor.model.LayoutSiteTaskActions;
import com.winsystems.smartfloor.model.SummarizedAlarms;
import com.winsystems.smartfloor.service.login.response.LayoutSiteTask;
import com.winsystems.smartfloor.service.login.response.LayoutSiteTaskTerminal;
import com.winsystems.smartfloor.view.utils.KioskModeActivity;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class TaskExpandableListAdapter extends BaseExpandableListAdapter{


  private final LayoutInflater inflater;
  private Context _context;
  private List<SummarizedAlarms> _listAlarm;
  private Format formatter = new SimpleDateFormat("HH:mm");

  public TaskExpandableListAdapter(Context context, List<SummarizedAlarms> listAlarm) {
    this._context = context;
    this._listAlarm = listAlarm;
    inflater = (LayoutInflater) context
        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
  }

  @Override
  public int getGroupCount() {
    return _listAlarm.size();
  }

  @Override
  public int getChildrenCount(int i) {
    return _listAlarm.get(i).getTasks().size();
  }

  @Override
  public Object getGroup(int i) {
    return _listAlarm.get(i);
  }

  @Override
  public Object getChild(int i, int i1) {
    return _listAlarm.get(i).getTasks().get(i1);
  }

  @Override
  public long getGroupId(int i) {
    return i;
  }

  @Override
  public long getChildId(int i, int i1) {
    return _listAlarm.get(i).getTasks().get(i1).getId();
  }

  @Override
  public boolean hasStableIds() {
    return true;
  }

  @Override
  public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {
    view = inflater.inflate(R.layout.task_item_detail_criticality, null);
    LinearLayout layout = (LinearLayout) view.findViewById(R.id.criticality_layout);
    FrameLayout imageView = (FrameLayout) view.findViewById(R.id.list_item_icon);
    layout.setBackgroundColor(AlarmResourseHelper.getColorByCriticality(_listAlarm.get(i).getAlarmCriticality(), view.getResources()));
    TextView text = (TextView) view.findViewById(R.id.list_item_description);
    text.setText(_listAlarm.get(i).getDescription());

    TextView textTotal = (TextView) view.findViewById(R.id.list_item_total);
    textTotal.setText(Integer.toString(_listAlarm.get(i).getTotal()));
    return view;
  }

  @Override
  public View getChildView(int i, int i1, boolean b, View view, ViewGroup viewGroup) {
    View vi = view;
    if (vi == null)
      vi = inflater.inflate(R.layout.task_by_criticality_item_detail, null);

    LayoutSiteTask task = _listAlarm.get(i).getTasks().get(i1);

    TextView description = (TextView)vi.findViewById(R.id.alarm_detail_description);
    TextView terminalId = (TextView)vi.findViewById(R.id.terminal_id);
    TextView taskId = (TextView) vi.findViewById(R.id.task_id);
    TextView catergoryDescription = (TextView)vi.findViewById(R.id.category_description);
    TextView date = (TextView)vi.findViewById(R.id.alarm_date);
    if(task.getDescription() == null || task.getDescription().isEmpty())
      description.setVisibility(View.GONE);
    else {
      description.setText(task.getDescription());
      description.setVisibility(View.VISIBLE);
    }
    taskId.setText("ID: " + task.getId());
    LayoutSiteTaskTerminal terminal = task.getTerminal();
    if(terminal.getId() != 10000) {
      terminalId.setText(terminal.getName());
      terminalId.setVisibility(View.VISIBLE);
    }else
      terminalId.setVisibility(View.GONE);

    if(LayoutSiteItemSubCategory.isCustomCategory(task.getSubcategory()))
      catergoryDescription.setText(task.getTitle());
    else
      catergoryDescription.setText(LayoutSiteItemSubCategory.forCode(task.getSubcategory()).getDescription(vi.getContext()));


    String typeOfAssignation;
    Date dateOfAssignation;
    TextView textStatus = (TextView) vi.findViewById(R.id.task_status);
    if(LayoutSiteTaskActions.GetActionLayoutSiteTaskAction(task.getStatus()) == LayoutSiteTaskActions.Assigned)
    {
      typeOfAssignation = _context.getResources().getString(R.string.status_assigned);
      dateOfAssignation = task.getAssigned();
      textStatus.setText(R.string.Pending);
      textStatus.setTextColor(Color.parseColor("#c62e2b"));
    }else
    {
      typeOfAssignation = _context.getResources().getString(R.string.status_accepted);
      dateOfAssignation = task.getAccepted();
      textStatus.setText(R.string.InProgress);
      textStatus.setTextColor(Color.parseColor("#ff0057a5"));
    }
    date.setText(String.format("%s %s",typeOfAssignation,DisplayDateHelper.getDateToShowInTasks(dateOfAssignation, _context)));

    FrameLayout imageView = (FrameLayout) vi.findViewById(R.id.alarm_criticality_type);
    GradientDrawable bgShape = (GradientDrawable)imageView.getBackground();
    bgShape.setColor(AlarmResourseHelper.getColorByCriticality(LayoutSiteItemSeverity.getById(task.getSeverity()), vi.getResources()));

    ImageView alarmCategoryType = (ImageView) vi.findViewById(R.id.alarm_category_image);
    alarmCategoryType.setImageResource(AlarmResourseHelper.getCategoryImageByType(LayoutSiteItemSubCategory.forCode(task.getSubcategory()).getGroupCategory()));




    description.setTextColor(_context.getResources().getColor(R.color.winsystem_black));
    terminalId.setTextColor(_context.getResources().getColor(R.color.winsystem_black));
    taskId.setTextColor(_context.getResources().getColor(R.color.winsystem_black));
    catergoryDescription.setTextColor(_context.getResources().getColor(R.color.winsystem_black));
    date.setTextColor(_context.getResources().getColor(R.color.winsystem_black));


    return vi;
  }

  @Override
  public boolean isChildSelectable(int i, int i1) {
    return true;
  }
}
