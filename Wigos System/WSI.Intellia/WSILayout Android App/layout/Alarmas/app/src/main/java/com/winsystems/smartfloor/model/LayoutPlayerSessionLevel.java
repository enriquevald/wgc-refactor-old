package com.winsystems.smartfloor.model;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: LayoutPlayerSessionLevel
//
// DESCRIPTION: Implements method for player session level.
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public enum LayoutPlayerSessionLevel {
  LEVEL00,
  LEVEL01,
  LEVEL02,
  LEVEL03,
  LEVEL04;

  public String getText() {
    switch (this){
      case LEVEL00:
        return "LEVEL 00";
      case LEVEL01:
        return "LEVEL 01";
      case LEVEL02:
        return "LEVEL 02";
      case LEVEL03:
        return "LEVEL 03";
      default:
        return "LEVEL 04";
    }
  }
}
