package com.wynsistems.alarmas.view.alarms;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.wynsistems.alarmas.R;
import com.wynsistems.alarmas.model.Alarm;
import com.wynsistems.alarmas.model.SummarizedAlarms;
import com.wynsistems.alarmas.view.task.TaskByCriticalityListAdapter;
import com.wynsistems.alarmas.view.utils.Constants;
import com.wynsistems.alarmas.view.utils.KioskModeActivity;

public class AlarmByCriticalityActivity extends KioskModeActivity {

    private SummarizedAlarms currentSummarizedAlarms;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm_by_criticality);
        currentSummarizedAlarms = (SummarizedAlarms) getIntent().getSerializableExtra(Constants.SELECTED_GROUP_ALARMS);
        ListView alarmsView =  (ListView) findViewById( R.id.tasks_by_criticality);
        ActionBar actionBar = getSupportActionBar();
        final TaskByCriticalityListAdapter taskByCriticalityListAdapter = new TaskByCriticalityListAdapter(this,currentSummarizedAlarms.getTasks());

        alarmsView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Alarm item = (Alarm) taskByCriticalityListAdapter.getItem(position);

                Intent intent = new Intent(AlarmByCriticalityActivity.this, AlarmDetailActivity.class);
                intent.putExtra(Constants.SELECTED_ALARM, item);
                startActivity(intent);
            }
        });

        actionBar.setTitle(currentSummarizedAlarms.getDescription());
        actionBar.setDisplayUseLogoEnabled(true);

        alarmsView.setAdapter(taskByCriticalityListAdapter);
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
