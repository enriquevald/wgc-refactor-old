package com.winsystems.smartfloor.service.alarm.response;

import com.winsystems.smartfloor.model.LayoutSiteAlarm;
import com.winsystems.smartfloor.service.ResponseCode;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.util.Hashtable;
import java.util.List;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: DATA_TerminalInfoResponse
//
// DESCRIPTION: Implements methods for get terminal info.
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public class DATA_TerminalInfoResponse implements KvmSerializable{

  private String sessionId;
  private Integer responseCode;
  private String responseCodeDescription;
  private List<LayoutSiteAlarm> alarms;
  private List<String> log;

  public void setSessionId(String sessionId) {

    this.sessionId = sessionId;
  }

  public String getSessionId() {
    return sessionId;
  }

  public void setResponseCode(Integer responseCode) {
    this.responseCode = responseCode;
  }

  public void setResponseCodeDescription(String responseCodeDescription) {
    this.responseCodeDescription = responseCodeDescription;
  }

  public void setAlarms(List<LayoutSiteAlarm> alarms) {
    this.alarms = alarms;
  }

  public ResponseCode getResponseCode(){
    return ResponseCode.getResponseCode(responseCode);
  }

  public String getResponseCodeDescription() {
    return responseCodeDescription;
  }

  public List<LayoutSiteAlarm> getAlarms() {
    return alarms;
  }

  public List<String> getLog() {
    return log;
  }

  @Override
  public Object getProperty(int i) {
    switch (i){
      case 0:
        return responseCode;
      case 1:
        return responseCodeDescription;
      case 2:
        return sessionId;
      case 3:
        return alarms;
      case 4:
        return log;
      default:
        return null;
    }
  }

  @Override
  public int getPropertyCount() {
    return 5;
  }

  @Override
  public void setProperty(int i, Object o) {
    switch(i)
    {
      case 0:
        responseCode = Integer.parseInt(o.toString());
        break;
      case 1:
        responseCodeDescription = o.toString();
        break;
      case 2:
        sessionId = o.toString();
        break;
      case 3:
        alarms = (List<LayoutSiteAlarm>)o;
        break;
      case 4:
        log = (List<String>)o;
        break;
      default:
        break;
    }
  }

  @Override
  public void getPropertyInfo(int i, Hashtable hashtable, PropertyInfo propertyInfo) {
    switch(i)
    {
      case 0:
        propertyInfo.type = PropertyInfo.INTEGER_CLASS;
        propertyInfo.name = "ResponseCode";
        break;
      case 1:
        propertyInfo.type = PropertyInfo.STRING_CLASS;
        propertyInfo.name = "ResponseCodeDescription";
        break;
      case 2:
        propertyInfo.type = PropertyInfo.STRING_CLASS;
        propertyInfo.name = "SessionId";
        break;
      case 3:
        propertyInfo.type = PropertyInfo.VECTOR_CLASS;
        propertyInfo.name = "Alarms";
        break;
      case 4:
        propertyInfo.type = PropertyInfo.VECTOR_CLASS;
        propertyInfo.name = "Log";
        break;
      default:break;
    }
  }
}
