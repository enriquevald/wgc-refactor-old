package com.winsystems.smartfloor.service.alarm;

import com.winsystems.smartfloor.service.alarm.response.DATA_TerminalInfoResponse;

public interface LayoutSiteAlarmsService{

  public DATA_TerminalInfoResponse getTerminalStatusByTerminalID(String url, String sessionId, String terminalId);
  public DATA_TerminalInfoResponse getAlarmsCreatedByUserID(String url, String sessionId);
}
