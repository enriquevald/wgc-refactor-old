package com.winsystems.smartfloor.service.login;

import android.content.Context;

import com.winsystems.smartfloor.service.DATA_PostActionResponse;
import com.winsystems.smartfloor.service.login.response.DATA_LogInResponse;

public interface LoginService {
  DATA_LogInResponse loginUser(Context appContext, String username, String password, String url, String deviceId);
  DATA_PostActionResponse keepALive(Context appContext, String session,String url);
}
