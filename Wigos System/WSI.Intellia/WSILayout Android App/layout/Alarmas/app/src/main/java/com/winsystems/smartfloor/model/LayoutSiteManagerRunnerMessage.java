package com.winsystems.smartfloor.model;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.io.Serializable;
import java.util.Date;
import java.util.Hashtable;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: LayoutSiteManagerRunnerMessage
//
// DESCRIPTION: Implements method for site manager runner message.
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public class LayoutSiteManagerRunnerMessage implements KvmSerializable, Serializable {
    private Integer id;
    private Date date;
    private Long managerId;
    private String managerUserName;
    private Long runnerId;
    private String runnerUserName;
    private String message;
    private Integer source;
    private Integer status;

    public Integer getId() {
        return id;
    }
    public Integer getStatus() {
        return status;
    }
    public Date getDate() {
        return date;
    }

    public Long getManagerId() {
        return managerId;
    }

    public String getManagerUserName() {
        return managerUserName;
    }

    public Long getRunnerId() {
        return runnerId;
    }

    public String getRunnerUserName() {
        return runnerUserName;
    }

    public String getMessage() {
        if(message == null)
            return "";
        return message;
    }

    public Integer getSource() {
        return source;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public void setStatus(Integer status) {
        this.status = status;
    }
    public void setDate(Date date) {
        this.date = date;
    }

    public void setManagerId(Long managerId) {
        this.managerId = managerId;
    }

    public void setManagerUserName(String managerUserName) {
        this.managerUserName = managerUserName;
    }

    public void setRunnerId(Long runnerId) {
        this.runnerId = runnerId;
    }

    public void setRunnerUserName(String runnerUserName) {
        this.runnerUserName = runnerUserName;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setSource(Integer source) {
        this.source = source;
    }

    @Override
    public Object getProperty(int i) {
        switch (i){
            case 0:
                return date;
            case 1:
                return id;
            case 2:
                return managerId;
            case 3:
                return managerUserName;
            case 4:
                return message;
            case 5:
                return runnerId;
            case 6:
                return runnerUserName;
            case 7:
                return source;
            case 8:
                return status;
            default:
                return null;
        }
    }

    @Override
    public int getPropertyCount() {
        return 9;
    }

    @Override
    public void setProperty(int i, Object o) {
        switch (i){
            case 0:
                date = (Date)o;
                break;
            case 1:
                id = Integer.parseInt(o.toString());
                break;
            case 2:
                managerId = Long.parseLong(o.toString());
                break;
            case 3:
                managerUserName = o.toString();
                break;
            case 4:
                message = o.toString();
                break;
            case 5:
                runnerId = Long.parseLong(o.toString());
                break;
            case 6:
                runnerUserName = o.toString();
                break;
            case 7:
                source = Integer.parseInt(o.toString());
                break;
            case 8:
                status = Integer.parseInt(o.toString());
                break;
        }
    }

    @Override
    public void getPropertyInfo(int i, Hashtable hashtable, PropertyInfo propertyInfo) {
        switch (i){
            case 0:
                propertyInfo.name = "Date";
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                break;
            case 1:
                propertyInfo.name = "Id";
                propertyInfo.type = PropertyInfo.INTEGER_CLASS;
                break;
            case 2:
                propertyInfo.name = "ManagerId";
                propertyInfo.type = PropertyInfo.LONG_CLASS;
                break;
            case 3:
                propertyInfo.name = "ManagerUserName";
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                break;
            case 4:
                propertyInfo.name = "Message";
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                break;
            case 5:
                propertyInfo.name = "RunnerId";
                propertyInfo.type = PropertyInfo.LONG_CLASS;
                break;
            case 6:
                propertyInfo.name = "RunnerUserName";
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                break;
            case 7:
                propertyInfo.name = "Source";
                propertyInfo.type = PropertyInfo.INTEGER_CLASS;
                break;
            case 8:
                propertyInfo.name = "Status";
                propertyInfo.type = PropertyInfo.INTEGER_CLASS;
                break;
        }
    }
}
