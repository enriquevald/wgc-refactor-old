package com.winsystems.smartfloor.view.alarms;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Base64;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.winsystems.smartfloor.R;
import com.winsystems.smartfloor.helpers.AlarmResourseHelper;
import com.winsystems.smartfloor.model.LayoutSiteAlarm;
import com.winsystems.smartfloor.model.LayoutSiteItemSubCategory;
import com.winsystems.smartfloor.view.utils.Constants;
import com.winsystems.smartfloor.view.utils.KioskModeActivity;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: AlarmDetailActivity
//
// DESCRIPTION: Activity that implements details alarm functioanlity.
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public class AlarmDetailActivity extends KioskModeActivity {
  private LayoutSiteAlarm currentAlarm;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_alarm_detail);

    this.currentAlarm = (LayoutSiteAlarm) getIntent().getSerializableExtra(Constants.SELECTED_ALARM);

    fillLayoutUsingCurrentAlarm(this);
  }

  private void fillLayoutUsingCurrentAlarm(ActionBarActivity activity) {
    TextView description = (TextView) activity.findViewById(R.id.alarm_detail_description);
    TextView terminalId = (TextView) activity.findViewById(R.id.terminal_id);
    TextView taskId = (TextView) activity.findViewById(R.id.task_id);
    TextView categoryDescription = (TextView) activity.findViewById(R.id.category_description);

    if(currentAlarm.getDescription() == null || currentAlarm.getDescription().isEmpty())
      description.setVisibility(View.GONE);
    else
      description.setText(currentAlarm.getDescription());
    taskId.setText("ID: " + currentAlarm.getId());
    terminalId.setText(currentAlarm.getTerminal().getName());

    if(!LayoutSiteItemSubCategory.isCustomCategory(currentAlarm.getSubCategory().getValue())){
      categoryDescription.setText(currentAlarm.getSubCategory().getDescription(getApplicationContext()));
    }else{
      categoryDescription.setText(currentAlarm.getTitle());
    }

    ImageView alarmImage = (ImageView) activity.findViewById(R.id.alarm_image);
    ImageView alarmCategoryType = (ImageView) activity.findViewById(R.id.alarm_category_image);
    alarmCategoryType.setImageResource(AlarmResourseHelper.getCategoryImageByType(currentAlarm.getSubCategory().getGroupCategory()));


    if(!currentAlarm.getImageData().isEmpty()) {
      byte[] decodedString = Base64.decode(currentAlarm.getImageData(), Base64.DEFAULT);
      Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
      alarmImage.setVisibility(View.VISIBLE);
      alarmImage.setImageBitmap(decodedByte);
    }else{
      alarmImage.setVisibility(View.GONE);
    }



    ActionBar actionBar = getSupportActionBar();
    if (actionBar != null) {
      actionBar.setTitle(R.string.alarm_info);
    }
  }


  @Override
  protected void onStop() {
    super.onStop();
  }
}
