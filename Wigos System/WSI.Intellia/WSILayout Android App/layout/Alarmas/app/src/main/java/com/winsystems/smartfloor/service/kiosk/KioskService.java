package com.winsystems.smartfloor.service.kiosk;

import android.app.ActivityManager;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.winsystems.smartfloor.common.AndroidLogger;
import com.winsystems.smartfloor.helpers.KioskModeHelper;
import com.winsystems.smartfloor.helpers.LogHelper;
import com.winsystems.smartfloor.view.login.LoginActivity;

import java.util.List;
import java.util.concurrent.TimeUnit;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: KioskService.
//
// DESCRIPTION: KioskService.
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public class KioskService extends Service {

  private static final long INTERVAL = TimeUnit.SECONDS.toMillis(1); // periodic interval to check in seconds -> 2 seconds
  private static final String TAG = KioskService.class.getSimpleName();

  private Thread t = null;
  private Context ctx = null;
  private boolean running = false;
  private AndroidLogger mLog;

  @Override
  public void onDestroy() {
    mLog = LogHelper.getLogger(ctx);
    mLog.info("Stopping service 'KioskService'");
    running =false;
    super.onDestroy();
  }

  @Override
  public int onStartCommand(Intent intent, int flags, int startId) {
    mLog = LogHelper.getLogger(ctx);
    mLog.info("Starting service 'KioskService'");
    running = true;
    ctx = this;

    // start a thread that periodically checks if your app is in the foreground
    t = new Thread(new Runnable() {
      @Override
      public void run() {
        do {
          handleKioskMode();
          try {
            Thread.sleep(INTERVAL);
          } catch (InterruptedException e) {
            mLog.info("Thread interrupted: 'KioskService'");
          }
        }while(running);
        stopSelf();
      }
    });

    t.start();
    return Service.START_NOT_STICKY;
  }

  private void handleKioskMode() {
    // is Kiosk Mode active?
    if(KioskModeHelper.isKioskModeActive(getApplicationContext())) {
      // is App in background?
      if(isInBackground()) {
        restoreApp(); // restore!
      }
    }
  }

  private boolean isInBackground() {
    ActivityManager am = (ActivityManager) ctx.getSystemService(Context.ACTIVITY_SERVICE);

    List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
    ComponentName componentInfo = taskInfo.get(0).topActivity;
    return (!ctx.getApplicationContext().getPackageName().equals(componentInfo.getPackageName()));
  }

  private void restoreApp() {
    // Restart activity
    Intent i = new Intent(ctx, LoginActivity.class);
    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    ctx.startActivity(i);
  }

  @Override
  public IBinder onBind(Intent intent) {
    return null;
  }
}