package com.wynsistems.alarmas.service.login.request;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.util.Hashtable;

public class DATA_LogIn implements KvmSerializable {

    public String login;
    public String idOperador;
    public String password;
    public String deviceId;

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setIdOperador(String idOperador) {
        this.idOperador = idOperador;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    @Override
    public Object getProperty(int index) {
        switch (index){
            case 0:
                return deviceId;
            case 1:
                return idOperador;
            case 2:
                return login;
            case 3:
                return password;
            default:
                return null;
        }
    }

    @Override
    public int getPropertyCount() {
        return 4;
    }

    @Override
    public void setProperty(int index, Object value) {
        switch(index)
        {
            case 0:
                deviceId = value.toString();
                break;
            case 1:
                idOperador = value.toString();
                break;
            case 2:
                login = value.toString();
                break;
            case 3:
                password = value.toString();
                break;
            default:
                break;
        }
    }

    @Override
    public void getPropertyInfo(int index, Hashtable properties, PropertyInfo info) {
        switch(index)
        {
            case 0:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "DeviceId";
                break;
            case 1:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "IdOperador";
                break;
            case 2:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Login";
                break;
            case 3:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Password";
                break;
            default:break;
        }
    }
}
