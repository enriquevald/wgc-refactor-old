package com.winsystems.smartfloor.view.menu;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.winsystems.smartfloor.R;

import java.util.List;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: NavigationListAdapter
//
// DESCRIPTION: Implements methods that correspond to the navigation list menu of the application.
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public class NavigationListAdapter extends BaseAdapter {

  private final List<NavigationItem> menuItemList;
  private final LayoutInflater inflater;
  private final Context _context;

  public NavigationListAdapter(Context context, List<NavigationItem> menuItemList) {
    this.menuItemList = menuItemList;
    _context = context;
    inflater = (LayoutInflater) context
        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
  }

  @Override
  public int getCount() {
    return menuItemList.size();
  }

  @Override
  public Object getItem(int position) {
    return menuItemList.get(position);
  }

  @Override
  public long getItemId(int position) {
    return position;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    View vi = convertView;
    if (vi == null)
      vi = inflater.inflate(R.layout.navigation_list_item, null);

    NavigationItem menuItem = (NavigationItem) this.getItem(position);
    TextView navItemText = (TextView) vi.findViewById(R.id.nav_item_text);
    TextView navItemCounter = (TextView) vi.findViewById(R.id.nav_item_counter);
    ImageView navItemImg = (ImageView) vi.findViewById(R.id.nav_item_img);

    navItemText.setText(menuItem.getTitle());
    navItemImg.setImageResource(menuItem.getImage());
    navItemCounter.setText(menuItem.getCount());


    return vi;
  }
}