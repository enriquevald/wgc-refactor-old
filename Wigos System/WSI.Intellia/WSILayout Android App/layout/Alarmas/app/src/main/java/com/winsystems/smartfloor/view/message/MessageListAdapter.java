package com.winsystems.smartfloor.view.message;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.winsystems.smartfloor.R;
import com.winsystems.smartfloor.helpers.DisplayDateHelper;
import com.winsystems.smartfloor.model.LayoutSiteManagerRunnerMessage;
import com.winsystems.smartfloor.model.LayoutSiteManagerRunnerMessageSource;
import com.winsystems.smartfloor.view.utils.Constants;

import java.util.List;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: MessageListAdapter
//
// DESCRIPTION: Activity that implements the functionality of message list adapter.
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public class MessageListAdapter extends BaseAdapter{

    private List<LayoutSiteManagerRunnerMessage> messages;
    private Context context;
    private static LayoutInflater inflater;
    public MessageListAdapter(Context applicationContext, List<LayoutSiteManagerRunnerMessage> messageList) {
        context = applicationContext;
        messages = messageList;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setListData(List<LayoutSiteManagerRunnerMessage> data){
        messages = data;
    }


    @Override
    public int getCount() {
        return this.messages.size();
    }

    @Override
    public Object getItem(int position) {
        return this.messages.get(position);
    }

    @Override
    public long getItemId(int position) {
        return this.messages.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null)
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        LayoutSiteManagerRunnerMessage message = (LayoutSiteManagerRunnerMessage)getItem(position);
        if(message.getSource() == LayoutSiteManagerRunnerMessageSource.Manager.getValue()){
            convertView = inflater.inflate(R.layout.message_bubble_item_manager, parent, false);

            TextView messageContent = (TextView)convertView.findViewById(R.id.message_content);
            TextView messageDate = (TextView)convertView.findViewById(R.id.message_date);
            FrameLayout messageBubble = (FrameLayout) convertView.findViewById(R.id.message_bubble);
            GradientDrawable bgShapeBubble = (GradientDrawable)messageBubble.getBackground();
            FrameLayout messageArrow = (FrameLayout) convertView.findViewById(R.id.message_bubble);
            GradientDrawable bgShapeArrow = (GradientDrawable)messageArrow.getBackground();
            String messageDateToShow = DisplayDateHelper.getDateToShowInMessages(message.getDate());

            messageDate.setText(messageDateToShow);
            messageContent.setText(message.getMessage());
            bgShapeBubble.setColor(convertView.getResources().getColor(R.color.manager_color));
            bgShapeArrow.setColor(convertView.getResources().getColor(R.color.manager_color));
        }else{
            convertView = inflater.inflate(R.layout.message_bubble_item_runner, parent, false);
            TextView messageContent = (TextView)convertView.findViewById(R.id.message_content);
            TextView messageDate = (TextView)convertView.findViewById(R.id.message_date);
            FrameLayout messageBubble = (FrameLayout) convertView.findViewById(R.id.message_bubble);
            GradientDrawable bgShapeBubble = (GradientDrawable)messageBubble.getBackground();
            FrameLayout messageArrow = (FrameLayout) convertView.findViewById(R.id.message_bubble);
            GradientDrawable bgShapeArrow = (GradientDrawable)messageArrow.getBackground();
            FrameLayout messageStatusIcon = (FrameLayout) convertView.findViewById(R.id.message_manager_status);
            messageStatusIcon.setBackgroundResource(this.getStatusIconRes(message.getStatus()));
            String messageDateToShow = DisplayDateHelper.getDateToShowInMessages(message.getDate());

            messageDate.setText(messageDateToShow);
            messageContent.setText(message.getMessage());
            bgShapeBubble.setColor(convertView.getResources().getColor(R.color.runner_color));
            bgShapeArrow.setColor(convertView.getResources().getColor(R.color.runner_color));
        }
        return convertView;
    }
    private int getStatusIconRes(int status){
        int _result;

        switch (status){
            case Constants.MESSAGE_STATUS_DELAYED:
                _result =  R.drawable.ic_message_delayed;
                break;
            case Constants.MESSAGE_STATUS_DELIVERED:
                _result =  R.drawable.ic_message_delivered;
                break;
            case Constants.MESSAGE_STATUS_SENT:
                _result =  R.drawable.ic_message_sent;
                break;
            case Constants.MESSAGE_STATUS_HASREAD:
                _result =  R.drawable.ic_message_hasread;
                break;
            default:
                _result =  R.drawable.ic_message_sent;
        }

        return _result;
    }

}
