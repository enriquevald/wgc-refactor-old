package com.winsystems.smartfloor.repository;

import android.content.Context;

import com.winsystems.smartfloor.model.LayoutSiteItemGroupCategory;
import com.winsystems.smartfloor.model.LayoutSiteItemSeverity;
import com.winsystems.smartfloor.service.login.response.LayoutSiteTask;

import java.util.List;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: TaskRepository
//
// DESCRIPTION: Interfaces and classes.
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public interface TaskRepository {
  LayoutSiteTask getById(String id);
  void saveFromAlarmResponse(List<LayoutSiteTask> tasks, Context context);
  List<LayoutSiteTask> getAlarmByCriticality(LayoutSiteItemSeverity alarmCriticality);
  List<LayoutSiteTask> getAlarmByCategory(LayoutSiteItemGroupCategory type);
  List<LayoutSiteTask> getAlarmsInProgress();
  List<LayoutSiteTask> getAlarmsAssigned();
}
