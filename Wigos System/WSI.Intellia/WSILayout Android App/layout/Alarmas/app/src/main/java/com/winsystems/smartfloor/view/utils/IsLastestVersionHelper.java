package com.winsystems.smartfloor.view.utils;


import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.winsystems.smartfloor.R;

public class IsLastestVersionHelper {

  public static boolean IsLastestVersion(Context context){
    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
    String lastVersion = preferences.getString("lastVersionApp", "");
    String currentVersion = context.getResources().getString(R.string.version);

    return lastVersion.equals(currentVersion);
  }
}
