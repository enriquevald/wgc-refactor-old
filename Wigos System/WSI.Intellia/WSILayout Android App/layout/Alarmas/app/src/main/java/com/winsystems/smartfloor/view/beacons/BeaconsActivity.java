package com.winsystems.smartfloor.view.beacons;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.widget.ListView;

import com.winsystems.smartfloor.R;
import com.winsystems.smartfloor.helpers.SaveInformationHelper;
import com.winsystems.smartfloor.service.login.response.BeaconAvailable;
import com.winsystems.smartfloor.view.utils.Constants;
import com.winsystems.smartfloor.view.utils.KioskModeActivity;

public class BeaconsActivity extends KioskModeActivity {
  private ListView beaconsList;
  private SaveInformationHelper<BeaconAvailable> beaconsDetected;
  private IntentFilter intentFilter = new IntentFilter(Constants.BEACONS_DETECTED);
  private BroadcastReceiver beaconsBroadcastReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      runOnUiThread(new Runnable() {
        @Override
        public void run() {
          fillBeaconsList();
        }
      });
    }
  };
  @Override
  protected void onCreate(Bundle bundle){
    super.onCreate(bundle);
    setContentView(R.layout.beacons_list);
    beaconsDetected = new SaveInformationHelper<>(this);
    beaconsList = (ListView) findViewById(R.id.beacons_list);
    fillBeaconsList();
  }

  private void fillBeaconsList() {
    BeaconsListAdapter beaconsListAdapter = new BeaconsListAdapter(this, beaconsDetected.readList(Constants.BEACONS_DETECTED));
    beaconsList.setAdapter(beaconsListAdapter);
  }

  @Override
  public void onResume(){
    super.onResume();
    registerReceiver(beaconsBroadcastReceiver, intentFilter);
  }

  @Override
  public void onPause(){
    super.onPause();
    unregisterReceiver(beaconsBroadcastReceiver);
  }
}
