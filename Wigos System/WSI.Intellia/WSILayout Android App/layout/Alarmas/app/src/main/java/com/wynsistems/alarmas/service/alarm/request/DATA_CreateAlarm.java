package com.wynsistems.alarmas.service.alarm.request;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.util.Hashtable;

public class DATA_CreateAlarm implements KvmSerializable {
    private String userId;
    private String description;
    private int severity;
    private int subCategory;
    private int category;
    private String terminalId;
    private String image;

    @Override
    public Object getProperty(int i) {
        switch (i){
            case 0:
                return category;
            case 1:
                return description;
            case 2:
                return image;
            case 3:
                return severity;
            case 4:
                return subCategory;
            case 5:
                return terminalId;
            case 6:
                return userId;
            default:
                return null;
        }
    }

    @Override
    public int getPropertyCount() {
        return 7;
    }

    @Override
    public void setProperty(int i, Object o) {
        switch (i){
            case 0:
                category = Integer.parseInt(o.toString());
            case 1:
                description = o.toString();
            case 2:
                image = o.toString();
            case 3:
                severity= Integer.parseInt(o.toString());
            case 4:
                subCategory = Integer.parseInt(o.toString());
            case 5:
                terminalId = o.toString();
            case 6:
                userId = o.toString();

        }
    }

    @Override
    public void getPropertyInfo(int i, Hashtable hashtable, PropertyInfo propertyInfo) {
          switch (i){
                case 0:
                    propertyInfo.type = PropertyInfo.INTEGER_CLASS;
                    propertyInfo.name = "Category";
                    break;
                case 1:
                    propertyInfo.type = PropertyInfo.STRING_CLASS;
                    propertyInfo.name = "Description";
                    break;
                case 2:
                    propertyInfo.type = PropertyInfo.STRING_CLASS;
                    propertyInfo.name = "Image";
                    break;
                case 3:
                    propertyInfo.type = PropertyInfo.INTEGER_CLASS;
                    propertyInfo.name = "Severity";
                    break;
                case 4:
                    propertyInfo.type = PropertyInfo.INTEGER_CLASS;
                    propertyInfo.name = "SubCategory";
                    break;
                case 5:
                    propertyInfo.type = PropertyInfo.STRING_CLASS;
                    propertyInfo.name = "TerminalId";
                    break;
                case 6:
                    propertyInfo.type = PropertyInfo.STRING_CLASS;
                    propertyInfo.name = "UserId";
                    break;
        }
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public void setSubCategory(int subCategory) {
        this.subCategory = subCategory;
    }

    public void setSeverity(int severity) {
        this.severity = severity;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public void setUserId(String userId) {
        this.userId = userId;
    }
}
