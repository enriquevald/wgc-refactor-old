package com.wynsistems.alarmas.service.photo;

import com.wynsistems.alarmas.service.DATA_PostActionResponse;

public interface PhotoService {

    DATA_PostActionResponse UploadPhoto(String url, String userName, String imagePath, String comment, String externalId, Integer type);
}
