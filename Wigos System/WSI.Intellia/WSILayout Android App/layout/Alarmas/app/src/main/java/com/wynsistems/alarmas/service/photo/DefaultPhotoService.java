package com.wynsistems.alarmas.service.photo;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;

import com.wynsistems.alarmas.common.AndroidLogger;
import com.wynsistems.alarmas.helpers.LogHelper;
import com.wynsistems.alarmas.service.DATA_PostActionResponse;
import com.wynsistems.alarmas.service.photo.request.DATA_LayoutSiteMediaAsset;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.ByteArrayOutputStream;

public class DefaultPhotoService implements PhotoService{
    private static PhotoService instance = null;
    AndroidLogger mLog = null;//LogHelper.getLogger();
    private DefaultPhotoService(){}

    @Override
    public DATA_PostActionResponse UploadPhoto(String url, String userId, String imagePath, String comment, String externalId, Integer type) {
        final String NAMESPACE = "http://winsystemsintl.com/";
        final String METHOD_NAME = "UploadLayoutSiteMediaAsset";
        final String SOAP_ACTION = "http://winsystemsintl.com/IDATAService/UploadLayoutSiteMediaAsset";

        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        String mediaEncoded = Base64.encodeToString(compressMedia(imagePath), Base64.DEFAULT);

        DATA_LayoutSiteMediaAsset layoutSiteMediaAsset = new DATA_LayoutSiteMediaAsset();
        layoutSiteMediaAsset.setSessionId(userId);
        layoutSiteMediaAsset.setDescription(comment);
        layoutSiteMediaAsset.setType(type);
        layoutSiteMediaAsset.setMediaType(1);//PHOTO
        layoutSiteMediaAsset.setExternalId(externalId);
        layoutSiteMediaAsset.setData(mediaEncoded);

        DATA_PostActionResponse postActionResponse = new DATA_PostActionResponse();

        PropertyInfo pi = new PropertyInfo();
        pi.setName("media");
        pi.setValue(layoutSiteMediaAsset);
        pi.setType(layoutSiteMediaAsset.getClass());

        request.addProperty(pi);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;

        envelope.setOutputSoapObject(request);

        envelope.addMapping(NAMESPACE, "DATA_LayoutSiteMediaAsset", layoutSiteMediaAsset.getClass());
        envelope.addMapping(NAMESPACE, "DATA_PostActionResponse", postActionResponse.getClass());

        HttpTransportSE transporte = new HttpTransportSE(url, 60000);

        try {
            transporte.call(SOAP_ACTION, envelope);

            SoapObject resSoap = (SoapObject) envelope.getResponse();

            postActionResponse.setResponseCode(Integer.parseInt(resSoap.getProperty(0).toString()));
            postActionResponse.setResponseCodeDescription(resSoap.getProperty(1).toString());
            postActionResponse.setSessionId(resSoap.getProperty(2).toString());

        } catch (Exception e) {
            mLog.error("DefaultPhotoService - UploadPhoto - " + e.getMessage(), e);
            Log.e("DefaultPhotoService", e.getMessage());
        }

        return postActionResponse;
    }

    private byte[] compressMedia(String imagePath) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 8;

        Bitmap bm = BitmapFactory.decodeFile(imagePath, options);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        return stream.toByteArray();
    }

    public static PhotoService getInstance() {
        if(instance == null){
            instance = new DefaultPhotoService();
        }
        return instance;
    }
}
