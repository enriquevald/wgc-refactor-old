package com.winsystems.smartfloor.view.terminal;

import android.app.ActionBar;
import android.os.Bundle;
import android.support.v4.view.ViewPager;

import com.winsystems.smartfloor.R;
import com.winsystems.smartfloor.view.utils.Constants;
import com.winsystems.smartfloor.view.utils.KioskModeActivity;

public class TerminalInfoMenuActivity extends KioskModeActivity implements android.support.v7.app.ActionBar.TabListener{

  private ViewPager tabsviewPager;
  private android.support.v7.app.ActionBar mActionBar;
  private TabsPagerAdapter mTabsAdapter;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_terminal_status_menu);
    Integer terminalId = getIntent().getIntExtra(Constants.TERMINAL_ID, 0);
    String terminalName = getIntent().getStringExtra(Constants.TERMINAL_NAME);
    tabsviewPager = (ViewPager) findViewById(R.id.pager);

    mTabsAdapter = new TabsPagerAdapter(getSupportFragmentManager());

    tabsviewPager.setAdapter(mTabsAdapter);
    mActionBar =  getSupportActionBar();
    mActionBar.setHomeButtonEnabled(false);
    mActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

    android.support.v7.app.ActionBar.Tab alarms = mActionBar.newTab().setText(R.string.alarms_title).setTabListener(this);
    //android.support.v7.app.ActionBar.Tab logs = mActionBar.newTab().setText(R.string.terminal_logs).setTabListener(this);

    mActionBar.addTab(alarms);
    //mActionBar.addTab(logs);
    if(terminalName != null)
      mActionBar.setTitle(terminalId + " - " + terminalName);
    else
      mActionBar.setTitle(terminalId);

    mActionBar.setDisplayShowHomeEnabled(true);
    mActionBar.setDisplayUseLogoEnabled(true);
    tabsviewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
      @Override
      public void onPageSelected(int position) {
        getSupportActionBar().setSelectedNavigationItem(position);
      }

      @Override
      public void onPageScrolled(int arg0, float arg1, int arg2) {
      }

      @Override
      public void onPageScrollStateChanged(int arg0) {
      }
    });

  }

  @Override
  public void onTabSelected(android.support.v7.app.ActionBar.Tab tab, android.support.v4.app.FragmentTransaction fragmentTransaction) {
    tabsviewPager.setCurrentItem(tab.getPosition()); //update tab position on tap
  }

  @Override
  public void onTabUnselected(android.support.v7.app.ActionBar.Tab tab, android.support.v4.app.FragmentTransaction fragmentTransaction) {
  }

  @Override
  public void onTabReselected(android.support.v7.app.ActionBar.Tab tab, android.support.v4.app.FragmentTransaction fragmentTransaction) {
  }
}
