package com.wynsistems.alarmas.service.login.response;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.io.Serializable;
import java.util.Hashtable;

public class BeaconAvailable implements KvmSerializable, Serializable {

    public Integer layoutObjectId;
    public String mac;
    public Double positionX;
    public Double positionY;

    public Integer getPositionFloorId() {
        return positionFloorId;
    }

    public Integer positionFloorId;
    public Double distance;

    public Double getDistance() {
        return distance;
    }

    public Double getPositionX() {
        return positionX;
    }

    public Double getPositionY() {
        return positionY;
    }

    public String getMac() {
        return mac;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public void setLayoutObjectId(Integer layoutObjectId) {
        this.layoutObjectId = layoutObjectId;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public void setPositionX(Double positionX) {
        this.positionX = positionX;
    }

    public void setPositionY(Double positionY) {
        this.positionY = positionY;
    }

    public void setPositionFloorId(Integer positionFloorId) {
        this.positionFloorId = positionFloorId;
    }

    @Override
    public Object getProperty(int i) {
        switch (i){
            case 0:
                return layoutObjectId;
            case 1:
                return mac;
            case 2:
                return positionFloorId;
            case 3:
                return positionX;
            case 4:
                return positionY;
            default:
                return null;
        }
    }

    @Override
    public int getPropertyCount() {
        return 6;
    }

    @Override
    public void setProperty(int i, Object o) {
        switch (i){
            case 0:
                layoutObjectId = Integer.parseInt(o.toString());
                break;
            case 1:
                mac = o.toString();
                break;
            case 2:
                positionFloorId = Integer.parseInt(o.toString());
                break;
            case 3:
                positionX = Double.parseDouble(o.toString());
                break;
            case 4:
                positionY = Double.parseDouble(o.toString());
                break;
        }
    }

    @Override
    public void getPropertyInfo(int i, Hashtable hashtable, PropertyInfo propertyInfo) {
        switch (i){
            case 0:
                propertyInfo.type = PropertyInfo.INTEGER_CLASS;
                propertyInfo.name = "LayoutObjectId";
                break;
            case 1:
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                propertyInfo.name = "Mac";
                break;
            case 2:
                propertyInfo.type = PropertyInfo.INTEGER_CLASS;
                propertyInfo.name = "PositionFloorId";
                break;
            case 3:
                propertyInfo.type = PropertyInfo.OBJECT_CLASS;
                propertyInfo.name = "PositionX";
                break;
            case 4:
                propertyInfo.type = PropertyInfo.OBJECT_CLASS;
                propertyInfo.name = "PositionY";
                break;
        }
    }
}
