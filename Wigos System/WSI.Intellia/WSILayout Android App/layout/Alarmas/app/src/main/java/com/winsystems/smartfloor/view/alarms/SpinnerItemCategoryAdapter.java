package com.winsystems.smartfloor.view.alarms;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.winsystems.smartfloor.R;
import com.winsystems.smartfloor.helpers.AlarmResourseHelper;
import com.winsystems.smartfloor.model.LayoutSiteItemCategory;
import com.winsystems.smartfloor.model.LayoutSiteItemSubCategory;

import java.util.ArrayList;
import java.util.List;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: SpinnerItemCategoryAdapter
//
// DESCRIPTION: Represent an item into the dropdownlist of Categories.
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public class SpinnerItemCategoryAdapter extends BaseAdapter {
  private List<LayoutSiteItemSubCategory> layoutSiteItemCategories;
  private LayoutInflater inflater = null;

  public SpinnerItemCategoryAdapter(Context context, List<LayoutSiteItemSubCategory> data, LayoutSiteItemCategory category) {
    this.layoutSiteItemCategories = new ArrayList<>();
    if(category != LayoutSiteItemCategory.CUSTOM)
      this.layoutSiteItemCategories.add(LayoutSiteItemSubCategory.Selection);
    this.layoutSiteItemCategories.addAll(data);
    inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
  }


  @Override
  public int getCount() {
    return layoutSiteItemCategories.size();
  }

  @Override
  public Object getItem(int position) {
    return layoutSiteItemCategories.get(position);
  }

  @Override
  public long getItemId(int position) {
    return position;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    LayoutSiteItemSubCategory itemCategory = layoutSiteItemCategories.get(position);

    FrameLayout imageView;
    GradientDrawable bgShape;

    convertView = inflater.inflate(R.layout.new_alarm_spinner_category_item,null);

    imageView = (FrameLayout) convertView.findViewById(R.id.list_item_icon);
    bgShape = (GradientDrawable)imageView.getBackground();
    bgShape.setColor(convertView.getResources().getColor(R.color.background_action_bar));
    if(itemCategory.getValue() == 2)
      imageView.setVisibility(View.INVISIBLE);
    ImageView alarmCategoryType = (ImageView) convertView.findViewById(R.id.alarm_category_image);
    alarmCategoryType.setImageResource(AlarmResourseHelper.getCategoryImageByType(itemCategory.getGroupCategory()));

    TextView text = (TextView) convertView.findViewById(R.id.list_item_description);
    text.setText(itemCategory.getDescription(convertView.getContext().getApplicationContext()));

    return convertView;
  }
}