package com.winsystems.smartfloor.view.utils;

import android.preference.Preference;

import com.winsystems.smartfloor.view.setting.SettingsFragment;

import java.util.Locale;

/**
 * Created by fede on 29/03/15.
 */
public class LanguageHelper {
  private static String[] availablesLanguages = {"es_AR", "en_US"};

  public static void setCurrentLocale(Locale locale, SettingsFragment settingsFragment) {
    boolean languageFounded = false;
    int i = 0;
    String currentLanguage = locale.getDefault().toString();
    while (!languageFounded && i < availablesLanguages.length) {
      if (availablesLanguages[i].equals(currentLanguage)) {
        Preference language = settingsFragment.findPreference("language");
        language.setDefaultValue(i+1);
        languageFounded = true;
      }
      i++;
    }
  }

  public static String getLanguageByValue(int index){
    String result = "";
    if(!(index > availablesLanguages.length) && !(index < 0)){
      result =availablesLanguages[--index];
    }
    return result;
  }
}
