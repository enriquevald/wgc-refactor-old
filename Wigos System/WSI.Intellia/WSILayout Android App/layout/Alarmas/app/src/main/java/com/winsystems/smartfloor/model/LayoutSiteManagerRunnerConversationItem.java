package com.winsystems.smartfloor.model;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.io.Serializable;
import java.util.Date;
import java.util.Hashtable;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: LayoutSiteManagerRunnerConversationItem
//
// DESCRIPTION: Implements method for site manager runner conversation.
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public class LayoutSiteManagerRunnerConversationItem implements KvmSerializable, Serializable {

    private Long managerId;
    private String managerUserName;

    private boolean pendingMessages;
    private String managerLastMessage;
    private Date managerLastMessageDate;

    public Long getManagerId() {
        return managerId;
    }

    public void setManagerId(Long managerId) {
        this.managerId = managerId;
    }

    public String getManagerUserName() {
        return managerUserName;
    }

    public void setManagerUserName(String managerUserName) {
        this.managerUserName = managerUserName;
    }


    public boolean isPendingMessages() {
        return pendingMessages;
    }

    public void setPendingMessages(boolean pendingMessages) {
        this.pendingMessages = pendingMessages;
    }
    public String getManagerLastMessage() {
        return managerLastMessage;
    }

    public void setManagerLastMessage(String managerLastMessageDate) {
        this.managerLastMessage = managerLastMessageDate;
    }
    public Date getManagerLastMessageDate() {
        return managerLastMessageDate;
    }

    public void setManagerLastMessageDate(Date managerLastMessageDate) {
        this.managerLastMessageDate = managerLastMessageDate;
    }


    @Override
    public Object getProperty(int i) {
        switch (i){
            case 0:
                return managerId;
            case 1:
                return managerUserName;
            case 2:
                return managerLastMessage;
            case 3:
                return managerLastMessageDate;
            default:
                return null;
        }
    }

    @Override
    public int getPropertyCount() {
        return 5;
    }

    @Override
    public void setProperty(int i, Object o) {

        switch (i){
            case 0:
                managerId = Long.parseLong(o.toString());
            case 1:
                managerUserName = o.toString();
            case 2:
                managerLastMessage = o.toString();
            case 3:
                managerLastMessageDate = (Date)o;
        }
    }

    @Override
    public void getPropertyInfo(int i, Hashtable hashtable, PropertyInfo propertyInfo) {
        switch (i){
            case 0:
                propertyInfo.type = PropertyInfo.LONG_CLASS;
                propertyInfo.name = "ManagerId";
                break;
            case 1:
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                propertyInfo.name = "ManagerUserName";
                break;
            case 2:
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                propertyInfo.name = "ManagerLastMessage";
                break;
            case 3:
                propertyInfo.type = propertyInfo.OBJECT_CLASS;
                propertyInfo.name = "ManagerLastMessageDate";
                break;
        }
    }
}

