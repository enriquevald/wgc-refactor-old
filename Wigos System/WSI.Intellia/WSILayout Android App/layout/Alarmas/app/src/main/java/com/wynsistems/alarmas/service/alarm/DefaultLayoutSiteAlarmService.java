package com.wynsistems.alarmas.service.alarm;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;

import com.wynsistems.alarmas.R;
import com.wynsistems.alarmas.common.AndroidLogger;
import com.wynsistems.alarmas.helpers.LogHelper;
import com.wynsistems.alarmas.helpers.SaveInformationHelper;
import com.wynsistems.alarmas.model.LayoutSiteAlarm;
import com.wynsistems.alarmas.service.ResponseCode;
import com.wynsistems.alarmas.service.alarm.request.DATA_TerminalInfo;
import com.wynsistems.alarmas.service.alarm.response.DATA_TerminalInfoResponse;
import com.wynsistems.alarmas.view.utils.Constants;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

public class DefaultLayoutSiteAlarmService implements LayoutSiteAlarmsService {

    private static DefaultLayoutSiteAlarmService instance = null;
    private SaveInformationHelper<LayoutSiteAlarm> saveLayoutSiteAlarms;

    private AndroidLogger mLog;
    private DefaultLayoutSiteAlarmService(Context context){
        mLog = LogHelper.getLogger(context);
        saveLayoutSiteAlarms = new SaveInformationHelper<>(context);
    }

    @Override
    public DATA_TerminalInfoResponse getTerminalStatusByTerminalID(String url, String sessionId, String terminalId) {
        final String NAMESPACE = "http://winsystemsintl.com/";
        final String METHOD_NAME = "GetTerminalInfoByTerminalId";
        final String SOAP_ACTION = "http://winsystemsintl.com/IDATAService/GetTerminalInfoByTerminalId";

        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);

        DATA_TerminalInfo terminalInfoRequest = new DATA_TerminalInfo();
        terminalInfoRequest.setSessionId(sessionId);
        terminalInfoRequest.setTerminalId(terminalId);

        DATA_TerminalInfoResponse terminalInfoResponse = new DATA_TerminalInfoResponse();

        PropertyInfo pi = new PropertyInfo();
        pi.setName("terminalInfo");
        pi.setValue(terminalInfoRequest);
        pi.setType(terminalInfoRequest.getClass());

        request.addProperty(pi);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;

        envelope.setOutputSoapObject(request);

        envelope.addMapping(NAMESPACE, "DATA_TerminalInfo", terminalInfoRequest.getClass());
        envelope.addMapping(NAMESPACE, "DATA_TerminalInfoResponse", terminalInfoResponse.getClass());

        HttpTransportSE transporte = new HttpTransportSE(url, 60000);

        try {
            transporte.call(SOAP_ACTION, envelope);

            SoapObject resSoap = (SoapObject) envelope.getResponse();

            terminalInfoResponse.setResponseCode(Integer.parseInt(resSoap.getProperty(0).toString()));
            terminalInfoResponse.setResponseCodeDescription(resSoap.getProperty(1).toString());
            terminalInfoResponse.setSessionId(resSoap.getProperty(2).toString());
            if(resSoap.getProperty(3) != null)
                terminalInfoResponse.setAlarms(ServerEntitiesBuilder.BuildAlarmsListFromServerResponse((SoapObject) resSoap.getProperty(3)));

        } catch (Exception e) {
            terminalInfoResponse.setResponseCode(9);
            terminalInfoResponse.setResponseCodeDescription(Resources.getSystem().getString(R.string.ErrorCallWebService));
            mLog.error("DefaultLayoutSiteAlarmService - getTerminalStatusByTerminalID - ", e);
            Log.e("DefaultLayout", e.getMessage());
        }

        if (terminalInfoResponse.getResponseCode() == ResponseCode.DATA_RC_OK)
        {
            saveLayoutSiteAlarms.writeList(terminalInfoResponse.getAlarms(), sessionId + "_layoutSiteAlarms");
            //saveLogs.writeList(terminalInfoResponse.getLog(), sessionId + "_terminalLogs");
            return terminalInfoResponse;
        }else
            return terminalInfoResponse;

    }

    @Override
    public DATA_TerminalInfoResponse getAlarmsCreatedByUserID(String url, String sessionId) {
        final String NAMESPACE = "http://winsystemsintl.com/";
        final String METHOD_NAME = "GetAlarmsCreatedByUser";
        final String SOAP_ACTION = "http://winsystemsintl.com/IDATAService/GetAlarmsCreatedByUser";

        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);

        DATA_TerminalInfo terminalInfoRequest = new DATA_TerminalInfo();
        terminalInfoRequest.setSessionId(sessionId);

        DATA_TerminalInfoResponse terminalInfoResponse = new DATA_TerminalInfoResponse();

        PropertyInfo pi = new PropertyInfo();
        pi.setName("terminalInfo");
        pi.setValue(terminalInfoRequest);
        pi.setType(terminalInfoRequest.getClass());

        request.addProperty(pi);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;

        envelope.setOutputSoapObject(request);

        envelope.addMapping(NAMESPACE, "DATA_TerminalInfo", terminalInfoRequest.getClass());
        envelope.addMapping(NAMESPACE, "DATA_TerminalInfoResponse", terminalInfoResponse.getClass());

        HttpTransportSE transporte = new HttpTransportSE(url, 60000);

        try {
            transporte.call(SOAP_ACTION, envelope);

            SoapObject resSoap = (SoapObject) envelope.getResponse();

            terminalInfoResponse.setResponseCode(Integer.parseInt(resSoap.getProperty(0).toString()));
            terminalInfoResponse.setResponseCodeDescription(resSoap.getProperty(1).toString());
            terminalInfoResponse.setSessionId(resSoap.getProperty(2).toString());

            SoapObject tasks = (SoapObject) resSoap.getProperty(3);
            terminalInfoResponse.setAlarms(ServerEntitiesBuilder.BuildAlarmsListFromServerResponse(tasks));
        } catch (Exception e) {
            terminalInfoResponse.setResponseCode(9);
            terminalInfoResponse.setResponseCodeDescription(Resources.getSystem().getString(R.string.ErrorCallWebService));
            mLog.error("DefaultLayoutSiteAlarmService - getAlarmsCreatedByUserID - ", e);
            Log.e("DefaultLayout", e.getMessage());
        }

        if (terminalInfoResponse.getResponseCode() == ResponseCode.DATA_RC_OK)
        {
            saveLayoutSiteAlarms.writeList(terminalInfoResponse.getAlarms(), sessionId + Constants.ALARMS_CREATED_BY_USER);
            return terminalInfoResponse;
        }else
            return null;
    }

    public static LayoutSiteAlarmsService getInstance(Context context) {
        if(instance == null){
            instance = new DefaultLayoutSiteAlarmService(context);
        }
        return instance;
    }
}
