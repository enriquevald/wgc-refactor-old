package com.wynsistems.alarmas.view.menu;

public class MenuItem {
    private int urlDrawable;
    private String description;
    private Class<?> targetClass;

    public MenuItem(int urlDrawable, String description, Class<?> targetClass){
        this.urlDrawable = urlDrawable;
        this.description = description;
        this.targetClass = targetClass;
    }

    public int getUrlDrawable() {
        return this.urlDrawable;
    }

    public String getDescription() {
        return this.description;
    }

    public Class<?> getTargetClass(){
        return this.targetClass;
    }
}
