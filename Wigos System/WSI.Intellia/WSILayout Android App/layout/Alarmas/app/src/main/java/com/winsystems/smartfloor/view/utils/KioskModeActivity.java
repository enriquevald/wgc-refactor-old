package com.winsystems.smartfloor.view.utils;


import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.winsystems.smartfloor.R;
import com.winsystems.smartfloor.common.LayoutNotificationManager;
import com.winsystems.smartfloor.common.LayoutNotificationType;
import com.winsystems.smartfloor.helpers.*;
import com.winsystems.smartfloor.helpers.LanguageHelper;
import com.winsystems.smartfloor.model.AlarmGroupingCriteria;
import com.winsystems.smartfloor.model.AlarmSortingCriteria;
import com.winsystems.smartfloor.model.LayoutSiteItemSubCategory;
import com.winsystems.smartfloor.service.mqtt.PahoMqttService;
import com.winsystems.smartfloor.view.message.MessageActivity;
import com.winsystems.smartfloor.view.notifications.ListNotificationsActivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class KioskModeActivity extends ActionBarActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
    private SharedPreferences prefs;
    protected AlarmGroupingCriteria groupPreference;
    protected AlarmSortingCriteria sortPreference;
    private String language;
    private static TextView notifCountMessage;
    private static TextView notifCountTasks;
    private static int mNotifCountMessage = 0;
    private static int mNotifCountTask = 0;
    private Menu iconsMenu;
    private IntentFilter intentFilter = new IntentFilter();
    private IntentFilter intentFilterConnection = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
    private IntentFilter intentFilterKeepAliveFail = new IntentFilter(Constants.KEEP_ALIVE_FAIL);
    private IntentFilter intentFilterKeepAliveSuccess = new IntentFilter(Constants.KEEP_ALIVE_SUCCESS);
    private IntentFilter intentFilterLogRequest = new IntentFilter(PahoMqttService.LOG_REQUESTED);

    private LayoutNotificationManager notificationManager;
    private boolean isConnected = true;
    private ConnectivityManager mConnectivityManager; // To check for connectivity changes

    private BroadcastReceiver updateIconMenuBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    refreshIconsMenu(iconsMenu);
                }
            });
        }
    };

    private BroadcastReceiver checkConnectivityReceiver  = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    checkConnectivity();
                }
            });
        }
    };


    private BroadcastReceiver KeepAliveSuccessReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    checkKeepAlive(0);
                }
            });
        }
    };


    private BroadcastReceiver KeepAliveFailReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    checkKeepAlive(1);
                }
            });
        }
    };


    private BroadcastReceiver SendLogReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    NotifyLogRequested();

                }
            });
        }
    };

    private void NotifyLogRequested() {

        String  strMessage = getString(R.string.LOG_REQUESTED_MSG);
        final Snackbar snackbar;
        snackbar = Snackbar.make(this.findViewById(android.R.id.content), strMessage, Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction(R.string.ACTION_DISMISS, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackbar.dismiss();
            }
        });
        snackbar.show();
    }


    private void checkKeepAlive(int keepAliveType) {

        if(keepAliveType == 0 ) {
            return;
        }

        String  strMessage = keepAliveType == 0 ? getString(R.string.snackBarKeepAliveReconnect) : getString(R.string.snackBarKeepAliveFail);

        final Snackbar snackbar;
        snackbar = Snackbar.make(this.findViewById(android.R.id.content), strMessage, Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction(R.string.ACTION_DISMISS, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackbar.dismiss();
            }
        });
        snackbar.show();

        onKeepAliveChange(keepAliveType);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFullScreen();
        prefs = PreferenceManager.getDefaultSharedPreferences(this);//get the preferences that are allowed to be given
        prefs.registerOnSharedPreferenceChangeListener(this);//set the listener to listen for changes in the preferences
        notificationManager = new LayoutNotificationManager(getApplicationContext());
        mConnectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);

        updatePreferencesAndLanguage();

        String[] localeInfo = LanguageHelper.getLanguageByValue(Integer.valueOf(prefs.getString("language", "1").toString())).split("_");
        String language = localeInfo[0];
        String country = new String(localeInfo[1]).toUpperCase();
        Locale newLocale = new Locale(language, country);
        Locale.setDefault(newLocale);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = newLocale;
        res.updateConfiguration(conf, dm);

        intentFilter.addAction(Constants.NOTIFY_NEW_ICON_MENU_NOTIFICATION);
        intentFilter.addAction(Constants.NEW_TASK_ARRIVE);
        intentFilter.addAction(PahoMqttService.NEW_MESSAGE_FROM_MANAGER_ARRIVE);
        registerReceiver(checkConnectivityReceiver, intentFilterConnection);
        registerReceiver(KeepAliveSuccessReceiver,intentFilterKeepAliveSuccess);
        registerReceiver(KeepAliveFailReceiver,intentFilterKeepAliveFail);

    }



    @Override
    public void onResume(){
        super.onResume();
        String defaultLanguage = Locale.getDefault().getDisplayLanguage();
        //updatePreferencesAndLanguage();
        if (!defaultLanguage.equals(language)){
            finish();
            startActivity(getIntent());
        }
        registerReceiver(updateIconMenuBroadcastReceiver, intentFilter);
        refreshIconsMenu(iconsMenu);
        registerReceiver(checkConnectivityReceiver, intentFilterConnection);
        checkConnectivity();
        registerReceiver(KeepAliveSuccessReceiver,intentFilterKeepAliveSuccess);
        registerReceiver(KeepAliveFailReceiver, intentFilterKeepAliveFail);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if( KioskModeHelper.isKioskModeActive(getApplicationContext())) {
            ActivityManager activityManager = (ActivityManager) getApplicationContext()
                    .getSystemService(Context.ACTIVITY_SERVICE);

            activityManager.moveTaskToFront(getTaskId(), 0);
        }

        unregisterReceiver(updateIconMenuBroadcastReceiver);
        unregisterReceiver(checkConnectivityReceiver);
        unregisterReceiver(KeepAliveSuccessReceiver);
        unregisterReceiver(KeepAliveFailReceiver);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        iconsMenu = menu;
        refreshIconsMenu(iconsMenu);

        return super.onCreateOptionsMenu(iconsMenu);
    }

    private void refreshIconsMenu(Menu menu) {
        if(iconsMenu != null) {
            iconsMenu.clear();
            MenuInflater inflater = this.getMenuInflater();
            inflater.inflate(R.menu.menu_menu, iconsMenu);

            MenuItem message_icon = menu.findItem(R.id.message_bar_icon);
            MenuItemCompat.setActionView(message_icon, R.layout.message_update_count);
            View count = message_icon.getActionView();
            notifCountMessage = (TextView) count.findViewById(R.id.notif_count);
            setNotifCountMessage();
            if (mNotifCountMessage > 0) {
                notifCountMessage.setVisibility(View.VISIBLE);
                count.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getApplicationContext(), MessageActivity.class);
                        startActivity(intent);
                    }
                });
                message_icon.setVisible(true);
            } else {
                notifCountMessage.setVisibility(View.GONE);
                message_icon.setVisible(false);
            }

            MenuItem tasks_icon = menu.findItem(R.id.tasks_bar_icon);
            MenuItemCompat.setActionView(tasks_icon, R.layout.tasks_update_count);
            View countTasks = tasks_icon.getActionView();
            notifCountTasks = (TextView) countTasks.findViewById(R.id.notif_count_tasks);
            setNotifCountTask();
            if(mNotifCountTask > 0) {
                notifCountTasks.setVisibility(View.VISIBLE);
                countTasks.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getApplicationContext(), ListNotificationsActivity.class);
                        intent.putExtra(Constants.NOTIFICATION_TYPE, LayoutNotificationType.TASK);
                        startActivity(intent);
                    }
                });
                tasks_icon.setVisible(true);
            }else{
                notifCountTasks.setVisibility(View.GONE);
                tasks_icon.setVisible(false);
            }
        }
    }

    private void setNotifCountMessage(){
        mNotifCountMessage = readUnReadNotifications(LayoutNotificationType.MESSAGE);
        notifCountMessage.setText(String.valueOf(mNotifCountMessage));
        invalidateOptionsMenu();
    }

    private void setNotifCountTask(){
        mNotifCountTask = readUnReadNotifications(LayoutNotificationType.TASK);
        notifCountTasks.setText(String.valueOf(mNotifCountTask));
        invalidateOptionsMenu();
    }

    private int readUnReadNotifications(LayoutNotificationType type){
        return notificationManager.getCountNotificationsByType(type);
    }

    private void updatePreferencesAndLanguage() {
        groupPreference = AlarmGroupingCriteria.getFromString(prefs.getString("grouping", "1"));
        sortPreference = AlarmSortingCriteria.getFromString(prefs.getString("sorting", "1"));
        language = prefs.getString("languageToLoad", Locale.getDefault().getDisplayLanguage());
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if(!hasFocus && KioskModeHelper.isKioskModeActive(getApplicationContext())) {
            // Close every kind of system dialog
            Intent closeDialog = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
            sendBroadcast(closeDialog);
        }
    }

    @Override
    public void onBackPressed() {
        // nothing to do here
        if(!isTaskRoot())
            super.onBackPressed();
    }

    /**
     * Detects and toggles immersive mode (also known as "hidey bar" mode).
     */
    public void setFullScreen() {
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        View decorView = getWindow().getDecorView();
//        int uiOptions =  View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION;
//                        //| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
//        decorView.setSystemUiVisibility(uiOptions);
    }



    private final List blockedKeys = new ArrayList(Arrays.asList(KeyEvent.KEYCODE_VOLUME_DOWN, KeyEvent.KEYCODE_VOLUME_UP));

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (blockedKeys.contains(event.getKeyCode()) && KioskModeHelper.isKioskModeActive(getApplicationContext())) {
            return true;
        } else {
            return super.dispatchKeyEvent(event);
        }
    }


    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if(key.equals("alarm_server_address")) {
            PahoMqttService.actionStop(this);
            PahoMqttService.actionStart(this);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if((keyCode==KeyEvent.KEYCODE_BACK || keyCode==KeyEvent.KEYCODE_APP_SWITCH) && KioskModeHelper.isKioskModeActive(getApplicationContext()))
        {
            if(KioskModeHelper.isKioskModeActive(getApplicationContext()))
                return true;
            else
                return super.onKeyDown(keyCode, event);
        }
        else
            return super.onKeyDown(keyCode, event);
    }

    private void checkConnectivity()
    {

        NetworkInfo info = mConnectivityManager.getActiveNetworkInfo();
        setIsConnected(false);
        if(info != null){
            if(info.isConnected() && info.getType() == ConnectivityManager.TYPE_WIFI) {
                setIsConnected(true);
            }
        }


        if(!getIsConnected()) {

            final Snackbar snackbar;
            snackbar = Snackbar.make(this.findViewById(android.R.id.content), getString(R.string.snackBarMessageNoConnection), Snackbar.LENGTH_INDEFINITE);
            snackbar.setAction(R.string.ACTION_DISMISS, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    snackbar.dismiss();
                }
            });
//        snackbar.setAction(R.string.snackBarMessage, new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
//            }
//        });
            snackbar.show();
        }

        onConnectivityChange();
    }


    public boolean getIsConnected() {
        return isConnected;
    }

    public void setIsConnected(boolean isConnected) {
        this.isConnected = isConnected;
    }

    public void onConnectivityChange(){

    }

    public void onKeepAliveChange (int keepAliveType){

    }
}
