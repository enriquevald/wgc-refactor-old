package com.winsystems.smartfloor.service.message.request;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.util.Hashtable;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: DATA_MessageToRequest
//
// DESCRIPTION: Implement methods for get and set session data.
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public class DATA_MessageToRequest implements KvmSerializable {

  private long managerId;
  private String message;
  private String session;

  public void setSession(String session) {
    this.session = session;
  }

  public void setManagerId(long managerId) {
    this.managerId = managerId;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  @Override
  public Object getProperty(int i) {
    switch (i){
      case 0:
        return managerId;
      case 1:
        return message;
      case 2:
        return session;
      default:
        return null;
    }
  }

  @Override
  public int getPropertyCount() {
    return 3;
  }

  @Override
  public void setProperty(int i, Object o) {

    switch (i){
      case 0:
        managerId =  Long.parseLong(o.toString());
        break;
      case 1:
        message =  o.toString();
        break;
      case 2:
        session =  o.toString();
        break;
    }

  }

  @Override
  public void getPropertyInfo(int i, Hashtable hashtable, PropertyInfo propertyInfo) {
    switch (i){
      case 0:
        propertyInfo.type = PropertyInfo.LONG_CLASS;
        propertyInfo.name = "ManagerId";
        break;
      case 1:
        propertyInfo.type = PropertyInfo.STRING_CLASS;
        propertyInfo.name = "Message";
        break;
      case 2:
        propertyInfo.type = PropertyInfo.STRING_CLASS;
        propertyInfo.name = "SessionId";
        break;
    }
  }
}
