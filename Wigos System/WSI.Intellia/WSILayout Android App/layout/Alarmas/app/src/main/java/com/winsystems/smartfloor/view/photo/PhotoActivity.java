package com.winsystems.smartfloor.view.photo;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.winsystems.smartfloor.R;
import com.winsystems.smartfloor.common.AndroidLogger;
import com.winsystems.smartfloor.helpers.CameraSetupHelper;
import com.winsystems.smartfloor.helpers.LogHelper;
import com.winsystems.smartfloor.service.DATA_PostActionResponse;
import com.winsystems.smartfloor.service.ResponseCode;
import com.winsystems.smartfloor.service.ServiceFactory;
import com.winsystems.smartfloor.service.photo.PhotoService;
import com.winsystems.smartfloor.view.camera.CaptureActivityAnyOrientation;
import com.winsystems.smartfloor.view.menu.MenuActivity;
import com.winsystems.smartfloor.view.utils.Constants;
import com.winsystems.smartfloor.view.utils.KioskModeActivity;
import com.winsystems.smartfloor.view.utils.ToastFactory;

import java.io.File;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: PhotoActivity
//
// DESCRIPTION: Activity that implements the functionality for photo and images of app.
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public class PhotoActivity extends KioskModeActivity implements SharedPreferences.OnSharedPreferenceChangeListener{
  private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
  private ImageView photoContainerView;
  private Button sendPhotoButton;
  private String userId;
  private ProgressDialog dialog;
  private HttpRequestTask downloadAsynchTask;
  private String urlServerUploadImage;
  private SharedPreferences userSettings;
  private SharedPreferences environmentSettings;
  RadioGroup radioGroup;
  private String photoToSend;
  private static AndroidLogger mLog;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    mLog = LogHelper.getLogger(this);
    setContentView(R.layout.activity_photo);
    setTitle(R.string.title_activity_photo);
    this.downloadAsynchTask = new HttpRequestTask();
    buildDialog();

    userSettings = this.getSharedPreferences(Constants.USER_ID_PREFERENCE_EDITOR, 0) ;
    environmentSettings = PreferenceManager.getDefaultSharedPreferences(this);

    ImageButton terminalQRCodeButton = (ImageButton) this.findViewById(R.id.terminal_qr_code_button);
    this.radioGroup = (RadioGroup)PhotoActivity.this.findViewById(R.id.radioGroupPhotoType);
    this.radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(RadioGroup group, int checkedId) {
        DisplatTerminalPromotionElements();
      }
    });

    DisplatTerminalPromotionElements();

    this.photoContainerView = (ImageView)this.findViewById(R.id.photo_container);
    this.sendPhotoButton = (Button) this.findViewById(R.id.send_photo_button);
    this.sendPhotoButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if(isValidAlarm()) {
          PhotoActivity.this.downloadAsynchTask.execute();
        }
        else{
          ToastFactory.buildSimpleToast(getResources().getString(R.string.error_field_required_new_alarm), PhotoActivity.this);
        }
      }
    });
    terminalQRCodeButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        IntentIntegrator scanIntegrator = new IntentIntegrator(PhotoActivity.this);
        scanIntegrator.setCaptureActivity(CaptureActivityAnyOrientation.class);
        scanIntegrator.setOrientationLocked(false);
        scanIntegrator.initiateScan();
      }
    });

    dispatchCameraIntent();
  }

  private void DisplatTerminalPromotionElements() {
    LinearLayout qr_scan_group = (LinearLayout) PhotoActivity.this.findViewById(R.id.qr_scan_group);
    EditText photo_comment = (EditText) PhotoActivity.this.findViewById(R.id.photo_comment);
    int id=radioGroup.getCheckedRadioButtonId();
    View radioButton = radioGroup.findViewById(id);
    Boolean isTerminalOptionChecked = radioButton.getId()==R.id.rdbTerminal;
    qr_scan_group.setVisibility(isTerminalOptionChecked ? View.VISIBLE : View.GONE);
    photo_comment.setVisibility(!isTerminalOptionChecked ? View.VISIBLE : View.GONE);
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
    super.onActivityResult(requestCode, resultCode, intent);
    if(requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
      if (resultCode == RESULT_OK) {
        this.photoContainerView.setImageBitmap(CameraSetupHelper.LoadBitmatOfPhotoTaken(photoToSend));
        this.photoContainerView.setVisibility(View.VISIBLE);
        this.sendPhotoButton.setVisibility(View.VISIBLE);
      }
      else
        finish();
    }else{
      IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
      if (scanningResult != null) {
        TextView textView = (TextView)this.findViewById(R.id.new_alarm_terminal_provider_field);
        textView.setText(scanningResult.getContents());
      } else {
        Toast toast = Toast.makeText(getApplicationContext(), "No scan data received!", Toast.LENGTH_SHORT);
        toast.show();
      }
    }
  }


  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();

    //noinspection SimplifiableIfStatement
    if (id == R.id.action_settings) {
      return true;
    }

    return super.onOptionsItemSelected(item);
  }

  private void dispatchCameraIntent() {
    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
    try {
      File photo = CameraSetupHelper.setupFilePathForCameraActivity();
      photoToSend = photo.getAbsolutePath();
      intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
    } catch (Exception e) {
      e.printStackTrace();
    }
    // start the image capture Intent
    startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
  }



  private void buildDialog() {
    this.dialog = new ProgressDialog(PhotoActivity.this);
    this.dialog.setTitle(PhotoActivity.this.getResources().getString(R.string.title_progress_dialog));
    this.dialog.setMessage(PhotoActivity.this.getResources().getString(R.string.photo_message_progress_dialog));
    this.dialog.setCanceledOnTouchOutside(false);
    this.dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
      @Override
      public void onCancel(DialogInterface dialogInterface) {
        PhotoActivity.this.downloadAsynchTask.cancel(true);
        finish();
      }
    });
  }

  @Override
  protected void onStop() {
    this.dialog.dismiss();
    super.onStop();
  }

  @Override
  protected void onDestroy() {
    if(!this.downloadAsynchTask.isCancelled()) {
      this.downloadAsynchTask.cancel(true);
      this.dialog.dismiss();
    }
    super.onDestroy();
  }

  @Override
  public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
    urlServerUploadImage = sharedPreferences.getString("alarm_server_address", urlServerUploadImage);
  }

  public boolean isValidAlarm() {
    String externalId = ((EditText) PhotoActivity.this.findViewById(R.id.new_alarm_terminal_provider_field)).getText().toString();
    RadioGroup radioGroup = (RadioGroup) PhotoActivity.this.findViewById(R.id.radioGroupPhotoType);
    Integer type = radioGroup.getCheckedRadioButtonId() == R.id.rdbTerminal ? 1 : 2;
    String comment = ((EditText) PhotoActivity.this.findViewById(R.id.photo_comment)).getText().toString();

    return !(type == 1 && externalId.isEmpty()) && !(type == 2 && comment.isEmpty());

  }


  private class HttpRequestTask extends AsyncTask<Void, Void, Boolean> {
    private String _comment;
    private String _externalId;
    private Integer _type;
    @Override
    protected void onPreExecute() {
      super.onPreExecute();
      PhotoActivity.this.dialog.show();
      _comment = ((EditText)PhotoActivity.this.findViewById(R.id.photo_comment)).getText().toString();
      _externalId = ((EditText)PhotoActivity.this.findViewById(R.id.new_alarm_terminal_provider_field)).getText().toString();
      RadioGroup radioGroup = (RadioGroup)PhotoActivity.this.findViewById(R.id.radioGroupPhotoType);
      _type = radioGroup.getCheckedRadioButtonId() == R.id.rdbTerminal? 1: 2;
    }

    @Override
    public Boolean doInBackground(Void... params) {
      try {
        urlServerUploadImage = environmentSettings.getString("alarm_server_address", urlServerUploadImage);
        userId = userSettings.getString(Constants.USER_ID,"");

        String path = PhotoActivity.this.photoToSend;
        PhotoService photoService = ServiceFactory.getInstance().getPhotoService();
        String comment = _comment;
        String externalId = _externalId;
        Integer type = _type;
        DATA_PostActionResponse response = photoService.UploadPhoto(getApplicationContext(), urlServerUploadImage, PhotoActivity.this.userId, path, comment, externalId, type);

        if(response.getResponseCode() == ResponseCode.DATA_RC_OK)
          return true;
        else
        {
          ToastFactory.buildSimpleToast(response.getResponseCodeDescription(), PhotoActivity.this);
          return false;
        }
      } catch (Exception e) {
        mLog.error("PhotoActivity", e);
        return false;
      }

    }

    @Override
    protected void onPostExecute(Boolean result) {
      if (dialog.isShowing()) {
        dialog.dismiss();
      }
      if(result){

        if(CameraSetupHelper.deletePhotoFile(photoToSend))
          mLog.info("Photo deletion // Delete Success");
        else
          mLog.info("Photo deletion // Delete failed");

        Intent intent = new Intent(PhotoActivity.this, MenuActivity.class);
        PhotoActivity.this.startActivity(intent);

      }
      else{
        mLog.info(getResources().getString(R.string.upload_photo_error_message));
        ToastFactory.buildSimpleToast(getResources().getString(R.string.upload_photo_error_message), PhotoActivity.this);
      }

    }
  }
}
