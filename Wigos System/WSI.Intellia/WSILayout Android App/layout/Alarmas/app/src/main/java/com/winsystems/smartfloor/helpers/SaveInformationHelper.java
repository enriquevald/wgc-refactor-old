package com.winsystems.smartfloor.helpers;


import android.content.Context;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.util.ArrayList;
import java.util.List;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: SaveInformationHelper
//
// DESCRIPTION: Implements Save Information Helper
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public class SaveInformationHelper<T> {

  private final Context _context;

  public SaveInformationHelper(Context context) {
    _context = context;
  }

  public List<T> readList(String fileName) {
    try {

      FileInputStream fis = _context.openFileInput(fileName);

      ObjectInputStream ois = new ObjectInputStream(fis);
      ArrayList<T> beacons = (ArrayList<T>) ois.readObject();
      ois.close();

      return beacons;
    } catch (Exception e) {

      Log.e("SaveInformationHelper", "File Is Empty");
      return new ArrayList<T>();
    }
  }

  public void writeList(List<T> objectList, String fileName) {
    if(objectList.size() > 0) {
      File myfile = _context.getFileStreamPath(fileName);
      try {
        if (myfile.exists() || myfile.createNewFile()) {
          FileOutputStream fos = _context.openFileOutput(fileName, _context.MODE_PRIVATE);
          FileChannel channel = fos.getChannel();
          FileLock lock = channel.lock();
          ObjectOutputStream oos = new ObjectOutputStream(fos);
          oos.writeObject(objectList);
          lock.release();
        }
      } catch (Exception e) {
        Log.e("SaveInformationHelper", e.getMessage());
      }
    }
  }

  public void clearList(String fileName) {
    File myfile = _context.getFileStreamPath(fileName);
    try {
      if (myfile.exists() || myfile.createNewFile()) {
        FileOutputStream fos = _context.openFileOutput(fileName, _context.MODE_PRIVATE);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(new ArrayList<>());
      }
    } catch (Exception e) {
      Log.e("SaveInformationHelper", e.getMessage());
    }

  }
}
