package com.winsystems.smartfloor.view.terminal;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.winsystems.smartfloor.R;
import com.winsystems.smartfloor.model.LayoutSiteAlarm;
import com.winsystems.smartfloor.model.LayoutSiteItemSubCategory;
import com.winsystems.smartfloor.model.LayoutSiteTaskActions;
import com.winsystems.smartfloor.helpers.AlarmResourseHelper;

import java.util.List;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: LayoutSiteAlarmAdapter
//
// DESCRIPTION: Implements the functionality for layout site alarm status.
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public class LayoutSiteAlarmAdapter extends BaseAdapter {

    private List<LayoutSiteAlarm> alarms;
    private static LayoutInflater inflater = null;
    private Context _context;
    public LayoutSiteAlarmAdapter(Context context, List<LayoutSiteAlarm> data) {
        _context = context;
        this.alarms = data;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return this.alarms.size();
    }

    @Override
    public Object getItem(int position) {
        return this.alarms.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi;
        vi = inflater.inflate(R.layout.terminal_status_alarm_item, null);

        LayoutSiteAlarm alarm = (LayoutSiteAlarm)getItem(position);

        TextView alarm_to_task = (TextView)vi.findViewById(R.id.alarm_to_task);
        TextView catergoryDescription = (TextView)vi.findViewById(R.id.category_description);
        Integer taskStatus = alarm.getStatusOfTask();

        if(taskStatus != 0) {

            if (LayoutSiteTaskActions.GetActionLayoutSiteTaskAction(taskStatus) == LayoutSiteTaskActions.Assigned) {
                alarm_to_task.setText(R.string.Pending);
                alarm_to_task.setTextColor(vi.getResources().getColor(R.color.winsystem_red));
            } else {
                alarm_to_task.setText(R.string.InProgress);
                alarm_to_task.setTextColor(vi.getResources().getColor(R.color.background_action_bar));
            }
        }else
        {
            alarm_to_task.setText(R.string.NotAssigned);
        }
        LayoutSiteItemSubCategory subCategory = alarm.getSubCategory();
        String title;
        if(subCategory != null)
            if(!LayoutSiteItemSubCategory.isCustomCategory(subCategory.getValue()))
                title = subCategory.getDescription(_context);
            else
                title = alarm.getTitle();
        else
            title = "";

        catergoryDescription.setText("ID: "+ alarm.getId() + " - " + title);
        FrameLayout imageView = (FrameLayout) vi.findViewById(R.id.alarm_criticality_type);
        GradientDrawable bgShape = (GradientDrawable)imageView.getBackground();
        bgShape.setColor(vi.getResources().getColor(R.color.background_action_bar));

        ImageView alarmCategoryType = (ImageView) vi.findViewById(R.id.alarm_category_image);
        if(subCategory != null)
            alarmCategoryType.setImageResource(AlarmResourseHelper.getCategoryImageByType(subCategory.getGroupCategory()));

        return vi;
    }
}
