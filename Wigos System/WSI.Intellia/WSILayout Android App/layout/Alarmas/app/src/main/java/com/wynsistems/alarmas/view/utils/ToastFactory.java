package com.wynsistems.alarmas.view.utils;

import android.content.Context;
import android.view.Gravity;
import android.widget.Toast;

public class ToastFactory {
    public static void buildSimpleToast(String message, Context context){
        int duration = Toast.LENGTH_LONG;
        Toast toast = Toast.makeText(context, message, duration);
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.show();
    }
}
