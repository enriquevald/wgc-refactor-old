package com.winsystems.smartfloor.view.message;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.winsystems.smartfloor.R;
import com.winsystems.smartfloor.model.LayoutSiteManagerAvailable;

import java.util.ArrayList;
import java.util.List;

public class ManagersAvailablesListAdapter extends BaseAdapter {

  private Context _context;
  LayoutInflater inflater;
  private List<LayoutSiteManagerAvailable> _layoutSiteManagerRunnerConversationItems;

  public ManagersAvailablesListAdapter(Context context, List<LayoutSiteManagerAvailable> layoutSiteManagerRunnerConversationItems) {
    _context = context;
    if(layoutSiteManagerRunnerConversationItems != null)
      this._layoutSiteManagerRunnerConversationItems = layoutSiteManagerRunnerConversationItems;
    else
      this._layoutSiteManagerRunnerConversationItems = new ArrayList<>();

    inflater = (LayoutInflater) context
        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
  }

  @Override
  public int getCount() {
    return this._layoutSiteManagerRunnerConversationItems.size();
  }

  @Override
  public Object getItem(int position) {
    return this._layoutSiteManagerRunnerConversationItems.get(position);
  }

  @Override
  public long getItemId(int position) {
    return position;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    convertView = inflater.inflate(R.layout.manager_available_list_item, null);

    LayoutSiteManagerAvailable item = (LayoutSiteManagerAvailable)getItem(position);
    TextView managerUserName = (TextView)convertView.findViewById(R.id.manager_name);
    managerUserName.setText(item.getUserName());
    return convertView;
  }
}
