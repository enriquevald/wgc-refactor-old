package com.wynsistems.alarmas.service.alarm.response;

import com.wynsistems.alarmas.service.login.response.LayoutSiteTask;
import com.wynsistems.alarmas.service.ResponseCode;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.util.Hashtable;
import java.util.List;

public class DATA_LayoutSiteTaskAssignedResponse implements KvmSerializable{

    private String sessionId;
    private Integer responseCode;
    private String responseCodeDescription;
    private ResponseCode enumResponseCode;
    private List<LayoutSiteTask> availableTasks;

    public DATA_LayoutSiteTaskAssignedResponse() {
    }

    public void setSessionId(String sessionId) {

        this.sessionId = sessionId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setResponseCode(Integer responseCode) {
        this.responseCode = responseCode;
    }

    public void setResponseCodeDescription(String responseCodeDescription) {
        this.responseCodeDescription = responseCodeDescription;
    }

    public void setAvailableTasks(List<LayoutSiteTask> availableTasks) {
        this.availableTasks = availableTasks;
    }

    public ResponseCode getResponseCode(){
        return ResponseCode.getResponseCode(responseCode);
    }

    public String getResponseCodeDescription() {
        return responseCodeDescription;
    }

    public List<LayoutSiteTask> getAvailableTasks() {
        return availableTasks;
    }

    @Override
    public Object getProperty(int i) {
        switch (i){
            case 0:
                return responseCode;
            case 1:
                return responseCodeDescription;
            case 2:
                return sessionId;
            case 3:
                return availableTasks;
            default:
                return null;
        }
    }

    @Override
    public int getPropertyCount() {
        return 4;
    }

    @Override
    public void setProperty(int i, Object o) {
        switch(i)
        {
            case 0:
                responseCode = Integer.parseInt(o.toString());
                break;
            case 1:
                responseCodeDescription = o.toString();
                break;
            case 2:
                sessionId = o.toString();
                break;
            case 3:
                availableTasks = (List<LayoutSiteTask>)o;
                break;
            default:
                break;
        }
    }

    @Override
    public void getPropertyInfo(int i, Hashtable hashtable, PropertyInfo propertyInfo) {
        switch(i)
        {
            case 0:
                propertyInfo.type = PropertyInfo.INTEGER_CLASS;
                propertyInfo.name = "ResponseCode";
                break;
            case 1:
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                propertyInfo.name = "ResponseCodeDescription";
                break;
            case 2:
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                propertyInfo.name = "SessionId";
                break;
            case 3:
                propertyInfo.type = PropertyInfo.VECTOR_CLASS;
                propertyInfo.name = "AssignedTasks";
                break;
            default:break;
        }
    }
}
