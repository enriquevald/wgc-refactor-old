package com.wynsistems.alarmas.model;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.io.Serializable;
import java.util.Hashtable;

public class LayoutSiteManagerRunnerConversationItem implements KvmSerializable, Serializable {

    private Long managerId;
    private String managerUserName;
    private boolean pendingMessages;


    public Long getManagerId() {
        return managerId;
    }

    public void setManagerId(Long managerId) {
        this.managerId = managerId;
    }

    public String getManagerUserName() {
        return managerUserName;
    }

    public void setManagerUserName(String managerUserName) {
        this.managerUserName = managerUserName;
    }

    public boolean isPendingMessages() {
        return pendingMessages;
    }

    public void setPendingMessages(boolean pendingMessages) {
        this.pendingMessages = pendingMessages;
    }



    @Override
    public Object getProperty(int i) {
        switch (i){
            case 0:
                return managerId;
            case 1:
                return managerUserName;
            default:
                return null;
        }
    }

    @Override
    public int getPropertyCount() {
        return 3;
    }

    @Override
    public void setProperty(int i, Object o) {

        switch (i){
            case 0:
                managerId = Long.parseLong(o.toString());
            case 1:
                managerUserName = o.toString();
        }
    }

    @Override
    public void getPropertyInfo(int i, Hashtable hashtable, PropertyInfo propertyInfo) {
        switch (i){
            case 0:
                propertyInfo.type = PropertyInfo.LONG_CLASS;
                propertyInfo.name = "ManagerId";
                break;
            case 1:
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                propertyInfo.name = "ManagerUserName";
                break;
        }
    }
}

