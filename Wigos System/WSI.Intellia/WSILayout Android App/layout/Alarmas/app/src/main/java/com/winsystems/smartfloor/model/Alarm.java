package com.winsystems.smartfloor.model;

import android.text.Editable;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.Date;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: Alarm
//
// DESCRIPTION: Implements methods to handle alarms app.
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------

public class Alarm implements Serializable, Comparable<Alarm>{

  private String id;
  @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyy/MM/dd")
  private Date date;
  private String description;
  private LayoutSiteItemSeverity alarmCriticality;
  private LayoutSiteItemSubCategory siteItemSubCategory;
  private LayoutSiteItemGroupCategory alarmCategory;
  private Terminal terminal;
  private Boolean completed;
  private int status;
  private String title;

  public Alarm(){}

  public Alarm(String id,
               Date date,
               String description,
               LayoutSiteItemSeverity alarmCriticality,
               LayoutSiteItemGroupCategory alarmCategory,
               Terminal terminal,
               Boolean completed){
    this.id = id;
    this.date = date;
    this.description = description;
    this.alarmCriticality = alarmCriticality;
    this.alarmCategory = alarmCategory;
    this.terminal = terminal;
    this.completed = completed;
  }


  public LayoutSiteItemSeverity getAlarmCriticality() {
    return alarmCriticality;
  }

  public String getDescription() {
    return description;
  }

  public String getId() {
    return id;
  }

  public Date getDate() {
    return date;
  }

  public int getStatus() {
    return status;
  }

  public LayoutSiteItemGroupCategory getAlarmCategory() {
    return siteItemSubCategory.getGroupCategory();
  }

  public LayoutSiteItemSubCategory getSiteItemSubCategory() {
    return siteItemSubCategory;
  }

  public Terminal getTerminal() {
    return terminal;
  }

  public Boolean getCompleted() {
    return completed;
  }

  public void setSiteItemSubCategory(LayoutSiteItemSubCategory siteItemSubCategory) {
    this.siteItemSubCategory = siteItemSubCategory;
  }

  @Override
  public int compareTo(Alarm another) {
    return getDescription().compareTo(another.getDescription());
  }

  public void setTerminal(Terminal terminal) {
    this.terminal = terminal;
  }

  public void setDate(Date date) {
    this.date = date;
  }


  public void setStatus(int status) {
    this.status = status;
  }

  public void setAlarmCategory(LayoutSiteItemGroupCategory alarmCategory) {
    this.alarmCategory = alarmCategory;
  }

  public void setAlarmCriticality(LayoutSiteItemSeverity alarmCriticality) {
    this.alarmCriticality = alarmCriticality;
  }

  public void setCompleted(boolean completed) {
    this.completed = completed;
  }

  public void setDescription(String description){
    this.description = description;
  }

  public void setId(String id) {
    this.id = id;
  }

  public LayoutSiteTaskActions getStatusEnum() {
    return LayoutSiteTaskActions.GetActionLayoutSiteTaskAction(status);
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getTitle() {
    return title;
  }
}
