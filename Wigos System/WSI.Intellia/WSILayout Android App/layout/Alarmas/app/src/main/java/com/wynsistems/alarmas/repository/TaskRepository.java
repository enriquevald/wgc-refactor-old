package com.wynsistems.alarmas.repository;

import android.content.Context;

import com.wynsistems.alarmas.model.Alarm;
import com.wynsistems.alarmas.model.LayoutSiteItemGroupCategory;
import com.wynsistems.alarmas.model.LayoutSiteItemSeverity;
import com.wynsistems.alarmas.repository.response.AlarmResponse;
import com.wynsistems.alarmas.service.login.response.LayoutSiteTask;

import java.util.List;

public interface TaskRepository {
    LayoutSiteTask getById(String id);
    void saveFromAlarmResponse(List<LayoutSiteTask> tasks, Context context);
    List<LayoutSiteTask> getAlarmByCriticality(LayoutSiteItemSeverity alarmCriticality);
    List<LayoutSiteTask> getAlarmByCategory(LayoutSiteItemGroupCategory type);
    List<LayoutSiteTask> getAlarmsInProgress();
    List<LayoutSiteTask> getAlarmsAssigned();
}
