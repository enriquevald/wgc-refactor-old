package com.wynsistems.alarmas.service.alarm;

import android.content.Context;

import com.wynsistems.alarmas.model.Alarm;
import com.wynsistems.alarmas.model.LayoutSiteItemGroupCategory;
import com.wynsistems.alarmas.model.LayoutSiteItemSeverity;
import com.wynsistems.alarmas.model.LayoutSiteTaskActions;
import com.wynsistems.alarmas.service.DATA_PostActionResponse;
import com.wynsistems.alarmas.service.login.response.LayoutSiteTask;

import java.util.List;

public interface AlarmService {
    LayoutSiteTask getById(String id);
    List<LayoutSiteTask> getAll(String sessionId, Context context);
    List<LayoutSiteTask> getAlarmByCriticality(LayoutSiteItemSeverity alarmCriticality);
    List<LayoutSiteTask> getAlarmByCategory(LayoutSiteItemGroupCategory type);
    List<LayoutSiteTask> getAlarmsInProgress();
    List<LayoutSiteTask> getAlarmsAssigned();
    AlarmService setUrl(String url);

    DATA_PostActionResponse assignAlarm(String id, String userId, LayoutSiteTaskActions action);
    DATA_PostActionResponse createAlarm(Alarm alarm, String photoToSend, String userId, String url);

}
