package com.winsystems.smartfloor.model;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.io.Serializable;
import java.util.Hashtable;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: LayoutSiteManagerAvailable
//
// DESCRIPTION: Implements method for site manager available.
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public class LayoutSiteManagerAvailable implements KvmSerializable, Serializable {

  private Integer Id;
  private String UserName;
  public Integer getId() {
    return Id;
  }

  public void setId(Integer id) {
    Id = id;
  }

  public String getUserName() {
    return UserName;
  }

  public void setUserName(String userName) {
    UserName = userName;
  }

  @Override
  public Object getProperty(int i) {
    switch (i){
      case 0:
        return Id;
      case 1:
        return UserName;
      default:
        return null;
    }
  }

  @Override
  public int getPropertyCount() {
    return 2;
  }

  @Override
  public void setProperty(int i, Object o) {

    switch (i){
      case 0:
        Id = Integer.parseInt(o.toString());
      case 1:
        UserName = o.toString();
    }
  }

  @Override
  public void getPropertyInfo(int i, Hashtable hashtable, PropertyInfo propertyInfo) {
    switch (i){
      case 0:
        propertyInfo.type = PropertyInfo.INTEGER_CLASS;
        propertyInfo.name = "Id";
        break;
      case 1:
        propertyInfo.type = PropertyInfo.STRING_CLASS;
        propertyInfo.name = "UserName";
        break;
    }
  }
}
