package com.winsystems.smartfloor.view.notifications;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.winsystems.smartfloor.R;
import com.winsystems.smartfloor.common.LayoutNotification;
import com.winsystems.smartfloor.common.LayoutNotificationType;

import java.util.List;

public class NotificationListAdapter extends BaseAdapter {

  private Context context;
  private List<LayoutNotification> notifications;
  private LayoutNotificationType type;
  private final LayoutInflater inflater;

  public NotificationListAdapter(Context context, List<LayoutNotification> notificationItemList, LayoutNotificationType type) {
    this.context = context;
    this.notifications = notificationItemList;
    this.type = type;
    inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
  }


  @Override
  public int getCount() {
    return notifications.size();
  }

  @Override
  public Object getItem(int i) {
    return notifications.get(i);
  }

  @Override
  public long getItemId(int i) {
    return i;
  }

  @Override
  public View getView(int i, View view, ViewGroup viewGroup) {
    View vi = view;
    if (vi == null)
      vi = inflater.inflate(R.layout.notification_item, null);

    LayoutNotification notification = (LayoutNotification) this.getItem(i);
    TextView title = (TextView)vi.findViewById(R.id.notification_item_title);
    TextView content = (TextView)vi.findViewById(R.id.notification_item_content);
    ImageView icon = (ImageView)vi.findViewById(R.id.notification_item_icon);

    title.setText(notification.getId() + " - " + notification.getTitle());
    content.setText(notification.getContent());
    Integer notificationIcon = notification.getIcon();
    if(notificationIcon != null && notificationIcon != 0)
      icon.setImageBitmap(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(), notificationIcon), 64, 64, false));


    return vi;
  }


}
