package com.winsystems.smartfloor.view.utils;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: Constants
//
// DESCRIPTION: Constants used in the application.
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public class Constants {

    public static final String SELECTED_ALARM = "SelectedAlarm";
    public static final String SELECTED_GROUP_ALARMS ="SelectedGroupAlarms";
    public static final String TERMINAL_DETAILS = "TerminalDetails";

    public static final String SOLVED_ALARM = "Alarma resuelta";
    public static final String SCALED_ALARM = "Alarma escalada";

    public static final String USER_ID_PREFERENCE_EDITOR = "UserId";
    public static final String USER_ID = "UserId";
    public static final String DOWNLOAD_ALARMS = "Download_alarms";
    public static final String ALARM_ID = "ALARM_ID";
    public static final String LOGGED_USER_ALARM_SERVICE_ADDRESS = "LoggedUserAlarmServiceAddress";

    public static final String LOGGED_USER_CREATED_ALARM_SERVICE_ADDRESS = "LoggedUserCreatedAlarmServiceAddress";
    public static final String TERMINAL_ID = "terminalId";
    public static final String TERMINAL_NAME = "terminalName";
    public static final String UPDATED_TASKS = "UpdatedAlarms";

    public static final String ALARMS_CREATED_BY_USER = "_layoutSiteAlarmsOfDay";
    public static final String ROLES_OF_USER = "_rolesOfUser";
    public static final String TERMINAL_LOGS = "_terminal_logs";
    public static final String BEACONS_AVAILABLES = "_beacons_availables";
    public static final String MANAGER_ID = "managerId" ;
    public static final String TODAY_MESSAGES = "_messagesOfDay";
    public static final String MANAGER_USERNAME = "managerUserName";
    public static final String TASKS_ASSIGNED = "assignedTasks";
    public static final String FROM_NOTIFICATION = "fromNotifications";
    public static final String LOG = "log";
    public static final int APP_INSTALL_REQUEST = 1305;
    public static final String MANAGERS_AVAILABLES = "managersAvailables";
    public static final String PENDING_MESSAGES = "pendingMessages";
    public static final String NOTIFICATION_MANAGER_NOTIFICATION_LIST = "notificationManagerNotificationList";
    public static final String NOTIFICATION_TYPE = "NOTIFICATION_TYPE";
    public static final String NOTIFY_NEW_ICON_MENU_NOTIFICATION = "new_icon_menu_notification";
    public static final String NOTIFICATION_ID = "notification_id";
    public static final String NEW_VERSION_AVAILABLE = "NewVersionAvailable" ;
    public static final String NEW_TASK_ARRIVE = "NEW TASK ARRIVE";
    public static final String KEEP_ALIVE_FAIL = "KEEP_ALIVE_FAIL";
    public static final String KEEP_ALIVE_SUCCESS = "KEEP_ALIVE_SUCCESS";
    public static final int APP_LOG_MAX_SIZE = 200;
    public static final int MESSAGE_STATUS_DELAYED = 1;
    public static final int MESSAGE_STATUS_DELIVERED = 2;
    public static final int MESSAGE_STATUS_HASREAD = 3;
    public static final int MESSAGE_STATUS_SENT = 4;
    public static final String BEACONS_DETECTED = "BEACONS_DETECTED";
    public static final String GETLOG_REQUEST = "GETLOG_REQUEST";
    public static final String TERMINAL_INFORMATION_SHOWCASE = "TERMINAL_INFORMATION_SHOWCASE";
    public static final String CREATE_ALARM_SHOWCASE = "CREATE_ALARM_SHOWCASE";
}
