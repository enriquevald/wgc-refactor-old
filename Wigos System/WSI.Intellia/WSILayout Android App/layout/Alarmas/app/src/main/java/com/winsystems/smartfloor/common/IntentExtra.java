package com.winsystems.smartfloor.common;

import java.io.Serializable;

public class IntentExtra implements Serializable {

  private String name;
  private String value;

  public IntentExtra(String name, String value) {

    this.name = name;
    this.value = value;
  }

  public String getName() {
    return name;
  }

  public String getValue() {
    return value;
  }
}
