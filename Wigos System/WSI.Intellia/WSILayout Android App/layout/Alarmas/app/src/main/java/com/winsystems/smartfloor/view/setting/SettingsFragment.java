package com.winsystems.smartfloor.view.setting;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.MultiSelectListPreference;
import android.preference.Preference;
import android.preference.PreferenceCategory;
import android.preference.PreferenceFragment;
import android.preference.PreferenceGroup;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;

import com.winsystems.smartfloor.R;
import com.winsystems.smartfloor.helpers.IsLastestVersionHelper;
import com.winsystems.smartfloor.helpers.LanguageHelper;

import java.util.Locale;


public class SettingsFragment extends PreferenceFragment implements
    SharedPreferences.OnSharedPreferenceChangeListener{
  private String lang;
  private SharedPreferences prefs;


  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    addPreferencesFromResource(R.xml.fragment_settings);
    PreferenceManager.setDefaultValues(getActivity(), R.xml.fragment_settings, false);
    initSummaryOfPreferences(getPreferenceScreen());
    //Update Version Preference
    Preference updatePreference = findPreference("update-lastest-version");
    PreferenceCategory preferenceCategory = (PreferenceCategory) findPreference("advanced_settings");
    preferenceCategory.removePreference(updatePreference);

    if(!IsLastestVersionHelper.IsLastestVersion(getActivity().getApplicationContext())) {
      updatePreference.setIcon(R.drawable.ic_system_update_alt_black_18dp);
      preferenceCategory.addPreference(updatePreference);
    }
    //Restart Activiy by Language preference
    LanguageHelper.setCurrentLocale(Locale.getDefault(), this);
    prefs = getActivity().getSharedPreferences("CommonPrefs", Activity.MODE_PRIVATE);
    lang = prefs.getString("languageToLoad", Locale.getDefault().getDisplayLanguage());

    final Preference languageButton = findPreference("language");
    //languageButton.setSummary(capitalize(lang));
    languageButton.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
      @Override
      public boolean onPreferenceChange(Preference preference, Object newValue) {
        Preference language = findPreference("language");
        String locale = LanguageHelper.getLanguageByValue(Integer.valueOf(newValue.toString()));
        updatePreferenceIfNeeded(newValue, language, locale);
        restartParentActivityIfLanguageChanged();
        return true;
      }
    });
  }
  @Override
  public void onResume() {
    super.onResume();
    getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
  }

  @Override
  public void onPause() {
    super.onPause();
    getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
  }
  @Override
  public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
    updatePreferenceSummary(findPreference(key));
  }

  private void initSummaryOfPreferences(Preference preference) {
    if (preference instanceof PreferenceGroup) {
      PreferenceGroup preferenceGroup = (PreferenceGroup) preference;
      for (int i = 0; i < preferenceGroup.getPreferenceCount(); i++) {
        initSummaryOfPreferences(preferenceGroup.getPreference(i));
      }
    } else {
      updatePreferenceSummary(preference);
    }
  }

  private void updatePreferenceSummary(Preference preference) {
    if (preference instanceof ListPreference) {
      ListPreference listPreference = (ListPreference) preference;
      preference.setSummary(listPreference.getEntry());
    }
    if (preference instanceof EditTextPreference) {
      EditTextPreference editTextPreference = (EditTextPreference) preference;
      preference.setSummary(editTextPreference.getText());
    }
    if (preference instanceof MultiSelectListPreference) {
      EditTextPreference editTextPreference = (EditTextPreference) preference;
      preference.setSummary(editTextPreference.getText());
    }
  }
  private void updatePreferenceIfNeeded(Object newValue, Preference preference, String locale) {
    String[] localeInfo = locale.split("_");
    String language = localeInfo[0];
    String country = new String(localeInfo[1]).toUpperCase();
    Locale newLocale = new Locale(language, country);
    Locale.setDefault(newLocale);
    Resources res = getActivity().getResources();
    DisplayMetrics dm = res.getDisplayMetrics();
    Configuration conf = res.getConfiguration();
    conf.locale = newLocale;
    res.updateConfiguration(conf, dm);
    preference.setDefaultValue(newValue);
  }

  private void restartParentActivityIfLanguageChanged() {
    String oldLanguage = lang;
    lang = prefs.getString("languageToLoad", Locale.getDefault().getDisplayLanguage());
    if (!oldLanguage.equals(lang)){
      restartActivity();
    }
  }

  private void restartActivity() {
    getActivity().finish();
    startActivity(getActivity().getIntent());
  }

  private String capitalize(final String line) {
    return Character.toUpperCase(line.charAt(0)) + line.substring(1);
  }
}
