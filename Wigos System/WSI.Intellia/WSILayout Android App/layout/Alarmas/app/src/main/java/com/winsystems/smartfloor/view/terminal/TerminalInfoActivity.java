package com.winsystems.smartfloor.view.terminal;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.winsystems.smartfloor.R;
import com.winsystems.smartfloor.common.AndroidLogger;
import com.winsystems.smartfloor.helpers.LogHelper;
import com.winsystems.smartfloor.helpers.SaveInformationHelper;
import com.winsystems.smartfloor.model.LayoutSiteAlarm;
import com.winsystems.smartfloor.service.ResponseCode;
import com.winsystems.smartfloor.service.ServiceFactory;
import com.winsystems.smartfloor.service.alarm.response.DATA_TerminalInfoResponse;
import com.winsystems.smartfloor.service.login.response.LayoutSiteTaskTerminal;
import com.winsystems.smartfloor.view.camera.CaptureActivityAnyOrientation;
import com.winsystems.smartfloor.view.utils.Constants;
import com.winsystems.smartfloor.view.utils.KioskModeActivity;
import com.winsystems.smartfloor.view.utils.ToastFactory;

import java.util.List;

import uk.co.deanwild.materialshowcaseview.MaterialShowcaseSequence;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;
import uk.co.deanwild.materialshowcaseview.ShowcaseConfig;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: TerminalInfoActivity
//
// DESCRIPTION: Implements the functionality for terminal info activity for code qr or identifier.
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public class TerminalInfoActivity extends KioskModeActivity {
    private SharedPreferences userSettings;
    private SharedPreferences environmentSettings;
    private String userId;
    private String url;
    private String qrContent;
    private ProgressDialog dialog;
    private ImageButton terminalQRCodeButton;
    private Button sendTerminalIdButton;
    private TextView textView;
    private static AndroidLogger mLog;

    public TerminalInfoActivity() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLog = LogHelper.getLogger(this);
        setContentView(R.layout.terminal_status_terminal_selection);
        setTitle(R.string.title_activity_terminal_status);

        userSettings = this.getSharedPreferences(Constants.USER_ID_PREFERENCE_EDITOR, 0) ;
        environmentSettings = PreferenceManager.getDefaultSharedPreferences(this);

        userId = userSettings.getString(Constants.USER_ID, userId);
        url = environmentSettings.getString(getString(R.string.server_url), url);

        buildDialog();

        terminalQRCodeButton = (ImageButton) this.findViewById(R.id.terminal_qr_code_button);
        sendTerminalIdButton = (Button) this.findViewById(R.id.select_terminal_id);
        textView = (TextView)this.findViewById(R.id.new_alarm_terminal_provider_field);
        sendTerminalIdButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!textView.getText().toString().isEmpty()) {
                    new GetTerminalStatusInformation().execute();
                }
                else{
                    ToastFactory.buildSimpleToast(getResources().getString(R.string.error_field_required_new_alarm), TerminalInfoActivity.this);
                }
            }
        });
        terminalQRCodeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentIntegrator scanIntegrator = new IntentIntegrator(TerminalInfoActivity.this);
                scanIntegrator.setCaptureActivity(CaptureActivityAnyOrientation.class);
                scanIntegrator.setOrientationLocked(false);
                scanIntegrator.initiateScan();
            }
        });

        presentShowcaseSequence();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {


        IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
            if (scanningResult != null && scanningResult.getContents() != "") {
                String result = scanningResult.getContents();
                String[] splitResults = result.split("#");
                if(splitResults.length > 1)
                    textView.setText(splitResults[1]);
                else
                    textView.setText("");
            }else {
                Toast toast = Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_data_received), Toast.LENGTH_SHORT);
                toast.show();
            }
    }

    private void buildDialog() {
        this.dialog = new ProgressDialog(TerminalInfoActivity.this);
        this.dialog.setTitle(TerminalInfoActivity.this.getResources().getString(R.string.title_progress_dialog));
        this.dialog.setMessage(TerminalInfoActivity.this.getResources().getString(R.string.alarm_message_progress_dialog));
        this.dialog.setCanceledOnTouchOutside(false);

    }

    private void presentShowcaseSequence() {

        ShowcaseConfig config = new ShowcaseConfig();
        config.setDelay(500);
        ScrollView scrollView = (ScrollView) findViewById(R.id.scrollView);
        MaterialShowcaseSequence sequence = new MaterialShowcaseSequence(this, Constants.TERMINAL_INFORMATION_SHOWCASE);

        sequence.setConfig(config);
        sequence.addSequenceItem(new MaterialShowcaseView.Builder(this)
                .setTarget(scrollView)
                .setContentText(getString(R.string.terminals_showcase_title))
                .withRectangleShape(true)
                .setDismissOnTouch(true)
                .setMaskColour(getResources().getColor(R.color.showcase_blue))
                .build());
        sequence.addSequenceItem(
                new MaterialShowcaseView.Builder(this)
                        .setTarget(terminalQRCodeButton)
                        .setDismissOnTouch(true)
                        .setMaskColour(getResources().getColor(R.color.showcase_blue))
                        .setContentText(getString(R.string.terminals_showcase_qr_button))
                        .withRectangleShape()
                        .build()
        );

        sequence.addSequenceItem(
                new MaterialShowcaseView.Builder(this)
                        .setTarget(textView)
                        .setContentText(getString(R.string.terminals_showcase_text_identifier))
                        .setDismissOnTouch(true)
                        .setMaskColour(getResources().getColor(R.color.showcase_blue))
                        .withRectangleShape()
                        .build()
        );
        sequence.addSequenceItem(new MaterialShowcaseView.Builder(this)
                .setTarget(sendTerminalIdButton)
                .setContentText(getString(R.string.terminals_showcase_select_button))
                .setDismissOnTouch(true)
                .setMaskColour(getResources().getColor(R.color.showcase_blue))
                .withRectangleShape(true)
                .build());

        sequence.start();
    }

    private class GetTerminalStatusInformation extends AsyncTask<Void,Void, Boolean> {

        private String text;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.show();
            text = textView.getText().toString();

        }

        @Override
        protected Boolean doInBackground(Void... params) {

            try {
                final DATA_TerminalInfoResponse response = ServiceFactory.getInstance().getLayoutSiteAlarmsService(getApplicationContext()).getTerminalStatusByTerminalID(url, userId, text.toString());
                if (response.getResponseCode() != ResponseCode.DATA_RC_OK)
                {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast toast = Toast.makeText(getApplicationContext(), R.string.WEBSERVICE_NO_DATA, Toast.LENGTH_SHORT);
                            toast.show();
                        }
                    });
                    return false;
                }
                mLog.info("TerminalStatus // DoInBackground Success");
                return true;
            } catch (Exception e){
                mLog.error("",e);
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            if(result) {
                Intent intent = new Intent(TerminalInfoActivity.this, TerminalInfoMenuActivity.class);

                SaveInformationHelper<LayoutSiteAlarm> saveInformationHelper = new SaveInformationHelper<>(getApplicationContext());
                List<LayoutSiteAlarm> list = saveInformationHelper.readList(userId + "_layoutSiteAlarms");

                if(list.size() > 0) {
                    LayoutSiteTaskTerminal terminal = list.get(0).getTerminal();
                    intent.putExtra(Constants.TERMINAL_NAME, terminal.getName());
                    intent.putExtra(Constants.TERMINAL_ID, terminal.getId());
                }

                startActivity(intent);
            }
        }
    }

    @Override
    public void onConnectivityChange(){
        Button button = (Button) this.findViewById(R.id.select_terminal_id);

        if(button == null)
        {
            return;
        }

        boolean isConnected = getIsConnected();
        if(!isConnected)
        {
            button.setEnabled(false);
        }
        else{
            if(!button.isEnabled()){
                button.setEnabled(true);
            }
        }
    }

    @Override
    public void onKeepAliveChange (int keepAliveType){
        Button button = (Button) this.findViewById(R.id.select_terminal_id);

        if(button == null)
        {
            return;
        }

        boolean isConnected = keepAliveType == 0;
        if(!isConnected)
        {
            button.setEnabled(false);
        }
        else{
            if(!button.isEnabled()){
                button.setEnabled(true);
            }
        }

    }

}
