package com.wynsistems.alarmas.view.terminal;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.wynsistems.alarmas.R;
import com.wynsistems.alarmas.model.LayoutSiteAlarm;
import com.wynsistems.alarmas.helpers.AlarmResourseHelper;

import java.util.ArrayList;
import java.util.List;

public class LayoutSiteAlarmListAdapter extends BaseAdapter {

    private List<LayoutSiteAlarm> alarms;
    private static LayoutInflater inflater = null;
    private Context _context;
    public LayoutSiteAlarmListAdapter(Context context, List<LayoutSiteAlarm> data) {
        _context = context;
        if(data != null)
            this.alarms = data;
        else
            this.alarms = new ArrayList<>();

        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return this.alarms.size();
    }

    @Override
    public Object getItem(int position) {
        return this.alarms.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.terminal_status_alarm_list_item, null);

        LayoutSiteAlarm alarm = (LayoutSiteAlarm)getItem(position);

        TextView terminalId = (TextView)convertView.findViewById(R.id.terminal_id);
        TextView catergoryDescription = (TextView)convertView.findViewById(R.id.category_description);

        terminalId.setText(alarm.getTerminal().getName());
        terminalId.setTextColor(Color.GRAY);
        catergoryDescription.setText(alarm.getSubCategory().getDescription(_context));
        catergoryDescription.setTextColor(Color.GRAY);

        FrameLayout imageView = (FrameLayout) convertView.findViewById(R.id.alarm_criticality_type);
        GradientDrawable bgShape = (GradientDrawable)imageView.getBackground();
        bgShape.setColor(convertView.getResources().getColor(R.color.background_action_bar));

        ImageView alarmCategoryType = (ImageView) convertView.findViewById(R.id.alarm_category_image);
        alarmCategoryType.setImageResource(AlarmResourseHelper.getCategoryImageByType(alarm.getSubCategory().getGroupCategory()));

        return convertView;
    }
}
