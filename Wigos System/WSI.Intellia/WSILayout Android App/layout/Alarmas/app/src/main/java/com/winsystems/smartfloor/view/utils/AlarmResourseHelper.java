package com.winsystems.smartfloor.view.utils;


import android.content.Context;
import android.content.res.Resources;

import com.winsystems.smartfloor.R;
import com.winsystems.smartfloor.model.LayoutSiteItemGroupCategory;
import com.winsystems.smartfloor.model.LayoutSiteItemSeverity;

public class AlarmResourseHelper {

  public static int getColorByCriticality(LayoutSiteItemSeverity alarmCriticality, Resources resources)
  {
    switch (alarmCriticality){
      case HIGH:
        return resources.getColor(R.color.winsystem_red);
      case MEDIUM:
        return resources.getColor(R.color.winsystem_yellow);
      case LOW:
        return resources.getColor(R.color.winsystem_blue);
      default:
        return resources.getColor(R.color.winsystem_blue);
    }
  }

  public static int getCategoryImageByType(LayoutSiteItemGroupCategory type) {

    switch (type) {
      case PayCheck:
        return R.drawable.playedwon;
      case Intrusion:
        return R.drawable.door;
      case JamBill:
        return R.drawable.bill;
      case MachineFlags:
        return R.drawable.machineflags;
      case Support:
        return R.drawable.support;
      case Printer:
        return R.drawable.printer;
      default:
        return R.drawable.door;
    }
  }

  public static int getCompletedImageByState(Boolean completed) {

    if(completed)
      return R.drawable.completed;

    return R.drawable.not_completed;
  }

  public static String getCriticalityDescriptionByCriticality(LayoutSiteItemSeverity alarmCriticality, Context context) {

    switch (alarmCriticality) {
      case LOW:
        return context.getResources().getString(R.string.low_level_description);
      case MEDIUM:
        return context.getResources().getString(R.string.medium_level_description);
      case HIGH:
        return context.getResources().getString(R.string.high_level_description);
      default:
        return "";
    }
  }


}
