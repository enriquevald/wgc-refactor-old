package com.winsystems.smartfloor.common;

import android.util.Log;

import com.winsystems.smartfloor.helpers.SaveInformationHelper;
import com.winsystems.smartfloor.view.utils.Constants;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: AndroidLogger
//
// DESCRIPTION: Implements records of errors, warnings and information.
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public class AndroidLogger{
    private SaveInformationHelper<String> _logFile;

    public AndroidLogger(SaveInformationHelper<String> logFile)
    {
        _logFile = logFile;
    }


    public void warning(String message, Throwable e)
    {
        String packageName = this.getClass().getCanonicalName().toLowerCase().replace(".common.androidlogger", "") + ".";
        StackTraceElement[] stacktrace = Thread.currentThread().getStackTrace();
        StackTraceElement ste = stacktrace[3];//obtengo el metodo que llamó al logger
        String methodName = ste.getClassName().toLowerCase().replace(packageName, "") + "." + ste.getMethodName();

        List<String> logs = _logFile.readList(Constants.LOG);
        String eMessage = e != null? e.getMessage() : "";
        String finalMessageException = eMessage.isEmpty()? "NO MESSAGE TO SHOW" : eMessage;
        logs.add(new Date() + " - WARNING - "+ methodName + " - " + message + " - Exception: " + finalMessageException);

        _logFile.writeList(logs, Constants.LOG);
        Log.w(methodName, finalMessageException);

    }

    public void error(String message, Throwable e)
    {
        String packageName = this.getClass().getCanonicalName().toLowerCase().replace(".common.androidlogger","") + ".";
        StackTraceElement[] stacktrace = Thread.currentThread().getStackTrace();
        StackTraceElement ste = stacktrace[3];//obtengo el metodo que llamó al logger
        String methodName = ste.getClassName().toLowerCase().replace(packageName,"") + "." + ste.getMethodName();

        List<String> logs = _logFile.readList(Constants.LOG);
        String eMessage = e != null? e.getMessage() : "";
        String finalMessageException = eMessage.isEmpty()? "NO MESSAGE TO SHOW" : eMessage;
        logs.add(new Date() + " - ERROR - " + methodName + " - " + message + " - Exception: " + finalMessageException);

        _logFile.writeList(logs, Constants.LOG);
        Log.e(methodName, finalMessageException);
    }

    public void info (String message)
    {
        String packageName = this.getClass().getCanonicalName().toLowerCase().replace(".common.androidlogger", "") + ".";
        StackTraceElement[] stacktrace = Thread.currentThread().getStackTrace();
        StackTraceElement ste = stacktrace[3];//obtengo el metodo que llamó al logger
        String methodName = ste.getClassName().toLowerCase().replace(packageName, "") + "." + ste.getMethodName();

        List<String> logs = _logFile.readList(Constants.LOG);
        logs.add(new Date() + " - INFO - " + methodName + " - " + message);

        _logFile.writeList(logs, Constants.LOG);
        Log.i(methodName, message);

    }

    public void trim()
    {
        List<String> logs = _logFile.readList(Constants.LOG);

        if(logs.size()>Constants.APP_LOG_MAX_SIZE){
            logs = new ArrayList(logs.subList((logs.size()- 1- Constants.APP_LOG_MAX_SIZE),logs.size()-1));
        }

        _logFile.writeList(logs, Constants.LOG);
    }


    public String content() {
        String log = "";

        if(_logFile != null)
        {
            log =  _logFile.readList(Constants.LOG).toString();
        }

        return log;
    }
}
