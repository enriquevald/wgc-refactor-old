package com.wynsistems.alarmas.service.login;

import android.content.Context;

import com.wynsistems.alarmas.service.DATA_PostActionResponse;
import com.wynsistems.alarmas.service.login.response.DATA_LogInResponse;

public interface LoginService {
    DATA_LogInResponse loginUser(Context appContext, String username, String password, String url, String deviceId);
    DATA_PostActionResponse keepALive(String session,String url);
}
