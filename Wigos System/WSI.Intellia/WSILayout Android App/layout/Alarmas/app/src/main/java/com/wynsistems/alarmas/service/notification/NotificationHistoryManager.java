package com.wynsistems.alarmas.service.notification;

import android.content.Context;

import com.wynsistems.alarmas.helpers.SaveInformationHelper;

import java.util.List;

public class NotificationHistoryManager {

    private SaveInformationHelper<String> notificationRepository;

    public NotificationHistoryManager(Context context){
        notificationRepository = new SaveInformationHelper<>(context);
    }

    public List<String> getNotificationsByKey(String managerId) {
        return notificationRepository.readList("NotificationsFromUser" + managerId);
    }

    public void AddNotification(String managerId, String content) {
        List<String> notifications =  notificationRepository.readList("NotificationsFromUser" + managerId);
        notifications.add(content);
        notificationRepository.writeList(notifications, "NotificationsFromUser" + managerId);
    }

    public void ClearNotifications(String managerId) {
        notificationRepository.clearList("NotificationsFromUser" + managerId);
    }

    public boolean AvailableNotificationsWithSameKey(String managerId) {

        return notificationRepository.readList("NotificationsFromUser" + managerId).size() > 0;
    }
}
