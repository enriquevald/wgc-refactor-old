package com.wynsistems.alarmas.service.login.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.io.Serializable;
import java.util.Date;
import java.util.Hashtable;

public class LayoutSiteTask implements KvmSerializable,Serializable, Comparable<LayoutSiteTask>{
    private Integer id;
    private Integer status;
    private Date start;
    private Date end;
    private Date creation;
    private Integer category;
    private Integer subcategory;
    private Integer terminalId;
    private Integer accountId;
    private String description;
    private Integer severity;
    private LayoutSiteTaskTerminal terminal;

    public LayoutSiteTask(){}

    public String getDescription() {
        return description;
    }

    public Integer getId() {
        return id;
    }

    public Date getCreation() {
        return creation;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public Integer getCategory() {
        return category;
    }

    public void setCategory(Integer category) {
        this.category = category;
    }

    public Integer getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(Integer subcategory) {
        this.subcategory = subcategory;
    }

    public Integer getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(Integer terminalId) {
        this.terminalId = terminalId;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getSeverity() {
        return severity;
    }

    public void setCreation(Date creation) {
        this.creation = creation;
    }

    public void setSeverity(Integer severity) {
        this.severity = severity;
    }

    public LayoutSiteTaskTerminal getTerminal() {
        return terminal;
    }

    public void setTerminal(LayoutSiteTaskTerminal terminal) {
        this.terminal = terminal;
    }

    @Override
    public int compareTo(LayoutSiteTask another) {
        return getId().compareTo(another.getId());
    }

    @Override
    public Object getProperty(int i) {
        switch (i){
            case 0:
                return accountId;
            case 1:
                return category;
            case 2:
                return creation;
            case 3:
                return description;
            case 4:
                return end;
            case 5:
                return id;
            case 6:
                return severity;
            case 7:
                return start;
            case 8:
                return status;
            case 9:
                return subcategory;
            case 10:
                return terminal;
            case 11:
                return terminalId;
            default:
                return null;
        }
    }

    @Override
    public int getPropertyCount() {
        return 11;
    }

    @Override
    public void setProperty(int i, Object o) {
        switch (i){
            case 0:
                accountId = Integer.parseInt(o.toString());
            case 1:
                category = Integer.parseInt(o.toString());
            case 2:
                creation = (Date) o;
            case 3:
                description = o.toString();
            case 4:
                end = (Date) o;
            case 5:
                id = Integer.parseInt(o.toString());
            case 6:
                severity = Integer.parseInt(o.toString());
            case 7:
                start = (Date) o;
            case 8:
                status = Integer.parseInt(o.toString());
            case 9:
                subcategory = Integer.parseInt(o.toString());
            case 10:
                terminal = (LayoutSiteTaskTerminal) o;
            case 11:
                terminalId = Integer.parseInt(o.toString());
        }
    }

    @Override
    public void getPropertyInfo(int i, Hashtable hashtable, PropertyInfo propertyInfo) {
        switch (i){
            case 0:
                propertyInfo.type = PropertyInfo.INTEGER_CLASS;
                propertyInfo.name = "AccountId";
            case 1:
                propertyInfo.type = PropertyInfo.INTEGER_CLASS;
                propertyInfo.name = "Category";
            case 2:
                propertyInfo.type = PropertyInfo.OBJECT_CLASS;
                propertyInfo.name = "Creation";
            case 3:
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                propertyInfo.name = "Description";
            case 4:
                propertyInfo.type = PropertyInfo.OBJECT_CLASS;
                propertyInfo.name = "End";
            case 5:
                propertyInfo.type = PropertyInfo.INTEGER_CLASS;
                propertyInfo.name = "Id";
            case 6:
                propertyInfo.type = PropertyInfo.INTEGER_CLASS;
                propertyInfo.name = "Severity";
            case 7:
                propertyInfo.type = PropertyInfo.OBJECT_CLASS;
                propertyInfo.name = "Start";
            case 8:
                propertyInfo.type = PropertyInfo.INTEGER_CLASS;
                propertyInfo.name = "Status";
            case 9:
                propertyInfo.type = PropertyInfo.INTEGER_CLASS;
                propertyInfo.name = "SubCategory";
            case 10:
                propertyInfo.type = PropertyInfo.OBJECT_CLASS;
                propertyInfo.name = "Terminal";
            case 11:
                propertyInfo.type = PropertyInfo.INTEGER_CLASS;
                propertyInfo.name = "TerminalId";
        }
    }
}
