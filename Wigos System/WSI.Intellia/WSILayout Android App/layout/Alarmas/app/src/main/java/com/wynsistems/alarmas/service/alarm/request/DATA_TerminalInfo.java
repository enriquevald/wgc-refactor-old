package com.wynsistems.alarmas.service.alarm.request;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.util.Hashtable;

/**
 * Created by Charles on 23/07/2015.
 */
public class DATA_TerminalInfo implements KvmSerializable{

    private String terminalId;
    private String sessionId;

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    @Override
    public Object getProperty(int i) {
        switch (i)
        {
            case 0:
                return sessionId;
            default:
                return terminalId;
        }
    }

    @Override
    public int getPropertyCount() {
        return 2;
    }

    @Override
    public void setProperty(int i, Object o) {
        switch (i)
        {
            case 0:
                sessionId = o.toString();
                break;
            case 1:
                terminalId = o.toString();
                break;
        }
    }

    @Override
    public void getPropertyInfo(int i, Hashtable hashtable, PropertyInfo propertyInfo) {
        switch (i){
            case 0:
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                propertyInfo.name = "SessionId";
                break;
            case 1:
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                propertyInfo.name = "TerminalId";
                break;
        }
    }
}
