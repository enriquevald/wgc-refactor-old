package com.winsystems.smartfloor.view.task;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.winsystems.smartfloor.R;
import com.winsystems.smartfloor.model.SummarizedAlarms;
import com.winsystems.smartfloor.repository.factory.SummarizedAlarmsFactory;
import com.winsystems.smartfloor.service.ServiceFactory;
import com.winsystems.smartfloor.service.alarm.AlarmService;
import com.winsystems.smartfloor.service.login.response.LayoutSiteTask;
import com.winsystems.smartfloor.service.mqtt.PahoMqttService;
import com.winsystems.smartfloor.view.setting.SettingsActivity;
import com.winsystems.smartfloor.view.utils.Constants;
import com.winsystems.smartfloor.view.utils.KioskModeActivity;

import java.util.List;


public class TaskListActivity extends KioskModeActivity implements SharedPreferences.OnSharedPreferenceChangeListener{

  private SharedPreferences prefs;

  private ProgressDialog dialog;
  private static String alarmServerUrl = "";
  private static String loggedUserAlarmServiceAddress;
  private String userId;
  private List<SummarizedAlarms> listAlarm;
  private SwipeRefreshLayout pullToRefresh;
  private IntentFilter intentFilter = new IntentFilter(PahoMqttService.NEW_TASK_ARRIVE);
  private BroadcastReceiver tasksUpdatedBroadcastReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      refreshView();
    }
  };

  private void refreshView() {
    runOnUiThread(new Runnable() {
      @Override
      public void run() {
        fillAlarmListView();
      }
    });
  }
  @Override
  protected void onStart() {
    super.onStart();
    fillAlarmListView();
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_task_list);
    setTitle(R.string.title_activity_task_list);

    prefs = PreferenceManager.getDefaultSharedPreferences(this);//get the preferences that are allowed to be given
    prefs.registerOnSharedPreferenceChangeListener(this);//set the listener to listen for changes in the preferences

    SharedPreferences settings = TaskListActivity.this.getSharedPreferences(Constants.USER_ID_PREFERENCE_EDITOR, 0);
    userId = settings.getString(Constants.USER_ID, "");

    alarmServerUrl = prefs.getString("alarm_server_address", alarmServerUrl);

    buildDialog();

    SharedPreferences.Editor editor = settings.edit();
    editor.putString(Constants.LOGGED_USER_ALARM_SERVICE_ADDRESS, alarmServerUrl);
    editor.apply();

    intentFilter.addAction(Constants.UPDATED_TASKS);
    intentFilter.addAction(PahoMqttService.NEW_TASK_ARRIVE);

    pullToRefresh = (SwipeRefreshLayout) findViewById(R.id.pullToRefresh);
    pullToRefresh.setColorSchemeColors(R.color.winsystem_blue,
        R.color.winsystem_red,
        R.color.winsystem_yellow);

    pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

      @Override
      public void onRefresh() {
        new UpdateTasksAsync().execute();
      }
    });

    loggedUserAlarmServiceAddress = alarmServerUrl;
  }


  @Override
  public void onResume(){
    super.onResume();
    fillAlarmListView();
    registerReceiver(tasksUpdatedBroadcastReceiver, intentFilter);
  }
  @Override
  protected void onPause() {
    this.dialog.dismiss();
    super.onPause();
    prefs.unregisterOnSharedPreferenceChangeListener(this);
    unregisterReceiver(tasksUpdatedBroadcastReceiver);
  }

  @Override
  protected void onStop() {
    this.dialog.dismiss();
    super.onStop();
    prefs.unregisterOnSharedPreferenceChangeListener(this);
  }


  @Override
  public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

  }


  private void buildDialog() {
    this.dialog = new ProgressDialog(TaskListActivity.this);
    this.dialog.setTitle(TaskListActivity.this.getResources().getString(R.string.title_progress_dialog));
    this.dialog.setMessage(TaskListActivity.this.getResources().getString(R.string.alarm_message_progress_dialog));
    this.dialog.setCanceledOnTouchOutside(false);
  }



  private void fillAlarmListView() {
    runOnUiThread(new Runnable() {
      @Override
      public void run() {
        ExpandableListView alarmListView = (ExpandableListView) findViewById( R.id.taskDetailList);
        SummarizedAlarmsFactory summarizedAlarmsFactory = SummarizedAlarmsFactory.getInstance(loggedUserAlarmServiceAddress, getApplicationContext());

        switch (groupPreference) {
          case CRITICALITY:
            listAlarm = summarizedAlarmsFactory.getSummarizedAlarmsByCriticality(sortPreference, getApplicationContext());
            break;
          case CATEGORY:
            listAlarm = summarizedAlarmsFactory.getSummarizedAlarmsByCategory(sortPreference, getApplicationContext());
            break;
          default:
            listAlarm = summarizedAlarmsFactory.getSummarizedAlarmsByCompleted(sortPreference, getApplicationContext());
            break;
        }

        final TaskExpandableListAdapter adapter = new TaskExpandableListAdapter(getApplicationContext(), listAlarm);

        alarmListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

          @Override
          public boolean onChildClick(ExpandableListView parent, View v,
                                      int groupPosition, int childPosition, long id) {
            LayoutSiteTask item = listAlarm.get(groupPosition).getTasks().get(childPosition);

            Intent intent = new Intent(TaskListActivity.this, TaskDetailActivity.class);
            intent.putExtra(Constants.SELECTED_ALARM, item);
            startActivity(intent);
            return true;
          }
        });

        alarmListView.setAdapter(adapter);
      }
    });
  }

  private class UpdateTasksAsync extends AsyncTask<Void, Void, Boolean> {
    @Override
    protected void onPreExecute() {
      super.onPreExecute();
      pullToRefresh.setRefreshing(true);
    }

    @Override
    protected Boolean doInBackground(Void... params) {
      try {
        AlarmService service = ServiceFactory.getInstance().getAlarmService(getApplicationContext());
        service.setUrl(alarmServerUrl);
        service.getAll(userId, getApplicationContext());
        return true;
      }catch (Exception e)
      {
        e.printStackTrace();
        return false;
      }
    }

    @Override
    protected void onPostExecute(Boolean result) {
      if(result)
        runOnUiThread(new Runnable() {
          @Override
          public void run() {
            fillAlarmListView();
          }
        });

      pullToRefresh.setRefreshing(false);
    }
  }


}
