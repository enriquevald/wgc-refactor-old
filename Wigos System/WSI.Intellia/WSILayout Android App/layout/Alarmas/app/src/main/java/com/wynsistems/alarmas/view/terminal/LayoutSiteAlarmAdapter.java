package com.wynsistems.alarmas.view.terminal;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.wynsistems.alarmas.R;
import com.wynsistems.alarmas.model.LayoutSiteAlarm;
import com.wynsistems.alarmas.model.LayoutSiteItemSubCategory;
import com.wynsistems.alarmas.model.LayoutSiteTaskActions;
import com.wynsistems.alarmas.helpers.AlarmResourseHelper;

import java.util.List;

public class LayoutSiteAlarmAdapter extends BaseAdapter {

    private List<LayoutSiteAlarm> alarms;
    private static LayoutInflater inflater = null;
    private Context _context;
    public LayoutSiteAlarmAdapter(Context context, List<LayoutSiteAlarm> data) {
        _context = context;
        this.alarms = data;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return this.alarms.size();
    }

    @Override
    public Object getItem(int position) {
        return this.alarms.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi;
        vi = inflater.inflate(R.layout.terminal_status_alarm_item, null);

        LayoutSiteAlarm alarm = (LayoutSiteAlarm)getItem(position);

        TextView alarm_to_task = (TextView)vi.findViewById(R.id.alarm_to_task);
        TextView catergoryDescription = (TextView)vi.findViewById(R.id.category_description);

        Integer taskStatus = alarm.getStatusOfTask();

        if(taskStatus != 0) {

            if (LayoutSiteTaskActions.GetActionLayoutSiteTaskAction(taskStatus) == LayoutSiteTaskActions.Assigned) {
                alarm_to_task.setText(R.string.Pending);
                alarm_to_task.setBackgroundResource(R.drawable.rectangle_red);
            } else {
                alarm_to_task.setText(R.string.InProgress);
                alarm_to_task.setBackgroundResource(R.drawable.rectangle_blue);
            }
        }else
        {
            alarm_to_task.setText(R.string.NotAssigned);
            alarm_to_task.setBackgroundResource(R.drawable.rectangle_grey);
        }
        LayoutSiteItemSubCategory subCategory = alarm.getSubCategory();
        catergoryDescription.setText(subCategory != null ? subCategory.getDescription(_context) : "");

        FrameLayout imageView = (FrameLayout) vi.findViewById(R.id.alarm_criticality_type);
        GradientDrawable bgShape = (GradientDrawable)imageView.getBackground();
        bgShape.setColor(vi.getResources().getColor(R.color.background_action_bar));

        ImageView alarmCategoryType = (ImageView) vi.findViewById(R.id.alarm_category_image);
        if(subCategory != null)
            alarmCategoryType.setImageResource(AlarmResourseHelper.getCategoryImageByType(subCategory.getGroupCategory()));

        return vi;
    }
}
