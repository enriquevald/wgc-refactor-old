package com.wynsistems.alarmas.service.login.response;

import com.wynsistems.alarmas.model.LayoutPlayerSession;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.io.Serializable;
import java.util.Hashtable;


public class LayoutSiteTaskTerminal implements KvmSerializable, Serializable {

    private String provider;
    private String name;
    private String area;
    private String bank;
    private String model;

    public void setPlayerSession(LayoutPlayerSession playerSession) {
        this.playerSession = playerSession;
    }

    public LayoutPlayerSession getPlayerSession() {
        return playerSession;
    }

    private LayoutPlayerSession playerSession;

    @Override
    public Object getProperty(int i) {
        switch (i){
            case 0:
                return area;
            case 1:
                return bank;
            case 2:
                return model;
            case 3:
                return name;
            case 4:
                return provider;
            default:
                return null;
        }
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int getPropertyCount() {
        return 5;
    }

    @Override
    public void setProperty(int i, Object o) {
        switch (i){
            case 0:
                area = o.toString();
                break;
            case 1:
                bank = o.toString();
                break;
            case 2:
                model = o.toString();
                break;
            case 3:
                name = o.toString();
                break;
            case 4:
                provider = o.toString();
                break;
        }
    }

    @Override
    public void getPropertyInfo(int i, Hashtable hashtable, PropertyInfo propertyInfo) {
        switch (i){
            case 0:
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                propertyInfo.name = "Area";
                break;
            case 1:
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                propertyInfo.name = "Bank";
                break;
            case 2:
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                propertyInfo.name = "Model";
                break;
            case 3:
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                propertyInfo.name = "Name";
                break;
            case 4:
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                propertyInfo.name = "Provider";
                break;

        }
    }
}
