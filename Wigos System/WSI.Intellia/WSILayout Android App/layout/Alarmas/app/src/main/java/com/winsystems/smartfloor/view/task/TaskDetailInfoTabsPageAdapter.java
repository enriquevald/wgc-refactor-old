package com.winsystems.smartfloor.view.task;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class TaskDetailInfoTabsPageAdapter extends FragmentPagerAdapter {

  private Fragment taskDetailsInfoFragment;
  private Fragment playerInfoFragment;

  public TaskDetailInfoTabsPageAdapter(FragmentManager fm) {
    super(fm);
  }

  @Override
  public Fragment getItem(int index) {

    switch (index) {
      case 0:
        // Top Rated fragment activity
        Fragment fragment = new TaskDetailsInfoFragment();
        taskDetailsInfoFragment = fragment;
        return fragment;
      case 1:
        // Games fragment activity
        Fragment fragment2 = new PlayerInfoFragment();
        playerInfoFragment = fragment2;
        return fragment2;
    }

    return null;
  }

  @Override
  public int getCount() {
    // get item count - equal to number of tabs
    return 2;
  }

  public Fragment getTaskDetailsInfoFragment() {
    return taskDetailsInfoFragment;
  }

  public Fragment getPlayerInfoFragment() {
    return playerInfoFragment;
  }
}