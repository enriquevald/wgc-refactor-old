package com.winsystems.smartfloor.service.message;

import android.content.Context;
import android.util.Log;

import com.winsystems.smartfloor.R;
import com.winsystems.smartfloor.common.AndroidLogger;
import com.winsystems.smartfloor.common.TrustManagerManipulator;
import com.winsystems.smartfloor.helpers.LogHelper;
import com.winsystems.smartfloor.helpers.SaveInformationHelper;
import com.winsystems.smartfloor.model.LayoutSiteManagerAvailable;
import com.winsystems.smartfloor.model.LayoutSiteManagerRunnerConversationItem;
import com.winsystems.smartfloor.service.ResponseCode;
import com.winsystems.smartfloor.common.ServerEntitiesBuilder;
import com.winsystems.smartfloor.service.message.request.DATA_GetConversationHistoryRequest;
import com.winsystems.smartfloor.service.message.request.DATA_GetConversationsOfTodayRequest;
import com.winsystems.smartfloor.service.message.request.DATA_MessageToRequest;
import com.winsystems.smartfloor.service.message.response.DATA_GetConversationHistoryResponse;
import com.winsystems.smartfloor.service.message.response.DATA_GetConversationsOfTodayResponse;
import com.winsystems.smartfloor.service.message.response.DATA_SendMessageResponse;
import com.winsystems.smartfloor.view.utils.Constants;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.net.ConnectException;
import java.net.SocketTimeoutException;

public class DefaultMessageService implements MessageService{

  private static DefaultMessageService instance = null;
  private static Context appContext;
  private static AndroidLogger mLog;
  public static MessageService getInstance(Context context) {
    mLog = LogHelper.getLogger(context);
    if(instance == null){
      instance = new DefaultMessageService();
      appContext = context;
    }
    return instance;
  }

  @Override
  public DATA_GetConversationHistoryResponse getConversationHistory(String session, long managerId, String url) {

    final String NAMESPACE = "http://winsystemsintl.com/";
    final String METHOD_NAME = "GetConversationHistory";
    final String SOAP_ACTION = "http://winsystemsintl.com/IDATAService/GetConversationHistory";

    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);

    DATA_GetConversationHistoryRequest getConversationHistoryRequest = new DATA_GetConversationHistoryRequest();
    getConversationHistoryRequest.setSession(session);
    getConversationHistoryRequest.setManagerId(managerId);


    DATA_GetConversationHistoryResponse getConversationHistoryResponse = new DATA_GetConversationHistoryResponse();

    PropertyInfo pi = new PropertyInfo();
    pi.setName("getConversationHistory");
    pi.setValue(getConversationHistoryRequest);
    pi.setType(getConversationHistoryRequest.getClass());

    request.addProperty(pi);

    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
    envelope.dotNet = true;

    envelope.setOutputSoapObject(request);

    envelope.addMapping(NAMESPACE, "DATA_GetConversationHistoryRequest", getConversationHistoryRequest.getClass());
    envelope.addMapping(NAMESPACE, "DATA_GetConversationHistoryResponse", getConversationHistoryResponse.getClass());

    HttpTransportSE transporte = new HttpTransportSE(url, 60000);

    try {
      TrustManagerManipulator.allowAllSSL();
      transporte.call(SOAP_ACTION, envelope);

      SoapObject resSoap = (SoapObject) envelope.getResponse();

      getConversationHistoryResponse.setResponseCode(Integer.parseInt(resSoap.getProperty(0).toString()));
      getConversationHistoryResponse.setResponseCodeDescription(resSoap.getProperty(1).toString());
      getConversationHistoryResponse.setSessionId(resSoap.getProperty(2).toString());
      if(getConversationHistoryResponse.getResponseCode() == ResponseCode.DATA_RC_OK)
        getConversationHistoryResponse.setMessages(ServerEntitiesBuilder.BuildMessageListFromServerResponse((SoapObject) resSoap.getProperty(3)));
    }catch (ConnectException e)
    {
      mLog.error("", e);
      getConversationHistoryResponse.setResponseCode(9);
      getConversationHistoryResponse.setResponseCodeDescription(appContext.getString(R.string.error_connection));
    }
    catch (SocketTimeoutException e)
    {
      mLog.error("", e);
      getConversationHistoryResponse.setResponseCode(9);
      getConversationHistoryResponse.setResponseCodeDescription(appContext.getString(R.string.error_timeout));

      return getConversationHistoryResponse;
    }
    catch (Exception e)
    {
      mLog.error("", e);
      getConversationHistoryResponse.setResponseCode(9);
      getConversationHistoryResponse.setResponseCodeDescription(appContext.getString(R.string.error_unknown));

      return getConversationHistoryResponse;
    }

    return getConversationHistoryResponse;
  }

  @Override
  public DATA_GetConversationsOfTodayResponse getConversationsOfToday(String session, String url) {
    final String NAMESPACE = "http://winsystemsintl.com/";
    final String METHOD_NAME = "GetConversationsOfToday";
    final String SOAP_ACTION = "http://winsystemsintl.com/IDATAService/GetConversationsOfToday";

    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
    DATA_GetConversationsOfTodayRequest getConversationOfTodayRequest = new DATA_GetConversationsOfTodayRequest();
    getConversationOfTodayRequest.setSession(session);

    DATA_GetConversationsOfTodayResponse getConversationOfTodayResponse = new DATA_GetConversationsOfTodayResponse();

    PropertyInfo pi = new PropertyInfo();
    pi.setName("getConversationOfToday");
    pi.setValue(getConversationOfTodayRequest);
    pi.setType(getConversationOfTodayRequest.getClass());

    request.addProperty(pi);

    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
    envelope.dotNet = true;

    envelope.setOutputSoapObject(request);

    envelope.addMapping(NAMESPACE, "DATA_GetConversationsOfTodayRequest", getConversationOfTodayRequest.getClass());
    envelope.addMapping(NAMESPACE, "DATA_GetConversationsOfTodayResponse", getConversationOfTodayResponse.getClass());

    HttpTransportSE transporte = new HttpTransportSE(url, 60000);

    try {
      TrustManagerManipulator.allowAllSSL();
      transporte.call(SOAP_ACTION, envelope);

      SoapObject resSoap = (SoapObject) envelope.getResponse();

      getConversationOfTodayResponse.setResponseCode(Integer.parseInt(resSoap.getProperty(0).toString()));
      getConversationOfTodayResponse.setResponseCodeDescription(resSoap.getProperty(1).toString());
      getConversationOfTodayResponse.setSessionId(resSoap.getProperty(2).toString());
      if(getConversationOfTodayResponse.getResponseCode() == ResponseCode.DATA_RC_OK) {
        getConversationOfTodayResponse.setHeaders(ServerEntitiesBuilder.BuildHeadersListFromServerResponse((SoapObject) resSoap.getProperty(3)));
        getConversationOfTodayResponse.setManagers(ServerEntitiesBuilder.BuildManagersListFromServerResponse((SoapObject) resSoap.getProperty(4)));

        SaveInformationHelper<LayoutSiteManagerRunnerConversationItem> saveHeaders = new SaveInformationHelper<>(appContext);
        SaveInformationHelper<LayoutSiteManagerAvailable> saveManagers = new SaveInformationHelper<>(appContext);

        saveHeaders.writeList(getConversationOfTodayResponse.getHeaders(), Constants.TODAY_MESSAGES);
        saveManagers.writeList(getConversationOfTodayResponse.getManagers(), Constants.MANAGERS_AVAILABLES);
      }
    }catch (ConnectException e)
    {
      mLog.error("", e);
      getConversationOfTodayResponse.setResponseCode(9);
      getConversationOfTodayResponse.setResponseCodeDescription(appContext.getString(R.string.error_connection));
    }
    catch (SocketTimeoutException e)
    {
      mLog.error("", e);
      getConversationOfTodayResponse.setResponseCode(9);
      getConversationOfTodayResponse.setResponseCodeDescription(appContext.getString(R.string.error_timeout));

      return getConversationOfTodayResponse;
    }
    catch (Exception e)
    {
      mLog.error("", e);
      getConversationOfTodayResponse.setResponseCode(9);
      getConversationOfTodayResponse.setResponseCodeDescription(appContext.getString(R.string.error_unknown));

      return getConversationOfTodayResponse;
    }

    return getConversationOfTodayResponse;
  }

  @Override
  public DATA_SendMessageResponse sendMessageTo(String session, long managerId, String message, String url) {
    final String NAMESPACE = "http://winsystemsintl.com/";
    final String METHOD_NAME = "SendMessageTo";
    final String SOAP_ACTION = "http://winsystemsintl.com/IDATAService/SendMessageTo";

    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
    DATA_MessageToRequest messageToRequest = new DATA_MessageToRequest();
    messageToRequest.setSession(session);
    messageToRequest.setManagerId(managerId);
    messageToRequest.setMessage(message);

    DATA_SendMessageResponse response = new DATA_SendMessageResponse();

    PropertyInfo pi = new PropertyInfo();
    pi.setName("message");
    pi.setValue(messageToRequest);
    pi.setType(messageToRequest.getClass());

    request.addProperty(pi);

    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
    envelope.dotNet = true;

    envelope.setOutputSoapObject(request);

    envelope.addMapping(NAMESPACE, "DATA_MessageToRequest", messageToRequest.getClass());
    envelope.addMapping(NAMESPACE, "DATA_SendMessageResponse", response.getClass());

    HttpTransportSE transporte = new HttpTransportSE(url, 60000);

    try {
      TrustManagerManipulator.allowAllSSL();
      transporte.call(SOAP_ACTION, envelope);

      SoapObject resSoap = (SoapObject) envelope.getResponse();

      response.setResponseCode(Integer.parseInt(resSoap.getProperty(0).toString()));
      response.setResponseCodeDescription(resSoap.getProperty(1).toString());
      response.setSessionId(resSoap.getProperty(2).toString());
      if(response.getResponseCode() == ResponseCode.DATA_RC_OK)
        response.setMessage(ServerEntitiesBuilder.BuildMessageFromServerResponse((SoapObject) resSoap.getProperty(3)));

    }
    catch (ConnectException e)
    {
      mLog.error("", e);
      response.setResponseCode(9);
      response.setResponseCodeDescription(appContext.getString(R.string.error_connection));
    }
    catch (SocketTimeoutException e)
    {
      mLog.error("", e);
      response.setResponseCode(9);
      response.setResponseCodeDescription(appContext.getString(R.string.error_timeout));

      return response;
    }
    catch (Exception e)
    {
      mLog.error("", e);
      response.setResponseCode(9);
      response.setResponseCodeDescription(appContext.getString(R.string.error_unknown));

      return response;
    }

    return response;
  }
}
