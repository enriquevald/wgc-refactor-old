package com.wynsistems.alarmas.view.terminal;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.wynsistems.alarmas.R;
import com.wynsistems.alarmas.helpers.SaveInformationHelper;
import com.wynsistems.alarmas.model.LayoutSiteAlarm;
import com.wynsistems.alarmas.service.ResponseCode;
import com.wynsistems.alarmas.service.ServiceFactory;
import com.wynsistems.alarmas.service.alarm.response.DATA_TerminalInfoResponse;
import com.wynsistems.alarmas.view.camera.CaptureActivityAnyOrientation;
import com.wynsistems.alarmas.view.utils.Constants;
import com.wynsistems.alarmas.view.utils.KioskModeActivity;
import com.wynsistems.alarmas.view.utils.ToastFactory;

import java.util.List;

public class TerminalInfoActivity extends KioskModeActivity {
    private SharedPreferences userSettings;
    private SharedPreferences environmentSettings;
    private String userId;
    private String url;
    private String qrContent;
    private ProgressDialog dialog;
    private TextView textView;
    public TerminalInfoActivity() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.terminal_status_terminal_selection);
        setTitle(R.string.title_activity_terminal_status);

        userSettings = this.getSharedPreferences(Constants.USER_ID_PREFERENCE_EDITOR, 0) ;
        environmentSettings = PreferenceManager.getDefaultSharedPreferences(this);

        userId = userSettings.getString(Constants.USER_ID, userId);
        url = environmentSettings.getString(getString(R.string.server_url), url);

        buildDialog();

        ImageButton terminalQRCodeButton = (ImageButton) this.findViewById(R.id.terminal_qr_code_button);
        Button sendTerminalIdButton = (Button) this.findViewById(R.id.select_terminal_id);
        textView = (TextView)this.findViewById(R.id.new_alarm_terminal_provider_field);
        sendTerminalIdButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!textView.getText().toString().isEmpty()) {
                    new GetTerminalStatusInformation().execute();
                }
                else{
                    ToastFactory.buildSimpleToast(getResources().getString(R.string.error_field_required_new_alarm), TerminalInfoActivity.this);
                }
            }
        });
        terminalQRCodeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentIntegrator scanIntegrator = new IntentIntegrator(TerminalInfoActivity.this);
                scanIntegrator.setCaptureActivity(CaptureActivityAnyOrientation.class);
                scanIntegrator.setOrientationLocked(false);
                scanIntegrator.initiateScan();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {


        IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
            if (scanningResult != null && scanningResult.getContents() != "") {
                textView.setText(scanningResult.getContents());
            }else {
                Toast toast = Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_data_received), Toast.LENGTH_SHORT);
                toast.show();
            }
    }

    private void buildDialog() {
        this.dialog = new ProgressDialog(TerminalInfoActivity.this);
        this.dialog.setTitle(TerminalInfoActivity.this.getResources().getString(R.string.title_progress_dialog));
        this.dialog.setMessage(TerminalInfoActivity.this.getResources().getString(R.string.alarm_message_progress_dialog));

    }

    private class GetTerminalStatusInformation extends AsyncTask<Void,Void, Boolean> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            try {
                final DATA_TerminalInfoResponse response = ServiceFactory.getInstance().getLayoutSiteAlarmsService(getApplicationContext()).getTerminalStatusByTerminalID(url, userId, textView.getText().toString());
                if (response.getResponseCode() != ResponseCode.DATA_RC_OK)
                {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast toast = Toast.makeText(getApplicationContext(), response.getResponseCodeDescription(), Toast.LENGTH_SHORT);
                            toast.show();
                        }
                    });
                    return false;
                }
                Log.d("TerminalStatus", "DoInBackground Success");

                return true;
            } catch (Exception e){
                Log.e("TerminalStatus", "DoInBackground Exception: " + e);
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            if(result) {
                Intent intent = new Intent(TerminalInfoActivity.this, TerminalInfoMenuActivity.class);

                SaveInformationHelper<LayoutSiteAlarm> saveInformationHelper = new SaveInformationHelper<>(getApplicationContext());
                List<LayoutSiteAlarm> list = saveInformationHelper.readList(userId + "_layoutSiteAlarms");

                if(list.size() > 0)
                    intent.putExtra(Constants.TERMINAL_NAME, list.get(0).getTerminal().getProvider());

                intent.putExtra(Constants.TERMINAL_ID, textView.getText().toString());
                startActivity(intent);
            }
        }
    }
}
