package com.winsystems.smartfloor.view.setting;

import android.content.SharedPreferences;
import android.os.Bundle;

import com.winsystems.smartfloor.R;
import com.winsystems.smartfloor.view.utils.KioskModeActivity;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: SettingsActivity
//
// DESCRIPTION: Implements the functionality for preferences of app.
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public class SettingsActivity extends KioskModeActivity {

  private String lang;
  private SharedPreferences prefs;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setTitle(R.string.title_activity_settigns);
    getFragmentManager().beginTransaction()
        .replace(android.R.id.content, new SettingsFragment())
        .commit();
  }
}
