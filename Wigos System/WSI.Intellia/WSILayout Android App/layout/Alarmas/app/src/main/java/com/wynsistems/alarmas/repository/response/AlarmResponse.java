package com.wynsistems.alarmas.repository.response;

import com.wynsistems.alarmas.model.Alarm;

import java.util.ArrayList;
import java.util.List;

public class AlarmResponse {
    private String user;
    private List<Alarm> alarmList;

    public List<Alarm> getAlarmList() {
        if(alarmList != null)
            return alarmList;
        return new ArrayList<>();
    }

    public void setAlarmList(List<Alarm> alarmList) {
        this.alarmList = alarmList;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }


}
