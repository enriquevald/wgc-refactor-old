package com.winsystems.smartfloor.repository.factory;

import android.content.Context;

import com.winsystems.smartfloor.R;
import com.winsystems.smartfloor.model.LayoutSiteItemGroupCategory;
import com.winsystems.smartfloor.model.LayoutSiteItemSeverity;
import com.winsystems.smartfloor.model.AlarmSortingCriteria;
import com.winsystems.smartfloor.model.SummarizedAlarms;
import com.winsystems.smartfloor.service.alarm.AlarmService;
import com.winsystems.smartfloor.service.ServiceFactory;
import com.winsystems.smartfloor.service.login.response.LayoutSiteTask;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: SummarizedAlarmsFactory
//
// DESCRIPTION: Implements class for summary alarms factory, example: category, criticality .
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public class SummarizedAlarmsFactory {

  private AlarmService alarmService;
  private static SummarizedAlarmsFactory instance = null;

  private SummarizedAlarmsFactory(String alarmServerUrl, Context context) {
    alarmService = ServiceFactory.getInstance().getAlarmService(context).setUrl(alarmServerUrl);
  }

  public static SummarizedAlarmsFactory getInstance(String alarmServerUrl,Context context){
    if(instance == null){
      instance = new SummarizedAlarmsFactory(alarmServerUrl, context);
    }
    return instance;
  }

  public ArrayList<SummarizedAlarms> getSummarizedAlarmsByCriticality(AlarmSortingCriteria sortingCriteria, Context context){

    ArrayList<SummarizedAlarms> summarizedAlarmsList = new ArrayList<>();

    List<LayoutSiteTask> highCriticality = alarmService.getAlarmByCriticality(LayoutSiteItemSeverity.HIGH);
    List<LayoutSiteTask> mediumCriticality = alarmService.getAlarmByCriticality(LayoutSiteItemSeverity.MEDIUM);
    List<LayoutSiteTask> lowCriticality = alarmService.getAlarmByCriticality(LayoutSiteItemSeverity.LOW);

    if(sortingCriteria == AlarmSortingCriteria.NEWEST) {
      Collections.sort(highCriticality);
      Collections.sort(mediumCriticality);
      Collections.sort(lowCriticality);

    }else{
      Collections.reverse(highCriticality);
      Collections.reverse(mediumCriticality);
      Collections.reverse(lowCriticality);

    }
    if(highCriticality.size() > 0)
      summarizedAlarmsList.add(new SummarizedAlarms(LayoutSiteItemSeverity.HIGH,context.getResources().getString(R.string.high_level_description),highCriticality.size(), highCriticality));
    if(mediumCriticality.size() > 0)
      summarizedAlarmsList.add(new SummarizedAlarms(LayoutSiteItemSeverity.MEDIUM, context.getResources().getString(R.string.medium_level_description),mediumCriticality.size(),mediumCriticality));
    if(lowCriticality.size() > 0)
      summarizedAlarmsList.add(new SummarizedAlarms(LayoutSiteItemSeverity.LOW, context.getResources().getString(R.string.low_level_description),lowCriticality.size(),lowCriticality));

    return summarizedAlarmsList;
  }

  public ArrayList<SummarizedAlarms> getSummarizedAlarmsByCategory(AlarmSortingCriteria sortingCriteria, Context context){

    ArrayList<SummarizedAlarms> summarizedAlarmsList = new ArrayList<>();

    List<LayoutSiteTask> intrusion = alarmService.getAlarmByCategory(LayoutSiteItemGroupCategory.Intrusion);
    List<LayoutSiteTask> machineFlags = alarmService.getAlarmByCategory(LayoutSiteItemGroupCategory.MachineFlags);
    List<LayoutSiteTask> support = alarmService.getAlarmByCategory(LayoutSiteItemGroupCategory.Support);
    List<LayoutSiteTask> payCheck = alarmService.getAlarmByCategory(LayoutSiteItemGroupCategory.PayCheck);
    List<LayoutSiteTask> jamBill = alarmService.getAlarmByCategory(LayoutSiteItemGroupCategory.JamBill);
    List<LayoutSiteTask> printer = alarmService.getAlarmByCategory(LayoutSiteItemGroupCategory.Printer);

    if(sortingCriteria == AlarmSortingCriteria.NEWEST) {
      Collections.sort(intrusion);
      Collections.sort(machineFlags);
      Collections.sort(support);
      Collections.sort(payCheck);
      Collections.sort(jamBill);
      Collections.sort(printer);

    }else{
      Collections.reverse(intrusion);
      Collections.reverse(machineFlags);
      Collections.reverse(support);
      Collections.reverse(payCheck);
      Collections.reverse(jamBill);
      Collections.reverse(printer);
    }
    if(intrusion.size() > 0)
      summarizedAlarmsList.add(new SummarizedAlarms(LayoutSiteItemGroupCategory.Intrusion, LayoutSiteItemGroupCategory.getTypeDescription(LayoutSiteItemGroupCategory.Intrusion, context),intrusion.size(), intrusion));
    if(jamBill.size() > 0)
      summarizedAlarmsList.add(new SummarizedAlarms(LayoutSiteItemGroupCategory.JamBill, LayoutSiteItemGroupCategory.getTypeDescription(LayoutSiteItemGroupCategory.JamBill, context),jamBill.size(), jamBill));
    if(machineFlags.size() > 0)
      summarizedAlarmsList.add(new SummarizedAlarms(LayoutSiteItemGroupCategory.MachineFlags, LayoutSiteItemGroupCategory.getTypeDescription(LayoutSiteItemGroupCategory.MachineFlags, context),machineFlags.size(), machineFlags));
    if(payCheck.size() > 0)
      summarizedAlarmsList.add(new SummarizedAlarms(LayoutSiteItemGroupCategory.PayCheck, LayoutSiteItemGroupCategory.getTypeDescription(LayoutSiteItemGroupCategory.PayCheck, context),payCheck.size(), payCheck));
    if(support.size() > 0)
      summarizedAlarmsList.add(new SummarizedAlarms(LayoutSiteItemGroupCategory.Support, LayoutSiteItemGroupCategory.getTypeDescription(LayoutSiteItemGroupCategory.Support, context),support.size(), support));
    if(printer.size() > 0)
      summarizedAlarmsList.add(new SummarizedAlarms(LayoutSiteItemGroupCategory.Printer, LayoutSiteItemGroupCategory.getTypeDescription(LayoutSiteItemGroupCategory.Printer, context),printer.size(), printer));

    return summarizedAlarmsList;
  }

  public ArrayList<SummarizedAlarms> getSummarizedAlarmsByCompleted(AlarmSortingCriteria sortingCriteria, Context context){

    ArrayList<SummarizedAlarms> summarizedAlarmsList = new ArrayList<>();

    List<LayoutSiteTask> inProgressTasks = alarmService.getAlarmsInProgress();
    List<LayoutSiteTask> assignedTasks = alarmService.getAlarmsAssigned();

    if(sortingCriteria == AlarmSortingCriteria.NEWEST) {
      Collections.sort(inProgressTasks);
      Collections.sort(assignedTasks);

    }else{
      Collections.reverse(assignedTasks);
      Collections.reverse(assignedTasks);
    }

    if(inProgressTasks.size() > 0)
      summarizedAlarmsList.add(new SummarizedAlarms(true, context.getResources().getString(R.string.accepted),inProgressTasks.size(), inProgressTasks));
    if(assignedTasks.size() > 0)
      summarizedAlarmsList.add(new SummarizedAlarms(false,context.getResources().getString(R.string.assigned),assignedTasks.size(), assignedTasks));

    return summarizedAlarmsList;
  }
}
