package com.winsystems.smartfloor.view.logout;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;

import com.winsystems.smartfloor.R;
import com.winsystems.smartfloor.common.LayoutNotification;
import com.winsystems.smartfloor.helpers.LayoutPendingMessage;
import com.winsystems.smartfloor.helpers.SaveInformationHelper;
import com.winsystems.smartfloor.model.LayoutSiteAlarm;
import com.winsystems.smartfloor.model.LayoutSiteManagerAvailable;
import com.winsystems.smartfloor.model.LayoutSiteManagerRunnerConversationItem;
import com.winsystems.smartfloor.service.login.response.BeaconAvailable;
import com.winsystems.smartfloor.service.login.response.LayoutSiteTask;
import com.winsystems.smartfloor.service.mqtt.PahoMqttService;
import com.winsystems.smartfloor.view.login.LoginActivity;
import com.winsystems.smartfloor.view.utils.Constants;
import com.winsystems.smartfloor.view.utils.KioskModeActivity;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: LogOutActivity
//
// DESCRIPTION: Implement methods for logout to the app.
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public class LogOutActivity  extends KioskModeActivity {
  private String url;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    SharedPreferences settings = this.getSharedPreferences(Constants.USER_ID_PREFERENCE_EDITOR, 0);
    SharedPreferences.Editor edit = settings.edit();
    edit.putString(Constants.USER_ID, "");
    edit.apply();
    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);//get the preferences that are allowed to be given
    url = prefs.getString("alarm_server_address", url);

    SaveInformationHelper<LayoutSiteAlarm> saveLayoutSiteAlarms = new SaveInformationHelper<>(getApplicationContext());
    SaveInformationHelper<Long> saveRoles = new SaveInformationHelper<>(getApplicationContext());
    SaveInformationHelper<BeaconAvailable> saveBeacons = new SaveInformationHelper<>(getApplicationContext());
    SaveInformationHelper<LayoutSiteManagerAvailable> managers = new SaveInformationHelper<>(getApplicationContext());
    SaveInformationHelper<LayoutSiteManagerRunnerConversationItem> conversations = new SaveInformationHelper<>(getApplicationContext());
    SaveInformationHelper<LayoutNotification> notification = new SaveInformationHelper<>(getApplicationContext());
    SaveInformationHelper<LayoutPendingMessage> pendingMessages = new SaveInformationHelper<>(getApplicationContext());
    SaveInformationHelper<LayoutSiteTask> assignedTasks = new SaveInformationHelper<>(getApplicationContext());
    saveLayoutSiteAlarms.clearList(Constants.ALARMS_CREATED_BY_USER);
    saveRoles.clearList(Constants.ROLES_OF_USER);
    saveBeacons.clearList(Constants.BEACONS_AVAILABLES);
    managers.clearList(Constants.MANAGERS_AVAILABLES);
    conversations.clearList(Constants.TODAY_MESSAGES);
    notification.clearList(Constants.NOTIFICATION_MANAGER_NOTIFICATION_LIST);
    pendingMessages.clearList(Constants.PENDING_MESSAGES);
    assignedTasks.clearList(Constants.TASKS_ASSIGNED);
    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
    SharedPreferences.Editor editor = preferences.edit();
    editor.putString("userId", "");
    editor.putString("lastVersionApp", getApplicationContext().getString(R.string.version));
    editor.putString("userName", "");
    editor.putString("userPassword", "");
    editor.apply();

    PahoMqttService.actionStop(this);

    Intent mainIntent = new Intent(LogOutActivity.this, LoginActivity.class);
    mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
    startActivity(mainIntent);
    finish();
  }
}