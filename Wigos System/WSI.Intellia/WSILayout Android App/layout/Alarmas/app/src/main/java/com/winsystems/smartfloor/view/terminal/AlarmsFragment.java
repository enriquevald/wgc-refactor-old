package com.winsystems.smartfloor.view.terminal;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.winsystems.smartfloor.R;
import com.winsystems.smartfloor.helpers.SaveInformationHelper;
import com.winsystems.smartfloor.model.LayoutSiteAlarm;
import com.winsystems.smartfloor.view.utils.Constants;

import java.util.List;

public class AlarmsFragment extends Fragment {

  private SaveInformationHelper<LayoutSiteAlarm> saveInformationHelper;
  private String userId;

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {

    saveInformationHelper = new SaveInformationHelper<>(this.getActivity().getApplicationContext());

    SharedPreferences settings = this.getActivity().getApplicationContext().getSharedPreferences(Constants.USER_ID_PREFERENCE_EDITOR, 0) ;
    userId = settings.getString(Constants.USER_ID, userId);

    View rootView = inflater.inflate(R.layout.fragment_alarms, container, false);
    TextView noAlarmsToDisplay = (TextView) rootView.findViewById(R.id.no_alarms_to_display);
    ListView alarmListView = (ListView) rootView.findViewById(R.id.terminal_assigned_alarms);
    List<LayoutSiteAlarm> data = saveInformationHelper.readList(userId + "_layoutSiteAlarms");
    if(data.size() == 0){
      alarmListView.setVisibility(View.GONE);
      noAlarmsToDisplay.setVisibility(View.VISIBLE);
    }else{
      ListAdapter listAdapter = new LayoutSiteAlarmAdapter(this.getActivity().getApplicationContext(), data);
      alarmListView.setAdapter(listAdapter);
      alarmListView.setVisibility(View.VISIBLE);
      noAlarmsToDisplay.setVisibility(View.GONE);
    }
    return rootView;
  }

}
