package com.wynsistems.alarmas.service.photo.request;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.util.Hashtable;

import javax.xml.transform.stream.StreamResult;

public class DATA_LayoutSiteMediaAsset implements KvmSerializable {

    public String sessionId;
    public String data;
    public String description;
    public int mediaType;
    public String externalId;
    public int type;

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public void setData(String data) {
        this.data = data;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setMediaType(int mediaType) {
        this.mediaType = mediaType;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    @Override
    public Object getProperty(int i) {
        switch (i){
            case 0:
                return data;
            case 1:
                return description;
            case 2:
                return externalId;
            case 3:
                return mediaType;
            case 4:
                return sessionId;
            case 5:
                return type;
            default:
                return null;
        }

    }

    @Override
    public int getPropertyCount() {
        return 6;
    }

    @Override
    public void setProperty(int i, Object o) {
        switch (i){
            case 0:
                data = o.toString();
            case 1:
                description = o.toString();
            case 2:
                externalId= o.toString();
            case 3:
                mediaType = Integer.parseInt(o.toString());
            case 4:
                sessionId = o.toString();
            case 5:
                type = Integer.parseInt(o.toString());
        }
    }

    @Override
    public void getPropertyInfo(int i, Hashtable hashtable, PropertyInfo propertyInfo) {
        switch (i){
            case 0:
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                propertyInfo.name = "Data";
                break;
            case 1:
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                propertyInfo.name = "Description";
                break;
            case 2:
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                propertyInfo.name = "ExternalId";
                break;
            case 3:
                propertyInfo.type = PropertyInfo.INTEGER_CLASS;
                propertyInfo.name = "MediaType";
                break;
            case 4:
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                propertyInfo.name = "SessionId";
                break;
            case 5:
                propertyInfo.type = PropertyInfo.INTEGER_CLASS;
                propertyInfo.name = "Type";
                break;
        }
    }
}
