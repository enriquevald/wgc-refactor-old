package com.winsystems.smartfloor.service.message.response;


import com.winsystems.smartfloor.model.LayoutSiteManagerRunnerMessage;
import com.winsystems.smartfloor.service.ResponseCode;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.util.Hashtable;

public class DATA_SendMessageResponse implements KvmSerializable {

  private LayoutSiteManagerRunnerMessage message;
  private String sessionId;
  private Integer responseCode;
  private String responseCodeDescription;
  private ResponseCode enumResponseCode;

  public String getSessionId() {
    return sessionId;
  }

  public void setSessionId(String sessionId) {

    this.sessionId = sessionId;
  }

  public void setResponseCode(Integer responseCode) {
    this.responseCode = responseCode;
  }

  public void setResponseCodeDescription(String responseCodeDescription) {
    this.responseCodeDescription = responseCodeDescription;
  }

  public ResponseCode getResponseCode(){
    return ResponseCode.getResponseCode(responseCode);
  }

  public String getResponseCodeDescription() {
    return responseCodeDescription;
  }

  public LayoutSiteManagerRunnerMessage getMessage() {
    return message;
  }

  public void setMessage(LayoutSiteManagerRunnerMessage message) {
    this.message = message;
  }

  @Override
  public Object getProperty(int i) {
    switch (i){

      case 0:
        return responseCode;
      case 1:
        return responseCodeDescription;
      case 2:
        return sessionId;
      case 3:
        return message;
      default:
        return null;
    }
  }

  @Override
  public int getPropertyCount() {
    return 4;
  }

  @Override
  public void setProperty(int i, Object o) {
    switch(i)
    {

      case 0:
        responseCode = Integer.parseInt(o.toString());
        break;
      case 1:
        responseCodeDescription = o.toString();
        break;
      case 2:
        sessionId = o.toString();
        break;
      case 3:
        message = (LayoutSiteManagerRunnerMessage)o;
        break;
      default:
        break;
    }
  }

  @Override
  public void getPropertyInfo(int i, Hashtable hashtable, PropertyInfo propertyInfo) {
    switch(i)
    {
      case 0:
        propertyInfo.type = PropertyInfo.INTEGER_CLASS;
        propertyInfo.name = "ResponseCode";
        break;
      case 1:
        propertyInfo.type = PropertyInfo.STRING_CLASS;
        propertyInfo.name = "ResponseCodeDescription";
        break;
      case 2:
        propertyInfo.type = PropertyInfo.STRING_CLASS;
        propertyInfo.name = "SessionId";
        break;
      case 3:
        propertyInfo.type = PropertyInfo.OBJECT_TYPE;
        propertyInfo.name = "Message";
        break;
      default:break;
    }
  }
}
