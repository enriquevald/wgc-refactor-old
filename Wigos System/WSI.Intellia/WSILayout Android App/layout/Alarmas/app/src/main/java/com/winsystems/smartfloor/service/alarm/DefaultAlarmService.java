package com.winsystems.smartfloor.service.alarm;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;

import com.winsystems.smartfloor.R;
import com.winsystems.smartfloor.common.AndroidLogger;
import com.winsystems.smartfloor.common.ServerEntitiesBuilder;
import com.winsystems.smartfloor.common.TrustManagerManipulator;
import com.winsystems.smartfloor.helpers.LogHelper;
import com.winsystems.smartfloor.model.Alarm;
import com.winsystems.smartfloor.model.LayoutSiteItemGroupCategory;
import com.winsystems.smartfloor.model.LayoutSiteItemSeverity;
import com.winsystems.smartfloor.model.LayoutSiteTaskActions;
import com.winsystems.smartfloor.repository.RepositoryFactory;
import com.winsystems.smartfloor.repository.TaskRepository;
import com.winsystems.smartfloor.service.DATA_PostActionResponse;
import com.winsystems.smartfloor.service.ResponseCode;
import com.winsystems.smartfloor.service.alarm.request.DATA_CreateAlarm;
import com.winsystems.smartfloor.service.alarm.request.DATA_GetAssignedLayoutSiteTasks;
import com.winsystems.smartfloor.service.alarm.request.DATA_UpdateLayoutSiteTaskInfo;
import com.winsystems.smartfloor.service.alarm.response.DATA_LayoutSiteTaskAssignedResponse;
import com.winsystems.smartfloor.service.login.response.LayoutSiteTask;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.ByteArrayOutputStream;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.List;

public class DefaultAlarmService implements AlarmService {

  private static DefaultAlarmService instance = null;
  private static TaskRepository taskRepository;
  private static String url;
  private AndroidLogger mLog;
  private Context appContext;

  private DefaultAlarmService(Context context){
    taskRepository = RepositoryFactory.getInstance().getAlarmRepository(context);
    mLog = LogHelper.getLogger(context);
    appContext = context;
  }

  public static AlarmService getInstance(Context context) {
    if(instance == null){
      instance = new DefaultAlarmService(context);
    }
    return instance;
  }

  @Override
  public AlarmService setUrl(String url) {
    this.url = url;
    return this;
  }

  @Override
  public DATA_PostActionResponse assignAlarm(String taskId, String userId, LayoutSiteTaskActions action, String scaleReason) {
    final String NAMESPACE = "http://winsystemsintl.com/";
    final String METHOD_NAME = "UpdateLayoutSiteTaskInfo";
    final String SOAP_ACTION = "http://winsystemsintl.com/IDATAService/UpdateLayoutSiteTaskInfo";

    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);

    DATA_UpdateLayoutSiteTaskInfo updateLayoutSiteTaskInfoRequest = new DATA_UpdateLayoutSiteTaskInfo();
    updateLayoutSiteTaskInfoRequest.setSessionId(userId);
    updateLayoutSiteTaskInfoRequest.setTaskId(taskId);
    updateLayoutSiteTaskInfoRequest.setAction(LayoutSiteTaskActions.GetActionId(action));
    updateLayoutSiteTaskInfoRequest.setReason(scaleReason);

    DATA_PostActionResponse updateLayoutSiteTaskInfoResponse = new DATA_PostActionResponse();

    PropertyInfo pi = new PropertyInfo();
    pi.setName("updatedValues");
    pi.setValue(updateLayoutSiteTaskInfoRequest);
    pi.setType(updateLayoutSiteTaskInfoRequest.getClass());

    request.addProperty(pi);

    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
    envelope.dotNet = true;

    envelope.setOutputSoapObject(request);

    envelope.addMapping(NAMESPACE, "DATA_UpdateLayoutSiteTaskInfo", updateLayoutSiteTaskInfoRequest.getClass());
    envelope.addMapping(NAMESPACE, "DATA_PostActionResponse", updateLayoutSiteTaskInfoResponse.getClass());

    HttpTransportSE transporte = new HttpTransportSE(url, 60000);

    try {
      TrustManagerManipulator.allowAllSSL();
      transporte.call(SOAP_ACTION, envelope);

      SoapObject resSoap = (SoapObject) envelope.getResponse();

      updateLayoutSiteTaskInfoResponse.setResponseCode(Integer.parseInt(resSoap.getProperty(0).toString()));
      updateLayoutSiteTaskInfoResponse.setResponseCodeDescription(resSoap.getProperty(1).toString());
      updateLayoutSiteTaskInfoResponse.setSessionId(resSoap.getProperty(2).toString());

    }catch (ConnectException c){
      mLog.error("", c);
      updateLayoutSiteTaskInfoResponse.setResponseCode(9);
      updateLayoutSiteTaskInfoResponse.setResponseCodeDescription(appContext.getString(R.string.error_connection));
    }
    catch (SocketTimeoutException e)
    {
      mLog.error("", e);
      updateLayoutSiteTaskInfoResponse.setResponseCode(9);
      updateLayoutSiteTaskInfoResponse.setResponseCodeDescription(appContext.getString(R.string.error_timeout));

      return updateLayoutSiteTaskInfoResponse;
    }
    catch (Exception e)
    {
      mLog.error("", e);
      updateLayoutSiteTaskInfoResponse.setResponseCode(9);
      updateLayoutSiteTaskInfoResponse.setResponseCodeDescription(appContext.getString(R.string.error_unknown));

      return updateLayoutSiteTaskInfoResponse;
    }

    return updateLayoutSiteTaskInfoResponse;
  }

  @Override
  public DATA_PostActionResponse createAlarm(Alarm alarm, String photoToSend, String userId, String url, Boolean isQrInput) {

    final String NAMESPACE = "http://winsystemsintl.com/";
    final String METHOD_NAME = "CreateAlarm";
    final String SOAP_ACTION = "http://winsystemsintl.com/IDATAService/CreateAlarm";

    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
    DATA_CreateAlarm createAlarmRequest = new DATA_CreateAlarm();

    createAlarmRequest.setSubCategory(alarm.getSiteItemSubCategory().getValue());
    createAlarmRequest.setCategory(alarm.getSiteItemSubCategory().getCategory().getValue());
    createAlarmRequest.setDescription(alarm.getDescription());
    createAlarmRequest.setTerminalId(alarm.getTerminal().getProvider());
    createAlarmRequest.setUserId(userId);
    createAlarmRequest.setTitle(alarm.getTitle());
    createAlarmRequest.setIsQrInput(isQrInput);
    if(photoToSend != null) {
      String mediaEncoded = Base64.encodeToString(compressMedia(photoToSend), Base64.DEFAULT);
      createAlarmRequest.setImage(mediaEncoded);
    }
    DATA_PostActionResponse createAlarmResponse = new DATA_PostActionResponse();

    PropertyInfo pi = new PropertyInfo();
    pi.setName("alarm");
    pi.setValue(createAlarmRequest);
    pi.setType(createAlarmRequest.getClass());

    request.addProperty(pi);

    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
    envelope.dotNet = true;

    envelope.setOutputSoapObject(request);

    envelope.addMapping(NAMESPACE, "DATA_CreateAlarm", createAlarmRequest.getClass());
    envelope.addMapping(NAMESPACE, "DATA_PostActionResponse", createAlarmResponse.getClass());

    HttpTransportSE transporte = new HttpTransportSE(url, 60000);

    try
    {
      TrustManagerManipulator.allowAllSSL();
      transporte.call(SOAP_ACTION, envelope);

      SoapObject resSoap =(SoapObject)envelope.getResponse();

      createAlarmResponse.setResponseCode(Integer.parseInt(resSoap.getProperty(0).toString()));
      createAlarmResponse.setResponseCodeDescription(resSoap.getProperty(1).toString());
      createAlarmResponse.setSessionId(resSoap.getProperty(2).toString());
    }catch (ConnectException e){
      mLog.error("", e);
      createAlarmResponse.setResponseCode(9);
      createAlarmResponse.setResponseCodeDescription(appContext.getString(R.string.error_connection));
    }
    catch (SocketTimeoutException e)
    {
      mLog.error("", e);
      createAlarmResponse.setResponseCode(9);
      createAlarmResponse.setResponseCodeDescription(appContext.getString(R.string.error_timeout));

      return createAlarmResponse;
    }
    catch (Exception e)
    {
      mLog.error("", e);
      createAlarmResponse.setResponseCode(9);
      createAlarmResponse.setResponseCodeDescription(appContext.getString(R.string.error_unknown));

      return createAlarmResponse;
    }

    return createAlarmResponse;
  }


  @Override
  public LayoutSiteTask getById(String id) {
    return this.taskRepository.getById(id);
  }

  @Override
  public List<LayoutSiteTask> getAll(String sessionId, Context context) {

    final String NAMESPACE = "http://winsystemsintl.com/";
    final String URL = url;
    final String METHOD_NAME = "GetAssignedTask";
    final String SOAP_ACTION = "http://winsystemsintl.com/IDATAService/GetAssignedTask";

    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);

    DATA_GetAssignedLayoutSiteTasks assinedTaskRequest = new DATA_GetAssignedLayoutSiteTasks();
    assinedTaskRequest.setSessionId(sessionId);

    DATA_LayoutSiteTaskAssignedResponse assinedTaskResponse = new DATA_LayoutSiteTaskAssignedResponse();

    PropertyInfo pi = new PropertyInfo();
    pi.setName("session");
    pi.setValue(assinedTaskRequest);
    pi.setType(assinedTaskRequest.getClass());

    request.addProperty(pi);

    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
    envelope.dotNet = true;

    envelope.setOutputSoapObject(request);

    envelope.addMapping(NAMESPACE, "DATA_GetAssignedLayoutSiteTasks", assinedTaskRequest.getClass());
    envelope.addMapping(NAMESPACE, "DATA_LayoutSiteTaskAssignedResponse", assinedTaskResponse.getClass());

    HttpTransportSE transporte = new HttpTransportSE(URL, 60000);

    try {
      TrustManagerManipulator.allowAllSSL();
      transporte.call(SOAP_ACTION, envelope);

      SoapObject resSoap = (SoapObject) envelope.getResponse();

      assinedTaskResponse.setResponseCode(Integer.parseInt(resSoap.getProperty(0).toString()));
      assinedTaskResponse.setResponseCodeDescription(resSoap.getProperty(1).toString());
      assinedTaskResponse.setSessionId(resSoap.getProperty(2).toString());

      SoapObject tasks = (SoapObject) resSoap.getProperty(3);
      assinedTaskResponse.setAvailableTasks(ServerEntitiesBuilder.BuildTasksListFromServerResponse(tasks));

    }catch (ConnectException c){
      mLog.error("", c);
      assinedTaskResponse.setResponseCode(9);
      assinedTaskResponse.setResponseCodeDescription(appContext.getString(R.string.error_connection));
    }
    catch (SocketTimeoutException e)
    {
      mLog.error("", e);
      assinedTaskResponse.setResponseCode(9);
      assinedTaskResponse.setResponseCodeDescription(appContext.getString(R.string.error_timeout));
    }
    catch (Exception e)
    {
      mLog.error("", e);
      assinedTaskResponse.setResponseCode(9);
      assinedTaskResponse.setResponseCodeDescription(appContext.getString(R.string.error_unknown));
    }

    if (assinedTaskResponse.getResponseCode() == ResponseCode.DATA_RC_OK)
    {
      taskRepository.saveFromAlarmResponse(assinedTaskResponse.getAvailableTasks(), context);
      return assinedTaskResponse.getAvailableTasks();
    }else
      return null;

  }



  @Override
  public List<LayoutSiteTask> getAlarmByCriticality(LayoutSiteItemSeverity alarmCriticality) {
    return taskRepository.getAlarmByCriticality(alarmCriticality);
  }

  @Override
  public List<LayoutSiteTask> getAlarmByCategory(LayoutSiteItemGroupCategory type) {
    return taskRepository.getAlarmByCategory(type);
  }

  @Override
  public List<LayoutSiteTask> getAlarmsInProgress() {
    return taskRepository.getAlarmsInProgress();
  }

  @Override
  public List<LayoutSiteTask> getAlarmsAssigned() {
    return taskRepository.getAlarmsAssigned();
  }

  private byte[] compressMedia(String imagePath) {
    Bitmap bm = BitmapFactory.decodeFile(imagePath);
    ByteArrayOutputStream stream = new ByteArrayOutputStream();
    bm.compress(Bitmap.CompressFormat.JPEG, 75, stream);
    if(bm!=null)
    {
      bm.recycle();
      bm=null;
    }
    return stream.toByteArray();
  }
}
