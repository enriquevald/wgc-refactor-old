package com.winsystems.smartfloor.model;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: LayoutSiteTaskActions
//
// DESCRIPTION: Implements method for site task actions.
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public enum LayoutSiteTaskActions
{
  None,
  Assigned,
  Accepted,
  Resolved,
  Scaled;

  public static int GetActionId(LayoutSiteTaskActions a){
    switch (a) {
      case None:
        return 0;
      case Assigned:
        return 1;
      case Accepted:
        return 2;
      case Resolved:
        return 3;
      case Scaled:
        return 4;
      default:
        return 0;
    }
  }

  public static LayoutSiteTaskActions GetActionLayoutSiteTaskAction(int a){
    switch (a) {
      case 0:
        return None;
      case 1:
        return Assigned;
      case 2:
        return Accepted;
      case 3:
        return Resolved;
      case 4:
        return Scaled;
      default:
        return None;
    }
  }
}