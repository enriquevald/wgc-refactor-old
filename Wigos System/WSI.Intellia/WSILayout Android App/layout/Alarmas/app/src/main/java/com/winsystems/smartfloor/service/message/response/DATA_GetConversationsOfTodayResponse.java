package com.winsystems.smartfloor.service.message.response;


import com.winsystems.smartfloor.model.LayoutSiteManagerAvailable;
import com.winsystems.smartfloor.model.LayoutSiteManagerRunnerConversationItem;
import com.winsystems.smartfloor.service.ResponseCode;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: DATA_GetConversationsOfTodayResponse
//
// DESCRIPTION: Implement methods for get conversation today.
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public class DATA_GetConversationsOfTodayResponse implements KvmSerializable{

  private List<LayoutSiteManagerRunnerConversationItem> headers;
  private List<LayoutSiteManagerAvailable> managers;
  private String sessionId;
  private Integer responseCode;
  private String responseCodeDescription;

  public DATA_GetConversationsOfTodayResponse() {
    headers = new ArrayList<>();
  }

  public List<LayoutSiteManagerAvailable> getManagers() {
    return managers;
  }

  public void setManagers(List<LayoutSiteManagerAvailable> managers) {
    this.managers = managers;
  }

  public String getSessionId() {
    return sessionId;
  }

  public void setSessionId(String sessionId) {

    this.sessionId = sessionId;
  }

  public void setResponseCode(Integer responseCode) {
    this.responseCode = responseCode;
  }

  public void setResponseCodeDescription(String responseCodeDescription) {
    this.responseCodeDescription = responseCodeDescription;
  }

  public void setHeaders(List<LayoutSiteManagerRunnerConversationItem> headers) {
    this.headers = headers;
  }

  public ResponseCode getResponseCode(){
    return ResponseCode.getResponseCode(responseCode);
  }

  public String getResponseCodeDescription() {
    return responseCodeDescription;
  }

  public List<LayoutSiteManagerRunnerConversationItem> getHeaders() {
    return headers;
  }

  @Override
  public Object getProperty(int i) {
    switch (i){

      case 0:
        return responseCode;
      case 1:
        return responseCodeDescription;
      case 2:
        return sessionId;
      case 3:
        return headers;
      case 4:
        return managers;
      default:
        return null;
    }
  }

  @Override
  public int getPropertyCount() {
    return 5;
  }

  @Override
  public void setProperty(int i, Object o) {
    switch(i)
    {

      case 0:
        responseCode = Integer.parseInt(o.toString());
        break;
      case 1:
        responseCodeDescription = o.toString();
        break;
      case 2:
        sessionId = o.toString();
        break;
      case 3:
        headers = (List<LayoutSiteManagerRunnerConversationItem>)o;
        break;
      case 4:
        managers = (List<LayoutSiteManagerAvailable>)o;
        break;
      default:
        break;
    }
  }

  @Override
  public void getPropertyInfo(int i, Hashtable hashtable, PropertyInfo propertyInfo) {
    switch(i)
    {
      case 0:
        propertyInfo.type = PropertyInfo.INTEGER_CLASS;
        propertyInfo.name = "ResponseCode";
        break;
      case 1:
        propertyInfo.type = PropertyInfo.STRING_CLASS;
        propertyInfo.name = "ResponseCodeDescription";
        break;
      case 2:
        propertyInfo.type = PropertyInfo.STRING_CLASS;
        propertyInfo.name = "SessionId";
        break;
      case 3:
        propertyInfo.type = PropertyInfo.VECTOR_CLASS;
        propertyInfo.name = "Headers";
        break;
      case 4:
        propertyInfo.type = PropertyInfo.VECTOR_CLASS;
        propertyInfo.name = "Managers";
        break;
      default:break;
    }
  }
}
