package com.winsystems.smartfloor.common;

import com.winsystems.smartfloor.model.LayoutPlayerSession;
import com.winsystems.smartfloor.model.LayoutSiteAlarm;
import com.winsystems.smartfloor.model.LayoutSiteManagerAvailable;
import com.winsystems.smartfloor.model.LayoutSiteManagerRunnerConversationItem;
import com.winsystems.smartfloor.model.LayoutSiteManagerRunnerMessage;
import com.winsystems.smartfloor.service.login.response.BeaconAvailable;
import com.winsystems.smartfloor.service.login.response.LayoutSiteTask;
import com.winsystems.smartfloor.service.login.response.LayoutSiteTaskTerminal;

import org.ksoap2.serialization.SoapObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: ServerEntitiesBuilder
//
// DESCRIPTION: Server Entities Builder.
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public class ServerEntitiesBuilder {

    public static List<LayoutSiteAlarm> BuildAlarmsListFromServerResponse(SoapObject soapObject)
    {
        int taskCount = soapObject.getPropertyCount();
        List<LayoutSiteAlarm> alarms = new ArrayList<>();
        for (int i = 0; i < taskCount; i++) {
            SoapObject ic = (SoapObject) soapObject.getProperty(i);

            LayoutSiteAlarm alarm = new LayoutSiteAlarm();
            alarm.setCategory(Integer.parseInt(ic.getProperty(0).toString()));
            alarm.setDescription(ic.getProperty(1) == null ? "" : ic.getProperty(1).toString());
            alarm.setId(Integer.parseInt(ic.getProperty(2).toString()));
            alarm.setImageData(ic.getProperty(3) == null ? "" : ic.getProperty(3).toString());
            alarm.setStatusOfTask(Integer.parseInt(ic.getProperty(4).toString()));
            alarm.setSubCategory(Integer.parseInt(ic.getProperty(5).toString()));
            alarm.setTerminalId(Integer.parseInt(ic.getProperty(7).toString()));
            alarm.setTitle(ic.getProperty(8) == null ? "" : ic.getProperty(8).toString());
            SoapObject t = (SoapObject) ic.getProperty(6);
            LayoutSiteTaskTerminal terminal = new LayoutSiteTaskTerminal();
            terminal.setId(t.getProperty(2) == null ? 0 : Integer.parseInt(t.getProperty(2).toString()));
            terminal.setName(t.getProperty(4).toString());
            terminal.setProvider(t.getProperty(6) == null ? "" : t.getProperty(6).toString());
            terminal.setArea(t.getProperty(0) == null ? "" : t.getProperty(0).toString());
            terminal.setBank(t.getProperty(1) == null ? "" : t.getProperty(1).toString());
            terminal.setModel(t.getProperty(3) == null ? "" : t.getProperty(3).toString());

            alarm.setTerminal(terminal);
            alarms.add(alarm);
        }

        return alarms;
    }

    public static List<LayoutSiteTask> BuildTasksListFromServerResponse(SoapObject soapObject) throws ParseException {
        int taskCount = soapObject.getPropertyCount();
        List<LayoutSiteTask> availableTasks = new ArrayList<>();
        for (int i = 0; i < taskCount; i++) {
            SoapObject ic = (SoapObject) soapObject.getProperty(i);

            LayoutSiteTask task = new LayoutSiteTask();
            task.setAccountId(Integer.parseInt(ic.getProperty(1).toString()));
            task.setCategory(Integer.parseInt(ic.getProperty(3).toString()));
            task.setAccepted(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(ic.getProperty(0).toString()));
            task.setAssigned(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(ic.getProperty(2).toString()));
            task.setCreation(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(ic.getProperty(4).toString()));
            task.setDescription(ic.getProperty(5) == null ? "" : ic.getProperty(5).toString());
            task.setId(Integer.parseInt(ic.getProperty(7).toString()));
            task.setSeverity(Integer.parseInt(ic.getProperty(8).toString()));
            task.setStatus(Integer.parseInt(ic.getProperty(10).toString()));
            task.setSubcategory(Integer.parseInt(ic.getProperty(11).toString()));
            task.setTerminalId(Integer.parseInt(ic.getProperty(13).toString()));
            task.setTitle(ic.getProperty(14) == null ? "" : ic.getProperty(14).toString());

            SoapObject t = (SoapObject) ic.getProperty(12);
            LayoutSiteTaskTerminal terminal = new LayoutSiteTaskTerminal();
            terminal.setId(Integer.parseInt(ic.getProperty(11).toString()));
            terminal.setName(t.getProperty(4).toString());
            terminal.setProvider(t.getProperty(6) == null ? "" : t.getProperty(6).toString());
            terminal.setArea(t.getProperty(0) == null ? "" : t.getProperty(0).toString());
            terminal.setBank(t.getProperty(1) == null ? "" : t.getProperty(1).toString());
            terminal.setModel(t.getProperty(3) == null ? "" : t.getProperty(3).toString());
            SoapObject p = (SoapObject) t.getProperty(5);
            if(p != null && Boolean.parseBoolean(p.getProperty(7).toString())) {
                LayoutPlayerSession playerSession = new LayoutPlayerSession();
                playerSession.setAccountId(Integer.parseInt(p.getProperty(0).toString()));
                playerSession.setAge(Integer.parseInt(p.getProperty(1).toString()));
                playerSession.setGender(Integer.parseInt(p.getProperty(2).toString()));
                playerSession.setIsAnonymous(Boolean.parseBoolean(p.getProperty(3).toString()));
                playerSession.setIsVip(Boolean.parseBoolean(p.getProperty(4).toString()));
                playerSession.setLevel(Integer.parseInt(p.getProperty(5).toString()));
                playerSession.setName(p.getProperty(6).toString());
                playerSession.setSessionStatus(Boolean.parseBoolean(p.getProperty(7).toString()));
                terminal.setPlayerSession(playerSession);
            }
                task.setTerminal(terminal);

            availableTasks.add(task);
        }

        return availableTasks;
    }
    public static List<LayoutSiteManagerRunnerMessage> BuildMessageListFromServerResponse(SoapObject soapObject) throws ParseException {
        int taskCount = soapObject.getPropertyCount();
        List<LayoutSiteManagerRunnerMessage> messages = new ArrayList<>();
        for (int i = 0; i < taskCount; i++) {
            SoapObject ic = (SoapObject) soapObject.getProperty(i);
            LayoutSiteManagerRunnerMessage message = new LayoutSiteManagerRunnerMessage();
            message.setDate(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(ic.getProperty(0).toString()));
            message.setId(Integer.parseInt(ic.getProperty(1).toString()));
            message.setManagerId(Long.parseLong(ic.getProperty(2).toString()));
            message.setManagerUserName(ic.getProperty(3).toString());
            message.setMessage(ic.getProperty(4) != null ? ic.getProperty(4).toString() : "");
            message.setRunnerId(Long.parseLong(ic.getProperty(5).toString()));
            message.setRunnerUserName(ic.getProperty(6).toString());
            message.setSource(Integer.parseInt(ic.getProperty(7).toString()));
            message.setStatus(Integer.parseInt(ic.getProperty(8).toString()));
            messages.add(message);
        }

        return messages;
    }

    public static List<BeaconAvailable> BuildBeaconsListFromServerResponse(SoapObject soapObject) {
        int taskCount = soapObject.getPropertyCount();
        List<BeaconAvailable> beaconAvailables = new ArrayList<>();
        for (int i = 0; i < taskCount; i++) {
            SoapObject ic = (SoapObject) soapObject.getProperty(i);

            BeaconAvailable beacon = new BeaconAvailable();
            beacon.setLayoutObjectId(Integer.parseInt(ic.getProperty(0).toString()));
            beacon.setMac(ic.getProperty(1).toString());
            beacon.setPositionFloorId(Integer.parseInt(ic.getProperty(2).toString()));
            beacon.setPositionX(Double.parseDouble(ic.getProperty(3).toString()));
            beacon.setPositionY(Double.parseDouble(ic.getProperty(4).toString()));

            beaconAvailables.add(beacon);
        }

        return beaconAvailables;
    }

    public static List<LayoutSiteManagerRunnerConversationItem> BuildHeadersListFromServerResponse(SoapObject soapObject) {
        int taskCount = soapObject.getPropertyCount();
        List<LayoutSiteManagerRunnerConversationItem> headers = new ArrayList<>();
        for (int i = 0; i < taskCount; i++) {
            SoapObject ic = (SoapObject) soapObject.getProperty(i);

            LayoutSiteManagerRunnerConversationItem header = new LayoutSiteManagerRunnerConversationItem();
            header.setManagerId(Long.parseLong(ic.getProperty(0).toString()));
            try {
                header.setManagerLastMessageDate(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(ic.getProperty(2).toString()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            header.setManagerLastMessage(ic.getProperty(1).toString());
            header.setManagerUserName(ic.getProperty(3).toString());
            headers.add(header);
        }

        return headers;
    }

    public static LayoutSiteManagerRunnerMessage BuildMessageFromServerResponse(SoapObject soapObject){

        SoapObject ic = soapObject;

        LayoutSiteManagerRunnerMessage message = new LayoutSiteManagerRunnerMessage();
        try {
            message.setDate(ic.getProperty(0) != null ? new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(ic.getProperty(0).toString()) : null);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        message.setId(Integer.parseInt(ic.getProperty(1).toString()));
        message.setManagerId(Long.parseLong(ic.getProperty(2).toString()));
        message.setManagerUserName(ic.getProperty(3) != null? ic.getProperty(3).toString() : "");
        message.setMessage(ic.getProperty(4) != null ? ic.getProperty(4).toString() : "");
        message.setRunnerId(Long.parseLong(ic.getProperty(5).toString()));
        message.setRunnerUserName(ic.getProperty(6).toString());
        message.setSource(Integer.parseInt(ic.getProperty(7).toString()));

        return message;
    }

    public static List<LayoutSiteManagerAvailable> BuildManagersListFromServerResponse(SoapObject soapObject) {
        int taskCount = soapObject.getPropertyCount();
        List<LayoutSiteManagerAvailable> managers = new ArrayList<>();
        for (int i = 0; i < taskCount; i++) {
            SoapObject ic = (SoapObject) soapObject.getProperty(i);

            LayoutSiteManagerAvailable header = new LayoutSiteManagerAvailable();
            header.setId(Integer.parseInt(ic.getProperty(0).toString()));
            header.setUserName(ic.getProperty(1).toString());
            managers.add(header);
        }

        return managers;
    }
}
