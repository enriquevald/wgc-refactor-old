package com.wynsistems.alarmas.service.message;

import com.wynsistems.alarmas.service.DATA_PostActionResponse;
import com.wynsistems.alarmas.service.message.response.DATA_GetConversationHistoryResponse;
import com.wynsistems.alarmas.service.message.response.DATA_GetConversationsOfTodayResponse;
import com.wynsistems.alarmas.service.message.response.DATA_SendMessageResponse;

public interface MessageService {

    DATA_GetConversationHistoryResponse getConversationHistory(String session, long managerId, String url);

    DATA_GetConversationsOfTodayResponse getConversationsOfToday(String session, String url);

    DATA_SendMessageResponse sendMessageTo(String session, long managerId, String message, String url);
}
