package com.winsystems.smartfloor.service.update;

import com.winsystems.smartfloor.service.update.response.DATA_UpdateApplicationResponse;

public interface UpdateService {

  public DATA_UpdateApplicationResponse getLastestApplicationVersion(String url, String sessionId);
}
