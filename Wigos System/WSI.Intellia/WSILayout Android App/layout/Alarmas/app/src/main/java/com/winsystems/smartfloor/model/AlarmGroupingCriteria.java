package com.winsystems.smartfloor.model;

//------------------------------------------------------------------------------
// Copyright © 2016-2020 Win Systems International.
//------------------------------------------------------------------------------
//
// MODULE NAME: AlarmGroupingCriteria
//
// DESCRIPTION: Implements enum for grouping criteria.
//
// AUTHOR: Carlos Rodrigo
//
// CREATION DATE: 2-FEB-2016
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 2-FEB-2016 CSR     First release.
//------------------------------------------------------------------------------
public enum AlarmGroupingCriteria {
  CRITICALITY,
  CATEGORY,
  COMPLETED;

  public static AlarmGroupingCriteria getFromString(String i){
    switch (i){
      case "1":
        return CRITICALITY;
      case "2":
        return CATEGORY;
      case "3":
        return COMPLETED;
      default:
        return CRITICALITY;
    }
  }
}
