package com.wynsistems.alarmas.service.kiosk;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.PowerManager;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.wynsistems.alarmas.common.AndroidLogger;
import com.wynsistems.alarmas.helpers.LogHelper;

public class AppContext extends MultiDexApplication {

    private AppContext instance;
    private PowerManager.WakeLock wakeLock;
    private OnScreenOffReceiver onScreenOffReceiver;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        registerKioskModeScreenOffReceiver();
        startKioskService();
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread thread, Throwable e) {
                handleUncaughtException(thread, e);
            }
        });
        configureLog4j();
    }


    private void configureLog4j() {

        LogHelper.Configure(this);
    }

    private void handleUncaughtException (Thread thread, Throwable e)
    {
        AndroidLogger mLog = LogHelper.getLogger(this);
        mLog.error("UncaughtException - ", e.getCause());
        Log.e("UncaughtException",e.getMessage(), e);
    }

    private void startKioskService() {
        startService(new Intent(this, KioskService.class));
    }

    private void registerKioskModeScreenOffReceiver() {
        // register screen off receiver
        final IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_OFF);
        onScreenOffReceiver = new OnScreenOffReceiver();
        registerReceiver(onScreenOffReceiver, filter);
    }

    public PowerManager.WakeLock getWakeLock() {
        if(wakeLock == null) {
            // lazy loading: first call, create wakeLock via PowerManager.
            PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
            wakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "wakeup");
        }
        return wakeLock;
    }
}
