package com.wynsistems.alarmas.view.task;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wynsistems.alarmas.R;
import com.wynsistems.alarmas.common.LayoutNotificationManager;
import com.wynsistems.alarmas.model.LayoutSiteItemSeverity;
import com.wynsistems.alarmas.model.LayoutSiteItemSubCategory;
import com.wynsistems.alarmas.model.LayoutSiteTaskActions;
import com.wynsistems.alarmas.service.DATA_PostActionResponse;
import com.wynsistems.alarmas.service.ResponseCode;
import com.wynsistems.alarmas.service.ServiceFactory;
import com.wynsistems.alarmas.service.alarm.AlarmService;
import com.wynsistems.alarmas.service.login.response.LayoutSiteTask;
import com.wynsistems.alarmas.service.mqtt.PahoMqttService;
import com.wynsistems.alarmas.helpers.AlarmResourseHelper;
import com.wynsistems.alarmas.view.utils.Constants;
import com.wynsistems.alarmas.view.utils.KioskModeActivity;

import java.text.Format;
import java.text.SimpleDateFormat;

public class TaskDetailActivity extends KioskModeActivity {

    private LayoutSiteTask task;
    private Format formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    private String alarmServerUrl = "";
    private String idAlarm;
    private String userId;
    private LayoutSiteTaskActions action;
    private IntentFilter intentFilter = new IntentFilter(PahoMqttService.NEW_TASK_ARRIVE);

    private BroadcastReceiver tasksUpdatedBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            TaskDetailActivity.this.refreshView();
        }
    };

    private void refreshView() {
        task = ServiceFactory.getInstance().getAlarmService(getApplicationContext()).getById(TaskDetailActivity.this.task.getId().toString());
        fillLayoutUsingCurrentAlarm(TaskDetailActivity.this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_detail);
        Boolean downloadAlarms = Boolean.parseBoolean(getIntent().getStringExtra(Constants.DOWNLOAD_ALARMS));
        Integer notificationId = getIntent().getIntExtra(Constants.NOTIFICATION_ID, 0);
        SharedPreferences settings = this.getSharedPreferences(Constants.USER_ID_PREFERENCE_EDITOR, 0) ;
        this.userId = settings.getString(Constants.USER_ID, "");
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        alarmServerUrl = prefs.getString("alarm_server_address", alarmServerUrl);
        if(downloadAlarms){
            this.idAlarm = getIntent().getStringExtra(Constants.ALARM_ID);
            task = ServiceFactory.getInstance().getAlarmService(getApplicationContext()).getById(TaskDetailActivity.this.idAlarm);
            fillLayoutUsingCurrentAlarm(TaskDetailActivity.this);
        }else {
            LayoutSiteTask selectedAlarm = (LayoutSiteTask) getIntent().getSerializableExtra(Constants.SELECTED_ALARM);
            task = ServiceFactory.getInstance().getAlarmService(getApplicationContext()).getById(selectedAlarm.getId().toString());
            fillLayoutUsingCurrentAlarm(this);
        }
        if(notificationId != 0)
        {
            LayoutNotificationManager notificationManager = new LayoutNotificationManager(getApplicationContext());
            notificationManager.remove(notificationId);
        }




        registerReceiver(tasksUpdatedBroadcastReceiver, intentFilter);
    }

    @Override
    public void onResume(){
        super.onResume();
        registerReceiver(tasksUpdatedBroadcastReceiver, intentFilter);


    }
    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(tasksUpdatedBroadcastReceiver);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private void fillLayoutUsingCurrentAlarm(ActionBarActivity activity) {
        TextView description = (TextView) activity.findViewById(R.id.alarm_detail_description);
        TextView terminalId = (TextView) activity.findViewById(R.id.terminal_id);
        TextView categoryDescription = (TextView) activity.findViewById(R.id.category_description);
        TextView alarmId = (TextView) activity.findViewById(R.id.alarm_id);
        TextView alarmDate = (TextView) activity.findViewById(R.id.alarm_date);
        LinearLayout location = (LinearLayout) activity.findViewById(R.id.terminal_details);
        final Button resolveButton = (Button) activity.findViewById(R.id.resolve_button);
        final Button scaleButton = (Button) activity.findViewById(R.id.scale_button);
        final Button assignButton = (Button)activity.findViewById(R.id.assign_button);
        final TextView noActionsOnTask = (TextView) activity.findViewById(R.id.no_action_text);

        if(task.getStatus() == LayoutSiteTaskActions.GetActionId(LayoutSiteTaskActions.Accepted))
        {
            assignButton.setVisibility(View.GONE);
            resolveButton.setVisibility(View.VISIBLE);
        }else if(task.getStatus() == LayoutSiteTaskActions.GetActionId(LayoutSiteTaskActions.Resolved) ||
                 task.getStatus() == LayoutSiteTaskActions.GetActionId(LayoutSiteTaskActions.Scaled)) {
            SetNoActionsAvailableButtonsView(resolveButton, scaleButton, noActionsOnTask);
        }

        TextView textStatus = (TextView) activity.findViewById(R.id.task_status);

        if(LayoutSiteTaskActions.GetActionLayoutSiteTaskAction(task.getStatus()) == LayoutSiteTaskActions.Assigned)
        {
            textStatus.setText(R.string.Pending);
            textStatus.setBackgroundResource(R.drawable.rectangle_red);
        }else
        {
            textStatus.setText(R.string.InProgress);
            textStatus.setBackgroundResource(R.drawable.rectangle_blue);
        }

        description.setText(task.getDescription());
        terminalId.setText(task.getTerminal().getName());
        categoryDescription.setText(LayoutSiteItemSubCategory.forCode(task.getSubcategory()).getDescription(getApplicationContext()));
        alarmId.setText(task.getId().toString());
        alarmDate.setText(formatter.format(task.getCreation()));


        FrameLayout imageView = (FrameLayout) activity.findViewById(R.id.alarm_criticality_type);
        GradientDrawable bgShape = (GradientDrawable) imageView.getBackground();
        bgShape.setColor(AlarmResourseHelper.getColorByCriticality(LayoutSiteItemSeverity.getById(task.getSeverity()), getResources()));

        ImageView alarmCategoryType = (ImageView) activity.findViewById(R.id.alarm_category_image);
        alarmCategoryType.setImageResource(AlarmResourseHelper.getCategoryImageByType(LayoutSiteItemSubCategory.forCode(task.getSubcategory()).getGroupCategory()));

        location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TaskDetailActivity.this, TerminalDetailsActivity.class);
                intent.putExtra(Constants.TERMINAL_DETAILS, task.getTerminal());
                startActivity(intent);
            }
        });

        assignButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                assignButton.setVisibility(View.GONE);
                resolveButton.setVisibility(View.VISIBLE);
                action = LayoutSiteTaskActions.Accepted;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        new HttpAssignTask().execute();
                    }
                });
            }
        });

        scaleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enableDisableButtons(resolveButton, scaleButton);
                action = LayoutSiteTaskActions.Scaled;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        new HttpAssignTask().execute();
                    }
                });
                SetNoActionsAvailableButtonsView(resolveButton, scaleButton, noActionsOnTask);
            }
        });

        resolveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                action = LayoutSiteTaskActions.Resolved;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        new HttpAssignTask().execute();
                    }
                });
                SetNoActionsAvailableButtonsView(resolveButton, scaleButton, noActionsOnTask);
            }

        });
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(AlarmResourseHelper.getCriticalityDescriptionByCriticality(LayoutSiteItemSeverity.getById(task.getSeverity()), activity));
        actionBar.setBackgroundDrawable(new ColorDrawable(AlarmResourseHelper.getColorByCriticality(LayoutSiteItemSeverity.getById(task.getSeverity()), getResources())));

    }

    private void SetNoActionsAvailableButtonsView(Button resolveButton, Button scaleButton, TextView noActionsOnTask) {
        resolveButton.setVisibility(View.GONE);
        scaleButton.setVisibility(View.GONE);
        noActionsOnTask.setVisibility(View.VISIBLE);
    }

    private void enableDisableButtons(Button enabledButton, Button disabledButton) {
        enabledButton.setEnabled(true);
        enabledButton.setAlpha(1F);
        disabledButton.setEnabled(false);
        disabledButton.setAlpha(0.5F);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class HttpAssignTask extends AsyncTask<Void, Void, Boolean>{
        ProgressDialog dialogAssingTask = new ProgressDialog(TaskDetailActivity.this);
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialogAssingTask.setTitle(TaskDetailActivity.this.getResources().getString(R.string.title_progress_dialog));
            dialogAssingTask.setMessage(TaskDetailActivity.this.getResources().getString(R.string.alarm_message_progress_dialog));

            dialogAssingTask.show();
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            final DATA_PostActionResponse response =  ServiceFactory.getInstance().getAlarmService(getApplicationContext()).setUrl(alarmServerUrl).assignAlarm(task.getId().toString(), userId, action);
            AlarmService service = ServiceFactory.getInstance().getAlarmService(getApplicationContext());
            service.setUrl(alarmServerUrl);
            service.getAll(userId, getApplicationContext());
            if(response.getResponseCode() == ResponseCode.DATA_RC_OK)
                return true;
            else{
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast toast = Toast.makeText(getApplicationContext(), response.getResponseCodeDescription(), Toast.LENGTH_SHORT);
                        toast.show();

                    }
                });
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (dialogAssingTask.isShowing()) {
                dialogAssingTask.dismiss();
            }
            if(result)
            {
                Intent broadcastIntent = new Intent();
                broadcastIntent.setAction(Constants.UPDATED_TASKS);
                sendBroadcast(broadcastIntent);
                if(action == LayoutSiteTaskActions.Accepted)
                    refreshView();
                else
                {
                    TaskDetailActivity.this.finish();
                }
            }
        }
    }
}
