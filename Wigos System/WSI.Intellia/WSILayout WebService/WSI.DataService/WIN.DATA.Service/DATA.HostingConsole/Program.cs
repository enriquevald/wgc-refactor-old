﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using DATA.Service;
using DATA.BL;
using System.Collections.Specialized;
using System.Configuration;

namespace DATA.HostingConsole
{
  class Program
  {
    static void Main(string[] args)
    {
      //String _text;
      ServiceHost Host;
      Host = null;
      try
      {
        Host = new ServiceHost(typeof(DATAService));
      }
      catch (Exception Ex)
      {

        //GestorLogs.LogException(Ex.Message, Ex);
        Console.ForegroundColor = ConsoleColor.Red;
        Console.WriteLine(Ex.Message);
        Console.ForegroundColor = ConsoleColor.Gray;
        Console.WriteLine("Press <enter> to terminate the Application");
        Console.ReadKey();

      }
      //Uri baseAddress = new Uri("http://localhost:62471/DATAService.svc");
      //Host = new ServiceHost(typeof(DATAService), baseAddress);
      try
      {
        Console.Clear();
        /*
         * // Enable metadata publishing.
        ServiceMetadataBehavior smb = new ServiceMetadataBehavior();
        smb.HttpGetEnabled = true;
        smb.MetadataExporter.PolicyVersion = PolicyVersion.Policy15;
        host.Description.Behaviors.Add(smb);
         */
        Console.ForegroundColor = ConsoleColor.Green;
        Host.Open();
        
        Console.WriteLine("Host.Open()");

        Console.ForegroundColor = ConsoleColor.Gray;

        Console.WriteLine("");
        Console.WriteLine("ESTADO ACTUAL: " + Host.State.ToString());

        Console.WriteLine("TO CONNECT:");
        foreach (Uri _uri in Host.BaseAddresses)
        {
          Console.WriteLine("   " + _uri.AbsoluteUri);
        }
        Console.WriteLine("");

        // Get the AppSettings section.
        NameValueCollection appSettings =
           ConfigurationManager.AppSettings;

        StringBuilder _sb;
        _sb = new StringBuilder();

        for (int i = 0; i < appSettings.Count; i++)
        {
          _sb.AppendLine(string.Format("#{0} Key: {1} Value: {2}", i, appSettings.GetKey(i), appSettings[i]));

        }
        //Console.WriteLine(System.Configuration.ConfigurationSettings.AppSettings["InsertParcial"]);

        Console.WriteLine(_sb.ToString());

        Console.WriteLine("");
        Console.ForegroundColor = ConsoleColor.Gray;

        Console.WriteLine("Press <enter> to terminate the Application");
        Console.ReadKey();
      }
      catch (Exception Ex)
      {
        
        //GestorLogs.LogException(Ex.Message, Ex);
        Console.ForegroundColor = ConsoleColor.Red;
        Console.WriteLine(Ex.Message);
        Console.ForegroundColor = ConsoleColor.Gray;
        Console.WriteLine("Press <enter> to terminate the Application");
        Console.ReadKey(true);
        //GestorLogs.LogMessage(Ex.Message);
        GestorLogs.LogSoap(0, Ex.Message); //(Int16)LogID.WS_LOG => 0
      }
      finally
      {
        if (Host.State == CommunicationState.Opened || Host.State == CommunicationState.Opening || Host.State == CommunicationState.Faulted)
        {
          Host.Abort();
        }
      }
    }
  }
}
