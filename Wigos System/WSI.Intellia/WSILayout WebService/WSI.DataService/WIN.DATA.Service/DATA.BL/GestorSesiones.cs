﻿////------------------------------------------------------------------------------
//// Copyright © 2012 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: GestorSesiones.cs
//// 
////      DESCRIPTION: Sessions Manager Class 
//// 
////           AUTHOR: 
//// 
////    CREATION DATE:   -OCT-2012
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 11-OCT-2012        First release.
//// 14-JAN-2013  JCA   Add function to log by GeneralParams
////------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using DATA.OBJ;
using DATA.OBJ.DTO;
using DATA.BL.Flujos;
using DATA.OBJ.Transactions;
using WSI.Common;
using DATA.BL.Mensajes;
using System.Data;

namespace DATA.BL
{

  public static class GestorSesiones
  {
    #region Fields
    private static Dictionary<String, Session> m_sessions = new Dictionary<string, Session>();
    private static Object m_lock = new object();
    private static String GLB_SAVESESSION = WSI.Common.Resource.String("STR_WS_DATA_SAVESESSION");
    #endregion

    #region Timeout Timer

    //------------------------------------------------------------------------------
    // PURPOSE: Checks active Sessions and kills which that timeout has been accomplished
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    public static void Expire()
    {
      String _text;
      Int32 _count = 0;

      GestorLogs.LogSoap(((Int16)LogID.WS_LOG), "Number Sessions Before Kill: " + m_sessions.Count().ToString());

      try
      {
        lock (m_lock)
        {

          foreach (String _key in m_sessions.Keys.ToList<String>())
          {
            _count++;
            String sessionExpire = Misc.ReadGeneralParams("DATA.Service", "DATA_Param_ExpiracionConexion");
            if (!m_sessions[_key].SessionInUse && !FlujoAplicacion.IsValidTime(m_sessions[_key].LastMessageTicks, Int32.Parse(Misc.ReadGeneralParams("DATA.Service", "DATA_Param_ExpiracionConexion"))))
            {

              _text = String.Format("Session_Kill_TimeOut [{0}]-[{1}] => Now [{3}] - LastMessageTicks [{2}] = [{4}]",
                                    m_sessions[_key].Login.UserName, m_sessions[_key].Login.Login, m_sessions[_key].LastMessageTicks,
                                    Environment.TickCount & Int32.MaxValue, (Environment.TickCount & Int32.MaxValue - m_sessions[_key].LastMessageTicks).ToString());

              GestorLogs.LogSoap(((Int16)LogID.WS_LOG), _text);
              GestorLogs.LogWarning(WSI.Common.AlarmSourceCode.WebService, 0, WSI.Common.Resource.String("STR_WS_DATA_APP_NAME"), AlarmCode.WebService_Session_Kill_TimeOut, _text, m_sessions[_key].OperadorId, 0);

              m_sessions.Remove(_key);

            }
            if (_count > 100)
            {
              break;
            }
          }
        }
      }
      catch (Exception Ex)
      {
        GestorLogs.LogException("Expire() KILL SESSION", Ex);
      }

      GestorLogs.LogSoap(((Int16)LogID.WS_LOG), "Number Sessions After KILL: " + m_sessions.Count().ToString());
    }
    #endregion

    //------------------------------------------------------------------------------
    // PURPOSE: Create new Session and Add to Session Dictionary
    //
    //  PARAMS:
    //      - INPUT: 
    //          - RespuestaInicioSesion
    //          - InicioSesion
    //          - List<Establecimiento>
    //          - CatalogosDisponibles Catalogos, 
    //          - Int64 OperatorId
    //      - OUTPUT:
    // 
    // RETURNS: True
    public static Boolean Create(DTO_LogInResponse ResponseObject, InicioSesion UserInfo, List<Establecimiento> Establecimientos, IList<LayoutSiteTask> Tareas, Int64 OperatorId, Int64 LoginId, Boolean Log)
    {
      DATA_WS_NET_MSG_LOG _DATA_ws_net_msg_log;
      Session _session;
      Boolean _not_session_created = false;
      Int32 _num_retry = 0;
      String _text;

      _DATA_ws_net_msg_log = DATA_WS_NET_MSG_LOG.NO_LOG;
      try
      {
        _DATA_ws_net_msg_log = (DATA_WS_NET_MSG_LOG)Int16.Parse(Misc.ReadGeneralParams("DATA.Service", "DATA_Param_NET_MSG_Enabled"));
      }
      catch (Exception Ex)
      {
        GestorLogs.LogException("Create Session: DATA_Param_NET_MSG_Enabled", Ex);
      }

      lock (m_lock)
      {

        _session = new Session();
        _session.LastMessageTicks = Environment.TickCount & Int32.MaxValue;
        _session.LastMessageType = MessageType.DATA_InicioSesion;
        _session.Login = UserInfo;
        _session.SessionId = ResponseObject.SessionId;
        _session.Places = Establecimientos;
        _session.CurrentTransaction = null;
        _session.OperadorId = OperatorId;
        _session.LoginId = LoginId;
        _session.LayoutTask = Tareas;
        _session.DATA_WS_NET_MSG_LOG = _DATA_ws_net_msg_log;
        _session.EnableLogs = Log;

        while (!_not_session_created)
        {
          _not_session_created = AddSession(_session);
          _num_retry++;
          if (_num_retry >= 10)
          {
            _text = String.Format("Exceded number of Retrys!!! [{0}],[{1}],[{2}],[{3}],[{4}]"
                                                    , _num_retry.ToString(), _session.Login.UserName, _session.Login.Login
                                                    , _session.SessionId, _session.LastMessageTicks);
            GestorLogs.LogSoap(((Int16)LogID.WS_LOG), _text);

            throw new DataServiceException(ResponseCode.DATA_RC_INTERNAL_DATA_ERROR,WSI.Common.Resource.String("STR_WS_DATA_INTERNAL_ERROR_INICIO_SESSION"));
          }
        }


        _text = WSI.Common.Resource.String("STR_WS_DATA_CREATE_SESSION", _session.Login.UserName, _session.Login.Login, _session.SessionId, _session.LastMessageTicks);

        GestorLogs.LogInfo(AlarmSourceCode.WebService, 0, WSI.Common.Resource.String("STR_WS_DATA_APP_NAME"), AlarmCode.WebService_Session_Begin, _text, _session.OperadorId);
      }
      return true;
    }//Create

    private static Boolean AddSession(Session _session)
    {
      try
      {
        m_sessions.Add(_session.SessionId, _session);
        return true;
      }
      catch (Exception _ex)
      {
        GestorLogs.LogSoap(((Int16)LogID.WS_LOG), "Add Session Error but Retry!!!: " + _ex.Message);
      }
      return false;
    }//AddSession

    //------------------------------------------------------------------------------
    // PURPOSE: Get Session
    //
    //  PARAMS:
    //      - INPUT: 
    //          - SessionId
    //
    //      - OUTPUT:
    //
    // RETURNS: session if correct, throw in error.
    public static Session GetSessionForRegistrosLinea(String SessionId)
    {
      Session _session;

      try
      {
        _session = m_sessions[SessionId];
        return _session;
      }
      catch (DataServiceException DataServiceEx)
      {
        throw DataServiceEx;
      }
      catch (Exception Ex)
      {
        GestorLogs.LogException("GetSession", Ex);
        throw new DataServiceException(ResponseCode.DATA_RC_BAD_MESSAGE_DATA, WSI.Common.Resource.String("STR_WS_DATA_ERROR_GET_SESSION"));
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get Session and Validate if correct Transaction, Sequence and MessageType
    //
    //  PARAMS:
    //      - INPUT: 
    //          - SessionId
    //          - MessageTypeRequest
    //
    //      - OUTPUT:
    //
    // RETURNS: session if correct, throw in error.
    public static Session GetSession(String SessionId, MessageType MessageTypeRequest)
    {
      try
      {
        String _ip_port = Utils.GetClientIpPort();
        Session _session;

        lock (m_lock)
        {
          _session = m_sessions[SessionId];
          ValidateGenericSession(MessageTypeRequest, _session);

          if (_session.SessionInUse)
          {
            GestorErrores.GenerateDataServiceException(String.Format(SessionId, "GetSession SessionInUse [{0}]"),
                                               AlarmCode.ReceivedData_BadMessageData, ResponseCode.DATA_RC_BAD_LOGIN, WSI.Common.Resource.String("STR_WS_DATA_ERROR_SESSIONINUSE"), _session);
          }
          else
          {
            _session.SessionInUse = true;
          }
        }
        return _session;
      }
      catch (DataServiceException DataServiceEx)
      {
        throw DataServiceEx;
      }
      catch (Exception Ex)
      {
        GestorLogs.LogException(WSI.Common.Resource.String("STR_WS_DATA_ERROR_GET_SESSION"), Ex);
        throw new DataServiceException(ResponseCode.DATA_RC_BAD_MESSAGE_DATA, WSI.Common.Resource.String("STR_WS_DATA_ERROR_GET_SESSION"));
      }
    }



    #region SaveMethods.
    //------------------------------------------------------------------------------
    // PURPOSE: Puts in Session the last MessageType and set the RetriesRemaining = m_max_retries and CurrentTransaction = null
    //
    //  PARAMS:
    //      - INPUT: 
    //          - SessionId
    //          - MessageType
    //
    //      - OUTPUT:
    //
    // RETURNS: True if correct, throw in error.
    public static Boolean SaveSession(String SessionId, MessageType MessageType)
    {
      try
      {
        Session _session;
        _session = m_sessions[SessionId];
        _session.LastMessageTicks = Environment.TickCount & Int32.MaxValue;

        _session.LastMessageType = MessageType;
        //_session.RetriesRemaining = Int32.Parse(Misc.ReadGeneralParams("DATA.Service", "DATA_Param_MaxNumIntentosReenvio"));

        _session.CurrentTransaction = null;

        return true;
      }
      catch (Exception Ex)
      {
        GestorLogs.LogException(GLB_SAVESESSION, Ex);
        throw new DataServiceException(ResponseCode.DATA_RC_INTERNAL_DATA_ERROR, GLB_SAVESESSION);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Puts Values in Session and create a new CurrentTransaction
    //
    //  PARAMS:
    //      - INPUT: 
    //          - SessionId
    //          - MessageType
    //          - SequenceId
    //          - TransactionId
    //          - RetriesRemaining
    //
    //      - OUTPUT:
    //
    // RETURNS: True if correct, throw in error.
    public static Boolean SaveGenericSession(String SessionId, MessageType MessageType, Int64 SequenceId, String TransactionId, Int32 RetriesRemaining)
    {
      try
      {
        Session _session;
        _session = m_sessions[SessionId];
        _session.LastMessageTicks = Environment.TickCount & Int32.MaxValue;
        _session.LastMessageType = MessageType;
        _session.RetriesRemaining = RetriesRemaining;

        if (_session.RetriesRemaining == 0)
        {
          GestorErrores.GenerateRetriesExpiredException(_session);
        }
        return true;
      }
      catch (DataServiceException DataServiceEx)
      {
        throw DataServiceEx;
      }
      catch (Exception Ex)
      {
        GestorLogs.LogException(GLB_SAVESESSION, Ex);
        throw new DataServiceException(ResponseCode.DATA_RC_INTERNAL_DATA_ERROR, GLB_SAVESESSION);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Down the RetriesRemaining
    //
    //  PARAMS:
    //      - INPUT: 
    //          - SessionId
    //
    //      - OUTPUT:
    //
    // RETURNS: True if correct, throw in error.
    public static Boolean DownRetriesRemaining(String SessionId)
    {
      Session _session;
      _session = m_sessions[SessionId];
      _session.LastMessageTicks = Environment.TickCount & Int32.MaxValue;
      _session.RetriesRemaining--;

      if (_session.RetriesRemaining == 0)
      {
        return false;
      }
      return true;
    }


    #endregion

    //------------------------------------------------------------------------------
    // PURPOSE: Set Session to default values
    //
    //  PARAMS:
    //      - INPUT: 
    //          - SessionId
    //
    //      - OUTPUT:
    //
    // RETURNS: True if correct, throw in error.
    public static Boolean SetDefaultSession(String SessionId, String TextToLog)
    {
      try
      {
        Session _session;

        _session = m_sessions[SessionId];
        _session.LastMessageTicks = Environment.TickCount & Int32.MaxValue;
        _session.RetriesRemaining = Int32.Parse(Misc.ReadGeneralParams("DATA.Service", "DATA_Param_MaxNumIntentosReenvio"));
        _session.LastMessageTicks = Environment.TickCount & Int32.MaxValue;
        _session.LastMessageType = MessageType.DATA_InicioSesion;

        //SI ES DATA_RC_INTERNAL_DATA_ERROR YA LO HA REPORTADO ANTES UN "CATCH" o THROW
        //SI VIENE A BLANCO ES MISMO CASO ARRIBA
        if (ResponseCode.DATA_RC_INTERNAL_DATA_ERROR.ToString() != TextToLog && !String.IsNullOrEmpty(TextToLog))
        {
          GestorLogs.LogSoap(((Int16)LogID.WS_LOG), TextToLog);

          GestorLogs.LogAlarm(WSI.Common.AlarmSourceCode.WebService,
                              0,
                              WSI.Common.Resource.String("STR_WS_DATA_APP_NAME"),
                              WSI.Common.AlarmCode.WebService_Session_Default,
                              TextToLog, _session.OperadorId, 0, 0);
        }

        return true;
      }
      catch (Exception Ex)
      {
        GestorLogs.LogException("SetDefaultSession", Ex);
        throw new DataServiceException(ResponseCode.DATA_RC_INTERNAL_DATA_ERROR, "SetDefaultSession");
      }
    }


    //------------------------------------------------------------------------------
    // PURPOSE: Validate if IsValidSecuence and IsValidTransaction
    //
    //  PARAMS:
    //      - INPUT: 
    //          - SessionId
    //          - TransactionId
    //          - SecuenceId
    //
    //      - OUTPUT:
    //
    // RETURNS: True if correct throw if not correct
    public static Boolean ValidateTransactionalSesion(String SessionId, String TransactionId, Int64 SecuenceId)
    {
      Session _session;
      String _text;

      _session = m_sessions[SessionId];
      _session.LastMessageTicks = Environment.TickCount & Int32.MaxValue;

      if (!FlujoAplicacion.IsValidSecuence(_session, SecuenceId))
      {
        GestorErrores.GenerateBadSequenceException(_session, WSI.Common.Resource.String("STR_WS_DATA_ERROR_INCORRECT_IDSECUENCE"));
      }
      if (!FlujoAplicacion.IsValidTransaction(_session, TransactionId))
      {
        //GestorErrores.GenerateCurrentTransactionException(_session, WSI.Common.Resource.String("STR_WS_DATA_ERROR_INCORRECT_VALIDTRANSACTION"));
        _text = WSI.Common.Resource.String("STR_WS_DATA_ERROR_INCORRECT_VALIDTRANSACTION",
                                              _session.Login.UserName,
                                              _session.Login.Login,
                                              _session.SessionId);
        GestorErrores.GenerateCurrentTransactionException(_session, _text, _text);

      }
      return true;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Set SessionInUse = false to use for another call
    //
    //  PARAMS:
    //      - INPUT: 
    //          - SessionId
    //
    //      - OUTPUT:
    //
    // RETURNS: Void
    public static void ReleaseSession(String SessionId)
    {
      if (m_sessions.ContainsKey(SessionId))
      {
        m_sessions[SessionId].SessionInUse = false;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Remove the Session of Dictionary
    //
    //  PARAMS:
    //      - INPUT: 
    //          - SessionId
    //
    //      - OUTPUT:
    //
    // RETURNS: True or False
    public static Boolean RemoveSession(String SessionId)
    {
      return m_sessions.Remove(SessionId);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Checks exist Session
    //
    //  PARAMS:
    //      - INPUT: 
    //          - SessionId
    //
    //      - OUTPUT:
    //
    // RETURNS: Boolean
    public static Boolean ExistIdSession(String SessionId)
    {
      try
      {
        foreach (KeyValuePair<String, Session> _sessio in m_sessions)
        {
          if (_sessio.Value.SessionId == SessionId)
          {
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        GestorLogs.LogException("Continue, with 'low error' in ExistIdSession: ", _ex);
      }
      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Validate if IsValidRequestMessageType, IsValidTime and IsValidTransaction are correct
    //
    //  PARAMS:
    //      - INPUT: 
    //          - SessionId
    //
    //      - OUTPUT:
    //
    // RETURNS: Void or throw if incorrect
    private static void ValidateGenericSession(MessageType MessageTypeRequest, Session Session)
    {
      String _text;
      _text = "";
      if (!FlujoAplicacion.IsValidTime(Session.LastMessageTicks, Int32.Parse(Misc.ReadGeneralParams("DATA.Service", "DATA_Param_ExpiracionConexion"))))
      {
        _text = String.Format("Session_Kill_TimeOut [{0}]-[{1}] => Now [{3}] - LastMessageTicks [{2}] = [{4}]",
                                   Session.Login.UserName, Session.Login.Login, Session.LastMessageTicks,
                                   Environment.TickCount & Int32.MaxValue, (Environment.TickCount & Int32.MaxValue - Session.LastMessageTicks).ToString());

        RemoveSession(Session.SessionId);

        GestorErrores.GenerateDataServiceWarning(_text,
          AlarmCode.WebService_Session_Remove, ResponseCode.DATA_RC_SESSION_TIMEOUT, "Sesion timeout", Session);
      }

      Session.LastMessageTicks = Environment.TickCount & Int32.MaxValue;

      if (!FlujoAplicacion.IsValidRequestMessageType(Session.LastMessageType, MessageTypeRequest))
      {
        if (!DownRetriesRemaining(Session.SessionId))
        {
          GestorErrores.GenerateRetriesExpiredException(Session);
        }
        else
        {
          GestorErrores.GenerateDataServiceException(WSI.Common.Resource.String("STR_WS_DATA_ERROR_INCORRECT_PROCESS", ". Actual[" + Session.LastMessageType.ToString() + "] Enviado [" + MessageTypeRequest + "]"), AlarmCode.WebService_Session_Remove, ResponseCode.DATA_RC_BAD_MESSAGE_TYPE,
          WSI.Common.Resource.String("STR_WS_DATA_ERROR_INCORRECT_PROCESS", ". Actual[" + Session.LastMessageType.ToString() + "]"), Session);
        }
      }
    }



    public static void InsertSoapRead(String SessionId, String SoapReceived)
    {
      if (ExistIdSession(SessionId))
      {
        m_sessions[SessionId].SoapReceived = SoapReceived;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Validate if LOG the Login or Line by GeneralParams
    //
    //  PARAMS:
    //      - INPUT: 
    //          - SessionId
    //          - MessageType
    //
    //      - OUTPUT:
    //
    // RETURNS: True or False
    public static bool LogMessage(Session Session, MessageType MessageType)
    {
      Boolean _tolog;

      _tolog = false;

      if (Session.DATA_WS_NET_MSG_LOG == DATA_WS_NET_MSG_LOG.NO_LOG)
      {
        _tolog = false;
      }
      else
      {
        if (Session.DATA_WS_NET_MSG_LOG == DATA_WS_NET_MSG_LOG.ALL)
        {
          _tolog = true;
        }
        else if (Session.DATA_WS_NET_MSG_LOG == DATA_WS_NET_MSG_LOG.BY_LOGIN && Session.EnableLogs)
        {
          _tolog = true;
        }
        else if (MessageType != MessageType.DATA_InicioSesion && Session.DATA_WS_NET_MSG_LOG == DATA_WS_NET_MSG_LOG.BY_LINE)
        {
          if (Session.CurrentTransaction != null)
          {
       

          }
          else if (Session.EnableLogCommitTrx && (MessageType == MessageType.DATA_ReporteFase4_TransaccionCompletada || MessageType == MessageType.DATA_MonitorizacionFase3_TransaccionCompletada))
          {
            //Seteamos que EnableLogs = false para que en la próxima acción lo vuelva a chekear con los datos introducidos por el user
            _tolog = true;
            Session.EnableLogCommitTrx = false;
          }

        }
      }
      return _tolog;
    }


    //------------------------------------------------------------------------------
    // PURPOSE: Validate if LOG the current Line by GeneralParams
    //
    //  PARAMS:
    //      - INPUT: 
    //          - TrxMonitorizacion
    //          - Establecimiento
    //          - MessageType
    //
    //      - OUTPUT:
    //
    // RETURNS: True or False
    private static bool LogMessage( Establecimiento Establecimiento, MessageType MessageType)
    {
      Boolean _tolog;
      Linea _line;
      String _reportedline;

      _tolog = false;
      _reportedline = "L007";

      _line = Establecimiento.Lineas.FirstOrDefault(x => x.Nombre == _reportedline);
      //Solo con que exista una linea de las pasadas que se deba loguear, se ha de loguear
      if (_line.EnableLog)
      {
        _tolog = true;
      }
      return _tolog;
    }

  }
}
