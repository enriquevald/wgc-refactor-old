﻿////------------------------------------------------------------------------------
//// Copyright © 2012 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: FlujoAplicacion.cs
//// 
////      DESCRIPTION: Partial Class that validates the requirements of a  sessions timeout
//// 
////           AUTHOR: Xavier Iglesia
//// 
////    CREATION DATE: 10-OCT-2012
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 10-OCT-2012 XIT    First release.
//// 17-OCT-2012 JCM    Added Thread Session Expires
//// 29-OCT-2012 JVV    Update HasValidTimeReport function
//// 21-NOV-2012 JVV    Update HasValidExpirationPlace function
////------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using WSI.Common;
using DATA.OBJ;

namespace DATA.BL.Flujos
{
  public static partial class FlujoAplicacion
  {
    #region Constants & Fields
    private const Int32 TICKS_THREAD = 30 * 60 * 1000;
    private const Int32 ERROR_WAIT_HINT = 30 * 1000;

    private static Thread m_thread;
    private static Boolean Initialized = false;
    #endregion

    #region Public Methods

    public static Boolean StartCheckExpires()
    {
      try
      {
        if (!Initialized)
        {
          m_thread = new Thread(new ThreadStart(Expire_Thread));
          m_thread.Name = "DATA_GestorSesiones_Expires";

          m_thread.Start();
          Initialized = true;
        }

        return true;
      }
      catch (Exception Ex)
      {
        GestorLogs.LogException(Ex.Message, Ex);
        Thread.Sleep(ERROR_WAIT_HINT);
        Initialized = false;
      }
      return false;
    }

    public static void Expire_Thread()
    {
      while (true)
      {
        try
        {
          GestorSesiones.Expire();
          Thread.Sleep(TICKS_THREAD);
        }
        catch (Exception Ex)
        {
          GestorLogs.LogException(Ex.Message, Ex);
          Thread.Sleep(ERROR_WAIT_HINT);
        }
      }
    }

    public static Boolean IsValidTime(Int64 LastMessage, Int64 ConfiguredTimeout)
    {
      ConfiguredTimeout *= 1000;
      return (Environment.TickCount & Int32.MaxValue) < (LastMessage + (ConfiguredTimeout));
    }

    public static Boolean HasValidTimeReport(DateTime DatetimeOfReport, Int64 IdOperador, Int64 IdEstablecimiento)
    {
      Int32 _report_limit_day;
      DateTime _date_now;
      DateTime _before_date;
      DateTime _after_date;

      try
      {
        _date_now = DateTime.Today;
        _report_limit_day = Int32.Parse(Misc.ReadGeneralParams("DATA", "ReportLimitDay"));

        if (DatetimeOfReport.Month == _date_now.Month && DatetimeOfReport <= _date_now)
        {
          return true;
        }
        else
        {
          if (DatetimeOfReport > _date_now)
          {
            GestorLogs.LogWarning(WSI.Common.AlarmSourceCode.WebService,
                     0,
                     WSI.Common.Resource.String("STR_WS_DATA_APP_NAME"),
                     WSI.Common.AlarmCode.User_MaxLoginAttemps,
                     "Internal WARNING: ReportLimitDay DateTimeReport: " + DatetimeOfReport + "  DateNow:" + _date_now, IdOperador, IdEstablecimiento);

            return true; // return false if use reportlimitday!
          }
          else
          {
            try
            {
              _before_date = _date_now.AddMonths(-1);
              _before_date = new DateTime(_before_date.Year, _before_date.Month, 1);
              _after_date = new DateTime(_date_now.Year, _date_now.Month, _report_limit_day, 0, 0, 0);
              if (DatetimeOfReport >= _before_date && _date_now <= _after_date)
              {
                return true;
              }
            }
            catch (Exception Ex)
            {
              GestorLogs.LogSoap(((Int16)LogID.WS_LOG), Ex.Message);//GestorLogs.LogMessage(InternalDescription);
              GestorLogs.LogWarning(AlarmSourceCode.WebService, 0, WSI.Common.Resource.String("STR_WS_DATA_APP_NAME"), AlarmCode.WebService_Session_InternalError, Ex.Message, IdOperador, IdEstablecimiento);
              return true;
            }
          }
        }
        GestorLogs.LogWarning(WSI.Common.AlarmSourceCode.WebService,
                     0,
                     WSI.Common.Resource.String("STR_WS_DATA_APP_NAME"),
                     WSI.Common.AlarmCode.User_MaxLoginAttemps,
                     "Internal WARNING: ReportLimitDay DateTimeReport: " + DatetimeOfReport + "  DateNow:" + _date_now, IdOperador, IdEstablecimiento);

        return true; // return false if use reportlimitday!
      }
      catch (Exception Ex)
      {
        GestorLogs.LogException(Ex.Message, Ex, IdOperador);
        return false;
      }
    }


    public static Boolean HasValidExpirationPlace(DateTime ReportDate, DateTime PlaceInitDate, DateTime PlaceExpirationDate)
    {
      return (ReportDate >= PlaceInitDate && ReportDate < PlaceExpirationDate);
    }


    public static Boolean HasValidExpirationLine(DateTime ReportDate, Linea Line)
    {
      if (ReportDate >= Line.FinVigencia || ReportDate < Line.InicioVigencia)
      {
        return false;
      }
      return true;
    }
    #endregion
  }
}
