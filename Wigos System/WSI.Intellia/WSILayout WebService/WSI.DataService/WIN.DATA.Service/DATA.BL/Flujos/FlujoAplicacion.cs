﻿////------------------------------------------------------------------------------
//// Copyright © 2012 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: FlujoAplicacion.cs
//// 
////      DESCRIPTION: Class that starts, implements General Threads, Configurations, Initializations
//// 
////           AUTHOR: Joaquim Cid
//// 
////    CREATION DATE: 19-OCT-2012
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 19-OCT-2012 JCM    First release.
//// 25-OCT-2012 JCM    Update code.
//// 19-DIC-2012 JVV    Unique LOG generated
////------------------------------------------------------------------------------
namespace DATA.BL.Flujos
{
  using System;
  using WSI.Common;
  using System.Configuration;
  using DATA.OBJ;
  using System.Diagnostics;
  using uPLibrary.Networking.M2Mqtt;

  public partial class FlujoAplicacion
  {
    #region Fields
    public static Boolean m_flag_init_dbtrx = false;
    public static Boolean m_flag_init_log = false;
    public static Boolean m_flag_init_resources = false;
    public static Boolean m_flag_init_mqtt = false;
    public static MqttBroker broker;

    #endregion

    //------------------------------------------------------------------------------
    // PURPOSE: Init Aplication
    //
    //  PARAMS:
    //      - INPUT: 
    //
    //      - OUTPUT:
    //
    // RETURNS: Void
    public static void Init()
    {
      // Start Log Service
      AddListener();
      // Start DB_TRX
      Init_DBTRX();
      // Start MQTT Broker
      //Init_MQTT_Broker();
      if (!m_flag_init_resources)
      {
        WSI.Common.Resource.Init();
        m_flag_init_resources = true;
      }

      // Start Session Expires
      FlujoAplicacion.StartCheckExpires();
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Init DB
    //
    //  PARAMS:
    //      - INPUT: 
    //
    //      - OUTPUT:
    //
    // RETURNS: Void or throw if incorrect
    private static void Init_DBTRX()
    {
      if (!m_flag_init_dbtrx)
      {
        int _clientId = Int32.Parse(ConfigurationManager.AppSettings["DBId"]);
        String _dB_server1 = ConfigurationManager.AppSettings["DBPrincipal"];
        String _dB_server2 = ConfigurationManager.AppSettings["DBMirror"];
#if DEBUG
        String _app_name = ConfigurationManager.AppSettings["AppName"] + DateTime.Now.Ticks;
#else
        String _app_name = ConfigurationManager.AppSettings["AppName"];
#endif
        String _app_version = ConfigurationManager.AppSettings["AppVersion"];
        WASDB.Init(_clientId, _dB_server1, _dB_server2);
        if (!WASDB.Initialized)
        {
          throw new DataServiceException(ResponseCode.DATA_RC_INTERNAL_DATA_ERROR, "FlujoAplicacion");
        }
        WASDB.SetApplication(_app_name, _app_version);
        WASDB.ConnectAs("WGROOT");
        m_flag_init_dbtrx = true;
      }
    }
    //------------------------------------------------------------------------------
    // PURPOSE: Create Log Listeners on Init
    //
    //  PARAMS:
    //      - INPUT: 
    //
    //      - OUTPUT:
    //
    // RETURNS: void
    private static void AddListener()
    {
      try
      {
        if (!m_flag_init_log)
        {
          if (Convert.ToBoolean(ConfigurationManager.AppSettings["LogInDebug"]))
          {
            Log.AddListener(new SimpleLogger(LogID.WS_LOG.ToString()));
          }
          Log.AddListener(new SimpleLogger(LogID.WS_NET_FLOW.ToString()));
          Log.AddListener(new SimpleLogger(LogID.WS_NET_MSG.ToString(), true));
          m_flag_init_log = true;

        }
      }
      catch
      {
        GestorLogs.LogAlarm(WSI.Common.AlarmSourceCode.WebService,
                            0,
                            WSI.Common.Resource.String("STR_WS_DATA_APP_NAME"),
                            WSI.Common.AlarmCode.WebService_Session_InternalError,
                            WSI.Common.Resource.String("STR_WS_DATA_ERROR_CREATE_LISTENER"));
      }
    }
  }
}
