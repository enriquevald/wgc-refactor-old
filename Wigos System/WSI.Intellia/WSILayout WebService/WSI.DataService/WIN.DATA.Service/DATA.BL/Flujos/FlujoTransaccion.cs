﻿////------------------------------------------------------------------------------
//// Copyright © 2012 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: FlujoAplicacion.cs
//// 
////      DESCRIPTION: Partial Class that validates the requirements of a transaction
//// 
////           AUTHOR: Xavier Iglesia
//// 
////    CREATION DATE: 10-OCT-2012
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 10-OCT-2012 XIT    First release.
////------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DATA.OBJ;

namespace DATA.BL.Flujos
{
  public static partial class FlujoAplicacion
  {
    #region Public Methods
    //------------------------------------------------------------------------------
    // PURPOSE: Checks if the session has ValidTransaction
    //
    //  PARAMS:
    //      - INPUT:
    //      -- Session ContextoSesionIn
    //      -  String TransactionId
    //
    //      - OUTPUT:
    //
    // RETURNS: Boolean
    public static Boolean IsValidTransaction(Session ContextoSesionIn,String TransactionId)
    {
      if (ContextoSesionIn.CurrentTransaction != null && ContextoSesionIn.CurrentTransaction.IdTransaccion == TransactionId)
      {
        return  true;
      }
      else
      {
        return false;
      }

    }

    //------------------------------------------------------------------------------
    // PURPOSE: Checks if the session are currently inside a transaction
    //
    //  PARAMS:
    //      - INPUT: MessageType LastMessageType
    //
    //      - OUTPUT:
    //
    // RETURNS: Boolean
    public static Boolean InTransaction(MessageType LastMessageType)
    {
      Boolean _return;
      _return = false;
      switch (LastMessageType)
      {
        case MessageType.DATA_ReporteFase1_Contexto:
        case MessageType.DATA_ReporteFase2_ReferenciaLinea:
        case MessageType.DATA_ReporteFase3_RegistrosLinea:
          _return = true;
          break;
        case MessageType.DATA_MonitorizacionFase1_Contexto:
        case MessageType.DATA_MonitorizacionFase2A_RegistrosSesionesJuego:
        case MessageType.DATA_MonitorizacionFase2B_RegistrosEstadisticasJuego:
          _return = true;
          break;
        default:
          _return = false;
          break;
      }

      return _return;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Checks if the session has ValidSecuence
    //
    //  PARAMS:
    //      - INPUT:
    //      -  Session ContextoSesionIn
    //      -  Int64 SecuenceId
    //
    //      - OUTPUT:
    //
    // RETURNS: Boolean
    public static Boolean IsValidSecuence(Session ContextoSesionIn, Int64 SecuenceId)
    {
      return ContextoSesionIn.CurrentTransaction.IdSecuencia == SecuenceId - 1;
    }

    #endregion
  }
}
