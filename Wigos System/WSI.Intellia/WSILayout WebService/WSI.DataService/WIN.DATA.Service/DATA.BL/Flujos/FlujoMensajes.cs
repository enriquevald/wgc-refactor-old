﻿//// Copyright © 2012 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: FlujoMensajes.cs
//// 
////      DESCRIPTION: Partial class with IsValidRequestMessageType method
//// 
////           AUTHOR: Joaquin Calero
//// 
////    CREATION DATE: 10-OCT-2012
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 10-OCT-2012 JCA    First release.
////------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DATA.OBJ;

namespace DATA.BL.Flujos
{
  public static partial class FlujoAplicacion
  {
    public static Boolean IsValidRequestMessageType(MessageType LastMessageType, MessageType tiposEstados)
    {
      //Recepción por parte del DATASERVICE de invocaciones de métodos que no se espera recibir, dado el actual
      //estado o contexto del protocolo mantenido entre ambas partes.
      //Se emitirá una respuesta con código DATA_RC_BAD_MESSAGE_TYPE, lo cual precisa que debe
      //efectuarse una invocación a otro método y no a dicho método inválido para el estado actual.
      Boolean _return;
      _return = false;
      switch (tiposEstados)
      {
        case MessageType.DATA_ConsultaReportesPendientes:
        case MessageType.DATA_ConsultaUltimosRegsMonitorizacion:
          if (LastMessageType == MessageType.DATA_InicioSesion || LastMessageType == MessageType.DATA_CatalogosOperador ||
              LastMessageType == MessageType.DATA_ConsultaReportesPendientes || LastMessageType == MessageType.DATA_ConsultaUltimosRegsMonitorizacion ||
              LastMessageType == MessageType.DATA_ReporteFase4_TransaccionCompletada ||
              LastMessageType == MessageType.DATA_MonitorizacionFase3_TransaccionCompletada)
          {
            _return = true;
          }
          break;
        case MessageType.DATA_CatalogosOperador:
          if (LastMessageType == MessageType.DATA_InicioSesion || LastMessageType == MessageType.DATA_CatalogosOperador ||
            LastMessageType == MessageType.DATA_ConsultaReportesPendientes || LastMessageType == MessageType.DATA_ConsultaUltimosRegsMonitorizacion ||
            LastMessageType == MessageType.DATA_ReporteFase4_TransaccionCompletada ||
              LastMessageType == MessageType.DATA_MonitorizacionFase3_TransaccionCompletada)
          {

            _return = true;
          }
          break;
        case MessageType.DATA_TareasOperador:
            _return = true;
          break;
        case MessageType.DATA_ReporteFase1_Contexto:
          if (LastMessageType == MessageType.DATA_InicioSesion ||
            LastMessageType == MessageType.DATA_ConsultaReportesPendientes || LastMessageType == MessageType.DATA_ConsultaUltimosRegsMonitorizacion ||
            LastMessageType == MessageType.DATA_ReporteFase4_TransaccionCompletada ||
            LastMessageType == MessageType.DATA_MonitorizacionFase3_TransaccionCompletada ||
             LastMessageType == MessageType.DATA_CatalogosOperador)
          {

            _return = true;
          }
          break;
        case MessageType.DATA_ReporteFase2_ReferenciaLinea:
          if (LastMessageType == MessageType.DATA_ReporteFase1_Contexto ||
            LastMessageType == MessageType.DATA_ReporteFase3_RegistrosLinea)
          {

            _return = true;
          }
          break;
        case MessageType.DATA_ReporteFase3_RegistrosLinea:
          if (LastMessageType == MessageType.DATA_ReporteFase2_ReferenciaLinea ||
            LastMessageType == MessageType.DATA_ReporteFase3_RegistrosLinea)
          {

            _return = true;
          }
          break;
        case MessageType.DATA_ReporteFase4_TransaccionCompletada:
          if (LastMessageType == MessageType.DATA_ReporteFase3_RegistrosLinea)
          {

            _return = true;
          }
          break;
        case MessageType.DATA_MonitorizacionFase1_Contexto:
          if (LastMessageType == MessageType.DATA_InicioSesion ||
            LastMessageType == MessageType.DATA_ConsultaReportesPendientes || LastMessageType == MessageType.DATA_ConsultaUltimosRegsMonitorizacion ||
            LastMessageType == MessageType.DATA_ReporteFase4_TransaccionCompletada ||
            LastMessageType == MessageType.DATA_MonitorizacionFase3_TransaccionCompletada ||
             LastMessageType == MessageType.DATA_CatalogosOperador)
          {

            _return = true;
          }
          break;
        case MessageType.DATA_MonitorizacionFase2A_RegistrosSesionesJuego:
        case MessageType.DATA_MonitorizacionFase2B_RegistrosEstadisticasJuego:
          if (LastMessageType == MessageType.DATA_MonitorizacionFase1_Contexto ||
            LastMessageType == MessageType.DATA_MonitorizacionFase2A_RegistrosSesionesJuego ||
            LastMessageType == MessageType.DATA_MonitorizacionFase2B_RegistrosEstadisticasJuego)
          {

            _return = true;
          }
          break;
        case MessageType.DATA_MonitorizacionFase3_TransaccionCompletada:
          if (LastMessageType == MessageType.DATA_MonitorizacionFase2A_RegistrosSesionesJuego ||
            LastMessageType == MessageType.DATA_MonitorizacionFase2B_RegistrosEstadisticasJuego)
          {

            _return = true;
          }
          break;

        case MessageType.DATA_CierreSesion:
          _return = true;
          break;
        default:
          break;
      }

      return _return;
    }
  }
}
