﻿////------------------------------------------------------------------------------
//// Copyright © 2012 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: GestorErrores.cs
//// 
////      DESCRIPTION: Errors Manager Class 
//// 
////           AUTHOR: Xavier Iglesia
//// 
////    CREATION DATE: 11-OCT-2012
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 11-OCT-2012 XIT    First release.
//// 15-OCT-2012 JCM    Added Listener
//// 08-JAN-2013 JCA    LITERALS ERRORS TO RESOURCE
////------------------------------------------------------------------------------

using System;
using DATA.OBJ.DTO;
using DATA.OBJ;
using System.IO;
using DATA.BL.Flujos;
using WSI.Common;
using DATA.OBJ.DTO.Responses;

namespace DATA.BL
{
    public static class GestorErrores
    {

        public static DTO_SaveLogResponse GetSaveLogException(ResponseCode ResponseCode, String ResponseDescription)
        {
            DTO_SaveLogResponse _exception;
            _exception = new DTO_SaveLogResponse()
            {
                ResponseCode = ResponseCode,
                ResponseCodeDescription = ResponseDescription,
                SessionId = string.Empty,
            };

            return _exception;
        }
        
        public static DTO_LogInResponse GetInicioSesionException(ResponseCode ResponseCode, String ResponseDescription)
        {
            DTO_LogInResponse _exception;
            _exception = new DTO_LogInResponse()
            {
                ResponseCode = ResponseCode,
                ResponseCodeDescription = ResponseDescription,
                SessionId = string.Empty,
            };

            return _exception;
        }

        public static DTO_GetLayoutSiteAssignedTasksResponse GetLayoutSiteAssignedTasksException(ResponseCode ResponseCode, String ResponseDescription)
        {
            DTO_GetLayoutSiteAssignedTasksResponse _exception;
            _exception = new DTO_GetLayoutSiteAssignedTasksResponse()
            {
                ResponseCode = ResponseCode,
                ResponseCodeDescription = ResponseDescription,
                SessionId = string.Empty,
            };

            return _exception;
        }

        public static DTO_UpdateLayoutSiteTaskInfoResponse GetUpdateLayoutSiteTaskInfoException(ResponseCode ResponseCode, String ResponseDescription)
        {
            DTO_UpdateLayoutSiteTaskInfoResponse _exception;
            _exception = new DTO_UpdateLayoutSiteTaskInfoResponse()
            {
                ResponseCode = ResponseCode,
                ResponseCodeDescription = ResponseDescription,
                SessionId = string.Empty,
            };

            return _exception;
        }

        public static DTO_GetLayoutSiteAlarmsResponse GetLayoutSiteAlarmsException(ResponseCode ResponseCode, String ResponseDescription)
        {
            DTO_GetLayoutSiteAlarmsResponse _exception;
            _exception = new DTO_GetLayoutSiteAlarmsResponse()
            {
                ResponseCode = ResponseCode,
                ResponseCodeDescription = ResponseDescription,
                SessionId = string.Empty,
            };

            return _exception;
        }

        public static DTO_CreateAlarmResponse GetCreateAlarmException(ResponseCode ResponseCode, String ResponseDescription)
        {
            DTO_CreateAlarmResponse _exception;
            _exception = new DTO_CreateAlarmResponse()
            {
                ResponseCode = ResponseCode,
                ResponseCodeDescription = ResponseDescription,
                SessionId = string.Empty,
            };

            return _exception;
        }

        public static DTO_UploadLayoutSiteMediaAssetResponse GetUploadLayoutSiteMediaAssetException(ResponseCode ResponseCode, String ResponseDescription)
        {
            DTO_UploadLayoutSiteMediaAssetResponse _exception;
            _exception = new DTO_UploadLayoutSiteMediaAssetResponse()
            {
                ResponseCode = ResponseCode,
                ResponseCodeDescription = ResponseDescription,
                SessionId = string.Empty,
            };

            return _exception;
        }

        public static DTO_UpdateBeaconRunnerPositionResponse GetUpdateBeaconRunnerPositionException(ResponseCode ResponseCode, String ResponseDescription)
        {
            DTO_UpdateBeaconRunnerPositionResponse _exception;
            _exception = new DTO_UpdateBeaconRunnerPositionResponse()
            {
                ResponseCode = ResponseCode,
                ResponseCodeDescription = ResponseDescription,
                SessionId = string.Empty,
            };

            return _exception;
        }

        public static DTO_GetConversationHistoryResponse GetConversationHistoryException(ResponseCode ResponseCode, String ResponseDescription)
        {
            DTO_GetConversationHistoryResponse _exception;
            _exception = new DTO_GetConversationHistoryResponse()
            {
                ResponseCode = ResponseCode,
                ResponseCodeDescription = ResponseDescription,
                SessionId = string.Empty,
            };

            return _exception;
        }

        public static DTO_GetConversationsOfTodayResponse GetConversationsOfTodayException(ResponseCode ResponseCode, String ResponseDescription)
        {
            DTO_GetConversationsOfTodayResponse _exception;
            _exception = new DTO_GetConversationsOfTodayResponse()
            {
                ResponseCode = ResponseCode,
                ResponseCodeDescription = ResponseDescription,
                SessionId = string.Empty,
            };

            return _exception;
        }

        public static DTO_UpdateApplicationResponse GetUpdateApplicationException(ResponseCode ResponseCode, String ResponseDescription)
        {
            DTO_UpdateApplicationResponse _exception;
            _exception = new DTO_UpdateApplicationResponse()
            {
                ResponseCode = ResponseCode,
                ResponseCodeDescription = ResponseDescription,
                SessionId = string.Empty,
            };

            return _exception;
        }

        public static DTO_KeepALiveResponse GetKeepALiveException(ResponseCode ResponseCode, String ResponseDescription)
        {
            DTO_KeepALiveResponse _exception;
            _exception = new DTO_KeepALiveResponse()
            {
                ResponseCode = ResponseCode,
                ResponseCodeDescription = ResponseDescription,
                SessionId = string.Empty,
            };

            return _exception;
        }

        public static DTO_MessageToResponse GetSendMessageToException(ResponseCode ResponseCode, String ResponseDescription)
        {
            DTO_MessageToResponse _exception;
            _exception = new DTO_MessageToResponse()
            {
                ResponseCode = ResponseCode,
                ResponseCodeDescription = ResponseDescription,
                SessionId = string.Empty,
            };

            return _exception;
        }

        public static DTO_LogOutResponse GetMsgException(ResponseCode ResponseCode, String Descripcion)
        {
            DTO_LogOutResponse _exception;
            _exception = new DTO_LogOutResponse()
          {
              ResponseCode = ResponseCode,
              ResponseCodeDescription = Descripcion,
          };
            return _exception;
        }


        #region Exceptions

        public static void GenerateReporteDiarioNotFoundException(Session Session, String Description)
        {
            String _text;

            if (!GestorSesiones.DownRetriesRemaining(Session.SessionId))
            {
                GestorErrores.GenerateRetriesExpiredException(Session);
            }
            else
            {


                _text = String.Format(Description,
                                     Session.Login.UserName,
                                     Session.Login.Login,
                                     Session.SessionId);

                GenerateDataServiceException(_text, AlarmCode.ReceivedData_BadMessageData, ResponseCode.DATA_RC_INCOHERENCE_WITH_DB_DATA, Description, Session);
            }
        }

        public static void GenerateIncorrectFormatException(Session Session, String Description)
        {
            String _text;

            if (!GestorSesiones.DownRetriesRemaining(Session.SessionId))
            {
                GestorErrores.GenerateRetriesExpiredException(Session);
            }
            else
            {

                _text = String.Format("[{0}-{1}-{2}]",
                                    Session.Login.UserName,
                                    Session.Login.Login,
                                    Session.SessionId);

                _text = Description + " " + _text;

                GenerateDataServiceException(_text, AlarmCode.ReceivedData_BadMessageData, ResponseCode.DATA_RC_BAD_MESSAGE_DATA, Description, Session);
            }
        }

        internal static void GenerateIncorrectNumberLinesException(Session Session)
        {
            String _text;

            if (!GestorSesiones.DownRetriesRemaining(Session.SessionId))
            {
                GestorErrores.GenerateRetriesExpiredException(Session);
            }
            else
            {
                _text = String.Format(WSI.Common.Resource.String("STR_WS_DATA_ERROR_NOT_RPORTED_ALL_REGISTERS"),
                                   Session.Login.UserName,
                                   Session.Login.Login,
                                   Session.SessionId);

                GenerateDataServiceException(_text, AlarmCode.ReceivedData_BadMessageData, ResponseCode.DATA_RC_BAD_MESSAGE_DATA, WSI.Common.Resource.String("STR_WS_DATA_ERROR_NOT_RPORTED_ALL_REGISTERS_CLIENT"), Session);
            }
        }

        internal static void GenerateLineNotFoundException(Session Session, String Description)
        {
            GenerateLineNotFoundException(Session, Description, "");
        }

        internal static void GenerateLineNotFoundException(Session Session, String Description, String Additional)
        {
            String _text;

            if (!String.IsNullOrEmpty(Additional))
            {
                Additional = "(" + Additional + ")";
            }

            if (!GestorSesiones.DownRetriesRemaining(Session.SessionId))
            {
                GestorErrores.GenerateRetriesExpiredException(Session);
            }
            else
            {
                _text = String.Format(Description,
                                          Session.Login.UserName,
                                          Session.Login.Login,
                                          Session.SessionId);

                GenerateDataServiceException(_text, AlarmCode.WebService_Session_IncoherenceWithDBData, ResponseCode.DATA_RC_INCOHERENCE_WITH_DB_DATA, WSI.Common.Resource.String("STR_WS_DATA_ERROR_NOT_ALLOWED_LINE_CLIENT") + Additional, Session);
            }
        }

        internal static void GenerateReportPeriodException(Session Session, String Description)
        {
            String _text;

            if (!GestorSesiones.DownRetriesRemaining(Session.SessionId))
            {
                GestorErrores.GenerateRetriesExpiredException(Session);
            }
            else
            {


                _text = String.Format(Description,
                                     Session.Login.UserName,
                                     Session.Login.Login,
                                     Session.SessionId);

                GenerateDataServiceException(_text, AlarmCode.ReceivedData_BadMessageData, ResponseCode.DATA_RC_PERIOD_EXPIRED, WSI.Common.Resource.String("STR_WS_DATA_ERROR_INCORRECT_DATE"), Session);
            }
        }

        /// <summary>
        /// Used also in Mensajes Monitorización
        /// </summary>
        /// <param name="Session"></param>
        internal static void GeneratePlaceNotFoundException(Session Session, String Description)
        {
            String _text;

            if (!GestorSesiones.DownRetriesRemaining(Session.SessionId))
            {
                GestorErrores.GenerateRetriesExpiredException(Session);
            }
            else
            {
                _text = String.Format(Description,
                                      Session.Login.UserName,
                                      Session.Login.Login,
                                      Session.SessionId);

                GenerateDataServiceException(_text, AlarmCode.WebService_Session_IncoherenceWithDBData, ResponseCode.DATA_RC_INCOHERENCE_WITH_DB_DATA, WSI.Common.Resource.String("STR_WS_DATA_ERROR_NOT_ALLOWED_ESTABLECIMIENTO"), Session);
            }
        }


        internal static void GenerateCurrentTransactionException(Session Session, String InternalDescription, String ClientDescription)
        {
            //String _text;
            if (!GestorSesiones.DownRetriesRemaining(Session.SessionId))
            {
                GestorErrores.GenerateRetriesExpiredException(Session);
            }
            else
            {

                GenerateDataServiceException(InternalDescription, AlarmCode.ReceivedData_BadMessageData, ResponseCode.DATA_RC_BAD_MESSAGE_DATA, ClientDescription, Session);
            }
        }



        internal static void GenerateLineNotInformedException(Session Session)
        {
            String _text;
            if (!GestorSesiones.DownRetriesRemaining(Session.SessionId))
            {
                GestorErrores.GenerateRetriesExpiredException(Session);
            }
            else
            {

                _text = String.Format(WSI.Common.Resource.String("STR_WS_DATA_SESSION_NOT_INFORMED_LINE"),
                                    Session.Login.UserName,
                                    Session.Login.Login,
                                    Session.SessionId);

                GenerateDataServiceException(_text, AlarmCode.ReceivedData_BadMessageData, ResponseCode.DATA_RC_BAD_MESSAGE_TYPE, WSI.Common.Resource.String("STR_WS_DATA_SESSION_NOT_INFORMED_LINE_CLIENT"), Session);
            }
        }


        internal static void GenerateIncorrectReportedLinesException(Session Session, String InternalDescription, String ClientDescription)
        {
            //String _internaldescription;
            if (!GestorSesiones.DownRetriesRemaining(Session.SessionId))
            {
                GestorErrores.GenerateRetriesExpiredException(Session);
            }
            else
            {

                InternalDescription = String.Format(InternalDescription,
                                   Session.Login.UserName,
                                   Session.Login.Login,
                                   Session.SessionId);

                GenerateDataServiceException(InternalDescription, AlarmCode.ReceivedData_BadMessageData, ResponseCode.DATA_RC_BAD_MESSAGE_TYPE, ClientDescription, Session);
            }

        }

        internal static void GenerateChecksumException(Session Session, String Description)
        {
            String _text;
            if (!GestorSesiones.DownRetriesRemaining(Session.SessionId))
            {
                GestorErrores.GenerateRetriesExpiredException(Session);
            }
            else
            {
                _text = String.Format(Description,
                                 Session.Login.UserName,
                                 Session.Login.Login,
                                 Session.SessionId);

                GenerateDataServiceException(_text, AlarmCode.WebService_Session_BadChecksum, ResponseCode.DATA_RC_BAD_CHECKSUM, WSI.Common.Resource.String("STR_WS_DATA_ERROR_BAD_CHECKSUM"), Session);
            }
        }

        internal static void GeneratePendingRegistersException(Session Session, String Description, String Additional)
        {
            String _text;

            if (!String.IsNullOrEmpty(Additional))
            {
                Additional = "(" + Additional + ")";
            }
            if (!GestorSesiones.DownRetriesRemaining(Session.SessionId))
            {
                GestorErrores.GenerateRetriesExpiredException(Session);
            }
            else
            {

                _text = String.Format(Description,
                                   Session.Login.UserName,
                                   Session.Login.Login,
                                   Session.SessionId);

                GenerateDataServiceException(_text, AlarmCode.ReceivedData_BadMessageData, ResponseCode.DATA_RC_BAD_MESSAGE_TYPE,
                                      WSI.Common.Resource.String("STR_WS_DATA_ERROR_NOT_REPORTED_ALL_LINES_F2", "registros") + Additional, Session);
            }
        }

        internal static void GeneratePendingLinesException(Session Session, String Description, String Additional)
        {
            String _text;

            if (!String.IsNullOrEmpty(Additional))
            {
                Additional = "(" + Additional + ")";
            }

            if (!GestorSesiones.DownRetriesRemaining(Session.SessionId))
            {
                GestorErrores.GenerateRetriesExpiredException(Session);
            }
            else
            {

                _text = String.Format(Description,
                                   Session.Login.UserName,
                                   Session.Login.Login,
                                   Session.SessionId);

                GenerateDataServiceException(_text, AlarmCode.ReceivedData_BadMessageData, ResponseCode.DATA_RC_BAD_MESSAGE_TYPE,
                                      WSI.Common.Resource.String("STR_WS_DATA_ERROR_NOT_REPORTED_ALL_LINES_F2", "lineas") + Additional, Session);
            }
        }

        internal static void GeneratePendingLinesException(Session Session)
        {
            String _text;

            _text = WSI.Common.Resource.String("STR_WS_DATA_NOT_REPORTED_ALL_LINES",
                                                 Session.Login.UserName,
                                                 Session.Login.Login,
                                                 "RF4"
                                                 , Session.SessionId);

            if (!GestorSesiones.DownRetriesRemaining(Session.SessionId))
            {
                GestorErrores.GenerateRetriesExpiredException(Session);
            }
            else
            {

                GenerateDataServiceException(_text, AlarmCode.ReceivedData_BadMessageData, ResponseCode.DATA_RC_BAD_MESSAGE_TYPE,
                                     WSI.Common.Resource.String("STR_WS_DATA_ERROR_NOT_REPORTED_ALL_LINESF1"), Session);
            }
        }

        internal static void GenerateCountMonitorizacionRegsitersException(Session Session, String Description)
        {
            String _text;

            if (!GestorSesiones.DownRetriesRemaining(Session.SessionId))
            {
                GestorErrores.GenerateRetriesExpiredException(Session);
            }
            else
            {

                _text = String.Format(Description,
                                   Session.Login.UserName,
                                   Session.Login.Login,
                                   Session.SessionId);

                GenerateDataServiceException(_text, AlarmCode.ReceivedData_BadMessageData, ResponseCode.DATA_RC_BAD_MESSAGE_TYPE,
                                      WSI.Common.Resource.String("STR_WS_DATA_ERROR_NOT_REPORTED_ALL_LINESF1"), Session);
            }
        }

        internal static void GenerateWritingDbException(Session Session)
        {
            String _text;
            _text = WSI.Common.Resource.String("STR_WS_DATA_NOT_SAVE_REGISTER",
                                               Session.Login.UserName,
                                               Session.Login.Login,
                                               "RF4",
                                               Session.SessionId);
            GenerateDataServiceException(_text, AlarmCode.WebService_Transaction_Error, ResponseCode.DATA_RC_INTERNAL_DATA_ERROR, _text, Session);//"No fué posible grabar la transacción en la BBDD", Session);
        }


        internal static void GenerateSessionStatisticsInfoException(Session Session)
        {
            String _text;

            if (!GestorSesiones.DownRetriesRemaining(Session.SessionId))
            {
                GestorErrores.GenerateRetriesExpiredException(Session);
            }
            else
            {

                _text = String.Format(WSI.Common.Resource.String("STR_WS_DATA_ERROR_MF1_INCORRECTS_GENERIC"),
                                   Session.Login.UserName,
                                   Session.Login.Login,
                                   Session.SessionId);

                GenerateDataServiceException(_text, AlarmCode.ReceivedData_BadMessageData, ResponseCode.DATA_RC_BAD_MESSAGE_DATA, WSI.Common.Resource.String("STR_WS_DATA_INCONSISTENT_DATA"), Session);
            }
        }

        internal static void GenerateBadSequenceException(Session Session, String Description)
        {
            String _text;
            if (!GestorSesiones.DownRetriesRemaining(Session.SessionId))
            {
                GestorErrores.GenerateRetriesExpiredException(Session);
            }
            else
            {

                _text = String.Format(Description,
                                       Session.Login.UserName,
                                       Session.Login.Login,
                                       Session.SessionId);

                GenerateDataServiceException(_text, AlarmCode.WebService_Session_BadSeqID, ResponseCode.DATA_RC_BAD_SEQID, WSI.Common.Resource.String("STR_WS_DATA_INCORRECT_SECUENCE"), Session);
            }
        }

        internal static void GenerateRetriesExpiredException(Session Session)
        {
            String _text;
            AlarmCode _alarm;
            if (!FlujoAplicacion.InTransaction(Session.LastMessageType))
            {
                _text = WSI.Common.Resource.String("STR_WS_DATA_ERROR_MAX_RETRIES_SESSION");
                GestorSesiones.RemoveSession(Session.SessionId);
                _alarm = AlarmCode.WebService_Session_Remove;

            }
            else
            {
                _text = WSI.Common.Resource.String("STR_WS_DATA_ERROR_MAX_RETRIES_TRANSACTION");
                GestorSesiones.SetDefaultSession(Session.SessionId, _text);
                _alarm = AlarmCode.WebService_Transaction_End;
            }

            GestorErrores.GenerateDataServiceException(_text, _alarm, ResponseCode.DATA_RC_TOO_MANY_RETRYS, _text, Session);
        }


        internal static void GenerateIncorrectSubline(Session Session, String Description)
        {
            String _text;
            if (!GestorSesiones.DownRetriesRemaining(Session.SessionId))
            {
                GestorErrores.GenerateRetriesExpiredException(Session);
            }
            else
            {

                _text = String.Format(Description,
                                       Session.Login.UserName,
                                       Session.Login.Login,
                                       Session.SessionId);

                GenerateDataServiceException(_text, AlarmCode.ReceivedData_BadMessageData, ResponseCode.DATA_RC_BAD_MESSAGE_DATA, WSI.Common.Resource.String("STR_WS_DATA_ERROR_OPERATOR_DATA", Description), Session);
            }
        }

        internal static void GenerateCatalogInfoException(Session Session, String Description)
        {
            String _text;
            if (!GestorSesiones.DownRetriesRemaining(Session.SessionId))
            {
                GestorErrores.GenerateRetriesExpiredException(Session);
            }
            else
            {

                _text = String.Format(Description,
                                       Session.Login.UserName,
                                       Session.Login.Login,
                                       Session.SessionId);

                GenerateDataServiceException(_text, AlarmCode.ReceivedData_BadMessageData, ResponseCode.DATA_RC_BAD_MESSAGE_DATA, Description, Session);
            }
        }


        internal static void GenerateDataServiceException(String InternalDescription, AlarmCode AlarmCode,
                                                ResponseCode ResponseCode, String ClientDescription)
        {
            GestorLogs.LogSoap(((Int16)LogID.WS_LOG), InternalDescription);// LogMessage(InternalDescription);
            GestorLogs.LogAlarm(AlarmSourceCode.WebService, 0, WSI.Common.Resource.String("STR_WS_DATA_APP_NAME"), AlarmCode, InternalDescription);

            throw new DataServiceException(ResponseCode, ClientDescription);
        }

        internal static void GenerateDataServiceException(String InternalDescription, AlarmCode AlarmCode,
                                            ResponseCode ResponseCode, String ClientDescription, Session Session)
        {
            GestorLogs.LogSoap(((Int16)LogID.WS_LOG), InternalDescription); //GestorLogs.LogMessage(InternalDescription);
            GestorLogs.LogAlarm(AlarmSourceCode.WebService, 0, WSI.Common.Resource.String("STR_WS_DATA_APP_NAME"), AlarmCode, InternalDescription, Session.OperadorId, 0, 0);

            throw new DataServiceException(ResponseCode, ClientDescription);
        }

        internal static void GenerateDataServiceWarning(String InternalDescription, AlarmCode AlarmCode,
                                            ResponseCode ResponseCode, String ClientDescription, Session Session)
        {
            GestorLogs.LogSoap(((Int16)LogID.WS_LOG), InternalDescription);//GestorLogs.LogMessage(InternalDescription);
            GestorLogs.LogWarning(AlarmSourceCode.WebService, 0, WSI.Common.Resource.String("STR_WS_DATA_APP_NAME"), AlarmCode, InternalDescription, Session.OperadorId, 0);

            throw new DataServiceException(ResponseCode, ClientDescription);
        }

        #endregion
    }
}
