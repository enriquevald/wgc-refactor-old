﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DATA.OBJ.DTO;
using DATA.OBJ;
using WSI.Common;
using DATA.OBJ.DTO.Responses;

namespace DATA.BL.Mensajes
{
    public class ProvideResponses
    {
        #region Responses

        //------------------------------------------------------------------------------
        // PURPOSE: It verifies if the user exists
        //
        //  PARAMS:
        //      - INPUT: DataTable with general params
        //          - 
        //
        //      - OUTPUT:
        //
        // RETURNS: Returns Msg to client
        internal DTO_LogInResponse ProvideRespuestaInicioSesion()
        {
            DTO_LogInResponse _resp_inicio_sesion;
            try
            {
                _resp_inicio_sesion = new DTO_LogInResponse();
                _resp_inicio_sesion.ResponseCode = ResponseCode.DATA_RC_OK;
                _resp_inicio_sesion.ResponseCodeDescription = WSI.Common.Resource.String("STR_WS_DATA_SESSION_CORRECT_LOGIN");
                _resp_inicio_sesion.SessionId = String.Empty;
                _resp_inicio_sesion.SessionId = Utils.CreateSessionID();

                return _resp_inicio_sesion;
            }
            catch (Exception Ex)
            {
                GestorLogs.LogException(Ex.Message, Ex);
                return GestorErrores.GetInicioSesionException(ResponseCode.DATA_RC_BAD_LOGIN, WSI.Common.Resource.String("STR_WS_DATA_ERROR_DATA_SESSION"));
            }
        }


        internal DTO_GetLayoutSiteAssignedTasksResponse ProvideGetLayoutSiteAssignedTasksResponse(String Session)
        {
            DTO_GetLayoutSiteAssignedTasksResponse _response;
            try
            {
                _response = new DTO_GetLayoutSiteAssignedTasksResponse();
                _response.ResponseCode = ResponseCode.DATA_RC_OK;
                _response.ResponseCodeDescription = WSI.Common.Resource.String("STR_WS_DATA_SESSION_CORRECT_GETTING_TASK");
                _response.SessionId = Session;

                return _response;
            }
            catch (Exception Ex)
            {
                GestorLogs.LogException(Ex.Message, Ex);
                return GestorErrores.GetLayoutSiteAssignedTasksException(ResponseCode.DATA_RC_BAD_MESSAGE_DATA, WSI.Common.Resource.String("STR_WS_DATA_ERROR_GETTING_TASK"));
            }
        }

        internal DTO_GetLayoutSiteAlarmsResponse ProvideGetLayoutSiteAlarmsResponse(String Session)
        {
            DTO_GetLayoutSiteAlarmsResponse _response;
            try
            {
                _response = new DTO_GetLayoutSiteAlarmsResponse();
                _response.ResponseCode = ResponseCode.DATA_RC_OK;
                _response.ResponseCodeDescription = WSI.Common.Resource.String("STR_WS_DATA_SESSION_CORRECT_GETTING_TASK");
                _response.SessionId = Session;

                return _response;
            }
            catch (Exception Ex)
            {
                GestorLogs.LogException(Ex.Message, Ex);
                return GestorErrores.GetLayoutSiteAlarmsException(ResponseCode.DATA_RC_BAD_MESSAGE_DATA, WSI.Common.Resource.String("STR_WS_DATA_ERROR_GETTING_TASK"));
            }
        }
        internal DTO_UpdateLayoutSiteTaskInfoResponse ProvideUpdateLayoutSiteTaskInfoResponse(String Session)
        {
            DTO_UpdateLayoutSiteTaskInfoResponse _response;
            try
            {
                _response = new DTO_UpdateLayoutSiteTaskInfoResponse();
                _response.ResponseCode = ResponseCode.DATA_RC_OK;
                _response.ResponseCodeDescription = WSI.Common.Resource.String("STR_WS_DATA_SESSION_CORRECT_GETTING_TASK");
                _response.SessionId = Session;

                return _response;
            }
            catch (Exception Ex)
            {
                GestorLogs.LogException(Ex.Message, Ex);
                return GestorErrores.GetUpdateLayoutSiteTaskInfoException(ResponseCode.DATA_RC_BAD_MESSAGE_DATA, WSI.Common.Resource.String("STR_WS_DATA_ERROR_GETTING_TASK"));
            }
        }

        internal DTO_SaveLogResponse ProvideSaveLogResponse(String Session)
        {
            DTO_SaveLogResponse _response;
            try
            {
                _response = new DTO_SaveLogResponse();
                _response.ResponseCode = ResponseCode.DATA_RC_OK;
                _response.ResponseCodeDescription = WSI.Common.Resource.String("STR_WS_DATA_SESSION_CORRECT_SAVE_LOGS");
                _response.SessionId = Session;

                return _response;
            }
            catch (Exception Ex)
            {
                GestorLogs.LogException(Ex.Message, Ex);
                return GestorErrores.GetSaveLogException(ResponseCode.DATA_RC_BAD_MESSAGE_DATA, WSI.Common.Resource.String("STR_WS_DATA_ERROR_SAVING_LOGS"));
            }
        }

        internal DTO_CreateAlarmResponse ProvideCreateAlarmResponse(String Session)
        {
            DTO_CreateAlarmResponse _response;
            try
            {
                _response = new DTO_CreateAlarmResponse();
                _response.ResponseCode = ResponseCode.DATA_RC_OK;
                _response.ResponseCodeDescription = WSI.Common.Resource.String("STR_WS_DATA_SESSION_CORRECT_CREATE_ALARM");
                _response.SessionId = Session;

                return _response;
            }
            catch (Exception Ex)
            {
                GestorLogs.LogException(Ex.Message, Ex);
                return GestorErrores.GetCreateAlarmException(ResponseCode.DATA_RC_BAD_MESSAGE_DATA, WSI.Common.Resource.String("STR_WS_DATA_ERROR_GETTING_TASK"));
            }
        }

        internal DTO_UploadLayoutSiteMediaAssetResponse ProvideUploadLayoutSiteMediaAssetResponse(String Session)
        {
            DTO_UploadLayoutSiteMediaAssetResponse _response;
            try
            {
                _response = new DTO_UploadLayoutSiteMediaAssetResponse();
                _response.ResponseCode = ResponseCode.DATA_RC_OK;
                _response.ResponseCodeDescription = WSI.Common.Resource.String("STR_WS_DATA_SESSION_CORRECT_GETTING_TASK");
                _response.SessionId = Session;

                return _response;
            }
            catch (Exception Ex)
            {
                GestorLogs.LogException(Ex.Message, Ex);
                return GestorErrores.GetUploadLayoutSiteMediaAssetException(ResponseCode.DATA_RC_BAD_MESSAGE_DATA, WSI.Common.Resource.String("STR_WS_DATA_ERROR_GETTING_TASK"));
            }
        }

        internal DTO_UpdateBeaconRunnerPositionResponse ProvideUpdateBeaconRunnerPositionResponse(String Session)
        {
            DTO_UpdateBeaconRunnerPositionResponse _response;
            try
            {
                _response = new DTO_UpdateBeaconRunnerPositionResponse();
                _response.ResponseCode = ResponseCode.DATA_RC_OK;
                _response.ResponseCodeDescription = WSI.Common.Resource.String("STR_WS_DATA_SESSION_CORRECT_SEND_BEACONS");
                _response.SessionId = Session;

                return _response;
            }
            catch (Exception Ex)
            {
                GestorLogs.LogException(Ex.Message, Ex);
                return GestorErrores.GetUpdateBeaconRunnerPositionException(ResponseCode.DATA_RC_BAD_MESSAGE_DATA, WSI.Common.Resource.String("STR_WS_DATA_ERROR_GETTING_TASK"));
            }
        }

        internal DTO_GetConversationHistoryResponse ProvideConversationHistoryResponse(String Session)
        {
            DTO_GetConversationHistoryResponse _response;
            try
            {
                _response = new DTO_GetConversationHistoryResponse();
                _response.ResponseCode = ResponseCode.DATA_RC_OK;
                _response.ResponseCodeDescription = WSI.Common.Resource.String("STR_WS_DATA_SESSION_CORRECT_GETTING_CONVERSATION_HISTORY");
                _response.SessionId = Session;

                return _response;
            }
            catch (Exception Ex)
            {
                GestorLogs.LogException(Ex.Message, Ex);
                return GestorErrores.GetConversationHistoryException(ResponseCode.DATA_RC_BAD_MESSAGE_DATA, WSI.Common.Resource.String("STR_WS_DATA_ERROR_GETTING_TASK"));
            }
        }

        internal DTO_GetConversationsOfTodayResponse ProvideConversationsOfTodayResponse(String Session)
        {
            DTO_GetConversationsOfTodayResponse _response;
            try
            {
                _response = new DTO_GetConversationsOfTodayResponse();
                _response.ResponseCode = ResponseCode.DATA_RC_OK;
                _response.ResponseCodeDescription = WSI.Common.Resource.String("STR_WS_DATA_SESSION_CORRECT_GETTING_CONVERSATION_HISTORY");
                _response.SessionId = Session;

                return _response;
            }
            catch (Exception Ex)
            {
                GestorLogs.LogException(Ex.Message, Ex);
                return GestorErrores.GetConversationsOfTodayException(ResponseCode.DATA_RC_BAD_MESSAGE_DATA, WSI.Common.Resource.String("STR_WS_DATA_ERROR_GETTING_TASK"));
            }
        }

        internal DTO_UpdateApplicationResponse ProvideUpdateApplicationResponse(String Session)
        {
            DTO_UpdateApplicationResponse _response;
            try
            {
                _response = new DTO_UpdateApplicationResponse();
                _response.ResponseCode = ResponseCode.DATA_RC_OK;
                _response.ResponseCodeDescription = WSI.Common.Resource.String("STR_WS_DATA_SESSION_CORRECT_GETTING_LASTEST_VERSION");
                _response.SessionId = Session;

                return _response;
            }
            catch (Exception Ex)
            {
                GestorLogs.LogException(Ex.Message, Ex);
                return GestorErrores.GetUpdateApplicationException(ResponseCode.DATA_RC_BAD_MESSAGE_DATA, WSI.Common.Resource.String("STR_WS_DATA_ERROR_GETTING_LASTEST_VERSION"));
            }
        }


        internal DTO_MessageToResponse ProvideSendMessageToResponse(String Session)
        {
            DTO_MessageToResponse _response;
            try
            {
                _response = new DTO_MessageToResponse();
                _response.ResponseCode = ResponseCode.DATA_RC_OK;
                _response.ResponseCodeDescription = WSI.Common.Resource.String("STR_WS_DATA_SESSION_CORRECT_SEND_MESSAGE");
                _response.SessionId = Session;

                return _response;
            }
            catch (Exception Ex)
            {
                GestorLogs.LogException(Ex.Message, Ex);
                return GestorErrores.GetSendMessageToException(ResponseCode.DATA_RC_BAD_MESSAGE_DATA, WSI.Common.Resource.String("STR_WS_DATA_ERROR_GETTING_TASK"));
            }
        }


        internal DTO_LogOutResponse ProvideValidResponse()
        {
            DTO_LogOutResponse _response;
            _response = new DTO_LogOutResponse();
            _response.ResponseCode = ResponseCode.DATA_RC_OK;
            _response.ResponseCodeDescription = ResponseCode.DATA_RC_OK.ToString();
            return _response;
        }



        #endregion
    }
}
