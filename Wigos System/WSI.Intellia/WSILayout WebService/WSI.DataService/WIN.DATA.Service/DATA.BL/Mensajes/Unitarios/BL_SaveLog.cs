﻿////------------------------------------------------------------------------------
//// Copyright © 2012 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: GestorMensajes.cs
//// 
////      DESCRIPTION: Management methods InicioSesion,CatalogosOperador,CierreSesion,TareasOperador
//// 
////           AUTHOR: Carlos Rodrigo
//// 
////    CREATION DATE: 27-MAY-2015
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 27-MAY-2015 CSR    First release.
////------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using DATA.OBJ.DTO;
using System.Data;
using DATA.DAL;
using DATA.OBJ;
using System.ServiceModel.Channels;
using System.ServiceModel;
using WSI.Common;
using System.Configuration;
using System.Data.SqlClient;
using DATA.OBJ.DTO.Responses;
using System.IO;
using System.Drawing;
namespace DATA.BL.Mensajes
{
  public partial class GestorMensajes
  {
    #region Public Methods
    //------------------------------------------------------------------------------
    // PURPOSE: return the IResponse for DATA_SaveLog
    //
    //  PARAMS:
    //      - INPUT: 
    //          - SaveLog
    //          - Session
    //
    //      - OUTPUT:
    //
    // RETURNS: IResponse
    public DTO_SaveLogResponse BL_SaveLog(SaveLog info, Session Session)
    {

      ConversorMensajes _conversor;
      ProvideResponses _provider;

      DateTime _ini_date;

      try
      {

        _conversor = new ConversorMensajes();
        _ini_date = new DateTime(WASDB.Now.Year, WASDB.Now.Month, 1);
        _provider = new ProvideResponses();

        // validacion de los datos enviados

        DbUnitarios.DataBaseSaveLog(info, Session.LoginId);


        DTO_SaveLogResponse _response = _provider.ProvideSaveLogResponse (Session.SessionId);

        return _response;
      }
      catch (DataServiceException DataServiceEx)
      {
        throw DataServiceEx;
      }
      catch (Exception Ex)
      {
        GestorLogs.LogException(Ex.Message, Ex, Session.OperadorId);
        throw new DataServiceException(ResponseCode.DATA_RC_BAD_MESSAGE_DATA, WSI.Common.Resource.String("STR_WS_DATA_ERROR_SAVING_LOGS"));
      }
    }


    #endregion
  }
}
