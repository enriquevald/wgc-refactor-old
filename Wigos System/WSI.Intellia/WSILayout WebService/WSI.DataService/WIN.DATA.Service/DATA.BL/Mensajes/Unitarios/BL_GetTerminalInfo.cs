﻿////------------------------------------------------------------------------------
//// Copyright © 2012 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: BL_GetTerminalInfo.cs
//// 
////      DESCRIPTION: Provides differents way of obtains Terminal Information
//// 
////           AUTHOR: Carlos Rodrigo
//// 
////    CREATION DATE: 27-MAY-2015
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 27-MAY-2015 CSR    First release.
////------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using DATA.OBJ.DTO;
using System.Data;
using DATA.OBJ;
using WSI.Common;
using System.Data.SqlClient;
namespace DATA.BL.Mensajes
{
  public partial class GestorMensajes
  {
    #region Public Methods
    /// <summary>
    /// Returns all terminal information by a specific Terminal.
    /// </summary>
    /// <param name="terminalInfo"></param>
    /// <param name="Session"></param>
    /// <returns>Returns a DTO_GetLayoutSiteAlarmsResponse that contains the Session Information, result of operation, and the list of Alarms in the Terminal</returns>
    public DTO_GetLayoutSiteAlarmsResponse BL_GetTerminalInfoByTerminalId(string terminalInfo, Session Session)
    {

      DataTable _asigned_task_table;
      ConversorMensajes _conversor;
      ProvideResponses _provider;

      DateTime _ini_date;

      try
      {
        _asigned_task_table = new DataTable();
        _conversor = new ConversorMensajes();
        _ini_date = new DateTime(WASDB.Now.Year, WASDB.Now.Month, 1);
        _provider = new ProvideResponses();

        long terminalId = GetTerminalIdFromData(terminalInfo);

        if (!DbUnitarios.IsValidTerminalId(terminalId))
          throw new DataServiceException(ResponseCode.DATA_RC_BAD_MESSAGE_DATA, WSI.Common.Resource.String("STR_WS_DATA_INEXISTENT_TERMINAL"));

        //String log = DbUnitarios.DataBaseGetLogFromTerminalByTerminalId(terminalInfo);

        _asigned_task_table = DbUnitarios.DataBaseGetLayoutSiteAlarms(terminalId);

        DTO_GetLayoutSiteAlarmsResponse _response = _provider.ProvideGetLayoutSiteAlarmsResponse(Session.SessionId);
        _response.Alarms = _conversor.ProvideLayoutSiteAlarms(_asigned_task_table);

        return _response;
      }
      catch (DataServiceException DataServiceEx)
      {
        throw DataServiceEx;
      }
      catch (Exception Ex)
      {
        GestorLogs.LogException(Ex.Message, Ex, Session.OperadorId);
        throw new DataServiceException(ResponseCode.DATA_RC_BAD_MESSAGE_DATA, WSI.Common.Resource.String("STR_WS_DATA_ERROR_TRATAR_DATOS"));
      }
    }
    /// <summary>
    /// Returns the Terminal Id using information like the BASE_NAME or the FLOOR_ID
    /// </summary>
    /// <param name="terminalInfo">BASE_NAME or the FLOOR_ID</param>
    /// <returns></returns>
    public static long GetTerminalIdFromData(string terminalInfo)
    {
      String generalParamValue = GeneralParam.GetString("Intellia", "Terminal.QRCode", "Terminal Id");
      String[] splitGeneralParam = generalParamValue.Split('#');
      String QRVariable = splitGeneralParam.Length > 1? splitGeneralParam[1] : string.Empty;

      switch (QRVariable)
      {
        case "{@QR}":
        case "{@AssetNumber}":
          return GetTerminalIdFromBaseName(terminalInfo);
        case "{@Location}":
          return GetTerminalIdFromFloorId(terminalInfo);
        case "{@Bank}":
          return GetTerminalIdFromBankId(terminalInfo);
        default:
          return GetTerminalIdFromPosition(terminalInfo);
      }
    }

    private static long GetTerminalIdFromBaseName(string terminalInfo)
    {
      Int32 countResult;
      DataTable _data;

      _data = new DataTable();
      using (DB_TRX _transaction = new DB_TRX())
      {
        using (SqlCommand _command = new SqlCommand("Layout_GetTerminalIdFromBaseName", _transaction.SqlTransaction.Connection, _transaction.SqlTransaction))
        {
          SqlParameter outputIdParam = new SqlParameter("@count_terminal", SqlDbType.Int)
          {
            Direction = ParameterDirection.Output
          };
          _command.Parameters.Add("@baseName", SqlDbType.NVarChar).Value = terminalInfo;
          _command.CommandType = CommandType.StoredProcedure;
          _command.Parameters.Add(outputIdParam);
          _command.ExecuteNonQuery();
          countResult = int.Parse(outputIdParam.Value.ToString());
        }
      }

      return countResult;
    }

    private static Int32 GetTerminalIdFromFloorId(string terminalInfo)
    {
      Int32 countResult;
      DataTable _data;

      _data = new DataTable();
      using (DB_TRX _transaction = new DB_TRX())
      {
        using (SqlCommand _command = new SqlCommand("Layout_GetTerminalIdFromFloorId", _transaction.SqlTransaction.Connection, _transaction.SqlTransaction))
        {
          SqlParameter outputIdParam = new SqlParameter("@count_terminal", SqlDbType.Int)
          {
            Direction = ParameterDirection.Output
          };
          _command.Parameters.Add("@floorId", SqlDbType.NVarChar).Value = terminalInfo;
          _command.CommandType = CommandType.StoredProcedure;
          _command.Parameters.Add(outputIdParam);
          _command.ExecuteNonQuery();
          countResult = int.Parse(outputIdParam.Value.ToString());
        }
      }

      return countResult;
    }

    private static long GetTerminalIdFromBankId(string terminalInfo)
    {
      Int32 countResult;
      DataTable _data;

      _data = new DataTable();
      using (DB_TRX _transaction = new DB_TRX())
      {
        using (SqlCommand _command = new SqlCommand("Layout_GetTerminalIdFromBankId", _transaction.SqlTransaction.Connection, _transaction.SqlTransaction))
        {
          SqlParameter outputIdParam = new SqlParameter("@count_terminal", SqlDbType.Int)
          {
            Direction = ParameterDirection.Output
          };
          _command.Parameters.Add("@bankId", SqlDbType.BigInt).Value = long.Parse(terminalInfo);
          _command.CommandType = CommandType.StoredProcedure;
          _command.Parameters.Add(outputIdParam);
          _command.ExecuteNonQuery();
          countResult = int.Parse(outputIdParam.Value.ToString());
        }
      }

      return countResult;
    }


    private static long GetTerminalIdFromPosition(string terminalInfo)
    {
      Int32 countResult;
      DataTable _data;

      _data = new DataTable();
      using (DB_TRX _transaction = new DB_TRX())
      {
        using (SqlCommand _command = new SqlCommand("Layout_GetTerminalIdFromPosition", _transaction.SqlTransaction.Connection, _transaction.SqlTransaction))
        {
          SqlParameter outputIdParam = new SqlParameter("@count_terminal", SqlDbType.Int)
          {
            Direction = ParameterDirection.Output
          };
          _command.Parameters.Add("@position", SqlDbType.BigInt).Value = long.Parse(terminalInfo);
          _command.CommandType = CommandType.StoredProcedure;
          _command.Parameters.Add(outputIdParam);
          _command.ExecuteNonQuery();
          countResult = int.Parse(outputIdParam.Value.ToString());
        }
      }

      return countResult;
    }

    public DTO_GetLayoutSiteAlarmsResponse BL_GetAlarmsCreatedByUser(Session Session)
    {

      DataTable _asigned_task_table;
      ConversorMensajes _conversor;
      ProvideResponses _provider;

      DateTime _ini_date;

      try
      {
        _asigned_task_table = new DataTable();
        _conversor = new ConversorMensajes();
        _ini_date = new DateTime(WASDB.Now.Year, WASDB.Now.Month, 1);
        _provider = new ProvideResponses();

        _asigned_task_table = DbUnitarios.DataBaseGetAlarmsCreatedByUser(Session.LoginId);

        DTO_GetLayoutSiteAlarmsResponse _response = _provider.ProvideGetLayoutSiteAlarmsResponse(Session.SessionId);
        _response.Alarms = _conversor.ProvideLayoutSiteAlarms(_asigned_task_table);

        _response.Log = new List<String>();



        return (_response);
      }
      catch (DataServiceException DataServiceEx)
      {
        throw DataServiceEx;
      }
      catch (Exception Ex)
      {
        GestorLogs.LogException(Ex.Message, Ex, Session.OperadorId);
        throw new DataServiceException(ResponseCode.DATA_RC_BAD_MESSAGE_DATA, WSI.Common.Resource.String("STR_WS_DATA_ERROR_TRATAR_DATOS"));
      }
    }



    #endregion
  }
}
