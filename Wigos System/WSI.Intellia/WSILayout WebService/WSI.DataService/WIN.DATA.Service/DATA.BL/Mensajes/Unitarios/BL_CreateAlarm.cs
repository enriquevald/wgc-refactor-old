﻿////------------------------------------------------------------------------------
//// Copyright © 2012 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: GestorMensajes.cs
//// 
////      DESCRIPTION: Management methods InicioSesion,CatalogosOperador,CierreSesion,TareasOperador
//// 
////           AUTHOR: Carlos Rodrigo
//// 
////    CREATION DATE: 27-MAY-2015
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 27-MAY-2015 CSR    First release.
////------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using DATA.OBJ.DTO;
using System.Data;
using DATA.DAL;
using DATA.OBJ;
using System.ServiceModel.Channels;
using System.ServiceModel;
using WSI.Common;
using System.Configuration;
using System.Data.SqlClient;
using DATA.OBJ.DTO.Responses;
using System.IO;
using System.Drawing;
namespace DATA.BL.Mensajes
{
  public partial class GestorMensajes
  {
    #region Public Methods
    //------------------------------------------------------------------------------
    // PURPOSE: return the IResponse for DATA_TareasOperador
    //
    //  PARAMS:
    //      - INPUT: 
    //          - TareasOperador
    //          - Session
    //
    //      - OUTPUT:
    //
    // RETURNS: IResponse
    public DTO_CreateAlarmResponse BL_CreateAlarm(CreateAlarm info, Session Session)
    {
      DataTable _asigned_task_table;
      ConversorMensajes _conversor;
      ProvideResponses _provider;

      DateTime _ini_date;

      try
      {
        _asigned_task_table = new DataTable();
        _conversor = new ConversorMensajes();
        _ini_date = new DateTime(WASDB.Now.Year, WASDB.Now.Month, 1);
        _provider = new ProvideResponses();
        long terminalId;
        if (info.SubCategory == 99000)
          terminalId = int.Parse(info.TerminalId);
        else
          terminalId = GestorMensajes.GetTerminalIdFromData(info.TerminalId);


        if (!DbUnitarios.IsValidTerminalId(terminalId))
          throw new DataServiceException(ResponseCode.DATA_RC_BAD_MESSAGE_DATA, WSI.Common.Resource.String("STR_WS_DATA_INEXISTENT_TERMINAL"));
        else {
          info.TerminalId = terminalId.ToString();
        }
        long imageId = 0;
        if (!string.IsNullOrEmpty(info.Image))
        {
          var thumbnail = GetThumbnailFromByteArray(Convert.FromBase64String(info.Image), 64, 64);
          imageId = DbUnitarios.DataBaseUploadLayoutSiteMediaAsset(new LayoutSiteMediaAsset
                                                                       {
                                                                         Data = info.Image,
                                                                         DataThumbnail = thumbnail,
                                                                         MediaType = LayoutSiteMediaAssetType.Photo,
                                                                         Type = LayoutSiteMediaType.Terminal
                                                                       }, Session.LoginId);
        }

        DbUnitarios.DataBaseCreateAlarm(info, Session.LoginId, imageId);


        DTO_CreateAlarmResponse _response = _provider.ProvideCreateAlarmResponse(Session.SessionId);

        return _response;
      }
      catch (DataServiceException DataServiceEx)
      {
        throw DataServiceEx;
      }
      catch (Exception Ex)
      {
        GestorLogs.LogException(Ex.Message, Ex, Session.OperadorId);
        throw new DataServiceException(ResponseCode.DATA_RC_BAD_MESSAGE_DATA, WSI.Common.Resource.String("STR_WS_DATA_ERROR_TRATAR_DATOS"));
      }
    }


    public static byte[] GetThumbnailFromByteArray(byte[] myImage, int thumbWidth, int thumbHeight)
    {
      using (MemoryStream ms = new MemoryStream())
      using (Image thumbnail = Image.FromStream(new MemoryStream(myImage)).GetThumbnailImage(thumbWidth, thumbHeight, null, new IntPtr()))
      {
        thumbnail.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
        return ms.ToArray();
      }
    }

    #endregion
  }
}
