﻿////------------------------------------------------------------------------------
//// Copyright © 2012 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: BL_LayoutSiteMessages.cs
//// 
////      DESCRIPTION: Manage the chat functionality in SmartFloor APP
//// 
////           AUTHOR: Carlos Rodrigo
//// 
////    CREATION DATE: 02-FEB-2016
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 02-FEB-2016    First release.
////------------------------------------------------------------------------------
using DATA.OBJ;
using DATA.OBJ.DTO;
using DATA.OBJ.DTO.Responses;
using System;
using System.Data;

namespace DATA.BL.Mensajes
{
  public partial class GestorMensajes
  {
    /// <summary>
    /// Send Message to another User
    /// </summary>
    /// <param name="session">Session Id</param>
    /// <param name="message">The object that represents the message</param>
    /// <returns>DTO_MessageToResponse</returns>
    public DTO_MessageToResponse BL_SendMessageTo(Session session, LayoutSiteManagerRunnerMessage message)
    {
      ProvideResponses _provider;
      DateTime _ini_date;

      try
      {
        _provider = new ProvideResponses();

        message.Id = DbUnitarios.DataBaseSendMessageTo(message);
        DTO_MessageToResponse _response = _provider.ProvideSendMessageToResponse(session.SessionId);
        _response.Message = message;
        return (_response);
      }
      catch (DataServiceException DataServiceEx)
      {
        throw DataServiceEx;
      }
      catch (Exception Ex)
      {
        GestorLogs.LogException(Ex.Message, Ex, session.OperadorId);
        throw new DataServiceException(ResponseCode.DATA_RC_BAD_MESSAGE_DATA, WSI.Common.Resource.String("STR_WS_DATA_ERROR_TRATAR_DATOS"));
      }
    }

    public DTO_GetConversationHistoryResponse BL_GetConversationHistory(Session session, int managerId)
    {
      DataTable _conversationHistory;
      ConversorMensajes _conversor;
      ProvideResponses _provider;
      DateTime _ini_date;

      try
      {
        _conversationHistory = new DataTable();
        _conversor = new ConversorMensajes();
        _provider = new ProvideResponses();

        _conversationHistory = DbUnitarios.DataBaseGetConversationHistory(session.LoginId, managerId);

        DTO_GetConversationHistoryResponse _response = _provider.ProvideConversationHistoryResponse(session.SessionId);
        _response.Messages = _conversor.ProvideLayoutSiteConversationHistory(_conversationHistory);

        return (_response);
      }
      catch (DataServiceException DataServiceEx)
      {
        throw DataServiceEx;
      }
      catch (Exception Ex)
      {
        GestorLogs.LogException(Ex.Message, Ex, session.OperadorId);
        throw new DataServiceException(ResponseCode.DATA_RC_BAD_MESSAGE_DATA, WSI.Common.Resource.String("STR_WS_DATA_ERROR_TRATAR_DATOS"));
      }
    }

    public DTO_GetConversationsOfTodayResponse BL_GetConversationsOfToday(Session session, long runnerId)
    {
      DataTable _conversationHistory;
      ConversorMensajes _conversor;
      ProvideResponses _provider;

      try
      {
        _conversationHistory = new DataTable();
        _conversor = new ConversorMensajes();
        _provider = new ProvideResponses();

        _conversationHistory = DbUnitarios.DataBaseGetConversationsOfToday(runnerId);
        DataTable _managers = DbUnitarios.DataBaseGetManagersAvailable();

        DTO_GetConversationsOfTodayResponse _response = _provider.ProvideConversationsOfTodayResponse(session.SessionId);
        _response.Headers = _conversor.ProvideLayoutSiteConversationsOfToday(_conversationHistory);
        _response.Managers = _conversor.ProvideLayoutSiteAvailableManagers(_managers);

        return (_response);
      }
      catch (DataServiceException DataServiceEx)
      {
        throw DataServiceEx;
      }
      catch (Exception Ex)
      {
        GestorLogs.LogException(Ex.Message, Ex, session.OperadorId);
        throw new DataServiceException(ResponseCode.DATA_RC_BAD_MESSAGE_DATA, WSI.Common.Resource.String("STR_WS_DATA_ERROR_TRATAR_DATOS"));
      }
    }

    public DTO_UpdateApplicationResponse BL_UpdateApplication(Session session)
    {
      ConversorMensajes _conversor;
      ProvideResponses _provider;

      DateTime _ini_date;

      try
      {
        _conversor = new ConversorMensajes();
        _provider = new ProvideResponses();

        String version = DbUnitarios.DataBaseGetApplicationLastestVersionApk();

        DTO_UpdateApplicationResponse _response = _provider.ProvideUpdateApplicationResponse(session.SessionId);
        _response.Version = version;

        return (_response);
      }
      catch (DataServiceException DataServiceEx)
      {
        throw DataServiceEx;
      }
      catch (Exception Ex)
      {
        GestorLogs.LogException(Ex.Message, Ex, session.OperadorId);
        throw new DataServiceException(ResponseCode.DATA_RC_BAD_MESSAGE_DATA, WSI.Common.Resource.String("STR_WS_DATA_ERROR_TRATAR_DATOS"));
      }
    }
  }
}
