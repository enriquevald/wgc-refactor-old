﻿////------------------------------------------------------------------------------
//// Copyright © 2012 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: GestorMensajes.cs
//// 
////      DESCRIPTION: Manages the methods inside Reporte Transaction
//// 
////           AUTHOR: Xavier Iglesia
//// 
////    CREATION DATE: 10-OCT-2012
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 10-OCT-2012 XIT    First release.
//// 19-OCT-2012 JCA    changes for the new specifications.
//// 24-OCT-2012 XIT-JCA Refactorized
//// 22-NOV-2012 JCA    Save to BBDD in each step
//// 05-DEC-2012 XIT         Moved from Transaction file to call file
////------------------------------------------------------------------------------
using System;
using System.Linq;
using DATA.OBJ.DTO;
using DATA.OBJ;
using DATA.BL.Flujos;
using DATA.OBJ.Transactions;
using System.Collections.Generic;
using System.Xml;
using WSI.Common;
using System.Configuration;
using System.Data;
using DATA.DAL;

namespace DATA.BL.Mensajes
{
    public partial class GestorMensajes
    {
        #region Public Methods
        //------------------------------------------------------------------------------
        // PURPOSE: Close de Session
        //
        //  PARAMS:
        //      - INPUT: 
        //          - CierreSesion
        //          - Session
        //
        //      - OUTPUT:
        //
        // RETURNS: RespuestaGenerica
        public DTO_LogOutResponse DATA_CierreSesion(CierreSesion Value, Session Session)
        {
            DTO_LogOutResponse _response;
            String _text;
            try
            {
                _response = GestorErrores.GetMsgException(ResponseCode.DATA_RC_INTERNAL_DATA_ERROR, ResponseCode.DATA_RC_INTERNAL_DATA_ERROR.ToString());
                if (GestorSesiones.RemoveSession(Value.IdSesion))
                {
                    _text = WSI.Common.Resource.String("STR_WS_DATA_SESSION_CLOSE", Session.Login.UserName, Session.Login.Login, Value.IdSesion);//String.Format("CLOSE SESSION [{0}]", Value.IdSesion);

                    GestorLogs.LogSoap(((Int16)LogID.WS_LOG), _text);// GestorLogs.LogMessage(_text);
                    GestorLogs.LogInfo(WSI.Common.AlarmSourceCode.WebService,
                                        0,
                                        WSI.Common.Resource.String("STR_WS_DATA_APP_NAME"),
                                        WSI.Common.AlarmCode.WebService_Session_End,
                                        _text, Session.OperadorId);

                    _response = new ProvideResponses().ProvideValidResponse();
                }
                else
                {
                    _text = WSI.Common.Resource.String("STR_WS_DATA_ERROR_SESSION_CLOSE", Value.IdSesion);// String.Format("INTENTO CERRAR SESSION INEXISTENTE[{0}]", Value.IdSesion);

                    GestorLogs.LogSoap(((Int16)LogID.WS_LOG), _text); //GestorLogs.LogMessage(_text);

                    GestorLogs.LogAlarm(WSI.Common.AlarmSourceCode.WebService,
                                        0,
                                        WSI.Common.Resource.String("STR_WS_DATA_APP_NAME"),
                                        WSI.Common.AlarmCode.WebService_Session_End,
                                        _text, Session.OperadorId, 0, 0);
                    _response = GestorErrores.GetMsgException(ResponseCode.DATA_RC_BAD_MESSAGE_TYPE, WSI.Common.Resource.String("STR_WS_DATA_ERROR_NOT_ACTIVE_SESSION"));//"Sin sesión activa");
                }

                return _response;
            }
            catch (DataServiceException DataServiceEx)
            {
                throw DataServiceEx;
            }
            catch (Exception Ex)
            {
                GestorLogs.LogException(Ex.Message, Ex, Session.OperadorId);
                throw new DataServiceException(ResponseCode.DATA_RC_INTERNAL_DATA_ERROR, ResponseCode.DATA_RC_INTERNAL_DATA_ERROR.ToString());
            }
        } //DATA_CierreSesion
        #endregion
    }
}
