﻿////------------------------------------------------------------------------------
//// Copyright © 2012 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: GestorMensajes.cs
//// 
////      DESCRIPTION: Management methods InicioSesion,CatalogosOperador,CierreSesion,TareasOperador
//// 
////           AUTHOR: Carlos Rodrigo
//// 
////    CREATION DATE: 27-MAY-2015
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 27-MAY-2015 CSR    First release.
////------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using DATA.OBJ.DTO;
using System.Data;
using DATA.DAL;
using DATA.OBJ;
using System.ServiceModel.Channels;
using System.ServiceModel;
using WSI.Common;
using System.Configuration;
using System.Data.SqlClient;
namespace DATA.BL.Mensajes
{
    public partial class GestorMensajes
    {
        #region Public Methods
        //------------------------------------------------------------------------------
        // PURPOSE: return the IResponse for DATA_TareasOperador
        //
        //  PARAMS:
        //      - INPUT: 
        //          - TareasOperador
        //          - Session
        //
        //      - OUTPUT:
        //
        // RETURNS: IResponse
        public DTO_GetLayoutSiteAssignedTasksResponse BL_GetAssignedTasks(Session Session)
        {
            DataTable _asigned_task_table;
            ConversorMensajes _conversor;
            ProvideResponses _provider;

            DateTime _ini_date;

            try
            {
                _asigned_task_table = new DataTable();
                _conversor = new ConversorMensajes();
                _ini_date = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                _provider = new ProvideResponses();

                _asigned_task_table = DbUnitarios.DataBaseGetLayoutSiteAssignedTasks(Session.LoginId);

                DTO_GetLayoutSiteAssignedTasksResponse _response = _provider.ProvideGetLayoutSiteAssignedTasksResponse(Session.SessionId);
                _response.AssignedTasks = _conversor.ProvideLayoutTasks(_asigned_task_table);
                Session.LayoutTask = _response.AssignedTasks;
                GestorSesiones.SaveSession(Session.SessionId, MessageType.DATA_TareasOperador);

                return (_response);
            }
            catch (DataServiceException DataServiceEx)
            {
                throw DataServiceEx;
            }
            catch (Exception Ex)
            {
                GestorLogs.LogException(Ex.Message, Ex, Session.OperadorId);
                throw new DataServiceException(ResponseCode.DATA_RC_BAD_MESSAGE_DATA, WSI.Common.Resource.String("STR_WS_DATA_ERROR_TRATAR_DATOS"));
            }
        }//DATA_CatalogosOperador


        #endregion


    }
}
