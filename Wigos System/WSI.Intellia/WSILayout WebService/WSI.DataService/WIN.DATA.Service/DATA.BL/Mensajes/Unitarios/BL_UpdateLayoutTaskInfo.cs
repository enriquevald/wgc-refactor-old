﻿////------------------------------------------------------------------------------
//// Copyright © 2012 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: GestorMensajes.cs
//// 
////      DESCRIPTION: Management methods InicioSesion,CatalogosOperador,CierreSesion,TareasOperador
//// 
////           AUTHOR: Carlos Rodrigo
//// 
////    CREATION DATE: 27-MAY-2015
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 27-MAY-2015 CSR    First release.
////------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using DATA.OBJ.DTO;
using System.Data;
using DATA.DAL;
using DATA.OBJ;
using System.ServiceModel.Channels;
using System.ServiceModel;
using WSI.Common;
using System.Configuration;
using System.Data.SqlClient;


namespace DATA.BL.Mensajes
{
  public partial class GestorMensajes
  {
    #region Public Methods
    //------------------------------------------------------------------------------
    // PURPOSE: return the IResponse for DATA_TareasOperador
    //
    //  PARAMS:
    //      - INPUT: 
    //          - TareasOperador
    //          - Session
    //
    //      - OUTPUT:
    //
    // RETURNS: IResponse
    public DTO_UpdateLayoutSiteTaskInfoResponse BL_UpdateLayoutTaskInfo(LayoutTaskInfo info, Session Session)
    {
      DataTable _asigned_task_table;
      ConversorMensajes _conversor;
      ProvideResponses _provider;
      DTO_UpdateLayoutSiteTaskInfoResponse _response;

      _response = new DTO_UpdateLayoutSiteTaskInfoResponse();

      DateTime _ini_date;

      try
      {
        _asigned_task_table = new DataTable();
        _conversor = new ConversorMensajes();
        _ini_date = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
        _provider = new ProvideResponses();


        if (!DbUnitarios.IsTaskAssignedToUser(long.Parse(info.TaskId), Session.LoginId))
        {
          Log.Warning(Resource.String("STR_WS_DATA_TASK_NOT_ASSIGNED_TO_USER") + " Task = " + info.TaskId + " LoginID = " + Session.LoginId);
           _response.ResponseCode = ResponseCode.DATA_RC_BAD_MESSAGE_DATA;
           throw new DataServiceException(ResponseCode.DATA_RC_BAD_MESSAGE_DATA, Resource.String("STR_WS_DATA_TASK_NOT_ASSIGNED_TO_USER"));
        }


        DbUnitarios.DataBaseUpdateLayoutSiteTaskInfo(info, Session.LoginId, Session.Login.Login);

        _response = _provider.ProvideUpdateLayoutSiteTaskInfoResponse(Session.SessionId);


        return _response;
      }
      catch (DataServiceException DataServiceEx)
      {
        throw DataServiceEx;
      }
      catch (Exception Ex)
      {
        GestorLogs.LogException(Ex.Message, Ex, Session.OperadorId);
        throw new DataServiceException(ResponseCode.DATA_RC_BAD_MESSAGE_DATA, WSI.Common.Resource.String("STR_WS_DATA_ERROR_TRATAR_DATOS"));
      }
    }//DATA_CatalogosOperador

    public IEnumerable<String> BL_GetUsersInRoleFromTask(long taskId)
    {
      IEnumerable<String> _result = new List<String>();

      LayoutSiteTask _task = DbUnitarios.DataBaseGetTaskById(taskId);
      if (_task.AssignedUserId == 0 || _task.AssignedUserId == -1)
      {
        string _roles = GetRoleListFromAssigned(new string[] { _task.AssignedRoleId.ToString(), "16", "" });
        if (!string.IsNullOrEmpty(_roles))
          _result = DbUnitarios.DataBaseGetUserByRoleId(_roles);
      }

      return _result;
    }//BL_GetUsersInRoleFromTask
    public LayoutSiteTask BL_GetTaskById(long taskId)
    {
      LayoutSiteTask _result = DbUnitarios.DataBaseGetTaskById(taskId);

      return _result;
    }//BL_GetTaskById
    #endregion

    #region private methods

    private static string GetRoleListFromAssigned(string[] assignedRoles)
    {
      int assignedRoleId = int.Parse(assignedRoles[0]);
      int maxRoleId = int.Parse(assignedRoles[1]);
      string result = "";
      string separator = "";

      for (int i = 1; i <= maxRoleId; i *= 2)
      {
        if ((assignedRoleId & i) == i)
        {
          result += separator + i.ToString();
          separator = ",";
        }
      }

      return result;
    }//GetRoleListFromAssigned


    #endregion

  }
}
