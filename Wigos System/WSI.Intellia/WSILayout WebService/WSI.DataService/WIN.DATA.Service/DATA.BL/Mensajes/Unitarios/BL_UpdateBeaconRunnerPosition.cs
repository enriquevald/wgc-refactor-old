﻿////------------------------------------------------------------------------------
//// Copyright © 2012 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: BL_UpdateBeaconRunnerPosition.cs
//// 
////      DESCRIPTION: Manage the beacons interaction and save the information relative to the distances
//// 
////           AUTHOR: Carlos Rodrigo
//// 
////    CREATION DATE: 27-MAY-2015
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 27-MAY-2015 CSR    First release.
////------------------------------------------------------------------------------
using System;
using DATA.OBJ.DTO;
using System.Data;
using DATA.OBJ;
namespace DATA.BL.Mensajes
{
  public partial class GestorMensajes
  {
    //
    public DTO_UpdateBeaconRunnerPositionResponse BL_UpdateBeaconRunnerPosition(LayoutRunnersPosition distance, Session session)
    {
      DataTable _asigned_task_table;
      ConversorMensajes _conversor;
      ProvideResponses _provider;

      DateTime _ini_date;

      try
      {
        _asigned_task_table = new DataTable();
        _conversor = new ConversorMensajes();
        _ini_date = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
        _provider = new ProvideResponses();

        DbUnitarios.DataBaseUpdateBeaconRunnerPosition(distance, session.LoginId);

        DTO_UpdateBeaconRunnerPositionResponse _response = _provider.ProvideUpdateBeaconRunnerPositionResponse(session.SessionId);

        return _response;
      }
      catch (DataServiceException DataServiceEx)
      {
        throw DataServiceEx;
      }
      catch (Exception Ex)
      {
        GestorLogs.LogException(Ex.Message, Ex, session.OperadorId);
        throw new DataServiceException(ResponseCode.DATA_RC_BAD_MESSAGE_DATA, WSI.Common.Resource.String("STR_WS_DATA_ERROR_TRATAR_DATOS"));
      }

    }
  }


}
