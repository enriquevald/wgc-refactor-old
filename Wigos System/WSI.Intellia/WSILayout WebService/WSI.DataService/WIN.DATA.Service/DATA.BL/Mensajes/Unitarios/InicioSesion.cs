﻿////------------------------------------------------------------------------------
//// Copyright © 2012 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: GestorMensajes.cs
//// 
////      DESCRIPTION: Management methods InicioSesion,CatalogosOperador,CierreSesion
//// 
////           AUTHOR: Xavier Iglesia
//// 
////    CREATION DATE: 10-OCT-2012
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 10-OCT-2012 XIT    First release.
//// 18-OCT-2012 JVV    Modify GestionMensajeInicioSesion to call CreateSessionID()
//// 05-DEC-2012 XIT         Moved from Transaction file to call file
//// 11-JAN-2013 XIT    Added ProvideIsLogEnabled
////------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using DATA.OBJ.DTO;
using System.Data;
using DATA.DAL;
using DATA.OBJ;
using System.ServiceModel.Channels;
using System.ServiceModel;
using WSI.Common;
using System.Configuration;
namespace DATA.BL.Mensajes
{
  public partial class GestorMensajes
  {
    #region Public Methods
    //------------------------------------------------------------------------------
    // PURPOSE: return the Response for InicioSesion
    //
    //  PARAMS:
    //      - INPUT: 
    //          - InicioSesion
    //
    //      - OUTPUT:
    //
    // RETURNS: RespuestaInicioSesion
    public DTO_LogInResponse DATA_InicioSesion(InicioSesion Value)
    {
      ProvideResponses _provider;
      ConversorMensajes _converter;
      long _user_id;
      DTO_LogInResponse _response;
      List<Establecimiento> _places;
      Int64 _operatorId;
      Int64 _loginId;
      Boolean _enable_log;
      try
      {
        _enable_log = false;
        _operatorId = 0;
        _loginId = 0;
        _provider = new ProvideResponses();
        _converter = new ConversorMensajes();
        _user_id = DbUnitarios.DatabaseInicioSesion(Value);

        if (_user_id > 0)
        {
          _response = _provider.ProvideRespuestaInicioSesion();
          _places = null;
          _loginId = _user_id;
          _enable_log = true;

          _response.AssignedTasks = _converter.ProvideLayoutTasks(DbUnitarios.DataBaseGetLayoutSiteAssignedTasks(_loginId));
          _response.Roles = DbUnitarios.DatabaseGetRolLayoutSiteByLoginId(_loginId);
          _response.TerminalAlarmsCreatedInTheDay = _converter.ProvideLayoutSiteAlarms(DbUnitarios.DataBaseGetAlarmsCreatedByUser(_loginId));
          _response.BeaconsAvailables = _converter.ProvideBeaconsAvailables(DbUnitarios.DataBaseGetBeaconsAvailables());
          _response.UserId = _loginId;
          _response.LastVersionApp = DbUnitarios.DataBaseGetApplicationLastestVersion();
          _response.IsTitoMode = WSI.Common.TITO.Utils.IsTitoMode();

          GestorSesiones.Create(_response, Value, _places, new List<LayoutSiteTask>(), _operatorId, _loginId, _enable_log);
        }
        else
        {
          String _text;
          _text = WSI.Common.Resource.String("STR_WS_DATA_BAD_LOGIN",
                                                                  Value.UserName,
                                                                  Value.Login,
                                                                  "XXX");

          GestorLogs.LogSoap(((Int16)LogID.WS_LOG), _text); //GestorLogs.LogMessage(_text);

          GestorLogs.LogAlarm(WSI.Common.AlarmSourceCode.WebService,
                              0,
                              WSI.Common.Resource.String("STR_WS_DATA_APP_NAME"),
                              WSI.Common.AlarmCode.WebService_Session_BadLogin,
                              _text, _operatorId, 0, 0);

          _response = GestorErrores.GetInicioSesionException(ResponseCode.DATA_RC_BAD_LOGIN, WSI.Common.Resource.String("STR_WS_DATA_ERROR_DATA_SESSION"));
        }

        return _response;
      }
      catch (DataServiceException DataServiceEx)
      {
        return GestorErrores.GetInicioSesionException((ResponseCode)DataServiceEx.ResponseCode, DataServiceEx.ResponseDescription);
      }
      catch (Exception Ex)
      {
        GestorLogs.LogException(Ex.Message, Ex);
        return GestorErrores.GetInicioSesionException(ResponseCode.DATA_RC_INTERNAL_DATA_ERROR, WSI.Common.Resource.String("STR_WS_DATA_INTERNAL_ERROR_INICIO_SESSION"));
      }
    } //DATA_InicioSesion
    #endregion
  }
}
