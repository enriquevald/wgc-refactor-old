﻿////------------------------------------------------------------------------------
//// Copyright © 2012 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: GestorMensajes.cs
//// 
////      DESCRIPTION: Manages all the methods using the specific file
//// 
////           AUTHOR: Xavier Iglesia
//// 
////    CREATION DATE: 05-DIC-2012
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 05-DIC-2012 XIT    First release.
////------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using DATA.OBJ.DTO;
using System.Data;
using DATA.DAL;
using DATA.OBJ;
using System.ServiceModel.Channels;
using System.ServiceModel;
using WSI.Common;
using System.Configuration;
using DATA.OBJ.Transactions;
using DATA.BL.Flujos;
namespace DATA.BL.Mensajes
{
  public partial class GestorMensajes
  {
    #region Fields
    DbUnitariosConnection DbUnitarios;
    DbReportesConnection DbReportes;

    #endregion

    #region Constructor
    public GestorMensajes()
    {
      DbUnitarios = new DbUnitariosConnection();
      DbReportes = new DbReportesConnection();
    }
    #endregion
    #region CommonMethods

    private void ValidateFlow(Session Session, String Fase)
    {
      String _text;
   
    }


    private Establecimiento ValidatePlace(String ClaveEstablecimiento, Session Session)
    {
      Establecimiento _place;
      _place = Session.Places.Find(x => x.Nombre == ClaveEstablecimiento);
      if (_place == null)
      {
        GestorErrores.GeneratePlaceNotFoundException(Session, WSI.Common.Resource.String("STR_WS_DATA_ERROR_ESTABLECIMIENTOS_INCORRECTOS"));
      }

      return _place;
    }

    private void ValidateExpirations(DateTime FechaReporte, Session Session, Establecimiento _place)
    {
      if ((!FlujoAplicacion.HasValidTimeReport(FechaReporte, Session.OperadorId, _place.Id)) ||
      (!FlujoAplicacion.HasValidExpirationPlace(FechaReporte, _place.InicioVigencia, _place.FinVigencia)))
      {
        GestorErrores.GenerateReportPeriodException(Session, WSI.Common.Resource.String("STR_WS_DATA_ERROR_DATE_EXPIRED"));
      }
    }

    private void ValidateExpirationLine(DateTime FechaReporte, Session Session, Linea _linea)
    {
      if (!FlujoAplicacion.HasValidExpirationLine(FechaReporte, _linea))
      {
        GestorErrores.GenerateReportPeriodException(Session, WSI.Common.Resource.String("STR_WS_DATA_ERROR_DATE_LINE_EXPIRED"));
      }

    }

    private void ValidateChecksum(String ValueChecksum, Session Session, String Msg)
    {
      if (ConfigurationManager.AppSettings["ValidateChecksum"] == "True" && ValueChecksum == "False")
      {
        GestorErrores.GenerateChecksumException(Session, WSI.Common.Resource.String("STR_WS_DATA_ERROR_BAD_CHECKSUM_LINES", Msg));
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Validate if IsValidSecuence and IsValidTransaction
    //
    //  PARAMS:
    //      - INPUT: 
    //          - SessionId
    //          - TransactionId
    //          - SecuenceId
    //
    //      - OUTPUT:
    //
    // RETURNS: True if correct throw if not correct
    public static Boolean ValidateTransactionalSesion(Session Session, String TransactionId, Int64 SecuenceId)
    {
      String _text;

      Session.LastMessageTicks = Environment.TickCount & Int32.MaxValue;

      if (!FlujoAplicacion.IsValidSecuence(Session, SecuenceId))
      {
        GestorErrores.GenerateBadSequenceException(Session, WSI.Common.Resource.String("STR_WS_DATA_ERROR_INCORRECT_IDSECUENCE"));
      }
      if (!FlujoAplicacion.IsValidTransaction(Session, TransactionId))
      {
        _text = WSI.Common.Resource.String("STR_WS_DATA_ERROR_INCORRECT_VALIDTRANSACTION",
                                              Session.Login.UserName,
                                              Session.Login.Login,
                                              Session.SessionId);
        GestorErrores.GenerateCurrentTransactionException(Session, _text, _text);

      }
      return true;
    }

    #endregion






   
  }
}
