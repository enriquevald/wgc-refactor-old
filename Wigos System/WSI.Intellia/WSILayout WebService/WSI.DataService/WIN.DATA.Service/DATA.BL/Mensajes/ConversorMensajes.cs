﻿////------------------------------------------------------------------------------
//// Copyright © 2012 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: ConversorMensajesUnitarios.cs
//// 
////      DESCRIPTION: Messages Conversor Class 
//// 
////           AUTHOR: Humbeto Braojos
//// 
////    CREATION DATE: 14-OCT-2012
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 15-OCT-2012 HBB, JCM , XIT ,JCA    First release.
//// 29-OCT-2012 XIT    AddedProvidePendingInfo
//// 02-NOV-2012 XIT    AddedProvideCatalogs
//// 21-NOV-2012 HBB    Estadísticas_Juego and Sesiones_Juego to the GetContexto method
//// 22-NOV-2012 JCM    Fixed: User Without "Catalago" of SubLine, Payment Type, or Combination couldn't loguin 
//// 11-JAN-2013 XIT    Added ProvideIsLogEnabled
////------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using DATA.OBJ.DTO;
using DATA.OBJ;
using WSI.Common;

using System.Data;
using System.Text;
using System.Linq;
using DATA.OBJ.Transactions;
using DATA.OBJ.DTO.Responses;

namespace DATA.BL.Mensajes
{
    public class ConversorMensajes
    {

        #region internal Methods

        //------------------------------------------------------------------------------
        // PURPOSE: return the places that user can provide
        //
        //  PARAMS:
        //      - INPUT: DataTable
        //          - 
        //
        //      - OUTPUT:
        //
        // RETURNS: List<Establecimiento>
        internal List<Establecimiento> ProviderPlaces(DataTable Data)
        {
            List<Establecimiento> _places;
            Establecimiento _place;
            Linea _line;

            _places = new List<Establecimiento>();
            _place = new Establecimiento();

            foreach (DataRow _row in Data.Rows)
            {
                _line = new Linea();
                _line.Id = (Int32)_row["li_linea_id"];
                _line.Nombre = (String)_row["li_nombre"];
                _line.InicioVigencia = (DateTime)_row["esl_fecha_inicio_vigencia"];
                _line.FinVigencia = (DateTime)_row["esl_fecha_termino_vigencia"];

                _line.EnableLog = false;
                if (_row["lop_enable_log"] != DBNull.Value)
                {
                    try
                    {
                        _line.EnableLog = (Boolean)_row["lop_enable_log"];
                    }
                    catch
                    { }
                }

                if (_place.Id == (Int64)_row["es_establecimiento_id"])
                {
                    _place.Lineas.Add(_line);
                }
                else
                {
                    _place = new Establecimiento();
                    _place.Lineas = new List<Linea>();
                    _place.Id = (Int64)_row["es_establecimiento_id"];
                    _place.Nombre = (String)_row["es_clave_establecimiento"];
                    _place.InicioVigencia = (DateTime)_row["es_fecha_inicio_vigencia"];
                    _place.FinVigencia = (DateTime)_row["es_fecha_termino_vigencia"];
                    _place.Lineas.Add(_line);
                    _places.Add(_place);
                }
            }

            return _places;
        }


        //------------------------------------------------------------------------------
        // PURPOSE: return valid catalogs of the user
        //
        //  PARAMS:
        //      - INPUT: DataTable
        //          - 
        //
        //      - OUTPUT:
        //
        // RETURNS: CatalogosDisponibles
        internal CatalogosDisponibles ProvideSiteTasks(DataTable Data)
        {
            CatalogosDisponibles _catalogs;
            Combinaciones _combination;
            TipoPago _pay_type;
            SubLineaNegocio _subline;

            _catalogs = new CatalogosDisponibles();
            _combination = new Combinaciones();
            _pay_type = new TipoPago();
            _subline = new SubLineaNegocio();

            foreach (DataRow _row in Data.Rows)
            {
                if (GetCatalogSubLine(_row, out _subline))
                {
                    if (!_catalogs.SubLineaNegocio.Exists(x => x.ClaveLineaNegocio == _subline.ClaveLineaNegocio
                      && x.ClaveSublineaNegocio == _subline.ClaveSublineaNegocio))
                    {
                        _catalogs.SubLineaNegocio.Add(_subline);
                    }
                }

                if (GetCatalogPayType(_row, out _pay_type))
                {
                    if (!_catalogs.TipoPago.Exists(x => x.ClaveLineaNegocio == _pay_type.ClaveLineaNegocio
                      && x.ClaveTipoPago == _pay_type.ClaveTipoPago))
                    {
                        _catalogs.TipoPago.Add(_pay_type);
                    }
                }

                if (GetCatalogCombination(_row, out _combination))
                {
                    if (!_catalogs.Combinaciones.Exists(x => x.ClaveLineaNegocio == _combination.ClaveLineaNegocio
                      && x.ClaveCombinacion == _combination.ClaveCombinacion))
                    {
                        _catalogs.Combinaciones.Add(_combination);
                    }
                }
            }

            return _catalogs;
        }


        //------------------------------------------------------------------------------
        // PURPOSE: return valid layout task of the user
        //
        //  PARAMS:
        //      - INPUT: DataTable
        //          - 
        //
        //      - OUTPUT:
        //
        // RETURNS: List of TareaLayout
        internal List<LayoutSiteTask> ProvideLayoutTasks(DataTable Data)
        {
            List<LayoutSiteTask> _tareas = new List<LayoutSiteTask>();

            foreach (DataRow _row in Data.Rows)
            {
                _tareas.Add(new LayoutSiteTask(_row));
            }

            return _tareas;
        }

        internal List<BeaconAvailable> ProvideBeaconsAvailables(DataTable Data)
        {
            List<BeaconAvailable> _beacons = new List<BeaconAvailable>();

            foreach (DataRow _row in Data.Rows)
            {
                _beacons.Add(new BeaconAvailable(_row));
            }

            return _beacons;
        }


        internal List<LayoutSiteManagerRunnerMessage> ProvideLayoutSiteConversationHistory(DataTable Data)
        {
            List<LayoutSiteManagerRunnerMessage> _history = new List<LayoutSiteManagerRunnerMessage>();

            foreach (DataRow _row in Data.Rows)
            {
                _history.Add(new LayoutSiteManagerRunnerMessage(_row));
            }

            return _history;
        }

        internal List<LayoutSiteManagerRunnerConversationItem> ProvideLayoutSiteConversationsOfToday(DataTable Data)
        {
            List<LayoutSiteManagerRunnerConversationItem> _history = new List<LayoutSiteManagerRunnerConversationItem>();

            foreach (DataRow _row in Data.Rows)
            {
                _history.Add(new LayoutSiteManagerRunnerConversationItem(_row));
            }

            return _history;
        }

        internal List<LayoutSiteManagerAvailable> ProvideLayoutSiteAvailableManagers(DataTable Data)
        {
            List<LayoutSiteManagerAvailable> _managers = new List<LayoutSiteManagerAvailable>();

            foreach (DataRow _row in Data.Rows)
            {
                _managers.Add(new LayoutSiteManagerAvailable(_row));
            }

            return _managers;
        }


        //------------------------------------------------------------------------------
        // PURPOSE: return valid layout task of the user
        //
        //  PARAMS:
        //      - INPUT: DataTable
        //          - 
        //
        //      - OUTPUT:
        //
        // RETURNS: List of TareaLayout
        internal List<LayoutSiteAlarm> ProvideLayoutSiteAlarms(DataTable Data)
        {
            List<LayoutSiteAlarm> _tareas = new List<LayoutSiteAlarm>();

            foreach (DataRow _row in Data.Rows)
            {
                _tareas.Add(new LayoutSiteAlarm(_row));
            }

            return _tareas;
        }

        //------------------------------------------------------------------------------
        // PURPOSE: return Id Operator
        //
        //  PARAMS:
        //      - INPUT: 
        //          - DataRow Data
        //
        //      - OUTPUT:
        //
        // RETURNS: Int64 ProvideOperatorId
        internal Int64 ProvideOperatorId(DataRow Data)
        {
            return Int64.Parse(Data["op_operador_id"].ToString());
        }

        //------------------------------------------------------------------------------
        // PURPOSE: return Id Login
        //
        //  PARAMS:
        //      - INPUT: 
        //          - DataRow Data
        //
        //      - OUTPUT:
        //
        // RETURNS: Int64 ProvideLoginId
        internal Int64 ProvideLoginId(DataRow Data)
        {
            return Int64.Parse(Data["lop_login_id"].ToString());
        }

        //------------------------------------------------------------------------------
        // PURPOSE: return Boolean if Log is Enabled
        //
        //  PARAMS:
        //      - INPUT: 
        //          - DataRow Data
        //
        //      - OUTPUT:
        //
        // RETURNS: Boolean
        internal Boolean ProvideIsLogEnabled(DataRow Data)
        {
            Boolean _value;
            if (Data["lg_enable_log"] == DBNull.Value || String.IsNullOrEmpty(Data["lg_enable_log"].ToString()))
            {
                _value = false;
            }
            else
            {
                _value = Boolean.Parse(Data["lg_enable_log"].ToString());
            }
            return _value;
        }





        #endregion

        #region Private Methods

        //------------------------------------------------------------------------------
        // PURPOSE: Parses Datetime to String with format (yyyy-mm-dd). Can refactor with datetime.format
        //
        //  PARAMS:
        //      - INPUT: DateTime dateTime
        //          - 
        //
        //      - OUTPUT:
        //
        // RETURNS: String
        private String GetFormattedDate(DateTime dateTime)
        {
            StringBuilder _date_string;

            _date_string = new StringBuilder();
            _date_string.Append((dateTime.Year).ToString().PadLeft(4, '0'));
            _date_string.Append("-");
            _date_string.Append((dateTime.Month).ToString().PadLeft(2, '0'));
            _date_string.Append("-");
            _date_string.Append((dateTime.Day).ToString().PadLeft(2, '0'));

            return _date_string.ToString();
        }

        //------------------------------------------------------------------------------
        // PURPOSE: Returns de String Line in internal Contexto Format
        //
        //  PARAMS:
        //      - INPUT: 
        //          - String Value
        //
        //      - OUTPUT:
        //
        // RETURNS: Contexto
        private Contexto GetContexto(String Value)
        {
            switch (Value)
            {
                case "1": return Contexto.Sesiones_Juego;
                case "2": return Contexto.Estadísticas_Juego;
                case "L001": return Contexto.L001;
                case "L002": return Contexto.L002;
                case "L003": return Contexto.L003;
                case "L004": return Contexto.L004;
                case "L005": return Contexto.L005;
                case "L006": return Contexto.L006;
                case "L007": return Contexto.L007;
                case "Estadísticas_Juego": return Contexto.Estadísticas_Juego;
                case "Sesiones_Juego": return Contexto.Sesiones_Juego;

                default: return Contexto.L002;
            }
        }

        //------------------------------------------------------------------------------
        // PURPOSE: Returns de SubLine in internal SubLineaNegocio Format
        //
        //  PARAMS:
        //      - INPUT: 
        //          - DataRow Row
        //
        //      - OUTPUT:
        //          - SubLineaNegocio SubLine
        //
        // RETURNS: Boolean // out SubLineaNegocio
        private Boolean GetCatalogSubLine(DataRow Row, out SubLineaNegocio SubLine)
        {
            SubLine = new SubLineaNegocio();

            if (Row.IsNull("csn_linea") || Row.IsNull("csn_clave_sublinea_negocio") || Row.IsNull("csn_sublinea_negocio_id") || Row.IsNull("csn_fecha_inicio_vigencia") || Row.IsNull("csn_fecha_termino_vigencia"))
            {
                return false;
            }

            SubLine.ClaveLineaNegocio = Row["csn_linea"].ToString();
            SubLine.ClaveSublineaNegocio = Row["csn_clave_sublinea_negocio"].ToString();
            SubLine.Id = Convert.ToInt32(Row["csn_sublinea_negocio_id"].ToString());
            SubLine.InicioVigencia = (DateTime)Row["csn_fecha_inicio_vigencia"];
            SubLine.FinVigencia = (DateTime)Row["csn_fecha_termino_vigencia"];


            return true;
        }

        //------------------------------------------------------------------------------
        // PURPOSE: Returns de Catalog Pay Type in internal TipoPago Format
        //
        //  PARAMS:
        //      - INPUT: 
        //          - DataRow Row
        //
        //      - OUTPUT:
        //          - TipoPago PaymentType
        //
        // RETURNS: Boolean // out TipoPago
        private Boolean GetCatalogPayType(DataRow Row, out TipoPago PaymentType)
        {
            PaymentType = new TipoPago();

            if (Row.IsNull("ctp_linea") || Row.IsNull("ctp_clave_tipo_pago") || Row.IsNull("ctp_tipo_pago_id") || Row.IsNull("ctp_fecha_inicio_vigencia") || Row.IsNull("ctp_fecha_termino_vigencia"))
            {
                return false;
            }

            PaymentType = new TipoPago()
            {
                ClaveLineaNegocio = Row["ctp_linea"].ToString(),
                ClaveTipoPago = Row["ctp_clave_tipo_pago"].ToString(),
                Id = Convert.ToInt32(Row["ctp_tipo_pago_id"].ToString()),
                InicioVigencia = (DateTime)Row["ctp_fecha_inicio_vigencia"],
                FinVigencia = (DateTime)Row["ctp_fecha_termino_vigencia"],
            };

            return true;
        }

        //------------------------------------------------------------------------------
        // PURPOSE: Returns de Catalog Combination in internal Combinaciones Format
        //
        //  PARAMS:
        //      - INPUT: 
        //          - DataRow Row
        //
        //      - OUTPUT:
        //          - Combinaciones Combination
        //
        // RETURNS: Boolean // out Combinaciones
        private Boolean GetCatalogCombination(DataRow Row, out Combinaciones Combination)
        {
            Combination = new Combinaciones();

            if (Row.IsNull("cco_linea") || Row.IsNull("cco_clave_combinacion") || Row.IsNull("cco_combinacion_id") || Row.IsNull("cco_fecha_inicio_vigencia") || Row.IsNull("cco_fecha_termino_vigencia"))
            {
                return false;
            }

            Combination = new Combinaciones()
            {
                ClaveLineaNegocio = Row["cco_linea"].ToString(),
                ClaveCombinacion = Row["cco_clave_combinacion"].ToString(),
                Id = Convert.ToInt32(Row["cco_combinacion_id"].ToString()),
                InicioVigencia = (DateTime)Row["cco_fecha_inicio_vigencia"],
                FinVigencia = (DateTime)Row["cco_fecha_termino_vigencia"],
            };

            return true;
        }


        #endregion


        
    }
}
