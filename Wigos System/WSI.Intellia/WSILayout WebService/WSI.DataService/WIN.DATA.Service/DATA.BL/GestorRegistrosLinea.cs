﻿//// Copyright © 2012 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: GestorRegistrosLinea.cs
//// 
////      DESCRIPTION: Class used to return RegistrosLinea
//// 
////           AUTHOR: Joaquin Calero
//// 
////    CREATION DATE: 10-OCT-2012
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 10-OCT-2012 JCA    First release.
//// 22-OCT-2012 JVV    Update use XmlRedaer, refactor RegistrosLineas
//// 19-NOV-2012 JVV    Add Optional fields to Node Constancia
//// 21-JAN-2013 JCA    Add CheckFields, method to validate registers TAGS, the correct order, if not exists, etc.
////------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DATA.OBJ.DTO;
using System.Xml;
using System.IO;
using DATA.OBJ;
using System.Configuration;
using System.Data;

namespace DATA.BL
{
  public class GestorRegistrosLinea
  {

    #region Constants
    // OptinalFields: separated for ',' (key sensitive)
    private static String[] OPTIONAL_FIELDS = { "Constancia", "NoMaquina", "CURP", 
                                 "DocumentoID", "NoDocumentoID", "noExterior",
                                 "noInterior", "colonia", "localidad",
                                 "referencia", "TipoCambio", "Domicilio"};

    private static String[] OPTIONAL_FIELDS_SIN_CONSTANCIA = { "TipoCambio" };



    #endregion

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE: List the optional fields in XML Node, assign true for a existing node;
    //
    //  PARAMS:
    //      - INPUT:
    //          - String StrNode
    //
    //      - OUTPUT:
    //
    // RETURNS:  Dictionary<String, Boolean> Optional_Fields
    private Dictionary<String, Boolean> CheckOptionalFields(String StrNode)
    {

      Dictionary<String, Boolean> Optional_Fields;

      Optional_Fields = new Dictionary<String, Boolean>();


      //if (StrNode.Contains("<Constancia>") || StrNode.Contains("<Constancia>"))
      if (StrNode.Contains("<Constancia>"))
      {
        foreach (string _key in OPTIONAL_FIELDS)
        {
          if (StrNode.Contains("<" + _key + ">") || StrNode.Contains("<" + _key + " />"))
          {
            Optional_Fields.Add(_key, true);
          }
          else
          {
            Optional_Fields.Add(_key, false);
          }
        }
      }
      else
      {
        Optional_Fields.Add("Constancia", false);
        foreach (string _key in OPTIONAL_FIELDS_SIN_CONSTANCIA)
        {
          if (StrNode.Contains("<" + _key + ">") || StrNode.Contains("<" + _key + " />"))
          {
            Optional_Fields.Add(_key, true);
          }
          else
          {
            Optional_Fields.Add(_key, false);
          }
        }
      }

      return Optional_Fields;
    }//CheckOptionalFields

    #endregion
  }
}
