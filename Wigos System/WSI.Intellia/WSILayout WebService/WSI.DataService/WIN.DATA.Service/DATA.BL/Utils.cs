﻿////------------------------------------------------------------------------------
//// Copyright © 2012 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: Utils.cs
//// 
////      DESCRIPTION: Object Class used to generate RandomId
//// 
////           AUTHOR: Joaquin Calero
//// 
////    CREATION DATE: 10-OCT-2012
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 10-OCT-2012 JCA    First release.
//// 17-OCT-2012 JCM  Added Crypt Class with Crypt & Decrypt Method
//// 18-OCT-2012 JVV  Added CreateSessionID/GenerateSessionID/getClient_IP_Port & Modify nomenclature
//// 06-NOV-2012 HBB  Added GetChecksum<T>(T ObjectToChecksum) a generic function
//// 03-SEP-2013 JCA  Added ClientIP to save in ESTABLECIMIENTOS table
////------------------------------------------------------------------------------
using System;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using System.ServiceModel.Channels;
using System.ServiceModel;
using WSI.Common;
using System.Runtime.Serialization;
using DATA.OBJ;

namespace DATA.BL
{
  public static class Utils
  {
    #region Constants
    private const String KEY_ENCRYPT = "sjdhHJK34837HJkeoiyasHKjh93428";

    #endregion

   #region Public Methods
    //------------------------------------------------------------------------------
    // PURPOSE: Create not existent SessionID
    //  PARAMS:
    //      - INPUT:
    //          - 
    //
    //      - OUTPUT:
    //
    // RETURNS: String _idSession
    public static String CreateSessionID()
    {
      String _rc_idsession;

      _rc_idsession = null;
      _rc_idsession = GenerateSessionID();
      while (GestorSesiones.ExistIdSession(_rc_idsession))
      {
        _rc_idsession = GenerateSessionID();
      }
      return _rc_idsession;
    }//CreateSessionID

    //------------------------------------------------------------------------------
    // PURPOSE: Generate SessionID
    //
    //  PARAMS:
    //      - INPUT:
    //          - 
    //
    //      - OUTPUT:
    //
    // RETURNS: String _idSession
    private static String GenerateSessionID()
    {
      StringBuilder _builder;
      String _rc_idsession;

      _rc_idsession = null;
      _builder = new StringBuilder();
      _builder.Append(RandomNumber(10, 9999));
      _builder.Append(RandomString(7, true));
      _builder.Append(GetClientIpPort());      
      //Encrypt returns encrypted String IdSesion 
      _rc_idsession = Utils.Encrypt(_builder.ToString());
      return _rc_idsession;
    }//GenerateSessionID

    //------------------------------------------------------------------------------
    // PURPOSE: Get client IP
    // Capture the IP to client endpoint
    //  PARAMS:
    //      - INPUT:      
    //
    //      - OUTPUT:
    //
    // RETURNS: String
    public static String GetClientIp()
    {
      String _rc_ip_port_client;
      RemoteEndpointMessageProperty _clientEndpoint;
      _rc_ip_port_client = null;
      _clientEndpoint = OperationContext.Current.IncomingMessageProperties[RemoteEndpointMessageProperty.Name] as RemoteEndpointMessageProperty;
      _rc_ip_port_client = _clientEndpoint.Address;
      return _rc_ip_port_client;
    }//GetClientIp

    //------------------------------------------------------------------------------
    // PURPOSE: Concat client IP:Port to IdSesion
    // Capture the last rang of IP and Port to client endpoint and add to IdSesion 'nnn:nnnnn'
    //  PARAMS:
    //      - INPUT:      
    //
    //      - OUTPUT:
    //
    // RETURNS: String
    public static String GetClientIpPort()
    {      
      String _rc_ip_port_client;
      RemoteEndpointMessageProperty _clientEndpoint;
      _rc_ip_port_client = null;
      _clientEndpoint= OperationContext.Current.IncomingMessageProperties[RemoteEndpointMessageProperty.Name] as RemoteEndpointMessageProperty;
      //substring obtain last rang IP (xxx.xxx.xxx.NNN) 
      _rc_ip_port_client = _clientEndpoint.Address.Substring(_clientEndpoint.Address.Length - 3) + ":" + _clientEndpoint.Port;
      return _rc_ip_port_client;
    }//GetClientIpPort

    //------------------------------------------------------------------------------
    // PURPOSE: Generates a MD5 Checksum
    //
    //  PARAMS:
    //      - INPUT:
    //          - TextToChecksum: String to get Checksum
    //
    //      - OUTPUT:
    //
    // RETURNS: Calculate Checksum
    //public static String CalculateMD5Hash(String TextToChecksum)
    //{
    //  MD5 md5Hash;

    //  md5Hash = MD5.Create();
    //  // Convert the input string to a byte array and compute the hash. 
    //  //byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(TextToChecksum));
    // // MD5 md5hash = MD5.Create();
    //  byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(TextToChecksum));
    //  // Create a new Stringbuilder to collect the bytes and create a string.
    //  StringBuilder sBuilder = new StringBuilder();
    //  // Loop through each byte of the hashed data and format each one as a hexadecimal string. 
    //  for (int i = 0; i < data.Length; i++)
    //  {
    //    sBuilder.Append(data[i].ToString("x2"));
    //  }
    //  // Return the hexadecimal string. 
    //  return sBuilder.ToString();
    //}//CalculateMD5Hash
    public static String CalculateMD5Hash(String TextToChecksum)
    {
      MD5 _md5;
      byte[] _hash;

      _md5 = MD5.Create();
      _hash = _md5.ComputeHash(Encoding.UTF8.GetBytes(TextToChecksum));

      return BitConverter.ToString(_hash).Replace("-", "").ToLowerInvariant();
    } // CalculateMD5Hash


    //------------------------------------------------------------------------------
    // PURPOSE: string Occurrence Count
    //
    //  PARAMS:
    //      - INPUT:  string Text and Pattern   
    //        
    //      - OUTPUT:
    //
    // RETURNS: String
    // 
    public static Int16 CountStringOccurrences(String Text, String Pattern)
      {
        // Loop through all instances of the string 'text'.
        Int16 _count = 0;
        Int32 _i = 0;

        while ((_i = Text.IndexOf(Pattern, _i)) != -1)
        {
          _i += Pattern.Length;
          _count++;
        }
        return _count;
      }//CountStringOccurrences

    //------------------------------------------------------------------------------
    // PURPOSE: Cast String to Hex to Encrypt
    //
    //  PARAMS:
    //      - INPUT:  string AsciiString   
    //        
    //      - OUTPUT:
    //
    // RETURNS: String
    // 
    public static String ConvertStringToHex(string AsciiString)
    {
      String _rc_hex = String.Empty;

      foreach (char _idx_char in AsciiString)
      {
        int _tmp = _idx_char;
        _rc_hex += String.Format("{0:x2}", (uint)System.Convert.ToUInt32(_tmp.ToString()));
      }
      return _rc_hex;
    }//ConvertStringToHex

    //------------------------------------------------------------------------------
    // PURPOSE: Cast Hex to String to Decrypt
    //
    //  PARAMS:
    //      - INPUT:  String HexValue
    //
    //      - OUTPUT:
    //
    // RETURNS: String
    // 
    public static String ConvertHexToString(String HexValue)
    {
      String _rc_strvalue = String.Empty;

      while (HexValue.Length > 0)
      {
        _rc_strvalue += System.Convert.ToChar(System.Convert.ToUInt32(HexValue.Substring(0, 2), 16)).ToString();
        HexValue = HexValue.Substring(2, HexValue.Length - 2);
      }
      return _rc_strvalue;
    }//ConvertHexToString


    //// PURPOSE: Encrypt a string use Encryption.Encrypt3DES
    ////  PARAMS:
    ////      - INPUT:
    ////          string ClearText
    ////
    ////      - OUTPUT:
    ////
    //// RETURNS: String  
    public static String Encrypt(string ClearText)
    {
      String _encrypt;
      String _encrypt_to_hex;

      _encrypt = null;
      _encrypt_to_hex = null;
      Encryption.Encrypt3DES(ClearText, KEY_ENCRYPT, out _encrypt);
      _encrypt_to_hex = ConvertStringToHex(_encrypt);

      return _encrypt_to_hex;
    }//Encrypt

    //// PURPOSE: Decrypt a string use Encryption.Decrypt3DES
    ////  PARAMS:
    ////      - INPUT:
    ////          string EncryptText
    ////
    ////      - OUTPUT:
    ////
    //// RETURNS: String  
    public static String Decrypt(string EncryptText)
    {
      String _decrypt;
      _decrypt = null;
      Encryption.Decrypt3DES(Utils.ConvertHexToString(EncryptText), KEY_ENCRYPT, out _decrypt);

      return _decrypt;
    }//Decrypt


    //------------------------------------------------------------------------------
    // PURPOSE: Calculate the Internal checksum(It's a generic method who can receive any type and calculate its checksum)
    //
    //  PARAMS:
    //      - INPUT:T ObjectToChecksum, String  ChecksumCliente (calculated Checksum by client)
    //          - 
    //
    //      - OUTPUT:
    //
    // RETURNS: Boolean
    public static Boolean GetChecksum<T>(T ObjectToChecksum, String  ChecksumCliente)
    {
      DataContractSerializer _ser = new DataContractSerializer(typeof(T));
      String _checksum4;
      String _checksum2;
      String _object;
      String _wwp;
      try
      {
        using (MemoryStream memoryStream = new MemoryStream())
        {
          using (StreamReader reader = new StreamReader(memoryStream))
          {
            _ser.WriteObject(memoryStream, ObjectToChecksum);
            memoryStream.Position = 0;
            _object = reader.ReadToEnd();
            _checksum4 = CalculateMD5Hash(_object);
            //si el client serialitza en 4.0
            if (ChecksumCliente == _checksum4)
            {
              return true;
            }
            //si el client serialitza en 2.0
            else
            {
              ObjectSerializableToXml(ObjectToChecksum, out _wwp);
              _checksum2 = CalculateMD5Hash(_wwp);
              if (ChecksumCliente == _checksum2)
              {
                return true;
              }
              return false;
            }            
          }
        }
      }
      catch (Exception Ex)
      {
        GestorLogs.LogException(Ex.Message, Ex);
        throw new DataServiceException(ResponseCode.DATA_RC_BAD_CHECKSUM, WSI.Common.Resource.String("STR_WS_DATA_ERROR_BAD_CHECKSUM"));
      }
    }//GetChecksum

    ////------------------------------------------------------------------------------
    //// PURPOSE: Calculate the Internal checksum(It's a generic method who can receive any type and calculate its checksum)
    ////
    ////  PARAMS:
    ////      - INPUT: T ObjectToChecksum
    ////          - 
    ////
    ////      - OUTPUT:
    ////
    //// RETURNS: String checksum
    //public static String GetChecksum<T>(T ObjectToChecksum)
    //{
    //  DataContractSerializer _ser = new DataContractSerializer(typeof(T));
    //  String _checksum;
    //  String _monitorizacion_pendiente;
    //  try
    //  {
    //    using (MemoryStream memoryStream = new MemoryStream())
    //    {
    //      using (StreamReader reader = new StreamReader(memoryStream))
    //      {
    //        _ser.WriteObject(memoryStream, ObjectToChecksum);
    //        memoryStream.Position = 0;
    //        _monitorizacion_pendiente = reader.ReadToEnd();
    //        _checksum = CalculateMD5Hash(_monitorizacion_pendiente);

    //        return _checksum;
    //      }
    //    }
    //  }
    //  catch (Exception _ex)
    //  {
    //    GestorLogs.LogException(_ex.Message, _ex);
    //    throw new DataServiceException(ResponseCode.DATA_RC_BAD_CHECKSUM,  WSI.Common.Resource.String("STR_WS_DATA_ERROR_BAD_CHECKSUM"));
    //  }
    //}

    //------------------------------------------------------------------------------
    // PURPOSE: Serialize object to string for clients .NET 2.0
    //
    //  PARAMS:
    //      - INPUT: Object XmlObject
    //          - 
    //
    //      - OUTPUT: String XmlStr
    //
    // RETURNS: Boolean

    public static Boolean ObjectSerializableToXml(Object XmlObject, out String XmlStr)
    {
      System.Xml.Serialization.XmlSerializer _xml_ser;
      StreamWriter _stream_writer;
      StreamReader _stream_reader;
      XmlStr = "";

      try
      {
        using (MemoryStream _memory_stream = new System.IO.MemoryStream())
        {
          _stream_writer = new System.IO.StreamWriter(_memory_stream);
          _stream_reader = new System.IO.StreamReader(_memory_stream);

          _xml_ser = new System.Xml.Serialization.XmlSerializer(XmlObject.GetType());
          _xml_ser.Serialize(_stream_writer, XmlObject);

          _memory_stream.Position = 0;
          XmlStr = _stream_reader.ReadToEnd();
        }
        return true;
      }
      catch (Exception _ex)
      {
        GestorLogs.LogException(_ex.Message, _ex);
        throw new DataServiceException(ResponseCode.DATA_RC_BAD_CHECKSUM,  WSI.Common.Resource.String("STR_WS_DATA_ERROR_BAD_CHECKSUM"));
      }
    } //ObjectSerializableToXml
   
    #endregion

   #region Private Methods
    //------------------------------------------------------------------------------
    // PURPOSE: Generates a random string with the given length
    //
    //  PARAMS:
    //      - INPUT:
    //          - size: Size of the string
    //          - lowerCase: If true, generate lowercase string
    //
    //      - OUTPUT:
    //
    // RETURNS: Random String
    private static string RandomString(int Size, bool LowerCase)
    {
      StringBuilder _rc_builder = new StringBuilder();
      Random _random = new Random();
      char _ch;
      for (int _idx_i = 0; _idx_i < Size; _idx_i++)
      {
        _ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * _random.NextDouble() + 65)));
        _rc_builder.Append(_ch);
      }
      if (LowerCase)
        return _rc_builder.ToString().ToLower();

      return _rc_builder.ToString();
    }//RandomString

    //------------------------------------------------------------------------------
    // PURPOSE: Generates a numeric random
    //
    //  PARAMS:
    //      - INPUT:
    //          - min: Min Value
    //          - max: Max Value
    //
    //      - OUTPUT:
    //
    // RETURNS: Random Int  
    private static int RandomNumber(int Min, int Max)
    {
      Random _rc_random;
      _rc_random = new Random();
      return _rc_random.Next(Min, Max);
    }///RandomNumber
    #endregion



  }
}
