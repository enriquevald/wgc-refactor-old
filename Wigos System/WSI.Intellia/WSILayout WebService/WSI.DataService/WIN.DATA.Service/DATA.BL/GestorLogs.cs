﻿////------------------------------------------------------------------------------
//// Copyright © 2012 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: GestorLogs.cs
//// 
////      DESCRIPTION: Object Class manage the logs 
//// 
////           AUTHOR: 
//// 
////    CREATION DATE: 10-OCT-2012
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 05-NOV-2012        First Version
//// 19-DIC-2012 JVV    Unique LOG generated
////------------------------------------------------------------------------------

namespace DATA.BL
{
  using System;
  using System.Text;
  using WSI.Common;
  using DATA.BL.Flujos;
  using DATA.OBJ;


  public class GestorLogs
  {

    public static void LogException(String Message, Exception Ex)
    {      
      if (FlujoAplicacion.m_flag_init_dbtrx)
      {
        Alarm.Register(AlarmSourceCode.WebService, 0, WSI.Common.Resource.String("STR_WS_DATA_APP_NAME"), AlarmCode.WebService_Session_InternalError, WSI.Common.Resource.String("STR_WS_DATA_APP_NAME2", Message));
      }

      if (FlujoAplicacion.m_flag_init_log)
      {
        Log.Exception(((Int16)LogID.WS_LOG), Ex);
      }
    }

    public static void LogException(String Message, Exception Ex, Int64 IdOperador)
    {
      if (FlujoAplicacion.m_flag_init_dbtrx)
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          Alarm.Register(AlarmSourceCode.WebService, 0, WSI.Common.Resource.String("STR_WS_DATA_APP_NAME"), (UInt32)AlarmCode.WebService_Session_InternalError, WSI.Common.Resource.String("STR_WS_DATA_APP_NAME2", Message), IdOperador, 0, 0, AlarmSeverity.Error, DateTime.Now, _db_trx.SqlTransaction);
          _db_trx.Commit();
        }
      }

      if (FlujoAplicacion.m_flag_init_log)
      {
        Log.Exception(((Int16)LogID.WS_LOG), Ex);
      }
    }

    public static void LogAlarm(AlarmSourceCode SourceCode, Int64 SourceId, String SourceName, AlarmCode Code, String Description)
    {
      if (FlujoAplicacion.m_flag_init_dbtrx)
      {
        Alarm.Register(SourceCode, SourceId, SourceName, Code, Description);
      }
    }

    public static void LogWarning(AlarmSourceCode SourceCode, Int64 SourceId, String SourceName, AlarmCode Code, String Description, Int64 IdOperador, Int64 IdEstablecimiento)
    {
      AlarmRegister(SourceCode, SourceId, SourceName, Code, Description, IdOperador, IdEstablecimiento, 0, AlarmSeverity.Warning);
    }

    public static void LogInfo(AlarmSourceCode SourceCode, Int64 SourceId, String SourceName, AlarmCode Code, String Description, Int64 IdOperador)
    {
      AlarmRegister(SourceCode, SourceId, SourceName, Code, Description, IdOperador, 0, 0, AlarmSeverity.Info);
    }

    public static void LogAlarm(AlarmSourceCode SourceCode, Int64 SourceId, String SourceName, AlarmCode Code, String Description, Int64 IdOperador, Int64 IdEstablecimiento, int IdLinea)
    {
      AlarmRegister(SourceCode, SourceId, SourceName, Code, Description, IdOperador, IdEstablecimiento, IdLinea, AlarmSeverity.Error);
    }

    // Log SOAP
    public static void LogSoap(Int16 LogId, String Message)
    {
      if (FlujoAplicacion.m_flag_init_log)
      {
        Log.Message(LogId, Message);
      }
    }


    private static void AlarmRegister(AlarmSourceCode SourceCode, Int64 SourceId, String SourceName, AlarmCode Code, String Description, Int64 IdOperador, Int64 IdEstablecimiento, int IdLinea,AlarmSeverity Severity)
    {
      if (FlujoAplicacion.m_flag_init_dbtrx)
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          Alarm.Register(SourceCode, SourceId, SourceName, (UInt32)Code, Description, IdOperador, IdEstablecimiento, IdLinea, Severity, DateTime.Now, _db_trx.SqlTransaction);
          _db_trx.Commit();
        }
      }
    }


  }
}
