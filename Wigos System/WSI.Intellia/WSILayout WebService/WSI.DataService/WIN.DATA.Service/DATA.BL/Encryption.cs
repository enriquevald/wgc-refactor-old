﻿//------------------------------------------------------------------------------
// Copyright © 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Encryption.cs
// 
//   DESCRIPTION: 
// 
//        AUTHOR: Rafael Xandri
// 
// CREATION DATE: 19-OCT-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 19-OCT-2012 RXM    First release 
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using System.IO.Compression;


public static class Encryption
{
  private static Encoding m_encoding = Encoding.Default;
  
  private static Byte[] Salt
  {
    get
    {
      return m_encoding.GetBytes("WinSystem000");
    }
  }
  


  //------------------------------------------------------------------------------
  // PURPOSE: Encrypt string using AES (Rijndael) and TripleDES  and compres
  //
  //  PARAMS:
  //      - INPUT: 
  //          - string
  //          - password
  //      - OUTPUT:
  //          -  Encrypted 
  //
  // RETURNS:
  //          - True: Correctly encrypted
  //          - False: Error encrypting
  //
  //   NOTES:
  public static Boolean Encript(Byte[] Body, String Password, out MemoryStream OutStream)
  {
    Rijndael _algorithm;
    TripleDES _algorithm2;
    byte[] _bytes;
    OutStream = new MemoryStream();
    _algorithm2 = TripleDES.Create();
    _algorithm = Rijndael.Create();
    _algorithm.Key = (new PasswordDeriveBytes(Password, Salt)).GetBytes(32);
    _algorithm.IV = (new PasswordDeriveBytes(Password, Salt)).GetBytes(16);
    _algorithm2.Key = (new PasswordDeriveBytes(Password, Salt)).GetBytes(16);
    _algorithm2.IV = (new PasswordDeriveBytes(Password, Salt)).GetBytes(8);

    try
    {

      // get bytes and restart _out_stream      
      OutStream = new MemoryStream();

      // first encrypt
      using (CryptoStream _crypto_stream = new CryptoStream(OutStream, _algorithm.CreateEncryptor(), CryptoStreamMode.Write))
      {
        _crypto_stream.Write(Body, 0, Body.Length);
        _crypto_stream.FlushFinalBlock();
        _crypto_stream.Close();
      }

      // get bytes and restart _out_stream
      _bytes = OutStream.ToArray();
      OutStream = new MemoryStream();

      //second encrypt
      using (CryptoStream _crypto_stream = new CryptoStream(OutStream, _algorithm2.CreateEncryptor(), CryptoStreamMode.Write))
      {
        _crypto_stream.Write(_bytes, 0, _bytes.Length);
        _crypto_stream.FlushFinalBlock();
        _crypto_stream.Close();
      }

      return true;

    }
    catch (Exception)
    {

      return false;

    }
  }

  //------------------------------------------------------------------------------
  // PURPOSE: Decrypt crypted string using AES (Rijndael) and TripleDES an unzip
  //
  //  PARAMS:
  //      - INPUT: 
  //          - crypted string
  //          - password
  //      - OUTPUT:
  //          - decrypted string
  //
  // RETURNS:
  //          - True: Correctly decrypted
  //          - False: Error decrypting
  //
  //   NOTES:
  public static Boolean DecriptAndUnZip(MemoryStream Encrypted, String Password, out  String OutString)
  {
    Rijndael _algorithm;
    TripleDES _algorithm2;
    byte[] _bytes;
    MemoryStream _out_stream;

    OutString = "";
    _algorithm2 = TripleDES.Create();
    _algorithm = Rijndael.Create();
    _algorithm.Key = (new PasswordDeriveBytes(Password, Salt)).GetBytes(32);
    _algorithm.IV = (new PasswordDeriveBytes(Password, Salt)).GetBytes(16);
    _algorithm2.Key = (new PasswordDeriveBytes(Password, Salt)).GetBytes(16);
    _algorithm2.IV = (new PasswordDeriveBytes(Password, Salt)).GetBytes(8);

    _bytes = Encrypted.ToArray();
    Encrypted.Flush();
    Encrypted.Close();
    Encrypted.Dispose();
    _out_stream = new MemoryStream();

    try
    {
      using (CryptoStream _crypto_stream = new CryptoStream(_out_stream, _algorithm2.CreateDecryptor(), CryptoStreamMode.Write))
      {
        _crypto_stream.Write(_bytes, 0, _bytes.Length);
        _crypto_stream.Flush();
        _crypto_stream.Close();
        _crypto_stream.Dispose();
        _algorithm2.Clear();
        _algorithm2.Dispose();
      }

      // get bytes and restart _out_stream
      _bytes = _out_stream.ToArray();
      _out_stream.Flush();
      _out_stream.Close();
      _out_stream = new MemoryStream();

      // first encrypt
      using (CryptoStream _crypto_stream = new CryptoStream(_out_stream, _algorithm.CreateDecryptor(), CryptoStreamMode.Write))
      {
        _crypto_stream.Write(_bytes, 0, _bytes.Length);
        _crypto_stream.Flush();
        _crypto_stream.Close();
        _crypto_stream.Dispose();
        _algorithm.Clear();
        _algorithm.Dispose();
      }

      // get bytes and restart _out_stream
      _bytes = _out_stream.ToArray();
      _out_stream = new MemoryStream();

      // decompress
      using (MemoryStream _ms_in = new MemoryStream(_bytes, 0, _bytes.Length))
      {
        using (DeflateStream _defalte_stream = new DeflateStream(_ms_in, CompressionMode.Decompress))
        {
          _defalte_stream.CopyTo(_out_stream);
          _defalte_stream.Flush();
          _defalte_stream.Close();
          _defalte_stream.Dispose();
        }
      }
      _bytes = null;
      System.GC.Collect();
      System.GC.WaitForPendingFinalizers();
      StreamReader _stream_reader;
      _out_stream.Position = 0;

      _stream_reader = new StreamReader(_out_stream);

      OutString = _stream_reader.ReadToEnd();
      //m_encoding.GetString(_out_stream.ToArray());
      return true;
    }
    catch (Exception)
    {

      return false;

    }
  }
  
  //------------------------------------------------------------------------------
  // PURPOSE: Decrypt crypted string using AES (Rijndael) and TripleDES an unzip
  //
  //  PARAMS:
  //      - INPUT: 
  //          - crypted string
  //          - password
  //      - OUTPUT:
  //          - decrypted string
  //
  // RETURNS:
  //          - True: Correctly decrypted
  //          - False: Error decrypting
  //
  //   NOTES:
  public static Boolean DecriptAndUnZipAndSave(MemoryStream Encrypted, String Password, FileStream OutFileStream)
  {
    Rijndael _algorithm;
    TripleDES _algorithm2;
    byte[] _bytes;
    MemoryStream _out_stream;

    _algorithm2 = TripleDES.Create();
    _algorithm = Rijndael.Create();
    _algorithm.Key = (new PasswordDeriveBytes(Password, Salt)).GetBytes(32);
    _algorithm.IV = (new PasswordDeriveBytes(Password, Salt)).GetBytes(16);
    _algorithm2.Key = (new PasswordDeriveBytes(Password, Salt)).GetBytes(16);
    _algorithm2.IV = (new PasswordDeriveBytes(Password, Salt)).GetBytes(8);

    _bytes = Encrypted.ToArray();
    Encrypted.Flush();
    Encrypted.Close();
    Encrypted.Dispose();

    _out_stream = new MemoryStream();

    try
    {
      using (CryptoStream _crypto_stream = new CryptoStream(_out_stream, _algorithm2.CreateDecryptor(), CryptoStreamMode.Write))
      {
        _crypto_stream.Write(_bytes, 0, _bytes.Length);
        _crypto_stream.Flush();
        _crypto_stream.Close();
        _crypto_stream.Dispose();
        _algorithm2.Clear();
        _algorithm2.Dispose();
      }

      // get bytes and restart _out_stream
      _bytes = _out_stream.ToArray();
      _out_stream.Flush();
      _out_stream.Close();
      _out_stream = new MemoryStream();

      // first encrypt
      using (CryptoStream _crypto_stream = new CryptoStream(_out_stream, _algorithm.CreateDecryptor(), CryptoStreamMode.Write))
      {
        _crypto_stream.Write(_bytes, 0, _bytes.Length);
        _crypto_stream.Flush();
        _algorithm.Clear();
        _algorithm.Dispose();
      }

      // get bytes and restart _out_stream
      _bytes = _out_stream.ToArray();

      // decompress
      using (MemoryStream _ms_in = new MemoryStream(_bytes, 0, _bytes.Length))
      {
        using (DeflateStream _defalte_stream = new DeflateStream(_ms_in, CompressionMode.Decompress))
        {
          _defalte_stream.CopyTo(OutFileStream);
          _defalte_stream.Flush();
          OutFileStream.Flush();
          OutFileStream.Close();
        }
      }
      _bytes = null;

      System.GC.Collect();
      System.GC.WaitForPendingFinalizers();

      return true;
    }
    catch (Exception)
    {

      return false;

    }
  }
  
  //------------------------------------------------------------------------------
  // PURPOSE: compres
  //
  //  PARAMS:
  //      - INPUT: 
  //      - OUTPUT:
  // RETURNS:
  //          - True: Correctly zip
  //          - False: Error zipping
  //
  //   NOTES:
  public static Boolean Compress(FileStream InStream, out MemoryStream Compressed)
  {
    Compressed = new MemoryStream();
    try
    {

      using (DeflateStream _deflate_stream = new DeflateStream(Compressed, CompressionMode.Compress))
      {
        InStream.CopyTo(_deflate_stream);
        _deflate_stream.Flush();
      }

      return true;

    }
    catch (Exception)
    {

      return false;

    }
  }

  //------------------------------------------------------------------------------
  // PURPOSE: decompres
  //
  //  PARAMS:
  //      - INPUT: 
  //      - OUTPUT:
  // RETURNS:
  //          - True: Correctly zip
  //          - False: Error zipping
  //
  //   NOTES:
  public static Boolean Decompress(MemoryStream Compressed, ref FileStream OutStream)
  {
    try
    {
      using (DeflateStream _defalte_stream = new DeflateStream(Compressed, CompressionMode.Decompress))
      {
        _defalte_stream.CopyTo(OutStream);
        _defalte_stream.Flush();
      }

      OutStream.Flush();

      return true;

    }
    catch (Exception)
    {

      return false;

    }
  }

  //------------------------------------------------------------------------------
  // PURPOSE: decompres
  //
  //  PARAMS:
  //      - INPUT: 
  //      - OUTPUT:
  // RETURNS:
  //          - True: Correctly zip
  //          - False: Error zipping
  //
  //   NOTES:
  public static Boolean Decompress(MemoryStream Compressed, out MemoryStream OutStream)
  {
    OutStream = new MemoryStream();

    try
    {
      using (DeflateStream _defalte_stream = new DeflateStream(Compressed, CompressionMode.Decompress))
      {

        _defalte_stream.CopyTo(OutStream);
        _defalte_stream.Flush();
      }

      OutStream.Flush();

      return true;

    }
    catch (Exception)
    {

      return false;

    }
  }
  

  //------------------------------------------------------------------------------
  // PURPOSE: Encrypt string using TripeDES
  //
  //  PARAMS:
  //      - INPUT: 
  //          - string
  //          - password
  //      - OUTPUT:
  //          -  Encrypted 
  //
  // RETURNS:
  //          - True: Correctly encrypted
  //          - False: Error encrypting
  //
  //   NOTES:
  public static Boolean Encrypt3DES(String Decrypted, String Password, out String Encrypted)
  {
    PasswordDeriveBytes _password_derive_bytes;
    MemoryStream _memory_stream;
    CryptoStream _crypto_stream;
    TripleDES _algorithm;

    byte[] _bytes;

    Encrypted = "";

    try
    {
      if (String.IsNullOrEmpty(Decrypted))
      {
        return false;
      }


      _bytes = m_encoding.GetBytes(Decrypted);
      _password_derive_bytes = new PasswordDeriveBytes(Password, Salt);
      _memory_stream = new MemoryStream();
      _algorithm = TripleDES.Create();
      _algorithm.Key = _password_derive_bytes.GetBytes(16);
      _algorithm.IV = _password_derive_bytes.GetBytes(8);
      _crypto_stream = new CryptoStream(_memory_stream, _algorithm.CreateEncryptor(), CryptoStreamMode.Write);
      _crypto_stream.Write(_bytes, 0, _bytes.Length);
      _crypto_stream.Close();
      byte[] encryptedData = _memory_stream.ToArray();
      Encrypted = m_encoding.GetString(encryptedData);
      return true;

    }
    catch (Exception)
    {
      return false;

    }
  }
    
  //------------------------------------------------------------------------------
  // PURPOSE: Decrypt crypted string using TripeDES
  //
  //  PARAMS:
  //      - INPUT: 
  //          - crypted string
  //          - password
  //      - OUTPUT:
  //          - decrypted string
  //
  // RETURNS:
  //          - True: Correctly decrypted
  //          - False: Error decrypting
  //
  //   NOTES:
  public static Boolean Decrypt3DES(String Encrypted, String Password, out String Decrypted)
  {
    PasswordDeriveBytes _password_derive_bytes;
    MemoryStream _memory_stream;
    CryptoStream _crypto_stream;
    TripleDES _algorithm;
    byte[] _bytes;

    Decrypted = "";

    try
    {
      if (String.IsNullOrEmpty(Encrypted))
      {
        return false;
      }
      _bytes = m_encoding.GetBytes(Encrypted);
      _password_derive_bytes = new PasswordDeriveBytes(Password, Salt);
      _memory_stream = new MemoryStream();
      _algorithm = TripleDES.Create();
      _algorithm.Key = _password_derive_bytes.GetBytes(16);
      _algorithm.IV = _password_derive_bytes.GetBytes(8);
      _crypto_stream = new CryptoStream(_memory_stream,
         _algorithm.CreateDecryptor(), CryptoStreamMode.Write);
      _crypto_stream.Write(_bytes, 0, _bytes.Length);
      _crypto_stream.Close();
      byte[] decryptedData = _memory_stream.ToArray();
      Decrypted = m_encoding.GetString(decryptedData);
      return true;
    }
    catch (Exception)
    {
      return false;

    }
  }

  //------------------------------------------------------------------------------
  // PURPOSE: Encrypt string using AES (Rijndael)
  //
  //  PARAMS:
  //      - INPUT: 
  //          - string
  //          - password
  //      - OUTPUT:
  //          -  Encrypted 
  //
  // RETURNS:
  //          - True: Correctly encrypted
  //          - False: Error encrypting
  //
  //   NOTES:
  private static Boolean EncryptAES(String Decrypted, String Password, out String Encrypted)
  {
    PasswordDeriveBytes _password_derive_bytes;
    MemoryStream _memory_stream;
    CryptoStream _crypto_stream;
    Rijndael _algorithm;
    byte[] _bytes;

    Encrypted = "";

    try
    {
      if (String.IsNullOrEmpty(Decrypted))
      {
        return false;
      }
      _bytes = m_encoding.GetBytes(Decrypted);
      _password_derive_bytes = new PasswordDeriveBytes(Password, Salt);
      _memory_stream = new MemoryStream();
      _algorithm = Rijndael.Create();
      _algorithm.Key = _password_derive_bytes.GetBytes(32);
      _algorithm.IV = _password_derive_bytes.GetBytes(16);
      _crypto_stream = new CryptoStream(_memory_stream, _algorithm.CreateEncryptor(), CryptoStreamMode.Write);
      _crypto_stream.Write(_bytes, 0, _bytes.Length);
      _crypto_stream.Close();
      byte[] encryptedData = _memory_stream.ToArray();
      Encrypted =  m_encoding.GetString(encryptedData);
      return true;

    }
    catch (Exception)
    {
      return false;

    }
  }

  //------------------------------------------------------------------------------
  // PURPOSE: Decrypt crypted string using AES (Rijndael)
  //
  //  PARAMS:
  //      - INPUT: 
  //          - crypted string
  //          - password
  //      - OUTPUT:
  //          - decrypted string
  //
  // RETURNS:
  //          - True: Correctly decrypted
  //          - False: Error decrypting
  //
  //   NOTES:
  private static Boolean DecryptAES(String Encrypted, String Password, out String Decrypted)
  {
    PasswordDeriveBytes _password_derive_bytes;
    MemoryStream _memory_stream;
    CryptoStream _crypto_stream;
    Rijndael _algorithm;
    byte[] _bytes;

    Decrypted = "";

    try
    {
      if (String.IsNullOrEmpty(Encrypted))
      {
        return false;
      }
      _bytes = m_encoding.GetBytes(Encrypted);
      _password_derive_bytes = new PasswordDeriveBytes(Password, Salt);
      _memory_stream = new MemoryStream();
      _algorithm = Rijndael.Create();
      _algorithm.Key = _password_derive_bytes.GetBytes(32);
      _algorithm.IV = _password_derive_bytes.GetBytes(16);
      _crypto_stream = new CryptoStream(_memory_stream,
         _algorithm.CreateDecryptor(), CryptoStreamMode.Write);
      _crypto_stream.Write(_bytes, 0, _bytes.Length);
      _crypto_stream.Close();
      byte[] decryptedData = _memory_stream.ToArray();
      Decrypted = m_encoding.GetString(decryptedData);
      return true;
    }
    catch (Exception)
    {
      return false;

    }
  }

  //------------------------------------------------------------------------------
  // PURPOSE: Encrypt string using AES (Rijndael) and TripleDES 
  //
  //  PARAMS:
  //      - INPUT: 
  //          - string
  //          - password
  //      - OUTPUT:
  //          -  Encrypted 
  //
  // RETURNS:
  //          - True: Correctly encrypted
  //          - False: Error encrypting
  //
  //   NOTES:
  public static Boolean EncryptMixed_1(String Decrypted, String Password, out String Encrypted)
  {
    Encrypted = "";
    if (!EncryptAES(Decrypted, Password, out Encrypted))
    {
      return false;

    }
    if (!Encrypt3DES(Encrypted, Password, out Encrypted))
    {
      return false;

    }
    return true;

  }

  //------------------------------------------------------------------------------
  // PURPOSE: Decrypt crypted string using AES (Rijndael) and TripleDES
  //
  //  PARAMS:
  //      - INPUT: 
  //          - crypted string
  //          - password
  //      - OUTPUT:
  //          - decrypted string
  //
  // RETURNS:
  //          - True: Correctly decrypted
  //          - False: Error decrypting
  //
  //   NOTES:
  public static Boolean DecryptMixed_1(String Encrypted, String Password, out String Decrypted)
  {
    Decrypted = "";
    if (!Decrypt3DES(Encrypted, Password, out Decrypted))
    {
      return false;

    }
    if (!DecryptAES(Decrypted, Password, out Decrypted))
    {
      return false;

    }
    return true;

  }






}
