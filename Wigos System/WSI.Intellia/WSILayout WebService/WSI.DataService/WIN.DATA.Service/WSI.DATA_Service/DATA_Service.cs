﻿////------------------------------------------------------------------------------
//// Copyright © 2012 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: DATA_Service.cs
//// 
////      DESCRIPTION: Class with methods published
//// 
////           AUTHOR: Joaquin Calero
//// 
////    CREATION DATE: 19-NOV-2012
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 19-NOV-2012 JCA    First release.
////------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using WSI.Common;
using System.Configuration.Install;
using System.ServiceModel;
using DATA.Service;
using System.Net;
using System.Configuration;
using System.Collections.Specialized;
using uPLibrary.Networking.M2Mqtt;


namespace WSI.DATA_Service
{
    public partial class DATA_Service : CommonService
    {
        public enum LogID
        {
            WS_LOG = 0
        }

        private const string VERSION = "18.030";
        private const string SVC_NAME = "DATAService";
        private const string PROTOCOL_NAME = "WS_DATA";
        private const int SERVICE_PORT = 8080;
        private const Boolean STAND_BY = true;
        private const string LOG_PREFIX = "WS_LOG";
        private const string CONFIG_FILE = "WSI.DATA_Configuration";
        private const ENUM_TSV_PACKET TSV_PACKET = ENUM_TSV_PACKET.TSV_PACKET_SMARTFLOOR_DATA_SERVICE;
        public ServiceHost HostWebService;
        public static Boolean m_flag_init_mqtt = false;
        public static MqttBroker broker;

        public static void Main(String[] Args)
        {
            DATA_Service _this_service;
            Init_MQTT_Broker();
            WIN.DATA.Service.WeatherService.Init();
            _this_service = new DATA_Service();
            Args = new string[] { ConfigurationManager.AppSettings["DBPrincipal"], ConfigurationManager.AppSettings["DBMirror"], ConfigurationManager.AppSettings["DBId"] };
            _this_service.Run(Args);

        } // Main

        private static void Init_MQTT_Broker()
        {
            if (!m_flag_init_mqtt)
            {
                broker = new MqttBroker();
                broker.Start();

                m_flag_init_mqtt = true;
                Log.Message("MQTT Broker Started");
            }

        }

        protected override void SetParameters()
        {
#if DEBUG
      m_service_name = SVC_NAME + DateTime.Now.Ticks;
#else
      m_service_name = SVC_NAME;
#endif
      m_protocol_name = PROTOCOL_NAME;
            m_version = VERSION;
            m_default_port = SERVICE_PORT;
            m_stand_by = STAND_BY;
            if (Debugger.IsAttached)
            {
                m_stand_by = false;
            }
            m_tsv_packet = TSV_PACKET;
            m_log_prefix = LOG_PREFIX;
            m_old_config_file = CONFIG_FILE;
        } // SetParameters

        protected override void OnServiceBeforeInit()
        {

        } // OnServiceBeforeInit


        //public DATA_Service()
        //{
        //  InitializeComponent();
        //}

        /// <summary>
        /// Starts the service
        /// </summary>
        /// <param name="ipe"></param>
        protected override void OnServiceRunning(IPEndPoint Ipe)
        {
            try
            {
                HostWebService = new ServiceHost(typeof(DATAService));
            }
            catch (Exception Ex)
            {
                if (Debugger.IsAttached)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(Ex.Message);
                    Console.ForegroundColor = ConsoleColor.Gray;
                    Console.WriteLine("Press <enter> to terminate the Application");
                    Console.ReadKey();

                }
                Log.Message(((Int16)LogID.WS_LOG), Ex.Message);
                Log.Message(((Int16)LogID.WS_LOG), "STOPPED");
            }
            //Uri baseAddress = new Uri("http ://localhost:62471/DATAService.svc");//Quitar espacio si se descomenta
            //Host = new ServiceHost(typeof(DATAService), baseAddress);
            try
            {
                //Console.Clear();

                Log.Message(((Int16)LogID.WS_LOG), "INICIO");
                if (Debugger.IsAttached)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine(SVC_NAME + " " + VERSION);
                    Console.WriteLine("Host.Open()");


                }
                HostWebService.Open();
                Log.Message(((Int16)LogID.WS_LOG), "Host.Open()");
                Log.Message(((Int16)LogID.WS_LOG), "ESTADO ACTUAL: " + HostWebService.State.ToString());

                Log.Message(((Int16)LogID.WS_LOG), "------------TO CONNECT:------------");

                foreach (Uri _uri in HostWebService.BaseAddresses)
                {
                    Log.Message(((Int16)LogID.WS_LOG), "   " + _uri.AbsoluteUri);
                }


                Log.Message(((Int16)LogID.WS_LOG), "-----------------------------------");

                Log.Message(((Int16)LogID.WS_LOG), "FINAL CODE");

                // Get the AppSettings section.
                NameValueCollection appSettings =
                   ConfigurationManager.AppSettings;

                StringBuilder _sb;
                _sb = new StringBuilder();

                String _appSettings_key;
                String _appSettings_value;

                for (int i = 0; i < appSettings.Count; i++)
                {
                    _appSettings_key = appSettings.GetKey(i);
                    _appSettings_value = appSettings[i];
                    if (_appSettings_key.ToLower() == "appversion")
                    {
                        _appSettings_value = this.m_version;
                    }
                    _sb.AppendLine(string.Format("#{0} Key: {1} Value: {2}", i, _appSettings_key, _appSettings_value));

                }
                Log.Message(((Int16)LogID.WS_LOG), _sb.ToString());

                if (Debugger.IsAttached)
                {
                    Console.WriteLine("");
                    Console.WriteLine("ESTADO ACTUAL: " + HostWebService.State.ToString());

                    Console.WriteLine("TO CONNECT:");
                    foreach (Uri _uri in HostWebService.BaseAddresses)
                    {
                        Console.WriteLine("   " + _uri.AbsoluteUri);
                    }
                    Console.WriteLine("");

                    //Console.WriteLine(System.Configuration.ConfigurationSettings.AppSettings["InsertParcial"]);

                    Console.WriteLine(_sb.ToString());

                    Console.WriteLine("");
                    Console.ForegroundColor = ConsoleColor.Gray;

                    Console.WriteLine("Press <enter> to terminate the Application");
                    Console.ReadKey();

                    if (HostWebService.State == CommunicationState.Opened || HostWebService.State == CommunicationState.Opening)
                    {
                        HostWebService.Abort();
                        Log.Message(((Int16)LogID.WS_LOG), "Host.Abort()");
                        this.OnStop();
                    }
                }

            }
            catch (Exception Ex)
            {

                if (Debugger.IsAttached)
                {
                    //GestorLogs.LogException(Ex.Message, Ex);
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(Ex.Message);
                    Console.ForegroundColor = ConsoleColor.Gray;
                    Console.WriteLine("Press <enter> to terminate the Application");
                    Console.ReadKey();

                }
                Log.Message(((Int16)LogID.WS_LOG), Ex.Message);
                Log.Message(((Int16)LogID.WS_LOG), "STOPPED");
                this.OnStop();
            }
            finally
            {
                //if (Host.State == CommunicationState.Opened || Host.State == CommunicationState.Opening || Host.State == CommunicationState.Faulted)
                //{
                //  Host.Abort();
                //  Log.Message(((Int16)LogID.LOG_SERVICE), "Host.Abort()");
                //}
            }
            Log.Message(((Int16)LogID.WS_LOG), HostWebService.State.ToString());

        } // OnServiceRunning

        //protected override void OnStart(string[] args)
        //{
        //}

        protected override void OnStop()
        {
            if (HostWebService.State == CommunicationState.Opened || HostWebService.State == CommunicationState.Opening)
            {
                HostWebService.Abort();
                Log.Message(((Int16)LogID.WS_LOG), "Host.Abort()");
            }
            base.OnStop();
            Log.Message(((Int16)LogID.WS_LOG), "base.OnStop()");
        }
    }


    [RunInstallerAttribute(true)]
    public class ProjectInstaller : Installer
    {
        private ServiceInstaller serviceInstaller;
        private ServiceProcessInstaller processInstaller;

        public ProjectInstaller()
        {
            processInstaller = new ServiceProcessInstaller();
            serviceInstaller = new ServiceInstaller();
            // Service will run under system account
            processInstaller.Account = ServiceAccount.NetworkService;
            // Service will have Start Type of Manual
            serviceInstaller.StartType = ServiceStartMode.Manual;
            // Here we are going to hook up some custom events prior to the install and uninstall
            BeforeInstall += new InstallEventHandler(BeforeInstallEventHandler);
            BeforeUninstall += new InstallEventHandler(BeforeUninstallEventHandler);
            // serviceInstaller.ServiceName = servicename;
            Installers.Add(serviceInstaller);
            Installers.Add(processInstaller);
        }

        private void BeforeInstallEventHandler(object sender, InstallEventArgs e)
        {
            //The service has a fixed name.
            serviceInstaller.ServiceName = "WSI.DATA_Service";
        }

        private void BeforeUninstallEventHandler(object sender, InstallEventArgs e)
        {
            //The service has a fixed name.
            serviceInstaller.ServiceName = "WSI.DATA_Service";
        }
    }
}

