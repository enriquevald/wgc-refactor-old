﻿////------------------------------------------------------------------------------
//// Copyright © 2012 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: IDATAService.cs
//// 
////      DESCRIPTION: Interface with methods published

//// 
////           AUTHOR: Carlos Rodrigo
//// 
////    CREATION DATE: 07-JUL-2015
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 07-JUL-2015 CSR    First release.
////------------------------------------------------------------------------------

using System.ServiceModel;
using DATA.Service.MessageObjects.Pregunta;
using DATA.Service.MessageObjects.Respuesta;
using WIN.DATA.Service.MessageObjects.Respuesta;
using WIN.DATA.Service.MessageObjects.Pregunta;
using System;
using System.ServiceModel.Web;



namespace DATA.Service
{

    [ServiceContract(Namespace = @"http://winsystemsintl.com/")]
    [RequiredParametersBehavior]
    public interface IDATAService
    {
        [OperationContract]
        DATA_LogInResponse LogIn(DATA_LogIn loginInfo);

        [OperationContract]
        DATA_LogOutResponse LogOut(DATA_LogOut logoutInfo);

        [OperationContract]
        DATA_LayoutSiteTaskAssignedResponse GetAssignedTask(DATA_GetAssignedLayoutSiteTasks session);

        [OperationContract]
        DATA_PostActionResponse UpdateLayoutSiteTaskInfo(DATA_UpdateLayoutSiteTaskInfo updatedValues);

        [OperationContract]
        DATA_PostActionResponse UploadLayoutSiteMediaAsset(DATA_LayoutSiteMediaAsset media);

        [OperationContract]
        DATA_PostActionResponse UpdateBeaconRunnerPosition(DATA_BeaconRunnerDistance beaconInfo);

        [OperationContract]
        DATA_PostActionResponse CreateAlarm(DATA_CreateAlarm alarm);

        [OperationContract]
        DATA_TerminalInfoResponse GetTerminalInfoByTerminalId(DATA_TerminalInfo terminalInfo);

        [OperationContract]
        DATA_TerminalInfoResponse GetAlarmsCreatedByUser(DATA_TerminalInfo terminalInfo);

        [OperationContract]
        DATA_GetConversationHistoryResponse GetConversationHistory(DATA_GetConversationHistoryRequest getConversationHistory);

        [OperationContract]
        DATA_SendMessageResponse SendMessageTo(DATA_MessageToRequest message);
        
        [OperationContract]
        DATA_GetConversationsOfTodayResponse GetConversationsOfToday(DATA_GetConversationsOfTodayRequest getConversationOfToday);

        [OperationContract]
        DATA_PostActionResponse KeepALive(DATA_KeepALive keepALive);

        [OperationContract]
        DATA_UpdateApplicationResponse UpdateApplication(DATA_UpdateApplicationRequest update);

        [OperationContract]
        DATA_SaveLogResponse SaveLog(DATA_SaveLogRequest request);
    }
}
