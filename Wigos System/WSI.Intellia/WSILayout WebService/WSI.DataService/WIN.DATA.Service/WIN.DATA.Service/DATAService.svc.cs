﻿using DATA.Service.MessageObjects.Respuesta;
using DATA.Service.MessageObjects.Pregunta;
using WIN.DATA.Service;
using DATA.OBJ;
using DATA.BL.Flujos;
using System.ServiceModel;
using WIN.DATA.Service.MessageObjects.Respuesta;
using WIN.DATA.Service.MessageObjects.Pregunta;
using DATA.OBJ.DTO;
using System.Collections.Generic;
using System.Configuration;
using uPLibrary.Networking.M2Mqtt;
using System;
using System.Text;
using log4net;
using WSI.Common;


namespace DATA.Service
{
    [InspectorBehavior]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall, Namespace = @"http://winsystemsintl.com/")]
    public class DATAService : IDATAService
    {
        string _mqttBrokerAddress = ConfigurationManager.AppSettings.Get("ip_broker");
        string _clientId = String.Format("DATAService_{0}", DateTime.Now.Ticks);
        static MqttClient client;
        public DATAService()
        {
            FlujoAplicacion.Init();
        }

        public DATA_LogInResponse LogIn(DATA_LogIn loginInfo)
        {
            return RequestProcess.DATA_LogIn(loginInfo);
        }

        public DATA_PostActionResponse KeepALive(DATA_KeepALive keepALive)
        {
            return RequestProcess.DATA_KeepALive(keepALive);
        }

        public DATA_LogOutResponse LogOut(DATA_LogOut logoutInfo)
        {
            return (DATA_LogOutResponse)RequestProcess.DATA_ProcessMessage(logoutInfo, MessageType.DATA_CierreSesion);
        }

        public DATA_LayoutSiteTaskAssignedResponse GetAssignedTask(DATA_GetAssignedLayoutSiteTasks session)
        {
            return RequestProcess.Data_GetAssignedTasks(session);
        }

        public DATA_PostActionResponse UpdateLayoutSiteTaskInfo(DATA_UpdateLayoutSiteTaskInfo updatedValues)
        {
            return RequestProcess.Data_UpdateLayoutSiteTaskInfo(updatedValues);
        }


        public DATA_PostActionResponse UploadLayoutSiteMediaAsset(DATA_LayoutSiteMediaAsset media)
        {
            return RequestProcess.Data_UploadLayoutSiteMediaAsset(media);
        }


        public DATA_PostActionResponse UpdateBeaconRunnerPosition(DATA_BeaconRunnerDistance media)
        {

            return RequestProcess.Data_UpdateBeaconRunnerPosition(media);
        }


        public DATA_PostActionResponse CreateAlarm(DATA_CreateAlarm alarm)
        {
            return RequestProcess.Data_CreateAlarm(alarm);
        }


        public DATA_TerminalInfoResponse GetTerminalInfoByTerminalId(DATA_TerminalInfo terminalInfo)
        {
            return RequestProcess.Data_GetTerminalInfoByTerminalId(terminalInfo);
        }


        public DATA_TerminalInfoResponse GetAlarmsCreatedByUser(DATA_TerminalInfo terminalInfo)
        {
            return RequestProcess.Data_GetAlarmsCreatedByUser(terminalInfo);
        }


        public DATA_GetConversationHistoryResponse GetConversationHistory(DATA_GetConversationHistoryRequest getConversationHistory)
        {
            return RequestProcess.Data_GetConversationHistory(getConversationHistory);
        }

        public DATA_GetConversationsOfTodayResponse GetConversationsOfToday(DATA_GetConversationsOfTodayRequest getConversationOfToday)
        {
            return RequestProcess.Data_GetConversationsOfToday(getConversationOfToday);
        }

        public DATA_SendMessageResponse SendMessageTo(DATA_MessageToRequest message)
        {
            DATA_SendMessageResponse response = RequestProcess.SendMessageTo(message);

            try
            {
                client = new MqttClient(_mqttBrokerAddress);
                client.Connect(_clientId);
                Log.Message(String.Format("WEBSERVICE - Successfully connect MQTT Client {0} to {1} broker address", client.ClientId, _mqttBrokerAddress));
                if (response.Message != null)
                {
                    string NOTIFICATION_TOPIC = String.Format("/Messages/{0}/{1}/{2}", message.ManagerId, response.Message.RunnerId, response.Message.RunnerUserName);
                    client.Publish(NOTIFICATION_TOPIC, Encoding.UTF8.GetBytes(message.Message + "+#" + response.Message.Id));
                    Log.Message(String.Format("WEBSERVICE - Successfully publish message to {0} topic", NOTIFICATION_TOPIC));
                }
            }
            catch (Exception e)
            {
                Log.Message(String.Format("WEBSERVICE - Error on MQTT action - Message: {0}| InnerException: {1}", e.Message, e.InnerException));
                throw;
            }
            return response;
        }


        public DATA_UpdateApplicationResponse UpdateApplication(DATA_UpdateApplicationRequest update)
        {
            return RequestProcess.Data_UpdateApplication(update);
        }

        public DATA_SaveLogResponse SaveLog(DATA_SaveLogRequest request) {

            return RequestProcess.Data_SaveLog(request);

        }
    }
}
