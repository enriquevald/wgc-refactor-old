﻿////------------------------------------------------------------------------------
//// Copyright © 2012 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: ValidacionSOAP.cs
//// 
////      DESCRIPTION: Object Class used to intercept the SOAP IN / OUT of Service
//// 
////           AUTHOR: Jordi vera
//// 
////    CREATION DATE: 05-OCT-2012
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 05-OCT-2012 JVV    First release.
//// 17-OCT-2012 JCM    Log Recived and Sended Message
//// 28-NOV-2012 JCM    Extract Soap from Envelope
//// 19-DIC-2012 JVV    Unique LOG generated
////------------------------------------------------------------------------------
using System;
using System.ServiceModel.Dispatcher;
using System.ServiceModel.Channels;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Configuration;
using System.IO;
using DATA.BL;
using System.Web;
using WSI.Common;
using System.Text;
using System.Configuration;
using DATA.OBJ;
using DATA.BL.Flujos;
using System.Xml;


namespace DATA.Service
{

  //------------------------------------------------------------------------------
  // PURPOSE: Implementation that allows custom inspection or modification of application messages incoming and outgoing service applications
  //
  public class MessageInspector : IDispatchMessageInspector
  {
    public static String ExtractSessionId(String Body)
    {
      Int32 _value_start;
      Int32 _value_end;

      _value_start = Body.IndexOf("IdSesion>");
      
      if (_value_start < 0)
      {
        return "";
      }

      _value_start += 9;
      _value_end = Body.IndexOf("</", _value_start);
      
      return Body.Substring(_value_start, _value_end - _value_start);
    }

    public static String ExtractBodyContentFromSoap(String SoapMessage)
    {
      Int32 _body_node_start;
      Int32 _body_content_starts = 0;
      Int32 _body_content_ends = 0;
      String _body_content;
      String _prefix = "";

      XmlDocument _doc;
      XmlNode _soap_node;

      _body_content = "";

      if (SoapMessage == null || SoapMessage.Length <= 0)
      {
        return "";
      }

      _doc = new XmlDocument();
      _doc.LoadXml(SoapMessage);

      _prefix = "";
      for (int i = 0; i < _doc.ChildNodes.Count; i++)
      {
        _soap_node = _doc.ChildNodes[i];
        _prefix = _soap_node.GetPrefixOfNamespace("http://schemas.xmlsoap.org/soap/envelope/");

        if (_prefix != null && _prefix.Length > 0)
        {
          break;
        }
      }

      if (_prefix == null || _prefix.Length <= 0)
      {
        //prefix not found 
        return "";
      }

      _body_node_start = SoapMessage.IndexOf("<" + _prefix + ":Body");

      _body_content_starts = SoapMessage.IndexOf(">", _body_node_start) + 3;
      _body_content_ends = SoapMessage.IndexOf("</" + _prefix + ":Body>") - 1;

      //_body = SoapMessage.Substring(_soap_start, _soap_end - _soap_start).Replace("    <", "<");
      _body_content = SoapMessage.Substring(_body_content_starts, _body_content_ends - _body_content_starts).Replace("&lt;", "<").Replace("&gt;", ">").Replace("    <", "<");
      _body_content = _body_content.Substring(0, _body_content.LastIndexOf(">")+1);
      
      return _body_content;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Capturing the event executed after receiving the SOAP request for validation or editing
    //
    //  PARAMS:
    //      - INPUT:      
    //
    //      - OUTPUT:
    //
    // RETURNS: object
    // 
    public object AfterReceiveRequest(ref Message Request, IClientChannel Channel, InstanceContext InstanceContext)
    {

      //TODO: VALIDACION CHECKSUM. PENDIENTE
      String _body_data;
      String _session_id;
     
      _body_data = ExtractBodyContentFromSoap(Request.ToString());
      _session_id = ExtractSessionId(_body_data);
     
      if (!String.IsNullOrEmpty(_session_id))
      {        
        GestorSesiones.InsertSoapRead(_session_id, _body_data);
       
      }

      return null;
    }


    //------------------------------------------------------------------------------
    // PURPOSE: Capturing the event executed before submit reply.
    //
    //  PARAMS:
    //      - INPUT:      
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    public void BeforeSendReply(ref Message reply, object correlationState)
    {

    }

    public void LogSoapMessage(String Description, Message Msg)
    {
     
      GestorLogs.LogSoap((Int16)LogID.WS_NET_MSG, Description + Msg.ToString());
    }
  }

  public class InspectorEndPointBehavior : IEndpointBehavior
  {
    public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
    {
    }

    public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
    {
      throw new Exception("Behavior no soportado por el cliente");
    }

    public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
    {
      MessageInspector inspector = new MessageInspector();
      endpointDispatcher.DispatchRuntime.MessageInspectors.Add(inspector);
    }

    public void Validate(ServiceEndpoint endpoint)
    {
    }
  }

  public class InspectorBehaviorExtensionElement : BehaviorExtensionElement
  {
    protected override object CreateBehavior()
    {
      return new InspectorEndPointBehavior();
    }

    public override Type BehaviorType
    {
      get
      {
        return typeof(InspectorEndPointBehavior);
      }
    }
  }

  [AttributeUsage(AttributeTargets.Class)]
  public class InspectorBehavior : Attribute, IServiceBehavior
  {
    public void AddBindingParameters(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase, System.Collections.ObjectModel.Collection<ServiceEndpoint> endpoints, BindingParameterCollection bindingParameters)
    {
    }

    public void ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
    {
      for (int i = 0; i < serviceHostBase.ChannelDispatchers.Count; i++)
      {
        ChannelDispatcher channelDispatcher = serviceHostBase.ChannelDispatchers[i] as ChannelDispatcher;
        if (channelDispatcher != null)
        {
          foreach (EndpointDispatcher endpointDispatcher in channelDispatcher.Endpoints)
          {
            MessageInspector inspector = new MessageInspector();
            endpointDispatcher.DispatchRuntime.MessageInspectors.Add(inspector);
          }
        }
      }
    }

    public void Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
    {
    }
  }
}
