﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;
using System.Runtime.Serialization;
using DATA.OBJ.DTO;
using DATA.Service.MessageObjects.Respuesta;
using DATA.OBJ;

namespace WIN.DATA.Service.MessageObjects.Respuesta
{
    [DataContract(Namespace = @"http://winsystemsintl.com/")]
    public class DATA_LogInResponse : DataResponse
    {
        [DataMember]
        public List<LayoutSiteTask> AssignedTasks { get; set; }
        [DataMember]
        public List<long> Roles { get; set; }
        [DataMember]
        public List<LayoutSiteAlarm> TerminalAlarmsCreatedInTheDay { get; set; }
        [DataMember]
        public List<BeaconAvailable> BeaconsAvailables { get; set; }
        [DataMember]
        public Int64 UserId { get; set; }
        [DataMember]
        public string LastVersionApp { get; set; }
        [DataMember]
        public bool IsTitoMode { get; set; }
  
        public DATA_LogInResponse()
        {
                
        }

        public DATA_LogInResponse(DTO_LogInResponse InfoValues)
        {
            this.ResponseCode = (int)InfoValues.ResponseCode;
            this.ResponseCodeDescription = InfoValues.ResponseCodeDescription;
            this.SessionId = InfoValues.SessionId;
            this.AssignedTasks = InfoValues.AssignedTasks;
            this.Roles = InfoValues.Roles;
            this.TerminalAlarmsCreatedInTheDay = InfoValues.TerminalAlarmsCreatedInTheDay;
            this.BeaconsAvailables = InfoValues.BeaconsAvailables;
            this.UserId = InfoValues.UserId;
            this.LastVersionApp = InfoValues.LastVersionApp;
            this.IsTitoMode = InfoValues.IsTitoMode;
        }

    }
}