﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;
using System.Runtime.Serialization;
using DATA.OBJ.DTO;

namespace WIN.DATA.Service.MessageObjects.Respuesta
{
    [DataContract(Namespace = @"http://winsystemsintl.com/")]
    public class DATA_PostActionResponse : DataResponse
    {
        public DATA_PostActionResponse(IDtoResponse _response)
        {
            this.ResponseCode = (int)_response.ResponseCode;
            this.ResponseCodeDescription = _response.ResponseCodeDescription;
            this.SessionId = _response.SessionId;
        }
    }
}