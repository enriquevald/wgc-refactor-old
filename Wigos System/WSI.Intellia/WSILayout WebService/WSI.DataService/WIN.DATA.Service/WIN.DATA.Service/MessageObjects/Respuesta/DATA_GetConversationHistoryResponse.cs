﻿using DATA.OBJ.DTO;
using DATA.OBJ.DTO.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WIN.DATA.Service.MessageObjects.Respuesta
{
    [DataContract(Namespace = @"http://winsystemsintl.com/")]
    public class DATA_GetConversationHistoryResponse : DataResponse
    {
        public DATA_GetConversationHistoryResponse(DTO_GetConversationHistoryResponse _response)
        {
            this.ResponseCode = (int)_response.ResponseCode;
            this.ResponseCodeDescription = _response.ResponseCodeDescription;
            this.SessionId = _response.SessionId;
            this.Messages = _response.Messages;
        }

        [DataMember]
        public IList<LayoutSiteManagerRunnerMessage> Messages { get; set; }
    }
}