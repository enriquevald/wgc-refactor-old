﻿using DATA.OBJ.DTO;
using DATA.OBJ.DTO.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WIN.DATA.Service.MessageObjects.Respuesta
{
    [DataContract(Namespace = @"http://winsystemsintl.com/")]
    public class DATA_SendMessageResponse : DataResponse
    {
        public DATA_SendMessageResponse(DTO_MessageToResponse _response)
        {
            this.ResponseCode = (int)_response.ResponseCode;
            this.ResponseCodeDescription = _response.ResponseCodeDescription;
            this.SessionId = _response.SessionId;
            this.Message = _response.Message;
        }

        [DataMember]
        public LayoutSiteManagerRunnerMessage Message { get; set; }
    }
}