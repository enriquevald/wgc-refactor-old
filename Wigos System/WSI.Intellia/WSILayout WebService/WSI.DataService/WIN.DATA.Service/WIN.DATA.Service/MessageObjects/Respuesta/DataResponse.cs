﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DATA.Service.MessageObjects.Respuesta;
using System.ServiceModel;
using DATA.OBJ;
using System.Runtime.Serialization;

namespace WIN.DATA.Service.MessageObjects.Respuesta
{
    [DataContract(Namespace = @"http://winsystemsintl.com/")]
    public abstract class DataResponse : IDataResponse
    {
        [DataMember]
        public int ResponseCode { get; set; }
        [DataMember]
        public String ResponseCodeDescription { get; set; }
        [DataMember]
        public String SessionId { get; set; }

    }
}
