﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DATA.OBJ.DTO.Responses
{
    public class LayoutSiteTaskTerminal
    {
        public string Provider { get; set; }

        public string Name { get; set; }
    }
}
