﻿using DATA.OBJ.DTO;
using DATA.OBJ.DTO.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WIN.DATA.Service.MessageObjects.Respuesta
{
    [DataContract(Namespace = @"http://winsystemsintl.com/")]
    public class DATA_GetConversationsOfTodayResponse : DataResponse
    {
        public DATA_GetConversationsOfTodayResponse(DTO_GetConversationsOfTodayResponse _response)
        {
            this.ResponseCode = (int)_response.ResponseCode;
            this.ResponseCodeDescription = _response.ResponseCodeDescription;
            this.SessionId = _response.SessionId;
            this.Headers = _response.Headers;
            this.Managers = _response.Managers;
        }

        [DataMember]
        public IList<LayoutSiteManagerRunnerConversationItem> Headers { get; set; }
        [DataMember]
        public IList<LayoutSiteManagerAvailable> Managers { get; set; }
    }
}