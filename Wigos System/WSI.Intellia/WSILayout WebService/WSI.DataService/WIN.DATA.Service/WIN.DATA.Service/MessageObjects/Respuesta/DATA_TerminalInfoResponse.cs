﻿using DATA.OBJ.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WIN.DATA.Service.MessageObjects.Respuesta
{
    [DataContract]
    public class DATA_TerminalInfoResponse : DataResponse
    {

        public DATA_TerminalInfoResponse(DTO_GetLayoutSiteAlarmsResponse _response)
        {
            this.ResponseCode = (int)_response.ResponseCode;
            this.ResponseCodeDescription = _response.ResponseCodeDescription;
            this.SessionId = _response.SessionId;
            this.Alarms = _response.Alarms;
            this.Log = _response.Log;
        }
        [DataMember]
        public IList<LayoutSiteAlarm> Alarms { get; set; }
        [DataMember]
        public IList<String> Log { get; set; }
        
    }
}