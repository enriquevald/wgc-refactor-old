﻿////------------------------------------------------------------------------------
//// Copyright © 2012 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: IRespuesta.cs
//// 
////      DESCRIPTION: Interface used to arrange the different Object Classes used to retrieve the client responses
//// 
////           AUTHOR: Xavier Iglesia
//// 
////    CREATION DATE: 11-OCT-2012
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 11-OCT-2012 XIT    First release.
////------------------------------------------------------------------------------

using DATA.OBJ;
using System;
namespace DATA.Service.MessageObjects.Respuesta
{
  public interface IDataResponse
  {
      int ResponseCode { get; set; }
      String ResponseCodeDescription { get; set; }
      String SessionId { get; set; }


  }
}
