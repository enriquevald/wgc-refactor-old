﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using DATA.OBJ.DTO;

namespace WIN.DATA.Service.MessageObjects.Respuesta
{
  [DataContract(Namespace = @"http://winsystemsintl.com/")]
    public class DATA_LayoutSiteTaskAssignedResponse : DataResponse
    {

        public DATA_LayoutSiteTaskAssignedResponse(DTO_GetLayoutSiteAssignedTasksResponse _response)
        {
            this.ResponseCode = (int)_response.ResponseCode;
            this.ResponseCodeDescription = _response.ResponseCodeDescription;
            this.SessionId = _response.SessionId;
            this.AssignedTasks = _response.AssignedTasks;
        }
        [DataMember]
        public IList<LayoutSiteTask> AssignedTasks { get; set; }
        
    }
}