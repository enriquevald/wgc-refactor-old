﻿using DATA.OBJ.DTO.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WIN.DATA.Service.MessageObjects.Respuesta
{
    [DataContract(Namespace = @"http://winsystemsintl.com/")]
    public class DATA_SaveLogResponse : DataResponse
    {
        public DATA_SaveLogResponse(DTO_SaveLogResponse _response)
        {
            this.ResponseCode = (int) _response.ResponseCode;
            this.ResponseCodeDescription = _response.ResponseCodeDescription;
            this.SessionId = _response.SessionId;
            this.Version = _response.Version;
        }
        [DataMember]
        public String Version { get; set; }
    }
}