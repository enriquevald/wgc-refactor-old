﻿////------------------------------------------------------------------------------
//// Copyright © 2016 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: LayoutSiteTask.cs
//// 
////      DESCRIPTION: Class that represent an DATA TRANSFER OBJECT for Create Alarm Process
//// 
////           AUTHOR: Carlos Rodrigo 
//// 
////    CREATION DATE: 03-ENE-2016
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 03-ENE-2016 CSR    First release.
////------------------------------------------------------------------------------
using DATA.OBJ.DTO;
using DATA.Service.MessageObjects.Pregunta;
using System;
using System.Runtime.Serialization;

namespace WIN.DATA.Service.MessageObjects.Pregunta
{
  [DataContract(Namespace = @"http://winsystemsintl.com/")]
  public class DATA_CreateAlarm : IPregunta
  {
    [DataMember]
    public String UserId { get; set; }
    [DataMember]
    public String Description { get; set; }
    [DataMember]
    public int Severity { get; set; }
    [DataMember]
    public int SubCategory { get; set; }
    [DataMember]
    public int Category { get; set; }
    [DataMember]
    public String TerminalId { get; set; }
    [DataMember]
    public String Image { get; set; }
    [DataMember]
    public String Title { get; set; }
    [DataMember]
    public Boolean IsQrInput { get; set; }
    public String SessionId
    {
      get
      {
        return UserId;
      }
      set
      {
        UserId = value;
      }

    }

    public object GetInternalObject()
    {
      return new CreateAlarm
      {
        SessionId = SessionId,
        Description = Description,
        Severity = Severity,
        SubCategory = SubCategory,
        Category = Category,
        TerminalId = TerminalId,
        Image = Image,
        Title = Title,
        IsQrInput = IsQrInput
      };
    }

    public string GetLogInfo()
    {
      throw new NotImplementedException();
    }
  }
}