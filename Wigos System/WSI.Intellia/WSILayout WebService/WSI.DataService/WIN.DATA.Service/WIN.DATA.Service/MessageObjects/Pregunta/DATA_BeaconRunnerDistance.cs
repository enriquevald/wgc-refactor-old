﻿using DATA.OBJ.DTO;
using DATA.Service.MessageObjects.Pregunta;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WIN.DATA.Service.MessageObjects.Pregunta
{
  [DataContract(Namespace = @"http://winsystemsintl.com/")]
  public class DATA_BeaconRunnerDistance : IPregunta
  {
    [DataMember]
    public String SessionId { get; set; }
    [DataMember]
    public String DeviceId { get; set; }
    [DataMember]
    public String FloorId { get; set; }
    [DataMember]
    public String PositionX { get; set; }
    [DataMember]
    public String PositionY { get; set; }
    [DataMember]
    public String NearBeacon { get; set; }
    [DataMember]
    public String Range { get; set; }


    public object GetInternalObject()
    {
      return new LayoutRunnersPosition
      {
        PositionX = Double.Parse(PositionX, CultureInfo.InvariantCulture),
        DeviceId = DeviceId,
        PositionY = Double.Parse(PositionY, CultureInfo.InvariantCulture),
        FloorId = Int32.Parse(FloorId),
        Range = float.Parse(Range, CultureInfo.InvariantCulture),
        NearBeacon = NearBeacon
      };
    }

    public string GetLogInfo()
    {
      throw new NotImplementedException();
    }

    public DateTime FromUnixTime(long unixTimeMillis)
    {
      var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
      return epoch.AddMilliseconds(unixTimeMillis);
    }
  }
}