﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.ServiceModel;
using DATA.Service.MessageObjects.Pregunta;
using DATA.OBJ.DTO;
using System.Text;

namespace WIN.DATA.Service.MessageObjects.Pregunta
{
    [DataContract(Namespace = @"http://winsystemsintl.com/")]
    public class DATA_GetAssignedLayoutSiteTasks : IPregunta
    {
        [DataMember(IsRequired = true)]
        public String SessionId { get; set; }

        public object GetInternalObject()
        {
            return new LayoutTaskInfo();
        }

        public string GetLogInfo()
        {
            StringBuilder _log_info = new StringBuilder();
            _log_info.Append("Creación TareasOperador Object");

            return _log_info.ToString();
        }
    } 
}