﻿using DATA.Service.MessageObjects.Pregunta;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WIN.DATA.Service.MessageObjects.Pregunta
{
    [DataContract(Namespace = @"http://winsystemsintl.com/")]
    public class DATA_GetConversationsOfTodayRequest : IPregunta
    {
        [DataMember]
        public string SessionId { get; set; }

        public object GetInternalObject()
        {
            throw new NotImplementedException();
        }

        public string GetLogInfo()
        {
            throw new NotImplementedException();
        }
    }
}