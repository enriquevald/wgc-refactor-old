﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DATA.Service.MessageObjects.Pregunta;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.IO;
using DATA.OBJ.DTO;

namespace WIN.DATA.Service.MessageObjects.Pregunta
{
    [DataContract(Namespace = @"http://winsystemsintl.com/")]
    public class DATA_LayoutSiteMediaAsset : IPregunta
    {
        [DataMember(IsRequired = true)]
        public String SessionId { get; set; }

        [DataMember(IsRequired = true)]
        public String Data { get; set; }

        [DataMember]
        public String Description { get; set; }

        [DataMember(IsRequired = true)]
        public LayoutSiteMediaType Type { get; set; }

        [DataMember(IsRequired = true)]
        public LayoutSiteMediaAssetType MediaType { get; set; }
        
        [DataMember]
        public String ExternalId { get; set; }

        public object GetInternalObject()
        {
            var media = new LayoutSiteMediaAsset
            {
                Data = this.Data,
                Description = this.Description,
                Type = this.Type,
                MediaType = this.MediaType,
            };
            if (Type == LayoutSiteMediaType.Terminal)
                media.ExternalId = string.IsNullOrEmpty(this.ExternalId)? new long?() : long.Parse(this.ExternalId);

            return media;
        }

        public string GetLogInfo()
        {
            throw new NotImplementedException();
        }

        
    }
}