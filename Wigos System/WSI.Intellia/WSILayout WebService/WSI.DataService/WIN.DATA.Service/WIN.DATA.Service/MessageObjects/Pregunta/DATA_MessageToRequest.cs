﻿using DATA.OBJ.DTO;
using DATA.Service.MessageObjects.Pregunta;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WIN.DATA.Service.MessageObjects.Pregunta
{
    [DataContract(Namespace = @"http://winsystemsintl.com/")]
    public class DATA_MessageToRequest : IPregunta
    {
        [DataMember]
        public Int64 ManagerId { get; set; }
        [DataMember]
        public String Message { get; set; }
        [DataMember]
        public Int32 Source { get; set; }
        [DataMember]
        public string SessionId { get; set; }

        public object GetInternalObject()
        {
            var message = new LayoutSiteManagerRunnerMessage 
            {
                Message = Message,
                Source = (int)LayoutSiteManagerRunnerMessageSource.RUNNER
            };

            return message;
        }

        public string GetLogInfo()
        {
            throw new NotImplementedException();
        }
    }
}