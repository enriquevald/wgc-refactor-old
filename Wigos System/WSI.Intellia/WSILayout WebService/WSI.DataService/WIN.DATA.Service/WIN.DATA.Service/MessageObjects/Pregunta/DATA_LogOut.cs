﻿////------------------------------------------------------------------------------
//// Copyright © 2012 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: DATA_CierreSesion.cs
//// 
////      DESCRIPTION: Object Class used to save client request
//// 
////           AUTHOR: Xavier Iglesia
//// 
////    CREATION DATE: 10-OCT-2012
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 10-OCT-2012 XIT    First release.
////------------------------------------------------------------------------------

using System;
using System.ServiceModel;
using DATA.BL;
using DATA.OBJ;
using System.Runtime.Serialization;
using DATA.OBJ.DTO;
using WSI.Common;
using System.Text;

namespace DATA.Service.MessageObjects.Pregunta
{
  [DataContract]
  public class DATA_LogOut : IPregunta
  {
    #region Fields
    [DataMember(IsRequired = true)]
    public String SessionId { get; set; }
    #endregion

    #region Public Methods
    //------------------------------------------------------------------------------
    // PURPOSE: Retrieves the internal version of the object class
    //
    //  PARAMS:
    //      - INPUT:
    //          - 
    //
    //      - OUTPUT:
    //
    // RETURNS: DATA_CierreSesion
    //  
    public Object GetInternalObject()
    {
      CierreSesion _internal_object;

      try
      {
        _internal_object = new CierreSesion();
        _internal_object.IdSesion = TypeValidator.ValidateLengthString(this.SessionId, "IdSesion");

        return _internal_object;
      }
      catch (FormatException FEx)
      {
        GestorLogs.LogException(FEx.Message, FEx);
        GestorErrores.GenerateIncorrectFormatException(GestorSesiones.GetSessionForRegistrosLinea(this.SessionId), WSI.Common.Resource.String("STR_WS_DATA_FORMAT_ERROR",FEx.Message));
       
        throw new DataServiceException(ResponseCode.DATA_RC_BAD_MESSAGE_DATA, WSI.Common.Resource.String("STR_WS_DATA_FORMAT_ERROR",FEx.Message));
      }
      catch (DataServiceException DataServiceEx)
      {
        GestorErrores.GenerateIncorrectFormatException(GestorSesiones.GetSessionForRegistrosLinea(this.SessionId), DataServiceEx.ResponseDescription);
        throw DataServiceEx;
      }
      catch (Exception Ex)
      {
        GestorLogs.LogException(Ex.Message, Ex);
        GestorErrores.GenerateIncorrectFormatException(GestorSesiones.GetSessionForRegistrosLinea(this.SessionId), WSI.Common.Resource.String("STR_WS_DATA_BAD_CABECERA"));
      
        throw new DataServiceException(ResponseCode.DATA_RC_BAD_MESSAGE_DATA, WSI.Common.Resource.String("STR_WS_DATA_ERROR_LAUCH_SESSION"));
      }
    } //GetInternalObject

    //------------------------------------------------------------------------------
    // PURPOSE: Retrieves the internal version of the object class to write log
    //
    //  PARAMS:
    //      - INPUT:
    //          - 
    //
    //      - OUTPUT:
    //
    // RETURNS: GetLogInfo
    public String GetLogInfo()
    {
      StringBuilder _log_info = new StringBuilder();
      _log_info.Append("IdSesion:" + this.SessionId);
      return _log_info.ToString();
    }
    #endregion
  }
}
