﻿using DATA.OBJ.DTO;
using DATA.Service.MessageObjects.Pregunta;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WIN.DATA.Service.MessageObjects.Pregunta
{
  [DataContract(Namespace = @"http://winsystemsintl.com/")]
  public class DATA_SaveLogRequest : IPregunta
  {
    [DataMember]
    public DateTime DateTime { get; set; }
    [DataMember]
    public String Source { get; set; }
    [DataMember]
    public String DeviceId { get; set; }
    [DataMember]
    public String UserId { get; set; }
    [DataMember]
    public String Content { get; set; }
    [DataMember]
    public String SessionId { get; set; }

    public object GetInternalObject()
    {
      return new SaveLog
      {
        SessionId = SessionId,
        DateTime = DateTime,
        Source = Source,
        DeviceId = DeviceId,
        UserId = UserId ,
        Content = Content
      };
    }

    public string GetLogInfo()
    {
      throw new NotImplementedException();
    }
  }
}