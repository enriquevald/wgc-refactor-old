﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DATA.Service.MessageObjects.Pregunta;
using System.Runtime.Serialization;
using System.ServiceModel;
using DATA.OBJ.DTO;

namespace WIN.DATA.Service.MessageObjects.Pregunta
{
  [DataContract(Namespace = @"http://winsystemsintl.com/")]
  public class DATA_UpdateLayoutSiteTaskInfo : IPregunta
  {
    [DataMember(IsRequired = true)]
    public String Reason { get; set; }

    [DataMember(IsRequired = true)]
    public String SessionId { get; set; }

    [DataMember(IsRequired = true)]
    public String TaskId { get; set; }


    [DataMember(IsRequired = true)]
    public int Action { get; set; }


    public object GetInternalObject()
    {
      return new LayoutTaskInfo
      {
        SesionId = this.SessionId,
        TaskId = this.TaskId,
        Action = ConvertToLayoutSiteTaskAction(this.Action),
        Reason = this.Reason

      };
    }

    private LayoutSiteTaskActions ConvertToLayoutSiteTaskAction(int action)
    {
      switch (action)
      {
        case 0:
          return LayoutSiteTaskActions.None;
        case 1:
          return LayoutSiteTaskActions.Assigned;
        case 2:
          return LayoutSiteTaskActions.Accepted;
        case 3:
          return LayoutSiteTaskActions.Resolved;
        default:
          return LayoutSiteTaskActions.Scaled;
      }
    }

    public string GetLogInfo()
    {
      throw new NotImplementedException();
    }
  }
}