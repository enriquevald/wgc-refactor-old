﻿////------------------------------------------------------------------------------
//// Copyright © 2012 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: IPregunta.cs
//// 
////      DESCRIPTION: Interface used to arrange the different Object Classes used to save client requests
//// 
////           AUTHOR: Xavier Iglesia
//// 
////    CREATION DATE: 10-OCT-2012
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 10-OCT-2012 XIT    First release.
////------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DATA.Service.MessageObjects.Pregunta
{
  //------------------------------------------------------------------------------
  // PURPOSE : Interface to manage the requests received from clients
  //
  //
  public interface IPregunta
  {
    String SessionId { get; set; }
    Object GetInternalObject();
    String GetLogInfo();
  }
}
