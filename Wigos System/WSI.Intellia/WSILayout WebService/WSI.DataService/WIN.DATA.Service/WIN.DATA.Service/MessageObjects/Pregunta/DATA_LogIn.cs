﻿////------------------------------------------------------------------------------
//// Copyright © 2012 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: DATA_InicioSesion.cs
//// 
////      DESCRIPTION: Object Class used to save client request
//// 
////           AUTHOR: Xavier Iglesia
//// 
////    CREATION DATE: 10-OCT-2012
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 10-OCT-2012 XIT    First release.
//// 31-AUG-2017 PM,MV  Prevent from creating an invalid BASE64 string
////------------------------------------------------------------------------------

using System;
using DATA.OBJ.DTO;
using System.ServiceModel;
using DATA.BL;
using DATA.OBJ;
using WSI.Common;
using System.Runtime.Serialization;
using System.Security.Cryptography;
using System.Web;
using System.Text;

namespace DATA.Service.MessageObjects.Pregunta
{

  [DataContract(Namespace = @"http://winsystemsintl.com/")]
  public class DATA_LogIn
  {
    #region Fields & Properties
    [DataMember(IsRequired = true)]
    public String Login { get; set; }
    [DataMember(IsRequired = true)]
    public String IdOperador { get; set; }
    [DataMember(IsRequired = true)]
    public String Password { get; set; }
    [DataMember(IsRequired = true)]
    public String DeviceId { get; set; }
    #endregion

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Retrieves the internal version of the object class
    //
    //  PARAMS:
    //      - INPUT:
    //          - 
    //
    //      - OUTPUT:
    //
    // RETURNS: InicioSesion
    //  
    public static string Decrypt(string textToDecrypt, string key)
    {
      System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();

      RijndaelManaged rijndaelCipher = new RijndaelManaged();
      rijndaelCipher.Mode = CipherMode.CBC;
      rijndaelCipher.Padding = PaddingMode.PKCS7;

      rijndaelCipher.KeySize = 0x80;
      rijndaelCipher.BlockSize = 0x80;

      string decodedUrl = HttpUtility.UrlDecode(textToDecrypt);
      decodedUrl = decodedUrl.Replace(" ", "+"); // 2017-08-31 prevent from creating an invalid BASE64 string
      byte[] encryptedData = Convert.FromBase64String(decodedUrl);
      byte[] pwdBytes = Encoding.UTF8.GetBytes(key);
      byte[] keyBytes = new byte[0x10];
      int len = pwdBytes.Length;
      if (len > keyBytes.Length)
      {
        len = keyBytes.Length;
      }
      Array.Copy(pwdBytes, keyBytes, len);
      rijndaelCipher.Key = keyBytes;
      rijndaelCipher.IV = keyBytes;
      byte[] plainText = rijndaelCipher.CreateDecryptor().TransformFinalBlock(encryptedData, 0, encryptedData.Length);
      return encoding.GetString(plainText);
    }

    public InicioSesion GetLoginObject()
    {
      InicioSesion _internal_object;

      try
      {

          WSI.Common.PasswordPolicy policy = new PasswordPolicy();

        _internal_object = new InicioSesion();
        _internal_object.Login = TypeValidator.ValidateLengthString(this.Login, "Login");
        _internal_object.UserName = TypeValidator.ValidateLengthString(this.IdOperador, "IdOperador");
        this.Password = policy.ParsePassword(Decrypt(this.Password, "LioMessiMascheranoBarca"));
        _internal_object.Password = TypeValidator.ValidateLengthString(this.Password, "Password");
        _internal_object.DeviceId = TypeValidator.ValidateLengthString(this.DeviceId, "DeviceId");

        return _internal_object;
      }
      catch (FormatException FEx)
      {
        GestorLogs.LogException(FEx.Message, FEx);
        throw new DataServiceException(ResponseCode.DATA_RC_BAD_MESSAGE_DATA, WSI.Common.Resource.String("STR_WS_DATA_FORMAT_ERROR", FEx.Message));
      }
      catch (DataServiceException DataServiceEx)
      {
        throw DataServiceEx;
      }
      catch (Exception Ex)
      {
        GestorLogs.LogException(Ex.Message, Ex);
        throw new DataServiceException(ResponseCode.DATA_RC_BAD_MESSAGE_DATA, WSI.Common.Resource.String("STR_WS_DATA_ERROR_LAUCH_SESSION"));
      }
    }

    #endregion

  }
}
