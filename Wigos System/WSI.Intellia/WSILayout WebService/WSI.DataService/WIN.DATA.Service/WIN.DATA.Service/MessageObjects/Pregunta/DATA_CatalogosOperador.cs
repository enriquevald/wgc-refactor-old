﻿////------------------------------------------------------------------------------
//// Copyright © 2012 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: DATA_CatalogosOperador.cs
//// 
////      DESCRIPTION: Object Class used to save client request
//// 
////           AUTHOR: Xavier Iglesia
//// 
////    CREATION DATE: 10-OCT-2012
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 10-OCT-2012 XIT    First release.
//// 02-NOV-2012 JVV    Update control exceptions errors.
////------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ServiceModel;
using DATA.OBJ.DTO;
using DATA.BL;
using DATA.OBJ;
using System.Runtime.Serialization;
using WSI.Common;
using System.IO;
using System.Text;

namespace DATA.Service.MessageObjects.Pregunta
{
  [MessageContract]
  [DataContract]
  public class DATA_CatalogosOperador : IPregunta
  {
    #region Fields
    [MessageBodyMember(Order = 0)]
    [DataMember(IsRequired = true)]
    public String SessionId { get; set; }

    [MessageBodyMember(Order = 1)]
    [DataMember(IsRequired = true)]
    public String ChecksumContenido { get; set; }
    
    [MessageBodyMember(Order = 2)]
    [DataMember(IsRequired = true)]
    public DATA_Catalogos_Operador CatalogosRequeridosDelOperador { get; set; }

    #endregion

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Retrieves the internal version of the object class
    //
    //  PARAMS:
    //      - INPUT:
    //          - 
    //
    //      - OUTPUT:
    //
    // RETURNS: CatalogosOperador
    //     
    public Object GetInternalObject()
    {
      CatalogosOperador _internal_object;

      try
      {
        _internal_object = new CatalogosOperador();
        _internal_object.ChecksumContenido = TypeValidator.ValidateLengthString(this.ChecksumContenido, "ChecksumContenido");
        _internal_object.CatalogosRequeridosDelOperador.CatalogoCombinaciones = GetInternalObjectCombinaciones();
        _internal_object.CatalogosRequeridosDelOperador.CatalogoSublineaNegocio = GetInternalObjectSublineaNegocio();
        _internal_object.CatalogosRequeridosDelOperador.CatalogoTipoPago = GetInternalObjectTipoPago(_internal_object);
        _internal_object.ChecksumCalculado = Utils.GetChecksum(CatalogosRequeridosDelOperador, _internal_object.ChecksumContenido).ToString();

        return _internal_object;
      }
      catch (FormatException FEx)
      { 
        GestorLogs.LogException(FEx.Message, FEx);
        GestorErrores.GenerateIncorrectFormatException(GestorSesiones.GetSessionForRegistrosLinea(this.SessionId), WSI.Common.Resource.String("STR_WS_DATA_FORMAT_ERROR", FEx.Message));
        throw new DataServiceException(ResponseCode.DATA_RC_BAD_MESSAGE_DATA, WSI.Common.Resource.String("STR_WS_DATA_FORMAT_ERROR", FEx.Message));
      }
      catch (DataServiceException DataServiceEx)
      {
        GestorErrores.GenerateIncorrectFormatException(GestorSesiones.GetSessionForRegistrosLinea(this.SessionId), DataServiceEx.ResponseDescription);
        throw DataServiceEx;
      }
      catch (Exception Ex)
      {
        GestorLogs.LogException(Ex.Message, Ex);
        GestorErrores.GenerateIncorrectFormatException(GestorSesiones.GetSessionForRegistrosLinea(this.SessionId), WSI.Common.Resource.String("STR_WS_DATA_BAD_CABECERA"));
        throw new DataServiceException(ResponseCode.DATA_RC_BAD_MESSAGE_DATA, WSI.Common.Resource.String("STR_WS_DATA_FORMAT_ERROR", Ex.Message));
      }
    } //GetInternalObject

    //------------------------------------------------------------------------------
    // PURPOSE: Retrieves the internal version of the object class to write log
    //
    //  PARAMS:
    //      - INPUT:
    //          - 
    //
    //      - OUTPUT:
    //
    // RETURNS: GetLogInfo
    public String GetLogInfo()
    {
      StringBuilder _log_info = new StringBuilder();
      _log_info.Append("Num TipoPago:" + CatalogosRequeridosDelOperador.CatalogoTipoPago.Count + ";");
      _log_info.Append("Num Combinaciones:" + CatalogosRequeridosDelOperador.CatalogoCombinaciones.Count + ";");
      _log_info.Append("Num SublineaNegocio:" + CatalogosRequeridosDelOperador.CatalogoSublineaNegocio.Count);

      return _log_info.ToString();
    }
    #endregion

    #region Private Methods
    //------------------------------------------------------------------------------
    // PURPOSE: Get the internal CatalogoTipoPago
    //
    //  PARAMS:
    //      - INPUT:
    //          - 
    //
    //      - OUTPUT:
    //
    // RETURNS: List<OBJ.DTO.TipoPago>
    // 
    private List<OBJ.DTO.TipoPago> GetInternalObjectTipoPago(CatalogosOperador InternalObject)
    {
      List<OBJ.DTO.TipoPago> _lista_tipo_pago;

      try
      {
        _lista_tipo_pago = new List<OBJ.DTO.TipoPago>();
        foreach (TipoPago tp in this.CatalogosRequeridosDelOperador.CatalogoTipoPago)
        {
          OBJ.DTO.TipoPago _tipopago = new OBJ.DTO.TipoPago();
          _tipopago.ClaveLineaNegocio = TypeValidator.ValidateLengthString(tp.ClaveLineaNegocio, "ClaveLineaNegocio de tipo pago");
          _tipopago.ClaveTipoPago = TypeValidator.ValidateNumericKey(tp.ClaveTipoPago, "ClaveTipoPago");
          _tipopago.Descripcion = TypeValidator.ValidateLengthString(tp.Descripcion, "Descripcion de tipo pago");
          _lista_tipo_pago.Add(_tipopago);
        }

        return _lista_tipo_pago;
      }
      catch (Exception Ex)
      {
        GestorLogs.LogException(Ex.Message, Ex);
        throw new DataServiceException(ResponseCode.DATA_RC_BAD_MESSAGE_DATA, WSI.Common.Resource.String("STR_WS_DATA_ERROR_NODE","TipoPago"));
      }
    }//GetInternalObjectTipoPago

    //------------------------------------------------------------------------------
    // PURPOSE: Get the internal CatalogoSublineaNegocio
    //
    //  PARAMS:
    //      - INPUT:
    //          - 
    //
    //      - OUTPUT:
    //
    // RETURNS: List<OBJ.DTO.SubLineaNegocio>
    private List<OBJ.DTO.SubLineaNegocio> GetInternalObjectSublineaNegocio()
    {
      List<OBJ.DTO.SubLineaNegocio> _lista_sublineas;
    
      try
      {
        _lista_sublineas = new List<OBJ.DTO.SubLineaNegocio>();
        foreach (SubLineaNegocio subln in this.CatalogosRequeridosDelOperador.CatalogoSublineaNegocio)
        {
          //converting SublineaNegocio
          OBJ.DTO.SubLineaNegocio _suslinea_negocio = new OBJ.DTO.SubLineaNegocio();

          _suslinea_negocio.ClaveLineaNegocio = TypeValidator.ValidateLengthString(subln.ClaveLineaNegocio, "ClaveLineaNegocio");
          _suslinea_negocio.ClaveSublineaNegocio = TypeValidator.ValidateNumericKey(subln.ClavesublineaNegocio, "ClavesublineaNegocio");
          _suslinea_negocio.Descripcion = TypeValidator.ValidateLengthString(subln.Descripcion, "Descripcion Sublinea");

          _lista_sublineas.Add(_suslinea_negocio);
        }
      }
      catch (Exception Ex)
      {
        GestorLogs.LogException(Ex.Message, Ex);
        throw new DataServiceException(ResponseCode.DATA_RC_BAD_MESSAGE_DATA, WSI.Common.Resource.String("STR_WS_DATA_ERROR_NODE","SublineaNegocio"));
      }

      return _lista_sublineas;
    }//GetInternalObjectSublineaNegocio

    //------------------------------------------------------------------------------
    // PURPOSE: Get the internal CatalogoCombinaciones
    //
    //  PARAMS:
    //      - INPUT:
    //          - 
    //
    //      - OUTPUT:
    //
    // RETURNS: List<DATA.OBJ.DTO.Combinaciones>
    private List<DATA.OBJ.DTO.Combinaciones> GetInternalObjectCombinaciones()
    {
      List<DATA.OBJ.DTO.Combinaciones> _lista_combinaciones;

      try
      {
        _lista_combinaciones = new List<DATA.OBJ.DTO.Combinaciones>();
        //converting "CatalogosRequeridosDelOperador"
        foreach (Combinaciones comb in this.CatalogosRequeridosDelOperador.CatalogoCombinaciones)
        {
          //converting combinaciones
          DATA.OBJ.DTO.Combinaciones _combicaciones = new OBJ.DTO.Combinaciones();

          _combicaciones.ClaveCombinacion = TypeValidator.ValidateNumericKey(comb.ClaveCombinacion, "ClaveCombinacion");
          _combicaciones.ClaveLineaNegocio = TypeValidator.ValidateLengthString(comb.ClaveLineaNegocio, "ClaveLineaNegocio");
          _combicaciones.Descripcion = TypeValidator.ValidateLengthString(comb.Descripcion, "Descripcion");

          _lista_combinaciones.Add(_combicaciones);
        }

        return _lista_combinaciones;
      }
      catch (Exception Ex)
      {
        GestorLogs.LogException(Ex.Message, Ex);
        throw new DataServiceException(ResponseCode.DATA_RC_BAD_MESSAGE_DATA, WSI.Common.Resource.String("STR_WS_DATA_ERROR_NODE","Combinaciones"));
      }
    }//GetInternalObjectCombinaciones
  } 
    #endregion


}
