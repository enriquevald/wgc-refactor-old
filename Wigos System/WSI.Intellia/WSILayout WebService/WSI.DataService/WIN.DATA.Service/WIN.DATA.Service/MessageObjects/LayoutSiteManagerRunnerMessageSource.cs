﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WIN.DATA.Service.MessageObjects
{
    public enum LayoutSiteManagerRunnerMessageSource
    {
        MANAGER = 1,
        RUNNER = 2
    }
}