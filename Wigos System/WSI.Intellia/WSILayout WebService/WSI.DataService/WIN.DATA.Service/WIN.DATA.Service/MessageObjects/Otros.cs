﻿////------------------------------------------------------------------------------
//// Copyright © 2012 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: 
//// 
////      DESCRIPTION: Object file used to arrange objects no primitives needed in communication
//// 
////           AUTHOR: Xavier Iglesia
//// 
////    CREATION DATE: 30-OCT-2012
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 30-OCT-2012 XIT    First release.
//// 20-DEC-2012 JCA    Add ConsultaUltimosRegsMonitorizacion
////------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;
using System.Runtime.Serialization;

namespace DATA.Service.MessageObjects.Pregunta
{

  #region Catalogos Operador

  [MessageContract]
  [DataContract]
  public class DATA_Catalogos_Operador
  {
    [MessageBodyMember(Order = 0)]
    [DataMember(IsRequired = true)]
    public List<Combinaciones> CatalogoCombinaciones { get; set; }

    [MessageBodyMember(Order = 1)]
    [DataMember(IsRequired = true)]
    public List<SubLineaNegocio> CatalogoSublineaNegocio { get; set; }

    [MessageBodyMember(Order = 2)]
    [DataMember(IsRequired = true)]
    public List<TipoPago> CatalogoTipoPago { get; set; }

  }

  [MessageContract]
  [DataContract]
  public class Combinaciones
  {

    [MessageBodyMember(Order = 0)]
    [DataMember(IsRequired = true)]
    public String ClaveCombinacion { get; set; }

    [MessageBodyMember(Order = 1)]
    [DataMember(IsRequired = true)]
    public String ClaveLineaNegocio { get; set; }

    [MessageBodyMember(Order = 2)]
    [DataMember(IsRequired = true)]
    public String Descripcion { get; set; }
  }

  [MessageContract]
  [DataContract]
  public class TipoPago
  {
    [MessageBodyMember(Order = 0)]
    [DataMember(IsRequired = true)]
    public String ClaveTipoPago { get; set; }

    [MessageBodyMember(Order = 1)]
    [DataMember(IsRequired = true)]
    public String ClaveLineaNegocio { get; set; }

    [MessageBodyMember(Order = 2)]
    [DataMember(IsRequired = true)]
    public String Descripcion { get; set; }
  }

  [MessageContract]
  [DataContract]
  public class SubLineaNegocio
  {

    [MessageBodyMember(Order = 0)]
    [DataMember(IsRequired = true)]
    public String ClaveLineaNegocio { get; set; }

    [MessageBodyMember(Order = 1)]
    [DataMember(IsRequired = true)]
    public String ClavesublineaNegocio { get; set; }

    [MessageBodyMember(Order = 2)]
    [DataMember(IsRequired = true)]
    public String Descripcion { get; set; }
  }

  #endregion

  #region Reporte Fase 1
  [MessageContract]
  [DataContract]
  public class SubLineasReportadas
  {
    [MessageBodyMember(Order = 0)]
    [DataMember(IsRequired = true)]
    public String Linea { get; set; }

    [MessageBodyMember(Order = 1)]
    [DataMember(IsRequired = true)]
    public List<SubLinea> SubLineas { get; set; }
  }
  [MessageContract]
  [DataContract]
  public class SubLinea
  {
    [MessageBodyMember(Order = 0)]
    [DataMember(IsRequired = true)]
    public String ClavesublineaNegocio { get; set; }
  }
  #endregion

  #region Reporte Fase 2

  [MessageContract]
  [DataContract]
  public class BalanceLinea
  {
    [MessageBodyMember(Order = 0)]
    [DataMember(IsRequired = true)]
    public BalanceLineaNegocio BalanceLineaNegocio { get; set; }

    [MessageBodyMember(Order = 1)]
    [DataMember(IsRequired = true)]
    public List<BalanceSubLineaNegocio> BalanceSublineasNegocio { get; set; }
  }


  [MessageContract]
  [DataContract]
  public class BalanceLineaNegocio
  {
    [MessageBodyMember(Order = 0)]
    [DataMember(IsRequired = true)]
    public String BalfinLinea { get; set; }

    [MessageBodyMember(Order = 1)]
    [DataMember(IsRequired = true)]
    public String BaliniLinea { get; set; }

    [MessageBodyMember(Order = 2)]
    [DataMember(IsRequired = true)]
    public String IngresosCaja { get; set; }

    [MessageBodyMember(Order = 3)]
    [DataMember(IsRequired = true)]
    public String SalidasCaja { get; set; }
  }

  [MessageContract]
  [DataContract]
  public class BalanceSubLineaNegocio
  {
    [MessageBodyMember(Order = 0)]
    [DataMember(IsRequired = true)]
    public String BalfinSubLinea { get; set; }

    [MessageBodyMember(Order = 1)]
    [DataMember(IsRequired = true)]
    public String BaliniSubLinea { get; set; }

    [MessageBodyMember(Order = 2)]
    [DataMember(IsRequired = true)]
    public String ClavesublineaNegocio { get; set; }

    [MessageBodyMember(Order = 3)]
    [DataMember(IsRequired = false)]
    public String IngresosCaja { get; set; }

    [MessageBodyMember(Order = 4)]
    [DataMember(IsRequired = false)]
    public String SalidasCaja { get; set; }
  }

  #endregion

  #region Monitorizacion Fase 2B
  [MessageContract]
  [DataContract]
  public class RegistroMonitorizacionEstadisticas
  {
    [MessageBodyMember(Order = 0)]
    [DataMember]
    public Int64 ContadorJugadas { get; set; }
    [MessageBodyMember(Order = 1)]
    [DataMember]
    public Int64 ContadorJugadasGanadoras { get; set; }
    [MessageBodyMember(Order = 2)]
    [DataMember]
    public String Fecha { get; set; }
    [MessageBodyMember(Order = 3)]
    [DataMember]
    public String ImporteGanado { get; set; }
    [MessageBodyMember(Order = 4)]
    [DataMember]
    public String ImporteJugado { get; set; }
    [MessageBodyMember(Order = 5)]
    [DataMember]
    public String Juego { get; set; }
    [MessageBodyMember(Order = 6)]
    [DataMember]
    public Int64 NoRegistro { get; set; }
    [MessageBodyMember(Order = 7)]
    [DataMember]
    public String NombreTerminal { get; set; }
    [MessageBodyMember(Order = 8)]
    [DataMember]
    public String ProveedorTerminal { get; set; }

    [MessageBodyMember(Order = 9)]
    [DataMember]
    public Int64 Timestamp { get; set; }
  }

  #endregion

  #region Monitorizacion Fase 2A

  [MessageContract]
  [DataContract]
  public class RegistroMonitorizacionSesiones
  {

    [MessageBodyMember(Order = 0)]
    [DataMember]
    public String BalanceFinal { get; set; }

    [MessageBodyMember(Order = 1)]
    [DataMember]
    public String BalanceInicial { get; set; }

    [MessageBodyMember(Order = 2)]
    [DataMember]
    public String CashInNoRedimible { get; set; }

    [MessageBodyMember(Order = 3)]
    [DataMember]
    public String CashInPromocionalRedimible { get; set; }

    [MessageBodyMember(Order = 4)]
    [DataMember]
    public String CashInRedimible { get; set; }

    [MessageBodyMember(Order = 5)]
    [DataMember]
    public String CashOutNoRedimible { get; set; }

    [MessageBodyMember(Order = 6)]
    [DataMember]
    public String CashOutPromocionalRedimible { get; set; }

    [MessageBodyMember(Order = 7)]
    [DataMember]
    public String CashOutRedimible { get; set; }

    [MessageBodyMember(Order = 8)]
    [DataMember]
    public Int64 ContadorJugadas { get; set; }

    [MessageBodyMember(Order = 9)]
    [DataMember]
    public Int64 ContadorJugadasGanadoras { get; set; }

    [MessageBodyMember(Order = 10)]
    [DataMember]
    public Int32 Estatus { get; set; }

    [MessageBodyMember(Order = 11)]
    [DataMember]
    public String FechaHoraCierre { get; set; }

    [MessageBodyMember(Order = 12)]
    [DataMember]
    public String FechaHoraInicio { get; set; }

    [MessageBodyMember(Order = 13)]
    [DataMember]
    public Int64 IdSesion { get; set; }

    [MessageBodyMember(Order = 14)]
    [DataMember]
    public String NoRedimibleGanado { get; set; }

    [MessageBodyMember(Order = 15)]
    [DataMember]
    public String NoRedimibleJugado { get; set; }

    [MessageBodyMember(Order = 16)]
    [DataMember]
    public Int64 NoRegistro { get; set; }

    [MessageBodyMember(Order = 17)]
    [DataMember]
    public String NombreTerminal { get; set; }

    [MessageBodyMember(Order = 18)]
    [DataMember]
    public String ProveedorTerminal { get; set; }

    [MessageBodyMember(Order = 19)]
    [DataMember]
    public String RedimibleGanado { get; set; }

    [MessageBodyMember(Order = 20)]
    [DataMember]
    public String RedimibleJugado { get; set; }

    [MessageBodyMember(Order = 21)]
    [DataMember]
    public Int64 Timestamp { get; set; }

  }

  #endregion
}

namespace DATA.Service.MessageObjects.Respuesta
{

  #region ReportesPendientes

  [MessageContract]
  [DataContract]
  public class DATA_Respuesta
  {
    [MessageBodyMember(Order = 0)]
    [DataMember]
    public String CodigoRespuesta { get; set; }
    [MessageBodyMember(Order = 1)]
    [DataMember]
    public String CodigoRespuestaDescripcion { get; set; }
  }

  [MessageContract]
  [DataContract]
  public class DiaPendiente
  {
    [MessageBodyMember(Order = 0)]
    [DataMember]
    public List<EstablecimientoPendiente> EstablecimientosPendientes { get; set; }

    [MessageBodyMember(Order = 1)]
    [DataMember]
    public String Fecha { get; set; } 
  }

  [MessageContract]
  [DataContract]
  public class EstablecimientoPendiente
  {
    [MessageBodyMember]
    [DataMember(Order = 0)]
    public String ClaveEstablecimiento;

    [MessageBodyMember]
    [DataMember(Order = 1)]
    public List<ReportePendiente> ReportesPendientes;
  }

  [MessageContract]
  [DataContract]
  public class ReportePendiente
  {
    [MessageBodyMember(Order = 0)]
    [DataMember]
    public String ContextoReporte { get; set; }
    [MessageBodyMember(Order = 1)]
    [DataMember]
    public String TipoCarencia { get; set; }
  }

  #endregion

  #region MonitorizacionesPendientes

  [MessageContract]
  [DataContract]
  public class Establecimientos
  {
    [MessageBodyMember]
    [DataMember(Order = 0)]
    public List<Establecimiento> Establecimiento;
  }

  [MessageContract]
  [DataContract]
  public class Establecimiento
  {
    [MessageBodyMember]
    [DataMember(Order = 0)]
    public String ClaveEstablecimiento;

    [MessageBodyMember]
    [DataMember(Order = 1)]
    public Int64 MaximaSesionJuego;              //me_max_id_sesion

    [MessageBodyMember]
    [DataMember(Order = 2)]
    public Int64 MaximoTimestampSesionJuego;     //me_sesion_timestamp

    [MessageBodyMember]
    [DataMember(Order = 3)]
    public DateTime? MaximaEstadisticaJuego;     //me_max_fecha_estadisticas

    [MessageBodyMember]
    [DataMember(Order = 4)]
    public Int64 MaximoTimestampEstadisticaJuego;//me_estadisticas_timestamp
  }

  #endregion

}