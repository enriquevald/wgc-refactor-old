﻿////------------------------------------------------------------------------------
//// Copyright © 2012 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: WeatherService.cs
//// 
////      DESCRIPTION: Object Class used to request and process API weather.
//// 
////           AUTHOR: Jaume Barnés
//// 
////    CREATION DATE: 05-APR-2016
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 05-APR-2016 JBC    First release.
////------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
using System.Threading;
using System.Data;
using System.Data.SqlClient;
using WSI.Common;
using System.Web;
using System.Xml.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Diagnostics;
using System.Configuration;
using System.Xml;
using System.Data.SqlTypes;

namespace WIN.DATA.Service
{
  public static class WeatherService
  {
    /// <summary>
    /// Multithread lock
    /// </summary>
    private static readonly object m_lock = new object();

    /// <summary>
    /// Thread delegate
    /// </summary>
    private delegate void ThreadWorker();
    private static Thread m_thread;

    /// <summary>
    /// 
    /// </summary>
    private static String m_base_address;

    /// <summary>
    /// Latitude and Longitude from General Params
    /// </summary>
    private static String m_lat_long;

    /// <summary>
    /// General initialization
    /// </summary>
    public static void Init()
    {
      m_thread = new Thread(new ThreadStart(Task));
      m_thread.Name = "Weather_Service";
      m_thread.Start();
    }

    /// <summary>
    /// Main task for the thread
    /// </summary>
    private static void Task()
    {
      Int32 _gaming_day;
      TimeSpan _date_diff;
      Boolean _processed = true;

      while (true)
      {
        try
        {
          _processed = true;

          if (GeneralParam.GetBoolean("Intellia", "Dashboard.Meteogram.Enabled"))
          {
            m_base_address = GeneralParam.GetString("Intellia", "Dashboard.Meteogram.URL");
            m_lat_long = GeneralParam.GetString("Intellia", "Dashboard.Meteogram.LatitudeLongitude");
            _gaming_day = Int32.Parse(Misc.TodayOpening().ToString("yyyyMMdd"));

            using (DB_TRX _db_trx = new DB_TRX())
            {
              if (!CheckTodayWeather(_gaming_day, _db_trx.SqlTransaction))
              {
                if (!ProcessTodayWeather(_gaming_day, _db_trx.SqlTransaction))
                {
                  _processed = false;

                  Log.Message("Error ProcessTodayWeather");
                }
              }
            }
          }
        }
        catch (Exception _ex)
        {
          Log.Message("WeatherServiceThread: " + _ex.Message);
          Log.Exception(_ex);

          _processed = false;
        }

        if (!_processed)
        {
          Thread.Sleep(50000);
        }
        else
        {
          _date_diff = Misc.TodayOpening().AddDays(1) - WASDB.Now;

          Thread.Sleep((int)_date_diff.TotalMilliseconds);
        }
      }
    }

    /// <summary>
    /// Check from database if today has row
    /// </summary>
    /// <param name="DeltaDate"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    private static Boolean CheckTodayWeather(Int32 DeltaDate, SqlTransaction Trx)
    {
      StringBuilder _str_query = new StringBuilder();
      Object _obj;

      try
      {
        _str_query.Append("SELECT COUNT(*)              ");
        _str_query.Append("  FROM LAYOUT_WEATHER_DIARY  ");
        _str_query.Append(" WHERE LWD_DATE = @pDate     ");

        using (SqlCommand _cmd = new SqlCommand(_str_query.ToString(), Trx.Connection, Trx))
        {
          // Params Adding
          _cmd.Parameters.Add("@pDate", SqlDbType.BigInt).Value = DeltaDate;
          _obj = _cmd.ExecuteScalar();
        }

        if (_obj != null && _obj != DBNull.Value)
        {
          return ((Int32)_obj != 0);
        }
        else
        {
          return false;
        }
      }
      catch (Exception _ex)
      {

        Log.Exception(_ex);
      }
      //Se devuelve true para que no genere registro si hay fallo
      return true;
    }

    /// <summary>
    /// Send request to API weather and save into database
    /// </summary>
    /// <param name="DeltaDate"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    private static Boolean ProcessTodayWeather(Int32 DeltaDate, SqlTransaction Trx)
    {
      XmlDocument _xml_response;
      HttpClient _client;
      HttpResponseMessage _response;
      String _str_response;
      try
      {
        _xml_response = new XmlDocument();

        _client = new HttpClient();
        _client.BaseAddress = new Uri(m_base_address);

        // Add an Accept header for JSON format.
        _client.DefaultRequestHeaders.Accept.Add(
        new MediaTypeWithQualityHeaderValue("application/json"));

        // List data response.
        _response = _client.GetAsync(GetWeatherParameters()).Result;

        if (_response.IsSuccessStatusCode)
        {
          // Parse the response body
          _str_response = _response.Content.ReadAsStringAsync().Result;

          _xml_response.LoadXml(_str_response);

          if (!Trx_SaveWeatherToday(DeltaDate, _xml_response, Trx))
          {

            Log.Message("Trx_SaveWeatherToday");
          }
        }
        else
        {
          Console.WriteLine("{0} ({1})", (int)_response.StatusCode, _response.ReasonPhrase);
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    /// <summary>
    /// Generate URI params
    /// </summary>
    /// <returns></returns>
    private static String GetWeatherParameters()
    {
      StringBuilder _str = new StringBuilder();
      String _key, _output, _temp_unit, _ws_unit;

      _key = ConfigurationManager.AppSettings["uac"].ToString();
      _output = ConfigurationManager.AppSettings["output"].ToString();
      _temp_unit = ConfigurationManager.AppSettings["temp_unit"].ToString();
      _ws_unit = ConfigurationManager.AppSettings["ws_unit"].ToString();


      //http://www.myweather2.com/developer/forecast.ashx?uac=g.kntSho5q&output=xml&query=29.073,-110.9559&temp_unit=c

      _str.Append("?");
      _str.Append("uac=" + _key);
      _str.Append("&output=" + _output);
      _str.Append("&query=" + m_lat_long);
      _str.Append("&temp_unit=" + _temp_unit);
      _str.Append("&ws_unit=" + _ws_unit);

      return _str.ToString();
    }

    /// <summary>
    /// Store data into database
    /// </summary>
    /// <param name="DeltaDate"></param>
    /// <param name="XmlData"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    private static Boolean Trx_SaveWeatherToday(Int32 DeltaDate, XmlDocument XmlData, SqlTransaction Trx)
    {
      int _weather_code;
      try
      {
        //Extract XML params
        XmlNodeList _forecast_list = XmlData.GetElementsByTagName("forecast");
        XmlNode _node = _forecast_list[0];

        _node = _node["day"];
        _weather_code = int.Parse(_node["weather_code"].InnerText);

        //Store into database
        using (SqlCommand _cmd = new SqlCommand("Layout_SaveWeatherDay", Trx.Connection, Trx))
        {
          _cmd.CommandType = CommandType.StoredProcedure;

          // Params Adding
          _cmd.Parameters.Add("@pDate", SqlDbType.Int).Value = DeltaDate;
          _cmd.Parameters.Add("@pWeatherCode", SqlDbType.Int).Value = _weather_code;
          _cmd.Parameters.Add("@pXML", SqlDbType.Xml).Value = new SqlXml(new XmlTextReader(XmlData.InnerXml, XmlNodeType.Document, null));

          _cmd.CommandTimeout = 2 * 60; // 2 minutes

          if (!(_cmd.ExecuteNonQuery() > 0))
          {
            return false;
          }

          Trx.Commit();
        }

        return true;
      }
      catch (Exception _ex)
      {

        Log.Exception(_ex);
      }

      return false;
    }
  }
}