﻿////------------------------------------------------------------------------------
//// Copyright © 2012 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: RequiredParametersBehaviorAttribute.cs
//// 
////      DESCRIPTION: Interface with methods published

//// 
////           AUTHOR: Joaquim Cid
//// 
////    CREATION DATE: 10-OCT-2012
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 10-OCT-2012 JCM    First release.
////------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ServiceModel.Description;
using System.Xml.Schema;
using System.Xml;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using System.Collections;
using System.ServiceModel;

namespace DATA.Service
{

  [AttributeUsage(AttributeTargets.All)]
  public class RequiredParametersBehaviorAttribute : Attribute, IContractBehavior, IWsdlExportExtension
  {
    private List<RequiredMessagePart> _requiredParameter;

    #region IWsdlExportExtension Members


    void IWsdlExportExtension.ExportContract(WsdlExporter exporter, WsdlContractConversionContext context)
    {

      _requiredParameter = new List<RequiredMessagePart>();

      foreach (var _operation in context.Contract.Operations)
      {
        foreach (var _message in _operation.Messages)
        {
          var _parameters = _operation.SyncMethod.GetParameters();

          for (int _idx_param = 0; _idx_param < _parameters.Length; _idx_param++)
          {
            object[] attributes = _parameters[_idx_param].GetCustomAttributes(typeof(OptionalAttribute), false);
            if (attributes.Length == 0)
            {
              for (int _idx_name = 0; _idx_name < _message.Body.Parts.Count; _idx_name++)
              {
                _requiredParameter.Add(new RequiredMessagePart()
                {
                  Namespace = _message.Body.Parts[_idx_name].Namespace,
                  Message = _operation.Messages[(_message.Direction == MessageDirection.Input ? 0 : 1)].Body.WrapperName,
                  Name = _message.Body.Parts[_idx_name].Name
                });

              }

            }
          }
        }
      }
    }

    /// <summary>
    /// When ExportEndpoint is called, the XML schemas have been generated. Now we can manipulate to
    /// our heart's content.
    /// </summary>
    void IWsdlExportExtension.ExportEndpoint(WsdlExporter exporter, WsdlEndpointConversionContext context)
    {
      if (_requiredParameter == null)
      {
        return;
      }
      foreach (var p in _requiredParameter)
      {
        var schemas = exporter.GeneratedXmlSchemas.Schemas(p.Namespace);

        foreach (XmlSchema schema in schemas)
        {
          var message = (XmlSchemaElement)schema.Elements[p.XmlQualifiedName];
          if (message == null)
          {
            continue;
          }
          var complexType = message.ElementSchemaType as XmlSchemaComplexType;
          var sequence = complexType.Particle as XmlSchemaSequence;

          foreach (XmlSchemaElement item in sequence.Items)
          {
            if (message.Name == "DATA_ReporteFase1_Contexto" && item.Name == p.Name && p.Name == "SubLineasReportadas")
            {
              item.MinOccurs = 0;
              item.MinOccursString = "0";
              item.IsNillable = true;
              item.DefaultValue = null;
            }
            else
            {
              if (item.Name == p.Name)
              {
                item.MinOccurs = 1;
                item.MinOccursString = "1";
                item.IsNillable = false;
              }
            }
          }
        }
      }

      // Throw away the temporary list we generated
      _requiredParameter = null;
    }

    private void AddImportedSchemas(XmlSchema schema, XmlSchemaSet schemaSet, List<XmlSchema> importsList)
    {
      foreach (XmlSchemaImport import in schema.Includes)
      {
        ICollection realSchemas =
            schemaSet.Schemas(import.Namespace);

        foreach (XmlSchema ixsd in realSchemas)
        {
          if (!importsList.Contains(ixsd))
          {
            importsList.Add(ixsd);
            AddImportedSchemas(ixsd, schemaSet, importsList);
          }
        }
      }
    }

    #endregion

    #region Nested types

    private class RequiredMessagePart
    {
      public string Namespace { get; set; }
      public string Message { get; set; }
      public string Name { get; set; }

      public XmlQualifiedName XmlQualifiedName
      {
        get
        {
          return new XmlQualifiedName(Message, Namespace);
        }
      }
    }

    #endregion

    #region IContractBehavior Members (nothing to be done)

    void IContractBehavior.AddBindingParameters(ContractDescription contractDescription, ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
    {
    }

    void IContractBehavior.ApplyClientBehavior(ContractDescription contractDescription, ServiceEndpoint endpoint, ClientRuntime clientRuntime)
    {
    }

    void IContractBehavior.ApplyDispatchBehavior(ContractDescription contractDescription, ServiceEndpoint endpoint, DispatchRuntime dispatchRuntime)
    {
    }

    void IContractBehavior.Validate(ContractDescription contractDescription, ServiceEndpoint endpoint)
    {
    }


    #endregion
  }

  [AttributeUsage(AttributeTargets.Method)]
  public class OptionalAttribute : Attribute
  {
  }





 





}