﻿////------------------------------------------------------------------------------
//// Copyright © 2012 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: DATA_ProcessMessage.cs
//// 
////      DESCRIPTION: Class used to manage the requests and retrieve the responses for the Smartfloor Android Application
//// 
////           AUTHOR: Carlos Rodrigo
//// 
////    CREATION DATE: 04-FEB-2016
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 04-FEB-2016 CSR    Modifications for Smartfloor Application
////------------------------------------------------------------------------------
#define DEBUG

using System;
using System.Reflection;
using DATA.OBJ.DTO;
using DATA.Service.MessageObjects.Respuesta;
using DATA.Service.MessageObjects.Pregunta;
using DATA.OBJ;
using DATA.BL;
using DATA.BL.Mensajes;
using WSI.Common;
using WIN.DATA.Service.MessageObjects.Pregunta;
using WIN.DATA.Service.MessageObjects.Respuesta;
using DATA.OBJ.DTO.Responses;
using System.Collections.Generic;
using System.Configuration;
using uPLibrary.Networking.M2Mqtt;
using System.Text;
using System.Threading;

namespace WIN.DATA.Service
{
  public static class RequestProcess
  {

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Process the received messages and retrieves the specified answer
    //
    //  PARAMS:
    //      - INPUT:
    //          - IPregunta     : Inbound message in a generic interface form
    //          - MessageType   : Kind of Message received
    //
    //      - OUTPUT:
    //
    // RETURNS: IRespuesta
    //  
    public static IDataResponse DATA_ProcessMessage(IPregunta DataServiceRequest, MessageType MessageTypeRequest, String ip = "")
    {
      Session _session = null;
      GestorMensajes _manager;
      Object _iresponse;
      String _log_response_info;
      String _log_request_info;

      _log_response_info = "";
      _log_request_info = "";
      try
      {
        _session = GestorSesiones.GetSession(DataServiceRequest.SessionId, MessageTypeRequest);
        _log_request_info = "<" + MessageTypeRequest.ToString() + ">" + DataServiceRequest.GetLogInfo();

        _manager = new GestorMensajes();
        Type _manager_type = Type.GetType(typeof(GestorMensajes).AssemblyQualifiedName);
        MethodInfo _manager_method = _manager_type.GetMethod(MessageTypeRequest.ToString());
        IResponse _response_value = (IResponse)_manager_method.Invoke(_manager, new object[] { DataServiceRequest.GetInternalObject(), _session });
        _iresponse = CreateClientResponse(MessageTypeRequest, new Type[] { typeof(IResponse) }, new object[] { _response_value });
        _log_response_info = "<" + MessageTypeRequest.ToString() + ">" + _response_value.CodigoRespuesta + ";" + _response_value.CodigoRespuestaDescripcion;
        return (IDataResponse)_iresponse;
      }
      catch (TargetInvocationException DataServiceEx)
      {
        _iresponse = GenerateResponse(MessageTypeRequest, ((DataServiceException)DataServiceEx.InnerException).ResponseCode, ((DataServiceException)DataServiceEx.InnerException).ResponseDescription, out _log_response_info);

        return (IDataResponse)_iresponse;
      }
      catch (DataServiceException DataServiceEx)
      {
        _iresponse = GenerateResponse(MessageTypeRequest, DataServiceEx.ResponseCode, DataServiceEx.ResponseDescription, out _log_response_info);

        return (IDataResponse)_iresponse;
      }
      catch (Exception Ex)
      {
        GestorLogs.LogException(Ex.Message, Ex);
        GestorSesiones.RemoveSession(DataServiceRequest.SessionId);

        _iresponse = GenerateResponse(MessageTypeRequest, ResponseCode.DATA_RC_INTERNAL_DATA_ERROR, WSI.Common.Resource.String("STR_WS_DATA_ERROR_PROCESS_MESSAGE"), out _log_response_info);

        return (IDataResponse)_iresponse;
      }
      finally
      {
        GenerateLog(MessageTypeRequest, _session, _log_response_info, _log_request_info);
      }
    }

    // PURPOSE: Process a received DATA_InicioSesion message and retrieves an answer
    //
    //  PARAMS:
    //      - INPUT:
    //          - DATA_InicioSesion     : Inbound message 
    //
    //      - OUTPUT:
    //
    // RETURNS: DATA_RespuestaInicioSesion
    public static DATA_LogInResponse DATA_LogIn(DATA_LogIn DataServiceRequest)
    {
      Session _session;
      String _additionalText;
      String _additionalTextadd;

      try
      {
        DTO_LogInResponse _response;
        _response = new GestorMensajes().DATA_InicioSesion(DataServiceRequest.GetLoginObject());

        if (Misc.ReadGeneralParams("DATA.Service", "DATA_Param_NET_FLOW_Enabled").Equals("1"))
        {
          GestorLogs.LogSoap((Int16)LogID.WS_NET_FLOW, "OPERADOR: " + DataServiceRequest.IdOperador + " -->  <DATA_InicioSesion>Operador:" + DataServiceRequest.IdOperador + ";Login:" + DataServiceRequest.Login);
          GestorLogs.LogSoap((Int16)LogID.WS_NET_FLOW, "OPERADOR: " + DataServiceRequest.IdOperador + " <--  <DATA_InicioSesion>" + _response.ResponseCode + ";" + _response.ResponseCodeDescription);
        }
        if (!String.IsNullOrEmpty(_response.SessionId))
        {

          if (GestorSesiones.ExistIdSession(_response.SessionId))
          {
            _session = GestorSesiones.GetSessionForRegistrosLinea(_response.SessionId);
            if (GestorSesiones.LogMessage(_session, MessageType.DATA_InicioSesion))
            {
              _additionalText = "OPERADOR: [" + _session.Login.Login + "|" + _session.Login.UserName + "]";
              _additionalTextadd = "<DATA_InicioSesion>Operador:" + DataServiceRequest.IdOperador + ";Login:" + DataServiceRequest.Login + ";Pass:" + DataServiceRequest.Password;
              GestorLogs.LogSoap((Int16)LogID.WS_NET_MSG, _additionalText + " <--  " + _additionalTextadd);

              _additionalTextadd = "<DATA_InicioSesion>" + _response.ResponseCode + ";" + _response.ResponseCodeDescription;
              _additionalTextadd += ";IdSession:" + _response.SessionId;
              GestorLogs.LogSoap((Int16)LogID.WS_NET_MSG, _additionalText + " -->  " + _additionalTextadd);
            }
          }
          else
          {
            _response.ResponseCode = ResponseCode.DATA_RC_SESSION_TIMEOUT;
            _response.ResponseCodeDescription = WSI.Common.Resource.String("STR_SESSION_TIME_OUT");

          }
        }
        else if (_response.ResponseCode == ResponseCode.DATA_RC_BAD_LOGIN)
        {
          _additionalText = "";
          _additionalTextadd = "<DATA_InicioSesion>Operador:" + DataServiceRequest.IdOperador + ";Login:" + DataServiceRequest.Login + ";Pass:" + DataServiceRequest.Password;
          GestorLogs.LogSoap((Int16)LogID.WS_NET_MSG, _additionalText + " <--  " + _additionalTextadd);

          _additionalTextadd = "<DATA_InicioSesion>" + _response.ResponseCode + ";" + _response.ResponseCodeDescription;
          _additionalTextadd += ";IdSession:" + _response.SessionId;
          GestorLogs.LogSoap((Int16)LogID.WS_NET_MSG, _additionalText + " -->  " + _additionalTextadd);
        }


        return new DATA_LogInResponse(_response);
      }
      catch (Exception Ex)
      {
        GestorLogs.LogException(Ex.Message, Ex);
        return new DATA_LogInResponse(GestorErrores.GetInicioSesionException(ResponseCode.DATA_RC_BAD_MESSAGE_DATA, Ex.Message));
      }
    }

    public static DATA_LayoutSiteTaskAssignedResponse Data_GetAssignedTasks(DATA_GetAssignedLayoutSiteTasks session)
    {
      Session _session;
      String _additionalText;
      String _additionalTextadd;

      try
      {
        DTO_GetLayoutSiteAssignedTasksResponse _response = new DTO_GetLayoutSiteAssignedTasksResponse();

        if (!String.IsNullOrEmpty(session.SessionId))
        {

          if (GestorSesiones.ExistIdSession(session.SessionId))
          {

            _session = GestorSesiones.GetSessionForRegistrosLinea(session.SessionId);
            _response = new GestorMensajes().BL_GetAssignedTasks(_session);

            if (GestorSesiones.LogMessage(_session, MessageType.DATA_TareasOperador))
            {
              _additionalText = "OPERADOR: [" + _session.Login.Login + "|" + _session.Login.UserName + "]";
              GestorLogs.LogSoap((Int16)LogID.WS_NET_MSG, _additionalText);

              _additionalTextadd = "<Data_GetAssignedTasks>" + _response.ResponseCode + ";" + _response.ResponseCodeDescription;
              _additionalTextadd += ";IdSession:" + _response.SessionId;
              GestorLogs.LogSoap((Int16)LogID.WS_NET_MSG, _additionalText + " -->  " + _additionalTextadd);
            }
          }
          else
          {
            _response.ResponseCode = ResponseCode.DATA_RC_SESSION_TIMEOUT;
            _response.ResponseCodeDescription = WSI.Common.Resource.String("STR_SESSION_TIME_OUT");
          }
        }
        else if (_response.ResponseCode == ResponseCode.DATA_RC_BAD_LOGIN)
        {
          _additionalText = "";
          GestorLogs.LogSoap((Int16)LogID.WS_NET_MSG, _additionalText);

          _additionalTextadd = "<Data_GetAssignedTasks>" + _response.ResponseCode + ";" + _response.ResponseCodeDescription;
          _additionalTextadd += ";IdSession:" + _response.SessionId;
          GestorLogs.LogSoap((Int16)LogID.WS_NET_MSG, _additionalText + " -->  " + _additionalTextadd);
        }


        return new DATA_LayoutSiteTaskAssignedResponse(_response);
      }
      catch (Exception Ex)
      {
        GestorLogs.LogException(Ex.Message, Ex);
        return new DATA_LayoutSiteTaskAssignedResponse(GestorErrores.GetLayoutSiteAssignedTasksException(ResponseCode.DATA_RC_BAD_MESSAGE_DATA, WSI.Common.Resource.String("STR_WS_DATA_ERROR_GETTING_TASK")));
      }
    }

    internal static DATA_PostActionResponse Data_UpdateLayoutSiteTaskInfo(DATA_UpdateLayoutSiteTaskInfo taskInfo)
    {
      Session _session;
      String _additionalText;
      String _additionalTextadd;

      try
      {
        DTO_UpdateLayoutSiteTaskInfoResponse _response = new DTO_UpdateLayoutSiteTaskInfoResponse();

        if (!String.IsNullOrEmpty(taskInfo.SessionId))
        {

          if (GestorSesiones.ExistIdSession(taskInfo.SessionId))
          {

            _session = GestorSesiones.GetSessionForRegistrosLinea(taskInfo.SessionId);
            _response = new GestorMensajes().BL_UpdateLayoutTaskInfo((LayoutTaskInfo)taskInfo.GetInternalObject(), _session);

            GestorMensajes _manager = new GestorMensajes();
            long _taskId = long.Parse(taskInfo.TaskId);
            LayoutSiteTask _task = _manager.BL_GetTaskById(_taskId);
            IEnumerable<String> _users = _manager.BL_GetUsersInRoleFromTask(_taskId);
            if (_task.AssignedRoleId != 0)
              BroadcastReloadTaskActionToRolUsers(_users, _task, _session.LoginId);




            if (GestorSesiones.LogMessage(_session, MessageType.DATA_TareasOperador))
            {
              _additionalText = "OPERADOR: [" + _session.Login.Login + "|" + _session.Login.UserName + "]";
              GestorLogs.LogSoap((Int16)LogID.WS_NET_MSG, _additionalText);

              _additionalTextadd = "<Data_UpdateLayoutSiteTaskInfo>" + _response.ResponseCode + ";" + _response.ResponseCodeDescription;
              _additionalTextadd += ";IdSession:" + _response.SessionId;
              GestorLogs.LogSoap((Int16)LogID.WS_NET_MSG, _additionalText + " -->  " + _additionalTextadd);
            }
          }
          else
          {
            _response.ResponseCode = ResponseCode.DATA_RC_SESSION_TIMEOUT;
            _response.ResponseCodeDescription = WSI.Common.Resource.String("STR_SESSION_TIME_OUT");
          }
        }
        else if (_response.ResponseCode == ResponseCode.DATA_RC_BAD_LOGIN)
        {
          _additionalText = "";
          GestorLogs.LogSoap((Int16)LogID.WS_NET_MSG, _additionalText);

          _additionalTextadd = "<Data_UpdateLayoutSiteTaskInfo>" + _response.ResponseCode + ";" + _response.ResponseCodeDescription;
          _additionalTextadd += ";IdSession:" + _response.SessionId;
          GestorLogs.LogSoap((Int16)LogID.WS_NET_MSG, _additionalText + " -->  " + _additionalTextadd);
        }


        return new DATA_PostActionResponse(_response);
      }
      catch (Exception Ex)
      {
        GestorLogs.LogException(Ex.Message, Ex);
        return new DATA_PostActionResponse(GestorErrores.GetUpdateLayoutSiteTaskInfoException(ResponseCode.DATA_RC_BAD_MESSAGE_DATA, WSI.Common.Resource.String("STR_WS_DATA_ERROR_UPDATING_TASK_STATE")));
      }
    }

    internal static DATA_PostActionResponse Data_UploadLayoutSiteMediaAsset(DATA_LayoutSiteMediaAsset DataServiceRequest)
    {
      Session _session;
      String _additionalText;
      String _additionalTextadd;

      try
      {
        DTO_UploadLayoutSiteMediaAssetResponse _response = new DTO_UploadLayoutSiteMediaAssetResponse();

        if (!String.IsNullOrEmpty(DataServiceRequest.SessionId))
        {

          if (GestorSesiones.ExistIdSession(DataServiceRequest.SessionId))
          {

            _session = GestorSesiones.GetSessionForRegistrosLinea(DataServiceRequest.SessionId);
            _response = new GestorMensajes().BL_UploadLayouSiteMediaAsset((LayoutSiteMediaAsset)DataServiceRequest.GetInternalObject(), _session);

            if (GestorSesiones.LogMessage(_session, MessageType.DATA_TareasOperador))
            {
              _additionalText = "OPERADOR: [" + _session.Login.Login + "|" + _session.Login.UserName + "]";
              GestorLogs.LogSoap((Int16)LogID.WS_NET_MSG, _additionalText);

              _additionalTextadd = "<Data_UploadLayoutSiteMediaAsset>" + _response.ResponseCode + ";" + _response.ResponseCodeDescription;
              _additionalTextadd += ";IdSession:" + _response.SessionId;
              GestorLogs.LogSoap((Int16)LogID.WS_NET_MSG, _additionalText + " -->  " + _additionalTextadd);
            }
          }
          else
          {
            _response.ResponseCode = ResponseCode.DATA_RC_SESSION_TIMEOUT;
            _response.ResponseCodeDescription = WSI.Common.Resource.String("STR_SESSION_TIME_OUT");
          }
        }
        else if (_response.ResponseCode == ResponseCode.DATA_RC_BAD_LOGIN)
        {
          _additionalText = "";
          GestorLogs.LogSoap((Int16)LogID.WS_NET_MSG, _additionalText);

          _additionalTextadd = "<Data_UploadLayoutSiteMediaAsset>" + _response.ResponseCode + ";" + _response.ResponseCodeDescription;
          _additionalTextadd += ";IdSession:" + _response.SessionId;
          GestorLogs.LogSoap((Int16)LogID.WS_NET_MSG, _additionalText + " -->  " + _additionalTextadd);
        }


        return new DATA_PostActionResponse(_response);
      }
      catch (Exception Ex)
      {
        GestorLogs.LogException(Ex.Message, Ex);
        return new DATA_PostActionResponse(GestorErrores.GetUpdateLayoutSiteTaskInfoException(ResponseCode.DATA_RC_BAD_MESSAGE_DATA, WSI.Common.Resource.String("STR_WS_DATA_UPLOADING_MEDIA_FILE")));
      }
    }

    #endregion

    #region Private Methods
    private static void BroadcastReloadTaskActionToRolUsers(IEnumerable<String> _users, LayoutSiteTask _task, long currentUserId)
    {
      string _mqttBrokerAddress = ConfigurationManager.AppSettings.Get("ip_broker");
      string _clientId = String.Format("DATAService_{0}", DateTime.Now.Ticks);
      var _client = new MqttClient(_mqttBrokerAddress);
      _client.Connect(_clientId);

      foreach (string user in _users)
      {
        //if (user.Equals(currentUserId.ToString())) continue;

        if (!SendNewTaskAsigned(_client, user, _task.Subcategory, _task.Description, _task.Id))
        {
          Log.Message("SendNewTaskAsigned() failed|| ");
        }
      }

    }//BroadcastReloadTaskActionToRolUsers

    private static Boolean SendNewTaskAsigned(MqttClient mqttClient, String userId, int subCategoryId, String message, long itemId)
    {
      try
      {
        string _defaultTopic = "/Task/Notifications/{0}/{1}";

        if (userId == "0")
          throw new Exception("deviceId=0");//Logger.Write();
        if (itemId != 0)
          message += "_" + itemId;

        var formattedTopic = String.Format(_defaultTopic, userId, subCategoryId);
        mqttClient.Publish(formattedTopic, Encoding.UTF8.GetBytes(message));

        return true;
      }
      catch (Exception e)
      {
        Log.Message(String.Format("WEBSERVICE - Error on MQTT action - Message: {0}| InnerException: {1}", e.Message, e.InnerException));
        throw;
      }
    }
    // PURPOSE: Creates a response to send to the client
    //
    //  PARAMS:
    //      - INPUT:
    //          - MessageType     : Kind of Message received
    //          - ResponseCode    : Response Code to assign in the answer
    //          - ResponseDescription  : Description of the given Response Code 
    //
    //      - OUTPUT:
    //
    // RETURNS: IRespuesta
    //  
    private static IDataResponse CreateClientResponse(MessageType MessageTypeRequest, Type[] Types, Object[] Objects)
    {
      Object _iresponse;
      String _response_name;

      _response_name = MessageTypeRequest.ToString().Replace("DATA_", "DATA_Respuesta");
      Type _response_type = Type.GetType("DATA.Service.MessageObjects.Respuesta." + _response_name);
      ConstructorInfo _response_constructor = _response_type.GetConstructor(Types);
      _iresponse = _response_constructor.Invoke(Objects);
      return (IDataResponse)_iresponse;
    }


    private static void GenerateLog(MessageType MessageTypeRequest, Session _session, String _log_response_info, String _log_request_info)
    {
      if (_session != null)
      {
        String _additionalText;
        _additionalText = "OPERADOR: [" + _session.Login.Login + "|" + _session.Login.UserName + "]";

        GestorSesiones.ReleaseSession(_session.SessionId);
        if (Misc.ReadGeneralParams("DATA.Service", "DATA_Param_NET_FLOW_Enabled").Equals("1"))
        {
          GestorLogs.LogSoap((Int16)LogID.WS_NET_FLOW, _additionalText + " -->  " + _log_request_info);
          GestorLogs.LogSoap((Int16)LogID.WS_NET_FLOW, _additionalText + " <--  " + _log_response_info);
        }
        if (GestorSesiones.LogMessage(_session, MessageTypeRequest))
        {
          if (_session.CurrentTransaction != null)
          {
            _additionalText += "[" + (_session.CurrentTransaction).Place.Nombre + "]";
          }
          GestorLogs.LogSoap((Int16)LogID.WS_NET_MSG, _additionalText + " <--  " + _session.SoapReceived);
          GestorLogs.LogSoap((Int16)LogID.WS_NET_MSG, _additionalText + " -->  " + _log_response_info);
        }
      }
    }

    private static Object GenerateResponse(MessageType MessageTypeRequest, ResponseCode Code, String Description, out String _log_response_info)
    {
      Object _iresponse;

      _iresponse = CreateClientResponse(MessageTypeRequest, new Type[] { typeof(ResponseCode), typeof(String) }, new object[]
  {
          Code,
          Description
  });

      _log_response_info = "<" + MessageTypeRequest.ToString() + ">" + Code.ToString() + ";" + Description;

      return (IDataResponse)_iresponse;

    }

    #endregion


    internal static DATA_PostActionResponse Data_UpdateBeaconRunnerPosition(DATA_BeaconRunnerDistance DataServiceRequest)
    {
      Session _session;
      String _additionalText;
      String _additionalTextadd;

      try
      {
        DTO_UpdateBeaconRunnerPositionResponse _response = new DTO_UpdateBeaconRunnerPositionResponse();

        if (!String.IsNullOrEmpty(DataServiceRequest.SessionId))
        {
          if (GestorSesiones.ExistIdSession(DataServiceRequest.SessionId))
          {
            _session = GestorSesiones.GetSessionForRegistrosLinea(DataServiceRequest.SessionId);
            //Log.Message(String.Format("VALUES FROM DEVICE -  DeviceId: {0} - X: {1} - Y:{2}", DataServiceRequest.DeviceId, DataServiceRequest.PositionX, DataServiceRequest.PositionY));
            _response = new GestorMensajes().BL_UpdateBeaconRunnerPosition((LayoutRunnersPosition)DataServiceRequest.GetInternalObject(), _session);

            if (GestorSesiones.LogMessage(_session, MessageType.DATA_TareasOperador))
            {
              _additionalText = "OPERADOR: [" + _session.Login.Login + "|" + _session.Login.UserName + "]";
              GestorLogs.LogSoap((Int16)LogID.WS_NET_MSG, _additionalText);

              _additionalTextadd = "<Data_UpdateBeaconRunnerPosition>" + _response.ResponseCode + ";" + _response.ResponseCodeDescription;
              _additionalTextadd += ";IdSession:" + _response.SessionId;
              GestorLogs.LogSoap((Int16)LogID.WS_NET_MSG, _additionalText + " -->  " + _additionalTextadd);
            }
          }
          else
          {
            _response.ResponseCode = ResponseCode.DATA_RC_SESSION_TIMEOUT;
            _response.ResponseCodeDescription = WSI.Common.Resource.String("STR_SESSION_TIME_OUT");
          }
        }
        else if (_response.ResponseCode == ResponseCode.DATA_RC_BAD_LOGIN)
        {
          _additionalText = "";
          GestorLogs.LogSoap((Int16)LogID.WS_NET_MSG, _additionalText);

          _additionalTextadd = "<Data_UpdateBeaconRunnerPosition>" + _response.ResponseCode + ";" + _response.ResponseCodeDescription;
          _additionalTextadd += ";IdSession:" + _response.SessionId;
          GestorLogs.LogSoap((Int16)LogID.WS_NET_MSG, _additionalText + " -->  " + _additionalTextadd);
        }


        return new DATA_PostActionResponse(_response);
      }
      catch (Exception Ex)
      {
        GestorLogs.LogException(Ex.Message, Ex);
        return new DATA_PostActionResponse(GestorErrores.GetUpdateLayoutSiteTaskInfoException(ResponseCode.DATA_RC_BAD_MESSAGE_DATA, WSI.Common.Resource.String("STR_WS_DATA_UPLOADING_RUNNER_POSITION")));
      }
    }

    internal static DATA_PostActionResponse Data_CreateAlarm(DATA_CreateAlarm DataServiceRequest)
    {
      Session _session;
      String _additionalText;
      String _additionalTextadd;

      try
      {
        DTO_CreateAlarmResponse _response = new DTO_CreateAlarmResponse();

        if (!String.IsNullOrEmpty(DataServiceRequest.SessionId))
        {

          if (GestorSesiones.ExistIdSession(DataServiceRequest.SessionId))
          {

            _session = GestorSesiones.GetSessionForRegistrosLinea(DataServiceRequest.SessionId);
            _response = new GestorMensajes().BL_CreateAlarm((CreateAlarm)DataServiceRequest.GetInternalObject(), _session);

            if (GestorSesiones.LogMessage(_session, MessageType.DATA_TareasOperador))
            {
              _additionalText = "OPERADOR: [" + _session.Login.Login + "|" + _session.Login.UserName + "]";
              GestorLogs.LogSoap((Int16)LogID.WS_NET_MSG, _additionalText);

              _additionalTextadd = "<Data_CreateAlarm>" + _response.ResponseCode + ";" + _response.ResponseCodeDescription;
              _additionalTextadd += ";IdSession:" + _response.SessionId;
              GestorLogs.LogSoap((Int16)LogID.WS_NET_MSG, _additionalText + " -->  " + _additionalTextadd);
            }
          }
          else
          {
            _response.ResponseCode = ResponseCode.DATA_RC_SESSION_TIMEOUT;
            _response.ResponseCodeDescription = WSI.Common.Resource.String("STR_SESSION_TIME_OUT");
          }
        }
        else if (_response.ResponseCode == ResponseCode.DATA_RC_BAD_LOGIN)
        {
          _additionalText = "";
          GestorLogs.LogSoap((Int16)LogID.WS_NET_MSG, _additionalText);

          _additionalTextadd = "<Data_CreateAlarm>" + _response.ResponseCode + ";" + _response.ResponseCodeDescription;
          _additionalTextadd += ";IdSession:" + _response.SessionId;
          GestorLogs.LogSoap((Int16)LogID.WS_NET_MSG, _additionalText + " -->  " + _additionalTextadd);
        }


        return new DATA_PostActionResponse(_response);
      }
      catch (DataServiceException Ex)
      {
        GestorLogs.LogException(Ex.Message, Ex);
        return new DATA_PostActionResponse(GestorErrores.GetUpdateLayoutSiteTaskInfoException(Ex.ResponseCode, Ex.ResponseDescription));
      }
      catch (Exception Ex)
      {
        GestorLogs.LogException(Ex.Message, Ex);
        return new DATA_PostActionResponse(GestorErrores.GetUpdateLayoutSiteTaskInfoException(ResponseCode.DATA_RC_BAD_MESSAGE_DATA, WSI.Common.Resource.String("STR_WS_DATA_ERROR_CREATING_SITE_ALARM")));
      }
    }

    internal static DATA_TerminalInfoResponse Data_GetTerminalInfoByTerminalId(DATA_TerminalInfo terminalInfo)
    {
      Session _session;
      String _additionalText;
      String _additionalTextadd;

      try
      {
        DTO_GetLayoutSiteAlarmsResponse _response = new DTO_GetLayoutSiteAlarmsResponse();

        if (!String.IsNullOrEmpty(terminalInfo.SessionId))
        {

          if (GestorSesiones.ExistIdSession(terminalInfo.SessionId))
          {
            _session = GestorSesiones.GetSessionForRegistrosLinea(terminalInfo.SessionId);
            _response = new GestorMensajes().BL_GetTerminalInfoByTerminalId(terminalInfo.TerminalId, _session);

            if (GestorSesiones.LogMessage(_session, MessageType.DATA_TareasOperador))
            {
              _additionalText = "OPERADOR: [" + _session.Login.Login + "|" + _session.Login.UserName + "]";
              GestorLogs.LogSoap((Int16)LogID.WS_NET_MSG, _additionalText);

              _additionalTextadd = "<Data_GetTerminalInfoByTerminalId>" + _response.ResponseCode + ";" + _response.ResponseCodeDescription;
              _additionalTextadd += ";IdSession:" + _response.SessionId;
              GestorLogs.LogSoap((Int16)LogID.WS_NET_MSG, _additionalText + " -->  " + _additionalTextadd);
            }
          }
          else
          {
            _response.ResponseCode = ResponseCode.DATA_RC_SESSION_TIMEOUT;
            _response.ResponseCodeDescription = WSI.Common.Resource.String("STR_SESSION_TIME_OUT");
          }
        }
        else if (_response.ResponseCode == ResponseCode.DATA_RC_BAD_LOGIN)
        {
          _additionalText = "";
          GestorLogs.LogSoap((Int16)LogID.WS_NET_MSG, _additionalText);

          _additionalTextadd = "<Data_GetTerminalInfoByTerminalId>" + _response.ResponseCode + ";" + _response.ResponseCodeDescription;
          _additionalTextadd += ";IdSession:" + _response.SessionId;
          GestorLogs.LogSoap((Int16)LogID.WS_NET_MSG, _additionalText + " -->  " + _additionalTextadd);
        }


        return new DATA_TerminalInfoResponse(_response);
      }
      catch (DataServiceException Ex)
      {
        GestorLogs.LogException(Ex.Message, Ex);
        return new DATA_TerminalInfoResponse(GestorErrores.GetLayoutSiteAlarmsException(Ex.ResponseCode, Ex.ResponseDescription));
      }
      catch (Exception Ex)
      {
        GestorLogs.LogException(Ex.Message, Ex);
        return new DATA_TerminalInfoResponse(GestorErrores.GetLayoutSiteAlarmsException(ResponseCode.DATA_RC_BAD_MESSAGE_DATA, WSI.Common.Resource.String("STR_WS_DATA_ERROR_GETTING_TERMINAL_INFO")));
      }
    }

    internal static DATA_TerminalInfoResponse Data_GetAlarmsCreatedByUser(DATA_TerminalInfo terminalInfo)
    {
      Session _session;
      String _additionalText;
      String _additionalTextadd;

      try
      {
        DTO_GetLayoutSiteAlarmsResponse _response = new DTO_GetLayoutSiteAlarmsResponse();

        if (!String.IsNullOrEmpty(terminalInfo.SessionId))
        {

          if (GestorSesiones.ExistIdSession(terminalInfo.SessionId))
          {
            _session = GestorSesiones.GetSessionForRegistrosLinea(terminalInfo.SessionId);
            _response = new GestorMensajes().BL_GetAlarmsCreatedByUser(_session);

            if (GestorSesiones.LogMessage(_session, MessageType.DATA_TareasOperador))
            {
              _additionalText = "OPERADOR: [" + _session.Login.Login + "|" + _session.Login.UserName + "]";
              GestorLogs.LogSoap((Int16)LogID.WS_NET_MSG, _additionalText);

              _additionalTextadd = "<Data_GetAlarmsCreatedByUser>" + _response.ResponseCode + ";" + _response.ResponseCodeDescription;
              _additionalTextadd += ";IdSession:" + _response.SessionId;
              GestorLogs.LogSoap((Int16)LogID.WS_NET_MSG, _additionalText + " -->  " + _additionalTextadd);
            }
          }
        }
        else if (_response.ResponseCode == ResponseCode.DATA_RC_BAD_LOGIN)
        {
          _additionalText = "";
          GestorLogs.LogSoap((Int16)LogID.WS_NET_MSG, _additionalText);

          _additionalTextadd = "<Data_GetAlarmsCreatedByUser>" + _response.ResponseCode + ";" + _response.ResponseCodeDescription;
          _additionalTextadd += ";IdSession:" + _response.SessionId;
          GestorLogs.LogSoap((Int16)LogID.WS_NET_MSG, _additionalText + " -->  " + _additionalTextadd);
        }
        else
        {
          _response.ResponseCode = ResponseCode.DATA_RC_SESSION_TIMEOUT;
          _response.ResponseCodeDescription = WSI.Common.Resource.String("STR_SESSION_TIME_OUT");
        }

        return new DATA_TerminalInfoResponse(_response);
      }
      catch (DataServiceException Ex)
      {
        GestorLogs.LogException(Ex.Message, Ex);
        return new DATA_TerminalInfoResponse(GestorErrores.GetLayoutSiteAlarmsException(Ex.ResponseCode, Ex.ResponseDescription));
      }
      catch (Exception Ex)
      {
        GestorLogs.LogException(Ex.Message, Ex);
        return new DATA_TerminalInfoResponse(GestorErrores.GetLayoutSiteAlarmsException(ResponseCode.DATA_RC_BAD_MESSAGE_DATA, WSI.Common.Resource.String("STR_WS_DATA_ERROR_GETTING_ALARMS_CREATED_BY_USER")));
      }
    }
    //SendMessageTo
    internal static DATA_SendMessageResponse SendMessageTo(DATA_MessageToRequest message)
    {
      Session _session;
      String _additionalText;
      String _additionalTextadd;

      try
      {
        DTO_MessageToResponse _response = new DTO_MessageToResponse();

        if (!String.IsNullOrEmpty(message.SessionId))
        {

          if (GestorSesiones.ExistIdSession(message.SessionId))
          {
            _session = GestorSesiones.GetSessionForRegistrosLinea(message.SessionId);
            LayoutSiteManagerRunnerMessage messageToSend = ((LayoutSiteManagerRunnerMessage)message.GetInternalObject());
            messageToSend.ManagerId = message.ManagerId;
            messageToSend.RunnerId = _session.LoginId;
            _response = new GestorMensajes().BL_SendMessageTo(_session, messageToSend);
            _response.Message.RunnerUserName = _session.Login.Login.ToUpper();
            if (GestorSesiones.LogMessage(_session, MessageType.DATA_TareasOperador))
            {
              _additionalText = "OPERADOR: [" + _session.Login.Login + "|" + _session.Login.UserName + "]";
              GestorLogs.LogSoap((Int16)LogID.WS_NET_MSG, _additionalText);

              _additionalTextadd = "<SendMessageTo>" + _response.ResponseCode + ";" + _response.ResponseCodeDescription;
              _additionalTextadd += ";IdSession:" + _response.SessionId;
              GestorLogs.LogSoap((Int16)LogID.WS_NET_MSG, _additionalText + " -->  " + _additionalTextadd);
            }
          }
          else
          {
            _response.ResponseCode = ResponseCode.DATA_RC_SESSION_TIMEOUT;
            _response.ResponseCodeDescription = WSI.Common.Resource.String("STR_SESSION_TIME_OUT");
          }
        }
        else if (_response.ResponseCode == ResponseCode.DATA_RC_BAD_LOGIN)
        {
          _additionalText = "";
          GestorLogs.LogSoap((Int16)LogID.WS_NET_MSG, _additionalText);

          _additionalTextadd = "<SendMessageTo>" + _response.ResponseCode + ";" + _response.ResponseCodeDescription;
          _additionalTextadd += ";IdSession:" + _response.SessionId;
          GestorLogs.LogSoap((Int16)LogID.WS_NET_MSG, _additionalText + " -->  " + _additionalTextadd);
        }

        return new DATA_SendMessageResponse(_response);
      }
      catch (DataServiceException Ex)
      {
        GestorLogs.LogException(Ex.Message, Ex);
        return new DATA_SendMessageResponse(GestorErrores.GetSendMessageToException(Ex.ResponseCode, Ex.ResponseDescription));
      }
      catch (Exception Ex)
      {
        GestorLogs.LogException(Ex.Message, Ex);
        return new DATA_SendMessageResponse(GestorErrores.GetSendMessageToException(ResponseCode.DATA_RC_BAD_MESSAGE_DATA, WSI.Common.Resource.String("STR_WS_DATA_ERROR_SENDING_MESSAGE")));
      }
    }

    internal static DATA_GetConversationHistoryResponse Data_GetConversationHistory(DATA_GetConversationHistoryRequest conversationHistory)
    {
      Session _session;
      String _additionalText;
      String _additionalTextadd;

      try
      {
        DTO_GetConversationHistoryResponse _response = new DTO_GetConversationHistoryResponse();

        if (!String.IsNullOrEmpty(conversationHistory.SessionId))
        {

          if (GestorSesiones.ExistIdSession(conversationHistory.SessionId))
          {
            _session = GestorSesiones.GetSessionForRegistrosLinea(conversationHistory.SessionId);
            _response = new GestorMensajes().BL_GetConversationHistory(_session, conversationHistory.ManagerId);

            if (GestorSesiones.LogMessage(_session, MessageType.DATA_TareasOperador))
            {
              _additionalText = "OPERADOR: [" + _session.Login.Login + "|" + _session.Login.UserName + "]";
              GestorLogs.LogSoap((Int16)LogID.WS_NET_MSG, _additionalText);

              _additionalTextadd = "<Data_GetConversationHistory>" + _response.ResponseCode + ";" + _response.ResponseCodeDescription;
              _additionalTextadd += ";IdSession:" + _response.SessionId;
              GestorLogs.LogSoap((Int16)LogID.WS_NET_MSG, _additionalText + " -->  " + _additionalTextadd);
            }
          }
          else
          {
            _response.ResponseCode = ResponseCode.DATA_RC_SESSION_TIMEOUT;
            _response.ResponseCodeDescription = WSI.Common.Resource.String("STR_SESSION_TIME_OUT");
          }
        }
        else if (_response.ResponseCode == ResponseCode.DATA_RC_BAD_LOGIN)
        {
          _additionalText = "";
          GestorLogs.LogSoap((Int16)LogID.WS_NET_MSG, _additionalText);

          _additionalTextadd = "<Data_GetConversationHistory>" + _response.ResponseCode + ";" + _response.ResponseCodeDescription;
          _additionalTextadd += ";IdSession:" + _response.SessionId;
          GestorLogs.LogSoap((Int16)LogID.WS_NET_MSG, _additionalText + " -->  " + _additionalTextadd);
        }


        return new DATA_GetConversationHistoryResponse(_response);
      }
      catch (DataServiceException Ex)
      {
        GestorLogs.LogException(Ex.Message, Ex);
        return new DATA_GetConversationHistoryResponse(GestorErrores.GetConversationHistoryException(Ex.ResponseCode, Ex.ResponseDescription));
      }
      catch (Exception Ex)
      {
        GestorLogs.LogException(Ex.Message, Ex);
        return new DATA_GetConversationHistoryResponse(GestorErrores.GetConversationHistoryException(ResponseCode.DATA_RC_BAD_MESSAGE_DATA, WSI.Common.Resource.String("STR_WS_DATA_ERROR_CONVERSATION_HISTORY")));
      }
    }

    internal static DATA_GetConversationsOfTodayResponse Data_GetConversationsOfToday(DATA_GetConversationsOfTodayRequest conversationHistory)
    {
      Session _session;
      String _additionalText;
      String _additionalTextadd;

      try
      {
        DTO_GetConversationsOfTodayResponse _response = new DTO_GetConversationsOfTodayResponse();

        if (!String.IsNullOrEmpty(conversationHistory.SessionId))
        {

          if (GestorSesiones.ExistIdSession(conversationHistory.SessionId))
          {
            _session = GestorSesiones.GetSessionForRegistrosLinea(conversationHistory.SessionId);
            _response = new GestorMensajes().BL_GetConversationsOfToday(_session, _session.LoginId);

            if (GestorSesiones.LogMessage(_session, MessageType.DATA_TareasOperador))
            {
              _additionalText = "OPERADOR: [" + _session.Login.Login + "|" + _session.Login.UserName + "]";
              GestorLogs.LogSoap((Int16)LogID.WS_NET_MSG, _additionalText);

              _additionalTextadd = "<Data_GetConversationsOfToday>" + _response.ResponseCode + ";" + _response.ResponseCodeDescription;
              _additionalTextadd += ";IdSession:" + _response.SessionId;
              GestorLogs.LogSoap((Int16)LogID.WS_NET_MSG, _additionalText + " -->  " + _additionalTextadd);
            }
          }
          else
          {
            _response.ResponseCode = ResponseCode.DATA_RC_SESSION_TIMEOUT;
            _response.ResponseCodeDescription = WSI.Common.Resource.String("STR_SESSION_TIME_OUT");
          }
        }
        else if (_response.ResponseCode == ResponseCode.DATA_RC_BAD_LOGIN)
        {
          _additionalText = "";
          GestorLogs.LogSoap((Int16)LogID.WS_NET_MSG, _additionalText);

          _additionalTextadd = "<Data_GetConversationsOfToday>" + _response.ResponseCode + ";" + _response.ResponseCodeDescription;
          _additionalTextadd += ";IdSession:" + _response.SessionId;
          GestorLogs.LogSoap((Int16)LogID.WS_NET_MSG, _additionalText + " -->  " + _additionalTextadd);
        }


        return new DATA_GetConversationsOfTodayResponse(_response);
      }
      catch (DataServiceException Ex)
      {
        GestorLogs.LogException(Ex.Message, Ex);
        return new DATA_GetConversationsOfTodayResponse(GestorErrores.GetConversationsOfTodayException(Ex.ResponseCode, Ex.ResponseDescription));
      }
      catch (Exception Ex)
      {
        GestorLogs.LogException(Ex.Message, Ex);
        return new DATA_GetConversationsOfTodayResponse(GestorErrores.GetConversationsOfTodayException(ResponseCode.DATA_RC_BAD_MESSAGE_DATA, WSI.Common.Resource.String("STR_WS_DATA_ERROR_CONVERSATION_OF_TODAY")));
      }
    }

    internal static DATA_PostActionResponse DATA_KeepALive(DATA_KeepALive keepALive)
    {
      Session _session;
      String _additionalText;
      String _additionalTextadd;

      try
      {
        DTO_KeepALiveResponse _response = new DTO_KeepALiveResponse();

        if (!String.IsNullOrEmpty(keepALive.SessionId))
        {
          if (GestorSesiones.ExistIdSession(keepALive.SessionId))
          {
            _session = GestorSesiones.GetSessionForRegistrosLinea(keepALive.SessionId);

            if (GestorSesiones.LogMessage(_session, MessageType.DATA_TareasOperador))
            {
              _additionalText = "OPERADOR: [" + _session.Login.Login + "|" + _session.Login.UserName + "]";
              GestorLogs.LogSoap((Int16)LogID.WS_NET_MSG, _additionalText);

              _additionalTextadd = "<DATA_KeepALive>" + _response.ResponseCode + ";" + _response.ResponseCodeDescription;
              _additionalTextadd += ";IdSession:" + _response.SessionId;
              GestorLogs.LogSoap((Int16)LogID.WS_NET_MSG, _additionalText + " -->  " + _additionalTextadd);
            }
            _response.ResponseCode = ResponseCode.DATA_RC_OK;
            _response.ResponseCodeDescription = WSI.Common.Resource.String("STR_SESSION_STR1");
            _response.SessionId = _session.SessionId;
          }
          else
          {
            _response.ResponseCode = ResponseCode.DATA_RC_SESSION_TIMEOUT;
            _response.ResponseCodeDescription = WSI.Common.Resource.String("STR_SESSION_TIME_OUT");
          }
        }
        else if (_response.ResponseCode == ResponseCode.DATA_RC_BAD_LOGIN)
        {
          _additionalText = "";
          GestorLogs.LogSoap((Int16)LogID.WS_NET_MSG, _additionalText);

          _additionalTextadd = "<DATA_KeepALive>" + _response.ResponseCode + ";" + _response.ResponseCodeDescription;
          _additionalTextadd += ";IdSession:" + _response.SessionId;
          GestorLogs.LogSoap((Int16)LogID.WS_NET_MSG, _additionalText + " -->  " + _additionalTextadd);
        }


        return new DATA_PostActionResponse(_response);
      }
      catch (DataServiceException Ex)
      {
        GestorLogs.LogException(Ex.Message, Ex);
        return new DATA_PostActionResponse(GestorErrores.GetKeepALiveException(Ex.ResponseCode, Ex.ResponseDescription));
      }
      catch (Exception Ex)
      {
        GestorLogs.LogException(Ex.Message, Ex);
        return new DATA_PostActionResponse(GestorErrores.GetKeepALiveException(ResponseCode.DATA_RC_BAD_MESSAGE_DATA, WSI.Common.Resource.String("STR_WS_DATA_ERROR_KEEP_A_LIVE")));
      }
    }

    internal static DATA_UpdateApplicationResponse Data_UpdateApplication(DATA_UpdateApplicationRequest update)
    {
      Session _session;
      String _additionalText;
      String _additionalTextadd;

      try
      {
        DTO_UpdateApplicationResponse _response = new DTO_UpdateApplicationResponse();

        if (!String.IsNullOrEmpty(update.SessionId))
        {

          if (GestorSesiones.ExistIdSession(update.SessionId))
          {
            _session = GestorSesiones.GetSessionForRegistrosLinea(update.SessionId);
            _response = new GestorMensajes().BL_UpdateApplication(_session);

            if (GestorSesiones.LogMessage(_session, MessageType.DATA_TareasOperador))
            {
              _additionalText = "OPERADOR: [" + _session.Login.Login + "|" + _session.Login.UserName + "]";
              GestorLogs.LogSoap((Int16)LogID.WS_NET_MSG, _additionalText);

              _additionalTextadd = "<Data_UpdateApplication>" + _response.ResponseCode + ";" + _response.ResponseCodeDescription;
              _additionalTextadd += ";IdSession:" + _response.SessionId;
              GestorLogs.LogSoap((Int16)LogID.WS_NET_MSG, _additionalText + " -->  " + _additionalTextadd);
            }
          }
          else
          {
            _response.ResponseCode = ResponseCode.DATA_RC_SESSION_TIMEOUT;
            _response.ResponseCodeDescription = WSI.Common.Resource.String("STR_SESSION_TIME_OUT");
          }
        }
        else if (_response.ResponseCode == ResponseCode.DATA_RC_BAD_LOGIN)
        {
          _additionalText = "";
          GestorLogs.LogSoap((Int16)LogID.WS_NET_MSG, _additionalText);

          _additionalTextadd = "<Data_UpdateApplication>" + _response.ResponseCode + ";" + _response.ResponseCodeDescription;
          _additionalTextadd += ";IdSession:" + _response.SessionId;
          GestorLogs.LogSoap((Int16)LogID.WS_NET_MSG, _additionalText + " -->  " + _additionalTextadd);
        }


        return new DATA_UpdateApplicationResponse(_response);
      }
      catch (DataServiceException Ex)
      {
        GestorLogs.LogException(Ex.Message, Ex);
        return new DATA_UpdateApplicationResponse(GestorErrores.GetUpdateApplicationException(Ex.ResponseCode, Ex.ResponseDescription));
      }
      catch (Exception Ex)
      {
        GestorLogs.LogException(Ex.Message, Ex);
        return new DATA_UpdateApplicationResponse(GestorErrores.GetUpdateApplicationException(ResponseCode.DATA_RC_BAD_MESSAGE_DATA, WSI.Common.Resource.String("STR_WS_DATA_ERROR_UPDATING_APPLICATION")));
      }
    }

    internal static DATA_SaveLogResponse Data_SaveLog(DATA_SaveLogRequest request)
    {
      Session _session;
      String _additionalText;
      String _additionalTextadd;

      try
      {
        DTO_SaveLogResponse _response = new DTO_SaveLogResponse();

        if (!String.IsNullOrEmpty(request.SessionId))
        {

          if (GestorSesiones.ExistIdSession(request.SessionId))
          {

            _session = GestorSesiones.GetSessionForRegistrosLinea(request.SessionId);
            _response = new GestorMensajes().BL_SaveLog((SaveLog)request.GetInternalObject(), _session);

            if (GestorSesiones.LogMessage(_session, MessageType.DATA_GuardarRegistros))
            {
              _additionalText = "OPERADOR: [" + _session.Login.Login + "|" + _session.Login.UserName + "]";
              GestorLogs.LogSoap((Int16)LogID.WS_NET_MSG, _additionalText);

              _additionalTextadd = "<DATA_GuardarRegistros>" + _response.ResponseCode + ";" + _response.ResponseCodeDescription;
              _additionalTextadd += ";IdSession:" + _response.SessionId;
              GestorLogs.LogSoap((Int16)LogID.WS_NET_MSG, _additionalText + " -->  " + _additionalTextadd);
            }
          }
          else
          {
            _response.ResponseCode = ResponseCode.DATA_RC_SESSION_TIMEOUT;
            _response.ResponseCodeDescription = WSI.Common.Resource.String("STR_SESSION_TIME_OUT");
          }
        }
        else if (_response.ResponseCode == ResponseCode.DATA_RC_BAD_LOGIN)
        {
          _additionalText = "";
          GestorLogs.LogSoap((Int16)LogID.WS_NET_MSG, _additionalText);

          _additionalTextadd = "<DATA_GuardarRegistros>" + _response.ResponseCode + ";" + _response.ResponseCodeDescription;
          _additionalTextadd += ";IdSession:" + _response.SessionId;
          GestorLogs.LogSoap((Int16)LogID.WS_NET_MSG, _additionalText + " -->  " + _additionalTextadd);
        }


        return new DATA_SaveLogResponse(_response);
      }
      catch (DataServiceException Ex)
      {
        GestorLogs.LogException(Ex.Message, Ex);
        return new DATA_SaveLogResponse(GestorErrores.GetSaveLogException(Ex.ResponseCode, Ex.ResponseDescription));
      }
      catch (Exception Ex)
      {
        GestorLogs.LogException(Ex.Message, Ex);
        return new DATA_SaveLogResponse(GestorErrores.GetSaveLogException(ResponseCode.DATA_RC_BAD_MESSAGE_DATA, WSI.Common.Resource.String("STR_WS_DATA_ERROR_SAVING_LOGS")));
      }

    }
  }
}