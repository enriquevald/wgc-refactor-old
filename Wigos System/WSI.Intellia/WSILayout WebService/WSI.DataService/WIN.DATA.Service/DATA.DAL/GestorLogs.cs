﻿////------------------------------------------------------------------------------
//// Copyright © 2012 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: GestorLogs.cs
//// 
////      DESCRIPTION: Object Class manage the logs 
//// 
////           AUTHOR: 
//// 
////    CREATION DATE: 10-OCT-2012
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 05-NOV-2012        First Version
////------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WSI.Common;
using DATA.OBJ;

namespace DATA.DAL
{
  public static class GestorLogs
  {
    //------------------------------------------------------------------------------
    // PURPOSE: Generates the log for the exception
    //
    //  PARAMS:
    //      - INPUT: String Message
    //          -    Exception Ex
    //
    //      - OUTPUT:
    //
    // RETURNS:
    public static void LogException(String Message, Exception Ex, Int64 IdOperador)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (Alarm.Register(AlarmSourceCode.WebService, 0, WSI.Common.Resource.String("STR_WS_DATA_APP_NAME"), (UInt32)AlarmCode.WebService_Session_InternalError, WSI.Common.Resource.String("STR_WS_DATA_APP_NAME2", Message), IdOperador, 0, 0, AlarmSeverity.Error, DateTime.Now, _db_trx.SqlTransaction))
        {
          _db_trx.Commit();
        }
      }

      Log.Exception(Ex);
    }

    public static void LogSoap(String Message)
    {
      LogSoap((Int16)LogID.WS_LOG, Message);
    }

    // Log SOAP
    public static void LogSoap(Int16 LogId, String Message)
    {
      Log.Message(LogId, Message);
    }
  }
}
