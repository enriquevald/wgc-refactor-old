﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DATA.OBJ.DTO;
using System.Data;
using System.Data.SqlClient;
using WSI.Common;
using DATA.OBJ;
using System.Threading;
using System.Configuration;

namespace DATA.DAL
{
  public partial class DbUnitariosConnection
  {


    public DataTable DataBaseGetLayoutSiteAssignedTasks(long loginId)
    {
      SqlDataAdapter _adapter;
      DataTable _data;

      _data = new DataTable();
      String storedProcedure = string.Empty;
      if (ConfigurationManager.AppSettings.Get("TestMode").Equals("true"))
        storedProcedure = "Layout_GetAssignedTaskToUser_Test";
      else
        storedProcedure = "Layout_GetAssignedTaskToUser";
      using (DB_TRX _transaction = new DB_TRX())
      {
        using (SqlCommand _command = new SqlCommand(storedProcedure, _transaction.SqlTransaction.Connection, _transaction.SqlTransaction))
        {
          _command.CommandType = CommandType.StoredProcedure;
          _command.Parameters.Add("@loginId", SqlDbType.BigInt).Value = loginId;

          _adapter = new SqlDataAdapter(_command);
          _adapter.Fill(_data);
        }
      }
      return _data;
    }
    public DataTable DataBaseGetLayoutSiteAlarms(long terminalId)
    {
      SqlDataAdapter _adapter;
      DataTable _data;

      _data = new DataTable();
      using (DB_TRX _transaction = new DB_TRX())
      {
        using (SqlCommand _command = new SqlCommand(GetLayoutSiteAlarmsByTerminalId(), _transaction.SqlTransaction.Connection, _transaction.SqlTransaction))
        {
          _command.Parameters.Add("@terminalId", SqlDbType.Int).Value = terminalId;

          _adapter = new SqlDataAdapter(_command);
          _adapter.Fill(_data);
        }
      }
      return _data;
    }

    public DataTable DataBaseGetAlarmsCreatedByUser(long loginId)
    {
      SqlDataAdapter _adapter;
      DataTable _data;

      _data = new DataTable();
      using (DB_TRX _transaction = new DB_TRX())
      {
        using (SqlCommand _command = new SqlCommand(GetLayoutSiteAlarmsByUserId(), _transaction.SqlTransaction.Connection, _transaction.SqlTransaction))
        {
          _command.Parameters.Add("@loginId", SqlDbType.Int).Value = loginId;

          _adapter = new SqlDataAdapter(_command);
          _adapter.Fill(_data);
        }
      }
      return _data;
    }
    public IEnumerable<String> DataBaseGetUserByRoleId(string roleId)
    {
      String _device_id = string.Empty;
      List<String> _devices = new List<String>();

      SqlDataAdapter _adapter;
      DataTable _data;
      _data = new DataTable();
      using (DB_TRX _transaction = new DB_TRX())
      {
        using (SqlCommand _cmd = new SqlCommand("Layout_GetUsersIdByRoleId", _transaction.SqlTransaction.Connection, _transaction.SqlTransaction))
        {
          _cmd.CommandType = CommandType.StoredProcedure;
          _cmd.Parameters.Add("@pRoleId", SqlDbType.NVarChar).Value = roleId;
          _adapter = new SqlDataAdapter(_cmd);
          _adapter.Fill(_data);
        }

        foreach (DataRow row in _data.Rows)
        {
          _devices.Add(row["GU_USER_ID"].ToString());
        }
      }


      return _devices;
    }
    public LayoutSiteTask DataBaseGetTaskById(long taskId)
    {
      SqlDataAdapter _adapter;
      DataTable _data;
      LayoutSiteTask _result;

      _data = new DataTable();
      using (DB_TRX _transaction = new DB_TRX())
      {
        using (SqlCommand _command = new SqlCommand(GetLayoutSiteTaskById(), _transaction.SqlTransaction.Connection, _transaction.SqlTransaction))
        {
          _command.Parameters.Add("@taskId", SqlDbType.Int).Value = taskId;

          _adapter = new SqlDataAdapter(_command);
          _adapter.Fill(_data);

          if (_data.Rows.Count > 0)
          {
            _result = new LayoutSiteTask(_data.Rows[0]);
            return _result;
          }
        }
      }
      return null;
    }

    public void DataBaseUpdateLayoutSiteTaskInfo(LayoutTaskInfo info, long loginId, String UserName)
    {

      try
      {

        using (DB_TRX _transaction = new DB_TRX())
        {
          SqlDataAdapter _adapter;
          _adapter = new SqlDataAdapter();

          String _query_info;
          StringBuilder _query = new StringBuilder();
          String _event_history = String.Empty;
          String _str_status = String.Empty;
          String _nls_status = String.Empty;
          String _reason = String.Empty;
          System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo("en-US");

          _query.AppendLine("UPDATE   layout_site_tasks");
          switch (info.Action)
          {
            case LayoutSiteTaskActions.Assigned:
              _query.AppendLine("   SET   lst_assigned_user_id  = @userId");
              _query.AppendLine("        ,lst_assigned = GETDATE() ");
              _str_status = WSI.Common.Resource.String("STR_LAYOUT_TASK_ASSIGNED");
              _nls_status = "NLS_STATE_PENDING";

              break;

            case LayoutSiteTaskActions.Accepted:
              _query.AppendLine("   SET   lst_accepted_user_id  = @userId");
              _query.AppendLine("        ,lst_accepted = GETDATE() ");
              _query.AppendLine("        ,lst_start = GETDATE() ");
              _str_status = WSI.Common.Resource.String("STR_LAYOUT_TASK_ACCEPTED");
              _nls_status = "NLS_STATE_IN_PROGRESS";

              break;

            case LayoutSiteTaskActions.Scaled:
              _query.AppendLine("   SET   lst_scale_from_user_id  = @userId");
              _query.AppendLine("        ,lst_scale = GETDATE() ");
              _query.AppendLine("        ,lst_accepted = NULL ");
              _query.AppendLine("        ,lst_accepted_user_id = NULL ");
              _query.AppendLine("        ,lst_assigned_user_id = NULL ");
              _query.AppendLine("        ,lst_scale_reason = @reason ");
              _query.AppendLine("        ,lst_assigned = NULL ");
              _str_status = WSI.Common.Resource.String("STR_LAYOUT_TASK_SCALED");
              _nls_status = "NLS_STATE_SCALED";

              _reason = "<br><div class=''taskHistory''><i>" + WASDB.Now.ToString("G",culture) + "</i>  " + UserName.ToUpper() + "  <b><span data-nls=''NLS_STATUS_CHANGED_REASON''></span>:</b>&nbsp;<span>" + info.Reason + "</span></div>";
              break;

            case LayoutSiteTaskActions.Resolved:
              _query.AppendLine("   SET   lst_solved_user_id  = @userId");
              _query.AppendLine("        ,lst_solved = GETDATE() ");
              _query.AppendLine("        ,lst_end = GETDATE() ");
              _str_status = WSI.Common.Resource.String("STR_LAYOUT_TASK_RESOLVED");
              _nls_status = "NLS_STATE_SOLVED";

              break;

            default:
              break;
          }

          //JBC Each event generates a historical message and it's saved into database.
          //_event_history = "<br><div class=''taskHistory''><i>" + Misc.GetDateTimeCurrentCulture(WASDB.Now.) + "</i>  " + UserName.ToUpper() + "  <span data-nls=''NLS_STATUS_CHANGED''></span>[<span data-nls=''" + _nls_status + "''>status</span>]</div>" + _reason;
          _event_history = "<br><div class=''taskHistory''><i>" + WASDB.Now.ToString("G",culture) + "</i>  " + UserName.ToUpper() + "  <span data-nls=''NLS_STATUS_CHANGED''></span>[<span data-nls=''" + _nls_status + "''>status</span>]</div>" + _reason;

          _query.AppendLine("        ,lst_events_history = '" + _event_history + "' + ISNULL(lst_events_history,'') ");
          _query.AppendLine("        ,lst_status = @status");
          _query.AppendLine(" WHERE   lst_id = @taskId");

          _query_info = _query.ToString();

          if (info.Action == LayoutSiteTaskActions.Scaled)
          {
            SqlParameter _derivations_left;

            using (SqlCommand _command = new SqlCommand("Layout_ReleaseAlarmFromTask", _transaction.SqlTransaction.Connection, _transaction.SqlTransaction))
            {
              _command.CommandType = CommandType.StoredProcedure;
              _command.Parameters.Add("@pTaskId", SqlDbType.Int).Value = info.TaskId;
              _command.Parameters.Add("@pUserId", SqlDbType.BigInt).Value = loginId;
              _derivations_left = _command.Parameters.Add("@pOutputDerivations", SqlDbType.Int);
              _derivations_left.Direction = ParameterDirection.Output;
              _command.ExecuteNonQuery();
            }

            //if it is the last user that scales the task, it changes to Scaled
            if ((int)_derivations_left.Value == 0)
            {
              using (SqlCommand _command = new SqlCommand(_query_info, _transaction.SqlTransaction.Connection, _transaction.SqlTransaction))
              {
                _command.Parameters.Add("@userId", SqlDbType.BigInt).Value = loginId;
                _command.Parameters.Add("@taskId", SqlDbType.BigInt).Value = info.TaskId;
                _command.Parameters.Add("@status", SqlDbType.Int).Value = (int)info.Action;
                _command.Parameters.Add("@reason", SqlDbType.NVarChar).Value = info.Reason;
                _command.ExecuteNonQuery();
              }
            }
            //SRL 15/9/2017 - Bug Fix INT-49
            //if it is not the las user it remains pending and save history of scale
            else
            {
              using (SqlCommand _command = new SqlCommand(_query_info, _transaction.SqlTransaction.Connection, _transaction.SqlTransaction))
              {
                _command.Parameters.Add("@userId", SqlDbType.BigInt).Value = loginId;
                _command.Parameters.Add("@taskId", SqlDbType.BigInt).Value = info.TaskId;
                _command.Parameters.Add("@status", SqlDbType.Int).Value = (int)LayoutSiteTaskActions.Assigned;
                _command.Parameters.Add("@reason", SqlDbType.NVarChar).Value = info.Reason;
                _command.ExecuteNonQuery();
              }
            }
          }
          else
          {
            using (SqlCommand _command = new SqlCommand(_query_info, _transaction.SqlTransaction.Connection, _transaction.SqlTransaction))
            {
              _command.Parameters.Add("@userId", SqlDbType.BigInt).Value = loginId;
              _command.Parameters.Add("@taskId", SqlDbType.BigInt).Value = info.TaskId;
              _command.Parameters.Add("@status", SqlDbType.Int).Value = (int)info.Action;
              _command.Parameters.Add("@reason", SqlDbType.NVarChar).Value = info.Reason;
              _command.ExecuteNonQuery();
            }

            if (info.Action == LayoutSiteTaskActions.Accepted)
            {
              using (SqlCommand _command = new SqlCommand("Layout_ReleaseUserDerivation", _transaction.SqlTransaction.Connection, _transaction.SqlTransaction))
              {
                _command.CommandType = CommandType.StoredProcedure;
                _command.Parameters.Add("@pTaskId", SqlDbType.Int).Value = info.TaskId;
                _command.ExecuteNonQuery();
              }
            }
          }

          _transaction.Commit();
        }

      }
      catch (Exception Ex)
      {
        GestorLogs.LogException(Ex.Message, Ex, loginId);
        throw new DataServiceException(ResponseCode.DATA_RC_INTERNAL_DATA_ERROR, WSI.Common.Resource.String("STR_WS_DATA_ERROR_SAVE_DATOS", " - Catalogos Operador"));
      }

    }


    public void DataBaseUpdateBeaconRunnerPosition(LayoutRunnersPosition info, long loginId)
    {
      try
      {
        using (DB_TRX _transaction = new DB_TRX())
        {
          SqlDataAdapter _adapter;
          _adapter = new SqlDataAdapter();

          using (SqlCommand _command = new SqlCommand("Layout_SaveLayoutRunnersPosition", _transaction.SqlTransaction.Connection, _transaction.SqlTransaction))
          {
            //Log.Message(String.Format("RUNNER POSITION ORIGINAL VALUES * ORIGINAL X: {0} - ORIGINAL Y: {1} - DEVICE ID: {2}", info.PositionX, info.PositionY, info.DeviceId));
            _command.CommandType = CommandType.StoredProcedure;
            _command.Parameters.Add("@pUserId", SqlDbType.BigInt).Value = loginId;
            _command.Parameters.Add("@pDeviceId", SqlDbType.NVarChar).Value = info.DeviceId;
            _command.Parameters.Add("@pPositionX", SqlDbType.Float).Value = Convert.ToInt32(Math.Round(info.PositionX));
            _command.Parameters.Add("@pPositionY", SqlDbType.Float).Value = Convert.ToInt32(Math.Round(info.PositionY));
            _command.Parameters.Add("@pPositionFloor", SqlDbType.Int).Value = info.FloorId;
            _command.Parameters.Add("@pRange", SqlDbType.Float).Value = info.Range;
            _command.Parameters.Add("@pNearBeacon", SqlDbType.NVarChar).Value = info.NearBeacon;
            _command.ExecuteNonQuery();
          }

          _transaction.Commit();
        }

      }
      catch (Exception Ex)
      {
        GestorLogs.LogException(Ex.Message, Ex, loginId);
        throw new DataServiceException(ResponseCode.DATA_RC_INTERNAL_DATA_ERROR, WSI.Common.Resource.String("STR_WS_DATA_ERROR_SAVE_DATOS", " - Catalogos Operador"));
      }

    }

    public void DataBaseSaveLog(SaveLog info, long loginId)
    {

      try
      {

        using (DB_TRX _transaction = new DB_TRX())
        {
          using (SqlCommand _command = new SqlCommand("Layout_SaveExternalLogs", _transaction.SqlTransaction.Connection, _transaction.SqlTransaction))
          {
            var dateNow = WASDB.Now;
            _command.CommandType = CommandType.StoredProcedure;

            _command.Parameters.Add("@pDatetime", SqlDbType.DateTime).Value = info.DateTime;
            _command.Parameters.Add("@pSource", SqlDbType.NVarChar).Value = info.Source;
            _command.Parameters.Add("@pDevice_id", SqlDbType.NVarChar).Value = info.DeviceId;
            _command.Parameters.Add("@pUser_id", SqlDbType.Int).Value = string.IsNullOrEmpty(info.UserId) ? -1 : Convert.ToInt32(info.UserId);
            _command.Parameters.Add("@pContent", SqlDbType.Binary).Value = Encoding.UTF8.GetBytes(info.Content);
            _command.ExecuteNonQuery();
          }

          _transaction.Commit();
        }

      }
      catch (Exception Ex)
      {
        GestorLogs.LogException(Ex.Message, Ex, loginId);
        throw new DataServiceException(ResponseCode.DATA_RC_INTERNAL_DATA_ERROR, WSI.Common.Resource.String("STR_WS_DATA_ERROR_SAVING_LOGS", ""));
      }

    }

    public void DataBaseCreateAlarm(CreateAlarm info, long loginId, long imageId)
    {

      try
      {

        using (DB_TRX _transaction = new DB_TRX())
        {
          using (SqlCommand _command = new SqlCommand("Layout_SaveSiteAlarm", _transaction.SqlTransaction.Connection, _transaction.SqlTransaction))
          {
            var dateNow = WASDB.Now;
            _command.CommandType = CommandType.StoredProcedure;

            _command.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = int.Parse(info.TerminalId);
            _command.Parameters.Add("@pAlarmId", SqlDbType.BigInt).Value = info.SubCategory;
            _command.Parameters.Add("@pDateCreated", SqlDbType.DateTime).Value = dateNow.AddMilliseconds(-dateNow.Millisecond);
            _command.Parameters.Add("@pAlarmType", SqlDbType.Int).Value = info.Category;
            _command.Parameters.Add("@pAlarmSource", SqlDbType.Int).Value = 1;
            _command.Parameters.Add("@pUserCreated", SqlDbType.Int).Value = loginId;
            if (imageId != 0)
              _command.Parameters.Add("@pMediaId", SqlDbType.BigInt).Value = imageId;
            else
              _command.Parameters.Add("@pMediaId", SqlDbType.BigInt).Value = System.DBNull.Value;
            _command.Parameters.Add("@pDescription", SqlDbType.NVarChar).Value = info.Description;
            _command.Parameters.Add("@pDateToTask", SqlDbType.DateTime).Value = System.DBNull.Value;
            _command.Parameters.Add("@pTaskId", SqlDbType.BigInt).Value = System.DBNull.Value;
            _command.Parameters.Add("@pStatus", SqlDbType.Int).Value = (int)LayoutSiteAlarmStatus.Active;
            if (!string.IsNullOrEmpty(info.Title))
              _command.Parameters.Add("@pTitle", SqlDbType.NVarChar).Value = info.Title;
            else
              _command.Parameters.Add("@pTitle", SqlDbType.NVarChar).Value = System.DBNull.Value;
            _command.ExecuteNonQuery();
          }

          _transaction.Commit();
        }

      }
      catch (Exception Ex)
      {
        GestorLogs.LogException(Ex.Message, Ex, loginId);
        throw new DataServiceException(ResponseCode.DATA_RC_INTERNAL_DATA_ERROR, WSI.Common.Resource.String("STR_WS_DATA_ERROR_SAVE_DATOS", "Layout site alarms"));
      }

    }
    public string DataBaseGetLogFromTerminalByTerminalId(string terminalInfo)
    {
      try
      {
        long id;
        //Insert command
        using (DB_TRX _transaction = new DB_TRX())
        {
          StringBuilder _query = new StringBuilder();
          _query.AppendLine("INSERT INTO WCP_COMMANDS(");
          _query.AppendLine("CMD_TERMINAL_ID");
          _query.AppendLine(",CMD_CODE");
          _query.AppendLine(",CMD_STATUS");
          _query.AppendLine(") VALUES ( @terminalId, @code, 0);");
          _query.AppendLine("SELECT CAST(scope_identity() AS int)");

          using (SqlCommand _command = new SqlCommand(_query.ToString(), _transaction.SqlTransaction.Connection, _transaction.SqlTransaction))
          {
            _command.Parameters.Add("@terminalId", SqlDbType.Int).Value = int.Parse(terminalInfo);
            _command.Parameters.Add("@code", SqlDbType.NVarChar).Value = 1;//Logger
            id = Convert.ToInt64(_command.ExecuteScalar());
          }

          _transaction.Commit();
        }

        //Waiting WCP listend the command and response
        Thread.Sleep(3000);

        //Get the responsed log
        DataTable _data = new DataTable();
        using (DB_TRX _transaction = new DB_TRX())
        {
          SqlDataAdapter _adapter;
          _adapter = new SqlDataAdapter();

          StringBuilder _query = new StringBuilder();
          _query.AppendLine("SELECT TE_NAME");
          _query.AppendLine(", TE_PROVIDER_ID");
          _query.AppendLine(", CMD_CREATED");
          _query.AppendLine(", CMD_RESPONSE");
          _query.AppendLine("FROM WCP_COMMANDS, TERMINALS");
          _query.AppendLine("WHERE CMD_TERMINAL_ID = TE_TERMINAL_ID");
          _query.AppendLine("AND CMD_ID = @cmd_id");


          using (SqlCommand _command = new SqlCommand(_query.ToString(), _transaction.SqlTransaction.Connection, _transaction.SqlTransaction))
          {
            _command.Parameters.Add("@cmd_id", SqlDbType.Int).Value = id;
            _adapter = new SqlDataAdapter(_command);
            _adapter.Fill(_data);
          }

          _transaction.Commit();
        }

        if (_data.Rows.Count > 0)
        {
          String response = _data.Rows[0]["CMD_RESPONSE"].ToString();
          if (string.IsNullOrEmpty(response))
            return string.Empty;
          else
          {
            String finalLogger = string.Empty;
            Misc.InflateString(response, out finalLogger);
            return finalLogger;
          }
        }

        return string.Empty;

      }
      catch (Exception Ex)
      {
        GestorLogs.LogException(Ex.Message, Ex, long.Parse(terminalInfo));
        throw new DataServiceException(ResponseCode.DATA_RC_INTERNAL_DATA_ERROR, WSI.Common.Resource.String("STR_WS_DATA_ERROR_SAVE_DATOS", " - Catalogos Operador"));
      }
    }

    public long DataBaseUploadLayoutSiteMediaAsset(LayoutSiteMediaAsset info, long loginId)
    {
      try
      {

        long id;
        using (DB_TRX _transaction = new DB_TRX())
        {
          SqlDataAdapter _adapter;
          _adapter = new SqlDataAdapter();

          String _query_info;
          StringBuilder _query = new StringBuilder();
          _query.AppendLine("INSERT INTO layout_media(");
          _query.AppendLine("lm_data");
          _query.AppendLine(", lm_created");
          _query.AppendLine(", lm_user_created");
          _query.AppendLine(", lm_type");
          _query.AppendLine(", lm_description");
          _query.AppendLine(", lm_media_type");
          _query.AppendLine(", lm_external_id");
          _query.AppendLine(", lm_data_thumbnail");
          _query.AppendLine(")");
          _query.AppendLine("VALUES(@data");
          _query.AppendLine(", GETDATE()");
          _query.AppendLine(", @userId");
          _query.AppendLine(", @type");
          _query.AppendLine(", @description");
          _query.AppendLine(", @mediaType");
          _query.AppendLine(", @externalId");
          _query.AppendLine(", @thumbnail");
          _query.AppendLine(");");
          _query.AppendLine("SELECT CAST(scope_identity() AS int)");

          _query_info = _query.ToString();


          using (SqlCommand _command = new SqlCommand(_query_info, _transaction.SqlTransaction.Connection, _transaction.SqlTransaction))
          {
            _command.Parameters.Add("@userId", SqlDbType.BigInt).Value = loginId;
            _command.Parameters.Add("@data", SqlDbType.Binary).Value = Convert.FromBase64String(info.Data);
            _command.Parameters.Add("@thumbnail", SqlDbType.Binary).Value = info.DataThumbnail;
            _command.Parameters.Add("@mediaType", SqlDbType.Int).Value = (int)info.MediaType;
            _command.Parameters.Add("@type", SqlDbType.Int).Value = (int)info.Type;
            if (info.ExternalId.HasValue)
              _command.Parameters.Add("@externalId", SqlDbType.BigInt).Value = info.ExternalId;
            else
              _command.Parameters.Add("@externalId", SqlDbType.BigInt).Value = System.DBNull.Value;
            if (string.IsNullOrEmpty(info.Description))
              _command.Parameters.Add("@description", SqlDbType.NVarChar).Value = System.DBNull.Value;
            else
              _command.Parameters.Add("@description", SqlDbType.NVarChar).Value = info.Description;
            id = Convert.ToInt64(_command.ExecuteScalar());
          }

          _transaction.Commit();
        }

        return id;

      }
      catch (Exception Ex)
      {
        GestorLogs.LogException(Ex.Message, Ex, loginId);
        throw new DataServiceException(ResponseCode.DATA_RC_INTERNAL_DATA_ERROR, WSI.Common.Resource.String("STR_WS_DATA_ERROR_SAVE_DATOS", " - Catalogos Operador"));
      }
    }


    public List<long> DatabaseGetRolLayoutSiteByLoginId(long loginId)
    {
      SqlDataAdapter _adapter;
      DataTable _data;

      _data = new DataTable();
      using (DB_TRX _transaction = new DB_TRX())
      {
        using (SqlCommand _command = new SqlCommand(GetLayoutSiteUserRoleByLoginIdQuery(loginId.ToString()), _transaction.SqlTransaction.Connection, _transaction.SqlTransaction))
        {
          _command.Parameters.Add("@loginId", SqlDbType.VarChar).Value = loginId;

          _adapter = new SqlDataAdapter(_command);
          _adapter.Fill(_data);
        }
      }
      List<long> _rolesIds = new List<long>();
      foreach (DataRow row in _data.Rows)
      {
        _rolesIds.Add(long.Parse(row["ROLE"].ToString()));
      }

      return _rolesIds;
    }


    private String GetLayoutSiteAlarmsByTerminalId()
    {
      StringBuilder _query;

      _query = new StringBuilder();
      _query.AppendLine("SELECT");
      _query.AppendLine("lsa.lsa_terminal_id,");
      _query.AppendLine("lsa.lsa_alarm_id,");
      _query.AppendLine("lsa.lsa_date_created,");
      _query.AppendLine("lsa.lsa_id,");
      _query.AppendLine("lsa.lsa_alarm_type,");
      _query.AppendLine("lsa.lsa_alarm_source,");
      _query.AppendLine("lsa.lsa_user_created,");
      _query.AppendLine("lsa.lsa_media_id,");
      _query.AppendLine("lsa.lsa_description,");
      _query.AppendLine("lsa.lsa_status,");
      _query.AppendLine("lsa.lsa_title,");
      _query.AppendLine("t.te_name,");
      _query.AppendLine("t.te_terminal_id,");
      _query.AppendLine("t.te_provider_id,");
      _query.AppendLine("lst.lst_status,");
      _query.AppendLine("lm.lm_data");
      _query.AppendLine("FROM layout_site_alarms lsa");
      _query.AppendLine("INNER JOIN terminals t ON t.te_terminal_id = lsa.lsa_terminal_id");
      _query.AppendLine("LEFT JOIN layout_site_tasks lst ON lsa.lsa_task_id = lst.lst_id");
      _query.AppendLine("LEFT JOIN layout_media lm ON lsa.lsa_media_id = lm.lm_id");
      _query.AppendLine("WHERE lsa_status = 0");
      _query.AppendLine("AND lsa_terminal_id = @terminalId");
      _query.AppendLine("ORDER BY lsa.lsa_date_created DESC");

      return _query.ToString();
    }

    private String GetLayoutSiteAlarmsByUserId()
    {
      StringBuilder _query;

      _query = new StringBuilder();
      _query.AppendLine("SELECT");
      _query.AppendLine("lsa.lsa_terminal_id,");
      _query.AppendLine("lsa.lsa_alarm_id,");
      _query.AppendLine("lsa.lsa_date_created,");
      _query.AppendLine("lsa.lsa_id,");
      _query.AppendLine("lsa.lsa_alarm_type,");
      _query.AppendLine("lsa.lsa_alarm_source,");
      _query.AppendLine("lsa.lsa_user_created,");
      _query.AppendLine("lsa.lsa_media_id,");
      _query.AppendLine("lm.lm_data,");
      _query.AppendLine("lsa.lsa_description,");
      _query.AppendLine("lsa.lsa_status,");
      _query.AppendLine("lsa.lsa_title,");
      _query.AppendLine("t.te_terminal_id,");
      _query.AppendLine("t.te_name,");
      _query.AppendLine("t.te_provider_id,");
      _query.AppendLine("lst.lst_status");
      _query.AppendLine("FROM layout_site_alarms lsa");
      _query.AppendLine("INNER JOIN terminals t ON t.te_terminal_id = lsa.lsa_terminal_id");
      _query.AppendLine("LEFT JOIN layout_site_tasks lst ON lsa.lsa_task_id = lst.lst_id");
      _query.AppendLine("LEFT JOIN layout_media lm ON lsa.lsa_media_id = lm.lm_id");
      _query.AppendLine("WHERE lsa_status = 0");
      _query.AppendLine("AND lsa_user_created = @loginId");
      _query.AppendLine("AND lsa_date_created >= dbo.TodayOpening(" + GeneralParam.GetInt32("Site", "Identifier", 0) + ")");

      if (WSI.Common.TITO.Utils.IsTitoMode())
      {
        _query.AppendLine("AND (SUBSTRING(CAST(LST_SUBCATEGORY AS VARCHAR(5)), 1, 2) NOT IN (13,14,18))");
      }

      return _query.ToString();
    }
    private String GetLayoutSiteTaskById()
    {
      StringBuilder _query;

      _query = new StringBuilder();
      _query.AppendLine("SELECT ");
      _query.AppendLine("lst_id,");
      _query.AppendLine("lst_status, ");
      _query.AppendLine("lst_start,");
      _query.AppendLine("lst_end,");
      _query.AppendLine("lst_category,");
      _query.AppendLine("lst_subcategory,");
      _query.AppendLine("lst_terminal_id,");
      _query.AppendLine("lst_account_id,");
      _query.AppendLine("lst_description,");
      _query.AppendLine("lst_severity,");
      _query.AppendLine("lst_creation_user_id,");
      _query.AppendLine("lst_creation,");
      _query.AppendLine("lst_assigned_role_id,");
      _query.AppendLine("lst_assigned_user_id,");
      _query.AppendLine("lst_assigned,");
      _query.AppendLine("lst_accepted_user_id, ");
      _query.AppendLine("lst_accepted,");
      _query.AppendLine("lst_scale_from_user_id,");
      _query.AppendLine("lst_scale_to_user_id,");
      _query.AppendLine("lst_scale_reason,");
      _query.AppendLine("lst_scale,");
      _query.AppendLine("lst_solved_user_id,");
      _query.AppendLine("lst_solved,");
      _query.AppendLine("lst_validate_user_id,");
      _query.AppendLine("lst_validate,");
      _query.AppendLine("lst_last_status_update_user_id,");
      _query.AppendLine("lst_last_status_update,");
      _query.AppendLine("lst_attached_media,");
      _query.AppendLine("lst_events_history,");
      _query.AppendLine("lst_title,");
      _query.AppendLine("'' as 'te_name',");
      _query.AppendLine("'' as 'te_provider_id',");
      _query.AppendLine("'' as 'bk_name',");
      _query.AppendLine("'' as 'ar_name',");
      _query.AppendLine("'' as 'te_model',");
      _query.AppendLine("'' as 'PLAY_SESSION_STATUS',");
      _query.AppendLine("'' as 'IS_ANONYMOUS',");
      _query.AppendLine("'' as 'PLAYER_ACCOUNT_ID',");
      _query.AppendLine("'' as 'PLAYER_NAME',");
      _query.AppendLine("'' as 'PLAYER_GENDER',");
      _query.AppendLine("'' as 'PLAYER_AGE',");
      _query.AppendLine("'' as 'PLAYER_LEVEL',");
      _query.AppendLine("'' as 'PLAYER_IS_VIP'");

      _query.AppendLine("FROM layout_site_tasks");
      _query.AppendLine("WHERE lst_id = @taskId");

      return _query.ToString();
    }
    private string GetRoleIdsAsParams(IList<long> roleIds)
    {
      StringBuilder builder = new StringBuilder();
      for (int i = 0; i < roleIds.Count; i++)
      {
        builder.Append(roleIds[i].ToString());
        if (i != roleIds.Count - 1)
          builder.Append(",");
      }

      return builder.ToString();
    }//GetAssigedTaskQuery
    private String GetLayoutSiteUserRoleByLoginIdQuery(string loginId)
    {
      StringBuilder _query;

      _query = new StringBuilder();
      _query.AppendLine("SELECT   DISTINCT(GPF_FORM_ID / 1000000) AS ROLE");
      _query.AppendLine("FROM   GUI_PROFILE_FORMS");
      _query.AppendLine("WHERE   GPF_GUI_ID = 203 AND GPF_READ_PERM <> 0");
      _query.AppendLine("AND   GPF_PROFILE_ID = (");
      _query.AppendLine("               SELECT   GU_PROFILE_ID");
      _query.AppendLine("                 FROM   GUI_USERS");
      _query.AppendLine(String.Format("                WHERE   GU_USER_ID    = {0}", loginId));
      _query.AppendLine("              )");
      _query.AppendLine("AND   (GPF_FORM_ID / 1000000) > 0");

      return _query.ToString();
    }

    public bool IsValidTerminalId(long? terminalId)
    {

      if (!terminalId.HasValue)
        return true;

      Int32 countResult;
      DataTable _data;

      _data = new DataTable();
      using (DB_TRX _transaction = new DB_TRX())
      {
        using (SqlCommand _command = new SqlCommand("Layout_IsValidTerminal", _transaction.SqlTransaction.Connection, _transaction.SqlTransaction))
        {
          SqlParameter outputIdParam = new SqlParameter("@count_terminal", SqlDbType.Int)
          {
            Direction = ParameterDirection.Output
          };
          _command.Parameters.Add("@terminalId", SqlDbType.VarChar).Value = terminalId;
          _command.CommandType = CommandType.StoredProcedure;
          _command.Parameters.Add(outputIdParam);
          _command.ExecuteNonQuery();
          countResult = int.Parse(outputIdParam.Value.ToString());
        }
      }
      return countResult > 0;
    }

    public bool IsTaskAssignedToUser(long taskId, long loginId)
    {
      SqlDataAdapter _adapter;
      DataTable _data;

      _data = new DataTable();
      using (DB_TRX _transaction = new DB_TRX())
      {
        using (SqlCommand _command = new SqlCommand("Layout_GetIsTaskAssignedToUser", _transaction.SqlTransaction.Connection, _transaction.SqlTransaction))
        {
          _command.CommandType = CommandType.StoredProcedure;
          _command.Parameters.Add("@taskId", SqlDbType.BigInt).Value = taskId;
          _command.Parameters.Add("@loginId", SqlDbType.BigInt).Value = loginId;

          _adapter = new SqlDataAdapter(_command);
          _adapter.Fill(_data);
        }
      }
      return _data.Rows.Count > 0;
    }

    public DataTable DataBaseGetBeaconsAvailables()
    {
      SqlDataAdapter _adapter;
      DataTable _data;

      _data = new DataTable();
      using (DB_TRX _transaction = new DB_TRX())
      {
        using (SqlCommand _command = new SqlCommand("Layout_GetBeaconsAvailables", _transaction.SqlTransaction.Connection, _transaction.SqlTransaction))
        {
          _command.CommandType = CommandType.StoredProcedure;
          _command.Parameters.Add("@pBeaconEnumId", SqlDbType.Int).Value = 70;
          _adapter = new SqlDataAdapter(_command);
          _adapter.Fill(_data);
        }
      }
      return _data;
    }

    public Int32 DataBaseSendMessageTo(LayoutSiteManagerRunnerMessage message)
    {
      SqlDataAdapter _adapter;
      DataTable _data;
      Int32 idMessage = 0;

      _data = new DataTable();
      using (DB_TRX _transaction = new DB_TRX())
      {
        using (SqlCommand _command = new SqlCommand("Layout_SaveManagerRunnerMessage", _transaction.SqlTransaction.Connection, _transaction.SqlTransaction))
        {
          SqlParameter outputIdParam = new SqlParameter("@new_identity", SqlDbType.Int)
          {
            Direction = ParameterDirection.Output
          };

          _command.CommandType = CommandType.StoredProcedure;
          _command.Parameters.Add("@pManagerId", SqlDbType.BigInt).Value = message.ManagerId;
          _command.Parameters.Add("@pRunnerId", SqlDbType.BigInt).Value = message.RunnerId;
          _command.Parameters.Add("@pMessage", SqlDbType.Text).Value = message.Message;
          _command.Parameters.Add("@pSource", SqlDbType.Int).Value = message.Source;
          _command.Parameters.Add(outputIdParam);
          _command.ExecuteNonQuery();
          idMessage = int.Parse(outputIdParam.Value.ToString());
        }

        _transaction.Commit();
      }

      return idMessage;
    }
    public DataTable DataBaseGetConversationHistory(long runnerId, long managerId)
    {
      SqlDataAdapter _adapter;
      DataTable _data;

      _data = new DataTable();
      using (DB_TRX _transaction = new DB_TRX())
      {
        using (SqlCommand _command = new SqlCommand("Layout_GetConversationHistory", _transaction.SqlTransaction.Connection, _transaction.SqlTransaction))
        {
          _command.CommandType = CommandType.StoredProcedure;
          _command.Parameters.Add("@pManagerId", SqlDbType.BigInt).Value = managerId;
          _command.Parameters.Add("@pRunnerId", SqlDbType.BigInt).Value = runnerId;
          _adapter = new SqlDataAdapter(_command);
          _adapter.Fill(_data);
        }
      }
      return _data;
    }


    public String DataBaseGetApplicationLastestVersion()
    {
      SqlDataAdapter _adapter;
      DataTable _data;

      _data = new DataTable();

      using (DB_TRX _transaction = new DB_TRX())
      {
        using (SqlCommand _command = new SqlCommand("Layout_GetLastestMobileApplicationVersion", _transaction.SqlTransaction.Connection, _transaction.SqlTransaction))
        {
          _command.CommandType = CommandType.StoredProcedure;
          _adapter = new SqlDataAdapter(_command);
          _adapter.Fill(_data);
        }

        _transaction.Commit();
      }

      if (_data.Rows.Count > 0)
        return _data.Rows[0]["lmav_version"].ToString();
      else
        return string.Empty;
    }

    public String DataBaseGetApplicationLastestVersionApk()
    {
      SqlDataAdapter _adapter;
      DataTable _data;

      _data = new DataTable();

      using (DB_TRX _transaction = new DB_TRX())
      {
        using (SqlCommand _command = new SqlCommand("Layout_GetLastestMobileApplicationVersionApk", _transaction.SqlTransaction.Connection, _transaction.SqlTransaction))
        {
          _command.CommandType = CommandType.StoredProcedure;
          _adapter = new SqlDataAdapter(_command);
          _adapter.Fill(_data);
        }

        _transaction.Commit();
      }

      if (_data.Rows.Count > 0)
      {
        var file = (byte[])_data.Rows[0]["lmav_apk"];
        return Convert.ToBase64String(file, Base64FormattingOptions.None);
      }
      else
        return string.Empty;
    }

    public DataTable DataBaseGetConversationsOfToday(long runnerId)
    {
      SqlDataAdapter _adapter;
      DataTable _data;

      _data = new DataTable();
      using (DB_TRX _transaction = new DB_TRX())
      {
        using (SqlCommand _command = new SqlCommand("Layout_GetConversationsOfRunner", _transaction.SqlTransaction.Connection, _transaction.SqlTransaction))
        {
          _command.CommandType = CommandType.StoredProcedure;
          _command.Parameters.Add("@pRunnerId", SqlDbType.BigInt).Value = runnerId;
          _adapter = new SqlDataAdapter(_command);
          _adapter.Fill(_data);
        }
      }
      return _data;
    }

    public DataTable DataBaseGetManagersAvailable()
    {
      SqlDataAdapter _adapter;
      DataTable _data;

      _data = new DataTable();
      using (DB_TRX _transaction = new DB_TRX())
      {
        using (SqlCommand _command = new SqlCommand("Layout_GetUsersWithManagerCapabilities", _transaction.SqlTransaction.Connection, _transaction.SqlTransaction))
        {
          _command.CommandType = CommandType.StoredProcedure;
          _adapter = new SqlDataAdapter(_command);
          _adapter.Fill(_data);
        }
      }
      return _data;
    }
  }
}