﻿////------------------------------------------------------------------------------
//// Copyright © 2012 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: DbUnitariosConnection.cs
//// 
////      DESCRIPTION: DbUnitariosConnection partial class for non transactional methods
//// 
////           AUTHOR: Humbeto Braojos
//// 
////    CREATION DATE: 15-OCT-2012
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 15-OCT-2012 HBB, JCM    First release.
//// 22-OCT-2012 HBB
//// 31-OCT-2012 XIT-JCA     Refactored
//// 21-OCT-2012 HBB         Added md_tipo_monitorizacion in the qury of GetPendingMonitorizationsQuery() method
//// 22-NOV-2012 JCM         Can send reports with diffent date than reported F1
//// 04-DEC-2012 XIT         Moved from Transaction file to call file
//// 11-JAN-2013 XIT         Added lg_enable_log field as return parameter
////------------------------------------------------------------------------------
using System;
using System.Text;
using DATA.OBJ.DTO;
using System.Data;
using System.Data.SqlClient;
using WSI.Common;
using DATA.OBJ;
using System.Collections.Generic;

namespace DATA.DAL
{
  public partial class DbUnitariosConnection
  {
    public enum EN_LOGIN_EXCEPTIONS
    {
      NONE = 0,
      NOT_EXIST = 1,
      BLOCKED = 2,
      NO_PERMISSION = 3,
      WRONG_PASSWORD = 4
    }

    #region Public Methods
    public int DatabaseInicioSesion(InicioSesion User)
    {
      PasswordPolicy _passpolicy = new PasswordPolicy();
      Object _obj;
      EN_LOGIN_EXCEPTIONS _login_result = EN_LOGIN_EXCEPTIONS.NONE;
      int _login_Id = -1;
      int _nls_id = 5000 + 265;
      String _str_error = "";

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _command = new SqlCommand("Layout_Login", _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _command.CommandType = CommandType.StoredProcedure;
            _command.CommandTimeout = 2 * 60;

            _command.Parameters.Add("@pUserName", SqlDbType.NVarChar, 40).Value = User.Login;
            _command.Parameters.Add("@pOrigin", SqlDbType.NVarChar, 40).Value = 2; //WEB=1, APP=2

            SqlParameter _param = _command.Parameters.Add("@opUserId", SqlDbType.Int);
            _param.Direction = ParameterDirection.Output;

            _obj = _command.ExecuteScalar();

            if (_obj != null)
            {
              _login_result = (EN_LOGIN_EXCEPTIONS)_obj;
              if (_login_result != EN_LOGIN_EXCEPTIONS.NOT_EXIST)
              {
                _login_Id = (int)_param.Value;
              }
              else
              {
                _login_Id = 0;
                User.Login = "SU";
              }
            }
          }

          if (_login_result == EN_LOGIN_EXCEPTIONS.NONE)
          {
            if (!_passpolicy.VerifyCurrentPassword(_login_Id, User.Password, _db_trx.SqlTransaction))
            {
              _login_result = EN_LOGIN_EXCEPTIONS.WRONG_PASSWORD;
            }
          }

          switch (_login_result)
          {
            case EN_LOGIN_EXCEPTIONS.BLOCKED:
              _nls_id = 5000 + 107;
              _str_error = "STR_FRM_USER_LOGIN_ERROR_LOGIN_DISABLED";
              break;

            case EN_LOGIN_EXCEPTIONS.NO_PERMISSION:
              _nls_id = 5000 + 107;
              _str_error = "STR_FRM_USER_LOGIN_ERROR_LOGIN_PERMS";
              break;

            case EN_LOGIN_EXCEPTIONS.NOT_EXIST:
              _nls_id = 5000 + 457;
              _str_error = "STR_FRM_USER_LOGIN_ERROR_LOGIN_INVALID";
              break;

            case EN_LOGIN_EXCEPTIONS.WRONG_PASSWORD:
              _nls_id = 5000 + 107;
              _str_error = "STR_FRM_USER_LOGIN_ERROR_LOGIN_INVALID";
              break;

            case EN_LOGIN_EXCEPTIONS.NONE:
              break;

            default:
              break;
          }

        }//using (DB_TRX _db_trx = new DB_TRX())

        // insert audit
        WSI.Common.Auditor.Audit(ENUM_GUI.SMARTFLOOR_APP,            // GuiId
                                  _login_Id,                  // AccountId
                                  User.Login,                // UserName
                                  User.DeviceId,             // MachineName
                                  4,                         // AuditCode
                                  _nls_id,                   // NlsId (GUI)
                                  WSI.Common.Resource.String(_str_error), // NlsParam01
                                  "",                        // NlsParam02
                                  "",                        // NlsParam03
                                  "",                        // NlsParam04
                                  "");                       // NlsParam05

        switch (_login_result)
        {
          case EN_LOGIN_EXCEPTIONS.BLOCKED:
          case EN_LOGIN_EXCEPTIONS.NO_PERMISSION:
          case EN_LOGIN_EXCEPTIONS.NOT_EXIST:
          case EN_LOGIN_EXCEPTIONS.WRONG_PASSWORD:
              throw new DataServiceException(ResponseCode.DATA_RC_INTERNAL_DATA_ERROR, WSI.Common.Resource.String(_str_error));

          case EN_LOGIN_EXCEPTIONS.NONE:
          default:
            break;
        }

        return _login_Id;
      }
      catch (Exception _ex)
      {
        
       DataServiceException _dex = (DataServiceException)_ex;

       if (!String.IsNullOrEmpty(_dex.ResponseDescription)) {
            throw new DataServiceException(_dex.ResponseCode,_dex.ResponseDescription);
       } else { 
           GestorLogs.LogException(_ex.Message, _ex, 0);
           throw new DataServiceException(ResponseCode.DATA_RC_INTERNAL_DATA_ERROR, WSI.Common.Resource.String("STR_FRM_USER_LOGIN_ERROR_LOGIN_INVALID"));
       }
      }
    }//DatabaseInicioSesion


    #endregion

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE:It generates a query to check if the user is valid and 
    //         if  the user is valid, returns a dataset with two tables
    //
    //  PARAMS:
    //      - INPUT:connection
    //          - 
    //
    //      - OUTPUT:
    //
    // RETURNS: Returns a String with the query
    private String GetInicioSesionQuery()
    {
      StringBuilder _query;

      _query = new StringBuilder();
      _query.Append(GetLoginQuery());

      return _query.ToString();
    }//GetInicioSesionQuery

    //------------------------------------------------------------------------------
    // PURPOSE: it generates a query to check if the user is valid
    //
    //  PARAMS:
    //      - INPUT:
    //          - 
    //
    //      - OUTPUT:
    //
    // RETURNS: Returns a StringBuilder with the query
    private String GetLoginQuery()
    {
      StringBuilder _query;

      _query = new StringBuilder();
      _query.AppendLine(" SELECT    GU_USER_ID                                                                ");
      _query.AppendLine("   FROM    GUI_USERS                                                                          ");
      _query.AppendLine("  WHERE    GU_USERNAME = @pLogin                                                              ");
      //_query.AppendLine("     AND    GU_PASSWORD = CAST (@pPwd AS BINARY(40))                                           ");


      return _query.ToString();
    }//GetLoginQuery


    private String GetCatalogsQuery()
    {
      StringBuilder _query;

      _query = new StringBuilder();
      _query.AppendLine("   SELECT");
      _query.AppendLine("        AT_ID ");
      _query.AppendLine("       ,AT_CREATION_USER_ID ");
      _query.AppendLine("       ,AT_CREATION ");
      _query.AppendLine("       ,AL_CATEGORY ");
      _query.AppendLine("       ,AL_SUBCATEGORY ");
      _query.AppendLine("       ,AT_TERMINAL_ID ");
      _query.AppendLine("       ,AT_ACCOUNT_ID ");
      _query.AppendLine("       ,AT_DESCRIPTION ");
      _query.AppendLine("       ,AT_ASSIGNED_USER_ID ");
      _query.AppendLine("       ,AT_SEVERITY ");
      _query.AppendLine("       ,AT_STATUS ");
      _query.AppendLine("       ,AT_START ");
      _query.AppendLine("       ,AT_END ");
      _query.AppendLine("       ,AT_VALIDATE ");
      _query.AppendLine("       ,AT_VALIDATE_USER_ID ");
      _query.AppendLine("       ,AT_ESCALATE_USER_ID ");
      _query.AppendLine("       ,AT_ESCALATE_REASON ");
      _query.AppendLine("       ,AL_LAST_STATUS_UPDATE ");
      _query.AppendLine("       ,AL_LAST_STATUS_UPDATE_USER_ID ");
      _query.AppendLine("     FROM   ALARM_TICKETS             ");

      //_query.AppendLine("    WHERE   OP_NOMBRE = @pOperatorName                                                                       ");


      return _query.ToString();
    }//GetCatalogsQuery
    #endregion
  }
}
