﻿////------------------------------------------------------------------------------
//// Copyright © 2012 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: DatabaseConnection.cs
//// 
////      DESCRIPTION: Partial class DatabaseConnection communicate with DB in Report Trx
//// 
////           AUTHOR: Joaquim Cid
//// 
////    CREATION DATE: 10-OCT-2012
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 15-OCT-2012 JCM    First release.
//// 31-OCT-2012 XIT    Refactored
//// 05-NOV-2012 JCA    Add Try Catch to Sentences
//// 22-NOV-2012 JCA    Add Functions to save in BBDD each step
//// 04-DEC-2012 XIT    Moved from Transaction file to call file
////------------------------------------------------------------------------------
using System;
using System.Linq;
using System.Text;
using DATA.OBJ;
using DATA.OBJ.DTO;
using System.Data.SqlClient;
using System.Data;
using WSI.Common;
using DATA.OBJ.Transactions;
using System.Collections.Generic;
using System.Diagnostics;

namespace DATA.DAL
{
  public partial class DbReportesConnection
  {

    #region ToDebugCommand
    public static String DebugCommand(DataTable DtReg, SqlCommand Cmd)
    {
      if (!Debugger.IsAttached)
      {
        return "";
      }
      String _query;
      StringBuilder _sb_declare;
      StringBuilder _sb_set;

      _sb_declare = new StringBuilder();
      _sb_set = new StringBuilder();

      foreach (SqlParameter _sqlparameter in Cmd.Parameters)
      {
        _query = "DECLARE " + _sqlparameter.ParameterName + " AS " + _sqlparameter.SqlDbType.ToString().ToUpper();
        if (_sqlparameter.DbType == DbType.AnsiString || _sqlparameter.DbType == DbType.AnsiStringFixedLength || _sqlparameter.DbType == DbType.String ||
        _sqlparameter.DbType == DbType.StringFixedLength)
        {
          _query += " (" + _sqlparameter.Size + ")";
        }

        _sb_declare.AppendLine(_query);
        if (_sqlparameter.SqlValue != null && !String.IsNullOrEmpty(_sqlparameter.SqlValue.ToString()))
        {
          _sb_set.AppendLine("SET " + _sqlparameter.ParameterName + " =" + SqlValue(_sqlparameter));
        }
        else if (!String.IsNullOrEmpty(_sqlparameter.SourceColumn))
        {
          _sb_set.AppendLine("SET " + _sqlparameter.ParameterName + " =" + SqlValue(DtReg, _sqlparameter));
        }
        else
        {
          if (_sqlparameter.Value != null)
          {
            _sb_set.AppendLine("SET " + _sqlparameter.ParameterName + " =" + _sqlparameter.Value + "--(" + _sqlparameter.SqlDbType + ")--");
          }
          else
          {
            _sb_set.AppendLine("SET " + _sqlparameter.ParameterName + " =");
          }
        }
      }
      _query = _sb_declare.ToString() + _sb_set.ToString() + Cmd.CommandText;
      return _query;
    }

    private static object SqlValue(DataTable DtReg, SqlParameter SqlPar)
    {
      if (DtReg == null)
      {
        return null;
      }
      object _retorno;

      try
      {
        if (SqlPar.DbType == DbType.AnsiString || SqlPar.DbType == DbType.AnsiStringFixedLength || SqlPar.DbType == DbType.String ||
           SqlPar.DbType == DbType.StringFixedLength)
        {
          //return "'" + DtReg.Rows[0][SqlPar.SourceColumn] + "'";
          _retorno = "'" + DtReg.Rows[0][SqlPar.SourceColumn] + "'";
        }
        else if (SqlPar.DbType == DbType.Date || SqlPar.DbType == DbType.DateTime || SqlPar.DbType == DbType.DateTime2)
        {
          //return "'" + Convert.ToDateTime(DtReg.Rows[0][SqlPar.SourceColumn]).ToString("yyyy-MM-dd HH:mm:ss") + "'";
          _retorno = "'" + Convert.ToDateTime(DtReg.Rows[0][SqlPar.SourceColumn]).ToString("yyyy-MM-dd HH:mm:ss") + "'";
        }
        else
        {
          _retorno = DtReg.Rows[0][SqlPar.SourceColumn];
        }
      }
      catch 
      {
        _retorno = DtReg.Rows[0][SqlPar.SourceColumn];
      }
      return _retorno;
    }

    private static String SqlValue(SqlParameter _sqlparameter)
    {
      String _retorno;
      _retorno= "";
      try
      {
        if (_sqlparameter.DbType == DbType.AnsiString || _sqlparameter.DbType == DbType.AnsiStringFixedLength || _sqlparameter.DbType == DbType.String ||
            _sqlparameter.DbType == DbType.StringFixedLength)
        {
          //return "'" + _sqlparameter.SqlValue.ToString() + "'";
          _retorno = "'" + _sqlparameter.SqlValue.ToString() + "'";
        }
        else if (_sqlparameter.DbType == DbType.Date || _sqlparameter.DbType == DbType.DateTime || _sqlparameter.DbType == DbType.DateTime2)
        {
          //return "'" + _sqlparameter.SqlValue.ToString() + "'";
          _retorno = "'" + _sqlparameter.SqlValue.ToString() + "'";
        }
        else
        {
          _retorno = _sqlparameter.SqlValue.ToString();
        }
      }
      catch
      {

        _retorno = "---CONVERSION MANUAL: [ " + _sqlparameter.SqlValue.ToString() + "]";
      }

      return _retorno;
    }

    #endregion


    private void DefineSchemaReportesDiarios(DataTable Data)
    {
      Data.Columns.Add(ReportesDiariosFields.RD_REPORTE_DIARIO_ID.ToString(), typeof(Int64));
      Data.Columns.Add(ReportesDiariosFields.RD_OPERADOR_ID.ToString(), typeof(Int64));
      Data.Columns.Add(ReportesDiariosFields.RD_ESTABLECIMIENTO_ID.ToString(), typeof(Int64));
      Data.Columns.Add(ReportesDiariosFields.RD_LINEA_ID.ToString(), typeof(Int32));
      Data.Columns.Add(ReportesDiariosFields.RD_FECHA_REPORTE.ToString(), typeof(DateTime));
      Data.Columns.Add(ReportesDiariosFields.RD_VERSION_WS_DATA.ToString(), typeof(String));
      Data.Columns.Add(ReportesDiariosFields.RD_NO_REGISTROS_REPORTE.ToString(), typeof(Int64));
      Data.Columns.Add(ReportesDiariosFields.RD_BALANCE_INICIAL.ToString(), typeof(Decimal));
      Data.Columns.Add(ReportesDiariosFields.RD_BALANCE_FINAL.ToString(), typeof(Decimal));
      Data.Columns.Add(ReportesDiariosFields.RD_INGRESOS_CAJA.ToString(), typeof(Decimal));
      Data.Columns.Add(ReportesDiariosFields.RD_SALIDAS_CAJA.ToString(), typeof(Decimal));
      Data.Columns.Add(ReportesDiariosFields.RD_UPDATED.ToString(), typeof(DateTime));
      Data.Columns.Add(ReportesDiariosFields.RD_UPDATED_TIMES.ToString(), typeof(Int32));
      Data.Columns.Add(ReportesDiariosFields.RD_STATUS.ToString(), typeof(Int32));
      Data.Columns.Add(ReportesDiariosFields.RD_STATUS_BEFORE_GENERATE.ToString(), typeof(Int32));
      Data.Columns.Add(ReportesDiariosFields.RD_LAST_CLIENT_COM.ToString(), typeof(DateTime));
    }
    private void DefineSchemaReportesDiariosSubLine(DataTable Data)
    {
      Data.Columns.Add(ReportesDiariosRegistrosFields.RG_SUBLINEA_NEGOCIO_ID.ToString(), typeof(Int32));
    }
  }
}