
//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: StandardService.cs
// 
//   DESCRIPTION: Procedures for WWP Client to run as a service
// 
//        AUTHOR: Andreu Juli�
// 
// CREATION DATE: 17-MAR-2011
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 17-MAR-2011 AJQ    First release.
//------------------------------------------------------------------------------
using System;
using System.ServiceProcess;
using System.ComponentModel;
using System.Reflection;
using System.IO;
using WSI.Common;
using System.Threading;
using System.Diagnostics;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;


namespace WSI.Common
{
  public partial class CommonService: ServiceBase
  {
    //
    // Functions Prototypes
    //
    [DllImport("CommonBase")]  //TARGET_64_BITS JCALERO
    static extern bool Common_NewReleaseCandidateInstalled(String Path); //TARGET_64_BITS JCALERO
    
    protected virtual void AutoUpdate ()
    {
      if ( m_console )
      {
        if ( System.IO.Directory.Exists("Release_Candidate") )
        {
          System.IO.Directory.Delete("Release_Candidate", true);
        }
      }

      //
      // Check for software update
      //

      //TARGET_64_BITS JCALERO
     
      try
      {

        if (Common_NewReleaseCandidateInstalled(m_working_folder))
        {
          Alarm.Register(AlarmSourceCode.Service, 0, m_alarm_source, AlarmCode.Service_NewVersionAvailable);
          Alarm.Register(AlarmSourceCode.Service, 0, m_alarm_source, AlarmCode.Service_Stopped);

          Environment.Exit(0);

          return;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      //
      // Automatically download new software versions
      //
      try
      {
        String[] _ver_part;

        _ver_part = m_version.Split(new char[1] { '.' });
        if (_ver_part.Length == 2)
        {
          int _client_id;
          int _build_id;

          int.TryParse(_ver_part[0], out _client_id);
          int.TryParse(_ver_part[1], out _build_id);
          FtpVersions.Init(m_tsv_packet, _client_id, _build_id);
          FtpVersions.OnNewVersionAvailable += new FtpVersions.NewVersionAvailableDelegate(this.NewVersionHandler);
        }
        else
        {
          Log.Error("Wrong number of version components (" + m_version + ")");
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }
    //------------------------------------------------------------------------------
    // PURPOSE: Actions on new version is available.
    //
    //  PARAMS:
    //      - INPUT:
    //          - Version
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    protected virtual void NewVersionHandler(FtpVersions.NewVersionEvent VersionEvent)
    {
      String _version_header;
      String _version_footer;
      String _alarm_desc;

      _version_header = "*\r\n* Version " + VersionEvent.version + ", ";
      _version_footer = "\r\n*";

      Log.Message(_version_header + "downloaded ---------------------------------------------------" + _version_footer);

      _alarm_desc = Alarm.ResourceId(AlarmCode.Service_NewVersionDownloaded);
      _alarm_desc = Resource.String(_alarm_desc, VersionEvent.version);
      Alarm.Register(AlarmSourceCode.Service, 0, m_alarm_source, AlarmCode.Service_NewVersionDownloaded, _alarm_desc);
 
      
      if (m_console)
      {
        MessageBox.Show(_version_header + "downloaded" + _version_footer, m_service_name, MessageBoxButtons.OK);
        Log.Message(_version_header + "update canceled" + _version_footer);

        VersionEvent.notification_period = 0;

        return;
      }

      try
      {
        Log.Message(_version_header + "updating ..." + _version_footer);

        // Release Candidate -> Production

        //TARGET_64_BITS JCALERO
       
        if (!Common_NewReleaseCandidateInstalled(m_working_folder))
        {
          Log.Message(_version_header + "update failed" + _version_footer);
        }
        else
        {
          Log.Message(_version_header + "updated" + _version_footer);
        }
     
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }


      // TODO:  IF WC2 -> STANDBY --> STOP  -> RUNNING --> PARTNER?
      if (m_stand_by)
      {
      }
      else
      {
      }

      RequestOrderlyShutdown("UPDATE");

    }

  }
}