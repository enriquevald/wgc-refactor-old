﻿using DATA.BL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSI.Common;
using uPLibrary.Networking.M2Mqtt;
namespace WIN.MqttBroker
{
    public class MqttBrokerService
    {
        public static uPLibrary.Networking.M2Mqtt.MqttBroker broker;
        static void Main(string[] args)
        {
            broker = new uPLibrary.Networking.M2Mqtt.MqttBroker();
            broker.Start();

            GestorLogs.LogInfo(AlarmSourceCode.WebService, 1, "MQTT Broker", AlarmCode.Service_Started, "Broker Started", 1); 
        }
    }
}
