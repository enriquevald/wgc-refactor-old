//------------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: CommonMiscDbConstants.h
//   DESCRIPTION: Constants, types, variables definitions for DB functions.
//        AUTHOR: Alberto Cuesta
// CREATION DATE: 08-MAR-2002
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ -----------------------------------------------------------
// 08-MAR-2002 ACC    Initial draft.
// 
//------------------------------------------------------------------------------

#ifndef _COMMON_MISC_DB_CONSTANTS_H
#define _COMMON_MISC_DB_CONSTANTS_H

//------------------------------------------------------------------------------------
//  INCLUDES 
//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------
//  PUBLIC CONSTANTS 
//------------------------------------------------------------------------------------

#define COMMON_MAX_LEN_USERNAME           80
#define COMMON_MAX_LEN_PASSWORD           80
#define COMMON_MAX_LEN_CONNECTSTR         80
#define COMMON_MAX_LEN_DATE               20
#define MAX_ORACLE_MSG_LEN                512
// Queues
#define MAX_QUEUE_MSG_LEN                 40
#define MAX_QUEUE_OWNER_LEN               30
#define MAX_QUEUE_NAME_LEN                30
#define MAX_SUB_QUEUE_NAME_LEN            30
#define MAX_CONSUMER_NAME_LEN             30

// SQLCODES
#define SQLCODE_OK              0       // Ok
#define SQLCODE_NOT_FOUND       1403    // Not Found
#define SQLCODE_DUPLICATE_VALUE -1      // Duplicate Value
#define SQLCODE_NOT_LOGGED      -1012   // Not Logged On
#define SQLCODE_END_OF_FILE     -3113   // end-of-file on communication channel.
#define SQLCODE_NOT_CONNECTED   -3114   // Not Connected to Oracle
#define SQLCODE_TNS_12154       -12154  // TNS:could not resolve service name.
#define SQLCODE_TNS_12560       -12560  // TNS:protocol adapter error.
#define SQLCODE_CONNECT_FAILED  -12545  // Connect failed because target host or object does not exist.


#define SQLCODE_QUEUE_NO_MSG    -25228 // There are no messages in the message queue

// To create special conn scheduling for online stock requests
#define COMMON_OTM_SYSTEM_CONN_ID         1

//------------------------------------------------------------------------------------
//  PUBLIC DATATYPES 
//------------------------------------------------------------------------------------


#endif // _COMMON_MISC_DB_CONSTANTS_H
