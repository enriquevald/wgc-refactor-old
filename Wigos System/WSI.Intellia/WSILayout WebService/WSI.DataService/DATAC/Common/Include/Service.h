//------------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Service.h
//   DESCRIPTION: Constants, types, variables definitions and prototypes 
//                for generic Services
//
//        AUTHOR: Andreu Juli�
//
// CREATION DATE: 12-MAR-2002
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ -----------------------------------------------------------
// 12-MAR-2002 AJQ    Initial draft.
// 17-MAY-2002 AJQ    ????
// 22-JUL-2002 AJQ    Watchdog added.
// 10-FEB-2004 RRT    Audit functions added.
// 24-FEB-2004 RRT    General Service Session Management functions added.
//------------------------------------------------------------------------------

#ifndef __SERVICE_H
#define __SERVICE_H
//------------------------------------------------------------------------------
//  INCLUDES 
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//  PUBLIC CONSTANTS 
//------------------------------------------------------------------------------
#define SERVICE_NAME_LEN           100

// Max CommMgr, Max BatchTrxMgr, Max xxxTrxMgr
#define MAX_IPC_SERVICES      100

//------------------------------------------------------------------------------
//  PUBLIC DATATYPES 
//------------------------------------------------------------------------------
typedef enum
{
  LOG_TO_PRIVATE = 1,
  LOG_TO_LOGGER  = 2,
  LOG_TO_BOTH    = LOG_TO_PRIVATE | LOG_TO_LOGGER,
  LOG_TO_DEFAULT = LOG_TO_LOGGER

} ENUM_LOG;

typedef struct
{
  BOOL error;  // Error
  BOOL debug;  // Debug
  BOOL stop;   // Stop

} TYPE_SERVICE_FLAGS;

// AJQ, 12-AUG-2002 Added: ipc_node_index, ipc_node_name, wdog_node_name
typedef struct
{
  CONTROL_BLOCK       control_block;          // Control Block
  TYPE_SERVICE_FLAGS  flags;                  // Flags
  HANDLE              stop_event;             // Stop event
  // IPC
  //  - Node
  DWORD               ipc_node_instance_id;                 // Node instance id
  DWORD               ipc_node_index;                       // Node index
  TCHAR               ipc_node_name [IPC_NODE_NAME_LENGTH]; // Node name
  //  - Watchdog
  DWORD               wdog_id;                                // Watchdog node instance id
  TCHAR               wdog_node_name [IPC_NODE_NAME_LENGTH];  // Watchdog name
  
  // Database
  SQL_CONTEXT         sql_context;            // SQL Context

  WORD                client_id;

  void                * p_user_data;          // Pointer to user data

} TYPE_SERVICE_DATA;


//// RRT, 10-FEB-2004, Added structure Service Connection Instance
//typedef struct
//{
//  DWORD   connection_id;    // Connection Id.
//  DWORD   instance_id;      // Instance Id.
//  DWORD   h_inst_id;        // Hierarchy Instance Id.
//  WORD    num_retries;      // Number of Retries
//
//} TYPE_SRV_CONN_INST;
//
//// RRT, 10-FEB-2004, Added structure Service Audit
//typedef struct
//{
//  CONTROL_BLOCK       control_block;            // Control Block
//  DWORD               agency_server_id;         // Internal Agency Id.
//  DWORD               ext_agency_server_id;     // External Agency Id.
//  DWORD               trx_type;                 // Transac. Mgr. Type (Batch, Online, etc.)
//  BYTE                trx_level;                // Audit Level (i.e: TRX_AUDIT_LEVEL_NO_EVENTS)
//  BYTE                trx_audit_priority_type;  // Audit Priority (i.e.: TRX_AUDIT_PRIORITY_ERROR, etc. )
//  DWORD               trx_alarm_priority_type;  // Alarm Priority
//  WORD                trx_command;              // Transac. Mgr. Command
//  DWORD               nls_id;                   // Absolute NLS Id.
//  TCHAR_NLS_PARAM     nls_param0;               // NLS Parameter 0
//  TCHAR_NLS_PARAM     nls_param1;               // NLS Parameter 1
//  TCHAR_NLS_PARAM     nls_param2;               // NLS Parameter 2
//  TCHAR_NLS_PARAM     nls_param3;               // NLS Parameter 3
//  TCHAR_NLS_PARAM     nls_param4;               // NLS Parameter 4
//  TYPE_SRV_CONN_INST  conn_inst;                // Connection Parameters
//
//} TYPE_TRX_MGR_AUDIT;


//typedef struct
//{
//  TCHAR             node_name [IPC_NODE_NAME_LENGTH];
//  DWORD             node_instance_id;
//
//  HANDLE            message_received;
//  API_OUTPUT_PARAMS api_output_params;
//  IPC_IUD_RECEIVE   ipc_iud_receive;
//  
//} TYPE_SERVICE_IPC_DATA;

//typedef struct
//{
//  HANDLE            handle_message_received; // Set when the message is received
//  API_OUTPUT_PARAMS api_output_params;       // API
//  IPC_IUD_RECEIVE   ipc_iud_receive;         // 
//
//} TYPE_SERVICE_MESSAGE;

typedef struct
{
  // Interface Routines
  BOOL (* p_data_recv)      (DWORD   IntAgencyId); // Data Received Function
  BOOL (* p_session_opened) (DWORD   IntAgencyId); // Open Session Function  
  BOOL (* p_session_closed) (DWORD   IntAgencyId); // Close Session Function

} TYPE_TRX_MGR_FUNCTIONS;

typedef struct
{
  CONTROL_BLOCK       control_block; // Control Block

  // Install information
  TCHAR   name [SERVICE_NAME_LEN + 1];          // Service Name
  TCHAR   display_name [SERVICE_NAME_LEN + 1];  // Service Display Name
  TCHAR   * p_help;                             // Service Help (Optional)
  
  // Flags
  BOOL    flag_ipc;           // IPC
  BOOL    flag_database;      // Database
  BOOL    flag_logger;        // Am I the Logger ?
  BOOL    flag_alarm_manager; // Am I the Alarm Manager ?

  // IPC node name
  TCHAR   ipc_node_name  [IPC_NODE_NAME_LENGTH]; // IPC node name
  
  // Interface Routines
  BOOL (* p_init) (TYPE_SERVICE_DATA * pServiceData); // Init (Optional)
  BOOL (* p_main) (TYPE_SERVICE_DATA * pServiceData); // Main  
  BOOL (* p_stop) (TYPE_SERVICE_DATA * pServiceData); // Stop (Optional)

  TYPE_TRX_MGR_FUNCTIONS trx_mgr_function; // Session Management Functions 

} TYPE_SERVICE;

//------------------------------------------------------------------------------
//  PUBLIC DATA STRUCTURES 
//------------------------------------------------------------------------------
extern TYPE_SERVICE GLB_Service; // To be defined in your service

//------------------------------------------------------------------------------
//  PUBLIC FUNCTION PROTOTYPES 
//------------------------------------------------------------------------------
void ServiceStop      (void);

void ServiceLogError (TCHAR     * pFunctionName,
                      TCHAR     * pCalledFunctionName,
                      DWORD     ErrorCode,
                      ENUM_LOG  LogTarget = LOG_TO_BOTH);

void ServiceLogWrongSize (TCHAR    * pFunctionName,
                          DWORD    ExpectedValue,
                          DWORD    ReceivedValue,
                          ENUM_LOG LogTarget = LOG_TO_BOTH);

void ServiceLogMsg (TCHAR     * pFunctionName,
                    TCHAR     * pMessage,
                    ENUM_LOG  LogTarget = LOG_TO_BOTH);

void ServiceLogUnexpected (TCHAR    * pFunctionName,
                           TCHAR    * pFieldName,
                           DWORD    Value,
                           ENUM_LOG LogTarget = LOG_TO_BOTH);

void ServiceLogInvalidLicense (ENUM_LOG  LogTarget = LOG_TO_BOTH);

// AJQ, 05-AUG-2002 Now uses Variable number of arguments ...
void ServiceTrace (const TCHAR * pFormat, ...);

// AJQ, 04-NOV-2002 Common Service Timer Routines
BOOL ServiceTimerCreate  (HANDLE * pTimer, TCHAR * pFunctionName);

BOOL ServiceTimerEnable  (HANDLE Timer, DWORD  Milliseconds, TCHAR * pFunctionName);

BOOL ServiceTimerDisable (HANDLE Timer, TCHAR * pFunctionName);

BOOL ServiceTimerDelete  (HANDLE Timer, TCHAR * pFunctionName);

// AJQ, 18-FEB-2004, Generic TrxMgr Routines
BOOL ServiceTrxMgr_Init          (DWORD TrxType, 
                                  BOOL  AutoQueryCommMgr = TRUE, 
                                  BOOL  AutoQueryTrxMgr  = TRUE);

BOOL ServiceTrxMgr_GetCommMgr    (DWORD * pCommMgrNodeInstId);
BOOL ServiceTrxMgr_GetAllCommMgr (DWORD * pNumCommMgr, DWORD * pCommMgrNodeInstId);


BOOL ServiceTrxMgr_GetDeadTrxMgr (DWORD * pNumDeadTrxMgr, DWORD * pDeadTrxMgrLockId);

// RRT, 10-FEB-2004, General Service Session Management
BOOL ServiceTrxMgr_HandleEventRcv (IPC_OUD_RECEIVE       * pIpcOudReceive,
                                   API_OUTPUT_PARAMS     * pApiOutputParams);

BOOL ServiceTrxMgr_OpenSession    (IPC_OUD_RECEIVE * pIpcOudReceive);

BOOL ServiceTrxMgr_DataReceived   (IPC_OUD_RECEIVE * pIpcOudReceive);

BOOL ServiceTrxMgr_CloseSession   (IPC_OUD_RECEIVE * pIpcOudReceive);

BYTE  ServiceTrxMgr_GetTrxType (void);
DWORD ServiceTrxMgr_GetIpcNodeInstId (void);


#endif // __SERVICE_H
