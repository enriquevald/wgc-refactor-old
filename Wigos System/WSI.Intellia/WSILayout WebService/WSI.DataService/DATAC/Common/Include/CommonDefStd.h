//-----------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//-----------------------------------------------------------------------------
// 
//   MODULE NAME: CommonDefStd.h
//   DESCRIPTION: Standard includes for all projects.
//        AUTHOR: Alberto Cuesta
// CREATION DATE: 08-MAR-2002
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 08-MAR-2002 ACC    Initial draft.
// 
//------------------------------------------------------------------------------

#ifndef __COMMONDEFSTD_H
#define __COMMONDEFSTD_H

//------------------------------------------------------------------------------
// STANDARD INCLUDES
//------------------------------------------------------------------------------

// Defines Windows 2000 as minimum version
#define _WIN32_WINNT 0x0500

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#if defined _WINDOWS && defined LKC_UNICODE
  // Unicode definitions for W32
  #define _UNICODE              // TCHAR functions
  #define UNICODE               // Other unicode compatible functions
#endif

#include <winsock2.h>
#include <windows.h>

// Includes for Windows version
#if defined _WINDOWS

#pragma warning (disable:4996)

#include <tchar.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#else

#error "_WINDOWS is not defined. Check project settings (C/C++, General, Preprocessor definitions)"

#endif

//------------------------------------------------------------------------------
// PUBLIC CONSTANTS
//------------------------------------------------------------------------------

// Thread stack size
#define THREAD_MAX_STACK    4096    // bytes

//------------------------------------------------------------------------------
//  PUBLIC DATATYPES 
//------------------------------------------------------------------------------
// ORACLE definitions
//      - Thread Safety
//      - Avoid to declare the SQLCA as a global variable

//      - Thread Safety
typedef void * SQL_CONTEXT;

// Control Block
typedef DWORD CONTROL_BLOCK;

#endif // __COMMONDEFSTD_H
