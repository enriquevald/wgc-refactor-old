//------------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//------------------------------------------------------------------------------
//
//   MODULE NAME : License.h
//
//   DESCRIPTION : Constant, type and function prototype definitions.
//
//        AUTHOR : Carles Iglesias
//
// CREATION DATE : 01-AUG-2002
//
// REVISION HISTORY :
//
// Date        Author Description
// ----------- ------ -----------------------------------------------------------
// 01-AUG-2002 CIR    Initial draft.
//-------------------------------------------------------------------------------

#ifndef __LKLICENSE_H
#define __LKLICENSE_H

//------------------------------------------------------------------------------
// PUBLIC CONSTANTS
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PUBLIC DATATYPES
//------------------------------------------------------------------------------

typedef enum
{
  LICENSE_TYPE_NEVER_EXPIRE = 0,        // License never expires
  LICENSE_TYPE_EXPIRE_ON_DATE = 1       // License expires on expiration_date.

} ENUM_LICENSE_TYPE;

// TJG 26-JAN-2004
// Added client id to the license (it makes random_filler_5 be necessary)
typedef struct
{
  DWORD                 client_id;          // Client ID
  DWORD                 license_id;         // License ID
  time_t                expiration_date;    // Expiration date (UTC value). 0 if not used.
  time_t                generation_date;    // License generation (UTC value)
  ENUM_LICENSE_TYPE     type;
  INT                   random_filler_1;    // Random fillers to make license more unpredictable.
  INT                   random_filler_2;    // 4 x fillers (2-byte) = 8 bytes 
  INT                   random_filler_3;    // because TYPE_LICENSE size must be 
  INT                   random_filler_4;    // a multiple of 8 (DES encryption purposes)
  INT                   random_filler_5;
                                    
} TYPE_LICENSE;

//------------------------------------------------------------------------------
// PUBLIC DATA STRUCTURES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PUBLIC FUNCTION PROTOTYPES
//------------------------------------------------------------------------------

BOOL        ReadLicense (TYPE_LICENSE    * pLicense,
                         TCHAR           * pFilePath);

BOOL        CreateLicense (TYPE_LICENSE * pLicense);

#endif // __LKLICENSE_H

