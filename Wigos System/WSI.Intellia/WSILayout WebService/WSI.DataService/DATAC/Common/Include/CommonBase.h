//------------------------------------------------------------------------------
// Copyright � 2002-2008 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: CommonBase.h
//
//   DESCRIPTION: Constants, types, variables definitions and prototypes for CommonBase.dll
//
//        AUTHOR: Alberto Cuesta
//
// CREATION DATE: 25-MAR-2002
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ -----------------------------------------------------------
// 25-MAR-2002 ACC    Initial draft.
// 13-MAY-2002 APB    Added definition for COMMON_MODULE_ID_APPLICATION_LOG.
// 16-MAY-2002 APB    Added definition for COMMON_MODULE_ID_SERVICES.
// 19-JUN-2002 APB    Added definition for COMMON_MODULE_ID_GUI_STOCKS.
// 20-JUN-2002 APB    Renamed constant COMMON_MODULE_ID_GUI_STOCKS to 
//                    COMMON_MODULE_ID_GUI_TICKETS_MANAGER.
// 01-AUG-2002 APB    Added definition for COMMON_MODULE_ID_RESOURCE_MANAGER.
// 27-AUG-2002 APB    Added definition for COMMON_MODULE_ID_BATCH_TRX_MGR.
// 02-OCT-2002 APB    Added definition for COMMON_MODULE_ID_ONLINE_TRX_MGR.
// 19-NOV-2002 APB    Added definition for COMMON_MODULE_ID_LKC_DATABASE.
// 27-NOV-2002 APB    Added definition for COMMON_MODULE_ID_GUI_SALES_CONTROL.
// 09-JAN-2003 APB    Added definition for COMMON_MODULE_ID_GUI_JACKPOT_MANAGER.
// 23-JAN-2003 APB    Added definition for COMMON_MODULE_ID_GUI_SYSTEM_MONITOR.
// 28-APR-2003 APB    Renamed COMMON_MODULE_ID_GUI_SALES_CONTROL to 
//                    COMMON_MODULE_ID_GUI_STATISTICS.
// 21-MAY-2003 APB    Added definition for COMMON_MODULE_ID_SW_DOWNLOAD_MGR.
// 22-MAY-2003 APB    Added definition for COMMON_MODULE_ID_GUI_INVOICE_SYSTEM.
// 04-JUN-2003 APB    Added definition for COMMON_MODULE_ID_GUI_SW_DOWNLOAD.
// 04-DEC-2003 ODC    Added definition for COMMON_MODULE_ID_GUI_LAUNCH.
// 17-MAR-2004 JOR    Added definition for COMMON_MODULE_ID_DATE_CODE.
// 29-OCT-2004 GZ     Added definition for COMMON_MODULE_ID_LKAS_FUNCTIONS.
//------------------------------------------------------------------------------

#ifndef __COMMONBASE_H
#define __COMMONBASE_H

//------------------------------------------------------------------------------
//  INCLUDES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PUBLIC CONSTANTS 
//------------------------------------------------------------------------------

// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the COMMONBASE_API_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// COMMONBASE_API_EXPORTS functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.

#ifdef COMMONBASE_API_EXPORTS
#define COMMONBASE_API __declspec(dllexport)
#else
#define COMMONBASE_API __declspec(dllimport)
#endif


// Return Codes
#define COMMON_BASE_OK        0
#define COMMON_BASE_ERROR     1

#define COMMON_DEFAULT_STRING_SIZE              128
#define COMMON_DB_USER_SIZE                     20
#define COMMON_DB_CONNECT_STRING_SIZE           30

// Date constants
#define COMMON_MAX_SIZE_DATE                    100     // Taken from SYSTEM date format
#define COMMON_SIZE_TIME                        11      // Format: hh:mm:ss.cc

// Time format
#define COMMON_TIME_FORMAT_HM                   0       // hh:mm
#define COMMON_TIME_FORMAT_HMS                  1       // hh:mm:ss
#define COMMON_TIME_FORMAT_HMSC                 2       // hh:mm:ss.cc

// Configuration constants
#define COMMON_MAX_PATH_SIZE                    _MAX_PATH // Visual C++ constant
//// Internals
//#define COMMON_CONFIGURATION_KEY                _T("Configuration")
//#define COMMON_DATABASE_KEY                     _T("Database")
//#define COMMON_DB_SUBKEY_ODBC                   _T("Odbc")
//#define COMMON_DB_SUBKEY_USER                   _T("User")
//#define COMMON_DB_SUBKEY_PASSWORD               _T("Password")
//#define COMMON_DB_SUBKEY_CONNECT_STRING         _T("ConnectString")
//#define COMMON_CONFIG_SUBKEY_LANGUAGE           _T("Language")
//#define COMMON_CONFIG_SUBKEY_LOG_PATH           _T("LogPath")

#define COMMON_CONF_FILE_NAME                   _T("LKSYSTEM.CFG")
#define COMMON_CONF_ENTRY_REGISTRY              _T("REGISTRY=")
#define COMMON_CONF_ENTRY_CLIENT_ID             _T("CLIENT_ID=")
#define COMMON_CONF_ENTRY_LANGUAGE_ID           _T("LANGUAGE_ID=")
#define COMMON_CONF_ENTRY_CONNECT_STRING        _T("CONNECT_STRING=")
#define COMMON_CONF_ENTRY_REGISTRY_PATH_SIZE    128

// Char constants
#define COMMON_CR_CHAR                          13
#define COMMON_LF_CHAR                          10
#define COMMON_SEMICOLON_CHAR                   ';'
#define COMMON_SPACE_CHAR                       ' '
#define COMMON_BACKSLASH_CHAR                   '\\'
#define COMMON_NULL_CHAR                        '\0'

// Return codes
#define COMMON_OK                               0
#define COMMON_ERROR                            1
#define COMMON_ERROR_CONTROL_BLOCK              2
#define COMMON_ERROR_DB_NOT_CONNECTED           3
#define COMMON_ERROR_DB                         4
#define COMMON_ERROR_SAFEARRAY_ACCESS           5
#define COMMON_ERROR_PARAMETER                  6
#define COMMON_ERROR_NOT_FOUND                  7
#define COMMON_ERROR_REGISTRY                   8
#define COMMON_ERROR_BAD_FORMAT_FILE            9
#define COMMON_ERROR_CONTEXT                   10
#define COMMON_ERROR_PASSWORD                  11
#define COMMON_ERROR_DUPLICATE_KEY             12
#define COMMON_ERROR_ARRAY_OVERFLOW            13
#define COMMON_ERROR_DUPLICATED_PASSWORD       14
#define COMMON_ERROR_ALREADY_EXISTS            15
#define COMMON_ERROR_DB_FATAL                  16
#define COMMON_ERROR_NOT_SUPORTED              17
#define COMMON_ERROR_NOT_EXIST                 18

#define COMMON_MAX_REGISTRY_KEY_NAME    80
#define COMMON_MAX_REGISTRY_VALUE_NAME  80
#define COMMON_MAX_REGISTRY_VALUE_DATA  80
#define COMMON_MAX_REGISTRY_KEYS        1000

#define COMMON_REG_KEY_TYPE_MACHINE     0
#define COMMON_REG_KEY_TYPE_USER        1

#define Common_MB2WC(mb_str, len_mb, wc_str, len_wc) Common_MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, mb_str, (int) len_mb, wc_str, (int) len_wc)
#define Common_WC2MB(wc_str, len_wc, mb_str, len_mb) Common_WideCharToMultiByte(CP_ACP, MB_PRECOMPOSED, wc_str, (int) len_wc, mb_str, (int) len_mb)

//------------------------------------------------------------------------------
//  PUBLIC DATATYPES 
//------------------------------------------------------------------------------
typedef struct
{
  TCHAR          * p_user; // COMMON_DB_USER_SIZE + 1
  TCHAR          * p_password; // COMMON_DB_PASSWORD_SIZE + 1
  TCHAR          * p_connect_string; // COMMON_DB_CONNECT_STRING_SIZE + 1

} TYPE_CENTER_CONFIGURATION_DB;

typedef struct
{
  CONTROL_BLOCK                control_block;
  TYPE_CENTER_CONFIGURATION_DB database;
  DWORD                        language;

} TYPE_CENTER_CONFIGURATION;

typedef void TYPE_VOID_DATE_TIME;

typedef struct 
{
  DWORD       control_block;
  SYSTEMTIME  utc_date_time;
  SYSTEMTIME  current_date_time;
  TCHAR       current_date_string [COMMON_MAX_SIZE_DATE + 1 + 3];  // Must be multiple 8 ( 100+1+4 = 104) 104/8=13
  TCHAR       current_time_string [COMMON_SIZE_TIME + 1 + 4];      // Must be multiple 8 ( 11+1+4 = 16) 16/8=2

} TYPE_GET_DATE_TIME;


// Module's identifiers
#define COMMON_MODULE_ID_ALARMS                 1
#define COMMON_MODULE_ID_VERSION_CONTROL        2
#define COMMON_MODULE_ID_GUI_ALARMS             3
#define COMMON_MODULE_ID_GUI_COMM               4
#define COMMON_MODULE_ID_GUI_CONTROL            5
#define COMMON_MODULE_ID_GUI_CONF               6
#define COMMON_MODULE_ID_GUI_VERSION_CONTROL    7
#define COMMON_MODULE_ID_LOGGER                 8
#define COMMON_MODULE_ID_GUI_CONTROLS           9
#define COMMON_MODULE_ID_GUI_SETUP              10
#define COMMON_MODULE_ID_GUI_AUDIT              11
#define COMMON_MODULE_ID_GUI_COMMON_MISC        12
#define COMMON_MODULE_ID_APPLICATION_LOG        13
#define COMMON_MODULE_ID_SERVICES               14
#define COMMON_MODULE_ID_GUI_TICKETS_MANAGER    15
#define COMMON_MODULE_ID_RESOURCE_MANAGER       16
#define COMMON_MODULE_ID_BATCH_TRX_MGR          17
#define COMMON_MODULE_ID_ONLINE_TRX_MGR         18
#define COMMON_MODULE_ID_TRX_AUDIT              19
#define COMMON_MODULE_ID_LKC_DATABASE           20
#define COMMON_MODULE_ID_GUI_STATISTICS         21
#define COMMON_MODULE_ID_GUI_JACKPOT_MANAGER    22
#define COMMON_MODULE_ID_GUI_SYSTEM_MONITOR     23
#define COMMON_MODULE_ID_SW_DOWNLOAD_MGR        24
#define COMMON_MODULE_ID_GUI_INVOICE_SYSTEM     25
#define COMMON_MODULE_ID_GUI_SW_DOWNLOAD        26
#define COMMON_MODULE_ID_GUI_LAUNCH             27
#define COMMON_MODULE_ID_GUI_DATE_CODE          28
#define COMMON_MODULE_ID_LKAS_FUNCTIONS         29
#define COMMON_MODULE_ID_GUI_CLASS_II           30
#define COMMON_MODULE_ID_GUI_PLAYER_TRACKING    30

// Client modules id's should start with COMMON_MODULE_ID_CLIENT_BASE
#define COMMON_MODULE_ID_CLIENT_BASE            100


// ALARMS
//              ID0      ID1   ID2   PRIORITY
// EVENTS:     0 - 99     0     0    ALARM_PRIORITY_EVENT 
// SYSTEM:   100 - 999   nnnn  nnnn  ALARM_PRIORITY_SYSTEM
// CLIENTS: 1000 - 9999  nnnn  nnnn  ALARM_PRIORITY_xxxxxx
// --- Events ---
#define ALARM_ID_EVENT    0     // Events
// --- Traps ---
#define ALARM_ID_TRAP     100   // Unhandled exceptions
// --- Clients ---
#define ALARM_ID_STOCKS   1000  // Stocks ...
#define ALARM_ID_DISK     1100  // Disk

// Number of retries to connect Database.
#define COMMON_NUM_RETRIES                      2
#define COMMON_RECONNECT_TIME_OUT               10000 // 10000 mseg, 10 seconds 


//------------------------------------------------------------------------------
//  PUBLIC DATA STRUCTURES 
//------------------------------------------------------------------------------
// 30-NOV-2004
typedef DWORD CTIME;

typedef enum
{
  GET_TIME_LOCAL_TIME     = 0,
  GET_TIME_UNIVERSAL_TIME = 1

} ENUM_GET_TIME;

// Locale information
// AJQ 17-NOV-2003 Added the Special Format
typedef struct
{
  TCHAR     number_decimal_symbol;                    // '.' or ','
  BYTE      number_digits_after_decimal;              // Number of fractional digits
  BYTE      number_special_format;
  TCHAR     number_digit_grouping_symbol;             // '.' or ','

  TCHAR     currency_symbol [COMMON_MAX_CURRENCY_SYMBOL + 1]; // '$', '�', ... 
  WORD      currency_symbol_position;                 // Position of the monetary symbol in the positive currency mode
                                                          // COMMON_CURRENCY_PREFIX  
                                                          // COMMON_CURRENCY_SUFFIX
                                                          // COMMON_CURRENCY_PREFIX_SEP
                                                          // COMMON_CURRENCY_SUFFIX_SEP 
  TCHAR     currency_decimal_symbol;                  // '.' or ','
  BYTE      currency_digits_after_decimal;            // Number of fractional digits
  BYTE      currency_special_format;
  TCHAR     currency_digit_grouping_symbol;           // '.' or ','

  TCHAR     date_format [COMMON_MAX_DATE_FORMAT + 1]; // combination of: d, dd, M, MM, y, yy, yyyy 
                                                      // with separators: '/', '-', '.', ':', ';', '\' or ' '.
  TCHAR     time_format [COMMON_MAX_TIME_FORMAT + 1]; // combination of: H, HH, m, mm, s, ss 
                                                      // with separators: '/', '-', '.', ':', ';', '\' or ' '.
} TYPE_COMMON_LOCALE_INFO;

typedef struct
{
  BYTE    commission_id;
  float   sales_pct;
  float   major_prizes_pct;
  float   minor_prizes_pct;

} TYPE_COMMON_COMMISSION_PCT;

typedef struct
{
  DWORD   key_type; //0 LOCAL_MACHINE, 1 CURRENT_USER   
  TCHAR   key_name    [COMMON_MAX_REGISTRY_KEY_NAME];
  TCHAR   value_name  [COMMON_MAX_REGISTRY_VALUE_NAME];
  DWORD   value_type;
  DWORD   value_length;
  TCHAR   value_data  [COMMON_MAX_REGISTRY_VALUE_DATA];
} TYPE_REGISTRY_DATA_ITEM;

//------------------------------------------------------------------------------
//  PUBLIC FUNCTION PROTOTYPES 
//------------------------------------------------------------------------------

// Miscellaneous functions
COMMONBASE_API void WINAPI Common_InitRandomSeed (void);

//COMMONBASE_API WORD WINAPI Common_Random (void);

COMMONBASE_API DWORD WINAPI Common_Random (void);

COMMONBASE_API void WINAPI Common_Beep (void);

// Date functions
COMMONBASE_API void WINAPI Common_FormatDateTime (SYSTEMTIME    * pDateTime,
                                                  WORD          TimeFormat,
                                                  TCHAR         * pFormattedDate,
                                                  TCHAR         * pFormattedTime);

COMMONBASE_API DWORD WINAPI Common_GetDateTime (TYPE_GET_DATE_TIME * pDateTime,
                                                UINT               TimeFormat = COMMON_TIME_FORMAT_HMSC);

COMMONBASE_API void WINAPI Common_SystemTimeToCTime (const SYSTEMTIME   * pSystemTime, 
                                                     DWORD              * pCTime);

COMMONBASE_API void WINAPI Common_CTimeToSystemTime (DWORD         CTime, 
                                                     SYSTEMTIME    * pSystemTime);

COMMONBASE_API time_t WINAPI Common_DateTimeToCTime (TYPE_DATE_TIME   * pDateTime);

COMMONBASE_API VOID Common_CTimeToDateTime (time_t Time, TYPE_DATE_TIME *pDateTime, BOOL Local);

COMMONBASE_API DWORD WINAPI Common_GetModuleFileName (HANDLE Module, 
                                                      TCHAR * pModulePath, 
                                                      DWORD ModulePathLen);

COMMONBASE_API void WINAPI Common_GetComputerName (TCHAR * pComputerName, 
                                                   DWORD ComputerNameLen);

// Database Date/Time
COMMONBASE_API void WINAPI Common_DbDateTimeToDateTime (TYPE_VOID_DATE_TIME * pDbDateTime, TYPE_DATE_TIME * pDateTime);

COMMONBASE_API void WINAPI Common_DateTimeToDbDateTime (TYPE_DATE_TIME * pDateTime, TYPE_VOID_DATE_TIME * pDbDateTime);

// Connection Scheduling
COMMONBASE_API BOOL WINAPI Common_ComputeNextRunAt (DWORD           Frequency, 
                                                    DWORD           FrequencyStep,
                                                    DWORD           Enabled,
                                                    DWORD           NumTimesAllowed,
                                                    DWORD           NumRuns,
                                                    TYPE_DATE_TIME  * pRunAt, 
                                                    TYPE_DATE_TIME  * pRunFrom, 
                                                    TYPE_DATE_TIME  * pRunTo, 
                                                    TYPE_DATE_TIME  * pDateNow, 
                                                    TYPE_DATE_TIME  * pNextRunAt);

// 04-NOV-2002, AJQ
// Registry Functions

COMMONBASE_API DWORD WINAPI Common_GetRegistryValueDWord (TCHAR     * pKeyName,
                                                          TCHAR     * pSubKeyName,
                                                          DWORD     * pSubKeyValue);

COMMONBASE_API DWORD WINAPI  Common_GetRegistryValueBin (TCHAR       * pKeyName,
                                                         TCHAR       * pSubKeyName,
                                                         BYTE        * pSubKeyValue,
                                                         DWORD       * pSubKeyValueLength);

COMMONBASE_API DWORD WINAPI  Common_GetRegistryValueString (TCHAR    * pKeyName,
                                                            TCHAR    * pSubKeyName,
                                                            TCHAR    * pSubKeyValue,
                                                            DWORD    * pSubKeyValueLength);

COMMONBASE_API DWORD WINAPI  Common_SaveRegistryValueDWord (TCHAR    * pKeyName,
                                                            TCHAR    * pSubKeyName,
                                                            DWORD    * pSubKeyValue);

COMMONBASE_API DWORD WINAPI  Common_SaveRegistryValueString (TCHAR   * pKeyName,
                                                             TCHAR   * pSubKeyName,
                                                             TCHAR   * pSubKeyValue);

COMMONBASE_API DWORD WINAPI  Common_SaveRegistryValueBin (TCHAR       * pKeyName,
                                                          TCHAR       * pSubKeyName,
                                                          BYTE        * pSubKeyValue,
                                                          DWORD       * pSubKeyValueLength);

COMMONBASE_API DWORD WINAPI  Common_GetRegistryValue (TCHAR * pKeyRoot, TYPE_REGISTRY_DATA_ITEM * pRegistryDataItem);

// 04-NOV-2002, AJQ
// Wide char functions
COMMONBASE_API WORD WINAPI Common_MultiByteToWideChar (UINT     CodePage, 
                                                       DWORD    Flags,
                                                       LPCSTR   pMultiByteStr,
                                                       int      MultiByteLength,
                                                       #if !defined (_WINDOWS) || defined (UNICODE)
                                                         LPWSTR pWideCharStr,
                                                       #else
                                                         TCHAR  * pWideCharStr,
                                                       #endif
                                                       int      WideCharSize);

COMMONBASE_API WORD WINAPI Common_WideCharToMultiByte (UINT      CodePage, 
                                                       DWORD     Flags,
                                                       #if !defined (_WINDOWS) || defined (UNICODE)
                                                         LPCWSTR pWideCharStr,  
                                                       #else
                                                         TCHAR   * pWideCharStr,
                                                       #endif
                                                       int       WideCharLength,
                                                       LPSTR     pMultiByteStr, 
                                                       int       MultiByteSize);

COMMONBASE_API BOOL WINAPI Common_CheckIpAddrPort (WORD IpA, WORD IpB, WORD IpC, WORD IpD,
                                                   WORD IpPort, BOOL CheckPort);

COMMONBASE_API DWORD WINAPI Common_GetConfiguration (TYPE_CENTER_CONFIGURATION  * pConfiguration);   

COMMONBASE_API BOOL WINAPI Common_GetClientId (DWORD  * pClientId);

COMMONBASE_API BOOL WINAPI  Common_SetDBUserName (TCHAR * pDBUserName);

COMMONBASE_API DWORD WINAPI Common_DiffTickCount (DWORD   FirstTickCount,
                                                  DWORD   SecondTickCount);

// 12-MAR-2004 GZ
COMMONBASE_API DWORD WINAPI Common_GetConfigurationFile (TYPE_CENTER_CONFIGURATION  * pConfiguration);

COMMONBASE_API DWORD WINAPI Common_GetDBUserPassword (TCHAR       * pUserName,
                                                      TCHAR       * pConnectString,
                                                      TCHAR       * pPassword);

COMMONBASE_API double WINAPI Common_TruncateDouble (double  ValueToTruncate,
                                                    int     Precission);

COMMONBASE_API double WINAPI Common_RoundDouble (double     ValueToRound,
                                                 int        Precission);

COMMONBASE_API void WINAPI Common_DateTimeToDbStrDateTime (TYPE_DATE_TIME * pDateTime, 
                                                           char * pDbStrDateTime);

COMMONBASE_API void WINAPI Common_DbStrDateTimeToDateTime (char * pDbStrDateTime, 
                                                           TYPE_DATE_TIME * pDateTime);

// AJQ 30-NOV-2004 TimeZone
COMMONBASE_API void WINAPI LocalTimeToUniversalTime (SYSTEMTIME            * pLocalTime, 
                                                     SYSTEMTIME            * pUniversalTime, 
                                                     TIME_ZONE_INFORMATION * pTimeZoneInfo = NULL);
                                                     
COMMONBASE_API void WINAPI LocalTimeToUniversalTime (CTIME                 * pLocalTime, 
                                                     CTIME                 * pUniversalTime, 
                                                     TIME_ZONE_INFORMATION * pTimeZoneInfo = NULL);

COMMONBASE_API void WINAPI UniversalTimeToLocalTime (SYSTEMTIME            * pUniversalTime,
                                                     SYSTEMTIME            * pLocalTime, 
                                                     TIME_ZONE_INFORMATION * pTimeZoneInfo = NULL);
                                                     
COMMONBASE_API void WINAPI UniversalTimeToLocalTime (CTIME                 * pUniversalTime,
                                                     CTIME                 * pLocalTime, 
                                                     TIME_ZONE_INFORMATION * pTimeZoneInfo = NULL);
                                                     
COMMONBASE_API void WINAPI SystemTimeToCTime (SYSTEMTIME  * pSystemTime,
                                              CTIME       * pCTime);
                                              
COMMONBASE_API void WINAPI CTimeToSystemTime (CTIME      CTime, 
                                              SYSTEMTIME * pSystemTime);
                                              
COMMONBASE_API void WINAPI TruncateTime (CTIME * pDateTime, 
                                         CTIME * pDate = NULL);
                                         
COMMONBASE_API void WINAPI TruncateTime (SYSTEMTIME * pDateTime, 
                                         SYSTEMTIME * pDate = NULL);
                                         
COMMONBASE_API void WINAPI GetTime   (ENUM_GET_TIME GetTimeType, 
                                      SYSTEMTIME    * pNow);

COMMONBASE_API void WINAPI GetTime   (ENUM_GET_TIME GetTimeType, 
                                      CTIME         * pNow);

COMMONBASE_API void WINAPI GetDate (ENUM_GET_TIME GetTimeType, 
                                    CTIME         * pToday);
                                    
COMMONBASE_API void WINAPI GetDate (ENUM_GET_TIME GetTimeType, 
                                    SYSTEMTIME    * pToday);

COMMONBASE_API DWORD WINAPI Common_ComputeInitCRC (DWORD   InitialCrc, 
                                                   BYTE    * pBuffer, 
                                                   WORD    BufferSize);

COMMONBASE_API DWORD WINAPI Common_ComputeCRC (BYTE    * pBuffer, 
                                               DWORD   BufferSize);

COMMONBASE_API DWORD WINAPI Common_FormatNumber (double Number,
                                                 TCHAR  * pFormattedNumber,
                                                 WORD   FormattedNumberLength,
                                                 INT    DecimalDigits = -1,     // -1 = use user default
                                                 INT    GroupDigits = -1);      // -1 = use user default

COMMONBASE_API void   Common_LogThreadInfo (DWORD  ThreadId);
COMMONBASE_API HANDLE Common_CreateThread  (LPSECURITY_ATTRIBUTES   lpThreadAttributes,
                                            SIZE_T                  dwStackSize,
                                            LPTHREAD_START_ROUTINE  lpStartAddress,
                                            LPVOID                  lpParameter,
                                            DWORD                   dwCreationFlags,
                                            LPDWORD                 lpThreadId,
                                            // Debug Parameters
                                            TCHAR                   * pDbgStartAddress,
                                            TCHAR                   * pDbgSourceFile,
                                            DWORD                   DbgSourceLine,
                                            TCHAR                   * pDbgModule);

COMMONBASE_API void Common_LogProcessThreadTimes (void);

//COMMONBASE_API  BOOL WINAPI Common_ExecuteSystemCommand (TCHAR     * pCommandLine, 
//                                                         DWORD     * pError,
//                                                         DWORD     TimeOut = 0);

#define CREATE_THREAD(attr,stk,fun,p,cflg,tid) Common_CreateThread(attr,stk,fun,p,cflg,tid,#fun,_T(__FILE__), __LINE__, MODULE_NAME)

COMMONBASE_API DWORD WINAPI CommonBase_VersionGetModuleVersion (TCHAR             * pModuleName, 
                                                                TYPE_MODULE_ABOUT * pModuleAbout);

COMMONBASE_API DWORD WINAPI CommonBase_VersionCheckModuleVersion (TCHAR             * pModuleName, 
                                                                  TYPE_MODULE_ABOUT * pModuleAbout);

// 
// - Software Download
//
COMMONBASE_API BOOL WINAPI Common_ExecuteSystemCommand (TCHAR     * pCommandLine, 
                                                        DWORD     * pError,
                                                        DWORD     TimeOut = 0);

COMMONBASE_API  BOOL WINAPI Common_CleanPath (TCHAR   * pWorkingPathName, 
                                              BOOL    DeleteDirectory);

#endif // __COMMONBASE_H
