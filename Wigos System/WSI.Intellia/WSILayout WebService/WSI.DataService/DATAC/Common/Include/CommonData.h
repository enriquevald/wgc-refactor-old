//------------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//------------------------------------------------------------------------------
//
//   MODULE NAME : CommonData.h
//   DESCRIPTION : Constants, types, variables definitions and prototypes
//                 for CommonData
//        AUTHOR : Sergio Calleja
// CREATION DATE : 04-FEB-2003
//
// REVISION HISTORY
//
// Date        Author Description
// ----------- ------ -----------------------------------------------------------
// 04-FEB-2003 SCM    Initial draft.
//------------------------------------------------------------------------------
#ifndef __COMMON_DATA_H
#define __COMMON_DATA_H

//------------------------------------------------------------------------------
//  INCLUDES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PUBLIC CONSTANTS
//------------------------------------------------------------------------------

// The following ifdef block is the standard way of creating macros which make exporting
// from a DLL simpler. All files within this DLL are compiled with the COMMON_DATA_API_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see
// COMMON_DATA_API_EXPORTS functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.

#ifdef COMMON_DATA_API_EXPORTS
#define COMMON_DATA_API __declspec(dllexport)
#else
#define COMMON_DATA_API __declspec(dllimport)
#endif

// Node Types
#define IPC_NODE_TYPE_BASE_PORT 0
#define IPC_NODE_TYPE_CLONABLE  1
#define IPC_NODE_TYPE_UNIQUE    2

#define MAX_IPC_NODE_NAME       30
#define MAX_IPC_DISPLAY_NAME    40
#define MAX_COMM_LINE_TYPE      10

// TODO Uses the values frim the CommonData/CommonConstants
#define APPEND_WATCHDOG         "_WDOG"
#define APPEND_UNIQUE_INSTANCE  "_"

// TODO Uses the values frim the CommonData/CommonConstants
#define IP_MAX_LEN              16
#define IPC_MAX_INST            30
#define IPC_LEN_NODE_NAME       30
#define IPC_COMPUTER_NAME_LEN   30

// Transaction type
#define TRX_NONE                (-1)
#define TRX_BATCH               0
#define TRX_ONLINE              1
#define TRX_JACKPOT             2
#define TRX_SOFTWARE            3

#define NODE_GUI                0
#define NODE_SERVICE            1

// General Params
#define MAX_GENERAL_PARAM_GROUP_KEY         20
#define MAX_GENERAL_PARAM_SUBJECT_KEY       20
#define MAX_GENERAL_PARAM_DEFAULT_VALUE     260
#define MAX_AGENCY_DEVICE_STATUS            10 // Per device

// Launch Bar
#define MAX_LEN_LAUNCH_BAR_DEF      260

// Regional Options
#define TYPE_REG_OPT_ITEM_MAX_LEN   21
#define NUMBERS_DECIMAL_SYMBOL      0
#define NUMBERS_GROUP_SYMBOL        1
#define NUMBERS_DECIMAL_DIGITS      2
#define CURRENCY_SYMBOL_POS         3
#define CURRENCY_DECIMAL_SYMBOL     4
#define CURRENCY_GROUP_SYMBOL       5
#define CURRENCY_DECIMAL_DIGITS     6
#define DATE_FORMAT                 7
#define DATE_GROUP_SYMBOL           8
#define TIME_FORMAT                 9
#define TIME_GROUP_SYMBOL           10

// Registry
#define MAX_REGISTRY_KEY_NAME       80
#define MAX_REGISTRY_VALUE_NAME     80
#define MAX_REGISTRY_VALUE_DATA     80
#define MAX_REGISTRY_VALUES         1000

#define REG_TYPE_LAUNCH_BAR         0

// Device Type
#define DEVICE_LKAS_RELATED         1
#define DEVICE_LKT_RELATED          2

// Invoicing movements
//      - System defined
#define MOVEMENT_TYPE_TICKETS_SALES             1
#define MOVEMENT_TYPE_MAJOR_PRIZES              2
#define MOVEMENT_TYPE_MINOR_PRIZES              3
#define MOVEMENT_TYPE_SPECIAL_PRIZES            4
#define MOVEMENT_TYPE_NETWIN                    5 
#define MOVEMENT_TYPE_SALE_COMMISSIONS          6
#define MOVEMENT_TYPE_MAJOR_PRIZE_COMMISSIONS   7
#define MOVEMENT_TYPE_MINOR_PRIZE_COMMISSIONS   8
#define MOVEMENT_TYPE_EXPIRED_COLLECTS          9 
#define MOVEMENT_TYPE_EXPIRED_CARDS             10
//      - User defined
#define MOVEMENT_TYPE_SYSTEM_MAX_ID             30      // // System movement types can only reach this value
#define MOVEMENT_TYPE_USER_FIRST_ID             MOVEMENT_TYPE_SYSTEM_MAX_ID + 1

//------------------------------------------------------------------------------
// PUBLIC DATATYPES 
//------------------------------------------------------------------------------
// Struct that contains node information
typedef struct
{
  CONTROL_BLOCK   control_block;
  DWORD           ipc_node_internal_id;                         // IPC_NETWORK.IPC_NODE_INTERNAL_ID
  DWORD           node_type;                                    // GUI or Service
  DWORD           trx_type;                                     // Trx Type
  DWORD           type;                                         // Node Type
  TCHAR           node_name    [MAX_IPC_NODE_NAME    + 1 + 1];  // 30 + 1 + 1 = 32
  TCHAR           display_name [MAX_IPC_DISPLAY_NAME + 1 + 3];  // 40 + 1 + 3 = 44

} TYPE_IPC_NODE_DEFINITION;

// Struct that contains lines type information
typedef struct
{
  CONTROL_BLOCK   control_block;
  DWORD           line_type;          // Line Type
  DWORD           nls_id;             // Nls Id
  DWORD           is_unique;          // 0/1 Flag that indicates whether or not is unique

} TYPE_COMM_LINE_TYPE;

//
// General Params
//
typedef struct
{
  CONTROL_BLOCK   control_block;
  TCHAR           group_key           [MAX_GENERAL_PARAM_GROUP_KEY +1 +3];      // 20  + 1 + 3 = 24
  TCHAR           subject_key         [MAX_GENERAL_PARAM_SUBJECT_KEY +1 +3]; // 20  + 1 + 3 = 24
  TCHAR           default_value       [MAX_GENERAL_PARAM_DEFAULT_VALUE +1 +3]; // 260 + 1 + 3 = 264
  DWORD           value_type;         // 0: Integer: 1: Double, 2:String, 3:Fecha, 4:Hora, 5:DiaHora, 6: boolean 
  DWORD           group_key_nls_id;
  DWORD           subject_key_nls_id;
  DWORD           value_max_len;
  BOOL            allows_zero;
  
} TYPE_GENERAL_PARAMS_DEFINITION;

//
// Launch Bar
//
typedef struct
{
  CONTROL_BLOCK   control_block;
  DWORD           caption_message;
  DWORD           tooltip_message;  
  TCHAR           executable_file     [MAX_LEN_LAUNCH_BAR_DEF +1 +3];      // 260 + 1 + 3 = 264
  TCHAR           image_file          [MAX_LEN_LAUNCH_BAR_DEF +1 +3];      // 260 + 1 + 3 = 264
  DWORD           group;
  DWORD           idx_img_list;
} TYPE_LAUNCH_BAR_DEFINITION;

// Auditor Data
typedef struct
{
  CONTROL_BLOCK control_block;
  DWORD         gui_id;
  DWORD         nls_id;

} TYPE_GUI_NAME;

typedef struct
{
  CONTROL_BLOCK control_block;
  DWORD         audit_code;
  DWORD         nls_id;

} TYPE_AUDIT_NAME;

// Invoicing movements
typedef struct
{
  CONTROL_BLOCK control_block;
  WORD          movement_id;
  DWORD         nls_id;

} TYPE_INVOICE_MOVEMENT;

// Launch Bar
typedef struct
{
  TCHAR         key_name [MAX_REGISTRY_KEY_NAME];
  TCHAR         value_name [MAX_REGISTRY_VALUE_NAME];
  DWORD         value_type;
  TCHAR         value_data [MAX_REGISTRY_VALUE_DATA];
  BOOL          auto_create;

} TYPE_REGISTRY;

// Event History
typedef struct
{
  DWORD         status_id;
  DWORD         nls_id;

} TYPE_AGENCY_DEVICE_STATUS;

typedef struct
{
  DWORD         device_code;
  DWORD         device_nls_id;
  DWORD         device_type;
  DWORD         num_device_status;
  TYPE_AGENCY_DEVICE_STATUS     device_status [MAX_AGENCY_DEVICE_STATUS];

} TYPE_AGENCY_DEVICE;

typedef struct
{
  CONTROL_BLOCK control_block;
  DWORD         operation_code;
  DWORD         operation_nls_id;
  DWORD         device_type;

} TYPE_AGENCY_OPERATION;

// LKAS FUNCTION INFO
typedef struct
{
  ENUM_LKAS_FUNCTION_ID   func_code;
  TCHAR                   func_string [MAX_GENERAL_PARAM_SUBJECT_KEY + 1 + 3];  
  
} TYPE_LKAS_FUNCTION_INFO;

//------------------------------------------------------------------------------
// PUBLIC FUNCTION PROTOTYPES
//------------------------------------------------------------------------------

// Regional Options
//COMMON_DATA_API BOOL WINAPI Common_GetRegOpt (DWORD OptionId, TYPE_REG_OPT_LIST * pRegOpt)

// IpcDef
COMMON_DATA_API DWORD WINAPI Common_IpcDefNumNodes  (VOID);
COMMON_DATA_API BOOL  WINAPI Common_IpcDefGetNode   (DWORD NodeIndex, TYPE_IPC_NODE_DEFINITION * pIpcNode);

// CommLines
COMMON_DATA_API DWORD WINAPI Common_GetNumCommLineTypes (VOID);
COMMON_DATA_API BOOL  WINAPI Common_GetCommLineType     (DWORD LineIndex, TYPE_COMM_LINE_TYPE * pCommLineType);

// TrxMgr's and Trx's
COMMON_DATA_API DWORD WINAPI Common_GetTrxMgrNumTrxs  (DWORD TrxMgr);
COMMON_DATA_API BOOL  WINAPI Common_GetTrxMgrTrxNlsId (DWORD TrxMgr, DWORD TrxType, DWORD TrxTypeNlsId);
COMMON_DATA_API BOOL  WINAPI Common_GetTrxMgrTrxType  (DWORD TrxMgr, DWORD Index, DWORD * pTrxType);

// General Params
COMMON_DATA_API DWORD WINAPI Common_GeneralParamDefNum (VOID);
COMMON_DATA_API BOOL  WINAPI Common_GeneralParamGet    (DWORD GeneralParamIndex, TYPE_GENERAL_PARAMS_DEFINITION * pGeneralParam);

// Launch Bar
COMMON_DATA_API DWORD WINAPI Common_LaunchBarDefNum (VOID);

COMMON_DATA_API BOOL  WINAPI Common_LaunchBarGet (DWORD LaunchBarIndex, TYPE_LAUNCH_BAR_DEFINITION * pLaunchBar);

// Agency Devices / Operations
COMMON_DATA_API BOOL WINAPI Common_GetDeviceNlsId (DWORD DeviceCode, DWORD * pDeviceNlsId);

COMMON_DATA_API BOOL WINAPI Common_GetDeviceStatusNlsId (DWORD DeviceCode, 
                                                         DWORD DeviceStatus,
                                                         DWORD * pDeviceStatusNlsId);
COMMON_DATA_API DWORD WINAPI Common_DeviceDefNum (VOID);

COMMON_DATA_API DWORD WINAPI Common_DeviceGetCode (DWORD DeviceIndex);

COMMON_DATA_API DWORD WINAPI Common_DeviceGetType (DWORD DeviceIndex);

COMMON_DATA_API DWORD WINAPI Common_OperationDefNum (VOID);

COMMON_DATA_API BOOL  WINAPI Common_OperationGet (DWORD Index, TYPE_GENERAL_PARAMS_DEFINITION * pDef);

COMMON_DATA_API BOOL  WINAPI Common_GetOperationNlsId (DWORD OperationCode, DWORD * pOperationNlsId);

// Auditor Data
COMMON_DATA_API BOOL WINAPI Common_AuditNameGetNode (DWORD  NodeIndex, TYPE_AUDIT_NAME * pGUIAuditNode);

COMMON_DATA_API DWORD WINAPI Common_AuditNameNumNodes (void);

COMMON_DATA_API BOOL WINAPI Common_GUINameGetNode (DWORD  NodeIndex, TYPE_GUI_NAME * pGUINameNode);

COMMON_DATA_API DWORD WINAPI Common_GUINameNumNodes (void);

// Date Code
COMMON_DATA_API DWORD WINAPI Common_DateCode (DWORD ClientId,
                                              DWORD AgencyId,
                                              DWORD ActualDay,
                                              DWORD ActualMonth,
                                              DWORD ActualYear);

// LKAS Functions
COMMON_DATA_API void WINAPI Common_GetLkasFuncCodeFromString (TCHAR                 * pFuncString, 
                                                              ENUM_LKAS_FUNCTION_ID * pFuncCode);

COMMON_DATA_API BOOL WINAPI Common_GetInvoiceMovement (TYPE_INVOICE_MOVEMENT   * pInvoiveMovement);

#endif // __COMMON_DATA_H

