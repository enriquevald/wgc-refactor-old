//------------------------------------------------------------------------------
// Copyright � 2008 Win Systems Ltd.
//------------------------------------------------------------------------------
//
//   MODULE NAME: MCW_API.h
//   DESCRIPTION: Constants, types, variables definitions and prototypes
//                for the MCW_API
//        AUTHOR: Xavier Ib��ez
// CREATION DATE: 22-SEP-2008
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ -----------------------------------------------------------
// 22-SEP-2008 XID    Initial draft.
//------------------------------------------------------------------------------

#ifndef __MCW_API_H
#define __MCW_API_H

//------------------------------------------------------------------------------
//  INCLUDES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PUBLIC CONSTANTS
//------------------------------------------------------------------------------

#ifdef MCW_API_EXPORTS
#define MCW_API __declspec(dllexport)
#else
#define MCW_API __declspec(dllimport)
#endif

// Module function codes
#define MCW_CODE_MODULE_INIT            1
#define MCW_CODE_DEVICE_INIT            2
#define MCW_CODE_DEVICE_STATUS_QUERY    3
#define MCW_CODE_WRITE                  4
#define MCW_CODE_DEVICE_CLOSE           5
#define MCW_CODE_CANCEL_WRITE           6

// Module return codes
#define MCW_STATUS_OK                   0 // API execution OK.
#define MCW_STATUS_ERROR                1 // General execution error.

#define MCW_DEVICE_STATUS_ONLINE        1
#define MCW_DEVICE_STATUS_OFFLINE       2

#define MCW_MAX_DATA_LENGTH             256

#define MCW_MAX_MODELS                  10
#define MCW_MODEL_NAME_SIZE             20

typedef enum
{
  CARD_HI_CO = 1,       // Hi coercivity
  CARD_LOW_CO = 2,      // Low coercivity

} ENUM_CARD_TYPE;

//------------------------------------------------------------------------------
//  PUBLIC DATATYPES
//------------------------------------------------------------------------------

typedef struct
{
  ENUM_CARD_TYPE  card_type;                   // Hi/Low coercivity
  WORD            data1_length;
  BYTE            data1 [MCW_MAX_DATA_LENGTH]; // Track 1
  WORD            data2_length;
  BYTE            data2 [MCW_MAX_DATA_LENGTH]; // Track 2
  WORD            data3_length;
  BYTE            data3 [MCW_MAX_DATA_LENGTH]; // Track 3

} MCW_CARD_DATA;

//
// API
//

// Module Init
typedef struct
{
  CONTROL_BLOCK         control_block;        // Control Block
  WORD                  num_models;
  struct
  {
    WORD                model;
    TCHAR               model_name[MCW_MODEL_NAME_SIZE];
  } model_data[MCW_MAX_MODELS];

} MCW_OUD_MODULE_INIT;

// Device Init
typedef struct
{
  CONTROL_BLOCK         control_block;        // Control Block
  WORD                  model;                // MCW_MODEL_001, etc

} MCW_IUD_DEVICE_INIT;

// Device Status
typedef struct
{
  CONTROL_BLOCK         control_block;        // Control Block
  WORD                  status;               // MCW_DEVICE_STATUS_ONLINE
                                              // MCW_DEVICE_STATUS_OFFLINE

} MCW_OUD_DEVICE_STATUS_QUERY;

// Write
typedef struct
{
  CONTROL_BLOCK         control_block;        // Control Block
  MCW_CARD_DATA         card_data;

} MCW_IUD_WRITE;

// Device Close
// No params

//
// DLL Model
//

// Device Init
typedef TYPE_PARAM_EMPTY  TYPE_MCW_INIT_INPUT;
typedef TYPE_PARAM_EMPTY  TYPE_MCW_INIT_OUTPUT;

// Device Status Query
typedef TYPE_PARAM_EMPTY  TYPE_MCW_GET_STATUS_INPUT;

typedef struct
{
  CONTROL_BLOCK control_block;

  WORD status;     // MCW_DEVICE_STATUS_ONLINE
                   // MCW_DEVICE_STATUS_OFFLINE

} TYPE_MCW_GET_STATUS_OUTPUT;

// Write
typedef struct
{
  CONTROL_BLOCK control_block;
  MCW_CARD_DATA card_data;

} TYPE_MCW_WRITE_INPUT;

typedef TYPE_PARAM_EMPTY  TYPE_MCW_WRITE_OUTPUT;

// Casncel Write
typedef TYPE_PARAM_EMPTY  TYPE_MCW_CANCEL_WRITE_INPUT;
typedef TYPE_PARAM_EMPTY  TYPE_MCW_CANCEL_WRITE_OUTPUT;

// Device Close
typedef TYPE_PARAM_EMPTY  TYPE_MCW_CLOSE_INPUT;
typedef TYPE_PARAM_EMPTY  TYPE_MCW_CLOSE_OUTPUT;

//------------------------------------------------------------------------------
//  PUBLIC DATA STRUCTURES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PUBLIC FUNCTION PROTOTYPES
//------------------------------------------------------------------------------

MCW_API WORD WINAPI Mcw_API (API_INPUT_PARAMS     * pApiInputParams,
                             API_INPUT_USER_DATA  * pApiInputUserData,
                             API_OUTPUT_PARAMS    * pApiOutputParams,
                             API_OUTPUT_USER_DATA * pApiOutputUserData);

#endif // __MCW_API_H