// ------------------------------------------------------------------------------------
// Copyright � 2002 Win Systems
// ------------------------------------------------------------------------------------
//
//   MODULE NAME: NLS_Common.h
//
//   DESCRIPTION: NLS Id ranges for Common LKC modules
//
//        AUTHOR: Carlos A. Costa
//
// CREATION DATE: 13-MAR-2002
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ -------------------------------------------------------------------
// 13-MAR-2002 CC     Initial release.
// 18-MAR-2002 APB    Added COMMON_MODULE_LOGGER definition.
// 25-MAR-2002 APB    Added COMMON_MODULE_GUI_CONTROLS definition.
// 16-APR-2002 APB    Added COMMON_MODULE_GUI_COMMON_MISC definition.
// 13-MAY-2002 APB    Added COMMON_MODULE_APPLICATION_LOG definition.
// 16-MAY-2002 APB    Added COMMON_MODULE_SERVICES definition.
// 19-JUN-2002 APB    Added COMMON_MODULE_GUI_STOCKS definition.
// 20-JUN-2002 APB    Renamed COMMON_MODULE_GUI_STOCKS to COMMON_MODULE_GUI_TICKETS_MANAGER.
// 01-AUG-2002 APB    Added COMMON_MODULE_RESOURCE_MANAGER definition.
// 27-AUG-2002 APB    Added COMMON_MODULE_BATCH_TRX_MGR definition.
// 02-OCT-2002 APB    Added COMMON_MODULE_ONLINE_TRX_MGR definition.
// 18-OCT-2002 APB    Added COMMON_MODULE_TRX_AUDIT definition.
// 19-NOV-2002 APB    Added COMMON_MODULE_LKC_DATABASE definition.
// 27-NOV-2002 APB    Added COMMON_MODULE_GUI_SALES_CONTROL definition.
// 09-JAN-2003 APB    Added COMMON_MODULE_GUI_JACKPOT_MANAGER definition.
// 23-JAN-2003 APB    Added COMMON_MODULE_GUI_SYSTEM_MONITOR definition.
// 23-JAN-2003 APB    Renamed COMMON_MODULE_GUI_SALES_CONTROL to COMMON_MODULE_GUI_STATISTICS.
// 21-MAY-2003 APB    Added COMMON_MODULE_SW_DOWNLOAD_MGR definition.
// 22-MAY-2003 APB    Added COMMON_MODULE_GUI_INVOICE_SYSTEM definition.
// 04-JUN-2003 APB    Added COMMON_MODULE_GUI_SW_DOWNLOAD definition.
// 04-DEC-2003 OC     Added COMMON_MODULE_GUI_LAUNCH definition.
// 17-MAR-2004 JOR    Added COMMON_MODULE_GUI_DATE_CODE definition.
// 29-OCT-2004 GZ     Added COMMON_MODULE_LKAS_FUNCTIONS definition.
// 15-MAR-2012 AJQ & RCI    Added COMMON_MODULE_REPORT definition.

// --------------------------------------------------------------------------------------

#ifndef __NLS_COMMON_MODULES
#define __NLS_COMMON_MODULES

// NLS Id ranges for Common LKC modules
#define COMMON_MODULE_ALARMS                1000    // Alarms
#define COMMON_MODULE_VERSION_CONTROL       1500    // Version Control
#define COMMON_MODULE_GUI_ALARMS            2000    // GUI Alarms
#define COMMON_MODULE_GUI_COMM              2500    // GUI Communications
#define COMMON_MODULE_GUI_CONTROL           3000    // GUI Control
#define COMMON_MODULE_GUI_CONF              3500    // GUI Configuration
#define COMMON_MODULE_GUI_VERSION_CONTROL   4000    // GUI Version Control
#define COMMON_MODULE_LOGGER                4500    // Logger Messages
#define COMMON_MODULE_GUI_CONTROLS          5000    // GUI Controls
#define COMMON_MODULE_GUI_SETUP             5500    // GUI Setup
#define COMMON_MODULE_GUI_AUDIT             6000    // GUI Auditor
#define COMMON_MODULE_GUI_COMMON_MISC       6500    // GUI Common Miscellaneous
#define COMMON_MODULE_APPLICATION_LOG       7000    // Application Log Service
#define COMMON_MODULE_SERVICES              7500    // Common Services Library
#define COMMON_MODULE_GUI_TICKETS_MANAGER   8000    // Gui Tickets Management
#define COMMON_MODULE_RESOURCE_MANAGER      8500    // Resource Manager
#define COMMON_MODULE_BATCH_TRX_MGR         9000    // Batch Transaction Manager
#define COMMON_MODULE_TRX_AUDIT             9500    // Transaction Audit: Transaction Manager names and types
#define COMMON_MODULE_ONLINE_TRX_MGR       10000    // Online Transaction Manager
#define COMMON_MODULE_LKC_DATABASE         10500    // LKC Database incidence messages
#define COMMON_MODULE_GUI_STATISTICS       11000    // GUI Statistics
#define COMMON_MODULE_GUI_JACKPOT_MANAGER  11500    // GUI Jackpot Manager
#define COMMON_MODULE_GUI_SYSTEM_MONITOR   12000    // GUI System Monitor
#define COMMON_MODULE_SW_DOWNLOAD_MGR      12500    // Software Download Manager
#define COMMON_MODULE_GUI_INVOICE_SYSTEM   13000    // GUI Invoice System
#define COMMON_MODULE_GUI_SW_DOWNLOAD      13500    // GUI Software Download
#define COMMON_MODULE_GUI_LAUNCH           14000    // GUI Launch Bar
#define COMMON_MODULE_GUI_DATE_CODE        14500    // GUI Date Code
#define COMMON_MODULE_LKAS_FUNCTIONS       15000    // LKAS Functions
#define COMMON_MODULE_GUI_CLASS_II         15500    // GUI Class II
#define COMMON_MODULE_GUI_PLAYER_TRACKING  16000    // GUI Player Tracking

#define COMMON_MODULE_RESERVED             17000    // Continue GUI Player Tracking

#define COMMON_MODULE_REPORT               50000    // Reports
#define COMMON_MODULE_GUI_PSA              20000    // PSA

#define COMMON_CLIENT_START                60000    // Client modules start their NLS id's from this 

#define NLS_ID_ALARMS(n)                    (COMMON_MODULE_ALARMS + n)
#define NLS_ID_VERSION_CONTROL(n)           (COMMON_MODULE_VERSION_CONTROL + n)
#define NLS_ID_GUI_ALARMS(n)                (COMMON_MODULE_GUI_ALARMS + n)
#define NLS_ID_GUI_COMM(n)                  (COMMON_MODULE_GUI_COMM + n)
#define NLS_ID_GUI_CONF(n)                  (COMMON_MODULE_GUI_CONF + n)
#define NLS_ID_GUI_CONTROL(n)               (COMMON_MODULE_GUI_CONTROL + n)
#define NLS_ID_GUI_VERSION_CONTROL(n)       (COMMON_MODULE_GUI_VERSION_CONTROL + n)
#define NLS_ID_LOGGER(n)                    (COMMON_MODULE_LOGGER + n)
#define NLS_ID_GUI_CONTROLS(n)              (COMMON_MODULE_GUI_CONTROLS + n)
#define NLS_ID_GUI_SETUP(n)                 (COMMON_MODULE_GUI_SETUP + n)
#define NLS_ID_GUI_AUDIT(n)                 (COMMON_MODULE_GUI_AUDIT + n)
#define NLS_ID_GUI_COMMON_MISC(n)           (COMMON_MODULE_GUI_COMMON_MISC + n)
#define NLS_ID_APPLICATION_LOG(n)           (COMMON_MODULE_APPLICATION_LOG + n)
#define NLS_ID_SERVICES(n)                  (COMMON_MODULE_SERVICES + n)
#define NLS_ID_GUI_TICKETS_MANAGER(n)       (COMMON_MODULE_GUI_TICKETS_MANAGER + n)
#define NLS_ID_RESOURCE_MANAGER(n)          (COMMON_MODULE_RESOURCE_MANAGER + n)
#define NLS_ID_BATCH_TRX_MGR(n)             (COMMON_MODULE_BATCH_TRX_MGR + n)
#define NLS_ID_TRX_AUDIT(n)                 (COMMON_MODULE_TRX_AUDIT + n)
#define NLS_ID_ONLINE_TRX_MGR(n)            (COMMON_MODULE_ONLINE_TRX_MGR + n)
#define NLS_ID_LKC_DATABASE(n)              (COMMON_MODULE_LKC_DATABASE + n)
#define NLS_ID_GUI_STATISTICS(n)            (COMMON_MODULE_GUI_STATISTICS + n)
#define NLS_ID_GUI_JACKPOT_MANAGER(n)       (COMMON_MODULE_GUI_JACKPOT_MANAGER + n)
#define NLS_ID_GUI_SYSTEM_MONITOR(n)        (COMMON_MODULE_GUI_SYSTEM_MONITOR + n)
#define NLS_ID_SOFTWARE_DOWNLOAD_MGR(n)     (COMMON_MODULE_SW_DOWNLOAD_MGR + n)
#define NLS_ID_GUI_SW_DOWNLOAD(n)           (COMMON_MODULE_GUI_SW_DOWNLOAD + n)
#define NLS_ID_GUI_INVOICE_SYSTEM(n)        (COMMON_MODULE_GUI_INVOICE_SYSTEM + n)
#define NLS_ID_GUI_LAUNCH(n)                (COMMON_MODULE_GUI_LAUNCH + n)
#define NLS_ID_GUI_DATE_CODE(n)             (COMMON_MODULE_GUI_DATE_CODE + n)
#define NLS_ID_LKAS_FUNCTIONS(n)            (COMMON_MODULE_LKAS_FUNCTIONS + n)
#define NLS_ID_GUI_CLASS_II(n)              (COMMON_MODULE_GUI_CLASS_II + n)
#define NLS_ID_GUI_PLAYER_TRACKING(n)       (COMMON_MODULE_GUI_PLAYER_TRACKING + n)
#define NLS_ID_REPORT(n)                    (COMMON_MODULE_REPORT + n)
#define NLS_ID_GUI_PSA(n)                   (COMMON_MODULE_GUI_PSA + n)

#define NLS_ID_REPORT_NAME(n)               NLS_ID_REPORT(n)
#define NLS_ID_REPORT_DESC(n)               NLS_ID_REPORT(1000 + n)

#define NLS_ID_REPORT_DEFINE(n, name, desc)\
NLS_ID_REPORT_NAME(n) name \
NLS_ID_REPORT_DESC(n) desc 

#endif // __NLS_COMMON_MODULES
