//------------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Communications.h
//   DESCRIPTION: Constants, types, variables definitions and prototypes 
//                for Communications
//        AUTHOR: Andreu Juli�
// CREATION DATE: 04-JUL-2002
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ -----------------------------------------------------------
// 04-JUL-2002 AJQ    Initial draft.
// 
//------------------------------------------------------------------------------

#ifndef __COMMUNICATIONS_H
#define __COMMUNICATIONS_H

//------------------------------------------------------------------------------
//  INCLUDES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PUBLIC CONSTANTS 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PUBLIC DATATYPES 
//------------------------------------------------------------------------------
// Message Commands
#define IPC_MSG_CMD_COMM_MGR_OPEN_SESSION           IPC_MSG_CMD_COMM_MGR(1)
#define IPC_MSG_CMD_COMM_MGR_CLOSE_SESSION          IPC_MSG_CMD_COMM_MGR(2)
#define IPC_MSG_CMD_COMM_MGR_SEND                   IPC_MSG_CMD_COMM_MGR(3)
#define IPC_MSG_CMD_COMM_MGR_SESSION_OPENED         IPC_MSG_CMD_COMM_MGR(4)
#define IPC_MSG_CMD_COMM_MGR_SESSION_CLOSED         IPC_MSG_CMD_COMM_MGR(5)
#define IPC_MSG_CMD_COMM_MGR_DATA_RECEIVED          IPC_MSG_CMD_COMM_MGR(6)
#define IPC_MSG_CMD_COMM_MGR_REFRESH_PHYS_ADD       IPC_MSG_CMD_COMM_MGR(7)
#define IPC_MSG_CMD_COMM_MGR_STATS                  IPC_MSG_CMD_COMM_MGR(8)
#define IPC_MSG_CMD_COMM_MGR_BROADCAST              IPC_MSG_CMD_COMM_MGR(9)


#define IPC_MSG_CMD_COMM_MGR_TRP_CONNECTED          IPC_MSG_CMD_COMM_MGR(201)  // Used between CommMgr's

#define IPC_MSG_CMD_COMM_MGR_QUERY_TRP_SESSIONS     IPC_MSG_CMD_COMM_MGR(202)
#define IPC_MSG_CMD_COMM_MGR_OPEN_SESSION_REDIRECT  IPC_MSG_CMD_COMM_MGR(203)


#define IPC_MSG_CMD_COMM_MGR_LINK_STATS             IPC_MSG_CMD_COMM_MGR(101)
#define IPC_MSG_CMD_COMM_MGR_PROTOCOL_STATS         IPC_MSG_CMD_COMM_MGR(102)
#define IPC_MSG_CMD_COMM_MGR_TRANSPORT_STATS        IPC_MSG_CMD_COMM_MGR(103)

// Comm Mgr Status
#define COMM_MGR_STATUS_OK                      0
#define COMM_MGR_STATUS_ERROR                   1
#define COMM_MGR_STATUS_DISCONNECTED            2
#define COMM_MGR_STATUS_DISCONNECTED_TIMEOUT    3
#define COMM_MGR_STATUS_BUSY                    4
#define COMM_MGR_STATUS_NO_HARDWARE             5
#define COMM_MGR_STATUS_ERROR_TRX_TYPE          7
#define COMM_MGR_STATUS_COMM_DISABLED           8


//------------------------------------------------------------------------------
//  PUBLIC DATA STRUCTURES 
//------------------------------------------------------------------------------
typedef struct
{
  CONTROL_BLOCK   control_block;
  BYTE            trx_type;     // Trx Type
  DWORD           trx_mgr_id;   // The TrxMgr that requested the session

} TYPE_COMM_MGR_OPEN_SESSION;

typedef struct
{
  CONTROL_BLOCK   control_block;
  DWORD           protocol;         // Requests the LKC2AS stats
  DWORD           protocol_reset;   // Reset the LKC2AS stats
  DWORD           transport;        // Request the TRP stats
  DWORD           transport_reset;  // Reset the TRP stats
  DWORD           links;            // Requests the list of 'connected' links at TRP level

} TYPE_COMM_MGR_STATS_REQUEST;

typedef struct
{
  DWORD frames_ok;                    // Total frames OK
  DWORD frames_error;                 // Total frames error
  
  DWORD error_fragmentation;          // Bad frames with fragmentation error
  DWORD error_size;                   // Bad frames with data size error
  DWORD error_address;                // Bad frames with target address error
  
} TYPE_COMM_MGR_STATS_DIRECTION;

typedef struct
{
  TYPE_COMM_MGR_STATS_DIRECTION stats_input;
  TYPE_COMM_MGR_STATS_DIRECTION stats_output;

} TYPE_COMM_MGR_TRP_STATS;

#define MAX_COMM_MGR_TRX_TYPES  10
#define MAX_COMM_MGR_NUM_LINES  10
#define MAX_COMM_MGR_LINE_TYPES 10

#ifndef INCLUDE_LKC2AS_API

typedef struct
{
  DWORD frames_ok;                    // Total frames OK
  DWORD frames_error;                 // Total frames error
  
  DWORD error_crc;                    // Bad frames with crc error
  DWORD error_encryption;             // Bad frames with encryption error
  DWORD error_compression;            // Bad frames with compression error
  DWORD error_size;                   // Bad frames with data size error
  
  float compression_rate;             // Compression rate
  DWORD compression_samples;
  
} TYPE_LKC2AS_STATS_LAYER;

typedef struct
{
  TYPE_LKC2AS_STATS_LAYER stats_net;  // Network
  TYPE_LKC2AS_STATS_LAYER stats_lnk;  // Link
  TYPE_LKC2AS_STATS_LAYER stats_enc;  // Encoding
  
} TYPE_LKC2AS_STATS_DIRECTION;

typedef struct
{
  TYPE_LKC2AS_STATS_DIRECTION stats_input;
  TYPE_LKC2AS_STATS_DIRECTION stats_output;

} TYPE_LKC2AS_STATS;

#endif


typedef struct
{
  CONTROL_BLOCK       control_block;
  TYPE_LKC2AS_STATS   protocol_stats;

} TYPE_COMM_MGR_PROTOCOL_STATS;


#define MAX_TRP_LINKS 1000

typedef struct
{
  CONTROL_BLOCK   control_block;                    // Control Block
  DWORD           trp_num_agencies;
  DWORD           app_num_agencies;

} TYPE_COMM_MGR_LINK_STATS;

#ifndef INCLUDE_TRANSPORT_API

typedef struct
{
  DWORD frames_ok;                    // Total frames OK
  DWORD frames_error;                 // Total frames error
  
  DWORD error_fragmentation;          // Bad frames with fragmentation error
  DWORD error_size;                   // Bad frames with data size error
  DWORD error_address;                // Bad frames with target address error
  
} TYPE_TRP_STATS_DIRECTION;

typedef struct
{
  TYPE_TRP_STATS_DIRECTION stats_input;
  TYPE_TRP_STATS_DIRECTION stats_output;

} TYPE_TRP_STATS;
#endif


typedef struct
{
  CONTROL_BLOCK   control_block;
  DWORD           num_lines;        // #Lines supported by the CommMgr
  DWORD           line_index;       // Current line index, (0-based)
  DWORD           line_type;        // Current line type
  TYPE_TRP_STATS  trp_stats;        // Current line stats

} TYPE_COMM_MGR_TRANSPORT_STATS;


//------------------------------------------------------------------------------
//  PUBLIC FUNCTION PROTOTYPES 
//------------------------------------------------------------------------------

#endif // __COMMUNICATIONS_H
