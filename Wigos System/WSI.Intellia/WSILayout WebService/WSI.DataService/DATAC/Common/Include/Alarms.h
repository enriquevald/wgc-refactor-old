//------------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//------------------------------------------------------------------------------
//
//   MODULE NAME: Alarms.h
//   DESCRIPTION: Constants, types, variables definitions and prototypes
//                for Alarms
//        AUTHOR: Andreu Juli�
// CREATION DATE: 08-MAR-2002
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ -----------------------------------------------------------
// 08-MAR-2002 AJQ    Initial draft.
//
//------------------------------------------------------------------------------

#ifndef __ALARMS_H
#define __ALARMS_H

//------------------------------------------------------------------------------
//  INCLUDES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PUBLIC CONSTANTS
//------------------------------------------------------------------------------
// Alarm Manager Node Name
#define ALARM_MGR_NODE_NAME       _T("ALARM_MGR")

// Alarm Message Types
#define ALARM_MSG_ACTIVATION      1
#define ALARM_MSG_DEACTIVATION    2
#define ALARM_MSG_KEEP_ALIVE      3
#define ALARM_MSG_NOTIFY          4
#define ALARM_MSG_DEVICE_STATUS   5

// Alarm Priorities
#define ALARM_PRIORITY_CRITICAL   0
#define ALARM_PRIORITY_URGENT     1
#define ALARM_PRIORITY_SYSTEM     2
#define ALARM_PRIORITY_EVENT      3
#define ALARM_PRIORITY_OPERATIVE  4
#define ALARM_PRIORITY_AGENCY     5
#define ALARM_PRIORITY_DEVICE     6

// Alarm Client Ids
#define ALARM_NUM_CLIENT_IDS      3
// Maximum number of notified alarms
#define ALARM_MAX_NOTIFY          20
// Computer name len (alarm source)
#define ALARM_TERMINAL_NAME_LEN   30

// Type of Terminals, related to client_id5
#define TYPE_AGENCY_SERVER        0
#define TYPE_KIOSK                1

#define MAX_RECEIVED_ALARM_EVENTS  COMMON_KIOSK_MAX_DEVICES * COMMON_MAX_TERMINAL_LIST

//------------------------------------------------------------------------------
//  PUBLIC DATATYPES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PUBLIC DATA STRUCTURES
//------------------------------------------------------------------------------
// IPC Messages:
//   - Alarm Activation
//   - Alarm Dectivation
//   - Alarm Keep Alive, has no data
//   - Alarm Notify

#define ALARM_ID_NOT_SET 0

//   - Alarm Activation
typedef struct
{
  CONTROL_BLOCK     control_block;
  DWORD             client_id [ALARM_NUM_CLIENT_IDS];       // Client IDs
  DWORD             priority;                               // Alarm Priority
  DWORD             alarm_id;                               // Id.
  DWORD             nls_code;                               // The NLS
  TCHAR_NLS_PARAM   nls_param [NLS_MAX_PARAMS];             // The parameters
  TCHAR             computer [ALARM_TERMINAL_NAME_LEN + 1]; // Terminal Name

} TYPE_ALARM_ACTIVATION;

//   - Alarm Dectivation
typedef struct
{
  CONTROL_BLOCK control_block;
  DWORD         client_id [ALARM_NUM_CLIENT_IDS]; // Client IDs
  DWORD         priority;                 // Priority

} TYPE_ALARM_DEACTIVATION;

//   - Alarm Notify
typedef struct
{
  CONTROL_BLOCK     control_block;
  DWORD    alarm_count;                    // Number of alarms
  DWORD    alarm_list [ALARM_MAX_NOTIFY];  // Alarm IDs

} TYPE_ALARM_NOTIFY;

typedef struct
{
  DWORD           event_date;
  BYTE            terminal_type;
  DWORD           terminal_id;
  BYTE            device_code;
  WORD            device_status;
    
} TYPE_ALARM_DEVICE_STATUS_DATA;

typedef struct
{
  CONTROL_BLOCK                   control_block;
  DWORD                           agency_server_id;
  TCHAR                           computer [ALARM_TERMINAL_NAME_LEN + 1];
  BOOL                            whole_list;
  DWORD                           number_events;
  TYPE_ALARM_DEVICE_STATUS_DATA   event_data [1];
    
} TYPE_ALARM_DEVICE_STATUS;



//------------------------------------------------------------------------------
//  PUBLIC FUNCTION PROTOTYPES
//------------------------------------------------------------------------------


#endif // __ALARMS_H
