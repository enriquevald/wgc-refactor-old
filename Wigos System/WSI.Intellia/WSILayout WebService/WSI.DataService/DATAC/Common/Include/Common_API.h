//------------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//------------------------------------------------------------------------------
//
//   MODULE NAME: Common_API.h
//   DESCRIPTION: Constants, types, variables definitions and prototypes
//                for generic API's
//        AUTHOR: Andreu Juli�
// CREATION DATE: 12-JUL-2002
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ -----------------------------------------------------------
// 12-JUL-2002 AJQ    Initial draft.
//------------------------------------------------------------------------------

#ifndef __COMMON_API_H
#define __COMMON_API_H
//------------------------------------------------------------------------------
//  INCLUDES
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//  PUBLIC CONSTANTS
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//  PUBLIC DATATYPES
//------------------------------------------------------------------------------
typedef struct
{
  // API Parameters
  TCHAR * p_api_name;     // API Name
  WORD  max_iud_size;     // Maximum InputUserDataSize
  BOOL  uses_private_log; // Indicates whether or not use PrivateLog instead of Common_Logger

  // Requiered when uses_private_log = TRUE
  void  (* p_private_log)   (const WORD  MsgId,           // Message Id
                             const TCHAR * pModuleName,   // Module name
                             const TCHAR * pFunctionName, // Function name
                             const TCHAR * pParam3,       // Message's 1st. parameter
                             const TCHAR * pParam4,       // Message's 2nd. parameter
                             const TCHAR * pParam5,       // Message's 3rd. parameter
                             const TCHAR * pExtraMsg);    // Extra Message

  // Requiered when uses_private_log = FALSE
  void  (WINAPI * p_logger) (const WORD  MsgId,        // Nls Msg Id
                             const TCHAR * pParam1,    // Message's 1st. parameter
                             const TCHAR * pParam2,    // Message's 2nd. parameter
                             const TCHAR * pParam3,    // Message's 3rd. parameter
                             const TCHAR * pParam4,    // Message's 4th. parameter
                             const TCHAR * pParam5,    // Message's 5th. parameter
                             const TCHAR * pExtraMsg); // Extra Message
  // API Functions
  BOOL         (* p_check_mode)   (const API_INPUT_PARAMS *pApiInputParams);
  QUEUE_HANDLE (* p_select_queue) (const API_INPUT_PARAMS *pApiInputParams);
  void         (* p_try_init)     (void); // Optional

  // API Status
  BOOL  init;        // API Initialized
  BOOL  init_tried;  // API Init Tried
  BOOL  stopped;     // API Stopped

} TYPE_COMMON_API_DATA;

//------------------------------------------------------------------------------
//  PUBLIC DATA STRUCTURES
//------------------------------------------------------------------------------
extern TYPE_COMMON_API_DATA GLB_API; // To be defined in your API

//------------------------------------------------------------------------------
//  PUBLIC FUNCTION PROTOTYPES
//------------------------------------------------------------------------------
void Common_API_Init (BOOL InitStatus);

void Common_API_Stop (void);

BOOL Common_API_CheckControlBlock  (DWORD ReceivedCB,
                                    DWORD RequestedCB,
                                    TCHAR * pFunctionName,
                                    TCHAR * pStructName);

BOOL Common_API_CheckStatus (TCHAR * pFunctionName);

void Common_API_CloseEvent  (HANDLE EventObject);

WORD Common_API_Entry (API_INPUT_PARAMS     * pApiInputParams,
                       API_INPUT_USER_DATA  * pApiInputUserData,
                       API_OUTPUT_PARAMS    * pApiOutputParams,
                       API_OUTPUT_USER_DATA * pApiOutputUserData,
                       TCHAR                * pFunctionName);

void Common_API_NotifyRequestStatus (WORD RequestOutput1,
                                     WORD RequestOutput2,
                                     API_INPUT_PARAMS  * pApiInputParams,
                                     API_OUTPUT_PARAMS * pApiOutputParams);

BOOL Common_API_StartThread (LPTHREAD_START_ROUTINE pThreadRoutine,
                             VOID                   * pThreadParameter,
                             HANDLE                 StartNotifyEvent,
                             TCHAR                  * pThreadName);

#endif // __COMMON_API_H
