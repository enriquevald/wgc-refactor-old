//-----------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//-----------------------------------------------------------------------------
//
//   MODULE NAME: Queue_API.h
//
//   DESCRIPTION: Constants, types, variables definitions and prototypes for Queue_API.CPP
//
//        AUTHOR: Toni Jord�
//
// CREATION DATE: 15-MAR-2002
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 15-MAR-2002 TJG    First release.
//
//------------------------------------------------------------------------------

#ifndef __QUEUE_API_H
#define __QUEUE_API_H

//------------------------------------------------------------------------------
// INCLUDES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PUBLIC CONSTANTS
//------------------------------------------------------------------------------

#ifdef QUEUE_API_EXPORTS
#define QUEUE_API __declspec(dllexport)
#else
#define QUEUE_API __declspec(dllimport)
#endif

#define QUEUE_STATUS_OK               0
#define QUEUE_STATUS_ERROR            1
#define QUEUE_STATUS_NOT_INITIALIZED  2
#define QUEUE_STATUS_TOO_BIG          3
#define QUEUE_STATUS_FULL             4
#define QUEUE_STATUS_PENDING_READ     5
#define QUEUE_STATUS_PARAM_ERROR      6

#define QUEUE_MODE_BLOCK              0
#define QUEUE_MODE_NO_WAIT            1

#define QUEUE_NULL_HANDLE             -1

#define Queue_CreateQueue(QueueSize, Handle) Queue_CreateQueueEx(QueueSize,Handle,_T(__FILE__), __LINE__)

//------------------------------------------------------------------------------
// PUBLIC DATATYPES
//------------------------------------------------------------------------------

typedef WORD QUEUE_HANDLE;

//------------------------------------------------------------------------------
// PUBLIC DATA STRUCTURES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PUBLIC FUNCTION PROTOTYPES
//------------------------------------------------------------------------------

QUEUE_API WORD WINAPI   Queue_CreateQueueEx (DWORD UserQueueSize,
                                             QUEUE_HANDLE * QHandle,
                                             TCHAR        * pFilename = NULL,
                                             DWORD        LineNumber  = 0);

QUEUE_API WORD WINAPI   Queue_Read (QUEUE_HANDLE    QHandle, 
                                    HANDLE          EventObject,
                                    VOID            * pUserAPCRoutine,
                                    VOID            * pUserAPCData,
                                    DWORD           UserBufferSize, 
                                    void            * pUserBuffer,
                                    DWORD           * pActualDataLength,
                                    DWORD           * pUserReadHandle);

QUEUE_API WORD WINAPI   Queue_Write (QUEUE_HANDLE   QHandle, 
                                     void           * pUserBuffer, 
                                     DWORD          UserBufferLength,
                                     WORD           QueueMode);

QUEUE_API WORD WINAPI   Queue_Purge (QUEUE_HANDLE   QHandle);

QUEUE_API WORD WINAPI   Queue_CancelRead (QUEUE_HANDLE   QHandle, 
                                          DWORD          ReadHandle);

QUEUE_API WORD WINAPI   Queue_GetNumItems (QUEUE_HANDLE   QHandle, 
                                           DWORD          * pNumItems);

#endif // __QUEUE_API_H
