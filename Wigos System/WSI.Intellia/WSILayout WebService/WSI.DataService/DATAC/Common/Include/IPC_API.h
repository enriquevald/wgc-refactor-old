//------------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//------------------------------------------------------------------------------
//
//   MODULE NAME: IPC_API.h
//   DESCRIPTION: Constants, types, variables definitions and prototypes
//                for the IPC_API
//        AUTHOR: Andreu Juli�
// CREATION DATE: 12-MAR-2002
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ -----------------------------------------------------------
// 12-MAR-2002 AJQ    Initial draft.
// 09-DEC-2002 AJQ    New function: IPC_CODE_GET_NODE_INST_ID_LIST
//------------------------------------------------------------------------------

#ifndef __IPC_API_H
#define __IPC_API_H

//------------------------------------------------------------------------------
//  INCLUDES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PUBLIC CONSTANTS
//------------------------------------------------------------------------------

#ifdef IPC_API_EXPORTS
#define IPC_API __declspec(dllexport)
#else
#define IPC_API __declspec(dllimport)
#endif

// Module function codes
#define IPC_CODE_MODULE_INIT            1
#define IPC_CODE_INIT_LOCAL_NODE        2
#define IPC_CODE_PURGE_NODE             3
#define IPC_CODE_GET_NODE_INST_ID       4
#define IPC_CODE_SEND                   5
#define IPC_CODE_RECEIVE                6
#define IPC_CODE_WAIT_SEND_QUEUE_EMPTY  7
#define IPC_CODE_MODULE_STOP            8
#define IPC_CODE_GET_NODE_INST_ID_LIST  9
#define IPC_CODE_GET_TUPLES             10
#define IPC_CODE_GET_INST_ID_DATA       11

// Module return codes
#define IPC_STATUS_OK                   0 // API execution OK.
#define IPC_STATUS_ERROR                1 // General execution error.
#define IPC_STATUS_NODE_NAME_NOT_FOUND  2 // Node name has not been found in the network configuration.
#define IPC_STATUS_INVALID_NODE_INST_ID 3 // Node Id used is not a valid node identifier.
#define IPC_STATUS_TIMEOUT              4 // Specified timeout time has been elapsed.
#define IPC_STATUS_PREV_REQ_PENDING     5 // Previous request not finished.
#define IPC_STATUS_NODE_NOT_INIT        6 // Node not initialized. Cannot be used.
#define IPC_STATUS_NODE_NOT_LOCAL       7 // Receive operation not work in non local nodes
#define IPC_STATUS_OVRFLW_RCV_NODE      8 // Too many receive operations pending on the same local node

// Module constants
#define IPC_NODE_NAME_LENGTH            31   // 30 + end of string
#define IPC_USER_BUFFER_SIZE            5120 // AJQ 18-MAR-2004, The user buffer size must be large enough to hold an AppFrame (Protocol LKC-LKAS) 4096 // Maximum user buffer length
#define IPC_TIMEOUT_INFINITE            0xFFFFFFFF  // No timeout
#define IPC_NODE_UNKNOWN                0xFFFFFFFF  // Used for unknown source nodes in "send" requests
#define IPC_MAX_INSTANCES_PER_NODE      100 // AJQ Increased to 100 = #CPU's      28          // The maximum number of instances per node

#define MAX_IPC_TUPLES                  (150 * IPC_MAX_INSTANCES_PER_NODE)  // 150 is IPC_MAX_NODES/2



// AJQ 03-JUL-2005, IPC MsgCmd Ranges
#define IPC_MSG_CMD_COMM_MGR(n)  ( 10000+(n))
#define IPC_MSG_CMD_APP_LOG(n)   ( 20000+(n))
#define IPC_MSG_CMD_ALARM_MGR(n) ( 30000+(n))
#define IPC_MSG_CMD_GUI(n)       ( 40000+(n))
#define IPC_MSG_CMD_TRX_MGR(t,n) (1000000+(t)*10000)+(n))



//------------------------------------------------------------------------------
//  PUBLIC DATATYPES
//------------------------------------------------------------------------------

// NODE INIT
typedef struct
{
  CONTROL_BLOCK  control_block;
  TCHAR          node_name [IPC_NODE_NAME_LENGTH];
  DWORD          input_buffer_size;

} IPC_IUD_INIT_LOCAL_NODE;

typedef struct
{
  CONTROL_BLOCK  control_block;
  DWORD          node_inst_id;

} IPC_OUD_INIT_LOCAL_NODE;

typedef struct
{
  CONTROL_BLOCK  control_block;
  DWORD          node_inst_id;  // Node to be purged

} IPC_IUD_PURGE_NODE;

typedef struct
{
  CONTROL_BLOCK  control_block;
  TCHAR          node_name [IPC_NODE_NAME_LENGTH];
  BOOL           local_instance;

} IPC_IUD_GET_NODE_INST_ID;

typedef struct
{
  CONTROL_BLOCK  control_block;
  DWORD          node_inst_id;

} IPC_OUD_GET_NODE_INST_ID;

typedef struct
{
  CONTROL_BLOCK  control_block;
  TCHAR          node_name [IPC_NODE_NAME_LENGTH];

} IPC_IUD_GET_NODE_INST_ID_LIST;

typedef struct
{
  CONTROL_BLOCK   control_block;
  DWORD           num_instances;                                // Number of instances
  DWORD           cfg_inst_id    [IPC_MAX_INSTANCES_PER_NODE];  // The Configuration Id of this instance
  DWORD           node_inst_id   [IPC_MAX_INSTANCES_PER_NODE];  // The node instance id
  BOOL            local_instance [IPC_MAX_INSTANCES_PER_NODE];  // Flag indicating whether or not is the local instance

} IPC_OUD_GET_NODE_INST_ID_LIST;

typedef struct
{
  CONTROL_BLOCK  control_block;
  DWORD          node_inst_id;

} IPC_IUD_GET_INST_ID_DATA;

typedef struct
{
  CONTROL_BLOCK  control_block;
  WORD           ipc_computer_id;
  WORD           ipc_node_id;

} IPC_OUD_GET_INST_ID_DATA;

typedef struct
{
  CONTROL_BLOCK         control_block;        // Control Block
  DWORD                 target_node_inst_id;  // Destination
  DWORD                 source_node_inst_id;  // Source node, can be used in destination to send the answer
  DWORD                 msg_cmd;              // Application command
  DWORD                 msg_status;           // Application status. Optional
  DWORD                 msg_id;               // Application message id. Optional
  DWORD                 user_dword;           // User field (dword). Optional  
  DWORD                 user_buffer_length;   // Actual user_buffer length, can be less that maximum size
  BYTE                  user_buffer [IPC_USER_BUFFER_SIZE]; // User buffer

} IPC_IUD_SEND;

typedef struct
{
  CONTROL_BLOCK   control_block; // Control Block
  DWORD           node_inst_id;  // Node used to receive message
  DWORD           timeout;       // Reception timeout, in msec. (No timeout = IPC_TIMEOUT_INFINITE)

} IPC_IUD_RECEIVE;

typedef struct
{
  CONTROL_BLOCK         control_block;        // Control Block
  DWORD                 source_node_inst_id;  // Message Source node
  DWORD                 msg_cmd;              // Application command
  DWORD                 msg_status;           // Application status. Optional
  DWORD                 msg_id;               // Application message id. Optional
  DWORD                 user_dword;           // User field (dword). Optional  
  DWORD                 user_buffer_length;   // Actual user_buffer length
  BYTE                  user_buffer [IPC_USER_BUFFER_SIZE]; // User buffer

} IPC_OUD_RECEIVE;

typedef struct
{
  DWORD node_def_id;
  DWORD computer_id;
  DWORD node_inst_id;
  DWORD wdog_inst_id;

} TYPE_IPC_TUPLE;

typedef struct
{
  CONTROL_BLOCK   control_block;
  DWORD           num_tuples;
  TYPE_IPC_TUPLE  tuple [MAX_IPC_TUPLES];

} IPC_OUD_GET_TUPLES;

//------------------------------------------------------------------------------
//  PUBLIC DATA STRUCTURES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PUBLIC FUNCTION PROTOTYPES
//------------------------------------------------------------------------------

IPC_API WORD WINAPI Ipc_API (API_INPUT_PARAMS     *pApiInputParams,
                             API_INPUT_USER_DATA  *pApiInputUserData,
                             API_OUTPUT_PARAMS    *pApiOutputParams,
                             API_OUTPUT_USER_DATA *pApiOutputUserData);

IPC_API BOOL WINAPI IPC_WriteConfigFile (TCHAR * pFilename);

#endif // __IPC_API_H