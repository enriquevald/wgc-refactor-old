//------------------------------------------------------------------------------
// Copyright � 2002 Win Systems
//------------------------------------------------------------------------------
//
//   MODULE NAME: PRIVATELOG.CPP
//
//   DESCRIPTION: Functions and Methods for PrivateLog function. Note that IPC
//                and NLS must use private log instead of Lykos Logger functionality .
//
//        AUTHOR: Alberto Cuesta 
//
// CREATION DATE: 27-MAR-2002
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 27-MAR-2002 ACC    Initial release
// 04-APR-2002 TJG    Only one log file for all. API encapsulates file name and messages.
//                    Therefore messages are referenced with its ID.
// 01-AUG-2002 APB    Added message number 9.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// INCLUDES
//------------------------------------------------------------------------------
#define INCLUDE_PRIVATE_LOG

#include "CommonDef.h"

//------------------------------------------------------------------------------
// PRIVATE CONSTANTS
//------------------------------------------------------------------------------

// Macro to calculate TCHAR string size
#define GetStringSize(_str)             (sizeof _str / sizeof _str[0])

#define PRIVATELOG_FILE_NAME            _T("LKC Private.Log")

#define PRIVATELOG_MSG_ENTRIES          11//10
#define PRIVATELOG_MAX_SIZE_DATE        100
#define PRIVATELOG_SIZE_TIME            12 
#define PRIVATELOG_SIZE_MSG             (4000/sizeof(TCHAR))

#define COMMON_MAX_COMPUTER_NAME        255

//------------------------------------------------------------------------------
// PRIVATE DATATYPES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE DATA STRUCTURES
//------------------------------------------------------------------------------

TCHAR   GLB_LogMsgs [PRIVATELOG_MSG_ENTRIES][PRIVATELOG_SIZE_MSG + 1] = 
{
  _T("Private log message id=%d not found. [%.*s][%.*s][%.*s]\n"),
  _T("Error=%.*s calling %.*s.\n"),
  _T("Error=%.*s. Requested=%.*s. Received=%.*s.\n"),
  _T("NLS message id=%.*s not found.\n"),
  _T("%.*s%.*s%.*s.\n"),
  _T("Unexpected value %.*s=%.*s.\n"),
  _T("Configuration error line=%.*s.\n"),
  _T("Error=%.*s calling %.*s: %.*s.\n"),
  _T("Invalid LK System license.\n"),
  _T("%.*s%.*s%.*s\n"),
  _T("Entry %.*s in file %.*s. not found or invalid value.\n")
};

//------------------------------------------------------------------------------
// PRIVATE FUNCTION PROTOTYPES
//------------------------------------------------------------------------------
//BOOL        GetDateTimeString (TCHAR  * pDateTimeString);

//------------------------------------------------------------------------------
// PUBLIC FUNCTIONS IMPLEMENTATION
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE: Writes text to private log file.Log entries follow this format:
//                                                            Message
//                                                   --------------------------
// Date-Time              Module     Function        Id    Text 
// |                      |          |               |     |
// dd/mm/yyyy hh:mm:ss.cc mmmmmmmmmm fffffffffffffff xxxxx xxxxxxxxxxxxx .. xxx
// 0                      23         34              50    56                 256
//
//  Note: the date size is not fixed, it depends of the settings.
//
//  PARAMS:
//      - INPUT:
//          - MsgId:    Message id 
//          - pParam1:  First parameter (Module name)
//          - pParam2:  Second parameter (Function name)
//          - pParam3:  Third parameter (Message text specific)
//          - pParam4:  Fourth parameter (Message text specific)
//          - pParam5:  Fifth parameter (Message text specific)
//          - pExtraMsg:The extra message to be displayed
//
//      - OUTPUT: None
//
// RETURNS: None
//
//   NOTES: None

void        PrivateLog (const WORD  MsgId, 
                        const TCHAR * pParam1, 
                        const TCHAR * pParam2, 
                        const TCHAR * pParam3, 
                        const TCHAR * pParam4, 
                        const TCHAR * pParam5,
                        const TCHAR * pExtraMsg)
{
  FILE      * p_file;
  WORD      _msg_id;
  TCHAR     _log_text    [PRIVATELOG_SIZE_MSG + 1];
  TCHAR     _log_msg     [PRIVATELOG_SIZE_MSG + 1];
  TCHAR     _log_message [PRIVATELOG_SIZE_MSG + 1];

  // Open file for read information.
  p_file = _tfopen (PRIVATELOG_FILE_NAME, _T("a"));
  if ( p_file == NULL )
  {
    return;
  }

  // Check message id range
  if ( MsgId < 2 || MsgId > PRIVATELOG_MSG_ENTRIES )
  {
    // Private log message not found
    _msg_id = 1;
  }
  else
  {
    _msg_id = MsgId;
  }

  //    - Message text
  if ( _msg_id == 1 )
  {
    // Private log message not found
    _stprintf (_log_text, GLB_LogMsgs[_msg_id - 1], MsgId,
                                                    PRIVATELOG_SIZE_PARAM, pParam3,
                                                    PRIVATELOG_SIZE_PARAM, pParam4,
                                                    PRIVATELOG_SIZE_PARAM, pParam5);
  }
  else
  {
    _stprintf (_log_text, GLB_LogMsgs[_msg_id - 1], PRIVATELOG_SIZE_PARAM, pParam3,
                                                    PRIVATELOG_SIZE_PARAM, pParam4,
                                                    PRIVATELOG_SIZE_PARAM, pParam5);
  }

  LogMsgText (NULL, pParam1, pParam2, _msg_id, _log_text, pExtraMsg, PRIVATELOG_SIZE_MSG, _log_msg);

  LogMsgPrefixDateTime (_log_msg, 
                        PRIVATELOG_SIZE_MSG, 
                        _log_message);

  // Write message to file
  _fputts (_log_message, p_file);

  // Close file
  fclose (p_file);

  return;
}

//------------------------------------------------------------------------------
// PRIVATE FUNCTIONS IMPLEMENTATION
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
// PURPOSE: 
//
//  PARAMS:
//      - INPUT:
//
//      - OUTPUT: 
//          - pDateTimeString : Formatted date-time string
//
// RETURNS: None
//
//   NOTES: None

static BOOL     GetDateTimeString (TCHAR  * pDateTimeString)
{
  SYSTEMTIME  _curr_time;
  TCHAR       _date_string [PRIVATELOG_MAX_SIZE_DATE + 1];      // Add terminating NULL char

  // Get current time from system
  GetLocalTime (&_curr_time);

  if ( GetDateFormat (LOCALE_USER_DEFAULT,              // Locale
                      DATE_SHORTDATE,                   // Options
                      &_curr_time,                      // Date
                      NULL,                             // Date format
                      _date_string,                     // Formatted string
                      PRIVATELOG_MAX_SIZE_DATE) == 0 )  // Size of formatted string
  {
    pDateTimeString[0] = '\0';

    return TRUE; 
  }

  if ( PRIVATELOG_MAX_SIZE_DATE - _tcslen (_date_string) < PRIVATELOG_SIZE_TIME )
  {
    pDateTimeString[0] = '\0';

    return TRUE;
  }

  _stprintf (pDateTimeString, _T("%s %02hu:%02hu:%02hu.%02hu"), _date_string,
                                                                _curr_time.wHour,
                                                                _curr_time.wMinute,
                                                                _curr_time.wSecond,
                                                                (WORD) (_curr_time.wMilliseconds / 10));

  return TRUE;

} // GetDateTimeString

//------------------------------------------------------------------------------
// PURPOSE: Builds a log message
// 
//  PARAMS:
//      - INPUT:
//        - pComputerName: The ComputerName, if it us NULL then the current cpu name is used
//        - pModuleName: 
//        - pFunctionName:
//        - MsgId:
//        - pMessage:       The message itself (NLS, PrivateLog)
//        - pExtraMessage:  Any exta string (as Oracle Errors)
//        - BufferSize:     The ouput buffer size
//
//      - OUTPUT:
//        - pLogMsg:        The ouput built message
// 
// RETURNS: The length of the ouput message.
// 
//   NOTES: The output buffer must be greater than LOG_MESSAGE_HEADER_SIZE
//
DWORD       LogMsgText (const TCHAR   * pComputerName,
                        const TCHAR   * pModuleName,
                        const TCHAR   * pFunctionName,
                        const WORD    MsgId,
                        const TCHAR   * pMessage,
                        const TCHAR   * pExtraMessage,
                        const DWORD   BufferSize,
                        TCHAR         * pLogMsg)
{
  TCHAR               _function_name [] = _T("LogMsgText");
  TCHAR               _blank []  = _T(" ");
  DWORD               _blank_len = _tcslen (_blank);
  DWORD               _append;
  DWORD               _max_len;
  DWORD               _complete_length;
  TCHAR               _computer_name [MAX_COMPUTERNAME_LENGTH + 1];
  TCHAR               _full_computer_name [COMMON_MAX_COMPUTER_NAME];
  const TCHAR         * p_cpu_name;

  if ( BufferSize < LOG_MESSAGE_HEADER_SIZE )
  {
    return 0;
  }

  if ( pComputerName == NULL )
  {
    // Don't use the Common_ComputerName 
    _complete_length = COMMON_MAX_COMPUTER_NAME;
    memset (_full_computer_name, 0, sizeof(_full_computer_name));

    if ( GetComputerName (_full_computer_name, &_complete_length) == FALSE )
    {
      // Failed
      _tcscpy (_computer_name, _T("UNKNOWN"));
    }
    else
    {
      _tcsncpy( _computer_name, _full_computer_name, MAX_COMPUTERNAME_LENGTH);
    }
    
    p_cpu_name = _computer_name;
  }
  else
  {
    p_cpu_name = pComputerName;
  }

  // 0         1         2         3         4         5    5    6
  // 0----5----0----5----0----5----0----5----0----5----0----5----0 �������
  // CCCCCCCCCCCCC MMMMMMMMMMMMMM FFFFFFFFFFFFFFFFF IIIII MESSAGE EXTRA
  _stprintf (pLogMsg, _T("%-13.13s %-14.14s %-17.17s %05d"), p_cpu_name,
                                                             pModuleName,
                                                             pFunctionName,
                                                             MsgId);

  // Max Length
  _max_len = BufferSize - 1 - _tcslen (pLogMsg);

  if ( pMessage != NULL )
  {
    _append  = _tcslen (pMessage);
    if ( _append > _max_len )
    {
      _append = _max_len;    
    }

    if ( _append > 0 )
    {
      // Copy the pMessage
      _tcsncat (pLogMsg, _blank, _blank_len);
      _tcsncat (pLogMsg, pMessage, _append);

      // Max Length
      _max_len = BufferSize - 1 - _tcslen (pLogMsg);
    }
  }

  if ( pExtraMessage != NULL )
  {
    _append  = _tcslen (pExtraMessage);
    if ( _append > _max_len )
    {
      _append = _max_len;    
    }
    
    if ( _append > 0 )
    {
      // Copy the pExtraMessage
      _tcsncat (pLogMsg, _blank, _blank_len);
      _tcsncat (pLogMsg, pExtraMessage, _append);
    }
  }

  return _tcslen (pLogMsg);
} // LogMsgText 

//------------------------------------------------------------------------------
// PURPOSE: Prefixes the input message with the current date and time.
// 
//  PARAMS:
//      - INPUT:
//        - pMessage: The original message
//
//      - OUTPUT:
//        - pLogMesg: The original message prefixed with date & time
// 
// RETURNS: Nothing
// 
//   NOTES:
//
DWORD       LogMsgPrefixDateTime (const TCHAR  * pMessage,
                                  const DWORD  BufferSize,
                                  TCHAR        * pLogMsg)
{
  DWORD     _max_len;
  TCHAR     _blank []  = _T(" ");
  DWORD     _blank_len = _tcslen (_blank);
  DWORD     _append;

  GetDateTimeString (pLogMsg);

  // 0         1         2         3         4         5         6         7         8
  // 0----5----0----5----0----5----0----5----0----5----0----5----0----5----0----5----0 ...
  // DD/MM/YYYY HH:MM:SS.CC CCCCCCCCCCCC MMMMMMMMMMMM FFFFFFFFFFFFFFFFFF IIIII MESSAGE ...
  // Max Length
  // Max Length
  _max_len = BufferSize - 1 - _tcslen (pLogMsg);

  if ( pMessage != NULL )
  {
    _append  = _tcslen (pMessage);
    if ( _append > _max_len )
    {
      _append = _max_len;    
    }
    
    if ( _append > 0 )
    {
      // Copy the pMessage
      _tcsncat (pLogMsg, _blank, _blank_len);
      _tcsncat (pLogMsg, pMessage, _append);
    }
  }

  return _tcslen (pLogMsg);
} // LogMsgPrefixDateTime



