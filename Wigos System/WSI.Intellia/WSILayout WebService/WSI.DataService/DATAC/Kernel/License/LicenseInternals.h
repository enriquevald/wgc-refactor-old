//------------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//------------------------------------------------------------------------------
//
//   MODULE NAME: LicenseInternals.h
//
//   DESCRIPTION: Constant, type and function prototype definitions.
//
//        AUTHOR: Carles Iglesias
//
// CREATION DATE: 01-AUG-2002
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ -----------------------------------------------------------
// 01-AUG-2002 CIR    Initial draft.
//-------------------------------------------------------------------------------

#ifndef __LKLICENSEINTERNALS_H
#define __LKLICENSEINTERNALS_H

//------------------------------------------------------------------------------
// PUBLIC CONSTANTS
//------------------------------------------------------------------------------

// Encryption constants

#define LKL_DES_ENCRYPT            1
#define LKL_DES_DECRYPT            0

#define LKL_ENCRYPT_DES_SINGLE  0
#define LKL_ENCRYPT_DES_TRIPLE  1

#define LKL_ENCRYPTION_KEY      _T("165425873912478945215679")

#define LICENSE_FILE_NAME       _T("License.wg")

//------------------------------------------------------------------------------
// PUBLIC DATATYPES
//------------------------------------------------------------------------------

typedef struct
{
  TYPE_LICENSE      license;
  BYTE              filler [4]; // 4 bytes to fill because TYPE_LICENSE_RECORD 
                                // size must be a multiple of 8 (CRC purposes)
  DWORD             crc;        // CRC 32 calculated using the data file

} TYPE_LICENSE_RECORD;

//------------------------------------------------------------------------------
// PUBLIC DATA STRUCTURES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PUBLIC FUNCTION PROTOTYPES
//------------------------------------------------------------------------------
BOOL        CheckLicenseStructSize ();
/*
// Encryption
BOOL Version_Encrypt (BYTE  * pBuffIn, 
                      int   BuffInSize, 
                      BYTE  * pBuffOut, 
                      int   BuffOutSize,
                      BYTE  * pKey,    
                      int   Action,
                      int   DesType);

// Crc 32
DWORD Version_Crc32_ComputeCRC (BYTE  * pBuffer, 
                                DWORD BufferSize);
*/
#endif // __LKLICENSEINTERNALS_H
