//------------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: CommonMiscInternals.h
//   DESCRIPTION: Constants, types, variables definitions and prototypes internals
//                for CommonMisc
//        AUTHOR: Alberto Cuesta
// CREATION DATE: 08-MAR-2002
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ -----------------------------------------------------------
// 08-MAR-2002 ACC    Initial draft.
// 
//-------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  INCLUDES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PUBLIC CONSTANTS 
//------------------------------------------------------------------------------
// Module name
#define MODULE_NAME                             _T("COMMONMISC")

// Configuration constants
#define COMMON_CONF_FILE_NAME                   _T("LKSYSTEM.CFG")
#define COMMON_CONF_ENTRY_REGISTRY              _T("REGISTRY=")
#define COMMON_CONF_ENTRY_REGISTRY_PATH_SIZE    128


//#define COMMON_CONFIGURATION_KEY                _T("Configuration")
//#define COMMON_DATABASE_KEY                     _T("Database")
//#define COMMON_DB_SUBKEY_ODBC                   _T("Odbc")
//#define COMMON_DB_SUBKEY_USER                   _T("User")
//#define COMMON_DB_SUBKEY_PASSWORD               _T("Password")
//#define COMMON_DB_SUBKEY_CONNECT_STRING         _T("ConnectString")
//#define COMMON_CONFIG_SUBKEY_LANGUAGE           _T("Language")
//#define COMMON_CONFIG_SUBKEY_LOG_PATH           _T("LogPath")

#define MASK_STRING                             "SZDREEIJDGHTLADP8376"

// Char constants
#define COMMON_CR_CHAR                          13
#define COMMON_LF_CHAR                          10
#define COMMON_SEMICOLON_CHAR                   ';'
#define COMMON_SPACE_CHAR                       ' '
#define COMMON_BACKSLASH_CHAR                   '\\'
#define COMMON_NULL_CHAR                        '\0'

// Tree levels in HIERARCHY_INSTANCES table
#define HI_TREE_LEVEL_GESTOR         0 
#define HI_TREE_LEVEL_OPERATOR       1
#define HI_TREE_LEVEL_DISTRIBUTOR    2
#define HI_TREE_LEVEL_AGENCY         3


//------------------------------------------------------------------------------
//  PUBLIC DATATYPES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PUBLIC DATA STRUCTURES 
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
//  PUBLIC FUNCTION PROTOTYPES 
//------------------------------------------------------------------------------

void  EncryptDecryptPassword (unsigned char *pInBuffer,
                              unsigned char *pOutBuffer,
                              int SizeOfBuffers);
