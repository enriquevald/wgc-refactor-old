//------------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : CommonMiscVersion.cpp
// 
//   DESCRIPTION : Functions and Methods to Check/Get the Version of a Module
// 
//        AUTHOR : Andreu Juli�
// 
// CREATION DATE : 30-JUL-2002
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ -----------------------------------------------------------
// 30-JUL-2002 AJQ    Initial draft.
// 
//-------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  INCLUDES 
//------------------------------------------------------------------------------

#define INCLUDE_NLS_API
#define INCLUDE_COMMON_MISC
#define INCLUDE_VERSION_CONTROL
#include "CommonDef.h"

#include "CommonMiscInternals.h"

//------------------------------------------------------------------------------
// PRIVATE CONSTANTS 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE DATATYPES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE DATA STRUCTURES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE FUNCTION PROTOTYPES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PUBLIC FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE : Checks the version of given module. It also retrieve module about 
//          information as virtual module version string, company name and
//          legal copyright string
// 
//  PARAMS :
//      - INPUT :
//        - pModuleName
//
//      - OUTPUT : 
//        - pModuleAbout: Information about module version:
//                          - Module version
//                          - Company name
//                          - Legal copyright
// 
// RETURNS : 
//
//   NOTES :

COMMONMISC_API DWORD WINAPI Common_VersionGetModuleVersion (TCHAR             * pModuleName, 
                                                            TYPE_MODULE_ABOUT * pModuleAbout)
{
  return CommonBase_VersionGetModuleVersion (pModuleName, pModuleAbout);

} // Common_VersionGetModuleVersion

//------------------------------------------------------------------------------
// PURPOSE : Checks the version of given module. It also retrieve module about 
//          information as virtual module version string, company name and
//          legal copyright string
// 
//  PARAMS :
//      - INPUT :
//        - pModuleName
//
//      - OUTPUT : 
//        - pModuleAbout: Information about module version:
//                          - Module version
//                          - Company name
//                          - Legal copyright
// 
// RETURNS : 
//
//   NOTES :

COMMONMISC_API DWORD WINAPI Common_VersionCheckModuleVersion (TCHAR             * pModuleName, 
                                                              TYPE_MODULE_ABOUT * pModuleAbout)
{
  return CommonBase_VersionCheckModuleVersion (pModuleName, pModuleAbout);

} // Common_VersionCheckModuleVersion

//------------------------------------------------------------------------------
// PURPOSE : Get center configuration 
//
//  PARAMS :
//      - INPUT :
//
//      - OUTPUT : 
//          - pSystemLanguage : System language value
//          - pDBUser : Database user
//          - pDBPassword : Database user password
//          - pConnectString : Database connect string
//
// RETURNS :
//      - COMMON_OK
//      - COMMON_ERROR
//      - COMMON_ERROR_DB
//
//   NOTES :

COMMONMISC_API WORD WINAPI Common_GetCenterConfiguration (TCHAR * pDBUser,
                                                          TCHAR * pDBPassword,
                                                          TCHAR * pConnectString)
{
  TCHAR         _log_param_1 [COMMON_LOGGER_PARAMETER_SIZE + 1];
  DWORD         _rc;
  TYPE_CENTER_CONFIGURATION_EX  _center_configuration_ex;

  memset (&_center_configuration_ex, 0, sizeof (TYPE_CENTER_CONFIGURATION_EX));

  // Set control block size
  _center_configuration_ex.control_block = sizeof (TYPE_CENTER_CONFIGURATION_EX);

  //Get center configuration from registry
  _rc = Common_GetConfigurationEx (&_center_configuration_ex);
  if ( _rc != COMMON_OK )
  {
    // Could not get center configuration
    // Write Logger
    _stprintf (_log_param_1, _T("%lu"), _rc);
    Common_Logger (NLS_ID_LOGGER(2), _log_param_1, _T("Common_GetConfigurationEx"), NULL);

    if ( _rc == COMMON_ERROR_DB )
    {
      return COMMON_ERROR_DB;
    }

    return COMMON_ERROR;
  }

  // Assign different values to output parameters
  _tcscpy (pDBUser, _center_configuration_ex.database.user);
  _tcscpy (pDBPassword, _center_configuration_ex.database.password);
  _tcscpy (pConnectString, _center_configuration_ex.database.connect_string);

  return COMMON_OK;
} // Common_GetCenterConfiguration
