//-----------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//-----------------------------------------------------------------------------
// 
//   MODULE NAME: CommonMiscLogger.cpp
// 
//   DESCRIPTION: Functions and Methods of the logger to handle CommonMiscLogger dll
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 08-MAR-2002
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 08-MAR-2002 ACC    Initial draft.
// 26-APR-2002 TJG    Log using IPC is commented out until System Log works 
//                    properly. Use PrivateLog instead (remember that PrivateLog.lib 
//                    has to be removed also from project)
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  INCLUDES 
//------------------------------------------------------------------------------

#define INCLUDE_COMMON_MISC
#define INCLUDE_NLS_API
#define INCLUDE_IPC_API
#define INCLUDE_APPLICATION_LOG
#define INCLUDE_PRIVATE_LOG
#include "CommonDef.h"

#include "CommonMiscInternals.h"

//------------------------------------------------------------------------------
//  PRIVATE CONSTANTS 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PRIVATE DATATYPES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PRIVATE DATA STRUCTURES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PRIVATE FUNCTION PROTOTYPES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PUBLIC FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE: write logger message in log file.
// 
//  PARAMS:
//      - INPUT:
//          - MsgId: Message to write
//          - pParam1: parameter 1 for log string.
//          - pParam2: parameter 2 for log string.
//          - pParam3: parameter 3 for log string.
//          - pParam4: parameter 4 for log string.
//          - pParam5: parameter 5 for log string.
//
//      - OUTPUT: None
// 
// RETURNS:
//      - COMMON_OK
//      - COMMON_ERROR
// 
//   NOTES:

COMMONMISC_API void WINAPI Common_LoggerMsg (WORD          MsgId,    
                                             const TCHAR   * pParam1, 
                                             const TCHAR   * pParam2, 
                                             const TCHAR   * pParam3, 
                                             const TCHAR   * pParam4, 
                                             const TCHAR   * pParam5,
                                             const TCHAR   * pExtraMsg)
{
  TCHAR               _function_name [] = _T("Common_LoggerMsg");
  static BOOL         _first_time = TRUE; 
  static DWORD        _ipc_target_node_id;  
  WORD                _rc;
  //TCHAR               _rc_str [NLS_MSG_PARAM_MAX_LENGTH];
  //API_INPUT_PARAMS    _api_input_params;
  //IPC_IUD_SEND        _ipc_iud_send;
  //API_OUTPUT_PARAMS   _api_output_params;
  //TYPE_LOG_MESSAGE    * p_log_message;
  TCHAR               _log_text [LOG_MESSAGE_BUFFER_SIZE + 1];  // Add terminating NULL char
  TCHAR               _param3 [NLS_MSG_PARAM_MAX_LENGTH + 1];
  TCHAR               _param4 [NLS_MSG_PARAM_MAX_LENGTH + 1];
  TCHAR               _param5 [NLS_MSG_PARAM_MAX_LENGTH + 1];
  //TCHAR               _sending_error_header[NLS_MSG_STRING_MAX_LENGTH+1];
 
  // Disable IPC interaction due IPC is not being used in PSAC.

  // Get computer name
  //if ( _first_time )
  //{
  //  if ( Common_IpcGetNodeInstanceId (IPC_NODE_NAME_APP_LOG, &_ipc_target_node_id, FALSE) == FALSE )
  //  {
  //    PrivateLog (2, MODULE_NAME, _function_name, _T("FALSE"), _T("Common_IpcGetNodeInstanceId"));
  //    if ( pExtraMsg != NULL)
  //    {
  //      TCHAR _param3 [NLS_MSG_PARAM_MAX_LENGTH + 1];
  //      TCHAR _param4 [NLS_MSG_PARAM_MAX_LENGTH + 1];
  //      TCHAR _param5 [NLS_MSG_PARAM_MAX_LENGTH + 1];

  //      NLS_GetString(NLS_ID_GUI_CONTROLS(132),_sending_error_header);
  //      _stprintf (_sending_error_header, _T("%s%s%s%s"), _sending_error_header, pParam3, pParam4, pParam5);
  //      NLS_SplitString(_sending_error_header, _param3, _param4, _param5, NULL, NULL);
  //      PrivateLog (10, pParam1, pParam2, _param3, _param4, _param5);
  //    }

  //    return;
  //  } // if
  //  _first_time = FALSE;
  //} // if

  // Input Params
  //_api_input_params.control_block = sizeof (API_INPUT_PARAMS);
  //_api_input_params.event_object  = NULL;
  //_api_input_params.function_code = IPC_CODE_SEND;
  //_api_input_params.mode          = API_MODE_NO_WAIT;

  //_ipc_iud_send.control_block       = sizeof (IPC_IUD_SEND);
  //_ipc_iud_send.msg_cmd             = MSG_CMD_LOG;
  //_ipc_iud_send.msg_id              = 0;
  //_ipc_iud_send.source_node_inst_id = IPC_NODE_UNKNOWN;
  //_ipc_iud_send.target_node_inst_id = _ipc_target_node_id; 
  //_ipc_iud_send.user_buffer_length  = sizeof (TYPE_LOG_MESSAGE);

  //_api_output_params.control_block  = sizeof (API_OUTPUT_PARAMS);

  //p_log_message = (TYPE_LOG_MESSAGE *) &_ipc_iud_send.user_buffer; 

  //// Specific data
  //p_log_message->control_block = sizeof (TYPE_LOG_MESSAGE);

  _rc = NLS_GetString (MsgId, _log_text, pParam3, pParam4, pParam5);
  if (   _rc != NLS_STATUS_OK 
      && _rc != NLS_STATUS_DLL_NOT_LOADED )
  {
    _stprintf (_log_text, _T("%s"), _T("Error in Nls API."));
  }

  if ( _rc == NLS_STATUS_DLL_NOT_LOADED )
  {
    _stprintf (_log_text, _T("%s"), _T("Error in Nls. Nls not loaded"));
  }
  
  //LogMsgText (NULL,
  //            pParam1, 
  //            pParam2, 
  //            MsgId,
  //            _log_text,
  //            pExtraMsg, 
  //            LOG_MESSAGE_BUFFER_SIZE, 
  //            p_log_message->message);


  // IPC not supported - Write directly on private log.
  NLS_SplitString (_log_text, _param3, _param4, _param5, NULL, NULL);

  PrivateLog (5, pParam1, pParam2, _param3, _param4, _param5, pExtraMsg);



  // Compute the message length 
  //p_log_message->message_length = _tcslen (p_log_message->message);

  //// Compute the User Buffer length
  //_ipc_iud_send.user_buffer_length = sizeof (TYPE_LOG_MESSAGE) 
  //                                   - sizeof (p_log_message->message) 
  //                                   + sizeof (TCHAR) * (1 + p_log_message->message_length);
  //
  // //API CALL
  // // don't care about the return code
  //_rc = Ipc_API (&_api_input_params,    // Input Params
  //               &_ipc_iud_send,        // Input User Data
  //               &_api_output_params,   // Output Params
  //               NULL);                 // Output User Data

  //
  //if ( _rc != API_STATUS_OK )
  //{
  //  _stprintf (_rc_str, _T("%u"), _rc);
  //  PrivateLog (2, MODULE_NAME, _function_name, _rc_str, _T("Ipc_API"));
  //} // if

  return;
} // Common_LoggerMsg


//------------------------------------------------------------------------------
//  PRIVATE FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------