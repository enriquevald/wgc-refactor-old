//-----------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//-----------------------------------------------------------------------------
// 
//   MODULE NAME: CommonMiscConfiguration.cpp
// 
//   DESCRIPTION: Functions and Methods of the configuration information.
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 13-MAR-2002
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 13-MAR-2002 ACC    Initial draft.
// 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  INCLUDES 
//------------------------------------------------------------------------------
#define INCLUDE_COMMON_MISC
#define INCLUDE_NLS_API
#define INCLUDE_COMMON_BASE

#include "CommonDef.h"

#include "CommonMiscInternals.h"

//------------------------------------------------------------------------------
//  PRIVATE CONSTANTS 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PRIVATE DATATYPES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PRIVATE DATA STRUCTURES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PRIVATE FUNCTION PROTOTYPES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PRIVATE FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PUBLIC FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE: Function used by Visual C generated modules in order to get the
//          center configuration from registry.
// 
//  PARAMS:
//      - INPUT:
//          - None
//
//      - OUTPUT: 
//          - pConfigurationEx : struct with entry values
// 
// RETURNS :
//      - COMMON_ERROR_CONTROL_BLOCK
//      - RCs from Common_GetConfiguration:
//          - COMMON_OK
//          - COMMON_ERROR
//          - COMMON_ERROR_CONTROL_BLOCK
//          - COMMON_ERROR_DB
//
//   NOTES:
//      - This function has an equivalen output structure than primitive 
//        Common_GetConfiguration function. The difference is in buffer handling.
//        For Visual Basic reasons, primitive function handles buffers as 
//        pointers to TCHARs. This Common_GetConfigurationEx function is needed
//        for Visual C modules in order to provide allocated memory (Arrays of TCHARs)
//        for the output data
//
//        This function receives caller ownership memory as output parameter, it 
//        assigns the addresses of this memory to TCHAR pointers of the primitive 
//        function output structure and calls Common_GetConfiguration in the same
//        way of a Visual Basic module.

COMMONMISC_API DWORD WINAPI Common_GetConfigurationEx (TYPE_CENTER_CONFIGURATION_EX  * pConfigurationEx)
{
  TCHAR     _log_param_1 [COMMON_LOGGER_PARAMETER_SIZE];
  TCHAR     _log_param_2 [COMMON_LOGGER_PARAMETER_SIZE];
  DWORD     _rc;

  TYPE_CENTER_CONFIGURATION   _center_configuration;

  // Check control block
  if ( pConfigurationEx->control_block != sizeof (TYPE_CENTER_CONFIGURATION_EX) )
  {
    _stprintf (_log_param_1, _T("%lu"), sizeof (TYPE_CENTER_CONFIGURATION_EX));
    _stprintf (_log_param_2, _T("%lu"), pConfigurationEx->control_block);
    Common_Logger (NLS_ID_LOGGER(3), _T("CONTROL_BLOCK"), _log_param_1, _log_param_2);

    return COMMON_ERROR_CONTROL_BLOCK;
  }

  // Reset primitive function output structure
  memset (&_center_configuration, 0, sizeof (TYPE_CENTER_CONFIGURATION));

  // Prepare primitive function output structure with references of output
  // parameter structure passed to this function
  // Control block expected by primitive function is the size of its structure parameter
  _center_configuration.control_block = sizeof (TYPE_CENTER_CONFIGURATION);
  // Assign pointer references
  _center_configuration.database.p_connect_string = pConfigurationEx->database.connect_string;
  _center_configuration.database.p_password       = pConfigurationEx->database.password;
  _center_configuration.database.p_user           = pConfigurationEx->database.user;

  // Call primitive function
  _rc = Common_GetConfiguration (&_center_configuration);
  if ( _rc != COMMON_OK )
  {
    _stprintf (_log_param_1, _T("%lu"), _rc);
    Common_Logger (NLS_ID_LOGGER(2), _log_param_1, _T("Common_GetConfiguration"), _T(""));
  }
  else
  {
    // Assign data passed as value to initial output structure
    pConfigurationEx->language = _center_configuration.language;
  }

  // Return code from Common_GetConfiguration
  return _rc;
} // Common_GetConfigurationEx
