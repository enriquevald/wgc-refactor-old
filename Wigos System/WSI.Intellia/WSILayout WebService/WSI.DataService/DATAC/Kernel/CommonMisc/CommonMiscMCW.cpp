//------------------------------------------------------------------------------
// Copyright © 2008 Win Systems International.
//------------------------------------------------------------------------------
//
//   MODULE NAME: CommonMiscMCW.cpp
//
//   DESCRIPTION: Functions to handle API calls to MCW_API (Card Writer)
//
//        AUTHOR: Ronald Rodríguez
//
// CREATION DATE: 25-SEP-2008
//
// REVISION HISTORY
//
// Date        Author Description
// ----------- ------ -----------------------------------------------------------
// 25-SEP-2008 RRT    Initial draft.
//-------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  INCLUDES
//------------------------------------------------------------------------------
#define INCLUDE_COMMON_MISC
#define INCLUDE_QUEUE_API
#define INCLUDE_MCW_API
#define INCLUDE_PRIVATE_LOG
#include "CommonDef.h"

#include "CommonMiscInternals.h"

//------------------------------------------------------------------------------
//  PRIVATE CONSTANTS
//------------------------------------------------------------------------------
#define RETURN_CODE_STR_LENGHT      20

// Dynamic API calls.
#define MAX_API_DLL                  10
#define MAX_DLL_NAME                 40
#define MAX_DLL_ENTRY_NAME           40

#define EXT_MCW_API_DLL_IDX                0
#define EXT_MCW_API_DLL_NAME               _T("MCW_API")
#define EXT_MCW_API_DLL_ENTRY_NAME         _T("Mcw_API")

//------------------------------------------------------------------------------
//  PRIVATE DATATYPES
//------------------------------------------------------------------------------
  
//------------------------------------------------------------------------------
//  PRIVATE DATA STRUCTURES
//------------------------------------------------------------------------------
// Dynamic API calls.
typedef TCHAR TYPE_DLL_NAME [MAX_DLL_NAME];

typedef TCHAR TYPE_DLL_ENTRY_NAME [MAX_DLL_ENTRY_NAME];

typedef WORD ( WINAPI * TYPE_API_DLL_ENTRY_ADDRESS ) (API_INPUT_PARAMS  *, API_INPUT_USER_DATA  *, API_OUTPUT_PARAMS *, API_OUTPUT_USER_DATA *);

typedef struct
{
  HMODULE                     dll_module;
  TYPE_DLL_NAME               dll_name;
  TYPE_DLL_ENTRY_NAME         dll_entry_name;
  TYPE_API_DLL_ENTRY_ADDRESS  dll_entry_address;

} TYPE_API_DLL_DATA;

typedef struct
{
  WORD                        num_api_dll;
  TYPE_API_DLL_DATA           api_dll_list [MAX_API_DLL];

} TYPE_DYNAMIC_API_DLL;


TYPE_DYNAMIC_API_DLL      GLB_Dymamic_API_Dll;

//------------------------------------------------------------------------------
//  PRIVATE FUNCTION PROTOTYPES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PRIVATE FUNCTIONS IMPLEMENTATION
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PUBLIC FUNCTIONS IMPLEMENTATION
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE: Dynamically loads API dlls.
//
//  PARAMS:
//      - INPUT:
//
//      - OUTPUT:
//
// RETURNS:
//      
//   NOTES:
//
BOOL Common_MCW_APIInit ()
{
  TYPE_LOGGER_PARAM   _log_param1;
  DWORD               _api_dll_idx;

  ZeroMemory(&GLB_Dymamic_API_Dll, sizeof (TYPE_DYNAMIC_API_DLL));
  GLB_Dymamic_API_Dll.num_api_dll = 0;

  _tcscpy (GLB_Dymamic_API_Dll.api_dll_list[EXT_MCW_API_DLL_IDX].dll_name, EXT_MCW_API_DLL_NAME);
  _tcscpy (GLB_Dymamic_API_Dll.api_dll_list[EXT_MCW_API_DLL_IDX].dll_entry_name, EXT_MCW_API_DLL_ENTRY_NAME);
  GLB_Dymamic_API_Dll.num_api_dll++;

  for ( _api_dll_idx = 0; _api_dll_idx < GLB_Dymamic_API_Dll.num_api_dll; _api_dll_idx++ )
  {
    GLB_Dymamic_API_Dll.api_dll_list[_api_dll_idx].dll_module = LoadLibrary (GLB_Dymamic_API_Dll.api_dll_list[_api_dll_idx].dll_name);
    if ( GLB_Dymamic_API_Dll.api_dll_list[_api_dll_idx].dll_module == NULL )
    {
      // Logger message
      _stprintf (_log_param1, _T("%lu"), GetLastError ());
      Common_Logger (21, _T("LoadLibrary"), _log_param1, GLB_Dymamic_API_Dll.api_dll_list[_api_dll_idx].dll_name);

      return FALSE;
    }

    GLB_Dymamic_API_Dll.api_dll_list[_api_dll_idx].dll_entry_address = (TYPE_API_DLL_ENTRY_ADDRESS) Common_GetProcAddress (GLB_Dymamic_API_Dll.api_dll_list[_api_dll_idx].dll_module, 
                                                                                                                           GLB_Dymamic_API_Dll.api_dll_list[_api_dll_idx].dll_entry_name);
    if ( GLB_Dymamic_API_Dll.api_dll_list[_api_dll_idx].dll_entry_address == NULL )
    {
      // Log message
      _stprintf (_log_param1, _T("%lu"), GetLastError ());
      Common_Logger (21, _T("Common_GetProcAddress"), _log_param1, GLB_Dymamic_API_Dll.api_dll_list[_api_dll_idx].dll_name);

      return FALSE;
    }

  } // for

  return TRUE;

} // Common_MCW_APIInit

//-----------------------------------------------------------------------------
// PURPOSE: Initialize the MCW (Card Writer) Api module.
//
//  PARAMS:
//     - INPUT:
//
//     - OUTPUT:
//
// RETURNS:
//     - TRUE: Success.
//     - FALSE: Error.
//
// NOTES:

COMMONMISC_API BOOL WINAPI Common_MCW_ModuleInit (TYPE_MCW_MODELS_LIST  * pModels)
{
  TCHAR                   _rc_str [RETURN_CODE_STR_LENGHT];
  MCW_OUD_MODULE_INIT     _mcw_oud;
  API_INPUT_PARAMS        _api_input;
  API_OUTPUT_PARAMS       _api_output;
  TYPE_MCW_MODEL          * p_model;
  WORD                    _idx_model;
  WORD                    _rc;

  // Check control block
  if ( pModels->control_block != sizeof (TYPE_MCW_MODELS_LIST) )
  {
    return FALSE;
  }

  if ( pModels->models.control_block_item != sizeof (TYPE_MCW_MODEL) )
  {
    return FALSE;
  }

  if ( Common_MCW_APIInit() == FALSE )
  {
    return FALSE;
  }

  // INPUT PARAMS:
  //   - API
  _api_input.control_block = sizeof (API_INPUT_PARAMS);
  _api_input.event_object = NULL;
  _api_input.mode = API_MODE_SYNC;
  _api_input.function_code = MCW_CODE_MODULE_INIT;

  // OUPUT PARAMS:
  //   - API
  _api_output.control_block = sizeof (API_OUTPUT_PARAMS);

  ZeroMemory(&_mcw_oud, sizeof(_mcw_oud));
  
  _mcw_oud.control_block = sizeof(MCW_OUD_MODULE_INIT);

  _rc = (GLB_Dymamic_API_Dll.api_dll_list[EXT_MCW_API_DLL_IDX].dll_entry_address)
        (&_api_input, // Input Params
         NULL,        // Input User Data
         &_api_output,// Output Params
         &_mcw_oud);  // Output User Data
  if ( _rc == API_STATUS_OK )
  {
    if ( _api_output.output_1 == MCW_STATUS_OK )
    {
      // Copy output data
      pModels->num_models = _mcw_oud.num_models;

      // Fill models
      p_model = (TYPE_MCW_MODEL *) pModels->models.p_items;

      for ( _idx_model = 0; _idx_model < _mcw_oud.num_models; _idx_model++ )
      {
        p_model[_idx_model].id   = _mcw_oud.model_data[_idx_model].model;
        _tcscpy (p_model[_idx_model].name, _mcw_oud.model_data[_idx_model].model_name);
      } // for
      pModels->models.num_items = _mcw_oud.num_models;


      return TRUE;  
    } // if

    _stprintf (_rc_str, _T("%u"), _api_output.output_1);
    PrivateLog (2, MODULE_NAME, __FUNCTION__, _rc_str, _T("Mcw_API"));

    return FALSE;
  } // if

  _stprintf (_rc_str, _T("%u"), _rc);
  PrivateLog (2, MODULE_NAME, __FUNCTION__, _rc_str, _T("Mcw_API"));

  return FALSE;


} // Common_MCW_ModuleInit 

//-----------------------------------------------------------------------------
// PURPOSE: Initialize a MCW device (Card Writer)
//
//  PARAMS:
//     - INPUT:
//        - Model: Device Model 
//
//     - OUTPUT:
//
// RETURNS:
//     - TRUE: Success.
//     - FALSE: Error.
//
// NOTES:

COMMONMISC_API BOOL WINAPI Common_MCW_DeviceInit (WORD Model)
{
  TCHAR                   _rc_str [RETURN_CODE_STR_LENGHT];
  API_INPUT_PARAMS        _api_input;
  API_OUTPUT_PARAMS       _api_output;
  MCW_IUD_DEVICE_INIT     _ipc_iud;
  WORD                    _rc;

  // INPUT PARAMS:
  //   - API
  _api_input.control_block = sizeof (API_INPUT_PARAMS);
  _api_input.event_object = NULL;
  _api_input.mode = API_MODE_SYNC;
  _api_input.function_code = MCW_CODE_DEVICE_INIT;

  _ipc_iud.control_block = sizeof (MCW_IUD_DEVICE_INIT);
  _ipc_iud.model       = Model;

  // OUPUT PARAMS:
  //   - API
  _api_output.control_block = sizeof (API_OUTPUT_PARAMS);

  // API CALL
  _rc = (GLB_Dymamic_API_Dll.api_dll_list[EXT_MCW_API_DLL_IDX].dll_entry_address)
        (&_api_input, // Input Params
         &_ipc_iud,   // Input User Data
         &_api_output,// Output Params
         NULL);       // Output User Data
  if ( _rc == API_STATUS_OK )
  {
    if ( _api_output.output_1 == MCW_STATUS_OK )
    {
      return TRUE;  
    } // if

    _stprintf (_rc_str, _T("%u"), _api_output.output_1);
    PrivateLog (2, MODULE_NAME, __FUNCTION__, _rc_str, _T("Mcw_API"));

    return FALSE;
  } // if

  _stprintf (_rc_str, _T("%u"), _rc);
  PrivateLog (2, MODULE_NAME, __FUNCTION__, _rc_str, _T("Mcw_API"));

  return FALSE;

} // Common_MCW_DeviceInit 


//-----------------------------------------------------------------------------
// PURPOSE: Perform a write operation on the card writer.
//
//  PARAMS:
//     - INPUT:
//        - pWriteData: Write data structure.
//
//     - OUTPUT:
//
// RETURNS:
//     - TRUE: Success.
//     - FALSE: Error.
//
// NOTES:

COMMONMISC_API BOOL WINAPI Common_MCW_DeviceWrite (TYPE_MCW_WRITE_DATA  * pWriteData)
{
  TCHAR                   _rc_str [RETURN_CODE_STR_LENGHT];
  API_INPUT_PARAMS        _api_input;
  API_OUTPUT_PARAMS       _api_output;
  MCW_IUD_WRITE           _ipc_iud;
  WORD                    _rc;

  // Check control block
  if ( pWriteData->control_block != sizeof (TYPE_MCW_WRITE_DATA) )
  {
    return FALSE;
  }

  // INPUT PARAMS:
  //   - API
  _api_input.control_block = sizeof (API_INPUT_PARAMS);
  _api_input.event_object = NULL;
  _api_input.mode = API_MODE_SYNC;
  _api_input.function_code = MCW_CODE_WRITE;

  ZeroMemory(&_ipc_iud, sizeof(_ipc_iud));

  _ipc_iud.control_block = sizeof (MCW_IUD_WRITE);

  // Fill input data
  _ipc_iud.card_data.card_type = (ENUM_CARD_TYPE) pWriteData->card_type;
  _ipc_iud.card_data.data1_length = min(pWriteData->data1_length, MCW_MAX_DATA_LENGTH);
  _stprintf ((TCHAR *)_ipc_iud.card_data.data1, _T("%s"), pWriteData->data1);

  _ipc_iud.card_data.data2_length = 0;
  _ipc_iud.card_data.data3_length = 0;

  // OUPUT PARAMS:
  //   - API
  _api_output.control_block = sizeof (API_OUTPUT_PARAMS);

  // API CALL
  _rc = (GLB_Dymamic_API_Dll.api_dll_list[EXT_MCW_API_DLL_IDX].dll_entry_address)
        (&_api_input, // Input Params
         &_ipc_iud,   // Input User Data
         &_api_output,// Output Params
         NULL);       // Output User Data

  if ( _rc == API_STATUS_OK )
  {
    if ( _api_output.output_1 == MCW_STATUS_OK )
    {
      return TRUE;  
    } // if

    _stprintf (_rc_str, _T("%u"), _api_output.output_1);
    PrivateLog (2, MODULE_NAME, __FUNCTION__, _rc_str, _T("Mcw_API"));

    return FALSE;
  } // if

  _stprintf (_rc_str, _T("%u"), _rc);
  PrivateLog (2, MODULE_NAME, __FUNCTION__, _rc_str, _T("Mcw_API"));

  return FALSE;

} // Common_MCW_DeviceWrite 



//-----------------------------------------------------------------------------
// PURPOSE: Stop a writing operation.
//
//  PARAMS:
//     - INPUT:
//
//     - OUTPUT:
//
// RETURNS:
//     - TRUE: Success.
//     - FALSE: Error.
//
// NOTES:

COMMONMISC_API BOOL WINAPI Common_MCW_DeviceCancelWrite ()
{
  TCHAR                   _rc_str [RETURN_CODE_STR_LENGHT];
  API_INPUT_PARAMS        _api_input;
  API_OUTPUT_PARAMS       _api_output;
  WORD                    _rc;

   // INPUT PARAMS:
  //   - API
  _api_input.control_block = sizeof (API_INPUT_PARAMS);
  _api_input.event_object = NULL;
  _api_input.mode = API_MODE_SYNC;
  _api_input.function_code = MCW_CODE_CANCEL_WRITE;

  // OUPUT PARAMS:
  //   - API
  _api_output.control_block = sizeof (API_OUTPUT_PARAMS);

  // API CALL
  _rc = (GLB_Dymamic_API_Dll.api_dll_list[EXT_MCW_API_DLL_IDX].dll_entry_address)
        (&_api_input, // Input Params
         NULL,        // Input User Data
         &_api_output,// Output Params
         NULL);       // Output User Data

  if ( _rc == API_STATUS_OK )
  {
    if ( _api_output.output_1 == MCW_STATUS_OK )
    {
      return TRUE;  
    } // if

    _stprintf (_rc_str, _T("%u"), _api_output.output_1);
    PrivateLog (2, MODULE_NAME, __FUNCTION__, _rc_str, _T("Mcw_API"));

    return FALSE;
  } // if

  _stprintf (_rc_str, _T("%u"), _rc);
  PrivateLog (2, MODULE_NAME, __FUNCTION__, _rc_str, _T("Mcw_API"));

  return FALSE;

} // Common_MCW_DeviceCancelWrite 


//-----------------------------------------------------------------------------
// PURPOSE: Close the communication with the device.
//
//  PARAMS:
//     - INPUT:
//
//     - OUTPUT:
//
// RETURNS:
//     - TRUE: Success.
//     - FALSE: Error.
//
// NOTES:

COMMONMISC_API BOOL WINAPI Common_MCW_DeviceClose ()
{
  TCHAR                   _rc_str [RETURN_CODE_STR_LENGHT];
  API_INPUT_PARAMS        _api_input;
  API_OUTPUT_PARAMS       _api_output;
  WORD                    _rc;

  // INPUT PARAMS:
  //   - API
  _api_input.control_block = sizeof (API_INPUT_PARAMS);
  _api_input.event_object = NULL;
  _api_input.mode = API_MODE_SYNC;
  _api_input.function_code = MCW_CODE_DEVICE_CLOSE;

  // OUPUT PARAMS:
  //   - API
  _api_output.control_block = sizeof (API_OUTPUT_PARAMS);

  // API CALL
  _rc = (GLB_Dymamic_API_Dll.api_dll_list[EXT_MCW_API_DLL_IDX].dll_entry_address)
        (&_api_input, // Input Params
         NULL,        // Input User Data
         &_api_output,// Output Params
         NULL);       // Output User Data

  if ( _rc == API_STATUS_OK )
  {
    if ( _api_output.output_1 == MCW_STATUS_OK )
    {
      return TRUE;  
    } // if

    _stprintf (_rc_str, _T("%u"), _api_output.output_1);
    PrivateLog (2, MODULE_NAME, __FUNCTION__, _rc_str, _T("Mcw_API"));

    return FALSE;
  } // if

  _stprintf (_rc_str, _T("%u"), _rc);
  PrivateLog (2, MODULE_NAME, __FUNCTION__, _rc_str, _T("Mcw_API"));

  return FALSE;

} // Common_MCW_DeviceClose 