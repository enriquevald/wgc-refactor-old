//-----------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : VersionControl.cpp
// 
//   DESCRIPTION : Functions and Methods to handle system modules version control
//
//                 This module is SHARED in LKC, LKAS and LKT solutions.
//
//                 VERY IMPORTANT !!!
//                 Due that this is a module is shared (LKC, LKAS & LKT), 
//                 ANY change must be checked twice by compiling LKC, LKAS
//                 and LKT DLLs
// 
//        AUTHOR : Carles Iglesias
// 
// CREATION DATE : 03-JUN-2002
// 
// REVISION HISTORY
// 
// Date        Author Description
//----------- ------ ----------------------------------------------------------
// 03-JUN-2002 CIR    Initial draft.
// 01-AUG-2002 ACC    Module is shared.
// 02-MAR-2004 RRT    Added two(2) Database versions: CommonDatabase, ClientDatabase.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  INCLUDES
//------------------------------------------------------------------------------
#define INCLUDE_COMMON_MISC
#define INCLUDE_VERSION_CONTROL
#define INCLUDE_NLS_API

#include "CommonDef.h"

#include "VersionControlInternals.h"

//------------------------------------------------------------------------------
// PRIVATE CONSTANTS 
//------------------------------------------------------------------------------
#define VERSION_LANGUAGE_NEUTRAL        MAKELANGID (LANG_NEUTRAL,SUBLANG_NEUTRAL)
#define VERSION_CHARACTERSET            0x04B0      // UNICODE 

#define VERSION_LENGTH_STRING_INFO      128
#define VERSION_LENGTH_STRING_VALUE     128
#define VERSION_LENGTH_STRING_KEY       64

#define VERSION_STRING_FORMAT           _T("%02hu.%03hu")
#define VERSION_STRING_FORMAT_DB        _T("%02hu.%03hu.%03hu")
#define VERSION_STRING_FORMAT_MODULE_DB _T("%02hu.%03hu.%03hu.%03hu")
#define VERSION_DATABASE_FLAG           _T("(DB)")

// Version info key data to retrieve
#define VERSION_KEY_COMPANY_NAME        _T("CompanyName")
#define VERSION_KEY_COPYRIGHT           _T("LegalCopyright")
#define VERSION_KEY_PRODUCT_VERSION     _T("ProductVersion")

//------------------------------------------------------------------------------
// PRIVATE DATATYPES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE FUNCTION PROTOTYPES 
//------------------------------------------------------------------------------
BOOL            Version_GetVersionInfo (BYTE                    * pVersionInfo, 
                                        TYPE_VERSION_COMPONENT  * pComponentVersion);

BOOL            Version_GetVersionData (BYTE   * pVersionInfo, 
                                        TCHAR  * pInfoKey,
                                        TCHAR  ** pSpecificInfoData);

BOOL            Version_AssignVersionData (TCHAR                  * pVersionData,
                                           TYPE_VERSION_COMPONENT * pComponentVersion);

WORD            Version_CheckVersions (TYPE_VERSION_MODULE * pModuleVersion);

//------------------------------------------------------------------------------
//  PUBLIC FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE : Check all parameter module components version information. Also,
//          this function returns the module about information. This about 
//          information is:
//              - Module version string (CC.BBB)
//              - Company name
//              - Legal copyright
// 
//  PARAMS :
//      - INPUT :
//          - pModuleName: Module name
//          - pModulePath: Full path where module components are located
//
//      - OUTPUT : 
//          - pModuleAbout: Module about information as module version,
//                          company name and legal copyright
// 
// RETURNS : 
//          - VERSION_STATUS_OK
//          - VERSION_STATUS_ERROR
//          - VERSION_STATUS_DL_NOT_FOUND
//          - VERSION_STATUS_COMPONENT_NOT_FOUND
//          - VERSION_STATUS_VERSION_ERROR
// 
//   NOTES : 

WORD        Version_CheckModuleVersion (TCHAR             * pModuleName, 
                                        TCHAR             * pModulePath,
                                        TYPE_MODULE_ABOUT * pModuleAbout)
{
  TCHAR               _function_name [] = _T("Version_CheckModuleVersion");
  TYPE_LOGGER_PARAM   _log_param1;
  TYPE_LOGGER_PARAM   _log_param2;
  TYPE_VERSION_MODULE _module_version;
  WORD                _rc;  

  // Check control block for output parameter
#if defined(LKT_VERSION) || defined(LKAS_VERSION)
  if ( pModuleAbout->control_block.StructSize != sizeof (TYPE_MODULE_ABOUT) )
#else
  if ( pModuleAbout->control_block != sizeof (TYPE_MODULE_ABOUT) )
#endif
  {
    // Erroneous control block
    _stprintf (_log_param1, _T("%lu"), sizeof (TYPE_MODULE_ABOUT));
    _stprintf (_log_param2, _T("%lu"), pModuleAbout->control_block);
    Common_LoggerVersion (VERSION_LOG_CODE_2, _T("control_block"), _log_param1, _log_param2);

    return VERSION_STATUS_ERROR;
  }

  // Init module version structure 
  memset (&_module_version, 0, sizeof (TYPE_VERSION_MODULE));
  Common_SetCB (_module_version.control_block, TYPE_VERSION_MODULE);

  // Check module version by
  //    - Get Module information (expected from DL and found from components)
  //    - Check expected and found values skipping first list component
  //      because it corresponds to virtual module version

  //    - Get Module information (expected from DL and found from components)
  _rc = Version_GetModuleVersion (pModuleName,
                                  pModulePath,
                                  &_module_version);
  if ( _rc != VERSION_STATUS_OK )
  {
    // Error gettimg module information
    _stprintf (_log_param1, _T("%hu"), _rc);    
    Common_LoggerVersion (VERSION_LOG_CODE_1, _log_param1, _T("Version_GetModuleVersion"), NULL);

    return _rc;
  } 

  // Fill output structure with the following items
  //    - Module version string
  //    - Company name 
  //    - Legal copyright
  //    - Client Id

  //    - Module version string
  _tcscpy (pModuleAbout->version, _module_version.component[0].expected.version);
  //    - Company name 
  _tcscpy (pModuleAbout->company_name, _module_version.component[0].company_name);
  //    - Legal copyright
  _tcscpy (pModuleAbout->copyright, _module_version.component[0].copyright);

  // TJG 26-JAN-2004
  //    - Client Id (required to check it against the license)
  pModuleAbout->client_id             = _module_version.client_id;
  pModuleAbout->build                 = _module_version.component[0].expected.build;
  pModuleAbout->database_client_build = _module_version.component[0].expected.client_database;
  pModuleAbout->database_common_build = _module_version.component[0].expected.common_database;

  //    - Check expected and found values skipping first list component
  //      because it corresponds to virtual module version
  return Version_CheckVersions (&_module_version);
} // Version_CheckModuleVersion

//------------------------------------------------------------------------------
// PURPOSE : Get all parameter module components version information 
// 
//  PARAMS :
//      - INPUT :
//          - pModuleName: Module name
//          - pModulePath: Full path where module components are located
//
//      - OUTPUT : 
//          - pModuleVersion: Pointer to all module components version information
// 
// RETURNS : 
//          - VERSION_STATUS_OK
//          - VERSION_STATUS_ERROR
//          - VERSION_STATUS_DL_NOT_FOUND
//          - VERSION_STATUS_COMPONENT_NOT_FOUND
// 
//   NOTES :

WORD        Version_GetModuleVersion (TCHAR               * pModuleName, 
                                      TCHAR               * pModulePath,
                                      TYPE_VERSION_MODULE * pModuleVersion)
{
  TCHAR                 _function_name [] = _T("Version_GetModuleVersion");
  TYPE_LOGGER_PARAM     _log_param1;
  TYPE_LOGGER_PARAM     _log_param2;
  TCHAR                 _db_user [COMMON_DB_USER_SIZE + 1];
  TCHAR                 _db_password [COMMON_DB_PASSWORD_SIZE + 1];
  TCHAR                 _db_connect_string [COMMON_DB_CONNECT_STRING_SIZE + 1];
  WORD                  _idx_component;
  WORD                  _rc;
  TYPE_DATABASE_VERSION _db_version;
  TYPE_SIGNATURE_CHECK  _signature_check;
  
  // Check function parameters
  //    - pModuleName cannot be NULL  
  //    - pModulePath cannot be NULL  
  //    - pModuleVersion cannot be NULL  

  if (   pModuleName == NULL 
      || pModulePath == NULL 
      || pModuleVersion == NULL )
  {
    // Any parameter has a NULL value
    Common_LoggerVersion (VERSION_LOG_CODE_3, _T("Parameter"), _T("NULL"), NULL);

    return VERSION_STATUS_ERROR;
  }

  // Check control block
#if defined(LKT_VERSION) || defined(LKAS_VERSION)
  if ( pModuleVersion->control_block.StructSize != sizeof (TYPE_VERSION_MODULE) )
#else
  if ( pModuleVersion->control_block != sizeof (TYPE_VERSION_MODULE) )
#endif
  {
    // Erroneous control block
    // Write Logger
    _stprintf (_log_param1, _T("%lu"), sizeof (TYPE_VERSION_MODULE));
    _stprintf (_log_param2, _T("%lu"), pModuleVersion->control_block);
    Common_LoggerVersion (VERSION_LOG_CODE_2, _T("control_block"), _log_param1, _log_param2);

    return VERSION_STATUS_ERROR;
  }

  // Get module version Process: 
  //    - Get Module information from DL file.
  //    - Get module's client Id.
  //    - Connect to Database and retrieve the two(2) Database versions:
  //        - Common Database Build.
  //        - Client Database Build.
  //    - Get all module component versions from component file.
  //      Note: Skip first list component because it corresponds to virtual module version.
  //    - If it is first component, store its version info to module (index 0)
  //        - Company name
  //        - Legal copyright

  // - Get Module information from DL file
  _rc = Version_GetDLData (pModulePath, pModuleName, pModuleVersion, &_signature_check, TRUE);
  if ( _rc != VERSION_STATUS_OK )
  {
    // Error getting module information from DL
    _stprintf (_log_param1, _T("%hu"), _rc);
    Common_LoggerVersion (VERSION_LOG_CODE_1, _log_param1, _T("Version_GetDLData"), NULL);
 
    return _rc;
  }

  // - Connect to Database and retrieve the two(2) Database versions:

  //  Get from center configuration values used in version control process:
  //    - User name to connect to database
  //    - User password to connect to database
  //    - Database connect string

  // Assign different values to output parameters
  _tcscpy (_db_user, _T(" "));
  _tcscpy (_db_password, _T(" "));
  _tcscpy (_db_connect_string, _T(" "));

  // Initialize Database variable
  memset (&_db_version, 0, sizeof (TYPE_DATABASE_VERSION));
  _db_version.control_block = sizeof (TYPE_DATABASE_VERSION);

  // Assign database version values to output parameters
  _db_version.client_id    = 0;
  _db_version.common_build = 0;
  _db_version.client_build = 0;

  // TJG 26-JAN-2004
  //    - Get module's client id
  pModuleVersion->client_id = pModuleVersion->component[0].expected.client;

  // For Virtual Module (Component 0) assign Database found Builds versions in order to compare them.
  pModuleVersion->component[0].found.client          = _db_version.client_id;
  pModuleVersion->component[0].found.common_database = _db_version.common_build;
  pModuleVersion->component[0].found.client_database = _db_version.client_build;   

  //    - Get all module component versions from component file
  //      Skip first list component because it corresponds to virtual module version

  for ( _idx_component = 1; _idx_component < pModuleVersion->num_components; _idx_component++ )
  {
    // Get version information from module
    _rc = Version_GetComponentVersion (pModulePath, 
                                       &pModuleVersion->component[_idx_component]);
    if ( _rc != VERSION_STATUS_OK )
    {
      // Error getting version
      _stprintf (_log_param1, _T("%hu"), _rc);
      Common_LoggerVersion (VERSION_LOG_CODE_4, _log_param1, _T("Version_GetComponentVersion"), pModuleVersion->component[_idx_component].name);

      return _rc;
    }

    //    - If it is first component, store its version info to module (index 0)
    if ( _idx_component == 1 )
    {
      //        - Company name
      _tcscpy (pModuleVersion->component[0].company_name, pModuleVersion->component[_idx_component].company_name);

      //        - Legal copyright
      _tcscpy (pModuleVersion->component[0].copyright, pModuleVersion->component[_idx_component].copyright);
    }

    // Invalidate component version if signature check has failed
    if ( ! _signature_check.signature_status[_idx_component] )
    {    
      pModuleVersion->component[_idx_component].found.client = 0;
      pModuleVersion->component[_idx_component].found.build  = 0;
    }

    // Format database version string
    // database component version is not taken from the DB
    Version_FormatVersionString (pModuleVersion->component[_idx_component].found.client,
                                 pModuleVersion->component[_idx_component].found.build,
                                 pModuleVersion->component[_idx_component].found.version);
  } // for

  return VERSION_STATUS_OK;
} // Version_GetModuleVersion

//------------------------------------------------------------------------------
// PURPOSE : Get all parameter module components version information 
// 
//  PARAMS :
//      - INPUT :
//          - pModuleName: Module name
//          - pModulePath: Full path where module components are located
//
//      - OUTPUT : 
//          - pModuleVersion: Pointer to all module components version information
// 
// RETURNS : 
//          - VERSION_STATUS_OK
//          - VERSION_STATUS_ERROR
//          - VERSION_STATUS_DL_NOT_FOUND
//          - VERSION_STATUS_COMPONENT_NOT_FOUND
// 
//   NOTES :

WORD        Version_GetComponentsVersions (TCHAR                 * pModuleName,
                                           TCHAR                 * pModulePath,
                                           TYPE_VERSION_MODULE   * pModuleVersion)
{
  TCHAR   _function_name [] = _T("Version_GetComponentsVersions");
  TCHAR   _log_param1 [COMMON_LOGGER_PARAMETER_SIZE + 1];
  TCHAR   _log_param2 [COMMON_LOGGER_PARAMETER_SIZE + 1];
  WORD    _idx_component;
  WORD    _rc;
  TYPE_SIGNATURE_CHECK _signature_check;

  // Check function parameters
  //    - pModuleName cannot be NULL  
  //    - pModulePath cannot be NULL  
  //    - pModuleVersion cannot be NULL  

  if (   pModuleName == NULL 
      || pModulePath == NULL 
      || pModuleVersion == NULL )
  {
    // Any parameter has a NULL value
    Common_LoggerVersion (VERSION_LOG_CODE_3, _T("Parameter"), _T("NULL"), NULL);

    return VERSION_STATUS_ERROR;
  }      

  // Check control block
#if defined(LKT_VERSION) || defined(LKAS_VERSION)
  if ( pModuleVersion->control_block.StructSize != sizeof (TYPE_VERSION_MODULE) )
#else
  if ( pModuleVersion->control_block != sizeof (TYPE_VERSION_MODULE) )
#endif
  {
    // Erroneous control block
    // Write Logger
    _stprintf (_log_param1, _T("%lu"), sizeof (TYPE_VERSION_MODULE));
    _stprintf (_log_param2, _T("%lu"), pModuleVersion->control_block);
    Common_LoggerVersion (VERSION_LOG_CODE_2, _T("control_block"), _log_param1, _log_param2);

    return VERSION_STATUS_ERROR;
  } 
  
  // Get module version by
  //    - Get Module information from DL file
  //    - Get all module component versions from component file.
  //      Skip first list component because it corresponds to
  //      virtual module version
  //    - If it is first component, store its version info to module (index 0)
  //        - Company name
  //        - Legal copyright
  //    - If component has to interact with a database, assign 
  //      database version found connecting to database only
  //      one time

  //    - Get Module information from DL file
  _rc = Version_GetDLData (pModulePath, pModuleName, pModuleVersion, &_signature_check, FALSE);
  if ( _rc != VERSION_STATUS_OK )
  {
    // Error getting module information from DL
    // Write Logger
    _stprintf (_log_param1, _T("%hu"), _rc);
    Common_LoggerVersion (VERSION_LOG_CODE_1, _log_param1, _T("Version_GetDLData"), NULL);
    
    // Check if DL file was not found or no components were defined
    if (   _rc == VERSION_STATUS_DL_NOT_FOUND 
        || _rc == VERSION_STATUS_COMPONENT_NOT_FOUND )
    {
      return _rc;
    }
  } 

  //    - Get all module component versions from component file
  //      Skip first list component because it corresponds to 
  //      virtual module version  
  for ( _idx_component = 1; _idx_component < pModuleVersion->num_components; _idx_component++ )
  {
    // Get version information from module
    _rc = Version_GetComponentVersion (pModulePath,
                                       &pModuleVersion->component[_idx_component]);
    if ( _rc != VERSION_STATUS_OK )
    {
      // Error getting version
      // Write Logger
      _stprintf (_log_param1, _T("%hu"), _rc);
      Common_LoggerVersion (VERSION_LOG_CODE_4, _log_param1, _T("Version_GetComponentVersion"), pModuleVersion->component[_idx_component].name);
      
    } 
  } // for

  return VERSION_STATUS_OK;
} // Version_GetComponentsVersions                                          


//------------------------------------------------------------------------------
// PRIVATE FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE : Format component full path name
// 
//  PARAMS :
//      - INPUT :
//          - pPath: Full path where module components are located
//          - pName
//
//      - OUTPUT : 
//          - pFullPathName: Formatted path
// 
// RETURNS : 
// 
//   NOTES :

void            Version_FormatFullPathName (TCHAR   * pPath,
                                            TCHAR   * pName,
                                            TCHAR   * pFullPathName)
{
  DWORD     _path_len;

  // Init output path parameter
  _tcscpy (pFullPathName, pPath);

  // Check if component path is back slash ended
  _path_len = _tcslen (pFullPathName);
  if ( _path_len > 0 )
  {
    if ( pFullPathName [_path_len - 1] != _T('\\') )
    { 
     // Not back slash ended. Insert it
      _tcscat (pFullPathName, _T("\\"));
    } 
  }  

  // Append module name
  _tcscat (pFullPathName, pName);

  return;
} // Version_FormatFullPathName

//------------------------------------------------------------------------------
// PURPOSE : Get component version 
// 
//  PARAMS :
//      - INPUT :
//          - pModulePath: Full path where module components are located
//
//      - OUTPUT : 
//          - pComponentVersion: Component version information structure
// 
// RETURNS : 
//          - VERSION_STATUS_OK
//          - VERSION_STATUS_ERROR
// 
//   NOTES :

WORD            Version_GetComponentVersion (TCHAR                  * pModulePath,
                                             TYPE_VERSION_COMPONENT * pComponentVersion)
{
  TCHAR     _function_name [] = _T("Version_GetComponentVersion");
  TCHAR     _log_param1 [COMMON_LOGGER_PARAMETER_SIZE + 1];
  TCHAR     _full_component_name [COMMON_MAX_PATH_SIZE + 1];
  DWORD     _handle;
  DWORD     _version_info_size;
  BYTE      * p_version_info_data;
  BOOL      _rc_bool;
  WORD      _rc;
   
  // Format a full path component name name
  Version_FormatFullPathName (pModulePath, 
                              pComponentVersion->name, 
                              _full_component_name);

  // Get version info size
  _version_info_size = GetFileVersionInfoSize ((LPSTR) &_full_component_name, &_handle);
  if ( _version_info_size == 0 )
  {
    // No version info available
    // Write Logger
    _stprintf (_log_param1, _T("%lu"), _version_info_size);
    Common_LoggerVersion (VERSION_LOG_CODE_4, _log_param1, _T("GetFileVersionInfoSize"), pComponentVersion->name);

    return VERSION_STATUS_ERROR;
  } 

  // Get version info
  p_version_info_data = (BYTE *) malloc (_version_info_size);
  if ( p_version_info_data == NULL )
  {
    // Could not allocate memomry
    // Write Logger
    Common_LoggerVersion (VERSION_LOG_CODE_1, _T("NULL"), _T("malloc"), NULL);

    return VERSION_STATUS_ERROR;
  } // if

  // Get version info block
  _rc_bool = GetFileVersionInfo ((LPSTR) &_full_component_name,
                                 _handle,
                                 _version_info_size,
                                 p_version_info_data);
  if ( ! _rc_bool )
  {
    // No version info available
    // Write Logger
    _stprintf (_log_param1, _T("%hu"), _rc_bool);
    Common_LoggerVersion (VERSION_LOG_CODE_4, _log_param1, _T("GetFileVersionInfo"), pComponentVersion->name);
    
    free (p_version_info_data);

    return VERSION_STATUS_ERROR;
  } // if

  _rc = VERSION_STATUS_OK;

  // Parse different version items from version info block
  _rc_bool = Version_GetVersionInfo (p_version_info_data, 
                                     pComponentVersion);
  if ( ! _rc_bool )
  {
    // Error getting version info
    // Write Logger
    _stprintf (_log_param1, _T("%hu"), _rc_bool);
    Common_LoggerVersion (VERSION_LOG_CODE_4, _log_param1, _T("Version_GetVersionInfo"), pComponentVersion->name);

    _rc = VERSION_STATUS_ERROR;
  } // if

  free (p_version_info_data);

  return _rc;
} // Version_GetComponentVersion

//------------------------------------------------------------------------------
// PURPOSE : Parse different version data from version info data block 
// 
//  PARAMS :
//      - INPUT :
//          - pVersionInfo : Buffer containing the resource version info data
//
//      - OUTPUT : 
//          - pComponentVersion : Component version data
// 
// RETURNS : 
//      - TRUE on success
//      - FALSE on Error
// 
//   NOTES :

BOOL            Version_GetVersionInfo (BYTE                    * pVersionInfo, 
                                        TYPE_VERSION_COMPONENT  * pComponentVersion)
{
  TCHAR     _function_name [] = _T("Version_GetVersionInfo");
  TCHAR     _log_param1 [COMMON_LOGGER_PARAMETER_SIZE + 1];
  TCHAR     * p_data;
  BOOL      _rc_bool;

  // Get company name 
  _rc_bool = Version_GetVersionData (pVersionInfo, 
                                     VERSION_KEY_COMPANY_NAME,
                                     &p_data);
  if ( ! _rc_bool )
  {
    // Could not retrieve company name value
    _stprintf (_log_param1, _T("%hu"), _rc_bool);
    Common_LoggerVersion (VERSION_LOG_CODE_4, _log_param1, _T("Version_GetVersionData-CompanyName"), pComponentVersion->name);

    return FALSE;
  }  

  // Store company name in output structure
  _stprintf (pComponentVersion->company_name, _T("%.*s"), VERSION_LENGTH_COMPANY,
                                                          p_data);

  // Get copyright from version info data block
  _rc_bool = Version_GetVersionData (pVersionInfo, 
                                     VERSION_KEY_COPYRIGHT,
                                     &p_data);
  if ( ! _rc_bool )
  {
    // Could not retrieve copyright value
    // Write Logger
    _stprintf (_log_param1, _T("%hu"), _rc_bool);
    Common_LoggerVersion (VERSION_LOG_CODE_4, _log_param1, _T("Version_GetVersionData-CopyRight"), pComponentVersion->name);

    return FALSE;
  }  

  // Store copyright in output structure
  _stprintf (pComponentVersion->copyright, _T("%.*s"), VERSION_LENGTH_COPYRIGHT,
                                                       p_data);
  // Get product version from version info data block
  _rc_bool = Version_GetVersionData (pVersionInfo, 
                                     VERSION_KEY_PRODUCT_VERSION,
                                     &p_data);
  if ( ! _rc_bool )
  {
    // Could not retrieve product version value
    // Write Logger
    _stprintf (_log_param1, _T("%hu"), _rc_bool);
    Common_LoggerVersion (VERSION_LOG_CODE_4, _log_param1, _T("Version_GetVersionData-ProductVersion"), pComponentVersion->name);

    return FALSE;
  }  

  // Product version in component has the format BB.CCC to store:
  //    BB --> Client number
  //    CCC -> Component build number  
  //
  // The numeric values for client number (CC) and component build (BBB) must be extracted 
  // from respective version strings

  _rc_bool = Version_AssignVersionData (p_data, pComponentVersion);
  if ( ! _rc_bool )
  {
    // Could not assign client and build numbers or bad version string format in 
    // version resource (it should be "CC.BBB.DDD")
    _stprintf (_log_param1, _T("%hu"), _rc_bool);
    Common_LoggerVersion (VERSION_LOG_CODE_4, _log_param1, _T("Version_AssignVersionData"), pComponentVersion->name);

    return FALSE;
  } // if

  return TRUE;
} // Version_GetVersionInfo

//------------------------------------------------------------------------------
// PURPOSE : Get specific version data from version info data block 
// 
//  PARAMS :
//      - INPUT :
//          - pVersionInfo: Pointer to buffer containing the resource version
//                              info data
//          - pInfoKey: Key for the specific version info data
//
//      - OUTPUT : 
//          - pSpecificInfoData: Specific required data buffer
// 
// RETURNS : 
//          - TRUE on success
//          - FALSE on Error
// 
//   NOTES :

BOOL            Version_GetVersionData (BYTE    * pVersionInfo, 
                                        TCHAR   * pInfoKey,
                                        TCHAR   ** pSpecificInfoData)
{
  TCHAR     _function_name [] = _T("Version_GetVersionData");
  TCHAR     _log_param1 [COMMON_LOGGER_PARAMETER_SIZE + 1];
  TCHAR     _info_key [VERSION_LENGTH_STRING_KEY + 1];
  BOOL      _rc;
  DWORD     _info_value_size;
  
  _info_value_size = (DWORD) (VERSION_LENGTH_STRING_VALUE + 1);

  // Build info key parameter language NEUTRAL
  _stprintf (_info_key, _T("\\StringFileInfo\\%04x%04x\\%s"), VERSION_LANGUAGE_NEUTRAL,
                                                              VERSION_CHARACTERSET,
                                                              pInfoKey);

  // Get specific data
  _rc = VerQueryValue (pVersionInfo, 
                       _info_key, 
                       (LPVOID *) pSpecificInfoData,
                       (PUINT) &_info_value_size);

  if ( ! _rc )
  {
    // Could not retrieve specific data value
    // Write Logger
    _stprintf (_log_param1, _T("%hu"), _rc);
    Common_LoggerVersion (VERSION_LOG_CODE_1, _log_param1, _T("VerQueryValue"), NULL);

    return FALSE;
  }  

  return TRUE;
} // Version_GetVersionData

//------------------------------------------------------------------------------
// PURPOSE : Create formatted version string for component based on Client and Build.
// 
//  PARAMS :
//      - INPUT :
//          - pVersionData: Version string retrieved from component file
//                          (BB.CCC)
//
//      - OUTPUT : 
//          - pComponentVersion: Component version information 
//                               structure with new values added
// 
// RETURNS : 
//          - TRUE on success
//          - FALSE on Error
// 
//   NOTES :

BOOL            Version_AssignVersionData (TCHAR                  * pVersionData,
                                           TYPE_VERSION_COMPONENT * pComponentVersion)
{
  TCHAR     _function_name [] = _T("Version_AssignVersionData");
  TCHAR     _log_param1 [COMMON_LOGGER_PARAMETER_SIZE + 1];
  WORD      _rc;

  // Init database check flag for this component
  pComponentVersion->database_check = FALSE;

  // Extract client and build numbers
  // For component version, values will be stored in _found fields
  // For database version, values will be stored in _expected fields 
  // because _found values will be retrieved from database
  _rc = _stscanf (pVersionData, _T("%lu.%lu"), &pComponentVersion->found.client,
                                               &pComponentVersion->found.build);

  // Check if returned code is different than lower than 2 or greater than 3
  // because this function returns the number of items assigned.
  if ( _rc < 2 || _rc > 3 )
  {
    // Bad version string format
    // Write Logger
    _stprintf (_log_param1, _T("%hu"), _rc);
    Common_LoggerVersion (VERSION_LOG_CODE_4, _log_param1, _T("_stscanf"), pComponentVersion->name);

    return FALSE;
  } 

  // Check client version range
  // Note: A check should be done at string level because is possible to include in C product
  //       version characters different than numbers  
  if ( pComponentVersion->found.client > MAX_CLIENT_ID )
  {
    // The strings for VERSION_LOG_CODE_1 and VERSION_LOG_CODE_4 are identical 
    // in LKC and LKT, LKAS except the order of the parameters.
    _stprintf (_log_param1, _T("%lu"), pComponentVersion->found.client);
    Common_LoggerVersion (VERSION_LOG_CODE_4, _log_param1, _T("Client version"), pComponentVersion->name);

    return FALSE;  
  } // if

  // Check build version range
  // Note: A check should be done at string level because is possible to include in C product
  //       version characters different than numbers
  if ( pComponentVersion->found.build > MAX_BUILD_ID )
  {
    // The strings for VERSION_LOG_CODE_1 and VERSION_LOG_CODE_4 are identical 
    // in LKC and LKT, LKAS except the order of the parameters.
    _stprintf (_log_param1, _T("%lu"), pComponentVersion->found.build);
    Common_LoggerVersion (VERSION_LOG_CODE_4, _log_param1, _T("Build version"), pComponentVersion->name);

    return FALSE;
  } // if

  // Format database version string
  Version_FormatVersionString (pComponentVersion->found.client,
                               pComponentVersion->found.build,
                               pComponentVersion->found.version);

  return TRUE;
} // Version_AssignVersionData

//------------------------------------------------------------------------------
// PURPOSE : Check version of all module components
// 
//  PARAMS :
//      - INPUT :
//          - pModuleVersion: Module structure with all module components
//                            versions
//
//      - OUTPUT : 
// 
// RETURNS : 
//          - VERSION_STATUS_OK
//          - VERSION_STATUS_VERSION_ERROR
// 
//   NOTES :

WORD  Version_CheckVersions (TYPE_VERSION_MODULE * pModuleVersion)
{
  TCHAR                     _function_name [] = _T("Version_CheckVersions");
//  TCHAR                     _log_param1 [COMMON_LOGGER_PARAMETER_SIZE + 1];
  WORD                      _idx_component;
  WORD                      _rc;
  TYPE_VERSION_COMPONENT    * p_component;

  _rc = VERSION_STATUS_OK;

  // Check all module components version skipping first list component because
  // it corresponds to virtual module version.
  // For the rest of list components:
  //  - Check component Client and Build numbers (CC.BBB) (They must be identical)
  // Note: For Database version it will be controlled using the first component
  // (virtual module version)

  for ( _idx_component = 1; _idx_component < pModuleVersion->num_components; _idx_component++ )
  {
    // To easy read the code
    p_component = &pModuleVersion->component[_idx_component];

    //      - Check component client and build numbers (they must be identical)
    if (   p_component->expected.client !=  p_component->found.client
        || p_component->expected.build  !=  p_component->found.build )
    {
      // Versions mismatch 
      // Write logger
      Common_LoggerVersion (VERSION_LOG_CODE_5, p_component->name, 
                                                p_component->expected.version, 
                                                p_component->found.version);

      _rc = VERSION_STATUS_VERSION_ERROR;
    } // if
  } // for

  return _rc;
} // Version_CheckVersions

//------------------------------------------------------------------------------
// PURPOSE : Format version string for components from:
//           Client and Build.
// 
//  PARAMS :
//      - INPUT :
//          - Client : Client number
//          - Build : Build number
//
//      - OUTPUT : 
//          - pVersionString: Formatted version string CC.BBB
// 
// RETURNS : 
// 
//   NOTES :

void  Version_FormatVersionString (DWORD   Client,
                                   DWORD   Build,
                                   TCHAR  * pVersionString)
{
  // Database version number with value zero(0)
  _stprintf (pVersionString, VERSION_STRING_FORMAT, Client, Build);

  return;
} // Version_FormatVersionString

//------------------------------------------------------------------------------
// PURPOSE : Format Module Version string from: 
//           Client, Build, CommonDatabase and ClientDatabase.
// 
//  PARAMS :
//      - INPUT :
//          - Client : Client number
//          - Build : Build number
//          - CommonDatabase : Common Database number (ODB)
//          - ClientDatabase : Client Database number (LDB)
//
//      - OUTPUT : 
//          - pVersionString: Formatted version string CC.BBB.ODB.LDB
// 
// RETURNS : 
// 
//   NOTES :

void  Version_FormatModuleVersionString (DWORD   Client,
                                         DWORD   Build,
                                         DWORD   CommonDatabase,
                                         DWORD   ClientDatabase,
                                         TCHAR  * pVersionString)
{
  if ( ( CommonDatabase == 0 ) && ( ClientDatabase == 0 ) )
  {
    // Database version number higher than zero(0)
    _stprintf (pVersionString, VERSION_STRING_FORMAT, Client, Build);
  }
  else
  {
    // Database version number higher than zero(0)
    _stprintf (pVersionString, VERSION_STRING_FORMAT_MODULE_DB, Client, Build, CommonDatabase, ClientDatabase);
  }
  return;
} // Version_FormatModuleVersionString
