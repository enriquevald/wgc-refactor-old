//-----------------------------------------------------------------------------
// Copyright � 2004 Win Systems Ltd.
//-----------------------------------------------------------------------------
// 
//   MODULE NAME : CommonBaseMath.cpp
// 
//   DESCRIPTION : Math Functions and Methods.
// 
//        AUTHOR : Alberto Cuesta
// 
// CREATION DATE : 08-APR-2004
// 
// REVISION HISTORY
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 08-APR-2004 ACC    Initial draft.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// INCLUDES 
//------------------------------------------------------------------------------
#include "CommonDef.h"

//------------------------------------------------------------------------------
// PRIVATE CONSTANTS 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE DATATYPES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE DATA STRUCTURES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE FUNCTION PROTOTYPES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PUBLIC FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE : Truncs a double 
//
//  PARAMS :
//      - INPUT :
//          - ValueToTruncate
//          - Precission : number of decimals
//
//      - OUTPUT :
//
// RETURNS :
//      - Double truncated
//
//   NOTES :

COMMONBASE_API double WINAPI Common_TruncateDouble (double  ValueToTruncate,
                                                    int     Precission)
{
  // Use table-lookup for rounding/truncation.
  static const double _precission_table [] = { 10E-1, 10E-2, 10E-3, 10E-4, 10E-5, 10E-6, 10E-7};

  // Check precision 
  if ( Precission < 0 || Precission > 6 )
  {
    return ValueToTruncate;
  }

  return floor (ValueToTruncate / _precission_table[Precission]) * _precission_table[Precission];
} // Common_TruncateDouble 

//------------------------------------------------------------------------------
// PURPOSE : Rounds a double
//
//  PARAMS :
//      - INPUT :
//          - ValueToRound
//          - Precission : number of decimals
//
//      - OUTPUT :
//
// RETURNS :
//      - Double rounded to 2 decs
//
//   NOTES :

COMMONBASE_API double WINAPI Common_RoundDouble (double     ValueToRound,
                                                 int        Precission)
{
  // Use table-lookup for rounding/truncation.
  static const double _precission_table [] = { 1E0, 1E+1, 1E+2, 1E+3, 1E+4, 1E+5, 1E+6};

  // Check precision 
  if ( Precission < 0 || Precission > 6 )
  {
    return ValueToRound;
  }

  return floor (ValueToRound * _precission_table[Precission] + (double) 0.50) / _precission_table[Precission];
} // Common_RoundDouble

