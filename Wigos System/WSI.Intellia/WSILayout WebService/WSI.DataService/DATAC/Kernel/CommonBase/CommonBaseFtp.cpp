//-----------------------------------------------------------------------------
// Copyright � 2008 Win Systems Ltd.
//-----------------------------------------------------------------------------
// 
//   MODULE NAME: CommonBaseFtp.cpp
//   DESCRIPTION: Functions and Methods to transfer files through FTP protocol.
//        AUTHOR: Agust� Poch
// CREATION DATE: 21-OCT-2008
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 21-OCT-2008 APB    Initial draft.
// 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  INCLUDES 
//------------------------------------------------------------------------------

#define INCLUDE_COMMON_MISC
#define INCLUDE_NLS_API
#define INCLUDE_PRIVATE_LOG
#include "CommonDef.h"

#include <Wininet.h>

//------------------------------------------------------------------------------
//  PRIVATE CONSTANTS 
//------------------------------------------------------------------------------

#define MODULE_NAME                             _T("COMMONBASE")

#define COMMON_RELEASE_TEMP_PATH                _T("Release_Temp")
#define COMMON_RELEASE_CANDIDATE_PATH           _T("Release_Candidate")
#define COMMON_FTP_USER                         _T("PSAPubReader")
#define COMMON_FTP_PASSWORD                     _T("PSA14Universal")
#define COMMON_OUTDATED_PREFIX                  _T("_Outdated_")
#define COMMON_LOG_PREFIX                       _T("INFO: ")
#define COMMON_FTP_WIN_CASHIER_TYPE             0
#define COMMON_FTP_WIN_KIOSK_TYPE               1
#define COMMON_FTP_WIN_GUI_TYPE                 2
#define COMMON_MAX_PACKAGE_FILES                100           // Max number of files to be packed     

//------------------------------------------------------------------------------
//  PRIVATE DATATYPES 
//------------------------------------------------------------------------------

typedef struct
{
  DWORD     control_block;
  TCHAR     name [MAX_PATH];
  TCHAR     version [31+1];
  DWORD     size;

} TYPE_FILE_BASIC_INFO;

typedef struct
{
  DWORD   control_block;
  DWORD   client;
  DWORD   build;
  DWORD   terminal_type;
  TCHAR   repository1 [MAX_PATH];
  TCHAR   repository2 [MAX_PATH];

} TYPE_FTP_SOFT_PARAMS;

//------------------------------------------------------------------------------
//  PRIVATE DATA STRUCTURES 
//------------------------------------------------------------------------------

TCHAR     GLB_Processed_Files [COMMON_MAX_PACKAGE_FILES][MAX_PATH];
DWORD     GLB_Num_Processed_Files = 0;

//------------------------------------------------------------------------------
//  PRIVATE FUNCTION PROTOTYPES 
//------------------------------------------------------------------------------

static BOOL NewSoftVersionCheck (TCHAR * pPath);
static BOOL RemoveFilesInFolder (TCHAR FolderName[]);
static BOOL FtpGetAllFiles (TCHAR * pHost, TCHAR * pUser, TCHAR * pPassword, TCHAR * pSourceFolder, TCHAR * pTargetFolder);
static BOOL RollbackSwUpdate (TCHAR * pPath);
static BOOL DeleteBackupFiles (TCHAR * pPath);

//------------------------------------------------------------------------------
//  PUBLIC FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE : File copy through ftp
//
//  PARAMS :
//      - INPUT :
//          - pHost: ftp host
//          - pUser, pPassword: Credentials
//
//      - OUTPUT :
//          - pSessionHandle: Handle to the FTP session that must be passed to other calls
//          - pContext: not used
//
// RETURNS :
//
//   NOTES :

COMMONBASE_API BOOL WINAPI Common_FtpConnect (TCHAR * pHost, TCHAR * pUser, TCHAR * pPassword, DWORD * pSessionHandle, DWORD * pContext)
{
  TYPE_LOGGER_PARAM   _log_param_1;
  HINTERNET           _internet_handle;
  HINTERNET           _ftp_session;
  TCHAR             * p_port_str;
  TCHAR               _host[MAX_PATH];
  INTERNET_PORT       _ftp_port;

  _ftp_port = INTERNET_DEFAULT_FTP_PORT;
  strcpy (_host, pHost);
  p_port_str = strstr (_host, _T(":"));
  if ( p_port_str != NULL )
  {
    p_port_str[0] = 0;
    p_port_str++;
    _ftp_port = atoi (p_port_str);
    if ( _ftp_port == 0 )
    {
      _ftp_port = INTERNET_DEFAULT_FTP_PORT;
    } // if
  } // if
      
  _internet_handle = InternetOpen (__FUNCTION__,                // LPCTSTR lpszAgent,
                                   INTERNET_OPEN_TYPE_DIRECT,   // DWORD dwAccessType,
                                   NULL,                        // LPCTSTR lpszProxyName,
                                   NULL,                        // LPCTSTR lpszProxyBypass,
                                   0);                          // DWORD dwFlags
  if ( _internet_handle == NULL )
  {
    // Logger message
    _stprintf (_log_param_1, _T("%lu"), GetLastError ());
    PrivateLog (2, MODULE_NAME, __FUNCTION__, _log_param_1, _T("InternetOpen"));    

    return FALSE;
  } // if

  // Connect to ftp server
  * pContext = 0;
  _ftp_session = InternetConnect (_internet_handle,           // HINTERNET hInternet,
                                  _host,                      // LPCTSTR lpszServerName,
                                  _ftp_port,                  // INTERNET_PORT nServerPort,
                                  pUser,                      // LPCTSTR lpszUsername,
                                  pPassword,                  // LPCTSTR lpszPassword,
                                  INTERNET_SERVICE_FTP,       // DWORD dwService,
                                  0,                          // DWORD dwFlags,
                                  (DWORD_PTR) pContext);      // DWORD_PTR dwContext
  if ( _ftp_session == NULL )
  {
    // Logger message
    _stprintf (_log_param_1, _T("%lu"), GetLastError ());
    PrivateLog (2, MODULE_NAME, __FUNCTION__, _log_param_1, _T("InternetConnect"));    

    return FALSE;
  } // if

  // Prepare output params
  * pSessionHandle = (DWORD) _ftp_session;
    
  return TRUE;
  
} // Common_FtpConnect

//------------------------------------------------------------------------------
// PURPOSE : Prepare the specified path for the new package to be copied
//            - Check path exists
//            - Create it if it does not
//            - Delete all files in case it does
//            - Set it as the current path for the ftp session
//
//  PARAMS :
//      - INPUT :
//          - SessionHandle: Handle to the already established connection
//          - pPath: Path to set as current and create if necessary
//
//      - OUTPUT :
//
// RETURNS :
//      - TRUE : Successful
//      - FALSE : Otherwise
//
//   NOTES :

COMMONBASE_API BOOL WINAPI Common_FtpSetPath (DWORD SessionHandle, TCHAR * pPath)
{
  TYPE_LOGGER_PARAM   _log_param_1;
  BOOL                _bool_rc;
  DWORD               _error_code;
  HINTERNET           _find_handle;
  BYTE                _search_info_buff [sizeof(WIN32_FIND_DATA) + 128];
  WIN32_FIND_DATA   * p_find_data;
  TCHAR               _search_mask [MAX_PATH];
  TCHAR               _delete_mask [MAX_PATH];

  // Check path exists and create it if it does not
  // APB: Done this way to avoid find_data structure being corrupted when files are found
  p_find_data = (WIN32_FIND_DATA *) _search_info_buff;
  _stprintf (_search_mask, _T("%s"), pPath);
  _find_handle = FtpFindFirstFile((HINTERNET) SessionHandle,
                                  _search_mask,
                                  p_find_data,
                                  INTERNET_FLAG_DONT_CACHE,
                                  0);  

  if ( _find_handle == NULL )
  {
    _error_code = GetLastError();

    // ERROR_NO_MORE_FILES indicates path exists but no files were found
    if ( _error_code != ERROR_NO_MORE_FILES )
    {
      // Path does not exist it must be created
      _bool_rc = FtpCreateDirectory((HINTERNET) SessionHandle,    // HINTERNET hConnect
                                    pPath);                       // LPCTSTR lpszDirectory

      if ( ! _bool_rc )
      {
        // Directory not created: report error and exit
        // Logger message
        _stprintf (_log_param_1, _T("%lu"), GetLastError ());
        PrivateLog (2, MODULE_NAME, __FUNCTION__, _log_param_1, _T("FtpCreateDirectory"));    

        return FALSE;
      } // if
    } // if
  } // if
  else
  {
    // Path exists, it must be purged
    _bool_rc = TRUE;
    while ((_find_handle != NULL) && (_bool_rc))
    {
      _tmakepath(_delete_mask, _T(""), pPath, p_find_data->cFileName, _T(""));
      _bool_rc = FtpDeleteFile((HINTERNET) SessionHandle,       //  __in  HINTERNET hConnect,
                               _delete_mask);                   //  __in  LPCTSTR lpszFileName                                

      if (! _bool_rc)
      {
        // Logger message
        _stprintf (_log_param_1, _T("%lu"), GetLastError ());
        PrivateLog (2, MODULE_NAME, __FUNCTION__, _log_param_1, _T("FtpDeleteFile"));    
        
        return FALSE;
      } // if

      _bool_rc = InternetFindNextFile((HINTERNET) _find_handle,     //  __in   HINTERNET hFind,
                                      p_find_data);                 //  __out  LPVOID lpvFindData
                                     
    } // while
    _error_code = GetLastError();

    // Search handle must be freed so that another search can be started
    InternetCloseHandle(_find_handle);

    // ERROR_NO_MORE_FILES indicates path exists but no files were found
    if ( _error_code != ERROR_NO_MORE_FILES )
    {
      // Logger message
      _stprintf (_log_param_1, _T("%lu"), _error_code);
      PrivateLog (2, MODULE_NAME, __FUNCTION__, _log_param_1, _T("FtpFindFirstFile/FindNext"));    

      return FALSE;
    } // if

  } // else

  // Set current directory in FTP 
  _bool_rc = FtpSetCurrentDirectory ((HINTERNET) SessionHandle,   // HINTERNET hConnect,
                                     pPath);                      // LPCTSTR lpszDirectory
  if ( !_bool_rc )
  {
    // Logger message
    _stprintf (_log_param_1, _T("%lu"), GetLastError ());
    PrivateLog (2, MODULE_NAME, __FUNCTION__, _log_param_1, _T("FtpSetCurrentDirectory"));    

    return FALSE;
  } // if

  return TRUE;
} // Common_FtpSetCurrentPath

//------------------------------------------------------------------------------
// PURPOSE : Copy specified file to target location through Ftp session
//
//  PARAMS :
//      - INPUT :
//          - SessionHandle: Handle to the already established connection
//          - pPath: Path to set as current and create if necessary
//
//      - OUTPUT :
//
// RETURNS :
//      - TRUE : Successful
//      - FALSE : Otherwise
//
//   NOTES :

COMMONBASE_API BOOL WINAPI Common_FtpPutFile (DWORD SessionHandle, DWORD * pContext, TCHAR * pSourceFile)
{
  TYPE_LOGGER_PARAM   _log_param_1;
  BOOL                _bool_rc;
  TCHAR               _temp_file[] = _T("file.tmp");
  TCHAR               _aux_drive [MAX_PATH];
  TCHAR               _aux_path [MAX_PATH];
  TCHAR               _aux_name [MAX_PATH];
  TCHAR               _aux_ext [MAX_PATH];
  TCHAR               _target_file [MAX_PATH];

  // Strip path from source file to obtain real target file name
  _splitpath (pSourceFile, _aux_drive, _aux_path, _aux_name, _aux_ext);
  _stprintf (_target_file, _T("%s%s"), _aux_name, _aux_ext);

  // Copy file with a temporary name
  _bool_rc = FtpPutFile ((HINTERNET) SessionHandle,   // HINTERNET hConnect,
                         pSourceFile,                 // LPCTSTR lpszLocalFile,
                         _temp_file,                  // LPCTSTR lpszNewRemoteFile,
                         FTP_TRANSFER_TYPE_BINARY,    // DWORD dwFlags,
                         (DWORD_PTR) pContext);       // DWORD_PTR dwContext
  if ( !_bool_rc )
  {
    // Logger message
    _stprintf (_log_param_1, _T("%lu"), GetLastError ());
    PrivateLog (8, MODULE_NAME, __FUNCTION__, _log_param_1, _T("FtpPutFile"), _target_file);        

    return FALSE;
  } // if

  // Rename file
  _bool_rc = FtpRenameFile ((HINTERNET) SessionHandle,     // HINTERNET hConnect,
                            _temp_file,                     // LPCTSTR lpszExisting,
                            _target_file);                  // LPCTSTR lpszNew
  if ( !_bool_rc )
  {
    // Logger message
    _stprintf (_log_param_1, _T("%lu"), GetLastError ());
    PrivateLog (8, MODULE_NAME, __FUNCTION__, _log_param_1, _T("FtpRenameFile"), _target_file);        

    return FALSE;
  } // if

  return TRUE;
} // Common_FtpPutFile

//------------------------------------------------------------------------------
// PURPOSE : Prepare the specified path for the new package to be copied
//            - Check path exists
//            - Create it if it does not
//            - Delete all files in case it does
//            - Set it as the current path for the ft6p session
//
//  PARAMS :
//      - INPUT :
//          - SessionHandle: Handle to the already established connection
//          - pPath: Path to set as current and create if necessary
//
//      - OUTPUT :
//
// RETURNS :
//      - TRUE : Successful
//      - FALSE : Otherwise
//
//   NOTES :

COMMONBASE_API BOOL WINAPI Common_FtpGetFileDetails (DWORD SessionHandle, DWORD * pSearchHandle, TCHAR * pPath, TYPE_FILE_BASIC_INFO * pFileInfo)
{
  TYPE_LOGGER_PARAM   _log_param_1;
  BOOL                _bool_rc;
  DWORD               _error_code;
  HINTERNET           _find_handle;
  BYTE                _search_info_buff [sizeof(WIN32_FIND_DATA) + 128];
  WIN32_FIND_DATA   * p_find_data;
  TCHAR               _search_mask [MAX_PATH];

  memset (pFileInfo, 0, sizeof (TYPE_FILE_BASIC_INFO));

  // APB: Done this way to avoid find_data structure being corrupted when files are found
  p_find_data = (WIN32_FIND_DATA *) _search_info_buff;

  // Check if it is the first call: initialization is needed
  if ( * pSearchHandle == NULL)
  {
    _stprintf (_search_mask, _T("%s"), pPath);
    _find_handle = FtpFindFirstFile((HINTERNET) SessionHandle,
                                    _search_mask,
                                    p_find_data,
                                    INTERNET_FLAG_DONT_CACHE,
                                    0);
    if ( _find_handle == NULL )
    {
      _error_code = GetLastError();

      // ERROR_NO_MORE_FILES indicates no files were found
      if ( _error_code != ERROR_NO_MORE_FILES )
      {
        // Logger message
        _stprintf (_log_param_1, _T("%lu"), GetLastError ());
        PrivateLog (2, MODULE_NAME, __FUNCTION__, _log_param_1, _T("FtpFindFirstFile"));                
      } // if

      return FALSE;
    } // if
    else
    {
      // Return search handle for next queries
      * pSearchHandle = (DWORD) _find_handle;
    } // else
  } // if
  else
  {
    // Just return information for next file
    _bool_rc = InternetFindNextFile((HINTERNET) * pSearchHandle,  //  __in   HINTERNET hFind,
                                    p_find_data);                 //  __out  LPVOID lpvFindData

    if (! _bool_rc )
    {
      _error_code = GetLastError();

      // ERROR_NO_MORE_FILES indicates no files were found
      if ( _error_code != ERROR_NO_MORE_FILES )
      {
        // Logger message
        _stprintf (_log_param_1, _T("%lu"), _error_code);
        PrivateLog (2, MODULE_NAME, __FUNCTION__, _log_param_1, _T("InternetFindNextFile"));        
      } // if

      return FALSE;
    } // if
  } // else

  // Prepare output parameters  
  _stprintf (pFileInfo->name, _T("%s"), p_find_data->cFileName);
  _stprintf (pFileInfo->version, _T(""));
  pFileInfo->size = p_find_data->nFileSizeLow;

  return TRUE;
} // Common_FtpGetFileDetails

//------------------------------------------------------------------------------
// PURPOSE : Delete files within a specified FTP folder and optionally the folder itself
//
//  PARAMS :
//      - INPUT :
//          - SessionHandle : handle to the FTP connection
//          - pPath : Path to clean of temporary files
//          - DeleteDirectory : remove directory or not
//
//      - OUTPUT :
//        
// RETURNS :
//      - TRUE : Path has been cleaned
//      - FALSE : Path could not be cleaned
//
//   NOTES : 
//      Any existing subdirectories inside are NOT removed

COMMONBASE_API  BOOL WINAPI Common_CleanFtpPath (DWORD SessionHandle, TCHAR * pPath, BOOL DeleteDirectory)
{
  TYPE_LOGGER_PARAM   _log_param_1;
  BOOL                _bool_rc;
  DWORD               _error_code;
  HINTERNET           _find_handle;
  BYTE                _search_info_buff [sizeof(WIN32_FIND_DATA) + 128];
  WIN32_FIND_DATA   * p_find_data;
  TCHAR               _search_mask [MAX_PATH];
  TCHAR               _delete_mask [MAX_PATH];

  // Check path exists 
  // APB: Done this way to avoid find_data structure being corrupted when files are found
  p_find_data = (WIN32_FIND_DATA *) _search_info_buff;
  _stprintf (_search_mask, _T("%s\\*.*"), pPath);
  _find_handle = FtpFindFirstFile((HINTERNET) SessionHandle,
                                  _search_mask,
                                  p_find_data,
                                  INTERNET_FLAG_DONT_CACHE,
                                  0);
  if ( _find_handle == NULL )
  {
    _error_code = GetLastError();

    // ERROR_NO_MORE_FILES indicates path exists but no files were found
    if ( ( _error_code != ERROR_NO_MORE_FILES )
      && ( _error_code != ERROR_FILE_NOT_FOUND ) )
    {
      // Logger message
      _stprintf (_log_param_1, _T("%lu"), GetLastError ());
      PrivateLog (2, MODULE_NAME, __FUNCTION__, _log_param_1, _T("FtpFindFirstFile"));    

      return FALSE;
    } // if
  } // if
  else
  {
    // Path exists and it contains files
    _bool_rc = TRUE;
    while ((_find_handle != NULL) && (_bool_rc))
    {
      _tmakepath(_delete_mask, _T(""), pPath, p_find_data->cFileName, _T(""));
      _bool_rc = FtpDeleteFile((HINTERNET) SessionHandle,       //  __in  HINTERNET hConnect,
                               _delete_mask);                   //  __in  LPCTSTR lpszFileName                                

      if (! _bool_rc)
      {
        _error_code = GetLastError ();
        if ( _error_code != ERROR_FILE_NOT_FOUND )
        {
          // Logger message
          _stprintf (_log_param_1, _T("%lu"), _error_code);
          PrivateLog (2, MODULE_NAME, __FUNCTION__, _log_param_1, _T("FtpDeleteFile"));
          InternetCloseHandle(_find_handle);
          
          return FALSE;
        } // if
      } // if

      _bool_rc = InternetFindNextFile((HINTERNET) _find_handle,     //  __in   HINTERNET hFind,
                                      p_find_data);                 //  __out  LPVOID lpvFindData
                                     
    } // while
    _error_code = GetLastError();

    InternetCloseHandle(_find_handle);

    // ERROR_NO_MORE_FILES indicates path exists but no files were found
    if ( _error_code != ERROR_NO_MORE_FILES )
    {
      // Logger message
      _stprintf (_log_param_1, _T("%lu"), _error_code);
      PrivateLog (2, MODULE_NAME, __FUNCTION__, _log_param_1, _T("FtpFindFirstFile/FindNext"));  
      InternetCloseHandle(_find_handle);

      return FALSE;
    } // if
  } // else

  // Remove directory if requested to do so
  if ( DeleteDirectory )
  {
    _bool_rc = FtpRemoveDirectory((HINTERNET) SessionHandle,    //  __in  HINTERNET hConnect,
                                  pPath);                       //  __in  LPCTSTR lpszDirectory

    if (! _bool_rc)
    {
      // Logger message
      _stprintf (_log_param_1, _T("%lu"), GetLastError ());
      PrivateLog (2, MODULE_NAME, __FUNCTION__, _log_param_1, _T("FtpRemoveDirectory"));    
      
      return FALSE;
    } // if
  } // if

  return TRUE;
} // Common_CleanFtpPath

//------------------------------------------------------------------------------
// PURPOSE : Prepare the specified path for the new package to be copied
//            - Check path exists
//            - Create it if it does not
//            - Delete all files in case it does
//            - Set it as the current path for the ftp session
//
//  PARAMS :
//      - INPUT :
//          - SessionHandle: Handle to the already established connection
//          - pPath: Path to set as current and create if necessary
//
//      - OUTPUT :
//
// RETURNS :
//      - TRUE : Successful
//      - FALSE : Otherwise
//
//   NOTES :

COMMONBASE_API BOOL WINAPI Common_FtpRenamePath (DWORD SessionHandle, TCHAR * pSourcePath, TCHAR * pTargetPath)
{
  TYPE_LOGGER_PARAM   _log_param_1;
  BOOL                _bool_rc;
  HINTERNET           _find_handle;
  BYTE                _search_info_buff [sizeof(WIN32_FIND_DATA) + 128];
  WIN32_FIND_DATA   * p_find_data;
  TCHAR               _search_mask [MAX_PATH];

  // APB: Done this way to avoid find_data structure being corrupted when files are found
  p_find_data = (WIN32_FIND_DATA *) _search_info_buff;

  // Check that source path exists
  _stprintf (_search_mask, _T("%s"), pSourcePath);
  _find_handle = FtpFindFirstFile((HINTERNET) SessionHandle,
                                  _search_mask,
                                  p_find_data,
                                  INTERNET_FLAG_DONT_CACHE,
                                  0);  

  if ( _find_handle == NULL )
  {
    // _find_handle is NULL if:
    //  - Path does not exist
    //  - Path exists but contains no files
    //  - Miscellaneous error
    // In any case, process cannot continue

    return FALSE;
  } // if

  // Free search handle so that another oe can be created within the same FTP session
  InternetCloseHandle(_find_handle);

  // Make sure target path does not exist
  Common_CleanFtpPath (SessionHandle, pTargetPath, TRUE);

  // Finally rename the file
  _bool_rc = FtpRenameFile((HINTERNET) SessionHandle,   //__in  HINTERNET hConnect,
                           pSourcePath,                 //__in  LPCTSTR lpszExisting,
                           pTargetPath);                //__in  LPCTSTR lpszNew

  if (! _bool_rc)
  {
    // Logger message
    _stprintf (_log_param_1, _T("%lu"), GetLastError ());
    PrivateLog (2, MODULE_NAME, __FUNCTION__, _log_param_1, _T("FtpRenameFile"));    
    
    return FALSE;
  } // if

  return TRUE;
} // Common_FtpRenamePath

//------------------------------------------------------------------------------
// PURPOSE : Close the specified Ftp session
//
//  PARAMS :
//      - INPUT :
//          - SessionHandle: Handle to the already established connection
//
//      - OUTPUT :
//
// RETURNS :
//      - TRUE : Successful
//      - FALSE : Otherwise
//
//   NOTES :

COMMONBASE_API BOOL WINAPI Common_FtpDisconnect (DWORD SessionHandle)
{
  TYPE_LOGGER_PARAM   _log_param_1;
  BOOL                _bool_rc;

  _bool_rc = InternetCloseHandle ((HINTERNET) SessionHandle);      // HINTERNET hInternet
  if ( !_bool_rc )
  {
    // Logger message
    _stprintf (_log_param_1, _T("%lu"), GetLastError ());
    PrivateLog (2, MODULE_NAME, __FUNCTION__, _log_param_1, _T("InternetCloseHandle"));    

    return FALSE;
  } // if

  return TRUE;
} // Common_FtpDisconnect

//------------------------------------------------------------------------------
// PURPOSE : Processing for new soft is available: copying from ftp and version checking 
//
//  PARAMS :
//      - INPUT :
//
//      - OUTPUT :
//
// RETURNS :
//      - COMMON_OK    : New software copied, restart needed
//      - COMMON_ERROR : No new software available
//
//   NOTES :

COMMONBASE_API DWORD WINAPI Common_ProcessFtpSoft (TYPE_FTP_SOFT_PARAMS  * pFtpParameters)
{
  BOOL  _bool_rc;
  TCHAR _target_folder [MAX_PATH + 1];
  TCHAR _source_folder [MAX_PATH + 1];
  TCHAR _tmp1_folder   [MAX_PATH + 1];
  TCHAR _tmp2_folder   [MAX_PATH + 1];
  TCHAR * p_ftp_addr;
  DWORD _idx_try;
  BOOL  _new_release_available;
  TYPE_FTP_SOFT_PARAMS _ftp_soft_params;

  // Check control block
  if ( pFtpParameters->control_block != sizeof (TYPE_FTP_SOFT_PARAMS) )
  {
    return COMMON_ERROR_CONTROL_BLOCK;
  }

  __try
  {
    _ftp_soft_params = * ((TYPE_FTP_SOFT_PARAMS *) pFtpParameters);

    _new_release_available = FALSE;
    _tmakepath (_target_folder, NULL, COMMON_RELEASE_TEMP_PATH, NULL, NULL);
    _stprintf (_tmp1_folder, _T("%02d.%03d"), (int) _ftp_soft_params.client, (int) _ftp_soft_params.build);
    _stprintf (_tmp2_folder, _T("%03d"), (int) _ftp_soft_params.terminal_type);
    _tmakepath (_source_folder, NULL, _tmp2_folder, _tmp1_folder, NULL);

    for ( _idx_try = 0; _idx_try < 2; _idx_try++ )
    {
      // Remove final and temporay folder to store new files
      CreateDirectory (COMMON_RELEASE_TEMP_PATH, NULL);
      RemoveFilesInFolder (COMMON_RELEASE_TEMP_PATH);

      RemoveFilesInFolder (COMMON_RELEASE_CANDIDATE_PATH);
      RemoveDirectory (COMMON_RELEASE_CANDIDATE_PATH);

      // Copy soft from FTP to temp
      p_ftp_addr = ( _idx_try == 0 ) ? _ftp_soft_params.repository1 : _ftp_soft_params.repository2;
      if ( p_ftp_addr[0] == 0 )
      {
        // Empty ftp address
        continue;
      }

      _bool_rc = FtpGetAllFiles (p_ftp_addr,
                                 COMMON_FTP_USER, COMMON_FTP_PASSWORD, 
                                 _source_folder, 
                                 _target_folder);
      if ( !_bool_rc )
      {
        continue;
      }

      // Check software
      _bool_rc = NewSoftVersionCheck (_target_folder);
      if ( !_bool_rc )
      {
        continue;
      }

      // New release correct, rename temp folder to new release
      _bool_rc = MoveFile (COMMON_RELEASE_TEMP_PATH, COMMON_RELEASE_CANDIDATE_PATH);
      if ( !_bool_rc )
      {
        continue;
      }

      _new_release_available = TRUE;
      break;

    } // for

    // Remove temporary files
    RemoveFilesInFolder (COMMON_RELEASE_TEMP_PATH);
    RemoveDirectory (COMMON_RELEASE_TEMP_PATH);

    if ( !_new_release_available )
    {
      RemoveFilesInFolder (COMMON_RELEASE_CANDIDATE_PATH);
      RemoveDirectory (COMMON_RELEASE_CANDIDATE_PATH);

      return COMMON_ERROR;
    }

    return COMMON_OK;

  } // __try

  __finally
  {

    // Nothing

  } // __finally

} // ProcessFtpSoft

//------------------------------------------------------------------------------
// PURPOSE : Delete outdated files within a specified path
//
//  PARAMS :
//      - INPUT :
//          - pPath : Path to clean of outdated files
//
//      - OUTPUT :
//        
// RETURNS :
//      - TRUE : Outdated files have been cleaned
//      - FALSE : One or more files could not be deleted
//
//   NOTES : 
//      

COMMONBASE_API  BOOL WINAPI Common_CleanOutdatedFiles (TCHAR * pPath)
{
  TYPE_LOGGER_PARAM   _log_param_1;
  BOOL                _bool_rc;
  DWORD               _error_code;
  HANDLE              _find_handle;
  WIN32_FIND_DATA     _find_data;
  TCHAR               _search_mask [MAX_PATH];
  TCHAR               _delete_mask [MAX_PATH];

  // Check path exists 
  _stprintf (_search_mask, _T("%s\\%s*.*"), pPath, COMMON_OUTDATED_PREFIX);
  _find_handle = FindFirstFile(_search_mask, &_find_data);
  if ( _find_handle == INVALID_HANDLE_VALUE )
  {
    _error_code = GetLastError();

    // ERROR_NO_MORE_FILES indicates path exists but no files were found
    if ( ( _error_code != ERROR_NO_MORE_FILES )
      && ( _error_code != ERROR_FILE_NOT_FOUND )
      && ( _error_code != ERROR_PATH_NOT_FOUND ) )
    {
      // Logger message
      _stprintf (_log_param_1, _T("%lu"), GetLastError ());
      PrivateLog (2, MODULE_NAME, __FUNCTION__, _log_param_1, _T("FindFirstFile"));    

      return FALSE;
    } // if
  } // if
  else
  {
    // Path exists and it contains files
    _bool_rc = TRUE;
    while ((_find_handle != INVALID_HANDLE_VALUE) && (_bool_rc))
    {
      _tmakepath(_delete_mask, _T(""), pPath, _find_data.cFileName, _T(""));
      _bool_rc = DeleteFile(_delete_mask);                   

      if (! _bool_rc)
      {
        _error_code = GetLastError ();
        if ( _error_code != ERROR_FILE_NOT_FOUND )
        {
          // Logger message
          _stprintf (_log_param_1, _T("%lu"), _error_code);
          PrivateLog (2, MODULE_NAME, __FUNCTION__, _log_param_1, _T("DeleteFile"));
          FindClose(_find_handle);
          
          return FALSE;
        } // if
      } // if

      _bool_rc = FindNextFile(_find_handle, &_find_data);
                                     
    } // while
    _error_code = GetLastError();

    FindClose(_find_handle);

    // ERROR_NO_MORE_FILES indicates path exists but no files were found
    if ( _error_code != ERROR_NO_MORE_FILES )
    {
      // Logger message
      _stprintf (_log_param_1, _T("%lu"), _error_code);
      PrivateLog (2, MODULE_NAME, __FUNCTION__, _log_param_1, _T("FtpFindFirstFile/FindNext"));  
      FindClose(_find_handle);

      return FALSE;
    } // if
  } // else

  return TRUE;
} // Common_CleanOutdatedFiles

//------------------------------------------------------------------------------
// PURPOSE : Restart application
//
//  PARAMS :
//      - INPUT :
//          - pProgramToStart : Application fully qualified name
//
//      - OUTPUT :
//        
// RETURNS :
//
//   NOTES : 
//

COMMONBASE_API void WINAPI Common_RestartApplication (TCHAR * pProgramToStart)
{
  TYPE_LOGGER_PARAM   _log_param_1;
  BOOL                _bool_rc;
  DWORD               _error_code;
  STARTUPINFO         _startup_info;
  PROCESS_INFORMATION _process_information;

  // Leave a trace of what's going on
  PrivateLog (5, MODULE_NAME, __FUNCTION__, COMMON_LOG_PREFIX, _T("Restarting application"), _T(""), _T(""));

  memset (&_startup_info, 0, sizeof (_startup_info));
  _startup_info.cb = sizeof (_startup_info);
  memset (&_process_information, 0, sizeof (_process_information));
  _process_information.hProcess = INVALID_HANDLE_VALUE;
  _process_information.hThread  = INVALID_HANDLE_VALUE;

  _bool_rc = CreateProcess(pProgramToStart,                //__in_opt     LPCTSTR lpApplicationName,
                           NULL,                    //__inout_opt  LPTSTR lpCommandLine,
                           NULL,                    //__in_opt     LPSECURITY_ATTRIBUTES lpProcessAttributes,
                           NULL,                    //__in_opt     LPSECURITY_ATTRIBUTES lpThreadAttributes,
                           FALSE,                   //__in         BOOL bInheritHandles,
                           0,                       //__in         DWORD dwCreationFlags,
                           NULL,                    //__in_opt     LPVOID lpEnvironment,
                           NULL,                    //__in_opt     LPCTSTR lpCurrentDirectory,
                           &_startup_info,          //__in         LPSTARTUPINFO lpStartupInfo,
                           &_process_information    //__out        LPPROCESS_INFORMATION lpProcessInformation
                           );
  if ( ! _bool_rc )
  {
    _error_code = GetLastError ();

    // Logger message
    _stprintf (_log_param_1, _T("%lu"), _error_code);
    PrivateLog (2, MODULE_NAME, __FUNCTION__, _log_param_1, _T("CreateProcess"));

    return;
  } // if

  // Close application so that new instance can come up clean
  ExitProcess(0);

} // Common_RestartApplication

//------------------------------------------------------------------------------
// PURPOSE : Start copying new software version to execution path
//
//  PARAMS :
//      - INPUT :
//          - pPath : Path where the new software resides
//
//      - OUTPUT :
//        
// RETURNS :
//      - TRUE : Update was completed successfully
//      - FALSE : Update failed
//
//   NOTES : 
//      Processed files list stored in GLB_Processed_Files
//      Passing a null value for pExeName will cause the application to exit instead 
//    os restarting

COMMONBASE_API  BOOL WINAPI Common_NewReleaseCandidateInstalled (TCHAR * pPath)
{
  TYPE_LOGGER_PARAM   _log_param_1;
  BOOL                _bool_rc;
  BOOL                _do_rollback;
  DWORD               _error_code;
  HANDLE              _find_handle;
  WIN32_FIND_DATA     _find_data;
  TCHAR               _search_mask [MAX_PATH];
  TCHAR               _rc_path [MAX_PATH];
  TCHAR               _file_to_rename [MAX_PATH];
  TCHAR               _source_file [MAX_PATH];
  TCHAR               _backup_file [MAX_PATH];
  TCHAR               _backup_name [MAX_PATH];
  DWORD               _file_attributes;

  GLB_Num_Processed_Files = 0;
  _do_rollback = FALSE;

  // Remove any backup file from previous versions
  DeleteBackupFiles (pPath);

  // Check path exists 
  _stprintf (_rc_path, _T("%s\\%s"), pPath, COMMON_RELEASE_CANDIDATE_PATH);
  _stprintf (_search_mask, _T("%s\\*.*"), _rc_path);
  _find_handle = FindFirstFile(_search_mask, &_find_data);
  if ( _find_handle == INVALID_HANDLE_VALUE )
  {
    _error_code = GetLastError();

    // ERROR_NO_MORE_FILES indicates path exists but no files were found
    if ( ( _error_code != ERROR_NO_MORE_FILES )
      && ( _error_code != ERROR_FILE_NOT_FOUND )
      && ( _error_code != ERROR_PATH_NOT_FOUND) )
    {
      // Logger message
      _stprintf (_log_param_1, _T("%lu"), GetLastError ());
      PrivateLog (2, MODULE_NAME, __FUNCTION__, _log_param_1, _T("FindFirstFile"));    

      return FALSE;
    } // if
  } // if
  else
  {
    // Leave a trace of what's going on
    PrivateLog (5, MODULE_NAME, __FUNCTION__, COMMON_LOG_PREFIX, _T("Transferring updated files"), _T(""), _T(""));

    // Path exists and it contains files
    _bool_rc = TRUE;
    while ((_find_handle != INVALID_HANDLE_VALUE) && (_bool_rc))
    {
      if (   _tcscmp (_find_data.cFileName, _T(".")) != 0 
          && _tcscmp (_find_data.cFileName, _T("..")) != 0 )
      {

        // Clear read only attribute
        _file_attributes = GetFileAttributes (_find_data.cFileName); 
        if ( _file_attributes != INVALID_FILE_ATTRIBUTES )
        {
          if ( _file_attributes & FILE_ATTRIBUTE_READONLY ) 
          { 
            SetFileAttributes (_find_data.cFileName, _file_attributes & ~FILE_ATTRIBUTE_READONLY); 
          } // if
        } // if

        _tmakepath(_file_to_rename, _T(""), pPath, _find_data.cFileName, _T(""));
        _tmakepath(_source_file, _T(""), _rc_path, _find_data.cFileName, _T(""));
        _stprintf (_backup_name, _T("%s%s"), COMMON_OUTDATED_PREFIX, _find_data.cFileName);
        _tmakepath(_backup_file, _T(""), pPath, _backup_name, _T(""));

        _bool_rc = MoveFile(_file_to_rename, _backup_file);

        if (! _bool_rc)
        {
          _error_code = GetLastError ();
          if ( _error_code != ERROR_FILE_NOT_FOUND )
          {
            // Logger message
            _stprintf (_log_param_1, _T("%lu"), _error_code);
            PrivateLog (8, MODULE_NAME, __FUNCTION__, _log_param_1, _T("MoveFile"), _find_data.cFileName);
            
            // Abort update and roll back
            _do_rollback = TRUE;
            break;
          } // if
        } // if

        _bool_rc = MoveFile(_source_file, _file_to_rename);

        if (! _bool_rc)
        {
          _error_code = GetLastError ();

          // Logger message
          _stprintf (_log_param_1, _T("%lu"), _error_code);
          PrivateLog (8, MODULE_NAME, __FUNCTION__, _log_param_1, _T("MoveFile"), _find_data.cFileName);
            
          // Abort update and roll back
          _do_rollback = TRUE;
          break;
        } // if

        // Keep track of copied files
        _tcscpy (GLB_Processed_Files [GLB_Num_Processed_Files], _find_data.cFileName);
        GLB_Num_Processed_Files++;
      } // if

      _bool_rc = FindNextFile(_find_handle, &_find_data);
      if (! _bool_rc)
      {
        _error_code = GetLastError ();

        if ( _error_code != ERROR_NO_MORE_FILES )
        {
          // Logger message
          _stprintf (_log_param_1, _T("%lu"), _error_code);
          PrivateLog (2, MODULE_NAME, __FUNCTION__, _log_param_1, _T("FindNextFile"));
            
          // Abort update and roll back
          _do_rollback = TRUE;
          break;
        } // if
      } // if
    } // while

    FindClose(_find_handle);

    if ( _do_rollback )
    {
      PrivateLog (5, MODULE_NAME, __FUNCTION__, COMMON_LOG_PREFIX, _T("Update failed, rolling back"), _T(""), _T(""));

      // Try to restore previous version
      RollbackSwUpdate (pPath);

      // Clean path with temporary files
      RemoveFilesInFolder (_rc_path);
      RemoveDirectory (_rc_path);
    } // if
    else
    {
      // Check if something at all was processed 
      if ( GLB_Num_Processed_Files > 0 )
      {
        PrivateLog (5, MODULE_NAME, __FUNCTION__, COMMON_LOG_PREFIX, _T("Update completed; clean up and restart"), _T(""), _T(""));

        // Clean temporary backup files after successful update
        DeleteBackupFiles (pPath);

        // Clean path with temporary files
        RemoveFilesInFolder (_rc_path);
        RemoveDirectory (_rc_path);

        return TRUE;

      } // else
    } // else
  } // else  

  // Nothing at all was found, just exit
  return FALSE;
} // Common_ProcessReleaseCandidate

//------------------------------------------------------------------------------
// PURPOSE : Start copying new software version to execution path
//
//  PARAMS :
//      - INPUT :
//          - pPath : Path where the new software resides
//          - pExeName : application name, to be used for a restart
//
//      - OUTPUT :
//        
// RETURNS :
//      - TRUE : Update was completed successfully
//      - FALSE : Update failed
//
//   NOTES : 
//      Processed files list stored in GLB_Processed_Files
//      Passing a null value for pExeName will cause the application to exit instead 
//    os restarting

COMMONBASE_API  BOOL WINAPI Common_ProcessReleaseCandidate (TCHAR * pPath, TCHAR * pProgramToStart)
{
  if ( Common_NewReleaseCandidateInstalled (pPath) )
  {
    if (pProgramToStart != NULL)
    {
      Common_RestartApplication (pProgramToStart);
    }
    else
    {
      ExitProcess(0);
    }
    
    return TRUE;
  }

  return FALSE;
} // Common_ProcessReleaseCandidate


//------------------------------------------------------------------------------
//  PRIVATE FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE : Checks the version LKT module in a given path.
// 
//  PARAMS :
//      - INPUT :
//        - pPath: Software path
//
//      - OUTPUT : 
// 
// RETURNS : 
//      - On success: TRUE
//      - otherwise:  FALSE
//
//   NOTES :

static BOOL NewSoftVersionCheck (TCHAR * pPath)
{
  BOOL _bool_rc;
  TYPE_MODULE_ABOUT _module_about;

  memset (&_module_about, 0, sizeof (TYPE_MODULE_ABOUT));
  Common_SetCB (_module_about.control_block, TYPE_MODULE_ABOUT);

  _bool_rc = Version_CheckModuleVersion (COMMON_LKT_MODULE_NAME, pPath, &_module_about);

  return _bool_rc;

} // NewSoftVersionCheck

//------------------------------------------------------------------------------
// FUNCTION NAME: RemoveMultipleFiles
// 
//   DESCRIPTION: Removes files in a given folder
//
//    PARAMETERS:
//      - INPUT:
//          - FileName
//
//      - OUTPUT:
//
//    RETURN:
//
//    NOTES:
//
static BOOL RemoveFilesInFolder (TCHAR FolderName[])
{
  TYPE_LOGGER_PARAM _log_param1;
  WIN32_FIND_DATA   _find_file_data;
  TCHAR             _file_name[MAX_PATH];
  TCHAR             _file_mask[MAX_PATH];
  HANDLE _handle;
  DWORD _file_attributes;
  BOOL  _bool_rc;
  DWORD _sys_error;

  _tmakepath (_file_mask, NULL, FolderName, _T("*"), _T("*"));

  _handle = FindFirstFile (_file_mask,        // LPCTSTR lpFileName,
                           &_find_file_data); // LPWIN32_FIND_DATA lpFindFileData
  if ( _handle == INVALID_HANDLE_VALUE )
  {
    _sys_error = GetLastError ();
    if (   _sys_error != ERROR_NO_MORE_FILES
        && _sys_error != ERROR_PATH_NOT_FOUND )
    {
      // Error
      _stprintf (_log_param1, _T("%lu"), _sys_error);
      PrivateLog (2, MODULE_NAME, __FUNCTION__, _log_param1, _T("FindFirstFile"));    

      return FALSE;
    }

    // No files
    return TRUE;
  }

  __try
  {
    while ( TRUE )
    {
      // Skip . and ..
      if (   _tcscmp (_find_file_data.cFileName, _T(".")) != 0 
          && _tcscmp (_find_file_data.cFileName, _T("..")) != 0 )
      {
        _tmakepath (_file_name, NULL, FolderName, _find_file_data.cFileName, NULL);

        // Clear read only attribyte
        _file_attributes = GetFileAttributes (_file_name); 
        if ( _file_attributes != INVALID_FILE_ATTRIBUTES )
        {
          if ( _file_attributes & FILE_ATTRIBUTE_READONLY ) 
          { 
            SetFileAttributes (_file_name, _file_attributes & ~FILE_ATTRIBUTE_READONLY); 
          } 
        } 

        _bool_rc = DeleteFile (_file_name);
        if ( !_bool_rc )
        {
          _sys_error = GetLastError ();
          // Ignore error if it is "file not found" 
          if ( _sys_error != ERROR_FILE_NOT_FOUND )
          {
            _stprintf (_log_param1, _T("%lu"), _sys_error);
            PrivateLog (2, MODULE_NAME, __FUNCTION__, _log_param1, _T("DeleteFile"));    
          }
        }
      }

      _bool_rc = FindNextFile (_handle,           // HANDLE hFindFile,
                               &_find_file_data); // LPWIN32_FIND_DATA lpFindFileData
      if ( !_bool_rc )
      {
        _sys_error = GetLastError ();
        if ( _sys_error != ERROR_NO_MORE_FILES )
        {
          // Error
          _stprintf (_log_param1, _T("%lu"), _sys_error);
          PrivateLog (2, MODULE_NAME, __FUNCTION__, _log_param1, _T("FindNextFile"));    

          return FALSE;
        }
        // No more files
        break;;
      }
    } // while

    return TRUE;
  } // __try

  __finally
  {

    _bool_rc = FindClose (_handle);  // HANDLE hFindFile

  } // __finally

} // RemoveFilesInFolder

//------------------------------------------------------------------------------
// PURPOSE : File copy from ftp server
//
//  PARAMS :
//      - INPUT :
//          - pHost: ftp host
//          - pUser, pPassword: Credentials
//          - pSourceFolder, pTargetFolder
//
//      - OUTPUT :
//
// RETURNS :
//      - On success: TRUE
//      - otherwise:  FALSE
//
//   NOTES :

static BOOL FtpGetAllFiles (TCHAR * pHost, TCHAR * pUser, TCHAR * pPassword, TCHAR * pSourceFolder, TCHAR * pTargetFolder)
{
  TYPE_LOGGER_PARAM   _log_param1;
  BOOL                _bool_rc;
  BOOL                _files_to_copy;
  DWORD               _error;
  DWORD               _str_length;
  HINTERNET           _internet_handle    = NULL;
  HINTERNET           _ftp_session_handle = NULL;
  HINTERNET           _find_file_handle   = NULL;
  DWORD_PTR           _ftp_context = 0;
  DWORD_PTR           _ff_context = 0;
  INTERNET_PORT       _ftp_port;
  TCHAR               _error_text[1024];
  TCHAR               _source_mask[MAX_PATH];
  TCHAR               _source_file[MAX_PATH];
  TCHAR               _target_file[MAX_PATH];
  BYTE                _buffer_ffd[sizeof (WIN32_FIND_DATA) + 128]; // Buffer used instead of structure to avoid stack corruption after FtpFindFirstFile call. It seems a implementation problem in FtpFindFirstFile system call (possibly alignment mismatch).
  WIN32_FIND_DATA     * p_find_file_data = (WIN32_FIND_DATA *) _buffer_ffd;
  DWORD               _sys_error;

  _str_length = sizeof (_error_text) / sizeof (_error_text[0]);
  memset (_error_text, 0, sizeof (_error_text));
  _stprintf (_source_mask, _T("*.*"));
  _ftp_port = INTERNET_DEFAULT_FTP_PORT;
      
  __try
  {
    _internet_handle = InternetOpen (__FUNCTION__,                // LPCTSTR lpszAgent,
                                     INTERNET_OPEN_TYPE_DIRECT,   // DWORD dwAccessType,
                                     NULL,                        // LPCTSTR lpszProxyName,
                                     NULL,                        // LPCTSTR lpszProxyBypass,
                                     0);                          // DWORD dwFlags
    if ( _internet_handle == NULL )
    {
      // Error
      _stprintf (_log_param1, _T("%lu"), GetLastError ());
      InternetGetLastResponseInfo (&_error,       // LPDWORD lpdwError,
                                   _error_text,   // LPTSTR lpszBuffer,
                                   &_str_length); // LPDWORD lpdwBufferLength
      PrivateLog (2, MODULE_NAME, __FUNCTION__, _log_param1, _T("InternetOpen"));    

      return FALSE;
    }                                   

    // Connect to ftp server
    _ftp_session_handle = InternetConnect (_internet_handle,           // HINTERNET hInternet,
                                           pHost,                      // LPCTSTR lpszServerName,
                                           _ftp_port,                  // INTERNET_PORT nServerPort,
                                           pUser,                      // LPCTSTR lpszUsername,
                                           pPassword,                  // LPCTSTR lpszPassword,
                                           INTERNET_SERVICE_FTP,       // DWORD dwService,
                                           0,                          // DWORD dwFlags,
                                           _ftp_context);              // DWORD_PTR dwContext
    if ( _ftp_session_handle == NULL )
    {
      // Error
      _stprintf (_log_param1, _T("%lu"), GetLastError ());
      InternetGetLastResponseInfo (&_error,       // LPDWORD lpdwError,
                                   _error_text,   // LPTSTR lpszBuffer,
                                   &_str_length); // LPDWORD lpdwBufferLength
      PrivateLog (2, MODULE_NAME, __FUNCTION__, _log_param1, _T("InternetConnect"));    

      return FALSE;
    }                                   

    // Set current dir
    _bool_rc = FtpSetCurrentDirectory (_ftp_session_handle, // HINTERNET hConnect,
                                       pSourceFolder);      // LPCTSTR lpszDirectory
    if ( !_bool_rc )
    {
      // Error
      _stprintf (_log_param1, _T("%lu"), GetLastError ());
      InternetGetLastResponseInfo (&_error,       // LPDWORD lpdwError,
                                   _error_text,   // LPTSTR lpszBuffer,
                                   &_str_length); // LPDWORD lpdwBufferLength
      PrivateLog (2, MODULE_NAME, __FUNCTION__, _log_param1, _T("FtpSetCurrentDirectory"));    

      return FALSE;
    }
    
    _find_file_handle = FtpFindFirstFile (_ftp_session_handle,       // HINTERNET hConnect,
                                          _source_mask,              // LPCTSTR lpszSearchFile,
                                          p_find_file_data,          // LPWIN32_FIND_DATA lpFindFileData,
                                          INTERNET_FLAG_RELOAD,      // DWORD dwFlags,
                                          _ff_context);              // DWORD_PTR dwContext
    if ( _find_file_handle == NULL )
    {
      _files_to_copy = FALSE;
      _sys_error = GetLastError ();
      if ( _sys_error != ERROR_NO_MORE_FILES )
      {
        // Error
        _stprintf (_log_param1, _T("%lu"), _sys_error);
        InternetGetLastResponseInfo (&_error,       // LPDWORD lpdwError,
                                     _error_text,   // LPTSTR lpszBuffer,
                                     &_str_length); // LPDWORD lpdwBufferLength
        PrivateLog (2, MODULE_NAME, __FUNCTION__, _log_param1, _T("FtpFindFirstFile"));    

        return FALSE;
      }
    }
    else
    {
      _files_to_copy = TRUE;
    }

    while ( _files_to_copy )
    {
      // Skip . and ..
      if (   _tcscmp (p_find_file_data->cFileName, _T(".")) != 0 
          && _tcscmp (p_find_file_data->cFileName, _T("..")) != 0 )
      {
        // Copy file
        _tcscpy (_source_file, p_find_file_data->cFileName);
        _tmakepath (_target_file, NULL, pTargetFolder, _source_file, NULL);
        _bool_rc = FtpGetFile (_ftp_session_handle,         // HINTERNET hConnect,
                               _source_file,                // LPCTSTR lpszRemoteFile,
                               _target_file,                // LPCTSTR lpszNewFile,
                               FALSE,                       // BOOL fFailIfExists,
                               FILE_ATTRIBUTE_NORMAL,       // DWORD dwFlagsAndAttributes,
                               FTP_TRANSFER_TYPE_BINARY,    // DWORD dwFlags,
                               _ftp_context);               // DWORD_PTR dwContext
        if ( !_bool_rc )
        {
          // Error
          _stprintf (_log_param1, _T("%lu"), GetLastError ());
          InternetGetLastResponseInfo (&_error,       // LPDWORD lpdwError,
                                       _error_text,   // LPTSTR lpszBuffer,
                                       &_str_length); // LPDWORD lpdwBufferLength
          PrivateLog (2, MODULE_NAME, __FUNCTION__, _log_param1, _T("FtpGetFile"));    

          return FALSE;
        }
      }

      _files_to_copy = InternetFindNextFile (_find_file_handle, // HINTERNET hFind,
                                             p_find_file_data); // LPVOID lpvFindData
      if ( !_files_to_copy )
      {
        _sys_error = GetLastError ();
        if ( _sys_error != ERROR_NO_MORE_FILES )
        {
          // Error
          _stprintf (_log_param1, _T("%lu"), _sys_error);
          InternetGetLastResponseInfo (&_error,       // LPDWORD lpdwError,
                                       _error_text,   // LPTSTR lpszBuffer,
                                       &_str_length); // LPDWORD lpdwBufferLength
          PrivateLog (2, MODULE_NAME, __FUNCTION__, _log_param1, _T("InternetFindNextFile"));    

          return FALSE;
        }
      }

    } // while

    return TRUE;
  
  } // __try

  __finally
  {
    if ( _find_file_handle != NULL )
    {
      _bool_rc = InternetCloseHandle (_find_file_handle);     // HINTERNET hInternet
    }
    if ( _ftp_session_handle != NULL )
    {
      _bool_rc = InternetCloseHandle (_ftp_session_handle);   // HINTERNET hInternet
    }
    if ( _internet_handle != NULL )
    {
      _bool_rc = InternetCloseHandle (_internet_handle);      // HINTERNET hInternet
    }
  } // __finally
    
} // FtpGetAllFiles

//------------------------------------------------------------------------------
// PURPOSE : Roll back an incomplete software update
//
//  PARAMS :
//      - INPUT :
//          - pPath : Path where the software is executing
//
//      - OUTPUT :
//        
// RETURNS :
//      - TRUE : Roll back was completed successfully
//      - FALSE : Roll back failed
//
//   NOTES : 
//      File list taken from GLB_Processed_Files

static BOOL RollbackSwUpdate (TCHAR * pPath)
{
  TYPE_LOGGER_PARAM   _log_param_1;
  BOOL                _bool_rc;
  DWORD               _error_code;
  TCHAR               _base_file [MAX_PATH];
  TCHAR               _backup_file [MAX_PATH];
  TCHAR               _backup_name [MAX_PATH];
  DWORD               _idx_file;
  BOOL                _completion_flag;

  _completion_flag = TRUE;

  // Check if there is something to roll back
  if ( GLB_Num_Processed_Files > 0 )
  {
    for ( _idx_file = 0; _idx_file < GLB_Num_Processed_Files; _idx_file++ )
    {
      _tmakepath(_base_file, _T(""), pPath, GLB_Processed_Files [_idx_file], _T(""));
      _stprintf (_backup_name, _T("%s%s"), COMMON_OUTDATED_PREFIX, GLB_Processed_Files [_idx_file]);
      _tmakepath(_backup_file, _T(""), pPath, _backup_name, _T(""));

      _bool_rc = DeleteFile(_base_file);
      if (! _bool_rc)
      {
        _error_code = GetLastError ();

        // Logger message
        _stprintf (_log_param_1, _T("%lu"), _error_code);
        PrivateLog (8, MODULE_NAME, __FUNCTION__, _log_param_1, _T("DeleteFile"), _base_file);  

        _completion_flag = FALSE;
      } // if

      _bool_rc = MoveFile(_backup_file, _base_file);
      if (! _bool_rc)
      {
        _error_code = GetLastError ();
        if ( _error_code != ERROR_FILE_NOT_FOUND )
        {
          // Logger message
          _stprintf (_log_param_1, _T("%lu"), _error_code);
          PrivateLog (8, MODULE_NAME, __FUNCTION__, _log_param_1, _T("MoveFile"), _base_file);          

          _completion_flag = FALSE;
        } // if
      } // if
    } // for
  } // if

  return _completion_flag;
} // RollbackSwUpdate

//------------------------------------------------------------------------------
// PURPOSE : Delete file backed up from previous versions
//
//  PARAMS :
//      - INPUT :
//          - pPath : Path where the software is executing
//
//      - OUTPUT :
//        
// RETURNS :
//      - TRUE : Roll back was completed successfully
//      - FALSE : Roll back failed
//
//   NOTES : 
//      Backup files have a prefix that is used to identify them

static BOOL DeleteBackupFiles (TCHAR * pPath)
{
  TYPE_LOGGER_PARAM   _log_param1;
  WIN32_FIND_DATA     _find_file_data;
  TCHAR               _file_name[MAX_PATH];
  TCHAR               _file_mask[MAX_PATH];
  TCHAR               _backup_file_name[MAX_PATH];
  HANDLE              _handle;
  DWORD               _file_attributes;
  BOOL                _bool_rc;
  DWORD               _sys_error;

  _stprintf (_backup_file_name, _T("%s*"), COMMON_OUTDATED_PREFIX);
  _tmakepath (_file_mask, NULL, pPath, _backup_file_name, _T("*"));

  _handle = FindFirstFile (_file_mask,        // LPCTSTR lpFileName,
                           &_find_file_data); // LPWIN32_FIND_DATA lpFindFileData
  if ( _handle == INVALID_HANDLE_VALUE )
  {
    _sys_error = GetLastError ();
    if (   _sys_error != ERROR_NO_MORE_FILES
        && _sys_error != ERROR_FILE_NOT_FOUND
        && _sys_error != ERROR_PATH_NOT_FOUND )
    {
      // Error
      _stprintf (_log_param1, _T("%lu"), _sys_error);
      PrivateLog (2, MODULE_NAME, __FUNCTION__, _log_param1, _T("FindFirstFile"));    

      return FALSE;
    } // if

    // No files
    return TRUE;
  } // if

  __try
  {
    while ( TRUE )
    {
      // Skip . and ..
      if (   _tcscmp (_find_file_data.cFileName, _T(".")) != 0 
          && _tcscmp (_find_file_data.cFileName, _T("..")) != 0 )
      {
        _tmakepath (_file_name, NULL, pPath, _find_file_data.cFileName, NULL);

        // Clear read only attribyte
        _file_attributes = GetFileAttributes (_file_name); 
        if ( _file_attributes != INVALID_FILE_ATTRIBUTES )
        {
          if ( _file_attributes & FILE_ATTRIBUTE_READONLY ) 
          { 
            SetFileAttributes (_file_name, _file_attributes & ~FILE_ATTRIBUTE_READONLY); 
          } // if
        } // if

        _bool_rc = DeleteFile (_file_name);
        if ( !_bool_rc )
        {
          _sys_error = GetLastError ();
          // Ignore error if it is "file not found" 
          if ( _sys_error != ERROR_FILE_NOT_FOUND )
          {
            _stprintf (_log_param1, _T("%lu"), _sys_error);
            PrivateLog (8, MODULE_NAME, __FUNCTION__, _log_param1, _T("DeleteFile"), _file_name); 
          } // if
        } // if
      } // if

      _bool_rc = FindNextFile (_handle,           // HANDLE hFindFile,
                               &_find_file_data); // LPWIN32_FIND_DATA lpFindFileData
      if ( !_bool_rc )
      {
        _sys_error = GetLastError ();
        if ( _sys_error != ERROR_NO_MORE_FILES )
        {
          // Error
          _stprintf (_log_param1, _T("%lu"), _sys_error);
          PrivateLog (2, MODULE_NAME, __FUNCTION__, _log_param1, _T("FindNextFile"));    

          return FALSE;
        } // if
        // No more files
        break;;
      } // if
    } // while

    return TRUE;
  } // __try

  __finally
  {

    _bool_rc = FindClose (_handle);  // HANDLE hFindFile

  } // __finally
} // DeleteBackupFiles
