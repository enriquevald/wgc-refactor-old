//-----------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//-----------------------------------------------------------------------------
// 
//   MODULE NAME : CommonBaseDates.cpp
//
//   DESCRIPTION : Functions and Methods of dates to handle the CommonBase dll.
//
//        AUTHOR : Toni Jord�
//
// CREATION DATE : 17-JUN-2005
// 
// REVISION HISTORY :
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 17-JUN-2005 TJG    Initial draft.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// INCLUDES 
//------------------------------------------------------------------------------
#include "CommonDef.h"

//------------------------------------------------------------------------------
// PRIVATE CONSTANTS 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE DATATYPES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE DATA STRUCTURES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE FUNCTION PROTOTYPES 
//------------------------------------------------------------------------------
COMMONBASE_API DWORD WINAPI GetLocaleNumberFormat (NUMBERFMT * pNumberFormat);

//------------------------------------------------------------------------------
// PRIVATE FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE : Formats a number.
// 
//  PARAMS :
//      - INPUT :
//          - pNumberFormat
//
//      - OUTPUT : 
//          - pNumberFormat
// 
// RETURNS :
//      - COMMON_OK
//      - COMMON_ERROR
// 
//   NOTES :

COMMONBASE_API DWORD WINAPI GetLocaleNumberFormat (NUMBERFMT * pNumberFormat)
{
  TCHAR         _str_aux [256];

  GetLocaleInfo (LOCALE_USER_DEFAULT, LOCALE_IDIGITS, _str_aux,  Common_StringSize (_str_aux));
  pNumberFormat->NumDigits = _ttoi (_str_aux);

  GetLocaleInfo (LOCALE_USER_DEFAULT, LOCALE_ILZERO, _str_aux,  Common_StringSize (_str_aux)); 
  pNumberFormat->LeadingZero = _ttoi (_str_aux);

  GetLocaleInfo (LOCALE_USER_DEFAULT, LOCALE_SDECIMAL, _str_aux, Common_StringSize (_str_aux)); 
  _tcscpy (pNumberFormat->lpDecimalSep, _str_aux);

  GetLocaleInfo (LOCALE_USER_DEFAULT, LOCALE_STHOUSAND, _str_aux, Common_StringSize (_str_aux)); 
  _tcscpy (pNumberFormat->lpThousandSep, _str_aux);

  GetLocaleInfo (LOCALE_USER_DEFAULT, LOCALE_INEGSIGNPOSN, _str_aux,  Common_StringSize (_str_aux));
  pNumberFormat->NegativeOrder = _ttoi (_str_aux);

  GetLocaleInfo (LOCALE_USER_DEFAULT, LOCALE_SGROUPING, _str_aux,  Common_StringSize (_str_aux));
  pNumberFormat->Grouping = _ttoi (_str_aux);

  return COMMON_OK;  
} // Common_FormatNumber

//------------------------------------------------------------------------------
// PUBLIC FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE : Formats a number.
// 
//  PARAMS :
//      - INPUT :
//          - Number : Number to format
//          - FormattedNumberLength
//
//          - DecimalDigits :   -1 = Take user's default
//                               0 = No decimal digits
//                            1..N = Number of decimal digits
//
//          - GroupDigits :     -1 = Take user's default
//                               0 = Do NOT group digits
//                               1 = Group digits
//
//      - OUTPUT : 
//          - pFormattedNumber
// 
// RETURNS :
//      - COMMON_OK
//      - COMMON_ERROR
// 
//   NOTES :

COMMONBASE_API DWORD WINAPI Common_FormatNumber (double Number,
                                                 TCHAR  * pFormattedNumber,
                                                 WORD   FormattedNumberLength,
                                                 INT    DecimalDigits,
                                                 INT    GroupDigits)
{
  NUMBERFMT _number_format;
  TCHAR     _decimal_sep [5];
  TCHAR     _thousand_sep [5];
  TCHAR     _str_aux [256];

  memset (pFormattedNumber, 0, FormattedNumberLength * sizeof (TCHAR));
  // Can not use DecimalDigits because it can be -1. Use 4 as maximum number of decimals.
  if ( _sntprintf (pFormattedNumber, FormattedNumberLength - 1, _T("%.*f"), 4, Number) <= 0 )
  {
    return COMMON_ERROR;
  }

  // Get locale settings
  _number_format.lpDecimalSep  = _decimal_sep;
  _number_format.lpThousandSep = _thousand_sep;
  GetLocaleNumberFormat (&_number_format);

  // Number of decimals
  if ( DecimalDigits != -1 )
  {
    // Do not use user's default
    _number_format.NumDigits = DecimalDigits;   
  }

  // Grouping
  if ( GroupDigits != -1 )
  {
    // Do not use user's default
    if ( GroupDigits == 0 )
    {
      // Do not group digits
      _number_format.Grouping = 0;
    }
    else
    {
      // Force group digits using the user's default (if any)
      if ( _number_format.Grouping == 0 )
      {
        _number_format.Grouping = 3;
      }
    }
  }

  if ( GetNumberFormat (LOCALE_USER_DEFAULT,
                        0,
                        pFormattedNumber,
                        &_number_format,
                        _str_aux,
                        FormattedNumberLength) == 0 )
  {
    return COMMON_ERROR;
  }

  _stprintf (pFormattedNumber, _T("%s"), _str_aux);

  return COMMON_OK;  
} // Common_FormatNumber
