//-----------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//-----------------------------------------------------------------------------
// 
//   MODULE NAME: CommonBaseDates.cpp
//
//   DESCRIPTION: Functions and Methods of dates to handle the CommonBase dll.
//
//        AUTHOR: Alberto Cuesta
//
// CREATION DATE: 25-MAR-2002
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 25-MAR-2002 ACC    Initial draft.
// 12-DEC-2003 IRP    Change All Function from CTime To SystemTime
// 23-FEB-2004 RRT    Included function to get difference based on GetTickCount
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// INCLUDES 
//------------------------------------------------------------------------------
#include "CommonDef.h"

//------------------------------------------------------------------------------
// PRIVATE CONSTANTS 
//------------------------------------------------------------------------------

// Frequency
#define FREQ_ONCE            0x1000
#define FREQ_HOURLY          0x2000
#define FREQ_DAILY           0x3000
#define FREQ_WEEKLY          0x4000
#define FREQ_MONTHLY         0x5000
#define FREQ_MINUTELY        0x6000
#define FREQ_SYSTEM          0xE000

// Week Days
#define FREQ_MONDAY          0x01
#define FREQ_TUESDAY         0x02
#define FREQ_WEDNESDAY       0x04
#define FREQ_THURSDAY        0x08
#define FREQ_FRIDAY          0x10
#define FREQ_SATURDAY        0x20
#define FREQ_SUNDAY          0x40

// Month Option Selected
#define FREQ_MONTH_DAY       0x20
#define FREQ_MONTH_FIRST     0x40
#define FREQ_MONTH_LAST      0x80

// Frequency Mask
#define FREQ_MASK_FREQUENCY  0xFF00
#define FREQ_MASK_WEEKDAYS   0xFF
#define FREQ_MASK_MONTH_OPT  0xE0
#define FREQ_MASK_MONTH_DAY  0x1F

#define DAYS_IN_A_WEEK       7
#define ERROR_MKTIME         -1

// Constants to transform to MICRO Seconds
#define MILI_SECOND                10000   //10^3 
#define SECOND                  10000000   //10^6 
#define MINUTE                 600000000   //60 * SECOND 
#define HOUR                 36000000000   //60 * 60 * SECOND
#define DAY                 864000000000   //60 * 60 * 24 * SECOND

// Dates
#define STR_DB_DATE_NULL    "01/01/1900 00:00:00"
#define STR_DB_DATE_FORMAT  "%02lu %02lu %04lu %02lu %02lu %02lu"

//------------------------------------------------------------------------------
// PRIVATE DATATYPES 
//------------------------------------------------------------------------------
typedef enum 
{ 
  ADD_MINUTE,
  ADD_HOUR,
  ADD_DAY

} TYPE_ADD_TO_SYSTEMTIME;

//------------------------------------------------------------------------------
// PRIVATE DATA STRUCTURES 
//------------------------------------------------------------------------------
// 21-JUN-2002 AJQ    TYPE_DB_DATE_TIME
typedef struct                // This type is equivalent to (Pro*C)
{                             //
  short         year;         //  typedef struct
  unsigned char month;        //  {
  unsigned char day;          //    OCIDate oci_date;
  unsigned char hour;         //    char    filler;   
  unsigned char minute;       //  
  unsigned char second;       //  } TYPE_DB_DATE_TIME;
  unsigned char filler;
  unsigned long is_null;

} TYPE_DB_DATE_TIME;

//------------------------------------------------------------------------------
// PRIVATE FUNCTION PROTOTYPES 
//------------------------------------------------------------------------------
BOOL ComputeNextWeekDay (DWORD EnabledWeekDays, 
                         DWORD WeekStep, 
                         WORD  WeekDay, 
                         WORD  * pNextWeekDay, 
                         DWORD * pNumDays);

BOOL GetSameMonthLastDay (WORD Year, WORD Month, WORD *pLastDay);

BOOL DateTimeToSystemTime (TYPE_DATE_TIME * pIn, SYSTEMTIME * pOut);

BOOL SystemTimeToDateTime (SYSTEMTIME * pIn, TYPE_DATE_TIME * pOut);

BOOL AddToSystemTime (SYSTEMTIME              * pSystemTime, 
                      TYPE_ADD_TO_SYSTEMTIME  TypeToAdd, 
                      DWORD                   NumberToAdd);

BOOL CompareSystemTime (SYSTEMTIME * pSystemTime1, SYSTEMTIME * pSystemTime2, int * pResult);

BOOL CompareDateTime (TYPE_DATE_TIME * pDateTime1, TYPE_DATE_TIME * pDateTime2, int * pResult);

BOOL ComputeNextRunAt (DWORD            Frequency, 
                       DWORD            FrequencyStep, 
                       TYPE_DATE_TIME   * pRunAt, 
                       TYPE_DATE_TIME   * pDateNow, 
                       TYPE_DATE_TIME   * pNextRunAt);

//------------------------------------------------------------------------------
// PUBLIC FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE : Converts a TYPE_DATE_TIME structure into a C time number (t_time).
//          A C time number contents seconds elapsed since January 1.
// 
//  PARAMS :
//      - INPUT :
//          - TYPE_DATE_TIME: time structure
//
//      - OUTPUT : 
//          - pCTime: time in C format
// 
// RETURNS : None
// 
//   NOTES :

COMMONBASE_API time_t WINAPI Common_DateTimeToCTime (TYPE_DATE_TIME   * pDateTime)
{
  SYSTEMTIME _sys_time;
  DWORD      _answer;

  if ( DateTimeToSystemTime (pDateTime, &_sys_time) == FALSE )
  {
    return -1;
  }

  Common_SystemTimeToCTime (&_sys_time, &_answer);
                                                 
  return _answer;
} //Common_DateTimeToCTime

//-----------------------------------------------------------------------------
// PURPOSE : Turn time from time_t (in Local Time) format to DateTime 
//
//  PARAMS :
//     - INPUT :
//           Time     : Time in time_t format (seconds elapsed from 1/1/70) ( Local Time )
//           Local    : Should the response be in local or GMT time
//     - OUTPUT :
//          pDateTime : Time in DateTime format (on error IsNull is set to TRUE)
//
// RETURNS :
//
// NOTES :
//
COMMONBASE_API VOID Common_CTimeToDateTime (time_t Time, TYPE_DATE_TIME *pDateTime, BOOL Local)
{
  SYSTEMTIME              _in_time;
  SYSTEMTIME              _out_time;
  DWORD                   _rc;
  TIME_ZONE_INFORMATION   _zone_info;
  BOOL                    _rc_bool;

  // Check parameter
  if ( pDateTime == NULL )
  {
    return;
  }

  Common_CTimeToSystemTime (Time, &_in_time);


  _rc = GetTimeZoneInformation (&_zone_info);
  if ( _rc == TIME_ZONE_ID_INVALID )
  {
    pDateTime->is_null = TRUE;
    
    return;
  }

  // Check if local or GMT time requested
  if ( Local )
  {
    // Local
    _out_time = _in_time;
  } 
  else
  {
    // GMT
    _rc_bool = TzSpecificLocalTimeToSystemTime (&_zone_info,    // Time Zone Info
                                                &_in_time,      // Local Time
                                                &_out_time);    // UTC
  } // if

  if ( _rc_bool = FALSE ) 
  {
    pDateTime->is_null = TRUE;

    return;
  }
  _rc_bool = SystemTimeToDateTime (&_out_time, pDateTime);
  if ( _rc_bool == FALSE )
  {
    pDateTime->is_null = TRUE;
    
    return;
  }

  return;
} // Common_CTimeToDateTime

//------------------------------------------------------------------------------
// PURPOSE : Formats received date-time
//
//  PARAMS :
//      - INPUT :
//          - pDateTime : Date-Time to format
//          - TimeFormat : 
//              - COMMON_TIME_FORMAT_HM
//              - COMMON_TIME_FORMAT_HMS
//              - COMMON_TIME_FORMAT_HMSC
//
//      - OUTPUT :
//          - pFormattedDate : formatted date string. Size nust be COMMON_MAX_SIZE_DATE + 1
//          - pFormattedTime : formatted time string. Size nust be COMMON_SIZE_TIME + 1
//
// RETURNS : None
//
//   NOTES :

COMMONBASE_API void WINAPI Common_FormatDateTime (SYSTEMTIME    * pDateTime,
                                                  WORD          TimeFormat,
                                                  TCHAR         * pFormattedDate,
                                                  TCHAR         * pFormattedTime)
{
  _tcscpy (pFormattedDate, _T(""));
  _tcscpy (pFormattedTime, _T(""));

  // Date format
  if ( GetDateFormat (LOCALE_USER_DEFAULT,          // Locale
                      DATE_SHORTDATE,               // Short date format
                      pDateTime,                    // Date-Time
                      NULL,                         // User defined date format
                      pFormattedDate,               // Formatted string
                      COMMON_MAX_SIZE_DATE) == 0 )  // Size of formatted string
  {
    return;
  }

  // Time format
  switch ( TimeFormat )
  {
    case COMMON_TIME_FORMAT_HMS :
      _stprintf (pFormattedTime, _T("%02hu:%02hu:%02hu"), pDateTime->wHour,
                                                          pDateTime->wMinute,
                                                          pDateTime->wSecond);
    break;

    case COMMON_TIME_FORMAT_HMSC :
      _stprintf (pFormattedTime, _T("%02hu:%02hu:%02hu.%02hu"), pDateTime->wHour,
                                                                pDateTime->wMinute,
                                                                pDateTime->wSecond,
                                                               (WORD) (pDateTime->wMilliseconds / 10));
    break;

    case COMMON_TIME_FORMAT_HM :
    default :
      _stprintf (pFormattedTime, _T("%02hu:%02hu"), pDateTime->wHour,
                                                    pDateTime->wMinute);
    break;
  }

  return;
} // Common_GetDateTime

//------------------------------------------------------------------------------
// PURPOSE : Gets system's date-time 
// 
//  PARAMS :
//      - INPUT :
//          - TimeFormat : Time format
//              - COMMON_TIME_FORMAT_HM
//              - COMMON_TIME_FORMAT_HMS
//              - COMMON_TIME_FORMAT_HMSC
//      - OUTPUT : 
//          - pDateTime : Date-Time to get
// 
// RETURNS : 
//      - COMMON_ERROR_CONTROL_BLOCK
//      - COMMON_OK
// 
//   NOTES :

COMMONBASE_API DWORD WINAPI Common_GetDateTime (TYPE_GET_DATE_TIME * pDateTime,
                                                UINT               TimeFormat)
{
  // Check control block
  if ( pDateTime->control_block != sizeof (TYPE_GET_DATE_TIME) )
  {
    return COMMON_ERROR_CONTROL_BLOCK;
  }

  // Get UTC time from system
  GetSystemTime (&pDateTime->utc_date_time);

  // Get current time from system
  GetLocalTime (&pDateTime->current_date_time);

  // Format system's time
  Common_FormatDateTime (&pDateTime->current_date_time,
                         TimeFormat,
                         pDateTime->current_date_string,
                         pDateTime->current_time_string);

  return COMMON_OK;
} // Common_GetDateTime

//------------------------------------------------------------------------------
// PURPOSE : Converts a SYSTEMTIME structure into a C time number (t_time).
//          A C time number contents seconds elapsed since January 1.
// 
//  PARAMS :
//      - INPUT :
//          - pSystemTime: time structure
//
//      - OUTPUT : 
//          - pCTime: time in C format
// 
// RETURNS : None
// 
//   NOTES :

COMMONBASE_API void WINAPI Common_SystemTimeToCTime (const SYSTEMTIME   * pSystemTime, 
                                                     DWORD              * pCTime)
{
  ULARGE_INTEGER  _time_ul;
  FILETIME        _time_ft;

  SystemTimeToFileTime (pSystemTime, &_time_ft);

  _time_ul.LowPart = _time_ft.dwLowDateTime;
  _time_ul.HighPart = _time_ft.dwHighDateTime;

  _time_ul.QuadPart -= (DWORDLONG) 116444736000000000;
  _time_ul.QuadPart /= (DWORDLONG) 10000000;

  * pCTime = (DWORD) _time_ul.QuadPart;

  return;
} // Common_SystemTimeToCTime

//------------------------------------------------------------------------------
// PURPOSE : Convert a C time number (t_time) into a SYSTEMTIME structure.
//          A C time number contents seconds elapsed since January 1.
// 
//  PARAMS :
//      - INPUT :
//          - CTime: time in C format
//
//      - OUTPUT : 
//          - pSystemTime: time structure
// 
// RETURNS : None
// 
//   NOTES :

COMMONBASE_API void WINAPI Common_CTimeToSystemTime (DWORD         CTime, 
                                                     SYSTEMTIME    * pSystemTime)
{
  ULARGE_INTEGER  _time_ul;
  FILETIME        _time_ft;

  // Convert seconds in 100-nanoseconds and add elapsed 100-nanoseconds from
  // January 1, 1601 to January 1, 1970
  _time_ul.QuadPart = (DWORDLONG) CTime;
  _time_ul.QuadPart *= (DWORDLONG) 10000000;
  _time_ul.QuadPart += (DWORDLONG) 116444736000000000;

  _time_ft.dwLowDateTime = _time_ul.LowPart;
  _time_ft.dwHighDateTime = _time_ul.HighPart;

  FileTimeToSystemTime (&_time_ft, pSystemTime);

  return;
} // Common_CTimeToSystemTime

//------------------------------------------------------------------------------
// PURPOSE : Convert a DbDateTime into a DateTime structure.
// 
//  PARAMS :
//      - INPUT :
//          - pDbDateTime: DbDateTime to format
//
//      - OUTPUT : 
//          - pDateTime: formatted Date-Time
// 
// RETURNS : None
// 
//   NOTES :

COMMONBASE_API void WINAPI Common_DbDateTimeToDateTime (TYPE_VOID_DATE_TIME * pDbDateTime, TYPE_DATE_TIME * pDateTime)
{
  TYPE_DB_DATE_TIME * p_db_date_time;

  p_db_date_time = (TYPE_DB_DATE_TIME *) pDbDateTime;

  pDateTime->year    = (DWORD) p_db_date_time->year; 
  pDateTime->month   = (DWORD) p_db_date_time->month;
  pDateTime->day     = (DWORD) p_db_date_time->day;
  pDateTime->hour    = (DWORD) p_db_date_time->hour;
  pDateTime->minute  = (DWORD) p_db_date_time->minute;
  pDateTime->second  = (DWORD) p_db_date_time->second;
  pDateTime->is_null = (DWORD) (pDateTime->year == 0);
}

//------------------------------------------------------------------------------
// PURPOSE : Convert a DateTime into a DbDateTime structure.
// 
//  PARAMS :
//      - INPUT :
//          - pDateTime:  Date-Time to format
//
//      - OUTPUT : 
//          - pDbDateTime: formatted DbDateTime 
// 
// RETURNS : None
// 
//   NOTES :

COMMONBASE_API void WINAPI Common_DateTimeToDbDateTime (TYPE_DATE_TIME * pDateTime, TYPE_VOID_DATE_TIME * pDbDateTime)
{
  TYPE_DB_DATE_TIME * p_db_date_time;

  p_db_date_time = (TYPE_DB_DATE_TIME *) pDbDateTime;

  if ( pDateTime->is_null )
  {
    // AJQ 05-MAR-2003, this avoids inserting an invalid date into Oracle
    p_db_date_time->year    = (short)         1900;
    p_db_date_time->month   = (unsigned char) 1;
    p_db_date_time->day     = (unsigned char) 1;
    p_db_date_time->hour    = (unsigned char) 0;
    p_db_date_time->minute  = (unsigned char) 0;
    p_db_date_time->second  = (unsigned char) 0;
    p_db_date_time->filler  = (unsigned char) 0;
    p_db_date_time->is_null = 1;
  }
  else
  {
    p_db_date_time->year    = (short)         pDateTime->year;
    p_db_date_time->month   = (unsigned char) pDateTime->month;
    p_db_date_time->day     = (unsigned char) pDateTime->day;
    p_db_date_time->hour    = (unsigned char) pDateTime->hour;
    p_db_date_time->minute  = (unsigned char) pDateTime->minute;
    p_db_date_time->second  = (unsigned char) pDateTime->second;
    p_db_date_time->filler  = (unsigned char) 0;
    p_db_date_time->is_null = 0;
  }
}

//------------------------------------------------------------------------------
// PURPOSE : Convert a DateTime into a DbDateTime structure.
// 
//  PARAMS :
//      - INPUT :
//          - pDateTime:  Date-Time to format
//
//      - OUTPUT : 
//          - pDbDateTime: formatted DbDateTime 
// 
// RETURNS : None
// 
//   NOTES :

COMMONBASE_API void WINAPI Common_DateTimeToDbStrDateTime (TYPE_DATE_TIME * pDateTime, 
                                                           char * pDbStrDateTime)
{
  if ( pDateTime->is_null )
  {
    strcpy (pDbStrDateTime, STR_DB_DATE_NULL);
  }
  else
  {
    sprintf(pDbStrDateTime, STR_DB_DATE_FORMAT, pDateTime->day,
                                                pDateTime->month,
                                                pDateTime->year,
                                                pDateTime->hour,
                                                pDateTime->minute,
                                                pDateTime->second);
  }
}

//-----------------------------------------------------------------------------------------------
// PURPOSE : Returns the date/time of the first possible execution time in Connection Scheduling.
//
//  PARAMS :
//      - INPUT :
//          - Frequency: a masked number indicating the frequency of the programmed execution.
//          - FrequencyStep: the step of the execution.
//          - Enabled: If connection sched. is enabled
//          - NumTimesAllowed: Number of connection allowed. 
//                    0 --> Is Not defined
//          - NumRuns: Number of instances on table CONNECTION_INSTANCES ( connections running, pending, error ...)
//          - pRunAt: the date/time of the programmed execution.
//          - pRunFrom: the date/time to enabled connections
//          - pRunTo: the date/time to disabled connections
//          - pDateNow: the now date/time.
//
//      - OUTPUT :
//          - pNextRunAt: the date/time of the next execution.
//
// RETURNS : True/False if the date/time of the next execution was calculated ok or not.
//
//   NOTES :
//
COMMONBASE_API BOOL WINAPI Common_ComputeNextRunAt (DWORD           Frequency, 
                                                    DWORD           FrequencyStep,
                                                    DWORD           Enabled,
                                                    DWORD           NumTimesAllowed,
                                                    DWORD           NumRuns,
                                                    TYPE_DATE_TIME  * pRunAt, 
                                                    TYPE_DATE_TIME  * pRunFrom, 
                                                    TYPE_DATE_TIME  * pRunTo, 
                                                    TYPE_DATE_TIME  * pDateNow, 
                                                    TYPE_DATE_TIME  * pNextRunAt)

{
  int     _result;
  DWORD   _mask_freq;  
  
  if ( Enabled == FALSE )
  {
    pNextRunAt->is_null = TRUE;
    return TRUE;
  }
  
  if ( NumTimesAllowed != 0 )
  {
    if ( NumRuns >= NumTimesAllowed )
    {
      pNextRunAt->is_null = TRUE;
      return TRUE;
    }
  }

  _mask_freq = Frequency & FREQ_MASK_FREQUENCY;
  if ( _mask_freq == FREQ_ONCE ) 
  {
    if ( !CompareDateTime (pRunAt, pDateNow, &_result) )
    {
      return FALSE;
    }
    if ( _result == 1 ) 
    {
      // pRunAt > pDateNow
      * pNextRunAt = * pRunAt;
    }
    else
    {
      pNextRunAt->is_null = TRUE;
    }
    return TRUE;

  } 
  else if ( _mask_freq == FREQ_SYSTEM )
  {
    pNextRunAt->is_null = TRUE;
    return TRUE;

  }//if

  if ( pRunFrom->is_null == FALSE )
  {
    if ( !CompareDateTime (pRunFrom, pDateNow, &_result) )
    {
      return FALSE;
    }
    if ( _result == 1 ) 
    {
      // pRunFrom > pDateNow
      * pDateNow = * pRunFrom;
    }
  }

  if ( !ComputeNextRunAt (Frequency, 
                          FrequencyStep, 
                          pRunAt, 
                          pDateNow, 
                          pNextRunAt) )
  {
    return FALSE;
  }
  
  if ( pNextRunAt->is_null == TRUE )
  {
    return FALSE;
  }
  
  if ( pRunTo->is_null == FALSE )
  {
    if ( !CompareDateTime (pNextRunAt, pRunTo, &_result) )
    {
      return FALSE;
    }
    if ( _result == 1 ) 
    {
      // pNextRunAt > pRunTo
      pNextRunAt->is_null = TRUE;
    }
  }

  return TRUE;
} // Common_ComputeNextRunAt

//------------------------------------------------------------------------------
// PURPOSE : Returns the difference in msegs 
//
//  PARAMS :
//      - INPUT :
//          -  FirstTickCount: First Mark to check.
//          -  SecondTickCount: Last Mark to check.
//
//      - OUTPUT :
//          - Diference between both marks.
//
// RETURNS :
//
//   NOTES :

COMMONBASE_API DWORD WINAPI Common_DiffTickCount (DWORD   FirstTickCount,
                                                  DWORD   SecondTickCount)
{
  // Check TickCount rollover (minimum time should have be elapsed)
  if (FirstTickCount > SecondTickCount)
  {
    // A Counter rollover exist
    return SecondTickCount;
  }

  return (SecondTickCount - FirstTickCount);
} // Common_DifTickCount

//------------------------------------------------------------------------------
// PRIVATE FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------
// PURPOSE : Returns the date/time of the next execution time in Connection Scheduling.
//
//  PARAMS :
//      - INPUT :
//          - Frequency: a masked number indicating the frequency of the programmed execution.
//          - FrequencyStep: the step of the execution.
//          - pRunAt: the date/time of the programmed execution.
//          - pDateNow: the now date/time.
//
//      - OUTPUT :
//          - pNextRunAt: the date/time of the next execution.
//
// RETURNS : True/False if the date/time of the next execution was calculated ok or not.
//
//   NOTES :

BOOL ComputeNextRunAt (DWORD            Frequency, 
                       DWORD            FrequencyStep, 
                       TYPE_DATE_TIME   * pRunAt, 
                       TYPE_DATE_TIME   * pDateNow, 
                       TYPE_DATE_TIME   * pNextRunAt)
{
  SYSTEMTIME _date_now;
  SYSTEMTIME _next_run_at;
  WORD      _next_wday;
  DWORD     _num_days;
  BOOL      _result_next_weekday_ok;
  WORD      _day;
  DWORD     _weekdays_masked;
  DWORD     _month_day;
  int       _result_compare;
  BOOL      _first_iteration;
  DWORD     _aux_freq_step;

  _result_next_weekday_ok = FALSE;
  _next_wday = (WORD) -1;
  
  if ( DateTimeToSystemTime (pRunAt, &_next_run_at) == FALSE )
  {
    return FALSE;
  }

  if ( DateTimeToSystemTime (pDateNow, &_date_now) == FALSE )
  {
    return FALSE;
  }

  _result_compare = -1;
  _first_iteration = TRUE;
  while ( _result_compare <= 0 ) // _date_now >= _next_run_at
  {
    switch ( Frequency & FREQ_MASK_FREQUENCY )
    {
      case FREQ_ONCE:
      {
        pNextRunAt->is_null = TRUE;
           
        return TRUE;
      }
      break;

      case FREQ_MINUTELY:
      {
        if ( !_first_iteration )
        {
          if ( AddToSystemTime (&_next_run_at, ADD_MINUTE, FrequencyStep) == FALSE )
          {
            return FALSE;
          }
        }
      }
      break;

      case FREQ_HOURLY:
      {
        if ( !_first_iteration )
        {
          if ( AddToSystemTime (&_next_run_at, ADD_HOUR, FrequencyStep) == FALSE )
          {
            return FALSE;
          }
        }
      }
      break;

      case FREQ_DAILY:
      {
        if ( !_first_iteration )
        {
          if ( AddToSystemTime (&_next_run_at, ADD_DAY, FrequencyStep) == FALSE )
          {
            return FALSE;
          }
        }
      }
      break;

      case FREQ_WEEKLY:
      {
        WORD _bit_day;

        // Get  Bit day
        if ( !_first_iteration )//_next_wday != (WORD) -1 )
        {
          // Next iteration of while
          _bit_day = _next_wday;
          _aux_freq_step = FrequencyStep;
        }
        else
        {
          _aux_freq_step = 0;
          if ( _next_run_at.wDayOfWeek == 0 )
          {
            _bit_day = FREQ_SUNDAY;
          }
          else
          {
            _bit_day = FREQ_MONDAY << ( _next_run_at.wDayOfWeek - 1 );
          } 
        }// if

        _weekdays_masked = Frequency & FREQ_MASK_WEEKDAYS;
        _result_next_weekday_ok = ComputeNextWeekDay (_weekdays_masked, 
                                                      _aux_freq_step, 
                                                      _bit_day, 
                                                      &_next_wday, 
                                                      &_num_days);
        if ( !_result_next_weekday_ok )      
        {
          return FALSE;
        }
        else
        {
          if ( AddToSystemTime (&_next_run_at, ADD_DAY, _num_days) == FALSE )
          {
            return FALSE;
          }
        } // if
      }
      break;

      case FREQ_MONTHLY:
      {
        _next_run_at.wDay = 1;
        if ( !_first_iteration )
        {
          if ( AddToSystemTime (&_next_run_at, ADD_DAY, FrequencyStep * 31) == FALSE )
          {
            return FALSE;
          }
        }
        
        switch ( Frequency & FREQ_MASK_MONTH_OPT )
        {
          case FREQ_MONTH_FIRST:
          {
            _next_run_at.wDay = 1;
          }
          break;

          case FREQ_MONTH_LAST:
          {
            if ( !GetSameMonthLastDay (_next_run_at.wYear, _next_run_at.wMonth, &_day) )
            {
              return FALSE;
            } // if
            _next_run_at.wDay = (WORD) _day;
          }
          break;

          case FREQ_MONTH_DAY:
          {
            _month_day = Frequency & FREQ_MASK_MONTH_DAY;
            if ( !GetSameMonthLastDay (_next_run_at.wYear, _next_run_at.wMonth, &_day) )
            {
              return FALSE;
            } // if
            if ( _month_day > _day )
            {
              _next_run_at.wDay = (WORD) _day;
            }
            else
            {
              _next_run_at.wDay = (WORD) _month_day;
            }
          } 
          break;

          default:
          {
            return FALSE;
          }
          break;
        } // switch
        
      }
      break;
      case FREQ_SYSTEM:
      {
        pNextRunAt->is_null = TRUE;
        return TRUE;

      }
      break;
      default:
      {
        return FALSE;
      }
      break;
    } // switch
    
    if ( !CompareSystemTime (&_next_run_at, &_date_now, &_result_compare) )
    {
      return FALSE;
    }
    _first_iteration = FALSE;
  } //while

  if ( SystemTimeToDateTime (&_next_run_at, pNextRunAt) == FALSE )
  {
    return FALSE;
  }

  return TRUE;
} // ComputeNextRunAt

//------------------------------------------------------------------------------
// PURPOSE : Calculates the next weekday to execute in scheduling.
//
//  PARAMS :
//      - INPUT :
//          - EnabledWeekDays: Indicates the weekdays mask checked.
//          - Weekstep: Indicates the frequency step.
//          - Weekday: Indicates the mask of the weekday.
//
//      - OUTPUT :
//        - pNextWeekDay: the next weekday mask to execute.
//        - pNumDays: the days of difference between now and the next weekday to execute.
//
// RETURNS : True/False if the next weekday was calculated ok.
//
//   NOTES :

BOOL ComputeNextWeekDay (DWORD EnabledWeekDays, 
                         DWORD WeekStep, 
                         WORD  WeekDay, 
                         WORD  *pNextWeekDay, 
                         DWORD *pNumDays)
{
  WORD  _week_day;
  DWORD  _idx;
  DWORD _num_days;
    
  if ( EnabledWeekDays == 0 )
  {
    return FALSE;
  }
  
  _week_day = WeekDay;
  _num_days = 0;

  if ( WeekStep > 0 )
  {
    _week_day = _week_day << 1;
  }
    
  for (_idx = 0; _idx < DAYS_IN_A_WEEK; _idx++)
  {
    if ( _week_day > FREQ_SUNDAY )
    {
      _week_day = FREQ_MONDAY; 
      if ( WeekStep > 0 )
      {
        _num_days = (WeekStep - 1) * DAYS_IN_A_WEEK;
      }
    } // if

    if ( (_week_day & EnabledWeekDays) == _week_day )
    {
      if ( WeekStep > 0 )
      {
        *pNumDays = _num_days + _idx + 1;
      }
      else
      {
        *pNumDays = _num_days + _idx;
      }
      *pNextWeekDay = _week_day;

      return TRUE;
    }

    // Next day
    _week_day = _week_day << 1;
    
  } // for

  return FALSE;
} // ComputeNextWeekDay

//------------------------------------------------------------------------------
// PURPOSE : Converts a TYPE_DATE_TIME type to SYSTEMTIME
//
//  PARAMS :
//      - INPUT :
//          - pIn:  TYPE_DATE_TIME
//          - pOut: SYSTEMTIME
//
//      - OUTPUT :
//          - none
//
// RETURNS : True/False if the conversion was done Ok or Not
//
//   NOTES :

BOOL DateTimeToSystemTime (TYPE_DATE_TIME * pIn, SYSTEMTIME * pOut)
{
  FILETIME _aux_file_time;
  BOOL     _rc_bool;

  if ( pIn->is_null )
  {
    return FALSE;
  }

  pOut->wYear   = (WORD) pIn->year;
  pOut->wMonth  = (WORD) pIn->month;
  pOut->wDay    = (WORD) pIn->day;
  pOut->wHour   = (WORD) pIn->hour;
  pOut->wMinute = (WORD) pIn->minute;
  pOut->wSecond = (WORD) pIn->second;
  pOut->wMilliseconds = 0;
  
  //The wDayOfWeek member of the SYSTEMTIME structure is ignored.
  pOut->wDayOfWeek = (WORD) 0; 
  
  // Conversion to compute wDayOfWeek
  //SystemTyme To FILETYME & FILETIME TO SystemTyme
  _rc_bool = SystemTimeToFileTime (pOut, &_aux_file_time); 
  if ( _rc_bool == FALSE )
  {
    return FALSE;
  }
  
  _rc_bool = FileTimeToSystemTime (&_aux_file_time, pOut);
  if ( _rc_bool == FALSE )
  {
    return FALSE;
  }

  return TRUE;

} // DateTimeToSystemTime

//------------------------------------------------------------------------------
// PURPOSE : Converts a TYPE_DATE_TIME type to SYSTEMTIME
//
//  PARAMS :
//      - INPUT :
//          - pIn:  TYPE_DATE_TIME
//          - pOut: SYSTEMTIME
//
//      - OUTPUT :
//          - none
//
// RETURNS : True/False if the conversion was done Ok or Not
//
//   NOTES :

BOOL SystemTimeToDateTime (SYSTEMTIME * pIn, TYPE_DATE_TIME * pOut)
{
  pOut->year    = pIn->wYear;
  pOut->month   = pIn->wMonth;
  pOut->day     = pIn->wDay;
  pOut->hour    = pIn->wHour;
  pOut->minute  = pIn->wMinute;
  pOut->second  = pIn->wSecond;
  pOut->is_null = FALSE;
  
  return TRUE;
} // SystemTimeToDateTime

//------------------------------------------------------------------------------
// PURPOSE : 
//
//  PARAMS :
//      - INPUT :
//          - pSystemTime:  SYSTEMTIME
//          - TypeToAdd: TYPE_ADD_TO_SYSTEMTIME
//          - NumberToAdd: DWORD
//
//      - OUTPUT :
//          - pSystemTime:  SYSTEMTIME
//
// RETURNS : True/False if the add was done Ok or Not
//
//   NOTES :

BOOL AddToSystemTime (SYSTEMTIME              * pSystemTime, 
                      TYPE_ADD_TO_SYSTEMTIME  TypeToAdd, 
                      DWORD                   NumberToAdd)
{
  ULARGE_INTEGER _msec;
  ULARGE_INTEGER _msec_to_add;
  ULARGE_INTEGER _msec_result;
  FILETIME       _file_time;
  FILETIME       _filet_time_result;
  BOOL           _rc_bool;
 
  // XPS 28-MAY-2004
  // Assign to DWORD a QWORD
  _msec_to_add.QuadPart = NumberToAdd;

  switch ( TypeToAdd )
  {
    // Multiply in variable QWORD
    // Because if multipliy directy lose precission
    case ADD_MINUTE :
      _msec_to_add.QuadPart *= MINUTE;
    break;

    case ADD_HOUR :
      _msec_to_add.QuadPart *= HOUR;
    break;

    case ADD_DAY:
      _msec_to_add.QuadPart *= DAY;
    break;
        
    default :
      return FALSE;
    break;
  } //switch

  // Conversion from SYSTEMTIME structure to FILETIME structure 
  _rc_bool = SystemTimeToFileTime (pSystemTime, &_file_time); 
  if ( _rc_bool == FALSE )
  {
    return FALSE;
  }

  // Copies the FILETIME structure into the ULARGER_INTEGER structure; 
  // (64-bit value for arithmetic purposes) 
  _msec.LowPart  = _file_time.dwLowDateTime; 
  _msec.HighPart = _file_time.dwHighDateTime; 

  _msec_result.QuadPart = _msec.QuadPart + _msec_to_add.QuadPart;

  _filet_time_result.dwLowDateTime  = _msec_result.LowPart;
  _filet_time_result.dwHighDateTime = _msec_result.HighPart;

  _rc_bool = FileTimeToSystemTime (&_filet_time_result, pSystemTime);
  if ( _rc_bool == FALSE )
  {
    return FALSE;
  }

  return TRUE;
} //AddToSystemTime 

//------------------------------------------------------------------------------
// PURPOSE : Compare two SYSTEMTIMEs
//
//  PARAMS :
//      - INPUT :
//          - pSystemTime1:  SYSTEMTIME
//          - pSystemTime2:  SYSTEMTIME
//
//      - OUTPUT :
//          - Result:
//                �1 First SYSTEMTIME is less than second SYSTEMTIME. 
//                 0 First SYSTEMTIME is equal to second SYSTEMTIME. 
//                 1 First SYSTEMTIME is greater than second SYSTEMTIME. 
//
// RETURNS : 
//
//   NOTES :

BOOL CompareSystemTime (SYSTEMTIME * pSystemTime1, SYSTEMTIME * pSystemTime2, int * pResult)
{
  FILETIME    _file_time1;
  FILETIME    _file_time2;

  // Conversion from SYSTEMTIME structure to FILETIME structure 
  if ( SystemTimeToFileTime (pSystemTime1, &_file_time1) == FALSE )
  {
    return FALSE;
  }
  
  if ( SystemTimeToFileTime (pSystemTime2, &_file_time2) == FALSE )
  {
    return FALSE;
  }

  *pResult = CompareFileTime (&_file_time1, &_file_time2);
  
  return TRUE;
} //CompareSystemTime

//------------------------------------------------------------------------------
// PURPOSE : Compare two TYPE_DATE_TIMES
//
//  PARAMS :
//      - INPUT :
//          - pDateTime1:  TYPE_DATE_TIME
//          - pDateTime2:  TYPE_DATE_TIME
//
//      - OUTPUT :
//          - Result:
//                �1 First SYSTEMTIME is less than second SYSTEMTIME. 
//                 0 First SYSTEMTIME is equal to second SYSTEMTIME. 
//                 1 First SYSTEMTIME is greater than second SYSTEMTIME. 
//
// RETURNS : 
//
//   NOTES :

BOOL CompareDateTime (TYPE_DATE_TIME * pDateTime1, TYPE_DATE_TIME * pDateTime2, int * pResult)
{
  SYSTEMTIME  _system_time1;
  SYSTEMTIME  _system_time2;
  FILETIME    _file_time1;
  FILETIME    _file_time2;

  if ( DateTimeToSystemTime (pDateTime1, &_system_time1) == FALSE )
  {
    return FALSE;
  }

  if ( DateTimeToSystemTime (pDateTime2, &_system_time2) == FALSE )
  {
    return FALSE;
  }

  // Conversion from SYSTEMTIME structure to FILETIME structure 
  if ( SystemTimeToFileTime (&_system_time1, &_file_time1) == FALSE )
  {
    return FALSE;
  }
  
  if ( SystemTimeToFileTime (&_system_time2, &_file_time2) == FALSE )
  {
    return FALSE;
  }

  *pResult = CompareFileTime (&_file_time1, &_file_time2);
  
  return TRUE;
} //CompareDateTime

//------------------------------------------------------------------------------
// PURPOSE : Returns the last correct day of a month.
//
//  PARAMS :
//      - INPUT :
//          - The date (Year, Month, Day)
//
//      - OUTPUT :
//          - The day of month.
//
// RETURNS : True/False if the last day of month was calculated ok or not.
//
//   NOTES :

BOOL GetSameMonthLastDay (WORD Year, WORD Month, WORD *pLastDay)
{
  SYSTEMTIME   _aux_sys_time;
  WORD         _last_day;

  if ( (Month > 12) || (Month == 0) )
  {
    return FALSE;
  }

  memset (&_aux_sys_time, 0, sizeof (SYSTEMTIME));
  _aux_sys_time.wDay = 27;  // Start Day to Find Month Last Day
  _aux_sys_time.wMonth = Month;
  _aux_sys_time.wYear = Year;
  _last_day = _aux_sys_time.wDay;

  while ( _aux_sys_time.wMonth == Month )
  {
    _last_day = _aux_sys_time.wDay;
    if (!AddToSystemTime (&_aux_sys_time, ADD_DAY, 1))
    {
      return FALSE;
    }
  } // while
  
  *pLastDay = _last_day;

  return TRUE;
} // GetSameMonthLastDay

//------------------------------------------------------------------------------
// PURPOSE : Convert a DbDateTime string into a DateTime.
// 
//  PARAMS :
//      - INPUT :
//          - pDbStrDateTime:  DBDateTime
//
//      - OUTPUT : 
//          - pDateTime: Date-Time
// 
// RETURNS : None
// 
//   NOTES :

COMMONBASE_API void WINAPI Common_DbStrDateTimeToDateTime (char           * pDbStrDateTime, 
                                                           TYPE_DATE_TIME * pDateTime)
{
  pDateTime->is_null = FALSE;
  sscanf( pDbStrDateTime, 
          STR_DB_DATE_FORMAT,  
          &pDateTime->day,
          &pDateTime->month,
          &pDateTime->year,
          &pDateTime->hour,
          &pDateTime->minute,
          &pDateTime->second);

  return;
} // Common_DbStrDateTimeToDateTime