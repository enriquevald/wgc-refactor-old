//------------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : CommonBaseVersion.cpp
// 
//   DESCRIPTION : Functions and Methods to Check/Get the Version of a Module
// 
//        AUTHOR : Alberto Cuesta
// 
// CREATION DATE : 30-OCT-2008
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ -----------------------------------------------------------
// 30-OCT-2008 ACC    Initial draft.
// 
//-------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  INCLUDES 
//------------------------------------------------------------------------------

#define INCLUDE_VERSION_CONTROL
#include "CommonDef.h"

//------------------------------------------------------------------------------
// PRIVATE CONSTANTS 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE DATATYPES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE DATA STRUCTURES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE FUNCTION PROTOTYPES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PUBLIC FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE : Checks the version of given module. It also retrieve module about 
//          information as virtual module version string, company name and
//          legal copyright string
// 
//  PARAMS :
//      - INPUT :
//        - pModuleName
//
//      - OUTPUT : 
//        - pModuleAbout: Information about module version:
//                          - Module version
//                          - Company name
//                          - Legal copyright
// 
// RETURNS : 
//
//   NOTES :

COMMONBASE_API DWORD WINAPI CommonBase_VersionGetModuleVersion (TCHAR             * pModuleName, 
                                                                TYPE_MODULE_ABOUT * pModuleAbout)
{
  TCHAR               _path [_MAX_PATH];
  TCHAR               _drive [_MAX_DRIVE];
  TCHAR               _dir [_MAX_DIR];
  TYPE_VERSION_MODULE _module_version;
  WORD                _rc;  

  // Check control block
  if ( pModuleAbout->control_block != sizeof (TYPE_MODULE_ABOUT) )
  {
    return COMMON_ERROR_CONTROL_BLOCK;
  }

  // Obtain executable path
  GetModuleFileName (NULL, _path, _MAX_PATH);
  _tsplitpath (_path, _drive, _dir, NULL, NULL);
  _tmakepath (_path, _drive, _dir, _T(""), _T(""));

  // Check control block for output parameter
  if ( pModuleAbout->control_block != sizeof (TYPE_MODULE_ABOUT) )
  {
    return COMMON_ERROR;
  }

  // Init module version structure 
  memset (&_module_version, 0, sizeof (TYPE_VERSION_MODULE));
  _module_version.control_block = sizeof (TYPE_VERSION_MODULE);

  // Check module version by
  //    - Get Module information (expected from DL and found from components)
  //    - Check expected and found values skipping first list component
  //      because it corresponds to virtual module version

  //    - Get Module information (expected from DL and found from components)
  _rc = Version_GetModuleVersion (pModuleName,
                                  _path,
                                  &_module_version);
  if ( _rc != VERSION_STATUS_OK )
  {
    return COMMON_ERROR;
  } 

  // Fill output structure with the following items
  //    - Module version string
  //    - Company name 
  //    - Legal copyright
  //    - Client Id

  //    - Module version string
  _tcscpy (pModuleAbout->version, _module_version.component[0].expected.version);
  //    - Company name 
  _tcscpy (pModuleAbout->company_name, _module_version.component[0].company_name);
  //    - Legal copyright
  _tcscpy (pModuleAbout->copyright, _module_version.component[0].copyright);

  //    - Client Id (required to check it against the license)
  pModuleAbout->client_id             = _module_version.client_id;
  pModuleAbout->build                 = _module_version.component[0].expected.build;
  pModuleAbout->database_client_build = _module_version.component[0].expected.client_database;
  pModuleAbout->database_common_build = _module_version.component[0].expected.common_database;

  return COMMON_OK;
} // CommonBase_VersionGetModuleVersion

//------------------------------------------------------------------------------
// PURPOSE : Checks the version of given module. It also retrieve module about 
//          information as virtual module version string, company name and
//          legal copyright string
// 
//  PARAMS :
//      - INPUT :
//        - pModuleName
//
//      - OUTPUT : 
//        - pModuleAbout: Information about module version:
//                          - Module version
//                          - Company name
//                          - Legal copyright
// 
// RETURNS : 
//
//   NOTES :

COMMONBASE_API DWORD WINAPI CommonBase_VersionCheckModuleVersion (TCHAR             * pModuleName, 
                                                                  TYPE_MODULE_ABOUT * pModuleAbout)
{
  TCHAR  _path [_MAX_PATH];
  TCHAR  _drive [_MAX_DRIVE];
  TCHAR  _dir [_MAX_DIR];
  WORD   _rc;

  // Check control block
  if ( pModuleAbout->control_block != sizeof (TYPE_MODULE_ABOUT) )
  {
    return COMMON_ERROR_CONTROL_BLOCK;
  }

  // Obtain executable path
  GetModuleFileName (NULL, _path, _MAX_PATH);
  _tsplitpath (_path, _drive, _dir, NULL, NULL);
  _tmakepath (_path, _drive, _dir, _T(""), _T(""));

  _rc = Version_CheckModuleVersion (pModuleName, _path, pModuleAbout);

  if ( _rc != VERSION_STATUS_OK )
  {
    if ( _rc == VERSION_STATUS_DB )
    {
      return COMMON_ERROR_DB;
    }

    return COMMON_ERROR;
  }

  return COMMON_OK;
} // CommonBase_VersionCheckModuleVersion

