//-----------------------------------------------------------------------------
// Copyright � 2004 Win Systems Ltd.
//-----------------------------------------------------------------------------
//
//   MODULE NAME : CommonBaseTimeZone.cpp
//
//   DESCRIPTION : Functions and Methods to handle Exceptions on MultipleThreads.
//
//        AUTHOR : Andreu Juli�
//
// CREATION DATE : 20-JUN-2005
//
// REVISION HISTORY
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 20-JUN-2005 AJQ First release.
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// INCLUDES
//------------------------------------------------------------------------------
#define INCLUDE_PRIVATE_LOG

#include "CommonDef.h"

//------------------------------------------------------------------------------
// PRIVATE CONSTANTS
//------------------------------------------------------------------------------
#define MAX_THREAD_INFO   1000

//------------------------------------------------------------------------------
// PRIVATE DATATYPES
//------------------------------------------------------------------------------
typedef struct
{
  DWORD   thread_id;
  HANDLE  handle;
  TCHAR * p_func;
  TCHAR * p_src;
  DWORD line;
  TCHAR * p_module;

} TYPE_THREAD_INFO;

//------------------------------------------------------------------------------
// PRIVATE DATA STRUCTURES
//------------------------------------------------------------------------------
static  CRITICAL_SECTION  GLB_ThreadCriticalSection;
static  DWORD             GLB_ThreadCount = 0;
static  TYPE_THREAD_INFO  GLB_ThreadInfo [MAX_THREAD_INFO];


//------------------------------------------------------------------------------
// PRIVATE FUNCTION PROTOTYPES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE FUNCTIONS IMPLEMENTATION
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PUBLIC FUNCTIONS IMPLEMENTATION
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
// PURPOSE : 
//
//  PARAMS :
//      - INPUT :
//      - OUTPUT :
//
// RETURNS :
//
//   NOTES :

COMMONBASE_API void Common_LogThreadInfo (DWORD  ThreadId)
{
  DWORD _idx;
  BOOL  _found;
  TCHAR _module [_MAX_PATH];
  TCHAR _file [_MAX_PATH];
  TCHAR _msg  [_MAX_PATH];
  TCHAR _param  [_MAX_PATH];

  _found = FALSE;
  for ( _idx = 0; _idx < GLB_ThreadCount; _idx++ )
  {
    if ( GLB_ThreadInfo[_idx].thread_id == ThreadId )
    {
      _found = TRUE;
      break;
    }
  } // for

  if ( !_found )
  {
    return;
  }

  GetModuleFileName (NULL, _file, _MAX_PATH);
  _tsplitpath (_file, NULL, NULL, _module, NULL);

  _stprintf (_param, _T("ThreadId: %4lu   Start: %s"), GLB_ThreadInfo[_idx].thread_id,
                                                       GLB_ThreadInfo[_idx].p_func);

  _stprintf (_msg, _T("Source: [%lu]%s\n"), GLB_ThreadInfo[_idx].line,
                                            GLB_ThreadInfo[_idx].p_src);

  PrivateLog(5, _module, GLB_ThreadInfo[_idx].p_func, _T("** THREAD INFO **"), _param, _T(""), _msg);
} // Common_LogThreadInfo



//------------------------------------------------------------------------------
// PURPOSE : Returns a pointer to the current Time Zone Information
//
//  PARAMS :
//      - INPUT :
//      - OUTPUT :
//
// RETURNS :
//
//   NOTES :

COMMONBASE_API HANDLE Common_CreateThread  (LPSECURITY_ATTRIBUTES   lpThreadAttributes,
                                            SIZE_T                  dwStackSize,
                                            LPTHREAD_START_ROUTINE  lpStartAddress,
                                            LPVOID                  lpParameter,
                                            DWORD                   dwCreationFlags,
                                            LPDWORD                 lpThreadId,
                                            // Debug Parameters
                                            TCHAR                   * pDbgStartAddress,
                                            TCHAR                   * pDbgSourceFile,
                                            DWORD                   DbgSourceLine,
                                            TCHAR                   * pDbgModule)
{
  HANDLE  _handle;
  DWORD   _thread_id;

  __try
  {
    if ( GLB_ThreadCount == 0 )
    {
      GLB_ThreadCount++;

      InitializeCriticalSection (&GLB_ThreadCriticalSection);
      EnterCriticalSection (&GLB_ThreadCriticalSection);

      GLB_ThreadInfo[0].thread_id = GetCurrentThreadId ();
      GLB_ThreadInfo[0].handle    = GetCurrentThread ();
      GLB_ThreadInfo[0].p_func    = _T("main");
      GLB_ThreadInfo[0].p_src     = _T("N/A");
      GLB_ThreadInfo[0].line      = 0;
      GLB_ThreadInfo[0].p_module  = _T("N/A");
    }
    else
    {
      EnterCriticalSection (&GLB_ThreadCriticalSection);
    }

    _handle = CreateThread (lpThreadAttributes,
                            dwStackSize,
                            lpStartAddress,
                            lpParameter,
                            dwCreationFlags,
                            &_thread_id);
    if ( _handle == NULL )
    {
      return NULL;
    } // if

    if ( lpThreadId != NULL )
    {
      * lpThreadId = _thread_id;
    }
    GLB_ThreadInfo[GLB_ThreadCount].thread_id = _thread_id;
    GLB_ThreadInfo[GLB_ThreadCount].handle    = _handle;
    GLB_ThreadInfo[GLB_ThreadCount].p_func    = pDbgStartAddress;
    GLB_ThreadInfo[GLB_ThreadCount].p_src     = pDbgSourceFile;
    GLB_ThreadInfo[GLB_ThreadCount].line      = DbgSourceLine;
    GLB_ThreadInfo[GLB_ThreadCount].p_module  = pDbgModule;
    GLB_ThreadCount++;
  }
  __finally
  {
    LeaveCriticalSection (&GLB_ThreadCriticalSection);
  }

  return _handle;
} // Common_CreateThread


//------------------------------------------------------------------------------
// PURPOSE : 
//
//  PARAMS :
//      - INPUT :
//      - OUTPUT :
//
// RETURNS :
//
//   NOTES :

COMMONBASE_API void Common_LogProcessThreadTimes (void)
{
  DWORD _idx;
  TCHAR _module [_MAX_PATH];
  TCHAR _file [_MAX_PATH];
  TCHAR _msg  [_MAX_PATH];
  TCHAR _param  [_MAX_PATH];
  TYPE_THREAD_INFO * p_info;

  FILETIME    _ft_c, _ft_e;
  FILETIME    _ft_k, _ft_u, _ft_t;
  SYSTEMTIME  _st_k, _st_u, _st_t;
  DWORD64     * p_a;
  DWORD64     * p_b;
  DWORD64     * p_c;

  // TODO: Remove the return sentence
  return;

  GetModuleFileName (NULL, _file, _MAX_PATH);
  _tsplitpath (_file, NULL, NULL, _module, NULL);

  memset (_param, 0, sizeof(_param));
  PrivateLog(5, _module, _T(""), _T("** THREAD TIME **"), _T(""), _T(""), _T("---------------- BEGIN\n"));

  for ( _idx = 0; _idx < GLB_ThreadCount; _idx++ )
  {
    p_info = &GLB_ThreadInfo[_idx];

    _stprintf (_param, _T("ThreadId: %4lu  Start: %-30s"), p_info->thread_id, p_info->p_func);

    GetThreadTimes (p_info->handle, &_ft_c, &_ft_e, &_ft_k, &_ft_u);

    p_a = (DWORD64 *) &_ft_k;
    p_b = (DWORD64 *) &_ft_u;
    p_c = (DWORD64 *) &_ft_t;

    * p_c = (* p_a) + (* p_b);

    FileTimeToSystemTime (&_ft_k, &_st_k);
    FileTimeToSystemTime (&_ft_u, &_st_u);
    FileTimeToSystemTime (&_ft_t, &_st_t);

    _stprintf (_msg, _T("  K:%02u:%02u.%03u  U:%02u:%02u.%03u  T:%02u:%02u.%03u"), _st_k.wMinute, _st_k.wSecond, _st_k.wMilliseconds,
                                                                                   _st_u.wMinute, _st_u.wSecond, _st_u.wMilliseconds,
                                                                                   _st_t.wMinute, _st_t.wSecond, _st_t.wMilliseconds);

    PrivateLog(5, _module, GLB_ThreadInfo[_idx].p_func, _T("** THREAD TIME **"), _param, _msg, NULL);
  } // for
  PrivateLog(5, _module, _T(""), _T("** THREAD TIME **"), _T(""), _T(""), _T("---------------- END\n"));

} // Common_LogThreadInfo
