//------------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//------------------------------------------------------------------------------
//
//   MODULE NAME: CommonAPI.cpp
//   DESCRIPTION: Common Functions and Methods for API's
//        AUTHOR: Andreu Juli�
// CREATION DATE: 12-JUL-2002
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ -----------------------------------------------------------
// 12-JUL-2002 AJQ    Initial draft.
//
//-------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  INCLUDES
//------------------------------------------------------------------------------
#define INCLUDE_API
#define INCLUDE_NLS_API
#define INCLUDE_PRIVATE_LOG
#define INCLUDE_COMMON_MISC
#include "CommonDef.h"

// AJQ, 14-FEB-2003, PrivateLog.h & CommonMisc.h are NOT really used
// TODO: Define the prototype of the log functions ...
//

//------------------------------------------------------------------------------
//  PRIVATE CONSTANTS
//------------------------------------------------------------------------------
#define COMMON_API_AUX_STRING_LEN     128
#define COMMON_API_MAX_IUD_SIZE       (16*1024) // 16KB

//------------------------------------------------------------------------------
//  PRIVATE DATATYPES
//------------------------------------------------------------------------------
typedef struct
{
  API_INPUT_PARAMS      api_input_params;
  API_OUTPUT_PARAMS    * p_api_output_params;
  API_OUTPUT_USER_DATA * p_api_output_user_data;
  BYTE                 api_input_user_data_buffer [COMMON_API_MAX_IUD_SIZE];

} TYPE_COMMON_API_REQUEST_PARAMS;

typedef enum
{
  LOG_CODE_CALL_FAILED = 0,
  LOG_CODE_WRONG_SIZE  = 1,
  LOG_CODE_UNEXPECTED  = 2

} ENUM_LOG_CODE;

//------------------------------------------------------------------------------
//  PRIVATE FUNCTION PROTOTYPES
//------------------------------------------------------------------------------

void Common_API_Log (ENUM_LOG_CODE  LogCode,
                     TCHAR * pFunctionName,
                     TCHAR * pParam1,
                     TCHAR * pParam2,
                     TCHAR * pParam3);

#define Common_API_Logger(MsgId,Param1,Param2,Param3) Common_API_Log (MsgId, _function_name, Param1, Param2, Param3)

//------------------------------------------------------------------------------------
//  PUBLIC FUNCTIONS IMPLEMENTATION
//------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PURPOSE: Frees event object
//
//  PARAMS:
//     - INPUT:
//      - EventObject
//
//     - OUTPUT: None
//
// RETURNS:
//
// NOTES:
//

void Common_API_CloseEvent (HANDLE EventObject)
{
  static TCHAR _function_name[] = _T("Common_API_CloseEvent");
  TCHAR  _aux_tchar_1[COMMON_API_AUX_STRING_LEN];

  BOOL  _bool_rc;

  if ( EventObject != NULL )
  {
    // Free event
    _bool_rc = CloseHandle (EventObject);
    if ( !_bool_rc )
    {
      // Logger message
      _stprintf (_aux_tchar_1, _T("%lu"), GetLastError ());
      Common_API_Logger (LOG_CODE_CALL_FAILED, _aux_tchar_1, _T("CloseHandle"), _T(""));
    } // if
  } // if

} // Common_API_CloseEvent


//-----------------------------------------------------------------------------
// PURPOSE: Checks control block
//
//  PARAMS:
//     - INPUT:
//      - ReceivedCB
//      - RequestedCB
//      - ModuleName, FunctionName and StructName for logger message
//
//     - OUTPUT: None
//
//
// RETURNS:
//  - TRUE: CB OK
//  - FALSE: CB error
//
// NOTES:
//

BOOL Common_API_CheckControlBlock (DWORD ReceivedCB,
                                   DWORD RequestedCB,
                                   TCHAR * pFunctionName,
                                   TCHAR * pStructName)
{
  TCHAR _aux_tchar_1 [COMMON_API_AUX_STRING_LEN];
  TCHAR _aux_tchar_2 [COMMON_API_AUX_STRING_LEN];

  if ( ReceivedCB != RequestedCB )
  {
    // Logger message
    _stprintf (_aux_tchar_1, _T("%lu"), RequestedCB);
    _stprintf (_aux_tchar_2, _T("%lu"), ReceivedCB);
    Common_API_Log (LOG_CODE_WRONG_SIZE, pFunctionName, pStructName, _aux_tchar_1, _aux_tchar_2);

    // CB error
    return FALSE;
  }

  return TRUE;

} // Common_API_CheckControlBlock


//-----------------------------------------------------------------------------
// PURPOSE: Common public interface
//
//  PARAMS:
//     - INPUT:
//      - pApiInputParams
//      - pApiInputUserData
//     - OUTPUT:
//      - pApiOutputParams
//      - pApiOutputUserData
//
// RETURNS:
//     - API_STATUS_OK
//     - API_STATUS_PARAMETER_ERROR
//     - API_STATUS_CB_SIZE_ERROR
//     - API_STATUS_EVENT_OBJECT_ERROR
//     - API_STATUS_NO_RESOURCES_AVAILABLE
//
// NOTES:
//
WORD Common_API_Entry (API_INPUT_PARAMS     * pApiInputParams,
                       API_INPUT_USER_DATA  * pApiInputUserData,
                       API_OUTPUT_PARAMS    * pApiOutputParams,
                       API_OUTPUT_USER_DATA * pApiOutputUserData,
                       TCHAR                * pFunctionName)
{
  TCHAR  _aux_tchar_1 [COMMON_API_AUX_STRING_LEN];
  TCHAR  _aux_tchar_2 [COMMON_API_AUX_STRING_LEN];

  TYPE_COMMON_API_REQUEST_PARAMS  _api_request_params;
  WORD                            _api_request_size;
  WORD                            _input_user_data_size;
  WORD                            _call_status;
  DWORD                           _wait_status;
  BOOL                            _bool_rc;
  QUEUE_HANDLE                    _request_queue;
  HANDLE                          _ev_sync_mode = NULL;

  // Check size of Request
  if ( GLB_API.max_iud_size > (WORD) sizeof (_api_request_params.api_input_user_data_buffer) )
  {
    // Logger message
    _stprintf (_aux_tchar_1, _T("%lu"), sizeof (_api_request_params.api_input_user_data_buffer));
    _stprintf (_aux_tchar_2, _T("%lu"), GLB_API.max_iud_size);
    Common_API_Log (LOG_CODE_WRONG_SIZE, pFunctionName, _T("MAX_IUD_SIZE"), _aux_tchar_1, _aux_tchar_2);

    return API_STATUS_CB_SIZE_ERROR;
  } // if

  // Check mandatory parameters
  if ( pApiInputParams == NULL )
  {
    // Logger message
    Common_API_Log (LOG_CODE_UNEXPECTED, pFunctionName, _T("ApiInputParameter"), _T("NULL"), _T(""));

    return API_STATUS_PARAMETER_ERROR;
  }

  if ( pApiOutputParams == NULL )
  {
    // Logger message
    Common_API_Log (LOG_CODE_UNEXPECTED, pFunctionName, _T("ApiOutputParameter"), _T("NULL"), _T(""));

    return API_STATUS_PARAMETER_ERROR;
  }

  // Control block checking for mandatory parameters
  _bool_rc = Common_API_CheckControlBlock  (pApiInputParams->control_block,
                                            sizeof (API_INPUT_PARAMS),
                                            pFunctionName,
                                            _T("ApiInputControlBlock"));
  if (  !_bool_rc  )
  {
    return API_STATUS_CB_SIZE_ERROR;
  }

  _bool_rc = Common_API_CheckControlBlock  (pApiOutputParams->control_block,
                                            sizeof (API_OUTPUT_PARAMS),
                                            pFunctionName,
                                            _T("ApiOutputControlBlock"));
  if (  !_bool_rc  )
  {
    return API_STATUS_CB_SIZE_ERROR;
  }

  // Check API mode for some operations
  if ( GLB_API.p_check_mode != NULL )
  {
    if ( GLB_API.p_check_mode (pApiInputParams) == FALSE )
    {
      return API_STATUS_CALL_MODE_ERROR;
    }
  }

  // Copy of user parameters.
  // Note that input parameters are totally copied in API's memory area.
  // For output parameters, only the pointer is copied.
  _api_request_params.api_input_params       = * pApiInputParams;
  _api_request_params.p_api_output_params    = pApiOutputParams;
  _api_request_params.p_api_output_user_data = pApiOutputUserData;
  // Move input user data to request structure, using the control block to know the user data size
  _input_user_data_size = 0;
  if ( pApiInputUserData != NULL )
  {
    _input_user_data_size = (WORD) *((CONTROL_BLOCK *) pApiInputUserData);
    if ( _input_user_data_size > GLB_API.max_iud_size )
    {
      _input_user_data_size = GLB_API.max_iud_size;
    }
    memcpy (_api_request_params.api_input_user_data_buffer, pApiInputUserData, _input_user_data_size);
  }

  // Compute variable request size. Size depends on user input data size
  _api_request_size = sizeof (_api_request_params) - sizeof (_api_request_params.api_input_user_data_buffer) + _input_user_data_size;

  // Mode synchronous. Create event to synchronize execution
  if ( pApiInputParams->mode == API_MODE_SYNC )
  {
    // Create a private event to synchronize execution
    _ev_sync_mode = CreateEvent (NULL,  // Event attributes
                                 FALSE, // ManualReset
                                 FALSE, // InitialState
                                 NULL); // Name
    if ( _ev_sync_mode == NULL )
    {
      // Logger message
      _stprintf (_aux_tchar_1, _T("%lu"), GetLastError ());
      Common_API_Log (LOG_CODE_CALL_FAILED, pFunctionName, _aux_tchar_1, _T("CreateEvent"), _T(""));

      return API_STATUS_EVENT_OBJECT_ERROR;
    }
    // Set event to synchronize execution end
    _api_request_params.api_input_params.event_object = _ev_sync_mode;
  }

  // Select thread queue depending on request
  _request_queue = GLB_API.p_select_queue (pApiInputParams);

  // Send request
  _call_status = Queue_Write (_request_queue, &_api_request_params, _api_request_size, QUEUE_MODE_BLOCK);
  if ( _call_status != QUEUE_STATUS_OK )
  {
    // Logger message
    _stprintf (_aux_tchar_1, _T("%hu"), _call_status);
    Common_API_Log (LOG_CODE_CALL_FAILED, pFunctionName, _aux_tchar_1, _T("Queue_Write"), _T(""));

    // Free event
    Common_API_CloseEvent (_ev_sync_mode);

    return API_STATUS_NO_RESOURCES_AVAILABLE;
  }

  // Mode synchronous
  if ( pApiInputParams->mode == API_MODE_SYNC )
  {
    // Wait with using the private event
    _wait_status = WaitForSingleObject (_ev_sync_mode,  // Handle
                                        INFINITE);      // Milliseconds

    switch ( _wait_status )
    {
      case WAIT_OBJECT_0:
        // Execution end
      break;

      case WAIT_FAILED:

      default:
        // Logger message
        _stprintf (_aux_tchar_1, _T("%lu"), GetLastError ());
        Common_API_Log (LOG_CODE_CALL_FAILED, pFunctionName, _aux_tchar_1, _T("WaitForSingleObject"), _T(""));

        // Free event
        Common_API_CloseEvent (_ev_sync_mode);

        return API_STATUS_EVENT_OBJECT_ERROR;

      break;

    } // switch

    // Free event
    Common_API_CloseEvent (_ev_sync_mode);

  }

  return API_STATUS_OK;

} // Common_API

//-----------------------------------------------------------------------------
// PURPOSE: Checks API module initialized.
//          If initialization was not tried previously, it tries to initialize it.
//
//  PARAMS:
//     - INPUT:
//      - FunctionName logger message
//
//     - OUTPUT: None
//
// RETURNS:
//  - TRUE: OK
//  - FALSE: API not initialized
//
// NOTES:
//
BOOL Common_API_CheckStatus (TCHAR * pFunctionName)
{
  // Check API stopped
  if ( GLB_API.stopped )
  {
    // Logger message
    Common_API_Log (LOG_CODE_UNEXPECTED, pFunctionName, _T("API Stopped"), _T("TRUE"), _T(""));

    return FALSE;
  }

  // Check if module initialization was tried
  if ( ! GLB_API.init_tried )
  {
    if ( GLB_API.p_try_init != NULL )
    {
      // Try to initialize
      GLB_API.p_try_init ();
    }
  }

  if ( ! GLB_API.init )
  {
    // Logger message
    Common_API_Log (LOG_CODE_UNEXPECTED, pFunctionName, _T("API Initialized"), _T("FALSE"), _T(""));

    return FALSE;
  }

  return TRUE;

} // Common_API_CheckStatus


//------------------------------------------------------------------------------
// PURPOSE: Notifies user about the status of the executed request.
//          This notification must be done only if API execution mode is
//          <> NO_WAIT.
//
//  PARAMS:
//      - INPUT:
//        - RequestOutput1, RequestOutput2: Request output status
//        - pApiInputParams
//      - OUTPUT:
//        - pApiOutputParams: User data updated
//
// RETURNS:
//
//   NOTES:
//
void Common_API_NotifyRequestStatus (WORD RequestOutput1,
                                     WORD RequestOutput2,
                                     API_INPUT_PARAMS  * pApiInputParams,
                                     API_OUTPUT_PARAMS * pApiOutputParams)
{
  static TCHAR _function_name[] = _T("Common_API_NotifyRequestStatus");
  TCHAR  _aux_tchar_1[COMMON_API_AUX_STRING_LEN];

  BOOL  _bool_rc;

  // Set completion status when no wait mode used
  if ( pApiInputParams->mode != API_MODE_NO_WAIT )
  {
    pApiOutputParams->output_1 = RequestOutput1;
    pApiOutputParams->output_2 = RequestOutput2;

    // Set execution end using the user event object
    _bool_rc = SetEvent (pApiInputParams->event_object);
    if ( ! _bool_rc )
    {
      // Logger message
      _stprintf (_aux_tchar_1, _T("%lu"), GetLastError ());
      Common_API_Logger (LOG_CODE_CALL_FAILED, _aux_tchar_1, _T("SetEvent"), _T(""));
    }
  }

} // Common_API_NotifyRequestStatus


//-----------------------------------------------------------------------------
// PURPOSE: Sets the init status and that the API tried to initialize.
//
//  PARAMS:
//     - INPUT:
//      - InitStatus
//
//     - OUTPUT: None
//
// RETURNS:
//
// NOTES:
//
void Common_API_Init (BOOL InitStatus)
{
  GLB_API.init_tried = TRUE;
  GLB_API.init       = InitStatus;
}
//-----------------------------------------------------------------------------
// PURPOSE: Sets the stop status to TRUE
//
//  PARAMS:
//     - INPUT: None
//
//     - OUTPUT: None
//
// RETURNS:
//
// NOTES:
//
void Common_API_Stop (void)
{
  GLB_API.stopped = TRUE;
}

//------------------------------------------------------------------------------
// PURPOSE: Starts a thread.
//
//  PARAMS:
//      - INPUT:
//        - pThreadRoutine
//        - ThreadParameter
//        - StartNotifyEvent: (optional) Event used to synchronize
//                            thread starting. NULL if sync if not necessary
//      - OUTPUT: None
//
// RETURNS:
//  - TRUE: OK
//  - FALSE: Error
//
//   NOTES:
//
BOOL Common_API_StartThread (LPTHREAD_START_ROUTINE pThreadRoutine,
                             VOID                   * pThreadParameter,
                             HANDLE                 StartNotifyEvent,
                             TCHAR                  * pThreadName)
{
  static TCHAR _function_name [] = _T("Common_API_StartThread");
  TCHAR  _aux_tchar_1 [COMMON_API_AUX_STRING_LEN];

  DWORD  _thread_id;
  DWORD  _wait_status;

  __try
  {
    // Start API threads
    if ( ! Common_CreateThread (NULL,              // ThreadAttributes
                                THREAD_MAX_STACK,  // StackSize
                                pThreadRoutine,    // StartAddress
                                pThreadParameter,  // Parameter
                                0,                 // CreationFlags
                                &_thread_id,       // ThreadId
                                pThreadName,
                                NULL,
                                0,
                                NULL) )

    {
      // Error creating thread
      // Logger message
      _stprintf (_aux_tchar_1, _T("%lu"), GetLastError ());
      Common_API_Logger (LOG_CODE_CALL_FAILED, _aux_tchar_1, _T("CREATE_THREAD"), _T(""));

      return FALSE;
    }

    if ( StartNotifyEvent != NULL )
    {
      // Wait for thread start
      _wait_status = WaitForSingleObject (StartNotifyEvent, // Handle
                                          INFINITE);        // Milliseconds

      switch ( _wait_status )
      {
        case WAIT_OBJECT_0:
          // Thread started
        break;

        case WAIT_FAILED:

        default:
          // Logger message
          _stprintf (_aux_tchar_1, _T("%lu"), GetLastError ());
          Common_API_Logger (LOG_CODE_CALL_FAILED, _aux_tchar_1, _T("WaitForSingleObject"), _T(""));

          return FALSE;

        break;

      } // switch
    }

    return TRUE;

  } // try

  __finally
  {

    if ( StartNotifyEvent != NULL )
    {
      // Free event
      Common_API_CloseEvent (StartNotifyEvent);
    }

  } // finally

} // Common_API_StartThread

//------------------------------------------------------------------------------------
//  PRIVATE FUNCTIONS IMPLEMENTATION
//------------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE: Sends the message to:
//          - PrivateLog (Client_API_UsePrivateLog)
//          - Logger
//
//  PARAMS:
//      - INPUT:
//        - LogCode (LOG_CODE_ERROR_CALLING, LOG_CODE_WRONG_SIZE, LOG_CODE_UNEXPECTED)
//        - pFunctionName
//        - pParam1
//        - pParam2
//        - pParam3
//
//      - OUTPUT: None
//
// RETURNS: Nothing
//
static void Common_API_Log (ENUM_LOG_CODE LogCode,
                            TCHAR         * pFunctionName,
                            TCHAR         * pParam1,
                            TCHAR         * pParam2,
                            TCHAR         * pParam3)
{
  TCHAR  _aux_tchar_1 [COMMON_API_AUX_STRING_LEN];
  TCHAR  _empty_param [] = _T("");
  TCHAR * p_param_1;
  TCHAR * p_param_2;
  TCHAR * p_param_3;
  WORD  _msg_id;
  WORD  _nls_id_logger;

  switch ( LogCode )
  {
    case LOG_CODE_CALL_FAILED:
    {
      _msg_id = 2;
      _nls_id_logger = NLS_ID_LOGGER(2);
    }
    break;

    case LOG_CODE_WRONG_SIZE:
    {
      _msg_id = 3;
      _nls_id_logger = NLS_ID_LOGGER(3);
    }
    break;

    case LOG_CODE_UNEXPECTED:
    {
      _msg_id = 6;
      _nls_id_logger = NLS_ID_LOGGER(7);
    }
    break;

    default:
    {
      // Unexpected MsgId
      _stprintf (_aux_tchar_1, _T("%lu"), LogCode);
      Common_API_Log (LOG_CODE_UNEXPECTED, pFunctionName, _T("LogCode"), _aux_tchar_1, _T(""));
    }
    return;
  } // switch

  p_param_1 = pParam1;
  p_param_2 = pParam2;
  p_param_3 = pParam3;
  if ( p_param_1 == NULL )
  {
    p_param_1 = _empty_param;
  }
  if ( p_param_2 == NULL )
  {
    p_param_2 = _empty_param;
  }
  if ( p_param_3 == NULL )
  {
    p_param_3 = _empty_param;
  }

  if ( GLB_API.uses_private_log )
  {
    GLB_API.p_private_log (_msg_id,
                           GLB_API.p_api_name,
                           pFunctionName,
                           p_param_1,
                           p_param_2,
                           p_param_3,
                           NULL);
  }
  else
  {
    GLB_API.p_logger (_nls_id_logger,
                      GLB_API.p_api_name,
                      pFunctionName,
                      p_param_1,
                      p_param_2,
                      p_param_3,
                      NULL);
  }

} // Common_API_Log
