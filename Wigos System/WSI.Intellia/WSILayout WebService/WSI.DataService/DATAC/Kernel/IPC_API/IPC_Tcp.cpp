//------------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : IPC_Tcp.cpp
//   DESCRIPTION : Functions and Methods to handle IPC tcp layer.
//        AUTHOR : Xavier Ib��ez
// CREATION DATE : 26-MAR-2002
// 
// REVISION HISTORY
// 
// Date        Author Description
// ----------- ------ -----------------------------------------------------------
// 26-MAR-2002 XID    Initial draft.
// 
//-------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  INCLUDES 
//------------------------------------------------------------------------------
#define INCLUDE_COMMON_API
#define INCLUDE_QUEUE_API
#define INCLUDE_IPC_API
#define INCLUDE_PRIVATE_LOG
#define INCLUDE_COMMON_DATA
#include "CommonDef.h"

#include "IPC_APIInternals.h"

//------------------------------------------------------------------------------
//  PRIVATE CONSTANTS 
//------------------------------------------------------------------------------

#define IPC_TCP_MAX_SEND_TRIES        2
#define IPC_TCP_MIN_TIME_BTW_SND_CONN 10000 // msec. Minimum time between two tcp connection tries
#define IPC_TCP_CONNECT_TIMEOUT       2     // sec. Maximum time waiting for a tcp connection

//------------------------------------------------------------------------------
//  PRIVATE DATATYPES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PRIVATE DATA STRUCTURES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PRIVATE FUNCTION PROTOTYPES 
//------------------------------------------------------------------------------

TYPE_TCP_SEND_BUFFER * IPC_Tcp_GetSendBuffer (void);
void IPC_Tcp_ReleaseSendBuffer (TYPE_TCP_SEND_BUFFER * pBuffer);

void IPC_Tcp_ConnectRemoteNode (DWORD NodeIdx,
                                DWORD NodeInstIdx);

BOOL IPC_Tcp_SetSocketBlocking (SOCKET Socket, 
                                BOOL Blocking);

void CALLBACK IPC_Tcp_RecvCompletionRoutine (IN DWORD Error, 
                                             IN DWORD Transferred, 
                                             IN LPWSAOVERLAPPED pOverlapped, 
                                             IN DWORD Flags);

void CALLBACK IPC_Tcp_SendCompletionRoutine (IN DWORD Error, 
                                             IN DWORD Transferred, 
                                             IN LPWSAOVERLAPPED pOverlapped, 
                                             IN DWORD Flags);

// AJQ 21-FEB-2003
void IPC_Tcp_ClientBufferToReceiveQueue (TYPE_TCP_CLIENT_BUFFER * pTcpClientBuffer);


DWORD WINAPI Ipc_Tcp_RecvThread (LPVOID ThreadParameter);

DWORD WINAPI Ipc_Tcp_SendThread (LPVOID ThreadParameter);

//------------------------------------------------------------------------------
//  PUBLIC FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE : Tcp/ip intialization
//
//  PARAMS :
//      - INPUT :
//      - OUTPUT :
//
// RETURNS :
//  - TRUE: OK
//  - FALSE: Error
//
//   NOTES :

BOOL IPC_Tcp_Init (void)
{
  static TCHAR _function_name[] = _T("IPC_Tcp_Init");
  TCHAR  _aux_tchar_1[IPC_AUX_STRING_LENGTH];

  int     _int_rc;
  WORD    _wsa_version_requested;
  WSADATA _wsa_data;
  
  // Init WSA
  _wsa_version_requested = MAKEWORD (2, 2); // Version 2.2
  _int_rc = WSAStartup (_wsa_version_requested, &_wsa_data);
  if ( _int_rc != 0 )
  {
    // WSA error
    // Logger message
    _stprintf (_aux_tchar_1, _T("%d"), WSAGetLastError ());
    PrivateLog (2, MODULE_NAME, _function_name, _aux_tchar_1,  _T("WSAStartup"));

    return FALSE;
   
  }

  return TRUE;  
  
} // IPC_Tcp_Init

//------------------------------------------------------------------------------
// PURPOSE : Tcp/ip local node intialization. Creates the tcp service 
//          (bind + listen).
//
//  PARAMS :
//      - INPUT :
//        - NodeIdx (index to ipc_node table)
//        - pThreadData
//      - OUTPUT :
//
// RETURNS :
//  - TRUE: OK
//  - FALSE: Error
//
//   NOTES :

BOOL IPC_Tcp_LocalNodeInit (DWORD NodeIdx, 
                            TYPE_API_THREAD_DATA * pThreadData)
{
  static TCHAR _function_name[] = _T("IPC_Tcp_LocalNodeInit");
  TCHAR  _aux_tchar_1[IPC_AUX_STRING_LENGTH];

  TYPE_TCP_THREAD_DATA  _tcp_thread_data;
  TYPE_IPC_NODE         * p_ipc_node;
  struct                sockaddr_in _addr;
  int                   _int_rc;
  BOOL                  _bool_rc;
  GROUP                 _socket_g;
  SOCKET                _socket;
  HANDLE                _tcp_accept_event;
  HANDLE                _start_notify_event;

  p_ipc_node = &GLB_IpcNodeTable.ipc_node[NodeIdx];

  if ( p_ipc_node->local_node_data.node_init )
  {
    // Node already initialized
    return TRUE;
  }

  // Create event to synchronize local node tcp start
  _start_notify_event = CreateEvent (NULL,  // Event attributes 
                                     FALSE, // ManualReset       
                                     FALSE, // InitialState     
                                     NULL); // Name             
  if ( _start_notify_event == NULL )
  {
    // Logger message
    _stprintf (_aux_tchar_1, _T("%lu"), GetLastError ());
    PrivateLog (2, MODULE_NAME, _function_name, _aux_tchar_1,  _T("CreateEvent"));

    return FALSE;
  }
  
  // Create event to synchronize accept and read network events
  _tcp_accept_event = CreateEvent (NULL,  // Event attributes 
                                   FALSE, // ManualReset       
                                   FALSE, // InitialState     
                                   NULL); // Name             
  if ( _tcp_accept_event == NULL )
  {
    // Logger message
    _stprintf (_aux_tchar_1, _T("%lu"), GetLastError ());
    PrivateLog (2, MODULE_NAME, _function_name, _aux_tchar_1,  _T("CreateEvent"));

    return FALSE;
  }
  p_ipc_node->local_node_data.tcp_accept_event = _tcp_accept_event;

  // Create local socket
  memset (&_socket_g, 0, sizeof (_socket_g));
  _socket = WSASocket (AF_INET,               // Address family
                       SOCK_STREAM,           // Socket type
                       IPPROTO_TCP,           // Protocol
                       NULL,                  // Protocol info
                       _socket_g,             // Reserved
                       WSA_FLAG_OVERLAPPED);  // Flags
  if ( _socket == INVALID_SOCKET )
  {
    // Logger message
    _stprintf (_aux_tchar_1, _T("%d"), WSAGetLastError ());
    PrivateLog (2, MODULE_NAME, _function_name, _aux_tchar_1, _T("WSASocket"));

    return FALSE;
  }
  p_ipc_node->local_node_data.node_socket = _socket;

  // Socket Bind
  _addr.sin_family       = AF_INET;
  _addr.sin_addr.s_addr  = htonl (INADDR_ANY);
  _addr.sin_port         = htons (p_ipc_node->local_node_data.tcp_port);
  _int_rc = bind (p_ipc_node->local_node_data.node_socket, (struct sockaddr *) &_addr, sizeof (_addr));
  if ( _int_rc == SOCKET_ERROR )
  {
    // Logger message
    _stprintf (_aux_tchar_1, _T("%d"), WSAGetLastError ());
    PrivateLog (2, MODULE_NAME, _function_name, _aux_tchar_1, _T("bind"));

    return FALSE;
  }

  // Socket listen
  _int_rc = listen (p_ipc_node->local_node_data.node_socket, SOMAXCONN);
  if ( _int_rc == SOCKET_ERROR )
  {
    // Logger message
    _stprintf (_aux_tchar_1, _T("%d"), WSAGetLastError ());
    PrivateLog (2, MODULE_NAME, _function_name, _aux_tchar_1, _T("listen"));

    return FALSE;
  }

  // Program socket to set an event objent when "Accept" network event occurs
  _int_rc = WSAEventSelect (p_ipc_node->local_node_data.node_socket,      // Socket
                            p_ipc_node->local_node_data.tcp_accept_event, // Event object
                            FD_ACCEPT);                                   // Network event
  if ( _int_rc == SOCKET_ERROR )
  {
    // Logger message
    _stprintf (_aux_tchar_1, _T("%d"), WSAGetLastError ());
    PrivateLog (2, MODULE_NAME, _function_name, _aux_tchar_1, _T("WSAEventSelect"));

    return FALSE;
  }

  // Start tcp thread to manage incomming connection messages and tcp messages

  memset (&_tcp_thread_data, 0, sizeof (_tcp_thread_data));
  _tcp_thread_data.node_idx           = NodeIdx;
  _tcp_thread_data.tcp_accept_event   = p_ipc_node->local_node_data.tcp_accept_event;
  _tcp_thread_data.start_notify_event = _start_notify_event;

  _bool_rc = Common_API_StartThread (Ipc_Tcp_RecvThread,
                                     &_tcp_thread_data,
                                     _start_notify_event,
                                     _T("Ipc_Tcp_RecvThread"));
  if ( !_bool_rc )
  {
    return FALSE;
  }

  // TCP initialized
  return TRUE;

} // IPC_Tcp_LocalNodeInit

//------------------------------------------------------------------------------
// PURPOSE : Finds given IP address in local machine address pool
//
//  PARAMS :
//      - INPUT :
//          - IpAddr. Ip address, dword format
//
//      - OUTPUT :
//
// RETURNS :
//  - TRUE: IP found
//  - FALSE: IP not found
//
//   NOTES :

BOOL IPC_Tcp_FindLocalAddr (DWORD IpAddr)
{
  static TCHAR _function_name[] = _T("IPC_Tcp_FindLocalAddr");
  TCHAR  _aux_tchar_1[IPC_AUX_STRING_LENGTH];

  DWORD  _idx_addr;
  int    _int_rc;
  char   _host_name[256];
  static struct hostent *_host_info = NULL;
  BOOL   _addr_found;
  DWORD  _local_addr;

  // AJQ, 27-MAY-2003. Allow the Loopback IP Addr.
  // This is only needed on Laptops to avoid using a fixed IP.
  if ( inet_addr (LOOPBACK_IP_ADDR) == IpAddr )
  {
    return TRUE;
  } // if

  // Get host name
  _int_rc = gethostname (_host_name, sizeof (_host_name));
  if ( _int_rc == SOCKET_ERROR )
  {
    // Logger message
    _stprintf (_aux_tchar_1, _T("%d"), WSAGetLastError ());
    PrivateLog (2, MODULE_NAME, _function_name, _aux_tchar_1,  _T("gethostname"));

    return FALSE;
  }

  // Get local list of IPs
  if ( !_host_info )
  {
    _host_info = gethostbyname (_host_name);
    if ( !_host_info )
    {
      // Logger message
      _stprintf (_aux_tchar_1, _T("%d"), WSAGetLastError ());
      PrivateLog (2, MODULE_NAME, _function_name, _aux_tchar_1,  _T("gethostbyname"));

      return FALSE;
    }
  }

  // Check local adaptors address list
  _idx_addr   = 0;
  _addr_found = FALSE;
  while (   !_addr_found
         && _host_info->h_addr_list[_idx_addr] != NULL )
  {
    _local_addr = *((DWORD *) _host_info->h_addr_list[_idx_addr]);
    if ( _local_addr == IpAddr )
    {
      _addr_found = TRUE;
    }
    _idx_addr++;
  } // while

  return _addr_found;

} // IPC_Tcp_FindLocalAddr

//------------------------------------------------------------------------------
// PURPOSE : Gets the initialized remote node list.
//
//  PARAMS :
//      - INPUT :
//      - OUTPUT :
//        - pNodeIdxList
//
// RETURNS : None
//
void IPC_Tcp_GetRemoteNodes (TYPE_NODE_IDX_LIST * pNodeIdxList)
{
  DWORD _idx_node;

  pNodeIdxList->num_nodes = 0;
  for ( _idx_node = 0; _idx_node < GLB_IpcNodeTable.num_nodes; _idx_node++ )
  {
    if ( GLB_IpcNodeTable.ipc_node[_idx_node].instances_data.tcp_send_init )
    {
      pNodeIdxList->node_idx[pNodeIdxList->num_nodes] = _idx_node;
      pNodeIdxList->num_nodes++;
    }
  } // for
  
} // IPC_Tcp_GetNumRemoteNodes

//------------------------------------------------------------------------------
// PURPOSE : Remote node tcp initialization:
//            - Creates tcp send thread for the node
//
//  PARAMS :
//      - INPUT :
//        - NodeIdx
//      - OUTPUT :
//
// RETURNS :
//  - TRUE: OK
//  - FALSE: Error
//
BOOL IPC_Tcp_RemoteNodeInit (DWORD NodeIdx)
{
  static TCHAR _function_name[] = _T("IPC_Tcp_RemoteNodeInit");
  TCHAR  _aux_tchar_1[IPC_AUX_STRING_LENGTH];

  TYPE_IPC_NODE         * p_ipc_node;
  TYPE_TCP_THREAD_DATA  _tcp_thread_data;
  BOOL   _bool_rc;
  WORD   _call_status;
  HANDLE _event;
  HANDLE _start_notify_event;
  
  p_ipc_node = &GLB_IpcNodeTable.ipc_node[NodeIdx];
  
  if ( p_ipc_node->instances_data.tcp_send_init )
  {
    return TRUE;
  }
  
  // Tcp send queue event
  _event = CreateEvent (NULL,  // Event attributes   
                        FALSE, // ManualReset
                        FALSE, // InitialState
                        NULL); // Name
  if ( _event == NULL )
  {
    // Logger message
    _stprintf (_aux_tchar_1, _T("%lu"), GetLastError ());
    PrivateLog (2, MODULE_NAME, _function_name, _aux_tchar_1, _T("CreateEvent"));

    return FALSE;
  }

  // Set Queue event
  p_ipc_node->instances_data.tcp_send_queue_event = _event;

  // Tcp send queue
  _call_status = Queue_CreateQueue (IPC_TCP_SEND_QUEUE_NUMBER_OF_ITEMS * IPC_TCP_SEND_QUEUE_ITEM_SIZE,
                                    &p_ipc_node->instances_data.tcp_send_queue);
  if ( _call_status != QUEUE_STATUS_OK )
  {
    // Logger message
    _stprintf (_aux_tchar_1, _T("%hu"), _call_status);
    PrivateLog (2, MODULE_NAME, _function_name, _aux_tchar_1, _T("Queue_CreateQueue"));

    return FALSE;
  }
  
  // Create event to synchronize threaf start
  _start_notify_event = CreateEvent (NULL,  // Event attributes 
                                     FALSE, // ManualReset       
                                     FALSE, // InitialState     
                                     NULL); // Name             
  if ( _start_notify_event == NULL )
  {
    // Logger message
    _stprintf (_aux_tchar_1, _T("%lu"), GetLastError ());
    PrivateLog (2, MODULE_NAME, _function_name, _aux_tchar_1,  _T("CreateEvent"));

    return FALSE;
  }
  
  // Start thread
  memset (&_tcp_thread_data, 0, sizeof (_tcp_thread_data));
  _tcp_thread_data.node_idx           = NodeIdx;
  _tcp_thread_data.start_notify_event = _start_notify_event;

  _bool_rc = Common_API_StartThread (Ipc_Tcp_SendThread,
                                     &_tcp_thread_data,
                                     _start_notify_event,
                                     _T("Ipc_Tcp_SendThread"));
  if ( !_bool_rc )
  {
    return FALSE;
  }
  
  // Remote node initialized
  p_ipc_node->instances_data.tcp_send_init = TRUE;
  
  return TRUE;
  
} // IPC_Tcp_RemoteNodeInit

//------------------------------------------------------------------------------
// PURPOSE : Writes a send request into the remote node tcp send queue
//
//  PARAMS :
//      - INPUT :
//        - NodeIdx
//        - pApiRequestParams
//      - OUTPUT :
//
// RETURNS :
//  - TRUE: OK
//  - FALSE: Error
//
BOOL IPC_Tcp_WriteSendRequest (DWORD NodeIdx,
                               TYPE_TCP_SEND_REQUEST * pTcpSendRequest)
{
  static TCHAR _function_name[] = _T("IPC_Tcp_WriteSendRequest");
  TCHAR  _aux_tchar_1[IPC_AUX_STRING_LENGTH];

  TYPE_IPC_NODE * p_ipc_node;
  WORD  _call_status;
  
  p_ipc_node = &GLB_IpcNodeTable.ipc_node[NodeIdx];

  _call_status = Queue_Write (p_ipc_node->instances_data.tcp_send_queue, 
                              pTcpSendRequest, 
                              (WORD) pTcpSendRequest->control_block, 
                              QUEUE_MODE_BLOCK);
  if ( _call_status != QUEUE_STATUS_OK )
  {
    _stprintf (_aux_tchar_1, _T("%hu"), _call_status);
    PrivateLog (2, MODULE_NAME, _function_name, _aux_tchar_1, _T("Queue_Write"));
    
    return FALSE;
  }
  
  return TRUE;
  
} // IPC_Tcp_WriteSendRequest

//------------------------------------------------------------------------------
// PURPOSE : Sends a tcp message. 
//          Note that tcp send operation takes care of possible 
//          tcp fragmentation.
//
//  PARAMS :
//      - INPUT :
//        - NodexIdx
//        - NodeInstIdx
//        - SendDataLength
//        - pSendDataBuffer
//      - OUTPUT :
//
// RETURNS :
//    - TRUE: OK
//    - FALSE: Error
//
//   NOTES : This function has the same format as a thread
//           because it is called from a system queue work (system thread pool).

BOOL WINAPI IPC_Tcp_Send (DWORD NodexIdx,
                          DWORD NodeInstIdx,
                          DWORD SendDataLength,
                          BYTE  * pSendDataBuffer)
{
  static TCHAR _function_name[] = _T("IPC_Tcp_Send");
  TCHAR  _aux_tchar_1[IPC_AUX_STRING_LENGTH];
  TYPE_INSTANCE_ENTRY   * p_instance_entry;
  TYPE_TCP_SEND_BUFFER  * p_tcp_send_buffer;
  int                   _int_rc;
  DWORD                 _idx_send_try;
  int                   _socket_error;
  BOOL                  _msg_sent;
  BOOL                  _try_again;
  DWORD                 _flags;
  DWORD                 _num_bytes_sent;

  // Copy task parameters before notify parent
  p_instance_entry = &GLB_IpcNodeTable.ipc_node[NodexIdx].instances_data.instance_entry[NodeInstIdx];

  if ( p_instance_entry->in_use == FALSE )
  {
    return FALSE;
  }
  
  // Connect to target node
  IPC_Tcp_ConnectRemoteNode (NodexIdx,      // Node index
                             NodeInstIdx);  // Node instance index

  // Send message only if socket connected
  _msg_sent  = FALSE;
  _try_again = TRUE;
  for ( _idx_send_try = 0; 
        _idx_send_try < IPC_TCP_MAX_SEND_TRIES && _try_again && !_msg_sent; 
        _idx_send_try++)
  {
    if ( p_instance_entry->socket_status != IPC_SOCKET_CONNECTED )
    {
      // Do not try again if socket connection failed
      _try_again = FALSE;
      continue;
    }

    // Get dynamic send buffer
    p_tcp_send_buffer = IPC_Tcp_GetSendBuffer ();
    if ( p_tcp_send_buffer == NULL )
    {
      // Memory error
      _stprintf (_aux_tchar_1, _T("NULL"));
      PrivateLog (2, MODULE_NAME, _function_name, _aux_tchar_1, _T("IPC_Tcp_GetSendBuffer"));
      return FALSE;
    }

    memset (&p_tcp_send_buffer->wsa_overlapped, 0, sizeof (p_tcp_send_buffer->wsa_overlapped));
    p_tcp_send_buffer->wsa_buffer.len = SendDataLength;
    p_tcp_send_buffer->wsa_buffer.buf = (char *) p_tcp_send_buffer->user_buffer;
    memcpy (p_tcp_send_buffer->user_buffer, pSendDataBuffer, SendDataLength);
    
    _flags          = 0;
    _num_bytes_sent = 0;

    // Send message
    _int_rc = WSASend (p_instance_entry->node_socket,       // Socket
                       &p_tcp_send_buffer->wsa_buffer,      // WSA buffers
                       1,                                   // WSA buffer count
                       &_num_bytes_sent,                    // Num bytes sent
                       _flags,                              // Flags
                       &p_tcp_send_buffer->wsa_overlapped,  // Overlapped structure
                       IPC_Tcp_SendCompletionRoutine);      // Completion routine

    if ( _int_rc != SOCKET_ERROR )
    {
      // Message sent OK
      _msg_sent = TRUE;
      continue;
    }

    _socket_error = WSAGetLastError ();

    switch ( _socket_error )
    {
      case WSA_IO_PENDING :
        // Message sent OK (overlapped)
        _msg_sent = TRUE;
        continue;
      break;

      default :
        // Release send buffer
        IPC_Tcp_ReleaseSendBuffer (p_tcp_send_buffer);
        
        // Send error
        // Set socket disconnected
        p_instance_entry->socket_status = IPC_SOCKET_NOT_CONNECTED;

        // Try to reconnect and next try
        _socket_error = WSAGetLastError ();
        
        // Try to reconnect only if there is a connexion error
        // If no connexion was stablished there is no retry
        if ( _socket_error != WSAENOTCONN )
        {
          // Reconnect this instance
          IPC_Tcp_ConnectRemoteNode (NodexIdx,      // Node index
                                     NodeInstIdx);  // Node instance index
          // Retry
          continue;
        }
        else
        {
          // No retry
          _try_again   = FALSE;
          continue;
        }
      break;
    } // switch

  } // for

  return _msg_sent;
    
} // IPC_Tcp_Send

//------------------------------------------------------------------------------
//  PRIVATE FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE : Allocs a dynamic buffer from process heap.
//
//  PARAMS :
//      - INPUT :
//      - OUTPUT :
//
// RETURNS :
//  - NULL: error
//  - <> NULL: Buffer allocated
//
//   NOTES :

TYPE_TCP_SEND_BUFFER * IPC_Tcp_GetSendBuffer (void)
{
  static TCHAR _function_name[] = _T("IPC_Tcp_GetSendBuffer");
  TCHAR  _aux_tchar_1[IPC_AUX_STRING_LENGTH];
  TYPE_TCP_SEND_BUFFER * p_out;

  p_out = (TYPE_TCP_SEND_BUFFER *) HeapAlloc (GetProcessHeap (),              // handle to private heap block
                                              HEAP_ZERO_MEMORY,               // heap allocation control
                                              sizeof (TYPE_TCP_SEND_BUFFER)); // number of bytes to allocate

  if ( p_out == NULL )
  {
    // Memory error
    _stprintf (_aux_tchar_1, _T("NULL"));
    PrivateLog (2, MODULE_NAME, _function_name, _aux_tchar_1, _T("HeapAlloc"));
  }

  return p_out;

} // IPC_Tcp_GetSendBuffer

//------------------------------------------------------------------------------
// PURPOSE : Releases a dynamic buffer.
//
//  PARAMS :
//      - INPUT :
//      - OUTPUT :
//
// RETURNS :
//
//   NOTES :

void IPC_Tcp_ReleaseSendBuffer (TYPE_TCP_SEND_BUFFER * pBuffer)
{
  static TCHAR _function_name[] = _T("IPC_Tcp_ReleaseSendBuffer");
  TCHAR  _aux_tchar_1[IPC_AUX_STRING_LENGTH];

  if ( !HeapFree (GetProcessHeap (),    // handle to heap
                  HEAP_ZERO_MEMORY,     // heap free options
                  pBuffer) )            // pointer to memory
  {
    // Memory error
    _stprintf (_aux_tchar_1, _T("NULL"));
    PrivateLog (2, MODULE_NAME, _function_name, _aux_tchar_1, _T("HeapFree"));
  }

  return;

} // IPC_Tcp_ReleaseSendBuffer

//------------------------------------------------------------------------------
// PURPOSE : Tcp Client buffer list management routines.
//          Cliente buffers are used every time a tcp client request a 
//          connection to a local node tcp service.
//
//  PARAMS :
//      - INPUT :
//        - pIpcNode
//      - OUTPUT :
//        - p_tcp_client_buffer: pointer to a client buffer
//
// RETURNS :
//  - TRUE: OK
//  - FALSE: No free client buffers
//
//   NOTES :

BOOL IPC_Tcp_GetFreeClientBuffer (TYPE_IPC_NODE             * pIpcNode, 
                                  TYPE_TCP_CLIENT_BUFFER    ** pTcpClientBuffer)
{
  static TCHAR _function_name[] = _T("IPC_Tcp_GetFreeClientBuffer");

  DWORD _tcp_client_buffer_idx;
  
  *pTcpClientBuffer = NULL;
  for ( _tcp_client_buffer_idx = 0; _tcp_client_buffer_idx < IPC_MAX_LOCAL_NODE_CLIENTS; _tcp_client_buffer_idx++)
  {
    if ( !pIpcNode->local_node_data.tcp_client_buffer[_tcp_client_buffer_idx].in_use )
    {
      // Free buffer found
      *pTcpClientBuffer = &pIpcNode->local_node_data.tcp_client_buffer[_tcp_client_buffer_idx];
      (*pTcpClientBuffer)->in_use = TRUE;
      if ( (*pTcpClientBuffer)->p_wsa_buffer == NULL )
      {
        // Allocate buffer
        (*pTcpClientBuffer)->wsa_buffer_size = IPC_WSA_BUFFER_SIZE;
        (*pTcpClientBuffer)->p_wsa_buffer    = (BYTE *) LocalAlloc (LPTR, (*pTcpClientBuffer)->wsa_buffer_size);
        if ( (*pTcpClientBuffer)->p_wsa_buffer == NULL )
        {
          // Error allocating memory
          PrivateLog (2, MODULE_NAME, _function_name, _T("NULL"), _T("LocalAlloc"));
          return FALSE;
        }
      }
      return TRUE;
    }
  } // for
  
  // Logger message
  PrivateLog (6, MODULE_NAME, _function_name, _T("client_buffer"), _T("Overflow"));
  return FALSE;
  
} // IPC_Tcp_GetFreeClientBuffer

//------------------------------------------------------------------------------
// PURPOSE : Tcp Client buffer list management routines
//
//  PARAMS :
//      - INPUT :
//        - p_tcp_client_buffer: pointer to a client buffer
//      - OUTPUT :
//
// RETURNS :
//
//   NOTES :
//
void IPC_Tcp_ReleaseClientBuffer (TYPE_TCP_CLIENT_BUFFER * pTcpClientBuffer)
{

  pTcpClientBuffer->in_use = FALSE;
  
} // IPC_Tcp_ReleaseClientBuffer

//------------------------------------------------------------------------------
// PURPOSE : Calls WSARecv in overlapped mode and with completion routine (APC)
//
//  PARAMS :
//      - INPUT :
//        - p_tcp_client_buffer
//      - OUTPUT :
//
// RETURNS :
//  - TRUE: OK
//  - FALSE: Error
//
//   NOTES :
//
BOOL IPC_Tcp_CallWSARecv (TYPE_TCP_CLIENT_BUFFER * p_tcp_client_buffer)
{
  static TCHAR  _function_name[] = _T("IPC_Tcp_CallWSARecv");
  TCHAR         _aux_tchar_1[IPC_AUX_STRING_LENGTH];
  WSABUF        _wsabuf;
  DWORD         _flags;
  int           _int_rc;

  _flags      = 0;
  _wsabuf.buf = (char *) &p_tcp_client_buffer->p_wsa_buffer[p_tcp_client_buffer->wsa_buffer_index];
  _wsabuf.len = p_tcp_client_buffer->wsa_buffer_size - p_tcp_client_buffer->wsa_buffer_index;
  memset (&p_tcp_client_buffer->wsa_overlapped, 0, sizeof (p_tcp_client_buffer->wsa_overlapped));
  p_tcp_client_buffer->wsa_overlapped.hEvent = p_tcp_client_buffer; // Event is used to pass IPC node client buffer to completion routine

  _int_rc = WSARecv (p_tcp_client_buffer->socket_c,            // Client Socket
                    &_wsabuf,                                  // Buffer list
                    1,                                         // Number of buffer in buffer list
                    &p_tcp_client_buffer->wsa_received_bytes,  // Received bytes
                    &_flags,                                   // Flags
                    &p_tcp_client_buffer->wsa_overlapped,      // Overlapped
                    IPC_Tcp_RecvCompletionRoutine);            // Completion routine

  if ( _int_rc == 0 )
  {
    // AJQ 24-FEB-2003
    // Receive operation completed immediatly
    // The completion routine has been also scheduled and
    // will be called later ...
  }
  else
  {
    _int_rc = WSAGetLastError ();
    if (   _int_rc != WSA_IO_PENDING
        && _int_rc != WSAECONNRESET  )
    {
      _stprintf (_aux_tchar_1, _T("%d"), _int_rc);
      PrivateLog (2, MODULE_NAME, _function_name, _aux_tchar_1,  _T("WSARecv"));

      return FALSE;
    }
  }
 
  return TRUE;
  
} // IPC_Tcp_CallWSARecv

//------------------------------------------------------------------------------
// PURPOSE : Move any/all received message(s) from the client buffer to the 
//           receive queue.
//
//  PARAMS :
//    - INPUT : 
//     - pTcpClientBuffer :
//
//    - OUTPUT : 
//
// RETURNS :
//
//   NOTES : This routine is to avoid recursivity between the 
//           IPC_Tcp_CallWSARecv & IPC_Tcp_RecvCompletionRoutine

void IPC_Tcp_ClientBufferToReceiveQueue (TYPE_TCP_CLIENT_BUFFER * pTcpClientBuffer)
{
  static TCHAR _function_name [] = _T("IPC_Tcp_ClientBufferToReceiveQueue");
  TCHAR  _aux_tchar_1[IPC_AUX_STRING_LENGTH];

  WORD                    _call_status;
  BOOL                    _data_defrag_pending;
  BOOL                    _total_msg_received;
  CONTROL_BLOCK           _msg_cb;
  
  // Check IPC stopped
  if ( GLB_API.stopped )
  {
    // IPC was stopped: nothing to do
    return;
  }
  
  _data_defrag_pending = TRUE;
  while ( _data_defrag_pending )
  {
    // Process tcp fragmentation
    _total_msg_received = FALSE;

    // Get msg length. Note that first DWORD of the message contains the message control block
    if ( pTcpClientBuffer->wsa_buffer_index >= sizeof (_msg_cb) )
    {
      _msg_cb = *((CONTROL_BLOCK *) pTcpClientBuffer->p_wsa_buffer);
      if ( pTcpClientBuffer->wsa_buffer_index >= _msg_cb )
      {
        _total_msg_received = TRUE;
      }
      else
      {
        // Finish data defragmentation
        _data_defrag_pending = FALSE;
      }
    }
    else
    {
      // Not enough data: Wait for more data in next reception
      _data_defrag_pending = FALSE;
    }
    
    if ( _total_msg_received )
    {
      _call_status = Queue_Write (pTcpClientBuffer->receive_queue, 
                                  pTcpClientBuffer->p_wsa_buffer, 
                                  (WORD) _msg_cb, 
                                  QUEUE_MODE_BLOCK);
      if ( _call_status != QUEUE_STATUS_OK )
      {
        _stprintf (_aux_tchar_1, _T("%hu"), _call_status);
        PrivateLog (2, MODULE_NAME, _function_name, _aux_tchar_1, _T("Queue_Write"));
      }
        
      // Check for more IPC messages in the tcp buffer
      if ( pTcpClientBuffer->wsa_buffer_index > _msg_cb )
      {
        // New message available
        pTcpClientBuffer->wsa_buffer_index = pTcpClientBuffer->wsa_buffer_index - _msg_cb;
        
        // Move data to beginning of the buffer
        memmove (pTcpClientBuffer->p_wsa_buffer,           // Destination
                 &pTcpClientBuffer->p_wsa_buffer[_msg_cb], // Source: New message
                 pTcpClientBuffer->wsa_buffer_index);      // WSA buffer remaining data size
      }
      else
      {
        // Reset buffer for next read operation
        pTcpClientBuffer->wsa_buffer_index = 0;
        _data_defrag_pending = FALSE;
      }
    }
  } // while

  return;

} // IPC_Tcp_ClientBufferToReceiveQueue

//------------------------------------------------------------------------------
// PURPOSE : WSARecv completion routine. Executed when a new client 
//          messages arrives. The received data is written to the local node 
//          receive queue.
//          Data reception takes care of data fragmentation.
//
//  PARAMS :
//      - INPUT :
//      - OUTPUT :
//
// RETURNS :
//
//   NOTES :

void CALLBACK IPC_Tcp_RecvCompletionRoutine (IN DWORD Error, 
                                             IN DWORD Transferred, 
                                             IN LPWSAOVERLAPPED pOverlapped, 
                                             IN DWORD Flags)
{
  static TCHAR _function_name[] = _T("IPC_Tcp_RecvCompletionRoutine");

  TYPE_TCP_CLIENT_BUFFER  * p_tcp_client_buffer;
  
  p_tcp_client_buffer = (TYPE_TCP_CLIENT_BUFFER *) pOverlapped->hEvent;
  
  if ( Transferred == 0 || Error != 0 )
  {
    // Close socket
    closesocket (p_tcp_client_buffer->socket_c);
    
    // Receive error: free client buffer
    IPC_Tcp_ReleaseClientBuffer (p_tcp_client_buffer);
    
    // No next receive operation
    return;
  }

  // TODO Why don't check this at the beggining?
  // Check IPC stopped
  if ( GLB_API.stopped )
  {
    // IPC was stopped: nothing to do
    return;
  }
  
  // Update received message  
  p_tcp_client_buffer->wsa_buffer_index += Transferred;

  IPC_Tcp_ClientBufferToReceiveQueue (p_tcp_client_buffer);
  
  // Program next receive method using call back
  IPC_Tcp_CallWSARecv (p_tcp_client_buffer);

  return;

} // IPC_Tcp_RecvCompletionRoutine

//------------------------------------------------------------------------------
// PURPOSE : WSARecv completion routine. Executed when a new client 
//          messages arrives. The received data is written to the local node 
//          receive queue.
//          Data reception takes care of data fragmentation.
//
//  PARAMS :
//      - INPUT :
//      - OUTPUT :
//
// RETURNS :
//
//   NOTES :

void CALLBACK IPC_Tcp_SendCompletionRoutine (IN DWORD Error, 
                                             IN DWORD Transferred, 
                                             IN LPWSAOVERLAPPED pOverlapped, 
                                             IN DWORD Flags)
{
  static TCHAR _function_name[] = _T("IPC_Tcp_SendCompletionRoutine");
  TCHAR  _aux_tchar_1[IPC_AUX_STRING_LENGTH];
  TYPE_TCP_SEND_BUFFER  * p_tcp_send_buffer;
  
  p_tcp_send_buffer = (TYPE_TCP_SEND_BUFFER *) pOverlapped;
  
  if ( Error == 0 )
  {
    // Mark socket as not connected? close socket?
    if ( Transferred != p_tcp_send_buffer->wsa_buffer.len )
    {
      // Fragmentation error
      _stprintf (_aux_tchar_1, _T("data_fragmentation"));
      PrivateLog (2, MODULE_NAME, _function_name, _aux_tchar_1,  _T("WSASend"));
    }
  }

  // Release buffer
  
  IPC_Tcp_ReleaseSendBuffer (p_tcp_send_buffer);
  
  return;

} // IPC_Tcp_SendCompletionRoutine

//------------------------------------------------------------------------------
// PURPOSE : Processes a request connection from tcp client.
//          Accepts the connection and calls the first Receive on the client socket.
//
//  PARAMS :
//      - INPUT :
//        - EventId
//      - OUTPUT :
//
// RETURNS :
//
//   NOTES :

void IPC_Tcp_ConnectionAccepted (DWORD NodeIdx)
{
  static TCHAR _function_name[] = _T("IPC_Tcp_ConnectionAccepted");
  TCHAR  _aux_tchar_1[IPC_AUX_STRING_LENGTH];
  
  BOOL                    _bool_rc;
  TYPE_IPC_NODE           * p_ipc_node;
  TYPE_TCP_CLIENT_BUFFER  * p_tcp_client_buffer;
  SOCKET                  _socket_c;

  p_ipc_node = &GLB_IpcNodeTable.ipc_node[NodeIdx];

  // Accept connection
  _socket_c = WSAAccept (p_ipc_node->local_node_data.node_socket, // Socket
                         NULL,                                    // Client address
                         NULL,
                         NULL,                                    // Accept function condition
                         0);
  if ( _socket_c == INVALID_SOCKET )
  {
    // Logger message
    _stprintf (_aux_tchar_1, _T("%d"), WSAGetLastError ());
    PrivateLog (2, MODULE_NAME, _function_name, _aux_tchar_1,  _T("WSAAccept"));

    return;
  }
  
  // Get a free client buffer
  _bool_rc = IPC_Tcp_GetFreeClientBuffer (p_ipc_node, &p_tcp_client_buffer);
  if ( !_bool_rc )
  {
    return;
  }
  
  p_tcp_client_buffer->wsa_buffer_index = 0;
  p_tcp_client_buffer->socket_c         = _socket_c;
  p_tcp_client_buffer->receive_queue    = p_ipc_node->local_node_data.receive_queue;
  
  // Program receive method using call back
  IPC_Tcp_CallWSARecv (p_tcp_client_buffer);
  
} // IPC_Tcp_ConnectionAccepted

//------------------------------------------------------------------------------
// PURPOSE : Thread to receive tcp incomming connection and messages 
//          for a local node.
//          Tcp messages are received via APC (callback)
//
//  PARAMS :
//      - INPUT :
//        - ThreadParameter
//      - OUTPUT : None
//
// RETURNS :
//
//   NOTES :

DWORD WINAPI Ipc_Tcp_RecvThread (LPVOID ThreadParameter)
{
  static TCHAR _function_name[] = _T("Ipc_Tcp_RecvThread");
  TCHAR  _aux_tchar_1[IPC_AUX_STRING_LENGTH];

  DWORD  _wait_status;
  BOOL   _bool_rc;
  TYPE_TCP_THREAD_DATA _tcp_thread_data;

  _tcp_thread_data = *((TYPE_TCP_THREAD_DATA *) ThreadParameter);

  // Notify start execution to parent
  _bool_rc = SetEvent (_tcp_thread_data.start_notify_event);
  if ( ! _bool_rc )
  {
    // Logger message
    _stprintf (_aux_tchar_1, _T("%lu"), GetLastError ());
    PrivateLog (2, MODULE_NAME, _function_name, _aux_tchar_1, _T("SetEvent"));
  }
  
  // Infinite loop to connection event
  while ( TRUE ) 
  {
    // Wait with using the private event
    _wait_status = WaitForSingleObjectEx (_tcp_thread_data.tcp_accept_event, // Handle
                                          INFINITE,                          // Milliseconds
                                          TRUE);                             // Alertable
    switch ( _wait_status )
    {
      case WAIT_IO_COMPLETION:
        // Completion routine
      break;
      
      case WAIT_OBJECT_0:
        // Connection to local service accepted
        IPC_Tcp_ConnectionAccepted (_tcp_thread_data.node_idx);
      break;
      
      default:
        // Error
        // Logger message
        _stprintf (_aux_tchar_1, _T("%lu"), GetLastError ());
        PrivateLog (2, MODULE_NAME, _function_name, _aux_tchar_1, _T("WaitForSingleObjectsEx"));
   
        return 0;

      break;
      
    } // switch

  } // while

  return 0;

} // Ipc_Tcp_RecvThread

//------------------------------------------------------------------------------
// PURPOSE : Processing of send request:
//            - Data to send
//            - Token to syncrhonize send user operations
//
//  PARAMS :
//      - INPUT :
//        - pTcpSendRequest
//      - OUTPUT : None
//
// RETURNS :
//
//   NOTES :

void IPC_Tcp_ProcessSendRequest (TYPE_TCP_SEND_REQUEST * pTcpSendRequest)
{
  BOOL   _bool_rc;
  DWORD  _idx_inst;
  BOOL   _node_available;
  WORD   _request_output_1 = IPC_STATUS_ERROR;  // Default values
  WORD   _request_output_2 = 0;
  BOOL   _request_exec_end = TRUE;
  
  TYPE_SEND_TOKEN * p_send_token;

  switch ( pTcpSendRequest->request_type )
  {
    case IPC_TCP_SEND_REQ_DATA:

      // Default execution status = Error. 
      // Only if one send is ok, then the user send operation is ok

      for ( _idx_inst = pTcpSendRequest->first_node_inst_idx; _idx_inst < pTcpSendRequest->last_node_inst_idx; _idx_inst++ )
      {
        if ( GLB_IpcNodeTable.ipc_node[pTcpSendRequest->node_idx].instances_data.instance_entry[_idx_inst].in_use )
        {
          // Check remote node tcp availavility before try a send operation
          _node_available = IPC_Tcp_CheckRemoteNodeAvail (pTcpSendRequest->node_idx, _idx_inst);
          if ( !_node_available )
          {
            // Next node
            continue;
          }
          
          _bool_rc = IPC_Tcp_Send (pTcpSendRequest->node_idx,
                                  _idx_inst,
                                  pTcpSendRequest->ipc_oud_receive.control_block,
                                  (BYTE *) &pTcpSendRequest->ipc_oud_receive);
          if ( !_bool_rc )
          {
            // Try next instance
            continue;
          }
          else
          {
            // Update current send status to ok, because at least one send was ok
            _request_output_1 = IPC_STATUS_OK;
          }
        }
      } // for

    break;
    
    case IPC_TCP_SEND_REQ_WAIT:

      _request_output_1 = IPC_STATUS_OK;
      _request_exec_end = FALSE;

      memcpy (&p_send_token, &pTcpSendRequest->ipc_oud_receive, sizeof (p_send_token));

      // Decrement token counter, and notify request end if counter becomes 0
      EnterCriticalSection (&p_send_token->cs_token_counter);
      if ( p_send_token->token_counter > 0 )
      {
        p_send_token->token_counter--;
        if ( p_send_token->token_counter == 0 )
        {
          _request_exec_end = TRUE;
          IPC_Misc_ReleaseSendToken (p_send_token);
        }
      }
      LeaveCriticalSection (&p_send_token->cs_token_counter);

    break;
    
    default:

      // Unknown function code
      _request_output_1 = IPC_STATUS_ERROR;
      _request_output_2 = 0;
      _request_exec_end = TRUE;

    break;

  } // switch

  if ( _request_exec_end )
  {
    // Notify completion status to user
    Common_API_NotifyRequestStatus (_request_output_1,
                                    _request_output_2,
                                    &pTcpSendRequest->api_input_paramas,
                                    pTcpSendRequest->p_api_output_params);

  }

} // IPC_Tcp_ProcessSendRequest

//------------------------------------------------------------------------------
// PURPOSE : Thread to send tcp outgoing messages for a temote node.
//
//  PARAMS :
//      - INPUT :
//        - ThreadParameter
//      - OUTPUT : None
//
// RETURNS :
//
//   NOTES :

DWORD WINAPI Ipc_Tcp_SendThread (LPVOID ThreadParameter)
{
  static TCHAR _function_name[] = _T("Ipc_Tcp_SendThread");
  TCHAR  _aux_tchar_1[IPC_AUX_STRING_LENGTH];

  DWORD  _wait_status;
  BOOL   _bool_rc;
  WORD   _call_status;
  DWORD  _buffer_length;
  TYPE_TCP_SEND_REQUEST _tcp_send_request;
  TYPE_IPC_NODE         * p_ipc_node;
  TYPE_TCP_THREAD_DATA  _tcp_thread_data;

  _tcp_thread_data = *((TYPE_TCP_THREAD_DATA *) ThreadParameter);

  // Notify start execution to parent
  _bool_rc = SetEvent (_tcp_thread_data.start_notify_event);
  if ( ! _bool_rc )
  {
    // Logger message
    _stprintf (_aux_tchar_1, _T("%lu"), GetLastError ());
    PrivateLog (2, MODULE_NAME, _function_name, _aux_tchar_1, _T("SetEvent"));
  }
  
  p_ipc_node = &GLB_IpcNodeTable.ipc_node[_tcp_thread_data.node_idx];

  // First call to read queue
  _call_status = Queue_Read (p_ipc_node->instances_data.tcp_send_queue,       // Queue
                             p_ipc_node->instances_data.tcp_send_queue_event, // Notification Event object
                             NULL,                                            // APC routine
                             NULL,                                            // APC data
                             IPC_TCP_SEND_QUEUE_ITEM_SIZE,                    // Output buffer size
                             &_tcp_send_request,                              // User Buffer
                             &_buffer_length,                                 // Actual Read buffer length
                             NULL);                                           // Read handle
  if ( _call_status != QUEUE_STATUS_OK )
  {
    // Critical error: Thread end
    // Logger message
    _stprintf (_aux_tchar_1, _T("%hu"), _call_status);
    PrivateLog (2, MODULE_NAME, _function_name, _aux_tchar_1, _T("Queue_Read"));

    return 0;
  }

  // Infinite loop to handle requests and events
  while ( TRUE ) 
  {
    // Wait with using the private event
    _wait_status = WaitForSingleObjectEx (p_ipc_node->instances_data.tcp_send_queue_event,  // Handle
                                          INFINITE,                                         // Milliseconds
                                          TRUE);                                           // Alertable
    switch ( _wait_status )
    {
      case WAIT_IO_COMPLETION:
        // Completion routine
      break;
      
      case WAIT_OBJECT_0:

        IPC_Tcp_ProcessSendRequest (&_tcp_send_request);
        
        // Call again to read snd queue
        _call_status = Queue_Read (p_ipc_node->instances_data.tcp_send_queue,       // Queue
                                   p_ipc_node->instances_data.tcp_send_queue_event, // Notification Event object
                                   NULL,                                            // APC routine
                                   NULL,                                            // APC data
                                   IPC_TCP_SEND_QUEUE_ITEM_SIZE,                    // Output buffer size
                                   &_tcp_send_request,                              // User Buffer
                                   &_buffer_length,                                 // Actual Read buffer length
                                   NULL);                                           // Read handle
        if ( _call_status != QUEUE_STATUS_OK )
        {
          // Critical error: Thread end
          // Logger message
          _stprintf (_aux_tchar_1, _T("%hu"), _call_status);
          PrivateLog (2, MODULE_NAME, _function_name, _aux_tchar_1, _T("Queue_Read"));

          return 0;
        }
      break;
      
      default:
        // Error
        // Logger message
        _stprintf (_aux_tchar_1, _T("%lu"), GetLastError ());
        PrivateLog (2, MODULE_NAME, _function_name, _aux_tchar_1, _T("WaitForSingleObjectsEx"));
   
        return 0;

      break;
      
    } // switch
      
  } // while

  return 0;

} // Ipc_Tcp_SendThread

//------------------------------------------------------------------------------
// PURPOSE : Sets a socket to blocking or non blocking state
//
//  PARAMS :
//      - INPUT :
//        - Socket
//        - Blocking: TRUE if socket must block
//                    FALSE if socket does not block
//      - OUTPUT :
//
// RETURNS :
//  - TRUE: OK
//  - FALSE: Error
//
//   NOTES :

BOOL IPC_Tcp_SetSocketBlocking (SOCKET Socket, 
                                BOOL Blocking)
{
  static TCHAR _function_name[] = _T("IPC_Tcp_SetSocketBlocking");
  TCHAR  _aux_tchar_1[IPC_AUX_STRING_LENGTH];

  int           _int_rc;
  unsigned long _arg;
  DWORD         _bytes_returned;

  // Set socket to non-blocking
  _arg    = Blocking ? 0 : 1;
  _int_rc = WSAIoctl (Socket,           // Socket
                      FIONBIO,          // Control code
                      &_arg,            // Input buffer
                      sizeof (_arg),
                      NULL,             // Output buffer
                      0,
                      &_bytes_returned, // Actual number of bytes in output buffer
                      NULL,             // Overlapped
                      NULL);            // Overlapped completion routine
  if ( _int_rc == SOCKET_ERROR )
  {
    // Logger message
    _stprintf (_aux_tchar_1, _T("%d"), WSAGetLastError ());
    PrivateLog (2, MODULE_NAME, _function_name, _aux_tchar_1, _T("WSAIoctl"));

    return FALSE;
  }

  return TRUE;

} // IPC_Tcp_SetSocketBlocking

//------------------------------------------------------------------------------
// PURPOSE : Makes tcp connnection to a node instance
//
//  PARAMS :
//      - INPUT :
//        - NodeIdx, NodeInstIdx: Node index and instances indexes
//      - OUTPUT :
//
// RETURNS :
//
//   NOTES :

void IPC_Tcp_ConnectRemoteNode (DWORD NodeIdx,
                                DWORD NodeInstIdx)
{
  static TCHAR _function_name[] = _T("IPC_Tcp_ConnectRemoteNode");
  TCHAR  _aux_tchar_1[IPC_AUX_STRING_LENGTH];

  BOOL                _connect_again;
  BOOL                _socket_connected;
  int                 _int_rc;
  GROUP               _socket_g;
  SOCKET              _socket;
  TYPE_INSTANCE_ENTRY * p_instance_entry;
  struct sockaddr_in  _addr;
  TIMEVAL             _timer;
  int                 _nfds = 0;
  fd_set              _write_fds;
  fd_set              _error_fds;

  p_instance_entry = &GLB_IpcNodeTable.ipc_node[NodeIdx].instances_data.instance_entry[NodeInstIdx];

  if ( p_instance_entry->socket_status == IPC_SOCKET_NOT_CONNECTED )
  {
    // Check if remote node is ready to connect again
    _connect_again = IPC_Tcp_CheckRemoteNodeAvail (NodeIdx, NodeInstIdx);
    if ( _connect_again )
    {
      // Close and recreate socket
      if ( p_instance_entry->node_socket != NULL )
      {
        _int_rc = closesocket (p_instance_entry->node_socket);
        if ( _int_rc != 0 )
        {
          // Logger message
          _stprintf (_aux_tchar_1, _T("%d"), WSAGetLastError ());
          PrivateLog (2, MODULE_NAME, _function_name, _aux_tchar_1,  _T("closesocket"));
        }
      }
      
      // Create socket
      memset (&_socket_g, 0, sizeof (_socket_g));
      _socket = WSASocket (AF_INET,               // Address family
                          SOCK_STREAM,           // Socket type
                          IPPROTO_TCP,           // Protocol
                          NULL,                  // Protocol info
                          _socket_g,             // Reserved
                          WSA_FLAG_OVERLAPPED);  // Flags

      if ( _socket == INVALID_SOCKET )
      {
        // Logger message
        _stprintf (_aux_tchar_1, _T("%d"), WSAGetLastError ());
        PrivateLog (2, MODULE_NAME, _function_name, _aux_tchar_1,  _T("WSASocket"));

        // Socket could not be created
        return;
      }

      p_instance_entry->node_socket = _socket;
    
      // Set socket to non blocking. Connection wait is done using a select statment with a timer
      // The timer avoid for long delays waiting for the connection
      IPC_Tcp_SetSocketBlocking (_socket, FALSE);

      // Build remote service address
      memcpy ((char FAR *) &(_addr.sin_addr), &p_instance_entry->ip_addr, sizeof (p_instance_entry->ip_addr));
      _addr.sin_family = AF_INET;
      _addr.sin_port   = htons (p_instance_entry->tcp_port);

      // Try connect to remote service. Note that we are using nonblocking sockets
      // to avoid a large wait
      _int_rc = WSAConnect (_socket,                    // Socket
                            (struct sockaddr *) &_addr, // Remote service address
                            sizeof (_addr),
                            NULL,                       // User connection data. Not used
                            NULL,                       // User connection data. Not used
                            NULL,                       // Quality of service specification. Not used.
                            NULL);                      // Reserved

      // Wait up to IPC_TCP_CONNECT_TIMEOUT seconds for connection succcess or fail to remote tcp service
      _socket_connected = TRUE;
      FD_ZERO (&_write_fds);
      FD_ZERO (&_error_fds);
      FD_SET  (_socket, &_write_fds);
      FD_SET  (_socket, &_error_fds);
      
      _timer.tv_sec  = IPC_TCP_CONNECT_TIMEOUT;
      _timer.tv_usec = 0;
      _int_rc = select (_nfds,        // Ignored
                        NULL,         // Read sockets to be checked
                        &_write_fds,  // Write sockets to be checked
                        &_error_fds,  // Socket error to be checked
                        &_timer);     // Timeout
                        
      if ( _int_rc == SOCKET_ERROR )
      {
        // Logger message
        _stprintf (_aux_tchar_1, _T("%d"), WSAGetLastError ());
        PrivateLog (2, MODULE_NAME, _function_name, _aux_tchar_1, _T("select"));
      }
      else
      {
        // Check timeout or socket write/error
        if ( _int_rc != 0 )
        {
          // No timeout, check for write/error
          if ( FD_ISSET (_socket, &_error_fds) )
          {
            // Connection error
            _socket_connected = FALSE;
          }
        }
      }
         
      if ( _socket_connected )
      {               
        // Socket is connected
        p_instance_entry->socket_status = IPC_SOCKET_CONNECTED;
        // Set socket to blocking
        IPC_Tcp_SetSocketBlocking (_socket, TRUE);
      }

      // Save last connection time for future connections
      p_instance_entry->last_status_time = GetTickCount ();
    }
  }
    
} // IPC_Tcp_ConnectRemoteNode

//------------------------------------------------------------------------------
// PURPOSE : Checks if socket node is connected or if it is not connected if
//          it could be connected (note there is a minumum time between 
//          connections)
//
//  PARAMS :
//      - INPUT :
//        - NodeIdx, NodeInstIdx: Node index and instances indexes
//      - OUTPUT :
//
// RETURNS :
//  - TRUE: If remote node is ready to be used in a send operations
//  - FALSE: Otherwise
//
//   NOTES :

BOOL IPC_Tcp_CheckRemoteNodeAvail (DWORD NodeIdx,
                                   DWORD NodeInstIdx)
{
  DWORD               _current_time;
  DWORD               _elapsed_time;
  BOOL                _connect_again;
  BOOL                _socket_connected;
  TYPE_INSTANCE_ENTRY * p_instance_entry;

  p_instance_entry = &GLB_IpcNodeTable.ipc_node[NodeIdx].instances_data.instance_entry[NodeInstIdx];

  _connect_again    = TRUE;
  _socket_connected = p_instance_entry->socket_status == IPC_SOCKET_CONNECTED;
  if ( !_socket_connected )
  {
    // Check socket status age. It is used to avoid a repeated connection 
    // to a node that is down or does not exist. A new connection is only executed after a given 
    // time (IPC_TCP_MIN_TIME_BTW_SND_CONN) after last connection try.
    if ( p_instance_entry->last_status_time > 0 )
    {
      _current_time  = GetTickCount ();
      if ( _current_time >= p_instance_entry->last_status_time )
      {
        _elapsed_time = _current_time - p_instance_entry->last_status_time;
        if ( _elapsed_time < IPC_TCP_MIN_TIME_BTW_SND_CONN )
        {
          _connect_again = FALSE;
        }
      }
    }
  }
  
  return    _socket_connected
         || _connect_again;
    
} // IPC_Tcp_CheckRemoteNodeAvail
