//------------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : IPC_Misc.cpp
// 
//   DESCRIPTION : Miscellaneous Functions and Methods of IPC module.
// 
//        AUTHOR : Xavier Ib��ez
// 
// CREATION DATE : 11-APR-2002
// 
// REVISION HISTORY
// 
// Date        Author Description
//----------- ------ -----------------------------------------------------------
// 11-APR-2002 XID    Initial draft.
// 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  INCLUDES 
//------------------------------------------------------------------------------
#define INCLUDE_QUEUE_API
#define INCLUDE_IPC_API
#define INCLUDE_PRIVATE_LOG
#define INCLUDE_COMMON_DATA
#include "CommonDef.h"

#include "IPC_APIInternals.h"

//------------------------------------------------------------------------------
//  PRIVATE CONSTANTS 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PRIVATE DATATYPES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PRIVATE DATA STRUCTURES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PRIVATE FUNCTION PROTOTYPES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PUBLIC FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE : Receive pending list management routines.
//          A Receive Pending structure is used in any receive operation to store
//          user parameters.
//
//  PARAMS :
//      - INPUT :
//        - pIpcNode
//      - OUTPUT :
//        - pReceivePending: pointer to a client buffer
//
// RETURNS :
//  - TRUE: OK
//  - FALSE: No free receive pending structure
//
//   NOTES :
//
BOOL IPC_Misc_GetFreeReceivePending (TYPE_IPC_NODE * pIpcNode, 
                                     TYPE_RECEIVE_PENDING ** pReceivePending)
{
  static TCHAR _function_name[] = _T("IPC_Misc_GetFreeReceivePending");
  TCHAR  _aux_tchar_1[IPC_AUX_STRING_LENGTH];

  DWORD   _receive_pending_idx;
  
  * pReceivePending = NULL;
  for ( _receive_pending_idx = 0; _receive_pending_idx < IPC_MAX_LOCAL_NODE_RECEIVE_PENDINGS; _receive_pending_idx++)
  {
    if ( !pIpcNode->local_node_data.receive_pending[_receive_pending_idx].in_use )
    {
      // Free buffer found
      * pReceivePending = &pIpcNode->local_node_data.receive_pending[_receive_pending_idx];

      // Create timer to be used in receive operations
      if ( (* pReceivePending)->timer == NULL )
      {
        (* pReceivePending)->timer = CreateWaitableTimer (NULL,   // Timer attributes
                                                         TRUE,   // Manual reset
                                                         NULL);  // Timer name
        if ( (* pReceivePending)->timer  == NULL )
        {
          // Errro creating timer
          // Logger message
          _stprintf (_aux_tchar_1, _T("%lu"), GetLastError ());
          PrivateLog (2, MODULE_NAME, _function_name, _aux_tchar_1, _T("CreateWaitableTimer"));
          
          return FALSE;
        }
      }
      (* pReceivePending)->in_use = TRUE;

      return TRUE;
    }
  } // for
  
  // Logger message
  PrivateLog (6, MODULE_NAME, _function_name, _T("receive_pending"), _T("Overflow"));
  return FALSE;
  
} // IPC_Misc_GetFreeReceivePending

//------------------------------------------------------------------------------
// PURPOSE : Receive pending list management routines
//
//  PARAMS :
//      - INPUT :
//        - pReceivePending: pointer to a client buffer
//      - OUTPUT :
//
// RETURNS :
//
//   NOTES :
//
void IPC_Misc_ReleaseReceivePending (TYPE_RECEIVE_PENDING * pReceivePending)
{

  pReceivePending->in_use = FALSE;
  
} // IPC_Misc_ReleaseReceivePending

//------------------------------------------------------------------------------
// PURPOSE : Send token management routines
//          Send token is used to synchronize client with send operations.
//
//  PARAMS :
//      - INPUT :
//        - pSendTokenList
//      - OUTPUT :
//        - pSendToken: pointer to a send token
//
// RETURNS :
//  - TRUE: OK
//  - FALSE: No free send tokens
//
//   NOTES :
//
BOOL IPC_Misc_GetFreeSendToken (TYPE_SEND_TOKEN *  pSendTokenList,
                                TYPE_SEND_TOKEN ** pSendToken)
{
  static TCHAR _function_name[] = _T("IPC_Misc_GetFreeSendToken");

  DWORD _send_token_idx;
  
  * pSendToken = NULL;
  for ( _send_token_idx = 0; _send_token_idx < IPC_MAX_SEND_TOKEN; _send_token_idx++)
  {
    if ( !pSendTokenList[_send_token_idx].in_use )
    {
      // Free buffer found
      * pSendToken = &pSendTokenList[_send_token_idx];
      
      // Initialize token if necessary
      if ( !(* pSendToken)->init )
      {
        InitializeCriticalSection (&(* pSendToken)->cs_token_counter);
        (* pSendToken)->init = TRUE;
      }
          
      (* pSendToken)->in_use = TRUE;

      return TRUE;
    }
  } // for
  
  // Logger message
  PrivateLog (6, MODULE_NAME, _function_name, _T("send_token"), _T("Overflow"));
  return FALSE;
  
} // IPC_Misc_GetFreeSendToken

//------------------------------------------------------------------------------
// PURPOSE : Send token management routines
//
//  PARAMS :
//      - INPUT :
//        - pSendToken: pointer to a send token
//      - OUTPUT :
//
// RETURNS :
//
//   NOTES :
//
void IPC_Misc_ReleaseSendToken (TYPE_SEND_TOKEN * pSendToken)
{

  pSendToken->in_use = FALSE;
  
} // IPC_Misc_ReleaseSendToken

//-----------------------------------------------------------------------------
// PURPOSE : Finds node in IPC node table
//
//  PARAMS :
//      - INPUT :
//          - NodeName: Node to find
//          - LocalInstance: TRUE if node id instances must be only the local one
//
//      - OUTPUT :
//          - pNodeInstId
//
// RETURNS :
//      - TRUE: Found
//      - FALSE: Not found
//
//   NOTES :
//
BOOL IPC_Misc_FindNodeInstId (TCHAR * pNodeName, 
                              DWORD * pNodeInstId,
                              BOOL  LocalInstance)
{
  DWORD _node_idx;
  DWORD _idx_instance;

  for ( _node_idx = 0; _node_idx < GLB_IpcNodeTable.num_nodes; _node_idx++)
  {
    if ( _tcscmp (pNodeName, GLB_IpcNodeTable.ipc_node[_node_idx].node_name) == 0 )
    {
      // Node found
      if ( LocalInstance )
      {
        // NodeInstId = Node index + local instance
        for ( _idx_instance = 0; 
              _idx_instance < GLB_IpcNodeTable.ipc_node[_node_idx].instances_data.num_instances; 
              _idx_instance++ )
        {
          if ( GLB_IpcNodeTable.ipc_node[_node_idx].instances_data.instance_entry[_idx_instance].in_use )
          {
            if ( GLB_IpcNodeTable.ipc_node[_node_idx].instances_data.instance_entry[_idx_instance].local_instance )
            {
              * pNodeInstId = GLB_IpcNodeTable.ipc_node[_node_idx].cfg_node_id | (_idx_instance << IPC_SHIFT_NODE_INST);
              
              return TRUE;
            }
          }
        } // for
        
        // Local instance not found
        return FALSE;
      }

      // If any instance or local instance not found: NodeInstId = Node index + any instance
      * pNodeInstId = GLB_IpcNodeTable.ipc_node[_node_idx].cfg_node_id | IPC_MASK_NODE_INST;
     
      return TRUE;
    }
  } // for
  
  * pNodeInstId = 0;
  return FALSE;

} // IPC_Misc_FindNodeId

//-----------------------------------------------------------------------------
// PURPOSE : Finds node in IPC node table & return the list of instances
//
//  PARAMS :
//      - INPUT :
//          - NodeName: Node to find
//
//      - OUTPUT :
//          - pNumInstances:  the number of instances
//          - pNodeInstId:    the list of instances
//          - pLocalInstance: flag indicating whether or not is the local instance
//
// RETURNS :
//      - TRUE: Found
//      - FALSE: Not found
//
//   NOTES :
//
BOOL IPC_Misc_FindNodeInstIdList (TCHAR * pNodeName,
                                  DWORD * pNumInstances,
                                  DWORD * pCfgInstId,
                                  DWORD * pNodeInstId,
                                  BOOL  * pLocalInstance)
{
  DWORD               _node_idx;
  DWORD               _idx_instance;
  DWORD               _num_instances;
  TYPE_IPC_NODE       * p_ipc_node;     // Local pointer
  TYPE_INSTANCE_ENTRY * p_inst_entry;   // Local pointer

  // Default return value
  * pNumInstances = 0;

  for ( _node_idx = 0; _node_idx < GLB_IpcNodeTable.num_nodes; _node_idx++)
  {
    p_ipc_node = &GLB_IpcNodeTable.ipc_node[_node_idx];
    if ( _tcscmp (pNodeName, p_ipc_node->node_name) == 0 )
    {  
      _num_instances = 0;
      // NodeInstId = Node index + local instance
      for ( _idx_instance = 0; _idx_instance < p_ipc_node->instances_data.num_instances; _idx_instance++ )
      {
        p_inst_entry = &p_ipc_node->instances_data.instance_entry[_idx_instance];
        if ( p_inst_entry->in_use )
        {
          pCfgInstId[_num_instances]     = p_inst_entry->cfg_inst_id;
          pNodeInstId[_num_instances]    = p_ipc_node->cfg_node_id | (_idx_instance << IPC_SHIFT_NODE_INST);
          pLocalInstance[_num_instances] = p_inst_entry->local_instance;
          _num_instances++;
        }
      } // for

      * pNumInstances = _num_instances;

      return TRUE;
    }
  } // for
  
  * pNumInstances = 0;

  return FALSE;

} // IPC_Misc_FindNodeInstIdList

//-----------------------------------------------------------------------------
// PURPOSE : Get instance id data
//
//  PARAMS :
//      - INPUT :
//          - NodeInstId
//
//      - OUTPUT :
//          - pComputerId
//          - pNodeId
//
// RETURNS :
//      - TRUE on success
//
//   NOTES :
//
BOOL IPC_Misc_GetInstIdData (DWORD NodeInstId,
                             WORD  * pComputerId,
                             WORD  * pNodeId)
{
  *pComputerId  = (WORD) ((NodeInstId & IPC_MASK_NODE_INST) >> IPC_SHIFT_NODE_INST);
  *pNodeId      = (WORD) (NodeInstId & IPC_MASK_NODE_IDX);

  return TRUE;
} // IPC_Misc_GetInstIdData

//-----------------------------------------------------------------------------
// PURPOSE : Checks IPC node: 
//            - Node index 
//            - Node initialized
//
//  PARAMS :
//     - INPUT :
//      - NodeInstId, NodeIdx
//      - NodeType: IPC_NODE_LOCAL, IPC_NODE_REMOTE
//      - pRequestOutput1: Status error.
//
//     - OUTPUT : None
//
//
// RETURNS :
//  - TRUE: OK
//  - FALSE: Node error
//
// NOTES :
//
BOOL IPC_Misc_CheckNode (DWORD NodeInstId, 
                         DWORD NodeIdx,
                         WORD  NodeType, 
                         WORD  * pRequestOutput1)
{
  TYPE_IPC_NODE * p_ipc_node;

  if ( NodeInstId == IPC_NODE_UNKNOWN )
  {
    if ( NodeType == IPC_NODE_LOCAL_SOURCE )
    {
      // Node unknown used as source: OK
      return TRUE;
    }
    else
    {
      // Node unknown must be source: Error
      return FALSE;
    }
  }
   
  // Note that local node unknown can be used as source node in send operations
  if ( NodeIdx >= GLB_IpcNodeTable.num_nodes )
  {
    * pRequestOutput1 = IPC_STATUS_INVALID_NODE_INST_ID;
    return FALSE;
  }
  p_ipc_node = &GLB_IpcNodeTable.ipc_node[NodeIdx];

  switch ( NodeType)
  {
    case IPC_NODE_LOCAL_SOURCE:
    case IPC_NODE_LOCAL:
      if ( !p_ipc_node->local_node_data.node_init )
      {
        // Node not initialized
        * pRequestOutput1 = IPC_STATUS_NODE_NOT_INIT;
        return FALSE;
      }
      if ( !p_ipc_node->local_node_data.local_node )
      {
        // Node not local
        * pRequestOutput1 = IPC_STATUS_NODE_NOT_LOCAL;
        return FALSE;
      }
    break;
    
    case IPC_NODE_REMOTE:
    break;
     
  } // switch
   
  return TRUE;

} // IPC_Misc_CheckNode                
                    
//------------------------------------------------------------------------------
// PURPOSE : From absolute NodeInstId gets internal node idx and instances.
//          Note that absolute Node Id is composed of: 0xIINNNNNN, where
//            - II is the node instance index (0 base). 0xFF for any instance.
//            - NNNNNN is the node index in node table (o base).
//
//  PARAMS :
//      - INPUT :
//        - NodeInstId
//      - OUTPUT : 
//        - pNodeIndex
//        - pFirstNodeInstIdx, pLastNodeInstIdx: Instances indexes, optional (can be NULL)
//          Note that pLastNodeInstIdx is the last index + 1 (it makes easier loops
//          on this indexes)
//
// RETURNS :
//  - TRUE: Node found
//  - FALSE: Node not found
//
//   NOTES :
//
BOOL IPC_Misc_GetNodeIdxAndInst (DWORD NodeInstId, 
                                 DWORD * pNodeIndex, 
                                 DWORD * pFirstNodeInstIdx, 
                                 DWORD * pLastNodeInstIdx)
{
  DWORD _node_instance;
  DWORD _cfg_node_id;
  BOOL  _cfg_node_id_found;
  
  * pNodeIndex = 0;
  _cfg_node_id = NodeInstId & IPC_MASK_NODE_IDX;

  _cfg_node_id_found = FALSE;

  // NIR + AJQ, 25-OCT-2002
  if ( _cfg_node_id >= IPC_MAX_IDX_NODES )
  {
    // Invalid node instance id
    return FALSE;
  } // if

  if ( GLB_IpcNodeTable.ipc_node_index[_cfg_node_id].in_use )
  {
    * pNodeIndex = GLB_IpcNodeTable.ipc_node_index[_cfg_node_id].node_idx;
    if (   pFirstNodeInstIdx != NULL 
        && pLastNodeInstIdx  != NULL )
    {
      // Set node instances  
      _node_instance = (NodeInstId & IPC_MASK_NODE_INST) >> IPC_SHIFT_NODE_INST;
      if ( _node_instance == IPC_ANY_NODE_INST )
      {
        // Any instance
        * pFirstNodeInstIdx = 0;
        * pLastNodeInstIdx  = GLB_IpcNodeTable.ipc_node[* pNodeIndex].instances_data.num_instances;
      }
      else
      {
        // Only one instance
        * pFirstNodeInstIdx = _node_instance;
        * pLastNodeInstIdx  = _node_instance + 1;
      }
    }
    
    return TRUE;
  }
  else
  {
    return FALSE;
  }

  return TRUE;
  
} // IPC_Misc_GetNodeIdxAndInst

//------------------------------------------------------------------------------
// PURPOSE : From absolute NodeInstId gets internal node idx. If node_inst_id does not
//          exist it is added and a new index in ipc_node table is allocated
//
//  PARAMS :
//      - INPUT :
//        - NodeInstId
//      - OUTPUT :
//        - pNodeIndex
//        - pNewNode: TRUE if new node
//
// RETURNS :
//  - TRUE: OK
//  - FALSE: Node table overflow
//
//   NOTES :
//
BOOL IPC_Misc_GetNodeIdx (DWORD NodeInstId, 
                          DWORD * pNodeIndex, 
                          BOOL  * pNewNode)
{
  static TCHAR _function_name[] = _T("IPC_Misc_GetNodeIdx");

  BOOL  _bool_rc;
  DWORD _cfg_node_id;

  * pNewNode   = FALSE;
  _cfg_node_id = NodeInstId & IPC_MASK_NODE_IDX;

  // Look for node id in the ipc_node_index table
  _bool_rc = IPC_Misc_GetNodeIdxAndInst (NodeInstId, pNodeIndex);
  if ( !_bool_rc )
  {
    // Node not found: must be added
    * pNewNode = TRUE;
    
    // Check table overflow
    if ( GLB_IpcNodeTable.num_nodes >= IPC_MAX_NODES )
    {
      // Overflow
      // Logger message
      PrivateLog (6, MODULE_NAME, _function_name, _T("num_nodes"), _T("Overflow"));
      return FALSE;
    }
    
    // Check node id value, between 0 and IPC_MAX_IDX_NODES
    if ( _cfg_node_id >= IPC_MAX_IDX_NODES )
    {
      // Node if out of range
      // Logger message
      PrivateLog (6, MODULE_NAME, _function_name, _T("node_id"), _T("OutOfRange"));
      return FALSE;
    }
    
    GLB_IpcNodeTable.ipc_node_index[_cfg_node_id].in_use      = TRUE;    
    GLB_IpcNodeTable.ipc_node_index[_cfg_node_id].node_idx    = GLB_IpcNodeTable.num_nodes;
    // Node data
    GLB_IpcNodeTable.ipc_node[GLB_IpcNodeTable.num_nodes].cfg_node_id = _cfg_node_id;

    * pNodeIndex = GLB_IpcNodeTable.num_nodes;
    GLB_IpcNodeTable.num_nodes++;
  }

  return TRUE;

} // IPC_Misc_GetNodeIdx

//------------------------------------------------------------------------------
//  PRIVATE FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------
