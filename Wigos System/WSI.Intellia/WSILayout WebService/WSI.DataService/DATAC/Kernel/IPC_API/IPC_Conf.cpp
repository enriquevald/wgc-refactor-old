//------------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : IPC_Conf.cpp
// 
//   DESCRIPTION : Configuration Functions and Methods of IPC module.
// 
//        AUTHOR : Xavier Ib��ez
// 
// CREATION DATE : 11-APR-2002
// 
// REVISION HISTORY
// 
// Date        Author Description
//----------- ------ -----------------------------------------------------------
// 11-APR-2002 XID    Initial draft.
// 11-APR-2002 CIR    Module implementation.
// 25-OCT-2002 SCM    Included support for read from database
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  INCLUDES 
//------------------------------------------------------------------------------
#define INCLUDE_QUEUE_API
#define INCLUDE_IPC_API
#define INCLUDE_PRIVATE_LOG
#define INCLUDE_COMMON_DATA

#include "CommonDef.h"

#include "IPC_APIInternals.h"

//------------------------------------------------------------------------------
//  PRIVATE CONSTANTS 
//------------------------------------------------------------------------------

#define IPC_CONF_FILE_NAME          _T("IPC.CFG")
#define IPC_CONF_FILE_NAME_DB       _T("IPCDB.CFG")

#define IPC_CONF_LINE_KEY_COMMENT   ';'
#define IPC_CONF_LINE_KEY_NODE      _T("NODE")
#define IPC_CONF_LINE_KEY_ID        _T("ID=")
#define IPC_CONF_LINE_KEY_NAME      _T("NAME=")
#define IPC_CONF_LINE_KEY_INST      _T("INST=")
#define IPC_CONF_LINE_COMMENT_FILE  _T("; ")
#define IPC_CONF_LINE_SEPARATOR     _T("---------------------------------------------------------")

#define IPC_CONF_LINE_TYPE_COMMENT  0
#define IPC_CONF_LINE_TYPE_NODE     1
#define IPC_CONF_LINE_TYPE_ID       2
#define IPC_CONF_LINE_TYPE_NAME     3
#define IPC_CONF_LINE_TYPE_INST     4
#define IPC_CONF_LINE_TYPE_EOF      5
#define IPC_CONF_LINE_TYPE_UNKNOWN  6

#define IPC_CONF_STATE_INIT         0
#define IPC_CONF_STATE_NODE         1
#define IPC_CONF_STATE_ID           2
#define IPC_CONF_STATE_NAME         3
#define IPC_CONF_STATE_INST         4
#define IPC_CONF_STATE_ERROR        5
#define IPC_CONF_STATE_OK           6
#define IPC_CONF_STATE_END          7

#define IPC_CONF_INSTANCE_SEPARATOR _T(",")
#define IPC_CONF_IP_ADDR_SEPARATOR  '.'
#define IPC_CONF_NUM_IP_BYTES       4   
#define IPC_CONF_NUM_IP_SEPARATORS  3

#define IPC_CONF_NODE_NAME_LENGTH   IPC_NODE_NAME_LENGTH - 1
#define IPC_CONF_MAX_NODE_INSTANCES IPC_MAX_INST_NODE
#define IPC_CONF_MAX_NODES          IPC_MAX_NODES
#define IPC_CONF_IP_STR_MAX_LENGTH  15
#define IPC_CONF_IP_STR_MIN_LENGTH  7

#define IPC_CONF_LINE_FEED_CHAR     0x0A

//------------------------------------------------------------------------------
//  PRIVATE DATATYPES 
//------------------------------------------------------------------------------
typedef enum
{
  SOURCE_UNKNOWN   = 0,
  SOURCE_FILE      = 1,
  SOURCE_DATABASE  = 2

} ENUM_CONFIG_SOURCE;

static TYPE_IPC_BUFFER GLB_IpcBuffer;

//------------------------------------------------------------------------------
//  PRIVATE FUNCTION PROTOTYPES 
//------------------------------------------------------------------------------
BOOL  IPC_Conf_LoadConfig          (TYPE_IPC_NODE_TABLE * pIpcNodeTable);

void  IPC_Conf_ProcessInitState    (TYPE_STATE_PROCESS * pStateProcess); 

void  IPC_Conf_ProcessNodeState    (TYPE_STATE_PROCESS * pStateProcess); 

void  IPC_Conf_ProcessIdState      (TYPE_STATE_PROCESS * pStateProcess); 

void  IPC_Conf_ProcessNameState    (TYPE_STATE_PROCESS * pStateProcess);

void  IPC_Conf_ProcessInstState    (TYPE_STATE_PROCESS * pStateProcess);

void  IPC_Conf_ProcessOKState      (TYPE_STATE_PROCESS * pStateProcess);

void  IPC_Conf_ProcessErrorState   (TYPE_STATE_PROCESS * pStateProcess);

BOOL  IPC_Conf_ParseLine         (TYPE_STATE_PROCESS * pStateProcess);

WORD  IPC_Conf_ParseLineType     (TCHAR * pBufferLine);

BOOL  IPC_Conf_ParseNode         (TYPE_STATE_PROCESS * pStateProcess);

BOOL  IPC_Conf_ParseNodeId       (TYPE_STATE_PROCESS * pStateProcess);

BOOL  IPC_Conf_ParseNodeName     (TYPE_STATE_PROCESS * pStateProcess);

BOOL  IPC_Conf_ParseNodeInstance (TYPE_STATE_PROCESS * pStateProcess);

BOOL  IPC_Conf_ExtractInstanceData      (TYPE_STATE_PROCESS * pStateProcess);

BOOL  IPC_Conf_GetNodeIdx               (TYPE_STATE_PROCESS * pStateProcess);

BOOL  IPC_Conf_CheckDuplicatedName      (TYPE_STATE_PROCESS * pStateProcess, TCHAR * pNewNodeName);

BOOL  IPC_Conf_CheckDuplicatedIP        (TYPE_STATE_PROCESS * pStateProcess, DWORD IpAddress);

BOOL  IPC_Conf_StoreNodeInTable         (TYPE_STATE_PROCESS * pStateProcess);

BOOL  IPC_Conf_CheckDuplicatedInstance  (TYPE_STATE_PROCESS * pStateProcess);

BOOL  IPC_Conf_CheckDuplicatedCfgInstId (TYPE_STATE_PROCESS * pStateProcess, WORD CfgInstId);

void  IPC_Conf_NodeToBuffer (TYPE_IPC_BUFFER * pBuffer, TYPE_IPC_NODE_DB * pNode);

BOOL  WriteIpcBufferToFile  (TYPE_IPC_BUFFER * pBuffer, TCHAR * pFileName);

void  WriteIpcLine          (TYPE_IPC_BUFFER * pBuffer, TCHAR * pLine);

//------------------------------------------------------------------------------
//  PUBLIC FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE : Fills IPC configuration
//
//  PARAMS :
//      - INPUT :
//        - pIpcNodeTable: Node table
//      - OUTPUT :
//
// RETURNS :
//  - TRUE: OK
//  - FALSE: Error
//
//   NOTES :

BOOL        IPC_Conf_ReadConfig (TYPE_IPC_NODE_TABLE * pIpcNodeTable)
{
  BOOL      _bool_rc;

  // Read config file and load IPC configuration structure
  _bool_rc = IPC_Conf_LoadConfig (pIpcNodeTable);
  if ( ! _bool_rc )
  {
    PrivateLog (2, MODULE_NAME, __FUNCTION__, _T("FALSE"), _T("IPC_Conf_LoadConfig"));
  }

  return _bool_rc;
} // IPC_Conf_ReadConfig

//-----------------------------------------------------------------------------
// PURPOSE : Function exportable to write the IPC config file
//
//  PARAMS :
//     - INPUT :
//         pFileName
//     - OUTPUT :
//         ----
//
//
// NOTES : A file with the name 'pFileName' is created and filled with the database IPC contents

IPC_API BOOL WINAPI     IPC_WriteConfigFile (TCHAR * pFileName)
{
  ////////////////////TYPE_IPC_DUAL_NODE_LIST   _dual_list;

  ////////////////////memset (&GLB_IpcBuffer, 0, sizeof (TYPE_IPC_BUFFER));

  ////////////////////if ( IPC_LoadConfigFromDataBase (&GLB_IpcBuffer, &_dual_list) )
  ////////////////////{
  ////////////////////  if ( WriteIpcBufferToFile (&GLB_IpcBuffer, pFileName) == TRUE )
  ////////////////////  {
  ////////////////////    return TRUE;
  ////////////////////  }
  ////////////////////}
 
  return FALSE;
} // IPC_WriteConfigFile

//------------------------------------------------------------------------------
//  PRIVATE FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PURPOSE : Load IPC configuration
//
//  PARAMS :
//     - INPUT :
//         pIpcFile, pointer to IPC configuration file
//
//     - OUTPUT :
//         pConfNodeTable, pointer to data read from config file
//
// RETURNS :
//     - TRUE on success
//     - FALSE on error
//
// NOTES :

BOOL  IPC_Conf_LoadConfig (TYPE_IPC_NODE_TABLE * pIpcNodeTable)
{
  TCHAR _aux_tchar_1 [IPC_AUX_STRING_LENGTH];
  TYPE_STATE_PROCESS _state_process;

  // Init state process structure
  memset (&_state_process, 0, sizeof (TYPE_STATE_PROCESS));

  // Initialize state process structure
  _state_process.p_ipc_node_table = pIpcNodeTable;

  // Set init state
  _state_process.current_state = IPC_CONF_STATE_INIT;

  while ( _state_process.current_state != IPC_CONF_STATE_END ) 
  {
    // States machine
    switch ( _state_process.current_state )
    {
      case IPC_CONF_STATE_INIT:
        // Process Initial state
        IPC_Conf_ProcessInitState (&_state_process);

      break;

      case IPC_CONF_STATE_NODE:
        // Process new node state
        IPC_Conf_ProcessNodeState (&_state_process);

      break;

      case IPC_CONF_STATE_ID:
        // Process node Id state
        IPC_Conf_ProcessIdState (&_state_process);

        break;

      case IPC_CONF_STATE_NAME :
        // Process node name state
        IPC_Conf_ProcessNameState (&_state_process);

      break;

      case IPC_CONF_STATE_INST:
        // Process node instance state
        IPC_Conf_ProcessInstState (&_state_process);

      break;

      case IPC_CONF_STATE_OK:
        // Process end OK state
        IPC_Conf_ProcessOKState (&_state_process);

      break;

      case IPC_CONF_STATE_ERROR:
        // Process end error state
        IPC_Conf_ProcessErrorState (&_state_process);

      break;

      case IPC_CONF_STATE_END:
        // End of process 
      break;

      default:
        // Unknown state
        _stprintf (_aux_tchar_1, _T("%hu"), _state_process.current_state);
        PrivateLog (6, MODULE_NAME, __FUNCTION__, _T("state"), _aux_tchar_1);

        return FALSE;

      break;
    } // switch

  } // while

  return _state_process.configuration_result;

} // IPC_Conf_LoadConfig

//-----------------------------------------------------------------------------
// PURPOSE : Init state process function
//
//  PARAMS :
//     - INPUT :
//         pStateProcess, pointer to state process environment data
//
//     - OUTPUT :
//
// RETURNS :
//
// NOTES :

void  IPC_Conf_ProcessInitState (TYPE_STATE_PROCESS * pStateProcess)
{
  TCHAR     _aux_str [IPC_AUX_STRING_LENGTH];
  BOOL      _bool_rc;

  if ( pStateProcess->source == SOURCE_UNKNOWN )
  {
    // File not open
    pStateProcess->source = SOURCE_FILE;
    pStateProcess->p_ipc_file = _tfopen (IPC_CONF_FILE_NAME, _T("r"));

    if ( pStateProcess->p_ipc_file == NULL )
    {
      // File error
      // Logger message
      _stprintf (_aux_str, _T("%d"), errno);
      PrivateLog (2, MODULE_NAME, __FUNCTION__, _aux_str, _T("ReadFile"));

      // Set next state to error state
      pStateProcess->current_state = IPC_CONF_STATE_ERROR;

      return;
    }

    //////////////////if ( pStateProcess->p_ipc_file == NULL )
    //////////////////{
    //////////////////  pStateProcess->source = SOURCE_DATABASE;

    //////////////////  // Init Ipc Buffer
    //////////////////  memset (&GLB_IpcBuffer, 0, sizeof (TYPE_IPC_BUFFER));
    //////////////////  pStateProcess->p_ipc_buffer = &GLB_IpcBuffer;

    //////////////////  if ( ! IPC_LoadConfigFromDataBase (pStateProcess->p_ipc_buffer, NULL) )
    //////////////////  {
    //////////////////    // File error
    //////////////////    // Logger message
    //////////////////    _stprintf (_aux_str, _T("%d"), errno);
    //////////////////    PrivateLog (2, MODULE_NAME, __FUNCTION__, _aux_str, _T("ReadFileDataBase"));

    //////////////////    // Set next state to error state
    //////////////////    pStateProcess->current_state = IPC_CONF_STATE_ERROR;

    //////////////////    return;
    //////////////////  }

    //////////////////  // Write 
    //////////////////  _bool_rc = WriteIpcBufferToFile (pStateProcess->p_ipc_buffer, IPC_CONF_FILE_NAME_DB);
    //////////////////  if ( _bool_rc == FALSE )
    //////////////////  {
    //////////////////    //Error writing the file

    //////////////////    return;
    //////////////////  }
    //////////////////}
  }

  // Open file successfully 
  // Get a file line
  _bool_rc = IPC_Conf_ParseLine (pStateProcess);
  if ( ! _bool_rc )
  {
    // Error occurred. Message error has been inserted in error log file in
    // specific function
    return;
  }

  // Check line read
  switch ( pStateProcess->current_line_type )
  {
    // Check for end of file
    case IPC_CONF_LINE_TYPE_EOF:
      // Empty configuration file
      // Set OK state
      pStateProcess->current_state = IPC_CONF_STATE_OK;
    break;

    // Check for a comment line 
    case IPC_CONF_LINE_TYPE_COMMENT:
      // Skip comment lines without change state
    break;

    // Check if line read from  config file corresponds to next state
    case IPC_CONF_LINE_TYPE_NODE:
      // Set node state
      pStateProcess->current_state = IPC_CONF_STATE_NODE;
    break;

    default:
      // Unexpected line type
      _stprintf (_aux_str, _T("%hu"), pStateProcess->current_line_type);
      PrivateLog (6, MODULE_NAME, __FUNCTION__, _T("current_line_type"), _aux_str);
      _stprintf (_aux_str, _T("%hu"), pStateProcess->current_line_number);
      PrivateLog (7, MODULE_NAME, __FUNCTION__, _aux_str);
      // Set current state as error state
      pStateProcess->current_state = IPC_CONF_STATE_ERROR;
    break;

  } // switch

  return;

} // IPC_Conf_ProcessInitState

//-----------------------------------------------------------------------------
// PURPOSE : Node state process function
//
//  PARAMS :
//     - INPUT :
//         pStateProcess, pointer to state process environment data
//
//     - OUTPUT :
//
// RETURNS :
//
// NOTES :

void  IPC_Conf_ProcessNodeState (TYPE_STATE_PROCESS * pStateProcess)
{
  TCHAR _aux_str [IPC_AUX_STRING_LENGTH];

  // Get a file line
  if ( ! IPC_Conf_ParseLine (pStateProcess) )
  {
    // Error occurred. Message error has been inserted in error log file in
    // specific function
    return;
  }

  // Check line read
  switch ( pStateProcess->current_line_type )
  {
    // Check for end of file
    case IPC_CONF_LINE_TYPE_EOF:
      // Unexpected end of file
      PrivateLog (6, MODULE_NAME, __FUNCTION__, _T("current_line_type"), _T("eof"));
      _stprintf (_aux_str, _T("%hu"), pStateProcess->current_line_number);
      PrivateLog (7, MODULE_NAME, __FUNCTION__, _aux_str);
      // Set error state
      pStateProcess->current_state = IPC_CONF_STATE_ERROR;
    break;

    // Check for a comment line 
    case IPC_CONF_LINE_TYPE_COMMENT:
      // Skip comment lines without change state
    break;

    // Check if line read from  config file corresponds to next state
    case IPC_CONF_LINE_TYPE_ID:
      // Set node Id state
      pStateProcess->current_state = IPC_CONF_STATE_ID;
    break;

    default:
      // Unexpected line type
      _stprintf (_aux_str, _T("%hu"), pStateProcess->current_line_type);
      PrivateLog (6, MODULE_NAME, __FUNCTION__, _T("current_line_type"), _aux_str);
      _stprintf (_aux_str, _T("%hu"), pStateProcess->current_line_number);
      PrivateLog (7, MODULE_NAME, __FUNCTION__, _aux_str);
      // Set current state as error state
      pStateProcess->current_state = IPC_CONF_STATE_ERROR;
    break;

  } // switch

  return;

} // IPC_Conf_ProcessNodeState

//-----------------------------------------------------------------------------
// PURPOSE : Node Id state process function
//
//  PARAMS :
//     - INPUT :
//         pStateProcess, pointer to state process environment data
//
//     - OUTPUT :
//
// RETURNS :
//
// NOTES :

void  IPC_Conf_ProcessIdState (TYPE_STATE_PROCESS * pStateProcess)
{
  TCHAR _aux_str [IPC_AUX_STRING_LENGTH];

  // Get a new file line
  if ( ! IPC_Conf_ParseLine (pStateProcess) )
  {
    // Error occurred. Message error has been inserted in error log file in
    // specific function
    return;
  }

  // Check line read
  switch ( pStateProcess->current_line_type )
  {
    // Check for end of file
    case IPC_CONF_LINE_TYPE_EOF:
      // Unexpected end of file
      PrivateLog (6, MODULE_NAME, __FUNCTION__, _T("current_line_type"), _T("eof"));
      _stprintf (_aux_str, _T("%hu"), pStateProcess->current_line_number);
      PrivateLog (7, MODULE_NAME, __FUNCTION__, _aux_str);
      // Set error state
      pStateProcess->current_state = IPC_CONF_STATE_ERROR;
    break;

    // Check for a comment line 
    case IPC_CONF_LINE_TYPE_COMMENT:
      // Skip comment lines without change state
    break;

    // Check if line read from  config file corresponds to next state
    case IPC_CONF_LINE_TYPE_NAME :
      // Valid node Id. Change to next state
      pStateProcess->current_state = IPC_CONF_STATE_NAME;
    break;

    default:
      // Unexpected line type
      _stprintf (_aux_str, _T("%hu"), pStateProcess->current_line_type);
      PrivateLog (6, MODULE_NAME, __FUNCTION__, _T("current_line_type"), _aux_str);
      _stprintf (_aux_str, _T("%hu"), pStateProcess->current_line_number);
      PrivateLog (7, MODULE_NAME, __FUNCTION__, _aux_str);
      // Set current state as error state
      pStateProcess->current_state = IPC_CONF_STATE_ERROR;
    break;

  } // switch

  return;

} // IPC_Conf_ProcessIdState

//-----------------------------------------------------------------------------
// PURPOSE : Node name state process function
//
//  PARAMS :
//     - INPUT :
//         pStateProcess, pointer to state process environment data
//
//     - OUTPUT :
//
// RETURNS :
//
// NOTES :

void  IPC_Conf_ProcessNameState (TYPE_STATE_PROCESS * pStateProcess)
{
  TCHAR _aux_str [IPC_AUX_STRING_LENGTH];

  // Get a new file line
  if ( ! IPC_Conf_ParseLine (pStateProcess) )
  {
    // Error occurred. Message error has been inserted in error log file in
    // specific function
    return;
  }

  // Check line read
  switch ( pStateProcess->current_line_type )
  {
    // Check for end of file
    case IPC_CONF_LINE_TYPE_EOF:
      // Unexpected end of file
      PrivateLog (6, MODULE_NAME, __FUNCTION__, _T("current_line_type"), _T("eof"));
      _stprintf (_aux_str, _T("%hu"), pStateProcess->current_line_number);
      PrivateLog (7, MODULE_NAME, __FUNCTION__, _aux_str);
      // Set error state
      pStateProcess->current_state = IPC_CONF_STATE_ERROR;
    break;

    // Check for a comment line 
    case IPC_CONF_LINE_TYPE_COMMENT:
      // Skip comment lines without change state
    break;

    // Check if line read from  config file corresponds to next state
    case IPC_CONF_LINE_TYPE_INST:
      // Change to next state
      pStateProcess->current_state = IPC_CONF_STATE_INST;
    break;

    default:
      // Unexpected line type
      _stprintf (_aux_str, _T("%hu"), pStateProcess->current_line_type);
      PrivateLog (6, MODULE_NAME, __FUNCTION__, _T("current_line_type"), _aux_str);
      _stprintf (_aux_str, _T("%hu"), pStateProcess->current_line_number);
      PrivateLog (7, MODULE_NAME, __FUNCTION__, _aux_str);
      // Set current state as error state
      pStateProcess->current_state = IPC_CONF_STATE_ERROR;
    break;
  } // switch

  return;

} // IPC_Conf_ProcessNameState

//-----------------------------------------------------------------------------
// PURPOSE : Node instance state process function
//
//  PARAMS :
//     - INPUT :
//         pStateProcess, pointer to state process environment data
//
//     - OUTPUT :
//
// RETURNS :
//
// NOTES :

void  IPC_Conf_ProcessInstState (TYPE_STATE_PROCESS * pStateProcess)
{
  TCHAR     _aux_str [IPC_AUX_STRING_LENGTH];
  BOOL      _bool_rc;

  // Get a new file line
  if ( ! IPC_Conf_ParseLine (pStateProcess) )
  {
    // Error occurred. Message error has been inserted in error log file in
    // specific function
    return;
  }

  // Check line read
  switch ( pStateProcess->current_line_type )
  {
    // Check for a comment line 
    case IPC_CONF_LINE_TYPE_COMMENT:
      // Skip comment lines without change state
    break;

    // Check if line read from config file corresponds to node instance state
    case IPC_CONF_LINE_TYPE_INST:
      // Do not change instance state in order to allow a new instance configuration
    break;

    // Check for end of file or node line
    case IPC_CONF_LINE_TYPE_EOF:
    case IPC_CONF_LINE_TYPE_NODE:
      // New node line, check if at least one instance has been 
      // configured for the current node
      if ( pStateProcess->private_ipc_node.instances_data.num_instances > 0 )
      {
        // Save node data into IPC node table
        _bool_rc = IPC_Conf_StoreNodeInTable (pStateProcess);
        if ( ! _bool_rc )
        {
          // Error occurred, error message has been inserted in 
          // error log in specific function
          // Set error state
          pStateProcess->current_state = IPC_CONF_STATE_ERROR;
        }
        else
        {
          // Node stored OK
          if ( pStateProcess->current_line_type == IPC_CONF_LINE_TYPE_NODE )
          {
            // New node, reset previous node data
            memset (&pStateProcess->private_ipc_node, 0, sizeof (TYPE_IPC_NODE));
            // Change to next state. 
            pStateProcess->current_state = IPC_CONF_STATE_NODE;
          } //
          else
          {
            // Valid end of file, check for duplicated instances in all nodes
            _bool_rc = IPC_Conf_CheckDuplicatedInstance (pStateProcess);
            if ( _bool_rc )
            {
              // Duplicated instance
              PrivateLog (2, MODULE_NAME, __FUNCTION__, _T("TRUE"), _T("IPC_Conf_CheckDuplicatedInstance"));
              _stprintf (_aux_str, _T("%hu"), pStateProcess->current_line_number);
              PrivateLog (7, MODULE_NAME, __FUNCTION__, _aux_str);

              // Set error state
              pStateProcess->current_state = IPC_CONF_STATE_ERROR;
            }
            else
            {
              // Ending process OK
              // Set OK state
              pStateProcess->current_state = IPC_CONF_STATE_OK;
            } // else
          } // else
        } //else
      }
      else
      {
        // No instance entries.
        _stprintf (_aux_str, _T("%lu"), pStateProcess->private_ipc_node.instances_data.num_instances);
        PrivateLog (6, MODULE_NAME, __FUNCTION__, _T("num_instances"), _aux_str);
        _stprintf (_aux_str, _T("%hu"), pStateProcess->current_line_number);
        PrivateLog (7, MODULE_NAME, __FUNCTION__, _aux_str);
        // Set current state as error state
        pStateProcess->current_state = IPC_CONF_STATE_ERROR;
      } // else
    break;

    default:
      // Unexpected line type
      _stprintf (_aux_str, _T("%hu"), pStateProcess->current_line_type);
      PrivateLog (6, MODULE_NAME, __FUNCTION__, _T("current_line_type"), _aux_str);
      _stprintf (_aux_str, _T("%hu"), pStateProcess->current_line_number);
      PrivateLog (7, MODULE_NAME, __FUNCTION__, _aux_str);
      // Set current state as error state
      pStateProcess->current_state = IPC_CONF_STATE_ERROR;
    break;

  } // switch

  return;

} // IPC_Conf_ProcessInstState

//-----------------------------------------------------------------------------
// PURPOSE : Process OK state
//
//  PARAMS :
//     - INPUT :
//         pStateProcess, pointer to state process environment data
//
//     - OUTPUT :
//
// RETURNS :
//
// NOTES :

void  IPC_Conf_ProcessOKState  (TYPE_STATE_PROCESS * pStateProcess)
{
  // Closes the configuration file
  if ( pStateProcess->p_ipc_file != NULL )
  {
    fclose (pStateProcess->p_ipc_file);
  }

  // Set configuration OK
  pStateProcess->configuration_result = TRUE;

  // Set end state 
  pStateProcess->current_state = IPC_CONF_STATE_END;

  return;

} // IPC_Conf_ProcessOKState

//-----------------------------------------------------------------------------
// PURPOSE : Process Error state
//
//  PARAMS :
//     - INPUT :
//         pStateProcess, pointer to state process environment data
//
//     - OUTPUT :
//
// RETURNS :
//
// NOTES :

void  IPC_Conf_ProcessErrorState  (TYPE_STATE_PROCESS * pStateProcess)
{
  // Closes the configuration file
  if ( pStateProcess->p_ipc_file != NULL )
  {
    fclose (pStateProcess->p_ipc_file);
  }

  // Set configuration Error
  pStateProcess->configuration_result = FALSE;

  // Set end state 
  pStateProcess->current_state = IPC_CONF_STATE_END;

  return;

} // IPC_Conf_ProcessErrorState

//-----------------------------------------------------------------------------
// PURPOSE : Read a line from config file and parse line data to process
//          state structure
//  PARAMS :
//     - INPUT :
//
//     - OUTPUT :
//         pStateProcess, pointer to state process environment data
//         
// RETURNS :
//
// NOTES :

BOOL  IPC_Conf_ParseLine (TYPE_STATE_PROCESS * pStateProcess)
{
  TCHAR _aux_str [IPC_AUX_STRING_LENGTH];
  BOOL  _bool_rc;
  TCHAR * p_tchar;
  WORD  _line_length;

  switch ( pStateProcess->source )
  {
    case SOURCE_FILE:
    {
      // Read line from file
      p_tchar = _fgetts (pStateProcess->current_line_buffer, IPC_CONF_LINE_BUFFER_SIZE, pStateProcess->p_ipc_file);

      if ( p_tchar == NULL )
      {
        // NULL pointer returned. _fgetts function returns a NULL pointer when end of file
        // is encountered and when read operation has failed. Check if this return code 
        // corresponds to an end of file or it is a read error
        if ( feof (pStateProcess->p_ipc_file) )
        {
          // End of file encountered
          pStateProcess->current_line_type = IPC_CONF_LINE_TYPE_EOF;

          return TRUE;
        }
        else
        {
          // Read error
          _stprintf (_aux_str, _T("%d"), ferror (pStateProcess->p_ipc_file));
          PrivateLog (2, MODULE_NAME, __FUNCTION__, _aux_str, _T("_fgetts"));

          // Set current state as error state
          pStateProcess->current_state = IPC_CONF_STATE_ERROR;

          return FALSE;
        } // else
      }
    } // case file
    break;

    case SOURCE_DATABASE:
    {
      if ( pStateProcess->current_line_number < pStateProcess->p_ipc_buffer->num_lines + 1)
      {
         // Read line from buffer  
        _tcscpy (pStateProcess->current_line_buffer, 
                pStateProcess->p_ipc_buffer->buffer[pStateProcess->current_line_number].line);

        p_tchar = pStateProcess->current_line_buffer;
      }
      else
      {
        // End of buffer encountered
        pStateProcess->current_line_type = IPC_CONF_LINE_TYPE_EOF;

        return TRUE;
      } //if
    } // case db
    break;

    default:
      return FALSE;
  }// switch

  // Update line counter
  pStateProcess->current_line_number++;

  // Check if last character in line buffer read is a line feed
  _line_length = _tcslen (pStateProcess->current_line_buffer);
  if (   _line_length
      && pStateProcess->current_line_buffer[_line_length - 1] == IPC_CONF_LINE_FEED_CHAR )
  {
    // Line feed. Remove it
    pStateProcess->current_line_buffer[_line_length - 1] = '\0';
  }

  // Check for an empty line content. 
  _line_length = _tcslen (pStateProcess->current_line_buffer);
  if ( _line_length == 0 )
  {
    // Empty lines are handled as comment lines
    pStateProcess->current_line_type = IPC_CONF_LINE_TYPE_COMMENT;

    return TRUE;
  }

  // Find out Line type 
  pStateProcess->current_line_type = IPC_Conf_ParseLineType (pStateProcess->current_line_buffer);

  switch ( pStateProcess->current_line_type )
  {
    case IPC_CONF_LINE_TYPE_COMMENT:
      // Skip comment lines
      _bool_rc = TRUE;
    break;

    case IPC_CONF_LINE_TYPE_NODE:
      // This line type does not have data to store, only check 
      // node header format
      _bool_rc = IPC_Conf_ParseNode (pStateProcess);
    break;

    case IPC_CONF_LINE_TYPE_ID:
      // Parse node Id 
      _bool_rc = IPC_Conf_ParseNodeId (pStateProcess);
    break;

    case IPC_CONF_LINE_TYPE_NAME :
      // Parse node name
      _bool_rc = IPC_Conf_ParseNodeName (pStateProcess);
    break;

    case IPC_CONF_LINE_TYPE_INST:
      // Parse node instance
      _bool_rc = IPC_Conf_ParseNodeInstance (pStateProcess);
    break;

    default:
      // Unknown line type
      _stprintf (_aux_str, _T("%hu"), pStateProcess->current_line_type);
      PrivateLog (6, MODULE_NAME, __FUNCTION__, _T("current_line_type"), _aux_str);
      _stprintf (_aux_str, _T("%hu"), pStateProcess->current_line_number);
      PrivateLog (7, MODULE_NAME, __FUNCTION__, _aux_str);
      _bool_rc = FALSE;
    break;

  } // switch

  if ( ! _bool_rc )
  {
    // Set current state as error state
    pStateProcess->current_state = IPC_CONF_STATE_ERROR;
  }

  return _bool_rc;

} // IPC_Conf_ParseLine

//-----------------------------------------------------------------------------
// PURPOSE : Parse line type read
//          
//  PARAMS :
//     - INPUT :
//          - pBufferLine, line read
//
//     - OUTPUT :
//
// RETURNS : 
//    IPC_CONF_LINE_TYPE_COMMENT  
//    IPC_CONF_LINE_TYPE_NODE     
//    IPC_CONF_LINE_TYPE_ID       
//    IPC_CONF_LINE_TYPE_NAME     
//    IPC_CONF_LINE_TYPE_INST     
//    IPC_CONF_LINE_TYPE_UNKNOWN  
//
// NOTES :

WORD  IPC_Conf_ParseLineType (TCHAR * pBufferLine)
{
  if ( pBufferLine[0] == IPC_CONF_LINE_KEY_COMMENT )
  {
    // Comment line
    return IPC_CONF_LINE_TYPE_COMMENT;
  }
  else if ( _tcsnicmp (pBufferLine, IPC_CONF_LINE_KEY_NODE, _tcslen (IPC_CONF_LINE_KEY_NODE)) == 0 )
  {
    // Node line
    return IPC_CONF_LINE_TYPE_NODE;
  }
  else if ( _tcsnicmp (pBufferLine, IPC_CONF_LINE_KEY_ID, _tcslen (IPC_CONF_LINE_KEY_ID)) == 0 )
  {
    // Node id line
    return IPC_CONF_LINE_TYPE_ID;
  }
  else if ( _tcsnicmp (pBufferLine, IPC_CONF_LINE_KEY_NAME, _tcslen (IPC_CONF_LINE_KEY_NAME)) == 0 )
  {
    // Node name line
    return IPC_CONF_LINE_TYPE_NAME;
  }
  else if ( _tcsnicmp (pBufferLine, IPC_CONF_LINE_KEY_INST, _tcslen (IPC_CONF_LINE_KEY_INST)) == 0 )
  {
    // Node instance line
    return IPC_CONF_LINE_TYPE_INST;
  }

  // Unknown line type
  return IPC_CONF_LINE_TYPE_UNKNOWN;

} // IPC_Conf_ParseLineType

//-----------------------------------------------------------------------------
// PURPOSE : Check node 
//          
//  PARAMS :
//     - INPUT :
//         pStateProcess, pointer to state process environment data
//
//     - OUTPUT :
//         
// RETURNS :
//
// NOTES :

BOOL  IPC_Conf_ParseNode (TYPE_STATE_PROCESS * pStateProcess)
{
  TCHAR _aux_str [IPC_AUX_STRING_LENGTH];
  WORD  _line_length;
  WORD  _key_length;

  // Check for a correct node line format (this line must include the word NODE only)
  _line_length = _tcslen (pStateProcess->current_line_buffer);
  _key_length  = _tcslen (IPC_CONF_LINE_KEY_NODE);
  if ( _line_length != _key_length )
  {
    // Bad format line 
    _stprintf (_aux_str, _T("%s"), pStateProcess->current_line_buffer);
    PrivateLog (6, MODULE_NAME, __FUNCTION__, _T("node"), _aux_str);
    _stprintf (_aux_str, _T("%hu"), pStateProcess->current_line_number);
    PrivateLog (7, MODULE_NAME, __FUNCTION__, _aux_str);

    // Set current state as error state
    pStateProcess->current_state = IPC_CONF_STATE_ERROR;

    return FALSE;
  }

  // This line does not include line content
  return TRUE;

} // IPC_Conf_ParseNode

//-----------------------------------------------------------------------------
// PURPOSE : store node Id in process state structure
//          
//  PARAMS :
//     - INPUT :
//
//     - OUTPUT :
//         pStateProcess, pointer to state process environment data
//         
// RETURNS :
//
// NOTES :

BOOL  IPC_Conf_ParseNodeId (TYPE_STATE_PROCESS * pStateProcess)
{
  TCHAR _aux_str [IPC_AUX_STRING_LENGTH];
  WORD  _aux_number;
  WORD  _key_length;
  DWORD _node_idx;
  BOOL  _new_node;
  BOOL  _bool_rc;
  WORD  _node_length;
  TCHAR * p_user_node_id;

  // Get ID line key length
  _key_length = _tcslen (IPC_CONF_LINE_KEY_ID);
  // Set pointer to node node id data skipping line key
  p_user_node_id = &pStateProcess->current_line_buffer[_key_length];

  // Check if node Id is really a number
  _aux_number = _tstoi ( p_user_node_id );
  // _tstoi () function returns a zero value as a function error return code. As this value (zero)
  // is allowed as node Id, a checking must be performed in order to know if the zero value returned
  // is the node Id zero or it is an error return code
  if ( _aux_number == 0 )
  {
    if ( p_user_node_id[0] != '0' )
    {
      // Invalid node Id
      _stprintf (_aux_str, _T("%s"), p_user_node_id);
      PrivateLog (6, MODULE_NAME, __FUNCTION__, _T("node_id"), _aux_str);
      _stprintf (_aux_str, _T("%hu"), pStateProcess->current_line_number);
      PrivateLog (7, MODULE_NAME, __FUNCTION__, _aux_str);

      return FALSE;
    }
    else
    {
      // Node Id line starts with a zero value, check for more
      // characters in line
      _node_length = _tcslen(p_user_node_id);
      if ( _node_length > 1 )
      {
        // Invalid node Id format
        _stprintf (_aux_str, _T("%s"), p_user_node_id);
        PrivateLog (6, MODULE_NAME, __FUNCTION__, _T("node_id"), _aux_str);
        _stprintf (_aux_str, _T("%hu"), pStateProcess->current_line_number);
        PrivateLog (7, MODULE_NAME, __FUNCTION__, _aux_str);

        return FALSE;
      }
    } // else
  }

  // Get node Idx
  _bool_rc = IPC_Misc_GetNodeIdx ((DWORD) _aux_number, &_node_idx, &_new_node);

  // Check for a valid IDX value
  if ( ! _bool_rc )
  {
    // Node idx could not be assigned. Error message has been inserted in error log 
    // in specific function
    return FALSE;
  } //if 

  // Check if this node already exists
  if ( _new_node == FALSE )
  {
    // Existing node
    PrivateLog (2, MODULE_NAME, __FUNCTION__, _T("new_node=FALSE"), _T("IPC_Misc_GetNodeIdx"));
    _stprintf (_aux_str, _T("%hu"), pStateProcess->current_line_number);
    PrivateLog (7, MODULE_NAME, __FUNCTION__, _aux_str);

    return FALSE;
  }

  // Valid node Id store id
  pStateProcess->private_ipc_node.cfg_node_id = _aux_number;

  return TRUE;

} // IPC_Conf_ParseNodeId

//-----------------------------------------------------------------------------
// PURPOSE : store node name in process state structure
//          
//  PARAMS :
//     - INPUT :
//
//     - OUTPUT :
//         pStateProcess, pointer to state process environment data
//         
// RETURNS :
//
// NOTES :

BOOL  IPC_Conf_ParseNodeName (TYPE_STATE_PROCESS * pStateProcess)
{
  TCHAR _aux_str [IPC_AUX_STRING_LENGTH];
  TCHAR * p_user_node_name;
  WORD  _key_length;
  WORD  _node_length;
  BOOL  _bool_rc;

  // Get name line key length
  _key_length = _tcslen (IPC_CONF_LINE_KEY_NAME);

  // Set pointer to node name skipping line key
  p_user_node_name = &pStateProcess->current_line_buffer[_key_length];
  
  // Check node name size
  _node_length = _tcslen (p_user_node_name);
  if ( _node_length > IPC_CONF_NODE_NAME_LENGTH ) 
  {                                                      
    // Node name length too long
    _stprintf (_aux_str, _T("%s"), p_user_node_name);
    PrivateLog (6, MODULE_NAME, __FUNCTION__, _T("node_name"), _aux_str);
    _stprintf (_aux_str, _T("%hu"), pStateProcess->current_line_number);
    PrivateLog (7, MODULE_NAME, __FUNCTION__, _aux_str);

    return FALSE;
  }

  // Check for repeat node name if more than one node has been configured
  _bool_rc = IPC_Conf_CheckDuplicatedName (pStateProcess, p_user_node_name);
  if ( _bool_rc )
  {
    // Duplicated name
    PrivateLog (2, MODULE_NAME, __FUNCTION__, _T("TRUE"), _T("IPC_Conf_CheckDuplicatedName"));
    _stprintf (_aux_str, _T("%hu"), pStateProcess->current_line_number);
    PrivateLog (7, MODULE_NAME, __FUNCTION__, _aux_str);

    return FALSE;
  }

  // Valid node name store it 
  _tcscpy (pStateProcess->private_ipc_node.node_name, p_user_node_name);

  return TRUE;

} // IPC_Conf_ParseNodeName

//-----------------------------------------------------------------------------
// PURPOSE : store node instance in process state structure
//          
//  PARAMS :
//     - INPUT :
//
//     - OUTPUT :
//         pStateProcess, pointer to state process environment data
//         
// RETURNS :
//
// NOTES :

BOOL  IPC_Conf_ParseNodeInstance (TYPE_STATE_PROCESS * pStateProcess)
{
  //TCHAR _aux_str [IPC_AUX_STRING_LENGTH];
  BOOL  _bool_rc;

  //// Check for max number of node instances reached
  //if ( pStateProcess->private_ipc_node.instances_data.num_instances >= IPC_CONF_MAX_NODE_INSTANCES )
  //{
  //  // Cannot assign more instances
  //  PrivateLog (6, MODULE_NAME, __FUNCTION__, _T("num_instances"), _T("Overflow"));
  //  _stprintf (_aux_str, _T("%hu"), pStateProcess->current_line_number);
  //  PrivateLog (7, MODULE_NAME, __FUNCTION__, _aux_str);
  //
  //  return FALSE;
  //}

  // Instance line type is formated as INST= III, AAA.AAA.AAA.AAA, PPPPP
  // Where AAA.AAA.AAA.AAA is the instance IP address
  //       PPPPP is instance TCP port
  //
  // Extract these two values
  _bool_rc = IPC_Conf_ExtractInstanceData (pStateProcess);
  if ( ! _bool_rc )
  {
    // Specific error message has been inserted in error log in 
    // IPC_Conf_ParseInstanceData function
    return FALSE;
  }

  return TRUE;

} // IPC_Conf_ParseNodeInstance

//-----------------------------------------------------------------------------
// PURPOSE : Extract node instance data into port number and IP address
//
//  PARAMS :
//     - INPUT :
//
//     - OUTPUT :
//         pStateProcess, pointer to state process environment data
//
// RETURNS :
//     - TRUE on success
//     - FALSE on error
//
// NOTES :

BOOL  IPC_Conf_ExtractInstanceData (TYPE_STATE_PROCESS * pStateProcess) 
{
  TCHAR _aux_str [IPC_AUX_STRING_LENGTH];

  char    _ip_addr_str[IPC_AUX_STRING_LENGTH];
  DWORD   _idx_instance;
//  TCHAR   * p_user_port;
  TCHAR   * p_user_data;
//  WORD    _separator_offset;
  WORD    _key_length;
  WORD    _numeric_port;
  DWORD   _numeric_IP_address;
//  WORD    _ip_string_length;
//  WORD    _ip_port_length;
//  WORD    _idx;
//  WORD    _separator_counter;
  WORD    _ip_item_1;
  WORD    _ip_item_2; 
  WORD    _ip_item_3; 
  WORD    _ip_item_4; 
  WORD    _rc;
  BOOL    _bool_rc;
  WORD    _cfg_inst_id;

  // Get instance line key length in order to initialize user data pointers
  _key_length = _tcslen (IPC_CONF_LINE_KEY_INST);

  // Initialize user data
  // Skip line key
  p_user_data = &pStateProcess->current_line_buffer[_key_length];


  _rc = _stscanf (p_user_data, "%hu,%hu%.%hu.%hu.%hu,%hu", 
                               &_cfg_inst_id,
                               &_ip_item_1, 
                               &_ip_item_2, 
                               &_ip_item_3, 
                               &_ip_item_4,
                               &_numeric_port);

  // SCM 13-MAR-2003: Not used by now
  //_bool_rc = Common_CheckIpAddrPort (_ip_item_1, _ip_item_2, _ip_item_3, _ip_item_4,
  //                                  _numeric_port, TRUE);

  if ( _rc != IPC_NUM_INSTANCE_FIELDS )
  {
    // Bad IP address format
    _stprintf (_aux_str, _T("%s"), p_user_data);
    PrivateLog (6, MODULE_NAME, __FUNCTION__, _T("InstanceFormat"), _aux_str);
    _stprintf (_aux_str, _T("%hu"), pStateProcess->current_line_number);
    PrivateLog (7, MODULE_NAME, __FUNCTION__, _aux_str);

    return FALSE;
  }

  if ( _cfg_inst_id >= IPC_MAX_INSTANCES_PER_NODE )
  {
    // Invalid inst Id
    _stprintf (_aux_str, _T("%u"), _cfg_inst_id);
    PrivateLog (6, MODULE_NAME, __FUNCTION__, _T("CfgInstId"), _aux_str);
    _stprintf (_aux_str, _T("%hu"), pStateProcess->current_line_number);
    PrivateLog (7, MODULE_NAME, __FUNCTION__, _aux_str);

    return FALSE;
  }

  if ( IPC_Conf_CheckDuplicatedCfgInstId (pStateProcess, _cfg_inst_id) == TRUE )
  {
    // IP repeated
    PrivateLog (2, MODULE_NAME, __FUNCTION__, _T("TRUE"), _T("IPC_Conf_CheckDuplicatedCfgInstId"));
    _stprintf (_aux_str, _T("%hu"), pStateProcess->current_line_number);
    PrivateLog (7, MODULE_NAME, __FUNCTION__, _aux_str);

    return FALSE;
  }

  // The CfgInstId is used as IdxInstance
  _idx_instance = _cfg_inst_id;

  if ( _numeric_port == 0 )
  {
    // Invalid TCP port
    _stprintf (_aux_str, _T("%u"), _numeric_port);
    PrivateLog (6, MODULE_NAME, __FUNCTION__, _T("TcpPort"), _aux_str);
    _stprintf (_aux_str, _T("%hu"), pStateProcess->current_line_number);
    PrivateLog (7, MODULE_NAME, __FUNCTION__, _aux_str);

    return FALSE;
  }

  // Valid Ip address and TCP port
  sprintf (_ip_addr_str, _T("%hu.%hu.%hu.%hu"), _ip_item_1,
                                                _ip_item_2,
                                                _ip_item_3,
                                                _ip_item_4);
  // Get numeric IP addres value
  _numeric_IP_address = inet_addr (_ip_addr_str);

  // AJQ, 27-MAY-2003. Allow the Loopback IP Addr.
  // This is only needed on Laptops to avoid using a fixed IP.
  if ( inet_addr (LOOPBACK_IP_ADDR) != _numeric_IP_address )
  {
    if (   _ip_item_1 > 223
        || _ip_item_1 < 1
        || _ip_item_1 == 127
        || _ip_item_2 > 255 
        || _ip_item_2 < 1
        || _ip_item_3 > 255  
        || _ip_item_3 < 1
        || _ip_item_4 > 255
        || _ip_item_4 < 1   )
    {
      // Invalid IP byte value
      _stprintf (_aux_str, _T("%s"), p_user_data);
      PrivateLog (6, MODULE_NAME, __FUNCTION__, _T("ip_address"), _aux_str);
      _stprintf (_aux_str, _T("%hu"), pStateProcess->current_line_number);
      PrivateLog (7, MODULE_NAME, __FUNCTION__, _aux_str);

      return FALSE;
    }
  }

  // Check for duplicated IP address in the this node 
  _bool_rc = IPC_Conf_CheckDuplicatedIP (pStateProcess, _numeric_IP_address);
  if ( _bool_rc )
  {
    // IP repeated
    PrivateLog (2, MODULE_NAME, __FUNCTION__, _T("TRUE"), _T("IPC_Conf_CheckDuplicatedIP"));
    _stprintf (_aux_str, _T("%hu"), pStateProcess->current_line_number);
    PrivateLog (7, MODULE_NAME, __FUNCTION__, _aux_str);

    return FALSE;
  }

  // Store instance data in instance structure
  pStateProcess->private_ipc_node.instances_data.instance_entry[_idx_instance].in_use   = TRUE;
  pStateProcess->private_ipc_node.instances_data.instance_entry[_idx_instance].cfg_inst_id = _cfg_inst_id;
  pStateProcess->private_ipc_node.instances_data.instance_entry[_idx_instance].tcp_port = _numeric_port;
  pStateProcess->private_ipc_node.instances_data.instance_entry[_idx_instance].ip_addr  = _numeric_IP_address;

  if ( _idx_instance >= pStateProcess->private_ipc_node.instances_data.num_instances )
  {
    pStateProcess->private_ipc_node.instances_data.num_instances = _idx_instance + 1;
  }

  return TRUE;

} // IPC_Conf_ExtractInstanceData
//-----------------------------------------------------------------------------
// PURPOSE : 
//
//  PARAMS :
//     - INPUT :
//
//     - OUTPUT :
//
// RETURNS :
//
// NOTES :

BOOL  IPC_Conf_CheckDuplicatedCfgInstId (TYPE_STATE_PROCESS * pStateProcess, WORD CfgInstId)
{
  // PRECONDITION CfgInstId < IPC_MAX_INSTANCES_PER_NODE
  if ( pStateProcess->private_ipc_node.instances_data.instance_entry[CfgInstId].in_use )
  {
    return TRUE;
  }

  // Not duplicated
  return FALSE;

} // IPC_Conf_CheckDuplicatedCfgInstId

//-----------------------------------------------------------------------------
// PURPOSE : Check if new instance IP address is repeat in any of the previous
//          node instances
//
//  PARAMS :
//     - INPUT :
//         pStateProcess, pointer to state process environment data
//         IpAddress, new IP address 
//
//     - OUTPUT :
//
// RETURNS :
//     - TRUE If new IP address is duplicated
//     - FALSE IP address not duplicated
//
// NOTES :

BOOL  IPC_Conf_CheckDuplicatedIP (TYPE_STATE_PROCESS * pStateProcess, DWORD IpAddress)
{
  WORD  _idx;

  for ( _idx = 0; _idx < pStateProcess->private_ipc_node.instances_data.num_instances; _idx++)
  {
    if ( pStateProcess->private_ipc_node.instances_data.instance_entry[_idx].in_use )
    {
      if ( pStateProcess->private_ipc_node.instances_data.instance_entry[_idx].ip_addr == IpAddress )
      {
        // Duplicated IP
        return TRUE;
      }
    }
  }
  
  // IP not duplicated
  return FALSE;

} // IPC_Conf_CheckDuplicatedIP

//-----------------------------------------------------------------------------
// PURPOSE : Store node data read from configuration file to IPC structure
//          
//
//  PARAMS :
//     - INPUT :
//         pStateProcess, pointer to state process environment data
//
//     - OUTPUT :
//
// RETURNS :
//     - TRUE If new IP address is duplicated
//     - FALSE IP address not duplicated
//
// NOTES :

BOOL  IPC_Conf_StoreNodeInTable (TYPE_STATE_PROCESS * pStateProcess)
{
  DWORD         _idx_node;
  BOOL          _new_node;
  WORD          _idx_instance;
  BOOL          _bool_rc;
  TYPE_IPC_NODE       * p_ipc_node;
  TYPE_IPC_NODE       * p_private_ipc_node;
  TYPE_INSTANCE_ENTRY * p_inst_entry;
  TYPE_INSTANCE_ENTRY * p_private_inst_entry;


  // Local pointer
  p_private_ipc_node = &pStateProcess->private_ipc_node; 

  // Get node Idx
  _bool_rc = IPC_Misc_GetNodeIdx (p_private_ipc_node->cfg_node_id, &_idx_node, &_new_node);

  // Check for a valid IDX value
  if ( ! _bool_rc )
  {
    // Node idx could not be assigned. Error message has been inserted in error log 
    // in specific function
    return FALSE;
  } //if 

  p_ipc_node = &pStateProcess->p_ipc_node_table->ipc_node[_idx_node];

  // Store node data
  _tcscpy (p_ipc_node->node_name, p_private_ipc_node->node_name);
  p_ipc_node->instances_data.num_instances = p_private_ipc_node->instances_data.num_instances;
  for ( _idx_instance = 0; _idx_instance < p_private_ipc_node->instances_data.num_instances; _idx_instance++ )
  {
    p_private_inst_entry = &p_private_ipc_node->instances_data.instance_entry[_idx_instance];
    p_inst_entry         = &p_ipc_node->instances_data.instance_entry[_idx_instance];
    if ( p_private_inst_entry->in_use )
    {
      p_inst_entry->in_use      = p_private_inst_entry->in_use;
      p_inst_entry->cfg_inst_id = p_private_inst_entry->cfg_inst_id;
      p_inst_entry->ip_addr     = p_private_inst_entry->ip_addr;
      p_inst_entry->tcp_port    = p_private_inst_entry->tcp_port;
    }
    else
    {
      // Not necessary, but ...
      p_inst_entry->in_use = FALSE;
    }
  }

  return TRUE;

} // IPC_Conf_StoreNodeInTable

//-----------------------------------------------------------------------------
// PURPOSE : Check in all nodes of the table that any node name does not match
//          the new one 
//
//  PARAMS :
//     - INPUT :
//         pStateProcess, pointer to state process environment data
//         pNewNodeName, new node name to check
//
//     - OUTPUT :
//
// RETURNS :
//     - TRUE duplicated node name
//     - FALSE name not duplicated
//
// NOTES :

BOOL        IPC_Conf_CheckDuplicatedName (TYPE_STATE_PROCESS    * pStateProcess, 
                                          TCHAR                 * pNewNodeName)
{
  WORD      _idx_node;

  for ( _idx_node = 0; _idx_node < pStateProcess->p_ipc_node_table->num_nodes; _idx_node++ )
  {
    if ( ! _tcsicmp (pStateProcess->p_ipc_node_table->ipc_node[_idx_node].node_name, 
                     pNewNodeName) )
    {
      // Duplicated node name
      return TRUE;
    }
  }

  // Node name not duplicated
  return FALSE;

} // IPC_Conf_CheckDuplicatedName

//-----------------------------------------------------------------------------
// PURPOSE : Check in all nodes of the table that any instance (IP address + TCP port)
//          is not duplicated between them 
//
//  PARAMS :
//     - INPUT :
//         pStateProcess, pointer to state process environment data
//
//     - OUTPUT :
//
// RETURNS :
//     - TRUE  instance (IP address + TCP port) duplicated
//     - FALSE Instance not duplicated
//
// NOTES :

BOOL  IPC_Conf_CheckDuplicatedInstance (TYPE_STATE_PROCESS * pStateProcess)
{
  WORD                _idx_node_1;
  WORD                _idx_node_2;
  WORD                _idx_inst_1;
  WORD                _idx_inst_2;
  DWORD               _num_nodes;
  DWORD               _num_inst_1;
  DWORD               _num_inst_2;
  TYPE_IPC_NODE_TABLE * p_node_table;
  TYPE_IPC_NODE       * p_node_1;
  TYPE_IPC_NODE       * p_node_2;
  TYPE_INSTANCE_ENTRY * p_inst_1;
  TYPE_INSTANCE_ENTRY * p_inst_2;

  p_node_table = pStateProcess->p_ipc_node_table;

  // Get number of total nodes in table
  _num_nodes = p_node_table->num_nodes;

  for ( _idx_node_1 = 0; _idx_node_1 < _num_nodes - 1; _idx_node_1 ++ )
  {
    p_node_1 = &p_node_table->ipc_node[_idx_node_1];

    // Get number of instances of the specific node
    _num_inst_1 = p_node_1->instances_data.num_instances;

    for ( _idx_inst_1 = 0; _idx_inst_1 < _num_inst_1; _idx_inst_1++ )
    {
      p_inst_1 = &p_node_1->instances_data.instance_entry[_idx_inst_1];

      if ( p_inst_1->in_use )
      {
        // Check all  nodes
        for ( _idx_node_2 = _idx_node_1 + 1; _idx_node_2 < _num_nodes; _idx_node_2++ )
        {
          p_node_2 = &p_node_table->ipc_node[_idx_node_2];

          // Get number of instances of the specific search node
          _num_inst_2 = p_node_2->instances_data.num_instances;

          // Check all instances
          for ( _idx_inst_2 = 0; _idx_inst_2 < _num_inst_2; _idx_inst_2++ )
          {
            p_inst_2 = &p_node_2->instances_data.instance_entry[_idx_inst_2];
            if ( p_inst_2->in_use )
            {
              if ( p_inst_1->ip_addr == p_inst_2->ip_addr )
              {
                if ( p_inst_1->tcp_port == p_inst_2->tcp_port )
                {
                  return TRUE;
                }
              }
            } // if ( p_inst_2->in_use )
          }
        }

      } // if ( p_inst_1->in_use )

    }
  } // for

  // No duplicated instances
  return FALSE;

} // IPC_Conf_CheckDuplicatedInstance


//-----------------------------------------------------------------------------
// PURPOSE : Write formatted node data in a buffer
//
//  PARAMS :
//     - INPUT :
//         pBuffer, pointer to a buffer struct
//         pNode , pointer to a node
//     - OUTPUT :
//         pBuffer with a new node
//
// RETURNS :
//     -
// NOTES :

void  IPC_Conf_NodeToBuffer (TYPE_IPC_BUFFER    * pBuffer,
                             TYPE_IPC_NODE_DB   * pNode)
{
  TYPE_LINE   _tmp_line_buffer;
  TYPE_LINE   _aux_line_buffer;
  DWORD       _idx_instances;

  // Write line separator
  _stprintf (_tmp_line_buffer.line, _T("%s%s"), IPC_CONF_LINE_COMMENT_FILE, IPC_CONF_LINE_SEPARATOR);
  WriteIpcLine (pBuffer, _tmp_line_buffer.line);

  // Write ";Nodename"
  _stprintf (_tmp_line_buffer.line, _T("%s%s"), IPC_CONF_LINE_COMMENT_FILE, pNode->node_name);
  WriteIpcLine (pBuffer, _tmp_line_buffer.line);

    // Write line separator
  _stprintf (_tmp_line_buffer.line, _T("%s%s"), IPC_CONF_LINE_COMMENT_FILE, IPC_CONF_LINE_SEPARATOR);
  WriteIpcLine (pBuffer, _tmp_line_buffer.line);

  // Write "NODE"
  _tcscpy (_tmp_line_buffer.line, IPC_CONF_LINE_KEY_NODE);
  WriteIpcLine (pBuffer, _tmp_line_buffer.line);

  // Write "ID=#"
  _stprintf (_tmp_line_buffer.line, _T("%s%lu"), IPC_CONF_LINE_KEY_ID, pNode->node_cfg_id );
  WriteIpcLine (pBuffer, _tmp_line_buffer.line);

  // Write "NAME=#"
  _stprintf (_tmp_line_buffer.line, _T("%s%s"), IPC_CONF_LINE_KEY_NAME, pNode->node_name);
  WriteIpcLine (pBuffer, _tmp_line_buffer.line);

  // Write all instances
  for ( _idx_instances = 0; _idx_instances < pNode->num_instances; _idx_instances++ )
  {
    // Write "INST= #Inst, @IP, #Port   ; ComputerName"
    _stprintf (_aux_line_buffer.line, _T("%s%3hu%s %s%s %5lu"),
                                      IPC_CONF_LINE_KEY_INST,
                                      pNode->instance_entry[_idx_instances].computer_id,
                                      IPC_CONF_INSTANCE_SEPARATOR,
                                      pNode->instance_entry[_idx_instances].ip_addr, 
                                      IPC_CONF_INSTANCE_SEPARATOR,
                                      pNode->instance_entry[_idx_instances].tcp_port);

    _stprintf (_tmp_line_buffer.line, _T("%-40s%s%s"),
                                      _aux_line_buffer.line,
                                      IPC_CONF_LINE_COMMENT_FILE, 
                                      pNode->instance_entry[_idx_instances].computer_name);

    WriteIpcLine (pBuffer, _tmp_line_buffer.line);

  }// for
 
  pBuffer->num_lines = pBuffer->current_line;

} // IPC_Conf_NodeToBuffer

//-----------------------------------------------------------------------------
// PURPOSE : Write in a buffer a IPC line
//
//  PARAMS :
//     - INPUT :
//         pBuffer, pointer to a buffer struct
//         pLine, pointer to a IPC line
//     - OUTPUT :
//         ---
//
//
// NOTES : 

void WriteIpcLine (TYPE_IPC_BUFFER * pBuffer,
                   TCHAR           * pLine)
{
  static BOOL _error_logged = FALSE;
  TCHAR _aux_tchar_1 [IPC_AUX_STRING_LENGTH];

  // Check if there are enough space in the buffer to write
  if ( pBuffer->num_lines >= MAX_BUFFER_LINES )
  {
    if ( !_error_logged )
    {
      _stprintf (_aux_tchar_1, _T("%hu"), pBuffer->num_lines);
      PrivateLog (6, MODULE_NAME, __FUNCTION__, _T("IpcBufferNumLines"), _aux_tchar_1);
      _error_logged = TRUE;
    }

    return;
  }

  _stprintf (pBuffer->buffer[pBuffer->current_line].line, _T("%s"), pLine);
  pBuffer->current_line++;
  
} // WriteIpcLine

//-----------------------------------------------------------------------------
// PURPOSE : Write in a file the buffer struct
//
//  PARAMS :
//     - INPUT :
//         pBuffer, pointer to a buffer struct
//         pFileName , pointer to the name of the file to be written
//     - OUTPUT :
//         ---
//
//
// NOTES : A file with the name 'pFileName' is created and filled with the buffer struct.

BOOL WriteIpcBufferToFile (TYPE_IPC_BUFFER  * pBuffer,
                           TCHAR            * pFileName)
{
  TYPE_GET_DATE_TIME  _date_time;
  FILE                * p_file;
  DWORD               _idx_line;
  DWORD               _rc;

  p_file = _tfopen (pFileName, _T("w"));
  if ( p_file == NULL )
  { 
    // Logger error
    PrivateLog (2, MODULE_NAME, __FUNCTION__, _T("NULL"), _T("_tfopen"));

    return FALSE;
  }

  _date_time.control_block = sizeof (TYPE_GET_DATE_TIME);
  _rc = Common_GetDateTime (&_date_time, COMMON_TIME_FORMAT_HMS);
  if ( _rc == COMMON_ERROR_CONTROL_BLOCK )
  { 
    // Logger error
    PrivateLog (2, MODULE_NAME, __FUNCTION__, _T("COMMON_ERROR_CONTROL_BLOCK"), _T("Common_GetDateTime"));

    return FALSE;
  }
  
  // Header 
  _ftprintf (p_file, _T("%s%s\n"),        IPC_CONF_LINE_COMMENT_FILE, IPC_CONF_LINE_SEPARATOR);
  _ftprintf (p_file, _T("%s%-20s%s\n"),   IPC_CONF_LINE_COMMENT_FILE, _T("IPC Config file:"), pFileName);  
  _ftprintf (p_file, _T("%s%-20s%s %s\n"),IPC_CONF_LINE_COMMENT_FILE, _T("Created:"), _date_time.current_date_string, _date_time.current_time_string);
  _ftprintf (p_file, _T("%s%s\n"),        IPC_CONF_LINE_COMMENT_FILE, IPC_CONF_LINE_SEPARATOR);

  for ( _idx_line = 0; _idx_line < pBuffer->num_lines; _idx_line++ )
  {
    _ftprintf (p_file, _T("%s\n"), pBuffer->buffer[_idx_line].line);
  }

  // Footer 
  _ftprintf (p_file, _T("%s%s\n"),        IPC_CONF_LINE_COMMENT_FILE, IPC_CONF_LINE_SEPARATOR);
  _ftprintf (p_file, _T("%s%-20s%s\n"),   IPC_CONF_LINE_COMMENT_FILE, _T("IPC Config file:"), pFileName);  
  _ftprintf (p_file, _T("%s%-20s%s %s\n"),IPC_CONF_LINE_COMMENT_FILE, _T("Created:"), _date_time.current_date_string, _date_time.current_time_string);
  _ftprintf (p_file, _T("%s%s\n"),        IPC_CONF_LINE_COMMENT_FILE, IPC_CONF_LINE_SEPARATOR);

  fclose (p_file);

  return TRUE;
} // WriteIpcBufferFile

//-----------------------------------------------------------------------------
// PURPOSE : Writes in a dual list the node and watchdog information
//
//  PARAMS :
//     - INPUT :
//         pNode, pointer to a node struct
//         pWatchdog , pointer to a node struct 
//     - OUTPUT :
//         pDualList
//
//
// NOTES : 

void IPC_Conf_NodeToDualList (TYPE_IPC_DUAL_NODE_LIST * pDualList, 
                              TYPE_IPC_NODE_DB        * pNode,
                              TYPE_IPC_NODE_DB        * pWdog)
{
  TCHAR _aux_tchar_1 [IPC_AUX_STRING_LENGTH];
 
  if ( pDualList->num_nodes >= IPC_MAX_DUAL_NODES )
  {
    _stprintf (_aux_tchar_1, _T("%hu"), IPC_MAX_DUAL_NODES);
    PrivateLog (6, MODULE_NAME, __FUNCTION__, _T("IpcDualList.NumNodes"), _aux_tchar_1);

    return;
  } 

  memcpy (&pDualList->dual_node[pDualList->num_nodes].node, pNode, sizeof (TYPE_IPC_NODE_DB));
  memcpy (&pDualList->dual_node[pDualList->num_nodes].wdog, pWdog, sizeof (TYPE_IPC_NODE_DB));
  pDualList->num_nodes++;

} // IPC_Conf_NodeToDualList
