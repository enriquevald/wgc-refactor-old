﻿//------------------------------------------------------------------------------
// Copyright © 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: cls_permisionario.vb (WIGOSASGUI)
// 
//   DESCRIPTION: Provides a list to save TipoEspecatulos data (which 
//                include String Espectaculo and Decimal Aprovechamiento) and
//                provides functions to pass to or read from a xml string.
// 
//        AUTHOR: Pau Guillamon
// 
// CREATION DATE: 17-OCT-2012
// 
// REVISION HISTORY:
// 
// Date        Author           Description
// ----------- ------           ------------------------------------------------
// 17-OCT-2012 PGJ              First release.
// 29-OCT-2012 QMP              Added temporary ID column.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.IO; 

namespace WSI.Common
{
  public class EspectaculosList
  {
    #region Enums

    public enum EL_COL
    {
      ESPECTACULO = 0,
      APROVECHAMIENTO = 1
    }

    #endregion // Enums


    #region Members

    private DataTable m_espectaculo_dt = null;
    private DataSet m_ds = null;

    #endregion // Members


    #region Properties

    public DataTable DataTable
    {
      get
      {
        return this.m_espectaculo_dt;
      }
      set
      {
        this.m_ds.Tables.Remove("Espectaculo");
        this.m_espectaculo_dt = value.Copy();
        this.m_ds.Tables.Add(this.m_espectaculo_dt);
      }
    } // DataTable

    #endregion // Properties

    # region Constructors

    //------------------------------------------------------------------------------
    // PURPOSE : Constructor for a EspectaculosList instance
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public EspectaculosList()
    {
      Init();
    } // EspectaculosList

    #endregion // Constructors


    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Add a new Espectaculo.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - String TipoEspectaculo
    //          - Decimal Aprovechamiento
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    public void AddEspectaculo(String TipoEspectaculo, Decimal Aprovechamiento)
    {
      DataRow _dr;

      _dr = m_espectaculo_dt.NewRow();
      _dr[0] = TipoEspectaculo;
      _dr[1] = Aprovechamiento;
      m_espectaculo_dt.Rows.Add(_dr);
    } // AddEspectaculo

    //------------------------------------------------------------------------------
    // PURPOSE: Remove an Espectaculo.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - String TipoEspectaculo
    //          - Decimal Aprovechamiento
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    public void RemoveEspectaculo(String TipoEspectaculo, Decimal Aprovechamiento)
    {
      DataRow _dr;

      _dr = SeekEspectaculo(TipoEspectaculo, Aprovechamiento);
      if (_dr != null)
      {
        _dr.Delete();
        m_espectaculo_dt.AcceptChanges();
      }
    } // RemoveEspectaculo

    //------------------------------------------------------------------------------
    // PURPOSE: Indicates if an Espectaculo is in the EspectaculosList.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - String TipoEspectaculo
    //          - Decimal Aprovechamiento
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - True: The Espectaculo is in the list. False: Otherwise.
    // 
    public Boolean Contains(String TipoEspectaculo, Decimal Aprovechamiento)
    {
      return (SeekEspectaculo (TipoEspectaculo, Aprovechamiento) != null);
    } // Contains

    //------------------------------------------------------------------------------
    // PURPOSE: Returns the item in the position Idx in the list
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Int Idx
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //          - DataRow containing the item or null if index is not valid
    // 
    public DataRow getItem(int Idx)
    {
      if (Idx + 1 > m_espectaculo_dt.Rows.Count || Idx < 0)
      {
        return null;
      }

      return m_espectaculo_dt.Rows[Idx];
    } // getItem

    //------------------------------------------------------------------------------
    // PURPOSE: Returns the item in the position Idx in the list
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Int Idx
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //          - DataRow containing the item or null if index is not valid
    // 
    public DataRow this[Int32 Idx]
    {
      get {
        return getItem(Idx);
      }
    } // this[Idx]

    //------------------------------------------------------------------------------
    // PURPOSE: Returns the number of rows in the dataTable
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //          - int number of rows
    // 
    public int getNumRows()
    {
      return m_espectaculo_dt.Rows.Count;
    } // getNumRows

    //------------------------------------------------------------------------------
    // PURPOSE: Get a XML representation of the Espectaculos list.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - String
    // 
    public String ToXml()
    {
      m_ds.Tables["Espectaculo"].Columns.Remove("Id");

      return this.DataSetToXml (m_ds, XmlWriteMode.IgnoreSchema);
    } // ToXml

    //------------------------------------------------------------------------------
    // PURPOSE: Read a XML representation of the Espectaculos list and store it into the DataSet.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    public void FromXml(String Xml)
    {
      if (Xml == null || Xml.Equals(""))
      {
        return;
      }

      m_espectaculo_dt.Clear();

      // Reset the ID AutoIncrement counter
      m_espectaculo_dt.Columns["Id"].AutoIncrementStep = -1;
      m_espectaculo_dt.Columns["Id"].AutoIncrementSeed = -1;
      m_espectaculo_dt.Columns["Id"].AutoIncrementStep = 1;
      m_espectaculo_dt.Columns["Id"].AutoIncrementSeed = 0;

      DataSetFromXml(m_ds, Xml, XmlReadMode.IgnoreSchema);

    } // FromXml

    //------------------------------------------------------------------------------
    // PURPOSE: Returns a String as a list with the rows between [ ]
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - String with the rows between [ ]
    // 
    public String[] AuditString(String EspectaculoPrefix)
    {
      String[] _list = new String[this.getNumRows()];

      int _idx_rows = 0;

      foreach (DataRow _dr in m_espectaculo_dt.Rows)
      {
        _list[_idx_rows] = EspectaculoPrefix + "[" + (_idx_rows + 1).ToString() + "] = " 
                          + (String)_dr[0] + ", " + ( (Decimal)_dr[1] * 100 ) + "%";

        _idx_rows++;
      }

      return _list;
    } // AudtirString

    //------------------------------------------------------------------------------
    // PURPOSE: Returns Duplicate of the current EspectaculoList
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - EspectaculoList as a duplicate of the current one
    // 
    public EspectaculosList Duplicate()
    {
      EspectaculosList _temp; 

      _temp = new EspectaculosList();

      if (!m_espectaculo_dt.Columns.Contains("Id"))
      {
        //QMP - Add temporary ID column
        _temp.DataTable.Columns.Add("Id");
        _temp.DataTable.Columns["Id"].AutoIncrement = true;
        _temp.DataTable.Columns["Id"].AutoIncrementSeed = 0;
        _temp.DataTable.Columns["Id"].AutoIncrementStep = 1;
      }

      _temp.DataTable = m_espectaculo_dt.Copy();

      return _temp;
    } // Duplicate

    #endregion


    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Init the properties of the class.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    private void Init()
    {
      m_ds = new DataSet("EspectaculosList");

      m_espectaculo_dt = new DataTable("Espectaculo");
      m_espectaculo_dt.Columns.Add("TipoEspectaculo", Type.GetType("System.String"));
      m_espectaculo_dt.Columns.Add("Aprovechamiento", Type.GetType("System.Decimal"));

      //QMP - Add temporary ID column
      m_espectaculo_dt.Columns.Add("Id");
      m_espectaculo_dt.Columns["Id"].AutoIncrement = true;
      m_espectaculo_dt.Columns["Id"].AutoIncrementSeed = 0;
      m_espectaculo_dt.Columns["Id"].AutoIncrementStep = 1;

      m_ds.Tables.Add(m_espectaculo_dt);
    } // Init

    //------------------------------------------------------------------------------
    // PURPOSE: Search for the Espectaculo in the Espectaculos list.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - String Key
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - DataRow: If found return the DataRow.
    // 
    private DataRow SeekEspectaculo(String TipoEspectaculo, Decimal Aprovechamiento)
    {
      foreach (DataRow _dr in m_espectaculo_dt.Rows)
      {
        if (TipoEspectaculo.Equals((String)_dr[0]) && Aprovechamiento.Equals ((Decimal)_dr[1]))
        {
          return _dr;
        }
      } // end foreach

      return null;
    } // SeekEspectaculo


    //------------------------------------------------------------------------------
    // PURPOSE : DataSet to Xml
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataSet DataSet
    //          - XmlWriteMode WriteMode
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String
    //
    private String DataSetToXml(DataSet DataSet, XmlWriteMode WriteMode)
    {
      String _xml;

      _xml = "";
      using (StringWriter _sw = new StringWriter())
      {
        DataSet.WriteXml(_sw, WriteMode);
        _xml = _sw.ToString();
      } // end using

      return _xml;
    } // DataSetToXml

    //------------------------------------------------------------------------------
    // PURPOSE : DataSet to Xml
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataSet DataSet
    //          - XmlWriteMode WriteMode
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String
    //
    private void DataSetFromXml(DataSet DataSet, String Xml, XmlReadMode ReadMode)
    {
      using (StringReader _sr = new StringReader(Xml))
      {
        DataSet.ReadXml(_sr, ReadMode);
      }
    } // DataSetFrmlXml


    #endregion // Private Methods
  }
}
