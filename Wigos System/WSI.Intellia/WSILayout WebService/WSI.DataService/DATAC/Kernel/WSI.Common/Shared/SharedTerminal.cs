//-------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//-------------------------------------------------------------------
//
// MODULE NAME:   SharedTerminal.cs
// DESCRIPTION:   
// AUTHOR:        Marcos Piedra Osuna
// CREATION DATE: 18-JUL-2012
//
// REVISION HISTORY:
//
// Date         Author Description
// -----------  ------ -----------------------------------------------
// 18-JUL-2012  MPO    Initial version
// -------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace WSI.Common
{
  public static class Terminal
  {

    #region Class

    public enum CreditsPlayMode
    {
      NotRedeemableLast = 0,   // NR2 -> PromoRE -> RE -> NR1
      PromotionalsFirst = 1,   // NR2 -> NR1 -> PromoRE -> RE
      SasMachine = 2,   // Defined by the SAS Machine
    }

    public class TerminalInfo
    {
      public Int32 TerminalId = 0;
      public Int32 ProvId = 0;
      public TerminalTypes Type = TerminalTypes.UNKNOWN;
      public String Name = "";
      public String ProviderName = "";
      public Boolean Blocked = true;
      public TerminalStatus Status = TerminalStatus.RETIRED;
      public CreditsPlayMode PlayMode = CreditsPlayMode.NotRedeemableLast;
      public Int64 CurrentPlaySessionId = -1;
      public Int64 CurrentAccountId = 0;
      public Int32 SASFlags = 0;

      public Decimal PvPointsMultiplier = 0;
      public Boolean PvSiteJackpot = true;
      public Boolean PvOnlyRedeemable = false;

      public Boolean SASFlagActivated(SAS_FLAGS Flag)
      {
        return Terminal.SASFlagActived(this.SASFlags, Flag);
      }
    }

    #endregion

    #region Public Methods

    // PURPOSE: Retrieve the terminal id according to  vendor id and the serial number
    //
    //  PARAMS:
    //     - INPUT:
    //           - VendorId:
    //           - SerialNumber:
    //           - MachineNumber:
    //           - TerminalId:
    //           - Trx:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //      - true: Failed retrieving 
    //      - false: error.

    public static Boolean Trx_GetTerminalInfo(String VendorId,
                                              String SerialNumber,
                                              Int32 MachineNumber,
                                              out TerminalInfo Terminal,
                                              SqlTransaction Trx)
    {
      StringBuilder _sb;
      Object _obj;

      Terminal = new TerminalInfo();

      try
      {
        _sb = new StringBuilder();
        _sb.Length = 0;

        _sb.AppendLine(" SELECT   @IgnoreMachineNumber = CAST(GP_KEY_VALUE AS int) ");
        _sb.AppendLine("   FROM   GENERAL_PARAMS ");
        _sb.AppendLine("  WHERE   GP_GROUP_KEY = 'Interface3GS' AND GP_SUBJECT_KEY ='IgnoreMachineNumber' ");

        _sb.AppendLine(" SELECT   TE_TERMINAL_ID ");
        _sb.AppendLine("   FROM   TERMINALS_3GS, TERMINALS ");
        _sb.AppendLine("  WHERE   T3GS_VENDOR_ID            = @pVendorId ");
        _sb.AppendLine("          AND T3GS_SERIAL_NUMBER    = @pSerialNumber ");
        _sb.AppendLine("          AND ( T3GS_MACHINE_NUMBER = @pMachineNumber OR @IgnoreMachineNumber = 1 ) ");
        _sb.AppendLine("          AND T3GS_TERMINAL_ID      = TE_TERMINAL_ID ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {

          _cmd.Parameters.Add("@IgnoreMachineNumber", SqlDbType.Int).Value = 0;
          _cmd.Parameters.Add("@pVendorId", SqlDbType.NVarChar).Value = VendorId;
          _cmd.Parameters.Add("@pSerialNumber", SqlDbType.NVarChar).Value = SerialNumber;
          _cmd.Parameters.Add("@pMachineNumber", SqlDbType.Int).Value = MachineNumber;

          _obj = _cmd.ExecuteScalar();

          if (_obj != null && _obj != DBNull.Value)
          {
            Int32 _terminal_id;

            _terminal_id = (Int32)_obj;

            return Trx_GetTerminalInfo(_terminal_id, out Terminal, Trx);
          }

          Trx_InsertPendingTerminal(0, VendorId, SerialNumber, MachineNumber, Trx);

          return true;
        }
      }
      catch
      { }

      return false;

    } // Trx_GetTerminalId

    // PURPOSE: If exists the terminal it update the table "TERMINALS_PENDING", otherwise insert into "TERMINALS_PENDING" 
    //
    //  PARAMS:
    //     - INPUT:
    //           - Source:
    //           - VendorId:
    //           - SerialNumber:
    //           - MachineNumber:
    //           - Trx:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //      - true: Fail updating TERMINALS_PENDING
    //      - false: error.

    public static Boolean Trx_InsertPendingTerminal(Int32 Source,
                                                    String VendorId,
                                                    String SerialNumber,
                                                    Int32 MachineNumber,
                                                    SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {

        _sb = new StringBuilder();
        _sb.Length = 0;

        _sb.AppendLine(" IF NOT EXISTS ");
        _sb.AppendLine(" ( ");
        _sb.AppendLine(" SELECT   1 ");
        _sb.AppendLine("   FROM   TERMINALS_PENDING ");
        _sb.AppendLine("  WHERE   TP_SOURCE         = @pSource ");
        _sb.AppendLine("    AND   TP_VENDOR_ID      = @pVendorId ");
        _sb.AppendLine("    AND   TP_SERIAL_NUMBER  = @pSerialNumber ");
        _sb.AppendLine(" ) ");

        _sb.AppendLine("   INSERT INTO TERMINALS_PENDING ( TP_SOURCE ");
        _sb.AppendLine("                                 , TP_VENDOR_ID ");
        _sb.AppendLine("                                 , TP_SERIAL_NUMBER ");
        _sb.AppendLine("                                 , TP_MACHINE_NUMBER ) ");
        _sb.AppendLine("                          VALUES ( @pSource ");
        _sb.AppendLine("                                 , @pVendorId ");
        _sb.AppendLine("                                 , @pSerialNumber ");
        _sb.AppendLine("                                 , @pMachineNumber ) ");

        _sb.AppendLine(" ELSE ");

        _sb.AppendLine("   UPDATE   TERMINALS_PENDING ");
        _sb.AppendLine("      SET   TP_MACHINE_NUMBER = @pMachineNumber ");
        _sb.AppendLine("    WHERE   TP_SOURCE         = @pSource ");
        _sb.AppendLine("      AND   TP_VENDOR_ID      = @pVendorId ");
        _sb.AppendLine("      AND   TP_SERIAL_NUMBER  = @pSerialNumber ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pSource", SqlDbType.Int).Value = Source;
          _cmd.Parameters.Add("@pVendorId", SqlDbType.NVarChar).Value = VendorId;
          _cmd.Parameters.Add("@pSerialNumber", SqlDbType.NVarChar).Value = SerialNumber;
          //Parameter for insert or update
          _cmd.Parameters.Add("@pMachineNumber", SqlDbType.Int).Value = MachineNumber;

          if (_cmd.ExecuteNonQuery() != 1)
          {
            return false;
          }

          return true;

        }

      }
      catch
      { }

      return false;

    } // Trx_InsertPendingTerminal

    // PURPOSE: Retrieve data of terminal
    //
    //  PARAMS:
    //     - INPUT:
    //           - TerminalId:
    //           - OutTerminalInfo:
    //           - Trx:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //      - true: Fail in retrieve data of db.
    //      - false: error.

    public static Boolean Trx_GetTerminalInfo(Int32 TerminalId,
                                              out TerminalInfo OutTerminalInfo,
                                              SqlTransaction Trx)
    {
      StringBuilder _sb;

      OutTerminalInfo = new TerminalInfo();
      _sb = new StringBuilder();

      try
      {

        _sb.AppendLine(" SELECT   TE_TERMINAL_ID             "); // 0
        _sb.AppendLine("        , TE_PROV_ID                 "); // 1
        _sb.AppendLine("        , TE_TERMINAL_TYPE           "); // 2
        _sb.AppendLine("        , TE_NAME                    "); // 3
        _sb.AppendLine("        , TE_PROVIDER_ID             "); // 4
        _sb.AppendLine("        , TE_BLOCKED                 "); // 5
        _sb.AppendLine("        , TE_STATUS                  "); // 6
        _sb.AppendLine("        , TE_CURRENT_PLAY_SESSION_ID "); // 7
        _sb.AppendLine("        , TE_CURRENT_ACCOUNT_ID      "); // 8
        _sb.AppendLine("        , PV_POINTS_MULTIPLIER       "); // 9
        _sb.AppendLine("        , PV_SITE_JACKPOT            "); // 10
        _sb.AppendLine("        , PV_ONLY_REDEEMABLE         "); // 11
        _sb.AppendLine("        , TE_SAS_FLAGS               "); // 12
        _sb.AppendLine("        , ISNULL ( ( SELECT   CAST (GP_KEY_VALUE AS INT)         ");
        _sb.AppendLine(" 	                     FROM   GENERAL_PARAMS                     ");
        _sb.AppendLine(" 	                    WHERE   GP_GROUP_KEY   = 'Credits'         ");
        _sb.AppendLine(" 	                      AND   GP_SUBJECT_KEY = 'PlayMode'        ");
        _sb.AppendLine("	                      AND   ISNUMERIC(GP_KEY_VALUE) = 1 ), 0 ) ");
        _sb.AppendLine("	        AS SYSTEM_PLAY_MODE        "); // 13
        _sb.AppendLine("   FROM   TERMINALS LEFT JOIN PROVIDERS ON PV_ID = TE_PROV_ID");
        _sb.AppendLine("  WHERE   TE_TERMINAL_ID = @pTerminalId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              return false;
            }

            OutTerminalInfo.TerminalId = _reader.GetInt32(0);
            if (!_reader.IsDBNull(1))
            {
              OutTerminalInfo.ProvId = _reader.GetInt32(1);
            }
            OutTerminalInfo.Type = (TerminalTypes)_reader.GetInt16(2);
            OutTerminalInfo.Name = _reader.GetString(3);
            OutTerminalInfo.ProviderName = _reader.GetString(4);
            OutTerminalInfo.Blocked = _reader.GetBoolean(5);
            OutTerminalInfo.Status = (TerminalStatus)_reader.GetInt32(6);
            OutTerminalInfo.CurrentPlaySessionId = _reader.IsDBNull(7) ? 0 : _reader.GetInt64(7);
            OutTerminalInfo.CurrentAccountId = _reader.IsDBNull(8) ? 0 : _reader.GetInt64(8);

            OutTerminalInfo.PvPointsMultiplier = Math.Max(0, _reader.GetDecimal(9));
            OutTerminalInfo.PvSiteJackpot = _reader.GetBoolean(10);
            OutTerminalInfo.PvOnlyRedeemable = _reader.GetBoolean(11);

            OutTerminalInfo.SASFlags = _reader.GetInt32(12);

            OutTerminalInfo.PlayMode = (CreditsPlayMode)_reader.GetInt32(13);
            switch (OutTerminalInfo.PlayMode)
            {
              case CreditsPlayMode.NotRedeemableLast:
              case CreditsPlayMode.PromotionalsFirst:
                break;

              default:
                OutTerminalInfo.PlayMode = CreditsPlayMode.NotRedeemableLast;
                break;
            }

            if (OutTerminalInfo.PlayMode == CreditsPlayMode.PromotionalsFirst)
            {
              if (OutTerminalInfo.Type == TerminalTypes.SAS_HOST)
              {
                if (OutTerminalInfo.SASFlagActivated(SAS_FLAGS.CREDITS_PLAY_MODE_SAS_MACHINE))
                {
                  OutTerminalInfo.PlayMode = CreditsPlayMode.SasMachine;
                }
              }
            }

          }

        }
        return true;

      }
      catch
      { }

      return false;
    } // Trx_TerminalInfo

    public static Boolean SASFlagActived(Int32 CurrentFlags, SAS_FLAGS Flag)
    {
      return ((CurrentFlags & (Int32)Flag) == (Int32)Flag);
    }


    // PURPOSE : Insert or update GAME_METERS for every play
    //
    //  PARAMS :
    //      - INPUT :
    //          - (I/O) ReportPlays
    //          - SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    public static SqlCommand GetGameMetersUpdateCommand(SqlTransaction Trx)
    {
      StringBuilder _sb;
      SqlCommand _cmd;

      // Insert or Update command
      _sb = new StringBuilder();
      _sb.AppendLine("IF NOT EXISTS ( ");
      _sb.AppendLine("      SELECT   1 ");
      _sb.AppendLine("        FROM   GAME_METERS ");
      _sb.AppendLine("       WHERE   GM_TERMINAL_ID    = @pTerminalId ");
      _sb.AppendLine("         AND   GM_GAME_BASE_NAME = @pGameBaseName ");
      _sb.AppendLine("      ) ");
      _sb.AppendLine("   INSERT INTO   GAME_METERS ");
      _sb.AppendLine("               ( GM_TERMINAL_ID ");
      _sb.AppendLine("               , GM_GAME_BASE_NAME ");
      _sb.AppendLine("               , GM_WCP_SEQUENCE_ID ");
      _sb.AppendLine("               , GM_DENOMINATION ");
      _sb.AppendLine("               , GM_PLAYED_COUNT ");
      _sb.AppendLine("               , GM_PLAYED_AMOUNT ");
      _sb.AppendLine("               , GM_WON_COUNT ");
      _sb.AppendLine("               , GM_WON_AMOUNT ");
      _sb.AppendLine("               , GM_JACKPOT_AMOUNT ");
      _sb.AppendLine("               , GM_DELTA_GAME_NAME ");
      _sb.AppendLine("               , GM_DELTA_PLAYED_COUNT ");
      _sb.AppendLine("               , GM_DELTA_PLAYED_AMOUNT ");
      _sb.AppendLine("               , GM_DELTA_WON_COUNT ");
      _sb.AppendLine("               , GM_DELTA_WON_AMOUNT ");
      _sb.AppendLine("               , GM_LAST_REPORTED ");
      _sb.AppendLine("               ) ");
      _sb.AppendLine("        VALUES ");
      _sb.AppendLine("               ( @pTerminalId ");
      _sb.AppendLine("               , @pGameBaseName ");
      _sb.AppendLine("               , @pWcpSequenceId ");
      _sb.AppendLine("               , @pDenomination ");
      _sb.AppendLine("               , @pDeltaPlayedCount ");
      _sb.AppendLine("               , @pDeltaPlayedAmount ");
      _sb.AppendLine("               , @pDeltaWonCount ");
      _sb.AppendLine("               , @pDeltaWonAmount ");
      _sb.AppendLine("               , 0 ");
      _sb.AppendLine("               , @pGameBaseName ");
      _sb.AppendLine("               , @pDeltaPlayedCount ");
      _sb.AppendLine("               , @pDeltaPlayedAmount ");
      _sb.AppendLine("               , @pDeltaWonCount ");
      _sb.AppendLine("               , @pDeltaWonAmount ");
      _sb.AppendLine("               , GETDATE() ");
      _sb.AppendLine("               ) ");
      _sb.AppendLine("ELSE ");
      _sb.AppendLine("               ");
      _sb.AppendLine("   UPDATE   GAME_METERS ");
      _sb.AppendLine("      SET   GM_WCP_SEQUENCE_ID     = @pWcpSequenceId ");
      _sb.AppendLine("          , GM_DENOMINATION        = @pDenomination ");
      _sb.AppendLine("          , GM_PLAYED_COUNT        = GM_PLAYED_COUNT         + @pDeltaPlayedCount ");
      _sb.AppendLine("          , GM_PLAYED_AMOUNT       = GM_PLAYED_AMOUNT        + @pDeltaPlayedAmount ");
      _sb.AppendLine("          , GM_WON_COUNT           = GM_WON_COUNT            + @pDeltaWonCount ");
      _sb.AppendLine("          , GM_WON_AMOUNT          = GM_WON_AMOUNT           + @pDeltaWonAmount ");
      _sb.AppendLine("          , GM_DELTA_GAME_NAME     = @pGameBaseName ");
      _sb.AppendLine("          , GM_DELTA_PLAYED_COUNT  = GM_DELTA_PLAYED_COUNT   + @pDeltaPlayedCount ");
      _sb.AppendLine("          , GM_DELTA_PLAYED_AMOUNT = GM_DELTA_PLAYED_AMOUNT  + @pDeltaPlayedAmount ");
      _sb.AppendLine("          , GM_DELTA_WON_COUNT     = GM_DELTA_WON_COUNT      + @pDeltaWonCount ");
      _sb.AppendLine("          , GM_DELTA_WON_AMOUNT    = GM_DELTA_WON_AMOUNT     + @pDeltaWonAmount ");
      _sb.AppendLine("          , GM_LAST_REPORTED       = GETDATE() ");
      _sb.AppendLine("    WHERE   GM_TERMINAL_ID         = @pTerminalId ");
      _sb.AppendLine("      AND   GM_GAME_BASE_NAME      = @pGameBaseName ");

      _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx);

      //_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "BU_COLUMN_TERMINAL_ID";
      //_cmd.Parameters.Add("@pGameBaseName", SqlDbType.NVarChar, 50).SourceColumn = "BU_COLUMN_GAME_NAME";
      //_cmd.Parameters.Add("@pDenomination", SqlDbType.Money).SourceColumn = "BU_COLUMN_DENOMINATION";
      //_cmd.Parameters.Add("@pWcpSequenceId", SqlDbType.BigInt).SourceColumn = "BU_COLUMN_WCP_SEQUENCE_ID";
      //_cmd.Parameters.Add("@pDeltaPlayedCount", SqlDbType.BigInt).Value = 1;
      //_cmd.Parameters.Add("@pDeltaPlayedAmount", SqlDbType.Money).SourceColumn = "BU_COLUMN_PLAYED_AMOUNT";
      //_cmd.Parameters.Add("@pDeltaWonCount", SqlDbType.BigInt).SourceColumn = "BU_COLUMN_WON_COUNT";
      //_cmd.Parameters.Add("@pDeltaWonAmount", SqlDbType.Money).SourceColumn = "BU_COLUMN_WON_AMOUNT";

      return _cmd;

    } // Cmd_InsertOrUpdateGameMeters

    #endregion

  }
}
