//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: DrawOfferList.cs
// 
//   DESCRIPTION: 
// 
//        AUTHOR: Andreu Juli�, Lucas Cordero, Ra�l Cervera
// 
// CREATION DATE: 02-MAY-2011
// 
// REVISION HISTORY:
// 
// Date        Author           Description
// ----------- ------           ------------------------------------------------
// 02-MAY-2011 AJQ, LCM, RCI    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.IO;

namespace WSI.Common
{
  public class ProviderList
  {
    public enum ProviderListType
    {
      All = 0,
      Listed = 1,
      NotListed = 2,
    }

    DataTable m_provider = null;
    DataTable m_type = null;
    DataSet   m_ds   = null;

    # region Constructors

    //------------------------------------------------------------------------------
    // PURPOSE : Constructor for a ProviderList instance
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public ProviderList()
    {
      Init();
    } // ProviderList

    #endregion // Constructors

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Get the type of the ProviderList.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - ProviderListType Type
    // 
    public ProviderListType ListType
    {
      get
      {
        return ((ProviderListType)m_type.Rows[0][0]);
      }
      set 
      {
        m_type.Rows[0][0] = value;
        m_provider.Clear();
      }
    } // ListType

    //------------------------------------------------------------------------------
    // PURPOSE: Get the type of the ProviderList.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - List
    // 
    public String AuditString (String All, String Listed, String Excluded, String None)
    {
      String _prefix;
      StringBuilder _list;
      
      switch ( (ProviderListType)m_type.Rows[0][0] )
      {
        case ProviderListType.All:
          return All;

        case ProviderListType.Listed:
          _prefix = Listed;
          break;

        case ProviderListType.NotListed:
          _prefix = Excluded;
          break;

        default:
          throw new Exception("Unknown ProviderListType: " + (Int32) m_type.Rows[0][0]);
      }
      
      
      _list = new StringBuilder();
      foreach (DataRow _dr in m_provider.Rows)
      {
        if ( _list.Length > 0 )
        {
          _list.Append(", ");
        }
        _list.Append((String)_dr[0]);
      }
      
      return _prefix + ": " + (_list.ToString() != string.Empty ? _list.ToString() : None );

    } // AuditString
    

    //------------------------------------------------------------------------------
    // PURPOSE: Add a new provider.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - String Provider
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    public void AddProvider(String Provider)
    {
      DataRow _dr;

      if ((ProviderListType)m_type.Rows[0][0] == ProviderListType.All)
      {
        return;
      }

      if (SeekProvider(Provider) != null)
      {
        return;
      }

      _dr = m_provider.NewRow();
      _dr[0] = Provider;
      m_provider.Rows.Add(_dr);
    } // AddProvider

    //------------------------------------------------------------------------------
    // PURPOSE: Remove a provider.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - String Provider
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    public void RemoveProvider(String Provider)
    {
      DataRow _dr;

      _dr = SeekProvider(Provider);
      if (_dr != null)
      {
        _dr.Delete();
        m_provider.AcceptChanges();
      }
    } // RemoveProvider

    //------------------------------------------------------------------------------
    // PURPOSE: Indicates if a Provider is in the ProviderList.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - String Provider
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - True: The provider is in the list. False: Otherwise.
    // 
    public Boolean Contains(String Provider)
    {
      switch ((ProviderListType)m_type.Rows[0][0])
      {
        case ProviderListType.All:
          return true;

        case ProviderListType.Listed:
          return (SeekProvider(Provider) != null);

        case ProviderListType.NotListed:
          return (SeekProvider(Provider) == null);

        default:
          return false;
      }
    } // Contains

    //------------------------------------------------------------------------------
    // PURPOSE: Get a XML representation of the provider list.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - String
    // 
    public String ToXml()
    {
      if ((ProviderListType)m_type.Rows[0][0] == ProviderListType.All)
      {
        return null;
      }

      return DataSetToXml(m_ds, XmlWriteMode.IgnoreSchema);
    } // ToXml

    //------------------------------------------------------------------------------
    // PURPOSE: Read a XML representation of the provider list and store it into the DataSet.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    public void FromXml(String Xml)
    {
      if (Xml == null)
      {
        ListType = ProviderListType.All;

        return;
      }

      m_type.Clear();
      m_provider.Clear();
      DataSetFromXml(m_ds, Xml, XmlReadMode.IgnoreSchema);
    } // FromXml

    #endregion // Public Methods

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Init the properties of the class.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    private void Init()
    {
      DataRow _dr;
      ProviderListType _list_type;

      _list_type = ProviderListType.All;

      m_ds = new DataSet("ProviderList");

      m_type = new DataTable("ListType");
      m_type.Columns.Add("Type", _list_type.GetType());
      _dr = m_type.NewRow();
      _dr[0] = ProviderListType.All;
      m_type.Rows.Add(_dr);

      m_provider = new DataTable("Provider");
      m_provider.Columns.Add("Name", Type.GetType("System.String"));
      m_provider.PrimaryKey = new DataColumn[1] { m_provider.Columns[0] };

      m_ds.Tables.Add(m_type);
      m_ds.Tables.Add(m_provider);
    } // Init

    //------------------------------------------------------------------------------
    // PURPOSE: Search for the provider in the provider list.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - String Key
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - DataRow: If found return the DataRow.
    // 
    private DataRow SeekProvider(String Provider)
    {
      foreach (DataRow _dr in m_provider.Rows)
      {
        if (Provider.Equals((String)_dr[0]))
        {
          return _dr;
        }
      }
      return null;
    } // SeekProvider

    //------------------------------------------------------------------------------
    // PURPOSE : DataSet to Xml
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataSet DataSet
    //          - XmlWriteMode WriteMode
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String
    //
    private String DataSetToXml(DataSet DataSet, XmlWriteMode WriteMode)
    {
      String _xml;

      _xml = "";
      using (StringWriter _sw = new StringWriter())
      {
        DataSet.WriteXml(_sw, WriteMode);
        _xml = _sw.ToString();
      }

      return _xml;
    } // DataSetToXml

    #endregion // Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE : DataSet to Xml
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataSet DataSet
    //          - XmlWriteMode WriteMode
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String
    //
    private void DataSetFromXml(DataSet DataSet, String Xml, XmlReadMode ReadMode)
    {
      using (StringReader _sr = new StringReader(Xml))
      {
        DataSet.ReadXml(_sr, ReadMode);
      }

    } // DataSetFromXml

  } // ProviderList

}  // WSI.Common
