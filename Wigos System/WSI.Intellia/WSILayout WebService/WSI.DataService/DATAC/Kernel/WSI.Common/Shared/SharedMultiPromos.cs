//-------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//-------------------------------------------------------------------
//
// MODULE NAME:   MultiPromos.cs
// DESCRIPTION:   
// AUTHOR:        Marcos Piedra Osuna
// CREATION DATE: 05-JUN-2012
//
// REVISION HISTORY:
//
// Date         Author Description
// -----------  ------ -----------------------------------------------
// 05-JUN-2012  MPO    Initial version
// 16-AUG-2012  RCI    Check if System has disabled new PlaySessions
// 16-AUG-2012  RCI    Release inactive play sessions and block related accounts.
// 19-SEP-2012  DDM    Added function Trx_SetInitialREPromotionInAccounts.
// -------------------------------------------------------------------

using System;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using System.Data.SqlClient;
using System.Data;
using WSI.Common;
using System.Xml;

namespace WSI.Common
{
  public static class MultiPromos
  {

    #region Class

    public const Int64 HANDPAY_VIRTUAL_PLAY_SESSION_ID = Int64.MinValue;

    public enum StartSessionStatus
    {
      Ok = 0,
      // Generic Error
      Error = 1,
      // Account Error
      AccountUnknown = 10,
      AccountBlocked = 11,
      AccountInSession = 12,
      // Terminal Error
      TerminalUnknown = 20,
      TerminalBlocked = 21,
    }

    public class StartSessionOutput
    {
      public Int64 PlaySessionId = 0;
      public Boolean IsNewPlaySession = false;

      public AccountBalance IniBalance = AccountBalance.Zero;
      public AccountBalance PlayableBalance = AccountBalance.Zero;
      public AccountBalance FinBalance = AccountBalance.Zero;

      public AccountBalance TotalToGMBalance = AccountBalance.Zero;
      public Decimal Points = 0;
      public String HolderName;


      public StartSessionStatus StatusCode = StartSessionStatus.Error;
      public StringBuilder ErrorMsg = new StringBuilder();

      public void SetError(StartSessionStatus Error, String Message)
      {
        StatusCode = Error;
        if (!String.IsNullOrEmpty(Message))
        {
          ErrorMsg.AppendLine(Message);
        }
      }

      public void Warning(String Message)
      {
        if (!String.IsNullOrEmpty(Message))
        {
          ErrorMsg.AppendLine("Warning: " + Message);
        }
      }

      public void SetSuccess()
      {
        StatusCode = StartSessionStatus.Ok;
      }

      public String Message
      {
        get
        {
          return ErrorMsg.ToString();
        }
      }
    }

    public class EndSessionInput
    {
      public MovementType MovementType = MovementType.EndCardSession;
      public String CashierName = ""; // Used for manual close in Cashier and GUI.

      public Int64 AccountId = -1;
      public Int64 PlaySessionId = -1;
      public Int64 TransactionId = 0;

      public Boolean FinalBalanceSelectedByUserAfterMismatch = false;
      public Decimal BalanceFromGM = 0;

      public Boolean HasMeters = false;
      public PlayedWonMeters Meters = new PlayedWonMeters();
    }

    public class PlayedWonMeters
    {
      public Int64 PlayedCount = 0;
      public Decimal PlayedAmount = 0;
      public Int64 WonCount = 0;
      public Decimal WonAmount = 0;
      public Decimal JackpotAmount = 0;
    }

    public enum EndSessionStatus
    {
      Ok = 0,
      ErrorRetry = 1,
      FaltalError = 2,
      NotOpened = 3,
      BalanceMismatch = 4,
      // Account Error
      AccountUnknown = 10,
      AccountBlocked = 11,
      AccountInSession = 12,
      // Terminal Error
      TerminalUnknown = 20,
      TerminalBlocked = 21,
    }

    public class EndSessionOutput
    {
      public AccountBalance PlaySessionFinalBalance = AccountBalance.Zero;

      public EndSessionStatus StatusCode = EndSessionStatus.ErrorRetry;
      public StringBuilder ErrorMsg = new StringBuilder();

      public void SetError(EndSessionStatus Error, String Message)
      {
        StatusCode = Error;
        if (!String.IsNullOrEmpty(Message))
        {
          ErrorMsg.AppendLine(Message);
        }
      }

      public void Warning(String Message)
      {
        if (!String.IsNullOrEmpty(Message))
        {
          ErrorMsg.AppendLine("Warning: " + Message);
        }
      }

      public void SetSuccess()
      {
        StatusCode = EndSessionStatus.Ok;
      }

      public String Message
      {
        get
        {
          return ErrorMsg.ToString();
        }
      }
    }

    public class InSessionParameters
    {
      public Int64 PlaySessionId = 0;
      public Int64 AccountId = 0;
      public DataTable AccountPromotion = null;


      public Decimal DeltaPlayed = 0;
      public Decimal DeltaWon = 0;

      // In Session
      public Decimal DeltaPlayedRe = 0;
      public Decimal DeltaPlayedNr = 0;
      public Decimal DeltaWonRe = 0;
      public Decimal DeltaWonNr = 0;

      public AccountBalance Balance = AccountBalance.Zero;

      public AccountBalance Played = AccountBalance.Zero;
      public AccountBalance Won = AccountBalance.Zero;


    }

    public class PlaySessionCloseParameters
    {
      public MovementType MovementType = MovementType.EndCardSession;
      public String CashierName = ""; // Used for manual close in Cashier and GUI.

      public Int64 PlaySessionId = 0;
      public Int64 AccountId = 0;
      public Int32 TerminalId = 0;
      public String TerminalName = "";

      public Boolean BalanceMismatch = true;
      public AccountBalance BalanceFromGMToAdd = AccountBalance.Zero;
      public StringBuilder ErrorMsg = new StringBuilder();
      public Int64 NumPlayed = 0;
      public AccountBalance Played = AccountBalance.Zero;
      public Int64 NumWon = 0;
      public AccountBalance Won = AccountBalance.Zero;
    }

    public class AccountBalance
    {
      public Decimal Redeemable;
      public Decimal PromoRedeemable;
      public Decimal PromoNotRedeemable;

      public override String ToString()
      {
        return String.Format("{0} <RE: {1}, PR: {2}, NR: {3}>", TotalBalance.ToString(), Redeemable.ToString(), PromoRedeemable.ToString(), PromoNotRedeemable.ToString());
      }

      public AccountBalance()
      {
      } // AccountBalance

      public AccountBalance(AccountBalance Value)
      {
        this.Redeemable = Value.Redeemable;
        this.PromoRedeemable = Value.PromoRedeemable;
        this.PromoNotRedeemable = Value.PromoNotRedeemable;
      } // AccountBalance

      public AccountBalance(Decimal Redeemable, Decimal PromoRedeemable, Decimal PromoNotRedeemable)
      {
        this.Redeemable = Redeemable;
        this.PromoRedeemable = PromoRedeemable;
        this.PromoNotRedeemable = PromoNotRedeemable;
      } // AccountBalance

      public Decimal TotalRedeemable
      {
        get
        {
          return Redeemable + PromoRedeemable;
        }
      }

      public Decimal TotalNotRedeemable
      {
        get
        {
          return PromoNotRedeemable;
        }
      }

      public Decimal TotalBalance
      {
        get
        {
          return Redeemable + PromoRedeemable + PromoNotRedeemable;
        }
      }

      static public AccountBalance Zero
      {
        get
        {
          return new AccountBalance();
        }
      }

      public static AccountBalance operator +(AccountBalance Bal1, AccountBalance Bal2)
      {
        return new AccountBalance(Bal1.Redeemable + Bal2.Redeemable,
                                  Bal1.PromoRedeemable + Bal2.PromoRedeemable,
                                  Bal1.PromoNotRedeemable + Bal2.PromoNotRedeemable);
      }

      public static AccountBalance operator -(AccountBalance Bal1, AccountBalance Bal2)
      {
        return new AccountBalance(Bal1.Redeemable - Bal2.Redeemable,
                                  Bal1.PromoRedeemable - Bal2.PromoRedeemable,
                                  Bal1.PromoNotRedeemable - Bal2.PromoNotRedeemable);
      }

      public static Boolean operator ==(AccountBalance Bal1, AccountBalance Bal2)
      {
        return (Bal1.Redeemable == Bal2.Redeemable
              && Bal1.PromoRedeemable == Bal2.PromoRedeemable
              && Bal1.PromoNotRedeemable == Bal2.PromoNotRedeemable);
      }

      public static Boolean operator !=(AccountBalance Bal1, AccountBalance Bal2)
      {
        return !(Bal1 == Bal2);
      }

      public override bool Equals(object obj)
      {
        return (this == (AccountBalance)obj);
      }

      public override int GetHashCode()
      {
        return base.GetHashCode();
      }

      static public AccountBalance Negate(AccountBalance Bal1)
      {
        return new AccountBalance(-Bal1.Redeemable, -Bal1.PromoRedeemable, -Bal1.PromoNotRedeemable);
      } // Negate

#if !SQL_BUSINESS_LOGIC
      // For messages propose
      public String ToXml()
      {
        StringBuilder _sb;

        _sb = new StringBuilder();
        _sb.AppendLine("<Redeemable>" + ((Int64)(Redeemable * 100)).ToString() + "</Redeemable>");
        _sb.AppendLine("<PromoRedeemable>" + ((Int64)(PromoRedeemable * 100)).ToString() + "</PromoRedeemable>");
        // Only Append (the caller set end of line if is necessary).
        _sb.Append("<PromoNotRedeemable>" + ((Int64)(PromoNotRedeemable * 100)).ToString() + "</PromoNotRedeemable>");

        return _sb.ToString();
      }

      public void LoadXml(XmlReader XmlReader)
      {
        Int64 _aux_int64;

        if (Int64.TryParse(XML.ReadTagValue(XmlReader, "Redeemable", true), out _aux_int64))
        {
          Redeemable = ((Decimal)_aux_int64) / 100;
        }
        if (Int64.TryParse(XML.ReadTagValue(XmlReader, "PromoRedeemable", true), out _aux_int64))
        {
          PromoRedeemable = ((Decimal)_aux_int64) / 100;
        }
        if (Int64.TryParse(XML.ReadTagValue(XmlReader, "PromoNotRedeemable", true), out _aux_int64))
        {
          PromoNotRedeemable = ((Decimal)_aux_int64) / 100;
        }
      }
#endif

    } // AccountBalance

    public class PromoBalance
    {
      public AccountBalance Balance;
      public Decimal Points;

      public override String ToString()
      {
        return String.Format("{0}, PT: {1}", Balance.ToString(), Points.ToString());
      }

      public PromoBalance()
      {
        Balance = AccountBalance.Zero;
        Points = 0;
      } // PromoBalance

      public PromoBalance(PromoBalance PromoBalance)
      {
        this.Balance = new AccountBalance(PromoBalance.Balance);
        this.Points = PromoBalance.Points;
      } // PromoBalance

      public PromoBalance(AccountBalance Balance, Decimal Points)
      {
        this.Balance = new AccountBalance(Balance);
        this.Points = Points;
      } // PromoBalance

      static public PromoBalance Zero
      {
        get
        {
          return new PromoBalance();
        }
      }

      public static PromoBalance operator +(PromoBalance Bal1, PromoBalance Bal2)
      {
        return new PromoBalance(Bal1.Balance + Bal2.Balance, Bal1.Points + Bal2.Points);
      }

      public static PromoBalance operator -(PromoBalance Bal1, PromoBalance Bal2)
      {
        return new PromoBalance(Bal1.Balance - Bal2.Balance, Bal1.Points - Bal2.Points);
      }

      public static Boolean operator ==(PromoBalance Bal1, PromoBalance Bal2)
      {
        return (Bal1.Balance == Bal2.Balance && Bal1.Points == Bal2.Points);
      }

      public static Boolean operator !=(PromoBalance Bal1, PromoBalance Bal2)
      {
        return !(Bal1 == Bal2);
      }

      public override bool Equals(object obj)
      {
        return (this == (PromoBalance)obj);
      }

      public override int GetHashCode()
      {
        return base.GetHashCode();
      }
    } // PromoBalance

    public class AccountInfo
    {
      public Int64 AccountId = 0;

      public Boolean Blocked = true;
      public Int32 BlockReason = 0;

      public Int32 HolderLevel = 0;
      public Int32 HolderGender = 0;
      public String HolderName = "";

      public Int64 CancellableOperationId = 0;

      public Int64 CurrentPlaySessionId = 0;
      public Int32 CurrentTerminalId = 0;
      public PlaySessionStatus CurrentPlaySessionStatus = PlaySessionStatus.Closed;
      public Boolean CurrentPlaySessionBalanceMismatch = true;
      public Decimal CurrentPlaySessionInitialBalance = 0;
      public Int64 CancellableTrxId = 0;
      public AccountBalance CancellableBalance = AccountBalance.Zero;

      public AccountBalance FinalBalance = AccountBalance.Zero;
      public Decimal FinalPoints = 0;


      public String ErrorMessage = "";
    }

    #endregion

    #region Public Methods

    public enum AccountPromotionsAction
    {
      CancelByPlayer = 1,
      NotRedeemableToRedeemable = 2,
    }

    // PURPOSE: Change the status to redeem of the promotions and then return the sum
    // PURPOSE: Change the status to cancelled and return the sum
    //
    //  PARAMS:
    //     - INPUT:
    //           - Action:
    //           - AccountId:
    //           - AffectedBalance:
    //           - Trx:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - true if is updated successfully, false otherwise.

    public static Boolean Trx_AccountPromotionsAction(AccountPromotionsAction Action,
                                                      Int64 AccountId,
                                                      out AccountBalance AffectedBalance,
                                                      out Int32 NumPromos,
                                                      SqlTransaction Trx)
    {
      StringBuilder _sb;
      DataTable _promos;
      AccountBalance _ini_balance;
      String _sql_where;
      ACCOUNT_PROMO_STATUS _status;
      Int64 _previous_play_session_id;

      AffectedBalance = AccountBalance.Zero;
      NumPromos = 0;

      try
      {
        if (!MultiPromos.Trx_UpdateAccountBalance(AccountId, MultiPromos.AccountBalance.Zero, out _ini_balance, out _previous_play_session_id, Trx))
        {
          return false;
        }

        if (_previous_play_session_id != 0)
        {
          return false;
        }

        _sb = new StringBuilder();

        switch (Action)
        {
          case AccountPromotionsAction.CancelByPlayer:
            _status = ACCOUNT_PROMO_STATUS.CANCELLED_BY_PLAYER;
            _sb.Length = 0;
            _sb.AppendLine("          ACP_ACCOUNT_ID       = @pAccountId ");
            _sb.AppendLine("    AND   ACP_STATUS           = @pStatusActive ");
            // The account is the master and it's not in session. If any promotion is in session, it's an error: will be cancelled.
            //_sb.AppendLine("    AND   ACP_PLAY_SESSION_ID    IS NULL ");
            _sb.AppendLine("    AND   ACP_CREDIT_TYPE      = @pCreditTypeNR1 ");
            _sb.AppendLine("    AND   ACP_BALANCE         >= 0 ");

            _sql_where = _sb.ToString();

            _sb.Length = 0;
            _sb.AppendLine("IF (SELECT COUNT(*)      FROM ACCOUNT_PROMOTIONS WITH(INDEX(IX_ACP_ACCOUNT_STATUS)) WHERE ACP_WONLOCK IS NOT NULL AND " + _sql_where + ") > 0 ");
            _sb.AppendLine("    SELECT ACP_UNIQUE_ID FROM ACCOUNT_PROMOTIONS WITH(INDEX(IX_ACP_ACCOUNT_STATUS)) WHERE ACP_WONLOCK IS NOT NULL AND " + _sql_where);
            _sb.AppendLine("ELSE ");
            _sb.AppendLine("  IF (SELECT COUNT(*)      FROM ACCOUNT_PROMOTIONS WITH(INDEX(IX_ACP_ACCOUNT_STATUS)) WHERE ACP_WONLOCK IS NULL AND " + _sql_where + ") > 0 ");
            _sb.AppendLine("      SELECT ACP_UNIQUE_ID FROM ACCOUNT_PROMOTIONS WITH(INDEX(IX_ACP_ACCOUNT_STATUS)) WHERE ACP_WONLOCK IS NULL AND " + _sql_where);
            _sb.AppendLine("  ELSE ");
            _sb.AppendLine("      SELECT CAST (-1 AS BIGINT) ");

            break;

          case AccountPromotionsAction.NotRedeemableToRedeemable:
            _status = ACCOUNT_PROMO_STATUS.REDEEMED;
            _sb.Length = 0;
            _sb.AppendLine("          ACP_ACCOUNT_ID       = @pAccountId ");
            _sb.AppendLine("    AND   ACP_STATUS           = @pStatusActive ");
            // When it's going to convert NR to RE, it's necessary to check that the promotions are not in session.
            _sb.AppendLine("    AND   ACP_PLAY_SESSION_ID    IS NULL ");
            _sb.AppendLine("    AND   ACP_CREDIT_TYPE      = @pCreditTypeNR1 ");
            _sb.AppendLine("    AND   ACP_BALANCE         >= ACP_WONLOCK ");
            _sb.AppendLine("    AND   ACP_WONLOCK            IS NOT NULL ");

            _sql_where = _sb.ToString();

            _sb.Length = 0;
            _sb.AppendLine(" SELECT   ACP_UNIQUE_ID ");
            _sb.AppendLine("   FROM   ACCOUNT_PROMOTIONS WITH(INDEX(IX_acp_account_status))");
            _sb.AppendLine("  WHERE   " + _sql_where);

            break;

          default:
            return false;
        }

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _cmd.Parameters.Add("@pStatusActive", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.ACTIVE;
          _cmd.Parameters.Add("@pCreditTypeNR1", SqlDbType.Int).Value = ACCOUNT_PROMO_CREDIT_TYPE.NR1;
          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
          {
            _promos = new DataTable();
            _sql_da.Fill(_promos);
          }
        }

        if (_promos.Rows.Count <= 0)
        {
          return true;
        }

        _sb.Length = 0;
        _sb.AppendLine(" UPDATE   ACCOUNT_PROMOTIONS ");
        _sb.AppendLine("    SET   ACP_STATUS           = @pNewStatus ");
        _sb.AppendLine(" OUTPUT   DELETED.ACP_BALANCE ");
        _sb.AppendLine("   FROM   ACCOUNT_PROMOTIONS WITH(INDEX(PK_account_promotions)) ");
        _sb.AppendLine("  WHERE   ACP_UNIQUE_ID        = @pUniqueId ");
        _sb.AppendLine("    AND   " + _sql_where);

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          SqlParameter _unique_id;

          _unique_id = _cmd.Parameters.Add("@pUniqueId", SqlDbType.BigInt);
          _cmd.Parameters.Add("@pNewStatus", SqlDbType.Int).Value = _status;
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _cmd.Parameters.Add("@pStatusActive", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.ACTIVE;
          _cmd.Parameters.Add("@pCreditTypeNR1", SqlDbType.Int).Value = ACCOUNT_PROMO_CREDIT_TYPE.NR1;

          foreach (DataRow _promo in _promos.Rows)
          {
            _unique_id.Value = _promo[0];

            if ((Int64)_promo[0] == -1) // Fix any mismatch Account.NR > 0, Promos.NR = 0
            {
              NumPromos = 0;
              AffectedBalance.PromoNotRedeemable = _ini_balance.PromoNotRedeemable;

              break;
            }

            using (SqlDataReader _reader = _cmd.ExecuteReader())
            {
              if (!_reader.Read())
              {
                // Not an error, conditions changed ...
                continue;
              }

              NumPromos++;
              AffectedBalance.PromoNotRedeemable += _reader.GetDecimal(0);
            }
          }
        }

        AffectedBalance.Redeemable = Math.Min(AffectedBalance.Redeemable, _ini_balance.Redeemable);
        AffectedBalance.PromoRedeemable = Math.Min(AffectedBalance.PromoRedeemable, _ini_balance.PromoRedeemable);
        AffectedBalance.PromoNotRedeemable = Math.Min(AffectedBalance.PromoNotRedeemable, _ini_balance.PromoNotRedeemable);

        return true;
      }
      catch
      { ; }

      return false;

    } // Trx_AccountPromotionsAction



    public static MultiPromos.StartSessionOutput Trx_WcpStartCardSession(Int64 TransactionId, int TerminalId, String ExternalTrackData, SqlTransaction Trx)
    {
      MultiPromos.StartSessionOutput _output;
      Terminal.TerminalInfo _terminal;
      MultiPromos.AccountInfo _account;


      _output = new MultiPromos.StartSessionOutput();

      if (!MultiPromos.Trx_UpdateAccountBalance(0, ExternalTrackData, MultiPromos.AccountBalance.Zero, 0, out _account, Trx))
      {
        _output.SetError(MultiPromos.StartSessionStatus.AccountUnknown, "");

        return _output;
      }

      if (!Terminal.Trx_GetTerminalInfo(TerminalId, out _terminal, Trx))
      {
        _output.SetError(MultiPromos.StartSessionStatus.TerminalUnknown, "");

        return _output;
      }


      return Trx_CommonStartCardSession(TransactionId, _terminal, _account, Trx);
    }

    // PURPOSE: 1) Block account.
    //          2) Retrieve the play session (if is necesarry insert new session).
    //          3) Compute Redeemable / NotRedeemable to be trasferred
    //          4) Substract the amount of the account to be transferred
    //          5) Create the StartCardSession movement
    //          6) Set activity
    //
    //  PARAMS:
    //     - INPUT:
    //           - AccountId:
    //           - TerminalId:
    //           - TerminalType:
    //           - TerminalName:
    //           - Trx:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - true if all the process was correct, false otherwise.

    public static StartSessionOutput Trx_CommonStartCardSession(Int64 TransactionId,
                                                                Terminal.TerminalInfo Terminal,
                                                                AccountInfo Account,
                                                                SqlTransaction Trx)
    {
      StartSessionOutput _output;
      Int32 _disable_new_sessions;

      _output = new StartSessionOutput();

      //
      // RCI 16-AUG-2012: Check if the system has disabled new play sessions.
      //
      Int32.TryParse(ReadGeneralParams("Site", "DisableNewSessions", Trx), out _disable_new_sessions);
      if (_disable_new_sessions != 0)
      {
        _output.SetError(MultiPromos.StartSessionStatus.TerminalBlocked, "System has disabled new PlaySessions.");

        return _output;
      }

      //
      // Check Terminal
      //
      if (Terminal == null || Terminal.TerminalId == 0)
      {
        _output.SetError(MultiPromos.StartSessionStatus.TerminalUnknown, "");

        return _output;
      }

      if (Terminal.Blocked || Terminal.Status != TerminalStatus.ACTIVE)
      {
        _output.SetError(MultiPromos.StartSessionStatus.TerminalBlocked, "");

        return _output;
      }

      //
      // Check Account
      //
      if (Account == null || Account.AccountId == 0)
      {
        _output.SetError(MultiPromos.StartSessionStatus.AccountUnknown, "");

        return _output;
      }

      if (Account.Blocked)
      {
        _output.SetError(MultiPromos.StartSessionStatus.AccountBlocked, "");

        return _output;
      }

      if (Account.CurrentPlaySessionId != 0)
      {
        // In Session
        if (Terminal.Type != TerminalTypes.SAS_HOST
            || Account.CurrentTerminalId != Terminal.TerminalId
            || Account.CurrentPlaySessionStatus != PlaySessionStatus.Opened
            || Account.CurrentPlaySessionBalanceMismatch)
        {
          _output.SetError(MultiPromos.StartSessionStatus.AccountInSession, "");

          return _output;
        }
      }

      Trx_OnStartCardSession(Account.AccountId, TransactionId, Terminal.TerminalId, Terminal.Type, Terminal.Name, out _output, Trx);

      // Save the holder name.
      _output.HolderName = Account.HolderName;

      return _output;
    }

    private static Boolean Trx_OnStartCardSession(Int64 AccountId,
                                                 Int64 TransactionId,
                                                 Int32 TerminalId,
                                                 TerminalTypes TerminalType,
                                                 String TerminalName,
                                                 out StartSessionOutput Output,
                                                 SqlTransaction Trx)
    {
      Int64 _play_session_id;
      AccountBalance _ini_balance;
      AccountBalance _fin_balance;
      AccountBalance _playable_balance;
      AccountBalance _total_to_gm_balance;
      AccountBalance _cancel_balance;
      AccountMovementsTable _am_table;
      InSessionAction _action;
      Decimal _ini_points;
      Decimal _fin_points;
      Boolean _is_new_session;
      Int64 _prev_play_session_id;
      AccountBalance _dummy_bal1;
      AccountBalance _dummy_bal2;

      Output = new StartSessionOutput();
      Output.SetError(StartSessionStatus.Error, "");

      try
      {
        // Lock Account
        if (!Trx_UpdateAccountBalance(AccountId, MultiPromos.AccountBalance.Zero, 0, out _ini_balance, out _ini_points, out _prev_play_session_id, out _cancel_balance, Trx))
        {
          Output.SetError(StartSessionStatus.Error, "Trx_UpdateAccountBalance LOCK account failed.");

          return false;
        }
        _is_new_session = (_prev_play_session_id == 0);

        // Get play sessions -- It will insert a new play session if is necessary
        if (!Trx_GetPlaySessionId(AccountId, TerminalId, TerminalType, out _play_session_id, out _action, Trx))
        {
          Output.SetError(StartSessionStatus.Error, "Trx_GetPlaySessionId failed.");

          return false;
        }

        if (_is_new_session)
        {
          Decimal _dummy1;
          Decimal _dummy2;

          if (!Trx_ResetAccountInSession(AccountId, 0, out _dummy1, out _dummy2, out _dummy_bal1, Trx))
          {
            Output.SetError(StartSessionStatus.Error, "Trx_ResetAccountInSession failed.");

            return false;
          }
        }

        // Compute Redeemable / NotRedeemable to be trasferred
        if (!Trx_GetPlayableBalance(AccountId, TerminalId, _play_session_id, TransactionId, _ini_balance, out _playable_balance, Trx))
        {
          Output.SetError(StartSessionStatus.Error, "Trx_GetPlayableBalance failed.");

          return false;
        }

        // Substract the amount to be transferred
        if (!Trx_UpdateAccountBalance(AccountId, AccountBalance.Negate(_playable_balance),
                                      0, out _fin_balance, out _fin_points, out _prev_play_session_id, out _cancel_balance, Trx))
        {
          Output.SetError(StartSessionStatus.Error, "Trx_UpdateAccountBalance failed. PlayableBalance: " + _playable_balance.ToString());

          return false;
        }

        // Create the StartCardSession movement
        _am_table = new AccountMovementsTable(TerminalId, TerminalName, _play_session_id);

        if (!_am_table.Add(0, AccountId, MovementType.StartCardSession,
                           _ini_balance.TotalBalance, _playable_balance.TotalBalance, 0, _fin_balance.TotalBalance))
        {
          Output.SetError(StartSessionStatus.Error, "AccountMovementsTable.Add(StartCardSession) failed.");

          return false;
        }

        if (!_am_table.Save(Trx))
        {
          Output.SetError(StartSessionStatus.Error, "AccountMovementsTable.Save failed.");

          return false;
        }

        // Account has activity
        if (!Trx_SetActivity(AccountId, Trx))
        {
          Output.SetError(StartSessionStatus.Error, "Trx_SetActivity failed.");

          return false;
        }

        if (!Trx_BalanceToPlaySession(_is_new_session, _play_session_id, _playable_balance, Trx))
        {
          Output.SetError(StartSessionStatus.Error, "Trx_BalanceToPlaySession failed: NewSession: " + _is_new_session.ToString() +
                          ", PlaySessionId: " + _play_session_id.ToString() + ", PlayableBalance: " + _playable_balance.ToString());

          return false;
        }

        if (!Trx_UpdateAccountInSession(_action, AccountId, TransactionId, _playable_balance,
                                        out _total_to_gm_balance, out _dummy_bal1, out _dummy_bal2, Trx))
        {
          Output.SetError(StartSessionStatus.Error, "Trx_UpdateAccountInSession failed. PlayableBalance: " + _playable_balance.ToString());

          return false;
        }

        Output.PlaySessionId = _play_session_id;
        Output.IsNewPlaySession = _is_new_session;
        Output.IniBalance = _ini_balance;
        Output.PlayableBalance = _playable_balance;
        Output.TotalToGMBalance = _total_to_gm_balance;
        Output.FinBalance = _fin_balance;
        Output.Points = _fin_points;

        Output.SetSuccess();

        return true;
      }
      catch (Exception _ex)
      {
        Output.SetError(StartSessionStatus.Error, _ex.Message);
      }

      return false;
    } // Trx_OnStartCardSession

    // PURPOSE: Link the account with the play session and the terminal id.
    //
    //  PARAMS:
    //     - INPUT:
    //           - AccountId:
    //           - TerminalId:
    //           - PlaySessionId:
    //           - EnsureNotInSession:
    //           - Trx:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - true if the update was correct , false otherwise.

    public static Boolean Trx_LinkAccountTerminal(Int64 AccountId,
                                                  Int32 TerminalId,
                                                  Int64 PlaySessionId,
                                                  Boolean EnsureNotInSession,
                                                  SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("UPDATE ACCOUNTS SET   AC_CURRENT_TERMINAL_ID     = @pTerminalId ");
        _sb.AppendLine("                    , AC_CURRENT_TERMINAL_NAME   = (SELECT TE_NAME FROM TERMINALS WHERE TE_TERMINAL_ID = @pTerminalId) ");
        _sb.AppendLine("                    , AC_CURRENT_PLAY_SESSION_ID = @pPlaySessionId ");
        _sb.AppendLine("              WHERE   AC_ACCOUNT_ID              = @pAccountId ");

        if (EnsureNotInSession)
        {
          _sb.AppendLine("                AND   AC_CURRENT_TERMINAL_ID     IS NULL ");
          _sb.AppendLine("                AND   AC_CURRENT_TERMINAL_NAME   IS NULL ");
          _sb.AppendLine("                AND   AC_CURRENT_PLAY_SESSION_ID IS NULL ");
        }

        _sb.AppendLine(" ; ");

        _sb.AppendLine("UPDATE TERMINALS SET  TE_CURRENT_ACCOUNT_ID      = @pAccountId ");
        _sb.AppendLine("                    , TE_CURRENT_PLAY_SESSION_ID = @pPlaySessionId ");
        _sb.AppendLine("              WHERE   TE_TERMINAL_ID             = @pTerminalId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
          _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = PlaySessionId;

          // ExecuteNonQuery() returns the number of rows affected. If everything ok, it will be 2.
          return (_cmd.ExecuteNonQuery() == 2);
        }
      }
      catch
      { ; }

      return false;
    } // Trx_LinkAccountTerminal

    // PURPOSE: Unlink the account with the play session and the terminal id.
    //
    //  PARAMS:
    //     - INPUT:
    //           - AccountId:
    //           - TerminalId:
    //           - PlaySessionId:
    //           - Trx:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - true if the update was correct , false otherwise.

    public static Boolean Trx_UnlinkAccountTerminal(Int64 AccountId,
                                                    Int32 TerminalId,
                                                    Int64 PlaySessionId,
                                                    SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" UPDATE ACCOUNTS SET   AC_LAST_TERMINAL_ID        = AC_CURRENT_TERMINAL_ID ");
        _sb.AppendLine("                     , AC_LAST_TERMINAL_NAME      = AC_CURRENT_TERMINAL_NAME ");
        _sb.AppendLine("                     , AC_LAST_PLAY_SESSION_ID    = AC_CURRENT_PLAY_SESSION_ID ");
        _sb.AppendLine("                     , AC_CURRENT_TERMINAL_ID     = NULL ");
        _sb.AppendLine("                     , AC_CURRENT_TERMINAL_NAME   = NULL ");
        _sb.AppendLine("                     , AC_CURRENT_PLAY_SESSION_ID = NULL ");
        _sb.AppendLine("               WHERE   AC_ACCOUNT_ID              = @pAccountId ");
        _sb.AppendLine("                 AND   AC_CURRENT_TERMINAL_ID     = @pTerminalId ");
        _sb.AppendLine("                 AND   AC_CURRENT_PLAY_SESSION_ID = @pPlaySessionId ");

        _sb.AppendLine(" ; ");

        _sb.AppendLine(" UPDATE TERMINALS SET  TE_CURRENT_ACCOUNT_ID      = NULL ");
        _sb.AppendLine("                     , TE_CURRENT_PLAY_SESSION_ID = NULL ");
        _sb.AppendLine("               WHERE   TE_TERMINAL_ID             = @pTerminalId ");
        _sb.AppendLine("                 AND   TE_CURRENT_ACCOUNT_ID      = @pAccountId ");
        _sb.AppendLine("                 AND   TE_CURRENT_PLAY_SESSION_ID = @pPlaySessionId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
          _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = PlaySessionId;

          _cmd.ExecuteNonQuery();
          // It doesn't matter if updated or not.

          return true;
        }
      }
      catch
      { ; }

      return false;
    } // Trx_UnlinkAccountTerminal

    // PURPOSE: Get a new session play session or the current play session of the account.
    //          * Link the play session id with the account if is necessary.
    //
    //  PARAMS:
    //     - INPUT:
    //           - AccountId:
    //           - TerminalId:
    //           - TerminalType:
    //           - PlaySessionId:
    //           - Trx:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - true if all the process was correct, false otherwise.

    public static Boolean Trx_GetPlaySessionId(Int64 AccountId,
                                               Int32 TerminalId,
                                               TerminalTypes TerminalType,
                                               out Int64 PlaySessionId,
                                               out InSessionAction Action,
                                               SqlTransaction Trx)
    {
      StringBuilder _sb;
      Int32 _current_terminal_id;
      Int64 _current_play_session_id;
      AccountType _ac_type;
      PlaySessionStatus _ps_status;
      String _xml_type_data;
      String _card_track_data;
      Boolean _rc;
      Boolean _balance_mismatch;

      PlaySessionId = 0;
      Action = InSessionAction.Unknown;

      _current_terminal_id = 0;
      _current_play_session_id = 0;
      _ac_type = 0;
      _ps_status = 0;
      _xml_type_data = "";

      _sb = new StringBuilder();

      try
      {
        _sb.AppendLine(" SELECT   AC_CURRENT_TERMINAL_ID ");
        _sb.AppendLine("        , AC_CURRENT_PLAY_SESSION_ID ");
        _sb.AppendLine("        , AC_TYPE ");
        _sb.AppendLine("        , PS_STATUS ");
        _sb.AppendLine("        , AC_TRACK_DATA ");
        _sb.AppendLine("        , PS_BALANCE_MISMATCH ");
        _sb.AppendLine("   FROM   ACCOUNTS LEFT JOIN PLAY_SESSIONS ON AC_CURRENT_PLAY_SESSION_ID = PS_PLAY_SESSION_ID ");
        _sb.AppendLine("  WHERE   AC_ACCOUNT_ID = @pAccountId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.Int).Value = AccountId;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              return false;
            }

            _current_terminal_id = _reader.IsDBNull(0) ? 0 : (Int32)_reader.GetInt32(0);
            _current_play_session_id = _reader.IsDBNull(1) ? 0 : (Int64)_reader.GetInt64(1);
            _ac_type = _reader.IsDBNull(2) ? AccountType.ACCOUNT_UNKNOWN : (AccountType)_reader.GetInt32(2);
            _ps_status = _reader.IsDBNull(3) ? PlaySessionStatus.Closed : (PlaySessionStatus)_reader.GetInt32(3);

            if (!_reader.IsDBNull(4))
            {
              _rc = CardNumber.CardNumberToTrackData(out _card_track_data, _reader.GetString(4), 0);
              if (_rc)
              {
                _xml_type_data = _card_track_data;
              }
            }
            _balance_mismatch = _reader.IsDBNull(5) ? false : _reader.GetBoolean(5);
          }
        }

        // If doesn't have current play session in the account, it insert a new play session
        if (_current_play_session_id == 0)
        {
          if (!Trx_InsertPlaySession(AccountId, _ac_type, _xml_type_data, TerminalId, TerminalType, out _current_play_session_id, Trx))
          {
            return false;
          }

          if (!Trx_LinkAccountTerminal(AccountId, TerminalId, _current_play_session_id, true, Trx))
          {
            return false;
          }

          Action = InSessionAction.StartSession;
          PlaySessionId = _current_play_session_id;

          return true;
        }

        // In Session
        if (TerminalId != _current_terminal_id
            || _ps_status != PlaySessionStatus.Opened
            || TerminalType != TerminalTypes.SAS_HOST
            || _balance_mismatch)
        {
          return false;
        }

        // This is a SAS-HOST 'asking more money'
        Action = InSessionAction.ReStartSession;
        PlaySessionId = _current_play_session_id;

        return true;
      }
      catch
      { ; }

      return false;

    } // Trx_GetPlaySessionId

    // PURPOSE: 1) Update state of all the sessions of the terminal to abandoned.
    //          2) Insert the new play session an return the id.
    //
    //  PARAMS:
    //     - INPUT:
    //           - AccountId:
    //           - AccountType:
    //           - TypeData:
    //           - TerminalId:
    //           - PlaySessionId:
    //           - Trx:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - true if all updated was successfully , false otherwise.

    public static Boolean Trx_InsertPlaySession(Int64 AccountId,
                                                AccountType AccountType,
                                                String TypeData,
                                                Int32 TerminalId,
                                                TerminalTypes TerminalType,
                                                out Int64 PlaySessionId,
                                                SqlTransaction Trx)
    {
      PlaySessionType _ps_type;
      StringBuilder _sb;
      SqlParameter _param_play_sesion_id;

      PlaySessionId = 0;

      _sb = new StringBuilder();
      _ps_type = PlaySessionType.NONE;

      switch (AccountType)
      {
        case AccountType.ACCOUNT_CADILLAC_JACK:
          _ps_type = PlaySessionType.CADILLAC_JACK;
          break;
        case AccountType.ACCOUNT_WIN:
          _ps_type = PlaySessionType.WIN;
          break;
        case AccountType.ACCOUNT_ALESIS:
          _ps_type = PlaySessionType.ALESIS;
          break;
        default:
          _ps_type = PlaySessionType.NONE;
          break;
      }

      try
      {
        _sb.Length = 0;
        _sb.AppendLine(" DECLARE @ActivityDueToCardIn AS INTEGER ");
        _sb.AppendLine(" ");
        _sb.AppendLine(" SELECT   @ActivityDueToCardIn = CAST (GP_KEY_VALUE AS INT) ");
        _sb.AppendLine("   FROM   GENERAL_PARAMS                                    ");
        _sb.AppendLine("  WHERE   GP_GROUP_KEY   = 'Play'                           ");
        _sb.AppendLine("    AND   GP_SUBJECT_KEY = 'ActivityDueToCardIn'            ");
        _sb.AppendLine(" ");
        _sb.AppendLine(" IF ( @ActivityDueToCardIn IS NULL ) ");
        _sb.AppendLine(" BEGIN                               ");
        _sb.AppendLine("   SET @ActivityDueToCardIn = 0      ");
        _sb.AppendLine(" END                                 ");
        _sb.AppendLine(" ");
        _sb.AppendLine(" INSERT INTO PLAY_SESSIONS (PS_ACCOUNT_ID       ");
        _sb.AppendLine("                          , PS_TERMINAL_ID      ");
        _sb.AppendLine("                          , PS_TYPE             ");
        _sb.AppendLine("                          , PS_TYPE_DATA        ");
        _sb.AppendLine("                          , PS_INITIAL_BALANCE  ");
        _sb.AppendLine("                          , PS_FINAL_BALANCE    ");
        _sb.AppendLine("                          , PS_FINISHED         ");
        _sb.AppendLine("                          , PS_STAND_ALONE     )");
        _sb.AppendLine("                    VALUES (@pAccountId         ");
        _sb.AppendLine("                          , @pTerminalId        ");
        _sb.AppendLine("                          , @pTypeWin           ");
        _sb.AppendLine("                          , @pTypeData          ");
        _sb.AppendLine("                          , @pDefaultBalance    ");
        _sb.AppendLine("                          , @pDefaultBalance    ");
        _sb.AppendLine("                          , CASE WHEN (@ActivityDueToCardIn = 1) THEN GETDATE() ELSE NULL END ");
        _sb.AppendLine("                          , @pStandAlone        )");
        _sb.AppendLine(" ");
        _sb.AppendLine(" SET @pPlaySessionId = SCOPE_IDENTITY() ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _cmd.Parameters.Add("@pTerminalId ", SqlDbType.Int).Value = TerminalId;
          _cmd.Parameters.Add("@pTypeWin", SqlDbType.Int).Value = _ps_type;
          _cmd.Parameters.Add("@pDefaultBalance", SqlDbType.Money).Value = 0;
          _cmd.Parameters.Add("@pTypeData", SqlDbType.Xml).Value = TypeData;
          _cmd.Parameters.Add("@pStandAlone", SqlDbType.Bit).Value = (TerminalType == TerminalTypes.SAS_HOST);
          _param_play_sesion_id = _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt);
          _param_play_sesion_id.Direction = ParameterDirection.Output;

          if (_cmd.ExecuteNonQuery() != 1)
          {
            return false;
          }

          if (_param_play_sesion_id.Value == DBNull.Value)
          {
            return false;
          }

          PlaySessionId = (Int64)_param_play_sesion_id.Value;

        }

        return true;

      }
      catch
      { ; }

      return false;

    } // Trx_InsertPlaySession


    public static Boolean Trx_HandpayInsertPlaySession(HANDPAY_TYPE Type,
                                                       Int64 AccountId,
                                                       Int32 TerminalId,
                                                       TerminalTypes TerminalType,
                                                       AccountBalance PlaySessionInitialBalance,
                                                       AccountBalance HandpayAmount,
                                                       out Int64 PlaySessionId,
                                                       SqlTransaction Trx)
    {
      StringBuilder _sb;
      SqlParameter _param_play_session;
      PlaySessionType _ps_type;
      PlaySessionStatus _ps_status;
      Decimal _won_amount;

      PlaySessionId = -1;
      _sb = new StringBuilder();

      if (HandpayAmount.TotalBalance > 0)
      {
        _ps_status = PlaySessionStatus.HandpayPayment;
      }
      else
      {
        _ps_status = PlaySessionStatus.HandpayCancelPayment;
      }


      _won_amount = 0;
      switch (Type)
      {
        case HANDPAY_TYPE.CANCELLED_CREDITS:
          _ps_type = PlaySessionType.HANDPAY_CANCELLED_CREDITS;
          break;

        case HANDPAY_TYPE.JACKPOT:
          _ps_type = PlaySessionType.HANDPAY_JACKPOT;
          _won_amount = HandpayAmount.TotalBalance;
          break;

        case HANDPAY_TYPE.ORPHAN_CREDITS:
          _ps_type = PlaySessionType.HANDPAY_ORPHAN_CREDITS;
          break;

        case HANDPAY_TYPE.MANUAL:
          _ps_type = PlaySessionType.HANDPAY_MANUAL;
          break;

        case HANDPAY_TYPE.SITE_JACKPOT:
          _ps_type = PlaySessionType.HANDPAY_SITE_JACKPOT;
          _won_amount = HandpayAmount.TotalBalance;
          break;

        case HANDPAY_TYPE.UNKNOWN:
        default:
          return false;
      }

      try
      {
        _sb.AppendLine("INSERT INTO PLAY_SESSIONS ( PS_ACCOUNT_ID ");
        _sb.AppendLine("                          , PS_TERMINAL_ID ");
        _sb.AppendLine("                          , PS_TYPE ");
        _sb.AppendLine("                          , PS_INITIAL_BALANCE ");
        _sb.AppendLine("                          , PS_FINAL_BALANCE ");
        _sb.AppendLine("                          , PS_PLAYED_COUNT ");
        _sb.AppendLine("                          , PS_PLAYED_AMOUNT ");
        _sb.AppendLine("                          , PS_WON_COUNT ");
        _sb.AppendLine("                          , PS_WON_AMOUNT ");
        _sb.AppendLine("                          , PS_CASH_IN ");
        _sb.AppendLine("                          , PS_CASH_OUT ");
        _sb.AppendLine("                          , PS_STATUS ");
        _sb.AppendLine("                          , PS_STARTED ");
        _sb.AppendLine("                          , PS_FINISHED ");
        _sb.AppendLine("                          , PS_STAND_ALONE ");
        _sb.AppendLine("                          , PS_PROMO ");
        _sb.AppendLine("                          ) ");
        _sb.AppendLine("                   VALUES ( @pAccountId ");      // PS_ACCOUNT_ID
        _sb.AppendLine("                          , @pTerminalId ");     // PS_TERMINAL_ID
        _sb.AppendLine("                          , @pType ");           // PS_TYPE
        _sb.AppendLine("                          , @pInitialBalance "); // PS_INITIAL_BALANCE
        _sb.AppendLine("                          , @pFinalBalance ");   // PS_FINAL_BALANCE
        _sb.AppendLine("                          , 0 ");                // PS_PLAYED_COUNT
        _sb.AppendLine("                          , 0 ");                // PS_PLAYED_AMOUNT
        _sb.AppendLine("                          , 0 ");                // PS_WON_COUNT
        _sb.AppendLine("                          , @pWonAmount ");      // PS_WON_AMOUNT
        _sb.AppendLine("                          , 0 ");                // PS_CASH_IN
        _sb.AppendLine("                          , 0 ");                // PS_CASH_OUT
        _sb.AppendLine("                          , @pStatus ");   // PS_STATUS
        _sb.AppendLine("                          , GETDATE() ");        // PS_STARTED
        _sb.AppendLine("                          , GETDATE() ");        // PS_FINISHED
        _sb.AppendLine("                          , @pStandAlone ");     // PS_STAND_ALONE
        _sb.AppendLine("                          , 0 ");                // PS_PROMO
        _sb.AppendLine("                          ) ");
        _sb.AppendLine("                           SET @pPlaySessionId = SCOPE_IDENTITY() ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
          _cmd.Parameters.Add("@pType", SqlDbType.Int).Value = _ps_type;
          _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = _ps_status;
          _cmd.Parameters.Add("@pInitialBalance", SqlDbType.Money).Value = (Decimal)PlaySessionInitialBalance.TotalBalance;
          _cmd.Parameters.Add("@pWonAmount", SqlDbType.Money).Value = _won_amount;
          _cmd.Parameters.Add("@pFinalBalance", SqlDbType.Money).Value = (Decimal)(PlaySessionInitialBalance.TotalBalance + HandpayAmount.TotalBalance);
          _cmd.Parameters.Add("@pStandAlone", SqlDbType.Bit).Value = (TerminalType == TerminalTypes.SAS_HOST);
          _param_play_session = _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt);
          _param_play_session.Direction = ParameterDirection.Output;

          if (_cmd.ExecuteNonQuery() != 1)
          {
            return false;
          }

          PlaySessionId = (Int64)_param_play_session.Value;

        }

        return true;

      }
      catch
      {
        return false;
      }

    } // Trx_HandpayInsertPlaySession

    //------------------------------------------------------------------------------
    // PURPOSE : Update Card Session Counters received to the DB.
    //
    //  PARAMS :
    //      - INPUT :
    //          - CardSessionId
    //          - PlayedCount
    //          - PlayedAmount
    //          - WonCount
    //          - WonAmount
    //          - JackpotAmount
    //          - SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - true: Card session counters updated.
    //      - false: error.
    //
    public static Boolean Trx_UpdatePlaySessionPlayedWon(TerminalTypes TerminalType, Int64 PlaySessionId, PlayedWonMeters PlaySessionMeters,
                                                         out PlayedWonMeters Delta,
                                                         SqlTransaction Trx)
    {
      StringBuilder _sb;

      Delta = new PlayedWonMeters();

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("UPDATE   PLAY_SESSIONS ");

        if (TerminalType == TerminalTypes.T3GS)
        {
          _sb.AppendLine("   SET   PS_PLAYED_COUNT    = @pPlayedCount   ");
          _sb.AppendLine("       , PS_PLAYED_AMOUNT   = @pPlayedAmount  ");
          _sb.AppendLine("       , PS_WON_COUNT       = @pWonCount      ");
          _sb.AppendLine("       , PS_WON_AMOUNT      = @pWonAmount     ");
        }
        else
        {
          _sb.AppendLine("   SET   PS_PLAYED_COUNT    = CASE WHEN (@pPlayedCount  > PS_PLAYED_COUNT)  THEN @pPlayedCount  ELSE PS_PLAYED_COUNT  END ");
          _sb.AppendLine("       , PS_PLAYED_AMOUNT   = CASE WHEN (@pPlayedAmount > PS_PLAYED_AMOUNT) THEN @pPlayedAmount ELSE PS_PLAYED_AMOUNT END ");
          _sb.AppendLine("       , PS_WON_COUNT       = CASE WHEN (@pWonCount     > PS_WON_COUNT)     THEN @pWonCount     ELSE PS_WON_COUNT     END ");
          _sb.AppendLine("       , PS_WON_AMOUNT      = CASE WHEN (@pWonAmount    > PS_WON_AMOUNT)    THEN @pWonAmount    ELSE PS_WON_AMOUNT    END ");
        }

        _sb.AppendLine("       , PS_LOCKED          = NULL ");
        _sb.AppendLine("       , PS_FINISHED        = GETDATE() ");
        _sb.AppendLine("OUTPUT   INSERTED.PS_PLAYED_COUNT  - DELETED.PS_PLAYED_COUNT  ");
        _sb.AppendLine("       , INSERTED.PS_PLAYED_AMOUNT - DELETED.PS_PLAYED_AMOUNT ");
        _sb.AppendLine("       , INSERTED.PS_WON_COUNT     - DELETED.PS_WON_COUNT     ");
        _sb.AppendLine("       , INSERTED.PS_WON_AMOUNT    - DELETED.PS_WON_AMOUNT    ");

        _sb.AppendLine(" WHERE   PS_PLAY_SESSION_ID = @pPlaySessionId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = PlaySessionId;
          _cmd.Parameters.Add("@pPlayedCount", SqlDbType.BigInt).Value = Math.Max(0, PlaySessionMeters.PlayedCount);
          _cmd.Parameters.Add("@pPlayedAmount", SqlDbType.Money).Value = Math.Max(0, PlaySessionMeters.PlayedAmount);
          _cmd.Parameters.Add("@pWonCount", SqlDbType.BigInt).Value = Math.Max(0, PlaySessionMeters.WonCount);
          _cmd.Parameters.Add("@pWonAmount", SqlDbType.Money).Value = Math.Max(0, PlaySessionMeters.WonAmount);

          // AJQ & ACC & XI 25-AUG-2010
          // JackpotAmount will be added when the handpay is paid.

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              return false;
            }
            Delta.PlayedCount = _reader.GetInt32(0);
            Delta.PlayedAmount = _reader.GetDecimal(1);
            Delta.WonCount = _reader.GetInt32(2);
            Delta.WonAmount = _reader.GetDecimal(3);
            Delta.JackpotAmount = 0;
          }

          return true;
        }
      }
      catch
      {
      }

      return false;
    } // Trx_UpdatePlaySessionPlayedWon

    public static Boolean Trx_OnEndCardSession(Int32 TerminalId,
                                               TerminalTypes TerminalType,
                                               String TerminalName,
                                               MultiPromos.EndSessionInput Input,
                                               out MultiPromos.EndSessionOutput Output,
                                               SqlTransaction Trx)
    {
      AccountBalance _balance_from_gm_calculated;
      AccountBalance _balance_from_gm_to_add;
      Boolean _big_mismatch;
      Boolean _mismatch;
      PlaySessionCloseParameters _psc_params;
      Decimal _balance_from_gm;
      InSessionParameters _params;
      InSessionParameters _final_params;

      Output = new EndSessionOutput();
      Output.SetError(EndSessionStatus.ErrorRetry, "");

      try
      {
        if (Input.PlaySessionId <= 0 || Input.TransactionId < 0)
        {
          Output.SetError(EndSessionStatus.FaltalError, "Unexpected PlaySessionId/TransactionId: " + Input.PlaySessionId.ToString() + ", " + Input.TransactionId.ToString());

          return false;
        }

        _balance_from_gm = Math.Max(0, Input.BalanceFromGM);
        if (_balance_from_gm != Input.BalanceFromGM)
        {
          Output.Warning("BalanceFromGM changed from " + Input.BalanceFromGM.ToString() + ", to " + _balance_from_gm.ToString());
        }

        switch (TerminalType)
        {
          case TerminalTypes.WIN:       // LKT
            break;

          case TerminalTypes.SAS_HOST:  // SAS-HOST
            if (Input.TransactionId == 0 || !Input.HasMeters)
            {
              if (Input.MovementType != MovementType.ManualEndSession)
              {
                Output.Warning("Session Meters not received. TransactionId: " + Input.TransactionId.ToString() + ", HasCounters: " + Input.HasMeters.ToString());
              }
            }
            break;

          case TerminalTypes.T3GS:      // 3GS
            break;

          default:
            Output.SetError(EndSessionStatus.FaltalError, "Unexpected TerminalType: " + TerminalType.ToString());

            return false;
        }


        if (Input.MovementType == MovementType.EndCardSession)
        {
          PlayedWonMeters _dummy;

          if (!Trx_UpdatePlaySessionPlayedWon(TerminalType, Input.PlaySessionId, Input.Meters, out _dummy, Trx))
          {
            Output.SetError(EndSessionStatus.ErrorRetry, "Trx_UpdatePlaySessionPlayedWon failed.");

            return false;
          }
        }

        Trx.Save("CalculatePlayedAndWon");

        if (!Trx_GetDeltaPlayedWon(Input.PlaySessionId, out _params, Trx))
        {
          Output.SetError(EndSessionStatus.ErrorRetry, "Trx_GetDeltaPlayedWon failed.");

          return false;
        }
        if (!Trx_UpdatePlayedAndWon(_params, Output.ErrorMsg, Trx))
        {
          Output.SetError(EndSessionStatus.ErrorRetry, "Trx_UpdatePlayedAndWon failed.");

          return false;
        }

        // Played & Won taken from here, the mismatch is not taken into account to award points.
        _final_params = _params;

        _balance_from_gm_calculated = new AccountBalance(_params.Balance);
        _balance_from_gm_to_add = new AccountBalance(_balance_from_gm_calculated);

        switch (TerminalType)
        {
          case TerminalTypes.WIN:       // LKT
            _balance_from_gm = _balance_from_gm_calculated.TotalBalance;  // Always calculated
            break;

          case TerminalTypes.SAS_HOST:  // SAS-HOST
            break;

          case TerminalTypes.T3GS:      // 3GS
            if (Input.MovementType == MovementType.ManualEndSession)
            {
              if (!Input.FinalBalanceSelectedByUserAfterMismatch)
              {
                _balance_from_gm = _balance_from_gm_calculated.TotalBalance;
              }
            }
            break;

          default:
            Output.SetError(EndSessionStatus.FaltalError, "Unexpected TerminalType: " + TerminalType.ToString());

            return false;
        }

        _mismatch = (_balance_from_gm_calculated.TotalBalance != _balance_from_gm);
        _big_mismatch = _mismatch;

        // Check Balance Mismatch
        if (_mismatch)
        {
          Decimal _diff;
          Decimal _maximum_balance_error;

          _balance_from_gm_to_add = new AccountBalance(_balance_from_gm_calculated);
          _diff = _balance_from_gm - _balance_from_gm_calculated.TotalBalance;

          _big_mismatch = false;

          // if BIG return
          if (TerminalType == TerminalTypes.SAS_HOST || TerminalType == TerminalTypes.WIN)
          {
            Decimal.TryParse(ReadGeneralParams("SasHost", "MaximumBalanceErrorCents", Trx), out _maximum_balance_error);
            _maximum_balance_error = _maximum_balance_error / 100;

            if (_diff > _maximum_balance_error)
            {
              _big_mismatch = true;
            }
          }

          if (TerminalType == TerminalTypes.T3GS)
          {
            Decimal.TryParse(ReadGeneralParams("Interface3GS", "MaximumBalanceErrorCents", Trx), out _maximum_balance_error);
            _maximum_balance_error = _maximum_balance_error / 100;

            if (Math.Abs(_diff) > _maximum_balance_error)
            {
              _big_mismatch = true;
            }
          }

          // TODO: Simulate Played/Won 
          InSessionParameters _tmp_params;

          _tmp_params = new InSessionParameters();

          _tmp_params.PlaySessionId = Input.PlaySessionId;
          _tmp_params.AccountId = Input.AccountId;
          _tmp_params.Balance = new AccountBalance(_balance_from_gm_calculated);

          if (_diff > 0)
          {
            _tmp_params.DeltaPlayed = 0;
            _tmp_params.DeltaWon = _diff;
          }
          else
          {
            _tmp_params.DeltaPlayed = Math.Abs(_diff);
            _tmp_params.DeltaWon = 0;
          }

          if (!Trx_UpdatePlayedAndWon(_tmp_params, Output.ErrorMsg, Trx))
          {
            Output.SetError(EndSessionStatus.ErrorRetry, "Trx_UpdatePlayedAndWon failed.");

            return false;
          }

          MultiPromos.AccountBalance _reported_balance;

          _reported_balance = _tmp_params.Balance;

          // Set final = reported
          _balance_from_gm_to_add = new AccountBalance(_reported_balance);

          if (Input.MovementType == MovementType.ManualEndSession)
          {
            _mismatch = false;
            _big_mismatch = false;
          }

          if (_big_mismatch)
          {
            // Set final = Min(reported, calculated)
            if (_reported_balance.TotalBalance > _balance_from_gm_calculated.TotalBalance)
            {
              _balance_from_gm_to_add = new AccountBalance(_balance_from_gm_calculated);
            }

            Trx.Rollback("CalculatePlayedAndWon");
          }
        }

        // Set Mismatch
        if (!Trx_PlaySessionSetFinalBalance(Input.PlaySessionId, _balance_from_gm_to_add.TotalBalance, _balance_from_gm,
                                            _mismatch, Input.MovementType, Trx))
        {
          Output.SetError(EndSessionStatus.ErrorRetry, "Trx_PlaySessionSetFinalBalance failed.");

          return false;
        }

        if (_big_mismatch)
        {
          Output.SetError(EndSessionStatus.BalanceMismatch,
                          String.Format("Balance Mismatch: Reported={0}, Calculated={1}, Final={2}",
                          Input.BalanceFromGM, _balance_from_gm_calculated.ToString(), _balance_from_gm_to_add.ToString()));

          Output.PlaySessionFinalBalance = new AccountBalance(_balance_from_gm_to_add);

          return true;
        }

        // Close PlaySession
        _psc_params = new PlaySessionCloseParameters();
        _psc_params.MovementType = Input.MovementType;
        _psc_params.CashierName = Input.CashierName;
        _psc_params.PlaySessionId = Input.PlaySessionId;
        _psc_params.AccountId = Input.AccountId;
        _psc_params.TerminalId = TerminalId;
        _psc_params.TerminalName = TerminalName;

        _psc_params.BalanceMismatch = _mismatch;
        _psc_params.BalanceFromGMToAdd = new AccountBalance(_balance_from_gm_to_add);
        _psc_params.ErrorMsg = Output.ErrorMsg;

        _psc_params.NumPlayed = Input.Meters.PlayedCount;
        _psc_params.Played = _final_params.Played;
        _psc_params.NumWon = Input.Meters.WonCount;
        _psc_params.Won = _final_params.Won;

        if (!Trx_PlaySessionClose(_psc_params, Output, Trx))
        {
          Output.SetError(EndSessionStatus.ErrorRetry, "Trx_PlaySessionClose failed.");

          return false;
        }

        Output.PlaySessionFinalBalance = new AccountBalance(_balance_from_gm_to_add);
        Output.SetSuccess();

        return true;
      }
      catch (Exception _ex)
      {
        Output.SetError(EndSessionStatus.ErrorRetry, _ex.Message);
      }

      return false;
    } // Trx_OnEndCardSession

    // PURPOSE: Close PlaySession
    //
    //  PARAMS:
    //     - INPUT:
    //           - Params:
    //           - Trx:
    //
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - true if was successfully , false otherwise.
    //
    private static Boolean Trx_PlaySessionClose(PlaySessionCloseParameters Params, EndSessionOutput Output, SqlTransaction Trx)
    {
      AccountBalance _ini_balance;
      AccountBalance _fin_balance;

      AccountBalance _to_gm;
      AccountBalance _from_gm;
      AccountBalance _ps_balance;

      AccountMovementsTable _am_table;
      PlaySessionStatus _desired_status;
      PlaySessionStatus _old_status;
      PlaySessionStatus _new_status;
      Int32 _terminal_id;
      Boolean _points_awarded;
      Decimal _won_points;

      Terminal.TerminalInfo _terminal_info;

      if (!Terminal.Trx_GetTerminalInfo(Params.TerminalId, out _terminal_info, Trx))
      {
        Output.SetError(EndSessionStatus.ErrorRetry, "Trx_GetTerminalInfo failed.");

        return false;
      }

      if (!Trx_UpdateAccountBalance(Params.AccountId, Params.BalanceFromGMToAdd, out _fin_balance, Trx))
      {
        Output.SetError(EndSessionStatus.ErrorRetry, "Trx_UpdateAccountBalance failed.");

        return false;
      }
      _ini_balance = _fin_balance - Params.BalanceFromGMToAdd;

      // Account has activity
      if (!Trx_SetActivity(Params.AccountId, Trx))
      {
        Output.SetError(EndSessionStatus.ErrorRetry, "Trx_SetActivity failed.");

        return false;
      }

      if (!Trx_UpdateAccountInSession(InSessionAction.EndSession, Params.AccountId, 0, Params.BalanceFromGMToAdd,
                                      out _to_gm, out _from_gm, out _ps_balance, Trx))
      {
        Output.SetError(EndSessionStatus.ErrorRetry, "Trx_UpdateAccountInSession failed.");

        return false;
      }

      if (!Trx_UnlinkPromotionsInSession(Params.AccountId, Params.PlaySessionId, 0, Trx))
      {
        Output.SetError(EndSessionStatus.ErrorRetry, "Trx_UnlinkPromotionsInSession failed.");

        return false;
      }

      switch (Params.MovementType)
      {
        case MovementType.EndCardSession:
          _desired_status = PlaySessionStatus.Closed;
          _terminal_id = Params.TerminalId;
          break;

        case MovementType.ManualEndSession:
          _desired_status = PlaySessionStatus.ManualClosed;
          // For ManualEndSession, _terminal_id = 0 indicates that, for AccountMovements,
          // the important thing is CashierName not TerminalName.
          _terminal_id = 0;
          break;

        default:
          Output.SetError(EndSessionStatus.FaltalError, "Unexpected MovementType: " + Params.MovementType.ToString());

          return false;
      }


      //
      // PsIniBalance
      // PsFinBalance
      // PsDiffBalance --> Agotado
      // PsPlayed
      // PsWon
      if (!Trx_PlaySessionSetMeters(Params.PlaySessionId, _to_gm, Params.Played, Params.Won, _from_gm, Trx))
      {
        Output.SetError(EndSessionStatus.ErrorRetry, "Trx_PlaySessionSetMeters failed.");

        return false;
      }

      if (!Trx_PlaySessionChangeStatus(_desired_status, Params.PlaySessionId, out _old_status, out _new_status, Trx))
      {
        Output.SetError(EndSessionStatus.ErrorRetry, "Trx_PlaySessionChangeStatus failed.");

        return false;
      }

      if (!Trx_UnlinkAccountTerminal(Params.AccountId, Params.TerminalId, Params.PlaySessionId, Trx))
      {
        Output.SetError(EndSessionStatus.ErrorRetry, "Trx_UnlinkAccountTerminal failed.");

        return false;
      }

      _points_awarded = false;
      _won_points = 0;

      if (Params.Played.TotalBalance > 0)
      {
        if (!Trx_ResetCancellableOperation(Params.AccountId, Trx))
        {
          Output.SetError(EndSessionStatus.ErrorRetry, "Trx_ResetCancellableOperation failed.");

          return false;
        }

        if (!Trx_ComputeWonPoints(Params.AccountId, _terminal_info,
                                  Params.Played.TotalRedeemable - Params.Won.TotalRedeemable,
                                  Params.Played.TotalRedeemable,
                                  Params.Played.TotalBalance,
                                  out _won_points, out _points_awarded, Trx))
        {
          Output.SetError(EndSessionStatus.ErrorRetry, "Trx_ComputeAwardPoints failed.");

          return false;
        }

        if (!Trx_SiteJackpotContribution(_terminal_info, Params.Played, Trx))
        {
          Output.SetError(EndSessionStatus.ErrorRetry, "Trx_SiteJackpotContribution failed.");

          return false;
        }

        if (Params.NumWon > Params.NumPlayed || Params.Played.TotalBalance > 1000000 || _won_points > 1000)
        {
          // Warning! 
          Output.Warning(String.Format("Played: {0}, {1}; Won: {2}, {3}; PointsAwarded: {4}",
                                        Params.NumPlayed.ToString(), Params.Played.ToString(),
                                        Params.NumWon.ToString(), Params.Won.ToString(),
                                        _won_points.ToString()));
        }

        if (Params.MovementType == MovementType.EndCardSession && !Params.BalanceMismatch)
        {
          if (!MultiPromos.Trx_PromoMarkAsExhausted(Params.AccountId, Trx))
          {
            Output.SetError(EndSessionStatus.ErrorRetry, "Trx_PromoMarkAsExhausted failed.");

            return false;
          }
        }
      }

      // Create the StartCardSession movement
      _am_table = new AccountMovementsTable(_terminal_id, Params.TerminalName, Params.PlaySessionId, Params.CashierName);

      if (!_am_table.Add(0, Params.AccountId, Params.MovementType,
                          _ini_balance.TotalBalance, 0, Params.BalanceFromGMToAdd.TotalBalance, _fin_balance.TotalBalance))
      {
        Output.SetError(EndSessionStatus.ErrorRetry, "AccountMovementsTable.Add(" + Params.MovementType.ToString() + ") failed.");

        return false;
      }

      if (_points_awarded)
      {
        Decimal _fin_points;

        if (!Trx_UpdateAccountPoints(Params.AccountId, _won_points, out _fin_points, Trx))
        {
          Output.SetError(EndSessionStatus.ErrorRetry, "Trx_UpdateAccountPoints failed.");

          return false;
        }

        if (!_am_table.Add(0, Params.AccountId, MovementType.PointsAwarded, _fin_points - _won_points, 0, _won_points, _fin_points))
        {
          Output.SetError(EndSessionStatus.ErrorRetry, "AccountMovementsTable.Add(" + MovementType.PointsAwarded.ToString() + ") failed.");

          return false;
        }
      }

      if (!_am_table.Save(Trx))
      {
        Output.SetError(EndSessionStatus.ErrorRetry, "AccountMovementsTable.Save failed.");

        return false;
      }

      return true;
    }

    private static bool Trx_SiteJackpotContribution(Terminal.TerminalInfo TerminalInfo, AccountBalance Played, SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {
        if (!TerminalInfo.PvSiteJackpot)
        {
          return true;
        }

        //
        // Update Site Jackpot Played amounts
        // 
        _sb = new StringBuilder();
        _sb.AppendLine("  UPDATE SITE_JACKPOT_PARAMETERS");
        _sb.AppendLine("     SET SJP_PLAYED  = SJP_PLAYED + CASE WHEN (SJP_ONLY_REDEEMABLE = 1) THEN @pTotalRedeemablePlayed ELSE @pTotalPlayed END");
        _sb.AppendLine("   WHERE SJP_ENABLED = 1");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pTotalRedeemablePlayed", SqlDbType.Money).Value = Played.TotalRedeemable;
          _cmd.Parameters.Add("@pTotalPlayed", SqlDbType.Money).Value = Played.TotalBalance;

          _cmd.ExecuteNonQuery();
        }

        return true;
      }
      catch
      { ; }

      return false;
    }

    public static Boolean Trx_PlaySessionSetMeters(Int64 PlaySessionId,
                                                   AccountBalance CashIn, AccountBalance Played, AccountBalance Won, AccountBalance CashOut,
                                                   SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.Length = 0;

        // Update PlaySession table (Redeemable & NonRedeemable amounts)

        _sb.AppendLine(" UPDATE   PLAY_SESSIONS ");
        // CASH IN 
        _sb.AppendLine("    SET   PS_NON_REDEEMABLE_CASH_IN    = @pNonRedeemableCashIn  ");
        _sb.AppendLine("        , PS_REDEEMABLE_CASH_IN        = @pTotalReCashIn ");
        _sb.AppendLine("        , PS_RE_CASH_IN                = @pReCashIn ");
        _sb.AppendLine("        , PS_PROMO_RE_CASH_IN          = @pPromoReCashIn ");
        // CASH OUT 
        _sb.AppendLine("        , PS_NON_REDEEMABLE_CASH_OUT   = @pNonRedeemableCashOut ");
        _sb.AppendLine("        , PS_REDEEMABLE_CASH_OUT       = @pTotalReCashOut ");
        _sb.AppendLine("        , PS_RE_CASH_OUT               = @pReCashOut ");
        _sb.AppendLine("        , PS_PROMO_RE_CASH_OUT         = @pPromoReCashOut ");
        // PLAYED
        _sb.AppendLine("        , PS_NON_REDEEMABLE_PLAYED     = @pNonRedeemablePlayed ");
        _sb.AppendLine("        , PS_REDEEMABLE_PLAYED         = @pRedeemablePlayed ");
        _sb.AppendLine("        , PS_PLAYED_AMOUNT             = @pTotalPlayed ");
        // WON
        _sb.AppendLine("        , PS_NON_REDEEMABLE_WON        = @pNonRedeemableWon ");
        _sb.AppendLine("        , PS_REDEEMABLE_WON            = @pRedeemableWon ");
        _sb.AppendLine("        , PS_WON_AMOUNT                = @pTotalWon ");
        //
        _sb.AppendLine("  WHERE   PS_PLAY_SESSION_ID           = @pPlaySessionId  ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          // CASH IN
          _cmd.Parameters.Add("@pNonRedeemableCashIn", SqlDbType.Money).Value = CashIn.TotalNotRedeemable;
          _cmd.Parameters.Add("@pTotalReCashIn", SqlDbType.Money).Value = CashIn.TotalRedeemable;
          _cmd.Parameters.Add("@pReCashIn", SqlDbType.Money).Value = CashIn.Redeemable;
          _cmd.Parameters.Add("@pPromoReCashIn", SqlDbType.Money).Value = CashIn.PromoRedeemable;
          // CASH OUT
          _cmd.Parameters.Add("@pNonRedeemableCashOut", SqlDbType.Money).Value = CashOut.TotalNotRedeemable;
          _cmd.Parameters.Add("@pTotalReCashOut", SqlDbType.Money).Value = CashOut.TotalRedeemable;
          _cmd.Parameters.Add("@pReCashOut", SqlDbType.Money).Value = CashOut.Redeemable;
          _cmd.Parameters.Add("@pPromoReCashOut", SqlDbType.Money).Value = CashOut.PromoNotRedeemable;
          // PLAYED
          _cmd.Parameters.Add("@pNonRedeemablePlayed", SqlDbType.Money).Value = Played.TotalNotRedeemable;
          _cmd.Parameters.Add("@pRedeemablePlayed", SqlDbType.Money).Value = Played.TotalRedeemable;
          _cmd.Parameters.Add("@pTotalPlayed", SqlDbType.Money).Value = Played.TotalBalance;
          // WON
          _cmd.Parameters.Add("@pNonRedeemableWon", SqlDbType.Money).Value = Won.TotalNotRedeemable;
          _cmd.Parameters.Add("@pRedeemableWon", SqlDbType.Money).Value = Won.TotalRedeemable;
          _cmd.Parameters.Add("@pTotalWon", SqlDbType.Money).Value = Won.TotalBalance;
          //
          _cmd.Parameters.Add("@pPlaySessionId ", SqlDbType.BigInt).Value = PlaySessionId;

          return (_cmd.ExecuteNonQuery() == 1);
        }
      }
      catch
      { ; }

      return false;

    } // Trx_PlaySessionClose


    public static Boolean Trx_ResetCancellableOperation(Int64 AccountId, SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" UPDATE   ACCOUNTS ");
        _sb.AppendLine("    SET   AC_CANCELLABLE_OPERATION_ID = NULL ");
        _sb.AppendLine("  WHERE   AC_ACCOUNT_ID               = @pAccountId   ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;

          return (_cmd.ExecuteNonQuery() == 1);
        }
      }
      catch
      { ; }

      return false;

    } // Trx_ResetCancellableOperation



    // PURPOSE: Initialize the fields "in session" of the account and play session of the account promotions
    //
    //  PARAMS:
    //     - INPUT:
    //           - AccountId:
    //           - PlaySessionId:
    //           - SqlTrx:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - true if updated successfully. false otherwise.

    public static Boolean Trx_ResetAccountInSession(Int64 AccountId,
                                                    Int64 PlaySessionId,
                                                    out Decimal InSessionPlayed,
                                                    out Decimal InSessionWon,
                                                    out AccountBalance InSessionBalance,
                                                    SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      InSessionPlayed = 0;
      InSessionWon = 0;
      InSessionBalance = AccountBalance.Zero;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" UPDATE   ACCOUNTS ");
        _sb.AppendLine("    SET   AC_IN_SESSION_PLAYED     = 0 ");
        _sb.AppendLine("        , AC_IN_SESSION_WON        = 0 ");
        _sb.AppendLine("        , AC_IN_SESSION_RE_PLAYED  = 0 ");
        _sb.AppendLine("        , AC_IN_SESSION_NR_PLAYED  = 0 ");
        _sb.AppendLine("        , AC_IN_SESSION_RE_WON     = 0 ");
        _sb.AppendLine("        , AC_IN_SESSION_NR_WON     = 0 ");
        //
        _sb.AppendLine("        , AC_IN_SESSION_RE_BALANCE       = 0 ");
        _sb.AppendLine("        , AC_IN_SESSION_PROMO_RE_BALANCE = 0 ");
        _sb.AppendLine("        , AC_IN_SESSION_PROMO_NR_BALANCE = 0 ");
        //
        _sb.AppendLine("        , AC_IN_SESSION_RE_TO_GM         = 0 ");
        _sb.AppendLine("        , AC_IN_SESSION_PROMO_RE_TO_GM   = 0 ");
        _sb.AppendLine("        , AC_IN_SESSION_PROMO_NR_TO_GM   = 0 ");
        //
        _sb.AppendLine("        , AC_IN_SESSION_RE_FROM_GM       = 0 ");
        _sb.AppendLine("        , AC_IN_SESSION_PROMO_RE_FROM_GM = 0 ");
        _sb.AppendLine("        , AC_IN_SESSION_PROMO_NR_FROM_GM = 0 ");
        //
        _sb.AppendLine("        , AC_IN_SESSION_CANCELLABLE_TRANSACTION_ID = 0 ");
        _sb.AppendLine("        , AC_IN_SESSION_RE_CANCELLABLE             = 0 ");
        _sb.AppendLine("        , AC_IN_SESSION_PROMO_RE_CANCELLABLE       = 0 ");
        _sb.AppendLine("        , AC_IN_SESSION_PROMO_NR_CANCELLABLE       = 0 ");
        //
        _sb.AppendLine(" OUTPUT   DELETED.AC_IN_SESSION_PLAYED ");
        _sb.AppendLine("        , DELETED.AC_IN_SESSION_WON ");
        _sb.AppendLine("        , DELETED.AC_IN_SESSION_RE_BALANCE ");
        _sb.AppendLine("        , DELETED.AC_IN_SESSION_PROMO_RE_BALANCE ");
        _sb.AppendLine("        , DELETED.AC_IN_SESSION_PROMO_NR_BALANCE ");
        _sb.AppendLine("  WHERE   AC_ACCOUNT_ID = @pAccountId ");

        if (PlaySessionId != 0)
        {
          _sb.AppendLine("    AND   AC_CURRENT_PLAY_SESSION_ID = @pPlaySessionId ");
        }

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = PlaySessionId;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {

            if (!_reader.Read())
            {
              return false;
            }

            InSessionPlayed = _reader.GetDecimal(0);
            InSessionWon = _reader.GetDecimal(1);
            InSessionBalance = new AccountBalance(_reader.GetDecimal(2), _reader.GetDecimal(3), _reader.GetDecimal(4));
          }

        }

        if (!Trx_UnlinkPromotionsInSession(AccountId, PlaySessionId, 0, SqlTrx))
        {
          return false;
        }

        return true;

      }
      catch
      { ; }

      return false;

    } // Trx_ResetAccountInSession


    public static Boolean Trx_UnlinkPromotionsInSession(Int64 AccountId,
                                                        Int64 PlaySessionId,
                                                        Int64 TransactionId,
                                                        SqlTransaction SqlTrx)
    {

      StringBuilder _sb;
      DataTable _promos;
      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" SELECT   ACP_UNIQUE_ID ");
        _sb.AppendLine("   FROM   ACCOUNT_PROMOTIONS WITH(INDEX(IX_acp_account_status)) ");
        _sb.AppendLine("  WHERE   ACP_ACCOUNT_ID       = @pAccountId ");
        _sb.AppendLine("    AND   ACP_STATUS           = @pStatusActive ");
        if (PlaySessionId != 0)
        {
          _sb.AppendLine("    AND   ACP_PLAY_SESSION_ID = @pPlaySessionId ");

          if (TransactionId != 0)
          {
            _sb.AppendLine("    AND   ACP_TRANSACTION_ID  = @pTransactionId ");
          }
        }
        else
        {
          // No play session indicated, but, at least, check that the play session is not null.
          _sb.AppendLine("    AND   ACP_PLAY_SESSION_ID IS NOT NULL ");
        }

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _cmd.Parameters.Add("@pStatusActive", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.ACTIVE;
          _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = PlaySessionId;
          _cmd.Parameters.Add("@pTransactionId", SqlDbType.BigInt).Value = TransactionId;

          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
          {
            _promos = new DataTable();
            _sql_da.Fill(_promos);
          }
        }

        if (_promos.Rows.Count == 0)
        {
          return true;
        }

        _sb.Length = 0;
        _sb.AppendLine(" UPDATE   ACCOUNT_PROMOTIONS ");
        _sb.AppendLine("    SET   ACP_PLAY_SESSION_ID = NULL ");
        _sb.AppendLine("        , ACP_TRANSACTION_ID  = NULL ");
        _sb.AppendLine("   FROM   ACCOUNT_PROMOTIONS WITH(INDEX(PK_account_promotions)) ");
        _sb.AppendLine("  WHERE   ACP_UNIQUE_ID       = @pUniqueId ");
        _sb.AppendLine("    AND   ACP_ACCOUNT_ID      = @pAccountId ");
        _sb.AppendLine("    AND   ACP_STATUS          = @pStatusActive ");

        if (PlaySessionId != 0)
        {
          _sb.AppendLine("    AND   ACP_PLAY_SESSION_ID = @pPlaySessionId ");

          if (TransactionId != 0)
          {
            _sb.AppendLine("    AND   ACP_TRANSACTION_ID  = @pTransactionId ");
          }
        }

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          SqlParameter _unique_id;

          _unique_id = _cmd.Parameters.Add("@pUniqueId", SqlDbType.BigInt);
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _cmd.Parameters.Add("@pStatusActive", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.ACTIVE;
          _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = PlaySessionId;
          _cmd.Parameters.Add("@pTransactionId", SqlDbType.BigInt).Value = TransactionId;
          foreach (DataRow _promo in _promos.Rows)
          {
            _unique_id.Value = _promo[0];

            _cmd.ExecuteNonQuery();
            // It doesn't matter if updated or not.
          }
        }

        return true;
      }
      catch
      { ; }

      return false;

    } // Trx_UnlinkPromotionsInSession

    // PURPOSE: Update the status and withhold of account promotions. If There isn't balance then the state is EXHAUSTED.
    //
    //  PARAMS:
    //     - INPUT:
    //           - AccountId:
    //           - SqlTrx:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - Is correct the process?

    public static Boolean Trx_PromoRecomputeWithholdOnRecharge(Int64 AccountId, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      DataTable _promos;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" SELECT   ACP_UNIQUE_ID ");
        _sb.AppendLine("   FROM   ACCOUNT_PROMOTIONS WITH(INDEX(IX_acp_account_status)) ");
        _sb.AppendLine("  WHERE   ACP_ACCOUNT_ID  = @pAccountId ");
        _sb.AppendLine("    AND   ACP_STATUS      = @pStatusActive ");
        _sb.AppendLine("    AND   ACP_WITHHOLD    > 0 ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _cmd.Parameters.Add("@pStatusActive", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.ACTIVE;

          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
          {
            _promos = new DataTable();
            _sql_da.Fill(_promos);
          }
        }

        if (_promos.Rows.Count == 0)
        {
          return true;
        }

        _sb.Length = 0;
        _sb.AppendLine(" UPDATE   ACCOUNT_PROMOTIONS ");
        _sb.AppendLine("    SET   ACP_WITHHOLD    = CASE WHEN ACP_BALANCE < ACP_WITHHOLD THEN ACP_BALANCE ELSE ACP_WITHHOLD END ");
        _sb.AppendLine("   FROM   ACCOUNT_PROMOTIONS WITH(INDEX(PK_account_promotions)) ");
        _sb.AppendLine("  WHERE   ACP_UNIQUE_ID   = @pUniqueId ");
        _sb.AppendLine("    AND   ACP_ACCOUNT_ID  = @pAccountId ");
        _sb.AppendLine("    AND   ACP_STATUS      = @pStatusActive ");
        _sb.AppendLine("    AND   ACP_WITHHOLD    > 0 ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          SqlParameter _unique_id;

          _unique_id = _cmd.Parameters.Add("pUniqueId", SqlDbType.BigInt);
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _cmd.Parameters.Add("@pStatusActive", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.ACTIVE;

          foreach (DataRow _promo in _promos.Rows)
          {
            _unique_id.Value = _promo[0];

            _cmd.ExecuteNonQuery();
            // It doesn't matter if updated or not.
          }
        }

        return true;
      }
      catch
      { ; }

      return false;

    } // Trx_PromoRecomputeWithholdOnRecharge

    // PURPOSE: Update the status and withhold of account promotions. If There isn't balance then the state is EXHAUSTED.
    //
    //  PARAMS:
    //     - INPUT:
    //           - AccountId:
    //           - SqlTrx:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - Is correct the process?

    public static Boolean Trx_PromoMarkAsExhausted(Int64 AccountId, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      DataTable _promos;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" SELECT   ACP_UNIQUE_ID ");
        _sb.AppendLine("   FROM   ACCOUNT_PROMOTIONS WITH(INDEX(IX_acp_account_status)) ");
        _sb.AppendLine("  WHERE   ACP_ACCOUNT_ID  = @pAccountId ");
        _sb.AppendLine("    AND   ACP_STATUS      = @pStatusActive ");
        _sb.AppendLine("    AND   ACP_BALANCE     = 0 ");
        _sb.AppendLine("    AND   ACP_PLAY_SESSION_ID IS NULL ");
        _sb.AppendLine("    AND   ACP_CREDIT_TYPE IN (@pCreditTypeNR1, @pCreditTypeNR2) ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _cmd.Parameters.Add("@pCreditTypeNR1", SqlDbType.Int).Value = ACCOUNT_PROMO_CREDIT_TYPE.NR1;
          _cmd.Parameters.Add("@pCreditTypeNR2", SqlDbType.Int).Value = ACCOUNT_PROMO_CREDIT_TYPE.NR2;
          _cmd.Parameters.Add("@pStatusActive", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.ACTIVE;
          _cmd.Parameters.Add("@pStatusExhausted", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.EXHAUSTED;

          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
          {
            _promos = new DataTable();
            _sql_da.Fill(_promos);
          }
        }

        if (_promos.Rows.Count == 0)
        {
          return true;
        }

        _sb.Length = 0;
        _sb.AppendLine(" UPDATE   ACCOUNT_PROMOTIONS ");
        _sb.AppendLine("    SET   ACP_STATUS      = @pStatusExhausted");
        _sb.AppendLine("   FROM   ACCOUNT_PROMOTIONS WITH(INDEX(PK_account_promotions)) ");
        _sb.AppendLine("  WHERE   ACP_UNIQUE_ID   = @pUniqueId ");
        _sb.AppendLine("    AND   ACP_ACCOUNT_ID  = @pAccountId ");
        _sb.AppendLine("    AND   ACP_STATUS      = @pStatusActive ");
        _sb.AppendLine("    AND   ACP_BALANCE     = 0 ");
        _sb.AppendLine("    AND   ACP_PLAY_SESSION_ID IS NULL ");
        _sb.AppendLine("    AND   ACP_CREDIT_TYPE IN (@pCreditTypeNR1, @pCreditTypeNR2) ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          SqlParameter _unique_id;

          _unique_id = _cmd.Parameters.Add("@pUniqueId", SqlDbType.BigInt);
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _cmd.Parameters.Add("@pCreditTypeNR1", SqlDbType.Int).Value = ACCOUNT_PROMO_CREDIT_TYPE.NR1;
          _cmd.Parameters.Add("@pCreditTypeNR2", SqlDbType.Int).Value = ACCOUNT_PROMO_CREDIT_TYPE.NR2;
          _cmd.Parameters.Add("@pStatusActive", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.ACTIVE;
          _cmd.Parameters.Add("@pStatusExhausted", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.EXHAUSTED;

          foreach (DataRow _promo in _promos.Rows)
          {
            _unique_id.Value = _promo[0];

            _cmd.ExecuteNonQuery();
            // It doesn't matter if updated or not.
          }
        }

        return true;
      }
      catch
      { ; }

      return false;

    } // Trx_PromoRecomputeWithhold

    // PURPOSE: Know the "Not Redeemable" of the account promotions and actualize the play session 
    //
    //  PARAMS:
    //     - INPUT:
    //           - AccountId:
    //           - TerminalId:
    //           - PlaySessionId:
    //           - NotRedeemable:
    //           - SqlTrx:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - none
    public static Boolean Trx_GetPlayableBalance(Int64 AccountId,
                                                 Int64 TerminalId,
                                                 Int64 PlaySessionId,
                                                 Int64 TransactionId,
                                                 AccountBalance Current,
                                                 out AccountBalance Playable,
                                                 SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      DataTable _promos;
      ACCOUNT_PROMO_CREDIT_TYPE _credit_type;
      Decimal _promo_balance;
      Boolean _only_redeemable;
      String _provider;
      ProviderList _provider_list;
      DataRow _row;

      // All the cashable credits are playable
      Playable = new AccountBalance(Math.Max(0, Current.Redeemable),
                                    Math.Max(0, Current.PromoRedeemable),
                                    0);  // Zero, NotRedeemable will be added promotion by promotion


      if (PlaySessionId != HANDPAY_VIRTUAL_PLAY_SESSION_ID)
      {
        // Non Cashable Exists?
        // NR Promotions can have balance 0, but with withhold. Don't exit here, continue to mark them.
        if (Current.PromoNotRedeemable <= 0)
        {
          return true;
        }
      }

      _provider = "";
      _only_redeemable = false;
      _sb = new StringBuilder();

      try
      {
        _sb.Length = 0;
        _sb.AppendLine("SELECT   PV_NAME ");
        _sb.AppendLine("       , PV_ONLY_REDEEMABLE ");
        _sb.AppendLine("  FROM   PROVIDERS INNER JOIN TERMINALS ON TE_PROV_ID = PV_ID ");
        _sb.AppendLine(" WHERE   TE_TERMINAL_ID = @pTerminalId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.BigInt).Value = TerminalId;
          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              return false;
            }
            _provider = (String)_reader.GetString(0);
            _only_redeemable = (Boolean)_reader.GetBoolean(1);
          }
        }

        if (_only_redeemable)
        {
          return true;
        }

        // Working Table
        _promos = new DataTable();
        _promos.Columns.Add("ACP_UNIQUE_ID", Type.GetType("System.Int64"));
        _promos.Columns.Add("ACP_CREDIT_TYPE", Type.GetType("System.Int32"));
        _promos.Columns.Add("ACP_BALANCE", Type.GetType("System.Decimal"));

        // Get Promotions
        _sb.Length = 0;
        _sb.AppendLine(" SELECT   ACP_UNIQUE_ID ");
        _sb.AppendLine("	      , CASE WHEN ( PM_PLAY_RESTRICTED_TO_PROVIDER_LIST = 1 ) THEN PM_PROVIDER_LIST ELSE NULL END AS PROVIDER_LIST ");
        //
        _sb.AppendLine("   FROM   ACCOUNT_PROMOTIONS WITH(INDEX(IX_acp_account_status)) ");
        _sb.AppendLine("   INNER  JOIN PROMOTIONS ON PM_PROMOTION_ID = ACP_PROMO_ID     ");
        //
        _sb.AppendLine("  WHERE   ACP_ACCOUNT_ID       = @pAccountId ");        // IX_ACP_ACCOUNT_STATUS.ACP_ACCOUNT_ID
        _sb.AppendLine("    AND   ACP_STATUS           = @pStatusActive ");     // IX_ACP_ACCOUNT_STATUS.ACP_STATUS
        //
        _sb.AppendLine("    AND   ACP_CREDIT_TYPE IN   (@pTypeNR1, @pTypeNR2) ");
        // Retenido
        _sb.AppendLine("    AND ( ( ACP_BALANCE > 0 ) OR ( ACP_BALANCE = 0 AND ACP_WITHHOLD > 0 )) ");
        // Not In Session
        _sb.AppendLine("    AND   ACP_PLAY_SESSION_ID IS NULL ");
        _sb.AppendLine("  ORDER BY ACP_UNIQUE_ID ASC ");


        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _cmd.Parameters.Add("@pStatusActive", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.ACTIVE;
          _cmd.Parameters.Add("@pTypeNR1", SqlDbType.Int).Value = ACCOUNT_PROMO_CREDIT_TYPE.NR1;
          _cmd.Parameters.Add("@pTypeNR2", SqlDbType.Int).Value = ACCOUNT_PROMO_CREDIT_TYPE.NR2;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            while (_reader.Read())
            {
              if (!_reader.IsDBNull(1))
              {
                _provider_list = new ProviderList();
                _provider_list.FromXml(_reader.GetString(1));

                if (!_provider_list.Contains(_provider))
                {
                  continue;
                }
              }
              _row = _promos.NewRow();
              _row[0] = _reader.GetInt64(0);
              _promos.Rows.Add(_row);
            }
          }
        }

        _sb.Length = 0;
        _sb.AppendLine("UPDATE   ACCOUNT_PROMOTIONS ");
        _sb.AppendLine("   SET   ACP_PLAY_SESSION_ID = @pPlaySessionId ");
        _sb.AppendLine("       , ACP_TRANSACTION_ID  = @pTransactionId ");
        _sb.AppendLine("OUTPUT   DELETED.ACP_CREDIT_TYPE ");
        _sb.AppendLine("       , DELETED.ACP_BALANCE     ");
        _sb.AppendLine("  FROM   ACCOUNT_PROMOTIONS WITH(INDEX(PK_account_promotions)) ");
        _sb.AppendLine(" WHERE   ACP_UNIQUE_ID       = @pUniqueId ");   // PK
        _sb.AppendLine("   AND   ACP_ACCOUNT_ID      = @pAccountId ");
        _sb.AppendLine("   AND   ACP_STATUS          = @pStatusActive ");
        _sb.AppendLine("   AND   ACP_CREDIT_TYPE IN   (@pTypeNR1, @pTypeNR2) ");
        _sb.AppendLine("   AND ( ( ACP_BALANCE > 0 ) OR ( ACP_BALANCE = 0 AND ACP_WITHHOLD > 0 )) ");
        _sb.AppendLine("   AND   ACP_PLAY_SESSION_ID IS NULL ");


        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          SqlParameter _unique_id;

          _unique_id = _cmd.Parameters.Add("@pUniqueId", SqlDbType.BigInt);
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _cmd.Parameters.Add("@pStatusActive", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.ACTIVE;
          _cmd.Parameters.Add("@pTypeNR1", SqlDbType.Int).Value = ACCOUNT_PROMO_CREDIT_TYPE.NR1;
          _cmd.Parameters.Add("@pTypeNR2", SqlDbType.Int).Value = ACCOUNT_PROMO_CREDIT_TYPE.NR2;
          _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = PlaySessionId;
          _cmd.Parameters.Add("@pTransactionId", SqlDbType.BigInt).Value = TransactionId;

          foreach (DataRow _promo in _promos.Rows)
          {
            _unique_id.Value = _promo[0];

            using (SqlDataReader _reader = _cmd.ExecuteReader())
            {
              if (!_reader.Read())
              {
                // Not an error, conditions changed ...
                continue;
              }

              _credit_type = (ACCOUNT_PROMO_CREDIT_TYPE)_reader.GetInt32(0);
              _promo_balance = _reader.GetDecimal(1);
              _promo_balance = Math.Max(0, _promo_balance);

              switch (_credit_type)
              {
                case ACCOUNT_PROMO_CREDIT_TYPE.NR1:
                case ACCOUNT_PROMO_CREDIT_TYPE.NR2:
                  Playable.PromoNotRedeemable += _promo_balance;
                  break;

                case ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE:
                case ACCOUNT_PROMO_CREDIT_TYPE.UNKNOWN:
                default:
                  break;
              }
            }
          }
        }

        Playable.PromoNotRedeemable = Math.Max(0, Math.Min(Playable.PromoNotRedeemable, Current.PromoNotRedeemable));

        return true;
      }
      catch
      { ; }

      return false;

    } // Trx_GetPlayableBalance

    // PURPOSE: 
    //
    //  PARAMS:
    //     - INPUT:
    //           - PlaySessionId:
    //           - Trx:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - none

    public static bool Trx_UpdatePlayedAndWon(Int64 PlaySessionId, out AccountBalance InSessionBalance, SqlTransaction Trx)
    {
      InSessionParameters _params;
      StringBuilder _err_msg;

      _err_msg = new StringBuilder();
      InSessionBalance = AccountBalance.Zero;

      if (PlaySessionId == 0)
      {
        return false;
      }

      if (!Trx_GetDeltaPlayedWon(PlaySessionId, out _params, Trx))
      {
        return false;
      }

      if (!Trx_UpdatePlayedAndWon(_params, _err_msg, Trx))
      {
        return false;
      }

      InSessionBalance = _params.Balance;

      return true;
    } // Trx_UpdatePlayedAndWon

    private static Boolean Trx_GetCreditsPlayMode(out Terminal.CreditsPlayMode PlayMode, SqlTransaction Trx)
    {
      StringBuilder _sb;

      PlayMode = Terminal.CreditsPlayMode.NotRedeemableLast;

      try
      {
        _sb = new StringBuilder();
        _sb.Length = 0;
        _sb.AppendLine("SELECT ISNULL ( ( SELECT   CAST (GP_KEY_VALUE AS INT)         ");
        _sb.AppendLine("	                  FROM   GENERAL_PARAMS                     ");
        _sb.AppendLine("	                 WHERE   GP_GROUP_KEY   = 'Credits'         ");
        _sb.AppendLine("	                   AND   GP_SUBJECT_KEY = 'PlayMode'        ");
        _sb.AppendLine("	                   AND   ISNUMERIC(GP_KEY_VALUE) = 1 ), 0 ) ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          PlayMode = (Terminal.CreditsPlayMode)((Int32)_cmd.ExecuteScalar());
        }

        switch (PlayMode)
        {
          case Terminal.CreditsPlayMode.NotRedeemableLast:
          case Terminal.CreditsPlayMode.PromotionalsFirst:
            break;

          default:
            PlayMode = Terminal.CreditsPlayMode.NotRedeemableLast;
            break;
        }

        return true;
      }
      catch
      {
      }
      return false;
    }

    // PURPOSE: Compute the played and won and update the account and promotion.
    //
    //  PARAMS:
    //     - INPUT:
    //           - InSessionParameters:
    //           - Trx:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - true if computed and update was successfully. false otherwise.

    public static bool Trx_UpdatePlayedAndWon(InSessionParameters InSessionParameters, StringBuilder ErrorMsg, SqlTransaction Trx)
    {
      StringBuilder _sb;
      DataRow[] _modified_rows;
      Terminal.CreditsPlayMode _credits_play_mode;

      try
      {
        _sb = new StringBuilder();

        if (InSessionParameters.DeltaPlayed != 0 || InSessionParameters.DeltaWon != 0)
        {
          if (!Trx_GetCreditsPlayMode(out _credits_play_mode, Trx))
          {
            ErrorMsg.AppendLine("Trx_GetCreditsPlayMode failed.");

            return false;
          }

          _sb.Length = 0;
          _sb.AppendLine("   SELECT   ACP_BALANCE ");
          _sb.AppendLine("          , ACP_WITHHOLD ");
          _sb.AppendLine(" 		      , ACP_WONLOCK ");
          _sb.AppendLine(" 	        , ACP_PLAYED ");
          _sb.AppendLine(" 		      , ACP_WON ");
          _sb.AppendLine(" 		      , ACP_CREDIT_TYPE ");
          _sb.AppendLine(" 		      , ACP_UNIQUE_ID ");
          _sb.AppendLine("     FROM   ACCOUNT_PROMOTIONS WITH (INDEX(IX_acp_account_status)) ");
          _sb.AppendLine("    WHERE   ACP_ACCOUNT_ID      = @pAccountId ");
          _sb.AppendLine("      AND   ACP_STATUS          = @pStatusActive ");
          _sb.AppendLine("      AND   ACP_PLAY_SESSION_ID = @pPlaySessionId ");
          _sb.AppendLine(" ORDER BY   ACP_UNIQUE_ID ASC ");

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
          {
            _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = InSessionParameters.PlaySessionId;
            _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = InSessionParameters.AccountId;
            _cmd.Parameters.Add("@pStatusActive", SqlDbType.Int).Value = (Int32)ACCOUNT_PROMO_STATUS.ACTIVE;

            InSessionParameters.AccountPromotion = new DataTable();
            using (SqlDataReader _reader = _cmd.ExecuteReader())
            {
              InSessionParameters.AccountPromotion.Load(_reader);
            }
          }

          //
          // Simulate Played/Won by System
          //

          switch (_credits_play_mode)
          {
            case Terminal.CreditsPlayMode.NotRedeemableLast: // Wigos Default
              Decimal _delta_played;
              Decimal _delta_won;
              Decimal _nr_ratio;
              Decimal _nr_replay;

              _nr_ratio = 0;
              if (InSessionParameters.Balance.TotalBalance > 0 && InSessionParameters.Balance.TotalNotRedeemable > 0)
              {
                _nr_ratio = InSessionParameters.Balance.TotalNotRedeemable / InSessionParameters.Balance.TotalBalance;
              }

              // 1.- PLAY the initial balance
              // 2.- When (OnlyNR) Replay NR
              // 3.- WON 
              // 4.- PLAY

              // 1.- PLAY the initial balance
              // PLAYED:   NR2 -> PromoRE -> RE -> NR1
              PlayPromoPrizeOnly(ACCOUNT_PROMO_CREDIT_TYPE.NR1, InSessionParameters);
              PlayPromoNotRedeemableBalance(ACCOUNT_PROMO_CREDIT_TYPE.NR2, InSessionParameters);
              PlayRedeemable(InSessionParameters);
              // Don't play NR1 here, it will be played later ... 
              // PlayPromoNotRedeemableBalance(ACCOUNT_PROMO_CREDIT_TYPE.NR1, InSessionParameters);

              // 2.- When (NR present) Replay NR
              // Compute the "Rejuego"
              _nr_replay = Math.Max(0, Math.Min(InSessionParameters.DeltaPlayed, InSessionParameters.DeltaWon));
              _nr_replay = Math.Round(_nr_replay * _nr_ratio, 2, MidpointRounding.AwayFromZero);

              if (_nr_replay > 0)
              {
                // Save 'delta' without the 'Rejuego'
                _delta_played = InSessionParameters.DeltaPlayed - _nr_replay;
                _delta_won = InSessionParameters.DeltaWon - _nr_replay;

                InSessionParameters.DeltaPlayed = _nr_replay;
                InSessionParameters.DeltaWon = _nr_replay;
                WonPromoNotRedeemable(InSessionParameters, true);
                PlayPromoNotRedeemableBalance(ACCOUNT_PROMO_CREDIT_TYPE.NR1, InSessionParameters);

                // Restore the previously computed 'delta'
                // Note: Delta MUST be computed as NewDelta = OldDelta + 'SavedDelta', 
                //       because it is possible that 'Delta' has some remaining value
                InSessionParameters.DeltaPlayed = InSessionParameters.DeltaPlayed + _delta_played;
                InSessionParameters.DeltaWon = InSessionParameters.DeltaWon + _delta_won;
              }

              // 3.- WON 
              WonPromoNotRedeemable(InSessionParameters, false);
              WonRedeemable(InSessionParameters);
              // 4.- PLAY
              // PLAYED:   NR2 -> PromoRE -> RE -> NR1
              PlayPromoPrizeOnly(ACCOUNT_PROMO_CREDIT_TYPE.NR1, InSessionParameters);
              PlayPromoNotRedeemableBalance(ACCOUNT_PROMO_CREDIT_TYPE.NR2, InSessionParameters);
              PlayRedeemable(InSessionParameters);
              PlayPromoNotRedeemableBalance(ACCOUNT_PROMO_CREDIT_TYPE.NR1, InSessionParameters);

              break;

            case Terminal.CreditsPlayMode.PromotionalsFirst: // Like SAS
              // WON
              WonRedeemable(InSessionParameters);
              // PLAYED: NR2 -> NR1 -> PromoRE -> RE
              PlayPromoNotRedeemableBalance(ACCOUNT_PROMO_CREDIT_TYPE.NR2, InSessionParameters);
              PlayPromoNotRedeemableBalance(ACCOUNT_PROMO_CREDIT_TYPE.NR1, InSessionParameters);
              PlayRedeemable(InSessionParameters);

              break;

            default:
              ErrorMsg.AppendLine(String.Format("Unknown CreditPlayMode: {0}", _credits_play_mode));

              return false;
          }

          // DeltaPlayed & DeltaWon contain the "error" amounts 
          InSessionParameters.DeltaPlayed = Math.Max(0, InSessionParameters.DeltaPlayed);
          InSessionParameters.DeltaWon = Math.Max(0, InSessionParameters.DeltaWon);

          // UPDATE NOT REDEEMABLE IN SESSION
          _modified_rows = InSessionParameters.AccountPromotion.Select("", "", DataViewRowState.ModifiedCurrent);
          if (_modified_rows.Length > 0)
          {
            _sb.Length = 0;
            _sb.AppendLine("UPDATE   ACCOUNT_PROMOTIONS ");
            _sb.AppendLine("   SET   ACP_BALANCE         = @pBalance ");
            _sb.AppendLine("       , ACP_WON             = @pWon ");
            _sb.AppendLine("       , ACP_PLAYED          = @pPlayed ");
            _sb.AppendLine("  FROM   ACCOUNT_PROMOTIONS WITH(INDEX(PK_account_promotions)) ");
            _sb.AppendLine(" WHERE   ACP_UNIQUE_ID       = @pUniqueId ");
            _sb.AppendLine("   AND   ACP_ACCOUNT_ID      = @pAccountId ");
            _sb.AppendLine("   AND   ACP_STATUS          = @pStatusActive ");
            _sb.AppendLine("   AND   ACP_PLAY_SESSION_ID = @pPlaySessionId ");

            using (SqlDataAdapter _da = new SqlDataAdapter())
            {
              _da.UpdateCommand = new SqlCommand(_sb.ToString(), Trx.Connection, Trx);
              _da.UpdateCommand.Parameters.Add("@pBalance", SqlDbType.Money).SourceColumn = "ACP_BALANCE";
              _da.UpdateCommand.Parameters.Add("@pWon", SqlDbType.Money).SourceColumn = "ACP_WON";
              _da.UpdateCommand.Parameters.Add("@pPlayed", SqlDbType.Money).SourceColumn = "ACP_PLAYED";
              _da.UpdateCommand.Parameters.Add("@pUniqueId", SqlDbType.BigInt).SourceColumn = "ACP_UNIQUE_ID";
              _da.UpdateCommand.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = InSessionParameters.AccountId;
              _da.UpdateCommand.Parameters.Add("@pStatusActive", SqlDbType.Int).Value = (Int32)ACCOUNT_PROMO_STATUS.ACTIVE;
              _da.UpdateCommand.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = InSessionParameters.PlaySessionId;

              _da.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;
              _da.UpdateBatchSize = 1;
              _da.ContinueUpdateOnError = true;

              int _num_updated;

              _num_updated = _da.Update(_modified_rows);
              if (_num_updated != _modified_rows.Length)
              {
                ErrorMsg.AppendLine(String.Format("Can't update account promotions. Updated {0} of {1}", _num_updated, _modified_rows.Length));
                foreach (DataRow _row in _modified_rows)
                {
                  ErrorMsg.AppendLine(String.Format("Promotion UniqueId: {0}, CreditType: {1}, Balance: {2}, Played: {3}, Won: {4}",
                                                    _row["ACP_UNIQUE_ID"], _row["ACP_CREDIT_TYPE"], _row["ACP_BALANCE"], _row["ACP_PLAYED"], _row["ACP_WON"]));


                  if (_row.HasErrors)
                  {
                    ErrorMsg.AppendLine("Error: " + _row.RowError);
                  }
                }
                ErrorMsg.AppendLine("Query.CommandText: " + _da.UpdateCommand.CommandText);
                foreach (SqlParameter _p in _da.UpdateCommand.Parameters)
                {
                  ErrorMsg.AppendLine(String.Format("Parameter {0} = {1}", _p.ParameterName, _p.Value));
                }


                return false;
              }
            }

          } // if (_modified_rows.Length

        } // if (InSessionParameters.DeltaPlayed != 0 || InSessionParameters.DeltaWon != 0)

        // UPDATE REDEEMABLE IN SESSION
        _sb.Length = 0;
        _sb.AppendLine(" UPDATE   ACCOUNTS ");
        _sb.AppendLine("    SET   AC_IN_SESSION_RE_BALANCE       = @pInSessionReBalance      ");
        _sb.AppendLine("        , AC_IN_SESSION_PROMO_RE_BALANCE = @pInSessionPromoReBalance ");
        _sb.AppendLine("        , AC_IN_SESSION_PROMO_NR_BALANCE = @pInSessionPromoNrBalance ");
        _sb.AppendLine("        , AC_IN_SESSION_RE_PLAYED        = AC_IN_SESSION_RE_PLAYED + @pInSessionRePlayed ");
        _sb.AppendLine("        , AC_IN_SESSION_NR_PLAYED        = AC_IN_SESSION_NR_PLAYED + @pInSessionNrPlayed ");
        _sb.AppendLine("        , AC_IN_SESSION_RE_WON           = AC_IN_SESSION_RE_WON    + @pInSessionReWon ");
        _sb.AppendLine("        , AC_IN_SESSION_NR_WON           = AC_IN_SESSION_NR_WON    + @pInSessionNrWon ");
        _sb.AppendLine("        , AC_IN_SESSION_PLAYED           = AC_IN_SESSION_PLAYED    - @pInSessionPlayed ");
        _sb.AppendLine("        , AC_IN_SESSION_WON              = AC_IN_SESSION_WON       - @pInSessionWon ");

        _sb.AppendLine(" OUTPUT   INSERTED.AC_IN_SESSION_RE_PLAYED  ");
        _sb.AppendLine("        , INSERTED.AC_IN_SESSION_NR_PLAYED  ");
        _sb.AppendLine("        , INSERTED.AC_IN_SESSION_RE_WON     ");
        _sb.AppendLine("        , INSERTED.AC_IN_SESSION_NR_WON     ");

        _sb.AppendLine("   FROM   ACCOUNTS ");
        _sb.AppendLine("  WHERE   AC_ACCOUNT_ID = @pAccountId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = InSessionParameters.AccountId;
          _cmd.Parameters.Add("@pInSessionReBalance", SqlDbType.Money).Value = InSessionParameters.Balance.Redeemable;
          _cmd.Parameters.Add("@pInSessionPromoReBalance", SqlDbType.Money).Value = InSessionParameters.Balance.PromoRedeemable;
          _cmd.Parameters.Add("@pInSessionPromoNrBalance", SqlDbType.Money).Value = InSessionParameters.Balance.PromoNotRedeemable;
          _cmd.Parameters.Add("@pInSessionRePlayed", SqlDbType.Money).Value = InSessionParameters.DeltaPlayedRe;
          _cmd.Parameters.Add("@pInSessionNrPlayed", SqlDbType.Money).Value = InSessionParameters.DeltaPlayedNr;
          _cmd.Parameters.Add("@pInSessionReWon", SqlDbType.Money).Value = InSessionParameters.DeltaWonRe;
          _cmd.Parameters.Add("@pInSessionNrWon", SqlDbType.Money).Value = InSessionParameters.DeltaWonNr;
          // DeltaPlayed & DeltaWon contain the "error" amounts 
          _cmd.Parameters.Add("@pInSessionPlayed", SqlDbType.Money).Value = InSessionParameters.DeltaPlayed;
          _cmd.Parameters.Add("@pInSessionWon", SqlDbType.Money).Value = InSessionParameters.DeltaWon;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              ErrorMsg.AppendLine(String.Format("Can't update 'in session' account, Id: {0}", InSessionParameters.AccountId));

              return false;
            }

            InSessionParameters.Played = new AccountBalance(_reader.GetDecimal(0), 0, _reader.GetDecimal(1));
            InSessionParameters.Won = new AccountBalance(_reader.GetDecimal(2), 0, _reader.GetDecimal(3));
          }
        }

        return true;

      }
      catch (Exception _ex)
      {
        ErrorMsg.AppendLine(_ex.Message);
      }

      return false;

    } // Trx_UpdatePlayedAndWon

    // PURPOSE: 
    //
    //  PARAMS:
    //     - INPUT:
    //           - PlaySessionId:
    //           - InSessionParameters:
    //           - Trx:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - none

    public static Boolean Trx_GetDeltaPlayedWon(Int64 PlaySessionId, out InSessionParameters InSessionParameters, SqlTransaction Trx)
    {
      StringBuilder _sb;

      InSessionParameters = new InSessionParameters();

      _sb = new StringBuilder();
      _sb.AppendLine(" UPDATE   ACCOUNTS ");
      _sb.AppendLine("    SET   AC_IN_SESSION_PLAYED = CASE WHEN (PS_PLAYED_AMOUNT > AC_IN_SESSION_PLAYED) THEN PS_PLAYED_AMOUNT ELSE AC_IN_SESSION_PLAYED END ");
      _sb.AppendLine("        , AC_IN_SESSION_WON    = CASE WHEN (PS_WON_AMOUNT    > AC_IN_SESSION_WON)    THEN PS_WON_AMOUNT    ELSE AC_IN_SESSION_WON    END ");
      _sb.AppendLine(" OUTPUT   INSERTED.AC_IN_SESSION_PLAYED - DELETED.AC_IN_SESSION_PLAYED AS DELTA_PLAYED ");
      _sb.AppendLine("        , INSERTED.AC_IN_SESSION_WON    - DELETED.AC_IN_SESSION_WON    AS DELTA_WON    ");
      _sb.AppendLine("        , DELETED.AC_IN_SESSION_RE_BALANCE ");
      _sb.AppendLine("        , DELETED.AC_IN_SESSION_PROMO_RE_BALANCE ");
      _sb.AppendLine("        , DELETED.AC_IN_SESSION_PROMO_NR_BALANCE ");
      _sb.AppendLine("        , DELETED.AC_ACCOUNT_ID ");
      _sb.AppendLine("   FROM   PLAY_SESSIONS AS PS INNER JOIN ACCOUNTS AS ACC ON PS.PS_ACCOUNT_ID = ACC.AC_ACCOUNT_ID ");
      _sb.AppendLine("  WHERE   PS.PS_PLAY_SESSION_ID = @pPlaySessionId ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = PlaySessionId;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              return false;
            }
            InSessionParameters = new InSessionParameters();
            InSessionParameters.DeltaPlayed = _reader.GetDecimal(0);
            InSessionParameters.DeltaWon = _reader.GetDecimal(1);
            InSessionParameters.Balance = new AccountBalance(_reader.GetDecimal(2), _reader.GetDecimal(3), _reader.GetDecimal(4));
            InSessionParameters.AccountId = _reader.GetInt64(5);
            InSessionParameters.PlaySessionId = PlaySessionId;

            return true;
          }
        }

      }
      catch
      {
      }

      return false;

    } // Trx_GetInSessionParameters

    // PURPOSE: Update the balance, redeemable and not redeemable of the account.
    //
    //  PARAMS:
    //     - INPUT:
    //           - AccountId:
    //           - RedimibleBalance: The redimible to aggregate in the balance
    //           - NotRedeemableToAdd: The not redimible to aggregate in the balance
    //           - IsActivity: Update the last activity of account
    //           - Trx:
    //     - OUTPUT:
    //           - RedeemableFinalBalance: The redeemable in the account after update.
    //           - NotRedeemableFinalBalance: The not redeemable in the account after update.
    //           - PlaySessionId:  Current play session
    //
    // RETURNS:
    //     - Is correct the process?

    public static Boolean Trx_UpdateAccountPoints(Int64 AccountId,
                                               Decimal PointsToAdd,
                                               out Decimal PointsToAddFinalBalance,
                                               SqlTransaction Trx)
    {
      AccountBalance _final;
      AccountBalance _cancel;
      Int64 _play_session_id;

      return Trx_UpdateAccountBalance(AccountId, AccountBalance.Zero, PointsToAdd, out _final, out PointsToAddFinalBalance, out _play_session_id, out _cancel, Trx);

    } // Trx_UpdateAccountPoints

    public static Boolean Trx_UpdateAccountBalance(Int64 AccountId,
                                                   AccountBalance ToAdd,
                                                   Decimal PointsToAdd,
                                                   out AccountBalance Final,
                                                   out Decimal FinalPoints,
                                                   SqlTransaction Trx)
    {
      AccountBalance _cancel;
      Int64 _play_session_id;

      return Trx_UpdateAccountBalance(AccountId, ToAdd, PointsToAdd, out Final, out FinalPoints, out _play_session_id, out _cancel, Trx);
    } // Trx_UpdateAccountBalance

    public static Boolean Trx_UpdateAccountBalance(Int64 AccountId,
                                               AccountBalance ToAdd,
                                               out AccountBalance Final,
                                               SqlTransaction Trx)
    {
      AccountBalance _cancel;
      Decimal _fin_points;
      Int64 _play_session_id;

      return Trx_UpdateAccountBalance(AccountId, ToAdd, 0, out Final, out _fin_points, out _play_session_id, out _cancel, Trx);
    } // Trx_UpdateAccountBalance

    public static Boolean Trx_UpdateAccountBalance(Int64 AccountId,
                                                   AccountBalance ToAdd,
                                                   out AccountBalance Final,
                                                   out Int64 PlaySessionId,
                                                   SqlTransaction Trx)
    {
      Decimal _final_points;
      AccountBalance _cancel;

      return Trx_UpdateAccountBalance(AccountId, ToAdd, 0, out Final, out _final_points, out PlaySessionId, out _cancel, Trx);
    } // Trx_UpdateAccountBalance

    public static Boolean Trx_UpdateAccountBalance(Int64 AccountId,
                                                 AccountBalance ToAdd,
                                                 out AccountBalance Final,
                                                 out Decimal FinalPoints,
                                                 out Int64 PlaySessionId,
                                                 SqlTransaction Trx)
    {

      AccountBalance _cancel;

      return Trx_UpdateAccountBalance(AccountId, ToAdd, 0, out Final, out FinalPoints, out PlaySessionId, out _cancel, Trx);
    } // Trx_UpdateAccountBalance

    public static Boolean Trx_UpdateAccountBalance(Int64 AccountId,
                                                   AccountBalance ToAdd,
                                                   out AccountBalance Final,
                                                   out Int64 PlaySessionId,
                                                   out AccountBalance Cancellable,
                                                   SqlTransaction Trx)
    {
      Decimal _final_points;
      return Trx_UpdateAccountBalance(AccountId, ToAdd, 0, out Final, out _final_points, out PlaySessionId, out Cancellable, Trx);
    } // Trx_UpdateAccountBalance

    public static Boolean Trx_UpdateAccountBalance(Int64 AccountId,
                                                   AccountBalance ToAdd,
                                                   Decimal PointsToAdd,
                                                   out AccountBalance Final,
                                                   out Decimal FinalPoints,
                                                   out Int64 PlaySessionId,
                                                   out AccountBalance Cancellable,
                                                   SqlTransaction Trx)
    {
      AccountInfo _acc_info;

      Final = AccountBalance.Zero;
      FinalPoints = 0;
      PlaySessionId = 0;
      Cancellable = AccountBalance.Zero;

      if (!Trx_UpdateAccountBalance(AccountId, String.Empty, ToAdd, PointsToAdd, out _acc_info, Trx))
      {
        return false;
      }

      Final = _acc_info.FinalBalance;
      FinalPoints = _acc_info.FinalPoints;
      PlaySessionId = _acc_info.CurrentPlaySessionId;
      Cancellable = _acc_info.CancellableBalance;

      return true;
    } // Trx_UpdateAccountBalance

    // PURPOSE: Update balance account and points.
    //
    //  PARAMS:
    //     - INPUT:
    //           - AccountId:
    //           - ExternalTrackData:
    //           - ToAdd:
    //           - PointsToAdd:
    //           - AccountInfo:
    //           - Trx:
    //
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - true if updated succefully. else otherwise.

    public static Boolean Trx_UpdateAccountBalance(Int64 AccountId,
                                                   String ExternalTrackData,
                                                   AccountBalance ToAdd,
                                                   Decimal PointsToAdd,
                                                   out AccountInfo AccountInfo,
                                                   SqlTransaction Trx)
    {
      StringBuilder _sb;
      String _track_data;

      _track_data = "";
      AccountInfo = new AccountInfo();

      _sb = new StringBuilder();
      _sb.AppendLine(" UPDATE   ACCOUNTS ");
      _sb.AppendLine("    SET   AC_BALANCE           = AC_RE_BALANCE + AC_PROMO_RE_BALANCE + AC_PROMO_NR_BALANCE + @pReBalance + @pPromoReBalance + @pPromoNrBalance ");
      _sb.AppendLine("        , AC_RE_BALANCE        = AC_RE_BALANCE        + @pReBalance ");
      _sb.AppendLine("        , AC_PROMO_RE_BALANCE  = AC_PROMO_RE_BALANCE  + @pPromoReBalance ");
      _sb.AppendLine("        , AC_PROMO_NR_BALANCE  = AC_PROMO_NR_BALANCE  + @pPromoNrBalance ");
      _sb.AppendLine("        , AC_POINTS            = ISNULL(AC_POINTS, 0) + @pPoints    ");
      //
      _sb.AppendLine(" OUTPUT   INSERTED.AC_ACCOUNT_ID ");
      //
      _sb.AppendLine("        , INSERTED.AC_BLOCKED       ");
      _sb.AppendLine("        , INSERTED.AC_BLOCK_REASON  ");
      //
      _sb.AppendLine("        , INSERTED.AC_HOLDER_LEVEL  ");
      _sb.AppendLine("        , ISNULL(INSERTED.AC_HOLDER_GENDER, 0) ");
      _sb.AppendLine("        , ISNULL(INSERTED.AC_HOLDER_NAME,  '') ");
      //
      _sb.AppendLine("        , INSERTED.AC_RE_BALANCE ");
      _sb.AppendLine("        , INSERTED.AC_PROMO_RE_BALANCE ");
      _sb.AppendLine("        , INSERTED.AC_PROMO_NR_BALANCE ");
      _sb.AppendLine("        , INSERTED.AC_POINTS     ");
      //
      _sb.AppendLine("        , INSERTED.AC_CURRENT_PLAY_SESSION_ID ");
      _sb.AppendLine("        , INSERTED.AC_CURRENT_TERMINAL_ID     ");
      //
      _sb.AppendLine("        , INSERTED.AC_IN_SESSION_CANCELLABLE_TRANSACTION_ID ");
      _sb.AppendLine("        , INSERTED.AC_IN_SESSION_RE_CANCELLABLE ");
      _sb.AppendLine("        , INSERTED.AC_IN_SESSION_PROMO_RE_CANCELLABLE ");
      _sb.AppendLine("        , INSERTED.AC_IN_SESSION_PROMO_NR_CANCELLABLE ");
      //
      _sb.AppendLine("        , ISNULL(INSERTED.AC_CANCELLABLE_OPERATION_ID, 0) ");

      if (AccountId != 0)
      {
        _sb.AppendLine("  WHERE   AC_ACCOUNT_ID   = @pAccountId ");
      }
      else
      {
        _sb.AppendLine("  WHERE   AC_TRACK_DATA   = dbo.TrackDataToInternal (@pExternalTrackData) ");
      }

      _sb.AppendLine("    AND   AC_RE_BALANCE        + @pReBalance      >= 0 ");
      _sb.AppendLine("    AND   AC_PROMO_RE_BALANCE  + @pPromoReBalance >= 0 ");
      _sb.AppendLine("    AND   AC_PROMO_NR_BALANCE  + @pPromoNrBalance >= 0 ");
      _sb.AppendLine("    AND   ISNULL(AC_POINTS, 0) + @pPoints         >= 0 ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          if (AccountId == 0)
          {
            _track_data = ExternalTrackData.Trim();
            _track_data = _track_data.PadLeft(20, '0');
            _cmd.Parameters.Add("@pExternalTrackData", SqlDbType.NVarChar, 50).Value = _track_data;
          }
          else
          {
            _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          }

          _cmd.Parameters.Add("@pReBalance", SqlDbType.Money).Value = ToAdd.Redeemable;
          _cmd.Parameters.Add("@pPromoReBalance", SqlDbType.Money).Value = ToAdd.PromoRedeemable;
          _cmd.Parameters.Add("@pPromoNrBalance", SqlDbType.Money).Value = ToAdd.PromoNotRedeemable;
          _cmd.Parameters.Add("@pPoints", SqlDbType.Money).Value = PointsToAdd;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              if (AccountId == 0)
              {
                AccountInfo.ErrorMessage = "ExternalTrackData: '" + ExternalTrackData.Trim() + "' --> Account Not Found.";
              }
              else
              {
                AccountInfo.ErrorMessage = "AccountId: " + AccountId.ToString() + " --> Account Not Found."; ;
              }
              return false;
            }

            AccountInfo.AccountId = _reader.GetInt64(0);

            AccountInfo.Blocked = _reader.GetBoolean(1);
            AccountInfo.BlockReason = _reader.GetInt32(2);

            AccountInfo.HolderLevel = _reader.GetInt32(3);
            if (AccountInfo.HolderLevel > 0)
            {
              AccountInfo.HolderGender = _reader.GetInt32(4);
              AccountInfo.HolderName = _reader.GetString(5);
            }

            AccountInfo.FinalBalance = new AccountBalance(_reader.GetDecimal(6), _reader.GetDecimal(7), _reader.GetDecimal(8));
            AccountInfo.FinalPoints = _reader.GetDecimal(9);

            // InSession?
            AccountInfo.CurrentPlaySessionId = _reader.IsDBNull(10) ? 0 : _reader.GetInt64(10);
            AccountInfo.CurrentTerminalId = _reader.IsDBNull(11) ? 0 : _reader.GetInt32(11);

            if (AccountInfo.CurrentPlaySessionId != 0)
            {
              AccountInfo.CancellableTrxId = _reader.IsDBNull(12) ? 0 : _reader.GetInt64(12);
              if (AccountInfo.CancellableTrxId != 0)
              {
                AccountInfo.CancellableBalance = new AccountBalance(_reader.GetDecimal(13), _reader.GetDecimal(14), _reader.GetDecimal(15));
              }
            }

            AccountInfo.CancellableOperationId = _reader.IsDBNull(16) ? 0 : _reader.GetInt64(16);

          }
        }

        if (AccountInfo.CurrentPlaySessionId != 0)
        {
          _sb.Length = 0;
          _sb.AppendLine(" SELECT   PS_STATUS ");
          _sb.AppendLine("        , ISNULL(PS_BALANCE_MISMATCH, 0) ");
          _sb.AppendLine("        , PS_INITIAL_BALANCE ");
          _sb.AppendLine("        , PS_CASH_IN ");
          _sb.AppendLine("   FROM   PLAY_SESSIONS ");
          _sb.AppendLine("  WHERE   PS_PLAY_SESSION_ID = @pPlaySessionId ");

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
          {
            _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = AccountInfo.CurrentPlaySessionId;
            using (SqlDataReader _reader = _cmd.ExecuteReader())
            {
              if (!_reader.Read())
              {
                return false;
              }

              AccountInfo.CurrentPlaySessionStatus = (PlaySessionStatus)_reader.GetInt32(0);
              AccountInfo.CurrentPlaySessionBalanceMismatch = _reader.GetBoolean(1);
              AccountInfo.CurrentPlaySessionInitialBalance = _reader.GetDecimal(2) + _reader.GetDecimal(3);
            }
          }
        }

        return true;

      }
      catch (Exception _ex)
      {
        if (AccountId == 0)
        {
          AccountInfo.ErrorMessage = "ExternalTrackData: '" + ExternalTrackData.Trim() + "' --> Exception: " + _ex.Message;
        }
        else
        {
          AccountInfo.ErrorMessage = "AccountId: " + AccountId.ToString() + " --> Exception: " + _ex.Message;
        }
      }

      return false;
    } // Trx_UpdateAccountBalance

    // PURPOSE: Set activity in the account
    //
    //  PARAMS:
    //     - INPUT:
    //           - AccountId:
    //           - Trx:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - none

    public static Boolean Trx_SetActivity(Int64 AccountId,
                                          SqlTransaction Trx)
    {

      StringBuilder _sb;
      _sb = new StringBuilder();
      _sb.AppendLine("UPDATE   ACCOUNTS ");
      _sb.AppendLine("   SET   AC_LAST_ACTIVITY  = GETDATE() ");
      _sb.AppendLine(" WHERE   AC_ACCOUNT_ID  = @pAccountId ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;

          return (_cmd.ExecuteNonQuery() == 1);

        }
      }
      catch
      { ; }

      return false;

    } // Trx_SetActivity

    //------------------------------------------------------------------------------
    // PURPOSE: Mark Account as Blocked indicating the reason why.
    //
    //  PARAMS:
    //      - INPUT:
    //          - AccountID
    //          - BlockReason
    //          - SqlTrx
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //        
    //   NOTES :
    //
    public static Boolean Trx_SetAccountBlocked(Int64 AccountId,
                                                AccountBlockReason BlockReason,
                                                SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      // Don't do try/catch !!!!
      // The routine DB_SetAccountBlocked() needs to do it itself.
      // If you call this routine, do a try/catch.

      _sb = new StringBuilder();
      _sb.AppendLine("UPDATE   ACCOUNTS ");
      _sb.AppendLine("   SET   AC_BLOCKED      = 1 ");
      _sb.AppendLine("       , AC_BLOCK_REASON = @pBlockReason ");
      _sb.AppendLine(" WHERE   AC_ACCOUNT_ID   = @pAccountId ");

      using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
      {
        _cmd.Parameters.Add("@pBlockReason", SqlDbType.Int).Value = BlockReason;
        _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;

        return (_cmd.ExecuteNonQuery() == 1);
      }
    } // Trx_SetAccountBlocked

    public enum InSessionAction
    {
      Unknown = 0,
      StartSession = 1,
      ReStartSession = 2,
      EndSession = 3,
      StartSessionCancelled = 4,
    }

    // PURPOSE: Add a value to fields in session redeemable and not redeemable and update the game meters of the account.
    //
    //  PARAMS:
    //     - INPUT:
    //           - AccountId:
    //           - InSessionReDelta:
    //           - InSessionNrDelta:
    //           - Trx:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - none

    public static Boolean Trx_CancelStartCardSession(Int32 TerminalId,
                                                     Int64 TransactionId,
                                                     out Boolean Cancelled,
                                                     out Int64 ClosedPlaySessionId,
                                                     SqlTransaction Trx)
    {
      StringBuilder _sb;
      Int64 _account_id;
      Int64 _play_session_id;
      String _terminal_name;
      AccountBalance _cancelled;
      AccountBalance _to_gm0;
      AccountBalance _to_gm1;
      AccountBalance _dummy_bal1;
      AccountBalance _dummy_bal2;
      AccountBalance _ini_balance;
      AccountBalance _fin_balance;
      Boolean _rollback;
      PlaySessionStatus _old_status;
      PlaySessionStatus _new_status;
      Boolean _was_first_start_session;

      Cancelled = false;
      ClosedPlaySessionId = 0;
      _was_first_start_session = true;

      _rollback = true;

      try
      {
        Trx.Save("CancelStartCardSession");

        _sb = new StringBuilder();
        _sb.AppendLine(" UPDATE   ACCOUNTS ");

        _sb.AppendLine("    SET   AC_IN_SESSION_CANCELLABLE_TRANSACTION_ID = 0 ");
        _sb.AppendLine("        , AC_IN_SESSION_RE_CANCELLABLE             = 0 ");
        _sb.AppendLine("        , AC_IN_SESSION_PROMO_RE_CANCELLABLE       = 0 ");
        _sb.AppendLine("        , AC_IN_SESSION_PROMO_NR_CANCELLABLE       = 0 ");
        //
        _sb.AppendLine(" OUTPUT   DELETED.AC_ACCOUNT_ID                      ");
        _sb.AppendLine("        , DELETED.AC_CURRENT_PLAY_SESSION_ID         ");
        _sb.AppendLine("        , DELETED.AC_IN_SESSION_RE_CANCELLABLE       ");
        _sb.AppendLine("        , DELETED.AC_IN_SESSION_PROMO_RE_CANCELLABLE ");
        _sb.AppendLine("        , DELETED.AC_IN_SESSION_PROMO_NR_CANCELLABLE ");
        _sb.AppendLine("        , DELETED.AC_IN_SESSION_RE_TO_GM ");
        _sb.AppendLine("        , DELETED.AC_IN_SESSION_PROMO_RE_TO_GM ");
        _sb.AppendLine("        , DELETED.AC_IN_SESSION_PROMO_NR_TO_GM ");
        _sb.AppendLine("        , TE.TE_NAME ");
        //
        _sb.AppendLine("   FROM   ACCOUNTS ");
        _sb.AppendLine("  INNER   JOIN TERMINALS     AS TE ON AC_CURRENT_TERMINAL_ID     = TE.TE_TERMINAL_ID             ");
        _sb.AppendLine("  INNER   JOIN PLAY_SESSIONS AS PS ON AC_CURRENT_PLAY_SESSION_ID = PS.PS_PLAY_SESSION_ID         ");

        _sb.AppendLine("  WHERE   TE.TE_TERMINAL_ID             = @pTerminalId");
        _sb.AppendLine("    AND   TE.TE_CURRENT_PLAY_SESSION_ID = AC_CURRENT_PLAY_SESSION_ID    ");
        _sb.AppendLine("    AND   PS.PS_PLAY_SESSION_ID         = TE.TE_CURRENT_PLAY_SESSION_ID ");
        _sb.AppendLine("    AND   PS.PS_TERMINAL_ID             = @pTerminalId                  ");
        _sb.AppendLine("    AND   AC_ACCOUNT_ID                 = TE.TE_CURRENT_ACCOUNT_ID      ");
        _sb.AppendLine("    AND   AC_CURRENT_TERMINAL_ID        = @pTerminalId                  ");
        _sb.AppendLine("    AND   AC_CURRENT_PLAY_SESSION_ID    = TE.TE_CURRENT_PLAY_SESSION_ID ");

        _sb.AppendLine("    AND   AC_IN_SESSION_CANCELLABLE_TRANSACTION_ID = @pTransactionId ");
        _sb.AppendLine("    AND   AC_CURRENT_PLAY_SESSION_ID IS NOT NULL ");
        _sb.AppendLine("    AND   PS.PS_STATUS = @pStatusOpened ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
          _cmd.Parameters.Add("@pTransactionId", SqlDbType.BigInt).Value = TransactionId;
          _cmd.Parameters.Add("@pStatusOpened", SqlDbType.Int).Value = PlaySessionStatus.Opened;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              // Not Cancellable
              return true;
            }

            _account_id = _reader.GetInt64(0);
            _play_session_id = _reader.GetInt64(1);
            _cancelled = new AccountBalance(_reader.GetDecimal(2), _reader.GetDecimal(3), _reader.GetDecimal(4));
            _to_gm0 = new AccountBalance(_reader.GetDecimal(5), _reader.GetDecimal(6), _reader.GetDecimal(7));
            _terminal_name = _reader.GetString(8);
          }
        }

        if (!Trx_UpdateAccountInSession(InSessionAction.StartSessionCancelled, _account_id, TransactionId, _cancelled,
                                        out _to_gm1, out _dummy_bal1, out _dummy_bal2, Trx))
        {
          return false;
        }

        if (!Trx_UpdateAccountBalance(_account_id, _cancelled, out _fin_balance, Trx))
        {
          return false;
        }
        _ini_balance = _fin_balance - _cancelled;

        _was_first_start_session = (_cancelled == _to_gm0);

        // Decrease 'TotalInitialBalance'
        if (!Trx_BalanceToPlaySession(_was_first_start_session, _play_session_id, AccountBalance.Negate(_cancelled), Trx))
        {
          return false;
        }

        if (_was_first_start_session)
        {
          // Close 
          if (!Trx_PlaySessionChangeStatus(PlaySessionStatus.Cancelled, _play_session_id, out _old_status, out _new_status, Trx))
          {
            return false;
          }

          ClosedPlaySessionId = _play_session_id;

          if (!Trx_UnlinkAccountTerminal(_account_id, TerminalId, _play_session_id, Trx))
          {
            return false;
          }
        }

        if (!Trx_UnlinkPromotionsInSession(_account_id, _play_session_id, TransactionId, Trx))
        {
          return false;
        }

        AccountMovementsTable _am_table;
        _am_table = new AccountMovementsTable(TerminalId, _terminal_name, _play_session_id);
        if (!_am_table.Add(0, _account_id, MovementType.CancelStartSession,
                           _ini_balance.TotalBalance, 0, _cancelled.TotalBalance, _fin_balance.TotalBalance))
        {
          return false;
        }

        if (!_am_table.Save(Trx))
        {
          return false;
        }

        Cancelled = true;
        _rollback = false;

        return true;
      }
      catch
      { ; }
      finally
      {
        if (_rollback)
        {
          try
          {
            Trx.Rollback("CancelStartCardSession");
          }
          catch
          { }
        }
      }

      return false;
    } // Trx_CancelStartCardSession

    public static Boolean Trx_UpdateAccountInSession(InSessionAction Action,
                                                     Int64 AccountId, Int64 TransactionId,
                                                     AccountBalance PlayableBalance,
                                                     out AccountBalance TotalToGMBalance,
                                                     out AccountBalance TotalFromGMBalance,
                                                     out AccountBalance SessionBalance,
                                                     SqlTransaction Trx)
    {
      StringBuilder _sb;

      AccountBalance _cancel;
      AccountBalance _to_gm;
      AccountBalance _from_gm;
      AccountBalance _to_add;
      Int64 _trx_id;

      TotalToGMBalance = AccountBalance.Zero;
      TotalFromGMBalance = AccountBalance.Zero;
      SessionBalance = AccountBalance.Zero;

      // All parts must be greater than 0
      if (PlayableBalance.Redeemable < 0
          || PlayableBalance.PromoRedeemable < 0
          || PlayableBalance.PromoNotRedeemable < 0)
      {
        return false;
      }

      try
      {
        switch (Action)
        {
          case InSessionAction.StartSession:
          case InSessionAction.ReStartSession:
            _to_add = PlayableBalance;
            _to_gm = PlayableBalance;
            _from_gm = AccountBalance.Zero;
            _cancel = PlayableBalance;
            _trx_id = TransactionId;
            break;

          case InSessionAction.EndSession:
            _to_add = AccountBalance.Negate(PlayableBalance);
            _to_gm = AccountBalance.Zero;
            _from_gm = PlayableBalance;
            _cancel = AccountBalance.Zero;
            _trx_id = 0;
            break;

          case InSessionAction.StartSessionCancelled:
            _to_add = AccountBalance.Negate(PlayableBalance);
            _to_gm = AccountBalance.Negate(PlayableBalance);
            _from_gm = AccountBalance.Zero;
            _cancel = AccountBalance.Zero;
            _trx_id = 0;
            break;

          default:
            return false;
        }

        _sb = new StringBuilder();
        _sb.AppendLine(" UPDATE   ACCOUNTS ");

        _sb.AppendLine("    SET   AC_IN_SESSION_RE_BALANCE       = AC_IN_SESSION_RE_BALANCE       + @pRedeemable ");
        _sb.AppendLine("        , AC_IN_SESSION_PROMO_RE_BALANCE = AC_IN_SESSION_PROMO_RE_BALANCE + @pPromoRe ");
        _sb.AppendLine("        , AC_IN_SESSION_PROMO_NR_BALANCE = AC_IN_SESSION_PROMO_NR_BALANCE + @pPromoNr ");
        //
        _sb.AppendLine("        , AC_IN_SESSION_RE_TO_GM         = AC_IN_SESSION_RE_TO_GM         + @pToGmRedeemable ");
        _sb.AppendLine("        , AC_IN_SESSION_PROMO_RE_TO_GM   = AC_IN_SESSION_PROMO_RE_TO_GM   + @pToGmPromoRe ");
        _sb.AppendLine("        , AC_IN_SESSION_PROMO_NR_TO_GM   = AC_IN_SESSION_PROMO_NR_TO_GM   + @pToGmPromoNr ");
        //
        _sb.AppendLine("        , AC_IN_SESSION_RE_FROM_GM       = AC_IN_SESSION_RE_FROM_GM       + @pFromGmRedeemable ");
        _sb.AppendLine("        , AC_IN_SESSION_PROMO_RE_FROM_GM = AC_IN_SESSION_PROMO_RE_FROM_GM + @pFromGmPromoRe ");
        _sb.AppendLine("        , AC_IN_SESSION_PROMO_NR_FROM_GM = AC_IN_SESSION_PROMO_NR_FROM_GM + @pFromGmPromoNr ");
        //
        _sb.AppendLine("        , AC_IN_SESSION_CANCELLABLE_TRANSACTION_ID = @pCancelTransactionId ");
        _sb.AppendLine("        , AC_IN_SESSION_RE_CANCELLABLE             = @pCancelRedeemable ");
        _sb.AppendLine("        , AC_IN_SESSION_PROMO_RE_CANCELLABLE       = @pCancelPromoRe ");
        _sb.AppendLine("        , AC_IN_SESSION_PROMO_NR_CANCELLABLE       = @pCancelPromoNr ");
        //
        _sb.AppendLine(" OUTPUT   INSERTED.AC_IN_SESSION_RE_TO_GM ");
        _sb.AppendLine("        , INSERTED.AC_IN_SESSION_PROMO_RE_TO_GM ");
        _sb.AppendLine("        , INSERTED.AC_IN_SESSION_PROMO_NR_TO_GM ");

        _sb.AppendLine("        , INSERTED.AC_IN_SESSION_RE_FROM_GM ");
        _sb.AppendLine("        , INSERTED.AC_IN_SESSION_PROMO_RE_FROM_GM ");
        _sb.AppendLine("        , INSERTED.AC_IN_SESSION_PROMO_NR_FROM_GM ");

        _sb.AppendLine("        , INSERTED.AC_IN_SESSION_RE_BALANCE ");
        _sb.AppendLine("        , INSERTED.AC_IN_SESSION_PROMO_RE_BALANCE ");
        _sb.AppendLine("        , INSERTED.AC_IN_SESSION_PROMO_NR_BALANCE ");

        _sb.AppendLine("   FROM   ACCOUNTS ");
        _sb.AppendLine("  WHERE   AC_ACCOUNT_ID = @pAccountId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;

          _cmd.Parameters.Add("@pRedeemable", SqlDbType.Decimal).Value = _to_add.Redeemable;
          _cmd.Parameters.Add("@pPromoRe", SqlDbType.Decimal).Value = _to_add.PromoRedeemable;
          _cmd.Parameters.Add("@pPromoNr", SqlDbType.Decimal).Value = _to_add.PromoNotRedeemable;

          _cmd.Parameters.Add("@pToGmRedeemable", SqlDbType.Decimal).Value = _to_gm.Redeemable;
          _cmd.Parameters.Add("@pToGmPromoRe", SqlDbType.Decimal).Value = _to_gm.PromoRedeemable;
          _cmd.Parameters.Add("@pToGmPromoNr", SqlDbType.Decimal).Value = _to_gm.PromoNotRedeemable;

          _cmd.Parameters.Add("@pFromGmRedeemable", SqlDbType.Decimal).Value = _from_gm.Redeemable;
          _cmd.Parameters.Add("@pFromGmPromoRe", SqlDbType.Decimal).Value = _from_gm.PromoRedeemable;
          _cmd.Parameters.Add("@pFromGmPromoNr", SqlDbType.Decimal).Value = _from_gm.PromoNotRedeemable;

          _cmd.Parameters.Add("@pCancelTransactionId", SqlDbType.BigInt).Value = _trx_id;
          _cmd.Parameters.Add("@pCancelRedeemable", SqlDbType.Decimal).Value = _cancel.Redeemable;
          _cmd.Parameters.Add("@pCancelPromoRe", SqlDbType.Decimal).Value = _cancel.PromoRedeemable;
          _cmd.Parameters.Add("@pCancelPromoNr", SqlDbType.Decimal).Value = _cancel.PromoNotRedeemable;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              return false;
            }

            TotalToGMBalance = new AccountBalance(_reader.GetDecimal(0), _reader.GetDecimal(1), _reader.GetDecimal(2));
            TotalFromGMBalance = new AccountBalance(_reader.GetDecimal(3), _reader.GetDecimal(4), _reader.GetDecimal(5));
            SessionBalance = new AccountBalance(_reader.GetDecimal(6), _reader.GetDecimal(7), _reader.GetDecimal(8));

            return true;
          }
        }
      }
      catch
      { ; }

      return false;

    } // Trx_UpdateAccountInSessionBalance

    // PURPOSE: Update the initial balance or cash in of the play sessions.
    //
    //  PARAMS:
    //     - INPUT:
    //           - PlaySessionId:
    //           - Redeemable:
    //           - NotRedeemable:
    //           - Trx:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - none

    public static Boolean Trx_BalanceToPlaySession(Boolean IsFirstTransfer,
                                                   Int64 PlaySessionId,
                                                   AccountBalance PlayableBalance,
                                                   SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("  UPDATE   PLAY_SESSIONS ");
        _sb.AppendLine("     SET   PS_INITIAL_BALANCE = CASE WHEN (@pIsFirstTransfer = 1) THEN PS_INITIAL_BALANCE + @pPlayableBalance ELSE PS_INITIAL_BALANCE END ");
        _sb.AppendLine(" 	       , PS_CASH_IN         = CASE WHEN (@pIsFirstTransfer = 1) THEN 0                                      ELSE PS_CASH_IN + @pPlayableBalance END ");
        _sb.AppendLine("   WHERE   PS_PLAY_SESSION_ID = @pPlaySessionId  ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pIsFirstTransfer", SqlDbType.Bit).Value = IsFirstTransfer;
          _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = PlaySessionId;
          _cmd.Parameters.Add("@pPlayableBalance", SqlDbType.Decimal).Value = PlayableBalance.TotalBalance;


          return (_cmd.ExecuteNonQuery() == 1);
        }

      }
      catch
      { ; }

      return false;

    } // Trx_BalanceToPlaySession

    //------------------------------------------------------------------------------
    // PURPOSE :  Calculate Award Points for account
    //
    //  PARAMS :
    //      - INPUT :
    //          - AccountId
    //          - TerminalId
    //          - InSessionRePlayed
    //          - InSessionReSpent
    //          - InSessionPlayed
    //          - Trx
    //
    //      - OUTPUT :
    //          - TotalPoints
    //          - GenerateMovement
    // RETURNS :
    //      - True:  If Points have been computed correctly
    //      - False: Otherwhise.
    //

    public static Boolean Trx_ComputeWonPoints(Int64 AccountId,
                                               Terminal.TerminalInfo TerminalInfo,
                                               Decimal InSessionReSpent,
                                               Decimal InSessionRePlayed,
                                               Decimal InSessionPlayed,
                                               out Decimal WonPoints,
                                               out Boolean PointHaveToBeAwarded,
                                               SqlTransaction Trx)
    {
      Decimal _redeemable_spent_to_1_point;
      Decimal _redeemable_played_to_1_point;
      Decimal _total_played_to_1_point;
      StringBuilder _sb;
      Int32 _holder_level;

      PointHaveToBeAwarded = false;
      WonPoints = 0;

      // Get holder level
      _sb = new StringBuilder();
      _sb.AppendLine(" SELECT   ISNULL (AC_HOLDER_LEVEL, 0) ");
      _sb.AppendLine("   FROM   ACCOUNTS ");
      _sb.AppendLine("  WHERE   AC_ACCOUNT_ID = @pAccountId ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;

          _holder_level = (Int32)_cmd.ExecuteScalar();
        }

        if (_holder_level == 0)
        {
          // Is anonymous
          return true;
        }

        _sb.Length = 0;
        _sb.AppendLine(" SELECT   GP_KEY_VALUE ");
        _sb.AppendLine("   FROM   GENERAL_PARAMS ");
        _sb.AppendLine("  WHERE   GP_GROUP_KEY  = 'PlayerTracking' ");
        _sb.AppendLine("    AND   GP_SUBJECT_KEY = 'LevelXX.RedeemablePlayedTo1Point'");
        _sb.AppendLine(" ;");
        _sb.AppendLine(" SELECT   GP_KEY_VALUE ");
        _sb.AppendLine("   FROM   GENERAL_PARAMS ");
        _sb.AppendLine("  WHERE   GP_GROUP_KEY  = 'PlayerTracking' ");
        _sb.AppendLine("    AND   GP_SUBJECT_KEY = 'LevelXX.RedeemableSpentTo1Point'");
        _sb.AppendLine(" ;");
        _sb.AppendLine(" SELECT   GP_KEY_VALUE ");
        _sb.AppendLine("   FROM   GENERAL_PARAMS ");
        _sb.AppendLine("  WHERE   GP_GROUP_KEY  = 'PlayerTracking' ");
        _sb.AppendLine("    AND   GP_SUBJECT_KEY = 'LevelXX.TotalPlayedTo1Point' ");
        _sb.AppendLine(" ;");

        _sb.Replace("XX", _holder_level.ToString("00"));

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            // Read Points and Points multiplier
            if (!_reader.Read())
            {
              return false;
            }
            _redeemable_played_to_1_point = Convert.ToDecimal(_reader.GetString(0));

            if (!_reader.NextResult())
            {
              return false;
            }
            if (!_reader.Read())
            {
              return false;
            }
            _redeemable_spent_to_1_point = Convert.ToDecimal(_reader.GetString(0));

            if (!_reader.NextResult())
            {
              return false;
            }
            if (!_reader.Read())
            {
              return false;
            }
            _total_played_to_1_point = Convert.ToDecimal(_reader.GetString(0));

          }
        }

        if (TerminalInfo.PvPointsMultiplier == 0)
        {
          // Provider doesn't generate points
          return true;
        }

        _redeemable_played_to_1_point = Math.Max(_redeemable_played_to_1_point, 0);
        _redeemable_spent_to_1_point = Math.Max(_redeemable_spent_to_1_point, 0);
        _total_played_to_1_point = Math.Max(_total_played_to_1_point, 0);

        // Compute points
        if (_redeemable_played_to_1_point > 0)
        {
          PointHaveToBeAwarded = true;
          WonPoints += Math.Max(0, InSessionRePlayed) / _redeemable_played_to_1_point;
        }
        if (_redeemable_spent_to_1_point > 0)
        {
          PointHaveToBeAwarded = true;
          WonPoints += Math.Max(0, InSessionReSpent) / _redeemable_spent_to_1_point;
        }
        if (_total_played_to_1_point > 0)
        {
          PointHaveToBeAwarded = true;
          WonPoints += Math.Max(0, InSessionPlayed) / _total_played_to_1_point;
        }

        WonPoints = PointHaveToBeAwarded ? Math.Round(WonPoints * TerminalInfo.PvPointsMultiplier, 4) : 0;

        return true;
      }
      catch
      { ;}

      return false;
    }

    //////////public static Boolean Trx_GetAccountInfo(Int64 AccountId, out AccountInfo AccountInfo, SqlTransaction Trx)
    //////////{
    //////////  StringBuilder _sb;

    //////////  AccountInfo = new AccountInfo();
    //////////  _sb = new StringBuilder();

    //////////  try
    //////////  {
    //////////    _sb.AppendLine(" SELECT   AC_ACCOUNT_ID              ");
    //////////    _sb.AppendLine("        , AC_BLOCKED                 ");
    //////////    _sb.AppendLine("        , ISNULL(AC_HOLDER_NAME, '') ");
    //////////    _sb.AppendLine("        , AC_HOLDER_LEVEL            ");
    //////////    _sb.AppendLine("        , AC_HOLDER_GENDER           ");
    //////////    _sb.AppendLine("   FROM   ACCOUNTS ");
    //////////    _sb.AppendLine("  WHERE   AC_ACCOUNT_ID = @pAccountId ");

    //////////    using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
    //////////    {
    //////////      _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;

    //////////      using (SqlDataReader _reader = _cmd.ExecuteReader())
    //////////      {
    //////////        if (!_reader.Read())
    //////////        {
    //////////          return false;
    //////////        }

    //////////        AccountInfo.AccountId = _reader.GetInt64(0);
    //////////        AccountInfo.Blocked = _reader.GetBoolean(1);
    //////////        AccountInfo.HolderName = _reader.GetString(2);
    //////////        AccountInfo.HolderLevel = _reader.GetInt32(3);
    //////////        AccountInfo.HolderGender = _reader.GetInt32(4);

    //////////      }
    //////////    }

    //////////    return true;
    //////////  }
    //////////  catch
    //////////  { ; }

    //////////  return false;
    //////////} // Trx_GetAccountInfo

    //////////public static Boolean Trx_GetAccountInfo(String ExternalTrackData, out AccountInfo AccountInfo, SqlTransaction Trx)
    //////////{
    //////////  String _internal_track_data;
    //////////  Int32 _card_type;
    //////////  Object _obj;
    //////////  Int64 _account_id;

    //////////  AccountInfo = new AccountInfo();

    //////////  if (!CardNumber.TrackDataToCardNumber(ExternalTrackData, out _internal_track_data, out _card_type))
    //////////  {
    //////////    return false;
    //////////  }

    //////////  try
    //////////  {
    //////////    using (SqlCommand _cmd = new SqlCommand("SELECT AC_ACCOUNT_ID FROM ACCOUNTS WHERE AC_TRACK_DATA = @pInternal", Trx.Connection, Trx))
    //////////    {
    //////////      _cmd.Parameters.Add("@pInternal", SqlDbType.NVarChar, 50).Value = _internal_track_data.Trim();
    //////////      _obj = _cmd.ExecuteScalar();

    //////////      if (_obj == null || _obj == DBNull.Value)
    //////////      {
    //////////        return false;
    //////////      }

    //////////      _account_id = (Int64)_obj;
    //////////    }

    //////////    return Trx_GetAccountInfo(_account_id, out AccountInfo, Trx);
    //////////  }
    //////////  catch
    //////////  { ; }

    //////////  return false;
    //////////}

    #endregion

    #region Private Methods

    // PURPOSE: Compute not redemeeble, delta won and delta won not redeemable and update the new balance and won.
    //
    //  PARAMS:
    //     - INPUT:
    //           - Params:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - none

    private static void WonPromoNotRedeemable(InSessionParameters Params, Boolean SimulateWonLock)
    {
      Decimal _promo_delta_won;

      foreach (DataRow _promo in Params.AccountPromotion.Rows)
      {
        if (Params.DeltaWon <= 0)
        {
          break;
        }

        if ((ACCOUNT_PROMO_CREDIT_TYPE)_promo["ACP_CREDIT_TYPE"] != ACCOUNT_PROMO_CREDIT_TYPE.NR1)
        {
          continue;
        }

        if (!_promo.IsNull("ACP_WONLOCK") || SimulateWonLock)
        {
          _promo_delta_won = Params.DeltaWon;
        }
        else
        {
          _promo_delta_won = (Decimal)_promo["ACP_WITHHOLD"] - Math.Min((Decimal)_promo["ACP_BALANCE"], (Decimal)_promo["ACP_WITHHOLD"]);
          _promo_delta_won = Math.Min(_promo_delta_won, Params.DeltaWon);
        }

        if (_promo_delta_won <= 0)
        {
          continue;
        }

        _promo["ACP_BALANCE"] = (Decimal)_promo["ACP_BALANCE"] + _promo_delta_won;
        _promo["ACP_WON"] = (Decimal)_promo["ACP_WON"] + _promo_delta_won;
        Params.DeltaWon -= _promo_delta_won;
        Params.DeltaWonNr += _promo_delta_won;
        Params.Balance.PromoNotRedeemable += _promo_delta_won;

      }
    } // WonPromoNotRedeemable

    // PURPOSE: Comouthe redeemable, delta won and delta won redeemble.
    //
    //  PARAMS:
    //     - INPUT:
    //           - Params:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - none

    private static void WonRedeemable(InSessionParameters Params)
    {
      Decimal _promo_delta_won;

      if (Params.DeltaWon <= 0)
      {
        return;
      }

      _promo_delta_won = Params.DeltaWon;
      Params.Balance.Redeemable += _promo_delta_won;
      Params.DeltaWonRe += _promo_delta_won;
      Params.DeltaWon -= _promo_delta_won;
    } // WonRedeemable

    // PURPOSE: Compute not redeemable, delta played not redeemable and delta played. Update the balance and played.
    //                                                                               
    //  PARAMS:
    //     - INPUT:
    //           - CreditType:
    //           - Params:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - none

    private static void PlayPromoNotRedeemableBalance(ACCOUNT_PROMO_CREDIT_TYPE CreditType, InSessionParameters Params)
    {
      Decimal _promo_delta_played;

      foreach (DataRow _promo in Params.AccountPromotion.Rows)
      {
        if (Params.DeltaPlayed <= 0)
        {
          break;
        }

        if ((ACCOUNT_PROMO_CREDIT_TYPE)_promo["ACP_CREDIT_TYPE"] != CreditType)
        {
          continue;
        }

        _promo_delta_played = Math.Min((Decimal)_promo["ACP_BALANCE"], Params.DeltaPlayed);
        _promo_delta_played = Math.Min(_promo_delta_played, Params.Balance.PromoNotRedeemable);

        if (_promo_delta_played <= 0)
        {
          continue;
        }

        _promo["ACP_BALANCE"] = (Decimal)_promo["ACP_BALANCE"] - _promo_delta_played;
        _promo["ACP_PLAYED"] = (Decimal)_promo["ACP_PLAYED"] + _promo_delta_played;
        Params.DeltaPlayedNr += _promo_delta_played;
        Params.DeltaPlayed -= _promo_delta_played;
        Params.Balance.PromoNotRedeemable -= _promo_delta_played;
      }
    } // PlayPromoBalance

    // PURPOSE: Compute the delta played not redeemed and played. Update the new balance and played.
    //
    //  PARAMS:
    //     - INPUT:
    //           - CreditType: NR1 or NR1
    //           - Params: 
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - none

    private static void PlayPromoPrizeOnly(ACCOUNT_PROMO_CREDIT_TYPE CreditType, InSessionParameters Params)
    {
      Decimal _promo_delta_played;
      Decimal _promo_prize;

      foreach (DataRow _promo in Params.AccountPromotion.Rows)
      {
        if (Params.DeltaPlayed <= 0)
        {
          break;
        }

        if ((ACCOUNT_PROMO_CREDIT_TYPE)_promo["ACP_CREDIT_TYPE"] != CreditType)
        {
          continue;
        }

        if (_promo.IsNull("ACP_WONLOCK"))
        {
          continue;
        }

        _promo_prize = Math.Max(0, (Decimal)_promo["ACP_BALANCE"] - (Decimal)_promo["ACP_WITHHOLD"]);
        _promo_delta_played = Math.Min(Params.DeltaPlayed, _promo_prize);
        _promo_delta_played = Math.Min(Params.Balance.PromoNotRedeemable, _promo_delta_played);

        if (_promo_delta_played <= 0)
        {
          continue;
        }

        _promo["ACP_BALANCE"] = (Decimal)_promo["ACP_BALANCE"] - _promo_delta_played;
        _promo["ACP_PLAYED"] = (Decimal)_promo["ACP_PLAYED"] + _promo_delta_played;
        Params.DeltaPlayedNr += _promo_delta_played;
        Params.DeltaPlayed -= _promo_delta_played;
        Params.Balance.PromoNotRedeemable -= _promo_delta_played;
      }

    } // PlayPromoPrizeOnly

    // PURPOSE: Compute the redeemable played and played.
    //
    //  PARAMS:
    //     - INPUT:
    //           - Params:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - none

    private static void PlayRedeemable(InSessionParameters Params)
    {
      Decimal _delta_played;


      _delta_played = Math.Min(Params.Balance.PromoRedeemable, Params.DeltaPlayed);
      if (_delta_played > 0)
      {
        Params.Balance.PromoRedeemable -= _delta_played;
        Params.DeltaPlayedRe += _delta_played;
        Params.DeltaPlayed -= _delta_played;
      }

      _delta_played = Math.Min(Params.Balance.Redeemable, Params.DeltaPlayed);
      if (_delta_played > 0)
      {
        Params.Balance.Redeemable -= _delta_played;
        Params.DeltaPlayedRe += _delta_played;
        Params.DeltaPlayed -= _delta_played;
      }

    } // PlayRedeemable

    //------------------------------------------------------------------------------
    // PURPOSE: Closes a Play Session
    //
    //  PARAMS:
    //      - INPUT:
    //          - PlaySessionId
    //          - AccountID
    //          - FinalBalance
    //          - IsPromotion
    //          - Trx
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //      - Nothing
    //        
    //   NOTES :
    //
    private static Boolean Trx_PlaySessionChangeStatus(PlaySessionStatus DesiredStatus, Int64 PlaySessionId,
                                                       out PlaySessionStatus OldStatus, out PlaySessionStatus NewStatus, SqlTransaction Trx)
    {
      StringBuilder _sb;

      OldStatus = PlaySessionStatus.Opened;
      NewStatus = PlaySessionStatus.Opened;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("UPDATE   PLAY_SESSIONS ");
        _sb.AppendLine("   SET   PS_STATUS          = CASE WHEN (PS_STATUS = @pStatusOpened) THEN @pNewStatus ELSE PS_STATUS   END ");
        _sb.AppendLine("       , PS_LOCKED          = CASE WHEN (PS_STATUS = @pStatusOpened) THEN NULL        ELSE PS_LOCKED   END ");
        _sb.AppendLine("       , PS_FINISHED        = CASE WHEN (PS_STATUS = @pStatusOpened) THEN GETDATE()   ELSE PS_FINISHED END ");
        _sb.AppendLine("OUTPUT   DELETED.PS_STATUS ");
        _sb.AppendLine("       , INSERTED.PS_STATUS ");
        _sb.AppendLine(" WHERE   PS_PLAY_SESSION_ID = @pPlaySessionId");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = PlaySessionId;
          _cmd.Parameters.Add("@pStatusOpened", SqlDbType.Int).Value = PlaySessionStatus.Opened;
          _cmd.Parameters.Add("@pNewStatus", SqlDbType.Int).Value = DesiredStatus;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              return false;
            }
            OldStatus = (PlaySessionStatus)_reader.GetInt32(0);
            NewStatus = (PlaySessionStatus)_reader.GetInt32(1);

            return true;
          }
        }
      }
      catch
      {
      }

      return false;
    } // Trx_PlaySessionChangeStatus

    //------------------------------------------------------------------------------
    // PURPOSE : Set ps_balance_mismatch flag and ps_final_balance into play session 
    //
    //  PARAMS :
    //      - INPUT :
    //          - PlaySessionId
    //          - EfectiveFinalBalance
    //          - ReportedFinalBalance
    //          - BalanceMismatch
    //          - MovementType
    //          - Trx
    //
    //      - OUTPUT :
    //          - None
    //
    // RETURNS :
    //      - True: Saved ok. False: Otherwise.
    //
    public static Boolean Trx_PlaySessionSetFinalBalance(Int64 PlaySessionId,
                                                          Decimal EfectiveFinalBalance,
                                                          Decimal ReportedFinalBalance,
                                                          Boolean BalanceMismatch,
                                                          MovementType MovementType,
                                                          SqlTransaction Trx)
    {
      String _sql_txt;
      // if CreditBalance and CreditBalanceReported is negative we assign 0.
      EfectiveFinalBalance = Math.Max(0, EfectiveFinalBalance);
      ReportedFinalBalance = Math.Max(0, ReportedFinalBalance);

      // Add the BalanceMismatch and CreditBalance to the play session       
      _sql_txt = " UPDATE   PLAY_SESSIONS";
      _sql_txt += "   SET   PS_REPORTED_BALANCE_MISMATCH = CASE WHEN (@pBalanceMismatch = 1) THEN @pReportedBalance ELSE PS_REPORTED_BALANCE_MISMATCH END ";
      _sql_txt += "       , PS_FINAL_BALANCE             = @pFinalBalance ";
      _sql_txt += "       , PS_BALANCE_MISMATCH          = @pBalanceMismatch ";
      _sql_txt += " WHERE   PS_PLAY_SESSION_ID           = @pPlaySessionID ";
      // Only force PS_STATUS = OPENED when it is a EndCardSession from the Game Machine.
      if (MovementType == MovementType.EndCardSession)
      {
        _sql_txt += "   AND   PS_STATUS                    = @pStatusOpened ";
      }

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sql_txt, Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pPlaySessionID", SqlDbType.BigInt).Value = PlaySessionId;
          _cmd.Parameters.Add("@pFinalBalance", SqlDbType.Money).Value = EfectiveFinalBalance;
          _cmd.Parameters.Add("@pReportedBalance", SqlDbType.Money).Value = ReportedFinalBalance;
          _cmd.Parameters.Add("@pBalanceMismatch", SqlDbType.Bit).Value = BalanceMismatch;
          _cmd.Parameters.Add("@pStatusOpened", SqlDbType.Int).Value = PlaySessionStatus.Opened;

          return (_cmd.ExecuteNonQuery() == 1);
        }
      }
      catch
      { }

      return false;
    } // Trx_PlaySessionSetFinalBalance

    public static String ReadGeneralParams(String Group, String Subject, SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   GP_KEY_VALUE ");
        _sb.AppendLine("   FROM   GENERAL_PARAMS ");
        _sb.AppendLine("  WHERE   GP_GROUP_KEY    = @pGroup ");
        _sb.AppendLine("    AND   GP_SUBJECT_KEY  = @pSubject ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pGroup", SqlDbType.NVarChar, 50).Value = Group;
          _cmd.Parameters.Add("@pSubject", SqlDbType.NVarChar, 50).Value = Subject;

          return (String)_cmd.ExecuteScalar();
        }
      }
      catch
      {
        return "";
      }
    } // ReadGeneralParams

    private static Boolean Trx_PlaySessionMarkAsAbandoned(Int64 PlaySessionId, StringBuilder ErrorMsg, SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("   UPDATE   PLAY_SESSIONS                          ");
        _sb.AppendLine("      SET   PS_STATUS          = @pStatusAbandoned ");
        _sb.AppendLine("          , PS_FINISHED        = GETDATE()         ");
        _sb.AppendLine("    WHERE   PS_PLAY_SESSION_ID = @pPlaySessionId   ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pStatusAbandoned", SqlDbType.Int).Value = PlaySessionStatus.Abandoned;
          _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = PlaySessionId;

          return (_cmd.ExecuteNonQuery() == 1);
        }
      }
      catch (Exception _ex)
      {
        ErrorMsg.AppendLine(_ex.Message);
      }

      return false;
    } // Trx_PlaySessionMarkAsAbandoned

    public static Boolean Trx_ReleaseInactivePlaySession(Int64 PlaySessionId, Decimal FinalBalance, Int64 AccountId,
                                                         Int32 TerminalId, TerminalTypes TerminalType, String TerminalName,
                                                         String SystemCashierName,
                                                         out StringBuilder ErrorMsg, SqlTransaction Trx)
    {
      ErrorMsg = new StringBuilder();
      EndSessionInput _end_input;
      EndSessionOutput _end_output;

      try
      {
        _end_input = new MultiPromos.EndSessionInput();
        _end_input.MovementType = MovementType.ManualEndSession;
        _end_input.AccountId = AccountId;
        _end_input.CashierName = SystemCashierName;
        _end_input.BalanceFromGM = FinalBalance;
        _end_input.HasMeters = true;
        _end_input.PlaySessionId = PlaySessionId;
        _end_input.TransactionId = 0;

        if (!MultiPromos.Trx_OnEndCardSession(TerminalId, TerminalType, TerminalName, _end_input, out _end_output, Trx))
        {
          ErrorMsg.AppendLine(_end_output.Message);
          ErrorMsg.AppendLine("Trx_OnEndCardSession failed.");

          return false;
        }

        if (!MultiPromos.Trx_PlaySessionMarkAsAbandoned(PlaySessionId, ErrorMsg, Trx))
        {
          ErrorMsg.AppendLine("Trx_PlaySessionMarkAsAbandoned failed.");

          return false;
        }

        if (!MultiPromos.Trx_SetAccountBlocked(AccountId, AccountBlockReason.ABANDONED_CARD, Trx))
        {
          ErrorMsg.AppendLine("Trx_SetAccountBlocked failed.");

          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        ErrorMsg.AppendLine(_ex.Message);
      }

      return false;
    } // Trx_ReleaseInactivePlaySession


    //------------------------------------------------------------------------------
    // PURPOSE : Set, Add and Reset initial redeemable promotion in ACCOUNTS
    //
    //  PARAMS :
    //      - INPUT:
    //            - AccountId
    //            - Action 
    //            - Amount
    //            - SqlTrx
    //
    //      - OUTPUT :
    //            - None
    //
    // RETURNS :
    //      -  Updated ok. False: Otherwise. 
    public static Boolean Trx_AccountPromotionCost(Int64 AccountId,
                                                   ACCOUNTS_PROMO_COST Action,
                                                   Decimal Amount,
                                                   SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      Int32 _num_active_nr_promos;

      try
      {
        _sb = new StringBuilder();

        switch (Action)
        {
          case ACCOUNTS_PROMO_COST.UPDATE_ON_RECHARGE:
          case ACCOUNTS_PROMO_COST.UPDATE_ON_REDEEM:
            if (Amount == 0)
            {
              return true;
            }

            // ADD AMOUNT recharge
            _sb.Length = 0;
            _sb.AppendLine("UPDATE   ACCOUNTS ");
            _sb.AppendLine("   SET   AC_PROMO_INI_RE_BALANCE = CASE WHEN ( AC_PROMO_INI_RE_BALANCE IS NOT NULL ) ");
            _sb.AppendLine("                              THEN CASE WHEN ( AC_PROMO_INI_RE_BALANCE + @pAmount >= 0) ");
            _sb.AppendLine("                              THEN AC_PROMO_INI_RE_BALANCE + @pAmount ");
            _sb.AppendLine("                              ELSE 0 END ELSE NULL END ");
            _sb.AppendLine(" WHERE   AC_ACCOUNT_ID = @pAccountId ");

            using (SqlCommand _sql_command = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
            {
              _sql_command.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
              _sql_command.Parameters.Add("@pAmount", SqlDbType.Money).Value = (Action == ACCOUNTS_PROMO_COST.UPDATE_ON_RECHARGE) ? Amount : - Amount;

              return (_sql_command.ExecuteNonQuery() == 1);
            }

          case ACCOUNTS_PROMO_COST.RESET_ON_REDEEM:
          case ACCOUNTS_PROMO_COST.SET_ON_FIRST_NR_PROMOTION:

            _sb.Length = 0;
            _sb.AppendLine(" SELECT   COUNT(1) ");
            _sb.AppendLine("   FROM   ACCOUNT_PROMOTIONS ");
            _sb.AppendLine("  WHERE   ACP_ACCOUNT_ID  = @pAccountId");
            _sb.AppendLine("    AND   ACP_STATUS      = @pStatusActive");
            _sb.AppendLine("    AND   ACP_CREDIT_TYPE IN (@pNR1,@pNR2)");

            using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
            {
              _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
              _sql_cmd.Parameters.Add("@pNR1", SqlDbType.Int).Value = ACCOUNT_PROMO_CREDIT_TYPE.NR1;
              _sql_cmd.Parameters.Add("@pNR2", SqlDbType.Int).Value = ACCOUNT_PROMO_CREDIT_TYPE.NR2;
              _sql_cmd.Parameters.Add("@pStatusActive", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.ACTIVE;

              _num_active_nr_promos = (Int32)_sql_cmd.ExecuteScalar();
            }

            _sb.Length = 0;
            _sb.AppendLine("UPDATE   ACCOUNTS ");
            _sb.AppendLine("   SET   AC_PROMO_INI_RE_BALANCE    = @pAmount");
            _sb.AppendLine(" WHERE   AC_ACCOUNT_ID = @pAccountId ");
            using (SqlCommand _sql_command = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
            {
              _sql_command.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
              _sql_command.Parameters.Add("@pAmount", SqlDbType.Money);

              if (Action == ACCOUNTS_PROMO_COST.RESET_ON_REDEEM)
              {
                if (_num_active_nr_promos == 0)
                {
                  _sql_command.Parameters["@pAmount"].Value = DBNull.Value;
                }
                else
                {
                  _sql_command.Parameters["@pAmount"].Value = Amount;
                }
              }
              else if (_num_active_nr_promos == 1 && Action == ACCOUNTS_PROMO_COST.SET_ON_FIRST_NR_PROMOTION)
              {
                _sql_command.Parameters["@pAmount"].Value = Amount;
              }
              else
              {
                // Nothing to do
                return true;
              }

              return (_sql_command.ExecuteNonQuery() == 1);
            }

          default:
            return false;
        }
       }
      catch { ; }

      return false;
    }

    #endregion

  }

}

