//-------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//-------------------------------------------------------------------
//
// MODULE NAME:   SharedAccountMovements.cs
// DESCRIPTION:   
// AUTHOR:        Jes�s �ngel Blanco
// CREATION DATE: 15-JUN-2012
//
// REVISION HISTORY:
//
// Date         Author Description
// -----------  ------ -----------------------------------------------
// 15-JUN-2012  JAB    Initial version.
// -------------------------------------------------------------------
//
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace WSI.Common
{
  public class AccountMovementsTest
  {
    public void Test1(CashierSessionInfo CashierSessionInfo, SqlTransaction SqlTrx)
    {
      AccountMovementsTable _table;
      MovementType _movement_type;

      _table = new AccountMovementsTable(CashierSessionInfo);

      _movement_type = MovementType.AccountPersonalization;

      //_table.Add(1, 2, _movement_type, 4, 5, 6, 7);
      //_table.Add(2, 3, _movement_type, 6, 3, 8, 9);
      //_table.Add(3, 7, _movement_type, 1, 1, 2, 1);
      //_table.Add(6, 4, _movement_type, 4, 2, 7, 9);
      //_table.Add(4, 8, _movement_type, 0, 3, 0, 0);
      _table.Add(1, 915, _movement_type, 101, 0, 0, 101);
      _table.Add(2, 915, _movement_type, 202, 202, 0, 0);

      _table.Save(SqlTrx);
    }

    public void Test2(Int32 TerminalId, String TerminalName, Int64 PlaySessionId, SqlTransaction SqlTrx)
    {
      AccountMovementsTable _table;
      MovementType _movement_type;

      _table = new AccountMovementsTable(TerminalId, TerminalName, PlaySessionId);

      _movement_type = MovementType.StartCardSession;

      //_table.Add(1, 2, _movement_type, 4, 5, 6, 7);
      //_table.Add(2, 3, _movement_type, 6, 3, 8, 9);
      //_table.Add(3, 7, _movement_type, 1, 1, 2, 1);
      //_table.Add(6, 4, _movement_type, 4, 2, 7, 9);
      //_table.Add(4, 8, _movement_type, 0, 3, 0, 0);
      _table.Add(3, 915, _movement_type, 303, 0, 0, 303);
      _table.Add(4, 915, _movement_type, 404, 404, 0, 0);

      _table.Save(SqlTrx);
    }

  } // AccountMovementsTest


  public class AccountMovementsTable
  {
    private DataTable m_table = null;

    private CashierSessionInfo m_cashier_session_info = null;

    private Int32 m_terminal_id = 0;
    private String m_terminal_name = "";
    private Int64 m_play_session_id = 0;

    // For now, used for manual end sessions. Indicates the Cashier or GUI terminal name and user.
    private String m_cashier_name = "";

    //------------------------------------------------------------------------------
    // PURPOSE : Default constructor.
    //
    //  PARAMS :
    //      - INPUT : 
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public void InitSchema()
    {
      m_table = new DataTable();
      m_table.Columns.Add("AM_OPERATION_ID", Type.GetType("System.Int64"));
      m_table.Columns.Add("AM_ACCOUNT_ID", Type.GetType("System.Int64")).AllowDBNull = false;

      m_table.Columns.Add("AM_TYPE", Type.GetType("System.Int32")).AllowDBNull = false;
      m_table.Columns.Add("AM_INITIAL_BALANCE", Type.GetType("System.Decimal")).AllowDBNull = false;
      m_table.Columns.Add("AM_SUB_AMOUNT", Type.GetType("System.Decimal")).AllowDBNull = false;
      m_table.Columns.Add("AM_ADD_AMOUNT", Type.GetType("System.Decimal")).AllowDBNull = false;
      m_table.Columns.Add("AM_FINAL_BALANCE", Type.GetType("System.Decimal")).AllowDBNull = false;

      m_table.Columns.Add("AM_DETAILS", Type.GetType("System.String")).AllowDBNull = true;

    } // InitSchema

    public AccountMovementsTable(CashierSessionInfo CashierSessionInfo)
    {
      InitSchema();

      m_cashier_session_info = CashierSessionInfo.Copy();

      m_terminal_id = 0;
      m_terminal_name = "";
      m_play_session_id = 0;

      m_cashier_name = "";

    } // AccountMovementsTable

    public AccountMovementsTable(Int32 TerminalId, String TerminalName, Int64 PlaySessionId)
      : this(TerminalId, TerminalName, PlaySessionId, "")
    {

    } // AccountMovementsTable

    public AccountMovementsTable(Int32 TerminalId, String TerminalName, Int64 PlaySessionId, String CashierName)
    {
      InitSchema();

      m_cashier_session_info = null;

      m_terminal_id = TerminalId;
      m_terminal_name = TerminalName;
      m_play_session_id = PlaySessionId;

      m_cashier_name = CashierName;

    } // AccountMovementsTable

    //------------------------------------------------------------------------------
    // PURPOSE : Adds a new record.
    //
    //  PARAMS :
    //      - INPUT : 
    //             - OperationId:                     AM_OPERATION_ID
    //             - AccountId:                       AM_ACCOUNT_ID
    //             - Type:                            AM_TYPE
    //             - InitialBalance:                  AM_INITIAL_BALANCE
    //             - SubAmount:                       AM_SUB_AMOUNT 
    //             - AddAmount:                       AM_ADD_AMOUNT 
    //             - FinalBalance:                    AM_FINAL_BALANCE 
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True: The record has been successfully added.
    //      - False: The record has not been successfully added.
    //
    public Boolean Add(Int64 OperationId,
                   Int64 AccountId,
                   MovementType Type,
                   Decimal InitialBalance,
                   Decimal SubAmount,
                   Decimal AddAmount,
                   Decimal FinalBalance)
    {
      return Add(OperationId, AccountId, Type, InitialBalance, SubAmount, AddAmount, FinalBalance, "");
    }

    public Boolean Add(Int64 OperationId,
                       Int64 AccountId,
                       MovementType Type,
                       Decimal InitialBalance,
                       Decimal SubAmount,
                       Decimal AddAmount,
                       Decimal FinalBalance,
                       String Details)
    {
      DataRow _row;

      try
      {
        _row = m_table.NewRow();
        _row["AM_OPERATION_ID"] = OperationId;
        _row["AM_ACCOUNT_ID"] = AccountId;
        _row["AM_TYPE"] = Type;
        _row["AM_INITIAL_BALANCE"] = InitialBalance;
        _row["AM_SUB_AMOUNT"] = SubAmount;
        _row["AM_ADD_AMOUNT"] = AddAmount;
        _row["AM_FINAL_BALANCE"] = FinalBalance;

        if (String.IsNullOrEmpty(Details))
        {
          _row["AM_DETAILS"] = DBNull.Value;
        }
        else
        {
          _row["AM_DETAILS"] = Details;
        }

        m_table.Rows.Add(_row);

        return true;
      }
      catch
      { ; }

      return false;
    } // Add

    //------------------------------------------------------------------------------
    // PURPOSE : Save the changes in the database.
    //
    //  PARAMS :
    //      - INPUT : 
    //             - Trx: SqlTransaction
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True: The changes are saved successfully.
    //      - False: The changes aren't saved successfully.
    //
    public Boolean Save(SqlTransaction Trx)
    {
      StringBuilder _sb;
      Int32 _num_rows_inserted;

      try
      {
        if (m_table.Rows.Count == 0)
        {
          return true;
        }

        _sb = new StringBuilder();
        _sb.AppendLine("INSERT INTO   ACCOUNT_MOVEMENTS                 ");
        _sb.AppendLine("            ( AM_ACCOUNT_ID                     ");
        _sb.AppendLine("            , AM_TYPE                           ");
        _sb.AppendLine("            , AM_INITIAL_BALANCE                ");
        _sb.AppendLine("            , AM_SUB_AMOUNT                     ");
        _sb.AppendLine("            , AM_ADD_AMOUNT                     ");
        _sb.AppendLine("            , AM_FINAL_BALANCE                  ");
        _sb.AppendLine("            , AM_DATETIME                       ");
        _sb.AppendLine("            , AM_CASHIER_ID                     ");
        _sb.AppendLine("            , AM_CASHIER_NAME                   ");
        _sb.AppendLine("            , AM_OPERATION_ID                   ");
        _sb.AppendLine("            , AM_PLAY_SESSION_ID                ");
        _sb.AppendLine("            , AM_TERMINAL_ID                    ");
        _sb.AppendLine("            , AM_TERMINAL_NAME                  ");
        _sb.AppendLine("            , AM_DETAILS)                       ");
        _sb.AppendLine("     VALUES ( @pAccountId                       ");
        _sb.AppendLine("            , @pType                            ");
        _sb.AppendLine("            , @pInitialBalance                  ");
        _sb.AppendLine("            , @pSubAmount                       ");
        _sb.AppendLine("            , @pAddAmount                       ");
        _sb.AppendLine("            , @pFinalBalance                    ");
        _sb.AppendLine("            , GETDATE()                         ");
        _sb.AppendLine("            , @pCashierId                       ");
        _sb.AppendLine("            , @pCashierName                     ");
        _sb.AppendLine("            , @pOperationId                     ");
        _sb.AppendLine("            , @pPlaySessionId                   ");
        _sb.AppendLine("            , @pTerminalId                      ");
        _sb.AppendLine("            , @pTerminalName                    ");
        _sb.AppendLine("            , @pDetails)                        ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).SourceColumn = "AM_OPERATION_ID";
          _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).SourceColumn = "AM_ACCOUNT_ID";

          _sql_cmd.Parameters.Add("@pType", SqlDbType.Int).SourceColumn = "AM_TYPE";
          _sql_cmd.Parameters.Add("@pInitialBalance", SqlDbType.Money).SourceColumn = "AM_INITIAL_BALANCE";
          _sql_cmd.Parameters.Add("@pSubAmount", SqlDbType.Money).SourceColumn = "AM_SUB_AMOUNT";
          _sql_cmd.Parameters.Add("@pAddAmount", SqlDbType.Money).SourceColumn = "AM_ADD_AMOUNT";
          _sql_cmd.Parameters.Add("@pFinalBalance", SqlDbType.Money).SourceColumn = "AM_FINAL_BALANCE";
          _sql_cmd.Parameters.Add("@pDetails", SqlDbType.NVarChar, 256).SourceColumn = "AM_DETAILS";

          if (m_cashier_session_info != null)
          {
            _sql_cmd.Parameters.Add("@pCashierId", SqlDbType.Int).Value = m_cashier_session_info.TerminalId;

            if (String.IsNullOrEmpty(m_cashier_session_info.AuthorizedByUserName))
            {
              _sql_cmd.Parameters.Add("@pCashierName", SqlDbType.NVarChar, 50).Value = m_cashier_session_info.TerminalName;
            }
            else
            {
              _sql_cmd.Parameters.Add("@pCashierName", SqlDbType.NVarChar, 50).Value = m_cashier_session_info.AuthorizedByUserName + "@" + m_cashier_session_info.TerminalName;
            }

            _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = DBNull.Value;
            _sql_cmd.Parameters.Add("@pTerminalName", SqlDbType.NVarChar, 50).Value = DBNull.Value;
            _sql_cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = DBNull.Value;
          }
          else
          {
            _sql_cmd.Parameters.Add("@pCashierId", SqlDbType.Int).Value = DBNull.Value;

            // m_cashier_name is used, for example, when closing manually a PlaySession (from Cashier or GUI).
            if (String.IsNullOrEmpty(m_cashier_name))
            {
              _sql_cmd.Parameters.Add("@pCashierName", SqlDbType.NVarChar, 50).Value = DBNull.Value;
            }
            else
            {
              _sql_cmd.Parameters.Add("@pCashierName", SqlDbType.NVarChar, 50).Value = m_cashier_name;
            }

            if (m_terminal_id == 0)
            {
              _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = DBNull.Value;
            }
            else
            {
              _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = m_terminal_id;
            }
            _sql_cmd.Parameters.Add("@pTerminalName", SqlDbType.NVarChar, 50).Value = m_terminal_name;
            if (m_play_session_id == 0)
            {
              _sql_cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = DBNull.Value;
            }
            else
            {
              _sql_cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = m_play_session_id;
            }
          }

          _sql_cmd.UpdatedRowSource = UpdateRowSource.None;

          using (SqlDataAdapter _da = new SqlDataAdapter())
          {
            _da.InsertCommand = _sql_cmd;

            _num_rows_inserted = _da.Update(m_table);

            if (_num_rows_inserted == m_table.Rows.Count)
            {
              return true;
            }
          }
        }
      }
      catch
      { ; }

      return false;
    } // Save

  } // AccountMovementsTable
}
