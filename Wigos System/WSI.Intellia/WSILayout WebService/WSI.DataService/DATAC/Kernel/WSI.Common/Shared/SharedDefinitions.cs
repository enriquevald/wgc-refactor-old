using System;
using System.Collections.Generic;
using System.Text;

namespace WSI.Common
{
  [Flags]
  public enum SAS_FLAGS
  {
    NONE = 0x00000000,
    USE_EXTENDED_METERS = 0x00000001,
    CREDITS_PLAY_MODE_SAS_MACHINE = 0x00000002,
  }

  public enum ACCOUNT_PROMO_CREDIT_TYPE
  {
    UNKNOWN = 0,
    NR1 = 1,
    NR2 = 2,
    REDEEMABLE = 3,
    POINT = 4
  }

  public enum ACCOUNT_PROMO_STATUS
  {
    UNKNOWN = 0,
    AWARDED = 1,
    ACTIVE = 2,
    EXHAUSTED = 3,
    EXPIRED = 4,
    REDEEMED = 5,
    NOT_AWARDED = 6,
    ERROR = 7,
    CANCELLED_NOT_ADDED = 8,
    CANCELLED = 9,
    CANCELLED_BY_PLAYER = 10,
    PREASSIGNED = 11
  }

  //SSC 23-MAR-2012
  public enum GU_USER_TYPE
  {
    NOT_ASSIGNED = -1,
    USER = 0,
    SYS_ACCEPTOR = 1,
    SYS_PROMOBOX = 2,
    SYS_SYSTEM = 3,
    SUPERUSER = 10,
  }

  public enum CASHIER_MOVEMENT
  {
    NOT_ASSIGNED = -1,
    OPEN_SESSION = 0,
    CLOSE_SESSION = 1,
    FILLER_IN = 2,
    FILLER_OUT = 3,
    CASH_IN = 4,
    CASH_OUT = 5,
    TAX_ON_PRIZE1 = 6,
    PROMOTION_NOT_REDEEMABLE = 7,
    PRIZES = 8,
    CARD_DEPOSIT_IN = 9,
    CARD_DEPOSIT_OUT = 10,
    CANCEL_NOT_REDEEMABLE = 11,
    MB_MANUAL_CHANGE_LIMIT = 12, //ManualChangeLimit
    MB_CASH_IN = 13,
    TAX_ON_PRIZE2 = 14,
    // AJQ 19-FEB-2010, Promotions
    PROMO_CREDITS = 20,
    PROMO_TO_REDEEMABLE = 21,
    PROMO_EXPIRED = 22,
    PROMO_CANCEL = 23,
    PROMO_START_SESSION = 24,
    PROMO_END_SESSION = 25,
    // TJG 04-MAR-2010 Replacement Cost for Personal Cards
    CARD_REPLACEMENT = 28,
    // RCI 14-JUN-2010 Draw Ticket Print
    DRAW_TICKET_PRINT = 29,
    // TJG 21-JUL-2010 Hand Pays
    HANDPAY = 30,
    HANDPAY_CANCELLATION = 31,
    // APB 31-AUG-2010 More Hand Pays
    MANUAL_HANDPAY = 32,
    MANUAL_HANDPAY_CANCELLATION = 33,
    // MBF 01-SEP-2010 Cash-in Split
    CASH_IN_SPLIT2 = 34,
    MB_CASH_IN_SPLIT2 = 35,
    DEV_SPLIT2 = 36,
    CASH_IN_SPLIT1 = 37,
    DEV_SPLIT1 = 38,
    MB_CASH_IN_SPLIT1 = 39,
    CANCEL_SPLIT1 = 40,
    CANCEL_SPLIT2 = 41,
    // RCI 29-OCT-2010: Draw Ticket in exchange of points
    POINTS_TO_DRAW_TICKET_PRINT = 42,
    PRIZE_COUPON = 43,
    // MBF 11-OCT-2011
    CARD_CREATION = 44,
    CARD_PERSONALIZATION = 45,
    // JMM 22-NOV-2011
    ACCOUNT_PIN_CHANGE = 46,
    ACCOUNT_PIN_RANDOM = 47,
    // RCI 17-FEB-2012
    CASH_IN_COVER_COUPON = 48,
    CASH_IN_NOT_REDEEMABLE2 = 49, // For future promotions
    // JMM 29-FEB-2012
    MB_CASH_IN_NOT_REDEEMABLE = 50,
    MB_CASH_IN_COVER_COUPON = 51,
    MB_CASH_IN_NOT_REDEEMABLE2 = 52,   // For future promotions
    MB_PRIZE_COUPON = 53,
    // SSC 08-MAR-2012: Added new movements for Note Acceptor
    NA_CASH_IN_SPLIT1 = 54,
    NA_CASH_IN_SPLIT2 = 55,
    NA_PRIZE_COUPON = 56,
    NA_CASH_IN_NOT_REDEEMABLE = 57,
    NA_CASH_IN_COVER_COUPON = 58,
    // JCM 24-APR-2012
    CARD_RECYCLED = 59,
    //SSC 11-JUN-2012
    POINTS_TO_NOT_REDEEMABLE = 60,
    POINTS_TO_REDEEMABLE = 61,

    // AJQ 25-JUN-2012
    REDEEMABLE_DEV_EXPIRED = 62,
    REDEEMABLE_PRIZE_EXPIRED = 63,
    NOT_REDEEMABLE_EXPIRED = 64,

    PROMOTION_REDEEMABLE = 65,
    CANCEL_PROMOTION_REDEEMABLE = 66,

    PROMOTION_POINT = 67,
    CANCEL_PROMOTION_POINT = 68,

    // AJQ 30-AUG-2012: Tax returning (redondeo por retención)
    TAX_RETURNING_ON_PRIZE_COUPON = 69,
    // AJQ 10-SEP-2012: Decimal Rounding (redondeo por decimales)
    DECIMAL_ROUNDING = 70,
    // RRB 12-SEP-2012: Service Charge (carga por servicio)
    SERVICE_CHARGE = 71,
    // JCM 13-SEP-2012: Mobile Bank delivered cash on the MB Session Close 
    MB_CASH_LOST = 72,
    // DDM 27-SEP-2012: 
    REQUEST_TO_VOUCHER_REPRINT = 73,
  }

  public enum MovementType
  {
    Play = 0,
    CashIn = 1,
    CashOut = 2,
    Devolution = 3,
    TaxOnPrize1 = 4,
    StartCardSession = 5,
    EndCardSession = 6,
    Activate = 7,
    Deactivate = 8,
    PromotionNotRedeemable = 9,
    DepositIn = 10,
    DepositOut = 11,
    CancelNotRedeemable = 12,
    TaxOnPrize2 = 13,

    // The following numbers have been used in the past, can't be used again
    // = 14,
    // = 15,
    // = 16,
    // = 17,
    // = 18,
    // = 19,

    // AJQ 19-FEB-2010, Promotions
    PromoCredits = 20,
    PromoToRedeemable = 21,
    PromoExpired = 22,
    PromoCancel = 23,
    PromoStartSession = 24,
    PromoEndSession = 25,

    // Credits Expiration
    CreditsExpired = 26,
    CreditsNotRedeemableExpired = 27,

    // TJG 04-MAR-2010 Replacement Cost for Personal Cards
    CardReplacement = 28,

    // RCI 14-JUN-2010 Draw Ticket Print
    DrawTicketPrint = 29,

    // RCI 05-JUL-2010: Handpays
    Handpay = 30,
    HandpayCancellation = 31,

    // ACC 31-AUG-2010 Manual Close Session
    ManualEndSession = 32,
    PromoManualEndSession = 33,

    // APB 31-AUG-2010 More Hand Pays
    ManualHandpay = 34,
    ManualHandpayCancellation = 35,

    // ACC 03-SEP-2010 PlayerTracking Points
    PointsAwarded = 36,
    PointsToGiftRequest = 37,
    PointsToNotRedeemable = 38,
    PointsGiftDelivery = 39,
    PointsExpired = 40,

    // RCI 29-OCT-2010: Draw Ticket in exchange of points
    PointsToDrawTicketPrint = 41,

    // RCI 05-NOV-2010: Services Gift
    PointsGiftServices = 42,

    // ACC 11-NOV-2010 Change holder levels
    HolderLevelChanged = 43,

    // AJQ 12-NOV-2010, Prize coupon     
    PrizeCoupon = 44,

    // Redemable Spent 
    //  - Used in Promotions     
    DEPRECATED_RedeemableSpentUsedInPromotions = 45,

    PointsToRedeemable = 46,

    PrizeExpired = 47,

    // MBF 11-OCT-2011
    CardCreated = 48,
    AccountPersonalization = 49,

    CardAdjustment = 50, // GUI Reserved

    // JMM 22-NOV-2011
    AccountPINChange = 51,
    AccountPINRandom = 52,

    // RCI 16-FEB-2012: For the new Not-Redeemable credit (used now for Cover Coupon).
    CreditsNotRedeemable2Expired = 53,
    CashInCoverCoupon = 54,
    CashInNotRedeemable2 = 55, // For future promotions

    // ACC 21-MAR-2012
    CancelStartSession = 56,

    // JCM 24-APR-2012
    CardRecycled = 57,

    PromotionRedeemable = 58,
    CancelPromotionRedeemable = 59,

    PromotionPoint = 60,
    CancelPromotionPoint = 61,

    // RCI 28-AUG-2012: For manual level change from the GUI.
    ManualHolderLevelChanged = 62,

    // AJQ 30-AUG-2012: Tax returning (redondeo por retención)
    TaxReturningOnPrizeCoupon = 63,
    // AJQ 10-SEP-2012: Decimal Rounding (redondeo por decimales)
    DecimalRounding = 64,
    // RRB 12-SEP-2012: Service Charge (carga por servicio)
    ServiceCharge = 65,
    // MPO 20-SEP-2012: Cancel gift instance (cancelación del regalo, ya sea solicitado o entregado (para NR o RE))
    CancelGiftInstance = 66,

  } // MovementType

  public enum PlaySessionType
  {
    NONE = 0,
    CADILLAC_JACK = 1,
    WIN = 2,
    ALESIS = 3,

    HANDPAY_CANCELLED_CREDITS = 100,
    HANDPAY_JACKPOT = 101,
    HANDPAY_ORPHAN_CREDITS = 102,
    HANDPAY_MANUAL = 110,
    HANDPAY_SITE_JACKPOT = 120,
  }

  public enum AccountType
  {
    ACCOUNT_UNKNOWN = 0,
    ACCOUNT_CADILLAC_JACK = 1,
    ACCOUNT_WIN = 2,
    ACCOUNT_ALESIS = 3,
  }

  public enum CashlessMode
  {
    NONE = 0,
    WIN = 1,
    ALESIS = 2,
    WIN_AND_ALESIS = 3, // WIN & ALESIS
    CADILLAC_JACK = 4,
  }

  public enum PlaySessionStatus
  {
    Opened = 0,
    Closed = 1,
    Abandoned = 2,
    ManualClosed = 3,

    Cancelled = 4,
    HandpayPayment = 5,
    HandpayCancelPayment = 6,

  }

  public enum AccountBlockReason
  {
    NONE = 0,
    FROM_CASHIER = 1,
    MAX_BALANCE = 2,
    ABANDONED_CARD = 3,
    FROM_GUI = 4,
    MAX_PIN_FAILURES = 5,
  }

  public enum TerminalTypes
  {
    // Unknown
    UNKNOWN = -1,
    // Machines
    WIN = 1,                // LKT 
    T3GS = 3,               // 3GS
    SAS_HOST = 5,           // SasHost
    // Site 
    SITE = 100,             // Site 
    SITE_JACKPOT = 101,     // Site Jackpot
    MOBILE_BANK = 102,      // MobileBank-SasHost
    MOBILE_BANK_IMB = 103,  // MobileBank-IPod
    ISTATS = 104,           // IStats-IPad
    PROMOBOX = 105,         // Promobox  
  }

  public enum HANDPAY_TYPE      // Why the Handpay was generated 
  {
    CANCELLED_CREDITS = 0,
    JACKPOT = 1,
    ORPHAN_CREDITS = 2,
    MANUAL = 10,
    SITE_JACKPOT = 20,
    UNKNOWN = 999
  }

  public enum TerminalStatus
  {
    ACTIVE = 0,
    OUT_OF_SERVICE = 1,
    RETIRED = 2
  }

  public enum ACCOUNTS_PROMO_COST
  {
    RESET_ON_REDEEM = 0,
    SET_ON_FIRST_NR_PROMOTION = 1,
    UPDATE_ON_RECHARGE = 2,
    UPDATE_ON_REDEEM = 3
  }

  public enum ACCOUNT_FLAG_STATUS
  {
    ACTIVE = 0,
    NO_ACTIVE = 1,
    EXPIRED = 2,
    CANCELLED = 3
  }

  public enum FLAG_EXPIRATION_TYPE
  {
    UNKNOWN = 0,

    NEVER = 1,
    DATETIME = 2,
    
    //SECONDS = 10,
    MINUTES = 11,
    HOURS = 12,
    DAYS = 13,
    MONTH = 14,
  }

  public enum FLAG_PROMOTION_FLAG_TYPE
  { 
    AWARDED = 0,
    REQUIRED = 1
  }

}
