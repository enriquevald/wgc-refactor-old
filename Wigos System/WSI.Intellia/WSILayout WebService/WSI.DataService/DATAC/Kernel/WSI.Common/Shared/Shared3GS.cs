//-------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//-------------------------------------------------------------------
//
// MODULE NAME:   Shared3GS.cs
// DESCRIPTION:   
// AUTHOR:        Andreu Julia & Raul Cervera
// CREATION DATE: 23-JUL-2012
//
// REVISION HISTORY:
//
// Date         Author Description
// -----------  ------ -----------------------------------------------
// 23-JUL-2012  AJQ    Initial version
// -------------------------------------------------------------------

using System;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using System.Data.SqlClient;
using System.Data;
using WSI.Common;

namespace WSI.Common
{
  public static class T3GS
  {
    public static MultiPromos.StartSessionOutput Start(String VendorId, String SerialNumber, Int32 MachineNumber,
                                                       String ExternalTrackData,
                                                       SqlTransaction Trx)
    {
      MultiPromos.StartSessionOutput _output;
      Terminal.TerminalInfo _terminal;
      MultiPromos.AccountInfo _account;

      _output = new MultiPromos.StartSessionOutput();

      if (!MultiPromos.Trx_UpdateAccountBalance(0, ExternalTrackData, MultiPromos.AccountBalance.Zero, 0, out _account, Trx))
      {
        _output.SetError(MultiPromos.StartSessionStatus.AccountUnknown, _account.ErrorMessage);

        return _output;
      }

      if (!Terminal.Trx_GetTerminalInfo(VendorId, SerialNumber, MachineNumber, out _terminal, Trx))
      {
        _output.SetError(MultiPromos.StartSessionStatus.TerminalUnknown, "");

        return _output;
      }

      Trx.Save("T3GS.Start");

      _output = MultiPromos.Trx_CommonStartCardSession(0, _terminal, _account, Trx);

      if (_output.StatusCode != MultiPromos.StartSessionStatus.Ok)
      {
        Trx.Rollback("T3GS.Start");
      }

      return _output;
    } // Start

    public static MultiPromos.EndSessionOutput Update(String VendorId, String SerialNumber, Int32 MachineNumber,
                                                      String ExternalTrackData,
                                                      Int64 PlaySessionId,
                                                      MultiPromos.PlayedWonMeters PlaySessionMeters,
                                                      Decimal FinalBalance,
                                                      SqlTransaction Trx)
    {
      MultiPromos.EndSessionOutput _output;
      Terminal.TerminalInfo _terminal;
      MultiPromos.AccountInfo _account;

      _output = new MultiPromos.EndSessionOutput();

      try
      {
        Trx.Save("T3GS.Update");

        if (!MultiPromos.Trx_UpdateAccountBalance(0, ExternalTrackData, MultiPromos.AccountBalance.Zero, 0, out _account, Trx))
        {
          _output.SetError(MultiPromos.EndSessionStatus.AccountUnknown, _account.ErrorMessage);

          return _output;
        }

        if (!Terminal.Trx_GetTerminalInfo(VendorId, SerialNumber, MachineNumber, out _terminal, Trx))
        {
          _output.SetError(MultiPromos.EndSessionStatus.TerminalUnknown, "");

          return _output;
        }

        _output = Update_Internal(_terminal, _account, PlaySessionId, PlaySessionMeters, FinalBalance, Trx);

        return _output;
      }
      finally
      {
        if (_output.StatusCode != MultiPromos.EndSessionStatus.Ok
            && _output.StatusCode != MultiPromos.EndSessionStatus.BalanceMismatch)
        {
          Trx.Rollback("T3GS.Update");
        }
      }
    } // Update


    public static MultiPromos.EndSessionOutput End(String VendorId, String SerialNumber, Int32 MachineNumber,
                                                   String ExternalTrackData,
                                                   Int64 PlaySessionId,
                                                   MultiPromos.PlayedWonMeters PlaySessionMeters,
                                                   Decimal FinalBalance,
                                                   SqlTransaction Trx)
    {
      MultiPromos.EndSessionOutput _output;
      Terminal.TerminalInfo _terminal;
      MultiPromos.AccountInfo _account;
      MultiPromos.EndSessionInput _input;

      _output = new MultiPromos.EndSessionOutput();

      try
      {
        Trx.Save("T3GS.End");

        if (!MultiPromos.Trx_UpdateAccountBalance(0, ExternalTrackData, MultiPromos.AccountBalance.Zero, 0, out _account, Trx))
        {
          _output.SetError(MultiPromos.EndSessionStatus.AccountUnknown, _account.ErrorMessage);

          return _output;
        }

        if (!Terminal.Trx_GetTerminalInfo(VendorId, SerialNumber, MachineNumber, out _terminal, Trx))
        {
          _output.SetError(MultiPromos.EndSessionStatus.TerminalUnknown, "Trx_GetTerminalInfo failed!");

          return _output;
        }

        _output = Update_Internal(_terminal, _account, PlaySessionId, PlaySessionMeters, FinalBalance, Trx);

        if (_output.StatusCode != MultiPromos.EndSessionStatus.Ok
            && _output.StatusCode != MultiPromos.EndSessionStatus.BalanceMismatch)
        {
          return _output;
        }

        _input = new MultiPromos.EndSessionInput();
        _input.AccountId = _account.AccountId;
        _input.BalanceFromGM = FinalBalance;
        _input.HasMeters = true;
        _input.Meters = PlaySessionMeters;
        _input.MovementType = MovementType.EndCardSession;
        _input.PlaySessionId = PlaySessionId;
        _input.TransactionId = 0;

        if (!MultiPromos.Trx_OnEndCardSession(_terminal.TerminalId, _terminal.Type, _terminal.Name, _input, out _output, Trx))
        {
          _output.SetError(MultiPromos.EndSessionStatus.FaltalError, "Trx_OnEndCardSession failed!");

          return _output;
        }

        return _output;
      }
      finally
      {
        if (_output.StatusCode != MultiPromos.EndSessionStatus.Ok
            && _output.StatusCode != MultiPromos.EndSessionStatus.BalanceMismatch)
        {
          Trx.Rollback("T3GS.End");
        }
      }
    } // End

    public static MultiPromos.StartSessionOutput AccountStatus(String ExternalTrackData,
                                                               String VendorId, Int32 MachineNumber,
                                                               SqlTransaction Trx)
    {
      MultiPromos.StartSessionOutput _output;
      MultiPromos.AccountInfo _account;

      _output = new MultiPromos.StartSessionOutput();

      try
      {
        Trx.Save("T3GS.AccountStatus");

        if (!MultiPromos.Trx_UpdateAccountBalance(0, ExternalTrackData, MultiPromos.AccountBalance.Zero, 0, out _account, Trx))
        {
          _output.SetError(MultiPromos.StartSessionStatus.AccountUnknown, _account.ErrorMessage);

          return _output;
        }

        if (_account.CurrentPlaySessionId != 0)
        {
          _output.SetError(MultiPromos.StartSessionStatus.AccountInSession, "");

          return _output;
        }

        _output.FinBalance = _account.FinalBalance;
        _output.SetSuccess();

        return _output;
      }
      finally
      {
        if (_output.StatusCode != MultiPromos.StartSessionStatus.Ok)
        {
          Trx.Rollback("T3GS.AccountStatus");
        }
      }
    } // AccountStatus

    public static MultiPromos.StartSessionOutput SendEvent(String ExternalTrackData,
                                                           String VendorId, String SerialNumber, Int32 MachineNumber,
                                                           Int64 EventId,
                                                           Decimal Amount,
                                                           SqlTransaction Trx)
    {

      MultiPromos.StartSessionOutput _output;
      Terminal.TerminalInfo _terminal;
      MultiPromos.AccountInfo _account;
      Int32 _operation_code;
      StringBuilder _sb;

      _output = new MultiPromos.StartSessionOutput();

      try
      {
        Trx.Save("T3GS.SendEvent");

        if (!Terminal.Trx_GetTerminalInfo(VendorId, SerialNumber, MachineNumber, out _terminal, Trx))
        {
          _output.SetError(MultiPromos.StartSessionStatus.TerminalUnknown, "");

          return _output;
        }

        //
        // Check Terminal
        //
        if (_terminal == null || _terminal.TerminalId == 0)
        {
          _output.SetError(MultiPromos.StartSessionStatus.TerminalUnknown, "");

          return _output;
        }

        switch (EventId)
        {
          case 1:
            _operation_code = 16;
            break;

          case 2:
            _operation_code = 17;
            break;

          case 3:
            _operation_code = 18;
            break;

          default:
            _operation_code = 0;
            break;
        }

        if (_operation_code == 0)
        {
          _output.SetError(MultiPromos.StartSessionStatus.Error, "Unknown EventId.");

          return _output;
        }

        if (!MultiPromos.Trx_UpdateAccountBalance(0, ExternalTrackData, MultiPromos.AccountBalance.Zero, 0, out _account, Trx))
        {
          _output.SetError(MultiPromos.StartSessionStatus.AccountUnknown, _account.ErrorMessage);

          return _output;
        }

        if (_account.CurrentPlaySessionId == 0)
        {
          _output.SetError(MultiPromos.StartSessionStatus.Error, "The account doesn't have a play session");

          return _output;
        }

        if (_account.CurrentTerminalId != _terminal.TerminalId)
        {
          _output.SetError(MultiPromos.StartSessionStatus.Error, "Current terminal of account mismatched with terminal passed");

          return _output;
        }

        _sb = new StringBuilder();
        _sb.AppendLine(" INSERT INTO EVENT_HISTORY ( EH_TERMINAL_ID ");
        _sb.AppendLine("                           , EH_SESSION_ID ");
        _sb.AppendLine("                           , EH_DATETIME ");
        _sb.AppendLine("                           , EH_EVENT_TYPE ");
        _sb.AppendLine("                           , EH_OPERATION_CODE ");
        _sb.AppendLine("                           , EH_OPERATION_DATA) ");
        _sb.AppendLine("                    VALUES ( @pTerminalId ");
        _sb.AppendLine("                           , @pPlaySessionId ");
        _sb.AppendLine("                           , GETDATE() ");
        _sb.AppendLine("                           , @pEventType ");
        _sb.AppendLine("                           , @pOperationCode ");
        _sb.AppendLine("                           , @pAmount) ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = _terminal.TerminalId;
          _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.Int).Value = _account.CurrentPlaySessionId;
          _cmd.Parameters.Add("@pEventType", SqlDbType.Int).Value = 2;
          _cmd.Parameters.Add("@pOperationCode", SqlDbType.Int).Value = _operation_code;
          _cmd.Parameters.Add("@pAmount", SqlDbType.Money).Value = Amount;

          if (_cmd.ExecuteNonQuery() != 1)
          {
            _output.SetError(MultiPromos.StartSessionStatus.Error, "Error on insert event_history!");

            return _output;
          }
        }

        _output.SetSuccess();

        return _output;
      }
      finally
      {
        if (_output.StatusCode != MultiPromos.StartSessionStatus.Ok)
        {
          Trx.Rollback("T3GS.SendEvent");
        }
      }
    } // SendEvent

    private static MultiPromos.EndSessionOutput Update_Internal(Terminal.TerminalInfo Terminal,
                                                                MultiPromos.AccountInfo Account,
                                                                Int64 PlaySessionId,
                                                                MultiPromos.PlayedWonMeters PlaySessionMeters,
                                                                Decimal FinalBalance,
                                                                SqlTransaction Trx)
    {
      MultiPromos.EndSessionOutput _output;
      MultiPromos.PlayedWonMeters _delta;
      Decimal _calculated;
      Decimal _effective_final_balance;
      Decimal _maximum_balance_error;
      Decimal _diff;
      Boolean _mismatch;
      Boolean _big_mismatch;

      _output = new MultiPromos.EndSessionOutput();

      if (Account.CurrentPlaySessionId != PlaySessionId
          || Account.CurrentTerminalId != Terminal.TerminalId
          || Terminal.CurrentPlaySessionId != PlaySessionId
          || Terminal.CurrentAccountId != Account.AccountId
          || Account.CurrentPlaySessionStatus != PlaySessionStatus.Opened)
      {
        _output.SetError(MultiPromos.EndSessionStatus.TerminalUnknown,
                         String.Format("Invalid PlaySessionId. Reported: {0}, Current: {1}, Status: {2}",
                                        PlaySessionId.ToString(),
                                        Account.CurrentPlaySessionId.ToString(),
                                        Account.CurrentPlaySessionStatus.ToString()));
        return _output;
      }

      if (FinalBalance < 0
          || PlaySessionMeters.PlayedAmount < 0
          || PlaySessionMeters.PlayedCount  < 0 
          || PlaySessionMeters.WonAmount    < 0 
          || PlaySessionMeters.WonCount     < 0) 
      {
        _output.SetError(MultiPromos.EndSessionStatus.TerminalBlocked,
                         String.Format("Negative Meters. Final: {0}; Played: {1}, #{2}; Won: {3}, #{4}",
                                        FinalBalance.ToString(),
                                        PlaySessionMeters.PlayedAmount.ToString(),
                                        PlaySessionMeters.PlayedCount.ToString(),
                                        PlaySessionMeters.WonAmount.ToString(),
                                        PlaySessionMeters.WonCount.ToString()));
        return _output;
      }

      _effective_final_balance = Math.Max(0, FinalBalance);
      _calculated = Math.Max(0, Account.CurrentPlaySessionInitialBalance - PlaySessionMeters.PlayedAmount + PlaySessionMeters.WonAmount);
      _diff = FinalBalance - _calculated;
      _mismatch = (_diff != 0);
      _big_mismatch = false;

      if (_mismatch)
      {
        Decimal.TryParse(MultiPromos.ReadGeneralParams("Interface3GS", "MaximumBalanceErrorCents", Trx), out _maximum_balance_error);
        _maximum_balance_error = _maximum_balance_error / 100;

        if (Math.Abs(_diff) > _maximum_balance_error)
        {
          // Big Balance Mismatch 
          _big_mismatch = true;
          _effective_final_balance = Math.Max(0, Math.Min(_calculated, FinalBalance));
        }
      }

      if (!MultiPromos.Trx_UpdatePlaySessionPlayedWon(TerminalTypes.T3GS, PlaySessionId, PlaySessionMeters, out _delta, Trx))
      {
        _output.SetError(MultiPromos.EndSessionStatus.FaltalError, "Trx_UpdatePlaySessionPlayedWon failed!");

        return _output;
      }

      if (!MultiPromos.Trx_PlaySessionSetFinalBalance(PlaySessionId, _effective_final_balance, FinalBalance, _mismatch, MovementType.EndCardSession, Trx))
      {
        _output.SetError(MultiPromos.EndSessionStatus.FaltalError, "Trx_PlaySessionSetFinalBalance failed!");

        return _output;
      }

      using (SqlCommand _cmd = WSI.Common.Terminal.GetGameMetersUpdateCommand(Trx))
      {
        _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = Terminal.TerminalId;
        _cmd.Parameters.Add("@pGameBaseName", SqlDbType.NVarChar, 50).Value = "GAME_3GS";
        _cmd.Parameters.Add("@pDenomination", SqlDbType.Money).Value = 0.01;
        _cmd.Parameters.Add("@pWcpSequenceId", SqlDbType.BigInt).Value = 0;
        _cmd.Parameters.Add("@pDeltaPlayedCount", SqlDbType.BigInt).Value = _delta.PlayedCount;
        _cmd.Parameters.Add("@pDeltaPlayedAmount", SqlDbType.Money).Value = _delta.PlayedAmount;
        _cmd.Parameters.Add("@pDeltaWonCount", SqlDbType.BigInt).Value = _delta.WonCount;
        _cmd.Parameters.Add("@pDeltaWonAmount", SqlDbType.Money).Value = _delta.WonAmount;

        if (_cmd.ExecuteNonQuery() != 1)
        {
          _output.SetError(MultiPromos.EndSessionStatus.FaltalError, "GameMetersUpdate failed!");

          return _output;
        }
      }

      if (_big_mismatch)
      {
        _output.SetError(MultiPromos.EndSessionStatus.BalanceMismatch,
                         String.Format("Balance Mismatch: Reported={0}, Calculated={1}, Final={2}",
                                        FinalBalance.ToString(), _calculated.ToString(), _effective_final_balance.ToString()));
        return _output;
      }

      _output.SetSuccess();

      return _output;
    } // Update_Internal

    public static void Translate(MultiPromos.StartSessionOutput StartSession, out int StatusCode, out String StatusText, out String ErrorText)
    {
      ErrorText = StartSession.Message;

      switch (StartSession.StatusCode)
      {
        case MultiPromos.StartSessionStatus.Ok:
          StatusCode = 0;
          StatusText = "Success";
          ErrorText = "";
          break;

        case MultiPromos.StartSessionStatus.AccountUnknown:
          StatusCode = 1;
          StatusText = "Invalid Account Number";
          break;

        case MultiPromos.StartSessionStatus.AccountInSession:
          StatusCode = 2;
          StatusText = "Account Locked";
          break;

        case MultiPromos.StartSessionStatus.TerminalUnknown:
        case MultiPromos.StartSessionStatus.TerminalBlocked:
          StatusCode = 3;
          StatusText = "Invalid Machine Information";
          break;

        case MultiPromos.StartSessionStatus.AccountBlocked:
        case MultiPromos.StartSessionStatus.Error:
        default:
          StatusCode = 4;
          StatusText = "Access Denied";
          break;
      } // switch (StartSession.StatusCode)
    } // Translate

    public static void Translate(MultiPromos.EndSessionOutput EndSession, out int StatusCode, out String StatusText, out String ErrorText)
    {
      ErrorText = EndSession.Message;

      switch (EndSession.StatusCode)
      {
        case MultiPromos.EndSessionStatus.Ok:
          StatusCode = 0;
          StatusText = "Success";
          ErrorText = "";
          break;

        case MultiPromos.EndSessionStatus.AccountUnknown:
          StatusCode = 1;
          StatusText = "Invalid Account Number";
          break;

        case MultiPromos.EndSessionStatus.AccountBlocked:
          StatusCode = 2;
          StatusText = "Account Locked";
          break;

        case MultiPromos.EndSessionStatus.TerminalUnknown:
        case MultiPromos.EndSessionStatus.TerminalBlocked:
          StatusCode = 3;
          StatusText = "Invalid Machine Information";
          break;

        case MultiPromos.EndSessionStatus.ErrorRetry:
        case MultiPromos.EndSessionStatus.FaltalError:
        case MultiPromos.EndSessionStatus.NotOpened:
        case MultiPromos.EndSessionStatus.AccountInSession:
        default:
          StatusCode = 4;
          StatusText = "Access Denied";
          break;

        case MultiPromos.EndSessionStatus.BalanceMismatch:
          StatusCode = 5;
          StatusText = "Final balance does not match validation.";
          break;
      } // switch (EndSession.StatusCode)
    } // Translate

  }

}
