//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: SharedCashierMovements.cs
// 
//   DESCRIPTION: Implements the following classes:
//                1. CommonCashierSessionInfo
//                2. CashierMovementsTable
//
//        AUTHOR: Susana Samso
// 
// CREATION DATE: 15-JUN-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
//19-JUN-2012  SSC    Initial version.
//19-JUN-2012  JAB    Added GetCashierMovementAddSubAmount and GetCashierMovementBalanceIncrement functions.
//27-SEP-2012  DDM    Added new movement REQUEST_TO_VOUCHER_REPRINT
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;


namespace WSI.Common
{
  public class CashierMovementsTest
  {
    public void Test1(CashierSessionInfo CashierSessionInfo, SqlTransaction SqlTrx)
    {
      Int64 _operation_id;
      Decimal _amount;
      Int64 _account_id;
      CashierMovementsTable _table;

      _operation_id = 10628;

      _amount = 50;
      _account_id = 2307;

      _table = new CashierMovementsTable(CashierSessionInfo);
      _table.Add(_operation_id, CASHIER_MOVEMENT.PRIZE_COUPON, _amount, _account_id, "");

      _table.Save(SqlTrx);
    }

  } // CashierMovementsTest


  // This class is used for grouping the cashier session info.
  // There is no constructor. Exists 2 static methods that returns an instance of this class:
  //   - In the class WSI.Common.CommonCashierInformation.
  //   - In the class WSI.Cashier.Cashier.
  public class CashierSessionInfo
  {
    public Int64 CashierSessionId;
    public Int32 TerminalId;
    public Int32 UserId;
    public String TerminalName;
    public String UserName;
    public Int32 AuthorizedByUserId;
    public String AuthorizedByUserName;
    public Boolean IsMobileBank;

    public GU_USER_TYPE UserType;

    public CashierSessionInfo Copy()
    {
      return (CashierSessionInfo)this.MemberwiseClone();
    } // Copy

  } // CashierSessionInfo

  public class CashierMovementsTable
  {
    private DataTable m_table = null;

    private CashierSessionInfo m_cashier_session_info = null;

    public CashierSessionInfo CashierSessionInfo
    {
      get { return m_cashier_session_info; }
    }

    public void InitSchema()
    {
      m_table = new DataTable();
      m_table.Columns.Add("CM_OPERATION_ID", Type.GetType("System.Int64"));
      m_table.Columns.Add("CM_ACCOUNT_ID", Type.GetType("System.Int64"));
      m_table.Columns.Add("CM_CARD_TRACK_DATA", Type.GetType("System.String"));

      m_table.Columns.Add("CM_TYPE", Type.GetType("System.Int32")).AllowDBNull = false;
      m_table.Columns.Add("CM_INITIAL_BALANCE", Type.GetType("System.Decimal")).AllowDBNull = false;
      m_table.Columns.Add("CM_ADD_AMOUNT", Type.GetType("System.Decimal")).AllowDBNull = false;
      m_table.Columns.Add("CM_SUB_AMOUNT", Type.GetType("System.Decimal")).AllowDBNull = false;
      m_table.Columns.Add("CM_FINAL_BALANCE", Type.GetType("System.Decimal")).AllowDBNull = false;
      m_table.Columns.Add("CM_BALANCE_INCREMENT", Type.GetType("System.Decimal")).AllowDBNull = false;

      m_table.Columns.Add("CM_DETAILS", Type.GetType("System.String")).AllowDBNull = true;

    } // InitSchema

    public CashierMovementsTable(CashierSessionInfo CashierSessionInfo)
    {
      InitSchema();

      m_cashier_session_info = CashierSessionInfo.Copy();

    } // CashierMovementsTable

    public Boolean Add(Int64 OperationId, CASHIER_MOVEMENT MovementType,
                   Decimal Amount, Int64 AccountId, String CardTrackData)
    {
      return Add(OperationId, MovementType, Amount, AccountId, CardTrackData, "");
    }

    public Boolean Add(Int64 OperationId, CASHIER_MOVEMENT MovementType,
                       Decimal Amount, Int64 AccountId, String CardTrackData, String PromoName)
    {
      DataRow _row;
      Decimal _balance_increment;
      Decimal _add_amount;
      Decimal _sub_amount;

      try
      {
        GetCashierMovementAddSubAmount(MovementType, Amount, out _add_amount, out _sub_amount, ref CardTrackData);
        _balance_increment = GetCashierMovementBalanceIncrement(MovementType, _add_amount, _sub_amount);

        _row = m_table.NewRow();

        _row["CM_OPERATION_ID"] = OperationId;

        _row["CM_TYPE"] = MovementType;
        _row["CM_INITIAL_BALANCE"] = 0;
        _row["CM_ADD_AMOUNT"] = _add_amount;
        _row["CM_SUB_AMOUNT"] = _sub_amount;
        _row["CM_FINAL_BALANCE"] = 0;
        _row["CM_BALANCE_INCREMENT"] = _balance_increment;

        if (String.IsNullOrEmpty(PromoName))
        {
          _row["CM_DETAILS"] = DBNull.Value;
        }
        else
        {
          _row["CM_DETAILS"] = PromoName;
        }

        if (!String.IsNullOrEmpty(CardTrackData))
        {
          _row["CM_CARD_TRACK_DATA"] = CardTrackData;
        }
        else
        {
          _row["CM_CARD_TRACK_DATA"] = DBNull.Value;
        }

        if (AccountId != 0)
        {
          _row["CM_ACCOUNT_ID"] = AccountId;
        }
        else
        {
          _row["CM_ACCOUNT_ID"] = DBNull.Value;
        }

        m_table.Rows.Add(_row);

        return true;
      }
      catch
      { ; }

      return false;
    } // Add

    public Boolean Save(SqlTransaction Trx)
    {
      StringBuilder _sb;
      Int32 _num_rows_inserted;
      SqlParameter _param_increment;

      try
      {
        if (m_table.Rows.Count == 0)
        {
          return true;
        }

        //Update Balance
        _sb = new StringBuilder();
        _sb.AppendLine("UPDATE   CASHIER_SESSIONS ");
        _sb.AppendLine("   SET   CS_BALANCE    = CS_BALANCE + @pBalanceIncrement ");
        _sb.AppendLine("OUTPUT   DELETED.CS_BALANCE ");
        _sb.AppendLine("       , INSERTED.CS_BALANCE ");
        _sb.AppendLine(" WHERE   CS_SESSION_ID = @pSessionId ");
        _sb.AppendLine("   AND   CS_STATUS     = 0 ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = m_cashier_session_info.CashierSessionId;
          _param_increment = _sql_cmd.Parameters.Add("@pBalanceIncrement", SqlDbType.Money);

          foreach (DataRow _row in m_table.Rows)
          {
            _param_increment.Value = _row["CM_BALANCE_INCREMENT"];

            using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
            {
              if (!_reader.Read())
              {
                return false;
              }
              _row["CM_INITIAL_BALANCE"] = _reader.GetDecimal(0);
              _row["CM_FINAL_BALANCE"] = _reader.GetDecimal(1);

            } // using SqlDataReader
          }
        }

        // Insert the movement
        _sb.Length = 0;
        _sb.AppendLine(" INSERT INTO   CASHIER_MOVEMENTS      ");
        _sb.AppendLine("             ( CM_SESSION_ID          ");
        _sb.AppendLine("             , CM_CASHIER_ID          ");
        _sb.AppendLine("             , CM_USER_ID             ");
        _sb.AppendLine("             , CM_TYPE                ");
        _sb.AppendLine("             , CM_INITIAL_BALANCE     ");
        _sb.AppendLine("             , CM_ADD_AMOUNT          ");
        _sb.AppendLine("             , CM_SUB_AMOUNT          ");
        _sb.AppendLine("             , CM_FINAL_BALANCE       ");
        _sb.AppendLine("             , CM_USER_NAME           ");
        _sb.AppendLine("             , CM_CASHIER_NAME        ");
        _sb.AppendLine("             , CM_CARD_TRACK_DATA     ");
        _sb.AppendLine("             , CM_ACCOUNT_ID          ");
        _sb.AppendLine("             , CM_OPERATION_ID        ");
        _sb.AppendLine("             , CM_DETAILS             ");
        _sb.AppendLine("             )                        ");
        _sb.AppendLine("   VALUES    ( @pSessionId            ");
        _sb.AppendLine("             , @pCashierId            ");
        _sb.AppendLine("             , @pUserId               ");
        _sb.AppendLine("             , @pType                 ");
        _sb.AppendLine("             , @pInitialBalance       ");
        _sb.AppendLine("             , @pAddAmount            ");
        _sb.AppendLine("             , @pSubAmmount           ");
        _sb.AppendLine("             , @pFinalBalance         ");
        _sb.AppendLine("             , @pUserName             ");
        _sb.AppendLine("             , @pCashierName          ");
        _sb.AppendLine("             , @pCardData             ");
        _sb.AppendLine("             , @pAccountId            ");
        _sb.AppendLine("             , @pOperationId          ");
        _sb.AppendLine("             , @pDetails              ");
        _sb.AppendLine("             )                        ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).SourceColumn = "CM_OPERATION_ID";
          _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).SourceColumn = "CM_ACCOUNT_ID";
          _sql_cmd.Parameters.Add("@pCardData", SqlDbType.NVarChar, 50).SourceColumn = "CM_CARD_TRACK_DATA";

          _sql_cmd.Parameters.Add("@pType", SqlDbType.Int).SourceColumn = "CM_TYPE";
          _sql_cmd.Parameters.Add("@pInitialBalance", SqlDbType.Money).SourceColumn = "CM_INITIAL_BALANCE";
          _sql_cmd.Parameters.Add("@pAddAmount", SqlDbType.Money).SourceColumn = "CM_ADD_AMOUNT";
          _sql_cmd.Parameters.Add("@pSubAmmount", SqlDbType.Money).SourceColumn = "CM_SUB_AMOUNT";
          _sql_cmd.Parameters.Add("@pFinalBalance", SqlDbType.Money).SourceColumn = "CM_FINAL_BALANCE";
          _sql_cmd.Parameters.Add("@pDetails", SqlDbType.NVarChar, 256).SourceColumn = "CM_DETAILS";

          _sql_cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = m_cashier_session_info.CashierSessionId;
          _sql_cmd.Parameters.Add("@pCashierId", SqlDbType.BigInt).Value = m_cashier_session_info.TerminalId;
          _sql_cmd.Parameters.Add("@pCashierName", SqlDbType.NVarChar, 50).Value = m_cashier_session_info.TerminalName;
          _sql_cmd.Parameters.Add("@pUserId", SqlDbType.BigInt).Value = m_cashier_session_info.AuthorizedByUserId;
          _sql_cmd.Parameters.Add("@pUserName", SqlDbType.NVarChar, 50).Value = m_cashier_session_info.AuthorizedByUserName;

          _sql_cmd.UpdatedRowSource = UpdateRowSource.None;

          using (SqlDataAdapter _sql_adap = new SqlDataAdapter())
          {
            _sql_adap.InsertCommand = _sql_cmd;

            _sql_adap.UpdateBatchSize = 500;
            _num_rows_inserted = _sql_adap.Update(m_table);

            if (_num_rows_inserted == m_table.Rows.Count)
            {
              return true;
            }
          }
        }
      }
      catch
      { ; }

      return false;
    } // Save

    public static void GetCashierMovementAddSubAmount(CASHIER_MOVEMENT MovementType, Decimal Amount, out Decimal AddAmount, out Decimal SubAmount, ref String CardTrackData)
    {
      String _card_track_data;

      _card_track_data = CardTrackData;

      AddAmount = 0;
      SubAmount = 0;
      CardTrackData = "";

      #region Switch(MovementType)

      switch (MovementType)
      {
        case CASHIER_MOVEMENT.OPEN_SESSION:
          AddAmount = Amount;
          SubAmount = 0;
          break;

        case CASHIER_MOVEMENT.CLOSE_SESSION:
          AddAmount = 0;
          SubAmount = 0;
          break;

        case CASHIER_MOVEMENT.PROMO_START_SESSION:
        case CASHIER_MOVEMENT.PROMO_END_SESSION:
          AddAmount = 0;
          SubAmount = 0;
          break;

        case CASHIER_MOVEMENT.PROMO_CREDITS:
          AddAmount = Amount;
          SubAmount = 0;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.PROMO_CANCEL:
        case CASHIER_MOVEMENT.NOT_REDEEMABLE_EXPIRED:
        case CASHIER_MOVEMENT.REDEEMABLE_DEV_EXPIRED:
        case CASHIER_MOVEMENT.REDEEMABLE_PRIZE_EXPIRED:
          AddAmount = 0;
          SubAmount = Amount;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.PROMO_TO_REDEEMABLE:
          AddAmount = Amount;
          SubAmount = 0;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.PROMO_EXPIRED:
          AddAmount = 0;
          SubAmount = 0;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.FILLER_IN:
          AddAmount = Amount;
          SubAmount = 0;
          break;

        case CASHIER_MOVEMENT.FILLER_OUT:
          AddAmount = 0;
          SubAmount = Amount;
          break;

        case CASHIER_MOVEMENT.CASH_IN:
          AddAmount = Amount;
          SubAmount = 0;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.MB_CASH_LOST:
          AddAmount = 0;
          SubAmount = Amount;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.CASH_OUT:
          AddAmount = 0;
          SubAmount = Amount;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.PROMOTION_NOT_REDEEMABLE:
        case CASHIER_MOVEMENT.PROMOTION_REDEEMABLE:
        case CASHIER_MOVEMENT.CASH_IN_COVER_COUPON:
          AddAmount = Amount;
          SubAmount = 0;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.PRIZES:
          AddAmount = 0;
          SubAmount = Amount;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.TAX_ON_PRIZE1:
        case CASHIER_MOVEMENT.TAX_ON_PRIZE2:
        case CASHIER_MOVEMENT.SERVICE_CHARGE:
          AddAmount = Amount;
          SubAmount = 0;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.TAX_RETURNING_ON_PRIZE_COUPON:
        case CASHIER_MOVEMENT.DECIMAL_ROUNDING:
          AddAmount = 0;
          SubAmount = Amount;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.CARD_DEPOSIT_IN:
          AddAmount = Amount;
          SubAmount = 0;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.CARD_DEPOSIT_OUT:
          AddAmount = 0;
          SubAmount = Amount;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.CARD_REPLACEMENT:
          AddAmount = Amount;
          SubAmount = 0;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.CANCEL_NOT_REDEEMABLE:
        case CASHIER_MOVEMENT.CANCEL_PROMOTION_REDEEMABLE:
          AddAmount = 0;
          SubAmount = Amount;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.MB_MANUAL_CHANGE_LIMIT:
          AddAmount = Amount;
          SubAmount = 0;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.MB_CASH_IN:
          AddAmount = Amount;
          SubAmount = 0;
          CardTrackData = _card_track_data;
          break;

        //SSC 09-MAR-2012: Note Acceptor
        case CASHIER_MOVEMENT.NA_CASH_IN_SPLIT1:
        case CASHIER_MOVEMENT.MB_CASH_IN_SPLIT1:
          AddAmount = Amount;
          SubAmount = 0;
          CardTrackData = _card_track_data;
          break;

        //SSC 09-MAR-2012: Note Acceptor
        case CASHIER_MOVEMENT.NA_CASH_IN_SPLIT2:
        case CASHIER_MOVEMENT.MB_CASH_IN_SPLIT2:
          AddAmount = Amount;
          SubAmount = 0;
          CardTrackData = _card_track_data;
          break;

        //SSC 11-JUN-2012: Movements for GiftRequest
        case CASHIER_MOVEMENT.POINTS_TO_NOT_REDEEMABLE:
        case CASHIER_MOVEMENT.POINTS_TO_REDEEMABLE:
          AddAmount = Amount;
          SubAmount = 0;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.DRAW_TICKET_PRINT:
        case CASHIER_MOVEMENT.POINTS_TO_DRAW_TICKET_PRINT:
          AddAmount = 0;
          SubAmount = 0;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.HANDPAY:
          AddAmount = 0;
          SubAmount = Amount;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.HANDPAY_CANCELLATION:
          AddAmount = Amount;
          SubAmount = 0;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.MANUAL_HANDPAY:
          AddAmount = 0;
          SubAmount = Amount;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.MANUAL_HANDPAY_CANCELLATION:
          AddAmount = Amount;
          SubAmount = 0;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.CASH_IN_SPLIT1:
          AddAmount = Amount;
          SubAmount = 0;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.CASH_IN_SPLIT2:
          AddAmount = Amount;
          SubAmount = 0;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.DEV_SPLIT1:
          AddAmount = 0;
          SubAmount = Amount;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.DEV_SPLIT2:
          AddAmount = 0;
          SubAmount = Amount;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.CANCEL_SPLIT1:
          AddAmount = 0;
          SubAmount = Amount;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.CANCEL_SPLIT2:
          AddAmount = 0;
          SubAmount = Amount;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.PRIZE_COUPON:
          AddAmount = Amount;
          SubAmount = 0;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.CARD_CREATION:
          AddAmount = 0;
          SubAmount = 0;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.CARD_PERSONALIZATION:
          AddAmount = 0;
          SubAmount = 0;
          CardTrackData = _card_track_data;
          break;
        case CASHIER_MOVEMENT.CARD_RECYCLED:
          AddAmount = 0;
          SubAmount = 0;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.ACCOUNT_PIN_CHANGE:
          AddAmount = 0;
          SubAmount = 0;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.ACCOUNT_PIN_RANDOM:
          AddAmount = 0;
          SubAmount = 0;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.PROMOTION_POINT:
          AddAmount = Amount;
          SubAmount = 0;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.CANCEL_PROMOTION_POINT:
          AddAmount = 0;
          SubAmount = Amount;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.REQUEST_TO_VOUCHER_REPRINT:
          AddAmount = Amount;
          SubAmount = 0;
          break;

        default:
          AddAmount = 0;
          SubAmount = 0;
          break;
      }

      #endregion

    } // GetCashierMovementAddSubAmount

    public static Decimal GetCashierMovementBalanceIncrement(CASHIER_MOVEMENT MovementType, Decimal AddAmount, Decimal SubAmount)
    {
      Decimal _balance_increment;

      switch (MovementType)
      {
        case CASHIER_MOVEMENT.MB_CASH_LOST:
        case CASHIER_MOVEMENT.TAX_ON_PRIZE1:
        case CASHIER_MOVEMENT.TAX_ON_PRIZE2:
        case CASHIER_MOVEMENT.SERVICE_CHARGE:
        case CASHIER_MOVEMENT.PRIZES:
        case CASHIER_MOVEMENT.PROMOTION_NOT_REDEEMABLE:
        case CASHIER_MOVEMENT.PROMOTION_REDEEMABLE:
        case CASHIER_MOVEMENT.CASH_IN_COVER_COUPON:
        case CASHIER_MOVEMENT.CANCEL_NOT_REDEEMABLE:
        case CASHIER_MOVEMENT.CANCEL_PROMOTION_REDEEMABLE:
        case CASHIER_MOVEMENT.MB_MANUAL_CHANGE_LIMIT:
        case CASHIER_MOVEMENT.PROMO_CREDITS:
        case CASHIER_MOVEMENT.PROMO_CANCEL:
        case CASHIER_MOVEMENT.PROMO_TO_REDEEMABLE:
        case CASHIER_MOVEMENT.HANDPAY:
        case CASHIER_MOVEMENT.HANDPAY_CANCELLATION:
        case CASHIER_MOVEMENT.MANUAL_HANDPAY:
        case CASHIER_MOVEMENT.MANUAL_HANDPAY_CANCELLATION:
        case CASHIER_MOVEMENT.CASH_IN_SPLIT1:
        case CASHIER_MOVEMENT.CASH_IN_SPLIT2:
        case CASHIER_MOVEMENT.DEV_SPLIT1:
        case CASHIER_MOVEMENT.DEV_SPLIT2:
        case CASHIER_MOVEMENT.CANCEL_SPLIT1:
        case CASHIER_MOVEMENT.CANCEL_SPLIT2:
        //SSC 09-MAR-2012: Note Acceptor
        case CASHIER_MOVEMENT.NA_CASH_IN_SPLIT1:
        case CASHIER_MOVEMENT.NA_CASH_IN_SPLIT2:
        case CASHIER_MOVEMENT.MB_CASH_IN_SPLIT1:
        case CASHIER_MOVEMENT.MB_CASH_IN_SPLIT2:
        case CASHIER_MOVEMENT.PRIZE_COUPON:
        //case CASHIER_MOVEMENT.DEPOSIT_IN:
        //case CASHIER_MOVEMENT.DEPOSIT_OUT:
        //SSC 11-JUN-2012: Movements for GiftRequest
        case CASHIER_MOVEMENT.POINTS_TO_NOT_REDEEMABLE:
        case CASHIER_MOVEMENT.POINTS_TO_REDEEMABLE:

        case CASHIER_MOVEMENT.NOT_REDEEMABLE_EXPIRED:
        case CASHIER_MOVEMENT.REDEEMABLE_DEV_EXPIRED:
        case CASHIER_MOVEMENT.REDEEMABLE_PRIZE_EXPIRED:

        case CASHIER_MOVEMENT.PROMOTION_POINT:
        case CASHIER_MOVEMENT.CANCEL_PROMOTION_POINT:
        case CASHIER_MOVEMENT.TAX_RETURNING_ON_PRIZE_COUPON:
        case CASHIER_MOVEMENT.DECIMAL_ROUNDING:
        case CASHIER_MOVEMENT.REQUEST_TO_VOUCHER_REPRINT:
          _balance_increment = 0;
          break;

        default:
          _balance_increment = AddAmount - SubAmount;
          break;
      }

      return _balance_increment;
    } // GetCashierMovementBalanceIncrement

  } // CashierMovementsTable
}
