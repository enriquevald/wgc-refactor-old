//------------------------------------------------------------------------------
// Copyright � 2009 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Misc.cs
// 
//   DESCRIPTION: Miscellaneous utilities for WCP and WC2 service
// 
//        AUTHOR: Agust� Poch (Header only)
// 
// CREATION DATE: 28-APR-2009
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 28-APR-2009 APB    First release (Header).
// 23-MAR-2012 RCI    Allow to check license by only valid date (routine CheckLicense).
// 07-JUN-2012 JCM    Add filter function to filter holder with birth today 
//                    Add filter function to filter holders who accomplish a promotion level
// 11-JUN-2012 DDM    Modified the function BirthDayFilter
// 20-JUN-2012 SSC    Added GetTerminalName function.
// 02-JUL-2012 ACC    Added Peru External Events.
// 07-SEP-2012 DDM    Added functions WCPTerminalTypes, WCPTerminalTypes and AllTerminalTypes.
// 12-JUL-2016 FAV         Fixed Bug 15010: Datetime format incorrect
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Net;
using System.Runtime.InteropServices;
using System.Management;
using System.IO;
using System.Xml;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using System.Data.SqlClient;
using System.IO.Compression;
using System.Diagnostics;
using System.Data.SqlTypes;
using System.Data;
using System.Runtime.Serialization.Formatters.Binary;
using System.Globalization;

namespace WSI.Common
{

  public class GetInfo
  {

    public string GetHardDriveSerial()
    {
      //ManagementScope x;
      ConnectionOptions opt;

      opt = new ConnectionOptions();

      opt.Username = @"DPIDC\Administrator";
      opt.Password = "stocking";


      ManagementScope scope =
            new ManagementScope(
            "\\\\CARLES\\root\\cimv2", opt);
      scope.Connect();

      //x = new ManagementScope ("RONALD");

      ManagementObjectSearcher searcher;

      searcher = new ManagementObjectSearcher("SELECT * FROM Win32_PhysicalMedia");
      searcher = new ManagementObjectSearcher("SELECT * FROM win32_logicaldisk");
      searcher.Scope = scope;


      foreach (ManagementObject wmi_HD in searcher.Get())
      {
        if (wmi_HD["VolumeSerialNumber"] != null)
        {
          return wmi_HD["VolumeSerialNumber"].ToString();
        }
        //if ( wmi_HD["SerialNumber"] != null )
        //{
        //  return wmi_HD["SerialNumber"].ToString ();
        //}

      }

      return "";

    }

    /// <summary>
    /// return Volume Serial Number from hard drive
    /// </summary>
    /// <param name="strDriveLetter">[optional] Drive letter</param>
    /// <returns>[string] VolumeSerialNumber</returns>
    public string GetVolumeSerial(string strDriveLetter)
    {
      if (strDriveLetter == "" || strDriveLetter == null) strDriveLetter = "C";
      ManagementObject disk =
          new ManagementObject("win32_logicaldisk.deviceid=\"" + strDriveLetter + ":\"");
      disk.Get();
      return disk["VolumeSerialNumber"].ToString();
    }

    /// <summary>
    /// Returns MAC Address from first Network Card in Computer
    /// </summary>
    /// <returns>[string] MAC Address</returns>
    public string GetMACAddress()
    {
      ManagementObjectSearcher _obj_query = null;
      ManagementObjectCollection _query_collection = null;
      String _mac_address;

      try
      {
        _obj_query = new ManagementObjectSearcher("SELECT * FROM Win32_NetworkAdapterConfiguration");
        _query_collection = _obj_query.Get();
        foreach (ManagementObject _m_object in _query_collection)
        {
          if (_m_object["MacAddress"] == null)
          {
            continue;
          }

          if (_m_object["IPAddress"] == null)
          {
            continue;
          }

          if (!((Boolean)_m_object["IPEnabled"]))
          {
            continue;
          }

          if (_m_object["MacAddress"] != null)
          {
            _mac_address = _m_object["MacAddress"].ToString().Replace(":", "-");

            return _mac_address;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        Log.Error("GetMacAddress: not found.");
      }

      return "";
    }

    /// <summary>
    /// Check if @mac is present
    /// </summary>
    /// <param name="MacToCheck"></param>
    /// <returns></returns>
    public Boolean IsMACAddressPresent(string MacToCheck)
    {
      ManagementClass mc = new ManagementClass("Win32_NetworkAdapterConfiguration");
      ManagementObjectCollection moc = mc.GetInstances();

      foreach (ManagementObject mo in moc)
      {
        try
        {
          if (mo["MacAddress"].ToString().Replace(":", "-") == MacToCheck)
          {
            return true;
          }
        }
        catch
        {
        }
      }

      return false;
    }

    /// <summary>
    /// Return processorId from first CPU in machine
    /// </summary>
    /// <returns>[string] ProcessorId</returns>
    public string GetCPUId()
    {
      string cpuInfo = String.Empty;
      string temp = String.Empty;
      ManagementClass mc = new ManagementClass("Win32_Processor");
      ManagementObjectCollection moc = mc.GetInstances();
      foreach (ManagementObject mo in moc)
      {
        if (cpuInfo == String.Empty)
        {// only return cpuInfo from first CPU
          cpuInfo = mo.Properties["ProcessorId"].Value.ToString();
        }
      }
      return cpuInfo;
    }
  }





  static public class Performance
  {
    private static Stopwatch m_stop_watch = Stopwatch.StartNew();

    public static TimeSpan GetTickCount()
    {
      return m_stop_watch.Elapsed;
    }
    public static TimeSpan GetElapsedTicks(TimeSpan FirstSample)
    {
      return GetElapsedTicks(FirstSample, m_stop_watch.Elapsed);
    }
    public static TimeSpan GetElapsedTicks(TimeSpan FirstSample, TimeSpan LastSample)
    {
      ulong _t0;
      ulong _t1;
      ulong _elapsed;

      _t0 = (ulong)(FirstSample.Ticks);
      _t1 = (ulong)(LastSample.Ticks);
      _elapsed = _t1;
      if (_t1 < _t0)
      {
        _elapsed += ulong.MaxValue;
        _elapsed += 1;
      }
      _elapsed -= _t0;

      if (_elapsed > long.MaxValue)
      {
        _elapsed = long.MaxValue;
      }

      return new TimeSpan((long)_elapsed);
    }
  }

  static public class Misc
  {

    //------------------------------------------------------------------------------
    // PURPOSE: Get the ProviderId from the terminal with the terminal_type SITE.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - ProviderId
    //
    public static String GetSiteProviderId()
    {
      SqlCommand _sql_cmd;
      StringBuilder _sql_str;
      String _provider_id;

      _provider_id = "";

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sql_str = new StringBuilder();
          _sql_str.AppendLine("SELECT   TOP 1 TE_PROVIDER_ID ");
          _sql_str.AppendLine("  FROM   TERMINALS ");
          _sql_str.AppendLine(" WHERE   TE_TERMINAL_TYPE = @pTerminalTypeSite ");
          _sql_str.AppendLine("ORDER BY TE_TERMINAL_ID DESC ");

          _sql_cmd = new SqlCommand(_sql_str.ToString());
          _sql_cmd.Parameters.Add("@pTerminalTypeSite", SqlDbType.SmallInt).Value = (Int16)TerminalTypes.SITE;

          _provider_id = (String)_db_trx.ExecuteScalar(_sql_cmd);
          if (_provider_id == null)
          {
            _provider_id = "";
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _provider_id;
    } // GetSiteProviderId

    //------------------------------------------------------------------------------
    // PURPOSE: Concat the provider and the terminal with a specific separator.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - String Provider
    //          - String Terminal
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - String
    //
    public static String ConcatProviderAndTerminal(String Provider, String Terminal)
    {
      return Provider + " - " + Terminal;
    } // ConcatProviderAndTerminal


    //------------------------------------------------------------------------------
    // PURPOSE:  Type of terminals used in command
    //           All terminals type, except:  3gs, mobile_bank_imb, istats.
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - List of TerminalTypes
    //
    public static TerminalTypes[] WCPTerminalTypes()
    {
      TerminalTypes[] _terminal_types;
      TerminalTypes[] _all_terminal_types;
      Int32 _idx_terminal_type;

      _idx_terminal_type = 0;
      _all_terminal_types = AllTerminalTypes();
      _terminal_types = new TerminalTypes[_all_terminal_types.Length - 3];

      foreach (TerminalTypes _type in _all_terminal_types)
      {
        if (_type != TerminalTypes.T3GS && _type != TerminalTypes.ISTATS && _type != TerminalTypes.MOBILE_BANK_IMB)
        {
          _terminal_types[_idx_terminal_type] = _type;
          _idx_terminal_type++;
        }
      }

      return _terminal_types;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: All terminals types, expcept 3GS.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - List of TerminalTypes
    //
    public static TerminalTypes[] AllTerminalTypesExcept3GS()
    {
      TerminalTypes[] _terminal_types;
      TerminalTypes[] _all_terminal_types;
      Int32 _index;

      _index = 0;
      _all_terminal_types = AllTerminalTypes();
      _terminal_types = new TerminalTypes[_all_terminal_types.Length - 1];

      foreach (TerminalTypes _type in _all_terminal_types)
      {
        if (_type != TerminalTypes.T3GS)
        {
          _terminal_types[_index] = _type;
          _index++;
        }
      }

      return _terminal_types;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: List of gaming terminal types
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - List of TerminalTypes
    //
    public static TerminalTypes[] AcceptorTerminalTypeList()
    {
      return new TerminalTypes[] { TerminalTypes.SAS_HOST, TerminalTypes.PROMOBOX };
    } // AcceptorTerminalTypeList

    //------------------------------------------------------------------------------
    // PURPOSE: All terminals types.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - List of TerminalTypes
    //
    // NOTES:
    //    IMPORTANT! If you add a new terminal type, increment GUI --> TYPE_TERMINAL_SEL_PARAMS.NUM_CHECKBOXES.
    public static TerminalTypes[] AllTerminalTypes()
    {
      return new TerminalTypes[] { TerminalTypes.WIN, TerminalTypes.T3GS, TerminalTypes.SAS_HOST, TerminalTypes.SITE_JACKPOT, 
                                   TerminalTypes.MOBILE_BANK, TerminalTypes.MOBILE_BANK_IMB, TerminalTypes.ISTATS, TerminalTypes.PROMOBOX};


    } // AllTerminalTypes

    //------------------------------------------------------------------------------
    // PURPOSE: List of gaming terminal types
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - List of TerminalTypes
    //
    public static TerminalTypes[] GamingTerminalTypeList()
    {
      return new TerminalTypes[] { TerminalTypes.WIN, TerminalTypes.T3GS, TerminalTypes.SAS_HOST };
    } // GamingTerminalTypeList

    //------------------------------------------------------------------------------
    // PURPOSE: List of gaming terminal types as String
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - String of list of TerminalTypes
    //
    public static String GamingTerminalTypeListToString()
    {
      String _str_in;

      _str_in = "";
      foreach (TerminalTypes _type in GamingTerminalTypeList())
      {
        _str_in += (Int16)_type + ", ";
      }
      if (_str_in.Length > 1)
      {
        _str_in = _str_in.Substring(0, _str_in.Length - 2);
      }

      return _str_in;
    } // GamingTerminalTypeListToString

    //------------------------------------------------------------------------------
    // PURPOSE: Generates and return the visible trackdata
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - String in this format "*****************1234"
    //      - Where 1234 is de visible part of the trackdata
    // 
    //   NOTES:
    //    
    public static String VisibleTrackdata(String TrackData)
    {
      Int32 _trackdata_length;
      Int32 _visible_length;
      String _trackdata_visible;

      if (TrackData == null)
      {
        return "";
      }

      _trackdata_length = TrackData.Length;
      _visible_length = _trackdata_length;

      try
      {
        _visible_length = Int32.Parse(WSI.Common.Misc.ReadGeneralParams("Cashier", "TrackDataVisibleChars"));
        _visible_length = Math.Max(_visible_length, 0);
        _visible_length = Math.Min(_visible_length, _trackdata_length);
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      _trackdata_visible = new string('*', _trackdata_length - _visible_length);
      _trackdata_visible = _trackdata_visible + TrackData.Substring(_trackdata_length - _visible_length);

      return _trackdata_visible;
    } // VisibleTrackdata

    /// <summary>
    /// Get a general parameters value
    /// </summary>
    /// <returns>Site id.</returns>
    public static String ReadGeneralParams(String Group, String Subject)
    {
      return GeneralParam.Value(Group, Subject);

    } // ReadGeneralParams

    /// <summary>
    /// Get a general parameters value
    /// </summary>
    /// <returns>Site id.</returns>
    public static String ReadGeneralParams(String Group, String Subject, SqlTransaction SqlTrx)
    {
      return GeneralParam.Value(Group, Subject);

    } // ReadGeneralParams

    static public long GetElapsedTicks(int FirstTick, int LastTick)
    {
      long _elapsed;

      _elapsed = LastTick;
      if (LastTick < FirstTick)
      {
        _elapsed += UInt32.MaxValue;
        _elapsed += 1;
      }
      _elapsed -= FirstTick;

      if (_elapsed < 0)
      {
        Log.Error("*** Computed elapsed time is less than zero.");
        _elapsed = 0;
      }

      return _elapsed;
    }

    static public int GetTickCount()
    {
      return Environment.TickCount;
    }

    static public long GetElapsedTicks(int FirstTick)
    {
      return GetElapsedTicks(FirstTick, GetTickCount());
    }

    /// <summary>
    /// Converts the given string to an IPEndPoint
    /// </summary>
    /// <param name="IPAddressPort"></param>
    /// <returns></returns>
    static public IPEndPoint IPEndPointParse(String IPAddressPort)
    {
      int idx;
      IPAddress ip;
      int port;

      idx = IPAddressPort.IndexOf(':');
      ip = IPAddress.Parse(IPAddressPort.Substring(0, idx));
      port = int.Parse(IPAddressPort.Substring(1 + idx));

      return new IPEndPoint(ip, port);
    }

    static public UInt32 Checksum(Byte[] Data)
    {
      const UInt16 C1 = 52845;
      const UInt16 C2 = 22719;
      const UInt16 C3 = 55665;

      Byte cipher;
      UInt16 r;
      UInt32 checksum;

      r = C3;
      checksum = 0;

      for (int i = 0; i < Data.Length; i++)
      {
        cipher = (Byte)(Data[i] ^ (Byte)(r >> 8));
        r = (UInt16)((cipher + r) * C1 + C2);
        checksum += cipher;
      }

      return checksum;
    } // Checksum

    /// <summary>
    /// Delete log files of the log directory
    /// </summary>
    /// <param name="Path"></param>
    /// <param name="MaskFile"></param>
    /// <param name="NumDays"></param>
    static public void ClearHistoryLogFiles(String LogPath, String MaskFile, Int32 NumDays)
    {
      DirectoryInfo dir;

      try
      {
        dir = new DirectoryInfo(LogPath);

        // Search all files in directory
        foreach (FileInfo file_info in dir.GetFiles(MaskFile))
        {
          if (file_info.LastWriteTime.AddDays(NumDays) < DateTime.Now)
          {
            file_info.Delete();
          }
        }
      }
      catch
      {
        ;
      }
    } // ClearHistoryLogFiles

    public static Boolean VerifyXmlDoc(XmlDocument Doc, RSA Key)
    {
      // Create a new SignedXml object and pass it
      // the XML document class.
      SignedXml signedXml = new SignedXml(Doc);

      // Find the "Signature" node and create a new
      // XmlNodeList object.
      XmlNodeList nodeList = Doc.GetElementsByTagName("Signature");

      if (nodeList.Count != 1)
      {
        return false;
      }

      // Load the signature node.
      signedXml.LoadXml((XmlElement)nodeList[0]);

      // Check the signature and return the result.
      return signedXml.CheckSignature(Key);
    }

    #region Events

    // For notify License status changes.
    public delegate void LicenseStatusChangedDelegate(LicenseStatus LicenseStatus);
    private static event LicenseStatusChangedDelegate OnLicenseStatusChanged;

    #endregion

    public class LicenseParameters
    {
      #region Private properties
      private Boolean m_check_only_valid_date;
      private List<string> m_features;
      #endregion

      #region Public properties
      public Boolean CheckOnlyValidDate
      {
        get { return m_check_only_valid_date; }
        set { m_check_only_valid_date = value; }
      }
      public List<string> Features
      {
        get { return m_features; }
        set { m_features = value; }
      }
      #endregion

      #region Public methods
      public LicenseParameters()
      {
        m_check_only_valid_date = false;
        m_features = new List<string>();
      }

      //------------------------------------------------------------------------------
      // PURPOSE : Add a feature into feature list.
      //
      //  PARAMS :
      //      - INPUT :
      //          
      //      - OUTPUT :
      //            
      //
      // RETURNS :
      //      - Boolean value
      //
      //   NOTES :
      //------------------------------------------------------------------------------
      public void AddFeature(String StrFeature)
      {
        if (!m_features.Contains(StrFeature))
        {
          m_features.Add(StrFeature);
        }
      }
      #endregion
    }

    public static void StartLicenseChecker(LicenseStatusChangedDelegate LicenseStatusChangedHandler)
    {
      StartLicenseChecker(LicenseStatusChangedHandler, false, new List<string>());
    }

    public static void StartLicenseChecker(LicenseStatusChangedDelegate LicenseStatusChangedHandler, Boolean CheckOnlyValidDate, List<string> Features)
    {
      System.Threading.Thread _thread;
      LicenseParameters _lisence_params = new LicenseParameters();

      _lisence_params.CheckOnlyValidDate = CheckOnlyValidDate;
      _lisence_params.Features = Features;

      OnLicenseStatusChanged += LicenseStatusChangedHandler;
      _thread = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(LicenseCheckerThread));
      _thread.Start(_lisence_params);
    }

    private static void LicenseCheckerThread(Object LisenceParams)
    {
      Boolean _check_only_valid_date;
      DateTime _not_valid_after;
      TimeSpan _left;
      TimeSpan _wait_hint;
      DateTime _now;
      LicenseStatus _license_status;
      LicenseStatus _previous_license_status;
      LicenseParameters _lisence_params;
      List<string> _features;

      _lisence_params = (LicenseParameters)LisenceParams;

      _check_only_valid_date = _lisence_params.CheckOnlyValidDate;
      _features = _lisence_params.Features;

      _license_status = LicenseStatus.NOT_CHECKED;

      _wait_hint = new TimeSpan();

      while (true)
      {
        System.Threading.Thread.Sleep(_wait_hint);
        _wait_hint = new TimeSpan(0, 1, 0);

        _now = WASDB.Now;

        _previous_license_status = _license_status;
        _license_status = Misc.CheckLicense(_now, _check_only_valid_date, _features, out _not_valid_after);

        // Error reading the license
        if (_license_status == LicenseStatus.NOT_CHECKED)
        {
          continue;
        }

        if (_license_status != _previous_license_status)
        {
          OnLicenseStatusChanged(_license_status);
        }

        if (_license_status != LicenseStatus.VALID && _license_status != LicenseStatus.VALID_EXPIRES_SOON)
        {
          continue;
        }

        _left = _not_valid_after.AddDays(1).Subtract(_now);
        if (_left.Ticks > 0)
        {
          if (_left.TotalDays > 1)
          {
            _wait_hint = new TimeSpan(1, 0, 0, 0);
          }
          else
          {
            _wait_hint = new TimeSpan(_left.Ticks / 2);
          }
        }
      }
    }

    private static LicenseStatus CheckLicense(DateTime CurrentDateTime, Boolean CheckOnlyValidDate, List<string> Features, out DateTime LicenseNotValidAfter)
    {
      string xml_public_key = "<RSAKeyValue><Modulus>1jZt5vR4VWWog3xZkGdyNjI2i+1F8zvAirwpR4aSVN1qSiMi1uRk2fuVYsgKYlYUDqzaaKjAuGr6DmKYeTTZUhSQS+vkL/6iLxnR52+5adJCD40AgHGIwm05lUegH8oWasVdNQXlIWEsVgiDSLLajA8clWUOXdFrdTLa07urppc=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>";

      XmlDocument doc;
      RSACryptoServiceProvider Key;

      SqlConnection sql_conn;
      SqlCommand sql_command;
      SqlDataReader sql_reader;
      String sql_str;

      String aux_text;
      StringReader str_reader;
      XmlReader xml_reader;

      LicenseStatus _license_status;

      _license_status = LicenseStatus.INVALID;
      LicenseNotValidAfter = DateTime.MinValue;

      try
      {
        Key = new RSACryptoServiceProvider();
        Key.FromXmlString(xml_public_key);

        sql_conn = WASDB.Connection();

        //
        // Read Licence from database
        //
        sql_str = "  SELECT TOP (1) WL_LICENCE " +
                  "    FROM LICENCES " +
                  "ORDER BY WL_INSERTION_DATE DESC";

        sql_command = new SqlCommand(sql_str);
        sql_command.Connection = sql_conn;

        sql_reader = sql_command.ExecuteReader();
        if (sql_reader.HasRows)
        {
          sql_reader.Read();
          aux_text = sql_reader.GetString(0);

          str_reader = new StringReader(aux_text);
          xml_reader = XmlReader.Create(str_reader);
        }
        else
        {
          Log.Message("*** License: DB Table not found or empty");

          return _license_status;
        }

        doc = new XmlDocument();
        try
        {
          //doc.Load("WSI.License.xml");
          doc.Load(xml_reader);
        }
        catch
        {
          Log.Message("*** License: NOT FOUND");
        }

        if (!VerifyXmlDoc(doc, Key))
        {
          Log.Message("*** License: NOT VALID");

          return _license_status;
        }

        sql_reader.Close();

        _license_status = LicenseStatus.NOT_AUTHORIZED;

        XmlNode site_license;
        XmlNode xml_node;

        if (CheckOnlyValidDate)
        {
          Int32 _gp_site_id;
          Int32 _xml_site_id;
          XmlNodeList _sites = doc.GetElementsByTagName("SiteLicense");

          if (!Int32.TryParse(Misc.ReadGeneralParams("Site", "Identifier"), out _gp_site_id))
          {
            Log.Message("*** License: General Parameter 'Site.Identifier' not set.");

            return _license_status;
          }

          site_license = null;
          foreach (XmlNode _site in _sites)
          {
            XmlNode _site_id;
            _site_id = _site.Attributes.GetNamedItem("SiteId");
            if (!Int32.TryParse(_site_id.Value, out _xml_site_id))
            {
              continue;
            }

            if (_xml_site_id == _gp_site_id)
            {
              site_license = _site;

              break;
            }
          }

          if (site_license == null)
          {
            Log.Message("*** License: SITE NOT AUTHORIZED (Not found)");

            return _license_status;
          }
        }
        else
        {
          XmlNodeList servers = doc.GetElementsByTagName("Server");
          XmlNode site_server;

          site_license = null;
          site_server = null;
          foreach (XmlNode server in servers)
          {
            XmlNode server_name;
            server_name = server.Attributes.GetNamedItem("Name");
            if (server_name.Value == Environment.MachineName)
            {
              site_server = server;
              site_license = server.ParentNode.ParentNode;

              break;
            }
          }

          if (site_license == null
              || site_server == null)
          {
            Log.Message("*** License: SERVER NOT AUTHORIZED (Not found)");

            return _license_status;
          }

          // Check Volume Serial Number
          string serial_number;
          GetInfo get_info;

          get_info = new GetInfo();

          xml_node = site_server.Attributes.GetNamedItem("VolumeSerialNumber");
          if (xml_node.Value != "ANY")
          {
            serial_number = get_info.GetVolumeSerial("C");
            if (serial_number != xml_node.Value.Replace("-", ""))
            {
              Log.Message("*** License: SERVER NOT AUTHORIZED (Volume Serial Number)");

              return _license_status;
            }
          }

          // Check @mac
          xml_node = site_server.Attributes.GetNamedItem("MacAddress");
          if (xml_node.Value != "ANY")
          {
            if (!get_info.IsMACAddressPresent(xml_node.Value))
            {
              Log.Message("*** License: SERVER NOT AUTHORIZED (MAC Address)");

              return _license_status;
            }
          }
        }

        _license_status = LicenseStatus.EXPIRED;

        // Check Date
        xml_node = site_license.Attributes.GetNamedItem("LicenseNotValidAfter");
        LicenseNotValidAfter = DateTime.Parse(xml_node.Value);
        if (CurrentDateTime >= LicenseNotValidAfter.AddDays(1))
        {
          Log.Message("*** License: EXPIRED");

          return _license_status;
        }

        //Check Features
        if (Features.Count > 0)
        {
          foreach (string _feature in Features)
          {
            XmlNodeList _xml_features = site_license.SelectNodes("Features");

            if (_xml_features.Count != 1)
            {
              Log.Message("*** License: NO FEATURES INSTALLED");

              return _license_status;
            }

            foreach (XmlNode _xml_feature_node in _xml_features[0])
            {
              Boolean _feature_enabled = false;

              if (_xml_feature_node.Attributes.GetNamedItem("Name").Value == _feature)
              {
                if (!Boolean.TryParse(_xml_feature_node.Attributes.GetNamedItem("Enabled").Value, out _feature_enabled))
                {
                  Log.Message("*** License Enabled Parameter: not set");

                }

                if (!_feature_enabled)
                {
                  Log.Message("*** License: DISABLED");

                  return _license_status;
                }
              }
            }
          }
        }

        TimeSpan ts = LicenseNotValidAfter.AddDays(1).Subtract(CurrentDateTime);
        int days_left = ((int)ts.TotalDays);

        if (days_left < 30)
        {
          string msg;
          msg = "Your license expires in " + days_left.ToString() + " days.";

          Log.Message("*** License: " + msg);
        }

        _license_status = LicenseStatus.VALID;

        if (days_left < 7)
        {
          _license_status = LicenseStatus.VALID_EXPIRES_SOON;
        }
      }
      catch (SqlException _sql_ex)
      {
        if (_sql_ex.Number == -2) // Connection timeout
        {
          Log.Warning(" *** Sql Database Timeout. License not validated.");

          return LicenseStatus.NOT_CHECKED;
        }
        else
        {
          Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
        }
      }
      catch (Exception ex)
      {
        Log.Message("*** License: Validation failed");
        Log.Exception(ex);
      }

      return _license_status;
    } // CheckLicense

    public static Boolean CheckLicenseFromFile(String LicenceFile, out String LicenceText, out DateTime LicenseNotValidAfter)
    {
      string xml_public_key = "<RSAKeyValue><Modulus>1jZt5vR4VWWog3xZkGdyNjI2i+1F8zvAirwpR4aSVN1qSiMi1uRk2fuVYsgKYlYUDqzaaKjAuGr6DmKYeTTZUhSQS+vkL/6iLxnR52+5adJCD40AgHGIwm05lUegH8oWasVdNQXlIWEsVgiDSLLajA8clWUOXdFrdTLa07urppc=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>";

      Boolean is_valid;
      XmlDocument doc;
      RSACryptoServiceProvider Key;

      is_valid = false;
      LicenseNotValidAfter = WASDB.Now.Date;
      LicenceText = "";

      try
      {
        Key = new RSACryptoServiceProvider();
        Key.FromXmlString(xml_public_key);

        doc = new XmlDocument();
        try
        {
          doc.Load(LicenceFile);
          //doc.Load(xml_reader);
        }
        catch (Exception _ex)
        {
          Log.Message("*** License: NOT FOUND" + _ex);
        }

        if (!VerifyXmlDoc(doc, Key))
        {
          Log.Message("*** License: NOT VALID");

          return false;
        }

        XmlNode xml_node;
        XmlNode site_server;
        XmlNode site_licence;
        XmlNodeList servers = doc.GetElementsByTagName("Server");

        site_server = null;
        site_licence = null;
        foreach (XmlNode server in servers)
        {
          site_server = server;
          site_licence = server.ParentNode.ParentNode;

          // Check Date
          xml_node = site_licence.Attributes.GetNamedItem("LicenseNotValidAfter");
          LicenseNotValidAfter = DateTime.Parse(xml_node.Value);
        }

        LicenceText = doc.InnerXml;

        is_valid = true;
      }

      catch (Exception ex)
      {
        Log.Message("*** License: Validation failed");
        Log.Exception(ex);
      }

      return is_valid;
    } // CheckLicenseFromFile


    //------------------------------------------------------------------------------
    // PURPOSE : Deflate String
    //
    //  PARAMS :
    //      - INPUT :
    //          - StringToDeflate
    //
    //      - OUTPUT :
    //          - DeflatedString
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public static void DeflateString(String StringToDeflate, out String DeflatedString)
    {
      MemoryStream _mem_stream;
      DeflateStream _compress;
      Byte[] _logger;

      _logger = Encoding.UTF8.GetBytes(StringToDeflate);

      _mem_stream = new MemoryStream();

      // Compressed
      _compress = new DeflateStream(_mem_stream, CompressionMode.Compress, true);
      _compress.Write(_logger, 0, _logger.Length);
      _compress.Close();
      _compress = null;

      DeflatedString = Convert.ToBase64String(_mem_stream.GetBuffer(), 0, (int)_mem_stream.Length);

    } // DeflateString

    //------------------------------------------------------------------------------
    // PURPOSE : Deflate String
    //
    //  PARAMS :
    //      - INPUT :
    //          - StringToInflate
    //
    //      - OUTPUT :
    //          - InflatedString
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public static void InflateString(String StringToInflate, out String InflatedString)
    {
      MemoryStream _mem_stream;
      DeflateStream _compress;
      Byte[] _logger;
      int _len;

      _logger = Convert.FromBase64String(StringToInflate);
      _mem_stream = new MemoryStream(_logger);

      // Decompressed
      _logger = new Byte[(64 * 1024)];
      _compress = new DeflateStream(_mem_stream, CompressionMode.Decompress, true);
      _len = _compress.Read(_logger, 0, _logger.Length);
      _compress.Close();
      _compress = null;

      InflatedString = Encoding.UTF8.GetString(_logger, 0, _len);

    } // InflateString


    /// <summary>
    /// Returns true if the given terminal name exists in the database
    /// </summary>
    /// <param name="ExternalTerminalId"></param>
    /// <param name="Type"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public static Boolean GetTerminalInfo(String ExternalTerminalId, int Type,
                                          out Int32 TerminalId, out String ProviderId, out String TerminalName, out TerminalStatus Status, out String ServerExternalId,
                                          SqlTransaction Trx)
    {
      SqlCommand _sql_cmd;
      SqlDataReader _sql_reader;
      String _cmd_txt;

      _sql_reader = null;
      TerminalId = 0;
      ProviderId = "";
      TerminalName = "";
      Status = 0;
      ServerExternalId = "";

      if (ExternalTerminalId.Length > 40)
      {
        ExternalTerminalId = ExternalTerminalId.Substring(0, 40);
      }

      // 
      // Get Terminal Id
      //
      _cmd_txt = "";
      _cmd_txt += "SELECT   TE_TERMINAL_ID ";
      _cmd_txt += "       , TE_PROVIDER_ID ";
      _cmd_txt += "       , TE_NAME        ";
      _cmd_txt += "       , TE_STATUS      ";
      _cmd_txt += "       , (SELECT TE_EXTERNAL_ID FROM TERMINALS WHERE TE_TERMINAL_ID = T.TE_SERVER_ID) ";
      _cmd_txt += "  FROM   TERMINALS T   ";
      _cmd_txt += " WHERE   TE_TYPE        = @pType ";
      _cmd_txt += "   AND   TE_EXTERNAL_ID = @pExternalId ";

      _sql_cmd = new SqlCommand(_cmd_txt);
      _sql_cmd.Connection = Trx.Connection;
      _sql_cmd.Transaction = Trx;

      _sql_cmd.Parameters.Add("@pType", SqlDbType.Int).Value = Type;
      _sql_cmd.Parameters.Add("@pExternalId", SqlDbType.NVarChar).Value = ExternalTerminalId;

      try
      {
        _sql_reader = _sql_cmd.ExecuteReader();
        if (_sql_reader.Read())
        {
          TerminalId = _sql_reader.GetInt32(0);
          ProviderId = _sql_reader.IsDBNull(1) ? "" : _sql_reader.GetString(1);
          TerminalName = _sql_reader.IsDBNull(2) ? "" : _sql_reader.GetString(2);
          Status = (TerminalStatus)_sql_reader.GetInt32(3);
          ServerExternalId = _sql_reader.IsDBNull(4) ? "" : _sql_reader.GetString(4);

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
        if (_sql_reader != null)
        {
          _sql_reader.Close();
          _sql_reader.Dispose();
          _sql_reader = null;
        }
      }

      return false;
    } // GetTerminalInfo

    //------------------------------------------------------------------------------
    // PURPOSE: Get Terminal name by TerminalId
    // 
    //  PARAMS:
    //      - INPUT:
    //          - TerminalId
    //          - SqlTrx
    //
    //      - OUTPUT:
    //          - TerminalName
    //
    // RETURNS:
    //    - Boolean:
    //
    public static Boolean GetTerminalName(Int32 TerminalId, out String TerminalName, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      Object _obj;

      TerminalName = "";

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("SELECT   TE_NAME                       ");
        _sb.AppendLine("  FROM   TERMINALS                     ");
        _sb.AppendLine(" WHERE   TE_TERMINAL_ID = @pTerminalId ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;

          _obj = _sql_cmd.ExecuteScalar();

          if (_obj != null && _obj != DBNull.Value)
          {
            TerminalName = (String)_obj;

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } //GetTerminalName

    //------------------------------------------------------------------------------
    // PURPOSE: Returns the Opening DateTime according to the DateTime, OpeningHour and OpeningMinutes parameters
    // 
    //  PARAMS:
    //      - INPUT:
    //          - DateTime
    //          - OpeningHour
    //          - OpeningMinutes
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //    - DateTime
    //
    public static DateTime Opening(DateTime DateTime, Int32 OpeningHour, Int32 OpeningMinutes)
    {
      DateTime _today_opening;
      DateTime _now;

      _now = DateTime;
      _today_opening = _now;

      _today_opening = new DateTime(_today_opening.Year, _today_opening.Month, _today_opening.Day,
                                   OpeningHour, OpeningMinutes, 0);
      if (_today_opening > _now)
      {
        _today_opening = _today_opening.AddDays(-1);
      }

      return _today_opening;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Returns the Opening DateTime according to the DateTime parameter
    // 
    //  PARAMS:
    //      - INPUT:
    //          - DateTime
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //    - DateTime
    //
    public static DateTime Opening(DateTime DateTime)
    {
      Int32 _opening_hour;
      Int32 _opening_minutes;

      Int32.TryParse(Misc.ReadGeneralParams("WigosGUI", "ClosingTime"), out _opening_hour);
      Int32.TryParse(Misc.ReadGeneralParams("WigosGUI", "ClosingTimeMinutes"), out _opening_minutes);

      return Opening(DateTime, _opening_hour, _opening_minutes);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Returns the Opening DateTime
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //    - DateTime
    //
    public static DateTime TodayOpening()
    {
      return Opening(WASDB.Now);
    } // OpeningDateTime

    //------------------------------------------------------------------------------
    // PURPOSE: Converts an hex string representation to the string
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Hex
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //    - String
    //
    public static String HexToString(String Hex)
    {
      String _one_char;
      String _str_out;
      //

      _str_out = "";

      if (Hex.Length % 2 != 0)
      {
        Hex = "0" + Hex;
      }

      while (Hex.Length > 0)
      {
        // First take two Hex value using Substring.
        // Then convert Hex value into ascii.
        // Then convert ascii value into character.
        _one_char = Convert.ToChar(Convert.ToUInt32(Hex.Substring(0, 2), 16)).ToString();

        _str_out += _one_char;
        Hex = Hex.Substring(2, Hex.Length - 2);
      }
      return _str_out;
    } // HexToString

    //------------------------------------------------------------------------------
    // PURPOSE: Converts an string to its hex representation
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Data
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //    - String
    //
    public static String StringToHex(String Data)
    {
      String _one_char;
      String _hex_out;

      _hex_out = "";

      // First take each character using ToCharArray.
      // Then convert character into ascii.
      // Then convert ascii value into Hex format.
      foreach (Char c in Data.ToCharArray())
      {
        _one_char = String.Format("{0:X}", Convert.ToUInt32(c));
        _hex_out += _one_char;
      }
      return _hex_out;
    } // StringToHex

    //------------------------------------------------------------------------------
    // PURPOSE : DataSet to Xml
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataSet DataSet
    //          - XmlWriteMode WriteMode
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String
    //
    public static String DataSetToXml(DataSet DataSet, XmlWriteMode WriteMode)
    {
      String _xml;

      _xml = "";
      using (StringWriter _sw = new StringWriter())
      {
        DataSet.WriteXml(_sw, WriteMode);
        _xml = _sw.ToString();
      }

      return _xml;
    } // DataSetToXml

    //------------------------------------------------------------------------------
    // PURPOSE : DataSet to Xml
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataSet DataSet
    //          - XmlWriteMode WriteMode
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String
    //
    public static void DataSetFromXml(DataSet DataSet, String Xml, XmlReadMode ReadMode)
    {
      using (StringReader _sr = new StringReader(Xml))
      {
        DataSet.ReadXml(_sr, ReadMode);
      }

    } // DataSetFromXml


    //------------------------------------------------------------------------------
    // PURPOSE : Calculate the Adler32 of an array of bytes
    //
    //  PARAMS :
    //      - INPUT :
    //          - Byte[] Bytes
    //          - Int32 Length
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Int32
    //
    private static Int32 Adler32(Byte[] Buffer, Int32 Length)
    {
      const UInt32 _a32mod = 65521;
      UInt32 _s1 = 1;
      UInt32 _s2 = 0;

      for (Int32 _idx_buf = 0; _idx_buf < Length; _idx_buf++)
      {
        _s1 = (_s1 + Buffer[_idx_buf]) % _a32mod;
        _s2 = (_s2 + _s1) % _a32mod;
      }

      return unchecked((Int32)((_s2 << 16) + _s1));
    } // Adler32

    //------------------------------------------------------------------------------
    // PURPOSE : DataSet to Xml
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataSet DataSet
    //          - DataSetRawFormat Format
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String
    //
    public static String DataSetToBase64(DataSet DataSet, DataSetRawFormat Format)
    {
      MemoryStream _ms_inflated;
      MemoryStream _ms_deflated;
      DeflateStream _df;
      String _xml64;

      if (DataSet == null)
      {
        return "";
      }

      _xml64 = "";

      _ms_inflated = new MemoryStream();
      _ms_deflated = new MemoryStream();
      _df = new DeflateStream(_ms_deflated, CompressionMode.Compress, true);

      switch (Format)
      {
        case DataSetRawFormat.BIN_DEFLATE_BASE64:
          {
            BinaryFormatter _formatter;
            SerializationFormat _sformat;

            // Serialize BinXml
            _formatter = new BinaryFormatter();
            _sformat = DataSet.RemotingFormat;
            DataSet.RemotingFormat = SerializationFormat.Binary;
            _formatter.Serialize(_ms_inflated, DataSet);
            DataSet.RemotingFormat = _sformat;

            // Compress
            _df.Write(_ms_inflated.GetBuffer(), 0, (Int32)_ms_inflated.Position);
            _df.Close();
          }
          break;

        case DataSetRawFormat.XML_ZLIB_BASE64:
          {
            Byte[] _buffer;
            Int32 _adler32;

            DataSet.WriteXml(_ms_inflated, XmlWriteMode.WriteSchema);

            // Compress
            _ms_deflated.WriteByte(0x58);
            _ms_deflated.WriteByte(0x85);
            _buffer = _ms_inflated.GetBuffer();

            _df.Write(_buffer, 0, (Int32)_ms_inflated.Position);
            _df.Close();

            _adler32 = Adler32(_buffer, (Int32)_ms_inflated.Position);
            _adler32 = IPAddress.HostToNetworkOrder(_adler32);
            _ms_deflated.Write(BitConverter.GetBytes(_adler32), 0, sizeof(Int32));
          }
          break;
      }

      _xml64 = Convert.ToBase64String(_ms_deflated.GetBuffer(), 0, (Int32)_ms_deflated.Position, Base64FormattingOptions.InsertLineBreaks);

      _df.Dispose();
      _ms_deflated.Dispose();
      _ms_inflated.Dispose();

      return _xml64;
    } // DataSetToBase64

    //------------------------------------------------------------------------------
    // PURPOSE : Xml to DataSet
    //
    //  PARAMS :
    //      - INPUT :
    //          - String Base64
    //          - DataSetRawFormat Format
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String
    //
    public static DataSet DataSetFromBase64(String Base64, DataSetRawFormat Format)
    {
      MemoryStream _ms_inflated;
      MemoryStream _ms_deflated;
      DeflateStream _df;
      Byte[] _aux;
      Int32 _num_read;
      DataSet _ds;

      if (String.IsNullOrEmpty(Base64))
      {
        return null;
      }

      _ds = null;

      _aux = Convert.FromBase64String(Base64);

      _ms_inflated = new MemoryStream();
      if (Format == DataSetRawFormat.XML_ZLIB_BASE64)
      {
        // Skip the Zlib header (2 bytes)
        // Lenght without Zlib header (2) and CRC (4).
        _ms_deflated = new MemoryStream(_aux, 2, _aux.Length - (2 + 4));
      }
      else
      {
        _ms_deflated = new MemoryStream(_aux);
      }
      _df = new DeflateStream(_ms_deflated, CompressionMode.Decompress, true);

      _aux = new Byte[8 * 1024];

      do
      {
        _num_read = _df.Read(_aux, 0, _aux.Length);
        _ms_inflated.Write(_aux, 0, _num_read);

      } while (_num_read > 0);

      _ms_inflated.Position = 0;

      switch (Format)
      {
        case DataSetRawFormat.BIN_DEFLATE_BASE64:
          {
            BinaryFormatter _formatter;

            _formatter = new BinaryFormatter();
            _ds = (DataSet)_formatter.Deserialize(_ms_inflated);
          }
          break;
        case DataSetRawFormat.XML_ZLIB_BASE64:
          {
            _ds = new DataSet();
            _ds.ReadXml(_ms_inflated);
          }
          break;
      }

      _df.Dispose();
      _ms_deflated.Dispose();
      _ms_inflated.Dispose();

      return _ds;
    } // DataSetFromBase64

    //------------------------------------------------------------------------------
    // PURPOSE : Write a String to a file
    //
    //  PARAMS :
    //      - INPUT :
    //          - String Filename
    //          - String TextToWrite
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //
    public static Boolean WriteStringToFile(String Filename, String TextToWrite)
    {
      StreamWriter _file_writer;

      _file_writer = null;

      try
      {
        _file_writer = new StreamWriter(Filename);
        _file_writer.BaseStream.Seek(0, SeekOrigin.Begin);
        _file_writer.Write(TextToWrite);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("WriteStringToFile. Error writing file: " + Filename + ".");
        Log.Exception(_ex);

        return false;
      }
      finally
      {
        if (_file_writer != null)
        {
          try
          {
            _file_writer.Close();
            _file_writer.Dispose();
          }
          catch { }
        }
      }
    } // GetFileName

    public static class ObjectManager
    {
      private static Hashtable m_ht = new Hashtable();

      public static void Register(String Name, DataSet DataSet)
      {
        lock (m_ht)
        {
          if (m_ht.Contains(Name))
          {
            m_ht.Remove(Name);
          }
          m_ht.Add(Name, DataSet);
        }
      } // Register

      public static void Unregister(String Name)
      {
        lock (m_ht)
        {
          if (m_ht.Contains(Name))
          {
            m_ht.Remove(Name);
          }
        }
      } // Unregister

      public static DataSet GetObject(String Name, int TableIndex, int RowIndex, int NumRows)
      {
        lock (m_ht)
        {
          if (m_ht.Contains(Name))
          {
            DataSet _ds;
            DataTable _dt;
            int _idx0;
            int _idx1;
            _ds = (DataSet)m_ht[Name];

            _dt = _ds.Tables[TableIndex];

            _dt.AcceptChanges();
            _idx0 = Math.Min(RowIndex, _dt.Rows.Count);
            _idx1 = Math.Min(_idx0 + NumRows, _dt.Rows.Count);
            for (int _idx = _idx0; _idx < _idx1; _idx++)
            {
              _dt.Rows[_idx].SetModified();
            }
            _dt.AcceptChanges();
            _dt = _dt.GetChanges(DataRowState.Modified);
            _ds = new DataSet();
            _ds.Tables.Add(_dt);

            return _ds;
          }
        }

        return null;
      } // GetObject
    } // ObjectManager

    //------------------------------------------------------------------------------
    // PURPOSE : Calculate the age from birth date
    //
    //  PARAMS :
    //      - INPUT :
    //          - DateTime Birthdate
    //          - String TextToWrite
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //
    public static Int32 CalculateAge(DateTime Birthdate)
    {
      Int32 _age;
      DateTime _now;

      _now = WASDB.Now;

      // Get the difference in years.
      _age = _now.Year - Birthdate.Year;
      // Subtract another year if we're before the birth day in the current year.
      if (Birthdate > _now.AddYears(-_age))
      {
        _age--;
      }

      return _age;
    } // CalculateAge

    //------------------------------------------------------------------------------
    // PURPOSE : Check if is today is the Client birth day
    //
    //  PARAMS :
    //      - INPUT :
    //          - DateTime Birthdate
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean: True. Today is birth day
    //                 False. Today isn't birth day
    public static Boolean IsClientBirthday(DateTime Today, DateTime Birthday)
    {
      // If your birthday is 29/02 and the current year is leap, 
      //          we must consider birth as if it was the 28/02
      if (Birthday.Day == 29 && Birthday.Month == 2 && !DateTime.IsLeapYear(Today.Year))
      {
        Birthday = new DateTime(Birthday.Year, Birthday.Month, 28);
      }

      return (Today.Month == Birthday.Month && Today.Day == Birthday.Day);

    } // IsClientBirthday

    //------------------------------------------------------------------------------
    // PURPOSE : Return the SQL Filter of witholders the level account mask promotion
    //                    
    //  PARAMS :
    //      - INPUT :
    //          - ColumnName: Name of column is filtered, usually [AC_HOLDER_LEVEL]
    //
    // RETURNS :
    //      - String:  
    //
    public static String LevelFilter(Int32 LevelMask, String ColumnName)
    {
      String _filter;

      _filter = "";

      // Any level allowed 
      if (LevelMask == 0)
      {
        //LevelMask = 31; //Binary: 11111 Decimal = 31
        return " " + ColumnName + " = 0 " + " OR " + ColumnName + " = 1 " + " OR " + ColumnName + " = 2 " + " OR " + ColumnName + " = 3 " + " OR " + ColumnName + " = 4 ";
      }

      for (int AcctLevel = 0; AcctLevel < 5; AcctLevel++)
      {
        if ((LevelMask & (1 << AcctLevel)) != 0)
        {
          if (_filter.Length != 0)
          {
            _filter += " OR";
          }
          _filter += " " + ColumnName + " = " + AcctLevel;
        }
      }

      return _filter;
    } // LevelFilter

    //------------------------------------------------------------------------------
    // PURPOSE : Return SAT string formatted
    //                    
    //  PARAMS :
    //      - INPUT :
    //          - Text
    //
    // RETURNS :
    //      - String:  
    //
    public static String SATStringNotEmpty(String Text)
    {
      try
      {
        if (Text == null)
        {
          return " "; // Blank space
        }

        if (String.IsNullOrEmpty(Text))
        {
          return " "; // Blank space
        }
      }
      catch { };

      return Text;

    } // SATStringNotEmpty

    //------------------------------------------------------------------------------
    // PURPOSE : Return SAT string formatted
    //                    
    //  PARAMS :
    //      - INPUT :
    //          - Text
    //
    // RETURNS :
    //      - String:  
    //
    public static String SATStringNotNullOrEmpty(SqlDataReader Reader, Int32 IdxColumn)
    {
      String _text;

      _text = " "; // Blank space

      try
      {
        if (Reader.IsDBNull(IdxColumn))
        {
          return " "; // Blank space
        }

        _text = Reader.GetString(IdxColumn);

        // Avoid the '\n' that has been saved into the database (we presume is due to an empty element reported as "<Element />"
        if (_text.Length > 0 && char.IsControl(_text[0]))
        {
          _text = _text.Substring(1).Trim();
        }

        if (String.IsNullOrEmpty(_text))
        {
          return " "; // Blank space
        }

      }
      catch { };

      return _text;

    } // SATStringNotNullOrEmpty

    //------------------------------------------------------------------------------
    // PURPOSE : Return a datetime formatted with the current system culture info
    //                    
    //  PARAMS :
    //      - INPUT :
    //          - DateTime
    //
    // RETURNS :
    //      - String:  
    //
    public static String GetDateTimeCurrentCulture(DateTime DateTime)
    {
      CultureInfo _culture_info;
      String _date_format;

      _culture_info = new CultureInfo(CultureInfo.CurrentCulture.Name);
      _date_format = _culture_info.DateTimeFormat.ShortDatePattern;

      return DateTime.ToString(_date_format + " HH:mm:ss");
    }


  } // Misc

  public static class Development
  {
    private static Boolean m_init = false;

    public static Boolean Enabled
    {
      get
      {
        Boolean _development;

        _development = (Environment.GetEnvironmentVariable("LKS_VC_DEV") != null);

        if (!m_init)
        {
          if (_development)
          {
            Log.Message("Development variable defined!");
          }
          m_init = true;
        }

        return _development;
      }
    } // Enabled

    public static void DumpVariables()
    {
      IDictionary _dictionary;
      StringBuilder _sb;
      String _value;
      String[] _keys;

      _sb = new StringBuilder();

      _dictionary = Environment.GetEnvironmentVariables();
      _keys = new String[_dictionary.Keys.Count];
      _dictionary.Keys.CopyTo(_keys, 0);
      Array.Sort(_keys);

      _sb.AppendLine();
      _sb.AppendLine("<ENVIRONMENT>");

      String _tab;
      _tab = "    ";
      foreach (String _key in _keys)
      {
        _value = (String)_dictionary[_key];
        _sb.AppendLine(_tab + _key + "=" + _value);
      }
      _sb.AppendLine("</ENVIRONMENT>");

      Log.Message(_sb.ToString());
    } // DumpVariables

  } // Development

} // WSI.Common
