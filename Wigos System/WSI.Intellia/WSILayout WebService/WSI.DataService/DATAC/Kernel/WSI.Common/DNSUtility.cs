//------------------------------------------------------------------------------
// Copyright © 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: DNSUtility.cs
// 
//   DESCRIPTION: DNS Utilities class
// 
//        AUTHOR: Ronald Rodríguez
// 
// CREATION DATE: 27-ABR-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 25-ABR-2007 RRT    First release.
//------------------------------------------------------------------------------
using System;
using System.Net;
using System.Net.Sockets; 
using System.IO;

//using WSI.Common;

namespace WSI.Common
{
  /// <summary>
  /// Provide with utilities for network related actions.
  /// </summary>
  public class DNSUtility
  {
    #region Class Atributes

    #endregion

    #region Private Methods

    #endregion

    #region Public Methods

    /// <summary>
    /// Returns the first IP (v4) address found.
    /// </summary>
    /// <returns></returns>
    public static IPAddress GetFirstIP()
    {
      String host_name;
      IPAddress[] ip_addresses;

      host_name = System.Net.Dns.GetHostName();
      ip_addresses = System.Net.Dns.GetHostAddresses(host_name);

      foreach (IPAddress ip in ip_addresses )
      {
        if ( ip.AddressFamily != AddressFamily.InterNetwork )
        {
          continue;
        }
        if (IPAddress.IsLoopback(ip))
        {
          continue;
        }
        return ip;
      }

      return IPAddress.None;

    } // GetAnyIPAddress

    /// <summary>
    /// Returns a private IP address (If exist)
    /// </summary>
    /// <returns></returns>
    public static string GetPrivateIPAddress()
    {
      String host_name;
      IPAddress [] ip_addresses;
      String ip_address;
      String [] ip_octes;

      host_name = System.Net.Dns.GetHostName ();
      ip_addresses = System.Net.Dns.GetHostAddresses(host_name);
      
      // Return private IP address:
      // Private IP Address range: (RFC 1918)
      // 10.0.0.0    -  10.255.255.255  (10/8 prefix)
      // 172.16.0.0  -  172.31.255.255  (172.16/12 prefix)
      // 192.168.0.0 -  192.168.255.255 (192.168/16 prefix)

      // Search from the last to the first: New addresses are included on top of the list
      for (int i = ip_addresses.Length - 1; i >= 0; i--)
      {
        ip_address = ip_addresses[i].ToString();
        ip_octes = ip_address.Split('.');
 
        if (    ip_octes[0] == "10"
            || (ip_octes[0] == "172" && ip_octes[1] == "16")
            || (ip_octes[0] == "192" && ip_octes[1] == "168"))
        {
          return ip_address;
        }
      }

      return "";
    } // GetPrivateIPAddress

    /// <summary>
    /// Returns a Public IP address (If exist)
    /// </summary>
    /// <returns></returns>
    public static string GetPublicIPAddress()
    {
      String host_name;
      IPAddress[] ip_addresses;
      String ip_address;
      String[] ip_octes;

      host_name = System.Net.Dns.GetHostName();
      ip_addresses = System.Net.Dns.GetHostAddresses(host_name);

      // Private IP Address range: (RFC 1918)
      // 10.0.0.0    -  10.255.255.255  (10/8 prefix)
      // 172.16.0.0  -  172.31.255.255  (172.16/12 prefix)
      // 192.168.0.0 -  192.168.255.255 (192.168/16 prefix)

      for (int i = 0; i < ip_addresses.Length; i++)
      {
        ip_address = ip_addresses[i].ToString();
        ip_octes = ip_address.Split('.');

        if (    ip_octes[0] != "10"
            && (ip_octes[0] != "172" || ip_octes[1] != "16")
            && (ip_octes[0] != "192" || ip_octes[1] != "168"))
        {
          return ip_address;
        }
      }

      return "";

    } // GetPublicIPAddress

    #endregion


  } // DNSUtility

}
