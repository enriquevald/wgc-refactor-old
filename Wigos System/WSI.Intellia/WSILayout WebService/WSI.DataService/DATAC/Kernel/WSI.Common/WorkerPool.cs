//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WorkerPool.cs
// 
//   DESCRIPTION: The Worker Pool
// 
//        AUTHOR: Andreu Juli�
// 
// CREATION DATE: 12-MAR-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 12-MAR-2007 AJQ    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Threading;
using WSI.Common;
using System.Data.SqlClient;

namespace WSI.Common
{
  /// <summary>
  /// Waitable Queue
  /// </summary>
  public class WaitableQueue
  {
    Queue m_queue;
    AutoResetEvent m_data_available;

    /// <summary>
    /// Creates the queue
    /// </summary>
    public WaitableQueue ()
    {
      m_queue = new Queue();
      m_data_available = new AutoResetEvent(false);
    }

    /// <summary>
    /// Dequeues an item. Blocks till an item is available
    /// </summary>
    /// <returns>An item</returns>
    public object Dequeue ()
    {
      object  _item;
      
      while (!Dequeue (out _item, System.Threading.Timeout.Infinite))
      {
        System.Threading.Thread.Sleep(10);
      }
      
      return _item;
    }

    /// <summary>
    /// Dequeues an item. Blocks till an item is available
    /// </summary>
    /// <returns>An item</returns>
    public Boolean Dequeue(out Object Item, int Timeout)
    {
      int _tick0;
      int _timeout;
      
      Item = null;
      _tick0 = Misc.GetTickCount();

      while (true)
      {
        if ( Timeout == System.Threading.Timeout.Infinite )
        {
          _timeout = System.Threading.Timeout.Infinite;
        }
        else
        {
          _timeout = Timeout - (int) Misc.GetElapsedTicks(_tick0);
          _timeout = Math.Max(0, _timeout);
        }

        if (!m_data_available.WaitOne(_timeout))
        {
          return false;
        } // if ( signaled )

        try
        {
          Monitor.Enter(m_queue);

          if (m_queue.Count == 0)
          {
            // No items queued
            continue;
          }

          // Dequeue one item
          Item = m_queue.Dequeue();

          return true;
        }
        finally
        {
          if (m_queue.Count == 0)
          {
            // Notify more items available
            m_data_available.Reset();
          }
          else 
          {
            m_data_available.Set();
          }
          
          Monitor.Exit(m_queue);
        }
      } // while ( true )
    }

    /// <summary>
    /// Dequeues an item. Blocks till an item is available
    /// </summary>
    /// <returns>An item</returns>
    /// 
    public object[] DequeueAll(WorkerPool Pool)
    {
      object[] _items;
      int _num_available_workers;
      int _max_items;


      while (true)
      {
        if (!m_data_available.WaitOne())
        {
          System.Threading.Thread.Sleep(100);

          continue;
        } // if ( signaled )

        try
        {
          Monitor.Enter(m_queue);

          if (m_queue.Count == 0)
          {
            // No items queued
            continue;
          }

          _num_available_workers = Pool.AvailableWorkers;
          if ( _num_available_workers <= 0 )
          {
            _num_available_workers = 1;
            Log.Warning ("No workers available");
          }
          _max_items = (m_queue.Count + _num_available_workers - 1) / _num_available_workers;
          _max_items = Math.Max(Math.Min(10, m_queue.Count), _max_items);

          // Dequeue 'N'-items
          if (m_queue.Count > _max_items)
          {
            _items = new object[_max_items];
            for (int _idx = 0; _idx < _max_items; _idx++)
            {
              _items[_idx] = m_queue.Dequeue();
            }
          }
          else
          {
            _items = m_queue.ToArray();
            m_queue.Clear();
          }

          return _items;
        }
        finally
        {
          if (m_queue.Count == 0)
          {
            // Notify more items available
            m_data_available.Reset();
          }
          else
          {
            m_data_available.Set();
          }
          
          Monitor.Exit(m_queue);
        }
      } // while ( true )
    }
    

    public int Count
    { 
      get
      {
        lock ( m_queue )
        {
          return m_queue.Count;
        }
      }
    }

    /// <summary>
    /// Enqueues an item
    /// </summary>
    /// <param name="Item"></param>
    public void Enqueue (Object Item)
    {
      try 
      {
        Monitor.Enter(m_queue);
        m_queue.Enqueue(Item);
        m_data_available.Set();
      }
      finally
      {
        Monitor.Exit(m_queue);
      }
    }
  }

  public interface ITask
  {
    void Execute ();
  }

  public class Task
  {
    protected Worker m_worker;

    internal void SetWorker (Worker Worker)
    {
      m_worker = Worker;
    }

    public virtual void Execute () { }
    public virtual void Execute (Task [] Tasks) { }
  }

  public delegate void WorkerTaskExecutedEventHandler (Task Task);


  public class Worker
  {
    private WaitableQueue m_work;
    private Boolean m_batch_update;
    private Thread m_thread;
    internal WorkerPool m_owner;

    private void Initialize (String Name, WaitableQueue PendingWorkQueue, Boolean BatchUpdate)
    {
      m_work = PendingWorkQueue;
      m_batch_update = BatchUpdate;
      m_thread = new Thread(new ThreadStart(this.Main));
      m_thread.Name = Name;
      m_thread.Start();
    }

    public Worker (String Name, WaitableQueue PendingWorkQueue, Boolean BatchUpdate)
    {
      Initialize(Name, PendingWorkQueue, BatchUpdate);
    }

    public Worker(String Name, WaitableQueue PendingWorkQueue)
    {
      Initialize(Name, PendingWorkQueue, false);
    }
    
    public Worker(WaitableQueue PendingWorkQueue)
    {
      Initialize("Worker", PendingWorkQueue, false);
    }
    
    public void Start ()
    {
      // TODO: Remove all references
    }

   
    protected void Main ()
    {
      Task _task;
      Task[] _tasks;
      TimeSpan _ts0;
      int _tick = Misc.GetTickCount ();
      
      System.Threading.Thread.Sleep(2000);
      
      try
      {
        try { Thread.BeginThreadAffinity(); }
        catch { ; }

        if (m_batch_update)
        {
          while (true)
          {
            object[] _otasks;

            _otasks = m_work.DequeueAll(m_owner);
            _ts0 = new TimeSpan(0);
            if (m_owner != null)
            {
              System.Threading.Interlocked.Increment(ref m_owner.m_num_working);
              System.Threading.Interlocked.Increment(ref m_owner.m_num_jobs);
              System.Threading.Interlocked.Add(ref m_owner.m_sum_tasks, _otasks.Length);
            }

            if (_otasks != null)
            {
              if (_otasks.Length > 0)
              {
                int _i;
                _i = 0;
                _tasks = new Task[_otasks.Length];
                foreach (Object _obj in _otasks)
                {
                  Task _t;
                  _t = (Task)_obj;
                  _tasks[_i] = _t;
                  _i++;
                }

                _task = (Task)_tasks[0];

                _ts0 = Performance.GetTickCount();
                _task.Execute(_tasks);
                _task = null;
                _ts0 = Performance.GetElapsedTicks(_ts0);
              }
            }

            if (m_owner != null)
            {
              System.Threading.Interlocked.Decrement(ref m_owner.m_num_working);
              m_owner.SetTaskStats (_otasks.Length, _ts0);
            }
          }
        }
        else
        {
          while (true)
          {

            _task = (Task)m_work.Dequeue();
            if (m_owner != null)
            {
              System.Threading.Interlocked.Increment(ref m_owner.m_num_working);
            }
            _task.Execute();
            if (m_owner != null)
            {
              System.Threading.Interlocked.Decrement(ref m_owner.m_num_working);
            }

            _task = null;
          }
        }
      }
      finally 
      {
        try { Thread.EndThreadAffinity(); }
        catch { ; }
      }
      
    }
  }


  public class WorkerPool
  {
    static private int    m_num_pools   = 0;
    static private List<WorkerPool> m_pools = new List<WorkerPool>();
    static private bool   m_stop = false;
    
    
    internal Int64        m_num_working = 0;
    internal Int64 m_num_jobs  = 0;
    internal Int64 m_sum_tasks = 0;
    
    private String        m_name;
    private int           m_num_workers;
    private Worker[]      m_workers;
    private Boolean       m_batch_update;
    private WaitableQueue m_tasks;
    
    

    // Only for test
    ////private Int64 m_stat_n;
    ////private Int64 m_stat_sum_working;
    ////private Int64 m_stat_sum_queued;


    public static void RequestStop()
    {
      m_stop = true;
    }
    
    public static bool CanShutdown (out int PendingTasks)
    {
      PendingTasks = 0;
      
      lock (m_pools)
      {
        foreach (WorkerPool _pool in m_pools)
        {
          PendingTasks += _pool.m_tasks.Count;  // Queued tasks ...
          PendingTasks += (int) _pool.m_num_working;  // Running tasks ...
        }
      }
      
      return ((PendingTasks == 0) && m_stop);
    }
    
    
    
    
    private TimeSpan [] _tt  = new TimeSpan[1000+1];
    private int      [] _tn  = new int [1000+1];
    ////private int _max = 0;
    private int _tick = Misc.GetTickCount ();
    ////private int m_best = 100;
    
    public int AvailableWorkers
    {
      get 
      {
        return (int) (m_num_workers - m_num_working);
      }
    }
    
    public void SetTaskStats (int NumTasks, TimeSpan Elapsed)
    {
      // Only for test

      ////int _n;
      
      ////lock (this)
      ////{
      ////  _n = Math.Min(NumTasks, 1000);
      ////  _max = Math.Max(_n, _max);
      ////  _tt[_n] += Elapsed;
      ////  _tn[_n] += NumTasks;
      ////  if ( Misc.GetElapsedTicks (_tick) >= 60000 )
      ////  {
      ////    TimeSpan _aux;
      ////    TimeSpan _min_ts;
      ////    int      _min_i;
      ////    int      _i;
          
      ////    _min_ts  = TimeSpan.MaxValue;
      ////    _min_i = 0;
      ////    for (_i = 0; _i <= _max; _i++)
      ////    {
      ////      if (_tn[_i] > 0)
      ////      {
      ////        _aux = new TimeSpan(_tt[_i].Ticks/_tn[_i]);
      ////        if ( _aux < _min_ts )
      ////        {
      ////          _min_ts  = _aux;
      ////          _min_i = _i;
      ////        }
      ////      }
      ////    }
      ////    _i     = _min_i;
      ////    _aux   = _min_ts;
      ////    if ( m_best == 0 )
      ////    {
      ////      m_best = _min_i;
      ////    }
      ////    else 
      ////    {
      ////      m_best = 0;
      ////    }

      ////    Log.Message("Pool " + m_name + " #Tasks:" + _i.ToString("000") + " N:" + _tn[_i].ToString("000000") + " AVG: " + _aux.TotalMilliseconds.ToString("00.000"));
      ////    _tick = Misc.GetTickCount(); 
                   
        
      ////  }
      ////}
    }

    public int NumWorkers
    {
      get { return m_num_workers; }
    }

    public WorkerPool (String Name, int NumWorkers)
    {
      WorkerPoolCreate (Name, NumWorkers, false );
    }
    
    public WorkerPool (String Name, int NumWorkers, Boolean BatchUpdate)
    {
      WorkerPoolCreate(Name, NumWorkers, BatchUpdate);
    }
    
    private void WorkerPoolCreate (String Name, int NumWorkers, Boolean BatchUpdate)
    {
      int _idx_worker;

      m_num_pools++;
      m_name         = Name;
      m_num_workers  = NumWorkers;
      m_workers      = new Worker[NumWorkers];
      m_batch_update = BatchUpdate;
      m_tasks        = new WaitableQueue();

      for ( _idx_worker = 0; _idx_worker < m_num_workers; _idx_worker++ )
      {
        m_workers[_idx_worker] = new Worker(Name + "-" + _idx_worker.ToString("000"), m_tasks, BatchUpdate);
        m_workers[_idx_worker].m_owner = this;        
      }

      lock (m_pools)
      {
        m_pools.Add(this);
      }
      Log.Message(Name + " - Workers: " + NumWorkers.ToString() + " - BatchUpdate: " + BatchUpdate.ToString());
    }
    

    public void EnqueueTask(Task Task)
    {
      ////Int64 _working;
      ////Int64 _n;
      ////Int64 _s;
      ////Int64 _q;
      ////Int64 _j;
      ////Int64 _t;
      
      if ( m_stop )
      {
        return;
      }
      
      lock (m_tasks)
      {
        // Only for test
        ////_working = System.Threading.Interlocked.Read(ref m_num_working);
        ////_n = System.Threading.Interlocked.Increment(ref m_stat_n);
        ////_s = System.Threading.Interlocked.Add(ref m_stat_sum_working, _working);
        ////_q = System.Threading.Interlocked.Add(ref m_stat_sum_queued,  m_tasks.Count);
        ////_j = System.Threading.Interlocked.Read(ref m_num_jobs);
        ////_t = System.Threading.Interlocked.Read(ref m_sum_tasks);
        
        m_tasks.Enqueue(Task);
      }

      // Only for test
      ////if ( _n >= 10000 )
      ////{
      ////  Decimal _avg_w;
      ////  Decimal _avg_q;
      ////  Decimal _avg_t;
        

      ////  _avg_w = (Decimal)_s / (Decimal)_n;
      ////  _avg_q = (Decimal)_q / (Decimal)_n;
      ////  if (_j > 0)
      ////  {
      ////    _avg_t = (Decimal)_t / (Decimal)_j;
      ////  }
      ////  else
      ////  {
      ////    _avg_t = 0;
      ////  }
        
      ////  System.Threading.Interlocked.Add(ref m_stat_n,   -_n);
      ////  System.Threading.Interlocked.Add(ref m_stat_sum_working, -_s);
      ////  System.Threading.Interlocked.Add(ref m_stat_sum_queued, -_q);
      ////  System.Threading.Interlocked.Add(ref m_num_jobs, -_j);
      ////  System.Threading.Interlocked.Add(ref m_sum_tasks, -_t);
      ////  Log.Message("POOL " + m_name + " Samples: " + _n + " Working: " + _avg_w.ToString("0.0") + " Queued: " + _avg_q.ToString("0.0") + " Jobs: " + _j.ToString("0.0") + " Tasks: " + _avg_t.ToString("0.0"));
      ////}
    }

  }
  

}
