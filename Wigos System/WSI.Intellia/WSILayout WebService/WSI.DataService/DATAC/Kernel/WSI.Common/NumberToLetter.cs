using System;
using System.Collections.Generic;
using System.Text;

namespace WSI.Common
{
  public static class NumberToLetter
  {
    public static String CurrencyString(Decimal Amount)
    {
      String _minus;
      String _cents;
      String _currency;
      String _currency_cents;
      Int64 _int_part;
      Int64 _dec_part;
      String _text;

      _text = "";

      try
      {
        _minus = String.Empty;

        Amount = Math.Round(Amount, 2, MidpointRounding.AwayFromZero);

        if (Amount < 0)
        {
          _minus = "menos ";

          if (Amount == Decimal.MinValue)
          {
            // Small error ...
            Amount = Decimal.MaxValue;
          }
          else
          {
            Amount = Decimal.Negate(Amount);
          }
        }

        _int_part = Decimal.ToInt64((Amount * 100));
        _dec_part = _int_part % 100;
        _int_part -= _dec_part;
        _int_part /= 100;

        _currency = " pesos";
        if (_int_part == 1) _currency = " peso";
        if (_int_part >= 1000000)
        {
          if (_int_part % 1000000 == 0)
          {
            _currency = " de pesos";
          }
        }


        _currency_cents = " centavos";
        if (_dec_part == 1) _currency_cents = " centavo";

        _cents = "";
        if (_dec_part > 0)
        {
          _cents = " con " + MillionToString(_dec_part) + _currency_cents + "";
        }

        _text = _minus + MillionToString(_int_part) + _currency + _cents;
        if (_text.Length > 1)
        {
          _text = _text.Substring(0, 1).ToUpper() + _text.Substring(1, _text.Length - 1);
        }
      }
      catch 
      {
      }

      return _text;
    }



    /// <summary>
    /// Convert units (Int64 format) to String format.
    /// </summary>
    /// <param name="Value">Value to convert</param>
    /// <returns>Returns a text which represents the specified Value</returns>
    private static String UnitToString(Int64 Value)
    {
      String _result;

      _result = String.Empty;

      switch (Value)
      {
        case 0: _result = "cero"; break;
        case 1: _result = "un"; break;
        case 2: _result = "dos"; break;
        case 3: _result = "tres"; break;
        case 4: _result = "cuatro"; break;
        case 5: _result = "cinco"; break;
        case 6: _result = "seis"; break;
        case 7: _result = "siete"; break;
        case 8: _result = "ocho"; break;
        case 9: _result = "nueve"; break;
        default: _result = TenToString(Value); break;
      }

      return _result;
    }

    /// <summary>
    /// Convert tens (Int64 format) to String format.
    /// </summary>
    /// <param name="Value">Value to convert</param>
    /// <returns>Returns a text which represents the specified Value</returns>
    private static String TenToString(Int64 Value)
    {
      if (Value < 10) return UnitToString(Value);
      if (Value > 99) return HundredToString(Value);

      String _result = String.Empty;

      switch (Value)
      {
        case 10: _result = "diez"; break;
        case 11: _result = "once"; break;
        case 12: _result = "doce"; break;
        case 13: _result = "trece"; break;
        case 14: _result = "catorce"; break;
        case 15: _result = "quince"; break;
        case 16: _result = "diecis�is"; break;
        case 17: _result = "diecisiete"; break;
        case 18: _result = "dieciocho"; break;
        case 19: _result = "diecinueve"; break;
        case 20: _result = "veinte"; break;
        case 21: _result = "veinti�n"; break;
        case 22: _result = "veintid�s"; break;
        case 23: _result = "veintitr�s"; break;
        case 24: _result = "veinticuatro"; break;
        case 25: _result = "veinticinco"; break;
        case 26: _result = "veintis�is"; break;
        case 27: _result = "veintisiete"; break;
        case 28: _result = "veintiocho"; break;
        case 29: _result = "veintinueve"; break;

        case 30: _result = "treinta"; break;
        case 40: _result = "cuarenta"; break;
        case 50: _result = "cincuenta"; break;
        case 60: _result = "sesenta"; break;
        case 70: _result = "setenta"; break;
        case 80: _result = "ochenta"; break;
        case 90: _result = "noventa"; break;
        default:
          {
            Int64 _ten;
            Int64 _unit;

            _unit = Value % 10;
            _ten = Value - _unit;

            _result = TenToString(_ten) + " y " + UnitToString(_unit);

            break;
          }
      }

      return _result;
    }

    private static String HundredToString(Int64 Value)
    {
      Int64 _hundred;
      Int64 _remainder;

      if (Value < 100) return TenToString(Value);
      if (Value > 999) return ThousandToString(Value);

      _remainder = 0;
      _hundred = Math.DivRem(Value, 100, out _remainder);

      String _result = String.Empty;

      switch (_hundred)
      {
        case 1:
          {
            _result = "ciento";
            if (_remainder == 0)
            {
              _result = "cien";
            }
            break;
          }
        case 2: _result = "doscientos"; break;
        case 3: _result = "trescientos"; break;
        case 4: _result = "cuatrocientos"; break;
        case 5: _result = "quinientos"; break;
        case 6: _result = "seiscientos"; break;
        case 7: _result = "setecientos"; break;
        case 8: _result = "ochocientos"; break;
        case 9: _result = "novecientos"; break;
      }

      if (_remainder == 0) return _result;

      return _result + " " + TenToString(_remainder);
    }

    /// <summary>
    /// Convert thousands (Int64 format) to String format.
    /// </summary>
    /// <param name="Value">Value to convert</param>
    /// <returns>Returns a text which represents the specified Value</returns>
    private static String ThousandToString(Int64 Value)
    {
      if (Value < 1000) return HundredToString(Value);
      if (Value > 999999) return MillionToString(Value);

      Int64 _remainder = 0;
      Int64 _thousands = System.Math.DivRem(Value, 1000, out _remainder);
      String _str_mil = "mil";

      if (_thousands > 1) _str_mil = HundredToString(_thousands) + " mil";

      if (_remainder == 0) return _str_mil;

      return _str_mil + " " + HundredToString(_remainder);
    }

    /// <summary>
    /// Convert millions (Int64 format) to String format.
    /// </summary>
    /// <param name="Value">Value to convert</param>
    /// <returns>Returns a text which represents the specified Value</returns>
    private static String MillionToString(Int64 Value)
    {
      if (Value < 1000000) return HundredToString(Value);

      Int64 _remainder = 0;
      Int64 _millions = System.Math.DivRem(Value, 1000000, out _remainder);

      String _str_millions = "un mill�n";

      if (_millions > 1)
        _str_millions = ThousandToString(_millions) + " millones";

      if (_remainder > 0)
        _str_millions = _str_millions + " " + ThousandToString(_remainder);

      return _str_millions;
    }

  }

}

