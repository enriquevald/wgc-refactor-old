﻿////------------------------------------------------------------------------------
//// Copyright © 2012 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: TypeValidator.cs
//// 
////      DESCRIPTION: Type Validator Class 
//// 
////           AUTHOR: Humberto Braojos
//// 
////    CREATION DATE: 15-OCT-2012
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 15-OCT-2012 HBB, JCM    First release.
//// 25-OCT-2012 HBB, JVV    Add culture to Double.TryParse
//// 02-NOV-2012 HBB, JCA    Change type Exception returns to  FormatException
//// 15-JAN-2013 XIT         Added Field Param to the calls. Used to know the Field that throws an Exception
//// 21-JAN-2013 JCA         Added method to validate registers TAGS, the correct order, if not exists, etc.
////------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Text.RegularExpressions;

namespace WSI.Common
{
  public static class TypeValidator
  {
    #region fields
    private const Decimal MAX_CURRENCY_VALUE_DECIMAL = 999999999999.99m;
    private const Decimal MIN_CURRENCY_VALUE_DECIMAL = -999999999999.99m;
    private const Int32 MIN_LENGHT_RFC = 10;// Francesc Aguilar: Són 10 según lo que pone en el XSD; // 9;
    private const Int32 MAX_LENGHT_RFC = 13;
    private const Int32 LENGHT_CURP = 18;
    private static Regex REGEX_CURRENCY = new Regex("^-?[0-9]{1,12}([.][0-9]{1,2})?$");
    private static Regex REGEX_NUMERICKEY = new Regex("^[0-9]{1,3}$");
    private static Regex REGEX_RFC = new Regex("[A-Z,Ñ,&]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,0-9]?[A-Z,0-9]?[0-9,A-Z]?");
    public const String RFC_GENERIC_01 = "XAXX010101000"; // Mexican
    public const String RFC_GENERIC_02 = "XEXX010101000"; // Foreign
    
                                                
    private static Regex REGEX_CURP = new Regex("[A-Z][A,E,I,O,U,X][A-Z]{2}[0-9]{2}[0-1][0-9][0-3][0-9][M,H][A-Z]{2}[B,C,D,F,G,H,J,K,L,M,N,Ñ,P,Q,R,S,T,V,W,X,Y,Z]{3}[0-9,A-Z][0-9]");
    private static Regex REGEX_NUMTRANSACTION = new Regex("^[0-9]{1,30}$");
    private static Regex REGEX_ZIPCODE = new Regex("^[0-9]{5}$");
    private static Regex REGEX_ONLYNUMBERS = new Regex(@"^\d+$");
    private static String STR_REGEX_TAG = (@"^*<(?<Tag>[a-zA-Z]*)>|<(?<Tag>.*)xsi.*/>");//new Regex(@"^*(?<Tag><[a-zA-Z]*>)");
    public const String DAY_DATE_FORMAT = "yyyy-MM-dd";
    public const String HOUR_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss"; 

    #endregion

    //------------------------------------------------------------------------------
    // PURPOSE: Validate Currency;
    //
    //  PARAMS:
    //      - INPUT:
    //          - String Currency
    //          - String Field
    //
    //      - OUTPUT:
    //
    // RETURNS:  Decimal Currency. If not correct, returns with throw new FormatException
    public static Decimal ValidateCurrency(String Currency, String Field)
    {
      Decimal _currency_result;
      NumberStyles style;
      CultureInfo culture;

      culture = CultureInfo.CreateSpecificCulture("en-US");
      style = NumberStyles.Currency;

      if (!Decimal.TryParse(Currency, style, culture, out _currency_result) || !REGEX_CURRENCY.IsMatch(Currency) || _currency_result > MAX_CURRENCY_VALUE_DECIMAL || _currency_result < MIN_CURRENCY_VALUE_DECIMAL || Currency[0] == '.')
      {
        throw new FormatException(WSI.Common.Resource.String("STR_WS_DATA_ERROR_FORMAT_DECIMAL", Currency, Field)); //Campo: @Field. Formato incorrecto valor Decimal: @Currency
      }
      return _currency_result;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Validate Lenght of String (255 max) and not null or Empty;
    //
    //  PARAMS:
    //      - INPUT:
    //          - String Value
    //          - String Field
    //
    //      - OUTPUT:
    //
    // RETURNS:  String. If not correct, returns with throw new FormatException
    public static String ValidateLengthString(String Value, String Field)
    {
      if (Value.Length > 255)
      {
        throw new FormatException(WSI.Common.Resource.String("STR_WS_DATA_ERROR_INCORRECT_STRING_LENGHT", Field)); //Campo: @Field. Cadena incorrecta.
      }
      else
      {
        return ValidateString(Value, Field);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Validate not null or empty String
    //
    //  PARAMS:
    //      - INPUT:
    //          - String Value
    //          - String Field
    //
    //      - OUTPUT:
    //
    // RETURNS:  String. If not correct, returns with throw new FormatException
    public static String ValidateString(String Value, String Field)
    {
      if (String.IsNullOrEmpty(Value))
      {
        throw new FormatException(WSI.Common.Resource.String("STR_WS_DATA_ERROR_INCORRECT_STRING_NULL", Field));  // Campo: @Field. String is null or empty");
      }
      return Value;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Validates that the input received matches with format "yyyy'-'MM'-'dd'T'HH':'mm':'ss"  and returns as Datetime
    //
    //  PARAMS:
    //      - INPUT: 
    //          - String Value
    //          
    //      - OUTPUT:
    //
    // RETURNS: DateTime
    public static DateTime ValidateDateTime(String Value, String Field)
    {
      DateTime _correctdatetime;
      DateTimeFormatInfo dti = new System.Globalization.DateTimeFormatInfo();
      String pattern = "yyyy'-'MM'-'dd'T'HH':'mm':'ss";


      if (!DateTime.TryParseExact(Value, pattern, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out _correctdatetime))
      {
        // Campo: @Field. Formato de fecha incorrecto, patrón esperado:  " + "'yyyy'-'MM'-'dd'T'HH':'mm':'ss' " + ", " + Value
        if (String.IsNullOrEmpty(Value))
        {
          Value = "";
        }
        throw new FormatException(WSI.Common.Resource.String("STR_WS_DATA_ERROR_INCORRECT_DATA_FORMAT", "'yyyy'-'MM'-'dd'T'HH':'mm':'ss'", Value, Field));
      }
      return _correctdatetime;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Validates that the input received matches with format "yyyy'-'MM'-'dd"  and returns as Datetime
    //
    //  PARAMS:
    //      - INPUT: 
    //          - String Value      
    //
    //      - OUTPUT:
    //
    // RETURNS: DateTime
    public static DateTime ValidateDate(String Value, String Field)
    {
      DateTime _date;
      DateTimeFormatInfo _dt;
      String _pattern;

      _dt = new System.Globalization.DateTimeFormatInfo();
      _pattern = "yyyy'-'MM'-'dd";
      if (!DateTime.TryParseExact(Value, _pattern, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out _date))
      {
        //Campo: @Field. Formato de fecha incorrecto, patrón esperado:" + "'yyyy'-'MM'-'dd' " + ", " + Value
        throw new FormatException(WSI.Common.Resource.String("STR_WS_DATA_ERROR_INCORRECT_DATA_FORMAT", "'yyyy'-'MM'-'dd'", Value, Field));
      }
      return _date;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Validates that the input received matches with a boolean
    //
    //  PARAMS:
    //      - INPUT: 
    //          - String Value
    //          
    //
    //      - OUTPUT:
    //
    // RETURNS: Boolean
    public static Boolean ValidateBoolean(String Value, String Field)
    {
      switch (Value)
      {
        case "1": return true;
        case "0": return false;
        case null:
        case "": throw new FormatException(WSI.Common.Resource.String("STR_WS_DATA_ERROR_INCORRECT_BOOLEAN_FORMAT", "null", Field));
        default: throw new FormatException(WSI.Common.Resource.String("STR_WS_DATA_ERROR_INCORRECT_BOOLEAN_FORMAT", "fuera del rango admitido", Field));
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Validates the period[yyyy-mm] and returns as DateTime
    //
    //  PARAMS:
    //      - INPUT: 
    //          - String Period
    //          
    //
    //      - OUTPUT:
    //
    // RETURNS: DateTime
    public static DateTime ValidatePeriod(String Period, String Field)
    {
      String[] _string_date;
      DateTime _period;
      try
      {
        _string_date = Period.Split('-');
        _period = new DateTime(Int16.Parse(_string_date[0]), Int16.Parse(_string_date[1]), 1);
        return _period;
      }
      catch
      {
        //throw new FormatException("Valor de periodo incorrecto: " + Period);
        throw new FormatException(WSI.Common.Resource.String("STR_WS_DATA_ERROR_INCORRECT_PERIODO", Period, Field));
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Validates the keys, checking if they are composed by numbers
    //
    //  PARAMS:
    //      - INPUT: 
    //          - String Period
    //          
    //
    //      - OUTPUT:
    //
    // RETURNS: if it´s ok returns the value otherwise throws an exception
    public static String ValidateNumericKey(String Value, String Field)
    {
      if (REGEX_NUMERICKEY.IsMatch(Value))
      {
        switch (Value.Length)
        {
          case 1: return "00" + Value;
          case 2: return "0" + Value;
          case 3: return Value;
        }
      }

      //throw new FormatException("Formato de clave incorrecto");
      throw new FormatException(WSI.Common.Resource.String("STR_WS_DATA_ERROR_INCORRECT_KEY", Value, Field));
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Validates the RFC. String between 9 & 13 caracters and SAT validation
    //
    //  PARAMS:
    //      - INPUT: 
    //          - String RFC
    //          
    //
    //      - OUTPUT:
    //
    public static String ValidateRFC(String Value,out Boolean Error)
    {
      String _value;
      Error = true;
      if ((Value.Length <= MAX_LENGHT_RFC && Value.Length >= MIN_LENGHT_RFC) && (REGEX_RFC.IsMatch(Value)))
      {
        _value = REGEX_RFC.Replace(Value, "");
        if (!String.IsNullOrWhiteSpace(_value))
        {
          Error = true;
        }
        else
        {
          Error = false;
        }
      }
      
      return Value;
     //throw new FormatException(WSI.Common.Resource.String("STR_WS_DATA_ERROR_INCORRECT_RFC_CURP", "RFC", Value));
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Validates the CURP. String 18 caracters and SAT validation
    //
    //  PARAMS:
    //      - INPUT: 
    //          - String CURP
    //          
    //
    //      - OUTPUT:
    //
    public static String ValidateCURP(String Value,out Boolean Error)
    {
      Error = false;
      if ((!String.IsNullOrEmpty(Value)) && (Value.Length != LENGHT_CURP || !REGEX_CURP.IsMatch(Value)))
      {
        //throw new FormatException(WSI.Common.Resource.String("STR_WS_DATA_ERROR_INCORRECT_RFC_CURP", "CURP", Value));
        Error = true;
      }
      
      return Value;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Validate Transaction, not null and only numbers
    //
    //  PARAMS:
    //      - INPUT:
    //          - String Value
    //
    //      - OUTPUT:
    //
    // RETURNS:  Value
    public static String ValidateNumTransaction(String Value)
    {
      if (String.IsNullOrEmpty(Value) || !REGEX_NUMTRANSACTION.IsMatch(Value))
      {
        //throw new FormatException("Formato de Número de Transacción incorrecto");
        throw new FormatException(WSI.Common.Resource.String("STR_WS_DATA_ERROR_INCORRECT_TRANSACCTION_FORMAT", Value));
      }

      return Value;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Validate the ZipCode
    //
    //  PARAMS:
    //      - INPUT:
    //          - String Value
    //
    //      - OUTPUT:
    //
    // RETURNS:  Value. If not correct, returns with throw new FormatException
    public static String ValidateZipCode(String Value,out Boolean Error)
    {
      Error = false;
      if (!REGEX_ZIPCODE.IsMatch(Value))
      {
        Error = true;
        //throw new FormatException(WSI.Common.Resource.String("STR_WS_DATA_ERROR_INCORRECT_POSTALCODE_FORMAT", Value));
      }

      return Value;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Validate the TAGS of XML exists and are in correct order;
    //
    //  PARAMS:
    //      - INPUT:
    //          - String Value
    //          - Dictionary<Int16, String> Fields
    //          - Dictionary<Int16, String> OptionalFields
    //
    //      - OUTPUT:
    //
    // RETURNS:  Void. If not correct, returns with throw new FormatException
    public static void ValidateTags(String Value, Dictionary<Int16, String> Fields, Dictionary<String, Boolean> OptionalFields)
    {
      String _tag;
      String _strnode;
      String _strnodeConstancia;
      RegexOptions _myRegexOptions;
      Regex _regex;
      Int16 _position;
      Boolean _correct;


      _tag = String.Empty;
      _strnodeConstancia = String.Empty;
      _position = 0;
      _myRegexOptions = RegexOptions.Multiline;
      _regex = new Regex(STR_REGEX_TAG, _myRegexOptions);
      _correct = false;

      _strnode = Value;
      if (OptionalFields["Constancia"])//if (_strnode.Contains("<Constancia"))
      {
        if (Value.LastIndexOf("<Constancia") < Value.LastIndexOf("<MontoPremioNoReclamado"))
        {
          throw new FormatException(WSI.Common.Resource.String("STR_WS_DATA_ERROR_INCORRECT_TAG", "Constancia"));
        }

        if (OptionalFields["TipoCambio"])
        {
          if (Value.LastIndexOf("<TipoCambio") < Value.LastIndexOf("</Constancia") || Value.LastIndexOf("<TipoCambio") > Value.LastIndexOf("<NumTransaccion"))
          {
            if (!Value.Contains("<MontoPremioNoReclamado"))
            {
              throw new FormatException(WSI.Common.Resource.String("STR_WS_DATA_ERROR_INCORRECT_TAG", "MontoPremioNoReclamado"));
            }
            else if (!Value.Contains("<NumTransaccion"))
            {
              throw new FormatException(WSI.Common.Resource.String("STR_WS_DATA_ERROR_INCORRECT_TAG", "NumTransaccion"));
            }
            else
            {
              throw new FormatException(WSI.Common.Resource.String("STR_WS_DATA_ERROR_INCORRECT_TAG", "TipoCambio"));
            }
          }

        }
        else
        {
          if (!OptionalFields["TipoCambio"] && Value.LastIndexOf("<NumTransaccion") < Value.LastIndexOf("</Constancia"))
          {
            throw new FormatException(WSI.Common.Resource.String("STR_WS_DATA_ERROR_INCORRECT_TAG", "Constancia"));
          }
        }

        _strnode = Value.Substring(0, Value.IndexOf("<Constancia"));
        _strnode += Value.Substring(Value.IndexOf("</Constancia"));


        //Check Requires TAGS for Constancia
        _strnodeConstancia = Value.Substring(Value.IndexOf("<Constancia") + 11, Value.IndexOf("</Constancia") - Value.IndexOf("<Constancia"));
        ValidateTagsConstancia(_strnodeConstancia);
      }
      else
      {
        if (OptionalFields["TipoCambio"])
        {
          if (Value.LastIndexOf("<TipoCambio") < Value.LastIndexOf("<MontoPremioNoReclamado") || Value.LastIndexOf("<TipoCambio") > Value.LastIndexOf("<NumTransaccion"))
          {
            if (!Value.Contains("<MontoPremioNoReclamado"))
            {
              throw new FormatException(WSI.Common.Resource.String("STR_WS_DATA_ERROR_INCORRECT_TAG", "MontoPremioNoReclamado"));
            }
            else if (!Value.Contains("<NumTransaccion"))
            {
              throw new FormatException(WSI.Common.Resource.String("STR_WS_DATA_ERROR_INCORRECT_TAG", "NumTransaccion"));
            }
            else
            {
              throw new FormatException(WSI.Common.Resource.String("STR_WS_DATA_ERROR_INCORRECT_TAG", "TipoCambio"));
            }
          }
        }
      }

      foreach (Match _match in _regex.Matches(_strnode))
      {
        if (_match.Success)
        {
          _tag = _match.Groups[1].Value;
          if (Fields.ContainsValue(_tag))
          {
            if (Fields[_position] == _tag)
            {
              _correct = true;
            }
            else
            {
              _correct = false;
            }
          }
          else
          {
            if (OptionalFields.ContainsKey(_tag))
            {
              if (OptionalFields[_tag])
              {
                _correct = true;
                _position--; //Sino corre 1 la posición y no cuadran
              }
              else
              {
                _correct = false;
              }
            }
            else
            {
              _correct = false;
            }
          }

        }

        if (!_correct)
        {
          _strnode = Fields[_position];
          //throw new FormatException(WSI.Common.Resource.String("STR_WS_DATA_ERROR_INCORRECT_TAG", _tag));
          throw new FormatException(WSI.Common.Resource.String("STR_WS_DATA_ERROR_INCORRECT_TAG_ORDER", _tag, _strnode));
        }
        else
        {
          //Fields[_position] = "OK";
        }
        _position++;
      }

      //_tag = Fields.FirstOrDefault(x => x.Value != "OK").Value;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Validate the TAGS of XML exists and are in correct order in Constancia;
    //
    //  PARAMS:
    //      - INPUT:
    //          - String Value
    //          - Dictionary<Int16, String> OptionalFields
    //
    //      - OUTPUT:
    //
    // RETURNS:  Void. If not correct, returns with throw new FormatException
    private static void ValidateTagsConstancia(String Value)
    {
      String _strnode;
      Dictionary<String, Boolean> _constancia_fields;

      _strnode = Value;


      _strnode = ExtractString(_strnode, "<General", "<Jugador");
      _constancia_fields = new Dictionary<String, Boolean>() { 
                                                                {"General",true},
                                                                {"MontoPremio",true},
                                                                {"ISRretenido",true},
                                                                {"NoMaquina",false }
                                                              };
      ValidateTagsConstanciaByNode(_strnode, _constancia_fields);


      _strnode = ExtractString(Value, "<Jugador", "</Jugador");
      //Si existe continuamos verificando por partes
      _strnode = ExtractString(_strnode, "<General", "</General");
      _constancia_fields = new Dictionary<String, Boolean>() { 
                                                                {"rfc",true},
                                                                {"NombreJugador",true},
                                                                {"CURP",false},
                                                                {"DocumentoID",false},
                                                                {"NoDocumentoID",false}
                                                              };
      ValidateTagsConstanciaByNode(_strnode, _constancia_fields);

      _constancia_fields = new Dictionary<String, Boolean>() { 
                                                                 {"calle",true},
                                                                 {"municipio",true},
                                                                 {"estado",true},
                                                                 {"pais",true},
                                                                 {"codigoPostal",true},
                                                                 {"noExterior",false},
                                                                 {"noInterior",false},
                                                                 {"colonia",false},
                                                                 {"localidad",false},
                                                                 {"referencia",false}
                                                              };

      if (Value.Contains("<Domicilio"))
      {
        _strnode = ExtractString(Value, "<Domicilio", "</Domicilio");

        ValidateTagsConstanciaByNode(_strnode, _constancia_fields);
      }
      else
      {
        foreach (KeyValuePair<String, Boolean> _kvp in _constancia_fields)
        {
          if (Value.Contains(_kvp.Key))
          {
            throw new FormatException(WSI.Common.Resource.String("STR_WS_DATA_ERROR_INCORRECT_TAG", "Domicilio"));
          }
        }
      }
    }


    //------------------------------------------------------------------------------
    // PURPOSE: Validate the TAGS of XML exists and are in correct order in Constancia;
    //
    //  PARAMS:
    //      - INPUT:
    //          - String Value
    //          - Dictionary<String, Boolean> Constancia_Fields
    //
    //      - OUTPUT:
    //
    // RETURNS:  Void. If not correct, returns with throw new FormatException
    private static void ValidateTagsConstanciaByNode(String Value, Dictionary<String, Boolean> ConstanciaFields)
    {
      String _previoustag;
      String _currenttag;
      Int64 _posFirst;
      Int64 _position;

      _previoustag = "";
      _currenttag = "";
      _posFirst = 0;
      _position = 0;

      foreach (KeyValuePair<String, Boolean> _kvp in ConstanciaFields)
      {
        _currenttag = _kvp.Key;
        _position = Value.IndexOf("<" + _currenttag + ">");
        if (_position < _posFirst)
        {
          if (_position == -1 && _kvp.Value)
          {
            if (_currenttag != "rfc")
            {
              throw new FormatException(WSI.Common.Resource.String("STR_WS_DATA_ERROR_INCORRECT_TAG", _currenttag));
            }

            
          }
          else if (_position == -1 && !_kvp.Value)
          {
            _position = _posFirst;
          }
          else
          {
            //throw new FormatException(WSI.Common.Resource.String("STR_WS_DATA_ERROR_INCORRECT_TAG_ORDER", _currenttag, _previoustag));
            throw new FormatException(WSI.Common.Resource.String("STR_WS_DATA_ERROR_INCORRECT_TAG", _currenttag)); 
          }
        }
        _previoustag = _currenttag;
        _posFirst = _position;
      }
    }



    //------------------------------------------------------------------------------
    // PURPOSE: Extract some part of String between two Strings ;
    //
    //  PARAMS:
    //      - INPUT:
    //          - String Value
    //          - String Initial
    //          - String Finally
    //
    //      - OUTPUT:
    //
    // RETURNS:  String. If not correct, returns with throw new FormatException
    private static string ExtractString(String Value, String Initial, String Finally)
    {

      String _strnode;
      int _initialposition;
      int _finalposition;

      _initialposition = Value.IndexOf(Initial);
      _finalposition = Value.IndexOf(Finally);
      if (_initialposition == -1 || _finalposition == -1 || _finalposition < _initialposition)
      {
        throw new FormatException(WSI.Common.Resource.String("STR_WS_DATA_ERROR_INCORRECT_TAG", "General"));
      }
      else
      {
        _strnode = Value.Substring(_initialposition, _finalposition - _initialposition);

      }
      return _strnode;
    }
  }
}
