using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace WSI.Common
{
  public static class Sequences
  {
    public enum SequenceId
    {
      NotSet = 0,
    }

    public static Boolean GetValue(SqlTransaction SqlTrx, SequenceId SequenceId, out Int64 Value)
    {
      StringBuilder _sb;

      Value = 0;

      if (SequenceId == Sequences.SequenceId.NotSet)
      {
        return false;
      }

      _sb = new StringBuilder();
      _sb.AppendLine("IF NOT EXISTS ( SELECT SEQ_NEXT_VALUE FROM SEQUENCES WHERE SEQ_ID = @pSeqId ) ");
      _sb.AppendLine("  INSERT INTO SEQUENCES VALUES (@pSeqId, 1); ");
      _sb.AppendLine(" ");
      _sb.AppendLine("UPDATE   SEQUENCES ");
      _sb.AppendLine("   SET   SEQ_NEXT_VALUE = SEQ_NEXT_VALUE + 1 ");
      _sb.AppendLine("OUTPUT   DELETED.SEQ_NEXT_VALUE ");
      _sb.AppendLine(" WHERE   SEQ_ID    = @pSeqId ");
      
      try 
      {
        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pSeqId", SqlDbType.Int).Value = SequenceId;
          
          Value = (Int64)_sql_cmd.ExecuteScalar();
          
          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      return false;
    } // GetValue
    
  }
}
