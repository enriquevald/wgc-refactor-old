//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: AdobeReader.cs
// 
//   DESCRIPTION: Methods to install adobe acrobat. 
//                  * Arguments for install Adobe Reader 10 (EULA accepted in advance) -->  /msi EULA_ACCEPT=YES /qn
//                    Arguments for install Adobe Reader 9 (EULA accepted in advance) --> 	/sAll /rs /l /msi"/qb-! /norestart ALLUSERS=1 EULA_ACCEPT=YES SUPPRESS_APP_LAUNCH=YES"
//                  * --> USED
//                  Source --> http://www.syamsoftware.com/support/faq/management-utilities-power-auditor-faqs/management-utilities-3rd-party-deployment-parameters
//
//                  Install the version 10, the package --> AdbeRdr1012_es_ES.exe
//                  Source --> ftp://ftp.adobe.com/pub/adobe/reader/win/10.x/10.1.2/es_ES/
//
//                Methods to print pdf files with adobe acrobat.
//
//        AUTHOR: MPO
// 
// CREATION DATE: 13-FEB-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 13-FEB-2012 MPO     First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using Microsoft.Win32;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Drawing.Printing;
using System.IO;

namespace WSI.Common
{
  public class AdobeReader
  {

    #region DllImport

    [DllImport("user32.dll")]
    private static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);
    private const int SW_MINIMIZE = 6;
    private const int SW_MAXIMIZE = 3;
    private const int SW_RESTORE = 9;

    #endregion

    #region Members

    private static Boolean m_installing = false;

    private static Boolean m_init = false;
    private static Boolean m_installed = false;
    private static String m_install_path = "";

    #endregion

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Returns a process depending on the file to be executed and its arguments
    //
    //  PARAMS:
    //      - INPUT: 
    //                FileName: Executable file
    //                Argument: Arguments for executable file
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //          - Boolean: true if check is correct
    // 
    //   NOTES:
    //   
    private static Process PrepareProcess(String FileName, String Argument)
    {
      ProcessStartInfo _start_info;
      Process _process_acrord32;

      _start_info = new System.Diagnostics.ProcessStartInfo();
      _start_info.Arguments = Argument;
      _start_info.FileName = FileName;
      _start_info.CreateNoWindow = true;
      _start_info.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
      _start_info.RedirectStandardOutput = true;
      _start_info.UseShellExecute = false;
      _start_info.RedirectStandardOutput = true;

      _process_acrord32 = new System.Diagnostics.Process();
      _process_acrord32.StartInfo = _start_info;

      return _process_acrord32;

    }

    #endregion

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Logic to determine if the acrobat reader is installed or installing
    //
    //  PARAMS:
    //      - INPUT: 
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //          - Boolean: true if is installed
    // 
    //   NOTES:
    // 
    public static Boolean Installed
    {
      get
      {
        Boolean _installed;
        RegistryKey _reg;

        if (m_init)
        {
          return m_installed;
        }

        m_init = true;

        // Read Registry 
        _reg = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("Software\\Adobe\\Acrobat Reader\\10.0\\InstallPath");
        if (_reg == null)
          _reg = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("Software\\Adobe\\Acrobat Reader\\9.0\\InstallPath");

        _installed = (_reg != null);

        if (_installed)
        {
          // Get the path of AcroRd32.exe 
          m_install_path = _reg.GetValue("").ToString(); ;
          m_installing = false;
          m_installed = true;
        }
        else
        {
          if (m_installing)
          {
            m_init = false;
          }
        }

        return _installed;

      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Install the acrobat reader in silent and accept EULA
    //
    //  PARAMS:
    //      - INPUT: 
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public static void Install()
    {
      Process _process_acrord32;
      String _filename_install;

      if (m_installing)
      {
        return;
      }
      if (Installed)
      {
        return;
      }

      _filename_install="AdbeRdr1012_es_ES.exe";
      _process_acrord32 = PrepareProcess(_filename_install, "/msi EULA_ACCEPT=YES /qn");

      try
      {
        // Launch Install
        _process_acrord32.Start();
        m_installing = true;
      }
      catch 
      {
        Log.Error("Error AdobeReader.Install: The file " + _filename_install + " is not available or does not exist.");
      }      

    }

    //------------------------------------------------------------------------------
    // PURPOSE: Show the print dialog and print
    //
    //  PARAMS:
    //      - INPUT: 
    //            FileName: PDF file for print
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public static Boolean ShowPrintDialong (String Filename)
    {
      String _printer_name;
      PrintDialog _print_dialog;
      //PrinterSettings _ps;

      _print_dialog = new PrintDialog();
      _print_dialog.PrinterSettings.Copies = 2;
      _print_dialog.PrinterSettings.PrintRange = PrintRange.AllPages;
      _print_dialog.UseEXDialog = true;
      _print_dialog.PrintToFile = false;
      _print_dialog.AllowPrintToFile = false;
      _print_dialog.AllowSomePages = false;
      _print_dialog.ShowDialog();

      _printer_name = _print_dialog.PrinterSettings.PrinterName;

      return Print(Filename, _printer_name, _print_dialog.PrinterSettings.Copies);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Print the file with default printer of SO
    //
    //  PARAMS:
    //      - INPUT: 
    //            FileName: PDF file for print
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public static Boolean Print(String FileName, Int16 Copies)
     {
      PrintDocument _print_document;
      String _printer_default;
      Boolean _return_process;

      _print_document = new PrintDocument();
      _print_document.PrinterSettings.Copies = Copies;
      _printer_default=_print_document.PrinterSettings.PrinterName;

      _return_process = Print(FileName, _printer_default, Copies);
      _print_document.Dispose();

      return _return_process;      
     }

    public static DialogResult NotInstalledDialog(String NotInstalled, String BeingInstalled)
    {
      if (Installed)
      {
        return DialogResult.No;
      }

      if (m_installing)
      {
        return MessageBox.Show(BeingInstalled, "", MessageBoxButtons.OK);
      }
      else
      {
        return MessageBox.Show(NotInstalled, "", MessageBoxButtons.YesNo);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Print the file pdf with AcroRd32.exe
    //
    //  PARAMS:
    //      - INPUT: 
    //            FileName: PDF file for print
    //            PrinterName: Printer name used for print
    //            Copies: Number of copies
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    public static Boolean Print(String FileName, String PrinterName, Int16 Copies)
    {
      String _arguments;
      Process _process_acrord32;
      String _path_acrobat;
      Int16 _n;
      IntPtr _win_handle;

      if (!Installed)
      {
        if (m_installing)
        {
          //
        }
        else
        {
          //
        }
        return false;
      }

      //Prepare the process to print with arguments
      // Argument available:
      //    /n - Launch a new instance of Reader even if one is already open
      //    /s - Don't show the splash screen
      //    /o - Don't show the open file dialog
      //  * /h - Open as a minimized window
      //    /p <filename> - Open and go straight to the print dialog
      //  * /t <filename> <printername> <drivername> <portname> - Print the file the specified printer.
      //  * --> USED

      try
      {

        for (_n=0; _n < Copies; _n++)
        {

          _arguments = " /h /t \"" + FileName + "\" \"" + PrinterName + "\"";
          _path_acrobat = Path.Combine(m_install_path, "AcroRd32.exe");
          _process_acrord32 = PrepareProcess(_path_acrobat, _arguments);

          _process_acrord32.Start();

          if (!_process_acrord32.HasExited)
          {
            _process_acrord32.WaitForExit(5000);
            _process_acrord32.CloseMainWindow();
            _process_acrord32.Close();
          }
        }

        System.Threading.Thread.Sleep(5000);
        //Minimize all process acrobat reader
        foreach (Process _process in Process.GetProcesses())
        {

          if (_process.ProcessName == "AcroRd32")
          {
            _win_handle = _process.MainWindowHandle;
            ShowWindow(_win_handle, SW_MINIMIZE);
          }

        }

      }
      catch (Exception _ex)
      {
        Log.Error("Printer.PrinterPDF FileName=" + FileName.ToString());
        Log.Exception(_ex);

        return false;
      }

      return true;

    }

    #endregion

  }
}


