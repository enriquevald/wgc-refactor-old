//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: XML.cs
// 
//   DESCRIPTION: Some XML utils
// 
//        AUTHOR: Andreu Juli�
// 
// CREATION DATE: 12-MAR-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 12-MAR-2007 AJQ    First release.
// 22-OCT-2012 RXM    Added XmlToHtml function
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Xsl;
using System.IO;
using System.Text.RegularExpressions;
 

namespace WSI.Common
{
  /// <summary>
  /// Provides a set of methods to facilitate the work with XML.
  /// </summary>
  public static class XML
  {
    #region PUBLIC

    /// <summary>
    /// Parses a 
    /// </summary>
    /// <param name="number"></param>
    /// <returns></returns>
    static public double ParseDouble(string number)
    {
      double num;
      
      number = number.Replace(".", System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator);
      
      num = double.Parse(number);

      return num;

    }

    /// <summary>
    /// Parses a 
    /// </summary>
    /// <param name="number"></param>
    /// <returns></returns>
    static public string DoubleToXmlValue(double number)
    {
      String num;

      num = number.ToString("0.00");
      num = num.Replace(System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator, ".");

      return num;
    }

    /// <summary>
    /// Parses a 
    /// </summary>
    /// <param name="number"></param>
    /// <returns></returns>
    static public string StringToXmlValue(String Value)
    {
      String _xml;

      if (String.IsNullOrEmpty(Value))
      {
        return "";
      }

      _xml = Value;
      _xml = _xml.Replace("&", "&amp;");
      _xml = _xml.Replace("<", "&lt;");
      _xml = _xml.Replace(">", "&gt;");
      _xml = _xml.Replace("\"", "&quot;");
      _xml = _xml.Replace("'", "&apos;");

      return _xml;
    }
    

    /// <summary>
    /// Validates an XML document
    /// </summary>
    /// <param name="Xml">The xml string</param>
    /// <returns> - True:  on success
    ///           - False: otherwise
    /// </returns>
    static public bool TryParse(String Xml)
    {
      bool parsed;

      parsed = false;

      try
      {
        XmlDocument xml_document;

        xml_document = new XmlDocument();
        xml_document.LoadXml(Xml);

        parsed = true;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      return parsed;
    }

    /// <summary>
    /// Appends a child element to the node.
    /// </summary>
    /// <param name="Node">The parent</param>
    /// <param name="Name">Child name</param>
    /// <param name="Value">Child value</param>
    /// <returns>The child node</returns>
    public static XmlNode AppendChild(XmlNode Node, String Name, String Value)
    {
      XmlNode child;

      // Create the child node
      child = Node.OwnerDocument.CreateNode(XmlNodeType.Element, Name, "");
      // Set the value
      child.InnerText = Value;
      // Append the child to the node
      Node.AppendChild(child);
      // Return the child
      return child;
    }

    /// <summary>
    /// Insert a child element before the RefChild node.
    /// </summary>
    /// <param name="Node">The parent</param>
    /// <param name="RefChild">The new Child is placed before this node</param>
    /// <param name="Name">Child name</param>
    /// <param name="Value">Child value</param>
    /// <returns>The child node</returns>
    public static XmlNode InsertBefore(XmlNode Node, XmlNode RefChild, String Name, String Value)
    {
      XmlNode child;

      // Create the child node
      child = Node.OwnerDocument.CreateNode(XmlNodeType.Element, Name, "");
      // Set the value
      child.InnerText = Value;
      // Insert the new child before RefChild
      Node.InsertBefore(child, RefChild);
      // Return the child
      return child;
    }

    /// <summary>
    /// Retrieves the value of the child element named 'Name'
    /// </summary>
    /// <param name="Node">Parent node</param>
    /// <param name="Name">Child name</param>
    /// <returns>Child value</returns>
    public static String GetValue(XmlNode Node, String Name)
    {
      XmlNode child;

      child = Node.SelectSingleNode(Name);
      if (child != null)
      {
        return child.InnerXml;
      }
      return "";
    }

    /// <summary>
    /// Retrieves the value of the element named 'FieldName'
    /// </summary>
    /// <param name="XmlDoc">Xml string</param>
    /// <param name="FieldName">The field name</param>
    /// <returns>The field value</returns>
    public static String GetValue(String XmlDoc, String FieldName)
    {
      try
      {
        XmlDocument doc;
        XmlNode node;

        doc = new XmlDocument();
        doc.LoadXml(XmlDoc);

        node = doc.SelectSingleNode(FieldName);
        if (node == null)
        {
          return "";
        }
        return node.InnerXml;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);

        return "";
      }
    }

    /// <summary>
    /// Retrieves the value of the element named 'FieldName' in sequencial mode, 
    /// getting it from the last possition read. This methots overrides the old
    /// with a XmlNode input param.
    /// </summary>
    /// <param name="XmlDoc">Xml reader</param>
    /// <param name="FieldName">The field name</param>
    /// <returns>The field value</returns>
    public static String GetValue(XmlReader XmlReader, String FieldName, Boolean IsOptional)
    {
      return GetValue(XmlReader, FieldName, IsOptional, false);
    }

    public static String ReadTagValue(XmlReader XmlReader, String FieldName, Boolean IsOptional)
    {
      return GetValue(XmlReader, FieldName, IsOptional, true);
    }
    public static String ReadTagValue(XmlReader XmlReader, String FieldName)
    {
      return ReadTagValue(XmlReader, FieldName, false);
    }

    public static String GetValue(XmlReader XmlReader, String FieldName, Boolean IsOptional, Boolean ReadEndTag)
    {
      String _value;

      _value = "";
      
      try
      {
        //while ((XmlReader.Name != FieldName) || (XmlReader.NodeType != XmlNodeType.Element))
        while ((XmlReader.Name != FieldName) || (XmlReader.NodeType != XmlNodeType.Element) || (XmlReader.IsEmptyElement))
        {
          if (XmlReader.Name == "NoRegistroCaja" && (XmlReader.IsEmptyElement))
          {
            break;
          }
          else
          {
            if (!XmlReader.Read())
            {
              if (!IsOptional)
              {
                throw new SystemException("Node not found");
              }

              return _value;
            }
          }
        }

        // Found Element.Name = FieldName
        XmlReader.Read();

        //if (XmlReader.HasValue && XmlReader.NodeType == XmlNodeType.Text)        
        if (XmlReader.HasValue)
        {
          _value = XmlReader.Value;

          if (ReadEndTag)
          {
            XmlReader.Read();
          }
        }

        return _value;
      }
      catch (Exception ex)
      {
        Log.Warning("GetValue Node not found -> FieldName: " + FieldName.ToString());
        Log.Exception(ex);

        return "";
      }
    }

    public static String GetValue(XmlReader XmlReader, String FieldName)
    {
      return GetValue(XmlReader, FieldName, false);
    }

    /// <summary>
    /// Get XML formated datetime
    /// </summary>
    /// <param name="DateTime"></param>
    /// <returns></returns>
    public static String XmlDateTimeString(DateTime DateTime)
    {
      DateTime _utc;
      TimeSpan _ts;
      String _dt_str;

      _utc = DateTime.ToUniversalTime();
      _ts = DateTime.Subtract(_utc);

      _dt_str = DateTime.ToString("yyyy") + "-";
      _dt_str += DateTime.ToString("MM") + "-";
      _dt_str += DateTime.ToString("dd") + "T";
      _dt_str += DateTime.ToString("HH") + ":";
      _dt_str += DateTime.ToString("mm") + ":";
      _dt_str += DateTime.ToString("ss");

      if (_ts.TotalMinutes >= 0)
      {
        _dt_str += "+";
      }
      _dt_str += _ts.Hours.ToString("00") + ":";
      _dt_str += _ts.Minutes.ToString("00");

      return _dt_str;

    } // XmlDateTimeString
   
    
    /// <summary>
    /// Convert XML string to HTML to be displayable in a browser
    /// </summary>
    /// <param name="DateTime"></param>
    /// <returns></returns>
    public static String XmlToHtml(XmlDocument xmlToRender, bool escapeCdata)
    {

      XslCompiledTransform xslCompiledTransform;
      XmlReader xmlReader = null;
      StringReader stringReader = null;
      StringBuilder stringBuilder;
      XmlWriter xmlWriter = null;
      XsltSettings xsltSettings;
      try
      {
        xslCompiledTransform = new XslCompiledTransform(true);
        stringReader = new StringReader(Resources.FileResource.XmlToHtml10.ToString());
        xmlReader = XmlReader.Create(stringReader);
        xsltSettings = new XsltSettings(true, true);
        xslCompiledTransform.Load(xmlReader, xsltSettings, new XmlUrlResolver());
        stringBuilder = new StringBuilder();
        xmlWriter = XmlWriter.Create(stringBuilder);
        XsltArgumentList a = new XsltArgumentList();
        // Need to pass the xml string as an input parameter so
        // we can do some parsing for extra bits that XSLT won't do.
        a.AddParam("xmlinput", string.Empty, xmlToRender.OuterXml);
        if (escapeCdata)
        {
          string xml = xmlToRender.OuterXml.Replace("<![CDATA[", "&lt;![CDATA[").Replace("]]>", "]]&gt;");
          xml = Regex.Replace(xml, @"(&lt;!\[CDATA\[[^\]]*)(<)([^\]]*\]\]&gt;)", "$1&lt;$3");
          xml = Regex.Replace(xml, @"(&lt;!\[CDATA\[[^\]]*)(>)([^\]]*\]\]&gt;)", "$1&gt;$3");
          xmlToRender.LoadXml(xml);
        }
        xslCompiledTransform.Transform(xmlToRender, a, xmlWriter);
        string result = stringBuilder.ToString();
        xmlWriter.Close();
        return result;
      }
      catch (Exception e) { return e.Message; }
      finally
      {
        xslCompiledTransform = null;
        xmlReader.Close();
        xmlReader = null;
        stringReader.Close();
        stringReader = null;
        stringBuilder = null;
        if (xmlWriter != null) xmlWriter.Close();
        xmlWriter = null;
        xsltSettings = null;
      }
    }

    #endregion // PUBLIC
  }
}
