//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Alarm.cs
// 
//   DESCRIPTION: 
//
//        AUTHOR: RCI
// 
// CREATION DATE: 15-JUL-2011
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 15-JUL-2011 RCI    First release.
// 18-OCT-2011 JML    Fixed bug in DB_Save.
// 02-JUL-2012 ACC    Added Peru External Event.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace WSI.Common
{
  public class Alarm
  {
    private const int READ_PERIOD = 60 * 1000; 
    private static Boolean m_avoid_register = false;
    private static int m_tick = Misc.GetTickCount() - READ_PERIOD;

    #region Public Methods

    public static String ResourceId(AlarmCode Code)
    {
      return ResourceId((UInt32)Code);
    }

    public static String ResourceId(UInt32 Code)
    {
      return "STR_EA_0x" + Code.ToString("X").PadLeft(8, '0');
    }

    public static String Description(AlarmCode Code)
    {
      return Resource.String(ResourceId(Code));
    }

    public static Boolean Register(AlarmSourceCode SourceCode,
                                   Int64 SourceId,
                                   String SourceName,
                                   AlarmCode Code)
    {
      return Register(SourceCode, SourceId, SourceName, Code, null);
    }

    public static Boolean Register(AlarmSourceCode SourceCode,
                                   Int64  SourceId,
                                   String SourceName,
                                   AlarmCode Code,
                                   String Description)
    {
      AlarmSeverity _severity;
      String _desc;

      if (String.IsNullOrEmpty(Description))
      {
        _desc = Alarm.Description(Code);
      }
      else 
      {
        _desc = Description;
      }

      switch (Code)
      { 
        case AlarmCode.WebService_Session_Begin:
        case AlarmCode.WebService_Session_End:
        case AlarmCode.WebService_Transaction_Begin:
        case AlarmCode.WebService_Transaction_End:
          _severity = AlarmSeverity.Info; 
          break;

        case AlarmCode.WebService_Session_IncoherenceWithDBData:
        case AlarmCode.WebService_Session_InternalError:
        case AlarmCode.WebService_Transaction_Error:
          _severity = AlarmSeverity.Error; 
          break;
        
        case AlarmCode.WebService_Session_TimeOut:
        case AlarmCode.WebService_Session_BadChecksum:
        case AlarmCode.WebService_Session_BadLogin:
        case AlarmCode.WebService_Session_BadMessageType:
        case AlarmCode.WebService_Session_BadSeqID:
        case AlarmCode.WebService_Session_TooManyRetrys:
          _severity = AlarmSeverity.Warning; 
          break;

        case AlarmCode.Service_Started:
        case AlarmCode.Service_Running:
        case AlarmCode.Service_NewVersionDownloaded:
        case AlarmCode.SystemPowerStatus_ResumeCritical:
          _severity = AlarmSeverity.Info; 
        break;

        case AlarmCode.Service_Stopping:
        case AlarmCode.Service_StandBy:
        case AlarmCode.Service_LicenseWillExpireSoon:
        case AlarmCode.Service_NewVersionAvailable:
        case AlarmCode.User_MaxLoginAttemps:
        case AlarmCode.SystemPowerStatus_PowerStatusChange:
        case AlarmCode.SystemPowerStatus_BatteryLow:
          _severity = AlarmSeverity.Warning; 
        break;

        case AlarmCode.Service_Stopped:
        case AlarmCode.Service_LicenseExpired:
        default:
          _severity = AlarmSeverity.Error;
        break;
      }

      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (Register(SourceCode, SourceId, SourceName, (UInt32) Code, _desc, _severity, DateTime.MinValue, _db_trx.SqlTransaction))
        {
          _db_trx.Commit();

          return true;
        }
      }

      return false;
    } // Register


    public static Boolean Register(AlarmSourceCode SourceCode,
                                   Int64 SourceId,
                                   String SourceName,
                                   UInt32 AlarmCode,
                                   String Description,
                                   AlarmSeverity Severity,
                                   DateTime GeneratedDateTime,
                                   SqlTransaction SqlTrx)
    {
      return Register(SourceCode, SourceId, SourceName, AlarmCode, Description, Int64.MinValue, Int64.MinValue, Int32.MinValue, Severity, GeneratedDateTime, SqlTrx);
    }


    public static Boolean Register(AlarmSourceCode SourceCode,
                                   Int64 SourceId,
                                   String SourceName,
                                   UInt32 AlarmCode,
                                   String Description,
                                   Int64 OperadorId,
                                   Int64 EstablecimientoId,
                                   Int32 LineaId,                                   
                                   AlarmSeverity Severity,
                                   DateTime GeneratedDateTime,
                                   SqlTransaction SqlTrx)
    {
      StringBuilder _sql_str;

      

      if (Misc.GetElapsedTicks(m_tick) >= READ_PERIOD)
      { 
        int _disabled;

        m_avoid_register = false;

        _disabled = 0;
        if (int.TryParse(Misc.ReadGeneralParams("Site", "DisableNewAlarms", SqlTrx), out _disabled))
        {
          if (_disabled != 0)
          {
            m_avoid_register = true;
          }
        }

        m_tick = Misc.GetTickCount();
      }

      if (m_avoid_register)
      {
        return true;
      }

      try
      {
        _sql_str = new StringBuilder();

        _sql_str.AppendLine("INSERT INTO ALARMS ( AL_SOURCE_CODE        ");
        _sql_str.AppendLine("                   , AL_SOURCE_ID          ");
        _sql_str.AppendLine("                   , AL_SOURCE_NAME        ");
        _sql_str.AppendLine("                   , AL_ALARM_CODE         ");
        _sql_str.AppendLine("                   , AL_ALARM_NAME         ");
        _sql_str.AppendLine("                   , AL_ALARM_DESCRIPTION  ");      
        _sql_str.AppendLine("                   , AL_SEVERITY           ");
        _sql_str.AppendLine("                   , AL_REPORTED           ");
        _sql_str.AppendLine("                   , AL_DATETIME           ");
        _sql_str.AppendLine("                   )                       ");    
        _sql_str.AppendLine("     VALUES        ( @pSourceCode          ");
        _sql_str.AppendLine("                   , @pSourceId            ");
        _sql_str.AppendLine("                   , @pSourceName          ");
        _sql_str.AppendLine("                   , @pAlarmCode           ");
        _sql_str.AppendLine("                   , @pAlarmName           ");
        _sql_str.AppendLine("                   , @pAlarmDescription    ");      
        _sql_str.AppendLine("                   , @pSeverity            ");
        _sql_str.AppendLine("                   , GETDATE ()            ");

        if (GeneratedDateTime == DateTime.MinValue)
        {
          _sql_str.AppendLine("                 , GETDATE ()           ");
        }
        else
        { 
          _sql_str.AppendLine("                 , @pGenerated          ");
        }
        _sql_str.AppendLine("                   )                      ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sql_str.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pSourceCode", SqlDbType.Int).Value = SourceCode;
          _sql_cmd.Parameters.Add("@pSourceId", SqlDbType.BigInt).Value = SourceId;
          _sql_cmd.Parameters.Add("@pSourceName", SqlDbType.NVarChar).Value = SourceName;
          _sql_cmd.Parameters.Add("@pAlarmCode", SqlDbType.Int).Value = AlarmCode;
          _sql_cmd.Parameters.Add("@pAlarmName", SqlDbType.NVarChar).Value = SourceCode.ToString();
          _sql_cmd.Parameters.Add("@pAlarmDescription", SqlDbType.NVarChar).Value = Description;


          _sql_cmd.Parameters.Add("@pSeverity", SqlDbType.Int).Value = Severity;

          if (GeneratedDateTime != DateTime.MinValue)
          {
            _sql_cmd.Parameters.Add("@pGenerated", SqlDbType.DateTime).Value = GeneratedDateTime;
          }

          return (_sql_cmd.ExecuteNonQuery() == 1);
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // Register

    #endregion // Private Methods

  } // Alarm

  public class AlarmList : List<Alarm>
  {
    #region Public Methods

    public static AlarmList Create(AlarmSourceCode AlarmSource)
    {
      AlarmList _alarm_list;

      _alarm_list = new AlarmList();
      //switch (AlarmSource)
      //{
      //  case AlarmSourceCode.GameTerminal:
      //    _alarm_list.Add(new Alarm(AlarmCode.TerminalTestModeEnter, AlarmSeverity.Warning));
      //    _alarm_list.Add(new Alarm(AlarmCode.TerminalTestModeExit, AlarmSeverity.Warning));
      //    _alarm_list.Add(new Alarm(AlarmCode.TerminalConfigChanged, AlarmSeverity.Info));
      //    _alarm_list.Add(new Alarm(AlarmCode.TerminalPayoutChanged, AlarmSeverity.Info));
      //    _alarm_list.Add(new Alarm(AlarmCode.TerminalMeterReset, AlarmSeverity.Warning));
      //    //_alarm_list.Add(new Alarm(AlarmCode.TerminalEventList, AlarmSeverity.Warning));
      //    break;
      //  case AlarmSourceCode.Cashier:
      //    break;
      //  case AlarmSourceCode.User:
      //    _alarm_list.Add(new Alarm(AlarmCode.MaxLoginAttemps, AlarmSeverity.Warning));
      //    break;
      //  default:          //.Default
      //    _alarm_list.Add(new Alarm(AlarmCode.TerminalTestModeEnter, AlarmSeverity.Warning));
      //    _alarm_list.Add(new Alarm(AlarmCode.TerminalTestModeExit, AlarmSeverity.Warning));
      //    _alarm_list.Add(new Alarm(AlarmCode.TerminalConfigChanged, AlarmSeverity.Info));
      //    _alarm_list.Add(new Alarm(AlarmCode.TerminalPayoutChanged, AlarmSeverity.Info));
      //    _alarm_list.Add(new Alarm(AlarmCode.TerminalMeterReset, AlarmSeverity.Warning));
      //    //_alarm_list.Add(new Alarm(AlarmCode.TerminalEventList, AlarmSeverity.Warning));
      //    _alarm_list.Add(new Alarm(AlarmCode.MaxLoginAttemps, AlarmSeverity.Warning));
      //    break;
      //}

     
      return _alarm_list;
    } // Create

    #endregion // Public Methods

  } // AlarmList

} // WSI.Common
