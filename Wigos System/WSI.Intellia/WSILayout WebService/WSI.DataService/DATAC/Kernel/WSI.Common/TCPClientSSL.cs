//------------------------------------------------------------------------------
// Copyright © 2007 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: TCPClientSSL.cs
// 
//   DESCRIPTION: Class that implements a client connection using secure sockets layer (SSL)
// 
//        AUTHOR: Ronald Rodríguez
// 
// CREATION DATE: 21-AUG-2008
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 21-AUG-2008 RRT    First release.
//------------------------------------------------------------------------------
using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Net.Security;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Xml;
using System.Text;

namespace WSI.Common
{
  /// <summary>
  /// Provide with utilities for network related actions.
  /// </summary>
  public class TCPClientSSL
  {
    public delegate void OnMessageReceived(String Xml);

    #region Constants

    const String target_host = "ServerId";

    #endregion

    #region Class Atributes

    private TcpClient m_tcp_client;
    private SslStream m_ssl_stream;
    private Byte[] m_buffer;
    private WaitableQueue m_receive_queue;
    public OnMessageReceived MessageReceived;

    #endregion

    #region Private Methods


    /// <summary>
    /// Initializes a new instance of the SslStream class using the specified Stream.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="targetHost"></param>
    /// <param name="localCertificates"></param>
    /// <param name="remoteCertificate"></param>
    /// <param name="acceptableIssuers"></param>
    /// <returns></returns>
    private static X509Certificate OnLocalCertificateSelectionCallback(Object Sender,
                                                                       string TargetHost,
                                                                       X509CertificateCollection LocalCertificates,
                                                                       X509Certificate RemoteCertificate,
                                                                       string[] AcceptableIssuers)
    {
      X509Certificate2 certificate_509;

      // Send this certificate as the client certificate
      certificate_509 = new X509Certificate2(@"WCP_ServerCertificate.pfx", "xixero08");

      return certificate_509;

    } // OnLocalCertificateSelectionCallback


    /// <summary>
    /// Initializes a new instance of the SslStream class using the specified Stream.
    /// Check the certificate for errors and to make sure it meets your security policy.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="certificate"></param>
    /// <param name="chain"></param>
    /// <param name="errors"></param>
    /// <returns></returns>
    private static bool OnRemoteCertificateValidation(object Sender, X509Certificate Certificate, X509Chain Chain, SslPolicyErrors Errors)
    {
      X509Certificate2 certificate_509;

      certificate_509 = new X509Certificate2(Certificate);

      // Return true if there are no policy errors
      // The certificate can also be manually verified to make sure it meets your specific policies by
      // interrogating the x509Certificate object.
      if (Errors != SslPolicyErrors.None)
      {
        return true;
      }
      else
      {
        return true;
      }
    } // OnCertificateValidation

    /// <summary>
    /// Called when data is received.
    /// </summary>
    /// <param name="AsyncResult">Status of an asynchronous operation</param>
    private static void ReceiveCallback(IAsyncResult AsyncResult)
    {
      TCPClientSSL tcp_client_ssl;
      Int32 received_bytes;
      String xml;

      try
      {
        tcp_client_ssl = (TCPClientSSL)AsyncResult.AsyncState;

        if (!tcp_client_ssl.IsConnected)
        {
          return;
        }

        // Callback for complete data received.
        received_bytes = tcp_client_ssl.m_ssl_stream.EndRead(AsyncResult);
        if (received_bytes > 0)
        {
          xml = Encoding.UTF8.GetString(tcp_client_ssl.m_buffer, 0, received_bytes);

          // Enqueue string received to the notification queue.
          tcp_client_ssl.m_receive_queue.Enqueue(xml);

          // Begins an asynchronous read operation that reads data from the stream and stores it in the specified array.
          tcp_client_ssl.m_ssl_stream.BeginRead(tcp_client_ssl.m_buffer,    // Buffer.
                                    0,                                  // Offset.
                                    tcp_client_ssl.m_buffer.Length,       // Max. number of bytes to read.
                                    new AsyncCallback(ReceiveCallback), // Method to invoke when the read operation is complete.
                                    tcp_client_ssl);                    // Async state.

        }
        else
        {
          tcp_client_ssl.Disconnect();
        }
      }
      catch (Exception)
      {
        //Log.Exception(ex);
      }
    } // ReceiveCallback

    #endregion

    #region Public Methods

    /// <summary>
    /// Constructor Class
    /// </summary>
    public TCPClientSSL(WaitableQueue ReceiveQueue)
    {
      m_buffer = new Byte[64 * 1024]; // 64 K reception buffer

      m_receive_queue = ReceiveQueue;
    } // TCPClientSSL

    /// <summary>
    /// Implements a TCP socket connection.
    /// </summary>
    /// <param name="ServerAddress"></param>
    public void Connect(IPEndPoint ServerIPEndPoint)
    {
      try
      {
        // Step 1.
        // Instantiate a TcpClient with the target server and port number
        m_tcp_client = new TcpClient();

        m_tcp_client.Connect(ServerIPEndPoint);

        // Step 2.
        // Instantiate an SslStream with the NetworkStream returned from the TcpClient.
        m_ssl_stream = new SslStream(m_tcp_client.GetStream(), true, new RemoteCertificateValidationCallback(OnRemoteCertificateValidation), new LocalCertificateSelectionCallback(OnLocalCertificateSelectionCallback));

        // Step 3.
        // As a client, you can authenticate the server and validate the results using the SslStream.
        // This is the host name of the server you are connecting to, which may or may not be the name used
        // to connect to the server when TcpClient is instantiated.
        m_ssl_stream.AuthenticateAsClient(target_host);
      }
      catch (SocketException _soex)
      {
        Log.Message(_soex.Message);
      }
      catch 
      {
      }
      finally
      {
      }
    } // Connect

    public IAsyncResult BeginSend(Byte [] Buffer, AsyncCallback Callback, Object State)
    {
      return m_ssl_stream.BeginWrite(Buffer,              // Buffer.
                                   0,                   // Offset.
                                   Buffer.Length,       // Count
                                   Callback,            // Method to invoke when the write operation is complete.
                                   State);              // State
    }
    
    public void EndSend (IAsyncResult Result)
    {
      m_ssl_stream.EndWrite (Result);
    }
    
    
    public IAsyncResult BeginReceive (AsyncCallback Callback, Object State)
    {
      IAsyncResult result;
      
      // Set a callback function when receive something
      result = m_ssl_stream.BeginRead(this.m_buffer,                    // Buffer.
                                    0,                              // Offset.
                                    this.m_buffer.Length,             // Max. number of bytes to read.
                                    new AsyncCallback(Callback),    // Method to invoke when the read operation is complete.
                                    State);                         // Async state.
    
    
      return result;
    }
    
    public Byte [] EndReceive (IAsyncResult Result)
    {
      int num_bytes;
      Byte[] out_buffer;

      out_buffer =null;
      
      try
      {
        num_bytes = m_ssl_stream.EndRead(Result);
        if (num_bytes > 0)
        {
          out_buffer = new Byte[num_bytes];
          Array.Copy(this.m_buffer, out_buffer, num_bytes);
        }
      }
      catch (IOException _ioex)
      {
        Boolean _log;

        _log = true;
        
        if ( _ioex.InnerException != null )
        {
          if ( _ioex.InnerException is SocketException )
          {
            SocketException _soex;
            _soex = (SocketException)_ioex.InnerException;
            switch ( _soex.SocketErrorCode )
            {
              case SocketError.ConnectionReset:
              case SocketError.ConnectionAborted:
                _log = false;
              break;
            }
            
            if ( _log )
            {
              Log.Exception(_soex);
              _log = false;
            }
          }
        }
        
        if ( _log )
        {
          Log.Exception(_ioex);
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      

      return out_buffer;
    }


    /// <summary>
    /// Defines if a client is connected.
    /// </summary>
    public Boolean IsConnected
    {
      get
      {
        if (   m_tcp_client == null
            || m_ssl_stream == null )
        {
          return false;
        }
        
        if (   !m_tcp_client.Client.Connected
            || !m_ssl_stream.CanRead 
            || !m_ssl_stream.CanWrite
            || !m_ssl_stream.IsEncrypted
            || !m_ssl_stream.IsAuthenticated) // NOT IsMutuallyAuthenticated 
        {
          return false;
        }
        
        return true;
      }
    } // IsConnected

    /// <summary>
    /// Sends a string to a specific IP address and Port.
    /// </summary>
    /// <param name="Xml">Data to be sent</param>
    public Boolean Send(Byte[] Data)
    {
      try
      {
        if ( m_tcp_client == null )
        {
          return false;
        }
        if ( !m_tcp_client.Client.Connected )
        {
          return false;
        }
        if ( m_ssl_stream == null )
        {
          return false;
        }
        lock (m_ssl_stream)
        {
          if (!m_ssl_stream.CanWrite)
          {
            return false;
          }
          
          if ( true )
          {
            System.IO.Compression.DeflateStream _df;
            System.IO.MemoryStream _ms;
            
            _ms = new MemoryStream(Data.Length + 1024);
            _df = new System.IO.Compression.DeflateStream(_ms, System.IO.Compression.CompressionMode.Compress, true);
            _df.Write(Data, 0, Data.Length);
          }
          
          
          m_ssl_stream.Write(Data, 0, Data.Length);
          
          return true;
        }
      }
      catch 
      {
        return false;
      }
    } // Send
    

    /// <summary>
    /// Implements a client disconection.
    /// </summary>
    public void Disconnect()
    {
      if (m_ssl_stream != null)
      {
        try
        {
          m_ssl_stream.Close();
        }
        catch { };
      }
      if (m_tcp_client != null)
      {
        try
        {
          m_tcp_client.Close();
        }
        catch { };
      }
    } // Disconnect

    #endregion
  }
}