//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: RemoteSqlCommand.cs
// 
//   DESCRIPTION: Includes routines to transform a SqlCommand to a DataSet representation and viceversa.
// 
//        AUTHOR: Andreu Julia, Ra�l Cervera
// 
// CREATION DATE: 14-FEB-2011 Valentine's day
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 14-FEB-2011 AJQ, RCI  First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace WSI.Common
{
  public class RemoteSqlCmd
  {
    //------------------------------------------------------------------------------
    // PURPOSE : Return a DataSet representation of an SqlCommand.
    //
    //  PARAMS :
    //      - INPUT :
    //          - SqlCommand SqlCmd
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - DataSet
    //
    public static DataSet SqlCommandToDataSet(SqlCommand SqlCmd)
    {
      DataSet _ds_command;
      DataTable _table;
      DataRow _row;

      _ds_command = new DataSet("Query");
      _table = new DataTable("Command");

      _table.Columns.Add("CommandText", Type.GetType("System.String"));
      _table.Columns.Add("CommandTimeout", Type.GetType("System.Int32"));

      _row = _table.NewRow();
      _row["CommandText"] = SqlCmd == null ? "" : SqlCmd.CommandText;
      _row["CommandTimeout"] = SqlCmd == null ? 0 : SqlCmd.CommandTimeout; 
                    
      _table.Rows.Add(_row);

      _ds_command.Tables.Add(_table);

       if ( SqlCmd != null && SqlCmd.Parameters.Count > 0 )
       {
        _table = new DataTable("Parameter");
        _table.Columns.Add("Name", Type.GetType("System.String"));
        _table.Columns.Add("SqlDbType", Type.GetType("System.String"));
        _table.Columns.Add("Value", Type.GetType("System.Object"));

        foreach (SqlParameter _sql_param in SqlCmd.Parameters)
        {
          _row = _table.NewRow();

          _row["Name"] = _sql_param.ParameterName;
          _row["SqlDbType"] = _sql_param.SqlDbType.ToString();
          _row["Value"] = _sql_param.Value;

          _table.Rows.Add(_row);
        }

        _ds_command.Tables.Add(_table);
      }
      
      return _ds_command;
    } // SqlCommandToDataSet

    //------------------------------------------------------------------------------
    // PURPOSE : Return the SqlCommand saved in the DataSet QueryDataSet.
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataSet QueryDataSet
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - SqlCommand
    //
    public static SqlCommand DataSetToSqlCommand(DataSet QueryDataSet)
    {
      SqlCommand _sql_cmd;
      DataTable _table;
      Int32  _timeout;
      
      _sql_cmd = new SqlCommand();
      _table = QueryDataSet.Tables["Command"];
      if (_table.Rows.Count > 0)
      {
        _sql_cmd.CommandText = (String)_table.Rows[0]["CommandText"];
        _timeout = (Int32)_table.Rows[0]["CommandTimeout"];
        if (_timeout != 0)
        {
          _sql_cmd.CommandTimeout = _timeout;
        }
      }

      if (QueryDataSet.Tables.Contains("Parameter"))
      {
        _table = QueryDataSet.Tables["Parameter"];
        foreach (DataRow _row in _table.Rows)
        {
          SqlDbType _db_type;

          _db_type = (SqlDbType)Enum.Parse(typeof(SqlDbType), (String)_row["SqlDbType"]);
          _sql_cmd.Parameters.Add((String)_row["Name"], _db_type).Value = _row["Value"];
        }
      }

      return _sql_cmd;
    } // DataSetToSqlCommand

  } // RemoteSqlCmd

} // WSI.Common
