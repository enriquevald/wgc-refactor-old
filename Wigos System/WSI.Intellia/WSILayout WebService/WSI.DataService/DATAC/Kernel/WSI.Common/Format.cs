using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;
using System.Runtime.InteropServices;

namespace WSI.Common
{
  //////public static class Format
  //////{
  //////  public static String Number (Int64 Value)
  //////  {
  //////    return Value.ToString ("N");
  //////  }
  //////  public static String Decimal (Decimal Value)
  //////  {
  //////    return Value.ToString ("F");
  //////  }
  //////  public static String Decimal (Decimal Value, int NumberOfDecimalPlaces)
  //////  {
  //////    return Value.ToString ("F" + NumberOfDecimalPlaces.ToString ());
  //////  }
  //////  public static String Percent (Decimal Value)
  //////  {
  //////    return Value.ToString ("P");
  //////  }
  //////  public static String Percent (Decimal Value, int NumberOfDecimalPlaces)
  //////  {
  //////    return Value.ToString ("P" + NumberOfDecimalPlaces.ToString ());
  //////  }
  //////  public static String Currency (Decimal Value)
  //////  {
  //////    return Value.ToString ("C");
  //////  }
  //////  public static String Currency (Decimal Value, int NumberOfDecimalPlaces)
  //////  {
  //////    return Value.ToString ("C" + NumberOfDecimalPlaces.ToString ());
  //////  }
  //////}

  public class Format
  {
    public static String LocalNumberToDBNumber(String LocalNumber)
    {
      double num = Convert.ToDouble(LocalNumber);
      return num.ToString(System.Globalization.CultureInfo.GetCultureInfo("en-US").NumberFormat);
    }

    public static String DBNumberToLocalNumber(String DBNumber)
    {
      double num = Convert.ToDouble(DBNumber, System.Globalization.CultureInfo.GetCultureInfo("en-US").NumberFormat);
      return num.ToString();
    }
  }

  public struct Number
  {
    private Int64 m_value;

    public static implicit operator Number (Int64 rhs)
    {
      Number n;
      n.m_value = rhs;

      return n;
    }

    static public Number Zero()
    {
      Number x = 0;
      return x;
    }

    public Int32 Int32
    {
      get
      {
        return (Int32)m_value;
      }
    }

    public static implicit operator Int64 (Number rhs)
    {
      return rhs.m_value;
    }

    public override string ToString ()
    {
      return m_value.ToString ("N0");
    }
  }

  public struct Currency
  {
    private Decimal m_value;

    public SqlMoney SqlMoney  
    {
      get 
      {
        return (SqlMoney) m_value;
      }
    }    

    public static implicit operator Currency (SqlMoney rhs)
    {
      Currency c;
      c.m_value = (Decimal) rhs;

      return c;
    }

    public static implicit operator Currency (decimal rhs)
    {
      Currency c;
      c.m_value = rhs;

      return c;
    }

    public static implicit operator decimal (Currency rhs)
    {
      return rhs.m_value;
    }

    public override string ToString ()
    {
      return m_value.ToString ("C");
    }

    public string ToString(String s)
    {
      return m_value.ToString(s);
    }

    static public Currency Zero()
    {
      Currency x = 0;
      return x;
    }
  }

  public struct Points
  {
    private Decimal m_value;

    public SqlMoney SqlMoney
    {
      get
      {
        return (SqlMoney)m_value;
      }
    }

    public static implicit operator Points(SqlMoney rhs)
    {
      Points c;
      c.m_value = (Decimal)rhs;

      return c;
    }

    public static implicit operator Points(decimal rhs)
    {
      Points c;
      c.m_value = rhs;

      return c;
    }

    public static implicit operator decimal(Points rhs)
    {
      return rhs.m_value;
    }

    public override string ToString()
    {
      return m_value.ToString("#,##0");
    }

    public string ToString(String s)
    {
      return m_value.ToString(s);
    }

    static public Points Zero()
    {
      Points x = 0;
      return x;
    }
  }


  public struct Percentage
  {
    private Decimal m_value;

    public static implicit operator Percentage (decimal rhs)
    {
      Percentage p;
      p.m_value = rhs;

      return p;
    }

    public static implicit operator decimal (Percentage rhs)
    {
      return rhs.m_value;
    }

    public override string ToString ()
    {
      return m_value.ToString ("P");
    }

    public string ToStringSmall()
    {
      return m_value.ToString("0.##%");
    }

    public static string ToString(Decimal Value)
    {
      Percentage _aux_value;

      _aux_value = Value / 100;

      return _aux_value.ToStringSmall();
    }
  }



}
