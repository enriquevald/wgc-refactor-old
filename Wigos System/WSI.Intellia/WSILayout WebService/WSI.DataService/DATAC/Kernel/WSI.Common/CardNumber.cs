//------------------------------------------------------------------------------
// Copyright � 2008-2009 Win Systems International
//------------------------------------------------------------------------------
// 
//   MODULE NAME: CardNumber.cs
// 
//   DESCRIPTION: Magnetic card conversion functions
// 
//        AUTHOR: Agust� Poch
// 
// CREATION DATE: 08-OCT-2008
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 08-OCT-2008 APB    First release.
//------------------------------------------------------------------------------
using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Security.Cryptography;

namespace WSI.Common
{
  public class CardNumber
  {
    #region Constants

    private const Int32 CHECKSUM_MODULE = 9973;

    public enum CriptMode
    {
      NONE = 0,
      WCP  = 1,
    }

    #endregion

    #region Attributes

    #endregion

    #region Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Return encryption/descryption key.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    private static byte[] GetKey()
    {
      byte[] key;
      
      key = new byte[] { 35, 33, 94, 72, 71, 94, 36, 35, 70, 68, 83, 72, 74, 118, 98, 45 };

      return key;
    } // GetKey


    //------------------------------------------------------------------------------
    // PURPOSE: Return encryption/descryption IV parameter.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    private static byte[] GetIV()
    {
      byte[] iv;

      iv = new byte[] { 1 };      
      
      return iv;
    } // GetKey

    //------------------------------------------------------------------------------
    // PURPOSE: Receive the track data and split it into the card data components.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - TrackData: Encrypted track data.
    //
    //      - OUTPUT:
    //          - CardId: internal card identifier. (the one stored in the database)
    //          - CardType: card type.
    //
    // RETURNS:
    //      - True: Incoming track data has been decrypted and the checksum has been 
    //              correctly verified. Output parameters contain the valid results
    //      - False: Incoming track data has some obvious format errors (length) or 
    //               it fails the checksum test.
    // 
    //   NOTES:
    //    Card pattern: (20 digits)
    //      LCCCCNNNNNNNNNNNNNTT
    //    Where:
    //    - L: must be 0 (1 digit)
    //    - CCCC: Checksum. (4 digits)
    //    - NNNNNNNNNNNNN: Card identifier. (13 digits)
    //      - Site id. (4 digits)
    //      - sequence. (9 digits)
    //    - TT is the card type (2 digits)
    //
    public static bool TrackDataToCardNumber(String TrackData, out String CardId, out int CardType)
    {
      RC4CryptoServiceProvider rc4_CSP;
      String leading_digit_str;
      String checksum_str;
      String card_id_str;
      String card_type_str;
      UInt32 calc_checksum;
      String calc_checksum_str;
      UInt64 enc_input_data;
      UInt64 output_data;
      UInt64 card_id;
      byte[] output_dec = new byte[8];
      byte[] checksum_data;
      String max_value_str;

      // Initialize
      CardId = "";
      CardType = 0;

      try
      {
        // Check for old format compatibility
        if (TrackData.Length < 13)
        {
          // Invalid card number
          return false;
        }

        rc4_CSP = new RC4CryptoServiceProvider();
        rc4_CSP.KeySize = 64;

        // Decrypt card data
        //Get a decryptor that uses the same key and IV as the encryptor.
        ICryptoTransform decryptor = rc4_CSP.CreateDecryptor(GetKey(), GetIV());

        // Check that incoming parameter is not too big for an UInt64
        max_value_str = UInt64.MaxValue.ToString();

        if (String.Compare(TrackData, max_value_str) > 0)
        {
          return false;
        } // if

        // Obtain encrypted data
        enc_input_data = UInt64.Parse(TrackData);

        // Convert the int 64 into a byte array
        byte[] toDecrypt;
        toDecrypt = BitConverter.GetBytes(enc_input_data);

        // Now decrypt the previously encrypted message using the decryptor
        // obtained in the above step.
        MemoryStream msDecrypt = new MemoryStream(toDecrypt);
        CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read);

        csDecrypt.Read(output_dec, 0, 8);
        output_data = BitConverter.ToUInt64(output_dec, 0);

        // Obtain the 20 digit-long string
        TrackData = output_data.ToString();
        TrackData = TrackData.PadLeft(20, '0');

        // Split Card number into components:
        // It should follow the pattern (20 digits)
        //    LCCCCNNNNNNNNNNNNNTT
        // Where:
        //  - L: must be 0 (1 digit)
        //  - CCCC: Checksum. (4 digits)
        //  - NNNNNNNNNNNNN: Card identifier. (13 digits)
        //    - Site id. (4 digits)
        //    - sequence. (9 digits)
        //  - TT is the card type (2 digits)
        //
        leading_digit_str = TrackData.Substring(0, 1);
        checksum_str = TrackData.Substring(1, 4);
        card_id_str = TrackData.Substring(5, 13);
        card_type_str = TrackData.Substring(18, 2);

        // Check parts
        if (leading_digit_str != "0")
        {
          // Leading digit must be zero 
          return false;
        }

        calc_checksum = 0;
        card_id = UInt64.Parse(String.Concat(card_id_str, card_type_str));
        checksum_data = BitConverter.GetBytes(card_id);

        for (int i = 0; i < checksum_data.Length; i++)
        {
          calc_checksum += (Byte)(checksum_data[i]);
        }

        calc_checksum = calc_checksum % CHECKSUM_MODULE;
        calc_checksum_str = string.Format("{0:D4}", calc_checksum);

        if (checksum_str != calc_checksum_str)
        {
          // Incorrect checksum
          return false;
        }

        // Prepare results
        CardId = card_id_str;
        CardType = int.Parse(card_type_str);

        return true;
      }
      catch
      {
        return false;
      }

    } // TrackDataToCardNumber

    //------------------------------------------------------------------------------
    // PURPOSE: Convert the card number into a encrypted track data to be recorded.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - CardId: internal card identifier (the one stored in the database)
    //          - CardType: card type.
    //
    //      - OUTPUT:
    //          - TrackData: Encrypted card data.
    //
    // RETURNS:
    //      - True: Track data has been built using the incoming parameters
    //      - False: CardId format error or CardType not supported
    // 
    //   NOTES:
    //
    //    Card pattern (20 digits)
    //      LCCCCNNNNNNNNNNNNNTT
    //    Where:
    //    - L: must be 0 (1 digit)
    //    - CCCC: Checksum. (4 digits)
    //    - NNNNNNNNNNNNN: Card identifier. (13 digits)
    //      - Site id. (4 digits)
    //      - sequence. (9 digits)
    //    - TT is the card type (2 digits)
    //
    public static bool CardNumberToTrackData(out String TrackData, String CardId, int CardType)
    {
      RC4CryptoServiceProvider rc4_CSP;      
      String aux_track_data;
      String aux_checksum;
      UInt32 checksum;
      UInt64 card_id;
      UInt64 input_data;
      byte[] checksum_data;

      // Initialize
      TrackData = "";

      try
      {
        // Check incoming parameters
        if (CardId.Length != 13)
        {
          return false;
        }

        if (CardType != 0)
        {
          // Non-supported card type
          return false;
        }

        rc4_CSP = new RC4CryptoServiceProvider();
        rc4_CSP.KeySize = 64;

        // Get an encryptor.
        ICryptoTransform encryptor = rc4_CSP.CreateEncryptor(GetKey(), GetIV());

        // Encrypt the data as an array of encrypted bytes in memory.
        MemoryStream msEncrypt = new MemoryStream();
        CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write);

        // Calculate checksum
        card_id = UInt64.Parse(String.Concat(CardId, String.Format("{0:D2}", CardType)));
        checksum_data = BitConverter.GetBytes(card_id);

        checksum = 0;
        for (int i = 0; i < checksum_data.Length; i++)
        {
          checksum += (Byte)(checksum_data[i]);
        }

        checksum = checksum % CHECKSUM_MODULE;
        aux_checksum = string.Format("{0:D4}", checksum);

        // Build complete string
        aux_track_data = String.Concat(aux_checksum, CardId, String.Format("{0:D2}", CardType));

        // Encrypt the formatted buffer
        input_data = UInt64.Parse(aux_track_data);

        // Convert the int 64 into a byte arrary
        byte[] toEncrypt;
        toEncrypt = BitConverter.GetBytes(input_data);

        // Write all data to the crypto stream and flush it.
        csEncrypt.Write(toEncrypt, 0, toEncrypt.Length);
        csEncrypt.FlushFinalBlock();

        // Get the encrypted array of bytes.
        byte[] encrypted = msEncrypt.ToArray();

        TrackData = "";
        TrackData = BitConverter.ToUInt64(encrypted, 0).ToString();

        // Format number using 20 digits (add zero if necessary)
        TrackData = TrackData.PadLeft(20, '0');

        return true;
      }
      catch
      {
        return false;
      }

    } // CardNumberToTrackData


    //------------------------------------------------------------------------------
    // PURPOSE: Compose the card internal id. 
    // 
    // PARAMS:
    //      - INPUT:
    //          - SiteId: Site id.
    //          - Sequence: Sequence number.
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - Card number.
    // 
    // NOTES:
    //
    public static String GetCardNumberFromCardData(int SiteId, Int64 Sequence)
    {
      return String.Concat(String.Format("{0:D4}", SiteId), String.Format("{0:D9}", Sequence));

    } // GetCardNumber

    //------------------------------------------------------------------------------
    // PURPOSE: Split card number into its components:
    //          - Site Id.
    //          - Sequence number.
    // 
    // PARAMS:
    //      - INPUT:
    //          - CardNumber: Composed card internal id.
    //
    //      - OUTPUT:
    //          - SiteId: Site id.
    //          - Sequence: Sequence number.
    //
    // RETURNS:
    //
    // NOTES:
    //
    public static void SplitInternalTrackData(String CardNumber, out Int32 SiteId, out Int64 Sequence)
    {
      SiteId = Convert.ToInt32(CardNumber.Substring(0, 4));
      Sequence = Convert.ToInt64(CardNumber.Substring(4));

    } // GetCardDataFromCardNumber

    #endregion



    //------------------------------------------------------------------------------
    // PURPOSE: Split card number into its components:
    //          - Site Id.
    //          - Sequence number.
    // 
    // PARAMS:
    //      - INPUT:
    //          - CardNumber: Composed card internal id.
    //
    //      - OUTPUT:
    //          - SiteId: Site id.
    //          - Sequence: Sequence number.
    //
    // RETURNS:
    //
    // NOTES:
    //
    public static Boolean ConvertTrackData(CriptMode Mode, String ReceivedTrackData, out String DBTrackData)
    {
      DBTrackData = ReceivedTrackData;

      if (Mode == CriptMode.WCP)
      {
        int card_type;

        return TrackDataToCardNumber(ReceivedTrackData, out DBTrackData, out card_type);
      }
      
      return true;
    }
    
  } // class CardNumber
}
