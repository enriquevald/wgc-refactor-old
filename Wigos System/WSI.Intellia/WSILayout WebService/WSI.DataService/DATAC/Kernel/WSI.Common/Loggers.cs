//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: XML.cs
// 
//   DESCRIPTION: Loggers
// 
//        AUTHOR: Andreu Juli�
// 
// CREATION DATE: 12-MAR-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 12-MAR-2007 AJQ    First release.
// 25-MAR-2007 AJQ    Add FileLogger class
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using System.Configuration;

namespace WSI.Common
{
  /// <summary>
  /// Logger interface
  /// </summary>
  public interface ILogger
  {
    void WriteLine (String LogMsg);
    void Close ();
    void Close (String CloseMsg);
  }
  /// <summary>
  ///   Network Event Types
  /// </summary>
  public enum NetEvent
  {
    Connected,
    Disconnected,
    RemoteClosedConnection,
    ConnectionTimeout,
    Authenticated,
    AuthenticationError,
    DataReceived,
    DataSent,
    DataSendError,
    IdentityChange,
    ListenerStarted,
    ListenerStopped,
    ListenerError,
    MessageReceived,
    MessageFormatError,
    MessageSent,
  }

  /// <summary>
  ///   Defines the Net Logger Interface
  /// </summary>
  public interface INetLogger
  {
    void Add (NetEvent Event, String Who, String Msg);
    void Close ();
  }

  /// <summary>
  /// The common logger
  /// </summary>
  public static class Log
  {
    private static ArrayList listeners = new ArrayList();

    private static void WriteLine(String Log)
    {
      foreach (ILogger logger in listeners)
      {        
        logger.WriteLine(Log);
      }
    }

    private static void WriteLine(Int16 LogId, String Log)
    {
        ((ILogger)listeners[LogId]).WriteLine(Log);
    }

    public static void Close ()
    {
      foreach ( ILogger logger in listeners )
      {
        logger.Close ();
      }
    }

    public static void Error(String Msg)
    {
      WriteLine("Error:" + Msg);
    }
    public static void Warning(String Msg)
    {
      WriteLine("Warning:" + Msg);
    }
    public static void Message(String Msg)
    {
      WriteLine(Msg);
    }
    public static void Message(Int16 LogId, String Msg)
    {
      WriteLine(LogId, Msg);
    }

    public static void Exception(Int16 LogId, Exception Ex)
    {
      StringBuilder _sb;
      Exception _ex;
      int _nn;

      _sb = new StringBuilder();
      _sb.Append("EXCEPTION: ");
      _sb.AppendLine(Ex.Message);

      _nn = 1;
      for (_ex = Ex.InnerException; _ex != null; _ex = _ex.InnerException)
      {
        _sb.Append("- INNER #");
        _sb.Append(_nn.ToString());
        _sb.Append(": ");
        _sb.AppendLine(_ex.Message);
        _nn++;
      }
      _sb.Append("- STACK: ");
      _sb.AppendLine(Ex.StackTrace);

      WriteLine(LogId, _sb.ToString());
    }

    public static void Exception(Exception Ex)
    {
      StringBuilder _sb;
      Exception     _ex;
      int           _nn;
      
      _sb = new StringBuilder();
      _sb.Append("EXCEPTION: ");
      _sb.AppendLine(Ex.Message);
      
      _nn = 1;
      for (_ex = Ex.InnerException; _ex != null; _ex = _ex.InnerException)
      {
        _sb.Append("- INNER #");
        _sb.Append(_nn.ToString());
        _sb.Append(": ");
        _sb.AppendLine(_ex.Message);
        _nn++;
      }
      _sb.Append("- STACK: ");
      _sb.AppendLine(Ex.StackTrace);
      
      WriteLine(_sb.ToString());
    }

    public static void AddListener(ILogger NewLogger)
    {
      lock (listeners)
      {
        listeners.Add(NewLogger);
      }
    }
  }
  
  
   /// <summary>
  /// A simple logger
  /// </summary>
  public class ConsoleLogger:ILogger
  {
    public ConsoleLogger()
    {
      Console.SetWindowPosition(0, 0);
      Console.SetBufferSize(256, 10240);
      Console.SetWindowSize(150, 50);
    }

    public void Close(String Log)
    {
      WriteLine (Log);
    }
    
    public void Close ()
    {
      Close("");
    }

    public void WriteLine (String Log)
    {
      Console.WriteLine(SimpleLogger.FormatEvent(Log));
    }

  }


  /// <summary>
  /// A simple logger
  /// </summary>
  public class SimpleLogger : ILogger
  {
    private Int16 m_days_to_delete_log = 90;
    private DateTime m_next_history_clear;
    private WaitableQueue m_log_queue;
    private String m_base_name;
    private Boolean m_stopping = false;
    private Boolean m_stop = false;
    private System.Threading.Thread th;
    private Boolean m_hourly = false;


    private void SimpleLoggerInit(String BaseName, Boolean Hourly)
    {
      DirectoryInfo dir;

      m_log_queue = new WaitableQueue();

      dir = new DirectoryInfo(@"..\LogFiles");
      if (!dir.Exists)
      {
        dir.Create();
      }

      m_base_name = BaseName;
      m_hourly = Hourly;

      Misc.ClearHistoryLogFiles(@"..\LogFiles", @"*.txt", m_days_to_delete_log);
      m_next_history_clear = DateTime.Now.AddDays(1).Date;

      th = new System.Threading.Thread(SimpleLoggerThread);
      th.Start();
    }

    public SimpleLogger(String BaseName)
    {
      SimpleLoggerInit(BaseName, false);
    }

    public SimpleLogger(String BaseName, Boolean Hourly)
    {
      SimpleLoggerInit(BaseName, Hourly);
    }


    public void FileLogger(String BaseName)
    {
      DirectoryInfo dir;
      String _pathLog;
      m_log_queue = new WaitableQueue();
      _pathLog = ConfigurationManager.AppSettings["PathFileLogger"];

      dir = new DirectoryInfo(@"..\LogFiles");
      if (!dir.Exists)
      {
        dir.Create();
      }

      m_base_name = BaseName;

      Misc.ClearHistoryLogFiles(@"..\LogFiles", @"*.txt", m_days_to_delete_log);
      m_next_history_clear = DateTime.Now.AddDays(1).Date;

      th = new System.Threading.Thread(SimpleLoggerThread);
      th.Start();
    }


    public void Close(String Log)
    {
      m_stopping = true;
      m_log_queue.Enqueue(Log); // Don't remove this line
      m_stop = true;
      th.Join(5000);
    }

    public void Close()
    {
      Close("");
    }

    public void WriteLine(String Log)
    {
      if (m_stopping)
      {
        return;
      }
      m_log_queue.Enqueue(Log);
    }

    public static String FormatEvent(String Event)
    {
      String _event;
      StringBuilder _sb;
      String _tab;
      String _new_line;
      String _log_msg;
      DateTime _date;

      _sb = new StringBuilder();
      _sb.AppendLine();
      _new_line = _sb.ToString();

      _date = DateTime.Now;
      _log_msg = _date.Year.ToString("0000") + "/" + _date.Month.ToString("00") + "/" + _date.Day.ToString("00");
      _log_msg += " " + _date.Hour.ToString("00") + ":" + _date.Minute.ToString("00") + ":" + _date.Second.ToString("00");
      _log_msg += "." + _date.Millisecond.ToString("000");

      _tab = "";
      while (_tab.Length < _log_msg.Length - 1)
      {
        _tab += " ";
      }
      _tab = _new_line + _tab + "| ";

      _event = Event;
      while (_event.StartsWith(_new_line))
      {
        _event = _event.Substring(_event.IndexOf(_new_line) + _new_line.Length);
      }
      while (_event.EndsWith(_new_line))
      {
        _event = _event.Substring(0, _event.LastIndexOf(_new_line));
      }

      _log_msg = _date.Year.ToString("0000") + "/" + _date.Month.ToString("00") + "/" + _date.Day.ToString("00");
      _log_msg += " " + _date.Hour.ToString("00") + ":" + _date.Minute.ToString("00") + ":" + _date.Second.ToString("00");
      _log_msg += "." + _date.Millisecond.ToString("000");
      _event = _event.Replace(_new_line, _tab);
      _log_msg += " " + _event;

      return _log_msg;
    }
    /// <summary>
    /// Thread to write into logger
    /// </summary>
    private void SimpleLoggerThread()
    {
      String _event;
      String _log_msg;
      String _log_filename;
      DateTime _date;      // Date
      DateTime _prev_date;
      String _str_date;
      int _timeout;


      _prev_date = DateTime.Now.AddDays(-1);

      _timeout = 1 * 60 * 60 * 1000; // 1 Hour ...
      
      if (m_hourly)
      {
        _timeout = 15 * 60 * 1000; // 15 Minute ...
      }

      while (true)
      {
        Object _item;

        if (m_stop)
        {
          if (m_log_queue.Count == 0)
          {
            break;
          }
        }

        if (m_log_queue.Dequeue(out _item, _timeout)) // 1 Hour ...
        {
          _event = (String)_item;
        }
        else
        {

          _event = "...";
        }

        if (m_next_history_clear <= DateTime.Now)
        {
          Misc.ClearHistoryLogFiles(@"..\LogFiles", @"*.txt", m_days_to_delete_log);
          m_next_history_clear = DateTime.Now.AddDays(1).Date;
        }

        for (int i = 0; i < 3; i++)
        {
          try
          {
            _date = DateTime.Now;

            _str_date = _date.Year.ToString("0000") + _date.Month.ToString("00") + _date.Day.ToString("00");
            if (m_hourly)
            {
              _str_date = _str_date + "_" + _date.Hour.ToString("00") + "00";
            }

            _log_filename = @"..\LogFiles\" + m_base_name + "_" + _str_date + "_" + Environment.MachineName + ".txt";

            using (StreamWriter _log_file = new StreamWriter(_log_filename, true))
            {
              _log_msg = FormatEvent(_event);

              _log_file.WriteLine(_log_msg);
              _log_file.Close();
            }
            break;
          }
          catch
          {
            System.Threading.Thread.Sleep(1000);
          }
        }

      } // while

    } // SimpleLoggerThread
  }



  #region FileLogger
  /// <summary>
  /// File logger, especifies pathlogger to web.config
  /// </summary>
  public class FileLogger : ILogger
  {
    private Int16 m_days_to_delete_log = 90;
    private DateTime m_next_history_clear;
    private WaitableQueue m_log_queue;
    private String m_base_name;
    private Boolean m_stopping = false;
    private Boolean m_stop = false;
    private System.Threading.Thread th;
    
    public FileLogger(String BaseName)
    {
      DirectoryInfo dir;
      String _pathLog;
      m_log_queue = new WaitableQueue();
      _pathLog = ConfigurationManager.AppSettings["PathFileLogger"];

      dir = new DirectoryInfo(@""+_pathLog);
      if (!dir.Exists)
      {
        dir.Create();
      }

      m_base_name = BaseName;

      Misc.ClearHistoryLogFiles(@"" + _pathLog, @"*.txt", m_days_to_delete_log);
      m_next_history_clear = DateTime.Now.AddDays(1).Date;

      th = new System.Threading.Thread(FileLoggerThread);
      th.Start();
    }



    public void Close(String Log)
    {
      m_stopping = true;
      m_log_queue.Enqueue(Log); // Don't remove this line
      m_stop = true;
      th.Join(5000);
    }

    public void Close()
    {
      Close("");
    }

    public void WriteLine(String Log)
    {
      if (m_stopping)
      {
        return;
      }
      m_log_queue.Enqueue(Log);
    }

    public static String FormatEvent(String Event)
    {
      String _event;
      StringBuilder _sb;
      String _tab;
      String _new_line;
      String _log_msg;
      DateTime _date;

      _sb = new StringBuilder();
      _sb.AppendLine();
      _new_line = _sb.ToString();

      _date = DateTime.Now;
      _log_msg = _date.Year.ToString("0000") + "/" + _date.Month.ToString("00") + "/" + _date.Day.ToString("00");
      _log_msg += " " + _date.Hour.ToString("00") + ":" + _date.Minute.ToString("00") + ":" + _date.Second.ToString("00");
      _log_msg += "." + _date.Millisecond.ToString("000");

      _tab = "";
      while (_tab.Length < _log_msg.Length - 1)
      {
        _tab += " ";
      }
      _tab = _new_line + _tab + "| ";

      _event = Event;
      while (_event.StartsWith(_new_line))
      {
        _event = _event.Substring(_event.IndexOf(_new_line) + _new_line.Length);
      }
      while (_event.EndsWith(_new_line))
      {
        _event = _event.Substring(0, _event.LastIndexOf(_new_line));
      }

      _log_msg = _date.Year.ToString("0000") + "/" + _date.Month.ToString("00") + "/" + _date.Day.ToString("00");
      _log_msg += " " + _date.Hour.ToString("00") + ":" + _date.Minute.ToString("00") + ":" + _date.Second.ToString("00");
      _log_msg += "." + _date.Millisecond.ToString("000");
      _event = _event.Replace(_new_line, _tab);
      _log_msg += " " + _event;

      return _log_msg;
    }
    /// <summary>
    /// Thread to write into logger
    /// </summary>
    private void FileLoggerThread()
    {
      String _event;
      String _log_msg;
      String _log_filename;
      DateTime _date;      // Date
      DateTime _prev_date;
      String _pathLog;

      _prev_date = DateTime.Now.AddDays(-1);
      _pathLog = ConfigurationManager.AppSettings["PathFileLogger"];

      while (true)
      {
        Object _item;

        if (m_stop)
        {
          if (m_log_queue.Count == 0)
          {
            break;
          }
        }

        if (m_log_queue.Dequeue(out _item, 1 * 60 * 60 * 1000)) // 1 Hour ...
        {
          _event = (String)_item;
        }
        else
        {

          _event = "...";
        }

        if (m_next_history_clear <= DateTime.Now)
        {
          Misc.ClearHistoryLogFiles(@"" + _pathLog, @"*.txt", m_days_to_delete_log);
          m_next_history_clear = DateTime.Now.AddDays(1).Date;
        }

        for (int i = 0; i < 3; i++)
        {
          try
          {
            _date = DateTime.Now;
            _log_filename = @""+_pathLog + @"\" + m_base_name + "_" + _date.Year.ToString("0000") + _date.Month.ToString("00") + _date.Day.ToString("00") + "_" + Environment.MachineName + ".txt";

            using (StreamWriter _log_file = new StreamWriter(_log_filename, true))
            {
              _log_msg = FormatEvent(_event);

              _log_file.WriteLine(_log_msg);
              _log_file.Close();
            }
            break;
          }
          catch
          {
            System.Threading.Thread.Sleep(1000);
          }
        }

      } // while

    } // SimpleLoggerThread
  }
  #endregion

  /// <summary>
  /// The Network logger
  /// </summary>
  public static class NetLog
  {
    private static ArrayList listeners = new ArrayList ();

    /// <summary>
    /// Adds the given listener
    /// </summary>
    /// <param name="NewLogger">The new network logger</param>
    public static void AddListener (INetLogger NewLogger)
    {
      lock ( listeners )
      {
        listeners.Add (NewLogger);
      }
    }

    public static void Close ()
    {
      foreach ( INetLogger net_logger in listeners )
      {
        net_logger.Close ();
      }
    }

    /// <summary>
    /// Add an event
    /// </summary>
    /// <param name="Event"></param>
    /// <param name="Who"></param>
    /// <param name="Msg"></param>
    public static void Add (NetEvent Event, String Who, String Msg)
    {
      foreach ( INetLogger net_logger in listeners )
      {
        net_logger.Add (Event, Who, Msg);
      }
    }
  }

  public class NetLogger:INetLogger
  {
    private DateTime clear_date;
    private WaitableQueue netlogger_queue;
    private System.Threading.Thread th;
    private StreamWriter file;  // File


    /// <summary>
    /// Creates a network logger
    /// </summary>
    public NetLogger ()
    {
      DirectoryInfo dir;

      netlogger_queue = new WaitableQueue();

      dir = new DirectoryInfo(@"..\LogFiles\NetLog");
      if (!dir.Exists)
      {
        dir.Create();
      }

      clear_date = DateTime.Now.AddMinutes(5);

      th = new System.Threading.Thread(NetLoggerThread);
      th.Start();
    }

    public void Close ()
    {
      System.Threading.Thread.Sleep (1000);
      th.Abort ();
      if ( file != null )
      {
        file.Close ();
        file = null;
      }
    }

    /// <summary>
    /// Adds an event
    /// </summary>
    /// <param name="Event"></param>
    /// <param name="Who"></param>
    /// <param name="Msg"></param>
    public void Add(NetEvent Event, String Who, String Msg)
    {
      String event_string;
      String aux;
      String who;
      DateTime date;      // Date

      // AJQ Get the whole date-time information
      date = DateTime.Now;

      aux = Event.ToString();

      switch (Event)
      {
        case NetEvent.DataReceived:
        case NetEvent.DataSent:
          return;

        case NetEvent.MessageReceived:
        case NetEvent.MessageFormatError:
          aux = "-->  " + aux;
          break;
        case NetEvent.MessageSent:
          aux = " <-- " + aux;
          break;
      }
      while (aux.Length < 20)
      {
        aux = aux + " ";
      }
      who = Who;
      while (who.Length < 20)
      {
        who += " ";
      }
      if (who.Length > 20)
      {
        who = who.Substring(0, 20);
      }

      for (int i = 0; i < 3; i++)
      {
        try
        {
          event_string = date.Year.ToString("0000") + "/" + date.Month.ToString("00") + "/" + date.Day.ToString("00");
          event_string += " " + date.Hour.ToString("00") + ":" + date.Minute.ToString("00") + ":" + date.Second.ToString("00");
          event_string += "." + date.Millisecond.ToString("000");
          event_string += " " + who + " " + aux + " " + Msg;

          netlogger_queue.Enqueue (event_string);

          break;
        }
        catch
        {
          System.Threading.Thread.Sleep(20);
        }
      }
    }

    /// <summary>
    /// Thread to write into logger
    /// </summary>
    private void NetLoggerThread()
    {
      String event_string;
      String path;
      DateTime date;      // Date
      DateTime last_date;

      file = null;
      last_date = DateTime.Now.AddDays(-1);

      while (true)
      {

        event_string = (String) netlogger_queue.Dequeue();

        if (clear_date <= DateTime.Now)
        {
          // Remove all the files older than 7 days
          Misc.ClearHistoryLogFiles(@"..\LogFiles\NetLog", @"*.txt", 7);
          // First, increase the threshold date
          clear_date = DateTime.Now.AddHours(1);
        }

        for (int i = 0; i < 3; i++)
        {
          try
          {

            // AJQ Get the whole date-time information
            date = DateTime.Now;

            if (last_date.Year != date.Year
                || last_date.Month != date.Month
                || last_date.Day != date.Day
                || last_date.Hour != date.Hour)
            {
              if (file != null)
              {
                file.Close();
              }

              // Base name
              path = @"..\LogFiles\NetLog\NETLOG_";
              // Add date
              path += date.Year.ToString("0000") + date.Month.ToString("00") + date.Day.ToString("00");
              // Add hour
              path += "_" + date.Hour.ToString("00") + "00";
              // Add file extension
              path += ".txt";

              file = new StreamWriter(path, true);
            }

            file.WriteLine(event_string);

            break;
          }
          catch
          {
            System.Threading.Thread.Sleep (20);
          }
        }

      } // while

    } // NetLoggerThread
  }

  public class NetLoggerPerTerminal : INetLogger
  {
    private DateTime clear_date;
    private WaitableQueue netlogger_per_term_queue;

    /// <summary>
    /// Creates a network logger
    /// </summary>
    public NetLoggerPerTerminal()
    {
      DirectoryInfo dir;
      System.Threading.Thread th;

      netlogger_per_term_queue = new WaitableQueue();

      th = new System.Threading.Thread(NetLoggerPerTerminalThread);
      th.Start();

      dir = new DirectoryInfo(@"..\LogFiles\\NetLog");
      if (!dir.Exists)
      {
        dir.Create();
      }

      clear_date = DateTime.Now.AddMinutes(5);
    }

    public void Close ()
    { 
    }

    /// <summary>
    /// Adds an event
    /// </summary>
    /// <param name="Event"></param>
    /// <param name="Who"></param>
    /// <param name="Msg"></param>
    public void Add(NetEvent Event, String Who, String Msg)
    {
      String event_string;
      String aux;
      String who;
      String path;
      DateTime date;      // Date
      
      date = DateTime.Now;
      path = @"..\LogFiles\NetLog\NETLOG_" + date.Year.ToString("0000") + date.Month.ToString("00") + date.Day.ToString("00") + "_" +  Who + ".txt";

      aux = Event.ToString();

      switch (Event)
      {
        case NetEvent.DataReceived:
        case NetEvent.DataSent:
          return;

        case NetEvent.MessageReceived:
        case NetEvent.MessageFormatError:
          aux = "-->  " + aux;
          break;
        case NetEvent.MessageSent:
          aux = " <-- " + aux;
          break;
      }
      while (aux.Length < 20)
      {
        aux = aux + " ";
      }
      who = Who;
      while (who.Length < 20)
      {
        who += " ";
      }
      if (who.Length > 20)
      {
        who = who.Substring(0, 20);
      }

      for (int i = 0; i < 3; i++)
      {
        try
        {
          event_string = date.Year.ToString("0000") + "/" + date.Month.ToString("00") + "/" + date.Day.ToString("00");
          event_string += " " + date.Hour.ToString("00") + ":" + date.Minute.ToString("00") + ":" + date.Second.ToString("00");
          event_string += "." + date.Millisecond.ToString("000");
          event_string += " " + who + " " + aux + " " + Msg;

          netlogger_per_term_queue.Enqueue(event_string);

          break;
        }
        catch
        {
          System.Threading.Thread.Sleep(20);
        }
      }
    }

    /// <summary>
    /// Thread to write into logger
    /// </summary>
    private void NetLoggerPerTerminalThread()
    {
      String event_string;
      String who;
      String path;
      StreamWriter file;  // File
      DateTime date;      // Date

      file = null;

      while (true)
      {

        event_string = (String) netlogger_per_term_queue.Dequeue();

        if (clear_date <= DateTime.Now)
        {
          // Remove all the files older than 7 days
          Misc.ClearHistoryLogFiles(@"..\LogFiles\NetLog", @"*.txt", 7);
          // First, increase the threshold date
          clear_date = DateTime.Now.AddHours(1);
        }

        // AJQ Get the whole date-time information
        date = DateTime.Now;

        who = event_string.Substring(event_string.IndexOf(" ", event_string.IndexOf(" ") + 1) + 1, event_string.IndexOf(" ", event_string.IndexOf(" ", event_string.IndexOf(" ") + 1) + 1) - (event_string.IndexOf(" ", event_string.IndexOf(" ") + 1) + 1));
        if (who.LastIndexOf(":") > 0)
        {
          continue;
        }

        path = @"..\LogFiles\NetLog\NETLOG_" + date.Year.ToString("0000") + date.Month.ToString("00") + date.Day.ToString("00") + "_" + who + ".txt";

        for (int i = 0; i < 3; i++)
        {
          try
          {
            file = new StreamWriter(path, true);
            file.WriteLine(event_string);
            file.Flush();
            file.Close();

            break;
          }
          catch
          {
            System.Threading.Thread.Sleep(20);
          }
        }

      } // while

    } // NetLoggerPerTerminalThread

  }
}
