//------------------------------------------------------------------------------
// Copyright � 2010 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Mailing.cs
// 
//   DESCRIPTION: 
// 
//        AUTHOR: Raul Cervera
// 
// CREATION DATE: 27-DEC-2010
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 27-DEC-2010 RCI    First release.
// 03-NOV-2011 RCI    GameStatisticsToExcel(): Game name now includes the theorical payout (without braces).
// 23-JAN-2012 JMM    DataTableToExcel(): Columns formatting.
// 24-JAN-2012 RCI    Added routines DataSetToExcel() and GetFullFileName().
// 25-JAN-2012 RCI & AJQ    In FillSheetConnectedTerminals(), compare Providers ignoring case,
//                          so "win" and "WIN" are considered the same provider.
// 13-MAR-2012 MPO    Added switch case for each type of cell in DataTableToSheet
//                      * "System.Decimal","System.DateTime","System.String","System.Integer"
// 14-MAR-2012 RCI    When DataTable has more rows thant allowed by Excel, make another sheet to fill them in.
// 15-MAR-2012 RCI    Added an event function to update a progress bar.
//------------------------------------------------------------------------------
using System;
using System.Reflection;
using System.Collections.Generic;
using System.Text;
using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Globalization;
using System.Threading;
using System.Drawing;
using System.IO;
using System.Text.RegularExpressions;

namespace WSI.Common
{
  public delegate void ProgressBarEventHandler(Int32 Count, Int32 Max);

  public static class ExcelConversion
  {
    public static event ProgressBarEventHandler ProgressBar;

    private const Int32 ROW_OFFSET = 5;
    private const Int32 COL_OFFSET = 2;

    // http://www.mvps.org/dmcritchie/excel/colors.htm
    // ColorIndex:
    //  2 White
    // 15 Grey
    // 49 DarkBlue

    private const Int32 EXCEL_COLOR_INDEX_WHITE = 2;
    private const Int32 EXCEL_COLOR_INDEX_GRAY = 15;
    private const Int32 EXCEL_COLOR_INDEX_DARK_BLUE = 49;

    // MAX ROWS (EXCEL 2003)      --> 65.536
    // MAX ROWS (EXCEL 2007/2010) --> 1.048.576
    private const Int32 EXCEL_MAX_ROWS_BEFORE_2007 = 65000;
    private const Int32 EXCEL_MAX_ROWS_AFTER_2007 = 1048000;

    private static Int32 m_excel_max_rows = EXCEL_MAX_ROWS_BEFORE_2007;

    private static Int32 m_excel_color_light_blue;

    private static String m_currency_format;
    private static String m_number_format;
    private static String m_percent_format;

    private static String m_currency_simbol;
    private static String m_currency_separator;
    private static String m_percent_simbol;
    private static String m_number_separator;
    private static String m_percent_separator;

    // For Games Statistics
    private const Int32 GAMES_HEADER_ROW = 5;

    private static Boolean m_draw_spaces = false;

    private static Int32 GAMES_DATE;
    private static Int32 GAMES_PLAYED_AMOUNT;
    private static Int32 GAMES_WON_AMOUNT;
    private static Int32 GAMES_PAYOUT_PCT;
    private static Int32 GAMES_JACKPOT;
    private static Int32 GAMES_CONTRIBUTION_PCT;
    private static Int32 GAMES_CONTRIBUTION_AMOUNT;
    private static Int32 GAMES_NETWIN;
    private static Int32 GAMES_REAL_NETWIN;
    private static Int32 GAMES_REAL_PAYOUT_PCT;
    private static Int32 GAMES_PLAYS_COUNT;
    private static Int32 GAMES_WON_COUNT;
    private static Int32 GAMES_WON_PCT;
    private static Int32 GAMES_AVERAGE_PLAY;

    private static Int32 GAMES_FIRST_COLUMN;
    private static Int32 GAMES_LAST_COLUMN;

    private static Int32 GAMES_VLT;
    private static Int32 GAMES_NW_VLT;

    // For Terminals Connected
    private const Int32 TERMS_HEADER_ROW = 5;

    private static Int32 TERMS_DAY_START;
    private static Int32 TERMS_DAY_END;
    private static Int32 TERMS_PROVIDER;
    private static Int32 TERMS_TOTAL;

    private static Int32 TERMS_LAST_COLUMN;

    public static void OnProgressBar(Int32 Count, Int32 Max)
    {
      if (ProgressBar != null)
      {
        ProgressBar(Count, Max);
      }
    } // OnProgressBar

    //------------------------------------------------------------------------------
    // PURPOSE : Add the extension depending on the Excel version to the parameter Filename.
    //
    //  PARAMS :
    //      - INPUT :
    //          - String Filename
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String: The full filename.
    //
    public static String GetFullFileName(String Filename)
    {
      CultureInfo _old_ci;
      Excel.Application _excel;

      _old_ci = Thread.CurrentThread.CurrentCulture;
      _excel = null;

      try
      {
        // Excel needs the running thread with Culture as en-US.
        Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US", true);

        try
        {
          _excel = new Excel.ApplicationClass();
        }
        catch (Exception _ex)
        {
          _excel = null;
          Log.Exception(_ex);
        }

        if (_excel == null)
        {
          Log.Error("It appears that Excel is not installed on this machine. This operation requires MS Excel to be installed on this machine.");

          return null;
        }

        if (_excel.Version.CompareTo("12") >= 0)
        {
          Filename = Filename + ".xlsx";
        }
        else
        {
          Filename = Filename + ".xls";
        }

        return Filename;
      }
      finally
      {
        if (_excel != null)
        {
          try
          {
            _excel.Quit();
          }
          catch { }
        }
        ReleaseComObject(_excel);

        // Restore previous culture
        Thread.CurrentThread.CurrentCulture = _old_ci;
      }
    } // GetFullFileName

    public static Boolean DataSetToExcel(DataSet Set, String Filename)
    {
      Boolean _has_more_sheets;

      return DataSetToExcel(Set, Filename, out _has_more_sheets);
    } // DataSetToExcel

    //------------------------------------------------------------------------------
    // PURPOSE : Generic Conversion from DataSet to Excel file.
    //           Each Table from the DataSet to an Excel Sheet.
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataSet Set
    //          - String Filename
    //
    //      - OUTPUT :
    //          - Boolean HasMoreSheets
    //
    // RETURNS :
    //      - True: Converted ok. False: Otherwise.
    //
    public static Boolean DataSetToExcel(DataSet Set, String Filename, out Boolean HasMoreSheets)
    {
      CultureInfo _old_ci;
      Excel.Application _excel;
      Excel.Workbooks _books;
      Excel.Workbook _book;
      Excel.Sheets _sheets;
      Excel.Worksheet _sheet;
      Int32 _idx_sheet;
      Int32 _num_needed_sheets;
      Int32 _idx_start_row;
      Int32 _num_sheets_per_table;

      HasMoreSheets = false;

      _old_ci = Thread.CurrentThread.CurrentCulture;

      _excel = null;
      _books = null;
      _book = null;
      _sheets = null;
      _sheet = null;

      try
      {
        // Excel needs the running thread with Culture as en-US.
        Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US", true);

        // Restore DateTime and Number Format.
        Thread.CurrentThread.CurrentCulture.DateTimeFormat = _old_ci.DateTimeFormat;
        Thread.CurrentThread.CurrentCulture.NumberFormat = _old_ci.NumberFormat;

        _excel = null;
        try
        {
          _excel = new Excel.ApplicationClass();
        }
        catch (Exception _ex)
        {
          _excel = null;
          Log.Exception(_ex);
        }

        if (_excel == null)
        {
          Log.Error("It appears that Excel is not installed on this machine. This operation requires MS Excel to be installed on this machine.");

          return false;
        }

        _excel.DisplayAlerts = false;

        SetFileExtensionAndMaxRows(_excel, ref Filename);

        _books = _excel.Workbooks;
        _book = _books.Add(Missing.Value);

        _sheets = _book.Worksheets;

        // Calculate the needed sheets according to the maximum allowed rows.
        // Must be calculated after calling SetFileExtensionAndMaxRows().
        _num_needed_sheets = 0;
        foreach (DataTable _table in Set.Tables)
        {
          _num_sheets_per_table = (Int32)Math.Ceiling((Decimal)_table.Rows.Count / m_excel_max_rows);
          if (_num_sheets_per_table > 1)
          {
            HasMoreSheets = true;
          }
          _num_needed_sheets += _num_sheets_per_table;
        }

        // Add needed sheets.
        if (_num_needed_sheets - _sheets.Count > 0)
        {
          _sheets.Add(Missing.Value, Missing.Value, _num_needed_sheets - _sheets.Count, Excel.XlSheetType.xlWorksheet);
        }
        // Or close unused sheets.
        while (_sheets.Count > _num_needed_sheets)
        {
          _sheet = (Excel.Worksheet)_sheets[_sheets.Count];
          _sheet.Delete();
          ReleaseComObject(_sheet);
        }

        CalculateNumberFormats();

        _idx_sheet = 1;
        foreach (DataTable _table in Set.Tables)
        {
          _idx_start_row = 0;
          _num_sheets_per_table = (Int32)Math.Ceiling((Decimal)_table.Rows.Count / m_excel_max_rows);
          OnProgressBar(0, _table.Rows.Count);

          for (Int32 _num_sheet = 0; _num_sheet < _num_sheets_per_table; _num_sheet++)
          {
            _sheet = (Excel.Worksheet)_sheets[_idx_sheet];

            DataTableToSheet(_sheet, _table, _idx_start_row);

            _idx_sheet++;
            _idx_start_row += m_excel_max_rows;
          }
          OnProgressBar(_table.Rows.Count, _table.Rows.Count);
        }

        if (_num_needed_sheets > 1)
        {
          _sheet = (Excel.Worksheet)_sheets[1];
          ((Excel._Worksheet)_sheet).Activate();
        }

        _book.SaveCopyAs(Filename);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
      finally
      {
        if (_sheets != null)
        {
          for (_idx_sheet = 1; _idx_sheet <= _sheets.Count; _idx_sheet++)
          {
            _sheet = (Excel.Worksheet)_sheets[_idx_sheet];
            ReleaseComObject(_sheet);
          }
        }
        ReleaseComObject(_sheets);

        if (_book != null)
        {
          try
          {
            _book.Close(false, Missing.Value, Missing.Value);
          }
          catch { }
        }

        ReleaseComObject(_book);
        ReleaseComObject(_books);

        if (_excel != null)
        {
          try
          {
            _excel.Quit();
          }
          catch { }
        }
        ReleaseComObject(_excel);

        // Restore previous culture
        Thread.CurrentThread.CurrentCulture = _old_ci;
      }
    } // DataSetToExcel

    public static Boolean DataTableToExcel(DataTable Table, String Filename)
    {
      Boolean _has_more_sheets;

      return DataTableToExcel(Table, Filename, out _has_more_sheets);
    } // DataTableToExcel

    //------------------------------------------------------------------------------
    // PURPOSE : Generic Conversion from DataTable to Excel file.
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataTable Table
    //          - String Filename
    //
    //      - OUTPUT :
    //          - Boolean HasMoreSheets
    //
    // RETURNS :
    //      - True: Converted ok. False: Otherwise.
    //
    public static Boolean DataTableToExcel(DataTable Table, String Filename, out Boolean HasMoreSheets)
    {
      CultureInfo _old_ci;
      Excel.Application _excel;
      Excel.Workbooks _books;
      Excel.Workbook _book;
      Excel.Sheets _sheets;
      Excel.Worksheet _sheet;
      Int32 _idx_sheet;
      Int32 _num_needed_sheets;
      Int32 _idx_start_row;

      HasMoreSheets = false;

      _old_ci = Thread.CurrentThread.CurrentCulture;

      _excel = null;
      _books = null;
      _book = null;
      _sheets = null;
      _sheet = null;

      try
      {
        // Excel needs the running thread with Culture as en-US.
        Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US", true);

        // Restore DateTime and Number Format.
        Thread.CurrentThread.CurrentCulture.DateTimeFormat = _old_ci.DateTimeFormat;
        Thread.CurrentThread.CurrentCulture.NumberFormat = _old_ci.NumberFormat;

        _excel = null;
        try
        {
          _excel = new Excel.ApplicationClass();
        }
        catch (Exception _ex)
        {
          _excel = null;
          Log.Exception(_ex);
        }

        if (_excel == null)
        {
          Log.Error("It appears that Excel is not installed on this machine. This operation requires MS Excel to be installed on this machine.");

          return false;
        }

        _excel.DisplayAlerts = false;

        SetFileExtensionAndMaxRows(_excel, ref Filename);

        _books = _excel.Workbooks;
        _book = _books.Add(Missing.Value);

        _sheets = _book.Worksheets;

        // Calculate the needed sheets according to the maximum allowed rows.
        // Must be calculated after calling SetFileExtensionAndMaxRows().
        _num_needed_sheets = (Int32)Math.Ceiling((Decimal)Table.Rows.Count / m_excel_max_rows);
        if (_num_needed_sheets > 1)
        {
          HasMoreSheets = true;
        }

        // Add needed sheets.
        if (_num_needed_sheets - _sheets.Count > 0)
        {
          _sheets.Add(Missing.Value, Missing.Value, _num_needed_sheets - _sheets.Count, Excel.XlSheetType.xlWorksheet);
        }
        // Or close unused sheets.
        while (_sheets.Count > _num_needed_sheets)
        {
          _sheet = (Excel.Worksheet)_sheets[_sheets.Count];
          _sheet.Delete();
          ReleaseComObject(_sheet);
        }

        CalculateNumberFormats();

        _idx_sheet = 1;
        _idx_start_row = 0;
        OnProgressBar(0, Table.Rows.Count);

        for (Int32 _num_sheet = 0; _num_sheet < _num_needed_sheets; _num_sheet++)
        {
          _sheet = (Excel.Worksheet)_sheets[_idx_sheet];

          DataTableToSheet(_sheet, Table, _idx_start_row);

          _idx_sheet++;
          _idx_start_row += m_excel_max_rows;
        }
        OnProgressBar(Table.Rows.Count, Table.Rows.Count);

        if (_num_needed_sheets > 1)
        {
          _sheet = (Excel.Worksheet)_sheets[1];
          ((Excel._Worksheet)_sheet).Activate();
        }

        _book.SaveCopyAs(Filename);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
      finally
      {
        if (_sheets != null)
        {
          for (_idx_sheet = 1; _idx_sheet <= _sheets.Count; _idx_sheet++)
          {
            _sheet = (Excel.Worksheet)_sheets[_idx_sheet];
            ReleaseComObject(_sheet);
          }
        }
        ReleaseComObject(_sheets);

        if (_book != null)
        {
          try
          {
            _book.Close(false, Missing.Value, Missing.Value);
          }
          catch { }
        }

        ReleaseComObject(_book);
        ReleaseComObject(_books);

        if (_excel != null)
        {
          try
          {
            _excel.Quit();
          }
          catch { }
        }
        ReleaseComObject(_excel);

        // Restore previous culture
        Thread.CurrentThread.CurrentCulture = _old_ci;
      }
    } // DataTableToExcel

    //------------------------------------------------------------------------------
    // PURPOSE : Conversion from Game Statistics to Excel file.
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataSet DataSet
    //          - DateTime FromDate
    //          - DateTime ToDate
    //          - String Filename
    //
    //      - OUTPUT :
    //          - String Filename
    //
    // RETURNS :
    //      - True: Converted ok. False: Otherwise.
    //
    public static Boolean GameStatisticsToExcel(DataSet DataSet,
                                                DateTime FromDate,
                                                DateTime ToDate,
                                                ref String Filename)
    {
      CultureInfo _old_ci;
      Excel.Application _excel;
      Excel.Window _window;
      Excel.Workbooks _books;
      Excel.Workbook _book;
      Excel.Sheets _sheets;
      Excel.Worksheet _sheet;
      Excel.Range _cells;
      Excel.Range _range;
      Excel.Font _font;
      DataTable _dt_only_game_names;
      Int32 _num_games;
      Int32 _idx_sheet;
      DataSetHelper _dsh;
      DataTable _dt_games;
      DataTable _dt_connected;
      DataTable _total_sales;

      _old_ci = Thread.CurrentThread.CurrentCulture;

      _excel = null;
      _books = null;
      _book = null;
      _sheets = null;
      _sheet = null;

      try
      {
        _dt_games = DataSet.Tables["GAMES"];
        _dt_connected = DataSet.Tables["CONNECTED"];

        _dt_only_game_names = _dt_games.DefaultView.ToTable(true, "Game");
        _num_games = _dt_only_game_names.Rows.Count;

        SetExcelColumnIndices();

        // Excel needs the running thread with Culture as en-US.
        Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US", true);

        // Restore DateTime and Number Format.
        Thread.CurrentThread.CurrentCulture.DateTimeFormat = _old_ci.DateTimeFormat;
        Thread.CurrentThread.CurrentCulture.NumberFormat = _old_ci.NumberFormat;

        _excel = null;
        try
        {
          _excel = new Excel.ApplicationClass();
        }
        catch (Exception _ex)
        {
          _excel = null;
          Log.Exception(_ex);
        }

        if (_excel == null)
        {
          Log.Error("It appears that Excel is not installed on this machine. This operation requires MS Excel to be installed on this machine.");

          return false;
        }

        _excel.DisplayAlerts = false;

        SetFileExtensionAndMaxRows(_excel, ref Filename);

        _books = _excel.Workbooks;
        _book = _books.Add(Missing.Value);

        _sheets = _book.Worksheets;

        // If no games, send empty Excel file.
        if (_num_games == 0)
        {
          if (_sheets.Count == 0)
          {
            _sheets.Add(Missing.Value, Missing.Value, 1, Excel.XlSheetType.xlWorksheet);
          }
          while (_sheets.Count > 1)
          {
            _sheet = (Excel.Worksheet)_sheets[_sheets.Count];
            _sheet.Delete();
            ReleaseComObject(_sheet);
          }

          _sheet = (Excel.Worksheet)_sheets[1];
          _cells = (Excel.Range)_sheet.Cells;
          _range = (Excel.Range)_cells[3, 2];
          _font = _range.Font;

          _range.Value2 = "NO HAY DATOS";
          _font.Size = 16;
          _font.Name = "Calibri";
          _font.Bold = true;

          ReleaseComObject(_font);
          ReleaseComObject(_range);
          ReleaseComObject(_cells);
          ReleaseComObject(_sheet);

          _book.SaveCopyAs(Filename);

          return true;
        }

        // Add needed sheets.
        if (_num_games - _sheets.Count > 0)
        {
          _sheets.Add(Missing.Value, Missing.Value, _num_games - _sheets.Count, Excel.XlSheetType.xlWorksheet);
        }
        // Or close unused sheets.
        while (_sheets.Count > _num_games)
        {
          _sheet = (Excel.Worksheet)_sheets[_sheets.Count];
          _sheet.Delete();
          ReleaseComObject(_sheet);
        }

        CalculateNumberFormats();

        // RCI 07-APR-2011: Add TOTAL tab in first place.
        _sheet = (Excel.Worksheet)_sheets[1];
        _sheets.Add(_sheet, Missing.Value, 1, Excel.XlSheetType.xlWorksheet);

        // Get a DataTable with the totals.
        _dsh = new DataSetHelper();
        _total_sales = _dsh.SelectGroupByInto("TOTAL", _dt_games,
                               "SiteIdentifier,SiteName,Game,Date,sum(PlayedAmount) PlayedAmount,sum(WonAmount) WonAmount," +
                               "sum(PlayedCount) PlayedCount,sum(WonCount) WonCount,sum(NumTerminals) NumTerminals," +
                               "sum(Jackpot) Jackpot,JackpotContributionPct", "", "Date");

        _sheet = (Excel.Worksheet)_sheets[1];
        ((Excel._Worksheet)_sheet).Activate();
        _sheet.Name = "TOTAL";

        _window = _excel.ActiveWindow;
        _window.DisplayGridlines = false;
        ReleaseComObject(_window);

        FillSheetGameStatistics(_sheet, FromDate, ToDate, _total_sales.Select());
        FillSheetGameTotalVLT(_sheet, FromDate, ToDate, _dt_connected.Select());

        _idx_sheet = 2;

        foreach (DataRow _game in _dt_only_game_names.Rows)
        {
          String _game_name;

          _game_name = (String)_game[0];
          _game_name = _game_name.Replace("WSI - ", "").Replace("[", "").Replace("]", "").Trim();

          _sheet = (Excel.Worksheet)_sheets[_idx_sheet];
          ((Excel._Worksheet)_sheet).Activate();

          _sheet.Name = _game_name;

          _window = _excel.ActiveWindow;
          _window.DisplayGridlines = false;
          ReleaseComObject(_window);

          FillSheetGameStatistics(_sheet, FromDate, ToDate, _dt_games.Select("Game = '" + _game[0] + "'"));

          _idx_sheet++;
        }

        _sheet = (Excel.Worksheet)_sheets[1];
        ((Excel._Worksheet)_sheet).Activate();

        _book.SaveCopyAs(Filename);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
      finally
      {
        if (_sheets != null)
        {
          for (_idx_sheet = 1; _idx_sheet <= _sheets.Count; _idx_sheet++)
          {
            _sheet = (Excel.Worksheet)_sheets[_idx_sheet];
            ReleaseComObject(_sheet);
          }
        }
        ReleaseComObject(_sheets);

        if (_book != null)
        {
          try
          {
            _book.Close(false, Missing.Value, Missing.Value);
          }
          catch { }
        }

        ReleaseComObject(_book);
        ReleaseComObject(_books);

        if (_excel != null)
        {
          try
          {
            _excel.Quit();
          }
          catch { }
        }
        ReleaseComObject(_excel);

        // Restore previous culture
        Thread.CurrentThread.CurrentCulture = _old_ci;
      }
    } // GameStatisticsToExcel

    //------------------------------------------------------------------------------
    // PURPOSE : Conversion from Connected Terminals to Excel file.
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataSet ConnectedTerminalsSet
    //          - DateTime FromDate
    //          - DateTime ToDate
    //          - String Filename
    //
    //      - OUTPUT :
    //          - String Filename
    //
    // RETURNS :
    //      - True: Converted ok. False: Otherwise.
    //
    public static Boolean ConnectedTerminalsToExcel(DataSet ConnectedTerminalsSet,
                                                    DateTime FromDate,
                                                    DateTime ToDate,
                                                    ref String Filename)
    {
      CultureInfo _old_ci;
      Excel.Application _excel;
      Excel.Window _window;
      Excel.Workbooks _books;
      Excel.Workbook _book;
      Excel.Sheets _sheets;
      Excel.Worksheet _sheet;
      Excel.Range _cells;
      Excel.Range _range;
      Excel.Font _font;
      Int32 _num_tables_in_set;
      Int32 _idx_sheet;
      DateTime _month_date;
      Int32 _last_day;

      _old_ci = Thread.CurrentThread.CurrentCulture;

      _excel = null;
      _books = null;
      _book = null;
      _sheets = null;
      _sheet = null;

      try
      {
        _num_tables_in_set = ConnectedTerminalsSet.Tables.Count;

        // Excel needs the running thread with Culture as en-US.
        Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US", true);

        // Restore DateTime and Number Format.
        Thread.CurrentThread.CurrentCulture.DateTimeFormat = _old_ci.DateTimeFormat;
        Thread.CurrentThread.CurrentCulture.NumberFormat = _old_ci.NumberFormat;

        _excel = null;
        try
        {
          _excel = new Excel.ApplicationClass();
        }
        catch (Exception _ex)
        {
          _excel = null;
          Log.Exception(_ex);
        }

        if (_excel == null)
        {
          Log.Error("It appears that Excel is not installed on this machine. This operation requires MS Excel to be installed on this machine.");

          return false;
        }

        _excel.DisplayAlerts = false;

        SetFileExtensionAndMaxRows(_excel, ref Filename);

        _books = _excel.Workbooks;
        _book = _books.Add(Missing.Value);

        _sheets = _book.Worksheets;

        // If no tables, send empty Excel file.
        if (_num_tables_in_set == 0)
        {
          if (_sheets.Count == 0)
          {
            _sheets.Add(Missing.Value, Missing.Value, 1, Excel.XlSheetType.xlWorksheet);
          }
          while (_sheets.Count > 1)
          {
            _sheet = (Excel.Worksheet)_sheets[_sheets.Count];
            _sheet.Delete();
            ReleaseComObject(_sheet);
          }

          _sheet = (Excel.Worksheet)_sheets[1];
          _cells = (Excel.Range)_sheet.Cells;
          _range = (Excel.Range)_cells[3, 2];
          _font = _range.Font;

          _range.Value2 = "NO HAY DATOS";
          _font.Size = 16;
          _font.Name = "Calibri";
          _font.Bold = true;

          ReleaseComObject(_font);
          ReleaseComObject(_range);
          ReleaseComObject(_cells);
          ReleaseComObject(_sheet);

          _book.SaveCopyAs(Filename);

          return true;
        }

        // Add needed sheets.
        if (_num_tables_in_set - _sheets.Count > 0)
        {
          _sheets.Add(Missing.Value, Missing.Value, _num_tables_in_set - _sheets.Count, Excel.XlSheetType.xlWorksheet);
        }
        // Or close unused sheets.
        while (_sheets.Count > _num_tables_in_set)
        {
          _sheet = (Excel.Worksheet)_sheets[_sheets.Count];
          _sheet.Delete();
          ReleaseComObject(_sheet);
        }

        // Need to translate colors from .Net to OLE.
        m_excel_color_light_blue = ColorTranslator.ToOle(Color.FromArgb(79, 129, 189));

        CalculateNumberFormats();

        _idx_sheet = 1;

        foreach (DataTable _connected_terms in ConnectedTerminalsSet.Tables)
        {
          if (_connected_terms.TableName == "CONNECTED_TERMINALS_CURRENT_MONTH" ||
              _num_tables_in_set == 1)
          {
            _month_date = FromDate;
            _last_day = ToDate.Day;
          }
          else
          {
            _month_date = FromDate.AddMonths(-1);
            _last_day = FromDate.AddDays(-1).Day;
          }

          _sheet = (Excel.Worksheet)_sheets[_idx_sheet];
          ((Excel._Worksheet)_sheet).Activate();

          _window = _excel.ActiveWindow;
          _window.DisplayGridlines = false;
          ReleaseComObject(_window);

          FillSheetConnectedTerminals(_sheet, _month_date, _last_day, _connected_terms);

          _idx_sheet++;
        }

        _sheet = (Excel.Worksheet)_sheets[1];
        ((Excel._Worksheet)_sheet).Activate();

        _book.SaveCopyAs(Filename);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
      finally
      {
        if (_sheets != null)
        {
          for (_idx_sheet = 1; _idx_sheet <= _sheets.Count; _idx_sheet++)
          {
            _sheet = (Excel.Worksheet)_sheets[_idx_sheet];
            ReleaseComObject(_sheet);
          }
        }
        ReleaseComObject(_sheets);

        if (_book != null)
        {
          try
          {
            _book.Close(false, Missing.Value, Missing.Value);
          }
          catch { }
        }

        ReleaseComObject(_book);
        ReleaseComObject(_books);

        if (_excel != null)
        {
          try
          {
            _excel.Quit();
          }
          catch { }
        }
        ReleaseComObject(_excel);

        // Restore previous culture
        Thread.CurrentThread.CurrentCulture = _old_ci;
      }
    } // ConnectedTerminalsToExcel

    //------------------------------------------------------------------------------
    // PURPOSE : Fill an Excel sheet with the game statistics.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Excel.Worksheet Sheet
    //          - DateTime FromDate
    //          - DateTime ToDate
    //          - DataRow[] GameRows
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static void FillSheetGameStatistics(Excel.Worksheet Sheet,
                                                DateTime FromDate,
                                                DateTime ToDate,
                                                DataRow[] GameRows)
    {
      Excel.Application _app;
      Excel.Window _window;
      Excel.Range _cells;
      Excel.Range _cell1;
      Excel.Range _cell2;
      Excel.Range _range;
      Excel.Range _entire_column;
      Excel.Range _entire_row;
      Excel.Font _font;
      Excel.Interior _interior;
      Int32 _xls_row;
      Int32 _first_data_row;
      Int32 _last_data_row;
      Int32 _idx_row;
      Int32 _idx_col;
      DateTime _current_date;
      DateTime _stats_date;

      _cells = Sheet.Cells;

      _font = _cells.Font;
      _font.Size = 9;
      _font.Name = "Calibri";
      ReleaseComObject(_font);

      _range = (Excel.Range)_cells[1, 6];
      _range.Value2 = GameRows[0]["SiteName"];
      ReleaseComObject(_range);

      _range = (Excel.Range)_cells[2, 6];
      _range.Value2 = Sheet.Name;
      ReleaseComObject(_range);


      _cell1 = (Excel.Range)_cells[1, 6];
      _cell2 = (Excel.Range)_cells[2, 6];
      _range = Sheet.get_Range(_cell1, _cell2);
      _font = _range.Font;

      _font.Size = 12;
      _font.Bold = true;

      ReleaseComObject(_font);
      ReleaseComObject(_range);
      ReleaseComObject(_cell1);
      ReleaseComObject(_cell2);


      _cell1 = (Excel.Range)_cells[1, GAMES_PLAYED_AMOUNT];
      _cell2 = (Excel.Range)_cells[1, GAMES_LAST_COLUMN];
      _range = Sheet.get_Range(_cell1, _cell2);
      _entire_row = _range.EntireRow;

      _range.Merge(false);
      _range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
      _range.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
      _entire_row.RowHeight = 15.75;

      ReleaseComObject(_entire_row);
      ReleaseComObject(_range);
      ReleaseComObject(_cell1);
      ReleaseComObject(_cell2);


      _cell1 = (Excel.Range)_cells[2, GAMES_PLAYED_AMOUNT];
      _cell2 = (Excel.Range)_cells[2, GAMES_LAST_COLUMN];
      _range = Sheet.get_Range(_cell1, _cell2);
      _entire_row = _range.EntireRow;

      _range.Merge(false);
      _range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
      _range.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
      _entire_row.RowHeight = 15.75;

      ReleaseComObject(_entire_row);
      ReleaseComObject(_range);
      ReleaseComObject(_cell1);
      ReleaseComObject(_cell2);


      // Column Headers

      _xls_row = GAMES_HEADER_ROW;

      _cell1 = (Excel.Range)_cells[_xls_row, GAMES_PLAYED_AMOUNT];
      _cell2 = (Excel.Range)_cells[_xls_row, GAMES_REAL_PAYOUT_PCT];
      _range = Sheet.get_Range(_cell1, _cell2);

      _range.Merge(false);
      _range.Value2 = "Amount in Mexican Pesos";

      ReleaseComObject(_range);
      ReleaseComObject(_cell1);
      ReleaseComObject(_cell2);


      _cell1 = (Excel.Range)_cells[_xls_row, GAMES_PLAYS_COUNT];
      _cell2 = (Excel.Range)_cells[_xls_row, GAMES_WON_PCT];
      _range = Sheet.get_Range(_cell1, _cell2);

      _range.Merge(false);
      _range.Value2 = "Plays";

      ReleaseComObject(_range);
      ReleaseComObject(_cell1);
      ReleaseComObject(_cell2);


      _cell1 = (Excel.Range)_cells[_xls_row, GAMES_PLAYED_AMOUNT];
      _cell2 = (Excel.Range)_cells[_xls_row, GAMES_WON_PCT];
      _range = Sheet.get_Range(_cell1, _cell2);
      _font = _range.Font;
      _interior = _range.Interior;
      _entire_row = _range.EntireRow;

      _range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
      _range.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
      _font.Bold = true;
      _font.ColorIndex = EXCEL_COLOR_INDEX_WHITE;
      _interior.ColorIndex = EXCEL_COLOR_INDEX_DARK_BLUE;
      _entire_row.RowHeight = 17.25;

      ReleaseComObject(_entire_row);
      ReleaseComObject(_interior);
      ReleaseComObject(_font);
      ReleaseComObject(_range);
      ReleaseComObject(_cell1);
      ReleaseComObject(_cell2);

      if (m_draw_spaces)
      {
        _xls_row++;

        _range = (Excel.Range)_cells[_xls_row, GAMES_FIRST_COLUMN];
        _range.EntireRow.RowHeight = 5.25;
        ReleaseComObject(_range);
      }

      _xls_row++;

      _range = (Excel.Range)_cells[_xls_row, GAMES_DATE];
      _range.Value2 = "DATE";
      ReleaseComObject(_range);

      _range = (Excel.Range)_cells[_xls_row, GAMES_PLAYED_AMOUNT];
      _range.Value2 = "Played";
      ReleaseComObject(_range);

      _range = (Excel.Range)_cells[_xls_row, GAMES_WON_AMOUNT];
      _range.Value2 = "Won";
      ReleaseComObject(_range);
      
      _range = (Excel.Range)_cells[_xls_row, GAMES_PAYOUT_PCT];
      _range.Value2 = "Payout %";
      ReleaseComObject(_range);
      
      _range = (Excel.Range)_cells[_xls_row, GAMES_JACKPOT];
      _range.Value2 = "Jackpot";
      ReleaseComObject(_range);


      _cell1 = (Excel.Range)_cells[_xls_row, GAMES_CONTRIBUTION_PCT];
      _cell2 = (Excel.Range)_cells[_xls_row, GAMES_CONTRIBUTION_AMOUNT];
      _range = Sheet.get_Range(_cell1, _cell2);

      _range.Merge(false);
      _range.Value2 = "Contribution";

      ReleaseComObject(_range);
      ReleaseComObject(_cell1);
      ReleaseComObject(_cell2);


      _range = (Excel.Range)_cells[_xls_row, GAMES_NETWIN];
      _range.Value2 = "Netwin";
      ReleaseComObject(_range);

      _range = (Excel.Range)_cells[_xls_row, GAMES_REAL_NETWIN];
      _range.Value2 = "REAL Netwin";
      ReleaseComObject(_range);
      
      _range = (Excel.Range)_cells[_xls_row, GAMES_REAL_PAYOUT_PCT];
      _range.Value2 = "Real Payout %";
      ReleaseComObject(_range);

      _range = (Excel.Range)_cells[_xls_row, GAMES_PLAYS_COUNT];
      _range.Value2 = "Plays";
      ReleaseComObject(_range);

      _range = (Excel.Range)_cells[_xls_row, GAMES_WON_COUNT];
      _range.Value2 = "Won";
      ReleaseComObject(_range);
      
      _range = (Excel.Range)_cells[_xls_row, GAMES_WON_PCT];
      _range.Value2 = "Won %";
      ReleaseComObject(_range);

      _range = (Excel.Range)_cells[_xls_row, GAMES_AVERAGE_PLAY];
      _range.Value2 = "Average Play";
      ReleaseComObject(_range);


      _cell1 = (Excel.Range)_cells[_xls_row, GAMES_FIRST_COLUMN];
      _cell2 = (Excel.Range)_cells[_xls_row, GAMES_LAST_COLUMN];
      _range = Sheet.get_Range(_cell1, _cell2);
      _font = _range.Font;
      _interior = _range.Interior;
      _entire_row = _range.EntireRow;

      _range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
      _range.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
      _font.Bold = true;
      _font.ColorIndex = EXCEL_COLOR_INDEX_WHITE;
      _interior.ColorIndex = EXCEL_COLOR_INDEX_DARK_BLUE;
      _entire_row.RowHeight = 27.75;

      ReleaseComObject(_entire_row);
      ReleaseComObject(_interior);
      ReleaseComObject(_font);
      ReleaseComObject(_range);
      ReleaseComObject(_cell1);
      ReleaseComObject(_cell2);


      // Data Rows

      _xls_row++;
      _first_data_row = _xls_row;
      _current_date = FromDate;

      foreach (DataRow _stats in GameRows)
      {
        if (_xls_row > m_excel_max_rows)
        {
          break;
        }

        _stats_date = (DateTime)_stats["Date"];

        while (_current_date < _stats_date)
        {
          SetEmptyRow(Sheet, _xls_row, _current_date);

          _xls_row++;
          _current_date = _current_date.AddDays(1);
        }
        _current_date = _stats_date.AddDays(1);

        if (_xls_row % 2 == (m_draw_spaces ? 1 : 0))
        {
          _cell1 = (Excel.Range)_cells[_xls_row, GAMES_FIRST_COLUMN];
          _cell2 = (Excel.Range)_cells[_xls_row, GAMES_LAST_COLUMN];
          _range = Sheet.get_Range(_cell1, _cell2);
          _interior = _range.Interior;

          _interior.ColorIndex = EXCEL_COLOR_INDEX_GRAY;

          ReleaseComObject(_interior);
          ReleaseComObject(_range);
          ReleaseComObject(_cell1);
          ReleaseComObject(_cell2);
        }

        _range = (Excel.Range)_cells[_xls_row, GAMES_DATE];
        _range.Value2 = _stats_date;
        ReleaseComObject(_range);

        _range = (Excel.Range)_cells[_xls_row, GAMES_PLAYED_AMOUNT];
        _range.Value2 = (Decimal)_stats["PlayedAmount"];
        ReleaseComObject(_range);

        _range = (Excel.Range)_cells[_xls_row, GAMES_WON_AMOUNT];
        _range.Value2 = (Decimal)_stats["WonAmount"];
        ReleaseComObject(_range);
        
        _range = (Excel.Range)_cells[_xls_row, GAMES_PAYOUT_PCT];
        _range.Value2 = "=IF(" + ColName(GAMES_PLAYED_AMOUNT) + _xls_row + "> 0, " + ColName(GAMES_WON_AMOUNT) + _xls_row + "/" + ColName(GAMES_PLAYED_AMOUNT) + _xls_row + ", \"\")";
        ReleaseComObject(_range);

        _range = (Excel.Range)_cells[_xls_row, GAMES_JACKPOT];
        _range.Value2 = (Decimal)_stats["Jackpot"];
        ReleaseComObject(_range);

        _range = (Excel.Range)_cells[_xls_row, GAMES_CONTRIBUTION_PCT];
        _range.Value2 = (0.01m * (Decimal)_stats["JackpotContributionPct"]);
        ReleaseComObject(_range);
        
        _range = (Excel.Range)_cells[_xls_row, GAMES_CONTRIBUTION_AMOUNT];
        _range.Value2 = "=" + ColName(GAMES_PLAYED_AMOUNT) + _xls_row + "*" + ColName(GAMES_CONTRIBUTION_PCT) + _xls_row;
        ReleaseComObject(_range);

        _range = (Excel.Range)_cells[_xls_row, GAMES_NETWIN];
        _range.Value2 = "=" + ColName(GAMES_PLAYED_AMOUNT) + _xls_row + "-" + ColName(GAMES_WON_AMOUNT) + _xls_row;
        ReleaseComObject(_range);

        _range = (Excel.Range)_cells[_xls_row, GAMES_REAL_NETWIN];
        _range.Value2 = "=" + ColName(GAMES_NETWIN) + _xls_row + "+" + ColName(GAMES_JACKPOT) + _xls_row + "-" + ColName(GAMES_CONTRIBUTION_AMOUNT) + _xls_row;
        ReleaseComObject(_range);
        
        _range = (Excel.Range)_cells[_xls_row, GAMES_REAL_PAYOUT_PCT];
        _range.Value2 = "=IF(" + ColName(GAMES_PLAYED_AMOUNT) + _xls_row + "> 0, 1-" + ColName(GAMES_REAL_NETWIN) + _xls_row + "/" + ColName(GAMES_PLAYED_AMOUNT) + _xls_row + ", \"\")";
        ReleaseComObject(_range);

        _range = (Excel.Range)_cells[_xls_row, GAMES_PLAYS_COUNT];
        _range.Value2 = _stats["PlayedCount"];
        ReleaseComObject(_range);

        _range = (Excel.Range)_cells[_xls_row, GAMES_WON_COUNT];
        _range.Value2 = _stats["WonCount"];
        ReleaseComObject(_range);
        
        _range = (Excel.Range)_cells[_xls_row, GAMES_WON_PCT];
        _range.Value2 = "=IF(" + ColName(GAMES_PLAYS_COUNT) + _xls_row + "> 0, " + ColName(GAMES_WON_COUNT) + _xls_row + "/" + ColName(GAMES_PLAYS_COUNT) + _xls_row + ", \"\")";
        ReleaseComObject(_range);
        
        _range = (Excel.Range)_cells[_xls_row, GAMES_AVERAGE_PLAY];
        _range.Value2 = "=IF(" + ColName(GAMES_PLAYS_COUNT) + _xls_row + "> 0, " + ColName(GAMES_PLAYED_AMOUNT) + _xls_row + "/" + ColName(GAMES_PLAYS_COUNT) + _xls_row + ", \"\")";
        ReleaseComObject(_range);

        _xls_row++;
      }

      while (_current_date < ToDate)
      {
        SetEmptyRow(Sheet, _xls_row, _current_date);

        _xls_row++;
        _current_date = _current_date.AddDays(1);
      }

      _last_data_row = _xls_row - 1;

      // Totals

      _range = (Excel.Range)_cells[_xls_row, GAMES_DATE];
      _range.Value2 = "TOTAL:";
      _range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
      ReleaseComObject(_range);

      _range = (Excel.Range)_cells[_xls_row, GAMES_PLAYED_AMOUNT];
      _range.Value2 = "=SUM(" + ColName(GAMES_PLAYED_AMOUNT) + _first_data_row + ":" + ColName(GAMES_PLAYED_AMOUNT) + (_xls_row - 1) + ")";
      ReleaseComObject(_range);

      _range = (Excel.Range)_cells[_xls_row, GAMES_WON_AMOUNT];
      _range.Value2 = "=SUM(" + ColName(GAMES_WON_AMOUNT) + _first_data_row + ":" + ColName(GAMES_WON_AMOUNT) + (_xls_row - 1) + ")";
      ReleaseComObject(_range);

      _range = (Excel.Range)_cells[_xls_row, GAMES_PAYOUT_PCT];
      _range.Value2 = "=IF(" + ColName(GAMES_PLAYED_AMOUNT) + _xls_row + "> 0, " + ColName(GAMES_WON_AMOUNT) + _xls_row + "/" + ColName(GAMES_PLAYED_AMOUNT) + _xls_row + ", \"\")";
      ReleaseComObject(_range);

      _range = (Excel.Range)_cells[_xls_row, GAMES_JACKPOT];
      _range.Value2 = "=SUM(" + ColName(GAMES_JACKPOT) + _first_data_row + ":" + ColName(GAMES_JACKPOT) + (_xls_row - 1) + ")";
      ReleaseComObject(_range);

      _range = (Excel.Range)_cells[_xls_row, GAMES_CONTRIBUTION_AMOUNT];
      _range.Value2 = "=SUM(" + ColName(GAMES_CONTRIBUTION_AMOUNT) + _first_data_row + ":" + ColName(GAMES_CONTRIBUTION_AMOUNT) + (_xls_row - 1) + ")";
      ReleaseComObject(_range);

      _range = (Excel.Range)_cells[_xls_row, GAMES_NETWIN];
      _range.Value2 = "=SUM(" + ColName(GAMES_NETWIN) + _first_data_row + ":" + ColName(GAMES_NETWIN) + (_xls_row - 1) + ")";
      ReleaseComObject(_range);

      _range = (Excel.Range)_cells[_xls_row, GAMES_REAL_NETWIN];
      _range.Value2 = "=SUM(" + ColName(GAMES_REAL_NETWIN) + _first_data_row + ":" + ColName(GAMES_REAL_NETWIN) + (_xls_row - 1) + ")";
      ReleaseComObject(_range);
      
      _range = (Excel.Range)_cells[_xls_row, GAMES_REAL_PAYOUT_PCT];
      _range.Value2 = "=IF(" + ColName(GAMES_PLAYED_AMOUNT) + _xls_row + "> 0, 1-" + ColName(GAMES_REAL_NETWIN) + _xls_row + "/" + ColName(GAMES_PLAYED_AMOUNT) + _xls_row + ", \"\")";
      ReleaseComObject(_range);

      _range = (Excel.Range)_cells[_xls_row, GAMES_PLAYS_COUNT];
      _range.Value2 = "=SUM(" + ColName(GAMES_PLAYS_COUNT) + _first_data_row + ":" + ColName(GAMES_PLAYS_COUNT) + (_xls_row - 1) + ")";
      ReleaseComObject(_range);

      _range = (Excel.Range)_cells[_xls_row, GAMES_WON_COUNT];
      _range.Value2 = "=SUM(" + ColName(GAMES_WON_COUNT) + _first_data_row + ":" + ColName(GAMES_WON_COUNT) + (_xls_row - 1) + ")";
      ReleaseComObject(_range);
      
      _range = (Excel.Range)_cells[_xls_row, GAMES_WON_PCT];
      _range.Value2 = "=IF(" + ColName(GAMES_PLAYS_COUNT) + _xls_row + "> 0, " + ColName(GAMES_WON_COUNT) + _xls_row + "/" + ColName(GAMES_PLAYS_COUNT) + _xls_row + ", \"\")";
      ReleaseComObject(_range);
      
      _range = (Excel.Range)_cells[_xls_row, GAMES_AVERAGE_PLAY];
      _range.Value2 = "=IF(" + ColName(GAMES_PLAYS_COUNT) + _xls_row + "> 0, " + ColName(GAMES_PLAYED_AMOUNT) + _xls_row + "/" + ColName(GAMES_PLAYS_COUNT) + _xls_row + ", \"\")";
      ReleaseComObject(_range);

      // Format Total Row
      _cell1 = (Excel.Range)_cells[_xls_row, GAMES_FIRST_COLUMN];
      _cell2 = (Excel.Range)_cells[_xls_row, GAMES_LAST_COLUMN];
      _range = Sheet.get_Range(_cell1, _cell2);
      _font = _range.Font;
      _interior = _range.Interior;
      _entire_row = _range.EntireRow;

      _range.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
      _font.ColorIndex = EXCEL_COLOR_INDEX_WHITE;
      _interior.ColorIndex = EXCEL_COLOR_INDEX_DARK_BLUE;
      _entire_row.RowHeight = 13.50;

      ReleaseComObject(_entire_row);
      ReleaseComObject(_interior);
      ReleaseComObject(_font);
      ReleaseComObject(_range);
      ReleaseComObject(_cell1);
      ReleaseComObject(_cell2);

      // Format Cells

      _cell1 = (Excel.Range)_cells[_first_data_row, GAMES_DATE];
      _cell2 = (Excel.Range)_cells[_last_data_row, GAMES_DATE];
      _range = Sheet.get_Range(_cell1, _cell2);
      _entire_row = _range.EntireRow;

      _range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
      _range.NumberFormat = "dddd, dd/mm/yyyy";
      _entire_row.RowHeight = 14.25;
      _entire_row.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;

      ReleaseComObject(_entire_row);
      ReleaseComObject(_range);
      ReleaseComObject(_cell1);
      ReleaseComObject(_cell2);


      _cell1 = (Excel.Range)_cells[_first_data_row, GAMES_PLAYED_AMOUNT];
      _cell2 = (Excel.Range)_cells[_xls_row, GAMES_PLAYED_AMOUNT];
      _range = Sheet.get_Range(_cell1, _cell2);
      _range.NumberFormatLocal = m_currency_format;
      ReleaseComObject(_range);
      ReleaseComObject(_cell1);
      ReleaseComObject(_cell2);

      _cell1 = (Excel.Range)_cells[_first_data_row, GAMES_WON_AMOUNT];
      _cell2 = (Excel.Range)_cells[_xls_row, GAMES_WON_AMOUNT];
      _range = Sheet.get_Range(_cell1, _cell2);
      _range.NumberFormatLocal = m_currency_format;
      ReleaseComObject(_range);
      ReleaseComObject(_cell1);
      ReleaseComObject(_cell2);

      _cell1 = (Excel.Range)_cells[_first_data_row, GAMES_PAYOUT_PCT];
      _cell2 = (Excel.Range)_cells[_xls_row, GAMES_PAYOUT_PCT];
      _range = Sheet.get_Range(_cell1, _cell2);
      _range.NumberFormatLocal = m_percent_format;
      ReleaseComObject(_range);
      ReleaseComObject(_cell1);
      ReleaseComObject(_cell2);

      _cell1 = (Excel.Range)_cells[_first_data_row, GAMES_JACKPOT];
      _cell2 = (Excel.Range)_cells[_xls_row, GAMES_JACKPOT];
      _range = Sheet.get_Range(_cell1, _cell2);
      _range.NumberFormatLocal = m_currency_format;
      ReleaseComObject(_range);
      ReleaseComObject(_cell1);
      ReleaseComObject(_cell2);

      _cell1 = (Excel.Range)_cells[_first_data_row, GAMES_CONTRIBUTION_PCT];
      _cell2 = (Excel.Range)_cells[_xls_row, GAMES_CONTRIBUTION_PCT];
      _range = Sheet.get_Range(_cell1, _cell2);
      _range.NumberFormatLocal = m_percent_format;
      ReleaseComObject(_range);
      ReleaseComObject(_cell1);
      ReleaseComObject(_cell2);

      _cell1 = (Excel.Range)_cells[_first_data_row, GAMES_CONTRIBUTION_AMOUNT];
      _cell2 = (Excel.Range)_cells[_xls_row, GAMES_CONTRIBUTION_AMOUNT];
      _range = Sheet.get_Range(_cell1, _cell2);
      _range.NumberFormatLocal = m_currency_format;
      ReleaseComObject(_range);
      ReleaseComObject(_cell1);
      ReleaseComObject(_cell2);

      _cell1 = (Excel.Range)_cells[_first_data_row, GAMES_NETWIN];
      _cell2 = (Excel.Range)_cells[_xls_row, GAMES_NETWIN];
      _range = Sheet.get_Range(_cell1, _cell2);
      _range.NumberFormatLocal = m_currency_format;
      ReleaseComObject(_range);
      ReleaseComObject(_cell1);
      ReleaseComObject(_cell2);

      _cell1 = (Excel.Range)_cells[_first_data_row, GAMES_REAL_NETWIN];
      _cell2 = (Excel.Range)_cells[_xls_row, GAMES_REAL_NETWIN];
      _range = Sheet.get_Range(_cell1, _cell2);
      _range.NumberFormatLocal = m_currency_format;
      ReleaseComObject(_range);
      ReleaseComObject(_cell1);
      ReleaseComObject(_cell2);

      _cell1 = (Excel.Range)_cells[_first_data_row, GAMES_REAL_PAYOUT_PCT];
      _cell2 = (Excel.Range)_cells[_xls_row, GAMES_REAL_PAYOUT_PCT];
      _range = Sheet.get_Range(_cell1, _cell2);
      _range.NumberFormatLocal = m_percent_format;
      ReleaseComObject(_range);
      ReleaseComObject(_cell1);
      ReleaseComObject(_cell2);

      _cell1 = (Excel.Range)_cells[_first_data_row, GAMES_PLAYS_COUNT];
      _cell2 = (Excel.Range)_cells[_xls_row, GAMES_PLAYS_COUNT];
      _range = Sheet.get_Range(_cell1, _cell2);
      _range.NumberFormatLocal = m_number_format;
      ReleaseComObject(_range);
      ReleaseComObject(_cell1);
      ReleaseComObject(_cell2);

      _cell1 = (Excel.Range)_cells[_first_data_row, GAMES_WON_COUNT];
      _cell2 = (Excel.Range)_cells[_xls_row, GAMES_WON_COUNT];
      _range = Sheet.get_Range(_cell1, _cell2);
      _range.NumberFormatLocal = m_number_format;
      ReleaseComObject(_range);
      ReleaseComObject(_cell1);
      ReleaseComObject(_cell2);

      _cell1 = (Excel.Range)_cells[_first_data_row, GAMES_WON_PCT];
      _cell2 = (Excel.Range)_cells[_xls_row, GAMES_WON_PCT];
      _range = Sheet.get_Range(_cell1, _cell2);
      _range.NumberFormatLocal = m_percent_format;
      ReleaseComObject(_range);
      ReleaseComObject(_cell1);
      ReleaseComObject(_cell2);

      _cell1 = (Excel.Range)_cells[_first_data_row, GAMES_AVERAGE_PLAY];
      _cell2 = (Excel.Range)_cells[_xls_row, GAMES_AVERAGE_PLAY];
      _range = Sheet.get_Range(_cell1, _cell2);
      _range.NumberFormatLocal = m_currency_format;
      ReleaseComObject(_range);
      ReleaseComObject(_cell1);
      ReleaseComObject(_cell2);

      // Auto Fit

      _cell1 = (Excel.Range)_cells[GAMES_HEADER_ROW, GAMES_DATE];
      _cell2 = (Excel.Range)_cells[GAMES_HEADER_ROW, GAMES_LAST_COLUMN];
      _range = Sheet.get_Range(_cell1, _cell2);
      _entire_column = _range.EntireColumn;
      _entire_column.AutoFit();
      ReleaseComObject(_entire_column);
      ReleaseComObject(_range);
      ReleaseComObject(_cell1);
      ReleaseComObject(_cell2);

      if (m_draw_spaces)
      {
        for (_idx_col = GAMES_FIRST_COLUMN + 1; _idx_col < GAMES_LAST_COLUMN; _idx_col += 2)
        {
          _idx_row = _first_data_row - 1;
          if (_idx_col == GAMES_CONTRIBUTION_PCT + 1)
          {
            _idx_row = _first_data_row;
          }
          _cell1 = (Excel.Range)_cells[_idx_row, _idx_col];
          _cell2 = (Excel.Range)_cells[_last_data_row, _idx_col];
          _range = Sheet.get_Range(_cell1, _cell2);
          _interior = _range.Interior;
          _entire_column = _range.EntireColumn;

          _interior.ColorIndex = EXCEL_COLOR_INDEX_WHITE;
          _entire_column.ColumnWidth = 0.58;

          ReleaseComObject(_entire_column);
          ReleaseComObject(_interior);
          ReleaseComObject(_range);
          ReleaseComObject(_cell1);
          ReleaseComObject(_cell2);
        }

        Int32[] _cols_in_white;

        _cols_in_white = new Int32[] { GAMES_PLAYS_COUNT - 1,
                                       GAMES_AVERAGE_PLAY - 1 };

        foreach (Int32 _col_idx in _cols_in_white)
        {
          _cell1 = (Excel.Range)_cells[GAMES_HEADER_ROW, _col_idx];
          _cell2 = (Excel.Range)_cells[GAMES_HEADER_ROW + 2, _col_idx];
          _range = Sheet.get_Range(_cell1, _cell2);
          _interior = _range.Interior;
          _interior.ColorIndex = EXCEL_COLOR_INDEX_WHITE;
          ReleaseComObject(_interior);
          ReleaseComObject(_range);
          ReleaseComObject(_cell1);
          ReleaseComObject(_cell2);
        }
      }

      // Freeze first rows as they are the header.
      _range = (Excel.Range)_cells[_first_data_row, 1];
      _range.Activate();

      _app = _range.Application;
      _window = _app.ActiveWindow;
      _window.FreezePanes = true;

      ReleaseComObject(_window);
      ReleaseComObject(_range);
 

      ReleaseComObject(_cells);
    } // FillSheetGameStatistics

    //------------------------------------------------------------------------------
    // PURPOSE : Fill an Excel sheet with the connected VLT info.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Excel.Worksheet Sheet
    //          - DateTime FromDate
    //          - DateTime ToDate
    //          - DataRow[] ConnectedRows
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static void FillSheetGameTotalVLT(Excel.Worksheet Sheet,
                                              DateTime FromDate,
                                              DateTime ToDate,
                                              DataRow[] ConnectedRows)
    {
      Excel.Range _cells;
      Excel.Range _cell1;
      Excel.Range _cell2;
      Excel.Range _range;
      Excel.Range _entire_column;
      Excel.Font _font;
      Excel.Interior _interior;
      Int32 _xls_row;
      Int32 _first_data_row;
      Int32 _last_data_row;
      Int32 _idx_row;
      Int32 _idx_col;
      DateTime _current_date;
      DateTime _connected_date;

      _cells = Sheet.Cells;

      // Column Headers

      _xls_row = GAMES_HEADER_ROW + (m_draw_spaces ? 2 : 1);

      _range = (Excel.Range)_cells[_xls_row, GAMES_VLT];
      _range.Value2 = "VLT";
      ReleaseComObject(_range);

      _range = (Excel.Range)_cells[_xls_row, GAMES_NW_VLT];
      _range.Value2 = "NW/VLT";
      ReleaseComObject(_range);


      _cell1 = (Excel.Range)_cells[_xls_row, GAMES_VLT];
      _cell2 = (Excel.Range)_cells[_xls_row, GAMES_NW_VLT];
      _range = Sheet.get_Range(_cell1, _cell2);
      _font = _range.Font;
      _interior = _range.Interior;

      _range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
      _range.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
      _font.Bold = true;
      _font.ColorIndex = EXCEL_COLOR_INDEX_WHITE;
      _interior.ColorIndex = EXCEL_COLOR_INDEX_DARK_BLUE;

      ReleaseComObject(_interior);
      ReleaseComObject(_font);
      ReleaseComObject(_range);
      ReleaseComObject(_cell1);
      ReleaseComObject(_cell2);


      // Data Rows

      _xls_row++;
      _first_data_row = _xls_row;
      _current_date = FromDate;

      foreach (DataRow _connected in ConnectedRows)
      {
        if (_xls_row > m_excel_max_rows)
        {
          break;
        }

        _connected_date = (DateTime)_connected["Date"];

        while (_current_date < _connected_date)
        {
          _cell1 = (Excel.Range)_cells[_xls_row, GAMES_VLT];
          _cell2 = (Excel.Range)_cells[_xls_row, GAMES_NW_VLT];
          _range = Sheet.get_Range(_cell1, _cell2);

          _range.Value2 = 0;

          // RCI 01-AUG-2011
          if (_xls_row % 2 == (m_draw_spaces ? 1 : 0))
          {
            _interior = _range.Interior;
            _interior.ColorIndex = EXCEL_COLOR_INDEX_GRAY;
            ReleaseComObject(_interior);
          }

          ReleaseComObject(_range);
          ReleaseComObject(_cell1);
          ReleaseComObject(_cell2);

          _xls_row++;
          _current_date = _current_date.AddDays(1);
        }
        _current_date = _connected_date.AddDays(1);

        if (_xls_row % 2 == (m_draw_spaces ? 1 : 0))
        {
          _cell1 = (Excel.Range)_cells[_xls_row, GAMES_VLT];
          _cell2 = (Excel.Range)_cells[_xls_row, GAMES_NW_VLT];
          _range = Sheet.get_Range(_cell1, _cell2);
          _interior = _range.Interior;

          _interior.ColorIndex = EXCEL_COLOR_INDEX_GRAY;

          ReleaseComObject(_interior);
          ReleaseComObject(_range);
          ReleaseComObject(_cell1);
          ReleaseComObject(_cell2);
        }

        _range = (Excel.Range)_cells[_xls_row, GAMES_VLT];
        _range.Value2 = (Int32)_connected["NumTerminals"];
        ReleaseComObject(_range);

        _range = (Excel.Range)_cells[_xls_row, GAMES_NW_VLT];
        _range.Value2 = "=IF(" + ColName(GAMES_VLT) + _xls_row + "> 0, " + ColName(GAMES_NETWIN) + _xls_row + "/" + ColName(GAMES_VLT) + _xls_row + ", \"\")";
        ReleaseComObject(_range);

        _xls_row++;
      }

      while (_current_date < ToDate)
      {
        _cell1 = (Excel.Range)_cells[_xls_row, GAMES_VLT];
        _cell2 = (Excel.Range)_cells[_xls_row, GAMES_NW_VLT];
        _range = Sheet.get_Range(_cell1, _cell2);

        _range.Value2 = 0;

        // RCI 01-AUG-2011
        if (_xls_row % 2 == (m_draw_spaces ? 1 : 0))
        {
          _interior = _range.Interior;
          _interior.ColorIndex = EXCEL_COLOR_INDEX_GRAY;
          ReleaseComObject(_interior);
        }

        ReleaseComObject(_range);
        ReleaseComObject(_cell1);
        ReleaseComObject(_cell2);

        _xls_row++;
        _current_date = _current_date.AddDays(1);
      }

      _last_data_row = _xls_row - 1;

      // Totals

      _range = (Excel.Range)_cells[_xls_row, GAMES_VLT];
      _range.Value2 = "=SUM(" + ColName(GAMES_VLT) + _first_data_row + ":" + ColName(GAMES_VLT) + (_xls_row - 1) + ")";
      ReleaseComObject(_range);

      _range = (Excel.Range)_cells[_xls_row, GAMES_NW_VLT];
      _range.Value2 = "=IF(" + ColName(GAMES_VLT) + _xls_row + "> 0, " + ColName(GAMES_NETWIN) + _xls_row + "/" + ColName(GAMES_VLT) + _xls_row + ", \"\")";
      ReleaseComObject(_range);

      // Format Total Row
      _cell1 = (Excel.Range)_cells[_xls_row, GAMES_FIRST_COLUMN];
      _cell2 = (Excel.Range)_cells[_xls_row, GAMES_NW_VLT];
      _range = Sheet.get_Range(_cell1, _cell2);
      _font = _range.Font;
      _interior = _range.Interior;

      _range.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
      _font.ColorIndex = EXCEL_COLOR_INDEX_WHITE;
      _interior.ColorIndex = EXCEL_COLOR_INDEX_DARK_BLUE;

      ReleaseComObject(_interior);
      ReleaseComObject(_font);
      ReleaseComObject(_range);
      ReleaseComObject(_cell1);
      ReleaseComObject(_cell2);

      // Format Cells

      _cell1 = (Excel.Range)_cells[_first_data_row, GAMES_VLT];
      _cell2 = (Excel.Range)_cells[_xls_row, GAMES_VLT];
      _range = Sheet.get_Range(_cell1, _cell2);
      _range.NumberFormatLocal = m_number_format;
      ReleaseComObject(_range);
      ReleaseComObject(_cell1);
      ReleaseComObject(_cell2);

      _cell1 = (Excel.Range)_cells[_first_data_row, GAMES_NW_VLT];
      _cell2 = (Excel.Range)_cells[_xls_row, GAMES_NW_VLT];
      _range = Sheet.get_Range(_cell1, _cell2);
      _range.NumberFormatLocal = m_currency_format;
      ReleaseComObject(_range);
      ReleaseComObject(_cell1);
      ReleaseComObject(_cell2);

      // Auto Fit

      _cell1 = (Excel.Range)_cells[GAMES_HEADER_ROW, GAMES_VLT];
      _cell2 = (Excel.Range)_cells[GAMES_HEADER_ROW, GAMES_NW_VLT];
      _range = Sheet.get_Range(_cell1, _cell2);
      _entire_column = _range.EntireColumn;
      _entire_column.AutoFit();
      ReleaseComObject(_entire_column);
      ReleaseComObject(_range);
      ReleaseComObject(_cell1);
      ReleaseComObject(_cell2);

      if (m_draw_spaces)
      {
        for (_idx_col = GAMES_VLT - 1; _idx_col < GAMES_NW_VLT; _idx_col += 2)
        {
          _idx_row = _first_data_row - 1;
          _cell1 = (Excel.Range)_cells[_idx_row, _idx_col];
          _cell2 = (Excel.Range)_cells[_last_data_row, _idx_col];
          _range = Sheet.get_Range(_cell1, _cell2);
          _interior = _range.Interior;
          _entire_column = _range.EntireColumn;

          _interior.ColorIndex = EXCEL_COLOR_INDEX_WHITE;
          _entire_column.ColumnWidth = 0.58;

          ReleaseComObject(_entire_column);
          ReleaseComObject(_interior);
          ReleaseComObject(_range);
          ReleaseComObject(_cell1);
          ReleaseComObject(_cell2);
        }

        Int32[] _cols_in_white;

        _cols_in_white = new Int32[] { GAMES_VLT - 1,
                                       GAMES_NW_VLT - 1 };

        foreach (Int32 _col_idx in _cols_in_white)
        {
          _cell1 = (Excel.Range)_cells[GAMES_HEADER_ROW, _col_idx];
          _cell2 = (Excel.Range)_cells[GAMES_HEADER_ROW + 2, _col_idx];
          _range = Sheet.get_Range(_cell1, _cell2);
          _interior = _range.Interior;
          _interior.ColorIndex = EXCEL_COLOR_INDEX_WHITE;
          ReleaseComObject(_interior);
          ReleaseComObject(_range);
          ReleaseComObject(_cell1);
          ReleaseComObject(_cell2);
        }
      }

      ReleaseComObject(_cells);
    } // FillSheetGameTotalVLT

    //------------------------------------------------------------------------------
    // PURPOSE : Fill an Excel sheet with the terminals connected for the month.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Excel.Worksheet Sheet
    //          - DateTime MonthDate
    //          - Int32 LastDay
    //          - DataTable ConnectedTerms
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static void FillSheetConnectedTerminals(Excel.Worksheet Sheet,
                                                    DateTime MonthDate,
                                                    Int32 LastDay,
                                                    DataTable ConnectedTerms)
    {
      Excel.Application _app;
      Excel.Window _window;
      Excel.Range _cells;
      Excel.Range _cell1;
      Excel.Range _cell2;
      Excel.Range _range;
      Excel.Range _entire_column;
      Excel.Range _entire_row;
      Excel.Font _font;
      Excel.Interior _interior;
      Excel.Borders _borders;
      Excel.Border _border;
      Int32 _xls_row;
      Int32 _first_data_row;
      Int32 _last_data_row;
      Int32 _idx_col;
      Int32 _last_day_of_the_month;
      Int32 _idx_day;
      DateTime _day_date;
      String _last_provider;
      Boolean _first_time_in_row;

      _last_day_of_the_month = MonthDate.AddMonths(1).AddDays(-1).Day;

      TERMS_PROVIDER = 1;
      TERMS_TOTAL = 2;
      TERMS_DAY_START = 3;
      TERMS_DAY_END = TERMS_DAY_START + _last_day_of_the_month - 1;

      TERMS_LAST_COLUMN = TERMS_DAY_END;

      Sheet.Name = MonthDate.ToString("MMMM yyyy");

      _cells = Sheet.Cells;

      _font = _cells.Font;
      _font.Size = 9;
      _font.Name = "Calibri";
      ReleaseComObject(_font);

      _range = (Excel.Range)_cells[1, TERMS_PROVIDER];
      if (ConnectedTerms.Rows.Count > 0)
      {
        _range.Value2 = ConnectedTerms.Rows[0]["SiteName"];
      }
      else
      {
        _range.Value2 = "SALA DESCONOCIDA";
      }
      ReleaseComObject(_range);

      _range = (Excel.Range)_cells[2, TERMS_PROVIDER];
      _range.Value2 = MonthDate;
      _range.NumberFormat = "MMMM yyyy";
      ReleaseComObject(_range);

      // Column Headers

      _xls_row = TERMS_HEADER_ROW;

      _range = (Excel.Range)_cells[_xls_row, TERMS_PROVIDER];
      _entire_column = _range.EntireColumn;

      _range.Value2 = "Proveedor";
      _entire_column.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
      _entire_column.ColumnWidth = 40;

      ReleaseComObject(_entire_column);
      ReleaseComObject(_range);


      _range = (Excel.Range)_cells[_xls_row, TERMS_TOTAL];
      _entire_column = _range.EntireColumn;

      _range.Value2 = "Total";
      _entire_column.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
      _entire_column.ColumnWidth = 10;

      ReleaseComObject(_entire_column);
      ReleaseComObject(_range);


      // Make these alignments after "Proveedor" alignments.
      _cell1 = (Excel.Range)_cells[1, TERMS_PROVIDER];
      _cell2 = (Excel.Range)_cells[2, TERMS_PROVIDER];
      _range = Sheet.get_Range(_cell1, _cell2);
      _font = _range.Font;
      _entire_row = _range.EntireRow;

      _font.Size = 12;
      _font.Bold = true;
      _range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
      _range.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
      _entire_row.RowHeight = 15.75;

      ReleaseComObject(_entire_row);
      ReleaseComObject(_font);
      ReleaseComObject(_range);
      ReleaseComObject(_cell1);
      ReleaseComObject(_cell2);


      _day_date = MonthDate;

      for (_idx_day = 1; _idx_day <= _last_day_of_the_month; _idx_day++)
      {
        _range = (Excel.Range)_cells[_xls_row, TERMS_DAY_START + _idx_day - 1];
        _range.Value2 = _day_date;
        ReleaseComObject(_range);

        _day_date = _day_date.AddDays(1);
      }

      _cell1 = (Excel.Range)_cells[_xls_row, TERMS_PROVIDER];
      _cell2 = (Excel.Range)_cells[_xls_row, TERMS_TOTAL];
      _range = Sheet.get_Range(_cell1, _cell2);
      _font = _range.Font;
      _interior = _range.Interior;

      _font.Bold = true;
      _font.ColorIndex = EXCEL_COLOR_INDEX_WHITE;
      _interior.ColorIndex = EXCEL_COLOR_INDEX_DARK_BLUE;
      
      ReleaseComObject(_interior);
      ReleaseComObject(_font);
      ReleaseComObject(_range);
      ReleaseComObject(_cell1);
      ReleaseComObject(_cell2);


      _cell1 = (Excel.Range)_cells[_xls_row, TERMS_DAY_START];
      _cell2 = (Excel.Range)_cells[_xls_row, TERMS_DAY_END];
      _range = Sheet.get_Range(_cell1, _cell2);
      _font = _range.Font;
      _interior = _range.Interior;
      _entire_column = _range.EntireColumn;
      _entire_row = _range.EntireRow;

      _font.Bold = true;
      _font.ColorIndex = EXCEL_COLOR_INDEX_WHITE;
      _interior.Color = m_excel_color_light_blue;
      _range.NumberFormat = "dd mmm";
      _entire_column.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
      _entire_column.ColumnWidth = 6;
      _entire_row.RowHeight = 17.25;
      _entire_row.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;

      ReleaseComObject(_entire_row);
      ReleaseComObject(_entire_column);
      ReleaseComObject(_interior);
      ReleaseComObject(_font);
      ReleaseComObject(_range);
      ReleaseComObject(_cell1);
      ReleaseComObject(_cell2);


      _first_data_row = _xls_row + 1;

      if (ConnectedTerms.Rows.Count > 0)
      {
        // Data Rows

        _xls_row++;

        _last_provider = (String)ConnectedTerms.Rows[0]["Provider"];
        _first_time_in_row = true;

        foreach (DataRow _provider in ConnectedTerms.Rows)
        {
          // RCI & AJQ 25-JAN-2012: Compare Providers ignoring case, so "win" and "WIN" are considered the same provider.
          if (!_last_provider.Equals((String)_provider["Provider"], StringComparison.CurrentCultureIgnoreCase))
          {
            _xls_row++;
            if (_xls_row % 2 == 1)
            {
              _cell1 = (Excel.Range)_cells[_xls_row, 1];
              _cell2 = (Excel.Range)_cells[_xls_row, TERMS_DAY_END];
              _range = Sheet.get_Range(_cell1, _cell2);
              _interior = _range.Interior;

              _interior.ColorIndex = EXCEL_COLOR_INDEX_GRAY;

              ReleaseComObject(_interior);
              ReleaseComObject(_range);
              ReleaseComObject(_cell1);
              ReleaseComObject(_cell2);
            }
            _first_time_in_row = true;
          }

          if (_xls_row > m_excel_max_rows)
          {
            break;
          }

          if (_first_time_in_row)
          {
            _range = (Excel.Range)_cells[_xls_row, TERMS_PROVIDER];
            _range.Value2 = (String)_provider["Provider"];
            ReleaseComObject(_range);

            _range = (Excel.Range)_cells[_xls_row, TERMS_TOTAL];
            _range.Value2 = "=SUM(" + ColName(TERMS_DAY_START) + _xls_row + ":" + ColName(TERMS_DAY_END) + _xls_row + ")";
            _range.NumberFormatLocal = m_number_format;
            ReleaseComObject(_range);

            _cell1 = (Excel.Range)_cells[_xls_row, TERMS_DAY_START];
            _cell2 = (Excel.Range)_cells[_xls_row, TERMS_DAY_START + LastDay - 1];
            _range = Sheet.get_Range(_cell1, _cell2);

            _range.Value2 = 0;
            _range.NumberFormatLocal = m_number_format;

            ReleaseComObject(_range);
            ReleaseComObject(_cell1);
            ReleaseComObject(_cell2);

            _first_time_in_row = false;
          }

          _idx_day = TERMS_DAY_START + ((DateTime)_provider["Date"]).Day - 1;
          _range = (Excel.Range)_cells[_xls_row, _idx_day];
          _range.Value2 = (Int32)_provider["NumTerminals"];
          ReleaseComObject(_range);

          _last_provider = (String)_provider["Provider"];
        }
      }

      _last_data_row = _xls_row;

      if (_last_data_row >= _first_data_row)
      {
        _cell1 = (Excel.Range)_cells[_first_data_row, TERMS_PROVIDER];
        _cell2 = (Excel.Range)_cells[_last_data_row, TERMS_PROVIDER];
        _range = Sheet.get_Range(_cell1, _cell2);
        _entire_row = _range.EntireRow;

        _entire_row.RowHeight = 14.25;
        _entire_row.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;

        ReleaseComObject(_entire_row);
        ReleaseComObject(_range);
        ReleaseComObject(_cell1);
        ReleaseComObject(_cell2);
      }

      // Totals

      _xls_row++;

      _range = (Excel.Range)_cells[_xls_row, TERMS_PROVIDER];
      _range.Value2 = "TOTAL:";
      ReleaseComObject(_range);

      _cell1 = (Excel.Range)_cells[_xls_row, TERMS_DAY_START];
      _cell2 = (Excel.Range)_cells[_xls_row, TERMS_DAY_START + LastDay - 1];
      _range = Sheet.get_Range(_cell1, _cell2);

      _range.Value2 = 0;
      _range.NumberFormatLocal = m_number_format;

      ReleaseComObject(_range);
      ReleaseComObject(_cell1);
      ReleaseComObject(_cell2);

      if (_last_data_row >= _first_data_row)
      {
        _range = (Excel.Range)_cells[_xls_row, TERMS_TOTAL];
        _range.Value2 = "=SUM(" + ColName(TERMS_TOTAL) + _first_data_row + ":" + ColName(TERMS_TOTAL) + _last_data_row + ")";
        _range.NumberFormatLocal = m_number_format;
        ReleaseComObject(_range);

        for (_idx_day = 1; _idx_day <= LastDay; _idx_day++)
        {
          _idx_col = TERMS_DAY_START + _idx_day - 1;
          _range = (Excel.Range)_cells[_xls_row, _idx_col];
          _range.Value2 = "=SUM(" + ColName(_idx_col) + _first_data_row + ":" + ColName(_idx_col) + _last_data_row + ")";
          _range.NumberFormatLocal = m_number_format;
          ReleaseComObject(_range);
        }
      }

      _cell1 = (Excel.Range)_cells[_xls_row, TERMS_PROVIDER];
      _cell2 = (Excel.Range)_cells[_xls_row, TERMS_TOTAL];
      _range = Sheet.get_Range(_cell1, _cell2);
      _font = _range.Font;
      _interior = _range.Interior;

      _font.Bold = true;
      _font.ColorIndex = EXCEL_COLOR_INDEX_WHITE;
      _interior.ColorIndex = EXCEL_COLOR_INDEX_DARK_BLUE;

      ReleaseComObject(_interior);
      ReleaseComObject(_font);
      ReleaseComObject(_range);
      ReleaseComObject(_cell1);
      ReleaseComObject(_cell2);


      _cell1 = (Excel.Range)_cells[_xls_row, TERMS_DAY_START];
      _cell2 = (Excel.Range)_cells[_xls_row, TERMS_DAY_END];
      _range = Sheet.get_Range(_cell1, _cell2);
      _font = _range.Font;
      _interior = _range.Interior;
      _entire_row = _range.EntireRow;

      _font.Bold = true;
      _font.ColorIndex = EXCEL_COLOR_INDEX_WHITE;
      _interior.Color = m_excel_color_light_blue;
      _entire_row.RowHeight = 17.25;
      _entire_row.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;

      ReleaseComObject(_entire_row);
      ReleaseComObject(_interior);
      ReleaseComObject(_font);
      ReleaseComObject(_range);
      ReleaseComObject(_cell1);
      ReleaseComObject(_cell2);


      _cell1 = (Excel.Range)_cells[TERMS_HEADER_ROW, TERMS_PROVIDER];
      _cell2 = (Excel.Range)_cells[_xls_row, TERMS_TOTAL];
      _range = Sheet.get_Range(_cell1, _cell2);
      _borders = _range.Borders;

      _border = _borders[Excel.XlBordersIndex.xlEdgeTop];
      _border.LineStyle = Excel.XlLineStyle.xlContinuous;
      ReleaseComObject(_border);
      _border = _borders[Excel.XlBordersIndex.xlEdgeBottom];
      _border.LineStyle = Excel.XlLineStyle.xlContinuous;
      ReleaseComObject(_border);
      _border = _borders[Excel.XlBordersIndex.xlEdgeLeft];
      _border.LineStyle = Excel.XlLineStyle.xlContinuous;
      ReleaseComObject(_border);
      _border = _borders[Excel.XlBordersIndex.xlEdgeRight];
      _border.LineStyle = Excel.XlLineStyle.xlContinuous;
      ReleaseComObject(_border);

      ReleaseComObject(_borders);
      ReleaseComObject(_range);
      ReleaseComObject(_cell1);
      ReleaseComObject(_cell2);


      _cell1 = (Excel.Range)_cells[TERMS_HEADER_ROW, TERMS_DAY_START];
      _cell2 = (Excel.Range)_cells[_xls_row, TERMS_DAY_END];
      _range = Sheet.get_Range(_cell1, _cell2);
      _borders = _range.Borders;

      _border = _borders[Excel.XlBordersIndex.xlEdgeTop];
      _border.LineStyle = Excel.XlLineStyle.xlContinuous;
      ReleaseComObject(_border);
      _border = _borders[Excel.XlBordersIndex.xlEdgeBottom];
      _border.LineStyle = Excel.XlLineStyle.xlContinuous;
      ReleaseComObject(_border);
      _border = _borders[Excel.XlBordersIndex.xlEdgeLeft];
      _border.LineStyle = Excel.XlLineStyle.xlContinuous;
      ReleaseComObject(_border);
      _border = _borders[Excel.XlBordersIndex.xlEdgeRight];
      _border.LineStyle = Excel.XlLineStyle.xlContinuous;
      ReleaseComObject(_border);
      _border = _borders[Excel.XlBordersIndex.xlInsideVertical];
      _border.LineStyle = Excel.XlLineStyle.xlContinuous;
      ReleaseComObject(_border);

      ReleaseComObject(_borders);
      ReleaseComObject(_range);
      ReleaseComObject(_cell1);
      ReleaseComObject(_cell2);


      if (_last_data_row >= _first_data_row)
      {
        // Freeze first rows as they are the header.
        _range = (Excel.Range)_cells[_first_data_row, TERMS_DAY_START];
        _range.Activate();

        _app = _range.Application;
        _window = _app.ActiveWindow;
        _window.FreezePanes = true;

        ReleaseComObject(_window);
        ReleaseComObject(_range);
      }

      ReleaseComObject(_cells);

    } // FillSheetConnectedTerminals

    //------------------------------------------------------------------------------
    // PURPOSE : Calculate NumberFormat for numbers, currency and percentages.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static void CalculateNumberFormats()
    {
      NumberFormatInfo _nfi;

      _nfi = Thread.CurrentThread.CurrentCulture.NumberFormat;
      m_number_format = "#" + _nfi.NumberGroupSeparator + "##0";

      m_percent_format = "#" + _nfi.PercentGroupSeparator + "##0" + _nfi.PercentDecimalSeparator + "00";
      switch (_nfi.PercentPositivePattern)
      {
        case 0:
          m_percent_format = m_percent_format + " " + _nfi.PercentSymbol;
          break;
        case 1:
          m_percent_format = m_percent_format + _nfi.PercentSymbol;
          break;
        case 2:
          m_percent_format = _nfi.PercentSymbol + m_percent_format;
          break;
        case 3:
          m_percent_format = _nfi.PercentSymbol + " " + m_percent_format;
          break;
        default:
          break;
      }

      m_currency_format = "#" + _nfi.CurrencyGroupSeparator + "##0" + _nfi.CurrencyDecimalSeparator + "00";
      switch (_nfi.CurrencyPositivePattern)
      {
        case 0:
          m_currency_format = _nfi.CurrencySymbol + m_currency_format;
          break;
        case 1:
          m_currency_format = m_currency_format + _nfi.CurrencySymbol;
          break;
        case 2:
          m_currency_format = _nfi.CurrencySymbol + " " + m_currency_format;
          break;
        case 3:
          m_currency_format = m_currency_format + " " + _nfi.CurrencySymbol;
          break;
        default:
          break;
      }

      m_currency_simbol = _nfi.CurrencySymbol;
      m_currency_separator = _nfi.CurrencyGroupSeparator;
      m_percent_simbol = _nfi.PercentSymbol;
      m_number_separator = _nfi.NumberGroupSeparator;
      m_percent_separator = _nfi.PercentGroupSeparator;

    } // CalculateNumberFormats

    //------------------------------------------------------------------------------
    // PURPOSE : Put the rows (from IndexStartRow to IndexStartRow + m_excel_max_rows - 1) of the DataTable to a Excel Worksheet.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Excel.Worksheet Sheet
    //          - DataTable Table
    //          - Int32 IndexStartRow
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static void DataTableToSheet(Excel.Worksheet Sheet, DataTable Table, Int32 IndexStartRow)
    {
      Excel.Range _cells;
      Excel.Range _cell1;
      Excel.Range _cell2;
      Excel.Range _range;
      Excel.Range _entire_column;
      Excel.Font _font;
      Excel.Interior _interior;
      Int32 _xls_row;
      Int32 _xls_col;
      DataRow _row;
      Decimal _aux;
      String _sheet_name;
      Int32 _index;

      _cells = Sheet.Cells;

      _sheet_name = Table.TableName;
      if (IndexStartRow > 0)
      {
        _index = (IndexStartRow / m_excel_max_rows) + 1;
        _sheet_name += " (" + _index + ")";
      }
      Sheet.Name = _sheet_name;

      _xls_row = ROW_OFFSET;
      _xls_col = COL_OFFSET;

      foreach (DataColumn _col in Table.Columns)
      {
        _range = (Excel.Range)_cells[_xls_row, _xls_col];
        _range.Value2 = String.IsNullOrEmpty(_col.Caption) ? _col.ColumnName : _col.Caption;
        ReleaseComObject(_range);
        _xls_col++;
      }

      _range = (Excel.Range)_cells[ROW_OFFSET - 3, COL_OFFSET];
      _range.Value2 = _sheet_name;
      ReleaseComObject(_range);

      _cell1 = (Excel.Range)_cells[ROW_OFFSET - 3, COL_OFFSET];
      _cell2 = (Excel.Range)_cells[ROW_OFFSET - 2, _xls_col - 1];
      _range = Sheet.get_Range(_cell1, _cell2);

      _range.Merge(false);
      _range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
      _range.HorizontalAlignment = Excel.XlVAlign.xlVAlignCenter;

      _font = _range.Font;
      _font.Bold = true;
      _font.Size = 26;

      ReleaseComObject(_font);
      ReleaseComObject(_range);
      ReleaseComObject(_cell1);
      ReleaseComObject(_cell2);

      _xls_row++;

      for (Int32 _idx_row = IndexStartRow; _idx_row < IndexStartRow + m_excel_max_rows; _idx_row++)
      {
        if (_xls_row > m_excel_max_rows + ROW_OFFSET)
        {
          break;
        }
        if (_idx_row >= Table.Rows.Count)
        {
          break;
        }
        _row = Table.Rows[_idx_row];

        OnProgressBar(_idx_row, Table.Rows.Count);

        _xls_col = COL_OFFSET;
        foreach (Object _item in _row.ItemArray)
        {
          _range = (Excel.Range)_cells[_xls_row, _xls_col];

          //ECMA-334
          switch (_item.GetType().ToString())
          {
            case "System.Decimal":
              _range.Value2 = _item;
              _range.NumberFormatLocal = m_currency_format;
              break;

            case "System.DateTime":
              _range.Value2 = _item;
              _range.NumberFormat = "dd/mm/yyyy hh:MM:ss";
              break;

            case "System.String":
              if (IsCardNumber((String)_item))
              {
                _range.NumberFormat = "@";
                _range.Value2 = _item;
              }
              else if (IsNumeric((String)_item, out _aux))
              {
                _range.NumberFormatLocal = m_number_format;
                _range.Value2 = _aux;
              }
              else if (PercentToDouble((String)_item, out _aux))
	            {
                _range.NumberFormat = m_percent_format;
                _range.Value2 = _aux;            		 
	            }
              else if (CurrencyToDouble((String)_item, out _aux))
              {
                _range.NumberFormatLocal = m_currency_format;
                _range.Value2 = _aux;
              }
              else
              {
                _range.NumberFormat = "@";
                _range.Value2 = _item;
              }
              break;

            case "System.Integer":
              _range.NumberFormat = m_number_format;
              _range.Value2 = _item;
              break;

            default:
              _range.Value2 = _item;
              break;
          }

          ReleaseComObject(_range);

          _xls_col++;
        } // foreach _item

        _xls_row++;
      } // for _idx_row

      _cell1 = (Excel.Range)_cells[ROW_OFFSET, COL_OFFSET];
      _cell2 = (Excel.Range)_cells[ROW_OFFSET, Table.Columns.Count + COL_OFFSET - 1];
      _range = Sheet.get_Range(_cell1, _cell2);
      _font = _range.Font;
      _interior = _range.Interior;
      _entire_column = _range.EntireColumn;

      _font.Bold = true;
      _interior.ColorIndex = EXCEL_COLOR_INDEX_GRAY;
      _interior.Pattern = 1;     // xlSolid
      _interior.PatternColorIndex = -4105;  // xlAutomatic
      _range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
      _entire_column.AutoFit();

      ReleaseComObject(_entire_column);
      ReleaseComObject(_interior);
      ReleaseComObject(_font);
      ReleaseComObject(_range);
      ReleaseComObject(_cell1);
      ReleaseComObject(_cell2);

      ReleaseComObject(_cells);

    } // DataTableToSheet

    //------------------------------------------------------------------------------
    // PURPOSE : Add an empty row in the Excel.Worksheet when there is no data for the specific CurrentDate.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Excel.Worksheet Sheet
    //          - Int32 XlsRow
    //          - DateTime CurrentDate
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static void SetEmptyRow(Excel.Worksheet Sheet,
                                    Int32 XlsRow,
                                    DateTime CurrentDate)
    {
      Excel.Range _cells;
      Excel.Range _cell1;
      Excel.Range _cell2;
      Excel.Range _range;
      Excel.Interior _interior;

      _cells = Sheet.Cells;

      if (XlsRow % 2 == (m_draw_spaces ? 1 : 0))
      {
        _cell1 = (Excel.Range)_cells[XlsRow, GAMES_FIRST_COLUMN];
        _cell2 = (Excel.Range)_cells[XlsRow, GAMES_LAST_COLUMN];
        _range = Sheet.get_Range(_cell1, _cell2);
        _interior = _range.Interior;

        _interior.ColorIndex = EXCEL_COLOR_INDEX_GRAY;

        ReleaseComObject(_interior);
        ReleaseComObject(_range);
        ReleaseComObject(_cell1);
        ReleaseComObject(_cell2);
      }

      _range = (Excel.Range)_cells[XlsRow, GAMES_DATE];
      _range.Value2 = CurrentDate;
      ReleaseComObject(_range);


      _cell1 = (Excel.Range)_cells[XlsRow, GAMES_PLAYED_AMOUNT];
      _cell2 = (Excel.Range)_cells[XlsRow, GAMES_LAST_COLUMN];
      _range = Sheet.get_Range(_cell1, _cell2);

      _range.Value2 = 0;

      ReleaseComObject(_range);
      ReleaseComObject(_cell1);
      ReleaseComObject(_cell2);


      _range = (Excel.Range)_cells[XlsRow, GAMES_PAYOUT_PCT];
      _range.Value2 = "";
      ReleaseComObject(_range);

      _range = (Excel.Range)_cells[XlsRow, GAMES_REAL_PAYOUT_PCT];
      _range.Value2 = "";
      ReleaseComObject(_range);
      
      _range = (Excel.Range)_cells[XlsRow, GAMES_WON_PCT];
      _range.Value2 = "";
      ReleaseComObject(_range);
      
      _range = (Excel.Range)_cells[XlsRow, GAMES_AVERAGE_PLAY];
      _range.Value2 = "";
      ReleaseComObject(_range);

      ReleaseComObject(_cells);
    } // SetEmptyRow

    //------------------------------------------------------------------------------
    // PURPOSE : Calculate the extension and the maximum rows allowed according to the Excel version.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Excel.Application Excel
    //          - String Filename
    //
    //      - OUTPUT :
    //          - String Filename
    //
    // RETURNS :
    //
    private static void SetFileExtensionAndMaxRows(Excel.Application Excel, ref String Filename)
    {
      String _ext;
      String _new_ext;

      if (Excel.Version.CompareTo("12") >= 0)
      {
        _new_ext = ".xlsx";
        m_excel_max_rows = EXCEL_MAX_ROWS_AFTER_2007;
      }
      else
      {
        _new_ext = ".xls";
        m_excel_max_rows = EXCEL_MAX_ROWS_BEFORE_2007;
      }

      // Add the extension to the Filename if not defined.
      _ext = Path.GetExtension(Filename);
      if (_ext != ".xls" && _ext != ".xlsx")
      {
        Filename = Filename + _new_ext;
      }
    } // SetFileExtensionAndMaxRows

    //------------------------------------------------------------------------------
    // PURPOSE : Set Excel column indices based on if column spaces is configured.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static void SetExcelColumnIndices()
    {
      if (m_draw_spaces)
      {
        GAMES_DATE = 1;
        GAMES_PLAYED_AMOUNT = 3;
        GAMES_WON_AMOUNT = 5;
        GAMES_PAYOUT_PCT = 7;
        GAMES_JACKPOT = 9;
        GAMES_CONTRIBUTION_PCT = 11;
        GAMES_CONTRIBUTION_AMOUNT = 13;
        GAMES_NETWIN = 15;
        GAMES_REAL_NETWIN = 17;
        GAMES_REAL_PAYOUT_PCT = 19;
        GAMES_PLAYS_COUNT = 21;
        GAMES_WON_COUNT = 23;
        GAMES_WON_PCT = 25;
        GAMES_AVERAGE_PLAY = 27;

        GAMES_FIRST_COLUMN = 1;
        GAMES_LAST_COLUMN = 27;

        // Don't include VLT indexes values inside LAST_COLUMN.
        GAMES_VLT = 29;
        GAMES_NW_VLT = 31;
      }
      else
      {
        GAMES_DATE = 1;
        GAMES_PLAYED_AMOUNT = 2;
        GAMES_WON_AMOUNT = 3;
        GAMES_PAYOUT_PCT = 4;
        GAMES_JACKPOT = 5;
        GAMES_CONTRIBUTION_PCT = 6;
        GAMES_CONTRIBUTION_AMOUNT = 7;
        GAMES_NETWIN = 8;
        GAMES_REAL_NETWIN = 9;
        GAMES_REAL_PAYOUT_PCT = 10;
        GAMES_PLAYS_COUNT = 11;
        GAMES_WON_COUNT = 12;
        GAMES_WON_PCT = 13;
        GAMES_AVERAGE_PLAY = 14;

        GAMES_FIRST_COLUMN = 1;
        GAMES_LAST_COLUMN = 14;

        // Don't include VLT indexes values inside LAST_COLUMN.
        GAMES_VLT = 15;
        GAMES_NW_VLT = 16;
      }
    } // SetExcelColumnIndices


    //------------------------------------------------------------------------------
    // PURPOSE : Returns Excel column name from numeric position.
    //           e.g.: col_no 27 returns "AA"
    //
    //  PARAMS :
    //      - INPUT :
    //          - Int32 ColNum
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String
    //
    private static string ColName(int ColumnNumber)
    {
      int _dividend;
      string _column_name;
      int _modulo;

      _column_name = "";
      _dividend = ColumnNumber;

      while (_dividend > 0)
      {
        _modulo = (_dividend - 1) % 26;
        _column_name = Convert.ToChar(65 + _modulo).ToString() + _column_name;
        _dividend = (int)((_dividend - _modulo) / 26);
      }

      return _column_name;
    } // ColName

    //------------------------------------------------------------------------------
    // PURPOSE : Release a COM object.
    //
    //  PARAMS :
    //      - INPUT :
    //          - object Obj
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static void ReleaseComObject(object Obj)
    {
      Int32 _num_ref;
      try
      {
        if (Obj != null)
        {
          do
          {
            _num_ref = System.Runtime.InteropServices.Marshal.ReleaseComObject(Obj);
          } while (_num_ref > 0);
        }
        Obj = null;
      }
      catch
      {
        Obj = null;
      }
    } // ReleaseComObject

    private static Boolean IsNumeric(String Expression)
    {
      Decimal _aux;

      return Decimal.TryParse(Expression, out _aux);
    }

    private static Boolean IsNumeric(String Expression, out Decimal Number)
    {
      return Decimal.TryParse(Expression, out Number);
    }

    private static Boolean IsCardNumber(String Expression)
    {

      if (Expression.Length != 20)
        return false;
      
      Regex _regexp;
      _regexp = new Regex("^[0-9]+$");
      if (_regexp.IsMatch(Expression)) 
      {
        return true;
      }
      else
      {
        return false;
      }
   
    }

    private static Boolean PercentToDouble(String Expression, out Decimal Percent)
    {
      String _aux;

      _aux = Expression;
      Percent = 0;

      if (!_aux.ToString().Contains(m_percent_simbol))
        return false;

      _aux = _aux.Replace(m_percent_simbol, "");
      _aux = _aux.Replace(m_percent_separator, "");

      if (IsNumeric(_aux))
      {
        Percent = Decimal.Parse(_aux);
        return true;
      }
      else
      {
        return false;
      }      

    }

    private static Boolean CurrencyToDouble(String Expression, out Decimal Currency)
    {
      String _aux;

      _aux = Expression;
      Currency = 0;

      if (!_aux.ToString().Contains(m_currency_simbol))
        return false;

      _aux = _aux.Replace(m_currency_simbol, "");
      _aux = _aux.Replace(m_currency_separator, "");

      if (IsNumeric(_aux))
      {
        Currency = Decimal.Parse(_aux);
        return true;
      }
      else
      {
        return false;
      }

    }

    public static Boolean DataTableToCSV(DataTable Table, String Filename)
    {
      StringBuilder _sb;
      String _value;
      String _separator;
      String _text_delimeter_0;
      String _text_delimeter_1;
      String _text_delimeter_2;

      _separator = "\t";
      _text_delimeter_0 = "";
      _text_delimeter_1 = "";
      _text_delimeter_2 = "'";

      if (Table == null)
      {
        return false;
      }
      if (Table.Columns.Count == 0)
      {
        return true;
      }

      try
      { 
        _sb = new StringBuilder(Table.Columns.Count * 20);

        using (FileStream _fs = new FileStream(Filename, FileMode.Create, FileAccess.Write, FileShare.Read))
        {
          using (TextWriter _tw = new StreamWriter(_fs, Encoding.Unicode))
          {
            foreach (DataColumn _col in Table.Columns)
            {
              if (_sb.Length > 0)
              {
                _sb.Append(_separator);
              }
              _sb.Append(_text_delimeter_0 + _col.Caption + _text_delimeter_1);
            }
            _tw.WriteLine(_sb.ToString());

            foreach (DataRow _row in Table.Rows)
            {
              _sb.Length = 0;
              for (int _idx_col = 0; _idx_col < _row.Table.Columns.Count; _idx_col++)
              {
                if (_idx_col > 0)
                {
                  _sb.Append(_separator);
                }

                _value = _row.IsNull(_idx_col) ? "" : _row[_idx_col].ToString();

                if (IsCardNumber(_value))
                {
                  _sb.Append(_text_delimeter_0 + _text_delimeter_2 + _value + _text_delimeter_1);
                }
                else
                {
                  _sb.Append(_text_delimeter_0 + _value + _text_delimeter_1);
                }
              }
              _tw.WriteLine(_sb.ToString());
            }

            _tw.Close();
          }

          _fs.Close();
        }
        return true;
      }
      catch 
      {
        return false;
      }
    }

    public static Boolean DataSetToCSV(DataSet Set, String Filename)
    {
      String _filename;
      String _extension;
      String _path;
      Int32  _id;
      String _base;

      if (Set == null)
      {
        return false;
      }
      if (Set.Tables.Count == 0)
      {
        return true;
      }

      _id = 1;

      _path = System.IO.Path.GetFullPath(Filename);
      _filename = System.IO.Path.GetFileNameWithoutExtension(Filename);

      _extension = System.IO.Path.GetExtension(Filename);
      if ( String.IsNullOrEmpty(_extension) )
      {
        _base = Filename;
      }
      else
      {
        _base = Filename.Substring(0, Filename.LastIndexOf (_extension));
      }

      foreach (DataTable _table in Set.Tables)
      { 
        _filename = _base; 
        if ( _id > 1 )
        {
          _filename += "_" + _id.ToString();  
        }
        _filename += _extension;

        if (!DataTableToCSV(_table, _filename))
        {
          return false;
        }
      }

      return true;
    } // DataSetToCSV



  } // ExcelConversion

} // WSI.Common
