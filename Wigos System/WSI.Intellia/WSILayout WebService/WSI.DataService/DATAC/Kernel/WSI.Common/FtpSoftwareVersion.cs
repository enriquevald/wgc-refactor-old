//------------------------------------------------------------------------------
// Copyright � 2008 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: FtpSoftwareVersion.cs
// 
//   DESCRIPTION: Check and Get FTP software versions
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 29-OCT-2008
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 29-OCT-2008 ACC    First release.
//------------------------------------------------------------------------------

using System;
using System.Threading;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Runtime.InteropServices;

namespace WSI.Common
{
  public static class FtpVersions
  {
    #region Structures

    //
    // Structure
    //
    [StructLayout(LayoutKind.Sequential)]
    private struct TYPE_FTP_SOFT_PARAMS
    {
      public int m_control_block;
      public int m_client_id;
      public int m_build;
      public int m_terminal_type;
      [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 260)]
      public String m_repository1;
      [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 260)]
      public String m_repository2;
    }

    #endregion

    #region Prototypes

    //
    // Functions Prototypes
    //
    [DllImport("CommonBase")]  //TARGET_64_BITS JCALERO
    static extern int Common_ProcessFtpSoft(ref TYPE_FTP_SOFT_PARAMS pFtpParameters); //TARGET_64_BITS JCALERO

    #endregion

    #region Events

    // For notify new version is available.
    public class NewVersionEvent
    {
      public String version;
      public Int32 notification_period;
    }

    public delegate void NewVersionAvailableDelegate(NewVersionEvent VersionEvent);
    public static event NewVersionAvailableDelegate OnNewVersionAvailable;

    #endregion

    #region Members

    static Thread m_thread_ftp;
    private static ENUM_TSV_PACKET m_packet_type = ENUM_TSV_PACKET.TSV_PACKET_NOT_SET;
    private static Int32 m_current_client;
    private static Int32 m_current_build;

    private static String m_repository1;
    private static String m_repository2;

    private static Int32 m_new_client;
    private static Int32 m_new_build;
    private static String m_new_version;

    #endregion

    #region Init

    //------------------------------------------------------------------------------
    // PURPOSE : Init mechanism for new software versions
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalType
    //          - CurrentClient
    //          - CurrentBuild
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public static void Init(ENUM_TSV_PACKET PacketType, Int32 CurrentClient, Int32 CurrentBuild)
    {
      m_repository1 = "";
      m_repository2 = "";
      m_new_client = 0;
      m_new_build = 0;
      m_new_version = "";

      m_packet_type = PacketType;
      m_current_client = CurrentClient;
      m_current_build = CurrentBuild;

      // Create thread
      m_thread_ftp = new Thread(FtpVersionsThread);
      m_thread_ftp.Name = "FtpVersions";

      // Start thread
      m_thread_ftp.Start();

    } // Init

    #endregion

    #region Thread

    //------------------------------------------------------------------------------
    // PURPOSE : Thread for Check and get new software versions
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    static private void FtpVersionsThread()
    {
      Random _random;
      int _wait_hint;
      NewVersionEvent _version_event;

      _random = new Random(Misc.GetTickCount());

      while (true)
      {
        if (NewVersionIsAvailable())
        {
          if (GetVersionFromFtp())
          {
            // New version downloaded
            break;
          }
        }
        Thread.Sleep(_random.Next(1 * 60 * 1000, 2 * 60 * 1000));
      }

      while (true)
      {
        _wait_hint = 5 * 60 * 1000; // 5 minutes
        if (OnNewVersionAvailable != null)
        {
          _version_event = new NewVersionEvent();
          _version_event.version = m_new_version;
          _version_event.notification_period = _wait_hint;

          OnNewVersionAvailable(_version_event);

          m_new_version = _version_event.version;
          _wait_hint = _version_event.notification_period;
        }
        if (_wait_hint == 0)
        {
          break;
        }
        _wait_hint = Math.Max(1 * 60 * 1000, _wait_hint);
        System.Threading.Thread.Sleep(_wait_hint);
      }

    } // FtpVersionsThread

    #endregion

    #region Private Functions

    //------------------------------------------------------------------------------
    // PURPOSE : Check if new version is available
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //      - TRUE: New version is available
    //      - FALSE: No new software available
    //
    static private Boolean NewVersionIsAvailable()
    {
      String _str_sql;
      Int16 _last_client_id;
      Int16 _last_build_id;

      // Init variables
      _last_client_id = 0;
      _last_build_id = 0;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          // Read last inserted version
          _str_sql = "";
          _str_sql += "  SELECT   TOP 1 TSV_CLIENT_ID, TSV_BUILD_ID ";
          _str_sql += "    FROM   TERMINAL_SOFTWARE_VERSIONS ";
          _str_sql += "   WHERE   TSV_TERMINAL_TYPE = @pTerminalType ";
          _str_sql += "ORDER BY   TSV_INSERTION_DATE DESC";

          using (SqlCommand _sql_cmd = new SqlCommand(_str_sql))
          {
            _sql_cmd.Parameters.Add("@pTerminalType", SqlDbType.Int).Value = m_packet_type;

            using (SqlDataReader _sql_reader = _db_trx.ExecuteReader(_sql_cmd))
            {
              if (_sql_reader == null)
              {
                return false;
              }

              if (_sql_reader.Read())
              {
                _last_client_id = _sql_reader.GetInt16(0);
                _last_build_id = _sql_reader.GetInt16(1);
              }
              _sql_reader.Close();
            }
          }
        }

        if (_last_client_id == 0 && _last_build_id == 0)
        {
          // Last version not read ...
          return false;
        }

        if (_last_client_id == m_current_client && _last_build_id == m_current_build)
        {
          // Same version
          return false;
        }

        // New version!
        m_new_client = _last_client_id;
        m_new_build = _last_build_id;
        m_new_version = m_new_client.ToString("00") + "." + m_new_build.ToString("000");
        m_repository1 = Common.Misc.ReadGeneralParams("Software.Download", "Location1");
        m_repository2 = Common.Misc.ReadGeneralParams("Software.Download", "Location2");

        if (String.IsNullOrEmpty(m_repository1))
        {
          // 1st repository is empty/null
          if (String.IsNullOrEmpty(m_repository2))
          {
            // 2nd repository is also empty/null -> New version can't be downloaded!
            return false;
          }

          // Replace 1st repository by the 2nd
          m_repository1 = m_repository2;
          m_repository2 = "";
        }

        return true;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      return false;

    } // NewVersionIsAvailable

    //------------------------------------------------------------------------------
    // PURPOSE : Get new version from FTP
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //      - TRUE: New version was copied successfully
    //      - FALSE: Error copying new version ...
    //
    static private Boolean GetVersionFromFtp()
    {

      //TARGET_64_BITS JCALERO
      
      TYPE_FTP_SOFT_PARAMS ftp_parameters;

      ftp_parameters = new TYPE_FTP_SOFT_PARAMS();

      ftp_parameters.m_control_block = Marshal.SizeOf(typeof(TYPE_FTP_SOFT_PARAMS));
      ftp_parameters.m_client_id = m_new_client;
      ftp_parameters.m_build = m_new_build;
      ftp_parameters.m_terminal_type = (int)m_packet_type;
      ftp_parameters.m_repository1 = m_repository1;
      ftp_parameters.m_repository2 = m_repository2;

      if (Common_ProcessFtpSoft(ref ftp_parameters) != 0)
      {
        return false;
      }

      return true;
       
    } // GetVersionFromFtp

    #endregion

  }
}
