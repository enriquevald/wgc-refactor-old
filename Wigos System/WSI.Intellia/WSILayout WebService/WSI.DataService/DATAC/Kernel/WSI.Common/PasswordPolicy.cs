using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Security.Cryptography;

namespace WSI.Common
{
  public class PasswordPolicy
  {
    #region Private Constants

    public const int MIN_PASSWORD_LENGHT_CONST = 4;
    public const int MAX_PASSWORD_LENGTH_CONST = 15;
    public const int MIN_LOGIN_LENGTH_CONST = 4;
    public const int MAX_LOGIN_LENGTH_CONST = 15;
    public const int MIN_PASSWORD_DIGITS_CONST = 0;
    public const int MAX_PASSWORD_DIGITS_CONST = 15;
    public const int MIN_PASSWORD_LOWERCASE_CONST = 0;
    public const int MAX_PASSWORD_LOWERCASE_CONST = 15;
    public const int MIN_PASSWORD_UPPERCASE_CONST = 0;
    public const int MAX_PASSWORD_UPPERCASE_CONST = 15;
    public const int MIN_PASSWORD_SPECIALCHARACTER_CONST = 0;
    public const int MAX_PASSWORD_SPECIALCHARACTER_CONST = 15;
    public const int MIN_LOGIN_ATTEMPTS_CONST = 1;
    public const int MAX_LOGIN_ATTEMPTS_CONST = Int32.MaxValue;
    public const int MIN_PASSWORD_HISTORY_CONST = 0;
    public const int MAX_PASSWOD_HISTORY_CONST = 5;
    public const int MIN_CHANGE_PASS_N_DAYS = 0;
    public const int MAX_CHANGE_PASS_N_DAYS = Int32.MaxValue;

    private const int DEFAULT_PASSWOD_LENGTH = 6;
    private const int DEFAULT_LOGIN_LENGTH = 6;
    private const int DEFAULT_COUNT_OF_DIGITS = 2;
    private const int DEFAULT_COUNT_OF_LOWERCASE = 2;
    private const int DEFAULT_COUNT_OF_UPPERCASE = 2;
    private const int DEFAULT_COUNT_OF_SPECIAL_CHARACTER = 0;
    private const int DEFAULT_COUNT_OF_LOGIN_ATTEMPT = 3;
    private const int DEFAULT_COUNT_OF_PASSWORD_HISTORY = 5;
    private const int DEFAULT_COUNT_OF_DAYS_WITHOUT_LOGIN = 24;
    private const int DEFAULT_CHANGE_PASSWORD_IN_N_DAYS = 0;
    private const int DEFAULT_LOGIN_ATTEMPTS = 3;

    #endregion

    #region Private Values

    private Boolean m_rules_enabled;
    private Int32 m_min_password_length;
    private Int32 m_min_digit;
    private Int32 m_min_lower_case;
    private Int32 m_min_upper_case;
    private Int32 m_min_special_case;
    private Int32 m_max_password_history;
    private Int32 m_max_login_attempts;
    private Int32 m_min_login_length;
    private Int32 m_max_days_without_login;
    private Int32 m_change_in_n_days;

    #endregion

    #region Public Geters

    public Int32 MaxLoginAttempts
    {
      get { return m_max_login_attempts; }
    }
    public Boolean RulesEnabled
    {
      get { return m_rules_enabled; }
    }
    public Int32 MinPasswordLength
    {
      get { return m_min_password_length; }
    }
    public Int32 MinPasswordDigits
    {
      get { return m_min_digit; }
    }
    public Int32 MinPasswordLowerCase
    {
      get { return m_min_lower_case; }
    }
    public Int32 MinPasswordUpperCase
    {
      get { return m_min_upper_case; }
    }
    public Int32 MinPasswordSpecialCase
    {
      get { return m_min_special_case; }
    }
    public Int32 MaxPasswordHistory
    {
      get { return m_max_password_history; }
    }
    public Int32 MinLoginLength
    {
      get { return m_min_login_length; }
    }
    public Int32 ChangeInNDays
    {
      get { return m_change_in_n_days; }
    }
    public Int32 MaxDaysWithoutLogin
    {
      get { return m_max_days_without_login; }
    }

    #endregion

    public PasswordPolicy()
    {
      Int32 _gp_value;

      //Set default Values
      m_rules_enabled = false;
      m_max_login_attempts = DEFAULT_LOGIN_ATTEMPTS;
      m_min_password_length = DEFAULT_PASSWOD_LENGTH;
      m_min_digit = DEFAULT_COUNT_OF_DIGITS;
      m_min_lower_case = DEFAULT_COUNT_OF_LOWERCASE;
      m_min_upper_case = DEFAULT_COUNT_OF_UPPERCASE;
      m_min_special_case = DEFAULT_COUNT_OF_SPECIAL_CHARACTER;
      m_max_password_history = DEFAULT_COUNT_OF_PASSWORD_HISTORY;
      m_min_login_length = DEFAULT_LOGIN_LENGTH;
      m_change_in_n_days = DEFAULT_CHANGE_PASSWORD_IN_N_DAYS;
      m_max_days_without_login = DEFAULT_COUNT_OF_DAYS_WITHOUT_LOGIN;

      //Check PWD Policy Enabled
      if (int.TryParse(WSI.Common.GeneralParam.Value("User", "Password.RulesEnabled"), out _gp_value))
      {
        m_rules_enabled = (_gp_value == 1);
      }

      // MaxLoginAttempts is used always.
      if (int.TryParse(WSI.Common.GeneralParam.Value("User", "MaxLoginAttempts"), out _gp_value))
      {
        m_max_login_attempts = _gp_value;
      }

      if (m_rules_enabled)
      {
        if (int.TryParse(GeneralParam.Value("User", "Password.MinLength"), out _gp_value))
        {
          m_min_password_length = _gp_value;
        }
        if (int.TryParse(GeneralParam.Value("User", "Password.MinDigits"), out _gp_value))
        {
          m_min_digit = _gp_value;
        }
        if (int.TryParse(GeneralParam.Value("User", "Password.MinLowerCase"), out _gp_value))
        {
          m_min_lower_case = _gp_value;
        }
        if (int.TryParse(GeneralParam.Value("User", "Password.MinUpperCase"), out _gp_value))
        {
          m_min_upper_case = _gp_value;
        }
        if (int.TryParse(GeneralParam.Value("User", "Password.MinSpecialCase"), out _gp_value))
        {
          m_min_special_case = _gp_value;
        }
        if (int.TryParse(GeneralParam.Value("User", "Password.MaxPasswordHistory"), out _gp_value))
        {
          m_max_password_history = _gp_value;
        }
        if (int.TryParse(GeneralParam.Value("User", "Login.MinLength"), out _gp_value))
        {
          m_min_login_length = _gp_value;
        }
        if (int.TryParse(GeneralParam.Value("User", "Password.ChangeInNDays"), out _gp_value))
        {
          m_change_in_n_days = _gp_value;
        }
        if (int.TryParse(GeneralParam.Value("User", "MaxDaysWithoutLogin"), out _gp_value))
        {
          m_max_days_without_login = _gp_value;
        }

      } // m_rules_enabled = true

    } // PasswordPolicy

    // PURPOSE: Check the password rules.
    //          The password don't should break the password policy rules.
    //
    //  PARAMS:
    //     - INPUT:
    //           - MachineName: string;
    //           - UserName:    string;
    //           - Password:    string;
    //
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - bool: if check is ok then return "true" else "false".
    //
    public Boolean CheckPasswordRules(String MachineName,
                                      String UserName,
                                      Int32 UserId,
                                      String Password)
    {
      int _num_upper = 0;
      int _num_lower = 0;
      int _num_digit = 0;
      int _num_special = 0;

      // Check WhiteSpace at Begin or End of password
      if (Password.Trim() != Password)
      {
        return false;
      }

      // UserId = -1, it's a new user. Don't check the recently used passwords.
      if (UserId >= 0)
      {
        // Check password history
        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (PasswordRecentlyUsed(UserId, Password, _db_trx.SqlTransaction))
          {
            return false;
          }

          _db_trx.Commit();
        }
      }

      // All Rules When Password Policy Disabled are checked.
      if (!RulesEnabled)
      {
        return true;
      }

      // Check Length
      if (Password.Length < MinPasswordLength)
      {
        return false;
      }

      // Should not contain the UserName
      if (Password.ToUpper().Contains(UserName.ToUpper()))
      {
        return false;
      }
      // Should not contain the MachineName
      if (Password.ToUpper().Contains(MachineName.ToUpper()))
      {
        return false;
      }

      // Count char types
      _num_upper = 0;
      _num_lower = 0;
      _num_digit = 0;
      _num_special = 0;
      foreach (char _char in Password.ToCharArray())
      {
        if (char.IsLetterOrDigit(_char))
        {
          if (char.IsDigit(_char))
          {
            _num_digit += 1;
          }
          else if (char.IsUpper(_char))
          {
            _num_upper += 1;
          }
          else
          {
            _num_lower += 1;
          }
        }
        else
        {
          _num_special += 1;
        }
      }

      // Check char types rules
      if (_num_upper < MinPasswordUpperCase)
      {
        return false;
      }
      if (_num_lower < MinPasswordLowerCase)
      {
        return false;
      }
      if (_num_digit < MinPasswordDigits)
      {
        return false;
      }
      if (_num_special < MinPasswordSpecialCase)
      {
        return false;
      }

      // Valid password
      return true;
    } // CheckPasswordRules

    // PURPOSE: Verify the current password.
    //
    //  PARAMS:
    //     - INPUT:
    //           - UserId:      Int32;
    //           - Password:    string;
    //
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - bool: if verification is ok then return "true" else "false".
    //
    public Boolean VerifyCurrentPassword(Int32 UserId, String Password, SqlTransaction SqlTrx)
    {
        StringBuilder _sb;
        Byte[] _bin_pwd;
        Int32 _effective_user_id;

        try
        {
            _sb = new StringBuilder();
            _sb.AppendLine("SELECT   GU_PASSWORD ");
            _sb.AppendLine("       , ISNULL(GU_MASTER_ID, @pUserId) ");
            _sb.AppendLine("  FROM   GUI_USERS   ");
            _sb.AppendLine(" WHERE   GU_USER_ID = @pUserId");

            using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
            {
                _cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = UserId;

                using (SqlDataReader _reader = _cmd.ExecuteReader())
                {
                    if (!_reader.Read())
                    {
                        return false;
                    }

                    _bin_pwd = _reader.GetSqlBytes(0).Buffer;
                    _effective_user_id = _reader.GetInt32(1);
                }

                if (_bin_pwd[0] == 0)
                {
                    Byte[] _in_pwd;

                    _in_pwd = this.HashPassword(_effective_user_id, Password);

                    return ComparePassword(_in_pwd, _bin_pwd);
                }
                else
                {
                    String _aux;

                    _aux = Encoding.Default.GetString(_bin_pwd);
                    _aux = _aux.Substring(0, _aux.IndexOf('\0'));

                    return (PasswordEquals(Password, _aux));
                }
            }

        }
        catch (Exception _ex)
        {
            Log.Exception(_ex);
        }

        return false;

    }// VerifyCurrentPassword

    private String CleanExtraCharsFromTrackdata(String Password)
    {
        String _pwd;
        UInt64 _num;
        String _aux;

        if (Password == null)
        {
            return "";
        }

        if (Password.Length == 22 && Password.StartsWith("%") && Password.EndsWith("_"))
        {
            _pwd = Password.Substring(1, Password.Length - 2);

            if (UInt64.TryParse(_pwd, out _num))
            {
                _aux = _num.ToString().PadLeft(20, '0');

                if (_aux == _pwd)
                {
                    return _pwd;
                }
            }
        }

        if (!RulesEnabled)
        {
            Password = Password.ToUpper();
        }

        return Password;

    }
    private Byte[] HashPassword(Int32 UserId, String Password)
    {
        SHA256 _sha;
        Byte[] _buffer;
        Byte[] _hash;
        int _idx_w;

        Password = CleanExtraCharsFromTrackdata(Password);

        _sha = SHA256.Create();
        _buffer = Encoding.UTF8.GetBytes(UserId.ToString() + Password);
        _hash = _sha.ComputeHash(_buffer);

        _buffer = new Byte[40];
        _idx_w = 0;
        _buffer[_idx_w++] = 0;

        for (int _idx = 0; _idx < _hash.Length; _idx++)
        {
            _buffer[_idx_w++] = _hash[_idx];
        }

        return _buffer;
    } // HashPassword




    private static Boolean ComparePassword(Byte[] Pwd1, Byte[] Pwd2)
    {
        if (Pwd1.Length != Pwd2.Length)
        {
            return false;
        }

        if (Pwd1.Length != 40)
        {
            return false;
        }

        for (int _i = 0; _i < Pwd1.Length; _i++)
        {
            if (Pwd1[_i] != Pwd2[_i])
            {
                return false;
            }
        }

        return true;
    }

    public Boolean PasswordRecentlyUsed(Int32 UserId, String Password, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      String[] _history_password;
      Int32 _idx_null_char;

      try
      {
        _history_password = new String[MaxPasswordHistory + 1];

        _sb = new StringBuilder();
        _sb.AppendLine("SELECT   CAST(GU_PASSWORD AS char(40)) AS GU_PASSWORD ");
        _sb.AppendLine("       , ISNULL(CAST(GU_PASSWORD_H1 AS char(40)), ' ') AS GU_PASSWORD_H1 ");
        _sb.AppendLine("       , ISNULL(CAST(GU_PASSWORD_H2 AS char(40)), ' ') AS GU_PASSWORD_H2 ");
        _sb.AppendLine("       , ISNULL(CAST(GU_PASSWORD_H3 AS char(40)), ' ') AS GU_PASSWORD_H3 ");
        _sb.AppendLine("       , ISNULL(CAST(GU_PASSWORD_H4 AS char(40)), ' ') AS GU_PASSWORD_H4 ");
        _sb.AppendLine("       , ISNULL(CAST(GU_PASSWORD_H5 AS char(40)), ' ') AS GU_PASSWORD_H5 ");
        _sb.AppendLine("  FROM   GUI_USERS ");
        _sb.AppendLine(" WHERE   GU_USER_ID = @pUserId");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = UserId;

          using (SqlDataReader _sql_rd = _cmd.ExecuteReader())
          {
            while (_sql_rd.Read())
            {
              for (Int32 _idx_pwd = 0; _idx_pwd <= MaxPasswordHistory; _idx_pwd++)
              {
                _history_password[_idx_pwd] = _sql_rd.GetString(_idx_pwd);
              }
            }
          }
        }

        for (Int32 _idx_pwd = 0; _idx_pwd <= MaxPasswordHistory; _idx_pwd++)
        {
          // Delete chars '\0'
          _idx_null_char = _history_password[_idx_pwd].IndexOf('\0');
          if (_idx_null_char >= 0)
          {
            _history_password[_idx_pwd] = _history_password[_idx_pwd].Substring(0, _idx_null_char);
          }
          if (PasswordEquals(Password, _history_password[_idx_pwd]))
          {
            return true;
          }
        }

        return false;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return true;

    }// PasswordRecentlyUsed

    // PURPOSE: Check the login rules.
    //
    //  PARAMS:
    //     - INPUT:
    //           - UserName:    string;
    //
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - bool: if check is ok then return "true" else "false".
    //
    public Boolean CheckLoginRules(String UserName)
    {
      if (!RulesEnabled)
      {
        return true;
      }

      return (UserName.Length >= MinLoginLength);

    }//End CheckLoginRules

    public String ParsePassword(String Password)
    {
      if (String.IsNullOrEmpty(Password))
      {
        return "";
      }

      if (RulesEnabled)
      {
        return Password;
      }
      return Password.ToUpper();
    }

    public Boolean PasswordEquals(String Password1, String Password2)
    {
      if (String.IsNullOrEmpty(Password1) || String.IsNullOrEmpty(Password2))
      {
        return false;
      }

      if (RulesEnabled)
      {
        return (Password1 == Password2);
      }
      return (Password1.ToUpper() == Password2.ToUpper());
    }

    public String MessagePasswordPolicyInvalid()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      //La nueva clave debe cumplir las siguientes condiciones:\n
      _sb.AppendLine(Resource.String("STR_PASSWORD_POLICY_LINE_0"));
      _sb.AppendLine("");

      if (!RulesEnabled)
      {
        //    - La contrase�a no puede dejarse en blanco.\n
        _sb.AppendLine(Resource.String("STR_PASSWORD_POLICY_DISABLE_LINE_1"));
      }
      else
      {
        //     - Debe tener una longitud m�nima de @p0.\n
        _sb.AppendLine(Resource.String("STR_PASSWORD_POLICY_ENABLE_MIN_LONG", MinPasswordLength));

        if (MinPasswordDigits > 0)
        {
          //     - Debe tener un m�nimo de @p0 d�gitos.\n
          _sb.AppendLine(Resource.String("STR_PASSWORD_POLICY_ENABLE_MIN_NUM", MinPasswordDigits));
        }
        if (MinPasswordLowerCase > 0)
        {
          //     - Debe tener un m�nimo de @p0 caracteres en min�sculas.\n
          _sb.AppendLine(Resource.String("STR_PASSWORD_POLICY_ENABLE_MIN_LOW_CASE_CHARS", MinPasswordLowerCase));
        }
        if (MinPasswordUpperCase > 0)
        {
          //     - Debe tener un m�nimo de @p0 caracteres en mayusculas.\n
          _sb.AppendLine(Resource.String("STR_PASSWORD_POLICY_ENABLE_MIN_UP_CASE_CHARS", MinPasswordUpperCase));
        }
        if (MinPasswordSpecialCase > 0)
        {
          //     - Debe tener un m�nimo de @p0 caracteres especiales.\n
          _sb.AppendLine(Resource.String("STR_PASSWORD_POLICY_ENABLE_MIN_SPECIAL_CHARS", MinPasswordSpecialCase));
        }

        //       - No puede contener el nombre de usuario.
        _sb.AppendLine(Resource.String("STR_PASSWORD_POLICY_ENABLE_USER_CONTAINED"));
        //        - No puede contener el nombre del equipo.
        _sb.AppendLine(Resource.String("STR_PASSWORD_POLICY_ENABLE_COMPUTER_CONTAINED"));
        //        - No puede contener espacios en blanco al inicio ni al final de la palabra clave
        _sb.AppendLine(Resource.String("STR_PASSWORD_POLICY_ENABLE_BEGIN_END_WHITE_SPACE"));

      }

      //    - No debe haber sido utilizada anteriormente.
      _sb.AppendLine(Resource.String("STR_PASSWORD_POLICY_ENABLE_HISTORY"));

      return _sb.ToString();
    } // MessagePasswordPolicyInvalid


  }
}
