//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Occupation.cs
// 
//   DESCRIPTION: Class to manage an Occupation data
// 
//        AUTHOR: Lucas Cordero
// 
// CREATION DATE: 13-MAY-2011
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 13-MAY-2011 LCM    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using WSI.Common;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Globalization;

namespace WSI.Common
{
  public class CommonOccupationInformation
  {
  } // CommonOccupationInformation

  public class Occupation
  {
    static public void ReadOccupationSessionData(SqlTransaction SQLTransaction,
                                                 DateTime DateFrom,
                                                 DateTime DateTo,
                                                 ref TYPE_OCCUPATION_SESSION_STATS OccupationSessionData)
    {
      SqlDataReader _sql_reader;
      SqlCommand _sql_cmd;
      Decimal _total_time;
      Int32 _total_connected;
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("SELECT   SUM(CASE WHEN COUNTER = 'T' THEN NUM ELSE 0 END) ");
      _sb.AppendLine("       , SUM(CASE WHEN COUNTER = 'S' THEN NUM ELSE 0 END) ");
      _sb.AppendLine("       , SUM(CASE WHEN COUNTER = 'C' THEN NUM ELSE 0 END) ");
      _sb.AppendLine("  FROM ( SELECT   'T' COUNTER, COUNT(*) NUM ");
      _sb.AppendLine("           FROM   TERMINALS     ");
      _sb.AppendLine("          WHERE   TE_STATUS = 0 ");
      _sb.AppendLine("            AND   TE_TERMINAL_TYPE IN ( " + Misc.GamingTerminalTypeListToString() + " ) ");
      _sb.AppendLine("          UNION   ");
      _sb.AppendLine("         SELECT   'S' COUNTER, COUNT(*) NUM         ");
      _sb.AppendLine("           FROM ( SELECT   DISTINCT PS_TERMINAL_ID  ");
      _sb.AppendLine("                    FROM   PLAY_SESSIONS            ");
      _sb.AppendLine("                   WHERE   PS_STATUS = 0 ) TMP      ");
      _sb.AppendLine("          UNION   ");
      _sb.AppendLine("         SELECT   'C' COUNTER, COUNT(*) NUM          ");
      _sb.AppendLine("           FROM   TERMINALS_CONNECTED    ");
      _sb.AppendLine("          WHERE   TC_CONNECTED = 1       ");
      _sb.AppendLine("            AND   TC_DATE      = @pToday ");
      _sb.AppendLine("        ) X ");

      _total_connected = 0;

      OccupationSessionData.is_set_num_opened_sessions = false;
      OccupationSessionData.is_set_now = false;
      OccupationSessionData.is_set_till_now = false;
      OccupationSessionData.is_set_last_hour = false;

      try
      {
        // Total terminals
        using (_sql_cmd = new SqlCommand(_sb.ToString(), SQLTransaction.Connection, SQLTransaction))
        {
          _sql_cmd.CommandTimeout = Mailing.SQL_COMMAND_TIMEOUT;
          _sql_cmd.Parameters.Add("@pToday", SqlDbType.DateTime).Value = Misc.Opening(DateFrom).Date;

          using (_sql_reader = _sql_cmd.ExecuteReader())
          {
            if (_sql_reader.Read())
            {
              OccupationSessionData.total_terminals = _sql_reader.GetInt32(0);
              OccupationSessionData.num_opened_sessions = _sql_reader.GetInt32(1);
              _total_connected = _sql_reader.GetInt32(2);
              OccupationSessionData.total_terminals = Math.Max(OccupationSessionData.total_terminals, 0);
              OccupationSessionData.num_opened_sessions = Math.Min(OccupationSessionData.num_opened_sessions, OccupationSessionData.total_terminals);
              _total_connected = Math.Max(_total_connected, 0);

              if (OccupationSessionData.total_terminals > 0)
              {
                OccupationSessionData.now = (1.0m * OccupationSessionData.num_opened_sessions) / OccupationSessionData.total_terminals;
              }
              else
              {
                OccupationSessionData.now = 0;
              }

              OccupationSessionData.is_set_num_opened_sessions = true;
              OccupationSessionData.is_set_now = true;
            }
          }
        }

        _sb = new StringBuilder();
        _sb.AppendLine("SELECT   SUM ( 1.0 * DATEDIFF ( SECOND ");
        _sb.AppendLine("                              , CASE WHEN (PS_STARTED  >= @pDateFrom) THEN PS_STARTED ELSE @pDateFrom END  ");
        _sb.AppendLine("                              , CASE WHEN (PS_STATUS = 0) THEN @pDateTo ELSE ( CASE WHEN (PS_FINISHED <= @pDateTo) THEN PS_FINISHED ELSE @pDateTo END ) END  ");
        _sb.AppendLine("                              )  ");
        _sb.AppendLine("             )             ");
        _sb.AppendLine("  FROM   PLAY_SESSIONS     ");
        _sb.AppendLine(" WHERE   @pDateFromX <= PS_STARTED  AND PS_STARTED  < @pDateTo   ");
        _sb.AppendLine("   AND   @pDateFrom  <= PS_FINISHED AND PS_FINISHED < @pDateToX ");

        // Today so far occupancy
        using (_sql_cmd = new SqlCommand(_sb.ToString(), SQLTransaction.Connection, SQLTransaction))
        {
          _sql_cmd.CommandTimeout = Mailing.SQL_COMMAND_TIMEOUT;
          _sql_cmd.Parameters.Add("@pDateFrom", SqlDbType.DateTime).Value = DateFrom;
          _sql_cmd.Parameters.Add("@pDateTo", SqlDbType.DateTime).Value = DateTo;
          _sql_cmd.Parameters.Add("@pDateFromX", SqlDbType.DateTime).Value = DateFrom.AddDays(-7);
          _sql_cmd.Parameters.Add("@pDateToX", SqlDbType.DateTime).Value = DateTo.AddDays(+7);

          using (_sql_reader = _sql_cmd.ExecuteReader())
          {
            if (_sql_reader.Read())
            {
              if (!_sql_reader.IsDBNull(0))
              {
                TimeSpan _ts;
                Decimal _ocup;

                _ts = DateTo.Subtract(DateFrom);
                _total_time = (Decimal)(_ts.TotalSeconds * _total_connected);

                _ocup = _sql_reader.GetDecimal(0);
                _ocup = Math.Min(_ocup, _total_time);

                if (_total_time > 0)
                {
                  _ocup = _ocup / _total_time;
                }
                else
                {
                  _ocup = 0;
                }

                OccupationSessionData.till_now = _ocup;
              }
              // Has been read, but can be null. It's ok.
              OccupationSessionData.is_set_till_now = true;
            } // (sql_data_reader.Read())
          }
        }

        // Last hour occupancy
        using (_sql_cmd = new SqlCommand(_sb.ToString(), SQLTransaction.Connection, SQLTransaction))
        {
          DateTime _from;

          _sql_cmd.CommandTimeout = Mailing.SQL_COMMAND_TIMEOUT;

          _from = DateTo.AddHours(-1); // Last hour
          _sql_cmd.Parameters.Add("@pDateFrom", SqlDbType.DateTime).Value = _from;
          _sql_cmd.Parameters.Add("@pDateTo", SqlDbType.DateTime).Value = DateTo;
          _sql_cmd.Parameters.Add("@pDateFromX", SqlDbType.DateTime).Value = _from.AddDays(-7);
          _sql_cmd.Parameters.Add("@pDateToX", SqlDbType.DateTime).Value = DateTo.AddDays(+7);

          using (_sql_reader = _sql_cmd.ExecuteReader())
          {
            if (_sql_reader.Read())
            {
              if (!_sql_reader.IsDBNull(0))
              {
                TimeSpan _ts;
                Decimal _ocup;

                _ts = DateTo.Subtract(_from);
                _total_time = (Decimal)(_ts.TotalSeconds * _total_connected);

                _ocup = _sql_reader.GetDecimal(0);
                _ocup = Math.Min(_ocup, _total_time);

                if (_total_time > 0)
                {
                  _ocup = _ocup / _total_time;
                }
                else
                {
                  _ocup = 0;
                }

                OccupationSessionData.last_hour = _ocup;
              }
              // Has been read, but can be null. It's ok.
              OccupationSessionData.is_set_last_hour = true;
            } // (sql_data_reader.Read())
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // ReadOccupationSessionData

    static private DateTime GetOpeningDate(DateTime Date, SqlTransaction SQLTransaction)
    {
      DateTime ret = DateTime.MinValue;
      SqlDataReader sql_data_reader;
      SqlCommand sql_command;

      using (sql_command = new SqlCommand())
      {
        sql_command.Parameters.Add("@pDate", SqlDbType.DateTime).Value = Date;
        sql_command.CommandText = "SELECT dbo.ConcatOpeningTime(0, @pDate) AS OPENING_DATE";
        sql_command.Connection = SQLTransaction.Connection;
        sql_command.Transaction = SQLTransaction;

        using (sql_data_reader = sql_command.ExecuteReader())
        {
          while (sql_data_reader.Read())
          {
            ret = Convert.ToDateTime(sql_data_reader["OPENING_DATE"]);
            break;
          } // (sql_data_reader.Read())

          sql_data_reader.Close();
        }
      }

      return ret;
    }
  } // Occupation

} // CommonOccupationInformation