using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace WSI.Common
{

  public enum SITE_STATE
  {
    UNKNOWN = -1,
    NOT_ACTIVE = 0,
    ENABLED = 1,
    DISABLED = 2,
    CLOSED = 3,
  }

  public struct TYPE_CARD_REPLACEMENT
  {
    // Structure used to print the card replacement voucher
    public Int64 account_id;
    public string old_track_data;
    public string new_track_data;
    public Currency card_replacement_price;
  }

  public struct TYPE_CASH_REDEEM
  {
    public Currency total_cash_in;          // Initial cash in

    public Currency TotalDevolution         // devolution + cancellation
    {
      get
      {
        return cancellation + devolution;
      }
    }
    public Currency cancellation;              // Amount not used for play

    public Currency devolution;                // Amount used for play
    public Currency prize;                     // Amount won
    public String tax1_name;                 // tax1 name
    public String tax2_name;                 // tax2 name
    public Currency tax1;                      // tax1
    public Currency tax2;                      // tax2

    public Currency TotalTaxes                 // tax1 + tax2
    {
      get { return tax1 + tax2; }
    }

    public Boolean tax_return_enabled;
    public Currency tax_return;

    public Boolean decimal_rounding_enabled;
    public Currency decimal_rounding;

    public Currency service_charge;


    public Boolean must_redeem_card_deposit;  // Indicate if the card deposit must be redeemed
    public Currency card_deposit;              // Deposit for the card

    public Currency won_lock_to_cancel;        // Amount to substract to WonLock if it is a cancellation

    public MultiPromos.PromoBalance lost_balance; // Total lost due redeem
    public DataTable lost_promotions;             // Lost promotions

    public Currency TotalRedeemed              // TotalDevolution + prize
    {
      get
      {
        return TotalDevolution + prize;
      }
    }
    public Currency TotalRedeemAfterTaxes      // TotalRedeemed - TotalTaxes + tax_return
    {
      get
      {
        return TotalRedeemed - TotalTaxes + TotalRounding - ServiceCharge;
      }
    }

    public Currency TotalPaid                  // TotalRedeemAfterTaxes + card_deposit
    {
      get
      {
        return TotalRedeemAfterTaxes + ((must_redeem_card_deposit) ? card_deposit : 0);
      }
    }

    public Currency cancelled_promo_redeemable;       // Amount cancelled (Split B)

    public Int64 cancelled_sequence_id;               // Sequence id for devolution/cancel


    public Boolean RoundingEnabled
    {
      get
      {
        return tax_return_enabled || decimal_rounding_enabled;
      }
    }
    public Currency TotalRounding
    {
      get
      {
        return tax_return + decimal_rounding;
      }
    }

    public Currency ServiceCharge
    {
      get
      {
        return service_charge;
      }
    }

  }

  public struct TYPE_SPLIT_ELEMENT
  {
    public String company_name;
    public String name;
    public Decimal cashin_pct;
    public Decimal cancellation_pct;           // Same as cashin_pct
    public Decimal devolution_pct;
    public String tax_name;
    public Decimal tax_pct;
    public String header;
    public String footer;
    public String footer_cashin;
    public String footer_dev;
    public String footer_cancel;
    public Boolean hide_total;
    public Boolean hide_currency_symbol;
    public Boolean show_sequence;
    public Boolean total_in_letters;
    public String cash_in_voucher_title;
    public String cash_out_voucher_title;
  }

  public struct TYPE_SPLITS
  {
    public Boolean enabled_b;
    public TYPE_SPLIT_ELEMENT company_a;
    public TYPE_SPLIT_ELEMENT company_b;
    public Boolean prize_coupon;
    public String prize_coupon_text;
    public Boolean hide_promo;
    public String text_promo;
    public Boolean promo_and_coupon;
    public String text_promo_and_coupon;
    public Boolean rounding_enabled;
    public Int32 rounding_cents;
  }

  public struct TYPE_SITE_JACKPOT_WINNER
  {
    public DateTime awarded_date;
    public Int32 jackpot_index;
    public String jackpot_name;
    public String awarded_on_provider_name;
    public Int32 awarded_on_terminal_id;
    public String awarded_on_terminal_name;
    public Int64 awarded_to_account_id;
    public String awarded_to_account_name;
    public Currency amount;
  }

  public struct TYPE_CJ_PARAMETERS
  {
    public String local_ip_1;
    public String remote_ip_1;
    public String vendor_id_1;
    public String local_ip_2;
    public String remote_ip_2;
    public String vendor_id_2;
  }

  public struct TYPE_MAILING_PARAMETERS
  {
    public String authorized_server_list;
    public Boolean enabled;
    public Boolean smtp_enabled;
    public String smtp_server;
    public Int32 smtp_port;
    public Boolean smtp_secured;
    public String smtp_username;
    public String smtp_password;
    public String smtp_domain;
    public String smtp_email_from;
  }

  public struct TYPE_MAILING_PROVIDER_STATS
  {
    public String name;
    public Currency played;
    public Currency won;
    public Currency netwin;
    public Int32 num_terminals;
  }

  public struct TYPE_OCCUPATION_SESSION_STATS
  {
    public Boolean is_set_num_opened_sessions;
    public Boolean is_set_now;
    public Boolean is_set_last_hour;
    public Boolean is_set_till_now;
    public Int32 total_terminals;
    public Int32 num_opened_sessions;
    public Percentage now;
    public Percentage last_hour;
    public Percentage till_now;
  }

  public struct TYPE_MAILING_REPORT
  {
    public String site_name;
    public DateTime date_from;
    public DateTime date_to;
    public Currency cashier_inputs;
    public Currency cashier_outputs;
    public Currency cashier_prize_gross;
    public Currency cashier_prize_taxes;
    public Currency cashier_result;
    public Currency site_jackpot;

    public TYPE_OCCUPATION_SESSION_STATS occupation;
    public TYPE_MAILING_PROVIDER_STATS[] provider_stats;
  }

  public enum CASH_MODE
  {
    PARTIAL_REDEEM = 0,
    TOTAL_REDEEM = 1
  }

  public enum MBMovementType
  {
    ManualChangeLimit = 0,
    TransferCredit = 1,
    DepositCash = 2,
    CloseSession = 3,
    CreditNonRedeemable = 4,
    CreditCoverCoupon = 5,
    CreditNonRedeemable2 = 6,   // For future promotions
    AutomaticChangeLimit = 7,
    CashLost = 8
  }

  public enum GIFT_TYPE
  {
    OBJECT = 0
  ,
    NOT_REDEEMABLE_CREDIT = 1
      ,
    DRAW_NUMBERS = 2
      ,
    SERVICES = 3
      ,
    REDEEMABLE_CREDIT = 4
      , UNKNOWN = 999
  }

  public enum GIFT_REQUEST_STATUS
  {
    PENDING = 0
  ,
    DELIVERED = 1
      ,
    EXPIRED = 2
      , CANCELED = 3
  }

  // RCI 21-SEP-2010: When an OperationCode is added, you must define it in Operations.cs:DB_InsertOperation().
  public enum OperationCode
  {
    CASH_IN = 1,
    CASH_OUT = 2,
    PROMOTION = 3,
    MB_CASH_IN = 4,
    MB_PROMOTION = 5,
    GIFT_REQUEST = 6,
    GIFT_DELIVERY = 7,
    DRAW_TICKET = 8,
    GIFT_DRAW_TICKET = 9,
    REDEEMABLE_CREDITS_EXPIRED = 100,
    NOT_REDEEMABLE_CREDITS_EXPIRED = 101,
    POINTS_EXPIRED = 102,
    HOLDER_LEVEL_CHANGED = 103, // OperationData [-3 .. 3]
    ACCOUNT_CREATION = 104,
    ACCOUNT_PERSONALIZATION = 105,

    // JMM 22-NOV-2011
    ACCOUNT_PIN_CHANGED = 106,
    ACCOUNT_PIN_RANDOM = 107,
    NOT_REDEEMABLE2_CREDITS_EXPIRED = 108,  // RCI 16-FEB-2012: For the new Not-Redeemable credit (used now for Cover Coupon).

    //SSC 09-MAR-2012: Added new Operation Codes for Note Acceptor
    NA_CASH_IN = 109,
    NA_PROMOTION = 110,

    CANCEL_GIFT_INSTANCE = 111,

  }

  public enum OperationSource
  {
    CASHIER = 1,
    MOBILE_BANK = 2,
    LAST_VOUCHER = 3
  }

  public enum VoucherSource
  {
    FROM_OPERATION = 1,
    FROM_VOUCHER = 2
  }

  public enum PinCheckStatus
  {
    OK = 0,
    WRONG_PIN = 1,
    ACCOUNT_BLOCKED = 2,
    ERROR = 3,
    ACCOUNT_NO_PIN = 4
  }

  public enum DrawType
  {
    BY_PLAYED_CREDIT = 0,
    BY_REDEEMABLE_SPENT = 1,
    BY_TOTAL_SPENT = 2,
    BY_POINTS = 3,
    BY_CASH_IN = 4,
    BY_CASH_IN_POSTERIORI = 5
  }

  public enum DrawAccountType
  {
    ALL = 0,
    ONLY_ANONYMOUS = 1,
    ONLY_PERSONALIZED = 2
  }

  public enum DrawGenderFilter
  {
    ALL = 0,
    MEN_ONLY = 1,
    WOMEN_ONLY = 2
  }

  // RCI 22-DEC-2010: Automatic Mailing Programming
  public enum MAILING_PROGRAMMING_TYPE
  {
    CASH_INFORMATION = 1,
    WSI_STATISTICS = 2,
    CONNECTED_TERMINALS = 3,
    ACCOUNT_MOVEMENTS = 4
  }

  public enum MAILING_INSTANCES_STATUS
  {
    PENDING = 0,
    READY = 1,
    SENT = 2,
    ERROR_SENDING = 3,
    ERROR_PROCESSING = 4,
    ERROR_CONVERSION = 5
  }

  public enum CashierMovementsFilter
  {
    SESSION_ID = 1,
    SESSION_ID_BY_QUERY = 2,
    DATE_RANGE = 3
  }

  public enum GroupDateFilter
  {
    NO_FILTER = 0,
    HOURLY = 1,
    G24_HOUR = 2,
    DAILY = 3,
    WEEKLY = 4,
    MONTHLY = 5
  }

  public enum QuerySalesProviderFilter
  {
    NO_FILTER = 0,
    DATE_PROVIDER_TERMINAL = 1,
    DATE_PROVIDER = 2,
    PROVIDER_TERMINAL = 3,
    PROVIDER = 4,
    DATE = 5
  }

  public enum LicenseStatus
  {
    NOT_CHECKED = 0,
    INVALID = 1,
    VALID = 2,
    NOT_AUTHORIZED = 3,
    EXPIRED = 4,
    VALID_EXPIRES_SOON = 5
  }

  public enum DataSetRawFormat
  {
    BIN_DEFLATE_BASE64 = 1,
    XML_ZLIB_BASE64 = 2,
  }

  public enum CashierVoucherType
  {
    NotSet = 0,
    CashIn_A = 1,
    CashIn_B = 2,
    CashOut_A = 3,
    Cancel_B = 4,
    ServiceCharge_B = 5,  // AJQ 12-SEP-2012, Service Charge: it should work like CashIn_B
  }

  public enum PrizeComputationMode
  {
    RecalculateInitialCashInEveryTime = 0,  // Default
    KeepCountingInitialCashIn = 1
  }

  //public enum AlarmSourceCode
  //{
  //  NotSet = 0,
  //  GameTerminal = 1,
  //  Cashier = 2, 
  //  User = 3,

  //  TerminalSASHost    = 4,
  //  TerminalSASMachine = 5,
  //  TerminalSystem     = 6,
  //  TerminalWCP = 7,
  //  Terminal3GS = 8,

  //  Service            = 9,
  //  System             = 10,
  //}

  //public enum AlarmCode
  //{
  //  Unknown = 0x00000000,  // Warn

  //  TerminalSystem_GameMeterReset = 0x00020001,  // Warn
  //  TerminalSystem_GameMeterBigIncrement = 0x00020002,  // Warn
  //  TerminalSystem_HPCMeterBigIncrement = 0x00020003,  // Warn
  //  TerminalSystem_MachineMeterReset = 0x00020004,  // Warn
  //  TerminalSystem_MachineMeterBigIncrement = 0x00020005,  // Warn
  //  TerminalSystem_GameMeterDenomination = 0x00020006,  // Warn

  //  // Protocols
  //  // - WCP
  //  TerminalSystem_WcpConnected = 0x00020011,     // Info
  //  TerminalSystem_WcpDisconnected = 0x00020012,  // Error
  //  // - WC2
  //  TerminalSystem_Wc2Connected = 0x00020021,  // Info
  //  TerminalSystem_Wc2Disconnected = 0x00020022,  // Error

  //  // SERVICES
  //  // - Start/Stop
  //  Service_Started  = 0x00030001,  // Info
  //  Service_Stopped  = 0x00030002,  // Error
  //  Service_Stopping = 0x00030003,  // Warn
  //  Service_Running  = 0x00030004,  // Info
  //  Service_StandBy  = 0x00030005,  // Info

  //  // - License
  //  Service_LicenseExpired = 0x00030011, // Error
  //  Service_LicenseWillExpireSoon = 0x00030012, // Warn
  //  // - Software update
  //  Service_NewVersionDownloaded = 0x00030021,  // Info
  //  Service_NewVersionAvailable = 0x00030022,  // Info

  //  // USER
  //  // - Login
  //  User_MaxLoginAttemps = 0x00040001,
  //  //-Closing Cashier
  //  User_WrongDelivered = 0x00040002,

  //  // SYSTEM
  //  // Summary:
  //  //     A change in the power status of the computer is detected, such as a switch
  //  //     from battery power to A/C. The system also broadcasts this event when remaining
  //  //     battery power slips below the threshold specified by the user or if the battery
  //  //     power changes by a specified percentage.
  //  SystemPowerStatus_PowerStatusChange = 0x00050001,
  //  // Summary:
  //  //     The system has resumed operation after a critical suspension caused by a
  //  //     failing battery.
  //  SystemPowerStatus_ResumeCritical = 0x00050002,
  //  //
  //  // Summary:
  //  //     Battery power is low.
  //  SystemPowerStatus_BatteryLow = 0x00050003,




  //  // WCP Operations "Events" --> Alarms 0x0006 
  //  //
  //  // 0x00060000 + OperationCode
  //  //

  //  // WCP DeviceStatus        --> Alarms 0x0007 
  //  //
  //  // 0x00070000 + ((DeviceCode << 8) & 0x0000FF00) + (DeviceStatus & 0x000000FF)
  //  //


  //}

  //public enum AlarmSeverity
  //{
  //  Info    = 1,
  //  Warning = 2,
  //  Error   = 3
  //}

  public enum ENUM_GUI
  {
    GUI_SETUP = 0,
    GUI_ALARMS = 1,
    GUI_VERSION_CONTROL = 2,
    GUI_AUDITOR = 3,
    GUI_CONFIGURATION = 4,
    GUI_COMMUNICATIONS = 5,
    GUI_CONTROL = 6,
    GUI_TICKETSMANAGER = 7,
    GUI_STATISTICS = 8,
    GUI_JACKPOTMANAGER = 9,
    GUI_SYSTEMMONITOR = 10,
    GUI_INVOICE_SYSTEM = 11,
    GUI_SOFTWARE_DOWNLOAD = 12,
    GUI_DATECODE = 13,
    WIGOS_AS_GUI = 14,
    CASHIER = 15,
    ISTATS = 104,
    MULTISITE_GUI = 200,
    CASHDESK_DRAWS_GUI = 201,
    CHILE_SIOC = 202,
    SMARTFLOOR = 203,
    SMARTFLOOR_APP = 204
  }

  public enum ENUM_MONEY_SOURCE
  {
    MONEY_SOURCE_UNKNOWN = 0,

    MONEY_SOURCE_CREDIT_LKAS = 1,
    MONEY_SOURCE_VOUCHER = 2,
    MONEY_SOURCE_SMARTCARD = 3,
    MONEY_SOURCE_CREDIT_LKT = 4,

    MONEY_SOURCE_NOTE_ACCEPTOR = 5,
    MONEY_SOURCE_COIN_ACCEPTOR = 6,

    MONEY_SOURCE_GAME_PRIZE = 7,

    MONEY_SOURCE_SAS_HOST = 20,
    MONEY_SOURCE_HANDPAY = 21,

  }

  public enum ENUM_MONEY_TARGET
  {
    MONEY_TARGET_UNKNOWN = 0,

    MONEY_TARGET_CREDIT_LKAS = 1,
    MONEY_TARGET_VOUCHER = 2,
    MONEY_TARGET_SMARTCARD = 3,
    MONEY_TARGET_CREDIT_LKT = 4,

    MONEY_TARGET_GAME_PLAY = 5,

    MONEY_TARGET_COIN_HOPPER = 10,

    MONEY_TARGET_SAS_HOST = 20,
    MONEY_TARGET_HANDPAY = 21,

  }

  public enum ENUM_ACCEPTED_MONEY_STATUS
  {
    ACCEPTED_MONEY_STATUS_NONE = 0,
    ACCEPTED_MONEY_STATUS_ACCEPTED = 1,
    ACCEPTED_MONEY_STATUS_TRANSFERRING = 2,
    ACCEPTED_MONEY_STATUS_TRANSFERRED = 3,
    ACCEPTED_MONEY_STATUS_TRANSFER_FAILED = 4,
    ACCEPTED_MONEY_STATUS_ERROR = 5,
  }

  //SSC 21-MAR-2012
  public enum MB_USER_TYPE
  {
    NOT_ASSIGNED = -1,
    ANONYMOUS = 0,
    PERSONAL = 1,
    SYS_ACCEPTOR = 2,
    SYS_PROMOBOX = 3,
  };

  public enum DOCUMENT_TYPE
  {
    WITHOLDING = 1,
    SIGNATURE = 2,
    PHOTO_GIFT = 3
  };

  // ANG 21-MAY-2012
  public enum MEDIA_TYPE
  {
    IMAGE = 1,
    AUDIO = 2,
    VIDEO = 3
  };


  // ANG 24-MAY-2012
  public enum PLAYER_INFO_FIELD_TYPE
  {
    FULL_NAME = 1,
    CITY = 2,
    ZIP = 3,
    PIN = 4,
    ADDRESS_01 = 10,
    ADDRESS_02 = 11,
    ADDRESS_03 = 12,
    EMAIL_01 = 20,
    EMAIL_02 = 21,
    PHONE_NUMBER_01 = 30,
    PHONE_NUMBER_02 = 31,
    ID1 = 40,
    ID2 = 41,
    NAME = 52,
    SURNAME1 = 50,
    SURNAME2 = 51,
    BIRTHDAY = 60
  };






  // ANG 24-MAY-2012
  public enum PLAYER_FUNCTIONALITIES_TYPE
  {
    SHOW_DRAW = 0,
    SHOW_PROMOTIONS = 1,
    SHOW_FUTURE_PROMOTIONS = 2
  }

  public enum WKT_IMAGES_ID
  {

    IMG01 = 0,
    IMG02 = 1,
    IMG03 = 2,
    IMG04 = 3,
    IMG05 = 4,
    IMG06 = 5,
    IMG07 = 6,
    IMG08 = 7,
    IMG09 = 9,
    IMG010 = 10
  }

  public enum WKT_FIELD_TYPE
  {
    NUMBER = 1,
    TEXT = 2,
    EMAIL = 3,
    ZIPCODE = 4,
    PHONE = 5,
    DATE = 6
  }


  // ANG 14-06-2012
  public enum WKT_UPDATE_FIELD_STATUS
  {
    INTERNAL_BEING_UPDATED = -1,
    UPDATED = 0,
    NOT_UPDATED = 1,
    ERROR_NOT_EDITABLE = 2,
    ERROR_MIN_LENGTH = 3,
    ERROR_MAX_LENGTH = 4,
    ERROR_VALIDATION = 5,
    ERROR_INVALID_DATE = 6,
    ERROR_LEGAL_AGE = 7,
    ERROR = 8
  }

  // DDM 03-AUG-2012
  public enum ACCOUNT_MARITAL_STATUS
  {
    SINGLE = 0,
    MARRIED = 1,
    DIVORCED = 2,
    WIDOWED = 3,
    UNMARRIED = 4,
    OTHER = 5,
    UNSPECIFIED = 6
  }

  // DDM 03-AUG-2012
  public enum ACCOUNT_HOLDER_ID_TYPE
  {
    UNKNOWN = 0,   // is shown as "OTHER"
    RFC = 1,
    CURP = 2,
    ELECTOR_CODE = 3,
    PASSPORT = 4,
    MAX_TYPES = 5   // Indicates the number of elements.
    // IMPORTANT! If added another type, should be increased.
  }

  // DDM 09-AUG-2012
  public enum FULL_NAME_TYPE
  {
    ACCOUNT = 0,
    WITHHOLDING = 1
  }

  // XCD 16-AUG-2012
  public enum GUI_USER_BLOCK_REASON
  {
    NONE = 0x0000,
    DISABLED = 0x0001,
    WRONG_PASSWORD = 0x0002,
    INACTIVITY = 0x0004,
    UNSUBSCRIBED = 0x0008
  }

  // DDM 27-AUG-2012
  public enum ACP_USER_TYPE
  {
    CASHIER = 0,
    MOBILE_BANK = 1
  }

  // RXM 01-OCT-2012
  public enum AC_HOLDER_PHONE_TYPE
  {
    MOBILE_PHONE = 1,
    HOME_PHONE = 2
  }

  public enum AC_HOLDER_GENDER
  {
    MALE = 1,
    FEMALE = 2
  }

}
