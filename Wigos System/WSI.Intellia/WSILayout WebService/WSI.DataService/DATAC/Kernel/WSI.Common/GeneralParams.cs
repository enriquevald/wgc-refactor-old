using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Threading;
using System.Diagnostics;

namespace WSI.Common
{
  public static class GeneralParam
  {
    private const Int32 REFRESH_PERIOD = 2 * 60 * 1000; // 2 minutes
    private const Int32 ERROR_WAIT_HINT = 10 * 1000;    // 10 seconds

    private static ManualResetEvent m_ev_values_available = new ManualResetEvent(false);
    private static Dictionary<String, String> m_params = new Dictionary<string, string>();
    private static Boolean m_init = GeneralParam.Init();
    private static Int64 m_num_read = 0;

    //------------------------------------------------------------------------------
    // PURPOSE : Creates the auto-refresh thread
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES : This function is "automatically" called on the first call to GeneralParam
    //
    private static Boolean Init()
    {
      Thread _thread = null;

      // AJQ 16-APR-2012, Avoid waiting for the event in design mode (the DB is not available)
      if (!String.IsNullOrEmpty(Environment.GetEnvironmentVariable("LKS_VC_DEV")))
      {
        if (System.Diagnostics.Process.GetCurrentProcess().ProcessName == "devenv")
        {
          m_ev_values_available.Set();

          return true;
        }
      }

      _thread = new Thread(new ThreadStart(GeneralParamsThread));
      _thread.Start();

      return true;
    } // Init

    //------------------------------------------------------------------------------
    // PURPOSE : Automatic reload of the General Params
    //
    //  PARAMS :
    //      - INPUT :
    //          - GroupKey
    //          - SubjectKey
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //        The value of the selected General Param
    //
    //   NOTES :
    //
    private static void GeneralParamsThread ()
    {
      int _period_wait;

      while (true)
      {
        try
        {
          // Ensure DB is initialized
          while (!WASDB.Initialized)
          {
            Thread.Sleep(250);
          }

          _period_wait = REFRESH_PERIOD;

          using (DB_TRX _db_trx = new DB_TRX())
          {
            using (SqlCommand _sql_cmd = new SqlCommand("SELECT GP_GROUP_KEY, GP_SUBJECT_KEY, ISNULL(GP_KEY_VALUE, '') FROM GENERAL_PARAMS"))
            {
              using (SqlDataReader _reader = _db_trx.ExecuteReader (_sql_cmd))
              {
                if (_reader != null)
                {
                  lock (m_params)
                  {
                    // Remove all the parameters & add the just read ones.
                    m_params.Clear();
                    while (_reader.Read())
                    {
                      m_params.Add(_reader.GetString(0) + "." + _reader.GetString(1), _reader.GetString(2));
                    }
                  }
                  // Notify 'Parameters Available'
                  m_ev_values_available.Set();
                }
                else
                { 
                  // Wait for that the connection is ready
                  _period_wait = ERROR_WAIT_HINT;
                }
              }
            }
          }
          Thread.Sleep(_period_wait); 
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
          Thread.Sleep(ERROR_WAIT_HINT); 
        }
      }

    } // GeneralParamsRefreshThread

    //------------------------------------------------------------------------------
    // PURPOSE : Return the value from the General Params (in memory)
    //
    //  PARAMS :
    //      - INPUT :
    //          - GroupKey
    //          - SubjectKey
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //        The value of the selected General Param
    //
    //   NOTES :
    //
    public static String Value(String GroupKey, String SubjectKey)
    {
      String _key;
      String _value;

      // Wait till the values have been retrieved from the DB
      m_ev_values_available.WaitOne();

      // Build the Key
      _key = GroupKey + "." + SubjectKey;

      // Get the value
      lock (m_params)
      {
        if (!m_params.TryGetValue(_key, out _value))
        {
          _value = String.Empty;
        }
      }

      Interlocked.Increment(ref m_num_read);

      return _value;
    } // Value

    public static Int32 GetInt32(String GroupKey, String SubjectKey, Int32 DefaultValue)
    {
      String _str_value;

      Int32 _value;

      _str_value = GeneralParam.Value(GroupKey, SubjectKey);

      if (String.IsNullOrEmpty(_str_value))
      {
        return DefaultValue;
      }

      if (!Int32.TryParse(_str_value, out _value))
      {
        return DefaultValue;
      }

      return _value;
    }

    public static Int32 GetInt32(String GroupKey, String SubjectKey)
    {
      return GetInt32(GroupKey, SubjectKey, 0);
    }


    public static Boolean GetBoolean(String GroupKey, String SubjectKey, Boolean DefaultValue)
    {
      return GetInt32(GroupKey, SubjectKey, DefaultValue ? 1 : 0) != 0;
    }

    public static Boolean GetBoolean(String GroupKey, String SubjectKey)
    {
      return GetBoolean(GroupKey, SubjectKey, false);
    }


    public static Decimal GetDecimal(String GroupKey, String SubjectKey, Decimal DefaultValue)
    {
      String _str_value;
      Decimal _value;

      _str_value = GeneralParam.Value(GroupKey, SubjectKey);

      if (String.IsNullOrEmpty(_str_value))
      {
        return DefaultValue;
      }

      if (!Decimal.TryParse(_str_value, 
                            System.Globalization.NumberStyles.Any, 
                            System.Globalization.CultureInfo.GetCultureInfo("en-US").NumberFormat, 
                            out _value))
      {
        return DefaultValue;
      }

      return _value;
    }

    public static Decimal GetDecimal(String GroupKey, String SubjectKey)
    {
      return GetDecimal(GroupKey, SubjectKey, 0);
    }

    public static Currency GetCurrency(String GroupKey, String SubjectKey, Currency DefaultValue)
    {
      return GetDecimal(GroupKey, SubjectKey, DefaultValue);
    }

    public static Currency GetCurrency(String GroupKey, String SubjectKey)
    {
      return GetDecimal(GroupKey, SubjectKey, 0);
    }


    public static String GetString(String GroupKey, String SubjectKey)
    {
      //return Value(GroupKey, SubjectKey);
      return GetString(GroupKey, SubjectKey, String.Empty);
    }

    public static String GetString(String GroupKey, String SujectKey, String DefaultValue)
    {
      String _value;
      _value = GeneralParam.Value(GroupKey, SujectKey);

      if (String.IsNullOrEmpty(_value))
      {
        _value = DefaultValue;
      }

      return _value;
    }

    public static Int64 GetInt64(String GroupKey, String SubjectKey, Int32 DefaultValue)
    {
      String _str_value;
      Int64 _value;

      _str_value = GeneralParam.Value(GroupKey, SubjectKey);

      if (String.IsNullOrEmpty(_str_value))
      {
        return DefaultValue;
      }

      if (!Int64.TryParse(_str_value, out _value))
      {
        return DefaultValue;
      }

      return _value;
    }

    public static Int64 GetInt64(String GroupKey, String SubjectKey)
    {
      return GetInt64(GroupKey, SubjectKey, 0);
    }
  }
}
