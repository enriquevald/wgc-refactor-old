﻿using System;
using System.Collections.Generic;
// using System.Linq;
using System.Text;
using System.Data;

namespace WSI.Common
{
  public enum AlarmSourceCode
  {
    NotSet = 0,
    User = 1,
    Service = 2,
    System = 3,

    WebService = 4,           //WebService
    ReceivedData = 5,         //Verify Data from client
    XMLGenerate = 6,          //XML Generate
    SATResponse = 7           //SAT RESPONSES
  }

  public enum AlarmCode
  {
    Unknown = 0x00000000,  // Warn

    // SERVICES
    // - Start/Stop
    Service_Started = 0x00030001,  // Info
    Service_Stopped = 0x00030002,  // Error
    Service_Stopping = 0x00030003,  // Warn
    Service_Running = 0x00030004,  // Info
    Service_StandBy = 0x00030005,  // Info

    // - License
    Service_LicenseExpired = 0x00030011, // Error
    Service_LicenseWillExpireSoon = 0x00030012, // Warn
    // - Software update
    Service_NewVersionDownloaded = 0x00030021,  // Info
    Service_NewVersionAvailable = 0x00030022,  // Info

    // USER
    // - Login
    User_MaxLoginAttemps = 0x00040001,
    //-Closing Cashier
    User_WrongDelivered = 0x00040002,

    // SYSTEM
    // Summary:
    //     A change in the power status of the computer is detected, such as a switch
    //     from battery power to A/C. The system also broadcasts this event when remaining
    //     battery power slips below the threshold specified by the user or if the battery
    //     power changes by a specified percentage.
    SystemPowerStatus_PowerStatusChange = 0x00050001,
    // Summary:
    //     The system has resumed operation after a critical suspension caused by a
    //     failing battery.
    SystemPowerStatus_ResumeCritical = 0x00050002,
    //
    // Summary:
    //     Battery power is low.
    SystemPowerStatus_BatteryLow = 0x00050003,

    //WebServiceXXXXXXX = 0x00060001
    WebService_Transaction_Begin = 0x00060001,
    WebService_Transaction_Error = 0x00060002,
    WebService_Transaction_End = 0x00060003,

    WebService_Session_Begin = 0x00060004,
    WebService_Session_TimeOut = 0x00060005,            //DATA_RC_SESSION_TIMEOUT
    WebService_Session_End = 0x00060006,                //DATA_RC_OK
    WebService_Session_BadSeqID = 0x00060007,           //DATA_RC_BAD_SEQID,
    WebService_Session_BadMessageType = 0x00060008,     //DATA_RC_BAD_MESSAGE_TYPE,
    WebService_Session_BadChecksum = 0x00060009,        //DATA_RC_BAD_CHECKSUM,
    WebService_Session_IncoherenceWithDBData = 0x00060010,    //DATA_RC_INCOHERENCE_WITH_DB_DATA,
    WebService_Session_InternalError = 0x00060011,            //DATA_RC_INTERNAL_DATA_ERROR,
    WebService_Session_BadLogin = 0x00060012,                  //DATA_RC_BAD_LOGIN,
    WebService_Session_TooManyRetrys = 0x00060013,            //DATA_RC_TOO_MANY_RETRYS,
    WebService_Session_Kill_TimeOut = 0x00060014,
    WebService_Session_Remove = 0x00060015,
    WebService_Session_Default = 0x00060016,
    WebService_Session_BadTransactionID = 0x00060017,           

    //ReceivedDataXXXXXXXX = 0x00070001,
    ReceivedData_BadMessageData = 0x00070001,          //DATA_RC_BAD_MESSAGE_DATA


    //XMLGenerateXXXXXXXXX = 0x00080001,
    XMLGenerate_BadXmlData = 0x00080001,

    //SATResponseXXXXXXXXX = 0x00090001           
    SAT_Error_Getting_Token = 0x00090001,
  }

  public enum AlarmSeverity
  {
    Info = 1,
    Warning = 2,
    Error = 3
  }

  // JML 15-OCT-2012
  // RXM 23-OCT-2012
  public enum REPORT_STATUS
  {
    NO_STATUS = -1,
    NOT_RECEIVED = 0,
    RECEIVED = 1,
    VALIDATED_TO_GENERATE = 2,
    GENERATED_OK = 3,
    GENERATED_WARNING = 4,
    VALIDATED_TO_SEND = 5,
    SENT_SAT = 6,
    ACCEPTED_SAT = 7,
    REJECTED_SAT = 8,
    ERROR_GENERATING = 9,
    ERROR_SENDING = 10,
    VALIDATED_TO_SEND_MANUALLY = 11,
    RECEIVED_WITH_ERRORS = 12
  }

  //JCA 01-FEB-2013
  public enum SUBLINE_RECEIVED_BY
  {
    NOT_RECEIVED = 0,
    LINE = 1,
    SUBLINE = 2,
  }
  // JML 18-OCT-2012
  public enum TIPO_XML
  {
    CATALOGO = 1,
    REPORTE = 2,
    CONSTANCIA = 3
  }

  // JML 18-OCT-2012
  public enum TIPO_ENVIO
  {   
   NORMAL = 'N',
   SUSTITUTIVO = 'S'
  }


  public enum ENUM_TSV_PACKET
  {
    TSV_PACKET_WIN_CASHIER = 0,
    TSV_PACKET_WIN_KIOSK = 1,
    TSV_PACKET_WIN_GUI = 2,

    TSV_PACKET_SAS_HOST = 5,
    TSV_PACKET_SITE = 100,
    TSV_PACKET_SITE_JACKPOT = 101,
    TSV_PACKET_MOBILE_BANK = 102,
    TSV_PACKET_MOBILE_BANK_IMB = 103,
    TSV_PACKET_ISTATS = 104,
    TSV_PACKET_PROMOBOX = 105,
    //
    // Services
    //
    TSV_PACKET_WCP_SERVICE = 3,
    TSV_PACKET_WC2_SERVICE = 4,
    TSV_PACKET_WWP_SITE_SERVICE = 6,
    TSV_PACKET_WWP_CENTER_SERVICE = 7,
    TSV_PACKET_WXP_SERVICE = 8,
    //
    // Bigaboom
    //
    TSV_PACKET_BGJOBS_SERVICE = 10,
    //
    // PSA
    //
    TSV_PACKET_WAS_SERVICE = 20,
    // TSV_PACKET_WIN_AS_GUI = 21, se usa el TSV_PACKET_WIN_GUI=2 para todos los GUI de distintos proyectos.
    TSV_PACKET_PSA_SERVICE = 21,
    TSV_PACKET_PSA_CLIENT = 22,

    //DATA SERVICE
    //TODO NEW ENUM TO DATA SERVICE
    TSV_PACKET_SMARTFLOOR_DATA_SERVICE = 23,

    TSV_PACKET_NOT_SET = -1
  }

  // QMP 12-NOV-2012
  public enum MONITORING_STATUS
  {
    NOT_RECEIVED = 0,
    RECEIVED = 1
  }

  public enum MONITORING_TYPE
  {
    SESSION = 1,
    STATISTICS = 2
  }

  public enum SEND_STATUS_SAT
  {
    NoStatus = -1,
    NoEnviado = 0,
    Recibido = 1,
    Aceptado = 2,
    AceptadoEnTiempo = 3,
    AceptadoExtemporaneo = 4,
    Rechazado = 5
  }

  // QMP 19-NOV-2012
  public enum GAME_SESSION_STATUS
  {
    OPEN = 0,
    CLOSED = 1,
    ABANDONED = 2,
    MANUAL_CLOSED = 3,
    CANCELLED = 4,
    HANDPAY = 5,
    HANDPAY_CANCEL = 6
  }

  public enum CATALOG_LIFECYCLE_STATUS
  {
    NO_STATUS = -1,
    PENDING_NEW_CHANGES = 0,
    VALIDATED_TO_GENERATE = 1,
    GENERATED_OK = 2,
    GENERATED_WARNING = 3,
    ERROR_GENERATING = 4,
    VALIDATED_TO_SEND = 5,
    SENT_SAT = 6,
    ERROR_SENDING = 7,
    ACCEPTED_SAT = 8,
    REJECTED_SAT = 9,
    VALIDATED_TO_SEND_MANUALLY = 10,
  }

  //JCA 14-JAN-2013
  public enum DATA_WS_NET_MSG_LOG
  {
    NO_LOG = 0,
    ALL = 1,
    BY_LOGIN = 2,
    BY_LINE = 3
  }

  // JML 11-FEB-2013
  public enum CONSTANCIA_ERRORES
  { 
    NONE = 0,
    RFC = 1,
    CURP = 2,
    POSTALCODE = 4
  }

  // JCA 08-APR-2013
  public enum ALESIS_STATUS_SEND
  {
    NONE = 0,
    SUCCES = 1,
    ERROR_SEND = 2,
  }

}
