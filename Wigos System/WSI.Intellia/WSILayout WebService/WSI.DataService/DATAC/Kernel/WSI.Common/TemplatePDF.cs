//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: TemplatePDF.cs
// 
//   DESCRIPTION: Handle a PDF file 
// 
//        AUTHOR: Marcos Piedra
// 
// CREATION DATE: 31-JAN-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 31-JAN-2012 MPO    First release.
// 02-MAY-2012 MPO    New feature, set an image and it filled into PDF.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using pdf = iTextSharp.text.pdf;
using System.Collections;
using System.IO;
using WSI.Common;

namespace WSI.Common
{
  public class TemplatePDF : Template
  {

    #region Members

    String m_current_pdf;

    #endregion // Members

    #region Constructor

    //------------------------------------------------------------------------------
    // PURPOSE: Constructor. Initialize properties
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public TemplatePDF(String TemplatePDF)
    {
      FileStream _file_stream;
      Stream _stream;

      _file_stream = new FileStream(TemplatePDF, FileMode.Open, FileAccess.Read);
      _stream = CopyToMemory(_file_stream);
      SetMemoryStreamTemplate(_stream);
      _file_stream.Close();

      m_current_pdf = "";

    } //TemplatePDF

    //------------------------------------------------------------------------------
    // PURPOSE: Constructor. Initialize properties
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public TemplatePDF(MemoryStream TemplatePDF)
    {

      SetMemoryStreamTemplate(TemplatePDF);
      m_current_pdf = "";

    } //TemplatePDF

    #endregion // Constructor

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Show a PDF file using the default application to open PDF files
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public override void Show()
    {

      CheckCurrentPdf();

      if (String.IsNullOrEmpty(m_current_pdf))
      {

        return;
      }

      System.Diagnostics.Process _process;

      _process = new System.Diagnostics.Process();
      _process.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Maximized;
      _process.StartInfo.FileName = m_current_pdf;
      _process.Start();

    } // Show

    //------------------------------------------------------------------------------
    // PURPOSE: Print a PDF file using the default application to open PDF files.
    //          It waits for print and close the application.
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public override void Print()
    {

      CheckCurrentPdf();

      if (String.IsNullOrEmpty(m_current_pdf))
      {

        return;
      }

      System.Diagnostics.ProcessStartInfo _start_info = new System.Diagnostics.ProcessStartInfo();
      _start_info.Verb = "Print";
      _start_info.FileName = m_current_pdf;
      _start_info.CreateNoWindow = true;
      _start_info.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;

      System.Diagnostics.Process _process = new System.Diagnostics.Process();
      _process.StartInfo = _start_info;

      _process.Start();

      // Wait for the process to start.
      System.Threading.Thread.Sleep(1000);

      if (!_process.HasExited)
      {
        _process.WaitForInputIdle();
        _process.CloseMainWindow();
        _process.Close();
      }

    } // Print

    //------------------------------------------------------------------------------
    // PURPOSE: Save the PDF file
    //
    //  PARAMS:
    //      - INPUT:
    //          - FileName: File where the PDF document is saved
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public override void SaveAs(String FileName)
    {

      WSI.Common.Template.Save(m_stream_target, FileName);
      m_current_pdf = FileName;
      m_changed = false;

    } // SaveAs

    //------------------------------------------------------------------------------
    // PURPOSE: Filling the template PDF that contains the fields and the images
    //
    //  PARAMS:
    //      - INPUT:
    //          - FileName: Template PDF that contains the fields
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //          - Boolean: Is correct the process of filling?
    // 
    //   NOTES:
    // 
    public override Boolean Fill()
    {

      pdf.AcroFields _acro_fields;
      pdf.PdfReader _reader;
      pdf.PdfStamper _stamper;
      pdf.PdfContentByte _byte;
      iTextSharp.text.Image _itext_image;

      _reader = null;
      _stamper = null;

      try
      {

        m_stream_template.Position = 0;
        _reader = new pdf.PdfReader(m_stream_template);

        //if (m_stream_target != null)
        //  m_stream_target.Close();

        m_stream_target = new MemoryStream();
        _stamper = new pdf.PdfStamper(_reader, m_stream_target);

        _acro_fields = _stamper.AcroFields;

        foreach (String _key in m_fields.Keys)
        {
          _acro_fields.SetField(_key, m_fields[_key]);
        }

        _byte = _stamper.GetOverContent(1);

        foreach (ImageInfo _image_info in m_images)
        {
          _itext_image = iTextSharp.text.Image.GetInstance(_image_info.BytesImg);
          _itext_image.ScaleToFit(_image_info.Rect.Width, _image_info.Rect.Height);
          //_itext_image.ScaleAbsolute(_image_info.Rect.Width, _image_info.Rect.Height);
          _itext_image.SetAbsolutePosition(_image_info.Rect.X + (_image_info.Rect.Width - _itext_image.ScaledWidth) / 2, _image_info.Rect.Y + (_image_info.Rect.Height - _itext_image.ScaledHeight) / 2);
          _byte.AddImage(_itext_image);
        }

        _stamper.FormFlattening = true;
        _stamper.Writer.CloseStream = false;

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
      finally
      {

        if (_stamper != null)
        {
          _stamper.Close();
        }

        if (_reader != null)
        {
          _reader.Close();
        }

      }

    } // FillField

    //------------------------------------------------------------------------------
    // PURPOSE: Get a list of field name of PDF
    //
    //  PARAMS:
    //      - INPUT:
    //          - FileName: Path of PDF to get field name
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //          - List<String>: List of field name
    // 
    //   NOTES:
    //    
    public override List<String> Fields()
    {
      List<String> _list;
      pdf.PdfReader _reader;

      _reader = null;

      try
      {
        m_stream_template.Position = 0;
        _reader = new pdf.PdfReader(m_stream_template);
        _list = new List<String>();

        foreach (DictionaryEntry _de in _reader.AcroFields.Fields)
        {
          _list.Add((String)_de.Key);
        }

        return _list;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return null;
      }
      finally
      {
        if (_reader != null)
        {
          _reader.Close();
        }

      }
    } // GetListOfField

    //------------------------------------------------------------------------------
    // PURPOSE: Get the current stream that contain the template has filled.
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //          - Stream:
    // 
    //   NOTES:
    // 
    public override Stream GetStream()
    {
      Boolean _is_filled;
      MemoryStream _copied;

      _is_filled = false;
      _copied = null;

      if (m_changed)
      {
        _is_filled = Fill();
      }

      if (_is_filled)
      {
        _copied = CopyToMemory(m_stream_target);
      }

      return _copied;
     
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Close the corresponding objects
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public override void Close()
    {

      if (m_stream_target != null) m_stream_target.Close();
      if (m_stream_template != null) m_stream_template.Close();

    } // Close

    //------------------------------------------------------------------------------
    // PURPOSE: Check if exists the library "itextsharp.dll" and 
    //          an application that open the PDF document
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //          - Boolean: true if check is correct
    // 
    //   NOTES:
    //    
    static public Boolean ApplicationAvailable()
    {
      String _path_itext;

      _path_itext = System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath);
      _path_itext = System.IO.Path.Combine(_path_itext, "itextsharp.dll");

      if (!System.IO.File.Exists(_path_itext))
      {

        return false;
      }

      try
      {

        if (!IsAssociated(".pdf"))
        {

          return false;
        }

        return true;
      }
      catch
      {

        return false;
      }

    } // ApplicationAvailable

    #endregion // Public Methods

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Check the current path that target pdf file
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    private void CheckCurrentPdf()
    {

      if (m_changed)
      {

        Fill();
        m_current_pdf = Path.Combine(Path.GetTempPath(), Path.GetTempFileName() + ".pdf");
        Template.Save(m_stream_target, m_current_pdf);
        m_changed = false;

      }

    }

    //------------------------------------------------------------------------------
    // PURPOSE: It is registered an extension of file in the Win32 System for an application
    //
    //  PARAMS:
    //      - INPUT:
    //          - Extension: The extension
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //          - Boolean: Is registered
    // 
    //   NOTES:
    // 
    static private Boolean IsAssociated(String Extension)
    {

      try
      {

        return (Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(Extension, false) != null);
      }
      catch (Exception)
      {

        return false;
      }

    }

    #endregion

  } // TemplatePDF
} // Wrapper
