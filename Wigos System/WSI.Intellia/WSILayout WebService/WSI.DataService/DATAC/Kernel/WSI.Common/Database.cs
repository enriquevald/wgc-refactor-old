//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Database.cs
// 
//   DESCRIPTION: The database
// 
//        AUTHOR: Andreu Juli�
// 
// CREATION DATE: 12-MAR-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 12-MAR-2007 AJQ    First release.
// 23-NOV-2011 RCI    Added type SqlDbType.DateTime in method SqlParameterType(). 
// 28-MAR-2012 MPO    User session: New feature in ApplicationVersionThread.
// 04-AUG-2016 FAV    Bug 16398: Error inserting values in APPLICATIONS table
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace WSI.Common
{
  public delegate bool DbFunction(SqlTransaction Trx);

  public struct Win32SystemTime
  {
    public ushort Year;
    public ushort Month;
    public ushort DayOfWeek;
    public ushort Day;
    public ushort Hour;
    public ushort Minute;
    public ushort Second;
    public ushort Millisecond;

    [DllImport("kernel32.dll", EntryPoint = "SetLocalTime", SetLastError = true)]
    public extern static bool SetLocalTime(ref Win32SystemTime LocalTime);
    [DllImport("kernel32.dll", EntryPoint = "SetSystemTime", SetLastError = true)]
    public extern static bool SetSystemTime(ref Win32SystemTime SystemTime);
    [DllImport("kernel32.dll", EntryPoint = "GetLocalTime", SetLastError = true)]
    public extern static void GetLocalTime(ref Win32SystemTime LocalTime);
    [DllImport("kernel32.dll", EntryPoint = "GetSystemTime", SetLastError = true)]
    public extern static void GetSystemTime(ref Win32SystemTime SystemTime);

    private void Init(DateTime Value)
    {
      this.Year = (ushort)Value.Year;
      this.Month = (ushort)Value.Month;
      this.DayOfWeek = (ushort)Value.DayOfWeek;
      this.Day = (ushort)Value.Day;
      this.Hour = (ushort)Value.Hour;
      this.Minute = (ushort)Value.Minute;
      this.Second = (ushort)Value.Second;
      this.Millisecond = (ushort)Value.Millisecond;
    }

    public static Win32SystemTime Create(DateTime Value)
    {
      Win32SystemTime _item;

      _item = new Win32SystemTime();
      _item.Init(Value);

      return _item;
    }
  };


  public class DB_TRX : IDisposable
  {
    private SqlConnection m_sql_conn = null;
    private SqlTransaction m_sql_trx = null;

    public DB_TRX()
    {
      try
      {
        m_sql_conn = WASDB.Connection();
        if (m_sql_conn == null)
        {
          return;
        }
        if (m_sql_conn.State != ConnectionState.Open)
        {
          return;
        }
        m_sql_trx = m_sql_conn.BeginTransaction();
      }
      catch
      {
        Dispose();
      }
    }

    public SqlTransaction SqlTransaction
    {
      get
      {
        return m_sql_trx;
      }
    }

    public Int32 ExecuteNonQuery(SqlCommand SqlCmd)
    {
      try
      {
        SqlCmd.Connection = m_sql_conn;
        SqlCmd.Transaction = m_sql_trx;

        return SqlCmd.ExecuteNonQuery();
      }
      finally
      {
        SqlCmd.Connection = null;
        SqlCmd.Transaction = null;
      }
    }

    public Object ExecuteScalar(SqlCommand SqlCmd)
    {
      try
      {
        SqlCmd.Connection = m_sql_conn;
        SqlCmd.Transaction = m_sql_trx;

        return SqlCmd.ExecuteScalar();
      }
      finally
      {
        SqlCmd.Connection = null;
        SqlCmd.Transaction = null;
      }
    }

    public SqlDataReader ExecuteReader(SqlCommand SqlCmd)
    {
      try
      {
        if (m_sql_conn.State == ConnectionState.Open)
        {
          SqlCmd.Connection = m_sql_conn;
          SqlCmd.Transaction = m_sql_trx;

          return SqlCmd.ExecuteReader();
        }

        return null;
      }
      finally
      {
        SqlCmd.Connection = null;
        SqlCmd.Transaction = null;
      }
    }

    public int Fill(SqlDataAdapter SqlDA, Object Object)
    {
      try
      {
        if (SqlDA.SelectCommand != null)
        {
          SqlDA.SelectCommand.Connection = m_sql_conn;
          SqlDA.SelectCommand.Transaction = m_sql_trx;
        }

        if (Object is DataTable)
        {
          return SqlDA.Fill((DataTable)Object);
        }
        else if (Object is DataSet)
        {
          return SqlDA.Fill((DataSet)Object);
        }
        else
        {
          throw new Exception("DataTable or DataSet expected.");
        }
      }
      finally
      {
        if (SqlDA.SelectCommand != null)
        {
          SqlDA.SelectCommand.Connection = null;
          SqlDA.SelectCommand.Transaction = null;
        }
      }
    }

    public int Update(SqlDataAdapter SqlDA, Object Object)
    {
      try
      {
        if (SqlDA.SelectCommand != null)
        {
          SqlDA.SelectCommand.Connection = m_sql_conn;
          SqlDA.SelectCommand.Transaction = m_sql_trx;
        }
        if (SqlDA.InsertCommand != null)
        {
          SqlDA.InsertCommand.Connection = m_sql_conn;
          SqlDA.InsertCommand.Transaction = m_sql_trx;
        }
        if (SqlDA.UpdateCommand != null)
        {
          SqlDA.UpdateCommand.Connection = m_sql_conn;
          SqlDA.UpdateCommand.Transaction = m_sql_trx;
        }
        if (SqlDA.DeleteCommand != null)
        {
          SqlDA.DeleteCommand.Connection = m_sql_conn;
          SqlDA.DeleteCommand.Transaction = m_sql_trx;
        }

        if (Object is DataRow[])
        {
          return SqlDA.Update((DataRow[])Object);
        }
        else if (Object is DataSet)
        {
          return SqlDA.Update((DataSet)Object);
        }
        else if (Object is DataTable)
        {
          return SqlDA.Update((DataTable)Object);
        }
        else
        {
          throw new Exception("Unexpected object type:" + Object.GetType().ToString());
        }
      }
      finally
      {
        if (SqlDA.SelectCommand != null)
        {
          SqlDA.SelectCommand.Connection = null;
          SqlDA.SelectCommand.Transaction = null;
        }
        if (SqlDA.InsertCommand != null)
        {
          SqlDA.InsertCommand.Connection = null;
          SqlDA.InsertCommand.Transaction = null;
        }
        if (SqlDA.UpdateCommand != null)
        {
          SqlDA.UpdateCommand.Connection = null;
          SqlDA.UpdateCommand.Transaction = null;
        }
        if (SqlDA.DeleteCommand != null)
        {
          SqlDA.DeleteCommand.Connection = null;
          SqlDA.DeleteCommand.Transaction = null;
        }
      }
    }

    public Boolean Commit()
    {
      if (m_sql_trx != null)
      {
        if (m_sql_trx.Connection != null)
        {
          try
          {
            m_sql_trx.Commit();

            return true;
          }
          catch { }
        }
        else
        {
          Log.Error("DB_TRX has been already commited!");
        }
      }
      return false;
    }

    public void Rollback()
    {
      if (m_sql_trx != null)
      {
        if (m_sql_trx.Connection != null)
        {
          try { m_sql_trx.Rollback(); }
          catch { }
        }
      }
    }

    #region IDisposable Members

    public void Dispose()
    {
      if (m_sql_trx != null)
      {
        Rollback();
        m_sql_trx.Dispose();
        m_sql_trx = null;
      }
      if (m_sql_conn != null)
      {
        try { m_sql_conn.Close(); }
        catch { }
        m_sql_conn.Dispose();
        m_sql_conn = null;
      }
    }

    #endregion
  }

  public static class WASDB
  {

    #region Class Atributes

    private static Int32 expected_db_version = 100;
    private static Int32 found_db_version = 0;
    private static TimeSpan m_timespan = new TimeSpan();

    private static String app_name = "UNKNOWN";
    private static String app_version = "00.000";
    private static String login_name = "<None>";

    private static Int32 m_times_to_update = 0;

    private static int client_id;
    private static String m_connection_string;
    private static String server_1;
    private static String server_2;
    private static DataSet ds;

    public delegate void StatusChangedHandler();
    public static event StatusChangedHandler OnStatusChanged;

    private static String m_data_source = "";
    private static ConnectionState m_conn_state = ConnectionState.Connecting;
    private static Boolean db_initialized = false;
    private static String db_catalog = "";
    static ReaderWriterLock db_lock;
    private static String m_db_version = "00.000.000";

    #endregion

    #region Properties

    public static DateTime Now
    {
      get { return DateTime.Now.Subtract(m_timespan); }
    }

    public static ConnectionState ConnectionState
    {
      get { return WASDB.m_conn_state; }
    }

    public static String DataSource
    {
      get { return WASDB.m_data_source; }
    }

    /// <summary>
    /// Return database name (catalog)
    /// </summary>
    public static String DatabaseCatalog
    {
      get
      {
        return db_catalog;
      }

      set
      {
        db_catalog = value;
      }
    }

    /// <summary>
    /// Return client id. from database table.
    /// </summary>
    static public int ClientId
    {
      get
      {
        DataTable db_version;
        db_version = ds.Tables[0];

        return (int)db_version.Rows[0]["db_client_id"];
      }
    } // ClientId

    //------------------------------------------------------------------------------
    // PURPOSE : Get database version 
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    static public String GetDatabaseVersion()
    {
      SqlConnection _sql_conn;
      String _sql_select;
      SqlDataAdapter _da;
      DataSet _ds;
      String _3gs_version;
      String _db_version;

      _da = null;
      _ds = null;
      _sql_conn = null;
      _3gs_version = "";
      _db_version = "";

      // Prepare query
      _sql_select = "";
      _sql_select += "SELECT * FROM DB_VERSION_INTERFACE_3GS ORDER BY DB_UPDATED DESC ";

      try
      {
        _sql_conn = Connection();

        _ds = new DataSet();
        _da = new SqlDataAdapter(_sql_select, _sql_conn);
        _da.Fill(_ds);

        _3gs_version = ((Int32)_ds.Tables[0].Rows[0]["DB_RELEASE_ID"]).ToString("000");
      }
      catch
      {
        _3gs_version = "000";
      }
      finally
      {
        if (_sql_conn != null)
        {
          _sql_conn.Close();
          _sql_conn = null;
        }
      }

      // Prepare query
      _sql_select = "";
      _sql_select += "SELECT * FROM DB_VERSION ORDER BY DB_UPDATED DESC ";

      try
      {
        _sql_conn = Connection();

        _ds = new DataSet();
        _da = new SqlDataAdapter(_sql_select, _sql_conn);
        _da.Fill(_ds);

        _db_version = ((Int32)_ds.Tables[0].Rows[0]["DB_CLIENT_ID"]).ToString() + "."
                    + ((Int32)_ds.Tables[0].Rows[0]["DB_RELEASE_ID"]).ToString("000") + "."
                    + _3gs_version;
      }
      catch
      {
        _db_version = m_db_version;
      }
      finally
      {
        if (_sql_conn != null)
        {
          _sql_conn.Close();
          _sql_conn = null;
        }
      }

      m_db_version = _db_version;

      return _db_version;

    } // GetDatabaseVersion

    /// <summary>
    /// Return Common build id
    /// </summary>
    static public int CommonBuildId
    {
      get
      {
        return found_db_version;
      }
    } // CommonBuildId

    /// <summary>
    /// Return initialization flag: connection could be made.
    /// </summary>
    public static Boolean Initialized
    {
      get { return WASDB.db_initialized; }
    }

    #endregion

    #region Private Methods

    #endregion

    #region Public Methods

    /// <summary>
    /// Database initial actions:
    ///   - Connect as WGPUBLIC
    ///   - Obtain current database version.
    ///   - Obtain database users.
    ///   - Validate database version.
    /// </summary>
    /// <param name="ClientId">Client identifier</param>
    /// <param name="DbServer1">Primary database Name/IP address</param>
    /// <param name="DbServer2">Secondary database Name/IP address</param>
    public static void Init(int ClientId, String DbServer1, String DbServer2)
    {
      SqlConnectionStringBuilder csb;
      SqlConnection conn;
      String sql_select;
      SqlDataAdapter da;

      client_id = ClientId;
      server_1 = DbServer1;
      server_2 = DbServer2;

      WASDB.db_initialized = false; 

      ds = new DataSet();
      db_lock = new ReaderWriterLock();
      csb = new SqlConnectionStringBuilder();

      // Prepare connection parameters
      csb.ConnectTimeout = 30;
      csb.DataSource = server_1;
      csb.FailoverPartner = server_2;
      csb.InitialCatalog = "WGDB_" + ClientId.ToString("000");
      csb.IntegratedSecurity = false;
      csb.Pooling = false;
      csb.AsynchronousProcessing = true;

      WASDB.db_catalog = csb.InitialCatalog;
      csb.UserID = "WGPUBLIC_" + ClientId.ToString("000");
      csb.Password = "Wigos_pu_" + ClientId.ToString("000");

      // Prepare query
      sql_select = "";
      sql_select += "SELECT * FROM DB_VERSION ORDER BY db_updated DESC";
      sql_select += "; SELECT * FROM DB_USERS";

      try
      {
        conn = new SqlConnection(csb.ConnectionString);
        conn.Open();

        da = new SqlDataAdapter(sql_select, conn);
        da.Fill(ds);

        if (conn.State == ConnectionState.Open)
        {
          WASDB.db_initialized = true;
        }
        conn.Close();
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
      finally
      {
      }

      // Check database connection
      if (!WASDB.db_initialized)
      {
        return;
      }
    } // Init

    /// <summary>
    /// 
    /// </summary>
    /// <param name="AppName"></param>
    /// <param name="AppVersion"></param>
    public static void SetApplication(String AppName, String AppVersion)
    {
      app_name = AppName;
      app_version = AppVersion;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="UserName"></param>
    public static void SetUserLoggedIn(Int32 UserId, String UserName, String ComputerName)
    {
      login_name = UserName;
      if (app_name == "WigosGUI")
      {
        Users.SetUserLoggedIn(UserId, UserName, ComputerName);
      }
    }
    /// <summary>
    /// 
    /// </summary>
    public static void SetUserLoggedOff()
    {
      login_name = "<None>";
      if (app_name == "WigosGUI")
      {
        Users.SetUserLoggedOff(Users.EXIT_CODE.LOGGED_OUT);
      }
    }

    /// <summary>
    /// Database Key.
    /// </summary>
    /// <param name="ClientId"></param>
    /// <returns></returns>
    private static Byte[] Key(int ClientId)
    {
      String str_key;
      Byte[] key;
      int idx;

      str_key = "WGDBKEY_" + ClientId.ToString("00000000");

      key = Encoding.UTF8.GetBytes(str_key);

      for (idx = 0; idx < key.Length; idx++)
      {
        key[idx] = (Byte)(key[idx] + idx);
      }

      return key;
    } // Key

    /// <summary>
    /// 
    /// </summary>
    /// <param name="ClientId"></param>
    /// <returns></returns>
    private static Byte[] VI(int ClientId)
    {
      String str_vi;
      Byte[] vi;
      int idx;

      str_vi = ClientId.ToString("00000000");

      vi = Encoding.UTF8.GetBytes(str_vi);

      for (idx = 0; idx < vi.Length; idx++)
      {
        vi[idx] = (Byte)(vi[idx] + idx);
      }

      return vi;
    } // VI

    /// <summary>
    /// 
    /// </summary>
    /// <param name="ClientId"></param>
    /// <param name="CryptedPassword"></param>
    /// <returns></returns>
    private static String DecryptUserPassword(int ClientId, Byte[] CryptedPassword)
    {
      Byte[] key;
      Byte[] vi;
      TripleDES csp;
      Byte[] pwd;
      MemoryStream ms;
      CryptoStream cs;

      key = WASDB.Key(ClientId);
      vi = WASDB.VI(ClientId);

      csp = new TripleDESCryptoServiceProvider();

      ms = new MemoryStream(CryptedPassword, 0, CryptedPassword.Length, false);

      cs = new CryptoStream(ms, csp.CreateDecryptor(key, vi), CryptoStreamMode.Read);

      pwd = new Byte[40];

      cs.Read(pwd, 0, pwd.Length);

      cs.Close();
      ms.Close();

      return Encoding.UTF8.GetString(pwd, 1, pwd[0]);

    } // DecryptUserPassword

    /// <summary>
    /// 
    /// </summary>
    /// <param name="ClientId"></param>
    /// <param name="Password"></param>
    /// <returns></returns>
    private static Byte[] EncryptUserPassword(int ClientId, String Password)
    {
      Byte[] key;
      Byte[] vi;
      TripleDES csp;
      Byte[] pwd;
      MemoryStream ms;
      CryptoStream cs;
      Random rnd;
      Byte[] xpwd;

      pwd = new Byte[39];
      xpwd = new Byte[40];
      rnd = new Random(ClientId);

      // Fill the password with random values
      rnd.NextBytes(pwd);

      // First byte will contain the password length
      pwd[0] = (Byte)Password.Length;

      // String -> Byte
      Encoding.UTF8.GetBytes(Password, 0, Password.Length, pwd, 1);

      key = WASDB.Key(ClientId);
      vi = WASDB.VI(ClientId);

      csp = new TripleDESCryptoServiceProvider();

      ms = new MemoryStream(xpwd, 0, xpwd.Length, true);

      cs = new CryptoStream(ms, csp.CreateEncryptor(key, vi), CryptoStreamMode.Write);

      cs.Write(pwd, 0, pwd.Length);

      cs.Close();
      ms.Close();

      return xpwd;

    } // EncryptUserPassword

    /// <summary>
    /// Changes the passwords of the following logins:
    ///   - wgroot_xxx
    ///   - wggui_xxx
    /// </summary>
    /// <param name="ClientId"></param>
    /// <param name="OldWgRootPwd"></param>
    /// <param name="NewWgRootPwd"></param>
    /// <param name="OldWgGuiPwd"></param>
    /// <param name="NewWgGuiPwd"></param>
    public static void SetPasswords(int ClientId,
                                     String OldWgRootPwd, String NewWgRootPwd,
                                     String OldWgGuiPwd, String NewWgGuiPwd)
    {
      DataRow user;
      DataRow[] row;
      SqlDataAdapter da;
      SqlCommandBuilder cb;
      SqlCommand cmd;
      SqlConnection conn;
      String username;
      SqlTransaction trx;
      SqlConnectionStringBuilder csb;

      username = "";

      if (true) // Using Integrated Security
      {
        try
        {
          // Change Login Root
          csb = new SqlConnectionStringBuilder();
          csb.ConnectTimeout = 30;
          csb.DataSource = server_1;
          //csb.InitialCatalog = "WGDB_" + ClientId.ToString("000");
          csb.IntegratedSecurity = true;

          conn = new SqlConnection(csb.ConnectionString);
          conn.Open();

          username = "wgroot_" + ClientId.ToString("000");
          cmd = new SqlCommand("sp_password '" + OldWgRootPwd + "', '" + NewWgRootPwd + "', '" + username + "'");
          cmd.Connection = conn;
          cmd.ExecuteNonQuery();
          conn.Close();
        }
        catch (Exception ex)
        {
          throw new Exception(username + " password not changed (Server: " + server_1 + "). " + ex.Message);
        }

        if (server_1 != server_2)
        {
          try
          {
            // Change Login Root
            csb = new SqlConnectionStringBuilder();
            csb.ConnectTimeout = 30;
            csb.DataSource = server_2;
            //csb.InitialCatalog = "WGDB_" + ClientId.ToString("000");
            csb.IntegratedSecurity = true;

            conn = new SqlConnection(csb.ConnectionString);
            conn.Open();

            username = "wgroot_" + ClientId.ToString("000");
            cmd = new SqlCommand("sp_password '" + OldWgRootPwd + "', '" + NewWgRootPwd + "', '" + username + "'");
            cmd.Connection = conn;
            cmd.ExecuteNonQuery();
            conn.Close();
          }
          catch (Exception ex)
          {
            throw new Exception(username + " password not changed (Server: " + server_2 + "). " + ex.Message);
          }
        }



        try
        {
          // Change Login GUI
          csb = new SqlConnectionStringBuilder();
          csb.ConnectTimeout = 30;
          csb.DataSource = server_1;
          //csb.InitialCatalog = "WGDB_" + ClientId.ToString("000");
          csb.IntegratedSecurity = true;

          conn = new SqlConnection(csb.ConnectionString);
          conn.Open();

          username = "wggui_" + ClientId.ToString("000");
          cmd = new SqlCommand("sp_password '" + OldWgGuiPwd + "', '" + NewWgGuiPwd + "', '" + username + "'");
          cmd.Connection = conn;
          cmd.ExecuteNonQuery();

          conn.Close();
        }
        catch (Exception ex)
        {
          throw new Exception(username + " password not changed (Server: " + server_1 + "). " + ex.Message);
        }

        if (server_1 != server_2)
        {
          try
          {
            // Change Login GUI
            csb = new SqlConnectionStringBuilder();
            csb.ConnectTimeout = 30;
            csb.DataSource = server_2;
            //csb.InitialCatalog = "WGDB_" + ClientId.ToString("000");
            csb.IntegratedSecurity = true;

            conn = new SqlConnection(csb.ConnectionString);
            conn.Open();

            username = "wggui_" + ClientId.ToString("000");
            cmd = new SqlCommand("sp_password '" + OldWgGuiPwd + "', '" + NewWgGuiPwd + "', '" + username + "'");
            cmd.Connection = conn;
            cmd.ExecuteNonQuery();

            conn.Close();
          }
          catch (Exception ex)
          {
            throw new Exception(username + " password not changed (Server: " + server_2 + "). " + ex.Message);
          }
        }
      }
      //////else
      //////{
      //////  if (OldWgRootPwd != NewWgRootPwd)
      //////  {
      //////    try
      //////    {
      //////      // Change Login Root
      //////      csb = new SqlConnectionStringBuilder();
      //////      csb.ConnectTimeout = 30;
      //////      csb.DataSource = server_1;
      //////      csb.FailoverPartner = server_2;
      //////      csb.InitialCatalog = "WGDB_" + ClientId.ToString("000");
      //////      csb.IntegratedSecurity = false;
      //////      csb.UserID = "wgroot_" + ClientId.ToString("000");
      //////      csb.Password = OldWgRootPwd;

      //////      conn = new SqlConnection(csb.ConnectionString);
      //////      conn.Open();

      //////      username = "wgroot_" + ClientId.ToString("000");
      //////      cmd = new SqlCommand("sp_password '" + OldWgRootPwd + "', '" + NewWgRootPwd + "', '" + username + "'");
      //////      cmd.Connection = conn;
      //////      cmd.ExecuteNonQuery();
      //////      conn.Close();
      //////    }
      //////    catch (Exception ex)
      //////    {
      //////      throw new Exception(username + " password not changed. " + ex.Message);
      //////    }
      //////  }

      //////  if (OldWgGuiPwd != NewWgGuiPwd)
      //////  {
      //////    try
      //////    {
      //////      // Change Login GUI
      //////      csb = new SqlConnectionStringBuilder();
      //////      csb.ConnectTimeout = 30;
      //////      csb.DataSource = server_1;
      //////      csb.FailoverPartner = server_2;
      //////      csb.InitialCatalog = "WGDB_" + ClientId.ToString("000");
      //////      csb.IntegratedSecurity = false;
      //////      csb.UserID = "wggui_" + ClientId.ToString("000");
      //////      csb.Password = OldWgGuiPwd;

      //////      conn = new SqlConnection(csb.ConnectionString);
      //////      conn.Open();

      //////      username = "wggui_" + ClientId.ToString("000");
      //////      cmd = new SqlCommand("sp_password '" + OldWgGuiPwd + "', '" + NewWgGuiPwd + "', '" + username + "'");
      //////      cmd.Connection = conn;
      //////      cmd.ExecuteNonQuery();

      //////      conn.Close();
      //////    }
      //////    catch (Exception ex)
      //////    {
      //////      throw new Exception(username + " password not changed. " + ex.Message);
      //////    }
      //////  }
      //////}


      username = "wgroot_" + ClientId.ToString("000");
      row = ds.Tables[1].Select("du_username = '" + username + "'");
      if (row.Length == 0)
      {
        user = ds.Tables[1].NewRow();
        user["du_username"] = username;
        user["du_password"] = new Byte[40];
        ds.Tables[1].Rows.Add(user);
      }

      username = "wggui_" + ClientId.ToString("000");
      row = ds.Tables[1].Select("du_username = '" + username + "'");
      if (row.Length == 0)
      {
        user = ds.Tables[1].NewRow();
        user["du_username"] = username;
        user["du_password"] = new Byte[40];
        ds.Tables[1].Rows.Add(user);
      }

      username = "wgroot_" + ClientId.ToString("000");
      row = ds.Tables[1].Select("du_username = '" + username + "'");
      if (row.Length == 1)
      {
        user = row[0];
        user["du_password"] = WASDB.EncryptUserPassword(ClientId, NewWgRootPwd); ;
      }

      username = "wggui_" + ClientId.ToString("000");
      row = ds.Tables[1].Select("du_username = '" + username + "'");
      if (row.Length == 1)
      {
        user = row[0];
        user["du_password"] = WASDB.EncryptUserPassword(ClientId, NewWgGuiPwd); ;
      }

      try
      {
        csb = new SqlConnectionStringBuilder();
        csb.ConnectTimeout = 30;
        csb.DataSource = server_1;
        csb.FailoverPartner = server_2;
        csb.InitialCatalog = "WGDB_" + ClientId.ToString("000");
        csb.IntegratedSecurity = false;
        csb.UserID = "wgroot_" + ClientId.ToString("000");
        csb.Password = NewWgRootPwd;

        conn = new SqlConnection(csb.ConnectionString);

        da = new SqlDataAdapter();
        cmd = new SqlCommand("SELECT * FROM DB_USERS");
        cmd.Connection = conn;
        da.SelectCommand = cmd;

        cb = new SqlCommandBuilder(da);
        da.InsertCommand = cb.GetInsertCommand();
        da.UpdateCommand = cb.GetUpdateCommand();

        conn.Open();
        trx = conn.BeginTransaction();

        da.InsertCommand.Connection = conn;
        da.UpdateCommand.Connection = conn;
        da.InsertCommand.Transaction = trx;
        da.UpdateCommand.Transaction = trx;

        da.Update(ds.Tables[1]);

        trx.Commit();
        conn.Close();
      }
      catch (Exception ex)
      {
        throw new Exception("Passwords not updated: " + ex.Message);
      }



    } // SetPasswords


    /// <summary>
    /// 
    /// </summary>
    /// <param name="UserBaseName"></param>
    private static String BuildConnectionString(String UserBaseName)
    {
      String username;
      DataRow[] row;
      SqlConnectionStringBuilder csb;
      Byte[] bin;

      if (ds.Tables.Count != 2)
      {
        Log.Message("Table Users Not Read");

        System.Threading.Thread.Sleep(5000);
        Environment.Exit(0);

        return "";
      }

      username = UserBaseName + "_" + client_id.ToString("000");

      row = ds.Tables[1].Select("du_username = '" + username + "'");

      if (row == null)
      {
        Log.Message("Table Users, User Not Found: " + username);

        System.Threading.Thread.Sleep(5000);
        Environment.Exit(0);

        return "";
      }

      bin = (Byte[])row[0]["du_password"];

      csb = new SqlConnectionStringBuilder();
      csb.ConnectTimeout = 30;
      csb.DataSource = server_1;
      csb.FailoverPartner = server_2;
      csb.InitialCatalog = "WGDB_" + client_id.ToString("000");
      csb.IntegratedSecurity = false;
      csb.UserID = username;
      csb.Password = WASDB.DecryptUserPassword(client_id, bin);
      csb.Pooling = true;
      csb.AsynchronousProcessing = true;
      csb.MaxPoolSize = 500;
      csb.MinPoolSize = 10;
      csb.ConnectTimeout = 30;

      return csb.ConnectionString;

    } // PrepareConnectionString



    public static SqlCommand DownsizeCommandText(SqlCommand Cmd)
    {
      String _txt;
      int _idx;

      _txt = Cmd.CommandText;

      if (Cmd.Parameters.Count > 0)
      {
        Object[] _xx;

        _xx = new Object[Cmd.Parameters.Count];

        for (_idx = 0; _idx < Cmd.Parameters.Count; _idx++)
        {
          String _new_name;
          SqlParameter _p;

          _p = Cmd.Parameters[_idx];
          _new_name = "@p" + (_idx + 1).ToString();
          _xx[_idx] = _p.ParameterName.Length.ToString("00000") + " " + _p.ParameterName + " " + _new_name;
        }

        Array.Sort(_xx);

        for (_idx = _xx.Length - 1; _idx > 0; _idx--)
        {
          String[] _part;
          SqlParameter _p;
          String _s;

          _s = (String)_xx[_idx];
          _part = _s.Split(new String[] { " " }, StringSplitOptions.RemoveEmptyEntries);
          _txt = _txt.Replace(_part[1], _part[2]);
          _p = Cmd.Parameters[_part[1]];
          _p.ParameterName = _part[2];
        }
      }

      _txt = _txt.Replace("\r\n", "");
      int _len;
      _len = 0;
      while (_len != _txt.Length)
      {
        _len = _txt.Length;
        _txt = _txt.Replace("  ", " ");
        _txt = _txt.Replace(" ,", ",");
        _txt = _txt.Replace(", ", ",");
        _txt = _txt.Replace(" )", ")");
        _txt = _txt.Replace("( ", "(");
      }
      _txt = _txt.Trim();

      Cmd.CommandText = _txt;

      return Cmd;
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="UserBaseName"></param>
    public static void ConnectAs(String UserBaseName)
    {
      System.Threading.Thread th;

      if (UserBaseName == "WGROOT")
      {
        // Validate database version (DB_COMMON_BUILD)
        if (!ValidateDbVersion())
        {
          Log.Error("Database version could not be validated. Application will stop");
          Log.Error("Stopping ...");
          Log.Error("");

          System.Threading.Thread.Sleep(5000);

          Environment.Exit(0);

          return;
        }
      }

      m_connection_string = BuildConnectionString(UserBaseName);

      th = new Thread(new ThreadStart(ConnectionThread));
      th.Start();

      th = new Thread(new ThreadStart(ApplicationVersionThread));
      th.Start();

    } // ConnectAs


    public static void Close(SqlConnection SqlConn)
    {
      if (SqlConn == null)
      {
        return;
      }
      try
      {
        SqlConn.Close();
      }
      catch
      {
      }
      SqlConn.Dispose();
    }
    public static void CommitTransaction(SqlTransaction SqlTrx)
    {
      if (SqlTrx == null)
      {
        return;
      }
      try
      {
        SqlTrx.Commit();
      }
      catch
      {
      }
      SqlTrx.Dispose();
    }
    public static void RollbackTransaction(SqlTransaction SqlTrx)
    {
      if (SqlTrx == null)
      {
        return;
      }
      try
      {
        SqlTrx.Rollback();
      }
      catch
      {
      }
      SqlTrx.Dispose();
    }


    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public static SqlConnection Connection()
    {
      SqlConnection _sql_conn;
      const int _num_tries = 2;
      int _idx;

      for (_idx = 0; _idx < _num_tries; _idx++)
      {
        try
        {
          _sql_conn = new SqlConnection(m_connection_string);
          if (_sql_conn != null)
          {
            try
            {
              _sql_conn.Open();
            }
            catch { ; }

            if (_sql_conn.State == ConnectionState.Open)
            {
              return _sql_conn;
            }

            // Clear Pool: Mark all the connections to be discarded.
            Log.Message("*");
            Log.Message("* Connection State: " + _sql_conn.State.ToString() + " -> Connection Pool cleared.");
            Log.Message("*");
            SqlConnection.ClearPool(_sql_conn);

            if (_idx >= _num_tries - 1)
            {
              Log.Error("Unable to return an open connection.");

              return _sql_conn;
            }

            try { _sql_conn.Close(); }
            catch { ; }
            _sql_conn.Dispose();
            _sql_conn = null;
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
      }

      // Not reachable ...
      Log.Error("Unable to return an open connection after " + _num_tries.ToString() + " tries.");
      return new SqlConnection(m_connection_string);

    } // Connection

    private static Boolean IsConnected(SqlConnection SqlConn)
    {
      if (SqlConn == null)
      {
        return false;
      }
      if (SqlConn.State != ConnectionState.Open)
      {
        return false;
      }
      try
      {
        using (SqlCommand _sql_cmd = SqlConn.CreateCommand())
        {
          _sql_cmd.CommandText = "SELECT 1";
          _sql_cmd.ExecuteScalar();

          return true;
        }
      }
      catch
      {
        return false;
      }
    }

    public static SqlConnection Reconnect(SqlConnection SqlConn)
    {
      if (IsConnected(SqlConn))
      {
        // Connected!
        return SqlConn;
      }

      // Not connected
      if (SqlConn != null)
      {
        try { SqlConn.Close(); }
        catch { ; }
        SqlConn.Dispose();
        SqlConn = null;
      }

      // Return new connection
      return WASDB.Connection();
    }

    private static void ApplicationVersionThread()
    {
      int _wait_hint;

      _wait_hint = 0;
      while (true)
      {
        Thread.Sleep(_wait_hint);
        _wait_hint = 1000;

        try
        {
          using (SqlConnection _sql_conn = WASDB.Connection())
          {
            if (_sql_conn.State != ConnectionState.Open)
            {
              continue;
            }
            UpdateApplicationVersionRecord(_sql_conn);
            _wait_hint = 3000;

            if (app_name == "WigosASGUI")
            {
              if (m_times_to_update > 1)
              {
                Users.CheckCurrentSession(app_name);

                m_times_to_update = 0;
              }
              m_times_to_update++;
            }

          }
        }
        catch { ; }
      }
    }


    private static void ConnectionThread()
    {
      const int SYNC_TIME_INTERVAL = 15 * 60 * 1000;

      ConnectionState _conn_state;
      String _data_source;
      String _data_base;
      DateTime _db_datetime;
      Boolean _state_changed;
      int _wait_hint;
      int _tick_sync_time;

      _data_source = "";
      _data_base = "";
      _conn_state = ConnectionState.Connecting;
      Log.Message("DATABASE ==> INITIAL STATUS: CONNECTING ...");

      _wait_hint = 0;
      _tick_sync_time = Environment.TickCount - SYNC_TIME_INTERVAL;

      while (true)
      {
        Thread.Sleep(_wait_hint);
        _wait_hint = 1000;

        _conn_state = ConnectionState.Connecting;

        try
        {
          using (SqlConnection _sql_conn = WASDB.Connection())
          {
            using (SqlCommand _sql_cmd = _sql_conn.CreateCommand())
            {
              _sql_cmd.CommandText = "SELECT GETDATE()";

              try
              {
                _db_datetime = (DateTime)_sql_cmd.ExecuteScalar();
                m_timespan = DateTime.Now.Subtract(_db_datetime);
                _conn_state = _sql_conn.State;
                _data_source = _sql_conn.DataSource;
                _data_base = _sql_conn.Database;
                if (Misc.GetElapsedTicks(_tick_sync_time) >= SYNC_TIME_INTERVAL)
                {
                  _tick_sync_time = Environment.TickCount;

                  try
                  {
                    if (app_name == "CASHIER")
                    {
                      Win32SystemTime _w32_st;
                      _w32_st = Win32SystemTime.Create(_db_datetime);
                      if (!Win32SystemTime.SetLocalTime(ref _w32_st))
                      {
                        Log.Error("Win32SystemTime.SetLocalTime failed!");
                      }
                    }
                  }
                  catch
                  {
                    ;
                  }
                }
              }
              catch
              {
                ;
              }
            }
            if (_sql_conn != null)
            {
              try { _sql_conn.Close(); }
              catch { ; }
            }
          }
        }
        catch
        {
          ;
        }

        if (_conn_state != ConnectionState.Open)
        {
          _conn_state = ConnectionState.Closed;
        }

        _state_changed = false;
        _state_changed |= (_conn_state != m_conn_state);
        _state_changed |= (_data_source != m_data_source);

        if (_state_changed)
        {
          if (m_data_source != _data_source)
          {
            if (m_data_source != "")
            {
              Log.Message("*");
              Log.Message("* FAIL OVER: " + m_data_source);
            }
            Log.Message("*");
            Log.Message("* Connected to DataSource: " + _data_source + " (" + _data_base + ")");
            Log.Message("*");
          }

          m_data_source = _data_source;
          m_conn_state = _conn_state;

          Log.Message("DATABASE ==> CHANGE STATUS: " + m_conn_state.ToString().ToUpper());
          Log.Message("");

          if (OnStatusChanged != null)
          {
            try
            {
              OnStatusChanged();
            }
            catch
            {
              ;
            }
          }
        }

        if (m_conn_state == ConnectionState.Open)
        {
          _wait_hint = 3000;
        }
      }
    }

    public static Boolean TRANSACTION(DbFunction Function)
    {
      SqlConnection _sql_con;
      SqlTransaction _sql_trx;

      _sql_con = null;
      _sql_trx = null;

      try
      {
        _sql_con = Connection();
        if (_sql_con == null)
        {
          return false;
        }
        if (_sql_con.State != ConnectionState.Open)
        {
          return false;
        }
        _sql_trx = _sql_con.BeginTransaction();
        if (_sql_trx == null)
        {
          return false;
        }
        if (Function(_sql_trx))
        {
          _sql_trx.Commit();

          return true;
        }
        return false;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);

        return false;
      }
      finally
      {
        if (_sql_trx != null)
        {
          if (_sql_trx.Connection != null)
          {
            try { _sql_trx.Rollback(); }
            catch { }
          }
          _sql_trx.Dispose();
          _sql_trx = null;
        }
        if (_sql_con != null)
        {
          try { _sql_con.Close(); }
          catch { }
          _sql_con.Dispose();
          _sql_con = null;
        }
      }
    } // Execute

    /// <summary>
    /// Validate database (DB_COMMON_BUILD) version:
    ///   - Check version current version is equal or lower than new version.
    ///   - Update to a new version if necessary.
    /// </summary>
    /// <returns></returns>
    private static Boolean ValidateDbVersion()
    {
      String sql_str;
      SqlCommand sql_cmd;
      SqlConnection sql_con;
      SqlTransaction sql_trx;
      Int32 old_db_version;
      int sql_value;
      Boolean first_time;

      sql_con = new SqlConnection(BuildConnectionString("WGROOT"));
      if (sql_con == null)
      {
        return false;
      }
      sql_trx = null;


      try
      {
        sql_con.Open();
        if (sql_con.State != ConnectionState.Open)
        {
          Log.Error("Unable to open connection for user: WGROOT");

          return false;
        }
        sql_trx = sql_con.BeginTransaction();

        first_time = true;

        while (true)
        {
          sql_str = "SELECT DB_COMMON_BUILD_ID FROM DB_VERSION";
          sql_cmd = new SqlCommand(sql_str, sql_trx.Connection, sql_trx);
          old_db_version = (Int32)sql_cmd.ExecuteScalar();
          found_db_version = old_db_version;

          if (old_db_version == expected_db_version)
          {
            sql_trx.Commit();

            return true;
          }

          if (old_db_version > expected_db_version)
          {
            Log.Error("DB Version Found/Expected mismatch: " + old_db_version.ToString() + "/" + expected_db_version.ToString());

            sql_trx.Rollback();

            return false;
          }

          if (first_time)
          {
            first_time = false;

            Log.Message("DB Version Found/Expected: " + old_db_version.ToString() + "/" + expected_db_version.ToString());
            Log.Message("Updating ...");
          }

          sql_str = "";
          sql_str += " UPDATE   DB_VERSION ";
          sql_str += "    SET   DB_COMMON_BUILD_ID = @p1 + 1 ";
          sql_str += "        , DB_UPDATED         = GETDATE () ";
          sql_str += "  WHERE   DB_COMMON_BUILD_ID = @p1 ";

          sql_cmd = new SqlCommand(sql_str, sql_trx.Connection, sql_trx);
          sql_cmd.Parameters.Add("@p1", System.Data.SqlDbType.Int, 4, "DB_COMMON_BUILD_ID").Value = old_db_version;

          sql_value = sql_cmd.ExecuteNonQuery();
          if (sql_value == 1)
          {
            Log.Message("Updating DB to version: " + (old_db_version + 1).ToString());

            // DbVersion updated, execute the Update
            if (!Update(old_db_version, old_db_version + 1, sql_trx))
            {
              sql_trx.Rollback();

              return false;
            }
          }
        } // while ( true )
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
      finally
      {
        if (sql_trx != null)
        {
          if (sql_trx.Connection != null)
          {
            sql_trx.Rollback();
          }
        }
        if (sql_con != null)
        {
          sql_con.Close();
        }
      }
    }

    /// <summary>
    /// Versions update actions. Applied when a new database version is found.
    /// </summary>
    /// <param name="OldDbVersion"></param>
    /// <param name="NewDbVersion"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private static Boolean Update(Int32 OldDbVersion, Int32 NewDbVersion, SqlTransaction SqlTrx)
    {
      try
      {
        if (OldDbVersion != (NewDbVersion - 1))
        {
          return false;
        }

        switch (OldDbVersion)
        {
          case 1:
            return Update_0001_to_0002(SqlTrx);

          case 100:
            return Update_0100_to_0101(SqlTrx);

          default:
            return false;
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);

        return false;
      }
      finally
      {

      }
    } // Update

    /// <summary>
    /// Update database actions from 1st. to 2nd. version
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private static Boolean Update_0001_to_0002(SqlTransaction SqlTrx)
    {
      StringBuilder sql_str;
      SqlCommand sql_cmd;
      int sql_value;

      sql_str = new StringBuilder();
      sql_str.AppendLine("INSERT INTO GENERAL_PARAMS ( GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE ) VALUES ( 'Play', 'MinimumPlayers',        '2' );     ");
      sql_str.AppendLine("INSERT INTO GENERAL_PARAMS ( GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE ) VALUES ( 'Play', 'WaitForMinimumPlayers', '1000' );  ");
      sql_str.AppendLine("INSERT INTO GENERAL_PARAMS ( GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE ) VALUES ( 'Play', 'ActivityTimeout',       '10000' ); ");
      sql_str.AppendLine("INSERT INTO GENERAL_PARAMS ( GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE ) VALUES ( 'Play', 'ActivityDueToCardIn',   '0' );     ");

      sql_cmd = new SqlCommand(sql_str.ToString(), SqlTrx.Connection, SqlTrx);
      try
      {
        sql_value = sql_cmd.ExecuteNonQuery();
        if (sql_value == 4)
        {
          return true;
        }

        return false;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);

        return false;
      }
      finally
      {
      }
    } // Update_0001_to_0002


    /// <summary>
    /// Update database actions from 1st. to 2nd. version
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private static Boolean Update_0100_to_0101(SqlTransaction SqlTrx)
    {
      StringBuilder sql_str;
      SqlCommand sql_cmd;
      int sql_value;

      sql_str = new StringBuilder();

      sql_str.AppendLine("ALTER TABLE dbo.c2_jackpot_parameters ADD	");
      sql_str.AppendLine("	c2jp_block_mode int NOT NULL CONSTRAINT DF_c2_jackpot_parameters_c2jp_block_mode DEFAULT ((0)),	");
      sql_str.AppendLine("	c2jp_block_interval int NOT NULL CONSTRAINT DF_c2_jackpot_parameters_c2jp_block_interval DEFAULT ((300)),	");
      sql_str.AppendLine("	c2jp_animation_interval int NOT NULL CONSTRAINT DF_c2_jackpot_parameters_c2jp_animation_interval DEFAULT ((15)),	");
      sql_str.AppendLine("	c2jp_recent_interval int NOT NULL CONSTRAINT DF_c2_jackpot_parameters_c2jp_recent_interval DEFAULT ((3600)),	");
      sql_str.AppendLine("	c2jp_promo_message1 varchar(MAX) NULL,	");
      sql_str.AppendLine("	c2jp_promo_message2 varchar(MAX) NULL,	");
      sql_str.AppendLine("	c2jp_promo_message3 varchar(MAX) NULL,	");
      sql_str.AppendLine("	c2jp_block_min_amount money NOT NULL CONSTRAINT DF_c2_jackpot_parameters_c2jp_block_min_amount DEFAULT ((0))	;");

      sql_str.AppendLine(" INSERT INTO [dbo].[general_params] ([gp_group_key], [gp_subject_key], [gp_key_value] ) VALUES ('Class II','Severity','0') ;");

      sql_cmd = new SqlCommand(sql_str.ToString(), SqlTrx.Connection, SqlTrx);
      try
      {
        sql_value = sql_cmd.ExecuteNonQuery();
        if (sql_value == 1)
        {
          return true;
        }

        return false;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);

        return false;
      }
      finally
      {
      }
    } // Update_0100_to_0101


    private static void UpdateApplicationVersionRecord(SqlConnection Conn)
    {
      String sql_update;
      String sql_insert;

      String user_name;
      String ip_address;
      SqlCommand sql_cmd;

      try
      {
        user_name = Environment.UserName;
        if (user_name == "")
        {
          user_name = "Unknown";
        }
        ip_address = WSI.Common.DNSUtility.GetFirstIP().ToString();


        sql_update = "UPDATE   APPLICATIONS"
                    + "   SET   APP_IP_ADDRESS  = @pIPAdress "
                    + "       , APP_OS_USERNAME = @pOSUserName "
                    + "       , APP_LOGIN_NAME  = @pLoginName "
                    + "       , APP_VERSION     = @pVersion "
                    + "       , APP_LAST_ACCESS = GETDATE () "
                    + " WHERE   APP_ID = ( SELECT   APP_ID "
                    + "                      FROM   APPLICATIONS "
                    + "                     WHERE   APP_NAME        = @pApplication "
                    + "                       AND   APP_MACHINE     = @pMachine     )";

        sql_insert = "INSERT INTO APPLICATIONS "
                    + "        ( APP_IP_ADDRESS  "
                    + "        , APP_OS_USERNAME "
                    + "        , APP_LOGIN_NAME  "
                    + "        , APP_VERSION     "
                    + "        , APP_LAST_ACCESS "
                    + "        , APP_NAME        "
                    + "        , APP_MACHINE     "
                    + "        , APP_ALIAS       "
                    + "        ) "
                    + " VALUES ( @pIPAdress "
                    + "        , @pOSUserName "
                    + "        , @pLoginName "
                    + "        , @pVersion "
                    + "        , GETDATE () "
                    + "        , @pApplication "
                    + "        , @pMachine "
                    + "        , @pAlias   "
                    + "        )";

        sql_cmd = new SqlCommand(sql_update);
        sql_cmd.Connection = Conn;

        sql_cmd.Parameters.Add("@pApplication", SqlDbType.NVarChar, 50).Value = app_name;
        sql_cmd.Parameters.Add("@pVersion", SqlDbType.NVarChar, 50).Value = app_version;
        sql_cmd.Parameters.Add("@pIPAdress", SqlDbType.NVarChar, 50).Value = ip_address;
        sql_cmd.Parameters.Add("@pOSUserName", SqlDbType.NVarChar, 50).Value = user_name;
        sql_cmd.Parameters.Add("@pLoginName", SqlDbType.NVarChar, 50).Value = login_name;
        sql_cmd.Parameters.Add("@pMachine", SqlDbType.NVarChar, 50).Value = Environment.MachineName;
        sql_cmd.Parameters.Add("@pAlias", SqlDbType.NVarChar, 50).Value = Environment.MachineName;

        if (sql_cmd.ExecuteNonQuery() == 1)
        {
          return;
        }

        sql_cmd.CommandText = sql_insert;

        if (sql_cmd.ExecuteNonQuery() == 1)
        {
          return;
        }
      }
      catch (SqlException _sql_ex)
      {
        if (_sql_ex.Number == -2) // command timeout
        {
          // Do nothing
        }
        else
        {
          Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
      finally
      {
        ;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Converts SQL parameters to SQL variables.
    //
    //  PARAMS :
    //      - INPUT :
    //          - SqlCommand SqlCmd
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - SqlCommand
    //
    public static SqlCommand ParametersToVariables(SqlCommand SqlCmd)
    {
      SqlCommand _sql_cmd;
      String _declares;
      String _sets;
      String _new_param_name;

      if (SqlCmd.Parameters.Count == 0)
      {
        return SqlCmd;
      }

      _declares = "";
      _sets = "";
      _sql_cmd = SqlCmd.Clone();

      foreach (SqlParameter _param in _sql_cmd.Parameters)
      {
        _new_param_name = _param.ParameterName + "_X";
        _declares += "DECLARE " + _param.ParameterName + " AS " + SqlParameterType(_param.SqlDbType) + Environment.NewLine;
        _sets += "SET " + _param.ParameterName + " = " + _new_param_name + Environment.NewLine;

        _param.ParameterName = _new_param_name;
      }

      _sql_cmd.CommandText = _declares + Environment.NewLine + _sets + Environment.NewLine + _sql_cmd.CommandText;

      return _sql_cmd;
    } // ParametersToVariables

    //------------------------------------------------------------------------------
    // PURPOSE : Give the string representation of a SqlDbType needed to declare variables.
    //
    //  PARAMS :
    //      - INPUT :
    //          - SqlDbType sqlDbType
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String
    //
    private static String SqlParameterType(SqlDbType SqlDbType)
    {
      switch (SqlDbType)
      {
        case SqlDbType.Int:
        case SqlDbType.BigInt:
        case SqlDbType.DateTime:
          return SqlDbType.ToString();

        default:
          throw new Exception("ParametersToVariables: Can't convert sql parameter type: " + SqlDbType.ToString());
      }
    } // ParametersToVariables


    //------------------------------------------------------------------------------
    // PURPOSE : Check the Disk Free Space on the PRIMARY SqlServer
    //
    //  PARAMS :
    //      - INPUT :
    //          - SqlDbType sqlDbType
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - TRUE  on success
    //      - FALSE on otherwise
    //
    private static Boolean CheckSqlServerFreeDiskSpace(out Boolean LowLevelDisk)
    {
      SqlConnection _sql_con;

      LowLevelDisk = false;

      try
      {
        using (_sql_con = WASDB.Connection())
        {
          using (SqlCommand _sql_cmd = new SqlCommand("EXEC MASTER.dbo.xp_fixeddrives", _sql_con))
          {
            using (SqlDataReader _sql_rdr = _sql_cmd.ExecuteReader())
            {
              while (_sql_rdr.Read())
              {
                if (_sql_rdr.IsDBNull(1))
                {
                  continue;
                }
                if (_sql_rdr.GetInt32(1) <= 1024) // 1024 MB
                {
                  LowLevelDisk = true;

                  return true;
                }
              }
              return true;
            }
          }
        }
      }
      catch
      {

      }

      return false;
    }


    #endregion

  }
}
