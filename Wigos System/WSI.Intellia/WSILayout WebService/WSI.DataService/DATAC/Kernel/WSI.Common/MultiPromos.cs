//////-------------------------------------------------------------------
////// Copyright � 2012 Win Systems Ltd.
//////-------------------------------------------------------------------
//////
////// MODULE NAME:   MultiPromos.cs
////// DESCRIPTION:   
////// AUTHOR:        Marcos Piedra Osuna
////// CREATION DATE: 30-MAY-2012
//////
////// REVISION HISTORY:
//////
////// Date         Author Description
////// -----------  ------ -----------------------------------------------
////// 30-MAY-2012  MPO    Initial version
////// -------------------------------------------------------------------

////using System;
////using System.Collections.Generic;
////using System.Text;
////using System.Data.SqlClient;
////using System.Data;

////namespace WSI.Common
////{
////  public static class MultiPromos
////  {

////    public static Boolean UpdateNotRedeemable2(Int64 AccountID, 
////                                               Currency DeltaPlayed, 
////                                               out Currency NotRedeemable2,
////                                               SqlTransaction SqlTrx)
////    {
////      SqlDataAdapter _da;
////      StringBuilder _sql_sb;
////      DataTable _dt;
////      DataRow _dr;
////      Currency _not_redeemable2;
////      Currency _tmp_delta_played;
////      Currency _aux;
////      Int32 _row_index;
////      SqlParameter _param;

////      NotRedeemable2 = 0;

////      _da = new SqlDataAdapter();
////      _da.DeleteCommand = null;
////      _da.InsertCommand = null;

////      try
////      {
////        _sql_sb = new StringBuilder();
////        _sql_sb.Append("   SELECT   ACP_UNIQUE_ID                  ");
////        _sql_sb.Append("          , ACP_BALANCE                    ");
////        _sql_sb.Append("          , ACP_PLAYED                     ");
////        _sql_sb.Append("          , ACP_STATUS                     ");
////        _sql_sb.Append("     FROM   ACCOUNT_PROMOTIONS             ");
////        _sql_sb.Append("    WHERE   ACP_ACCOUNT_ID  = @pAccountId  ");
////        _sql_sb.Append("      AND   ACP_BALANCE     > 0            ");
////        _sql_sb.Append("      AND   ACP_STATUS      = @pStatus     ");
////        _sql_sb.Append("      AND   ACP_CREDIT_TYPE = @pCreditType ");
////        _sql_sb.Append(" ORDER BY   ACP_AWARDED   ASC              ");
////        _sql_sb.Append("          , ACP_UNIQUE_ID ASC              ");

////        _da.SelectCommand = new SqlCommand(_sql_sb.ToString(), SqlTrx.Connection, SqlTrx);
////        _da.SelectCommand.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountID;
////        _da.SelectCommand.Parameters.Add("@pStatus", SqlDbType.Int).Value = (Int32)ACCOUNT_PROMO_STATUS.ACTIVE;
////        _da.SelectCommand.Parameters.Add("@pCreditType", SqlDbType.Int).Value = (Int32)ACCOUNT_PROMO_CREDIT_TYPE.NR2; 

////        _sql_sb.Length = 0;

////        _sql_sb.Append(" UPDATE   ACCOUNT_PROMOTIONS                ");
////        _sql_sb.Append("    SET   ACP_BALANCE   = @pBalance         ");
////        _sql_sb.Append("        , ACP_PLAYED    = @pPlayed          ");
////        _sql_sb.Append("        , ACP_STATUS    = @pStatus          ");
////        _sql_sb.Append("  WHERE   ACP_UNIQUE_ID = @pUniqueId        ");
////        _sql_sb.Append("    AND   ACP_BALANCE   = @pOriginalBalance ");
////        _sql_sb.Append("    AND   ACP_PLAYED    = @pOriginalPlayed  ");
////        _sql_sb.Append("    AND   ACP_STATUS    = @pOriginalStatus  ");

////        _da.UpdateCommand = new SqlCommand(_sql_sb.ToString(), SqlTrx.Connection, SqlTrx);
////        _da.UpdateCommand.Parameters.Add("@pBalance", SqlDbType.Money).SourceColumn = "ACP_BALANCE";
////        _da.UpdateCommand.Parameters.Add("@pPlayed", SqlDbType.Money).SourceColumn = "ACP_PLAYED";
////        _da.UpdateCommand.Parameters.Add("@pStatus", SqlDbType.Int).SourceColumn = "ACP_STATUS";
////        _da.UpdateCommand.Parameters.Add("@pUniqueId", SqlDbType.BigInt).SourceColumn = "ACP_UNIQUE_ID";
////        _da.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;

////        _param = _da.UpdateCommand.Parameters.Add("@pOriginalBalance", SqlDbType.Money);
////        _param.SourceColumn = "ACP_BALANCE";
////        _param.SourceVersion = DataRowVersion.Original;
////        _param = _da.UpdateCommand.Parameters.Add("@pOriginalPlayed", SqlDbType.Money);
////        _param.SourceColumn = "ACP_PLAYED";
////        _param.SourceVersion = DataRowVersion.Original;
////        _param = _da.UpdateCommand.Parameters.Add("@pOriginalStatus", SqlDbType.Int);
////        _param.SourceColumn = "ACP_STATUS";
////        _param.SourceVersion = DataRowVersion.Original;

////        // Execute Select command
////        _dt = new DataTable();
////        _da.Fill(_dt);

////        _tmp_delta_played = DeltaPlayed;
////        _row_index = 0;
////        _not_redeemable2 = 0;

////        while (_tmp_delta_played > 0 && _row_index < _dt.Rows.Count)
////        {
////          _dr = _dt.Rows[_row_index];
////          _aux = Math.Min(_tmp_delta_played, (Decimal)_dr["ACP_BALANCE"]);
////          _tmp_delta_played -= _aux;
////          _dr["ACP_BALANCE"] = (Decimal)_dr["ACP_BALANCE"] - (Decimal)_aux;
////          _dr["ACP_PLAYED"] = (Decimal)_dr["ACP_PLAYED"] + (Decimal)_aux;
////          if ((Decimal)_dr["ACP_BALANCE"] == 0)
////          {
////            _dr["ACP_STATUS"] = ACCOUNT_PROMO_STATUS.EXHAUSTED;
////          }
////          _not_redeemable2 += (Decimal)_dr["ACP_BALANCE"];
////          _row_index++;
////        }

////        if (_row_index > 0)
////        {
////          _da.UpdateBatchSize = 500;
////          _da.ContinueUpdateOnError = true;
////          if (_da.Update(_dt) != _row_index)
////          {
////            return false;
////          }
////        }

////        NotRedeemable2 = _not_redeemable2;

////        return true;
////      }
////      catch (Exception _ex)
////      {
////        Log.Exception(_ex);
////        Log.Error("MultiPromos.UpdateNotRedeemable2");
        
////        return false;
////      }

////    } // UpdateNotRedeemable2

////    public static Boolean UpdateNotRedeemable1OnEndSession(Int64 AccountID,
////                                                           Currency Withhold,
////                                                           SqlTransaction SqlTrx)
////    {
////      SqlDataAdapter _da;
////      DataTable _dt;
////      DataRow _dr;
////      Currency _aux;
////      Currency _tmp_withhold;
////      Currency _delta_balance;
////      StringBuilder _sql_sb;
////      Int32 _row_index;
////      SqlParameter _param;

////      _da = new SqlDataAdapter();
////      _da.DeleteCommand = null;
////      _da.InsertCommand = null;

////      try
////      {
////        _sql_sb = new StringBuilder();
////        _sql_sb.Append(" SELECT     ACP_BALANCE                    ");
////        _sql_sb.Append("          , ACP_WITHHOLD                   ");
////        _sql_sb.Append("          , ACP_PLAYED                     ");
////        _sql_sb.Append("          , ACP_WON                        ");
////        _sql_sb.Append("          , ACP_UNIQUE_ID                  ");
////        _sql_sb.Append("     FROM   ACCOUNT_PROMOTIONS             ");
////        _sql_sb.Append("    WHERE   ACP_ACCOUNT_ID  = @pAccountId  ");
////        _sql_sb.Append("      AND   ACP_CREDIT_TYPE = @pCreditType ");
////        _sql_sb.Append(" ORDER BY   ACP_AWARDED   DESC             ");
////        _sql_sb.Append("          , ACP_UNIQUE_ID DESC             ");

////        _da.SelectCommand = new SqlCommand(_sql_sb.ToString(), SqlTrx.Connection, SqlTrx);
////        _da.SelectCommand.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountID;
////        _da.SelectCommand.Parameters.Add("@pCreditType", SqlDbType.Int).Value = (Int32)ACCOUNT_PROMO_CREDIT_TYPE.NR1;

////        _sql_sb.Length = 0;

////        _sql_sb.Append(" UPDATE   ACCOUNT_PROMOTIONS                ");
////        _sql_sb.Append("    SET   ACP_BALANCE   = @pBalance         ");
////        _sql_sb.Append("        , ACP_PLAYED    = @pPlayed          ");
////        _sql_sb.Append("        , ACP_WON       = @pWon             ");
////        _sql_sb.Append("  WHERE   ACP_UNIQUE_ID = @pUniqueId        ");
////        _sql_sb.Append("    AND   ACP_BALANCE   = @pOriginalBalance ");
////        _sql_sb.Append("    AND   ACP_PLAYED    = @pOriginalPlayed  ");
////        _sql_sb.Append("    AND   ACP_WON       = @pOriginalWon     ");

////        _da.UpdateCommand = new SqlCommand(_sql_sb.ToString(), SqlTrx.Connection, SqlTrx);
////        _da.UpdateCommand.Parameters.Add("@pBalance", SqlDbType.Money).SourceColumn = "ACP_BALANCE";
////        _da.UpdateCommand.Parameters.Add("@pPlayed", SqlDbType.Money).SourceColumn = "ACP_PLAYED";
////        _da.UpdateCommand.Parameters.Add("@pWon", SqlDbType.Money).SourceColumn = "ACP_WON";
////        _da.UpdateCommand.Parameters.Add("@pUniqueId", SqlDbType.BigInt).SourceColumn = "ACP_UNIQUE_ID";
////        _da.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;

////        _param = _da.UpdateCommand.Parameters.Add("@pOriginalBalance", SqlDbType.Money);
////        _param.SourceColumn = "ACP_BALANCE";
////        _param.SourceVersion = DataRowVersion.Original;
////        _param = _da.UpdateCommand.Parameters.Add("@pOriginalPlayed", SqlDbType.Money);
////        _param.SourceColumn = "ACP_PLAYED";
////        _param.SourceVersion = DataRowVersion.Original;
////        _param = _da.UpdateCommand.Parameters.Add("@pOriginalWon", SqlDbType.Money);
////        _param.SourceColumn = "ACP_WON";
////        _param.SourceVersion = DataRowVersion.Original;

////        _dt = new DataTable();
////        _da.Fill(_dt);

////        _tmp_withhold = Withhold;
////        _delta_balance = 0;
////        _row_index = 0;
        
////        while (_tmp_withhold > 0 && _row_index < _dt.Rows.Count)
////        {
////          _dr = _dt.Rows[_row_index];
////          _aux = Math.Min(_tmp_withhold, (Decimal)_dr["ACP_WITHHOLD"]);
////          _delta_balance = (Decimal)_dr["ACP_BALANCE"] - _aux;

////          if (_delta_balance > 0)
////          {
////            _dr["ACP_PLAYED"] = (Decimal)_dr["ACP_PLAYED"] + _delta_balance;
////          }
////          else
////          {
////            _dr["ACP_WON"] = (Decimal)_dr["ACP_WON"] - _delta_balance;
////          }

////          _tmp_withhold -= _aux;
////          _dr["ACP_BALANCE"] = (Decimal)_aux;
////          _row_index++;
////        }

////        if (_row_index > 0)
////        {
////          _da.UpdateBatchSize = 500;
////          _da.ContinueUpdateOnError = true;
////          if (_da.Update(_dt) != _row_index)
////          {
////            return false;
////          }
////        }

////        return true;

////      }
////      catch (Exception _ex)
////      {
////        Log.Exception(_ex);
////        Log.Error("MultiPromos.UpdateNotRedeemable1OnEndSession");

////        return false;
////      }

////    } // UpdateNotRedeemable1OnEndSession

////    //public static Boolean UpdateNotRedeemableBeforeCashIn(Int64 AccountId, SqlTransaction SqlTrx)
////    //{
////    //  SqlDataAdapter _da;
////    //  DataTable _dt;
////    //  StringBuilder _sql_sb;

////    //  _da = new SqlDataAdapter();
////    //  _da.DeleteCommand = null;
////    //  _da.InsertCommand = null;

////    //  try
////    //  {
////    //    _sql_sb = new StringBuilder();
////    //    _sql_sb.Append(" SELECT   ACP_BALANCE                    ");
////    //    _sql_sb.Append("        , ACP_WITHHOLD                   ");
////    //    _sql_sb.Append("        , ACP_STATUS                     ");
////    //    _sql_sb.Append("        , ACP_UNIQUE_ID                  ");
////    //    _sql_sb.Append("   FROM   ACCOUNT_PROMOTIONS             ");
////    //    _sql_sb.Append("  WHERE   ACP_ACCOUNT_ID  = @pAccountId  ");
////    //    _sql_sb.Append("    AND   ACP_CREDIT_TYPE = @pCreditType ");
////    //    _sql_sb.Append("    AND   ACP_STATUS      = @pStatus     ");

////    //    _da.SelectCommand = new SqlCommand(_sql_sb.ToString(), SqlTrx.Connection, SqlTrx);
////    //    _da.SelectCommand.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
////    //    _da.SelectCommand.Parameters.Add("@pStatus", SqlDbType.Int).Value = (int)ACCOUNT_PROMO_STATUS.ACTIVE;
////    //    _da.SelectCommand.Parameters.Add("@pCreditType", SqlDbType.Int).Value = (Int32)ACCOUNT_PROMO_CREDIT_TYPE.NR1;

////    //    _sql_sb.Length = 0;

////    //    _sql_sb.Append(" UPDATE   ACCOUNT_PROMOTIONS         ");
////    //    _sql_sb.Append("    SET   ACP_WITHHOLD  = @pWithhold ");
////    //    _sql_sb.Append("        , ACP_STATUS    = @pStatus   ");
////    //    _sql_sb.Append("  WHERE   ACP_UNIQUE_ID = @pUniqueId ");

////    //    _da.UpdateCommand = new SqlCommand(_sql_sb.ToString(), SqlTrx.Connection, SqlTrx);
////    //    _da.UpdateCommand.Parameters.Add("@pWithhold", SqlDbType.Money).SourceColumn = "ACP_WITHHOLD";
////    //    _da.UpdateCommand.Parameters.Add("@pStatus", SqlDbType.Int).SourceColumn = "ACP_STATUS";
////    //    _da.UpdateCommand.Parameters.Add("@pUniqueId", SqlDbType.BigInt).SourceColumn = "ACP_UNIQUE_ID";
////    //    _da.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;

////    //    _dt = new DataTable();
////    //    _da.Fill(_dt);

////    //    foreach (DataRow _dr in _dt.Rows)
////    //    {
////    //      if ((Decimal)_dr["ACP_BALANCE"] == 0)
////    //      {
////    //        _dr["ACP_STATUS"] = (Int32)ACCOUNT_PROMO_STATUS.EXHAUSTED;
////    //      }
////    //      else
////    //      {
////    //        _dr["ACP_WITHHOLD"] = _dr["ACP_BALANCE"];
////    //      }
////    //    }

////    //    if (_dt.Rows.Count > 0)
////    //    {
////    //      _da.UpdateBatchSize = 500;
////    //      _da.ContinueUpdateOnError = true;
////    //      if (_da.Update(_dt) != _dt.Rows.Count)
////    //      {
////    //        return false;
////    //      }
////    //    }

////    //    return true;
////    //  }
////    //  catch (Exception _ex)
////    //  {
////    //    Log.Exception(_ex);
////    //    Log.Error("MultiPromos.UpdateNotRedeemableBeforeCashIn");                

////    //    return false;
////    //  }

////    //} // UpdateNotRedeemableBeforeCashIn

////    public static Boolean UpdateNotRedeemable1BeforeCashIn(Int64 AccountId, SqlTransaction SqlTrx)
////    {
////      SqlCommand _sql_cmd;
////      StringBuilder _sql_sb;

////      try
////      {

////        _sql_sb = new StringBuilder();
////        _sql_sb.Append(" UPDATE   ACCOUNT_PROMOTIONS         ");
////        _sql_sb.Append("    SET   ACP_WITHHOLD    = CASE WHEN ACP_BALANCE > 0 THEN ACP_BALANCE ELSE ACP_WITHHOLD END ");
////        _sql_sb.Append("        , ACP_STATUS      = CASE WHEN ACP_BALANCE = 0 THEN @pStatusExhausted ELSE ACP_STATUS END ");
////        _sql_sb.Append("  WHERE   ACP_ACCOUNT_ID  = @pAccountId  ");
////        _sql_sb.Append("    AND   ACP_CREDIT_TYPE = @pCreditType ");
////        _sql_sb.Append("    AND   ACP_STATUS      = @pStatusActive ");

////        _sql_cmd = new SqlCommand(_sql_sb.ToString(), SqlTrx.Connection, SqlTrx);
////        _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
////        _sql_cmd.Parameters.Add("@pCreditType", SqlDbType.Int).Value = (Int32)ACCOUNT_PROMO_CREDIT_TYPE.NR1;
////        _sql_cmd.Parameters.Add("@pStatusActive", SqlDbType.Int).Value = (Int32)ACCOUNT_PROMO_STATUS.ACTIVE;
////        _sql_cmd.Parameters.Add("@pStatusExhausted", SqlDbType.Int).Value = (Int32)ACCOUNT_PROMO_STATUS.EXHAUSTED;

////        _sql_cmd.ExecuteNonQuery();

////        return true;
////      }
////      catch (Exception _ex)
////      {
////        Log.Exception(_ex);
////        Log.Error("MultiPromos.UpdateNotRedeemable1BeforeCashIn");

////        return false;
////      }

////    } // UpdateNotRedeemable1BeforeCashIn

////    public static Boolean Trx_BalanceToPlaySession(Int64 AccountId, 
////                                                   Int64 TerminalId, 
////                                                   Int64 PlaySessionId,
////                                                   out Decimal PreviousBalance,
////                                                   out Decimal Balance, 
////                                                   out Decimal Redimible, 
////                                                   out Decimal NotRedimible,
////                                                   SqlTransaction SqlTrx)
////    {
////      StringBuilder _sql_sb;
////      SqlCommand _sql_cmd;
////      SqlDataAdapter _da;
////      Int32 _updated;
////      DataTable _acc_promo;
////      String _provider;
////      ProviderList _provider_list;
////      String _aux_str;
////      SqlDataReader

////      PreviousBalance = 0;
////      Balance = 0;
////      Redimible = 0;
////      NotRedimible = 0;

////      _sql_sb = new StringBuilder();

////      try
////      {
////        // Retrieve the provider id
////        _sql_sb.Append("SELECT   ISNULL(TE_PROVIDER_ID,'') ");
////        _sql_sb.Append("  FROM   TERMINALS ");
////        _sql_sb.Append(" WHERE   TE_TERMINAL_ID = @pTerminalId ");

////        _sql_cmd = new SqlCommand(_sql_sb.ToString(), SqlTrx.Connection, SqlTrx);
////        _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.BigInt).Value = TerminalId;
////        _provider = (String)_sql_cmd.ExecuteScalar();        

////        // Block the account
////        _sql_sb.Length = 0;
////        _sql_sb.Append("UPDATE   ACCOUNTS ");
////        _sql_sb.Append("   SET   AC_BALANCE = AC_BALANCE ");
////        _sql_sb.Append(" WHERE   AC_ACCOUNT_ID = @pAccountId ");

////        _sql_cmd = new SqlCommand(_sql_sb.ToString(), SqlTrx.Connection, SqlTrx);
////        _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;

////        _updated = _sql_cmd.ExecuteNonQuery();
////        if (_updated!=1)
////        {
////          return false;
////        }

////        //
////        _sql_sb.Length = 0;
////        _sql_sb.Append("UPDATE   ACCOUNT_PROMOTIONS ");
////        _sql_sb.Append("   SET   ACP_BALANCE = ACP_BALANCE ");
////        _sql_sb.Append("OUTPUT   DELETED.ACP_BALANCE ");
////        _sql_sb.Append("	     , DELETED.ACP_UNIQUE_ID ");
////        _sql_sb.Append("	     , DELETED.ACP_CREDIT_TYPE ");
////        _sql_sb.Append("	     , CASE PROMO.PM_PLAY_RESTRICTED_TO_PROVIDER_LIST WHEN 1 ");
////        _sql_sb.Append("               THEN PROMO.PM_PROVIDER_LIST ");
////        _sql_sb.Append("		           ELSE NULL END AS PROVIDER_LIST ");
////        _sql_sb.Append("  FROM   ACCOUNT_PROMOTIONS LEFT JOIN PROMOTIONS AS PROMO ON ACP_PROMO_ID = PROMO.PM_PROMOTION_ID");
////        _sql_sb.Append(" WHERE   ACP_ACCOUNT_ID       = @pAccountId ");
////        _sql_sb.Append("   AND   ACP_BALANCE          > 0 ");
////        _sql_sb.Append("   AND   ACP_STATUS           = @pActive ");
////        _sql_sb.Append("   AND   ISNULL(ACP_PLAY_SESSION_ID,0) = 0 ");

////        _sql_cmd = new SqlCommand(_sql_sb.ToString(), SqlTrx.Connection, SqlTrx);
////        _sql_cmd.Parameters.Add("@pActive", SqlDbType.BigInt).Value = 2; // (Int32)ACCOUNT_PROMO_STATUS.ACTIVE;
////        _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;

////        _acc_promo = new DataTable();
////        _acc_promo.Load(_sql_cmd.ExecuteReader());

////        foreach (DataRow _dr in _acc_promo.Rows)
////        {
////          _aux_str = _dr["PROVIDER_LIST"] == DBNull.Value? null:(String)_dr["PROVIDER_LIST"];
////          _provider_list = new ProviderList();
////          _provider_list.FromXml(_aux_str);

////          if (!_provider_list.Contains(_provider))
////          {
////            _dr.Delete();
////            continue;
////          }        
         
////          if ((Int32)_dr["ACP_CREDIT_TYPE"] == 1) // NR1 = 1
////          {
////            Balance += (Decimal)_dr["ACP_BALANCE"];
////            NotRedimible += (Decimal)_dr["ACP_BALANCE"];
////          }
////          else if ((Int32)_dr["ACP_CREDIT_TYPE"] == 2) // NR2 = 2
////          {
////            Balance += (Decimal)_dr["ACP_BALANCE"];
////            NotRedimible += (Decimal)_dr["ACP_BALANCE"];     
////          }
////          else if ((Int32)_dr["ACP_CREDIT_TYPE"] == 3) // REDEEMABLE = 3
////          {
////            Balance += (Decimal)_dr["ACP_BALANCE"];
////            Redimible += (Decimal)_dr["ACP_BALANCE"];
////          }

////          _dr.AcceptChanges();
////          _dr.SetModified();          

////        }

////        //_acc_promo.AcceptChanges();

////        if (_acc_promo.Rows.Count > 0)
////        {

////          _sql_sb.Length = 0;
////          _sql_sb.Append("UPDATE   ACCOUNT_PROMOTIONS ");
////          _sql_sb.Append("   SET   ACP_PLAY_SESSION_ID = @pPlaySession ");
////          _sql_sb.Append(" WHERE   ACP_UNIQUE_ID = @pUniqueId ");

////          _da = new SqlDataAdapter();
////          _da.UpdateCommand = new SqlCommand(_sql_sb.ToString(), SqlTrx.Connection, SqlTrx);
////          _da.UpdateCommand.Parameters.Add("@pPlaySession", SqlDbType.BigInt).Value = PlaySessionId;
////          _da.UpdateCommand.Parameters.Add("@pUniqueId", SqlDbType.BigInt).SourceColumn = "ACP_UNIQUE_ID";
////          _da.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;

////          _da.UpdateBatchSize = 500;
////          _da.ContinueUpdateOnError = true;
////          if (_acc_promo.Rows.Count != _da.Update(_acc_promo))
////          {
////            return false;
////          }

////          _sql_sb.Length = 0;
////          _sql_sb.Append("UPDATE   ACCOUNTS ");
////          _sql_sb.Append("   SET   AC_BALANCE     = AC_BALANCE - @pBalanceToPlay ");
////          _sql_sb.Append("OUTPUT   DELETED.AC_BALANCE ");
////          _sql_sb.Append(" WHERE   AC_ACCOUNT_ID  = @pAccountId ");
////          _sql_sb.Append("   AND   AC_BALANCE    >= @pBalanceToPlay ");

////          _sql_cmd = new SqlCommand(_sql_sb.ToString(), SqlTrx.Connection, SqlTrx);
////          _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
////          _sql_cmd.Parameters.Add("@pBalanceToPlay", SqlDbType.Money).Value = Balance;

////          if (_sql_cmd.ExecuteNonQuery()!=1)
////          {
////            return false;
////          }

////        }

////        return true;

////      }
////      catch (Exception)
////      {

////        return false;
////      }

////    }

////  }

////}
