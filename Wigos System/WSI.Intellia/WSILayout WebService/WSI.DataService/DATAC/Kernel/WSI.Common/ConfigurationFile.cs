//------------------------------------------------------------------------------
// Copyright © 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: ConfigurationFile.cs
// 
//   DESCRIPTION: Configuration file class
// 
//        AUTHOR: Ronald Rodríguez
// 
// CREATION DATE: 25-ABR-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 25-ABR-2007 RRT    First release.
//------------------------------------------------------------------------------
using System;
using System.IO;
using System.Text;
using WSI.Common;
using System.Xml;

namespace WSI.Common
{
  public class ConfigurationFile
  {
    #region Class Atributes

    private static FileStream FStream;
    private static XmlDocument XmlConfigFile;
    private static XmlDocument XmlConfigDefault;
    private static String FileConfigName;

    #endregion

    #region Private Methods

    #endregion

    #region Public Methods

    /// <summary>
    /// Initialize configuration settings:
    /// 1. Load default values.
    /// 2. Load configuration file.
    /// </summary>
    /// <param name="InputFileName"></param>
    /// <param name="DefaultXML"></param>
    /// <returns></returns>
    public static Boolean Init(String InputFileName, String DefaultXML )
    {
      FileConfigName = InputFileName;

      // Load default xml string
      try
      {
        XmlConfigDefault = new XmlDocument();
        XmlConfigDefault.LoadXml(DefaultXML);
      }
      catch (Exception ex)
      {
        Log.Error("Reading configuration default values."); 
        Log.Exception(ex);

        return false;
      }

      // Load configuration file
      try
      {
        FStream = new FileStream(FileConfigName, FileMode.Open, FileAccess.Read);

        // Load configuration file
        XmlConfigFile = new XmlDocument();
        XmlConfigFile.Load(FStream);
      }
      catch (Exception ex)
      {
        Log.Error(" Reading configuration file values.");
        Log.Exception(ex);

        return true;
      }
      finally
      {
        if (FStream != null)
        {
          FStream.Close();
        }
      }

      return true;

    } // Init

    /// <summary>
    /// Return the string that correspond to a Tag value.
    /// </summary>
    /// <param name="KeyValue"></param>
    /// <returns></returns>
    public static String GetSetting(String TagValue)
    {
      XmlNodeList xmlconfignode;
      XmlNodeList xmldefaultnode;
      String value;

      value = "";

      try
      {
        xmldefaultnode = XmlConfigDefault.GetElementsByTagName(TagValue);
        if ( xmldefaultnode != null )
        {
          value = xmldefaultnode[0].FirstChild.InnerText;
        }
      }
      catch 
      {
      }


      try
      {
        xmlconfignode = XmlConfigFile.GetElementsByTagName(TagValue);
        if (   xmlconfignode != null 
            && xmlconfignode.Count > 0 )
        {
          value = xmlconfignode[0].FirstChild.InnerText;
        }
      }
      catch 
      {
      }

      return value;
    } // GetSetting

    //------------------------------------------------------------------------------
    // PURPOSE : Save config file
    //
    //  PARAMS :
    //      - INPUT :
    //          - InputFileName
    //          - DefaultXML
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public static Boolean SaveConfigFile (String InputFileName, String DefaultXML)
    {
      FileStream file_stream;
      XmlDocument xml_config;

      if (File.Exists(InputFileName))
      {
        FileInfo file_info = new FileInfo(InputFileName);

        if ((file_info.Attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
        {
          Log.Error("Configuration file: " + InputFileName + " has ReadOnly attribute.");

          return false;
        }
      }

      try
      {
        // Close Old File 
        if (FStream != null)
        {
          FStream.Close();
          FStream.Dispose();
          FStream = null;
        }

        // Create the new file
        file_stream = new FileStream(InputFileName, FileMode.Create, FileAccess.Write);

        // Save new configuration
        xml_config = new XmlDocument();
        xml_config.LoadXml(DefaultXML);
        xml_config.Save(file_stream);
        file_stream.Close();

        // Init with new file.
        Init(InputFileName, DefaultXML);
      }
      catch (Exception ex)
      {
        Log.Error(" Writing configuration file.");
        Log.Exception(ex);

        return false;
      }

      return true;

    } // SaveConfigFile

    #endregion

  } // ConfigurationFile
}