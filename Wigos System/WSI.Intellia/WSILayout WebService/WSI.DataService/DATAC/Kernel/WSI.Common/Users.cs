//-------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//-------------------------------------------------------------------
//
// MODULE NAME:   Users.cs
// DESCRIPTION:   User utility
// AUTHOR:        Marcos Piedra Osuna
// CREATION DATE: 14-MAR-2012
//
// REVISION HISTORY:
//
// Date         Author Description
// -----------  ------ -----------------------------------------------
// 14-MAR-2012  MPO    Initial version
// 02-APR-2012  RCI & MPO     Added event handler OnAutoLogout of type AutoLogoutEventHandler.
// 06-AUG-2012  JAB    Added function "CheckPasswordRules"
// 07-AUG-2012  JAB    Eliminated parameter in the function "CheckPasswordRules" (EnableCheck).
//                     Function now reads this value from the table "general_params"
// 20-AUG-2012  HBB    Added function "CheckLoginRules"
// 29-AUG-2012  JAB    Added function "CheckPasswordHistory"
// -------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Threading;


namespace WSI.Common
{
  public class Users
  {
    #region Public Constants

    // PASSWORD POLICY
    private const int MIN_PASSWORD_LENGHT_CONST = 4;
    private const int MAX_PASSWORD_LENGTH_CONST = 15;
    private const int MIN_LOGIN_LENGTH_CONST = 4;
    private const int MAX_LOGIN_LENGTH_CONST = 15;
    private const int MIN_PASSWORD_DIGITS_CONST = 0;
    private const int MAX_PASSWORD_DIGITS_CONST = 15;
    private const int MIN_PASSWORD_LOWERCASE_CONST = 0;
    private const int MAX_PASSWORD_LOWERCASE_CONST = 15;
    private const int MIN_PASSWORD_UPPERCASE_CONST = 0;
    private const int MAX_PASSWORD_UPPERCASE_CONST = 15;
    private const int MIN_PASSWORD_SPECIALCHARACTER_CONST = 0;
    private const int MAX_PASSWORD_SPECIALCHARACTER_CONST = 15;
    private const int MIN_LOGIN_ATTEMPTS_CONST = 1;
    private const int MAX_LOGIN_ATTEMPTS_CONST = Int32.MaxValue;
    private const int MIN_PASSWORD_HISTORY_CONST = 0;
    private const int MAX_PASSWOD_HISTORY_CONST = 5;
    private const int MIN_CHANGE_PASS_N_DAYS = 0;
    private const int MAX_CHANGE_PASS_N_DAYS = Int32.MaxValue;

    private const int DEFAULT_PASSWOD_LENGTH = 6;
    private const int DEFAULT_LOGIN_LENGTH = 6;
    private const int DEFAULT_COUNT_OF_DIGITS = 2;
    private const int DEFAULT_COUNT_OF_LOWERCASE = 2;
    private const int DEFAULT_COUNT_OF_UPPERCASE = 2;
    private const int DEFAULT_COUNT_OF_SPECIAL_CHARACTER = 0;
    private const int DEFAULT_COUNT_OF_LOGIN_ATTEMPT = 3;
    private const int DEFAULT_COUNT_OF_PASSWORD_HISTORY = 5;
    private const int DEFAULT_COUNT_OF_DAYS_WITHOUT_LOGIN = 24;
    private const int DEFAULT_CHANGE_PASSWORD_IN_N_DAYS = 0;
    private const int DEFAULT_LOGIN_ATTEMPTS = 3;

    // PASSWORD POLICY



    //min and max values

      public  const int MIN_DAYS_WITHOUT_LOGIN_CONST = 0;
      public  const int MAX_DAYS_WITHOUT_LOGIN_CONST = Int32.MaxValue;


      public  const int MIN_TIMEOUT_GUI = 5;
      public  const int MAX_TIMEOUT_GUI = 60;
      public  const int MIN_TIMEOUT_CAJERO = 5;
      public  const int MAX_TIMEOUT_CAJERO = 60;
    //default values

      public  const int DEFAULT_TIME_OUT_GUI = 60;
      public  const int DEFAULT_TIME_OUT_CASHIER = 60;


    #endregion

    public delegate void AutoLogoutEventHandler(String Title, String Message);

    public static event AutoLogoutEventHandler AutoLogout;

    public enum EXIT_CODE
    {
      ABRUPT = -1
    , LOGGED_OUT = 0
    , EXPIRED = 1
    }

    public enum USER_SESSION_STATE
    {
      OPENED = 0
    , CLOSED = 1
    , EXPIRED = 2
    , OWN = 3
    , UNEXPECTED = 4
    }

    public static void OnAutoLogout(String Title, String Message)
    {
      if (AutoLogout != null)
      {
        AutoLogout(Title, Message);
      }
    } // OnAutoLogout
      
    internal class UserActionData
    {
      internal Int64 m_generation_id;
      internal Int64 m_saved_generation_id;

      internal Boolean m_user_logged = false;
      internal Int32 m_current_user_id = -1;
      internal String m_current_username = "";
      internal String m_logon_computer = "";
      internal DateTime m_logged_in = DateTime.MinValue;
      internal EXIT_CODE m_exit_code = EXIT_CODE.ABRUPT;
      internal DateTime m_last_activity = DateTime.MinValue;
      internal String m_last_action = "";
      internal String m_last_action_form = "";

      internal void SetSaved(Int64 SavedId)
      {
        lock (this)
        {
          m_saved_generation_id = SavedId;
        }
      }

      internal Boolean Changed()
      {
        lock (this)
        {
          return m_saved_generation_id != m_generation_id;
        }
      }

      internal UserActionData Copy()
      {
        lock (this)
        {
          return (UserActionData)this.MemberwiseClone();
        }
      }
    }

    private static ReaderWriterLock m_user_lock = new ReaderWriterLock();
    private static UserActionData m_action_data = new UserActionData();

    private static Dictionary<String, String> m_nls = new Dictionary<String, String>();

    public static void Init(String STR_LAST_ACTION,
                            String STR_SESSION_MACHINE,
                            String STR_SESSION_STR1,
                            String STR_SESSION_STR2,
                            String STR_SESSION_STR3,
                            String STR_SESSION_STR4,
                            String STR_SESSION_STR5,
                            String STR_USER_SESSION,
                            String STR_ACTION_OPEN,
                            String STR_ACTION_CLOSE,
                            String STR_ACTION_CONTINUE,
                            String STR_ACTION_RESTART,
                            String STR_SESSION_USER,
                            String STR_SESSION_DATE,
                            String STR_SESSION_SESSION)
    {

      m_nls.Add("STR_LAST_ACTION", STR_LAST_ACTION);
      m_nls.Add("STR_SESSION_MACHINE", STR_SESSION_MACHINE);
      m_nls.Add("STR_SESSION_STR1", STR_SESSION_STR1);
      m_nls.Add("STR_SESSION_STR2", STR_SESSION_STR2);
      m_nls.Add("STR_SESSION_STR3", STR_SESSION_STR3);
      m_nls.Add("STR_SESSION_STR4", STR_SESSION_STR4);
      m_nls.Add("STR_SESSION_STR5", STR_SESSION_STR5);
      m_nls.Add("STR_USER_SESSION", STR_USER_SESSION);
      m_nls.Add("STR_ACTION_OPEN", STR_ACTION_OPEN);
      m_nls.Add("STR_ACTION_CLOSE", STR_ACTION_CLOSE);
      m_nls.Add("STR_ACTION_CONTINUE", STR_ACTION_CONTINUE);
      m_nls.Add("STR_ACTION_RESTART", STR_ACTION_RESTART);
      m_nls.Add("STR_SESSION_USER", STR_SESSION_USER);
      m_nls.Add("STR_SESSION_DATE", STR_SESSION_DATE);
      m_nls.Add("STR_SESSION_SESSION", STR_SESSION_SESSION);

    }

    // PURPOSE: Get information of the user session from db
    //
    //  PARAMS:
    //     - INPUT:
    //           - UserId:
    //           - HasActivity:
    //           - UserName:
    //           - LoggedIn:
    //           - LogonComputer:
    //           - LastActivity:
    //           - WithoutActivity:
    //           - LastAction:
    //           - ExitCode:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - Boolean: True->Process completed False->There is a error of the DB.

    public static Boolean GetActivityUser(      Int32 UserId
                                          , out Boolean HasActivity
                                          , out String UserName
                                          , out DateTime LoggedIn
                                          , out String LogonComputer
                                          , out DateTime LastActivity
                                          , out TimeSpan WithoutActivity
                                          , out String LastAction
                                          , out EXIT_CODE ExitCode)
    {

      Boolean _result;

      using (DB_TRX _bd_trx = new DB_TRX())
      {
        _result = GetActivityUser(UserId, _bd_trx.SqlTransaction, out HasActivity, out UserName, out LoggedIn, out LogonComputer, out LastActivity, out WithoutActivity, out LastAction, out ExitCode);
      }

      return _result;
    } // GetUserInfo


    // PURPOSE: Get information of the user session from db
    //
    //  PARAMS:
    //     - INPUT:
    //           - UserId:
    //           - SqlTrx:
    //           - HasActivity:
    //           - UserName:
    //           - LoggedIn:
    //           - LogonComputer:
    //           - LastActivity:
    //           - WithoutActivity:
    //           - LastAction:
    //           - ExitCode:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - Boolean: True->Process completed False->There is a error of the DB.

    public static Boolean GetActivityUser(Int32 UserId
                                          , SqlTransaction SqlTrx
                                          , out Boolean HasActivity
                                          , out String UserName
                                          , out DateTime LoggedIn
                                          , out String LogonComputer
                                          , out DateTime LastActivity
                                          , out TimeSpan WithoutActivity
                                          , out String LastAction
                                          , out EXIT_CODE ExitCode)
    {

      StringBuilder _sb_sql;
      SqlCommand _sql_cmd;

      _sb_sql = new StringBuilder();

      _sb_sql.Append(" SELECT   GU_LOGGED_IN ");
      _sb_sql.Append("        , GU_LOGON_COMPUTER ");
      _sb_sql.Append("        , GU_LAST_ACTIVITY ");
      _sb_sql.Append("        , GU_LAST_ACTION ");
      _sb_sql.Append("        , GU_EXIT_CODE ");
      _sb_sql.Append("        , GU_USERNAME ");

      _sb_sql.Append(" FROM     GUI_USERS  ");
      _sb_sql.Append(" WHERE    GU_USER_ID = @pUserId ");

      HasActivity = false;
      LoggedIn = DateTime.MinValue;
      LogonComputer = "";
      LastActivity = DateTime.MinValue;
      WithoutActivity = TimeSpan.MinValue;
      LastAction = "";
      ExitCode = EXIT_CODE.ABRUPT;
      UserName = "";

      _sql_cmd = new SqlCommand(_sb_sql.ToString(), SqlTrx.Connection, SqlTrx);
      _sql_cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = UserId;

      try
      {
        using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
        {
          if (_reader.Read())
          {
            if (!_reader.IsDBNull(0))
            {
              HasActivity = true;
              LoggedIn = _reader.GetDateTime(0);
            }

            if (!_reader.IsDBNull(1))
            {
              LogonComputer = _reader.GetString(1).ToUpper();
            }

            UserName = _reader.GetString(5);

            if (!_reader.IsDBNull(2))
            {
              WithoutActivity = WASDB.Now.Subtract(_reader.GetDateTime(2));
              LastActivity = _reader.GetDateTime(2);
            }

            if (!_reader.IsDBNull(3))
            {
              LastAction = _reader.GetString(3);
            }

            if (!_reader.IsDBNull(4))
            {
              ExitCode = (EXIT_CODE)_reader.GetInt16(4);
            }

            _reader.Close();
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error("GetActivityUser. Failed to read values in the DB. Username:" + UserId.ToString() + "\nException:" + _ex.Message);

        return false;
      }
      finally
      {

      }

      return true;
    } // GetUserInfo


    // PURPOSE: Return a state of the user session.
    //
    //  PARAMS:
    //     - INPUT:
    //           - Application: Which is the source application
    //           - UserId:
    //           - LogonComputer: Where is logged
    //           - Message: Message of return its depend of the EXIT_CODE and user session
    //           - Title: Title of return for message boxes
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - USER_SESSION_STATE

    public static USER_SESSION_STATE GetStateUserSession(String Application
                                                        , Int32 UserId
                                                        , String LogonComputer
                                                        , out String Message
                                                        , out String Title)
    {
      Boolean _has_activity;
      String _username;
      DateTime _logged_in;
      String _logon_computer;
      DateTime _last_activity;
      TimeSpan _without_activity;
      String _last_action;
      String _message_exit_code;
      String _message_log;

      Users.EXIT_CODE _exit_code;

      _message_exit_code = "";
      _message_log = "";
      Message = "";
      Title = "";
      _exit_code = EXIT_CODE.ABRUPT;
      Title = GetNLS("STR_USER_SESSION");

      if (UserId == 0)
        return USER_SESSION_STATE.CLOSED;

      using (DB_TRX _bd_trx = new DB_TRX())
      {

        GetActivityUser(UserId
                        , _bd_trx.SqlTransaction
                        , out _has_activity
                        , out _username
                        , out _logged_in
                        , out _logon_computer
                        , out _last_activity
                        , out _without_activity
                        , out _last_action
                        , out _exit_code);

        //Sesi�n:
        //  Fecha:    19/MAR/2012 12:00
        //  Usuario:  MARCOS
        //  M�quina:  WKS-XYZ

        _message_log += "\n\n" + GetNLS("STR_SESSION_SESSION") + ":";
        _message_log += String.Format("\n\n{0} {1}", GetNLS("STR_SESSION_DATE") + ":", _last_activity.ToShortDateString() + " " + _last_activity.ToShortTimeString());
        _message_log += String.Format("\n{0} {1}", GetNLS("STR_SESSION_USER") + ":", _username);
        _message_log += String.Format("\n{0} {1}", GetNLS("STR_SESSION_MACHINE") + ":", _logon_computer);        

        if (_has_activity)
        {

          if (IsExpired(Application, _last_activity,0))
          {
            GetMessageExitCode(EXIT_CODE.EXPIRED, out Message, out Title);
            Message += _message_log;

            return USER_SESSION_STATE.EXPIRED;
          }

          if (LogonComputer.ToUpper() == _logon_computer)
          {
            //
            // If is the same machine, there is a abrupt exit code
            //

            if (_exit_code == EXIT_CODE.ABRUPT)
            {
              Message += GetNLS("STR_SESSION_STR5");
              Message += _message_log;

              return USER_SESSION_STATE.UNEXPECTED;
            }

            return USER_SESSION_STATE.OWN;
          }

          if (_exit_code == EXIT_CODE.EXPIRED)
          {
            GetMessageExitCode(_exit_code, out _message_exit_code, out Title);
            Message += _message_exit_code;
            Message += _message_log;

            return USER_SESSION_STATE.EXPIRED;
          }

          //Ya tiene una sesi�n abierta.
          Message = GetNLS("STR_SESSION_STR1");
          Message += _message_log;
          //Cierre su sesi�n para volver a conectarse.
          Message += "\n\n" + GetNLS("STR_SESSION_STR4");

          return USER_SESSION_STATE.OPENED;
        }
        else
        {

          if (_exit_code == EXIT_CODE.EXPIRED)
          {

            GetMessageExitCode(_exit_code, out _message_exit_code, out Title);
            Message += _message_exit_code;
            Message += _message_log;

            return USER_SESSION_STATE.EXPIRED;
          }

          return USER_SESSION_STATE.CLOSED;
        }

      }
    } // GetStateUserSession

    // PURPOSE: Save the info of user session 
    //
    //  PARAMS:
    //     - INPUT:
    //           - none
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - Boolean: true -> Saved correctly in the db

    public static void SaveUserActivity()
    {
      UserActionData _uad;

      lock (m_action_data)
      {
        if (!m_action_data.Changed())
        {
          return;
        }
        _uad = m_action_data.Copy();
      }

      SaveUserActivity(_uad);

    } // SaveUserActivity

    // PURPOSE: Updated the info of user session 
    //
    //  PARAMS:
    //     - INPUT:
    //           - SqlTrx:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - Boolean: true -> Updated correctly in the db

    private static Boolean SaveUserActivity(UserActionData UAD)
    {
      SqlCommand _sql_cmd;
      StringBuilder _sql_txt;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sql_txt = new StringBuilder();
          _sql_txt.AppendLine("UPDATE   GUI_USERS");
          _sql_txt.AppendLine("   SET   GU_LOGGED_IN      = @pLastLoggedIn");
          _sql_txt.AppendLine("       , GU_LOGON_COMPUTER = @pLogonComputer");
          _sql_txt.AppendLine("       , GU_LAST_ACTIVITY  = @pLastActivity");
          _sql_txt.AppendLine("       , GU_LAST_ACTION    = @pLastAction");
          _sql_txt.AppendLine("       , GU_EXIT_CODE      = @pExitCode");
          _sql_txt.AppendLine(" WHERE   GU_USER_ID        = @pUserId");

          _sql_cmd = new SqlCommand(_sql_txt.ToString());

          _sql_cmd.Parameters.Add("@pLastLoggedIn", SqlDbType.DateTime).Value = UAD.m_user_logged ? (Object) UAD.m_logged_in : (Object) DBNull.Value;
          _sql_cmd.Parameters.Add("@pLogonComputer", SqlDbType.NVarChar, 50).Value = !String.IsNullOrEmpty(UAD.m_logon_computer) ? (Object) UAD.m_logon_computer.ToUpper() : (Object) DBNull.Value;

          if (UAD.m_last_activity != DateTime.MinValue)
          {
            _sql_cmd.Parameters.Add("@pLastAction", SqlDbType.NVarChar, 50).Value = UAD.m_last_action + " - " + UAD.m_last_action_form;
            _sql_cmd.Parameters.Add("@pLastActivity", SqlDbType.DateTime).Value = UAD.m_last_activity;
          }
          else
	        {
            _sql_cmd.Parameters.Add("@pLastAction", SqlDbType.NVarChar, 50).Value = DBNull.Value;
            _sql_cmd.Parameters.Add("@pLastActivity", SqlDbType.DateTime).Value = DBNull.Value;
	        }

          _sql_cmd.Parameters.Add("@pExitCode", SqlDbType.Int).Value = UAD.m_exit_code;
          _sql_cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = UAD.m_current_user_id;

          if (_db_trx.ExecuteNonQuery(_sql_cmd) == 1)
          {
            if (_db_trx.Commit())
            {
              m_action_data.SetSaved(UAD.m_generation_id);

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      
      return false;

    } // SaveUserActivity

    // PURPOSE: Set the logged in of the current user with additional information
    //
    //  PARAMS:
    //     - INPUT:
    //           - UserId:
    //           - UserName:
    //           - ComputerName:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - none

    public static void SetUserLoggedIn(Int32 UserId, String UserName, String ComputerName)
    {
      UserActionData _uad;
      DateTime _now;

      _now = WASDB.Now;
      lock (m_action_data)
      {
        m_action_data.m_user_logged = true;
        m_action_data.m_current_user_id = UserId;
        m_action_data.m_current_username = UserName;
        m_action_data.m_logon_computer = ComputerName;
        m_action_data.m_logged_in = _now;
        m_action_data.m_exit_code = EXIT_CODE.ABRUPT;
        m_action_data.m_last_activity = m_action_data.m_logged_in;
        m_action_data.m_last_action = "";
        m_action_data.m_last_action_form = "";

        m_action_data.m_generation_id++;
        _uad = m_action_data.Copy();
      }

      SaveUserActivity(_uad);
    } // SetUserLoggedIn

    // PURPOSE: Set the action logged out and reset the additional information
    //
    //  PARAMS:
    //     - INPUT:
    //           - UserId:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - none

    public static void SetUserLoggedOff(EXIT_CODE ExitCode)
    {
      UserActionData _uad;
      DateTime _now;

      _now = WASDB.Now;
      lock (m_action_data)
      {
        m_action_data.m_user_logged = false;
        m_action_data.m_exit_code = ExitCode;

        switch (ExitCode)
        {

          case EXIT_CODE.LOGGED_OUT:
            m_action_data.m_last_activity = _now;
            m_action_data.m_last_action = "Logout";
            break;
          case EXIT_CODE.EXPIRED:
            //m_action_data.m_last_activity = _now;
            m_action_data.m_last_action = "Timeout";
            break;
          default:
            //Unexpected
            break;
        }

        m_action_data.m_generation_id++;
        _uad = m_action_data.Copy();
      }

      SaveUserActivity(_uad);

    } // SetUserLoggedOff

    // PURPOSE: Set the last action 
    //
    //  PARAMS:
    //     - INPUT:
    //           - Action: What is the last action
    //           - Form: Where is the las action
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - none

    public static void SetLastAction(String Action, String Form)
    {
      DateTime _now;

      _now = WASDB.Now;

      lock (m_action_data)
      {
        if (!m_action_data.m_user_logged)
        {
          return;
        }
        m_action_data.m_last_activity = _now;
        m_action_data.m_last_action = Action;
        if (!String.IsNullOrEmpty(Form))
        {
          m_action_data.m_last_action_form = Form;
        }        
        m_action_data.m_generation_id++;
      }
    } // SetLastAction 


    // PURPOSE: The session is valid if exists the record of user logon and the session didn't expire.
    //
    //  PARAMS:
    //     - INPUT:
    //           - Application:
    //           - UserLogged:
    //           - UserId:
    //           - LogonComputer:
    //           - Message:
    //           - Title:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - Boolean

    private static Boolean UserSessionExpired( String Application
                                               , out String Message
                                               , out String Title)
    {
      Boolean _expired;
      Int32 _user_id;
      String _logon_computer;
      DateTime _last_activity;

      Message = "";
      Title = "";

      lock (m_action_data)
      {
        if (!m_action_data.m_user_logged)
        {
          return false;
        }
        _logon_computer = m_action_data.m_logon_computer;
        _user_id = m_action_data.m_current_user_id;
        _last_activity = m_action_data.m_last_activity;        
      }

      _expired = IsExpired(Application, _last_activity, 60);

      if (_expired)
      {
        Title = GetNLS("STR_USER_SESSION");
        //Su sesi�n se cerrar� autom�ticamente
        Message += "\n" + GetNLS("STR_ACTION_RESTART");
      }

      return _expired;
    } // UserSessionExpired

    // PURPOSE: Set a messages depending on the EXIT_CODE 
    //
    //  PARAMS:
    //     - INPUT:
    //           - ExitCode:
    //           - Message:
    //           - Title:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - none

    private static void GetMessageExitCode(  EXIT_CODE ExitCode
                                           , out String Message
                                           , out String Title)
    {

      Message = "";
      //Sesi�n usuario;
      Title = GetNLS("STR_USER_SESSION");

      switch (ExitCode)
      {
        case EXIT_CODE.ABRUPT:
          // Su sesi�n se cerr� de forma inesperada.
          Message = GetNLS("STR_SESSION_STR2");
          break;
        case EXIT_CODE.EXPIRED:
          //Su sesi�n se cerr� por inactividad.
          Message = GetNLS("STR_SESSION_STR3");
          break;
        default:
          Message = "";
          break;
      }

    } // GetMessageExitCode

    // PURPOSE: Check if the date "LastAction" is expired.
    //
    //  PARAMS:
    //     - INPUT:
    //           - Application:
    //           - LastAction:
    //           - DeltaSeconds:
    //           - SqlTrx:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - none

    private static Boolean IsExpired(String Application, DateTime LastAction, Int16 Elapse)
    {
      TimeSpan _without_activity;
      TimeSpan _time_expiration;
      int _minutes_expiration;
      String _subject;


      _subject = "SessionTimeOut.Cashier";
      if (Application == "WigosASGUI")
      {
        _subject = "SessionTimeOut.GUI";
      }

      if (!int.TryParse(Misc.ReadGeneralParams("User", _subject), out _minutes_expiration))
      {
        _minutes_expiration = 20;
      }

      _minutes_expiration = Math.Max(_minutes_expiration, 5);
      _minutes_expiration = Math.Min(_minutes_expiration, 60);

      _time_expiration = new TimeSpan(0, _minutes_expiration, 0);
      _without_activity = WASDB.Now.Subtract(LastAction.Subtract(new TimeSpan(0, 0, Elapse)));
      if (_without_activity > _time_expiration)
      {
        return true;
      }
      return false;

    } // IsExpired

    private static String GetNLS(String Key)
    {

      if (m_nls.Count == 0)
      {
        return Resource.String(Key);
      }
      else
      {
        return m_nls[Key];
      }

    }

    // PURPOSE: Check the current user session.
    //          If it has expired, call the specific AutoLogout application event handler.
    //
    //  PARAMS:
    //     - INPUT:
    //           - Application:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - none

    public static void CheckCurrentSession(String Application)
    {
      String _message;
      String _title;

      if (Users.UserSessionExpired(Application, out _message, out _title))
      {
        try
        {
          OnAutoLogout(_title, _message);
        }
        catch (Exception _ex)
        {
          Log.Message("Exception OnAutoLogout. Exit anyway!");
          Log.Exception(_ex);

          Environment.Exit(0);
        }
      }

      Users.SaveUserActivity();
    } // CheckCurrentSession

  }



}


