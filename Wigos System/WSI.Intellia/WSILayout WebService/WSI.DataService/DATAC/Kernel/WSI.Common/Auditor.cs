//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Auditor.cs
// 
//   DESCRIPTION: Auditor class for Cashier
//
//        AUTHOR: MBF
// 
// CREATION DATE: 13-OCT-2011
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 13-OCT-2011 MBF     First release.
//------------------------------------------------------------------------------


using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace WSI.Common
{
  static public class Auditor
  {
    //private static Int64 AuditId;
    private static ENUM_GUI GuiId;
    private static Int32 UserId;
    private static String Username;
    private static String ComputerName;
    private static Int32 AuditCode;
    // private static Int32 ItemOrder;
    private static Int32 AuditLevel;
    private static Int32 NlsId;
    private static String NlsParam01;
    private static String NlsParam02;
    private static String NlsParam03;
    private static String NlsParam04;
    private static String NlsParam05;

    public static bool Audit(ENUM_GUI GuiId
                            , Int32 UserId
                            , String Username
                            , String ComputerName
                            , Int32 AuditCode
                            , Int32 NlsId
                            , String NlsParam01
                            , String NlsParam02
                            , String NlsParam03
                            , String NlsParam04
                            , String NlsParam05)
    {
      Auditor.GuiId = GuiId;
      Auditor.UserId = UserId;
      Auditor.Username = Username;
      Auditor.ComputerName = ComputerName;
      Auditor.AuditCode = AuditCode;
      Auditor.NlsId = NlsId;
      Auditor.NlsParam01 = NlsParam01;
      Auditor.NlsParam02 = NlsParam02;
      Auditor.NlsParam03 = NlsParam03;
      Auditor.NlsParam04 = NlsParam04;
      Auditor.NlsParam05 = NlsParam05;

      AuditLevel = 0;

      return DB_Save();
    } // Audit

    public static bool Audit(Int32 AuditCode
                            , Int32 NlsId
                            , String NlsParam01
                            , String NlsParam02
                            , String NlsParam03
                            , String NlsParam04
                            , String NlsParam05)
    {
      return Audit(GuiId
                  , UserId
                  , Username
                  , ComputerName
                  , AuditCode
                  , NlsId
                  , NlsParam01
                  , NlsParam02
                  , NlsParam03
                  , NlsParam04
                  , NlsParam05);

    } // Audit

    private static bool DB_Save()
    {
      String _sql_str;
      bool _rc;
      Int64 _audit_id;

      _sql_str = "";
      _sql_str = " SELECT MAX(GA_AUDIT_ID) +1 FROM GUI_AUDIT; ";

      _audit_id = 0;

      using (DB_TRX _db_trx = new DB_TRX())
      {
        using (SqlCommand _sql_cmd = new SqlCommand(_sql_str, _db_trx.SqlTransaction.Connection))
        {
          _sql_cmd.Transaction = _db_trx.SqlTransaction;
          _audit_id = (long)_sql_cmd.ExecuteScalar();
        }
      }

      if (_audit_id == 0)
      {
        return false;
      }

      _sql_str = "";
      _sql_str += "INSERT INTO   GUI_AUDIT ";
      _sql_str += " ( GA_AUDIT_ID ";
      _sql_str += " , GA_GUI_ID ";
      _sql_str += " , GA_GUI_USER_ID ";
      _sql_str += " , GA_GUI_USERNAME ";
      _sql_str += " , GA_COMPUTER_NAME ";
      _sql_str += " , GA_DATETIME ";
      _sql_str += " , GA_AUDIT_CODE ";
      _sql_str += " , GA_ITEM_ORDER ";
      _sql_str += " , GA_AUDIT_LEVEL ";
      _sql_str += " , GA_NLS_ID ";
      _sql_str += " , GA_NLS_PARAM01 ";
      _sql_str += " , GA_NLS_PARAM02 ";
      _sql_str += " , GA_NLS_PARAM03 ";
      _sql_str += " , GA_NLS_PARAM04 ";
      _sql_str += " , GA_NLS_PARAM05 ";
      _sql_str += " ) ";
      _sql_str += " VALUES ";
      _sql_str += " ( @pAuditId ";
      _sql_str += " , @pGuiId ";
      _sql_str += " , @pUserId ";
      _sql_str += " , @pUsername ";
      _sql_str += " , @pComputerName ";
      _sql_str += " , GETDATE() ";
      _sql_str += " , @pAuditCode ";
      _sql_str += " , @pItemOrder ";
      _sql_str += " , @pAuditLevel ";
      _sql_str += " , @pNlsId ";
      _sql_str += " , @pParam01 ";
      _sql_str += " , @pParam02 ";
      _sql_str += " , @pParam03 ";
      _sql_str += " , @pParam04 ";
      _sql_str += " , @pParam05 ";
      _sql_str += " ) ";

      using (DB_TRX _db_trx = new DB_TRX())
      {
        using (SqlCommand _sql_cmd = new SqlCommand(_sql_str,_db_trx.SqlTransaction.Connection))
        {
          _sql_cmd.Parameters.Add("@pAuditId", SqlDbType.BigInt).Value = _audit_id;
          _sql_cmd.Parameters.Add("@pGuiId", SqlDbType.Int).Value = GuiId;
          _sql_cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = UserId;
          _sql_cmd.Parameters.Add("@pUsername", SqlDbType.NVarChar,50).Value = Username;
          _sql_cmd.Parameters.Add("@pComputerName", SqlDbType.NVarChar, 50).Value = ComputerName;
          _sql_cmd.Parameters.Add("@pAuditCode", SqlDbType.Int).Value = AuditCode;
          _sql_cmd.Parameters.Add("@pItemOrder", SqlDbType.Int).Value = 0;
          _sql_cmd.Parameters.Add("@pAuditLevel", SqlDbType.Int).Value = AuditLevel;
          _sql_cmd.Parameters.Add("@pNlsId", SqlDbType.Int).Value = NlsId;
          _sql_cmd.Parameters.Add("@pParam01", SqlDbType.NVarChar, 50).Value = NlsParam01;
          _sql_cmd.Parameters.Add("@pParam02", SqlDbType.NVarChar, 50).Value = NlsParam02;
          _sql_cmd.Parameters.Add("@pParam03", SqlDbType.NVarChar, 50).Value = NlsParam03;
          _sql_cmd.Parameters.Add("@pParam04", SqlDbType.NVarChar, 50).Value = NlsParam04;
          _sql_cmd.Parameters.Add("@pParam05", SqlDbType.NVarChar, 50).Value = NlsParam05;

          _sql_cmd.Transaction = _db_trx.SqlTransaction;

          _rc = (_sql_cmd.ExecuteNonQuery() == 1);

          _db_trx.SqlTransaction.Commit();

          return _rc;
        }
      }
    } // DB_Save

  }
}
