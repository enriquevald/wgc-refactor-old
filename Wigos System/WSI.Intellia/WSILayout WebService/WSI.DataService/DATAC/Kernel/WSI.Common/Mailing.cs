//------------------------------------------------------------------------------
// Copyright � 2010 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Mailing.cs
// 
//   DESCRIPTION: 
// 
//        AUTHOR: Raul Cervera
// 
// CREATION DATE: 27-DEC-2010
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 27-DEC-2010 RCI    First release.
// 19-JAN-2012 JMM    Adding account movements mail type
// 24-JAN-2012 RCI    SiteId is 3 char lenght, not 4.
// 23-FEB-2012 RCI&JMM    Fixed bug on SendEmail when MailingInstanceId is 0.
// 13-APR-2012 MPO    Defect 219: StatsMailingReport --> Added new field with name site_jackpot
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using WSI.Common;
using System.Data.SqlClient;
using System.Data;
using System.Net.Mail;
using System.Net;
using System.IO;

namespace WSI.Common
{
  public static class Mailing
  {
    public const Int32 SQL_COMMAND_TIMEOUT = 60;

    private const Int32 WAIT_SHORT = (1 * 60 * 1000);                   // 1 minute
    private const Int32 WAIT_LONG  = (5 * 60 * 1000);                   // 5 minute
    private const Int32 WAIT_FOR_AUTHORIZED_SERVER = WAIT_LONG;         // WAIT_LONG minutes
    private const Int32 DELAY_DAILY_STATISTICS_SECS = (5 * 60);         // 5 minutes
    private const Int32 SECONDS_IN_ONE_DAY = (24 * 60 * 60);

    internal const Int32 WSI_STATISTICS_DAYS_FROM = 7;

    static Thread m_thread;
    static TYPE_MAILING_PARAMETERS m_mailing_parameters = new TYPE_MAILING_PARAMETERS();

    #region Public Functions

    //------------------------------------------------------------------------------
    // PURPOSE : Initializes and Starts WCP_Mailing class 
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static void Start()
    {
      m_thread = new Thread(MailingThread);
      m_thread.Name = "Mailing";
      m_thread.Start();
    } // Init

    //------------------------------------------------------------------------------
    // PURPOSE : Read Mailing Parameters from General Params. 
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //          - TYPE_MAILING_PARAMETERS Parameters
    //
    // RETURNS :
    //      - True: Read ok. False: Otherwise.
    //
    public static Boolean ReadParameters(out TYPE_MAILING_PARAMETERS Parameters)
    {
      String _value;

      Parameters = new TYPE_MAILING_PARAMETERS();

      Parameters.authorized_server_list = "";
      Parameters.enabled = false;
      Parameters.smtp_enabled = false;
      Parameters.smtp_server = "";
      Parameters.smtp_port = 25;
      Parameters.smtp_secured = false;
      Parameters.smtp_username = "";
      Parameters.smtp_password = "";
      Parameters.smtp_domain = "";
      Parameters.smtp_email_from = "";

      try
      {
        Parameters.authorized_server_list = GeneralParam.Value("Mailing", "AuthorizedServerList");
        _value = GeneralParam.Value("Mailing", "Enabled");
        Parameters.enabled = (_value != "" && _value != "0");
        _value = GeneralParam.Value("Mailing", "SMTP.Enabled");
        Parameters.smtp_enabled = (_value != "" && _value != "0");
        Parameters.smtp_server = GeneralParam.Value("Mailing", "SMTP.Connection.ServerAddress");
        if (!Int32.TryParse(GeneralParam.Value("Mailing", "SMTP.Connection.ServerPort"), out Parameters.smtp_port))
        {
          Parameters.smtp_port = 25;
        }
        _value = GeneralParam.Value("Mailing", "SMTP.Connection.Secured");
        Parameters.smtp_secured = (_value != "" && _value != "0");
        Parameters.smtp_username = GeneralParam.Value("Mailing", "SMTP.Credentials.UserName");
        _value = GeneralParam.Value("Mailing", "SMTP.Credentials.Password");
        try
        {
          Parameters.smtp_password = Misc.HexToString(_value);
        }
        catch
        {
          // AJQ & XCD 30-JAN-2012 Avoid wrong hexadecimal password
          // We can't display a MessageBox because this is called by the services
          Parameters.smtp_password = "";    
        }

        Parameters.smtp_domain = GeneralParam.Value("Mailing", "SMTP.Credentials.Domain");
        Parameters.smtp_email_from = GeneralParam.Value("Mailing", "SMTP.Credentials.EMailAddress");

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    } // ReadParameters

    //------------------------------------------------------------------------------
    // PURPOSE : Send test email
    //
    //  PARAMS :
    //      - INPUT :
    //          - MailingParams
    //          - AddressList
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    // NOTES :
    //      - Throw exception in case of error
    //
    public static void SendTestEmail(TYPE_MAILING_PARAMETERS MailingParams,
                                     String AddressList)
    {
      String _subject;
      String _body;
     
      _subject = "Test Email (Don't reply)";
      _body = "Sent only for testing purposes.";

      SendEmail(MailingParams, 0, AddressList, _subject, _body, false);
    } // SendTestEmail

    #endregion // Public Functions

    #region Private Functions

    //------------------------------------------------------------------------------
    // PURPOSE : Main thread
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    static private void MailingThread()
    {
      SqlConnection _sql_conn;
      DateTime _now;

      while (true)
      {
        _now = WASDB.Now;
        if (_now.Minute >= 55 || _now.Minute <= 10)
        {
          Thread.Sleep(WAIT_SHORT);
        }
        else
        {
          Thread.Sleep(WAIT_LONG);
        }

        if (!Mailing.ReadParameters(out m_mailing_parameters))
        {
          Log.Error("Can't read Mailing Parameters!");
          continue;
        }

        if (!CheckAuthorizedServer(m_mailing_parameters.authorized_server_list))
        {
          Thread.Sleep(WAIT_FOR_AUTHORIZED_SERVER);
          continue;
        }

        _sql_conn = null;

        try
        {
          //
          // Connect to DB
          //
          _sql_conn = WASDB.Connection();

          if (_sql_conn == null)
          {
            continue;
          }

          // Check connection states
          if (_sql_conn.State != ConnectionState.Open)
          {
            if (_sql_conn.State != ConnectionState.Broken)
            {
              _sql_conn.Close();
            }

            _sql_conn.Dispose();
            _sql_conn = null;

            continue;
          }

          //
          // Process Programming Job
          //
          if (m_mailing_parameters.enabled)
          {
            CreateJobInstances(_sql_conn);
          }

          ProcessJobInstances(_sql_conn);

          if (m_mailing_parameters.smtp_enabled)
          {
            NotifyJobInstancesProcessed(_sql_conn);
          }

        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
        finally
        {
          if (_sql_conn != null)
          {
            try { _sql_conn.Close(); }
            catch { }
            _sql_conn = null;
          }
        }

      } // while

    } // MailingThread

    //------------------------------------------------------------------------------
    // PURPOSE : Check if this server is in the list of authorized servers
    //
    //  PARAMS :
    //      - INPUT :
    //          - String AuthorizedServerList
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean: True if server is authorized. False, otherwise.
    //
    private static Boolean CheckAuthorizedServer(String AuthorizedServerList)
    {
      String[] _servers;
      String[] _sep = new String[1];
      String _machine_name;

      try
      {
        _machine_name = Environment.MachineName.Trim().ToUpper();

        _sep[0] = ",";
        _servers = AuthorizedServerList.Split(_sep, StringSplitOptions.RemoveEmptyEntries);
        foreach (String _server in _servers)
        {
          if (_server.Trim().ToUpper() == _machine_name)
          {
            return true;
          }
        }
      }
      catch { }

      return false;
    } // CheckAuthorizedServer

    //------------------------------------------------------------------------------
    // PURPOSE : Create Job Instances
    //
    //  PARAMS :
    //      - INPUT :
    //          - SqlConn
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static void CreateJobInstances(SqlConnection SqlConn)
    {
      DataTable _dt_prog;
      SqlDataAdapter _sql_da;
      SqlCommand _sql_cmd;
      StringBuilder _sql_sb;
      SqlTransaction _sql_trx;
      
      Int64 _prog_id;
      String _prog_name;
      MAILING_PROGRAMMING_TYPE _prog_type;
      String _address_list;
      String _subject;
      Int32 _weekday;
      Int32 _time_from;
      Int32 _time_to;
      Int32 _time_step;
      DateTime _date_now;
      DateTime _date_last_process_ok;

      Int32 _time_sched;
      Int32 _time_now;
      TimeSpan _span_now;
      DateTime _date_sched;
      Boolean _dummy_already_processed;

      _sql_sb = new StringBuilder();
      _sql_sb.AppendLine("SELECT   MP_PROG_ID ");
      _sql_sb.AppendLine("       , MP_NAME ");
      _sql_sb.AppendLine("       , MP_TYPE ");
      _sql_sb.AppendLine("       , MP_ADDRESS_LIST ");
      _sql_sb.AppendLine("       , MP_SUBJECT ");
      _sql_sb.AppendLine("       , MP_SCHEDULE_WEEKDAY ");
      _sql_sb.AppendLine("       , MP_SCHEDULE_TIME_FROM ");
      _sql_sb.AppendLine("       , MP_SCHEDULE_TIME_TO ");
      _sql_sb.AppendLine("       , MP_SCHEDULE_TIME_STEP ");
      _sql_sb.AppendLine("       , GETDATE () AS MP_NOW ");
      _sql_sb.AppendLine("       , (SELECT   ISNULL(MAX(MI_DATETIME), GETDATE() - 1) ");
      _sql_sb.AppendLine("            FROM   MAILING_INSTANCES ");
      _sql_sb.AppendLine("           WHERE   MI_PROG_ID = MP_PROG_ID ");
      _sql_sb.AppendLine("         ) AS MP_LAST_PROCESS_OK ");
      _sql_sb.AppendLine("  FROM   MAILING_PROGRAMMING ");
      _sql_sb.AppendLine(" WHERE   MP_ENABLED = 1 ");
      _sql_sb.AppendLine("ORDER BY MP_PROG_ID ");

      _sql_cmd = new SqlCommand(_sql_sb.ToString(), SqlConn);

      _dt_prog = new DataTable("PROGRAMMING");
      _sql_da = new SqlDataAdapter();
      _sql_da.SelectCommand = _sql_cmd;

      _sql_da.Fill(_dt_prog);

      _sql_cmd.Dispose();
      _sql_da.Dispose();

      foreach (DataRow _dr_mailing in _dt_prog.Rows)
      {
        _sql_trx = null;

        try
        {
          _prog_id = (Int64)_dr_mailing["MP_PROG_ID"];
          _prog_name = (String)_dr_mailing["MP_NAME"];
          _prog_type = (MAILING_PROGRAMMING_TYPE)_dr_mailing["MP_TYPE"];
          _address_list = (String)_dr_mailing["MP_ADDRESS_LIST"];
          _subject = (String)_dr_mailing["MP_SUBJECT"];
          _weekday = (Int32)_dr_mailing["MP_SCHEDULE_WEEKDAY"];
          _time_from = (Int32)_dr_mailing["MP_SCHEDULE_TIME_FROM"];
          _time_to = (Int32)_dr_mailing["MP_SCHEDULE_TIME_TO"];
          _time_step = (Int32)_dr_mailing["MP_SCHEDULE_TIME_STEP"];
          _date_now = (DateTime)_dr_mailing["MP_NOW"];
          _date_last_process_ok = (DateTime)_dr_mailing["MP_LAST_PROCESS_OK"];

          // Eliminate seconds part of the DateTime.
          _date_now = new DateTime(_date_now.Year, _date_now.Month, _date_now.Day, _date_now.Hour, _date_now.Minute, 0);

          _span_now = _date_now.Subtract(_date_now.Date);
          _time_now = (Int32)_span_now.TotalSeconds;

          _time_sched = CalculateSchedTime(_time_now, _time_from, _time_to, _time_step);
          if (_time_sched < 0)
          {
            Log.Error("WCP_Mailing: Error calculating Sched Time. ProgId: " + _prog_id + ", Name: " + _prog_name + ".");
            
            continue;
          }

          _date_sched = new DateTime(_date_now.Year, _date_now.Month, _date_now.Day);
          _date_sched = _date_sched.AddSeconds(_time_sched);

          if (HaveToProcess (_prog_type, _date_now, _date_sched, _date_last_process_ok,
                             _weekday, _time_from, _time_to))
          {
            _sql_trx = SqlConn.BeginTransaction();

            DB_InsertInstance (_prog_id, _date_sched, _prog_name, _prog_type, _address_list, _subject,
                               out _dummy_already_processed, _sql_trx);

            _sql_trx.Commit();
          }
        }
        catch ( Exception _ex )
        {
          Log.Exception (_ex);

          return;
        }
        finally
        {
          if ( _sql_trx != null )
          {
            if ( _sql_trx.Connection != null )
            {
              try { _sql_trx.Rollback(); }
              catch { }
            }
            _sql_trx = null;
          }
        }
      } // foreach

    } // CreateJobInstances

    //------------------------------------------------------------------------------
    // PURPOSE : Process Job Instances
    //
    //  PARAMS :
    //      - INPUT :
    //          - SqlConn
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static void ProcessJobInstances(SqlConnection SqlConn)
    {
      DataTable _dt_pending_instances;
      Int64 _job_instance_id;
      MailingInstance _prog_report;

      _dt_pending_instances = GetInstances(MAILING_INSTANCES_STATUS.PENDING, SqlConn);
      if (_dt_pending_instances == null)
      {
        return;
      }

      foreach (DataRow _dr_instance in _dt_pending_instances.Rows)
      {
        try
        {
          _job_instance_id = (Int64)_dr_instance["MI_MAILING_INSTANCE_ID"];
          
          _prog_report = MailingInstance.Create(_job_instance_id);
          if (_prog_report != null)
          {
            _prog_report.Process();
          }
        }
        catch ( Exception _ex )
        {
          Log.Exception (_ex);

          return;
        }
      }
    } // ProcessJobInstances

    //------------------------------------------------------------------------------
    // PURPOSE : Notify Job Instances Processed
    //
    //  PARAMS :
    //      - INPUT :
    //          - SqlConn
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static void NotifyJobInstancesProcessed(SqlConnection SqlConn)
    {
      DataTable _dt_ready_instances;
      Int64 _job_instance_id;
      String _address_list;
      String _subject;
      String _body;
      SqlTransaction _sql_trx;

      _dt_ready_instances = GetInstances(MAILING_INSTANCES_STATUS.READY, SqlConn);
      if (_dt_ready_instances == null)
      {
        return;
      }

      foreach (DataRow _dr_instance in _dt_ready_instances.Rows)
      {
        _sql_trx = null;

        try
        {
          _job_instance_id = (Int64)_dr_instance["MI_MAILING_INSTANCE_ID"];
          _address_list = (String)_dr_instance["MI_ADDRESS_LIST"];
          _subject = (String)_dr_instance["MI_SUBJECT"];
          _body = (String)_dr_instance["MI_MESSAGE"];
 
          _sql_trx = SqlConn.BeginTransaction ();

          if (NotifyJobInstanceProcessed(_job_instance_id, _address_list, _subject, _body, _sql_trx))
          {
            _sql_trx.Commit ();
          }
        }
        catch ( Exception _ex )
        {
          Log.Exception (_ex);

          return;
        }
        finally
        {
          if ( _sql_trx != null )
          {
            if ( _sql_trx.Connection != null )
            {
              try { _sql_trx.Rollback (); }
              catch { }
            }
            _sql_trx = null;
          }
        }
      } // foreach

    } // NotifyJobInstancesProcessed

    //------------------------------------------------------------------------------
    // PURPOSE : Calculate the sched time near to TimeNow
    //
    //  PARAMS :
    //      - INPUT :
    //          - Int32 TimeNow
    //          - Int32 TimeFrom
    //          - Int32 TimeTo
    //          - Int32 TimeStep
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - TimeSched: Int32
    //
    private static Int32 CalculateSchedTime(Int32 TimeNow, Int32 TimeFrom, Int32 TimeTo, Int32 TimeStep)
    {
      Int32 _t0;
      Int32 _t1;

      // Only once
      if (TimeStep == 0)
      {
        return TimeFrom;
      }

      if (TimeFrom >= TimeTo)
      {
        TimeTo += SECONDS_IN_ONE_DAY;
      }
      if (TimeFrom > TimeNow)
      {
        TimeNow += SECONDS_IN_ONE_DAY;
      }

      for (_t0 = TimeFrom; _t0 <= TimeTo; _t0 += TimeStep)
      {
        _t1 = _t0 + TimeStep;

        if (   _t0 <= TimeNow 
            && TimeNow <= _t1 )
        {
          return _t0 % SECONDS_IN_ONE_DAY;
        }
        if (_t1 > TimeTo)
        {
          return _t0 % SECONDS_IN_ONE_DAY;
        }
      }

      // Unreachable code
      return -1;
    } // CalculateSchedTime

    //------------------------------------------------------------------------------
    // PURPOSE : Check if have to process
    //
    //  PARAMS :
    //      - INPUT :
    //          - MAILING_PROGRAMMING_TYPE ProgType
    //          - DateTime DateNow
    //          - DateTime DateSched
    //          - DateTime DateLastProcessOk
    //          - Int32 Weekday
    //          - Int32 TimeFrom
    //          - Int32 TimeTo
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True: Have to process
    //      - False: Otherwise
    //
    private static Boolean HaveToProcess(MAILING_PROGRAMMING_TYPE ProgType,
                                         DateTime DateNow,
                                         DateTime DateSched,
                                         DateTime DateLastProcessOk,
                                         Int32 Weekday,
                                         Int32 TimeFrom,
                                         Int32 TimeTo)
    {
      Int32 _day_mask;
      Int32 _time_now;
      TimeSpan _span_now;

      // Delay apply to all the MAILING_PROGRAMMING_TYPE to avoid differences in email reports.
      DateNow = DateNow.AddSeconds(-DELAY_DAILY_STATISTICS_SECS);

      _span_now = DateNow.Subtract(DateNow.Date);
      _time_now = (Int32)_span_now.TotalSeconds;

      _day_mask = (1 << (Int32)Common.Misc.Opening(DateNow).DayOfWeek);
      if ((Weekday & _day_mask) != _day_mask)
      {
        return false;
      }

      return (DateLastProcessOk < DateSched &&
              DateSched <= DateNow);

    } // HaveToProcess

    //------------------------------------------------------------------------------
    // PURPOSE : Obtain the instances filtered by Status
    //
    //  PARAMS :
    //      - INPUT :
    //          - MAILING_INSTANCES_STATUS Status
    //          - SqlConnection SqlConn
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - DataTable
    //
    private static DataTable GetInstances(MAILING_INSTANCES_STATUS Status, SqlConnection SqlConn)
    {
      DataTable _dt_instances;
      SqlDataAdapter _sql_da;
      SqlCommand _sql_cmd;
      StringBuilder _sql_sb;

      try
      {
        _sql_sb = new StringBuilder();
        _sql_sb.AppendLine("SELECT   MI_MAILING_INSTANCE_ID ");
        _sql_sb.AppendLine("       , MI_PROG_ID ");
        _sql_sb.AppendLine("       , MI_PROG_DATE ");
        _sql_sb.AppendLine("       , MI_PROG_DATA ");
        _sql_sb.AppendLine("       , MI_TYPE ");
        _sql_sb.AppendLine("       , MI_ADDRESS_LIST ");
        _sql_sb.AppendLine("       , MI_SUBJECT ");
        _sql_sb.AppendLine("       , MI_MESSAGE ");
        _sql_sb.AppendLine("  FROM   MAILING_INSTANCES ");
        _sql_sb.AppendLine(" WHERE   MI_STATUS = @pStatus ");
        _sql_sb.AppendLine("ORDER BY MI_PROG_ID ");
        _sql_sb.AppendLine("       , MI_PROG_DATE ");
        _sql_sb.AppendLine("       , MI_PROG_DATA ");

        _sql_cmd = new SqlCommand(_sql_sb.ToString(), SqlConn);
        _sql_cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = Status;

        _dt_instances = new DataTable("INSTANCES");
        _sql_da = new SqlDataAdapter();
        _sql_da.SelectCommand = _sql_cmd;

        _sql_da.Fill(_dt_instances);

        _sql_cmd.Dispose();
        _sql_da.Dispose();

        return _dt_instances;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return null;
      }
    } // GetInstances

    //------------------------------------------------------------------------------
    // PURPOSE : Notify Job Instance Processed
    //
    //  PARAMS :
    //      - INPUT :
    //          - Int64 MailingInstanceId
    //          - String AddressList
    //          - String Subject
    //          - String Body
    //          - SqlTransaction SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True: Job executed ok
    //      - False: Otherwise
    //
    private static Boolean NotifyJobInstanceProcessed(Int64 MailingInstanceId,
                                                      String AddressList,
                                                      String Subject,
                                                      String Body,
                                                      SqlTransaction SqlTrx)
    {
      MAILING_INSTANCES_STATUS _old_status;
      MAILING_INSTANCES_STATUS _new_status;
      String _msg;

      _old_status = MAILING_INSTANCES_STATUS.READY;
      _new_status = MAILING_INSTANCES_STATUS.SENT;

      try
      {
        if (!DB_UpdateInstance(MailingInstanceId, _old_status, _new_status, "", "", SqlTrx))
        {
          return false;
        }
        _old_status = MAILING_INSTANCES_STATUS.SENT;
        _new_status = MAILING_INSTANCES_STATUS.ERROR_CONVERSION;

        // Send email: In case of send error, throws exception... In other error cases, return false.
        if (SendEmail(m_mailing_parameters, MailingInstanceId, AddressList, Subject, Body, true))
        {
          _new_status = MAILING_INSTANCES_STATUS.SENT;
        }
      }
      catch (Exception _ex)
      {
        _msg = "                                    " + _ex.Message;
        _new_status = MAILING_INSTANCES_STATUS.ERROR_SENDING;

        while (_ex.InnerException != null)
        {
          _ex = _ex.InnerException;
          _msg += "\r\n                                    " + _ex.Message;
        }

        Log.Error("   Mailing. Excepction:\r\n" + _msg + "\r\n                                 SendEmail: SMTP: " +
                  m_mailing_parameters.smtp_server + ":" + m_mailing_parameters.smtp_port +
                  ", SSL: " + m_mailing_parameters.smtp_secured + ",\r\n                                 User: " +
                  m_mailing_parameters.smtp_username + ", Domain: " + m_mailing_parameters.smtp_domain +
                  ", AddressList " + AddressList + ".");
      }

      return DB_UpdateInstance(MailingInstanceId, _old_status, _new_status, "", "", SqlTrx);
    } // NotifyJobInstanceProcessed

    //------------------------------------------------------------------------------
    // PURPOSE : Send email
    //
    //  PARAMS :
    //      - INPUT :
    //          - MailingParams
    //          - MailingInstanceId
    //          - AddressList
    //          - Subject
    //          - Body
    //          - IsBodyHtml
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True:  Sent ok.
    //      - False: Error Conversion.
    //
    // NOTES :
    //      - Throw exception in case of Send error.
    //
    private static Boolean SendEmail(TYPE_MAILING_PARAMETERS MailingParams,
                                     Int64 MailingInstanceId,
                                     String AddressList,
                                     String Subject,
                                     String Body,
                                     Boolean IsBodyHtml)
    {
      SmtpClient _smtp_client;
      MailAddress _ma_from;
      MailMessage _mm_msg;
      Attachment _mm_attach;
      String _xml_filename;
      String _compressed_xml_filename;
      String _excel_filename;
      String _name_dummy;
      DateTime _sched;
      MAILING_PROGRAMMING_TYPE _type;
      String _subject_dummy;
      DateTime _date_opening;
      DateTime _date_from;
      DateTime _date_to;

      _type = MAILING_PROGRAMMING_TYPE.CASH_INFORMATION;
      _date_opening = DateTime.MinValue;
      
      _xml_filename = "";
      _excel_filename = "";
      _compressed_xml_filename = "";
      
      _mm_msg = null;

      try
      {
        if (MailingInstanceId > 0)
        {
          if (!ReadInstance(MailingInstanceId, out _name_dummy, out _sched, out _type, out _subject_dummy))
          {
            Log.Error("SendEmail: Error reading mailing instance " + MailingInstanceId + ".");

            return false;
          }

          // RCI & JMM 23-FEB-2012: Calculate _date_opening only when _sched has a valid value.
          _date_opening = Misc.Opening(_sched);
          // Eliminate the Minutes part.
          _date_opening = _date_opening.AddMinutes(-_date_opening.Minute);
        }

        _smtp_client = new SmtpClient();
        _smtp_client.Host = MailingParams.smtp_server;
        _smtp_client.Port = MailingParams.smtp_port;
        _smtp_client.EnableSsl = MailingParams.smtp_secured;
        _smtp_client.DeliveryMethod = SmtpDeliveryMethod.Network;

        _smtp_client.Credentials = new NetworkCredential(MailingParams.smtp_username,
                                                         MailingParams.smtp_password,
                                                         MailingParams.smtp_domain);

        _ma_from = new MailAddress(MailingParams.smtp_email_from);
        _mm_msg = new MailMessage();

        _mm_msg.From = _ma_from;
        _mm_msg.Bcc.Add(FormatAddressList(AddressList));

        _mm_msg.Subject = Subject;
        _mm_msg.SubjectEncoding = Encoding.UTF8;

        switch (_type)
        {
          case MAILING_PROGRAMMING_TYPE.CASH_INFORMATION:
            _mm_msg.Body = Body;
            _mm_msg.BodyEncoding = Encoding.UTF8;
            _mm_msg.IsBodyHtml = IsBodyHtml;
            break;

          case MAILING_PROGRAMMING_TYPE.WSI_STATISTICS:
            DataSet _ds_games;            

            _date_from = _date_opening.AddDays(-WSI_STATISTICS_DAYS_FROM);
            _date_to = _date_opening;

            _excel_filename = GetAttachmentFilename("WSI_STATISTICS", _date_from, _date_to);
            _xml_filename += ".xml";

            if (!Misc.WriteStringToFile(_xml_filename, Body))
            {
              Log.Error("SendEmail: Error writing file " + _xml_filename + ".");

              return false;
            }

            _ds_games = new DataSet();
            _ds_games.ReadXml(_xml_filename);

            if (!ExcelConversion.GameStatisticsToExcel(_ds_games, _date_from, _date_to, ref _excel_filename))
            {
              Log.Error("SendEmail: Error creating Excel file " + _excel_filename + ".");

              return false;
            }

            _mm_msg.Body = "";

            _mm_attach = new Attachment(_excel_filename, System.Net.Mime.MediaTypeNames.Application.Octet);
            _mm_msg.Attachments.Add(_mm_attach);
            break;

          case MAILING_PROGRAMMING_TYPE.CONNECTED_TERMINALS:
            DataSet _ds_connected_terminals;

            if (_date_opening.Day > 1)
            {
              _date_from = _date_opening.AddDays(-_date_opening.Day + 1);
              _date_to = _date_opening.AddDays(-1);
            }
            else
            {
              _date_from = _date_opening.AddDays(-_date_opening.Day + 1).AddMonths(-1);
              _date_to = _date_from.AddMonths(1).AddDays(-1);
            }

            _excel_filename = GetAttachmentFilename("CONNECTED_TERMINALS", _date_from, _date_to);
            _xml_filename += ".xml";

            if (!Misc.WriteStringToFile(_xml_filename, Body))
            {
              Log.Error("SendEmail: Error writing file " + _xml_filename + ".");

              return false;
            }

            _ds_connected_terminals = new DataSet();
            _ds_connected_terminals.ReadXml(_xml_filename);

            if (!ExcelConversion.ConnectedTerminalsToExcel(_ds_connected_terminals, _date_from, _date_to, ref _excel_filename))
            {
              Log.Error("SendEmail: Error creating Excel file " + _excel_filename + ".");

              return false;
            }

            _mm_msg.Body = "";

            _mm_attach = new Attachment(_excel_filename, System.Net.Mime.MediaTypeNames.Application.Octet);
            _mm_msg.Attachments.Add(_mm_attach);
            break;

          case MAILING_PROGRAMMING_TYPE.ACCOUNT_MOVEMENTS:

            _date_from = _date_opening.AddDays(-1);
            _date_to = _date_from;

            _xml_filename = GetAttachmentFilename("ACCOUNT_MOVEMENTS", _date_from, _date_to);
            _xml_filename += ".xml";
            _compressed_xml_filename = _xml_filename + ".compressed";

            if (!Misc.WriteStringToFile(_xml_filename, Body))
            {
              Log.Error("SendEmail: Error writing file " + _xml_filename + ".");

              return false;
            }

            //batch calling to compress the attachment
            System.Diagnostics.Process.Start(@"WSI.Compress.bat ", "\"" + _xml_filename + "\" \"" + _compressed_xml_filename);

            _mm_msg.Body = "";

            _mm_attach = new Attachment(_compressed_xml_filename, System.Net.Mime.MediaTypeNames.Application.Octet);
            _mm_msg.Attachments.Add(_mm_attach);
            break;

          default:
            Log.Error("SendEmail: Mailing Type not known '" + _type + "'.");

            return false;
        }

        _smtp_client.Send(_mm_msg);

        return true;
      }
      finally
      {
        if (_mm_msg != null)
        {
          _mm_msg.Dispose();
        }
        if (_xml_filename != "")
        {
          try
          {
            // Delete attached file
            System.IO.File.Delete(_xml_filename);

            if (_excel_filename != "")
            {
              System.IO.File.Delete(_excel_filename);
            }

            if (_compressed_xml_filename != "")
            {
              System.IO.File.Delete(_compressed_xml_filename);
            }
          }
          catch (Exception _ex)
          {
            Log.Error("SendEmail: Error deleting attachment file " + _excel_filename != "" ? _compressed_xml_filename : _excel_filename + ".");
            Log.Exception(_ex);
          }
        }
      }
    } // SendEmail

    //------------------------------------------------------------------------------
    // PURPOSE : Insert a row in table MAILING_INSTANCES
    //
    //  PARAMS :
    //      - INPUT :
    //          - Int64 ProgId
    //          - DateTime DateSched
    //          - String ProgName
    //          - MAILING_PROGRAMMING_TYPE ProgType,
    //          - String AddressList
    //          - String Subject
    //          - SqlTransaction SqlTrx
    //
    //      - OUTPUT :
    //          - Boolean AlreadySent
    //
    // RETURNS :
    //      - True: Insert ok
    //      - False: Otherwise
    //
    private static Boolean DB_InsertInstance(Int64 ProgId,
                                             DateTime DateSched,
                                             String ProgName,
                                             MAILING_PROGRAMMING_TYPE ProgType,
                                             String AddressList,
                                             String Subject,
                                             out Boolean AlreadyProcessed,
                                             SqlTransaction SqlTrx)
    {
      StringBuilder _sql_sb;
      SqlCommand _sql_cmd;
      Int64 _prog_data;

      AlreadyProcessed = false;

      try
      {
        switch (ProgType)
        {
          case MAILING_PROGRAMMING_TYPE.CASH_INFORMATION:
          case MAILING_PROGRAMMING_TYPE.WSI_STATISTICS:
            _prog_data = 0;
            break;
          default:
            _prog_data = 0;
            break;
        }

        _sql_sb = new StringBuilder();
        _sql_sb.AppendLine("INSERT INTO MAILING_INSTANCES (MI_PROG_ID ");
        _sql_sb.AppendLine("                             , MI_PROG_DATE ");
        _sql_sb.AppendLine("                             , MI_PROG_DATA ");
        _sql_sb.AppendLine("                             , MI_NAME ");
        _sql_sb.AppendLine("                             , MI_TYPE ");
        _sql_sb.AppendLine("                             , MI_DATETIME ");
        _sql_sb.AppendLine("                             , MI_ADDRESS_LIST ");
        _sql_sb.AppendLine("                             , MI_SUBJECT ");
        _sql_sb.AppendLine("                             , MI_MESSAGE ");
        _sql_sb.AppendLine("                             , MI_STATUS ");
        _sql_sb.AppendLine("                              ) ");
        _sql_sb.AppendLine("                       VALUES (@pProgId ");
        _sql_sb.AppendLine("                             , @pProgDate ");
        _sql_sb.AppendLine("                             , @pProgData ");
        _sql_sb.AppendLine("                             , @pName ");
        _sql_sb.AppendLine("                             , @pType ");
        _sql_sb.AppendLine("                             , GETDATE () ");
        _sql_sb.AppendLine("                             , @pAddressList ");
        _sql_sb.AppendLine("                             , @pSubject ");
        _sql_sb.AppendLine("                             , '' ");
        _sql_sb.AppendLine("                             , @pStatusPending ) ");

        _sql_cmd = new SqlCommand(_sql_sb.ToString(), SqlTrx.Connection, SqlTrx);

        _sql_cmd.Parameters.Add("@pProgId", SqlDbType.BigInt).Value = ProgId;
        _sql_cmd.Parameters.Add("@pProgDate", SqlDbType.DateTime).Value = DateSched;
        _sql_cmd.Parameters.Add("@pProgData", SqlDbType.BigInt).Value = _prog_data;
        _sql_cmd.Parameters.Add("@pName", SqlDbType.NVarChar).Value = ProgName;
        _sql_cmd.Parameters.Add("@pType", SqlDbType.Int).Value = ProgType;
        _sql_cmd.Parameters.Add("@pAddressList", SqlDbType.NVarChar).Value = AddressList;
        _sql_cmd.Parameters.Add("@pSubject", SqlDbType.NVarChar).Value = Subject;
        _sql_cmd.Parameters.Add("@pStatusPending", SqlDbType.Int).Value = MAILING_INSTANCES_STATUS.PENDING;

        return (_sql_cmd.ExecuteNonQuery() == 1);
      }
      catch (SqlException _sql_ex)
      {
        if (_sql_ex.Number == 2601 || _sql_ex.Number == 2627) // Unique Key Violation
        {
          // Do nothing, the other WCP service already sent this email.
          AlreadyProcessed = true;

          return true;
        }
        Log.Exception(_sql_ex);

        return false;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    } // DB_InsertInstance

    //------------------------------------------------------------------------------
    // PURPOSE : Update a row in table MAILING_INSTANCES
    //
    //  PARAMS :
    //      - INPUT :
    //          - Int64 MailingInstanceId
    //          - MAILING_INSTANCES_STATUS InstanceOldStatus
    //          - MAILING_INSTANCES_STATUS InstanceNewStatus
    //          - String Subject
    //          - String Body
    //          - SqlTransaction SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True: Update ok
    //      - False: Otherwise
    //
    internal static Boolean DB_UpdateInstance(Int64 MailingInstanceId,
                                              MAILING_INSTANCES_STATUS InstanceOldStatus,
                                              MAILING_INSTANCES_STATUS InstanceNewStatus,
                                              String Subject,
                                              String Body,
                                              SqlTransaction SqlTrx)
    {
      SqlCommand _sql_cmd;
      StringBuilder _sql_sb;
      String _update_message;

      try
      {
        _sql_cmd = new SqlCommand();

        _update_message = "";
        if (InstanceNewStatus == MAILING_INSTANCES_STATUS.READY)
        {
          _update_message = "      , MI_SUBJECT             = @pSubject " +
                            "      , MI_MESSAGE             = @pMessage ";
          _sql_cmd.Parameters.Add("@pSubject", SqlDbType.NVarChar).Value = Subject;
          _sql_cmd.Parameters.Add("@pMessage", SqlDbType.NVarChar).Value = Body;
        }

        _sql_sb = new StringBuilder();
        _sql_sb.AppendLine("UPDATE   MAILING_INSTANCES ");

        if (InstanceNewStatus == MAILING_INSTANCES_STATUS.ERROR_SENDING)
        {
            _sql_sb.AppendLine("   SET   MI_STATUS = CASE WHEN ABS(DATEDIFF(minute, MI_PROG_DATE, GETDATE())) > 30 THEN @pNewStatus ELSE @pPendingStatus END ");
        }
        else
        {
            _sql_sb.AppendLine("   SET   MI_STATUS              = @pNewStatus ");
        }

        _sql_sb.AppendLine(_update_message);
        _sql_sb.AppendLine(" WHERE   MI_MAILING_INSTANCE_ID = @pMailingInstanceId ");
        _sql_sb.AppendLine("   AND   MI_STATUS              = @pOldStatus ");

        _sql_cmd.CommandText = _sql_sb.ToString();
        _sql_cmd.Connection = SqlTrx.Connection;
        _sql_cmd.Transaction = SqlTrx;

        _sql_cmd.Parameters.Add("@pMailingInstanceId", SqlDbType.BigInt).Value = MailingInstanceId;
        _sql_cmd.Parameters.Add("@pOldStatus", SqlDbType.Int).Value = InstanceOldStatus;
        _sql_cmd.Parameters.Add("@pNewStatus", SqlDbType.Int).Value = InstanceNewStatus;

        if (InstanceNewStatus == MAILING_INSTANCES_STATUS.ERROR_SENDING)
        {
            _sql_cmd.Parameters.Add("@pPendingStatus", SqlDbType.Int).Value = MAILING_INSTANCES_STATUS.PENDING;
        }

        return (_sql_cmd.ExecuteNonQuery() == 1);
      }
      catch (SqlException _sql_ex)
      {
        if (_sql_ex.Number == -2) // command timeout
        {
          // Do nothing
        }
        else
        {
          Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // DB_UpdateInstance

    //------------------------------------------------------------------------------
    // PURPOSE : Return AddressList properly formatted: list separated by ','.
    //
    //  PARAMS :
    //      - INPUT :
    //          - String AddressList
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String
    //
    private static String FormatAddressList(String AddressList)
    {
      String[] _emails;
      String[] _sep = new String[2];
      String _tmp_address;

      _emails = AddressList.Split();
      _tmp_address = String.Join(",", _emails);
      _sep[0] = ";";
      _sep[1] = ",";
      _emails = _tmp_address.Split(_sep, StringSplitOptions.RemoveEmptyEntries);
      _tmp_address = String.Join(",", _emails);

      return _tmp_address;
    } // FormatAddressList

    //------------------------------------------------------------------------------
    // PURPOSE : Read a Mailing Instance
    //
    //  PARAMS :
    //      - INPUT :
    //          - Int64 MailingInstanceId
    //
    //      - OUTPUT :
    //          - String Name
    //          - DateTime DateSched
    //          - MAILING_PROGRAMMING_TYPE ProgType
    //          - String Subject
    //
    // RETURNS :
    //      - True: Read ok
    //      - False: Otherwise
    //
    internal static Boolean ReadInstance(Int64 MailingInstanceId,
                                         out String Name,
                                         out DateTime DateSched,
                                         out MAILING_PROGRAMMING_TYPE ProgType,
                                         out String Subject)
    {
      SqlConnection _sql_conn;
      SqlCommand _sql_cmd;
      StringBuilder _sql_sb;
      SqlDataReader _sql_reader;

      Name = "";
      DateSched = DateTime.MinValue;
      ProgType = MAILING_PROGRAMMING_TYPE.CASH_INFORMATION;
      Subject = "";

      _sql_conn = null;
      _sql_reader = null;

      try
      {
        _sql_conn = WASDB.Connection();

        _sql_sb = new StringBuilder();
        _sql_sb.AppendLine("SELECT   MI_NAME ");
        _sql_sb.AppendLine("       , MI_PROG_DATE ");
        _sql_sb.AppendLine("       , MI_TYPE ");
        _sql_sb.AppendLine("       , MI_SUBJECT ");
        _sql_sb.AppendLine("  FROM   MAILING_INSTANCES ");
        _sql_sb.AppendLine(" WHERE   MI_MAILING_INSTANCE_ID = @pMailingInstanceId ");

        _sql_cmd = new SqlCommand(_sql_sb.ToString(), _sql_conn);
        _sql_cmd.Parameters.Add("@pMailingInstanceId", SqlDbType.BigInt).Value = MailingInstanceId;

        _sql_reader = _sql_cmd.ExecuteReader();
        if (_sql_reader.Read())
        {
          Name = _sql_reader.GetString(0);
          DateSched = _sql_reader.GetDateTime(1);
          ProgType = (MAILING_PROGRAMMING_TYPE)_sql_reader.GetInt32(2);
          Subject = _sql_reader.GetString(3);
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
      finally
      {
        if (_sql_reader != null)
        {
          _sql_reader.Close();
          _sql_reader.Dispose();
          _sql_reader = null;
        }

        if (_sql_conn != null)
        {
          try { _sql_conn.Close(); }
          catch { }
          _sql_conn = null;
        }
      }
    } // ReadInstance

    private static String GetAttachmentFilename(String FileName, DateTime DateFrom, DateTime DateTo)
    {
      Int32 _site_id;
      String _site_name;      
      String _excel_filename;
      String _date_filename_part;

      Int32.TryParse(GeneralParam.Value("Site", "Identifier"), out _site_id);
      _site_name = GeneralParam.Value("Site", "Name");

      _date_filename_part = DateFrom.ToString("yyyyMMdd");

      if (DateFrom != DateTo)
      {
        _date_filename_part += "-" + DateTo.ToString("yyyyMMdd");
      }

      // Not assign Excel extension now. Will assign in class ExcelConversion.
      _excel_filename = Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar + 
                        FileName + "-" + _site_id.ToString("000") + "-" +
                        _date_filename_part + "-" + _site_name;

      return _excel_filename;
    } // GetAttachmentFilename

    #endregion // Private Functions

  } // WCP_Mailing

  
  class MailingInstance
  {
    Int64    m_mailing_instance_id;
    String   m_name;
    DateTime m_scheduled;
    MAILING_PROGRAMMING_TYPE m_type;
    String   m_subject;
    String   m_body;
    DateTime m_date_from;
    DateTime m_date_to;
    String m_site_id;
    String m_site_name;

    //------------------------------------------------------------------------------
    // PURPOSE : Create a new mailing instance, based on the mailing type.
    //
    //  PARAMS :
    //      - INPUT :
    //          - SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - MailingInstance or null if error.
    //
    public static MailingInstance Create(Int64 MailingInstanceId)
    {
      String _name;
      DateTime _sched;
      MAILING_PROGRAMMING_TYPE _type;
      String _subject;

      if (!Mailing.ReadInstance(MailingInstanceId, out _name, out _sched, out _type, out _subject))
      {
        return null;
      }
      
      switch (_type)
      {
        case MAILING_PROGRAMMING_TYPE.CASH_INFORMATION:
          return new CashInformation(MailingInstanceId, _name, _sched, _type, _subject);

        case MAILING_PROGRAMMING_TYPE.WSI_STATISTICS:
          return new WsiStatistics(MailingInstanceId, _name, _sched, _type, _subject);

        case MAILING_PROGRAMMING_TYPE.CONNECTED_TERMINALS:
          return new ConnectedTerminals(MailingInstanceId, _name, _sched, _type, _subject);

        case MAILING_PROGRAMMING_TYPE.ACCOUNT_MOVEMENTS:
          return new AccountMovements(MailingInstanceId, _name, _sched, _type, _subject);

        default:
          return null;
      }
    } // ProgReport

    //------------------------------------------------------------------------------
    // PURPOSE : Process a Mailing report (Virtual Method)
    //
    //  PARAMS :
    //      - INPUT :
    //          - SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True: Processed ok. False: Otherwise.
    //
    public virtual bool Execute(SqlTransaction SqlTrx)
    {
      return false;
    } // Execute

    //------------------------------------------------------------------------------
    // PURPOSE : Process a generic report
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True: Processed ok (status == READY)
    //      - False: Otherwise
    //
    public bool Process()
    {
      MAILING_INSTANCES_STATUS _old_status;
      MAILING_INSTANCES_STATUS _new_status;
      SqlConnection _sql_conn;
      SqlTransaction _sql_trx;
      String _type_name;

      _sql_conn = null;
      _sql_trx = null;
      
      try 
      {
        _sql_conn = WASDB.Connection();
        _sql_trx = _sql_conn.BeginTransaction();
        
        _old_status = MAILING_INSTANCES_STATUS.PENDING;
        _new_status = MAILING_INSTANCES_STATUS.READY;

        try
        {
          // RCI 27-JUL-2011: Obtain the subject from the programming if it is not defined at this point.
          //                  Necessary in case of a retry for this mailing instance.
          if (String.IsNullOrEmpty(m_subject))
          {
            m_subject = GetMailingProgrammingSubject(m_mailing_instance_id, _sql_trx);
          }

          if (!Mailing.DB_UpdateInstance(m_mailing_instance_id, _old_status, _new_status, m_subject, "", _sql_trx))
          {
            return false;
          }
          _old_status = MAILING_INSTANCES_STATUS.READY;
          _new_status = MAILING_INSTANCES_STATUS.ERROR_PROCESSING;

          m_site_id = GeneralParam.Value("Site", "Identifier");
          m_site_name = GeneralParam.Value("Site", "Name");

          // Execute
          if (Execute(_sql_trx))
          {
            _new_status = MAILING_INSTANCES_STATUS.READY;

            switch (m_type)
            {
              case MAILING_PROGRAMMING_TYPE.CASH_INFORMATION:
                _type_name = Resource.String("STR_MAILING_TYPE_CASH_INFORMATION");
                break;
              case MAILING_PROGRAMMING_TYPE.WSI_STATISTICS:
                _type_name = Resource.String("STR_MAILING_TYPE_WSI_STATISTICS");
                break;
              case MAILING_PROGRAMMING_TYPE.CONNECTED_TERMINALS:
                _type_name = Resource.String("STR_MAILING_TYPE_CONNECTED_TERMINALS");
                break;
              case MAILING_PROGRAMMING_TYPE.ACCOUNT_MOVEMENTS:
                _type_name = Resource.String("STR_MAILING_TYPE_ACCOUNT_MOVEMENTS");
                break;
              default:
                _type_name = "*** UNKNOWN TYPE ***";
                break;
            }

            m_subject = m_subject.Replace("@Name", m_name);
            m_subject = m_subject.Replace("@Type", _type_name);
            m_subject = m_subject.Replace("@SiteId", m_site_id);
            m_subject = m_subject.Replace("@SiteName", m_site_name);
            m_subject = m_subject.Replace("@FromDate", m_date_from.ToShortDateString());
            m_subject = m_subject.Replace("@ToDate", m_date_to.ToShortDateString());
            m_subject = m_subject.Replace("@FromTime", m_date_from.ToShortTimeString());
            m_subject = m_subject.Replace("@ToTime", m_date_to.ToShortTimeString());
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }

        // Update Status
        if (Mailing.DB_UpdateInstance(m_mailing_instance_id, _old_status, _new_status,
                                      m_subject, m_body, _sql_trx))
        {
          _sql_trx.Commit();
        }
      
        return (_new_status == MAILING_INSTANCES_STATUS.READY);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
      finally
      {
        if (_sql_trx != null)
        {
          if (_sql_trx.Connection != null)
          {
            try { _sql_trx.Rollback(); }
            catch { }
          }
          _sql_trx.Dispose();
          _sql_trx = null;
        }

        // Close connection
        if (_sql_conn != null)
        {
          try { _sql_conn.Close(); }
          catch { }
          _sql_conn = null;
        }
      }
    } // Process

    //------------------------------------------------------------------------------
    // PURPOSE : Get the base subject defined in the programming for this mailing instance.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Int64 MailingInstanceId
    //          - SqlTransaction SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String
    private String GetMailingProgrammingSubject(Int64 MailingInstanceId, SqlTransaction SqlTrx)
    {
      String _sql_str;
      Object _obj;
      String _subject;

      _subject = "";

      try
      {
        _sql_str = "SELECT   MP_SUBJECT " +
                   "  FROM   MAILING_INSTANCES " +
                   "       , MAILING_PROGRAMMING " +
                   " WHERE   MI_PROG_ID             = MP_PROG_ID " +
                   "   AND   MI_MAILING_INSTANCE_ID = @pMailingInstanceId ";

        using (SqlCommand _sql_cmd = new SqlCommand(_sql_str, SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pMailingInstanceId", SqlDbType.BigInt).Value = MailingInstanceId;

          _obj = _sql_cmd.ExecuteScalar();
          if (_obj != null && _obj != DBNull.Value)
          {
            _subject = (String)_obj;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _subject;
    } // GetMailingProgrammingSubject

    private class CashInformation : MailingInstance
    {
      # region Constructors

      //------------------------------------------------------------------------------
      // PURPOSE : Constructor for a Cash Information Mailing instance
      //
      //  PARAMS :
      //      - INPUT :
      //          - Int64 MailingInstanceId
      //          - String Name
      //          - DateTime DateSched
      //          - MAILING_PROGRAMMING_TYPE Type
      //          - String Subject
      //
      //      - OUTPUT :
      //
      // RETURNS :
      //
      internal CashInformation(Int64 MailingInstanceId,
                               String Name,
                               DateTime DateSched,
                               MAILING_PROGRAMMING_TYPE Type,
                               String Subject)
      {
        m_mailing_instance_id = MailingInstanceId;
        m_name = Name;
        m_scheduled = DateSched;
        m_type = Type;
        m_subject = Subject;
        m_body = "";
        m_date_from = DateTime.MinValue;
        m_date_to = DateTime.MaxValue;
        m_site_id = "";
        m_site_name = "";
      } // CashInformation

      #endregion // Constructors

      //------------------------------------------------------------------------------
      // PURPOSE : Process a Cash Information Mailing report
      //
      //  PARAMS :
      //      - INPUT :
      //          - SqlTrx
      //
      //      - OUTPUT :
      //
      // RETURNS :
      //      - True: Processed ok. False: Otherwise.
      //
      public override bool Execute(SqlTransaction SqlTrx)
      {
        TYPE_MAILING_REPORT _report;
        TYPE_MAILING_PROVIDER_STATS[] _provider_stats;
        TYPE_MAILING_PROVIDER_STATS _provider;
        Cashier.TYPE_CASHIER_SESSION_STATS _cashier_data;
        DataTable _dt_providers_num_terms;
        DataTable _dt_providers_sales;
        DataTable _dt_providers_netwin;
        DataTable _dt_site_jackpot;
        Int32 _terminals_connected;
        StatsMailingReport _stats_report;

        _report.site_name = m_site_id + " - " + m_site_name;

        _report.date_from = Misc.Opening(m_scheduled);
        // Eliminate the Minutes part of the date_from.
        _report.date_from = _report.date_from.AddMinutes(-_report.date_from.Minute);
        _report.date_to = new DateTime(m_scheduled.Year, m_scheduled.Month, m_scheduled.Day, m_scheduled.Hour, 0, 0);

        // To == From? From = From - 1 day
        if (_report.date_to == _report.date_from)
        {
          _report.date_from = _report.date_from.AddDays(-1);
        }

        m_date_from = _report.date_from;
        m_date_to = _report.date_to;

        _cashier_data = new Cashier.TYPE_CASHIER_SESSION_STATS();
        Cashier.ReadCashierSessionData(SqlTrx, m_date_from, m_date_to, ref _cashier_data);

        _report.cashier_inputs = _cashier_data.total_cash_in;
        _report.cashier_outputs = _cashier_data.total_cash_out;
        _report.cashier_prize_gross = _cashier_data.prize_gross;
        _report.cashier_prize_taxes = _cashier_data.prize_taxes;
        _report.cashier_result = _cashier_data.result_cashier;

        // Site Jackpot
        _report.site_jackpot = 0;
        _dt_site_jackpot = SqlStatistics.GetNetWinByProvider(m_date_from, m_date_to, true, SqlTrx);
        if (_dt_site_jackpot != null && _dt_site_jackpot.Rows.Count > 0)
        {
          _report.site_jackpot = Math.Abs((Decimal)_dt_site_jackpot.Rows[0]["CashIn"] - (Decimal)_dt_site_jackpot.Rows[0]["CashOut"]);
        }

        // Occupation
        _report.occupation = new TYPE_OCCUPATION_SESSION_STATS();
        Occupation.ReadOccupationSessionData(SqlTrx, m_date_from, m_date_to, ref _report.occupation);

        // DT - Terminals connected - Num. of terminals for each provider
        _dt_providers_num_terms = SqlStatistics.GetNumTerminalsConnectedByProvider(m_date_from, m_date_to, false, SqlTrx);
        if (_dt_providers_num_terms == null)
        {
          Log.Error(" Mailing.CashInformation: Can't obtain NumTerminalsConnectedByProvider.");

          return false;
        }

        // DT - Sales by provider - PlayedAmount and WonAmount for each provider
        _dt_providers_sales = SqlStatistics.GetSalesByProvider(m_date_from, m_date_to, SqlTrx);
        if (_dt_providers_sales == null)
        {
          Log.Error(" Mailing.CashInformation: Can't obtain SalesByProvider.");

          return false;
        }

        // DT - NetWin by provider - CashIn and CashOut of each provider
        _dt_providers_netwin = SqlStatistics.GetNetWinByProvider(m_date_from, m_date_to, false, SqlTrx);
        if (_dt_providers_netwin == null)
        {
          Log.Error(" Mailing.CashInformation: Can't obtain NetWinByProvider.");

          return false;
        }

        _dt_providers_num_terms.Merge(_dt_providers_sales);
        _dt_providers_num_terms.Merge(_dt_providers_netwin);

        _provider_stats = new TYPE_MAILING_PROVIDER_STATS[0];

        foreach (DataRow _dr_provider in _dt_providers_num_terms.Rows)
        {
          _terminals_connected = _dr_provider["NumTerminals"] == DBNull.Value? 0:(Int32)_dr_provider["NumTerminals"];

          // Add the provider that It have terminals connected
          if (_terminals_connected>0)
          {
            _provider = new TYPE_MAILING_PROVIDER_STATS();
            _provider.name = (String)_dr_provider["Provider"];
            _provider.played = _dr_provider["PAmount"] == DBNull.Value? 0.0m:(Decimal)_dr_provider["PAmount"];
            _provider.won = _dr_provider["WAmount"] == DBNull.Value? 0.0m:(Decimal)_dr_provider["WAmount"];
            _provider.netwin = 0;
            if (_dr_provider["CashIn"] != DBNull.Value && _dr_provider["CashOut"] != DBNull.Value)
            {
              _provider.netwin = (Decimal)_dr_provider["CashIn"] - (Decimal)_dr_provider["CashOut"];
            }
            _provider.num_terminals = _terminals_connected;

            Array.Resize(ref _provider_stats,_provider_stats.Length+1);
            _provider_stats.SetValue(_provider,_provider_stats.Length-1);
          }

        }

        _report.provider_stats = _provider_stats;

        _stats_report = new StatsMailingReport(_report);
        
        m_body = _stats_report.GetHTML();
        
        return true;
      } // Execute

    } // CashInformation

    private class WsiStatistics : MailingInstance
    {
      # region Constructors

      //------------------------------------------------------------------------------
      // PURPOSE : Constructor for a WSI Statistics Mailing instance
      //
      //  PARAMS :
      //      - INPUT :
      //          - Int64 MailingInstanceId
      //          - String Name
      //          - DateTime DateSched
      //          - MAILING_PROGRAMMING_TYPE Type
      //          - String Subject
      //
      //      - OUTPUT :
      //
      // RETURNS :
      //
      internal WsiStatistics(Int64 MailingInstanceId,
                             String Name,
                             DateTime DateSched,
                             MAILING_PROGRAMMING_TYPE Type,
                             String Subject)
      {
        m_mailing_instance_id = MailingInstanceId;
        m_name = Name;
        m_scheduled = DateSched;
        m_type = Type;
        m_subject = Subject;
        m_body = "";
        m_date_from = DateTime.MinValue;
        m_date_to = DateTime.MaxValue;
        m_site_id = "";
        m_site_name = "";
      } // WsiStatistics

      #endregion // Constructors

      //------------------------------------------------------------------------------
      // PURPOSE : Process a WSI Statistics Mailing report
      //
      //  PARAMS :
      //      - INPUT :
      //          - SqlTrx
      //
      //      - OUTPUT :
      //
      // RETURNS :
      //      - True: Processed ok. False: Otherwise.
      //
      public override bool Execute(SqlTransaction SqlTrx)
      {
        DataSet _ds_games;
        DataTable _dt_games;
        DataTable _dt_connected;
        DateTime _date_opening;

        _date_opening = Misc.Opening(m_scheduled);
        // Eliminate the Minutes part.
        _date_opening = _date_opening.AddMinutes(-_date_opening.Minute);

        m_date_from = _date_opening.AddDays(-Mailing.WSI_STATISTICS_DAYS_FROM);
        m_date_to = _date_opening;

        _dt_games = SqlStatistics.GetGamesInfoSales(m_date_from, m_date_to, SqlTrx);
        if (_dt_games == null)
        {
          Log.Error(" Mailing.WsiStatistics: Can't obtain GamesInfoSales.");

          return false;
        }

        _dt_connected = SqlStatistics.GetNumWinTerminalsConnectedByDay(m_date_from, m_date_to, SqlTrx);
        if (_dt_connected == null)
        {
          Log.Error(" Mailing.WsiStatistics: Can't obtain NumWinTerminalsConnectedByDay.");

          return false;
        }

        _ds_games = new DataSet();
        _ds_games.Tables.Add(_dt_games);
        _ds_games.Tables.Add(_dt_connected);

        m_body = Misc.DataSetToXml(_ds_games, XmlWriteMode.WriteSchema);

        return true;
      } // Execute

    } // WsiStatistics

    private class ConnectedTerminals : MailingInstance
    {
      # region Constructors

      //------------------------------------------------------------------------------
      // PURPOSE : Constructor for a Connected Terminals Mailing instance
      //
      //  PARAMS :
      //      - INPUT :
      //          - Int64 MailingInstanceId
      //          - String Name
      //          - DateTime DateSched
      //          - MAILING_PROGRAMMING_TYPE Type
      //          - String Subject
      //
      //      - OUTPUT :
      //
      // RETURNS :
      //
      internal ConnectedTerminals(Int64 MailingInstanceId,
                                  String Name,
                                  DateTime DateSched,
                                  MAILING_PROGRAMMING_TYPE Type,
                                  String Subject)
      {
        m_mailing_instance_id = MailingInstanceId;
        m_name = Name;
        m_scheduled = DateSched;
        m_type = Type;
        m_subject = Subject;
        m_body = "";
        m_date_from = DateTime.MinValue;
        m_date_to = DateTime.MaxValue;
        m_site_id = "";
        m_site_name = "";
      } // ConnectedTerminals

      #endregion // Constructors

      //------------------------------------------------------------------------------
      // PURPOSE : Process a Connected Terminals Mailing report
      //
      //  PARAMS :
      //      - INPUT :
      //          - SqlTrx
      //
      //      - OUTPUT :
      //
      // RETURNS :
      //      - True: Processed ok. False: Otherwise.
      //
      public override bool Execute(SqlTransaction SqlTrx)
      {
        DataSet _ds_terminals_connected;
        DataTable _dt_current_month;
        DataTable _dt_previous_month;
        DateTime _date_opening;
        DateTime _from_date;
        DateTime _to_date;

        _date_opening = Misc.Opening(m_scheduled);
        // Eliminate the Minutes part.
        _date_opening = _date_opening.AddMinutes(-_date_opening.Minute);

        _ds_terminals_connected = new DataSet();

        // Previous month (Always calculated)
        _from_date = _date_opening.AddDays(-_date_opening.Day + 1).AddMonths(-1);
        _to_date = _from_date.AddMonths(1);

        _dt_previous_month = SqlStatistics.GetNumTerminalsConnectedByProviderAndDay(_from_date, _to_date, SqlTrx);
        if (_dt_previous_month == null)
        {
          Log.Error(" Mailing.ConnectedTerminals: Can't obtain PREVIOUS NumTerminalsConnectedByProviderAndDay.");

          return false;
        }

        _dt_previous_month.TableName = "CONNECTED_TERMINALS_PREVIOUS_MONTH";

        _ds_terminals_connected.Tables.Add(_dt_previous_month);

        // Current month (only if we are NOT in the first day of the month)
        if (_date_opening.Day > 1)
        {
          m_date_from = _date_opening.AddDays(-_date_opening.Day + 1);
          m_date_to = _date_opening;
          _dt_current_month = SqlStatistics.GetNumTerminalsConnectedByProviderAndDay(m_date_from, m_date_to, SqlTrx);
          if (_dt_current_month == null)
          {
            Log.Error(" Mailing.ConnectedTerminals: Can't obtain CURRENT NumTerminalsConnectedByProviderAndDay.");

            return false;
          }

          _dt_current_month.TableName = "CONNECTED_TERMINALS_CURRENT_MONTH";

          _ds_terminals_connected.Tables.Add(_dt_current_month);
        }
        else
        {
          m_date_from = _from_date;
          m_date_to = _to_date;
        }

        m_body = Misc.DataSetToXml(_ds_terminals_connected, XmlWriteMode.WriteSchema);

        return true;
      } // Execute

    } // ConnectedTerminals

    private class AccountMovements : MailingInstance
    {
      # region Constructors

      //------------------------------------------------------------------------------
      // PURPOSE : Constructor for a Account Movements Mailing instance
      //
      //  PARAMS :
      //      - INPUT :
      //          - Int64 MailingInstanceId
      //          - String Name
      //          - DateTime DateSched
      //          - MAILING_PROGRAMMING_TYPE Type
      //          - String Subject
      //
      //      - OUTPUT :
      //
      // RETURNS :
      //
      internal AccountMovements(Int64 MailingInstanceId,
                                  String Name,
                                  DateTime DateSched,
                                  MAILING_PROGRAMMING_TYPE Type,
                                  String Subject)
      {
        m_mailing_instance_id = MailingInstanceId;
        m_name = Name;
        m_scheduled = DateSched;
        m_type = Type;
        m_subject = Subject;
        m_body = "";
        m_date_from = DateTime.MinValue;
        m_date_to = DateTime.MaxValue;
        m_site_id = "";
        m_site_name = "";
      } // AccountMovements

      #endregion // Constructors

      //------------------------------------------------------------------------------
      // PURPOSE : Process a Account Movements Mailing report
      //
      //  PARAMS :
      //      - INPUT :
      //          - SqlTrx
      //
      //      - OUTPUT :
      //
      // RETURNS :
      //      - True: Processed ok. False: Otherwise.
      //
      public override bool Execute(SqlTransaction SqlTrx)
      {
        DataSet _ds_account_movements;        

        _ds_account_movements = XMLReport.CajaUnicaOneDayReport(m_scheduled, out m_date_from, out m_date_to);

        m_body = Misc.DataSetToXml(_ds_account_movements, XmlWriteMode.IgnoreSchema);

        return true;
      } // Execute

    } // AccountMovements

  } // MailingInstance

} // WSI.Common
