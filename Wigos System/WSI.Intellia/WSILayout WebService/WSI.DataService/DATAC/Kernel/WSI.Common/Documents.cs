//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: DocumentList.cs
// 
//   DESCRIPTION: Manage a Document List
// 
//        AUTHOR: Andreu Julia 
// 
// CREATION DATE: 14-FEB-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 14-FEB-2012 AJQ    First release.
// 02-MAY-2012 MPO    Added feature --> Delete/Save a doc_type
// 30-AUG-2012 ANG    Fixed bug, WKTResources.Save() doesn't update file extension
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Security.Cryptography;
using System.IO.Compression;
using System.Data;
using System.Net.Mime;
using System.Runtime.Serialization.Formatters.Binary;
using System.Data.SqlClient;
using System.Drawing;

namespace WSI.Common
{


  //------------------------------------------------------------------------------
  // PURPOSE : Interface to manage documents
  //
  //
  public interface IDocument
  {
    String Name { get;}
    Byte[] Content { get;}
  }

  //------------------------------------------------------------------------------
  // PURPOSE : ReadOnly document
  //
  //
  public class ReadOnlyDocument : IDocument
  {
    private String m_name;
    private Byte[] m_content;

    public ReadOnlyDocument(String Name, Byte[] Content)
    {
      m_name = Name;
      m_content = Content;
    }

    public ReadOnlyDocument(String Name, MemoryStream MemStream)
    {
      m_name = Name;
      MemStream.Seek(0, SeekOrigin.End);
      m_content = new Byte[MemStream.Position];
      Array.Copy(MemStream.GetBuffer(), m_content, m_content.Length);
    }

    public string Name
    {
      get
      {
        return m_name;
      }
    }

    public byte[] Content
    {
      get
      {
        if (m_content == null)
        {
          return new Byte[0];
        }
        return m_content;
      }
    }

  }

  public class DocumentList : List<IDocument>
  {
    private enum CompressionMethod
    {
      None = 0,
      Deflate = 1,
    }

    private static Byte[] Compress(Byte[] ExpandedBuffer)
    {
      Byte[] _out;

      _out = null;

      using (MemoryStream _ms = new MemoryStream())
      {
        using (DeflateStream _dfs = new DeflateStream(_ms, CompressionMode.Compress, true))
        {
          _dfs.Write(ExpandedBuffer, 0, ExpandedBuffer.Length);
          _dfs.Close();
        }
        _out = new Byte[_ms.Position];
        Array.Copy(_ms.GetBuffer(), _out, _out.Length);
      }

      return _out;
    }

    private static Byte[] Expand(Byte[] CompressedBuffer)
    {
      Byte[] _tmp;
      Byte[] _out;
      int _read;

      _tmp = new Byte[64 * 1024];
      _out = null;

      using (MemoryStream _mso = new MemoryStream())
      {
        using (MemoryStream _msi = new MemoryStream(CompressedBuffer, 0, CompressedBuffer.Length))
        {
          using (DeflateStream _dfs = new DeflateStream(_msi, CompressionMode.Decompress))
          {
            do
            {
              _read = _dfs.Read(_tmp, 0, _tmp.Length);
              _mso.Write(_tmp, 0, _read);

            } while (_read == _tmp.Length);

            _dfs.Close();
          }
        }

        _out = new Byte[_mso.Position];
        Array.Copy(_mso.GetBuffer(), _out, _out.Length);
      }

      return _out;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Creates a document list from a binary array
    //
    //  PARAMS:
    //      - INPUT:
    //          - BinaryDocumentList
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      DocumentList
    public static DocumentList CreateFromBinary(Byte[] BinaryDocumentList)
    {
      BinaryFormatter _bin_formatter;
      DataSet _dataset;
      DataTable _table;
      MemoryStream _mem_stream;
      DocumentList _doc_list;

      _doc_list = new DocumentList();

      _mem_stream = new MemoryStream(BinaryDocumentList);
      _bin_formatter = new BinaryFormatter();
      _dataset = (DataSet)_bin_formatter.Deserialize(_mem_stream);

      _table = _dataset.Tables["PackedFile"];

      foreach (DataRow _file in _table.Rows)
      {
        CompressionMethod _method;
        String _name;
        Byte[] _content;

        _name = (String)_file["Name"];
        _content = (Byte[])_file["Content"];
        _method = (CompressionMethod)_file["CompressionMethod"];
        if (_method == CompressionMethod.Deflate)
        {
          _content = Expand(_content);
        }

        _doc_list.Add(new ReadOnlyDocument(_name, _content));

      }

      return _doc_list;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Convert a document list to binary
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - Byte []
    public Byte[] ConvertToBinary()
    {
      DataTable _table;
      DataSet _dataset;
      DataColumn _column;
      CompressionMethod _method;

      _method = CompressionMethod.None;

      _dataset = new DataSet("PackedFileList");
      _table = _dataset.Tables.Add("PackedFile");
      _column = _table.Columns.Add("Name", Type.GetType("System.String"));
      _column = _table.Columns.Add("Size", Type.GetType("System.Int64"));
      _column = _table.Columns.Add("CompressionMethod", _method.GetType());
      _column = _table.Columns.Add("CompressedSize", Type.GetType("System.Int64"));
      _column = _table.Columns.Add("Content", Type.GetType("System.Byte[]"));
      _column = _table.Columns.Add("SHA1", Type.GetType("System.Byte[]"));


      //------------------------------
      Byte[] _content;
      Byte[] _zip;
      Byte[] _hash;
      DataRow _row;
      SHA1 _csp;

      foreach (IDocument _doc in this)
      {
        _method = CompressionMethod.Deflate;
        _content = _doc.Content;
        _zip = Compress(_content);
        if (_zip.Length >= _content.Length)
        {
          _method = CompressionMethod.None;
          _zip = _content;
        }

        // Hash
        _csp = SHA1.Create();
        _hash = _csp.ComputeHash(_content);

        _row = _table.NewRow();
        _row["Name"] = _doc.Name;
        _row["Size"] = _content.LongLength;
        _row["CompressionMethod"] = _method;
        _row["CompressedSize"] = _zip.LongLength;
        _row["Content"] = _zip;
        _row["SHA1"] = _hash;

        _table.Rows.Add(_row);
      }

      MemoryStream _mem_stream;
      BinaryFormatter _bin_formatter;
      SerializationFormat _serial_format;

      _mem_stream = new MemoryStream();

      // Serialize 
      _bin_formatter = new BinaryFormatter();
      _serial_format = _dataset.RemotingFormat;
      _dataset.RemotingFormat = SerializationFormat.Binary;
      _bin_formatter.Serialize(_mem_stream, _dataset);

      Byte[] _out;

      _out = new Byte[_mem_stream.Position];

      Array.Copy(_mem_stream.GetBuffer(), _out, _out.Length);

      return _out;
    }

    public Boolean Save(String Path)
    {
      try
      {
        foreach (IDocument _doc in this)
        {
          String _path;

          _path = System.IO.Path.Combine(Path, _doc.Name);

          try
          {
            using (FileStream _fstream = new FileStream(_path, FileMode.Create, FileAccess.Write))
            {
              _fstream.Write(_doc.Content, 0, _doc.Content.Length);
              _fstream.Close();
            }
          }
          catch (Exception _ex)
          {
            Log.Exception(_ex);

            return false;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }


  }


  public static class TableDocuments
  {

    public static Boolean Save(ref Int64 DocumentId, DOCUMENT_TYPE DocumentType, DocumentList DocList, SqlTransaction Trx)
    {
      String _sql_str;
      SqlParameter _parameter;

      try
      {
        if (DocumentId == 0)
        {
          _sql_str = "";
          _sql_str += "INSERT INTO DOCUMENTS ( DOC_TYPE ) ";
          _sql_str += "               VALUES ( @pDocumentType   ) ";
          _sql_str += "                  SET   @DocumentId = SCOPE_IDENTITY() ";

          using (SqlCommand _cmd = new SqlCommand(_sql_str, Trx.Connection, Trx))
          {
            _cmd.Parameters.Add("@pDocumentType", SqlDbType.Int).Value = DocumentType;
            _parameter = _cmd.Parameters.Add("@DocumentId", SqlDbType.BigInt);
            _parameter.Direction = ParameterDirection.Output;
            if (_cmd.ExecuteNonQuery() != 1)
            {
              return false;
            }

            DocumentId = Convert.ToInt64(_parameter.Value);

            if (DocList == null)
            {
              return true;
            }

            if (DocList.Count == 0)
            {
              return true;
            }
          }
        }

        if (DocumentId == 0)
        {
          return false;
        }

        _sql_str = "";
        _sql_str += "UPDATE DOCUMENTS SET   DOC_TYPE        = @pDocumentType ";
        _sql_str += "                     , DOC_MODIFIED    = GETDATE() ";
        _sql_str += "                     , DOC_DATA        = @pData ";
        _sql_str += "               WHERE   DOC_DOCUMENT_ID = @pDocumentId ";

        using (SqlCommand _sql_cmd = new SqlCommand(_sql_str, Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pDocumentType", SqlDbType.Int).Value = DocumentType;
          _sql_cmd.Parameters.Add("@pData", SqlDbType.VarBinary).Value = DocList.ConvertToBinary();
          _sql_cmd.Parameters.Add("@pDocumentId", SqlDbType.BigInt).Value = DocumentId;

          if (_sql_cmd.ExecuteNonQuery() != 1)
          {
            return false;
          }

          return true;
        }
      }
      catch (SqlException _sql_ex)
      {
        Log.Exception(_sql_ex);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    public static Boolean Save(DOCUMENT_TYPE DocumentType, DocumentList DocList, SqlTransaction Trx)
    {
      String _sql_str;
      Object _obj;
      Int64 _document_id;

      _document_id = 0;

      try
      {

        _sql_str = "";
        _sql_str += "SELECT TOP 1   DOC_DOCUMENT_ID ";
        _sql_str += "        FROM   DOCUMENTS ";
        _sql_str += "       WHERE   DOC_TYPE = @pDocumentType ";
        _sql_str += "    ORDER BY   DOC_DOCUMENT_ID DESC ";
        using (SqlCommand _cmd = new SqlCommand(_sql_str, Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pDocumentType", SqlDbType.Int).Value = DocumentType;
          _obj = _cmd.ExecuteScalar();

          if (_obj == null)
          {
            //Only create a new ID
            if (!(Save(ref _document_id, DocumentType, null, Trx)))
            {
              return false;
            }
          }
          else
          {
            _document_id = Convert.ToInt64(_obj);
          }

          if (DocList == null)
          {
            return true;
          }

          if (DocList.Count == 0)
          {
            return true;
          }
        }

        return Save(ref _document_id, DocumentType, DocList, Trx);

      }
      catch (SqlException _sql_ex)
      {
        Log.Exception(_sql_ex);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    }

    public static Boolean Load(Int64 DocumentId, out DOCUMENT_TYPE DocumentType, out DocumentList DocList, SqlTransaction Trx)
    {
      String _sql_str;

      DocumentType = 0;
      DocList = new DocumentList();

      try
      {
        _sql_str = "";
        _sql_str += "SELECT   DOC_TYPE  ";
        _sql_str += "       , DOC_DATA  ";
        _sql_str += "  FROM   DOCUMENTS ";
        _sql_str += " WHERE   DOC_DOCUMENT_ID = @pDocumentId ";

        using (SqlCommand _cmd = new SqlCommand(_sql_str, Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pDocumentId", SqlDbType.BigInt).Value = DocumentId;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              DocumentType = (DOCUMENT_TYPE)_reader.GetInt32(0);
              DocList = DocumentList.CreateFromBinary((Byte[])_reader.GetSqlBinary(1));

              return true;
            }
          }
        }
      }
      catch (SqlException _sql_ex)
      {
        Log.Exception(_sql_ex);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    public static Boolean Load(DOCUMENT_TYPE DocumentType, out DocumentList DocList, SqlTransaction Trx)
    {
      String _sql_str;

      DocList = new DocumentList();

      try
      {
        _sql_str = "";
        _sql_str += "SELECT TOP 1   DOC_DATA  ";
        _sql_str += "        FROM   DOCUMENTS ";
        _sql_str += "       WHERE   DOC_TYPE = @pDocumentType ";
        _sql_str += "    ORDER BY   DOC_DOCUMENT_ID DESC ";

        using (SqlCommand _cmd = new SqlCommand(_sql_str, Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pDocumentType", SqlDbType.Int).Value = DocumentType;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              DocList = DocumentList.CreateFromBinary((Byte[])_reader.GetSqlBinary(0));

              return true;
            }
          }
        }
      }
      catch (SqlException _sql_ex)
      {
        Log.Exception(_sql_ex);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    public static Boolean Delete(Int64 DocumentId, SqlTransaction Trx)
    {
      String _sql_str;
      Int32 _rows_affected;

      try
      {
        _sql_str = "";
        _sql_str += "DELETE   FROM DOCUMENTS";
        _sql_str += " WHERE   DOC_DOCUMENT_ID = @pDocumentId";

        using (SqlCommand _cmd = new SqlCommand(_sql_str, Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pDocumentId", SqlDbType.BigInt).Value = DocumentId;

          _rows_affected = _cmd.ExecuteNonQuery();

          if (_rows_affected == 1)
          {
            return true;
          }
        }
      }
      catch (SqlException _sql_ex)
      {
        Log.Exception(_sql_ex);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

  }


  public static class WKTResources
  {
    public static Boolean Load(Int64 ResourceId,
                               Int32 Start,
                               Int32 Offset, 
                               out Byte[] ResourceBytes, 
                               SqlTransaction Trx)
    {
      String _sql_str;

      ResourceBytes = null;

      try
      {
        _sql_str = "";
        _sql_str += "SELECT   SUBSTRING(RES_DATA,@pStart,@pOffSet) ";
        _sql_str += "  FROM   WKT_RESOURCES ";
        _sql_str += " WHERE   RES_RESOURCE_ID = @pResourceId ";

        using (SqlCommand _cmd = new SqlCommand(_sql_str, Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pResourceId", SqlDbType.BigInt).Value = ResourceId;
          _cmd.Parameters.Add("@pOffSet", SqlDbType.Int).Value = Offset;
          _cmd.Parameters.Add("@pStart", SqlDbType.Int).Value = Start;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              ResourceBytes = (Byte[])_reader.GetSqlBinary(0);

              return true;
            }
          }
        }
      }
      catch (SqlException _sql_ex)
      {
        Log.Exception(_sql_ex);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }


    public static Boolean Load(Int64 ResourceId,
                               Int32 Offset,
                               out String Extension,
                               out Int32 Length,
                               out Byte[] Hash,
                               out Byte[] ResourceBytes,
                               SqlTransaction Trx)
    {

      String _sql_str;

      Hash = null;
      ResourceBytes = null;
      Extension = "";
      Length = 0;

      try
      {
        _sql_str = "";
        _sql_str += "SELECT   SUBSTRING(RES_DATA,1,@pOffSet) ";
        _sql_str += "       , RES_LENGTH";
        _sql_str += "       , RES_EXTENSION ";
        _sql_str += "       , RES_HASH ";
        _sql_str += "  FROM   WKT_RESOURCES ";
        _sql_str += " WHERE   RES_RESOURCE_ID = @pResourceId ";

        using (SqlCommand _cmd = new SqlCommand(_sql_str, Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pResourceId", SqlDbType.BigInt).Value = ResourceId;
          _cmd.Parameters.Add("@pOffSet", SqlDbType.Int).Value = Offset;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              ResourceBytes = (Byte[])_reader.GetSqlBinary(0);
              Length = (Int32)_reader.GetInt32(1);
              Extension = (String)_reader.GetString(2);
              Hash = (Byte[])_reader.GetSqlBinary(3);

              return true;
            }
          }
        }
      }
      catch (SqlException _sql_ex)
      {
        Log.Exception(_sql_ex);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    
    }

    public static Boolean Load(Int64 ResourceId, 
                               out String Extension,
                               out Int32 Length,
                               out Byte[] Hash,
                               out Byte[] ResourceBytes, 
                               SqlTransaction Trx)
    {
      String _sql_str;

      Hash = null;
      ResourceBytes = null;
      Extension = "";
      Length =0;

      try
      {
        _sql_str = "";
        _sql_str += "SELECT   RES_DATA ";
        _sql_str += "       , RES_LENGTH";
        _sql_str += "       , RES_EXTENSION ";
        _sql_str += "       , RES_HASH ";
        _sql_str += "  FROM   WKT_RESOURCES ";
        _sql_str += " WHERE   RES_RESOURCE_ID = @pResourceId ";

        using (SqlCommand _cmd = new SqlCommand(_sql_str, Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pResourceId", SqlDbType.BigInt).Value = ResourceId;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              ResourceBytes = (Byte[])_reader.GetSqlBinary(0);
              Length = (Int32)_reader.GetInt32(1);
              Extension = (String)_reader.GetString(2);
              Hash = (Byte[])_reader.GetSqlBinary(3);

              return true;
            }
          }
        }
      }
      catch (SqlException _sql_ex)
      {
        Log.Exception(_sql_ex);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    public static Boolean Load(Int64 ResourceId,
                               out Image ResourceImage,
                               SqlTransaction Trx)
    {
      String _extension;
      Int32 _length;
      Byte[] _hash;
      Byte[] _resource_bytes;
      MemoryStream _mm_image;

      ResourceImage = null;

      if (Load(ResourceId, out _extension, out _length, out _hash, out _resource_bytes, Trx))
      {
        _mm_image = new MemoryStream(_resource_bytes);
        ResourceImage = Image.FromStream(_mm_image);

        return true;
      }

      return false;
    }


    public static Boolean Save(ref Int64 ResourceId, 
                               String Extension,
                               Int32 Length,
                               Byte[] ResourceBytes, 
                               SqlTransaction Trx)
    {
      String _sql_str;
      SqlParameter _parameter;
      
      try
      {
        if (ResourceId == 0)
        {
          _sql_str = "";
          _sql_str += "INSERT INTO WKT_RESOURCES ( RES_EXTENSION, RES_LENGTH, RES_DATA, RES_HASH ) ";
          _sql_str += "                   VALUES ( @pExtension  , @pLength  , @pData  , @pHash   ) ";
          _sql_str += "                      SET   @ResourceId = SCOPE_IDENTITY() ";

          using (SqlCommand _cmd = new SqlCommand(_sql_str, Trx.Connection, Trx))
          {
            _cmd.Parameters.Add("@pExtension", SqlDbType.NVarChar).Value = Extension;
            _cmd.Parameters.Add("@pLength", SqlDbType.Int).Value = 0;
            _cmd.Parameters.Add("@pData", SqlDbType.Binary).Value = DBNull.Value;
            _cmd.Parameters.Add("@pHash", SqlDbType.Binary).Value = DBNull.Value;
            _parameter = _cmd.Parameters.Add("@ResourceId", SqlDbType.BigInt);
            _parameter.Direction = ParameterDirection.Output;
            if (_cmd.ExecuteNonQuery() != 1)
            {
              return false;
            }

            ResourceId = Convert.ToInt64(_parameter.Value);

            if (ResourceBytes == null)
            {
              return true;
            }

          }
        }

        if (ResourceId == 0)
        {
          return false;
        }

        _sql_str = "";
        _sql_str += "UPDATE WKT_RESOURCES SET   RES_EXTENSION        = @pExtension ";
        _sql_str += "                         , RES_LENGTH           = @pLength ";
        _sql_str += "                         , RES_DATA             = @pData ";
        _sql_str += "                         , RES_HASH             = @pHash ";
        _sql_str += "                   WHERE   RES_RESOURCE_ID      = @pResourceId ";

        using (SqlCommand _sql_cmd = new SqlCommand(_sql_str, Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pResourceId", SqlDbType.BigInt).Value = ResourceId;

          if (ResourceBytes == null)
          {
            _sql_cmd.Parameters.Add("@pData", SqlDbType.VarBinary).Value = DBNull.Value ;
          }
          else
          {
            _sql_cmd.Parameters.Add("@pData", SqlDbType.VarBinary).Value = ResourceBytes;
          }
          
          _sql_cmd.Parameters.Add("@pLength", SqlDbType.Int).Value = Length;
          
          if (ResourceBytes == null)
          {
            _sql_cmd.Parameters.Add("@pHash", SqlDbType.VarBinary).Value = DBNull.Value;
          }
          else
          {
            using (SHA1 _sha1 = SHA1.Create())
            {
              _sql_cmd.Parameters.Add("@pHash", SqlDbType.VarBinary).Value = _sha1.ComputeHash(ResourceBytes);
            }
          }
          if (ResourceBytes == null)
          {
            _sql_cmd.Parameters.Add("@pExtension", SqlDbType.NVarChar).Value = DBNull.Value;
          }
          else 
          {
            _sql_cmd.Parameters.Add("@pExtension", SqlDbType.NVarChar).Value = Extension;
          }

          if (_sql_cmd.ExecuteNonQuery() != 1)
          {
            return false;
          }

          return true;
        }
      }
      catch (SqlException _sql_ex)
      {
        Log.Exception(_sql_ex);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    public static Boolean Save(ref Int64 ResourceId, Image ResourceImage, SqlTransaction Trx)
    {
      MemoryStream _ms;
      String _extension;
      Byte[] _img_buf;
      Int32 _img_len;

      _extension = "";
      if (ResourceImage == null)
      {
        return Save(ref ResourceId, _extension, 0, null, Trx);
      }

      _ms = new MemoryStream();
      ResourceImage.Save(_ms, ResourceImage.RawFormat);

      // AJQ 04-SEP-2012, The lenght of the image could be smaller than the size of the buffer
      _img_len = (Int32)_ms.Position;
      _img_buf = _ms.GetBuffer();

      if (ResourceImage.RawFormat.Guid == System.Drawing.Imaging.ImageFormat.Bmp.Guid)
      {
        _extension = ".bmp";
      }
      else if (ResourceImage.RawFormat.Guid == System.Drawing.Imaging.ImageFormat.Gif.Guid)
      {
        _extension = ".gif";
      }
      else if (ResourceImage.RawFormat.Guid == System.Drawing.Imaging.ImageFormat.Jpeg.Guid)
      {
        _extension = ".jpg";
      }
      else if (ResourceImage.RawFormat.Guid == System.Drawing.Imaging.ImageFormat.Png.Guid)
      {
        _extension = ".png";
      }
      else if (ResourceImage.RawFormat.Guid == System.Drawing.Imaging.ImageFormat.Tiff.Guid)
      {
        _extension = ".tif";
      }
      else if (ResourceImage.RawFormat.Guid == System.Drawing.Imaging.ImageFormat.Wmf.Guid)
      {
        _extension = ".wmf";
      }
      else
      {
        // AJQ 04-SEP-2012, Default extension
        _extension = ".bmp";
      }

      return Save(ref ResourceId, _extension, _img_len, _img_buf, Trx);

    }

    public static Boolean Delete(Int64 ResourceId, SqlTransaction Trx)
    {
      String _sql_str;
      Int32 _rows_affected;

      try
      {
        _sql_str = "";
        _sql_str += "DELETE   FROM WKT_RESOURCES ";
        _sql_str += " WHERE   RES_RESOURCE_ID = @pResourceId";

        using (SqlCommand _cmd = new SqlCommand(_sql_str, Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pResourceId", SqlDbType.BigInt).Value = ResourceId;

          _rows_affected = _cmd.ExecuteNonQuery();

          if (_rows_affected == 1)
          {
            return true;
          }
        }
      }
      catch (SqlException _sql_ex)
      {
        Log.Exception(_sql_ex);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

  }

}

