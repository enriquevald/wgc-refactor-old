//-------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//-------------------------------------------------------------------
//
// MODULE NAME:   UserLogin.cs
// DESCRIPTION:   Common login (WigosGUI and Cashier), check login
//                status.
// AUTHOR:        Raul Ruiz Barea
// CREATION DATE: 13-AUG-201
//
// REVISION HISTORY:
//
// Date         Author Description
// -----------  ------ -----------------------------------------------
// 13-AUG-2012  RRB    Initial version
// -------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Threading;

namespace WSI.Common
{
  public class UserLogin
  {

    #region ENUMS

    public enum LoginStatus
    {
      ERROR = 0,
      OK = 1,
      USER_NOT_EXISTS = 2,
      WRONG_PASSWORD = 3,
      WRONG_PASSWORD_USER_HAS_BEEN_BLOCKED = 4,
      WRONG_PASSWORD_USER_IS_BLOCKED = 5,
      WRONG_PASSWORD_SUPER_USER = 6,
      USER_BLOCKED = 7,
      ILLEGAL_ACCESS = 8,
      SESION_OPENED = 9,
      NOT_PERMISSIONS = 10,
      OK_PASSWORD_EXPIRED = 11,
      OK_PASSWORD_CHANGE_REQ = 12
    } // LoginStatus

    public enum CommonForm
    {
      FORM_LOGIN_GUI = 10001,
      FORM_LOGIN_CASHIER = 0
    } // CommonForm
    
    #endregion

    #region Constants

    public const Int32 MAX_FAILURES = 3;
    public const Int32 MIN_PASSWORD_LENGTH = 1;
    public const Int32 MAX_PASSWORD_LENGTH = 15;
    public const Int32 MIN_USERNAME_LENGTH = 1;
    public const Int32 MAX_USERNAME_LENGTH = 10;

    #endregion

    public class UserInfo
    {
      public Int32 user_id;
      public Int32 block_reason;
      public String password;
      public DateTime not_valid_before;
      public DateTime not_valid_after;
      public DateTime last_changed;
      public DateTime password_exp;
      public DateTime current_date;
      public Int32 password_exp_days;
      public Int32 login_failures;
      public Int32 profile_id;
      public Boolean psw_chg_req;
    }; // UserInfo

    // PURPOSE: Check if the user has entered at least one character 
    // as UserName and Password
    //
    //  PARAMS:
    //     - INPUT:
    //           - UserName:
    //           - Password:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - Boolean: True->OK False->Length error.
    public static Boolean CheckLengthUserAndPwd(String UserName,
                                                 String Password)
    {
      return    UserName.Length < MIN_USERNAME_LENGTH 
             || UserName.Length > MAX_USERNAME_LENGTH 
             || Password.Length < MIN_PASSWORD_LENGTH
             || Password.Length > MAX_PASSWORD_LENGTH;
    } // CheckLengthUserAndPwd

    // PURPOSE: Get information of the user from db
    //
    //  PARAMS:
    //     - INPUT:
    //           - UserName:
    //           - UserInfo:
    //           - SqlTrx:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - Boolean: True->User found False->User not found.
    public static Boolean GetUserInfoInDb(String UserName,
                                            UserInfo UserInfo,
                                            SqlTransaction SqlTrx)
    {
      try
      {
        StringBuilder _sb;
        Byte[] _buffer;
        long _len;

        _sb = new StringBuilder();

        _sb.AppendLine(" SELECT   GU_USER_ID ");
        _sb.AppendLine("        , GU_BLOCK_REASON ");
        _sb.AppendLine("        , ISNULL(GU_LOGIN_FAILURES, 0) AS GU_LOGIN_FAILURES ");
        _sb.AppendLine("        , GU_PASSWORD ");
        _sb.AppendLine("        , GU_NOT_VALID_BEFORE ");
        _sb.AppendLine("        , GU_NOT_VALID_AFTER ");
        _sb.AppendLine("        , GU_LAST_CHANGED ");
        _sb.AppendLine("        , GU_PASSWORD_EXP ");
        _sb.AppendLine("        , GU_PROFILE_ID ");
        _sb.AppendLine("        , GU_PWD_CHG_REQ ");
        _sb.AppendLine("   FROM   GUI_USERS ");
        _sb.AppendLine("  WHERE   GU_USERNAME = @pUserName ");
        
        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pUserName", SqlDbType.NVarChar, 50).Value = UserName;
          using (SqlDataReader _sql_reader = _sql_cmd.ExecuteReader())
          {
            if (!_sql_reader.Read())
            {
              return false;
            }

            UserInfo.user_id = _sql_reader.GetInt32(0);
            UserInfo.block_reason = _sql_reader.GetInt32(1);
            UserInfo.login_failures = _sql_reader.GetInt32(2);

            _len = _sql_reader.GetBytes(3, 0, null, 0, 0);
            _buffer = new Byte[_len];
            _sql_reader.GetBytes(3, 0, _buffer, 0, (int)_len);
            UserInfo.password = Encoding.UTF8.GetString(_buffer).Replace("\0","");

            UserInfo.not_valid_before = _sql_reader.GetDateTime(4);
            UserInfo.not_valid_after = _sql_reader.IsDBNull(5) ? WASDB.Now.AddDays(1) : _sql_reader.GetDateTime(5);
            UserInfo.last_changed = _sql_reader.IsDBNull(6) ? WASDB.Now : _sql_reader.GetDateTime(6);

            if (_sql_reader.IsDBNull(7))
            {
              UserInfo.password_exp = WASDB.Now;
              UserInfo.password_exp_days = 0;
            }
            else
            {
              UserInfo.password_exp = _sql_reader.GetDateTime(7);
              UserInfo.password_exp_days = UserInfo.password_exp.Subtract(UserInfo.not_valid_before).Days;
            }

            UserInfo.profile_id = _sql_reader.GetInt32(8);
            UserInfo.current_date = WSI.Common.WASDB.Now;
            UserInfo.psw_chg_req = _sql_reader.GetBoolean(9);
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
      return true;
    } // GetUserInfoInDb

    // PURPOSE: Check if the password is correct
    //
    //  PARAMS:
    //     - INPUT:
    //           - AccountPwd:
    //           - Password:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - Boolean: True->Password OK False->Wrong pwd.
    public static Boolean CheckPassword(String Password,
                                          String AccountPwd)
    {
	    return ( AccountPwd.ToUpper() == Password.ToUpper() );
    } // CheckPassword

    // PURPOSE: Disable user and reset login failures
    //          when login_failures > MAX_FAILURES
    //
    //  PARAMS:
    //     - INPUT:
    //           - UserId:
    //           - SqlTrx:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - None
    public static void DisableUser(Int32 UserId,
                                     SqlTransaction SqlTrx)
    {
      try
      {
        StringBuilder _sb;

        _sb = new StringBuilder();

        _sb.AppendLine(" UPDATE   GUI_USERS ");
        _sb.AppendLine("    SET   GU_BLOCK_REASON   = GU_BLOCK_REASON | " + WSI.Common.GUI_USER_BLOCK_REASON.WRONG_PASSWORD );
        _sb.AppendLine("        , GU_LOGIN_FAILURES = 0 ");
        _sb.AppendLine("  WHERE   GU_USER_ID = @pUserId ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = UserId;

          _sql_cmd.ExecuteNonQuery();
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // DisableUser

    // PURPOSE: Reset login failures
    //
    //  PARAMS:
    //     - INPUT:
    //           - UserId:
    //           - SqlTrx:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - None
    public static void ResetLoginFailures(Int32 UserId,
                                            SqlTransaction SqlTrx)
    {
      try
      {
        StringBuilder _sb;

        _sb = new StringBuilder();

        _sb.AppendLine(" UPDATE   GUI_USERS ");
        _sb.AppendLine("    SET   GU_LOGIN_FAILURES = 0 ");
        _sb.AppendLine("  WHERE   GU_USER_ID        = @pUserId ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = UserId;

          _sql_cmd.ExecuteNonQuery();
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // ResetLoginFailures

    // PURPOSE: Increase login failures
    //
    //  PARAMS:
    //     - INPUT:
    //           - UserId:
    //           - SqlTrx:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - None
    public static void IncreaseLoginFailures(Int32 UserId,
                                               SqlTransaction SqlTrx)
    {
      try
      {
        StringBuilder _sb;

        _sb = new StringBuilder();

        _sb.AppendLine(" UPDATE   GUI_USERS ");
        _sb.AppendLine("    SET   GU_LOGIN_FAILURES = ISNULL(GU_LOGIN_FAILURES, 0) + 1 ");
        _sb.AppendLine("  WHERE   GU_USER_ID        = @pUserId ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = UserId;

          _sql_cmd.ExecuteNonQuery();
        }

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // IncreaseLoginFailures

    // PURPOSE: Check about illegal access
    //
    //  PARAMS:
    //     - INPUT:
    //           - UserInfo:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - Boolean: True->OK False->Illegal access.
    public static Boolean CheckIllegalAccess(UserInfo UserInfo)
    {
      TimeSpan _diff_time;
      Double _total_days;

      _diff_time = WASDB.Now.Subtract(UserInfo.not_valid_before);
      _total_days = _diff_time.TotalDays;
      if (_total_days < 0.0)
      {
        return false;
      }

      _diff_time = UserInfo.not_valid_after.Subtract(WASDB.Now);
      _total_days = _diff_time.TotalDays;
      if (_total_days < 0.0)
      {
        return false;
      }

      _diff_time = WASDB.Now.Subtract(UserInfo.last_changed);
      _total_days = _diff_time.TotalDays;
      if (UserInfo.password_exp_days > 0 && _total_days >= UserInfo.password_exp_days)
      {
        return false;
      }

      return true;
    } // CheckIllegalAccess

    // PURPOSE: Check session status, login_mode = REGULAR
    //
    //  PARAMS:
    //     - INPUT:
    //           - UserId:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - Boolean: True->OK False->Session opened.
    public static Boolean CheckSession(Int32 UserId,
                                        ENUM_GUI GuiId )
    {
      WSI.Common.Users.USER_SESSION_STATE _session_state;
      String _message;
      String _title;

      _message = "";
      _title = "";

      if ( GuiId == ENUM_GUI.CASHIER )
      {
        _session_state = WSI.Common.Users.GetStateUserSession("CASHIER", UserId, Environment.MachineName, out _message, out _title);
      }
      else if ( GuiId == ENUM_GUI.WIGOS_AS_GUI )
      {
        _session_state = WSI.Common.Users.GetStateUserSession("WigosGUI", UserId, Environment.MachineName, out _message, out _title);
      }
      else
      {
        _session_state = Users.USER_SESSION_STATE.OPENED;
      }


      if (_session_state == Users.USER_SESSION_STATE.OPENED)
      {
        if (UserId != 0)
        {
          return false;
        }
      }
      //else if (_session_state == Users.USER_SESSION_STATE.EXPIRED || _session_state == Users.USER_SESSION_STATE.UNEXPECTED)
      //{
        //frm_message.Show(_message, _title,
        //                 MessageBoxButtons.OK,
        //                 MessageBoxIcon.Warning,
        //                 this.ParentForm);
      //}

      return true;
    } // CheckSession

    // PURPOSE: Check permissions, login_mode = REGULAR
    //
    //  PARAMS:
    //     - INPUT:
    //           - UserId:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - Boolean: True->OK False->Don't have perms.
    public static Boolean CheckPermissions(Int32 UserId,
                                             Int32 ProfileId,
                                             ENUM_GUI GuiId,
                                             SqlTransaction SqlTrx)
    {
      try
      {
        StringBuilder _sb;
        Object _obj;
        Boolean _read_perm;

        _sb = new StringBuilder();

        _sb.AppendLine(" SELECT   GPF_READ_PERM ");
        //_sb.AppendLine("        , GPF_WRITE_PERM ");
        //_sb.AppendLine("        , GPF_DELETE_PERM ");
        //_sb.AppendLine("        , GPF_EXECUTE_PERM ");
        _sb.AppendLine("   FROM   GUI_PROFILE_FORMS ");
        _sb.AppendLine("  WHERE   GPF_PROFILE_ID = @pProfileId ");
        _sb.AppendLine("    AND   GPF_GUI_ID     = @pGuiId ");
        _sb.AppendLine("    AND   GPF_FORM_ID    = @pFormId ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pProfileId", SqlDbType.Int).Value = ProfileId;
          _sql_cmd.Parameters.Add("@pGuiId", SqlDbType.Int).Value = GuiId;

          if ( GuiId == ENUM_GUI.CASHIER )
          {
            _sql_cmd.Parameters.Add("@pFormId", SqlDbType.Int).Value =  CommonForm.FORM_LOGIN_CASHIER;
          }
          else if ( GuiId == ENUM_GUI.WIGOS_AS_GUI )
          {
            _sql_cmd.Parameters.Add("@pFormId", SqlDbType.Int).Value = CommonForm.FORM_LOGIN_GUI;
          }
          else
          {
            if (UserId != 0)
            {
              return false;
            }
          }

          _obj = _sql_cmd.ExecuteScalar();
          if (_obj == null)
          {
            if (UserId != 0)
            {
              return false;
            }
          }
          else
          {
            _read_perm = (Boolean)_obj;
            if (!_read_perm )
            {
              if (UserId != 0)
              {
                return false;
              }
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
      return true;
    } // CheckPermission

    // PURPOSE: Check if the number of days for password expiration
    //          has been reached.
    //          
    //    - INPUT: UserInfo
    //              
    //    - OUTPUT: None
    //              
    // RETURNS:
    //              TRUE: password has expired
    //              FALSE: password has NOT expired
    // NOTES
    //     We obtain the current date from the server in the format
    //     'month  dd, yyyy' because it is independent of the local computer
    //     format that could not be understanded (dd/mm/yyyy) or (mm/dd/yyyy)
    //     The visualbasic understands english and spanish name of the month.
    //
    public static Boolean CheckPasswordExpiration( UserInfo UserInfo )
    {
      System.TimeSpan date_diff;
      Int32 num_of_days;
      DateTime center_date_only;

      if ( UserInfo.user_id != 0 )
      {
        //Subtracts today minus last changed and get the number of days
        center_date_only = UserInfo.current_date.Date;
        date_diff = center_date_only.Subtract(UserInfo.last_changed);
        num_of_days = date_diff.Days;

        // check if limit has been reached
        if ((UserInfo.password_exp_days > 0) && (num_of_days >= UserInfo.password_exp_days))
        {
          //ask the user to change the password
          return true;
        }
      }
      else
      {
        // password has NOT expired
        return false;
      }
      
      // password has NOT expired
      return false;
    } //CheckPasswordExpiration

    // PURPOSE: Gets and checks the parameters entered by the user in the Login Form. 
    //	      Check if the user exists, check if password is OK, check if the user is blocked.
    //	      Check illegal access, sessions, permissions and finally if pwd not expired.
    //
    //  PARAMS:
    //      - INPUT:
    //          - UserName
    //          - Password
    //          - GuiId
    //
    //      - OUTPUT:
    //          - AccountId
    //
    // RETURNS:
    //      - LoginStatus
    // 
    //   NOTES:
    //     
    public static LoginStatus LoginUser( String UserName , 
									                       String Password ,
									                       ENUM_GUI GuiId,
									                       out Int32 AccountId )
    {
	    UserInfo _user_info;
      LoginStatus _status;
      Int32 _nls_id;
      Int32 _audit_code;
      String _description;
      WSI.Common.AlarmCode _alarm_code;
    	
	    _user_info = new UserInfo();
      _status = LoginStatus.ERROR;
      _alarm_code = AlarmCode.Unknown;
      _description = "";

      _audit_code = 4;
      _nls_id = 5000;
    	
	    try
	    {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          if ( CheckLengthUserAndPwd( UserName, Password ) )
          {
            if (!GetUserInfoInDb(UserName, _user_info, _db_trx.SqlTransaction))
            {
              _status = LoginStatus.USER_NOT_EXISTS;
            }
            else if (!CheckPassword(_user_info.password, Password))
            {
              if (_user_info.user_id != 0)
              {
                if (_user_info.block_reason == 0)
                {
                  _user_info.login_failures = _user_info.login_failures + 1;
                  if ((_user_info.login_failures >= MAX_FAILURES) && (_user_info.login_failures % MAX_FAILURES == 0))
                  {
                    DisableUser(_user_info.user_id, _db_trx.SqlTransaction);
                    _status = LoginStatus.WRONG_PASSWORD_USER_HAS_BEEN_BLOCKED;
                  }
                  else
                  {
                    IncreaseLoginFailures(_user_info.user_id, _db_trx.SqlTransaction);
                    _status = LoginStatus.WRONG_PASSWORD;
                  }
                }
                else
                {
                  _status = LoginStatus.WRONG_PASSWORD_USER_IS_BLOCKED;
                }
              }
              else
              {
                _status = LoginStatus.WRONG_PASSWORD_SUPER_USER;
              }
            }
            else if (_user_info.block_reason > 0)
            {
              _status = LoginStatus.USER_BLOCKED;
            }
            else if (!CheckIllegalAccess(_user_info))
            {
              _status = LoginStatus.ILLEGAL_ACCESS;
            }
            else if (!CheckSession(_user_info.user_id, GuiId))
            {
              _status = LoginStatus.SESION_OPENED;
            }
            else if (!CheckPermissions(_user_info.user_id, _user_info.profile_id, GuiId, _db_trx.SqlTransaction))
            {
              _status = LoginStatus.NOT_PERMISSIONS;
            }
            else
            {
              ResetLoginFailures(_user_info.user_id, _db_trx.SqlTransaction);

              if (_user_info.psw_chg_req)
              {
                _status = LoginStatus.OK_PASSWORD_CHANGE_REQ;
              }
              else
              {
                if (!CheckPasswordExpiration(_user_info))
                {
                  _status = LoginStatus.OK;
                }
                else
                {
                  _status = LoginStatus.OK_PASSWORD_EXPIRED;
                }
              }
            }
          }

          _db_trx.Commit();
        }
	    }
	    catch(Exception _ex)
	    {
		    Log.Exception(_ex);
	    }
	    finally
	    {
        AccountId = _user_info.user_id;

        switch ( _status )
        {
          case LoginStatus.ERROR:
            //_nls_id +=;
            //_audit_code = ;
            break;
          case LoginStatus.OK:
            //_nls_id +=;
            //_audit_code = ;
            break;
          case LoginStatus.USER_NOT_EXISTS:
            AccountId = -1;
            //_nls_id +=;
            //_audit_code = ;
            break;
          case LoginStatus.WRONG_PASSWORD:
            //_nls_id +=;
            //_audit_code = ;
            break;
          case LoginStatus.WRONG_PASSWORD_USER_HAS_BEEN_BLOCKED:
            _nls_id += 106;
            //_audit_code = ;
            //_description = "";
            _alarm_code = AlarmCode.User_MaxLoginAttemps;
            break;
          case LoginStatus.WRONG_PASSWORD_USER_IS_BLOCKED:
            //_nls_id +=;
            //_audit_code = ;
            break;
          case LoginStatus.WRONG_PASSWORD_SUPER_USER:
            //_nls_id +=;
            //_audit_code = ;
            //_description = "";
            //_alarm_code = AlarmCode;
            break;
          case LoginStatus.USER_BLOCKED:
            //_nls_id +=;
            //_audit_code = ;
            break;
          case LoginStatus.ILLEGAL_ACCESS:
            //_nls_id +=;
            //_audit_code = ;
            //_description = "";
            //_alarm_code = AlarmCode;
            break;
          case LoginStatus.SESION_OPENED:
            //_nls_id +=;
            //_audit_code = ;
            //_description = "";
            //_alarm_code = AlarmCode;
            break;
          case LoginStatus.NOT_PERMISSIONS:
            //_nls_id +=;
            //_audit_code = ;
            //_description = "";
            //_alarm_code = AlarmCode;
            break;
          case LoginStatus.OK_PASSWORD_EXPIRED:
            //_nls_id +=;
            //_audit_code = ;
            //_description = "";
            //_alarm_code = AlarmCode;
            break;
          case LoginStatus.OK_PASSWORD_CHANGE_REQ:
            //_nls_id +=;
            //_audit_code = ;
            //_description = "";
            //_alarm_code = AlarmCode;
            break;
          default:
            break;
        }
	    }

      // insert audit
      WSI.Common.Auditor.Audit( GuiId,                     // GuiId
                                AccountId,                 // AccountId
                                UserName,                  // UserName
                                Environment.MachineName,   // MachineName
                                _audit_code,               // AuditCode
                                _nls_id,                   // NlsId (GUI)
                                "",                        // NlsParam01
                                "",                        // NlsParam02
                                "",                        // NlsParam03
                                "",                        // NlsParam04
                                "" );                      // NlsParam05

      // insert alarm
      if (   _status == LoginStatus.WRONG_PASSWORD_SUPER_USER
          || _status == LoginStatus.NOT_PERMISSIONS
          || _status == LoginStatus.WRONG_PASSWORD_USER_HAS_BEEN_BLOCKED
          || _status == LoginStatus.ILLEGAL_ACCESS 
          || _status == LoginStatus.SESION_OPENED
          || _status == LoginStatus.NOT_PERMISSIONS )
      {
        WSI.Common.Alarm.Register( WSI.Common.AlarmSourceCode.User,           // SourceCode
                                   (Int64)GuiId,                              // SourceId
                                   UserName + "@" + Environment.MachineName,  // SourceName
                                   _alarm_code,                               // Code
                                   _description );                            // Description
      }

      return _status;
    } // LoginUser

  } // UserLogin
}
