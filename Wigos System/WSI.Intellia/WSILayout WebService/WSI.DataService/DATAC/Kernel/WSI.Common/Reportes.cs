﻿//------------------------------------------------------------------------------
// Copyright © 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Reportes.cs
// 
//   DESCRIPTION: Reportes Class
//
//        AUTHOR: RXM
// 
// CREATION DATE: 18-OCT-2012
// 
// REVISION HISTORY:
// 
// Date         Author Description
// -----------  ------ ----------------------------------------------------------
// 18-OCT-2012  RXM    First release.
// 25-OCT-2012  QMP    Added CancelGeneratedReport and ValidateReport.
// 30-OCT-2012  QMP    Added DeleteDailyReport.
// 02-NOV-2012  JCA    Added New Functionalities for WebService.
// 03-OCT-2013  JAB    Added new property "ErrorEnConstancia".
// 04-NOV-2013  JAB    Set "RD_ERROR_EN_CONSTANCIA" = 0, when deleted operations are called.
// 05-JUN-2014  AMF    Adapted to new alesis version
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace WSI.Common
{
  public static class Reportes
  {



  }
}
