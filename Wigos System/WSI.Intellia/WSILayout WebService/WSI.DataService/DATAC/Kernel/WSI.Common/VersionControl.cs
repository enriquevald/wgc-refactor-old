//------------------------------------------------------------------------------
// Copyright � 2008 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VersionControl.cs
// 
//   DESCRIPTION: Check and Get Module Version versions
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 30-OCT-2008
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 30-OCT-2008 ACC    First release.
//------------------------------------------------------------------------------

using System;
using System.Threading;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Runtime.InteropServices;
using System.IO;

namespace WSI.Common
{
  //
  // Class Version Control
  //
  public static class VersionControl
  {
    #region Structures

    //
    // Structure
    //
    [StructLayout(LayoutKind.Sequential)]
    private struct TYPE_MODULE_ABOUT
    {
      public int m_control_block;
      public int m_client_id;
      public int m_build;
      public int m_database_common_build;
      public int m_database_client_build;
      [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 16 + 3)]
      public String m_version;
      [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128 + 3)]
      public String m_company_name;
      [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128 + 3)]
      public String m_copyright;
    }

    #endregion

    #region Prototypes

    //
    // Functions Prototypes
    //
    [DllImport("CommonBase")]  //TARGET_64_BITS JCALERO
    static extern int CommonBase_VersionGetModuleVersion(String ModuleName, ref TYPE_MODULE_ABOUT pModuleAbout); //TARGET_64_BITS JCALERO

    [DllImport("CommonBase")] //TARGET_64_BITS JCALERO
    static extern int CommonBase_VersionCheckModuleVersion(String ModuleName, ref TYPE_MODULE_ABOUT pModuleAbout); //TARGET_64_BITS JCALERO

    #endregion

    #region Members

    private static TYPE_MODULE_ABOUT m_module_about;

    #endregion

    #region Properties

    /// <summary>
    /// Return string for version
    /// </summary>
    public static String VersionStr
    {
      get { return m_module_about.m_version; }
    }

    /// <summary>
    /// Return Client Id
    /// </summary>
    public static Int32 ClientId
    {
      get { return m_module_about.m_client_id; }
    }

    /// <summary>
    /// Return Build Id
    /// </summary>
    public static Int32 BuildId
    {
      get { return m_module_about.m_build; }
    }

    #endregion

    #region Public Functions

    //------------------------------------------------------------------------------
    // PURPOSE : Check if version is OK
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //      - TRUE: Version is OK.
    //      - FALSE: Version is Wrong.
    //
    public static Boolean IsVersionOk(String ModuleName)
    {
      
      //TARGET_64_BITS JCALERO
      
      m_module_about = new TYPE_MODULE_ABOUT();

      m_module_about.m_control_block = Marshal.SizeOf(typeof(TYPE_MODULE_ABOUT));
      m_module_about.m_version = "Version (?)";
      m_module_about.m_company_name = "Company (?)";
      m_module_about.m_copyright = "Copyright (?)";
      m_module_about.m_client_id = 0;
      m_module_about.m_build = 0;
      m_module_about.m_database_common_build = 0;
      m_module_about.m_database_client_build = 0;

      if (CommonBase_VersionGetModuleVersion(ModuleName, ref m_module_about) != 0)
      {
        return false;
      }

      if (CommonBase_VersionCheckModuleVersion(ModuleName, ref m_module_about) != 0)
      {
        return false;
      }

      return true;
      
    }

    #endregion

  }
}
