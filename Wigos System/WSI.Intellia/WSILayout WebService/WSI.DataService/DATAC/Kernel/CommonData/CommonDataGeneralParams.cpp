//------------------------------------------------------------------------------
// Copyright © 2002 Win Systems Ltd.
//------------------------------------------------------------------------------
//
//   MODULE NAME : CommonDataGeneralParams.cpp
//
//   DESCRIPTION : Functions and Methods to handle Common Data General Params.
//
//        AUTHOR : Luis Rodríguez
//
// CREATION DATE : 10-FEB-2003
//
// REVISION HISTORY
//
// Date        Author Description
// ----------- ------ -----------------------------------------------------------
// 04-FEB-2003 LRS    Initial draft.
// 02-NOV-2004 GZ     Added LKAS functions
//-------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// INCLUDES
//------------------------------------------------------------------------------
#define INCLUDE_COMMON_DATA
#define INCLUDE_NLS_API

#include "CommonDef.h"

//------------------------------------------------------------------------------
// PRIVATE CONSTANTS
//------------------------------------------------------------------------------
#define LKAS_FUNC_2_GENERAL_PARAM(func_code, nls_id) {sizeof (TYPE_GENERAL_PARAMS_DEFINITION), _T("LKAS_FUNCS_AREA"), _T(#func_code), _T("0"), 6 /* CheckBox */, NLS_ID_LKAS_FUNCTIONS(101), NLS_ID_LKAS_FUNCTIONS(nls_id), 0, FALSE}
#define LKAS_FUNC_2_FUNC_INFO(func_code, nls_id)     {func_code, _T(#func_code)} 

//------------------------------------------------------------------------------
// PRIVATE DATATYPES
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// PRIVATE DATA STRUCTURES
//------------------------------------------------------------------------------
static TYPE_GENERAL_PARAMS_DEFINITION GLB_GeneralParamDef [] = 
{  // Group Key, Subject Key, Default Value, Type, Group Nls, Subject Nls, Max long, Allows zero or Blanks
  { sizeof (TYPE_GENERAL_PARAMS_DEFINITION), _T("CONTROL_AREA"),    _T("CREDITS_EXPIRATION"),       _T("30")   , 0 /* Word */ ,    NLS_ID_GUI_CONF(401), NLS_ID_GUI_CONF(402),   0, FALSE},
  { sizeof (TYPE_GENERAL_PARAMS_DEFINITION), _T("CONTROL_AREA"),    _T("REPRINT_TIME_LIMIT"),       _T("30000"), 0 /* Word  */ ,   NLS_ID_GUI_CONF(401), NLS_ID_GUI_CONF(403),   0,  TRUE},
  { sizeof (TYPE_GENERAL_PARAMS_DEFINITION), _T("CONTROL_AREA"),    _T("COLLECTS_EXPIRATION"),      _T("30")   , 0 /* Word */ ,    NLS_ID_GUI_CONF(401), NLS_ID_GUI_CONF(404),   0, FALSE},
  { sizeof (TYPE_GENERAL_PARAMS_DEFINITION), _T("INVOICE_AREA"),    _T("INVOICES_DUE_DATE"),        _T("30")   , 0 /* Word */ ,    NLS_ID_GUI_CONF(434), NLS_ID_GUI_CONF(435),   0, FALSE},
  { sizeof (TYPE_GENERAL_PARAMS_DEFINITION), _T("INVOICE_AREA"),    _T("ISSUER_NAME"),              _T("")     , 2 /* String */ ,  NLS_ID_GUI_CONF(434), NLS_ID_GUI_CONF(420),  30, FALSE},
  { sizeof (TYPE_GENERAL_PARAMS_DEFINITION), _T("INVOICE_AREA"),    _T("ISSUER_ADDRESS"),           _T("")     , 2 /* String */ ,  NLS_ID_GUI_CONF(434), NLS_ID_GUI_CONF(419),  80, FALSE},
  { sizeof (TYPE_GENERAL_PARAMS_DEFINITION), _T("INVOICE_AREA"),    _T("ISSUER_ZIP_CODE"),          _T("")     , 2 /* String */ ,  NLS_ID_GUI_CONF(434), NLS_ID_GUI_CONF(418),  10, FALSE},
  { sizeof (TYPE_GENERAL_PARAMS_DEFINITION), _T("INVOICE_AREA"),    _T("ISSUER_PHONE1"),            _T("")     , 2 /* String */ ,  NLS_ID_GUI_CONF(434), NLS_ID_GUI_CONF(372),  11, FALSE},
  { sizeof (TYPE_GENERAL_PARAMS_DEFINITION), _T("INVOICE_AREA"),    _T("ISSUER_PHONE2"),            _T("")     , 2 /* String */ ,  NLS_ID_GUI_CONF(434), NLS_ID_GUI_CONF(358),  11, FALSE},
  { sizeof (TYPE_GENERAL_PARAMS_DEFINITION), _T("INVOICE_AREA"),    _T("ISSUER_BN"),                _T("")     , 2 /* String */ ,  NLS_ID_GUI_CONF(434), NLS_ID_GUI_CONF(348),  20, FALSE},
  { sizeof (TYPE_GENERAL_PARAMS_DEFINITION), _T("INVOICE_AREA"),    _T("ISSUER_LOCATION"),          _T("")     , 2 /* String */ ,  NLS_ID_GUI_CONF(434), NLS_ID_GUI_CONF(291),  20, FALSE},
  { sizeof (TYPE_GENERAL_PARAMS_DEFINITION), _T("INVOICE_AREA"),    _T("INVOICES_FOOTER"),          _T("")     , 2 /* String */,   NLS_ID_GUI_CONF(434), NLS_ID_GUI_CONF(436), 100,  TRUE}, 
  { sizeof (TYPE_GENERAL_PARAMS_DEFINITION), _T("CONTROL_AREA"),    _T("CONFIRMATION_VOUCHER"),     _T("0")    , 6 /* CheckBox */, NLS_ID_GUI_CONF(401), NLS_ID_GUI_CONF(484),   0, FALSE},
  { sizeof (TYPE_GENERAL_PARAMS_DEFINITION), _T("CONTROL_AREA"),    _T("DISPLAY_PLAYER_TIME"),      _T("0")    , 6 /* CheckBox */, NLS_ID_GUI_CONF(401), NLS_ID_GUI_CONF(472),   0, FALSE},
  { sizeof (TYPE_GENERAL_PARAMS_DEFINITION), _T("CONTROL_AREA"),    _T("SYSTEM_MODE"),              _T("0")    , 6 /* CheckBox */, NLS_ID_GUI_CONF(401), NLS_ID_GUI_CONF(82),    0, FALSE},
  { sizeof (TYPE_GENERAL_PARAMS_DEFINITION), _T("CONTROL_AREA"),    _T("WINGS_RNG_AT_CENTER"),      _T("0")    , 6 /* CheckBox */, NLS_ID_GUI_CONF(401), NLS_ID_GUI_CONF(83),    0, FALSE},
  { sizeof (TYPE_GENERAL_PARAMS_DEFINITION), _T("CONTROL_AREA"),    _T("WINGS_RNG_NUM_TO_LKT"),     _T("10")   , 0 /* Word  */,    NLS_ID_GUI_CONF(401), NLS_ID_GUI_CONF(84),    0, FALSE},
  { sizeof (TYPE_GENERAL_PARAMS_DEFINITION), _T("SALES_AREA"),      _T("BATCH_TIME_RESOLUT"),       _T("1440") , 0 /* Word  */ ,   NLS_ID_GUI_CONF(473), NLS_ID_GUI_CONF(475),   0, FALSE},
  { sizeof (TYPE_GENERAL_PARAMS_DEFINITION), _T("SALES_AREA"),      _T("ONLINE_TIME_RESOLUT"),      _T("60")   , 0 /* Word  */ ,   NLS_ID_GUI_CONF(473), NLS_ID_GUI_CONF(474),   0, FALSE},
  { sizeof (TYPE_GENERAL_PARAMS_DEFINITION), _T("SERVICE_PARAM"),   _T("NUM_WORKERS_BATCH"),        _T("20")   , 0 /* Word  */ ,   NLS_ID_GUI_CONF(51), NLS_ID_GUI_CONF(52),     0, FALSE},
  { sizeof (TYPE_GENERAL_PARAMS_DEFINITION), _T("SERVICE_PARAM"),   _T("NUM_WORKERS_SWDL"),         _T("20")   , 0 /* Word  */ ,   NLS_ID_GUI_CONF(51), NLS_ID_GUI_CONF(53),     0, FALSE},
  { sizeof (TYPE_GENERAL_PARAMS_DEFINITION), _T("SERVICE_PARAM"),   _T("NUM_WORKERS_ON_DEF"),       _T("2")    , 0 /* Word  */ ,   NLS_ID_GUI_CONF(51), NLS_ID_GUI_CONF(54),     0, FALSE},
  { sizeof (TYPE_GENERAL_PARAMS_DEFINITION), _T("SERVICE_PARAM"),   _T("NUM_WORKERS_ON_SALES"),     _T("20")   , 0 /* Word  */ ,   NLS_ID_GUI_CONF(51), NLS_ID_GUI_CONF(55),     0, FALSE},
  { sizeof (TYPE_GENERAL_PARAMS_DEFINITION), _T("SERVICE_PARAM"),   _T("NUM_WORKERS_ON_DEVIC"),     _T("2")    , 0 /* Word  */ ,   NLS_ID_GUI_CONF(51), NLS_ID_GUI_CONF(56),     0, FALSE},
  { sizeof (TYPE_GENERAL_PARAMS_DEFINITION), _T("SERVICE_PARAM"),   _T("NUM_WORKERS_ON_EVENT"),     _T("2")    , 0 /* Word  */ ,   NLS_ID_GUI_CONF(51), NLS_ID_GUI_CONF(57),     0, FALSE},
  { sizeof (TYPE_GENERAL_PARAMS_DEFINITION), _T("SERVICE_PARAM"),   _T("NUM_WORKERS_ON_VERS"),      _T("2")    , 0 /* Word  */ ,   NLS_ID_GUI_CONF(51), NLS_ID_GUI_CONF(58),     0, FALSE},
  { sizeof (TYPE_GENERAL_PARAMS_DEFINITION), _T("SERVICE_PARAM"),   _T("MAX_SESSIONS_BATCH"),       _T("100")  , 0 /* Word  */ ,   NLS_ID_GUI_CONF(51), NLS_ID_GUI_CONF(59),     0, FALSE},
  { sizeof (TYPE_GENERAL_PARAMS_DEFINITION), _T("SERVICE_PARAM"),   _T("MAX_SESSIONS_SWDL"),        _T("100")  , 0 /* Word  */ ,   NLS_ID_GUI_CONF(51), NLS_ID_GUI_CONF(60),     0, FALSE},

  // JMP 03/10/2005
  // Game's counter configuration
  { sizeof (TYPE_GENERAL_PARAMS_DEFINITION), _T("GAME_COUNT"),      _T("APPLY_LIMIT"),              _T("0")    , 6 /* CheckBox */, NLS_ID_GUI_CONF(61), NLS_ID_GUI_CONF(62),     0, FALSE},
  { sizeof (TYPE_GENERAL_PARAMS_DEFINITION), _T("GAME_COUNT"),      _T("SHOW_COUNT"),               _T("0")    , 6 /* CheckBox */, NLS_ID_GUI_CONF(61), NLS_ID_GUI_CONF(63),     0, FALSE},
  { sizeof (TYPE_GENERAL_PARAMS_DEFINITION), _T("GAME_COUNT"),      _T("RESET_INTERVAL"),           _T("30")   , 0 /* Word  */ ,   NLS_ID_GUI_CONF(61), NLS_ID_GUI_CONF(64),     0, FALSE},
  { sizeof (TYPE_GENERAL_PARAMS_DEFINITION), _T("GAME_COUNT"),      _T("CASH_LIMIT_PER_GAME"),      _T("75")   , 0 /* Word  */ ,   NLS_ID_GUI_CONF(61), NLS_ID_GUI_CONF(65),     0, FALSE},

  // JMP 04/10/2005
  // Smart Card configuration
  { sizeof (TYPE_GENERAL_PARAMS_DEFINITION), _T("SMART_CARD"),     _T("PURSE_CARD"),                _T("1")    , 6 /* CheckBox */, NLS_ID_GUI_CONF(71), NLS_ID_GUI_CONF(72),     0, FALSE},

  // ACC 03-NOV-2005
  // Minimum bet amount
  { sizeof (TYPE_GENERAL_PARAMS_DEFINITION), _T("CONTROL_AREA"),   _T("MINIMUM_BET_AMOUNT"),        _T("0")    , 0 /* Word  */ ,   NLS_ID_GUI_CONF(401), NLS_ID_GUI_CONF(73),    0, TRUE},

  // LKAS Functions
  LKAS_FUNC_2_GENERAL_PARAM (LKAS_FUNC_LOGIN, 1),
  LKAS_FUNC_2_GENERAL_PARAM (LKAS_FUNC_LOGOUT, 2),
  LKAS_FUNC_2_GENERAL_PARAM (LKAS_FUNC_CHANGE_PWD, 3)

};

static TYPE_LKAS_FUNCTION_INFO GLB_LkasFuncsInfo [] = 
{  // Function Code, Function Code String (as saved in the databse)
  LKAS_FUNC_2_FUNC_INFO (LKAS_FUNC_LOGIN, 1),
  LKAS_FUNC_2_FUNC_INFO (LKAS_FUNC_LOGOUT, 2),
  LKAS_FUNC_2_FUNC_INFO (LKAS_FUNC_CHANGE_PWD, 3)
};
//------------------------------------------------------------------------------
// PUBLIC FUNCTIONS IMPLEMENTATION
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE : Gets General Params Definitions number
//
//  PARAMS :
//      - INPUT :  none
//
//      - OUTPUT : none
//
// RETURNS : General Params Definitions number
//
//   NOTES :
//
COMMON_DATA_API DWORD WINAPI Common_GeneralParamDefNum (void)
{
    return sizeof (GLB_GeneralParamDef) / sizeof (TYPE_GENERAL_PARAMS_DEFINITION);

} //Common_GeneralParamDefNum

//------------------------------------------------------------------------------
// PURPOSE : Gets General Param definition
//
//   PARAMS :
//
//     - INPUT :
//       - GeneralParamIndex: The index of the requested General Param
//
//     - OUTPUT :
//       - pGeneralParam    : The General Param definition
//
// RETURNS :
//  - TRUE, on success
//  - FALSE, otherwise
// 
//   NOTES :

COMMON_DATA_API BOOL WINAPI Common_GeneralParamGet (DWORD                           GeneralParamIndex, 
                                                    TYPE_GENERAL_PARAMS_DEFINITION  * pGeneralParam)
{
  if ( pGeneralParam->control_block != sizeof (TYPE_GENERAL_PARAMS_DEFINITION) )
  {
    return FALSE;
  }

  if ( GeneralParamIndex >= Common_GeneralParamDefNum() )
  {
    return FALSE;
  }

  memcpy (pGeneralParam, &GLB_GeneralParamDef[GeneralParamIndex], sizeof (TYPE_GENERAL_PARAMS_DEFINITION));

  return TRUE;
} // Common_GeneralParamGet

//------------------------------------------------------------------------------
// PURPOSE : Gets the function code of the given function string (as saved in db)
//
//  PARAMS :
//      - INPUT : 
//          - pFuncString: Agency function string
//
//      - OUTPUT : 
//          - pFuncCode: the Code of the agency function ( if not found then 
//                       return LKAS_FUNC_NONE
//
// RETURNS : 
//   NOTES :
//
COMMON_DATA_API void WINAPI Common_GetLkasFuncCodeFromString (TCHAR                 * pFuncString, 
                                                              ENUM_LKAS_FUNCTION_ID * pFuncCode)
{
  DWORD               _num_funcs;
  DWORD               _idx_func_code;


  _num_funcs = sizeof (GLB_LkasFuncsInfo) / sizeof (TYPE_LKAS_FUNCTION_INFO);

  // Search Function Code
  for ( _idx_func_code = 0; _idx_func_code < _num_funcs; _idx_func_code++ )
  {
    if ( _tcscmp(pFuncString, GLB_LkasFuncsInfo[_idx_func_code].func_string) == 0 )
    {
      * pFuncCode = GLB_LkasFuncsInfo[_idx_func_code].func_code;
      return;
    } //if
  } //for

  // Not found
  * pFuncCode = LKAS_FUNC_NONE;

  return;

} // Common_GetLkasFuncCodeFromString
