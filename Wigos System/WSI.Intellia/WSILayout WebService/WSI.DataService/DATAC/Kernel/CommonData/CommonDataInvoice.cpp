//------------------------------------------------------------------------------
// Copyright � 2005 Win Systems Ltd.
//------------------------------------------------------------------------------
//
//   MODULE NAME : CommonDataInvoic.cpp
//
//   DESCRIPTION : Functions and Methods to handle common data invoices.
//
//        AUTHOR : Toni Jord�
//
// CREATION DATE : 21-JUN-2005
//
// REVISION HISTORY
//
// Date        Author Description
// ----------- ------ -----------------------------------------------------------
// 21-JUN-2005 TJG    Initial draft.
//
//-------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  INCLUDES
//------------------------------------------------------------------------------
#define INCLUDE_NLS_API
#define INCLUDE_COMMON_DATA
#define INCLUDE_COMMON_MISC
#include "CommonDef.h"

//------------------------------------------------------------------------------
//  PRIVATE CONSTANTS
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PRIVATE DATATYPES
//------------------------------------------------------------------------------
  
//------------------------------------------------------------------------------
//  PRIVATE DATA STRUCTURES
//------------------------------------------------------------------------------

static TYPE_INVOICE_MOVEMENT GLB_InvoiceMovements [] = 
{ 
  // control_block                    movement_id                             nls_id
    {sizeof (TYPE_INVOICE_MOVEMENT) , MOVEMENT_TYPE_TICKETS_SALES           , NLS_ID_GUI_INVOICE_SYSTEM (401) }
  , {sizeof (TYPE_INVOICE_MOVEMENT) , MOVEMENT_TYPE_MAJOR_PRIZES            , NLS_ID_GUI_INVOICE_SYSTEM (402) }
  , {sizeof (TYPE_INVOICE_MOVEMENT) , MOVEMENT_TYPE_MINOR_PRIZES            , NLS_ID_GUI_INVOICE_SYSTEM (403) }
  , {sizeof (TYPE_INVOICE_MOVEMENT) , MOVEMENT_TYPE_SPECIAL_PRIZES          , NLS_ID_GUI_INVOICE_SYSTEM (404) }

  , {sizeof (TYPE_INVOICE_MOVEMENT) , MOVEMENT_TYPE_NETWIN                  , NLS_ID_GUI_INVOICE_SYSTEM (401) }
  , {sizeof (TYPE_INVOICE_MOVEMENT) , MOVEMENT_TYPE_SALE_COMMISSIONS        , NLS_ID_GUI_INVOICE_SYSTEM (406) }
  , {sizeof (TYPE_INVOICE_MOVEMENT) , MOVEMENT_TYPE_MAJOR_PRIZE_COMMISSIONS , NLS_ID_GUI_INVOICE_SYSTEM (407) } // 402
  , {sizeof (TYPE_INVOICE_MOVEMENT) , MOVEMENT_TYPE_MINOR_PRIZE_COMMISSIONS , NLS_ID_GUI_INVOICE_SYSTEM (408) } // 403
  , {sizeof (TYPE_INVOICE_MOVEMENT) , MOVEMENT_TYPE_EXPIRED_COLLECTS        , NLS_ID_GUI_INVOICE_SYSTEM (409) }
  , {sizeof (TYPE_INVOICE_MOVEMENT) , MOVEMENT_TYPE_EXPIRED_CARDS           , NLS_ID_GUI_INVOICE_SYSTEM (410) }
  , {sizeof (TYPE_INVOICE_MOVEMENT) , MOVEMENT_TYPE_SYSTEM_MAX_ID           , NLS_ID_GUI_INVOICE_SYSTEM (411) }
};

//------------------------------------------------------------------------------
//  PRIVATE FUNCTION PROTOTYPES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PRIVATE FUNCTIONS IMPLEMENTATION
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PUBLIC FUNCTIONS IMPLEMENTATION
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE : 
//
//  PARAMS :
//      - INPUT :
//          - pInvoiveMovement->movement_id
//
//      - OUTPUT :
//          - pInvoiveMovement->nls_id
//
// RETURNS : 
//
//   NOTES :

COMMON_DATA_API BOOL WINAPI Common_GetInvoiceMovement (TYPE_INVOICE_MOVEMENT   * pInvoiveMovement)
{
  WORD      _idx_mov;

  if ( pInvoiveMovement->control_block != sizeof (TYPE_INVOICE_MOVEMENT) )
  {
    return FALSE;
  }

  for ( _idx_mov = 0; _idx_mov < Common_ArraySize(GLB_InvoiceMovements); _idx_mov++ )
  {
    if ( GLB_InvoiceMovements[_idx_mov].movement_id == pInvoiveMovement->movement_id )
    {
      * pInvoiveMovement = GLB_InvoiceMovements[_idx_mov];

      return TRUE;
    }
  }

  return FALSE;
} // Common_GetInvoiceMovement 
