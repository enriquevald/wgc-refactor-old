//------------------------------------------------------------------------------
// Copyright © 2002 Win Systems Ltd.
//------------------------------------------------------------------------------
//
//   MODULE NAME : CommonDataLaunchBar.cpp
//
//   DESCRIPTION : Functions and Methods to handle Common Data Launch Bar.
//
//        AUTHOR : Luis Rodríguez
//
// CREATION DATE : 02-DIC-2003
//
// REVISION HISTORY
//
// Date        Author Description
// ----------- ------ -----------------------------------------------------------
// 02-DIC-2003 LRS    Initial draft.
//
//-------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// INCLUDES
//------------------------------------------------------------------------------
#define INCLUDE_COMMON_DATA
#define INCLUDE_COMMON_MISC
#define INCLUDE_NLS_API


#include "CommonDef.h"

//------------------------------------------------------------------------------
// PRIVATE CONSTANTS
//------------------------------------------------------------------------------

// Index Image List (img_list in GUI_LaunchBar)
#define IMG_LIST_IDX_ALARMS     0
#define IMG_LIST_IDX_AUDITOR    1
#define IMG_LIST_IDX_COMM       2
#define IMG_LIST_IDX_CONFIG     3
#define IMG_LIST_IDX_D_CODE     4
#define IMG_LIST_IDX_INVOICE    5
#define IMG_LIST_IDX_JACKPOT    6
#define IMG_LIST_IDX_MONITOR    7
#define IMG_LIST_IDX_SW_DOWN    8
#define IMG_LIST_IDX_STATIST    9
#define IMG_LIST_IDX_TMG       10
#define IMG_LIST_IDX_VERSION   11

//------------------------------------------------------------------------------
// PRIVATE DATATYPES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE DATA STRUCTURES
//------------------------------------------------------------------------------
 
 
static TYPE_LAUNCH_BAR_DEFINITION GLB_LaunchBarDef [] = 
{  // Caption NLS_ID_GUI_LAUNCH, Tooltip NLS_ID_GUI_LAUNCH, Executable, Image, Index Image List 
  { sizeof (TYPE_LAUNCH_BAR_DEFINITION),  NLS_ID_GUI_LAUNCH(15) ,NLS_ID_GUI_LAUNCH(5)  , _T("GUI_Configuration.exe")    , _T("GUI_Configuration.ico")    , 1 , IMG_LIST_IDX_CONFIG},
  { sizeof (TYPE_LAUNCH_BAR_DEFINITION),  NLS_ID_GUI_LAUNCH(15) ,NLS_ID_GUI_LAUNCH(8)  , _T("GUI_TicketsManager.exe")   , _T("GUI_TicketsManager.ico")   , 0 , IMG_LIST_IDX_TMG},
  { sizeof (TYPE_LAUNCH_BAR_DEFINITION),  NLS_ID_GUI_LAUNCH(15) ,NLS_ID_GUI_LAUNCH(10) , _T("GUI_JackpotManager.exe")   , _T("GUI_JackpotManager.ico")   , 0 , IMG_LIST_IDX_JACKPOT},
  { sizeof (TYPE_LAUNCH_BAR_DEFINITION),  NLS_ID_GUI_LAUNCH(15) ,NLS_ID_GUI_LAUNCH(3)  , _T("GUI_Auditor.exe")          , _T("GUI_Auditor.ico")          , 0 , IMG_LIST_IDX_AUDITOR},
  { sizeof (TYPE_LAUNCH_BAR_DEFINITION),  NLS_ID_GUI_LAUNCH(15) ,NLS_ID_GUI_LAUNCH(6)  , _T("GUI_Communications.exe")   , _T("GUI_Communications.ico")   , 1 , IMG_LIST_IDX_COMM},
  { sizeof (TYPE_LAUNCH_BAR_DEFINITION),  NLS_ID_GUI_LAUNCH(15) ,NLS_ID_GUI_LAUNCH(11) , _T("GUI_SystemMonitor.exe")    , _T("GUI_SystemMonitor.ico")    , 0 , IMG_LIST_IDX_MONITOR},
  { sizeof (TYPE_LAUNCH_BAR_DEFINITION),  NLS_ID_GUI_LAUNCH(15) ,NLS_ID_GUI_LAUNCH(1)  , _T("GUI_Alarms.exe")           , _T("GUI_Alarms.ico")           , 0 , IMG_LIST_IDX_ALARMS},
  // AJQ 04-DEC-2006, Invoice System doesn't work in WINGS
  //{ sizeof (TYPE_LAUNCH_BAR_DEFINITION),  NLS_ID_GUI_LAUNCH(15) ,NLS_ID_GUI_LAUNCH(13) , _T("GUI_InvoiceSystem.exe")    , _T("GUI_InvoiceSystem.ico")    , 1 , IMG_LIST_IDX_INVOICE},
  { sizeof (TYPE_LAUNCH_BAR_DEFINITION),  NLS_ID_GUI_LAUNCH(15) ,NLS_ID_GUI_LAUNCH(9)  , _T("GUI_Statistics.exe")       , _T("GUI_Statistics.ico")       , 0 , IMG_LIST_IDX_STATIST},
  { sizeof (TYPE_LAUNCH_BAR_DEFINITION),  NLS_ID_GUI_LAUNCH(15) ,NLS_ID_GUI_LAUNCH(14) , _T("GUI_SoftwareDownload.exe") , _T("GUI_SoftwareDownload.ico") , 1 , IMG_LIST_IDX_SW_DOWN},
  { sizeof (TYPE_LAUNCH_BAR_DEFINITION),  NLS_ID_GUI_LAUNCH(15) ,NLS_ID_GUI_LAUNCH(2)  , _T("GUI_VersionViewer.exe")    , _T("GUI_VersionViewer.ico")    , 0 , IMG_LIST_IDX_VERSION},
  { sizeof (TYPE_LAUNCH_BAR_DEFINITION),  NLS_ID_GUI_LAUNCH(15) ,NLS_ID_GUI_LAUNCH(39) , _T("GUI_DateCode.exe")         , _T("GUI_DateCode.ico")         , 1 , IMG_LIST_IDX_D_CODE}
};                                                                                                                                                           
                                                                                                                                                             
//------------------------------------------------------------------------------                                                                             
// PRIVATE FUNCTIONS IMPLEMENTATION                                                                                                                          
//------------------------------------------------------------------------------                                                                             
                                                                                                                                                             
//------------------------------------------------------------------------------
// PUBLIC FUNCTIONS IMPLEMENTATION
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE : Gets Launch Bar Definitions number
//
//  PARAMS :
//      - INPUT :  none
//
//      - OUTPUT : none
//
// RETURNS : Launch Bar Definitions number
//
//   NOTES :
//
COMMON_DATA_API DWORD WINAPI Common_LaunchBarDefNum (void)
{
    return sizeof (GLB_LaunchBarDef) / sizeof (TYPE_LAUNCH_BAR_DEFINITION);

} //Common_LaunchBarDefNum

//------------------------------------------------------------------------------
// PURPOSE : Gets Launch Bar definition
//
//   PARAMS :
//
//     - INPUT :
//       - LaunchBarIndex: The index of the requested Launch Bar
//
//     - OUTPUT :
//       - pLaunchBar    : The Launch Bar definition
//
// RETURNS :
//  - TRUE, on success
//  - FALSE, otherwise
//
//   NOTES :

COMMON_DATA_API BOOL WINAPI Common_LaunchBarGet (DWORD                       LaunchBarIndex, 
                                                 TYPE_LAUNCH_BAR_DEFINITION  * pLaunchBar)
{
  if ( pLaunchBar->control_block != sizeof (TYPE_LAUNCH_BAR_DEFINITION) )
  {
    return FALSE;
  }

  if ( LaunchBarIndex >= sizeof (GLB_LaunchBarDef) / sizeof (TYPE_LAUNCH_BAR_DEFINITION) )
  {
    return FALSE;
  }

  memcpy (pLaunchBar, &GLB_LaunchBarDef[LaunchBarIndex], sizeof (TYPE_LAUNCH_BAR_DEFINITION));

  return TRUE;
} // Common_LaunchBarGet
