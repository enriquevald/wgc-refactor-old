//------------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//------------------------------------------------------------------------------
//
//   MODULE NAME : CommonDataAuditor.cpp
//
//   DESCRIPTION : Functions and Methods to handle common data Auditor.
//
//        AUTHOR : Ignasi Ripoll
//
// CREATION DATE : 15-DEC-2003
//
// REVISION HISTORY
//
// Date        Author Description
// ----------- ------ -----------------------------------------------------------
// 15-DEC-2003 IRP    Initial draft.
//
//-------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  INCLUDES
//------------------------------------------------------------------------------
#define INCLUDE_NLS_API
#define INCLUDE_COMMON_DATA
#define INCLUDE_COMMON_MISC

#include "CommonDef.h"

//------------------------------------------------------------------------------
//  PRIVATE CONSTANTS
//------------------------------------------------------------------------------

#define GUI_WIGOS              14

// Audit names are defined on WigosGUI on the following mdl_globals.
// Changes on the current codes will make incompatible audit codes with older versions display.
// GUI_CommonMisc:
#define AUDIT_NAME_GENERIC                0
// GUI_Controls:
#define AUDIT_NAME_LOGIN_LOGOUT           1
#define AUDIT_NAME_TIME_ZONE              2
// WigosGUI:
#define AUDIT_NAME_USERS                  3
#define AUDIT_NAME_PROFILES               4
#define AUDIT_NAME_GENERAL_PARAMS         5
#define AUDIT_NAME_ALARMS                 7
#define AUDIT_NAME_HIERARCHIES            8
#define AUDIT_NAME_REPORTS                9
#define AUDIT_NAME_CATALOGS               10
#define AUDIT_NAME_IP_CONFIG              11

#define AUDIT_NAME_SOFTWARE_DOWNLOAD      23

//------------------------------------------------------------------------------
//  PRIVATE DATATYPES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PRIVATE DATA STRUCTURES
//------------------------------------------------------------------------------

static TYPE_GUI_NAME GLB_GUIName [] = 
{ 
  { sizeof (TYPE_GUI_NAME)   , GUI_WIGOS                         , NLS_ID_GUI_AUDIT (100) }
};

// Audit codes defined here should be those who wanted to have a grouped operations
static TYPE_AUDIT_NAME GLB_AuditName [] = 
{ 
  { sizeof (TYPE_AUDIT_NAME) , AUDIT_NAME_LOGIN_LOGOUT           , NLS_ID_GUI_AUDIT (81) }
, { sizeof (TYPE_AUDIT_NAME) , AUDIT_NAME_USERS                  , NLS_ID_GUI_AUDIT (56) }
, { sizeof (TYPE_AUDIT_NAME) , AUDIT_NAME_PROFILES               , NLS_ID_GUI_AUDIT (57) }
, { sizeof (TYPE_AUDIT_NAME) , AUDIT_NAME_GENERAL_PARAMS         , NLS_ID_GUI_AUDIT (67) }
, { sizeof (TYPE_AUDIT_NAME) , AUDIT_NAME_SOFTWARE_DOWNLOAD      , NLS_ID_GUI_AUDIT (71) }
, { sizeof (TYPE_AUDIT_NAME) , AUDIT_NAME_ALARMS                 , NLS_ID_GUI_AUDIT (84) }
, { sizeof (TYPE_AUDIT_NAME) , AUDIT_NAME_HIERARCHIES            , NLS_ID_GUI_AUDIT (70) }
, { sizeof (TYPE_AUDIT_NAME) , AUDIT_NAME_REPORTS                , NLS_ID_GUI_AUDIT (69) }
, { sizeof (TYPE_AUDIT_NAME) , AUDIT_NAME_CATALOGS               , NLS_ID_GUI_AUDIT (68) }
, { sizeof (TYPE_AUDIT_NAME) , AUDIT_NAME_IP_CONFIG              , NLS_ID_GUI_AUDIT (86) }
};

//------------------------------------------------------------------------------
//  PRIVATE FUNCTION PROTOTYPES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PRIVATE FUNCTIONS IMPLEMENTATION
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PUBLIC FUNCTIONS IMPLEMENTATION
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE : Return the number of GUI�s registered.
//
//  PARAMS :
//      - INPUT :
//
//      - OUTPUT :
//
// RETURNS : Number of GUI�s registered.
//
//   NOTES :
//
COMMON_DATA_API DWORD WINAPI Common_GUINameNumNodes (void)
{
  return sizeof (GLB_GUIName) / sizeof (TYPE_GUI_NAME);

} // Common_GUINameNumNodes

//------------------------------------------------------------------------------
// PURPOSE : Gets GUI Name node
//
//   PARAMS :
//
//     - INPUT :
//       - NodeIndex: The index of the requested node
//
//     - OUTPUT :
//       - pGUINameNode: The node GUI Name
//
// RETURNS :
//  - TRUE,  on success
//  - FALSE, otherwise
//
//   NOTES :

COMMON_DATA_API BOOL WINAPI Common_GUINameGetNode (DWORD         NodeIndex, 
                                                   TYPE_GUI_NAME * pGUINameNode)
{
  if ( pGUINameNode->control_block != sizeof (TYPE_GUI_NAME) )
  {
    return FALSE;
  }

  if ( NodeIndex >= sizeof (GLB_GUIName) / sizeof (TYPE_GUI_NAME) )
  {
    return FALSE;
  }

  memcpy (pGUINameNode, &GLB_GUIName[NodeIndex], sizeof (TYPE_GUI_NAME));

  return TRUE;

} // Common_GUINameGetNode


//------------------------------------------------------------------------------
// PURPOSE : Gets Audit Names nodes number
//
//  PARAMS :
//      - INPUT : None
//
//      - OUTPUT : None
//
// RETURNS : Audit Names nodes number
//
//   NOTES :
//
COMMON_DATA_API DWORD WINAPI Common_AuditNameNumNodes (void)
{
  return sizeof (GLB_AuditName) / sizeof (TYPE_AUDIT_NAME);

} // Common_AuditNameNumNodes

//------------------------------------------------------------------------------
// PURPOSE : Gets Audit Name node
//
//   PARAMS :
//
//     - INPUT :
//       - NodeIndex: The index of the requested node
//
//     - OUTPUT :
//       - pGUIAuditNode: The node Audit Name
//
// RETURNS :
//  - TRUE,  on success
//  - FALSE, otherwise
//
//   NOTES :
COMMON_DATA_API BOOL WINAPI Common_AuditNameGetNode (DWORD           NodeIndex, 
                                                     TYPE_AUDIT_NAME * pGUIAuditNode)
{
  if ( pGUIAuditNode->control_block != sizeof (TYPE_AUDIT_NAME) )
  {
    return FALSE;
  }

  if ( NodeIndex >= sizeof (GLB_AuditName) / sizeof (TYPE_AUDIT_NAME) )
  {
    return FALSE;
  }

  memcpy (pGUIAuditNode, &GLB_AuditName[NodeIndex], sizeof (TYPE_AUDIT_NAME));

  return TRUE;

} // Common_AuditNameGetNode

