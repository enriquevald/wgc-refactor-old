// ------------------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
// ------------------------------------------------------------------------------------
//
//   MODULE NAME: NLS_010_GUIControls.rc
//
//   DESCRIPTION: NLS resource LKC GUI Controls
//
//        AUTHOR: Agust� Poch
//
// CREATION DATE: 25-MAR-2002
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ -------------------------------------------------------------------
// 25-MAR-2002 APB    Initial release.
// 05-APR-2002 APB    Renamed GUICommon_Dialogs to GUIControls. Reformatted messages.
// 16-APR-2002 APB    Added new strings (104-107).
// 17-APR-2002 APB    Added new string (108).
// 18-APR-2002 APB    Added new strings (11; 109; 272).
// 23-APR-2002 APB    Added new string (110).
// 03-MAY-2002 APB    Added new string (111); reformatted strings (256; 261; 264).
// 08-MAY-2002 APB    Added new string (12).
// 15-MAY-2002 APB    Added new strings (112-113).
// 17-MAY-2002 APB    Added new string (273); reformatted strings (110-111; 256).
// 23-MAY-2002 APB    Reformatted string (256).
// 10-JUN-2002 ACC    Added new strings (274-292).
// 11-JUN-2002 ACC    Added new strings (293-295); reformatted string (292).
// 19-JUN-2002 APB    Reformatted string (279).
// 02-JUL-2002 APB    Added new strings (296-297); reformatted strings (260, 286).
// 11-JUL-2002 APB    Reformatted string (260).
// 06-SEP-2002 APB    Added new strings (114-116).
// 17-SEP-2002 APB    Added new strings (13-17; 117-118).
// 18-SEP-2002 APB    Reformatted strings (14-17).
// 20-SEP-2002 APB    Added new string (119).
// 18-OCT-2002 APB    Added new strings (16-17; 120);
//                    reformatted strings (13-14;275-278; 280-294; 297).
// 25-OCT-2002 APB    Added new string (121).
// 14-NOV-2002 APB    Reformatted strings (275-278; 280-285).
// 18-NOV-2002 APB    Reformatted string (13).
// 19-NOV-2002 APB    Reformatted string (283).
// 21-NOV-2002 APB    Added new string (122).
// 09-DEC-2002 APB    Reformatted string (104).
// 13-JAN-2003 APB    Reformatted strings (262-263; 286-287).
// 14-JAN-2003 APB    Reformatted strings (263; 288).
// 21-JAN-2003 APB    Reformatted strings (289-290).
// 20-MAR-2003 APB    Reformatted strings (291-292).
// 07-APR-2003 APB    Reformatted strings (293-294).
// 11-APR-2003 APB    Added new strings (125; 297-310).
// 14-APR-2003 APB    Added new strings (126; 311-313).
// 22-DEC-2004 GZ     Added new strings (18-20).
// ---------------------------------------------------------------------------------------

// WARNING !!!
//
// Do NOT modify this file with Visual Studio Resource Editor.
// This file MUST be edited using a TEXT editor.
//
// WARNING !!!

STRINGTABLE DISCARDABLE
BEGIN

  // IDs from 1 to 50 are reserved for buttons
  // IDs from 51 to 100 are reserved for application specific requirements
  // IDs from 101 to 200 are reserved for messages
  // IDs from 201 to 450 are reserved for Labels
  // IDs from 451 to 499 are reserved for Menu Items


  // Buttons: 1-50
  NLS_ID_GUI_CONTROLS(1)     "Aceptar"
  NLS_ID_GUI_CONTROLS(2)     "Cancelar"
  NLS_ID_GUI_CONTROLS(3)     "S�"
  NLS_ID_GUI_CONTROLS(4)     "No"
  NLS_ID_GUI_CONTROLS(5)     "Imprimir"
  NLS_ID_GUI_CONTROLS(6)     "Nuevo"
  NLS_ID_GUI_CONTROLS(7)     "Reset"
  NLS_ID_GUI_CONTROLS(8)     "Buscar"
  NLS_ID_GUI_CONTROLS(9)     "Seleccionar"
  NLS_ID_GUI_CONTROLS(10)    "Salir"
  NLS_ID_GUI_CONTROLS(11)    "Borrar"
  NLS_ID_GUI_CONTROLS(12)    "A�adir"
  NLS_ID_GUI_CONTROLS(13)    "Guardar"
  NLS_ID_GUI_CONTROLS(14)    "Sesi�n"
  NLS_ID_GUI_CONTROLS(15)    "Nuevo Oper."
  NLS_ID_GUI_CONTROLS(16)    "Nuevo Dist."
  NLS_ID_GUI_CONTROLS(17)    "Nueva Agen."
  NLS_ID_GUI_CONTROLS(18)    "�ltimo Seleccionado"
  NLS_ID_GUI_CONTROLS(19)    "Reset"
  NLS_ID_GUI_CONTROLS(20)    "Seleccionar"
  NLS_ID_GUI_CONTROLS(21)    "Apagar"
  NLS_ID_GUI_CONTROLS(22)    "Reiniciar"
  NLS_ID_GUI_CONTROLS(23)    "(+)"
  NLS_ID_GUI_CONTROLS(24)    "(-)"
  NLS_ID_GUI_CONTROLS(25)    "Editar"
  NLS_ID_GUI_CONTROLS(26)    "Ver Logger"
  NLS_ID_GUI_CONTROLS(27)    "Excel"
  NLS_ID_GUI_CONTROLS(28)    "Filtrar"
  // JGR 20-02-2012: frm_area_bank_sel.vb
  NLS_ID_GUI_CONTROLS(29)    "Nueva �rea"
  NLS_ID_GUI_CONTROLS(30)    "Nueva Isla"
	NLS_ID_GUI_CONTROLS(31)    "Aplicar"


  // IDs from 101 to 200 are reserved for messages
  NLS_ID_GUI_CONTROLS(101)   "Los cambios realizados se perder�n.\n�Est� seguro que quiere salir?"
  NLS_ID_GUI_CONTROLS(102)   "Se va a proceder al borrado de los datos de %1 %2 %3.\n�Confirma la operaci�n?"
  NLS_ID_GUI_CONTROLS(103)   "No se guardaron los cambios debido a un error en la BD."
  NLS_ID_GUI_CONTROLS(104)   "La nueva clave debe cumplir las siguientes condiciones:\n    - Debe tener un m�ximo de %1 caracteres.\n    - S�lo puede contener n�meros y letras est�ndar (del alfabeto ingl�s)\n    - No debe haber sido utilizada anteriormente."
  NLS_ID_GUI_CONTROLS(105)   "La clave de acceso ha sido cambiada."
  NLS_ID_GUI_CONTROLS(106)   "El usuario ha sido bloqueado."
  NLS_ID_GUI_CONTROLS(107)   "Acceso denegado."
  NLS_ID_GUI_CONTROLS(108)   "La nueva clave y la confirmaci�n no coinciden."
  NLS_ID_GUI_CONTROLS(109)   "El usuario no tiene permiso para realizar esta operaci�n."
  NLS_ID_GUI_CONTROLS(110)   "Va a ser desconectado de la aplicaci�n.\n�Est� seguro que quiere salir?"
  NLS_ID_GUI_CONTROLS(111)   "El n�mero de resultados obtenidos excede el m�ximo permitido.\nS�lo se mostrar�n %1."
  NLS_ID_GUI_CONTROLS(112)   "El usuario s�lo puede crear o modificar jerarqu�as de nivel inferior al suyo."
  NLS_ID_GUI_CONTROLS(113)   "El usuario s�lo puede seleccionar jerarqu�as de nivel igual o inferior al suyo."
  NLS_ID_GUI_CONTROLS(114)   "Se han detectado nuevas funcionalidades. �Desea actualizar la base de datos?"
  NLS_ID_GUI_CONTROLS(115)   "Se han detectado nuevas funcionalidades; por favor, contacte con el administrador."
  NLS_ID_GUI_CONTROLS(116)   "Se ha producido un error obteniendo las funcionalidades."
  NLS_ID_GUI_CONTROLS(117)   "Se ha producido un error cr�tico. La aplicaci�n se cerrar� inmediatamente."
  NLS_ID_GUI_CONTROLS(118)   "Se ha producido una excepci�n. La aplicaci�n se cerrar� inmediatamente."
  NLS_ID_GUI_CONTROLS(119)   "No se han encontrado datos que cumplan las condiciones."
  NLS_ID_GUI_CONTROLS(120)   "Error iniciando la aplicaci�n %1; ya se est� ejecutando una instancia, no se puede iniciar otra."
  NLS_ID_GUI_CONTROLS(121)   "La clave debe cumplir las siguientes condiciones:\n    - Debe tener un m�nimo de 6 caracteres\n    - Debe tener al menos dos letras\n    - Debe tener al menos dos d�gitos\n    - No puede ser parte del nombre de usuario\n    - No puede contener el nombre de usuario"
  NLS_ID_GUI_CONTROLS(122)   "La b�squeda provocar� la p�rdida de los cambios, �desea continuar?"
  NLS_ID_GUI_CONTROLS(123)   "Se ha producido un error al acceder a la base de datos."
  NLS_ID_GUI_CONTROLS(124)   "Se ha producido un error al formatear los datos de una o m�s filas."
  NLS_ID_GUI_CONTROLS(125)   "Se ha producido un error al obtener el estado de la conexi�n con la agencia %1."
  NLS_ID_GUI_CONTROLS(126)   "Se ha producido un error y no se podr� establecer la sesi�n."
  NLS_ID_GUI_CONTROLS(127)   "ERROR CR�TICO: La aplicaci�n %1 tiene una versi�n incorrecta: %2."
  NLS_ID_GUI_CONTROLS(128)   "ERROR CR�TICO: Error leyendo la configuraci�n del Centro."
  NLS_ID_GUI_CONTROLS(129)   "ERROR CR�TICO: Error leyendo el componente NLS."
  NLS_ID_GUI_CONTROLS(130)   "ERROR CR�TICO: No es posible conectar con la Base de Datos."
  NLS_ID_GUI_CONTROLS(131)   "No ha sido posible el login del Usuario."
  NLS_ID_GUI_CONTROLS(132)   "Fallo enviando el mensaje de error: "
  NLS_ID_GUI_CONTROLS(133)   "Se ha producido un error actualizando las funcionalidades."
  NLS_ID_GUI_CONTROLS(134)   "Se ha producido un error al intentar conectar con la Base de Datos."
  NLS_ID_GUI_CONTROLS(135)   "Error en el intervalo de duraci�n: el filtro inicial debe ser menor que el final."
  NLS_ID_GUI_CONTROLS(136)   "Error en el intervalo de fechas: la fecha inicial debe ser menor o igual que la final."
  NLS_ID_GUI_CONTROLS(137)   "Se ha producido una excepci�n: \n%1"
  NLS_ID_GUI_CONTROLS(138)   "Se ha producido un error al acceder a la base de datos."
  NLS_ID_GUI_CONTROLS(139)   "Se puede ver el hist�rico de conexiones �nicamente para agencias de modo Online."
  NLS_ID_GUI_CONTROLS(140)   "Por favor seleccione un intervalo de tiempo. Como maximo se puede solicitar %1 d�as."
  NLS_ID_GUI_CONTROLS(141)   "Se ha producido un error al recuperar informaci�n de la agencia."
  NLS_ID_GUI_CONTROLS(142)   "Sin conexi�n con la base de datos."
  NLS_ID_GUI_CONTROLS(143)   "ERROR CR�TICO: Versi�n de base de datos incorrecta."
  NLS_ID_GUI_CONTROLS(144)   "Tiempo de espera caducado. El per�odo de tiempo de espera caduc� antes de completar la operaci�n o el servidor no responde."
  NLS_ID_GUI_CONTROLS(145)   "Excel no est� instalado en este equipo. Esta operaci�n requiere MS Excel para ser ejecutada."
  NLS_ID_GUI_CONTROLS(146)   "Se ha producido un error al exportar datos a Excel."
  NLS_ID_GUI_CONTROLS(147)   "Debe seleccionar una isla donde a�adir terminales."
  NLS_ID_GUI_CONTROLS(148)   "Error a�adiendo terminales a la isla %1."
  NLS_ID_GUI_CONTROLS(149)   "Los terminales se han a�adido correctamente a la isla %1."
  NLS_ID_GUI_CONTROLS(150)   "[Out Of Memory]\nPor favor, restrinja la b�squeda.\nNo se mostrar�n todos los datos."

  // RCI 17-JUN-2010: WCP Commands
  NLS_ID_GUI_CONTROLS(151)   "No se encontraron datos para el proveedor %1, terminal %2 en fecha %3."
  NLS_ID_GUI_CONTROLS(152)   "Se reiniciar�n los terminales seleccionados.\n�Desea continuar?"
  NLS_ID_GUI_CONTROLS(153)   "S�lo los comandos tipo Logger en estado Ok se pueden visualizar."

  NLS_ID_GUI_CONTROLS(154)   "M�ximo n�mero de intentos de inicio de sesi�n superado."

  // JMM 18-JAN-2012: Accounts movements xml report
  NLS_ID_GUI_CONTROLS(155)   "Se ha producido un error al generar el fichero XML."

  // MPO 20-APR-2012
  NLS_ID_GUI_CONTROLS(156)   "Los cambios realizados se perder�n.\n�Est� seguro que quiere cancelar?"

  // JAB 31-MAY-2012: Control number of displayed rows
  NLS_ID_GUI_CONTROLS(157)   "Se han obtenido %1 registros.\nPara mostrarlos se ha estimado un tiempo superior a %2 minutos, �desea visualizarlos?"

  // XCD 17-AUG-2012: User subscrib/unsubscrib
  NLS_ID_GUI_CONTROLS(158)   "El usuario %1 se va a dar de baja, �desea continuar?"
  NLS_ID_GUI_CONTROLS(159)   "El usuario %1 se va a dar de alta, �desea continuar?"


  // IDs from 201 to 450 are reserved for Labels
  // Form Name
  NLS_ID_GUI_CONTROLS(251)   "Selecci�n de Operadores, Distribuidores y Agencias"
  NLS_ID_GUI_CONTROLS(252)   "Gestor"
  NLS_ID_GUI_CONTROLS(253)   "Operador"
  NLS_ID_GUI_CONTROLS(254)   "Distribuidor"
  NLS_ID_GUI_CONTROLS(255)   "Agencia"
  NLS_ID_GUI_CONTROLS(256)   "---"
  NLS_ID_GUI_CONTROLS(257)   "Direcci�n"
  NLS_ID_GUI_CONTROLS(258)   "C�digo Postal"
  NLS_ID_GUI_CONTROLS(259)   "Terminal"
  // GUI user
  NLS_ID_GUI_CONTROLS(260)   "Usuario"
  NLS_ID_GUI_CONTROLS(261)   "Estado"
  NLS_ID_GUI_CONTROLS(262)   "Habilitadas"
  NLS_ID_GUI_CONTROLS(263)   "No Habilitadas"
  NLS_ID_GUI_CONTROLS(264)   "Resultados"
  NLS_ID_GUI_CONTROLS(265)   "Login de Usuario"
  NLS_ID_GUI_CONTROLS(266)   "Aplicaci�n"
  NLS_ID_GUI_CONTROLS(267)   "Clave"
  NLS_ID_GUI_CONTROLS(268)   "Cambio de Clave"
  NLS_ID_GUI_CONTROLS(269)   "Clave Actual"
  NLS_ID_GUI_CONTROLS(270)   "Nueva Clave"
  NLS_ID_GUI_CONTROLS(271)   "Confirmaci�n"
  NLS_ID_GUI_CONTROLS(272)   "WGC"
  NLS_ID_GUI_CONTROLS(273)   "Depende de"
  NLS_ID_GUI_CONTROLS(274)   "Selecci�n de Terminales"
  // 275-276: 'One' / 'All' (gender male)
  NLS_ID_GUI_CONTROLS(275)   "Uno"
  NLS_ID_GUI_CONTROLS(276)   "Todos"
  // 277-278: 'One' / 'All' (gender female)
  NLS_ID_GUI_CONTROLS(277)   "Una"
  NLS_ID_GUI_CONTROLS(278)   "Todas"
  // Generic Agency user
  NLS_ID_GUI_CONTROLS(279)   "Operario"
  NLS_ID_GUI_CONTROLS(280)   "Juego"
  NLS_ID_GUI_CONTROLS(281)   "Habilitado"
  NLS_ID_GUI_CONTROLS(282)   "Deshabilitado"
  NLS_ID_GUI_CONTROLS(283)   "S�"
  NLS_ID_GUI_CONTROLS(284)   "No"
  // Both: male
  NLS_ID_GUI_CONTROLS(285)   "Ambos"
  NLS_ID_GUI_CONTROLS(286)   "Ventas"
  // Both: female
  NLS_ID_GUI_CONTROLS(287)   "Ambas"
  NLS_ID_GUI_CONTROLS(288)   "Jerarqu�a"
  NLS_ID_GUI_CONTROLS(289)   "Comunicaciones"
  NLS_ID_GUI_CONTROLS(290)   "Financiero"
  // Several: male
  NLS_ID_GUI_CONTROLS(291)   "Varios"
  // Several: female
  NLS_ID_GUI_CONTROLS(292)   "Varias"
  NLS_ID_GUI_CONTROLS(293)   "Habilitados"
  NLS_ID_GUI_CONTROLS(294)   "Deshabilitados"
  NLS_ID_GUI_CONTROLS(295)   "Selecci�n de Operarios"
  NLS_ID_GUI_CONTROLS(296)   "Selecci�n de perfiles y usuarios del GUI"
  NLS_ID_GUI_CONTROLS(297)   "Estado de la Sesi�n"
  NLS_ID_GUI_CONTROLS(298)   "Intento"
  NLS_ID_GUI_CONTROLS(299)   "%1 de %2"
  NLS_ID_GUI_CONTROLS(300)   "Pendiente"
  NLS_ID_GUI_CONTROLS(301)   "Ejecuci�n"
  NLS_ID_GUI_CONTROLS(302)   "OK"
  NLS_ID_GUI_CONTROLS(303)   "Error"
  NLS_ID_GUI_CONTROLS(304)   "Tiempo Transcurrido"
  NLS_ID_GUI_CONTROLS(305)   "Sincronizaci�n con la Agencia"
  NLS_ID_GUI_CONTROLS(306)   "En curso"
  NLS_ID_GUI_CONTROLS(307)   "..."
  NLS_ID_GUI_CONTROLS(308)   "Comando"
  NLS_ID_GUI_CONTROLS(309)   "Finalizado"
  NLS_ID_GUI_CONTROLS(310)   "Fase"
  NLS_ID_GUI_CONTROLS(311)   "Nivel de Precio"
  NLS_ID_GUI_CONTROLS(312)   "Descripci�n"
  NLS_ID_GUI_CONTROLS(313)   "Importe"
  NLS_ID_GUI_CONTROLS(314)   "Progreso"
  NLS_ID_GUI_CONTROLS(315)   "Libros"
  NLS_ID_GUI_CONTROLS(316)   "Bloqueados"
  NLS_ID_GUI_CONTROLS(317)   "Descargados"
  NLS_ID_GUI_CONTROLS(318)   "Cargados"
  NLS_ID_GUI_CONTROLS(319)   "Cancelados"
  NLS_ID_GUI_CONTROLS(320)   "N�mero"
  NLS_ID_GUI_CONTROLS(321)   "Detalles"
  NLS_ID_GUI_CONTROLS(322)   "N�mero de mensaje"
  NLS_ID_GUI_CONTROLS(323)   "Nombre"
  NLS_ID_GUI_CONTROLS(324)   "Libros Bloqueados"
  NLS_ID_GUI_CONTROLS(325)   "Libros Enviados"
  NLS_ID_GUI_CONTROLS(326)   "Libros Recibidos"
  // Form Name
  NLS_ID_GUI_CONTROLS(327)   "Estado de la Conexi�n"
  NLS_ID_GUI_CONTROLS(328)   "Conectada"
  NLS_ID_GUI_CONTROLS(329)   "Desconectada"
  // Form Name
  NLS_ID_GUI_CONTROLS(330)   "Hist�rico de Conexiones"
  NLS_ID_GUI_CONTROLS(331)   "Hist�rico"
  NLS_ID_GUI_CONTROLS(332)   "Modo"
  NLS_ID_GUI_CONTROLS(333)   "Batch"
  NLS_ID_GUI_CONTROLS(334)   "On-line"
  NLS_ID_GUI_CONTROLS(335)   "Estado"
  NLS_ID_GUI_CONTROLS(336)   "OK"
  NLS_ID_GUI_CONTROLS(337)   "Error"
  NLS_ID_GUI_CONTROLS(338)   "Fecha"
  NLS_ID_GUI_CONTROLS(339)   "Desde"
  NLS_ID_GUI_CONTROLS(340)   "Hasta"
  NLS_ID_GUI_CONTROLS(341)   "�ltima Actualizaci�n"
  // Form Name
  NLS_ID_GUI_CONTROLS(342)   "Estado Agencia"
  NLS_ID_GUI_CONTROLS(343)   "Muestra:\n- �ltima Fecha de Conexi�n cuando el estado es Conectada\n- �ltima Fecha de Desconexi�n cuando el estado es Desconectada"
  NLS_ID_GUI_CONTROLS(344)   "Agencia"
  NLS_ID_GUI_CONTROLS(345)   "Duraci�n"
  NLS_ID_GUI_CONTROLS(346)   "Se ha producido un error al formatear los datos de una o m�s filas."
  NLS_ID_GUI_CONTROLS(347)   "Loc. 1"
  NLS_ID_GUI_CONTROLS(348)   "Loc. 2"
  NLS_ID_GUI_CONTROLS(349)   "Loc. 3"
  NLS_ID_GUI_CONTROLS(350)   "Duraci�n Conexi�n"
  NLS_ID_GUI_CONTROLS(351)   "(hh:mm)"
  NLS_ID_GUI_CONTROLS(352)   "Duraci�n Desconexi�n"
  NLS_ID_GUI_CONTROLS(353)   "Tiempo transcurrido desde la previa\ndesconexi�n hasta conexi�n"
  NLS_ID_GUI_CONTROLS(354)   "Sesi�n"
  NLS_ID_GUI_CONTROLS(355)   "Fin"
  NLS_ID_GUI_CONTROLS(356)   "Aviso"
  NLS_ID_GUI_CONTROLS(357)   "Comienzo"
  NLS_ID_GUI_CONTROLS(358)   "Comienzo/Fin"
  NLS_ID_GUI_CONTROLS(359)   "Informaci�n de Contacto"
  NLS_ID_GUI_CONTROLS(360)   "Tel�fono %1"
  NLS_ID_GUI_CONTROLS(361)   "NIF"
  NLS_ID_GUI_CONTROLS(362)   "Estado Com. Agencia"
  NLS_ID_GUI_CONTROLS(363)   "Origen"
  NLS_ID_GUI_CONTROLS(364)   "Dispositivo"
  NLS_ID_GUI_CONTROLS(365)   "Servidor Ag."
  NLS_ID_GUI_CONTROLS(366)   "Kiosco %1"
  NLS_ID_GUI_CONTROLS(367)   "No Activo"
  NLS_ID_GUI_CONTROLS(368)   "Conexi�n"
  NLS_ID_GUI_CONTROLS(369)   "Agencias debajo de"
  NLS_ID_GUI_CONTROLS(370)   "Tiempo Hasta Pr�xima Ejecuci�n"
  NLS_ID_GUI_CONTROLS(371)   "Conectando con la Base de datos..."
  NLS_ID_GUI_CONTROLS(372)   "Configuraci�n de la Base de datos"
  NLS_ID_GUI_CONTROLS(373)   "Primario"
  NLS_ID_GUI_CONTROLS(374)   "Secundario"
  NLS_ID_GUI_CONTROLS(375)   "Id. de Base de Datos"
  NLS_ID_GUI_CONTROLS(376)   "Hora de cierre"
  NLS_ID_GUI_CONTROLS(377)   "Generado"
  NLS_ID_GUI_CONTROLS(378)   "Generado por"

  // RCI 17-JUN-2010: WCP Commands
//  NLS_ID_GUI_CONTROLS(401)   "Fecha"  // CreatedDate   Use CONTROLS(338) instead

  // Status Values
  NLS_ID_GUI_CONTROLS(402)   "Pendiente Env�o"
  NLS_ID_GUI_CONTROLS(403)   "Pendiente Respuesta"
  NLS_ID_GUI_CONTROLS(404)   "Ok"
  NLS_ID_GUI_CONTROLS(405)   "Error"
  NLS_ID_GUI_CONTROLS(406)   "Desconectado (Timeout)"
  NLS_ID_GUI_CONTROLS(407)   "Sin respuesta (Timeout)"
  NLS_ID_GUI_CONTROLS(408)   "Desconocido"

  // Command Values
  NLS_ID_GUI_CONTROLS(415)   "Logger"
  NLS_ID_GUI_CONTROLS(416)   "Restart"

  // Form Title
  NLS_ID_GUI_CONTROLS(421)   "Env�o de Comandos"
  NLS_ID_GUI_CONTROLS(422)   "Hist�rico de Comandos"
  NLS_ID_GUI_CONTROLS(423)   "Log Viewer"
  NLS_ID_GUI_CONTROLS(424)   "Logout de Usuario"

  // RCI 20-10-2011: frm_area_island_sel.vb
  NLS_ID_GUI_CONTROLS(425)   "�reas e Islas" // Form Title && Permission && Audit
  NLS_ID_GUI_CONTROLS(426)   "�rea"
  NLS_ID_GUI_CONTROLS(427)   "Isla"
  NLS_ID_GUI_CONTROLS(428)   "Fumadores"
  NLS_ID_GUI_CONTROLS(429)   "�reas"
  NLS_ID_GUI_CONTROLS(430)   "Islas"
  NLS_ID_GUI_CONTROLS(431)   "Islas del �rea %1"
  NLS_ID_GUI_CONTROLS(432)   "(F)"
  NLS_ID_GUI_CONTROLS(433)   "(NF)"
  NLS_ID_GUI_CONTROLS(434)   "del �rea"
  NLS_ID_GUI_CONTROLS(435)   "Terminales"
  NLS_ID_GUI_CONTROLS(436)   "Terminales de la isla %1"
  NLS_ID_GUI_CONTROLS(437)   "Floor ID"
  NLS_ID_GUI_CONTROLS(438)   "de la isla"
  // JGR 20-02-2012: frm_area_bank_sel.vb
  NLS_ID_GUI_CONTROLS(439)   "Zona"
  NLS_ID_GUI_CONTROLS(440)   "No Fumadores"

  // MPO 27-MAR-2012
  NLS_ID_GUI_CONTROLS(441)   "en"
  NLS_ID_GUI_CONTROLS(442)   "segundos"
  NLS_ID_GUI_CONTROLS(443)   "Cancelar"

  // IDs from 451 to 499 are reserved for Menu Items

  // RCI 17-JUN-2010: WCP Commands
  NLS_ID_GUI_CONTROLS(451)   "Comandos"
  NLS_ID_GUI_CONTROLS(452)   "Env�o"
  NLS_ID_GUI_CONTROLS(453)   "Hist�rico"

  NLS_ID_GUI_CONTROLS(454)   "�reas e Islas" // Menu Item

END
