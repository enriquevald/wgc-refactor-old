// ------------------------------------------------------------------------------------
// Copyright � 2008 Win Systems Ltd.
// ------------------------------------------------------------------------------------
//
//   MODULE NAME : NLS_009_GUISwDownload.rc
//
//   DESCRIPTION : NLS resources for GUI Software Download.
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ -------------------------------------------------------------------
// 05-SEP-2008 TJG    Initial release
// --------------------------------------------------------------------------------------
//
// WARNING !!!
//
// Do NOT modify this file with Visual Studio Resource Editor.
// This file MUST be edited using a TEXT editor.
//
// WARNING !!!

STRINGTABLE DISCARDABLE
BEGIN

  // IDs from 1 to 25 are reserved for buttons
  // IDs from 26 to 100 are reserved for application specific requirements
  // IDs from 101 to 200 are reserved for messages
  // IDs from 201 to 450 are reserved for Labels
  // IDs from 451 to 499 are reserved for Menu Items

  // Buttons: 1-25
  NLS_ID_GUI_SW_DOWNLOAD(1)     "Download"
  NLS_ID_GUI_SW_DOWNLOAD(2)     "Exit"
  NLS_ID_GUI_SW_DOWNLOAD(3)     "New"
  NLS_ID_GUI_SW_DOWNLOAD(4)     "Delete"

  // IDs from 26 to 100 are reserved for application specific requirements

  // IDs from 101 to 200 are reserved for messages
  NLS_ID_GUI_SW_DOWNLOAD(101)   "Error in dates interval: Initial date must be earlier than the final date."
  NLS_ID_GUI_SW_DOWNLOAD(102)   "Unexpected client identifier: System only accepts software packages for client %1; id reported from package is %2."
  NLS_ID_GUI_SW_DOWNLOAD(103)   "A package in the database already exists for Client %1, Build %2 and Terminal Type %3."
  NLS_ID_GUI_SW_DOWNLOAD(104)   "Are you sure you want to delete package %1?"
  NLS_ID_GUI_SW_DOWNLOAD(105)   "Package %1 could only be deleted from %2 repository(s)."
  NLS_ID_GUI_SW_DOWNLOAD(106)   "Package %1 could only be copied to %2 repository(s) out of %3."
  NLS_ID_GUI_SW_DOWNLOAD(107)   "Package %1 was copied to all the repositories (%2)."
  NLS_ID_GUI_SW_DOWNLOAD(108)   "Package %1 could not be copied to any of the defined repositories (%2)."
  NLS_ID_GUI_SW_DOWNLOAD(109)   "No addresses have been defined for the software repository."
  NLS_ID_GUI_SW_DOWNLOAD(110)   "Package %1 was successfully deleted."
  NLS_ID_GUI_SW_DOWNLOAD(111)   "Could not retrieve version information from package file %1."
  NLS_ID_GUI_SW_DOWNLOAD(112)   "Could not retrieve software repository information from database."
  NLS_ID_GUI_SW_DOWNLOAD(113)   "Package file %1 has reported an incorrect type (%2)."
  NLS_ID_GUI_SW_DOWNLOAD(114)   "System accepts FULL packages only."
  NLS_ID_GUI_SW_DOWNLOAD(115)   "Could not retrieve modules information for package file %1."
  NLS_ID_GUI_SW_DOWNLOAD(116)   "A new software version for GUI is available. Do you want to install it now?"
  NLS_ID_GUI_SW_DOWNLOAD(117)   "Restart application to update..."
  NLS_ID_GUI_SW_DOWNLOAD(118)   "Package %1 could not be deleted from any of the defined repositories (%2)."
  NLS_ID_GUI_SW_DOWNLOAD(119)   ""
  NLS_ID_GUI_SW_DOWNLOAD(120)   "Are you sure you want to remove the license with expiration date %1?"
  NLS_ID_GUI_SW_DOWNLOAD(121)   "Error reading data related to Software Package=%1"
  NLS_ID_GUI_SW_DOWNLOAD(122)   "Error reading licence file %1."
  NLS_ID_GUI_SW_DOWNLOAD(123)   "Error retrieving licence information."
  NLS_ID_GUI_SW_DOWNLOAD(124)   "Licence has been saved successfully."
  NLS_ID_GUI_SW_DOWNLOAD(125)   "Are you sure you want to delete licence with expiration date %1?"

  // IDs from 201 to 450 are reserved for Labels
  // Form Title
  NLS_ID_GUI_SW_DOWNLOAD(201)   "Software versions"
  NLS_ID_GUI_SW_DOWNLOAD(202)   "Build"
  NLS_ID_GUI_SW_DOWNLOAD(203)   "Version %1."
  NLS_ID_GUI_SW_DOWNLOAD(204)   "Status"
  NLS_ID_GUI_SW_DOWNLOAD(205)   "Date"
  NLS_ID_GUI_SW_DOWNLOAD(206)   "From"
  NLS_ID_GUI_SW_DOWNLOAD(207)   "To"
  NLS_ID_GUI_SW_DOWNLOAD(208)   "Type"
  NLS_ID_GUI_SW_DOWNLOAD(209)   "All"
  NLS_ID_GUI_SW_DOWNLOAD(210)   "Win Kiosk"
  NLS_ID_GUI_SW_DOWNLOAD(211)   "Win Cashier"
  NLS_ID_GUI_SW_DOWNLOAD(212)   "Wigos GUI"
  // Form Title
  NLS_ID_GUI_SW_DOWNLOAD(213)   "Terminal Status"
  NLS_ID_GUI_SW_DOWNLOAD(214)   "Terminal Name"
  NLS_ID_GUI_SW_DOWNLOAD(215)   "Provider"
  NLS_ID_GUI_SW_DOWNLOAD(216)   "In Use"
  NLS_ID_GUI_SW_DOWNLOAD(217)   "Build"
  NLS_ID_GUI_SW_DOWNLOAD(218)   "Idle"
  NLS_ID_GUI_SW_DOWNLOAD(219)   "Update Status"
  NLS_ID_GUI_SW_DOWNLOAD(220)   "Up-to-date"
  NLS_ID_GUI_SW_DOWNLOAD(221)   "Outdated"
  NLS_ID_GUI_SW_DOWNLOAD(222)   "Latest Version"
  NLS_ID_GUI_SW_DOWNLOAD(223)   "Status"
  NLS_ID_GUI_SW_DOWNLOAD(224)   "Not available"
  NLS_ID_GUI_SW_DOWNLOAD(225)   "Stopped"
  NLS_ID_GUI_SW_DOWNLOAD(226)   "WCP Service"
  NLS_ID_GUI_SW_DOWNLOAD(227)   "WC2 Service"
  NLS_ID_GUI_SW_DOWNLOAD(228)   "Latest Version"
  // Form Title
  NLS_ID_GUI_SW_DOWNLOAD(229)   "Licences"
  NLS_ID_GUI_SW_DOWNLOAD(230)   "Version"
  NLS_ID_GUI_SW_DOWNLOAD(231)   "Build"
  NLS_ID_GUI_SW_DOWNLOAD(232)   "From"
  NLS_ID_GUI_SW_DOWNLOAD(233)   "Type"
  NLS_ID_GUI_SW_DOWNLOAD(234)   "Inserted"
  NLS_ID_GUI_SW_DOWNLOAD(235)   "Package "
  NLS_ID_GUI_SW_DOWNLOAD(236)   "Client"
  NLS_ID_GUI_SW_DOWNLOAD(237)   "Copy OK"
  NLS_ID_GUI_SW_DOWNLOAD(238)   "Copy Not OK"
  // Form Title
  NLS_ID_GUI_SW_DOWNLOAD(239)   "Version vontrol"
  NLS_ID_GUI_SW_DOWNLOAD(240)   "Application"
  NLS_ID_GUI_SW_DOWNLOAD(241)   "Services"
  NLS_ID_GUI_SW_DOWNLOAD(242)   "Wigos-AS GUI"
  //NLS_ID_GUI_SW_DOWNLOAD(243)   "WIN Cashier"
  NLS_ID_GUI_SW_DOWNLOAD(244)   "OS User"
  NLS_ID_GUI_SW_DOWNLOAD(245)   "App. User"
  NLS_ID_GUI_SW_DOWNLOAD(246)   "Computer"
  NLS_ID_GUI_SW_DOWNLOAD(247)   "Last Update"
  NLS_ID_GUI_SW_DOWNLOAD(248)   "IP Address"
  NLS_ID_GUI_SW_DOWNLOAD(249)   "Never"
  NLS_ID_GUI_SW_DOWNLOAD(250)   "Insertion"
  // Form Title
  NLS_ID_GUI_SW_DOWNLOAD(251)   "Licence Details"
  NLS_ID_GUI_SW_DOWNLOAD(252)   "Licence expiring on %1"
  NLS_ID_GUI_SW_DOWNLOAD(253)   "Installation Date:"
  NLS_ID_GUI_SW_DOWNLOAD(254)   "Expiration Date:"
  NLS_ID_GUI_SW_DOWNLOAD(255)   "Valid To (Inclusive)"

  // Form Title
  NLS_ID_GUI_SW_DOWNLOAD(300)   "Software Package Details"
  NLS_ID_GUI_SW_DOWNLOAD(301)   "Version %1"
  NLS_ID_GUI_SW_DOWNLOAD(302)   "Repository Address (%1)"
  NLS_ID_GUI_SW_DOWNLOAD(303)   "Inserted"
  NLS_ID_GUI_SW_DOWNLOAD(304)   "Type"
  NLS_ID_GUI_SW_DOWNLOAD(305)   "Target"
  NLS_ID_GUI_SW_DOWNLOAD(306)   "Full"
  NLS_ID_GUI_SW_DOWNLOAD(307)   "Update"
  NLS_ID_GUI_SW_DOWNLOAD(308)   ""
  NLS_ID_GUI_SW_DOWNLOAD(309)   "Package"
  NLS_ID_GUI_SW_DOWNLOAD(310)   "Module"
  NLS_ID_GUI_SW_DOWNLOAD(311)   "Version"
  NLS_ID_GUI_SW_DOWNLOAD(312)   "Size (Bytes)"
  // IDs 313-320 reserved for package status
  NLS_ID_GUI_SW_DOWNLOAD(313)   "OK"
  NLS_ID_GUI_SW_DOWNLOAD(314)   "Error"
  NLS_ID_GUI_SW_DOWNLOAD(315)   ""
  NLS_ID_GUI_SW_DOWNLOAD(316)   ""
  NLS_ID_GUI_SW_DOWNLOAD(317)   ""
  NLS_ID_GUI_SW_DOWNLOAD(318)   ""
  NLS_ID_GUI_SW_DOWNLOAD(319)   ""
  NLS_ID_GUI_SW_DOWNLOAD(320)   ""

  NLS_ID_GUI_SW_DOWNLOAD(321)   "SAS Host Kiosk"
  NLS_ID_GUI_SW_DOWNLOAD(322)   "Type"
  NLS_ID_GUI_SW_DOWNLOAD(323)   "Kiosk"

  NLS_ID_GUI_SW_DOWNLOAD(324)   "Last update"
  NLS_ID_GUI_SW_DOWNLOAD(325)   "Connected"
  NLS_ID_GUI_SW_DOWNLOAD(326)   "Disconnected"
  NLS_ID_GUI_SW_DOWNLOAD(327)   "Type"
  NLS_ID_GUI_SW_DOWNLOAD(328)   "Site Jackpot"
  NLS_ID_GUI_SW_DOWNLOAD(329)   "Mobile Bank"
  NLS_ID_GUI_SW_DOWNLOAD(330)   "iMB"
  NLS_ID_GUI_SW_DOWNLOAD(331)   "iStats"
  NLS_ID_GUI_SW_DOWNLOAD(332)   "Type: %1"
  NLS_ID_GUI_SW_DOWNLOAD(333)   "WWP Site Service"
  NLS_ID_GUI_SW_DOWNLOAD(334)   "WWP Center Service"
  NLS_ID_GUI_SW_DOWNLOAD(335)   "WXP Service"
  NLS_ID_GUI_SW_DOWNLOAD(336)   "PromoBOX"

  // IDs from 451 to 499 are reserved for Menu Items
  NLS_ID_GUI_SW_DOWNLOAD(451)   "SW Download"
  NLS_ID_GUI_SW_DOWNLOAD(452)   "Software versions"
  NLS_ID_GUI_SW_DOWNLOAD(453)   "Terminal Status"
  NLS_ID_GUI_SW_DOWNLOAD(454)   "Version control"
  NLS_ID_GUI_SW_DOWNLOAD(455)   "Licences"

  NLS_ID_GUI_SW_DOWNLOAD(456)   "WAS Service"
  NLS_ID_GUI_SW_DOWNLOAD(457)   "WS.PSA_Service"

END
