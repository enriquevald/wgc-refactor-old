// ------------------------------------------------------------------------------------
// Copyright © 2004 Win Systems Ltd.
// ------------------------------------------------------------------------------------
//
//   MODULE NAME: NLS_009_GUIVersionControl.rc
//
//   DESCRIPTION: NLS resource LKC GUI Version Control.
//
//        AUTHOR: Ronald Rodríguez T.
//
// CREATION DATE: 08-MAR-2004
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ -------------------------------------------------------------------
// 08-MAR-2004 RRT    Initial release
// --------------------------------------------------------------------------------------

// WARNING !!!
//
// Do NOT modify this file with Visual Studio Resource Editor.
// This file MUST be edited using a TEXT editor.
//
// WARNING !!!

STRINGTABLE DISCARDABLE
BEGIN

  // IDs from 1 to 50 are reserved for buttons
  // IDs from 51 to 100 are reserved for application specific requirements
  // IDs from 101 to 200 are reserved for messages
  // IDs from 201 to 450 are reserved for Labels
  // IDs from 451 to 499 are reserved for Menu Items

    // IDs from 1 to 50 are reserved for buttons
    NLS_ID_GUI_VERSION_CONTROL(1)     "Select"
    NLS_ID_GUI_VERSION_CONTROL(2)     "DL"
    NLS_ID_GUI_VERSION_CONTROL(3)     "Reread"
    NLS_ID_GUI_VERSION_CONTROL(4)     "Generate"
    // Browse directories to replace the current one
    NLS_ID_GUI_VERSION_CONTROL(5)     "Change"
    NLS_ID_GUI_VERSION_CONTROL(6)     "Exit"


  // IDs from 101 to 200 are reserved for messages
    NLS_ID_GUI_VERSION_CONTROL(101)   "The maximum number of components (%1) has been reached."
    NLS_ID_GUI_VERSION_CONTROL(102)   "Components with the same name cannot exist."
    NLS_ID_GUI_VERSION_CONTROL(103)   "You must input the name of the module."
    NLS_ID_GUI_VERSION_CONTROL(104)   "The module must have at least one component to be created."
    NLS_ID_GUI_VERSION_CONTROL(105)   "You must input the name of the DL to generate."
    NLS_ID_GUI_VERSION_CONTROL(106)   "Are you sure you want to exit?"
    NLS_ID_GUI_VERSION_CONTROL(107)   "You must input a component name."
    NLS_ID_GUI_VERSION_CONTROL(108)   "The maximum number of modules (%1) has been reached."
    NLS_ID_GUI_VERSION_CONTROL(109)   "Are you sure you want to delete the module %1?"
    NLS_ID_GUI_VERSION_CONTROL(110)   "The module %1 already exists."
    NLS_ID_GUI_VERSION_CONTROL(111)   "Configuration file for the module %1 cannot be found."
    NLS_ID_GUI_VERSION_CONTROL(112)   "Component %1 cannot be found."
    NLS_ID_GUI_VERSION_CONTROL(113)   "Component %1 doesn't have a version."
    NLS_ID_GUI_VERSION_CONTROL(114)   "Component %1 has an incorrect version."
    NLS_ID_GUI_VERSION_CONTROL(115)   "Error creating the Distribution List."
    NLS_ID_GUI_VERSION_CONTROL(116)   "All the components must have the same Client Id. or the Universal Client Id. (00)."
    NLS_ID_GUI_VERSION_CONTROL(117)   "The distribution List build must be defined."
    NLS_ID_GUI_VERSION_CONTROL(118)   "The distribution List has been generated successfully."
    NLS_ID_GUI_VERSION_CONTROL(119)   "Distribution List %1 could not be found."
    NLS_ID_GUI_VERSION_CONTROL(120)   "Changes have been detected in one or more components."
    NLS_ID_GUI_VERSION_CONTROL(121)   "You must input a database common build."
    NLS_ID_GUI_VERSION_CONTROL(122)   "You must input a database client build."
    NLS_ID_GUI_VERSION_CONTROL(123)   "You must input a client identifier."


  // IDs from 201 to 450 are reserved for Labels
    NLS_ID_GUI_VERSION_CONTROL(201)   "Version Control Generator"
    NLS_ID_GUI_VERSION_CONTROL(202)   "Module Selection"
    NLS_ID_GUI_VERSION_CONTROL(203)   "Module Edition"
    NLS_ID_GUI_VERSION_CONTROL(204)   "DL Generation"
    NLS_ID_GUI_VERSION_CONTROL(205)   "Module"
    NLS_ID_GUI_VERSION_CONTROL(206)   "Component"
    NLS_ID_GUI_VERSION_CONTROL(207)   "Common DB"
    NLS_ID_GUI_VERSION_CONTROL(208)   "Directory"
    NLS_ID_GUI_VERSION_CONTROL(209)   "Distribution List"
    NLS_ID_GUI_VERSION_CONTROL(210)   "Version"
    NLS_ID_GUI_VERSION_CONTROL(211)   "Build"
    NLS_ID_GUI_VERSION_CONTROL(212)   "Client"
    NLS_ID_GUI_VERSION_CONTROL(213)   "Distribution List Selection"
    NLS_ID_GUI_VERSION_CONTROL(214)   "Versions Viewer"
    NLS_ID_GUI_VERSION_CONTROL(215)   "Client DB"
    NLS_ID_GUI_VERSION_CONTROL(216)   "Expected"
    NLS_ID_GUI_VERSION_CONTROL(217)   "Found"


  // IDs from 451 to 499 are reserved for Menu Items

END
