// ------------------------------------------------------------------------------------
// Copyright © 2004 Win Systems Ltd.
// ------------------------------------------------------------------------------------
//
//   MODULE NAME: NLS_009_Services.rc
//
//   DESCRIPTION: NLS resource LKC Services.
//
//        AUTHOR: Ronald Rodríguez T.
//
// CREATION DATE: 09-MAR-2004
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ -------------------------------------------------------------------
// 09-MAR-2004 RRT    Initial release
// --------------------------------------------------------------------------------------

// WARNING !!!
//
// Do NOT modify this file with Visual Studio Resource Editor.
// This file MUST be edited using a TEXT editor.
//
// WARNING !!!

STRINGTABLE DISCARDABLE
BEGIN

  // IDs from 1 to 100 are reserved for messages sent to Logger
  // IDs from 101 to 200 are reserved for alarms
  // IDs from 201 to 300 are reserved for help messages
  // IDs from 301 to 400 are reserved for console messages
  // IDs from 401 to 499 are reserved for future use

  // Logger Messages: 1-100
  NLS_ID_SERVICES(1)              "PSAC %1 %2"
  // %1: Service Name
  NLS_ID_SERVICES(2)              "%1 Started."
  // %1: Service Name
  NLS_ID_SERVICES(3)              "%1 Stopped."
  // %1: Service Name; %2: Computer Name
  NLS_ID_SERVICES(4)              " *** CRITICAL ERROR *** Exception Code: %1. Address: %2."

  // Alarms: 101-200
  // %1: Service Name; %2: Computer Name
  NLS_ID_SERVICES(101)            "%1 Started in %2."
  // %1: Service Name; %2: Computer Name
  NLS_ID_SERVICES(102)            "%1 Stopped in %2."
  // %1: Service Name; %2: Computer Name; %3: Exception code; %4: Address
  NLS_ID_SERVICES(103)            " *** CRITICAL ERROR *** Service: %1 in %2. Exception Code: %3. Address: %4."

  // Help Messages: 201-300
  NLS_ID_SERVICES(201)            "** HELP %1 **"
  NLS_ID_SERVICES(202)            "%1 %2%3 [User [Password]]  , to install the service."
  NLS_ID_SERVICES(203)            "%1 %2%3                    , to uninstall the service."
  NLS_ID_SERVICES(204)            "%1 %2%3                    , to start it in console mode."
  NLS_ID_SERVICES(205)            "%1 %2%3                    , to display this help screen."

  // Console Messages: 301-400
  NLS_ID_SERVICES(301)            "%1 has been successfully installed."
  NLS_ID_SERVICES(302)            "%1 was already installed; uninstall it before installing  a new version."
  NLS_ID_SERVICES(303)            "%1 could not be installed."
  NLS_ID_SERVICES(304)            "%1 has been un installed successfully."
  NLS_ID_SERVICES(305)            "%1 is not installed."
  NLS_ID_SERVICES(306)            "%1 could not be un installed."
  NLS_ID_SERVICES(307)            "Starting %1 in console mode."
  NLS_ID_SERVICES(308)            "Stopping %1."
  NLS_ID_SERVICES(309)            "Execution Directory: %s"
  NLS_ID_SERVICES(310)            ""
  NLS_ID_SERVICES(311)            ""
  NLS_ID_SERVICES(312)            ""
  NLS_ID_SERVICES(313)            "%1 could not be stopped."
  NLS_ID_SERVICES(314)            "%1 stopped."

END
