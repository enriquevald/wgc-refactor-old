//------------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VersionControl.h
// 
//   DESCRIPTION: Constants, types, variables definitions and prototypes 
//                 for VersionControl
//
//                 This module is SHARED in LKC, LKAS and LKT solutions.
//
//                 VERY IMPORTANT !!!
//                 Due that this is a module is shared (LKC, LKAS & LKT), 
//                 ANY change must be checked twice by compiling LKC, LKAS
//                 and LKT DLLs
// 
//        AUTHOR: Carles Iglesias
// 
// CREATION DATE: 04-JUN-2002
// 
// REVISION HISTORY
// 
// Date        Author Description 
// ----------- ------ -----------------------------------------------------------
// 04-JUN-2002 CIR    Initial draft.
// 01-AUG-2002 ACC    Module is shared.
// 
//------------------------------------------------------------------------------
#ifndef __VERSIONCONTROL_H
#define __VERSIONCONTROL_H

//------------------------------------------------------------------------------
//  INCLUDES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PUBLIC CONSTANTS 
//------------------------------------------------------------------------------
#define CLIENT_ID_COMMON                        0
#define MAX_CLIENT_ID                           99
#define MAX_BUILD_ID                            999

// Return codes
#define VERSION_STATUS_OK                       0
#define VERSION_STATUS_ERROR                    1
#define VERSION_STATUS_DL_NOT_FOUND             2
#define VERSION_STATUS_COMPONENT_NOT_FOUND      3
#define VERSION_STATUS_VERSION_ERROR            4
#define VERSION_STATUS_DB                       5

// Module constants
#define VERSION_LENGTH_MODULE_NAME              40
#define VERSION_LENGTH_COMPONENT_NAME           40
#define VERSION_LENGTH_COMPANY                  128
#define VERSION_LENGTH_COPYRIGHT                128
#define VERSION_LENGTH_DESCRIPTION              30
#define VERSION_LENGTH_VERSION                  16  // CC.BBB.ODB.CDB (Client, Build, Common DB, Client DB)
#define VERSION_MAX_COMPONENTS                  100

// Encryption constants
#define VERSION_DES_ENCRYPT                     1
#define VERSION_DES_DECRYPT                     0

#define VERSION_ENCRYPT_DES_SINGLE              0
#define VERSION_ENCRYPT_DES_TRIPLE              1
#define VERSION_ENCRYPT_KEY                     "hsgtear64uu7ds0pqjbba39f"  // 24 bytes key

// TYPE_DL_REGISTER constants
#define VERSION_SIZE_KEY_BLOB                   84
#define VERSION_SIZE_DIGITAL_SIGNATURE          64
#define VERSION_SIZE_FILLER_REG                 156

//------------------------------------------------------------------------------
// PUBLIC DATATYPES 
//------------------------------------------------------------------------------
typedef struct
{
  union
  {
    struct
    {
      DWORD   dl_file_date;
      BYTE    key_blob [VERSION_SIZE_KEY_BLOB];
      BYTE    digital_signature [VERSION_SIZE_DIGITAL_SIGNATURE];

    } header;

    struct
    {
      TCHAR   name [VERSION_LENGTH_COMPONENT_NAME + 1];
      DWORD   client;
      DWORD   build;
      DWORD   common_database;
      DWORD   client_database;
      BYTE    digital_signature [VERSION_SIZE_DIGITAL_SIGNATURE];

    } component;

    struct
    {
      BYTE    filler [VERSION_SIZE_FILLER_REG];
    } filler_register;

  } reg_data;
 
  DWORD   crc_32;

} TYPE_DL_REGISTER;

typedef struct
{
  DWORD         client;           // Client Id.
  DWORD         build;            // Build Id.
  DWORD         common_database;  // Base Software (Common) DB.
  DWORD         client_database;  // Client Specific Software DB.
  TCHAR         version [VERSION_LENGTH_VERSION + 1];

} TYPE_VERSION_BASE;

typedef struct
{
  TCHAR                     name [VERSION_LENGTH_COMPONENT_NAME + 1];
  TCHAR                     company_name [VERSION_LENGTH_COMPANY + 1];
  TCHAR                     copyright [VERSION_LENGTH_COPYRIGHT + 1];
  BOOL                      database_check;
  TYPE_VERSION_BASE         found;
  TYPE_VERSION_BASE         expected;

} TYPE_VERSION_COMPONENT;

typedef struct
{
  CONTROL_BLOCK             control_block;
  DWORD                     client_id;
  DWORD                     num_components;
  TYPE_VERSION_COMPONENT    component [VERSION_MAX_COMPONENTS];

} TYPE_VERSION_MODULE;

typedef struct
{
  CONTROL_BLOCK     control_block;
  DWORD             client_id;
  DWORD             build;
  DWORD             database_common_build;
  DWORD             database_client_build;
  TCHAR             version [VERSION_LENGTH_VERSION + 1 + 3];
  TCHAR             company_name [VERSION_LENGTH_COMPANY + 1 + 3];  
  TCHAR             copyright [VERSION_LENGTH_COPYRIGHT + 1 + 3];  

} TYPE_MODULE_ABOUT;

//------------------------------------------------------------------------------
//  PUBLIC DATA STRUCTURES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PUBLIC FUNCTION PROTOTYPES 
//------------------------------------------------------------------------------
// Encryption
BOOL        Version_Encrypt (BYTE   * pBuffIn, 
                             int    BuffInSize, 
                             BYTE   * pBuffOut, 
                             int    BuffOutSize,
                             BYTE   * pKey,    
                             int    Action,
                             int    DesType);

// Crc 32
DWORD       Version_Crc32_ComputeCRC (BYTE  * pBuffer, 
                                      DWORD BufferSize);

WORD        Version_GetModuleVersion (TCHAR                 * pModuleName, 
                                      TCHAR                 * pModulePath,
                                      TYPE_VERSION_MODULE   * pModuleVersion);

WORD        Version_GetComponentsVersions (TCHAR                 * pModuleName,
                                           TCHAR                 * pModulePath,
                                           TYPE_VERSION_MODULE   * pModuleVersion);

WORD        Version_CheckModuleVersion (TCHAR             * pModuleName, 
                                        TCHAR             * pModulePath,
                                        TYPE_MODULE_ABOUT * pModuleAbout);

WORD        Version_GetComponentVersion (TCHAR                  * pModulePath,
                                         TYPE_VERSION_COMPONENT * pComponentVersion);

void        Version_EncryptDecryptBuffer (unsigned char * pInBuffer,
                                          unsigned char * pOutBuffer,
                                          int           SizeOfBuffers);

#endif // __VERSIONCONTROL_H
