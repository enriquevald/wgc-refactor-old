﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WSI.Common;
using WSI.Layout;
using System.Data;

namespace AlarmSim
{
  public partial class alarmsim : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    private static String AddErrorToResult(String CurrentResult, Exception Ex)
    {
      var _result = "{\"result\":" + CurrentResult + ",";
      _result += "\"error\":{\"message\":" + Ex.Message + ",\"source\":" + Ex.Source + "}";
      _result += "}";
      return _result;
    }

    [WebMethod(), ScriptMethod()]
    public static String GetAlarms()
    {
      String _result = String.Empty;
      List<SqlParameter> _params = new List<SqlParameter>();

      try
      {

        using (DB_TRX _db_trx = new DB_TRX())
        {
          _params.Clear();
          _params.Add(new SqlParameter("@pRole", Convert.ToInt32(0)));
          _params.Add(new SqlParameter("@pUserId", Convert.ToInt32(1)));
          _result = clsDatabase.ExecuteQuery("Layout_GetRanges", _params.ToArray(), 3, _db_trx.SqlTransaction);
        }

      }
      catch (Exception _ex)
      {
        _result = AddErrorToResult(_result, _ex);
      }

      return _result;
    }

    [WebMethod(), ScriptMethod()]
    public static String GetTerminals()
    {
      String _result = String.Empty;

      try
      {

        using (DB_TRX _db_trx = new DB_TRX())
        {
          _result = clsDatabase.ExecuteQuery("Layout_GetTerminalsInfo", null, _db_trx.SqlTransaction);
        }

      }
      catch (Exception _ex)
      {
        _result = AddErrorToResult(_result, _ex);
      }

      return _result;
    }

    private static String SetTerminalStatusFlag(String TerminalId, String AlarmId, String Status, SqlTransaction Transaction)
    {
      String _result = "false";
      Int32 _affected = 0;

      using (SqlCommand _cmd = new SqlCommand("Layout_SetTerminalStatusFlag", Transaction.Connection, Transaction))
      {

        _cmd.CommandType = CommandType.StoredProcedure;

        // Params Adding
        _cmd.Parameters.Add("@pTerminalId", SqlDbType.BigInt).Value = TerminalId;
        _cmd.Parameters.Add("@pAlarmId", SqlDbType.BigInt).Value = AlarmId;
        _cmd.Parameters.Add("@pStatus", SqlDbType.BigInt).Value = Status;

        _affected = _cmd.ExecuteNonQuery();

        if (_affected > 0)
        {
          _result = "true";

          Transaction.Commit();
        }
      }

      return _result;
    }

    [WebMethod(), ScriptMethod()]
    public static String UpdateTerminalAlarm(String TerminalId, String AlarmId, String Status)
    {
      String _result = String.Empty;

      try
      {

        if (AlarmId != "0")
        {
          // Update terminal alarm
          using (DB_TRX _db_trx = new DB_TRX())
          {
            _result = SetTerminalStatusFlag(TerminalId, AlarmId, Status, _db_trx.SqlTransaction);
          }
        }
        else
        {
          // Reset all terminal alarms
        }

      }
      catch (Exception _ex)
      {
        AddErrorToResult(_result, _ex);
      }

      return _result;
    }

  }
}