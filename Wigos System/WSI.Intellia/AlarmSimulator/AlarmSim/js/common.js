﻿function AddLog(Message, isError) {
  var $logger = $("#textLog");
  $logger.append(new Date() + ((isError) ? " ERROR -> " : "") + Message + '\n');
}

// ----------------------------------------------------------------------------------------------------------------

function OnSendAlarmCompleted(Result) {
  if (Result != '') {
    switch (Result) {
      case "true":
        AddLog('Terminal Status modified!');
        break;
      case "false":
        AddLog('Cannot set Terminal Status!');
        break;
    }
  }
}

function OnSendAlarmError(Result) {
  alert(Result.message);
  AddLog(Result.message);
}

function SendAlarm(Status) {
  var _terminal = $("#selTerminal").val(),
      _alarm = $("#selAlarm").val();

  PageMethods.UpdateTerminalAlarm(_terminal, _alarm, Status, OnSendAlarmCompleted, OnSendAlarmError);
}

// ----------------------------------------------------------------------------------------------------------------

function FillAlarms(Rows) {
  var $select = $("#selAlarm"),
      _item = '';
      //_item = '<option value="0_0" data-type="0" data-sub-category="0" selected>Reset</option>';

  //$select.append(_item);

  for (_t in Rows) {
    _r = Rows[_t];
    _item = '';

    _item = _item + '<option value="' + _r[2] + '" data-type="' + _r[0] + '" data-sub-category="' + _r[2] + '">';
    switch (+_r[0]) {
      case 41:
        _item = _item + '[Player] ';
        break;
      case 42:
        _item = _item + '[Machine] ';
        break;
    }
    _item = _item + _r[1] + '</option>';

    $select.append(_item);
  }

}

function OnGetAlarmsComplete(Result) {
  if (Result != "") {
    var _data = JSON.parse(Result);

    if (_data) {

      if (!_data["error"]) {

        FillAlarms(_data.rows);
        AddLog("Alarms loaded.");

      } else {
        // Error
        alert(_data["error"].error.message);
        AddLog(_data["error"].error.message);
      }

    }
  }
}

function OnGetAlarmsError(Result) {
  alert(Result.message);
  AddLog(Result.message);
}

function LoadAlarms() {
  PageMethods.GetAlarms(OnGetAlarmsComplete, OnGetAlarmsError);
}

// ----------------------------------------------------------------------------------------------------------------

function FillTerminals(Rows) {
  var $select = $("#selTerminal"),
      _t = undefined,
      _r = undefined,
      _item = '<option value="" selected></option>';

  $select.append(_item);

  for (_t in Rows) {
    _r = Rows[_t];
    _item = '';
    _item = '<option value="' + _r[0] + '">' + '[' + _r[0] + '] ' + _r[3] + '</option>';

    $select.append(_item);
  }
}

function OnGetTerminalsComplete(Result) {
  if (Result != "") {
    var _data = JSON.parse(Result);

    if (_data) {

      if (!_data["error"]) {

        FillTerminals(_data.rows);
        AddLog("Terminals loaded.");

      } else {
        // Error
        alert(_data["error"].error.message);
        AddLog(_data["error"].error.message);
      }

    }
  }
}

function OnGetTerminalsError(Result) {
  alert(Result.message);
  AddLog(Result.message);
}

function LoadTerminals() {
  PageMethods.GetTerminals(OnGetTerminalsComplete, OnGetTerminalsError);
}