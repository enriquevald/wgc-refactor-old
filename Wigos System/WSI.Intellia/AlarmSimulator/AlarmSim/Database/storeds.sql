﻿/****** Object:  StoredProcedure [dbo].[Layout_SetTerminalStatusFlag]    Script Date: 09/17/2015 10:41:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_SetTerminalStatusFlag]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Layout_SetTerminalStatusFlag]
GO

/****** Object:  StoredProcedure [dbo].[Layout_SetTerminalStatusFlag]    Script Date: 09/17/2015 10:41:56 ******/
CREATE PROCEDURE [dbo].[Layout_SetTerminalStatusFlag]
      @pTerminalId INT
    , @pAlarmId INT
    , @pStatus INT -- 0 unset, 1 set, 2 reset all alarms
AS
BEGIN

  IF NOT EXISTS (SELECT * FROM TERMINAL_STATUS WHERE TS_TERMINAL_ID = @pTerminalId)
   BEGIN
    -- CREATE TERMINAL INTO TERMINAL_STATUS
    INSERT INTO TERMINAL_STATUS (TS_TERMINAL_ID) VALUES (@pTerminalId)
   END

  IF (@pStatus <> 2)
   BEGIN
   
    -- Update alarm status
   
    DECLARE @_alarm AS VARCHAR(5) = CAST(@pAlarmId AS VARCHAR)
    DECLARE @_col AS INT = SUBSTRING(@_alarm, 1, 2)
    DECLARE @_flag AS INT = SUBSTRING(@_alarm, 3, 3)
    DECLARE @_col_name AS VARCHAR(MAX) = ''
    DECLARE @_is_bit AS BIT = 0
    
    IF (@_col = 12)
     BEGIN
      SET @_col_name = 'TS_DOOR_FLAGS'
     END
    ELSE IF (@_col = 13)
     BEGIN
      SET @_col_name = 'TS_BILL_FLAGS'
     END
    ELSE IF (@_col = 14)
     BEGIN
      SET @_col_name = 'TS_PRINTER_FLAGS'
     END
    ELSE IF (@_col = 16)
     BEGIN
      SET @_col_name = 'TS_PLAYED_WON_FLAGS'
     END
    ELSE IF (@_col = 17)
     BEGIN
      SET @_col_name = 'TS_JACKPOT_FLAGS'
     END
    ELSE IF (@_col = 19)
     BEGIN
      SET @_col_name = 'TS_CALL_ATTENDANT_FLAGS'
      SET @_is_bit = 1
     END
    ELSE IF (@_col = 20)
     BEGIN
      IF (@_flag = 512) 
       BEGIN
        SET @_col_name = 'TS_MACHINE_FLAGS'
       END
      ELSE
       BEGIN
        SET @_col_name = 'TS_EGM_FLAGS'
       END
     END

    DECLARE @sql NVARCHAR(MAX) = ''
    
    SET @sql = @sql + 'UPDATE   TERMINAL_STATUS '
    SET @sql = @sql + '   SET   ' + @_col_name + ' = '
    
    IF (@_is_bit = 1)
     BEGIN
      SET @sql = @sql + CAST(@pStatus AS VARCHAR(50))
     END
    ELSE
     BEGIN 
      IF (@pStatus = 0)
       BEGIN
        SET @sql = @sql + @_col_name + ' & ~'+CAST(@_flag AS VARCHAR(50))+' '
       END
      ELSE IF (@pStatus = 1)
       BEGIN
        SET @sql = @sql + @_col_name + ' ^ '+CAST(@_flag AS VARCHAR(50))+' '
       END
     END

    SET @sql = @sql + ' WHERE   TS_TERMINAL_ID   = ' + CAST(@pTerminalId AS VARCHAR(50)) + ''
           
    --SELECT @_alarm, @_col, @_flag, @_col_name, @sql
           
    EXEC SP_EXECUTESQL @sql
  END
 ELSE
  BEGIN
   -- Reset all alarms 
   UPDATE   TERMINAL_STATUS
      SET   TS_EGM_FLAGS            = 0
          , TS_DOOR_FLAGS           = 0
          , TS_BILL_FLAGS           = 0
          , TS_PRINTER_FLAGS        = 0
          , TS_PLAYED_WON_FLAGS     = 0
          , TS_JACKPOT_FLAGS        = 0
          , TS_CALL_ATTENDANT_FLAGS = 0
          , TS_MACHINE_FLAGS        = 0
   WHERE  TS_TERMINAL_ID = @pTerminalId
    
  END

END

GO

