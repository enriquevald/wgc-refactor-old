﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="alarmsim.aspx.cs" Inherits="AlarmSim.alarmsim" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <title></title>
  <link rel="stylesheet" href="css/jquery-ui.css" />
  <link rel="stylesheet" href="css/alarmsim.css" />
  <link rel="stylesheet" href="css/estilo.css" />
  <script type="text/javascript" src="js/apis/jquery-1.11.2.min.js"></script>
  <script type="text/javascript" src="js/apis/jquery-ui.js"></script>
  <script type="text/javascript" src="js/common.js"></script>
</head>
<body class="rolPCA" style="background-image: url(img/winscube.jpg);">

  <div id="divControls" style="margin: 25px 25px 25px 25px; padding: 25px 25px 25px 25px;">

  <h1 class="headerTop rolPCA">Terminal alarm simulator</h1>
  <br />

  <div class="">
    <label for="selTerminal">Terminal</label>
    <select id="selTerminal"></select>
    
    <br />

    <label for="selAlarm">Alarma</label>
    <select id="selAlarm"></select>

    <br />

    <input id="btnActivate" class="ui-button" type="button" value="Activate" />
    <input id="btnDeactivate" class="ui-button" type="button" value="Deactivate" />
    <input id="btnReset" class="ui-button" type="button" value="Reset" />

  </div>

  <br />
  <hr />
  <br />

  <div class="">
    <textarea id="textLog" style="width: 100%; height: 400px; background-color: rgba(255,255,255,0.5);" readonly="readonly"></textarea>
  </div>

  </div>

  <form id="frmAlarmSim" runat="server">
    <div>
      <asp:ScriptManager ID="scrManager" runat="server" EnablePageMethods="true"></asp:ScriptManager>
    </div>
  </form>

  <script type="text/javascript">
    $(document).ready (function () {
      LoadTerminals();
      LoadAlarms();
      $("#btnActivate").click(function () { SendAlarm(1); });
      $("#btnDeactivate").click(function () { SendAlarm(0); });
      $("#btnReset").click(function () { SendAlarm(2); });
    });
  </script>

</body>
</html>
