﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.SessionState;
using WSI.Common;

namespace AlarmSim
{
  public class Global : System.Web.HttpApplication
  {

    protected void Application_Start(object sender, EventArgs e)
    {

      // Connection
      if (WGDB.ConnectionState != ConnectionState.Open)
      {
        String _principal = WebConfigurationManager.AppSettings["DBPrincipal"].ToString();
        String _mirror = WebConfigurationManager.AppSettings["DBMirror"].ToString();
        Int32 _id = Int32.Parse(WebConfigurationManager.AppSettings["DBId"].ToString());

        WGDB.Init(_id, _principal, _mirror);
        WGDB.SetApplication("AlarmsSim", "1.00");
        WGDB.ConnectAs("WGROOT");
      }

    }

    protected void Session_Start(object sender, EventArgs e)
    {

    }

    protected void Application_BeginRequest(object sender, EventArgs e)
    {

    }

    protected void Application_AuthenticateRequest(object sender, EventArgs e)
    {

    }

    protected void Application_Error(object sender, EventArgs e)
    {

    }

    protected void Session_End(object sender, EventArgs e)
    {

    }

    protected void Application_End(object sender, EventArgs e)
    {

    }
  }
}