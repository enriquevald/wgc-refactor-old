﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Admin.aspx.cs" Inherits="WSI.Layout.Admin.Admin" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <title>Smart Floor - Administration</title>
  <link rel="icon" type="image/png" href="images/logo.png" />
  <link rel="stylesheet" href="../design/css/jquery-ui.css" type="text/css" />
  <link rel="stylesheet" href="lib/jgraduate/css/jPicker.css" type="text/css" />
  <link rel="stylesheet" href="lib/jgraduate/css/jgraduate.css" type="text/css" />
  <script type="text/javascript" src="../design/js/jquery-1.11.2.min.js"></script>
  <script type="text/javascript" src="../design/js/jquery-ui.js"></script>

  <script type="text/javascript">
    $(function () {
      $("#menu").menu();
    });
  </script>

  <style type="text/css">
    .ui-menu {
      width: 150px;
    }

  </style>

</head>
<body>

<table style="width: 100%; height: 100%; border: none">
  <tr>
    <td style="width: 10%;">

      <ul id="menu">
        <li>General</li>
        <li>Dispositivos
          <ul>
            <li>Smartphones</li>
            <li>Beacons</li>
          </ul>
        </li>
        <li>Usuarios
        <ul>
          <li>Usuarios</li>
          <li data-nls="NLS_WIDGET_TITLE_RUNNERS">Asistente de Piso</li>
        </ul>
        </li>
        <li id="logs">Logs</li>
        <li id="resources">Recursos
          <ul>
            <li>Modelos y texturas</li>
          </ul>
        </li>
        <li id="flooredit">Editor</li>

      </ul>

      </td><td style="width: 90%;">

      <iframe id="targetFrame1" src="" style="position:fixed; top:0px; width: 85%; height: 100%; border: none">
      </iframe>

          </td>
  </tr>
</table>

  <form id="form1" runat="server">
    <div>
    </div>
  </form>

  <script type="text/javascript">

    $(document).ready(function () {
      $("#logs").click(function () {
        $("#targetFrame1").attr("src", "/log/logview.aspx");
      });
      $("#resources").click(function () {
        $("#targetFrame1").attr("src", "/models/modelsio.aspx");
      });
      $("#flooredit").click(function () {
        $("#targetFrame1").attr("src", "/editor/editor.aspx");
      });
    });

  </script>
</body>
</html>
