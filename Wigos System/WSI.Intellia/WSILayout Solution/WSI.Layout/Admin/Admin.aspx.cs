﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WSI.Layout.classes;

namespace WSI.Layout.Admin
{
  public partial class Admin : System.Web.UI.Page
  {

    //
    private void Page_PreLoad(object sender, EventArgs e)
    {
      // Check
      if (Global.Sessions.Information[Session.SessionID].User.Logged)
      {
        // Load Security
        Global.Sessions.Information[Session.SessionID].User.Permissions = new LayoutUserPermissions(Global.Sessions.Information[Session.SessionID].User.Id);

      }
      else
      {
        // Not logged
        Logger.Write(HttpContext.Current, "ADMIN_PRELOAD | Not logged in!");

        // Redirect to Login
        Response.Redirect("../Login.aspx?s=" + clsStrings.EncodeTo64("admin"));
      }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    private void InitializeComponent()
    {
      this.Load += new System.EventHandler(this.Admin_Load);

    }

    private void Admin_Load(object sender, EventArgs e)
    {
      
    }

  }
}