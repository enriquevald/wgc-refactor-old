﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using uPLibrary.Networking.M2Mqtt;
using WSI.Common;

namespace WSI.Layout
{
  public partial class App : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void UploadButton_Click(object sender, EventArgs e)
    {
      if (FileUploadControl.HasFile)
      {
        try
        {
          //string filename = Path.GetFileName(FileUploadControl.FileName);
          var content = FileUploadControl.FileBytes;
          var version = TxtVersion.Text;

          using (DB_TRX _transaction = new DB_TRX())
          {
            using (SqlCommand _command = new SqlCommand("Layout_UploadMobileApplicationVersion", _transaction.SqlTransaction.Connection, _transaction.SqlTransaction))
            {
              _command.CommandType = CommandType.StoredProcedure;
              _command.Parameters.Add("@pApk", SqlDbType.VarBinary).Value = FileUploadControl.FileBytes;
              _command.Parameters.Add("@pVersion", SqlDbType.Text).Value = version;
              _command.ExecuteNonQuery();
            }

            _transaction.Commit();
          }



          MqttClient client = new MqttClient(clsNetwork.CheckServiceAddress(ConfigurationManager.AppSettings.Get("BrokerAddress")));
          client.Connect(DateTime.Now.Ticks.ToString());

          string NOTIFICATION_TOPIC = String.Format("/NewVersion/{0}", version);
          client.Publish(NOTIFICATION_TOPIC, Encoding.UTF8.GetBytes(version));


          StatusLabel.Text = "Upload status: File uploaded!";
        }
        catch (Exception ex)
        {
          StatusLabel.Text = "Upload status: The file could not be uploaded. The following error occured: " + ex.Message;
        }
      }
    }
  }
}