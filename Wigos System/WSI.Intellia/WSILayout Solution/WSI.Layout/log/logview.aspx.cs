﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.IO;
using System.Data;
using System.Drawing;
using System.Text;
using System.Xml;
using WSI.Layout.classes;

namespace WSI.Layout.log
{
  public partial class logview : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!IsPostBack)
      {
        string[] filePaths = Directory.GetFiles(Server.MapPath("~/logfiles/"), "*.xml");
        List<ListItem> files = new List<ListItem>();
        foreach (string filePath in filePaths)
        {
          files.Add(new ListItem(Path.GetFileName(filePath), filePath));
        }
        GridView1.DataSource = files;
        GridView1.DataBind();
      }
    }

    //
    private void Page_PreLoad(object sender, EventArgs e)
    {
      // Check
      if (Global.Sessions.Information[Session.SessionID].User.Logged)
      {
        // Load Security
        Global.Sessions.Information[Session.SessionID].User.Permissions = new LayoutUserPermissions(Global.Sessions.Information[Session.SessionID].User.Id);

      }
      else
      {
        // Not logged
        Logger.Write(HttpContext.Current, "LOGVIEW_PRELOAD | Not logged in!");

        // Redirect to Login
        Response.Redirect("../Login.aspx?s=" + clsStrings.EncodeTo64("logview"));
      }
    }

    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {
      try
      {
        GridViewRow row = GridView1.SelectedRow;
        String _file = Server.MapPath("~/logfiles/" + row.Cells[3].Text);
        String _text = String.Empty;

        Label1.Text = "Current file: " + _file;

        using (StreamReader sr = new StreamReader(_file))
        {
          _text = sr.ReadToEnd();
        }
        _text = "<ROOT>" + _text + "</ROOT>";
        using (DataSet ds = new DataSet())
        {
          ds.ReadXml(new XmlTextReader(new StringReader(_text)));
          GridView2.DataSource = ds;
          GridView2.DataBind();
        }
      }
      catch (Exception _ex)
      {
        Logger.Exception(HttpContext.Current, "GridView1_SelectedIndexChanged()", _ex);
      }

    }

    protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
    {
      if (e.Row.RowType == DataControlRowType.DataRow)
      {
        switch (e.Row.Cells[4].Text)
        {
          case "ERR":
            e.Row.BackColor = Color.Cornsilk;
            e.Row.ForeColor = Color.Black;
            e.Row.Font.Bold = true;
            e.Row.Cells[4].BackColor = Color.Red;
            e.Row.Cells[4].ForeColor = Color.White;
            break;
          case "TS":
            e.Row.BackColor = Color.PowderBlue;
            e.Row.ForeColor = Color.Black;
            e.Row.Font.Italic = true;
            break;
          default:
            e.Row.ForeColor = Color.DarkGray;
            break;
        }
      }
    }
  }
}