﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Threading;
using System.Web.Security;

namespace WSI.Layout.classes
{

  // Contains information about a session
  public class LayoutSessionInformation
  {
    private String m_id = String.Empty;
    //private String m_mac_address = String.Empty;
    private String m_ip_address = String.Empty;
    private Boolean m_logged = false;
    private LayoutUser m_user = new LayoutUser();
    private Int32 m_current_section = Int32.MinValue;
    private String m_casino_name = String.Empty;
    private String m_dashboard = String.Empty;
    private String m_config = String.Empty;

    public String Dashboard
    {
      get { return m_dashboard; }
      set { m_dashboard = value; }
    }

    public String Configuration
    {
      get { return m_config; }
      set { m_config = value; }
    }
    // Session Id
    public String Id
    {
      get { return m_id; }
      set { m_id = value; }
    }

    //// MAC Address
    //public String MacAddress
    //{
    //  get { return m_mac_address; }
    //  set { m_mac_address = value; }
    //}

    // Client machine IP address
    public String IpAddress
    {
      get { return m_ip_address; }
      set { m_ip_address = value; }
    }

    // User information
    public LayoutUser User
    {
      get { return m_user; }
      set { m_user = value; }
    }

    // Logger state
    public Boolean Logged
    {
      get { return m_logged; }
      set { m_logged = value; }
    }

    // Last reported menu position - current section
    public Int32 CurrentSection
    {
      get { return m_current_section; }
      set { m_current_section = value; }
    }

    // Casino name from DB
    public String CasinoName
    {
      get { return m_casino_name; }
      set { m_casino_name = value; }
    }

    // Constructor
    public LayoutSessionInformation()
    {
    }

  }

  // Global object to mantain current sessions data
  public class LayoutSessions
  {
    
    private Int32 m_interval = 1000 * 60 * 5;
    private Int32 m_expiration_minutes = 9999;

    private delegate void ThreadWorker();
    private Thread m_thread;

    private Dictionary<String, LayoutSessionInformation> m_sessions_info = new Dictionary<String, LayoutSessionInformation>();
    private Dictionary<String, DateTime> m_sessions_expiration = new Dictionary<String, DateTime>();
    private static readonly object m_lock = new object();

    // Information thread protected
    public Dictionary<String, LayoutSessionInformation> Information
    {
      get
      {
        lock (m_lock)
        {
          return m_sessions_info;
        }
      }
      set
      {
        lock (m_lock)
        {
          m_sessions_info = value;
        }
      }
    }

    // Information thread protected
    public Dictionary<String, DateTime> Expirations
    {
      get
      {
        lock (m_lock)
        {
          return m_sessions_expiration;
        }
      }
      set
      {
        lock (m_lock)
        {
          m_sessions_expiration = value;
        }
      }
    }

    // Check if a session exists by session id
    public Boolean SessionExists(String SessionId)
    {
      return this.m_sessions_info.Keys.Contains(SessionId);
    }

    // Check for a user to be already logged in
    public Boolean UserAlreadyLogged(String UserName, String SessionId)
    {
      foreach (KeyValuePair<String, LayoutSessionInformation> _session in this.Information)
      {
        if (_session.Value.User.Nick == UserName && _session.Value.Id != SessionId)
        {
          return true;
        }
      }
      return false;
    }

    //
    private Double SessionIdle(String SessionId)
    {
      Double _result = Double.MaxValue;
      if (this.m_sessions_info.ContainsKey(SessionId))
      {
        _result = (DateTime.Now - this.m_sessions_expiration[SessionId]).TotalMinutes;
      }
      return (DateTime.Now - this.m_sessions_expiration[SessionId]).TotalMinutes;
    }

    //
    public void ExpireSession(String SessionId)
    {
      Logger.Write(HttpContext.Current, "Session '" + SessionId + "' expired!");
      
      this.m_sessions_info.Remove(SessionId);
    }

    //
    public void UpdateSessionExpiration(String SessionId)
    {
      DateTime _now = DateTime.Now;
      if (this.m_sessions_expiration.Keys.Contains(SessionId))
      {
        this.m_sessions_expiration[SessionId] = _now;
      }
      else
      {
        this.m_sessions_expiration.Add(SessionId, _now);
      }
      //
      Logger.Write(HttpContext.Current, "Session '" + SessionId + "' -> New expiration date : " + _now.ToString());
    }

    private void Initialize(ThreadWorker work)
    {
      m_thread = new Thread(new ThreadStart(work));
      m_thread.Name = "LayoutData_SessionsManager";
      m_thread.Start();
    }

    public void Init()
    {
      this.Initialize(this.Task);
    }

    private void Task()
    {
      Logger.Write(HttpContext.Current, "Thread starting:", "TS");
      Logger.Write(HttpContext.Current, " - Wait time : " + this.m_interval + "ms", "TS");
      Logger.Write(HttpContext.Current, " - Expiration time: " + this.m_expiration_minutes + " minutes", "TS");

      List<String> _expired_sessions = new List<String>();

      while (true)
      {
        Thread.Sleep(this.m_interval);

        try
        {
          _expired_sessions.Clear();

          // Look for expired sessions
          foreach (KeyValuePair<String, DateTime> _session in this.m_sessions_expiration)
          {
            // Check session expiration time
            if (this.SessionIdle(_session.Key) > this.m_expiration_minutes)
            {
              _expired_sessions.Add(_session.Key);
            }
          }

          // Clean expired sessions
          foreach (String _session_id in _expired_sessions)
          {
            // Expire the session
            this.ExpireSession(_session_id);
            this.m_sessions_expiration.Remove(_session_id);
          }
        }
        catch (Exception _ex)
        {
          Logger.Write(HttpContext.Current, "Thread exception:", "TS");
          Logger.Exception(HttpContext.Current, "[" + this.m_thread.Name + "]Task()", _ex);
        }

      }
    }

    //
    public void ReValidateSession(HttpContext Context, HttpRequest Request)
    {
      // Add Session Information
      if (!Global.Sessions.Information.ContainsKey(Context.Session.SessionID))
      {
        LayoutSessionInformation _new_session = new LayoutSessionInformation();

        _new_session.Id = Context.Session.SessionID;
        _new_session.IpAddress = Request.ServerVariables["REMOTE_ADDRESS"];

        Global.Sessions.Information.Add(Context.Session.SessionID, _new_session);
        //
        Global.Sessions.UpdateSessionExpiration(Context.Session.SessionID);
      }
      else
      {
        // Duplicated session id
        Logger.Write(HttpContext.Current, "Duplicated session!");
      }
    }

    // 
    public void SessionFinalize(HttpContext Context)
    {
      
      FormsAuthentication.SignOut();
      //Context.Session.Abandon();
      //Context.Session.Clear();
    }

    //
    public Boolean IsSessionLogged(HttpContext Context) {
      if (this.m_sessions_info.Keys.Contains(Context.Session.SessionID))
      {
        return this.m_sessions_info[Context.Session.SessionID].User.Logged;
      }
      else
      {
        return false;
      }
    }

    // Constructor
    public LayoutSessions()
    {
      Init();
    }

  }

}