﻿using System;
using System.Web;

namespace WSI.Layout.classes
{
  public class Meters
  {

    public static String GetSiteMeters()
    {
      try
      {
        Logger.Write(HttpContext.Current, "(Thread " + System.Threading.Thread.CurrentThread.ManagedThreadId + ")");

        return Global.LayoutMetersData.Data;
      }
      catch (Exception _ex)
      {
        Logger.Exception(HttpContext.Current, "GetSiteMeters()", _ex);
      }

      return String.Empty;
    }
  }
}