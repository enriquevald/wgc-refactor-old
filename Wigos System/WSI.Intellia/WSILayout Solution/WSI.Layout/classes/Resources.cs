﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using Newtonsoft.Json;
using WSI.Common;
using System.Data.SqlClient;
using WSI.Layout;
using WSI.Layout.classes;
using System.Data;

namespace WSI.Layout.classes
{
  public static class Resources
  {

    public static String SaveResources(String ResourceList)
    {
      String _cmd_str = String.Empty;
      String _result = String.Empty;

      using (DB_TRX _db_trx = new DB_TRX())
      {

        try
        {

          _cmd_str += "DECLARE @_ID AS INT \n";
          _cmd_str += "\n";

          dynamic _resource_list = JsonConvert.DeserializeObject(ResourceList);

          foreach (dynamic _resource in _resource_list)
          {
            _cmd_str += "INSERT   INTO layout_resources \n";
            _cmd_str += "       ( lr_type, lr_sub_type, lr_name, lr_data, lr_options ) \n";
            _cmd_str += "VALUES ( " + _resource.type + " \n";

            if (_resource.subtype != 0)
            {
              _cmd_str += "       , " + _resource.subtype + " \n";
            }
            else
            {
              _cmd_str += "       , @_ID \n";
            }

            _cmd_str += "       , '" + _resource.name + "' \n";
            //_cmd_str += "       , '" + (String)(_resource.data.ToString()).Replace("\"", "\\\"") + "' \n";
            //_cmd_str += "       , '" + (String)(_resource.options.ToString()).Replace("\"", "\\\"") + "' \n";
            //_cmd_str += "       , '" + _resource.data.ToString(Formatting.None) + "' \n";
            //_cmd_str += "       , '" + _resource.options.ToString(Formatting.None) + "' \n";
            _cmd_str += "       , '" + _resource.data + "' \n";
            _cmd_str += "       , '" + _resource.options + "' \n";
            _cmd_str += "       ) \n";
            _cmd_str += "\n";

            if (_resource.type == EN_RESOURCE_TYPES.RT_GEOMETRY)
            {
              _cmd_str += "SET @_ID = @@IDENTITY \n";
              _cmd_str += "\n";
            }

          }

          using (SqlCommand _cmd = new SqlCommand(String.Empty, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.CommandText = _cmd_str;

            _cmd.ExecuteNonQuery();
          }

          _db_trx.Commit();

          _result = "[OK]";

        }
        catch (Exception _ex)
        {
          _db_trx.Rollback();
          _result = "[ERROR]";
          throw _ex;
        }

      }

      return _result;
    }

    public static String GetAvailableMeshes()
    {
      String _result = String.Empty;
      DataSet _meshes = new DataSet();

      using (DB_TRX _db_trx = new DB_TRX())
      {
        using (SqlCommand _cmd = new SqlCommand("Layout_Editor_GetAvailableMeshes", _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
        {
          _cmd.CommandTimeout = 2 * 60; // 2 minutes
          _cmd.CommandType = CommandType.StoredProcedure;

          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
          {
            _sql_da.Fill(_meshes);
          }

          _result = clsDefaultConverter.DataTableToReport(_meshes.Tables[0]);
        }
      }

      return _result;
    }

    public static String GetFloorResources(Int32 FloorId)
    {
      String _result = String.Empty;
      DataSet _resources = new DataSet();

      using (DB_TRX _db_trx = new DB_TRX())
      {
        using (SqlCommand _cmd = new SqlCommand("Layout_GetFloorResources", _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
        {
          _cmd.CommandType = CommandType.StoredProcedure;
          _cmd.CommandTimeout = 2 * 60; // 2 minutes

          _cmd.Parameters.Add("@pFloorId", FloorId);

          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
          {
            _sql_da.Fill(_resources);
          }

          _result = clsDefaultConverter.DataTableToReport(_resources.Tables[0]);
        }
      }

      return _result;
    }

  }
}