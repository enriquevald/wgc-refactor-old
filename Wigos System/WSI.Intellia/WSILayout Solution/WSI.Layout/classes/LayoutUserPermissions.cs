﻿using System;
using System.Collections.Generic;
using System.Web;

using WSI.Common;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace WSI.Layout.classes
{
  #region Enums

  // Permission type enumeration
  public enum EN_PERMISSION_TYPE
  {
    PT_READ = 0,
    PT_WRITE = 1,
    PT_DELETE = 2,
    PT_EXECUTE = 3
  }

  // Sections and menu items levels
  public enum EN_ITEM_LEVEL
  {
    IL_LEVEL_NONE = 0,
    IL_LEVEL_1 = 1,
    IL_LEVEL_2 = 2,
    IL_LEVEL_3 = 3
  }

  // Sections and menu items action types
  public enum EN_ITEM_ACTION_TYPE
  {
    IACT_NONE = 0,
    IACT_OPEN_MENU = 1,
    IACT_CHANGE_VIEW = 2,
    IACT_CHECK = 3,
    IACT_SEARCH = 4,
    IACT_LEGEND = 5,

    IACT_CUSTOM = 99
  }

  public enum EN_ROLES
  {
    NONE = 0,
    PCA = 1, // 10 Players Club Attendant
    OPS = 2, // 11 Ops Manager
    SLO = 4, // 12 Slot Attendant
    TCH = 8  // 13 Technical Support
  }

  public enum EN_SECTION_ID
  {
    DASHBOARD = 10,
    FLOOR = 20,
    TASKS = 30,
    ALARMS = 40,
    ALARMS_PLAYERS = 41,
    ALARMS_MACHINES = 42,
    CHARTS = 50,
    PLAYERS = 60,
    MACHINES = 70,
    MONITOR = 80
  }
  #endregion

  // Section item details
  public class LayoutMenuItem
  {
    private Int32 m_id = Int32.MinValue;
    private String m_nls = String.Empty;
    private String m_script = String.Empty;
    private String m_css = String.Empty;
    private Int32 m_section_id = Int32.MinValue;
    private EN_ITEM_LEVEL m_level = EN_ITEM_LEVEL.IL_LEVEL_NONE;
    private EN_ITEM_ACTION_TYPE m_action_type = EN_ITEM_ACTION_TYPE.IACT_NONE;
    private EN_ROLES m_rol;

    #region Properties

    public Int32 Id
    {
      get { return m_id; }
    }
    public String NLS
    {
      get { return m_nls; }
    }
    public String Script
    {
      get { return m_script; }
    }
    public String CSS
    {
      get { return m_css; }
    }
    public Int32 SectionId
    {
      get { return m_section_id; }
    }
    public EN_ITEM_LEVEL Level
    {
      get { return m_level; }
    }
    public EN_ITEM_ACTION_TYPE ActionType
    {
      get { return m_action_type; }
    }
    public EN_ROLES Rol
    {
      get { return m_rol; }
    }
    #endregion

    #region Constructor
    public LayoutMenuItem(Int32 Id, String NLS, String Script, String Css, EN_ITEM_LEVEL Level, EN_ITEM_ACTION_TYPE ActionType, EN_ROLES Rol)
      : this(Id, NLS, Script, Css, Int32.MinValue, Level, ActionType,Rol)
    { }

    public LayoutMenuItem(Int32 Id, String NLS, String Script, String Css, Int32 SectionId, EN_ITEM_LEVEL Level, EN_ITEM_ACTION_TYPE ActionType, EN_ROLES Rol)
    {
      this.m_id = Id;
      this.m_nls = NLS;
      this.m_script = Script;
      this.m_css = Css;
      this.m_level = Level;
      this.m_action_type = ActionType;
      this.m_section_id = SectionId;
      this.m_rol = Rol;
    }
    #endregion

    public Boolean Equals(LayoutMenuItem Item)
    {
      return Item.Id == this.m_id
          && Item.NLS == this.m_nls
          && Item.Script == this.m_script
          && Item.CSS == this.m_css
          && Item.SectionId == this.m_section_id
          && Item.Level == this.m_level
          && Item.ActionType == this.m_action_type;
    }
  }

  // Contains the permissions structure for a user
  public class LayoutUserPermissions
  {
    private const Int32 SECTION_LENGHT = 3;
    private const Int32 FEATURE_LENGHT = 3;

    private DataTable m_dt_user_functions;
    private Int32 m_user_id = Int32.MinValue;
    private List<Int32> m_roles = new List<Int32>();
    private List<Int32> m_availableRoles = new List<Int32>();
    private EN_ROLES m_current_role = EN_ROLES.NONE;

    #region "Properties"
    // User Id
    public Int32 UserId
    {
      get { return m_user_id; }
    }

    // public property with parsed role list
    public List<Int32> Roles
    {
      get { return m_roles; }
    }
    public List<Int32> AvailableRoles
    {
      get { return m_availableRoles; }

    }
    #endregion

    // Constructor 
    public LayoutUserPermissions(Int32 UserId)
    {
      this.m_user_id = UserId;

      this.DB_LoadPermissions();

      this.ParseRoles();
      this.ParseAvailableRoles();
    }

    // Constructor 
    public LayoutUserPermissions(Int32 UserId, EN_ROLES CurrentRole)
    {
      this.m_current_role = CurrentRole;
      this.m_user_id = UserId;

      this.DB_LoadPermissions();

      this.ParseRoles();
      this.ParseAvailableRoles();
    }

    /// <summary>
    /// Returns a datatable with all features with format: ROL|SEC|FNC|CSS|NLS|LVL|ACTT|SCR|ORD|DEF|     R|    W|    X|    D
    /// </summary>
    /// <returns>DataTable</returns>
    private DataTable GetLayoutFeaturesList()
    {
      DataTable _dt_features;

      _dt_features = new DataTable();

      _dt_features.Columns.Add("LUF_ROLE", typeof(Int32));
      _dt_features.Columns.Add("LUF_SECTION", typeof(Int32));
      _dt_features.Columns.Add("LUF_FUNCTION", typeof(Int32));
      _dt_features.Columns.Add("LUF_CSS", typeof(String));
      _dt_features.Columns.Add("LUF_NLS", typeof(String));
      _dt_features.Columns.Add("LUF_LEVEL", typeof(Int32));
      _dt_features.Columns.Add("LUF_ACT_TYPE", typeof(Int32));
      _dt_features.Columns.Add("LUF_SCRIPT", typeof(String));
      _dt_features.Columns.Add("LUF_ORDER", typeof(Int32));
      _dt_features.Columns.Add("LUF_DEFAULT", typeof(Boolean));

      //                       ROL|SEC|FNC|CSS|NLS|LVL|ACTT|SCR|ORD|DEF|     R|    W|    X|    D

      _dt_features.Rows.Add(0, 0, 1, "", "LAYOUT_ALLOWED_ACCESS", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "", 0, false);
      _dt_features.Rows.Add(0, 0, 2, "", "HIDE_MONETARY_DATA", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "", 0, false);

      //********************* PCA ****************************************************//
      _dt_features.Rows.Add(1, 10, 000, "ui-menu-item", "NLS_DASHBOARD", EN_ITEM_LEVEL.IL_LEVEL_1, EN_ITEM_ACTION_TYPE.IACT_CHANGE_VIEW, "", 1, false); //Dashboard
      _dt_features.Rows.Add(1, 10, 100, "ui-menu-item", "NLS_DASH_FEAT", EN_ITEM_LEVEL.IL_LEVEL_3, EN_ITEM_ACTION_TYPE.IACT_NONE, "", 0, false); //Dashboard - Feats
     // _dt_features.Rows.Add(1, 10, 200, "ui-menu-item", "NLS_DASH_STATS", EN_ITEM_LEVEL.IL_LEVEL_3, EN_ITEM_ACTION_TYPE.IACT_NONE, "", 0, false); //Dashboard - Stats

      //widgets
      _dt_features.Rows.Add(1, 10, 200, "", "widgetVipsList", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetListBase", 0, false);
      _dt_features.Rows.Add(1, 10, 201, "", "widgetBirthday", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetListBase", 0, false);
      _dt_features.Rows.Add(1, 10, 202, "", "widgetTestList2", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetListBase", 0, false);
      _dt_features.Rows.Add(1, 10, 203, "", "widgetTasks1", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetBase", 0, false);
      _dt_features.Rows.Add(1, 10, 204, "", "widgetDonut1", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetSVGDonutChart", 0, false);
      _dt_features.Rows.Add(1, 10, 205, "", "widgetHRList", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetListBase", 0, false);
      _dt_features.Rows.Add(1, 10, 206, "", "widgetPlayersMaxLevel", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetListBase", 0, false);
      _dt_features.Rows.Add(1, 10, 207, "", "widgetMachineOccupation", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetSVGDonutChart", 0, false);
      _dt_features.Rows.Add(1, 10, 208, "", "widgetChart2", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetSVGChart", 0, false);
      _dt_features.Rows.Add(1, 10, 209, "", "widgetRunnersList", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetListBase", 0, false);
      _dt_features.Rows.Add(1, 10, 210, "", "widgetIndicator3", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetBase", 0, false);
      _dt_features.Rows.Add(1, 10, 211, "", "widgetRoomOcc", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetSVGDonutChart", 0, false);
      _dt_features.Rows.Add(1, 10, 212, "", "widgetAgeSegmentation", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetSVGDonutChart", 0, false);
      _dt_features.Rows.Add(1, 10, 213, "", "widgetLevelSegmentation", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetSVGDonutChart", 0, false);
      _dt_features.Rows.Add(1, 10, 214, "", "widgetMachineOccRoomOcc", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetSVGDonutChart", 0, false);
      _dt_features.Rows.Add(1, 10, 215, "", "widgettheater2", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetSVGAmphitheater", 0, false);
      _dt_features.Rows.Add(1, 10, 216, "", "widgettheater3", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetSVGAmphitheater", 0, false);
      _dt_features.Rows.Add(1, 10, 217, "", "widgetOccupancy1", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetSVGAmphitheater", 0, false);


      _dt_features.Rows.Add(1, 30, 000, "ui-menu-item", "NLS_MY_TASKS", EN_ITEM_LEVEL.IL_LEVEL_1, EN_ITEM_ACTION_TYPE.IACT_CHANGE_VIEW, "", 0, false); // My TAsks

      _dt_features.Rows.Add(1, 40, 000, "ui-menu-item", "NLS_ALARMS", EN_ITEM_LEVEL.IL_LEVEL_1, EN_ITEM_ACTION_TYPE.IACT_NONE, "", 5, false);
      _dt_features.Rows.Add(1, 43, 003, "ui-menu-item", "NLS_ALARMS_SHOW_RUNNERS", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_CHECK, "SetRunnersmap();", 0, false);
      _dt_features.Rows.Add(1, 40, 001, "ui-menu-item", "NLS_PLAYERS_LIST", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_CHECK, "", 1, false);
      _dt_features.Rows.Add(1, 41, 001, "ui-menu-item", "NLS_ALARMS_ONLY_PLAYERS", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_LEGEND, "", 3, false);
      _dt_features.Rows.Add(1, 42, 002, "ui-menu-item", "NLS_ALARMS_ONLY_MACHINES", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_LEGEND, "", 2, false);

      _dt_features.Rows.Add(1, 60, 000, "ui-menu-item", "NLS_PLAYERS", EN_ITEM_LEVEL.IL_LEVEL_1, EN_ITEM_ACTION_TYPE.IACT_NONE, "", 4, false);
      _dt_features.Rows.Add(1, 60, 002, "ui-menu-item", "NLS_FLOOR_COMPARE_TERMS", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_CHECK, "", 0, false);
      _dt_features.Rows.Add(1, 60, 100, "ui-menu-item", "NLS_PLAYERS_SEARCH", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_SEARCH, "", 3, false);
      _dt_features.Rows.Add(1, 60, 300, "ui-menu-item", "NLS_FLOOR_TEMP_MAP", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_CHECK, "SetHeatmap();", 1, false); // Floor - Heat Map
      _dt_features.Rows.Add(1, 60, 200, "ui-menu-item", "NLS_PLAYERS_FILTERS", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_LEGEND, "", 4, false);
      _dt_features.Rows.Add(1, 60, 001, "ui-menu-item", "NLS_PLAYERS_LIST", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_CHECK, "", 2, false);

      _dt_features.Rows.Add(1, 70, 000, "ui-menu-item", "NLS_TERMINALS", EN_ITEM_LEVEL.IL_LEVEL_1, EN_ITEM_ACTION_TYPE.IACT_NONE, "", 3, false);
      _dt_features.Rows.Add(1, 70, 001, "ui-menu-item", "NLS_FLOOR_COMPARE_TERMS", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_CHECK, "", 0, false);
      _dt_features.Rows.Add(1, 70, 002, "ui-menu-item", "NLS_FLOOR_FILTERS", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_LEGEND, "", 4, false);
      _dt_features.Rows.Add(1, 70, 003, "ui-menu-item", "NLS_PLAYERS_LIST", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_CHECK, "", 2, false);
      _dt_features.Rows.Add(1, 70, 004, "ui-menu-item", "NLS_PLAYERS_SEARCH", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_SEARCH, "", 3, false);
      _dt_features.Rows.Add(1, 70, 005, "ui-menu-item", "NLS_FLOOR_TEMP_MAP", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_CHECK, "SetHeatmap();", 1, false); // Floor - Heat Map

      _dt_features.Rows.Add(1, 80, 000, "ui-menu-item", "NLS_MONITOR", EN_ITEM_LEVEL.IL_LEVEL_1, EN_ITEM_ACTION_TYPE.IACT_NONE, "", 6, false);
      _dt_features.Rows.Add(1, 80, 001, "ui-menu-item", "NLS_FLOOR_FILTERS", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_LEGEND, "", 1, false);

      _dt_features.Rows.Add(1, 90, 000, "ui-menu-item", "NLS_FLOOR_FILTERS", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "", 1, false);

      //********************* OPS
      _dt_features.Rows.Add(2, 10, 000, "ui-menu-item", "NLS_DASHBOARD", EN_ITEM_LEVEL.IL_LEVEL_1, EN_ITEM_ACTION_TYPE.IACT_CHANGE_VIEW, "", 1, false); //Dashboard
      _dt_features.Rows.Add(2, 10, 100, "ui-menu-item", "NLS_DASH_FEAT", EN_ITEM_LEVEL.IL_LEVEL_3, EN_ITEM_ACTION_TYPE.IACT_NONE, "", 0, false); //Dashboard - Feats
    //  _dt_features.Rows.Add(2, 10, 200, "ui-menu-item", "NLS_DASH_STATS", EN_ITEM_LEVEL.IL_LEVEL_3, EN_ITEM_ACTION_TYPE.IACT_NONE, "", 0, false); //Dashboard - Stats


      //widgets
      _dt_features.Rows.Add(2, 10, 200, "", "widgetVipsList", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetListBase", 0, false);
      _dt_features.Rows.Add(2, 10, 201, "", "widgetBirthday", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetListBase", 0, false);
      _dt_features.Rows.Add(2, 10, 202, "", "widgetTestList2", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetListBase", 0, false);
      _dt_features.Rows.Add(2, 10, 203, "", "widgetTasks1", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetBase", 0, false);
      _dt_features.Rows.Add(2, 10, 204, "", "widgetDonut1", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetSVGDonutChart", 0, false);
      _dt_features.Rows.Add(2, 10, 205, "", "widgetHRList", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetListBase", 0, false);
      _dt_features.Rows.Add(2, 10, 206, "", "widgetPlayersMaxLevel", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetListBase", 0, false);
      _dt_features.Rows.Add(2, 10, 207, "", "widgetMachineOccupation", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetSVGDonutChart", 0, false);
      _dt_features.Rows.Add(2, 10, 208, "", "widgetChart2", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetSVGChart", 0, false);
      _dt_features.Rows.Add(2, 10, 209, "", "widgetRunnersList", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetListBase", 0, false);
      _dt_features.Rows.Add(2, 10, 210, "", "widgetIndicator3", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetBase", 0, false);
      _dt_features.Rows.Add(2, 10, 211, "", "widgetRoomOcc", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetSVGDonutChart", 0, false);
      _dt_features.Rows.Add(2, 10, 212, "", "widgetAgeSegmentation", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetSVGDonutChart", 0, false);
      _dt_features.Rows.Add(2, 10, 213, "", "widgetLevelSegmentation", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetSVGDonutChart", 0, false);
      _dt_features.Rows.Add(2, 10, 214, "", "widgetMachineOccRoomOcc", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetSVGDonutChart", 0, false);
      _dt_features.Rows.Add(2, 10, 215, "", "widgettheater2", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetSVGAmphitheater", 0, false);
      _dt_features.Rows.Add(2, 10, 216, "", "widgettheater3", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetSVGAmphitheater", 0, false);
      _dt_features.Rows.Add(2, 10, 217, "", "widgetOccupancy1", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetSVGAmphitheater", 0, false);


      _dt_features.Rows.Add(2, 30, 000, "ui-menu-item", "NLS_MY_TASKS", EN_ITEM_LEVEL.IL_LEVEL_1, EN_ITEM_ACTION_TYPE.IACT_CHANGE_VIEW, "", 0, false); // My TAsks

      _dt_features.Rows.Add(2, 60, 000, "ui-menu-item", "NLS_PLAYERS", EN_ITEM_LEVEL.IL_LEVEL_1, EN_ITEM_ACTION_TYPE.IACT_NONE, "", 4, false);
      _dt_features.Rows.Add(2, 60, 002, "ui-menu-item", "NLS_FLOOR_COMPARE_TERMS", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_CHECK, "", 0, false);
      _dt_features.Rows.Add(2, 60, 100, "ui-menu-item", "NLS_PLAYERS_SEARCH", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_SEARCH, "", 3, false);
      _dt_features.Rows.Add(2, 60, 300, "ui-menu-item", "NLS_FLOOR_TEMP_MAP", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_CHECK, "SetHeatmap();", 1, false); // Floor - Heat Map
      _dt_features.Rows.Add(2, 60, 200, "ui-menu-item", "NLS_PLAYERS_FILTERS", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_LEGEND, "", 4, false);
      _dt_features.Rows.Add(2, 60, 001, "ui-menu-item", "NLS_PLAYERS_LIST", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_CHECK, "", 2, false);

      _dt_features.Rows.Add(2, 40, 000, "ui-menu-item", "NLS_ALARMS", EN_ITEM_LEVEL.IL_LEVEL_1, EN_ITEM_ACTION_TYPE.IACT_NONE, "", 5, false);
      _dt_features.Rows.Add(2, 43, 003, "ui-menu-item", "NLS_ALARMS_SHOW_RUNNERS", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_CHECK, "SetRunnersmap();", 0, false);
      _dt_features.Rows.Add(2, 40, 001, "ui-menu-item", "NLS_PLAYERS_LIST", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_CHECK, "", 1, false);
      _dt_features.Rows.Add(2, 41, 001, "ui-menu-item", "NLS_ALARMS_ONLY_PLAYERS", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_LEGEND, "", 3, false);
      _dt_features.Rows.Add(2, 42, 002, "ui-menu-item", "NLS_ALARMS_ONLY_MACHINES", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_LEGEND, "", 2, false);

      _dt_features.Rows.Add(2, 70, 000, "ui-menu-item", "NLS_TERMINALS", EN_ITEM_LEVEL.IL_LEVEL_1, EN_ITEM_ACTION_TYPE.IACT_NONE, "", 3, false);
      _dt_features.Rows.Add(2, 70, 001, "ui-menu-item", "NLS_FLOOR_COMPARE_TERMS", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_CHECK, "", 0, false);
      _dt_features.Rows.Add(2, 70, 002, "ui-menu-item", "NLS_FLOOR_FILTERS", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_LEGEND, "", 4, false);
      _dt_features.Rows.Add(2, 70, 003, "ui-menu-item", "NLS_PLAYERS_LIST", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_CHECK, "", 2, false);
      _dt_features.Rows.Add(2, 70, 004, "ui-menu-item", "NLS_PLAYERS_SEARCH", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_SEARCH, "", 3, false);
      _dt_features.Rows.Add(2, 70, 005, "ui-menu-item", "NLS_FLOOR_TEMP_MAP", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_CHECK, "SetHeatmap();", 1, false); // Floor - Heat Map

      _dt_features.Rows.Add(2, 80, 000, "ui-menu-item", "NLS_MONITOR", EN_ITEM_LEVEL.IL_LEVEL_1, EN_ITEM_ACTION_TYPE.IACT_NONE, "", 6, false);
      _dt_features.Rows.Add(2, 80, 001, "ui-menu-item", "NLS_FLOOR_FILTERS", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_LEGEND, "", 1, false);

      _dt_features.Rows.Add(2, 90, 000, "ui-menu-item", "NLS_FLOOR_FILTERS", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "", 1, false);
      //********************* SLO
      _dt_features.Rows.Add(4, 10, 000, "ui-menu-item", "NLS_DASHBOARD", EN_ITEM_LEVEL.IL_LEVEL_1, EN_ITEM_ACTION_TYPE.IACT_CHANGE_VIEW, "", 1, false); //Dashboard
      _dt_features.Rows.Add(4, 10, 100, "ui-menu-item", "NLS_DASH_FEAT", EN_ITEM_LEVEL.IL_LEVEL_3, EN_ITEM_ACTION_TYPE.IACT_NONE, "", 0, false); //Dashboard - Feats
     // _dt_features.Rows.Add(4, 10, 200, "ui-menu-item", "NLS_DASH_STATS", EN_ITEM_LEVEL.IL_LEVEL_3, EN_ITEM_ACTION_TYPE.IACT_NONE, "", 0, false); //Dashboard - Stats

      //widgets
      _dt_features.Rows.Add(4, 10, 200, "", "widgetVipsList", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetListBase", 0, false);
      _dt_features.Rows.Add(4, 10, 201, "", "widgetBirthday", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetListBase", 0, false);
      _dt_features.Rows.Add(4, 10, 202, "", "widgetTestList2", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetListBase", 0, false);
      _dt_features.Rows.Add(4, 10, 203, "", "widgetTasks1", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetBase", 0, false);
      _dt_features.Rows.Add(4, 10, 204, "", "widgetDonut1", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetSVGDonutChart", 0, false);
      _dt_features.Rows.Add(4, 10, 205, "", "widgetHRList", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetListBase", 0, false);
      _dt_features.Rows.Add(4, 10, 206, "", "widgetPlayersMaxLevel", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetListBase", 0, false);
      _dt_features.Rows.Add(4, 10, 207, "", "widgetMachineOccupation", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetSVGDonutChart", 0, false);
      _dt_features.Rows.Add(4, 10, 208, "", "widgetChart2", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetSVGChart", 0, false);
      _dt_features.Rows.Add(4, 10, 209, "", "widgetRunnersList", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetListBase", 0, false);
      _dt_features.Rows.Add(4, 10, 210, "", "widgetIndicator3", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetBase", 0, false);
      _dt_features.Rows.Add(4, 10, 211, "", "widgetRoomOcc", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetSVGDonutChart", 0, false);
      _dt_features.Rows.Add(4, 10, 212, "", "widgetAgeSegmentation", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetSVGDonutChart", 0, false);
      _dt_features.Rows.Add(4, 10, 213, "", "widgetLevelSegmentation", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetSVGDonutChart", 0, false);
      _dt_features.Rows.Add(4, 10, 214, "", "widgetMachineOccRoomOcc", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetSVGDonutChart", 0, false);
      _dt_features.Rows.Add(4, 10, 215, "", "widgettheater2", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetSVGAmphitheater", 0, false);
      _dt_features.Rows.Add(4, 10, 216, "", "widgettheater3", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetSVGAmphitheater", 0, false);
      _dt_features.Rows.Add(4, 10, 217, "", "widgetOccupancy1", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetSVGAmphitheater", 0, false);


      _dt_features.Rows.Add(4, 30, 000, "ui-menu-item", "NLS_MY_TASKS", EN_ITEM_LEVEL.IL_LEVEL_1, EN_ITEM_ACTION_TYPE.IACT_CHANGE_VIEW, "", 0, false); // My TAsks

      _dt_features.Rows.Add(4, 60, 000, "ui-menu-item", "NLS_PLAYERS", EN_ITEM_LEVEL.IL_LEVEL_1, EN_ITEM_ACTION_TYPE.IACT_NONE, "", 4, false);
      _dt_features.Rows.Add(4, 60, 002, "ui-menu-item", "NLS_FLOOR_COMPARE_TERMS", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_CHECK, "", 0, false);
      _dt_features.Rows.Add(4, 60, 100, "ui-menu-item", "NLS_PLAYERS_SEARCH", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_SEARCH, "", 3, false);
      _dt_features.Rows.Add(4, 60, 300, "ui-menu-item", "NLS_FLOOR_TEMP_MAP", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_CHECK, "SetHeatmap();", 1, false); // Floor - Heat Map
      _dt_features.Rows.Add(4, 60, 200, "ui-menu-item", "NLS_PLAYERS_FILTERS", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_LEGEND, "", 4, false);
      _dt_features.Rows.Add(4, 60, 001, "ui-menu-item", "NLS_PLAYERS_LIST", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_CHECK, "", 2, false);

      _dt_features.Rows.Add(4, 40, 000, "ui-menu-item", "NLS_ALARMS", EN_ITEM_LEVEL.IL_LEVEL_1, EN_ITEM_ACTION_TYPE.IACT_NONE, "", 5, false);
      _dt_features.Rows.Add(4, 43, 003, "ui-menu-item", "NLS_ALARMS_SHOW_RUNNERS", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_CHECK, "SetRunnersmap();", 0, false);
      _dt_features.Rows.Add(4, 40, 001, "ui-menu-item", "NLS_PLAYERS_LIST", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_CHECK, "", 1, false);
      _dt_features.Rows.Add(4, 41, 001, "ui-menu-item", "NLS_ALARMS_ONLY_PLAYERS", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_LEGEND, "", 3, false);
      _dt_features.Rows.Add(4, 42, 002, "ui-menu-item", "NLS_ALARMS_ONLY_MACHINES", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_LEGEND, "", 2, false);

      _dt_features.Rows.Add(4, 70, 000, "ui-menu-item", "NLS_TERMINALS", EN_ITEM_LEVEL.IL_LEVEL_1, EN_ITEM_ACTION_TYPE.IACT_NONE, "", 3, false);
      _dt_features.Rows.Add(4, 70, 001, "ui-menu-item", "NLS_FLOOR_COMPARE_TERMS", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_CHECK, "", 0, false);
      _dt_features.Rows.Add(4, 70, 002, "ui-menu-item", "NLS_FLOOR_FILTERS", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_LEGEND, "", 4, false);
      _dt_features.Rows.Add(4, 70, 003, "ui-menu-item", "NLS_PLAYERS_LIST", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_CHECK, "", 2, false);
      _dt_features.Rows.Add(4, 70, 004, "ui-menu-item", "NLS_PLAYERS_SEARCH", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_SEARCH, "", 3, false);
      _dt_features.Rows.Add(4, 70, 005, "ui-menu-item", "NLS_FLOOR_TEMP_MAP", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_CHECK, "SetHeatmap();", 1, false); // Floor - Heat Map

      _dt_features.Rows.Add(4, 80, 000, "ui-menu-item", "NLS_MONITOR", EN_ITEM_LEVEL.IL_LEVEL_1, EN_ITEM_ACTION_TYPE.IACT_NONE, "", 6, false);
      _dt_features.Rows.Add(4, 80, 001, "ui-menu-item", "NLS_FLOOR_FILTERS", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_LEGEND, "", 1, false);

      _dt_features.Rows.Add(4, 90, 000, "ui-menu-item", "NLS_FLOOR_FILTERS", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "", 1, false);
      //********************* TCH
      _dt_features.Rows.Add(8, 10, 000, "ui-menu-item", "NLS_DASHBOARD", EN_ITEM_LEVEL.IL_LEVEL_1, EN_ITEM_ACTION_TYPE.IACT_CHANGE_VIEW, "", 1, false); //Dashboard
      _dt_features.Rows.Add(8, 10, 100, "ui-menu-item", "NLS_DASH_FEAT", EN_ITEM_LEVEL.IL_LEVEL_3, EN_ITEM_ACTION_TYPE.IACT_NONE, "", 0, false); //Dashboard - Feats
      //_dt_features.Rows.Add(8, 10, 200, "ui-menu-item", "NLS_DASH_STATS", EN_ITEM_LEVEL.IL_LEVEL_3, EN_ITEM_ACTION_TYPE.IACT_NONE, "", 0, false); //Dashboard - Stats

      //widgets
      _dt_features.Rows.Add(8, 10, 200, "", "widgetVipsList", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetListBase", 0, false);
      _dt_features.Rows.Add(8, 10, 201, "", "widgetBirthday", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetListBase", 0, false);
      _dt_features.Rows.Add(8, 10, 202, "", "widgetTestList2", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetListBase", 0, false);
      _dt_features.Rows.Add(8, 10, 203, "", "widgetTasks1", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetBase", 0, false);
      _dt_features.Rows.Add(8, 10, 204, "", "widgetDonut1", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetSVGDonutChart", 0, false);
      _dt_features.Rows.Add(8, 10, 205, "", "widgetHRList", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetListBase", 0, false);
      _dt_features.Rows.Add(8, 10, 206, "", "widgetPlayersMaxLevel", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetListBase", 0, false);
      _dt_features.Rows.Add(8, 10, 207, "", "widgetMachineOccupation", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetSVGDonutChart", 0, false);
      _dt_features.Rows.Add(8, 10, 208, "", "widgetChart2", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetSVGChart", 0, false);
      _dt_features.Rows.Add(8, 10, 209, "", "widgetRunnersList", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetListBase", 0, false);
      _dt_features.Rows.Add(8, 10, 210, "", "widgetIndicator3", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetBase", 0, false);
      _dt_features.Rows.Add(8, 10, 211, "", "widgetRoomOcc", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetSVGDonutChart", 0, false);
      _dt_features.Rows.Add(8, 10, 212, "", "widgetAgeSegmentation", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetSVGDonutChart", 0, false);
      _dt_features.Rows.Add(8, 10, 213, "", "widgetLevelSegmentation", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetSVGDonutChart", 0, false);
      _dt_features.Rows.Add(8, 10, 214, "", "widgetMachineOccRoomOcc", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetSVGDonutChart", 0, false);
      _dt_features.Rows.Add(8, 10, 215, "", "widgettheater2", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetSVGAmphitheater", 0, false);
      _dt_features.Rows.Add(8, 10, 216, "", "widgettheater3", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetSVGAmphitheater", 0, false);
      _dt_features.Rows.Add(8, 10, 217, "", "widgetOccupancy1", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "WidgetSVGAmphitheater", 0, false);

      _dt_features.Rows.Add(8, 30, 000, "ui-menu-item", "NLS_MY_TASKS", EN_ITEM_LEVEL.IL_LEVEL_1, EN_ITEM_ACTION_TYPE.IACT_CHANGE_VIEW, "", 0, false); // My TAsks

      _dt_features.Rows.Add(8, 60, 000, "ui-menu-item", "NLS_PLAYERS", EN_ITEM_LEVEL.IL_LEVEL_1, EN_ITEM_ACTION_TYPE.IACT_NONE, "", 4, false);
      _dt_features.Rows.Add(8, 60, 002, "ui-menu-item", "NLS_FLOOR_COMPARE_TERMS", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_CHECK, "", 0, false);
      _dt_features.Rows.Add(8, 60, 100, "ui-menu-item", "NLS_PLAYERS_SEARCH", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_SEARCH, "", 3, false);
      _dt_features.Rows.Add(8, 60, 300, "ui-menu-item", "NLS_FLOOR_TEMP_MAP", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_CHECK, "SetHeatmap();", 1, false); // Floor - Heat Map
      _dt_features.Rows.Add(8, 60, 200, "ui-menu-item", "NLS_PLAYERS_FILTERS", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_LEGEND, "", 4, false);
      _dt_features.Rows.Add(8, 60, 001, "ui-menu-item", "NLS_PLAYERS_LIST", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_CHECK, "", 2, false);

      _dt_features.Rows.Add(8, 40, 000, "ui-menu-item", "NLS_ALARMS", EN_ITEM_LEVEL.IL_LEVEL_1, EN_ITEM_ACTION_TYPE.IACT_NONE, "", 5, false);
      _dt_features.Rows.Add(8, 43, 003, "ui-menu-item", "NLS_ALARMS_SHOW_RUNNERS", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_CHECK, "SetRunnersmap();", 0, false);
      _dt_features.Rows.Add(8, 40, 001, "ui-menu-item", "NLS_PLAYERS_LIST", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_CHECK, "", 1, false);
      _dt_features.Rows.Add(8, 41, 001, "ui-menu-item", "NLS_ALARMS_ONLY_PLAYERS", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_LEGEND, "", 3, false);
      _dt_features.Rows.Add(8, 42, 002, "ui-menu-item", "NLS_ALARMS_ONLY_MACHINES", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_LEGEND, "", 2, false);

      _dt_features.Rows.Add(8, 70, 000, "ui-menu-item", "NLS_TERMINALS", EN_ITEM_LEVEL.IL_LEVEL_1, EN_ITEM_ACTION_TYPE.IACT_NONE, "", 3, false);
      _dt_features.Rows.Add(8, 70, 001, "ui-menu-item", "NLS_FLOOR_COMPARE_TERMS", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_CHECK, "", 0, false);
      _dt_features.Rows.Add(8, 70, 002, "ui-menu-item", "NLS_FLOOR_FILTERS", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_LEGEND, "", 4, false);
      _dt_features.Rows.Add(8, 70, 003, "ui-menu-item", "NLS_PLAYERS_LIST", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_CHECK, "", 2, false);
      _dt_features.Rows.Add(8, 70, 004, "ui-menu-item", "NLS_PLAYERS_SEARCH", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_SEARCH, "", 3, false);
      _dt_features.Rows.Add(8, 70, 005, "ui-menu-item", "NLS_FLOOR_TEMP_MAP", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_CHECK, "SetHeatmap();", 1, false); // Floor - Heat Map

      _dt_features.Rows.Add(8, 80, 000, "ui-menu-item", "NLS_MONITOR", EN_ITEM_LEVEL.IL_LEVEL_1, EN_ITEM_ACTION_TYPE.IACT_NONE, "", 6, false);
      _dt_features.Rows.Add(8, 80, 001, "ui-menu-item", "NLS_FLOOR_FILTERS", EN_ITEM_LEVEL.IL_LEVEL_2, EN_ITEM_ACTION_TYPE.IACT_LEGEND, "", 1, false);

      _dt_features.Rows.Add(8, 90, 000, "ui-menu-item", "NLS_FLOOR_FILTERS", EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ITEM_ACTION_TYPE.IACT_NONE, "", 1, false);

      return _dt_features;

    }

    // ----------------------------------------------------------------------------

    // Generates the list of roles from the functions collected
    private void ParseRoles()
    {
      m_roles.Clear();
      foreach (DataRow _row in m_dt_user_functions.Rows)
      {
        if ((Int32)_row["LUF_ROLE"] == 0 || (Int32)_row["LUF_SECTION"] == 0) { continue; }
        if (!m_roles.Contains((Int32)_row["LUF_ROLE"]))
        {
          m_roles.Add((Int32)_row["LUF_ROLE"]);
        }

        //if (m_current_role == EN_ROLES.NONE)
        //{
        //  if ((EN_ROLES)_row["LUF_ROLE"] == EN_ROLES.OPS)
        //  {
        //    Global.Sessions.Information[HttpContext.Current.Session.SessionID].User.CurrentRole = (EN_ROLES)_row["LUF_ROLE"];
        //  }
        //  else if (Global.Sessions.Information[HttpContext.Current.Session.SessionID].User.CurrentRole == EN_ROLES.NONE)
        //  {
        //    Global.Sessions.Information[HttpContext.Current.Session.SessionID].User.CurrentRole = (EN_ROLES)_row["LUF_ROLE"];
        //  }
        //}
        //else
        //{
        //  if ((EN_ROLES)_row["LUF_ROLE"] == m_current_role)
        //  {
        //    Global.Sessions.Information[HttpContext.Current.Session.SessionID].User.CurrentRole = m_current_role;
        //  }
        //}
      }
    }
    private void ParseAvailableRoles()
    {
      m_availableRoles = GetAvailableRoles();
     }
    // Check if user has permissions for READ the specified function
    public Boolean CheckFunction(Int32 Profile, Int32 Section, Int32 Function)
    {
      return CheckFunction(Profile, Section, Function, EN_PERMISSION_TYPE.PT_READ);
    }

    // Check if user has the specified type of permission on a function 
    public Boolean CheckFunction(Int32 Profile, Int32 Section, Int32 Function, EN_PERMISSION_TYPE Permission)
    {
      Boolean _result = false;

      DataRow[] _rows = this.m_dt_user_functions.Select("LUF_READ <> 0 AND LUF_ROLE = " + (Int32)Profile + " AND LUF_SECTION = " + (Int32)Section + " AND LUF_FUNCTION = " + (Int32)Function);

      if (_rows.Length == 1)
      {
        switch (Permission)
        {
          case EN_PERMISSION_TYPE.PT_READ:
            _result = (Boolean)_rows[0]["LUF_READ"];
            break;
          case EN_PERMISSION_TYPE.PT_WRITE:
            _result = (Boolean)_rows[0]["LUF_WRITE"];
            break;
          case EN_PERMISSION_TYPE.PT_DELETE:
            _result = (Boolean)_rows[0]["LUF_DELETE"];
            break;
          case EN_PERMISSION_TYPE.PT_EXECUTE:
            _result = (Boolean)_rows[0]["LUF_EXECUTE"];
            break;
        }
      }

      return _result;
    }

    // Return the available sections of a profile for the current user
    public List<LayoutMenuItem> GetProfileMenuItemsByLevel(EN_ITEM_LEVEL Level, EN_ROLES Profile)
    {
      return GetProfileMenuItemsByLevel(Level, 0, Profile);
    }

    /// <summary>
    /// Return the available sections of a profile and section for the current user
    /// </summary>
    /// <param name="Level">EN_ITEM_LEVEL</param>
    /// <param name="SectionId">Int32</param>
    /// <param name="Profile">EN_ROLES</param>
    /// <returns>List of menu items</returns>
    public List<LayoutMenuItem> GetProfileMenuItemsByLevel(EN_ITEM_LEVEL Level, Int32 SectionId, EN_ROLES Profile)
    {
      List<LayoutMenuItem> _items_by_level = new List<LayoutMenuItem>();
      String _filter = String.Empty;
      Int32 _id = Int32.MinValue;

      _filter = "LUF_LEVEL = " + (Int32)Level + " AND LUF_ROLE = " + (Int32)Profile + " ";

      if (SectionId > 0)
      {
        //Apply format <=SectionID AND >SECTIONID
        _filter += FormatSectionNumber(SectionId);
      }

      foreach (DataRow _row in m_dt_user_functions.Select(_filter, "LUF_ORDER ASC "))
      {
        switch (Level)
        {
          case EN_ITEM_LEVEL.IL_LEVEL_2:
            _id = (Int32)_row["LUF_SECTION"] * 1000 + (Int32)_row["LUF_FUNCTION"];
            break;
          default:
            _id = (Int32)_row["LUF_SECTION"];
            break;
        }

        LayoutMenuItem _item = new LayoutMenuItem(_id,
                                                  (String)_row["LUF_NLS"],
                                                  (String)_row["LUF_SCRIPT"],
                                                  (String)_row["LUF_CSS"],
                                                  (Int32)_row["LUF_SECTION"],
                                                  (EN_ITEM_LEVEL)_row["LUF_LEVEL"],
                                                  (EN_ITEM_ACTION_TYPE)_row["LUF_ACT_TYPE"],
                                                  Profile
                                                  );

        if (!_items_by_level.Contains(_item))
        {
          _items_by_level.Add(_item);
        }
      }
      return _items_by_level;
    }
    public List<int> GetAvailableRoles()
    {
      List<LayoutMenuItem> _items_by_level = new List<LayoutMenuItem>();
      String _filter = String.Empty;
      List<int> _roles = new List<int>();

      _filter = "LUF_LEVEL = 1 " ;

      foreach (DataRow _row in m_dt_user_functions.Select(_filter, "LUF_ORDER ASC "))
      {

        if (!_roles.Contains((Int32)_row["LUF_ROLE"]))
        {
          _roles.Add((Int32)_row["LUF_ROLE"]);
        }
      }
      return _roles;
    }
    /// <summary>
    /// Load Permissions from DB
    /// </summary>
    private void DB_LoadPermissions()
    {
      StringBuilder _sb;
      DataTable _dt_permissions;

      _sb = new StringBuilder();
      _dt_permissions = new DataTable();

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand("Layout_GetUserPermissions", _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = m_user_id;

            _sql_cmd.CommandType = CommandType.StoredProcedure;
            _sql_cmd.CommandTimeout = 2 * 60; // 2 minutes

            _dt_permissions.Load(_db_trx.ExecuteReader(_sql_cmd));
          }

          if (_dt_permissions.Rows.Count > 0)
          {
            CompletePermissions(ref _dt_permissions); // SOLO PARA DEBUG

            ParseFunctions(ref _dt_permissions);
          }
          else
          {
            Logger.Write(HttpContext.Current, "DB_LoadPermissions() | No permissions for this user (" + this.m_user_id.ToString() + ")");
          }
        }

        FillPermissionsTable(ref _dt_permissions);

        m_dt_user_functions = _dt_permissions;

      }
      catch (Exception _ex)
      {
        Logger.Exception(HttpContext.Current, "DB_LoadPermissions()", _ex);
      }
    }

    //TODO: Esta funcion va fuera cuando nos pasen los permisos correctamente. Ahora faltan permisos para las funcionalidades.
    private void CompletePermissions(ref DataTable _dt_permissions)
    {
      //_dt_permissions.Rows.Add(1040001, true, true, true, true);
      //_dt_permissions.Rows.Add(1070003, true, true, true, true);
      //_dt_permissions.Rows.Add(1070004, true, true, true, true);
      //_dt_permissions.Rows.Add(1070005, true, true, true, true);
      //_dt_permissions.Rows.Add(1060300, true, true, true, true);
      //_dt_permissions.Rows.Add(1060002, true, true, true, true);


      //_dt_permissions.Rows.Add(2070003, true, true, true, true);
      //_dt_permissions.Rows.Add(2070004, true, true, true, true);
      //_dt_permissions.Rows.Add(2060300, true, true, true, true);
      //_dt_permissions.Rows.Add(2070005, true, true, true, true);
      //_dt_permissions.Rows.Add(2040001, true, true, true, true);
      //_dt_permissions.Rows.Add(2060002, true, true, true, true);

      //_dt_permissions.Rows.Add(4070003, true, true, true, true);
      //_dt_permissions.Rows.Add(4070004, true, true, true, true);
      //_dt_permissions.Rows.Add(4060300, true, true, true, true);
      //_dt_permissions.Rows.Add(4070005, true, true, true, true);
      //_dt_permissions.Rows.Add(4040001, true, true, true, true);
      //_dt_permissions.Rows.Add(4060002, true, true, true, true);

      //_dt_permissions.Rows.Add(8070003, true, true, true, true);
      //_dt_permissions.Rows.Add(8070004, true, true, true, true);
      //_dt_permissions.Rows.Add(8060300, true, true, true, true);
      //_dt_permissions.Rows.Add(8070005, true, true, true, true);
      //_dt_permissions.Rows.Add(8040001, true, true, true, true);
      //_dt_permissions.Rows.Add(8060002, true, true, true, true);


      _dt_permissions.Rows.Add(1040002, true, true, true, true);
      _dt_permissions.Rows.Add(2040002, true, true, true, true);
      _dt_permissions.Rows.Add(4040002, true, true, true, true);
      _dt_permissions.Rows.Add(8040002, true, true, true, true);

      _dt_permissions.AcceptChanges();
    }

    /// <summary>
    /// Fill user permission table with all the info.
    /// </summary>
    /// <param name="DtPermissions"> DataTable with user permissions</param>
    private void FillPermissionsTable(ref DataTable DtPermissions)
    {

      DataColumn _dc = DtPermissions.Columns.Add("LUF_CSS", typeof(String));
      _dc.SetOrdinal(3);

      _dc = DtPermissions.Columns.Add("LUF_NLS", typeof(String));
      _dc.SetOrdinal(4);

      _dc = DtPermissions.Columns.Add("LUF_LEVEL", typeof(Int32));
      _dc.SetOrdinal(5);

      _dc = DtPermissions.Columns.Add("LUF_ACT_TYPE", typeof(Int32));
      _dc.SetOrdinal(6);

      _dc = DtPermissions.Columns.Add("LUF_SCRIPT", typeof(String));
      _dc.SetOrdinal(7);

      _dc = DtPermissions.Columns.Add("LUF_ORDER", typeof(Int32));
      _dc.SetOrdinal(8);

      _dc = DtPermissions.Columns.Add("LUF_DEFAULT", typeof(Boolean));
      _dc.SetOrdinal(9);

      DataTable _features = GetLayoutFeaturesList();

      foreach (DataRow _row in DtPermissions.Rows)
      {
        DataRow[] _result = _features.Select("LUF_ROLE = " + _row["LUF_ROLE"] + " AND LUF_SECTION = " + _row["LUF_SECTION"] + " AND LUF_FUNCTION = " + _row["LUF_FUNCTION"] + "");

        if (_result.Length > 0)
        {
          _row["LUF_CSS"] = _result[0]["LUF_CSS"];
          _row["LUF_NLS"] = _result[0]["LUF_NLS"];
          _row["LUF_LEVEL"] = _result[0]["LUF_LEVEL"];
          _row["LUF_ACT_TYPE"] = _result[0]["LUF_ACT_TYPE"];
          _row["LUF_SCRIPT"] = _result[0]["LUF_SCRIPT"];
          _row["LUF_ORDER"] = _result[0]["LUF_ORDER"];
          _row["LUF_DEFAULT"] = _result[0]["LUF_DEFAULT"];

          _row.AcceptChanges();
        }
      }
    }

    /// <summary>
    /// Transform datatable information.
    /// </summary>
    /// <param name="dtPermissions"> User permissions datatable.</param>
    private void ParseFunctions(ref DataTable dtPermissions)
    {
      Int32 _fnc_id;
      String[] _splitted_number;

      DataColumn _dc = dtPermissions.Columns.Add("LUF_ROLE", typeof(Int32));
      _dc.SetOrdinal(0);

      _dc = dtPermissions.Columns.Add("LUF_SECTION", typeof(Int32));
      _dc.SetOrdinal(1);

      _dc = dtPermissions.Columns.Add("LUF_FUNCTION", typeof(Int32));
      _dc.SetOrdinal(2);

      foreach (DataRow _row in dtPermissions.Rows)
      {
        _fnc_id = (Int32)_row["GPF_FORM_ID"];

        _splitted_number = SplitID(_fnc_id);

        _row["LUF_ROLE"] = Convert.ToInt32(_splitted_number[0]);
        _row["LUF_SECTION"] = Convert.ToInt32(_splitted_number[1]);
        _row["LUF_FUNCTION"] = Convert.ToInt32(_splitted_number[2]);

        _row.AcceptChanges();
      }

      dtPermissions.Columns.Remove("GPF_FORM_ID");
      dtPermissions.AcceptChanges();
    } //ParseFunctions

    /// <summary>
    /// Split ID to 00-00-000 format.
    /// </summary>
    /// <param name="ID">ID to split</param>
    /// <returns>Array with the splits</returns>
    private String[] SplitID(Int32 ID)
    {
      String[] _split_number = new String[3];
      String _number = ID.ToString();

      while (_number.Length < 7)
      {
        _number += "0";
      }

      _split_number[0] = _number.Substring(0, _number.Length - (FEATURE_LENGHT + SECTION_LENGHT));
      _split_number[1] = _number.Substring(_number.Length - FEATURE_LENGHT - SECTION_LENGHT, SECTION_LENGHT);
      _split_number[2] = _number.Substring(_number.Length - FEATURE_LENGHT, FEATURE_LENGHT);

      return _split_number;
    }

    /// <summary>
    /// Returns a query with the range of section introduced.
    /// </summary>
    /// <param name="SectionId">Section ID</param>
    /// <returns>String with the query</returns>
    private String FormatSectionNumber(Int32 SectionId)
    {
      String _filter_result;
      String _number_range_from;
      String _number_range_to;
      String _str_section_id;

      _str_section_id = SectionId.ToString("000");

      _filter_result = String.Empty;
      _number_range_from = String.Empty;
      _number_range_to = String.Empty;

      for (ushort _idx = 0; _idx <= _str_section_id.Length - 1; _idx++)
      {
        _number_range_from = _str_section_id.Substring(_idx, 1);

        if (Convert.ToInt32(_number_range_from) > 0)
        {
          Int32 _value = Convert.ToInt32(_number_range_from);
          _value++;
          _number_range_to = _value.ToString();
          while (_idx < 2)
          {
            _number_range_from += "0";
            _number_range_to += "0";

            _idx++;
          }
          break;
        }

      }

      return "AND LUF_SECTION >= " + _number_range_from + " AND LUF_SECTION < " + _number_range_to;
    }
  } //public class LayoutUserPermissions
}