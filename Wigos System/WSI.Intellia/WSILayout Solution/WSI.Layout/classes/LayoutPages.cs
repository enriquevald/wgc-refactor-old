﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.HtmlControls;

using System.Web.UI;

namespace WSI.Layout.classes
{
  public class LayoutPages
  {
       
    // Includes a CSS style file in the specified page
    public static void IncludeStyle(System.Web.UI.Page Page, String StyleName)
    {
      System.Web.UI.HtmlControls.HtmlLink _link = null;

      _link = new System.Web.UI.HtmlControls.HtmlLink();
      _link.Href = "css/" + StyleName + ".css";
      _link.Attributes.Add("rel", "stylesheet");
      _link.Attributes.Add("type", "text/css");
      Page.Header.Controls.Add(_link);
    }

    // Includes a CSS style file in the specified page
    public static void IncludeExternalStyle(System.Web.UI.Page Page, String PathStyleName)
    {
      System.Web.UI.HtmlControls.HtmlLink _link = null;

      _link = new System.Web.UI.HtmlControls.HtmlLink();
      _link.Href = PathStyleName + ".css";
      _link.Attributes.Add("rel", "stylesheet");
      _link.Attributes.Add("type", "text/css");
      Page.Header.Controls.Add(_link);
    }

    // Adds a JS file to the Script Manager
    public static void RegisterJS(ScriptManager Manager, String JsFile)
    {
      Manager.Scripts.Add(new ScriptReference("js/" + JsFile + ".js"));
    }

    // 
    public static void RegisterScript(Page page, String Script)
    {
      page.ClientScript.RegisterClientScriptBlock(page.GetType(), page.UniqueID, Script);
    }

    // Includes a JS file in the specified page
    public static void IncludeScript(Page page, String Script)
    {
      HtmlGenericControl child = new HtmlGenericControl("script");
      child.Attributes.Add("type", "text/javascript");
      child.InnerText = Script.ToString();
      page.Header.Controls.Add(child);
    }

    // Includes a JS file in the specified page
    public static void IncludeJS(Page page, String JsFile)
    {
      HtmlGenericControl child = new HtmlGenericControl("script");
      child.Attributes.Add("type", "text/javascript");
      child.Attributes.Add("src", "js/" + JsFile + ".js");
      page.Header.Controls.Add(child);
    }

    // Includes a JS file in the specified page
    public static void IncludeExternalJS(Page Page, String PathJsFile)
    {
      HtmlGenericControl child = new HtmlGenericControl("script");
      child.Attributes.Add("type", "text/javascript");
      child.Attributes.Add("src", PathJsFile + ".js");
      Page.Header.Controls.Add(child);
    }

    /*
    public static void IncludeJSScript(Page page, string script)
    {
      HtmlGenericControl child = new HtmlGenericControl("script");
      child.Attributes.Add("type", "text/javascript");
      child.InnerHtml = script;
      page.Header.Controls.Add(child);
    }
    */

    // Returns a generic control in the pae by id
    public static HtmlGenericControl GetCommonControl(System.Web.UI.Page Page, String ControlID)
    {
      return (HtmlGenericControl)Page.FindControl(ControlID);
    }

    // Set a control InnerHtml
    public static Boolean SetCommonControlInnerHtml(System.Web.UI.Page Page, String ControlID, String InnerHtml)
    {
      HtmlGenericControl _control = GetCommonControl(Page, ControlID);
      if (_control != null)
      {
        _control.InnerHtml = InnerHtml;
        return true;
      }
      else
      {
        return false;
      }
    }

  }
}