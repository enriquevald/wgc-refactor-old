﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WSI.Layout.classes
{

    // Contains the information for the current user
    public class LayoutUser
    {

        public Int32 Id = Int32.MinValue;
        public Boolean Logged = false;
        public String Nick = String.Empty;
        public String FullName = String.Empty;
        public LayoutUserPermissions Permissions = null;
        private EN_ROLES m_current_role = EN_ROLES.NONE;
        // TODO: Create ENUM for ViewMode
        private Int32 m_view_mode = 2; // 2 = 2D, 1 = 3D 

        // Constructor
        public LayoutUser()
        {
        }

        // Current View Mode 2D / 3D
        public Int32 ViewMode
        {
          get { return m_view_mode; }
          set { m_view_mode = value; }
        }

        // Current Role for the current session
        public EN_ROLES CurrentRole
        {
          get { return m_current_role; }
          set { m_current_role = value; }
        }

    }

}