﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using WSI.Common;

namespace WSI.Layout.classes
{

  public class LayoutSecurityRight
  {
    public Int64 Id = Int64.MinValue;
    public Boolean Read = false;
    public Boolean Write = false;
    public Boolean Delete = false;
    public Boolean Execute = false;

    public LayoutSecurityRight()
    {
      
    }
  }

  public class LayoutSecurity2
  {
    public List<LayoutSecurityRight> Rights = new List<LayoutSecurityRight>();

    // <Role, Right>
    public Dictionary<Int32, LayoutSecurityRight> RighList = new Dictionary<Int32, LayoutSecurityRight>();

    public void Load(Int32 UserId)
    {

    }

    public LayoutSecurity2()
    {

    }
  }


  // -------------------------------------------------------------------------------------------------------

  // Contains the session security attributes to generate dynamic scripts
  public class LayoutSecurity
  {
    public Boolean AccessTabRealTime = true;
    public Boolean AccessTabConfiguration = true;
    public Boolean AccessTabAlarms = true;
    public Boolean AccessTabCustomers = true;
    public Boolean AccessTabReports = true;

    public LayoutSecurity()
    {
    }
  }

}