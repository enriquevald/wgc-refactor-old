﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WSI.Layout.classes
{
    public enum ENUM_TICKET_STATUS
    {
        NOT_ASSIGNED = 0,
        ASSIGNED = 1,
        ESCALATED = 2,
        IN_PROCESS = 3,
        FINALIZED = 4,
        RECOGNIZED = 5,
        VALIDATION_PENDING = 6
    }
    public class clsTicket
    {
        public clsTicket()
        {
        }
    }
}