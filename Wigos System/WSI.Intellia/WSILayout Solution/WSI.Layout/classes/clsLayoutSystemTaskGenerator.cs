﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Threading;
using System.Data;
using System.Data.SqlClient;
using WSI.Common;
using WSI.Layout;
using System.Web;
using System.Xml.Linq;
using System.Collections;

using System.Web.Script.Serialization;
using System.Diagnostics;
using WSI.Layout.classes;

namespace WSI.Layout
{
  public class clsLayoutSystemTaskGenerator
  {
    #region Enums
    public enum EN_NOTIFICATION_TYPE
    {
      NONE = 0,
      MANUAL = 1,
      AUTO_BY_ROLE = 2,
      AUTO_BY_POS = 3
    };

    public enum EN_TASK_GENERATOR_PROCESS
    {
      OK = 0,
      ERROR_ON_CREATE_ALARMS = 1,
      ERROR_ON_ASSIGN_ALARMS = 2,
      ERROR_ON_LOAD_ALARMS = 3
    };
    #endregion

    #region vars
    /// <summary>
    /// Multithread lock
    /// </summary>
    private static readonly object m_lock = new object();

    /// <summary>
    /// Thread Interval
    /// </summary>
    private Int32 m_interval = 5000;

    /// <summary>
    /// Thread delegate
    /// </summary>
    private delegate void ThreadWorker();
    private Thread m_thread;
    private int[] m_alarms_source;
    private DataTable m_dt_alarms_role;

    #endregion

    /// <summary>
    /// Initializes the thread with the worker
    /// </summary>
    /// <param name="work">The method to execute by the thread</param>
    private void Initialize(ThreadWorker work)
    {
      m_thread = new Thread(new ThreadStart(work));
      m_thread.Name = "LayoutData_Tasks_GEN";
      m_thread.Start();
    }

    /// <summary>
    /// General initialization
    /// </summary>
    public void Init()
    {
      this.Initialize(this.Task);

      m_alarms_source = new int[5];

      //PLayerAlarms 41
      m_alarms_source[0] = 19001;
      m_alarms_source[1] = 17001;
      m_alarms_source[2] = 17002;
      m_alarms_source[3] = 17016;
      m_alarms_source[4] = 17032;
    }

    /// <summary>
    /// Main task for the thread
    /// </summary>
    private void Task()
    {
      Stopwatch _time_control;
      Int64 _elapsed;
      DataSet _dataset;
      String _json_data = "";
      Dictionary<string, object> _dic_alarms;
      DataTable _dt_alarms;

      Debug.WriteLine("LAYOUTSYSTEMTASKGENERATOR Init");

      Logger.Write(HttpContext.Current, "Thread starting:", "TS");
      Logger.Write(HttpContext.Current, " - Wait time : " + this.m_interval + "ms", "TS");

      Thread.Sleep(this.m_interval / 5);

      while (true)
      {
        try
        {

#if !DEBUG
          if (!Services.IsPrincipal("WS_INTELLIA"))
          {
            Thread.Sleep(1000);
            continue;
          }
#endif

          _time_control = Stopwatch.StartNew();
          _dic_alarms = new Dictionary<string, object>();
          _dataset = new DataSet();
          _dt_alarms = new DataTable();

          Debug.WriteLine("clsLayoutSystemTaskGenerator Want Data");

          _json_data = Global.LayoutAlarmsData.Data;

          Debug.WriteLine(_json_data);

          if (_json_data != "")
          {

            JavaScriptSerializer jss = new JavaScriptSerializer();
            _dic_alarms = jss.Deserialize<Dictionary<string, object>>(_json_data);

            using (DB_TRX _db_trx = new DB_TRX())
            {
              using (SqlCommand _cmd = new SqlCommand("Layout_GetTodayActiveAlarms", _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
              {
                _cmd.CommandType = CommandType.StoredProcedure;
                _cmd.CommandTimeout = 2 * 60; // 2 minutes

                using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
                {
                  _sql_da.Fill(_dataset);
                  _dt_alarms = _dataset.Tables[0];
                }
              }

              if (m_dt_alarms_role == null)
              {
                Trx_LoadAlarmsRol(_db_trx.SqlTransaction);
              }
            }

            lock (m_lock)
            {
              if (_dt_alarms.Rows.Count > 0)
              {
                //comprobarlas de una en una
                if (!checkAndAddAlarms(_dic_alarms, _dt_alarms))
                {

                  throw new Exception("checkAndAddAlarms failed");
                }
              }
              else
              {
                //añadirlas todas
                if (!addAllAlarms(_dic_alarms))
                {

                  throw new Exception("addAllAlarms failed");
                }
              }
            }
          }

          _time_control.Stop();
          _elapsed = _time_control.ElapsedMilliseconds;
          Logger.Write(HttpContext.Current, "Layout_GetTerminalStatusData Elapsed time: " + _elapsed.ToString() + "ms", "TC");
          Debug.WriteLine("LAYOUTSYSTEMTASKGENERATOR FINISHED");
        }
        catch (Exception _ex)
        {
          Logger.Write(HttpContext.Current, "Thread exception:", "TS");
          Logger.Exception(HttpContext.Current, "[" + this.m_thread.Name + "]Task()", _ex);
          Debug.WriteLine("[" + this.m_thread.Name + "]Task()");
        }

        Thread.Sleep(this.m_interval);
      }
    }

    private EN_TASK_GENERATOR_PROCESS Trx_LoadAlarmsRol(SqlTransaction Trx)
    {
      DataSet _dataset;
      StringBuilder _sb;

      try
      {
        _dataset = new DataSet();
        _sb = new StringBuilder();

        _sb.Append("          SELECT LR_SECTION_ID AS [CATEGORY]                                  ");
        _sb.Append("               , LR_NAME + ' - ' + LRL_LABEL AS [ALARM_DESC]                  ");
        _sb.Append("               , CAST((LR_FIELD * 1000) + LRL_VALUE1 AS INT) AS [SUBCATEGORY] ");
        _sb.Append("               , L.LRL_AUTO_ASSIGN_ROLE AS [ROLE]                             ");
        _sb.Append("               , L.LRL_AUTO_ASSIGN_PRIORITY AS [PRIORITY]                     ");
        _sb.Append("            FROM LAYOUT_RANGES AS R                                           ");
        _sb.Append("      INNER JOIN LAYOUT_RANGES_LEGENDS  AS L                                  ");
        _sb.Append("              ON R.LR_ID = L.LRL_RANGE_ID                                     ");
        _sb.Append("           WHERE LR_SECTION_ID IN (41,42,43)                                     ");
        _sb.Append("        ORDER BY LR_SECTION_ID                                                ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.CommandTimeout = 2 * 60; // 2 minutes

          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
          {
            _sql_da.Fill(_dataset);
            m_dt_alarms_role = _dataset.Tables[0];
          }
        }
        return EN_TASK_GENERATOR_PROCESS.OK;
      }
      catch (Exception _ex)
      {

        Log.Exception(_ex);
      }

      return EN_TASK_GENERATOR_PROCESS.ERROR_ON_LOAD_ALARMS;

    }

    /// <summary>
    /// Prepare batch update to insert all generated alarms.
    /// </summary>
    /// <param name="Alarms">Dictionary with alarms list</param>
    /// <returns></returns>
    private Boolean addAllAlarms(Dictionary<string, object> Alarms)
    {
      DataTable _bach_alarms = CreateAlarmsDT();
      int[] _list;
      EN_TASK_GENERATOR_PROCESS _result;

      try
      {
        foreach (string _key in Alarms.Keys)
        {
          _list = (int[])((ArrayList)Alarms[_key]).ToArray(typeof(int));

          foreach (int _alarm_id in _list)
          {
            DataRow _dr = _bach_alarms.NewRow();

            DateTime _now = WGDB.Now;

            _dr["LSA_TERMINAL_ID"] = _key.Split('_')[0];
            _dr["LSA_ALARM_ID"] = _alarm_id;
            _dr["LSA_DATE_CREATED"] = TruncateMilliseconds(_now);
            _dr["LSA_ALARM_TYPE"] = m_alarms_source.Contains(_alarm_id) ? 41 : 42;
            _dr["LSA_ALARM_SOURCE"] = 2;
            _dr["LSA_USER_CREATED"] = -1;
            _dr["LSA_STATUS"] = 0;
            _dr["LSA_TERMINAL_FLOOR_ID"] = _key.Split('_')[1];
            _dr["LSA_TERMINAL_LOCATION_X"] = _key.Split('_')[2];
            _dr["LSA_TERMINAL_LOCATION_Y"] = _key.Split('_')[3];

            _bach_alarms.Rows.Add(_dr);
          }
        }

        _result = DB_InsertAlarms(_bach_alarms);


        switch (_result)
        {
          case EN_TASK_GENERATOR_PROCESS.ERROR_ON_ASSIGN_ALARMS:
            Log.Message("Error on AutoAssignAlarms");

            return true;

          case EN_TASK_GENERATOR_PROCESS.ERROR_ON_CREATE_ALARMS:

            Log.Message("Error on SaveAlarms");

            return false;

          case EN_TASK_GENERATOR_PROCESS.OK:

            return true;

        }

        return true;
      }
      catch (Exception _ex)
      {

        Debug.WriteLine("addAllAlarms " + _ex.Message);
      }

      return false;
    }

    /// <summary>
    /// Delete miliseconds from a DateTime.
    /// </summary>
    /// <param name="d"></param>
    /// <returns></returns>
    private DateTime TruncateMilliseconds(DateTime d)
    {

      return d.AddMilliseconds(-d.Millisecond);
    }

    /// <summary>
    /// Generate DataTable Alarms
    /// </summary>
    /// <returns>Datatable</returns>
    private DataTable CreateAlarmsDT()
    {
      DataTable _dt_alarms = new DataTable();

      _dt_alarms.Columns.Add("LSA_TERMINAL_ID", Type.GetType("System.Int32"));
      _dt_alarms.Columns.Add("LSA_ALARM_ID", Type.GetType("System.Int64"));
      _dt_alarms.Columns.Add("LSA_DATE_CREATED", Type.GetType("System.DateTime"));
      _dt_alarms.Columns.Add("LSA_ALARM_TYPE", Type.GetType("System.Int32"));
      _dt_alarms.Columns.Add("LSA_ALARM_SOURCE", Type.GetType("System.Int32"));
      _dt_alarms.Columns.Add("LSA_USER_CREATED", Type.GetType("System.Int32"));
      //_dt_alarms.Columns.Add("LSA_MEDIA_ID", Type.GetType("System.Int64")).AllowDBNull = true;
      //_dt_alarms.Columns.Add("LSA_DESCRIPTION", Type.GetType("System.Text")).AllowDBNull = true;
      //_dt_alarms.Columns.Add("LSA_DATE_TO_TASK", Type.GetType("System.DateTime")).AllowDBNull = true;
      //_dt_alarms.Columns.Add("LSA_TASK_ID", Type.GetType("System.Int64")).AllowDBNull = true;
      _dt_alarms.Columns.Add("LSA_STATUS", Type.GetType("System.Int32"));
      _dt_alarms.Columns.Add("LSA_TERMINAL_FLOOR_ID", Type.GetType("System.Int32"));
      _dt_alarms.Columns.Add("LSA_TERMINAL_LOCATION_X", Type.GetType("System.Int32"));
      _dt_alarms.Columns.Add("LSA_TERMINAL_LOCATION_Y", Type.GetType("System.Int32"));

      _dt_alarms.PrimaryKey = new DataColumn[] { _dt_alarms.Columns["LSA_TERMINAL_ID"], _dt_alarms.Columns["LSA_ALARM_ID"], _dt_alarms.Columns["LSA_DATE_CREATED"] };

      return _dt_alarms;
    }

    /// <summary>
    /// Generate DataTable Alarms
    /// </summary>
    /// <returns>Datatable</returns>
    private DataTable CreateTasksDT()
    {
      DataTable _dt_tasks = new DataTable();

      _dt_tasks.Columns.Add("LST_STATUS", Type.GetType("System.Int32"));
      _dt_tasks.Columns.Add("LST_CATEGORY", Type.GetType("System.Int32"));
      _dt_tasks.Columns.Add("LST_SUBCATEGORY", Type.GetType("System.Int32"));
      _dt_tasks.Columns.Add("LST_TERMINAL_ID", Type.GetType("System.Int32"));
      _dt_tasks.Columns.Add("LST_SEVERITY", Type.GetType("System.Int32"));
      _dt_tasks.Columns.Add("LST_CREATION_USER_ID", Type.GetType("System.Int32"));
      _dt_tasks.Columns.Add("LST_CREATION", Type.GetType("System.DateTime"));
      _dt_tasks.Columns.Add("LST_ASSIGNED_ROLE_ID", Type.GetType("System.Int32"));
      _dt_tasks.Columns.Add("LST_ASSIGNED_USER_ID", Type.GetType("System.Int32"));
      _dt_tasks.Columns.Add("LST_ASSIGNED", Type.GetType("System.DateTime"));
      _dt_tasks.Columns.Add("LST_EVENTS_HISTORY", Type.GetType("System.String"));
      _dt_tasks.Columns.Add("ALARM_DATE", Type.GetType("System.DateTime"));
      _dt_tasks.Columns.Add("TASK_ID", Type.GetType("System.Int32"));

      return _dt_tasks;
    }

    /// <summary>
    /// Save into database all the alarms and auto assign if it's necessary
    /// </summary>
    /// <param name="BachDtAlarms"></param>
    /// <returns></returns>
    private EN_TASK_GENERATOR_PROCESS DB_InsertAlarms(DataTable BachDtAlarms)
    {
      EN_TASK_GENERATOR_PROCESS _success;
      DataTable _dt_tasks_to_notify = new DataTable();
      EN_NOTIFICATION_TYPE _notification_type = EN_NOTIFICATION_TYPE.NONE;

      using (DB_TRX _db_trx = new DB_TRX())
      {
        _success = Trx_InsertAlarms(BachDtAlarms, _db_trx.SqlTransaction);

        if (_success == EN_TASK_GENERATOR_PROCESS.OK)
        {
          _notification_type = (EN_NOTIFICATION_TYPE)WSI.Common.GeneralParam.GetInt32("Intellia", "Notification.Type", 0);

          if (_notification_type != EN_NOTIFICATION_TYPE.MANUAL && _notification_type != EN_NOTIFICATION_TYPE.NONE)
          {
            _success = DB_AutoAssignAlarms(BachDtAlarms, out _dt_tasks_to_notify, _notification_type, _db_trx.SqlTransaction);

            if (_dt_tasks_to_notify.Rows.Count > 0)
            {
              if (!SendNotifications(_dt_tasks_to_notify, _notification_type, _db_trx.SqlTransaction))
              {

                Log.Message("Error sending notification during auto assign alarms");
              }
            }
          }

          _db_trx.Commit();
        }
      }

      return _success;
    }

    /// <summary>
    /// Store Alarms into layout_site_alarms
    /// </summary>
    /// <param name="BachDtAlarms"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private EN_TASK_GENERATOR_PROCESS Trx_InsertAlarms(DataTable BachDtAlarms, SqlTransaction SqlTrx)
    {
      StringBuilder _sb_insert;
      int _nr;

      try
      {
        _sb_insert = new StringBuilder();

        _sb_insert.AppendLine(" INSERT INTO   LAYOUT_SITE_ALARMS        ");
        _sb_insert.AppendLine("           (   LSA_TERMINAL_ID           ");
        _sb_insert.AppendLine("           ,   LSA_ALARM_ID              ");
        _sb_insert.AppendLine("           ,   LSA_DATE_CREATED          ");
        _sb_insert.AppendLine("           ,   LSA_ALARM_TYPE            ");
        _sb_insert.AppendLine("           ,   LSA_ALARM_SOURCE          ");
        _sb_insert.AppendLine("           ,   LSA_USER_CREATED          ");
        _sb_insert.AppendLine("           ,   LSA_STATUS       )        ");
        _sb_insert.AppendLine("      VALUES                             ");
        _sb_insert.AppendLine("           (   @pTerminalId              ");
        _sb_insert.AppendLine("           ,   @pAlarmId                 ");
        _sb_insert.AppendLine("           ,   @pDateCreated             ");
        _sb_insert.AppendLine("           ,   @pAlarmType               ");
        _sb_insert.AppendLine("           ,   @pAlarmSource             ");
        _sb_insert.AppendLine("           ,   @pUserCreated             ");
        _sb_insert.AppendLine("           ,   @pStatus          )       ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb_insert.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "LSA_TERMINAL_ID";
          _sql_cmd.Parameters.Add("@pAlarmId", SqlDbType.BigInt).SourceColumn = "LSA_ALARM_ID";
          _sql_cmd.Parameters.Add("@pDateCreated", SqlDbType.DateTime).SourceColumn = "LSA_DATE_CREATED";
          _sql_cmd.Parameters.Add("@pAlarmType", SqlDbType.Int).SourceColumn = "LSA_ALARM_TYPE";
          _sql_cmd.Parameters.Add("@pAlarmSource", SqlDbType.Int).SourceColumn = "LSA_ALARM_SOURCE";
          _sql_cmd.Parameters.Add("@pUserCreated", SqlDbType.Int).SourceColumn = "LSA_USER_CREATED";
          _sql_cmd.Parameters.Add("@pStatus", SqlDbType.Int).SourceColumn = "LSA_STATUS";

          using (SqlDataAdapter _sql_da = new SqlDataAdapter())
          {
            _sql_da.InsertCommand = _sql_cmd;

            _nr = _sql_da.Update(BachDtAlarms);

            if (_nr == BachDtAlarms.Rows.Count)
            {
              return EN_TASK_GENERATOR_PROCESS.OK;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        //Logger.Write(_ex.Message);
        Debug.WriteLine("Trx_Insert_Failed " + _ex.Message);
      }

      return EN_TASK_GENERATOR_PROCESS.ERROR_ON_CREATE_ALARMS;
    }

    /// <summary>
    /// Send MQTT Notifications
    /// </summary>
    /// <param name="BatchDtAlarms"></param>
    /// <param name="NotificationType"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    private Boolean SendNotifications(DataTable BatchDtAlarms, EN_NOTIFICATION_TYPE NotificationType, SqlTransaction Trx)
    {
      int _role, _subcategory;
      long _item_id;
      string _user_id;

      try
      {
        foreach (DataRow _dr in BatchDtAlarms.Rows)
        {
          switch (NotificationType)
          {
            case EN_NOTIFICATION_TYPE.AUTO_BY_ROLE:

              _role = (int)_dr["LST_ASSIGNED_ROLE_ID"];
              _subcategory = (int)_dr["LST_SUBCATEGORY"];
              _item_id = (int)_dr["TASK_ID"];

              if (_item_id > 0)
              {
                if (!clsNotifications.SendNotificationByRole(_role, _subcategory, String.Empty, _item_id, Trx))
                {

                  Log.Message("Autonotification Error sending ");
                }
              }
              break;

            case EN_NOTIFICATION_TYPE.AUTO_BY_POS:

              _user_id = _dr["LST_ASSIGNED_USER_ID"].ToString();
              _subcategory = (int)_dr["LST_SUBCATEGORY"];
              _item_id = (int)_dr["TASK_ID"];

              if (_item_id > 0)
              {
                if (!clsNotifications.SendNotificationByUser(_user_id, _subcategory, String.Empty, _item_id))
                {

                  Log.Message("Autonotification Error sending ");
                }
              }
              break;

            default:

              break;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="Alarms"></param>
    /// <param name="DtWorkingDayAlarms"></param>
    /// <returns></returns>
    private Boolean checkAndAddAlarms(Dictionary<string, object> Alarms, DataTable DtWorkingDayAlarms)
    {
      int[] _list;
      DataTable _bach_alarms = CreateAlarmsDT();
      string _terminal_id;

      foreach (string _key in Alarms.Keys)
      {
        _list = (int[])((ArrayList)Alarms[_key]).ToArray(typeof(int));

        foreach (int _alarm_id in _list)
        {
          _terminal_id = _key.Split('_')[0];
          DataRow[] result = DtWorkingDayAlarms.Select("LSA_TERMINAL_ID = " + _terminal_id + " AND LSA_ALARM_ID = " + _alarm_id);

          if (result.Length == 0)
          {
            DataRow _dr = _bach_alarms.NewRow();
            DateTime _now = WGDB.Now;

            _dr["LSA_TERMINAL_ID"] = Int32.Parse(_terminal_id);
            _dr["LSA_ALARM_ID"] = _alarm_id;
            _dr["LSA_DATE_CREATED"] = TruncateMilliseconds(_now);
            _dr["LSA_ALARM_TYPE"] = m_alarms_source.Contains(_alarm_id) ? 41 : 42; //MACHINE - PLAYER
            _dr["LSA_ALARM_SOURCE"] = 2;
            _dr["LSA_USER_CREATED"] = -1;
            //_dr["LSA_MEDIA_ID"]
            //_dr["LSA_DESCRIPTION"]
            //_dr["LSA_DATE_TO_TASK"]
            //_dr["LSA_TASK_ID"]
            _dr["LSA_STATUS"] = 0;
            _dr["LSA_TERMINAL_FLOOR_ID"] = _key.Split('_')[1];
            _dr["LSA_TERMINAL_LOCATION_X"] = _key.Split('_')[2];
            _dr["LSA_TERMINAL_LOCATION_Y"] = _key.Split('_')[3];

            _bach_alarms.Rows.Add(_dr);
          }
        }
      }
      if (_bach_alarms.Rows.Count > 0)
      {
        if (DB_InsertAlarms(_bach_alarms) != EN_TASK_GENERATOR_PROCESS.OK)
        {

          return false;
        }
      }

      return true;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="BachDtAlarms"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private EN_TASK_GENERATOR_PROCESS DB_AutoAssignAlarms(DataTable BachDtAlarms, out DataTable BatchDtTasks, EN_NOTIFICATION_TYPE NotificationType, SqlTransaction SqlTrx)
    {
      int _nr;
      DataTable _dt_tasks;
      DataTable _dt_runners;
      DateTime _now;

      BatchDtTasks = new DataTable();

      try
      {
        _dt_tasks = CreateTasksDT();
        _dt_runners = GetRunnersInfo(SqlTrx);

        switch (NotificationType)
        {
          case EN_NOTIFICATION_TYPE.AUTO_BY_POS:

            //NOTIFICATION TYPE --> AUTO BY POS
            if (_dt_runners == null || _dt_runners.Rows.Count == 0)
            {

              return EN_TASK_GENERATOR_PROCESS.ERROR_ON_ASSIGN_ALARMS;
            }

            foreach (DataRow _dr in BachDtAlarms.Rows)
            {
              DataRow[] _alarm_info = m_dt_alarms_role.Select("SUBCATEGORY = " + _dr["LSA_ALARM_ID"].ToString());

              if (_alarm_info.Length == 0)
              {

                continue;
              }

              DataRow _dr_task = _dt_tasks.NewRow();
              _now = WGDB.Now;

              _dr_task["LST_STATUS"] = 1; //PENDING
              _dr_task["LST_CATEGORY"] = (Int32)_alarm_info[0]["CATEGORY"];
              _dr_task["LST_SUBCATEGORY"] = (Int32)_alarm_info[0]["SUBCATEGORY"];
              _dr_task["LST_TERMINAL_ID"] = (Int32)_dr[0];
              _dr_task["LST_SEVERITY"] = (Int32)_alarm_info[0]["PRIORITY"] == 0 ? 3 : (Int32)_alarm_info[0]["PRIORITY"];
              _dr_task["LST_CREATION_USER_ID"] = -1;
              _dr_task["LST_CREATION"] = TruncateMilliseconds(_now);
              _dr_task["LST_ASSIGNED_USER_ID"] = GetNearestRunner(_dt_runners, _dr);
              _dr_task["LST_ASSIGNED"] = TruncateMilliseconds(_now);
              _dr_task["LST_EVENTS_HISTORY"] = "<div class='taskHistory'><i>" + TruncateMilliseconds(_now).ToString() + "</i>  SYSTEM  <span data-NLS='NLS_AUTO_NOTIFICATION_ALARM'>Task Created</span></div>";
              _dr_task["ALARM_DATE"] = TruncateMilliseconds((DateTime)_dr["LSA_DATE_CREATED"]);
              _dr_task["TASK_ID"] = -1;

              _dt_tasks.Rows.Add(_dr_task);
            }
            break;

          case EN_NOTIFICATION_TYPE.AUTO_BY_ROLE:

            //NOTIFICATION TYPE --> AUTO BY ROLE
            foreach (DataRow _dr in BachDtAlarms.Rows)
            {
              DataRow[] _alarm_info = m_dt_alarms_role.Select("SUBCATEGORY = " + _dr["LSA_ALARM_ID"].ToString());

              if (_alarm_info.Length == 0)
              {

                continue;
              }

              DataRow _dr_task = _dt_tasks.NewRow();
              _now = WGDB.Now;

              _dr_task["LST_STATUS"] = 1; //PENDING
              _dr_task["LST_CATEGORY"] = (Int32)_alarm_info[0]["CATEGORY"];
              _dr_task["LST_SUBCATEGORY"] = (Int32)_alarm_info[0]["SUBCATEGORY"];
              _dr_task["LST_TERMINAL_ID"] = (Int32)_dr[0];
              _dr_task["LST_SEVERITY"] = (Int32)_alarm_info[0]["PRIORITY"] == 0 ? 3 : (Int32)_alarm_info[0]["PRIORITY"];
              _dr_task["LST_CREATION_USER_ID"] = -1;
              _dr_task["LST_CREATION"] = TruncateMilliseconds(_now);
              _dr_task["LST_ASSIGNED_ROLE_ID"] = (Int32)_alarm_info[0]["ROLE"];
              _dr_task["LST_ASSIGNED"] = TruncateMilliseconds(_now);
              _dr_task["LST_EVENTS_HISTORY"] = "<div class='taskHistory'><i>" + TruncateMilliseconds(_now).ToString() + "</i>  SYSTEM  <span data-NLS='NLS_AUTO_NOTIFICATION_ALARM'>Task Created</span></div>";
              _dr_task["ALARM_DATE"] = TruncateMilliseconds((DateTime)_dr["LSA_DATE_CREATED"]);
              _dr_task["TASK_ID"] = -1;

              _dt_tasks.Rows.Add(_dr_task);
            }
            break;

          default:

            break;
        }

        if (_dt_tasks.Rows.Count > 0)
        {
          using (SqlCommand _sql_cmd = new SqlCommand("Layout_AutoNotificationAlarm_TEST", SqlTrx.Connection, SqlTrx))
          {
            _sql_cmd.CommandType = CommandType.StoredProcedure;

            _sql_cmd.Parameters.Add("@pStatus", SqlDbType.Int).SourceColumn = "LST_STATUS";
            _sql_cmd.Parameters.Add("@pCategory", SqlDbType.Int).SourceColumn = "LST_CATEGORY";
            _sql_cmd.Parameters.Add("@pSubcategory", SqlDbType.Int).SourceColumn = "LST_SUBCATEGORY";
            _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "LST_TERMINAL_ID";
            _sql_cmd.Parameters.Add("@pSeverity", SqlDbType.Int).SourceColumn = "LST_SEVERITY";
            _sql_cmd.Parameters.Add("@pCreationUserId", SqlDbType.Int).SourceColumn = "LST_CREATION_USER_ID";
            _sql_cmd.Parameters.Add("@pCreation", SqlDbType.DateTime).SourceColumn = "LST_CREATION";
            _sql_cmd.Parameters.Add("@pAssignedRoleId", SqlDbType.Int).SourceColumn = "LST_ASSIGNED_ROLE_ID";
            _sql_cmd.Parameters.Add("@pAssignedUserId", SqlDbType.Int).SourceColumn = "LST_ASSIGNED_USER_ID";
            _sql_cmd.Parameters.Add("@pAssigned", SqlDbType.DateTime).SourceColumn = "LST_ASSIGNED";
            _sql_cmd.Parameters.Add("@pHistory", SqlDbType.Text).SourceColumn = "LST_EVENTS_HISTORY";
            _sql_cmd.Parameters.Add("@pDateCreated", SqlDbType.DateTime).SourceColumn = "ALARM_DATE";

            SqlParameter _output_param = _sql_cmd.Parameters.Add("@poutTaskId", SqlDbType.Int);
            _output_param.SourceColumn = "TASK_ID";
            _output_param.Direction = ParameterDirection.Output;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter())
            {
              _sql_da.InsertCommand = _sql_cmd;

              _nr = _sql_da.Update(_dt_tasks);

              if (_nr == _dt_tasks.Rows.Count)
              {
                BatchDtTasks = _dt_tasks;
                return EN_TASK_GENERATOR_PROCESS.OK;
              }
            }
          }
        }

        return EN_TASK_GENERATOR_PROCESS.OK;
      }
      catch (Exception _ex)
      {
        //Logger.Write(_ex.Message);
        Debug.WriteLine("Trx_Insert_Failed " + _ex.Message);
      }

      return EN_TASK_GENERATOR_PROCESS.ERROR_ON_CREATE_ALARMS;
    }

    /// <summary>
    /// Return user id from the nearest user between beacon.
    /// </summary>
    /// <param name="_dt_runners">Runners List</param>
    /// <param name="AlarmInfo">Alarm Information</param>
    /// <returns></returns>
    private Int32 GetNearestRunner(DataTable _dt_runners, DataRow AlarmInfo)
    {
      Int32 _t_pos_x, _t_pos_y, _result, _nearest_point, _current_point;
      DataRow[] _runners_current_floor = _dt_runners.Select("LRP_FLOOR_ID = " + AlarmInfo["LSA_TERMINAL_FLOOR_ID"].ToString());

      _result = 0;

      try
      {
        _nearest_point = -1;

        _t_pos_x = (Int32)AlarmInfo["LSA_TERMINAL_LOCATION_X"];
        _t_pos_y = (Int32)AlarmInfo["LSA_TERMINAL_LOCATION_Y"];

        foreach (DataRow _dr in _runners_current_floor)
        {
          _current_point = GetDistancePoints(_t_pos_x, _t_pos_y, Int32.Parse(_dr["LRP_X"].ToString()), Int32.Parse(_dr["LRP_Y"].ToString()));

          if (_nearest_point == -1)
          {
            _nearest_point = _current_point;
          }

          if (_nearest_point > _current_point)
          {
            _nearest_point = _current_point;
            _result = (Int32)_dr["GU_USER_ID"];
          }
        }
      }
      catch (Exception _ex)
      {

        Debug.WriteLine("GetNearestRunner" + _ex.Message);
      }

      return _result;
    }

    /// <summary>
    /// Get the distance between two points using Pythagoras Theorem
    /// </summary>
    /// <param name="TerminalPosX"></param>
    /// <param name="TerminalPosY"></param>
    /// <param name="RunnerPosX"></param>
    /// <param name="RunnerPosY"></param>
    /// <returns></returns>
    private Int32 GetDistancePoints(Int32 TerminalPosX, Int32 TerminalPosY, Int32 RunnerPosX, Int32 RunnerPosY)
    {

      Int32 _diff_x, _diff_y;

      _diff_x = Math.Abs(TerminalPosX - RunnerPosX);
      _diff_y = Math.Abs(TerminalPosY - RunnerPosY);

      //Pythagoras
      return (int)Math.Sqrt(_diff_x * _diff_x + _diff_y * _diff_y);
    }

    /// <summary>
    /// Returns runners positions info
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private DataTable GetRunnersInfo(SqlTransaction SqlTrx)
    {
      DataSet _ds_runners_info = new DataSet();
      try
      {
        using (SqlCommand _sql_cmd = new SqlCommand("Layout_GetRunnersPositions", SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.CommandType = CommandType.StoredProcedure;
          _sql_cmd.CommandTimeout = 2 * 60;

          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
          {
            _sql_da.Fill(_ds_runners_info);
          }
        }


        return _ds_runners_info.Tables[0];
      }
      catch (Exception _ex)
      {

        Debug.WriteLine("GetRunnersInfo" + _ex.Message);
      }

      return null;
    }

    /// <summary>
    /// Default Constructor
    /// </summary>
    public clsLayoutSystemTaskGenerator()
      : this(5000)
    {
    }

    /// <summary>
    /// Constructor for the class
    /// </summary>
    /// <param name="Interval">Thread delay</param>
    public clsLayoutSystemTaskGenerator(Int32 Interval)
    {
      this.m_interval = Interval;
    }
  }
}

