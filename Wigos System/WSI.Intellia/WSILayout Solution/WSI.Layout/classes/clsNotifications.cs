﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Configuration;
using WSI.Common;
using System.Data.SqlClient;
using System.Web.Services;
using System.Web.Script.Services;
using Newtonsoft;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Text;
using System.IO;
using WSI.Layout.classes;


namespace WSI.Layout.classes
{
  public static class clsNotifications
  {
    public static IEnumerable<String> GetUserByRole(int roleId, SqlTransaction sqlTransaction)
    {
      String _device_id = string.Empty;
      List<String> _devices = new List<String>();
      try
      {
        SqlDataAdapter _adapter;
        DataTable _data;
        _data = new DataTable();

        using (SqlCommand _cmd = new SqlCommand("Layout_GetAssignedDevicesByRoleId", sqlTransaction.Connection, sqlTransaction))
        {
          _cmd.CommandType = CommandType.StoredProcedure;
          _cmd.Parameters.Add("@pRoleId", SqlDbType.BigInt).Value = roleId;
          _adapter = new SqlDataAdapter(_cmd);
          _adapter.Fill(_data);
        }

        foreach (DataRow row in _data.Rows)
        {
          _devices.Add(row["GU_USER_ID"].ToString());
        }

      }
      catch (Exception _ex)
      {
        Logger.Exception(HttpContext.Current, "GetUserAssignedDevice()", _ex);
      }

      return _devices;
    }

    public static Boolean SendNotificationByRole(Int32 Role, int SubcategoryId, String Message, long ItemID, SqlTransaction Trx)
    {
      try
      {
        var deviceUsersByRole = GetUserByRole(Role, Trx);

        foreach (var device in deviceUsersByRole)
        {
          if (!MqttClientsManager.Default.SendNewTaskAsigned(device, SubcategoryId, Message, ItemID))
          {
            Logger.Write(HttpContext.Current, "MqttMessageSender.Instance.SendNewTaskAsigned() failed|| ");
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Message("Error SendNotificationByRole");
        Log.Exception(_ex);
      }

      return false;
    }

    public static Boolean SendNotificationByRole(Int32 Role, int SubcategoryId, String Message, long ItemID)
    {
      Boolean _result;

      using (DB_TRX _db_trx = new DB_TRX())
      {
        _result = SendNotificationByRole(Role, SubcategoryId, Message, ItemID, _db_trx.SqlTransaction);
      }

      return _result;
    }

    public static Boolean SendNotificationByUser(String userId, int subCategoryId, String message, long itemId)
    {
      Boolean _result = true;

      if (!MqttClientsManager.Default.SendNewTaskAsigned(userId, subCategoryId, message, itemId))
      {
        _result = false;

        Logger.Write(HttpContext.Current, "MqttMessageSender.Instance.SendNewTaskAsigned() failed|| ");
      }

      return _result;
    }
  }
}