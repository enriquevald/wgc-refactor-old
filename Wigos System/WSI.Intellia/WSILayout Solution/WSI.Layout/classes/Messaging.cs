﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using WSI.Common;

namespace WSI.Layout.classes
{
  public class Messaging
  {
    public enum MessageSource {
      MANAGER = 1,
      RUNNER = 2
    }
    public class Message
    {
      public Message(DataRow _row)
      {
        if (!string.IsNullOrEmpty(_row["lmrm_id"].ToString()))
          this.Id = Int32.Parse(_row["lmrm_id"].ToString());
        if (!string.IsNullOrEmpty(_row["lmrm_manager_id"].ToString()))
          this.ManagerId = Int64.Parse(_row["lmrm_manager_id"].ToString());
        if (!string.IsNullOrEmpty(_row["lmrm_runner_id"].ToString()))
          this.RunnerId = Int64.Parse(_row["lmrm_runner_id"].ToString());
        if (!string.IsNullOrEmpty(_row["lmrm_message"].ToString()))
          this.Content = _row["lmrm_message"].ToString();
        if (!string.IsNullOrEmpty(_row["manager_name"].ToString()))
          this.ManagerUserName = _row["manager_name"].ToString();
        if (!string.IsNullOrEmpty(_row["runner_name"].ToString()))
          this.RunnerUserName = _row["runner_name"].ToString();
        if (!string.IsNullOrEmpty(_row["lmrm_date"].ToString()))
          this.Date = _row["lmrm_date"].ToString();
        if (!string.IsNullOrEmpty(_row["lmrm_source"].ToString()))
          this.Source = Int32.Parse(_row["lmrm_source"].ToString());
        if (!string.IsNullOrEmpty(_row["lmrm_status"].ToString()))
          this.Status = (MessageStatus)Int32.Parse(_row["lmrm_status"].ToString());
      }

      public Message()
      {
        // TODO: Complete member initialization
      }
      public Int32 Id { get; set; }
      public String Date { get; set; }
      public Int64 ManagerId { get; set; }
      public String ManagerUserName { get; set; }
      public Int64 RunnerId { get; set; }
      public String RunnerUserName { get; set; }
      public String Content { get; set; }
      public Int32 Source { get; set; }
      public MessageStatus Status { get; set; }
    }

    public enum MessageStatus
    {
      NOTDEFINED = 0,
      DELAYED = 1,
      DELIVERED = 2,
      HASREAD = 3,
      SENT = 4
    }
    public static String GetConversationHistory(int managerId, int runnerId)
    {
      try
      {
        SqlDataAdapter _adapter;
        DataTable _data;

        _data = new DataTable();
        if(runnerId == 0)
          return "{\"Messages\":{}}";

        using (DB_TRX _transaction = new DB_TRX())
        {
          using (SqlCommand _command = new SqlCommand("Layout_GetConversationHistory", _transaction.SqlTransaction.Connection, _transaction.SqlTransaction))
          {
            _command.CommandType = CommandType.StoredProcedure;
            _command.Parameters.Add("@pManagerId", SqlDbType.BigInt).Value = managerId;
            _command.Parameters.Add("@pRunnerId", SqlDbType.BigInt).Value = runnerId;
            _adapter = new SqlDataAdapter(_command);
            _adapter.Fill(_data);
          }
        }

        List<Message> _messages = new List<Message>();

        foreach (DataRow _row in _data.Rows)
        {
          _messages.Add(new Message(_row));
        }

        UpdateMessageStatusToRead(_messages, managerId, runnerId);

        JavaScriptSerializer jss = new JavaScriptSerializer();

        return "{\"Messages\":"+jss.Serialize(_messages)+"}";
      }
      catch (Exception _ex)
      {
        Logger.Exception(HttpContext.Current, "SendMessageToRunner()", _ex);
        return "{\"Messages\":{}}";
      }
    }
    private static void UpdateMessageStatusToRead(List<Message> messages,int managerId, int runnerId)
    {
      var _messageToUpdate = messages.Where(x => x.Status != MessageStatus.HASREAD).Select(x => x.Id).ToList();

      //actualizar estado
      if (_messageToUpdate.Any())
      {
        var _messageIdString = string.Join<int>(", ", _messageToUpdate);
        var _queryString = "UPDATE LAYOUT_MANAGER_RUNNER_MESSAGES SET LMRM_STATUS=@messageStatus WHERE LMRM_ID IN (" + _messageIdString + ")";
        using (DB_TRX _transaction = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_queryString, _transaction.SqlTransaction.Connection, _transaction.SqlTransaction))
          {
            _cmd.Parameters.Add("@messageStatus", SqlDbType.Int).Value = (int)MessageStatus.HASREAD;
            _cmd.CommandType = CommandType.Text;

            _cmd.ExecuteNonQuery();
          }
          _transaction.Commit();
        }
      }

      var client = MqttClientsManager.getInstance(managerId.ToString());
      client.SendMessageStatusChanged(managerId.ToString(), runnerId.ToString());

      
    }
    public static void UpdateMessageStatusToDelivered(int messageId)
    {
      //actualizar estado
      if (messageId > 0)
      {
        var _queryString = "UPDATE LAYOUT_MANAGER_RUNNER_MESSAGES SET LMRM_STATUS=@messageStatus WHERE LMRM_ID = @messageId";
        using (DB_TRX _transaction = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_queryString, _transaction.SqlTransaction.Connection, _transaction.SqlTransaction))
          {
            _cmd.Parameters.Add("@messageStatus", SqlDbType.Int).Value = (int)MessageStatus.DELIVERED;
            _cmd.Parameters.Add("@messageId", SqlDbType.Int).Value = messageId;
            _cmd.CommandType = CommandType.Text;
            
            _cmd.ExecuteNonQuery();
          }
          _transaction.Commit();
        }
      }
      //enviar notificacion
      //Messaging.Message messageToSend = new Messaging.Message();
      //messageToSend.ManagerId = Int64.Parse(clsJsonUtils.GetPropertyValue(message, "managerId", "-1"));
      //messageToSend.RunnerId = Int64.Parse(clsJsonUtils.GetPropertyValue(message, "runnerId", "-1"));
      //messageToSend.RunnerUserName = clsJsonUtils.GetPropertyValue(message, "runnerUserName", "");
      //messageToSend.ManagerUserName = clsJsonUtils.GetPropertyValue(message, "managerUserName", "");
      //messageToSend.Content = clsJsonUtils.GetPropertyValue(message, "message", string.Empty);
      //messageToSend.Source = (int)Messaging.MessageSource.MANAGER;
      //SendMessageToRunner(messageToSend);


    }
    public static String SendMessageToRunner(Message data)
    {
        try
        {
            using (DB_TRX _db_trx = new DB_TRX())
            {
                var client = MqttClientsManager.getInstance(data.ManagerId.ToString());
                var message = SendMessageToRunner(data, _db_trx.SqlTransaction);
                if (!String.IsNullOrEmpty(message))
                {
                    _db_trx.Commit();

                    client.SendNewChatMessage(data);

                    return message;
                }
            }

        }
        catch (TypeInitializationException e)
        {
            Logger.Exception(HttpContext.Current, "SendMessageToRunner()" + e.InnerException.Message, e.InnerException);
        }
        return string.Empty;
    }

    private static string SendMessageToRunner(Message data, System.Data.SqlClient.SqlTransaction sqlTransaction)
    {
      try
      {
        SqlDataAdapter _adapter;
        DataTable _data;

        _data = new DataTable();

        using (SqlCommand _cmd = new SqlCommand("Layout_SaveManagerRunnerMessage", sqlTransaction.Connection, sqlTransaction))
        {
            SqlParameter outputIdParam = new SqlParameter("@new_identity", SqlDbType.Int)
            {
                Direction = ParameterDirection.Output
            };

            _cmd.CommandType = CommandType.StoredProcedure;
            _cmd.Parameters.Add("@pManagerId", SqlDbType.BigInt).Value = data.ManagerId;
            _cmd.Parameters.Add("@pRunnerId", SqlDbType.BigInt).Value = data.RunnerId;
            _cmd.Parameters.Add("@pMessage", SqlDbType.Text).Value = data.Content;
            _cmd.Parameters.Add("@pSource", SqlDbType.Int).Value = data.Source;
            _cmd.Parameters.Add(outputIdParam);
            _cmd.ExecuteNonQuery();
        }

        JavaScriptSerializer jss = new JavaScriptSerializer();

        return jss.Serialize(data);
      }
      catch (Exception _ex)
      {
        Logger.Exception(HttpContext.Current, "SendMessageToRunner()", _ex);
        return string.Empty;
      }
    }
  }


}