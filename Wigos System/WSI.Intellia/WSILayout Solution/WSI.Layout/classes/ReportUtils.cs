﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using WSI.Common;
using System.Web.Services;
using System.Web.Script.Services;
using WSI.Layout;

namespace WSI.Layout.classes
{

  public static class ReportUtils
  {

    public static String GetLayoutReports(String ReportName, String ReportIndex, String Concept, String Extra, String Date, String DateGroup)
    {
      return GetLayoutReports(ReportName, ReportIndex, Concept, Extra, Date, DateGroup, "");
    }

    // Executes the specified report calling the right stored procedure
    public static String GetLayoutReports(String ReportName, String ReportIndex, String Concept, String Extra, String Date, String DateGroup, String Pivot)
    {
      String _log_string = String.Empty;
      Int32 _log_rows = 0;

      Int64 _time = DateTime.Now.Ticks;

      String _table_string = String.Empty;
      String _result = String.Empty;
      DataSet _reports_data = new DataSet();

      List<String> _tables = new List<String>();

      Int32 _report_index;
      Int32 _concept;
      Int32 _extra;
      DateTime _date;
      Int32 _date_group;

      try
      {
        _log_string += "Start: " + DateTime.Now.ToString() + " Report:" + ReportName + " Index:" + ReportIndex.ToString() + " ";

        Int32.TryParse(ReportIndex, out _report_index);
        Int32.TryParse(Concept, out _concept);
        Int32.TryParse(Extra, out _extra);
        DateTime.TryParse(Date, out _date);
        Int32.TryParse(DateGroup, out _date_group);

        using (DB_TRX _db_trx = new DB_TRX())
        {
          //using (SqlCommand _cmd = new SqlCommand("Layout_Reports", _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          using (SqlCommand _cmd = new SqlCommand("Layout_Reports_" + ReportName, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {

            _cmd.Parameters.Add("@pReport", SqlDbType.Int).Value = _report_index;
            _cmd.Parameters.Add("@pConcept", SqlDbType.Int).Value = _concept;
            _cmd.Parameters.Add("@pExtra", SqlDbType.Int).Value = _extra;
            _cmd.Parameters.Add("@pDate", SqlDbType.DateTime).Value = _date;
            _cmd.Parameters.Add("@pDateGroups", SqlDbType.Int).Value = _date_group;

            //----------------------------------------
            _cmd.CommandType = CommandType.StoredProcedure;
            _cmd.CommandTimeout = 2 * 60; // 2 minutes

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
            {
              _sql_da.Fill(_reports_data);
            }

            for (Int32 _i = 0; _i < _reports_data.Tables.Count; _i++)
            {
              _log_rows += _reports_data.Tables[_i].Rows.Count;

              _table_string = clsDefaultConverter.DataTableToReport(_reports_data.Tables[_i]);

              if (_table_string != String.Empty)
              {
                _tables.Add(_table_string);
              }
              //break;
            }

            _result = String.Join(",", _tables.ToArray());
          }
        }
      }

      catch (Exception _ex)
      {
        Logger.Write(HttpContext.Current, "GetLayoutReports() | " + _log_string);
        Logger.Exception(HttpContext.Current, "GetLayoutReports()", _ex);
      }

      _log_string += "Rows: " + _log_rows.ToString() + " End: " + DateTime.Now.ToString();
      Logger.Write(HttpContext.Current, "GetLayoutReports() | " + _log_string);

      return "[" + _result + "]";
    }
  }

}