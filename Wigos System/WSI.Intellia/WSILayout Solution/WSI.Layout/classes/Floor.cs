﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using WSI.Common;
using System.Data;
using System.Data.SqlClient;

namespace WSI.Layout.classes
{

  /// <summary>
  /// A class to embed the data of a floor definition
  /// </summary>
  public class FloorData
  {
    public Int32 id = 0;
    public String name = String.Empty;
    public String image_map = String.Empty;
    public Double Unit = 0;
    public String color = "White";
    public Double width = 0;
    public Double height = 0;

    public String Locations = String.Empty; // Locations JSON

    /// <summary>
    /// Check if floor data is correct
    /// </summary>
    /// <returns>True/False</returns>
    public Boolean Check()
    {
      return ((this.Unit >= 0) && (this.Unit <= 2))
          && (this.color != "")
          && (this.image_map != "")
          && (this.name != "")
          && (this.width > 0)
          && (this.height > 0);
    }
  }

  /// <summary>
  /// A static class that contains the methods to manage the layout floor definitions
  /// </summary>
  public static class Floor
  {

    /// <summary>
    /// Load the list of available floor definitions
    /// </summary>
    /// <returns>The created floor id. (-1 when a problem occurs)</returns>
    public static String GetLayoutFloors()
    {
      String _result = String.Empty;
      DataSet _floors = new DataSet();

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand("Layout_GetFloors", _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.CommandType = CommandType.StoredProcedure;
            _cmd.CommandTimeout = 2 * 60; // 2 minutes

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
            {
              _sql_da.Fill(_floors);
            }

            _result = clsDefaultConverter.DataTableToReport(_floors.Tables[0]);
          }
        }
      }
      catch (Exception _ex)
      {
        Logger.Exception(HttpContext.Current, "GetLayoutFloors", _ex);
      }

      return _result;
    }

    /// <summary>
    /// Save or Update a layout floor definition
    /// </summary>
    /// <param name="Data"></param>
    /// <param name="trx"></param>
    /// <returns></returns>
    public static Int32 SaveLayoutFloor(FloorData Data, DB_TRX trx)
    {
      Object _obj;
      Int32 _result = -1;

      //using (SqlCommand _cmd = new SqlCommand("Layout_SaveFloor", trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
      using (SqlCommand _cmd = new SqlCommand("Layout_SaveFloor", trx.SqlTransaction.Connection, trx.SqlTransaction))
      {
        _cmd.CommandType = CommandType.StoredProcedure;
        _cmd.CommandTimeout = 2 * 60; // 2 minutes

        //_cmd.Parameters.Add("@pId", SqlDbType.Int).Value = (Int32)Data.Id;
        _cmd.Parameters.Add("@pGeometry", SqlDbType.VarChar, -1).Value = String.Empty;
        _cmd.Parameters.Add("@pImageMap", SqlDbType.VarChar, -1).Value = Data.image_map;
        _cmd.Parameters.Add("@pColor", SqlDbType.VarChar, 20).Value = Data.color;
        _cmd.Parameters.Add("@pName", SqlDbType.VarChar, 50).Value = Data.name;
        _cmd.Parameters.Add("@pUnit", SqlDbType.Int).Value = (Int32)Data.Unit;
        _cmd.Parameters.Add("@pWidth", SqlDbType.Int).Value = (Int32)Data.width;
        _cmd.Parameters.Add("@pHeight", SqlDbType.Int).Value = (Int32)Data.height;

        _obj = _cmd.ExecuteScalar();

        if (_obj != null && _obj != DBNull.Value)
        {
          // Obtain new floor ID
          _result = (Int32)_obj;

          Logger.Write(HttpContext.Current, "Created/Updated new floor [" + Data.name + "]", "LOG");
        }
      }
      return _result;
    }


    /// <summary>
    /// Save or Update a layout floor definition
    /// </summary>
    /// <param name="Data">See Floor.FloorData class</param>
    /// <returns>The created floor id. (-1 when a problem occurs)</returns>
    public static Int32 SaveLayoutFloor(FloorData Data)
    {
      Int32 _result = -1;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _result = SaveLayoutFloor(Data, _db_trx);

          if (_result != -1) { _db_trx.Commit(); }
        }
      }
      catch (Exception _ex)
      {
        Logger.Exception(HttpContext.Current, "SaveLayoutFloor", _ex);
      }
      return _result;
    }

    public static String CreateFloorLocations(FloorData Data, DB_TRX trx)
    {
      String _result = String.Empty;
      DataSet _locations = new DataSet();

      try
      {
        using (SqlCommand _cmd = new SqlCommand("Layout_CreateFloorLocations", trx.SqlTransaction.Connection, trx.SqlTransaction))
        {
          _cmd.CommandType = CommandType.StoredProcedure;
          _cmd.CommandTimeout = 2 * 60; // 2 minutes

          _cmd.Parameters.Add("@pFloorId", SqlDbType.Int).Value = (Int32)Data.id;
          _cmd.Parameters.Add("@pWidth", SqlDbType.Int).Value = (Int32)Data.width;
          _cmd.Parameters.Add("@pHeight", SqlDbType.Int).Value = (Int32)Data.height;
          // TODO: Review if there must be a grid for each type of object
          _cmd.Parameters.Add("@pType", SqlDbType.Int).Value = (Int32)1;

          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
          {
            _sql_da.Fill(_locations);
          }

          _result = clsDefaultConverter.DataTableToReport(_locations.Tables[0]);
        }
      }
      catch (Exception _ex)
      {
        Logger.Exception(HttpContext.Current, "CreateFloorLocations", _ex);
      }

      return _result;
    }

    public static String CreateFloorLocations(FloorData Data)
    {
      String _result = String.Empty;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _result = CreateFloorLocations(Data, _db_trx);

          if (_result != String.Empty) { _db_trx.Commit(); }
        }
      }
      catch (Exception _ex)
      {
        Logger.Exception(HttpContext.Current, "CreateFloorLocations", _ex);
      }
      return _result;
    }

    public static bool UpdateLocationName(long LocationId, string Name)
    {
      Int32 _count;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          //using (SqlCommand _cmd = new SqlCommand("Layout_SaveFloor", trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          using (SqlCommand _cmd = new SqlCommand("Layout_UpdateLocationName", _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.CommandType = CommandType.StoredProcedure;
            _cmd.CommandTimeout = 2 * 60; // 2 minutes

            //_cmd.Parameters.Add("@pId", SqlDbType.Int).Value = (Int32)Data.Id;
            _cmd.Parameters.Add("@pId", SqlDbType.VarChar, -1).Value = LocationId;
            _cmd.Parameters.Add("@pGridId", SqlDbType.VarChar, -1).Value = Name;

            _count = _cmd.ExecuteNonQuery();

            if (_count<1)
            {
              Logger.Write(HttpContext.Current, "", "LOG");
            }
            _db_trx.Commit();
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Logger.Write(HttpContext.Current, _ex.Message, "LOG");
      }

      return false;
    }

  }
}