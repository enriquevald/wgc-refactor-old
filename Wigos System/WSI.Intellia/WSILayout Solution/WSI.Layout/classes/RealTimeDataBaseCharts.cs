﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Configuration;
using WSI.Common;
using System.Data.SqlClient;
using System.Web.Services;
using System.Web.Script.Services;
using Newtonsoft;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Text;
using System.IO;
using WSI.Layout.classes;

namespace WSI.Layout.classes
{
  public class RealTimeDataBaseCharts
  {
    [WebMethod(), ScriptMethod()]
    public static String GetActivity(Boolean RecordNeeded, Boolean NewScene)
    {
      String _json_res;
      DataSet _activity = new DataSet();

      Int64 _time = DateTime.Now.Ticks;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand("Charts_GetActivity", _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.CommandType = CommandType.StoredProcedure;
            _cmd.CommandTimeout = 2 * 60; // 2 minutes

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
            {
              _sql_da.Fill(_activity);
            }
          }
        }
      }

      catch (Exception _ex)
      {
        Logger.Exception(HttpContext.Current, "GetActivity()", _ex);
      }

      List<String> _prop_names = new List<String>();
      _prop_names.Add("Terminal");
      _prop_names.Add("Bank");
      _prop_names.Add("Area");

      //return JsonConverters.ConvertDataSetToJson(_prop_names, _activity);

      if (_activity.Tables.Count > 0)
      {
        _json_res = clsJsonConverters.ConvertDataTableToJson("Terminal", _activity.Tables[0]);
        //_json_res = _json_res + JsonConverters.ConvertDataTableToJson("Bank", _activity.Tables[1]);
        //_json_res = _json_res + JsonConverters.ConvertDataTableToJson("Area", _activity.Tables[2]);

        /* 
        // RMS : In charts, record scene is no used 
        // Check if is needed to record scene
        if ((RecordNeeded) && (RealTimeDataBase.NeedToRecordScene()))
        {
            RealTimeDataBase.RecordScene(_json_res, NewScene);
        }
        */

        return _json_res;
      }
      else
      {
        Logger.Write(HttpContext.Current, "GetActivity() | Without activity!");
      }

      return String.Empty;
    }

  }
}