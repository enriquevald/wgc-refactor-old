﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.IO;
using System.Data.SqlClient;
using WSI.Common;
using System.Data;
using Newtonsoft.Json;
using WSI.Layout.classes;

namespace WSI.Layout
{
  public class Layout
  {

    //
    //
    //
    public static DataSet GetLayoutDataSet(Boolean ChartsMode)
    {
      String _procedure = String.Empty;
      _procedure = (ChartsMode) ? "Charts_GetObjectInfo" : "Layout_GetObjectInfo";

      DataSet _layout = new DataSet();

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_procedure, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.CommandType = CommandType.StoredProcedure;

            //_cmd.Parameters.Add("@pExternalObjectId", SqlDbType.Int).Value = 10052;
            //_cmd.Parameters.Add("@pExternalObjectId", SqlDbType.Int).Value = ;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
            {
              _sql_da.Fill(_layout);
            }

          }

        }

        return _layout;
      }

      catch (Exception _ex)
      {
        Logger.Exception(HttpContext.Current, "GetLayout()", _ex);
      }

      return null;
    }

    //
    //
    //
    public static String GetLayout()
    {
      return GetLayout(false);
    }

    //
    //
    //
    public static String GetLayout(Boolean ChartsMode)
    {
      String _procedure = String.Empty;

      _procedure = (ChartsMode) ? "Charts_GetObjectInfo" : "Layout_GetObjectInfo";

      StringBuilder json = new StringBuilder();
      StringBuilder sb = new StringBuilder();
      StringWriter sw = new StringWriter(sb);

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_procedure, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.CommandType = CommandType.StoredProcedure;

            //_cmd.Parameters.Add("@pExternalObjectId", SqlDbType.Int).Value = 10052;
            //_cmd.Parameters.Add("@pExternalObjectId", SqlDbType.Int).Value = ;

            using (SqlDataReader reader = _cmd.ExecuteReader())
            {
              JsonWriter jsonWriter = new JsonTextWriter(sw);
              int fieldcount = reader.FieldCount;
              object[] values = new object[fieldcount];

              //RMS
              jsonWriter.WriteStartArray();

              while (reader.Read())
              {
                try
                {
                  reader.GetValues(values);

                  jsonWriter.WriteStartObject();

                  //all fields 
                  jsonWriter.WritePropertyName("userData");

                  jsonWriter.WriteStartObject();
                  jsonWriter.WritePropertyName("objectLayout");

                  jsonWriter.WriteStartArray();
                  for (int index = 0; index < fieldcount; index++)
                  {
                    jsonWriter.WriteValue(values[index]);
                  }
                  jsonWriter.WriteEndArray();

                  jsonWriter.WriteEndObject();

                  jsonWriter.WriteEndObject();
                }

                catch (SqlException sqlException)
                { // exception
                  Logger.Exception(HttpContext.Current, "GetLayout()", sqlException);
                }
              }

              jsonWriter.WriteEndArray();
            }
          }

          //WGDB.Close(WGDB.Connection());
        }

        return sb.ToString();
      }

      catch (Exception _ex)
      {
        Logger.Exception(HttpContext.Current, "GetLayout()", _ex);
      }

      //WGDB.Close(WGDB.Connection());

      return sb.ToString();
    }

    //
    //
    //
    public static string GetConfiguration()
    {
      Reports.AgroupNamesFromSegmentation _data = Reports.GetLiteralsFromSegmentationAgroups();

      return String.Empty;
    }

    //
    //
    //

    public enum EN_LOGIN_EXCEPTIONS
    {
      NONE = 0,
      NOT_EXIST = 1,
      BLOCKED = 2,
      NO_PERMISSION = 3,
      WRONG_PASSWORD = 4,
      NO_ROLE=5
    }

    public static LayoutUser CheckLogin(String Username, String Password, String HostName, out String ErrorMessage)
    {
      Int32 _userId = 0;
      String _full_name = String.Empty;
      PasswordPolicy _passpolicy = new PasswordPolicy();
      LayoutUser _user_data = new LayoutUser();
      Object _obj;
      EN_LOGIN_EXCEPTIONS _login_result = EN_LOGIN_EXCEPTIONS.NONE;
      int _login_Id = -1;
      int _nls_id = 5000 + 265;
      String _str_error = "";
      String sql_str = "";
      String _pass = Password;

      _user_data.Nick = Username;
      _user_data.Logged = false;
      ErrorMessage = string.Empty;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _command = new SqlCommand("Layout_Login", _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _command.CommandType = CommandType.StoredProcedure;
            _command.CommandTimeout = 2 * 60;

            _command.Parameters.Add("@pUserName", SqlDbType.NVarChar, 40).Value = Username;
            _command.Parameters.Add("@pOrigin", SqlDbType.NVarChar, 40).Value = 1; //WEB=1, APP=2

            SqlParameter _param = _command.Parameters.Add("@opUserId", SqlDbType.Int);
            _param.Direction = ParameterDirection.Output;

            SqlParameter _param2 = _command.Parameters.Add("@opUserName", SqlDbType.NVarChar, 50);
            _param2.Direction = ParameterDirection.Output;

            _obj = _command.ExecuteScalar();

            if (_obj != null)
            {
              _login_result = (EN_LOGIN_EXCEPTIONS)_obj;
              if (_login_result != EN_LOGIN_EXCEPTIONS.NOT_EXIST)
              {
                _login_Id = (int)_param.Value;
                _user_data.FullName = _param2.Value.ToString();
              }
              else
              {
                _login_Id = 0;
                Username = "SU";
              }
            }
          }

          if (_login_result == EN_LOGIN_EXCEPTIONS.NONE)
          {
            if (!_passpolicy.VerifyCurrentPassword(_login_Id, _pass, _db_trx.SqlTransaction))
            {
              _login_result = EN_LOGIN_EXCEPTIONS.WRONG_PASSWORD;
            }
          }

          LayoutUserPermissions _permisos = new LayoutUserPermissions(_login_Id);
          if (_permisos.AvailableRoles.Count == 0)
          {
            _login_result =EN_LOGIN_EXCEPTIONS.NO_ROLE;

          }
          switch (_login_result)
          {
            case EN_LOGIN_EXCEPTIONS.BLOCKED:
              _nls_id = 5000 + 107;

              if (Login.m_culture_info == "en-US")
              {
                _str_error = "User is locked.";
              }
              else
              {
                _str_error = "El usuario está bloqueado.";
              }

              break;

            case EN_LOGIN_EXCEPTIONS.NO_PERMISSION:
              _nls_id = 5000 + 107;

              if (Login.m_culture_info == "en-US")
              {
                _str_error = "User has no permissions";
              }
              else
              {
                _str_error = "El usuario no tiene permisos";
              }
              break;

            case EN_LOGIN_EXCEPTIONS.NOT_EXIST:
              _nls_id = 5000 + 457;

              if (Login.m_culture_info == "en-US")
              {
                _str_error = "Invalid user and/or password.";
              }
              else
              {
                _str_error = "Usuario y/o contraseña incorrectos.";
              }
              break;

            case EN_LOGIN_EXCEPTIONS.WRONG_PASSWORD:
              _nls_id = 5000 + 107;

              if (Login.m_culture_info == "en-US")
              {
                _str_error = "Invalid user and/or password.";
              }
              else
              {
                _str_error = "Usuario y/o contraseña incorrectos.";
              }
              break;

            case EN_LOGIN_EXCEPTIONS.NO_ROLE:
              _nls_id = 5000 + 107;

              if (Login.m_culture_info == "en-US")
              {
                _str_error = "User don't have Rol Permissions";
              }
              else
              {
                _str_error = "El usuario no posee permisos de Rol";
              }

              break;

            case EN_LOGIN_EXCEPTIONS.NONE:
              _user_data.Logged = true;
              _user_data.Id = _login_Id;
              HttpContext.Current.Session["USER_ID"] = _login_Id;
              break;

            default:
              break;
          }

          if (_login_Id > 0 && _login_result != EN_LOGIN_EXCEPTIONS.NONE)
          {
            SqlCommand sql_command;
            SqlDataReader reader;
            Int32 login_failures = 0;
            Boolean _is_corporate_user = false;
            String AuditStr = string.Empty;

            // Increase Password failures
            sql_str = "UPDATE   GUI_USERS " +
                  " SET   GU_LOGIN_FAILURES = ISNULL(GU_LOGIN_FAILURES, 0) + 1 " +
                " WHERE   GU_USERNAME = @p1 " +
                "   AND   GU_USER_ID <> 0 "; // only disable when is not a superuser

            sql_command = new SqlCommand(sql_str);
            sql_command.Connection = _db_trx.SqlTransaction.Connection;
            sql_command.Transaction = _db_trx.SqlTransaction;

            sql_command.Parameters.Add("@p1", SqlDbType.NVarChar, 50, "GU_USERNAME").Value = Username;

            sql_command.ExecuteNonQuery();

            sql_str = "  SELECT   ISNULL(GU_LOGIN_FAILURES, 0)    " +
                      "         , CAST (CASE WHEN GU_MASTER_ID IS NULL THEN 0 ELSE 1 END AS BIT) AS GU_MASTER " +
                      "    FROM   GUI_USERS     " +
                      "   WHERE   GU_USERNAME = @p1 ";

            sql_command = new SqlCommand(sql_str);
            sql_command.Connection = _db_trx.SqlTransaction.Connection;
            sql_command.Transaction = _db_trx.SqlTransaction;

            sql_command.Parameters.Add("@p1", SqlDbType.NVarChar, 50, "GU_USERNAME").Value = Username;

            reader = sql_command.ExecuteReader();

            if (reader.Read())
            {
              login_failures = (Int32)reader[0];
              _is_corporate_user = (Boolean)reader[1];
            }

            reader.Close();

            // Check MaxLoginAttempts parameter
            if (login_failures == _passpolicy.MaxLoginAttempts)
            {

              if (Login.m_culture_info == "en-US")
              {
                _str_error = "User has been locked.";
              }
              else
              {
                _str_error = "El usuario ha sido bloqueado.";
              }

              sql_str = "UPDATE   GUI_USERS " +
                    "   SET   GU_BLOCK_REASON = GU_BLOCK_REASON | " + (Int32)GUI_USER_BLOCK_REASON.WRONG_PASSWORD +
                    "     ,   GU_LOGIN_FAILURES = 0 " +
                    " WHERE   GU_USERNAME = @p1 " +
                    "   AND   GU_USER_ID <> 0 "; // only block user when is not a superuser

              if (WSI.Common.GeneralParam.GetBoolean("Site", "MultiSiteMember", false))
              {
                // Not block if is a corporate user!!
                sql_str += " AND GU_MASTER_ID IS NULL";
              }

              sql_command = new SqlCommand(sql_str);
              sql_command.Connection = _db_trx.SqlTransaction.Connection;
              sql_command.Transaction = _db_trx.SqlTransaction;

              sql_command.Parameters.Add("@p1", SqlDbType.NVarChar, 50, "GU_USERNAME").Value = Username;
              sql_command.ExecuteNonQuery();

              _db_trx.Commit();

              if (!_is_corporate_user)
              {
                WSI.Common.Auditor.Audit(ENUM_GUI.SMARTFLOOR,            // GuiId
                                          _login_Id,                  // AccountId
                                          Username,                // UserName
                                          HostName,             // MachineName
                                          4,                         // AuditCode
                                          _nls_id,                   // NlsId (GUI)
                                          _str_error, // NlsParam01
                                          "",                        // NlsParam02
                                          "",                        // NlsParam03
                                          "",                        // NlsParam04
                                          "");                       // NlsParam05
              }
              return _user_data;
            }

            _db_trx.Commit();
          }
        }//using (DB_TRX _db_trx = new DB_TRX())

        // insert audit
        WSI.Common.Auditor.Audit(ENUM_GUI.SMARTFLOOR,            // GuiId
                                  _login_Id,                  // AccountId
                                  Username,                // UserName
                                  HostName,             // MachineName
                                  4,                         // AuditCode
                                  _nls_id,                   // NlsId (GUI)
                                  _str_error, // NlsParam01
                                  "",                        // NlsParam02
                                  "",                        // NlsParam03
                                  "",                        // NlsParam04
                                  "");                       // NlsParam05

        ErrorMessage = _str_error;
        return _user_data;
      }
      catch (Exception _ex)
      {
        Logger.Exception(HttpContext.Current, "CheckLogin()", _ex);
        return _user_data;
      }
    }
  }
}