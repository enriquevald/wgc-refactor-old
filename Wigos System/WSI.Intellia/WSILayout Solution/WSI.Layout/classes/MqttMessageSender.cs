﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;
using WSI.Layout.Hubs;
using WSI.Layout;

namespace WSI.Layout.classes
{

  public class MqttClientsManager
  {
    private static Dictionary<String, MqttMessageSender> clients;
    private static MqttMessageSender _default;


    static MqttClientsManager()
    {
      clients = new Dictionary<string, MqttMessageSender>();
      _default = new MqttMessageSender(string.Empty);
    }

    public static void Register(String managerId)
    {
      var existInstance = clients.SingleOrDefault(c => c.Key == managerId).Value;

      if (existInstance == null)
      {
        var client = new MqttMessageSender(managerId);
        clients.Add(managerId, client);
      }
    }

    public static MqttMessageSender getInstance(String managerId)
    {
      var existInstance = clients.SingleOrDefault(c => c.Key == managerId).Value;

      if (existInstance != null)
      {
        return existInstance;
      }
      else
      {
        var client = new MqttMessageSender(managerId);
        clients.Add(managerId, client);

        return client;

      }
    }

    public static MqttMessageSender Default { get { return _default; } }
  }


  public class MqttMessageSender
  {
    private string defaultTopic = "/Task/Notifications/{0}/{1}";
    private MqttClient client;
    private String managerId;
    private String clientId = ConfigurationManager.AppSettings.Get("MqttClientId") + DateTime.Now.Ticks;
    private String brokerAddress = clsNetwork.CheckServiceAddress(ConfigurationManager.AppSettings.Get("BrokerAddress"));
    private String beaconsTopic = "BeaconDistance";

    public MqttMessageSender(String _managerId)
    {
      managerId = _managerId;
      try
      {
        ConnectClient();
        Logger.Write(HttpContext.Current, String.Format("SMARTFLOOR MQTT - Connect Successfully to broker. Client: {0} - Broker Address: {1}", clientId, brokerAddress));

        if (!string.IsNullOrEmpty(_managerId))
        {
          client.Subscribe(new string[] { String.Format("/Messages/{0}/+", managerId) }, new byte[] { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE });
          Logger.Write(HttpContext.Current, String.Format("SMARTFLOOR MQTT - Subscribe Successfully to broker. Topic: {0}", String.Format("/Messages/{0}/+", managerId)));
        }
      }
      catch (Exception e)
      {
        Logger.Exception(HttpContext.Current, "MqttMessageSender.Instance.SendNewTaskAsigned || ", e);
        throw;
      }
    }

    private void ConnectClient()
    {
      client = new MqttClient(brokerAddress);
      client.MqttMsgPublishReceived += client_MqttMsgPublishReceived;
      client.ConnectionClosed += client_ConnectionClosed;
      client.Connect(clientId);
    }

    private void client_ConnectionClosed(object sender, EventArgs e)
    {
      Logger.Write(HttpContext.Current, String.Format("INTELLIA MQTT - Client {0} disconected from broker", client.ClientId));
    }

    private void client_MqttMsgPublishReceived(object sender, uPLibrary.Networking.M2Mqtt.Messages.MqttMsgPublishEventArgs e)
    {
      var message = Encoding.UTF8.GetString(e.Message);
      var topic = e.Topic;

      var topicData = topic.Split('/');
      var runnerId = topicData[3];
      var managerId = topicData[2];
      var runnerUserName = topicData[4];
      var messageId = 0;
      var messageContent = "";

      var messageArray = message.Split(new string[] { "+#" }, StringSplitOptions.None);
      messageContent = messageArray[0];
      if (messageArray.Count() > 1)
      {
        messageId =Int32.Parse(messageArray[1]);
      }

      Logger.Write(HttpContext.Current, String.Format("INTELLIA MQTT - Message received from Broker. Message: {0} - Topic:{1}", messageContent, topic));

      IHubContext context = GlobalHost.ConnectionManager.GetHubContext<ChatHub>();
      context.Clients.All.broadcastMessage(new { managerId = managerId, runnerId = runnerId, runnerUserName = runnerUserName, message = messageContent });
      Logger.Write(HttpContext.Current, "INTELLIA MQTT - Message sended to frontend");

      Messaging.UpdateMessageStatusToDelivered(messageId);
      SendMessageStatusChanged(managerId, runnerId);
    }

    public Boolean SendNewTaskAsigned(String userId, int subCategoryId, String message, long itemId)
    {
      try
      {
        if (!client.IsConnected)
        {
          ConnectClient();
        }
        if (userId == "0")
          throw new Exception("deviceId=0");//Logger.Write();
        if (itemId != 0)
          message += "_" + itemId;

        var formattedTopic = String.Format(defaultTopic, userId, subCategoryId);
        client.Publish(formattedTopic, Encoding.UTF8.GetBytes(message));
        Logger.Write(HttpContext.Current, String.Format("INTELLIA MQTT - Asigned Task or Modification Status notification sended Successfully to broker. Topic {0} - Message {1} - UserId {2}", formattedTopic, message, userId));
        return true;
      }
      catch (Exception _ex)
      {
        Logger.Exception(HttpContext.Current, "MqttMessageSender.Instance.SendNewTaskAsigned || ", _ex);
      }
      return false;
    }
    public Boolean SendMessageStatusChanged(string managerId, string runnerId)
    {
      try
      {
        if (!client.IsConnected)
        {
          ConnectClient();
        }
        string NOTIFICATION_TOPIC = String.Format("/MessagesStatusChange/{0}/{1}", managerId, runnerId);
        client.Publish(NOTIFICATION_TOPIC, Encoding.UTF8.GetBytes(""));
        Logger.Write(HttpContext.Current, String.Format("INTELLIA MQTT - Message status changed signal sended Successfully to broker. Topic {0} - Message {1} - UserId {2}", NOTIFICATION_TOPIC, "", runnerId));
        return true;
      }
      catch (Exception _ex)
      {
        Logger.Exception(HttpContext.Current, "MqttMessageSender.Instance.SendNewChatMessage || ", _ex);
      }
      return false;
    }
    internal Boolean SendNewChatMessage(WSI.Layout.classes.Messaging.Message message)
    {
      try
      {
        if (!client.IsConnected)
        {
          ConnectClient();
        }
        string NOTIFICATION_TOPIC = String.Format("/Messages/{0}/{1}/{2}", message.RunnerId, message.ManagerId, message.ManagerUserName);
        client.Publish(NOTIFICATION_TOPIC, Encoding.UTF8.GetBytes(message.Content));
        Logger.Write(HttpContext.Current, String.Format("SMARTFLOOR MQTT - Message sended Successfully to broker. Topic {0} - Message {1} - UserId {2}", NOTIFICATION_TOPIC, message, message.RunnerId));
        return true;
      }
      catch (Exception _ex)
      {
        Logger.Exception(HttpContext.Current, "MqttMessageSender.Instance.SendNewChatMessage || ", _ex);
      }
      return false;
    }

    internal Boolean SendNewApplicationVersionAvailable(String version)
    {
      try
      {
        if (!client.IsConnected)
        {
          ConnectClient();
        }
        string NOTIFICATION_TOPIC = "/NewVersion";
        client.Publish(NOTIFICATION_TOPIC, Encoding.UTF8.GetBytes(version));
        Logger.Write(HttpContext.Current, String.Format("SMARTFLOOR MQTT - Message sended Successfully to broker. Topic {0} - Message {1} ", NOTIFICATION_TOPIC, version));
        return true;
      }
      catch (Exception _ex)
      {
        Logger.Exception(HttpContext.Current, "MqttMessageSender.Instance.SendNewChatMessage || ", _ex);
      }
      return false;
    }
  }
}