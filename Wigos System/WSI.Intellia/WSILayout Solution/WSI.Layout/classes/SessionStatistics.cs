﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Timers;
using System.Xml;
using System.Xml.Serialization;

namespace WSI.Layout.classes
{
    public class SessionStatistics
    {
        public String SessionId = String.Empty;
        public Int64 DataReceivedLength = 0;
        public String Started = String.Empty;
        public String Ended = String.Empty;
        public Int64 QueriedReports = 0;
        public Int64 QueriedReportsLength = 0;
        public Int64 QueriedReportsLastDuration = 0;
        public Int64 QueriedReportsAverageDuration = 0;
        public String LastReportQueried = String.Empty;
        public Int64 RealTimeIntervals = 0;
        public Int64 RealTimeEmptyIntervals = 0;
        public Int64 RealTimeIntervalsLastLength = 0;
        public Int64 RealTimeIntervalsAccumLength = 0;
        public Int64 RealTimeIntervalsLastDuration = 0;
        public Int64 RealTimeIntervalsAverageDuration = 0;

        public SessionStatistics()
        {
        }

        public SessionStatistics(String SessionId)
        {
            this.SessionId = SessionId;
        }
    }

    public class SessionStatisticsManager
    {
        public class StatsEntry
        {
            public String EntryTime = String.Empty;
            public String SessionId = String.Empty;
            public String Message = String.Empty;
            public String Source = String.Empty;
            public String Params = String.Empty;

            public StatsEntry()
            {
            }

            public StatsEntry(String SessionId)
            {
                this.SessionId = SessionId;
                this.EntryTime = DateTime.Now.ToString();
            }
        }

        Timer aTimer = new Timer();

        public List<SessionStatistics> SessionsData = new List<SessionStatistics>();
        public String BasePath = String.Empty;

        public SessionStatisticsManager()
        {
            aTimer.Interval = 10 * 1000;
            aTimer.Elapsed += aTimer_Tick;

            if (WebConfigurationManager.AppSettings["Statistics"].ToString() == "ON")
            {
                aTimer.Start();
            }
        }

        void aTimer_Tick(object sender, EventArgs e)
        {
            for (Int32 _i = 0; _i < this.SessionsData.Count; _i++)
            {
                System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(this.SessionsData[_i].GetType());

                using (System.IO.StreamWriter file = new System.IO.StreamWriter(this.BasePath + this.SessionsData[_i].SessionId + ".xml", false))
                {
                    x.Serialize(file, this.SessionsData[_i]);
                }
            }

        }

        public void Stats(String SessionId, String Message)
        {
            Stats(SessionId, Message, String.Empty, String.Empty);
        }

        public void Stats(String SessionId, String Message, String Source, String Params)
        {
            if (WebConfigurationManager.AppSettings["Statistics"].ToString() == "ON")
            {
                StatsEntry _entry = new StatsEntry(SessionId);
                _entry.Source = Source;
                _entry.Params = Params;
                _entry.Message = Message;

                XmlSerializerNamespaces emptyNs = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });
                XmlSerializer x = new XmlSerializer(_entry.GetType());
                
                XmlWriterSettings xWriterSettings = new XmlWriterSettings();
                xWriterSettings.OmitXmlDeclaration = true;

                using (System.IO.StreamWriter file = new System.IO.StreamWriter(this.BasePath + (DateTime.Today.Ticks).ToString() + ".xml", true))
                {
                    XmlWriter xmlWriter = XmlWriter.Create(file, xWriterSettings);
                    x.Serialize(xmlWriter, _entry, emptyNs);
                }
            }
        }
    }
}