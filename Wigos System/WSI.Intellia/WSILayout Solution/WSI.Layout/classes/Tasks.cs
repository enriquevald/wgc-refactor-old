﻿//------------------------------------------------------------------------------
// Copyright © 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Tasks.cs
// 
//   DESCRIPTION: Implements the following classes:
//                1. Tasks
//                2. TaskData
// 
//        AUTHOR: Jaume Barnés
// 
// CREATION DATE: 29-JAN-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 29-JAN-2016 JBC    Initial version.
// 29-JAN-2016 JBC    Fixed Bug-8811: Error validating Alarms.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Configuration;
using WSI.Common;
using System.Data.SqlClient;
using System.Web.Services;
using System.Web.Script.Services;
using Newtonsoft;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Text;
using System.IO;
using WSI.Layout.classes;
using System.Diagnostics;
using WSI.Layout.Data.entities;

namespace WSI.Layout.classes
{
  public class Tasks
  {
    public static String GetSiteTasks()
    {
      try
      {
        Logger.Write(HttpContext.Current, "(Thread " + System.Threading.Thread.CurrentThread.ManagedThreadId + ")");
        //MqttMessageSender.Instance.GetBeaconsPositions();
        var permiso = Global.Sessions.Information[HttpContext.Current.Session.SessionID].User.Permissions.GetProfileMenuItemsByLevel(EN_ITEM_LEVEL.IL_LEVEL_1, Global.Sessions.Information[HttpContext.Current.Session.SessionID].User.CurrentRole).Where(x=>x.Id==30).ToList();
        string _data=Global.LayoutTaskData.Data;

        if (permiso.Count == 0)
        {
          JObject _object = JObject.Parse(_data);
          JArray _arrayTask = (JArray)_object["Tasks"]["rows"];
          JArray _arrayLastTask = (JArray)_object["LastTasks"]["rows"];

          foreach (JArray _row in _arrayTask)
          {
            _row[9] = "";
            _row[12] = "";
            _row[13] = "";
            _row[14] = "";
          }
          foreach (JArray _row in _arrayLastTask)
          {
            _row[3] = "";
            _row[4] = "";
            _row[5] = "";
            _row[6] = "";
          }

          _object["Tasks"]["rows"] = _arrayTask;
          _object["LastTasks"]["rows"] = _arrayLastTask;

          _data = _object.ToString();
        }
        return _data;

      }
      catch (Exception _ex)
      {
        Logger.Exception(HttpContext.Current, "GetSiteTasks()", _ex);
      }

      return String.Empty;
    }

    public static Boolean SaveSiteAlarm(AlarmData Data)
    {
      Boolean _result = false;

      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (Global.LayoutTaskData.SaveSiteAlarm(Data, _db_trx.SqlTransaction))
        {
          _db_trx.Commit();
          _result = true;
        }
      }

      return _result;
    }
 
    public static Int64 SaveSiteTask(TaskData Data)
    {
      Int64 _task_id = -1;
      IEnumerable<String> usersByRole;
      using (DB_TRX _db_trx = new DB_TRX())
      {

        _task_id = Global.LayoutTaskData.SaveSiteTask(Data, _db_trx.SqlTransaction);

        if (_task_id >= 0)
        {
          if (Data.Status == 5)
          {
            if (!ACKTerminalStatusAlarm(Data.TerminalId, Data.Subcategory, _db_trx.SqlTransaction))
            {
              //throw new Exception("ACKTerminalStatusAlarm failed");
              Logger.Write(HttpContext.Current, "ACKTerminalStatusAlarm() TerminalId" + Data.TerminalId);
            }
          }
          _db_trx.Commit();
        }
      }

      using (DB_TRX _db_trx = new DB_TRX())
      {
        try
        {
          if (_task_id >= 0)
          {
            if (Data.AssignedUserId == 0 || Data.AssignedUserId == -1)
            {
              string _roles = GetRoleListFromAssigned(new string[] { Data.AssignedRoleId.ToString(), "16", "" });
              usersByRole = GetUserByRole(_roles, _db_trx.SqlTransaction);
              foreach (string user in usersByRole)
              {
                if (!MqttClientsManager.Default.SendNewTaskAsigned(user, Data.Subcategory, Data.Description, Data.ID))
                {
                  Logger.Write(HttpContext.Current, "MqttMessageSender.Instance.SendNewTaskAsigned() failed|| ");
                }
              }
            }
            if (!MqttClientsManager.Default.SendNewTaskAsigned(Data.AssignedUserId.ToString(), Data.Subcategory, Data.Description, Data.ID))
            {
              Logger.Write(HttpContext.Current, "MqttMessageSender.Instance.SendNewTaskAsigned() failed|| ");
            }

          }
        }
        catch (Exception _ex)
        {
          Logger.Exception(HttpContext.Current, "MqttMessageSender.Instance.SendNewTaskAsigned() failed|| ", _ex);
        }

      }

      return _task_id;
    }
    private static string GetRoleListFromAssigned(string[] assignedRoles)
    {
      int assignedRoleId = int.Parse(assignedRoles[0]);
      int maxRoleId = int.Parse(assignedRoles[1]);
      string result = "";
      string separator = "";

      for (int i = 1; i <= maxRoleId; i *= 2)
      {
        if ((assignedRoleId & i) == i)
        {
          result += separator + i.ToString();
          separator = ",";
        }
      }

      return result;
    }

    private static bool ACKTerminalStatusAlarm(int TerminalId, int AlarmId, SqlTransaction Trx)
    {
      try
      {
        using (SqlCommand _cmd = new SqlCommand("Layout_TerminalStatusUnsetFlag", Trx.Connection, Trx))
        {
          //Split alarmid
          Int32 _column_id;
          Int32 _flag;
          String _alarm_id = AlarmId.ToString();
          String _column_name;

          if (!Int32.TryParse(_alarm_id.Substring(0, 2), out _column_id))
          {

            return false;
          }

          if (!Int32.TryParse(_alarm_id.Substring(2, 3), out _flag))
          {

            return false;
          }


          switch (_column_id)
          {
            case 12:
              _column_name = "TS_DOOR_FLAGS";
              break;

            case 13:
              _column_name = "TS_BILL_FLAGS";
              break;

            case 14:
              _column_name = "TS_PRINTER_FLAGS";
              break;

            case 16:
              _column_name = "TS_PLAYED_WON_FLAGS";
              break;

            case 17:
              _column_name = "TS_JACKPOT_FLAGS";
              break;

            case 20:
              if (_flag == 512)
              {
                _column_name = "TS_MACHINE_FLAGS";
              }
              else
              {
                _column_name = "TS_EGM_FLAGS";
              }
              break;

            case 19:
              _column_name = "TS_CALL_ATTENDANT_FLAGS";
              break;

            case 99:
              return true;

            default:
              return false;
          }

          _cmd.CommandType = CommandType.StoredProcedure;

          // Params Adding
          _cmd.Parameters.Add("@pColumn", SqlDbType.NVarChar).Value = _column_name;
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.BigInt).Value = TerminalId;
          _cmd.Parameters.Add("@pFlag", SqlDbType.BigInt).Value = _flag;

          if (!(_cmd.ExecuteNonQuery() > 0))
          {

            return true;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Logger.Write(HttpContext.Current, "SaveSiteTask() || " + _ex.Message);
      }
      return false;
    }

    public static TaskData AssignAlarm(AlarmData Alarm, TaskData Task)
    {
      Int64 _task_id = -1;
      DataTable _bach_derivations;
      DataRow _dr;
      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _task_id = Global.LayoutTaskData.SaveSiteTask(Task, _db_trx.SqlTransaction);
          Task.ID = _task_id;

          if (_task_id >= 0)
          {
            Alarm.TaskId = _task_id;
            Alarm.DateToTask = TruncateMilliseconds(DateTime.Now);

            if (Global.LayoutTaskData.SaveSiteAlarm(Alarm, _db_trx.SqlTransaction))
            {
              _db_trx.Commit();
            }
            else
            {
              throw new Exception("SaveSiteAlarm failed");
            }
          }
        }

        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (_task_id >= 0)
          {
            try
            {
              _bach_derivations = CreateTaskDerivationsDT();

              if (Task.AssignedUserId == 0 || Task.AssignedUserId == -1)
              {
                string _roles = GetRoleListFromAssigned(new string[] { Task.AssignedRoleId.ToString(), "16", "" });
                var usersByRole = GetUserByRole(_roles, _db_trx.SqlTransaction);

                foreach (var user in usersByRole)
                {
                  if (MqttClientsManager.Default.SendNewTaskAsigned(user, Task.Subcategory, Alarm.Description, Task.ID))
                  {
                    _dr = _bach_derivations.NewRow();

                    _dr["LSTD_TASK_ID"] = Task.ID;
                    _dr["LSTD_USER_ID"] = user;
                    _dr["LSTD_REJECTED_DATE"] = DBNull.Value;

                    _bach_derivations.Rows.Add(_dr);
                  }
                  else
                  {
                    Logger.Write(HttpContext.Current, "MqttMessageSender.Instance.SendNewTaskAsigned() failed|| ");
                  }
                }
              }
              else
              {
                _dr = _bach_derivations.NewRow();

                _dr["LSTD_TASK_ID"] = Task.ID;
                _dr["LSTD_USER_ID"] = Task.AssignedUserId;
                _dr["LSTD_REJECTED_DATE"] = DBNull.Value;

                _bach_derivations.Rows.Add(_dr);

                if (!MqttClientsManager.Default.SendNewTaskAsigned(Task.AssignedUserId.ToString(), Task.Subcategory, Alarm.Description, Task.ID))
                {
                  Logger.Write(HttpContext.Current, "MqttMessageSender.Instance.SendNewTaskAsigned() failed|| ");
                }
              }
              if (_bach_derivations.Rows.Count > 0)
              {
                if (!DB_InsertUserTaskDerivations(_bach_derivations, _db_trx.SqlTransaction))
                {
                  Logger.Write(HttpContext.Current, "DB_InsertUserTaskDerivations() failed|| ");
                }
              }

              _db_trx.Commit();
            }
            catch (Exception _ex)
            {
              Logger.Write(HttpContext.Current, "MqttMessageSender.Instance.SendNewTaskAsigned() failed|| " + _ex.Message);
            }

          }
        }
      }
      catch (Exception _ex)
      {
        Logger.Write(HttpContext.Current, "AssignAlarm() || " + _ex.Message);
      }

      return Task;
    }

    /// <summary>
    /// Save Bach data of derivations into layout_users_tasks_derivations table.
    /// </summary>
    /// <param name="BachDerivations">Datatable instance of derivations</param>
    /// <param name="Trx">Sql transaction</param>
    /// <returns>True if everything works/False if error</returns>
    public static Boolean DB_InsertUserTaskDerivations(DataTable BachDerivations, SqlTransaction Trx)
    {
      int _nr = 0;
      try
      {
        using (SqlCommand _sql_cmd = new SqlCommand("Layout_InsertTasksDerivations", Trx.Connection, Trx))
        {
          _sql_cmd.CommandType = CommandType.StoredProcedure;

          _sql_cmd.Parameters.Add("@pTaskId", SqlDbType.BigInt).SourceColumn = "LSTD_TASK_ID";
          _sql_cmd.Parameters.Add("@pUserId", SqlDbType.Int).SourceColumn = "LSTD_USER_ID";
          _sql_cmd.Parameters.Add("@pDateRejected", SqlDbType.DateTime).SourceColumn = "LSTD_REJECTED_DATE";

          using (SqlDataAdapter _sql_da = new SqlDataAdapter())
          {
            _sql_da.InsertCommand = _sql_cmd;
          
            _nr = _sql_da.Update(BachDerivations);
            
            if (_nr == BachDerivations.Rows.Count)
            {
              _sql_cmd.ExecuteNonQuery();
              return true;
            }
          }
        }

        return false;
      }
      catch (Exception _ex)
      {
        Debug.WriteLine("Trx_Insert_Failed " + _ex.Message);
      }

      return true;
    }
    /// <summary>
    /// Create a DataTable with the layout_site_task_derivation schema.
    /// </summary>
    /// <returns>Instance of DataTable</returns>
    public static DataTable CreateTaskDerivationsDT()
    {
      DataTable _dt_tasks_derivations = new DataTable();

      _dt_tasks_derivations.Columns.Add("LSTD_TASK_ID", Type.GetType("System.Int64"));
      _dt_tasks_derivations.Columns.Add("LSTD_USER_ID", Type.GetType("System.Int32"));
      _dt_tasks_derivations.Columns.Add("LSTD_REJECTED_DATE", Type.GetType("System.DateTime"));

      return _dt_tasks_derivations;
    }

    private static IEnumerable<String> GetUserByRole(string roleId, SqlTransaction sqlTransaction)
    {
      String _device_id = string.Empty;
      List<String> _devices = new List<String>();
      try
      {
        SqlDataAdapter _adapter;
        DataTable _data;
        _data = new DataTable();

        using (SqlCommand _cmd = new SqlCommand("Layout_GetUsersIdByRoleId", sqlTransaction.Connection, sqlTransaction))
        {
          _cmd.CommandType = CommandType.StoredProcedure;
          _cmd.Parameters.Add("@pRoleId", SqlDbType.NVarChar).Value = roleId;
          _adapter = new SqlDataAdapter(_cmd);
          _adapter.Fill(_data);
        }

        foreach (DataRow row in _data.Rows)
        {
          _devices.Add(row["GU_USER_ID"].ToString());
        }

      }
      catch (Exception _ex)
      {
        Logger.Exception(HttpContext.Current, "GetUserByRole()", _ex);
      }

      return _devices;
    }

    public static DateTime TruncateMilliseconds(DateTime d)
    {
      return d.AddMilliseconds(-d.Millisecond);
    }

    private static string GetUserAssignedDevice(int assignedUserId, SqlTransaction Trx)
    {
      String _device_id = string.Empty;
      try
      {
        using (SqlCommand _cmd = new SqlCommand("Layout_GetAssignedDeviceByUserId", Trx.Connection, Trx))
        {
          SqlParameter _sql_device_id;

          _cmd.CommandType = CommandType.StoredProcedure;

          // Params Adding
          _cmd.Parameters.Add("@pUserId", SqlDbType.BigInt).Value = assignedUserId;
          _sql_device_id = _cmd.Parameters.Add("@pDeviceId", SqlDbType.NVarChar);
          _sql_device_id.Direction = ParameterDirection.Output;
          _sql_device_id.Size = 50;
          _cmd.CommandTimeout = 2 * 60; // 2 minutes

          if (!(_cmd.ExecuteNonQuery() > 0))
          {
            _device_id = _sql_device_id.Value.ToString();
          }
        }
      }
      catch (Exception _ex)
      {
        Logger.Exception(HttpContext.Current, "GetUserAssignedDevice()", _ex);
      }

      return _device_id;
    }

    public static string GetMediaData(int MediaId)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return GetMediaData(MediaId, _db_trx.SqlTransaction);
      }
    }

    public static string GetMediaData(int MediaId, SqlTransaction Trx)
    {
      try
      {
        String _result = string.Empty;

        using (SqlCommand _cmd = new SqlCommand("Layout_GetMediaData", Trx.Connection, Trx))
        {
          SqlParameter _sql_media_data;

          _cmd.CommandType = CommandType.StoredProcedure;

          // Params Adding
          _cmd.Parameters.Add("@pId", SqlDbType.BigInt).Value = MediaId;
          _sql_media_data = _cmd.Parameters.Add("@pMediaData", SqlDbType.NVarChar);
          _sql_media_data.Direction = ParameterDirection.Output;
          _sql_media_data.Size = -1;
          _cmd.CommandTimeout = 2 * 60; // 2 minutes

          if (!(_cmd.ExecuteNonQuery() > 0))
          {
            _result = _sql_media_data.Value.ToString();
          }
        }

        return _result;
      }
      catch (Exception _ex)
      {
        Log.Message(_ex.Message);
      }

      return string.Empty;
    }
  }
}