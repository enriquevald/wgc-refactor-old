﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using System.IO;
using WSI.Layout.classes;
using System.Data.SqlClient;
using System.Data;
using WSI.Common;
using System.Text;

namespace WSI.Layout
{
    // Editor static class for database operations
    public class EditorRoutines
    {
        // Returns a JSon String that contains the list of objects not "inserted" in layout
        public static String LoadAvailableObjects()
        {
            List<String> _reg_items = new List<String>();
            String _reg = String.Empty;

            try
            {
                using (DB_TRX _db_trx = new DB_TRX())
                {
                    using (SqlCommand _cmd = new SqlCommand("Layout_Available_Objects", _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
                    {
                        _cmd.CommandType = CommandType.StoredProcedure;
                        _cmd.CommandTimeout = 2 * 60; // 2 minutes

                        using (SqlDataReader _reader = _cmd.ExecuteReader())
                        {
                            while (_reader.Read())
                            {
                                _reg_items.Clear();

                                for (Int32 _i = 0; _i < _reader.FieldCount; _i++)
                                {
                                    _reg_items.Add(String.Empty);
                                    _reg_items[_i] = _reader.GetName(_i).ToString() + ":'" + _reader.GetString(_i) + "'";
                                }

                                _reg = "{" + String.Join(",", _reg_items) + "}";
                            }
                        }

                        _db_trx.Commit();

                        return _reg;
                    }
                }
            }
            catch (Exception _ex)
            {
              Logger.Exception(HttpContext.Current, "LoadAvailableObjects()", _ex);
            }

            return String.Empty;
        }

        // Update one layout object in database
        private static void UpdateDatabaseObject(Dictionary<String, String> Values)
        {
            try
            {

                using (DB_TRX _db_trx = new DB_TRX())
                {
                    using (SqlCommand _cmd = new SqlCommand("Layout_Object_Update", _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
                    {
                        _cmd.CommandType = CommandType.StoredProcedure;
                        _cmd.CommandTimeout = 2 * 60; // 2 minutes

                        _cmd.Parameters.Add("@pObjectId", SqlDbType.BigInt).Value = Convert.ToInt32(Values["LO_ID"]);
                        _cmd.Parameters.Add("@pPositionDate", SqlDbType.DateTime).Value = WSI.Common.Format.DBDatetimeToLocalDateTime(Values["LO_UPDATE_DATE"]);
                        _cmd.Parameters.Add("@pPosX", SqlDbType.Int).Value = Convert.ToInt32(Values["LOP_X"]);
                        _cmd.Parameters.Add("@pPosY", SqlDbType.Int).Value = Convert.ToInt32(Values["LOP_Y"]);
                        _cmd.Parameters.Add("@pPosZ", SqlDbType.Int).Value = Convert.ToInt32(Values["LOP_Z"]);
                        _cmd.Parameters.Add("@pOrientation", SqlDbType.Int).Value = Convert.ToInt32(Values["LOP_ORIENTATION"]); ;
                        _cmd.Parameters.Add("@pFloorId", SqlDbType.BigInt).Value = Convert.ToInt32(Values["LOP_FLOOR_ID"]); ;
                        _cmd.Parameters.Add("@pAreaId", SqlDbType.BigInt).Value = Convert.ToInt32(Values["LOP_AREA_ID"]);
                        _cmd.Parameters.Add("@pBankId", SqlDbType.BigInt).Value = Convert.ToInt32(Values["LOP_BANK_ID"]);
                        _cmd.Parameters.Add("@pExternalId", SqlDbType.BigInt).Value = Convert.ToInt32(Values["LO_EXTERNAL_ID"]); ;
                        _cmd.Parameters.Add("@pType", SqlDbType.Int).Value = Convert.ToInt32(Values["LO_TYPE"]);
                        if (Values["LO_PARENT_ID"] != "NULL")
                        {
                            _cmd.Parameters.Add("@pParentId", SqlDbType.BigInt).Value = Convert.ToInt32(Values["LO_PARENT_ID"]);
                        }
                        else
                        {
                            _cmd.Parameters.Add("@pParentId", SqlDbType.BigInt).Value = DBNull.Value;
                        }
                        _cmd.Parameters.Add("@pMeshId", SqlDbType.Int).Value = Convert.ToInt32(Values["LO_MESH_ID"]);
                        _cmd.Parameters.Add("@pCreationDate", SqlDbType.DateTime).Value = WSI.Common.Format.DBDatetimeToLocalDateTime(Values["LO_CREATION_DATE"]);
                        _cmd.Parameters.Add("@pUpdateDate", SqlDbType.DateTime).Value = WSI.Common.Format.DBDatetimeToLocalDateTime(Values["LO_UPDATE_DATE"]);

                        _cmd.ExecuteNonQuery();

                        _db_trx.Commit();
                    }
                }
            }
            catch (Exception _ex)
            {
              Logger.Exception(HttpContext.Current, "UpdateDatabaseObject()", _ex);
            }

        }

        // Convert a JSon string in to a dictionary with property name as key and the value.
        // ** Reader parameter is "readed" until a StartObject token. **
        // Process ends on no more reads from Reader or founded token EndObject
        private static Dictionary<String, String> ReadJSObject(JsonTextReader Reader)
        {
            Dictionary<String, String> _result = new Dictionary<String, String>();
            Boolean _end = false;
            Boolean _end_field = false;
            String _field = String.Empty;
            String _value = String.Empty;

            while (!_end)
            {
                if (Reader.Read())
                {
                    switch (Reader.TokenType)
                    {
                        // End Object -> must end
                        case JsonToken.EndObject:
                            _end = true;
                            continue;
                        // Property name (field)
                        case JsonToken.PropertyName:
                            _field = Reader.Value.ToString().ToUpper();
                            break;
                        // Null value
                        case JsonToken.Null:
                            _value = "NULL";
                            _end_field = true;
                            break;
                        // String types
                        case JsonToken.Date:
                            _value = WSI.Common.Format.CustomFormatDateTime(((DateTime)Reader.Value), true);
                            _end_field = true;
                            break;
                        case JsonToken.String:
                        // Numeric types
                        case JsonToken.Integer:
                        case JsonToken.Float:
                            _value = Reader.Value.ToString();
                            _end_field = true;
                            break;
                    }

                    // Only save when there is a field name and its value
                    if (_end_field)
                    {
                        _result.Add(_field, _value);
                        _end_field = false;
                    }
                }
                else
                {
                    // End of Data
                    _end = true;
                }
            }

            return _result;
        }

        // With a Data string that contains an array of objects, each one with the all the properties of the layout object, 
        // uses a JSon converter and reader to process each object in the array. Each object will be updated or inserted in the database.
        public static void SaveObjects(String Data)
        {
            String _date_update = WSI.Common.Format.CustomFormatDateTime(WGDB.Now, true);
            Dictionary<String, String> _object_data;

            using (JsonTextReader _js_reader = new JsonTextReader(new StringReader(Data)))
            {
                _js_reader.Read();

                if (_js_reader.TokenType == JsonToken.StartArray)
                {
                    while (_js_reader.Read())
                    {
                        if (_js_reader.TokenType == JsonToken.StartObject)
                        {
                            _object_data = ReadJSObject(_js_reader);

                            if (_object_data.Count > 0)
                            {
                                // Update the date of change before save it
                                _object_data["LO_UPDATE_DATE"] = _date_update;
                                UpdateDatabaseObject(_object_data);
                            }
                        }
                    }
                }
                else
                {
                    // Array Expected
                }

            }

        }

    }
}