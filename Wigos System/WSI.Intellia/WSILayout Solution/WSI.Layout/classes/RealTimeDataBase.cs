﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Configuration;
using WSI.Common;
using System.Data.SqlClient;
using System.Web.Services;
using System.Web.Script.Services;
using Newtonsoft;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Text;
using System.IO;
using WSI.Layout.classes;
using System.Web.Configuration;
using System.Web.Script.Serialization;

namespace WSI.Layout
{
  public class RealTimeDataBase
  {

    private const String m_default_user_configuration = "{\"ShowActivity\":false,\"ShowAlarms\":false,\"ShowBackground\":true,\"ShowGrid\":false,\"ShowAreas\":false,\"ShowBanks\":false,\"ShowHeatMap\":false,\"ShowGamingTables\":true,\"ShowRealTimeRefresh\":true,\"TestEnabled\":false,\"TerminalAppearance3D\":1,\"ViewMode\":2,\"Language\":\"ES\"}";
    private static String m_default_user_dashboard = "";//"[{\"id\":\"widgetTestList1\",\"x\":2,\"y\":6,\"width\":1,\"height\":8,\"type\":\"WidgetListBase\",\"canResize\":false,\"canMove\":true},{\"id\":\"widgetTestList2\",\"x\":1,\"y\":6,\"width\":1,\"height\":8,\"type\":\"WidgetListBase\",\"canResize\":false,\"canMove\":true},{\"id\":\"widgetIndicator1\",\"x\":3,\"y\":13,\"width\":1,\"height\":3,\"type\":\"WidgetBase\",\"canResize\":false,\"canMove\":true},{\"id\":\"widgetIndicator2\",\"x\":3,\"y\":16,\"width\":1,\"height\":3,\"type\":\"WidgetBase\",\"canResize\":false,\"canMove\":true},{\"id\":\"widgettheater2\",\"x\":1,\"y\":14,\"width\":1,\"height\":6,\"type\":\"WidgetSVGAmphitheater\",\"canResize\":false,\"canMove\":true},{\"id\":\"widgettheater3\",\"x\":2,\"y\":14,\"width\":1,\"height\":7,\"type\":\"WidgetSVGAmphitheater\",\"canResize\":false,\"canMove\":true},{\"id\":\"widgetOccupancy1\",\"x\":0,\"y\":14,\"width\":1,\"height\":6,\"type\":\"WidgetSVGAmphitheater\",\"canResize\":false,\"canMove\":true},{\"id\":\"widgetTasks1\",\"x\":3,\"y\":5,\"width\":1,\"height\":7,\"type\":\"WidgetBase\",\"canResize\":false,\"canMove\":true},{\"id\":\"widgetColumns1\",\"x\":3,\"y\":19,\"width\":1,\"height\":4,\"type\":\"WidgetColumnsBase\",\"canResize\":false,\"canMove\":true},{\"id\":\"widgetComparation1\",\"x\":0,\"y\":0,\"width\":1,\"height\":6,\"type\":\"WidgetBase\",\"canResize\":false,\"canMove\":true},{\"id\":\"widgetChart1\",\"x\":1,\"y\":0,\"width\":1,\"height\":6,\"type\":\"WidgetSVGChart\",\"canResize\":false,\"canMove\":true},{\"id\":\"widgetHBar1\",\"x\":3,\"y\":2,\"width\":1,\"height\":3,\"type\":\"WidgetBase\",\"canResize\":false,\"canMove\":true},{\"id\":\"widgetChart2\",\"x\":3,\"y\":23,\"width\":1,\"height\":6,\"type\":\"WidgetSVGChart\",\"canResize\":false,\"canMove\":true},{\"id\":\"widgetDonut1\",\"x\":2,\"y\":0,\"width\":1,\"height\":6,\"type\":\"WidgetSVGDonutChart\",\"canResize\":false,\"canMove\":true},{\"id\":\"widgetVipsList\",\"x\":0,\"y\":6,\"width\":1,\"height\":8,\"type\":\"WidgetListBase\",\"canResize\":false,\"canMove\":true},{\"id\":\"widgetIndicator3\",\"x\":0,\"y\":6,\"width\":1,\"height\":3,\"type\":\"WidgetBase\",\"canResize\":false,\"canMove\":true},{\"id\":\"widgetMachineOccupation\",\"x\":2,\"y\":0,\"width\":1,\"height\":6,\"type\":\"WidgetSVGDonutChart\",\"canResize\":false,\"canMove\":true},{\"id\":\"widgetHRList\",\"x\":2,\"y\":6,\"width\":1,\"height\":8,\"type\":\"WidgetListBase\",\"canResize\":false,\"canMove\":true}]";
    private const Int64 CONST_NUMBER_OF_DAYS_BEFORE = 3;
    private static string m_last_profile_update;
    private static DateTime m_last_recording_update;

    public static String GetActivity(Boolean RecordNeeded, Boolean NewScene)
    {
      String _json_res;
      DataSet _activity = new DataSet();

      Int64 _time = DateTime.Now.Ticks;

      List<String> _prop_names = new List<String>();
      _prop_names.Add("Terminal");
      _prop_names.Add("Bank");
      _prop_names.Add("Area");

      try
      {
        Logger.Write(HttpContext.Current, "(Thread " + System.Threading.Thread.CurrentThread.ManagedThreadId + ")");
        //DataSet _data = Global.LayoutData.GetData();
        //DataSet _data = (DataSet)HttpContext.Current.Application["ActivityData"];
        //String _data = (String)HttpContext.Current.Application["ActivityData"];
        String _data = Global.LayoutData.Data;
        _json_res = _data;

        //if (_data.Tables.Count > 0)
        //{
        //  _json_res = JsonConverters.ConvertDataTableToJson("Terminal", _data.Tables[0]);
        //  //_json_res = _json_res + JsonConverters.ConvertDataTableToJson("Bank", _activity.Tables[1]);
        //  //_json_res = _json_res + JsonConverters.ConvertDataTableToJson("Area", _activity.Tables[2]);

        //  // Check if is needed to record scene
        //  if ((RecordNeeded) && (RealTimeDataBase.NeedToRecordScene()))
        //  {
        //    RealTimeDataBase.RecordScene(_json_res, NewScene);
        //  }

        return _json_res;
        //}
        //else
        //{
        //  Logger.Write(HttpContext.Current, "GetActivity() | Without activity!");
        //}

      }
      catch (Exception _ex)
      {
        Logger.Exception(HttpContext.Current, "GetActivity()", _ex);
        //throw;
      }

      return String.Empty;
    }

    //[WebMethod(), ScriptMethod()]
    //public static String GetActivity(Boolean RecordNeeded, Boolean NewScene)
    //{
    //  String _json_res;
    //  DataSet _activity = new DataSet();

    //  Int64 _time = DateTime.Now.Ticks;

    //  try
    //  {
    //    using (DB_TRX _db_trx = new DB_TRX())
    //    {
    //      using (SqlCommand _cmd = new SqlCommand("Layout_GetActivity", _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
    //      {
    //        _cmd.CommandType = CommandType.StoredProcedure;
    //        _cmd.CommandTimeout = 2 * 60; // 2 minutes

    //        using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
    //        {
    //          _sql_da.Fill(_activity);
    //        }
    //      }
    //    }
    //  }

    //  catch (Exception _ex)
    //  {
    //    Logger.Exception(HttpContext.Current, "GetActivity()", _ex);
    //  }

    //  List<String> _prop_names = new List<String>();
    //  _prop_names.Add("Terminal");
    //  _prop_names.Add("Bank");
    //  _prop_names.Add("Area");

    //  //return JsonConverters.ConvertDataSetToJson(_prop_names, _activity);

    //  if (_activity.Tables.Count > 0)
    //  {
    //    _json_res = JsonConverters.ConvertDataTableToJson("Terminal", _activity.Tables[0]);
    //    //_json_res = _json_res + JsonConverters.ConvertDataTableToJson("Bank", _activity.Tables[1]);
    //    //_json_res = _json_res + JsonConverters.ConvertDataTableToJson("Area", _activity.Tables[2]);

    //    // Check if is needed to record scene
    //    if ((RecordNeeded) && (RealTimeDataBase.NeedToRecordScene()))
    //    {
    //      RealTimeDataBase.RecordScene(_json_res, NewScene);
    //    }

    //    return _json_res;
    //  }
    //  else
    //  {
    //    Logger.Write(HttpContext.Current, "GetActivity() | Without activity!");
    //  }

    //  return String.Empty;
    //}

    public static String GetActivityTest(Boolean RecordNeeded, Boolean NewScene)
    {
      String _json_res;
      DataSet _activity = new DataSet();

      Int64 _time = DateTime.Now.Ticks;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          //using (SqlCommand _cmd = new SqlCommand("Layout_GetActivity_Test", _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          using (SqlCommand _cmd = new SqlCommand("Layout_GetActivity_ynm", _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {

            _cmd.CommandType = CommandType.StoredProcedure;
            _cmd.CommandTimeout = 2 * 60; // 2 minutes
            _cmd.Parameters.Add("@Date", SqlDbType.DateTime).Value = DateTime.Parse("2014-06-16 08:00:0.000");
            _cmd.Parameters.Add("@IntervalHour", SqlDbType.Int).Value = 1;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
            {
              _sql_da.Fill(_activity);
            }
          }
          _db_trx.Commit();
        }
      }

      catch (Exception _ex)
      {
        Logger.Exception(HttpContext.Current, "GetActivityTest()", _ex);
      }

      List<String> _prop_names = new List<String>();
      _prop_names.Add("Terminal");
      _prop_names.Add("Bank");
      _prop_names.Add("Area");

      //return JsonConverters.ConvertDataSetToJson(_prop_names, _activity);

      if (_activity.Tables.Count > 0)
      {
        _json_res = clsJsonConverters.ConvertDataTableToJson("Terminal", _activity.Tables[0]);
        //_json_res = _json_res + JsonConverters.ConvertDataTableToJson("Bank", _activity.Tables[1]);
        //_json_res = _json_res + JsonConverters.ConvertDataTableToJson("Area", _activity.Tables[2]);

        // Check if is needed to record scene
        if ((RecordNeeded) && (RealTimeDataBase.NeedToRecordScene()))
        {
          RealTimeDataBase.RecordScene(_json_res, NewScene);
        }

        return _json_res;
      }
      else
      {
        Logger.Write(HttpContext.Current, "GetActivityTest() | Without activity!");
      }

      return String.Empty;
    }

    public static String GetPlayerActivity(String ListAccountId, Boolean OnlyActivePlaySession) //ListAccountId = "100,322,1553"
    {
      DataSet _player_activity = new DataSet();

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand("Layout_GetPlayerActivity", _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.CommandType = CommandType.StoredProcedure;

            // Params Adding
            _cmd.Parameters.Add("@pAccountId", SqlDbType.NVarChar).Value = ListAccountId;
            _cmd.Parameters.Add("@pOnlyActivePlaySession", SqlDbType.Bit).Value = OnlyActivePlaySession;
            _cmd.CommandTimeout = 2 * 60; // 2 minutes

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
            {
              _sql_da.Fill(_player_activity);
            }
          }
        }
      }

      catch (Exception _ex)
      {
        Logger.Exception(HttpContext.Current, "GetPlayerActivity()", _ex);
      }

      List<String> _prop_names = new List<String>();
      _prop_names.Add("Terminal");
      _prop_names.Add("BillIn");

      return clsJsonConverters.ConvertDataSetToJson(_prop_names, _player_activity);
    }

    public static String GetPlayerActivityTest(String ListAccountId, Boolean OnlyActivePlaySession) //ListAccountId = "100,322,1553"
    {
      DataSet _player_activity = new DataSet();

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand("Layout_GetPlayerActivity_Test", _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.CommandType = CommandType.StoredProcedure;

            // Params Adding
            _cmd.Parameters.Add("@pAccountId", SqlDbType.NVarChar).Value = ListAccountId;
            _cmd.Parameters.Add("@pOnlyActivePlaySession", SqlDbType.Bit).Value = OnlyActivePlaySession;
            _cmd.CommandTimeout = 2 * 60; // 2 minutes

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
            {
              _sql_da.Fill(_player_activity);
            }
          }
          _db_trx.Commit();
        }
      }

      catch (Exception _ex)
      {
        Logger.Exception(HttpContext.Current, "GetPlayerActivityTest()", _ex);
      }

      List<String> _prop_names = new List<String>();
      _prop_names.Add("Terminal");
      _prop_names.Add("BillIn");

      return clsJsonConverters.ConvertDataSetToJson(_prop_names, _player_activity);
    }

    public static DataTable GetTerminalsStatistics()
    {
      return RealTimeDataBase.GetTerminalsStatistics(-1);
    }

    public static DataTable GetTerminalsStatistics(Int32 GameId)
    {
      DataTable _term_activity = new DataTable();

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand("Layout_GetTerminalStatistics", _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.CommandType = CommandType.StoredProcedure;

            // Params Adding
            _cmd.Parameters.Add("@pFrom", SqlDbType.DateTime).Value = WGDB.Now.AddDays(-1);
            _cmd.Parameters.Add("@pTo", SqlDbType.DateTime).Value = WGDB.Now;

            if (GameId > 0)
            {
              _cmd.Parameters.Add("@pGameId", SqlDbType.DateTime).Value = GameId;
            }

            _cmd.CommandTimeout = 2 * 60; // 2 minutes

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
            {
              _sql_da.Fill(_term_activity);
            }
          }
        }
      }

      catch (Exception _ex)
      {
        Logger.Exception(HttpContext.Current, "GetTerminalStatistics()", _ex);
      }

      return _term_activity;
    }

    /// <summary>
    /// Return JSON String with parameters of User Configuration
    /// </summary>
    /// <returns>String</returns>
    public static String GetLayoutConfiguration()
    {
      String _configuration;
      String _dashboard;
      String _server_config;
      Int32 _user_id;
      string _role_id;
      JArray _widgets_list;
      Dictionary<string, string> _generalparams = new Dictionary<string, string>();
      JavaScriptSerializer _js = new JavaScriptSerializer();
      string _json = "";
      

      _user_id = 0;
      _configuration = String.Empty;
      _server_config = String.Empty;

      // Search for user id at Global.Sessions
      if (!Global.Sessions.Information.ContainsKey(HttpContext.Current.Session.SessionID) || !Int32.TryParse(Global.Sessions.Information[HttpContext.Current.Session.SessionID].User.Id.ToString(), out _user_id))
      {
        return "{\"Message\":\"950\"}";
      }

      m_default_user_dashboard = GetDefaultUserDashBoard();

      try
      {
        byte[] _change ;
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand("Layout_GetIsUpdateProfile", _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = _user_id;

            _cmd.CommandType = CommandType.StoredProcedure;
            _cmd.CommandTimeout = 2 * 60; // 2 minutes
            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
            {
              using (DataSet _data = new DataSet())
              {
                _sql_da.Fill(_data);
                _change =(byte[])_data.Tables[0].Rows[0][0];
                m_last_profile_update = Convert.ToBase64String(_change);
              }
            }
          }
     
          using (SqlCommand _cmd = new SqlCommand("Layout_GetConfiguration", _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = _user_id;

            _cmd.CommandType = CommandType.StoredProcedure;
            _cmd.CommandTimeout = 2 * 60; // 2 minutes

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
            {
              using (DataSet _data = new DataSet())
              {
                _sql_da.Fill(_data);

                if (_data.Tables[0].Rows.Count > 0 && _data.Tables[0].Rows[0][0] != DBNull.Value && (String)_data.Tables[0].Rows[0][0] != String.Empty)
                {
                  _configuration = (String)_data.Tables[0].Rows[0][0];
                }
                else
                {
                  _configuration = m_default_user_configuration;
                }
                string _tempData = "";
                if (_data.Tables[0].Rows.Count>0 && _data.Tables[0].Rows[0][2] != DBNull.Value)
                  _tempData = _data.Tables[0].Rows[0][2].ToString();
                if (_data.Tables[0].Rows.Count>0 && m_last_profile_update == _tempData)
                  _dashboard = (String)_data.Tables[0].Rows[0][1];
                else
                {
                  _dashboard = AdjustDasboard(_data.Tables[0].Rows.Count > 0 && _data.Tables[0].Rows[0][1] != DBNull.Value && _data.Tables[0].Rows[0][1]!="" ? (String)_data.Tables[0].Rows[0][1] : m_default_user_dashboard);

                }
              }
            }
          }
        }

        _role_id = ((int)Global.Sessions.Information[HttpContext.Current.Session.SessionID].User.CurrentRole).ToString();

        //HttpContext.Current.Session["dashboardComplete"] = _dashboard;
        Global.Sessions.Information[HttpContext.Current.Session.SessionID].Dashboard = _dashboard;

        // TODO: Review BUG with invisible dashboard
        _widgets_list = JArray.Parse(_dashboard);

        foreach (JObject _widget in _widgets_list.Children<JObject>())
        {
          IEnumerable<JProperty> _widget_properties = _widget.Properties();

          foreach (JProperty _prop in _widget_properties)
          {
            if (_prop.Name == _role_id)
              _dashboard = _prop.Value.ToString();
          }
        }

        if (_dashboard == "[]")
        {
          Logger.Write(HttpContext.Current, "Empty dashboard for user id: " + _user_id);
          _dashboard = m_default_user_dashboard;
        }

        // Save configuration in Session
        if (!String.IsNullOrEmpty(_configuration))
        {
          //HttpContext.Current.Session["configuration"] = _configuration;
          Global.Sessions.Information[HttpContext.Current.Session.SessionID].Configuration = _configuration;
        }
        
        _configuration = "\"user\":" + _configuration + ",\"dashboard\":" + _dashboard ;

        LoadGeneralParams(ref _generalparams);

        _json = _js.Serialize(_generalparams);

        // Server configuration
        _server_config += "{";
        _server_config += "\"language\":\"" + WSI.Layout.clsIntl.GetSystemLanguageName() + "\",";
        _server_config += "\"currency\":\"" + WSI.Common.CurrencyExchange.GetNationalCurrency() + "\",";
        _server_config += "\"symbol\":\"" + WSI.Common.CurrencyExchangeProperties.GetNationalCurrencySymbol() + "\"";
        _server_config += "}";

        _server_config = "\"common\":" + _server_config;
        _server_config += ",\"generalparams\":" + _json;


        return "{" + _server_config + "," + _configuration + "}";
      }
      catch (Exception _ex)
      {

        Logger.Exception(HttpContext.Current, "GetLayoutConfiguration()", _ex);
      }

      return String.Empty;
    }

    private static string AdjustDasboard(string p)
    {
      List<LayoutMenuItem> _widgets = null;
      JArray _dashboard;
      JArray _dashboardFinal=new JArray();

      //fill widgets
      _widgets = GetWidgets();
      
      _dashboard = JArray.Parse(p);
      foreach (JObject j in _dashboard)  //paso por roles
      {
        int _rol=0;
        if (j.Property("1") != null) //ops
         _rol= 1;
        if (j.Property("2") != null) //ops
         _rol= 2;
        if (j.Property("4") != null) //ops
         _rol= 4;
        if (j.Property("8") != null) //ops
         _rol= 8;
        
        JArray _tempArray=new JArray();

        JArray _rolValues = JArray.Parse(j.Property(_rol.ToString()).Value.ToString());
        var _vacio=_rolValues[0];
        _rolValues[0].Remove();
        foreach (JObject ob in _rolValues)  //paso por los widgets..supuestamente
          {
              var _found = (from o in _widgets where o.Rol == (EN_ROLES)_rol && o.NLS == ob.Property("id").Value.ToString()  select o).Count();
              if (_found > 0)
                _tempArray.Add(ob);
 
          }
          _tempArray = JArray.Parse("[" + string.Join<JToken>(",", _tempArray.OrderBy(x => x["y"]).ThenBy(x => x["x"]).ToArray()) + "]");
          int paso = 0;  
        foreach (LayoutMenuItem mlay in _widgets.Where(x => x.Rol == (EN_ROLES)_rol))
          {
            var _found = _tempArray.Where(x => x["id"].ToString() == mlay.NLS).Count();
            if (_found == 0)
            {
              paso++;
              JObject _object = new JObject();
              _object.Add("id",mlay.NLS);
            _object.Add("x",99);
              _object.Add("y",99);
              _object.Add("width",1);
              _object.Add("height", 6);
              _object.Add("type", mlay.Script);
              _object.Add("canResize", false);
              _object.Add("canMove", false);
              
              _tempArray.Add(_object);
            }
          }
        int _x = 0;
        int _y=0;
        foreach (JObject ob in _tempArray)
        {
          ob["x"] = _x;
          ob["y"] = _y;
          if (_x == 3)
          {
            _x = 0;
            _y = _y + 6;
          }
          else
            _x++;

        }
            JObject objectRol=new JObject();
            _tempArray.AddFirst(_vacio);
            objectRol.Add(_rol.ToString(), _tempArray);
            _dashboardFinal.Add(objectRol);
        }



      return _dashboardFinal.ToString();
    } //GetLayoutConfiguration


    private static List<LayoutMenuItem> GetWidgets()
    {

      List<LayoutMenuItem> _items = null;
      List<LayoutMenuItem> _itemsOPS = null;
      List<LayoutMenuItem> _itemsPCA = null;
      List<LayoutMenuItem> _itemsSLO = null;
      List<LayoutMenuItem> _itemsTCH = null;
      _itemsOPS = Global.Sessions.Information[HttpContext.Current.Session.SessionID].User.Permissions.GetProfileMenuItemsByLevel(EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ROLES.OPS);
      _itemsPCA = Global.Sessions.Information[HttpContext.Current.Session.SessionID].User.Permissions.GetProfileMenuItemsByLevel(EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ROLES.PCA);
      _itemsSLO = Global.Sessions.Information[HttpContext.Current.Session.SessionID].User.Permissions.GetProfileMenuItemsByLevel(EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ROLES.SLO);
      _itemsTCH = Global.Sessions.Information[HttpContext.Current.Session.SessionID].User.Permissions.GetProfileMenuItemsByLevel(EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ROLES.TCH);

      _items = _itemsOPS.Concat(_itemsPCA)
                                   .Concat(_itemsSLO).Concat(_itemsTCH)
                                   .ToList();
      return _items;
    }

    public static void LoadGeneralParams(ref Dictionary<string, string> GeneralP)
    {
      GeneralP.Add("Casino.Name", GeneralParam.GetString("Site", "Name", ""));
      GeneralP.Add("Alarms.HighRoller.Enabled", GeneralParam.GetString("Alarms", "HighRoller.Enabled", "0"));
      GeneralP.Add("Alarms.HighRoller.Limit.BetAverage", GeneralParam.GetString("Alarms", "HighRoller.Limit.BetAverage", "0"));
      GeneralP.Add("Alarms.HighRoller.Limit.CoinIn", GeneralParam.GetString("Alarms", "HighRoller.Limit.CoinIn", "0"));
      GeneralP.Add("RegionalOptions.CountryISOCode2", GeneralParam.GetString("RegionalOptions", "CountryISOCode2", "MX"));
      GeneralP.Add("RegionalOptions.CountryISOCode", GeneralParam.GetString("RegionalOptions", "CountryISOCode", "MXN"));
      GeneralP.Add("Intellia.Site.Capacity", GeneralParam.GetString("Intellia", "Site.Capacity", "0"));
    }

    public static String GetLayoutConfigurationFromSession()
    {

      return GetLayoutConfiguration();
    } //GetLayoutConfigurationFromSession

    /// <summary>
    /// Get the historification of the terminal from sales_per_hour table
    /// </summary>
    /// <param name="TerminalId"></param>
    /// <returns></returns>
    public static String GetTerminalHistory(Int64 TerminalId)
    {
      String _result = "";
      DataTable _dt_terminal_history = new DataTable();
      DateTime _history_date;
      Int64 _idx_days = 0;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (WebConfigurationManager.AppSettings["TestMode"].ToString() == "true")
          {
            _history_date = Misc.Opening(DateTime.Parse(WebConfigurationManager.AppSettings["TestModeDate"].ToString()));
          }
          else
          {
            _history_date = Misc.TodayOpening();
          }


          for (_idx_days = 0; _idx_days < CONST_NUMBER_OF_DAYS_BEFORE; _idx_days++)
          {
            if (!Trx_GetTerminalHistoryByDate(TerminalId, _history_date.AddDays(-_idx_days), ref _dt_terminal_history, _db_trx.SqlTransaction))
            {
              Logger.Write(HttpContext.Current, "Error Trx_GetTerminalHistoryByDate");
            }
          }
        }

        _result = "{\"LastNDays\":" + clsDefaultConverter.DataTableToReport(_dt_terminal_history) + "}";

        return _result;
      }
      catch (Exception _ex)
      {
        Logger.Exception(HttpContext.Current, "GetTerminalHistory()", _ex);
      }

      return string.Empty;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="TerminalId"></param>
    /// <param name="Date"></param>
    /// <param name="Trx"></param>
    public static Boolean Trx_GetTerminalHistoryByDate(Int64 TerminalId, DateTime Date, ref  DataTable DtTerminalHistory, SqlTransaction Trx)
    {
      DataTable _dt;

      try
      {
        using (SqlCommand _cmd = new SqlCommand("Layout_GetTerminalHistory", Trx.Connection, Trx))
        {
          _cmd.CommandType = CommandType.StoredProcedure;
          _cmd.CommandTimeout = 2 * 60; // 2 minutes
          _cmd.Parameters.Add("@TerminalId", SqlDbType.BigInt).Value = TerminalId;
          _cmd.Parameters.Add("@DateStart", SqlDbType.DateTime).Value = Date;
          _cmd.Parameters.Add("@DateEnd", SqlDbType.DateTime).Value = Date.AddDays(1);

          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
          {
            using (DataSet _dataset = new DataSet())
            {
              _sql_da.Fill(_dataset);

              _dt = _dataset.Tables[0];
            }
          }
        }

        if (DtTerminalHistory.Rows.Count == 0)
        {
          DtTerminalHistory = _dt;

          if (DtTerminalHistory.Rows.Count == 0)
          {
            DtTerminalHistory.Rows.Add();
          }
        }
        else
        {
          if (_dt.Rows.Count > 0)
          {
            DtTerminalHistory.ImportRow(_dt.Rows[0]);
          }
          else
          {
            DtTerminalHistory.Rows.Add();
          }
        }
        return true;

      }
      catch (Exception _ex)
      {
        Log.Message("Trx_GetTerminalHistoryByDate");
        Log.Exception(_ex);
      }

      return false;
    }



    public static String GetLayoutRanges(EN_ROLES CurrentRole)
    {
      DataSet _ranges = new DataSet();
      Int32 _user_id;

      try
      {
        _user_id = 0;

        if (Global.Sessions.Information[HttpContext.Current.Session.SessionID].User.Id == null  || !Int32.TryParse(Global.Sessions.Information[HttpContext.Current.Session.SessionID].User.Id.ToString(), out _user_id))
        {
          Logger.Write(HttpContext.Current, "GetLayoutRanges() | ERROR: UserId is null.");

          return String.Empty;
        }

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand("Layout_GetRanges", _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pRole", SqlDbType.Int).Value = (Int32)CurrentRole;
            _cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = _user_id;

            _cmd.CommandType = CommandType.StoredProcedure;
            _cmd.CommandTimeout = 2 * 60; // 2 minutes

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
            {
              _sql_da.Fill(_ranges);
            }
          }
        }
      }

      catch (Exception _ex)
      {
        Logger.Exception(HttpContext.Current, "GetLayoutRanges()", _ex);
      }

      List<String> _prop_names = new List<String>();
      _prop_names.Add("Ranges");
      _prop_names.Add("Legends");
      _prop_names.Add("CustomFilters");
      _prop_names.Add("AlarmsDictionary");

      return clsJsonConverters.ConvertDataSetToJson(_prop_names, _ranges);

    } //GetLayoutConfiguration

    /// <summary>
    /// Save configuration properties on JSON format into DB
    /// </summary>
    /// <param name="Configuration"></param>
    /// <returns>Boolean</returns>

    public static Boolean SaveLayoutConfig(String Configuration, String Dashboard)
    {
      Int32 _user_id;
      string _dashboardFinal;
      string _idxRol;
      JArray _dashboardComplete;

      _user_id = 0;
      _dashboardFinal = "[";

      if (Global.Sessions.Information[HttpContext.Current.Session.SessionID].User.Id == null || !Int32.TryParse(Global.Sessions.Information[HttpContext.Current.Session.SessionID].User.Id.ToString(), out _user_id))
      {
        Logger.Write(HttpContext.Current, "SaveLayoutConfig() | ERROR: UserId is null.");

        return false;
      }
      _idxRol = ((int)Global.Sessions.Information[HttpContext.Current.Session.SessionID].User.CurrentRole).ToString();
      _dashboardComplete = JArray.Parse(Global.Sessions.Information[HttpContext.Current.Session.SessionID].Dashboard.ToString());
      foreach (JObject content in _dashboardComplete.Children<JObject>())
      {
        foreach (JProperty prop in content.Properties())
        {
          // Only override the current role id value
          if (prop.Name == _idxRol)
          {
            _dashboardFinal += "{" + prop.Name + ":" + Dashboard + "},";
          }
          else
          {
            _dashboardFinal += "{" + prop.Name + ":" + prop.Value.ToString() + "},";
          }
          //prop.Value =JToken.Parse(Dashboard);
        }
      }
      _dashboardFinal = _dashboardFinal.Substring(0, _dashboardFinal.Length - 1) + "]";


      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand("Layout_SaveConfiguration", _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.CommandType = CommandType.StoredProcedure;

            // Params Adding
            _cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = _user_id;
            _cmd.Parameters.Add("@pParameters", SqlDbType.VarChar).Value = Configuration;
            _cmd.Parameters.Add("@pDashboard", SqlDbType.VarChar).Value = _dashboardFinal;
            _cmd.Parameters.Add("@pLastUpdate", SqlDbType.DateTime).Value = WGDB.Now;
            _cmd.Parameters.Add("@pLastProfileUpdate", SqlDbType.VarChar ).Value =m_last_profile_update;
            _cmd.CommandTimeout = 2 * 60; // 2 minutes

            if (!(_cmd.ExecuteNonQuery() > 0))
            {
              return false;
            }

            _db_trx.Commit();
          }
        }

        return true;
      }

      catch (Exception _ex)
      {

        Logger.Exception(HttpContext.Current, "SaveLayoutConfig()", _ex);
      }

      return false;
    } //SaveLayoutConfig

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    private static Boolean NeedToRecordScene()
    {
      if (m_last_recording_update != DateTime.MinValue)
      {
        TimeSpan ts = DateTime.Now - m_last_recording_update;

        if (ts.Minutes < 1)
        {
          return false;
        }
      }

      m_last_recording_update = DateTime.Now;

      return true;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="Json"></param>
    /// <param name="NewScene"></param>
    /// <returns></returns>
    private static Boolean RecordScene(String Json, Boolean NewScene)
    {
      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand("Layout_SceneRecording", _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.CommandType = CommandType.StoredProcedure;

            // Params Adding
            _cmd.Parameters.Add("@pFloorId", SqlDbType.Int).Value = 1;
            _cmd.Parameters.Add("@pJson", SqlDbType.NVarChar).Value = Json;
            _cmd.Parameters.Add("@pNewScene", SqlDbType.Bit).Value = NewScene;
            _cmd.CommandTimeout = 2 * 60; // 2 minutes

            if (_cmd.ExecuteNonQuery() != 1)
            {
              return false;
            }

            _db_trx.Commit();
          }
        }

        return true;
      }

      catch (Exception _ex)
      {
        Logger.Exception(HttpContext.Current, "RecordScene()", _ex);
        return false;
      }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="Name"></param>
    /// <param name="FilterParameters"></param>
    /// <param name="FilterDisplay"></param>
    /// <param name="ID"></param>
    /// <returns></returns>
    public static Int32 SaveCustomAlarm(String Name, String FilterParameters, String FilterDisplay, Int32 ID)
    {
      Int32 _user_id;
      Int32 _custom_alarm_id;

      _user_id = 0;
      _custom_alarm_id = 0;

      if (Global.Sessions.Information[HttpContext.Current.Session.SessionID].User.Id == null || !Int32.TryParse(Global.Sessions.Information[HttpContext.Current.Session.SessionID].User.Id.ToString(), out _user_id))
      {
        Logger.Write(HttpContext.Current, "SaveCustomAlarm() | ERROR: UserId is null.");

        return 0;
      }

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand("Layout_SaveCustomFilter", _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            SqlParameter _sql_custom_alarm_id;

            _cmd.CommandType = CommandType.StoredProcedure;

            // Params Adding
            _cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = _user_id;
            _cmd.Parameters.Add("@pCustomFilterName", SqlDbType.VarChar).Value = Name;
            _cmd.Parameters.Add("@pParameters", SqlDbType.VarChar).Value = FilterParameters;
            _cmd.Parameters.Add("@pDisplay", SqlDbType.VarChar).Value = FilterDisplay;

            //INSERT
            if (ID == 0)
            {
              _cmd.Parameters.Add("@pFilterId", SqlDbType.Int).Value = ID;
              _sql_custom_alarm_id = _cmd.Parameters.Add("@pOutputFilterId", SqlDbType.Int);
              _sql_custom_alarm_id.Direction = ParameterDirection.Output;

              _cmd.CommandTimeout = 2 * 60; // 2 minutes

              if (!(_cmd.ExecuteNonQuery() > 0))
              {
                return 0;
              }

              _custom_alarm_id = (Int32)_sql_custom_alarm_id.Value;

            }
            //UPDATE
            else
            {
              _cmd.Parameters.Add("@pFilterId", SqlDbType.Int).Value = ID;
              _sql_custom_alarm_id = _cmd.Parameters.Add("@pOutputFilterId", SqlDbType.Int);
              _sql_custom_alarm_id.Direction = ParameterDirection.Output;

              _cmd.CommandTimeout = 2 * 60; // 2 minutes

              if (!(_cmd.ExecuteNonQuery() > 0))
              {
                return 0;
              }
            }

            _db_trx.Commit();
          }
        }

        return _custom_alarm_id;
      }

      catch (Exception _ex)
      {

        Logger.Exception(HttpContext.Current, "SaveCustomAlarm()", _ex);
      }

      return 0;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="AccountId"></param>
    /// <returns></returns>
    public static String GetPlayerInfoData(Int64 AccountId)
    {
      String _result = "";
      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand("Layout_GetPlayerInfo", _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.CommandType = CommandType.StoredProcedure;
            _cmd.CommandTimeout = 2 * 60; // 2 minutes
            _cmd.Parameters.Add("@AccountId", SqlDbType.BigInt).Value = AccountId;

            if (WebConfigurationManager.AppSettings["TestMode"].ToString() == "true")
            {
              _cmd.Parameters.Add("@Date", SqlDbType.DateTime).Value = DateTime.Parse(WebConfigurationManager.AppSettings["TestModeDate"].ToString());
            }
            else
            {
              _cmd.Parameters.Add("@Date", SqlDbType.DateTime).Value = Misc.TodayOpening();

            }


            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
            {
              using (DataSet _dataset = new DataSet())
              {
                _sql_da.Fill(_dataset);

                _result = "{\"WorkingDay\":" + clsDefaultConverter.DataTableToReport(_dataset.Tables[0]) + "," +
                          "\"LastNDays\":" + clsDefaultConverter.DataTableToReport(_dataset.Tables[1]) + "}";
              }
            }
          }
        }
        return _result;
      }
      catch (Exception _ex)
      {
        Logger.Exception(HttpContext.Current, "GetPlayerInfoData()", _ex);
      }

      return string.Empty;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="RunnerId"></param>
    /// <returns></returns>
    public static String GetRunnerTasks(Int64 RunnerId)
    {
      String _result = "";
      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand("Layout_GetRunnerTasks", _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.CommandType = CommandType.StoredProcedure;
            _cmd.CommandTimeout = 2 * 60; // 2 minutes
            _cmd.Parameters.Add("@pRunnerId", SqlDbType.BigInt).Value = RunnerId;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
            {
              using (DataSet _dataset = new DataSet())
              {
                _sql_da.Fill(_dataset);

                _result = "{\"Current\":" + clsDefaultConverter.DataTableToReport(_dataset.Tables[0]) + "," +
                          "\"Last\":" + clsDefaultConverter.DataTableToReport(_dataset.Tables[1]) + "}";
              }
            }
          }
        }
        return _result;
      }
      catch (Exception _ex)
      {
        Logger.Exception(HttpContext.Current, "GetPlayerInfoData()", _ex);
      }

      return string.Empty;
    }


    /// <summary>
    /// Return the runners information currently in the threat
    /// </summary>
    /// <returns></returns>
    public static String GetRunnersInformation()
    {
      try
      {
        String _data = Global.LayoutData.DataRunners;
        return _data;

      }
      catch (Exception _ex)
      {
        Logger.Exception(HttpContext.Current, "GetRunnersInformation()", _ex);
        //throw;
      }

      return String.Empty;
    }

    //TODO
    public static String GetTerminalInfoData(Int64 TerminalId)
    {
      String _result = "";
      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {

          using (SqlCommand _cmd = new SqlCommand("Layout_GetTerminalInfo", _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.CommandType = CommandType.StoredProcedure;
            _cmd.CommandTimeout = 2 * 60; // 2 minutes
            _cmd.Parameters.Add("@TerminalId", SqlDbType.BigInt).Value = TerminalId;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
            {
              using (DataSet _dataset = new DataSet())
              {
                _sql_da.Fill(_dataset);

                _result = "{\"LastNDays\":" + clsDefaultConverter.DataTableToReport(_dataset.Tables[0]) + "," +
                          "\"DataAvg\":" + clsDefaultConverter.DataTableToReport(_dataset.Tables[1]) + "}";
              }
            }
          }
        }
        return _result;
      }
      catch (Exception _ex)
      {
        Logger.Exception(HttpContext.Current, "GetPlayerInfoData()", _ex);
      }

      return string.Empty;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public static String GetDefaultUserDashBoard()
    {
      string _result;

      _result = "";
      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand("Layout_GetDashboardRoles", _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.CommandType = CommandType.StoredProcedure;
            _cmd.CommandTimeout = 2 * 60; // 2 minutes

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
            {
              using (DataSet _data = new DataSet())
              {
                _sql_da.Fill(_data);

                if (_data.Tables[0].Rows[0][0] != DBNull.Value && (String)_data.Tables[0].Rows[0][0] != String.Empty)
                {
                  _result = (String)_data.Tables[0].Rows[0][0];
                }


              }
              return _result;

            }
          }
        }
      }

      catch (Exception _ex)
      {

        Logger.Exception(HttpContext.Current, "GetDefaultUserDashBoard()", _ex);
      }

      return String.Empty;

      //_result.Append("{\"id\":\"widgetTestList1\",\"x\":2,\"y\":6,\"width\":1,\"height\":8,\"type\":\"WidgetHighChart\",\"canResize\":false,\"canMove\":true}");
      //_result.Append(",{\"id\":\"widgetTestList2\",\"x\":1,\"y\":6,\"width\":1,\"height\":8,\"type\":\"WidgetListBase\",\"canResize\":false,\"canMove\":true}");
      ////_result.Append(",{\"id\":\"widgetIndicator1\",\"x\":3,\"y\":13,\"width\":1,\"height\":3,\"type\":\"WidgetBase\",\"canResize\":false,\"canMove\":true}");
      //_result.Append(",{\"id\":\"widgetIndicator2\",\"x\":3,\"y\":16,\"width\":1,\"height\":3,\"type\":\"WidgetBase\",\"canResize\":false,\"canMove\":true}");
      //_result.Append(",{\"id\":\"widgettheater2\",\"x\":1,\"y\":14,\"width\":1,\"height\":6,\"type\":\"WidgetSVGAmphitheater\",\"canResize\":false,\"canMove\":true}");
      //_result.Append(",{\"id\":\"widgettheater3\",\"x\":2,\"y\":14,\"width\":1,\"height\":7,\"type\":\"WidgetSVGAmphitheater\",\"canResize\":false,\"canMove\":true}");
      //_result.Append(",{\"id\":\"widgetOccupancy1\",\"x\":0,\"y\":14,\"width\":1,\"height\":6,\"type\":\"WidgetSVGAmphitheater\",\"canResize\":false,\"canMove\":true}");
      //_result.Append(",{\"id\":\"widgetTasks1\",\"x\":3,\"y\":5,\"width\":1,\"height\":8,\"type\":\"WidgetBase\",\"canResize\":false,\"canMove\":true}");
      ////_result.Append(",{\"id\":\"widgetColumns1\",\"x\":3,\"y\":19,\"width\":1,\"height\":4,\"type\":\"WidgetColumnsBase\",\"canResize\":false,\"canMove\":true}");
      //_result.Append(",{\"id\":\"widgetComparation1\",\"x\":0,\"y\":0,\"width\":1,\"height\":6,\"type\":\"WidgetBase\",\"canResize\":false,\"canMove\":true}");
      //_result.Append(",{\"id\":\"widgetChart1\",\"x\":1,\"y\":0,\"width\":1,\"height\":6,\"type\":\"WidgetHighChart\",\"canResize\":false,\"canMove\":true}");
      ////_result.Append(",{\"id\":\"widgetHBar1\",\"x\":3,\"y\":2,\"width\":1,\"height\":3,\"type\":\"WidgetBase\",\"canResize\":false,\"canMove\":true}");
      //_result.Append(",{\"id\":\"widgetChart2\",\"x\":3,\"y\":23,\"width\":1,\"height\":6,\"type\":\"WidgetSVGChart\",\"canResize\":false,\"canMove\":true}");
      //_result.Append(",{\"id\":\"widgetDonut1\",\"x\":2,\"y\":0,\"width\":1,\"height\":6,\"type\":\"WidgetSVGDonutChart\",\"canResize\":false,\"canMove\":true}");
      //_result.Append(",{\"id\":\"widgetVipsList\",\"x\":0,\"y\":6,\"width\":1,\"height\":8,\"type\":\"WidgetListBase\",\"canResize\":false,\"canMove\":true}");
      //_result.Append(",{\"id\":\"widgetIndicator3\",\"x\":0,\"y\":6,\"width\":1,\"height\":3,\"type\":\"WidgetBase\",\"canResize\":false,\"canMove\":true}");
      //_result.Append(",{\"id\":\"widgetMachineOccupation\",\"x\":2,\"y\":0,\"width\":1,\"height\":6,\"type\":\"WidgetSVGDonutChart\",\"canResize\":false,\"canMove\":true}");
      //_result.Append(",{\"id\":\"widgetHRList\",\"x\":2,\"y\":6,\"width\":1,\"height\":8,\"type\":\"WidgetListBase\",\"canResize\":false,\"canMove\":true}");
      //_result.Append(",{\"id\":\"widgetAgeSegmentation\",\"x\":0,\"y\":0,\"width\":1,\"height\":6,\"type\":\"WidgetSVGDonutChart\",\"canResize\":false,\"canMove\":true}");
      //_result.Append(",{\"id\":\"widgetLevelSegmentation\",\"x\":0,\"y\":0,\"width\":1,\"height\":6,\"type\":\"WidgetSVGDonutChart\",\"canResize\":false,\"canMove\":true}");
      //_result.Append(",{\"id\":\"widgetRunnersList\",\"x\":0,\"y\":0,\"width\":1,\"height\":8,\"type\":\"WidgetListBase\",\"canResize\":false,\"canMove\":true}");

      /*
       version antes de generar dashboard por rol
            _result.Append("[");
            _result.Append("{\"id\":\"widgetTestList1\",\"x\":0,\"y\":0,\"width\":1,\"height\":8,\"type\":\"WidgetHighChart\",\"canResize\":false,\"canMove\":true}");
            _result.Append(",{\"id\":\"widgetTestList2\",\"x\":0,\"y\":0,\"width\":1,\"height\":8,\"type\":\"WidgetListBase\",\"canResize\":false,\"canMove\":true}");
            //_result.Append(",{\"id\":\"widgetIndicator1\",\"x\":0,\"y\":0,\"width\":1,\"height\":3,\"type\":\"WidgetBase\",\"canResize\":false,\"canMove\":true}");
            //_result.Append(",{\"id\":\"widgetIndicator2\",\"x\":0,\"y\":0,\"width\":1,\"height\":3,\"type\":\"WidgetBase\",\"canResize\":false,\"canMove\":true}");
            _result.Append(",{\"id\":\"widgettheater2\",\"x\":0,\"y\":0,\"width\":1,\"height\":6,\"type\":\"WidgetSVGAmphitheater\",\"canResize\":false,\"canMove\":true}");
            _result.Append(",{\"id\":\"widgettheater3\",\"x\":0,\"y\":0,\"width\":1,\"height\":6,\"type\":\"WidgetSVGAmphitheater\",\"canResize\":false,\"canMove\":true}");
            _result.Append(",{\"id\":\"widgetOccupancy1\",\"x\":0,\"y\":0,\"width\":1,\"height\":6,\"type\":\"WidgetSVGAmphitheater\",\"canResize\":false,\"canMove\":true}");
            _result.Append(",{\"id\":\"widgetTasks1\",\"x\":0,\"y\":0,\"width\":1,\"height\":8,\"type\":\"WidgetBase\",\"canResize\":false,\"canMove\":true}");
            //_result.Append(",{\"id\":\"widgetColumns1\",\"x\":0,\"y\":0,\"width\":1,\"height\":4,\"type\":\"WidgetColumnsBase\",\"canResize\":false,\"canMove\":true}");
            _result.Append(",{\"id\":\"widgetComparation1\",\"x\":0,\"y\":0,\"width\":1,\"height\":6,\"type\":\"WidgetBase\",\"canResize\":false,\"canMove\":true}");
            _result.Append(",{\"id\":\"widgetChart1\",\"x\":0,\"y\":0,\"width\":1,\"height\":6,\"type\":\"WidgetHighChart\",\"canResize\":false,\"canMove\":true}");
            //_result.Append(",{\"id\":\"widgetHBar1\",\"x\":0,\"y\":0,\"width\":1,\"height\":3,\"type\":\"WidgetBase\",\"canResize\":false,\"canMove\":true}");
            _result.Append(",{\"id\":\"widgetChart2\",\"x\":0,\"y\":0,\"width\":1,\"height\":6,\"type\":\"WidgetSVGChart\",\"canResize\":false,\"canMove\":true}");
            _result.Append(",{\"id\":\"widgetDonut1\",\"x\":0,\"y\":0,\"width\":1,\"height\":6,\"type\":\"WidgetSVGDonutChart\",\"canResize\":false,\"canMove\":true}");
            _result.Append(",{\"id\":\"widgetVipsList\",\"x\":0,\"y\":0,\"width\":1,\"height\":8,\"type\":\"WidgetListBase\",\"canResize\":false,\"canMove\":true}");
            _result.Append(",{\"id\":\"widgetIndicator3\",\"x\":0,\"y\":0,\"width\":1,\"height\":3,\"type\":\"WidgetBase\",\"canResize\":false,\"canMove\":true}");
            _result.Append(",{\"id\":\"widgetMachineOccupation\",\"x\":0,\"y\":0,\"width\":1,\"height\":6,\"type\":\"WidgetSVGDonutChart\",\"canResize\":false,\"canMove\":true}");
            _result.Append(",{\"id\":\"widgetHRList\",\"x\":0,\"y\":0,\"width\":1,\"height\":8,\"type\":\"WidgetListBase\",\"canResize\":false,\"canMove\":true}");
            _result.Append(",{\"id\":\"widgetAgeSegmentation\",\"x\":0,\"y\":0,\"width\":1,\"height\":6,\"type\":\"WidgetSVGDonutChart\",\"canResize\":false,\"canMove\":true}");
            _result.Append(",{\"id\":\"widgetLevelSegmentation\",\"x\":0,\"y\":0,\"width\":1,\"height\":6,\"type\":\"WidgetSVGDonutChart\",\"canResize\":false,\"canMove\":true}");
            _result.Append(",{\"id\":\"widgetRunnersList\",\"x\":0,\"y\":0,\"width\":1,\"height\":8,\"type\":\"WidgetListBase\",\"canResize\":false,\"canMove\":true}");
            _result.Append(",{\"id\":\"widgetBirthday\",\"x\":0,\"y\":0,\"width\":1,\"height\":8,\"type\":\"WidgetListBase\",\"canResize\":false,\"canMove\":true}");
            _result.Append(",{\"id\":\"widgetPlayersMaxLevel\",\"x\":0,\"y\":0,\"width\":1,\"height\":8,\"type\":\"WidgetListBase\",\"canResize\":false,\"canMove\":true}");
            _result.Append("]");

            return _result.ToString();
       */
    }

    /// <summary>
    /// Delete selected custom filter from DataBase
    /// </summary>
    /// <param name="CustomFilterId">long custom filter id</param>
    /// <returns>true/false</returns>
    public static bool DeteleCustomFilter(long CustomFilterId)
    {
      StringBuilder _sb;
      Int32 _nr;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" DELETE FROM  LAYOUT_USERS_CUSTOM_FILTERS    ");
        _sb.AppendLine("       WHERE  LCF_ID = @pCustomFilterId      ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pCustomFilterId", SqlDbType.BigInt).Value = CustomFilterId;

            _nr = _db_trx.ExecuteNonQuery(_sql_cmd);

          } //SqlCommand _sql_cmd

          _db_trx.Commit();

          return _nr > 0;

        } //DB_TRX _db_trx
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        //throw _ex;
      }

      return true;
    }
  }
}
