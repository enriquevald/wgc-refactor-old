﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using WSI.Layout.Data.classes;

namespace WSI.Layout.classes
{
  public class Dashboard
  {
    public static String GetSegmentationLevelsClients()
    {
      //return Global.LayoutDashboard.SegmentationLevelClients;
      return "{\"columns\":[\"TD_AGROUP\",\"Mar  1 2014 12:00AM\",\"Apr  1 2014 12:00AM\",\"May  1 2014 12:00AM\",\"Jun  1 2014 12:00AM\",\"Oct  1 2014 12:00AM\",\"Feb  1 2015 12:00AM\"],\"rows\":[[\"0\",\"40\",\"20\",\"30\",\"76\",\"80\",\"90\"],[\"1\",\"70\",\"20\",\"10\",\"115\",\"35\",\"66\"],[\"2\",\"80\",\"30\",\"60\",\"25\",\"50\",\"15\"],[\"3\",\"55\",\"60\",\"30\",\"20\",\"67\",\"43\"],[\"4\",\"43\",\"60\",\"5\",\"15\",\"72\",\"80\"]]}";
    }

    public static String GetSegmentationLevelsVisits()
    {
      //return Global.LayoutDashboard.SegmentationLevelVisits;
      return "{\"columns\":[\"TD_AGROUP\",\"Mar  1 2014 12:00AM\",\"Apr  1 2014 12:00AM\",\"May  1 2014 12:00AM\",\"Jun  1 2014 12:00AM\",\"Oct  1 2014 12:00AM\",\"Feb  1 2015 12:00AM\"],\"rows\":[[\"0\",\"80\",\"30\",\"60\",\"25\",\"50\",\"15\"],[\"1\",\"55\",\"60\",\"30\",\"20\",\"67\",\"43\"],[\"2\",\"43\",\"60\",\"5\",\"15\",\"72\",\"80\"],[\"3\",\"70\",\"20\",\"10\",\"115\",\"35\",\"66\"],[\"4\",\"40\",\"20\",\"30\",\"76\",\"80\",\"90\"]]}";
    }

    public static String GetCurrentPromotions()
    {
      return "{\"columns\":[\"acp_promo_id\",\"acp_promo_type\",\"acp_promo_name\",\"acp_credit_type\",\"acp_amount\"],\"rows\":[[\"736\",\"13\",\"Sorteo Cortesía NR\",\"1\",\"360.90\"],[\"740\",\"17\",\"Sorteo No Participa\",\"3\",\"656.77\"],[\"817\",\"1\",\"Bonus NR\",\"1\",\"19.00\"],[\"973\",\"0\",\"* anm2\",\"1\",\"2.00\"],[\"695\",\"11\",\"Promoción Divisa 4\",\"1\",\"12.19\"],[\"987\",\"0\",\"RMS PROMO 001\",\"3\",\"5.00\"],[\"430\",\"0\",\"* PromoBOX Test\",\"1\",\"242.00\"],[\"738\",\"15\",\"Sorteo Premio NR\",\"1\",\"140.60\"]]}";
    }
  }
}