﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WSI.Common;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace WSI.Layout.classes.editor
{
  public static class clsFloor
  {

    private static DataTable toDataTable(dynamic JData)
    {
      var result = new DataTable();
      foreach (var row in JData)
      {
        foreach (var jToken in row)
        {
          var jproperty = jToken as JProperty;
          if (jproperty == null) continue;
          if (result.Columns[jproperty.Name] == null)
            result.Columns.Add(jproperty.Name, typeof(string));
        }
      }
      foreach (var row in JData)
      {
        var datarow = result.NewRow();
        foreach (var jToken in row)
        {
          var jProperty = jToken as JProperty;
          if (jProperty == null) continue;
          datarow[jProperty.Name] = jProperty.Value.ToString();
        }
        result.Rows.Add(datarow);
      }

      return result;
    }

    //---------------------------------------------------------------------------------------------------------------------------------------------------------

    public static DataSet GetLayout(int FloorId)
    {
      String _procedure = String.Empty;
      _procedure = "Layout_LoadFloor";

      DataSet _layout = new DataSet();

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_procedure, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.CommandType = CommandType.StoredProcedure;

            _cmd.Parameters.Add("@pFloorId", SqlDbType.Int).Value = FloorId;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
            {
              _sql_da.Fill(_layout);
            }

          }

        }

      }

      catch (Exception _ex)
      {
        Logger.Exception(HttpContext.Current, "clsFloor.GetLayout()", _ex);
      }

      return _layout;

    }

    //---------------------------------------------------------------------------------------------------------------------------------------------------------

    private static String GetFieldValue(Object Field)
    {
      if (Field == DBNull.Value || Field == "")
      {
        return "NULL";
      }
      else
      {
        return ((String)Field).Replace(',','.');
      }
    }

    public static Boolean Save(String FloorData)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return Save(FloorData, _db_trx.SqlTransaction);
      }
    }

    public static Boolean Save(String FloorData, SqlTransaction Trx)
    {
      Boolean _result = false;
      String _str = String.Empty;
      String _str_command = String.Empty;
      DataTable _changes_layout;

      if (FloorData != "")
      {
        dynamic _floor_changes = JsonConvert.DeserializeObject(FloorData, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });

        // Fill new layout data
        _changes_layout = clsDefaultConverter.DataTableFromReport(_floor_changes);

        // Compare data
        _result = Save(_changes_layout, Trx);

      }

      // Return floorId
      return _result;
    }

    public static Boolean Save(DataTable ChangesLayout)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return Save(ChangesLayout, _db_trx.SqlTransaction);
      }
    }

    public static Boolean Save(DataTable ChangesLayout, SqlTransaction Trx)
    {
      Boolean _result = false;
      String _str = String.Empty;
      String _str_command = String.Empty;
      DataRow _change_row;

      try
      {

          // Compare data

            SqlCommand _cmd = new SqlCommand();

            _str_command += "DECLARE   @RowId AS BIGINT \n";
            _str_command += "DECLARE   @ObjectCurrentLocationId AS BIGINT \n";
            _str_command += "DECLARE   @Now AS DATETIME \n";
            _str_command += "SET   @Now = GETDATE() \n";
            
            // Set connection, etc.
            EN_OBJECT_STATUS _row_status;
            for (int i = 0; i < ChangesLayout.Rows.Count; i++)
            {
              _change_row = ChangesLayout.Rows[i];

              _str = String.Empty;
              _str += "\n";
              _str += "-- CHANGE " + i + " -- \n";

              _row_status = (EN_OBJECT_STATUS) Enum.Parse(typeof(EN_OBJECT_STATUS), (string)_change_row["lo_status"]);
              if (((String)_change_row["lo_id"]) == "")
              {
                if (_row_status == EN_OBJECT_STATUS.OS_DELETED)
                  continue;

                // Insert new object 
                _str += "INSERT   INTO layout_objects \n";
                _str += "       ( lo_external_id, lo_type, lo_parent_id, lo_mesh_id, lo_creation_date, lo_update_date, lo_status ) \n";
                _str += "VALUES ( @1@, @2@, @3@, @4@, @Now, NULL, @5@ ) \n";
                _str += "SET   @RowId = @@IDENTITY \n";
                _str += "\n";

                _str = _str.Replace("@1@", GetFieldValue(_change_row["lo_external_id"]));
                _str = _str.Replace("@2@", GetFieldValue(_change_row["lo_type"]));
                _str = _str.Replace("@3@", GetFieldValue(_change_row["lo_parent_id"]));
                _str = _str.Replace("@4@", GetFieldValue(_change_row["lo_mesh_id"]));
                _str = _str.Replace("@5@", ((Int32)EN_OBJECT_STATUS.OS_INSERTED).ToString());

                // Insert object location
                _str += "INSERT   INTO layout_object_locations \n";
                _str += "        ( lol_object_id, lol_location_id, lol_date_from, lol_date_to, lol_orientation, lol_area_id, lol_bank_id, lol_bank_location, lol_current_location, lol_offset_x, lol_offset_y ) \n";
                _str += " VALUES ( @RowId, @1@, @Now, NULL, @2@, @3@, @4@, @5@, 1, @6@, @7@ ) \n";

                _str = _str.Replace("@1@", ((String)_change_row["lol_location_id"]));
                string orientationValue = "";
                if (Convert.ToInt32(GetFieldValue(_change_row["lol_orientation"])) < 0)
                  orientationValue = (360 + Convert.ToInt32(GetFieldValue(_change_row["lol_orientation"]))).ToString();
                else
                  orientationValue = GetFieldValue(_change_row["lol_orientation"]);

                _str = _str.Replace("@2@", orientationValue);
                _str = _str.Replace("@3@", GetFieldValue(_change_row["lol_area_id"]));
                _str = _str.Replace("@4@", GetFieldValue(_change_row["lol_bank_id"]));
                _str = _str.Replace("@5@", GetFieldValue(_change_row["lol_bank_location"]));
                _str = _str.Replace("@6@", GetFieldValue(_change_row["lol_offset_x"]));
                _str = _str.Replace("@7@", GetFieldValue(_change_row["lol_offset_y"]));
              }
              else
              {

                // Check current location
                _str += "SELECT   @ObjectCurrentLocationId = lol_location_id \n";
                _str += "  FROM   layout_object_locations \n";
                _str += " WHERE   lol_object_id = @1@ \n";
                _str += "   AND   lol_current_location = 1 \n";
                _str += "IF (@ObjectCurrentLocationId IS NOT NULL) \n";
                _str += " BEGIN \n";
                // Location changed, mark old
                _str += "  UPDATE   layout_object_locations \n";
                _str += "     SET   lol_current_location = 0 \n";
                _str += "         , lol_date_to          = @Now \n";
                _str += "   WHERE   lol_object_id        = @1@ \n";
                _str += "     AND   lol_location_id      = @ObjectCurrentLocationId \n";
                _str += "     AND   lol_current_location = 1 \n";
                _str += "\n";
                if (_row_status != EN_OBJECT_STATUS.OS_DELETED)
                {
                  // Location changed, create new object location
                  _str += "  INSERT   INTO layout_object_locations \n";
                  _str += "         ( lol_object_id, lol_location_id, lol_date_from, lol_date_to, lol_orientation, lol_area_id, lol_bank_id, lol_bank_location, lol_current_location, lol_offset_x, lol_offset_y ) \n";
                  _str += "  VALUES ( @1@, @2@, @Now, NULL, @3@, @4@, @5@, @6@, 1, @7@, @8@ ) \n";
                }
                _str += " END \n";
                _str += "ELSE \n";
                _str += " BEGIN \n";
                // Location unchanged, update fields
                _str += "  UPDATE   layout_object_locations \n";
                _str += "     SET   lol_object_id        = @1@ \n";
                _str += "         , lol_location_id      = @2@ \n";
                _str += "         , lol_date_to          = NULL \n";
                _str += "         , lol_orientation      = @3@ \n";
                _str += "         , lol_area_id          = @4@ \n";
                _str += "         , lol_bank_id          = @5@ \n";
                _str += "         , lol_bank_location    = @6@ \n";
                _str += "         , lol_current_location = 1 \n";
                _str += "         , lol_offset_x         = @7@ \n";
                _str += "         , lol_offset_y         = @8@ \n";
                _str += "   WHERE   lol_object_id        = @1@ \n";
                _str += "     AND   lol_location_id      = @2@ \n";
                _str += " END \n";
                // Update object update date and other data
                _str += "UPDATE   layout_objects \n";
                _str += "   SET   lo_update_date = @Now \n";
                _str += "       , lo_mesh_id     = @A@ \n";
                _str += "       , lo_parent_id   = @B@ \n";
                _str += " WHERE   lo_id          = @1@ \n";
                _str += "   AND   lo_status      = @9@ \n";

                _str = _str.Replace("@1@", GetFieldValue(_change_row["lol_object_id"]));
                _str = _str.Replace("@2@", GetFieldValue(_change_row["lol_location_id"]));

                string orientationValue = "";
                if (Convert.ToInt32(GetFieldValue(_change_row["lol_orientation"])) < 0)
                  orientationValue = (360 + Convert.ToInt32(GetFieldValue(_change_row["lol_orientation"]))).ToString();
                else
                  orientationValue = GetFieldValue(_change_row["lol_orientation"]);

                _str = _str.Replace("@3@", orientationValue);
                _str = _str.Replace("@4@", GetFieldValue(_change_row["lol_area_id"]));
                _str = _str.Replace("@5@", GetFieldValue(_change_row["lol_bank_id"]));
                _str = _str.Replace("@6@", GetFieldValue(_change_row["lol_bank_location"]));
                _str = _str.Replace("@7@", GetFieldValue(_change_row["lol_offset_x"]));
                _str = _str.Replace("@8@", GetFieldValue(_change_row["lol_offset_y"]));
                _str = _str.Replace("@9@", ((Int32)EN_OBJECT_STATUS.OS_INSERTED).ToString());
                _str = _str.Replace("@A@", GetFieldValue(_change_row["lo_mesh_id"]));
                _str = _str.Replace("@B@", GetFieldValue(_change_row["lo_parent_id"]));

              }

              _str_command += _str;
            }

            _cmd.CommandText += _str_command;
            //_cmd.CommandText += "GO \n";

            try
            {
              _cmd.Transaction = Trx;
              _cmd.Connection = Trx.Connection;
              _cmd.ExecuteNonQuery();

              Trx.Commit();
            }
            catch (Exception _ex)
            {
              Logger.Exception(HttpContext.Current, "clsFloor.Save()[COMMAND]", _ex);
              Trx.Rollback();
            }

      }
      catch (Exception _ex)
      {
        Logger.Exception(HttpContext.Current, "clsFloor.Save()", _ex);
        throw;
      }

      // Return floorId
      return _result;
    }

  }
}