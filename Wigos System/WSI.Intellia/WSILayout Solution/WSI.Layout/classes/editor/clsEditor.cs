﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using WSI.Common;
using System.Data.SqlClient;
using System.Data;
using Newtonsoft.Json;
using WSI.Layout.classes;

namespace WSI.Layout.classes.editor
{
  public static class clsEditor
  {

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public static String GetAvailableObjects()
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return clsDatabase.ExecuteQuery("Layout_Editor_Available_Objects", null, _db_trx.SqlTransaction);
      }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="ChartsMode"></param>
    /// <returns></returns>
    public static String GetLayout(int FloorId)
    {
      String _result = String.Empty;

      DataSet _layout = clsFloor.GetLayout(FloorId);

      if (_layout.Tables.Count > 0)
      {
        _result = "{\"Locations\":" + clsDefaultConverter.DataTableToReport(_layout.Tables[0]) +
                    ",\"Objects\":" + clsDefaultConverter.DataTableToReport(_layout.Tables[1]) +
                    ",\"FloorMap\":" + clsDefaultConverter.DataTableToReport(_layout.Tables[2]) + "}";
      }

      return _result;
    }

  }
}