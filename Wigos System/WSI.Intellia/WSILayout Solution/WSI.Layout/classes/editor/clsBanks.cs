﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using WSI.Common;

namespace WSI.Layout.classes.editor
{

  /// <summary>
  /// 
  /// </summary>
  public static class clsBanks
  {

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public static String GetAvailableBanks()
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return clsDatabase.ExecuteQuery("Layout_Available_Banks", null, _db_trx.SqlTransaction);
      }
    }

  }
}