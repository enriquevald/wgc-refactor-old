﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data;
using Newtonsoft.Json;

using WSI.Layout;

namespace editor.classes
{

  public static class clsConverter
  {

    public static DataSet LayoutFromString(String JSONData)
    {
      DataSet _result = new DataSet("Layout");

      //DataTable _dt_objects;
      //DataTable _dt_positions;

      //      _dt_features = new DataTable();

      //      _dt_features.Columns.Add("LUF_ROLE", typeof(Int32));
      //      _dt_features.Columns.Add("LUF_SECTION", typeof(Int32));
      //      _dt_features.Columns.Add("LUF_FUNCTION", typeof(Int32));
      //      _dt_features.Columns.Add("LUF_CSS", typeof(String));
      //      _dt_features.Columns.Add("LUF_NLS", typeof(String));
      //      _dt_features.Columns.Add("LUF_LEVEL", typeof(Int32));
      //      _dt_features.Columns.Add("LUF_ACT_TYPE", typeof(Int32));
      //      _dt_features.Columns.Add("LUF_SCRIPT", typeof(String));
      //      _dt_features.Columns.Add("LUF_ORDER", typeof(Int32));
      //      _dt_features.Columns.Add("LUF_DEFAULT", typeof(Boolean));

      //_result.Tables.Add

      return null;
    }

    /// <summary>
    /// Converts a DataSet with the layout data table to a JSON string
    /// </summary>
    /// <param name="LayoutData">The result to call Layout_GetObjectInfo procedure</param>
    /// <returns></returns>
    public static String LayoutToString(DataSet LayoutData)
    {
      try
      {
        const String C_TERMINAL = "{\"attr\":{\"x\":%X%,\"y\":%Y%,\"id\":\"%ID%\",\"data-binded-terminal\":\"%EXTID%\"}}";
        List<String> _terminals = new List<String>();
        String _terminal = String.Empty;

        Int32 _x = 0;
        Int32 _y = 0; // Z
        String _id = String.Empty;
        String _ext_id = String.Empty;

        foreach (DataRow _row in LayoutData.Tables[0].Rows)
        {
          _terminal = C_TERMINAL;

          _x = Int32.Parse(_row["LOP_X"].ToString());
          _y = Int32.Parse(_row["LOP_Z"].ToString());
          _id = "terminal_" + _row["LO_ID"].ToString();
          _ext_id = _row["LO_EXTERNAL_ID"].ToString();

          _terminal = _terminal.Replace("%X%", _x.ToString());
          _terminal = _terminal.Replace("%Y%", _y.ToString());
          _terminal = _terminal.Replace("%ID%", _id.ToString());
          _terminal = _terminal.Replace("%EXTID%", _ext_id.ToString());

          _terminals.Add(_terminal);
        }

        return "[" + String.Join(",", _terminals.ToArray()) + "]";
      }
      catch (Exception _ex)
      {
        Logger.Exception(null, "clsConverter.LayoutToString", _ex);
        
      }
      return String.Empty;
    }
  }
}