﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using WSI.Common;
using System.Drawing;
using System.Diagnostics;
using System.IO;
using WSI.Layout.classes.editor;
using Newtonsoft.Json;

namespace WSI.Layout.classes.demo
{

  class Matris
  {
    public int Id;
    public int[,] Map;
    private Int32 m_width;
    private Int32 m_height;
    public int Margin = 0;
    public Size Size
    {
      get
      {
        return new Size(this.m_width, this.m_height);
      }
    }

    private void SetSize(Int32 Width, Int32 Height)
    {
      this.m_width = Width;
      this.m_height = Height;
    }

    private void Init()
    {
      this.Map = new int[this.m_height, this.m_width];
    }

    public Matris(int Id, Size Size)
    {
      this.Id = Id;
      this.SetSize(Size.Width, Size.Height);
      this.Init();
    }

    public Matris(int Id, int Items)
    {
      this.Id = Id;
      int _cols = (Int32)(Items / 2);
      if ((Items % 2) > 0) { _cols++; }
      this.SetSize(_cols, 2);
      this.Init();
    }

    public Matris(int Id, int SquareWidth, int Margin)
    {
      this.Id = Id;
      this.Margin = Margin;
      this.SetSize(SquareWidth * Margin, SquareWidth * Margin);
      this.Init();
    }

    public void Draw(DataRow[] Rows, int FieldIndex)
    {
      this.DrawOn(0, 0, Rows, FieldIndex);
    }

    public void DrawOn(int X, int Y, DataRow[] Rows, int FieldIndex)
    {
      if (Rows.Length == 0) { return; }
      int _counter = 0;
      for (int _col = 0; _col < this.m_width; _col++)
      {
        for (int _row = 0; _row < this.m_height; _row++)
        {
          this.Map[Y + _row, X + _col] = (int)(Rows[_counter][FieldIndex]);
          _counter++;
          if (_counter >= Rows.Length) { return; }
        }
      }
    }

    public void DrawOn(int X, int Y, Matris Data)
    {
      DrawOn(X, Y, Data, false);
    }

    public void DrawOn(int X, int Y, Matris Data, Boolean DrawId)
    {
      for (int _col = 0; _col < Data.Size.Width; _col++)
      {
        for (int _row = 0; _row < Data.Size.Height; _row++)
        {
          this.Map[Y + _row, X + _col] = (DrawId) ? Data.Id : Data.Map[_row, _col];
        }
      }
    }

    public void ConsolePaint(Boolean ToFile)
    {
      String _line = String.Empty;
      StreamWriter _file = null;

      if (ToFile)
      {
        _file = File.CreateText("c:\\demo_map.txt");
      }

      for (int _row = 0; _row < this.m_height; _row++)
      {
        for (int _col = 0; _col < this.m_width; _col++)
        {
          _line = _line + ((this.Map[_row, _col] == 0) ? " " : "▓");
        }
        Console.WriteLine(_line);
        Debug.WriteLine(_line);

        if (_file != null)
        {
          _file.WriteLine(_line);
        }
        _line = String.Empty;
      }
    }
  }

  public static class clsDemoFloorGenerator
  {
    private static Size Draw(Matris Map, List<Matris> Blocks)
    {
      Size _result = new Size(0, 0);
      int _current_loc_x = Map.Margin;
      int _current_loc_y = Map.Margin;

      foreach (Matris _block in Blocks)
      {
        if ((_current_loc_x + _block.Size.Width) >= (Map.Size.Width - Map.Margin))
        {
          if (_current_loc_x > _result.Width) { _result.Width = _current_loc_x; }
          _current_loc_x = Map.Margin;
          _current_loc_y += _block.Size.Height + Map.Margin;
        }

        Map.DrawOn(_current_loc_x, _current_loc_y, _block, false);

        _current_loc_x += _block.Size.Width + Map.Margin;
      }
      if (_current_loc_y > _result.Height) { _result.Height = _current_loc_y + Map.Margin; }

      return _result;
    }

    private static void PlaceObjects(DataTable Target, Matris Map, DataSet Source, FloorData Floor, Size FinalSize)
    {
      Point _offset = new Point((int)(FinalSize.Width / 2), (int)(FinalSize.Height / 2));
      int _machine_id = 0;
      DataRow _new_row;
      DataRow _machine_data;
      DateTime _now = WGDB.Now;
      DataTable _locations = clsDefaultConverter.DataTableFromReport(JsonConvert.DeserializeObject(Floor.Locations, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }));
      String _loc_id = String.Empty;

      for (int _row = 0; _row < Map.Size.Height; _row++)
      {
        for (int _col = 0; _col < Map.Size.Width; _col++)
        {
          _machine_id = Map.Map[_row, _col];

          if (_machine_id != 0)
          {
            _machine_data = Source.Tables[1].Select("TE_TERMINAL_ID = " + _machine_id.ToString())[0];
            _loc_id = _locations.Select("loc_x = '" + _col + "' AND loc_y = '" + _row + "'")[0][0].ToString();

            _new_row = Target.NewRow();
            _new_row["lo_id"] = "";
            _new_row["lo_external_id"] = _machine_id.ToString();
            _new_row["lo_type"] = "1";
            _new_row["lo_parent_id"] = "";
            _new_row["lo_mesh_id"] = "7";
            _new_row["lo_creation_date"] = _now.ToString();
            _new_row["lo_update_date"] = "";
            _new_row["lo_status"] = "1";
            _new_row["lol_object_id"] = "";
            _new_row["lol_location_id"] = _loc_id;
            _new_row["lol_date_from"] = _now.ToString();
            _new_row["lol_date_to"] = "";
            _new_row["lol_orientation"] = (Map.Map[_row - 1, _col] == 0) ? 4 : 0;
            _new_row["lol_area_id"] = _machine_data[0];
            _new_row["lol_bank_id"] = _machine_data[3];
            _new_row["lol_bank_location"] = "";
            _new_row["lol_current_location"] = "1";
            _new_row["lol_offset_x"] = "0";
            _new_row["lol_offset_y"] = "0";

            Target.Rows.Add(_new_row);
          }
        }
      }
    }

    public static void Generate()
    {
      String _query = String.Empty;
      DataSet _banks = null;

      Matris _demo_map = null;
      int _map_size = 0;
      List<Matris> _islands = new List<Matris>();
      Matris _new_island = null;
      Size _final_size;

      FloorData _floor;

      // Query banks
      _query += "SELECT   DISTINCT(BK_BANK_ID) ";
      _query += "       , COUNT(TE_TERMINAL_ID) AS ITEMS ";
      _query += "  FROM   BANKS_TEST ";
      _query += "  JOIN   TERMINALS_TEST ON TE_BANK_ID = BK_BANK_ID ";
      _query += " GROUP   BY BK_BANK_ID ; ";

      // Query items
      _query += "SELECT   AR_AREA_ID ";
      _query += "       , AR_NAME ";
      _query += "       , AR_SMOKING ";
      _query += "       , BK_BANK_ID ";
      _query += "       , BK_NAME ";
      _query += "       , TE_TERMINAL_ID ";
      _query += "       , TE_NAME ";
      _query += "       , TE_FLOOR_ID ";
      _query += "  FROM   TERMINALS_TEST ";
      _query += "  JOIN   BANKS_TEST ON BK_BANK_ID = TE_BANK_ID ";
      _query += "  JOIN   AREAS_TEST ON AR_AREA_ID = BK_AREA_ID ";
      _query += " WHERE   TE_TERMINAL_ID NOT IN (SELECT LO_EXTERNAL_ID FROM LAYOUT_OBJECTS) ";
      _query += "   AND   TE_BLOCKED = 0 ";
      _query += "   AND   TE_TYPE    = 1 ";
      _query += "   AND   (TE_TERMINAL_TYPE < 100 AND TE_TERMINAL_TYPE > 0) ";
      _query += "   AND   TE_ACTIVE  = 1 ";
      _query += " ORDER   BY AR_AREA_ID, BK_BANK_ID ; ";

      //_query += "";

      using (DB_TRX _db_trx = new DB_TRX())
      {

        _banks = clsDatabase.ExecuteQueryString(_query, _db_trx.SqlTransaction);

        if (_banks.Tables.Count == 2)
        {

          // Creating initial map
          //_map_size = (int)Math.Sqrt(_banks.Tables[1].Rows.Count);
          _map_size = (int)Math.Sqrt(_banks.Tables[1].Rows.Count);
          //_map_size = (int)_banks.Tables[1].Rows.Count / 2;
          _demo_map = new Matris(0, _map_size, 3);

          // Prepare Islands
          foreach (DataRow _row in _banks.Tables[0].Rows)
          {
            _new_island = new Matris((Int32)_row[0], (Int32)_row[1]);

            // Put terminals in a places
            _new_island.Draw(_banks.Tables[1].Select("BK_BANK_ID = " + _row[0]), 5);

            _islands.Add(_new_island);
          }

          // Add banks to the main floor
          _final_size = Draw(_demo_map, _islands);

          // DEBUG
           _demo_map.ConsolePaint(true);
          //

          // Create the floor
          _floor = new FloorData();
          _floor.id = 0;
          _floor.color = "rgb(255,255,255)";
          _floor.height = _final_size.Height;
          _floor.width = _final_size.Width;
          _floor.Unit = 0;
          _floor.name = "Auto-Generated demo floor";
          _floor.image_map = "";
          _floor.id = Floor.SaveLayoutFloor(_floor, _db_trx);

          // Initialize Locations
          if (_floor.id > 0)
          {
            _floor.Locations = Floor.CreateFloorLocations(_floor, _db_trx);
          }

          // Prepare table to place objects
          using (DataTable _floor_changes = new DataTable())
          {
            _floor_changes.Columns.Add("lo_id", typeof(String));
            _floor_changes.Columns.Add("lo_external_id", typeof(String));
            _floor_changes.Columns.Add("lo_type", typeof(String));
            _floor_changes.Columns.Add("lo_parent_id", typeof(String));
            _floor_changes.Columns.Add("lo_mesh_id", typeof(String));
            _floor_changes.Columns.Add("lo_creation_date", typeof(String));
            _floor_changes.Columns.Add("lo_update_date", typeof(String));
            _floor_changes.Columns.Add("lo_status", typeof(String));
            _floor_changes.Columns.Add("lol_object_id", typeof(String));
            _floor_changes.Columns.Add("lol_location_id", typeof(String));
            _floor_changes.Columns.Add("lol_date_from", typeof(String));
            _floor_changes.Columns.Add("lol_date_to", typeof(String));
            _floor_changes.Columns.Add("lol_orientation", typeof(String));
            _floor_changes.Columns.Add("lol_area_id", typeof(String));
            _floor_changes.Columns.Add("lol_bank_id", typeof(String));
            _floor_changes.Columns.Add("lol_bank_location", typeof(String));
            _floor_changes.Columns.Add("lol_current_location", typeof(String));
            _floor_changes.Columns.Add("lol_offset_x", typeof(String));
            _floor_changes.Columns.Add("lol_offset_y", typeof(String));

            // Place objects
            PlaceObjects(_floor_changes, _demo_map, _banks, _floor, _final_size);

            // Update floor in database
            clsFloor.Save(_floor_changes, _db_trx.SqlTransaction);
          }

        }

        //_db_trx.Rollback(); // <---- to commit!
        _db_trx.Commit();
      }
    }
  }
}