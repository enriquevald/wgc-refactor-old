﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WSI.Common;
using System.Configuration;
using System.Data;
using System.Web.Configuration;

namespace WSI.Layout
{
  public static class DataBaseManager
  {

    public static void LogWGDBState()
    {
      Logger.Write(HttpContext.Current, "Database Connection State: " + WGDB.ConnectionState.ToString());
    }

    public static void OpenConnection()
    {
      try
      {

        // Connection
        if (WGDB.ConnectionState != ConnectionState.Open)
        {
          String _principal = WebConfigurationManager.AppSettings["DBPrincipal"].ToString();
          String _mirror = WebConfigurationManager.AppSettings["DBMirror"].ToString();
          Int32 _id = Int32.Parse(WebConfigurationManager.AppSettings["DBId"].ToString());

          Logger.Write(HttpContext.Current, "Database: Locating servers and database...");
          WGDB.Init(_id, _principal, _mirror);
          Logger.Write(HttpContext.Current, "Database: Setting application...");

          WGDB.SetApplication(Global.CurrentVersion.Name, Global.CurrentVersion.Version);

          Logger.Write(HttpContext.Current, "Database: Connecting...");
          WGDB.ConnectAs("WGROOT");

          LogWGDBState();
        }

      }
      catch (Exception _ex)
      {
        Logger.Write(HttpContext.Current, "Database Connection ERROR: " + _ex.Message, "ERR");
        
      }

    }
  }
}