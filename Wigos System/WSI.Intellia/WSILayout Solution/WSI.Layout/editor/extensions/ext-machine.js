﻿/*
  Requires:
    
*/

//function MachinesPanelUpdate() {

//  var _item = null;
//  var _t = null;
//  var _ttype = 0;

//  // Clean current links
//  $("#AvailableTerminals").empty();
//  $("#UsedTerminals").empty();

//  if (m_available_objectsOLD) {

//    for (_t in m_available_objectsOLD.ByType[_ttype]) {
      
//      _item = m_available_objectsOLD.Items[_t];

//      if (_item)  {

//        switch (+_item[0]) {
//          // -- Used in current floor
//          case _CurrentFloor.id:
//            $("#UsedTerminals").append('<button class="ui-button-text" id="button_tool_terminal_' + _item[3] + '" style="width: 100%;" data-type="2" data-machine="' + _item[3] + '">Terminal: ' + _item[3] + '</button>');
//            break;
//          // -- Available
//          case "":
//          case 0:
//          case "None":
//            $("#AvailableTerminals").append('<button class="ui-button-text" id="button_tool_terminal_' + _item[3] + '" style="width: 100%;" data-type="2" data-machine="' + _item[3] + '">Terminal: ' + _item[3] + '</button>');
//            break;
//          default:
//            // Item in another floor
//            break;
//        }

//      }

//    }

//  } else {
//    $("#UsedTerminals").html('Unavailable');
//    $("#AvailableTerminals").html('Unavailable');
//  }

//  // Assign events
//  $("button[id^='button_tool_bank_']").dblclick(function (evt) {
//    var _obj = $(evt.target);
//    if (_obj.length == 1) {
//      // Make sure that is a machine
//      if (_obj.data("type") == "2") {
//        // Perform actions
//        switch (_obj.parent().data("type")) {
//          case "1":
//            // Used

//            break;
//          case "2":
//            // Available

//            break;
//        }
//      }
//    }
//  });

//}

// An event to be triggered on available list double-click
function InsertAvailableMachine() {

  // TODO
  // DrawSVGItem();
  // Remove available button

}

/////////////////////////////////////////////////////////////////////////////////////////////

methodDraw.addExtension("Machine", function () {

  return {
    commonData: {
      machine_size : 20,
      machine_color : "#777",
      machine_border_size : 1,
      machine_border_color : "#000",
      machine_id_prefix : "machine_"
    },
    name: "Machine",
    //svgicons: "extensions/-icon.xml",

    buttons: [{
      //id: "", // xml icon id
      type: "mode",
      title: "Create Machine",

      events: {
        'click': function () {
          svgCanvas.setMode("Machine");

          // Update and show Machines context panel
          //MachinesPanelUpdate();

          UpdateContextPanels();
        }
      }
    }],

    // RMS - Allow showing panels on mode change
    modeChanged: function (NewMode, OldMode) {
      if (NewMode == "Machine") {
        svgCanvas.clearSelection();
        $("#mode_machine_panel").show();
      } else {
        $("#mode_machine_panel").hide();
      }
    },

    mouseDown: function () {
      // Check the mode on mousedown
      if (svgCanvas.getMode() == "Machine") {
        return { started: true };
      }
    },
    mouseUp: function (opts) {
      if (svgCanvas.getMode() == "Machine") {
        var zoom = svgCanvas.getZoom();
        var x = opts.mouse_x / zoom;
        var y = opts.mouse_y / zoom;

        if (methodDraw.curConfig.gridSnapping) {
          x = svgedit.utilities.snapToGrid(x) + (methodDraw.curConfig.snappingStep / 2);
          y = svgedit.utilities.snapToGrid(y) + (methodDraw.curConfig.snappingStep / 2);
        }

        // Create machine
        var _attrs = WSIObjectTypes(0);

        _attrs.attr.original_x = x;
        _attrs.attr.original_y = y;
        _attrs.attr.d = _attrs.attr.d.replace("%X%", _attrs.attr.original_x);
        _attrs.attr.d = _attrs.attr.d.replace("%Y%", _attrs.attr.original_y);
        _attrs.attr.externalId = "0";
        
        var _elem = svgCanvas.addSvgElementFromJson(_attrs);

        // Perform changes
        keep = true;
        element = null;
        current_mode = "Machine";
      } 
    }
  };
});

