﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: filename
// 
//   DESCRIPTION: Description.
// 
//        AUTHOR: Author
// 
// CREATION DATE: Date
// 
//------------------------------------------------------------------------------

/*
  Requires:
    
*/

var m_default_mesh_id = 7;

/////////////////////////////////////////////////////////////////////////////////////////////

methodDraw.addExtension("Beacons", function () {

  var editStarted = false;

  function InsertAvailableBeacon(NewItem, Location) {

    if (svgCanvas.getMode() == "Beacons") {

      var zoom = svgCanvas.getZoom();
      var x = (UnitsToPixels(+Location[EN_LOCATION_FIELDS.loc_x]) + NewItem[EN_OBJECT_LOCATION_FIELDS.lol_offset_x] + 10) / zoom;
      var y = (UnitsToPixels(+Location[EN_LOCATION_FIELDS.loc_y]) + NewItem[EN_OBJECT_LOCATION_FIELDS.lol_offset_y] + 10) / zoom;

      if (methodDraw.curConfig.gridSnapping) {
        x = svgedit.utilities.snapToGrid(x) + (methodDraw.curConfig.snappingStep / 2);
        y = svgedit.utilities.snapToGrid(y) + (methodDraw.curConfig.snappingStep / 2);
      }

      //var _attrs = WSIObjectTypes(+object.category);
      var _attrs = WSIObjectTypes(NewItem[EN_OBJECT_LOCATION_FIELDS.lo_type]);

      if (NewItem[EN_OBJECT_LOCATION_FIELDS.svg_id] == null) {
        NewItem[EN_OBJECT_LOCATION_FIELDS.svg_id] = _attrs.attr.id;
      } else {
        _attrs.attr.id = NewItem[EN_OBJECT_LOCATION_FIELDS.svg_id];
      }

      _attrs.attr.d = _attrs.attr.d.replace("%X%", x);
      _attrs.attr.d = _attrs.attr.d.replace("%Y%", y);

      //Check that target location is not already occupied
      if (!_CurrentChanges3.GetByField(EN_OBJECT_LOCATION_FIELDS.lol_location_id, Location[EN_LOCATION_FIELDS.loc_id], EN_OBJECT_STATUS.deleted) &&
          !_CurrentFloor3.GetByField(EN_OBJECT_LOCATION_FIELDS.lol_location_id, Location[EN_LOCATION_FIELDS.loc_id], EN_OBJECT_STATUS.deleted)) {

        if (_CurrentFloor3.SetObject(NewItem)) {

          var _elem = svgCanvas.addSvgElementFromJson(_attrs);

          // Perform changes
          keep = true;
          element = null;
        } else {

          // Perform changes
          keep = false;
          element = null;
        }
      } else {

        // Location already occupied
        var _current_mode = svgCanvas.getMode();
        svgCanvas.setMode('dialog_open');
        var dialog = $('<p>Ubicación ya ocupada, retire primero el objeto que reside en ella para poder insertar este.</p>').dialog({
          buttons: {
            "Aceptar": function () { dialog.dialog("close"); svgCanvas.setMode(_current_mode); }
          }
        });

        // Undo changes
        keep = false;
      }

      m_available_objects.UpdateList($("#mode_beacons_availabled_list"));

    }

  }

  function RemoveObjects(Elements) {

    for (var _el in Elements) {
      var _element = Elements[_el];
      var _el_item = _CurrentChanges3.GetByField(EN_OBJECT_LOCATION_FIELDS.svg_id, _element.id, EN_OBJECT_STATUS.deleted);
      if (!_el_item) {
        _el_item = _CurrentFloor3.GetByField(EN_OBJECT_LOCATION_FIELDS.svg_id, _element.id, EN_OBJECT_STATUS.deleted);
      }
      if (_el_item) {
        _el_item[EN_OBJECT_LOCATION_FIELDS.lo_status] = EN_OBJECT_STATUS.deleted;

        _CurrentChanges3.AddRow(EN_OBJECT_LOCATION_FIELDS.svg_id, _el_item[EN_OBJECT_LOCATION_FIELDS.svg_id], _el_item);
      }
    }

    //for (var _el in Elements) {
    //  var _o = WSIGetAttributesFromElement(Elements[_el]);
    //  _CurrentFloor3.RemoveObject(_o);
    //}

    svgCanvas.deleteSelectedElements_();
  }

  return {
    name: "Beacons",
    svgicons: "../extensions/iBeacon.svg",

    buttons: [{
      id: "beacons", // xml icon id
      type: "mode",
      title: "Beacons",

      events: {
        'click': function () {
          svgCanvas.setMode("Beacons");

          if (m_available_objects) {
            m_available_objects.UpdateList($("#mode_beacons_availabled_list"));
          }

          UpdateContextPanels();
        }
      }
    }],

    // RMS - Allow showing panels on mode change
    modeChanged: function (NewMode, OldMode) {
      if (NewMode == "Beacons") {
        svgCanvas.clearSelection();
      }
    },

    mouseDown: function (a, b, c) {

    },

    mouseUp: function (opts) {

    },

    mouseClick: function (opts) {
      var _mode = svgCanvas.getMode();
      if (_mode == 'Beacons') {
        var _object = m_available_objects.GetListSelectedItem($("#mode_beacons_availabled_list"));
        if (_object) {

          var _new_item = _CurrentFloor3.CreateNewObjectLocation();
          var _location = _CurrentFloor3.GetLocation(_MousePos.location.x, _MousePos.location.y);

          if (_location) {
            _new_item[EN_OBJECT_LOCATION_FIELDS.lo_id] = _object.item.id;
            _new_item[EN_OBJECT_LOCATION_FIELDS.lo_external_id] = _object.item.identity;
            _new_item[EN_OBJECT_LOCATION_FIELDS.lo_type] = +_object.item.type;
            _new_item[EN_OBJECT_LOCATION_FIELDS.lo_status] = EN_OBJECT_STATUS.inserted;

            _new_item[EN_OBJECT_LOCATION_FIELDS.lo_mesh_id] = m_default_mesh_id;

            _new_item[EN_OBJECT_LOCATION_FIELDS.lol_bank_id] = _object.item.bank;
            _new_item[EN_OBJECT_LOCATION_FIELDS.lol_location_id] = _location[EN_LOCATION_FIELDS.loc_id];
            _new_item[EN_OBJECT_LOCATION_FIELDS.lol_date_from] = new Date();
            _new_item[EN_OBJECT_LOCATION_FIELDS.lol_orientation] = 0;
            _new_item[EN_OBJECT_LOCATION_FIELDS.lol_current_location] = 1;
            _new_item[EN_OBJECT_LOCATION_FIELDS.lol_offset_x] = 0;
            _new_item[EN_OBJECT_LOCATION_FIELDS.lol_offset_y] = 0;

            InsertAvailableBeacon(_new_item, _location);

            //InsertAvailableMachine({
            //  position: _MousePos.position,
            //  location: _MousePos.location
            //}, _object);
          }
          else {
            svgCanvas.undoMgr.undo();
            var _current_mode = svgCanvas.getMode();
            svgCanvas.setMode('dialog_open');
            var dialog = $('<p>Ubicación fuera del plano. Resulta imposible ubicar el objeto.</p>').dialog({
              buttons: {
                "Aceptar": function () { dialog.dialog("close"); svgCanvas.setMode(_current_mode); }
              }
            });
          }
        }
      }
    },

    beforeDelete: function (elements) {

    },

    // Transition per element
    transition: function (opts) {
      editStarted = true;
    },

    BeforeAttributeChange: function (opts) {
      if (opts.attr == 'x') {
        var _o = WSIGetAttributesFromElement(opts.elem);
        var _mat = WSIGetPathMatrix(_o.attr.d);
        return +_mat[1] - 10;
      }
    },

    onUndo: function (opts) {
      if (opts.command != "") {

      }
    },

    onRedo: function (opts) {
      if (opts.command != "") {

      }
    },

    onSelectMode: function (opts) {
      UpdateContextPanels();
    },

    onRotating: function (opts) {
      //var _els = svgCanvas.getSelectedElems();
      //if (_els.length == 1) {
      //  //var _el = _els[0];
      //  //var _item = _CurrentChanges3.GetByField(EN_OBJECT_LOCATION_FIELDS.svg_id, _el.id, EN_OBJECT_STATUS.deleted);
      //  //if (!_item) {
      //  //  _item = _CurrentFloor3.GetByField(EN_OBJECT_LOCATION_FIELDS.svg_id, _el.id, EN_OBJECT_STATUS.deleted);
      //  //}
      //  //if (_item) {
      //  //  if (_item[EN_OBJECT_LOCATION_FIELDS.lol_orientation] != opts.value) {
      //  //    _item[EN_OBJECT_LOCATION_FIELDS.lol_orientation] = opts.value;

      //  //    _CurrentChanges3.AddRow(EN_OBJECT_LOCATION_FIELDS.svg_id, _item[EN_OBJECT_LOCATION_FIELDS.svg_id], _item);
      //  //  }
      //  //}
      //}
    }

  };
});

