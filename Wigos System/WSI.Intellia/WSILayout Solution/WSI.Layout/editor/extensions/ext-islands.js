﻿/*
Requires:
    
*/

function BankPanelUpdate() {

  var _ipf = null;

  // Clean current links
  m_banks_list.ClearAll();

  if (m_available_objects) {

    // Available Islands ( floor = 'None' )
    if (m_available_objects.IslandsPerFloor["None"]) {
      for (_ipf in m_available_objects.IslandsPerFloor["None"]) {
        m_banks_list.AddItem(2, _ipf);
      }
    }

    // Current floor Islands
    if (m_available_objects.IslandsPerFloor[_CurrentFloor.id]) {
      for (_ipf in m_available_objects.IslandsPerFloor[_CurrentFloor.id]) {
        m_banks_list.AddItem(1, _ipf);
      }
    }

  } else {
    // Empty
  }

}

// An event to be triggered on available list double-click
function InsertAvailableIsland() {

  // TODO
  // DrawSVGIsland();
  // Remove available button

}

/////////////////////////////////////////////////////////////////////////////////////////////

methodDraw.addExtension("Islands", function () {

  return {
    commonData: {

      showDialog: function () {

        LayoutNewIslandOpen();

      }


    },
    name: "Islands",
    //svgicons: "extensions/-icon.xml",

    buttons: [{
      //id: "", // xml icon id
      type: "mode",
      title: "Create Island",
      events: {
        'click': function () {
          svgCanvas.setMode("Islands");

          // Update and show Islands context panel
          BankPanelUpdate();

          UpdateContextPanels();
        }
      }
    }],
    mouseDown: function () {
      // Check the mode on mousedown
      if (svgCanvas.getMode() == "Islands") {
        return { started: true };
      }
    },
    mouseUp: function (opts) {
      if (svgCanvas.getMode() == "Islands") {
        var zoom = svgCanvas.getZoom();
        var x = opts.mouse_x / zoom;
        var y = opts.mouse_y / zoom;

        var _g_id = svgCanvas.getNextId();

        // Show dialog
        this.commonData.showDialog();

        // Perform changes
        //keep = true;
        keep = false;
        element = null;
        current_mode = "Islands";
      }
    }
  }
});

