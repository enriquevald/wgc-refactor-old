﻿/*
Requires:
    
*/

methodDraw.addExtension("GamingTable", function () {

  return {
    commonData: {
      gaming_table_size: 40,
      gaming_table_color: "#777",
      gaming_table_border_size: 1,
      gaming_table_border_color: "#000",
      gaming_table_id_prefix: "gaming_table_"
    },
    name: "GamingTable",
    //svgicons: "extensions/-icon.xml",

    buttons: [{
      //id: "", // xml icon id
      type: "mode",
      title: "Create GamingTable",

      events: {
        'click': function () {
          svgCanvas.setMode("GamingTable");
        }
      }
    }],
    mouseDown: function () {
      // Check the mode on mousedown
      if (svgCanvas.getMode() == "GamingTable") {
        return { started: true };
      }
    },
    mouseUp: function (opts) {
      if (svgCanvas.getMode() == "GamingTable") {
        var zoom = svgCanvas.getZoom();
        var x = opts.mouse_x / zoom;
        var y = opts.mouse_y / zoom;

        // Create gaming_table
        var _g_id = svgCanvas.getNextId();

        var _elem = svgCanvas.addSvgElementFromJson({
          "element": "rect",
          "curStyles": false,
          "attr": {
            "x": x, // - (m_terminal_size * zoom / 2),
            "y": y, // - (m_terminal_size * zoom / 2),
            "width": this.commonData.gaming_table_size,
            "height": this.commonData.gaming_table_size,
            "id": this.commonData.gaming_table_id_prefix + _g_id,
            "alt": this.commonData.gaming_table_id_prefix + _g_id,
            "stroke": this.commonData.gaming_table_border_color,
            "stroke-width": this.commonData.gaming_table_border_size,
            "fill": this.commonData.gaming_table_color,
            "opacity": 1,
            "data-binded-terminal": ""
          }
        });

        // Perform changes
        keep = true;
        element = null;
        current_mode = "GamingTable";
      }
    }
  };
});