﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: filename
// 
//   DESCRIPTION: Description.
// 
//        AUTHOR: Author
// 
// CREATION DATE: Date
// 
//------------------------------------------------------------------------------

methodDraw.addExtension("view_back_pic", function (s) {

  function InitializeBackPic(Width, Height) {
      /*
       <image x="20" y="20" width="300" height="80"
         xlink:href="http://jenkov.com/images/layout/top-bar-logo.png" />
       */

      if (!svgCanvas.setCurrentLayer("background")) {
        svgCanvas.createLayer("background");
      }

      var cur_shape = svgCanvas.addSvgElementFromJson({
        "element": "image",
        "attr": {
          "x": -1,
          "y": -1,
          "width": Width + 2,
          "height": Height + 2,
          "xlink:href": "",
          "id": "canvasBackPic"
        }
      });

  }
  
  function UpdateBackPic(NewPic, Width, Height) {
    if ($("#canvasBackPic").length == 0) {
      InitializeBackPic(Width, Height);
    }
    $("#canvasBackPic").attr("xlink:href", NewPic);
  }

  return {
    name: "view_back_pic",

    buttons: [{
      id: "view_back_pic",
      type: "menu",
      after: "tool_wireframe",
      panel: "view_menu",
      title: "View Background Image",
      cls: "push_button_pressed",
      events: {
        'click': function () {
          
          var gr = !$('#view_back_pic').hasClass('push_button_pressed');
          if (gr) {
            $('#view_back_pic').addClass('push_button_pressed');
            $('#canvasBackPic').attr('display', 'inline');
          }
          else {
            $('#view_back_pic').removeClass('push_button_pressed');
            $('#canvasBackPic').attr('display', 'none');
          }

        }
      }
    }],

    onOpenDocument: function (opts) {
      if (opts.docData) {
        UpdateBackPic(opts.docData.image_map, opts.docData.dimensions.x, opts.docData.dimensions.y);
      }
    }
  };
});
