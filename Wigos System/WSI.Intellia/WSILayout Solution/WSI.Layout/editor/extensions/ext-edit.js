﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: filename
// 
//   DESCRIPTION: Description.
// 
//        AUTHOR: Author
// 
// CREATION DATE: Date
// 
//------------------------------------------------------------------------------

/*
  Requires:
    
*/

var m_default_terminal_mesh_id = -1;

/////////////////////////////////////////////////////////////////////////////////////////////

methodDraw.addExtension("Edit", function () {

  var editStarted = false;

  $("#machine_panel_available_meshes").change(function (Event) {
    var _elems = svgCanvas.getSelectedElems(),
        _l = _elems.length,
        _element,
        _val = Event.target.value;

    for (var _i = 0; _i < _l; _i++) {
      _element = _elems[_i];
      var _item = _CurrentChanges3.GetByField(EN_OBJECT_LOCATION_FIELDS.svg_id, _element.id, EN_OBJECT_STATUS.deleted);
      if (!_item) {
        _item = _CurrentFloor3.GetByField(EN_OBJECT_LOCATION_FIELDS.svg_id, _element.id, EN_OBJECT_STATUS.deleted);
      }
      if (_item) {
        _item[EN_OBJECT_LOCATION_FIELDS.lo_mesh_id] = _val;

        _CurrentChanges3.AddRow(EN_OBJECT_LOCATION_FIELDS.svg_id, _item[EN_OBJECT_LOCATION_FIELDS.svg_id], _item);
      }

    }
  });

  function InsertAvailableMachine(NewItem, Location) {

    if (svgCanvas.getMode() == "Edit") {

      var zoom = svgCanvas.getZoom();
      var x = (UnitsToPixels(+Location[EN_LOCATION_FIELDS.loc_x]) + NewItem[EN_OBJECT_LOCATION_FIELDS.lol_offset_x] + 10) / zoom;
      var y = (UnitsToPixels(+Location[EN_LOCATION_FIELDS.loc_y]) + NewItem[EN_OBJECT_LOCATION_FIELDS.lol_offset_y] + 10) / zoom;

      if (methodDraw.curConfig.gridSnapping) {
        x = svgedit.utilities.snapToGrid(x) + (methodDraw.curConfig.snappingStep / 2);
        y = svgedit.utilities.snapToGrid(y) + (methodDraw.curConfig.snappingStep / 2);
      }

      //var _attrs = WSIObjectTypes(+object.category);
      var _attrs = WSIObjectTypes(NewItem[EN_OBJECT_LOCATION_FIELDS.lo_type]);

      if (NewItem[EN_OBJECT_LOCATION_FIELDS.svg_id] == null) {
        NewItem[EN_OBJECT_LOCATION_FIELDS.svg_id] = _attrs.attr.id;
      } else {
        _attrs.attr.id = NewItem[EN_OBJECT_LOCATION_FIELDS.svg_id];
      }

      _attrs.attr.d = _attrs.attr.d.replace("%X%", x);
      _attrs.attr.d = _attrs.attr.d.replace("%Y%", y);

      //Check that target location is not already occupied
      if (!_CurrentChanges3.GetByField(EN_OBJECT_LOCATION_FIELDS.lol_location_id, Location[EN_LOCATION_FIELDS.loc_id], EN_OBJECT_STATUS.deleted) &&
          !_CurrentFloor3.GetByField(EN_OBJECT_LOCATION_FIELDS.lol_location_id, Location[EN_LOCATION_FIELDS.loc_id], EN_OBJECT_STATUS.deleted)) {

        var _obj_changed = _CurrentChanges3.GetByField(EN_OBJECT_LOCATION_FIELDS.lo_external_id, NewItem[EN_OBJECT_LOCATION_FIELDS.lo_external_id]);
        if (_obj_changed) {
          NewItem[EN_OBJECT_LOCATION_FIELDS.lo_id] = _obj_changed[EN_OBJECT_LOCATION_FIELDS.lo_id];
          NewItem[EN_OBJECT_LOCATION_FIELDS.lol_object_id] = _obj_changed[EN_OBJECT_LOCATION_FIELDS.lol_object_id];
        }
        
        if (_CurrentFloor3.SetObject(NewItem)) {

          var _elem = svgCanvas.addSvgElementFromJson(_attrs);

          // Perform changes
          keep = true;
          element = null;
        } else {

          // Perform changes
          keep = false;
          element = null;
        }

      } else {

        // Location already occupied
        var _current_mode = svgCanvas.getMode();
        svgCanvas.setMode('dialog_open');
        var dialog = $('<p>Ubicación ya ocupada, retire primero el objeto que reside en ella para poder insertar este.</p>').dialog({
          buttons: {
            "Aceptar": function () { dialog.dialog("close"); svgCanvas.setMode(_current_mode); }
          }
        });

        // Undo changes
        keep = false;
      }

      m_available_objects.UpdateList($("#mode_edit_availabled_list"));

    }

  }

  function RemoveObjects(Elements) {

    for (var _el in Elements) {
      var _element = Elements[_el];
      var _el_item = _CurrentChanges3.GetByField(EN_OBJECT_LOCATION_FIELDS.svg_id, _element.id, EN_OBJECT_STATUS.deleted);
      if (!_el_item) {
        _el_item = _CurrentFloor3.GetByField(EN_OBJECT_LOCATION_FIELDS.svg_id, _element.id, EN_OBJECT_STATUS.deleted);
      }
      if (_el_item) {
        _el_item[EN_OBJECT_LOCATION_FIELDS.lo_status] = EN_OBJECT_STATUS.deleted;

        _CurrentChanges3.AddRow(EN_OBJECT_LOCATION_FIELDS.svg_id, _el_item[EN_OBJECT_LOCATION_FIELDS.svg_id], _el_item);
      }
    }

    //for (var _el in Elements) {
    //  var _o = WSIGetAttributesFromElement(Elements[_el]);
    //  _CurrentFloor3.RemoveObject(_o);
    //}

    svgCanvas.deleteSelectedElements_();
  }

  return {
    name: "Edit",
    svgicons: "../extensions/iBeacon.svg",

    buttons: [{
      id: "maquinas", // xml icon id
      type: "mode",
      title: "Terminales",

      events: {
        'click': function () {
          svgCanvas.setMode("Edit");

          if (m_available_objects) {
            m_available_objects.UpdateList($("#mode_edit_availabled_list"));
          }

          UpdateContextPanels();
        }
      }
    }],

    // RMS - Allow showing panels on mode change
    modeChanged: function (NewMode, OldMode) {
      if (NewMode == "Edit") {
        svgCanvas.clearSelection();
      }
    },

    mouseDown: function (a, b, c) {

    },

    mouseUp: function (opts) {
      // TODO: RMS
      // Result must be { keep: , element:, started: }
      var _result = [];
      keep = true;
      out = false;
      var _tempsrows = _CurrentChanges3.rows.slice(0);
      var _tempFloor = _CurrentFloor3.ObjectLocations.slice(0);

      if (editStarted) {
        // TODO: RMS - Changes on elements to support multiple selection
        var _els = new Array();
        for (var _iel in opts.elems) {
          var _el = opts.elems[_iel];
              _els[_iel] = _el;
        }
        for (var _iel in opts.elems) {
          _el = _els[_iel];
              _el_box = _el.getBBox();

          // Get location from element
          var _destination_location_x = Math.floor(PixelsToUnits(_el_box.x + 10)),
              _destination_location_y = Math.floor(PixelsToUnits(_el_box.y + 10));

          // Get Object and Location Data
          var _item = _CurrentChanges3.GetByField(EN_OBJECT_LOCATION_FIELDS.svg_id, _el.id, EN_OBJECT_STATUS.deleted),
              _loc = _CurrentFloor3.GetLocation(_destination_location_x, _destination_location_y);

          if (!_item) {
            _item = _CurrentFloor3.GetByField(EN_OBJECT_LOCATION_FIELDS.svg_id, _el.id, EN_OBJECT_STATUS.deleted);
          }

          if (_loc) {
          if (_item) {

            // Check object location
            if (_item[EN_OBJECT_LOCATION_FIELDS.lol_location_id] == _loc[EN_LOCATION_FIELDS.loc_id]) {
              // Same location, update
              var _el_loc = WSIGetLocationFromElement(_el);
              _item[EN_OBJECT_LOCATION_FIELDS.lol_offset_x] = _el_loc.x - (UnitsToPixels(_loc[EN_LOCATION_FIELDS.loc_x]) + 10);
              _item[EN_OBJECT_LOCATION_FIELDS.lol_offset_y] = _el_loc.y - (UnitsToPixels(_loc[EN_LOCATION_FIELDS.loc_y]) + 10);

              _CurrentFloor3.SetObject(_item, _item[EN_OBJECT_LOCATION_FIELDS.lo_status]);
            } else {
              // Different location, check if target location is free


              if (!_CurrentChanges3.GetByField(EN_OBJECT_LOCATION_FIELDS.lol_location_id, _loc[EN_LOCATION_FIELDS.loc_id], EN_OBJECT_STATUS.deleted,_els) &&
                  !_CurrentFloor3.GetByField(EN_OBJECT_LOCATION_FIELDS.lol_location_id, _loc[EN_LOCATION_FIELDS.loc_id], EN_OBJECT_STATUS.deleted,_els) 
                ) {

                // Location Free, update item
                _item[EN_OBJECT_LOCATION_FIELDS.lol_location_id] = _loc[EN_LOCATION_FIELDS.loc_id];
                _item[EN_OBJECT_LOCATION_FIELDS.lol_date_from] = new Date();
                _item[EN_OBJECT_LOCATION_FIELDS.lol_current_location] = 1;

  /*              // Clear offset -no aplica sino al mover en grupo se corren las terminales.
                var _xx = UnitsToPixels(_loc[EN_LOCATION_FIELDS.loc_x]) + 10,
                    _yy = UnitsToPixels(_loc[EN_LOCATION_FIELDS.loc_y]) + 10;
                WSISetElementLocation(_el, { x: _xx, y: _yy });
*/
                         var _el_loc = WSIGetLocationFromElement(_el);
                _item[EN_OBJECT_LOCATION_FIELDS.lol_offset_x] = _el_loc.x -(UnitsToPixels(_loc[EN_LOCATION_FIELDS.loc_x]) +10);
                _item[EN_OBJECT_LOCATION_FIELDS.lol_offset_y] = _el_loc.y - (UnitsToPixels(_loc[EN_LOCATION_FIELDS.loc_y]) + 10);

                //debe permitir mantener el offset para cuando mueve en grupo no ajuste cada objeto a su casilla y pierda el dibujo
                //_item[EN_OBJECT_LOCATION_FIELDS.lol_offset_x] = 0;
                //_item[EN_OBJECT_LOCATION_FIELDS.lol_offset_y] = 0;



                _CurrentFloor3.SetObject(_item, _item[EN_OBJECT_LOCATION_FIELDS.lo_status]);
              } else {

                // Location occupied
                svgCanvas.undoMgr.undo();

                keep = false;
              }

            }

          }
          }
          else {
            // Location out of floor
            svgCanvas.undoMgr.undo();

            out=true;
          }

          // if all is ok then update object and locations and link
        }

        if (!keep) {
          _CurrentChanges3.rows = _tempsrows;
          _CurrentFloor3.ObjectLocations=_tempFloor;
          var _current_mode = svgCanvas.getMode();
          svgCanvas.setMode('dialog_open');
          var dialog = $('<p>Ubicación ya ocupada, retire primero el objeto que reside en ella para poder insertar este.</p>').dialog({
            buttons: {
              "Aceptar": function () { dialog.dialog("close"); svgCanvas.setMode(_current_mode); }
            }
          });
        }
        if (out) {
          var _current_mode = svgCanvas.getMode();
          svgCanvas.setMode('dialog_open');
          var dialog = $('<p>Ubicación fuera del plano. Resulta imposible ubicar el objeto.</p>').dialog({
            buttons: {
              "Aceptar": function () { dialog.dialog("close"); svgCanvas.setMode(_current_mode); }
            }
          });
        }

      }
      editStarted = false;

    },

    mouseClick: function (opts) {
      var _mode = svgCanvas.getMode();
      if (_mode == 'Edit') {
        var _object = m_available_objects.GetListSelectedItem($("#mode_edit_availabled_list"));
        if (_object) {

          var _new_item = _CurrentFloor3.CreateNewObjectLocation();
          var _location = _CurrentFloor3.GetLocation(_MousePos.location.x, _MousePos.location.y);

          if (_location) {
          _new_item[EN_OBJECT_LOCATION_FIELDS.lo_id] = _object.item.id;
          _new_item[EN_OBJECT_LOCATION_FIELDS.lo_external_id] = _object.item.identity;
          _new_item[EN_OBJECT_LOCATION_FIELDS.lo_type] = +_object.item.type;
          _new_item[EN_OBJECT_LOCATION_FIELDS.lo_status] = EN_OBJECT_STATUS.inserted;

          _new_item[EN_OBJECT_LOCATION_FIELDS.lo_mesh_id] = m_default_terminal_mesh_id;

          _new_item[EN_OBJECT_LOCATION_FIELDS.lol_bank_id] = _object.item.bank;
          _new_item[EN_OBJECT_LOCATION_FIELDS.lol_location_id] = _location[EN_LOCATION_FIELDS.loc_id];
          _new_item[EN_OBJECT_LOCATION_FIELDS.lol_date_from] = new Date();
          _new_item[EN_OBJECT_LOCATION_FIELDS.lol_orientation] = 0;
          _new_item[EN_OBJECT_LOCATION_FIELDS.lol_current_location] = 1;
          _new_item[EN_OBJECT_LOCATION_FIELDS.lol_offset_x] = 0;
          _new_item[EN_OBJECT_LOCATION_FIELDS.lol_offset_y] = 0;

          InsertAvailableMachine(_new_item, _location);

          //InsertAvailableMachine({
          //  position: _MousePos.position,
          //  location: _MousePos.location
          //}, _object);
        }
          else {
            svgCanvas.undoMgr.undo();
            var _current_mode = svgCanvas.getMode();
            svgCanvas.setMode('dialog_open');
            var dialog = $('<p>Ubicación fuera del plano. Resulta imposible ubicar el objeto.</p>').dialog({
              buttons: {
                "Aceptar": function () { dialog.dialog("close"); svgCanvas.setMode(_current_mode); }
              }
            });
          }
        }
      }
    },

    beforeDelete: function (elements) {
      if (elements.length > 0) {
        var _current_mode = svgCanvas.getMode();
        svgCanvas.setMode('dialog_open');
        var dialog = $('<p>Seguro que desea eliminar [' + elements.length + '] objetos?</p>').dialog({
          buttons: {
            "Si": function () { RemoveObjects(elements); dialog.dialog("close"); svgCanvas.setMode(_current_mode); },
            "No": function () { dialog.dialog("close"); svgCanvas.setMode(_current_mode); }
          }
        });

      }

    },

    // Transition per element
    transition: function (opts) {
      editStarted = true;
    },

    BeforeAttributeChange: function (opts) {
      if (opts.attr == 'x') {
        var _o = WSIGetAttributesFromElement(opts.elem);
        var _mat = WSIGetPathMatrix(_o.attr.d);
        return +_mat[1] - 10;
      }
    },

    onUndo: function (opts) {
      if (opts.command != "") {

      }
    },

    onRedo: function (opts) {
      if (opts.command != "") {

      }
    },

    onSelectMode: function (opts) {
      UpdateContextPanels();
    },

    onRotating: function (opts) {
      var _els = svgCanvas.getSelectedElems();
      var _value = opts.value;
      if (_els.length == 1) {
        var _el = _els[0];
        var _item = _CurrentChanges3.GetByField(EN_OBJECT_LOCATION_FIELDS.svg_id, _el.id, EN_OBJECT_STATUS.deleted);
        if (!_item) {
          _item = _CurrentFloor3.GetByField(EN_OBJECT_LOCATION_FIELDS.svg_id, _el.id, EN_OBJECT_STATUS.deleted);
        }
        if (_item) {
          //if (_value < 0) {
          //  _value = 180 + (_value * -1);
          //}
          //_value -= 180;
          if (_item[EN_OBJECT_LOCATION_FIELDS.lol_orientation] != _value) {
            _item[EN_OBJECT_LOCATION_FIELDS.lol_orientation] = _value;

            _CurrentChanges3.AddRow(EN_OBJECT_LOCATION_FIELDS.svg_id, _item[EN_OBJECT_LOCATION_FIELDS.svg_id], _item);
          }
        }
      }
    },

    onAvailableMeshesLoaded: function (items) {
      if (items.command != "") {
        $("#machine_panel_available_meshes").empty();

        for (var _id in items.rows) {
          var _item = items.rows[_id];
          if (+_item[1] == EN_LAYOUT_OBJECT_TYPES.LOT_TERMINAL) {
            if (m_default_terminal_mesh_id < 0) {
              m_default_terminal_mesh_id = +_item[0];
            }
            $("#machine_panel_available_meshes").append("<option value=\"" + _item[0] + "\">" + _item[2] + "</option>");
          }
        }
      }
    }

  };
});

