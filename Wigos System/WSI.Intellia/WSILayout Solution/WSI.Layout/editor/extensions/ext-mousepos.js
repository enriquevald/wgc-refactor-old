﻿
methodDraw.addExtension("MousePos", function () {

  $('#tools_bottom').append('<div id="MousePos_Pos">X:<span id="MousePos_X"></span>Y:<span id="MousePos_Y"></span> Location:<span id="MousePos_LOC_X"></span>-<span id="MousePos_LOC_Y"></span> Name: <span id="MousePos_Name"></span></div>');

  // Find your root SVG element
  var svg = document.querySelector('svg');

  // Create an SVGPoint for future math
  var pt = svg.createSVGPoint();

  // Get point in global SVG space
  function cursorPoint(evt) {
    var _zoom = svgCanvas.getZoom();
    var _offset = svgCanvas.getOffset();

    pt.x = evt.clientX - _offset.x; pt.y = evt.clientY - _offset.y;
    pt = pt.matrixTransform(svg.getScreenCTM().inverse());
    pt.x = pt.x / _zoom; pt.y = pt.y / _zoom;
    return pt;
    //return pt;
  }

  svg.addEventListener('mouseleave', function (evt, ui) {
    if (_MousePos) {
      _MousePos.mouseonsvg = false;
    }
  }, false);

  svg.addEventListener('mousemove', function (evt, ui) {
    var loc = cursorPoint(evt);
    $("#MousePos_X").text(Math.floor(loc.x));
    $("#MousePos_Y").text(Math.floor(loc.y));
    $("#MousePos_LOC_X").text(PixelsToUnits(loc.x));
    $("#MousePos_LOC_Y").text(PixelsToUnits(loc.y));

    if (_CurrentFloor3)
    {
      var _location = _CurrentFloor3.GetLocation(PixelsToUnits(loc.x), PixelsToUnits(loc.y));

      if (_location) {
        $("#MousePos_Name").text(_location[EN_LOCATION_FIELDS.loc_gridId]);
      }
      else {
        $("#MousePos_Name").text("");
      }
    }

    if(_MousePos) {
      _MousePos.Update(
                          { x: loc.x, y: loc.y },
    { x: PixelsToUnits(loc.x), y: PixelsToUnits(loc.y) }
                      );
      _MousePos.mouseonsvg = true;
    }
  }, false);

  return {
    name: "MousePos"
  };

});

function MousePos() {
  this.position = {};
  this.position.x = 0;
  this.position.y = 0;
  this.location = {};
  this.location.x = 0;
  this.location.y = 0;
  this.mouseonsvg = false;
}

MousePos.prototype.Update = function (Pos, Loc) {
  this.position.x = Pos.x;
  this.position.y = Pos.y;
  this.location.x = Loc.x;
  this.location.y = Loc.y;
}

_MousePos = new MousePos();