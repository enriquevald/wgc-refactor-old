﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data;

using WSI.Layout;
using WSI.Layout.classes;
using WSI.Layout.classes.editor;

using Newtonsoft.Json;

namespace editor
{
  public partial class editor : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    //
    private void Page_PreLoad(object sender, EventArgs e)
    {
      // Check
      //if (Global.Sessions.Information[Session.SessionID].User.Logged)
      //{
      //  // Load Security
      //  Global.Sessions.Information[Session.SessionID].User.Permissions = new LayoutUserPermissions(Global.Sessions.Information[Session.SessionID].User.Id);

      //}
      //else
      //{
      //  // Not logged
      //  Logger.Write(HttpContext.Current, "EDITOR_PRELOAD | Not logged in!");

      //  // Redirect to Login
      //  Response.Redirect("../Login.aspx?s=" + clsStrings.EncodeTo64("editor"));
      //}

      if (Request.Params["q"] != "NabAnjjeZiTepilAHxxBeNN") { Response.Redirect("../Login.aspx"); };
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="JSONVersion"></param>
    /// <returns></returns>
    [WebMethod(), ScriptMethod()]
    public static Boolean SaveLayout(String JSONVersion)
    {
      Boolean _result = false;

      if (JSONVersion != String.Empty)
      {
        _result = clsFloor.Save(JSONVersion);
      }

      return _result;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    [WebMethod(), ScriptMethod()]
    public static String LoadLayout()
    {
      DataSet _layout = new DataSet();
      _layout = WSI.Layout.Layout.GetLayoutDataSet(false);

      if (_layout != null)
      {
        return classes.clsConverter.LayoutToString(_layout);
      }
      else
      {
        // No data
        return "";
      }

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="Data"></param>
    /// <returns></returns>
    [WebMethod(), ScriptMethod()]
    public static String CreateNewFloor(String Data)
    {
      Boolean _valid = false;
      FloorData _data = new FloorData();
      String _result = String.Empty;

      // Get new floor data
      _data.id = Int32.Parse(clsJsonUtils.GetPropertyValue(Data, "id", "-1"));
      _data.name = clsJsonUtils.GetPropertyValue(Data, "name", "");
      _data.image_map = clsJsonUtils.GetPropertyValue(Data, "image_map", "");
      _data.color = clsJsonUtils.GetPropertyValue(Data, "color", "");
      _data.Unit = Double.Parse(clsJsonUtils.GetPropertyValue(Data, "metric", "0"));
      _data.width = Double.Parse(clsJsonUtils.GetPropertyValue(Data, "width", "0"));
      _data.height = Double.Parse(clsJsonUtils.GetPropertyValue(Data, "height", "0"));

      // Validate
      _valid = _data.Check();

      if (_valid)
      {
        // Create floor
        _data.id = Floor.SaveLayoutFloor(_data);

        if (_data.id > 0)
        {
          // Create floor locations
          _data.Locations = Floor.CreateFloorLocations(_data);
        }
      }

      _result = JsonConvert.SerializeObject(_data);

      return _result;
    }
    
    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    [WebMethod(), ScriptMethod()]
    public static String GetLayoutFloors()
    {
      return Floor.GetLayoutFloors();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    [WebMethod(), ScriptMethod()]
    public static String GetEditorAvailableObjects()
    {
      return clsEditor.GetAvailableObjects();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="FloorId"></param>
    /// <returns></returns>
    [WebMethod(), ScriptMethod()]
    public static String OpenFloor(Int16 FloorId)
    {
      return clsEditor.GetLayout(FloorId);
    }

    ////////////////////////////////////////////////////////////////////////////////

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    [WebMethod(), ScriptMethod()]
    public static String GetAvailableBanks()
    {
      return clsBanks.GetAvailableBanks();
    }

    //
    [WebMethod(), ScriptMethod()]
    public static String GetAvailableMeshes()
    {
      return Resources.GetAvailableMeshes();
    }

    [WebMethod(), ScriptMethod()]
    public static Boolean UpdateLocationName(Int64 LocationId, String Name) 
    {

      return Floor.UpdateLocationName(LocationId, Name);
    }

  }
}