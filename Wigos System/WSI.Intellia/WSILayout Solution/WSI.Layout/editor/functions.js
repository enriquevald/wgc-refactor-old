﻿var m_current_floor = {
  id : null,
  name : 'Nuevo',
  size : 0,
  image_map : ''
};

var m_terminal_size = 19;
var m_terminal_color = "#f00";
var m_terminal_border_size = 1;
var m_terminal_border_color = "#000";
var m_terminal_id_prefix = "terminal_";

var m_banks_list = null;
var m_terms_list = null;

function TerminalBindSelect() {

  // TODO: Show terminals list

  alert('Gathering terminal list...');
}

function LayoutLoad() {
  PageMethods.LoadLayout(onLayoutLoadComplete, onEditorError);
}

function onLayoutLoadComplete(result, method, context) {

  try {
    if (result != "") {
      // ProcessData
      var _data = JSON.parse(result);

      DrawMachines(_data);

    } else {
      // NO DATA
    }
  } catch (e) {
    alert(e.message);
  }

}

//function LayoutSave() {
//  var _data = window.methodDraw.canvas.getSvgString();
//  PageMethods.SaveLayout(_data, onLayoutSaveComplete, onEditorError);
//}

//function onLayoutSaveComplete(result, method, context) {
//}

function onEditorError() {

}

/////////////////////////////////////////////////////////////////////////////////////////////

function CoordToPos(Coordinate, Size) {
  // TODO:
}

function PosToCoord(Position, Size) {
  return (Size / 2) + (Position * 5) - 5;
}

/////////////////////////////////////////////////////////////////////////////////////////////

function DrawMachines(Machines) {
  var _machine = null;
  var _canvas = window.methodDraw.canvas;
  var _elem = null;

  try {

    if (_canvas) {
      for (var _idx in Machines) {
        _machine = Machines[_idx];

        if (_machine) {

          _machine.attr.alt = _machine.attr.id;
          _machine.attr.height = m_terminal_size;
          _machine.attr.width = m_terminal_size;
          _machine.attr.stroke = m_terminal_border_color;
          _machine.attr["stroke-width"] = m_terminal_border_size;
          _machine.attr.fill = m_terminal_color;

          _machine.attr.original_x = _machine.attr.x;
          _machine.attr.original_y = _machine.attr.y;

          _machine.attr.x = PosToCoord(_machine.attr.x, _canvas.contentW); // (_canvas.contentW / 2) + (_machine.attr.x * 5) - 5;
          _machine.attr.y = PosToCoord(_machine.attr.y, _canvas.contentH); // (_canvas.contentH / 2) + (_machine.attr.y * 5) - 5;

          _elem = _canvas.addSvgElementFromJson({
            "element": "rect",
            "curStyles": false,
            "attr": _machine.attr
          }
        );

        } else {
          // Not a machine
        }

      }
    }

  } catch (e) {
    alert(e.message);
  }

}

/////////////////////////////////////////////////////////////////////////////////////////////

function ReflownMachine(Machine) {
  
}

function ReflownMachines() {
  $('rect[id^="terminal_"]').each(
    function (key, value) {
      ReflownMachine(value);
    }
  );

}

/////////////////////////////////////////////////////////////////////////////////////////////

$(document).ready(

  function () {
    $("#terminal_binded_button").click(TerminalBindSelect);

    //$("#file_menu_load").click(LayoutLoad);

    $("#tool_clear").click(function () {
      //Recorrer los objetos existentes en el plano
      //Agregarlos al conjunto de cambios, con estado deleted
      //Marcar como deleted todas las rows del conjunto de cambios
      //Limpiar el plano
      for (var _i = 0; _i < _CurrentFloor3.ObjectLocations.length; _i++) {
        var _obj_location = _CurrentFloor3.ObjectLocations[_i];
        var _obj_changed = _CurrentChanges3.GetByField(EN_OBJECT_LOCATION_FIELDS.lo_external_id, _obj_location[EN_OBJECT_LOCATION_FIELDS.lo_external_id], EN_OBJECT_STATUS.deleted);

        if (_obj_changed) {
          _CurrentFloor3.SetObject(_obj_changed, EN_OBJECT_STATUS.deleted);
        }
        else {
          _CurrentFloor3.SetObject(_obj_location, EN_OBJECT_STATUS.deleted);
        }
      }

      for (var _i = 0; _i < _CurrentChanges3.rows.length; _i++) {
        var _obj = _CurrentChanges3.rows[_i];
        _obj[EN_OBJECT_LOCATION_FIELDS.lo_status] = EN_OBJECT_STATUS.deleted;
      }

      _CurrentFloor3.ObjectLocations = [];
      _CurrentFloor3.UpdateSVG();
      $("#zoom").val(100).trigger("change");
      //alert("LIMPIA");
    });

    $("#file_menu_save").click(function () {
      if (_CurrentFloor3.id != "") {
        //_CurrentFloor.Save();
        _CurrentFloor3.SaveFloor();
      }
    });

    $("#unbind_object_button").click(function () {
      var _elements = svgCanvas.getSelectedElems();
      EditorUnbindObject(_elements[0]);
    });

    //$("#tool_reflown").click(ReflownMachines);

    //// Prepare banks context panel
    //m_banks_list = new WSIDualList({
    //  name: "banks",
    //  sections: ["Used", "Available"],
    //  parent: "#bank_panel",
    //  tabParams: { heightStyle: "fill" },
    //  OnItemClick: undefined,
    //  OnItemDblClick: undefined
    //});
    ////    $("#bank_panel_tabs").tabs({
    ////      heightStyle: "fill"
    ////    });

    //// Prepare machines context panel
    //m_terms_list = new WSIDualList({
    //  name: "terminals",
    //  sections: ["Used", "Available"],
    //  parent: "#machines_panel",
    //  tabParams: { heightStyle: "fill" },
    //  OnItemClick: undefined,
    //  OnItemDblClick: undefined
    //});
    ////    $("#terminal_panel_tabs").tabs({
    ////      heightStyle: "fill"
    ////    });

    // Load Available Objects
    m_available_objects.Load();

    //$.ui.intersect_o = $.ui.intersect;
    //$.ui.intersect = function (draggable, droppable, toleranceMode) {
    //  //Fix helper
    //  if (draggable.helperProportions && draggable.helperProportions.width === 0 && draggable.helperProportions.height === 0) {
    //    draggable.helperProportionsBBox = draggable.helperProportionsBBox || $(draggable.element).get(0).getBBox();
    //    draggable.helperProportions = draggable.helperProportionsBBox;
    //  }

    //  //Fix droppable
    //  if (droppable.proportions && droppable.proportions.width === 0 && droppable.proportions.height === 0) {
    //    droppable.proportionsBBox = droppable.proportionsBBox || $(droppable.element).get(0).getBBox();
    //    droppable.proportions = droppable.proportionsBBox;
    //  }

    //  return $.ui.intersect_o(draggable, droppable, toleranceMode);
    //};

    // Language parsing
    _Language.parse();

  }

);

/////////////////////////////////////////////////////////////////////////////////////////////