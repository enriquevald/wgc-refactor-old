﻿/*
Requires: 
- jQuery
- jQuery-ui
*/

// Global dialog variable
var m_dlg_new_island = null;

// Variables
var m_available_islands = null;
var m_temp_new_island = null;
var m_new_island_dual_list = null;

/////////////////////////////////////////////////////////////////////////////////////////////

function PrepareNewIsland(IslandId) {
  m_temp_new_island = {
    id: null,
    name: '',
    type: 0,
    columns: 1,
    rows: 1,
    itemCount: 1,
    angle: 0,
    terminals: []
  }

  m_temp_new_island.id = IslandId;
  m_temp_new_island.name = m_available_islands[IslandId].name;
  m_temp_new_island.terminals = m_available_islands[IslandId].terminals;

  m_temp_new_island.type = +$("#sel_island_type").val();

  if (m_temp_new_island.terminals.length > 0) {

    // Rect
    // Columns
    m_temp_new_island.columns = Math.round(Math.sqrt(m_temp_new_island.terminals.length)) + 1;
    // Rows
    m_temp_new_island.rows = 2 + ((m_temp_new_island.terminals.length - (m_temp_new_island.columns * 2)) / 2);

    // Circular
    // Items
    m_temp_new_island.itemCount = m_temp_new_island.terminals.length;
    // Angle
    m_temp_new_island.angle = 360;

  } else {
    // Island has no terminals
  }

}

function UpdateNewIslandValues() {
  $("#sel_island_type").val(m_temp_new_island.type);
  $("#txt_island_columns").val(m_temp_new_island.columns);
  $("#txt_island_rows").val(m_temp_new_island.rows);
  $("#txt_island_angle").val(m_temp_new_island.angle);
  $("#txt_island_positions").val(m_temp_new_island.itemCount);
}

function DrawIsland() {
  $("#island_editor_svg").empty();

  switch (m_temp_new_island.type) {
    case 0:
      DrawRectangleIsland();
      break;
    case 1:
      DrawCircleIsland();
      break;
  }
}

function DrawRectangleIsland() {
  var _item = null;
  var _items = "";
  var _target = document.getElementById('island_editor_svg');
  var _cnt = 0;
  var _yy = 0;
  var _xx = 0;

  try {

    for (var _row = 0; _row < m_temp_new_island.rows; _row++) {
      _yy = 20 * _row;
      _item = "";
      if (_row > 0 && _row < m_temp_new_island.rows - 1) {
        // Left
        _cnt = _cnt + 1;
        _xx = 0;
        DrawSVGItem(_target, _cnt, _xx, _yy);
        // Right
        _cnt = _cnt + 1;
        _xx = m_temp_new_island.columns * 20
        DrawSVGItem(_target, _cnt, _xx, _yy);
      } else {
        // All row
        for (var _n = 0; _n <= m_temp_new_island.columns; _n++) {
          _cnt = _cnt + 1;
          _xx = _n * 20;
          DrawSVGItem(_target, _cnt, _xx, _yy);
        }
      }
    }

  } catch (e) {
    alert(e.message);
  }

}

function polarToCartesian(centerX, centerY, radius, angleInDegrees) {
  var angleInRadians = angleInDegrees * Math.PI / 180.0;
  var x = centerX + radius * Math.cos(angleInRadians);
  var y = centerY + radius * Math.sin(angleInRadians);
  return [x, y];
}

function DrawCircleIsland() {

  try {

    var _cnt = 0;
    var _target = document.getElementById('island_editor_svg');
    var _coords = null;
    var _start_angle = 0;
    var _end_angle = m_temp_new_island.angle;
    var _step = (_end_angle - _start_angle) / m_temp_new_island.itemCount; 

    for (var _n = 0; _n < m_temp_new_island.itemCount; _n++) {
      _cnt = _cnt + 1;
      _coords = polarToCartesian(250, 250, 100, _step * _n);
      DrawSVGItem(_target, _cnt, _coords[0], _coords[1]);
    }

  } catch (e) {
    alert(e.message);
  }
}


// Dialog cretion

function LayoutNewIslandCreate() {
  var _string_dialog = '';

  _string_dialog = _string_dialog + '<div id="dialog_new_island" title="Insert island">';
  _string_dialog = _string_dialog + '    <table style="width: 100%; height: 100%;">';
  _string_dialog = _string_dialog + '    <tr>';
  _string_dialog = _string_dialog + '      <td style="vertical-align: top;">';
  _string_dialog = _string_dialog + '        <label for="txt_island_name">Seleccionar isla:</label>';
  _string_dialog = _string_dialog + '        <select id="sel_island_available">';
  _string_dialog = _string_dialog + '        </select>';
  _string_dialog = _string_dialog + '        <br />';
  _string_dialog = _string_dialog + '        <label for="sel_island_type">Tipo:</label>';
  _string_dialog = _string_dialog + '        <select id="sel_island_type">';
  _string_dialog = _string_dialog + '          <option value="0" selected="selected">Rectangular</option>';
  _string_dialog = _string_dialog + '          <option value="1">Circular</option>';
  _string_dialog = _string_dialog + '        </select>';
  _string_dialog = _string_dialog + '        <br />';
  _string_dialog = _string_dialog + '        <div>';
  _string_dialog = _string_dialog + '          <div id="div_island_rect" title="Rectangular">';
  _string_dialog = _string_dialog + '            <label for="txt_island_columns">Columnas:</label>';
  _string_dialog = _string_dialog + '            <input id="txt_island_columns" type="number" value="4" />';
  _string_dialog = _string_dialog + '            <label for="txt_island_rows">Filas:</label>';
  _string_dialog = _string_dialog + '            <input id="txt_island_rows" type="number" value="2" />';
  _string_dialog = _string_dialog + '          </div>';
  _string_dialog = _string_dialog + '          <div id="div_island_circ" title="Circular" style="display: none;">';
  _string_dialog = _string_dialog + '            <label for="sel_island_angle">Angulo:</label>';
  _string_dialog = _string_dialog + '            <select id="sel_island_angle">';
  _string_dialog = _string_dialog + '              <option value="90">90</option>';
  _string_dialog = _string_dialog + '              <option value="180">180</option>';
  _string_dialog = _string_dialog + '              <option value="270">270</option>';
  _string_dialog = _string_dialog + '              <option value="360" selected="selected">360</option>';
  _string_dialog = _string_dialog + '            </select>';
  _string_dialog = _string_dialog + '            <label for="txt_island_positions">Posiciones:</label>';
  _string_dialog = _string_dialog + '            <input id="txt_island_positions" type="number" value="1" />';
  _string_dialog = _string_dialog + '          </div>';
  _string_dialog = _string_dialog + '        </div>';
  _string_dialog = _string_dialog + '      </td>';
  _string_dialog = _string_dialog + '      <td style="vertical-align: top;">';
  _string_dialog = _string_dialog + '        <div style="width: 100%; height: 380px; overflow: scroll;">';
  _string_dialog = _string_dialog + '          <svg id="island_editor_svg" xmlns="http://www.w3.org/2000/svg">';
  _string_dialog = _string_dialog + '          </svg>';
  _string_dialog = _string_dialog + '        </div>';
  _string_dialog = _string_dialog + '      </td>';
  _string_dialog = _string_dialog + '      <td style="vertical-align: top;">';
  _string_dialog = _string_dialog + '        <div id="new_island_panel">';
  _string_dialog = _string_dialog + '         </div>';
  _string_dialog = _string_dialog + '      </td>';
  _string_dialog = _string_dialog + '    </tr>';
  _string_dialog = _string_dialog + '  </table>';
  _string_dialog = _string_dialog + '</div>';
  $("#editor_dialogs").append(_string_dialog);

  // Prepare banks context panel
  m_new_island_dual_list = new WSIDualList({
    name: "new_island_bank",
    sections: ["Used", "Available"],
    parent: "#new_island_panel",
    tabParams: { heightStyle: "fill" },
    OnItemClick: undefined,
    OnItemDblClick: undefined
  });

  $("#sel_island_type").on('change', function (evt) {

    $("#div_island_rect").hide();
    $("#div_island_circ").hide();

    switch ($("#sel_island_type").val()) {
      case "0":
        $("#div_island_rect").show();
        break;
      case "1":
        $("#div_island_circ").show();
        break;
    }

    // Init values
    PrepareNewIsland($("#sel_island_available").val());

    // Update
    UpdateNewIslandValues();

    // Draw
    DrawIsland();

  });

  $("#sel_island_available").on('change', function (evt) {

    // Init values
    PrepareNewIsland($("#sel_island_available").val());

    // Update Island values
    UpdateNewIslandValues();

    // Update available machines
    UpdateAvailableMachines();

    // Draw
    DrawIsland();
  });
}

/////////////////////////////////////////////////////////////////////////////////////////////

// Dialog opener
function LayoutNewIslandOpen() {

  GetAvailableBanks();

}

/////////////////////////////////////////////////////////////////////////////////////////////

// Dialog functions

/////////////////////////////////////////////////////////////////////////////////////////////

function ParseIslands(Data) {
  var _island_name = null;
  var _island_id = null;
  var _terminal_id = null;
  var _area_id = null;

  delete m_available_islands;
  m_available_islands = [];

  for (var _n in Data) {
    var _item = Data[_n];
    _island_name = _item[3];
    _island_id = _item[0];
    _terminal_id = _item[2];
    _area_id = _item[1];

    if (m_available_islands[_island_id] == undefined) {
      if (_terminal_id != "") {
        m_available_islands[_island_id] = { name: _island_name, area: _area_id, terminals: [_terminal_id] };
      } else {
        m_available_islands[_island_id] = { name: _island_name, area: _area_id, terminals: [] };
      }
    } else {
      if (_terminal_id != null) {
        m_available_islands[_island_id].terminals.push(_terminal_id);
      }
    }
  }
}

function AddBankList() {
  var _items = '';
  var _list = $("#sel_island_available");
  _list.html('');

  for (var _n in m_available_islands) {
    _item = m_available_islands[_n];
    _items = _items + '<option value="' + _n + '">' + _item.name + ' (' + _item.terminals.length + ')</option>';
  }

  _list.html(_items);

  return;
}

function UpdateAvailableMachines() {

  var _item = null;
  var _t = null;
  var _ttype = 0;

  // Clean current links
  m_new_island_dual_list.ClearAll();

  if (m_available_objects) {

    for (_t in m_available_objects.ByType[_ttype]) {

      _item = m_available_objects.Items[_t];

      if (_item) {

        switch (+_item[0]) {
          // -- Used in current floor
          case _CurrentFloor.id:
            if (_item[0] == m_temp_new_island.id) {
              // Only items of this Island Id
              m_new_island_dual_list.AddItem(1, _item[3]);
            }
            break;
          // -- Available 
          case "":
          case 0:
          case "None":
            m_new_island_dual_list.AddItem(2, _item[3]);
            break;
          default:
            // Item in another floor
            break;
        }

      }

    }

  } else {
    // Empty
  }

}

function GetAvailableBanks() {
  PageMethods.GetAvailableBanks(OnGetAvailableBanksComplete, OnGetAvailableBanksError);
}

function OnGetAvailableBanksComplete(Result) {

  if (Result && Result != "") {
    var _data = JSON.parse(Result);

    // Parse available data
    ParseIslands(_data.rows);

    // Add items to the list
    AddBankList();

    // Init values
    for (var _first in m_available_islands) break;
    PrepareNewIsland(_first);

    // Update
    UpdateNewIslandValues();

    // Draw
    DrawIsland();

    // Also load available machines
    UpdateAvailableMachines();

    // Open the dialog
    m_dlg_new_island.dialog("open");

  }

}

function OnGetAvailableBanksError() {
  // TODO: LOG
}


/////////////////////////////////////////////////////////////////////////////////////////////

function CreateNewIsland() {
  var valid = true;

  // Validate fields

  if (valid) {

    // Create the island
    var _canvas = window.methodDraw.canvas;

    if (_canvas) {
      var _g_id = _canvas.getNextId();

      //      var _elem = _canvas.addSvgElementFromJson({
      //        "element": "rect",
      //        "curStyles": false,
      //        "attr": {
      //          "x": x, // - (m_terminal_size * zoom / 2),
      //          "y": y, // - (m_terminal_size * zoom / 2),
      //          "width": this.commonData.machine_size,
      //          "height": this.commonData.machine_size,
      //          "id": this.commonData.machine_id_prefix + _g_id,
      //          "alt": this.commonData.machine_id_prefix + _g_id,
      //          "stroke": this.commonData.machine_border_color,
      //          "stroke-width": this.commonData.machine_border_size,
      //          "fill": this.commonData.machine_color,
      //          "opacity": 1,
      //          "data-binded-terminal": ""
      //        }
      //      });
    }

  }

  return valid;
}

/////////////////////////////////////////////////////////////////////////////////////////////

// Initialization
$(document).ready(function () {

  try {

    // Create Dialog
    LayoutNewIslandCreate();

    // Prepare Dialog
    m_dlg_new_island = $("#dialog_new_island").dialog({
      dialogClass: "no-close",
      autoOpen: false,
      height: 500,
      width: 800,
      modal: true,
      buttons: {
        "Insert": CreateNewIsland,
        Cancel: function () {
          m_dlg_new_island.dialog("close");
        }
      },
      close: function () {
        // Free
      }
    });

    // Must check controls

    // Tips control

    // Form control

    // Asign to menu option

  } catch (e) {
    alert(e.message);
  }

});