﻿/*
Requires: 
- jQuery
- jQuery-ui
- dlgProgress.js
*/

// Global dialog variable
var m_dlg_open_floor = null;

// Variables
var m_open_floor_id = null;
var m_open_floor_form = null;
var m_open_floor_list = null;

var m_current_floor_data = null;

/////////////////////////////////////////////////////////////////////////////////////////////
// Dialog cretion

function LayoutOpenFloorCreate() {
  var _string_dialog = '';

  _string_dialog = _string_dialog + '<div id="dialog_layout_open" title="Abrir piso">';
  _string_dialog = _string_dialog + '<form id="dialog_layout_open_form">';
  _string_dialog = _string_dialog + '<label for="lst_floor_list">Seleccionar piso:</label>';
  _string_dialog = _string_dialog + '<select id="lst_floor_list" size="10" style="width: 100%;">';
  _string_dialog = _string_dialog + '</select>';
  _string_dialog = _string_dialog + '</form>';
  _string_dialog = _string_dialog + '</div>';

  $("#editor_dialogs").append(_string_dialog);
}

/////////////////////////////////////////////////////////////////////////////////////////////

// Dialog opener
function LayoutOpenFloorOpen() {

  // Dissable cancel button if no floor is open
  if (_CurrentFloor3)
    $("#dialog_layout_open").parent().children(".ui-dialog-buttonpane").find("button:contains('Cancel')").show();
  else
    $("#dialog_layout_open").parent().children(".ui-dialog-buttonpane").find("button:contains('Cancel')").hide();

  OpenFloorLoadFloors();
}

/////////////////////////////////////////////////////////////////////////////////////////////

// Dialog functions

function OpenFloorAddItemsToList(Items) {
  var _item = null;
  var _row = null;

  for (var _row in Items) {
    _item = '';
    _row = Items[_row];

    if (_row) {
      // TODO: RMS - FIX BUG WIDTH GEOMETRY AS JSON
      //_item = "<option value='" + _row[0] + "' data-name='" + _row[1] + "' data-color='" + _row[5] + "' data-image-map='" + _row[4] + "' data-height='" + _row[2] + "' data-geometry='" + _row[3] + "'>";

      _item = "<option value='" + _row[0] + "' data-name='" + _row[1] + "' data-color='" + _row[5] + "' data-image-map='" + _row[4] + "' data-height='" + _row[2] + "' data-geometry=''" + " data-width='" + _row[6] + "'" + " data-metric='" + _row[7] + "'" + ">";
      _item = _item + "[" + _row[0] + "]";
      _item = _item + " " + _row[1] + " ";
      _item = _item + "(" + UnitsToMetric(+_row[6], +_row[7]) + "x" + UnitsToMetric(+_row[2], +_row[7]) + " " + m_floor_metrics[+_row[7]] + "." + ")";
      _item = _item + "</option>";

      m_open_floor_list.append(_item);
    }
  }
}

/////////////////////////////////////////////////////////////////////////////////////////////

var floorMapImageIndex = 0;
var floorMapIdIndex = 1;
var floorMapNameIndex = 2;
var floorMapColorIndex = 3;
var floorMapHeightIndex = 4;
var floorMapWidthIndex = 5;
var floorMapMetricIndex = 6;

function UpdateCurrentFloor(FloorData) {

  m_current_floor_data = FloorData;

  var _floor_data = {};
  _floor_data.id = FloorData.FloorMap.rows[0][floorMapIdIndex];
  _floor_data.name = FloorData.FloorMap.rows[0][floorMapNameIndex];
  _floor_data.image_map = FloorData.FloorMap.rows[0][floorMapImageIndex];
  _floor_data.color = FloorData.FloorMap.rows[0][floorMapColorIndex];
  _floor_data.height = FloorData.FloorMap.rows[0][floorMapHeightIndex];
  _floor_data.width = FloorData.FloorMap.rows[0][floorMapWidthIndex];
  _floor_data.metric = FloorData.FloorMap.rows[0][floorMapMetricIndex];

  _floor_data.Locations = FloorData.Locations;

  // Recreate current floor
  //_CurrentFloor = new WSIFloor().Load(_floor_data, FloorData.Objects);

  // RMS - New Floor object
  _CurrentFloor3 = new WSIFloor3();
  _CurrentFloor3.Load(_floor_data, FloorData);

  // Update SVG
  _CurrentFloor3.UpdateSVG();

  return;
}

/////////////////////////////////////////////////////////////////////////////////////////////

function OpenFloorLoadFloors() {
  // Reset list
  m_open_floor_list.empty();

  // Show progress
  LayoutProgressOpen();

  // Get list of available floors
  PageMethods.GetLayoutFloors(OnLoadFloorsComplete, OnLoadFloorsError);

}

function OnLoadFloorsComplete(Result) {

  if (Result != "") {

    // Parse data
    var _floor = JSON.parse(Result);

    // Create items
    OpenFloorAddItemsToList(_floor.rows);

    // Update Available Meshes
    UpdateAvailableMeshes();

  } else {

    // Unable to obtain floors / There are no floors

    // TODO: Alert?
  }

}

function OnLoadFloorsError() {

  LayoutProgressClose();
}

/////////////////////////////////////////////////////////////////////////////////////////////

function UpdateAvailableMeshes() {
  LayoutProgressOpen();

  //PageMethods.GetAvailableMeshes(EN_LAYOUT_OBJECT_TYPES.LOT_TERMINAL, OnUpdateAvailableMeshesComplete, OnUpdateAvailableMeshesError);
  PageMethods.GetAvailableMeshes(OnUpdateAvailableMeshesComplete, OnUpdateAvailableMeshesError);
}

function OnUpdateAvailableMeshesComplete(Result) {

  // Check for result
  if (Result) {
    var _items = JSON.parse(Result);

    svgCanvas.runExtensions("onAvailableMeshesLoaded", _items);
  }

  // Close progress
  LayoutProgressClose();

  // Open dialog
  m_dlg_open_floor.dialog("open");

}

function OnUpdateAvailableMeshesError() {
  //
}

/////////////////////////////////////////////////////////////////////////////////////////////

function OpenFloor() {
  return OpenFloorFromId(m_open_floor_list.val());
}

function OpenFloorFromId(floorId) {
  var valid = true;

  // Validate

  if (valid) {
    _floorId = floorId;
    // Call page method 
    PageMethods.OpenFloor(floorId, OnOpenFloorComplete, OnOpenFloorError);

  }

  return valid;
}

function CreateFloor() {
  LayoutNewFloorOpen();
}

function OnOpenFloorComplete(Result) {
  var _result = JSON.parse(Result);

  if (_result) {
    m_dlg_open_floor.dialog("close");

    _CurrentChanges3.Initialize(_result.FloorMap.rows[0][floorMapIdIndex]);

    // Update Current Floor information
    UpdateCurrentFloor(_result);

    // Update editor
    FloorOpenedOrCreated();

    $("#zoom").val(100).trigger("change");

  } else {
    OnOpenFloorError(null);
  }
}

function OnOpenFloorError(Result) {

  // TODO: Alert?
  PageMethods.OpenFloor(_floorId, OnOpenFloorComplete, OnOpenFloorError);
}

/////////////////////////////////////////////////////////////////////////////////////////////

// Initialization
$(document).ready(function () {

  try {

    // Create Dialog
    LayoutOpenFloorCreate();

    // Prepare Dialog
    m_dlg_open_floor = $("#dialog_layout_open").dialog({
      dialogClass: "no-close",
      autoOpen: false,
      height: 450,
      width: 300,
      modal: true,
      resizable: false,
      draggable: false,
      buttons: {
        "Open": OpenFloor,
        "Create": CreateFloor,
        Cancel: function () {
          m_dlg_open_floor.dialog("close");
        }
      },
      close: function () {
        // Free
      }
    });

    // Must check controls
    m_open_floor_id = $("#lst_floor_list");

    // Form control
    m_open_floor_form = $("#dialog_layout_open_form").on("submit", function (event) {
      event.preventDefault();
      OpenFloor();
    });

    // Other controls
    m_open_floor_list = $("#lst_floor_list");

    // Asign to menu option
    $("#file_menu_load").click(LayoutOpenFloorOpen);

    //
    LayoutOpenFloorOpen();

  } catch (e) {
    alert(e.message);
  }

});