﻿/*
 Requires: 
    - jQuery
    - jQuery-ui
*/

// Global dialog variable
var m_dlg_progress = null;

// Variables

/////////////////////////////////////////////////////////////////////////////////////////////
// Dialog cretion

function LayoutProgressCreate() {
  var _string_dialog = '';

  _string_dialog = _string_dialog + '<div id="dialog_layout_progress" title="Please wait..." style="overflow: none;">';
  _string_dialog = _string_dialog + '<image src="../design/img/loading2.gif" alt="" />';
  _string_dialog = _string_dialog + '</div>';

  $("#editor_dialogs").append(_string_dialog);
}

/////////////////////////////////////////////////////////////////////////////////////////////

// Dialog opener
function LayoutProgressOpen() {
  if (m_dlg_progress == null) {
    // Create Dialog
    LayoutProgressCreate();

    // Prepare Dialog
    m_dlg_progress = $("#dialog_layout_progress").dialog({
      dialogClass: "no-close progress",
      autoOpen: false,
      height: 74,
      width: 330,
      modal: true,
      buttons: null,
    });
  }
  if (!m_dlg_progress.dialog("isOpen")) {
    m_dlg_progress.dialog("open");
  }
}

function LayoutYesNoCreate(Title, Text) {
  var _string_dialog = '';

  _string_dialog = _string_dialog + '<div id="dialog_layout_progress" title="' + Title + '" style="overflow: none;">';
  _string_dialog = _string_dialog + '<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>' + Text + '</p>';
  _string_dialog = _string_dialog + '</div>';

  $("#editor_dialogs").append(_string_dialog);
}

// Dialog opener
function LayoutYesNoOpen(Title, Text, OnClickYes, OnClickNo) {
  if (m_dlg_progress == null) {
    // Create Dialog
    LayoutProgressCreate(Title, Text);

    // Prepare Dialog
    m_dlg_progress = $("#dialog_layout_progress").dialog({
      resizable: false,
      height: 140,
      modal: true,
      buttons: {
        "Si": function () {
          $(this).dialog("close");
          m_dlg_progress = null;
          OnClickYes();
        },
        "No": function () {
          $(this).dialog("close");
          OnClickNo();
        }
      }
    });
  }
  if (!m_dlg_progress.dialog("isOpen")) {
    m_dlg_progress.dialog("open");
  }
}

// Dialog closer
function LayoutProgressClose() {
  if (m_dlg_progress.dialog("isOpen")) {
    m_dlg_progress.dialog("close");
  }
}

/////////////////////////////////////////////////////////////////////////////////////////////

// Dialog functions

/////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////

// Initialization
$(document).ready(function () {


});