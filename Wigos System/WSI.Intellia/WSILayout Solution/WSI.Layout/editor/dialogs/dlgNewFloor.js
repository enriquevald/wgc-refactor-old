﻿/*
Requires: 
- jQuery
- jQuery-ui
- dlgProgress.js
*/

// Global dialog variable
var m_dlg_new_floor = null;

// Variables
var m_new_floor_name = null;
var m_new_floor_backplane = null;
var m_new_floor_backplane_image = null;
var m_new_floor_current_color = null;
var m_new_floor_size_range = null;
var m_new_floor_size_unit = null;
var m_new_floor_width = null;
var m_new_floor_height = null;
var m_new_floor_tips = null;
var m_new_floor_fields = $([]).add(m_new_floor_name).add(m_new_floor_backplane).add(m_new_floor_width).add(m_new_floor_height).add(m_new_floor_size_unit);
var m_new_floor_form = null;

/////////////////////////////////////////////////////////////////////////////////////////////
// Dialog cretion

function LayoutNewFloorCreate() {
  var _string_dialog = '';

  _string_dialog = _string_dialog + '<div id="dialog_layout_new" title="Create new floor">';
  _string_dialog = _string_dialog + '<p class="validateTips">Todos los campos requeridos.</p>';
  _string_dialog = _string_dialog + '<form id="dialog_layout_new_form">';
  _string_dialog = _string_dialog + '<table>';
  _string_dialog = _string_dialog + '<tr>';
  _string_dialog = _string_dialog + '<td style="padding: 5px; vertical-align: top;">';
  _string_dialog = _string_dialog + '<label for="txt_floor_name">Nombre:</label>';
  _string_dialog = _string_dialog + '<input id="txt_floor_name" type="text" />';
  _string_dialog = _string_dialog + '<br />';
  _string_dialog = _string_dialog + '<label for="fil_floor_backplane">Imagen de fondo:</label>';
  _string_dialog = _string_dialog + '<input id="fil_floor_backplane" type="file" />';
  _string_dialog = _string_dialog + '<br />';
  //_string_dialog = _string_dialog + '<label for="btn_floor_color">Color de fondo:</label>';
  //_string_dialog = _string_dialog + '<input id="btn_floor_color" type="button" value="Color" />';
  //_string_dialog = _string_dialog + '<div id="div_floor_current_color" style="height: 20px; width: 20px; background-color: white; border: 1px solid black;"></div>';
  //_string_dialog = _string_dialog + '<br />';
  _string_dialog = _string_dialog + '<label for="sel_floor_size_unit">Unidad de medida:</label>';
  _string_dialog = _string_dialog + '<select id="sel_floor_size_unit">';
  _string_dialog = _string_dialog + '<option value="0" selected="selected">Metros</option>';
  _string_dialog = _string_dialog + '<option value="1">Millas</option>';
  _string_dialog = _string_dialog + '<option value="2">Pies</option>';
  _string_dialog = _string_dialog + '</select>';
  _string_dialog = _string_dialog + '<label for="txt_floor_width">Ancho:</label>';
  _string_dialog = _string_dialog + '<input id="txt_floor_width" type="text" />';
  _string_dialog = _string_dialog + '<label for="txt_floor_height">Alto:</label>';
  _string_dialog = _string_dialog + '<input id="txt_floor_height" type="text" />';
  _string_dialog = _string_dialog + '</td>';
  _string_dialog = _string_dialog + '<td>';
  _string_dialog = _string_dialog + '<div style="overflow: scroll; height: 350px; width: 300px; background-color: DarkGray;">';
  _string_dialog = _string_dialog + '<image id="img_floor_preview" src="" alt="Preview..." />';
  _string_dialog = _string_dialog + '</div>';
  _string_dialog = _string_dialog + '</td>';
  _string_dialog = _string_dialog + '</tr>';
  _string_dialog = _string_dialog + '</table>';
  _string_dialog = _string_dialog + '</form>';
  _string_dialog = _string_dialog + '</div>';

  $("#editor_dialogs").append(_string_dialog);

  $("#fil_floor_backplane").change(FileToDataUri);
}

/////////////////////////////////////////////////////////////////////////////////////////////

// Dialog opener
function LayoutNewFloorOpen() {
  m_dlg_new_floor.dialog("open");
}

/////////////////////////////////////////////////////////////////////////////////////////////

// Dialog functions

function updateTips(t) {
  m_new_floor_tips.text(t)
                  .addClass("ui-state-highlight");
  setTimeout(function () {
    m_new_floor_tips.removeClass("ui-state-highlight", 1500);
  }, 500);
}

function checkImageEmpty(o, n) {
  if (!o.attr('src') && o.attr('src').length == 0) {
    o.addClass("ui-state-error");
    updateTips(n + " no puede estar vacío.");
    return false;
  }
  return true;
}

function checkLength(o, n, min, max) {
  if (o.val().length > max || o.val().length < min) {
    o.addClass("ui-state-error");
    updateTips("El tamaño de " + n + " debe estar entre " +
          min + " y " + max + ".");
    return false;
  } else {
    return true;
  }
}

function checkRegexp(o, regexp, n) {
  if (!(regexp.test(o.val()))) {
    o.addClass("ui-state-error");
    updateTips(n);
    return false;
  } else {
    return true;
  }
}

/////////////////////////////////////////////////////////////////////////////////////////////

function FileToDataUri() {
  var reader = new FileReader();
  var _file = m_new_floor_backplane.prop('files')[0];

  // Open progress
  LayoutProgressOpen();

  reader.onload = (function (theFile) {
    return function (e) {
      // Render
      m_new_floor_backplane_image.attr('src', e.target.result);

      // Close progress
      LayoutProgressClose();
    };
  })(_file);

  // Start read
  reader.readAsDataURL(_file);
}

/////////////////////////////////////////////////////////////////////////////////////////////

function CreateNewFloor() {
  var valid = true;
  m_new_floor_fields.removeClass("ui-state-error");

  // Validate fields
  valid = valid && checkLength(m_new_floor_name, "'Nombre de piso'", 3, 25);
  valid = valid && checkRegexp(m_new_floor_name, /^[a-z]([0-9a-z_\s])+$/i, "El nombre del piso debe empezar por una letra.");

  valid = valid && checkRegexp(m_new_floor_width, /^[0-9]+$/i, "La anchura debe ser un número.");
  valid = valid && checkRegexp(m_new_floor_height, /^[0-9]+$/i, "La altura debe ser un número.");

  valid = valid && checkImageEmpty(m_new_floor_backplane_image, "Imagen de plano");

  if (valid) {

    // Collect floor data
    var _floor_data = {};
    _floor_data.name = m_new_floor_name.val();
    _floor_data.image_map = m_new_floor_backplane_image.attr('src');
    //_floor_data.color = m_new_floor_current_color.css('background-color');
    _floor_data.color = 0;

    _floor_data.metric = +m_new_floor_size_unit.val();

    // Save the unit size
    var _metric_width = +m_new_floor_width.val();
    var _metric_height = +m_new_floor_height.val();
    _floor_data.width = MetricToUnits(_floor_data.metric, _metric_width);
    _floor_data.height = MetricToUnits(_floor_data.metric, _metric_height);

    //_CurrentFloor = new WSIFloor().Create(_floor_data);

    // Call page method
    PageMethods.CreateNewFloor(JSON.stringify(_floor_data), OnCreateNewFloorComplete, OnCreateNewFloorError);

  }

  return valid;
}

function OnCreateNewFloorComplete(Result) {
  var _result = JSON.parse(Result);

  // This result can be:
  /*
    <1 : An error ocurred in process
    >0 : A floor Id
  */

  if (_result.id > 0) {
    // All ok
    m_dlg_open_floor.dialog("close");
    m_dlg_new_floor.dialog("close");

    // If not in Open mode, 
    if (!m_dlg_new_floor || (m_dlg_new_floor && !m_dlg_new_floor.dialog("isOpen"))) {
      // Update CurrentFloor
      _result.Locations = JSON.parse(_result.Locations);
      _CurrentFloor3 = new WSIFloor3();
      _CurrentFloor3.Load(_result, _result);

      // Update SVG
      _CurrentFloor3.UpdateSVG();
    }

    // Update editor
    FloorOpenedOrCreated();

    $("#zoom").val(100).trigger("change");

  } else {
    // Error ocurred
    OnCreateNewFloorError(null);
  }
}

function OnCreateNewFloorError(Result) {
  updateTips("Se ha producido un error al intentar crear el piso.");
}

/////////////////////////////////////////////////////////////////////////////////////////////

// Initialization
$(document).ready(function () {

  try {

    // Create Dialog
    if ($("#dialog_layout_new").length == 0) {
      LayoutNewFloorCreate();
    }

    // Parse language
    _Language.parse($("#dialog_layout_new")[0]);

    // Prepare Dialog
    m_dlg_new_floor = $("#dialog_layout_new").dialog({
      dialogClass: "no-close",
      autoOpen: false,
      height: 450,
      width: 600,
      modal: true,
      resizable: false,
      draggable: false,
      buttons: {
        "Create": CreateNewFloor,
        Cancel: function () {
          m_dlg_new_floor.dialog("close");
        }
      },
      close: function () {
        // Free
        m_new_floor_form[0].reset();
        m_new_floor_fields.removeClass("ui-state-error");
        m_new_floor_backplane_image.attr('src', '');
      }
    });

    // Must check controls
    m_new_floor_name = $("#txt_floor_name");
    m_new_floor_backplane = $("#fil_floor_backplane");
    m_new_floor_backplane_image = $("#img_floor_preview");
    m_new_floor_size_unit = $("#sel_floor_size_unit");
    m_new_floor_width = $("#txt_floor_width");
    m_new_floor_height = $("#txt_floor_height")
    //m_new_floor_current_color = $("#div_floor_current_color");

    m_new_floor_backplane_image.load(function (evt) {

      if (m_new_floor_width.val() == "" && m_new_floor_height.val() == "") {
        var _current_metric = +m_new_floor_size_unit.val();
        m_new_floor_width.val(Math.round(PixelsToMetric(this.width, _current_metric)));
        m_new_floor_height.val(Math.round(PixelsToMetric(this.height, _current_metric)));
      }

    });

    // Tips control
    m_new_floor_tips = $("#dialog_layout_new").children(".validateTips");

    // Form control
    m_new_floor_form = $("#dialog_layout_new_form").on("submit", function (event) {
      event.preventDefault();
      CreateNewFloor();
    });

    // Asign to menu option
    $("#file_menu_new").click(LayoutNewFloorOpen);

  } catch (e) {
    alert(e.message);
  }

});