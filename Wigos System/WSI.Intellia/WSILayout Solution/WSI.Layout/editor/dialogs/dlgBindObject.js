﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: filename
// 
//   DESCRIPTION: Description.
// 
//        AUTHOR: Author
// 
// CREATION DATE: Date
// 
//------------------------------------------------------------------------------

/*
Requires: 
- jQuery
- jQuery-ui
- dlgProgress.js
- WSIObjectManagement.js
*/

// Global dialog variable
var m_dlg_bind_object = null;

// Variables
var m_dlg_bind_form = null;
var m_dlg_bind_tabs = null;
var m_dlg_bind_selected_item = null;
var m_dlg_bind_selected_category = null;

/////////////////////////////////////////////////////////////////////////////////////////////
// Dialog cretion

function LayoutBindObjectCreate() {
  var _string_dialog = '';

  _string_dialog = _string_dialog + '<div id="dialog_layout_bind" title="Vincular">';
  _string_dialog = _string_dialog + '<form id="dialog_layout_bind_form">';
  _string_dialog = _string_dialog + '<div id="dialog_layout_bind_tabs" class="editor_tabs">';
  _string_dialog = _string_dialog + '</div>';
  _string_dialog = _string_dialog + '</form>';
  _string_dialog = _string_dialog + '</div>';

  $("#editor_dialogs").append(_string_dialog);
}

/////////////////////////////////////////////////////////////////////////////////////////////

// Dialog opener
function LayoutBindObjectOpen() {
  OpenBindObject();
}

/////////////////////////////////////////////////////////////////////////////////////////////

// Dialog functions

/////////////////////////////////////////////////////////////////////////////////////////////

function GenerateTab(Index) {

  var _string_tab_list_item = '';
  var _tab_name = Index;

  switch (+Index) {
    case 1:
      _tab_name = "Terminales";
      break;
    case 2:
      _tab_name = "Mesas de Juego";
      break;
  }

  _string_tab_list_item = _string_tab_list_item + '<li><a href="#dialog_layout_bind_tabs-' + Index + '" data-id="' + Index + '">' + _tab_name + '</a></li>';

  $("#dialog_layout_bind_tabs_list").append(_string_tab_list_item);

  var _string_tab = '';
  _string_tab = _string_tab + '<div id="dialog_layout_bind_tabs-' + Index + '" class="editor_tab_content" data-id="' + Index + '">';
  _string_tab = _string_tab + '<ol id="dialog_layout_bind_selectable_tabs-' + Index + '" class="editor_bind_select_list selectable"></ol>';
  _string_tab = _string_tab + '</div>';

  $("#dialog_layout_bind_tabs").append(_string_tab);
}

function GenerateItems() {

  m_dlg_bind_tabs.off("tabsactivate");
  $("#dialog_layout_bind_tabs").empty();
  try { $("#dialog_layout_bind_tabs").tabs("destroy"); } catch (_ex) {}
  $("#dialog_layout_bind_tabs").append('<ul id="dialog_layout_bind_tabs_list"></ul>');

  var _n_objects = m_available_objects.Items.length,
      _item = undefined,
      _category = undefined,
      _first = undefined;

  for (var _first in m_available_objects.Items) break;
  m_dlg_bind_selected_category = (_n_objects > 0) ? m_available_objects.Items[_first].type : null;

  for (var _i in m_available_objects.Items) {

    _item = m_available_objects.Items[_i];

    if (_item.ExistsInMemory() != false) { continue; }

    if (_category != _item.type) {
      _category = _item.type;
      GenerateTab(_category);
    }

    $("#dialog_layout_bind_selectable_tabs-" + _category).append('<li class="editor_bind_item ui-widget-content" data-id="' + _item.identity + '">' + _item.name + '</li>');

  }

  $(".selectable").selectable({
    selected: function (event, ui) {
      $(ui.selected).addClass("ui-selected").siblings().removeClass("ui-selected");
      m_dlg_bind_selected_item = $(ui.selected).data("id");
    }
  });

  m_dlg_bind_selected_item = null;

  $("#dialog_layout_bind_tabs").tabs().on("tabsactivate", function (event, ui) {
    m_dlg_bind_selected_item = null;
    m_dlg_bind_selected_category = ui.newPanel.data('id');
  });

  
}

/////////////////////////////////////////////////////////////////////////////////////////////

function OpenBindObject() {
  
  // Prepare Items
  GenerateItems();

  // Open dialog
  m_dlg_bind_object.dialog("open");
}

/////////////////////////////////////////////////////////////////////////////////////////////

function PerformBindObject() {
  if (m_dlg_bind_selected_item != null && m_dlg_bind_selected_category != null) {
    var _elements = svgCanvas.getSelectedElems();
    if (EditorBindObject(_elements[0], m_dlg_bind_selected_category, m_dlg_bind_selected_item)) {
      m_dlg_bind_object.dialog("close");
    }
  }
}

/////////////////////////////////////////////////////////////////////////////////////////////

// Initialization
$(document).ready(function () {

  try {

    // Create Dialog
    LayoutBindObjectCreate();

    // Prepare Dialog
    m_dlg_bind_object = $("#dialog_layout_bind").dialog({
      dialogClass: "no-close",
      autoOpen: false,
      height: 450,
      width: 400,
      modal: true,
      buttons: {
        "Bind": function () {
          PerformBindObject();
        },
        Cancel: function () {
          m_dlg_bind_object.dialog("close");
        }
      },
      close: function () {
        // Free
      }
    });

    // Form control
    m_dlg_bind_form = $("#dialog_layout_bind_form").on("submit", function (event) {
      event.preventDefault();
      
      // Bind
      PerformBindObject();
    });

    // Other controls
    m_dlg_bind_tabs = $("#dialog_layout_bind_tabs");

    m_open_floor_list = $("#lst_floor_list");

    // Asign to menu option
    $("#bind_object_button").click(OpenBindObject);

  } catch (e) {
    alert(e.message);
  }

});