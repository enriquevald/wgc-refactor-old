﻿/////////////////////////////////////////////////////////////////////////////////////////////////////////

// Balloon reports
var _report_balloon = {
  colors: ['#0000ff', '#eeeeee'],
  chart: {
//    plotBackgroundColor: '#ffffff',
//    plotBorderWidth: 0,
//    plotShadow: false
  },
  title: {
    text: '',
    align: 'center',
    verticalAlign: 'middle',
    y: -10
  },
  tooltip: {
    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
  },
  plotOptions: {
    pie: {
      dataLabels: {
        enabled: false,
        distance: -50,
        style: {
          fontWeight: 'bold',
          color: 'white',
          textShadow: '0px 1px 2px black'
        }
      },
      startAngle: 0,
      endAngle: 359,
      center: ['50%', '50%']
    }
  },
  series: [{
    type: 'pie',
    name: '',
    innerSize: '95%',
    data: []
  }]
}

var _report_balloon_2 = {
        chart: {
            type: 'pie',
//            plotBackgroundColor: '#ffffff',
//            plotBorderWidth: 0,
//            plotShadow: false
        },
        title: {
            text: '',
            align: 'center',
            verticalAlign: 'middle',
            y: -10
        },
        yAxis: {
            title: {
                text: ''
            }
        },
        plotOptions: {
            pie: {
                shadow: false,
                center: ['50%', '50%']
            }
        },
        tooltip: {
            valueSuffix: ''
        },
        series: []
    }

var _report_balloon_legend = {
  colors: ['#0000ff', '#ff5588'],
  chart: {
//    plotBackgroundColor: '#ffffff',
//    plotBorderWidth: 0,
//    plotShadow: false
  },
  title: {
    text: '',
    align: 'center',
    //verticalAlign: 'middle',
    //y: -10
  },
  tooltip: {
    pointFormat: '{series.name}: <b> {point.percentage:.1f}% - {point.y:.1f}</b>'
  },
  plotOptions: {
    pie: {
      dataLabels: {
        enabled: false,
        distance: -25,
        formatter: function () { return this.percentage + '%' }
      },
      startAngle: 0,
      endAngle: 359,
      center: ['50%', '50%'],
      showInLegend: true
    }
  },
  series: [{
    type: 'pie',
    name: '',
    innerSize: '60%',
    data: []
  }]
}

var _report_column_percent = {
        chart: {
            type: 'column',
//            plotBackgroundColor: '#ffffff',
//            plotBorderWidth: 0,
//            plotShadow: false
        },
        title: {
            text: ''
        },
        tooltip: {
            enabled: false
          },
        xAxis: {
            categories: ['']
        },
        yAxis: {
            min: 0,
            
            title: {
                text: ''
            }            
        },
        legend: {
            enabled: true,
            reversed: false
        },
        plotOptions: {
            column: {
                dataLabels: {
                    enabled: true,
                    formatter: function () {
                                    return this.series.name + ': ' + this.y + '%';
                                }    
                },
                events: {
//                  legendItemClick: function () {
//                    return false;
//                  }
                }
            }
        },
        series: []
    }

var _report_horizontal_stack = {
      chart: {
        type: 'bar',
//        plotBackgroundColor: '#ffffff',
//        plotBorderWidth: 0,
//        plotShadow: false
      },
      title: {
        text: ''
      },
      xAxis: [{
        categories: [],
        reversed: false,
        labels: {
          step: 1
        }
      }, { // mirror axis on right side
        opposite: true,
        reversed: false,
        categories: [],
        linkedTo: 0,
        labels: {
          step: 1
        }
      }],
      yAxis: {
        title: {
          text: ''
        },
        labels: {
//          formatter: function () {
//            return (Math.abs(this.value) / 1000000) + 'M';
//          }
        } //,
//        min: -120,
//        max: 120
      },

      plotOptions: {
        series: {
          stacking: 'normal'
        }
      },

      tooltip: {
        formatter: function () {
          return false;
          //'<b>' + this.series.name + ', age ' + this.point.category + '</b><br/>' +
          //            'Population: ' + Highcharts.numberFormat(Math.abs(this.point.y), 0);
        }
      },

      series: []
    }

var _report_scatter = {
        chart: {
            type: 'scatter',
            zoomType: 'xy'
        },
        title: {
            text: 'Height Versus Weight of 507 Individuals by Gender'
        },
        subtitle: {
            text: 'Source: Heinz  2003'
        },
        xAxis: {
            title: {
                enabled: true,
                text: 'Height (cm)'
            },
            startOnTick: true,
            endOnTick: true,
            showLastLabel: true
        },
        yAxis: {
            title: {
                text: 'Weight (kg)'
            }
        },
        legend: {
            layout: 'vertical',
            align: 'left',
            verticalAlign: 'top',
            x: 100,
            y: 70,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF',
            borderWidth: 1
        },
        plotOptions: {
            scatter: {
                marker: {
                    radius: 5,
                    states: {
                        hover: {
                            enabled: true,
                            lineColor: 'rgb(100,100,100)'
                        }
                    }
                },
                states: {
                    hover: {
                        marker: {
                            enabled: false
                        }
                    }
                },
                tooltip: {
                    headerFormat: '<b>{series.name}</b><br>',
                    pointFormat: '{point.x} cm, {point.y} kg'
                }
            }
        },
        series: []
    }


var _report_spider = {

        chart: {
            polar: true,
            type: 'bar'
        },

        title: {
            text: '',
            x: -80
        },

        pane: {
            size: '95%'
        },

        xAxis: {
            categories: [],
            tickmarkPlacement: 'on',
            lineWidth: 0,
            labels: {
                rotation: 0                
            }
        },

        plotOptions: { series: { animation: false } },

        yAxis: {
            gridLineInterpolation: 'polygon',
            lineWidth: 0,
            min: 0
        },

        tooltip: {
            shared: true,
            pointFormat: '<span style="color:{series.color}">{series.name}: <b>${point.y:,.0f}</b><br/>'
        },

        legend: {
            enabled: true,
            align: 'right',
            verticalAlign: 'top',
            y: 70,
            layout: 'vertical'
        },

        series: []
    }

var _report_basic_lines = {
        chart: {
          type: 'areaspline'
        },
        title: {
            text: '',
            x: -20 //center
        },
        subtitle: {
            text: '',
            x: -20
        },
        xAxis: {
            categories: []
        },
        yAxis: {
            title: {
                text: ''
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: []
    }

var _report_pie_percent = {
        chart: {
            type: 'pie'
        },
        title: {
            text: ''
        },
        tooltip: {
          pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b><br>Total: <b>{point.y}</b>'
        },
        xAxis: {
            categories: ['']
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            }            
        },
        legend: {
            enabled: true,
            reversed: false
        },
        plotOptions: {
            pie: {
              dataLabels: {
                      enabled: true,
                      distance: -50,
                      style: {
                          fontWeight: 'bold',
                          color: 'white',
                          textShadow: '0px 1px 2px black'
                      }
                  },
              startAngle: -90,
              endAngle: 90,
              center: ['50%', '50%']
          }              
//            column: {
//                dataLabels: {
//                    enabled: true,
//                    formatter: function () {
//                                    return this.series.name + ': ' + this.y + '%';
//                                }    
//                },
//                events: {
//                }
//            }
        },
        series: []
    }

var _report_gauge_2 = {
        chart: {
            type: 'solidgauge'
        },
        title: null,
        pane: {
            center: ['50%', '50%'],
            size: '100%',
            startAngle: -90,
            endAngle: 90,
            background: {
                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
                innerRadius: '60%',
                outerRadius: '100%',
                shape: 'arc'
            }
        },
        tooltip: {
            enabled: false
        },
        yAxis: {
            stops: [
                [0.1, '#55BF3B'], // green
                [0.5, '#DDDF0D'], // yellow
                [0.9, '#DF5353'] // red
            ],
            lineWidth: 0,
            minorTickInterval: null,
            tickPixelInterval: 999,
            tickWidth: 0,
            title: {
                y: -70
            },
            labels: {
              //enabled: false,
                y: 16
            }
        },
        plotOptions: {
            solidgauge: {
                dataLabels: {
                    y: 5,
                    borderWidth: 0,
                    useHTML: true
                }
            }
        }
    };

/////////////////////////////////////////////////////////////////////////////////////////////////////////

var _report_speed_triband = {
        chart: {
            type: 'gauge',
            alignTicks: false,
            plotBackgroundColor: null,
            plotBackgroundImage: null,
            plotBorderWidth: 0,
            plotShadow: false,
            spacingTop: 0,
            spacingBottom: 50
        },
        title: {
            text: ''
        },
        pane: {
            size: '120%',
            startAngle: -150,
            endAngle: 150
        },
        yAxis: [
            {
            min: 0,
            max: 2000,
            lineColor: '#999',
            lineWidth: 1,
            tickPositioner: function() { return []; },
            labels: {
                enabled: false
            },
            offset: 0,
            plotBands: [{
                from: 0,
                to: 1500,
                color: '#00f',
                innerRadius: '90%',
                outerRadius: '100%'
            },{
                from: 1500,
                to: 2000,
                color: '#77f',
                innerRadius: '90%',
                outerRadius: '100%'
            }]
          },
          {
            min: 0,
            max: 2000,
            lineWidth: 0,
            tickPositioner: function() { return []; },
            labels: {
                enabled: false
            },
            offset: 0,
                plotBands: [{
                    from: 0,
                    to: 1625,
                    color: '#f00',
                    innerRadius: '75%',
                    outerRadius: '85%'
                },{
                    from: 1625,
                    to: 2000,
                    color: '#f77',
                    innerRadius: '75%',
                    outerRadius: '85%'
            }]
          },
          {
            min: 0,
            max: 2000,
            lineWidth: 0,
            minorTickInterval: 'auto',
            minorTickWidth: 0,
            minorTickLength: 0,
            tickWidth: 0,
            labels: {
                distance: -70,
                enabled: true
            },
            offset: 0,
            plotBands: [{
                from: 0,
                to: 1450,
                color: '#080',
                innerRadius: '60%',
                outerRadius: '70%'
            },{
                from: 1450,
                to: 2000,
                color: '#5f5',
                innerRadius: '60%',
                outerRadius: '70%'
            }]
          }                  
        ],
        series: []
    }


/////////////////////////////////////////////////////////////////////////////////////////////////////////

// List of dashboard reports
var m_report_dashboard = ['Occupation',
                          'Occupation by Card',
                          'VIPS at Room',
                          'Occupation by Sex',
                          'Occupation by Age'
                         ];

/////////////////////////////////////////////////////////////////////////////////////////////////////////

var m_report_dashboard_descriptions  = ['NLS_REPORT_DASH_OCCUPATION',
                                        'NLS_REPORT_DASH_OCCUPATION_BY_CARD',
                                        'NLS_REPORT_DASH_OCCUPATION_VIP',
                                        'NLS_REPORT_DASH_OCCUPATION_SEX',
                                        'NLS_REPORT_DASH_OCCUPATION_AGE'
                                       ];

/////////////////////////////////////////////////////////////////////////////////////////////////////////

function UpdateReportDashboard(ForceUpdate) {

  // RMS - DashBoard Changes
  return;

  var _report = undefined;

  // Update all dashboard reports
  for (var _rep in m_report_dashboard_objects) {

    _report = m_report_dashboard_objects[_rep];

    if (_report) {

      //
      if (_report.CustomUpdate == 2) {

        if (ForceUpdate) {
          // By Query updatable
          _report.Query();
        }

      } else {

        if (_report.CustomUpdate != 99) {
          // Standard report
          _report.Prepare();

          //
          WSILogger.Write("Building/Updating report: [" + _report.id + "]");
          _report.Build(false);
        } 

        if (_report.template.series != null) {
          // disable full refresh after first update
          $.extend(_report.template.plotOptions, { series: { animation: false} });
        }

      }

    }
  }

  // TODO: DELETE
  // UPDATE MANUAL TEMPO REPORTS
  $('#report_dash_bet_average_triband').highcharts({
  
        chart: {
            type: 'gauge',
            alignTicks: false,
            plotBackgroundColor: null,
            plotBackgroundImage: null,
            plotBorderWidth: 0,
            plotShadow: false,
            spacingTop: 0,
            spacingBottom: 50
        },

        title: {
            text: 'Apuesta Media'
        },

       
        pane: {
            size: '120%',
            startAngle: -150,
            endAngle: 150
        },

        yAxis: [
            {
            min: 0,
            max: 2000,
            lineColor: '#999',
            lineWidth: 1,
            tickPositioner: function() { return []; },
            labels: {
                enabled: false
            },
            offset: 0,
                plotBands: [{
                    from: 0,
                    to: 1500,
                    color: '#00f',
                    innerRadius: '90%',
                    outerRadius: '100%'
                },{
                    from: 1500,
                    to: 2000,
                    color: '#77f',
                    innerRadius: '90%',
                    outerRadius: '100%'
                }
                           ]
        },
{
            min: 0,
            max: 2000,
            lineWidth: 0,
            tickPositioner: function() { return []; },
            labels: {
                enabled: false
            },
            offset: 0,
                plotBands: [{
                    from: 0,
                    to: 1625,
                    color: '#f00',
                    innerRadius: '75%',
                    outerRadius: '85%'
                },{
                    from: 1625,
                    to: 2000,
                    color: '#f77',
                    innerRadius: '75%',
                    outerRadius: '85%'
                }
                           ]
        },
{
            min: 0,
            max: 2000,
            lineWidth: 0,
    
            minorTickInterval: 'auto',
            minorTickWidth: 0,
            minorTickLength: 0,
    tickWidth: 0,
            //tickPositioner: function() { return []; },
            labels: {
                distance: -70,
                enabled: true
            },
            offset: 0,
                plotBands: [{
                    from: 0,
                    to: 1450,
                    color: '#080',
                    innerRadius: '60%',
                    outerRadius: '70%'
                },{
                    from: 1450,
                    to: 2000,
                    color: '#5f5',
                    innerRadius: '60%',
                    outerRadius: '70%'
                }
                           ]
        }                  
        /*
        {
            min: 100,
            max: 200,
            lineColor: '#339',
            lineWidth: 10,
            tickPositioner: function() { return []; },
            labels: {
                enabled: false
            },
            offset: -15,
        },
        {
            min: 0,
            max: 124,
            lineColor: '#933',
            lineWidth: 10,
            tickPositioner: function() { return []; },
            labels: {
                enabled: false
            },
            offset: -30,
        }, {
            min: 0,
            max: 300,
            lineColor: '#393',
            lineWidth: 10,
            tickPositioner: function() { return []; },
            labels: {
                enabled: false
            },
            offset: -45,
        }*/],

        series: [{
            name: 'Today',
            data: [1499],
            dataLabels: {
                y: 150,
                x: 200,
                crop: false,
                //overflow: 'none',
                useHTML: true,
                formatter: function () {
                    var kmh = this.y,
                        mph = Math.round(kmh * 0.621);
                   // return '<div style="text-align:center;"><table  id="tblComparativa" style="width:40%;margin: 0 auto;"><tr><th >Ayer</th><th >Semana</th><th >Mes</th></tr><tr><td >1650</td><td >1450</td><td >1525</td></tr></table></div>';
                    return '&nbsp;';
                },
                backgroundColor: {
                    linearGradient: {
                        x1: 0,
                        y1: 0,
                        x2: 0,
                        y2: 1
                    },
                    stops: [
                        [0, '#DDD'],
                        [1, '#FFF']
                    ]
                }
            },
            tooltip: {
                valueSuffix: '$'
            }
        }]

    });

$('#report_dash_occup_average').highcharts(
{

        chart: {
            type: 'gauge',
            plotBackgroundColor: null,
            plotBackgroundImage: null,
            plotBorderWidth: 0,
            plotShadow: false
        },

        title: {
            text: 'Ocupación'
        },

        pane: {
            startAngle: -150,
            endAngle: 150,
            size: '100%',
            background: [{
                backgroundColor: {
                    linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                    stops: [
                        [0, '#FFF'],
                        [1, '#333']
                    ]
                },
                borderWidth: 0,
                outerRadius: '109%'
            }, {
                backgroundColor: {
                    linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                    stops: [
                        [0, '#333'],
                        [1, '#FFF']
                    ]
                },
                borderWidth: 1,
                outerRadius: '107%'
            }, {
                // default background
            }, {
                backgroundColor: '#DDD',
                borderWidth: 0,
                outerRadius: '105%',
                innerRadius: '103%'
            }]
        },

        // the value axis
        yAxis: {
            min: -233,
            max: 233,

            minorTickInterval: 'auto',
            minorTickWidth: 1,
            minorTickLength: 10,
            minorTickPosition: 'inside',
            minorTickColor: '#888',

            tickPixelInterval: 30,
            tickWidth: 2,
            tickPosition: 'inside',
            tickLength: 12,
            tickColor: '#666',
            labels: {
                useHTML: true,
                formatter: function () {
                    if (this.value == 0) {
                        return '<b><font size="3">' + 233 + '</font></b>';
                    } else {
                        return null;
                    }
                },
                step: 0,
                rotation: 'auto'
            },
            title: {
                text: 'Jugadores<br><i>(Media de sala)</i>'
            },
            plotBands: [{
                from: -233,
                to: -20,
                color: '#DF5353' // red
            }, {
                from: -20,
                to: 20,
                color: '#DDDF0D' // yellow
            }, {
                from: 20,
                to: 233,
                color: '#55BF3B' // green
            }]
        },

        series: [{
            name: '',
            data: [-15], // The difference between
            dataLabels: {
                enabled:true,
                formatter: function () {
                    return 'Current: ' +  (233 + this.y) + ' Jugadores';
                }
            }
            
        }],
        tooltip: {
            enabled:true,
                formatter: function() {
                  return (233 + this.y) + ' Jugadores';
                },
                valueSuffix: ' Jugadores'
            }

    }
);
  

//  // Show last update time
//  // TODO: NLS
//  if ($("#spLastUpdate").length == 0) {
//    $("#div_report_chart_reports").prepend("<span id='spLastUpdate' style='float:left;'>" + "Last update: " + new Date().toDateString() + "</span>");
//  } else {
//    $("#spLastUpdate").html("Last update: " + new Date().toString());
//  }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////

// Occupation
var m_report_dash_occupation = new WSIReportType('report_dash_occupation', -1, '', -1, _report_balloon_2);
m_report_dash_occupation.CustomUpdate = true;
m_report_dash_occupation.type = 'pie';
m_report_dash_occupation.caption = 'DASHBOARD - OCCUPATION';
m_report_dash_occupation.series.type = 'custom';
m_report_dash_occupation.categories.type = 'custom';
m_report_dash_occupation.resizable = true;
m_report_dash_occupation.forceWidget = true;
m_report_dash_occupation.style = 'col50';
m_report_dash_occupation.hasComparative = true;
m_report_dash_occupation.onCustomGetSeries = function (Data, Template) {
  if (Data.rows.length == 0) {
    return null;
  }
  var _occ = 0;
  var _free = 0;
  var _reg = 0;
  var _an = 0;
  for (var _idx in Data.rows) {
    switch (Data.rows[_idx][0]) {
      case 'Idle':
        _free += Data.rows[_idx][1];
        break;
      case 'Anonymous':
        _an += Data.rows[_idx][1];
        _occ += Data.rows[_idx][1];
        break;
      case 'Registered':
        _reg += Data.rows[_idx][1];
        _occ += Data.rows[_idx][1];
        break;
    }
  }
  var data = [
  {
    y: _free, color: m_dash_report_occupation_colors[0], drilldown: { name: 'Free', categories: ['Free'], data: [_free], color: '#007700'}
  },
  {
    y: _occ, color: m_dash_report_occupation_colors[1], drilldown: { name: 'Card', categories: ['Anonymous', 'Registered'], data: [_an, _reg], color: '#770000'}
  }
  ];
  this.caption = Round((_occ * 100) / (_occ + _free)) + "% Occupation";
  //
  var categories = ['Free', 'Card'];
  var _oc_data = [];
  var _oc_detail = [];
  var dataLen = data.length;
  var drillDataLen = 0;
  var brightness = 0;
  //
  for (var i = 0; i < dataLen; i += 1) {

        // add browser data
        _oc_data.push({
            name: _Language.get("NLS_" + categories[i].toUpperCase()),
            y: data[i].y,
            color: data[i].color
        });

        // add version data
        drillDataLen = data[i].drilldown.data.length;
        for (var j = 0; j < drillDataLen; j += 1) {
            brightness = 0.3 - (j / drillDataLen) / 1;
            _oc_detail.push({
                name: _Language.get("NLS_" + data[i].drilldown.categories[j].toUpperCase()),
                y: data[i].drilldown.data[j],
                color: Highcharts.Color(data[i].color).brighten(brightness).get()
            });
        }
    }
  //
  var _result = [{
    name: 'Ocupación',
    size: '60%',
    innerSize: '45%',
    data: _oc_detail
  },{
    name: 'Ocupación',
    data: _oc_data,
    size: '75%',
    innerSize: '70%',  
    dataLabels: { enabled: false }  
  }];  

  return _result;
};
m_report_dash_occupation.onBeforeUpdate = function (Report) {
  var _i = 0;
  Report.data = {};
  Report.data.columns = ["OCC", "VALUE"];
  Report.data.rows = [];
  for (_i in _Manager.Terminals.OccupationData.Occupation) {
    Report.data.rows.push([_i, _Manager.Terminals.OccupationData.Occupation[_i]]);
  }
};


// VIPS at Room
var m_report_dash_occupation_vips = new WSIReportType('report_dash_occupation_vips', -1, '', -1, _report_gauge_2);
m_report_dash_occupation_vips.CustomUpdate = true;
//m_report_dash_occupation_vips.type = 'column';
m_report_dash_occupation_vips.caption = 'DASHBOARD - OCCUPATION - VIPS';
//m_report_dash_occupation_vips.subtitleDate = true;
//m_report_dash_occupation_vips.style = 'box-40-2';
m_report_dash_occupation_vips.series.type = 'custom';
m_report_dash_occupation_vips.categories.type = 'custom';
m_report_dash_occupation_vips.resizable = true;
m_report_dash_occupation_vips.hasComparative = true;
m_report_dash_occupation_vips.forceWidget = true;
m_report_dash_occupation_vips.style = 'col50';
m_report_dash_occupation_vips.onCustomGetSeries = function (Data, Template) {
  if (Data.rows.length == 0) {
    return null;
  }
  var _tot = Data.rows[0][1];
  var _vips = Data.rows[1][1];
  this.template.yAxis.min = 0;
  this.template.yAxis.max = _tot;
  //this.template.yAxis.tickPixelInterval = _tot + 1;
  this.template.yAxis.tickPositioner = function() { return [0, _tot] };
  this.template.credits = {enabled: false};
  //this.caption = Round((_vips * 100) / (_tot)) + "% Vips";
  this.caption = "Jugadores VIP";
  return [{
    name: 'Value',
    innerSize: '60%',
    data: [_vips],
    dataLabels: {
                format: '<div style="text-align:center"><span style="font-size:25px;color:' +
                    ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
                       '<span style="font-size:12px;color:silver">Vips</span></div>'
            },
            tooltip: {
                valueSuffix: 'Jugadores VIP'
    }
  }];
};
m_report_dash_occupation_vips.onBeforeUpdate = function (Report) {
  var _i = 0;
  Report.data = {};
  Report.data.columns = ["OCC", "VALUE"];
  Report.data.rows = [];
  Report.data.rows.push(["Total", _Manager.Terminals.OccupationData.TotalCard]);
  Report.data.rows.push(["Vips", _Manager.Terminals.OccupationData.Vips]);
};

// Occupation at Room
var m_report_dash_occupation_room = new WSIReportType('report_dash_occupation_room', -1, '', -1, _report_gauge_2);
m_report_dash_occupation_room.CustomUpdate = true;
//m_report_dash_occupation_room.type = 'column';
m_report_dash_occupation_room.caption = 'DASHBOARD - OCCUPATION';
//m_report_dash_occupation_room.subtitleDate = true;
//m_report_dash_occupation_room.style = 'box-40-2';
m_report_dash_occupation_room.series.type = 'custom';
m_report_dash_occupation_room.categories.type = 'custom';
m_report_dash_occupation_room.hasComparative = true;
m_report_dash_occupation_room.resizable = true;
m_report_dash_occupation_room.forceWidget = true;
m_report_dash_occupation_room.style = 'col50';
m_report_dash_occupation_room.onCustomGetSeries = function (Data, Template) {
  if (Data.rows.length == 0) {
    return null;
  }
  var _card = Data.rows[0][1];
  var _tot = Data.rows[1][1];
  this.template.yAxis.min = 0;
  this.template.yAxis.max = _tot;
  //this.template.yAxis.tickPixelInterval = _tot - 1;
  this.template.yAxis.tickPositioner = function() { return [0, _tot] };
  this.template.credits = {enabled: false};
  //this.caption = Round((_vips * 100) / (_tot)) + "% Vips";
  this.caption = "Jugadores";
  return [{
    name: 'Value',
    innerSize: '60%',
    data: [_card],
    dataLabels: {
                format: '<div style="text-align:center"><span style="font-size:25px;color:' +
                    ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
                       '<span style="font-size:12px;color:silver">Jugadores</span></div>'
            },
            tooltip: {
                valueSuffix: ' Jugadores'
    }
  }];
};
m_report_dash_occupation_room.onBeforeUpdate = function (Report) {
  var _i = 0;
  Report.data = {};
  Report.data.columns = ["OCC", "VALUE"];
  Report.data.rows = [];
  Report.data.rows.push(["Total", _Manager.Terminals.OccupationData.TotalCard]);
  Report.data.rows.push(["Players", _Manager.Terminals.OccupationData.Total]);
};


// Occupation by sex
var m_report_dash_occupation_sex = new WSIReportType('report_dash_occupation_sex', -1, '', -1, _report_balloon_legend);
m_report_dash_occupation_sex.CustomUpdate = true;
//m_report_dash_occupation_sex.type = 'column';
m_report_dash_occupation_sex.caption = 'DASHBOARD - OCCUPATION - SEX';
//m_report_dash_occupation_sex.subtitleDate = true;
//m_report_dash_occupation_sex.style = 'box-40-2';
m_report_dash_occupation_sex.series.type = 'custom';
m_report_dash_occupation_sex.categories.type = 'custom';
m_report_dash_occupation_sex.hasComparative = true;
m_report_dash_occupation_sex.resizable = true;
m_report_dash_occupation_sex.forceWidget = true;
m_report_dash_occupation_sex.style = 'col50';
m_report_dash_occupation_sex.onCustomGetSeries = function (Data, Template) {
  if (Data.rows.length == 0) {
    return null;
  }
  var _result = [{
    type: 'pie',
    name: 'Value',
    innerSize: '60%',
    data: []
  }];
  for (var _idx in Data.rows) {
    _result[0].data.push({ name: Data.rows[_idx][0], y: Data.rows[_idx][1], color: m_dash_report_occupation_gender_colors[Data.rows[_idx][0]] });
  }
  this.caption = _Language.get("NLS_REPORT_OCCUP_GEN");
  return _result;
};
m_report_dash_occupation_sex.onBeforeUpdate = function (Report) {
  var _i = 0;
  Report.data = {};
  Report.data.columns = ["OCC", "VALUE"];
  Report.data.rows = [];
  for (_i in _Manager.Terminals.OccupationData.Gender) {
    Report.data.rows.push([_i, _Manager.Terminals.OccupationData.Gender[_i]]);
  }
};

// Occupation by age
var m_report_dash_occupation_age = new WSIReportType('report_dash_occupation_age', -1, '', -1, _report_pie_percent);
m_report_dash_occupation_age.template.colors = m_dash_report_occupation_age_colors
m_report_dash_occupation_age.CustomUpdate = true;
m_report_dash_occupation_age.type = 'pie';
m_report_dash_occupation_age.caption = 'DASHBOARD - OCCUPATION - AGE';
//m_report_dash_occupation_age.subtitleDate = true;
//m_report_dash_occupation_age.style = 'box-40-2';
m_report_dash_occupation_age.series.type = 'custom';
m_report_dash_occupation_age.categories.type = 'custom';
m_report_dash_occupation_age.resizable = true;
m_report_dash_occupation_age.hasComparative = true;
m_report_dash_occupation_age.forceWidget = true;
m_report_dash_occupation_age.style = 'col50';
m_report_dash_occupation_age.onCustomGetSeries = function (Data, Template) {
  if (Data.rows.length == 0) {
    return null;
  }
  var _result = [];
  var _serie = {};
  _serie.type = 'pie';
  _serie.name = _Language.get("NLS_REPORT_OCCUP_AGE");
  _serie.innerSize = '50%';
  _serie.data = [];
  var _inc = 0;
  for (var _idx in Data.rows) {
    _serie.data.push({ name: Data.rows[_idx][0], y: Data.rows[_idx][1], color: m_dash_report_occupation_age_colors[_inc] });
    _inc += 1;
  }
  this.caption = _Language.get("NLS_REPORT_OCCUP_AGE");
  _result.push(_serie);
  return _result;
};
m_report_dash_occupation_age.onBeforeUpdate = function (Report) {
  var _i = 0;
  Report.data = {};
  Report.data.columns = ["OCC", "VALUE"];
  Report.data.rows = [];
  for (_i in _Manager.Terminals.OccupationData.Ages) {
    Report.data.rows.push([_i, _Manager.Terminals.OccupationData.Ages[_i]]);
  }
};

// by Age / Sex
var m_report_dash_occupation_age_sex = new WSIReportType('report_dash_occupation_age_sex', -1, '', -1, _report_horizontal_stack);
m_report_dash_occupation_age_sex.template.colors = m_dash_report_occupation_age_sex_colors
m_report_dash_occupation_age_sex.CustomUpdate = true;
m_report_dash_occupation_age_sex.type = 'bar';
m_report_dash_occupation_age_sex.caption = 'DASHBOARD - OCCUPATION - AGE/SEX';
//m_report_dash_occupation_age_sex.subtitleDate = true;
//m_report_dash_occupation_age_sex.style = 'box-40-2';
m_report_dash_occupation_age_sex.series.type = 'custom';
m_report_dash_occupation_age_sex.categories.type = 'custom';
m_report_dash_occupation_age_sex.resizable = true;
m_report_dash_occupation_age_sex.forceWidget = true;
m_report_dash_occupation_age_sex.style = 'col50';
m_report_dash_occupation_age_sex.onCustomGetSeries = function (Data, Template) {
  if (Data.rows.length == 0) {
    return null;
  }
  var _result = [];
  var _males = [];
  var _females = [];
  var _categories = [];
  var _max_male = 0;
  var _max_female = 0;
  for (var _idx in Data.rows) {
    _categories.push(Data.rows[_idx][0]);
    _males.push(-Data.rows[_idx][1][0]);
    _females.push(Data.rows[_idx][1][1]);
    if (+_max_male < +Data.rows[_idx][1][0]) { _max_male = Data.rows[_idx][1][0]; }
    if (+_max_female < +Data.rows[_idx][1][1]) { _max_female = Data.rows[_idx][1][1]; }
  }
  _result.push({ name: 'Male', data: _males });
  _result.push({ name: 'Female', data: _females });
  this.template.xAxis[0].categories = _categories;
  this.template.xAxis[1].categories = _categories;
  this.template.yAxis.min = (+_max_male > +_max_female) ? -_max_male : -_max_female;
  this.template.yAxis.max = (+_max_male > +_max_female) ? _max_male : _max_female;
  this.template.yAxis.min -= 20;
  this.template.yAxis.max += 20;
  this.caption = "Ocupación por Edad/Sexo";   // TODO: _Language.get("");
  return _result;
};
m_report_dash_occupation_age_sex.onBeforeUpdate = function (Report) {
  var _i = 0;
  Report.data = {};
  Report.data.columns = ["OCC", "VALUE"];
  Report.data.rows = [];
  for (_i in _Manager.Terminals.OccupationData.AgesGender) {
    Report.data.rows.push([_i, _Manager.Terminals.OccupationData.AgesGender[_i]]);
  }
};

// Alarms
var m_report_dash_occupation_alarms = new WSIReportType('report_dash_occupation_alarms', -1, '', -1, _report_balloon_2);
m_report_dash_occupation_alarms.CustomUpdate = true;
//m_report_dash_occupation_alarms.type = 'column';
m_report_dash_occupation_alarms.caption = 'DASHBOARD - OCCUPATION - VIPS';
//m_report_dash_occupation_alarms.subtitleDate = true;
//m_report_dash_occupation_alarms.style = 'box-40-2';
m_report_dash_occupation_alarms.series.type = 'custom';
m_report_dash_occupation_alarms.categories.type = 'custom';
m_report_dash_occupation_alarms.resizable = true;
m_report_dash_occupation_alarms.hasComparative = true;
m_report_dash_occupation_alarms.forceWidget = true;
m_report_dash_occupation_alarms.style = 'col50';
m_report_dash_occupation_alarms.onCustomGetSeries = function (Data, Template) {
  if (Data.rows.length == 0) {
    return null;
  }
  var _result = [{
    type: 'pie',
    name: 'Value',
    size: '60%',
    innerSize: '45%',
    data: []
  },
  {
    type: 'pie',
    name: 'Value',
    size: '75%',
    innerSize: '70%',
    dataLabels: { enabled: false },
    data: []
  }];
  var _inc = 2;
  var _sum = 0;
  //_result[0].data.push({ name: "Clean", y: _Manager.Terminals.OccupationData.Total - _sum, color: m_dash_report_occupation_alarms_colors[0] });
  for (_i in Data.rows) {
    if (Data.rows[_i][1] > 0) {
    _result[0].data.push({ name: Data.rows[_i][0], y: Data.rows[_i][1], color: m_dash_report_occupation_alarms_colors[_inc] });
    _sum += Data.rows[_i][1];
    _inc += 1;
    }
  }
  //_result[1].data.push({ name: "Clean", y: _Manager.Terminals.OccupationData.Total - _sum, color: m_dash_report_occupation_alarms_colors[0]});
  _result[1].data.push({ name: "Alarmas", y: _sum , color: m_dash_report_occupation_alarms_colors[1]});
  //this.caption = Round((_sum * 100) / (_Manager.Terminals.OccupationData.Total)) + "% Alarms";
  this.caption = " Alarams";
  return _result;
};
m_report_dash_occupation_alarms.onBeforeUpdate = function (Report) {
  var _i = 0;
  Report.data = {};
  Report.data.columns = ["OCC", "VALUE"];
  Report.data.rows = [];
  for (_i in _Manager.Terminals.OccupationData.Alarms) {
    Report.data.rows.push([_i, _Manager.Terminals.OccupationData.Alarms[_i]]);
  }
};

// Winning - floor
var m_report_dash_winnings_floor = new WSIReportType('report_dash_winnings_floor', -1, '', -1, _report_scatter);
m_report_dash_winnings_floor.CustomUpdate = true;
//m_report_dash_winnings_floor.type = 'column';
m_report_dash_winnings_floor.caption = 'DASHBOARD - WINNINGS';
//m_report_dash_winnings_floor.subtitleDate = true;
//m_report_dash_winnings_floor.style = 'box-40-2';
m_report_dash_winnings_floor.series.type = 'custom';
m_report_dash_winnings_floor.categories.type = 'custom';
m_report_dash_winnings_floor.resizable = true;
m_report_dash_winnings_floor.forceWidget = true;
m_report_dash_winnings_floor.style = 'col50';
m_report_dash_winnings_floor.onCustomGetSeries = function (Data, Template) {
  if (Data.rows.length == 0) {
    return null;
  }
  var _result = [{
    name: 'AWPD',
    color: 'rgba(223, 83, 83, .5)',
    data: []
  }
//  , {
//    name: 'COIN_IN',
//    color: 'rgba(119, 152, 191, .5)',
//    data: []
//  }
  ];
  this.caption = "Winnings on floor"; // TODO: _Language.get("");
  //this.caption = Round((_sum * 100) / (_Manager.Terminals.OccupationData.Total)) + "% <br> Alarms";
  for (_i in Data.rows) {
    _result[0].data.push([+Data.rows[_i][1], +Data.rows[_i][2] ]);
    //_result[1].data.push(+Data.rows[_i][2]);
  }
  Template.xAxis.tickPixelInterval = 25000;
  Template.yAxis.tickPixelInterval = 25000;
  return _result;
};
m_report_dash_winnings_floor.onBeforeUpdate = function (Report) {
  var _i = 0;
  Report.data = {};
  Report.data.columns = ["TERMINAL", "AWPD", "COIN_IN"];
  Report.data.rows = [];
  var _cnt = 0;
  for (_i in _Manager.Terminals.Items) {
    if (_cnt > 10) { break; }
    _cnt += 1;
    var _props = _Manager.Terminals.Items[_i].Properties;
    Report.data.rows.push([ _props.external_id, _props.won_amount, _props.played_amount]);
  }
};

//{
//            name: 'MAQUINA1',
//            color: 'rgba(223, 83, 83, .5)',
//            data: [[161.2, 51.6], [167.5, 59.0], [159.5, 49.2], [157.0, 63.0], [155.8, 53.6],
//                [170.0, 59.0], [159.1, 47.6], [166.0, 69.8], [176.2, 66.8], [160.2, 75.2],
//                [172.5, 55.2], [170.9, 54.2], [172.9, 62.5], [153.4, 42.0], [160.0, 50.0],
//                [147.2, 49.8], [168.2, 49.2], [175.0, 73.2], [157.0, 47.8], [167.6, 68.8],
//                [159.5, 50.6], [175.0, 82.5], [166.8, 57.2], [176.5, 87.8], [170.2, 72.8],
//                [174.0, 54.5], [173.0, 59.8], [179.9, 67.3], [170.5, 67.8], [160.0, 47.0],
//                [154.4, 46.2], [162.0, 55.0], [176.5, 83.0], [160.0, 54.4], [152.0, 45.8],
//                [162.1, 53.6], [170.0, 73.2], [160.2, 52.1], [161.3, 67.9], [166.4, 56.6],
//                [168.9, 62.3], [163.8, 58.5], [167.6, 54.5], [160.0, 50.2], [161.3, 60.3],
//                [167.6, 58.3], [165.1, 56.2], [160.0, 50.2], [170.0, 72.9], [157.5, 59.8],
//                [167.6, 61.0], [160.7, 69.1], [163.2, 55.9], [152.4, 46.5], [157.5, 54.3],
//                [168.3, 54.8], [180.3, 60.7], [165.5, 60.0], [165.0, 62.0], [164.5, 60.3],
//                [156.0, 52.7], [160.0, 74.3], [163.0, 62.0], [165.7, 73.1], [161.0, 80.0],
//                [162.0, 54.7], [166.0, 53.2], [174.0, 75.7], [172.7, 61.1], [167.6, 55.7],
//                [151.1, 48.7], [164.5, 52.3], [163.5, 50.0], [152.0, 59.3], [169.0, 62.5],
//                [164.0, 55.7], [161.2, 54.8], [155.0, 45.9], [170.0, 70.6], [176.2, 67.2],
//                [170.0, 69.4], [162.5, 58.2], [170.3, 64.8], [164.1, 71.6], [169.5, 52.8],
//                [163.2, 59.8], [154.5, 49.0], [159.8, 50.0], [173.2, 69.2], [170.0, 55.9],
//                [161.4, 63.4], [169.0, 58.2], [166.2, 58.6], [159.4, 45.7], [162.5, 52.2],
//                [159.0, 48.6], [162.8, 57.8], [159.0, 55.6], [179.8, 66.8], [162.9, 59.4],
//                [161.0, 53.6], [151.1, 73.2], [168.2, 53.4], [168.9, 69.0], [173.2, 58.4],
//                [171.8, 56.2], [178.0, 70.6], [164.3, 59.8], [163.0, 72.0], [168.5, 65.2],
//                [166.8, 56.6], [172.7, 105.2], [163.5, 51.8], [169.4, 63.4], [167.8, 59.0],
//                [159.5, 47.6], [167.6, 63.0], [161.2, 55.2], [160.0, 45.0], [163.2, 54.0],
//                [162.2, 50.2], [161.3, 60.2], [149.5, 44.8], [157.5, 58.8], [163.2, 56.4],
//                [172.7, 62.0], [155.0, 49.2], [156.5, 67.2], [164.0, 53.8], [160.9, 54.4],
//                [162.8, 58.0], [167.0, 59.8], [160.0, 54.8], [160.0, 43.2], [168.9, 60.5],
//                [158.2, 46.4], [156.0, 64.4], [160.0, 48.8], [167.1, 62.2], [158.0, 55.5],
//                [167.6, 57.8], [156.0, 54.6], [162.1, 59.2], [173.4, 52.7], [159.8, 53.2],
//                [170.5, 64.5], [159.2, 51.8], [157.5, 56.0], [161.3, 63.6], [162.6, 63.2],
//                [160.0, 59.5], [168.9, 56.8], [165.1, 64.1], [162.6, 50.0], [165.1, 72.3],
//                [166.4, 55.0], [160.0, 55.9], [152.4, 60.4], [170.2, 69.1], [162.6, 84.5],
//                [170.2, 55.9], [158.8, 55.5], [172.7, 69.5], [167.6, 76.4], [162.6, 61.4],
//                [167.6, 65.9], [156.2, 58.6], [175.2, 66.8], [172.1, 56.6], [162.6, 58.6],
//                [160.0, 55.9], [165.1, 59.1], [182.9, 81.8], [166.4, 70.7], [165.1, 56.8],
//                [177.8, 60.0], [165.1, 58.2], [175.3, 72.7], [154.9, 54.1], [158.8, 49.1],
//                [172.7, 75.9], [168.9, 55.0], [161.3, 57.3], [167.6, 55.0], [165.1, 65.5],
//                [175.3, 65.5], [157.5, 48.6], [163.8, 58.6], [167.6, 63.6], [165.1, 55.2],
//                [165.1, 62.7], [168.9, 56.6], [162.6, 53.9], [164.5, 63.2], [176.5, 73.6],
//                [168.9, 62.0], [175.3, 63.6], [159.4, 53.2], [160.0, 53.4], [170.2, 55.0],
//                [162.6, 70.5], [167.6, 54.5], [162.6, 54.5], [160.7, 55.9], [160.0, 59.0],
//                [157.5, 63.6], [162.6, 54.5], [152.4, 47.3], [170.2, 67.7], [165.1, 80.9],
//                [172.7, 70.5], [165.1, 60.9], [170.2, 63.6], [170.2, 54.5], [170.2, 59.1],
//                [161.3, 70.5], [167.6, 52.7], [167.6, 62.7], [165.1, 86.3], [162.6, 66.4],
//                [152.4, 67.3], [168.9, 63.0], [170.2, 73.6], [175.2, 62.3], [175.2, 57.7],
//                [160.0, 55.4], [165.1, 104.1], [174.0, 55.5], [170.2, 77.3], [160.0, 80.5],
//                [167.6, 64.5], [167.6, 72.3], [167.6, 61.4], [154.9, 58.2], [162.6, 81.8],
//                [175.3, 63.6], [171.4, 53.4], [157.5, 54.5], [165.1, 53.6], [160.0, 60.0],
//                [174.0, 73.6], [162.6, 61.4], [174.0, 55.5], [162.6, 63.6], [161.3, 60.9],
//                [156.2, 60.0], [149.9, 46.8], [169.5, 57.3], [160.0, 64.1], [175.3, 63.6],
//                [169.5, 67.3], [160.0, 75.5], [172.7, 68.2], [162.6, 61.4], [157.5, 76.8],
//                [176.5, 71.8], [164.4, 55.5], [160.7, 48.6], [174.0, 66.4], [163.8, 67.3]]
//        }, {
//            name: 'Male',
//            color: 'rgba(119, 152, 191, .5)',
//            data: [[174.0, 65.6], [175.3, 71.8], [193.5, 80.7], [186.5, 72.6], [187.2, 78.8],
//                [181.5, 74.8], [184.0, 86.4], [184.5, 78.4], [175.0, 62.0], [184.0, 81.6],
//                [180.0, 76.6], [177.8, 83.6], [192.0, 90.0], [176.0, 74.6], [174.0, 71.0],
//                [184.0, 79.6], [192.7, 93.8], [171.5, 70.0], [173.0, 72.4], [176.0, 85.9],
//                [176.0, 78.8], [180.5, 77.8], [172.7, 66.2], [176.0, 86.4], [173.5, 81.8],
//                [178.0, 89.6], [180.3, 82.8], [180.3, 76.4], [164.5, 63.2], [173.0, 60.9],
//                [183.5, 74.8], [175.5, 70.0], [188.0, 72.4], [189.2, 84.1], [172.8, 69.1],
//                [170.0, 59.5], [182.0, 67.2], [170.0, 61.3], [177.8, 68.6], [184.2, 80.1],
//                [186.7, 87.8], [171.4, 84.7], [172.7, 73.4], [175.3, 72.1], [180.3, 82.6],
//                [182.9, 88.7], [188.0, 84.1], [177.2, 94.1], [172.1, 74.9], [167.0, 59.1],
//                [169.5, 75.6], [174.0, 86.2], [172.7, 75.3], [182.2, 87.1], [164.1, 55.2],
//                [163.0, 57.0], [171.5, 61.4], [184.2, 76.8], [174.0, 86.8], [174.0, 72.2],
//                [177.0, 71.6], [186.0, 84.8], [167.0, 68.2], [171.8, 66.1], [182.0, 72.0],
//                [167.0, 64.6], [177.8, 74.8], [164.5, 70.0], [192.0, 101.6], [175.5, 63.2],
//                [171.2, 79.1], [181.6, 78.9], [167.4, 67.7], [181.1, 66.0], [177.0, 68.2],
//                [174.5, 63.9], [177.5, 72.0], [170.5, 56.8], [182.4, 74.5], [197.1, 90.9],
//                [180.1, 93.0], [175.5, 80.9], [180.6, 72.7], [184.4, 68.0], [175.5, 70.9],
//                [180.6, 72.5], [177.0, 72.5], [177.1, 83.4], [181.6, 75.5], [176.5, 73.0],
//                [175.0, 70.2], [174.0, 73.4], [165.1, 70.5], [177.0, 68.9], [192.0, 102.3],
//                [176.5, 68.4], [169.4, 65.9], [182.1, 75.7], [179.8, 84.5], [175.3, 87.7],
//                [184.9, 86.4], [177.3, 73.2], [167.4, 53.9], [178.1, 72.0], [168.9, 55.5],
//                [157.2, 58.4], [180.3, 83.2], [170.2, 72.7], [177.8, 64.1], [172.7, 72.3],
//                [165.1, 65.0], [186.7, 86.4], [165.1, 65.0], [174.0, 88.6], [175.3, 84.1],
//                [185.4, 66.8], [177.8, 75.5], [180.3, 93.2], [180.3, 82.7], [177.8, 58.0],
//                [177.8, 79.5], [177.8, 78.6], [177.8, 71.8], [177.8, 116.4], [163.8, 72.2],
//                [188.0, 83.6], [198.1, 85.5], [175.3, 90.9], [166.4, 85.9], [190.5, 89.1],
//                [166.4, 75.0], [177.8, 77.7], [179.7, 86.4], [172.7, 90.9], [190.5, 73.6],
//                [185.4, 76.4], [168.9, 69.1], [167.6, 84.5], [175.3, 64.5], [170.2, 69.1],
//                [190.5, 108.6], [177.8, 86.4], [190.5, 80.9], [177.8, 87.7], [184.2, 94.5],
//                [176.5, 80.2], [177.8, 72.0], [180.3, 71.4], [171.4, 72.7], [172.7, 84.1],
//                [172.7, 76.8], [177.8, 63.6], [177.8, 80.9], [182.9, 80.9], [170.2, 85.5],
//                [167.6, 68.6], [175.3, 67.7], [165.1, 66.4], [185.4, 102.3], [181.6, 70.5],
//                [172.7, 95.9], [190.5, 84.1], [179.1, 87.3], [175.3, 71.8], [170.2, 65.9],
//                [193.0, 95.9], [171.4, 91.4], [177.8, 81.8], [177.8, 96.8], [167.6, 69.1],
//                [167.6, 82.7], [180.3, 75.5], [182.9, 79.5], [176.5, 73.6], [186.7, 91.8],
//                [188.0, 84.1], [188.0, 85.9], [177.8, 81.8], [174.0, 82.5], [177.8, 80.5],
//                [171.4, 70.0], [185.4, 81.8], [185.4, 84.1], [188.0, 90.5], [188.0, 91.4],
//                [182.9, 89.1], [176.5, 85.0], [175.3, 69.1], [175.3, 73.6], [188.0, 80.5],
//                [188.0, 82.7], [175.3, 86.4], [170.5, 67.7], [179.1, 92.7], [177.8, 93.6],
//                [175.3, 70.9], [182.9, 75.0], [170.8, 93.2], [188.0, 93.2], [180.3, 77.7],
//                [177.8, 61.4], [185.4, 94.1], [168.9, 75.0], [185.4, 83.6], [180.3, 85.5],
//                [174.0, 73.9], [167.6, 66.8], [182.9, 87.3], [160.0, 72.3], [180.3, 88.6],
//                [167.6, 75.5], [186.7, 101.4], [175.3, 91.1], [175.3, 67.3], [175.9, 77.7],
//                [175.3, 81.8], [179.1, 75.5], [181.6, 84.5], [177.8, 76.6], [182.9, 85.0],
//                [177.8, 102.5], [184.2, 77.3], [179.1, 71.8], [176.5, 87.9], [188.0, 94.3],
//                [174.0, 70.9], [167.6, 64.5], [170.2, 77.3], [167.6, 72.3], [188.0, 87.3],
//                [174.0, 80.0], [176.5, 82.3], [180.3, 73.6], [167.6, 74.1], [188.0, 85.9],
//                [180.3, 73.2], [167.6, 76.3], [183.0, 65.9], [183.0, 90.9], [179.1, 89.1],
//                [170.2, 62.3], [177.8, 82.7], [179.1, 79.1], [190.5, 98.2], [177.8, 84.1],
//                [180.3, 83.2], [180.3, 83.2]]
//        }

/////////////////////////////////////////////////////////////////////////////////////////////////////////

// PLAYED - WON
var m_report_dash_played = new WSIReportType('report_dash_played', -1, '', -1, _report_balloon_legend);
m_report_dash_played.CustomUpdate = true;
//m_report_dash_played.type = 'column';
m_report_dash_played.caption = 'DASHBOARD - PLAYED/WON';
//m_report_dash_played.subtitleDate = true;
//m_report_dash_played.style = 'box-40-2';
m_report_dash_played.series.type = 'custom';
m_report_dash_played.categories.type = 'custom';
m_report_dash_played.resizable = true;
m_report_dash_played.forceWidget = true;
m_report_dash_played.style = 'col50';
m_report_dash_played.onCustomGetSeries = function (Data, Template) {
  if (Data.rows.length == 0) {
    return null;
  }
  this.caption = "Played vs Won"; // TODO: _Language.get("");
  var _result = [{
    type: 'pie',
    name: 'Value',
    innerSize: '65%',
    //outterSize: '60%'
    data: []
  }];
  var _idx = 0;
  for (_serie in Data.rows) {
    _result[0].data.push({ name: Data.rows[_serie][0], y: Data.rows[_serie][1], color: m_dash_report_played_colors[_idx] });
    _idx += 1;
  }
  this.template.title.y = -40;
  // TODO: _Language.get("");
  this.caption = parseCurrency(Round(_Manager.Terminals.MonetaryData.CoinIn)) + " Played vs " + parseCurrency(Round(_Manager.Terminals.MonetaryData.Won)) + " Won";
  return _result;
};
m_report_dash_played.onBeforeUpdate = function (Report) {
  var _i = 0;
  Report.data = {};
  Report.data.columns = ["OCC", "VALUE"];
  Report.data.rows = [];
  Report.data.rows.push([ "Played", _Manager.Terminals.MonetaryData.CoinIn]);
  Report.data.rows.push([ "Won", _Manager.Terminals.MonetaryData.Won]);
};

// LEVELS at Room
var m_report_dash_occupation_levels = new WSIReportType('report_dash_occupation_levels', -1, '', -1, _report_balloon_legend);
m_report_dash_occupation_levels.CustomUpdate = true;
//m_report_dash_occupation_levels.type = 'column';
m_report_dash_occupation_levels.caption = 'DASHBOARD - OCCUPATION - LEVELS';
//m_report_dash_occupation_levels.subtitleDate = true;
//m_report_dash_occupation_levels.style = 'box-40-2';
m_report_dash_occupation_levels.series.type = 'custom';
m_report_dash_occupation_levels.categories.type = 'custom';
m_report_dash_occupation_levels.resizable = true;
m_report_dash_occupation_levels.forceWidget = true;
m_report_dash_occupation_levels.hasComparative = true;
m_report_dash_occupation_levels.style = 'col50';
m_report_dash_occupation_levels.onCustomGetSeries = function (Data, Template) {
  if (Data.rows.length == 0) {
    return null;
  }
  // TODO: _Language.get("");
  this.caption = "Ocupación por Nivel"; 
  var _result = [{
    type: 'pie',
    name: 'Value',
    innerSize: '60%',
    data: []
  }];
  var _idx = 0;
  for (_serie in Data.rows) {
    _result[0].data.push({ name: Data.rows[_serie][0], y: Data.rows[_serie][1], color: m_dash_report_occupation_levels_colors[_idx] });
    _idx += 1;
  }
  return _result;
};
m_report_dash_occupation_levels.onBeforeUpdate = function (Report) {
  var _i = 0;
  Report.data = {};
  Report.data.columns = ["OCC", "VALUE"];
  Report.data.rows = [];
  for (_lvl in _Manager.Terminals.OccupationData.Levels)
  {
    Report.data.rows.push([ _lvl, _Manager.Terminals.OccupationData.Levels[_lvl]]);
  }
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////

// Segmentation level - clients
var m_report_dash_segment_lvl_cli = new WSIReportType('report_dash_segment_lvl_cli', -1, '', -1, _report_basic_lines);
m_report_dash_segment_lvl_cli.CustomUpdate = 2;
m_report_dash_segment_lvl_cli.getDataMethod = "GetSegmentationLevelsClients";
m_report_dash_segment_lvl_cli.getDataMethodParams = null;
// TODO: _Language.get("");
m_report_dash_segment_lvl_cli.series.type = 'row';
m_report_dash_segment_lvl_cli.resizable = true;
m_report_dash_segment_lvl_cli.forceWidget = true;
m_report_dash_segment_lvl_cli.style = 'col50';
m_report_dash_segment_lvl_cli.onFormatCategories = function (Categories) { 
  for (_categorie in Categories) {
    Categories[_categorie] = FormatDate(Categories[_categorie]);
  }
  this.caption = 'Segmentación por Nivel/CLiente'; //'DASHBOARD - SEGMENT LVL/CLI';
  return Categories; 
}
m_report_dash_segment_lvl_cli.onFormatSerie = function (Serie, Unused) {
  Serie.name = _Language.get("NLS_LVL_" + Unused);
  Serie.type = "areaspline";
  return Serie;
}

// Segmentation level - visits
var m_report_dash_segment_lvl_vis = new WSIReportType('report_dash_segment_lvl_vis', -1, '', -1, _report_basic_lines);
m_report_dash_segment_lvl_vis.CustomUpdate = 2;
m_report_dash_segment_lvl_vis.getDataMethod = "GetSegmentationLevelsVisits";
m_report_dash_segment_lvl_vis.getDataMethodParams = null;
// TODO: _Language.get("");
m_report_dash_segment_lvl_vis.series.type = 'row';
m_report_dash_segment_lvl_vis.resizable = true;
m_report_dash_segment_lvl_vis.forceWidget = true;
m_report_dash_segment_lvl_vis.style = 'col50';
m_report_dash_segment_lvl_vis.onFormatCategories = function (Categories) { 
  for (_categorie in Categories) {
    Categories[_categorie] = FormatDate(Categories[_categorie]);
  }
  this.caption = 'Segmentación por Nivel/Visita';
  return Categories; 
}
m_report_dash_segment_lvl_vis.onFormatSerie = function (Serie, Unused) {
  Serie.name = _Language.get("NLS_LVL_" + Unused);
  Serie.type = "areaspline";
  return Serie;
}

// Current promotions
var m_report_dash_current_promotions = new WSIReportType('report_dash_current_promotions', -1, '', -1, _report_pie_percent);
m_report_dash_current_promotions.CustomUpdate = 2;
m_report_dash_current_promotions.getDataMethod = "GetCurrentPromotions";
m_report_dash_current_promotions.getDataMethodParams = null;
// TODO: _Language.get("");
m_report_dash_current_promotions.caption = 'Promociones en sala';
m_report_dash_current_promotions.series.type = 'custom';
m_report_dash_current_promotions.resizable = true;
m_report_dash_current_promotions.hasComparative = true;
m_report_dash_current_promotions.forceWidget = true;
m_report_dash_current_promotions.style = 'col50';
m_report_dash_current_promotions.onCustomGetSeries = function (Data, Template) {
  if (Data.rows.length == 0) {
    return null;
  }
  var _result = [];
  var _serie = {};
  _serie.type = 'pie';
  _serie.name = "Promociones en sala";
  _serie.innerSize = '50%';
  _serie.data = [];
  var _inc = 0;
  for (var _idx in Data.rows) {
    _serie.data.push({ name: Data.rows[_idx][2], y: parseCurrency(Data.rows[_idx][4]) });
    _inc += 1;
  }
  this.caption = "Promociones en sala";
  _result.push(_serie);

  return _result;
};


/////////////////////////////////////////////////////////////////////////////////////////////////////////

// LEVELS at Room
var m_report_dash_bet_average_triband = new WSIReportType('report_dash_bet_average_triband', -1, '', -1, _report_speed_triband);
m_report_dash_bet_average_triband.CustomUpdate = 99;
////m_report_dash_bet_average_triband.type = 'column';
//m_report_dash_bet_average_triband.caption = 'Bet Average';
////m_report_dash_bet_average_triband.subtitleDate = true;
////m_report_dash_bet_average_triband.style = 'box-40-2';
//m_report_dash_bet_average_triband.series.type = 'custom';
//m_report_dash_bet_average_triband.categories.type = 'custom';
//m_report_dash_bet_average_triband.resizable = true;
//m_report_dash_bet_average_triband.forceWidget = true;
//m_report_dash_bet_average_triband.style = 'col50';
//m_report_dash_bet_average_triband.onCustomGetSeries = function (Data, Template) {
//  return {
//            name: 'Today',
//            data: [1499],
//            dataLabels: {
//                y: 100,
//                x: 0,
//                crop: false,
//                overflow: 'none',
//                useHTML: true,
//                formatter: function () {
//                    var kmh = this.y;
//                    return '<table cellpadding="2" cellspacing="0"><tbody><tr><td>Yesterday</td><td style="border-left: 1px black solid;">This month/day</td><td style="border-left: 1px black solid;">Prev. month/day</td></tr><tr style="text-align: center;"><td style="color: blue;">1500$</td><td style="color: red; border-left: 1px solid black;">1625$</td><td style="color: green; border-left: 1px solid black;">1450$</td></tr></tbody></table>';
//                },
//                backgroundColor: {
//                    linearGradient: {
//                        x1: 0,
//                        y1: 0,
//                        x2: 0,
//                        y2: 1
//                    },
//                    stops: [
//                        [0, '#DDD'],
//                        [1, '#FFF']
//                    ]
//                }
//            },
//            tooltip: {
//                valueSuffix: '$'
//            }
//        };
//};
//m_report_dash_bet_average_triband.onBeforeUpdate = function (Report) {
//  Report.data = {};
//  Report.data.columns = ["OCC", "VALUE"];
//  Report.data.rows = [];
//  Report.data.rows.push([]);
//};

var m_report_dash_occup_average = new WSIReportType('report_dash_occup_average', -1, '', -1, _report_speed_triband);
m_report_dash_occup_average.CustomUpdate = 99;


/////////////////////////////////////////////////////////////////////////////////////////////////////////

//var m_report_dashboard_objects = [m_report_dash_occupation,
////                                  m_report_dash_occupation_by_card,
//                                  m_report_dash_occupation_vips,
//                                  m_report_dash_occupation_sex,
//                                  m_report_dash_occupation_age,
//                                  m_report_dash_occupation_age_sex,
//                                 ];


