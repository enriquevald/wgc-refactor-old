﻿
function CommonDefinitions() {
  this.EN_SECTION_ID =
  {
    DASHBOARD: "10",
    //        FLOOR: "20",
    TASKS: "30",
    ALARMS: "40",
    ALARMS_PLAYERS: "41",
    ALARMS_MACHINES: "42",
    CHARTS: "50",
    PLAYERS: "60",
    MACHINES: "70",
    MONITOR: "80"
  }

  this.EN_CUSTOM_ALARM_TYPE = {
    SIMPLE: "1",
    PATTERN: "2"
  }

  this.EN_FIELD_TYPE = {
    NONE: 0,
    CURRENCY: 1
  }

  this.EN_TASK_STATUS = {
    NONE: 0,
    ASSIGNED: 1,
    IN_PROGRESS: 2,
    RESOLVED: 3,
    SCALED: 4,
    VALIDATED: 5
  }

  this.EN_TASK_SEVERITY = {
    LOW: 1,
    MEDIUM: 2,
    HIGH: 3
  }

  this.EN_ALARM_SOURCE = {
    SYSTEM: 1,
    FLOOR: 2
  }

  this.EN_ALARM_TYPE = {
    USER: 41,
    TERMINAL: 42,
    CUSTOM: 43
  }


  /*
   * 
   * 
   */
}

// Global Legends variable
var Enum = new CommonDefinitions();
