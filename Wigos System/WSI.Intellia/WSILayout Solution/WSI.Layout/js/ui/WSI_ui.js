﻿////////////////////////////////////////////////////////////////////////////////////////////////
// Functions to set/get UI status
// * requires JQUERY
// * requires WSIUserConfiguration
////////////////////////////////////////////////////////////////////////////////////////////////

// 
m_from_logic = false;

// Section ID from left panel
var CONST_SECTION_ID_DASHBOARD = 10;
var CONST_SECTION_ID_FLOOR = 20;
var CONST_SECTION_ID_TASKS = 30;
var CONST_SECTION_ID_ALARMS = 40;
var CONST_SECTION_ID_ALARMS_ONLY_PLAYERS = 41;
var CONST_SECTION_ID_ALARMS_ONLY_MACHINES = 42;
var CONST_SECTION_ID_CHARTS = 50;
var CONST_SECTION_ID_PLAYERS = 60;
var CONST_SECTION_ID_MACHINES = 70;
var CONST_SECTION_ID_MONITOR = 80;

var m_terminals_filters_selected_count = 0;
var m_players_filters_selected_count = 0;

var m_monitor_alarms_blink = [];

var m_wgdb_date;

var m_current_language = 'en';

var m_used_in_demo = [];

var m_floor_loaded = false;

var m_loading_dialog;

var _m_first_load = false;

// Contructor
function WSI_ui() {
  this.ui_Initialized = false;
}

// Updates the checked statud of CheckBox
WSI_ui.prototype.SetCheckState = function (CheckBox, State) {
  if (State) {
    $(CheckBox).attr("checked", "checked");
  } else {
    $(CheckBox).removeAttr("checked");
  }
}

// Updates value of Selector
WSI_ui.prototype.SetSelectorValue = function (Selector, Value) {
  $(Selector).val(Value);
}

WSI_ui.prototype.GetInitialSection = function () {
  _menu = $('#div_left_submenus').children();

  _section = 9999;
  for (_idx = 0; _idx < _menu.length; _idx++) {
    _aux = _menu[_idx].id.substring(13, 15);
    if (_aux < _section) {
      _section = _aux;
    }
  }
  return _section;
}
// Sets the initial active menu section
WSI_ui.prototype.SetMenuInitialSection = function (Sender, SectionID) {
  m_current_section = SectionID;
  _section = "#a_section_" + SectionID
  $(_section).click();
}

// Sets the initial active legend 
WSI_ui.prototype.SetMenuInitialLegend = function (LegendIndex) {
  _legend = "#div_legend_container_" + LegendIndex
  $(_legend).accordion(m_accordion_props);
}

// Activates a clicked sesion from Sender
WSI_ui.prototype.SetMenuActiveSection = function (Sender, SectionID) {
  var _has_items = $('#sub_menu_' + SectionID).html() != "";

  if (_LayoutMode == 2) {
    //Unselect Terminals
    $($("#" + m_terminal_id_selected))
      .css('border', '1px solid black')
      .css('z-index', '2');

    if ($($("#" + m_terminal_id_selected)).attr("selected")) {
      $($("#" + m_terminal_id_selected))
      .css("top", ($("#" + m_terminal_id_selected).attr('initial_top')))
      .css("left", ($("#" + m_terminal_id_selected).attr('initial_left')))
      .removeAttr("selected");
    }
  }

  if (SectionID > 10) {
    CheckSoundsLoaded();
  }

  //TODO: Debe aparecer un mensaje cuando cambiemos de sección y esté en modo creación de filtros.
  if ((m_filter_mode_enabled || m_custom_alarm_edit_mode)) {

    LayoutYesNoOpen(_Language.get('NLS_WARNING'), _Language.get('NLS_SURE_TO_EXIT_FILTER_CREATION_MODE'), function () {

      if (m_current_section == 80) {
        if (m_custom_alarm_edit_mode) {
          onClickCustomAlarmEditMode();
          $(".btnEdicionFiltros").removeClass("btnEdicionFiltrosOff");
          m_filter_mode_enabled = false;
        } else {
          onClickFilterMode(m_current_section);
        }

      } else {
        onClickFilterMode(m_current_section);
        $("#filtermode_" + m_current_section).removeClass("filtersActivated")
      }
      $("#div_dialog_background_filter_mode").hide();
      _UI.SetMenuActiveSection(Sender, SectionID);

    }, function () { /*onClickFilterMode(m_current_section);*/ });

    return;
  }

  // RMS - Revision paneles derecha
  // Hide info panels
  $(".infoMaquina").hide();
  $(".infoJugador").hide();
  $(".panelDerivar").hide();
  $(".infoMonitor").hide();
  $(".panelInfoAlarma").hide();
  $(".panelModificarEstado").hide();
  $(".infoAlarmas.infoPanelAlarmas").hide();
  $(".infoRunner").hide();

  // Update right panel
  ocultarPanelDerecha();
  ocultarPanelDerechaExtendido();

  if (Sender.parentElement) {
  $('li[id^="btn"]').removeClass("activo");
  $(Sender.parentElement).addClass("activo");
  }

  $('div[id^="div_sub_menu"]').hide();
  cerrarPanelDerecho()

  if ($('.subMenu').hasClass('slideLeft'))
    $("#div_legend_selected_detail").css("visibility", "visible");
  else
    $("#div_legend_selected_detail").css("visibility", "hidden");

  if (_has_items) {
    $('.subMenu').show();
    $('#div_sub_menu_' + SectionID).show();
  } else {
    $('.subMenu').hide();
  }

  previous_section = m_current_section; //DMT Fix Bug 15229
  m_current_section = SectionID;

  if (m_current_filter[m_current_section]) {
    if (m_current_section != 80) {
      RefreshLegendDetailDialog(m_current_filter[m_current_section]);

      var _prop_id = _Legends.Items[m_current_filter[m_current_section]].Property,
      _state = (SectionID != 80 && SectionID != 40) ? m_field_properties[_prop_id].type != 1 : false;
      _UI.controlSetState($("[data-nls=NLS_FLOOR_TEMP_MAP]").parent(), _state);


      if (SectionID == 60) {
        var _st = !_state && _Configuration.ShowHeatMapPlayer;
        $(document).find('#ON60300').prop("checked", _st);
      }
      if (SectionID == 70) {
        var _st = !_state && _Configuration.ShowHeatMap;
        $(document).find('#ON70005').prop("checked", _st);
      }
    }
    //else {
    //  RefreshListLegendDetailDialog();
    //}
  }

  m_section_first_time = true;

  $("#txt_current_section").text(_Language.get($(Sender).children().last().data('nls')).toUpperCase());

  if (SectionID == CONST_SECTION_ID_MONITOR) {
    if ($("#img_sound_image").attr("src") == "design/iconos/alarms/soundOn.svg") {
      //m_monitor_notify_alarms = true;
    } else {
      //m_monitor_notify_alarms = false;
    }
  } else {
    //m_monitor_notify_alarms = false;
  }

  // Aditional operations
  if (SectionID == CONST_SECTION_ID_DASHBOARD) { // if Dashboard

    if (!WSIData.AutoUpdateCharts) {
      UpdateReportDashboard(true)
    };
    $(".wrapDashboard").show();

    //Actualiza toda la info de las listas.
    _Manager.WidgetManager.UpdateWidgetsList();

    // Update dashboard
    _Manager.WidgetManager.UpdateWidgets();

  } else {
    $(".wrapDashboard").hide();
  }

  if (SectionID == CONST_SECTION_ID_TASKS) {
    //Update MyAlarms & MyTasks
    $(".wrapMisTareas").show();
    _Manager.TaskManager.locked = false;
    _Manager.TaskManager.Update();
  } else {
    $(".wrapMisTareas").hide();
  }

  //DMT Fix Bug 15229
  if (previous_section == CONST_SECTION_ID_PLAYERS || previous_section == CONST_SECTION_ID_MACHINES) {
    switch (SectionID) {
      case CONST_SECTION_ID_ALARMS:
      case CONST_SECTION_ID_PLAYERS:
      case CONST_SECTION_ID_MACHINES:
      case CONST_SECTION_ID_MONITOR:
      case CONST_SECTION_ID_DASHBOARD:
      case CONST_SECTION_ID_FLOOR:
      case CONST_SECTION_ID_TASKS:
        $(".wrapJugadores").hide();
        mostrarPanelControl();
        $("#div_legend_selected_detail").css("visibility", "hidden");
        break;
    }
  }

  //// Info panel update
  //OpenInfoPanel();

  if (SectionID == CONST_SECTION_ID_MONITOR) {
    RefreshListLegendDetailDialog();
  }

  var _pos = $("#div_sub_menu_" + m_current_section).find("h3[data-id='" + m_current_filter[m_current_section] + "']").attr("data-index");

  if (m_current_section != 40) {
    //Expand accordion option
    $("#div_sub_menu_" + m_current_section).find(".accordion").accordion("option", "active", +_pos + 1);
    _Configuration.ShowRunnersMap = false;
  } else {
    //Expand accordion option
    $("#div_legend_container_40000").accordion("option", "active", +_pos + 1);
    _Configuration.ShowRunnersMap = true;
  }

  if (m_dashboard_drag_mode_on) {
    clearInterval(m_dashboard_result_timeout);
    m_dashboard_click = false;
    m_dashboard_click_iterations = 0;
    m_dashboard_drag_mode_on = false;
    $("#div_report_chart_reports").find("div[id^='widget'].ui-draggable").removeClass("WigdetDragMode");
  }

  if (m_floor_loaded) {

    if (m_current_section != 40) {
      $(document).find('[onclick*="SetRunnersmap();"]').prop("checked", false);
      _Manager.RunnersMap.SetVisibility(false);
    } else {
      $(document).find('[onclick*="SetRunnersmap();"]').prop("checked", _Configuration.ShowRunnersMap);
      _Manager.RunnersMap.SetVisibility(_Configuration.ShowRunnersMap);
      _Manager.HeatMap.SetVisibility(false);
    }

    UpdateTerminals();

  } else {

    if (SectionID != CONST_SECTION_ID_TASKS && SectionID != CONST_SECTION_ID_DASHBOARD) {
      m_changing_floor = true;

      // RMS : try load without realocation
      //$("#sp_loading_step").hide();
      //$("#loading").css('opacity', '0.5').show();

      $("#loading_floor").show();

      m_loading_dialog = $('<p>' + _Language.get("NLS_LOADING_FLOOR") + '</p>').dialog({
        dialogClass: "no-close",
        title: _Language.get("NLS_LOADING_FLOOR_TITLE"),
        draggable: false,
        modal: true,
        buttons: {}
      });

      m_floor_changed = true;

      // Load
      InitializeScene();

      if (_LayoutMode == 2) { // 2D
        _UI.SetMenuActiveSection(this, m_current_section);
      }
    }
  }

  // Set heatmap state
  // SRL: Moved because it don't work before all scene loaded
  if (_Legends.Items[m_current_filter[m_current_section]]) {
    var _prop_id = _Legends.Items[m_current_filter[m_current_section]].Property,
      _state = (m_current_section != 80 && m_current_section != 40) ? m_field_properties[_prop_id].type != 1 : false;
    _UI.controlSetState($("[data-nls=NLS_FLOOR_TEMP_MAP]").parent(), _state);

    if (_LayoutMode == 2) { // 2D
      // Hide heatmap
      if (m_current_section == 60)
        _Manager.HeatMap.SetVisibility(!_state && _Configuration.ShowHeatMapPlayer);
      if (m_current_section == 70)
        _Manager.HeatMap.SetVisibility(!_state && _Configuration.ShowHeatMap);
    }
  }
}

WSI_ui.prototype.SetMenuActiveRole = function (Sender, RoleID) {
  PageMethods.SetCurrentRole(+RoleID, OnActiveRoleSuccess, OnActiveRoleError);
}

WSI_ui.prototype.Set3DMode = function (Sender) {
  //if ($("#btn3DModeIcon").hasClass('3DModeMove')) {
  //  // To rotate
  //  //WSIData.swap3DButtons = false;
  //  $("#btn3DModeIcon").switchClass('3DModeMove', '3DModeRotate');
  //  $("#btn3DModeText").text("Rotate");
  //} else {
  //  // To move
  //  WSIData.swap3DButtons = true;
  //  $("#btn3DModeIcon").switchClass('3DModeRotate', '3DModeMove');
  //  $("#btn3DModeText").text("Move");
  //}
}

WSI_ui.prototype.CreateAditionalControls = function () {

  var _control = "";

  switch (_LayoutMode) {
    case 1: // 3D
      _control = _control + "<div class='tool_div'>";
      _control = _control + " <a onclick='onZoomIn();' href='#'>";
      _control = _control + " <div class='toolImgIcon btnZoomInIcon'></div>";
      _control = _control + " <a onclick='onZoomOut();' href='#'>";
      _control = _control + " <div class='toolImgIcon btnZoomOutIcon'></div>";
      _control = _control + " <a onclick='_UI.Set3DMode(this);' href='#'>";
      _control = _control + " <div class='toolImgIcon btnRotateIcon tool_icon_selected'></div>";
      _control = _control + " <a onclick='_UI.Set3DMode(this);' href='#'>";
      _control = _control + " <div class='toolImgIcon btnMoveIcon'></div>";
      _control = _control + " <a onclick='onResetZoom();' href='#'>";
      _control = _control + " <div class='toolImgIcon btnResetIcon'></div>";
      _control = _control + "</div>";

      $("#WSIRenderer").append(_control);

      $(".btnRotateIcon").click(function (e) {
        $(".btnMoveIcon").removeClass("tool_icon_selected");
        $(".btnRotateIcon").addClass("tool_icon_selected");
        WSIData.swap3DButtons = false;
      });

      $(".btnMoveIcon").click(function (e) {
        $(".btnRotateIcon").removeClass("tool_icon_selected");
        $(".btnMoveIcon").addClass("tool_icon_selected");
        WSIData.swap3DButtons = true;
      });

      break;
    case 2: // 2D
      _control = _control + "<div class='tool_div_2d'>";
      _control = _control + " <a onclick='onZoomIn_2D();' href='#'>";
      _control = _control + " <div class='toolImgIcon btnZoomInIcon'></div>";
      _control = _control + " <a onclick='onZoomOut_2D();' href='#'>";
      _control = _control + " <div class='toolImgIcon btnZoomOutIcon'></div>";
      _control = _control + " <a onclick='onResetZoom_2D();' href='#'>";
      _control = _control + " <div class='toolImgIcon btnResetIcon_2d'></div>";
      _control = _control + "</div>";

      $("#div_container").before(_control);

      break;
  }

  // Add an event to the document that close the menu on any click outside that
  $("html").click('click', function () {
    if ($('.configBtn').hasClass('close')) {
      // Activate when target is not contained by the menu and clicked item is not the open menu button
      if (!$.contains($('.configuracionProfile')[0], $(event.target)[0]) && !$(event.target).parent().hasClass('configBtn')) {
        if (($('.configBtn').hasClass('close'))) slideConfig();
      }
      CheckSoundsLoaded();
    }

    if ($('.floorBtn').hasClass('close')) {
      // Activate when target is not contained by the menu and clicked item is not the open menu button
      if (!$.contains($('.configuracionFloor')[0], $(event.target)[0]) && !$(event.target).parent().hasClass('floorBtn')) {
        if (($('.floorBtn').hasClass('close'))) slideConfigFloor();

      }
      CheckSoundsLoaded();
    }

    //if ($(element).closest(".WidgetDraggablePart").length > 0)
    if ($(event.target).closest(".wrapDashboard").length == 0) {
      _Manager.WidgetManager.SetMove(false);
      $("#div_report_chart_reports").find("div[id^='widget'].ui-draggable").removeClass("WigdetDragMode");
      clearInterval(m_dashboard_result_timeout);
      m_dashboard_click = false;
      m_dashboard_click_iterations = 0;
      m_dashboard_drag_mode_on = false;
    }

    if (m_runner_id_selected == null) {
      $("#div_runner_selector").hide();
    }
  });
}

// Apply disabled control class and mark children elements as disabled
WSI_ui.prototype.controlDisable = function (Control) {
  if (Control instanceof jQuery) {
    Control = $(Control);
  }
  Control.addClass("control-dissabled");
  Control.children().prop("disabled", true);
}

// Removes disabled control class and mark children elements as enabled
WSI_ui.prototype.controlEnable = function (Control) {
  if (Control instanceof jQuery) {
    Control = $(Control);
  }
  Control.removeClass("control-dissabled");
  Control.children().prop("disabled", false);
}

// Sets the control enabled or disabled
WSI_ui.prototype.controlSetState = function (Control, Disabled) {
  if (Control instanceof jQuery) {
    Control = $(Control);
  }
  if (Disabled) { this.controlDisable(Control); } else {
    this.controlEnable(Control);
  }
}

// Returns true if a control is enabled, false if its disabled
WSI_ui.prototype.controlGetState = function (Control) {
  if (Control instanceof jQuery) {
    Control = $(Control);
  }
  return (Control.prop("disabled") == false);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function OnActiveRoleSuccess(result) {
  if (result) {
    location.href = './Home.aspx'; //?Role=' + RoleID;
  }
}

function cerrarPanelDerecho() {

  if ($(".wrapJugadores.terminalMode").children().length > 0) {
    $(".wrapJugadores.terminalMode").removeClass("terminalMode");
    cerrarListaTerminales();
  }

  if ($(".wrapJugadores.customerMode").children().length > 0) {
    $(".wrapJugadores.customerMode").removeClass("customerMode");
    cerrarListaJugadores();
  }

  if ($(".wrapJugadores.terminalcompareMode").children().length > 0) {
    $(".wrapJugadores.terminalcompareMode").removeClass("terminalcompareMode");
    cerrarCompararTerminales();
  }

  if ($(".wrapJugadores.customercompareMode").children().length > 0) {
    cerrarCompararJugadores();
    $(".wrapJugadores.customercompareMode").removeClass("customercompareMode");
  }

  if ($(".wrapJugadores.terminalAlarmsMode").children().length > 0) {
    cerrarListaAlarmas();
    $(".wrapJugadores.terminalAlarmsMode").removeClass("terminalAlarmsMode");
  }

}

function OnActiveRoleError() {
  GotoError("OnActiveRoleError");
}

// Initializes the screen controls using user configuration
WSI_ui.prototype.InitControls = function () {
  if (!this.ui_Initialized) {

    WSILogger.Write("Initializing UI...");

    // Handling close window?
    window.addEventListener("beforeunload", function (e) {
      if (m_from_logic) {
        PageMethods.OnBeforeUnload();
      }
    });

    RadionButtonSelectedValueSet('radLanguageSelector', _Configuration.Language);

    switch (_Configuration.Language) {
      case 0:
        m_current_language = 'es';
        break;

      case 1:
        m_current_language = 'en';
        break;

      default:
        m_current_language = 'en';
        break;
    }
    _Language.LoadLanguage(m_current_language);

    // Aditional controls creation
    this.CreateAditionalControls();

    // 
    $.each($("div[id^='div_legend_container_']"), function (key, value) {
      if ($(value).children().first().data("section") == 41 || $(value).children().first().data("section") == 42) {
        m_current_filter[40] = _Legends.Items.length - 1;
        m_section_alarms_all = true;
      } else {
        m_current_filter[$(value).children().first().data("section")] = $(value).children().first().data("id");
      }
    });

    // TODO: Intialize screen controls from _Configuration

    // -----------------------------------------------------
    // TODO: 
    // RMS - Work in progress of new Dashboard

    // Create dashboard control
    _Manager.WidgetManager = new WidgetManager({
      id: "WidgetManagerGrid",
      parentId: "div_report_chart_reports",
      //id: "div_dashboard",
      //leftMargin: "20%",
      width: 4,
      //animate: true,
      cell_height: 30,
      resizable: {
        autoHide: true, handles: 'se'
      }
    });

    // TODO: Get current dashboard from database
    //_Manager.WidgetManager.SetDashboard(m_loaded_dashboard);
    //_Manager.WidgetManager.UpdateWidgetsList();
    //_Manager.WidgetManager.UpdateWidgets();


    // Layout Background Image
    this.SetCheckState("#mostrarFondoConfig", _Configuration.ShowBackground);
    //SetBackground(true);

    // Areas visibility
    this.SetCheckState("#mostrarAreasConfig", _Configuration.ShowAreas);
    //SetAreas(true);

    // Banks visibility
    this.SetCheckState("#mostrarIslasConfig", _Configuration.ShowBanks);
    //SetIslands(true);

    //	Estado en tiempo real	(ShowActivity)
    // this.SetCheckState("#mostrarIslasConfig", _Configuration.ShowActivity);

    //	Mostrar Cuadrícula	(ShowGrid)
    //this.SetCheckState("#mostrarCuadricula", _Configuration.ShowGrid);
    //SetGrid(true);

    //	Mostrar Alarmas (ShowAlarms)
    //this.SetCheckState("#mostrarAlarmas", _Configuration.ShowAlarms);

    //	Actividad (numero)	(RealTimeRefreshSeconds)
    // this.SetCheckState("#mostrarIslasConfig", _Configuration.RealTimeRefreshSeconds);

    //	Modo Test	(TestEnabled)
    //this.SetCheckState("#mostrarModoTest", _Configuration.TestEnabled);

    //	Monitor	(RealTimeRefresh)
    //this.SetCheckState("#mostrarModoMonitor", _Configuration.ShowRealTimeRefresh);

    // Views 
    //this.SetSelectorValue("#sel_views", _Configuration.DefaultView);

    //Get Initial Section for current permissions.
    _section = this.GetInitialSection();

    // Activate DashBoard section
    this.SetMenuInitialSection(0, _section);

    //Activate Menu Legend
    this.SetMenuInitialLegend(_section);

    // 2D/3D
    RadionButtonSelectedValueSet('radioLayoutViewMode', _LayoutMode);

    //PlaySound
    SetMonitorSound();

    // Chat send with return
    $('#txtSendMessage').keypress(function (ev) {
      if (window.event.keyCode == 13 && $(ev.target).val() != '') {
        // Send message
        $('.btnSendMessageDerecha').click();
      }
    });

    /**
     * EVENTS
     */
    // Language
    $("input[name=radLanguageSelector]:radio").click(function () {
      var _new_language = +$("input[type='radio'][name=radLanguageSelector]:checked").val();
      _Language.CurrentLanguage = _new_language;
      _Configuration.Language = _new_language;

      switch (m_current_section) {
        case 10:
            _Manager.WidgetManager.UpdateWidgets();
            break;
        case 30:
          var encabezado = "<option value='' disable selected data-nls='NLS_TYPE'>" + _Language.get('NLS_TYPE') + "</option>";
          $("#selTaskType").html(encabezado + selectAlarmTypeHTMLContent());
          $("#selAlarmStatus").html(encabezado + selectAlarmTypeHTMLContent());
        case 40:
          var encabezado = "<option value='' selected>" + _Language.get('NLS_SUBCATEGORY') + "</option>";
          $("#selSubcategoryAlarms").html(encabezado + selectAlarmTypeHTMLContent());

          var _term_pro;
          var _term_name;
          var _runner_name;
          var _runner;

          if ($(".popup_terminal_name").length) {
            _term_name = $(".popup_terminal_name").eq(0).text();
          }
          
          for (_idx in _Manager.Terminals.Items) {
            if (_Manager.Terminals.Items[_idx].Properties.name == _term_name) {
              _term_pro = _Manager.Terminals.Items[_idx].Properties;
              break;
            }
          }
          
          if (_term_pro) {
            _alarms = _Manager.TaskManager.AlarmsByTerminal[+_term_pro.external_id];
            _tasks = _Manager.TaskManager.TasksByTerminal[+_term_pro.external_id];
            _last_tasks = _Manager.TaskManager.LastTasks[+_term_pro.external_id];

            TerminalInfoPanelSectionAlarmsFill(_alarms, _tasks, _last_tasks);
          }

          if ($(".popup_runner_name").length) {
            _runner_name = $(".popup_runner_name").eq(0).text();
          }

          for (_idx_runner in _Manager.Runners.Items) {

            if (_Manager.Runners.Items[_idx_runner].userName == _runner_name) {
              _runner = _Manager.Runners.Items[_idx_runner];
              break;
            }
          }

          if (_runner) {
            UpdatePopupRunner(_runner);
          }

        case 60:
        case 70:
          if ($(".wrapJugadores").is(":visible")) {
            if ($('.subMenu').hasClass('slideLeft')) {
              mostrarPanelControl();
              $("#div_legend_selected_detail").css("visibility", "hidden");
            }
            cerrarPanelDerecho();
          }

          break;
        default:
          break;
      }

      switch (_Configuration.Language) {
        case 0:
          m_current_language = 'es';
          break;

        case 1:
          m_current_language = 'en';
          break;

        default:
          m_current_language = 'en';
          break;
      }
      _Language.ReloadLanguage(m_current_language);

      m_config_has_changed = true;
      SaveConfigurationDB(false);
      _Language.parse();
      if (m_current_section != 80) {
        RefreshLegendDetailDialog(m_current_filter[m_current_section]);
      }
    });

    // 2D/3D
    $("input[name=radioLayoutViewMode]:radio").click(function () {
      var _new_view_mode = +$("input[type='radio'][name=radioLayoutViewMode]:checked").val();
      //
      _Configuration.ViewMode = _new_view_mode;
      m_config_has_changed = true;
      SaveConfigurationDB(true);
      //
      //PageMethods.ChangeViewMode(_new_view_mode, function () { location.href = './Home.aspx'; });
    });

    // Cameras
    $("input[name=radioLayoutCameraMode]:radio").click(function () {
      var _new_view_mode = +$("input[type='radio'][name=radioLayoutCameraMode]:checked").val();
      _Manager.Cameras.CurrentMode = _new_view_mode; _Manager.Controls.SetCurrentMode(_new_view_mode);

      // LOD Update
      switch (_new_view_mode) {
        case EN_CAMERA_MODE.CM_FIRST_PERSON:
          WSIData.ShowPlayers = true;
          $(".tool_div").hide();
          break;
        default:
          WSIData.ShowPlayers = false;
          $(".tool_div").show();
          break;
      }

      UpdateTerminals();

    });

    // Hide selector on close info panel
    addEvent($(".cierraPanel a")[0], 'click', function () {
      switch (_LayoutMode) {
        case 1: // 3D
          if (m_current_section == 40 && _Configuration.ShowRunnersMap) {
            _Manager.RunnersMap.RunnerSelected = null;
            _Manager.RunnersMap.ForcedUpdate();
          } else {
            _Manager.HideSelector();
          }
          break;
        case 2: // 2D
          UnselectTerminalDiv();
          break;
      }
    });

    //Acción del modo lista jugadores
    $('#jugadoresListaON').change(function () {
      mostrarListaJugadores();
    });

    function selectAlarmTypeHTMLContent() {
      var _result = "";
      for (_idx_dic_alarms_machine in _Legends.Dictionary_Alarms[41]) {
        _result += "        <option value='" + _idx_dic_alarms_machine + "'>" + $(_Language.parse($("<div>" + _Legends.Dictionary_Alarms[41][_idx_dic_alarms_machine] + "</div>")[0])).html() + "</option>";
      }
      for (_idx_dic_alarms_player in _Legends.Dictionary_Alarms[42]) {
        _result += "        <option value='" + _idx_dic_alarms_player + "'>" + $(_Language.parse($("<div>" + _Legends.Dictionary_Alarms[42][_idx_dic_alarms_player] + "</div>")[0])).html() + "</option>";
      }
      for (_idx_dic_alarms_player in _Legends.Dictionary_Alarms[43]) {
        _result += "        <option value='" + _idx_dic_alarms_player + "'>" + $(_Language.parse($("<div>" + _Legends.Dictionary_Alarms[43][_idx_dic_alarms_player] + "</div>")[0])).html() + "</option>";
      }
      return _result;
    }


    function mostrarListaJugadores() {
      if ($('#jugadoresListaON').is(':checked')) {
        ocultarPanelControl();
        ocultarSlideButton();
        WSICustomersAtRoom_Open();

        $('.wrapJugadores').css('display', 'block');
        $('.wrapJugadores').animate({
          right: '0'
        });

        $('.wrapJugadores').removeClass('show').addClass('hide');
      } else {
        $('.wrapJugadores').animate({
          right: '-100%'
        }, 300, function () {
          $('.wrapJugadores').css('display', 'none');
        });
      }
    }

    $('#jugadoresCompararON').change(function () {
      mostrarCompararJugadores();
    });

    function mostrarCompararJugadores() {
      if ($('#jugadoresCompararON').is(':checked')) {
        ocultarPanelControl();
        ocultarSlideButton();

        WSICustomersAtRoom_Comparar_Open();

        $('.wrapJugadores').css('display', 'block');
        $('.wrapJugadores').animate({
          right: '0'
        });

        $('.wrapJugadores').removeClass('show').addClass('hide');
      } else {
        $('.wrapJugadores').animate({
          right: '-100%'
        }, 300, function () {
          $('.wrapJugadores').css('display', 'none');
        });
      }
    }

    //Acción del modo lista jugadores
    $('#terminalesListaON').change(function () {
      mostrarListaTerminales();
    });

    function mostrarListaTerminales() {
      if ($('#terminalesListaON').is(':checked')) {
        ocultarPanelControl();
        ocultarSlideButton();
        WSITerminalsAtRoom_Open();
        $('.wrapJugadores').css('display', 'block');
        $('.wrapJugadores').animate({
          right: '0'
        });
        $('.wrapJugadores').removeClass('show').addClass('hide');
      } else {
        $('.wrapJugadores').animate({
          right: '-100%'
        }, 300, function () {
          $('.wrapJugadores').css('display', 'none');
        });
      }
    }

    $('#terminalesCompararON').change(function () {
      mostrarCompararTerminales();
    });

    function mostrarCompararTerminales() {
      if ($('#terminalesCompararON').is(':checked')) {
        ocultarPanelControl();
        WSITerminalsAtRoom_Compare_Open();
        $('.wrapJugadores').css('display', 'block');
        $('.wrapJugadores').animate({
          right: '0'
        });
        $('.wrapJugadores').removeClass('show').addClass('hide');
      } else {
        $('.wrapJugadores').animate({
          right: '-100%'
        }, 300, function () {
          $('.wrapJugadores').css('display', 'none');
        });
      }
    }

    $('#alarmasListaON').change(function () {
      mostrarListaAlarmas();
    });

    function mostrarListaAlarmas() {
      if ($('#alarmasListaON').is(':checked')) {
        ocultarPanelControl();
        ocultarSlideButton();

        WSIAlarmsAtRoom_Open();
        $('.wrapJugadores').css('display', 'block');
        $('.wrapJugadores').animate({
          right: '0'
        });
        $('.wrapJugadores').removeClass('show').addClass('hide');
      } else {
        $('.wrapJugadores').animate({
          right: '-100%'
        }, 300, function () {
          $('.wrapJugadores').css('display', 'none');
        });
      }
    }



    $('#inputBuscarJugadores').bind("focus", function () {
      var _html = '';
      var _list = _Manager.Terminals.Items;
      $('#datalist1').empty();

      for (_idx_customers_in_terminals in _list) {
        var _name = _Manager.Terminals.Items[_idx_customers_in_terminals].Properties.player_name;
        if (_name) {
          if (_name != "Anonymous" && _name != "Anónimo") {
          _html += '<option value="' + _name + '">';
        }
      }
      }

      $('#datalist1').html(_html)
    })

    $('#jugadoresbtnBuscarUsuario').click(function () {

      m_account_id_selected = null;

      if (_LayoutMode == 2) {
        //Unselect Terminals
        $($("#" + m_terminal_id_selected))
          .css('border', '1px solid black')
          .css('z-index', '2');

        if ($($("#" + m_terminal_id_selected)).attr("selected")) {
          $($("#" + m_terminal_id_selected))
          .css("top", ($("#" + m_terminal_id_selected).attr('initial_top')))
          .css("left", ($("#" + m_terminal_id_selected).attr('initial_left')))
          .removeAttr("selected");
        }
      }
      var _search_value = $("#inputBuscarJugadores").val();

      if (_search_value != "") {
        for (_idx_buscar_jugador in _Manager.Terminals.Items) {
          var _t = _Manager.Terminals.Items[_idx_buscar_jugador];
          if (_t.Properties.player_name && _t.Properties.player_name != "") {
            if (_t.Properties.player_name.toUpperCase().startsWith(_search_value.toUpperCase())) {
              m_account_id_selected = _t.Properties.player_account_id

              break;
            }
          }
        }
        m_terminal_id_selected = _t.Properties.lo_id;

        if(m_account_id_selected) {
        ShowTerminalInfo(_t, 1, false);
        } else {
          ocultarPanelDerecha();
      }

      }
    });

    $('#inputBuscarTerminales').bind("focus", function () {
      var _html = '';
      var _list = _Manager.Terminals.Items;
      $('#datalist2').empty();

      for (_idx_terminals_in_terminals in _list) {
        var _name = _Manager.Terminals.Items[_idx_terminals_in_terminals].Properties.name;
        var _status = _Manager.Terminals.Items[_idx_terminals_in_terminals].Properties.status;

        //SL 1/11 - RETIRED TERMINALS NOT SHOW IN LIST
        if (_name && _status != 2) {
          _html += '<option value="' + _name + '">';
        }
      }

      $('#datalist2').html(_html)
    })

    $('#terminalesbtnBuscarTerminal').click(function () {

      m_terminal_id_selected = null;

      if (_LayoutMode == 2) {
        //Unselect Terminals
        $($("#" + m_terminal_id_selected))
          .css('border', '1px solid black')
          .css('z-index', '2');

        if ($($("#" + m_terminal_id_selected)).attr("selected")) {
          $($("#" + m_terminal_id_selected))
          .css("top", ($("#" + m_terminal_id_selected).attr('initial_top')))
          .css("left", ($("#" + m_terminal_id_selected).attr('initial_left')))
          .removeAttr("selected");
        }
      }


      var _search_value = $("#inputBuscarTerminales").val();

      if (_search_value != "") {
        for (_idx_buscar_terminal in _Manager.Terminals.Items) {
          var _t = _Manager.Terminals.Items[_idx_buscar_terminal];
          if (_t.Properties.name && _t.Properties.name != "") {
            if (_t.Properties.name.toUpperCase().startsWith(_search_value.toUpperCase())) {
              m_terminal_id_selected = _t.Properties.lo_id;

              break;
            }
          }
        }


        if (m_terminal_id_selected) {
          ShowTerminalInfo(_t, 1, false);
        } else {
          ocultarPanelDerecha();
        }



      }
    });

    //When we have machine alarms and player alarms section, create filters icon has to be hidden.
    if ($("[data-alarmtype='41']").length > 0 && $("[data-alarmtype='42']").length > 0) {
      $("[data-alarmtype='41']").css("visibility", "hidden");
    }

    // RMS - Floor change combo
    $("#planoPisoSelector").change(function (Event) {
      if (Event) {

        m_changing_floor = true;

        // RMS - Save configuration with the new floor id and navigate
        _Configuration.CurrentFloorId = +Event.target.value;
        m_config_has_changed = true;

        // RMS : try load without realocation
        $("#sp_loading_step").hide();
        $("#loading").css('opacity', '0.5').show();

        SaveConfigurationDB(false);
        ResetScene();

        // Reset terminal and customer compare lists
        m_tar_compare_list = [];
        m_cat_compare_list = [];

        // Init terminals
        _Manager.Terminals = new WSITerminals();
        _Manager.OtherTerminals = new WSITerminals();

        m_floor_changed = true;

        // Load
        //LoadLayout();

        // Close menu
        slideConfig();
      }
    });

    $(".btnJugadorComparar").click(function () {

      var _idx = $.inArray(m_account_id_selected, m_cat_compare_list);

      if (_idx == -1) {
        // Add to compare

        m_cat_compare_list.push(m_account_id_selected);

        //$(this).removeClass("btnCompararJugador");
        $(this).addClass("selCompararJug");

        // switch nls
        $(this).find("a").attr("data-nls", "NLS_RETIRE_COMPARE_PLAYER");

        showOrHiddenButtonCompare();

      } else {
        // Retire from compare

        m_cat_compare_list.splice(_idx, 1);

        $(this).removeClass("selCompararJug");
        //$(this).addClass("btnCompararJugador");

        // switch nls
        $(this).find("a").attr("data-nls", "NLS_COMPARE_PLAYER");

        showOrHiddenButtonCompare();

      }

      _Language.parse(this);

    });

    function showOrHiddenButtonCompare() {
      if (m_cat_compare_list.length > 1) {
        $("#jugadoresListaIraComparar").show();
      } else {
        $("#jugadoresListaIraComparar").hide();
      }
    }

    $(".btnTerminalComparar").click(function () {
      var _ter;
      if (m_terminal_id_selected.indexOf('_')!=-1)
        _ter = m_terminal_id_selected.split('_')[1]
      else
        _ter = m_terminal_id_selected;
      var _terminal_ext_id = _Manager.Terminals.Items[_ter].Properties.lo_external_id;
      var _idx = $.inArray(_terminal_ext_id, m_tar_compare_list);

      if (_idx == -1) {
        // Add to compare

        m_tar_compare_list.push(_terminal_ext_id);

        $(this).addClass("selCompararTerminal");

        // switch nls
        $(this).find("a").attr("data-nls", "NLS_RETIRE_COMPARE_TERMINAL");

      } else {
        // Retire from compare

        m_tar_compare_list.splice(_idx, 1);

        $(this).removeClass("selCompararTerminal");

        // switch nls
        $(this).find("a").attr("data-nls", "NLS_COMPARE_TERMINAL");

      }

      _Language.parse(this);
    });

    $(".ojoTerminal").click(function (e) {
      _UI.SetMenuActiveSection($("#a_section_70")[0], 70);

      if (_LayoutMode == 1) { // 3D
        var _terminal_sel = m_terminal_id_selected;
        OnTerminalClick(m_terminal_id_selected, "#" + CONST_ID_TERMINAL + m_terminal_id_selected, null);
        m_terminal_id_selected = _terminal_sel;
        OpenInfoPanel();
        //ShowTerminalInfo(_Manager.Terminals.Items[_terminal_id], 1, false);
      } else {
        TerminalClickEvent(e, $("#" + m_terminal_id_selected), null);
        OpenInfoPanel();
      }
    });

    $(".ojoJugador").click(function (e) {
      _UI.SetMenuActiveSection($("#a_section_60")[0], 60);

      if (_LayoutMode == 1) { // 3D
        var _terminal_sel = m_terminal_id_selected;
        OnTerminalClick(m_terminal_id_selected, "#" + CONST_ID_TERMINAL + m_terminal_id_selected, null);
        m_terminal_id_selected = _terminal_sel;
        OpenInfoPanel();
      } else {
        TerminalClickEvent(e, $("#" + m_terminal_id_selected), null);
        OpenInfoPanel();
      }

    });

    $(".infoMaquina").hide();
    $(".infoJugador").hide();
    $(".infoPanelAlarmas").hide();
    $(".panelDerivar").hide();
    $(".panelInfoAlarma").hide();
    $(".panelModificarEstado").hide();
    $(".infoMonitor").hide();
    $(".infoRunner").hide();

    if (!_Configuration.SoundEnabled) {
      $('.SoundBtn').addClass("mute");
    }

    //Activa/Descativa sonido
    $('.SoundBtn a').click(function () {
      //mutear sonido
      if ($('.SoundBtn').hasClass("mute")) {
        $('.SoundBtn').removeClass("mute");
        _Configuration.SoundEnabled = true;
      } else {
        $('.SoundBtn').addClass("mute");
        _Configuration.SoundEnabled = false;
        $("#sound_alarm")[0].pause();
        if ($("#sound_alarm")[0].readyState > 0) {
          $("#sound_alarm")[0].currentTime = 0;
        }
      }

      CheckSoundsLoaded();
      m_config_has_changed = true;
      SaveConfigurationDB(false);
    });

    $('.refreshDashboardBtn').prop("title", _Language.get("NLS_RESET_DASHBOARD")).prop("data-nls", "NLS_RESET_DASHBOARD");
    $('.messageBtn').prop("title", _Language.get("NLS_MESSAGES")).prop("data-nls", "NLS_MESSAGES");
    $('.SoundBtn').prop("title", _Language.get("NLS_SOUND")).prop("data-nls", "NLS_SOUND");
    $('.floorBtn').prop("title", _Language.get("NLS_FLOOR_CONFIG")).prop("data-nls", "NLS_FLOOR_CONFIG");
    $('.configBtn').prop("title", _Language.get("NLS_CONFIGURATION")).prop("data-nls", "NLS_CONFIGURATION");

    // Translate
    _Language.parse();

    // Prevent another initialization
    this.ui_Initialized = true;

    // Activate "threat"
    // $("#sp_loading_step").text('Starting Activity threat...');

    WSILogger.Write("Setting Activity On!");
    SetActivity('on', true);
  }

  InitializeTopBarTime();

}

WSI_ui.prototype.InitFloorControls = function () {

  SetBackground(true);
  SetAreas(true);
  SetIslands(true);

  //	Mostrar Mapa de Temperatura	(ShowHeatMap)
  _Manager.HeatMap.Create(101 * 4, 54 * 4);


  //_Manager.HeatMap.SetVisibility(true);
  //_Manager.HeatMap.SetVisibility(_Configuration.ShowHeatMap);
  if (m_current_section == 40)
    _Manager.HeatMap.SetVisibility(false);
  else
    _Manager.HeatMap.SetVisibility(true);


  $(document).find('#ON60300').prop("checked", _Configuration.ShowHeatMapPlayer);
  $(document).find('#ON70005').prop("checked", _Configuration.ShowHeatMap);

  // In 3D mode hide background image
  if (_LayoutMode == 1) {
    $("#img_background").hide();
  } else {
    // In 2D mode, hide cameras menú
    $('#radioLayoutCameraModeDiv').hide();
  };

  //Mostrar Mapa de Runners(_Configuration.ShowRunnersMap)
  _Manager.RunnersMap.Create(101 * 4, 54 * 4);
  _Manager.RunnersMap.SetVisibility(true);
  _Manager.RunnersMap.SetVisibility(_Configuration.ShowRunnersMap);
  $(document).find('[onclick*="SetRunnersmap();"]').prop("checked", _Configuration.ShowRunnersMap);

  if (!$(document).find('[onclick*="SetRunnersmap();"]').length)
    _Manager.RunnersMap.SetVisibility(false);

  // Camera default
  RadionButtonSelectedValueSet('radioLayoutCameraMode', 0);

  ////////////////////////////////////////////////////////////////////////////////////////////////

  // Gestures support
  var _element = null;
  switch (_LayoutMode) {
    case 1: // 3D
      _element = document.getElementById("WebGLRenderer");
      break;
    case 2: // 2D
      _element = document.body;
      break;
  }

  var mc = new Hammer.Manager(_element);
  mc.add(new Hammer.Pan({
    threshold: 0, pointers: 0
  }));
  mc.add(new Hammer.Rotate({
    threshold: 0
  })).recognizeWith(mc.get('pan'));
  mc.add(new Hammer.Pinch({
    threshold: 0
  })).recognizeWith([mc.get('pan'), mc.get('rotate')]);

  // Pinch
  mc.on("pinchstart pinchmove", function (e) {
    event.stopPropagation();

    switch (_LayoutMode) {
      case 2: // 2D
        if (e.scale > 1) {
          DivPosition_Transform($("#div_background"), CONST_MOVEMENT_TYPE_ZOOM_MORE, CONST_MOVEMENT_MOUSE);
        } else {
          DivPosition_Transform($("#div_background"), CONST_MOVEMENT_TYPE_ZOOM_MINUS, CONST_MOVEMENT_MOUSE);
        }
        break;
    }
    return;
  });
}


function InitializeTopBarTime() {
  /// <summary>
  /// Initialize the Timer to refresh the time at the top bar.
  /// </summary>

  var _interval_delay = 1000;

  setInterval(function () {
    var _date;

    if (_Manager.WGDBDate == undefined) {
      var _current_date;

      if (m_wgdb_date == undefined) {
        m_wgdb_date = moment();
      }

      _current_date = moment(m_wgdb_date).add(1, 's');

      $("#txt_database_hora").text(moment(_current_date).locale(m_current_language).format('HH:mm'));
      $("#txt_database_fecha").text(moment(_current_date).locale(m_current_language).format('ddd DD/MM'));
    }
    else {
      m_wgdb_date = moment(_Manager.WGDBDate);

      $("#txt_database_hora").text(moment(m_wgdb_date).locale(m_current_language).format('HH:mm'));
      $("#txt_database_fecha").text(moment(m_wgdb_date).locale(m_current_language).format('ddd DD/MM'));

      _Manager.WGDBDate = undefined;
    }

    for (_idx_w_hr in widget_highrollers_list.Rows) {
      widget_highrollers_list.Rows[_idx_w_hr].timer += 1;
    }

    for (_idx_w_mpl in widget_players_max_level.Rows) {
      widget_players_max_level.Rows[_idx_w_mpl].timer += 1;
    }

  }, _interval_delay);


}

function SetMonitorSound() {
  m_monitor_notify_alarms = _Configuration.PlayAlarmSound;

  if (m_monitor_notify_alarms) {
    $(".btnAlarmaMonitor").removeClass("btnAlarmaMonitorOff");
  } else {
    $(".btnAlarmaMonitor").addClass("btnAlarmaMonitorOff");
  }
}


function PlayStopMonitorAlarmIcon() {
  m_monitor_notify_alarms = !m_monitor_notify_alarms;

  _Configuration.PlayAlarmSound = m_monitor_notify_alarms;
  m_config_has_changed = true;

  if (m_monitor_notify_alarms) {
    //$("#img_sound_image").attr("src", "design/iconos/alarms/soundOn.svg");
  } else {
    //$("#img_sound_image").attr("src", "design/iconos/alarms/soundOff.svg");
    var _alarm_sound = document.getElementById('sound_alarm');

    if (_alarm_sound) {
      _alarm_sound.pause();
      _alarm_sound.currenTime = 0;
    }
  }
}

var _UI = new WSI_ui();

//JBCDEMO
function PrintMyTasks() {

  $("#div_tasks").css("display", "true");
}

function onChange_sel_views() {
  var _control = document.getElementById("sel_views");

  _Configuration.DefaultView = _control.options[_control.selectedIndex].value;
  m_config_has_changed = true;
  SaveConfigurationDB(true);
}

function onZoomIn_2D() {
  DivPosition_Transform($("#div_background"), CONST_MOVEMENT_TYPE_ZOOM_MORE, CONST_MOVEMENT_MOUSE);
}

function onZoomOut_2D() {
  DivPosition_Transform($("#div_background"), CONST_MOVEMENT_TYPE_ZOOM_MINUS, CONST_MOVEMENT_MOUSE);
}

function onResetZoom_2D() {

  var _css_width = window.innerWidth;
  var _css_height = window.innerHeight;

  // Adjust location
  m_current_background_position_top = ((_css_height / +CONST_INITIAL_ZOOM) / 2) - (_CurrentFloor3.dimensions.y / 2);

  m_current_background_position_left = ((_css_width / +CONST_INITIAL_ZOOM) / 2) - (_CurrentFloor3.dimensions.x / 2);
  m_current_zoom = 0.6;

  // Initial position
  DivPosition_Move($("#div_background"), m_current_background_position_left, m_current_background_position_top, m_current_zoom, CONST_TRANSFORM_ORIGIN);

  // Zoom Out
  m_current_zoom = CONST_INITIAL_ZOOM;
  DivPosition_Move($("#div_background"), m_current_background_position_left, m_current_background_position_top, m_current_zoom, CONST_TRANSFORM_ORIGIN);
  UpdateSlider();
}

function CheckSoundsLoaded() {
  // Load sounds only the with the first interact by the user (For Android on Mobile browsers) 
  if (!_m_first_load) {
    $("#sound_alarm")[0].load();
    $("#sound_notify")[0].load();
    $("#sound_taskAlarm")[0].load();
    _m_first_load = true;
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
