﻿var _table_terminal_alarms = new TableListBase(".tablaAlarmasTerminales");
var m_aar_filter = "";

//************************************************************************************************************
//LISTA
//************************************************************************************************************
function WSIAlarmsAtRoom_Open() {

  if ($(".wrapJugadores.terminalAlarmsMode").children().length == 0) {

    $(".wrapJugadores").empty();
    $(".wrapJugadores").html(WSIAlarmsAtRoom_Init());
    $(".wrapJugadores").addClass("terminalAlarmsMode");

  }

  CreateTableTerminalsAlarms();
  WSIAlarmsAtRoom_Init_Events();
  m_aar_filter = "";
}

function WSIAlarmsAtRoom_Init() {
  var _result = '';

  _result += '<div class="headerTablas">';
  _result += '  <div class="cierreLista">';
  _result += '    <a href="#">Cerrar Lista</a>';
  _result += '  </div>';
  _result += '  <div class="terminalesSearchBox">';
  _result += '    <input class="srcTerminalPanel" id="inputTerminalAlarmasListasrc" type="text" placeholder="' + _Language.get('NLS_SEARCH_TERMINAL') + '" value="">';
  _result += '    <div class="btnBuscarTerminal" id="btnBuscarTerminalAlarmas">';
  _result += '      <a href="#">' + _Language.get('NLS_PLAYERS_SEARCH') + '</a>';
  _result += '    </div>';
  _result += '  </div>';
  _result += "  <div class='selectorCategoriaAlarmas'>"
  _result += "    <select class='selectCategoriaTerminal' id='selCategoryAlarms'>";
  _result += "      <option value='' selected>" + _Language.get('NLS_CATEGORY') + "</option>";
  _result += "      <option value='41'>" + _Language.get('NLS_PLAYER') + "</option>";
  _result += "      <option value='42'>" + _Language.get('NLS_MACHINE') + "</option>";
  _result += "    </select>";
  _result += "  </div>";
  _result += "  <div class='selectorSubcategoriaAlarmas'>"
  _result += "    <select class='selectSubcategoriaAlarmas' id='selSubcategoryAlarms'>";
  _result += "      <option value='' selected>" + _Language.get('NLS_SUBCATEGORY') + "</option>";

  for (_idx_aar_list_mach in _Legends.Dictionary_Alarms[41]) {
    _result += "      <option value='" + _idx_aar_list_mach + "'>" + _Legends.Dictionary_Alarms[41][_idx_aar_list_mach] + "</option>";
  }

  for (_idx_aar_list_play in _Legends.Dictionary_Alarms[42]) {
    _result += "      <option value='" + _idx_aar_list_play + "'>" + _Legends.Dictionary_Alarms[42][_idx_aar_list_play] + "</option>";
  }
  for (_idx_aar_list_play in _Legends.Dictionary_Alarms[43]) {
    _result += "      <option value='" + _idx_aar_list_play + "'>" + _Legends.Dictionary_Alarms[43][_idx_aar_list_play] + "</option>";
  }
  _result += "    </select>";
  _result += "  </div>";
  _result += "</div>";
  _result += '<div class="insideAlarmas">  ';
  _result += '  <div class="tablaAlarmasTerminales">';
  _result += '  </div>';
  _result += '</div>';

  return _result;
}

function WSIAlarmsAtRoom_Init_Events() {
  //Cierre lista jugadores
  $('.cierreLista a').click(function () {
    cerrarListaAlarmas();
    $(".wrapJugadores").removeClass("terminalAlarmsMode");
  });

  $("#btnBuscarTerminalAlarmas").click(function () {

    m_aar_filter = $("#inputTerminalAlarmasListasrc").val();

    WSIAlarmsAtRoom_GetFilters();
  });
}

function WSIAlarmsAtRoom_LoadData() {
  var _list = [];

  for (_idx_alarms_terminal in _Manager.Terminals.Items) {
    _terminal = _Manager.Terminals.Items[_idx_alarms_terminal];

    //SL 2/11 - RETIRED TERMINALS NOT SHOW IN ALARMS
    if (_terminal.Properties.status != 2 && (_terminal.Properties.alarms_machine_count > 0 || _terminal.Properties.alarms_player_count > 0)) {
      var terminalName = '';

      if (_terminal.Properties.name != undefined) {
        terminalName = _terminal.Properties.name.toUpperCase();
      }

      for (_idx_terminal_system_alarm_category in _terminal.Properties.list_system_alarms) {
        for (_idx_terminal_system_alarm_subcategory in _terminal.Properties.list_system_alarms[_idx_terminal_system_alarm_category]) {
          if (m_aar_filter == "" || terminalName.startsWith(m_aar_filter.toUpperCase())) {
            var _item = {
              terminal_id: _terminal.Properties.external_id,
              area_id: _terminal.Properties.area_id,
              category: _idx_terminal_system_alarm_category,
              provider_name: _terminal.Properties.provider_name,
              subcategory: _terminal.Properties.list_system_alarms[_idx_terminal_system_alarm_category][_idx_terminal_system_alarm_subcategory],
              terminal_name: _terminal.Properties.name,
              id: _terminal.Properties.external_id
            }

            _list.push(_item);
          }
        }//_idx_terminal_system_alarm_subcategory
      }//_idx_terminal_system_alarm_category
    }
  }//_idx_alarms_terminal

  return _list
}

function WSIAlarmsAtRoom_Refresh() {
  _table_terminal_alarms.rows = WSIAlarmsAtRoom_LoadData();
  _table_terminal_alarms.Refresh();
}

function CreateTableTerminalsAlarms() {
  //Field, IsPrimaryKey, Visible, Title, Width, Display, ColumnType, Sortable
  _table_terminal_alarms.AddColumn("id", true, false, "", "0%", "", "text", false);
  _table_terminal_alarms.AddColumn("terminal_id", false, true, "NLS_TERMINAL", "5%", WSIAlarmsAtRoom_PrintRowTerminalId, "text", true);
  _table_terminal_alarms.AddColumn("area_id", false, true, "NLS_AREA", "5%", "", "text", false);
  _table_terminal_alarms.AddColumn("provider_name", false, true, "NLS_PROVIDER", "5%", "", "text", false);
  _table_terminal_alarms.AddColumn("category", false, true, "NLS_CATEGORY", "5%", WSIAlarmsAtRoom_PrintRowCategory, "text", false);
  _table_terminal_alarms.AddColumn("subcategory", false, true, "NLS_SUBCATEGORY", "15%", WSIAlarmsAtRoom_PrintRowSubCategory, "text", true);
  _table_terminal_alarms.AddColumn("actions", false, true, "NLS_INFO", "3%", WSIAlarmsAtRoom_PrintRowInfo, "text", false);
  _table_terminal_alarms.AddColumn("Keeper", false, true, "NLS_KEEPER", "3%", WSIAlarmsAtRoom_PrintRowLocateCustomerAtRoom, "text", false);

  _table_terminal_alarms.rows = WSIAlarmsAtRoom_LoadData();
  _table_terminal_alarms.paging = true;
  _table_terminal_alarms.allowDelete = false;
  _table_terminal_alarms.sortValue = "terminal_id ASC";

  _table_terminal_alarms.CreateTable();
}

//ROW ALARMS Events**************
function WSIAlarmsAtRoom_PrintRowLocateCustomerAtRoom(data) {

  if (data.record._alarm_type != 43) {
    var $control = $("<a class='btnLocalizarAlarma' data-terminalid='" + data.record.terminal_id + "' href='#'>Localizar Alarma</a>");

    $control.click(function (evt) {
      _UI.SetMenuActiveSection($("#a_section_40")[0], 40);
      ShowTerminalInfo(_Manager.Terminals.Items[_Manager.Terminals.ByTerminal[$(evt.currentTarget).data("terminalid")]], 1, false);
    });

    return $control;
  }
}

function WSIAlarmsAtRoom_PrintRowTerminalId(data) {
  var _result = "";

  _result += data.record.terminal_name + " {ID:" + data.record.terminal_id + "}";

  return _result;
}

function WSIAlarmsAtRoom_PrintRowInfo(data) {
  var $item = $("<div data-terminalid='" + data.record.terminal_id + "' class='icoSnapshot' />")

  $item.click(function (evt) {
    var _terminal_id = _Manager.Terminals.Items[_Manager.Terminals.ByTerminal[$(evt.currentTarget).data("terminalid")]];

    m_terminal_id_selected = _terminal_id.Properties.lo_id;
    UpdatePopupTerminal(null, _terminal_id);
    OpenInfoPanel();
  })

  return $item;
}

function WSIAlarmsAtRoom_PrintRowCategory(data) {
  var _result = "";

  if (data.record.category == 41) {
    _result = _Language.get("NLS_PLAYER");
  } else if (data.record.category == 42) {
    _result = _Language.get("NLS_MACHINE");
  }
  else
    _result = "Custom";

  return _result;
}

function WSIAlarmsAtRoom_PrintRowSubCategory(data) {
  var _result = "";

  switch (data.record.subcategory) {
    case 16016:
    case 16032:
      _result = _Legends.Dictionary_Alarms["41"][data.record.subcategory];

      break;
    default:
      _result = _Legends.Dictionary_Alarms[data.record.category][data.record.subcategory];
      break;

  }


  return _result;
}

function WSIAlarmsAtRoom_GetFilters() {
  var _aar_filter_list = [];
  var _cb_alarm_category = document.getElementById("selCategoryAlarms");
  var _cb_alarm_subcategory = document.getElementById("selSubcategoryAlarms");
  var _alarm_category_value = _cb_alarm_category.options[_cb_alarm_category.selectedIndex].value;
  var _alarm_subcategory_value = _cb_alarm_subcategory.options[_cb_alarm_subcategory.selectedIndex].value;

  if (_alarm_category_value != "") {
    _aar_filter_list["category"] = _alarm_category_value;
  }

  if (_alarm_subcategory_value != "") {
    _aar_filter_list["subcategory"] = _alarm_subcategory_value;
  }

  _table_terminal_alarms.ApplyFilter(_aar_filter_list);

  WSIAlarmsAtRoom_Refresh();
}