﻿function WSIObjectProperties() {

  // Layout Object
  this.lo_id = null;
  this.lo_external_id = null;
  this.lo_type = null;
  this.lo_parent_id = null;
  this.lo_mesh_id = null;
  this.lo_creation_date = null;
  this.lo_update_date = null;
  // Layout Meshes
  this.lme_type = null;
  this.lme_sub_type = null;
  this.lme_geometry = null;
  // Layout Object Positions
  this.lop_x = null;
  this.lop_y = null;
  this.lop_z = null;
  this.lop_orientation = null;
  this.lop_floor_id = null;
  this.lop_area_id = null;
  this.lop_bank_id = null;
  this.lop_position_date = null;

  // Activity
  this.id = null; //lo_external_id
  this.status = null;
  this.play_session_status = null;
  this.is_anonymous = null;
  this.player_account_id = null;
  this.player_name = null;
  this.player_gender = null;
  this.player_age = null;
  this.player_level = null;
  this.player_is_vip = null;

  this.al_sas_host_error = null;
  this.al_door_flags = null;
  this.al_bill_flags = null;
  this.al_printer_flags = null;
  this.al_egm_flags = null;
  this.al_played_won_flags = null;
  this.al_jackpot_flags = null;
  this.al_stacker_status = null;
  this.al_call_attendant_flags = null;

  this.al_machine_flags = null;
  this.al_big_won = null;
  this.al_custom = null;
  this.played_amount = null;
  this.won_amount = null;
  this.play_session_bet_average = null;
  this.terminal_bet_average = null;
  this.play_session_duration = null;
  this.play_session_total_played = null;
  this.play_session_played_count = null;
  this.bank_id = null;
  this.area_id = null;
  this.provider_name = null;
  this.play_session_won_amount = null;
  this.net_win = null;
  this.play_session_net_win = null;
  this.name = null;
  this.played_count = null;
  this.player_trackdata = null;
  this.player_level_name = null;

  this.play_occupation_status = null;
  this.alarms = [];
  this.alarms_machine_count = 0;
  this.alarms_player_count = 0;
  this.denomination = null;
  this.list_system_alarms = []; //IDXbit 17001

  this.terminal_payout = null;
  this.theoretical_hold = null;

  this.player_birth_date = null;

  this.play_session_cash_in = null;
  this.play_session_cash_out = null;

}

WSIObjectProperties.prototype.Initialize = function (dataArray) {

  // Layout Object
  this.lo_id = dataArray[0];
  this.lo_external_id = dataArray[1];
  this.lo_type = dataArray[2];
  this.lo_parent_id = dataArray[3];
  this.lo_mesh_id = dataArray[4];

  this.lme_type = dataArray[5];
  this.lme_sub_type = dataArray[6];

  this.lo_creation_date = dataArray[7];
  this.lo_update_date = dataArray[8];

  // Layout Object Positions
  this.lop_x = dataArray[9];
  this.lop_y = dataArray[10];
  this.lop_z = dataArray[11];
  this.lop_orientation = dataArray[12];

  this.lop_floor_id = dataArray[13];
  this.lop_area_id = dataArray[14];
  this.lop_bank_id = dataArray[15];
  this.lop_position_date = dataArray[16];

  /*
  this.lo_object_id = dataArray[0];
  this.lo_external_id = dataArray[1]; // Terminal Id
  this.lo_object_type = dataArray[2];
  this.lme_type = dataArray[3];
  this.lme_sub_type = dataArray[4];
  this.lme_lod_level = dataArray[5];
  this.ll_type = dataArray[6];
  this.ll_default_text = dataArray[7];
  this.ll_color = dataArray[8];
  this.ll_font = dataArray[9];
  this.ll_font_size = dataArray[10];
  this.ll_font_color = dataArray[11];
  this.ll_x_size = dataArray[12];
  this.ll_y_size = dataArray[13];
  this.ll_z_size = dataArray[14];
  this.lme_voxel_size = dataArray[15];
  this.lme_show_label = dataArray[16];
  this.lme_show_statistics = dataArray[17];
  this.lme_show_client = dataArray[18];
  this.lme_label_position = dataArray[19];
  this.lme_statistics_position = dataArray[20];
  this.lme_client_position = dataArray[21];
  this.lme_label_lookat = dataArray[22];
  this.lo_creation_date = dataArray[23];
  this.lo_update_date = dataArray[24];
  this.lop_x = dataArray[25];
  this.lop_y = dataArray[26];
  this.lop_z = dataArray[27];
  this.lop_orientation = dataArray[28];
  this.lop_area_id = dataArray[29];
  this.lop_bank_id = dataArray[30];
  this.lop_position_date = dataArray[31];
  this.lme_geometry = dataArray[32];
  this.lma_material = dataArray[33];
  */
  return this;
}

WSIObjectProperties.prototype.SetActivityData = function (dataArray) {
  // this.lo_object_id = dataArray[0];
  var _alarms_machine = 0;
  var _alarms_player = 0;
  var _alarms_custom = 0;
  var _occupation_level = 0;

  this.list_system_alarms[41] = [];
  this.list_system_alarms[42] = [];
  this.list_system_alarms[43] = [];
  this.external_id = dataArray[1]; //lo_external_id
  this.status = dataArray[2];
  this.play_session_status = dataArray[3];
  this.is_anonymous = dataArray[4];
  this.player_account_id = dataArray[5];
  this.player_name = dataArray[6];
  this.player_gender = dataArray[7];
  //this.player_age = dataArray[8];
  this.player_level = dataArray[9];
  this.player_is_vip = dataArray[10];
  this.player_first_name = dataArray[48];
  this.player_last_name1 = dataArray[49];
  this.player_last_name2 = dataArray[50];
  //Alarms
  this.al_sas_host_error = dataArray[11];
  this.al_door_flags = dataArray[12];
  this.al_bill_flags = dataArray[13];
  this.al_printer_flags = dataArray[14];
  //this.al_egm_flags = dataArray[15];
  this.al_played_won_flags = dataArray[16];
  this.al_jackpot_flags = dataArray[17];
  this.al_stacker_status = dataArray[18];
  this.al_call_attendant_flags = dataArray[19];
  this.al_machine_flags = dataArray[20];
  this.al_big_won = dataArray[21].replace(",", ".");
  this.al_custom = 0;
  //------

  //Counting alarms
  _alarms_machine += this.GetSetBitCount(this.al_sas_host_error, 11, 42);
  _alarms_machine += this.GetSetBitCount(this.al_door_flags, 12, 42);
  _alarms_machine += this.GetSetBitCount(this.al_bill_flags, 13, 42);
  _alarms_machine += this.GetSetBitCount(this.al_printer_flags, 14, 42);
  //_alarms_machine += this.GetSetBitCount(this.al_egm_flags, 15, 42);
  _alarms_machine += this.GetSetBitCount(this.al_played_won_flags, 16, 42);
  _alarms_machine += this.GetSetBitCount(this.al_stacker_status, 18, 42);
  _alarms_machine += this.GetSetBitCount(this.al_machine_flags, 20, 42);

  _alarms_player += this.GetSetBitCount(this.al_jackpot_flags, 17, 41);
  _alarms_player += this.GetSetBitCount(this.al_call_attendant_flags, 19, 41);

  _alarms_custom += this.GetSetBitCount(this.al_custom, 99, 43);
  this.alarms_machine_count = _alarms_machine;
  this.alarms_player_count = _alarms_player;

  // Monetary data
  this.play_session_bet_average = Number(dataArray[24].replace(",", "."));
  this.terminal_bet_average = Number(dataArray[25].replace(",", "."));
  this.play_session_duration = dataArray[26].replace(",", ".");
  this.play_session_total_played = Number(dataArray[27].replace(",", "."));
  this.play_session_played_count = Number(dataArray[28].replace(",", "."));
  this.bank_id = dataArray[29];
  this.area_id = dataArray[30];
  this.provider_name = dataArray[31];
  this.play_session_won_amount = Number(dataArray[32].replace(",", "."));
  this.net_win = Number(dataArray[33].replace(",", "."));
  this.play_session_net_win = Number(dataArray[34].replace(",", "."));
  this.name = dataArray[35];

  this.played_count = Number(dataArray[36].replace(",", "."));
  this.played_amount = Number(dataArray[22].replace(",", "."));
  this.won_amount = Number(dataArray[23].replace(",", "."));

  this.terminal_payout = Number(dataArray[43].replace(",", "."));
  this.theoretical_hold = Number(dataArray[44].replace(",", "."));

  // Player
  this.player_trackdata = dataArray[37];
  this.player_level_name = dataArray[38];

  // Alarms
  if (dataArray[39] != '') {
    this.alarms = dataArray[39].split(",");
  }

  if (dataArray[40] != "") {
    this.denomination = parseFloat(dataArray[40]).toFixed(2);
  }

  this.created = parseInt(dataArray[41]);

  this.occupancy = 1;

  if (+this.player_level == 0 || +this.player_level == -1) {
    _occupation_level = 0;
  }
  else {
    _occupation_level = +this.player_level;
  }

  this.play_occupation_status = (+this.play_session_status + _occupation_level).toString();

  if (this.is_anonymous && this.is_anonymous == "1") {
    this.player_level_name = _Language.get("NLS_ANONYMOUS");
    this.occupancy = 2;
  } else {
    this.occupancy = this.play_occupation_status;
  }

  this.occupancy = new String(this.occupancy - 1);

  this.player_birth_date = dataArray[45];

  this.player_age = null;

  if (this.player_birth_date != "") {
    this.player_age = moment().diff(moment(this.player_birth_date, "MM-DD-YYYY hh:mm A"), 'years');
  } else if (this.play_session_status == "2") {
    this.player_age = "-1";
  }

  this.play_session_cash_in = Number(dataArray[46].replace(",", "."));
  this.play_session_cash_out = Number(dataArray[47].replace(",", "."));

  //

  this.UpdateNeeded = true;
}

//Return number of bits activated in Value
WSIObjectProperties.prototype.GetSetBitCount = function (Value, Index, Category) {
  var iCount = 0;
  var _idx_bit = 1;
  var lValue = parseInt(Value);

  if (lValue != 0) {
    while (_idx_bit <= 256) {
      if ((lValue & _idx_bit) == _idx_bit) {
        this.list_system_alarms[Category].push((Index * 1000) + _idx_bit);

        iCount++;
      }
      _idx_bit = _idx_bit * 2;
    }
  }

  ////Loop the value while there are still bits
  //while (lValue != 0) {
  //  //Remove the end bit
  //  lValue = lValue & (lValue - 1);

  //  //Increment the count
  //  iCount++;
  //}

  //Return the count
  return iCount;
}

WSIObjectProperties.prototype.Initialize2 = function (dataArray, Location, Dimensions) {

  //var EN_OBJECT_LOCATION_FIELDS = {
  //  lo_id: 0,
  //  lo_external_id: 1,
  //  lo_type: 2,
  //  lo_parent_id: 3,
  //  lo_mesh_id: 4,
  //  lo_creation_date: 5,
  //  lo_update_date: 6,
  //  lo_status: 7,
  //  lol_object_id: 8,
  //  lol_location_id: 9,
  //  lol_date_from: 10,
  //  lol_date_to: 11,
  //  lol_orientation: 12,
  //  lol_area_id: 13,
  //  lol_bank_id: 14,
  //  lol_bank_location: 15,
  //  lol_current_location: 16,
  //  lol_offset_x: 17,
  //  lol_offset_y: 18,
  //  //
  //  svg_id: 19
  //}

  //var EN_LOCATION_FIELDS = {
  //  loc_id: 0,
  //  loc_x: 1,
  //  loc_y: 2,
  //  loc_creation: 3,
  //  loc_type: 4,
  //  loc_floorId: 5,
  //  loc_gridId: 6
  //}


  // Layout Object
  this.lo_id = dataArray[EN_OBJECT_LOCATION_FIELDS.lo_id];
  this.lo_external_id = dataArray[EN_OBJECT_LOCATION_FIELDS.lo_external_id];
  this.lo_type = dataArray[EN_OBJECT_LOCATION_FIELDS.lo_type];
  this.lo_parent_id = dataArray[EN_OBJECT_LOCATION_FIELDS.lo_parent_id];
  this.lo_mesh_id = dataArray[EN_OBJECT_LOCATION_FIELDS.lo_mesh_id];

  //this.lme_type = dataArray[5];
  //this.lme_sub_type = dataArray[6];

  this.lo_creation_date = dataArray[EN_OBJECT_LOCATION_FIELDS.lo_creation_date];
  this.lo_update_date = dataArray[EN_OBJECT_LOCATION_FIELDS.lo_upadte_date];

  // Layout Object Positions

  // RMS - Center positions by dimensions
  var _fx = -Math.floor(PixelsToUnits(Dimensions.x) / 2) * 4,
      _fy = -Math.floor(PixelsToUnits(Dimensions.y) / 2) * 4;

  this.lop_x = _fx + (+Location[EN_LOCATION_FIELDS.loc_x] * 4);
  this.lop_y = 0;
  this.lop_z = _fy + (+Location[EN_LOCATION_FIELDS.loc_y] * 4);

  this.lop_orientation = dataArray[EN_OBJECT_LOCATION_FIELDS.lol_orientation];

  this.lop_floor_id = Location[EN_LOCATION_FIELDS.loc_floorId];
  this.lop_area_id = dataArray[EN_OBJECT_LOCATION_FIELDS.lol_area_id];
  this.lop_bank_id = dataArray[EN_OBJECT_LOCATION_FIELDS.lol_bank_id];
  this.lop_position_date = dataArray[EN_OBJECT_LOCATION_FIELDS.lol_date_from];

  /*
  this.lo_object_id = dataArray[0];
  this.lo_external_id = dataArray[1]; // Terminal Id
  this.lo_object_type = dataArray[2];
  this.lme_type = dataArray[3];
  this.lme_sub_type = dataArray[4];
  this.lme_lod_level = dataArray[5];
  this.ll_type = dataArray[6];
  this.ll_default_text = dataArray[7];
  this.ll_color = dataArray[8];
  this.ll_font = dataArray[9];
  this.ll_font_size = dataArray[10];
  this.ll_font_color = dataArray[11];
  this.ll_x_size = dataArray[12];
  this.ll_y_size = dataArray[13];
  this.ll_z_size = dataArray[14];
  this.lme_voxel_size = dataArray[15];
  this.lme_show_label = dataArray[16];
  this.lme_show_statistics = dataArray[17];
  this.lme_show_client = dataArray[18];
  this.lme_label_position = dataArray[19];
  this.lme_statistics_position = dataArray[20];
  this.lme_client_position = dataArray[21];
  this.lme_label_lookat = dataArray[22];
  this.lo_creation_date = dataArray[23];
  this.lo_update_date = dataArray[24];
  this.lop_x = dataArray[25];
  this.lop_y = dataArray[26];
  this.lop_z = dataArray[27];
  this.lop_orientation = dataArray[28];
  this.lop_area_id = dataArray[29];
  this.lop_bank_id = dataArray[30];
  this.lop_position_date = dataArray[31];
  this.lme_geometry = dataArray[32];
  this.lma_material = dataArray[33];
  */
  return this;
}
