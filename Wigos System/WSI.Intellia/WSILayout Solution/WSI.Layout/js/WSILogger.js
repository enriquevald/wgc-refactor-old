﻿// ---------------------------------------------------------------------

// Object to console log and debug
var WSILogger = {
  enable: true,
  debug: true,
  Log: function (FunctionName, Error) {
    if (typeof console != "undefined" && WSILogger.enable) {
      console.log(FunctionName + " -> " + Error.message);
    }
  },
  Write: function (Message) {
    if (typeof console != "undefined" && WSILogger.enable) {
      console.log(new Date().toGMTString() + ': ' + Message);
    }
  },
  Debug: function (FunctionName, VarName, Var) {
    
      if (typeof console != "undefined" && WSILogger.enable && WSILogger.debug) {
        console.log(new Date().toGMTString() + ': ' + ">>> DEBUG: Function: " + FunctionName + " >>> Variable: " + VarName + " >>>");
        console.log(Var);
        console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
      }
    
  }
};

// ---------------------------------------------------------------------
