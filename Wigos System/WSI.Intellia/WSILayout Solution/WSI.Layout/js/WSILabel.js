﻿function WSILabel() {

    this.BackColor = null;
    this.TextColor = null;
    this.BorderColor = null;
    this.Text = null;
    this.ParentId = null;
    this.ParentType = null;
    this.Position = null;
    this.Mesh = null;                   //Label
    this.hasError = false;              //unused
    this.Stats = null;                  //FloorLabel
    this.Icon = null;

    this.IsVip = false;
    this.HaveJackpot = false;

    this.view = 0;
    this.viewtime = new Date().getTime();

    this.rottime = new Date().getTime();

    this.Items = [];

}

WSILabel.prototype.Update = function (Terminal, activeFilter, activeAlarms) {

    this.ParentId = Terminal.lo_external_id;
    this.ParentType = 1; // Terminal

    // default view
    this.BackColor = "white";
    this.TextColor = "black";
    this.BorderColor = "black";

    //
    this.Items = [];

    if (m_current_section == Enum.EN_SECTION_ID.ALARMS || m_current_section == Enum.EN_SECTION_ID.MONITOR) {
        var _legend_values = undefined;
        activeFilter = m_current_filter[m_current_section];
        if (m_current_section == Enum.EN_SECTION_ID.ALARMS) {
            // Alarms special case
            if (activeAlarms.length == 0) { return; }
            _legend_values = _Legends.CheckAlarms(Terminal, activeAlarms);
        }
        else {
          if (!activeFilter) { return; }
          _legend_values = _Legends.CheckCustomFilter(activeFilter, Terminal);
        }
    } else {
        // Default flow
        if (!activeFilter) { return; }
        var _legend_values = _Legends.Check(activeFilter, Terminal);
    }

    if (_legend_values != null) {
        this.BackColor = _legend_values.color;
        this.Text = (activeFilter == 'By Use') ? ("NetWin " + Math.round((+Terminal.played_amount - +Terminal.won_amount) * 100) / 100) : _legend_values.name;
        this.Icon = _legend_values.icon;
    }
}

/////////////////////////////////////////////////////////////////////////////

function WSILabels() {

    this.Count = 0;
    this.Items = [];

    if (_LayoutMode == 1) {
        this.Pivot = new THREE.Object3D();
        this.Pivot.visible = true;

        this.FloorPivot = new THREE.Object3D();
        this.FloorPivot.visible = true;
    }

}

WSILabels.prototype.SetVisible = function (value) {
    if (this.Pivot) {

        this.Pivot.visible = value;

        for (_i in this.Pivot.children) {
            var _tvalue = value;
            var _l = this.Pivot.children[_i].name;
            _l = _l.replace("_label", "");
            _l = _l.replace("_line", "");
            _l = +_l;

            this.Pivot.children[_i].visible = value;
        }
    }
}

WSILabels.prototype.Add = function (parentObjectId, labelObject) {
    if (this.Items[parentObjectId]) {
        this.Count++;
    }

    this.Items[parentObjectId] = labelObject;
}

WSILabels.prototype.Refresh = function () {
    for (_i in this.Items) {
        var _L = this.Items[_i];
        var _ct = new Date().getTime();
        if (_L) {
            //
            if (_L.IsVip && _L.view != 1 && (_ct - _L.viewtime) > 1000) {
                _L.view = 1;
                _L.viewtime = new Date().getTime();
                //
                if (_LayoutMode == 1) {
                    // 3D
                    var _mesh = _Manager.Labels.Pivot.getObjectByName(_i + "_label_pyramid", true);
                    if (_mesh) {
                        var _old_alarm = _Manager.Labels.Pivot.getObjectByName(_i + "_label_alarm", true);
                        if (_old_alarm) {
                            _old_alarm.material.map = _Manager.Materials.Textures['vip_pic'];
                        }
                        var _mesh = _Manager.Labels.Pivot.getObjectByName(_i + "_label_pyramid", true);
                        if (_mesh) {
                            WSIObjectUtils.ChangeMaterialColor(_mesh, "_mat_body", 0xffff00);
                        }
                    }
                } else {
                    // 2D
                    $("#" + CONST_ID_TERMINAL + _i).css("background-image", 'url(images/vip.png)')

                } // End If Other VIP
            } else {
                if (_L.HaveJackpot && _L.view != 2 && (_ct - _L.viewtime) > 1000) {
                    _L.view = 2;
                    _L.viewtime = new Date().getTime();
                    //
                    if (_LayoutMode == 1) {
                        // 3D
                        var _mesh = _Manager.Labels.Pivot.getObjectByName(_i + "_label_pyramid", true);
                        if (_mesh) {
                            var _old_alarm = _Manager.Labels.Pivot.getObjectByName(_i + "_label_alarm", true);
                            if (_old_alarm) {
                                _old_alarm.material.map = _Manager.Materials.Textures['jackpot_pic'];
                            }
                            var _mesh = _Manager.Labels.Pivot.getObjectByName(_i + "_label_pyramid", true);
                            if (_mesh) {
                                WSIObjectUtils.ChangeMaterialColor(_mesh, "_mat_body", 0xff0080);
                            }
                        }
                    } else {
                        // 2D
                        $("#" + CONST_ID_TERMINAL + _i).css("background-image", 'url(images/jackpot.png)')

                    } // End If Jackpot
                    //
                } else {
                    if (_L.Items.length > 0 && _L.view != 3 && (_ct - _L.viewtime) > 1000) {
                        _L.view = 3;
                        _L.viewtime = new Date().getTime();
                        //
                        if (_LayoutMode == 1) {
                            // 3D
                            var _mesh = _Manager.Labels.Pivot.getObjectByName(_i + "_label_pyramid", true);
                            if (_mesh) {
                                var _old_alarm = _Manager.Labels.Pivot.getObjectByName(_i + "_label_alarm", true);
                                if (_old_alarm) {
                                    _old_alarm.material.map = _Manager.Materials.Textures['alarm_pic'];
                                }
                                var _mesh = _Manager.Labels.Pivot.getObjectByName(_i + "_label_pyramid", true);
                                if (_mesh) {
                                    WSIObjectUtils.ChangeMaterialColor(_mesh, "_mat_body", 0xff0000);
                                }
                            }
                        } else {
                            // 2D
                            $("#" + CONST_ID_TERMINAL + _i).css("background-image", 'url(img/alarm.png)')

                        } // End If Other Alarms
                    }
                }
            }
        }
    }
}