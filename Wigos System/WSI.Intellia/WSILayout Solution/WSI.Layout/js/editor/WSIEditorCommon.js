﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: filename
// 
//   DESCRIPTION: Description.
// 
//        AUTHOR: Author
// 
// CREATION DATE: Date
// 
//------------------------------------------------------------------------------

/*
 Requires:

 */

//var m_floor_metrics = ['m', 'mi', 'ft'];

var EN_LAYOUT_OBJECT_TYPES = {
  LOT_SYSTEM: 0,
  LOT_TERMINAL: 1,
  LOT_GAMING_TABLE: 2,
  LOT_GAMING_TABLE_SEAT: 3,

  LOT_3D_CAMERA: 20,

  LOT_BEACON: 70,

  LOT_SMARTPHONE: 100,

  LOT_PLAYER_MALE: 200,
  LOT_PLAYER_FEMALE: 201
}

var EN_OBJECT_STATUS = {
  inserted: 1,
  deleted: 2
}

var EN_OBJECT_LOCATION_FIELDS = {
  lo_id: 0,
  lo_external_id: 1,
  lo_type: 2,
  lo_parent_id: 3,
  lo_mesh_id: 4,
  lo_creation_date: 5,
  lo_update_date: 6,
  lo_status: 7,
  lol_object_id: 8,
  lol_location_id: 9,
  lol_date_from: 10,
  lol_date_to: 11,
  lol_orientation: 12,
  lol_area_id: 13,
  lol_bank_id: 14,
  lol_bank_location: 15,
  lol_current_location: 16,
  lol_offset_x: 17,
  lol_offset_y: 18,
  //
  svg_id: 19
}

var EN_LOCATION_FIELDS = {
  loc_id: 0,
  loc_x: 1,
  loc_y: 2,
  loc_creation: 3,
  loc_type: 4,
  loc_floorId: 5,
  loc_gridId: 6
}

var m_click_mouse_location_x, m_click_mouse_location_y;

function RenameLocation() {
  //var _loc_x = _MousePos.location.x;
  //var _loc_y = _MousePos.location.y;
  var _loc_x = m_click_mouse_location_x;
  var _loc_y = m_click_mouse_location_y;
  var _location = _CurrentFloor3.GetLocation(_loc_x, _loc_y);
  var _old_value = _location[EN_LOCATION_FIELDS.loc_gridId];

  // TODO: Check if location not exists
  var dialog = $('Nombre: <input type="text" value="' + _location[EN_LOCATION_FIELDS.loc_gridId] + '" />').dialog({
    title: 'Localización (' + _location[EN_LOCATION_FIELDS.loc_x] + '-' + _location[EN_LOCATION_FIELDS.loc_y] + ')',
    buttons: {
      "Accept": function (evt) {
        var _value = $(dialog).val();

        //Comprobar cambio
        if (_value != _old_value) {

          //Guardar en BD
          PageMethods.UpdateLocationName(+_location[0], _value, function (result) {
            if (result) {
              //Modificar en _CurrentFloor
              _CurrentFloor3.UpdateLocationName(_location[EN_LOCATION_FIELDS.loc_id], _value);

            } else {

              var dialog = $('<p>Error al guardar.</p>').dialog({
                buttons: {
                  "Ok": function () { dialog.dialog("destroy"); },
                }
              });
            }

          }, OnSaveLocationError);
        }

        dialog.dialog("destroy");
      },
      "Cancel": function () { dialog.dialog("destroy"); }
    }
  });
}

function OnSaveLocationError() {

}

function UpdateContextPanels() {
  var _mode = window.methodDraw.canvas.getMode();

  $("#terminals_panel").hide();
  $("#bank_panel").hide();
  $("#mode_edit_panel").hide();
  $("#mode_beacons_panel").hide();

  switch (_mode) {
    case "Islands":
      $("#bank_panel").show();
      break;
    case "Machine":
      $("#terminal_panel").show();
      $("#terminals_panel").show();
      break;
    case "Edit":
      $("#mode_edit_panel").show();
      break;
    case "Beacons":
      $("#mode_beacons_panel").show();
      break;
    default:
      // Other modes
      break;
  }

}

//---------------------------------------------------------------------
