﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: filename
// 
//   DESCRIPTION: Description.
// 
//        AUTHOR: Author
// 
// CREATION DATE: Date
// 
//------------------------------------------------------------------------------

/*
 Requires:

 */

/*
LOP_FLOOR_ID	
LO_ID	
OBJ_TYPE	
OBJ_IDENTITY	
OBJ_NAME	
OBJ_BANK	
BK_NAME	
LO_TYPE	
LOP_X	
LOP_Y	
LOP_ORIENTATION
*/
function WSIAvailableObject() {
  this.floor = "";
  this.id = "";
  this.type = "";
  this.identity = "";
  this.name = "";
  this.bank = "";
  this.bank_name = "";
}

// Returns if the object of Type and Indentity is placed in the current not saved plane
WSIAvailableObject.prototype.ExistsInMemory = function () {
  var _result = false;

  _result = (_CurrentChanges3.GetByField(EN_OBJECT_LOCATION_FIELDS.lo_external_id, this.identity, EN_OBJECT_STATUS.deleted) != undefined);

  if (!_result) {
    _result = _CurrentFloor3.GetByField(EN_OBJECT_LOCATION_FIELDS.lo_external_id, this.identity, EN_OBJECT_STATUS.deleted) != undefined
  }

  return _result;

  //return (_CurrentChanges3.GetByField(EN_OBJECT_LOCATION_FIELDS.lo_external_id, this.identity) != undefined);
  //var _search = $('[lo_type="' + this.type + '"][current_lo_externalId="' + this.identity + '"]');
  //if (_search.length != 0) {
  //  return _search.attr("current_lo_externalId");
  //}
  //this.id = "";
  //return false;
}

WSIAvailableObject.prototype.Load = function (AvailableObjectData) {
  this.floor = AvailableObjectData[0];
  this.id = AvailableObjectData[1];
  this.type = AvailableObjectData[2];
  this.identity = AvailableObjectData[3];
  this.name = AvailableObjectData[4];
  this.bank = AvailableObjectData[5];
  this.bank_name = AvailableObjectData[6];

  if (_CurrentFloor3) {
    var _in_mem_id = this.ExistsInMemory();
    if (_in_mem_id) { this.id = _in_mem_id }

  }
}

//------------------------------------------------------------------------------

function WSIAvailableObjects() {
  this.Items = [];
}

WSIAvailableObjects.prototype.AddItem = function (NewItem) {
  this.Items[NewItem.identity] = NewItem;
}

// Returns if the object of Type and Indentity is placed in the current not saved plane
WSIAvailableObjects.prototype.ExistsInMemory = function (Indentity) {
  var _result = false;

  _result = (_CurrentChanges3.GetByField(EN_OBJECT_LOCATION_FIELDS.lo_external_id, this.identity) != undefined);

  if (!_result) {
    _result = _CurrentFloor3.GetByField(EN_OBJECT_LOCATION_FIELDS.lo_external_id, this.identity) != undefined
  }

  return _result;

  //return (_CurrentChanges3.GetByField(EN_OBJECT_LOCATION_FIELDS.lo_external_id, Identity) != undefined);
  //var _search = $('[lo_type="' + Type + '"][current_lo_externalId="' + Identity + '"]');
  //if (_search.length != 0) {
  //  return _search.attr("current_lo_externalId");
  //}
  //return false;
}

WSIAvailableObjects.prototype.ParseItems = function (Objects) {
  var _obj = undefined;

  for (var _i in Objects.rows) {
    _obj = new WSIAvailableObject();
    _obj.Load(Objects.rows[_i]);
    this.AddItem(_obj);
  }

}

WSIAvailableObjects.prototype.Update = function () {
  var _in_mem = undefined;
  for (var _i in this.Items) {
    _in_mem = this.Items[_i].ExistsInMemory();
    this.Items[_i].id = (_in_mem) ? _in_mem : "";
  }
}

WSIAvailableObjects.prototype.Load = function () {

  LayoutProgressOpen();

  var _that = this;

  var OnAvailableObjectsComplete = function (Result) {
    if (Result != "") {

      // Parse data
      var _objects = JSON.parse(Result);

      // Create items
      _that.ParseItems(_objects);

      LayoutProgressClose();

    } else {
      // Nothing available
    }
  };

  var OnAvailableObjectsError = function () {
    LayoutProgressClose();

    // TODO: Error report
  }

  PageMethods.GetEditorAvailableObjects(OnAvailableObjectsComplete, OnAvailableObjectsError);
}

WSIAvailableObjects.prototype.GetListSelectedItem = function (Container) {
  var _cat = Container.attr('selected_category'),
      _ite = Container.attr('selected_item');
  
  if (_cat != "" && _ite != "") {
    return { category: _cat, item: this.Items[_ite] };
  } else {
    return undefined;
  }
}

WSIAvailableObjects.prototype.UpdateList = function (Container) {

  var _available_types = JSON.parse(Container.attr("data-property-types"));
  var _items_ico_class = Container.attr("data-property-icos");

  function GenerateTab(ListId, CategoryId, Index) {

    var _string_tab_list_item = '';
    var _tab_name = Index;
    var _selectable_id = '';

    switch (+Index) {
      case 1:
        _tab_name = "Terminales";
        break;
      case 2:
        _tab_name = "Mesas de Juego";
        break;
      case 70:
        _tab_name = "Beacons";
        break;
    }

    _string_tab_list_item = _string_tab_list_item + '<li><a href="#' + CategoryId + Index + '" data-id="' + Index + '">' + _tab_name + '</a></li>';

    $("#" + ListId).append(_string_tab_list_item);

    _selectable_id = CategoryId + Index + '_selectables';

    var _string_tab = '';
    _string_tab = _string_tab + '<div id="' + CategoryId + Index + '" class="editor_tab_content" data-id="' + Index + '">';
    _string_tab = _string_tab + '<ol id="' + _selectable_id + '" class="editor_bind_select_list selectable"></ol>';
    _string_tab = _string_tab + '</div>';

    Container.append(_string_tab);

    return _selectable_id;
  }

  function GenerateItems(Container) {

    var _list_id = Container.attr("id") + '_list';
    var _categories_id = Container.attr("id") + '_categories-';
    var _selectable_id = undefined;

    //var _movable_class = "beacons_movable";

    Container.off("tabsactivate");
    Container.empty();
    try { Container.tabs("destroy"); } catch (_ex) { }
    Container.append('<ul id="' + _list_id + '"></ul>');

    var _n_objects = m_available_objects.Items.length,
        _item = undefined,
        _category = undefined,
        _first = undefined;

    for (var _first in m_available_objects.Items) break;
    Container.attr('selected_category', (_n_objects > 0) ? m_available_objects.Items[_first].type : '');

    for (var _i in m_available_objects.Items) {

      _item = m_available_objects.Items[_i];

      if (_available_types.types.indexOf(+_item.type) < 0) { continue; }

      if (_item.ExistsInMemory() != false) { continue; }

      if (_category != _item.type) {
        _category = _item.type;
        _selectable_id = GenerateTab(_list_id, _categories_id, _category);
      }

      $("#" + _selectable_id).append('<li class="editor_bind_item ui-widget-content" data-id="' + _item.identity + '"><div class="movable ' + _items_ico_class + '"></div>' + _item.name + '</li>');

    }

    $(".selectable").selectable({
      selected: function (event, ui) {
        $(ui.selected).not(".terminal_movable").addClass("ui-selected").siblings().removeClass("ui-selected");
        Container.attr('selected_item', $(ui.selected).data("id"));
      }
    });

    $(".movable").draggable({
      appendTo: 'body',
      helper: 'clone',
      cursorAt: { left: -1, top: -1 },
      zIndex: 3000,
      start: function (evt, ui) {
        $(ui.helper.prevObject).parent().siblings().removeClass("ui-selected");
        $(ui.helper.prevObject).parent().addClass("ui-selected");
        Container.attr('selected_item', $(ui.helper.prevObject).parent().data("id"));
      }
    });
    
    Container.attr('selected_item', '');

    Container.tabs().on("tabsactivate", function (event, ui) {
      Container.attr('selected_item', '');
      Container.attr('selected_category', ui.newPanel.data('id'));
    });

  }

  GenerateItems(Container);

}


var m_available_objects = new WSIAvailableObjects();
