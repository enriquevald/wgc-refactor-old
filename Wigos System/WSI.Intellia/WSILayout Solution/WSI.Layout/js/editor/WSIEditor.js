﻿////////////////////////////////////////////////////////////////////////////////////////

function FloorOpenedOrCreated() {

  // Update the open dialog
  if (m_dlg_open_floor && m_dlg_open_floor.dialog("isOpen")) {
    //m_dlg_open_floor.dialog("Destroy");
    LayoutOpenFloorOpen();
  }

  if (_CurrentFloor3) {
    // Update current floor menu label
    $("#current_floor_name").text(_CurrentFloor3.name);
  }

  // Obtain/Update the list of available objects
  m_available_objects.Update();

}
