﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: filename
// 
//   DESCRIPTION: Description.
// 
//        AUTHOR: Author
// 
// CREATION DATE: Date
// 
//------------------------------------------------------------------------------

/*
 Requires:
 
 */


function WSILanguage() {

  this.CurrentLanguage = 0;

  this.DefaultLanguage = 1;

  this.Languages = [{
    id: "ES",
    flag: "images/flag_spain.png",
    nls: "NLS_SPANISH"
  },
  {
    id: "EN",
    flag: "images/flag_united_kingdom.png",
    nls: "NLS_ENGLISH"
  }];

  this.Values = [];

  // Languages
  this.Values["ES"] = [];
  this.Values["EN"] = [];

  /***************************************** NLS DEFINITION *****************************************/

  // Spanish
  this.Values["ES"]["NLS_FLOOR"] = "Piso";

  // ------------------------- //

  // English
  this.Values["EN"]["NLS_FLOOR"] = "Floor";

  // ----------------------------------------------------------------------------------------- //


}

WSILanguage.prototype.get = function (NLS_ID, Params) {
  var _result = this.Values[this.Languages[this.CurrentLanguage].id][NLS_ID]
  if (_result) {
    if (Params) {
      for (var _i = 0; _i < Params.length; _i++) {
        _result = _result.replace("%" + _i, Params[_i]);
      }
    }
  } else {
    _result = "";
  }
  return _result;
}

WSILanguage.prototype.parse = function (element) {

  if (!element) { element = document.body; }

  var childNodes = element.childNodes;

  for (var i = 0; i < childNodes.length; i++) {

    if (childNodes[i].attributes != null) {
      var _nls = childNodes[i].getAttribute("data-nls");
    }

    if (_nls) {
      _nls = _nls.trim();

      if (_nls != "") {

        if (this.Values[this.Languages[this.CurrentLanguage].id][_nls]) {
          if ($(childNodes[i]).prop("tagName") == "INPUT") {
            $(childNodes[i]).attr("placeholder", this.Values[this.Languages[this.CurrentLanguage].id][_nls]);
          } else if ($(childNodes[i]).attr("placeholder")) {
            $(childNodes[i]).attr("placeholder", this.Values[this.Languages[this.CurrentLanguage].id][_nls]);
          } else {
            $(childNodes[i]).text(this.Values[this.Languages[this.CurrentLanguage].id][_nls]);
          }
        }

      }

    }

    if (childNodes[i].childNodes) {
      if (childNodes[i].childNodes.length > 0) {
        this.parse(childNodes[i]);
      }
    }
  }

}

WSILanguage.prototype.createSelector = function (id, onChange) {
  var _result = '';
  var _lng = '';

  _result += '<select id="' + id + '" style="width:150px;" onchange="' + onChange + '">';
  for (_lng in this.Languages) {
    _result += ' <option value="' + _lng + '" ';
    if (_lng == 0) { _result += 'selected="selected" '; }
    _result += 'data-image="' + this.Languages[_lng].flag + '" data-nls="' + this.Languages[_lng].nls + '"></option>';
  }
  _result += '</select>';

  return _result;
}

WSILanguage.prototype.getLanguageIdByName = function (name) {
  var _result = -1;
  var _lng = 0;
  for (_lng in this.Languages) {
    if (name == this.Languages[_lng].id) {
      _result = _lng;
      break;
    }
  }
  if (_result == -1) { _result = this.DefaultLanguage; }
  return _result;
}

//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////

var _Language = new WSILanguage();

//////////////////////////////////////////////////////////////////////

