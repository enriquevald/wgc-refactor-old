﻿var logged_user_mqtt;
var MQTTclient;
var m_messages_panel = [];
function WSIMQTTclient(userId) {
  logged_user_mqtt = userId

}

WSIMQTTclient.prototype.init = function () {

  WSILogger.Write("Initializing MQTT Client...");

  // Create a client instance
  MQTTclient = $.connection.chatHub;
  if (typeof (MQTTclient) !== "undefined" && MQTTclient !== null) {
    // set callback handlers
    MQTTclient.client.broadcastMessage = onMessageArrived;

    $.connection.hub.start();

    PageMethods.InitMqttClient(m_current_user_id, OnInitMqttClientOnSuccess, OnInitMqttClientOnError);

  }
}

function OnInitMqttClientOnSuccess() {
  WSILogger.Write("Client Initialization Success");
}

function OnInitMqttClientOnError(error) {
  WSILogger.Debug("OnInitMqttClientOnError()", "ERROR", error);
}
// called when a message arrives
function onMessageArrived(messageData) {

  if (messageData.managerId == m_current_user_id) {
    var runnerId = messageData.runnerId;
    var managerId = messageData.managerId;
    var runnerUserName = messageData.runnerUserName; //_Manager.TaskManager.Users[messageData.runnerId];
    var messageContent = messageData.message;

    m_messages_panel.push(runnerId);

    var isChatPanelDisplayed = $(".panelEnviarMensaje").css("display") != "none" && $(".slideDerechaExtendido").css("display") != "none";
    if (isChatPanelDisplayed) {
      if (runnerId != $(".contactSeleccionado").attr('data-userKey'))
        WSI_message_Refresh(_Manager.TaskManager.Users);
      else {

       var _MessageManager = MessageManager.getInstance();
        _MessageManager.ReloadConversation(runnerId, runnerUserName);

        $(".conversation").animate({ scrollTop: $('.conversation').prop("scrollHeight") }, 1000);
      }
    } else {
      if ($(".toast8-title:contains('" + runnerUserName +  "')").length == 0) {
        toastr8.info({
          message: messageContent,
          title: runnerUserName,
          timeOut: 0,
          showDuration: 300,
          onclick: function () {
            animaPanelDerechaExtendido();
            var _MessageManager = MessageManager.getInstance();
            _MessageManager.InitConversation(runnerId, runnerUserName);
            if ($(".toast8-title").length > 0) {
              toastr8.clear();
            }
          },
          //iconClass: "fa fa-calendar",
          imgURI: '../design/iconos/ic_message_black_48dp_2x.png'

        });
      }
      if (_Configuration.SoundEnabled)
      {
        $("#sound_notify")[0].load();
        $("#sound_notify")[0].play();
      } else {
        $("#sound_notify")[0].pause();
        if ($("#sound_notify")[0].readyState > 0) {
          $("#sound_notify")[0].currentTime = 0;
        }
      }

      if ($(".badgeMessage").length) {
        if ($('.badgeMessage').is(':hidden')) {
          $('.badgeMessage').show();
          $(".badgeMessage").text(1);
        }
        else {
          var _msgs = $(".badgeMessage").html();
          _msgs++
          $(".badgeMessage").text(_msgs);
        }
      }
      else
        $("#message").prepend('<span class="badgeMessage severidadMessages">1</span>');

      var _left = $("#message").width() / 2 - 10;
      var _top = ($("#message").height() / 2) - 5;
      $(".badgeMessage").css({ 'position': 'absolute', 'left': _left, 'top': _top })

     // if (m_messages_panel.indexOf(runnerId)==-1)
        

    }
    WSILogger.Debug("onMessageArrived: " + messageContent);
  }
}

function WSI_message_Refresh(List)
{
  for (_idx_user_names in List) {
    if (_idx_user_names != m_current_user_id) {
      if (m_messages_panel.indexOf(_idx_user_names) != -1)
     {
        var _msg = 0;
        for (var i = 0; i < m_messages_panel.length ; i++) {
          if (m_messages_panel[i] == _idx_user_names) {
            _msg++;
          }
        }

        $("#_id" + _idx_user_names).html("<span>" + List[_idx_user_names] + "</span><span class='badgeMessage severidadMessages' id='div_" + _idx_user_names + "'>" + _msg + "</span>");

       // _result += "<li class='rolAcordeon' data-userKey='" + _idx_user_names + "' ><span>" + List[_idx_user_names] + "</span><span class='badgeMessage severidadMessages' id='div_" + _idx_user_names + "'>" + _msg + "</span></li>";
      }
    }
  }
}



