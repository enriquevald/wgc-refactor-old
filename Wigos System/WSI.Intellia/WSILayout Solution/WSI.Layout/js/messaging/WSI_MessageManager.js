﻿var MessageManager = (function () {

    // Instance stores a reference to the Singleton
    var instance;

    function init() {

        // Singleton

        // Private methods and variables
        function WSI_Messages_AddContacts_to_List(List) {
          var _result = "";

          $(".resultadoContactRol").children(0).empty();

          for (_idx_user_names in List) {
            if (_idx_user_names != m_current_user_id) {
              if (m_messages_panel.indexOf(_idx_user_names) == -1)
                _result += "<li class='rolAcordeon' id='_id" + _idx_user_names + "' data-userKey='" + _idx_user_names + "' ><span>" + List[_idx_user_names] + "</span></li>";
              else {
                var _msg = 0;
                for (var i = 0; i < m_messages_panel.length ; i++) {
                  if (m_messages_panel[i] == _idx_user_names) {
                    _msg++;
                  }
                }

                _result += "<li class='rolAcordeon'  id='_id" + _idx_user_names + "' data-userKey='" + _idx_user_names + "' ><span>" + List[_idx_user_names] + "</span><span class='badgeMessage severidadMessages' id='div_" + _idx_user_names + "'>" + _msg + "</span></li>";
              }
            }
          }
          $(".resultadoContactRol").children(0).html(_result);

          $('.resultadoContactRol li').off('click').on('click', function () {
            var userId = $(this).attr('data-userKey');
            $("#div_" + userId).remove();
            m_messages_panel = jQuery.grep(m_messages_panel, function (value) {
              return value != userId;
            });
      
            if ($(this).hasClass("contactSeleccionado")) {
              if ($(".buscarUsuario.buscarOculto").length != 0) {
                $(this).removeClass("contactSeleccionado");
                SetConversationPanelOff();
              }
            } else {
              var selectedContacts = $(".contactSeleccionado");
              for (var i = 0; i < selectedContacts.length; i++) {
                selectedContacts.removeClass("contactSeleccionado");
                SetConversationPanelOff();
              }
              $(this).addClass("contactSeleccionado");
              SetConversationPanelOn();
              if ($(".buscarUsuario.buscarOculto").length == 0) {
                $(".rolAcordeon").not(".contactSeleccionado").toggle("slow");
              }
            }

            $('.panelEnviarMensaje').find('#runner-id').text(" " + _Manager.TaskManager.Users[userId]);
            PageMethods.GetConversationHistory(userId, m_current_user_id, OnGetConversationHistoryOnSuccess, OnGetConversationHistoryOnError);
            $('.btnSendMessageDerecha').off('click').on('click', function () {

                if ($("#txtSendMessage").val().trim() == '')
                    return; // DMT Fix Bug 15607

                var message = { runnerId: userId, runnerUserName: _Manager.TaskManager.Users[userId], managerId: m_current_user_id, managerUserName: m_current_user_name, message: $("#txtSendMessage").val() }
              PageMethods.SendMessageToRunner(JSON.stringify(message), OnSendMessageOnSuccess, OnSendMessageOnError);
              PageMethods.ManagerUserName
            });

          })
        }

        function OnSendMessageOnSuccess(message) {
          
            if (message != '') {

                ShowOrHideErrorMessage(false);

                var message = JSON.parse(message);

                PageMethods.GetConversationHistory(message.RunnerId, message.ManagerId, OnGetConversationHistoryOnSuccess, OnGetConversationHistoryOnError);

                $('.conversation').scrollTop($('.conversation')[0].scrollHeight);
                $("#txtSendMessage").val('');
            } else {//Error
                ShowOrHideErrorMessage(true);
            }
        }

        function ShowOrHideErrorMessage(status) {
          
            if (status) {
                $('.conversation-error').show();
                $(".messaging-error-text").show();
               
            } else {
                $('.conversation-error').hide();
                $(".messaging-error-text").hide();
            }
        }

        function OnGetConversationHistoryOnSuccess(messages) {
            var weekdays= 'DOMINGO_LUNES_MARTES_MIERCOLES_JUEVES_VIERNES_SABADO'.split('_')
            var _messages = JSON.parse(messages).Messages;
            var conversation = $('.panelEnviarMensaje').find('.conversation');
            var hoy = moment();
            conversation.empty();
            ShowOrHideErrorMessage(false);
            for (var i = 0; i < _messages.length; i++) {
                var message = _messages[i];
                var messageClass = message.Source == 1 ? "manager-message" : "runner-message";
                var arrowClass = message.Source == 1 ? "manager-message-arrow" : "runner-message-arrow";
                var messageOwner = message.Source == 1 ? message.ManagerUserName : message.RunnerUserName;
                var messagePlain = $($.parseHTML(message.Content)).text();
                var messageCheck;
                var fechaMensaje = moment(message.Date);
                switch (message.Status) {
                  case 0:
                    messageCheck = "message_send";
                    break;
                  case 2:
                    messageCheck = "message_delivered";
                    break;
                  case 3:
                    messageCheck = "message_read";
                    break;
                  default:
                    messageCheck = "message_send";
                    break;
                }

                var div = "<div class='message-buble'>" +
                            "<div class='" + messageClass + "' style='word-wrap: break-word;'>" +
                              "<label class='message-content'>" + messagePlain + "</label>" +
                              "<br/>" +
                              "<label class='message-date' style='word-wrap: italic;'>" +
                                (hoy.diff(fechaMensaje, 'days') == 0 ? '' : (hoy.diff(fechaMensaje, 'days') < 7 && fechaMensaje.day() < hoy.day()) ? _Language.get(weekdays[fechaMensaje.day()]) : fechaMensaje.format('DD/MM/YYYY')) +
                                "    " + moment(message.Date).format('HH:mm')  +
                              (message.Source == 1 ? "<img src='img/" + messageCheck + ".png' width='16px'></img>" : "") +
                              "</label>"+
                              "<br/>" +
                              "<label class='message-owner' style='word-wrap: normal;'>" + messageOwner + "</label>" +
                            "</div>" +
                            "<div class='" + arrowClass + "'/>" +
                          "</div>";
                conversation.append(div);
            }
            $(".conversation").animate({ scrollTop: $('.conversation').prop("scrollHeight") }, 1000);
        }



        function OnGetConversationHistoryOnError(error) {
          ShowOrHideErrorMessage(true);
          WSILogger.Debug("OnGetCOnversationHistoryOnError()", "error", error);
        }


        function OnSendMessageOnError(error) {
          ShowOrHideErrorMessage(true);
          WSILogger.Debug("OnSendMessageOnError()", "error", error);
        }

        function SetConversationPanelOn()
        {
          var conversation = $('.panelEnviarMensaje').find('.conversation');
          //conversation.css('background-color', '#778f9b');
          $('.conversation-panel').show();
        }

        function SetConversationPanelOff() {
          var conversation = $('.panelEnviarMensaje').find('.conversation');
          conversation.empty();
          //conversation.css('background-color', '');
          $('.conversation-panel').hide();
        }

        return {

            // Public methods and variables
            InitConversation: function (runnerId, runnerName) {
                
                $('.panelDerechaExtendido').find('.infoPanel').css("display", "none");
                $('.panelEnviarMensaje').css("display", "block");
                $('.panelEnviarMensaje').find('#runner-id').text(" " + runnerName)
                WSI_Tasks_ui_PanelDerecho_Reload();
                WSI_Messages_AddContacts_to_List(_Manager.TaskManager.Users);

                var listUfUsers = $('.resultadoContactRol li');
                for (var i = 0; i < listUfUsers.length; i++) {
                  var userId = $(listUfUsers[i]).attr('data-userKey');
                  if (userId == runnerId)
                  {
                    $(listUfUsers[i]).addClass("contactSeleccionado");
                    SetConversationPanelOn();
                    
                    if ($(".buscarUsuario.buscarOculto").length == 0) {
                      $(".rolAcordeon").not(".contactSeleccionado").toggle("slow");
                    }
                  }
                }
                if (runnerId === '') {
                  SetConversationPanelOff();
                }
                PageMethods.GetConversationHistory(runnerId, m_current_user_id, OnGetConversationHistoryOnSuccess, OnGetConversationHistoryOnError);
                $('.btnSendMessageDerecha').off('click').on('click', function () {
                    var message = { runnerId: runnerId, runnerUserName: _Manager.TaskManager.Users[runnerId], managerId: m_current_user_id, managerUserName: m_current_user_name, message: $("#txtSendMessage").val() }
                    PageMethods.SendMessageToRunner(JSON.stringify(message), OnSendMessageOnSuccess, OnSendMessageOnError);
                });
            },
            ReloadConversation: function (runnerId, runnerName) {

              PageMethods.GetConversationHistory(runnerId, m_current_user_id, OnGetConversationHistoryOnSuccess, OnGetConversationHistoryOnError);
              $('.btnSendMessageDerecha').off('click').on('click', function () {
                var message = { runnerId: runnerId, runnerUserName: _Manager.TaskManager.Users[runnerId], managerId: m_current_user_id, managerUserName: m_current_user_name, message: $("#txtSendMessage").val() }
                PageMethods.SendMessageToRunner(JSON.stringify(message), OnSendMessageOnSuccess, OnSendMessageOnError);
              });

              m_messages_panel = jQuery.grep(m_messages_panel, function (value) {
                return value != runnerId;
              });
            }
        };

      

    };

    return {

        // Get the Singleton instance if one exists
        // or create one if it doesn't
        getInstance: function () {

            if (!instance) {
                instance = init();
            }

            return instance;
        }
    };

})();