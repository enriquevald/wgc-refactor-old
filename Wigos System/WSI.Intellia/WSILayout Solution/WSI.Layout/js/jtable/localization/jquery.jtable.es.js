﻿/*
    jTable localization file for 'Spanish' language.
    Author: Guillermo Bisheimer
*/
/* AVZ 21/12/2016 
Se agregó el span con el atributo data-nls para que la rutina general de cambio de idioma tenga en cuenta estos elementos.
Hay que agregar también dentro del span el texto inicial del elemento, sino la primera vez que se ejecuta la rutina de localización
-al no existir la grilla ya que se carga en tiempo de ejecución- se mostrará un texto vacío.
*/
(function ($) {

    $.extend(true, $.hik.jtable.prototype.options.messages, {
        serverCommunicationError: 'Ocurrió un error en la comunicación con el servidor.',
        loadingMessage: 'Cargando registros...',
        noDataAvailable: '<span data-nls="NLS_TABLE_NO_DATA_AVAILABLE">No hay datos disponibles!</span>',
        addNewRecord: 'Crear nuevo registro',
        editRecord: 'Editar registro',
        areYouSure: '¿Está seguro?',
        deleteConfirmation: 'El registro será eliminado. ¿Está seguro?',
        save: 'Guardar',
        saving: 'Guardando',
        cancel: 'Cancelar',
        deleteText: 'Eliminar',
        deleting: 'Eliminando',
        error: 'Error',
        close: 'Cerrar',
        cannotLoadOptionsFor: 'No se pueden cargar las opciones para el campo {0}',
        pagingInfo: 'Mostrando registros {0} a {1} de {2}',
        canNotDeletedRecords: 'No se puede borrar registro(s) {0} de {1}!',
        deleteProggress: 'Eliminando {0} de {1} registros, procesando...',
        pageSizeChangeLabel: '<span data-nls="NLS_TABLE_PAGE_SIZE">Registros por página</span>',
        gotoPageLabel: '<span data-nls="NLS_TABLE_GO_TO_PAGE">Ir a página</span>'
    });

})(jQuery);
