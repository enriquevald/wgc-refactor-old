﻿/*
    jTable localization file for 'Spanish' language.
    Author: Guillermo Bisheimer
*/
/* AVZ 21/12/2016 
Se agregó el span con el atributo data-nls para que la rutina general de cambio de idioma tenga en cuenta estos elementos.
Hay que agregar también dentro del span el texto inicial del elemento, sino la primera vez que se ejecuta la rutina de localización
-al no existir la grilla ya que se carga en tiempo de ejecución- se mostrará un texto vacío.
*/
(function ($) {

  $.extend(true, $.hik.jtable.prototype.options.messages, {
    serverCommunicationError: 'An error occured while communicating to the server.',
    loadingMessage: 'Loading records...',
    noDataAvailable: '<span data-nls="NLS_TABLE_NO_DATA_AVAILABLE">No data available!</span>',
    areYouSure: 'Are you sure?',
    save: 'Save',
    saving: 'Saving',
    cancel: 'Cancel',
    error: 'Error',
    close: 'Close',
    cannotLoadOptionsFor: 'Can not load options for field {0}',
    addNewRecord: 'Add new record',
    editRecord: 'Edit Record',
    deleteConfirmation: 'This record will be deleted. Are you sure?',
    deleteText: 'Delete',
    deleting: 'Deleting',
    canNotDeletedRecords: 'Can not delete {0} of {1} records!',
    deleteProggress: 'Deleting {0} of {1} records, processing...',
    pagingInfo: '{0}-{1} TOTAL: {2}',
    pageSizeChangeLabel: '<span data-nls="NLS_TABLE_PAGE_SIZE">Row count</span>',
    gotoPageLabel: '<span data-nls="NLS_TABLE_GO_TO_PAGE">Go to page</span>'

    });

})(jQuery);
