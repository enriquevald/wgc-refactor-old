﻿
///////////////////////////////////////////////////////////////////////////////////////////

var MACHINE_OCUPATION_REAL = 60 // %
var MACHINE_OCUPATION_INITIAL = 60;  // %
var MACHINE_OCUPATION_INTERVAL = 2500 //ms
var MACHINE_OCUPATION_TIME = 0; //ms = new Date().getTime(); // Return the number of milliseconds since 1970/01/01

var m_reports_realtime_ = null;

var m_reports_first_time = true;

var m_show_reports_descriptions = true;

var m_datepicker_size = "0.9em";

///////////////////////////////////////////////////////////////////////////////////////////

function GetMachineOcupationRandom() {
  var _ocupation;

  MACHINE_OCUPATION_TIME += (MACHINE_OCUPATION_INTERVAL);
  Tsec = MACHINE_OCUPATION_TIME / 1000; //sec
  _ocupation = 50 + 20 * Math.sin(2 * Math.PI * Tsec / (5 * 60)) + 20 * Math.sin(2 * Math.PI * Tsec / (15 * 60))

  return _ocupation;
} // GetMachineOcupationRandom

///////////////////////////////////////////////////////////////////////////////////////////

function CreateOptionReports() {

  $("#div_report_chart_reports").empty();

  var _html = "";

  div_report_chart_ = $("#div_report_chart_reports");

  _html += "<div class='box-set'>";
  for (_report in m_report_objects[m_reports_type_selected]) {
    if (m_report_objects[m_reports_type_selected][_report] != null && typeof m_report_objects[m_reports_type_selected][_report] === 'object') {
      _html += "<div class='" + m_report_objects[m_reports_type_selected][_report].style + "' id='" + m_report_objects[m_reports_type_selected][_report].id + "'>";
      _html += "<i>Loading...</i>";
      _html += "</div>";
    }
  }
  _html += "</div>";

  $(div_report_chart_).html(_html);

}

///////////////////////////////////////////////////////////////////////////////////////////

function UpdateReportCharts() {

  for (_report in m_report_objects[m_reports_type_selected]) {

    try {

      if (m_report_objects[m_reports_type_selected][_report].categoryId > -1) {
        // Normal reports

        if (typeof m_report_objects[m_reports_type_selected][_report] == 'object') {


          /*
          var _rname = "'ReportName':'" + m_reports_types[m_reports_type_selected] + "'";

          if (m_report_objects[m_reports_type_selected][_report].reportIndex != undefined) {
          var _rreport = "'ReportIndex':'" + m_report_objects[m_reports_type_selected][_report].reportIndex + "'";
          } else {
          var _rreport = "'ReportIndex':'" + _report + "'";
          }
                

          var _rconcepts = "'Concept':'" + m_reports_concept_selected + "'";
          var _rextras = "'Extra':'" + m_reports_extra_selected + "'";
          var _rdate = "'Date':'" + m_reports_date_selected + "'";
          //var _rdate = "'Date':'2014/10/09'";
          var _rdategroup = "'DateGroup':'" + m_reports_date_group_selected + "'";
          var _pivot = "'Pivot':'" + (m_report_objects[m_reports_type_selected][_report].pivot ? "true" : "") + "'";
          */

          //m_report_objects[m_reports_type_selected][_report].parameters.ReportName = m_reports_types[m_reports_type_selected];
          //m_report_objects[m_reports_type_selected][_report].parameters.ReportIndex = '';
          m_report_objects[m_reports_type_selected][_report].parameters.Concept = m_reports_concept_selected;
          m_report_objects[m_reports_type_selected][_report].parameters.Extra = m_reports_extra_selected;
          m_report_objects[m_reports_type_selected][_report].parameters.Date = FormatDate(m_reports_date_selected);
          //m_report_objects[m_reports_type_selected][_report].parameters.DateGroup = m_reports_date_group_selected;
          m_report_objects[m_reports_type_selected][_report].parameters.Pivot = '';

          $.ajax({
            type: 'POST',
            async: false,
            url: ((_LayoutMode == 0) ? 'charts' : 'Layout') + '.aspx/GetLayoutReportData',
            //data: "{" + _rname + "," + _rreport + "," + _rconcepts + "," + _rextras + "," + _rdate + "," + _rdategroup + "," + _pivot + "}",
            data: JSON.stringify(m_report_objects[m_reports_type_selected][_report].parameters),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (result) {
              OnGetLayoutReportDataComplete(result, m_report_objects[m_reports_type_selected][_report]);
            },
            error: function (jqXHR, textStatus, errorThrown) {
              // --
              WSILogger.Write("-- Error getting report data --");
              WSILogger.Write(textStatus);
              WSILogger.Log("UpdateReportCharts", errorThrown);
              // --
              OnGetLayoutReportDataComplete(textStatus, _report);
            }

          });
        }
      } else {
        // Special reports that no need database data updated by main thread
      }
    }
    catch (_ex) {
      //
      WSILogger.Log('UpdateReportCharts', _ex);
    }

  }

  //OnResizeReportComplete();
  if (_LayoutMode == 0) { FitReports(); }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function OnGetLayoutReportDataComplete(result, Report) {
  //
  if (result.d != "") {
    try {

      WSILogger.Write(" Report [" + Report.id + "] = " + result.d);

      var _result_data = JSON.parse(result.d);

      Report.data = _result_data[0];
      Report.conceptTitle = m_report_concepts[Report.categoryId][+Report.parameters.Concept];

      Report.Prepare();

      Report.Build();

      if (!Report.hasData) {
        //$("#" + Report.id).remove();
        $("#" + Report.id).html('<i>No data for current filters.</i>');
      }
      //

      // Update tooltips
      //$("#" + Report.id).find('.has-tooltip').tooltip();

    } catch (_ex) {
      WSILogger.Log('OnGetLayoutReportDataComplete', _ex);
    }
  } else {
    // NO RESULT
    WSILogger.Write(Report.id + ' without data.');
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function OnGetLayoutReportDataFailed(result, Report, methodName) {
  //
  //_reports_config[Report].data = null;
  m_report_objects[Report].data = null;
  WSILogger.Log('OnGetLayoutReportDataFailed("' + Report.id + '")', result);
  //
}

///////////////////////////////////////////////////////////////////////////////////////////

function SetReportsDescriptions() {

  var _i = 0;
  var _html = "";
  if (m_show_reports_descriptions) {
    for (_i in m_report_objects[m_reports_type_selected]) {
      if (m_report_objects[m_reports_type_selected][_i].hasData || m_report_objects[m_reports_type_selected][_i].CustomUpdate) {
        _html += '<span style="font-size: 10px; font-weight: bold;">' + m_report_objects[m_reports_type_selected][_i].caption + ': ' + '</span><br />';
        _html += '<span style="font-size: 9px; font-style: italic;" data-nls="' + m_report_descriptions[m_reports_type_selected][_i] + '">' + _Language.get(m_report_descriptions[m_reports_type_selected][_i]) + '</span><hr />';
      }
    }
  }
  $("#_report_desc_" + m_reports_type_selected).html(_html);
}

///////////////////////////////////////////////////////////////////////////////////////////

function ChangeReportsType(sender) {

  if (m_reports_type_selected != sender.getAttribute("data-type")) {

    ResetBuildedManualReports();

    m_reports_type_selected = sender.getAttribute("data-type");

    if (m_reports_type_selected != 99) {

      ShowHideDatePicker(m_reports_type_selected, sender.getAttribute("data-date"));

      if (sender.getAttribute("data-date") == "false") {
        m_reports_date_selected = new Date();
      } else {
        m_reports_date_selected = $('#ReportsDatepicker').datepicker('getDate');
      }

      m_reports_concept_selected = 0;

      if (sender.getAttribute("data-extra") == 0) {
        m_reports_extra_selected = 1;
      } else {
        m_reports_extra_selected = 0;
      }

      CreateOptionReports();

      UpdateReportCharts();

      SetReportsDescriptions();
    } else {
      //
      // SNAPSHOT
      FillReportCharts();
      //
    }

  }

}


function SetSelectedConcept(sender) {
  $('#_container_reports_' + m_reports_type_selected).children('button[id^="concept_"]').switchClass("ui-state-highlight", "ui-state-default");
  $(sender).switchClass("ui-state-default", "ui-state-highlight");
}

function ChangeReportsConcept(sender) {

  if (m_reports_concept_selected != sender.getAttribute("data-concept")) {

    m_reports_concept_selected = sender.getAttribute("data-concept");

    SetSelectedConcept(sender);

    CreateOptionReports();

    UpdateReportCharts();

    SetReportsDescriptions();
  }

}

function ChangeReportsExtra(sender) {

  if (m_reports_extra_selected != sender.getAttribute("data-extra")) {

    m_reports_extra_selected = sender.getAttribute("data-extra");

    CreateOptionReports();

    UpdateReportCharts();

    SetReportsDescriptions();
  }

}

function ChangeReportsDate(newDate) {

  if (m_reports_date_selected != newDate) {

    m_reports_date_selected = newDate;

    CreateOptionReports();

    UpdateReportCharts();

    SetReportsDescriptions();
  }
}

function ShowHideDatePicker(Type, Value) {

  if (Value == "true") {
    $("#ReportsDatepicker").show();
  } else {
    $("#ReportsDatepicker").hide();
    return;
  }

  $("#ReportsDatepicker").prependTo($("#_container_reports_" + Type));

}

///////////////////////////////////////////////////////////////////////////////////////////

function CreateReportCharts() {
  var _html;

  _html = "";

  // Options
  $("#div_report_chart_menu").empty();
  _html += "  <div id='accordion_reports_menu' style='width: 202px;  height: 650px; float: left; margin-right: 10px;'>";
  //Create the accordion menu
  _html += "  <div class='group'>";

  //        _html += "    <h3 id='_default_report_type' onclick='ChangeReportsType(this);' data-type='99' data-concept='0' data-extra='0' data-date='false'><span data-nls='NLS_CHART_OPTION_SNAPSHOT'></span></h3>";
  //        _html += "    <div id='_container_reports_99' style= 'height: 0px;'>";
  //        _html += "    </div>";

  _html += "    <h3 id='_default_report_type' onclick='ChangeReportsType(this);' data-type='0' data-concept='1' data-extra='1' data-date='true'><span data-nls='NLS_CHART_OPTION_STATS'></span></h3>";
  _html += "    <div id='_container_reports_0'>";
  _html += "        <br/>";
  _html += "        <div id='_report_desc_0'></div>";
  _html += "        <br/>";
  _html += "        <button id='concept_stats_PLAYED' class='ui-button ui-widget ui-widget-full-width ui-state-highlight ui-corner-all ui-button-text-only' role='button' onclick='ChangeReportsConcept(this);' data-concept='0'>";
  _html += "            <span class='ui-button-text'><span data-nls='NLS_CHART_OPTION_STATS_PLAYED'></span></span></button><br>";
  _html += "        <button id='concept_stats_NWT' class='ui-button ui-widget ui-widget-full-width ui-state-default ui-corner-all ui-button-text-only' role='button' onclick='ChangeReportsConcept(this);' data-concept='1'>";
  _html += "            <span class='ui-button-text'><span data-nls='NLS_CHART_OPTION_STATS_NWTOTAL'></span></span></button><br>";
  _html += "        <button id='concept_stats_NWP' class='ui-button ui-widget ui-widget-full-width ui-state-default ui-corner-all ui-button-text-only' role='button' onclick='ChangeReportsConcept(this);' data-concept='2'>";
  _html += "            <span class='ui-button-text'><span data-nls='NLS_CHART_OPTION_STATS_NWCASHABLE'></span></span></button><br>";
  _html += "        <br/><br/>";

  _html += "        <form>";
  _html += "           <div id='stats_radio' class='ui-buttonset'>";
  _html += "               <input type='radio' id='stats_TOTAL' name='stats_rad' checked='checked' class='ui-helper-hidden-accessible' onclick='ChangeReportsExtra(this);' data-extra='0' style='visibility:hidden;'>";
  _html += "           </div>";
  _html += "        </form>";
  _html += "        <br/><br/>";
  _html += "    </div>";
  _html += "  </div>";
  _html += "<div class='group'>";

  _html += "    <h3 onclick='ChangeReportsType(this);' data-type='1' data-concept='1' data-extra='0' data-date='false'><span data-nls='NLS_CHART_OPTION_OCUPATION'></span></h3>";
  _html += "    <div id='_container_reports_1'>";
  _html += "        <br/>";
  _html += "        <div id='_report_desc_1'></div>";
  _html += "        <br/>";
  _html += "   </div>";
  _html += " </div>";

  _html += "  <div class='group'>";
  _html += "   <h3 onclick='ChangeReportsType(this);' data-type='2' data-concept='1' data-extra='0' data-date='true'><span data-nls='NLS_CHART_OPTION_CASH'></span></h3>";
  _html += "    <div id='_container_reports_2'>";
  _html += "        <br/>";
  _html += "        <div id='_report_desc_2'></div>";
  _html += "        <br/>";
  _html += "    </div>";
  _html += "  </div>";

  _html += "  <div class='group'>";
  _html += "    <h3 onclick='ChangeReportsType(this);' data-type='3' data-concept='1' data-extra='1' data-date='false'><span data-nls='NLS_CHART_OPTION_SEGMENTATION'></span></h3>";
  _html += "    <div id='_container_reports_3'>";
  _html += "        <br/>";
  _html += "        <div id='_report_desc_3'></div>";
  _html += "        <br/>";
  _html += "        <button id='concept_segment_CLIENTS' class='ui-button ui-widget ui-widget-full-width ui-state-highlight ui-corner-all ui-button-text-only' role='button' onclick='ChangeReportsConcept(this);' data-concept='0'>";
  _html += "          <span class='ui-button-text'><span data-nls='NLS_CHART_OPTION_SEGMENT_CLIENTS'></span></span></button>";
  _html += "        <button id='concept_segment_VISITS' class='ui-button ui-widget ui-widget-full-width ui-state-default ui-corner-all ui-button-text-only' role='button' onclick='ChangeReportsConcept(this);' data-concept='1'>";
  _html += "          <span class='ui-button-text'><span data-nls='NLS_CHART_OPTION_SEGMENT_VISITS'></span></span></button>";
  _html += "    </div>";
  _html += "  </div>";

  _html += "  <div id='ReportsDatepicker'></div>";
  _html += "  </div>";


  // Button to Print
  //_html += "<button id='btn_print_reports' class='ui-button ui-widget ui-state-highlight ui-corner-all ui-button-text-only' role='button' onclick='PrintReports();'>";
  //_html += " <span class='ui-button-text'>PRINT</span></button>";


  _html += "</div>";



  //_html += "  </div>";

  $("#div_report_chart_menu").html(_html);


  $(function () {
    $("#accordion_reports_menu")
      .accordion({
        header: "> div > h3",
        heightStyle: "content",
        collapsible: true
      })
      .sortable({
        axis: "y",
        handle: "h3",
        stop: function (event, ui) {
          // IE doesn't register the blur when sorting
          // so trigger focusout handlers to remove .ui-state-focus
          ui.item.children("h3").triggerHandler("focusout");

          // Refresh accordion to handle new order
          $(this).accordion("refresh");
        }
      });
  });

  $("#ReportsDatepicker").datepicker({
    inline: true,
    dateFormat: "yy/mm/dd",
    onSelect: function (dateText) {
      // TODO: Check to format date
      ChangeReportsDate(dateText);
    },
    maxDate: new Date()
  });

  // Adjust datepicker size
  $("div.ui-datepicker").css({ "font-size": m_datepicker_size });

  $("#ReportsDatepicker").hide();

  $("#stats_radio").buttonset();

  //TODO : Mover la creación del menu charts fuera y quitar el     _Language.parse();
  _Language.parse();
  //TODO


  //CreateOptionReports();

  // _Language.parse();

} //CreateReportCharts

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

function SetCurrentReports() {

  CreateReportCharts();
  //UpdateReportCharts();

  // Reports
  ChangeReportsType($("#_default_report_type")[0]);

  //FillReportCharts();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

function printCharts(charts) {

  var origDisplay = [],
                    origParent = [],
                    body = document.body,
                    childNodes = body.childNodes;

  // hide all body content
  Highcharts.each(childNodes, function (node, i) {
    if (node.nodeType === 1) {
      origDisplay[i] = node.style.display;
      node.style.display = "none";
    }
  });

  // put the charts back in
  $.each(charts, function (i, chart) {
    origParent[i] = chart.container.parentNode;
    body.appendChild(chart.container);
  });

  // print
  window.print();

  // allow the browser to prepare before reverting
  setTimeout(function () {
    // put the chart back in
    $.each(charts, function (i, chart) {
      origParent[i].appendChild(chart.container);
    });

    // restore all body content
    Highcharts.each(childNodes, function (node, i) {
      if (node.nodeType === 1) {
        node.style.display = origDisplay[i];
      }
    });
  }, 500);
}

/*
function printCanvas(Canvas) {
var ctx = Canvas.getContext('2d');

ctx.webkitImageSmoothingEnabled = false;
ctx.mozImageSmoothingEnabled = false;
ctx.imageSmoothingEnabled = false;

var dataUrl = Canvas.toDataURL(); //attempt to save base64 string to server using this var  
var windowContent = '<!DOCTYPE html>';
windowContent += '<html>'
windowContent += '<head><title>Print</title></head>';
windowContent += '<body>'
windowContent += '<img src="' + dataUrl + '">';
windowContent += '</body>';
windowContent += '</html>';
var printWin = window.open('', '', 'width=340,height=260,resizable=yes,scrollbars=yes');
printWin.document.open();
printWin.document.write(windowContent);
printWin.document.close();
/*
printWin.focus();
printWin.print();
printWin.close();
*/
/*
}
*/

function PrintReports() {
  //$('#div_reports_mainframe').printThis();
  //$('#div_report_chart_reports').printThis();

  try {
    /*
    var to_print = html2canvas($("#div_report_chart_frame_"), {
    svgRendering: true
    });
    var queue = to_print.parse();
    var canvas = to_print.render(queue);

    printCanvas(canvas);
    */

    /*
    var img = canvas.toDataURL();
    window.open(img);
    html2canvas(document.body, {
    logging: true, 
    svgRendering: false,
    taintTest: false,
    onrendered: function (canvas) {
    //document.body.appendChild(canvas);
    printCanvas(canvas);
    }
    });
    */

    var _charts = [];
    for (_ridx in m_report_objects[m_reports_type_selected]) {
      var _chart = $("#" + m_report_objects[m_reports_type_selected][_ridx].id).highcharts();
      if (_chart) {
        _charts.push(_chart);
      }
    }

    printCharts(_charts);
  } catch (_ex) {
    WSILogger.Log("PrintReports()", _ex);
  }
}


/////Reports Charts functions //////////////////////////////////////////////////////////////////////////////////////
/*
function FillReportCharts() {

var div_report_chart_;
var _html;

div_report_chart_ = $("#div_report_chart_reports");
_html = "";

//    _html += "<div>";
//    _html += "  <div style='width: 12%;  height:650px; float: left;'>";
//    _html += "    <button type='button' class='boxMenu'>NetWin</button>";
//    _html += "    <button type='button' class='boxMenu'>Played</button>";
//    _html += "  </div>";
//    _html += "</div>";

_html += "<div class='box-set'>";
_html += "  <div ><div id='Top10BestEGM' class='box-26'></div></div>";
_html += "  <div ><div id='Top10WorstEGM' class='box-26'></div></div>";
_html += " <div ><div id='Provider' class='box-26'></div></div>";
_html += "</div>";
_html += "<div class='box-set'>";
_html += "  <div id='splineLine' class='box-40'></div>";
_html += "  <div id='linezoom' class='box-40'></div>";
_html += "</div>";
_html += "<div class='box-set'>";
_html += "  <div id='realTime' class='box-60'></div>";
_html += "  <div id='container-speed' class='box-20'></div>";
_html += "</div>";

$(div_report_chart_).html(_html);

$('#Top10BestEGM').highcharts({

chart: {
type: 'column',
options3d: {
enabled: true,
alpha: 15,
beta: 15,
viewDistance: 25,
depth: 40
},
marginTop: 50,
marginRight: 40,
marginBottom: 60,
backgroundColor:'rgba(255, 255, 255, 0.1)'
},

exporting: {
buttons: {
contextButtons: {
enabled: true,
x:20
}
},
enabled: true
},


legend:
{
layout: '',
align: '',
verticalAlign: ''
},

title: {
text: 'Top 10 Best EGM - NetWin'
},

xAxis: {
categories: ['EGM-125', 'EGM-332', 'EGM-003', 'EGM-016', 'EGM-115', 'EGM-001', 'EGM-055', 'EGM-063', 'EGM-084', 'EGM-095'],
labels: {
style: {
fontSize:'9px'
}
}
},

        
yAxis: {
allowDecimals: false,
min: 0,
title: {
text: 'WinSystems'
}
},

tooltip: {
headerFormat: '<b>{point.key}</b><br>',
pointFormat: '<span style="color:{series.color}">\u25CF</span> {series.name}: {point.y} / {point.stackTotal}'
},

plotOptions: {
column: {
stacking: null,
depth: 40
}
},

series: [{
name: 'NetWin',
data: [281.95, 279, 200, 175.50, 164, 152, 110, 108.05, 97.30, 96.62],
color: {
linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
stops: [
[0, '#009900'],
[1, '#004C00']
]
},
stack: ''
}
]
});


$('#Top10WorstEGM').highcharts({

chart: {
type: 'column',
options3d: {
enabled: true,
alpha: 15,
beta: 15,
viewDistance: 25,
depth: 40
},
marginTop: 50,
marginRight: 40,
marginBottom: 60,
backgroundColor:'rgba(255, 255, 255, 0.1)'
},
legend:
{
layout: '',
align: '',
verticalAlign: ''
},
title: {
text: 'Top 10 Wrost - EGM NetWin'
},

xAxis: {
categories: ['EGM-013', 'EGM-125', 'EGM-089', 'EGM-012', 'EGM-111', 'EGM-005', 'EGM-077', 'EGM-064', 'EGM-084', 'EGM-405'],
labels: {
style: {
fontSize:'9px'
}
}
},

yAxis: {
allowDecimals: false,
min: -400,
title: {
text: 'WinSystems'
}
},

tooltip: {
headerFormat: '<b>{point.key}</b><br>',
pointFormat: '<span style="color:{series.color}">\u25CF</span> {series.name}: {point.y} / {point.stackTotal}'
},


plotOptions: {
column: {
stacking: 'normal',
depth: 20

}

},

series: [{
name: 'NetWin',
data: [-275, -250.20, -220, -200.55, -200.20, -135.58, -105, -82, -76, -25.20],
color: {
linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
stops: [
[0, '#FF6600'],
[1, '#993D00']
]
},
stack: ''
}   
]
});

$('#Provider').highcharts({

chart: {
type: 'column',
marginTop: 60,
marginRight: 40,
backgroundColor:'rgba(255, 255, 255, 0.1)'
},

title: {
text: 'NetWin by Provider'
},

xAxis: {
categories: ['Prov-001', 'Prov-002', 'Prov-003', 'Prov-004', 'Prov-005']
},

yAxis: {
allowDecimals: false,
min: 0,
title: {
text: 'WinSystems'
}
},

tooltip: {
headerFormat: '<b>{point.key}</b><br>',
pointFormat: '<span style="color:{series.color}">\u25CF</span> {series.name}: {point.y} / {point.stackTotal}'
},

plotOptions: {
column: {
stacking: 'normal',
depth: 10
}
},

series: [{
name: 'NetWin',
data: [340, 205, 260, 195, 180],
stack: 'male'
}, {
type: 'spline',
name: 'Average',
data: [275, 200, 160, 75, 190],
marker: {
lineWidth: 2,
lineColor: Highcharts.getOptions().colors[3],
fillColor: 'white'
}
}]
});
   
$('#linezoom').highcharts({
chart: {
zoomType: 'x',
marginRight: 30,
backgroundColor:'rgba(255, 255, 255, 0.1)'
},
title: {
text: 'NetWin Today'
},
subtitle: {
text: document.ontouchstart === undefined ?
'Click and drag in the plot area to zoom in' :
'Pinch the chart to zoom in'
},
xAxis: {
type: 'datetime',
minRange: 1 * 24 * 3600000 // fourteen days

},
yAxis: {
title: {
text: 'WinSystems'
}
},
legend: {
enabled: false
},
plotOptions: {
area: {
fillColor: {
linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1},
stops: [
[0, Highcharts.getOptions().colors[0]],
[1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
]
},
marker: {
radius: 2
},
lineWidth: 1,
states: {
hover: {
lineWidth: 1
}
},
threshold: null
}
},
    
series: [{
type: 'area',
name: 'NetWin',
pointInterval: 1 * 3600 * 20,
pointStart: Date.UTC(2014, 8, 29),
data: [
0.8446, 0.8445, 0.8444, 0.8451,    0.8418, 0.8264,    0.8258, 0.8232,    0.8233, 0.8258,
0.8283, 0.8278, 0.8256, 0.8292,    0.8239, 0.8239,    0.8245, 0.8265,    0.8261, 0.8269,
0.8273, 0.8244, 0.8244, 0.8172,    0.8139, 0.8146,    0.8164, 0.82,    0.8269, 0.8269,
0.8269, 0.8258, 0.8247, 0.8286,    0.8289, 0.8316,    0.832, 0.8333,    0.8352, 0.8357,
0.8355, 0.8354, 0.8403, 0.8403,    0.8406, 0.8403,    0.8396, 0.8418,    0.8409, 0.8384,
0.8386, 0.8372, 0.839, 0.84, 0.8389, 0.84, 0.8423, 0.8423, 0.8435, 0.8422,
0.838, 0.8373, 0.8316, 0.8303,    0.8303, 0.8302,    0.8369, 0.84, 0.8385, 0.84,
0.8401, 0.8402, 0.8381, 0.8351,    0.8314, 0.8273,    0.8213, 0.8207,    0.8207, 0.8215,
0.8242, 0.8273, 0.8301, 0.8346,    0.8312, 0.8312,    0.8312, 0.8306,    0.8327, 0.8282,
0.824, 0.8255, 0.8256, 0.8273, 0.8209, 0.8151, 0.8149, 0.8213, 0.8273, 0.8273,
0.8261, 0.8252, 0.824, 0.8262, 0.8258, 0.8261, 0.826, 0.8199, 0.8153, 0.8097,
0.8101, 0.8119, 0.8107, 0.8105,    0.8084, 0.8069,    0.8047, 0.8023,    0.7965, 0.7919,
0.7921, 0.7922, 0.7934, 0.7918,    0.7915, 0.787, 0.7861, 0.7861, 0.7853, 0.7867,
0.7827, 0.7834, 0.7766, 0.7751, 0.7739, 0.7767, 0.7802, 0.7788, 0.7828, 0.7816,
0.7829, 0.783, 0.7829, 0.7781, 0.7811, 0.7831, 0.7826, 0.7855, 0.7855, 0.7845,
0.7798, 0.7777, 0.7822, 0.7785, 0.7744, 0.7743, 0.7726, 0.7766, 0.7806, 0.785,
0.7907, 0.7912, 0.7913, 0.7931, 0.7952, 0.7951, 0.7928, 0.791, 0.7913, 0.7912,
0.7941, 0.7953, 0.7921, 0.7919, 0.7968, 0.7999, 0.7999, 0.7974, 0.7942, 0.796,
0.7969, 0.7862, 0.7821, 0.7821, 0.7821, 0.7811, 0.7833, 0.7849, 0.7819, 0.7809,
0.7809, 0.7827, 0.7848, 0.785, 0.7873, 0.7894, 0.7907, 0.7909, 0.7947, 0.7987,
0.799, 0.7927, 0.79, 0.7878, 0.7878, 0.7907, 0.7922, 0.7937, 0.786, 0.787,
0.7838, 0.7838, 0.7837, 0.7836, 0.7806, 0.7825, 0.7798, 0.777, 0.777, 0.7772,
0.7793, 0.7788, 0.7785, 0.7832, 0.7865, 0.7865, 0.7853, 0.7847, 0.7809, 0.778,
0.7799, 0.78, 0.7801, 0.7765, 0.7785, 0.7811, 0.782, 0.7835, 0.7845, 0.7844,
0.782, 0.7811, 0.7795, 0.7794, 0.7806, 0.7794, 0.7794, 0.7778, 0.7793, 0.7808,
0.7824, 0.787, 0.7894, 0.7893, 0.7882, 0.7871, 0.7882, 0.7871, 0.7878, 0.79,
0.7901, 0.7898, 0.7879, 0.7886, 0.7858, 0.7814, 0.7825, 0.7826, 0.7826, 0.786,
0.7878, 0.7868, 0.7883, 0.7893, 0.7892, 0.7876, 0.785, 0.787, 0.7873, 0.7901,
0.7936, 0.7939, 0.7938, 0.7956, 0.7975, 0.7978, 0.7972, 0.7995, 0.7995, 0.7994,
0.7976, 0.7977, 0.796, 0.7922, 0.7928, 0.7929, 0.7948, 0.797, 0.7953, 0.7907,
0.7872, 0.7852, 0.7852, 0.786, 0.7862, 0.7836, 0.7837, 0.784, 0.7867, 0.7867,
0.7869, 0.7837, 0.7827, 0.7825, 0.7779, 0.7791, 0.779, 0.7787, 0.78, 0.7807,
0.7803, 0.7817, 0.7799, 0.7799, 0.7795, 0.7801, 0.7765, 0.7725, 0.7683, 0.7641,
0.7639, 0.7616, 0.7608, 0.759, 0.7582, 0.7539, 0.75, 0.75, 0.7507, 0.7505,
0.7516, 0.7522, 0.7531, 0.7577, 0.7577, 0.7582, 0.755, 0.7542, 0.7576, 0.7616,
0.7648, 0.7648, 0.7641, 0.7614, 0.757, 0.7587, 0.7588, 0.762, 0.762, 0.7617,
0.7618, 0.7615, 0.7612, 0.7596, 0.758, 0.758, 0.758, 0.7547, 0.7549, 0.7613,
0.7655, 0.7693, 0.7694, 0.7688, 0.7678, 0.7708, 0.7727, 0.7749, 0.7741, 0.7741,
0.7732, 0.7727, 0.7737, 0.7724, 0.7712, 0.772, 0.7721, 0.7717, 0.7704, 0.769,
0.7711, 0.774, 0.7745, 0.7745, 0.774, 0.7716, 0.7713, 0.7678, 0.7688, 0.7718,
0.7718, 0.7728, 0.7729, 0.7698, 0.7685, 0.7681, 0.769, 0.769, 0.7698, 0.7699,
0.7651, 0.7613, 0.7616, 0.7614, 0.7614, 0.7607, 0.7602, 0.7611, 0.7622, 0.7615,
0.7598, 0.7598, 0.7592, 0.7573, 0.7566, 0.7567, 0.7591, 0.7582, 0.7585, 0.7613,
0.7631, 0.7615, 0.76, 0.7613, 0.7627, 0.7627, 0.7608, 0.7583, 0.7575, 0.7562,
0.752, 0.7512, 0.7512, 0.7517, 0.752, 0.7511, 0.748, 0.7509, 0.7531, 0.7531,
0.7527, 0.7498, 0.7493, 0.7504, 0.75, 0.7491, 0.7491, 0.7485, 0.7484, 0.7492,
0.7471, 0.7459, 0.7477, 0.7477, 0.7483, 0.7458, 0.7448, 0.743, 0.7399, 0.7395,
0.7395, 0.7378, 0.7382, 0.7362, 0.7355, 0.7348, 0.7361, 0.7361, 0.7365, 0.7362,
0.7331, 0.7339, 0.7344, 0.7327, 0.7327, 0.7336, 0.7333, 0.7359, 0.7359, 0.7372,
0.736, 0.736, 0.735, 0.7365, 0.7384, 0.7395, 0.7413, 0.7397, 0.7396, 0.7385,
0.7378, 0.7366, 0.74, 0.7411, 0.7406, 0.7405, 0.7414, 0.7431, 0.7431, 0.7438,
0.7443, 0.7443, 0.7443, 0.7434, 0.7429, 0.7442, 0.744, 0.7439, 0.7437, 0.7437,
0.7429, 0.7403, 0.7399, 0.7418, 0.7468, 0.748, 0.748, 0.749, 0.7494, 0.7522,
0.7515, 0.7502, 0.7472, 0.7472, 0.7462, 0.7455, 0.7449, 0.7467, 0.7458, 0.7427,
0.7427, 0.743, 0.7429, 0.744, 0.743, 0.7422, 0.7388, 0.7388, 0.7369, 0.7345,
0.7345, 0.7345, 0.7352, 0.7341, 0.7341, 0.734, 0.7324, 0.7272, 0.7264, 0.7255,
0.7258, 0.7258, 0.7256, 0.7257, 0.7247, 0.7243, 0.7244, 0.7235, 0.7235, 0.7235,
0.7235, 0.7262, 0.7288, 0.7301, 0.7337, 0.7337, 0.7324, 0.7297, 0.7317, 0.7315,
0.7288, 0.7263, 0.7263, 0.7242, 0.7253, 0.7264, 0.727, 0.7312, 0.7305, 0.7305,
0.7318, 0.7358, 0.7409, 0.7454, 0.7437, 0.7424, 0.7424, 0.7415, 0.7419, 0.7414,
0.7377, 0.7355, 0.7315, 0.7315, 0.732, 0.7332, 0.7346, 0.7328, 0.7323, 0.734,
0.734, 0.7336, 0.7351, 0.7346, 0.7321, 0.7294, 0.7266, 0.7266, 0.7254, 0.7242,
0.7213, 0.7197, 0.7209, 0.721, 0.721, 0.721, 0.7209, 0.7159, 0.7133, 0.7105,
0.7099, 0.7099, 0.7093, 0.7093, 0.7076, 0.707, 0.7049, 0.7012, 0.7011, 0.7019,
0.7046, 0.7063, 0.7089, 0.7077, 0.7077, 0.7077, 0.7091, 0.7118, 0.7079, 0.7053,
0.705, 0.7055, 0.7055, 0.7045, 0.7051, 0.7051, 0.7017, 0.7, 0.6995, 0.6994,
0.7014, 0.7036, 0.7021, 0.7002, 0.6967, 0.695, 0.695, 0.6939, 0.694, 0.6922,
0.6919, 0.6914, 0.6894, 0.6891, 0.6904, 0.689, 0.6834, 0.6823, 0.6807, 0.6815,
0.6815, 0.6847, 0.6859, 0.6822, 0.6827, 0.6837, 0.6823, 0.6822, 0.6822, 0.6792,
0.6746, 0.6735, 0.6731, 0.6742, 0.6744, 0.6739, 0.6731, 0.6761, 0.6761, 0.6785,
0.6818, 0.6836, 0.6823, 0.6805, 0.6793, 0.6849, 0.6833, 0.6825, 0.6825, 0.6816,
0.6799, 0.6813, 0.6809, 0.6868, 0.6933, 0.6933, 0.6945, 0.6944, 0.6946, 0.6964,
0.6965, 0.6956, 0.6956, 0.695, 0.6948, 0.6928, 0.6887, 0.6824, 0.6794, 0.6794,
0.6803, 0.6855, 0.6824, 0.6791, 0.6783, 0.6785, 0.6785, 0.6797, 0.68, 0.6803,
0.6805, 0.676, 0.677, 0.677, 0.6736, 0.6726, 0.6764, 0.6821, 0.6831, 0.6842,
0.6842, 0.6887, 0.6903, 0.6848, 0.6824, 0.6788, 0.6814, 0.6814, 0.6797, 0.6769,
0.6765, 0.6733, 0.6729, 0.6758, 0.6758, 0.675, 0.678, 0.6833, 0.6856, 0.6903,
0.6896, 0.6896, 0.6882, 0.6879, 0.6862, 0.6852, 0.6823, 0.6813, 0.6813, 0.6822,
0.6802, 0.6802, 0.6784, 0.6748, 0.6747, 0.6747, 0.6748, 0.6733, 0.665, 0.6611,
0.6583, 0.659, 0.659, 0.6581, 0.6578, 0.6574, 0.6532, 0.6502, 0.6514, 0.6514,
0.6507, 0.651, 0.6489, 0.6424, 0.6406, 0.6382, 0.6382, 0.6341, 0.6344, 0.6378,
0.6439, 0.6478, 0.6481, 0.6481, 0.6494, 0.6438, 0.6377, 0.6329, 0.6336, 0.6333,
0.6333, 0.633, 0.6371, 0.6403, 0.6396, 0.6364, 0.6356, 0.6356, 0.6368, 0.6357,
0.6354, 0.632, 0.6332, 0.6328, 0.6331, 0.6342, 0.6321, 0.6302, 0.6278, 0.6308,
0.6324, 0.6324, 0.6307, 0.6277, 0.6269, 0.6335, 0.6392, 0.64, 0.6401, 0.6396,
0.6407, 0.6423, 0.6429, 0.6472, 0.6485, 0.6486, 0.6467, 0.6444, 0.6467, 0.6509,
0.6478, 0.6461, 0.6461, 0.6468, 0.6449, 0.647, 0.6461, 0.6452, 0.6422, 0.6422,
0.6425, 0.6414, 0.6366, 0.6346, 0.635, 0.6346, 0.6346, 0.6343, 0.6346, 0.6379,
0.6416, 0.6442, 0.6431, 0.6431, 0.6435, 0.644, 0.6473, 0.6469, 0.6386, 0.6356,
0.634, 0.6346, 0.643, 0.6452, 0.6467, 0.6506, 0.6504, 0.6503, 0.6481, 0.6451,
0.645, 0.6441, 0.6414, 0.6409, 0.6409, 0.6428, 0.6431, 0.6418, 0.6371, 0.6349,
0.6333, 0.6334, 0.6338, 0.6342, 0.632, 0.6318, 0.637, 0.6368, 0.6368, 0.6383,
0.6371, 0.6371, 0.6355, 0.632, 0.6277, 0.6276, 0.6291, 0.6274, 0.6293, 0.6311,
0.631, 0.6312, 0.6312, 0.6304, 0.6294, 0.6348, 0.6378, 0.6368, 0.6368, 0.6368,
0.636, 0.637, 0.6418, 0.6411, 0.6435, 0.6427, 0.6427, 0.6419, 0.6446, 0.6468,
0.6487, 0.6594, 0.6666, 0.6666, 0.6678, 0.6712, 0.6705, 0.6718, 0.6784, 0.6811,
0.6811, 0.6794, 0.6804, 0.6781, 0.6756, 0.6735, 0.6763, 0.6762, 0.6777, 0.6815,
0.6802, 0.678, 0.6796, 0.6817, 0.6817, 0.6832, 0.6877, 0.6912, 0.6914, 0.7009,
0.7012, 0.701, 0.7005, 0.7076, 0.7087, 0.717, 0.7105, 0.7031, 0.7029, 0.7006,
0.7035, 0.7045, 0.6956, 0.6988, 0.6915, 0.6914, 0.6859, 0.6778, 0.6815, 0.6815,
0.6843, 0.6846, 0.6846, 0.6923, 0.6997, 0.7098, 0.7188, 0.7232, 0.7262, 0.7266,
0.7359, 0.7368, 0.7337, 0.7317, 0.7387, 0.7467, 0.7461, 0.7366, 0.7319, 0.7361,
0.7437, 0.7432, 0.7461, 0.7461, 0.7454, 0.7549, 0.7742, 0.7801, 0.7903, 0.7876,
0.7928, 0.7991, 0.8007, 0.7823, 0.7661, 0.785, 0.7863, 0.7862, 0.7821, 0.7858,
0.7731, 0.7779, 0.7844, 0.7866, 0.7864, 0.7788, 0.7875, 0.7971, 0.8004, 0.7857,
0.7932, 0.7938, 0.7927, 0.7918, 0.7919, 0.7989, 0.7988, 0.7949, 0.7948, 0.7882,
0.7745, 0.771, 0.775, 0.7791, 0.7882, 0.7882, 0.7899, 0.7905, 0.7889, 0.7879,
0.7855, 0.7866, 0.7865, 0.7795, 0.7758, 0.7717, 0.761, 0.7497, 0.7471, 0.7473,
0.7407, 0.7288, 0.7074, 0.6927, 0.7083, 0.7191, 0.719, 0.7153, 0.7156, 0.7158,
0.714, 0.7119, 0.7129, 0.7129, 0.7049, 0.7095
]
}]
});

$(document).ready(function() {
Highcharts.setOptions({
global: {
useUTC: false
}
});
    
var chart;
$('#realTime').highcharts({
chart: {
type: 'spline',
animation: Highcharts.svg, // don't animate in old IE
marginRight: 30,
backgroundColor:'rgba(255, 255, 255, 0.1)',
events: {
load: function() {
    
// set up the updating of the chart each second
var series = this.series[0];
//
if (m_reports_realtime_ != null) {
//clearInterval(m_reports_realtime_);
}
//
m_reports_realtime_ = setInterval(function() {
var x = (new Date()).getTime(), // current time
//y = Math.random();
y = MACHINE_OCUPATION_REAL; //SMN

series.addPoint([x, y], true, true);
}, MACHINE_OCUPATION_INTERVAL);
}
}
},
title: {
text: '% EGM Ocupation'
},
xAxis: {
type: 'datetime',
tickPixelInterval: 150
},
yAxis: {
title: {
text: 'WinSystems'
},
plotLines: [{
value: 0,
width: 1,
color: '#808080'
}]
},
tooltip: {
formatter: function() {
return '<b>'+ this.series.name +'</b><br/>'+
Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x) +'<br/>'+
Highcharts.numberFormat(this.y, 2);
}
},
legend: {
enabled: false
},
exporting: {
enabled: false
},
series: [{
name: 'Random data',
data: (function() {
// generate an array of random data
var data = [],
time = (new Date()).getTime(),
i;
    
for (i = -19; i <= 0; i++) {
data.push({
x: time + i * 1000,
//y: Math.random() +i //SMN
y: MACHINE_OCUPATION_INITIAL
});
}
return data;
})()
}]
});
});
    

$('#splineLine').highcharts({
chart: {
type: 'areaspline',
marginRight: 30,
backgroundColor:'rgba(255, 255, 255, 0.1)'
},
title: {
text: 'Played vs Netwin Today'
},
            
xAxis: {
categories: [
'11:00',
'13:00',
'15:00',
'17:00',
'19:00',
'21:00',
'23:00',
'01:00',
'03:00',
'05:00',
'07:00',
'09:00'
],
plotBands: [{ // visualize the weekend
from: 2.5,
to: 7.5,
color: 'rgba(68, 170, 213, .2)'
}]
},
yAxis: {
title: {
text: 'WinSystems'
}
},
tooltip: {
shared: true,
valueSuffix: ' $'
},
credits: {
enabled: false
},
plotOptions: {
areaspline: {
fillOpacity: 0.5
}
},
series: [{
name: 'Played',
data: [3056, 7768, 3980, 5667, 4678, 12545, 10678, 9875, 8754, 6254, 1425, 7865]
}, {
name: 'NetWin',
data: [1457, 5242, 422, 3656, 3098, 9002.3, 8155.05, 6875, 6754, 3254, 925, 2565]
}]
});
   

$('#pie3D').highcharts({




chart: {
type: 'pie',
backgroundColor:'rgba(255, 255, 255, 0.1)',

options3d: {
			
enabled: true,
alpha: 45,
beta: 0,
                 
},
margin: 20
},

       
title: {
text: 'NetWin(%) in Room by Provider'
},
tooltip: {
pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
},
plotOptions: {
pie: {
allowPointSelect: true,
cursor: 'pointer',				 
depth: 35,
dataLabels: {
enabled: true,
format: '{point.name}'
}
}
},
		
				
series: [{
type: 'pie',
name: 'Netwin(%)',
data: [
                
{
name: 'WINS',
y: 35.3,
sliced: true,
selected: true
},
['Citro',       26.8],
['PEP-Tronic',   8.5],
['Scainnet',     6.2],
['Others',   	 0.7]
]
}]
});

	
var gaugeOptions = {
	
chart: {
type: 'solidgauge',
backgroundColor:'rgba(255, 255, 255, 0.1)'
},
	    
title: null,
	    
pane: {
center: ['50%', '85%'],
size: '140%',
startAngle: -90,
endAngle: 90,
background: {
backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
innerRadius: '60%',
outerRadius: '100%',
shape: 'arc'
}
},

tooltip: {
enabled: false
},
	       
// the value axis
yAxis: {
stops: [
[0.1, '#55BF3B'], // green
[0.5, '#DDDF0D'], // yellow
[0.9, '#DF5353'] // red
],
lineWidth: 0,
minorTickInterval: null,
tickPixelInterval: 400,
tickWidth: 0,
title: {
y: -70
},
labels: {
y: 16
}        
},
        
plotOptions: {
solidgauge: {
dataLabels: {
y: -30,
borderWidth: 0,
useHTML: true
}
}
}
};
    
// The speed gauge
$('#container-speed').highcharts(Highcharts.merge(gaugeOptions, {

chart: {
	     
backgroundColor:'rgba(255, 255, 255, 0.1)'
},
yAxis: {
min: 0,
max: 100,
title: {
text: ''
}       
},

credits: {
enabled: false
},
	
series: [{
name: 'Slot Machines Ocupation',
//data: [8],
data: [MACHINE_OCUPATION_INITIAL], //SMN
dataLabels: {
format: '<div style="text-align:center"><span style="font-size:25px;color:' + 
((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' + 
'<span style="font-size:12px;color:silver">% Ocupation</span></div>'
},
tooltip: {
valueSuffix: ' %'
}
}]
	
}));
    
    
// Bring life to the dials
setInterval(function () {
// Speed
var chart = $('#container-speed').highcharts();
if (chart) {
var point = chart.series[0].points[0],
newVal;
//var inc = Math.round((Math.random() - 0.5) * 100);
var inc = GetMachineOcupationRandom() / 100; //SMN
if(Math.random() <  0.45)
{
inc *= -1;
}

newVal = point.y + inc;
newVal = Math.round(newVal);
if (newVal < 0 || newVal > 100) {
newVal = point.y - inc;
}
MACHINE_OCUPATION_REAL = newVal;
            
point.update(newVal);
}

      
}, MACHINE_OCUPATION_INTERVAL);  

}
*/

