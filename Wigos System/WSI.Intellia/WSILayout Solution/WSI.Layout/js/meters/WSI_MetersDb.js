﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WSI_Meters.js
// 
//   DESCRIPTION: Routines to obtain the system meters.
// 
//        AUTHOR: Ramón Monclús
// 
// CREATION DATE: 18-JUN-2015
// 
//------------------------------------------------------------------------------

/*
  Requires:
    WSICommon.js
    WSIManager.js
    WSILogger.js
 */

// meters hour
var m_current_hour = -1;

// Last valid meters hour
var m_last_meters_hour = -1;

// Method to obtain meters data
function GetCurrentMeters() {
  if (m_current_hour != GetCurrentHour()) {
    m_current_hour = GetCurrentHour();

    // Ensure manager has meters
    if (!_Manager.MetersManager) {
      _Manager.MetersManager = new WSIMetersManager();
    }

    PageMethods.GetSiteMetersData(OnGetSiteMetersDataComplete, OnGetSiteMetersDataFailed);
  }
}

// Callback to execute when meters data obtained without problems
function OnGetSiteMetersDataComplete(result, userContext, methodName) {

  try {

    WSILogger.Debug("OnGetSiteMetersDataComplete()", "DATA_SIZE", result.length);

    if (result != "") {

      // Parse received data
      var _data = JSON.parse(result);

      _Manager.MetersManager.AddMeters(_data.Meters.rows);
      _Manager.MetersManager.AddMetersData(_data.Data.rows);
      _Manager.MetersManager.WeatherMeter = _data.Weather.rows;

      // Set meters hour
      m_last_meters_hour = m_current_hour;

      WSILogger.Write('Finished: OnGetSiteMetersDataComplete');

    } else {
      // Retry inmediatelly
      m_current_hour = -1;
      m_last_meters_hour = -1;
    }

  } catch (error) {

    // Retry inmediatelly
    m_current_hour = -1;
    m_last_meters_hour = -1;

    WSILogger.Debug("OnGetSiteMetersDataComplete()", "error", error);
  }

  //Create DashBoard Widgets
  CreateWidgets();

  LoadLayoutFloors();
 
}

// Callback for error trap when the process to obtain meters data throws an error
function OnGetSiteMetersDataFailed(error, userContext, methodName) {
  WSILogger.Debug("OnGetSiteMetersDataFailed()", "error", error);

  // Redirect to page error
  //GotoError('OnRealTimeFailed');
}

//-----------------------------------------------------------------------------
