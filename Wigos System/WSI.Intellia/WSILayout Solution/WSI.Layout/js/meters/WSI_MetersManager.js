﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WSI_MetersData_new
// 
//   DESCRIPTION: Classes for manage Meters.
// 
//        AUTHOR: Ramón Monclús
// 
// CREATION DATE: JUN-2015
// 
//------------------------------------------------------------------------------

// Meter item Range constructor
function WSIMeterItemRange(RangeId) {
  if (RangeId == undefined) { RangeId = "TDA"; }
  this.Range = RangeId;

  this.Hour = 0;
  this.Date = 0;
  this.WeekDay = 0;
  this.SiteId = 0;
  this.MeterId = 0;
  this.MeterItem = 0;
  this.HourMin = 0;
  this.HourMax = 0;
  this.HourAcc = 0;
  this.HourAvg = 0;
  this.HourNum = 0;
  this.DayMin = 0;
  this.DayMax = 0;
  this.DayAcc = 0;
  this.DayAvg = 0;
  this.DayNum = 0;

}

// Meter Item constructor
function WSIMeterItem() {
  this.Id = "";

  this.Type = 0;
  this.Description = "";
  this.LastUpdated = "";
  this.Ranges = [];
  this.Ranges["TDA"] = new WSIMeterItemRange();
}

// Meter constructor
function WSIMeter() {
  this.Id = "";

  this.Items = [];
}

// MeterNow constructor
function WSIMeterNow() {
  this.Id = 0;

  this.Value = 0;
}

// Returns a range from a meter item
WSIMeter.prototype.GetRange = function (MeterItem, MeterRange) {
  if (this.Items[MeterItem].Ranges[MeterRange]) {
    return this.Items[MeterItem].Ranges[MeterRange];
  } else {
    return new WSIMeterItemRange(MeterRange);
  }
}

// Returns a value for the specified meter item
WSIMeter.prototype.GetValue = function (MeterItem, MeterRange, MeterValue) {

  switch (MeterValue) {
    case 'range':
      switch (MeterRange) {
        //case 'NOW':
        //  return "Current";
        //  break;
        case 'TDA':
          return "Today";
          break;
        case 'WTD':
          return "Week";
          break;
        case 'MTD':
          return "Month";
          break;
        case 'YTD':
          return "Year";
          break;
        case 'TDY':
          return "Yesterday";
          break;
        case 'TDW':
          return "LastWeekDay";
          break;
        default:
          return MeterRange;
          break;
      }
      break;
    case 'descript':
      return this.Items[MeterItem].Description;
      break;
    case 'lastupdate':
      return this.Items[MeterItem].LastUpdate;
      break;
    case 'hmin':
      return this.GetRange(MeterItem, MeterRange).HourMin;
      break;
    case 'hmax':
      return this.GetRange(MeterItem, MeterRange).HourMax;
      break;
    case 'hacc':
      return this.GetRange(MeterItem, MeterRange).HourAcc;
      break;
    case 'havg':
      return this.GetRange(MeterItem, MeterRange).HourAvg;
      break;
    case 'hnum':
      return this.GetRange(MeterItem, MeterRange).HourNum;
      break;
    case 'dmin':
      return this.GetRange(MeterItem, MeterRange).DayMin;
      break;
    case 'dmax':
      return this.GetRange(MeterItem, MeterRange).DayMax;
      break;
    case 'dacc':
      return this.GetRange(MeterItem, MeterRange).DayAcc;
      break;
    case 'davg':
      return this.GetRange(MeterItem, MeterRange).DayAvg;
      break;
    case 'dnum':
      return this.GetRange(MeterItem, MeterRange).DayNum;
      break;
    default:
      return '';
      break;
  }
}

// Reset the values of a meter for range NOW
WSIMeter.prototype.ResetMeterNow = function () {
  for (var _item in this.Items) {
    //this.Items[_item].Ranges["TDA"] = new WSIMeterItemRange("TDA");
    this.Items[_item].Ranges["TDA"].HourMin = 0;
    this.Items[_item].Ranges["TDA"].HourMax = 0;
    this.Items[_item].Ranges["TDA"].HourAcc = 0;
    this.Items[_item].Ranges["TDA"].HourAvg = 0;
    this.Items[_item].Ranges["TDA"].HourNum = 0;
  }
}

// Updates the values of a meter in range NOW from terminal activity data
WSIMeter.prototype.UpdateMeterNow = function (Terminal) {

  // Update meters data

  switch (+this.Id) {
    case 1: // BET
      this.Items[0].Ranges["TDA"].HourNum = this.Items[0].Ranges["TDA"].HourNum + Terminal.Properties.played_count;
      this.Items[0].Ranges["TDA"].HourMin = 0;
      this.Items[0].Ranges["TDA"].HourMax = 0;
      this.Items[0].Ranges["TDA"].HourAcc = this.Items[0].Ranges["TDA"].HourAcc + Terminal.Properties.played_amount;
      this.Items[0].Ranges["TDA"].HourAvg = this.Items[0].Ranges["TDA"].HourAcc / this.Items[0].Ranges["TDA"].HourNum;
      break;

    case 2: // OCC. GENDER
      if (Terminal.Properties.play_occupation_status != "1") {
        switch (Terminal.Properties.player_gender) {
          case "1": // MALE
            this.Items[1].Ranges["TDA"].HourNum = this.Items[2].Ranges["TDA"].HourNum + 1;
            break;
          case "2": // FEMALE
            this.Items[2].Ranges["TDA"].HourNum = this.Items[2].Ranges["TDA"].HourNum + 1;
            break;
          default: // UNKNOWN
            this.Items[3].Ranges["TDA"].HourNum = this.Items[3].Ranges["TDA"].HourNum + 1;
            break;
        }
        this.Items[1].Ranges["TDA"].HourAcc = this.Items[1].Ranges["TDA"].HourAcc + 1;
        this.Items[2].Ranges["TDA"].HourAcc = this.Items[2].Ranges["TDA"].HourAcc + 1;
        this.Items[3].Ranges["TDA"].HourAcc = this.Items[3].Ranges["TDA"].HourAcc + 1;
        this.Items[1].Ranges["TDA"].HourAvg = this.Items[1].Ranges["TDA"].HourAcc / this.Items[1].Ranges["TDA"].HourNum;
        this.Items[2].Ranges["TDA"].HourAvg = this.Items[2].Ranges["TDA"].HourAcc / this.Items[2].Ranges["TDA"].HourNum;
        this.Items[3].Ranges["TDA"].HourAvg = this.Items[3].Ranges["TDA"].HourAcc / this.Items[3].Ranges["TDA"].HourNum;
      }
      break;

    case 3: // OCC. PLAYER TYPE
      if (Terminal.Properties.play_occupation_status != "1") {
        switch (Terminal.Properties.player_level) {
          case "1": // Registered
            this.Items[1].Ranges["TDA"].HourNum = this.Items[2].Ranges["TDA"].HourNum + 1;
            break;
          case "2": // Anonymous
            this.Items[2].Ranges["TDA"].HourNum = this.Items[2].Ranges["TDA"].HourNum + 1;
            break;
          default: // UNKNOWN
            this.Items[3].Ranges["TDA"].HourNum = this.Items[3].Ranges["TDA"].HourNum + 1;
            break;
        }
        this.Items[1].Ranges["TDA"].HourAcc = this.Items[1].Ranges["TDA"].HourAcc + 1;
        this.Items[2].Ranges["TDA"].HourAcc = this.Items[2].Ranges["TDA"].HourAcc + 1;
        this.Items[3].Ranges["TDA"].HourAcc = this.Items[3].Ranges["TDA"].HourAcc + 1;
        this.Items[1].Ranges["TDA"].HourAvg = this.Items[1].Ranges["TDA"].HourAcc / this.Items[1].Ranges["TDA"].HourNum;
        this.Items[2].Ranges["TDA"].HourAvg = this.Items[2].Ranges["TDA"].HourAcc / this.Items[2].Ranges["TDA"].HourNum;
        this.Items[3].Ranges["TDA"].HourAvg = this.Items[3].Ranges["TDA"].HourAcc / this.Items[3].Ranges["TDA"].HourNum;
      }
      break;

      //case 4: // OCCUPATION
      //if (Terminal.Properties.play_occupation_status != "1") {
      //  this.Items[0].Ranges["TDA"].HourNum = this.Items[0].Ranges["TDA"].HourNum + 1;
      //}
      //this.Items[0].Ranges["TDA"].HourAcc = this.Items[0].Ranges["TDA"].HourAcc + 1;
      //this.Items[0].Ranges["TDA"].HourAvg = (100 * this.Items[0].Ranges["TDA"].HourNum) / this.Items[0].Ranges["TDA"].HourAcc;
      //break;

     case 12: // PLAYED
      var _last_hour_acc = 0;

        if (this.Items[0].Ranges["TDL"] != undefined) {
          _last_hour_acc = this.Items[0].Ranges["TDL"].HourAcc;
        } else {
          _last_hour_acc = this.Items[0].Ranges["TDA"].DayAcc;
        }

      if (Terminal.Properties.played_amount > 0) {
        this.Items[0].Ranges["TDA"].HourNum = this.Items[0].Ranges["TDA"].HourNum + 1;
        this.Items[0].Ranges["TDA"].HourMin = 0;
        this.Items[0].Ranges["TDA"].HourMax = 0;
        this.Items[0].Ranges["TDA"].HourAcc = this.Items[0].Ranges["TDA"].HourAcc + Terminal.Properties.played_amount;
        this.Items[0].Ranges["TDA"].HourAvg = Math.abs(_last_hour_acc - this.Items[0].Ranges["TDA"].HourAcc);
      }
      break;

    case 14: // Played
      var _last_hour_acc = 0;

        if (this.Items[0].Ranges["TDL"]!= undefined) {
          _last_hour_acc = parseFloat(this.Items[0].Ranges["TDL"].HourAcc);
        } else {
          _last_hour_acc = parseFloat(this.Items[0].Ranges["TDA"].DayAcc);
        }

      if (Terminal.Properties.played_count > 0) {
        this.Items[0].Ranges["TDA"].HourNum = this.Items[0].Ranges["TDA"].HourNum +1;
        this.Items[0].Ranges["TDA"].HourMin = 0;
        this.Items[0].Ranges["TDA"].HourMax = 0;
        this.Items[0].Ranges["TDA"].HourAcc = this.Items[0].Ranges["TDA"].HourAcc + Terminal.Properties.played_count;
        this.Items[0].Ranges["TDA"].HourAvg = Math.abs(_last_hour_acc -this.Items[0].Ranges["TDA"].HourAcc);
      }
      break;

    case 6: // NEW CLIENTS
      if (Terminal.Properties.Created == 1) {
        this.Items[0].Ranges["TDA"].HourNum = this.Items[0].Ranges["TDA"].HourNum + 1;
      }
      break;

      //case 7: // HIGH ROLLERS
      //  this.Items[0].Ranges["TDA"].HourNum = 0;
      //  this.Items[0].Ranges["TDA"].HourMin = 0;
      //  this.Items[0].Ranges["TDA"].HourMax = 0;
      //  this.Items[0].Ranges["TDA"].HourAcc = 0;
      //  this.Items[0].Ranges["TDA"].HourAvg = 0;
      //  break;

      //case 8: // PROMOTIONS ASSIGNED
      //  this.Items[0].Ranges["TDA"].HourNum = 0;
      //  this.Items[0].Ranges["TDA"].HourMin = 0;
      //  this.Items[0].Ranges["TDA"].HourMax = 0;
      //  this.Items[0].Ranges["TDA"].HourAcc = 0;
      //  this.Items[0].Ranges["TDA"].HourAvg = 0;
      //  break;

    case 9: // NET WIN
      var _last_hour_acc = 0;

        if (this.Items[0].Ranges["TDL"] != undefined) {
          _last_hour_acc = parseFloat(this.Items[0].Ranges["TDL"].HourAcc);
        } else {
          _last_hour_acc = parseFloat(this.Items[0].Ranges["TDA"].DayAcc);
        }

      if (Terminal.Properties.played_amount > 0) {
        this.Items[0].Ranges["TDA"].HourNum = this.Items[0].Ranges["TDA"].HourNum + 1;
        this.Items[0].Ranges["TDA"].HourMin = 0;
        this.Items[0].Ranges["TDA"].HourMax = 0;
        this.Items[0].Ranges["TDA"].HourAcc = this.Items[0].Ranges["TDA"].HourAcc + Terminal.Properties.net_win;
        this.Items[0].Ranges["TDA"].HourAvg = Math.abs(_last_hour_acc - this.Items[0].Ranges["TDA"].HourAcc) / this.Items[0].Ranges["TDA"].HourNum;
      }
      break;

    case 10: // THEORETICAL WIN
      this.Items[0].Ranges["TDA"].HourNum = 0;
      this.Items[0].Ranges["TDA"].HourMin = 0;
      this.Items[0].Ranges["TDA"].HourMax = 0;
      this.Items[0].Ranges["TDA"].HourAcc = 0;
      this.Items[0].Ranges["TDA"].HourAvg = 0;
      break;

    case 11: // LOSS
      if (Terminal.Properties.net_win < 0) {
        this.Items[0].Ranges["TDA"].HourNum = this.Items[0].Ranges["TDA"].HourNum + 1;
        this.Items[0].Ranges["TDA"].HourMin = 0;
        this.Items[0].Ranges["TDA"].HourMax = 0;
        this.Items[0].Ranges["TDA"].HourAcc = this.Items[0].Ranges["TDA"].HourAcc + Terminal.Properties.net_win;
        this.Items[0].Ranges["TDA"].HourAvg = this.Items[0].Ranges["TDA"].HourAcc / this.Items[0].Ranges["TDA"].HourNum;
      }
      break;

    default:
      break;
  }

}
//------------------------------------------------------------------------------

// Meter update site Meter.
WSIMeter.prototype.UpdateSiteMeterNow = function (_meter_now) {
  switch (+this.Id) {
    case 4: // OCCUPATION
    case 1: // Bet AVG
      this.Items[0].Ranges["TDA"].HourAvg = _meter_now;
      break
    default:
      break;
  }
}

//------------------------------------------------------------------------------

// Meters class constructor
function WSIMetersManager() {
  this.Meters = [];
  this.MetersNow = [];
  this.ByMeterName = [];
  this.WeatherMeter = [];
}

// Method to add a meter to the meters list
WSIMetersManager.prototype.AddMeter = function (Meter) {

  var _meter_id = Meter[0],
      _meter_item_id = Meter[1],
      _meter_type = Meter[2],
      _meter_desc = Meter[3],
      _meter_last_update = Meter[4];


  if (!this.Meters[_meter_id]) {
    this.Meters[_meter_id] = new WSIMeter();
  }

  var _meter = this.Meters[_meter_id];

  _meter.Id = _meter_id;

  if (!_meter.Items[_meter_item_id]) {
    _meter.Items[_meter_item_id] = new WSIMeterItem();
  }

  var _meter_item = _meter.Items[_meter_item_id];

  _meter_item.Id = _meter_item_id;
  _meter_item.Type = _meter_type;
  _meter_item.Description = _meter_desc;
  _meter_item.LastUpdate = _meter_last_update;

}

// Method to add a list of meters to the meters list
WSIMetersManager.prototype.AddMeters = function (MetersList) {
  if (MetersList) {
    for (var _meter in MetersList) {
      this.AddMeter(MetersList[_meter]);
      this.ByMeterName[MetersList[_meter][3]] = MetersList[_meter][0];
    }
  }
}

// Parse the data of a meter to set it in the meters list
WSIMetersManager.prototype.AddMeterData = function (MeterData) {
  try {
    var _meter_range = MeterData[0], //RANGE
        _meter_hour = MeterData[1], //HOUR
        _meter_date = MeterData[2], //DATE
        _meter_weekday = MeterData[3], //WEEKDAY
        _meter_filter = MeterData[4], // Site
        _meter_id = MeterData[5],//METER ID
        _meter_item_id = MeterData[6], //
        _meter_h_min = MeterData[7],
        _meter_h_max = MeterData[8],
        _meter_h_acc = MeterData[9],
        _meter_h_avg = MeterData[10],
        _meter_h_num = MeterData[11],
        _meter_d_min = MeterData[12],
        _meter_d_max = MeterData[13];
    _meter_d_acc = MeterData[14],
    _meter_d_avg = MeterData[15],
    _meter_d_num = MeterData[16],
    _today = GetDeltaDate();

    var _meter = this.Meters[_meter_id];
    var _meter_item = _meter.Items[_meter_item_id];

    var _meter_item_range = {};

    if (!_meter_item.Ranges[_meter_range]) {
      if (_meter_range != "TDA") {
        _meter_item.Ranges[_meter_range] = new WSIMeterItemRange(_meter_range);
        _meter_item_range = _meter_item.Ranges[_meter_range];
      } else {
        var _date_diff = DeltaDateDiff(_today, _meter_date);
        _meter_range = _meter_range + ((_date_diff < 0) ? _date_diff : '');
        _meter_item.Ranges[_meter_range] = new WSIMeterItemRange(_meter_range);
        _meter_item_range = _meter_item.Ranges[_meter_range];
      }

    }

    _meter_item_range.Hour = _meter_hour;
    _meter_item_range.Date = _meter_date;
    _meter_item_range.WeekDay = _meter_weekday;
    _meter_item_range.SiteId = _meter_filter;
    _meter_item_range.MeterId = _meter_id;
    _meter_item_range.MeterItem = _meter_item_id;
    _meter_item_range.HourMin = parseCurrency(_meter_h_min);// / 100;
    _meter_item_range.HourMax = parseCurrency(_meter_h_max);// / 100;
    _meter_item_range.HourAcc = parseCurrency(_meter_h_acc);// / 100;
    _meter_item_range.HourAvg = parseCurrency(_meter_h_avg);// / 100;
    _meter_item_range.HourNum = _meter_h_num;// / 100;
    _meter_item_range.DayMin = parseCurrency(_meter_d_min);// / 100;
    _meter_item_range.DayMax = parseCurrency(_meter_d_max);// / 100;
    _meter_item_range.DayAcc = parseCurrency(_meter_d_acc);// / 100;
    _meter_item_range.DayAvg = parseCurrency(_meter_d_avg);// / 100;
    _meter_item_range.DayNum = _meter_d_num;// / 100;

    this.Meters[_meter_id].Items[_meter_item_id].Ranges[_meter_range] = _meter_item_range;

  } catch (_ex) {
    WSILogger.Write(_ex);
  }

}

WSIMetersManager.prototype.AddMeterNowData = function (MetersData) {

  try {
    var _meter_id = MetersData[0],
    _meter_value = MetersData[1]

    var _meter_now = new WSIMeterNow();

    _meter_now.Id = _meter_id;
    _meter_now.Value = parseCurrency(_meter_value);

    this.MetersNow[_meter_id] = _meter_now;

  } catch (_ex) {
    WSILogger.Write(_ex);
  }
}

// Adds a set of meter data to the meter
WSIMetersManager.prototype.AddMetersData = function (MetersData) {
  if (MetersData) {
    for (var _data in MetersData) {
         this.AddMeterData(MetersData[_data]);
    }
  }
}

// Adds a set of meter data to the meter
WSIMetersManager.prototype.AddMetersNowData = function (MetersData) {
  if (MetersData) {
    for (var _data in MetersData) {
      this.AddMeterNowData(MetersData[_data]);
    }
  }
}


// Return a value of the specified meter
WSIMetersManager.prototype.GetMeterValue = function (MeterId, MeterItem, MeterRange, MeterValue) {

  var _meter = this.Meters[MeterId];

  if (_meter) {
    return _meter.GetValue(MeterItem, MeterRange, MeterValue);
  } else {
    return '';
  }
}

// Return a value of the specified meter
WSIMetersManager.prototype.GetMeterValueByName = function (MeterName, MeterItem, MeterRange, MeterValue) {

  var _meter = this.Meters[this.ByMeterName[MeterName]];

  if (_meter) {
    return _meter.GetValue(MeterItem, MeterRange, MeterValue);
  } else {
    return '';
  }
}

// Reset activity values to meters
WSIMetersManager.prototype.ResetMetersNow = function () {
  for (var _meter in this.Meters) {
    this.Meters[_meter].ResetMeterNow();
  }
}

// Updates activity values to meters for a terminal
WSIMetersManager.prototype.UpdateMetersNow = function (Terminal) {
  // Update all meters for a terminal
  for (var _meter in this.Meters) {
    this.Meters[_meter].UpdateMeterNow(Terminal);
  }

}

WSIMetersManager.prototype.UpdateSiteMetersNow = function () {

  for (var _meter in this.MetersNow) {
    if (this.Meters[_meter])
      this.Meters[_meter].UpdateSiteMeterNow(this.MetersNow[_meter].Value);
  }
  if (this.Meters.length > 0)
  {
    if (this.Meters[12].Items[0].Ranges["TDA"].HourAvg
      && this.Meters[14].Items[0].Ranges["TDA"].HourAvg
      && this.Meters[14].Items[0].Ranges["TDA"].HourAvg > 0)
    {
      this.Meters[1].Items[0].Ranges["TDA"].HourAvg = this.Meters[12].Items[0].Ranges["TDA"].HourAvg / this.Meters[14].Items[0].Ranges["TDA"].HourAvg;
    }
    //else {
    //  this.Meters[1].Items[0].Ranges["TDA"].HourAvg = 0;
    //}
  }

}

