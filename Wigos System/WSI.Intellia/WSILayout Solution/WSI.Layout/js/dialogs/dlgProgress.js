﻿/*
 Requires: 
    - jQuery
    - jQuery-ui
*/

// Global dialog variable
var m_dlg_progress = null;

// Variables

/////////////////////////////////////////////////////////////////////////////////////////////
// Dialog cretion

function LayoutProgressCreate() {
  var _string_dialog = '';

  var _string_title = _Language.get("NLS_PLEASE_WAIT")

  _string_dialog = _string_dialog + '<div id="dialog_layout_progress" title="' + _string_title + '" style="overflow: none;">';
  _string_dialog = _string_dialog + '<image src="design/img/loading2.gif" alt="" />';
  _string_dialog = _string_dialog + '</div>';

  $("#editor_dialogs").append(_string_dialog);
}

/////////////////////////////////////////////////////////////////////////////////////////////

// Dialog opener
function LayoutProgressOpen() {
  m_dlg_progress = null;
  if (m_dlg_progress == null) {
    // Create Dialog
    LayoutProgressCreate();

    // Prepare Dialog
    m_dlg_progress = $("#dialog_layout_progress").dialog({
      dialogClass: "no-close progress",
      autoOpen: false,
      height: 80,
      width: 330,
      modal: true,
      buttons: null,
    });
  }
  if (!m_dlg_progress.dialog("isOpen")) {
    m_dlg_progress.dialog("open");
  }
}

// Dialog closer
function LayoutProgressClose() {
  if (m_dlg_progress.dialog("isOpen")) {
    m_dlg_progress.dialog("close");
    m_dlg_progress = null;
  }
}

/////////////////////////////////////////////////////////////////////////////////////////////

// Dialog functions

/////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////

// Initialization
$(document).ready(function () {


});