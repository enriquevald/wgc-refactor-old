﻿var _table_terminals = new TableListBase(".tablaTerminales");
var _table_terminals_compare = new TableListBase(".tablaTerminales");
var m_tar_compare_list = [];
var m_tar_filter = "";

//************************************************************************************************************
//LISTA
//************************************************************************************************************
function WSITerminalsAtRoom_Open() {

    if ($(".wrapJugadores.terminalMode").children().length == 0) {

        $(".wrapJugadores").empty();
        $(".wrapJugadores").html(WSITerminalsAtRoom_Init());
        $(".wrapJugadores").addClass("terminalMode");

    }

    CreateTableTerminals();
    WSITerminalsAtRoom_Init_Events();
    m_tar_filter = "";

    _Language.parse();

}

function WSITerminalsAtRoom_Init() {
    var _result = '';

    _result += '<div class="headerTablas">';
    _result += '  <div class="cierreLista">';
    _result += '    <a href="#" data-nls="NLS_CLOSE_LIST">' + _Language.get('NLS_CLOSE_LIST') + '</a>';
    _result += '  </div>';
    _result += '  <div class="terminalesSearchBox">';
    _result += '    <input class="srcTerminalPanel" id="inputTerminalListasrc" type="text" data-nls="NLS_TERMINALS_SEARCH_PLACEHOLDER" placeholder="" value="">';
    _result += '    <div class="btnBuscarTerminal" id="btnBuscarTerminalLista">';
    _result += '      <a href="#" data-nls="NLS_SEARCH">' + _Language.get('NLS_SEARCH') + '</a>';
    _result += '    </div>';
    _result += '  </div>';
    _result += "  <div class='selectorAreaTerminal'>"
    _result += "    <select class='selectAreaTerminal' id='selTerminalArea'>";
    _result += "      <option value='' selected data-nls='NLS_AREA'>" + _Language.get('NLS_AREA') + "</option>";

    for (_idx_area_id_sel in _Manager.Terminals.ByArea) {
        _result += "      <option value='" + _idx_area_id_sel + "' disable>" + _idx_area_id_sel + "</option>";
    }

    _result += "    </select>";
    _result += "  </div>";
    _result += "  <div class='selectorProveedorTerminales'>"
    _result += "    <select class='selectProveedorTerminal' id='selTerminalProvider'>";
    _result += "      <option value='' selected data-nls='NLS_PROVIDER'>" + _Language.get('NLS_PROVIDER') + "</option>";

    for (_idx_provider_sel in _Manager.Terminals.ByProvider) {
        _result += "      <option value='" + _idx_provider_sel + "' disable>" + _idx_provider_sel + "</option>";
    }

    _result += "    </select>";
    _result += "  </div>";
    _result += "  <div class='iraComparar' id='terminalesListaIraComparar' " + (m_tar_compare_list.length > 1 ? "" : "style='display:none;'") + " data-nls='NLS_COMPARE_TERMINALS'>" + _Language.get('NLS_COMPARE_TERMINALS');
    _result += "  </div>";
    _result += "</div>";
    _result += '<div class="insideTerminales">  ';
    _result += '  <div class="tablaTerminales">';
    _result += '  </div>';
    _result += '</div>';

    return _result;
}

function WSITerminalsAtRoom_Init_Events() {
    //Cierre lista jugadores
    $('.cierreLista a').click(function () {
        cerrarListaTerminales();
        $(".wrapJugadores").removeClass("terminalMode");
    });

    $("#btnBuscarTerminalLista").click(function () {

        m_tar_filter = $("#inputTerminalListasrc").val();

        WSITerminalsAtRoom_GetFilters();
    });

    $("#terminalesListaIraComparar").click(function () {
        cerrarListaTerminales(function () { mostrarCompararTerminales2(); });
        $(".wrapJugadores").removeClass("terminalMode");
    })
}

function mostrarCompararTerminales2() {
    ocultarSlideButton();
    ocultarPanelControl();
    WSITerminalsAtRoom_Compare_Open();
    $('.wrapJugadores').css('display', 'block');
    $('.wrapJugadores').animate({
        right: '0'
    });
    $('.wrapJugadores').removeClass('show').addClass('hide');
}
function ocultarCompararTerminales() {
    $('.wrapJugadores').animate({
        right: '-100%'
    }, 300, function () {
        $('.wrapJugadores').css('display', 'none');
    });
    mostrarSlideButton();
}



function WSITerminalsAtRoom_LoadData() {
    var _list = [];

    for (_idx_account in _Manager.Terminals.ByAccount) {
        _terminal = _Manager.Terminals.Items[_Manager.Terminals.ByAccount[_idx_account]];

        //SL 1/11 - RETIRED TERMINALS NOT SHOW IN LIST
        if (_terminal.Properties.status != 2) {
          var terminalName = '';
          if (_terminal.Properties.name != undefined) {
              terminalName = _terminal.Properties.name.toUpperCase();
          }

          if (m_tar_filter == "" || terminalName.startsWith(m_tar_filter.toUpperCase())) {
              var _item = {
                  terminal_id: _terminal.Properties.external_id,
                  area_id: _terminal.Properties.area_id,
                  provider_name: _terminal.Properties.provider_name,
                  play_session_bet_average: _terminal.Properties.play_session_bet_average,
                  play_session_total_played: _terminal.Properties.play_session_total_played,
                  won_amount: _terminal.Properties.won_amount,
                  terminal_name: _terminal.Properties.name,
                  id: _terminal.Properties.external_id
              }
              _list.push(_item);
          }
        }
    }

    return _list
}

function WSITerminalsAtRoom_Refresh() {
    _table_terminals.rows = WSITerminalsAtRoom_LoadData();
    _table_terminals.Refresh();
}

function CreateTableTerminals() {
    //Field, IsPrimaryKey, Visible, Title, Width, Display, ColumnType, Sortable
    _table_terminals.AddColumn("id", true, false, "", "0%", "", "text", true);
    _table_terminals.AddColumn("terminal_id", false, true, "NLS_TERMINAL", "10%", WSITerminalsAtRoom_PrintRowTerminalId, "text", true);
    _table_terminals.AddColumn("area_id", false, true, "NLS_AREA", "5%", "", "text", true);
    _table_terminals.AddColumn("provider_name", false, true, "NLS_PROVIDER", "8%", "", "text", true);
    _table_terminals.AddColumn("play_session_bet_average", false, true, "NLS_BETAV", "8%", WSITerminalsAtRoom_PrintRowFormatBetAverage, "text", true);
    _table_terminals.AddColumn("play_session_total_played", false, true, "NLS_PLAYED", "8%", WSITerminalsAtRoom_PrintRowFormatTotalPlayed, "text", true);
    _table_terminals.AddColumn("won_amount", false, true, "NLS_WON", "8%", WSITerminalsAtRoom_PrintRowFormatWonAmount, "text", true);
    _table_terminals.AddColumn("actions", false, true, "NLS_INFO", "5%", WSITerminalsAtRoom_PrintRowInfo, "text", false);
    _table_terminals.AddColumn("Keeper", false, true, "NLS_LOCATE", "5%", WSITerminalsAtRoom_PrintRowLocateCustomerAtRoom, "text", false);
    _table_terminals.AddColumn("compare", false, true, "NLS_COMPARE", "5%", WSITerminalsAtRoom_PrintRowCompare, "text", false);


    _table_terminals.rows = WSITerminalsAtRoom_LoadData();
    _table_terminals.paging = true;
    _table_terminals.allowDelete = false;
    _table_terminals.sortValue = "terminal_id ASC";
    _table_terminals.defaultSorting = "terminal_id ASC";
    // var cantidad = _table_terminals.rows.length;
    _table_terminals.CreateTable();
}

//ROW ALARMS Events**************
function WSITerminalsAtRoom_PrintRowLocateCustomerAtRoom(data) {
    var $control = $("<a class='btnLocalizarAlarma' data-terminalid='" + data.record.terminal_id + "' href='#' data-nls='NLS_LOCATE_TERMINAL'>" + _Language.get('NLS_LOCATE_TERMINAL') + "</a>");

    $control.click(function (evt) {
        _UI.SetMenuActiveSection($("#a_section_70")[0], 70);
        ShowTerminalInfo(_Manager.Terminals.Items[_Manager.Terminals.ByTerminal[$(evt.currentTarget).data("terminalid")]], 1, false);
    });

    return $control;
}

function WSITerminalsAtRoom_PrintRowTerminalId(data) {
    var _result = "";

    _result += data.record.terminal_name + " {ID:" + data.record.terminal_id + "}";

    return _result;
}

function WSITerminalsAtRoom_PrintRowFormatBetAverage(data) {
    return FormatString(data.record.play_session_bet_average, EN_FORMATS.F_MONEY)
}

function WSITerminalsAtRoom_PrintRowFormatTotalPlayed(data) {
    return FormatString(data.record.play_session_total_played, EN_FORMATS.F_MONEY)
}

function WSITerminalsAtRoom_PrintRowFormatWonAmount(data) {
    return FormatString(data.record.won_amount, EN_FORMATS.F_MONEY)
}

function WSITerminalsAtRoom_PrintRowCompare(data) {
    var _compare_class = "";

    if ($.inArray(data.record.terminal_id, m_tar_compare_list) == -1) {
        _compare_class = "btnCompararTerminales";
    } else {
        _compare_class = "btnCompararTerminalesSel";
    }

    var _$result = $("<div class='" + _compare_class + "' data-terminalid='" + data.record.terminal_id + "'></div>");


    _$result.click(function (evt) {
        var _terminal = _Manager.Terminals.Items[_Manager.Terminals.ByTerminal[$(evt.currentTarget).data("terminalid")]];
        var _idx = $.inArray(_terminal.Properties.lo_external_id, m_tar_compare_list);
        if (_idx == -1) {
            m_tar_compare_list.push(_terminal.Properties.lo_external_id);
            $(this).removeClass("btnCompararTerminales");
            $(this).addClass("btnCompararTerminalesSel");
        }
        else {
            m_tar_compare_list.splice(_idx, 1);

            $(this).addClass("btnCompararTerminales");
            $(this).removeClass("btnCompararTerminalesSel");
        }


        if (m_tar_compare_list.length > 1) {
            $("#terminalesListaIraComparar").show();
        } else {
            $("#terminalesListaIraComparar").hide();
        }
    })

    return _$result;
}

function WSITerminalsAtRoom_PrintRowInfo(data) {
    var $item = $("<div data-terminalid='" + data.record.terminal_id + "' class='icoSnapshot' />")

    $item.click(function (evt) {
        var _terminal_id = _Manager.Terminals.Items[_Manager.Terminals.ByTerminal[$(evt.currentTarget).data("terminalid")]];

        m_terminal_id_selected = _terminal_id.Properties.lo_id;
        UpdatePopupTerminal(null, _terminal_id);
        OpenInfoPanel();
    })

    return $item;
}

function WSITerminalsAtRoom_GetFilters() {
    var _tar_filter_list = [];
    var _cb_terminal_provider = document.getElementById("selTerminalProvider");
    var _cb_terminal_area = document.getElementById("selTerminalArea");
    var _terminal_provider_value = _cb_terminal_provider.options[_cb_terminal_provider.selectedIndex].value;
    var _terminal_area_value = _cb_terminal_area.options[_cb_terminal_area.selectedIndex].value;

    if (_terminal_provider_value != "") {
        _tar_filter_list["provider_name"] = _terminal_provider_value;
    }

    if (_terminal_area_value != 0) {
        _tar_filter_list["area_id"] = _terminal_area_value;
    }

    _table_terminals.ApplyFilter(_tar_filter_list);

    WSITerminalsAtRoom_Refresh();
}

function WSITerminalsAtRoom_Refresh() {
    _table_terminals.rows = WSITerminalsAtRoom_LoadData();
    _table_terminals.Refresh();
}

//************************************************************************************************************
//COMPARAR
//************************************************************************************************************

function WSITerminalsAtRoom_Compare_Open() {

    if ($(".wrapJugadores.terminalMode").children().length == 0) {
        $(".wrapJugadores").empty();
        $(".wrapJugadores").html(WSITerminalsAtRoom_Compare_Init());
        $(".wrapJugadores").addClass("terminalCompareMode");

    }

    CreateTableCompareTerminals();
    WSITerminalsAtRoom_Compare_Init_Events();
}

function WSITerminalsAtRoom_Compare_Init() {
    var _result = '';

    _result += '<div class="headerTablas">';
    _result += '  <div class="cierreLista">';
    _result += '    <a href="#" data-nls="NLS_CLOSE_LIST">' + _Language.get('NLS_CLOSE_LIST') + '</a>';
    _result += '  </div>';
    _result += "</div>";
    _result += '<div class="insideTerminales">  ';
    _result += '  <div class="tablaTerminales">';
    _result += '  </div>';
    _result += '</div>';

    return _result;
}

function WSITerminalsAtRoom_Compare_Init_Events() {
    //Cierre lista jugadores
    $('.cierreLista a').click(function () {
        cerrarCompararTerminales();
        $(".wrapJugadores").removeClass("terminalCompareMode");
    });
}

function WSITerminalsAtRoom_Compare_LoadData() {
    var _list = [];

    for (_idx_account in m_tar_compare_list) {
        _terminal = _Manager.Terminals.Items[_Manager.Terminals.ByTerminal[m_tar_compare_list[_idx_account]]];

        var _item = {
            terminal_id: _terminal.Properties.external_id,
            area_id: _terminal.Properties.area_id,
            provider_name: _terminal.Properties.provider_name,
            play_session_bet_average: _terminal.Properties.play_session_bet_average,
            play_session_total_played: _terminal.Properties.play_session_total_played,
            won_amount: _terminal.Properties.won_amount,
            terminal_name: _terminal.Properties.name,
            id: _terminal.Properties.external_id
        }
        _list.push(_item);
    }

    return _list
}

function WSITerminalsAtRoom_Compare_Refresh() {
    _table_terminals.rows = WSITerminalsAtRoom_Compare_LoadData();
    _table_terminals.Refresh();
}

function CreateTableCompareTerminals() {
    //Field, IsPrimaryKey, Visible, Title, Width, Display, ColumnType, Sortable
    _table_terminals_compare.AddColumn("id", true, false, "", "0%", "", "text", false);
    _table_terminals_compare.AddColumn("terminal_id", false, true, "NLS_TERMINAL", "5%", WSITerminalsAtRoom_Compare_PrintRowTerminalId, "text", true);
    _table_terminals_compare.AddColumn("area_id", false, true, "NLS_AREA", "5%", "", "text", false);
    _table_terminals_compare.AddColumn("provider_name", false, true, "NLS_PROVIDER", "5%", "", "text", false);
    _table_terminals_compare.AddColumn("play_session_bet_average", false, true, "NLS_BETAV", "5%", WSITerminalsAtRoom_Compare_PrintRowFormatBetAverage, "text", false);
    _table_terminals_compare.AddColumn("play_session_total_played", false, true, "NLS_PLAYED", "5%", WSITerminalsAtRoom_Compare_PrintRowFormatTotalPlayed, "text", true);
    _table_terminals_compare.AddColumn("won_amount", false, true, "NLS_WON", "5%", WSITerminalsAtRoom_Compare_PrintRowFormatWonAmount, "text", false);
    _table_terminals_compare.AddColumn("actions", false, true, "NLS_INFO", "10%", WSITerminalsAtRoom_Compare_PrintRowInfo, "text", false);
    _table_terminals_compare.AddColumn("Keeper", false, true, "NLS_LOCATE", "10%", WSITerminalsAtRoom_Compare_PrintRowLocateCustomerAtRoom, "text", false);

    _table_terminals_compare.rows = WSITerminalsAtRoom_Compare_LoadData();
    _table_terminals_compare.paging = true;
    _table_terminals_compare.allowDelete = true;
    _table_terminals_compare.OnDeleteRow = WSITerminalsAtRoom_Compare_OnDeleteRow;
    _table_terminals_compare.sortValue = "terminal_id ASC";
    _table_terminals_compare.OnDeleteMessage = 'NLS_DELETE_CURRENT_RECORD';


    _table_terminals_compare.CreateTable();
}

//ROW ALARMS Events**************
function WSITerminalsAtRoom_Compare_PrintRowLocateCustomerAtRoom(data) {
    var $control = $("<a class='btnLocalizarAlarma' data-terminalid='" + data.record.terminal_id + "' href='#' data-nls='NLS_LOCATE_TERMINAL'>" + _Language.get('NLS_LOCATE_TERMINAL') + "</a>");

    $control.click(function (evt) {
        _UI.SetMenuActiveSection($("#a_section_70")[0], 70);
        ShowTerminalInfo(_Manager.Terminals.Items[_Manager.Terminals.ByTerminal[$(evt.currentTarget).data("terminalid")]], 1, false);
    });

    return $control;
}

function WSITerminalsAtRoom_Compare_PrintRowTerminalId(data) {
    var _result = "";

    _result += data.record.terminal_name + " {ID:" + data.record.terminal_id + "}";

    return _result;
}

function WSITerminalsAtRoom_Compare_PrintRowFormatBetAverage(data) {
    return "$" + data.record.play_session_bet_average;
}

function WSITerminalsAtRoom_Compare_PrintRowFormatTotalPlayed(data) {
    return "$" + data.record.play_session_total_played;
}

function WSITerminalsAtRoom_Compare_PrintRowFormatWonAmount(data) {
    return "$" + data.record.won_amount;
}

function WSITerminalsAtRoom_Compare_OnDeleteRow(data, Sender, SenderClick) {
  var _index=-1;
  for (var _i in m_tar_compare_list)
  {
    if (m_tar_compare_list[_i] == data)
      _index = _i;
  }

    m_tar_compare_list.splice(_index, 1);

    WSITerminalsAtRoom_Compare_Refresh();
}

function WSITerminalsAtRoom_Compare_PrintRowInfo(data) {
    var $item = $("<div class='icoSnapshot'  data-terminalid='" + data.record.terminal_id + "'/>")

    $item.click(function (evt) {
        var _terminal_id = _Manager.Terminals.Items[_Manager.Terminals.ByTerminal[$(evt.currentTarget).data("terminalid")]];

        m_terminal_id_selected = _terminal_id.Properties.lo_id;
        UpdatePopupTerminal(null, _terminal_id);
        OpenInfoPanel();
    })

    return $item;
}