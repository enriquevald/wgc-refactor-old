﻿JS/Controls

This folder contains API to manage the events produced by user input
(MouseDown, MouseUp, MouseMove, MouseWheel Events...)

OrbitControls.js

	To create: on WSI.Init() -> this.controls = new THREE.OrbitControls(this.camera, this.renderer.domElement);

TrackballControls.js

	To create: on WSI.Init() -> this.controls = new THREE.TrackballControls(this.camera, this.renderer.domElement);

* Don't forget to change script include in Default.aspx page: <script type="text/jscript" src="js/controls/????????.js"></script>

