﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: filename
// 
//   DESCRIPTION: Description.
// 
//        AUTHOR: Author
// 
// CREATION DATE: Date
// 
//------------------------------------------------------------------------------

var EN_CONTROLS_MODE = {
  CM_PERSPECTIVE: 0,
  CM_CENIT: 1,
  CM_FIRST_PERSON: 2
};

function WSIControlsManager() {
  this.CurrentMode = EN_CONTROLS_MODE.CM_PERSPECTIVE;
  this.Controls = [];

  var _that = this;

  // Init

  // 3D
  this.Controls.push(new THREE.OrbitControls(_Manager.Cameras.Cameras[EN_CAMERA_MODE.CM_PERSPECTIVE], WSI.renderer.domElement));
  this.Controls[EN_CONTROLS_MODE.CM_PERSPECTIVE].enabled = true; // Important!
  this.Controls[EN_CONTROLS_MODE.CM_PERSPECTIVE].maxPolarAngle = Math.PI / 2;
  this.Controls[EN_CONTROLS_MODE.CM_PERSPECTIVE].minDistance = 10;
  this.Controls[EN_CONTROLS_MODE.CM_PERSPECTIVE].maxDistance = 450;
  this.Controls[EN_CONTROLS_MODE.CM_PERSPECTIVE].noZoom = false;
  this.Controls[EN_CONTROLS_MODE.CM_PERSPECTIVE].zoomSpeed = -0.1;
  this.Controls[EN_CONTROLS_MODE.CM_PERSPECTIVE].autoRadius = false;
  
  // 2D
  this.Controls.push(new THREE.OrbitControls(_Manager.Cameras.Cameras[EN_CAMERA_MODE.CM_CENIT], WSI.renderer.domElement));
  this.Controls[EN_CONTROLS_MODE.CM_CENIT].enabled = false; // Important!
  this.Controls[EN_CONTROLS_MODE.CM_CENIT].noRotate = true;
  this.Controls[EN_CONTROLS_MODE.CM_CENIT].minDistance = 10;
  this.Controls[EN_CONTROLS_MODE.CM_CENIT].maxDistance = 450;
  this.Controls[EN_CONTROLS_MODE.CM_CENIT].noZoom = false;
  this.Controls[EN_CONTROLS_MODE.CM_CENIT].autoRadius = false;

  // FP
  this.Controls.push(new WSIJoysticks());
  this.Controls[EN_CONTROLS_MODE.CM_FIRST_PERSON].LeftAxisEnd = function () {
    this.MouseControls.moveForward = false;
    this.MouseControls.moveBackward = false;
    this.MouseControls.moveLeft = false;
    this.MouseControls.moveRight = false;
  }
  this.Controls[EN_CONTROLS_MODE.CM_FIRST_PERSON].LeftVerticalAxisActions = function (JoyStick, Delta) {
    if (JoyStick.up()) {
      this.MouseControls.moveForward = true;
      this.MouseControls.moveBackward = false;
    }
    if (JoyStick.down()) {
      this.MouseControls.moveForward = false;
      this.MouseControls.moveBackward = true;
    }
  };
  this.Controls[EN_CONTROLS_MODE.CM_FIRST_PERSON].LeftHorizontalAxisActions = function (JoyStick, Delta) {
    if (JoyStick.left()) {
      this.MouseControls.moveLeft = true;
      this.MouseControls.moveRight = false;
    }
    if (JoyStick.right()) {
      this.MouseControls.moveLeft = false;
      this.MouseControls.moveRight = true;
    }
  };

  this.Controls[EN_CONTROLS_MODE.CM_FIRST_PERSON].RightHorizontalAxisActions = function (JoyStick, Delta) {
    if (JoyStick.left()) {
      this.MouseControls.mouseX = JoyStick.deltaX();
    }
    if (JoyStick.right()) {
      this.MouseControls.mouseX = JoyStick.deltaX();
    }
  };


  this.CurrentControls = function () {
    return this.Controls[this.CurrentMode];
  }

  this.SetCurrentMode = function (Mode) {
    this.CurrentMode = Mode;

    if (Mode == EN_CONTROLS_MODE.CM_FIRST_PERSON) {
      this.Controls[EN_CONTROLS_MODE.CM_FIRST_PERSON].Init(WSI.container);
      if (this.Controls[EN_CONTROLS_MODE.CM_FIRST_PERSON].MouseControls != null) {
        if ('ontouchstart' in window) {
          // Don't create events for controller in FP in touch screens
        } else {
          this.Controls[EN_CONTROLS_MODE.CM_FIRST_PERSON].MouseControls.CreateEvents();
        }
      }
    } else {
      this.Controls[EN_CONTROLS_MODE.CM_FIRST_PERSON].Destroy();
    }

    for (var _c in this.Controls) { this.Controls[_c].enabled = false; }
    this.Controls[Mode].enabled = true;
  }

  this.UpdateControls = function (Delta) {
    switch (this.CurrentMode) {
      case EN_CONTROLS_MODE.CM_FIRST_PERSON:
        if ('ontouchstart' in window) {
          this.Controls[this.CurrentMode].Update(Delta);
          this.Controls[this.CurrentMode].MouseControls.update(Delta);
        } else {
          this.Controls[this.CurrentMode].MouseControls.update(Delta);
        }
        break; 
      case EN_CONTROLS_MODE.CM_PERSPECTIVE:
      case EN_CONTROLS_MODE.CM_CENIT:
        this.Controls[this.CurrentMode].update();
        break;
    }
  }
}

// ---------------------------------------------------------------------------------------------------------------------------------------

/*
 * 
 */
function WSIJoysticks() {
  this.enabled = true;
  this.RestricedHeightPercent = 20;
  this.domElement = null;
  this.LeftController = null;
  this.RightController = null;
  this.LeftVerticalAxisActions = undefined;
  this.LeftHorizontalAxisActions = undefined;
  this.LeftAxisEnd = undefined;
  this.RightVerticalAxisActions = undefined;
  this.RightHorizontalAxisActions = undefined;
  this.RightAxisEnd = undefined;
  this.LeftRadius = 100;
  this.RightRadius = 100;
  this.MouseControls = null;
}

WSIJoysticks.prototype.Init = function (DOMElement) {
  var _that = this;
  this.domElement = DOMElement;

  this.LeftController = new VirtualJoystick({
    container: this.domElement,
    strokeStyle: 'yellow',
    limitStickTravel: true,
    stickRadius: this.LeftRadius
  });
  this.LeftController.addEventListener('touchStartValidation', function (event) {
    var _height = $(_that.domElement).height();
    var _valid_height = _height * (_that.RestricedHeightPercent / 100);
    var touch = event.changedTouches[0];
    if (touch.pageY < _height - _valid_height) return false;
    if (touch.pageX >= window.innerWidth / 2) return false;
    return true;
  });
  this.LeftController.addEventListener('touchEnd', function () {
    if (_that.LeftAxisEnd) {
      _that.LeftAxisEnd();
    }
  });

  this.RightController = new VirtualJoystick({
    container: this.domElement,
    strokeStyle: 'white',
    limitStickTravel: true,
    stickRadius: this.RightRadius
  });
  this.RightController.addEventListener('touchStartValidation', function (event) {
    var _height = $(_that.domElement).height();
    var _valid_height = _height * (_that.RestricedHeightPercent / 100);
    var touch = event.changedTouches[0];
    if (touch.pageY < _height - _valid_height) return false;
    if (touch.pageX < window.innerWidth / 2) return false;
    return true;
  });
  this.RightController.addEventListener('touchEnd', function () {
    if (_that.RightAxisEnd) {
      _that.RightAxisEnd();
    }
  });

  this.RightAxisEnd = function () {
    _that.MouseControls.mouseX = 0;
  }

  this.MouseControls = new THREE.FirstPersonControls(_Manager.Cameras.Cameras[EN_CAMERA_MODE.CM_FIRST_PERSON], this.domElement);
  this.MouseControls.movementSpeed = 20;
  this.MouseControls.lookSpeed = 0.3;
  this.MouseControls.noFly = true;
  this.MouseControls.lookVertical = false;
  
}

WSIJoysticks.prototype.Destroy = function () {
  if (this.LeftController != null) {
    this.LeftController.destroy();
    this.LeftController = null;
  }
  if (this.RightController != null) {
    this.RightController.destroy();
    this.RightController = null;
  }
  this.enabled = false;

  if (this.MouseControls != null) {
    this.MouseControls.RemoveEvents();
    this.MouseControls = null;
  }
}

WSIJoysticks.prototype.SetRadius = function (Radius) {
  this.Radius = Radius;
  this.LeftController.stickRadius = this.Radius;
  this.RightController.stickRadius = this.Radius;
}

WSIJoysticks.prototype.Update = function (Delta) {
  if (this.enabled) {
    // Left Joy
    if (this.LeftVerticalAxisActions) { this.LeftVerticalAxisActions(this.LeftController, Delta); }
    if (this.LeftHorizontalAxisActions) { this.LeftHorizontalAxisActions(this.LeftController, Delta); }
    // Right Joy
    if (this.RightVerticalAxisActions) { this.RightVerticalAxisActions(this.RightController, Delta); }
    if (this.RightHorizontalAxisActions) { this.RightHorizontalAxisActions(this.RightController, Delta); }
  }
}

function onZoomIn() {
  _Manager.Controls.Controls[EN_CONTROLS_MODE.CM_PERSPECTIVE].onZoomIn();
}

function onZoomOut() {
  _Manager.Controls.Controls[EN_CONTROLS_MODE.CM_PERSPECTIVE].onZoomOut();
}

function onResetZoom() {
  _Manager.Controls.Controls[EN_CONTROLS_MODE.CM_PERSPECTIVE].reset();
}