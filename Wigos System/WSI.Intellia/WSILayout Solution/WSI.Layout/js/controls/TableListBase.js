﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: TableListBase
// 
//   DESCRIPTION: Implementation of JTable.
// 
//        AUTHOR: Jaume Barnés
// 
// CREATION DATE: 07/07/2015
// 
//------------------------------------------------------------------------------

TableListBase.prototype.constructor = TableListBase;

function TableListBase(Container) {
  this.Container = Container;

  this.title = "";
  this.paging = false;
  this.pageSize = 10; //Set page size (default value)
  this.sorting = true; //Enable sorting
  this.multiSorting = true,
  this.defaultSorting = "";
  this.columns = {};
  this.rows = [];
  this.allowDelete = true;
  this.OnDeleteRow;
  this.sortValue = "";
  this.rowsHide = true;
  this.Filters = [];
  this.rowChildExpandedId = null;
  this.AfterLoadRecords;
  this.child_HTML = "";
  this.last_chid_id = null;
  this.OnDeleteMessage = "NLS_DELETE_CURRENT_RECORD";
};

TableListBase.prototype.Refresh = function (Reload) {

  try {
    var _container = this.Container;
    this.currentEvent = "refresh";
    this.currentPageSelected = $(this.Container).find(".jtable-page-number-active").val();
    if (this.currentPageSelected == undefined || this.currentPageSelected == "") {
      this.currentPageSelected = $(this.Container).find(".jtable-page-number-active").text();
    }
    //rowChildExpandedId
    if (this.rowChildExpandedId) {
      for (_idx_tlb_rows in this.rows) {
        if (this.rows[_idx_tlb_rows].id == this.rowChildExpandedId) {
          this.rows[_idx_tlb_rows].hasChild = this.rowChildExpandedId;
          if (this.last_chid_id != this.rowChildExpandedId) {
            //    if (this.child_HTML != "" && $(this.Container + " .jtable-child-row").length > 1) {
            //      this.rows[_idx_tlb_rows].childHTML = $(this.Container + " .jtable-child-row :visible")[1].outerHTML;
            //      this.last_chid_id = this.rowChildExpandedId;
            //      this.child_HTML = $(this.Container + " .jtable-child-row")[1].outerHTML;
            //  } else {
            //      this.rows[_idx_tlb_rows].childHTML = $(this.Container + " .jtable-child-row")[0].outerHTML;
            //      this.last_chid_id = this.rowChildExpandedId;
            //      this.child_HTML = $(this.Container + " .jtable-child-row")[0].outerHTML;
            //}
            this.rows[_idx_tlb_rows].childHTML = $(this.Container + " .jtable-child-row:visible")[0].outerHTML;
            this.last_chid_id = this.rowChildExpandedId;
            this.child_HTML = $(this.Container + " .jtable-child-row:visible")[0].outerHTML;

          } else {
            this.rows[_idx_tlb_rows].childHTML = this.child_HTML;
          }
          break;
        }
      }
    }
    $(this.Container).jtable('load', this.rows, function () { TableListBase_Refresh_DataRows(_container); });
    this.currentEvent = "none";
  } catch (error) {
    WSILogger.Write("TableListBase.Refresh.click() " + error);
  }
}

TableListBase.prototype.CreateTable = function () {
  _table_customers.currentEvent = "load";
  var _that = this;
  var _columns = {
  };

  if (this.allowDelete) {
    _columns = this.columns;

    _columns.delete = {
      list: true,
      sorting: false,
      title: "<a class='iconoDelete'/>",
      width: "5%",
      display: this.WSI_Tasks_ui_DeleteAlarmRow,
      parent: this
    }
  } else {
    _columns = this.columns;
  }

  $(this.Container).jtable({
    title: this.title,
    paging: this.paging,//this.paging,
    pageSize: this.pageSize,
    sorting: this.sorting,
    multiSorting: this.multiSorting,
    defaultSorting: this.defaultSorting,
    actions: {
      listAction: function (Rows, jParams) {
        if (jParams.jtSorting) {
          _that.sortValue = jParams.jtSorting
        }

        if (jParams.jtPageSize) {
          if (_that.currentEvent != "refresh") {
            _that.jtStartIndex = jParams.jtStartIndex
            _that.jtPageSize = jParams.jtPageSize
          }
        }

        //return TableListBase_RefreshJtableData(Rows, _that.sortValue, _that.Filters);
        return TableListBase_RefreshJtableData(Rows, _that);
      }
    },
    fields: _columns
  });

  $(this.Container).jtable('load', this.rows, function () { TableListBase_Refresh_DataRows(_that.Container); });

  _table_customers.currentEvent = "none";

}

TableListBase.prototype.AddColumn = function (Field, IsPrimaryKey, Visible, Title, Width, Display, ColumnType, Sortable) {
  var _str_column_properties,
      _translated_title = _Language.get(Title);

  if (Sortable == true) {
    Title = "<span style='cursor:pointer;' data-nls='" + Title + "' >" + _translated_title + "</span>";
  }
  _str_column_properties = 'this.columns.' + Field + ' = {';
  _str_column_properties += 'key: IsPrimaryKey,';
  _str_column_properties += 'list: Visible,';
  _str_column_properties += 'title: Title,';
  _str_column_properties += 'sorting: Sortable,';

  switch (Type) {
    case "date":
      _str_column_properties += 'type: date,';
      _str_column_properties += 'displayFormat: "yy-mm-dd",';
      break;

    default:
      break;
  }

  _str_column_properties += 'width: Width';

  if (Display != "") {
    _str_column_properties += ',display: Display';
  }
  _str_column_properties += '};';

  eval(_str_column_properties);
}

function TableListBase_RefreshJtableData(Rows, jParams) {
  var _result;
  var _list_after = Rows;
  var _jparams = jParams.sortValue;
  var _paging_init;
  var _paging_size;
  var _list = [];
  var _page_selected = 0;

  _list_after = TableListBase_ApplyFilter(jParams, Rows);

  if (_jparams != "") {
    var _sort = _jparams.split(' ');

    if (_sort[0] == "assigned_user_id") {
      _list_after = _.sortBy(_list_after, "assigned_name");
    } else {
      _list_after = _.sortBy(_list_after, _sort[0]);
    }

    if (_sort[1] == "DESC") {
      _list_after.reverse();
    }
  }

  if (jParams.jtPageSize) {
    if (jParams.currentEvent == "refresh") {
      $(jParams.Container).find(".jtable-goto-page").find(":input").val(parseInt(jParams.currentPageSelected)).trigger('change');
    }

    _paging_init = jParams.jtStartIndex;
    _paging_size = jParams.jtPageSize - 1;

    var _list_after_count = 0;

    for (var _idx_list_after = _paging_init; _idx_list_after <= _list_after.length - 1; _idx_list_after++) {
      if (_list_after_count > _paging_size) {
        break;
      }

      _list.push(_list_after[_idx_list_after]);
      _list_after_count++;
    }

  } else {
    _list = _list_after;
  }

  _result = {
    "Result": "OK", "Records": _list, "TotalRecordCount": _list_after.length
  };

  return _result;
}

//formato Array["nombre_del_campo"] : valor
TableListBase.prototype.ApplyFilter = function (Fields) {
  try {

    this.Filters = Fields;
    _that = this;

    $(this.Container).jtable('load', this.rows, function () { TableListBase_Refresh_DataRows(_that.Container); });

    this.WSI_Tasks_ui_Refresh_DataRows();
  }
  catch (e) {
  }
}

function TableListBase_ApplyFilter(jParams, DataArray) {
  var _result = [];
  var _count = 0;
  var _filter_properties_count = 0;
  var _total_data = 0;
  var _filters = jParams.Filters;

  for (_idx_datarray_filter in DataArray) {
    var _item = DataArray[_idx_datarray_filter];
    _filter_properties_count = 0;
    _count = 0;
    for (_idx_field_apply_filter in _filters) {
      if (_item[_idx_field_apply_filter] == _filters[_idx_field_apply_filter]) {
        _count++;
      }
      _filter_properties_count++;
    }

    if (_filter_properties_count == _count) {
      _result.push(_item);
      _total_data++;
    }
  }

  if (_total_data == 0 && _filter_properties_count == 0) {
    _result = DataArray;
  }

  return _result;
}

TableListBase.prototype.AddRow = function (Item) {
  var _str_ = "";

  _str_ += "var object_row={";

  for (var property in this.columns) {
    var _type = typeof Item[property];

    switch (_type) {
      case "string":
        _str_ += property + ":'" + Item[property] + "',";
        break;

      case "undefined":
        break;

      default:
        _str_ += property + ":" + Item[property] + ",";
        break;
    }
  }
  _str_ = _str_.substring(0, _str_.length - 1);
  _str_ += "};";

  eval(_str_);

  this.rows[object_row.id] = object_row;

  $(this.Container).jtable('addRecord', {
    record: object_row,
    clientOnly: true
  });
}

TableListBase.prototype.WSI_Tasks_ui_DeleteAlarmRow = function (Data) {
  var $ctrl = $('<a class="EliminarAlarmaIcono" />');
  var _that = this;

  $ctrl.click(function (data) {
    var $row = $($(this).parents()[1]);
    var _key = +$row.attr("data-record-key");

    LayoutYesNoOpen("", _Language.get(_that.parent.OnDeleteMessage), function () { TableListBase_DeleteRow(_that, this, $row, _key); }, function () {
    });

  });

  return $ctrl;
}

function TableListBase_DeleteRow(That, cThis, $row, _key) {
  try {
    //$row.css("display", "none");
    if (That.parent.OnDeleteRow) {
      if (m_dlg_progress != null) {
        LayoutProgressClose();
      }
      That.parent.OnDeleteRow(_key, That, cThis);
    } else {

      That.parent.rows.splice(TableList_deleteItemFromArray(_key, That.parent.rows), 1);

      if (m_dlg_progress != null) {
        LayoutProgressClose();
      }

      $(That.parent.Container).jtable('load', That.parent.rows, function () { TableListBase_Refresh_DataRows(That.parent.Container); });

      //Muestra y esconde alarmas
      $(That.parent.Container).each(function () {
        var cThis = $(cThis);
        if (cThis.find('tr[display!="none"]').length > 5) {
          cThis.find('tr:nth-child(n+6)').addClass('tablaHide');
        }
      });


      this.WSI_Tasks_ui_Refresh_DataRows();
    }
  } catch (error) {
    WSILogger.Write("WSI_Tasks_ui_DeleteAlarmRow.click() " + error);
  }
}

function LayoutYesNoCreate(Title, Text) {
  var _string_dialog = '';

  _string_dialog = _string_dialog + '<div id="dialog_layout_progress" title="' + Title + '" style="overflow: none;">';
  _string_dialog = _string_dialog + '<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>' + Text + '</p>';
  _string_dialog = _string_dialog + '</div>';

  $("#editor_dialogs").append(_string_dialog);
}

// Dialog opener
function LayoutYesNoOpen(Title, Text, OnClickYes, OnClickNo) {
  var _yes = _Language.get("NLS_POP_YESNO_YES");
  var _no = _Language.get("NLS_POP_YESNO_NO");

  // Create Dialog
  LayoutYesNoCreate(Title, Text);

  // Prepare Dialog
  m_dlg_progress = $("#dialog_layout_progress").dialog({
    dialogClass: "no-close",
    resizable: false,
    height: 140,
    modal: true,
    buttons: [
    {
      text: _yes,
      click: function () {
        $(this).dialog("close");
        m_dlg_progress = null;
        OnClickYes();
      }
    }
      , {
        text: _no,
        click: function () {
          $(this).dialog("close");
          m_dlg_progress = null;
          OnClickNo();
        }
      }]
  });
}

function WSI_Tasks_ui_DeleteAlarmRowComplete(Key, Sender, SenderClick) {
  Sender.parent.rows.splice(TableList_deleteItemFromArray(Key, Sender.parent.rows), 1);

  $(Sender.parent.Container).jtable('load', Sender.parent.rows, function () { TableListBase_Refresh_DataRows(Sender.parent.Container); });

  ////Muestra y esconde alarmas
  //$(Sender.parent.Container).each(function () {
  //  var $this = $(SenderClick);
  //  if ($this.find('tr[display!="none"]').length > 5) {
  //    $this.find('tr:nth-child(n+6)').addClass('tablaHide');
  //  }
  //});

  WSI_Tasks_ui_RefreshCollapseStatusAlarms();
}

function WSI_Tasks_ui_DeleteAlarmRowFailed() {
  WSILogger.Write("DeleteFail");
}

function TableList_deleteItemFromArray(Key, Rows) {
  var _result = -1;

  for (_idx in Rows) {
    if (_result >= 0) {
      continue;
    }

    if (Rows[_idx].id == Key) {
      _result = _idx;
    }
  }

  return _result;
}

function LayoutOkCreate(Title, Text) {
  var _string_dialog = '';

  _string_dialog = _string_dialog + '<div id="dialog_layout_progress" title="' + Title + '" style="overflow: none;">';
  _string_dialog = _string_dialog + '<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>' + Text + '</p>';
  _string_dialog = _string_dialog + '</div>';

  $("#editor_dialogs").empty();
  $("#editor_dialogs").append(_string_dialog);
}

// Dialog opener
function LayoutOkOpen(Title, Text, OnClickOk) {
  // Create Dialog
  LayoutYesNoCreate(Title, Text);

  // Prepare Dialog
  m_dlg_progress = $("#dialog_layout_progress").dialog({
    dialogClass: "no-close",
    resizable: false,
    height: 140,
    modal: true,
    buttons: {
      "Ok": function () {
        $(this).dialog("close");
        OnClickOk();
      }
    }
  });
}

function TableListBase_Refresh_DataRows(Container) {
  $($(Container.replace(" .tablaHide", "")).find(".jtable").children()[1]).children(".jtable-data-row").each(function (index, value) {
    if ((index % 2) != 0) {

      $(value).css("background", "#eee")
    }
  })

  //$($(".tablaTareas").find(".jtable").children()[1]).children(".jtable-data-row").each(function (index, value) {
  //  if ((index % 2) != 0) {

  //    $(value).css("background", "#eee")
  //  }
  //})
}
