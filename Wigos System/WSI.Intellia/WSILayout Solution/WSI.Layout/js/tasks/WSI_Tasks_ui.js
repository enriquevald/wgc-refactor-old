﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WSI_Tasks_ui
// 
//   DESCRIPTION: .
// 
//        AUTHOR: Jaume Barnés
// 
// CREATION DATE: 07/07/2015
// 
//------------------------------------------------------------------------------

var _table_alarms = new TableListBase(".tablaAlarmas");
var _table_tasks = new TableListBase(".tablaTareas");
var m_current_alarm_selected = -1;
var m_load = 0;

//ROW ALARMS Events**************
function WSI_Tasks_ui_PrintRowLocateAlarmIcon(data) {
  if (data.record.alarm_type != 43) {
    var $control = $("<a class='btnLocalizarAlarma' data-terminalid='" + data.record.terminal_id + "' href='#' data-nls='NLS_LOCATE_ALARM'>" + _Language.get('NLS_LOCATE_ALARM') + "</a>");

    $control.click(function (evt) {

      _UI.SetMenuActiveSection($("#a_section_70")[0], 70);

      ShowTerminalInfo(_Manager.Terminals.Items[_Manager.Terminals.ByTerminal[$(evt.currentTarget).data("terminalid")]], 1, false);
    });

    return $control;
  }
}

function WSI_Tasks_ui_PrintRowTerminalId(data) {
  var _result = "";

  _result += data.record.terminal_name + " {ID:" + data.record.terminal_id + "}";

  return _result;
}

function WSI_Tasks_ui_PrintRowActionsAlarms(data) {
  var $control;
  var _result = "";

  _result = " <div class='btnsAccionAlarma'>";
  _result += "<a class='btnDerivarAlarma' data-id='" + data.record.id + "' href='#' data-nls='NLS_DERIVATE'>" + _Language.get('NLS_DERIVATE') + "</a>";
  if (m_current_user_is_runner == 1) {
    _result += "<a class='btnAccionarAlarma' data-id='" + data.record.id + "' href='#' data-nls='NLS_ACTIVATE'>" + _Language.get('NLS_ACTIVATE') + "</a>";
  }
  _result += "</div>";

  $control = $(_result);

  $control.find('.btnDerivarAlarma').click(function (evt) {
    _Manager.TaskManager.locked = true;
    //animaPanelDerecha();
    mostrarPanelDerecha();
    $('.panelDerecha').find('.infoPanel').css("display", "none");
    $('.panelDerivar').css("display", "block");
    WSI_Tasks_ui_PanelDerecho_Reload();
    m_current_alarm_selected = $(evt.currentTarget).data("id");
  });

  $control.find('.btnAccionarAlarma').click(function (evt) {
    _Manager.TaskManager.locked = true;
    //TO-DO: Seleccionar usuario para derivar la alarma.
    //animaPanelDerecha();
    mostrarPanelDerecha();
    $('.panelDerecha').find('.infoPanel').css("display", "none")
    $('.panelDerivar').css("display", "block");

    WSI_Tasks_ui_PanelDerecho_Reload();

    m_current_alarm_selected = $(evt.currentTarget).data("id");

    /////
    $("#derivaUsuario").prop("checked", true);
    $(".buscarOculto").removeClass("buscarOculto");
    $($(".buscarUsuario").children(".resultadoUsuarioRol.seleccionRol")[0]).find(".rolAcordeon").each(function (index, value) {
      if (+$(value).data("userkey") == m_current_user_id) {
        $(value).addClass("rolSeleccionado");
      } else {
        $(value).removeClass("rolSeleccionado");
      }
    })
    $(".rolAcordeon").not(".rolSeleccionado").toggle("slow");

  });

  return $control;
}

function WSI_Tasks_ui_PrintRowChildAttachment(data) {
  var $img;

  if (data.record.status == 2) {
    $img = $('<div class="circuloAlarmaEscalada severidad3"/>');
  }

  if (data.record.has_attachment == true) {
    if ($img) {
      $img = $('<div class="circuloAlarma severidad3"/>');
    } else {
      $img = $('<div class="AdjuntoIcono"/>');
    }
    if (data.record.childHTML) {
      if ($img[0].className == "circuloAlarma severidad3") {
        $img.toggleClass('circuloAlarmaOn'); //icon change
      } else {
        $img.toggleClass('AdjuntoIconoOn'); //icon change
      }
    }

    $img.click(function (evt) {
      var $row = $($(this).parents()[1]);
      var _key = +$row.attr("data-record-key");
      var _child_row = $("[data-record-key='" + _key + "']").next();

      if (_child_row.attr("class") == "jtable-child-row" && _child_row.css("display") != undefined && _child_row.css("display") != "none") {
        _child_row.toggle('slow');
        if (this.className == "circuloAlarma severidad3 circuloAlarmaOn") {
          $(this).toggleClass("circuloAlarmaOn");
        } else {
          $(this).toggleClass('AdjuntoIconoOn'); //icon change
        }
        _table_alarms.rowChildExpandedId = null;
        _table_alarms.last_chid_id = null;

      } else {
        $(".tablaAlarmas").find(".jtable-child-row").hide();
        $(".tablaAlarmas").find(".AdjuntoIconoOn").removeClass("AdjuntoIconoOn");
        _table_alarms.rowChildExpandedId = _key;

        if (this.className == "circuloAlarma severidad3") {
          $(this).toggleClass("circuloAlarmaOn");
        } else {
          $(this).toggleClass('AdjuntoIconoOn'); //icon change
        }

        $(".tablaAlarmas").jtable('openChildTable', $img.closest('tr'), {
          title: "",
          actions: {
            listAction: function () {
              var _item = {
                MediaId: data.record.media_id,
                Observations: data.record.description,
                Image: data.record.media
              };
              var _list = [];
              _list.push(_item);

              return _result = {
                "Result": "OK", "Records": _list, "TotalRecordCount": 1
              };
            }
          },
          fields: {
            Image: {
              title: "",
              display: function (data) {
                var _result;
                var _result2;
                var _observation;

                _result = '<div class="bodyChild">';
                _result += '  <div class="imgBodyChild"> <img class="AdjuntoImagen" ' + ((data.record.Image != undefined && data.record.Image != "") ? 'src="data:image/png;base64,' + data.record.Image + '"' : 'style="visibility:hidden;"') + ' /></div>';
                _result += '  <div class="observationChild">';
                _result += '    <h1 data-nls="NLS_OBSERVATIONS">' + _Language.get('NLS_OBSERVATIONS') + '</h1>';


                _observation = $("<div>" + data.record.Observations + "</div>");

                _observation.find('[data-nls]').each(function (e, val) {
                  var _nls = $(val).data();

                  $(val).text(_Language.get(_nls.nls));
                });

                _observation.find('i').each(function (e, val) {
                  var dateFormat;
                  if (_Configuration.Language == 0)
                    dateFormat = 'D/M/YYYY HH:mm:ss';
                  else
                    dateFormat = 'M/D/YYYY HH:mm:ss';

                  var _time = $(val).text();

                  $(val).text(moment(_time).format(dateFormat));
                });

                _result += '    <p>' + _observation[0].innerHTML;
                //_result += '    <p>' + _Language.parse(data.record.Observations);
                _result += '    </p>';
                _result += '  </div>  ';
                _result += '</div>';

                _result2 = $(_result);

                _result2.find(".AdjuntoImagen").click(function () {
                  var _that = $(this);
                  var _media_id = data.record.MediaId;
                  var _thumbnail = data.record.Image;
                  _Manager.TaskManager.locked = !_Manager.TaskManager.locked;

                  if (!_that.hasClass('photoZoom')) {
                    PageMethods.LoadMediaData(_media_id, function (Result) {
                      _that.attr('src', 'data:image/png;base64,' + Result)
                      _that.toggleClass('photoZoom');
                    }, function () { });
                  }
                  else {
                    _that.toggleClass('photoZoom');
                    _that.attr('src', 'data:image/png;base64,' + _thumbnail)
                  }
                });

                return _result2;
              }
            }
          }
        }, function (data) {
          data.childTable.jtable('load');
        });
      }
    });

    return $img;
  }

  if ($img) {

    return $img;
  }
}

function WSI_Tasks_ui_PrintImage(data) {
  var $img = $('<img src="design/img/ico_photo_male.png" alt="popup_player_name" class="photoPlayer" />');

  return $img;

}

function WSI_Tasks_ui_OnDeleteRow(data, Sender, SenderClick) {

  WSILogger.Write("WSI_Tasks_ui_OnDeleteRow()");

  var _idx_alarm = -1;

  for (_idx in _Manager.TaskManager.Alarms) {
    if (_Manager.TaskManager.Alarms[_idx].id == data) {
      _idx_alarm = _idx;

      break;
    }
  }

  if (_idx_alarm >= 0) {
    var _alarm = _Manager.TaskManager.Alarms[_idx_alarm]

    _alarm.status = 1;

    _Manager.TaskManager.DeleteAlarm(_alarm, Sender, SenderClick);
  }

  WSI_Tasks_ui_RefreshCollapseStatusAlarms();
}

function WSI_Tasks_ui_PrintRowAlarmType(data) {

  var _result = "";

  switch (data.record.alarm_id) {
    case "99000":
    case "98000":
    case "97000":
      _result = data.record.title;
      break;

    case "16016":
    case "16032":
      _result = $(_Language.parse($("<div>" + _Legends.Dictionary_Alarms["41"][data.record.alarm_id] + "</div>")[0])).html();
      break;

    default:
      _result = $(_Language.parse($("<div>" + _Legends.Dictionary_Alarms[data.record.alarm_type][data.record.alarm_id] + "</div>")[0])).html();
      break;
  }
  
  return _result;
}

function WSI_Tasks_ui_PrintRowAlarmTypeTask(data) {

  var _result = "";
  
  switch (data.record.subcategory) {
    case 99000:
    case 98000:
    case 97000:
      _result = data.record.title;
      break;

    case 16016:
    case 16032:
      _result = $(_Language.parse($("<div>"+_Legends.Dictionary_Alarms["41"][data.record.subcategory]+"</div>")[0])).html();
      break;

    default:
      _result = $(_Language.parse($("<div>" + _Legends.Dictionary_Alarms[data.record.category][data.record.subcategory] + "</div>")[0])).html();
      break;
  }


  return _result;
}

function WSI_Tasks_ui_GetAlarmsFilters() {

}

//************************
function WSI_Tasks_ui_Init() {
  var _result = "<br>";

  _result += "<div class='wrapPanel'>";
  _result += "  <div class='wrapMisAlarmas'>";
  //Header 
  _result += "    <div class='misAlarmasHeader'>";
  _result += "<h1 data-nls='NLS_MY_ALARMS'>" + _Language.get('NLS_MY_ALARMS') + "</h1>";

  _result += "      <div class='accionesAlarmas'>";
  _result += "        <ul>";
  _result += "          <li><a class='filtrarAlarmas' href='#' data-nls='NLS_FILTER_ALARMS'>" + _Language.get('NLS_FILTER_ALARMS') + "</a></li>";
  _result += "          <li><a class='btnTodasAlarmas' href='#' data-nls='NLS_VIEW_ALL'>" + _Language.get('NLS_VIEW_ALL') + "</a></li>";
  _result += "        </ul>";
  _result += "      </div>";
  _result += "      <div class='selectorNivelAlarma'>"
  _result += "        <select class='selectNivelAlarma250' id='selAlarmStatus'>";
  _result += "          <option data-nls='NLS_TYPE' value='' disable selected>" + _Language.get('NLS_TYPE') + "</option>";

  for (_idx_dic_alarms_machine in _Legends.Dictionary_Alarms[41]) {
    _result += "        <option value='" + _idx_dic_alarms_machine + "'>" + _Legends.Dictionary_Alarms[41][_idx_dic_alarms_machine] + "</option>";
  }

  for (_idx_dic_alarms_player in _Legends.Dictionary_Alarms[42]) {
    _result += "        <option value='" + _idx_dic_alarms_player + "'>" + _Legends.Dictionary_Alarms[42][_idx_dic_alarms_player] + "</option>";
  }

  for (_idx_dic_alarms_player in _Legends.Dictionary_Alarms[43]) {
    _result += "        <option value='" + _idx_dic_alarms_player + "'>" + _Legends.Dictionary_Alarms[43][_idx_dic_alarms_player] + "</option>";
  }
  _result += "        </select>";
  _result += "        <div class='btnBuscar' id='btnBuscarAlarma'>"
  _result += "          <a href='#'>Buscar</a>";
  _result += "        </div>";
  _result += "      </div>";
  _result += "    </div>";

  //Table
  _result += "    <div class='tablaAlarmas .tablaHide'>";

  //Filters
  _result += "      <div class='tableFilters'>";
  _result += "        <h1></h1>";
  _result += "      </div>";
  _result += "    </div>";
  _result += "    <div class='leyendaTablas'>";
  _result += "      <ul>";
  _result += "        <li> <div class='circuloValor severidad3'></div><span data-nls='NLS_SCALED'>" + _Language.get('NLS_SCALED') + "</span> </li>";
  _result += "      </ul>";
  _result += "    </div>";
  _result += "  </div>";
  _result += "  <div class='wrapTareasTabla'>";

  //Header 
  _result += "    <div class='misTareasHeader'>";
  _result += "      <h1 data-nls='NLS_MY_ALARMS'>" + _Language.get('NLS_MY_ALARMS') + "</h1>";
  _result += "      <div class='accionesTareas'>";
  _result += "        <ul>";
  _result += "          <li><a class='filtrarTareas' href='#' data-nls='NLS_FILTER_TASKS'>" + _Language.get('NLS_FILTER_TASKS') + "</a></li>";
  _result += "          <li><a class='btnTodasTareas' href='#' data-nls='NLS_VIEW_ALL'>" + _Language.get('NLS_VIEW_ALL') + "</a></li>";
  _result += "        </ul>";
  _result += "      </div>";

  _result += "      <div class='selectorNivelTareas'>"
  _result += "        <select class='selectNivelTareas200' id='selTaskRole'>";
  _result += "          <option value='' disable selected data-nls='NLS_ROLE'>" + _Language.get('NLS_ROLE') + "</option>";
  _result += "          <option value='1' data-nls='NLS_ROLE_PCA'>" + _Language.get('NLS_ROLE_PCA') + "</option>";
  _result += "          <option value='2' data-nls='NLS_ROLE_OPM'>" + _Language.get('NLS_ROLE_OPM') + "</option>";
  _result += "          <option value='4' data-nls='NLS_ROLE_SLA'>" + _Language.get('NLS_ROLE_SLA') + "</option>";
  _result += "          <option value='8' data-nls='NLS_ROLE_TSP'>" + _Language.get('NLS_ROLE_TSP') + "</option>";
  _result += "        </select>";
  _result += "        <select class='selectNivelTareas' id='selTaskUser'>";
  _result += "          <option value='' disable selected data-nls='NLS_USER'>" + _Language.get('NLS_USER') + "</option>";
  for (_idx_tasks_users_filter in _Manager.TaskManager.Users) {
    _result += "        <option value='" + _idx_tasks_users_filter + "'>" + _Manager.TaskManager.Users[_idx_tasks_users_filter] + "</option>";
  }
  _result += "        </select>";
  _result += "        <select class='selectNivelTareas' id='selTaskPriority'>";
  _result += "          <option value='' disable selected data-nls='NLS_PRIORITY'>" + _Language.get('NLS_PRIORITY') + "</option>";
  _result += "          <option value='3' data-nls='NLS_PRIORITY_HIGH'>" + _Language.get('NLS_PRIORITY_HIGH') + "</option>";
  _result += "          <option value='2' data-nls='NLS_PRIORITY_MEDIUM'>" + _Language.get('NLS_PRIORITY_MEDIUM') + "</option>";
  _result += "          <option value='1' data-nls='NLS_PRIORITY_LOW'>" + _Language.get('NLS_PRIORITY_LOW') + "</option>";
  _result += "        </select>";
  _result += "        <select class='selectNivelTareas250' id='selTaskType'>";
  _result += "          <option value='' disable selected data-nls='NLS_TYPE'>" + _Language.get('NLS_TYPE') + "</option>";

  for (_idx_dic_alarms_machine in _Legends.Dictionary_Alarms[41]) {
    _result += "        <option value='" + _idx_dic_alarms_machine + "'>" + _Legends.Dictionary_Alarms[41][_idx_dic_alarms_machine] + "</option>";
  }
  for (_idx_dic_alarms_player in _Legends.Dictionary_Alarms[42]) {
    _result += "        <option value='" + _idx_dic_alarms_player + "'>" + _Legends.Dictionary_Alarms[42][_idx_dic_alarms_player] + "</option>";
  }
  for (_idx_dic_alarms_player in _Legends.Dictionary_Alarms[43]) {
    _result += "        <option value='" + _idx_dic_alarms_player + "'>" + _Legends.Dictionary_Alarms[43][_idx_dic_alarms_player] + "</option>";
  }

  _result += "        </select>";
  _result += "        <select class='selectNivelTareas' id='selTaskStatus'>";
  _result += "          <option value='' disable selected data-nls='NLS_STATE'>" + _Language.get('NLS_STATE') + "</option>";
  _result += "          <option value='1' data-nls='NLS_STATE_PENDING'>" + _Language.get('NLS_STATE_PENDING') + "</option>";
  _result += "          <option value='2' data-nls='NLS_STATE_IN_PROGRESS'>" + _Language.get('NLS_STATE_IN_PROGRESS') + "</option>";
  _result += "          <option value='3' data-nls='NLS_STATE_SOLVED'>" + _Language.get('NLS_STATE_SOLVED') + "</option>";
  _result += "          <option value='4' data-nls='NLS_STATE_SCALED'>" + _Language.get('NLS_STATE_SCALED') + "</option>";
  _result += "          <option value='5' data-nls='NLS_STATE_VALIDATED'>" + _Language.get('NLS_STATE_VALIDATED') + "</option>";
  _result += "        </select>";
  _result += "        <div class='btnBuscar' id='btnBuscarTarea'>"
  _result += "          <a href='#' data-nls='NLS_SEARCH'>" + _Language.get('NLS_SEARCH') + "</a>";
  _result += "        </div>";
  _result += "      </div>";
  _result += "    </div>";

  //Table
  _result += "    <div class='tablaTareas .tablaHide'>";
  //Filters
  _result += "    <div class='tableFilters'>";
  _result += "      <h1></h1>";
  _result += "    </div>";
  _result += "  </div>";
  _result += "  <div class='leyendaTablas'>";
  _result += "    <ul>";
  _result += "      <li> <div class='circuloValor severidad3'></div><span data-nls='NLS_PRIORITY_HIGH_PRIORITY'>" + _Language.get('NLS_PRIORITY_HIGH_PRIORITY') + "</span> </li>";
  _result += "      <li> <div class='circuloValor severidad2'></div><span data-nls='NLS_PRIORITY_MEDIUM_PRIORITY'>" + _Language.get('NLS_PRIORITY_MEDIUM_PRIORITY') + "</span> </li>";
  _result += "      <li> <div class='circuloValor severidad1'></div><span data-nls='NLS_PRIORITY_LOW_PRIORITY'>" + _Language.get('NLS_PRIORITY_LOW_PRIORITY') + "</span> </li>";
  _result += "    </ul>";
  _result += "  </div>";
  _result += "</div>";
  _result += "</div>";

  $("#div_tasks").html(_result);

  WSI_Tasks_ui_FillCombo();

  CreateTableAlarms();
  CreateTableTareas();

  WSI_Tasks_ui_Init_Events();
}

function WSI_Tasks_ui_Reset_Filters() {

  $("#selTaskRole option:eq('')").prop('selected', true)
  $("#selTaskUser option:eq('')").prop('selected', true)
  $("#selTaskPriority option:eq('')").prop('selected', true)
  $("#selTaskType option:eq('')").prop('selected', true)
  $("#selTaskStatus option:eq('')").prop('selected', true)
  $("#btnBuscarAlarma option:eq('')").prop('selected', true)

}

function WSI_Tasks_ui_Alarms_Reset_Filters() {
  $("#selAlarmStatus option:eq('')").prop('selected', true)
}

function WSI_Tasks_ui_Init_Events() {
  $('.btnTodasAlarmas').click(function () {
    mostrarOcultarFiltrosAlarmas();
    if ($(this).hasClass("btnTodasAlarmasShow")) {
      $(this).removeClass("btnTodasAlarmasShow");
      $(this).text(_Language.get('NLS_VIEW_ALL', undefined, true));
      muestraAlarmas();
    } else {
      $(this).addClass("btnTodasAlarmasShow");
      $(this).text(_Language.get('NLS_HIDE', undefined, true));
      $('.tablaHide').toggle('slow');
    }
    _table_alarms.rowsHide = !($(this).hasClass("btnTodasAlarmasShow"));
  });

  $('.btnTodasTareas').click(function () {
    mostrarOcultarFiltrosTareas();
    if ($(this).hasClass("btnTodasTareasShow")) {
      $(this).removeClass("btnTodasTareasShow");
      $(this).text(_Language.get('NLS_VIEW_ALL', undefined, true));
      muestraTareas();
    } else {
      $(this).addClass("btnTodasTareasShow");
      $(this).text(_Language.get('NLS_HIDE', undefined, true));
      $('.tareasHide').toggle('slow');
    }

    _table_tasks.rowsHide = !($(this).hasClass("btnTodasAlarmasShow"));
  });

  $('.btnInfoTareas').click(function () {
    animaPanelDerecha();
    $('.panelDerecha').find('.infoPanel').css("display", "none")
    $('.panelInfoAlarma').css("display", "block");
  });

  $('.btnEnviarMensaje').click(function () {
    animaPanelDerecha();
    $('.panelDerecha').find('.infoPanel').css("display", "none")
    $('.panelInfoAlarma').css("display", "block");
  });

  $('.btnsModificarEstado').click(function () {
    animaPanelDerecha();
    $('.panelDerecha').find('.infoPanel').css("display", "none")
    $('.panelModificarEstado').css("display", "block");
  });

  $('#crearAlarma').click(function () {
    WSILogger.Write("btnAccionDerivar");
    WSI_Tasks_ui_AssignAlarm();
  });

  $('#modificarTarea').click(function () {

    WSI_Tasks_ui_ModifyTask();
  });

  $('#btnBuscarUsuario').click(function () {
    $('#derivarBuscarUsuario').click();
  });

  $('#derivarBuscarUsuario').click(function () {
    var _list = {
    };
    var _search_value = $("#derivarBuscarUsuario").val();

    if (_search_value != "") {
      for (_idx_buscar_jugador in _Manager.TaskManager.Users) {
        if (_Manager.TaskManager.Users[_idx_buscar_jugador].toUpperCase().startsWith(_search_value.toUpperCase())) {
          _list[_idx_buscar_jugador] = _Manager.TaskManager.Users[_idx_buscar_jugador];
        }
      }
    } else {
      _list = _Manager.TaskManager.Users;
    }

    WSI_Tasks_ui_PanelDerecho_AddUserNames(_list);
  });

  $('#RoleSelector li').click(function () {
    if ($(this).hasClass("rolSeleccionado")) {
      $(this).removeClass("rolSeleccionado")
    } else {
      $(this).addClass("rolSeleccionado");
    }
  });

  $("#btnBuscarAlarma").click(function () {
    WSI_Tasks_ui_GetAlarmFilters();
  });

  $("#btnBuscarTarea").click(function () {
    WSI_Tasks_ui_GetTasksFilters();
  });

  function muestraTareas() {
    $('.tablaTareas').each(function () {
      var $this = $(this);
      if ($this.find('tr').length > 5) {
        if (_table_tasks.rowChildExpandedId) {
          $this.find('tr:nth-child(n+7)').addClass('tareasHide');
        } else {
          $this.find('tr:nth-child(n+6)').addClass('tareasHide');
        }
      }
    });
  }

  function muestraAlarmas() {
    $('.tablaAlarmas').each(function () {
      var $this = $(this);
      if ($this.find('tr').length > 5) {
        if (_table_alarms.rowChildExpandedId) {
          $this.find('tr:nth-child(n+7)').addClass('tablaHide');
        } else {
          $this.find('tr:nth-child(n+6)').addClass('tablaHide');
        }
      }
    });

  }

  $('.filtrarTareas').click(function () {
    $('.filtrarTareas').toggleClass('filtrarTareasClose');
    $('.selectorNivelTareas').toggleClass('selectorNivelTareasShow');
    WSI_Tasks_ui_Reset_Filters();
    WSI_Tasks_ui_GetTasksFilters();
  });

  $('.filtrarAlarmas').click(function () {
    $('.filtrarAlarmas').toggleClass('filtrarAlarmasClose');
    $('.selectorNivelAlarma').toggleClass('selectorNivelAlarmaShow');
    WSI_Tasks_ui_Alarms_Reset_Filters();
    WSI_Tasks_ui_GetAlarmsFilters();
  });
}

function mostrarOcultarFiltrosTareas() {
  $('.filtrarTareasClose').toggleClass('filtrarTareasClose');
  $('.selectorNivelTareasShow').toggleClass('selectorNivelTareasShow');
}

function mostrarOcultarFiltrosAlarmas() {
  $('.filtrarAlarmasClose').toggleClass('filtrarAlarmasClose');
  $('.selectorNivelAlarmaShow').toggleClass('selectorNivelAlarmaShow');
}

function WSI_Tasks_ui_PanelDerecho_AddUserNames(List) {
  var _result = "";

  $(".resultadoUsuarioRol.seleccionRol").children(0).empty();

  for (_idx_user_names in List) {
    _result += "<li class='rolAcordeon' data-userKey='" + _idx_user_names + "' ><span>" + List[_idx_user_names] + "</span></li>";
  }
  $(".resultadoUsuarioRol.seleccionRol").children(0).html(_result);

  $('.resultadoUsuarioRol.seleccionRol li').click(function () {
    if ($(this).hasClass("rolSeleccionado")) {
      if ($(".buscarUsuario.buscarOculto").length != 0) {
        $(this).removeClass("rolSeleccionado")
      }
    } else {
      $(this).addClass("rolSeleccionado");

      if ($(".buscarUsuario.buscarOculto").length == 0) {
        $(".resultadoUsuarioRol").find(".rolAcordeon").not(".rolSeleccionado").toggle("slow")
      }

    }
  })
}



function CreateTableAlarms() {
  //Field, IsPrimaryKey, Visible, Title, Width, Display, ColumnType, Sortable
  _table_alarms.AddColumn("id", true, false, "", "0%", "", "text", false);
  _table_alarms.AddColumn("has_attachment", false, true, "NLS_COLUMN_ATTATCH", "5%", WSI_Tasks_ui_PrintRowChildAttachment, "text", false);
  _table_alarms.AddColumn("alarm_id", false, true, "NLS_COLUMN_DESCRIPTION", "25%", WSI_Tasks_ui_PrintRowAlarmType, "text", true);
  _table_alarms.AddColumn("date_created", false, true, "NLS_COLUMN_CREATED", "10%", WSI_Tasks_ui_Print_Localized_Date_Alarm_Created, "text", true);
  _table_alarms.AddColumn("remaining_time", false, true, "NLS_COLUMN_LENGTH", "10%", WSI_Tasks_ui_Print_Remaining_Time, "text", true);
  _table_alarms.AddColumn("terminal_id", false, true, "NLS_COLUMN_TERMINAL", "10%", WSI_Tasks_ui_PrintRowTerminalId, "text", true);
  _table_alarms.AddColumn("trace", false, true, "NLS_COLUMN_LOCATE", "10%", WSI_Tasks_ui_PrintRowLocateAlarmIcon, "text", false);
  _table_alarms.AddColumn("actions", false, true, "NLS_COLUMN_ACTION", "20%", WSI_Tasks_ui_PrintRowActionsAlarms, "text", false);

  _table_alarms.rows = _Manager.TaskManager.Alarms;
  _table_alarms.sortValue = "remaining_time ASC";

  _table_alarms.OnDeleteRow = WSI_Tasks_ui_OnDeleteRow;

  _table_alarms.CreateTable();
}

function CreateTableTareas() {

  _table_tasks.allowDelete = false;
  //Field, IsPrimaryKey, Visible, Title, Width, Display, ColumnType, Sortable
  _table_tasks.AddColumn("severity", false, true, "", "5%", WSI_Tasks_ui_Print_Severity, "text", false);
  _table_tasks.AddColumn("id", true, true, "NLS_ID", "0%", "", "text", true);
  _table_tasks.AddColumn("assigned_user_id", false, true, "NLS_COLUMN_ASSIGNED", "12%", WSI_Tasks_ui_Print_UserName, "text", true);
  _table_tasks.AddColumn("description", false, true, "NLS_COLUMN_DESCRIPTION", "24%", WSI_Tasks_ui_PrintRowAlarmTypeTask, "text", true);
  _table_tasks.AddColumn("creation", false, true, "NLS_COLUMN_CREATED", "10%", WSI_Tasks_ui_Print_Localized_Date_Task_Created, "text", true);
  _table_tasks.AddColumn("remaining_time", false, true, "NLS_COLUMN_LENGTH", "10%", WSI_Tasks_ui_Print_Remaining_Time, "text", true);
  _table_tasks.AddColumn("last_status_update", false, true, "NLS_COLUMN_UPDATED", "10%", WSI_Tasks_ui_Print_Localized_Date_Task_Updated, "text", true);
  _table_tasks.AddColumn("actions", false, true, "NLS_COLUMN_ACTION", "14%", WSI_Tasks_ui_Print_Change_Status, "text", false);
  _table_tasks.AddColumn("status", false, true, "NLS_COLUMN_STATE", "10%", WSI_Tasks_ui_Print_Status, "text", true);

  _table_tasks.rows = _Manager.TaskManager.Tasks;
  _table_tasks.sortValue = "remaining_time ASC";

  _table_tasks.CreateTable();
}

function WSI_Tasks_ui_Refresh() {

  _table_alarms.rows = _Manager.TaskManager.Alarms;
  _table_tasks.rows = _Manager.TaskManager.Tasks;

  var _task;
  var _count;

  _count = 0;
  for (_task in _table_tasks.rows) {
    if (_table_tasks.rows[_task].status != Enum.EN_TASK_STATUS.VALIDATED && _table_tasks.rows[_task].status != Enum.EN_TASK_STATUS.SCALED) {
      _count++;
    }
  }
  $(".severidadTec1").text(_count);

  WSILogger.Write('Refreshing Alarms UI...');
  _table_alarms.Refresh();
  WSILogger.Write('Refreshing Tasks UI...');
  _table_tasks.Refresh();

  WSILogger.Write('Refreshing collapsed status for alarms...');
  WSI_Tasks_ui_RefreshCollapseStatusAlarms();

  WSILogger.Write('Updating usernames...');
  WSI_Tasks_ui_PanelDerecho_AddUserNames(_Manager.TaskManager.Users);
}

function WSI_Tasks_ui_FillCombo() {

}

function WSI_Tasks_ui_RefreshCollapseStatusAlarms() {
  if (!$('.btnTodasAlarmas').hasClass("btnTodasAlarmasShow")) {
    $('.tablaAlarmas').each(function () {
      var $this = $(this);
      if ($this.find('tr').length > 5) {
        if (_table_alarms.rowChildExpandedId) {
          $this.find('tr:nth-child(n+7)').addClass('tablaHide');
        } else {
          $this.find('tr:nth-child(n+6)').addClass('tablaHide');
        }
      }
    });
  }

  if (!$('.btnTodasTareas').hasClass("btnTodasTareasShow")) {
    $('.tablaTareas').each(function () {
      var $this = $(this);
      if ($this.find('tr').length > 5) {
        if (_table_tasks.rowChildExpandedId) {
          $this.find('tr:nth-child(n+7)').addClass('tareasHide');
        } else {
          $this.find('tr:nth-child(n+6)').addClass('tareasHide');
        }
      }
    });
  }
}

function WSI_Tasks_ui_PanelDerecho_Reload() {
  $(".rolAcordeon").not(".rolSeleccionado").show();
  $($(".severidadAlarma").children(":radio:checked")[0]).prop("checked", false) //borra prioridad 
  $(".panelDerivar").find(".comentarioAlarma").val("");
  $(".panelModificarEstado").find(".comentarioAlarma").val("");
  $("#acordeon").addClass("rolOculto"); //Oculta roles
  $(".buscarUsuario").addClass("buscarOculto") //Oculta usuarios
  $(".derivarRol").find(":input:checked").prop("checked", false) //borra rol/user
  $(".srcJugadorPanel").val("");
  $(".rolSeleccionado").removeClass("rolSeleccionado");
  $("#errorDerivarAlarma").val("");
  $("#errorDerivarAlarma").text("");
}

function WSI_Tasks_ui_AssignAlarm() {
  var _priority;
  var _role = -1;
  var _description;
  var _user_key = -1;

  if (WSI_Tasks_ui_CheckDataAssignAlarm()) {
    _priority = $(".severidadAlarma").children(":radio:checked").val();
    _description = $(".panelDerivar").find(".comentarioAlarma").val();

    if ($(".accordion.rolOculto").length == 0) {
      _role = 0;
      $("#acordeon").children(".seleccionRol").children(0).children(".rolAcordeon.rolSeleccionado").each(function (index, value) {
        _role += +$(value).data("rolekey");
      });
    } else {
      _user_key = $($(".buscarUsuario").children(".resultadoUsuarioRol.seleccionRol")[0]).find(".rolAcordeon.rolSeleccionado").data("userkey");
    }

    //AlarmKey, RolId, UserId, Severity, Comment
    _Manager.TaskManager.CreateTask(m_current_alarm_selected, +_role, +_user_key, +_priority, _description);
  } else {
    LayoutOkOpen(_Language.get('NLS_ERROR'), _Language.get('NLS_TASKS_ERROR_ASSIGN'), function () {
    });
  }

}

function WSI_Tasks_ui_CheckDataAssignAlarm() {
  if ($(".buscarUsuario.buscarOculto").length > 0 && $(".derivarRol.rolOculto").length > 0) {
    $("#errorDerivarAlarma").text(_Language.get('NLS_ERR_USER_ROLE_ASSIGNED'));

    return false;
  }

  if ($(".derivarRol.rolOculto").length == 0) {
    if ($(".rolAcordeon.rolSeleccionado").length == 0) {
      $("#errorDerivarAlarma").text(_Language.get('NLS_ERR_USER_ROLE_ASSIGNED'));

      return false;
    }
  }

  if ($(".buscarUsuario.buscarOculto").length == 0) {
    if ($(".rolAcordeon.rolSeleccionado").length == 0) {

      return false;
    }

  }

  if ($(".severidadAlarma").children(":radio:checked").length == 0) {
    $("#errorDerivarAlarma").text(_Language.get('NLS_ERR_SEVERITY'));

    return false;
  }

  return true;
}

function WSI_Tasks_ui_CheckDataModifyTask() {
  if ($($(".estadoAlarmas").children(":radio:checked")[0]).lenght == 0) {

    return false;
  }

  return true;
}

function WSI_Tasks_ui_After_AssignAlarm() {

  animaPanelDerecha();
  WSILogger.Debug("WSI_Tasks_ui_After_SaveTask.Update()");
}

function WSI_Tasks_ui_After_SaveTask(Task) {

  for (_idx_task_modify_ in _Manager.TaskManager.Tasks) {
    if (_Manager.TaskManager.Tasks[_idx_task_modify_].id == Task.ID) {
      break;
    }
  }

  _Manager.TaskManager.Tasks[_idx_task_modify_].status = Task.Status;
  _Manager.TaskManager.Tasks[_idx_task_modify_].last_status_update = new Date(parseInt(Task.LastStatusUpdate.substr(6))).format('dd/MM/yyyy HH:mm:ss');

  WSI_Tasks_ui_Refresh();
  LayoutProgressClose();
  animaPanelDerecha();
  _Manager.TaskManager.locked = false;
  _Manager.TaskManager.AlarmsTasksEventsLastUpdate = GetTimeSpanNow();

  WSILogger.Debug("WSI_Tasks_ui_After_SaveTask.Update()");
}

function WSI_Tasks_ui_ModifyTask() {
  var _task;

  for (_idx_task_modify in _Manager.TaskManager.Tasks) {
    if (_Manager.TaskManager.Tasks[_idx_task_modify].id == m_current_alarm_selected) {
      break;
    }
  }

  _task = _Manager.TaskManager.Tasks[_idx_task_modify];

  var _status = -1;
  var _description = "";
  var _events_history = "";

  if (WSI_Tasks_ui_CheckDataModifyTask) {

    _status = $($(".estadoAlarmas").children(":radio:checked")[0]).prop("value");

    if (+_status == _task.status) {
      LayoutOkOpen(_Language.get('NLS_ERROR'), _Language.get('NLS_TASKS_ERROR_MODIFY_STATUS'), function () {
      });

      return;
    }

    var _str_status = "";
    var _nls_status = "";

    switch (+_status) {
      case 1:
        _nls_status = 'NLS_STATE_PENDING';//
        break;

      case 2:
        _nls_status = 'NLS_STATE_IN_PROGRESS';//"En Curso";
        break;

      case 3:
        _nls_status = 'NLS_STATE_SOLVED';//"Resuelta";
        break;

      case 5:
        _nls_status = 'NLS_STATE_VALIDATED';//"Validada";
        break;
    }

    _str_status = _Language.get(_nls_status);

    //var _time = GetNowDateTime();
    //_time = moment(_time).format('M/D/YYYY hh:mm:ss A');

    //LA 06-04-2017 PBI 26631
    var _str_username = m_current_user_name.toUpperCase();

    //var _str_username = _Manager.TaskManager.Users[m_current_user_id];

    if ($(".panelModificarEstado").find(".comentarioAlarma").val() != "" && $(".panelModificarEstado").find(".comentarioAlarma").val() != undefined) {
      _description = $(".panelModificarEstado").find(".comentarioAlarma").val();
    } else {
      _description = $(".panelModificarEstado").find(".comentarioAlarma").text();
    }

    var _time = m_wgdb_date._i;
    _events_history = "<div class='taskHistory'><i>" + _time + "</i>  " + _str_username + "  <span data-nls='NLS_STATUS_CHANGED'>" + _Language.get('NLS_STATE_VALIDATED') + "</span>[<span data-nls='" + _nls_status + "'>status</span>]</div>";
    _events_history = "<br><div class='taskHistory'><i>" + _time + "</i>  " + _str_username + "  <b><span data-nls='NLS_HISTORY_COMMENTS'>Comentario:</span></b> " + _description.trim() + "</div>" + _events_history;

  } else {
    LayoutOkOpen(_Language.get('NLS_ERROR'), _Language.get('NLS_TASKS_ERROR_MODIFY'), function () {
    });

    return;
  }

  _task.status = +_status;

  if (_description != "") {
    _task.description = _description.trim();
  }

  if (_events_history != "") {
    _task.history = _events_history + "<br>" + _task.history;
  }

  _Manager.TaskManager.SaveTask(_task);
}

//ROW TASK Events**************
function WSI_Tasks_ui_Print_Severity(data) {
  var _severity = data.record.severity;
  var _result;
  var $img;

  if (_severity > 0 && _severity <= 3) {
    $img = $('<div class="circuloAlarma severidad' + _severity + '"></div>');

    if (data.record.childHTML) {
      $img.toggleClass('circuloAlarmaOn'); //icon change
    }

    $img.click(function () {
      var $row = $($(this).parents()[1]);
      var _key = +$row.attr("data-record-key");
      var _child_row = $("[data-record-key='" + _key + "']").next();

      if (_child_row.attr("class") == "jtable-child-row" && _child_row.css("display") != undefined && _child_row.css("display") != "none") {
        _child_row.toggle('slow');

        //
        $(this).toggleClass('circuloAlarmaOn'); //icon change
        _table_tasks.rowChildExpandedId = null;
        _table_tasks.last_chid_id = null;

      } else {

        //
        $(".tablaTareas").find(".jtable-child-row").hide()
        $(".tablaTareas").find(".circuloAlarmaOn").removeClass("circuloAlarmaOn")
        _table_tasks.rowChildExpandedId = _key;

        $(this).toggleClass('circuloAlarmaOn'); //icon change

        //

        $(".tablaTareas").jtable('openChildTable', $img.closest('tr'), {
          title: "",
          actions: {
            listAction: function () {
              var _item = {
                Observations: data.record.history,
                Image: data.record.attached_media,
                MediaId: data.record.attached_media_id
              };
              var _list = [];
              _list.push(_item);

              return _result = {
                "Result": "OK", "Records": _list, "TotalRecordCount": 1
              };
            }
          },
          fields: {
            Image: {
              title: "",
              display: function (data) {
                var _result;
                var _result2;
                var _observation;

                _result = '<div class="bodyChild">';
                _result += '  <div class="imgBodyChild"> <img class="AdjuntoImagen" ' + ((data.record.Image != undefined && data.record.Image != "") ? 'src="data:image/png;base64,' + data.record.Image + '"' : 'style="visibility:hidden;"') + ' /></div>';
                _result += '  <div class="observationChild">';
                _result += '    <h1 data-nls="NLS_EVENTS_HISTORY">' + _Language.get('NLS_EVENTS_HISTORY') + '</h1>';

                _observation = $("<div>" + data.record.Observations + "</div>");

                _observation.find('[data-nls]').each(function (e, val) {
                  var _nls = $(val).data();

                  $(val).text(_Language.get(_nls.nls));
                });


                _observation.find('i').each(function (e, val) {
                  var dateFormat;
                  if (_Configuration.Language == 0)
                    dateFormat = 'D/M/YYYY HH:mm:ss';
                  else
                    dateFormat = 'M/D/YYYY HH:mm:ss';

                  var _time = $(val).text();

                  $(val).text(moment(_time).format(dateFormat));
                });


                _result += '    <p>' + _observation[0].innerHTML;
                _result += '    </p>';
                _result += '  </div>  ';
                _result += '</div>';

                _result2 = $(_result);

                _result2.find(".AdjuntoImagen").click(function () {
                  var _that = $(this);
                  var _media_id = data.record.MediaId;
                  var _thumbnail = data.record.Image;
                  _Manager.TaskManager.locked = !_Manager.TaskManager.locked;

                  if (!_that.hasClass('photoZoom')) {
                    PageMethods.LoadMediaData(_media_id, function (Result) {
                      _that.attr('src', 'data:image/png;base64,' + Result)
                      _that.toggleClass('photoZoom');
                    }, function () {
                    });
                  }
                  else {
                    _that.toggleClass('photoZoom');
                    _that.attr('src', 'data:image/png;base64,' + _thumbnail)
                  }
                });

                return _result2;
              }
            }
          }
        }, function (data) {
          setTimeout(function(){data.childTable.jtable('load')},200);
        });
      }
    });
  }
  return $img;

}

function WSI_Tasks_ui_Print_Change_Status(data) {
  var _str = '';
  var $control;

  if (data.record.status != 4) {
    _str += "<div class='btnsModificarEstado'>";
  } else {
    _str += "<div class='btnsModificarEstado' style='display:none;'>";
  }

  _str += "<a class='btnEstadoTarea' data-id='" + data.record.id + "' href='#' data-nls='NLS_MODIFY_STATE'>" + _Language.get('NLS_MODIFY_STATE') + "</a>";
  _str += "</div>";

  $control = $(_str);

  $control.find('.btnEstadoTarea').click(function (evt) {
    animaPanelDerecha();
    $('.panelDerecha').find('.infoPanel').css("display", "none")
    $('.panelModificarEstado').css("display", "block");

    m_current_alarm_selected = $(evt.currentTarget).data("id");

    WSI_Tasks_ui_PanelDerecho_ModifyAlarm_Reload();

    var _current_task;

    for (_idx_tasks_status in _Manager.TaskManager.Tasks) {
      if (_Manager.TaskManager.Tasks[_idx_tasks_status].id == m_current_alarm_selected) {
        break;
      }
    }

    _current_task = _Manager.TaskManager.Tasks[_idx_tasks_status];

    $("#EstadoAlarmas" + _current_task.status).prop("checked", true);
  });

  return $control;
}

function WSI_Tasks_ui_Message_Action(data) {
  var $control;
  var _result = "";

  if (data.record.status != 4) {
    _result += "<div class='btnsEnviarMensaje'>";
  } else {
    _result += "<div class='btnsEnviarMensaje' style='display:none;'>";
  }

  //Fix Bug 16023:Intellia: El manager puede auto-mandarse mensajes
  var current_user_id = $("#txt_current_role").data("userid");

  if (data.record.assigned_user_id == current_user_id)
    _result += "<p>" + _Manager.TaskManager.Users[data.record.assigned_user_id] + "</p>";
  else
    _result += "<a class='btnEnviarMensaje' data-id='" + data.record.assigned_user_id + "' href='#'>" + _Manager.TaskManager.Users[data.record.assigned_user_id] + "<\/a>";

  _result += "</div>";

  $control = $(_result);
  $control.find('.btnEnviarMensaje').click(function (evt) {
    mostrarPanelDerechaExtendido();
    var _MessageManager = MessageManager.getInstance();
    _MessageManager.InitConversation(data.record.assigned_user_id, _Manager.TaskManager.Users[data.record.assigned_user_id]);
  });


  return $control;
}

function WSI_Tasks_ui_PanelDerecho_ModifyAlarm_Reload() {
  $($(".estadoAlarmas").children(":radio:checked")[0]).prop("checked", false)
  $(".comentarioAlarma").val("");
}

function WSI_Tasks_ui_Print_Status(data) {
  var _str = '';
  var _task_status = data.record.status;

  switch (_task_status) {
    case 1:
      _str = '<div class="statusAlarma alarmaPendiente" data-nls="NLS_STATE_PENDING">' + _Language.get('NLS_STATE_PENDING') + '</div>';
      break;

    case 2:
      _str = '<div class="statusAlarma alarmaCurso" data-nls="NLS_STATE_IN_PROGRESS">' + _Language.get('NLS_STATE_IN_PROGRESS') + '</div>';
      break;

    case 3:
      _str = '<div class="statusAlarma alarmaResuelta" data-nls="NLS_STATE_SOLVED">' + _Language.get('NLS_STATE_SOLVED') + '</div>';
      break;

    case 4:
      _str = '<div class="statusAlarma alarmaEscalada" data-nls="NLS_STATE_SCALED">' + _Language.get('NLS_STATE_SCALED') + '</div>';
      break;

    case 5:
      _str = '<div class="statusAlarma alarmaValidada" data-nls="NLS_STATE_VALIDATED">' + _Language.get('NLS_STATE_VALIDATED') + '</div>';
      break;
  }

  return $(_str);
  /*
   * <div class="statusAlarma alarmaPendiente">Pendiente</div>
   * 
   */
}

function Localize_Date(fecha) {
  var dateFormat;
  if (_Configuration.Language == 0)
    dateFormat = 'D/M/YYYY HH:mm:ss';
  else
    dateFormat = 'M/D/YYYY HH:mm:ss';

  return moment(fecha).format(dateFormat);
}

function WSI_Tasks_ui_Print_Localized_Date_Alarm_Created(data) {
  return Localize_Date(data.record.date_created);
}

function WSI_Tasks_ui_Print_Localized_Date_Task_Created(data) {
  return Localize_Date(data.record.creation);
}

function WSI_Tasks_ui_Print_Localized_Date_Task_Updated(data) {
  return Localize_Date(data.record.last_status_update)
}

function WSI_Tasks_ui_Print_UserName(data) {
  var _result = "";
  var _is_role = false;

  if (data.record.assigned_user_id <= 0) {
    if ((data.record.assigned_role_id & 1) == 1) {
      _result += _Language.get('NLS_ROLE_PCA');
    }

    if ((data.record.assigned_role_id & 2) == 2) {
      if (_result != "") {
        _result += ",";
      }
      _result += _Language.get('NLS_ROLE_OPM');
    }

    if ((data.record.assigned_role_id & 4) == 4) {
      if (_result != "") {
        _result += ",";
      }
      _result += _Language.get('NLS_ROLE_SLA');
    }

    if ((data.record.assigned_role_id & 8) == 8) {
      if (_result != "") {
        _result += ",";
      }
      _result += _Language.get('NLS_ROLE_TSP');
    }

  } else {
    return WSI_Tasks_ui_Message_Action(data);
  }

  return _result;
}

//function WSI_Tasks_ui_PrintRowAlarmTypeTask(data) {

//  var _result = "";

//  if (data.record.subcategory == "99000" ||
//      data.record.subcategory == "98000" ||
//      data.record.subcategory == "97000")
//    _result = data.record.title;
//  else
//    _result = _Legends.Dictionary_Alarms[data.record.category][data.record.subcategory];

//  return _result;
//}

function WSI_Tasks_ui_GetTasksFilters() {
  var _task_filter_list = [];
  var cb_alarm_type = document.getElementById("selTaskType");
  var cb_task_status = document.getElementById("selTaskStatus");
  var cb_task_priority = document.getElementById("selTaskPriority");
  var cb_task_user = document.getElementById("selTaskUser");
  var cb_task_role = document.getElementById("selTaskRole");

  var _alarm_type_value = cb_alarm_type.options[cb_alarm_type.selectedIndex].value;
  var _task_status_value = cb_task_status.options[cb_task_status.selectedIndex].value;
  var _task_priority_value = cb_task_priority.options[cb_task_priority.selectedIndex].value;
  var _task_user_value = cb_task_user.options[cb_task_user.selectedIndex].value;
  var _task_role_value = cb_task_role.options[cb_task_role.selectedIndex].value;

  if (_alarm_type_value != 0) {
    _task_filter_list["subcategory"] = _alarm_type_value;
  }

  if (_task_status_value != 0) {
    _task_filter_list["status"] = _task_status_value;
  }

  if (_task_priority_value != 0) {
    _task_filter_list["severity"] = _task_priority_value;
  }

  if (_task_user_value != 0) {
    _task_filter_list["assigned_user_id"] = +_task_user_value;
  }

  if (_task_role_value != 0) {
    _task_filter_list["assigned_role_id"] = +_task_role_value;
  }

  _table_tasks.ApplyFilter(_task_filter_list);

  WSI_Tasks_ui_RefreshCollapseStatusAlarms();
}

function WSI_Tasks_ui_GetAlarmFilters() {
  var _alarm_filter_list = [];
  var _alarm_filter_value = 0;
  var cb_alarm_type = document.getElementById("selAlarmStatus");

  _alarm_filter_value = cb_alarm_type.options[cb_alarm_type.selectedIndex].value;

  if (_alarm_filter_value != 0) {
    _alarm_filter_list["alarm_id"] = _alarm_filter_value;
  }

  _table_alarms.ApplyFilter(_alarm_filter_list);

  WSI_Tasks_ui_RefreshCollapseStatusAlarms();

}

//*******************************

function WSI_Tasks_ui_FormatJTables() {
  WSI_Tasks_ui_RefreshCollapseStatusAlarms();
}

function WSI_Tasks_ui_Print_Remaining_Time(data) {
  var _result = "";

  if (data.record.remaining_time.trim() == "") {
    _result = _Language.get('NLS_N_HOURS');
  } else {
    _result = data.record.remaining_time;
  }

  return _result;
}