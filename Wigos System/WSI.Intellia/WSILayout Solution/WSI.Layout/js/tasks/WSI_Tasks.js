﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WSI_Tasks.js
// 
//   DESCRIPTION: Contains classes and methods to manage tasks.
// 
//        AUTHOR: Jaume Barnés
// 
// CREATION DATE: 03/07/2015
// 
//------------------------------------------------------------------------------

TaskManager.prototype.constructor = TaskManager;
var _alarmsTask = -1;
function TaskManager() {
  /// <summary>
  /// Create an instance of TaskManager object.
  /// </summary>

  this.Alarms = [];
  this.Tasks = [];
  this.Users = [];
  this.locked = false;
  this.ThreadLastUpdate = 0;
  this.AlarmsTasksEventsLastUpdate = 0;
  this.LastTasks = [];
}

TaskManager.prototype.Init = function (Data) {
}

function Alarm() {

}

function Tasks() {
  /// <summary>
  /// Create an instance of TaskManager object.
  /// </summary>

  this.id;
  this.terminal_id;
  this.severity;
  this.user;
  this.description;
  this.date_created;
  this.remaining_time;
  this.date_updated;
  this.status;
  this.start;
  this.end;
  this.category;
  this.subcategory;
  this.account_id;
  this.creation_user_id;
  this.creation;
  this.assigned_role_id;
  this.assigned_user_id;
  this.assigned;
  this.accepted_user_id;
  this.accepted;
  this.scale_from_user_id;
  this.scale_to_user_id;
  this.scale_reason;
  this.scale;
  this.solved_user_id;
  this.solved;
  this.validate_user_id;
  this.validate;
  this.last_status_update_user_id;
  this.last_status_update;
  this.attached_media;
  this.history;
  this.title;
}

TaskManager.prototype.AddTasks = function (TasksList) {
  /// <summary>
  /// Add task to TaskManager.Tasks using JSON list
  /// </summary>
  /// <param name="TasksList"> TaskList object</param>

  if (TasksList) {
    for (var _task in TasksList) {
      this.AddTask(TasksList[_task]);
    }
  }
}

TaskManager.prototype.AddAlarms = function (AlarmsList) {
  /// <summary>
  /// Add alarms to TaskManager.Tasks using JSON list
  /// </summary>
  /// <param name="AlarmsList"></param>
  if (AlarmsList) {
    for (var _alarm in AlarmsList) {
      this.AddAlarm(AlarmsList[_alarm]);
    }
  }
}

TaskManager.prototype.AddLastTasks = function (TasksList) {
  /// <summary>
  /// Add a list of last tasks of a terminal into a list.
  /// </summary>
  /// <param name="TasksList"></param>
  if (TasksList) {
    for (_last_task in TasksList) {
      this.AddLastsTasks(TasksList[_last_task]);
    }
  }
}

TaskManager.prototype.AddLastsTasks = function (Task) {
  /// <summary>
  /// Add a list of last tasks of a terminal into a list.
  /// </summary>
  /// <param name="TasksList"></param>

  var _task = {};

  _task.terminal_id = +Task[0];
  _task.category = +Task[1];
  _task.subcategory = +Task[2];
  _task.severity = +Task[3];
  _task.assigned_role_id = +Task[4];
  _task.assigned_user_id = +Task[5];
  _task.creation = Task[6];
  _task.status = +Task[7];
  _task.title = Task[32];

  if (!this.LastTasks[_task.terminal_id]) {
    this.LastTasks[_task.terminal_id] = [];
  }

  this.LastTasks[_task.terminal_id].push(_task)
}

TaskManager.prototype.AddTask = function (TaskData) {
  /// <summary>
  /// Add task to TaskManager.Tasks
  /// </summary>
  /// <param name="TaskData"></param>

  var _task = {};

  _task.id = +TaskData[0];
  _task.status = +TaskData[1];
  _task.start = TaskData[2];
  _task.end = TaskData[3];
  _task.category = +TaskData[4];
  _task.subcategory = +TaskData[5];
  _task.terminal_id = +TaskData[6];
  _task.account_id = +TaskData[7];
  _task.description = decodeURIComponent(TaskData[8]);
  _task.severity = +TaskData[9];
  _task.creation_user_id = +TaskData[10];
  _task.creation = TaskData[11];
  _task.assigned_role_id = +TaskData[12];
  _task.assigned_user_id = +TaskData[13];
  _task.assigned = TaskData[14];
  _task.accepted_user_id = +TaskData[15];
  _task.accepted = TaskData[16];
  _task.scale_from_user_id = +TaskData[17];
  _task.scale_to_user_id = +TaskData[18];
  _task.scale_reason = decodeURIComponent(TaskData[19]);
  _task.scale = TaskData[20];
  _task.solved_user_id = +TaskData[21];
  _task.solved = TaskData[22];
  _task.validate_user_id = +TaskData[23];
  _task.validate = TaskData[24];
  _task.last_status_update_user_id = +TaskData[25];
  _task.last_status_update = TaskData[26];
  _task.attached_media_id = TaskData[27];
  _task.attached_media = TaskData[28];
  _task.remaining_time = TaskData[29];
  _task.history = decodeURIComponent(TaskData[30]);
  _task.assigned_name = TaskData[31];
  _task.title = TaskData[32];

  var addTask = true;

  if (_task.terminal_id != "") {
    var _terminal = _Manager.Terminals.Items[_Manager.Terminals.ByTerminal[_task.terminal_id]];

    //SL 2/11 - RETIRED TERMINALS NOT SHOW IN TASK
    if (_terminal.Properties.status == 2)
      addTask = false;
  }

  if (addTask)
    this.Tasks.push(_task)
}

TaskManager.prototype.AddUsers = function (UsersList) {
  /// <summary>
  /// Add a list of Users into TaskManager.
  /// </summary>
  /// <param name="UsersList"></param>
  if (UsersList) {
    for (var _users in UsersList) {
      var user_data = UsersList[_users];
      this.Users[user_data[0]] = user_data[1];
    }
  }
}

TaskManager.prototype.AddAlarm = function (AlarmData) {
  /// <summary>
  /// Add alarm to TaskManager.Alarms
  /// </summary>
  /// <param name="AlarmData"></param>

  /*
 * terminal_id 0
 * alarm_id 1
 * date_created 2
 * date_remaining 3
 * id 4 
 * alarm_type 5
 * user_created 6
 * data 7
 * description 8
 * source 9
 */

  var _alarm = {};

  _alarm.terminal_id = +AlarmData[0];
  _alarm.id = +AlarmData[4];
  _alarm.alarm_id = AlarmData[1]
  _alarm.alarm_type = +AlarmData[5];
  _alarm.date_created = AlarmData[2];
  _alarm.user_created = AlarmData[6];
  _alarm.media_id = AlarmData[7]
  _alarm.media = AlarmData[8];
  _alarm.remaining_time = AlarmData[3].substring(0, 8);
  _alarm.description = decodeURIComponent(AlarmData[9]);
  _alarm.has_attachment = (_alarm.media != "" || _alarm.description != "");
  _alarm.alarm_source = +AlarmData[10];
  _alarm.status = +AlarmData[11];
  _alarm.terminal_name = AlarmData[12];
  _alarm.title = AlarmData[13];
  //this.Alarms[_alarm.id] = _alarm;

  var addAlarm = true;

  if (_alarm.terminal_id != "") {
    var _terminal = _Manager.Terminals.Items[_Manager.Terminals.ByTerminal[_alarm.terminal_id]];

    //SL 2/11 - RETIRED TERMINALS NOT SHOW IN ALARMS
    if (_terminal.Properties.status == 2)
      addAlarm = false;
  }

  if (addAlarm)
    this.Alarms.push(_alarm);
}

TaskManager.prototype.Update = function () {
  /// <summary>
  /// Refresh TaskManager.Alarms and TaskManager.Tasks from DB (PageMethod)
  /// </summary>

  PageMethods.GetSiteTasksData(OnGetSiteTasksDataComplete, OnGetSiteTasksDataFailed);
}

function OnGetSiteTasksDataComplete(result, userContext, methodName) {
  /// <summary>
  /// CallBack for _Manager.TaskManager.Update
  /// </summary>
  /// <param name="result"></param>
  /// <param name="userContext"></param>
  /// <param name="methodName"></param>
  try {

    if (result != null) {

      WSILogger.Debug("OnGetSiteTasksDataComplete()", "DATA_SIZE", result.length);

      if (result != "") {

        // Parse received data
        var _data = JSON.parse(result);

        if (_Manager.TaskManager.ThreadLastUpdate == 0 || (_Manager.TaskManager.ThreadLastUpdate > _Manager.TaskManager.AlarmsTasksEventsLastUpdate)) {
          _Manager.TaskManager.Alarms = [];
          _Manager.TaskManager.Tasks = [];
          _Manager.TaskManager.LastTasks = [];

          if (_alarmsTask < _data.Alarms.rows.length && _alarmsTask != -1 && _Configuration.SoundEnabled) {
            $("#sound_taskAlarm")[0].load();
            $("#sound_taskAlarm")[0].play();
          }
          else {
            $("#sound_taskAlarm")[0].pause();
            if ($("#sound_taskAlarm")[0].readyState > 0) {
              $("#sound_taskAlarm")[0].currentTime = 0;
            }
          }
          _alarmsTask = _data.Alarms.rows.length;

          _Manager.TaskManager.AddAlarms(_data.Alarms.rows);
          _Manager.TaskManager.AddTasks(_data.Tasks.rows);
          _Manager.TaskManager.AddUsers(_data.Users.rows);
          _Manager.TaskManager.AddLastTasks(_data.LastTasks.rows);

          _Manager.TaskManager.AlarmsByTerminal = [];
          _Manager.TaskManager.TasksByTerminal = [];

          for (_idx_site_task_alarms in _Manager.TaskManager.Alarms) {
            var _obj_alarm = _Manager.TaskManager.Alarms[_idx_site_task_alarms];
            var _info_alarm = {
              id: _obj_alarm.alarm_id,
              type: _obj_alarm.alarm_type
            }
            if (!_Manager.TaskManager.AlarmsByTerminal[_obj_alarm.terminal_id]) {
              _Manager.TaskManager.AlarmsByTerminal[_obj_alarm.terminal_id] = [];
            }
            _Manager.TaskManager.AlarmsByTerminal[_obj_alarm.terminal_id].push(_info_alarm);
          }

          for (_idx_site_task in _Manager.TaskManager.Tasks) {
            var _obj_task = _Manager.TaskManager.Tasks[_idx_site_task];
            var _info_task = {
              category: _obj_task.category,
              subcategory: _obj_task.subcategory,
              severity: _obj_task.severity,
              elapsed_time: _obj_task.remaining_time,
              assigned_role_id: _obj_task.assigned_role_id,
              assigned_user_id: _obj_task.assigned_user_id,
              status: _obj_task.status
            }
            if (!_Manager.TaskManager.TasksByTerminal[_obj_task.terminal_id]) {
              _Manager.TaskManager.TasksByTerminal[_obj_task.terminal_id] = [];
            }

            _Manager.TaskManager.TasksByTerminal[_obj_task.terminal_id].push(_info_task);
          }
        }

        if (_Manager.TaskManager.ThreadLastUpdate == 0) {
          WSI_Tasks_ui_Init();
        }

        _Manager.TaskManager.ThreadLastUpdate = +_data.LastUpdate;

      } else {
        _Manager.TaskManager.Alarms = [];
        _Manager.TaskManager.Tasks = [];
      }

      if (_Manager.TaskManager.Users[m_current_user_id]) {
        m_current_user_is_runner = 1;
      } else {
        m_current_user_is_runner = 0;
      }


      WSILogger.Write('Finished: OnGetSiteTasksDataComplete');

      //if (m_current_section == CONST_SECTION_ID_TASKS) {
      WSI_Tasks_ui_Refresh();
      //}

      //_Manager.TaskManager.AlarmsTasksEventsLastUpdate = Date.parse(new Date);
      _Manager.TaskManager.locked = false;

    } else {
      // No data received
      WSILogger.Write("OnGetSiteTasksDataComplete() -> NO DATA");
    }

  } catch (error) {
    WSILogger.Debug("OnGetSiteTasksDataComplete()", "error", error);
  }

}

function OnGetSiteTasksDataFailed(error, userContext, methodName) {
  /// <summary>
  /// CallBack for _Manager.TaskManager.Update
  /// </summary>
  /// <param name="error"></param>
  /// <param name="userContext"></param>
  /// <param name="methodName"></param>
  WSILogger.Debug("OnGetSiteMetersDataFailed()", "error", error);
}

TaskManager.prototype.CreateTask = function (AlarmKey, RolId, UserId, Severity, Comment) {
  /// <summary>
  /// Assign Alarm to a User and Create Task
  /// </summary>
  /// <param name="AlarmKey"></param>
  /// <param name="RolId"></param>
  /// <param name="UserId"></param>
  /// <param name="Severity"></param>
  /// <param name="Comment"></param>

  var _alarm;

  for (_idx_site_alarms in this.Alarms) {
    if (this.Alarms[_idx_site_alarms].id == AlarmKey) {
      _alarm = this.Alarms[_idx_site_alarms];

      break;
    }
  }
  var _old_task_history;
  //var _time = GetNowDateTime();
  var _time = m_wgdb_date._i;
  //_time = moment(_time).format('M/D/YYYY hh:mm:ss A');
  var _str_username = m_current_user_name.toUpperCase();

  _old_task_history = "<div class='taskHistory'><i>" + _time + "</i>  " + _str_username + "  <span data-nls='NLS_AUTO_NOTIFICATION_TASK_CREATED'>Tarea creada</span></div>";
  _old_task_history = "<br><div class='taskHistory'><i>" + _time + "</i>  " + _str_username + "  <b><span data-nls='NLS_HISTORY_COMMENTS'>Comentario:</span></b> " + Comment + "</div>" + _old_task_history;

  var _task_old = {
    id: -1,
    terminal_id: _alarm.terminal_id,
    severity: Severity,
    assigned_role_id: RolId,
    assigned_user_id: UserId,
    description: Comment,
    status: 1, //ASSIGNED
    category: _alarm.alarm_type,
    attached_media: _alarm.media,
    attached_media_id: _alarm.media_id,
    category: _alarm.alarm_type,
    subcategory: _alarm.alarm_id,
    history: _old_task_history,
    title: _alarm.title
  };

  LayoutProgressOpen();
  _Manager.TaskManager.locked = true;

  PageMethods.AssignAlarm(JSON.stringify(_alarm), JSON.stringify(_task_old), function (Task) {

    var _task = JSON.parse(Task);

    OnAssignAlarmComplete(_task, _alarm, _task_old);
  }
  , OnAssignAlarmFailed);
}

function OnAssignAlarmComplete(Task, Alarm, OldTask) {
  /// <summary>
  /// CallBack from _Manager.TaskManager.CreateTask
  /// </summary>
  /// <param name="Task"></param>
  /// <param name="Alarm"></param>
  /// <param name="OldTask"></param>
  var _task = {
  };

  _task.id = Task.ID;
  _task.status = Task.Status;
  _task.start = "";
  _task.end = "";
  _task.category = Task.Category;
  _task.subcategory = Task.Subcategory;
  _task.terminal_id = Task.TerminalId;
  _task.account_id = Task.AccountId;
  _task.description = Task.Description;
  _task.severity = Task.Severity;
  _task.creation_user_id = Task.CreationUserId;
  _task.creation = new Date(parseInt(Task.Creation.substr(6))).format('dd/MM/yyyy hh:mm');
  _task.assigned_role_id = Task.AssignedRoleId;
  _task.assigned_user_id = Task.AssignedUserId;
  _task.assigned = new Date(parseInt(Task.Assigned.substr(6))).format('dd/MM/yyyy hh:mm');
  _task.accepted_user_id = -1;
  _task.accepted = "";
  _task.scale_from_user_id = "";
  _task.scale_to_user_id = "";
  _task.scale_reason = "";
  _task.scale = "";
  _task.solved_user_id = "";
  _task.solved = "";
  _task.validate_user_id = "";
  _task.validate = "";
  _task.last_status_update_user_id = Task.LastStatusUpdateUserId;
  _task.last_status_update = new Date(parseInt(Task.LastStatusUpdate.substr(6))).format('dd/MM/yyyy hh:mm');
  _task.attached_media_id = OldTask.attached_media_id;
  _task.attached_media = OldTask.attached_media;
  _task.remaining_time = "00:00:00";
  _task.history = OldTask.history;
  _task.title = "";

  _Manager.TaskManager.Tasks.push(_task);

  for (_idx in _Manager.TaskManager.Alarms) {
    if (_Manager.TaskManager.Alarms[_idx].id == Alarm.id) {
      _Manager.TaskManager.Alarms.splice(_idx, 1);

      break;
    }
  }

  WSI_Tasks_ui_Refresh();
  WSI_Tasks_ui_After_AssignAlarm();

  LayoutProgressClose();

  _Manager.TaskManager.AlarmsTasksEventsLastUpdate = GetTimeSpanNow();
  _Manager.TaskManager.locked = false;

}

function OnAssignAlarmFailed() {
  /// <summary>
  /// CallBack from _Manager.TaskManager.CreateTask
  /// </summary>
  LayoutProgressClose();
}

TaskManager.prototype.SaveTask = function (Task) {
  /// <summary>
  /// Save Task modification into DB
  /// </summary>
  /// <param name="Task"></param>
  LayoutProgressOpen();
  _Manager.TaskManager.locked = true;

  PageMethods.SaveSiteTask(JSON.stringify(Task), function (Task) { OnSaveSiteTaskComplete(Task); }, OnSaveSiteTaskFailed);
}

function OnSaveSiteTaskComplete(Task) {
  /// <summary>
  /// CallBack from _Manager.TaskManager.SaveTask
  /// </summary>
  /// <param name="Task"></param>

  WSI_Tasks_ui_After_SaveTask(JSON.parse(Task));
}

function OnSaveSiteTaskFailed(error, userContext, methodName) {
  /// <summary>
  /// CallBack from _Manager.TaskManager.SaveTask
  /// </summary>
  /// <param name="error"></param>
  /// <param name="userContext"></param>
  /// <param name="methodName"></param>
}

TaskManager.prototype.DeleteAlarm = function (Alarm, Sender, SenderClick) {
  /// <summary>
  /// Delete alarm 
  /// </summary>
  /// <param name="Alarm"></param>
  /// <param name="Sender"></param>
  /// <param name="SenderClick"></param>

  PageMethods.SaveSiteAlarm(JSON.stringify(Alarm), function () { WSI_Tasks_ui_DeleteAlarmRowComplete(Alarm.id, Sender, SenderClick); }, WSI_Tasks_ui_DeleteAlarmRowFailed);
}

TaskManager.prototype.UpdateAlarm = function (Alarm) {
  /// <summary>
  /// Update Alarm values
  /// </summary>
  /// <param name="Alarm"></param>

  PageMethods.SaveSiteAlarm(JSON.stringify(Alarm), OnSaveSiteAlarmComplete, OnSaveSiteAlarmFailed);
}

function OnSaveSiteAlarmComplete(result, userContext, methodName) {
}

function OnSaveSiteAlarmFailed(error, userContext, methodName) {

}
