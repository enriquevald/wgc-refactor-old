﻿function WSIUserConfiguration() {

    // UserConfiguration Object
    this.ShowActivity = true;           // Boolean
    this.ShowAlarms = false;            // Boolean
    this.ShowBackground = true;         // Boolean
    this.ShowGrid = false;              // Boolean
    this.ShowAreas = false;             // Boolean
    this.ShowBanks = false;             // Boolean
    this.ShowHeatMap = false;           // Boolean
    this.ShowHeatMapPlayer = false;     // Boolean
    this.ShowGamingTables = false;      // Boolean
    this.ShowRealTimeRefresh = false;   // Boolean
  
    this.TestEnabled = false;           // Boolean
    this.RealTimeRefreshSeconds = 0;    // Int32
    this.TerminalAppearance3D = 0;      // Int32

    this.ViewMode = 2;                  // Int32

    this.DefaultView = 1;               // Int32

    this.PlayAlarmSound = true;         // Boolean

    // RMS - Current Floor 
    this.CurrentFloorId = 1;             // Int32

  //Sound System
    this.SoundEnabled = true;

  //List of selected alarms checkboxes
    this.AlarmsToSound = [];
}

var _Configuration = new WSIUserConfiguration();