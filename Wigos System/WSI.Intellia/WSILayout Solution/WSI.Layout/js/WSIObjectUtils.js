﻿// ---------------------------------------------------------------------

// Object with utilities for creating meshes and others
var WSIObjectUtils = {

  // JSON loader for parsing models
  //JSLoader: new THREE.JSONLoader(),
  JSLoader: new WSIJSONLoader(),

  //
  // Converts a string that contains a JSON model into an object 
  ProcessModel: function (ModelString) {
    try {
      var _model = jQuery.parseJSON(ModelString);
      return this.JSLoader.parse(_model);
    } catch (err) {
      WSILogger.Log("WSI.ProcessModel()", err);
      return null;
    }
  },

  // Creates a full LOD (Level of detail object)
  //  - geometries = [[ THREE.Geometry, Distance of rendering ], ...]
  //  - materials = [ material, ... ]
  CreateLODObject: function (geometries, materials, x, y, z, scalex, scaley, scalez, orientation, name) {
    try {
      var _new_lod = new THREE.LOD();

      var _mesh;

      WSI.objectcounter++;

      for (i = 0; i < geometries.length; i++) {
        _mesh = null;
        _mesh = new THREE.Mesh(geometries[i][0], new THREE.MeshFaceMaterial(materials[i]));
        _mesh.id = "LodObject" + WSI.objectcounter + "_" + i;

        _mesh.name = "LodObject" + WSI.objectcounter + "_" + i;

        _new_lod.addLevel(_mesh, geometries[i][1]);

      }

      //            _new_lod.position.x = x + 0.5;
      //            _new_lod.position.y = y;
      //            _new_lod.position.z = z + 0.5;
      //            _new_lod.scale.set(scalex, scaley, scalez);
      //            _new_lod.rotation.y = -(45 * orientation) * Math.PI / 180;
      //            _new_lod.updateMatrix();
      //            _new_lod.matrixAutoUpdate = false;
      _new_lod.userData = { old_color: 0, counter: (WSI.objectcounter - 1) };

      //WSIData.Terminals.push(_new_lod.name);

      return _new_lod;
    } catch (err) {
      WSILogger.Log("WSI.CreateLODObject()", err);
      return null;
    }
  },

  // Change the material of all LOD subobjects
  ChangeLODMaterial: function (LODObject, NewColor) {
    try {
      if (LODObject instanceof THREE.LOD) {
        for (var oi = 0; oi < LODObject.objects.length; oi++) {
          LODObject.objects[oi].object.material = new THREE.MeshLambertMaterial({ color: NewColor, ambient: 0x00ff80, shading: THREE.FlatShading });
          LODObject.objects[oi].object.material.vertexColors = THREE.FaceColors;
        }
      }
    } catch (err) {
      WSILogger.Log("WSI.ChangeLODMaterial()", err);
      return null;
    }

  },

  // Change the material of all LOD subobjects
  ChangeFacesMaterial: function (Object, NewColor) {
    try {
      if (Object instanceof THREE.Mesh) {

        //mat_color_1

        for (var oi = 0; oi < Object.material.materials.length; oi++) {
          if (Object.material.materials[oi].name == 'mat_color_1') {
            //Object.material.materials[oi] = new THREE.MeshLambertMaterial({ name: 'mat_color_1', color: NewColor, ambient: NewColor, shading: THREE.NoShading, overdraw: true });
            Object.material.materials[oi] = new THREE.MeshLambertMaterial({ name: 'mat_color_1', color: NewColor, ambient: NewColor, shading: THREE.NoShading, overdraw: true });
            //Object.material.materials[oi] = new THREE.MeshLambertMaterial({ name: 'mat_color_1', color: NewColor, shading: THREE.FlatShading, overdraw: true });
            //Object.material.materials[oi] = new THREE.MeshBasicMaterial({ name: 'mat_color_1', color: NewColor, shading: THREE.NoShading });
            //Object.material.materials[oi].vertexColors = THREE.VertexColors;
          }

        }
      }
    } catch (err) {
      WSILogger.Log("WSI.ChangeFacesMaterial()", err);
      return null;
    }

  },

  //

  CreateObjectStats3: function (position, orientation, parentname) {

    var rectLength;
    var rectWidth;
    var rectShape;

    var geometry;
    var color;
    var mat;
    var pivot = new THREE.Object3D();

    var mats = [];

    var mergedGeo = null;
    var statsmesh = null;

    for (var _i = 0; _i < 4; _i++) {
      rectLength = Math.random() * 1.5;
      rectWidth = 0.2;

      rectShape = new THREE.Shape();
      rectShape.moveTo(0, 0);
      rectShape.lineTo(0, rectWidth);
      rectShape.lineTo(rectLength, rectWidth);
      rectShape.lineTo(rectLength, 0);
      rectShape.lineTo(0, 0);

      color = Math.floor(Math.random() * 100);

      switch (true) {
        case (color >= 0 && color <= 25):
          mat = new THREE.MeshLambertMaterial({ color: 0xff0000 });
          break;
        case (color >= 26 && color <= 50):
          mat = new THREE.MeshLambertMaterial({ color: 0xaaaa00 });
          break;
        case (color >= 51 && color <= 75):
          mat = new THREE.MeshLambertMaterial({ color: 0x00aa00 });
          break;
        default:
          mat = new THREE.MeshLambertMaterial({ color: 0x00ff00 });
          break;
      }

      geometry = new THREE.ShapeGeometry(rectShape);

      //            var mesh = new THREE.Mesh(geometry, mat);
      //            //mesh.position.set(position.x - 2.5, position.y + 0.1, position.z + (0.2 * _i));
      //            //mesh.position.set(position.x - (0.2 * _i), position.y + 0.1, position.z + 2.5);
      //            mesh.position.set(-(0.2 * _i), 0.1, 2.5);
      //            //mesh.rotation.y = 180 * Math.PI / 180;
      //            mesh.rotation.x = -90 * Math.PI / 180;
      //            mesh.rotation.z = 90 * Math.PI / 180;

      //            mesh.name = parentname + "_stat" + _i;

      //            mesh.userData = { objectType: "stat" };

      //            pivot.add(mesh);

      THREE.GeometryUtils.Merge(mergedGeo, geometry);
      mats.push(mat);
    }



    //pivot.name = parentname + "_statpivot";

    //WSIData.Terminal_Stats.push(pivot.name);

    //return pivot;

    mergedGeo.computeFaceNormals();
    statsmesh = new THREE.Mesh(mergedGeo, mats);

    return statsmesh;
  },

  //
  CreateObjectStats2: function (position, orientation, parentname) {

    var rectLength;
    var rectWidth;
    var rectShape;
    var geometry;
    var color;
    var mat;
    var pivot = new THREE.Object3D();

    for (var _i = 0; _i < 4; _i++) {
      rectLength = Math.random() * 1.5;
      rectWidth = 0.2;

      rectShape = new THREE.Shape();
      rectShape.moveTo(0, 0);
      rectShape.lineTo(0, rectWidth);
      rectShape.lineTo(rectLength, rectWidth);
      rectShape.lineTo(rectLength, 0);
      rectShape.lineTo(0, 0);

      color = Math.floor(Math.random() * 100);

      switch (true) {
        case (color >= 0 && color <= 25):
          mat = new THREE.MeshLambertMaterial({ color: 0xff0000 });
          break;
        case (color >= 26 && color <= 50):
          mat = new THREE.MeshLambertMaterial({ color: 0xaaaa00 });
          break;
        case (color >= 51 && color <= 75):
          mat = new THREE.MeshLambertMaterial({ color: 0x00aa00 });
          break;
        default:
          mat = new THREE.MeshLambertMaterial({ color: 0x00ff00 });
          break;
      }

      geometry = new THREE.ShapeGeometry(rectShape);

      var mesh = new THREE.Mesh(geometry, mat);
      //mesh.position.set(position.x - 2.5, position.y + 0.1, position.z + (0.2 * _i));
      //mesh.position.set(position.x - (0.2 * _i), position.y + 0.1, position.z + 2.5);
      //mesh.position.set(-(0.2 * _i), 0.1, 2.5);
      mesh.position.set(0.5 + (0.2 * _i), 3.01, 1);
      //mesh.rotation.y = 180 * Math.PI / 180;
      mesh.rotation.x = -90 * Math.PI / 180;
      mesh.rotation.z = 90 * Math.PI / 180;

      mesh.name = parentname + "_stat" + _i;

      mesh.userData = { objectType: "stat" };

      pivot.add(mesh);
    }

    pivot.name = parentname + "_statpivot";

    WSIData.Terminal_Stats.push(pivot.name);

    return pivot;

  },

  CreateObjectStats: function (position, parentname) {
    var geometry;
    var material;
    var cylinder;
    var stats = new THREE.Object3D();

    for (var _i = 0; _i < 4; _i++) {
      var _l = Math.round(Math.random() * 25);
      geometry = new THREE.CylinderGeometry(0.2, 0.2, _l / 10, 4);
      material = new THREE.MeshBasicMaterial({ color: 0xffff00 });

      switch (true) {
        case (_l < 6):
          cylinder = new THREE.Mesh(geometry, material);
          break;
        case (_l < 11):
          cylinder = new THREE.Mesh(geometry, material);
          break;
        case (_l < 16):
          cylinder = new THREE.Mesh(geometry, material);
          break;
        case (_l < 21):
          cylinder = new THREE.Mesh(geometry, material);
          break;
        case (_l < 26):
          cylinder = new THREE.Mesh(geometry, material);
          break;
        default:
          alert(_l);
          break;
      }

      cylinder.position.x = position.x - 2;
      cylinder.position.y = 0.1; // +(_l / 10);
      cylinder.position.z = position.z + (_i * 0.5);

      cylinder.rotation.y = 45 * Math.PI / 180;
      //cylinder.rotation.z = 45 * Math.PI / 180;

      cylinder.updateMatrix();

      //        var _pivot = new THREE.Object3D();
      //        _pivot.add(cylinder);


      stats.add(cylinder);
    }

    //stats.rotation.y = (-45 * orientation) * Math.PI / 180;
    //scene.add(stats);

    stats.name = parentname + "_stats";

    WSIData.Terminal_Stats.push(stats.name);

    return stats;
  },

  //

  //    Orientate: function (object, orientation) {

  //        if (object) {
  //            object.rotation.y = - (45 * orientation) * Math.PI / 180;
  //        }

  //    },

  //

  CreateObjectLabel2: function (index, text, x, y, z, size, color) {
    try {
      var _text = new THREE.TextGeometry(text, { size: size, curveSegments: 3, font: "helvetiker", style: "normal" });
      var _mesh = new THREE.Mesh(_text, new THREE.MeshBasicMaterial({ color: color }));

      _mesh.position.y = 0.1;
      _mesh.position.x = 0.5;

      return _mesh;
    } catch (err) {
      WSILogger.Log("", err);
    }
  },

  CreateObjectLabel3: function (index, text, x, y, z, size, color, parentname) {
    var canvas = document.createElement('canvas');
    var size = 245; // CHANGED
    canvas.width = size;
    canvas.height = size;
    var context = canvas.getContext('2d');
    context.fillStyle = color; // CHANGED
    context.textAlign = 'center';
    context.font = '40 px Arial Black';
    context.fillText(text, size / 2, size / 2);

    var amap = new THREE.Texture(canvas);
    amap.needsUpdate = true;

    var mat = new THREE.SpriteMaterial({
      map: amap,
      transparent: false,
      useScreenCoordinates: false,
      color: 0xff0000 // CHANGED
    });

    var sp = new THREE.Sprite(mat);

    sp.position.y = y;
    sp.position.z = z;
    sp.position.x = x;

    sp.scale.set(10, 10, 1); // CHANGED

    sp.name = "label_" + parentname;
    sp.userData = { objectType: "label" };

    WSIData.Terminal_Labels.push(sp.name);

    return sp;
  },

  CreateObjectLabel: function (index, text, x, y, z, size, color, backGroundColor, backgroundMargin) {

    if (!backgroundMargin)
      backgroundMargin = 5;

    var canvas = document.createElement("canvas");

    var context = canvas.getContext("2d");
    context.font = size + "pt Arial";

    var textWidth = context.measureText(text).width;

    canvas.width = textWidth + backgroundMargin;
    canvas.height = size + backgroundMargin;
    context = canvas.getContext("2d");
    context.font = size + "pt Verdana";

    if (backGroundColor) {
      context.fillStyle = backGroundColor;
      context.fillRect(canvas.width / 2 - textWidth / 2 - backgroundMargin / 2, canvas.height / 2 - size / 2 - +backgroundMargin / 2, textWidth + backgroundMargin, size + backgroundMargin);
    }

    context.textAlign = "center";
    context.textBaseline = "middle";
    context.fillStyle = color;
    context.fillText(text, canvas.width / 2, canvas.height / 2);

    context.strokeStyle = "white";
    context.strokeRect(0, 0, canvas.width, canvas.height);

    var texture = new THREE.Texture(canvas);
    texture.needsUpdate = true;

    var material = new THREE.MeshBasicMaterial({ map: texture, colorAmbient: [0.4, 0.4, 0.4] });

    //var mesh = new THREE.Mesh(new THREE.PlaneGeometry(canvas.width, canvas.height), material);
    var mesh = new THREE.Mesh(new THREE.PlaneBufferGeometry(canvas.width, canvas.height), material);
    // mesh.overdraw = true;
    //mesh.doubleSided = true;
    mesh.position.x = x; //- canvas.width;
    mesh.position.y = y + 10; // - canvas.height;
    mesh.position.z = z;
    mesh.scale.set(0.5, 0.5, 0.5);

    //counter_labels++;
    mesh.name = 'terminal_label_' + index; // _labels;

    //alert(mesh.name);

    //mesh.lookAt(camera.position);
    mesh.updateMatrix();

    return mesh;
  },

  CreateIsland: function (name, start, end, color) {

    var width = Math.abs(end[0] - start[0]);
    var height = Math.abs(end[1] - start[1]);

    if (height == 0) { height += 5 };
    if (width == 0) { width += 5 };

    var material = new THREE.MeshBasicMaterial({ color: color, side: THREE.DoubleSide });
    //var geo = new THREE.PlaneGeometry(width, height);
    var geo = new THREE.PlaneBufferGeometry(width, height);

    var mesh = new THREE.Mesh(geo, material);

    mesh.name = name + "_island";

    mesh.position.x = start[0] - 5;
    mesh.position.y = -0.3;
    mesh.position.z = start[1];

    mesh.scale.set(1.3, 1.3, 1.3);

    mesh.rotation.x = -90 * Math.PI / 180;
    //mesh.rotation.z = 90 * Math.PI / 180;

    return mesh;
  },

  CreateIsland2: function (name, position, width, height, color) {

    //var material = new THREE.MeshBasicMaterial({ color: color, side: THREE.DoubleSide });
    var material = new THREE.MeshLambertMaterial({ color: Math.random() * 0xffffff });
    //var geo = new THREE.PlaneGeometry((width * 5), (height * 5));
    var geo = new THREE.BoxGeometry((width * 4) + 2, 4.5, (height * 4) + 2);

    var mesh = new THREE.Mesh(geo, material);

    mesh.name = name + "_island";

    //mesh.rotation.x = -90 * Math.PI / 180;

    mesh.position.x = position[0] + ((width * 4) / 2) - 2;
    mesh.position.y = 2.5;
    mesh.position.z = position[1] + ((height * 4) / 2) - 2;

    mesh.userData = {};
    //mesh.scale.set(1.3, 1.3, 1.3);

    //mesh.rotation.z = 90 * Math.PI / 180;

    return mesh;
  },

  CreateArea2: function (name, position, width, height, color) {

    //var material = new THREE.MeshBasicMaterial({ color: color, side: THREE.DoubleSide });
    var material = new THREE.MeshLambertMaterial({ color: Math.random() * 0xffffff });
    //var geo = new THREE.PlaneGeometry((width * 5), (height * 5));
    var geo = new THREE.BoxGeometry((width * 4) + 2, 4, (height * 4) + 2);

    var mesh = new THREE.Mesh(geo, material);

    mesh.name = name + "_area";

    //mesh.rotation.x = -90 * Math.PI / 180;

    mesh.position.x = position[0] + ((width * 4) / 2) - 2;
    mesh.position.y = 2.3;
    mesh.position.z = position[1] + ((height * 4) / 2) - 2;

    mesh.userData = {};
    //mesh.scale.set(1.3, 1.3, 1.3);

    //mesh.rotation.z = 90 * Math.PI / 180;

    return mesh;
  },

  DownColor: function (color, _step) {
    var _result = new THREE.Color(color);
    if (!_step) { _step = 0.75; }
    _result.r = ((_result.r - _step) > 0) ? (_result.r - _step) : 0;
    _result.g = ((_result.g - _step) > 0) ? (_result.g - _step) : 0;
    _result.b = ((_result.b - _step) > 0) ? (_result.b - _step) : 0;
    return _result;
  },

  rotateAroundObjectAxis: function (object, axis, radians) {
    rotObjectMatrix = new THREE.Matrix4();
    rotObjectMatrix.makeRotationAxis(axis.normalize(), radians);
    object.matrix.multiply(rotObjectMatrix);
    object.rotation.setFromRotationMatrix(object.matrix);
  },

  //ChangeMaterialColor: function (Mesh, MaterialName, NewColor) {
  //    if (Mesh.material.materials) {
  //        // MeshFaceMaterial
  //        for (var oi = 0; oi < Mesh.material.materials.length; oi++) {
  //            if (Mesh.material.materials[oi].name == MaterialName) {
  //                Mesh.material.materials[oi].color = new THREE.Color(NewColor);
  //                // Downgrade the emissive color to avoid reflection
  //                Mesh.material.materials[oi].emissive = WSIObjectUtils.DownColor(NewColor);
  //                Mesh.material.materials[oi].combine = THREE.AddOperation;
  //                // Replace the material 
  //                Mesh.material.materials[oi] = new THREE.MeshPhongMaterial(Mesh.material.materials[oi].clone());
  //            }
  //        }
  //    } else {
  //        // OtherMaterial
  //        if (Mesh.material.name == MaterialName) {
  //            Mesh.material.color = new THREE.Color(NewColor);
  //            // Downgrade the emissive color to avoid reflection
  //            Mesh.material.emissive = WSIObjectUtils.DownColor(NewColor);
  //            Mesh.material.combine = THREE.AddOperation;
  //            // Replace the material
  //            Mesh.material = new THREE.MeshPhongMaterial(Mesh.material.clone());
  //        }
  //    }
  //},

  ReplaceMaterial: function (Mesh, MaterialIndex, NewMaterial) {
    if (Mesh.material.materials) {
      Mesh.material.materials[MaterialIndex] = NewMaterial;
    }
  },

  GetMeshSize: function (Mesh) {
    var _result = undefined;
    if (Mesh.geometry) {
      Mesh.geometry.computeBoundingBox();
      _result = Mesh.geometry.boundingBox;
    }
    _result.max.x *= Mesh.scale.x;
    _result.max.y *= Mesh.scale.y;
    _result.max.z *= Mesh.scale.z;
    return _result;
  },

  Frustum: new THREE.Frustum(),
  ProjScreenMatrix: new THREE.Matrix4(),

  UpdateVisibility: function (Camera, Mesh, UpdateChildren) {

    if (!Mesh) { return false; }

    var _mesh = (Mesh.children[0].visible) ? Mesh.children[0] : Mesh

    UpdateChildren = (UpdateChildren) ? UpdateChildren : false;
    //this.ProjScreenMatrix.multiplyMatrices(Camera.projectionMatrix, Camera.matrixWorldInverse);
    //Camera.updateMatrixWorld();
    //Camera.matrixWorldInverse.getInverse(Camera.matrixWorld);
    //this.Frustum.setFromMatrix(new THREE.Matrix4().multiplyMatrices(Camera.projectionMatrix, Camera.matrixWorldInverse));

    var _state = this.Frustum.containsPoint(Mesh.position);

    //Mesh.visible = _state;
    //Mesh.children[0].visible = _state;

    if (UpdateChildren && _mesh.children && _mesh.children.length > 0) {
      _mesh.traverse(function (child) {
        if (child instanceof THREE.Mesh) { child.visible = _state; }
      });
    }

    return _state;
  },

  UpdateCameraProjMatrix: function (Camera) {
    this.ProjScreenMatrix.multiplyMatrices(Camera.projectionMatrix, Camera.matrixWorldInverse);
    Camera.updateMatrixWorld();
    Camera.matrixWorldInverse.getInverse(Camera.matrixWorld);
    this.Frustum.setFromMatrix(new THREE.Matrix4().multiplyMatrices(Camera.projectionMatrix, Camera.matrixWorldInverse));
  },

  Distance: function (Vector1, Vector2) {
    return Vector2.distanceTo(Vector1);
  },

  Anim: function (Position1, Position2, OnUpdate, OnComplete, Delay) {
    if (Delay == undefined) { Delay = 2000; }
    var tween = new TWEEN.Tween(Position1).to(Position2, Delay);
    tween.onUpdate(OnUpdate);
    if (OnComplete && OnComplete != null) {
      tween.onComplete(OnComplete);
    }
    tween.start();
  },

  CreateStaticObject: function (ResourceName) {
    if (!_Manager.StaticObjects) {
      _Manager.StaticObjects = [];
    }
    _Manager.StaticObjects.push(ResourceList.Items[ResourceName]);
  },

  UpdatePlayer: function (Terminal) {
    if (WSIData.ShowPlayers && Terminal.Mesh.material.visible == false) {
      switch (+Terminal.Properties.player_gender) {
        case 1:
          Terminal.Mesh.children[1].visible = true;
          Terminal.Mesh.children[2].visible = false;
          break;
        case 2:
          Terminal.Mesh.children[1].visible = false;
          Terminal.Mesh.children[2].visible = true;
          break;
        default:
          Terminal.Mesh.children[1].visible = false;
          Terminal.Mesh.children[2].visible = false;
          break;
      }
    } else {
      Terminal.Mesh.children[1].visible = false;
      Terminal.Mesh.children[2].visible = false;
    }
  },

  TerminalFront: function (TerminalOrientation) {
    var _top = 0, _left = 0;
    switch (+TerminalOrientation) {
      case 0:
        _left = -2;
        break;
      case 1:
      case 45:
        break;
      case 2:
      case 90:
        _top = 2;
        break;
      case 3:
      case 135:
        break;
      case 4:
      case 180:
        _left = 2;
        break;
      case 5:
      case 225:
        break;
      case 6:
      case 270:
        _top = -2;
        break;
      case 7:
      case 315:
        break;
    }
    return { x: _left, z: _top };
  },

  RoundSkyBox: function (ResourceName) {

    var _s_g = new THREE.SphereGeometry(800, 8, 8, 90 * Math.PI / 180);
    var _s_m = new THREE.MeshLambertMaterial({
      map: _ResourceList.Items[ResourceName].object,
      side: THREE.BackSide

    });

    var _s = new THREE.Mesh(_s_g, _s_m);
    _s.frustumCulled = false;
    _s.position.set(0, 0, 0);
    WSI.scene.add(_s);
  },

  BoxSkyBox: function (ResourceNamePrefix) {
    var directions = ["_xp", "_xn", "_yp", "_yn", "_zp", "_zn"];
    var skyGeometry = new THREE.CubeGeometry(_Manager.BackPlane.userData.width, 20, _Manager.BackPlane.userData.height);

    var materialArray = [];
    for (var i = 0; i < 6; i++) {
      _Manager.Resources.Items[ResourceNamePrefix + directions[i]].object.wrapS = THREE.RepeatWrapping;
      _Manager.Resources.Items[ResourceNamePrefix + directions[i]].object.wrapT = THREE.RepeatWrapping;

      switch (i) {
        case 0: // X+
          _Manager.Resources.Items[ResourceNamePrefix + directions[i]].object.repeat.set(4, 1);
          break;
        case 1: // X-
          _Manager.Resources.Items[ResourceNamePrefix + directions[i]].object.repeat.set(4, 1);
          break;
        case 2: // Y+
          _Manager.Resources.Items[ResourceNamePrefix + directions[i]].object.repeat.set(4, 8);
          break;
        case 3: // Y-
          _Manager.Resources.Items[ResourceNamePrefix + directions[i]].object.repeat.set(32, 32);
          break;
        case 4: // Z+
          _Manager.Resources.Items[ResourceNamePrefix + directions[i]].object.repeat.set(4, 1);
          break;
        case 5: // Z-
          _Manager.Resources.Items[ResourceNamePrefix + directions[i]].object.repeat.set(4, 1);
          break;
      }

      _Manager.Resources.Items[ResourceNamePrefix + directions[i]].object.needsUpdate = true;

      materialArray.push(new THREE.MeshBasicMaterial({
        map: _Manager.Resources.Items[ResourceNamePrefix + directions[i]].object,
        side: THREE.BackSide
      }));
      var skyMaterial = new THREE.MeshFaceMaterial(materialArray);
      var skyBox = new THREE.Mesh(skyGeometry, skyMaterial);
      skyBox.position.set(0, 9, 0);
      skyBox.visible = false;
      WSI.scene.add(skyBox);
    }

    return skyBox;
  },

  CylinderSkyBox: function (ResourceName) {
    var _r = (_Manager.BackPlane.userData.width > _Manager.BackPlane.userData.height) ? _Manager.BackPlane.userData.width : _Manager.BackPlane.userData.height;
    _r = _r / 2;
    var cylgeo = new THREE.CylinderGeometry(_r, _r, 50, 16, 1, false, false);
    var skybox = new THREE.Mesh(cylgeo, new THREE.MeshLambertMaterial({ map: _ResourceList.Items[ResourceName].object, side: THREE.BackSide }));
    skybox.position.set(0, 24, 0);
    WSI.scene.add(skybox);
  },

  CreateScreen: function (Width, Height, ResourceName) {
    var _monitor_mat = new THREE.MeshLambertMaterial({ map: _Manager.Resources.Items[ResourceName].object });
    _monitor_mat.ambien = new THREE.Color("#FFFFFF");
    return new THREE.Mesh(new THREE.PlaneBufferGeometry(Width, Height), _monitor_mat);
  },

  //Position: 0, 1, 2, 3, 4, 5, 6, 7
  GetDegrees: function (Position) {
    var _degrees;

    switch (+Position) {
      case 0:
        _degrees = 0; break;
      case 1:
        _degrees = 45; break;
      case 2:
        _degrees = 90; break;
      case 3:
        _degrees = 135; break;
      case 4:
        _degrees = 180; break;
      case 5:
        _degrees = 225; break;
      case 6:
        _degrees = 270; break;
      case 7:
        _degrees = 315; break;
      default:
        //_degrees = 0;
        _degrees = Position;
    }
    return _degrees; //+ "deg";

  } // GetDegrees

}

// ---------------------------------------------------------------------

