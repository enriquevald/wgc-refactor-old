﻿/*
var cssTransitionsSupported = false;
(function() {

var div = document.createElement('div');

div.setAttribute('style', 'transition:top 1s ease;-webkit-transition:top 1s ease;-moz-transition:top 1s ease;');

cssTransitionsSupported = !!(div.style.transition || div.style.webkitTransition || div.style.MozTransition);

delete div;

})();
*/

/////////////////////////////////////////////////////////////////////////////////////

var _Manager = new WSIManager();
/////////////////////////////////////////////////////////////////////////////////////

var CONST_INITIAL_ZOOM = "0.6";

var m_terminal_id_selected = null;
var m_browser;
var m_movements_timeout;
var m_movements_speed = 1;
var m_current_zoom = 1.0;
var m_current_background_position_left = 0;
var m_current_background_position_top = 0;
var m_current_popup_background_position_left = 0;
var m_current_popup_background_position_top = 0;
//var m_activity_sleep = 1000;

var m_div_background_is_clicked = false;
var m_popup_infoWindow_is_clicked = false;
var m_current_mouse_position_x;
var m_current_mouse_position_y;
_Configuration.ShowGrid = true;

var m_left_button_down = false;

var m_islands_positions;
m_islands_positions = [ // posX, posY, width, height
[-86, 2, 4, 1],
[-5, 2, 4, 1],
[20, 2, 4, 1],
[-88, 21, 2, 2],
[-68, 21, 2, 2],
[-46, 21, 2, 2],
[-26, 21, 2, 2],
[-1, 21, 2, 2],
[22, 21, 2, 2],
[-68, 38, 2, 4],
[-48, 38, 2, 4],
[-24, 38, 2, 4],
[-89, 62, 2, 3],
[-68, 62, 2, 3],
[-48, 62, 2, 3],
[-27, 62, 2, 3],
[-29, 84, 4, 1],
[-52, 84, 4, 1],
[-71, 84, 3, 1],
[-90, 84, 3, 1],
[-105, 84, 2, 1],
[36, 36, 4, 4],
[101, 18, 4, 1],
[122, -6, 1, 4],
[122, -28, 1, 4],
[146, -6, 2, 4],
[168, -6, 2, 4],
[146, 17, 2, 4],
[169, 19, 2, 4],
[146, 41, 2, 3],
[169, 41, 2, 3],
[101, 41, 2, 3],
[120, 41, 2, 3],
[146, 62, 2, 3],
[169, 62, 2, 3],
[101, 62, 2, 3],
[120, 62, 2, 3],
[80, 43, 2, 3],
[97, 84, 8, 1],
[138, 84, 4, 1],
[161, 84, 4, 1],
[136, -32, 3, 1],
[152, -32, 3, 1],
[168, -32, 3, 1],
[191, -10, 1, 3],
[190, 7, 1, 3],
[75, 78, 4, 3]
];


var m_areas_positions;
m_areas_positions = [ // posX, posY, width, height
[-172, 16, 16, 18],
[-106, 0, 24, 23],
[-8, 0, 18, 14],
[68, -20, 12, 8],
[119, -35, 20, 28],
[69, 16, 12, 15],
[69, 80, 28, 3]
];




/* test ///////////////////////////////////////////////////////////////////////*/


function fire(e) { }

function touchHandler(event) {
    var touches = event.changedTouches,
        first = touches[0],
        type = "";

    switch (event.type) {

        case "touchstart": type = "mousedown"; break;
        case "touchmove": type = "mousemove"; break;
        case "touchend": type = "mouseup"; break;
        default: return;
    }

    var simulatedEvent = document.createEvent("MouseEvent");
    simulatedEvent.initMouseEvent(type, true, true, window, 1,
                          first.screenX, first.screenY,
                          first.clientX, first.clientY, false,
                          false, false, false, 0/*left*/, null);

    first.target.dispatchEvent(simulatedEvent);
    event.preventDefault();
}

function init() {

    // Initialize background events
    var _control = document.getElementById("div_editor_background");
    if (_control) {
        _control.addEventListener("touchstart", touchHandler, true);
        _control.addEventListener("touchmove", touchHandler, true);
        _control.addEventListener("touchend", touchHandler, true);
        _control.addEventListener("touchcancel", touchHandler, true);
    }

    //Create Ribbon Toolbar
    CreateRibbonToolbar("div_editor_toolbar");

    //Create Div InfoWindow
    //CreateInfoWindow("popup_infoWindow");

    //

    // Initialize zoom controls
    $("#sldZoom").slider({
        orientation: "horizontal",
        min: 10,
        max: 300,
        step: 10,
        value: 30,
        slide: function (event, ui) {
            SetZoom(ui.value);
        }
    });

    // Alarm notifications
    // _AlarmList.CreateObject(document.getElementById("popup_alerts"));

    // Parse language
    _Language.CurrentLanguage = 0;
    _Language.parse();

    // Start
    LoadConfig();

    // Initialize anglepicker
    $("#anglepicker").anglepicker({
        value: 0,
        snap: 45,
        shiftSnap: 45,
        clockwise: false,
        change: function (e, ui) {
            Rotating(e, ui);
            SetAnglepickerValue();
        },
        start: function (e, ui) {
            StartRotate(e, ui);
            SetAnglepickerValue();
        },
        stop: function () {
            EndRotate();
            SetAnglepickerValue();
        }
    });

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // TEST - DEBUG - Fill some data to do tests
    $(function () {
        makeDraggableTerminal("ins_term_1");
        makeDraggableTerminal("ins_term_2");
        makeDraggableTerminal("ins_term_3");

        makeDroppableForTerminals("div_background");

        var _back = document.getElementById("div_background");
        if (_back) {
            addEvent(_back, "mouseover", CoordinatesParser);
        }
    });

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}


/* test ///////////////////////////////////////////////////////////////////////*/

//////////////////////////////////////////////




//////////////////////////////////////////////

// ready
$(document).ready(function () {


    browserDetect();

    init();

    //////////////////////////////////////////////////////////////

    SetDivBackgroundToInitialPosition();

    //////////////////////////////////////////////////////////////

    $("#div_background").css("width", CONST_DIV_BACKGROUND_WIDTH);
    $("#div_background").css("height", CONST_DIV_BACKGROUND_HEIGHT);

    CreateGrid(25);

    $("#div_heatmapArea").css("top", -(CONST_DIV_BACKGROUND_HEIGHT / 2));
    $("#div_heatmapArea").css("left", -(CONST_DIV_BACKGROUND_WIDTH / 2));

    $(document).mousedown(function (e) {
        //e.preventDefault();

        m_current_mouse_position_x = e.pageX;
        m_current_mouse_position_y = e.pageY;

        return true;
    });

    $(document).mouseup(function (e) {
        clearInterval(m_movements_timeout);
        m_popup_infoWindow_is_clicked = false;
        m_div_background_is_clicked = false;
        m_movements_timeout = null;

        return true;
    });

    $("#div_background").mousedown(function (e) {
        //e.preventDefault();
        m_div_background_is_clicked = true;
        m_movements_timeout = null;
        m_current_mouse_position_x = e.pageX;
        m_current_mouse_position_y = e.pageY;
        return false;
    });

    $("#div_background").mouseup(function (e) {
        //e.preventDefault();
        clearInterval(m_movements_timeout);
        m_div_background_is_clicked = false;
        m_popup_infoWindow_is_clicked = false;
        return false;
    });

    $("#popup_infoWindow").mousedown(function (e) {
        e.preventDefault();
        m_popup_infoWindow_is_clicked = true;
        m_current_mouse_position_x = e.pageX;
        m_current_mouse_position_y = e.pageY;

        return false;
    });


    $(this).mousemove(function (e) {
        var _temp_left;
        var _temp_top;
        var _css_transform;
        var _css_value;

        if (!m_is_moving) {
            // e.preventDefault();
            if (m_div_background_is_clicked) {
                _temp_left = (e.pageX - m_current_mouse_position_x) / m_current_zoom + m_current_background_position_left;
                _temp_top = (e.pageY - m_current_mouse_position_y) / m_current_zoom + m_current_background_position_top;

                _css_value = "scale(" + m_current_zoom + ")" + "translate(" + _temp_left + "px, " + _temp_top + "px)";

                _css_transform = GetPropertyName_Transform();

                jQuery("#div_background").css(_css_transform, _css_value);

                m_current_background_position_left = _temp_left;
                m_current_background_position_top = _temp_top;
                m_current_mouse_position_x = e.pageX;
                m_current_mouse_position_y = e.pageY;
            }

            if (m_popup_infoWindow_is_clicked) {
                _temp_left = (e.pageX - m_current_mouse_position_x) + m_current_popup_background_position_left;
                _temp_top = (e.pageY - m_current_mouse_position_y) + m_current_popup_background_position_top;

                _css_value = "translate(" + _temp_left + "px, " + _temp_top + "px)";

                _css_transform = GetPropertyName_Transform();

                jQuery("#popup_infoWindow").css(_css_transform, _css_value);

                m_current_popup_background_position_left = _temp_left;
                m_current_popup_background_position_top = _temp_top;
                m_current_mouse_position_x = e.pageX;
                m_current_mouse_position_y = e.pageY;
            }
        }
        return true;
    });



    $(this).on('DOMMouseScroll mousewheel', function (e) {
        var _zoom_movement;

        e.preventDefault();
        switch (m_browser) {

            case "Chrome":
            case "Opera":
            case "Safari":
                if (e.originalEvent.wheelDeltaY < 0) {
                    _zoom_movement = CONST_MOVEMENT_TYPE_ZOOM_MINUS;
                }
                else {
                    _zoom_movement = CONST_MOVEMENT_TYPE_ZOOM_MORE;
                }

                break;


            //IE and Firefox                                                            
            default:

                if (e.originalEvent.wheelDelta < 0) {
                    _zoom_movement = CONST_MOVEMENT_TYPE_ZOOM_MINUS;
                }
                else {
                    _zoom_movement = CONST_MOVEMENT_TYPE_ZOOM_MORE;
                }
        }

        DivPosition_Transform($("#div_background"), _zoom_movement, CONST_MOVEMENT_MOUSE);
        //jQuery("#div_background").css("-webkit-transform", "50% 50%");

        return false;
    });

});                              // end ready


function SetActivityOn() {
    activator();
    m_activity_timeout = setInterval(activator, WSIData.ActivityInterval);

}
function SetActivityOff() {
    clearInterval(m_activity_timeout);
    m_activity_timeout = null;
    RestoreTerminals();
}

function SetActivity(Status) {
    //setInterval(function () { ChangeColor(); }, m_activity_sleep);

    switch (Status) {
        case "on":
            WSIData.Activity = true;
            if (!m_activity_timeout) {
                SetActivityOn();
            }
            break;

        case "off":
            WSIData.Activity = false;
            if (m_activity_timeout) {
                SetActivityOff();
            }
            break;

        default:
            WSIData.Activity = (m_activity_timeout == null);
            if (!m_activity_timeout) {
                SetActivityOn();
            } else {
                SetActivityOff();
            }
            break;
    }

    $("#DivLegend").css('visibility', WSIData.Activity ? 'visible' : 'hidden');
    $("#imgChkActivity")
        .attr("src", WSIData.Activity ? "images/chk_on.png" : "images/chk_off.png")
        .attr("Activity", WSIData.Activity ? "on" : "off");

} // ThreadChangeColorDiv

function RestoreTerminals() {
    $("div[id*=" + CONST_ID_TERMINAL + "]").css('background-color', 'white');
    $("div[id*=" + CONST_ID_TERMINAL + "]").html("");
    //$("div[id*=" + CONST_ID_TERMINAL + "]").removeClass('blink_me');
}

function UpdateTerminals() {
    // TODO
    var _new_color;

    if (_Configuration.ShowHeatMap) {
        _Manager.UpdateHeatMap();
    }

    //if (!m_popup_infoWindow_is_clicked && !m_div_background_is_clicked && !m_movements_timeout) {
    for (_idx in _Manager.Terminals.Items) {

        var _label = new WSILabel();
        _label.Update(_Manager.Terminals.Items[_idx].Properties, 0);

        _new_color = _label.BackColor;

        $("#" + CONST_ID_TERMINAL + _idx).html("");

        if (_new_color !== $("#" + CONST_ID_TERMINAL + _idx).css("background-color")) {
            $("#" + CONST_ID_TERMINAL + _idx).css("background-color", _new_color);
            //_terminal.alt = _Manager.Terminals.Items[_terminal_id].play_session_status;
        }

        if (_AlarmList.enabled) {
            if (_label.hasError) {
                //$("#" + CONST_ID_TERMINAL + _idx).css("background-position", "center");
                //$("#" + CONST_ID_TERMINAL + _idx).html("<img src='images/wrng.png' style='width:100%; height:100%;'></img>");

                $("#" + CONST_ID_TERMINAL + _idx).css("background-position", "center");
                $("#" + CONST_ID_TERMINAL + _idx).html("<img id='terminal_pic_" + _idx + "' src='images/alarm.png' style='width:100%; height:100%;'></img>");

                //$("#" + CONST_ID_TERMINAL + _idx).addClass('blink_me');
                $("#" + CONST_ID_LABEL + _idx).css("z-index", "10");

                var _terminal_rotate_deg = GetDegrees(_Manager.Terminals.Items[_idx].Properties.lop_orientation);
                $('#terminal_pic_' + _idx).css(GetPropertyName_Transform(), "rotate(-" + _terminal_rotate_deg + ")");

            } else {
                // $("#" + CONST_ID_TERMINAL + _idx).removeClass('blink_me');

                if (_Manager.Terminals.Items[_idx].Properties.al_jackpot_flags == "1") {
                    $("#" + CONST_ID_TERMINAL + _idx).css("background-position", "center");
                    $("#" + CONST_ID_TERMINAL + _idx).html("<img id='terminal_pic_" + _idx + "' src='images/jackpot.png' style='width:100%; height:100%;'></img>");
                    $("#" + CONST_ID_LABEL + _idx).css("z-index", "10");

                    var _terminal_rotate_deg = GetDegrees(_Manager.Terminals.Items[_idx].Properties.lop_orientation);
                    $('#terminal_pic_' + _idx).css(GetPropertyName_Transform(), "rotate(-" + _terminal_rotate_deg + ")");
                } else {
                    if (_Manager.Terminals.Items[_idx].Properties.player_is_vip == "1") {
                        $("#" + CONST_ID_TERMINAL + _idx).css("background-position", "center");
                        $("#" + CONST_ID_TERMINAL + _idx).html("<img id='terminal_pic_" + _idx + "' src='images/vip.png' style='width:100%; height:100%;'></img>");
                        $("#" + CONST_ID_LABEL + _idx).css("z-index", "10");

                        var _terminal_rotate_deg = GetDegrees(_Manager.Terminals.Items[_idx].Properties.lop_orientation);
                        $('#terminal_pic_' + _idx).css(GetPropertyName_Transform(), "rotate(-" + _terminal_rotate_deg + ")");
                    }
                }
            }
        } // if (_AlarmList.enabled)

        $("#" + CONST_ID_LABEL + _idx).css("background-color", _new_color);
        //$("#" + CONST_ID_LABEL + _idx).css("border", "1px solid");
        $("#" + CONST_ID_LABEL + _idx).css("border-color", _label.BorderColor);
        $("#" + CONST_ID_LABEL + _idx).css("color", _label.TextColor);
        $("#" + CONST_ID_LABEL + _idx).html(_label.Text);

        // Slot Label
        UpdateSlotLabel(_Manager.Terminals.Items[_idx]);

        //            if (_label.hasError) {
        //                $("#" + CONST_ID_LABEL + _idx).addClass("blink_me");
        //            } else {
        //                $("#" + CONST_ID_LABEL + _idx).removeClass("blink_me");
        //            }
        if (m_terminal_id_selected && _idx == m_terminal_id_selected.replace(CONST_ID_TERMINAL, "")) {
            //only actualize terminal information if div is not hidden
            if (!$("#popup_div_principal").hasClass("div_popup_terminal_hidden")) {
                TerminalClickEvent(null, $("#" + CONST_ID_TERMINAL + _idx), false);
            }
        }
        $("#" + CONST_ID_LABEL + _idx).css("visibility", (_label.hasError || $("#chkShowLabels:checked").length != 0 ? "visible" : "hidden"));


        //        //click en etiqueta --> abrir popup
        //        $("#" + CONST_ID_LABEL + _idx).mousedown(function (e) {
        //            TerminalClickEvent(e, $("#" + CONST_ID_TERMINAL + _idx), false);
        //        });

        if (_Manager.Terminals.Items[_idx].Properties.player_is_vip == "1") {
            _Alarm = new WSIAlarmListItem(_Alarm_Types["VIP"]);
            _Alarm.sourceid = _Manager.Terminals.Items[_idx].Properties.lo_external_id;
            _Alarm.link = GotoTerminal;
            _AlarmList.AddAlarm(_Alarm);
        }

    } // for

    //UpdateAllClientSnapshots();

    //} //if

    UpdateLastTimeUpdated();
} // UpdateTerminals

//
function UpdateSlotLabel(Terminal) {
    function Round(Value) {
        return Math.round(Value * 100) / 100;
    }

    $("#" + CONST_ID_LABEL + _idx).css('text-align', 'center');
    var _left = (Terminal.Properties.lop_x * 5) - 5;
    var _top = (Terminal.Properties.lop_z * 5) - 5;

    var _btop = "none";
    var _bbott = "none";
    var _bleft = "none";
    var _bright = "none";

    var _align = "right";

    switch (Terminal.Properties.lop_orientation) {
        case 0:
            _top += 23;
            _left -= 5;
            //_bleft = "1px solid black";
            break;
        case 1:
            break;
        case 2:
            _left -= 27;
            //_btop = "1px solid black";
            break;
        case 3:
            break;
        case 4:
            _top -= 20;
            //_bleft = "1px solid black";
            _left -= 5;
            break;
        case 5:
            break;
        case 6:
            _left += 20;
            //_btop = "1px solid black";
            break;
        case 7:
            break;
    }

    var _new_div;

    _new_div = document.getElementById(CONST_ID_SLOT_LABEL + Terminal.Properties.lo_id);

    if (!_new_div) {
        _new_div = document.createElement("div");

        $(_new_div).attr("id", CONST_ID_SLOT_LABEL + Terminal.Properties.lo_id)
            .addClass("div_terminal")
            .css("top", _top + "px")
            .css("left", _left + "px")
            .css("height", "25px")
            .css("width", "25px")
            .css("background-color", "transparent")
            .css("border-top", _btop)
            .css("border-bottom", _bbott)
            .css("border-left", _bleft)
            .css("border-right", _bright)
            .css("text-align", _align)
            .css("z-index", 2)
            .css("font", "3.2pt Arial");

        $("#div_center").append($(_new_div));
    }

    var _net_win = Round(Terminal.Properties.net_win);
    var _img_background_visibility = $("#img_background").css("visibility");

    var _html = '';
    _html += '<span style="color: ' + (_img_background_visibility == "visible" ? 'black' : 'white') + ';">' + Round(Terminal.Properties.played_amount) + "$" + '</span><br>';
    _html += '<span style="color: ' + (_img_background_visibility == "visible" ? 'black' : 'white') + ';">' + Round(Terminal.Properties.won_amount) + "$" + '</span><br>';
    _html += '<span style="color: ' + (_net_win < 0 ? 'red' : 'green') + ';">' + _net_win + "$" + '</span>';

    $(_new_div).html(_html);

}

function CreateDivAreas() {
    for (var _i = 0; _i < m_areas_positions.length; _i++) {
        var _new_div;

        _new_div = document.createElement("div");
        $(_new_div).addClass("div_area")
            .attr("id", CONST_ID_AREA + _i)
            .css("left", (m_areas_positions[_i][0] * 5) - 5 + "px")
            .css("top", (m_areas_positions[_i][1] * 5) - 5 + "px")
            .css("width", (m_areas_positions[_i][2] * 20) + "px")
            .css("height", (m_areas_positions[_i][3] * 20) + "px");

        $(_new_div).click(function (e) {
            AreaClickEvent(e, this);
        });

        $("#div_center").append($(_new_div));

    }
}  // CreateDivAreas

function CreateDivIslands() {
    for (var _i = 0; _i < m_islands_positions.length; _i++) {
        var _new_div;

        _new_div = document.createElement("div");
        $(_new_div).addClass("div_island")
            .attr("id", CONST_ID_ISLAND + _i)
            .css("left", (m_islands_positions[_i][0] * 5) - 10 + "px")
            .css("top", (m_islands_positions[_i][1] * 5) - 10 + "px")
            .css("width", (m_islands_positions[_i][2] * 20) + "px")
            .css("height", (m_islands_positions[_i][3] * 20) + "px");

        $(_new_div).click(function (e) {
            IslandClickEvent(e, this);
        });

        $("#div_center").append($(_new_div));

    }
} // CreateDivIslands


function CreateTerminal(Terminal, callbackLabelBuilder) {
    var _css_transform;
    var _terminal_rotate_deg;

    _css_transform = GetPropertyName_Transform();

    var _new_div;

    _new_div = document.createElement("div");
    $(_new_div).addClass("div_terminal")
            .attr("id", CONST_ID_TERMINAL + Terminal.Properties.lo_id)
            .css("top", (Terminal.Properties.lop_z * 5) - 5 + "px")
            .css("left", (Terminal.Properties.lop_x * 5) - 5 + "px")
            .css("z-index", 2);

    $(_new_div).mousedown(function (e) {
        m_left_button_down = (e.which == 1);
        e.preventDefault();
        TerminalClickEvent(e, this, false);
        m_operation_target = this.id;
        StartMove(e, this);
    });

    $(_new_div).mouseup(function (e) {
        m_left_button_down = false;
        e.preventDefault();
        EndMove(e, this);
    });

    $(_new_div).mousemove(function (e) {
        e.preventDefault();
        Move(e, this);
    });

    $(_new_div).mouseleave(function (e) {
        e.preventDefault();
        EndMove(e, this);
    });

    _terminal_rotate_deg = GetDegrees(Terminal.Properties.lop_orientation);
    $(_new_div).css(_css_transform, "rotate(" + _terminal_rotate_deg + ")");

    $("#div_center").append($(_new_div));

    callbackLabelBuilder(Terminal);
}

function CreateTerminalLabel(Terminal) {
    //    var _css_transform;
    //    var _terminal_rotate_deg;

    //    _css_transform = GetPropertyName_Transform();

    //    var _new_label;

    //    _new_label = document.createElement("p");

    //    $(_new_label).addClass("labels")
    //                .attr("id", CONST_ID_LABEL + Terminal.Properties.lo_id)
    //                .css("top", (Terminal.Properties.lop_z * 10) + "px")
    //                .css("left", (Terminal.Properties.lop_x * 10) - 8 + "px")
    //                .css("border", "none")
    //                .text(Math.round(Math.random() * 100) + "$")
    //                .css("z-index", 3);

    //    $("#div_center").append($(_new_label));
}

function CreateDivTerminales() {

    _Manager.Terminals.Build(CreateTerminal, CreateTerminalLabel);

} // CreateDivTerminales


//Position: 0, 1, 2, 3, 4, 5, 6, 7
function GetDegrees(Position) {
    var _degrees;

    switch (Position) {
        case 0:
            _degrees = 0; break;
        case 1:
            _degrees = 45; break;
        case 2:
            _degrees = 90; break;
        case 3:
            _degrees = 135; break;
        case 4:
            _degrees = 180; break;
        case 5:
            _degrees = 225; break;
        case 6:
            _degrees = 270; break;
        case 7:
            _degrees = 315; break;
        default:
            _degrees = 0;
    }
    return _degrees + "deg";

} // GetDegrees

function CreateDivRoulettes() {
    /*
    var _css_transform;
    var _roulette_rotate_deg;

    var m_roulettes_positions;
    m_roulettes_positions = [ //posX, posY, width, height, Orientation
    [780, 0, 120, 80, 0],
    [980, -0, 120, 80, 4]
    ];

    _css_transform = GetPropertyName_Transform();

    for (var _i = 0; _i < m_roulettes_positions.length; _i++) {
    var _new_div;

    _new_div = document.createElement("div");
    $(_new_div).addClass("div_roulette")
    .attr("id", "roulette_" + _i)
    .css("left", (m_roulettes_positions[_i][0]) + "px")
    .css("top", (m_roulettes_positions[_i][1]) + "px")
    .css("width", (m_roulettes_positions[_i][2]) + "px")
    .css("height", (m_roulettes_positions[_i][3]) + "px")
    .css("background", "url(images/m_roulette.png) no-repeat");

    $(_new_div).click(function (e) {
    RouletteClick(e, this);
    });

    _roulette_rotate_deg = GetDegrees(m_roulettes_positions[_i][4]);
    $(_new_div).css(_css_transform, "rotate(" + _roulette_rotate_deg + ")");

    $("#div_center").append($(_new_div));
    }
    */
} //CreateDivRoulettes

function RouletteClick(e, Roulette) {

    alert($(Roulette).attr("id"));
} // RouletteClick


function CreateDivGamingtables() {
    /*
    var _css_transform;
    var m_gamingtables_positions;
    var _gamingtables_rotate_deg;

    m_gamingtables_positions = [ //posX, posY, width, height, size, , Orientation
    [680, -200, 80, 80, "big", 0],
    [680, -300, 80, 80, "big", 0],
    [780, -100, 60, 40, "small", 7],
    [980, -100, 60, 40, "small", 1]
    ];

    _css_transform = GetPropertyName_Transform();

    for (var _i = 0; _i < m_gamingtables_positions.length; _i++) {
    var _new_div;

    _new_div = document.createElement("div");
    $(_new_div).addClass("div_gamingtable")
    .attr("id", "gamingtable_" + _i)
    .css("left", (m_gamingtables_positions[_i][0]) + "px")
    .css("top", (m_gamingtables_positions[_i][1]) + "px")
    .css("width", (m_gamingtables_positions[_i][2]) + "px")
    .css("height", (m_gamingtables_positions[_i][3]) + "px");
    if (m_gamingtables_positions[_i][4] == "small") {
    $(_new_div).css("background", "url(images/m_BJ.png) no-repeat");
    }
    else {
    $(_new_div).css("background", "url(images/m_PK.png) no-repeat");
    }

    $(_new_div).click(function (e) {
    GamingtableClick(e, this);
    });

    _gamingtables_rotate_deg = GetDegrees(m_gamingtables_positions[_i][5]);
    $(_new_div).css(_css_transform, "rotate(" + _gamingtables_rotate_deg + ")");

    $("#div_center").append($(_new_div));
    }
    */
} //CreateDivGamingtables

function GamingtableClick(e, Gamingtable) {

    alert($(Gamingtable).attr("id"));
} // GamingtableClick


function browserDetect() {

    var nAgt = navigator.userAgent;
    var verOffset, ix;
    // IE is default
    m_browser = navigator.appName;

    // Opera
    if ((verOffset = nAgt.indexOf("Opera")) != -1) {
        m_browser = "Opera";
    }
    //  Chrome
    else if ((verOffset = nAgt.indexOf("Chrome")) != -1) {
        m_browser = "Chrome";
    }
    //  Safari
    else if ((verOffset = nAgt.indexOf("Safari")) != -1) {
        m_browser = "Safari";
    }
    // Firefox
    else if ((verOffset = nAgt.indexOf("Firefox")) != -1) {
        m_browser = "Firefox";
    }

}

function GetPropertyName_Transform() {
    var _transform;

    switch (m_browser) {
        case "Chrome":
        case "Opera":
        case "Safari":
            _transform = "-webkit-transform"
            break;


        //IE and Firefox                
        default:
            _transform = "transform";
    }

    return _transform;
}

function GetPropertyName_TransformOrigin() {
    var _transform_origin;

    switch (m_browser) {
        case "Chrome":
        case "Opera":
        case "Safari":
            _transform_origin = "-webkit-transform-origin"
            break;


        //IE and Firefox                 
        default:
            _transform_origin = "transform-origin";
    }

    return _transform_origin;
}

function DivPosition_Move(Div, Left, Top, Zoom, TransformOrigin) {
    var _css_value;


    _css_value = "scale(" + Zoom + ")" + "translate(" + Left + "px, " + Top + "px)";

    if (TransformOrigin != null) {
        Div.css(GetPropertyName_TransformOrigin(), TransformOrigin);
    }
    Div.css(GetPropertyName_Transform(), _css_value);
} // DivPosition_Move

function DivPosition_Transform(Div, Action, MovementOrigin) {
    var _temp;
    switch (Action) {
        case CONST_MOVEMENT_TYPE_RIGTH: m_current_background_position_left -= CONST_MOVEMENT_PX; break;
        case CONST_MOVEMENT_TYPE_LEFT: m_current_background_position_left += CONST_MOVEMENT_PX; break;
        case CONST_MOVEMENT_TYPE_UP: m_current_background_position_top -= CONST_MOVEMENT_PX; break;
        case CONST_MOVEMENT_TYPE_DOWN: m_current_background_position_top += CONST_MOVEMENT_PX; break;
        case CONST_MOVEMENT_TYPE_ZOOM_MORE:
            switch (true) {
                case MovementOrigin === CONST_MOVEMENT_BUTTON:
                    _temp = CONST_MOVEMENT_ZOOM_PX;
                    break;
                case MovementOrigin === CONST_MOVEMENT_MOUSE:
                    _temp = CONST_MOVEMENT_ZOOM_WHEEL_PX;
                    break;
                default: break;
            }
            if (+m_current_zoom + _temp <= CONST_MAX_ZOOM) {
                m_current_zoom = +m_current_zoom + _temp;
            }
            UpdateSlider();
            break;

        case CONST_MOVEMENT_TYPE_ZOOM_MINUS:
            switch (true) {
                case MovementOrigin === CONST_MOVEMENT_BUTTON:
                    _temp = CONST_MOVEMENT_ZOOM_PX;
                    break;
                case MovementOrigin === CONST_MOVEMENT_MOUSE:
                    _temp = CONST_MOVEMENT_ZOOM_WHEEL_PX;
                    break;
                default: break;
            }
            if (+m_current_zoom - _temp >= CONST_MIN_ZOOM) {
                m_current_zoom = +m_current_zoom - _temp;
            }
            UpdateSlider();
            break;

        default: break;
    }

    DivPosition_Move(Div, m_current_background_position_left, m_current_background_position_top, m_current_zoom, CONST_TRANSFORM_ORIGIN);

} // DivPosition_Transform

function IslandClickEvent(e, IslandDiv) {

    alert($(IslandDiv).attr("id"));
}


function AreaClickEvent(e, AreaDiv) {

    alert($(AreaDiv).attr("id"));
}


(function ($) {
    // Menu Functions
    $(document).ready(function () {

        // previously click
        $('#openner').mousedown(function (e) {

            var _r = $('#menu_rigth').css("right");

            if (_r == "0px") {
                $('#menu_rigth').css({
                    'right': '-400px',
                    'top': '0px'
                });
                $('#openner').css({
                    'right': '0px',
                    'top': '100px'

                });

                $('#openner').css('background', 'url(images/m_more.png) no-repeat');

            }
            else {
                $('#menu_rigth').css({
                    'right': '0px',
                    'top': '0px'
                });
                $('#openner').css({
                    'right': '400px',
                    'top': '100px'
                });

                $('#openner').css('background', 'url(images/m_less.png) no-repeat');
            }

            e.preventDefault();
        });

    });
})(jQuery);


////////////////////


function SetBackground(checkbox) {
    var _visibility;

    if (checkbox.checked) {
        _visibility = "visible";
    } else {
        _visibility = "hidden";
    }

    $("#img_background").css("visibility", _visibility);
} // SetBackground


function SetIslands(checkbox) {
    var _visibility;

    if (checkbox.checked) {
        _visibility = "visible";
    } else {
        _visibility = "hidden";
    }

    for (var _i = 0; _i < m_islands_positions.length; _i++) {
        $("#" + CONST_ID_ISLAND + _i).css("visibility", _visibility);
    }
} // SetIslands

function SetAreas(checkbox) {
    var _visibility;

    if (checkbox.checked) {
        _visibility = "visible";
    } else {
        _visibility = "hidden";
    }

    for (var _i = 0; _i < m_areas_positions.length; _i++) {
        $("#" + CONST_ID_AREA + _i).css("visibility", _visibility);
    }
} // SetAreas

function SetLabels(checkbox) {
    var _visibility;

    if (checkbox.checked) {
        _visibility = "visible";
    } else {
        _visibility = "hidden";
    }

    for (_idx in _Manager.Terminals.Items) {
        $("#" + CONST_ID_LABEL + _idx).css("visibility", _visibility);
    }

    //    for (var _i = 0; _i < _Manager.Terminals.Items.length; _i++) {
    //        $("#" + CONST_ID_LABEL + _i).css("visibility", _visibility);
    //    }
} // SetLabels


function SetControls(checkbox) {
    var _visibility;

    if (checkbox.checked) {
        _visibility = "visible";
    } else {
        _visibility = "hidden";
    }

    $("#pnlNavigation").css("visibility", _visibility);
}

//
//
//
function SetMachineSize(control) {
    $(".div_terminal").css("height", control.value);
    $(".div_terminal").css("width", control.value);
}

function InitializeScene() {

    //
    CreateDivTerminales();
    CreateDivIslands();
    CreateDivAreas();
    CreateDivGamingtables();
    CreateDivRoulettes();

    //ThreadChangeColorDiv();
    ApplyDefaultValues();

    if (_Configuration.ShowHeatMap) {
        _Manager.CreateHeatMap();
    }
}

function HiddenDiv(Div) {
    //$(Div).css("visibility","hidden");
    $(Div).removeClass("div_popup_terminal");
    $(Div).addClass("div_popup_terminal_hidden");

    if (m_terminal_id_selected) {
        UnselectTerminalDiv();
    }
}

function TerminalClickEvent(e, TerminalDiv, UpdateHistorical) {
    var _info;
    var _popup_div_principal;
    var _terminal;
    var _label;

    _terminal = _Manager.Terminals.GetTerminal($(TerminalDiv).attr("id").replace(CONST_ID_TERMINAL, ""));

    TerminalDivSelected($(TerminalDiv).attr("id"));
    m_terminal_id_selected = $(TerminalDiv).attr("id");


} //TerminalClickEvent

function CreateGrid(Size) {
    var _i;
    var _height = $("#div_background").height();
    var _width = $("#div_background").width();
    var _ratioW = Math.floor(_width / Size);
    var _ratioH = Math.floor(_height / Size);

    for (_i = 0; _i <= _ratioW; _i++) {  // vertical grid lines
        $('<div />').css({
            'top': 1,
            'left': _i * Size,
            'width': 1,
            'height': _height
        })
        .addClass('gridlines')
        .appendTo($("#div_background"));
    }
    for (_i = 0; _i <= _ratioH; _i++) { // horizontal grid lines
        $('<div />').css({
            'top': 1 + _i * Size,
            'left': 0,
            'width': _width,
            'height': 1
        })
        .addClass('gridlines')
        .appendTo($("#div_background"));
    }

    $('.gridlines').show();

    if (_Configuration.ShowGrid) {
        $('.gridlines').css("visibility", "visible");
    }
    else {
        $('.gridlines').css("visibility", "hidden");
    }
}  // CreateGrid

function UnselectTerminalDiv(Selected) {
    TerminalDivSelected(m_terminal_id_selected, false);
    m_terminal_id_selected = null;
}

function SelectTerminal(Terminal, Selected) {

    if (Selected) {
        // Only mark as select if it is unselected
        if (!$(Terminal).attr("selected")) {
            var _final_left;
            var _final_top;

            _final_left = $(Terminal).css("left").replace("px", "") + "px";
            _final_top = $(Terminal).css("top").replace("px", "") + "px";

            // Mark terminal as select
            $(Terminal)
            .css('border', '1px solid yellow')
            .css('z-index', '5')
            .attr('initial_left', $(Terminal).css("left"))
            .attr('initial_top', $(Terminal).css("top"))
            .attr('selected', '1')
            .css('left', (_final_left))
            .css('top', (_final_top));
        }
    }
    else {
        // Only mark as unselect if it is select
        if ($(Terminal).attr("selected")) {
            $(Terminal)
            .css('border', '1px solid black')
            .css('z-index', '2')
            .css('left', ($(Terminal).attr("initial_left")))
            .css('top', ($(Terminal).attr("initial_top")))
            .removeAttr("selected");
        }
    }
} //SelectTerminal


function TerminalDivSelected(TerminalId, Selected) {

    if (Selected == undefined) { Selected = true; }

    var _term_div = TerminalId;

    // Unselect actual terminal
    if (m_terminal_id_selected != null && _term_div != m_terminal_id_selected) {

        $($("#" + m_terminal_id_selected))
        .css('border', '1px solid black')
        .css('z-index', '2');

        if ($($("#" + m_terminal_id_selected)).attr("selected")) {
            $($("#" + m_terminal_id_selected)).removeAttr("selected");
        }
    }

    // Select/Unselect terminal
    if ($("#" + _term_div).length > 0) {
        SelectTerminal($("#" + _term_div), Selected);
    } else {
        // Does not exist the Terminal?
    }

}

function SetZoom(Value) {
    //m_current_zoom = 1 - Value;
    m_current_zoom = Value / 100;
    DivPosition_Move($("#div_background"), m_current_background_position_left, m_current_background_position_top, m_current_zoom, CONST_TRANSFORM_ORIGIN);
}

function UpdateSlider() {
    try {
        $("#sldZoom").slider("value", m_current_zoom * 100);

    } catch (err) { }

}

//////////////////////////////////////////////////////////////////////////////////////

function GotoTerminal(Sender) {
}


//////////////////////////////////////////////////////////////////////////////////////

function ShowTerminalFromCustomer(AccountId) {
    var _terminal_div;
    var _object_id;

    try {
        _object_id = _Manager.Terminals.ByAccount[AccountId];

        if (_object_id) {
            _terminal_div = $("#" + CONST_ID_TERMINAL + _object_id);
            if ($(_terminal_div).length > 0) {
                // open div "Activity"
                $("#ribbon-tab-header-0").click();

                SetDivBackgroundToInitialPosition();

                // move popup
                m_current_popup_background_position_left = 0;
                m_current_popup_background_position_top = 0;
                var _css_value = "translate(" + m_current_popup_background_position_left + "px, " + m_current_popup_background_position_top + "px)";
                var _css_transform = GetPropertyName_Transform();
                jQuery("#popup_infoWindow").css(_css_transform, _css_value);

                //ClickEvent
                //TerminalClickEvent(null, _terminal_div, false);
                TerminalDivSelected($(_terminal_div).attr("id"), true);
                m_terminal_id_selected = $(_terminal_div).attr("id");

            }
        }
    }
    catch (err) { }

    return;
} // ShowTerminalFromCustomer

function SetGrid(checkbox) {
    _Configuration.ShowGrid = checkbox.checked;

    if (_Configuration.ShowGrid) {
        $('.gridlines').css("visibility", "visible");
    }
    else {
        $('.gridlines').css("visibility", "hidden");
    }

}


function ApplyDefaultValues() {
    // chkShowAlarms
    if ($("#chkShowAlarms").prop("checked")) {
        _AlarmList.enabled = true;
    }
    else {
        _AlarmList.enabled = false;
    }

    //chkSetBackground
    if ($("#chkSetBackground").prop("checked")) {
        $("#img_background").css("visibility", "visible");
    }
    else {
        $("#img_background").css("visibility", "hidden");
    }

    //chkShowGrid
    if ($("#chkShowGrid").prop("checked")) {
        $('.gridlines').css("visibility", "visible");
    }
    else {
        $('.gridlines').css("visibility", "hidden");
    }

    //chkShowAreas
    if ($("#chkShowAreas").prop("checked")) {
        $("div[id*=" + CONST_ID_AREA + "]").css("visibility", "visible");
    }
    else {
        $("div[id*=" + CONST_ID_AREA + "]").css("visibility", "hidden");
    }

    //chkShowIslands
    if ($("#chkShowIslands").prop("checked")) {
        $("div[id*=" + CONST_ID_ISLAND + "]").css("visibility", "visible");
    }
    else {
        $("div[id*=" + CONST_ID_ISLAND + "]").css("visibility", "hidden");
    }

    //LC_LANGUAGE AS LANG
    if ($("#radEnglish").prop("checked")) {
        _Language.CurrentLanguage = 1;
    }
    else if ($("#radSpanish").prop("checked")) {
        _Language.CurrentLanguage = 0;
    }
    else {
        // default SPANISH
        _Language.CurrentLanguage = 0;
    }
    _Language.parse();

    //chkRealTimeSnapshots
    if ($("#chkRealTimeSnapshots").prop("checked")) {
        _Configuration.ShowRealTimeRefresh = true;
    }
    else {
        _Configuration.ShowRealTimeRefresh = false;
    }

    //chkTestMode
    if ($("#chkTestMode").prop("checked")) {
        _Configuration.TestEnabled = true;
    }
    else {
        _Configuration.TestEnabled = false;
    }

    //SetActivity
    if ($("#imgChkActivity").attr("Activity") == "on") {
        SetActivity("on")
    }
    else {
        SetActivity("off")
    }


    if ($("#chkHeatmap").prop("checked")) {
        _Configuration.ShowHeatMap = true;
        //_Manager.CreateHeatMap();
        //_Manager.UpdateHeatMap();
    }
    else {
        _Configuration.ShowHeatMap = false;
        //$("#div_heatmapArea").empty();
    }

} // ApplyDefaultValues

function SetDivBackgroundToInitialPosition() {
    //m_current_background_position_left = -1 * (CONST_DIV_BACKGROUND_WIDTH / 2);
    //m_current_background_position_top = -1 * (CONST_DIV_BACKGROUND_HEIGHT / 2);

    // Adjust location
    //m_current_background_position_left += (50 / +CONST_INITIAL_ZOOM);
    m_current_background_position_top += (70 / +CONST_INITIAL_ZOOM);

    // Initial position
    DivPosition_Move($("#div_background"), m_current_background_position_left, m_current_background_position_top, m_current_zoom, CONST_TRANSFORM_ORIGIN);

    // Zoom Out
    m_current_zoom = CONST_INITIAL_ZOOM;
    DivPosition_Move($("#div_background"), m_current_background_position_left, m_current_background_position_top, m_current_zoom, CONST_TRANSFORM_ORIGIN);

} // SetDivBackgroundToInitialPosition

//////////////////////////////////////////////////////////////////////////////

function SetHeatmap(Checkbox) {
    _Configuration.ShowHeatMap = $(Checkbox).is(':checked');

    if (_Configuration.ShowHeatMap) {
        _Manager.CreateHeatMap();
        _Manager.UpdateHeatMap();
    }
    else {
        $("#div_heatmapArea").empty();
        _Manager.HeatMapControl = null;
    }
} // SetHeatmap

//////////////////////////////////////////////////////////////////////////////

var m_current_mode = 1;

function setMode(Mode) {
    m_current_mode = Mode;
}

var m_operation_target = null;

var m_is_moving = false;
var m_start_left = null;
var m_start_top = null;
var m_old_left = null;
var m_old_top = null;
var m_old_angle = null;

function StartMove(sender, object) {
    if (object.id == m_operation_target) {
        m_start_left = +$(object).css("left").replace("px", "");
        m_start_top = +$(object).css("top").replace("px", "");
        m_old_left = sender.clientX;
        m_old_top = sender.clientY;
        $(object).css("background-color", "green");
    }
}

function EndMove(sender, object) {
    m_is_moving = false;
    $(object).css("background-color", "white");
}

function Move(sender, object) {
    if (m_left_button_down && m_operation_target == object.id) {
        m_is_moving = true;
        $(object).css("left", m_start_left + ((sender.clientX - m_old_left) / m_current_zoom));
        $(object).css("top", m_start_top + ((sender.clientY - m_old_top) / m_current_zoom));
    }
}

