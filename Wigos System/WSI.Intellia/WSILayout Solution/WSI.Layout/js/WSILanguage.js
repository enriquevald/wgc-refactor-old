﻿var m_language_binds = {
  "ES": 0,
  "EN": 1
};

function WSILanguage() {

  this.CurrentLanguage = 0;

  this.DefaultLanguage = 1;

  this.Languages = [{
    id: "ES",
    flag: "images/flag_spain.png",
    nls: "NLS_SPANISH"
  },
  {
    id: "EN",
    flag: "images/flag_united_kingdom.png",
    nls: "NLS_ENGLISH"
  }];

  this.Values = [];

  // Languages
  this.Values["ES"] = [];
  this.Values["EN"] = [];

  /***************************************** NLS DEFINITION *****************************************/

  // Login Screen
  // Spanish
  this.Values["ES"]["NLS_LOGIN_USER"] = "Usuario";
  this.Values["ES"]["NLS_LOGIN_PASSWORD"] = "Password";
  this.Values["ES"]["NLS_LOGIN_ACCESS"] = "ENTRAR";
  this.Values["ES"]["NLS_LOGIN_LOGIN"] = "LOGIN";
  // English
  this.Values["EN"]["NLS_LOGIN_USER"] = "Username";
  this.Values["EN"]["NLS_LOGIN_PASSWORD"] = "Password";
  this.Values["EN"]["NLS_LOGIN_ACCESS"] = "ENTER";
  this.Values["EN"]["NLS_LOGIN_LOGIN"] = "LOGIN";


  // Spanish
  this.Values["ES"]["NLS_ACTIVITY"] = "Actividad";
  this.Values["ES"]["NLS_LAYOUT"] = "Plano";
  this.Values["ES"]["NLS_CONFIGURATION"] = "Configuración";
  this.Values["ES"]["NLS_BACKGROUND"] = "Fondo";
  this.Values["ES"]["NLS_LANGUAGE"] = "Idioma";
  this.Values["ES"]["NLS_NAVIGATION"] = "Controles navegación";
  this.Values["ES"]["NLS_REFRESH"] = "Refresco";
  this.Values["ES"]["NLS_SLOTSSIZE"] = "Tamaño máquinas";
  this.Values["ES"]["NLS_ONOFF"] = "OFF/ON";
  this.Values["ES"]["NLS_RANGES"] = "Rangos";
  this.Values["ES"]["NLS_OCUPATION"] = "Ocupación";
  this.Values["ES"]["NLS_BYUSE"] = "Uso";
  this.Values["ES"]["NLS_BYLEVEL"] = "Nivel";
  this.Values["ES"]["NLS_BYAGE"] = "Edad";
  this.Values["ES"]["NLS_BETAVERAGE"] = "Apuesta media";
  this.Values["ES"]["NLS_LEGEND"] = "Leyenda";
  this.Values["ES"]["NLS_ALARMS"] = "Alarmas";
  this.Values["ES"]["NLS_ALARM"] = "Alarma";
  this.Values["ES"]["NLS_ALARMS_ONLY_PLAYERS"] = "Filtros";
  this.Values["ES"]["NLS_ALARMS_ONLY_MACHINES"] = "Filtros";
  this.Values["ES"]["NLS_TERMINALSNAPSHOTS"] = "Estado de terminales";
  this.Values["ES"]["NLS_CLIENTSNAPSHOTS"] = "Estado de jugadores";
  this.Values["ES"]["NLS_CUSTOMERSATROOM"] = "Jugadores actuales";
  this.Values["ES"]["NLS_SHOWNAVIGATIONCONTROLS"] = "Mostrar controles navegación";
  this.Values["ES"]["NLS_SHOWLABELS"] = "Mostrar etiquetas";
  this.Values["ES"]["NLS_SHOWSLOTLABELS"] = "Mostrar etiquetas de máquina";
  this.Values["ES"]["NLS_SHOWAREAS"] = "Mostrar áreas";
  this.Values["ES"]["NLS_SHOWBANKS"] = "Mostrar islas";
  this.Values["ES"]["NLS_SNAPSHOTINREALTIME"] = "Estado en tiempo real";
  this.Values["ES"]["NLS_SMALL"] = "Pequeño";
  this.Values["ES"]["NLS_BIG"] = "Grande";
  this.Values["ES"]["NLS_SHOWALARMS"] = "Mostrar alarmas";
  this.Values["ES"]["NLS_SHOWIMAGE"] = "Mostrar imagen";
  this.Values["ES"]["NLS_SHOWGRID"] = "Mostrar cuadrícula";
  this.Values["ES"]["NLS_ACCOUNTID"] = "Cuenta";
  this.Values["ES"]["NLS_TRACKDATA"] = "Tarjeta";
  this.Values["ES"]["NLS_NAME"] = "Nombre";
  this.Values["ES"]["NLS_LEVEL"] = "Nivel";
  this.Values["ES"]["NLS_AGE"] = "Edad";
  this.Values["ES"]["NLS_TERMINAL"] = "Terminal";
  this.Values["ES"]["NLS_PROVIDER"] = "Proveedor";
  this.Values["ES"]["NLS_BANK"] = "Isla";
  this.Values["ES"]["NLS_AREA"] = "Área";
  this.Values["ES"]["NLS_SNAPSHOT"] = "Estado";
  this.Values["ES"]["NLS_KEEPER"] = "Localizar";
  this.Values["ES"]["NLS_CURRENTCLIENT"] = "Jugador actual";
  this.Values["ES"]["NLS_GENDER"] = "Sexo";
  this.Values["ES"]["NLS_UNKNOWN"] = "Desconocido";
  this.Values["ES"]["NLS_MALE"] = "Hombre";
  this.Values["ES"]["NLS_FEMALE"] = "Mujer";
  this.Values["ES"]["NLS_TERMINALTODAY"] = "Jornada actual";
  this.Values["ES"]["NLS_CURRENTPLAYSESSION"] = "Sesión de juego actual";
  this.Values["ES"]["NLS_BETAV"] = "Apuesta media";
  this.Values["ES"]["NLS_PLAYED"] = "Jugado";
  this.Values["ES"]["NLS_COIN_IN"] = "Jugado";
  this.Values["ES"]["NLS_PLAYEDCOUNT"] = "Nº jugadas";
  this.Values["ES"]["NLS_WON"] = "Ganado";
  this.Values["ES"]["NLS_COIN_OUT"] = "Ganado";
  this.Values["ES"]["NLS_NETWIN"] = "Netwin";
  this.Values["ES"]["NLS_DURATION"] = "Duración";
  this.Values["ES"]["NLS_INACTIVE"] = "Inactivo";
  this.Values["ES"]["NLS_ACTIVE"] = "Activo";
  this.Values["EN"]["NLS_MACHINE_ACTIVE"] = "Activo";
  this.Values["EN"]["NLS_MACHINE_OUTOFSERVICE"] = "Fuera de servicio";
  this.Values["EN"]["NLS_MACHINE_RETIRED"] = "Retirado";
  this.Values["ES"]["NLS_ANONYMOUS"] = "Anónimo";
  this.Values["ES"]["NLS_LESSTHAN"] = "Menor";
  this.Values["ES"]["NLS_ORMORE"] = "o más";
  this.Values["ES"]["NLS_PV"] = "Pv";
  this.Values["ES"]["NLS_T"] = "T";
  this.Values["ES"]["NLS_P"] = "J";
  this.Values["ES"]["NLS_W"] = "G";
  this.Values["ES"]["NLS_NP"] = "NJ";
  this.Values["ES"]["NLS_%P"] = "AM";
  this.Values["ES"]["NLS_BILLIN"] = "Billetes";
  this.Values["ES"]["NLS_SPANISH"] = "Español";
  this.Values["ES"]["NLS_ENGLISH"] = "Inglés";
  this.Values["ES"]["NLS_DATE"] = "Fecha";
  this.Values["ES"]["NLS_AMOUNT"] = "Valor";
  this.Values["ES"]["NLS_STACKER"] = "Aceptador";
  this.Values["ES"]["NLS_AL_SASHOST"] = "SAS-HOST";
  this.Values["ES"]["NLS_AL_DOOR"] = "PUERTA";
  this.Values["ES"]["NLS_AL_BILL"] = "ACEPTADOR";
  this.Values["ES"]["NLS_AL_PRINTER"] = "IMPRESORA";
  this.Values["ES"]["NLS_AL_EGM"] = "EGM";
  this.Values["ES"]["NLS_AL_ATTENDANT"] = "ASISTENTE";
  this.Values["ES"]["NLS_AL_BLOQUED"] = "BLOQUEO";
  this.Values["ES"]["NLS_AL_PLAYER_WON"] = "al_played_won_flags";
  this.Values["ES"]["NLS_AL_JACKPOT"] = "JACKPOT";
  this.Values["ES"]["NLS_AL_STACKER_STATUS"] = "al_stacker_status";
  this.Values["ES"]["NLS_AL_MACHINE"] = "al_machine_flags";
  this.Values["ES"]["NLS_AL_BIG_WON"] = "al_big_won";

  this.Values["ES"]["NLS_CHARTS"] = "Gráficos";

  this.Values["ES"]["NLS_ALARM_VIP"] = "<img src='img/alarm.png' style='width:12px;heigth:12px'></img>&nbsp;&nbsp; Cliente VIP en sala <br/> %0";
  this.Values["ES"]["NLS_ALARM_ERROR"] = "<img src='img/alarm.png' style='width:12px;heigth:12px'></img>&nbsp;&nbsp; ERROR en terminal %0";

  this.Values["ES"]["NLS_DETAIL"] = "Detalle";
  this.Values["ES"]["NLS_REALTIME"] = "Tiempo real";
  this.Values["ES"]["NLS_HISTORIC"] = "Histórico";
  this.Values["ES"]["NLS_MODE"] = "Modo";
  this.Values["ES"]["NLS_MONITOR"] = "Mis Filtros";
  this.Values["ES"]["NLS_ENABLED"] = "Habilitado";
  this.Values["ES"]["NLS_CLOSE"] = "Cerrar";
  this.Values["ES"]["NLS_EXIT"] = "Salir";
  this.Values["ES"]["NLS_HEATMAP"] = "Mapa de temperatura";
  this.Values["ES"]["NLS_HEATMAP_LEGEND"] = "Leyenda mapa de temperatura";
  this.Values["ES"]["NLS_FLOOR"] = "Planta";
  //Charts Options
  this.Values["EN"]["NLS_CHART_OPTION_SNAPSHOT"] = "Resumen";
  this.Values["ES"]["NLS_CHART_OPTION_STATS"] = "Estadísticas";
  this.Values["ES"]["NLS_CHART_OPTION_OCUPATION"] = "Ocupación";
  this.Values["ES"]["NLS_CHART_OPTION_CASH"] = "Caja";
  this.Values["ES"]["NLS_CHART_OPTION_SEGMENTATION"] = "Segmentación";
  //Charts Options-Items
  this.Values["ES"]["NLS_CHART_OPTION_STATS_PLAYED"] = "Jugado";
  this.Values["ES"]["NLS_CHART_OPTION_STATS_NWTOTAL"] = "NW Total";
  this.Values["ES"]["NLS_CHART_OPTION_STATS_NWCASHABLE"] = "NW Pagable";
  this.Values["ES"]["NLS_CHART_OPTION_STATS_TOTAL"] = "Total";
  this.Values["ES"]["NLS_CHART_OPTION_STATS_AVERAGE"] = "Promedio";
  this.Values["ES"]["NLS_CHART_OPTION_OCUPATION_PROV"] = "Proveedor";
  this.Values["ES"]["NLS_CHART_OPTION_OCUPATION_24H"] = "24h";
  this.Values["ES"]["NLS_CHART_OPTION_CASH_INPUTS"] = "Entradas";
  this.Values["ES"]["NLS_CHART_OPTION_CASH_RESULT"] = "Resultado";
  this.Values["ES"]["NLS_CHART_OPTION_SEGMENT_LP"] = "Lealtad";
  this.Values["ES"]["NLS_CHART_OPTION_SEGMENT_AGE"] = "Edad";
  this.Values["ES"]["NLS_CHART_OPTION_SEGMENT_NW"] = "NW Teórico";
  this.Values["ES"]["NLS_CHART_OPTION_SEGMENT_VISITS"] = "Visitas";
  this.Values["ES"]["NLS_CHART_OPTION_SEGMENT_CLIENTS"] = "Clientes";

  this.Values["ES"]["NLS_SESSION"] = "Sesión";
  this.Values["ES"]["NLS_LINK_TERMINAL"] = "Terminal";
  this.Values["ES"]["NLS_ACTUAL_SESSION"] = "Sesión Actual";
  this.Values["ES"]["NLS_CASH_IN"] = "Cash In";
  this.Values["ES"]["NLS_CASH_OUT"] = "Cash Out";
  this.Values["ES"]["NLS_DAY1"] = "Hoy";
  this.Values["ES"]["NLS_DAY2"] = "Ayer";
  this.Values["ES"]["NLS_DAY3"] = "Anteayer";


  this.Values["ES"]["NLS_UNDER_AGE"] = "Menor de %0";
  this.Values["ES"]["NLS_OVER_AGE"] = "%0 o más";

  this.Values["ES"]["NLS_ZOOMABLE"] = "Reporte con zoom, seleccione una zona para ampliar.";
  this.Values["ES"]["NLS_RESIZABLE"] = "Reporte redimensionable, click para cambiar tamaño.";

  this.Values["ES"]["NLS_LVL_0"] = "Anónimo";
  this.Values["ES"]["NLS_LVL_1"] = "BRONZE";
  this.Values["ES"]["NLS_LVL_2"] = "GOLD";
  this.Values["ES"]["NLS_LVL_3"] = "PLATINUM";
  this.Values["ES"]["NLS_LVL_4"] = "VIP";
  this.Values["ES"]["NLS_LVL_5"] = "Nivel 5";

  this.Values["ES"]["NLS_REPORT_STATS_TOP_10"] = "10 mejores terminales.";
  this.Values["ES"]["NLS_REPORT_STATS_BOTTOM_10"] = "10 peores terminales.";
  this.Values["ES"]["NLS_REPORT_STATS_PROVIDERS"] = "Total por proveedor los ultimos 10 dias.";
  this.Values["ES"]["NLS_REPORT_STATS_ACCUM"] = "Total acumulado los ultimos 10 dias.";
  this.Values["ES"]["NLS_REPORT_STATS_PERIOD"] = "Acumulado proveedores del dia seleccionado.";
  this.Values["ES"]["NLS_REPORT_OCCUP_GAUGE"] = "% de ocupacion en tiempo real.";
  this.Values["ES"]["NLS_REPORT_OCCUP_OCC"] = "Ocupacion por tipo de registro.";
  this.Values["ES"]["NLS_REPORT_OCCUP_LVL"] = "Ocupacion por niveles.";
  this.Values["ES"]["NLS_REPORT_OCCUP_AGE"] = "Ocupacion por edad";
  this.Values["ES"]["NLS_REPORT_OCCUP_GEN"] = "Ocupacion por genero";
  this.Values["ES"]["NLS_REPORT_OCCUP_PROV7"] = "Ocupacion por proveedor los ultimos 7 dias.";
  this.Values["ES"]["NLS_REPORT_OCCUP_PROV30"] = "Ocupacion por proveedor los ultimos 30 dias.";
  this.Values["ES"]["NLS_REPORT_CASH_RESULT"] = "Resultado de caja.";
  this.Values["ES"]["NLS_REPORT_CASH_UNBALANCED"] = "Sesiones de caja descuadradas del dia.";
  this.Values["ES"]["NLS_REPORT_CASH_CASHIERS"] = "Resultado de cajeros del dia.";
  this.Values["ES"]["NLS_REPORT_SEGM_LVL"] = "Segmentación por nivel del año.";
  this.Values["ES"]["NLS_REPORT_SEGM_AGE"] = "Segmentación por edad del año.";
  this.Values["ES"]["NLS_REPORT_SEGM_ADT"] = "Segmentación por programa de afiliación del año.";
  this.Values["ES"]["NLS_VIP"] = "VIP";
  this.Values["ES"]["NLS_LOAD"] = "Cargar";
  this.Values["ES"]["NLS_PROPERTIES"] = "Propiedades";
  this.Values["ES"]["NLS_ROLE_1"] = "Player's Club Attendant";
  this.Values["ES"]["NLS_ROLE_2"] = "Ops Manager";
  this.Values["ES"]["NLS_ROLE_4"] = "Slot Attendant";
  this.Values["ES"]["NLS_ROLE_8"] = "Technical Support";
  this.Values["ES"]["NLS_MONITOR"] = "Mis Filtros";

  this.Values["ES"]["NLS_FREE"] = "Libre";
  this.Values["ES"]["NLS_REGISTERED"] = "Registrado";

  this.Values["ES"]["NLS_DERIVATE"] = "Asignar"; //"Derivar";
  this.Values["ES"]["NLS_ACTIVATE"] = "Iniciar"; //"Accionar";
  this.Values["ES"]["NLS_OBSERVATIONS"] = "OBSERVACION";
  this.Values["ES"]["NLS_EVENTS_HISTORY"] = "HISTORIAL DE EVENTOS";
  this.Values["ES"]["NLS_FILTER_ALARMS"] = "Filtrar alarmas";
  this.Values["ES"]["NLS_VIEW_ALL"] = "Ver todas";
  this.Values["ES"]["NLS_TYPE"] = "Tipo";

  this.Values["ES"]["NLS_SCALED"] = "Escalada";
  this.Values["ES"]["NLS_LOCATE_ALARM"] = "Localizar alarma";
  this.Values["ES"]["NLS_FILTER_TASKS"] = "Filtrar tareas";
  this.Values["ES"]["NLS_PRIORITY"] = "Prioridad";
  this.Values["ES"]["NLS_PRIORITY_HIGH"] = "Alta";
  this.Values["ES"]["NLS_PRIORITY_MEDIUM"] = "Media";
  this.Values["ES"]["NLS_PRIORITY_LOW"] = "Baja";
  this.Values["ES"]["NLS_STATE"] = "Estado";
  this.Values["ES"]["NLS_STATE_PENDING"] = "Pendiente";
  this.Values["ES"]["NLS_STATE_IN_PROGRESS"] = "En curso";
  this.Values["ES"]["NLS_STATE_SOLVED"] = "Resuelta";
  this.Values["ES"]["NLS_STATE_VALIDATED"] = "Validada";
  this.Values["ES"]["NLS_STATE_SCALED"] = "Escalada";
  this.Values["ES"]["NLS_SEARCH"] = "Buscar";
  this.Values["ES"]["NLS_PRIORITY_HIGH_PRIORITY"] = "Prioridad alta";
  this.Values["ES"]["NLS_PRIORITY_MEDIUM_PRIORITY"] = "Prioridad media";
  this.Values["ES"]["NLS_PRIORITY_LOW_PRIORITY"] = "Prioridad baja";
  this.Values["ES"]["NLS_HIDE"] = "Ocultar";
  this.Values["ES"]["NLS_COLUMN_ATTATCH"] = "Adj.";
  this.Values["ES"]["NLS_COLUMN_DESCRIPTION"] = "Descripción";
  this.Values["ES"]["NLS_COLUMN_CREATED"] = "Creada";
  this.Values["ES"]["NLS_COLUMN_LENGTH"] = "Duración";
  this.Values["ES"]["NLS_COLUMN_TERMINAL"] = "Terminal";
  this.Values["ES"]["NLS_COLUMN_LOCATE"] = "Localizar";
  this.Values["ES"]["NLS_COLUMN_ACTION"] = "Acción";
  this.Values["ES"]["NLS_COLUMN_ASSIGNED"] = "Asignada";
  this.Values["ES"]["NLS_COLUMN_UPDATED"] = "Actualizada";
  this.Values["ES"]["NLS_COLUMN_STATE"] = "Estado";
  this.Values["ES"]["NLS_MODIFY_STATE"] = "Modificar estado";
  this.Values["ES"]["NLS_STATE_SCALED"] = "Escalada";
  this.Values["ES"]["NLS_ROLE_PCA"] = "Player's Club Attendant";
  this.Values["ES"]["NLS_ROLE_OPM"] = "OPS Manager";
  this.Values["ES"]["NLS_ROLE_SLA"] = "Slot Attendant";
  this.Values["ES"]["NLS_ROLE_TSP"] = "Technical Support";

  this.Values["ES"]["NLS_LANGUAGES"] = "Idioma";
  this.Values["ES"]["NLS_FLOOR_PLANE"] = "Plano";
  this.Values["ES"]["NLS_FLOOR"] = "Piso";
  this.Values["ES"]["NLS_TIME_VIEW"] = "Vista Horaria";
  this.Values["ES"]["NLS_SHOW_BACKGROUND"] = "Fondo";
  this.Values["ES"]["NLS_ROLE"] = "Rol";
  this.Values["ES"]["NLS_USER"] = "Usuario";
  this.Values["ES"]["NLS_SHOW_GRID"] = "Cuadrícula";
  this.Values["ES"]["NLS_SHOW_ALARMS"] = "Alarmas";
  this.Values["ES"]["NLS_VIEW_PERSPECTIVE"] = "Perspectiva";
  this.Values["ES"]["NLS_VIEW_CENITAL"] = "Cenital";
  this.Values["ES"]["NLS_VIEW_FIRSTPERSON"] = "1era Persona";
  this.Values["ES"]["NLS_REAL_TIME_STATE"] = "Estado en tiempo real";

  this.Values["ES"]["NLS_COININ"] = "Jugado";
  this.Values["ES"]["NLS_PLAYS"] = "Jugadas";
  this.Values["ES"]["NLS_COMPARE_TERMINAL"] = "Añadir a comparar";
  this.Values["ES"]["NLS_RETIRE_COMPARE_TERMINAL"] = "Quitar de comparar";
  this.Values["ES"]["NLS_CURRENT_SESSION"] = "Sesión Actual";
  this.Values["ES"]["NLS_NUMBER_OF_PLAYS"] = "Nº de Jugadas";
  this.Values["ES"]["NLS_COMPARE_PLAYER"] = "Añadir a comparar";
  this.Values["ES"]["NLS_RETIRE_COMPARE_PLAYER"] = "Quitar de comparar";

  this.Values["ES"]["NLS_DERIVATE_TO"] = "Asignar a"; //"Derivar a:";

  this.Values["ES"]["NLS_ROLES"] = "Roles";
  this.Values["ES"]["NLS_SEARCH_USER"] = "Buscar Usuario";
  this.Values["ES"]["NLS_SEVERITY"] = "Severidad";
  this.Values["ES"]["NLS_SEVERITY_HIGH"] = "Alta";
  this.Values["ES"]["NLS_SEVERITY_MEDIUM"] = "Media";
  this.Values["ES"]["NLS_SEVERITY_LOW"] = "Baja";
  this.Values["ES"]["NLS_COMMENT"] = "Comentario";
  this.Values["ES"]["NLS_TASK_INFO"] = "Info de la Tarea";
  this.Values["ES"]["NLS_MODIFY_TASK_STATE"] = "Modificar el estado de la Tarea";
  this.Values["ES"]["NLS_ALARM_COMENT"] = "Comentario de la Alarma";
  this.Values["ES"]["NLS_MODIFY"] = "MODIFICAR";
  this.Values["ES"]["NLS_CHAT_WITH"] = "Conversación con ";
  this.Values["ES"]["NLS_CHAT_CONTACT"] = "Contactos";
  this.Values["ES"]["NLS_SEND"] = "Enviar";

  this.Values["ES"]["NLS_CREATE_FILTER"] = "Crear filtro";
  this.Values["ES"]["NLS_FILTER_NAME"] = "Nombre del Filtro";
  this.Values["ES"]["NLS_PLAY_SOUND"] = "Reprod. Sonido";
  this.Values["ES"]["NLS_ENABLED"] = "Habilitado";

  this.Values["ES"]["NLS_FILTER_ITEMS"] = "Items";
  this.Values["ES"]["NLS_COLOR"] = "Color";
  this.Values["ES"]["NLS_FILTER"] = "Filtrar";
  this.Values["ES"]["NLS_SAVE"] = "Guardar";
  this.Values["ES"]["NLS_OK"] = "Aceptar";
  this.Values["ES"]["NLS_CANCEL"] = "Cancelar";
  this.Values["ES"]["NLS_CATEGORY"] = "Categoría";

  this.Values["ES"]["NLS_ERROR"] = "Error";
  this.Values["ES"]["NLS_SELECT_A_FILTER"] = "Debe seleccionar algún filtro.";
  this.Values["ES"]["NLS_CUSTOM_FILTER"] = "FILTRO PERSONALIZADO";
  this.Values["ES"]["NLS_FILTER_EDIT"] = "Editar Filtro";

  this.Values["ES"]["NLS_WARNING"] = "Aviso";
  this.Values["ES"]["NLS_SURE_TO_EXIT_FILTER_CREATION_MODE"] = "¿Esta seguro de querer abandonar el modo de creación/edición de filtros?";

  this.Values["ES"]["NLS_CLOSE_LIST"] = "Cerrar Lista";
  this.Values["ES"]["NLS_DELETE_CURRENT_RECORD"] = "¿Seguro que desea eliminar el registro actual?";
  this.Values["ES"]["NLS_COMPARE_PLAYERS"] = "Comparar Jugadores";
  this.Values["ES"]["NLS_LOCATE"] = "Localizar";
  this.Values["ES"]["NLS_COMPARE"] = "Comparar";

  this.Values["ES"]["NLS_COMPARE_TERMINALS"] = "Comparar Terminales";
  this.Values["ES"]["NLS_INFO"] = "Info.";
  this.Values["ES"]["NLS_LOCATE_TERMINAL"] = "Localizar Terminal";

  this.Values["ES"]["NLS_ALARM_SOUND"] = "Sonido Alarma";
  this.Values["ES"]["NLS_EDIT_FILTERS"] = "Editar Filtros";
  this.Values["ES"]["NLS_CLEAN"] = "Limpiar";

  this.Values["ES"]["NLS_VIEW_2D"] = "2D";
  this.Values["ES"]["NLS_VIEW_3D"] = "3D";

  this.Values["ES"]["NLS_SUBCATEGORY"] = "Subcategoría";
  this.Values["ES"]["NLS_ASSIGNED_TO"] = "Asignada a";

  this.Values["ES"]["NLS_TASKS_ERROR_ASSIGN"] = "La tarea no ha sido asignada";
  this.Values["ES"]["NLS_TASKS_ERROR_MODIFY"] = "La tarea no ha sido modificada";
  this.Values["ES"]["NLS_TASKS_ERROR_MODIFY_STATUS"] = "Debe modificar el estado de la tarea"

  this.Values["ES"]["NLS_PLAYER_WORKING_SESION"] = "Jornada";
  this.Values["ES"]["NLS_PLAYER_CURRENT_SESION"] = "Actual";
  this.Values["ES"]["NLS_PLAYER_LAST_VISITS"] = "Últ. Visitas";
  this.Values["ES"]["NLS_NO_TASKS_ASSIGNED"] = "Sin tarea asignada."

  this.Values["ES"]["NLS_STATUS_CHANGED"] = "Cambio de estado a ";
  this.Values["ES"]["NLS_STATUS_CHANGED_REASON"] = "Motivo";
  this.Values["ES"]["NLS_N_HOURS"] = "Más de 48 horas";
  this.Values["ES"]["NLS_ERR_SEVERITY"] = "*Debe seleccionar una severidad.";
  this.Values["ES"]["NLS_ERR_USER_ROLE_ASSIGNED"] = "*Debe seleccionar un rol o usuario para asignar.";
  this.Values["ES"]["NLS_PLAYER"] = "Jugador";
  this.Values["ES"]["NLS_MACHINE"] = "Máquina";
  this.Values["ES"]["NLS_CATEGORY"] = "Categoría";
  this.Values["ES"]["NLS_SEARCH_TERMINAL"] = "Buscar Terminal";
  this.Values["ES"]["NLS_WIDGET_MACHINE_OCC_ACTIVE"] = "Libres";
  this.Values["ES"]["NLS_WIDGET_MACHINE_OCC_IN_SESSION"] = "En sesión";



  this.Values["ES"]["NLS_WIDGET_OCC_YEAR"] = "Año";
  this.Values["ES"]["NLS_WIDGET_OCC_MONTH"] = "Mes";
  this.Values["ES"]["NLS_WIDGET_OCC_WEEK"] = "Semana";
  this.Values["ES"]["NLS_WIDGET_OCC_TODAY"] = "Hoy";

  this.Values["ES"]["NLS_WIDGET_OCC_LAST_YEAR"] = "Último Año";
  this.Values["ES"]["NLS_WIDGET_OCC_LAST_MONTH"] = "Último Mes";
  this.Values["ES"]["NLS_WIDGET_OCC_LAST_WEEK"] = "Última Semana";
  this.Values["ES"]["NLS_WIDGET_OCC_YESTERDAY"] = "Ayer";
  this.Values["ES"]["NLS_WIDGET_OCC_CURRENT"] = "Actual";

  this.Values["ES"]["NLS_TERMINAL_CURRENTPLAY"] = "Juego Actual";
  this.Values["ES"]["NLS_TERMINAL_PAYOUTPLAY"] = "Payout Actual";
  this.Values["ES"]["NLS_TERMINAL_SESSION"] = "Sesión";
  this.Values["ES"]["NLS_TERMINAL_LINKPLAYER"] = "Link Jugador";


  this.Values["ES"]["NLS_SURE_TO_DELETE_CUSTOM_FILTER"] = "¿Esta seguro de querer eliminar el filtro personalizado?";

  this.Values["ES"]["NLS_ALARMS_ALL"] = "Todas";
  this.Values["ES"]["NLS_ALARMS_PRIORITY_HIGH"] = "Prioridad Alta";
  this.Values["ES"]["NLS_ALARMS_PRIORITY_MEDIUM"] = "Prioridad Media";
  this.Values["ES"]["NLS_ALARMS_PRIORITY_LOW"] = "Prioridad Baja";
  this.Values["ES"]["NLS_ALARMS_LASTS"] = "Últimas";
  this.Values["ES"]["NLS_ALARMS_ACTIVE"] = "Activas";
  this.Values["ES"]["NLS_POP_YESNO_YES"] = "Sí";
  this.Values["ES"]["NLS_POP_YESNO_NO"] = "No";
  this.Values["ES"]["NLS_NO_NAME"] = "Introduzca un nombre";
  this.Values["ES"]["NLS_ERROR_FILTER_DISPLAY"] = "Debe seleccionar un filtro para visualizar";
  this.Values["ES"]["NLS_MESSAGING_ERROR"] = "Estamos experimentando dificultades para enviar este mensaje. Por favor contacte a soporte técnico";

  this.Values["ES"]["NLS_MY_TASKS"] = "Tareas";
  this.Values["ES"]["NLS_DASHBOARD"] = "Dashboard";
  this.Values["ES"]["NLS_DASH_STATS"] = "Estadísticas";
  this.Values["ES"]["NLS_DASH_FEAT"] = "Destacados";
  this.Values["ES"]["NLS_FLOOR"] = "Piso";
  this.Values["ES"]["NLS_TERMINALS"] = "Máquinas";
  this.Values["ES"]["NLS_TERMINALS_SEARCH_PLACEHOLDER"] = "Buscar terminales";
  this.Values["ES"]["NLS_PLAYERS"] = "Jugadores";
  this.Values["ES"]["NLS_PLAYERS_SEARCH_PLACEHOLDER"] = "Buscar Jugadores";
  this.Values["ES"]["NLS_ALARMS"] = "Alarmas";
  this.Values["ES"]["NLS_CHARTS"] = "Charts";
  this.Values["ES"]["NLS_FLOOR_COMPARE_TERMS"] = "Comparar";
  this.Values["ES"]["NLS_FLOOR_TEMP_MAP"] = "Mapa Temp.";
  this.Values["ES"]["NLS_FLOOR_VISUAL"] = "Visualización";
  this.Values["ES"]["NLS_PLAYERS_SEARCH"] = "Buscar";
  this.Values["ES"]["NLS_PLAYERS_FILTERS"] = "Filtros";
  this.Values["ES"]["NLS_PLAYERS_LIST"] = "Modo Lista";
  this.Values["ES"]["NLS_FLOOR_FILTERS"] = "Filtros"

  this.Values["ES"]["NLS_WIDGET_TITLE_SEGMENT_AGE"] = "OCUPACIÓN POR EDAD";
  this.Values["ES"]["NLS_WIDGET_TITLE_SEGMENT_LEVEL"] = "Ocupación por Nivel";
  this.Values["ES"]["NLS_WIDGET_TITLE_RUNNERS"] = "ASISTENTE DE PISO";
  this.Values["ES"]["NLS_WIDGET_TITLE_OCCUPANCY"] = "OCUPACIÓN";

  this.Values["ES"]["NLS_WIDGET_MACHINE_OCC_TITLE"] = "Ocupación de máquinas";
  this.Values["ES"]["NLS_WIDGET_TOP10_TITLE"] = "TOP 10 [JUGADO]";
  this.Values["ES"]["NLS_WIDGET_TOPWINNINGPLAYER_TITLE"] = "TOP JUGADORES GANADORES";
  this.Values["ES"]["NLS_WIDGET_TOPLOSINGPLAYER_TITLE"] = "TOP JUGADORES PERDEDORES";
  this.Values["ES"]["NLS_WIDGET_VIPS_TITLE"] = "VIPS";
  this.Values["ES"]["NLS_WIDGET_HIGHROLLERS_TITLE"] = "High Rollers";
  this.Values["ES"]["NLS_WIDGET_COININ_TITLE"] = "JUGADO";
  this.Values["ES"]["NLS_WIDGET_AVERAGEBET_TITLE"] = "APUESTA MEDIA";
  this.Values["ES"]["NLS_WIDGET_TASKS_TITLE"] = "TAREAS";
  this.Values["ES"]["NLS_WIDGET_TASKS_TECHNICAL"] = "TÉCNICAS";
  this.Values["ES"]["NLS_WIDGET_TASKS_PLAYERS"] = "JUGADOR";
  this.Values["ES"]["NLS_WIDGET_TASKS_CUSTOM"] = "PERSONALIZADAS";
  this.Values["ES"]["NLS_WIDGET_TASKS_PENDING"] = "EN CURSO";
  this.Values["ES"]["NLS_WIDGET_TASKS_ASSIGNED"] = "ASIGNADA";
  this.Values["ES"]["NLS_WIDGET_TASKS_SOLVED"] = "RESUELTA";
  this.Values["ES"]["NLS_WIDGET_OCCVSWEATHER_TITLE"] = "OCUPACIÓN vs TIEMPO";
  this.Values["ES"]["NLS_WIDGET_GENDER_TITLE"] = "OCUPACIÓN POR GÉNERO";
  this.Values["ES"]["NLS_WIDGET_GENDER_MALE"] = "Hombres";
  this.Values["ES"]["NLS_WIDGET_GENDER_FEMALE"] = "Mujeres";
  this.Values["ES"]["NLS_WIDGET_BIRTHDAY_TITLE"] = "CUMPLEAÑOS";
  this.Values["ES"]["NLS_CAMERAS"] = "CÁMARAS";

  this.Values["ES"]["NLS_WIDGET_UNKNOWN"] = "Anónimos";
  this.Values["ES"]["NLS_BRONZE"] = "BRONCE";
  this.Values["ES"]["NLS_SILVER"] = "PLATA";
  this.Values["ES"]["NLS_GOLD"] = "ORO";
  this.Values["ES"]["NLS_DIAMOND"] = "DIAMANTE";

  this.Values["ES"]["NLS_WIDINF_NO_INFO"] = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus mattis, nulla quis interdum ultrices, lorem dolor laoreet felis, et semper dolor ex id ligula. Phasellus ac nulla mollis, elementum eros placerat, laoreet justo. Praesent ac placerat ipsum, vel cursus mi. Mauris pulvinar ornare sem sed pharetra. Praesent faucibus, diam ac egestas congue, ante magna vestibulum velit, placerat posuere enim felis id sapien. Curabitur risus risus, consequat at elit sit amet, auctor tincidunt urna. Integer pharetra bibendum massa non malesuada.";
  this.Values["ES"]["NLS_WIDINF_SITE_AVERAGE_BET"] = "<P><I>Evolución de la <B>apuesta media</B> de la <B>sala</B> en tiempo real.</I></P>";
  this.Values["ES"]["NLS_WIDINF_SEGMENT_AGE"] = "<P><I>Porcentaje de <B>máquinas en sesión de juego</B> según la <B>edad del jugador</B>.</I></P>";
  this.Values["ES"]["NLS_WIDINF_SEGMENT_LEVEL"] = "<P><I>Porcentaje de <B>máquinas en sesión de juego</B> según <B>el nivel del jugador </B> en el Programa de Lealtad.</I></P>";

  this.Values["ES"]["NLS_AUTO_NOTIFICATION_TASK_CREATED"] = "Tarea Creada";
  this.Values["ES"]["NLS_HISTORY_COMMENTS"] = "Comentario:";

  this.Values["ES"]["NLS_WIDGET_PLAYERS_MAX_LVL_TITLE"] = "JUGADORES CON NIVEL MÁXIMO";
  this.Values["ES"]["NLS_WIDGET_PLAYERS_MAX_LVL_COIN_IN"] = "Jugad.:";
  this.Values["ES"]["NLS_WIDGET_PLAYERS_MAX_LVL_BET_AVG"] = "Ap. M.:";

  this.Values["ES"]["NLS_WIDGET_HIGHROLLER_COIN_IN"] = "CoinIn:";
  this.Values["ES"]["NLS_WIDGET_HIGHROLLER_BET_AVG"] = "Ap. Med:";
  this.Values["ES"]["NLS_WIDGET_GENERAL"] = "General";
  this.Values["ES"]["NLS_SESSION_ACTIVE"] = "Activa";
  this.Values["ES"]["NLS_SESSION_INACTIVE"] = "Inactiva";
  this.Values["ES"]["NLS_RUNNER_CURRENT"] = "Tareas en proceso";
  this.Values["ES"]["NLS_RUNNER_LAST"] = "Últimas tareas";

  this.Values["ES"]["NLS_RUNNER_INFO_ASSIGNED"] = "Asignada:";
  this.Values["ES"]["NLS_RUNNER_INFO_INPROGRESS"] = "Aceptada:";
  this.Values["ES"]["NLS_RUNNER_INFO_SOLVED"] = "Resuelta:";
  this.Values["ES"]["NLS_RUNNER_INFO_SCALED"] = "Escalada:";
  this.Values["ES"]["NLS_DOW_SUNDAY"] = "DOM";
  this.Values["ES"]["NLS_DOW_MONDAY"] = "LUN";
  this.Values["ES"]["NLS_DOW_TUESDAY"] = "MAR";
  this.Values["ES"]["NLS_DOW_WEDNESDAY"] = "MIE";
  this.Values["ES"]["NLS_DOW_THURSDAY"] = "JUE";
  this.Values["ES"]["NLS_DOW_FRIDAY"] = "VIE";
  this.Values["ES"]["NLS_DOW_SATURDAY"] = "SAB";


  // DMT 26/05/2016
  this.Values["ES"]["NLS_WIDGET_HIGHROLLERS_INFO"] = "<<P><I>Listado de jugadores activos con una apuesta media o monto jugado elevados durante un periodo de tiempo. Puede configurar estos límites desde WigosGUI. Toque el icono de localización para mostrar al jugador en el plano del piso.</I></P>";
  this.Values["ES"]["NLS_WIDINF_AVERAGE_BET_INFO"] = "<<P><I>Apuesta media durante la hora actual, comparado con la apuesta media el mismo día de la semana anterior (a la misma hora), el promedio del mismo día de la semana durante el mes anterior (a la misma hora) y el promedio del mismo día de la semana durante el año anterior (a la misma hora).</I></P>";
  this.Values["ES"]["NLS_WIDGET_PLAYERS_MAX_LVL_INFO"] = "<<P><I>Listado de jugadores activos que pertenecen al nivel superior del Programa de Lealtad. Toque el icono de localización para mostrar al jugador en el plano del piso.</I></P>";
  this.Values["ES"]["NLS_WIDGET_TOPWINNINGPLAYER_INFO"] = "<<P><I>Listado de jugadores activos que acumulan mayores ganancias en la sesión de juego actual. Toque el icono de localización para mostrar al jugador en el plano del piso.</I></P>";
  this.Values["ES"]["NLS_WIDGET_TOPLOSINGPLAYER_INFO"] = "<<P><I>Listado de jugadores activos que acumulan mayores pérdidas en la sesión de juego actual. Toque el icono de localización para mostrar al jugador en el plano del piso.</I></P>";
  this.Values["ES"]["NLS_WIDGET_VIPS_INFO"] = "<<P><I>Listado de jugadores VIP activos. Toque el icono de localización para mostrar al jugador en el plano del piso.</I></P>";
  this.Values["ES"]["NLS_WIDGET_BIRTHDAY_INFO"] = "<<P><I>Listado de jugadores activos que cumplen años hoy. Toque el icono de localización para mostrar al jugador en el plano del piso.</I></P>";
  this.Values["ES"]["NLS_WIDGET_MACHINE_OCC_INFO"] = "<<P><I>Porcentaje de máquinas activas que actualmente están en sesión de juego.</I></P>";
  this.Values["ES"]["NLS_WIDGET_GENDER_INFO"] = "<<P><I>Porcentaje de máquinas en sesión de juego según el sexo del jugador.</I></P>";
  this.Values["ES"]["NLS_WIDGET_COININ_INFO"] = "<<P><I>Monto jugado acumulado durante la hora actual, comparado con el monto jugado acumulado el mismo día de la semana anterior (a la misma hora), el promedio del mismo día de la semana durante el més anterior (a la misma hora) y el promedio del mismo día de la semana durante el año anterior (a la misma hora).</I></P>";
  this.Values["ES"]["NLS_WIDGET_TITLE_OCCUPANCY_INFO"] = "<<P><I>Ocupación actual de la sala, comparada con el mismo día de la semana anterior (a la misma hora), el promedio del mismo día de la semana durante el mes anterior (a la misma hora) y el promedio del mismo día de la semana durante el año anterior (a la misma hora).</I></P>";
  this.Values["ES"]["NLS_WIDGET_NETWIN_INFO"] = "<<P><I>Netwin y monto jugado acumulados durante la jornada actual, comparados con la jornada anterior.</I></P>";
  this.Values["ES"]["NLS_WIDGET_OCCVSWEATHER_INFO"] = "<<P><I>Ocupación real promedio de la sala durante los últimos 7 días, comparada con las condiciones meteorológicas.</I></P>";
  this.Values["ES"]["NLS_WIDGET_TASKS_INFO"] = "<<P><I>Listado de tareas activas durante la jornada actual, agrupadas por tipo y estado.</I></P>";
  this.Values["ES"]["NLS_WIDGET_RUNNERS_INFO"] = "<<P><I>Listado de asistentes de piso que están actualmente conectados a Intellia App y sus roles. Toque el icono de localización para mostrar al runner en el plano del piso.</I></P>";

  this.Values["ES"]["NLS_LOADING_FLOOR"] = "Cargando...";
  this.Values["ES"]["NLS_LOADING_FLOOR_TITLE"] = "Piso";

  this.Values["ES"]["NLS_ALARMS_SHOW_RUNNERS"] = "Mostrar Asistentes de Piso";

  //ETP 08/07/2016
  this.Values["ES"]["NLS_LOGOUT"] = "Salir";
  this.Values["ES"]["NLS_NO_CHATS"] = "No hay chats aquí… Por favor seleccione un contacto para ver o empezar un chat";
  this.Values["ES"]["NLS_PLEASE_WAIT"] = "Por favor espere...";
  this.Values["ES"]["NLS_LOADING"] = "Cargando...";
  this.Values["ES"]["NLS_RESOURCE_LIST"] = "Lista de recursos";
  this.Values["ES"]["NLS_TO_HOUR"] = "a";

  this.Values["ES"]["NLS_WIDGET_SITE_OCC_TITLE"] = "OCUPACIÓN DE SALA";
  this.Values["ES"]["NLS_WIDGET_SITE_OCC_INFO"] = "<<P><I>Ocupación actual de la sala.</I></P>";
  this.Values["ES"]["NLS_WIDGET_MACHINE_OCC_SITE_OCC_TITLE"] = "OCUPACIÓN MÁQUINA vs OCUPACIÓN SALA";
  this.Values["ES"]["NLS_WIDGET_MACHINE_OCC_SITE_OCC_INFO"] = "<<P><I>Máquinas activas que actualmente están en sesión de juego, comparada con la ocupación actual de la sala.</I></P>";
  this.Values["ES"]["NLS_WIDGET_MACHINE_OCC_SITE_OCC_FREE_OCC"] = "Sin jugar";
  this.Values["ES"]["NLS_WIDGET_SITE_OCCUPATION_TOTAL"] = "Aforo";
  this.Values["ES"]["NLS_WIDGET_SITE_OCCUPATION_CURRENT"] = "En sala";

  //AVZ 21/12/2016
  this.Values["ES"]["NLS_TABLE_NO_DATA_AVAILABLE"] = "No hay datos disponibles!";
  this.Values["ES"]["NLS_TABLE_PAGE_SIZE"] = "Registros por página";
  this.Values["ES"]["NLS_TABLE_GO_TO_PAGE"] = "Ir a página";
  this.Values["ES"]["NLS_RUNNERS_IN_THE_ZONE"] = "ASISTENTES DE PISO EN LA ZONA";
  this.Values["ES"]["NLS_LEVEL_0"] = "UNKNOWN";
  this.Values["ES"]["NLS_LEVEL_1"] = "Anonymous";
  this.Values["ES"]["NLS_LEVEL_2"] = "Bronze";
  this.Values["ES"]["NLS_LEVEL_3"] = "Silver";
  this.Values["ES"]["NLS_LEVEL_4"] = "Gold";
  this.Values["ES"]["NLS_LEVEL_4"] = "Platinum";

  this.Values["ES"]["NLS_LEVEL_1"] = "BRONZE";
  this.Values["ES"]["NLS_LEVEL_2"] = "GOLD";
  this.Values["ES"]["NLS_LEVEL_3"] = "PLATINUM";
  this.Values["ES"]["NLS_LEVEL_4"] = "VIP";

  this.Values["ES"]["NLS_LEVEL_1"] = "BRONZE";
  this.Values["ES"]["NLS_LEVEL_2"] = "GOLD";
  this.Values["ES"]["NLS_LEVEL_3"] = "PLATINUM";
  this.Values["ES"]["NLS_LEVEL_4"] = "VIP";

  this.Values["ES"]["DOMINGO"] = "dom.";
  this.Values["ES"]["LUNES"] = "lun.";
  this.Values["ES"]["MARTES"] = "mar.";
  this.Values["ES"]["MIERCOLES"] = "mie.";
  this.Values["ES"]["JUEVES"] = "jue.";
  this.Values["ES"]["VIERNES"] = "vie.";
  this.Values["ES"]["SABADO"] = "sab.";
  this.Values["ES"]["NLS_BET_AVERAGE"] = "Apuesta media";
  this.Values["ES"]["NLS_DOOR"] = "Puerta";
  this.Values["ES"]["NLS_BILL_ACEPTOR"] = "Aceptador de billetes";
  this.Values["ES"]["NLS_PRINTER"] = "Impresora";
  this.Values["ES"]["NLS_PLAYED_WON"] = "Jugado/Ganado";
  this.Values["ES"]["NLS_SUPPORT"] = "Soporte";
  this.Values["ES"]["NLS_JACKPOT"] = "Jackpot";
  this.Values["ES"]["NLS_VENDOR"] = "Vendedor";
  this.Values["ES"]["NLS_DENOMINATION"] = "Denominación";
  this.Values["ES"]["NLS_VIPS"] = "VIPS";
  this.Values["ES"]["NLS_CALL_ATTENDANT"] = "Call Attendant";
  this.Values["ES"]["NLS_HIGH_ROLLER"] = "High Roller";
  this.Values["ES"]["NLS_OCCUPANCY"] = "Ocupación";
  this.Values["ES"]["NLS_CUSTOM_CUSTOM"] = "Personalizada (Otros)";
  this.Values["ES"]["NLS_CUSTOM_PLAYER"] = "Personalizada (Jugador)";
  this.Values["ES"]["NLS_CUSTOM_MACHINE"] = "Personalizada (Máquina)";
  this.Values["ES"]["NLS_PAYOUT"] = "Payout";
  this.Values["ES"]["NLS_WITHOUT_PLAYS"] = "Sin jugadas";
  this.Values["ES"]["NLS_BILL_JAM"] = "Atasco billete";
  this.Values["ES"]["NLS_COUNTERFEITS"] = "Billetes falsos";
  this.Values["ES"]["NLS_STACKER_ALMOST_FULL"] = "Stacker casi lleno";
  this.Values["ES"]["NLS_STACKER_FULL"] = "Stacker lleno";
  this.Values["ES"]["NLS_COM_ERROR"] = "Error comunicación";
  this.Values["ES"]["NLS_OUTPUT_ERROR"] = "Fallo salida";
  this.Values["ES"]["NLS_RIBBON_REPLACEMENT"] = "Reemplazo cinta";
  this.Values["ES"]["NLS_LOW_PAPER"] = "Papel bajo";
  this.Values["ES"]["NLS_CARRIAGE_JAM"] = "Atasco carro";
  this.Values["ES"]["NLS_POWER_SUPPLY"] = "Fallo de alimentación";
  this.Values["ES"]["NLS_CARRIAGE_JAM"] = "Atasco carro";
  this.Values["ES"]["NLS_SLOT_DOOR"] = "Slot door";
  this.Values["ES"]["NLS_DROP_DOOR"] = "Drop door";
  this.Values["ES"]["NLS_CARD_CAGE_DOOR"] = "Card cage door";
  this.Values["ES"]["NLS_BELLY_DOOR"] = "Belly door";
  this.Values["ES"]["NLS_CASHBOX_DOOR"] = "Cashbox door";
  this.Values["ES"]["NLS_ERROR_CMOS_RAM"] = "Error CMOS RAM";
  this.Values["ES"]["NLS_ERROR_EEPROM"] = "Error EEPROM";
  this.Values["ES"]["NLS_LOW_BATTERY"] = "Batería baja";
  this.Values["ES"]["NLS_CANCELLED_CREDITS"] = "Créditos cancelados";
  this.Values["ES"]["NLS_HIGH_ROLLER_ANONYMOUS"] = "High roller anónimo";
  this.Values["ES"]["NLS_NO_LICENSE"] = "No existe una licencia válida de Intellia";
  this.Values["ES"]["NLS_RETURN"] = "Volver";

  this.Values["ES"]["NLS_RESET_DASHBOARD"] = "Reacomodar Tablero de Control";
  this.Values["ES"]["NLS_SOUND"] = "Sonido";
  this.Values["ES"]["NLS_MESSAGES"] = "Mensajes";
  this.Values["ES"]["NLS_FLOOR_CONFIG"] = "Configuración del piso";

  this.Values["ES"]["NLS_ID"] = "ID";


  // ------------------------- //
  //  English
  // ------------------------- //
  this.Values["EN"]["NLS_ACTIVITY"] = "Activity";
  this.Values["EN"]["NLS_LAYOUT"] = "Layout";
  this.Values["EN"]["NLS_CONFIGURATION"] = "Configuration";
  this.Values["EN"]["NLS_BACKGROUND"] = "Background";
  this.Values["EN"]["NLS_LANGUAGE"] = "Language";
  this.Values["EN"]["NLS_NAVIGATION"] = "Navigation controls";
  this.Values["EN"]["NLS_REFRESH"] = "Refresh";
  this.Values["EN"]["NLS_SLOTSSIZE"] = "Machine size";
  this.Values["EN"]["NLS_ONOFF"] = "OFF/ON";
  this.Values["EN"]["NLS_RANGES"] = "Ranges";
  this.Values["EN"]["NLS_OCUPATION"] = "Occupancy";
  this.Values["EN"]["NLS_BYUSE"] = "Use";
  this.Values["EN"]["NLS_BYLEVEL"] = "Level";
  this.Values["EN"]["NLS_BYAGE"] = "Age";
  this.Values["EN"]["NLS_BETAVERAGE"] = "Average bet";
  this.Values["EN"]["NLS_LEGEND"] = "Legend";
  this.Values["EN"]["NLS_ALARMS"] = "Alarms";
  this.Values["EN"]["NLS_ALARMS_ONLY_PLAYERS"] = "Filters";
  this.Values["EN"]["NLS_ALARMS_ONLY_MACHINES"] = "Filters";
  this.Values["EN"]["NLS_ALARM"] = "Alarm";
  this.Values["EN"]["NLS_TERMINALSNAPSHOTS"] = "Machine status";
  this.Values["EN"]["NLS_CLIENTSNAPSHOTS"] = "Player status";
  this.Values["EN"]["NLS_CUSTOMERSATROOM"] = "Current players";
  this.Values["EN"]["NLS_SHOWNAVIGATIONCONTROLS"] = "Show navigation controls";
  this.Values["EN"]["NLS_SHOWLABELS"] = "Show labels";
  this.Values["EN"]["NLS_SHOWSLOTLABELS"] = "Show machine labels";
  this.Values["EN"]["NLS_SHOWAREAS"] = "Show areas";
  this.Values["EN"]["NLS_SHOWBANKS"] = "Show banks";
  this.Values["EN"]["NLS_SNAPSHOTINREALTIME"] = "Real-time status";
  this.Values["EN"]["NLS_SMALL"] = "Small";
  this.Values["EN"]["NLS_BIG"] = "Big";
  this.Values["EN"]["NLS_SHOWALARMS"] = "Show alarms";
  this.Values["EN"]["NLS_SHOWIMAGE"] = "Show image";
  this.Values["EN"]["NLS_SHOWGRID"] = "Show grid";
  this.Values["EN"]["NLS_ACCOUNTID"] = "Account";
  this.Values["EN"]["NLS_TRACKDATA"] = "Card";
  this.Values["EN"]["NLS_NAME"] = "Name";
  this.Values["EN"]["NLS_LEVEL"] = "Level";
  this.Values["EN"]["NLS_AGE"] = "Age";
  this.Values["EN"]["NLS_TERMINAL"] = "Machine";
  this.Values["EN"]["NLS_PROVIDER"] = "Vendor";
  this.Values["EN"]["NLS_BANK"] = "Bank";
  this.Values["EN"]["NLS_AREA"] = "Area";
  this.Values["EN"]["NLS_SNAPSHOT"] = "Status";
  this.Values["EN"]["NLS_KEEPER"] = "Locate";
  this.Values["EN"]["NLS_CURRENTCLIENT"] = "Current player";
  this.Values["EN"]["NLS_GENDER"] = "Gender";
  this.Values["EN"]["NLS_UNKNOWN"] = "Unknown";
  this.Values["EN"]["NLS_MALE"] = "Male";
  this.Values["EN"]["NLS_FEMALE"] = "Female";
  this.Values["EN"]["NLS_TERMINALTODAY"] = "Today";
  this.Values["EN"]["NLS_CURRENTPLAYSESSION"] = "Current play session";
  this.Values["EN"]["NLS_BETAV"] = "Average bet";
  this.Values["EN"]["NLS_PLAYED"] = "Played";
  this.Values["EN"]["NLS_COIN_IN"] = "Coin in";
  this.Values["EN"]["NLS_PLAYEDCOUNT"] = "No. of plays";
  this.Values["EN"]["NLS_WON"] = "Won";
  this.Values["EN"]["NLS_COIN_OUT"] = "Coin out";
  this.Values["EN"]["NLS_NETWIN"] = "Netwin";
  this.Values["EN"]["NLS_DURATION"] = "Duration";
  this.Values["EN"]["NLS_INACTIVE"] = "Idle";
  this.Values["EN"]["NLS_ACTIVE"] = "Active";
  this.Values["EN"]["NLS_MACHINE_ACTIVE"] = "Active";
  this.Values["EN"]["NLS_MACHINE_OUTOFSERVICE"] = "Out of Service";
  this.Values["EN"]["NLS_MACHINE_RETIRED"] = "Retired";
  this.Values["EN"]["NLS_ANONYMOUS"] = "Anonymous";
  this.Values["EN"]["NLS_LESSTHAN"] = "Less than";
  this.Values["EN"]["NLS_ORMORE"] = "or more";

  this.Values["EN"]["NLS_PV"] = "Pv";
  this.Values["EN"]["NLS_T"] = "T";
  this.Values["EN"]["NLS_P"] = "P"; // Played = Coin in / CI
  this.Values["EN"]["NLS_W"] = "W"; // Won = Coin out / CO
  this.Values["EN"]["NLS_NP"] = "NP";
  this.Values["EN"]["NLS_%P"] = "AB";

  this.Values["EN"]["NLS_BILLIN"] = "Bill in";
  this.Values["EN"]["NLS_SPANISH"] = "Spanish";
  this.Values["EN"]["NLS_ENGLISH"] = "English";
  this.Values["EN"]["NLS_DATE"] = "Date";
  this.Values["EN"]["NLS_AMOUNT"] = "Amount";
  this.Values["EN"]["NLS_STACKER"] = "Stacker";
  this.Values["EN"]["NLS_AL_SASHOST"] = "SAS-HOST";
  this.Values["EN"]["NLS_AL_DOOR"] = "DOOR";
  this.Values["EN"]["NLS_AL_BILL"] = "BILL";
  this.Values["EN"]["NLS_AL_PRINTER"] = "PRINTER";
  this.Values["EN"]["NLS_AL_EGM"] = "EGM";
  this.Values["EN"]["NLS_AL_ATTENDANT"] = "ATTENDANT";
  this.Values["EN"]["NLS_AL_BLOQUED"] = "LOCK";
  this.Values["EN"]["NLS_AL_PLAYER_WON"] = "al_played_won_flags";
  this.Values["EN"]["NLS_AL_JACKPOT"] = "JACKPOT";
  this.Values["EN"]["NLS_AL_STACKER_STATUS"] = "al_stacker_status";
  this.Values["EN"]["NLS_AL_MACHINE"] = "al_machine_flags";
  this.Values["EN"]["NLS_AL_BIG_WON"] = "al_big_won";
  this.Values["EN"]["NLS_CAMERAS"] = "CAMERAS";
  this.Values["EN"]["NLS_SUBCATEGORY"] = "Subcategory";

  this.Values["EN"]["NLS_CHARTS"] = "Charts";

  this.Values["EN"]["NLS_ALARM_VIP"] = "VIP customer in room %0";
  this.Values["EN"]["NLS_ALARM_ERROR"] = "ERROR in terminal %0";

  this.Values["EN"]["NLS_DETAIL"] = "Details";
  this.Values["EN"]["NLS_REALTIME"] = "Real-time";
  this.Values["EN"]["NLS_HISTORIC"] = "History";
  this.Values["EN"]["NLS_MODE"] = "Mode";
  this.Values["EN"]["NLS_MONITOR"] = "My filters";
  this.Values["EN"]["NLS_ENABLED"] = "Enabled";
  this.Values["EN"]["NLS_CLOSE"] = "Close";
  this.Values["EN"]["NLS_EXIT"] = "Exit";
  this.Values["EN"]["NLS_HEATMAP"] = "Heatmap";
  this.Values["EN"]["NLS_HEATMAP_LEGEND"] = "Heatmap legend";
  this.Values["EN"]["NLS_FLOOR"] = "Floor";

  //Charts Options   
  this.Values["EN"]["NLS_CHART_OPTION_SNAPSHOT"] = "Summary";
  this.Values["EN"]["NLS_CHART_OPTION_STATS"] = "Stats";
  this.Values["EN"]["NLS_CHART_OPTION_OCUPATION"] = "Occupancy";
  this.Values["EN"]["NLS_CHART_OPTION_CASH"] = "Cash";
  this.Values["EN"]["NLS_CHART_OPTION_SEGMENTATION"] = "Segmentation";
  //Charts Options-Items
  this.Values["EN"]["NLS_CHART_OPTION_STATS_PLAYED"] = "Coin in";
  this.Values["EN"]["NLS_CHART_OPTION_STATS_NWTOTAL"] = "Total NW";
  this.Values["EN"]["NLS_CHART_OPTION_STATS_NWCASHABLE"] = "Payable NW";
  this.Values["EN"]["NLS_CHART_OPTION_STATS_TOTAL"] = "Total";
  this.Values["EN"]["NLS_CHART_OPTION_STATS_AVERAGE"] = "Average";
  this.Values["EN"]["NLS_CHART_OPTION_OCUPATION_PROV"] = "Vendor";
  this.Values["EN"]["NLS_CHART_OPTION_OCUPATION_24H"] = "24h";
  this.Values["EN"]["NLS_CHART_OPTION_CASH_INPUTS"] = "Inputs";
  this.Values["EN"]["NLS_CHART_OPTION_CASH_RESULT"] = "Result";
  this.Values["EN"]["NLS_CHART_OPTION_SEGMENT_LP"] = "Loyalty";
  this.Values["EN"]["NLS_CHART_OPTION_SEGMENT_AGE"] = "Age";
  this.Values["EN"]["NLS_CHART_OPTION_SEGMENT_NW"] = "Theoretical NW";
  this.Values["EN"]["NLS_CHART_OPTION_SEGMENT_VISITS"] = "Visits";
  this.Values["EN"]["NLS_CHART_OPTION_SEGMENT_CLIENTS"] = "Players";

  this.Values["EN"]["NLS_SESSION"] = "Session";
  this.Values["EN"]["NLS_LINK_TERMINAL"] = "Terminal";
  this.Values["EN"]["NLS_ACTUAL_SESSION"] = "Current Session";
  this.Values["EN"]["NLS_CASH_IN"] = "Cash In";
  this.Values["EN"]["NLS_CASH_OUT"] = "Cash Out";

  this.Values["EN"]["NLS_UNDER_AGE"] = "Under %0";
  this.Values["EN"]["NLS_OVER_AGE"] = "%0 or more";

  this.Values["EN"]["NLS_ZOOMABLE"] = "Zoomable report, select a region to zoom in.";
  this.Values["EN"]["NLS_RESIZABLE"] = "Resizable report, click to enlarge/shrink.";

  this.Values["EN"]["NLS_LVL_0"] = "Anónimo";
  this.Values["EN"]["NLS_LVL_1"] = "BRONZE";
  this.Values["EN"]["NLS_LVL_2"] = "GOLD";
  this.Values["EN"]["NLS_LVL_3"] = "PLATINUM";
  this.Values["EN"]["NLS_LVL_4"] = "VIP";
  this.Values["EN"]["NLS_LVL_5"] = "NIVEL 5";

  this.Values["EN"]["NLS_REPORT_STATS_TOP_10"] = "Best 10 terminals.";
  this.Values["EN"]["NLS_REPORT_STATS_BOTTOM_10"] = "Worst 10 terminals.";
  this.Values["EN"]["NLS_REPORT_STATS_PROVIDERS"] = "Total by provider last 10 days.";
  this.Values["EN"]["NLS_REPORT_STATS_ACCUM"] = "Total accumulated las 10 days.";
  this.Values["EN"]["NLS_REPORT_STATS_PERIOD"] = "Providers accumulated in selected day.";
  this.Values["EN"]["NLS_REPORT_OCCUP_GAUGE"] = "Real time occupation %.";
  this.Values["EN"]["NLS_REPORT_OCCUP_OCC"] = "Registry type occupation.";
  this.Values["EN"]["NLS_REPORT_OCCUP_LVL"] = "Levels occupation.";
  this.Values["EN"]["NLS_REPORT_OCCUP_AGE"] = "Age occupation.";
  this.Values["EN"]["NLS_REPORT_OCCUP_GEN"] = "Gender occupation.";
  this.Values["EN"]["NLS_REPORT_OCCUP_PROV7"] = "Provider occupation last 7 days.";
  this.Values["EN"]["NLS_REPORT_OCCUP_PROV30"] = "Provider occupation last 30 days.";
  this.Values["EN"]["NLS_REPORT_CASH_RESULT"] = "Cash result.";
  this.Values["EN"]["NLS_REPORT_CASH_UNBALANCED"] = "Unbalanced cash sessions of the day.";
  this.Values["EN"]["NLS_REPORT_CASH_CASHIERS"] = "Cashiers results of the day.";
  this.Values["EN"]["NLS_REPORT_SEGM_LVL"] = "By Level segmentation this year.";
  this.Values["EN"]["NLS_REPORT_SEGM_AGE"] = "By Age segmentation this year.";
  this.Values["EN"]["NLS_REPORT_SEGM_ADT"] = "By Loyalty program segmentation this year.";

  this.Values["EN"]["NLS_VIP"] = "VIP";
  this.Values["EN"]["NLS_LOAD"] = "Load";
  this.Values["EN"]["NLS_PROPERTIES"] = "Properties";
  this.Values["EN"]["NLS_ROLE_1"] = "Player's Club Attendant";
  this.Values["EN"]["NLS_ROLE_2"] = "Operations Manager";
  this.Values["EN"]["NLS_ROLE_4"] = "Slot Attendant";
  this.Values["EN"]["NLS_ROLE_8"] = "Technical Support";
  this.Values["EN"]["NLS_MONITOR"] = "My Filters";

  this.Values["EN"]["NLS_FREE"] = "Idle";
  this.Values["EN"]["NLS_REGISTERED"] = "Carded";

  this.Values["EN"]["NLS_DERIVATE"] = "Assign";
  this.Values["EN"]["NLS_ACTIVATE"] = "Start";
  this.Values["EN"]["NLS_OBSERVATIONS"] = "COMMENTS";
  this.Values["EN"]["NLS_EVENTS_HISTORY"] = "EVENTS HISTORY";
  this.Values["EN"]["NLS_FILTER_ALARMS"] = "Apply";
  this.Values["EN"]["NLS_VIEW_ALL"] = "View all";
  this.Values["EN"]["NLS_TYPE"] = "Type";

  this.Values["EN"]["NLS_SCALED"] = "Rejected";
  this.Values["EN"]["NLS_LOCATE_ALARM"] = "Locate alarm";
  this.Values["EN"]["NLS_FILTER_TASKS"] = "Apply";
  this.Values["EN"]["NLS_PRIORITY"] = "Priority";
  this.Values["EN"]["NLS_PRIORITY_HIGH"] = "High";
  this.Values["EN"]["NLS_PRIORITY_MEDIUM"] = "Medium";
  this.Values["EN"]["NLS_PRIORITY_LOW"] = "Low";
  this.Values["EN"]["NLS_STATE"] = "Status";
  this.Values["EN"]["NLS_STATE_PENDING"] = "Pending";
  this.Values["EN"]["NLS_STATE_IN_PROGRESS"] = "In progress";
  this.Values["EN"]["NLS_STATE_SOLVED"] = "Solved";
  this.Values["EN"]["NLS_STATE_VALIDATED"] = "Validated";
  this.Values["EN"]["NLS_STATE_SCALED"] = "Scaled";
  this.Values["EN"]["NLS_SEARCH"] = "Search";
  this.Values["EN"]["NLS_PRIORITY_HIGH_PRIORITY"] = "High priority";
  this.Values["EN"]["NLS_PRIORITY_MEDIUM_PRIORITY"] = "Medium priority";
  this.Values["EN"]["NLS_PRIORITY_LOW_PRIORITY"] = "Low priority";
  this.Values["EN"]["NLS_HIDE"] = "Hide";
  this.Values["EN"]["NLS_COLUMN_ATTATCH"] = "";
  this.Values["EN"]["NLS_COLUMN_DESCRIPTION"] = "Description";
  this.Values["EN"]["NLS_COLUMN_CREATED"] = "Created";
  this.Values["EN"]["NLS_COLUMN_LENGTH"] = "Elapsed";
  this.Values["EN"]["NLS_COLUMN_TERMINAL"] = "Machine";
  this.Values["EN"]["NLS_COLUMN_LOCATE"] = "Locate";
  this.Values["EN"]["NLS_COLUMN_ACTION"] = "Action";
  this.Values["EN"]["NLS_COLUMN_ASSIGNED"] = "Assigned"; // Owner
  this.Values["EN"]["NLS_COLUMN_UPDATED"] = "Updated";
  this.Values["EN"]["NLS_COLUMN_STATE"] = "Status";
  this.Values["EN"]["NLS_MODIFY_STATE"] = "Change status";
  this.Values["EN"]["NLS_STATE_SCALED"] = "Rejected";
  this.Values["EN"]["NLS_ROLE_PCA"] = "Player's Club Attendant";
  this.Values["EN"]["NLS_ROLE_OPM"] = "Operations Manager";
  this.Values["EN"]["NLS_ROLE_SLA"] = "Slot Attendant";
  this.Values["EN"]["NLS_ROLE_TSP"] = "Technical Support";

  this.Values["EN"]["NLS_LANGUAGES"] = "Language";
  this.Values["EN"]["NLS_FLOOR_PLANE"] = "Floor map";
  this.Values["EN"]["NLS_FLOOR"] = "Floor";
  this.Values["EN"]["NLS_TIME_VIEW"] = "Time view";
  this.Values["EN"]["NLS_SHOW_BACKGROUND"] = "Background";
  this.Values["EN"]["NLS_ROLE"] = "Role";
  this.Values["EN"]["NLS_USER"] = "User";
  this.Values["EN"]["NLS_SHOW_GRID"] = "Grid";
  this.Values["EN"]["NLS_SHOW_ALARMS"] = "Alarms";
  this.Values["EN"]["NLS_LAYOUT"] = "Layout";
  this.Values["EN"]["NLS_VIEW_PERSPECTIVE"] = "Perspective";
  this.Values["EN"]["NLS_VIEW_CENITAL"] = "Cenit";
  this.Values["EN"]["NLS_VIEW_FIRSTPERSON"] = "First Person";
  this.Values["EN"]["NLS_REAL_TIME_STATE"] = "Real-time status";

  this.Values["EN"]["NLS_COININ"] = "Coin in";
  this.Values["EN"]["NLS_PLAYS"] = "Plays";
  this.Values["EN"]["NLS_COMPARE_TERMINAL"] = "Add to compare";
  this.Values["EN"]["NLS_RETIRE_COMPARE_TERMINAL"] = "Retire from compare";
  this.Values["EN"]["NLS_CURRENT_SESSION"] = "Current session";
  this.Values["EN"]["NLS_NUMBER_OF_PLAYS"] = "No. of plays";
  this.Values["EN"]["NLS_COMPARE_PLAYER"] = "Add to compare";
  this.Values["EN"]["NLS_RETIRE_COMPARE_PLAYER"] = "Retire from compare";
  this.Values["EN"]["NLS_DERIVATE_TO"] = "Assign to";

  this.Values["EN"]["NLS_ROLES"] = "Roles";
  this.Values["EN"]["NLS_SEARCH_USER"] = "Search User";
  this.Values["EN"]["NLS_SEVERITY"] = "Severity";
  this.Values["EN"]["NLS_SEVERITY_HIGH"] = "High";
  this.Values["EN"]["NLS_SEVERITY_MEDIUM"] = "Medium";
  this.Values["EN"]["NLS_SEVERITY_LOW"] = "Low";
  this.Values["EN"]["NLS_COMMENT"] = "Comments";
  this.Values["EN"]["NLS_TASK_INFO"] = "Task information";
  this.Values["EN"]["NLS_MODIFY_TASK_STATE"] = "Change task status";
  this.Values["EN"]["NLS_ALARM_COMENT"] = "Alarm comments";
  this.Values["EN"]["NLS_MODIFY"] = "APPLY";
  this.Values["EN"]["NLS_CHAT_WITH"] = "Chat with ";
  this.Values["EN"]["NLS_CHAT_CONTACT"] = "Contacts";
  this.Values["EN"]["NLS_SEND"] = "Send";

  this.Values["EN"]["NLS_CREATE_FILTER"] = "Create filter";
  this.Values["EN"]["NLS_FILTER_NAME"] = "Name";
  this.Values["EN"]["NLS_PLAY_SOUND"] = "Play sound";
  this.Values["EN"]["NLS_ENABLED"] = "Enabled";

  this.Values["EN"]["NLS_FILTER_ITEMS"] = "Items";
  this.Values["EN"]["NLS_COLOR"] = "Color";
  this.Values["EN"]["NLS_FILTER"] = "Filter";
  this.Values["EN"]["NLS_SAVE"] = "Save"; // Review order of NLS in controls
  this.Values["EN"]["NLS_OK"] = "Save";
  this.Values["EN"]["NLS_CANCEL"] = "Cancel";

  this.Values["EN"]["NLS_ERROR"] = "Error";
  this.Values["EN"]["NLS_SELECT_A_FILTER"] = "You must select a filter.";
  this.Values["EN"]["NLS_CUSTOM_FILTER"] = "CUSTOM FILTER";
  this.Values["EN"]["NLS_FILTER_EDIT"] = "Edit filter";

  this.Values["EN"]["NLS_WARNING"] = "Warning";
  this.Values["EN"]["NLS_SURE_TO_EXIT_FILTER_CREATION_MODE"] = " Are you sure to exit filter creation/edition mode?";

  this.Values["EN"]["NLS_CLOSE_LIST"] = "Close list";
  this.Values["EN"]["NLS_DELETE_CURRENT_RECORD"] = "Are you sure to delete the current record?";
  this.Values["EN"]["NLS_COMPARE_PLAYERS"] = "Compare players";
  this.Values["EN"]["NLS_LOCATE"] = "Locate";
  this.Values["EN"]["NLS_COMPARE"] = "Compare";

  this.Values["EN"]["NLS_COMPARE_TERMINALS"] = "Compare machines";
  this.Values["EN"]["NLS_INFO"] = "Info";
  this.Values["EN"]["NLS_LOCATE_TERMINAL"] = "Locate machine";

  this.Values["EN"]["NLS_ALARM_SOUND"] = "Alarm sound";
  this.Values["EN"]["NLS_EDIT_FILTERS"] = "Edit filters";
  this.Values["EN"]["NLS_CLEAN"] = "Clear";

  this.Values["EN"]["NLS_VIEW_2D"] = "2D";
  this.Values["EN"]["NLS_VIEW_3D"] = "3D";

  this.Values["EN"]["NLS_CATEGORY"] = "Category";
  this.Values["EN"]["NLS_SUBCATEGORY"] = "Subcategory";

  this.Values["EN"]["NLS_ASSIGNED_TO"] = "Assigned to";

  this.Values["EN"]["NLS_TASKS_ERROR_ASSIGN"] = "The task has not been assigned";
  this.Values["EN"]["NLS_TASKS_ERROR_MODIFY"] = "The task has not been modified";
  this.Values["EN"]["NLS_TASKS_ERROR_MODIFY_STATUS"] = "You should modify the task status"

  this.Values["EN"]["NLS_PLAYER_WORKING_SESION"] = "Gaming Day";
  this.Values["EN"]["NLS_PLAYER_CURRENT_SESION"] = "Current";
  this.Values["EN"]["NLS_PLAYER_LAST_VISITS"] = "Last visits";
  this.Values["EN"]["NLS_DAY1"] = "Today";
  this.Values["EN"]["NLS_DAY2"] = "Yesterday";
  this.Values["EN"]["NLS_DAY3"] = "Before Yesterday";

  this.Values["EN"]["NLS_STATUS_CHANGED"] = "Status changed to ";
  this.Values["EN"]["NLS_STATUS_CHANGED_REASON"] = "Reason";
  this.Values["EN"]["NLS_N_HOURS"] = "Over 48 hours";
  this.Values["EN"]["NLS_ERR_SEVERITY"] = "*You must select a severity.";
  this.Values["EN"]["NLS_ERR_USER_ROLE_ASSIGNED"] = "*You must select a role or user to assign.";
  this.Values["EN"]["NLS_PLAYER"] = "Player";
  this.Values["EN"]["NLS_MACHINE"] = "Machine";
  this.Values["EN"]["NLS_CATEGORY"] = "Category";
  this.Values["EN"]["NLS_SUBCATEGORY"] = "Subcategory";
  this.Values["EN"]["NLS_SEARCH_TERMINAL"] = "Seach Terminal";
  this.Values["EN"]["NLS_WIDGET_MACHINE_OCC_ACTIVE"] = "Available";
  this.Values["EN"]["NLS_WIDGET_MACHINE_OCC_IN_SESSION"] = "In session";

  this.Values["EN"]["NLS_WIDGET_OCC_YEAR"] = "Year";
  this.Values["EN"]["NLS_WIDGET_OCC_MONTH"] = "Month";
  this.Values["EN"]["NLS_WIDGET_OCC_WEEK"] = "Week";
  this.Values["EN"]["NLS_WIDGET_OCC_TODAY"] = "Today";

  this.Values["EN"]["NLS_WIDGET_OCC_LAST_YEAR"] = "Last Year";
  this.Values["EN"]["NLS_WIDGET_OCC_LAST_MONTH"] = "Last Month";
  this.Values["EN"]["NLS_WIDGET_OCC_LAST_WEEK"] = "Last Week";
  this.Values["EN"]["NLS_WIDGET_OCC_YESTERDAY"] = "Yesterday";
  this.Values["EN"]["NLS_WIDGET_OCC_CURRENT"] = "Current";

  this.Values["EN"]["NLS_TERMINAL_CURRENTPLAY"] = "Current Game";
  this.Values["EN"]["NLS_TERMINAL_PAYOUTPLAY"] = "Current Payout";
  this.Values["EN"]["NLS_TERMINAL_SESSION"] = "Session";
  this.Values["EN"]["NLS_TERMINAL_LINKPLAYER"] = "Link Player";

  this.Values["EN"]["NLS_SURE_TO_DELETE_CUSTOM_FILTER"] = "Are you sure to delete the custom filter?";
  this.Values["EN"]["NLS_POP_YESNO_YES"] = "Yes";
  this.Values["EN"]["NLS_POP_YESNO_NO"] = "No";
  this.Values["EN"]["NLS_NO_NAME"] = "Enter a Name";
  this.Values["EN"]["NLS_ERROR_FILTER_DISPLAY"] = "You must select a filter to display";

  this.Values["EN"]["NLS_MY_TASKS"] = "Tasks";
  this.Values["EN"]["NLS_DASHBOARD"] = "Dashboard";
  this.Values["EN"]["NLS_DASH_STATS"] = "Statistics";
  this.Values["EN"]["NLS_DASH_FEAT"] = "Featured";
  this.Values["EN"]["NLS_FLOOR"] = "Floor";
  this.Values["EN"]["NLS_TERMINALS"] = "Machines";
  this.Values["EN"]["NLS_TERMINALS_SEARCH_PLACEHOLDER"] = "Search machines";
  this.Values["EN"]["NLS_PLAYERS"] = "Players";
  this.Values["EN"]["NLS_PLAYERS_SEARCH_PLACEHOLDER"] = "Search players";
  this.Values["EN"]["NLS_ALARMS"] = "Alarms";
  this.Values["EN"]["NLS_CHARTS"] = "Charts";
  this.Values["EN"]["NLS_FLOOR_COMPARE_TERMS"] = "Compare";
  this.Values["EN"]["NLS_FLOOR_TEMP_MAP"] = "Heatmap";
  this.Values["EN"]["NLS_FLOOR_VISUAL"] = "Visual";
  this.Values["EN"]["NLS_PLAYERS_SEARCH"] = "Search";
  this.Values["EN"]["NLS_PLAYERS_FILTERS"] = "Filters";
  this.Values["EN"]["NLS_PLAYERS_LIST"] = "List mode";
  this.Values["EN"]["NLS_NO_TASKS_ASSIGNED"] = "No task assigned."
  this.Values["EN"]["NLS_WIDGET_OCC_YEAR"] = "Year";
  this.Values["EN"]["NLS_WIDGET_OCC_MONTH"] = "Month";
  this.Values["EN"]["NLS_WIDGET_OCC_WEEK"] = "Week";
  this.Values["EN"]["NLS_WIDGET_OCC_TODAY"] = "Today";
  this.Values["EN"]["NLS_FLOOR_FILTERS"] = "Filters"

  this.Values["EN"]["NLS_ALARMS_ALL"] = "All";
  this.Values["EN"]["NLS_ALARMS_PRIORITY_HIGH"] = "High Priority";
  this.Values["EN"]["NLS_ALARMS_PRIORITY_MEDIUM"] = "Medium Priority";
  this.Values["EN"]["NLS_ALARMS_PRIORITY_LOW"] = "Low Priority";

  this.Values["EN"]["NLS_TERMINAL_CURRENTPLAY"] = "Current Game";
  this.Values["EN"]["NLS_TERMINAL_PAYOUTPLAY"] = "Current Payout";
  this.Values["EN"]["NLS_TERMINAL_SESSION"] = "Session";
  this.Values["EN"]["NLS_TERMINAL_LINKPLAYER"] = "Link Player";
  this.Values["EN"]["NLS_MESSAGING_ERROR"] = "We're experiencing difficulties to send this message. Please contact to technical support.";

  this.Values["EN"]["NLS_WIDGET_TITLE_SEGMENT_AGE"] = "OCCUPANCY BY AGE";
  this.Values["EN"]["NLS_WIDGET_TITLE_SEGMENT_LEVEL"] = "Occupancy by tier";
  this.Values["EN"]["NLS_WIDGET_TITLE_RUNNERS"] = "FLOOR ATTENDANT";
  this.Values["EN"]["NLS_WIDGET_TITLE_OCCUPANCY"] = "OCCUPANCY";

  this.Values["EN"]["NLS_WIDGET_MACHINE_OCC_TITLE"] = "Slot Occupancy";
  this.Values["EN"]["NLS_WIDGET_TOP10_TITLE"] = "TOP 10 [COIN IN]";
  this.Values["EN"]["NLS_WIDGET_TOPWINNINGPLAYER_TITLE"] = "HOT PLAYERS";
  this.Values["EN"]["NLS_WIDGET_TOPLOSINGPLAYER_TITLE"] = "TOP LOSERS";
  this.Values["EN"]["NLS_WIDGET_VIPS_TITLE"] = "VIPS";
  this.Values["EN"]["NLS_WIDGET_HIGHROLLERS_TITLE"] = "High Rollers";
  this.Values["EN"]["NLS_WIDGET_COININ_TITLE"] = "COIN IN";
  this.Values["EN"]["NLS_WIDGET_AVERAGEBET_TITLE"] = "AVERAGE BET";
  this.Values["EN"]["NLS_WIDGET_TASKS_TITLE"] = "TASKS";
  this.Values["EN"]["NLS_WIDGET_TASKS_TECHNICAL"] = "TECHNICAL";
  this.Values["EN"]["NLS_WIDGET_TASKS_PLAYERS"] = "PLAYERS";
  this.Values["EN"]["NLS_WIDGET_TASKS_CUSTOM"] = "CUSTOM";
  this.Values["EN"]["NLS_WIDGET_TASKS_PENDING"] = "IN PROGRESS";
  this.Values["EN"]["NLS_WIDGET_TASKS_ASSIGNED"] = "ASSIGNED";
  this.Values["EN"]["NLS_WIDGET_TASKS_SOLVED"] = "SOLVED";


  this.Values["EN"]["NLS_WIDGET_OCCVSWEATHER_TITLE"] = "OCCUPANCY vs WEATHER";
  this.Values["EN"]["NLS_WIDGET_GENDER_TITLE"] = "OCCUPANCY BY GENDER";
  this.Values["EN"]["NLS_WIDGET_GENDER_MALE"] = "Male";
  this.Values["EN"]["NLS_WIDGET_GENDER_FEMALE"] = "Female";

  this.Values["EN"]["NLS_WIDGET_UNKNOWN"] = "Unknown";
  this.Values["EN"]["NLS_BRONZE"] = "Win";
  this.Values["EN"]["NLS_SILVER"] = "Platinum";
  this.Values["EN"]["NLS_GOLD"] = "Gold";
  this.Values["EN"]["NLS_DIAMOND"] = "Silver";

  this.Values["EN"]["NLS_WIDGET_BIRTHDAY_TITLE"] = "BirthDay";

  this.Values["EN"]["NLS_WIDINF_NO_INFO"] = "<<P><I>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus mattis, nulla quis interdum ultrices, lorem dolor laoreet felis, et semper dolor ex id ligula. Phasellus ac nulla mollis, elementum eros placerat, laoreet justo. Praesent ac placerat ipsum, vel cursus mi. Mauris pulvinar ornare sem sed pharetra. Praesent faucibus, diam ac egestas congue, ante magna vestibulum velit, placerat posuere enim felis id sapien. Curabitur risus risus, consequat at elit sit amet, auctor tincidunt urna. Integer pharetra bibendum massa non malesuada.</I></P>";
  this.Values["EN"]["NLS_WIDINF_SITE_AVERAGE_BET"] = "<<P><I>Real time evolution of the <B>Site</B> <B>average bet</B>.</I></P>";
  this.Values["EN"]["NLS_WIDINF_SEGMENT_AGE"] = "<<P><I>Percentage of <B>machines in play session</B> by <B>player's</B> age.</I></P>";
  this.Values["EN"]["NLS_WIDINF_SEGMENT_LEVEL"] = "<<P><I>Percentage of <B>machines in play session</B> by <B>player's </B> Loyalty Program tier.</I></P>";

  this.Values["EN"]["NLS_ALARMS_LASTS"] = "Lasts";
  this.Values["EN"]["NLS_ALARMS_ACTIVE"] = "Active";

  this.Values["EN"]["NLS_AUTO_NOTIFICATION_TASK_CREATED"] = "Task Created";
  this.Values["EN"]["NLS_HISTORY_COMMENTS"] = "Comment:";

  this.Values["EN"]["NLS_WIDGET_PLAYERS_MAX_LVL_TITLE"] = "TOP LEVEL PLAYERS BY TIER";
  this.Values["EN"]["NLS_WIDGET_PLAYERS_MAX_LVL_COIN_IN"] = "C.I.:";
  this.Values["EN"]["NLS_WIDGET_PLAYERS_MAX_LVL_BET_AVG"] = "B.A.:";

  this.Values["EN"]["NLS_WIDGET_HIGHROLLER_COIN_IN"] = "CoinIn:";
  this.Values["EN"]["NLS_WIDGET_HIGHROLLER_BET_AVG"] = "Bet Avg:";
  this.Values["EN"]["NLS_WIDGET_GENERAL"] = "General";
  this.Values["EN"]["NLS_SESSION_ACTIVE"] = "Active";
  this.Values["EN"]["NLS_SESSION_INACTIVE"] = "Inactive";
  this.Values["EN"]["NLS_RUNNER_CURRENT"] = "Tasks in progress";
  this.Values["EN"]["NLS_RUNNER_LAST"] = "Lasts tasks";

  this.Values["EN"]["NLS_RUNNER_INFO_ASSIGNED"] = "Assigned:";
  this.Values["EN"]["NLS_RUNNER_INFO_INPROGRESS"] = "Accepted:";
  this.Values["EN"]["NLS_RUNNER_INFO_SOLVED"] = "Solved:";
  this.Values["EN"]["NLS_RUNNER_INFO_SCALED"] = "Scaled:";

  this.Values["EN"]["NLS_DOW_SUNDAY"] = "SUN";
  this.Values["EN"]["NLS_DOW_MONDAY"] = "MON";
  this.Values["EN"]["NLS_DOW_TUESDAY"] = "TUE";
  this.Values["EN"]["NLS_DOW_WEDNESDAY"] = "WED";
  this.Values["EN"]["NLS_DOW_THURSDAY"] = "THU";
  this.Values["EN"]["NLS_DOW_FRIDAY"] = "FRI";
  this.Values["EN"]["NLS_DOW_SATURDAY"] = "SAT";

  // DMT 26/05/2016
  this.Values["EN"]["NLS_WIDINF_AVERAGE_BET_INFO"] = "<<P><I>Average bet over the current hour, compared to the average bet same day last week (at the same hour),the average for the same day of the week over last month (at the same hour) and the average for the same day of the week over last year (at the same hour).</I></P>";
  this.Values["EN"]["NLS_WIDGET_HIGHROLLERS_INFO"] = "<<P><I>List of active players with a high average bet or coin in amount over a period of time. You can configure these thresholds in WigosGUI. Tap the location button to show the player on the floor map.</I></P>";
  this.Values["EN"]["NLS_WIDGET_PLAYERS_MAX_LVL_INFO"] = "<<P><I>List of active top tier Loyalty Program players. Tap the location button to show the player on the floor map.</I></P>";
  this.Values["EN"]["NLS_WIDGET_TOPWINNINGPLAYER_INFO"] = "<<P><I>List of active players accumulating the highest winnings over the current play session. Tap the location button to show the player on the floor map.</I></P>";
  this.Values["EN"]["NLS_WIDGET_TOPLOSINGPLAYER_INFO"] = "<<P><I>List of active players accumulating the highest losses for the working day. Tap the location button to show the player on the top map.</I></P>";
  this.Values["EN"]["NLS_WIDGET_VIPS_INFO"] = "<<P><I>List of active VIP players. Tap the location button to show the player on the floor map.</I></P>";
  this.Values["EN"]["NLS_WIDGET_BIRTHDAY_INFO"] = "<<P><I>List of active players whose birthday is today. Tap the location button to show the player on the floor map.</I></P>";
  this.Values["EN"]["NLS_WIDGET_MACHINE_OCC_INFO"] = "<<P><I>Percentage of active slots that are currently in play session.</I></P>";
  this.Values["EN"]["NLS_WIDGET_GENDER_INFO"] = "<<P><I>Percentage of machines in play session by player's gender.</I></P>";
  this.Values["EN"]["NLS_WIDGET_COININ_INFO"] = "<<P><I>Accumulated coin in amount over the current hour, compared to the accumulated coin in amount same day last week (at the same hour), the accumulated for the same day of the week over last month (at the same hour) and the accumulated for the same day of the week over last year (at the same hour).</I></P>";
  this.Values["EN"]["NLS_WIDGET_TITLE_OCCUPANCY_INFO"] = "<<P><I>Current actual site occupancy, compared to the average actual site occupancy same day last week (at the same hour), the average for the same day of the week over last month (at the same hour) and the average for the same day of the week over last year (at the same hour).</I></P>";
  this.Values["EN"]["NLS_WIDGET_NETWIN_INFO"] = "<<P><I>Accumulated netwin and coin in amount over the current day, compared to yesterday.</I></P>";
  this.Values["EN"]["NLS_WIDGET_OCCVSWEATHER_INFO"] = "<<P><I>Average actual site occupancy over the last 7 days, compared to weather conditions.</I></P>";
  this.Values["EN"]["NLS_WIDGET_TASKS_INFO"] = "<<P><I>List of active tasks over the current day, grouped by type and status.</I></P>";
  this.Values["EN"]["NLS_WIDGET_RUNNERS_INFO"] = "<<P><I>List of floor attendants currently logged into Intellia App and their roles. Tap the location button to show the player on the floor map.</I></P>";

  this.Values["EN"]["NLS_LOADING_FLOOR"] = "Loading...";
  this.Values["EN"]["NLS_LOADING_FLOOR_TITLE"] = "Floor";

  this.Values["EN"]["NLS_ALARMS_SHOW_RUNNERS"] = "Show Floor Attendants";

  //ETP 08/07/2016
  this.Values["EN"]["NLS_LOGOUT"] = "Logout";
  this.Values["EN"]["NLS_NO_CHATS"] = "No chats here… Please select a contact to see or start a chat";
  this.Values["EN"]["NLS_PLEASE_WAIT"] = "Please wait...";
  this.Values["EN"]["NLS_LOADING"] = "Loading...";
  this.Values["EN"]["NLS_RESOURCE_LIST"] = "Resource List";
  this.Values["EN"]["NLS_TO_HOUR"] = "to";

  this.Values["EN"]["NLS_WIDGET_SITE_OCC_TITLE"] = "SITE OCCUPANCY";
  this.Values["EN"]["NLS_WIDGET_SITE_OCC_INFO"] = "<<P><I>Current occupancy of the site.</I></P>";
  this.Values["EN"]["NLS_WIDGET_MACHINE_OCC_SITE_OCC_TITLE"] = "SLOT VS SITE OCCUPANCY";
  this.Values["EN"]["NLS_WIDGET_MACHINE_OCC_SITE_OCC_INFO"] = "<<P><I>Active slots that are currently in play session, compared with the current occupation of the site.</I></P>";
  this.Values["EN"]["NLS_WIDGET_MACHINE_OCC_SITE_OCC_FREE_OCC"] = "Not playing";
  this.Values["EN"]["NLS_WIDGET_SITE_OCCUPATION_TOTAL"] = "Capacity";
  this.Values["EN"]["NLS_WIDGET_SITE_OCCUPATION_CURRENT"] = "In site";

  //AVZ 21/12/2016
  this.Values["EN"]["NLS_TABLE_NO_DATA_AVAILABLE"] = "No data available!";
  this.Values["EN"]["NLS_TABLE_PAGE_SIZE"] = "Row count";
  this.Values["EN"]["NLS_TABLE_GO_TO_PAGE"] = "Go to page";
  this.Values["EN"]["NLS_RUNNERS_IN_THE_ZONE"] = "FLOOR ATTENDANTS IN THE ZONE";

  this.Values["EN"]["NLS_BET_AVERAGE"] = "Bet Average";
  this.Values["EN"]["NLS_DOOR"] = "Door";
  this.Values["EN"]["NLS_BILL_ACEPTOR"] = "Bill Aceptor";
  this.Values["EN"]["NLS_PRINTER"] = "Printer";
  this.Values["EN"]["NLS_PLAYED_WON"] = "Played/Won";
  this.Values["EN"]["NLS_SUPPORT"] = "Support";
  this.Values["EN"]["NLS_JACKPOT"] = "Jackpot";
  this.Values["EN"]["NLS_VENDOR"] = "Vendor";
  this.Values["EN"]["NLS_DENOMINATION"] = "Denomination";
  this.Values["EN"]["NLS_VIPS"] = "VIPS";
  this.Values["EN"]["NLS_CALL_ATTENDANT"] = "Call Attendant";
  this.Values["EN"]["NLS_HIGH_ROLLER"] = "High Roller";
  this.Values["EN"]["NLS_OCCUPANCY"] = "Occupancy";
  this.Values["EN"]["NLS_CUSTOM_CUSTOM"] = "Custom (Others)";
  this.Values["EN"]["NLS_CUSTOM_PLAYER"] = "Custom (Player)";
  this.Values["EN"]["NLS_CUSTOM_MACHINE"] = "Custom (Machine)";
  this.Values["EN"]["NLS_PAYOUT"] = "Payout";
  this.Values["EN"]["NLS_WITHOUT_PLAYS"] = "Without plays";
  this.Values["EN"]["NLS_BILL_JAM"] = "Bill jam";
  this.Values["EN"]["NLS_COUNTERFEITS"] = "Counterfeits";
  this.Values["EN"]["NLS_STACKER_ALMOST_FULL"] = "Stacker almost full";
  this.Values["EN"]["NLS_STACKER_FULL"] = "Stacker full";
  this.Values["EN"]["NLS_COM_ERROR"] = "COM Error";
  this.Values["EN"]["NLS_OUTPUT_ERROR"] = "Output Error";
  this.Values["EN"]["NLS_RIBBON_REPLACEMENT"] = "Ribbon replacement";
  this.Values["EN"]["NLS_LOW_PAPER"] = "Low paper";
  this.Values["EN"]["NLS_CARRIAGE_JAM"] = "Carriage jam";
  this.Values["EN"]["NLS_POWER_SUPPLY"] = "Power supply";
  this.Values["EN"]["NLS_SLOT_DOOR"] = "Slot door";
  this.Values["EN"]["NLS_DROP_DOOR"] = "Drop door";
  this.Values["EN"]["NLS_CARD_CAGE_DOOR"] = "Card cage door";
  this.Values["EN"]["NLS_BELLY_DOOR"] = "Belly door";
  this.Values["EN"]["NLS_CASHBOX_DOOR"] = "Cashbox door";
  this.Values["EN"]["NLS_ERROR_CMOS_RAM"] = "Error CMOS RAM";
  this.Values["EN"]["NLS_ERROR_EEPROM"] = "Error EEPROM";
  this.Values["EN"]["NLS_LOW_BATTERY"] = "Low battery";
  this.Values["EN"]["NLS_CANCELLED_CREDITS"] = "Credits cancelled";
  this.Values["EN"]["NLS_HIGH_ROLLER_ANONYMOUS"] = "High roller anonymous";
  this.Values["EN"]["NLS_NO_LICENSE"] = "There is no valid license for Intellia";
  this.Values["EN"]["NLS_RETURN"] = "Return";

  this.Values["EN"]["DOMINGO"] = "Sun";
  this.Values["EN"]["LUNES"] = "Mon";
  this.Values["EN"]["MARTES"] = "Tue";
  this.Values["EN"]["MIERCOLES"] = "Wed";
  this.Values["EN"]["JUEVES"] = "Thu";
  this.Values["EN"]["VIERNES"] = "Fri";
  this.Values["EN"]["SABADO"] = "Sat";

  this.Values["EN"]["NLS_RESET_DASHBOARD"] = "Reset Dashboard";
  this.Values["EN"]["NLS_SOUND"] = "Sound";
  this.Values["EN"]["NLS_MESSAGES"] = "Messages";
  this.Values["EN"]["NLS_FLOOR_CONFIG"] = "Floor Configuration";

  this.Values["EN"]["NLS_ID"] = "ID";

  this.Values["ES"]["NLS_LEVEL_0"] = "UNKNOWN";
  this.Values["ES"]["NLS_LEVEL_1"] = "Anonymous";
  this.Values["ES"]["NLS_LEVEL_2"] = "Bronze";
  this.Values["ES"]["NLS_LEVEL_3"] = "Silver";
  this.Values["ES"]["NLS_LEVEL_4"] = "Gold";
  this.Values["ES"]["NLS_LEVEL_4"] = "Platinum";

}

WSILanguage.prototype.get = function (NLS_ID, Params, Upcase) {
  if (Upcase == undefined) { Upcase = false; }
  var _result = this.Values[this.Languages[this.CurrentLanguage].id][NLS_ID]
  if (_result) {
    if (Params) {
      for (var _i = 0; _i < Params.length; _i++) {
        _result = _result.replace("%" + _i, Params[_i]);
      }
    }
  } else {
    _result = "";
  }
  if (Upcase) { _result = _result.toUpperCase(); }
  return _result;
}

WSILanguage.prototype.parse = function (element) {

  var _str = undefined;

  if (!element) {
    element = document.body;
  }

  var childNodes = element.childNodes;

  for (var i = 0; i < childNodes.length; i++) {

    if (childNodes[i].attributes != null) {
      var _nls = childNodes[i].getAttribute("data-nls");
    }

    if (_nls) {
      _nls = _nls.trim();

      if (_nls != "") {

        if (_nls == "NLS_RESET_DASHBOARD")
          var a = 0;

        if (this.Values[this.Languages[this.CurrentLanguage].id][_nls]) {

          _str = this.Values[this.Languages[this.CurrentLanguage].id][_nls];
    
          if ($(childNodes[i]).prop("tagName") == "INPUT") { 
                $(childNodes[i]).attr("placeholder", _str); 
          } else if ($(childNodes[i]).attr("placeholder")) {
                $(childNodes[i]).attr("placeholder", _str);
          } else if ($(childNodes[i]).attr("title")) {
                $(childNodes[i]).attr("title", _str);
          } else {

            //
            if (_str.indexOf("<<") != -1) {
              $(childNodes[i]).empty();
              _str = _str.replace("<<", "<");
              _str = _str.replace(">>", ">");
              $(childNodes[i]).append($(_str));
            } else {
              $(childNodes[i]).text(_str);
            }
          }
        }

      }

    }

    if (childNodes[i].childNodes) {
      if (childNodes[i].childNodes.length > 0) {
        this.parse(childNodes[i]);
      }
    }
  }

  return element;
}

WSILanguage.prototype.createSelector = function (id, onChange) {
  var _result = '';
  var _lng = '';

  _result += '<select id="' + id + '" style="width:150px;" onchange="' + onChange + '">';
  for (_lng in this.Languages) {
    _result += ' <option value="' + _lng + '" ';
    if (_lng == 0) {
      _result += 'selected="selected" ';
    }
    _result += 'data-image="' + this.Languages[_lng].flag + '" data-nls="' + this.Languages[_lng].nls + '"></option>';
  }
  _result += '</select>';

  return _result;
}

WSILanguage.prototype.getLanguageIdByName = function (name) {
  var _result = -1;
  var _lng = 0;
  for (_lng in this.Languages) {
    if (name == this.Languages[_lng].id) {
      _result = _lng;
      break;
    }
  }
  if (_result == -1) { _result = this.DefaultLanguage; }
  return _result;
}

WSILanguage.prototype.getLanguageFromNavigator = function () {
  var _lng = navigator.language.toUpperCase();
  for (var _idx in m_language_binds) {
    if (_lng.indexOf(_idx) == 0) {
      return m_language_binds[_idx];
    }
  }
  // ETP 07/07/2016
  return 0;
}

var dynScript, dynParent;

function createjsfile(language) {
  var fileref = document.createElement('script');
  fileref.setAttribute("type", "text/javascript");
  fileref.setAttribute("src", "../js/jtable/localization/jquery.jtable." + language + ".js");
  return fileref;
}

WSILanguage.prototype.LoadLanguage = function loadjsfile(language) {
  dynScript = createjsfile(language);
  dynParent = document.getElementsByTagName("head")[0];
  dynParent.appendChild(dynScript);
}

WSILanguage.prototype.ReloadLanguage = function replacejsfile(language) {
  var newelement = createjsfile(language);
  dynParent.replaceChild(newelement, dynScript);
  dynScript = newelement;
}


//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////

var _Language = new WSILanguage();

//////////////////////////////////////////////////////////////////////

