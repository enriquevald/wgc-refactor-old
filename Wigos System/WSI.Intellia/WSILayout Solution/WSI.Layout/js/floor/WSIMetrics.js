﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: filename
// 
//   DESCRIPTION: Description.
// 
//        AUTHOR: Author
// 
// CREATION DATE: Date
// 
//------------------------------------------------------------------------------

function MetricToUnits(Metric, Size) {
  var _size = 0;
  switch (Metric) {
    case 0: // meters
      _size = Size;
      break;
    case 1: // miles
      _size = (Size / 0.000621371192);
      break;
    case 2: // feet
      _size = (Size / 3.2808399);
      break;
  }

  return _size;
}

function UnitsToMetric(Units, Metric) {
  var _size = 0;
  switch (Metric) {
    case 0: // meters
      _size = Units;
      break;
    case 1: // miles
      _size = (Size * 0.000621371192);
      break;
    case 2: // feet
      _size = (Size * 3.2808399);
      break;
  }

  return _size;
}

function UnitsToPixels(Size) {
  return Size * 20;
}

function PixelsToUnits(Size) {
  return Math.floor(Size / 20);
}

function MetricToPixels(Metric, Size) {
  return UnitsToPixels(MetricToUnits(Metric, Size));
}

function PixelsToMetric(Size, Metric) {
  return UnitsToMetric(PixelsToUnits(Size), Metric);
}
