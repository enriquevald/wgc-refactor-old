﻿/*
  Requires:

*/

/////////////////////////////////////////////////////////////////////////////////////////////

var m_floor_metrics = ['m', 'mi', 'ft'];

// A pseudo-class to mantain Floor data
WSIFloor3 = function () {

  // Floor database Id
  this.id = '';
  // Floor Name
  this.name = '';
  // Color for the background
  this.color = null;
  // DataUri for the background image
  this.image_map = null;
  // Floor width/height/level
  this.width = 0;
  this.height = 0;
  // Metric type
  this.metric = 0;
  // Size of the layout
  this.dimensions = {
    x: 0,   // Width
    y: 0    // Height
  };

  // Current Floor Locations
  this.Locations = [];
  // Object - Location relation
  this.ObjectLocations = [];
  // Items index by binded object (type, externalid)
  //this.ByBinded = [];
  // A variable to store a temporary copy of the floor data between operations
  this.cache_object = null;

}

//
WSIFloor3.prototype.ParseLocations = function (Locations) {
  if (Locations) {
    this.Locations = [];

    for (_rl in Locations.rows) {
      var _loc = Locations.rows[_rl];
      this.Locations.push(_loc);
    }

  }
}

//
WSIFloor3.prototype.ParseObjectLocations = function (ObjectLocations) {
  if (ObjectLocations) {
    this.ObjectLocations = [];

    for (var _io in ObjectLocations.rows) {
      var _ol = ObjectLocations.rows[_io];

      this.ObjectLocations.push(_ol);

    }
  }
}

//
WSIFloor3.prototype.GetObject = function (ObjectId) {
  for (var _n in this.ObjectLocations) {
    if (this.ObjectLocations[_n][EN_OBJECT_LOCATION_FIELDS.lo_id] == ObjectId) {
      return this.ObjectLocations[_n];
    }
  }

  return undefined;
}

//
WSIFloor3.prototype.GetLocationById = function (LocationId) {
  for (_il in this.Locations) {
    var _loc = this.Locations[_il];
    if (_loc[EN_LOCATION_FIELDS.loc_id] == LocationId) {
      return _loc;
    }
  }

  return undefined;
}

WSIFloor3.prototype.UpdateLocationName = function (LocationId, Name) {
  for (_il in this.Locations) {
    var _loc = this.Locations[_il];
    if (_loc[EN_LOCATION_FIELDS.loc_id] == LocationId) {
      _loc[EN_LOCATION_FIELDS.loc_gridId] = Name;
    }
  }

  return undefined;
}

WSIFloor3.prototype.GetLocation = function (X, Y) {
  for (_il in this.Locations) {
    var _loc = this.Locations[_il];
    if (_loc[EN_LOCATION_FIELDS.loc_x] == X && _loc[EN_LOCATION_FIELDS.loc_y] == Y) {
      return _loc;
    }
  }

  return undefined;
}

WSIFloor3.prototype.GetByField = function (IdField, FieldValue, Status, elements) {
  var _that = this,
        _row_id = -1;

  for (var _i = 0; _i < _that.ObjectLocations.length; _i++) {
    if (_that.ObjectLocations[_i][IdField] == FieldValue && _that.ObjectLocations[_i][EN_OBJECT_LOCATION_FIELDS.lo_status] != Status) {
      _row_id = _i;
      break;
    }
  }
  if (elements && _row_id != -1) {
    for (var _iel in elements) {
      var _el = elements[_iel];
      if (_el.id == _that.ObjectLocations[_row_id][EN_OBJECT_LOCATION_FIELDS.svg_id])
        return undefined;

    }
  }
  return (_row_id == -1) ? undefined : _that.ObjectLocations[_row_id];
}

WSIFloor3.prototype.GetObjectLocation = function (Location) {
  for (var _iol in this.ObjectLocations) {
    var _obj_location = this.ObjectLocations[_iol];
    if (_obj_location.lol_location_id == Location.loc_id) {
      return _obj_location;
    }
  }
  return undefined;
}

WSIFloor3.prototype.UpdateObjectLocationOffset = function (ObjectLocation, Attribs, Location) {
  // Update Offset
  var _lx = UnitsToPixels(ObjectLocation.loc_x);
  var _ly = UnitsToPixels(ObjectLocation.loc_y);

  ObjectLocation.lol_offset_x = Attribs.attr.origial_x - _lx;
  ObjectLocation.lol_offset_y = Attribs.attr.origial_y - _ly;

}

WSIFloor3.prototype.CreateNewObjectLocation = function () {
  return Array.apply(null, Array(EN_OBJECT_LOCATION_FIELDS.lol_offset_y)).map(function () { return null });
}

WSIFloor3.prototype.SetObject = function (Object, Status) {
  if (Object) {

    if (Status == undefined) { Status = EN_OBJECT_STATUS.inserted; }

    Object[EN_OBJECT_LOCATION_FIELDS.lo_status] = Status;

    if (Object[EN_OBJECT_LOCATION_FIELDS.lo_id] == "") {
      // New object inserted/modified
      _CurrentChanges3.AddRow(EN_OBJECT_LOCATION_FIELDS.lo_external_id, Object[EN_OBJECT_LOCATION_FIELDS.lo_external_id], Object);

      return true;

    } else {
      // Existing object inserted/modified
      _CurrentChanges3.AddRow(EN_OBJECT_LOCATION_FIELDS.lo_id, Object[EN_OBJECT_LOCATION_FIELDS.lo_id], Object);

      return true;
    }

  }
  return false;
}

// Adds a loaded objects to the current floor
WSIFloor3.prototype.AddItem = function (Item) {

  //if (!this.ByBinded[Item.type]) {
  //  this.ByBinded[Item.type] = [];
  //}

  //var _ident = (Item.type == 0) ? Item.id : Item.externalId;

  //if (!this.ByBinded[Item.type][_ident]) {
  //  this.ByBinded[Item.type][_ident] = this.Items.length;
  //}

  return this.Items.push(Item);

}

//
WSIFloor3.prototype.Load = function (Data, Content) {
  if (Data && Content) {

    _CurrentChanges3.FloorId = this.id;
    _CurrentChanges3.rows = [];

    this.id = (Data.id) ? Data.id : '';
    this.name = (Data.name) ? Data.name : '';
    this.image_map = (Data.image_map) ? Data.image_map : '';
    this.color = (Data.color) ? Data.color : 'rgb(ff ff ff);';
    this.height = (Data.height) ? Data.height : 0;
    this.dimensions = (Data.dimensions) ? Data.dimensions : { x: 0, y: 0 };
    this.width = (Data.width) ? Data.width : 0;
    this.metric = (Data.metric) ? Data.metric : 0;

    this.dimensions.x = UnitsToPixels(this.width);
    this.dimensions.y = UnitsToPixels(this.height);

    if (Content.Locations) {
      this.ParseLocations(Content.Locations);
    }

    if (Content.Objects) {
      this.ParseObjectLocations(Content.Objects);
    }

  }
}

//
WSIFloor3.prototype.SaveFloor = function () {

  if (_CurrentChanges3.rows.length == 0) { return; }

  LayoutProgressOpen();

  _CurrentChanges3.FloorId = this.id;

  var _floor_data = JSON.stringify(_CurrentChanges3);

  var _onSaveComplete = function (result,context,name) {

    //_CurrentFloor = _saved_floor;
    OpenFloorFromId(_CurrentChanges3.FloorId);
    //_CurrentChanges3.Initialize(_CurrentChanges3.FloorId);


    // alert("Saved!");

    LayoutProgressClose();
  };

  var _onSaveError = function (result,context,name) {

    if (result._statusCode == 404) {
      OpenFloorFromId(_CurrentChanges3.FloorId);
      //_CurrentChanges3.Initialize(_CurrentChanges3.FloorId);
    }
    else alert("Error!");

    LayoutProgressClose();
  };

  PageMethods.SaveLayout(_floor_data, _onSaveComplete, _onSaveError);

}

//
WSIFloor3.prototype.UpdateSVG = function () {
  if (methodDraw) {
    var _canvas = methodDraw.canvas;
    var _object = null;
    var _attribs = null;
    var _elem = null;

    // Clear
    svgCanvas.selectAllInCurrentLayer();
    svgCanvas.deleteSelectedElements_();
    svgCanvas.clearSelection();
    svgCanvas.clear();

    // Set Size
    svgCanvas.setResolution(+this.dimensions.x, +this.dimensions.y);
    $("#canvas_width").val(+this.dimensions.x);
    $("#canvas_height").val(+this.dimensions.y);

    // Update Grid & Background
    svgCanvas.runExtensions("onOpenDocument", { docData: this });

    // Draw each item
    if (!svgCanvas.setCurrentLayer(this.name)) {
      svgCanvas.createLayer(this.name);
    }

    for (var _idx in this.ObjectLocations) {
      var _ol = this.ObjectLocations[_idx];
      var _object = this.GetObject(_ol[EN_OBJECT_LOCATION_FIELDS.lol_object_id]);
      var _loc = this.GetLocationById(_ol[EN_OBJECT_LOCATION_FIELDS.lol_location_id]);

      if (_object) {

        _attribs = WSIObjectTypes(+_object[EN_OBJECT_LOCATION_FIELDS.lo_type]);

        var _xx = UnitsToPixels(_loc[EN_LOCATION_FIELDS.loc_x]) + 10 + +_ol[EN_OBJECT_LOCATION_FIELDS.lol_offset_x].replace(',','.'),
            _yy = UnitsToPixels(_loc[EN_LOCATION_FIELDS.loc_y]) + 10 + +_ol[EN_OBJECT_LOCATION_FIELDS.lol_offset_y].replace(',', '.');
        _attribs.attr.d = WSISetPathLocation(_attribs.attr.d, { x: _xx, y: _yy });

        if (_ol[EN_OBJECT_LOCATION_FIELDS.lol_orientation] > 0) {
          _attribs = WSIObjectRotate(_attribs, _ol[EN_OBJECT_LOCATION_FIELDS.lol_orientation]);
        }

        _elem = _canvas.addSvgElementFromJson(_attribs);

        _object[EN_OBJECT_LOCATION_FIELDS.svg_id] = _attribs.attr.id;

      }

    }

    svgCanvas.updateCanvas();
  }
}

WSIFloor3.prototype.RemoveObject = function (ObjectData) {
  var _location = this.GetLocation(ObjectData.attr.location_x, ObjectData.attr.location_y);
  var _obj_location = this.GetObjectLocation(_location);
  _obj_location.lol_object_id = '';

  var _index = -1;
  for (var _i = 0; _i < this.Objects.length; _i++) {
    if (String(ObjectData.attr.current_lo_externalId) == String(this.Objects[_i].identity)) {
      _index = _i;
    }
  }

  if (_index > -1) { this.Objects.splice(_index, 1); }
}

var _CurrentFloor3 = undefined; //new WSIFloor3();

////////////////////////////////////////////////////////////////////////////////////////

function ChangesControl() {
  this.FloorId = null;
  this.columns = ['lo_id', 'lo_external_id', 'lo_type', 'lo_parent_id', 'lo_mesh_id', 'lo_creation_date', 'lo_update_date', 'lo_status', 'lol_object_id', 'lol_location_id', 'lol_date_from', 'lol_date_to', 'lol_orientation', 'lol_area_id', 'lol_bank_id', 'lol_bank_location', 'lol_current_location', 'lol_offset_x', 'lol_offset_y'];
  this.rows = [];
}

ChangesControl.prototype.GetByField = function (IdField, FieldValue, Status,elements) {
  var _that = this,
      _row_id = -1;

  if (Status == undefined) { Status = EN_OBJECT_STATUS.inserted; }

  for (var _i = 0; _i < _that.rows.length; _i++) {
    if (_that.rows[_i][IdField] == FieldValue && _that.rows[_i][EN_OBJECT_LOCATION_FIELDS.lo_status] != Status) {
      _row_id = _i; 
      break;
    }
  }

  if (elements && _row_id != -1)
  {
    for (var _iel in elements) {
      var _el = elements[_iel];
      if (_el.id == _that.rows[_row_id][EN_OBJECT_LOCATION_FIELDS.svg_id])
        return undefined;

    }
  }
  return (_row_id == -1) ? undefined : _that.rows[_row_id];



}

ChangesControl.prototype.Initialize = function (FloorId) {
  this.FloorId = FloorId;
  this.rows = [];
}

ChangesControl.prototype.AddRow = function (IdField, IdValue, Data) {
  var _that = this;

  function GetCurrentRow() {
    var _row_id = -1;
    for (var _i = 0; _i < _that.rows.length; _i++) {
      if (_that.rows[_i][IdField] == IdValue) {
        _row_id = _i;
        break;
      }
    }
    return _row_id;
  }

  var _rid = GetCurrentRow();

  if (_rid == -1) {
    _that.rows.push(Data);
  } else {
    _that.rows.splice(_rid, 1);
    _that.rows.push(Data);
  }

}

var _CurrentChanges3 = new ChangesControl();