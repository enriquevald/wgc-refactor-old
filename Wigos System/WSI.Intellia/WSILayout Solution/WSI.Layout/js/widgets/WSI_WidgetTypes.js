﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WSI_WidgetTypes.js
// 
//   DESCRIPTION: Classes for widget types.
// 
//        AUTHOR: Ramón Monclús
// 
// CREATION DATE: 18-JUN-2015
// 
//------------------------------------------------------------------------------

/*
 Requires:
    WSI_MetersManager.js
    WSIManager.js
 */

WidgetBase.prototype.constructor = WidgetBase;
// Basic widget class
function WidgetBase() {

  this.id = "";
  this.style = undefined;
  this["class"] = undefined;
  this.titleClass = "listtittle";
  this.title = "";
  this.titleNLS = "";
  this.animations = "none";

  this.draggableTemplate = "";
  this.staticTemplate = "";
  this.draggableTemplateSize = "";
  this.staticTemplateSize = "";

  this.properties = {};

  this.OnUpdate = undefined; // function ()
  this.OnAfterAdded = undefined; // function ()

  this.BindEvents = undefined;
  this.BindEventsToClass = '.item';

  this.prepared = false;

  this.showOptions = false;

  this.MetersBind = [];
  this.MeterId = [];
  this.MeterNameAsTitle = true;

  this.PrepareStyles = undefined;

  this.ShowInfoButton = true;
  this.InfoNLS = 'NLS_WIDINF_NO_INFO';
  this.InfoVisible = false;

  this.hasScroll = false;
}

// Obtains the id of the widget
WidgetBase.prototype.getId = function () {
  return this.id + "_widget";
}

// Parse the template to use in the widget and create its properties.
// In templates use the attribute "data-property-name" to generate a property.
// Templates have 2 parts, the draggable and the static code. (Static code cannot be used to drag the widget).
WidgetBase.prototype.SetTemplate = function (Template) {
  if (typeof Template === 'object') {
    this.draggableTemplate = this.ParseTemplate(Template.draggable);
    this.staticTemplate = this.ParseTemplate(Template["static"]);
  } else {
    this.draggableTemplate = Template;
  }
}

// Update Properties
WidgetBase.prototype.UpdateProperties = function () {
  var _that = this;
  var _that_object = $("#" + this.id);
  for (var _prop in this.properties) {
    _that_object.find('[data-property-name="' + _prop + '"]')
         .each(function (Index, Element) {
           var _el = $(Element);
           _el.text(FormatString(_that.properties[_prop], GetStringFormat(_el)));
         });
  }
}

// Return the template with the current properties values.
WidgetBase.prototype.Prepare = function (StandAlone) {

  var _w_title = undefined;

  if (StandAlone == undefined) { StandAlone = false; }

  var _drag_size = "10%";
  var _stat_size = "90%";

  if (this.staticTemplate == "") {
    _drag_size = "100%";
    _stat_size = "";
  }

  if (this.draggableTemplate == "") {
    _stat_size = "100%";
    _drag_size = "";
  }

  var _template = '<div id="' + this.id + '"';

  _template = _template + ' class="' + (this["class"] ? this["class"] : "") + '"';
  _template = _template + ((this.style) ? (' style="' + this.style + '"') : (''));
  _template = _template + '>';

  if (this.draggableTemplate != "") {
    if (!StandAlone) { _template = _template + '<div class="WidgetDraggablePart" style="height: ' + _drag_size + ';">'; }
    _template = _template + this.draggableTemplate;
    if (!StandAlone) { _template = _template + '</div>'; }
  }

  if (this.staticTemplate != "") {
    if (!StandAlone) {
      _template = _template + '<div class="WidgetStaticPart">';
    }
    _template = _template + this.staticTemplate;
    if (!StandAlone) {
      _template = _template + '</div>';
    }
    if (!StandAlone) {
      _template = _template + '<div class="pagination-bar"><span id="' + this.id + '-pagination-button-left" class="pagination-button-left"></span><span id="' + this.id + '-pagination-indicator" class="pagination-indicator"></span><span id="' + this.id + '-pagination-button-right" class="pagination-button-right"></span></div>';
    }
  }

  _template = _template + '</div>';

  var _object = $('<div>' + _template + '</div>');
  var _that = this;

  // NLS for the title if its not a meter name title
  if (this.titleNLS != "" && !this.MeterNameAsTitle) {
    _object.find("." + this.titleClass).append("<div id='" + this.id + "-title' class='title-wrap' data-nls='" + this.titleNLS + "'>" + this.title + "</div>");
    //_object.find("#" + this.titleClass).attr("data-nls", this.titleNLS);
  } else {
    _object.find("." + this.titleClass).append("<div id='" + this.id + "-title' class='title-wrap'>" + this.title + "</div>");
  }

  //
  if (this.showOptions) {
    _w_title = $(_object).find(".listtittle");
    if (_w_title.length == 1) {
      _w_title.append($("<div class='icometer widgetBtn' onclick='if (_Manager.WidgetManager) { _Manager.WidgetManager.GetWidget(\"" + this.id + "\").ShowMenu(); } '></div>"));
    }
  }
  //

  // Info button & panel
  if (this.ShowInfoButton) {
    _w_title = $(_object).find(".listtittle");
    if (!_w_title.find(".widgetBtn").length) {
      _w_title.append($("<div class='widgetBtn' onclick='if (_Manager.WidgetManager) { _Manager.WidgetManager.GetWidget(\"" + this.id + "\").SetInfoState(); } '><span class='ui-icon ui-icon-info'>INF</span></div>"));
    }
    $(_object).append($("<div class='widgetInfoPanel' style='display: none;' data-nls='" + this.InfoNLS + "'>" + _Language.get(this.InfoNLS).substring(1) + "</div>"));
  }
  //

  this.prepared = true;
  return _object[0].innerHTML;
}

// Obtain the value of a property
WidgetBase.prototype.GetPropertyValue = function (PropertyName) {
  return this.properties[PropertyName];
}

// Executed when widget recently added
WidgetBase.prototype.Added = function (WidgetObject, Widget) {

  // Prepare properties
  if (WidgetObject.properties) {
    WidgetObject.UpdateProperties();
  }

  // Update Meter information
  if (WidgetObject.MeterId.length > 0) {
    WidgetObject.UpdateMeterInfo();
  }

  // Custom actions
  if (WidgetObject.OnAfterAdded) {
    WidgetObject.OnAfterAdded(Widget);
  }
}

// Updates the title by the meter
WidgetBase.prototype.UpdateTitleByMeters = function () {
  var _title = '';
  var _m_id = undefined;
  for (var _m in this.MeterId) {
    _m_id = this.MeterId[_m];
    if (_Manager.MetersManager.Meters[_m_id]) {
      _title = _title + (_title == '' ? '' : ' vs ') + _Manager.MetersManager.Meters[_m_id].GetValue(0, '', "descript");
    } else {
      WSILogger.Write("Meter not defined [" + _m_id + "]");
    }
  }
  if (_title != '') {
    $("#" + this.id).find('.listtittle').text(_title);
  }

  // Info button & panel
  if (this.ShowInfoButton) {
    _w_title = $("#" + this.id).find(".listtittle");
    if (!_w_title.find(".widgetBtn").length) {
      _w_title.append($("<div class='widgetBtn' onclick='if (_Manager.WidgetManager) { _Manager.WidgetManager.GetWidget(\"" + this.id + "\").SetInfoState(); } '><span class='ui-icon ui-icon-info'>INF</span></div>"));
    }
    $("#" + this.id).append($("<div class='widgetInfoPanel' style='display: none;' data-nls='" + this.InfoNLS + "'>" + _Language.get(this.InfoNLS).substring(1) + "</div>"));
  }
  //

}

// Visually update the widget, if it has an update method
WidgetBase.prototype.Update = function () {
  if (this.prepared) {

    // Prepare properties
    if (this.properties) {
      this.UpdateProperties();
    }

    // Update Meter information
    if (this.MeterId.length > 0) {
      this.UpdateMeterInfo();

      if (this.MeterNameAsTitle) {
        this.UpdateTitleByMeters();
      }
    }

    // Call custom update method
    if (this.OnUpdate) {
      this.OnUpdate(this);
    }
  }
}

// Updates Meter information
WidgetBase.prototype.UpdateMeterInfo = function () {
  var _meter_bind = undefined;
  var _meter_id = undefined;
  var _that_object = $("#" + this.id);
  var _that = this;

  for (var _mb in this.MetersBind) {
    _meter_bind = this.MetersBind[_mb];
    _meter_id = _that.MeterId[_meter_bind.Id];
    _that_object.find('[data-meter-id=' + _meter_bind.Id + '][data-meter-item=' + _meter_bind.Item + '][data-meter-range=' + _meter_bind.Range + '][data-meter-value=' + _meter_bind.Value + ']')
                .each(function (Index, Element) {
                  var _el = $(Element);
                  var _meter_info = _Manager.MetersManager.Meters[_meter_id];
                  var _value = parseCurrency(_meter_info.GetValue(_meter_bind.Item, _meter_bind.Range, _meter_bind.Value));
                  if (isFinite(String(_value))) {
                    _value = Round(_value, 4);
                  }
                  _el.text(FormatString(_value, GetStringFormat(_el)));
                });

  }

}

// This method really parse a template.
WidgetBase.prototype.ParseTemplate = function (Template) {
  var _that = this;
  var _template = $('<div>' + Template + '</div>');
  $(_template).find('*')
    .each(function (Index, Element) {

      var _meter_id = $(Element).data("meter-id");
      var _property_name = $(Element).data("property-name");

      if (_meter_id != undefined) {
        var _meter_item = $(Element).data("meter-item");
        var _meter_value = $(Element).data("meter-value");
        var _meter_range = $(Element).data("meter-range");

        if (!_meter_item) { _meter_item = "0"; }
        if (!_meter_value) { _meter_value = "hacc"; }
        if (!_meter_range) { _meter_range = "NOW"; }

        $(Element).attr("data-meter-item", _meter_item);
        $(Element).attr("data-meter-value", _meter_value);
        $(Element).attr("data-meter-range", _meter_range);

        _that.MetersBind.push({
          Id: _meter_id,
          Item: _meter_item,
          Range: _meter_range,
          Value: _meter_value
        });

      }

      if (_property_name != undefined) {
        if (!_that.properties[_property_name]) {
          _that.properties[_property_name] = "";
        }
      }

    });
  return _template[0].innerHTML;
}

// Changes the value of a widget property, with exists check
// No visual update
WidgetBase.prototype.SetPropertyValue = function (PropertyName, PropertyValue) {
  this.properties[PropertyName] = PropertyValue;
}

// Visually updates title of the widget 
WidgetBase.prototype.UpdateTitle = function (Title) {
  if (!this.prepared) {
    throw "Widget not prepared!";
    return;
  }
  this.title = Title;
  $("#" + this.id).find("." + this.titleClass).find('div').eq(0).text(this.title);

  // Info button
  if (this.ShowInfoButton) {
    _w_title = $("#" + this.id).find(".listtittle");
    if (!_w_title.find(".widgetBtn").length) {
      _w_title.append($("<div class='widgetBtn' onclick='if (_Manager.WidgetManager) { _Manager.WidgetManager.GetWidget(\"" + this.id + "\").SetInfoState(); } '><span class='ui-icon ui-icon-info'>INF</span></div>"));
    }
  }
  //
}

// Visually updates the value of the property
WidgetBase.prototype.UpdateProperty = function (PropertyName, PropertyValue, PropertyRange, MeterCount) {
  if (!this.prepared) {
    throw "Widget not prepared!";
    return;
  }
  if (this.SetPropertyValue(PropertyName, PropertyValue)) {
    $("#" + this.id).find('[data-property-name="' + PropertyName + '"]').each(function (Index, Element) {
      var _el = $(Element);
      _el.text(FormatString(this.properties[PropertyName], GetStringFormat(_el)));
    });

  }
}

//
WidgetBase.prototype.ShowMenu = function () {

  //var _w_pop_id = this.id +'_popup_menu';

  //var _popup = $("#" + this.id).find("#" + _w_pop_id);

  //if (_popup.length == 0) {
  //  $("#" + this.id).append("<ul id='" + _w_pop_id + "' class='widgetMenuPopup'>Opciones</ul>");
  //  _popup = $("#" + this.id).find("#" + _w_pop_id);

  //  // Meters
  //  if (this.MeterId.length > 0) {
  //    for (var _meter in _Manager.MetersManager) {
  //      _popup.append('<li>' + _Manager.MetersManager[_meter].Items[0].Description + '</li>')
  //    }
  //  }

  //  _popup.mouseleave(function (evt) {
  //    $(evt.currentTarget).hide();
  //  });
  //  _popup.menu();
  //}

  //_popup.show();

  //alert(this.id);
}

WidgetBase.prototype.SetInfoState = function () {
  if (this.ShowInfoButton) {
    var _widget = $("#" + this.id);
    if (this.InfoVisible) {
      _widget.parent().find(".widgetInfoPanel").hide();
    } else {
      _widget.parent().find(".widgetInfoPanel").show();
    }
    this.InfoVisible = !this.InfoVisible;
  }
}

// ----------------------------------------------------------------------------------------------------------------------------------------------

WidgetItemBase.prototype.constructor = WidgetItemBase;
// Basic ListItem class for widgets that type is List
function WidgetItemBase(IndexValue, Template) {
  this.prepared = false;
  this.template = (!Template ? '' : Template);
  this.indexValue = (!IndexValue ? '' : IndexValue);
  this.properties = {};
  this.parentId = undefined;
  if (this.template != '') { this.SetTemplate(); }
}

// Method to set the template.
WidgetItemBase.prototype.SetTemplate = function () {
  var _that = this;
  $('<div>' + _that.template + '</div>').find('[data-property-name]')
    .each(function (Index, Element) {
      var _property_name = $(Element).data("property-name");
      if (_property_name) { _that.properties[_property_name] = ""; }
    });
}

// Set value of a property
WidgetItemBase.prototype.SetPropertyValue = function (PropertyName, PropertyValue) {
  if (this.properties[PropertyName] == undefined) {
    //throw "Undefined property [" + PropertyName + "]!";
    return false;
  } else {
    this.properties[PropertyName] = PropertyValue;
    return true;
  }
}

// Parse the template with the current property values
WidgetItemBase.prototype.Prepare = function (Counter) {
  var _prepared_template = this.template;
  var _template = $('<div>' + _prepared_template + '</div>');
  var _that = this;
  if (_that.indexValue != '') { _template.find('.item').attr('item-index', _that.indexValue); }
  _template.find('[data-property-item]')
    .each(function (Index, Element) {
      $(Element).attr('property-item', Counter.toString());
    });
  _template.find('[data-property-name]')
    .each(function (Index, Element) {
      var _el = $(Element);
      _el.text(FormatString(_that.properties[_el.data('property-name')], GetStringFormat(_el)));
      //$(Element).text(_that.properties[$(Element).data('property-name')]);
    });
  this.prepared = true;
  return _template[0].innerHTML;
}

// ----------------------------------------------------------------------------------------------------------------------------------------------

WidgetListBase.prototype = new WidgetBase();
WidgetListBase.prototype.constructor = WidgetListBase;
// Basic class for widgets of List type
function WidgetListBase() {
  this.title = 'TITLE';
  this["class"] = "WidgetListBase";
  this.draggableTemplate = ' <div class="listdash"></div>' + ' <div class="listtittle txt_fore_color"></div>';
  this.staticTemplate = ' <div class="listcontent"></div>'
  this.Items = [];
  this.TotalItems = [];
  this.currentPage = 1;
  this.countOfPages = 0;
  this.itemsPerPage = 3;
  this.titleSelector = '.listtittle';
  this.contentSelector = '.listcontent';
  this.showCounter = false;
  this.maxItems = -1;
  this.OnItemClick = undefined;
  this.OnBeforeUpdate = undefined;
  this.Rows = [];
  this.hasScroll = true;
  this.OnRealTimeComplete = null;
}

// Method to add items to the list
WidgetListBase.prototype.AddItem = function (NewListItem) {
  if (this.maxItems == -1 || this.TotalItems.length <= this.maxItems) {
    NewListItem.parentId = this.id;
    this.TotalItems.push(NewListItem);
  }
}

// Calls the Prepare method of the items to generate the parsed template
WidgetListBase.prototype.PrepareItems = function () {
  var _result = '',
       _nitem = '',
       _counter = 0;
  for (var _item in this.Items) {
    _nitem = this.Items[_item].Prepare(_counter);
    if (this.OnItemPrepared) {
      _nitem = this.OnItemPrepared(this.Items[_item], _nitem);
    }
    _result = _result + _nitem;
    _counter++;
  }

  return _result;
}

// Overrided method to update the title with the counter or not
WidgetListBase.prototype.SetTitle = function (Object) {
  var _w_title = undefined;

  Object.find("#" + this.id + "-title").text(_Language.get(this.titleNLS) + (this.showCounter ? ' [' + this.Items.length + ']' : ''));

  // Info button
  if (this.ShowInfoButton) {
    _w_title = $("#" + this.id).find(".listtittle");
    if (!_w_title.find(".widgetBtn").length) {
      _w_title.append($("<div class='widgetBtn' onclick='if (_Manager.WidgetManager) { _Manager.WidgetManager.GetWidget(\"" + this.id + "\").SetInfoState(); } '><span class='ui-icon ui-icon-info'>INF</span></div>"));
    }
  }
  //
}

// Overrides Prepare method
WidgetListBase.prototype.Prepare = function () {
  var _prepared_template = WidgetBase.prototype.Prepare.call(this);
  var _object = $('<div>' + _prepared_template + '</div>');
  var _list = _object.find(this.contentSelector);
  var _that = this;
  var _items_prepared = this.PrepareItems();

  _list.append(_items_prepared);
  this.SetTitle(_object);
  _object.find('[data-property-name]')
    .each(function (Index, Element) {
      var _el = $(Element);
      _el.text(FormatString(_that.properties[_el.data('property-name')], GetStringFormat(_el)));
      //$(Element).text(_that.properties[$(Element).data('property-name')]);
    });

  return _object[0].innerHTML;
}

// This method binds the event of each item click to the property OnItemClick of the list
WidgetListBase.prototype.BindEvents = function (Object) {
  var _that = this;
  if (!Object) { return; }
  Object.find(this.BindEventsToClass).each(function (Index, Element) {
    $(Element).click(function () {
      if (_that.OnItemClick) {
        _that.OnItemClick(_that, Element);
      }
    });
  });
}

// Clear all items on the list, optionally can force visually update
WidgetListBase.prototype.ClearItems = function (Update) {
  if (!Update) { Update = false; }
  var _object = $("#" + this.id);
  if (_object) {
    _object.find(this.contentSelector).empty();
    this.Items = [];
    //this.TotalItems = [];
    //this.Rows = [];


  }
  if (Update) {
    this.Update();
  }
}

// Visually update of the list
WidgetListBase.prototype.Update = function () {
  this.ClearItems(false);


  if (this.OnBeforeUpdate) {
    this.OnBeforeUpdate(this);
  }

  var _object = $("#" + this.id);
  var _list = _object.find(this.contentSelector);
  _list.append(this.PrepareItems());
  this.SetTitle(_object);

  if (this.PrepareStyles) {
    _list = this.PrepareStyles(_list);
  }

  this.PreparePagination();
}

WidgetListBase.prototype.PreparePagination = function () {
  var _object = $("#" + this.id);
  var _paginationBar = _object.find(".pagination-bar");
  var _totalItems = this.TotalItems;
  var _widget = this;
  setPageItems(this, this.currentPage);
  if (_totalItems.length > 0 && _totalItems.length >= this.itemsPerPage) {
    _paginationBar.show();
    $("#" + this.id + "-pagination-button-right").off('click');
    $("#" + this.id + "-pagination-button-right").click(function () {
      var _currentPage = _widget.currentPage;
      if (Math.ceil(_widget.TotalItems.length / _widget.itemsPerPage) >= _currentPage + 1) {
        _paginationBar
        _currentPage++;
        _widget.currentPage = _currentPage;
        setPageItems(_widget, _widget.currentPage);

      }
    });
    $("#" + this.id + "-pagination-button-left").off('click');
    $("#" + this.id + "-pagination-button-left").click(function () {
      var _currentPage = _widget.currentPage;
      if (_currentPage > 1) {
        _currentPage--;
        _widget.currentPage = _currentPage;
        setPageItems(_widget, _widget.currentPage);
      }
    });
  } else {
    _paginationBar.hide();
  }
}

function setPageItems(Widget, currentPage) {
  var i = 0;
  var initialPage = (currentPage - 1);
  var _indexMinor = Widget.itemsPerPage * initialPage;
  var _indexMayor = (Widget.itemsPerPage * initialPage) + Widget.itemsPerPage;
  Widget.Items = [];
  //Fill the Widget with the items of the page 
  for (var item in Widget.TotalItems) {
    if (i >= _indexMinor && i < _indexMayor) {
      Widget.Items.push(Widget.TotalItems[item]);
    }
    i++;
  }
  //Update Lists of items in Widgets
  var _object = $("#" + Widget.id);
  var _list = _object.find(Widget.contentSelector);
  _list.html(Widget.PrepareItems());

  //Update the pagination indicator
  var _paginationIndicator = $("#" + Widget.id + "-pagination-indicator");
  _paginationIndicator.text(Widget.currentPage + " / " + Math.ceil(Widget.TotalItems.length / Widget.itemsPerPage));

  //Show or hide buttons
  if (Widget.currentPage == Math.ceil(Widget.TotalItems.length / Widget.itemsPerPage))
    $("#" + Widget.id + "-pagination-button-right").css('visibility', 'hidden');
  else
    $("#" + Widget.id + "-pagination-button-right").css('visibility', 'visible');
  if (Widget.currentPage == 1)
    $("#" + Widget.id + "-pagination-button-left").css('visibility', 'hidden');
  else
      $("#" + Widget.id + "-pagination-button-left").css('visibility', 'visible');

  if (Widget.BindEvents) {
      Widget.BindEvents(_Manager.WidgetManager.Container.find("#" + Widget.id));
  }
}

function setNextPage(Widget) {
  var _currentPage = Widget.currentPage;
  Widget.currentPage = _currentPage++;
  setPageItems(Widget, Widget.currentPage);
}

function setPreviousPage(Widget) {
  var _currentPage = Widget.currentPage;
  if (_currentPage > 1) {
    Widget.currentPage = _currentPage--;
    setPageItems(Widget, Widget.currentPage);
  }
}

// ----------------------------------------------------------------------------------------------------------------------------------------------

//WidgetTimerBase.prototype = new WidgetBase()
//WidgetTimerBase.prototype.constructor = WidgetTimerBase;
//// Basic Time widget class
//function WidgetTimerBase(Id, Template) {
//  var _that = this;
//  this.id = Id;
//  this.SetTemplate(Template);
//  this.properties.handle_interval = null;
//  this.properties.interval_delay = 1000;
//  this.properties.time_update = function () {
//    var _date = new Date();
//    var _hours = _date.getHours()
//    var _minutes = _date.getMinutes();
//    var _time = (_hours > 12) ? (_.padLeft(_hours - 12, 2, "0") + ':' + _.padLeft(_minutes, 2, "0") + ":" + _.padLeft(_date.getSeconds(), 2, "0") + ' PM') : (_.padLeft(_hours, 2, "0") + ':' + _.padLeft(_minutes, 2, "0") + ":" + _.padLeft(_date.getSeconds(), 2, "0") + ' AM');
//    var _today = _date.toDateString();
//    $('#' + _that.id).find('[data-property-name="today"]').text(_today);
//    $('#' + _that.id).find('[data-property-name="hour"]').text(_time);
//  };
//  this.OnAfterAdded = function () {
//    this.properties.handle_interval = setInterval(this.properties.time_update, this.properties.interval_delay);
//  };
//}

// ----------------------------------------------------------------------------------------------------------------------------------------------

WidgetColumnBase.prototype = new WidgetItemBase();
WidgetColumnBase.prototype.constructor = WidgetColumnBase;
//Basic class for ColumnItems in Columns widget
function WidgetColumnBase(IndexValue, Template) {
  WidgetItemBase.call(this, IndexValue, Template);

  this.Prepare = function (Counter) {
    var _result = WidgetItemBase.prototype.Prepare.call(this, Counter);
    return _result;
  }
}

// ----------------------------------------------------------------------------------------------------------------------------------------------

WidgetColumnsBase.prototype = new WidgetListBase()
WidgetColumnsBase.prototype.constructor = WidgetColumnsBase;
// Basic class for columns list widget
function WidgetColumnsBase() {
  this.contentSelector = '.columnscontent';
  this.barClass = ".task_last_7_char_bg";
  this.barValueProperty = "value";
  this.barMaxIncrement = 0;
  this.Items = [];
  this.OnAfterAdded = function (Widget) {
    this.Update();
  }
}

// Visually updates the widget
WidgetColumnsBase.prototype.Update = function () {
  var _num_items = this.Items.length;
  var _wid = $("#" + this.id);
  var _object = undefined;
  if (_num_items > 0) {
    var _max = 0;
    for (_i = 0; _i < _num_items; _i++) {
      if (_max < this.Items[_i].properties[this.barValueProperty]) {
        _max = this.Items[_i].properties[this.barValueProperty];
      }
    }
    _max = _max + this.barMaxIncrement;
    for (_i = 0; _i < _num_items; _i++) {
      _object = _wid.find('[item-index="' + this.Items[_i].indexValue + '"]').find(this.barClass)[0];
      if (_object) {
        $(_object).progressbarVertical({
          value: ((this.Items[_i].properties[this.barValueProperty] * 100) / _max)
        });
        $(_object).addClass("task_bars");
        $(_object).find('.ui-progressbar-value').addClass('task_last_7_char_bar_value');
      }
    }
  }
}

// ----------------------------------------------------------------------------------------------------------------------------------------------

WidgetSVGAmphitheater.prototype = new WidgetBase()
WidgetSVGAmphitheater.prototype.constructor = WidgetSVGAmphitheater;
// Basic Arcs chart SVG widget
function WidgetSVGAmphitheater() {
  // Defaults
  this.defaultColors = ["#DE6900", "#0088CA", "#5A2F8B ", "#EEC30A", "#0cc", "#f00", "#0f0", "#00f", "#cc0", "#0cc"];
  // Properties
  this.animations = "bounce";
  this.bars = [];
  this.Calculated = false;
  this.horizontalDisplacement = 0;
  this.verticalDisplacement = 100;
  this.initialAngle = -90;
  this.maxAngle = 90;
  this.maxBarSize = 50;
  this.valueType = EN_FORMATS.F_PERCENT;  // See WSICommonFormat
  this.animationTime = 1500;
  this.barMargin = 7;
  this.labelMargin = this.barMargin;
  this.drawBarContainers = true;
  this.drawLabels = true;
  this.labelsClass = "txt_fore_color amphitheater_label";
  this.barContainerClass = "gauge_background_container";
  this.barContainerColor = "#878787";
  this.defaultLabelSize = "12em";
  this.defaultMeterFontSite = "13em";
  this.defaultMeterFontFamily = "Calibri";
  this.SVGContainer = '.svg_container';
  // Members
  this.svgContainer = undefined;
  this.paper = undefined;
  // Events
  this.OnGraphClick = undefined;
  this.OnBeforeUpdate = undefined;
  // Create svg container after added
  this.OnAfterAdded = function () {

    var _w_title = undefined;

    // Info button & panel
    if (this.ShowInfoButton) {
      _w_title = $("#" + this.id).find(".listtittle");
      if (!_w_title.find(".widgetBtn").length) {
        _w_title.append($("<div class='widgetBtn' onclick='if (_Manager.WidgetManager) { _Manager.WidgetManager.GetWidget(\"" + this.id + "\").SetInfoState(); } '><span class='ui-icon ui-icon-info'>INF</span></div>"));
      }
      $("#" + this.id).append($("<div class='widgetInfoPanel' style='display: none;' data-nls='" + this.InfoNLS + "'>" + _Language.get(this.InfoNLS).substring(1) + "</div>"));
    }
    //

    this.svgContainer = $("#" + this.id).find(this.SVGContainer)[0];
    var _width = $(this.svgContainer).width(),
       _height = $(this.svgContainer).height();
    if (this.svgContainer) {
      // Create container
      this.paper = Raphael(this.svgContainer, _width, _height);
      //this.paper.setViewBox(0, 0, _width, _height);
      this.paper.canvas.setAttribute('preserveAspectRatio', 'none');
      $(this.svgContainer).attr('width', _width).attr('height', _height);
      // Add extensions
      this.paper.customAttributes.arcExt = function (centerX, centerY, startAngle, endAngle, innerR, outerR) {
        var radians = Math.PI / 180,
                largeArc = +(endAngle - startAngle > 180);
        outerX1 = centerX + outerR * Math.cos((startAngle - 90) * radians),
                outerY1 = centerY + outerR * Math.sin((startAngle - 90) * radians),
                outerX2 = centerX + outerR * Math.cos((endAngle - 90) * radians),
                outerY2 = centerY + outerR * Math.sin((endAngle - 90) * radians),
                innerX1 = centerX + innerR * Math.cos((endAngle - 90) * radians),
                innerY1 = centerY + innerR * Math.sin((endAngle - 90) * radians),
                innerX2 = centerX + innerR * Math.cos((startAngle - 90) * radians),
                innerY2 = centerY + innerR * Math.sin((startAngle - 90) * radians);
        var path = [
                        ["M", outerX1, outerY1],
                        ["A", outerR, outerR, 0, largeArc, 1, outerX2, outerY2],
                        ["L", innerX1, innerY1],
                        ["A", innerR, innerR, 0, largeArc, 0, innerX2, innerY2],
                        ["z"]
        ];
        return { path: path };
      };

      if (this.bars.length > 0) {
        this.Update();
      }

    } else {
      throw "Unable to find the SVG container!";
    }
  }
}

// Generates the bars for the current Meters
WidgetSVGAmphitheater.prototype.addMeterBar = function (Meter, MeterItem, Range, Value, Text) {
  var _idx = this.bars.length;
  var _params = {
    value: null,
    valueKey: Value,
    category: Range,
    meter: Meter,
    item: MeterItem,
    "class": this.labelsClass,
    angle: this.initialAngle,
    color: [this.defaultColors[_idx]],
    label_size: this.defaultLabelSize,
    IsMeterBar: true,
    text: Text
  };
  this.bars.push(_params);
  return this.bars[_idx];
}

// Adds a bar arc to the list of arcs
WidgetSVGAmphitheater.prototype.addBar = function (Category, Value) {
  var _idx = this.bars.length;
  var _params = {
    value: Value,
    category: Category,
    "class": this.labelsClass,
    angle: this.initialAngle,
    color: [this.defaultColors[_idx]],
    label_size: this.defaultLabelSize
  };
  this.bars.push(_params);
  return this.bars[_idx];
}

// Convert a percent value to an angle
WidgetSVGAmphitheater.prototype.AngleByPercent = function (Value, Max) {
  return (Value * Max) / 100;
}

// Bind the click on the svg container to the widget click event
WidgetSVGAmphitheater.prototype.BindEvents = function (Object) {
  //var _that = this;
  //if (!Object) { return; }
  //Object.find(this.SVGContainer).each(function (Index, Element) {
  //  $(Element).click(function () {
  //    if (_that.OnGraphClick) {
  //      _that.OnGraphClick(_that, Element);
  //    }
  //  });
  //});
}

// Creates the label in he chart for a bar
WidgetSVGAmphitheater.prototype.DrawLabel = function (Bar, BarHeight) {
  var _text = undefined,
      _box = undefined,
      _value = Bar.value,
      _bar_width = Bar.outerRadious - Bar.innerRadious,
      _final_value;

  if (isNaN(_value)) { _value = 0; }

  //_text = this.paper.text((Bar.centerX - Bar.outerRadious) + (BarHeight / 2), Bar.centerY + (this.labelMargin), Bar.category).rotate(Bar.angle);
  _text = this.paper.text((Bar.centerX - Bar.outerRadious) + (BarHeight / 2), Bar.centerY + (this.labelMargin), _Language.get(Bar.text)).rotate(Bar.angle);
  _text.attr({ "font-size": this.defaultMeterFontSite, "font-family": this.defaultMeterFontFamily, "font-weight": "normal" });
  _text.node.setAttribute("class", Bar["class"]);
  _box = _text.getBBox();
  _text.transform("t0," + (_box.height / 2)).rotate(Bar.angle);

  if ((this.maxAngle - this.initialAngle) <= 180) {
    // Only show values in <= 180 degrees arcs
    _final_value = FormatString(_value, this.valueType, 2);
    _text = this.paper.text((Bar.centerX + Bar.innerRadious) + (BarHeight / 2), Bar.centerY + (this.labelMargin), _final_value).rotate(Bar.angle);
    _text.attr({ "font-size": this.defaultMeterFontSite, "font-family": this.defaultMeterFontFamily, "font-weight": "normal" });
    _text.node.setAttribute("class", Bar["class"]);
    _box = _text.getBBox();
    _text.transform("t0," + (_box.height / 2)).rotate(Bar.angle);
  }
}

// Visually updates the chart
WidgetSVGAmphitheater.prototype.Update = function (RecalculateBars) {
  if (RecalculateBars == undefined) { RecalculateBars = true; }

  var _num_bars = this.bars.length + 1,
      _height = $(this.svgContainer).height(),
      _width = $(this.svgContainer).width(),
      _bar_height = 0,
      _max_distance = 0,
      _angle_length = this.maxAngle - this.initialAngle,
      _bar_container = undefined,
      _bar = null,
      _meter_info = undefined,
      _max_value = undefined,
      _value = undefined;

  this.paper.clear();

  // Prepare data
  for (var _bi = 0; _bi < _num_bars - 1; _bi++) {
    _bar = this.bars[_bi];

    if (_bar.IsMeterBar) {
      // Obtain meter value
      _meter_info = _Manager.MetersManager.Meters[this.MeterId[_bar.item]];

      if (_meter_info) {
        var _value;
        _value = parseCurrency(_meter_info.GetValue(_bar.item, _bar.category, _bar.valueKey));
        _bar.value = Round(_value, 4);
      }
    }
  }

  // Before Update (Bars data is updated)
  if (this.OnBeforeUpdate) {
    this.OnBeforeUpdate(this);
  }

  if (this.valueType == EN_FORMATS.F_PERCENT) {
    _max_value = 100;
  } else {
    // Calculate max value
    for (var _bi = 0; _bi < _num_bars - 1; _bi++) {
      _bar = this.bars[_bi];
      _max_value = _max_value > _bar.value ? _max_value : _bar.value;
    }
  }

  // Process to draw
  for (var _bi = 0; _bi < _num_bars - 1; _bi++) {
    _bar = this.bars[_bi];

    // Pre-calc
    if (RecalculateBars || !this.Calculated) {
      _bar.centerX = (_width / 2) + this.horizontalDisplacement;
      _bar.centerY = (_height) - this.verticalDisplacement;
      _bar.startAngle = this.initialAngle;

      // RMS : Check if must show percent values from other type of vals.
      _value = (100 * _bar.value) / _max_value;
      if (isNaN(_value)) { _value = 0; }
      _bar.endAngle = _bar.startAngle + this.AngleByPercent(_value, _angle_length);

      _max_distance = (_bar.centerY > _bar.centerX) ? _bar.centerX : _bar.centerY;
      _max_distance = _max_distance;
      _bar_height = (_max_distance / _num_bars);
      _bar_height = (_bar_height > this.maxBarSize) ? this.maxBarSize : _bar_height;
      _bar.innerRadious = _max_distance - (_bar_height * (_bi + 1));
      _bar.outerRadious = _bar.innerRadious + _bar_height - this.barMargin;
    }

    // Bar Containers
    if (this.drawBarContainers) {
      _bar_container = this.paper.path().attr({ arcExt: [_bar.centerX, _bar.centerY, this.initialAngle, this.maxAngle, _bar.innerRadious, _bar.outerRadious] });
      _bar_container.node.setAttribute("class", this.barContainerClass);
    }
    // Draw texts
    if (this.drawLabels) {
      this.DrawLabel(_bar, _bar_height);
    }
    // Draw & Animate
    if (this.animations != "none") {
      _bar.object = this.paper.path().attr({ "fill": _bar.color, "stroke": _bar.color, "stroke-width": 1, arcExt: [_bar.centerX, _bar.centerY, _bar.startAngle, _bar.startAngle, _bar.innerRadious, _bar.outerRadious] });
      _bar.object.animate({ arcExt: [_bar.centerX, _bar.centerY, _bar.startAngle, _bar.endAngle, _bar.innerRadious, _bar.outerRadious] }, this.animationTime, this.animations);
    } else {
      _bar.object = this.paper.path().attr({ "fill": _bar.color, "stroke": _bar.color, "stroke-width": 1, arcExt: [_bar.centerX, _bar.centerY, _bar.startAngle, _bar.endAngle, _bar.innerRadious, _bar.outerRadious] });
    }
  }

  // Draw title
  if (this.MeterNameAsTitle) {
    this.UpdateTitleByMeters();
  }

  this.Calculated = true;
}

// ----------------------------------------------------------------------------------------------------------------------------------------------

// Enumeration of chart series drawing types
var WidgetSVGChartTypes = {
  linear: 0,
  linearea: 1,
  bars: 2,
  curves: 3, // UNABLE
  curvesarea: 4, // UNABLE
  smothlines: 5,
  smothlinesarea: 6
}

// ----------------------------------------------------------------------------------------------------------------------------------------------

WidgetSVGChart.prototype = new WidgetBase()
WidgetSVGChart.prototype.constructor = WidgetSVGChart;
// Basic class for standard charting with SVG
function WidgetSVGChart() {
  // Defaults
  this.defaultColors = ["#f00", "#0f0", "#00f", "#cc0", "#0cc", "#f00", "#0f0", "#00f", "#cc0", "#0cc"];
  // Properties
  this.animations = "none";
  this.categories = [];
  this.lines = [];
  this.linesMaxValue = 0;
  this.linesMinValue = 0;
  this.Calculated = false;
  this.showLabels = false;
  this.labelsClass = '';
  this.LabelSpace = 5;
  this.margin = 5;
  this.topmargin = 5;
  this.lineMaxIncrement = 0;
  this.valueType = 1;  // 0: none - 1: percent - 2: monetary - 3: decimal ,...
  this.animationTime = 1500;
  this.labelsClass = "txt_fore_color amphitheater_label";
  this.defaultLabelSize = "12em";
  this.SVGContainer = ".svg_container";
  // Members
  this.svgContainer = undefined;
  this.paper = undefined;
  // Events
  this.OnGraphClick = undefined;
  // Create svg container after added
  this.OnAfterAdded = function () {
    this.svgContainer = $("#" + this.id).find(this.SVGContainer)[0];
    this.svg_width = $(this.svgContainer).width();
    this.svg_height = $(this.svgContainer).height();
    this.svg_height = this.svg_height - (this.svg_height * 0.10);
    //this.svg_width = $("#" + this.id).width();
    //this.svg_height = $("#" + this.id).height();
    if (this.svgContainer) {
      // Create container
      this.paper = Raphael(this.svgContainer, this.svg_width, this.svg_height);
      //this.paper.setViewBox(0, 0, _width, _height);
      this.paper.canvas.setAttribute('preserveAspectRatio', 'none');
      $(this.svgContainer).attr('width', this.svg_width).attr('height', this.svg_height);
      if (this.lines.length > 0) {
        this.Update();
      }

    } else {
      throw "Unable to find the SVG container!";
    }
  }
}

// Adds a serie from a meter to the widget
WidgetSVGChart.prototype.addMeterSerie = function (Meter, MeterItem, Value) {
  var _idx = this.lines.length;
  var _params = {
    values: [], // Obtain values
    serie: _Manager.MetersManager.GetMeterValue(this.MeterId[Meter], MeterItem, 0, "Descript"),
    isMeterSerie: true,
    "class": this.labelsClass,
    color: [this.defaultColors[_idx]],
    fill_opacity: 0.5,
    border_opacity: 1.0,
    border_dasharray: "1", // ["", "-", ".", "-.", "-..", ". ", "- ", "--", "- .", "--.", "--.."]
    border_width: 1,
    label_size: this.defaultLabelSize,
    type: WidgetSVGChartTypes.linear,
    visible: true,
    imageHeight: 0,
    imageWidth: 0,
    pointImages: null,
    drawDots: false,
    dotSize: 5,
    dotClass: null
  };
  this.lines.push(_params);
  if (this.categories.length < Values.length) {
    this.categories.length = Values.length;
  }
  for (var _v in Values) {
    if (Values[_v] > this.linesMaxValue) { this.linesMaxValue = Values[_v]; };
    if (Values[_v] < this.linesMinValue) { this.linesMinValue = Values[_v]; };
  }
  return this.lines[_idx];
}

// Adds a serie to the widget
WidgetSVGChart.prototype.addSerie = function (Serie, Values) {
  var _idx = this.lines.length;
  var _params = {
    values: Values,
    serie: Serie,
    isMeterSerie: false,
    "class": this.labelsClass,
    color: [this.defaultColors[_idx]],
    fill_opacity: 0.5,
    border_opacity: 1.0,
    border_dasharray: "1", // ["", "-", ".", "-.", "-..", ". ", "- ", "--", "- .", "--.", "--.."]
    border_width: 1,
    label_size: this.defaultLabelSize,
    type: WidgetSVGChartTypes.linear,
    visible: true,
    imageHeight: 0,
    imageWidth: 0,
    pointImages: null,
    drawDots: false,
    dotSize: 5,
    dotClass: null
  };
  this.lines.push(_params);
  if (this.categories.length < Values.length) {
    this.categories.length = Values.length;
  }
  for (var _v in Values) {
    if (Values[_v] > this.linesMaxValue) { this.linesMaxValue = Values[_v]; };
    if (Values[_v] < this.linesMinValue) { this.linesMinValue = Values[_v]; };
  }
  return this.lines[_idx];
}

// Bind the events
WidgetSVGChart.prototype.BindEvents = function (Object) {
  var _that = this;
  if (!Object) { return; }
  Object.find(this.SVGContainer).each(function (Index, Element) {
    $(Element).click(function () {
      if (_that.OnGraphClick) {
        _that.OnGraphClick(_that, Element);
      }
    });
  });
}

// Set the categories names
WidgetSVGChart.prototype.setCategories = function (Categories) {
  if (!Categories) {
    Categories = [];
  }
  if (Categories.length < this.lines.length) {
    Categories.length = this.lines.length;
  }
  this.categories = Categories;
}

// Obtains the start point for drawing a serie
WidgetSVGChart.prototype.getStartPoint = function (Serie) {
  var _result = ['M', 0, this.ceroPoint];
  if (this.margin > 0) {
    _result[1] = this.margin;
  }
  return _result;
}

// Generates the svg path points for a serie
WidgetSVGChart.prototype.getPoints = function (Serie) {
  //var _height = this.svg_height,
  var _height = this.svgContainer.clientHeight-20,
      _serie_points = Serie.values.length,
      _result = [],
      _img = null;
  for (var _v = 0; _v < _serie_points; _v++) {

    switch (Serie.type) {
      case WidgetSVGChartTypes.linear:
      case WidgetSVGChartTypes.linearea:
        _result.push((_v == 0) ? "M" : "L");
        _result.push(this.margin + (this.pointSpace * _v)); // X
        _result.push(_height - (Serie.values[_v] * _height / (this.linesMaxValue + this.topmargin))); // Y
        break;
      case WidgetSVGChartTypes.smothlines:
      case WidgetSVGChartTypes.smothlinesarea:
        switch (_v) {
          case 0:
            _result.push("M");
            _result.push(this.margin + (this.pointSpace * _v)); // X
            _result.push(_height - (Serie.values[_v] * _height / (this.linesMaxValue + this.topmargin))); // Y
            break;
          case 1:
            _result.push("R");
            _result.push(this.margin + (this.pointSpace * _v)); // X
            _result.push(_height - (Serie.values[_v] * _height / (this.linesMaxValue + this.topmargin))); // Y
            break;
          default:
            _result.push(this.margin + (this.pointSpace * _v)); // X
            _result.push(_height - (Serie.values[_v] * _height / (this.linesMaxValue + this.topmargin))); // Y
            break;
        }
        break;
      case WidgetSVGChartTypes.bars:
        // TODO:
        //var test = _v;
        //var test2 = _height -(Serie.values[_v]* _height);
        //var test3 = (Serie.values[_v]* _height / this.linesMaxValue);
        //var test4 = Serie.values[_v] * _height
        _height_aux = this.ceroPoint;
        _result.push("L");
        _result.push(this.margin + (this.pointSpace * _v));
        _result.push(_height_aux - (Serie.values[_v] * _height_aux / this.linesMaxValue));
        if (_v < _serie_points) {
          _result.push("M");
          _result.push(this.margin + (this.pointSpace * (_v + 1)));
          _result.push(this.ceroPoint);
        }
        break;
      case WidgetSVGChartTypes.curves:
      case WidgetSVGChartTypes.curvesarea:
        if (_v == 0) {
          _result.push("M");
          _result.push(this.margin + (this.pointSpace * _v)); // X
          _result.push(_height - (Serie.values[_v] * _height / (this.linesMaxValue + this.topmargin))); // Y
        }
        _result.push("Q");
        _result.push(this.margin + (this.pointSpace * _v) - (this.pointSpace / 2)); // Control Point X
        if (_v == 0 || Serie.values[_v] > Serie.values[_v - 1]) {
          // up
          //_result.push(this.margin); // Control Point Y  
          _result.push(_height - (Serie.values[_v] * _height / this.linesMaxValue) - 5);
        } else {
          // down
          //_result.push(_height); // Control Point Y
          _result.push(_height - (Serie.values[_v] * _height / this.linesMaxValue) + 5);
        }
        _result.push(this.margin + (this.pointSpace * _v)); // X
        _result.push(_height - (Serie.values[_v] * _height / (this.linesMaxValue + this.topmargin))); // Y
        break;
      default:
        continue;
        break;
    }

    if (Serie.pointImages != null) {
      if (this.animations != "none") {
        _img = this.paper.image(Serie.pointImages[_v],
                  (this.margin + (this.pointSpace * _v)) - (Serie.imageWidth / 2),
                  -Serie.imageHeight,
                  Serie.imageWidth,
                  Serie.imageHeight
                  );
        _img.animate({ transform: 'T0,' + (Serie.imageHeight + _height - (Serie.values[_v] * _height / this.linesMaxValue) - (Serie.imageHeight)) }, this.animationTime, this.animations);
      } else {
        _img = this.paper.image(Serie.pointImages[_v],
                  (this.margin + (this.pointSpace * _v)) - (Serie.imageWidth / 2), // eje X
                this.ceroPoint+5, // eje Y // DMT 07/06/2016 - Con esto los iconos quedan alineados encima de los dias y debajo de las barras.
                Serie.imageWidth,
                Serie.imageHeight
                );
      }
    }

    if (Serie.labels != null) {
      var _txt = this.paper.text(
                (this.margin + (this.pointSpace * _v))
                , this.ceroPoint + 36
                , Serie.labels[_v]);
      _txt.attr({ "font-size": 16, "font-family": "Calibri", fill: "white" });
    }

  }

  if (Serie.type == WidgetSVGChartTypes.linearea ||
      Serie.type == WidgetSVGChartTypes.curvesarea ||
      Serie.type == WidgetSVGChartTypes.smothlinesarea) {
    // Close the shape
    _result.push("L");
    _result.push(this.margin + (this.pointSpace * (_v - 1)));
    _result.push(_height);
    _result.push("L");
    _result.push(this.margin);
    _result.push(_height);
    _result.push("L");
    _result.push(this.margin);
    _result.push(this.ceroPoint);
    _result.push("Z");
  }
  return _result;
}

// Obtains the serie attributes
WidgetSVGChart.prototype.getSerieAttributes = function (Serie) {

  switch (Serie.type) {
    case WidgetSVGChartTypes.linear:
    case WidgetSVGChartTypes.curves:
    case WidgetSVGChartTypes.smothlines:
      return { "stroke": Serie.color, "stroke-width": Serie.border_width, "stroke-dasharray": Serie.border_dasharray };
      break;
    case WidgetSVGChartTypes.linearea:
    case WidgetSVGChartTypes.curvesarea:
    case WidgetSVGChartTypes.smothlinesarea:
      return { "fill": Serie.color, "stroke": Serie.color, "stroke-width": Serie.border_width, "fill-opacity": Serie.fill_opacity, "stroke-dasharray": Serie.border_dasharray };
      break;
    case WidgetSVGChartTypes.bars:
      return { "stroke": Serie.color, "stroke-width": (this.pointSpace / 3) };
      break;
    default:
      return {};
      break;
  }

}

// Create Serie Labels
WidgetSVGChart.prototype.CreateLabels = function (Serie) {
  var _label = null,
      _diff = 10, _inc = 2;
  switch (Serie.type) {
    case WidgetSVGChartTypes.linear:
    case WidgetSVGChartTypes.curves:
    case WidgetSVGChartTypes.smothlines:
    case WidgetSVGChartTypes.linearea:
    case WidgetSVGChartTypes.curvesarea:
    case WidgetSVGChartTypes.smothlinesarea:
      if (this.showLabels) {

        switch (Serie.type) {
          case WidgetSVGChartTypes.linear:
          case WidgetSVGChartTypes.curves:
            _diff = 0;
            _inc = 3;
            break;
          case WidgetSVGChartTypes.smothlines:
            _diff = 0;
            _inc = 2
            break;
          case WidgetSVGChartTypes.linearea:
            _diff = 10;
            _inc = 3;
            break;
          case WidgetSVGChartTypes.curvesarea:
          case WidgetSVGChartTypes.smothlinesarea:
            break;
        }

        var _c = 1, _x, _y, _val;

        var _p = this.getPoints(Serie);

        // First Value
        _val = Serie.values[0];
        _x = _p[1];
        _y = _p[2];
        _label = this.paper.text(_x, _y - this.LabelSpace, _val).attr({ 'text-anchor': 'middle' });
        if (this.labelsClass != '') {
          _label.node.setAttribute('class', this.labelsClass);
        }

        // Rest of Values
        for (var _i = 4; _i < _p.length - _diff; _i = _i + _inc) {
          _val = Serie.values[_c];
          _x = _p[_i];
          _y = _p[_i + 1];

          _label = this.paper.text(_x, _y - this.LabelSpace, _val).attr({ 'text-anchor': 'middle' });
          if (this.labelsClass != '') {
            _label.node.setAttribute('class', this.labelsClass);
          }

          _c++;
        }
      }
      break;
    case WidgetSVGChartTypes.bars:

      break;
    default:

      break;
  }
}

// TODO - Create Serie Dots
WidgetSVGChart.prototype.CreateDots = function (Serie) {
  var _dot = null,
      _diff = 10, _inc = 2;
  // TODO: know where to put
  switch (Serie.type) {
    case WidgetSVGChartTypes.linear:
    case WidgetSVGChartTypes.curves:
    case WidgetSVGChartTypes.smothlines:
    case WidgetSVGChartTypes.linearea:
    case WidgetSVGChartTypes.curvesarea:
    case WidgetSVGChartTypes.smothlinesarea:

      if (Serie.drawDots) {

        switch (Serie.type) {
          case WidgetSVGChartTypes.linear:
          case WidgetSVGChartTypes.curves:
            _diff = 0;
            _inc = 3;
            break;
          case WidgetSVGChartTypes.smothlines:
            _diff = 0;
            _inc = 2
            break;
          case WidgetSVGChartTypes.linearea:
            _diff = 10;
            _inc = 3;
            break;
          case WidgetSVGChartTypes.curvesarea:
          case WidgetSVGChartTypes.smothlinesarea:
            break;
        }

        var _c = 1, _x, _y, _val;

        var _p = this.getPoints(Serie);

        // First Value
        _val = Serie.values[0];
        _x = _p[1];
        _y = _p[2];

        _dot = this.paper.circle(_x, _y, Serie.dotSize);

        if (Serie.dotClass != null) {
          _dot.node.setAttribute('class', Serie.dotClass);
        } else {
          _dot.attr(this.getSerieAttributes(Serie));
        }

        // Rest of Values
        for (var _i = 4; _i < _p.length - _diff; _i = _i + _inc) {
          _val = Serie.values[_c];
          _x = _p[_i];
          _y = _p[_i + 1];

          _dot = this.paper.circle(_x, _y, Serie.dotSize);

          if (Serie.dotClass != null) {
            _dot.node.setAttribute('class', Serie.dotClass);
          } else {
            _dot.attr(this.getSerieAttributes(Serie));
          }

          _c++;
        }
      }
      break;
    case WidgetSVGChartTypes.bars:

      break;
    default:

      break;
  }
}

// Visually updates the chart
WidgetSVGChart.prototype.Update = function (RecalculateLines) {

  if (this.OnBeforeUpdate) {
    this.OnBeforeUpdate(this);
  }

  if (!this.paper) { return; }
  if (RecalculateLines == undefined) { RecalculateLines = false; }

  //update meter bars (weekday)
  if (this.lines[1].number_labels) {
    var _day_of_week = [];
    for (_number_day in this.lines[1].number_labels) {
      _day_of_week.push(getDayOfWeek(this.lines[1].number_labels[_number_day]));
    }
    this.lines[1].labels = _day_of_week
  }

  if (RecalculateLines) {
    this.svg_width = $(this.svgContainer).width(),
    this.svg_height = $(this.svgContainer).height();
    this.svg_height = this.svg_height - (this.svg_height * 0.10);
  }

  var _num_lines = this.lines.length + 1,
      _line_data = null,
      _cero_point = this.svgContainer.clientHeight-20,
      _line_points = 0,
      _lines_height = 0,
      _line = null;

  this.paper.clear();

  // Pre-calc
  _lines_height = this.linesMaxValue - this.linesMinValue + this.topmargin;
  if (this.linesMinValue < 0) {
    //_cero_point = this.svg_height - (lines_height * Math.abs(this.linesMinValue) / this.svg_height);
    _cero_point = this.svgContainer.clientHeight - 20 - (lines_height * Math.abs(this.linesMinValue) / this.svg_height);
  }

  if (!this.Calculated) { this.linesMaxValue = this.linesMaxValue + this.lineMaxIncrement; }

  this.ceroPoint = _cero_point - 40 // DMT 07/06/2016 - Con - 25 hace mas chicas las barras del widget de Acc vs Weather para q entre todo en el widget
  this.linesHeight = _lines_height;
  this.pointSpace = (this.svgContainer.clientWidth - (this.margin * 2)) / (this.categories.length - 1);
  for (var _bi = 0; _bi < _num_lines - 1; _bi++) {
    _line = this.lines[_bi];
    if (!_line.visible) { continue; }
    _line_points = _line.values.length;
    _line_data = [];

    // Start point
    _line_data = _line_data.concat(this.getStartPoint(_line));

    // Points
    _line_data = _line_data.concat(this.getPoints(_line));
    _line.object = this.paper.path(_line_data).attr(this.getSerieAttributes(_line));

    // Labels
    this.CreateLabels(_line);

    // Dots
    this.CreateDots(_line);

    // Animate
    if (this.animations != "none") {
      _line.object.attr({ "transform": "s0 0" });
      _line.object.animate({ "transform": "s1 1" }, this.animationTime, this.animations);
    }
  }

  this.Calculated = true;
}

// ----------------------------------------------------------------------------------------------------------------------------------------------

WidgetSVGDonutChart.prototype = new WidgetBase()
WidgetSVGDonutChart.prototype.constructor = WidgetSVGDonutChart;
// Basic donut chart class
function WidgetSVGDonutChart() {
  // Defaults
  this.defaultColors = ["#f00", "#0f0", "#00f", "#cc0", "#0cc", "#f00", "#0f0", "#00f", "#cc0", "#0cc"];
  // Properties
  this.titleClass = "listtittle";
  this.animations = "none";
  this.values = [];
  this.labels = [];
  this.animationTime = 1500;
  this.margin = 20;
  this.innerRadious = 50;
  this.drawLabels = true;
  this.showTotal = false;
  this.colorTotal = "white";
  this.labelTotal = "Total";
  this.ShowInfoButton = true;
  this.labelsClass = "txt_fore_color amphitheater_label";
  this.defaultLabelSize = "12em";
  this.defaultFontSizeDesktop = "16em";
  this.SVGContainer = ".svg_container";
  // Members
  this.svgContainer = undefined;
  this.paper = undefined;
  // Events
  this.OnGraphClick = undefined;
  this.OnBeforeUpdate = undefined;
  // Create svg container after added
  this.OnAfterAdded = function () {
    this.svgContainer = $("#" + this.id).find(this.SVGContainer)[0];
    this.svg_width = $(this.svgContainer).width(),
    this.svg_height = $(this.svgContainer).height();
    if (this.svgContainer) {
      // Create container
      this.paper = Raphael(this.svgContainer, this.svg_width, this.svg_height);
      //this.paper.setViewBox(0, 0, _width, _height);
      this.paper.canvas.setAttribute('preserveAspectRatio', 'none');
      $(this.svgContainer).attr('width', this.svg_width).attr('height', this.svg_height);
      if (this.values.length > 0) {
        // RMS
        //this.Update();
      }

    } else {
      throw "Unable to find the SVG container!";
    }
  }
}

// Bind the events
WidgetSVGDonutChart.prototype.BindEvents = function (Object) {
  //var _that = this;
  //if (!Object) { return; }
  //Object.find(this.SVGContainer).each(function (Index, Element) {
  //  $(Element).click(function () {
  //    if (_that.OnGraphClick) {
  //      _that.OnGraphClick(_that, Element);
  //    }
  //  });
  //});
}

// Visually updates
WidgetSVGDonutChart.prototype.Update = function (Recalculate) {
  if (!this.paper) { return; }
  if (this.OnBeforeUpdate) {
    this.OnBeforeUpdate(this);
  }

  this.paper.clear();

  var _height = $(this.svgContainer).height(),
      _width = $(this.svgContainer).width();

  var _tabletScreen = _height + 50 > _width;

  var _center = {
    x: _tabletScreen ? _width / 2 : _width / 3,
    y: _tabletScreen ? _height / 3 + this.margin : _height / 2
  }

  var _sizes = {
    r: (_center.x > _center.y ? _center.y : _center.x) - this.margin,
    rin: this.innerRadious
  }

  var _options = {
    colors: this.defaultColors,
    drawLabels: this.drawLabels,
    hideZeros: this.hideZeros,
    fontSize: this.defaultFontSize,
    fontSizeDesktop: this.defaultFontSizeDesktop,
    fontFamily: this.defaultLabelsFontFamily,
    marginLeft: _center.x - _sizes.r,
    marginRight: _width - (_center.x - _sizes.r),
    tabletScreen: _tabletScreen,
    showTotal: this.showTotal,
    colorTotal: this.colorTotal,
    labelTotal: this.labelTotal,
    height: _height
  }

  var _txt = null;
  var _box = null;
  var _cnt_box = null;
  var _txt_box = null;

  this.paper.donutChart(_center.x, _center.y, _sizes.r, _sizes.rin, this.values, this.labels, _options);

  if (this.centerLabel) {
    _txt = this.paper.text(_center.x, _center.y, this.centerLabel.text).attr(this.centerLabel.attrs);
    _box = _txt.getBBox();
    _cnt_box = _box.height;
    _txt.translate(0, _cnt_box - (_cnt_box / 1.5));
  }

  if (this.bottomLeftLabel) {
    _txt = this.paper.text(0, $(this.svgContainer).height(), this.bottomLeftLabel.text).attr(this.bottomLeftLabel.attrs);
    _box = _txt.getBBox();
    _txt.translate(_box.width, -_box.height);
    _txt_box = -_box.height;
  }

  if (this.bottomRightLabel) {
    _txt = this.paper.text($(this.svgContainer).width(), $(this.svgContainer).height(), this.bottomRightLabel.text).attr(this.bottomRightLabel.attrs);
    _box = _txt.getBBox();
    _txt.translate(-_box.width, -_box.height);
  }

  if (this.centerLabelTitle) {
    if (this.centerLabelTitle.NLS) {
      this.centerLabelTitle.text = _Language.get(this.centerLabelTitle.NLS);
    }

    _txt = this.paper.text(_center.x, _center.y, this.centerLabelTitle.text).attr(this.centerLabelTitle.attrs);
    _txt.translate(0, -_txt.getBBox().height + (_txt.getBBox().height / 2));
  }

  if (this.bottomLeftLabelTitle) {
    _txt = this.paper.text(0, $(this.svgContainer).height(), this.bottomLeftLabelTitle.text).attr(this.bottomLeftLabelTitle.attrs);
    _box = _txt.getBBox();
    _txt.translate(_box.width, -_box.height + _txt_box - 5);
  }

  if (this.bottomRightLabelTitle) {
    _txt = this.paper.text($(this.svgContainer).width(), $(this.svgContainer).height(), this.bottomRightLabelTitle.text).attr(this.bottomRightLabelTitle.attrs);
    _box = _txt.getBBox();
    _txt.translate(-_box.width, -_box.height + _txt_box - 5);
  }

  this.Calculated = true;
}

WidgetHighChart.prototype = new WidgetBase()
WidgetHighChart.prototype.constructor = WidgetHighChart;

function WidgetHighChart() {

  // Defaults
  this.defaultColors = ["#f00", "#0f0", "#00f", "#cc0", "#0cc", "#f00", "#0f0", "#00f", "#cc0", "#0cc"];
  this.id;
  // Properties
  this.animations = "none";
  this.categories = [];
  this.lines = [];
  this.linesMaxValue = 0;
  this.linesMinValue = 0;
  this.Calculated = false;
  this.showLabels = false;
  this.firstime = true;
  this.labelsClass = '';
  this.LabelSpace = 5;
  this.margin = 5;
  this.topmargin = 5;
  this.lineMaxIncrement = 0;
  this.valueType = 1;  // 0: none - 1: percent - 2: monetary - 3: decimal ,...
  this.animationTime = 1500;
  this.labelsClass = "txt_fore_color amphitheater_label";
  this.defaultLabelSize = "12em";
  this.SVGContainer = ".svg_container";
  this.OnBeforeUpdate = undefined;
  // Members
  this.svgContainer = undefined;
  this.paper = undefined;
  // Events
  this.OnGraphClick = undefined;
  // Create svg container after added
  this.OnAfterAdded = function () {
    this.svgContainer = $("#" + this.id).find(this.SVGContainer)[0];
    this.svg_width = $(this.svgContainer).width();
    this.svg_height = $(this.svgContainer).height();
    //this.svg_width = $("#" + this.id).width();
    //this.svg_height = $("#" + this.id).height();
    if (this.svgContainer) {
      // Create container
      this.paper = Raphael(this.svgContainer, this.svg_width, this.svg_height);
      //this.paper.setViewBox(0, 0, _width, _height);
      this.paper.canvas.setAttribute('preserveAspectRatio', 'none');
      $(this.svgContainer).attr('width', this.svg_width).attr('height', this.svg_height);
      if (this.lines.length > 0) {
        this.Update();
      }

      this.Update();

    } else {
      throw "Unable to find the SVG container!";
    }
  }
}

WidgetHighChart.prototype.Update = function () {
  if (this.firstime) {


    var $ctrl1 = $("#" + this.id).find(".svg_container").empty();
    //$ctrl1.css("width", "275px");
    $ctrl1.css("height", "240px");

    $ctrl1.highcharts({
      chart: {
        type: 'areaspline',
        backgroundColor: '#232C30',
        animation: Highcharts.svg, // don't animate in old IE
        marginRight: 10,
        events: {
          load: function () {

            // set up the updating of the chart each second
            var series = this.series[0];
            setInterval(function () {
              var x = (new Date()).getTime(), // current time
                y = _Manager.MetersManager.Meters[1] ? _Manager.MetersManager.Meters[1].Items[0].Ranges["TDA"].HourAvg : 0;

              var _point_item = {
                x: x,
                y: y,
                dataLabels: {
                  enabled: true,
                  rotation: 0,
                  color: '#FFFFFF',
                  align: 'center',
                  formatter: function () {

                    return FormatString(this.point.y, EN_FORMATS.F_MONEY);
                  },
                  y: 0 // 10 pixels down from the top
                }
              }


              series.addPoint(_point_item, true, true, true);
            }, 5000);
          }
        }
      },
      title: {
        text: ''
      },
      xAxis: {
        lineWidth: 0,
        minorGridLineWidth: 0,
        lineColor: 'transparent',
        labels: {
          enabled: false
        },
        minorTickLength: 0,
        tickLength: 0
      },
      yAxis: {
        lineWidth: 0,
        minorGridLineWidth: 0,
        gridLineWidth: 0,
        lineColor: 'transparent',
        labels: {
          enabled: false
        },
        minorTickLength: 0,
        tickLength: 0,
        title: ''
      },
      legend: {
        enabled: false
      },
      exporting: {
        enabled: false
      },
      tooltip: {
        enabled: false
      },
      series: [{
        color: '#FF0000',
        data: (function () {
          // generate an array of random data
          var data = [],
              time = (new Date()).getTime(),
              i;

          for (i = -7; i <= 0; i += 1) {
            data.push({
              x: time + i * 1000,
              y: null,
              dataLabels: {
                enabled: true,
                rotation: 0,
                color: '#FFFFFF',
                align: 'center',
                formatter: function () {
                  return FormatString(this.point.y, EN_FORMATS.F_MONEY);
                },
                y: 0
              }
            });
          }
          return data;
        }())
      }]

    });
    this.firstime = false;
  }

  if (this.OnBeforeUpdate) {
    this.OnBeforeUpdate(this);
  }

}