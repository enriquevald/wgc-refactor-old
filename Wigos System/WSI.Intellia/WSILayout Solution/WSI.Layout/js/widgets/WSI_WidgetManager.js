﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WSI_WidgetManager.js
// 
//   DESCRIPTION: Contains class to manage the dashboard widgets.
// 
//        AUTHOR: Ramón Monclús
// 
// CREATION DATE: JUN-2015
// 
//------------------------------------------------------------------------------

/*
  Requires:
    jQuery
    jQuery-ui
    Raphael
    raphael.extra
    GridStack
*/
var m_dashboard_result_timeout;
var m_dashboard_click_iterations = 0;

// Returns the class name of the argument or undefined if it's not a valid JavaScript object.
function getObjectClass(obj) {
  if (obj && obj.constructor && obj.constructor.toString) {
    var arr = obj.constructor.toString().match(
            /function\s*(\w+)/);

    if (arr && arr.length == 2) {
      return arr[1];
    }
  }

  return undefined;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
Options = {
id: "string",
parentId: "#string",
leftMargin: %/px,
width: N,
cell_height: N,
resizable: { autoHide: true/false, handles: 'e, se, s, sw, w'}
}
*/
// Class to manage te widgets and the dashboard
function WidgetManager(Options) {

  if (!Options) {
    throw "Options must be indicated!";
    return null;
  }

  this.CurrentOptions = Options;

  this.CurrentOptions.draggable = { handle: '.WidgetDraggablePart' }

  this.Container = null;
  this.Widgets = [];
  this.WidgetIndex = [];

  this.CurrentOptions.parentId = (this.CurrentOptions.parentId) ? this.CurrentOptions.parentId : "body";

  $("#" + this.CurrentOptions.parentId).append(
    $('<div id="' + this.CurrentOptions.id + '" class="grid-stack" data-gs-width="' + this.CurrentOptions.width + '" style="margin-left: ' + this.CurrentOptions.leftMargin + '"></div>')
  );
  this.Container = $("#" + this.CurrentOptions.id).gridstack(this.CurrentOptions);

  $("#" + this.CurrentOptions.id).on('dragstart', function (event, ui) {
    var grid = this;
    var element = event.target;
    //WSILogger.Write("Dashboard dragstart");
    return m_dashboard_drag_mode_on;
  });

  $("#" + this.CurrentOptions.id).on('drag', function (event, ui) {
    var grid = this;
    var element = event.target;
    $('#div_report_chart_reports').scrollTop(ui.position.top - 115);
    //WSILogger.Write("Dashboard dragstart");
    return m_dashboard_drag_mode_on;
  });
  
  $("#" + this.CurrentOptions.id).on('mousedown', function (event) {
    var element = event.target;
    //WSILogger.Write("Dashboard mousedown");
    if (!m_dashboard_drag_mode_on) {
      if ($(element).closest(".WidgetDraggablePart").length > 0) {
        m_dashboard_click = true;

        m_dashboard_result_timeout = setInterval(function () {

          if (m_dashboard_click_iterations >= 1) {
            m_dashboard_drag_mode_on = true;
            WSILogger.Write("drag mode on");
            $("#div_report_chart_reports").find("div[id^='widget'].ui-draggable").addClass("WigdetDragMode");
            _Manager.WidgetManager.SetMove(true);

          } else {
            m_dashboard_drag_mode_on = false;
            _Manager.WidgetManager.SetMove(false);

            $("#div_report_chart_reports").find("div[id^='widget'].ui-draggable").removeClass("WigdetDragMode");
          }
          m_dashboard_click_iterations++;

        }, 1000);
      }
    }
  });

  $("#" + this.CurrentOptions.id).on('change', function (event, nodes, wgt) {
    for (var _wid in _Manager.WidgetManager.Widgets) {
      var _widget = _Manager.WidgetManager.Widgets[_wid];
      if (_widget.id == 'widgetChart2') {
        _widget.Update();
      }
    }
  });

  $("#" + this.CurrentOptions.id).on('adjust', function (event, wgt) {
    $('#div_report_chart_reports').animate({ scrollTop: wgt.posY-115 },400);
  });

  $("#" + this.CurrentOptions.id).on('mouseup', function (event) {
    var element = event.target;
    //WSILogger.Write("Dashboard mouseup");
    if (m_dashboard_drag_mode_on) {
      WSILogger.Write("Dashboard mouseup - drag mode on");
      m_config_has_changed = true;
    } else {
      clearInterval(m_dashboard_result_timeout);
      m_dashboard_click = false;
      m_dashboard_click_iterations = 0;
      m_dashboard_drag_mode_on = false;
      _Manager.WidgetManager.SetMove(false);
    }
  });

  //// Add events to enable auto-save
  //$('.grid-stack').on('onchange', function (items) {
  //  m_config_has_changed = true;
  //});
}

/*
Widget = Template of the widget (HTML)
Options = {
id: "string",
x: N,
y: N,
width: N,
height: N
}
OnWidgetInsert = callback event after widget inserted
*/
// Adds a Widget to the Grid
WidgetManager.prototype.AddWidget = function (Widget, Options) {

  if (!Widget || Widget.id === "") { return; }

  var _grid = this.Container.data('gridstack');
  if (!_grid) { throw "AddWidget - Cannot acces gridstack!"; }
  var _new_widget = undefined;

  if (typeof Widget === "object" && Widget.Prepare) {
    _new_widget = _grid.add_widget($("<div id='" + Widget.id + "_widget'><div class='grid-stack-item-content fondoContraste'>" + Widget.Prepare() + "</div></div>"), Options.x, Options.y, Options.width, Options.height);
    Options.id = Widget.id;
  } else {
    _new_widget = _grid.add_widget($("<div id='" + Options.id + "_widget'><div class='grid-stack-item-content fondoContraste'>" + Widget + "</div></div>"), Options.x, Options.y, Options.width, Options.height);
  }

  // ScrollBar fix
  if (!Widget.hasScroll) {
    _new_widget.find(".grid-stack-item-content").addClass("widgetNoScroll");
  }
    //DMT 1/6/2016
  else
    _new_widget.find(".listcontent").addClass("widgetScroll");
  //

  if (Options.canResize === undefined) { Options.canResize = true; }
  if (Options.canMove === undefined) { Options.canMove = true; }
  _grid.resizable(_new_widget, Options.canResize);
  _grid.movable(_new_widget, Options.canMove);
  Widget.resizable = m_dashboard_drag_mode_on;
  Widget.movable = m_dashboard_drag_mode_on;

  this.Widgets.push(Widget);
  this.WidgetIndex[Options.id + "_widget"] = this.Widgets.length - 1;

  if (Widget.Added) {
    // RMS : Info on after added
    WSILogger.Write("Added: " + Options.id);
    Widget.Added(Widget, _new_widget);
  }

  if (Widget.BindEvents) {
    Widget.BindEvents(this.Container.find("#" + Widget.id));
  }

}

// Obtains properties of a Widget
WidgetManager.prototype.GetProperties = function (Widget) {
  function getProperties(o) {
    var results = [];
    function properties(obj) {
      var props, i;
      if (obj == null) {
        return results;
      }
      if (typeof obj !== 'object' && typeof obj !== 'function') {
        return properties(obj.constructor.prototype);
      }
      props = Object.getOwnPropertyNames(obj);
      i = props.length;
      while (i--) {
        if (! ~results.indexOf(props[i])) {
          if (typeof obj[props[i]] !== 'object' && typeof obj[props[i]] !== 'function') {
            results.push(props[i]);
          } else {

          }
        }
      }
      return properties(Object.getPrototypeOf(obj));
    }
    return properties(o);
  }
  var _result = {};
  var _props = getProperties(Widget);
  var _prop = undefined;
  for (var _pi in _props) {
    _prop = _props[_pi];
    _result[_prop] = Widget[_prop];
  }
  return _result;
}

// Obtain the list of the current dashboard widgets to save it
WidgetManager.prototype.GetDashboard = function () {
  var that = this;

  var res = _.map(this.Container.find('.grid-stack-item'), function (el) {
    el = $(el);
    var node = el.data('_gridstack_node');
    if (node) {
      var _widget = that.Widgets[that.WidgetIndex[el.attr('id')]];
      var _id = _widget.id;
      var _type_name = getObjectClass(_widget);
      return {
        id: _id,
        x: node.x,
        y: node.y,
        width: node.width,
        height: node.height,
        type: _type_name,
        canResize: m_dashboard_drag_mode_on,
        canMove: m_dashboard_drag_mode_on
        // TODO: Evaluate if more properties are needed to keep some aspect configuration about the widget
      };
    }
  });

  return res;
}

// Creates the dashboard from a snapshot of it
WidgetManager.prototype.SetDashboard = function (SavedDashboard) {

  var _widget = undefined;
  if (SavedDashboard) {
    for (var _wid in SavedDashboard) {
      _widget = SavedDashboard[_wid];
      if (_widget != null) {
        this.AddWidget(m_widgets_list[_widget.id], _widget);
      }
    }
    _Manager.WidgetManager.SetMove(false);
  }

  //if (!SavedDashboard || SavedDashboard == "") {
  //  SavedDashboard = m_default_dashboard;
  //}
  //var _widgets = JSON.parse(SavedDashboard);
  //var _widget = undefined;
  //if (_widgets) {
  //  for (var _wid in _widgets) {
  //    _widget = _widgets[_wid];
  //    this.AddWidget(m_widgets_list[_widget.id], _widget);
  //  }
  //}
}

// Update all the widgets of the dashboard
WidgetManager.prototype.UpdateWidgets = function () {

  if (!_Manager.Terminals.Updating) {
    //while (m_activity_process) {
    //  // Wait until activity processed
    //}

    var _widget = undefined;
    for (var _wid in this.Widgets) {
      _widget = this.Widgets[_wid];
      if (m_dashboard_click) {
        WSILogger.Write("UpdateWidgets break");
        break;
      }
      // Update date_hour indicator for activity
      if (_widget.properties["DATE_HOUR"]) {
        _widget.SetPropertyValue("DATE_HOUR", GetHourNowRange());
      }

      // RMS : Info on update
      //WSILogger.Write("Updating: " + _widget.id);
      _widget.Update();

      if (_widget.BindEvents) {
        _widget.BindEvents(this.Container.find("#" + _widget.id));
      }
    }
  }

}

// Returns a widget by Id
WidgetManager.prototype.GetWidget = function (WidgetId) {
  return this.Widgets[this.WidgetIndex[WidgetId + "_widget"]];
}

WidgetManager.prototype.UpdateWidgetsList = function () {

  for (_idx_widget_list in this.Widgets) {
    if (m_dashboard_click) {
      WSILogger.Write("UpdateWidgets break");
      break;
    }
    if (this.Widgets[_idx_widget_list].OnRealTimeComplete) {
      this.Widgets[_idx_widget_list].OnRealTimeComplete();
      if (this.Widgets[_idx_widget_list].BindEvents) {
        this.Widgets[_idx_widget_list].BindEvents(this.Container.find("#" + this.Widgets[_idx_widget_list].id));
      }

    }
  }
}

WidgetManager.prototype.SetMove = function (move) {

  var _grid = this.Container.data('gridstack');
  if (!_grid) { throw "SetMove - Cannot acces gridstack!"; }

  if (move)
  {
    _grid.enable();
  } else
  {
    _grid.disable();
  }
 }


function UpdateWidgetHighRoller() {
  /// <summary>
  /// Update the list of high rollers
  /// </summary>
  //var _current_list = widget_highrollers_list.Rows;

    var _current_list = [];
        
  // Get HighRollers
  for (var _account_id in _Manager.Terminals.ByAccount) {
    var _term = _Manager.Terminals.Items[_Manager.Terminals.ByAccount[_account_id]].Properties;
    if (((+_term.al_played_won_flags & 16) == 16 || ((+_term.al_played_won_flags & 32) == 32))) {
      var _cl_item;

      if (_current_list[_account_id]) {
        _current_list[_account_id].timer = 0;
        if (_term.external_id != _current_list[_account_id].terminal_id) {
          _current_list[_account_id].coin_in = +_term.play_session_total_played;
          _current_list[_account_id].bet_avg = +_term.play_session_bet_averag;
          _current_list[_account_id].terminal_id = _term.external_id;
          _current_list[_account_id].lo_id = _term.lo_id;
        }
      }
      else {
        _cl_item = {
          terminal_id: _term.external_id,
          user_name: _term.player_name,
          player_first_name: _term.player_first_name,
          player_last_name1: _term.player_last_name1,
          player_last_name2: _term.player_last_name2,
          coin_in: +_term.play_session_total_played,
          bet_avg: +_term.play_session_bet_average,
          player_level_name: _term.player_level_name,
          lo_id: _term.lo_id,
          timer: 0
        };
        _current_list[_account_id] = _cl_item;
      }
    }
  }
  widget_highrollers_list.Rows = _current_list;

  //Limpieza de la lista para borrar los que lleven más de media hora
  for (var _idx_hr in widget_highrollers_list.Rows) {
    var _hr_item = widget_highrollers_list.Rows[_idx_hr];
    //Borrar si
    var _user_playing_flag = false;

    if (_Manager.Terminals.ByAccount[_idx_hr]) {
      _user_playing_flag = true;
    }

    //Es registrado y lleva más de media hora
    //Es anónimo y ya no juega
    if (_hr_item.timer >= 120000 && !_user_playing_flag) {
      widget_highrollers_list.Rows.splice(_idx_hr, 1);
    }

    if (_hr_item.player_level_name == _Language.get("NLS_ANONYMOUS") && _user_playing_flag == false) {
      widget_highrollers_list.Rows.splice(_idx_hr, 1);
    }
  }
}

function UpdateWidgetMaxLvlPlayers() {
  /// <summary>
  /// Update the list of the widget Max Level Players
  /// </summary>

  var _current_list = [];
  var _counter = 0;
  var _max_player_level = 0;

  for (var i = 0; i < _Manager.Ranges.length; i++) {
    if (parseInt(_Manager.Ranges[i].Range[0]) > _max_player_level)
      _max_player_level = parseInt(_Manager.Ranges[i].Range[0]);
  }

  if (_max_player_level > 4) _max_player_level = 4;

  for (var _account_id in _Manager.Terminals.ByAccount) {
    var _term = _Manager.Terminals.Items[_Manager.Terminals.ByAccount[_account_id]].Properties;

    if (_term.player_level == _max_player_level) {
      var _cl_item;

        _cl_item = {
          terminal_id: _term.external_id,
          user_name: _term.player_name,
          player_first_name: _term.player_first_name,
          player_last_name1: _term.player_last_name1,
          player_last_name2: _term.player_last_name2,
          coin_in: +_term.play_session_total_played,
          bet_avg: +_term.play_session_bet_average,
          player_level_name: _term.player_level_name,
          lo_id: _term.lo_id,
          term_provider: _term.provider_name,
          term_name: _term.name,
          timer: 0
        };
        if (_counter > 4)
          continue;
        else {
          _current_list[_account_id] = _cl_item;
          _counter++;
        }
      
    }
  }
  widget_players_max_level.Rows = _current_list;
}

function MaxLevelShowPlayerInfo(e) {
  var _lo_id = $(e).parent().children().last().text();
  var _terminal_id = _Manager.Terminals.Items[_lo_id];

  m_terminal_id_selected = _terminal_id.Properties.lo_id;
  UpdatePopupTerminal(null, _terminal_id);
  OpenInfoPanel();

}
