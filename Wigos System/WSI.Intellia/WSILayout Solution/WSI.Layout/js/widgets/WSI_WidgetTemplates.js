﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WSI_WidgetTemplates.js
// 
//   DESCRIPTION: Contains the templates for the widgets.
// 
//        AUTHOR: Ramón Monclús
// 
// CREATION DATE: JUN-2015
// 
//------------------------------------------------------------------------------

var m_widgets_templates = {

  ListItemType1: '<div class="item">'
                 + '  <div class="itemrectp">'
                 + '    <div class="first-column">'
                 + '      <div class="first-row">'
                 + '        <div class="itemmachineleft itemwrap" data-property-name="terminal">TERMINAL</div>'
                 + '      </div>'
                 + '      <div class="second-row">'
                 + '        <div class="itemnameleft txt_fore_color itemwrap" data-property-name="name"></div>'
                 + '      </div>'
                 + '      <div class="third-row">'
                 + '        <div class="itemnameleft txt_fore_color itemwrap" data-property-name="last-name"></div>'
                 + '      </div>'
                 + '    </div>'
                 + '    <div class="second-column">'
                 + '      <div data-property-name="identifier" style="display: none;"></div>'
                 + '      <div class="itemtracerect"></div>'
                 + '    </div>'
                 + '    <div class="third-column">'
                 + '      <div class="first-row">'
                 + '      </div>'
                 + '      <div class="second-row">'
                 + '      </div>'
                 + '      <div class="third-row">'
                 + '        <div class="itemamountrightbottom txt_fore_color_orange_dark" data-property-name="amount" data-property-type="1"></div>'
                 + '      </div>'
                 + '    </div>'
                 + '  </div>'
                 + '</div>',

  ListItemType1OtherFloor: '<div class="item">'
               + '  <div class="itemrectp">'
               + '    <div class="first-column">'
               + '      <div class="first-row">'
               + '        <div class="itemmachineleft itemwrap" data-property-name="terminal">TERMINAL</div>'
               + '      </div>'
               + '      <div class="second-row">'
               + '        <div class="itemnameleft txt_fore_color itemwrap" data-property-name="name"></div>'
               + '      </div>'
               + '      <div class="third-row">'
               + '        <div class="itemnameleft txt_fore_color itemwrap" data-property-name="last-name"></div>'
               + '      </div>'
               + '    </div>'
               //+ '    <div class="second-column">'
              //  + '      <div data-property-name="identifier" style="display: none;"></div>'
              // + '      <div class="itemtracerect"></div>'
               //+ '    </div>'
               + '    <div class="third-column" style="margin-right: 5px;">'
               + '      <div class="first-row">'
               + '      </div>'
               + '      <div class="second-row">'
               + '      </div>'
               + '      <div class="third-row">'
               + '        <div class="itemamountrightbottom txt_fore_color_orange_dark" data-property-name="amount" data-property-type="1"></div>'
               + '      </div>'
               + '    </div>'
               + '</div>',

  ListItemType2: '<div class="item">'
                 + '   <div class="itemrectp">'
                 + '    <div class="first-column">'
                 + '      <div class="first-row">'
                 + '        <div class="itemmachineleft itemwrap" data-property-name="terminal">TERMINAL</div>'
                 + '      </div>'
                 + '      <div class="second-row">'
                 + '        <div class="itemnameleft txt_fore_color itemwrap" data-property-name="name"></div>'
                 + '      </div>'
                 + '      <div class="third-row">'
                 + '        <div class="itemnameleft txt_fore_color itemwrap" data-property-name="last-name"></div>'
                 + '      </div>'
                 + '    </div>'
                 + '    <div class="second-column">'
                 + '      <div data-property-name="identifier" style="display: none;"></div>'
                 + '      <div class="itemtracerect"></div>'
                 + '    </div>'
                 + '    <div class="third-column">'
                 + '      <div class="first-row">'
                 + '      </div>'
                 + '      <div class="second-row">'
                 + '      </div>'
                 + '      <div class="third-row">'
                 + '        <div class="itemamountrightbottom txt_fore_color_orange_dark" data-property-name="amount" data-property-type="1"></div>'
                 + '      </div>'
                 + '    </div>'
                 + '  </div>'
                 + '</div>',

  VipListItem: '<div class="item">'
             + '  <div class="itemrectt">'
             + '    <div class="first-column">'
             + '      <div class="first-row">'
             + '        <div class="itemmachineleft itemwrap" data-property-name="terminal">TERMINAL</div>'
             + '      </div>'
             + '      <div class="second-row">'
             + '        <div class="itemnameleft txt_fore_color itemwrap" data-property-name="name"></div>'
             + '      </div>'
             + '      <div class="third-row">'
             + '        <div class="itemnameleft txt_fore_color itemwrap" data-property-name="last-name"></div>'
             + '      </div>'
             + '    </div>'
             + '    <div class="second-column">'
             + '      <div data-property-name="identifier" style="display: none;"></div>'
             + '      <div class="itemtracerect"></div>'
             + '    </div>'
             + '    <div class="third-column">'
            
             + '    </div>'
             + '  </div>'
             + '</div>',

  VipListItemOtherFloor: '<div class="item">'
           + '  <div class="itemrectp">'
                 + '    <div class="first-column">'
                 + '      <div class="first-row">'
                 + '        <div class="itemmachineleft itemwrap" data-property-name="terminal">TERMINAL</div>'
                 + '      </div>'
                 + '      <div class="second-row">'
                 + '        <div class="itemnameleft txt_fore_color itemwrap" data-property-name="name"></div>'
                 + '      </div>'
                 + '      <div class="third-row">'
                 + '        <div class="itemnameleft txt_fore_color itemwrap" data-property-name="last-name"></div>'
                 + '      </div>'
                 + '    </div>'
                 //+ '    <div class="second-column">'
                 //+ '      <div data-property-name="identifier" style="display: none;"></div>'
                 //+ '      <div class="itemtracerect"></div>'
                 //+ '    </div>'
                 + '    <div class="third-column">'
                 + '      <div class="first-row">'
                 + '      </div>'
                 + '      <div class="second-row">'
                 + '      </div>'
                 + '      <div class="third-row">'
                 //+ '        <div class="itemamountrightbottom txt_fore_color_orange_dark" data-property-name="amount" data-property-type="1"></div>'
                 + '      </div>'
                 + '    </div>'
                 + '  </div>'
           + '</div>',

  RunnerListItem: '<div class="item">'
                + '  <div class="two-columns-first-column">'
                + '     <div class="two-columns-first-row">'
                + '       <div class="itemrectt-runners">'
                + '         <div class="itemnameleft txt_fore_color" data-property-name="device"></div>'
                + '         <div class="itemnameright2 txt_fore_color" data-property-name="name"></div>'
                + '       </div>'
                + '     </div>'
                + '     <div class="two-columns-second-row">'
                + '       <div class="itemrolescontainer"></div>'
                + '     </div>'
                + '  </div>'
                + '  <div class="two-columns-second-column">'
                + '     <div class="itemrolesdevice"></div>'
                + '  </div>'
                + '</div>',

  Indicator: {
    draggable: '<div class="">'
              + '<div class="listdash"></div>'
              + '<div class="listtittle txt_fore_color"></div>'
			        + '<div class="news_today txt_fore_color_gray" data-property-name="name"></div>'
			        + '<div class="news_today_value txt_fore_color_gray" data-property-name="amount" data-property-type="1"></div>'
              + '<div class="news_ico_up"></div>'
			        + '<div class="news_percent txt_fore_color_green" data-property-name="percent" data-property-type="2"></div>'
              + '</div>',
    static: ''
  },

  Indicator2: {
    draggable: '<div class="">'
              + '<div class="listdash"></div>'
              + '<div class="listtittle txt_fore_color"></div>'
			        + '<div class="news_today txt_fore_color_gray" data-property-name="name"></div>'
			        + '<div class="news_today_value txt_fore_color_gray" data-property-name="amount" data-property-type="1"></div>'
              + '<div class="news_ico_up"></div>'
			        + '<div class="news_percent txt_fore_color_green" data-property-name="percent" data-property-type="1"></div>'
              + '</div>',
    static: ''
  },

  IndicatorMonetary: {
    draggable: '<div class="">'
              + '<div class="listdash"></div>'
              + '<div class="listtittle txt_fore_color"></div>'
              //+ '<div class="indicator_icon news_ico_up"></div>'
              + ' <div class="metersDiv">'
			        + ' <div class="netwinDiv" ><div class="labelWidgetTitle metersTitleDiv white-letters">NETWIN</div>'
              + '<div class="labelWidgetDescription white-letters" data-nls="NLS_WIDGET_OCC_TODAY" >HOY</div><div style="text-align: right;" class="labelWidgetDescription white-letters" data-nls="NLS_WIDGET_OCC_YESTERDAY">AYER</div>'
              + '<div id="value" class="labelWidgetLeft labelWidget"> <div class="labelWidgetText" data-property-name="value" data-property-type="1"></div></div><div class="labelWidgetSeparator">&nbsp;</div>'
              + '<div id="value2" class="labelWidgetRight labelWidget labelWidgetGray"><div class="labelWidgetText" data-property-name="value2" data-property-type="1"></div></div></div>'
               + '<div class="coinInDiv"><div class="labelWidgetTitle metersTitleDiv white-letters">COIN IN</div>'
               + '<div class="labelWidgetDescription white-letters" data-nls="NLS_WIDGET_OCC_TODAY">HOY</div><div  style="text-align: right;" class="labelWidgetDescription white-letters" data-nls="NLS_WIDGET_OCC_YESTERDAY">AYER</div>'
               + '<div id="value3" class="labelWidgetLeft labelWidget"><div class="labelWidgetText" data-property-name="value3" data-property-type="1"></div></div><div class="labelWidgetSeparator">&nbsp;</div>'
               + '<div id="value4" class="labelWidgetRight labelWidget labelWidgetGray"><div class="labelWidgetText" data-property-name="value4" data-property-type="1"></div></div></div>'
               + '</div>'
              + '</div>',
    static: ''
  },

  HProgress: {
    draggable: '<div class="">'
              + '<div class="listdash"></div>'
              + '<div class="listtittle txt_fore_color"></div>'
              + '<div class="l_task task_bars bar_progress"></div>'
              + '</div>',
    static: ''
  },

  Chart: {
    draggable: '<div id="widgetHighCharts1_chart"></div>',
    static: ''
  },

  SVGChart: {
    draggable: '<div class="listdash"></div>'
			        //+ '<div class="icometer" id="icometer"><img src="./design/img/ico_meter.png" width="25" height="25" /></div>'
			        + '<div class="listtittle txt_fore_color"></div>'
              + '<div class="svg_container"></div>'
			        + '<div class="datehour txt_fore_color " data-property-name="DATE_HOUR">00:00am</div>',
    static: ''
  },

  Amphitheater: {
    draggable: '<div class="listdash"></div>'
			        //+ '<div class="icometer" id="icometer"><img src="./design/img/ico_meter.png" width="25" height="25" /></div>'
			        + '<div class="listtittle txt_fore_color"></div>'
              + '<div class="svg_container"></div>'
			        + '<div class="datehour txt_fore_color " data-property-name="DATE_HOUR">00:00am</div>',
    //			        + '<div class="METRIC TDA txt_fore_color">TDA</div>'
    //			        + '<div class="METRIC SLW txt_fore_color">SLW</div>'
    //			        + '<div class="METRIC MTD txt_fore_color">MTD</div>'
    //			        + '<div class="METRIC YTD txt_fore_color">YTD</div>'
    //			        + '<div class="METRIC_VALUE TDA_VALUE txt_fore_color" data-property-name="TDA_VALUE"></div>'
    //			        + '<div class="METRIC_VALUE SLW_VALUE txt_fore_color" data-property-name="SLW_VALUE"></div>'
    //			        + '<div class="METRIC_VALUE MTD_VALUE txt_fore_color" data-property-name="MTD_VALUE"></div>'
    //			        + '<div class="METRIC_VALUE YTD_VALUE txt_fore_color" data-property-name="YTD_VALUE"></div>',
    static: ''
  },

  DonutChart: {
    draggable: '<div class="listdash"></div>'
			        + '<div class="listtittle listtittle-donut txt_fore_color"></div>'
              + '<div class="svg_container"></div>'
			        + '<div class="datehour txt_fore_color " data-property-name="DATE_HOUR">00:00am</div>'
              + '<div class=""></div>',
    static: ''
  },

  Tasks: {
    draggable: '<div class="">'
                + '<div class="listdash"></div>'
                + '<div class="listtittle txt_fore_color"></div>'
                + '<div class="task-top-info">'
                + '<div class="l_task lbl_task_technical txt_fore_color" data-nls="NLS_WIDGET_TASKS_TECHNICAL">TECHNICAL</div>'
                + '<div class="l_task task_bars task_bar_technical">'
                //+ ' <div class="task_bar_lbl_value txt_fore_color">1024</div>'
                + '</div>'
                + '<div class="l_task lbl_task_players txt_fore_color" data-nls="NLS_WIDGET_TASKS_PLAYERS">PLAYERS</div>'
                + '<div class="l_task task_bars task_bar_players">'
                //+ ' <div class="task_bar_lbl_value txt_fore_color">25</div>'
                + '</div>'
                + '<div class="l_task lbl_task_site txt_fore_color" data-nls="NLS_WIDGET_TASKS_CUSTOM">CUSTOM</div>'
                + '<div class="l_task task_bars task_bar_site">'
                //+ ' <div class="task_bar_lbl_value txt_fore_color">666</div>'
                + '</div>'
                + '</div>'
                + '<div class="task-bottom-info">'
                + '<ul class="l_task_list">'
                + '<li>'
                + '<div class="legend_task legend_task_pending">'
                + ' <div class="legend_lbl_value_task txt_fore_color" data-property-name="TASKS_PENDING"></div>'
                + ' <div class="legend_lbl_task txt_fore_color" data-nls="NLS_WIDGET_TASKS_PENDING">PENDING</div>'
                + '</div>'
                + '</li>'
                + '<li>'
                + '<div class="legend_task legend_task_assigned">'
                + ' <div class="legend_lbl_value_task txt_fore_color" data-property-name="TASKS_ASSIGNED"></div>'
                + ' <div class="legend_lbl_task txt_fore_color" data-nls="NLS_WIDGET_TASKS_ASSIGNED">ASSIGNED</div>'
                 + '</div>'
                + '</li>'
                + '<li>'
                 + '<div class="legend_task legend_task_solved">'
                + ' <div class="legend_lbl_value_task txt_fore_color" data-property-name="TASKS_SOLVED"></div>'
                + ' <div class="legend_lbl_task txt_fore_color" data-nls="NLS_WIDGET_TASKS_SOLVED">SOLVED</div>'
                + '</div>'
                + '</li>'
                + '</ul>'
                + '</div>'
                + '</div>',
    static: ''
  },

  Timer: {
    draggable: '<div class="">'
			        + '<div class="clock_day txt_fore_color" data-property-name="today">- - -</div>'
			        + '<div class="clock_hour txt_fore_color_gray" data-property-name="hour">00:00 am</div>'
		          + '</div>',
    static: ''
  },

  ColumnsContainer: {
    draggable: '<div class="">'
              + ' <div class="listdash"></div>'
              + ' <div class="listtittle txt_fore_color"></div>'
              + ' <div class="columnscontent"></div>'
              + ' <div class="task_last_7_max_lbl txt_fore_color">'
              + '  <span data-property-name="label"></span>'
              + '  <div class="task_last_7_max_day_lbl txt_fore_color_yellow" data-property-name="date"></div>'
              + '  <div class="task_last_7_max_hour_lbl txt_fore_color_yellow" data-property-name="hour"></div>'
              + '  <div class="task_last_7_max_value txt_fore_color_green" data-property-name="total_value"></div>'
              + ' </div>'
		          + '</div>',
    static: ''
  },

  ColumnItem: '<div class="item column7Item">'
              + ' <div class="task_last_7_char_bg"></div>'
			        + ' <div class="task_last_7_day txt_fore_color" data-property-name="name"></div>'
			        + ' <div class="task_last_7_value txt_fore_color" data-property-name="value"></div>'
              + '</div>',

  DualCircle: {
    draggable: '<div class="">'
              + ' <div class="listdash"></div>'
              + ' <div class="listtittle txt_fore_color"></div>'
              + ' <div class="container_comparation">'
              + ' <div class="comparation_char1_bg">'
              + '  <div class="comparation_char1_value txt_fore_color" data-property-name="left_desc"></div>'
              + '  <div class="comparation_char1_txt txt_fore_color" data-property-name="left_value"></div>'
              + ' </div>'
              + ' <div class="comparation_char2_bg">'
              + '  <div class="comparation_char2_value txt_fore_color" data-property-name="right_desc"></div>'
              + '  <div class="comparation_char2_txt txt_fore_color" data-property-name="right_value"></div>'
              + ' </div>'
              + ' </div>'
              + ' <div class="comparation_left_bottom_value txt_fore_color_gray" data-property-name="right_complementary_value"></div>'
              + ' <div class="comparation_right_top_txt txt_fore_color_gray" data-property-name="right_complementary_desc"></div>'
              + ' <div class="comparation_right_bottom_value txt_fore_color_yellow" data-property-name="left_complementary_value"></div>'
              + ' <div class="comparation_left_bottom_txt txt_fore_color_yellow" data-property-name="left_complementary_desc"></div>',
    static: ''
  },

  // -------------------------------

  MeterAverageIndicator: {
    draggable: '<div class="">'
              + '<div class="listdash"></div>'
              + '<div class="listtittle txt_fore_color"></div>'
              //+ '<div class="icometer" id="icometer"><img src="./design/img/ico_meter.png" width="25" height="25" /></div>'
			        + '<div class="news_today txt_fore_color_gray" data-property-range="TDA" data-property-meter="0" data-property-name="average"></div>'
			        + '<div class="news_today_value txt_fore_color_gray" data-property-range="NOW" data-property-meter="0" data-property-name="number"></div>'
              //+ '<div class="news_ico_up"><img src="./design/img/Ico_Up.png" /></div>'
			        + '<div class="news_percent txt_fore_color_green" data-property-range="NOW" data-property-meter="0" data-property-name="average" data-property-type="2"></div>'
              + '</div>',
    static: ''
  },


  // -------------------------------

  MeterIndicatorNew: {
    draggable: '<div class="">'
              + '<div class="listdash"></div>'
              + '<div class="listtittle txt_fore_color"></div>'
              //+ '<div class="icometer" id="icometer"><img src="./design/img/ico_meter.png" width="25" height="25" /></div>'
              + '<div class="news_today txt_fore_color_gray" data-meter-id="0" data-meter-range="TDA" data-meter-value="range"></div>'
              + '<div class="news_today_value txt_fore_color_gray" data-meter-id="0" data-meter-range="NOW" data-meter-value="havg" data-property-type="2"></div>'
              + '<div class="news_ico_up"></div>'
			        + '<div class="news_percent txt_fore_color_green" data-meter-id="0" data-meter-range="TDA" data-meter-value="havg" data-property-type="2"></div>'
              + '</div>',
    static: ''
  },

  MeterMoneyIndicatorNew: {
    draggable: '<div class="">'
              + '<div class="listdash"></div>'
              + '<div class="listtittle txt_fore_color"></div>'
              //+ '<div class="icometer" id="icometer"><img src="./design/img/ico_meter.png" width="25" height="25" /></div>'
              + '<div class="news_today txt_fore_color_gray" data-meter-id="0" data-meter-range="TDA" data-meter-value="range"></div>'
              //+ '<div class="news_today_value txt_fore_color_gray" data-meter-id="0" data-meter-range="NOW" data-meter-value="havg" data-property-type="1"></div>'
              + '<div class="news_ico_up"></div>'
			        + '<div class="news_percent txt_fore_color_green" data-meter-id="0" data-meter-range="TDA" data-meter-value="havg" data-property-type="1"></div>'
              + '</div>',
    static: ''
  },

  HighRollers: '<div class="item">'
             + ' <div class="itemrectt">'
             + '    <div class="first-column">'
             + '        <div class="two-columns-first-row display-table">'
             + '          <div class="itemnameleft txt_fore_color display-table-cell vertical-bottom" data-property-name="name"></div>'
             + '        </div>'
             + '        <div class="two-columns-second-row display-table">'
             + '          <div class="itemnameleft txt_fore_color vertical-top" data-property-name="last-name"></div>'
             + '        </div>'
             + '      </div>'
             + '      <div class="second-column">'
             + '        <div data-property-name="identifier" style="display: none;"></div>'
             + '        <div class="itemtracerectHR"></div>'
             + '      </div>'
             + '      <div class="third-column">'
             + '        <div class="two-columns-first-row display-table">'
             + '          <div class="itemamountrightbottom txt_fore_color_orange_dark display-table-cell vertical-bottom" data-property-name="hr1">$$$$$</div>'
             + '        </div>'
             + '        <div class="two-columns-second-row display-table">'
             + '          <div class="itemamountrightbottom txt_fore_color_orange display-table-cell vertical-top" data-property-name="hr2">$$$$$</div>'
             + '        </div>'
             + '      </div>'
             + '    </div>'
             + ' </div>'
             + '</div>',

  SVGPlayerInfoChart: '<div class="svg_info_player_chart_container"></div>',

  BirthDay: '<div class="item_birth_day">'
           + '  <div class="birth_day_container">'
           + '    <div class="first-column first-column-birthday">'
           + '      <div class="first-row">'
           + '        <div class="itemmachineleft itemwrap" data-property-name="machine_name">XX</div>'
           + '      </div>'
           + '      <div class="second-row">'
           + '        <div class="itemnameleft txt_fore_color itemwrap" data-property-name="name"></div>'
           + '      </div>'
           + '      <div class="third-row">'
           + '        <div class="itemnameleft txt_fore_color itemwrap" data-property-name="last-name"></div>'
           + '      </div>'
           + '    </div>'
           + '    <div class="second-column">'
           + '      <div data-property-name="identifier" style="display: none;"></div>'
           + '      <div class="itemtracerectBirthD"></div>'
           + '    </div>'
           + '    <div class="third-column third-column-birthday">'
           + '      <div class="birthd_container_age"><span class="birthd_user_ages" data-property-name="ages">28</span></div>'
           + '    </div>'
           + '  </div>'
           + '</div>',

  MaxLvlPlayers: '<div class="item_mlp">'
           + '  <div class="mlp_container">'
           + '    <div class="first-column">'
           + '      <div class="first-row">'
           + '        <div class="itemlevel" data-property-name="lvl"></div>'
           + '      </div>'
           + '      <div class="second-row">'
           + '        <div class="itemnameleft txt_fore_color" data-property-name="name"></div>'
           //+ '    <div class="birthd_isvip" data-property-name="isvip">VIP</div>'
           + '      </div>'
           + '      <div class="third-row">'
           + '        <div class="itemnameleft txt_fore_color" data-property-name="last-name"></div>'
           + '      </div>'
           + '    </div>'
           + '    <div class="second-column">'
           + '      <div data-property-name="identifier" style="display: none;"></div>'
           + '      <div class="itemtracerectMaxLvlPlayer"></div>'
           + '    </div>'
           + '    <div class="third-column">'
            //+ '    <div class="itemmachineleft" data-property-name="machine_name">XX</div>'
           //+ '    <div class="mlp_coin_in_label" data-property-name="coin_in_label" data-nls="NLS_WIDGET_PLAYERS_MAX_LVL_COIN_IN">Coin in</div>'
           + '      <div class="first-row">'
           + '      </div>'
           + '      <div class="second-row">'
           + '        <div class="mlp_coin_in txt_fore_color_orange_dark" data-property-name="coin_in">XX</div>'
           + '      </div>'
           + '      <div class="third-row">'
           //+ '      <div class="mlp_bet_avg_label" data-property-name="bet_avg_label" data-nls="NLS_WIDGET_PLAYERS_MAX_LVL_BET_AVG">Bet Avg</div>'
           + '        <div class="mlp_bet_avg txt_fore_color_orange" data-property-name="bet_avg">XX</div>'
           + '      </div>'
           + '    </div>'
           + '  </div>'
           + '</div>',
  // -------------------------------

  Empty: ''

};

// ----------------------------------------------------------------------------------------------------------------------------------------------
