﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WSI_WidgetDefinition.js
// 
//   DESCRIPTION: Default Widgets Instances.
// 
//        AUTHOR: Ramón Monclús
// 
// CREATION DATE: JUN-2015
// 
//------------------------------------------------------------------------------

/*
  Requies:
    WSI_WidgetTypes.js
    WSI_WidgetManager.js
    WSI_WidgetTemplates.js
*/


var widget_highcharts = new WidgetBase();
var widget_list_item = undefined;
var widget_top_10_coin_in = new WidgetListBase();
var widget_top_winning_players = new WidgetListBase();
var widget_vip_list = new WidgetListBase();
var widget_highrollers_list = new WidgetListBase();
var widget_runners_list = new WidgetListBase();
var widget_birthday = new WidgetListBase();
var widget_players_max_level = new WidgetListBase();
var widget_amphit_occ = new WidgetSVGAmphitheater();
var widget_amphit_coin_in = new WidgetSVGAmphitheater();
var widget_amphit_bet_avg = new WidgetSVGAmphitheater();
var widget_tasks1 = new WidgetBase();
var widget_column_item = undefined;
var widget_monitor_bet_avg = new WidgetHighChart();
var widget_occ_vs_weather = new WidgetSVGChart();
var widget_donut_gender = new WidgetSVGDonutChart();
var widget_donut_occ = new WidgetSVGDonutChart();
var widget_indicator_netwin = new WidgetBase();
var widget_age_segment = new WidgetSVGDonutChart();
var widget_level_segment = new WidgetSVGDonutChart();
var widget_donut_room_occ = new WidgetSVGDonutChart();
var widget_donut_machine_occ_room_occ = new WidgetSVGDonutChart();

function CreateWidgets() {

  WSILogger.Write("Create Widgets...");

  widget_highcharts.id = 'widgetHighCharts1';
  widget_highcharts.SetTemplate(m_widgets_templates.Chart);
  widget_highcharts.style = 'width:100%; height:100%; background-color: Yellow; color: Black;';
  widget_highcharts.title = _Language.get('NLS_WIDGET_MACHINE_OCC_TITLE');//"Ocupación de máquinas [Now]";
  widget_highcharts.titleNLS = 'NLS_WIDGET_MACHINE_OCC_TITLE';
  widget_highcharts.OnRealTimeComplete = widget_highcharts.Update();
  widget_highcharts.MeterNameAsTitle = false;
  widget_highcharts.OnAfterAdded = function (Widget) {
    if (Widget) {
      $("#widgetHighCharts1_chart").highcharts({
        chart: {
          type: 'bar'
        },
        title: {
          text: 'Fruit Consumption'
        },
        xAxis: {
          categories: ['Apples', 'Bananas', 'Oranges']
        },
        yAxis: {
          title: {
            text: 'Fruit eaten'
          }
        },
        series: [{
          name: 'Jane',
          data: [1, 0, 4]
        }, {
          name: 'John',
          data: [5, 7, 3]
        }]
      });
    }
  }

  // *****************************************************************************************************************

  // 1st list
  widget_top_10_coin_in.id = 'widgetTestList1';
  widget_top_10_coin_in.title = _Language.get('NLS_WIDGET_TOP10_TITLE');
  widget_top_10_coin_in.maxItems = 15;
  widget_top_10_coin_in.titleNLS = 'NLS_WIDGET_TOP10_TITLE';
  widget_top_10_coin_in.BindEventsToClass = '.itemtracerect';
  widget_top_10_coin_in.OnRealTimeComplete = widget_top_10_coin_in.Update;
  widget_top_10_coin_in.MeterNameAsTitle = false;
  widget_top_10_coin_in.OnItemClick = function (List, Item) {

    WSIData.SelectedObject = $(Item).parent().find("[data-property-name='identifier']").text();

    _UI.SetMenuActiveSection($("#a_section_70")[0], CONST_SECTION_ID_MACHINES);

    LocateTerminal(WSIData.SelectedObject, this);


  };
  widget_top_10_coin_in.OnBeforeUpdate = function (List) {
    var _top_10_list = [],
        _items_count,
        _color;

    // Get top 10 by COIN_IN
    for (var _termid in _Manager.Terminals.ByTerminal) {
      var _term = _Manager.Terminals.Items[_Manager.Terminals.ByTerminal[_termid]].Properties;
      _top_10_list.push({ id: _termid, amount: _term.played_amount });
    }
    // Sort the list
    _top_10_list.sort(function (a, b) { return (b.amount - a.amount); });

    // Add Items
    _items_count = ((List.maxItems) > 0 ? List.maxItems : _top_10_list.length);
    for (var _i = 0; _i < _items_count; _i++) {
      if (!_top_10_list[_i]) { break; }
      var _term = _Manager.Terminals.Items[_Manager.Terminals.ByTerminal[_top_10_list[_i].id]].Properties;
      if (_term.played_amount == 0) { break; }
      var _new_item = new WidgetItemBase('term-data-' + _top_10_list[_i].id, m_widgets_templates.ListItemType2);
      _new_item.SetPropertyValue("name", "" + _term.provider_name + " " + _term.name);
      _new_item.SetPropertyValue("amount", _term.played_amount);
      _new_item.SetPropertyValue("identifier", _term.lo_id);
      _new_item.terminal_id = _top_10_list[_i].id;
      List.AddItem(_new_item);
    }
  }
  widget_top_10_coin_in.OnItemPrepared = function (Item, ItemPrepared) {
    var _result = ItemPrepared,
        _term = _Manager.Terminals.Items[_Manager.Terminals.ByTerminal[Item.terminal_id]].Properties,
        _obj = $(ItemPrepared);
    // Occupation indicator
    if (_term.play_session_status == "2") {
      _color = "red !important";
    }
    else {
      _color = "green !important";
    }
    _obj.find(".itemocupationrect").each(function (i, item) { $(item).css("background-color", _color); });

    // Comparison
    var _value = _Manager.MetersManager.GetMeterValueByName("COIN IN", 0, "MTD", "avg");
    if (_value == '') { _value = 0; }
    //_obj.find('[data-property-name="average"]').text(FormatString((_term.played_amount -_value), EN_FORMATS.F_PERCENT));
    //_obj.find('[data-property-name="average"]').text(FormatString((_term.played_amount - _value), EN_FORMATS.F_MONEY));

    var _avg = (100 * (_term.played_amount - _value)) / _value;
    if (!isFinite(_avg)) { _avg = 100; }
    _obj.find('[data-property-name="average"]').text(FormatString(_avg, EN_FORMATS.F_PERCENT));

    if (_term.played_amount < _value) {
      _obj.find('[data-property-name="coaverage"]').addClass("icoaveragedown");
    }

    _result = _obj.outerHTML();
    return _result;
  }

  // *****************************************************************************************************************

  // 2nd list
  widget_top_winning_players.id = 'widgetTestList2';
  widget_top_winning_players.maxItems = 15;
  widget_top_winning_players.showCounter = false;
  widget_top_winning_players.hasScroll = false;
  widget_top_winning_players.title = _Language.get('NLS_WIDGET_TOPWINNINGPLAYER_TITLE');
  widget_top_winning_players.BindEventsToClass = '.itemtracerect';
  widget_top_winning_players.titleNLS = 'NLS_WIDGET_TOPWINNINGPLAYER_TITLE';
  widget_top_winning_players.InfoNLS = 'NLS_WIDGET_TOPWINNINGPLAYER_INFO';
  widget_top_winning_players.OnRealTimeComplete = widget_top_winning_players.Update;
  widget_top_winning_players.MeterNameAsTitle = false;
  widget_top_winning_players.OnItemClick = function (List, Item) {


    WSIData.SelectedObject = $(Item).parent().find("[data-property-name='identifier']").text();

    _UI.SetMenuActiveSection($("#a_section_60")[0], CONST_SECTION_ID_PLAYERS);

    LocateTerminal(WSIData.SelectedObject, this);

  };
  widget_top_winning_players.OnBeforeUpdate = function (List) {
    var _top_10_list = [],
        _items_count,
        _term;

    List.TotalItems = [];

    // Get top 10 by Winnings
    for (var _accid in _Manager.Terminals.ByAccount) {
      _term = _Manager.Terminals.Items[_Manager.Terminals.ByAccount[_accid]].Properties;
      _top_10_list.push({ id: _accid, amount: _term.play_session_won_amount, inFloor: true });
    }

    if (WSIData.SiteDashboard) {
      for (var _accid in _Manager.OtherTerminals.ByAccount) {
        _term = _Manager.OtherTerminals.Items[_Manager.OtherTerminals.ByAccount[_accid]].Properties;
        _top_10_list.push({ id: _accid, amount: _term.play_session_won_amount, inFloor: false });
      }
    }

    // Sort the list
    _top_10_list.sort(function (a, b) { return (b.amount - a.amount); });

    // Add Items
    _items_count = ((List.maxItems) > 0 ? List.maxItems : _top_10_list.length);
    for (var _i = 0; _i < _items_count; _i++) {
      if (!_top_10_list[_i]) { break; }
      if (_top_10_list[_i].inFloor) {
        _term = _Manager.Terminals.Items[_Manager.Terminals.ByAccount[_top_10_list[_i].id]].Properties;
      } else {
        _term = _Manager.OtherTerminals.Items[_Manager.OtherTerminals.ByAccount[_top_10_list[_i].id]].Properties;
      }
      var _new_item = new WidgetItemBase('track-data-' + _top_10_list[_i].id, (_top_10_list[_i].inFloor) ? m_widgets_templates.ListItemType1 : m_widgets_templates.ListItemType1OtherFloor);

      if (_term.player_last_name1 != _term.player_first_name)
        _new_item.SetPropertyValue("name", _term.player_first_name);
      else
        _new_item.SetPropertyValue("name", '');

      if (_term.player_last_name1 == _term.player_last_name2)
        _new_item.SetPropertyValue("last-name", _term.player_last_name1);
      else
        _new_item.SetPropertyValue("last-name", _term.player_last_name1 + ' ' + _term.player_last_name2);

      _new_item.SetPropertyValue("amount", _term.play_session_won_amount);
      _new_item.SetPropertyValue("terminal", "" + _term.provider_name.replace("[", "").replace("]", "") + " " + _term.name.replace("[", "").replace("]", ""));
      _new_item.SetPropertyValue("coaverage", "");
      _new_item.SetPropertyValue("identifier", _term.lo_id);
      _new_item.SetPropertyValue("level", _term.player_level_name);
      List.AddItem(_new_item);
    }
  }

  // *****************************************************************************************************************

  // list
  widget_vip_list.id = 'widgetVipsList';
  widget_vip_list.showCounter = false;
  widget_vip_list.hasScroll = false;
  widget_vip_list.title = _Language.get('NLS_WIDGET_VIPS_TITLE');
  widget_vip_list.titleNLS = 'NLS_WIDGET_VIPS_TITLE';
  widget_vip_list.InfoNLS = 'NLS_WIDGET_VIPS_INFO';
  widget_vip_list.BindEventsToClass = '.itemtracerect';
  widget_vip_list.OnRealTimeComplete = widget_vip_list.Update;
  widget_vip_list.MeterNameAsTitle = false;
  widget_vip_list.OnItemClick = function (List, Item) {

    WSIData.SelectedObject = $(Item).parent().find("[data-property-name='identifier']").text();

    _UI.SetMenuActiveSection($("#a_section_60")[0], CONST_SECTION_ID_PLAYERS);

    LocateTerminal(WSIData.SelectedObject, this);

  };

  widget_vip_list.OnBeforeUpdate = function (List) {
    // Get VIP Players
    var _max_player_level = 4;
    List.TotalItems = [];

    for (var _accid in _Manager.Terminals.ByAccount) {
      var _term = _Manager.Terminals.Items[_Manager.Terminals.ByAccount[_accid]].Properties;

      if (_term.player_is_vip == "1") {
        var _new_item = new WidgetItemBase('track-data-' + _accid, m_widgets_templates.VipListItem);
        _new_item.SetPropertyValue("name", _term.player_first_name);
        _new_item.SetPropertyValue("last-name", _term.player_last_name1 + ' ' + _term.player_last_name2);
        _new_item.SetPropertyValue("level", _term.player_level_name);
        _new_item.SetPropertyValue("identifier", _term.lo_id);
        _new_item.SetPropertyValue("terminal", "" + _term.provider_name.replace("[", "").replace("]", "") + " " + _term.name.replace("[", "").replace("]", ""));
        _new_item.SetPropertyValue("ages", "28");

        if (!(_accid in List.TotalItems))
          List.AddItem(_new_item);
      }
    }

    if (WSIData.SiteDashboard) {
      // Get VIP players on site too
      for (var _accid in _Manager.OtherTerminals.ByAccount) {
        var _term = _Manager.OtherTerminals.Items[_Manager.OtherTerminals.ByAccount[_accid]].Properties;
        if (_term.player_is_vip == "1") {
          var _new_item = new WidgetItemBase('track-data-' + _accid, m_widgets_templates.VipListItemOtherFloor);
          _new_item.SetPropertyValue("name", _term.player_first_name);
          _new_item.SetPropertyValue("last-name", _term.player_last_name1 + ' ' + _term.player_last_name2);
          _new_item.SetPropertyValue("level", _term.player_level_name);
          _new_item.SetPropertyValue("identifier", _term.lo_id);
          _new_item.SetPropertyValue("terminal", "" + _term.provider_name.replace("[", "").replace("]", "") + " " + _term.name.replace("[", "").replace("]", ""));

          if (!(_accid in List.TotalItems))
            List.AddItem(_new_item);
        }
      }
    }
  }

  // *****************************************************************************************************************

  // list
  if (_Manager.GeneralParams["Alarms.HighRoller.Enabled"] == "1") {
    m_widgets_list.widgetHRList = widget_highrollers_list;
    widget_highrollers_list.id = 'widgetHRList';

    //CSR - 04/05/2016
    widget_highrollers_list.showCounter = false;
    widget_highrollers_list.hasScroll = false;

    widget_highrollers_list.title = _Language.get('NLS_WIDGET_HIGHROLLERS_TITLE');
    widget_highrollers_list.titleNLS = 'NLS_WIDGET_HIGHROLLERS_TITLE';
    widget_highrollers_list.InfoNLS = 'NLS_WIDGET_HIGHROLLERS_INFO';
    widget_highrollers_list.BindEventsToClass = '.itemtracerectHR';
    widget_highrollers_list.OnRealTimeComplete = UpdateWidgetHighRoller;
    widget_highrollers_list.MeterNameAsTitle = false;
    widget_highrollers_list.PrepareStyles = function (Html) {
      if (Html == "") { return Html; }
      var _coin_in_value, _bet_avg_value;
      _bet_avg_value = parseFloat(_Manager.GeneralParams["Alarms.HighRoller.Limit.BetAverage"]);
      _coin_in_value = parseFloat(_Manager.GeneralParams["Alarms.HighRoller.Limit.CoinIn"]);
      var _html = $(Html);


      if (_coin_in_value == 0) {
        _html.find(".itemlevelhr2_1").hide();
        _html.find(".itemlevelhr2_2").hide();
        _html.find(".itemrecttHR").css("height", "50px");
        _html.find(".itemtracerectHR").css("height", "50px");
        _html.find(".itemHR").css("height", "50px");
      }

      if (_bet_avg_value == 0) {
        _html.find(".itemlevelhr3_1").hide();
        _html.find(".itemlevelhr3_2").hide();
        _html.find(".itemrecttHR").css("height", "50px");
        _html.find(".itemtracerectHR").css("height", "50px");
        _html.find(".itemHR").css("height", "50px");
      }
      return _html[0].outerHTML;
    }
    widget_highrollers_list.OnItemClick = function (List, Item) {

      WSIData.SelectedObject = $(Item).parent().find("[data-property-name='identifier']").text();

      _UI.SetMenuActiveSection($("#a_section_60")[0], CONST_SECTION_ID_PLAYERS);

      LocateTerminal(WSIData.SelectedObject, this);

    };
    widget_highrollers_list.OnBeforeUpdate = function (List) {
      List.TotalItems = [];
      var _coin_in_value, _bet_avg_value, _list_count = 0;
      _bet_avg_value = parseFloat(_Manager.GeneralParams["Alarms.HighRoller.Limit.BetAverage"]);
      _coin_in_value = parseFloat(_Manager.GeneralParams["Alarms.HighRoller.Limit.CoinIn"]);
      UpdateWidgetHighRoller();
      for (var _idx_hr_col in widget_highrollers_list.Rows) {
        var _new_item = new WidgetItemBase('track-data-' + _idx_hr_col, m_widgets_templates.HighRollers);
        var _mhl_item = widget_highrollers_list.Rows[_idx_hr_col];

        _new_item.SetPropertyValue("name", _mhl_item.player_first_name);
        _new_item.SetPropertyValue("last-name", _mhl_item.player_last_name1 + ' ' + _mhl_item.player_last_name2);

        if (_coin_in_value > 0) {
          $(".itemlevelhr2_1").text(_Language.get('NLS_WIDGET_HIGHROLLER_COIN_IN'));
          _new_item.SetPropertyValue("hr1", FormatString(parseFloat(+_mhl_item.coin_in).toFixed(2), EN_FORMATS.F_MONEY));
        }

        if (_bet_avg_value > 0) {
          if (_coin_in_value > 0) {
            _new_item.SetPropertyValue("hr2", FormatString(parseFloat(+_mhl_item.bet_avg).toFixed(2), EN_FORMATS.F_MONEY));
            $(".itemlevelhr3_1").text(_Language.get('NLS_WIDGET_HIGHROLLER_BET_AVG'));
          } else {
            $(".itemlevelhr2_1").text(_Language.get('NLS_WIDGET_HIGHROLLER_COIN_IN'));
            _new_item.SetPropertyValue("hr1", FormatString(parseFloat(+_mhl_item.coin_in).toFixed(2), EN_FORMATS.F_MONEY));
          }
        }

        _new_item.SetPropertyValue("identifier", _mhl_item.lo_id);

        if (testMode == "true" && _list_count < 5) {
          List.AddItem(_new_item);
          _list_count++;
        }
        else {
          List.AddItem(_new_item);
        }
      }
      if (List.Items.length == 0)
        $("#" + List.id).find(".pagination-bar").hide();
      else
        $("#" + List.id).find(".pagination-bar").show();
    }
  }

  var _barrrr = undefined;

  widget_amphit_occ.id = "widgetOccupancy1";
  widget_amphit_occ.SetTemplate(m_widgets_templates.Amphitheater);
  widget_amphit_occ.barContainerColor = "#fff";
  widget_amphit_occ.defaultFontSize = '13em';
  widget_amphit_occ.defaultLabelsFontFamily = 'Calibri';
  widget_amphit_occ.title = _Language.get('NLS_WIDGET_TITLE_OCCUPANCY');
  widget_amphit_occ.titleNLS = "NLS_WIDGET_TITLE_OCCUPANCY";
  widget_amphit_occ.InfoNLS = "NLS_WIDGET_TITLE_OCCUPANCY_INFO";
  widget_amphit_occ.MeterId = [4];
  _barrrr = widget_amphit_occ.addMeterBar(4, 0, "YTD", "havg", "NLS_WIDGET_OCC_LAST_YEAR");
  _barrrr = widget_amphit_occ.addMeterBar(4, 0, "MTD", "havg", "NLS_WIDGET_OCC_LAST_MONTH");
  _barrrr = widget_amphit_occ.addMeterBar(4, 0, "TDW", "havg", "NLS_WIDGET_OCC_LAST_WEEK");
  _barrrr = widget_amphit_occ.addMeterBar(4, 0, "TDA", "havg", "NLS_WIDGET_OCC_TODAY");
  _barrrr["class"] = "txt_fore_color_yellow amphitheater_label";
  widget_amphit_occ.valueType = EN_FORMATS.F_INTEGER;
  widget_amphit_occ.MeterNameAsTitle = false;
  widget_amphit_occ.SetPropertyValue("DATE_HOUR", GetHourNowRange());
  widget_amphit_occ.animations = "none";
  widget_amphit_occ.OnRealTimeComplete = widget_amphit_occ.Update;

  widget_amphit_occ.OnBeforeUpdate = function (Widget) {
    var _max_value = 0,
        _bar,
      _factor;
    for (var _b = 0; _b < Widget.bars.length; _b++) {
      _bar = Widget.bars[_b];
      _max_value = (_max_value > _bar.value) ? _max_value : _bar.value;
    }

    for (var _b = 0; _b < Widget.bars.length; _b++) {
      _bar = Widget.bars[_b];
      _bar.value = Math.ceil(_bar.value);
    }

    if (testMode == "true") {
      // Last year
      _bar = Widget.bars[0];
      _bar.value = 656876;

      // Last month
      _bar = Widget.bars[1];
      _bar.value = 238907;

      // Last week
      _bar = Widget.bars[2];
      _bar.value = 57387;

      // Today
      _bar = Widget.bars[3];
      _bar.value = 851;
    }

    $("#" + Widget.id).find("[data-property-name='DATE_HOUR']").text(GetHourNowRange());
  }


  widget_amphit_coin_in.barContainerColor = "#36464E";
  widget_amphit_coin_in.defaultFontSize = '13em';
  widget_amphit_coin_in.defaultLabelsFontFamily = 'Calibri';
  widget_amphit_coin_in.id = "widgettheater2";
  widget_amphit_coin_in.SetTemplate(m_widgets_templates.Amphitheater);
  widget_amphit_coin_in.title = _Language.get('NLS_WIDGET_COININ_TITLE');
  widget_amphit_coin_in.titleNLS = "NLS_WIDGET_COININ_TITLE";
  widget_amphit_coin_in.InfoNLS = "NLS_WIDGET_COININ_INFO";
  widget_amphit_coin_in.MeterId = [12];
  widget_amphit_coin_in.valueType = EN_FORMATS.F_MONEY;
  _barrrr = widget_amphit_coin_in.addMeterBar(12, 0, "YTD", "hacc", "NLS_WIDGET_OCC_LAST_YEAR");
  _barrrr = widget_amphit_coin_in.addMeterBar(12, 0, "MTD", "hacc", "NLS_WIDGET_OCC_LAST_MONTH");
  _barrrr = widget_amphit_coin_in.addMeterBar(12, 0, "TDW", "hacc", "NLS_WIDGET_OCC_LAST_WEEK");
  _barrrr = widget_amphit_coin_in.addMeterBar(12, 0, "TDA", "havg", "NLS_WIDGET_OCC_CURRENT");
  _barrrr["class"] = "txt_fore_color_yellow amphitheater_label";
  widget_amphit_coin_in.SetPropertyValue("DATE_HOUR", GetHourNowRange());
  widget_amphit_coin_in.animations = "none";
  widget_amphit_coin_in.OnRealTimeComplete = widget_amphit_coin_in.Update;
  widget_amphit_coin_in.MeterNameAsTitle = false;

  widget_amphit_coin_in.OnBeforeUpdate = function (Widget) {
    var _max_value = 0,
        _bar,
      _factor;
    for (var _b = 0; _b < Widget.bars.length; _b++) {
      _bar = Widget.bars[_b];
      _max_value = (_max_value > _bar.value) ? _max_value : _bar.value;
    }
    //for (var _b = 0; _b < Widget.bars.length; _b++) {
    //  _bar = Widget.bars[_b];
    //  _bar.value = Math.ceil(_bar.value);
    //}

    $("#" + Widget.id).find("[data-property-name='DATE_HOUR']").text(GetHourNowRange());

  }

  widget_amphit_bet_avg.defaultFontSize = '13em';
  widget_amphit_bet_avg.defaultLabelsFontFamily = 'Calibri';
  widget_amphit_bet_avg.id = "widgettheater3";
  widget_amphit_bet_avg.SetTemplate(m_widgets_templates.Amphitheater);
  widget_amphit_bet_avg.MeterId = [1];
  widget_amphit_bet_avg.animations = "none";
  _barrrr = widget_amphit_bet_avg.addMeterBar(1, 0, "YTD", "havg", "NLS_WIDGET_OCC_LAST_YEAR");
  _barrrr = widget_amphit_bet_avg.addMeterBar(1, 0, "MTD", "havg", "NLS_WIDGET_OCC_LAST_MONTH");
  _barrrr = widget_amphit_bet_avg.addMeterBar(1, 0, "TDW", "havg", "NLS_WIDGET_OCC_LAST_WEEK");
  _barrrr = widget_amphit_bet_avg.addMeterBar(1, 0, "TDA", "havg", "NLS_WIDGET_OCC_CURRENT");
  _barrrr["class"] = "txt_fore_color_yellow amphitheater_label";
  widget_amphit_bet_avg.valueType = EN_FORMATS.F_MONEY;
  widget_amphit_bet_avg.MeterNameAsTitle = false;
  widget_amphit_bet_avg.title = _Language.get('NLS_WIDGET_AVERAGEBET_TITLE');
  widget_amphit_bet_avg.titleNLS = "NLS_WIDGET_AVERAGEBET_TITLE";
  widget_amphit_bet_avg.InfoNLS = "NLS_WIDINF_AVERAGE_BET_INFO";
  widget_amphit_bet_avg.SetPropertyValue("DATE_HOUR", GetHourNowRange());
  widget_amphit_bet_avg.OnRealTimeComplete = widget_amphit_bet_avg.Update;

  widget_amphit_bet_avg.OnBeforeUpdate = function (Widget) {

    // RMS : Fake data on empty meters !! DELETE THIS !!
    //var _max_value = 0,
    //    _bar,
    //  _factor;
    //for (var _b = 0; _b < Widget.bars.length; _b++) {
    //  _bar = Widget.bars[_b];
    //  _max_value = (_max_value > _bar.value) ? _max_value : _bar.value;
    //}
    //for (var _b = 0; _b < Widget.bars.length; _b++) {
    //  _bar = Widget.bars[_b];
    //  _bar.value = Math.ceil(_bar.value);
    //}

    $("#" + Widget.id).find("[data-property-name='DATE_HOUR']").text(GetHourNowRange());
  }

  // ----------------------------------------------------------------------------------------------------------------------------------------------

  widget_tasks1.id = "widgetTasks1";
  widget_tasks1.SetTemplate(m_widgets_templates.Tasks);
  widget_tasks1.title = _Language.get('NLS_WIDGET_TASKS_TITLE');
  widget_tasks1.titleNLS = "NLS_WIDGET_TASKS_TITLE";
  widget_tasks1.InfoNLS = "NLS_WIDGET_TASKS_INFO";
  widget_tasks1.SetPropertyValue("TASKS_PENDING", "0");
  widget_tasks1.SetPropertyValue("TASKS_ASSIGNED", "0");
  widget_tasks1.SetPropertyValue("TASKS_SOLVED", "0");
  widget_tasks1.MeterNameAsTitle = false;
  widget_tasks1.OnRealTimeComplete = widget_tasks1.Update;
  widget_tasks1.MeterNameAsTitle = false;

  widget_tasks1.OnAfterAdded = function () {
    this.Update();
  }
  widget_tasks1.OnUpdate = function (Widget) {

    // TODO: Create function on task manager or set data on update task list process
    var _data_total = { total: 0, pending: 0, assigned: 0, solved: 0 };
    var _data_category_technical = { total: 0, pending: 0, assigned: 0, solved: 0 };
    var _data_category_player = { total: 0, pending: 0, assigned: 0, solved: 0 };
    var _data_category_custom = { total: 0, pending: 0, assigned: 0, solved: 0 };

    if (_Manager.TaskManager) {

      for (var _idx in _Manager.TaskManager.Tasks) {
        var _task = _Manager.TaskManager.Tasks[_idx];
        // Total
        switch (_task.status) {
          case Enum.EN_TASK_STATUS.NONE:
          case Enum.EN_TASK_STATUS.SCALED:
            break;
          case Enum.EN_TASK_STATUS.ASSIGNED:
            _data_total.assigned += 1;
            break;
          case Enum.EN_TASK_STATUS.IN_PROGRESS:
            _data_total.pending += 1;
            break;
          case Enum.EN_TASK_STATUS.RESOLVED:
            _data_total.solved += 1;
            break;
        }
        // Category - technical
        switch (_task.category) {
          case Enum.EN_ALARM_TYPE.USER:
            switch (_task.status) {
              case Enum.EN_TASK_STATUS.NONE:
              case Enum.EN_TASK_STATUS.SCALED:
                break;
              case Enum.EN_TASK_STATUS.ASSIGNED:
                _data_category_player.assigned += 1;
                break;
              case Enum.EN_TASK_STATUS.IN_PROGRESS:
                _data_category_player.pending += 1;
                break;
              case Enum.EN_TASK_STATUS.RESOLVED:
                _data_category_player.solved += 1;
                break;
                _data_category_player.pending += 1;
                break;
            }
            break;
          case Enum.EN_ALARM_TYPE.TERMINAL:
            switch (_task.status) {
              case Enum.EN_TASK_STATUS.NONE:
              case Enum.EN_TASK_STATUS.SCALED:
                break;
              case Enum.EN_TASK_STATUS.ASSIGNED:
                _data_category_technical.assigned += 1;
                break;
              case Enum.EN_TASK_STATUS.IN_PROGRESS:
                _data_category_technical.pending += 1;
                break;
              case Enum.EN_TASK_STATUS.RESOLVED:
                _data_category_technical.solved += 1;
                break;
                _data_category_technical.pending += 1;
                break;
            }
            break;
          case Enum.EN_ALARM_TYPE.CUSTOM:
            switch (_task.status) {
              case Enum.EN_TASK_STATUS.NONE:
              case Enum.EN_TASK_STATUS.SCALED:
                break;
              case Enum.EN_TASK_STATUS.ASSIGNED:
                _data_category_custom.assigned += 1;
                break;
              case Enum.EN_TASK_STATUS.IN_PROGRESS:
                _data_category_custom.pending += 1;
                break;
              case Enum.EN_TASK_STATUS.RESOLVED:
                _data_category_custom.solved += 1;
                break;
            }
            break;
        }
      }
      _data_total.total = _data_total.pending + _data_total.assigned + _data_total.solved;
      _data_category_technical.total = _data_category_technical.pending + _data_category_technical.assigned + _data_category_technical.solved;
      _data_category_player.total = _data_category_player.pending + _data_category_player.assigned + _data_category_player.solved;
      _data_category_custom.total = _data_category_custom.pending + _data_category_custom.assigned + _data_category_custom.solved;



      Widget.SetPropertyValue("TASKS_PENDING", _data_total.pending);
      Widget.SetPropertyValue("TASKS_ASSIGNED", _data_total.assigned);
      Widget.SetPropertyValue("TASKS_SOLVED", _data_total.solved);

      Widget.UpdateProperties();

      // RMS
      // TODO: When all values are 0 show a bar with 0 value and gray background

      //var _data = { total: 24, pending: 3, assigned: 6, solved: 15 };
      $('.task_bar_technical').css('background', 'gray').multiprogressbar({
        parts: [
        {
          value: ((_data_category_technical.pending * 100) / _data_category_technical.total), text: _data_category_technical.pending, barClass: "task_bar_pending", textClass: "lbl_value_task"
        },
        {
          value: ((_data_category_technical.assigned * 100) / _data_category_technical.total), text: _data_category_technical.assigned, barClass: "task_bar_assigned", textClass: "lbl_value_task"
        },
        {
          value: ((_data_category_technical.solved * 100) / _data_category_technical.total), text: _data_category_technical.solved, barClass: "task_bar_solved", textClass: "lbl_value_task"
        }
        ]
      });

      //var _data = { total: 45, pending: 32, assigned: 2, solved: 11 };
      $('.task_bar_players').css('background', 'gray').multiprogressbar({
        parts: [
        {
          value: ((_data_category_player.pending * 100) / _data_category_player.total), text: _data_category_player.pending, barClass: "task_bar_pending", textClass: "lbl_value_task"
        },
        {
          value: ((_data_category_player.assigned * 100) / _data_category_player.total), text: _data_category_player.assigned, barClass: "task_bar_assigned", textClass: "lbl_value_task"
        },
        {
          value: ((_data_category_player.solved * 100) / _data_category_player.total), text: _data_category_player.solved, barClass: "task_bar_solved", textClass: "lbl_value_task"
        }
        ]
      });

      //var _data = { total: 5, pending: 2, assigned: 2, solved: 1 };
      $('.task_bar_site').css('background', 'gray').multiprogressbar({
        parts: [
        {
          value: ((_data_category_custom.pending * 100) / _data_category_custom.total), text: _data_category_custom.pending, barClass: "task_bar_pending", textClass: "lbl_value_task"
        },
        {
          value: ((_data_category_custom.assigned * 100) / _data_category_custom.total), text: _data_category_custom.assigned, barClass: "task_bar_assigned", textClass: "lbl_value_task"
        },
        {
          value: ((_data_category_custom.solved * 100) / _data_category_custom.total), text: _data_category_custom.solved, barClass: "task_bar_solved", textClass: "lbl_value_task"
        }
        ]
      });

      // TODO: SUM
      Widget.UpdateTitle(_Language.get(Widget.titleNLS) + " [" + _data_total.total + "]");
      _Language.parse();
    }
  }

  // ----------------------------------------------------------------------------------------------------------------------------------------------

  widget_monitor_bet_avg.id = "widgetChart1";
  widget_monitor_bet_avg.SetTemplate(m_widgets_templates.SVGChart);
  widget_monitor_bet_avg.SetPropertyValue("DATE_HOUR", GetHourNow());
  widget_monitor_bet_avg.titleNLS = 'NLS_WIDGET_AVERAGEBET_TITLE';
  widget_monitor_bet_avg.title = _Language.get('NLS_WIDGET_AVERAGEBET_TITLE');
  widget_monitor_bet_avg.InfoNLS = "NLS_WIDINF_SITE_AVERAGE_BET";
  widget_monitor_bet_avg.MeterNameAsTitle = false;
  widget_monitor_bet_avg.OnRealTimeComplete = widget_monitor_bet_avg.Update;
  widget_monitor_bet_avg.OnBeforeUpdate = function (Widget) {
    $("#" + Widget.id).find("[data-property-name='DATE_HOUR']").text('hola');
  }

  // ----------------------------------------------------------------------------------------------------------------------------------------------

  widget_occ_vs_weather.id = "widgetChart2";
  widget_occ_vs_weather.SetTemplate(m_widgets_templates.SVGChart);
  widget_occ_vs_weather.title = _Language.get('NLS_WIDGET_OCCVSWEATHER_TITLE');
  widget_occ_vs_weather.titleNLS = "NLS_WIDGET_OCCVSWEATHER_TITLE";
  widget_occ_vs_weather.InfoNLS = "NLS_WIDGET_OCCVSWEATHER_INFO";
  widget_occ_vs_weather.margin = 25;
  widget_occ_vs_weather.lineMaxIncrement = 0;
  widget_occ_vs_weather.MeterNameAsTitle = false;

  widget_occ_vs_weather.OnBeforeUpdate = function (Widget) {
    var _max_value = 0,
        _bar,
      _factor;
    for (var _b = 0; _b < Widget.lines[0].values.length; _b++) {
      _bar = Widget.lines[0].values[_b];
      _max_value = (_max_value > _bar) ? _max_value : _bar;
    }

    if (isNaN(_max_value)) { _max_value = 0; }

    this.linesMaxValue = _max_value;
  }


  var _cap_list = [], _promo_list = [], weather_list = [], _weather_line = [], _day_of_week = [], _number_day_of_week = [];

  for (_idx_meter_weather in _Manager.MetersManager.WeatherMeter) {
    var _item = _Manager.MetersManager.WeatherMeter[_idx_meter_weather];
    var _dow = _item[0];
    var _num_promotions = _item[1];
    var _occupancy = _item[2];
    var _weather_icon = _item[3];
    var _day_w = _item[4];

    if (_occupancy == null || _occupancy[3] == undefined || _occupancy == '') {
      _cap_list.push(0);
    } else {
      _cap_list.push(Math.ceil(parseFloat(_occupancy.replace(',','.'))));
    }

    _promo_list.push(_num_promotions);
    weather_list.push(_weather_icon);
    _number_day_of_week.push(_day_w);
    _day_of_week.push(getDayOfWeek(_day_w));

  }

  //Bars
  _barrrr = widget_occ_vs_weather.addSerie("CAP", _cap_list.reverse());
  _barrrr.type = WidgetSVGChartTypes.bars;
  _barrrr.color = "blue";

  //Lines
  _barrrr = widget_occ_vs_weather.addSerie("WEA", _promo_list.reverse());
  _barrrr.type = WidgetSVGChartTypes.smothlines;
  _barrrr.border_dasharray = "";
  _barrrr.border_width = 0; // // DMT 07/06/2016 en 0 para q no se vea la barra blanca
  _barrrr.pointImages = GetWeatherIcons(weather_list.reverse());
  _barrrr.imageWidth = 40;
  _barrrr.imageHeight = 28;
  _barrrr.number_labels = _number_day_of_week.reverse();
  _barrrr.labels = _day_of_week.reverse();


  widget_occ_vs_weather.OnRealTimeComplete = widget_occ_vs_weather.Update;

  widget_occ_vs_weather.OnGraphClick = function () {

    //widget_occ_vs_weather.lines = [];
    //var _cap_list = [], _promo_list = [], weather_list = [], weather_list = [], _day_of_week = [];

    //for (_idx_meter_weather in _Manager.MetersManager.WeatherMeter) {
    //  var _item = _Manager.MetersManager.WeatherMeter[_idx_meter_weather];

    //  if (_item[3] == null || _item[3] == undefined || _item[3] == '') {
    //    _cap_list.push(0);
    //  } else {
    //    _cap_list.push(_item[3]);
    //  }

    //  _promo_list.push(_item[2]);
    //  //weather_list.push(_item[4]);
    //  weather_list.push(0);
    //  _day_of_week.push(getDayOfWeek(_item[1]));
    //}

    ////Bars
    //_barrrr = widget_occ_vs_weather.addSerie("CAP", _cap_list.reverse());
    //_barrrr.type = WidgetSVGChartTypes.bars;
    //_barrrr.color = "blue";

    ////Lines
    //_barrrr = widget_occ_vs_weather.addSerie("WEA", _promo_list.reverse());
    //_barrrr.type = WidgetSVGChartTypes.smothlines;
    //_barrrr.color = "white";
    //_barrrr.border_dasharray = "";
    //_barrrr.border_width = 3;
    //_barrrr.pointImages = GetWeatherIcons(_weather_line.reverse());
    //_barrrr.imageWidth = 42;
    //_barrrr.imageHeight = 30;

    //this.Update(true);
    return true;
  }

  //// ----------------------------------------------------------------------------------------------------------------------------------------------

  widget_donut_gender.id = "widgetDonut1";
  widget_donut_gender.SetTemplate(m_widgets_templates.DonutChart);
  widget_donut_gender.values = [752, 325];
  widget_donut_gender.defaultColors = ["#CC66FF", "#0099FF", "#9A9C9E"];
  widget_donut_gender.drawLabels = true;
  widget_donut_gender.defaultFontSize = '13em';
  widget_donut_gender.defaultLabelsFontFamily = 'Calibri';
  widget_donut_gender.title = _Language.get('NLS_WIDGET_GENDER_TITLE');
  widget_donut_gender.titleNLS = "NLS_WIDGET_GENDER_TITLE";
  widget_donut_gender.InfoNLS = "NLS_WIDGET_GENDER_INFO";
  widget_donut_gender.MeterNameAsTitle = false;
  widget_donut_gender.OnRealTimeComplete = widget_donut_gender.Update;
  widget_donut_gender.centerLabel = {
    text: "1077", attrs: {
      fill: "white", "font-size": "25em", "font-family": "Calibri", "font-weight": "bold"
    }
  };
  widget_donut_gender.centerLabelTitle = {
    text: _Language.get('NLS_CHART_OPTION_STATS_TOTAL'), attrs: {
      fill: "white", "font-size": "15em", "font-family": "Calibri", "font-weight": "bold"
    },
    NLS: "NLS_CHART_OPTION_STATS_TOTAL"
  };

  //widget_donut_gender.OnGraphClick = function () {
  //  this.Update(true);
  //}
  widget_donut_gender.OnBeforeUpdate = function (Chart) {
    var _total_male = 0,
        _total_female = 0,
        _total_unknown = 0,
        _total_site = 0;

    if (!WSIData.SiteDashboard) {
      _total_male = _Manager.Terminals.OccupationData.Gender["Male"];
      _total_female = _Manager.Terminals.OccupationData.Gender["Female"];
      _total_unknown = _Manager.Terminals.OccupationData.Gender["Unknown"];
      _total_site = _Manager.Terminals.OccupationData.TotalCard;
    } else {
      _total_male = _Manager.Terminals.OccupationData.Gender["Male"] + _Manager.OtherTerminals.OccupationData.Gender["Male"];
      _total_female = _Manager.Terminals.OccupationData.Gender["Female"] + _Manager.OtherTerminals.OccupationData.Gender["Female"];
      _total_unknown = _Manager.Terminals.OccupationData.Gender["Unknown"] + _Manager.OtherTerminals.OccupationData.Gender["Unknown"];
      _total_site = _Manager.Terminals.OccupationData.TotalCard + _Manager.OtherTerminals.OccupationData.TotalCard;
    }

    Chart.labels = [];
    Chart.values = [];
    Chart.labels.push('NLS_WIDGET_GENDER_FEMALE')
    Chart.labels.push('NLS_WIDGET_GENDER_MALE')
    Chart.labels.push('NLS_WIDGET_UNKNOWN')
    Chart.values.push(_total_female);
    Chart.values.push(_total_male);
    Chart.values.push(_total_unknown);
    Chart.centerLabel.text = FormatString(_total_site, EN_FORMATS.F_NUMBER, 0);
  }


  /******************************************************************************
   * WIDGET OCUPACIÓN DE MÁQUINA
   ******************************************************************************/
  widget_donut_occ.id = "widgetMachineOccupation";
  widget_donut_occ.SetTemplate(m_widgets_templates.DonutChart);
  widget_donut_occ.defaultColors = ["#1BA81D", "#606060", "#828282"];
  widget_donut_occ.defaultFontSize = '13em';
  widget_donut_occ.defaultLabelsFontFamily = 'Calibri';
  widget_donut_occ.drawLabels = true;
  widget_donut_occ.showTotal = true;
  widget_donut_occ.MeterNameAsTitle = false;
  widget_donut_occ.title = _Language.get('NLS_WIDGET_MACHINE_OCC_TITLE');//"Ocupación de máquinas [Now]";
  widget_donut_occ.titleNLS = 'NLS_WIDGET_MACHINE_OCC_TITLE';
  widget_donut_occ.InfoNLS = 'NLS_WIDGET_MACHINE_OCC_INFO';
  widget_donut_occ.OnRealTimeComplete = widget_donut_occ.Update;
  widget_donut_occ.centerLabel = {
    text: "1077", attrs: {
      fill: "#1BA81D", "font-size": "25em", "font-family": "Calibri", "font-weight": "bold"
    }
  };
  widget_donut_occ.centerLabelTitle = {
    text: _Language.get('NLS_WIDGET_MACHINE_OCC_IN_SESSION'), attrs: {
      fill: "#1BA81D", "font-size": "15em", "font-family": "Calibri", "font-weight": "bold"
    },
    NLS: "NLS_WIDGET_MACHINE_OCC_IN_SESSION"
  };

  //widget_donut_occ.OnGraphClick = function () {
  //  this.Update(true);
  //}
  widget_donut_occ.OnBeforeUpdate = function (Chart) {
    var _total_occupation_in_session = 0,
        _total_idle = 0,
        _total_unregistered = 0,
        _total_items = 0;

    if (!WSIData.SiteDashboard) {
      _total_occupation_in_session = _Manager.Terminals.OccupationData.Total - _Manager.Terminals.OccupationData.Occupation["Idle"];
      _total_idle = _Manager.Terminals.OccupationData.Total - _total_occupation_in_session;
      _total_unregistered = _Manager.Terminals.OccupationData.Gender["Unknown"];
      _total_items = _Manager.Terminals.OccupationData.Total;
    } else {
      _total_occupation_in_session = (_Manager.Terminals.OccupationData.Total + _Manager.OtherTerminals.OccupationData.Total) - (_Manager.Terminals.OccupationData.Occupation["Idle"] + _Manager.OtherTerminals.OccupationData.Occupation["Idle"]);
      _total_idle = (_Manager.Terminals.OccupationData.Total + _Manager.OtherTerminals.OccupationData.Total) - (_total_occupation_in_session);
      _total_unregistered = _Manager.Terminals.OccupationData.Gender["Unknown"] + _Manager.OtherTerminals.OccupationData.Gender["Unknown"];
      _total_items = _Manager.Terminals.OccupationData.Total + _Manager.OtherTerminals.OccupationData.Total;
    }

    Chart.values = [];
    Chart.labels = [];
    var _acc = 0;
    Chart.labels.push('NLS_WIDGET_MACHINE_OCC_IN_SESSION');
    Chart.labels.push('NLS_WIDGET_MACHINE_OCC_ACTIVE');

    //Chart.labels.push("Total");
    Chart.values.push(_total_occupation_in_session);
    Chart.values.push(_total_idle);
    //Chart.values.push(_total_items);

    Chart.centerLabel.text = parseCurrency(Math.round((_total_occupation_in_session * 100) / _total_items)) + "%";

  }

  /******************************************************************************
   * WIDGET SEGMENTACION EDAD
   ******************************************************************************/
  widget_age_segment.id = "widgetAgeSegmentation";
  widget_age_segment.SetTemplate(m_widgets_templates.DonutChart);
  widget_age_segment.defaultColors = ['#535AA1', '#EF8F39', '#C6559B', '#35B4C7', '#9A9C9E', '#434348', '#e4d354', '#8085e8', '#8d4653', '#91e8e1', '#4572A7', '#AA4643', '#89A54E', '#80699B', '#3D96AE', '#DB843D', '#92A8CD', '#A47D7C', '#B5CA92'];
  widget_age_segment.defaultFontSize = '13em';
  widget_age_segment.defaultLabelsFontFamily = 'Calibri';
  widget_age_segment.drawLabels = true;
  widget_age_segment.hideZeros = true;
  widget_age_segment.MeterNameAsTitle = false;
  widget_age_segment.title = _Language.get('NLS_WIDGET_TITLE_SEGMENT_AGE'); // 'SEGMENTACION POR EDAD';
  widget_age_segment.titleNLS = 'NLS_WIDGET_TITLE_SEGMENT_AGE';
  widget_age_segment.InfoNLS = "NLS_WIDINF_SEGMENT_AGE";
  widget_age_segment.OnRealTimeComplete = widget_age_segment.Update;
  widget_age_segment.centerLabel = {
    text: "1077", attrs: {
      fill: "white", "font-size": "25em", "font-family": "Calibri", "font-weight": "bold"
    }
  };
  widget_age_segment.centerLabelTitle = {
    text: _Language.get('NLS_CHART_OPTION_STATS_TOTAL'), attrs: {
      fill: "white", "font-size": "15em", "font-family": "Calibri", "font-weight": "bold"
    },
    NLS: "NLS_CHART_OPTION_STATS_TOTAL"
  };

  //widget_age_segment.OnGraphClick = function () {
  //  this.Update(true);
  //}
  widget_age_segment.OnBeforeUpdate = function (Chart) {

    var _age_segmentation = [];

    for (var _idx in _Manager.Terminals.OccupationData.Ages) {
      _age_segmentation[_idx] = _Manager.Terminals.OccupationData.Ages[_idx];
    }

    if (WSIData.SiteDashboard) {
      for (var _idx in _Manager.OtherTerminals.OccupationData.Ages) {
        if (_age_segmentation[_idx]) {
          _age_segmentation[_idx] += _Manager.OtherTerminals.OccupationData.Ages[_idx];
        } else {
          _age_segmentation[_idx] = _Manager.OtherTerminals.OccupationData.Ages[_idx];
        }
      }
    }

    Chart.values = [];
    Chart.labels = [];
    var _acc = 0;

    for (var _idx in _age_segmentation) {
      //if(_idx != "Unknown")
      _acc += _age_segmentation[_idx];
    }

    var _idx_seg = undefined;

    for (_idx_seg in _age_segmentation) {
      Chart.labels.push(_idx_seg);
      Chart.values.push(_age_segmentation[_idx_seg]);
    }

    Chart.centerLabel.text = FormatString(_acc, EN_FORMATS.F_NUMBER, 0);

  }

  /******************************************************************************
  * WIDGET SEGMENTACION NIVEL
  ******************************************************************************/
  widget_level_segment.id = "widgetLevelSegmentation";
  widget_level_segment.SetTemplate(m_widgets_templates.DonutChart);
  widget_level_segment.defaultColors = ['#535AA1', '#EF8F39', '#C6559B', '#35B4C7', '#9A9C9E', '#434348', '#e4d354', '#8085e8', '#8d4653', '#91e8e1', '#4572A7', '#AA4643', '#89A54E', '#80699B', '#3D96AE', '#DB843D', '#92A8CD', '#A47D7C', '#B5CA92'];
  widget_level_segment.defaultFontSize = '13em';
  widget_level_segment.defaultLabelsFontFamily = 'Calibri';
  widget_level_segment.drawLabels = true;
  widget_level_segment.hideZeros = true;
  widget_level_segment.MeterNameAsTitle = false;
  widget_level_segment.titleNLS = 'NLS_WIDGET_TITLE_SEGMENT_LEVEL';
  widget_level_segment.title = _Language.get('NLS_WIDGET_TITLE_SEGMENT_LEVEL'); // 'SEGMENTACION POR NIVEL';
  widget_level_segment.InfoNLS = "NLS_WIDINF_SEGMENT_LEVEL";
  widget_level_segment.OnRealTimeComplete = widget_level_segment.Update;
  widget_level_segment.centerLabel = {
    text: "1077", attrs: {
      fill: "white", "font-size": "25em", "font-family": "Calibri", "font-weight": "bold"
    }
  };
  widget_level_segment.centerLabelTitle = {
    text: _Language.get('NLS_CHART_OPTION_STATS_TOTAL'), attrs: {
      fill: "white", "font-size": "15em", "font-family": "Calibri", "font-weight": "bold"
    },
    NLS: "NLS_CHART_OPTION_STATS_TOTAL"
  };

  //widget_level_segment.OnGraphClick = function () {
  //  this.Update(true);
  //}
  widget_level_segment.OnBeforeUpdate = function (Chart) {

    var _level_segmentation = [];

    for (var _idx in _Manager.Terminals.OccupationData.Levels) {
      _level_segmentation[_idx] = _Manager.Terminals.OccupationData.Levels[_idx];
    }

    if (WSIData.SiteDashboard) {
      for (var _idx in _Manager.OtherTerminals.OccupationData.Levels) {
        if (_level_segmentation[_idx]) {
          _level_segmentation[_idx] += _Manager.OtherTerminals.OccupationData.Levels[_idx];
        } else {
          _level_segmentation[_idx] = _Manager.OtherTerminals.OccupationData.Levels[_idx];
        }
      }
    }

    Chart.values = [];
    Chart.labels = [];
    var _acc = 0;

    for (var _idx in _level_segmentation) {
      //if(_idx != "Unknown")
      _acc += _level_segmentation[_idx];
    }

    //var occupationInSession = (_Manager.Terminals.OccupationData.Total + _Manager.OtherTerminals.OccupationData.Total) - (_Manager.Terminals.OccupationData.Occupation["Idle"] + _Manager.OtherTerminals.OccupationData.Occupation["Idle"]);
    //var unknownValue = occupationInSession - _acc;
    /*
    for (var i = 0; i < _Manager.Ranges.length; i++) {
      var _levelDenom=_Manager.Ranges[i].Range[1]

      if (_level_segmentation[_levelDenom]) {
        Chart.labels.push(_levelDenom)
        Chart.values.push(_level_segmentation[_levelDenom]);
    }
    }
    if (_level_segmentation["Unknown"]) {
      Chart.labels.push("Unknown")
      Chart.values.push(_level_segmentation["Unknown"]);
    }
    */

    for (index in Object.keys(_level_segmentation)) {
      var level_key = Object.keys(_level_segmentation)[index];
      var level_value = _level_segmentation[level_key];
      Chart.labels.push(level_key)
      Chart.values.push(level_value);
    }




    //if (_level_segmentation["VIP"]) {
    //  Chart.labels.push("VIP")
    //  Chart.values.push(_level_segmentation["VIP"]);
    //}
    //if (_level_segmentation["Bronze"]) {
    //  Chart.labels.push("Bronze")
    //  Chart.values.push(_level_segmentation["Bronze"]);
    //}
    //if (_level_segmentation["Gold"]) {
    //  Chart.labels.push(_Language.get('NLS_GOLD'))
    //  Chart.values.push(_level_segmentation["Gold"]);
    //}
    //if (_level_segmentation["Platinum"]) {
    //  Chart.labels.push("Platinum")
    //  Chart.values.push(_level_segmentation["Platinum"]);
    //}
    //if (_level_segmentation["Unknown"]) {
    //  Chart.labels.push(_Language.get('NLS_WIDGET_UNKNOWN'));
    //  Chart.values.push(_level_segmentation["Unknown"]);
    //}

    Chart.centerLabel.text = FormatString(_acc, EN_FORMATS.F_NUMBER, 0);
  }

  //----------------------
  widget_indicator_netwin.id = "widgetIndicator3";
  widget_indicator_netwin.SetTemplate(m_widgets_templates.IndicatorMonetary);
  widget_indicator_netwin.SetPropertyValue("value", "");
  widget_indicator_netwin.SetPropertyValue("value2", "");
  widget_indicator_netwin.SetPropertyValue("value3", "");
  widget_indicator_netwin.SetPropertyValue("value4", "");
  widget_indicator_netwin.title = _Manager.GeneralParams["Casino.Name"];
  widget_indicator_netwin.InfoNLS = "NLS_WIDGET_NETWIN_INFO";
  widget_indicator_netwin.OnRealTimeComplete = widget_indicator_netwin.Update;
  widget_indicator_netwin.MeterNameAsTitle = false;
  widget_indicator_netwin.OnUpdate = function (Widget) {

    var _obj = $("#" + Widget.id),
        _value_comp = _Manager.MetersManager.GetMeterValueByName("NET WIN", 0, "MTD", "davg"),
        _value = (_Manager.Terminals.MonetaryData.CoinIn - _Manager.Terminals.MonetaryData.Won),
        _value2 = testMode == "true" ? 302317.56 : _Manager.MetersManager.GetMeterValueByName("NET WIN", 0, "TDY", "dacc"),
        _value3 = _Manager.Terminals.MonetaryData.CoinIn,
        _value4 = testMode == "true" ? 5340895.39 : _Manager.MetersManager.GetMeterValueByName("PLAYED", 0, "TDY", "dacc");

    if (_value_comp == '') {
      _value_comp = 0;
    }

    // Indicator
    if (_value < _value_comp) {
      _obj.find(".indicator_icon").each(function (i, item) {
        $(item).removeClass('news_ico_up').addClass('news_ico_down');
      });
    }

    SwapClassByValue(_value, '#value', 'labelWidgetGreen', 'labelWidgetRed');
    SwapClassByValue(_value3, '#value3', 'labelWidgetGreen', 'labelWidgetRed');

    Widget.SetPropertyValue("value", _value);
    Widget.SetPropertyValue("value2", _value2);
    Widget.SetPropertyValue("value3", _value3);
    Widget.SetPropertyValue("value4", _value4);
    Widget.UpdateProperties();
  }


  /******************************************************************************
  * WIDGET OCUPACION DE SALA
  ******************************************************************************/
  widget_donut_room_occ.id = "widgetRoomOcc";
  widget_donut_room_occ.SetTemplate(m_widgets_templates.DonutChart);
  widget_donut_room_occ.defaultColors = ["#535AA1", "#606060", "#828282"];
  widget_donut_room_occ.defaultFontSize = '13em';
  widget_donut_room_occ.defaultLabelsFontFamily = 'Calibri';
  widget_donut_room_occ.drawLabels = true;
  widget_donut_room_occ.showTotal = true;
  widget_donut_room_occ.colorTotal = "white";
  widget_donut_room_occ.labelTotal = _Language.get('NLS_WIDGET_SITE_OCCUPATION_TOTAL');
  widget_donut_room_occ.MeterNameAsTitle = false;
  widget_donut_room_occ.title = _Language.get('NLS_WIDGET_SITE_OCC_TITLE'); // 'OCUPACION DE SALA';
  widget_donut_room_occ.titleNLS = 'NLS_WIDGET_SITE_OCC_TITLE';
  widget_donut_room_occ.InfoNLS = "NLS_WIDGET_SITE_OCC_INFO";
  widget_donut_room_occ.OnRealTimeComplete = widget_donut_room_occ.Update;
  widget_donut_room_occ.centerLabel = {
    text: "1077", attrs: {
      fill: "#535AA1", "font-size": "25em", "font-family": "Calibri", "font-weight": "bold"
    }
  };
  widget_donut_room_occ.centerLabelTitle = {
    text: _Language.get('NLS_WIDGET_SITE_OCCUPATION_CURRENT'), attrs: {
      fill: "#535AA1", "font-size": "15em", "font-family": "Calibri", "font-weight": "bold"
    },
    NLS: "NLS_WIDGET_SITE_OCCUPATION_CURRENT"
  };

  widget_donut_room_occ.OnBeforeUpdate = function (Chart) {

    var _total_in_site = 0;
    var _total_capacity = 0;
    var _total_idle = 0;

    widget_donut_room_occ.labelTotal = _Language.get('NLS_WIDGET_SITE_OCCUPATION_TOTAL');

    _total_capacity = parseInt(_Manager.GeneralParams["Intellia.Site.Capacity"]); // Aforo
    _total_in_site = _Manager.Occupancy > 0 ? _Manager.Occupancy : 0; // Current

    _total_in_site = _total_in_site > _total_capacity ? _total_capacity : _total_in_site;

    _total_idle = _total_capacity - _total_in_site;

    Chart.values = [];
    Chart.labels = [];

    Chart.labels.push(_Language.get('NLS_WIDGET_SITE_OCCUPATION_CURRENT'));
    Chart.labels.push(_Language.get('NLS_WIDGET_MACHINE_OCC_ACTIVE'));

    Chart.values.push(parseInt(_total_in_site));
    Chart.values.push(parseInt(_total_idle));

    Chart.centerLabel.text = parseCurrency(Math.round((_total_in_site * 100) / _total_capacity)) + "%";;
  }

  /******************************************************************************
  * WIDGET OCUPACION MAQUINA vs OCUPACION SALA
  ******************************************************************************/
  widget_donut_machine_occ_room_occ.id = "widgetMachineOccRoomOcc";
  widget_donut_machine_occ_room_occ.SetTemplate(m_widgets_templates.DonutChart);
  widget_donut_machine_occ_room_occ.defaultColors = ["#1BA81D", "#606060", "#828282"];
  widget_donut_machine_occ_room_occ.defaultFontSize = '13em';
  widget_donut_machine_occ_room_occ.defaultLabelsFontFamily = 'Calibri';
  widget_donut_machine_occ_room_occ.drawLabels = true;
  widget_donut_machine_occ_room_occ.showTotal = true;
  widget_donut_machine_occ_room_occ.colorTotal = "#535AA1";
  widget_donut_machine_occ_room_occ.labelTotal = _Language.get('NLS_WIDGET_SITE_OCCUPATION_CURRENT');
  widget_donut_machine_occ_room_occ.MeterNameAsTitle = false;
  widget_donut_machine_occ_room_occ.title = _Language.get('NLS_WIDGET_MACHINE_OCC_SITE_OCC_TITLE'); // 'OCUPACION MAQUINA vs OCUPACION SALA';
  widget_donut_machine_occ_room_occ.titleNLS = 'NLS_WIDGET_MACHINE_OCC_SITE_OCC_TITLE';
  widget_donut_machine_occ_room_occ.InfoNLS = "NLS_WIDGET_MACHINE_OCC_SITE_OCC_INFO";
  widget_donut_machine_occ_room_occ.OnRealTimeComplete = widget_donut_machine_occ_room_occ.Update;
  widget_donut_machine_occ_room_occ.centerLabel = {
    text: "1077", attrs: {
      fill: "#1BA81D", "font-size": "25em", "font-family": "Calibri", "font-weight": "bold"
    }
  };
  widget_donut_machine_occ_room_occ.centerLabelTitle = {
    text: _Language.get('NLS_WIDGET_MACHINE_OCC_IN_SESSION'), attrs: {
      fill: "#1BA81D", "font-size": "15em", "font-family": "Calibri", "font-weight": "bold"
    },
    NLS: "NLS_WIDGET_MACHINE_OCC_IN_SESSION"
  };

  widget_donut_machine_occ_room_occ.OnBeforeUpdate = function (Chart) {
    var _total_in_site = 0;
    var _total_occupation_in_session = 0;
    var _total_idle = 0;

    widget_donut_machine_occ_room_occ.labelTotal = _Language.get('NLS_WIDGET_SITE_OCCUPATION_CURRENT');

    _total_in_site = _Manager.Occupancy; // Current

    if (!WSIData.SiteDashboard) {
      _total_occupation_in_session = _Manager.Terminals.OccupationData.Total - _Manager.Terminals.OccupationData.Occupation["Idle"];
    } else {
      _total_occupation_in_session = (_Manager.Terminals.OccupationData.Total + _Manager.OtherTerminals.OccupationData.Total) - (_Manager.Terminals.OccupationData.Occupation["Idle"] + _Manager.OtherTerminals.OccupationData.Occupation["Idle"]);
    }

    if (_total_in_site < _total_occupation_in_session)
      _total_in_site = _total_occupation_in_session;

    _total_idle = _total_in_site - _total_occupation_in_session;

    Chart.values = [];
    Chart.labels = [];

    Chart.labels.push('NLS_WIDGET_MACHINE_OCC_IN_SESSION');
    Chart.labels.push('NLS_WIDGET_MACHINE_OCC_SITE_OCC_FREE_OCC');

    Chart.values.push(parseInt(_total_occupation_in_session));
    Chart.values.push(parseInt(_total_idle));

    if (_total_in_site > 0)
      Chart.centerLabel.text = parseCurrency(Math.round((_total_occupation_in_session * 100) / _total_in_site)) + "%";
    else
      Chart.centerLabel.text = "100%";

  }

}


/******************************************************************************
* WIDGET LISTA RUNNERS
******************************************************************************/

widget_runners_list.id = 'widgetRunnersList';
//CSR - 04/05/2016
widget_runners_list.showCounter = false;
widget_runners_list.hasScroll = false;

widget_runners_list.title = _Language.get("NLS_WIDGET_TITLE_RUNNERS"); //"RUNNERS";
widget_runners_list.titleNLS = "NLS_WIDGET_TITLE_RUNNERS"; //"RUNNERS";
widget_runners_list.InfoNLS = 'NLS_WIDGET_RUNNERS_INFO';
widget_runners_list.BindEventsToClass = '.itemrolesdevice';
widget_runners_list.MeterNameAsTitle = false;
widget_runners_list.OnRealTimeComplete = widget_runners_list.Update;
widget_runners_list.MeterNameAsTitle = false;

widget_runners_list.OnItemClick = function (List, Item) {
  var _runner_id;
  var _function;
  //_UI.SetMenuActiveSection($("#a_section_60")[0], CONST_SECTION_ID_PLAYERS);
  //_UI.SetMenuActiveSection($("#a_section_40")[0], CONST_SECTION_ID_ALARMS);

  _runner_id = $(Item).parent().parent().attr("item-index").split("_")[1];

  for (_idx_widget_runners in _Manager.Runners.Items) {
    if (_Manager.Runners.Items[_idx_widget_runners].id == _runner_id) {
      //GotoRunner(_Manager.Runners.Items[_idx_widget_runners]);
      //_function = m_function_wrapper(GotoRunner, this, [_runner_id]);
      //m_function_queue.push(_function);
      //_UI.SetMenuActiveSection($("#a_section_60")[0], CONST_SECTION_ID_PLAYERS);
      _UI.SetMenuActiveSection($("#a_section_40")[0], CONST_SECTION_ID_ALARMS);
      LocateRunner(this, _runner_id)
      break;
    }
  }

};
widget_runners_list.OnItemPrepared = function (Item, ItemPrepared) {
  var _$item = $(ItemPrepared);
  var _$target = _$item.find(".itemrolescontainer");

  if (Item.roles && _$target.length == 1) {

    for (var _r in Item.roles) {
      var _role = Item.roles[_r];
      var _rdata = "";
      switch (_role) {
        case 1:
          _rdata = "<div class='itemroleitem itemrole-1'>PCA</div>";
          break;
        case 2:
          _rdata = "<div class='itemroleitem itemrole-2'>OPS</div>";
          break;
        case 4:
          _rdata = "<div class='itemroleitem itemrole-4'>SLA</div>";
          break;
        case 8:
          _rdata = "<div class='itemroleitem itemrole-8'>TS</div>";
          break;
      }
      if (_rdata != "") { _$target.append(_rdata); }
    }
    return _$item[0].outerHTML;
  } else {
    return ItemPrepared;
  }
};
widget_runners_list.OnBeforeUpdate = function (List) {
  // Get Runners
  //_Manager.Runners
  for (var _idx in _Manager.Runners.Items) {
    var _runner = _Manager.Runners.Items[_idx];
    if (_runner) {
      // Check floor if
      //if (_CurrentFloor3.id === _runner.floorId || WSIData.SiteDashboard) {

      var _new_item = new WidgetItemBase('Runner_' + _runner.id, m_widgets_templates.RunnerListItem);
      _new_item.SetPropertyValue("name", _runner.userName);
      _new_item.SetPropertyValue("device", _runner.deviceId);

      var _roles = [8,4,2,1];
      var _u_roles = _runner.roles;
      var _role_list = [];
      for (var _i in _roles) {
        var _role = _roles[_i];
        if (_u_roles - _role >= 0) {
          _role_list.push(_role);
          _u_roles -= _role;
        }
      }
      _new_item.roles = _role_list.reverse();
      if (!List.TotalItems[_idx])
        List.AddItem(_new_item);
      //}
    }
  }
};



//****************************
//  WIDGET BIRTHDAY
//****************************

widget_birthday.id = 'widgetBirthday';
widget_birthday.title = _Language.get('NLS_WIDGET_BIRTHDAY_TITLE');
widget_birthday.titleNLS = "NLS_WIDGET_BIRTHDAY_TITLE";
widget_birthday.InfoNLS = "NLS_WIDGET_BIRTHDAY_INFO";
//CSR - 04/05/2016
widget_birthday.hasScroll = false;
widget_birthday.showCounter = false;

widget_birthday.BindEventsToClass = '.itemtracerectBirthD';
widget_birthday.OnRealTimeComplete = widget_birthday.OnUpdate;
widget_birthday.MeterNameAsTitle = false;

//widget_birthday.SetTemplate(m_widgets_templates.BirthDay);
widget_birthday.OnItemClick = function (List, Item) {

  WSIData.SelectedObject = $(Item).parent().find("[data-property-name='identifier']").text();

  _UI.SetMenuActiveSection($("#a_section_60")[0], CONST_SECTION_ID_PLAYERS);

  LocateTerminal(WSIData.SelectedObject, this);



};
widget_birthday.OnBeforeUpdate = function (List) {
  List.TotalItems = [];
  for (var _idx_birth_d in _Manager.Terminals.ByAccount) {
    var _new_item = new WidgetItemBase('track-data-' + _idx_birth_d, m_widgets_templates.BirthDay);
    var _term = _Manager.Terminals.Items[_Manager.Terminals.ByAccount[_idx_birth_d]].Properties;

    if (CheckBirthDate(_term.player_birth_date)) {
      if (_term.player_is_vip == "1") {
        _new_item.SetPropertyValue("isvip", "VIP");
      }
      else {
        _new_item.SetPropertyValue("isvip", " ");
      }
      if (_term.player_age > -1) {
        _new_item.SetPropertyValue("name", _term.player_first_name);
        _new_item.SetPropertyValue("last-name", _term.player_last_name1 + ' ' + _term.player_last_name2);
        _new_item.SetPropertyValue("lvl", _term.player_level_name);
        _new_item.SetPropertyValue("machine_name", "" + _term.provider_name.replace("[", "").replace("]", "") + " " + _term.name.replace("[", "").replace("]", ""));
        _new_item.SetPropertyValue("ages", _term.player_age);
        _new_item.SetPropertyValue("identifier", _term.lo_id);

        if (!(_idx_birth_d in List.TotalItems))
          List.AddItem(_new_item);
      }
    }
  }

  if (List.Items.length > 0)
    List.Items.sort(comparison);
}

// ordenamiento de mayor a menor, si se quiere hacer de menor a mayor,
// invertir resta a: elem1.properties.ages - elem2.properties.ages
function comparison(elem1, elem2) {
  return elem2.properties.ages - elem1.properties.ages;
}

widget_birthday.OnUpdate = function (Widget) {
  // TODO: SUM
  Widget.UpdateTitle(_Language.get(Widget.titleNLS) + " [" + _data_total.total + "]");
}

//****************************,
//  WIDGET PLAYERS MAX LVL
//****************************

widget_players_max_level.id = 'widgetPlayersMaxLevel';
widget_players_max_level.title = _Language.get('NLS_WIDGET_PLAYERS_MAX_LVL_TITLE');
widget_players_max_level.titleNLS = "NLS_WIDGET_PLAYERS_MAX_LVL_TITLE";
widget_players_max_level.InfoNLS = "NLS_WIDGET_PLAYERS_MAX_LVL_INFO";
widget_players_max_level.maxItems = 15;
widget_players_max_level.hasScroll = false;
widget_players_max_level.showCounter = false;
widget_players_max_level.BindEventsToClass = '.itemtracerectMaxLvlPlayer';
widget_players_max_level.OnRealTimeComplete = UpdateWidgetMaxLvlPlayers;
widget_players_max_level.MeterNameAsTitle = false;

widget_players_max_level.OnItemClick = function (List, Item) {

  WSIData.SelectedObject = $(Item).parent().find("[data-property-name='identifier']").text();

  _UI.SetMenuActiveSection($("#a_section_60")[0], CONST_SECTION_ID_PLAYERS);

  LocateTerminal(WSIData.SelectedObject, this);
};
widget_players_max_level.OnBeforeUpdate = function (List) {

  List.TotalItems = [];

  var _list_count = 0;
  UpdateWidgetMaxLvlPlayers();
  for (var _idx_mlp in widget_players_max_level.Rows) {
    var _new_item = new WidgetItemBase('track-data-' + _idx_mlp, m_widgets_templates.MaxLvlPlayers);
    var _term = widget_players_max_level.Rows[_idx_mlp];

    var _term_obj = _Manager.Terminals.Items[_term.lo_id];

    if (_term_obj.Properties.player_first_name == "Anonymous") {
      continue;
    }

    var decodedLevel = "";
    //var level = _term.player_level_name.split('.').length;
    //switch (_term_obj.Properties.player_level) {
    //  case "1":
    //    decodedLevel = "Bronze";
    //    break;
    //  case "2":
    //    decodedLevel = "Gold";
    //    break;
    //  case "3":
    //    decodedLevel = "Platinum";
    //    break;
    //  case "4":
    //    decodedLevel = "VIP";
    //    break;
    //  default:
    //    decodedLevel = _Language.get('NLS_WIDGET_UNKNOWN');
    //    break;
    //}

    decodedLevel = _Manager.Ranges.BuscaNivel(_term_obj.Properties.player_level);


    _new_item.SetPropertyValue("name", _term.player_first_name);
    _new_item.SetPropertyValue("last-name", _term.player_last_name1 + ' ' + _term.player_last_name2);
    _new_item.SetPropertyValue("lvl", decodedLevel.toUpperCase());
    _new_item.SetPropertyValue("machine_name", "" + _term.term_provider + " " + _term.term_name.replace("[", "").replace("]", ""));
    _new_item.SetPropertyValue("identifier", _term.lo_id);


    if (isNaN(+_term.coin_in)) {
      _new_item.SetPropertyValue("coin_in", FormatString(parseFloat(0).toFixed(2), EN_FORMATS.F_MONEY));
    } else {
      _new_item.SetPropertyValue("coin_in", FormatString(parseFloat(+_term.coin_in).toFixed(2), EN_FORMATS.F_MONEY));
    }

    if (isNaN(+_term.bet_avg)) {
      _new_item.SetPropertyValue("bet_avg", FormatString(parseFloat(0).toFixed(2), EN_FORMATS.F_MONEY));
    } else {
      _new_item.SetPropertyValue("bet_avg", FormatString(parseFloat(+_term.bet_avg).toFixed(2), EN_FORMATS.F_MONEY));
    }


    _new_item.SetPropertyValue("bet_avg_label", _Language.get('NLS_WIDGET_PLAYERS_MAX_LVL_BET_AVG'));
    _new_item.SetPropertyValue("coin_in_label", _Language.get('NLS_WIDGET_PLAYERS_MAX_LVL_COIN_IN'));

    if (testMode == "true") {
      if (!List.TotalItems[_idx_mlp] && _list_count < 5) {
        List.AddItem(_new_item);
        _list_count++;
      }
    }
    else {
      if (!List.TotalItems[_idx_mlp])
        List.AddItem(_new_item);
    }

  }

}


widget_players_max_level.OnItemPrepared = function (Item, ItemPrepared) {
  var _obj = $(ItemPrepared);
  var _result;

  if (Item.properties.machine_name == "") {
    _obj.find(".itemtracerectMaxLvlPlayer").each(function (i, item) {
      $(item).hide();
    });
  }
  _result = _obj.outerHTML();
  return _result;
}

// *****************************************************************************************************************



//******************************************************************************

// TODO: JVV -> Add widget to list (previously create the object)
var m_widgets_list = loadWidgetsList();

function loadWidgetsList() {
  var _widgets_list = {
  };
  _widgets_list.widgetTasks1 = widget_tasks1;
  _widgets_list.widgettheater2 = widget_amphit_coin_in;
  _widgets_list.widgetOccupancy1 = widget_amphit_occ;
  //_widgets_list.widgetIndicator1 = widget_indicator1;
  //_widgets_list.widgetIndicator2 = widget_indicator2;
  _widgets_list.widgetTestList2 = widget_top_winning_players;
  //LA SDS -03-05-2016
  //_widgets_list.widgetTestList1 = widget_top_10_coin_in;

  //_widgets_list.widgetColumns1 = widget_columns1;
  _widgets_list.widgettheater3 = widget_amphit_bet_avg;
  //_widgets_list.widgetComparation1 = widget_comparation1;

  //LA SDS -03-05-2016
  // _widgets_list.widgetChart1 = widget_monitor_bet_avg;

  //_widgets_list.widgetHBar1 = widget_hbar;
  _widgets_list.widgetChart2 = widget_occ_vs_weather;
  _widgets_list.widgetDonut1 = widget_donut_gender;
  _widgets_list.widgetMachineOccupation = widget_donut_occ;
  _widgets_list.widgetVipsList = widget_vip_list;
  _widgets_list.widgetIndicator3 = widget_indicator_netwin;
  _widgets_list.widgetAgeSegmentation = widget_age_segment;
  _widgets_list.widgetLevelSegmentation = widget_level_segment;
  _widgets_list.widgetRunnersList = widget_runners_list;
  _widgets_list.widgetBirthday = widget_birthday;
  _widgets_list.widgetPlayersMaxLevel = widget_players_max_level

  _widgets_list.widgetRoomOcc = widget_donut_room_occ;
  _widgets_list.widgetMachineOccRoomOcc = widget_donut_machine_occ_room_occ;

  return _widgets_list;
}


function GetWeatherIcons(WeatherList) {
  var _list_icons = [];


  for (_idx_w_icons in WeatherList) {
    var _uri_icon = "";

    switch (+WeatherList[_idx_w_icons]) {
      case 0:
        _uri_icon = './design/iconos/meteo/Sunny.gif';
        break;
      case 1:
        _uri_icon = './design/iconos/meteo/PartlyCloudyDay.gif';
        break;
      case 2:
        _uri_icon = './design/iconos/meteo/Cloudy.gif';
        break;
      case 3:
        _uri_icon = './design/iconos/meteo/Overcast.gif';
        break;
      case 10:
        _uri_icon = './design/iconos/meteo/Mist.gif';
        break;
      case 21:
        _uri_icon = './design/iconos/meteo/OccLightRain.gif';
        break;
      case 22:
        _uri_icon = './design/iconos/meteo/IsoSleetSwrsDay.gif';
        break;
      case 23:
        _uri_icon = './design/iconos/meteo/OccLightSleet.gif';
        break;
      case 24:
        _uri_icon = './design/iconos/meteo/FreezingDrizzle.gif';
        break;
      case 29:
        _uri_icon = './design/iconos/meteo/PartCloudRainThunderDay.gif';
        break;
      case 38:
        _uri_icon = './design/iconos/meteo/ModSnow.gif';
        break;
      case 39:
        _uri_icon = './design/iconos/meteo/Blizzard.gif';
        break;
      case 45:
        _uri_icon = './design/iconos/meteo/Fog.gif';
        break;
      case 49:
        _uri_icon = './design/iconos/meteo/FreezingFog.gif';
        break;
      case 50:
        _uri_icon = './design/iconos/meteo/IsoRainSwrsDay.gif';
        break;
      case 51:
        _uri_icon = './design/iconos/meteo/OccLightRain.gif';
        break;
      case 56:
        _uri_icon = './design/iconos/meteo/FreezingDrizzle.gif';
        break;
      case 57:
        _uri_icon = './design/iconos/meteo/FreezingDrizzle.gif';
        break;
      case 60:
        _uri_icon = './design/iconos/meteo/OccLightRain.gif';
        break;
      case 61:
        _uri_icon = './design/iconos/meteo/ModRain.gif';
        break;
      case 62:
        _uri_icon = './design/iconos/meteo/ModRainSwrsDay.gif';
        break;
      case 63:
        _uri_icon = './design/iconos/meteo/ModRain.gif';
        break;
      case 64:
        _uri_icon = './design/iconos/meteo/HeavyRainSwrsDay.gif';
        break;
      case 65:
        _uri_icon = './design/iconos/meteo/HeavyRain.gif';
        break;
      case 66:
      case 67:
        _uri_icon = './design/iconos/meteo/FreezingRain.gif';
        break;
      case 68:
        _uri_icon = './design/iconos/meteo/ModSleet.gif';
        break;
      case 69:
        _uri_icon = './design/iconos/meteo/SunnyHeavySleet.gif';
        break;
      case 70:
      case 71:
        _uri_icon = './design/iconos/meteo/OccLightSnow.gif';
        break;
      case 72:
      case 73:
        _uri_icon = './design/iconos/meteo/ModSnow.gif';
        break;
      case 74:
        _uri_icon = './design/iconos/meteo/HeavySnowSwrsDay.gif';
        break;
      case 75:
        _uri_icon = './design/iconos/meteo/HeavySnow.gif';
        break;
      case 79:
        _uri_icon = './design/iconos/meteo/FreezingRain.gif';
        break;
      case 80:
        _uri_icon = './design/iconos/meteo/IsoRainSwrsDay.gif';
        break;
      case 81:
        _uri_icon = './design/iconos/meteo/ModRainSwrsDay.gif';
        break;
      case 82:
        _uri_icon = './design/iconos/meteo/HeavyRain.gif';
        break;
      case 83:
        _uri_icon = './design/iconos/meteo/ModSleetSwrsDay.gif';
        break;
      case 84:
        _uri_icon = './design/iconos/meteo/ModSleetSwrsDay.gif';
        break;
      case 85:
        _uri_icon = './design/iconos/meteo/IsoSnowSwrsDay.gif';
        break;
      case 86:
        _uri_icon = './design/iconos/meteo/ModSnowSwrsDay.gif';
        break;
      case 87:
      case 88:
        _uri_icon = './design/iconos/meteo/FreezingRain.gif';
        break;
      case 91:
        _uri_icon = './design/iconos/meteo/PartCloudRainThunderDay.gif';
        break;
      case 92:
        _uri_icon = './design/iconos/meteo/CloudRainThunder.gif';
        break;
      case 93:
        _uri_icon = './design/iconos/meteo/PartCloudSleetSnowThunderDay.gif';
        break;
      case 94:
        _uri_icon = './design/iconos/meteo/CloudSleetSnowThunder.gif';
        break;
      default:
        _uri_icon = './design/iconos/meteo/Sunny.gif';
        break;



    }
    _list_icons.push(_uri_icon);
  }


  return _list_icons;
}

function getDayOfWeek(DayNum) {

  var _result = "";
  switch (+DayNum) {
    case 1:
      _result = _Language.get("NLS_DOW_SUNDAY");
      break;

    case 2:
      _result = _Language.get("NLS_DOW_MONDAY");
      break;

    case 3:
      _result = _Language.get("NLS_DOW_TUESDAY");
      break;

    case 4:
      _result = _Language.get("NLS_DOW_WEDNESDAY");
      break;

    case 5:
      _result = _Language.get("NLS_DOW_THURSDAY");
      break;

    case 6:
      _result = _Language.get("NLS_DOW_FRIDAY");
      break;

    case 7:
      _result = _Language.get("NLS_DOW_SATURDAY");
      break;

    default:
      break;
  }

  return _result;

}

function IsUsedInDemo(Terminal, Type) {
  var _trackdata = Terminal.player_trackdata;
  for (var i = 0; i < m_used_in_demo.length; i++) {
    var _arr = m_used_in_demo[i].split("|");
    if (_arr.length > 1) {
      var _used_track_data = _arr[0];
      var _type = _arr[1];

      if (_used_track_data == _trackdata) {
        if (_type != Type) {
          return true;
        }

        // Es un registro que se encuentra en el mismo widgeth con lo cual si debe añadirse y lo tratamos como nuevo. 
        return false;
      }
    }
  }

  if (_used_track_data != _trackdata) {
    m_used_in_demo[m_used_in_demo.length] = _trackdata + "|" + Type;
  }

  return false;
}