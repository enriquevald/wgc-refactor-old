﻿function WSIMaterials() {

    this.Textures = new Object();
    this.Items = new Object();

    /*
    this.Textures_loaded = 0;
    this.Textures_errors = 0;
    */

}

WSIMaterials.prototype.AddTexture = function (url, key, mapping) {

    if (!url) {
        throw new Error("No url specified!");
        return;
    }

    if (!key) {
        throw new Error("No key specified for the texture!");
        return;
    }

    if (!mapping) {
        mapping = false;
    }

    this.Textures[key] = THREE.ImageUtils.loadTexture(url, undefined);

}

WSIMaterials.prototype.AddMaterial = function (materialType, key, attribs) {

    if (!materialType) {
        throw new Error("No url specified!");
        return;
    }

    if (!key) {
        throw new Error("No key specified for the texture!");
        return;
    }

    if (!attribs) {
        attribs = new Object();
    }

    attribs["name"] = key;

    switch (materialType) {

        case 2: // Lambert
            this.Items[key] = new THREE.MeshLambertMaterial(attribs);
            this.Items[key].vertexColors = THREE.FaceColors;
            this.Items[key].doubleSidded = true;
            break;
        default: // Basic
            this.Items[key] = new THREE.MeshBasicMaterial(attribs);
            this.Items[key].vertexColors = THREE.FaceColors;
            this.Items[key].doubleSidded = true;
            break;
    }

}

WSIMaterials.prototype.CreateMaterialsByFace = function (materialsArray) {

    var _result = [];

    for (var _i = 0; _i < materialsArray.length; _i++) {

        if (this.Items[materialsArray[_i]]) {
            _result.push(this.Items[materialsArray[_i]].clone());
        } else {
            // Material won't exists, clone default
            
        }

    }

    return _result;

}

