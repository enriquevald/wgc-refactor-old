﻿
////////////////////////////////////////////////////////////////////////////

var CONST_MODE_3D = 1;
var CONST_MODE_2D = 2;
var CONST_MODE_EDITOR = 3;

////////////////////////////////////////////////////////////////////////////

// Selected section
var m_current_section = 10;                     // Current menu section selected

// Save configuration flag
var m_config_has_changed = false;               // Notifier for auto-save current configuration

// Prefixes
var CONST_ID_TERMINAL = "terminal_";            // Terminal object id prefix
var CONST_ID_LABEL = "label_";                  // Terminal label object id prefix
var CONST_ID_SLOT_LABEL = "slot_label_";        // Terminal ground label id prefix
var CONST_ID_AREA = "area_";                    // Area object id prefix
var CONST_ID_ISLAND = "island_";                // Island object id prefix
var CONST_ID_BEACON = "beacon_";
var CONST_ID_RUNNER = "runner_";

// Origin for the events of moving/zooming scene
var CONST_MOVEMENT_BUTTON = "button";
var CONST_MOVEMENT_MOUSE = "mouse";
var CONST_MOVEMENT_TOUCH = "touch";

// Flag for action type with fingers in touchable device
var CONST_MOVEMENT_PINCH = false;
var CONST_MOVEMENT_ROTATE = false;

// Actions on scene typsesignel
var CONST_MOVEMENT_TYPE_ZOOM_MORE = "zoom+";
var CONST_MOVEMENT_TYPE_ZOOM_MINUS = "zoom-";
var CONST_MOVEMENT_TYPE_LEFT = "left";
var CONST_MOVEMENT_TYPE_RIGTH = "rigth";
var CONST_MOVEMENT_TYPE_UP = "up";
var CONST_MOVEMENT_TYPE_DOWN = "down";

// Influence of actions in scene
var CONST_MOVEMENT_PX = 10;
var CONST_MOVEMENT_ZOOM_PX = 0.010;
var CONST_MOVEMENT_ZOOM_WHEEL_PX = 0.050;
var CONST_MOVEMENT_ZOOM_TOUCH_PX = 1.1;

// Limits for Zoom
var CONST_MAX_ZOOM = 3.0;
var CONST_MIN_ZOOM = 0.1;

// Constant transformation origin for 2D scene
var CONST_TRANSFORM_ORIGIN = "0% 0%";

// Scene size
//var CONST_DIV_BACKGROUND_HEIGHT = 2160;
//var CONST_DIV_BACKGROUND_WIDTH = 4040;
var CONST_DIV_BACKGROUND_HEIGHT = 1080;
var CONST_DIV_BACKGROUND_WIDTH = 2020;

// Real time refresh to true for compatibility with lab-version
_Configuration.ShowRealTimeRefresh = true;

// Current selected snapshots
var m_shapshots_client_array = [];
var m_shapshots_terminal_array = [];

// Flag to update historical data in snapshots
var m_flag_is_necessary_actualize_all_historical_terminals_of_snapshots = true;

///////////////////////////////////////////////////////////////////////////////////////////

// Alarms
var m_alarms_to_print = []; //list of alarms printed on floor

///////////////////////////////////////////////////////////////////////////////////////////

// Activity
var m_activity_timeout = null;  // "Thread" id for the activity data update
var m_activity_process = false;
var m_notify_alarm = false; // revisar

var m_filter_mode_enabled = false;
var m_filter_list_selected = []; //Alarms selected on filter mode
var m_apply_filter = false;
var m_filter_selected_color;
var m_current_user_id = 0;
var m_current_user_is_runner = 0;
var m_current_user_name = ""; //DMT
var m_current_runner_in_chat_id = 0;

var m_real_time_iterations = 0;

///////////////////////////////////////////////////////////////////////////////////////////

//INFO PANEL CHARTS
var m_info_player_charts = {
};

// Properties
var m_field_properties = [];
m_field_properties["2"] = {
  field: "status", type: Enum.EN_FIELD_TYPE.NONE
};
m_field_properties["3"] = {
  field: "play_session_status", type: Enum.EN_FIELD_TYPE.NONE
};
m_field_properties["7"] = {
  field: "player_gender", type: Enum.EN_FIELD_TYPE.NONE
};
m_field_properties["8"] = {
  field: "player_age", type: Enum.EN_FIELD_TYPE.NONE
};
m_field_properties["9"] = {
  field: "player_level", type: Enum.EN_FIELD_TYPE.NONE
};
m_field_properties["10"] = {
  field: "player_is_vip", type: Enum.EN_FIELD_TYPE.NONE
};
m_field_properties["11"] = {
  field: "al_sas_host_error", type: Enum.EN_FIELD_TYPE.NONE
};
m_field_properties["12"] = {
  field: "al_door_flags", type: Enum.EN_FIELD_TYPE.NONE
};
m_field_properties["13"] = {
  field: "al_bill_flags", type: Enum.EN_FIELD_TYPE.NONE
};
m_field_properties["14"] = {
  field: "al_printer_flags", type: Enum.EN_FIELD_TYPE.NONE
};
m_field_properties["15"] = {
  field: "al_egm_flags", type: Enum.EN_FIELD_TYPE.NONE
};
m_field_properties["16"] = {
  field: "al_played_won_flags", type: Enum.EN_FIELD_TYPE.NONE
};
m_field_properties["17"] = {
  field: "al_jackpot_flags", type: Enum.EN_FIELD_TYPE.NONE
};
m_field_properties["18"] = {
  field: "al_stacker_status", type: Enum.EN_FIELD_TYPE.NONE
};
m_field_properties["19"] = {
  field: "al_call_attendant_flags", type: Enum.EN_FIELD_TYPE.NONE
};
m_field_properties["20"] = {
  field: "al_machine_flags", type: Enum.EN_FIELD_TYPE.NONE
};
m_field_properties["22"] = {
  field: "played_amount", type: Enum.EN_FIELD_TYPE.CURRENCY
};
m_field_properties["23"] = {
  field: "won_amount", type: Enum.EN_FIELD_TYPE.CURRENCY
};
m_field_properties["24"] = {
  field: "play_session_bet_average", type: Enum.EN_FIELD_TYPE.CURRENCY
};
m_field_properties["25"] = {
  field: "terminal_bet_average", type: Enum.EN_FIELD_TYPE.CURRENCY
};
m_field_properties["26"] = {
  field: "play_session_duration", type: Enum.EN_FIELD_TYPE.NONE
};
m_field_properties["27"] = {
  field: "play_session_total_played", type: Enum.EN_FIELD_TYPE.CURRENCY
};
m_field_properties["28"] = {
  field: "play_session_played_count", type: Enum.EN_FIELD_TYPE.NONE
};
m_field_properties["31"] = {
  field: "provider_name", type: Enum.EN_FIELD_TYPE.NONE
};
m_field_properties["32"] = {
  field: "play_session_won_amount", type: Enum.EN_FIELD_TYPE.CURRENCY
};
m_field_properties["33"] = {
  field: "net_win", type: Enum.EN_FIELD_TYPE.CURRENCY
};
m_field_properties["34"] = {
  field: "play_session_net_win", type: Enum.EN_FIELD_TYPE.CURRENCY
};
m_field_properties["36"] = {
  field: "played_count", type: Enum.EN_FIELD_TYPE.NONE
};
m_field_properties["37"] = {
  field: "play_occupation_status", type: Enum.EN_FIELD_TYPE.NONE
};
m_field_properties["40"] = {
  field: "denomination", type: Enum.EN_FIELD_TYPE.NONE
};
m_field_properties["41"] = {
  field: "created", type: Enum.EN_FIELD_TYPE.NONE
};
m_field_properties["42"] = {
  field: "occupancy", type: Enum.EN_FIELD_TYPE.NONE
};


///////////////////////////////////////////////////////////////////////////////////////////

var EN_LAYOUT_OBJECT_TYPES = {
  LOT_SYSTEM: 0,
  LOT_TERMINAL: 1,
  LOT_GAMING_TABLE: 2,
  LOT_GAMING_TABLE_SEAT: 3,

  LOT_3D_CAMERA: 20,

  LOT_BEACON: 70,

  LOT_SMARTPHONE: 100,

  LOT_PLAYER_MALE: 200,
  LOT_PLAYER_FEMALE: 201,

  LOT_PLAYER_RUNNER: 500
}

var EN_OBJECT_STATUS = {
  inserted: 1,
  deleted: 2
}

var EN_OBJECT_LOCATION_FIELDS = {
  lo_id: 0,
  lo_external_id: 1,
  lo_type: 2,
  lo_parent_id: 3,
  lo_mesh_id: 4,
  lo_creation_date: 5,
  lo_update_date: 6,
  lo_status: 7,
  lol_object_id: 8,
  lol_location_id: 9,
  lol_date_from: 10,
  lol_date_to: 11,
  lol_orientation: 12,
  lol_area_id: 13,
  lol_bank_id: 14,
  lol_bank_location: 15,
  lol_current_location: 16,
  lol_offset_x: 17,
  lol_offset_y: 18,
  //
  svg_id: 19
}

var EN_LOCATION_FIELDS = {
  loc_id: 0,
  loc_x: 1,
  loc_y: 2,
  loc_creation: 3,
  loc_type: 4,
  loc_floorId: 5,
  loc_gridId: 6
}

///////////////////////////////////////////////////////////////////////////////////////////

var m_reports_type_selected = -1;
var m_reports_concept_selected = 0;
var m_reports_extra_selected = 0;
var m_reports_date_selected = DateToday();
var m_reports_date_group_selected = 0;

var m_current_filter = [];

///////////////////////////////////////////////////////////////////////////////////////////

var m_alarms_selected = []; //list of alarms before click filter.

///////////////////////////////////////////////////////////////////////////////////////////

var m_accordion_props = {
  active: false,
  heightStyle: "content",
  collapsible: true,
  activate: function (evt, ui) {
    if (ui.newHeader.length > 0) {
      // RMS - Update terminals only when 2D mode (3D will do it automatically on pre-render
      if (_LayoutMode == 2) {
        // Update terminals when new filter is activated
        UpdateTerminals();
      }
    }
  }
};

///////////////////////////////////////////////////////////////////////////////////////////

var m_loaded_dashboard = '';

///////////////////////////////////////////////////////////////////////////////////////////

var m_floor_changed = false;
var m_changing_floor = false;

///////////////////////////////////////////////////////////////////////////////////////////

var m_dashboard_drag_mode_on = false;
var m_dashboard_click = false;

// Function wrapping code.
// fn - reference to function.
// context - what you want "this" to be.
// params - array of parameters to pass to function.
var m_function_wrapper = function (fn, context, params) {
  return function () {
    fn.apply(context, params);
  };
}

var m_function_queue = [];

function CheckMessage(Result) {
  if (Result) {
    if (Result.Message) {
      switch (Result.Message) {
        case 900: // Session expired
          location.href = './Login.aspx';
          break;

        case 401: // Unauthorized
          location.href = './ErrorPage.aspx';
          break;
      }
    }
  }
}

///////////////////////////////////////////////////////////////////////////////////////////

// Rounds a decimal number with the indicated number of Decimals
function Round(Value, Decimals) {
  var _rounder = 10;
  if (Decimals == undefined) {
    Decimals = 2;
  }
  _rounder = Math.pow(_rounder, Decimals);
  if (Value) {
    //Value = Value.toString();
    //return Math.round(Value.replace(",", ".") * (10 * Decimals)) / (10 * Decimals);
    return Math.round(Value * _rounder) / _rounder;
  }
  return 0;
}

///////////////////////////////////////////////////////////////////////////////////////////

// Returns the current date
function GetTimeNow() {
  return new Date().getTime();
} // GetLastTimeUpdated

///////////////////////////////////////////////////////////////////////////////////////////

var m_last_time_updated = 0;  //Number of milliseconds since 1970/01/01

// Returns the last time confiuration was updated
function GetLastTimeUpdated() {
  return m_last_time_updated;
} // GetLastTimeUpdated

// Updates the value of last time update
function UpdateLastTimeUpdated() {
  m_last_time_updated = GetTimeNow(); //Number of milliseconds since 1970/01/01
} // SetLastTimeUpdated

///////////////////////////////////////////////////////////////////////////////////////////

function SnapshotClient(Account_id, ObjectId) {
  this.account_id = Account_id;
  this.object_id = ObjectId; // indicates the actual terminal that client is playing
  this.all_historical_terminals_actualized = false;
}

function SnapshotTerminal(TerminalId, ObjectId, Account_id) {
  this.terminal_id = TerminalId;
  this.object_id = ObjectId;
}
///////////////////////////////////////////////////////////////////////////////////////////

function SetAlarms(Sender, firstTime) {
  if (!firstTime) {
    //    m_alarms_enabled = !m_alarms_enabled;
    _Configuration.ShowActivity = !_Configuration.ShowActivity;
  }
  _AlarmList.enabled = _Configuration.ShowActivity; // m_alarms_enabled;
  // RMS - Update terminals only when 2D mode (3D will do it automatically on pre-render
  if (_LayoutMode == 2) {
    UpdateTerminals();
  }
  m_config_has_changed = !firstTime;
  if (Sender != null) {
    ToggleCheckState(Sender, _Configuration.ShowActivity);
  }
}

///////////////////////////////////////////////////////////////////////////////////////////

// Flag to control more than one activation of activity
var m_activator_control = false;

// Fills WSIData.Terminal_Status with a random values
function activator() {

  // Check flag
  if (!m_activator_control) {

    // Activate flag
    m_activator_control = true;

    if (_Configuration.TestEnabled) {
      // Get test data
      PageMethods.GetRealTimeInformationTest(OnRealTimeComplete, OnRealTimeFailed);
    }
    else {
      // Get production data
      PageMethods.GetRealTimeInformation(OnRealTimeComplete, OnRealTimeFailed);
    }

    // Runners
    //if (WSIData.ShowRunners) {
    PageMethods.GetRunnersInformation(OnRunnersInformationComplete, OnRunnersInformationFailed);

    // Update custom updatable reports
    UpdateManualReports();

  } else {
    WSILogger.Write('RealTime already active...');
  }
}

// Callbacks for activator - Ok
function OnRealTimeComplete(result, userContext, methodName) {

  var _date = DateTimeToday();

  if (WSIData.Activity == true) {

    m_activity_process = true;

    // Catch results
    if (result != "" && result != null) {

      if (!_Manager.TaskManager.locked) {
        _Manager.TaskManager.Update();
      }

      // Convert data in object
      var __result = JSON.parse(result);
      var _result = __result.Terminals;
      var _resultRanges = __result.Ranges;

      CheckMessage(__result);

      // Clean current data
      delete _Manager.Terminals.ByAccount;
      delete _Manager.Terminals.ByProvider;
      delete _Manager.Terminals.ByArea;
      _Manager.Terminals.ByAccount = [];
      _Manager.Terminals.ByProvider = [];
      _Manager.Terminals.ByArea = [];
      _Manager.WGDBDate = __result.WGDBDate;

      _Manager.Ranges = _resultRanges;
      _Manager.Ranges.BuscaNivel = function (id) {
        for (var i = 0; i < _Manager.Ranges.length;i++) {
          if (parseInt(_Manager.Ranges[i].Range[0]) == id) {
            return _Manager.Ranges[i].Range[1];
          }
        }
        return "Unknown";
      }
      // Occupancy of the site (Aforo)
      _Manager.Occupancy = __result.Occupancy;

      // Reset Meters data for "NOW"
      _Manager.MetersManager.ResetMetersNow();

      // Update data
      _Manager.Terminals.Updating = true;

      for (var _i = 0; _i < _result.length; _i++) {

        //// RMS - TODO - Adapt GetActivity to obtain activity for a concrete floorid
        // Get terminal id
        var _tid = _result[_i].Terminal[0];

        // Is Terminal in current floor?
        if (!_Manager.Terminals.Items[_tid]) {

          // RMS : Terminal not in floor, update site data for widgets
          _Manager.OtherTerminals.Items[_tid] = new WSITerminal();
          _Manager.OtherTerminals.Items[_tid].Properties.SetActivityData(_result[_i].Terminal);

          // Update indexes
          _Manager.OtherTerminals.UpdateIndexes(_tid);

          // Update meters range "NOW"
          _Manager.MetersManager.UpdateMetersNow(_Manager.OtherTerminals.Items[_tid]);

          // Update occupation data
          _Manager.OtherTerminals.UpdateOccupationData(_date, _Manager.OtherTerminals.Items[_tid]);

          // Update monetary data
          _Manager.OtherTerminals.UpdateMonetaryData(_date, _Manager.OtherTerminals.Items[_tid]);

          continue;
        }

        // Update/Set activity data on terminal
        _Manager.Terminals.Items[_tid].Properties.SetActivityData(_result[_i].Terminal);

        // Update indexes
        _Manager.Terminals.UpdateIndexes(_tid);

        // Update meters range "NOW"
        _Manager.MetersManager.UpdateMetersNow(_Manager.Terminals.Items[_tid]);

        // Update occupation data
        _Manager.Terminals.UpdateOccupationData(_date, _Manager.Terminals.Items[_tid]);

        // Update monetary data
        _Manager.Terminals.UpdateMonetaryData(_date, _Manager.Terminals.Items[_tid]);
      }

      if (__result.Capacity != "") {
        _Manager.MetersManager.AddMetersNowData(__result.Capacity.rows);
      }

      _Manager.MetersManager.UpdateSiteMetersNow();


      // Control update in progress
      _Manager.Terminals.Updating = false;

      // Fire onTerminalsUpdate Event
      if (_Manager.Terminals.OnTerminalsUpdated) {
        _Manager.Terminals.OnTerminalsUpdated();
      }


    } else {

      // No data received
      WSILogger.Debug("OnRealTimeFailed()", "NO DATA RECEIVED", result);

    }

    // Update application
    var _activity_refresh_conf_seconds;
    // Current forced to update each 5 seconds
    _activity_refresh_conf_seconds = 5;



    // Check time
    if ((GetTimeNow() - GetLastTimeUpdated() + 1000) >= (_activity_refresh_conf_seconds * 1000)) {

      // Save current configuration
      SaveConfigurationDB();

    }

    // Ensure that the loading div is hide / It must be visible until first update is complete
    if ($("#loading").css('display') != "none") {
      if (_Legends.Items.length > 0 && _Legends.Items[30]) {
        UpdateMachineProviders();

        UpdateMachineDenominations();

        // Update DashBoard
        UpdateReportDashboard(true);

      }

      $("#loading").hide();
      WSILogger.Write('Hide Loading / Enable interactions');

      //$("#loading").css('display', "none");
      //document.getElementById('loading').style.display = "none";

      setTimeout(function () {
        _Manager.WidgetManager.SetDashboard(m_loaded_dashboard);
        _Manager.WidgetManager.UpdateWidgetsList();
        _Manager.WidgetManager.UpdateWidgets();
      }, 1);


    } else {

      if (WSIData.AutoUpdateDashboard) {
        // Update DashBoard
        UpdateReportDashboard(false);

      }

    }

    // Special actions by Mode
    switch (_LayoutMode) {
      case 1: // 3D

        if (m_floor_loaded) {
          UpdateTerminals();
        }
        break;
      case 2: // 2D
        // Refresh Labels
        _Manager.Labels.Refresh();
        // RMS - Update terminals only when 2D mode (3D will do it automatically on pre-render
        // Update terminal objects
        UpdateTerminals();
        break;
    }

    if ($(".wrapJugadores.customerMode").length > 0 && $(".wrapJugadores.customerMode").css('display') != "none") {
      WSICustomersAtRoom_Refresh();
      WSILogger.Write('WSICustomersAtRoom_Refresh();');
    }

    if ($(".wrapJugadores.terminalMode").length > 0 && $(".wrapJugadores.terminalMode").css('display') != "none") {
      WSITerminalsAtRoom_Refresh();
      WSILogger.Write('WSITerminalsAtRoom_Refresh();');
    }


    if ($(".wrapJugadores.terminalAlarmsMode").length > 0 && $(".wrapJugadores.terminalAlarmsMode").css('display') != "none") {
      WSIAlarmsAtRoom_Refresh();
      WSILogger.Write('WSIAlarmsAtRoom_Refresh();');
    }

    m_activity_process = false;

    // Update Widgets
    if (!_Manager.WidgetManagerFirstUpdated) {
      _Manager.WidgetManager.UpdateWidgetsList();
      _Manager.WidgetManager.UpdateWidgets();
      _Manager.WidgetManagerFirstUpdated = true;
      m_real_time_iterations = 0;
    }
    else if (_Manager.WidgetManagerFirstUpdated && !m_dashboard_click && m_current_section == 10 && m_real_time_iterations > 2) {
      _Manager.WidgetManager.UpdateWidgetsList();
      _Manager.WidgetManager.UpdateWidgets();
      _Manager.WidgetManagerFirstUpdated = true;
      m_real_time_iterations = 0;

    }

    // Relaunch timeout process
    m_activator_control = false;
    SetActivityOn();

    //m_activator_control = false;

    WSILogger.Write('Finished: OnRealTimeComplete');

    // Update things when floor changed 
    if (m_floor_changed) {
      if (m_current_section == 10) {
        _UI.SetMenuActiveSection($("#btnDashboard"), 10);
      }
      m_floor_changed = false;
    }
  }
  m_real_time_iterations = m_real_time_iterations + 1;
}

// Callbacks for activator - Error
function OnRealTimeFailed(error, userContext, methodName) {
  WSILogger.Debug("OnRealTimeFailed()", "error", error);

  // Redirect to page error
  GotoError('OnRealTimeFailed');
}

///////////////////////////////////////////////////////////////////////////////////////////

function OnRunnersInformationComplete(result, userContext, methodName) {

  try {
    if (result) {
      var _runners_data = JSON.parse(result);

      _Manager.Runners.Update(_runners_data);


      if (!_Manager.RunnersMap.Created && m_floor_loaded) {
        _Manager.RunnersMap.SetVisibility(false);

        //Si el RunnersMap está activo lo pinto
        _Manager.RunnersMap.Create(101 * 4, 54 * 4);
        _Manager.RunnersMap.SetVisibility(true);
      }

      if (m_floor_loaded && _Configuration.ShowRunnersMap) {
        _Manager.Runners.Build();
        _Manager.RunnersMap.SetVisibility(_Configuration.ShowRunnersMap);

        if (!$(document).find('[onclick*="SetRunnersmap();"]').length)
          _Manager.RunnersMap.SetVisibility(false);
      } else {
        _Manager.RunnersMap.SetVisibility(false);
      }

    }
  } catch (e) {
    WSILogger.Debug("OnRunnersInformationComplete()", "error", e);
  }
}

function OnRunnersInformationFailed(error, userContext, methodName) {
  WSILogger.Debug("OnRunnersInformationFailed()", "error", error);

  // Redirect to page error
  GotoError('OnRunnersInformationFailed');
}

///////////////////////////////////////////////////////////////////////////////////////////
//Update Providers from machine legends
function UpdateMachineProviders() {
  var _content = "";
  //var _idx_max_providers = 8;
  //var _idx_count = 0;

  for (_idx_provider in _Manager.Terminals.ByProvider) {
    //if (_idx_count < _idx_max_providers) {
    var _legend_color = "rgb(" + Math.floor((Math.random() * 255) + 1) + ", " + Math.floor((Math.random() * 255) + 1) + ", " + Math.floor((Math.random() * 255) + 1) + ")";
    var _legend_name = _idx_provider;
    var _legend_minValue = _idx_provider;
    var _legend_maxValue = "";
    var _value = 0;
    var _legend_operator = "=";

    var _legend_sub_item = new WSILegendItem(_legend_color, _legend_name, _legend_minValue, _legend_maxValue, _value, _legend_operator);

    _Legends.Items[30].Items.push(_legend_sub_item);
    //}
    //_idx_count++;
  }
  _content += '<ul><div>';

  var _providers_items_sorted = _Legends.Items[30].Items.sort(function (a, b) {
    if (a.name < b.name) return -1;
    if (a.name > b.name) return 1;
    return 0;
  });


  for (_idx in _providers_items_sorted) {

    //        _content += '<li><div class="circuloValor" style="background: ' + _Legends.Items[30].Items[_idx].color + ';"></div><span>' + _Legends.Items[30].Items[_idx].name + '</span></li>';

    _content += "<li class='childLegend' id='childlegend_7030_" + _idx + "' onclick='Child_Legend_Click(70" + ",\"7030_" + _idx + "\");' data-rangeid='" + _idx + "' data-legendid='30'>";
    _content += '<div class="circuloValor" data-sectionid="70" id="leyenda_item_70_30" style="background: ' + _Legends.Items[30].Items[_idx].color + ';">';
    _content += '</div><span>' + _Legends.Items[30].Items[_idx].name + '</span></li>';

  }



  _content += '</ul></div>';

  $("[data-section='70'][data-id='30']").next().html(_content);
}

///////////////////////////////////////////////////////////////////////////////////////////
//Update Providers from machine legends
function UpdateMachineDenominations() {
  var _content = "";
  //var _idx_max_providers = 8;
  var _idx_count = 0;

  for (_idx_denomination in _Manager.Terminals.Items) {
    var _legend_color = "rgb(" + Math.floor((Math.random() * 255) + 1) + ", " + Math.floor((Math.random() * 255) + 1) + ", " + Math.floor((Math.random() * 255) + 1) + ")";
    var _legend_name = _Manager.Terminals.Items[_idx_denomination].Properties["denomination"];
    var _legend_minValue = _legend_name;
    var _legend_maxValue = "";
    var _value = 0;
    var _legend_operator = "=";

    if (_legend_name != null) {
      var _legend_sub_item = new WSILegendItem(_legend_color, _legend_name, _legend_minValue, _legend_maxValue, _value, _legend_operator);

      _legend_sub_item.Editable = false;

      var _is_new_item = true;
      for (_idx_n in _Legends.Items[31].Items) {
        if (_Legends.Items[31].Items[_idx_n].name == _legend_name) {
          _is_new_item = false;
        }
      }

      if (_is_new_item == true) {
        _Legends.Items[31].Items.push(_legend_sub_item);
      }

      _idx_count++;
    }
  }

  if (_Legends.Items[31].Items.length > 0) {
    _content += '<ul><div>';

    var _denominations_items_sorted = _Legends.Items[31].Items.sort(function (a, b) {
      if (a.name < b.name) return -1;
      if (a.name > b.name) return 1;
      return 0;
    });

    for (_idx in _denominations_items_sorted) {

      _content += "<li class='childLegend' id='childlegend_7031_" + _idx + "' onclick='Child_Legend_Click(70" + ",\"7031_" + _idx + "\");' data-rangeid='" + _idx + "' data-legendid='31'>";
      _content += '<div class="circuloValor" data-sectionid="70" id="leyenda_item_70_31" style="background: ' + _Legends.Items[31].Items[_idx].color + ';">';
      _content += '</div><span>' + _Legends.Items[31].Items[_idx].name + '</span></li>';

    }

    _content += '</ul></div>';

    $("[data-section='70'][data-id='31']").next().html(_content);
  } else {
    $("[data-section='70'][data-id='31']").css("display", "none");
  }
}

// Part of initial process : Obtains layout objects
function LoadLayout() {
  // $("#sp_loading_step").text('Obtaining current layout...');

  WSILogger.Write("Loading layout...");

  // Prepare notifications listener system
  m_current_user_id = $("#txt_current_role").data("userid");
  m_current_user_name = $("#txt_user_nick").text();
  //MQTT Notifications
  if (this._MQTTclient === undefined) {
    this._MQTTclient = new WSIMQTTclient(m_current_user_id);
    this._MQTTclient.init();
  }

  // Call the page method
  PageMethods.GetLayout(_Configuration.CurrentFloorId, OnLoadLayoutSucceed, OnLoadLayoutError);
}

// Callback for LoadLayout - Ok
function OnLoadLayoutSucceed(result, userContext, methodName) {

  try {

    WSILogger.Debug("OnLoadLayoutSucceed()", "DATA_SIZE", result.length);

    // Convert result into object
    var _items = JSON.parse(result),
        _l = _items.Objects.rows.length,
        _row,
        _floorId = _Configuration.CurrentFloorId,
      $floorData = $("#floorData_" + _floorId),
        _fdata = {},
            _loc;

    _fdata.id = $floorData.val();
    _fdata.name = $floorData.attr("data-name");
    _fdata.image_map = _items.FloorMap.rows[0][0];
    _fdata.color = $floorData.attr("data-color");
    _fdata.height = $floorData.attr("data-height");
    _fdata.width = $floorData.attr("data-width");
    _fdata.metric = $floorData.attr("data-metric");

    var _width = UnitsToPixels(_fdata.width);
    var _height = UnitsToPixels(_fdata.height);
    $("#div_background").css("width", _width);
    $("#div_background").css("height", _height);

    $("#img_background").attr('src', _fdata.image_map);
    _CurrentFloor3 = new WSIFloor3();
    _CurrentFloor3.Load(_fdata, _items);

    // WSILogger.Debug("LoadLayout.OnLoadLayoutSucceed()", "_items", _items);
    for (var _i = 0; _i < _l; _i++) {
      _row = _items.Objects.rows[_i];
      _loc = _CurrentFloor3.GetLocationById(_row[EN_OBJECT_LOCATION_FIELDS.lol_location_id]);

      switch (+_row[EN_OBJECT_LOCATION_FIELDS.lo_type]) {
        case EN_LAYOUT_OBJECT_TYPES.LOT_TERMINAL:
          _Manager.Terminals.Items[_row[EN_OBJECT_LOCATION_FIELDS.lo_id]] = new WSITerminal();
          _Manager.Terminals.Items[_row[EN_OBJECT_LOCATION_FIELDS.lo_id]].Properties.Initialize2(_row, _loc, _CurrentFloor3.dimensions);

          break;
        case EN_LAYOUT_OBJECT_TYPES.LOT_BEACON:
          _Manager.Beacons.AddItem(_row, _loc, _CurrentFloor3.dimensions);
          break;
        default:
          // Unknown object type
          // WSILogger.Write("OnLoadLayoutSucceed() -> Unrecognized object type [" +_items[_i].userData.objectLayout[2]+ "] - Data: " +_items[_i]);
          break;
      }
    }

  } catch (_ex) {
    // Log Error and redirect
    WSILogger.Log("OnLoadLayoutSucceed()", _ex);
    GotoError('OnLoadLayoutSucceed');
  }

  // Update floor name
  $("#txt_current_floor").text(_fdata.name);

  if (_LayoutMode == 2) { // 2D
    SetDivBackgroundToInitialPosition();
  }

  // Initialize Scene
  _UI.InitControls();

  if (m_floor_changed) {

    InitializeScene();
  }
}

// Callback for LoadLayout - Error
function OnLoadLayoutError(error, userContext, methodName) {
  // Redirect
  GotoError('OnLoadLayoutError');
}

//////////////////////////////////////////////////////////////////////////////

// Part of initial process : Load the filters of all sections
function LoadLegends() {
  //  $("#sp_loading_step").text('Preparing filters...');

  // Call the page method
  PageMethods.GetLayoutRanges(OnLoadLegendsComplete, OnLoadLegendsFailed);
}

function LoadTaskManagerData() {
  //$("#sp_loading_step").text('Preparing filters...');

  // Call the page method
  //PageMethods.GetLayoutRanges(OnLoadLegendsComplete, OnLoadLegendsFailed);
  _Manager.TaskManager.Init("null");
}

// Callback for LoadLegends - Ok
function OnLoadLegendsComplete(result, userContext, methodName) {
  try {
    //SetActivityOff();
    if (result) {
      // Correct data check
      var Data = eval("(" + result + ")");

      if (Data) {
        // Initialize 
        _Legends.Init(Data);
      }
    }

    // Update meters data and update dashboard
    GetCurrentMeters();

    //_Manager.TaskManager.Update();

    //Prepare Alarms Section
    PrepareAlarmsSection();

    // Set Legends
    _Manager.Legends = _Legends;

  } catch (_ex) {
    // Report and redirect on error
    WSILogger.Log("OnLoadLegendsComplete()", _ex);
    GotoError('OnLoadLegendsComplete');
  }
}

// Callback for LoadLegends - Error
function OnLoadLegendsFailed(error, userContext, methodName) {
  // Redirect
  GotoError('OnLoadLegendsFailed');
}

//////////////////////////////////////////////////////////////////////////////

function PrepareAlarmsSection() {
  var _container_alarm = $("#sub_menu_40 [data-notificationalarm=42002]");
  var _result = "";
  var _legend_id = _Legends.Items.length;

  if (_container_alarm.length > 0) {
    $("#sub_menu_40 [data-notificationalarm=41001]").parent().remove();
  }


  var _legend_alarm_priorities = new WSILegend();

  _legend_alarm_priorities.Name = _Language.get('NLS_ALARMS_ALL'); // Name
  _legend_alarm_priorities.NLS = "NLS_ALARMS_ALL";
  _legend_alarm_priorities.Property = 99; // Field
  _legend_alarm_priorities.SectionID = 40;
  _legend_alarm_priorities.Icon = undefined;

  _Legends.Add(_legend_id, _legend_alarm_priorities);

  var _sub_item_high = new WSILegendItem("#FF0000", _Language.get("NLS_ALARMS_PRIORITY_HIGH"), 1, 1, 3, "=", null, null, 0);
  var _sub_item_medium = new WSILegendItem("#ff9900", _Language.get("NLS_ALARMS_PRIORITY_MEDIUM"), 1, 1, 2, "=", null, null, 0);
  var _sub_item_low = new WSILegendItem("#009933", _Language.get("NLS_ALARMS_PRIORITY_LOW"), 1, 1, 1, "=", null, null, 0);

  _sub_item_high.NLS = "NLS_ALARMS_PRIORITY_HIGH";
  _sub_item_medium.NLS = "NLS_ALARMS_PRIORITY_MEDIUM";
  _sub_item_low.NLS = "NLS_ALARMS_PRIORITY_LOW";

  //_Legends.Items[_legend_id].Items[0] = _sub_item_high;
  _Legends.Items[_legend_id].Items[3] = _sub_item_low;
  _Legends.Items[_legend_id].Items[2] = _sub_item_medium;
  _Legends.Items[_legend_id].Items[1] = _sub_item_high;

  _result += "<div id='div_legend_container_40000'>";
  _result += "<h3 data-nls='NLS_ALARMS_ALL' onclick='SetCurrentLegend(" + _legend_id + ",40);' data-id='" + _legend_id + "' data-section='40' '>" + _Language.get('NLS_ALARMS_ALL') + "<div class='chkAlarmsSound chkAlarmsSoundDisabled' /></h3>";
  _result += "<div><ul>";
  _result += "<li class='childLegend'>";
  _result += "<div  class='circuloValor' style='background: rgb(255,0,0);'>";
  _result += "</div>";
  _result += "<span data-nls='NLS_ALARMS_PRIORITY_HIGH'>" + _Language.get('NLS_ALARMS_PRIORITY_HIGH') + "</span>";
  _result += "</li>";
  _result += "<li class='childLegend'>";
  _result += "<div  class='circuloValor' style='background: rgb(255, 102,0);'>";
  _result += "</div>";
  _result += "<span data-nls='NLS_ALARMS_PRIORITY_MEDIUM'>" + _Language.get('NLS_ALARMS_PRIORITY_MEDIUM') + "</span>";
  _result += "</li>";
  _result += "<li class='childLegend'>";
  _result += "<div  class='circuloValor' style='background: rgb(0,153, 51);'>";
  _result += "</div>";
  _result += "<span data-nls='NLS_ALARMS_PRIORITY_LOW'>" + _Language.get('NLS_ALARMS_PRIORITY_LOW') + "</span>";
  _result += "</li>";
  _result += "</ul></div>";
  _result += "</div>";

  $("#sub_menu_40 .comparaControl").after(_result);
  $("#div_legend_container_40000").accordion(m_accordion_props);
  $("#div_legend_container_40000").addClass("accordion")

  //m_alarms_filter_sounds_changed = true;

  $(".chkAlarmsSound").click(function (e) {
    e.stopPropagation();

    if ($($(this)[0]).hasClass('chkAlarmsSoundEnabled')) {
      $($(this)[0]).removeClass('chkAlarmsSoundEnabled');
      $($(this)[0]).addClass('chkAlarmsSoundDisabled');
    } else {
      $($(this)[0]).removeClass('chkAlarmsSoundDisabled');
      $($(this)[0]).addClass('chkAlarmsSoundEnabled');
    }

    if ($(e.target).parent().parent()[0].id == "div_legend_container_40000") {
      if ($($("#div_legend_container_40000 .chkAlarmsSound")[0]).hasClass('chkAlarmsSoundEnabled')) {
        $(".chkAlarmsSound").each(function (i, val) {
          $(val).removeClass('chkAlarmsSoundDisabled');
          $(val).addClass('chkAlarmsSoundEnabled');
          //val.checked = true;
        });
      } else {
        $(".chkAlarmsSound").each(function (i, val) {
          $(val).removeClass('chkAlarmsSoundEnabled');
          $(val).addClass('chkAlarmsSoundDisabled');
          //val.checked = false;
        });
      }
    }

    if (!$($(this)[0]).hasClass('chkAlarmsSoundEnabled')) {
      $($("#div_legend_container_40000 .chkAlarmsSound")[0]).addClass('chkAlarmsSoundDisabled');
      $($("#div_legend_container_40000 .chkAlarmsSound")[0]).removeClass('chkAlarmsSoundEnabled');
    }

    if ($("#div_legend_container_42002 .chkAlarmsSoundDisabled").length == 0 && $("#div_legend_container_41001 .chkAlarmsSoundDisabled").length == 0) {
      $($("#div_legend_container_40000 .chkAlarmsSound")[0]).addClass('chkAlarmsSoundEnabled');
      $($("#div_legend_container_40000 .chkAlarmsSound")[0]).removeClass('chkAlarmsSoundDisabled');
    }

    m_alarms_filter_sounds_changed = true;
  });

  //Check only alarms from config
  for (_idx_al_to_sound in _Configuration.AlarmsToSound) {
    var _alarm_id = _Configuration.AlarmsToSound[_idx_al_to_sound];
    //$(".chkAlarmsSound[data-soundlegendid='" + _alarm_id + "']")[0].checked = true;
    $($(".chkAlarmsSound[data-soundlegendid='" + _alarm_id + "']")[0]).addClass('chkAlarmsSoundEnabled');
    $($(".chkAlarmsSound[data-soundlegendid='" + _alarm_id + "']")[0]).removeClass('chkAlarmsSoundDisabled');
  }

  if ($("#div_legend_container_42002 .chkAlarmsSoundDisabled").length == 0 && $("#div_legend_container_41001 .chkAlarmsSoundDisabled").length == 0) {
    $($("#div_legend_container_40000 .chkAlarmsSound")[0]).addClass('chkAlarmsSoundEnabled');
    $($("#div_legend_container_40000 .chkAlarmsSound")[0]).removeClass('chkAlarmsSoundDisabled');
  }

  m_alarms_filter_sounds_changed = true;
}

function LoadLayoutFloors() {
  //  $("#sp_loading_step").text('Loading floors list...');
  WSILogger.Write("Loading Floors...");

  // Call the page method
  PageMethods.GetLayoutFloors(onLoadLayoutFloorsComplete, onLoadLayoutFloorsFailed);
}

function onLoadLayoutFloorsComplete(result, userContext, methodName) {

  var _item;

  try {

    if (result) {

      WSILogger.Debug("onLoadLayoutFloorsComplete()", "DATA_SIZE", result.length);

      result = JSON.parse(result);

      $("#planoPisoSelector").empty();

      for (var _row in result.rows) {
        _item = '';
        _row = result.rows[_row];

        WSILogger.Write("Loading Floor: " + _row[1]);

        if (_row) {
          _item = "<option id='floorData_" + _row[0] + "' value='" + _row[0] + "' data-name='" + _row[1] + "' data-color='" + _row[5] + "' data-image-map='" + _row[4] + "' data-height='" + _row[2] + "' data-geometry=''" + " data-width='" + _row[6] + "'" + " data-metric='" + _row[7] + "'" + ">";
          _item = _item + " " + _row[1] + " ";
          _item = _item + "</option>";

          $("#planoPisoSelector").append(_item);
        }
      }

    }

    $("#planoPisoSelector").val(_Configuration.CurrentFloorId);

    // Launch next step - LoadLayout
    LoadLayout();

  } catch (_ex) {
    // Report and redirect on error
    WSILogger.Log("onLoadLayoutFloorsComplete()", _ex);
    GotoError('onLoadLayoutFloorsComplete');
  }

}

function onLoadLayoutFloorsFailed(error, userContext, methodName) {
  GotoError('onLoadLaoutFloorsFailed');
}

//////////////////////////////////////////////////////////////////////////////

// Part of initial process : Load user configuration
function LoadConfig() {
  // $("#sp_loading_step").text('Loading configuration...');
  WSILogger.Write("Loading configuration...");

  // Launch the page method
  PageMethods.GetLayoutConfigurationFromSession(GetLayoutConfigurationCompleted, GetLayoutConfigurationFailed);
}

// Callback for LoadConfig - Ok
function GetLayoutConfigurationCompleted(result, userContext, methodName) {
  var _result;

  try {

    if (result != "") {

      WSILogger.Debug("GetLayoutConfigurationCompleted()", "DATA_SIZE", result.length);

      _result = JSON.parse(result);

      if (_result.Message) {
        if (_result.Message == "950") {
          GotoLogin();
        }
      }

      if (_result.common) {
        m_format_language = _result.common.language;
        m_format_currency = _result.common.currency;
        m_format_currency_symbol = _result.common.symbol;

      } else {
        WSILogger.Write("Common configuration unavailable!");
      }

      $.extend(true, _Configuration, _result.user);

      // Keep dashboard
      m_loaded_dashboard = _result.dashboard;

      // RMS - Language patch
      if (!$.isNumeric(_Configuration.Language)) {
        _Configuration.Language = m_language_binds[_Configuration.Language];
      }
      _Language.CurrentLanguage = _Configuration.Language;

      _Configuration.AlarmsToSound = [];

      for (_idx_alarms_to_sound_conf in _result.user.AlarmsToSound) {
        var _val_ats = _result.user.AlarmsToSound[_idx_alarms_to_sound_conf];
        if (_val_ats != null) {
          _Configuration.AlarmsToSound[_val_ats] = _val_ats;
        }
      }

      _Manager.GeneralParams = _result.generalparams;
    } else {
      WSILogger.Debug("GetLayoutConfigurationCompleted()", "USER WITHOUT CONFIGURATION", null);
    }

    //// Create tables
    //CreateTableCustomersAtRoom();
    //CreateTableAlarmsAtRoom();


    // Tasks Manager
    _Manager.TaskManager = new TaskManager();

    // Launch next step - LoadLegends
    LoadLegends();

    //LoadAlarms & Tasks
    LoadTaskManagerData();


  } catch (_ex) {
    WSILogger.Log("GetLayoutConfigurationCompleted()", _ex);
  }
}

// Callback for LoadConfig - Error
function GetLayoutConfigurationFailed(error, userContext, methodName) {
  // Report and Redirect
  WSILogger.Debug("GetLayoutConfigurationFailed()", "error", error);
  GotoError('LoadConfig');
}

//////////////////////////////////////////////////////////////////////////////

function GotoError(Sender) {

  // TODO: Uncomment when tests are done

  //  var _loc = '/ErrorPage.aspx';
  //  if (Sender) {
  //    _loc = _loc + '?' + Sender;
  //  }
  //  window.location(_loc);

}

//////////////////////////////////////////////////////////////////////////////

// Real-time historic data update On/Off
function SetHistoricalInRealTime(firstTime) {
  if (!firstTime) {
    _Configuration.ShowRealTimeRefresh = !_Configuration.ShowRealTimeRefresh;
  }
  SetHistoricalInRealTimeChecks(_Configuration.ShowRealTimeRefresh);
  m_config_has_changed = !firstTime;

} //SetHistoricalInRealTime

// Update checks state for historical real-time update
function SetHistoricalInRealTimeChecks(Value) { //Value=true; Value=false

  $("img[id*=chkRealTime]").each(function (idx, el) {
    ToggleCheckState($(el), Value);
  });

  m_flag_is_necessary_actualize_all_historical_terminals_of_snapshots = true;

}

//////////////////////////////////////////////////////////////////////////////

function ShowBills(BillTrId) {
  var _bill_tr;
  var _bill_tr_display;

  _bill_tr = $("#" + BillTrId);
  if ($(_bill_tr).length > 0) {
    _bill_tr_display = $(_bill_tr).css("display");

    if (_bill_tr_display == "none") {
      $(_bill_tr).css("display", "table-row")
    }
    else {
      $(_bill_tr).css("display", "none")
    }
  }
}  //ShowBills

//////////////////////////////////////////////////////////////////////////////

function CreateTableHistoricalTerminals_TableBill(TerminalId, AccountId, ResultJsonBills, Origin) { //Origin="snapshot" ; "popup"
  var _table_id;
  var _table_html;
  var _id;

  _table_html = "";
  switch (Origin) {
    case "popup":
      _id = "popup";
      break;
    case "snapshot":
      _id = "snapshot_" + TerminalId;
      break;
    default:
      _id = "";
      break;
  }

  _table_id = "table_bills_" + _id + "_" + AccountId;
  _table_html += "<table id='" + _table_id + "' class='table_report ui-state-default' style='width:100%;margin-left:0px;'>";
  _table_html += "<tr>";
  _table_html += '<td><b>' + _Language.get("NLS_DATE") + '</b></td>';
  _table_html += '<td><b>' + _Language.get("NLS_AMOUNT") + '</b></td>';
  _table_html += '<td><b>' + _Language.get("NLS_STACKER") + '</b></td>';
  _table_html += "</tr>";

  if (ResultJsonBills) {

    for (var _i_bill = 0; _i_bill < ResultJsonBills.length; _i_bill++) {
      if (ResultJsonBills[_i_bill].BillIn[0] == AccountId && TerminalId == ResultJsonBills[_i_bill].BillIn[1]) {
        _table_html += "<td>" + ResultJsonBills[_i_bill].BillIn[2] + "</td><td>" + ResultJsonBills[_i_bill].BillIn[3] + "</td><td>" + ResultJsonBills[_i_bill].BillIn[4] + "</td></tr>";
      }
    }
  }
  _table_html += "</table>";

  return _table_html;

}  //CreateTableHistoricalTerminals_TableBill

//////////////////////////////////////////////////////////////////////////////

function CreateNewHistoricalTerminalRow(TerminalId, AccountId, ResultJsonTerminal, ResultJsonBills, IdxTerminal, Origin) { //Origin="popup"; "snapshot"
  var _tr_info_id;
  var _table_html;
  var _td_bill_id;
  var _tr_bill_id;
  var _id;

  _table_html = "";
  _id = "";

  switch (Origin) {
    case "popup":
      _id = "popup";
      break;
    case "snapshot":
      _id = "snapshot_" + TerminalId;
      break;
    default: break;
  }
  //tr 1: principal
  _tr_info_id = "tr_" + _id + "_" + AccountId + "_info";
  _table_html += "<tr id='" + _tr_info_id + "'>"
  _td = "td_" + _id + "_" + AccountId + "_terminal";
  _table_html += "<td id='" + _td + "'>" + ResultJsonTerminal[IdxTerminal].Terminal[2] + "</td>";
  _td = "td_" + _id + "_" + AccountId + "_provider";
  _table_html += "<td id='" + _td + "'>" + ResultJsonTerminal[IdxTerminal].Terminal[3] + "</td>";
  _td = "td_" + _id + "_" + AccountId + "_timeplayed";
  _table_html += "<td id='" + _td + "'>" + ResultJsonTerminal[IdxTerminal].Terminal[4] + "</td>";
  _td = "td_" + _id + "_" + AccountId + "_totalplayed";
  _table_html += "<td id='" + _td + "'>" + ResultJsonTerminal[IdxTerminal].Terminal[5] + "</td>";
  _td = "td_" + _id + "_" + AccountId + "_playedwon";
  _table_html += "<td id='" + _td + "'>" + ResultJsonTerminal[IdxTerminal].Terminal[6] + "</td>";
  _td = "td_" + _id + "_" + AccountId + "_playedcount";
  _table_html += "<td id='" + _td + "'>" + ResultJsonTerminal[IdxTerminal].Terminal[7] + "</td>";
  _td = "td_" + _id + "_" + AccountId + "_betaverage";
  _table_html += "<td id='" + _td + "'>" + ResultJsonTerminal[IdxTerminal].Terminal[8] + "</td>";

  _tr_bill_id = "tr_" + _id + "_" + AccountId + "_bill";
  //_table_html += "<td><img class='cursor_hand' src='images/dollar.png' onclick='ShowBills(\"" + _tr_bill_id + "\");' /></td>";
  _table_html += "<td><div class='icoDollar' onclick='ShowBills(\"" + _tr_bill_id + "\");' onmousedown='DivOnMouseDown(this);' onmouseup='DivOnMouseUp(this);'></div></td>";
  _table_html += "</tr>";

  //tr 2: bills
  _td_bill_id = "td_" + _id + "_" + AccountId + "_bill";
  _table_html += "<tr id='" + _tr_bill_id + "' style='display:none;'><td></td><td colspan='7' id='" + _td_bill_id + "'>";

  _table_html += CreateTableHistoricalTerminals_TableBill(TerminalId, AccountId, ResultJsonBills, Origin);

  _table_html += "</td></tr>";

  return _table_html;
} //InsertNewTerminalRow

//////////////////////////////////////////////////////////////////////////////

function CreateTableHisoricalTerminals(ResultJson, AccountIdFilter, Origin) { //Origin="popup"; "snapshot"
  var _table_html;
  var _result_json_terminal;
  var _result_json_bills;
  var _tr_bill_id;
  var _td_bill_id;
  var _tr_info_id;
  var _terminal_id;
  var _account_id;
  var _td;
  var _table_id;

  // ResultJson[0] --> Terminals
  // ResultJson[1] --> Bill-in

  _result_json_terminal = ResultJson[0];
  _result_json_bills = ResultJson[1];

  _table_html = "";

  try {

    _table_id = "table_historical_terminals_" + Origin + "_" + AccountIdFilter;

    _table_html = "<table id='" + _table_id + "' class='table_report ui-state-default'>";

    _table_html += '<tr class="ui-widget-header"><td width="1px"><b>' + _Language.get("NLS_TERMINAL") + '</b></td><td width="1px"><b>' + _Language.get("NLS_PV") + '.</b></td><td width="1px"><b>' + _Language.get("NLS_T") + '. (s)</b></td><td width="1px"><b>' + _Language.get("NLS_P") + '. ($)</b></td><td width="1px"><b>' + _Language.get("NLS_W") + '. ($)</b></td><td width="1px"><b>' + _Language.get("NLS_NP") + '.</b></td><td width="1px"><b>' + _Language.get("NLS_%P") + '. ($)</b></td><td width="1px"><b>' + _Language.get("NLS_BILLIN") + '</b></td></tr>';

    for (var _i_terminal = 0; _i_terminal < _result_json_terminal.length; _i_terminal++) {
      _account_id = _result_json_terminal[_i_terminal].Terminal[0];

      if (_account_id == AccountIdFilter) {
        _terminal_id = _result_json_terminal[_i_terminal].Terminal[1];

        _table_html += CreateNewHistoricalTerminalRow(_terminal_id, _account_id, _result_json_terminal, _result_json_bills, _i_terminal, Origin);

      } // if
    } // for
    _table_html += "</table>";
  }
  catch (err) {
    WSILogger.Log("CreateTableHisoricalTerminals()", err);
  }

  return _table_html;
} //CreateTableHisoricalTerminals

function GetPlayerActivityComplete(result, userContext, methodName) {
  var _table;
  var _result;
  var _popup_historical_terminals;

  _popup_historical_terminals = null;
  try {
    _result = JSON.parse(result);
    if (_result[0].length > 0) {
      _table = CreateTableHisoricalTerminals(_result, _result[0][0].Terminal[0], "popup");
    }
    else {
      _table = "";
    }
    _popup_historical_terminals = $("#popup_div_principal_table_historical_terminals");

    if (_popup_historical_terminals) {
      $(_popup_historical_terminals).html(_table);
    }

  } catch (err) {
    WSILogger.Log("GetPlayerActivityComplete()", err);

    _popup_historical_terminals = $("#popup_div_principal_table_historical_terminals");

    if (_popup_historical_terminals) {
      $(_popup_historical_terminals).html("");
    }
  }
}     // GetPlayerActivityComplete

function GetPlayerActivityFailed(result, userContext, methodName) {
  $("#popup_div_principal_table_historical_terminals").html("");
}  //GetPlayerActivityFailed


function UpdatePopupTerminalHistorical(e, TerminalDiv) {

  $("#popup_div_principal_table_historical_terminals").html("<div style='align:center; vertical-align:middle'><img src='images/loading.gif' style='width:5%; height:5%;' alt='' /></div>");
  try {

    switch (_LayoutMode) {
      case 1:
        _terminal = TerminalDiv;
        break;
      default:
        _terminal = _Manager.Terminals.GetTerminal($(TerminalDiv).attr("id").replace(CONST_ID_TERMINAL, ""));
        break;
    }

    if (_Configuration.TestEnabled) {
      PageMethods.GetPlayerActivityTest(_terminal.player_account_id, false, GetPlayerActivityComplete, GetPlayerActivityFailed);
    } else {
      PageMethods.GetPlayerActivity(_terminal.player_account_id, false, GetPlayerActivityComplete, GetPlayerActivityFailed);
    }
  } catch (err) {
    WSILogger.Log("UpdatePopupTerminalHistorical()", err);
  }
} //UpdatePopupTerminalHistorical


function UpdatePopupTerminal(e, TerminalDiv) {
  var _term_pro;
  var _empty;

  var _player_name;
  var _player_age;
  var _player_level;
  var _player_gender;
  var _player_account_id;
  var _play_session_duration;
  var _play_session_total_played;
  var _play_session_played_count;
  var _play_session_bet_average;
  var _play_session_won_amount;
  var _play_session_cash_in;
  var _play_session_cash_out;
  var _terminal_provider_name;
  var _lo_object_id;
  var _terminal_bet_average;
  var _played_amount;
  var _won_amount;
  var _net_win;
  var _play_session_net_win;
  var _terminal_played_count;
  var _terminal_id;
  var _terminal_name;
  var _player_trackdata;
  var _lop_floor_id
  var _player_level_name;
  var _alarms;
  var _tasks;
  var _last_tasks;

  _empty = "---";

  try {

    switch (_LayoutMode) {
      case 1:
        _term_pro = TerminalDiv.Properties;
        break;
      default:
        if (TerminalDiv.Properties) {
          _term_pro = TerminalDiv.Properties;
        } else {
          try {
            _term_pro = _Manager.Terminals.GetTerminal($(TerminalDiv).attr("id").replace(CONST_ID_TERMINAL, "")).Properties;
          }
          catch (err) {
            _term_pro = _Manager.Terminals.GetTerminal(TerminalDiv).Properties;

          }
        }
        break;
    }

    if (_term_pro.player_name != null) {
      _player_name = _term_pro.player_name;
    } else {
      _player_name = _empty;
    }
    if (_term_pro.player_age != null) {
      _player_age = _term_pro.player_age;
    } else {
      _player_age = _empty;
    }
    if (_term_pro.player_level != null) {
      _player_level = _term_pro.player_level;
    } else {
      _player_level = _empty;
    }
    if (_term_pro.player_gender != null) {
      _player_gender = _term_pro.player_gender;
    } else {
      _player_gender = _empty;
    }
    if (_term_pro.player_account_id != null) {
      _player_account_id = _term_pro.player_account_id;
    } else {
      _player_account_id = _empty;
    }
    if (_term_pro.play_session_duration != null) {
      _play_session_duration = _term_pro.play_session_duration;
    } else {
      _play_session_duration = 0;
    }
    if (_term_pro.play_session_total_played != null) { _play_session_total_played = _term_pro.play_session_total_played; } else {
      _play_session_total_played = _empty;
    }
    if (_term_pro.play_session_played_count != null) { _play_session_played_count = _term_pro.play_session_played_count; } else {
      _play_session_played_count = _empty;
    }
    if (_term_pro.play_session_bet_average != null) { _play_session_bet_average = _term_pro.play_session_bet_average; } else {
      _play_session_bet_average = _empty;
    }
    if (_term_pro.play_session_won_amount != null) { _play_session_won_amount = _term_pro.play_session_won_amount; } else {
      _play_session_won_amount = _empty;
    }
    if (_term_pro.provider_name != null) {
      _terminal_provider_name = _term_pro.provider_name;
    } else {
      _terminal_provider_name = _empty;
    }
    if (_term_pro.lo_id != null) { _lo_object_id = _term_pro.lo_id; } else {
      _lo_object_id = _empty;
    }
    if (_term_pro.terminal_bet_average != null) {
      _terminal_bet_average = _term_pro.terminal_bet_average;
    } else {
      _terminal_bet_average = _empty;
    }
    if (_term_pro.played_amount != null) {
      _played_amount = _term_pro.played_amount;
    } else {
      _played_amount = _empty;
    }
    if (_term_pro.won_amount != null) {
      _won_amount = _term_pro.won_amount;
    } else {
      _won_amount = _empty;
    }
    if (_term_pro.net_win != null) { _net_win = _term_pro.net_win; } else {
      _net_win = _empty;
    }
    if (_term_pro.play_session_net_win != null) {
      _play_session_net_win = _term_pro.play_session_net_win;
    } else {
      _play_session_net_win = _empty;
    }
    if (_term_pro.played_count != null) {
      _terminal_played_count = _term_pro.played_count;
    } else {
      _terminal_played_count = _empty;
    }
    if (_term_pro.external_id != null) {
      _terminal_id = _term_pro.external_id;
    } else {
      _terminal_id = _empty;
    }
    if (_term_pro.name != null) { _terminal_name = _term_pro.name; } else {
      _terminal_name = _empty;
    }
    if (_term_pro.player_trackdata != null) {
      _player_trackdata = _term_pro.player_trackdata;
    } else {
      _player_trackdata = _empty;
    }
    if (_term_pro.lop_floor_id != null) {
      _lop_floor_id = _term_pro.lop_floor_id;
    } else {
      _lop_floor_id = _empty;
    }
    if (_term_pro.player_level_name != null) {
      _player_level_name = _term_pro.player_level_name;
    } else {
      _player_level_name = _empty;
    }

    if (_term_pro.play_session_cash_in != null) {
      _play_session_cash_in = _term_pro.play_session_cash_in;
    } else {
      _play_session_cash_in = _empty;
    }

    if (_term_pro.play_session_cash_out != null) {
      _play_session_cash_out = _term_pro.play_session_cash_out;
    } else {
      _play_session_cash_out = _empty;
    }

    if (_player_gender == "1") {
      _player_gender = _Language.get("NLS_MALE");
    }
    else if (_player_gender == "2") {
      _player_gender = _Language.get("NLS_FEMALE");
    }

    //TODO:Quitar
    if (_term_pro.player_is_vip == "1") {
      $(".photoIsVIPContainer").show();
    } else {
      $(".photoIsVIPContainer").hide();
    }

    if (_player_name == _empty || _player_name == "Anonymous") {
      $(".popup_player_age").parent().hide();
      $(".popup_player_level").parent().hide();
      $(".popup_player_gender").parent().hide();
      $(".jqDay").hide();
      $(".datosSesion").hide();
    }
    else {
      $(".popup_player_age").parent().show();
      $(".popup_player_level").parent().show();
      $(".popup_player_gender").parent().show();
      $(".jqDay").show();
      $(".datosSesion").show();
    }

    $(".popup_terminal_id").text(_terminal_id);
    $(".popup_player_name").text(_player_name);
    $(".popup_account_id").text(_player_account_id);
    $(".popup_player_trackdata").text(_player_trackdata);
    $(".popup_player_age").text(_player_age);
    $(".popup_player_level").text(_player_level_name);
    $(".popup_player_gender").html(_player_gender);
    $(".popup_play_session_duration").text(FormatSeconds(_play_session_duration));
    $(".popup_object_id").text(_lo_object_id);
    $(".popup_img_close")
        .attr("onclick", "HiddenDiv(popup_div_principal); if (_LayoutMode != 1) { UnselectTerminalDiv(" + $(TerminalDiv).attr("id") + "); } else { WSIData.SelectedObject = null; _Manager.HideSelector(); } ")
        .attr("onmousedown", "DivOnMouseDown(this);")
        .attr("onmouseup", "DivOnMouseUp(this);");
    $(".popup_terminal_played_count").text(_terminal_played_count);
    //grilla
    $(".popup_session_terminal_played_count").text(_terminal_played_count);

    $(".popup_terminal_floor").text(_lop_floor_id);

    if (_play_session_net_win && _play_session_net_win < 0) {
      $(".popup_play_session_net_win").removeClass("netwin_positive");
      $(".popup_play_session_net_win").addClass("netwin_negative");
    }
    else {
      $(".popup_play_session_net_win").removeClass("netwin_negative");
      $(".popup_play_session_net_win").addClass("netwin_positive");
    }

    $(".popup_terminal_link").text(_term_pro.name);
    $(".popup_session_played_count").text(_play_session_played_count);
    $(".popup_session_won_amount").text(FormatString(_play_session_won_amount, EN_FORMATS.F_MONEY));
    // $(".popup_day_won_amount").text = _play_session_won_amount;  //cambiar a jornada
    $(".popup_session_played_amount").text(FormatString(_play_session_total_played, EN_FORMATS.F_MONEY));
    // $(".popup_day_played_amount").text = _play_session_total_played; //cambiar a jornada
    $(".popup_session_bet_average").text(FormatString(_play_session_bet_average, EN_FORMATS.F_MONEY));
    // $(".popup_day_bet_average").text = _play_session_bet_average; //cambiar a jornada

    //  $(".popup_day_netwin").text = PlaySessionNetWin; //cambiar a jornada
    $(".popup_session_cash_in").text = _play_session_bet_average;
    $(".popup_day_cash_in").text = _play_session_bet_average; //cambiar a jornada
    $(".popup_session_cash_out").text = _play_session_bet_average;
    $(".popup_day_cash_out").text = _play_session_bet_average; //cambiar a jornada

    switch (m_current_section) {
      case 40:
        //LOAD ALARMS & TASKS DATA
        _alarms = _Manager.TaskManager.AlarmsByTerminal[+_term_pro.external_id];
        _tasks = _Manager.TaskManager.TasksByTerminal[+_term_pro.external_id];
        _last_tasks = _Manager.TaskManager.LastTasks[+_term_pro.external_id];

        TerminalInfoPanelSectionAlarmsFill(_alarms, _tasks, _last_tasks);

        $(".popup_terminal_name").text(_terminal_name);
        $(".popup_terminal_provider").text(_terminal_provider_name);


        break;

      case 60:

        $(".popup_session_netwin").text(FormatString(_play_session_total_played - _play_session_won_amount, EN_FORMATS.F_MONEY));

        if ((_play_session_total_played - _play_session_won_amount) < 0) {
          $(".popup_session_netwin").addClass("netwin_negative");
          $(".popup_session_netwin").removeClass("netwin_positive");
        } else {
          $(".popup_session_netwin").removeClass("netwin_negative");
          $(".popup_session_netwin").addClass("netwin_positive");
        }


        //LOAD Player Info Data + Charts
        LoadPlayerInfo(_play_session_total_played
                      , _play_session_played_count
                      , _play_session_bet_average
                      , _play_session_won_amount
                      , _play_session_net_win
                      , _play_session_cash_in
                      , _play_session_cash_out
                      , _player_account_id)
        break;

      case 70:
        $(".popup_session_netwin").text(FormatString(_play_session_total_played - _play_session_won_amount, EN_FORMATS.F_MONEY));


        //LOAD Player Info Data + Charts
        LoadTerminalInfo(_terminal_id
                      , _terminal_name
                      , _terminal_provider_name
                      , _play_session_bet_average
                      , _play_session_total_played
                      , _play_session_won_amount
                      , _play_session_net_win
                      , _term_pro.terminal_payout
                      , _term_pro.play_session_status
                      , _play_session_played_count
                      )

        break;

      case 80:

        ////LOAD Player Info Data + Charts
        //LoadPlayerInfo(_play_session_total_played
        //              , _play_session_played_count
        //              , _play_session_bet_average
        //              , _play_session_won_amount
        //              , _play_session_net_win
        //              , _player_account_id)

        ////LOAD Player Info Data + Charts
        //LoadTerminalInfo(_terminal_id
        //            , _terminal_name
        //            , _terminal_provider_name
        //            , _terminal_bet_average
        //            , _played_amount
        //            , _won_amount
        //            , _net_win)

        //if (_term_pro.player_account_id == "") {
        //  $(".monitorJugador").hide();
        //} else {
        //  $(".monitorJugador").show();
        //}
        break;
    }

    if ((_play_session_total_played - _play_session_won_amount) && (_play_session_total_played - _play_session_won_amount) < 0) {
      $(".popup_net_win").removeClass("netwin_positive");
      $(".popup_net_win").addClass("netwin_negative");
    }
    else {
      $(".popup_net_win").removeClass("netwin_negative");
      $(".popup_net_win").addClass("netwin_positive");
    }


    //only show client info if exists (terminal has activity)
    if (_term_pro.play_session_status == 2 && m_current_section != 80) {
      $("#popup_div_principal_table_info_client").css("display", "block");
    }
    else {
      $("#popup_div_principal_table_info_client").css("display", "none");
    }
  }
  catch (err) {
    _table1 = "";
    WSILogger.Log("UpdatePopupTerminal()", err);
  }

} // UpdatePopupTerminal

function LoadPlayerInfo(PlaySessionTotalPlayed
                    , PlaySessionPlayedCount
                    , PlaySessionBetAverage
                    , PlaySessionWonAmount
                    , PlaySessionNetWin
                    , PlaySessionCashIn
                    , PlaySessionCashOut
                    , AccountId) {
  var opts = {
    lines: 13, // The number of lines to draw
    length: 20, // The length of each line
    width: 13, // The line thickness
    radius: 60, // The radius of the inner circle
    corners: 1, // Corner roundness (0..1)
    rotate: 0, // The rotation offset
    direction: 1, // 1: clockwise, -1: counterclockwise
    color: '#EDEDED', // #rgb or #rrggbb or array of colors
    speed: 1.4, // Rounds per second
    trail: 100, // Afterglow percentage
    shadow: true, // Whether to render a shadow
    hwaccel: true, // Whether to use hardware acceleration
    className: 'spinner', // The CSS class to assign to the spinner
    zIndex: 2e9, // The z-index (defaults to 2000000000)
    top: '35%', // Top position relative to parent
    left: '50%' // Left position relative to parent
  };
  var _loading_tag_id = "playerInfoLoad";
  var _slide_tag = ".infoJugador";

  if (m_current_section == 80) {
    _loading_tag_id = "playerInfoLoadMonitor";
    _slide_tag = ".infoMonitor";
  }

  var target = document.getElementById(_loading_tag_id);
  var spinner = new Spinner(opts).spin(target);

  $(_slide_tag).find(".datosSesion").css("visibility", "hidden");

  PageMethods.LoadInfoPlayerData(+AccountId, function (Result) {

    var _result = JSON.parse(Result);
    var _today;
    var _working_day;
    var _last_n_days = _result.LastNDays.rows;
    var _working_day = _result.WorkingDay.rows[0];
    var _td_plays_count = [], _td_played = [], _td_won = [], _td_bet_avg = [], _td_netwin = []; //today
    var _wd_plays_count = [], _wd_played = [], _wd_won = [], _wd_bet_avg = [], _wd_netwin = []; //working day
    var _lnd_plays_count = [], _lnd_played = [], _lnd_won = [], _lnd_bet_avg = [], _lnd_netwin = [], _lnd_cash_in = [], _lnd_cash_out = []; // last n days
    var _chart_width = 300;
    var _wid_played_tag = "#wid_played";
    var _wid_played_count_tag = "#wid_played_count";
    var _wid_won_tag = "#wid_won";
    var _wid_betavg_tag = "#wid_betavg";
    var _wid_netwin_tag = "#wid_netwin";

    if ($(window).width() < 1400) {
      _chart_width = 290;
    }

    for (_idx_last_n_days in _last_n_days) {

      if (_idx_last_n_days == 0) {
        var _netwin = (+_working_day[3]) - (+_working_day[1]);
        var _bet_avg = (+_working_day[1]) / (+_working_day[0]);

        _lnd_plays_count.push(+_working_day[0]);
        _lnd_played.push(+_working_day[1]);
        _lnd_won.push(+_working_day[3]);
        _lnd_bet_avg.push(_bet_avg);
        _lnd_netwin.push(_netwin);
        _lnd_cash_in.push(+_working_day[5]);
        _lnd_cash_out.push(+_working_day[6]);

        continue;
      }

      _lnd_plays_count.push(+_last_n_days[_idx_last_n_days][0]);
      _lnd_played.push(+_last_n_days[_idx_last_n_days][1].replace(",", "."));
      _lnd_won.push(+_last_n_days[_idx_last_n_days][2].replace(",", "."));
      _lnd_bet_avg.push(+_last_n_days[_idx_last_n_days][3].replace(",", "."));
      _lnd_netwin.push(+_last_n_days[_idx_last_n_days][4].replace(",", "."));
      _lnd_cash_in.push(+_last_n_days[_idx_last_n_days][5].replace(",", "."));
      _lnd_cash_out.push(+_last_n_days[_idx_last_n_days][6].replace(",", "."));
    }

    if (m_current_section == 80) {
      _wid_played_tag = "#wid_played_monitor";
      _wid_played_count_tag = "#wid_played_count_monitor";
      _wid_won_tag = "#wid_won_monitor";
      _wid_betavg_tag = "#wid_betavg_monitor";
      _wid_netwin_tag = "#wid_netwin_monitor";
    }

    var clase = "player_bar_positive";

    if (_lnd_played[0] < 0)
      clase = "player_bar_negative";
    else
      clase = "player_bar_positive";

    var _maximo = Math.max.apply(Math, _lnd_played);
    //jugado
    $('.graf_bar_played1').multiprogressbar({
      parts: [
      {
        value: Math.round((_lnd_played[0] * 100) / _maximo), text: FormatString(_lnd_played[0], EN_FORMATS.F_MONEY), barClass: clase, textClass: "lbl_value_player"
      },
      {
        value: Math.round(((_maximo - _lnd_played[0]) * 100) / _maximo), text: '', barClass: "player_bar_days", textClass: "lbl_value_task"
      }
      ]
    });

    if (_lnd_played[1] < 0)
      clase = "player_bar_negative";
    else
      clase = "player_bar_positive";

    $('.graf_bar_played2').multiprogressbar({
      parts: [
      {
        value: Math.round((_lnd_played[1] * 100) / _maximo), text: FormatString(_lnd_played[1], EN_FORMATS.F_MONEY), barClass: clase, textClass: "lbl_value_player"
      },
      {
        value: Math.round(((_maximo - _lnd_played[1]) * 100) / _maximo), text: '', barClass: "player_bar_days", textClass: "lbl_value_player"
      }
      ]
    });

    if (_lnd_played[2] < 0)
      clase = "player_bar_negative";
    else
      clase = "player_bar_positive";

    $('.graf_bar_played3').multiprogressbar({
      parts: [
      {
        value: Math.round((_lnd_played[2] * 100) / _maximo), text: FormatString(_lnd_played[2], EN_FORMATS.F_MONEY), barClass: clase, textClass: "lbl_value_player"
      },
      {
        value: Math.round(((_maximo - _lnd_played[2]) * 100) / _maximo), text: '', barClass: "player_bar_days", textClass: "lbl_value_player"
      }
      ]
    });
    //cantidad de jugadas
    _maximo = Math.max.apply(Math, _lnd_plays_count);

    $('.graf_bar_played_count1').multiprogressbar({
      parts: [
      {
        value: Math.round((_lnd_plays_count[0] * 100) / _maximo), text: _lnd_plays_count[0], barClass: clase, textClass: "lbl_value_player"
      },
      {
        value: Math.round(((_maximo - _lnd_plays_count[0]) * 100) / _maximo), text: '', barClass: "player_bar_days", textClass: "lbl_value_task"
      }
      ]
    });

    $('.graf_bar_played_count2').multiprogressbar({
      parts: [
      {
        value: Math.round((_lnd_plays_count[1] * 100) / _maximo), text: _lnd_plays_count[1], barClass: clase, textClass: "lbl_value_player"
      },
      {
        value: Math.round(((_maximo - _lnd_plays_count[1]) * 100) / _maximo), text: '', barClass: "player_bar_days", textClass: "lbl_value_player"
      }
      ]
    });

    $('.graf_bar_played_count3').multiprogressbar({
      parts: [
      {
        value: Math.round((_lnd_plays_count[2] * 100) / _maximo), text: _lnd_plays_count[2], barClass: clase, textClass: "lbl_value_player"
      },
      {
        value: Math.round(((_maximo - _lnd_plays_count[2]) * 100) / _maximo), text: '', barClass: "player_bar_days", textClass: "lbl_value_player"
      }
      ]
    });

    //cantidad de Ganadas
    _maximo = Math.max.apply(Math, _lnd_won);

    $('.graf_bar_played_won1').multiprogressbar({
      parts: [
      {
        value: Math.round((_lnd_won[0] * 100) / _maximo), text: FormatString(_lnd_won[0], EN_FORMATS.F_MONEY), barClass: clase, textClass: "lbl_value_player"
      },
      {
        value: Math.round(((_maximo - _lnd_won[0]) * 100) / _maximo), text: '', barClass: "player_bar_days", textClass: "lbl_value_task"
      }
      ]
    });

    $('.graf_bar_played_won2').multiprogressbar({
      parts: [
      {
        value: Math.round((_lnd_won[1] * 100) / _maximo), text: FormatString(_lnd_won[1], EN_FORMATS.F_MONEY), barClass: clase, textClass: "lbl_value_player"
      },
      {
        value: Math.round(((_maximo - _lnd_won[1]) * 100) / _maximo), text: '', barClass: "player_bar_days", textClass: "lbl_value_player"
      }
      ]
    });

    $('.graf_bar_played_won3').multiprogressbar({
      parts: [
      {
        value: Math.round((_lnd_won[2] * 100) / _maximo), text: FormatString(_lnd_won[2], EN_FORMATS.F_MONEY), barClass: clase, textClass: "lbl_value_player"
      },
      {
        value: Math.round(((_maximo - _lnd_won[2]) * 100) / _maximo), text: '', barClass: "player_bar_days", textClass: "lbl_value_player"
      }
      ]
    });

    //apuesta media
    _maximo = Math.max.apply(Math, _lnd_bet_avg);

    $('.graf_bar_played_avg1').multiprogressbar({
      parts: [
      {
        value: Math.round((_lnd_bet_avg[0] * 100) / _maximo), text: FormatString(_lnd_bet_avg[0], EN_FORMATS.F_MONEY), barClass: clase, textClass: "lbl_value_player"
      },
      {
        value: Math.round(((_maximo - _lnd_bet_avg[0]) * 100) / _maximo), text: '', barClass: "player_bar_days", textClass: "lbl_value_task"
      }
      ]
    });

    $('.graf_bar_played_avg2').multiprogressbar({
      parts: [
      {
        value: Math.round((_lnd_bet_avg[1] * 100) / _maximo), text: FormatString(_lnd_bet_avg[1], EN_FORMATS.F_MONEY), barClass: clase, textClass: "lbl_value_player"
      },
      {
        value: Math.round(((_maximo - _lnd_bet_avg[1]) * 100) / _maximo), text: '', barClass: "player_bar_days", textClass: "lbl_value_player"
      }
      ]
    });

    $('.graf_bar_played_avg3').multiprogressbar({
      parts: [
      {
        value: Math.round((_lnd_bet_avg[2] * 100) / _maximo), text: FormatString(_lnd_bet_avg[2], EN_FORMATS.F_MONEY), barClass: clase, textClass: "lbl_value_player"
      },
      {
        value: Math.round(((_maximo - _lnd_bet_avg[2]) * 100) / _maximo), text: '', barClass: "player_bar_days", textClass: "lbl_value_player"
      }
      ]
    });

    //NET WIN
    if (_lnd_netwin[0] < 0)
      clase = "player_bar_negative";
    else
      clase = "player_bar_positive";

    _maximo = obtenerMaximo(_lnd_netwin);
    $('.graf_bar_played_netwin1').multiprogressbar({
      parts: [
      {
        value: Math.abs(Math.round((Math.abs(_lnd_netwin[0]) * 100) / _maximo)), text: FormatString(_lnd_netwin[0], EN_FORMATS.F_MONEY), barClass: clase, textClass: "lbl_value_player"
      },
      {
        value: Math.round(((_maximo - Math.abs(_lnd_netwin[0])) * 100) / _maximo), text: '', barClass: "player_bar_days", textClass: "lbl_value_task"
      }
      ]
    });

    if (_lnd_netwin[1] < 0)
      clase = "player_bar_negative";
    else
      clase = "player_bar_positive";

    $('.graf_bar_played_netwin2').multiprogressbar({
      parts: [
      {
        value: Math.abs(Math.round((_lnd_netwin[1] * 100) / _maximo)), text: FormatString(_lnd_netwin[1], EN_FORMATS.F_MONEY), barClass: clase, textClass: "lbl_value_player"
      },
      {
        value: Math.round(((_maximo - Math.abs(_lnd_netwin[1])) * 100) / _maximo), text: '', barClass: "player_bar_days", textClass: "lbl_value_player"
      }
      ]
    });

    if (_lnd_netwin[2] < 0)
      clase = "player_bar_negative";
    else
      clase = "player_bar_positive";

    $('.graf_bar_played_netwin3').multiprogressbar({
      parts: [
      {
        value: Math.abs(Math.round((_lnd_netwin[2] * 100) / _maximo)), text: FormatString(_lnd_netwin[2], EN_FORMATS.F_MONEY), barClass: clase, textClass: "lbl_value_player"
      },
      {
        value: Math.round(((_maximo - Math.abs(_lnd_netwin[2])) * 100) / _maximo), text: '', barClass: "player_bar_days", textClass: "lbl_value_player"
      }
      ]
    });

    //Actual
    $(".popup_play_session_total_played").text(FormatString(PlaySessionTotalPlayed, EN_FORMATS.F_MONEY));
    $(".popup_play_session_played_count").text(PlaySessionPlayedCount);
    $(".popup_play_session_bet_average").text(FormatString(PlaySessionBetAverage, EN_FORMATS.F_MONEY));
    $(".popup_play_session_won_amount").text(FormatString(PlaySessionWonAmount, EN_FORMATS.F_MONEY));
    $(".popup_play_session_net_win").text(FormatString(PlaySessionWonAmount - PlaySessionTotalPlayed, EN_FORMATS.F_MONEY));
    $(".popup_session_cash_in").text(FormatString(PlaySessionCashIn, EN_FORMATS.F_MONEY));
    $(".popup_session_cash_out").text(FormatString(PlaySessionCashOut, EN_FORMATS.F_MONEY));

    if (+PlaySessionNetWin && PlaySessionNetWin < 0) {
      $(".popup_play_session_net_win").addClass("netwin_negative");
    } else {
      $(".popup_play_session_net_win").removeClass("netwin_positive");
    }

    //Jornada

    //Played
    if (_working_day[1] == "" || isNaN(_working_day[1])) {
      $(".popup_wd_play_session_total_played").text(FormatString(PlaySessionTotalPlayed, EN_FORMATS.F_MONEY));
      $(".popup_day_played_amount").text(FormatString(PlaySessionTotalPlayed, EN_FORMATS.F_MONEY));
    } else {
      $(".popup_wd_play_session_total_played").text(FormatString(_working_day[1], EN_FORMATS.F_MONEY));
      $(".popup_day_played_amount").text(FormatString(_working_day[1], EN_FORMATS.F_MONEY));
    }

    //Played count
    if (_working_day[0] == "" || isNaN(_working_day[0])) {
      $(".popup_wd_play_session_played_count").text(PlaySessionPlayedCount);
      $(".popup_day_played_count").text(PlaySessionPlayedCount);
    } else {
      $(".popup_wd_play_session_played_count").text(_working_day[0]);
      $(".popup_day_played_count").text(_working_day[0]);
    }

    //Bet Avg
    if (_working_day[2] == "" || isNaN(_working_day[2])) {
      $(".popup_wd_play_session_bet_average").text(FormatString(PlaySessionBetAverage, EN_FORMATS.F_MONEY));
      $(".popup_day_bet_average").text(FormatString(PlaySessionBetAverage, EN_FORMATS.F_MONEY));
    } else {
      $(".popup_wd_play_session_bet_average").text(FormatString(_working_day[2], EN_FORMATS.F_MONEY));
      $(".popup_day_bet_average").text(FormatString(_working_day[2], EN_FORMATS.F_MONEY));

    }

    //Won
    if (_working_day[3] == "" || isNaN(_working_day[3])) {
      $(".popup_wd_play_session_won_amount").text(FormatString(PlaySessionWonAmount, EN_FORMATS.F_MONEY));
      $(".popup_day_won_amount").text(FormatString(PlaySessionWonAmount, EN_FORMATS.F_MONEY));
    } else {
      $(".popup_wd_play_session_won_amount").text(FormatString(_working_day[3], EN_FORMATS.F_MONEY));
      $(".popup_day_won_amount").text(FormatString(_working_day[3], EN_FORMATS.F_MONEY));
    }

    //NetWin
    if (_working_day[4] == "" || isNaN(_working_day[4])) {
      $(".popup_day_netwin").text(FormatString(PlaySessionNetWin, EN_FORMATS.F_MONEY));
      if (+PlaySessionNetWin && PlaySessionNetWin < 0) {
        $(".popup_day_netwin").addClass("netwin_negative");
        $(".popup_day_netwin").removeClass("netwin_positive");
      } else {
        $(".popup_day_netwin").removeClass("netwin_negative");
        $(".popup_day_netwin").addClass("netwin_positive");
      }

    } else {
      var _netwin = +_working_day[1] - +_working_day[3];
      $(".popup_day_netwin").text(FormatString(_netwin, EN_FORMATS.F_MONEY));
      if (_netwin && +_netwin < 0) {
        $(".popup_day_netwin").addClass("netwin_negative");
        $(".popup_day_netwin").removeClass("netwin_positive");
      } else {
        $(".popup_day_netwin").removeClass("netwin_negative");
        $(".popup_day_netwin").addClass("netwin_positive");
      }
    }

    //CashIn
    if (_working_day[5] == "" || isNaN(_working_day[5])) {
      $(".popup_day_cash_in").text(FormatString(PlaySessionCashIn, EN_FORMATS.F_MONEY));
    } else {
      $(".popup_day_cash_in").text(FormatString(_working_day[5], EN_FORMATS.F_MONEY));
    }

    //CashOut
    if (_working_day[6] == "" || isNaN(_working_day[6])) {
      $(".popup_day_cash_out").text(FormatString(PlaySessionCashOut, EN_FORMATS.F_MONEY));
    } else {
      $(".popup_day_cash_out").text(FormatString(_working_day[6], EN_FORMATS.F_MONEY));
    }


    if (m_current_section == 80) {
      $("#playerInfoLoadMonitor").hide();
      $("#playerInfoLoadMonitor").empty();
      $(".infoMonitor").find(".datosSesion").css("visibility", "visible");
    } else {
      $("#playerInfoLoad").hide();
      $("#playerInfoLoad").empty();
      $(".infoJugador").find(".datosSesion").css("visibility", "visible");
    }

  }, OnLoadInfoPlayerDataFailed);

}

function obtenerMaximo(valores) {
  var max = 0;
  for (var i = 0; i < valores.length; i++) {
    if (max < Math.abs(valores[i])) {
      max = Math.abs(valores[i]);
    }
  }
  return max;
}


function UpdatePopupRunner(Runner) {
  //TODO: DEMO BEACONS

  $(".popup_runner_name").text(Runner.userName);
  //$(".popup_runner_role").text(Runner.roles);
  $(".popup_runner_device").text(Runner.deviceId);

  PageMethods.GetRunnerTasks(+Runner.id, function (Result) {
    var _data = JSON.parse(Result);

    _current_tasks = _data.Current.rows;
    _lasts_tasks = _data.Last.rows;

    TerminalInfoPanelSectionRunnersFill(_current_tasks, _lasts_tasks, Runner);

  }, OnGetRunnerTasksError);
}

function OnGetRunnerTasksError() {

}

function LoadTerminalInfo(TerminalId
                      , TerminalName
              , TerminalProviderName
              , TerminalBetAverage
              , PlayedAmount
              , WonAmount
              , NetWin
          , Payout
          , Session
, PlayedCount) {

  var opts = {
    lines: 13, // The number of lines to draw
    length: 20, // The length of each line
    width: 13, // The line thickness
    radius: 60, // The radius of the inner circle
    corners: 1, // Corner roundness (0..1)
    rotate: 0, // The rotation offset
    direction: 1, // 1: clockwise, -1: counterclockwise
    color: '#EDEDED', // #rgb or #rrggbb or array of colors
    speed: 1.4, // Rounds per second
    trail: 100, // Afterglow percentage
    shadow: true, // Whether to render a shadow
    hwaccel: true, // Whether to use hardware acceleration
    className: 'spinner', // The CSS class to assign to the spinner
    zIndex: 2e9, // The z-index (defaults to 2000000000)
    top: '35%', // Top position relative to parent
    left: '50%' // Left position relative to parent
  };
  var _loading_tag_id = "playerInfoLoad";
  var _slide_tag = ".infoJugador";
  var _chart_width = 300;


  if (m_current_section == 80) {
    _loading_tag_id = "playerInfoLoadMonitor";
    _slide_tag = ".infoMonitor";
  }

  var target = document.getElementById(_loading_tag_id);
  var spinner = new Spinner(opts).spin(target);

  $(_slide_tag).find(".datosSesion").css("visibility", "hidden");


  if ($(window).width() < 1400) {
    _chart_width = 290;
  }

  var _common_chart = {
    chart: {
      backgroundColor: null,
      borderWidth: 0,
      marginBottom: 4,
      marginTop: 4,
      marginLeft: 2,
      marginRight: 4,
      width: _chart_width,
      height: 80,
      skipClone: true
    },
    credits: {
      enabled: false
    },
    exporting: {
      enabled: false
    },
    title: {
      text: null
    },
    xAxis: {
      labels: {
        enabled: false
      },
      title: {
        text: null
      },
      startOnTick: false,
      endOnTick: false,
      tickPositions: []
    },
    yAxis: {
      endOnTick: false,
      startOnTick: false,
      labels: {
        enabled: true
      },
      title: {
        text: null
      },
      tickPositions: [0]
    },
    title: {
      text: null
    },
    legend: {
      enabled: false
    },
    tooltip: {
      //enabled: false,
      formatter: function () {
        return this.y;
      }
    },
    series: [{
      name: '',
      data: [0, 0, 0],
      dataLabels: {
        enabled: true
      }
    },
    {
      name: '',
      data: [0, 0, 0],
      dataLabels: {
        enabled: false
      },
      lineWidth: 1,
      marker: {
        radius: 0
      }
    },
    ]
  };

  //Current Session
  $(".popup_session_terminal_netwin").text(FormatString(NetWin, EN_FORMATS.F_MONEY));
  $(".popup_session_terminal_bet_average").text(FormatString(TerminalBetAverage, EN_FORMATS.F_MONEY));
  $(".popup_session_terminal_played_amount").text(FormatString(PlayedAmount, EN_FORMATS.F_MONEY));
  $(".popup_session_terminal_won_amount").text(FormatString(WonAmount, EN_FORMATS.F_MONEY));
  $(".popup_session_terminal_played_count").text(PlayedCount);

  if (NetWin && NetWin < 0) {
    $(".popup_session_terminal_netwin").removeClass("netwin_positive");
    $(".popup_session_terminal_netwin").addClass("netwin_negative");
  }
  else {
    $(".popup_session_terminal_netwin").removeClass("netwin_negative");
    $(".popup_session_terminal_netwin").addClass("netwin_positive");
  }

  if (m_current_section == 80) {
    $("#terminalInfoLoadMonitor").hide();
    $("#terminalInfoLoadMonitor").empty();
    $(".infoMonitor").find(".datosSesion").css("visibility", "visible");
  } else {
    $("#terminalInfoLoad").hide();
    $("#terminalInfoLoad").empty();
    $(".infoTerminal").find(".datosSesion").css("visibility", "visible");
  }


  $(".popup_terminal_name").text(TerminalName);
  $(".popup_terminal_provider").text(TerminalProviderName);

  $(".popup_terminal_currentplay ").text(TerminalName);

  if (Session = "1") {
    $(".popup_terminal_session").text(_Language.get("NLS_SESSION_ACTIVE"));
  } else {
    $(".popup_terminal_session").text(_Language.get("NLS_SESSION_INACTIVE"));
  }

  $(".popup_terminal_payoutplay").text(Payout);


  PageMethods.LoadTerminalHistoryData(+TerminalId, function (Result) {

    if (Result != "") {
      var _result = JSON.parse(Result);
      //     var _chart_width = 300;
      var _last_n_days = _result.LastNDays.rows;
    }




    var _lnd_plays_count = [], _lnd_played = [], _lnd_won_count = [], _lnd_won = [], _lnd_bet_avg = [], _lnd_netwin = [], _lnd_won_amount; // last n days
    var _first_day_counter = 0;

    for (_idx_last_n_days in _last_n_days) {

      if (_first_day_counter < 1) {
        //Gaming Day
        var _net_win = +_last_n_days[_idx_last_n_days][6].replace(",", ".");
        $(".popup_day_terminal_netwin").text(FormatString(_net_win, EN_FORMATS.F_MONEY));
        $(".popup_day_terminal_bet_average").text(FormatString(+_last_n_days[_idx_last_n_days][5].replace(",", "."), EN_FORMATS.F_MONEY));
        $(".popup_day_terminal_played_amount").text(FormatString(+_last_n_days[_idx_last_n_days][3].replace(",", "."), EN_FORMATS.F_MONEY));
        $(".popup_day_won_amount").text(FormatString(+_last_n_days[_idx_last_n_days][7].replace(",", "."), EN_FORMATS.F_MONEY));
        $(".popup_day_terminal_played_count").text(+_last_n_days[_idx_last_n_days][2]);

        if (_net_win && _net_win < 0) {
          $(".popup_day_terminal_netwin").removeClass("netwin_positive");
          $(".popup_day_terminal_netwin").addClass("netwin_negative");
        }
        else {
          $(".popup_day_terminal_netwin").removeClass("netwin_negative");
          $(".popup_day_terminal_netwin").addClass("netwin_positive");
        }

        _first_day_counter++;
      }

      _lnd_plays_count.push(+_last_n_days[_idx_last_n_days][2]);
      _lnd_played.push(+_last_n_days[_idx_last_n_days][3].replace(",", "."));
      _lnd_won_count.push(+_last_n_days[_idx_last_n_days][4].replace(",", "."));
      _lnd_bet_avg.push(+_last_n_days[_idx_last_n_days][5].replace(",", "."));
      _lnd_netwin.push(+_last_n_days[_idx_last_n_days][6].replace(",", "."));
      _lnd_won.push(+_last_n_days[_idx_last_n_days][7].replace(",", "."));
    }

    //graficos

    //NetWin

    var _maximo = obtenerMaximo(_lnd_netwin);

    if (_lnd_netwin.length > 0) {
      if (_lnd_netwin[0] < 0) clase = "player_bar_negative"; else clase = "player_bar_positive";
      $('.graf_bar_terminal_netWin1').multiprogressbar({
        parts: [{
          value: Math.round((Math.abs(_lnd_netwin[0]) * 100) / _maximo), text: FormatString(_lnd_netwin[0], EN_FORMATS.F_MONEY), barClass: clase, textClass: "lbl_value_player"
        },
        {
          value: Math.round(((_maximo - Math.abs(_lnd_netwin[0])) * 100) / _maximo), text: '', barClass: "player_bar_days", textClass: "lbl_value_task"
        }]
      });
    }
    if (_lnd_netwin.length > 1) {
      if (_lnd_netwin[1] < 0) clase = "player_bar_negative"; else clase = "player_bar_positive";
      $('.graf_bar_terminal_netWin2').multiprogressbar({
        parts: [{
          value: Math.round((Math.abs(_lnd_netwin[1]) * 100) / _maximo), text: FormatString(_lnd_netwin[1], EN_FORMATS.F_MONEY), barClass: clase, textClass: "lbl_value_player"
        },
        {
          value: Math.round(((_maximo - Math.abs(_lnd_netwin[1])) * 100) / _maximo), text: '', barClass: "player_bar_days", textClass: "lbl_value_player"
        }]
      });
    }
    if (_lnd_netwin.length == 3) {
      if (_lnd_netwin[2] < 0) clase = "player_bar_negative"; else clase = "player_bar_positive";
      $('.graf_bar_terminal_netWin3').multiprogressbar({
        parts: [{
          value: Math.round((Math.abs(_lnd_netwin[2]) * 100) / _maximo), text: FormatString(_lnd_netwin[2], EN_FORMATS.F_MONEY), barClass: clase, textClass: "lbl_value_player"
        },
        {
          value: Math.round(((_maximo - Math.abs(_lnd_netwin[2])) * 100) / _maximo), text: '', barClass: "player_bar_days", textClass: "lbl_value_player"
        }]
      });
    }

    //Average
    _maximo = 0;
    _maximo = obtenerMaximo(_lnd_bet_avg);
    if (_lnd_bet_avg.length > 0) {
      if (_lnd_bet_avg[0] < 0) clase = "player_bar_negative"; else clase = "player_bar_positive";
      $('.graf_bar_terminal_avg1').multiprogressbar({
        parts: [{
          value: Math.round((_lnd_bet_avg[0] * 100) / _maximo), text: FormatString(_lnd_bet_avg[0], EN_FORMATS.F_MONEY), barClass: clase, textClass: "lbl_value_player"
        },
        {
          value: Math.round(((_maximo - _lnd_bet_avg[0]) * 100) / _maximo), text: '', barClass: "player_bar_days", textClass: "lbl_value_task"
        }]
      });
    }
    if (_lnd_bet_avg.length > 1) {
      if (_lnd_bet_avg[1] < 0) clase = "player_bar_negative"; else clase = "player_bar_positive";
      $('.graf_bar_terminal_avg2').multiprogressbar({
        parts: [{
          value: Math.round((_lnd_bet_avg[1] * 100) / _maximo), text: FormatString(_lnd_bet_avg[1], EN_FORMATS.F_MONEY), barClass: clase, textClass: "lbl_value_player"
        },
        {
          value: Math.round(((_maximo - _lnd_bet_avg[1]) * 100) / _maximo), text: '', barClass: "player_bar_days", textClass: "lbl_value_player"
        }]
      });
    }
    if (_lnd_bet_avg.length == 3) {
      if (_lnd_bet_avg[2] < 0) clase = "player_bar_negative"; else clase = "player_bar_positive";
      $('.graf_bar_terminal_avg3').multiprogressbar({
        parts: [{
          value: Math.round((_lnd_bet_avg[2] * 100) / _maximo), text: FormatString(_lnd_bet_avg[2], EN_FORMATS.F_MONEY), barClass: clase, textClass: "lbl_value_player"
        },
        {
          value: Math.round(((_maximo - _lnd_bet_avg[2]) * 100) / _maximo), text: '', barClass: "player_bar_days", textClass: "lbl_value_player"
        }]
      });
    }


    //Coin In
    _maximo = 0;
    _maximo = obtenerMaximo(_lnd_played);
    if (_lnd_played.length > 0) {
      if (_lnd_played[0] < 0) clase = "player_bar_negative"; else clase = "player_bar_positive";
      $('.graf_bar_terminal_coinin1').multiprogressbar({
        parts: [{
          value: Math.round((_lnd_played[0] * 100) / _maximo), text: FormatString(_lnd_played[0], EN_FORMATS.F_MONEY), barClass: clase, textClass: "lbl_value_player"
        },
        {
          value: Math.round(((_maximo - _lnd_played[0]) * 100) / _maximo), text: '', barClass: "player_bar_days", textClass: "lbl_value_task"
        }]
      });
    }
    if (_lnd_played.length > 1) {
      if (_lnd_played[1] < 0) clase = "player_bar_negative"; else clase = "player_bar_positive";
      $('.graf_bar_terminal_coinin2').multiprogressbar({
        parts: [{
          value: Math.round((_lnd_played[1] * 100) / _maximo), text: FormatString(_lnd_played[1], EN_FORMATS.F_MONEY), barClass: clase, textClass: "lbl_value_player"
        },
        {
          value: Math.round(((_maximo - _lnd_played[1]) * 100) / _maximo), text: '', barClass: "player_bar_days", textClass: "lbl_value_player"
        }]
      });
    }
    if (_lnd_played.length == 3) {
      if (_lnd_played[2] < 0) clase = "player_bar_negative"; else clase = "player_bar_positive";
      $('.graf_bar_terminal_coinin3').multiprogressbar({
        parts: [{
          value: Math.round((_lnd_played[2] * 100) / _maximo), text: FormatString(_lnd_played[2], EN_FORMATS.F_MONEY), barClass: clase, textClass: "lbl_value_player"
        },
        {
          value: Math.round(((_maximo - _lnd_played[2]) * 100) / _maximo), text: '', barClass: "player_bar_days", textClass: "lbl_value_player"
        }]
      });
    }

    //Won
    _maximo = 0;
    _maximo = obtenerMaximo(_lnd_won);
    if (_lnd_won.length > 0) {
      if (_lnd_won[0] < 0) clase = "player_bar_negative"; else clase = "player_bar_positive";
      $('.graf_bar_terminal_won1').multiprogressbar({
        parts: [{
          value: Math.round((_lnd_won[0] * 100) / _maximo), text: FormatString(_lnd_won[0], EN_FORMATS.F_MONEY), barClass: clase, textClass: "lbl_value_player"
        },
        {
          value: Math.round(((_maximo - _lnd_won[0]) * 100) / _maximo), text: '', barClass: "player_bar_days", textClass: "lbl_value_task"
        }]
      });
    }
    if (_lnd_won.length > 1) {
      if (_lnd_won[1] < 0) clase = "player_bar_negative"; else clase = "player_bar_positive";
      $('.graf_bar_terminal_won2').multiprogressbar({
        parts: [{
          value: Math.round((_lnd_won[1] * 100) / _maximo), text: FormatString(_lnd_won[1], EN_FORMATS.F_MONEY), barClass: clase, textClass: "lbl_value_player"
        },
        {
          value: Math.round(((_maximo - _lnd_won[1]) * 100) / _maximo), text: '', barClass: "player_bar_days", textClass: "lbl_value_player"
        }]
      });
    }
    if (_lnd_won.length == 3) {
      if (_lnd_won[2] < 0) clase = "player_bar_negative"; else clase = "player_bar_positive";
      $('.graf_bar_terminal_won3').multiprogressbar({
        parts: [{
          value: Math.round((_lnd_won[2] * 100) / _maximo), text: FormatString(_lnd_won[2], EN_FORMATS.F_MONEY), barClass: clase, textClass: "lbl_value_player"
        },
        {
          value: Math.round(((_maximo - _lnd_won[2]) * 100) / _maximo), text: '', barClass: "player_bar_days", textClass: "lbl_value_player"
        }]
      });

    }

    //Played
    _maximo = 0;
    _maximo = obtenerMaximo(_lnd_plays_count);
    if (_lnd_plays_count.length > 0) {
      if (_lnd_plays_count[0] < 0) clase = "player_bar_negative"; else clase = "player_bar_positive";
      $('.graf_bar_terminal_played1').multiprogressbar({
        parts: [{
          value: Math.round((_lnd_plays_count[0] * 100) / _maximo), text: _lnd_plays_count[0], barClass: clase, textClass: "lbl_value_player"
        },
        {
          value: Math.round(((_maximo - _lnd_plays_count[0]) * 100) / _maximo), text: '', barClass: "player_bar_days", textClass: "lbl_value_task"
        }]
      });
    }
    if (_lnd_plays_count.length > 1) {
      if (_lnd_plays_count[1] < 0) clase = "player_bar_negative"; else clase = "player_bar_positive";
      $('.graf_bar_terminal_played2').multiprogressbar({
        parts: [{
          value: Math.round((_lnd_plays_count[1] * 100) / _maximo), text: _lnd_plays_count[1], barClass: clase, textClass: "lbl_value_player"
        },
        {
          value: Math.round(((_maximo - _lnd_plays_count[1]) * 100) / _maximo), text: '', barClass: "player_bar_days", textClass: "lbl_value_player"
        }]
      });
    }
    if (_lnd_plays_count.length == 3) {
      if (_lnd_plays_count[2] < 0) clase = "player_bar_negative"; else clase = "player_bar_positive";
      $('.graf_bar_terminal_played3').multiprogressbar({
        parts: [{
          value: Math.round((_lnd_plays_count[2] * 100) / _maximo), text: _lnd_plays_count[2], barClass: clase, textClass: "lbl_value_player"
        },
        {
          value: Math.round(((_maximo - _lnd_plays_count[2]) * 100) / _maximo), text: '', barClass: "player_bar_days", textClass: "lbl_value_player"
        }]
      });
    }
  }, OnLoadTerminalHistoryFailed);
}

function OnLoadTerminalHistoryFailed() {

}

function OnLoadInfoPlayerDataFailed() {

}
function OnLoadInfoTerminalDataFailed() {

}

function TerminalInfoPanelSectionAlarmsFill(AlarmsList, TasksList, LastTasks) {

  $("#info_panel_active_alarms").empty();
  $("#info_panel_last_alarms").empty();

  var _str_html = "";

  _str_html += "<div id='infopanel_alarm_acc'>";

  for (_idx_info_panel_alarms_alarms in AlarmsList) {
    var _item = AlarmsList[_idx_info_panel_alarms_alarms];

    _str_html += "<h3>" + $(_Language.parse($("<div>" + _Legends.Dictionary_Alarms[_item.type][_item.id] + "</div>")[0])).text() + "</h3>";
    _str_html += "<div>";
    _str_html += "<p>" + _Language.get('NLS_NO_TASKS_ASSIGNED') + ": " + _str_severity + "</p>";
    _str_html += "</div>";
  }

  for (_idx_info_panel_alarms_tasks in TasksList) {
    var _item = TasksList[_idx_info_panel_alarms_tasks];

    _str_html += "<h3>" + $(_Language.parse($("<div>" + _Legends.Dictionary_Alarms[_item.category][_item.subcategory] + "</div>")[0])).text();

    switch (_item.status) {
      case 1:
        _str_html += '<span class="infoAlarmasStatus alarmaPendiente" data-nls="NLS_STATE_PENDING">' + _Language.get('NLS_STATE_PENDING') + '</span>';
        break;

      case 2:
        _str_html += '<span class="infoAlarmasStatus alarmaCurso" data-nls="NLS_STATE_IN_PROGRESS">' + _Language.get('NLS_STATE_IN_PROGRESS') + '</span>';
        break;

      case 3:
        _str_html += '<span class="infoAlarmasStatus alarmaResuelta" data-nls="NLS_STATE_SOLVED">' + _Language.get('NLS_STATE_SOLVED') + '</span>';
        break;

      case 4:
        _str_html += '<span class="infoAlarmasStatus alarmaEscalada" data-nls="NLS_STATE_SCALED">' + _Language.get('NLS_STATE_SCALED') + '</span>';
        break;

      case 5:
        _str_html += '<span class="infoAlarmasStatus alarmaValidada" data-nls="NLS_STATE_VALIDATED">' + _Language.get('NLS_STATE_VALIDATED') + '</span>';
        break;
    }

    _str_html += "</h3>";
    _str_html += "<div>";

    var _str_severity = "";

    switch (_item.severity) {
      case 1:
        _str_severity = _Language.get('NLS_PRIORITY_HIGH');
        break;

      case 2:
        _str_severity = _Language.get('NLS_PRIORITY_MEDIUM');
        break;

      case 3:
        _str_severity = _Language.get('NLS_PRIORITY_LOW');
        break;
    }

    _str_html += "<p>" + _Language.get('NLS_SEVERITY') + ":</p><span>" + _str_severity + "</span>";


    if (_item.assigned_role_id <= 0) {
      _str_html += "<p>" + _Language.get('NLS_ASSIGNED_TO') + ":</p><span>" + (!_Manager.TaskManager.Users[_item.assigned_user_id] ? "&nbsp;" : _Manager.TaskManager.Users[_item.assigned_user_id]) + "</span>";
    } else {
      var _str_role = "";

      if ((_item.assigned_role_id & 1) == 1) {
        _str_role += _Language.get('NLS_ROLE_PCA');
      }

      if ((_item.assigned_role_id & 2) == 2) {
        if (_str_role != "") {
          _str_role += ",";
        }
        _str_role += _Language.get('NLS_ROLE_OPM');
      }

      if ((_item.assigned_role_id & 4) == 4) {
        if (_str_role != "") {
          _str_role += ",";
        }
        _str_role += _Language.get('NLS_ROLE_SLA');
      }

      if ((_item.assigned_role_id & 8) == 8) {
        if (_str_role != "") {
          _str_role += ",";
        }
        _str_role += _Language.get('NLS_ROLE_TSP');
      }

      _str_html += "<p>" + _Language.get('NLS_ASSIGNED_TO') + ":</p><span>" + _str_role + "</span>";
    }

    if (_item.elapsed_time == " ") {
      _str_html += "<p>" + _Language.get('NLS_COLUMN_LENGTH') + ":</p><span>Más de 48 horas.</span>";
    } else {
      _str_html += "<p>" + _Language.get('NLS_COLUMN_LENGTH') + ":</p><span>" + _item.elapsed_time + "</span>";
    }

    _str_html += "</div>";
  }

  _str_html += "</div>";

  if (_str_html != "") {
    $("#info_panel_active_alarms").html(_str_html);
    $("#infopanel_alarm_acc").accordion({
      heightStyle: "content", collapsible: true, autoHeight:false
    });

    // Styling
    var $acc_header = $("#infopanel_alarm_acc").find(".ui-accordion-header");
    var $acc_content = $("#infopanel_alarm_acc").find(".ui-accordion-content");

    $acc_header.addClass('infoAlarmsHeader');
    $acc_content.addClass('infoAlarmsContent');

    $acc_header.click(function () {
      if ($(this).hasClass("ui-state-active")) {
        $(this).addClass("infoAlarmsHeaderActive");
      } else {
        $(this).removeClass("infoAlarmsHeaderActive");
      }
    });

  }

  _str_html = "";

  _str_html += "<div id='infopanel_lastalarms_acc'>";

  //
  for (_idx_info_panel_alarms_last_tasks in LastTasks) {
    var _item = LastTasks[_idx_info_panel_alarms_last_tasks];

    _str_html += "<h3>" + $(_Language.parse($("<div>" + _Legends.Dictionary_Alarms[_item.category][_item.subcategory] + "</div>")[0])).text();

    switch (_item.status) {
      case 1:
        _str_html += '<span class="infoAlarmasStatus alarmaPendiente" data-nls="NLS_STATE_PENDING">' + _Language.get('NLS_STATE_PENDING') + '</span>';
        break;

      case 2:
        _str_html += '<span class="infoAlarmasStatus alarmaCurso" data-nls="NLS_STATE_IN_PROGRESS">' + _Language.get('NLS_STATE_IN_PROGRESS') + '</span>';
        break;

      case 3:
        _str_html += '<span class="infoAlarmasStatus alarmaResuelta" data-nls="NLS_STATE_SOLVED">' + _Language.get('NLS_STATE_SOLVED') + '</span>';
        break;

      case 4:
        _str_html += '<span class="infoAlarmasStatus alarmaEscalada" data-nls="NLS_STATE_SCALED">' + _Language.get('NLS_STATE_SCALED') + '</span>';
        break;

      case 5:
        _str_html += '<span class="infoAlarmasStatus alarmaValidada" data-nls="NLS_STATE_VALIDATED">' + _Language.get('NLS_STATE_VALIDATED') + '</span>';
        break;
    }

    _str_html += "</h3>";
    _str_html += "<div>";

    var _str_severity = "";

    switch (_item.severity) {
      case 1:
        _str_severity = _Language.get('NLS_PRIORITY_HIGH');
        break;

      case 2:
        _str_severity = _Language.get('NLS_PRIORITY_MEDIUM');
        break;

      case 3:
        _str_severity = _Language.get('NLS_PRIORITY_LOW');
        break;
    }

    _str_html += "<p>" + _Language.get('NLS_SEVERITY') + ":</p><span>" + _str_severity + "</span>";


    if (_item.assigned_role_id <= 0) {
      _str_html += "<p>" + _Language.get('NLS_ASSIGNED_TO') + ":</p><span>" + (!_Manager.TaskManager.Users[_item.assigned_user_id] ? "&nbsp;" : _Manager.TaskManager.Users[_item.assigned_user_id]) + "</span>";
    } else {
      var _str_role = "";

      if ((_item.assigned_role_id & 1) == 1) {
        _str_role += _Language.get('NLS_ROLE_PCA');
      }

      if ((_item.assigned_role_id & 2) == 2) {
        if (_str_role != "") {
          _str_role += ",";
        }
        _str_role += _Language.get('NLS_ROLE_OPM');
      }

      if ((_item.assigned_role_id & 4) == 4) {
        if (_str_role != "") {
          _str_role += ",";
        }
        _str_role += _Language.get('NLS_ROLE_SLA');
      }

      if ((_item.assigned_role_id & 8) == 8) {
        if (_str_role != "") {
          _str_role += ",";
        }
        _str_role += _Language.get('NLS_ROLE_TSP');
      }

      _str_html += "<p>" + _Language.get('NLS_ASSIGNED_TO') + ":</p><span>" + _str_role + "</span>";
    }

    var dateFormat;
    if (_Configuration.Language == 0)
      dateFormat = 'D/M/YYYY h:mm:ss A';
    else
      dateFormat = 'M/D/YYYY h:mm:ss A';

    _str_html += "<p>" + _Language.get('NLS_COLUMN_CREATED') + ":</p><span>" + moment(_item.creation).format(dateFormat) + "</span>";
    _str_html += "</div>";
  }

  _str_html += "</div>";

  if (_str_html != "") {
    $("#info_panel_last_alarms").html(_str_html);
    $("#infopanel_lastalarms_acc").accordion({
      heightStyle: "content", collapsible: true
    });

    // Styling
    var $acc_header = $("#infopanel_lastalarms_acc").find(".ui-accordion-header");
    var $acc_content = $("#infopanel_lastalarms_acc").find(".ui-accordion-content");

    $acc_header.addClass('infoAlarmsHeader');
    $acc_content.addClass('infoAlarmsContent');

    $acc_header.click(function () {
      if ($(this).hasClass("ui-state-active")) {
        $(this).addClass("infoAlarmsHeaderActive");
      } else {
        $(this).removeClass("infoAlarmsHeaderActive");
      }
    });
  }
}

function TerminalInfoPanelSectionRunnersFill(CurrentTasks, LastTasks, Runner) {

  $("#info_panel_active_runner").empty();
  $("#info_panel_last_runner").empty();

  var _str_html = "";

  _str_html += "<div id='infopanel_alarm_acc'>";

  for (_idx_info_panel_alarms_tasks in CurrentTasks) {
    var _item = CurrentTasks[_idx_info_panel_alarms_tasks];

    _str_html += "<h3>" + $(_Language.parse($("<div>" + _Legends.Dictionary_Alarms[_item[3]][_item[4]] + "</div>")[0])).text();

    switch (+_item[1]) {
      case 1:
        _str_html += '<span class="infoAlarmasStatus alarmaPendiente" data-nls="NLS_STATE_PENDING">' + _Language.get('NLS_STATE_PENDING') + '</span>';
        break;

      case 2:
        _str_html += '<span class="infoAlarmasStatus alarmaCurso" data-nls="NLS_STATE_IN_PROGRESS">' + _Language.get('NLS_STATE_IN_PROGRESS') + '</span>';
        break;

      case 3:
        _str_html += '<span class="infoAlarmasStatus alarmaResuelta" data-nls="NLS_STATE_SOLVED">' + _Language.get('NLS_STATE_SOLVED') + '</span>';
        break;

      case 4:
        _str_html += '<span class="infoAlarmasStatus alarmaEscalada" data-nls="NLS_STATE_SCALED">' + _Language.get('NLS_STATE_SCALED') + '</span>';
        break;

      case 5:
        _str_html += '<span class="infoAlarmasStatus alarmaValidada" data-nls="NLS_STATE_VALIDATED">' + _Language.get('NLS_STATE_VALIDATED') + '</span>';
        break;
    }

    _str_html += "</h3>";
    _str_html += "<div>";

    var _str_severity = "";

    switch (+_item[6]) {
      case 1:
        _str_severity = _Language.get('NLS_PRIORITY_HIGH');
        break;

      case 2:
        _str_severity = _Language.get('NLS_PRIORITY_MEDIUM');
        break;

      case 3:
        _str_severity = _Language.get('NLS_PRIORITY_LOW');
        break;
    }

    _str_html += "<p>" + _Language.get('NLS_SEVERITY') + ":</p><span>" + _str_severity + "</span>";
    //_str_html += "<p>" + _Language.get('NLS_ASSIGNED_TO') + "</p>:<span>" + _Manager.TaskManager.Users[RunnerId] + "</span>";

    if (1 == 1) {
      _str_html += "<p>" + _Language.get('NLS_COLUMN_LENGTH') + ":</p><span>" + _Language.get('NLS_N_HOURS') + "</span>";
    } else {
      _str_html += "<p>" + _Language.get('NLS_COLUMN_LENGTH') + ":</p><span>" + _item.elapsed_time + "</span>";
    }

    _str_html += "</div>";
  }

  _str_html += "</div>";

  if (_str_html != "") {
    $("#info_panel_active_runner").html(_str_html);
    $("#infopanel_alarm_acc").accordion({
      heightStyle: "content", collapsible: true
    });

    // Styling
    var $acc_header = $("#infopanel_alarm_acc").find(".ui-accordion-header");
    var $acc_content = $("#infopanel_alarm_acc").find(".ui-accordion-content");

    $acc_header.addClass('infoAlarmsHeader');
    $acc_content.addClass('infoAlarmsContent');

    $acc_header.click(function () {
      if ($(this).hasClass("ui-state-active")) {
        $(this).addClass("infoAlarmsHeaderActive");
      } else {
        $(this).removeClass("infoAlarmsHeaderActive");
      }
    });

  }

  _str_html = "";

  _str_html += "<div id='infopanel_lastalarms_acc_run'>";

  //
  for (_idx_info_panel_alarms_last_tasks in LastTasks) {
    var _item = LastTasks[_idx_info_panel_alarms_last_tasks];

    _str_html += "<h3>" + $(_Language.parse($("<div>" + _Legends.Dictionary_Alarms[_item[3]][_item[4]] + "</div>")[0])).text();

    switch (+_item[1]) {
      case 1:
        _str_html += '<span class="infoAlarmasStatus alarmaPendiente" data-nls="NLS_STATE_PENDING">' + _Language.get('NLS_STATE_PENDING') + '</span>';
        break;

      case 2:
        _str_html += '<span class="infoAlarmasStatus alarmaCurso" data-nls="NLS_STATE_IN_PROGRESS">' + _Language.get('NLS_STATE_IN_PROGRESS') + '</span>';
        break;

      case 3:
        _str_html += '<span class="infoAlarmasStatus alarmaResuelta" data-nls="NLS_STATE_SOLVED">' + _Language.get('NLS_STATE_SOLVED') + '</span>';
        break;

      case 4:
        _str_html += '<span class="infoAlarmasStatus alarmaEscalada" data-nls="NLS_STATE_SCALED">' + _Language.get('NLS_STATE_SCALED') + '</span>';
        break;

      case 5:
        _str_html += '<span class="infoAlarmasStatus alarmaValidada" data-nls="NLS_STATE_VALIDATED">' + _Language.get('NLS_STATE_VALIDATED') + '</span>';
        break;
    }

    _str_html += "</h3>";
    _str_html += "<div>";

    var _str_severity = "";

    switch (+_item[6]) {
      case 1:
        _str_severity = _Language.get('NLS_PRIORITY_HIGH');
        break;

      case 2:
        _str_severity = _Language.get('NLS_PRIORITY_MEDIUM');
        break;

      case 3:
        _str_severity = _Language.get('NLS_PRIORITY_LOW');
        break;
    }

    _str_html += "<p>" + _Language.get('NLS_SEVERITY') + ":</p><span>" + _str_severity + "</span>";


    switch (+_item[1]) {
      case 1:
        _str_html += "<p>" + _Language.get('NLS_RUNNER_INFO_ASSIGNED') + "</p><span>" + _item[5] + "</span>";
        break;

      case 2:
        _str_html += "<p>" + _Language.get('NLS_RUNNER_INFO_INPROGRESS') + "</p><span>" + _item[5] + "</span>";
        break;

      case 3:
        _str_html += "<p>" + _Language.get('NLS_RUNNER_INFO_SOLVED') + "</p><span>" + _item[5] + "</span>";
        break;

      case 4:
        _str_html += "<p>" + _Language.get('NLS_RUNNER_INFO_SCALED') + "</p><span>" + _item[5] + "</span>";
        break;
    }

    //_str_html += "<p>" + _Language.get('NLS_ASSIGNED_TO') + ":</p><span>" + _Manager.TaskManager.Users[RunnerId] + "</span>";
    //_str_html += "<p>" + _Language.get('NLS_COLUMN_CREATED') + ":</p><span>" + _item[5] + "</span>";
    _str_html += "</div>";
  }

  _str_html += "</div>";


  if (_str_html != "") {
    $("#info_panel_last_runner").html(_str_html);
    $("#infopanel_lastalarms_acc_run").accordion({
      heightStyle: "content", collapsible: true
    });

    // Styling
    var $acc_header = $("#infopanel_lastalarms_acc_run").find(".ui-accordion-header");
    var $acc_content = $("#infopanel_lastalarms_acc_run").find(".ui-accordion-content");

    $acc_header.addClass('infoAlarmsHeader');
    $acc_content.addClass('infoAlarmsContent');

    $acc_header.click(function () {
      if ($(this).hasClass("ui-state-active")) {
        $(this).addClass("infoAlarmsHeaderActive");
      } else {
        $(this).removeClass("infoAlarmsHeaderActive");
      }
    });
  }
}


function RemoveSnapshotClient(AccountId) {
  var _i;
  var _exists;

  _exists = false;
  _i = 0;

  while (!_exists && _i < m_shapshots_client_array.length) {
    if (m_shapshots_client_array[_i].account_id == $("#snapshot_" + AccountId + "_account_id").text()) {
      _exists = true;
      m_shapshots_client_array.splice(_i, 1);
    }
    _i++;
  }

  $("#snapshot_" + AccountId + "_client").remove();

}   //RemoveSnapshotClient

function RemoveSnapshotTerminal(ObjectId) {
  var _i;
  var _exists;

  _exists = false;
  _i = 0;
  while (!_exists && _i < m_shapshots_terminal_array.length) {
    if (m_shapshots_terminal_array[_i].object_id == ObjectId) {
      _exists = true;
      m_shapshots_terminal_array.splice(_i, 1);
    }
    _i++;
  }

  $("#snapshot_" + ObjectId + "_terminal").remove();

}   //RemoveSnapshotTerminal


function doSnapshot_Terminal(Div) {
  var _new_div;
  var _new_div_img;
  var _html;
  var _terminal_id;
  var _object_id;
  var _account_id;
  var _exists;
  var _i;
  var _snapshot_div_title;

  _snapshot_div_title = true;
  _terminal_id = $("#popup_terminal_id").text();
  _object_id = $("#popup_object_id").text();
  _account_id = $("#popup_account_id").text();
  _exists = false;
  _i = 0;

  if (!_terminal_id) {
    return;
  }

  _html = "";

  _html += $("#popup_div_principal_table_title").html();
  _html += $("#popup_div_principal_table_info_terminal").html();

  while (!_exists && _i < m_shapshots_terminal_array.length) {
    _exists = (m_shapshots_terminal_array[_i].terminal_id == _terminal_id);
    _i++;
  }

  if (!_exists) {
    m_shapshots_terminal_array.push(new SnapshotTerminal(_terminal_id, _object_id));

    // replace all "popup_"
    while (_html.indexOf("popup_") >= 0) {
      _html = _html.replace("popup_", "snapshot_" + _object_id + "_");
    }
    _new_div = document.createElement("div");
    $(_new_div).html(_html);
    $(_new_div).attr("id", "snapshot_" + _object_id + "_terminal");
    $(_new_div).attr("style", "float:left");

    $("#div_snapshot_terminal_").append($(_new_div));
    $("tr[id*=tr_img_snapshot_" + _object_id + "_table_]").css("display", "none");

    _new_div_img = document.createElement("div");
    $(_new_div_img).html("<div style='width=100%'><div id='popup_img_trash_terminal" + _object_id + "' class='icoClose' onclick='RemoveSnapshotTerminal(\"" + _object_id + "\")' onmousedown='DivOnMouseDown(this);' onmouseup='DivOnMouseUp(this);'></div></div>");
    $(_new_div).append($(_new_div_img));

    // change class to change icon
    $(Div).removeClass("icoSnapshot");
    $(Div).addClass("icoSnapshot_undo");

  } // if (!_exists)
} // doSnapshot_Terminal

function undoSnapshot_Terminal(Div) {

  // change class to change icon
  $(Div).removeClass("icoSnapshot_undo");
  $(Div).addClass("icoSnapshot");

} // undoSnapshot_Client


function doSnapshot_Client(Div) {
  var _new_div;
  var _new_div_img;
  var _html;
  var _terminal_id;
  var _object_id;
  var _account_id;
  var _exists;
  var _i;
  var _snapshot_div_title = false;

  _terminal_id = $("#popup_terminal_id").text();
  _object_id = $("#popup_object_id").text();
  _account_id = $("#popup_account_id").text();
  _exists = false;
  _i = 0;

  if (!_account_id) {
    return;
  }

  _html = "";

  _html += $("#popup_div_principal_table_info_client").html();

  while (!_exists && _i < m_shapshots_client_array.length) {
    _exists = (_account_id != "" && m_shapshots_client_array[_i].account_id == _account_id);
    _i++;
  }

  if (!_exists) {

    m_shapshots_client_array.push(new SnapshotClient(_account_id, _object_id));

    //replace id 
    _html = _html.replace("popup_div_principal_table_historical_terminals", "snapshot_" + _account_id + "_div_principal_table_historical_terminals");

    //replace id
    _html = _html.replace("table_historical_terminals_popup", "table_historical_terminals_snapshot_" + _account_id);

    // replace all "popup_"
    while (_html.indexOf("popup_") >= 0) {
      _html = _html.replace("popup_", "snapshot_" + _account_id + "_");
    }
    _new_div = document.createElement("div");
    $(_new_div).html(_html);
    $(_new_div).attr("id", "snapshot_" + _account_id + "_client");
    $(_new_div).attr("style", "float:left");

    $("#div_snapshot_client_").append($(_new_div));
    $("table[id*=snapshot_" + _account_id + "_table]").addClass("table_report ui-state-default");
    $("tr[id*=tr_img_snapshot_" + _account_id + "_table_]").css("display", "none");
    _new_div_img = document.createElement("div");
    $(_new_div_img).html("<div style='width=100%'><div id='popup_img_trash_terminal" + _account_id + "' class='icoClose' onclick='RemoveSnapshotClient(\"" + _account_id + "\")' onmousedown='DivOnMouseDown(this);' onmouseup='DivOnMouseUp(this);'></div></div>");
    $(_new_div).append($(_new_div_img));

    // change class to change icon
    $(Div).removeClass("icoSnapshot");
    $(Div).addClass("icoSnapshot_undo");

  } // if (!_exists)
} // doSnapshot_Client


function SaveSnapshot(SnapshotType, Div) {
  var _actual_class;

  if ($(Div).hasClass("icoSnapshot")) {
    _actual_class = "icoSnapshot";
  }
  else if ($(Div).hasClass("icoSnapshot_undo")) {
    _actual_class = "icoSnapshot_undo";
  }

  try {
    switch (true) {
      case _actual_class == "icoSnapshot" && SnapshotType == "terminal":
        doSnapshot_Terminal(Div);
        break;

      case _actual_class == "icoSnapshot_undo" && SnapshotType == "terminal":
        RemoveSnapshotTerminal($("#popup_object_id").text());
        $(Div).removeClass("icoSnapshot_undo");
        $(Div).addClass("icoSnapshot");
        break;

      case _actual_class == "icoSnapshot" && SnapshotType == "client":
        doSnapshot_Client(Div);
        break;

      case _actual_class == "icoSnapshot_undo" && SnapshotType == "client":
        RemoveSnapshotClient($("#popup_account_id").text());
        $(Div).removeClass("icoSnapshot_undo");
        $(Div).addClass("icoSnapshot");
        break;

      default: break;
    } //switch
  }
  catch (err) {
    WSILogger.Log("SaveSnapshot()", err);
  }
}  //SaveSnapshot


function GetPlayerActivityComplete_Snapshot(result, userContext, methodName) {
  var _table_html;
  var _snapshot;
  var _account_id;
  var _terminal_id;
  var _result;
  var _result_json_terminals;
  var _result_json_bills;
  var _account_id;
  var _account_id_last;
  var _td;
  var _table_html_bills;
  var _tr_snapshot_info;

  _table_html = "";

  try {
    _result = JSON.parse(result);

    _result_json_terminals = _result[0];
    _result_json_bills = _result[1];

    _account_id_last = "";

    if (!_result_json_terminals) {
      return;
    }

    //if (_result_json_terminals.length > 0) {
    for (var _i_terminal = 0; _i_terminal < _result_json_terminals.length; _i_terminal++) {
      _terminal_id = _result_json_terminals[_i_terminal].Terminal[1];
      _account_id = _result_json_terminals[_i_terminal].Terminal[0];

      var _table_snapshot_historical_terminals;
      _table_snapshot_historical_terminals = $("#table_historical_terminals_snapshot_" + _account_id);
      if ($(_table_snapshot_historical_terminals).length == 0 && _account_id_last != _account_id) {
        _table_html = CreateTableHisoricalTerminals(_result, _account_id, "snapshot");
        var _popup_historical_terminals = $("#snapshot_" + _account_id + "_div_principal_table_historical_terminals");

        if ($(_popup_historical_terminals).length > 0) {
          $(_popup_historical_terminals).html(_table_html);
        }
      }
      else {

        // CreateNewHistoricalTerminalRow
        _tr_snapshot_info = $("#tr_snapshot_" + _terminal_id + "_" + _account_id + "_info");
        if ($(_tr_snapshot_info).length == 0) {
          var _table_snapshot_historical_terminals;

          _table_snapshot_historical_terminals = $("#table_historical_terminals_snapshot_" + _account_id);
          if ($(_table_snapshot_historical_terminals).length > 0) {
            var _tr = CreateNewHistoricalTerminalRow(_terminal_id, _account_id, _result_json_terminals, _result_json_bills, _i_terminal, "snapshot");
            $(_table_snapshot_historical_terminals).find("tr:first").after(_tr);
          }
        }
        else {
          _td = "#td_snapshot_" + _terminal_id + "_" + _account_id + "_terminal";
          if ($(_td).length > 0) {
            $(_td).text(_result_json_terminals[_i_terminal].Terminal[2]);
          }
          _td = "#td_snapshot_" + _terminal_id + "_" + _account_id + "_provider";
          if ($(_td).length > 0) {
            $(_td).text(_result_json_terminals[_i_terminal].Terminal[3]);
          }
          _td = "#td_snapshot_" + _terminal_id + "_" + _account_id + "_timeplayed";
          if ($(_td).length > 0) {
            $(_td).text(_result_json_terminals[_i_terminal].Terminal[4]);
          }
          _td = "#td_snapshot_" + _terminal_id + "_" + _account_id + "_totalplayed";
          if ($(_td).length > 0) {
            $(_td).text(_result_json_terminals[_i_terminal].Terminal[5]);
          }
          _td = "#td_snapshot_" + _terminal_id + "_" + _account_id + "_playedwon";
          if ($(_td).length > 0) {
            $(_td).text(_result_json_terminals[_i_terminal].Terminal[6]);
          }
          _td = "#td_snapshot_" + _terminal_id + "_" + _account_id + "_playedcount";
          if ($(_td).length > 0) {
            $(_td).text(_result_json_terminals[_i_terminal].Terminal[7]);
          }
          _td = "#td_snapshot_" + _terminal_id + "_" + _account_id + "_betaverage";
          if ($(_td).length > 0) {
            $(_td).text(_result_json_terminals[_i_terminal].Terminal[8]);
          }

          // Create Bills table
          if (_account_id != _account_id_last) {
            var _td_bill = $("#td_snapshot_" + _terminal_id + "_" + _account_id + "_bill");
            if ($(_td_bill).length > 0) {
              _table_html_bills = CreateTableHistoricalTerminals_TableBill(_terminal_id, _account_id, _result_json_bills, "snapshot");
              $(_td_bill).html(_table_html_bills);
            }

            _account_id_last = _account_id;
          }
        } //else
      } //else
      _account_id_last = _account_id;
    }  //for
    //} // if

  } catch (err) {
    WSILogger.Log("GetPlayerActivityComplete_Snapshot()", err);
  }

} //GetPlayerActivityComplete_Snapshot

function GetPlayerActivityFailed_Snapshot(result, userContext, methodName) {
  m_flag_is_necessary_actualize_all_historical_terminals_of_snapshots = true;
} //GetPlayerActivityFailed_Snapshot

function ActualizeTerminal(Array, Position) {
  var _idx;
  var _info_terminal;

  try {
    Array[Position].object_id = _Manager.Terminals.ByAccount[Array[Position].account_id];
    if (!Array[Position].object_id) {
      Array[Position].object_id = "";
    }
  } catch (err) {
    Array[Position].object_id = "";
  }
} // ActualizeTerminal


function UpdateAllClientSnapshots() {
  var _i;
  var _terminal_id;
  var _terminal;
  var _object_id;
  var _account_id_list;
  var _account_id_list2;
  var _snapshot;
  var _account_id;
  var _actual_terminal;

  if (!_Configuration.ShowRealTimeRefresh) {
    return;
  }

  _snapshot = "snapshot_";
  _account_id_list = ""; // actualize all historical terminals
  _account_id_list2 = ""; // actualize only the terminal whose has the active play session

  try {
    // client info
    if (m_selected_ribbon == CONST_RIBBON_SNAPSHOT_CUSTOMER) {
      for (_i = 0; _i < m_shapshots_client_array.length; _i++) {
        _account_id = m_shapshots_client_array[_i].account_id;
        _object_id = m_shapshots_client_array[_i].object_id;

        _actual_terminal = _Manager.Terminals.ByAccount[_account_id]; // Actual terminal where customer is playing
        if (!_actual_terminal) {
          _actual_terminal = "";
        }

        switch (true) {
          case m_shapshots_client_array[_i].all_historical_terminals_actualized == false: // historical terminals have never been actualized
            m_shapshots_client_array[_i].all_historical_terminals_actualized = true;
            _account_id_list += m_shapshots_client_array[_i].account_id + ",";
            ActualizeTerminal(m_shapshots_client_array, _i);
            break;

          case m_shapshots_client_array[_i].object_id != _actual_terminal: // there is another customer in the terminal (client has lived the terminal)
            ActualizeTerminal(m_shapshots_client_array, _i);
            _account_id_list += m_shapshots_client_array[_i].account_id + ",";
            break;

          case m_flag_is_necessary_actualize_all_historical_terminals_of_snapshots:
            _account_id_list += m_shapshots_client_array[_i].account_id + ",";
            break;

          default:
            _account_id_list2 += m_shapshots_client_array[_i].account_id + ",";
            break;
        }

        //loading
        /*
        _terminal_id = m_shapshots_client_array[_i].terminal_id;
        var _div_snapshot = $("#snapshot_" + _terminal_id + "_div_principal_table_historical_terminals");
      
        if ($(_div_snapshot).length > 0) {
        $(_div_snapshot).html("<div style='align:center; vertical-align:middle'><img src='images/loading.gif' style='width:5%; height:5%;' alt='' /></div>");
        }
        */
      }

      if (_account_id_list.length > 0) {
        _account_id_list = _account_id_list.substr(0, _account_id_list.length - 1);
        if (_Configuration.TestEnabled) {
          PageMethods.GetPlayerActivityTest(_account_id_list, false, GetPlayerActivityComplete_Snapshot, GetPlayerActivityFailed_Snapshot);
        }
        else {
          PageMethods.GetPlayerActivity(_account_id_list, false, GetPlayerActivityComplete_Snapshot, GetPlayerActivityFailed_Snapshot);
        }
      }
      if (_account_id_list2.length > 0) {
        _account_id_list2 = _account_id_list2.substr(0, _account_id_list2.length - 1);
        if (_Configuration.TestEnabled) {
          PageMethods.GetPlayerActivityTest(_account_id_list, true, GetPlayerActivityComplete_Snapshot, GetPlayerActivityFailed_Snapshot);
        }
        else {
          PageMethods.GetPlayerActivity(_account_id_list, true, GetPlayerActivityComplete_Snapshot, GetPlayerActivityFailed_Snapshot);
        }
      }

      m_flag_is_necessary_actualize_all_historical_terminals_of_snapshots = false;
    }

    // terminal info
    if (m_selected_ribbon == CONST_RIBBON_SNAPSHOT_TERMINAL) {
      for (_i = 0; _i < m_shapshots_terminal_array.length; _i++) {

        _terminal_id = m_shapshots_terminal_array[_i].terminal_id;
        _object_id = m_shapshots_terminal_array[_i].object_id;

        _terminal = _Manager.Terminals.GetTerminal(_object_id).Properties;

        $("#" + _snapshot + _object_id + "_play_session_duration").text(FormatSeconds(_terminal.play_session_duration));
        $("#" + _snapshot + _object_id + "_play_session_total_played").text(_terminal.play_session_total_played);
        $("#" + _snapshot + _object_id + "_play_session_bet_average").text(_terminal.play_session_bet_average);
        $("#" + _snapshot + _object_id + "_play_session_played_count").text(_terminal.play_session_played_count);
        $("#" + _snapshot + _object_id + "_play_session_won_amount").text(_terminal.play_session_won_amount);
        $("#" + _snapshot + _object_id + "_terminal_bet_average").text(_terminal.terminal_bet_average);
        $("#" + _snapshot + _object_id + "_played_amount").text(_terminal.played_amount);
        $("#" + _snapshot + _object_id + "_won_amount").text(_terminal.won_amount);

        // _net_win
        $("#" + _snapshot + _object_id + "_net_win").text(_terminal.net_win);
        if (_terminal.net_win && _terminal.net_win < 0) {
          $("#" + _snapshot + _object_id + "_net_win").removeClass("netwin_positive");
          $("#" + _snapshot + _object_id + "_net_win").addClass("netwin_negative");
        }
        else {
          $("#" + _snapshot + _object_id + "_net_win").removeClass("netwin_negative");
          $("#" + _snapshot + _object_id + "_net_win").addClass("netwin_positive");
        }

        // play_session_net_win
        $("#" + _snapshot + _object_id + "_play_session_net_win").text(_terminal.play_session_net_win);
        if (_terminal.play_session_net_win && _terminal.play_session_net_win < 0) {
          $("#" + _snapshot + _object_id + "_play_session_net_win").removeClass("netwin_positive");
          $("#" + _snapshot + _object_id + "_play_session_net_win").addClass("netwin_negative");
        }
        else {
          $("#" + _snapshot + _object_id + "_play_session_net_win").removeClass("netwin_negative");
          $("#" + _snapshot + _object_id + "_play_session_net_win").addClass("netwin_positive");
        }


        $("#" + _snapshot + _object_id + "_terminal_played_count").text(_terminal.played_count);

        //TODO
        // popup_XXXX (num_partidas)
      }
    }

  } catch (err) {
    WSILogger.Log("UpdateAllClientSnapshots()", err);
  }
}  //UpdateAllClientSnapshots



// Examples:
//      7290 seconds --> 02:01:30
//      20000 seconds --> 05:33:20
//      -5561 seconds --> -01:32:41
function FormatSeconds(Seconds) {
  var _hours_minutes_seconds;
  var _hours;
  var _minutes;
  var _seconds;
  var _is_negative;
  var _FORMAT = "HH:mm:ss";

  if (Seconds < 0) {
    _is_negative = true;
    Seconds = Math.abs(Seconds);
  }
  else {
    _is_negative = false;
  }

  try {
    _hours = Math.floor(Seconds / 3600);
    _minutes = Math.floor((Seconds / 60) - (_hours * 60));
    _seconds = Seconds - ((_hours * 3600) + (_minutes * 60));

    _hours = ("00" + _hours);
    _minutes = ("00" + _minutes).slice(-2);
    _seconds = ("00" + _seconds).slice(-2);

    if (_hours.length > 2) {
      _hours = _hours.substr(1, _hours.length - 1);
    }
    else {
      _hours = _hours.slice(-2);
    }
  }
  catch (err) {
  }
  _hours_minutes_seconds = _FORMAT.replace("HH", _hours).replace("mm", _minutes).replace("ss", _seconds);
  if (_is_negative) {
    _hours_minutes_seconds = "-" + _hours_minutes_seconds;
  }
  return _hours_minutes_seconds;
}  //FormatSeconds


function UpdatePopupAlarms(TerminalDiv) {
  var _min_rows_alarms = 20;
  var _object_id;
  var _div_alarms;
  var _html;
  var _label;

  _object_id = null;
  _div_alarms = null;

  if ($("#popup_object_id").length == 0) {
    WSILogger.Debug("UpdatePopupAlarms()", "error", "popup_object_id not exists");

    return;

  }
  if ($("#popup_div_alarms").length == 0) {
    WSILogger.Debug("UpdatePopupAlarms()", "error", "popup_div_alarms not exists");

    return;
  }

  _div_alarms = $("#popup_div_alarms");

  _label = new WSILabel();
  _label.Update(TerminalDiv.Properties, 0);

  if (!_label.hasError) {

    $(_div_alarms).empty();
    return;
  }

  _object_id = $("#popup_object_id").text();

  _html = "";
  _html += '<table id="popup_table_3" class="table_alarms">';
  _html += '<tr class="ui-widget-header">';
  _html += '<td>' + _Language.get("NLS_ALARMS") + '</td>';
  _html += '</tr>';

  if (_Manager.Terminals.Items[_object_id].Properties.al_sas_host_error > 0) {
    _html += '<tr>';
    _html += '<td>' + _Language.get("NLS_AL_SASHOST") + '</td>';
    _html += '<td>' + "" + '</td>';
    _html += '</tr>';
  }

  if (_Manager.Terminals.Items[_object_id].Properties.al_door_flags > 0) {
    _html += '<tr>';
    _html += '<td>' + _Language.get("NLS_AL_DOOR") + '</td>';
    _html += '</tr>';
  }

  if (_Manager.Terminals.Items[_object_id].Properties.al_bill_flags > 0) {
    _html += '<tr>';
    _html += '<td>' + _Language.get("NLS_AL_BILL") + '</td>';
    _html += '</tr>';
  }

  if (_Manager.Terminals.Items[_object_id].Properties.al_printer_flags > 0) {
    _html += '<tr>';
    _html += '<td>' + _Language.get("NLS_AL_PRINTER") + '</td>';
    _html += '</tr>';
  }

  if (_Manager.Terminals.Items[_object_id].Properties.al_egm_flags > 0) {
    _html += '<tr>';
    _html += '<td>' + _Language.get("NLS_AL_EGM") + '</td>';
    _html += '</tr>';
  }

  if (_Manager.Terminals.Items[_object_id].Properties.al_played_won_flags > 0) {
    _html += '<tr>';
    //_html += '<td>' + _Language.get("") + '</td>';
    _html += '<td>' + "PLAYED WON" + '</td>';
    _html += '</tr>';
  }

  if (_Manager.Terminals.Items[_object_id].Properties.al_jackpot_flags > 0) {
    _html += '<tr>';
    //_html += '<td>' + _Language.get("") + '</td>';
    _html += '<td>' + "JACKPOT" + '</td>';
    _html += '</tr>';
  }

  if (_Manager.Terminals.Items[_object_id].Properties.al_stacker_status > 0) {
    _html += '<tr>';
    //_html += '<td>' + _Language.get("") + '</td>';
    _html += '<td>' + "STACKER" + '</td>';
    _html += '</tr>';
  }
  if (_Manager.Terminals.Items[_object_id].Properties.al_call_attendant_flags > 0) {
    _html += '<tr>';
    _html += '<td>' + _Language.get("NLS_AL_ATTENDANT") + '</td>';
    _html += '</tr>';
  }
  if (_Manager.Terminals.Items[_object_id].Properties.al_machine_flags > 0) {
    _html += '<tr>';
    //_html += '<td>' + _Language.get("") + '</td>';
    _html += '<td>' + "MACHINE" + '</td>';
    _html += '</tr>';
  }
  if (_Manager.Terminals.Items[_object_id].Properties.al_big_won > 0) {
    _html += '<tr>';
    //_html += '<td>' + _Language.get("") + '</td>';
    _html += '<td>' + "BIG WON" + '</td>';
    _html += '</tr>';
  }

  _html += '</table>';

  $(_div_alarms).html(_html);

}   //UpdatePopupAlarms


// TEST_MODE
function SetTestMode(checkbox) {
  _Configuration.TestEnabled = checkbox.checked;
  m_config_has_changed = true;
}

function DisableMonitorMode() {
  $("#tabs").css("visibility", "visible");
  $("#divSldZoom").css("top", (_LayoutMode == 2) ? $("#tabs").height() + 10 : 10);
  $("#divCloseMonitorMode").css("display", "none");

  if (_LayoutMode == 2) {
    SetDivBackgroundToInitialPosition();
  }

}  // CloseMonitorMode

function EnableMonitorMode() {
  $("#tabs").css("visibility", "hidden");
  $("#divSldZoom").css("top", (_LayoutMode == 2) ? 10 : -180);
  $("#divCloseMonitorMode")
      .css("display", "inline")
    .css("top", "-1px");

  if (_LayoutMode == 2) {
    SetDivBackgroundToInitialPosition();
    var _offset = $("#tabs").css("height").replace("px", "");
    m_current_background_position_top -= (_offset / m_current_zoom);
    DivPosition_Move($("#div_background"), m_current_background_position_left, m_current_background_position_top, m_current_zoom, CONST_TRANSFORM_ORIGIN);
  }

} // OpenMonitorMode

//////////////////////////////////////////////////////////////////////////////

//function Heatmap_GetData(Width, Height) {

//  try {
//    var activeFilter = m_current_filter[m_current_section];

//    if (!activeFilter) { return { max: 100, data: [{ x: -50, y: -50, count: 0}] }; }

//    // Update heatmap legend colors
//    _Manager.HeatMapControl.set("gradient", _Legends.GetLegendColors(activeFilter));
//    _Manager.HeatMapControl.initColorPalette();

//    if (typeof Width === "string") {
//      Width = Width.replace("px", "");
//      Height = Height.replace("px", "");

//      Width = +Width;
//      Height = +Height;
//    }

//    var obj = {};
//    obj.max = 100;
//    obj.data = [];

//    for (var _idx in _Manager.Terminals.Items) {
//      var _terminal = _Manager.Terminals.Items[_idx];

//      if (_LayoutMode == 1) {
//        // 3D
//        var _rx = ((_terminal.Properties.lop_x) * Width) / _Manager.BackPlane.userData.width;
//        var _ry = ((_terminal.Properties.lop_z) * Height) / _Manager.BackPlane.userData.height;

//        var _o_x = Math.round(Width / 2);
//        var _o_z = Math.round(Height / 2);

//        var _x = _o_x + _rx;
//        var _z = _o_z + _ry;

//      } else {
//        // TODO_2D: check for coordinates!!!
//        //            var _div_center_left = +$("#div_center").css("left").replace("px", "");
//        //            var _div_center_top = +$("#div_center").css("top").replace("px", "");

//        //            var _x = _div_center_left + (_terminal.Properties.lop_x * 10) - 10;
//        //            var _z = _div_center_top + (_terminal.Properties.lop_z * 10) - 10;

//        var _o_x = Math.round(Width / 2);
//        var _o_z = Math.round(Height / 2);

//        //var _x = _o_x + (_terminal.Properties.lop_x * 10) - 10;
//        //var _z = _o_z + (_terminal.Properties.lop_z * 10) - 10;
//        var _x = _o_x + (_terminal.Properties.lop_x * 5) + 5;
//        var _z = _o_z + (_terminal.Properties.lop_z * 5) + 5;
//      }

//      var _value = -1;

//      var _legend_values = _Legends.Check(m_current_filter[m_current_section], _terminal.Properties);

//      if (_legend_values) {
//        _value = _legend_values.value;
//      }

//      if (_value > 0) {
//        obj.data.push({ x: _x, y: _z, count: _value });
//      }
//    }

//    // WSILogger.Debug("HEATMAP DATA", "obj", obj);

//    return obj;

//  } catch (_ex) {
//    WSILogger.Log("Heatmap_GetData", _ex);
//    return { max: 100, data: [{ x: -50, y: -50, count: 0}] };
//  }
//}

//////////////////////////////////////////////////////////////////////////////
function DivOnMouseDown(Div) {
  $(Div).css("background-color", "Gray");
}  // DivOnMouseDown

//////////////////////////////////////////////////////////////////////////////

function DivOnMouseUp(Div) {
  $(Div).css("background-color", "Transparent");
} // DivOnMouseUp

//////////////////////////////////////////////////////////////////////////////

function GetPropertyName_Transform() {
  var _transform;

  switch (m_browser) {
    case "Chrome":
    case "Opera":
    case "Safari":
      _transform = "-webkit-transform"
      break;


      //IE and Firefox                                                                                                            
    default:
      _transform = "transform";
  }

  return _transform;
} // GetPropertyName_Transform

//////////////////////////////////////////////////////////////////////////////

function GetPropertyName_TransformOrigin() {
  var _transform_origin;

  switch (m_browser) {
    case "Chrome":
    case "Opera":
    case "Safari":
      _transform_origin = "-webkit-transform-origin"
      break;


      //IE and Firefox                                                                                                             
    default:
      _transform_origin = "transform-origin";
  }

  return _transform_origin;
} // GetPropertyName_TransformOrigin

//////////////////////////////////////////////////////////////////////////////

function CoordToPosition(Origin, Translation) {
  if (!Translation) {
    Translation = 0;
  }
  return Math.round((+Origin + +Translation + 5) / 5);
}

function PositionToCoord(Position) {
  return (+Position * 5) - 5;
}

//////////////////////////////////////////////////////////////////////////////

function UpdateTransform(Target, Editor) {
  var _angle = $("#" + Target).attr("rotate_value");
  var _translate = $("#" + Target).attr("translate_value");
  var _property = GetPropertyName_Transform();
  if (!Editor) {
    $("#" + Target).css(_property, "rotate(" + _angle + ")");
  } else {
    $("#" + Target).css(_property, "translate(" + _translate + ") rotate(" + _angle + ")");
  }
}

//////////////////////////////////////////////////////////////////////////////

function browserDetect() {

  var nAgt = navigator.userAgent;
  var verOffset, ix;
  // IE is default
  m_browser = navigator.appName;

  // Opera
  if ((verOffset = nAgt.indexOf("Opera")) != -1) {
    m_browser = "Opera";
  }
    //  Chrome
  else if ((verOffset = nAgt.indexOf("Chrome")) != -1) {
    m_browser = "Chrome";
  }
    //  Safari
  else if ((verOffset = nAgt.indexOf("Safari")) != -1) {
    m_browser = "Safari";
  }
    // Firefox
  else if ((verOffset = nAgt.indexOf("Firefox")) != -1) {
    m_browser = "Firefox";
  }

}

function SaveConfigurationDB(relocate) {
  var ShowActivity;           // Boolean
  var Ranges;                 // Int32
  var ShowAlarms;             // Boolean
  var ShowImage;              // Boolean
  var ShowGrid;               // Boolean
  var ShowAreas;              // Boolean
  var ShowBanks;              // Boolean
  var Language;               // String
  var RealTimeRefresh;        // Boolean
  var TestEnabled;            // Boolean
  var RealTimeRefreshSeconds; // Int32
  var ShowHeatmap;            // Boolean
  var ShowGamingTables;       // Boolean
  var TerminalAppearance3d;   // Int32

  if (!m_config_has_changed) {
    return;
  }

  //LC_SHOW_ACTIVITY AS SHOW_ACTIVITY
  _Configuration.ShowActivity = $("#imgChkActivity").attr("Activity") == "on";

  //Realtime seconds
  _Configuration.RealTimeRefreshSeconds = $("#textActivityRefresh").attr("value");

  //Show heatmap
  //    ShowHeatmap = m_heatmap_enabled; //  $("#chkHeatmap").is(":checked");

  //LC_TERMINAL_APPEARANCE_3D
  //_Configuration.TerminalAppearance3D = $("#chkModels").is(":checked") ? 0 : 1;

  //    ShowGamingTables = true;
  _Configuration.ShowGamingTables = true;

  //    PageMethods.SaveLayoutConfig(JSON.stringify(_Configuration)
  //                               , SaveLayoutConfigCompleted
  //                               , SaveLayoutConfigFailed);

  var _Dashboard = _Manager.WidgetManager.GetDashboard();

  //PageMethods.SaveLayoutConfig(JSON.stringify(_Configuration)
  //                           , function (result, userContext, methodName) {
  //                             return SaveLayoutConfigCompleted(result, userContext, methodName, relocate);
  //                           },
  //                 SaveLayoutConfigFailed);


  if(_Dashboard.length > 1 ){

      PageMethods.SaveLayoutConfig(JSON.stringify(_Configuration), JSON.stringify(_Dashboard)
                       , function (result, userContext, methodName) {
                            return SaveLayoutConfigCompleted(result, userContext, methodName, relocate);
                         }, SaveLayoutConfigFailed);
  }

}

function SaveLayoutConfigCompleted(result, userContext, methodName, relocate) {
  m_config_has_changed = false;

  if (console && console.log) {
    console.log("Configuration Saved!");
  }

  if (relocate) {
    location.href = "./Home.aspx";
  }
}

function SaveLayoutConfigFailed(error, userContext, methodName) {
  WSILogger.Debug("SaveLayoutConfigFailed()", "error", error);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

function CloneObject(obj) {
  if (Object.prototype.toString.call(obj) === '[object Array]') {
    var clone = [];
    for (var i = 0; i < obj.length; i++)
      clone[i] = CloneObject(obj[i]);

    return clone;
  }
  else if (typeof (obj) == "object") {
    var clone = {
    };
    for (var prop in obj)
      if (obj.hasOwnProperty(prop))
        clone[prop] = CloneObject(obj[prop]);

    return clone;
  }
  else
    return obj;
}

///////////////////////////////////////////////////////////////////////////////////////////

function DateToday() {
  var _tmp = new Date();
  return _tmp.getFullYear() + "/" + (_tmp.getMonth() + 1) + "/" + _tmp.getDate();
}

function DateTimeToday() {
  var _tmp = new Date();
  return _tmp.getFullYear() + "/" + (_tmp.getMonth() + 1) + "/" + _tmp.getDate() + " " + _tmp.getHours() + ":" + _tmp.getMinutes() + ":" + _tmp.getSeconds();
}

function FormatDate(Date) {
  if (typeof Date === "string") {
    return Date;
  }
  if (!Date) {
    Date = new Date();
  }
  return Date.getFullYear() + "/" + (Date.getMonth() + 1) + "/" + Date.getDate();
}

///////////////////////////////////////////////////////////////////////////////////////////

function ToggleCheckState(Control, Enabled) {
  $(Control).attr("src", (Enabled) ? "images/switch_on.png" : "images/switch_off.png");
}

///////////////////////////////////////////////////////////////////////////////////////////

function SetBackground(firstTime) {
  if (!firstTime) {
    _Configuration.ShowBackground = !_Configuration.ShowBackground;
  }
  switch (_LayoutMode) {
    case 1: // 3D
      _Manager.BackPlaneVisible = _Configuration.ShowBackground;
      _Manager.UpdateBackPlane();
      break;
    case 2: // 2D
      $("#img_background").css("visibility", (_Configuration.ShowBackground) ? "visible" : "hidden");
      break;
  }
  m_config_has_changed = !firstTime;
} // SetBackground

//function SetHeatMap(firstTime) {
//    if (!firstTime) {
//        _Configuration.ShowHeatMap = !_Configuration.ShowHeatMap;
//    }

//    m_config_has_changed = !firstTime;
//} // SetHeatMap


////////////////////////////////////////////////////////////////////////////////////////////

function hex2rgb(col) {
  var r, g, b;
  if (col.charAt(0) == '#') {
    col = col.substr(1);
  }
  r = col.charAt(0) + col.charAt(1);
  g = col.charAt(2) + col.charAt(3);
  b = col.charAt(4) + col.charAt(5);
  r = parseInt(r, 16);
  g = parseInt(g, 16);
  b = parseInt(b, 16);
  return [r, g, b];
} // hex2rgb

////////////////////////////////////////////////////////////////////////////////////////////

function GetColorContrast(RGBColor) {
  return Math.round(((parseInt(RGBColor[0]) * 299) + (parseInt(RGBColor[1]) * 587) + (parseInt(RGBColor[2]) * 114)) / 1000);
}

////////////////////////////////////////////////////////////////////////////////////////////

function colourNameToHex(colour) {
  var colours = {
    "aliceblue": "#f0f8ff", "antiquewhite": "#faebd7", "aqua": "#00ffff", "aquamarine": "#7fffd4", "azure": "#f0ffff",
    "beige": "#f5f5dc", "bisque": "#ffe4c4", "black": "#000000", "blanchedalmond": "#ffebcd", "blue": "#0000ff", "blueviolet": "#8a2be2", "brown": "#a52a2a", "burlywood": "#deb887",
    "cadetblue": "#5f9ea0", "chartreuse": "#7fff00", "chocolate": "#d2691e", "coral": "#ff7f50", "cornflowerblue": "#6495ed", "cornsilk": "#fff8dc", "crimson": "#dc143c", "cyan": "#00ffff",
    "darkblue": "#00008b", "darkcyan": "#008b8b", "darkgoldenrod": "#b8860b", "darkgray": "#a9a9a9", "darkgreen": "#006400", "darkkhaki": "#bdb76b", "darkmagenta": "#8b008b", "darkolivegreen": "#556b2f",
    "darkorange": "#ff8c00", "darkorchid": "#9932cc", "darkred": "#8b0000", "darksalmon": "#e9967a", "darkseagreen": "#8fbc8f", "darkslateblue": "#483d8b", "darkslategray": "#2f4f4f", "darkturquoise": "#00ced1",
    "darkviolet": "#9400d3", "deeppink": "#ff1493", "deepskyblue": "#00bfff", "dimgray": "#696969", "dodgerblue": "#1e90ff",
    "firebrick": "#b22222", "floralwhite": "#fffaf0", "forestgreen": "#228b22", "fuchsia": "#ff00ff",
    "gainsboro": "#dcdcdc", "ghostwhite": "#f8f8ff", "gold": "#ffd700", "goldenrod": "#daa520", "gray": "#808080", "green": "#008000", "greenyellow": "#adff2f",
    "honeydew": "#f0fff0", "hotpink": "#ff69b4",
    "indianred ": "#cd5c5c", "indigo": "#4b0082", "ivory": "#fffff0", "khaki": "#f0e68c",
    "lavender": "#e6e6fa", "lavenderblush": "#fff0f5", "lawngreen": "#7cfc00", "lemonchiffon": "#fffacd", "lightblue": "#add8e6", "lightcoral": "#f08080", "lightcyan": "#e0ffff", "lightgoldenrodyellow": "#fafad2",
    "lightgrey": "#d3d3d3", "lightgreen": "#90ee90", "lightpink": "#ffb6c1", "lightsalmon": "#ffa07a", "lightseagreen": "#20b2aa", "lightskyblue": "#87cefa", "lightslategray": "#778899", "lightsteelblue": "#b0c4de",
    "lightyellow": "#ffffe0", "lime": "#00ff00", "limegreen": "#32cd32", "linen": "#faf0e6",
    "magenta": "#ff00ff", "maroon": "#800000", "mediumaquamarine": "#66cdaa", "mediumblue": "#0000cd", "mediumorchid": "#ba55d3", "mediumpurple": "#9370d8", "mediumseagreen": "#3cb371", "mediumslateblue": "#7b68ee",
    "mediumspringgreen": "#00fa9a", "mediumturquoise": "#48d1cc", "mediumvioletred": "#c71585", "midnightblue": "#191970", "mintcream": "#f5fffa", "mistyrose": "#ffe4e1", "moccasin": "#ffe4b5",
    "navajowhite": "#ffdead", "navy": "#000080",
    "oldlace": "#fdf5e6", "olive": "#808000", "olivedrab": "#6b8e23", "orange": "#ffa500", "orangered": "#ff4500", "orchid": "#da70d6",
    "palegoldenrod": "#eee8aa", "palegreen": "#98fb98", "paleturquoise": "#afeeee", "palevioletred": "#d87093", "papayawhip": "#ffefd5", "peachpuff": "#ffdab9", "peru": "#cd853f", "pink": "#ffc0cb", "plum": "#dda0dd", "powderblue": "#b0e0e6", "purple": "#800080",
    "red": "#ff0000", "rosybrown": "#bc8f8f", "royalblue": "#4169e1",
    "saddlebrown": "#8b4513", "salmon": "#fa8072", "sandybrown": "#f4a460", "seagreen": "#2e8b57", "seashell": "#fff5ee", "sienna": "#a0522d", "silver": "#c0c0c0", "skyblue": "#87ceeb", "slateblue": "#6a5acd", "slategray": "#708090", "snow": "#fffafa", "springgreen": "#00ff7f", "steelblue": "#4682b4",
    "tan": "#d2b48c", "teal": "#008080", "thistle": "#d8bfd8", "tomato": "#ff6347", "turquoise": "#40e0d0",
    "violet": "#ee82ee",
    "wheat": "#f5deb3", "white": "#ffffff", "whitesmoke": "#f5f5f5",
    "yellow": "#ffff00", "yellowgreen": "#9acd32"
  };

  if (typeof colours[colour.toLowerCase()] != 'undefined')
    return colours[colour.toLowerCase()];

  return colour;
}

///////////////////////////////////////////////////////////////////////////////////////////

function SetGrid(firstTime) {
  if (!firstTime) {
    _Configuration.ShowGrid = !_Configuration.ShowGrid;
  }
  switch (_LayoutMode) {
    case 1: // 3D
      //_Manager.Grid.visible = _Configuration.ShowGrid;
      _Manager.Grid.material.visible = _Configuration.ShowGrid;
      break;
    case 2: // 2D
      $('.gridlines').css("visibility", (_Configuration.ShowGrid) ? "visible" : "hidden");
      break;
  }
  m_config_has_changed = !firstTime;
} // SetGrid

///////////////////////////////////////////////////////////////////////////////////////////

function SetAreas(firstTime) {
  if (!firstTime) {
    _Configuration.ShowAreas = !_Configuration.ShowAreas;
  }
  switch (_LayoutMode) {
    case 1: // 3D
      WSI.SetAreas(_Configuration.ShowAreas);
      break;
    case 2: // 2D
      for (var _i = 0; _i < m_areas_positions.length; _i++) {
        $("#" + CONST_ID_AREA + _i).css("visibility", (_Configuration.ShowAreas) ? "visible" : "hidden");
      }
      break;
  }
  m_config_has_changed = !firstTime;
} // SetAreas

///////////////////////////////////////////////////////////////////////////////////////////

function SetLanguage(Sender, firstTime) {
  if (!firstTime) {
    _Language.CurrentLanguage = $(Sender).val();
  }
  _Language.parse();
  m_config_has_changed = !firstTime;
  _Configuration.Language = _Language.Languages[_Language.CurrentLanguage].id;

  if (Sender != null) {
    $(Sender).val(_Language.CurrentLanguage);
  }
}

///////////////////////////////////////////////////////////////////////////////////////////

function SetIslands(firstTime) {
  if (!firstTime) {
    _Configuration.ShowBanks = !_Configuration.ShowBanks;
  }
  switch (_LayoutMode) {
    case 1: // 3D
      WSI.SetIslands(_Configuration.ShowBanks);
      break;
    case 2: // 2D
      for (var _i = 0; _i < m_islands_positions.length; _i++) {
        $("#" + CONST_ID_ISLAND + _i).css("visibility", (_Configuration.ShowBanks) ? "visible" : "hidden");
      }
      break;
  }
  m_config_has_changed = !firstTime;
} // SetIslands

///////////////////////////////////////////////////////////////////////////////////////////

function SetHeatmap(firstTime) {
  var _targetId = event.currentTarget.id;

  if (_targetId == "ON60300") {
    if (!firstTime) {
      _Configuration.ShowHeatMapPlayer = !_Configuration.ShowHeatMapPlayer;
    }
    var _hmconf = _Configuration.ShowHeatMapPlayer;
  }
  else {
    if (!firstTime) {
      _Configuration.ShowHeatMap = !_Configuration.ShowHeatMap;
    }
    var _hmconf = _Configuration.ShowHeatMap;
  }

  _Manager.HeatMap.SetVisibility(_hmconf);

  $(document).find('#' + _targetId).prop("checked", _hmconf);

  m_config_has_changed = !firstTime;
} // SetHeatmap

function SetRunnersmap(firstTime) {
  if (!firstTime) {
    _Configuration.ShowRunnersMap = !_Configuration.ShowRunnersMap;
  }

  _Manager.RunnersMap.SetVisibility(_Configuration.ShowRunnersMap);

  var _targetId = event.currentTarget.id;
  $(document).find('[onclick*="SetRunnersmap();"]').prop("checked", _Configuration.ShowRunnersMap);

  m_config_has_changed = !firstTime;
} // SetHeatmap

// Remove childLegendSelected CSS class from the current section.
function ClearSectionFilters(SectionID, CleanAll) {

  switch (SectionID.toString()) {
    case Enum.EN_SECTION_ID.MACHINES:
      $(".childLegend[id ^= 'childlegend_70']").removeClass("childLegendSelected");
      m_terminals_filters_selected_count = 0;
      break;

    case Enum.EN_SECTION_ID.MONITOR:
      $(".childLegend[id ^= 'leyenda_item_80']").removeClass("childLegendSelected");
      m_terminals_filters_selected_count = 0;
      break;

    case Enum.EN_SECTION_ID.PLAYERS:
      $(".childLegend[id ^= 'childlegend_60']").removeClass("childLegendSelected");
      m_players_filters_selected_count = 0;
      break;

    case Enum.EN_SECTION_ID.ALARMS_PLAYERS:
    case Enum.EN_SECTION_ID.ALARMS_MACHINES:
    case Enum.EN_SECTION_ID.ALARMS:
      $(".childLegend[id ^= 'childlegend_41']").removeClass("childLegendSelected");
      $(".childLegend[id ^= 'childlegend_42']").removeClass("childLegendSelected");
      m_players_filters_selected_count = 0;
      break;

    default:
      break;
  }

}

function SetCurrentLegend(LegendId, SectionID, FirstTime) {
  var _changed = (LegendId != m_current_filter[SectionID]);

  if (m_current_section == 40) {
    if ($("#div_legend_container_41001").find("[data-id='" + LegendId + "']").length > 0) {
      $("#div_legend_container_42002").accordion("option", "active", false);
      $("#div_legend_container_40000").accordion("option", "active", false);
      m_section_alarms_all = false;
    } else if ($("#div_legend_container_42002").find("[data-id='" + LegendId + "']").length > 0) {
      $("#div_legend_container_41001").accordion("option", "active", false);
      $("#div_legend_container_40000").accordion("option", "active", false);
      m_section_alarms_all = false;
    } else {
      $("#div_legend_container_41001").accordion("option", "active", false);
      $("#div_legend_container_42002").accordion("option", "active", false);
      m_section_alarms_all = true;
    }
  }

  if (!m_filter_mode_enabled && _changed  /*|| FirstTime*/) {

    m_current_filter[SectionID] = LegendId;

    // Set heatmap state
    var _prop_id = _Legends.Items[LegendId].Property,
        _state = (SectionID != 80 && SectionID != 40) ? m_field_properties[_prop_id].type != 1 : false;
    _UI.controlSetState($("[data-nls=NLS_FLOOR_TEMP_MAP]").parent(), _state);

    if (SectionID == 60) {
      var _st = !_state && _Configuration.ShowHeatMapPlayer;
      $(document).find('#ON60300').prop("checked", _st);
    }
    if (SectionID == 70) {
      var _st = !_state && _Configuration.ShowHeatMap;
      $(document).find('#ON70005').prop("checked", _st);
    }

    //RefreshLegendDetailDialog(LegendId);
    UpdateTerminals(); // RMS: Moved to respond after accordion activation

    // SRL: Moved because it don't work before Terminals Update
    _Manager.HeatMap.SetVisibility(_st);
  }

  m_config_has_changed = _changed;

  if (m_current_section == 80) {
    RefreshListLegendDetailDialog();
  } else {
    RefreshLegendDetailDialog(m_current_filter[m_current_section]);
  }
}
///////////////////////////////////////////////////////////////////////////////////////////

function ApplyDefaultValues() {
  //    // chkShowAlarms
  //    _AlarmList.enabled = _Configuration.ShowActivity; //m_alarms_enabled;
  //    SetAlarms($("#chkShowAlarms"), true);

  //chkSetBackground
  // SetBackground($("#chkSetBackground"), true);

  //chkShowGrid
  SetGrid($("#chkShowGrid"), true);

  switch (_LayoutMode) {
    case 1: // 3D
      // Areas and banks are loaded after
      break;
    case 2: // 2D
      //chkShowAreas
      SetAreas($("#chkShowAreas"), true);
      //chkShowIslands
      SetIslands($("#chkShowIslands"), true);
      break;
  }

  //chkRealTimeSnapshots
  SetHistoricalInRealTimeChecks(_Configuration.ShowRealTimeRefresh);
  //    if ($("#chkRealTimeSnapshots").prop("checked")) {
  //        m_historical_in_real_time = true;
  //    }
  //    else {
  //        m_historical_in_real_time = false;
  //    }

  //chkTestMode
  if ($("#chkTestMode").prop("checked")) {
    _Configuration.TestEnabled = true;
  }
  else {
    _Configuration.TestEnabled = false;
  }

  // Except for editor
  if (_LayoutMode != 3) {

    ////SetActivity 
    //if ($("#imgChkActivity").attr("Activity") == "on") {
    //  SetActivity("on", true)
    //}
    //else {
    //  SetActivity("off", true)
    //}

    // Set Heatmap
    //SetHeatmap("#chkHeatmap", true);

  }

  //LC_LANGUAGE AS LANG
  //SetLanguage($("#radLanguageSelector"), true);

  m_last_time_updated = 0;

} // ApplyDefaultValues


//function DefaultValues(Config) {

//  if (Config.length == 0) {
//    $("#chkTestMode").prop("checked", true);
//    $("#imgChkActivity")
//            .attr("src", "images/switch_on.png")
//            .attr("Activity", "on");
//    SaveConfigurationDB();

//    return;
//  }

//  //LC_SHOW_ACTIVITY AS SHOW_ACTIVITY
//  $("#imgChkActivity")
//            .attr("src", Config[0].Configuration[0] == "True" ? "images/switch_on.png" : "images/switch_off.png")
//            .attr("Activity", Config[0].Configuration[0] == "True" ? "on" : "off");

//  //LC_RANGES AS RANGES
//  switch (Config[0].Configuration[1]) {
//    case "0": // ocupation
//      $("#chkByUse").prop("checked", true);
//      break;
//    case "1": // level
//      $("#chkByLevel").prop("checked", true)
//      break;
//    case "2": // age
//      $("#chkByAge").prop("checked", true)
//      break;
//    case "3": // % Bet average
//    default:
//      $("#chkByBetAverage").prop("checked", true)
//      break;

//  }

//  //LC_SHOW_ALARMS AS SHOW_ALARMS
//  _Configuration.ShowActivity = (Config[0].Configuration[2] == "True") ? true : false;

//  //LC_SHOW_IMAGE AS SHOW_IMAGE
//  _Configuration.ShowBackground = (Config[0].Configuration[3] == "True") ? true : false;

//  //LC_SHOW_GRID AS SHOW_GRID
//  _Configuration.ShowGrid = (Config[0].Configuration[4] == "True") ? true : false;

//  //LC_SHOW_AREAS AS SHOW_AREAS
//  _Configuration.ShowAreas = (Config[0].Configuration[5] == "True") ? true : false;

//  //LC_SHOW_BANKS AS SHOW_BANKS
//  _Configuration.ShowBanks = (Config[0].Configuration[6] == "True") ? true : false;

//  //LC_LANGUAGE AS LANG
//  _Language.CurrentLanguage = _Language.getLanguageIdByName(Config[0].Configuration[7]);

//  //LC_REAL_TIME_REFRESH AS REAL_TIME_REFRESH
//  _Configuration.ShowRealTimeRefresh = (Config[0].Configuration[8] == "True") ? true : false;

//  //    if (Config[0].Configuration[8] == "True") {
//  //        SetHistoricalInRealTimeChecks(true);
//  //    }
//  //    else {
//  //        SetHistoricalInRealTimeChecks(false);
//  //    }

//  //LC_TEST_ENABLED AS TEST_ENABLED
//  if (Config[0].Configuration[9] == "True") {
//    $("#chkTestMode").prop("checked", true);
//  }
//  else {
//    $("#chkTestMode").prop("checked", false);
//  }

//  //Realtime seconds
//  var _value = Config[0].Configuration[10];
//  if (_value && _value < 5 || _value > 999) {
//    _value = 5;
//  }
//  $("#textActivityRefresh").attr("value", _value);

//  WSIData.ActivityInterval = _value * 1000;

//  //Show heatmap
//  _Configuration.ShowHeatMap = Config[0].Configuration[11] == "True" ? true : false;

//  // -----------------------------------------------------------------------
//  // ------- TODO : Review why HeatMap don't save configuration ------------
//  _Configuration.ShowHeatMap = false;

//  _Configuration.TerminalAppearance3D = Config[0].Configuration[0];
//  WSIData.ModelTypes = _Configuration.TerminalAppearance3D;

//  // -----------------------------------------------------------------------

//  //    if (Config[0].Configuration[11] == "True") {
//  //        $("#chkHeatmap").prop("checked", true);
//  //    }
//  //    else {
//  //        $("#chkHeatmap").prop("checked", false);
//  //    }

//  // LC_TERMINAL_APPEARANCE_3D
//  WSIData.ModelTypes = Config[0].Configuration[12]; //0=Models ; 1=Cubes
//  $("#chkCubes").prop("checked", WSIData.ModelTypes == 1);
//  $("#chkModels").prop("checked", WSIData.ModelTypes == 0);
//}  // DefaultValues

/////////////////////////////////////////////////////////////////////////

function SetToolTip(ObjectName, ToolTipText) {
  $('#' + ObjectName).attr('title', ToolTipText);
  $('#' + ObjectName).tooltip();
}

/////////////////////////////////////////////////////////////////////////

function addEvent(elem, type, eventHandle) {
  if (elem == null || typeof (elem) == 'undefined') return;
  if (elem.addEventListener) {
    elem.addEventListener(type, eventHandle, false);
  } else if (elem.attachEvent) {
    elem.attachEvent("on" + type, eventHandle);
  } else {
    elem["on" + type] = eventHandle;
  }
}

/////////////////////////////////////////////////////////////////////////

function GenerateExcel(Data) {
  //application / vnd.openxmlformats - officedocument.spreadsheetml.sheet
  window.open('data:application/vnd.ms-excel;base64,' + $.base64.encode(Data));
} // GenerateExcel

/////////////////////////////////////////////////////////////////////////

function SetActivityOn(FirstTime) {
  WSIData.Activity = true;

  FirstTime = (!FirstTime) ? false : FirstTime;

  WSILogger.Write('Setting activity (ON) ' + ((FirstTime) ? 'for first time.' : ''));

  if (FirstTime) {
    activator();
  } else {
    m_activity_timeout = setTimeout(activator, WSIData.ActivityInterval);
  }

} // SetActivityOn

function SetActivityOff() {
  // Clear activity simulator process
  //clearInterval(m_activity_timeout);

  WSILogger.Write('Setting activity (OFF)');

  clearTimeout(m_activity_timeout);
  m_activity_timeout = null;

  switch (_LayoutMode) {
    case 0: // Charts
      break;
    case 1: // 3D
      //Update Labels and terminals to default;
      UpdateLabels();
      RestoreTerminals();
      break;
    case 2: // 2D
      RestoreTerminals();
      break;
  }

  WSIData.Activity = false;

} // SetActivityOff

// Starts or Ends the random activity process
function SetActivity(Status, FirstTime) {

  FirstTime = (!FirstTime) ? false : FirstTime;

  switch (Status) {
    case "on":
    case "ON":
      WSIData.Activity = true;
      if (!m_activity_timeout) {
        SetActivityOn(FirstTime);
      }
      break;

    case "off":
    case "OFF":
      WSIData.Activity = false;
      if (m_activity_timeout) {
        SetActivityOff();
      }
      break;

    default:
      WSILogger.Write('Setting [' + Status + '] status activity - UNKNOWN');
      WSIData.Activity = (!m_activity_timeout || m_activity_timeout == null);
      if (!m_activity_timeout || m_activity_timeout == null) {
        SetActivityOn(FirstTime);
      } else {
        SetActivityOff();
      }
      break;
  }

  if (_LayoutMode != 0) {
    // Not in charts mode
    $("#DivLegend").css('visibility', WSIData.Activity ? 'inherit' : 'hidden');
    $("#imgChkActivity")
    .attr("src", WSIData.Activity ? "images/switch_on.png" : "images/switch_off.png")
        .attr("Activity", WSIData.Activity ? "on" : "off");
  }

  m_config_has_changed = true;
}

/////////////////////////////////////////////////////////////////////////

function parseCurrency(Value) {

  //WSILogger.Write("Parsing currency: " + Value + " -> Type: " + typeof Value);

  var _coma = 0;
  var _dot = 0;
  var _parsed = null;

  if (typeof Value != "string") {
    Value = "" + Value;
  }

  _coma = Value.indexOf(',');
  _dot = Value.indexOf('.');
  if (_coma < _dot) {
    _parsed = +Number(Value.replace(/[^0-9-\.]+/g, ""));
  } else {
    _parsed = +Number(Value.replace('.', '').replace(',', '.').replace(/[^0-9-\.]+/g, ""));
  }

  //WSILogger.Write("Parsed to: " + _parsed + " -> Type: " + typeof _parsed);

  return _parsed;
}

/////////////////////////////////////////////////////////////////////////

function RadionButtonSelectedValueSet(name, SelectdValue) {
  $('input[name="' + name + '"][value="' + SelectdValue + '"]').prop('checked', true);
}

/////////////////////////////////////////////////////////////////////////

function OpenInfoPanel() {
  var _id = null;
  _id = m_terminal_id_selected;

  // Hide info panels
  $(".infoMaquina").hide();
  $(".infoJugador").hide();
  $(".panelDerivar").hide();
  $(".infoMonitor").hide();
  $(".panelInfoAlarma").hide();
  $(".panelModificarEstado").hide();
  $(".infoAlarmas.infoPanelAlarmas").hide();
  $(".panelEnviarMensaje").hide();
  $(".infoRunner").hide();

  // Perform action
  switch (m_current_section) {
    case 10: // Dashboard
      ocultarPanelDerecha();
      ocultarPanelDerechaExtendido();

      // Check for terminal selected
      if (_id != null) {
        // The update terminals process, update the content of the panel

        // Obtain account id on the selected terminal
        _id = _id.toString();
        m_account_id_selected = _Manager.Terminals.Items[_id.replace('terminal_', '')].Properties.player_account_id;
        if (m_account_id_selected == '' || m_account_id_selected == '0') {
          m_account_id_selected = null;
        }

        // If there is an account, show the right panel
        if (m_account_id_selected != null) {
          $(".btnJugadorComparar").removeClass("compararJugInfo");

          if ($.inArray(m_account_id_selected, m_cat_compare_list) != -1) {
            $(".btnJugadorComparar").removeClass("compararJugInfo");
            $(".btnJugadorComparar").addClass("selCompararJug");
            $(".btnJugadorComparar").find("a").attr("data-nls", "NLS_RETIRE_COMPARE_PLAYER");
          } else {
            $(".btnJugadorComparar").removeClass("selCompararJug");
            $(".btnJugadorComparar").addClass("compararJugInfo");
            $(".btnJugadorComparar").find("a").attr("data-nls", "NLS_COMPARE_PLAYER");
          }

          _Language.parse($(".btnJugadorComparar")[0]);

          $(".infoJugador").show();
          //animaPanelDerecha();
          mostrarPanelDerecha();
          $(".infoRunner").hide();
        }
      }

      break;

    case 70: // Floor
      // Check for terminal selected
      if (_id != null) {
        // The update terminals process, update the content of the panel

        if (_LayoutMode == 2) {
          // In 2D convert the div id to a object id
          _id = _id.replace("terminal_", "");
        }

        // If there is an account, show the right panel
        var _terminal_ext_id = _Manager.Terminals.Items[_id].Properties.lo_external_id;
        var _idx = $.inArray(_terminal_ext_id, m_tar_compare_list);

        if (_idx == -1) {
          // Add to compare
          $(".btnTerminalComparar").removeClass("selCompararTerminal");

          // switch nls
          $(".btnTerminalComparar").find("a").attr("data-nls", "NLS_COMPARE_TERMINAL");

        } else {
          // Retire from compare
          $(".btnTerminalComparar").addClass("selCompararTerminal");

          // switch nls
          $(".btnTerminalComparar").find("a").attr("data-nls", "NLS_RETIRE_COMPARE_TERMINAL");

        }

        _Language.parse($(".btnTerminalComparar")[0]);

        // Show the right panel
        $(".infoMaquina").show();
        //animaPanelDerecha();
        mostrarPanelDerecha();
      }
      break;

    case 30: // Tasks
      ocultarPanelDerecha();
      ocultarPanelDerechaExtendido();
      break;

    case 40: // Alarms
      // Check for terminal selected
      if (_id != null) {
        // Show the right panel
        $(".infoAlarmas").show();
        //animaPanelDerecha();
        mostrarPanelDerecha();
      }
      break;

    case 50: // Charts
      ocultarPanelDerecha();
      ocultarPanelDerechaExtendido();
      break;

    case 60: // Players
      // Check for terminal selected
      if (_id != null) {
        // The update terminals process, update the content of the panel

        // Obtain account id on the selected terminal
        _id = _id.toString();
        m_account_id_selected = _Manager.Terminals.Items[_id.replace('terminal_', '')].Properties.player_account_id;
        if (m_account_id_selected == '' || m_account_id_selected == '0') {
          m_account_id_selected = null;
        }

        // If there is an account, show the right panel
        if (m_account_id_selected != null) {
          $(".btnJugadorComparar").removeClass("compararJugInfo");

          if ($.inArray(m_account_id_selected, m_cat_compare_list) != -1) {
            $(".btnJugadorComparar").removeClass("compararJugInfo");
            $(".btnJugadorComparar").addClass("selCompararJug");
            $(".btnJugadorComparar").find("a").attr("data-nls", "NLS_RETIRE_COMPARE_PLAYER");
          } else {
            $(".btnJugadorComparar").removeClass("selCompararJug");
            $(".btnJugadorComparar").addClass("compararJugInfo");
            $(".btnJugadorComparar").find("a").attr("data-nls", "NLS_COMPARE_PLAYER");
          }

          _Language.parse($(".btnJugadorComparar")[0]);
          
          $(".infoJugador").show();
          //animaPanelDerecha();
          mostrarPanelDerecha();
          m_runner_id_selected = null;
        }
      }
      break;

    case 80: // Monitor

      // Check for terminal selected
      if (_id != null) {
        // Show the right panel
        $(".infoMonitor").show();
        mostrarPanelDerecha();
      }
      break;
      //ocultarPanelDerecha();
      //break;

    default:
      //
      ocultarPanelDerecha();
      ocultarPanelDerechaExtendido();
      break;
  }

  if (m_runner_id_selected != null) {

    $(".infoRunner").show();
    $(".infoAlarmas").hide();
    mostrarPanelDerecha();
    m_runner_id_selected = null;
  }
}

/////////////////////////////////////////////////////////////////////////////


function PreLoader() {
  var opts = {
    lines: 13, // The number of lines to draw
    length: 20, // The length of each line
    width: 13, // The line thickness
    radius: 60, // The radius of the inner circle
    scale: 0.5, //
    corners: 1, // Corner roundness (0..1)
    rotate: 0, // The rotation offset
    direction: 1, // 1: clockwise, -1: counterclockwise
    color: '#EDEDED', // #rgb or #rrggbb or array of colors
    speed: 1.4, // Rounds per second
    trail: 100, // Afterglow percentage
    shadow: true, // Whether to render a shadow
    hwaccel: true, // Whether to use hardware acceleration
    // className: 'spinner', // The CSS class to assign to the spinner
    zIndex: 2e9, // The z-index (defaults to 2000000000)
    top: '80%', // Top position relative to parent
    left: '50%' // Left position relative to parent
  };
  var target = document.getElementById('div_loader');
  var spinner = new Spinner(opts).spin(target);

}

/////////////////////////////////////////////////////////////////////////////

function GotoLogin() {
  m_from_logic = true;
  window.location = "login.aspx";
}

function CheckBirthDate(BirthDate) {
  /// <summary>
  /// Return true if today is player's birthday.
  /// </summary>
  /// <param name="BirthDate"></param>
  var _birth_date = moment(BirthDate, 'MM-DD-YYYY');
  var _today = moment();
  var _birth_date_day = _birth_date.date();

  if (_birth_date_day == 29 && _birth_date.month() == 2) {
    if (moment().isLeapYear()) {
      _birth_date_day -= 1;
    }
  }

  return (_birth_date_day == _today.date() && _birth_date.month() == _today.month());
}

function getRandomColor() {
  var _letters = '0123456789ABCDEF'.split('');
  var _color = '#';

  for (var i = 0; i < 6; i++) {
    _color += _letters[Math.floor(Math.random() * 16)];
  }

  return '#3366ff';
}

//Get the distance between two points using Pythagoras Theorem
function GetDistancePoints(OriginX, OriginY, DestinyX, DestinyY) {

  var _diff_x, _diff_y;

  _diff_x = DestinyX - OriginX;
  _diff_y = DestinyY - OriginY;


  return Math.sqrt(Math.pow(_diff_x, 2) + Math.pow(_diff_y, 2));
}
/////////////////////////////////////////////////////////////////////////////

function SwapClassByValue(value, Id, ClassGreen, ClassRed) {
  if (value >= 0) {
    if (!$(Id).hasClass(ClassGreen)) {
      $(Id).addClass(ClassGreen);
      $(Id).removeClass(ClassRed);
    }
  } else {
    if (!$(Id).hasClass(ClassRed)) {
      $(Id).addClass(ClassRed);
      $(Id).removeClass(ClassGreen);
    }
  }
}



function OnRunnerClick(RunnerId) {

  try {

    var _info;
    var _popup_div_principal;
    var _runner;

    //_runner = _Manager.Terminals.GetTerminal(TerminalDiv.replace("#" + CONST_ID_TERMINAL, ""));
    _Manager.RunnersMap.RunnerSelected = RunnerId;
    _Manager.RunnersMap.ForcedUpdate();

    for (_idx_runner in _Manager.Runners.Items) {

      if (_Manager.Runners.Items[_idx_runner].id == RunnerId) {
        _runner = _Manager.Runners.Items[_idx_runner];

        break;
      }
    }


    if (_runner) {
      UpdatePopupRunner(_runner);
    }

  } catch (_ex) {
    WSILogger.Log("OnRunnerClick()", _ex);
  }
}