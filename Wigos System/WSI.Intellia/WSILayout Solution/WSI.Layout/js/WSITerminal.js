﻿/*
WSITerminal:
Information and data about a terminal object in layout
*/

// Constructor
function WSITerminal() {

  // Common data
  this.Name = null;
  this.Mesh = null;
  this.MeshHelper = null;
  this.Visible = true;
  this.MonitorDisplayTime = 0;
  this.MonitorDisplayDuration = Math.floor(Math.random() * (10000 - 5000 + 1)) + 5000;
  this.MonitorMaterialIndex = -1;
  this.BodyMaterialIndex = -1;
  this.MonitorNextResource = "monitor 1";

  // Properties
  this.Properties = new WSIObjectProperties();

}

// Function to know if terminal is initialized.
// In 3D Mode indicates if mesh is loaded
WSITerminal.prototype.IsInitialized = function () {
  return (this.Mesh != null);
}

// Updates the visibility of a terminal
WSITerminal.prototype.Update = function () {
  if (this.IsInitialized()) { this.Mesh.object.visible = this.Visible; }
}

// Makes the terminal visible
WSITerminal.prototype.Show = function () {

  if (this.IsInitialized()) {
    this.Mesh.object.visible = true;
    this.Visible = true;
  }

}

// Hides the terminal
WSITerminal.prototype.Hide = function () {

  if (this.IsInitialized()) {
    this.Mesh.object.visible = false;
    this.Visible = false;
  }

}

// 3D Mode function that changes the material in a model. 
WSITerminal.prototype.ReplaceMaterial = function (Manager, OldMaterial, NewMaterial) {
  // Search all materials in terminal mesh to find the changes
  for (var oi = 0; oi < this.Mesh.material.materials.length; oi++) {
    if (this.Mesh.material.materials[oi].name == OldMaterial) {
      this.Mesh.material.materials[oi] = Manager.Materials.Items[NewMaterial];
    }
  }
}

//// Change the material color by name in the list of materials in the model
//WSITerminal.prototype.ChangeMaterialColor = function (MaterialName, NewColor) {
//  if (this.Mesh.material.materials) {
//    // MeshFaceMaterial
//    for (var oi = 0; oi < this.Mesh.material.materials.length; oi++) {
//      if (this.Mesh.material.materials[oi].name == MaterialName) {
//        this.Mesh.material.materials[oi].color = new THREE.Color(NewColor);
//        // Downgrade the emissive color to avoid reflection
//        this.Mesh.material.materials[oi].emissive = WSIObjectUtils.DownColor(NewColor);
//        this.Mesh.material.materials[oi].combine = THREE.AddOperation;
//        // Replace the material 
//        this.Mesh.material.materials[oi] = new THREE.MeshPhongMaterial(this.Mesh.material.materials[oi].clone());
//      }
//    }
//  } else {
//    // OtherMaterial
//    if (this.Mesh.material.name == MaterialName) {
//      this.Mesh.material.color = new THREE.Color(NewColor);
//      // Downgrade the emissive color to avoid reflection
//      this.Mesh.material.emissive = WSIObjectUtils.DownColor(NewColor);
//      this.Mesh.material.combine = THREE.AddOperation;
//      // Replace the material
//      this.Mesh.material = new THREE.MeshPhongMaterial(this.Mesh.material.clone());
//    }
//  }
//}

// Change the material color by name in the list of materials in the model
WSITerminal.prototype.ChangeMaterialColor = function (MaterialName, NewColor) {

  var _mesh = (this.Mesh.children[0].visible) ? this.Mesh.children[0] : this.Mesh;

  if (NewColor == "0xffffff" || NewColor == "white") {
    NewColor = undefined;
  }

  if (_mesh.material.materials) {
    // MeshFaceMaterial
    for (var oi = 0; oi < _mesh.material.materials.length; oi++) {
      // RMS - Keep original color if not activity effect

      if (_mesh.material.materials[oi].name == "MACHINE_SLIVER" ||
          _mesh.material.materials[oi].name == "body" ||
        _mesh.material.materials[oi].name == "a_body") {

        if (NewColor) {
          if (!_mesh.material.materials[oi].original_emissive) {
            _mesh.material.materials[oi].original_emissive = _mesh.material.materials[oi].color;
          }
          _mesh.material.materials[oi].color = new THREE.Color(NewColor);
          _mesh.material.materials[oi].transparent = false;
          _mesh.material.materials[oi].opacity = 1;
        } else {
          if (_mesh.material.materials[oi].original_emissive) {
            _mesh.material.materials[oi].color = _mesh.material.materials[oi].original_emissive;
          }

          _mesh.material.materials[oi].transparent = WSIData.TransparentOutOfRange;
          _mesh.material.materials[oi].opacity = WSIData.TransparentOutOfRange ? WSIData.TransparentOutOfRangeOpacity : 1;
        }
        //_mesh.material.materials[oi].emissive = WSIObjectUtils.DownColor(_mesh.material.materials[oi].color);

      }

      _mesh.material.materials[oi].emissive = new THREE.Color("0x000000");
      _mesh.material.materials[oi].needsUpdate = true;

    }
  } else {
    if (_mesh.material.name == MaterialName) {
      if (NewColor == undefined) {
        NewColor = "#a0a0a0";
        _mesh.material.transparent = WSIData.TransparentOutOfRange;
        _mesh.material.opacity = WSIData.TransparentOutOfRange ? WSIData.TransparentOutOfRangeOpacity : 1;
      } else {
        _mesh.material.transparent = false;
        _mesh.material.opacity = 1;
      }
      _mesh.material.color = new THREE.Color(NewColor);
      //_mesh.material.emissive = WSIObjectUtils.DownColor(_mesh.material.color, 0.5);
      //_mesh.material.emissive = new THREE.Color("0x000000");
      _mesh.material.needsUpdate = true;
    }
  }
}

// Method to call the builders or constructors of a terminal including the label
WSITerminal.prototype.Build = function (callbackBuilder, callbackLabelBuilder) {
  return callbackBuilder(this, callbackLabelBuilder);
}

// Method to call the update routine for the terminal
WSITerminal.prototype.Update = function (callbackUpdater) {
  callbackUpdater(this);
}
WSITerminal.prototype.GetAlarmsCount = function () {

  return (this.Properties.alarms_machine_count + this.Properties.alarms_player_count);
}

// Returns true if there is an error in some properties
WSITerminal.prototype.HasError = function () {
  var _result = false;
  if (+this.Properties.al_sas_host_error > 0) {
    _result = true;
  } else if (+this.Properties.al_door_flags > 0) {
    _result = true;
  } else if (+this.Properties.al_bill_flags > 0) {
    _result = true;
  } else if (+this.Properties.al_printer_flags > 0) {
    _result = true;
  } else if (+this.Properties.al_egm_flags > 0) {
    _result = true;
  } else if (+this.Properties.al_call_attendant_flags > 0) {
    _result = true;
  } else if (+this.Properties.al_machine_flags > 0) {
    _result = true;
  }
  return _result;
}

WSITerminal.prototype.Front = function () {
  var _top = 0, _left = 0;
  switch (+this.Properties.lop_orientation) {
    case 0:
      _left = -2;
      break;
    case 1:
    case 45:
      break;
    case 2:
    case 90:
      _top = 2;
      break;
    case 3:
    case 135:
      break;
    case 4:
    case 180:
      _left = 2;
      break;
    case 5:
    case 225:
      break;
    case 6:
    case 270:
      _top = -2;
      break;
    case 7:
    case 315:
      break;
  }
  return { x: _left, z: _top };
}

/////////////////////////////////////////////////////////////////////////////////////////////////////

/*
WSITerminals:
A class to mantain the list of terminals loaded in layout
*/

// Constructor
function WSITerminals() {

  // Update process in progress 
  this.Updating = false;

  // Event to fire when update terminals process complete
  this.OnTerminalsUpdated = undefined;

  // Data
  this.Count = 0;
  this.Items = [];
  this.ByAccount = [];
  this.ByTerminal = [];
  this.ByProvider = [];
  this.ByArea = [];

  // Pivot is used in 3D Mode to contain all the terminals 
  if (_LayoutMode == 1) {
    this.Pivot = new THREE.Object3D();
  }

  this.OccupationData = {
    Total: 0,
    TotalCard: 0,
    Date: null,
    Occupation: [],
    Levels: [],
    Ages: [],
    AgesGender: [],
    Gender: [],
    Vips: 0,
    Alarms: [],
    Capacity: 0
  }

  // RMS : To avoid NaNs
  this.OccupationData.Gender["Male"] = 0;
  this.OccupationData.Gender["Female"] = 0;
  this.OccupationData.Gender["Unknown"] = 0;
  this.OccupationData.Occupation["Idle"] = 0;
  this.OccupationData.Occupation["Registered"] = 0;
  this.OccupationData.Occupation["Anonymous"] = 0;

  this.MonetaryData = {
    CoinIn: 0,
    CoinIns: 0,
    Won: 0,
    PSBetAverage: 0,
    Date: null
  }
}

// Build method callbacks by terminal
WSITerminals.prototype.Build = function (callbackBuilder, callbackLabelBuilder) {

  if (!callbackBuilder) {
    throw new Error("WSITerminals.Build() need a callback Builder!");
    return;
  }

  WSILogger.Write('WSITerminals.Build() -> Start building terminals...');

  // Call the callbacks on each terminal
  for (_terminal_id in this.Items) {

    var _terminal = this.Items[_terminal_id];

    // Reset Mesh and Pivot
    if (_LayoutMode == 1) {
      this.Pivot.remove(this.Items[_terminal_id].Mesh);
      this.Items[_terminal_id].Mesh = null;
    }

    if (_terminal) {
      // Call the callbacks
      var _builded_terminal = _terminal.Build(callbackBuilder, callbackLabelBuilder);

      this.Count++;
    }

  }

  WSILogger.Write('WSITerminals.Build() -> End building terminals.');

}

// Returns a terminal by the ObjectID (lo_id)
WSITerminals.prototype.GetTerminal = function (ObjectId) {
  if (this.Items[ObjectId]) {
    return this.Items[ObjectId];
  } else {
    WSILogger.Write('WSITerminals.GetTerminal() -> Object ' + ObjectId + ' does not exist!');
  }
}

// Returns a terminal by TerminalId (lo_external_id)
WSITerminals.prototype.FindTerminal = function (TerminalId) {
  for (_oid in this.Items) {
    if (this.Items[_oid].Properties.lo_external_id == TerminalId) {
      return this.Items[_oid];
    }
  }
  return null;
}

// Returns a terminal by the AccountId playing on it using index-matrix
WSITerminals.prototype.FindByAccountId = function (AccountId) {
  if (this.ByAccount[AccountId]) {
    return this.Items[this.ByAccount[AccountId]];
  }
  return null;
}

// Returns a terminal by the TerminalId using index-matrix
WSITerminals.prototype.FindByTerminalId = function (TerminalId) {
  if (this.ByTerminal[TerminalId]) {
    return this.Items[this.ByTerminal[TerminalId]];
  }
  return null;
}

//
WSITerminals.prototype.ResetMonetaryData = function (Date) {
  if (Date != this.MonetaryData.Date) {
    this.MonetaryData = {
      CoinIn: 0,
      CoinIns: 0,
      Won: 0,
      PSBetAverage: 0
    }
    this.MonetaryData.Date = Date;
  }
}

//
WSITerminals.prototype.UpdateMonetaryData = function (Date, Terminal) {

  this.ResetMonetaryData(Date);

  // Check for data ready
  if (Terminal.Properties.external_id != null) {
    // Played
    this.MonetaryData.CoinIn += +Terminal.Properties.played_amount;
    // Plays number
    this.MonetaryData.CoinIns += +Terminal.Properties.played_count;
    // Won
    this.MonetaryData.Won += +Terminal.Properties.won_amount;
    // Play session bet average
    this.MonetaryData.PSBetAverage += +Terminal.Properties.play_session_bet_average;
  }

  return null;
}

//
WSITerminals.prototype.ResetOccupationData = function (Date) {
  
  if (Date != this.OccupationData.Date) {
    this.OccupationData = {
      Total: 0,
      TotalCard: 0,
      Date: null,
      Occupation: [],
      Levels: [],
      Ages: [],
      AgesGender: [],
      Gender: [],
      Vips: 0,
      Alarms: [],
      Capacity: 0
    };
    for (var i = 0; i < _Manager.Ranges.length; i++) {
      var labelName = (_Manager.Ranges[i].Range[1]);
      this.OccupationData.Levels[labelName] = 0;
    }

    this.OccupationData.Date = Date;
    // RMS : To avoid NaNs
    this.OccupationData.Gender["Male"] = 0;
    this.OccupationData.Gender["Female"] = 0;
    this.OccupationData.Gender["Unknown"] = 0;
    this.OccupationData.Occupation["Idle"] = 0;
    this.OccupationData.Occupation["Registered"] = 0;
    this.OccupationData.Occupation["Anonymous"] = 0;


    // Reset Age Occupation Data to display array sorted.
    for (var _idx in _Manager.Legends.Items) {
      if (_Manager.Legends.Items[_idx].Name == "Age") {
        for (var _idx_age in _Manager.Legends.Items[_idx].Items) {
          var _item_age = _Manager.Legends.Items[_idx].Items[_idx_age];

          if (!this.OccupationData.Ages[_item_age.name]) {
            this.OccupationData.Ages[_item_age.name] = 0;
          }

        }
      }
    }
  }
}

//
WSITerminals.prototype.UpdateOccupationData = function (Date, Terminal) {

  var _occ = "";
  var _age = "";
  var _lev = "";
  var _gen = "";

  this.ResetOccupationData(Date);

  // Check for data ready
  if (Terminal.Properties.external_id != null) {
    this.OccupationData.Total += 1;

    _occ = Terminal.Properties.play_session_status == "" ? "Unknown" : Terminal.Properties.play_session_status;
    //_occ = Terminal.Properties.play_occupation_status == "" ? "Unknown" : Terminal.Properties.play_occupation_status;

    if (_occ != 1) {
      _age = Terminal.Properties.player_age == "" ? "Unknown" : Terminal.Properties.player_age;
      _lev = Terminal.Properties.player_level == "" ? "Unknown" : Terminal.Properties.player_level;
      _gen = Terminal.Properties.player_gender == "" ? "Unknown" : Terminal.Properties.player_gender;
    }

    // Occupation

    // TODO : Translations

    switch (true) {
      case _occ == 1:
        _occ = "Idle";
        break;
      case _occ == 2:
        _occ = "Anonymous";
        this.OccupationData.TotalCard += 1;
        break;
      default:
        _occ = "Registered";
        this.OccupationData.TotalCard += 1;
        break;
    }

    if (this.OccupationData.Occupation[_occ]) {
      this.OccupationData.Occupation[_occ] += 1;
    } else {
      this.OccupationData.Occupation[_occ] = 1;
    }

    if (_occ == "Idle") {

      return null;
    }

    // Age
    var _age_temp = "Unknown";

    for (var _idx in _Manager.Legends.Items) {
      if (_Manager.Legends.Items[_idx].Name == "Age") {
        for (var _idx_age in _Manager.Legends.Items[_idx].Items) {
          var _item_age = _Manager.Legends.Items[_idx].Items[_idx_age];

          if (_item_age.maxValue != 0 && _age >= _item_age.minValue && _age <= _item_age.maxValue) {
            _age_temp = _item_age.name;
          }
          else if (_item_age.maxValue == 0 && _age >= _item_age.minValue) {
            _age_temp = _item_age.name;
          }
        }
        break;
      }
    }
    _age = _age_temp;


    //if (_age != "Unknown") {
    if (this.OccupationData.Ages[_age]) {
      this.OccupationData.Ages[_age] += 1;
    } else {
      this.OccupationData.Ages[_age] = 1;
    }
    if (!this.OccupationData.AgesGender[_age]) {
      this.OccupationData.AgesGender[_age] = [0, 0];
    }
    //}

    // Levels

    //switch (+_lev) {
    //  case -1:
    //    _lev = "Unknown";
    //    break;
    //  case 1:
    //    _lev = "Bronze";
    //    break;
    //  case 2:
    //    _lev = "Gold";
    //    break;
    //  case 3:
    //    _lev = "Platinum";
    //    break;
    //  case 4:
    //    _lev = "VIP";
    //    break;
    //  default:
    //    _lev = "Unknown";
    //    break;
    //}

    _lev = _Manager.Ranges.BuscaNivel(_lev);

    //if (_lev != "Unknown") {
    if (this.OccupationData.Levels[_lev]) {
      this.OccupationData.Levels[_lev] += 1;
    } else {
      this.OccupationData.Levels[_lev] = 1;
    }


    // Gender

    switch (_gen) {
      case "0":
        _gen = "Unknown";
        if (_age != "Unknown") { this.OccupationData.AgesGender[_age][0] += 1; }
        break;
      case "1":
        _gen = "Male";
        if (_age != "Unknown") { this.OccupationData.AgesGender[_age][0] += 1; }
        break;
      case "2":
        _gen = "Female";
        if (_age != "Unknown") { this.OccupationData.AgesGender[_age][1] += 1; }
        break;
      default:
        _gen = "Idle";
        break;
    }

    //if (_gen != "Unknown") {
    if (_occ != "Idle") {
      if (this.OccupationData.Gender[_gen]) {
        this.OccupationData.Gender[_gen] += 1;
      } else {
        this.OccupationData.Gender[_gen] = 1;
      }
    }
    //}

    // Vips

    if (+Terminal.Properties.player_is_vip != 0) {
      this.OccupationData.Vips += 1;
    }

    // Alamrs

    if (!this.OccupationData.Alarms['Software']) { this.OccupationData.Alarms['Software'] = 0; }
    if (!this.OccupationData.Alarms['Hardware']) { this.OccupationData.Alarms['Hardware'] = 0; }
    if (!this.OccupationData.Alarms['Player']) { this.OccupationData.Alarms['Player'] = 0; }
    if (!this.OccupationData.Alarms['Jackpot']) { this.OccupationData.Alarms['Jackpot'] = 0; }

    switch (true) {
      case Terminal.Properties.al_sas_host_error == 1:
        this.OccupationData.Alarms['Software'] += 1;
        break;
      case Terminal.Properties.al_door_flags == 1:
        this.OccupationData.Alarms['Hardware'] += 1;
        break;
      case Terminal.Properties.al_bill_flags == 1:
        this.OccupationData.Alarms['Hardware'] += 1;
        break;
      case Terminal.Properties.al_printer_flags == 1:
        this.OccupationData.Alarms['Hardware'] += 1;
        break;
      case Terminal.Properties.al_egm_flags == 1:
        this.OccupationData.Alarms['Software'] += 1;
        break;
      case Terminal.Properties.al_played_won_flags == 1:
        this.OccupationData.Alarms['Player'] += 1;
        break;
      case Terminal.Properties.al_jackpot_flags == 1:
        this.OccupationData.Alarms['Jackpot'] += 1;
        break;
      case Terminal.Properties.al_stacker_status == 1:
        this.OccupationData.Alarms['Hardware'] += 1;
        break;
      case Terminal.Properties.al_call_attendant_flags == 1:
        this.OccupationData.Alarms['Player'] += 1;
        break;
    }

  }

  return null;
}

WSITerminals.prototype.UpdateIndexes = function (Identifier) {

  // Update ByAccount Index
  if (this.Items[Identifier].Properties.player_account_id != "") {
    this.ByAccount[this.Items[Identifier].Properties.player_account_id] = (this.Items[Identifier].Properties.lo_id == null) ? Identifier : this.Items[Identifier].Properties.lo_id;
  }

  // Update ByTerminal external_id Index
  this.ByTerminal[this.Items[Identifier].Properties.lo_external_id] = (this.Items[Identifier].Properties.lo_id == null) ? Identifier : this.Items[Identifier].Properties.lo_id;

  // Update ByProvider Index
  if (!this.ByProvider[this.Items[Identifier].Properties.provider_name]) {
    this.ByProvider[this.Items[Identifier].Properties.provider_name] = [];
  }
  this.ByProvider[this.Items[Identifier].Properties.provider_name].push(Identifier);

  // Update ByArea Index
  if (!this.ByArea[this.Items[Identifier].Properties.area_id]) {
    this.ByArea[this.Items[Identifier].Properties.area_id] = [];
  }
  this.ByArea[this.Items[Identifier].Properties.area_id].push(Identifier);


}

/////////////////////////////////////////////////////////////////////////////////////////////////////-->