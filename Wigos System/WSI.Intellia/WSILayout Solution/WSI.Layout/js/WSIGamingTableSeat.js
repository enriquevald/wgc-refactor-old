﻿function WSIGamingTableSeat() {

    this.Name = null;
    this.Mesh = null;
    this.Visible = true;

    // Properties
    this.Properties = new WSIObjectProperties();

}

WSIGamingTableSeat.prototype.Build = function (callbackBuilder) {
    return callbackBuilder(this);
}

//////////////////////////////////////////////////////

function WSIGamingTableSeats() {

    this.Count = 0;
    this.Items = [];

}

WSIGamingTableSeats.prototype.Build = function (callbackBuilder) {

    if (!callbackBuilder) {
        throw new Error("WSIGamingTableSeats.Build() need a callback Builder!");
        return;
    }

    for (_seat in this.Items) {
        _seat.Build(callBackBuilder);
    }

}