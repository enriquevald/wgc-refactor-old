﻿var _table_customers = new TableListBase(".tablaJugadores");
var _table_customers_compare = new TableListBase(".tablaJugadores");
var m_cat_compare_list = [];
var m_cat_filter = "";

//************************************************************************************************************
//LISTA
//************************************************************************************************************
function WSICustomersAtRoom_Open() {

    if ($(".wrapJugadores.customerMode").children().length == 0) {

        $(".wrapJugadores").empty();
        $(".wrapJugadores").html(WSICustomersAtRoom_Init());
        $(".wrapJugadores").addClass("customerMode");

    }

    CreateTableCustomers();
    WSICustomersAtRoom_Init_Events();
    m_cat_filter = "";

    _Language.parse();

}

function WSICustomersAtRoom_Init() {
    var _result = '';

    _result += '<div class="headerTablas">';
    _result += '  <div class="cierreLista">';
    _result += '    <a href="#" data-nls="NLS_CLOSE_LIST">Cerrar Lista</a>';
    _result += '  </div>';
    _result += '    <div class="jugadoresSearchBox">';
    _result += '     <input class="srcJugadorPanel" id="inputJugadorListasrc" type="text" data-nls="NLS_PLAYERS_SEARCH_PLACEHOLDER" placeholder="" value="">';
    _result += '     <div class="btnBuscarJugador" id="btnBuscarJugadorLista">';
    _result += '       <a href="#" data-nls="NLS_SEARCH">' + _Language.get('NLS_SEARCH') + '</a>';
    _result += '     </div>';
    _result += '    </div>';
    _result += "    <div class='selectorNivelJugadores'>"
    _result += "      <select class='selectNivelJugadores' id='selPlayerLevel'>";
    _result += "        <option value='' selected data-nls='NLS_LEVEL'>" + _Language.get('NLS_LEVEL') + "</option>";
    for (var i = 0; i < _Manager.Ranges.length; i++) {
      _result += "        <option value='" + parseInt(_Manager.Ranges[i].Range[0]) + "'>" + _Manager.Ranges[i].Range[1] + "</option>";
    }
    _result += "      </select>";
    _result += "    </div>";
    _result += "  <div class='iraComparar' id='jugadoresListaIraComparar' " + (m_cat_compare_list.length > 1 ? "" : "style='display:none;'") + " data-nls='NLS_COMPARE_PLAYERS'>" + _Language.get('NLS_COMPARE_PLAYERS');
    _result += "  </div>";
    _result += "</div>";
    _result += '<div class="insideJugadores">  ';
    _result += '  <div class="tablaJugadores">';
    _result += '  </div>';
    _result += '</div>';

    return _result;
}

function WSICustomersAtRoom_Init_Events() {
    //Cierre lista jugadores
    $('.cierreLista a').click(function () {
        cerrarListaJugadores();
        $(".wrapJugadores").removeClass("customerMode");
    });

    $("#btnBuscarJugadorLista").click(function () {

        m_cat_filter = $("#inputJugadorListasrc").val();

        WSICustomerAtRoom_GetFilters();
    });

    $("#jugadoresListaIraComparar").click(function () {

       cerrarListaJugadores(function () { mostrarCompararJugadores2(); });
       $(".wrapJugadores").removeClass("customerMode");
    })
}
function mostrarCompararJugadores2() {
    ocultarSlideButton();
    ocultarPanelControl();
    WSICustomersAtRoom_Comparar_Open();

    $('.wrapJugadores').css('display', 'block');
    $('.wrapJugadores').animate({
        right: '0'
    });

    $('.wrapJugadores').removeClass('show').addClass('hide');

}
//function WSICustomersAtRoom_mostrarCompararJugadores() {

//  if ($('#jugadoresCompararON').is(':checked')) {
//    ocultarPanelControl();
//    WSICustomersAtRoom_Comparar_Open();

//    $('.wrapJugadores').css('display', 'block');
//    $('.wrapJugadores').animate({
//      right: '0'
//    });

//    $('.wrapJugadores').removeClass('show').addClass('hide');


//}

function WSICustomersAtRoom_LoadData() {
    var _list = [];

    for (_idx_account in _Manager.Terminals.ByAccount) {
        _terminal = _Manager.Terminals.Items[_Manager.Terminals.ByAccount[_idx_account]];

        if (_terminal.Properties.player_name != "" && _terminal.Properties.player_name != undefined) {
            if (m_cat_filter == "" || _terminal.Properties.player_name.toUpperCase().startsWith(m_cat_filter.toUpperCase())) {
                var _player_gender = "";
                if (_terminal.Properties.player_gender == "1") {
                    _player_gender = _Language.get("NLS_MALE");
                }
                else if (_terminal.Properties.player_gender == "2") {
                    _player_gender = _Language.get("NLS_FEMALE");
                }

                var _item = {
                    player_name: _terminal.Properties.player_name,
                    player_level_name: _terminal.Properties.player_level_name,
                    player_is_vip: _terminal.Properties.player_is_vip && +_terminal.Properties.player_is_vip == 1 ? "1" : "0",
                    player_gender: _player_gender,
                    name: _terminal.Properties.name,
                    area_id: _terminal.Properties.area_id,
                    terminal_id: _terminal.Properties.external_id,
                    player_account_id: _terminal.Properties.player_account_id
                }

                _list.push(_item);
            }
        }
    }

    return _list
}

function WSICustomersAtRoom_Refresh() {
    _table_customers.rows = WSICustomersAtRoom_LoadData();
    _table_customers.Refresh();
}

function CreateTableCustomers() {
    //Field, IsPrimaryKey, Visible, Title, Width, Display, ColumnType, Sortable
    _table_customers.AddColumn("id", true, false, "", "0%", "", "text", false);
    _table_customers.AddColumn("player_name", false, true, "NLS_NAME", "25%", "", "text", true);
    _table_customers.AddColumn("player_level_name", false, true, "NLS_LEVEL", "5%", "", "text", true);
    _table_customers.AddColumn("player_is_vip", false, true, "NLS_VIP", "5%", WSICustomerAtRoom_PrintRowVip, "text", true);
    _table_customers.AddColumn("player_gender", false, true, "NLS_GENDER", "10%", "", "text", true);
    _table_customers.AddColumn("name", false, true, "NLS_TERMINAL", "10%", "", "text", true);
    _table_customers.AddColumn("area_id", false, true, "NLS_AREA", "5%", "", "text", true);
    _table_customers.AddColumn("actions", false, true, "NLS_STATE", "10%", WSICustomerAtRoom_PrintRowInfo, "text", false);
    _table_customers.AddColumn("Keeper", false, true, "NLS_LOCATE", "10%", WSICustomersAtRoom_PrintRowLocateCustomerAtRoom, "text", false);
    _table_customers.AddColumn("compare", false, true, "NLS_COMPARE", "10%", WSICustomerAtRoom_PrintRowCompare, "text", false);

    _table_customers.rows = WSICustomersAtRoom_LoadData();
    _table_customers.paging = true;
    _table_customers.allowDelete = false;
    _table_customers.sortValue = "player_name ASC";
    _table_customers.defaultSorting = "player_name ASC";

    _table_customers.CreateTable();
}

function WSICustomersAtRoom_PrintRowLocateCustomerAtRoom(data) {
    var $control = $("<a class='btnLocalizarAlarma' data-terminalid='" + data.record.terminal_id + "' href='#'>Localizar Alarma</a>");

    $control.click(function (evt) {

        cerrarListaJugadores();
        $(".wrapJugadores").removeClass("customerMode");

        _UI.SetMenuActiveSection($("#a_section_60")[0], 60);
        ShowTerminalInfo(_Manager.Terminals.Items[_Manager.Terminals.ByTerminal[$(evt.currentTarget).data("terminalid")]], 1, false);
    });

    return $control;
}

function WSICustomerAtRoom_PrintRowVip(data) {
    if (+data.record.player_is_vip == 1) {
        return $('<div style="width:100%;background-color:yellow;text-align:center;">VIP</div>');
    }
}

function WSICustomerAtRoom_PrintRowInfo(data) {
    var $item = $("<div data-terminalid='" + data.record.terminal_id + "' class='icoSnapshot' />")

    $item.click(function (evt) {
      if (_LayoutMode == 2) {
        //Unselect Terminals
        $($("#" + m_terminal_id_selected))
          .css('border', '1px solid black')
          .css('z-index', '2'); if ($($("#" + m_terminal_id_selected)).attr("selected")) {
            $($("#" + m_terminal_id_selected))
            .css("top", ($("#" + m_terminal_id_selected).attr('initial_top')))
            .css("left", ($("#" + m_terminal_id_selected).attr('initial_left')))
            .removeAttr("selected");
          }
      }
        var _terminal_id = _Manager.Terminals.Items[_Manager.Terminals.ByTerminal[$(evt.currentTarget).data("terminalid")]];

        m_terminal_id_selected = _terminal_id.Properties.lo_id;
        UpdatePopupTerminal(null, _terminal_id);
        OpenInfoPanel();
    })

    return $item;
}

function WSICustomerAtRoom_PrintRowCompare(data) {
    var _compare_class = "";

    if ($.inArray(data.record.player_account_id, m_cat_compare_list) == -1) {
        _compare_class = "btnCompararJugador";
    } else {
        _compare_class = "compararJugSel";
    }

    var _$result = $("<div class='" + _compare_class + "' data-terminalid='" + data.record.terminal_id + "'></div>");

    _$result.click(function (evt) {
        var _terminal = _Manager.Terminals.Items[_Manager.Terminals.ByTerminal[$(evt.currentTarget).data("terminalid")]];
        var _idx = $.inArray(_terminal.Properties.player_account_id, m_cat_compare_list);
        if (_idx == -1) {
            m_cat_compare_list.push(_terminal.Properties.player_account_id);
            $(this).removeClass("btnCompararJugador");
            $(this).addClass("compararJugSel");
        }
        else {
            m_cat_compare_list.splice(_idx, 1);
            $(this).removeClass("compararJugSel");
            $(this).addClass("btnCompararJugador");
        }


        if (m_cat_compare_list.length > 1) {
            $("#jugadoresListaIraComparar").show();
        } else {
            $("#jugadoresListaIraComparar").hide();
        }
    })

    return _$result;
}


function WSICustomerAtRoom_GetFilters() {
    var _cat_filter_list = [];
    var cb_player_level = document.getElementById("selPlayerLevel");

    var _alarm_type_value;



    _alarm_type_value = _Manager.Ranges.BuscaNivel(cb_player_level.options[cb_player_level.selectedIndex].value);


    if (_alarm_type_value != 0) {
        _cat_filter_list["player_level_name"] = _alarm_type_value;
    }

    _table_customers.ApplyFilter(_cat_filter_list);

    WSICustomersAtRoom_Refresh();
}

//************************************************************************************************************
//COMPARAR
//************************************************************************************************************
function WSICustomersAtRoom_Comparar_Open() {

    if ($(".wrapJugadores.customerMode").children().length == 0) {
        $(".wrapJugadores").empty();
        $(".wrapJugadores").html(WSICustomersAtRoom_Comparar_Init());
        $(".wrapJugadores").addClass("customerCompareMode");
    }

    CreateTableCustomersCompare();
    WSICustomersAtRoom_Comparar_Init_Events();
}

function WSICustomersAtRoom_Comparar_Init() {
    var _result = '';

    _result += '<div class="headerTablas">';
    _result += '  <div class="cierreLista">';
    _result += '    <a href="#" data-nls="NLS_CLOSE_LIST">Cerrar Lista</a>';
    _result += '  </div>';
    _result += "</div>";
    _result += '<div class="insideJugadores">  ';
    _result += '  <div class="tablaJugadores">';
    _result += '  </div>';
    _result += '</div>';

    return _result;
}

function WSICustomersAtRoom_Comparar_Init_Events() {
    //Cierre lista jugadores
    $('.cierreLista a').click(function () {
        cerrarCompararJugadores();
        $(".wrapJugadores").removeClass("customerCompareMode");
    });
}

function WSICustomersAtRoom_Comparar_LoadData() {
    var _list = [];

    for (_idx_account in m_cat_compare_list) {
        _terminal = _Manager.Terminals.Items[_Manager.Terminals.ByAccount[m_cat_compare_list[_idx_account]]];

        if (_terminal && _terminal.Properties.player_name != "") {

            var _player_gender = "";
            if (_terminal.Properties.player_gender == "1") {
                _player_gender = _Language.get("NLS_MALE");
            }
            else if (_terminal.Properties.player_gender == "2") {
                _player_gender = _Language.get("NLS_FEMALE");
            }

            var _item = {
                id: _terminal.Properties.player_account_id,
                player_name: _terminal.Properties.player_name,
                player_level_name: _terminal.Properties.player_level_name,
                player_is_vip: _terminal.Properties.player_is_vip && +_terminal.Properties.player_is_vip == 1 ? "1" : "0",
                player_gender: _player_gender, //Math.floor((Math.random() * 200) + 1),
                name: _terminal.Properties.name,
                area_id: _terminal.Properties.area_id,
                terminal_id: _terminal.Properties.external_id,
                player_account_id: _terminal.Properties.player_account_id
            }

            _list.push(_item);
        }
    }

    return _list
}

function WSICustomersAtRoom_Comparar_Refresh() {
    _table_customers_compare.rows = WSICustomersAtRoom_Comparar_LoadData();
    _table_customers_compare.Refresh();
}

function CreateTableCustomersCompare() {
    //Field, IsPrimaryKey, Visible, Title, Width, Display, ColumnType, Sortable
    _table_customers_compare.AddColumn("id", true, false, "", "0%", "", "text", false);
    _table_customers_compare.AddColumn("player_name", false, true, "NLS_NAME", "25%", "", "text", true);
    _table_customers_compare.AddColumn("player_level_name", false, true, "NLS_LEVEL", "5%", "", "text", true);
    _table_customers_compare.AddColumn("player_is_vip", false, true, "NLS_VIP", "5%", WSICustomersAtRoom_Compare_PrintRowVip, "text", true);
    _table_customers_compare.AddColumn("player_gender", false, true, "NLS_GENDER", "10%", "", "text", true);
    _table_customers_compare.AddColumn("name", false, true, "NLS_TERMINAL", "10%", "", "text", true);
    _table_customers_compare.AddColumn("area_id", false, true, "NLS_AREA", "5%", "", "text", true);
    _table_customers_compare.AddColumn("actions", false, true, "NLS_STATE", "10%", WSICustomersAtRoom_Compare_PrintRowInfo, "text", false);
    _table_customers_compare.AddColumn("Keeper", false, true, "NLS_LOCATE", "10%", WSICustomersAtRoom_Compare_PrintRowLocateCustomerAtRoom, "text", false);

    _table_customers_compare.rows = WSICustomersAtRoom_Comparar_LoadData();
    _table_customers_compare.paging = true;
    _table_customers_compare.allowDelete = true;
    _table_customers_compare.OnDeleteRow = WSICustomersAtRoom_Compare_OnDeleteRow;
    _table_customers_compare.sortValue = "player_name ASC";
    _table_customers_compare.OnDeleteMessage = "NLS_DELETE_CURRENT_RECORD";

    _table_customers_compare.CreateTable();
}

function WSICustomersAtRoom_Compare_PrintRowLocateCustomerAtRoom(data) {
    var $control = $("<a class='btnLocalizarAlarma' data-terminalid='" + data.record.terminal_id + "' href='#' data-nls='NLS_LOCATE_ALARM'>" + _Language.get('NLS_LOCATE_ALARM') + "</a>");

    $control.click(function (evt) {

        cerrarCompararJugadores();
        $(".wrapJugadores").removeClass("customerCompareMode");

        _UI.SetMenuActiveSection($("#a_section_60")[0], 60);

        ShowTerminalInfo(_Manager.Terminals.Items[_Manager.Terminals.ByTerminal[$(evt.currentTarget).data("terminalid")]], 1, false);
    });

    return $control;
}

function WSICustomersAtRoom_Compare_PrintRowVip(data) {
    if (+data.record.player_is_vip == 1) {
        return $('<div style="width:100%;background-color:yellow;text-align:center;" data-nls="NLS_VIP">' + _Language.get('NLS_VIP') + '</div>');
    }
}

function WSICustomersAtRoom_Compare_PrintRowInfo(data) {
    var $item = $("<div data-terminalid='" + data.record.terminal_id + "' class='icoSnapshot' />")

    $item.click(function (evt) {
        var _terminal_id = _Manager.Terminals.Items[_Manager.Terminals.ByTerminal[$(evt.currentTarget).data("terminalid")]];

        m_terminal_id_selected = _terminal_id.Properties.lo_id;
        UpdatePopupTerminal(null, _terminal_id);
        OpenInfoPanel();
    })

    return $item;
}

function WSICustomersAtRoom_Compare_OnDeleteRow(data, Sender, SenderClick) {
    //var _index = $.inArray(data, m_cat_compare_list);
    var _index = -1;
    for (var _i in m_cat_compare_list) {
      if (m_cat_compare_list[_i] == data)
        _index = _i;
    }
    m_cat_compare_list.splice(_index, 1);
    WSICustomersAtRoom_Comparar_Refresh();
}
