﻿var WSIConstants = (function () {
    var private = {
        'Material Type': { 'Basic': 1, 'Lambert': 2 },
        'Geometry' : { 'Level' : 1 }
    };

    return {
        get: function (category, name) { return private[category][name]; }
    };
})();