﻿var m_mouse_actions_mousedown = false;
var m_mouse_actions_mousemove_before_mousedown = false;
var m_update_terminals_pending = false;

/*
var cssTransitionsSupported = false;
(function() {

var div = document.createElement('div');

div.setAttribute('style', 'transition:top 1s ease;-webkit-transition:top 1s ease;-moz-transition:top 1s ease;');

cssTransitionsSupported = !!(div.style.transition || div.style.webkitTransition || div.style.MozTransition);

delete div;

})();
*/

/////////////////////////////////////////////////////////////////////////////////////

var _Manager = new WSIManager();
/////////////////////////////////////////////////////////////////////////////////////

var CONST_INITIAL_ZOOM = "0.6";

var m_terminal_id_selected = null;
var m_terminal_id_selected_old = null;
var m_account_id_selected = null;
var m_account_id_selected_old = null;

var m_runner_id_selected = null;

var m_browser;
var m_movements_timeout;
var m_movements_speed = 1;
var m_current_zoom = 1.0;
var m_current_background_position_left = 0;
var m_current_background_position_top = 0;

var incial_position_left = 0;
var incial_position_top = 0;

var m_current_popup_background_position_left = 0;
var m_current_popup_background_position_top = 0;
//var m_activity_sleep = 1000;

var m_div_background_is_clicked = false;
var m_popup_infoWindow_is_clicked = false;
var m_current_mouse_position_x;
var m_current_mouse_position_y;

var m_islands_positions;

var m_monitor_notify_alarms = false;
var m_section_first_time = false;

var m_zindex_terminal = 4;
var m_zindex_terminal_selected = 5;

m_islands_positions = [ // posX, posY, width, height
[-86, 2, 4, 1],
[-5, 2, 4, 1],
[20, 2, 4, 1],
[-88, 21, 2, 2],
[-68, 21, 2, 2],
[-46, 21, 2, 2],
[-26, 21, 2, 2],
[-1, 21, 2, 2],
[22, 21, 2, 2],
[-68, 38, 2, 4],
[-48, 38, 2, 4],
[-24, 38, 2, 4],
[-89, 62, 2, 3],
[-68, 62, 2, 3],
[-48, 62, 2, 3],
[-27, 62, 2, 3],
[-29, 84, 4, 1],
[-52, 84, 4, 1],
[-71, 84, 3, 1],
[-90, 84, 3, 1],
[-105, 84, 2, 1],
[36, 36, 4, 4],
[101, 18, 4, 1],
[122, -6, 1, 4],
[122, -28, 1, 4],
[146, -6, 2, 4],
[168, -6, 2, 4],
[146, 17, 2, 4],
[169, 19, 2, 4],
[146, 41, 2, 3],
[169, 41, 2, 3],
[101, 41, 2, 3],
[120, 41, 2, 3],
[146, 62, 2, 3],
[169, 62, 2, 3],
[101, 62, 2, 3],
[120, 62, 2, 3],
[80, 43, 2, 3],
[97, 84, 8, 1],
[138, 84, 4, 1],
[161, 84, 4, 1],
[136, -32, 3, 1],
[152, -32, 3, 1],
[168, -32, 3, 1],
[191, -10, 1, 3],
[190, 7, 1, 3],
[75, 78, 4, 3]
];


var m_areas_positions;
m_areas_positions = [ // posX, posY, width, height
[-172, 16, 16, 18],
[-106, 0, 24, 23],
[-8, 0, 18, 14],
[68, -20, 12, 8],
[119, -35, 20, 28],
[69, 16, 12, 15],
[69, 80, 28, 3]
];




/* test ///////////////////////////////////////////////////////////////////////*/


function fire(e) { }

function touchHandler(event) {
    if (!event.changedTouches) { return; }

    var touches = event.changedTouches,
        first = touches[0],
        type = "";

    switch (event.type) {

        case "touchstart": type = "mousedown"; break;
        case "touchmove": type = "mousemove"; break;
        case "touchend": type = "mouseup"; break;
        default: return;
    }

    var simulatedEvent = document.createEvent("MouseEvent");
    simulatedEvent.initMouseEvent(type, true, true, window, 1,
                          first.screenX, first.screenY,
                          first.clientX, first.clientY, false,
                          false, false, false, 0/*left*/, null);

    first.target.dispatchEvent(simulatedEvent);
    event.preventDefault();
}

function init() {

    var _control = document.getElementById("div_background");

    if (_control) {
        _control.addEventListener("touchstart", touchHandler, true);
        _control.addEventListener("touchmove", touchHandler, true);
        _control.addEventListener("touchend", touchHandler, true);
        _control.addEventListener("touchcancel", touchHandler, true);

        _control.addEventListener("mousedown", touchHandler, true);
        _control.addEventListener("mousemove", touchHandler, true);
        _control.addEventListener("mouseup", touchHandler, true);
        _control.addEventListener("mousecancel", touchHandler, true);
    }

    _control = document.getElementById("pnlNavigation");

    if (_control) {
        _control.addEventListener("touchstart", touchHandler, true);
        _control.addEventListener("touchmove", touchHandler, true);
        _control.addEventListener("touchend", touchHandler, true);
        _control.addEventListener("touchcancel", touchHandler, true);
    }

    // 20150225
    //Create Ribbon Toolbar
    //CreateRibbonToolbar("tabs");
    //Create Div InfoWindow
    //CreateInfoWindow("popup_infoWindow");



}


/* test ///////////////////////////////////////////////////////////////////////*/

//////////////////////////////////////////////




//////////////////////////////////////////////

// ready
$(document).ready(function () {

    browserDetect();

    init();

    //////////////////////////////////////////////////////////////

    $("#div_background").css("width", CONST_DIV_BACKGROUND_WIDTH);
    $("#div_background").css("height", CONST_DIV_BACKGROUND_HEIGHT);


    /*$("#div_runnersmapArea").click(function (e) {
        var posX = $("#div_background").position().left, posY = $("#div_background").position().top;
        //alert((e.pageX - posX) + ' , ' + (e.pageY - posY));
        //RunnersMapClick(e);
    });*/

    $("#div_runnersmapArea").on('click', '.marker', function (e) {

        //var posX = $(e.currentTarget).position().left + $(e.currentTarget).width()/2;
        //var posY = $(e.currentTarget).position().top + $(e.currentTarget).height();

        var posX = e.currentTarget.offsetLeft + e.currentTarget.width / 2;
        var posY = e.currentTarget.offsetTop + e.currentTarget.height;

        console.log('div_runnersmapArea.click', posX + ' ' + posY);

        var beacon2D = new Object();
        beacon2D.x = _Manager.RunnersMap.convertXTo2D(posX);
        beacon2D.y = _Manager.RunnersMap.convertYTo2D(posY);

        var _runner_id = _Manager.RunnersMap.GetClosestRunners(beacon2D.x, beacon2D.y);

   
        if (_runner_id != null && _runner_id.count > 0) {

            m_runner_id_selected = _runner_id.length - 1;
            _Manager.RunnersMap.RunnerSelected = _runner_id.length - 1

            if (_runner_id.count == 1) {

                // Design integration

                OnRunnerClick(m_runner_id_selected);

                OpenInfoPanel();

            } else {

                //OpenRunnerDialog(_runner_id);
                DialogRunner(_runner_id);

            }

        } else {
            WSIData.SelectedObject = null;
            m_terminal_id_selected = null;
            _Manager.HideSelector();
            _Manager.RunnersMap.RunnerSelected = null;
        }

    });

    CreateGrid(25);

    $(document).mousedown(function (e) {
        //e.preventDefault();

        m_current_mouse_position_x = e.pageX;
        m_current_mouse_position_y = e.pageY;

        return true;
    });

    $(document).mouseup(function (e) {
        //clearInterval(m_movements_timeout);
        m_popup_infoWindow_is_clicked = false;
        m_div_background_is_clicked = false;
        //m_movements_timeout = null;

        return true;
    });

    $("#div_background").mouseup(function (e) {
        //e.preventDefault();
        //clearInterval(m_movements_timeout);
        m_div_background_is_clicked = false;

        m_popup_infoWindow_is_clicked = false;

        if (!m_mouse_actions_mousemove_before_mousedown) {
            m_mouse_actions_mousemove_before_mousedown = false;
            m_mouse_actions_mousedown = false;
        } else {
            m_mouse_actions_mousemove_before_mousedown = false;
            m_mouse_actions_mousedown = false;

            if (m_update_terminals_pending) {
                UpdateTerminals();

                m_update_terminals_pending = false;
            }
        }

        return false;
    });

    $("#div_background").mousedown(function (e) {
        e.preventDefault();
        m_div_background_is_clicked = true;
        m_current_mouse_position_x = e.pageX;
        m_current_mouse_position_y = e.pageY;
        m_mouse_actions_mousedown = true;

        return false;
    });

    $("#popup_infoWindow").mousedown(function (e) {
        e.preventDefault();
        m_popup_infoWindow_is_clicked = true;
        m_current_mouse_position_x = e.pageX;
        m_current_mouse_position_y = e.pageY;

        return false;
    });


    $(this).mousemove(function (e) {
        var _temp_left;
        var _temp_top;
        var _css_transform;
        var _css_value;

        m_mouse_actions_mousemove_before_mousedown = m_mouse_actions_mousedown;

        // e.preventDefault();
        if (m_div_background_is_clicked) {
            _temp_left = (e.pageX - m_current_mouse_position_x) / m_current_zoom + m_current_background_position_left;
            _temp_top = (e.pageY - m_current_mouse_position_y) / m_current_zoom + m_current_background_position_top;

            _css_value = "scale(" + m_current_zoom + ")" + "translate(" + _temp_left + "px, " + _temp_top + "px)";

            _css_transform = GetPropertyName_Transform();

            jQuery("#div_background").css(_css_transform, _css_value);

            m_current_background_position_left = _temp_left;
            m_current_background_position_top = _temp_top;
            m_current_mouse_position_x = e.pageX;
            m_current_mouse_position_y = e.pageY;
        }

        if (m_popup_infoWindow_is_clicked) {
            _temp_left = (e.pageX - m_current_mouse_position_x) + m_current_popup_background_position_left;
            _temp_top = (e.pageY - m_current_mouse_position_y) + m_current_popup_background_position_top;

            _css_value = "translate(" + _temp_left + "px, " + _temp_top + "px)";

            _css_transform = GetPropertyName_Transform();

            jQuery("#popup_infoWindow").css(_css_transform, _css_value);

            m_current_popup_background_position_left = _temp_left;
            m_current_popup_background_position_top = _temp_top;
            m_current_mouse_position_x = e.pageX;
            m_current_mouse_position_y = e.pageY;
        }
        return true;
    });



    //$(this).on('DOMMouseScroll mousewheel', function (e) {
    $("#div_container").on('DOMMouseScroll mousewheel', function (e) {
        var _zoom_movement;

        e.preventDefault();

        if (CheckZoomDissabled()) { return; }

        switch (m_browser) {

            case "Chrome":
            case "Opera":
            case "Safari":
                if (e.originalEvent.wheelDeltaY < 0) {
                    _zoom_movement = CONST_MOVEMENT_TYPE_ZOOM_MINUS;
                }
                else {
                    _zoom_movement = CONST_MOVEMENT_TYPE_ZOOM_MORE;
                }

                break;


                //IE and Firefox                                                                    
            default:

                if (e.originalEvent.wheelDelta < 0) {
                    _zoom_movement = CONST_MOVEMENT_TYPE_ZOOM_MINUS;
                }
                else {
                    _zoom_movement = CONST_MOVEMENT_TYPE_ZOOM_MORE;
                }
        }

        DivPosition_Transform($("#div_background"), _zoom_movement, CONST_MOVEMENT_MOUSE);
        //jQuery("#div_background").css("-webkit-transform", "50% 50%");

        return false;
    });

});                              // end ready

function RestoreTerminals() {
    $("div[id*=" + CONST_ID_TERMINAL + "]").css('background-color', 'white');
    $("div[id*=" + CONST_ID_TERMINAL + "]").html("");
    //$("div[id*=" + CONST_ID_TERMINAL + "]").removeClass('blink_me');
}

function GetAlarmsToSound() {
    var _legends_list = [];

    $("#div_legend_container_42002 .chkAlarmsSound.chkAlarmsSoundEnabled").each(function () {
        var _l_id = $(this).data("soundlegendid")
        _legends_list[_l_id] = _l_id;
    });

    $("#div_legend_container_41001 .chkAlarmsSound.chkAlarmsSoundEnabled").each(function () {
        var _l_id = $(this).data("soundlegendid")
        _legends_list[_l_id] = _l_id;
    });

    return _legends_list;
}

function UpdateTerminals() {
    var _date = DateTimeToday();
    var _new_color;
    var _alarms_machine_count = 0;
    var _alarms_player_count = 0;
    var _custom_alarms_count = 0;
    var _active_filter = m_current_filter[m_current_section];
    var _legend_values;
    var _alarms_collection;
    //var _monitor_alarms_blink = [];
    var _alarm_sound = false;

    if (m_mouse_actions_mousemove_before_mousedown) {
        m_update_terminals_pending = true;

        return;
    }

    if (_Configuration.ShowHeatMap) {
        _Manager.HeatMap.ClearData();
    }

    _Manager.Labels.Items = [];

    if (m_alarms_filter_sounds_changed) {
        m_monitor_alarms_blink = GetAlarmsToSound();
        _Configuration.AlarmsToSound = m_monitor_alarms_blink;
        m_config_has_changed = true;
        SaveConfigurationDB(false);
        m_alarms_filter_sounds_changed = false;
    }

    //if (!m_popup_infoWindow_is_clicked && !m_div_background_is_clicked && !m_movements_timeout) {
    for (_idx in _Manager.Terminals.Items) {
        // Create Label
        var _label = new WSILabel();

        if (_Manager.Terminals.Items[_idx].Properties) {
            _alarms_machine_count += _Manager.Terminals.Items[_idx].Properties.alarms_machine_count;
            _alarms_player_count += _Manager.Terminals.Items[_idx].Properties.alarms_player_count;
        }
        //        //TODO: Revisar LabelBuilder ya que ahora no esta implementado.
        //------------------------------------------------------------------------------------
        switch (m_current_section.toString()) {
            case Enum.EN_SECTION_ID.ALARMS:
            case Enum.EN_SECTION_ID.ALARMS_PLAYERS:
            case Enum.EN_SECTION_ID.ALARMS_MACHINES:

                if (_active_filter && m_section_alarms_all != true) {
                    _legend_values = _Legends.Check(_active_filter, _Manager.Terminals.Items[_idx].Properties);
                } else if (m_section_alarms_all) {
                    _legend_values = _Legends.CheckAlarmsByPriority(_active_filter, _Manager.Terminals.Items[_idx].Properties);
                }

                break;

            case Enum.EN_SECTION_ID.MONITOR:
                if (_active_filter) {
                    _legend_values = _Legends.CheckCustomFilter(_active_filter, _Manager.Terminals.Items[_idx].Properties);
                }

                break;

            case Enum.EN_SECTION_ID.PLAYERS:
                if (m_filter_mode_enabled) {
                    if (m_filter_list_selected.length > 0 && m_apply_filter) {
                        _legend_values = _Legends.CheckCustomFilter(m_filter_list_selected, "black", _Manager.Terminals.Items[_idx].Properties);
                    }
                } else {
                    if (_active_filter) {
                        _legend_values = _Legends.Check(_active_filter, _Manager.Terminals.Items[_idx].Properties);
                    }
                }
                break;

            case Enum.EN_SECTION_ID.MACHINES:
                if (m_filter_mode_enabled) {
                    if (m_filter_list_selected.length > 0 && m_apply_filter) {
                        _legend_values = _Legends.CheckCustomFilter(m_filter_list_selected, "black", _Manager.Terminals.Items[_idx].Properties);
                    }
                } else {
                    if (_active_filter) {
                        _legend_values = _Legends.Check(_active_filter, _Manager.Terminals.Items[_idx].Properties);
                    }
                }

                break;
            default:
                if (_active_filter) {
                    _legend_values = _Legends.Check(_active_filter, _Manager.Terminals.Items[_idx].Properties);
                }
                break;
        }

        //------------------------------------------------------------------------------------
        if (_legend_values != null) {
            _new_color = _legend_values.color;

            _Manager.Terminals.Items[_idx].CurrentColor = _new_color;
            _Manager.Terminals.Items[_idx].CurrentValue = _legend_values.value;
            _Manager.Terminals.Items[_idx].CurrentProperty = _legend_values.prop;

            $("#" + CONST_ID_TERMINAL + _idx).html("");
            $("#" + CONST_ID_TERMINAL + _idx).css("background-image", "");
            $("#" + CONST_ID_TERMINAL + _idx).css("background-position", "center");
            $("#" + CONST_ID_TERMINAL + _idx).css("background-size", "100% 100%");

            if (_new_color !== $("#" + CONST_ID_TERMINAL + _idx).css("background-color")) {
                $("#" + CONST_ID_TERMINAL + _idx).css("background-color", _new_color);
            }

            if (_label.Icon) {
                $("#" + CONST_ID_TERMINAL + _idx).append(_label.Icon);
            }

            $("#" + CONST_ID_LABEL + _idx).css("background-color", _new_color);
            $("#" + CONST_ID_LABEL + _idx).css("border-color", _label.BorderColor);
            $("#" + CONST_ID_LABEL + _idx).css("color", _label.TextColor);
            $("#" + CONST_ID_LABEL + _idx).html(_label.Text);
        } else {
            _Manager.Terminals.Items[_idx].CurrentColor = "#fff";
            _Manager.Terminals.Items[_idx].CurrentValue = 0;
            _Manager.Terminals.Items[_idx].CurrentProperty = undefined;
        }

        //Return bool if the alarm must to sound.
        if (!_alarm_sound && m_monitor_alarms_blink.length > 0) {
            _alarm_sound = _Legends.CheckAlarmsSound(m_monitor_alarms_blink, _Manager.Terminals.Items[_idx].Properties);
        }

        if (_Manager.HeatMap.CanUpdate()) {
            // Update Heatmap
            _Manager.HeatMap.AddDataByTerminal(_Manager.Terminals.Items[_idx]);
        }

        if (WSIData.OnFloorActivity) {
            // Slot Label
            UpdateSlotLabel(_Manager.Terminals.Items[_idx]);
        }

        $("#" + CONST_ID_LABEL + _idx).css("visibility", (_label.hasError || $("#chkShowLabels:checked").length != 0 ? "visible" : "hidden"));

    } // for

    if (_Configuration.ShowHeatMap) {
        _Manager.HeatMap.Update();
    }

    UpdateLastTimeUpdated();

    if ((_alarms_machine_count + _alarms_player_count) > 0) {
        $("#monitorAlarmsNotification").show();
    } else {
        $("#monitorAlarmsNotification").hide();
    }

    m_section_first_time = false;

    if (_alarm_sound && _Configuration.SoundEnabled) {
        $("#sound_alarm")[0].load();
        $("#sound_alarm")[0].play();
    } else {
        $("#sound_alarm")[0].pause();
        if ($("#sound_alarm")[0].readyState > 0) {
            $("#sound_alarm")[0].currentTime = 0;
        }
    }

} // UpdateTerminals

//
function UpdateSlotLabel(Terminal) {
    $("#" + CONST_ID_LABEL + _idx).css('text-align', 'center');
    var _left = (Terminal.Properties.lop_x * 5) - 5;
    var _top = (Terminal.Properties.lop_z * 5) - 5;

    var _btop = "none";
    var _bbott = "none";
    var _bleft = "none";
    var _bright = "none";

    var _align = "right";

    switch (Terminal.Properties.lop_orientation) {
        case 0:
            _top += 23;
            _left -= 5;
            break;
        case 1:
        case 45:
            _top += 17;
            _left -= 23;
            break;
        case 2:
        case 90:
            _left -= 27;
            break;
        case 3:
        case 135:
            _top -= 13;
            _left -= 23;
            break;
        case 4:
        case 180:
            _top -= 20;
            _left -= 5;
            break;
        case 5:
        case 225:
            _top -= 13;
            _left += 13;
            break;
        case 6:
        case 270:
            _left += 20;
            break;
        case 7:
        case 315:
            _top += 17;
            _left += 13;
            break;
    }

    var _new_div;

    _new_div = document.getElementById(CONST_ID_SLOT_LABEL + Terminal.Properties.lo_id);

    if (!_new_div) {
        _new_div = document.createElement("div");

        $(_new_div).attr("id", CONST_ID_SLOT_LABEL + Terminal.Properties.lo_id)
            .addClass("div_terminal")
            .css("top", _top + "px")
            .css("left", _left + "px")
            .css("height", "25px")
            .css("width", "25px")
            .css("background-color", "transparent")
            .css("border-top", _btop)
            .css("border-bottom", _bbott)
            .css("border-left", _bleft)
            .css("border-right", _bright)
            .css("text-align", _align)
            .css("z-index", m_zindex_terminal)
            .css("font", "3.2pt Arial");

        $("#div_center").append($(_new_div));
    }

    var _net_win = Round(Terminal.Properties.net_win);
    var _img_background_visibility = $("#img_background").css("visibility");

    var _html = '';
    _html += '<span style="color: ' + (_img_background_visibility == "visible" ? 'black' : 'white') + ';">' + Round(Terminal.Properties.played_amount) + "$" + '</span><br>';
    _html += '<span style="color: ' + (_img_background_visibility == "visible" ? 'black' : 'white') + ';">' + Round(Terminal.Properties.won_amount) + "$" + '</span><br>';
    _html += '<span style="color: ' + (_net_win < 0 ? 'red' : 'green') + ';">' + _net_win + "$" + '</span>';

    $(_new_div).html(_html);

}

function CreateDivAreas() {
    for (var _i = 0; _i < m_areas_positions.length; _i++) {
        var _new_div;

        _new_div = document.createElement("div");
        $(_new_div).addClass("div_area")
            .attr("id", CONST_ID_AREA + _i)
            .css("left", (m_areas_positions[_i][0] * 5) - 5 + "px")
            .css("top", (m_areas_positions[_i][1] * 5) - 5 + "px")
            .css("width", (m_areas_positions[_i][2] * 20) + "px")
            .css("height", (m_areas_positions[_i][3] * 20) + "px");

        $(_new_div).click(function (e) {
            AreaClickEvent(e, this);
        });

        $("#div_center").append($(_new_div));

    }
}  // CreateDivAreas

function CreateDivIslands() {
    for (var _i = 0; _i < m_islands_positions.length; _i++) {
        var _new_div;

        _new_div = document.createElement("div");
        $(_new_div).addClass("div_island")
            .attr("id", CONST_ID_ISLAND + _i)
            .css("left", (m_islands_positions[_i][0] * 5) - 10 + "px")
            .css("top", (m_islands_positions[_i][1] * 5) - 10 + "px")
            .css("width", (m_islands_positions[_i][2] * 20) + "px")
            .css("height", (m_islands_positions[_i][3] * 20) + "px");

        $(_new_div).click(function (e) {
            IslandClickEvent(e, this);
        });

        $("#div_center").append($(_new_div));

    }
} // CreateDivIslands


function CreateTerminal(Terminal, callbackLabelBuilder) {
    var _css_transform;
    var _terminal_rotate_deg;
  try {
      //SL 1/11 - RETIRED TERMINALS NOT DRAW IN FLOOR
      if (Terminal.Properties.status != 2) {
        _css_transform = GetPropertyName_Transform();

        var _new_div;

        // Get object location to capture de offset
        var _loc = _CurrentFloor3.GetByField(EN_OBJECT_LOCATION_FIELDS.lo_external_id, Terminal.Properties.lo_external_id);

        _new_div = document.createElement("div");
        $(_new_div).addClass("div_terminal")
                .attr("id", CONST_ID_TERMINAL + Terminal.Properties.lo_id)
                .css("top", ((Terminal.Properties.lop_z * 5) + parseFloat((_loc[EN_OBJECT_LOCATION_FIELDS.lol_offset_y]).replace(",", "."))) + "px")
                .css("left", ((Terminal.Properties.lop_x * 5) + parseFloat((_loc[EN_OBJECT_LOCATION_FIELDS.lol_offset_x]).replace(",", "."))) + "px")
                .css("z-index", m_zindex_terminal);
        //.css("background", "url(images/t_0.png) no-repeat");


        //$(_new_div).click(function (e) {
        $(_new_div).mouseup(function (e) {

          if (WSIData.Activity && m_current_section != 80) {
            var _terminal = _Manager.Terminals.GetTerminal($(this).attr("id").replace(CONST_ID_TERMINAL, ""));
            //if (_terminal && (_terminal.Properties.play_session_status == 2 || _terminal.HasError())) {
            if (_terminal) {
              TerminalClickEvent(e, this, false);

              // Design integration
              OpenInfoPanel();
            }
          }
        });

        _terminal_rotate_deg = GetDegrees(Terminal.Properties.lop_orientation);
        $(_new_div).css(_css_transform, "rotate(" + _terminal_rotate_deg + ")");

        $("#div_center").append($(_new_div));

        callbackLabelBuilder(Terminal);
      }
        
    } catch (e) { throw new Error("CreateTerminal") }
}

function CreateTerminalLabel(Terminal) {
    //    var _css_transform;
    //    var _terminal_rotate_deg;

    //    _css_transform = GetPropertyName_Transform();

    //    var _new_label;

    //    _new_label = document.createElement("p");

    //    $(_new_label).addClass("labels")
    //                .attr("id", CONST_ID_LABEL + Terminal.Properties.lo_id)
    //                .css("top", (Terminal.Properties.lop_z * 10) + "px")
    //                .css("left", (Terminal.Properties.lop_x * 10) - 8 + "px")
    //                .css("border", "none")
    //                .text(Math.round(Math.random() * 100) + "$")
    //                .css("z-index", 3);

    //    $("#div_center").append($(_new_label));
}

function CreateDivTerminales() {

    _Manager.Terminals.Build(CreateTerminal, CreateTerminalLabel);

} // CreateDivTerminales


//Position: 0, 1, 2, 3, 4, 5, 6, 7
function GetDegrees(Position) {
    var _degrees;

    switch (+Position) {
        case 0:
            _degrees = 0; break;
        case 1:
            _degrees = 45; break;
        case 2:
            _degrees = 90; break;
        case 3:
            _degrees = 135; break;
        case 4:
            _degrees = 180; break;
        case 5:
            _degrees = 225; break;
        case 6:
            _degrees = 270; break;
        case 7:
            _degrees = 315; break;
        default:
            //_degrees = 0;
            _degrees = Position;
    }
    return _degrees + "deg";

} // GetDegrees

function CreateDivRoulettes() {
    /*
    var _css_transform;
    var _roulette_rotate_deg;
  
    var m_roulettes_positions;
    m_roulettes_positions = [ //posX, posY, width, height, Orientation
    [780, 0, 120, 80, 0],
    [980, -0, 120, 80, 4]
    ];
  
    _css_transform = GetPropertyName_Transform();
  
    for (var _i = 0; _i < m_roulettes_positions.length; _i++) {
    var _new_div;
  
    _new_div = document.createElement("div");
    $(_new_div).addClass("div_roulette")
    .attr("id", "roulette_" + _i)
    .css("left", (m_roulettes_positions[_i][0]) + "px")
    .css("top", (m_roulettes_positions[_i][1]) + "px")
    .css("width", (m_roulettes_positions[_i][2]) + "px")
    .css("height", (m_roulettes_positions[_i][3]) + "px")
    .css("background", "url(images/m_roulette.png) no-repeat");
  
    $(_new_div).click(function (e) {
    RouletteClick(e, this);
    });
  
    _roulette_rotate_deg = GetDegrees(m_roulettes_positions[_i][4]);
    $(_new_div).css(_css_transform, "rotate(" + _roulette_rotate_deg + ")");
  
    $("#div_center").append($(_new_div));
    }
    */
} //CreateDivRoulettes

function RouletteClick(e, Roulette) {

    alert($(Roulette).attr("id"));
} // RouletteClick


function CreateDivGamingtables() {
    /*
    var _css_transform;
    var m_gamingtables_positions;
    var _gamingtables_rotate_deg;
  
    m_gamingtables_positions = [ //posX, posY, width, height, size, , Orientation
    [680, -200, 80, 80, "big", 0],
    [680, -300, 80, 80, "big", 0],
    [780, -100, 60, 40, "small", 7],
    [980, -100, 60, 40, "small", 1]
    ];
  
    _css_transform = GetPropertyName_Transform();
  
    for (var _i = 0; _i < m_gamingtables_positions.length; _i++) {
    var _new_div;
  
    _new_div = document.createElement("div");
    $(_new_div).addClass("div_gamingtable")
    .attr("id", "gamingtable_" + _i)
    .css("left", (m_gamingtables_positions[_i][0]) + "px")
    .css("top", (m_gamingtables_positions[_i][1]) + "px")
    .css("width", (m_gamingtables_positions[_i][2]) + "px")
    .css("height", (m_gamingtables_positions[_i][3]) + "px");
    if (m_gamingtables_positions[_i][4] == "small") {
    $(_new_div).css("background", "url(images/m_BJ.png) no-repeat");
    }
    else {
    $(_new_div).css("background", "url(images/m_PK.png) no-repeat");
    }
  
    $(_new_div).click(function (e) {
    GamingtableClick(e, this);
    });
  
    _gamingtables_rotate_deg = GetDegrees(m_gamingtables_positions[_i][5]);
    $(_new_div).css(_css_transform, "rotate(" + _gamingtables_rotate_deg + ")");
  
    $("#div_center").append($(_new_div));
    }
    */
} //CreateDivGamingtables

function GamingtableClick(e, Gamingtable) {

    alert($(Gamingtable).attr("id"));
} // GamingtableClick


function browserDetect() {

    var nAgt = navigator.userAgent;
    var verOffset, ix;
    // IE is default
    m_browser = navigator.appName;

    // Opera
    if ((verOffset = nAgt.indexOf("Opera")) != -1) {
        m_browser = "Opera";
    }
        //  Chrome
    else if ((verOffset = nAgt.indexOf("Chrome")) != -1) {
        m_browser = "Chrome";
    }
        //  Safari
    else if ((verOffset = nAgt.indexOf("Safari")) != -1) {
        m_browser = "Safari";
    }
        // Firefox
    else if ((verOffset = nAgt.indexOf("Firefox")) != -1) {
        m_browser = "Firefox";
    }

}

function DivPosition_Move(Div, Left, Top, Zoom, TransformOrigin) {
    var _css_value;

    WSILogger.Write("Transform: " + "scale(" + Zoom + ")" + "translate(" + Left + "px, " + Top + "px)");

    _css_value = "scale(" + Zoom + ")" + "translate(" + Left + "px, " + Top + "px)";

    if (TransformOrigin != null) {
        Div.css(GetPropertyName_TransformOrigin(), TransformOrigin);
    }
    Div.css(GetPropertyName_Transform(), _css_value);
} // DivPosition_Move

function DivPosition_Transform(Div, Action, MovementOrigin, Force) {

    if ((CONST_MOVEMENT_PINCH || CONST_MOVEMENT_ROTATE) && !Force) {
        return;
    }

   
    var menuPrincipal = $('.menuPrincipal');
    var menuSuperior = $('.headerTop');

    var offsetCenterLeft = menuPrincipal.width();
    var offsetCenterTop = menuSuperior.height();

    var menuFiltro = $('.subMenu');

    var filterMenuHidden = menuFiltro.hasClass("slideLeft");

    if (!filterMenuHidden)
      offsetCenterLeft += menuFiltro.width();

    
  
    var centerStartW = (window.innerWidth + offsetCenterLeft) / m_current_zoom / 2;
    var centerStartH = (window.innerHeight + offsetCenterTop) / m_current_zoom / 2;
    
  

    var offsetW = centerStartW - m_current_background_position_left;
    var offsetH = centerStartH - m_current_background_position_top;

    var _temp;
    switch (Action) {
        case CONST_MOVEMENT_TYPE_RIGTH: m_current_background_position_left -= CONST_MOVEMENT_PX; break;
        case CONST_MOVEMENT_TYPE_LEFT: m_current_background_position_left += CONST_MOVEMENT_PX; break;
        case CONST_MOVEMENT_TYPE_UP: m_current_background_position_top -= CONST_MOVEMENT_PX; break;
        case CONST_MOVEMENT_TYPE_DOWN: m_current_background_position_top += CONST_MOVEMENT_PX; break;
        case CONST_MOVEMENT_TYPE_ZOOM_MORE:
            switch (true) {
                case MovementOrigin === CONST_MOVEMENT_BUTTON:
                    _temp = CONST_MOVEMENT_ZOOM_PX;
                    break;
                case MovementOrigin === CONST_MOVEMENT_MOUSE:
                    _temp = CONST_MOVEMENT_ZOOM_WHEEL_PX;
                    break;
                case MovementOrigin === CONST_MOVEMENT_TOUCH:
                    _temp = CONST_MOVEMENT_ZOOM_TOUCH_PX;
                    break;
                default: break;
            }
            if (+m_current_zoom + _temp <= CONST_MAX_ZOOM) {
                m_current_zoom = +m_current_zoom + _temp;
            }
            UpdateSlider();
            break;

        case CONST_MOVEMENT_TYPE_ZOOM_MINUS:
            switch (true) {
                case MovementOrigin === CONST_MOVEMENT_BUTTON:
                    _temp = CONST_MOVEMENT_ZOOM_PX;
                    break;
                case MovementOrigin === CONST_MOVEMENT_MOUSE:
                    _temp = CONST_MOVEMENT_ZOOM_WHEEL_PX;
                    break;
                case MovementOrigin === CONST_MOVEMENT_TOUCH:
                    _temp = CONST_MOVEMENT_ZOOM_TOUCH_PX;
                    break;
                default: break;
            }
            if (+m_current_zoom - _temp >= CONST_MIN_ZOOM) {
                m_current_zoom = +m_current_zoom - _temp;
            }
            UpdateSlider();
            break;

        default: break;
    }

    
    var centerW = (window.innerWidth + offsetCenterLeft) / m_current_zoom / 2;
    var centerH = (window.innerHeight + offsetCenterTop) / m_current_zoom / 2;
      
    m_current_background_position_left = centerW - offsetW;
    m_current_background_position_top = centerH  - offsetH;

    DivPosition_Move(Div, m_current_background_position_left, m_current_background_position_top, m_current_zoom, CONST_TRANSFORM_ORIGIN);

} // DivPosition_Transform

function IslandClickEvent(e, IslandDiv) {

    alert($(IslandDiv).attr("id"));
}


function AreaClickEvent(e, AreaDiv) {

    alert($(AreaDiv).attr("id"));
}


(function ($) {
    // Menu Functions
    $(document).ready(function () {

        // previously click
        $('#openner').mousedown(function (e) {

            var _r = $('#menu_rigth').css("right");

            if (_r == "0px") {
                $('#menu_rigth').css({
                    'right': '-400px',
                    'top': '0px'
                });
                $('#openner').css({
                    'right': '0px',
                    'top': '100px'

                });

                $('#openner').css('background', 'url(images/m_more.png) no-repeat');

            }
            else {
                $('#menu_rigth').css({
                    'right': '0px',
                    'top': '0px'
                });
                $('#openner').css({
                    'right': '400px',
                    'top': '100px'
                });

                $('#openner').css('background', 'url(images/m_less.png) no-repeat');
            }

            e.preventDefault();
        });

    });
})(jQuery);


////////////////////

function SetLabels(checkbox) {
    var _visibility;

    if (checkbox.checked) {
        _visibility = "visible";
    } else {
        _visibility = "hidden";
    }

    for (_idx in _Manager.Terminals.Items) {
        $("#" + CONST_ID_LABEL + _idx).css("visibility", _visibility);
    }

    //    for (var _i = 0; _i < _Manager.Terminals.Items.length; _i++) {
    //        $("#" + CONST_ID_LABEL + _i).css("visibility", _visibility);
    //    }
} // SetLabels


function SetControls(checkbox) {
    var _visibility;

    if (checkbox.checked) {
        _visibility = "visible";
    } else {
        _visibility = "hidden";
    }

    $("#pnlNavigation").css("visibility", _visibility);
}

//
//
//
function SetMachineSize(control) {
    $(".div_terminal").css("height", control.value);
    $(".div_terminal").css("width", control.value);
}

function ResetScene() {
    $("#div_center").empty();
}


function InitializeScene() {

    //
    CreateDivTerminales();

    //CreateDivBeacons();
    CreateDivRunners();

    //    if (_Configuration.ShowHeatMap) {
    //        _Manager.CreateHeatMap();
    //    }

    // Initialize UI
    //_UI.InitControls();

    WSILogger.Write("LoadLayout complete");

    //// Update dashboard on floor change
    //if (m_floor_changed) {
    //  m_changing_floor = false;
    //}

    LoadEnd()
}

function LoadEnd() {
    // Initialize UI
    _UI.InitFloorControls();

    WSILogger.Write("LoadLayout complete");
    m_floor_loaded = true;
    m_floor_changed = false;

    UpdateTerminals();


    if (m_loading_dialog) {
        m_loading_dialog.dialog("close");
    }

    $("#loading_floor").hide();

    setTimeout(function () { }, 2000);

    // Remove and execute all items in the array
    while (m_function_queue.length > 0) {
        (m_function_queue.shift())();
    }
}


function HiddenDiv(Div) {
    //$(Div).css("visibility","hidden");
    $(Div).removeClass("div_popup_terminal ui-widget-content");
    $(Div).addClass("div_popup_terminal_hidden");
    $("#popup_Top10Best2").css("display", "none");

    if (m_terminal_id_selected) {
        UnselectTerminalDiv();
    }
}

function TerminalClickEvent(e, TerminalDiv, UpdateHistorical) {
    var _info;
    var _popup_div_principal;
    var _terminal;
    var _label;
    var idTerminal = $(TerminalDiv).attr("id");

    if (idTerminal == undefined) {
        var _terminal_div = $(TerminalDiv);
        idTerminal = _terminal_div["selector"].replace("#", "");
        _terminal = _Manager.Terminals.GetTerminal(idTerminal);
    }
    else
        _terminal = _Manager.Terminals.GetTerminal(idTerminal.replace(CONST_ID_TERMINAL, ""));

    if (_terminal) {

        TerminalDivSelected(idTerminal);
        m_terminal_id_selected_old = m_terminal_id_selected;
        m_terminal_id_selected = idTerminal;

        m_account_id_selected_old = m_account_id_selected;
        m_account_id_selected = _terminal.Properties.player_account_id;
        if ($("#popup_account_id").text() == "") {
            m_account_id_selected = null;
        }

        UpdatePopupTerminal(e, _terminal);

        if (UpdateHistorical) {
            UpdatePopupTerminalHistorical(e, TerminalDiv);
        }

        //UpdatePopupAlarms(_terminal);

        $("#popup_div_principal").removeClass("div_popup_terminal_hidden");
        $("#popup_div_principal").addClass("div_popup_terminal ui-widget-content ui-widget");

        $("#popup_Top10Best2").css("display", "");

        var _div_img_icoSnapshot;
        //change class
        if (m_terminal_id_selected != m_terminal_id_selected_old) {
            _div_img_icoSnapshot = $("div[class*=icoSnapshot_undo]");
            $(_div_img_icoSnapshot).each(function () {

                if ($(this).attr('type') == 'terminal') {
                    $(this).removeClass("icoSnapshot_undo");
                    $(this).addClass("icoSnapshot");
                }
            });

        }
        if (m_account_id_selected != m_account_id_selected_old) {
            _div_img_icoSnapshot = $("div[class*=icoSnapshot_undo]");

            $(_div_img_icoSnapshot).each(function () {
                if ($(this).attr('type') == 'client') {
                    $(this).removeClass("icoSnapshot_undo");
                    $(this).addClass("icoSnapshot");
                }
            });
        }
    }

} //TerminalClickEvent

function RunnerClickEvent(e, TerminalDiv, UpdateHistorical) {
    var _info;
    var _popup_div_principal;
    var _terminal;
    var _label;
    var _runner;
    var Selected;
    var _runner_x, _runner_y;

    var _runner_div = $(e.currentTarget);
    var _runner_id = _runner_div[0].id.split("_")[1];


    for (_idx_runner in _Manager.Runners.Items) {
        if (_Manager.Runners.Items[_idx_runner].id == _runner_id) {
            _runner = _Manager.Runners.Items[_idx_runner];

            break;
        }
    }

    //COMPROBAR
    if (_runner) {
        _runner_x = ((_runner.z * 5) - 2) + "px";
        _runner_y = ((_runner.x * 5) - 2) + "px"

        _t_runners = $(".div_runner").filter(function () {

            var self = $(this);
            return self.css('top') === _runner_x &&
                   self.css('left') === _runner_y;
        });

        if (+_t_runners.text() > 1) {
            GetRunnersFromPosition(_runner.x, _runner.y);

        } else {
            LocateRunner(_runner);
        }
    }
} //RunnerClickEvent

function GetRunnersFromPosition(PositionX, PositionY) {
    var _runner;
    var _list_runner = [];
    for (_idx_runner in _Manager.Runners.Items) {
        _runner = _Manager.Runners.Items[_idx_runner];

        if (_runner.x == PositionX && _runner.y == PositionY) {
            _list_runner[_idx_runner] = _runner;
        }
    }

    OpenRunnerDialog(_list_runner);
}

function OnSelectedRunnerDialog(RunnerId) {

    $("#div_runner_selector").hide();

    LocateRunner(_Manager.Runners.Items[RunnerId]);
}

function LocateRunner(Runner) {
    var _runner = Runner;
    var Selected;

    if (_runner) {

        if (Selected == undefined) { Selected = true; }

        // Unselect actual terminal
        if (m_terminal_id_selected != null) {

            $($("#" + m_terminal_id_selected))
            .css('border', '1px solid black')
            .css('z-index', m_zindex_terminal);

            if ($($("#" + m_terminal_id_selected)).attr("selected")) {
                $($("#" + m_terminal_id_selected))
                .css("top", ($("#" + m_terminal_id_selected).attr('initial_top')))
                .css("left", ($("#" + m_terminal_id_selected).attr('initial_left')))
                .removeAttr("selected");
            }
        }

        // Unselect actual terminal
        if (m_runner_id_selected != null && m_runner_id_selected != _runner.id) {

            $($("#runner_" + m_runner_id_selected))
            .css('border', '1px solid black')
            .css('z-index', '2');

            if ($($("#runner_" + m_runner_id_selected)).attr("selected")) {
                $($("#" + m_runner_id_selected))
                .css("top", ($("#runner_" + m_runner_id_selected).attr('initial_top')))
                .css("left", ($("#runner_" + m_runner_id_selected).attr('initial_left')))
                .removeAttr("selected");
            }
        }

        // Select/Unselect terminal
        if ($("#runner_" + _runner.id).length > 0) {
            SelectTerminal($("#runner_" + _runner.id), Selected);
            m_runner_id_selected = _runner.id;

            if (_runner) {
                UpdatePopupRunner(_runner);
            }

        } else {
            // Does not exist the Terminal?
        }

        OpenInfoPanel();
    }

}

function CreateGrid(Size) {
    var _i;
    var _height = $("#div_background").height();
    var _width = $("#div_background").width();
    var _ratioW = Math.floor(_width / Size);
    var _ratioH = Math.floor(_height / Size);

    for (_i = 0; _i <= _ratioW; _i++) {  // vertical grid lines
        $('<div />').css({
            'top': 1,
            'left': _i * Size,
            'width': 1,
            'height': _height
        })
        .addClass('gridlines')
        .appendTo($("#div_background"));
    }
    for (_i = 0; _i <= _ratioH; _i++) { // horizontal grid lines
        $('<div />').css({
            'top': 1 + _i * Size,
            'left': 0,
            'width': _width,
            'height': 1
        })
        .addClass('gridlines')
        .appendTo($("#div_background"));
    }

    $('.gridlines').show();

    if (_Configuration.ShowGrid) {
        $('.gridlines').css("visibility", "visible");
    }
    else {
        $('.gridlines').css("visibility", "hidden");
    }
}  // CreateGrid

function UnselectTerminalDiv(Selected) {
    TerminalDivSelected(m_terminal_id_selected, false);
    m_terminal_id_selected = null;
}

function SelectTerminal(Terminal, Selected) {

    if (Selected) {
        // Only mark as select if it is unselected
        if (!$(Terminal).attr("selected")) {
            var _final_left;
            var _final_top;

            _final_left = $(Terminal).css("left").replace("px", "") - 10 + "px";
            _final_top = $(Terminal).css("top").replace("px", "") - 10 + "px";

            // Mark terminal as select
            //
            $(Terminal)
            .css('border', '10px solid yellow')
            .css('border-bottom-style', '10px dotted yellow')
            .css('z-index', m_zindex_terminal_selected)
            .attr('initial_left', $(Terminal).css("left"))
            .attr('initial_top', $(Terminal).css("top"))
            .attr('selected', '1')
            .css('left', (_final_left))
            .css('top', (_final_top));
            //.css('background', 'url(../iconos/localizar.svg)');
        }
    }
    else {
        // Only mark as unselect if it is select
        if ($(Terminal).attr("selected")) {
            //
            $(Terminal)
            .css('border', '1px solid black')
            .css('border-bottom-style', '1px dotted black')
            .css('z-index', m_zindex_terminal)
            .css('left', ($(Terminal).attr("initial_left")))
            .css('top', ($(Terminal).attr("initial_top")))
            .removeAttr("selected");
        }
    }
} //SelectTerminal


function TerminalDivSelected(TerminalId, Selected) {

    if (Selected == undefined) { Selected = true; }

    var _term_div = TerminalId;

    // Unselect actual terminal
    if (m_terminal_id_selected != null && _term_div != m_terminal_id_selected) {

        $($("#" + m_terminal_id_selected))
        .css('border', '1px solid black')
        .css('z-index', m_zindex_terminal);

        if ($($("#" + m_terminal_id_selected)).attr("selected")) {
            $($("#" + m_terminal_id_selected))
            .css("top", ($("#" + m_terminal_id_selected).attr('initial_top')))
            .css("left", ($("#" + m_terminal_id_selected).attr('initial_left')))
            .removeAttr("selected");
        }
    }

    // Select/Unselect terminal
    if ($("#" + _term_div).length > 0) {
        SelectTerminal($("#" + _term_div), Selected);
    } else {
        // Does not exist the Terminal?
    }

}

function SetZoom(Value) {
    //m_current_zoom = 1 - Value;
    m_current_zoom = Value / 100;
    DivPosition_Move($("#div_background"), m_current_background_position_left, m_current_background_position_top, m_current_zoom, CONST_TRANSFORM_ORIGIN);
}

function UpdateSlider() {
    try {
        $("#sldZoom").slider("value", m_current_zoom * 100);

    } catch (err) { }

}

//////////////////////////////////////////////////////////////////////////////////////

function GotoTerminal(sender, byType, showPopup) {
    // MV - 2016-07-12 - Changed signature to enable calling from widgets
    ShowTerminalInfo(sender);
}

function LocateTerminal(object, context) {

    ShowTerminalInfo(object);
}



function GotoRunner(Runner) {

  
  var _point = _Manager.RunnersMap.RunnersById[Runner];

  var _runner = new Object();
  _runner.x = _Manager.RunnersMap.convertXfrom2D(_point.x);
  _runner.y = _Manager.RunnersMap.convertYfrom2D(_point.y);

  m_current_zoom = 1.8;
    
  m_current_background_position_top = ((window.innerHeight / m_current_zoom) / 2) - _runner.y; 
  m_current_background_position_left = ((window.innerWidth / m_current_zoom) / 2) - _runner.x; 
    
  DivPosition_Move($("#div_background"), m_current_background_position_left, m_current_background_position_top, m_current_zoom, "0% 0%");

  m_runner_id_selected = Runner;

  OnRunnerClick(Runner);

  OpenInfoPanel();

}

function LocateRunner(context, object) {

    GotoRunner(object)
}


//////////////////////////////////////////////////////////////////////////////////////
function ShowTerminalInfo(Sender) {
    var _object_id = 0
    if (!Sender.Properties)
        _object_id = Sender;
    else
        _object_id = Sender.Properties.lo_id;

    ShowTerminalFromObjectId(_object_id);
    var _terminal_div = $("#" + CONST_ID_TERMINAL + _object_id);
    var TerminalSelected = _terminal_div.offset();

    var _top = TerminalSelected.top - 70;
    var _left = TerminalSelected.left - 70;
  //CONST_INITIAL_ZOOM
  //se compenta porque hace desaparecer el mapa, SDS 07-03-2017
   // DivPosition_Move($("#div_background"), -_left, -_top, 1, CONST_TRANSFORM_ORIGIN);
    TerminalClickEvent(null, _terminal_div, false);
    OpenInfoPanel();
}

function ShowTerminalFromCustomer(AccountId) {
    var _object_id;

    try {
        _object_id = _Manager.Terminals.ByAccount[AccountId];
        ShowTerminalFromObjectId(_object_id);
        $('#jugadoresListaON').click();
    }
    catch (err) { }
} // ShowTerminalFromCustomer

//////////////////////////////////////////////////////////////////////////////////////

function ShowTerminalFromObjectId(ObjectId) {
    var _terminal_div;

    try {

        if (ObjectId) {
            _terminal_div = $("#" + CONST_ID_TERMINAL + ObjectId);
            if ($(_terminal_div).length > 0) {
                // open div "Activity"
                //$("#ribbon-tab-header-0").click();
                //ChangeRibbonTab("tabs", 0, CONST_RIBBON_REALTIME);

                SetDivBackgroundToInitialPosition();

                // move popup
                m_current_popup_background_position_left = 0;
                m_current_popup_background_position_top = 0;
                var _css_value = "translate(" + m_current_popup_background_position_left + "px, " + m_current_popup_background_position_top + "px)";
                var _css_transform = GetPropertyName_Transform();
                jQuery("#popup_infoWindow").css(_css_transform, _css_value);

                //ClickEvent
                TerminalClickEvent(null, _terminal_div, false);
                TerminalDivSelected($(_terminal_div).attr("id"), true);
                m_terminal_id_selected = $(_terminal_div).attr("id");

            }
        }
    }
    catch (err) {
        WSILogger.Log("ShowTerminalFromObjectId", err);
    }

    return;
} // ShowTerminalFromObjectId

function SetDivBackgroundToInitialPosition() {
    ////m_current_background_position_left = -1 * (CONST_DIV_BACKGROUND_WIDTH / 2);
    ////m_current_background_position_top = -1 * (CONST_DIV_BACKGROUND_HEIGHT / 2);

    //// Adjust location
    //m_current_background_position_left += (50 / +CONST_INITIAL_ZOOM);
    ////m_current_background_position_top += (70 / +CONST_INITIAL_ZOOM);

    //// Initial position
    //DivPosition_Move($("#div_background"), m_current_background_position_left, m_current_background_position_top, m_current_zoom, CONST_TRANSFORM_ORIGIN);
    var _cf_x, _cf_y;

    _cf_x = -_CurrentFloor3.dimensions.x;
    _cf_y = -_CurrentFloor3.dimensions.y;

    $("#div_heatmapArea").css('top', _cf_y / 2);
    $("#div_heatmapArea").css('left', _cf_x / 2);

    $("#div_runnersmapArea").css('top', _cf_y / 2);
    $("#div_runnersmapArea").css('left', _cf_x / 2);

    //// Zoom Out
    //m_current_zoom = CONST_INITIAL_ZOOM;
    //DivPosition_Move($("#div_background"), m_current_background_position_left, m_current_background_position_top, m_current_zoom, CONST_TRANSFORM_ORIGIN);
    //UpdateSlider();
    onResetZoom_2D();

} // SetDivBackgroundToInitialPosition
//////////////////////////////////////////////////////////////////////////////

// Check if zoom must be disabled or allow it
function CheckZoomDissabled() {
    return ((m_filter_mode_enabled && $("#div_dialog_background_filter_mode").css("display") != "none") || m_custom_alarm_edit_mode || m_current_section == 30 || m_current_section == 10);

} // CheckZoomDissabled

//////////////////////////////////////////////////////////////////////////////

function CreateBeacon(Beacon) {
    var _new_div;

    _new_div = document.createElement("div");
    $(_new_div).addClass("div_beacon")
            .attr("id", CONST_ID_BEACON + Beacon.id)
            .css("top", (Beacon.z * 5) - 5 + "px")
            .css("left", (Beacon.x * 5) - 5 + "px")
            .css("z-index", 3);

    $(_new_div).mousedown(function (e) {
        if (WSIData.Activity) {
            var _runner = _Manager.Runners.FindItem($(this).attr("id").replace(CONST_ID_TERMINAL, ""));
            //if (_terminal && (_terminal.Properties.play_session_status == 2 || _terminal.HasError())) {
            if (_runner) {
                RunnerClickEvent(e, this, false);

                // Design integration
                //OpenInfoPanel();
            }
        }
    });

    $("#div_center").append($(_new_div));
}

//////////////////////////////////////////////////////////////////////////////

function CreateRunner(Runner) {
    var _new_div;
    var _runner_x, _runner_y;
    var _t_runners;


    if (m_runner_id_selected != null) {
        //Mira si el seleccionado tiene más
        _runner_x = ((Runner.z * 5) - 2) - 10 + "px";
        _runner_y = ((Runner.x * 5) - 2) - 10 + "px"

        _t_runners = $(".div_runner").filter(function () {

            var self = $(this);
            return self.css('top') === _runner_x &&
                   self.css('left') === _runner_y;
        });

        //miramos el resto
        if (_t_runners.length == 0) {
            _runner_x = (Runner.z * 5) - 2 + "px";
            _runner_y = (Runner.x * 5) - 2 + "px"
        }
    } else {
        _runner_x = (Runner.z * 5) - 2 + "px";
        _runner_y = (Runner.x * 5) - 2 + "px"
    }

    _t_runners = $(".div_runner").filter(function () {

        var self = $(this);
        return self.css('top') === _runner_x &&
               self.css('left') === _runner_y;
    });

    if (_t_runners.length != 0) {
        var _num = +_t_runners.text();
        if (_num == 0) {
            _num = 1
        };
        _num += 1;
        _t_runners.text(_num);

    } else {
        /// 
        _new_div = document.createElement("div");

        if (m_runner_id_selected == Runner.id) {
            $(_new_div).addClass("div_runner")
            .attr("id", CONST_ID_RUNNER + Runner.id)
            .css("top", (Runner.z * 5) - 2 + "px")
            .css("left", (Runner.x * 5) - 2 + "px")
            .css("z-index", 3);


            var _final_left = $(_new_div).css("left").replace("px", "") - 10 + "px";
            var _final_top = $(_new_div).css("top").replace("px", "") - 10 + "px";
            var _background_color = (Runner.source == "1") ? "#3366ff" : "#ff00ff";

            $(_new_div)
            .css('border', '10px solid yellow')
            .css('border-bottom-style', '10px dotted yellow')
            .css('z-index', '5')
            .attr('initial_left', $(_new_div).css("left"))
            .attr('initial_top', $(_new_div).css("top"))
            .attr('selected', '1')
            .css('left', (_final_left))
            .css('background-color', _background_color)
            .css('top', (_final_top));



        } else {
            var _background_color = (Runner.source == "1") ? "#3366ff" : "#ff00ff";
            $(_new_div).addClass("div_runner")
                .attr("id", CONST_ID_RUNNER + Runner.id)
                .css("top", (Runner.z * 5) - 2 + "px")
                .css("left", (Runner.x * 5) - 2 + "px")
                .css('background-color', _background_color)
                .css("z-index", 3);
        }

        $(_new_div).click(function (e) {
            RunnerClickEvent(e);
        });

        $("#div_center").append($(_new_div));
    }
}

//////////////////////////////////////////////////////////////////////////////

function CreateDivBeacons() {
    if (WSIData.ShowBeacons) {
        _Manager.Beacons.Build(CreateBeacon);
    }
}

//////////////////////////////////////////////////////////////////////////////

function CreateDivRunners() {
    if (WSIData.ShowRunners) {
        _Manager.Runners.Build(CreateRunner);
    }
}

function RunnersMapClick(event) {


}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
