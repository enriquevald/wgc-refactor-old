﻿/////////////////////////////////////////////////////////////////////////////////////

var _Manager = new WSIManager();
/////////////////////////////////////////////////////////////////////////////////////
var m_browser;


/* test ///////////////////////////////////////////////////////////////////////*/


function fire(e) { }

function touchHandler(event) {
  var touches = event.changedTouches,
        first = touches[0],
        type = "";

  switch (event.type) {

    case "touchstart": type = "mousedown"; break;
    case "touchmove": type = "mousemove"; break;
    case "touchend": type = "mouseup"; break;
    default: return;
  }

  var simulatedEvent = document.createEvent("MouseEvent");
  simulatedEvent.initMouseEvent(type, true, true, window, 1,
                          first.screenX, first.screenY,
                          first.clientX, first.clientY, false,
                          false, false, false, 0/*left*/, null);

  first.target.dispatchEvent(simulatedEvent);
  event.preventDefault();
}

function init() {

  CreateCharts("tabs");
}


/* //////////////////////////////////////////////////////////////////*/

function CreateCharts(DivRibbon) {

  var _html = "";

  try {

    _html += '<span class="ribbon-window-title"></span>';

    _html += '	<ul  role="tablist" style="display:none">';    
    _html += '		<li  ><div class="tab-charts"></div><a href="#tabs-Charts" id="Charts" data-nls="NLS_CHARTS" onclick="SelectRibbon(\'' + CONST_RIBBON_REPORT_CHARTS + '\')"></a></li>'; 
    _html += '	</ul>'; 
    // ******************** CHARTS SECTION *************************************
    _html += '	<div id="tabs-Charts" class="tabs-content-max" >';
    _html += '      <div id="div_report_chart_frame_" class="ribbon-backstage">';
    _html += '          <div  id="div_report_chart_menu">';
    _html += '          </div>';
    _html += '          <div  id="div_report_chart_reports">';
    _html += '          </div>';
    _html += '      </div>';
    _html += '	</div>';


    // Catch the ribbon container  
    $("#" + DivRibbon).html(_html);
    $("#tabs").tabs();


  } catch (err) {
    WSILogger.Log("CreateRibbonToolbar()", err);
  }

  //TODO Move function to Cenital.aspx :
  //CreateReportCharts();
  //    UpdateReportCharts();
  //    FillReportCharts();

} //CreateCharts



//////////////////////////////////////////////

// ready
$(document).ready(function () {


  browserDetect();

  init();

  //////////////////////////////////////////////////////////////


});                              // end ready


function UpdateTerminals() {
  var _date = DateTimeToday();

  for (_idx in _Manager.Terminals.Items) {
  
    // Update occupation data
    _Manager.Terminals.UpdateOccupationData(_date, _Manager.Terminals.Items[_idx]);

  } // for
   
  //// Set last time configuration was updated
  //UpdateLastTimeUpdated();
} // UpdateTerminals

//
function InitializeScene() {
  // Apply loaded configuration
  ApplyDefaultValues();
}
