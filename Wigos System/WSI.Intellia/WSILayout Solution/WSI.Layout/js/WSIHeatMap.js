﻿////////////////////////////////////////////////////////////////////////////////////////////////
// Functions to set/get and update HeatMap
// * requires JQUERY
// * requires WSIUserConfiguration
// * requires HeatMap.JS
// * requires WSILogger
////////////////////////////////////////////////////////////////////////////////////////////////

// TODO: RMS - Review window resize to adap heatmap size

// Main pseudo-class
function WSIHeatMap() {
  this.Data = {
    max: 1,
    data: []
  };;
  this.Height = null;
  this.Width = null;
  this.PlaneHeight = null;
  this.PlaneWidth = null;
  this.HeatMap3D = null;
  this.visible = false;
  this.Control = null;
  this.Colors = undefined;
  this.Updating = false;
  this.visible = false;
}

// Method that creates the object and ensure target component exists
WSIHeatMap.prototype.Create = function (Width, Height) {
  WSILogger.Write("HeatMap - Creating...");

  var _plane = null;
  // To obtain HeatMap size
  _plane = document.getElementById('img_background');
  if (!_plane) { _plane = document.getElementById('div_background'); }

  if (!_plane) {
    WSILogger.Write("HeatMap - No plane to create!");
    return;
  }

  // HeatMap target for 2D
  var _hmap = document.getElementById("div_heatmapArea");

  this.Width = Width ? Width : $(_plane).width();
  this.Height = Height ? Height : $(_plane).height();
  this.PlaneWidth = $(_plane).width();
  this.PlaneHeight = $(_plane).height();
  // Set Size
  $(_hmap).width(this.PlaneWidth);
  $(_hmap).height(this.PlaneHeight);

  this.EnsureControlCreated();

  // 3D
  if (_LayoutMode == 1) {
    // Create material for the object
    WSILogger.Write("Heatmap - 3D - Creating material...");
    var _mat = null;
    _mat = new THREE.MeshBasicMaterial({ color: 0xffffff, ambient: 0xffffff, shininess: 0, transparent: true });

    var _3d_width, _3d_height;

    _3d_width = PixelsToUnits(_CurrentFloor3.dimensions.x) * 4;
    _3d_height = PixelsToUnits(_CurrentFloor3.dimensions.y) * 4;

    this.Width = PixelsToUnits(_CurrentFloor3.dimensions.x) * 4;
    this.Height = PixelsToUnits(_CurrentFloor3.dimensions.y) * 4;

    //this.HeatMap3D = new THREE.Mesh(new THREE.PlaneGeometry(this.Width, this.Height), _mat);
    this.HeatMap3D = new THREE.Mesh(new THREE.PlaneBufferGeometry(_3d_width, _3d_height), _mat);
    this.HeatMap3D.userData = { width: _3d_width, height: _3d_height };
    this.HeatMap3D.position.y = 12;
    this.HeatMap3D.rotation.x = -Math.PI / 2;
    this.HeatMap3D.updateMatrix();
    this.HeatMap3D.material.needsUpdate = true;
    this.HeatMap3D.material.transparent = true;
    this.HeatMap3D.name = "HeatMap_plane";

    // Hide div
    //$("#div_heatmapArea").css("display", "none");

    WSILogger.Write("Heatmap - 3D - Adding to Scene");
    WSI.scene.add(_Manager.HeatMap.HeatMap3D);
  }

}

WSIHeatMap.prototype.ClearData = function () {
  delete this.Data
  this.Data = {
    max: 1,
    data: []
  };

  if (this.Control) {
    this.Control.clear();
  }
}

// Initializes de object on each update
WSIHeatMap.prototype.Initialize = function (Width, Height) {
  WSILogger.Write("Heatmap - Initializing");

  this.ClearData();

  this.EnsureControlCreated();

  var activeFilter = m_current_filter[m_current_section];
  //var activeFilter = m_current_filter[70];

  if (activeFilter) {
    // Update heatmap legend colors
    if (m_current_section != Enum.EN_SECTION_ID.MONITOR) {
      this.Colors = _Legends.GetLegendColors(activeFilter);
      this.Control.set("gradient", this.Colors);
      this.Control.initColorPalette();
    }
  } else {
    //throw "No active filter!";
  }

  // Set Height and Width
  if (Width) {
    if (typeof Width === "string") {
      this.Width = +(Width.replace("px", ""));
      this.Height = +(Height.replace("px", ""));
    } else {
      this.Width = Width;
      this.Height = Height;
    }
  }
}

// Forces all the update process
WSIHeatMap.prototype.ForcedUpdate = function () {
  WSILogger.Write("Heatmap - Forcing update!");
  var _terminal = null;
  this.Initialize();
  for (var _terminal_id in _Manager.Terminals.Items) {
    _terminal = _Manager.Terminals.Items[_terminal_id];
    this.AddDataByTerminal(_terminal);
  }
  this.Update();
}

//
WSIHeatMap.prototype.CanUpdate = function () {
  return (m_current_section == 60 || m_current_section == 70);
}

//
WSIHeatMap.prototype.GetValueByColor = function (Color) {
  if (this.Colors) {
    for (var _i in this.Colors) {
      if (Color == this.Colors[_i]) {
        return _i;
      }
    }
  } else {
    return 0;
  }
}

// Add data to the heatmap of a single Terminal
WSIHeatMap.prototype.AddDataByTerminal = function (Terminal) {
  //WSILogger.Write("Heatmap - Adding terminal data...");

    if (Terminal.CurrentProperty == undefined || m_field_properties[Terminal.CurrentProperty] == undefined || m_field_properties[Terminal.CurrentProperty].type != Enum.EN_FIELD_TYPE.CURRENCY) {
    return;
  }

  var _rc = Terminal.Front();

  if (_LayoutMode == 1) {
    // 3D
    var _rx = ((Terminal.Properties.lop_x) * this.PlaneWidth) / this.Width;
    var _ry = ((Terminal.Properties.lop_z) * this.PlaneHeight) / this.Height;
    var _o_x = Math.round(this.PlaneWidth / 2);
    var _o_z = Math.round(this.PlaneHeight / 2);

    var _x = _o_x + _rx;
    var _z = _o_z + _ry;

    if (!WSIData.snapHeatmapToTerminal) {
      _x = _x + (-_rc.z * WSIData.displacementFactor3D);
      _z = _z + (-_rc.x * WSIData.displacementFactor3D);
    }

  } else {
    // 2D
    var _o_x = Math.round(this.PlaneWidth / 2);
    var _o_z = Math.round(this.PlaneHeight / 2);

    var _x = _o_x + (Terminal.Properties.lop_x * 5) + 5;
    var _z = _o_z + (Terminal.Properties.lop_z * 5) + 5;

    if (!WSIData.snapHeatmapToTerminal) {
      _x = _x + (-_rc.z * WSIData.displacementFactor2D);
      _z = _z + (-_rc.x * WSIData.displacementFactor2D);
    }

  }

  var _value = Terminal.CurrentValue;

  if (Terminal.CurrentColor != "white") {
    _value = this.GetValueByColor(Terminal.CurrentColor);
  }

  // Obatin values from legends
  // var _legend_values = _Legends.Check(m_current_filter[m_current_section], Terminal.Properties);

  //if (_legend_values) {
  //  // Set value
  //  _value = _legend_values.value;
  //}

  if (_value > 0) {
    // Insert data
    this.Data.data.push({ x: _x, y: _z, count: _value });
  }
}

// Update the location (vertical) for 3D mode
WSIHeatMap.prototype.UpdateLocation = function () {
  WSILogger.Write("Heatmap - Updating location...");
  var _y_offset = (WSIData.ModelTypes == 2) ? 9 : 12;
  this.HeatMap3D.position.y = _y_offset;
}

// Method that re-create the heatmap control if necessary
WSIHeatMap.prototype.EnsureControlCreated = function () {
  if (_LayoutMode == 2) {
    // 2D Recreate control
    WSILogger.Write("HeatMap - 2D - disposing...");
    $("#div_heatmapArea").empty();
    this.Control = null;
  }
  if (this.Control == null) {
    WSILogger.Write("HeatMap - recreating control...");
    var _hmap = document.getElementById("div_heatmapArea");
    var _rad = _LayoutMode == 1 ? WSIData.radius3D : WSIData.radius2D;
    this.Control = h337.create({ "element": _hmap, "radius": _rad, "visible": true });
  }
}

// Simple update control and target component
WSIHeatMap.prototype.Update = function () {

  this.Updating = true;

  if (this.Data.data.length == 0) {
    // totally transparent if there is no data
    WSILogger.Write("Heatmap - No data!");
    this.Data = { max: 0, data: [{ x: 0, y: 0, count: 0 }] };
  }

  try {

    WSILogger.Write("HeatMap - Updating...");

    if (this.Data != null) {

      //      // -------------------------------------
      //      // DELETE
      //      var _debug = JSON.stringify(this.Data);
      //      WSILogger.Write(_debug);
      //      this.Data = {
      //        max: 1,
      //        data: [{ x: 0, y: 0, count: 100}]
      //      }
      //      // -------------------------------------

      WSILogger.Write("HeatMap - Setting data...");

      this.Control.store.setDataSet(this.Data);
    }

    if (_LayoutMode == 1) { // 3D

      if (this.HeatMap3D == null) {
        WSILogger.Write("HeatMap - Object not created!");
        return;
      }

      // Height of heatmap is different
      this.UpdateLocation();

      // Update material
      this.HeatMap3D.material.transparent = true;

      // Re-create material      
      var _canvas = this.Control.get("canvas");

      //
      //var _url = _canvas.toDataURL();
      //

      var _texture = new THREE.Texture(_canvas);
      _texture.minFilter = THREE.LinearFilter;
      _texture.magFilter = THREE.LinearFilter;
      _texture.needsUpdate = true;
      this.HeatMap3D.material.map = _texture;
    }

  } catch (ex) {
    WSILogger.Log("HeatMap.Update", ex);
  }

  this.Updating = false;
}

// Set the HeatMap component visibility
WSIHeatMap.prototype.SetVisibility = function (Visible) {
  WSILogger.Write("Heatmap - Setting visibility = [" + Visible + "]");
  if (Visible) {
    $("#div_heatmapArea").show();
    this.ForcedUpdate();
  }
  if (_LayoutMode == 2 && !Visible) {
    $("#div_heatmapArea").empty();
    $("#div_heatmapArea").hide();
    this.Control = null;
  }

  this.visible = Visible;
  if (this.HeatMap3D) {
      if (_LayoutMode == 1 && this.CanUpdate()) {
          this.HeatMap3D.visible = Visible;
      } else {
          this.HeatMap3D.visible = false;
      }
  }
}

