﻿Raphael.fn.donutChart = function (cx, cy, r, rin, values, labels, options) {
  var paper = this,
      rad = Math.PI / 180,
      chart = this.set();

  if (!options) { options = {}; }

  function sector(cx, cy, r, startAngle, endAngle) {

    var x1 = cx + r * Math.cos(-startAngle * rad),
        x2 = cx + r * Math.cos(-endAngle * rad),
        y1 = cy + r * Math.sin(-startAngle * rad),
        y2 = cy + r * Math.sin(-endAngle * rad),
        xx1 = cx + rin * Math.cos(-startAngle * rad),
        xx2 = cx + rin * Math.cos(-endAngle * rad),
        yy1 = cy + rin * Math.sin(-startAngle * rad),
        yy2 = cy + rin * Math.sin(-endAngle * rad);

    return { "x1": x1, "x2": x2, "y1": y1, "y2": y2, "xx1": xx1, "xx2": xx2, "yy1": yy1, "yy2": yy2, "startAngle": startAngle, "endAngle": endAngle };

    //return paper.path(["M", xx1, yy1,
    //                   "L", x1, y1,
    //                   "A", r, r, 0, +(endAngle - startAngle > 180), 0, x2, y2,
    //                   "L", xx2, yy2,
    //                   "A", rin, rin, 0, +(endAngle - startAngle > 180), 1, xx1, yy1, "z"]
    //                 ).attr(params);

  }

  var angle = 0,
      total = 0,
      start = 0,
      yCircleBorder = cy + r,
      process = function (j, drawLabel, hideZeros, column) {
        var value = values[j],
            // RMS
            angleplus = 359.95 * value / total,  // replaced 360 value for a zero value support in array
            //popangle = angle + (angleplus / 2),
            //popangle = p_data.startAngle + ((p_data.endAngle - p_data.startAngle) / 2),
            //color = Raphael.hsb(start, .75, 1),
            ms = 500,
            delta = 0, // 5
            //bcolor = Raphael.hsb(start, 1, 1),
            //_params = { fill: "80-" + bcolor + "-" + color, "stroke-width": 0 };
            _params = { fill: options.colors[j], "stroke-width": 0 };
        p_data = sector(cx, cy, r, angle, angle + angleplus);

        if (isNaN(p_data.x1)) { return; } // Uninitialized values control
        if (isNaN(angleplus)) { return; }

        var p = paper.path(["M", p_data.xx1, p_data.yy1,
                       "L", p_data.x1, p_data.y1,
                       "A", r, r, 0, +(p_data.endAngle - p_data.startAngle > 180), 0, p_data.x2, p_data.y2,
                       "L", p_data.xx2, p_data.yy2,
                       "A", rin, rin, 0, +(p_data.endAngle - p_data.startAngle > 180), 1, p_data.xx1, p_data.yy1, "z"]
                     ).attr(_params);

        if (drawLabel && (!hideZeros || value > 0)) {

          //var txt = paper.text(cx + (r + delta) * Math.cos(-popangle * rad), cy + (r + delta + 25) * Math.sin(-popangle * rad), labels[j]).attr({ fill: 'white', stroke: "none", opacity: 1, "font-size": options.fontSize });
          //var popangle = p_data.startAngle + ((p_data.endAngle - p_data.startAngle) / 2);
          //var txt = paper.text(cx + (r + delta) * Math.cos(-popangle * rad), cy + (r + delta + 25) * Math.sin(-popangle * rad), labels[j]).attr({ fill: 'white', stroke: "none", opacity: 1, "font-size": options.fontSize });

          //var circ = paper.circle(20, 10 + (15 * j), 7).attr({ fill: options.colors[j], stroke: "none" });
          var ajusteX = 0.1;
          var txt, val = null; 
          if (options.tabletScreen) {
            var positionX,
                positionY = yCircleBorder + 15,
                deltaY = j;
            if (column % 2 == 0) {//Columna de la izquierda
              positionX = (cx) * ajusteX;
              if (_Language.get(labels[j]) == "")
                txt = paper.text(positionX, positionY, labels[j]).attr({ fill: options.colors[j], stroke: "none", opacity: 1, "font-size": options.fontSize, "font-family": options.fontFamily, 'text-anchor': 'start'});
              else
                txt = paper.text(positionX, positionY, _Language.get(labels[j])).attr({ fill: options.colors[j], stroke: "none", opacity: 1, 'data-nls': labels[j], "font-size": options.fontSize, "font-family": options.fontFamily, 'text-anchor': 'start' });

              val = paper.text(positionX, positionY, FormatString(value, EN_FORMATS.F_NUMBER, 0)).attr({ fill: options.colors[j], stroke: "none", opacity: 1, "font-size": options.fontSize, "font-family": options.fontFamily, 'text-anchor': 'end' });

              txt.translate(0, ((txt.getBBox().height / 2) * (column)));
              val.translate(80, ((val.getBBox().height / 2) * (column)));
            } else { //Columna de la derecha
              var totalWidth = cx * 2;
              positionX = totalWidth - ((cx) * 0.10);
              deltaY = j - 1;
              if (_Language.get(labels[j]) == "")
                txt = paper.text(cx + ((cx) * 0.10), positionY, labels[j]).attr({ fill: options.colors[j], stroke: "none", opacity: 1, "font-size": options.fontSize, "font-family": options.fontFamily, 'text-anchor': 'start'});
              else
                txt = paper.text(cx + ((cx) * 0.10), positionY, _Language.get(labels[j])).attr({ fill: options.colors[j], stroke: "none", opacity: 1, "font-size": options.fontSize, "font-family": options.fontFamily, 'text-anchor': 'start', 'data-nls': labels[j] });

              val = paper.text(positionX, positionY, FormatString(value, EN_FORMATS.F_NUMBER, 0)).attr({ fill: options.colors[j], stroke: "none", opacity: 1, "font-size": options.fontSize, "font-family": options.fontFamily, 'text-anchor': 'end' });

              txt.translate(0, ((txt.getBBox().height / 2) * (column - 1)));
              val.translate(0, ((val.getBBox().height / 2) * (column - 1)));
            }
            
          } else {
            var positionY = yCircleBorder - 10;
            if (!options.showTotal) {
              positionY = yCircleBorder + 10;
            }
            if (_Language.get(labels[j]) == "")
              txt = paper.text(options.marginRight - 50, positionY, labels[j]).attr({ fill: options.colors[j], stroke: "none", opacity: 1, "font-size": options.fontSizeDesktop, "font-family": options.fontFamily, 'text-anchor': 'end'});
            else
              txt = paper.text(options.marginRight - 50, positionY, _Language.get(labels[j])).attr({ fill: options.colors[j], stroke: "none", opacity: 1, "font-size": options.fontSizeDesktop, "font-family": options.fontFamily, 'text-anchor': 'end', 'data-nls': labels[j], 'class':labels[j] });

            val = paper.text(options.marginRight, positionY, FormatString(value, EN_FORMATS.F_NUMBER, 0)).attr({ fill: options.colors[j], stroke: "none", opacity: 1, "font-size": options.fontSizeDesktop, "font-family": options.fontFamily, 'text-anchor': 'end' });
            var deltaYDesktop = ((txt.getBBox().height));
            var valuesLenght;
            if (hideZeros) {
              valuesLenght = values.filter(function (elem, i, array) {
                return elem != 0;
              }).length;
            } else {
              valuesLenght = values.length;
            }
            txt.translate(0, -((valuesLenght - column) * deltaYDesktop));
            val.translate(0, -((valuesLenght - column) * deltaYDesktop));
          }
          column++;
        }


        //if (options.drawLabels) {
        //  var txt = paper.text(cx + (r + delta) * Math.cos(-popangle * rad), cy + (r + delta + 25) * Math.sin(-popangle * rad), labels[j]).attr({ fill: options.colors[j], stroke: "none", opacity: 0, "font-size": options.fontSize });
        //}

        //p.mouseover(function () {
        //  p.stop().animate({ transform: "s1.1 1.1 " + cx + " " + cy }, ms, "elastic");
        //  if (txt) { txt.stop().animate({ opacity: 1 }, ms, "elastic"); }

        //}).mouseout(function () {
        //  p.stop().animate({ transform: "" }, ms, "elastic");
        //  if (txt) { txt.stop().animate({ opacity: 0 }, ms); }
        //});

        angle += angleplus;
        chart.push(p);

        if (options.drawLabels) {
          //chart.push(circ);
          chart.push(txt);
          chart.push(val);
        }

        start += .1;
        return column;
      };
  // empty circle
  var _oldcolor = options.colors[0];
  options.colors[0] = "#646464";
  total = 100;
  values.splice(0, 0, total);
  process(0, false, false, 0);
  values.splice(0, 1);
  options.colors[0] = _oldcolor;
  // circles by values
  total = 0;
  for (var i = 0, ii = values.length; i < ii; i++) {
    total += values[i];
  }
  var col = 0
  for (i = 0; i < ii; i++) {
    col = process(i, options.drawLabels, options.hideZeros, col);
  }

  if (!options.tabletScreen && options.showTotal) {
    var txt = paper.text(options.marginRight - 50, yCircleBorder, options.labelTotal).attr({ fill: options.colorTotal, stroke: "none", opacity: 1, "font-size": options.fontSizeDesktop, "font-family": options.fontFamily, 'text-anchor': 'end' });
    var val = paper.text(options.marginRight, yCircleBorder, FormatString(total, EN_FORMATS.F_NUMBER, 0)).attr({ fill: options.colorTotal, stroke: "none", opacity: 1, "font-size": options.fontSizeDesktop, "font-family": options.fontFamily, 'text-anchor': 'end' });
    val.translate(0, -val.getBBox().height / 2);
    txt.translate(0, -txt.getBBox().height / 2);

    chart.push(txt);
    chart.push(val);
  } else if (options.showTotal) {
    var positionX,
        positionY = yCircleBorder + 15,
        deltaY = ii;
    positionX = (cx) * 0.10;
    var txt = paper.text(positionX, positionY, options.labelTotal).attr({ fill: options.colorTotal, stroke: "none", opacity: 1, "font-size": options.fontSize, "font-family": options.fontFamily, 'text-anchor': 'start' });
    var val = paper.text(positionX, positionY, FormatString(total, EN_FORMATS.F_NUMBER, 0)).attr({ fill: options.colorTotal, stroke: "none", opacity: 1, "font-size": options.fontSize, "font-family": options.fontFamily, 'text-anchor': 'end' });
    txt.translate(0, ((txt.getBBox().height / 2) * (deltaY)));
    val.translate(80, ((val.getBBox().height / 2) * (deltaY)));
  }

  return chart;
};