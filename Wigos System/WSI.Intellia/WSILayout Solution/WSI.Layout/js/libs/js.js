function animaPanelControl(){
	if($('.panelControl').hasClass('show')){
		$( ".btnSlide, .panelControl" ).animate({
          left: "+=95%"
		  })
		$('.panelControl').removeClass('show').addClass('hide');
		$('.btnSlide').removeClass('slideShow').addClass('slideHide');
	}
	else {   	
	    	$( ".btnSlide, .panelControl" ).animate({
          	left: "-=95.5%"
		  	});
		  $('.panelControl').removeClass('hide').addClass('show');
		  $('.btnSlide').removeClass('slideHide').addClass('slideShow');    
        };
	return false;
}

function animaMenuComparar() {
	if($("#capturaON").is(':checked')){
		$(".menuComparar").css("display", "block");
        $( ".menuComparar" ).animate({
          	right: "+=100%"
		  	});
		$( ".btnSlide, .panelControl" ).animate({
          	left: "-=95.5%"
		  	});
		  $('.panelControl').removeClass('hide').addClass('show');  
		  $('.btnSlide').removeClass('slideHide').addClass('slideShow');
		  $('.btnCompararTerminales').css("display", "block");
		  $('.btnAgregarTerminales').css("display", "none");      
        
		 }else {
			 
			$( ".menuComparar" ).animate({
				right: "-=100%"
				}, 700, function (){
					$(".menuComparar").css("display", "none");
					});
			if($('.wrapContenidoComparar').hasClass('hide')){
				
				$( ".wrapContenidoComparar" ).animate({
					right: "-=106%"
				}, 700, function (){
					$(".wrapContenidoComparar").css("display", "none");
					});
				
				$('.wrapContenidoComparar').removeClass('hide').addClass('show');	
			}
	}
}

function mostrarComparaciones(){
	if($('.wrapContenidoComparar').hasClass('show')){
			$(".wrapContenidoComparar").css("display", "block");
	    	$( ".wrapContenidoComparar" ).animate({
          		right: "+=106%"
		  	});
		  $('.wrapContenidoComparar').removeClass('show').addClass('hide');
		  $('.btnCompararTerminales').css("display", "none");
		  $('.btnAgregarTerminales').css("display", "block");
     }
}

function dejarComparar() {
	if($('.wrapContenidoComparar').hasClass('hide')){
	    	$( ".wrapContenidoComparar" ).animate({
          		right: "-=106%"
		  	}, 700, function (){
				$(".wrapContenidoComparar").css("display", "none");
				});
			
			$( ".menuComparar" ).animate({
          	right: "-=100%"
		  	}, 700, function (){
				$(".menuComparar").css("display", "none");
				});
			
		  $('.wrapContenidoComparar').removeClass('hide').addClass('show');
		  $('#capturaON').attr('checked', false);
		  $('.btnCompararTerminales').css("display", "block");
		  $('.btnAgregarTerminales').css("display", "none");
		  
        } else {
			$( ".menuComparar" ).animate({
          	right: "-=100%"
		  	});
		
			 $('#capturaON').attr('checked', false);
		}
}


//function mostrarListaJugadores(){
//	if($("#jugadoresListaON").is(':checked')){ 
//		$(".wrapJugadores").css("display", "block");
//		$( ".wrapJugadores" ).animate({
//          	right: "+=100%"
//		  	});
//		$( ".btnSlide, .panelControl" ).animate({
//          	left: "-=95.5%"
//		  	});
//		  $('.panelControl').removeClass('hide').addClass('show');  
//		  $('.btnSlide').removeClass('slideHide').addClass('slideShow');
//		  $('.wrapJugadores').removeClass('show').addClass('hide');        
//	} else {
			
//			$( ".wrapJugadores" ).animate({
//          	right: "-=100%"
//		  	}, 700, function() {
//			$(".wrapJugadores").css("display", "none");
//			});
//	  }
//}

//function cerrarListaJugadores() {
//		$( ".wrapJugadores" ).animate({
//          	right: "-=100%"
//		  }, 700, function() {
//			  $(".wrapJugadores").css("display", "none");
//			  });
		  
//		  $('#jugadoresListaON').attr('checked', false);
//}


////JBC Terminales
//function mostrarListaTerminales() {
//  if ($("#terminalesListaON").is(':checked')) {
//    $(".wrapJugadores").css("display", "block");
//    $(".wrapJugadores").animate({
//      right: "+=100%"
//    });
//    $(".btnSlide, .panelControl").animate({
//      left: "-=95.5%"
//    });
//    $('.panelControl').removeClass('hide').addClass('show');
//    $('.btnSlide').removeClass('slideHide').addClass('slideShow');
//    $('.wrapJugadores').removeClass('show').addClass('hide');
//  } else {

//    $(".wrapJugadores").animate({
//      right: "-=100%"
//    }, 700, function () {
//      $(".wrapJugadores").css("display", "none");
//    });
//  }
//}

//function cerrarListaTerminales() {
//  $(".wrapJugadores").animate({
//    right: "-=100%"
//  }, 700, function () {
//    $(".wrapJugadores").css("display", "none");
//  });

//  $('#terminalesListaON').attr('checked', false);
//}


function slideConfig(){
	$( ".configuracionProfile" ).slideToggle( "slow")
	//$(".configuracionProfile").slideToggle().animate({"margin-bottom": "toggle"});
	$(".configBtn").toggleClass("close");
}

function slideMessage() {
  animaPanelDerechaExtendido();
  var _MessageManager = MessageManager.getInstance();
  _MessageManager.InitConversation('', '');
  $(".messageBtn").toggleClass("close");
}

$(document).ready(function(){
 	
	//Primer animación del Panel de Control
	$(".panelControl").addClass("show");
	animaPanelControl();
	
	//Acción del boton de Slide del Panel de Control
  	$(".btnSlide a").click(function(){
		animaPanelControl();
	});
	
//	// Tweak para el height del acordeon
//    $( ".accordion" ).accordion({
//		 heightStyle: "content"
//	});
  	
	//Acción del modo comparar
	$("#capturaON").change(function() {
    	animaMenuComparar ();
	});
	
	//Slide panel donde carga info de comparaciones
	$(".btnCompararTerminales a").click(function(){
		mostrarComparaciones();
	});
	
	//Acción para dejar de comparar
	$(".btnDejarComparar a").click(function(){
		dejarComparar();
	});
	
	//Muestra oculata el panel de configuración
	$(".configBtn a").click(function(){
		slideConfig();
	});

  //Muestra oculata el panel de mensajes
	$(".messageBtn a").click(function () {
	  slideMessage();
	});

	
	
//	//Acción del modo lista jugadores
//	$("#jugadoresListaON").change(function() {
//    	mostrarListaJugadores ();
//	});

//  //Acción del modo lista jugadores
//$("#jugadoresCompararON").change(function () {
//    	mostrarCompararJugadores ();
//});
	
//	//Cierre lista jugadores
//	 $(".cierreLista a").click(function(){
//		 cerrarListaJugadores();
//	 });

//  //Acción del modo lista terminales
//	 $("#terminalesListaON").change(function () {
//	   mostrarListaTerminales();
//	 });
});
