﻿function WSIManager() {

  this.Grid = null;
  this.Selector = null;
  this.SelectorHelper = null;
  this.BackPlane = null;
  this.BackPlaneVisible = true;
  //this.HeatMap = null;
  this.HeatMap = new WSIHeatMap();
  //this.HeatMapVisible = true;
  //this.HeatMapControl = null;
  this.Terminals = new WSITerminals();

  this.OtherTerminals = new WSITerminals();

  this.Labels = new WSILabels();
  this.GamingTables = new WSIGamingTables();
  this.Areas = null;
  this.Banks = null;

  // this.Alarms = [];

  this.WidgetManager = null;

  this.PyramidAngles = 0;

  // Beacons
  this.Beacons = new WSIBeaconsManager();

//  Ranges
  this.Ranges = {};

  // Legends
  this.Legends = new WSILegends();

  this.Occupancy = 0;

  // Runners
  this.Runners = new WSIRunnersManager();
  this.RunnersMap = new WSIRunnersMap();

  this.GeneralParams = null;

  this.WidgetManagerFirstUpdated = false;

  if (_LayoutMode == 1) {
    this.Materials = new WSIMaterials();
    this.Geometries = new WSIGeometries();

    // Matrix to intersects test by Object_Id
    this.Objects = [];

    // Resources
    this.Resources = null;

    // Controls
    this.Controls = null;

    // Cameras
    this.Cameras = null;

    // FP light
    this.FPLight = null;
    this.FPRoom = null;

  }
}

/////////////////////////////////////////////////////////////

//WSIManager.prototype.CreateGrid = function (size, step) {
WSIManager.prototype.CreateGrid = function (Width, Height, Step) {
  var _size = (Width > Height) ? Width : Height,
      _step = (Step == undefined) ? 4 : Step;

  this.Grid = new THREE.GridHelper(_size, _step);
}

WSIManager.prototype.CreateSelector = function () {
  // Deprecated
}

// Manage the item selector
WSIManager.prototype.ShowSelector = function (x, y, z, Mesh) {

  if (this.Selector) { if (this.SelectorHelper) { this.Selector.remove(this.SelectorHelper); } }

  WSI.scene.remove(this.SelectorHelper);
  WSI.scene.remove(this.Selector);

  if (!Mesh && WSIData.SelectedObject != null) {
    Mesh = _Manager.Terminals.Items[WSIData.SelectedObject].Mesh;
  }

  if (Mesh) {
    Mesh.geometry.computeBoundingBox();

    this.Selector = new THREE.Mesh(new THREE.BoxGeometry(4, 16, 4), new THREE.MeshBasicMaterial({ color: 0xFF0000, opacity: 0.2, transparent: true, depthWrite: false, depthTest: false }));
    this.Selector.position.set(x, 8, z);

    this.SelectorHelper = new THREE.EdgesHelper(this.Selector, 0xFFFF00);
    this.SelectorHelper.material.linewidth = 1;
    this.SelectorHelper.material.depthWrite = false;
    this.SelectorHelper.material.depthTest = false;
    this.SelectorHelper.visible = true;
    this.SelectorHelper.updateMatrix();

    WSI.scene.add(this.Selector);
    WSI.scene.add(this.SelectorHelper);
  }
}

WSIManager.prototype.HideSelector = function () {
  if (this.Selector != null) {
    this.Selector.visible = false;
    this.SelectorHelper.visible = false;
  }
}

WSIManager.prototype.CreateBackPlane = function (imageUrl, width, height) {

  var _mat = null;

  if (imageUrl != null && imageUrl != "") {

    if (String(imageUrl).indexOf("data:") == 0) {
      var _image = document.createElement('img');
      _image.src = imageUrl;
      var _texture = new THREE.Texture(_image);
      _texture.needsUpdate = true;
      _mat = new THREE.MeshBasicMaterial({ color: 0xffffff, ambient: 0xffffff, shininess: 0, map: _texture, depthWrite: true, depthTest: true });
      var _plane = document.getElementById('img_background');
      _plane.src = imageUrl;
    } else {
      _mat = new THREE.MeshBasicMaterial({ color: 0xffffff, ambient: 0xffffff, shininess: 0, map: THREE.ImageUtils.loadTexture(imageUrl), depthWrite: true, depthTest: true });
    }

  } else {
    _mat = new THREE.MeshBasicMaterial({ color: 0xffffff, ambient: 0xffffff, shininess: 0, depthWrite: true, depthTest: true });
  }

  //this.BackPlane = new THREE.Mesh(new THREE.PlaneGeometry(width, height), _mat);
  this.BackPlane = new THREE.Mesh(new THREE.PlaneBufferGeometry(width, height), _mat);
  this.BackPlane.userData = { width: width, height: height };
  this.BackPlane.position.y = -0.01;
  this.BackPlane.rotation.x = -Math.PI / 2;
  //this.BackPlane.position.x -= 1000;
  //this.BackPlane.color = 0x606060;
  this.BackPlane.visible = true;
  this.BackPlane.updateMatrix();
  this.BackPlane.material.needsUpdate = true;
  this.BackPlane.name = "Background_Plane";


}

WSIManager.prototype.UpdateBackPlane = function () {

  if (_Manager.Cameras.CurrentMode == EN_CAMERA_MODE.CM_FIRST_PERSON) {
    this.BackPlane.visible = false;
    $("#swBackgroundVisibilty").addClass("control-dissabled");
    $("#swBackgroundVisibilty").children().prop("disabled", true);
  } else {
    $("#swBackgroundVisibilty").children().prop("disabled", false);
    $("#swBackgroundVisibilty").removeClass("control-dissabled");
    this.BackPlane.visible = this.BackPlaneVisible;
  }

  try {

    if (this.BackPlane != null) {
      var _canvas = document.createElement("canvas");
      var _context = _canvas.getContext("2d");
      var _plane = document.getElementById('img_background');

      if (_plane) {
        _canvas.height = _plane.height; //this.BackPlane.userData.height * 2;
        _canvas.width = _plane.width;  //this.BackPlane.userData.width * 2;

        if (this.BackPlaneVisible) {
          _context.drawImage(_plane, 0, 0, _canvas.width, _canvas.height);
          this.BackPlane.material.transparent = false;
          this.Grid.position.y = -0.5;
        } else {
          this.BackPlane.material.transparent = true;
          this.Grid.position.y = 0;
        }

        if (WSIData.OnFloorActivity) {

          // Draw Floor terminal stats
          _context.font = "bold " + WSIData.OnFloorActivityFontSize + "px Arial";

          var _x_offset = Math.round(_canvas.width / 2);
          var _y_offset = Math.round(_canvas.height / 2);

          for (var _idx in this.Terminals.Items) {
            var _terminal = this.Terminals.Items[_idx];

            var _rx = ((_terminal.Properties.lop_x) * _plane.width) / this.BackPlane.userData.width;
            var _ry = ((_terminal.Properties.lop_z) * _plane.height) / this.BackPlane.userData.height;

            _context.save();
            _context.translate(_x_offset + _rx, _y_offset + _ry);

            _context.rotate((90 + (_terminal.Properties.lop_orientation * 45)) * (Math.PI / 180));

            _context.translate(25, -10);
            _context.textAlign = 'left';
            _context.fillStyle = this.BackPlaneVisible ? 'black' : 'white';
            _context.fillText(Round(_terminal.Properties.played_amount) + "$", 0, 0);

            _context.translate(0, 11);
            _context.textAlign = 'left';
            _context.fillStyle = this.BackPlaneVisible ? 'black' : 'white';
            _context.fillText(Round(_terminal.Properties.won_amount) + "$", 0, 0);

            _context.translate(0, 11);
            _context.textAlign = 'left';
            if (Round(_terminal.Properties.net_win) < 0) {
              _context.fillStyle = 'red';
            } else {
              _context.fillStyle = this.BackPlaneVisible ? 'green' : 'lime';
            }
            _context.fillText(Round(_terminal.Properties.net_win) + "$", 0, 0);

            _context.restore();

          }
        }

        var _texture = new THREE.Texture(_canvas);
        _texture.minFilter = THREE.LinearFilter;
        _texture.magFilter = THREE.LinearFilter;
        _texture.needsUpdate = true;
        this.BackPlane.material.map = _texture;

      }
    }

  } catch (err) {
    WSILogger.Log("WSIManager.UpdateBackPlane()", err);
  }

}

WSIManager.prototype.BuildLabels = function (callbackBuilder) {
  this.Labels.Build(callbackBuilder);
}

