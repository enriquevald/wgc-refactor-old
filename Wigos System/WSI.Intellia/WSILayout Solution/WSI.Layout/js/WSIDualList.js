﻿/*

Dual List Item manager

Requires:
- jQuery
- jQUery-ui

*/

/*
  Options = {
    name: "string",
    sections: ["section1", "section2"],
    parent: "parentId",
    tabParams: { jQuery-ui tabs options },
    OnItemClick: function (Section, Item) { ... },
    OnItemDblClick: function (Section, Item) { ... },
  }
*/
var WSIDualList = function (Options) {

  // Check
  if (!Options || !Options.parent) return null;

  // Take parent
  var _parent = $(Options.parent);
  if (_parent.length == 0) return null;

  // Init
  this.name = Options.name;
  this.sections = Options.sections;
  this.parent = Options.parent;
  // Events
  this.OnItemClick = Options.OnItemClick;
  this.OnItemDblClick = Options.OnItemDblClick;

  // Draw
  var _this = "";
  _this = _this + '<div id="dual_list_' + this.name + '">';
  _this = _this + '    <ul>';
  _this = _this + '      <li><a href="#section1_' + this.name + '">' + this.sections[0] + '</a></li>';
  _this = _this + '      <li><a href="#section2_' + this.name + '">' + this.sections[1] + '</a></li>';
  _this = _this + '    </ul>';
  _this = _this + '    <div id="section1_' + this.name + '" data-section="1">';
  _this = _this + '    </div>';
  _this = _this + '    <div id="section2_' + this.name + '" data-section="2">';
  _this = _this + '    </div>';
  _this = _this + '</div>';
  _parent.append(_this);

  // Convert to tabs
  $("#dual_list_" + this.name).tabs(Options.tabParams);
}

// Adds a new item to the section Section on the dual-list
WSIDualList.prototype.AddItem = function (Section, Item) {
  // Prepare
  var _item = null;
  _item = '<button id="dual_list_item_' + this.name + '_' + Item + '" data-section="' + Section + '">' + Item + '</button>';

  // Add
  $("#section" + Section + "_" + this.name).append(_item);

  // Convert & Attatch event
  $("#dual_list_item_" + this.name + '_' + Item)
    .button()
    .click(function (evt) {
      if (this.OnItemClick) this.OnItemClick(Section, Item);
    })
    .dblclick(function (evt) {
      if (this.OnItemDblClick) this.OnItemDblClick(Section, Item);
    });
}

// Moves an item between sections
WSIDualList.prototype.MoveItem = function (Item) {
  var _item = $("#dual_list_item_" + this.name + "_" + Item);
  if (_item.length == 0) return false;
  switch (_item.data("section")) {
    case "1":
      _item.detach().appendTo("#section2_" + this.name);
      _item.data('section', '2');
      break;
    case "2":
      _item.detach().appendTo("#section1_" + this.name);
      _item.data('section', '1');
      break;
  }
  return true;
}

// Removes an item
WSIDualList.prototype.RemoveItem = function (Item) {
  var _item = $("#dual_list_item_" + this.name + "_" + Item);
  if (_item.length == 0) return false;
  _item.detach();
  return true;
}

// Clears a section
WSIDualList.prototype.ClearSection = function (Section) {
  $("#section" + Section + "_" + this.name).empty();
}

// Clears all sections
WSIDualList.prototype.ClearAll = function () {
  this.ClearSection("1");
  this.ClearSection("2");
}

