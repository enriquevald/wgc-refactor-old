﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WSICommonDateTime
// 
//   DESCRIPTION: Contains methods to manage dates and times.
// 
//        AUTHOR: Ramón Monclús
// 
// CREATION DATE: 06-JUL-2015
// 
//------------------------------------------------------------------------------

// Returns current hour
function GetCurrentHour() {
  return new Date().getHours();
}

// Convert standard date into delta date format yyyymmdd
function GetDeltaDate(DateFrom) {
  if (!DateFrom) { DateFrom = new Date(); }
  var yyyy = DateFrom.getFullYear().toString();
  var mm = (DateFrom.getMonth() + 1).toString(); // getMonth() is zero-based
  var dd = DateFrom.getDate().toString();
  return yyyy + (mm[1] ? mm : "0" + mm[0]) + (dd[1] ? dd : "0" + dd[0]); // padding
};

// Check if a date is in format yyyymmdd
function ParseDeltaDate(DeltaDate) {
  var _y = DeltaDate.substr(0, 4),
      _m = DeltaDate.substr(4, 2) - 1,
      _d = DeltaDate.substr(6, 2);
  var _D = new Date(_y, _m, _d);
  return (_D.getFullYear() == _y && _D.getMonth() == _m && _D.getDate() == _d) ? _D : 'invalid date';
}

// Calculates the difference between to dates in delta format
function DeltaDateDiff(DateStart, DateEnd) {
  var _start_date = ParseDeltaDate(DateStart),
      _end_date = ParseDeltaDate(DateEnd),
      _one_day_in_milliseconds = 1000 * 60 * 60 * 24,
      _date_diff = Math.floor((_end_date.getTime() - _start_date.getTime()) / _one_day_in_milliseconds);

  return _date_diff;
}

function GetNowDateTime() {

  //var d = new Date();
  //var time = addZero(d.getHours()) + ":" + addZero(d.getMinutes()) + ":" + addZero(d.getSeconds());
  //var _date = $.datepicker.formatDate('dd/mm/yy', new Date());

  //var _format_date = moment.localeData().longDateFormat('L');
  //var _date = moment(m_wgdb_date !== undefined ? m_wgdb_date._i : m_wgdb_date).locale(m_current_language).format(_format_date + ' HH:MM:SS');
  var _date = moment(m_wgdb_date !== undefined ? m_wgdb_date._i : m_wgdb_date).locale(m_current_language).format("DD-MM-YYYY hh:mm:ss");

  return _date;
}


function addZero(i) {
  if (i < 10) {
    i = "0" + i;
  }
  return i;
}

function GetHourNow() {

  return moment(m_wgdb_date).locale(m_current_language).format('HH:MM:SS');
}

function GetTimeSpanNow() {

  return moment(m_wgdb_date).locale(m_current_language).format('YYYYMMDDHHmmss');
}

function GetDoWValue(Date) {
  return moment(Date).locale(m_current_language).format('e');
}

function GetHourNowRange() {

  var _hour_next = moment(m_wgdb_date).locale(m_current_language).add(1, 'hours');
  //var _hour_from = new Date(new Date().getHours()),
  //    _hour_to = new Date(new Date().getHours() + 1);
  //return FormatString(new Date(new Date(0).setHours(_hour_from)), EN_FORMATS.F_TIME) + " - " + FormatString(new Date(new Date(0).setHours(_hour_to)), EN_FORMATS.F_TIME);
  return moment(m_wgdb_date).locale(m_current_language).format('HH') + "h "+ _Language.get("NLS_TO_HOUR") + " " + _hour_next.format('HH') + "h";
}

function GetDateAndHourNowRange() {

  var _time = moment(m_wgdb_date).locale(m_current_language);
  var _initialTime = _time.format('HH').toString();
  var _hour_next = _time.add(1, 'hours');
  var _finalTime = _hour_next.format('HH').toString();
  return _time.format('ddd D/M') + " " + _initialTime + "h to " + _finalTime + "h";

}