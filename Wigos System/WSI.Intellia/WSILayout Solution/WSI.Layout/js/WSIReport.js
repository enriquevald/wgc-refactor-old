﻿///////////////////////////////////////////////////////////////////////////////////////////////////////////

function WSIReportType(reportId, reportCategoryId, reportCategory, reportIndex, reportTemplate) {

  this.CommonReportOptions = {
    colors: CloneObject(m_series_default_colors),
    chart: {
      events: {
        load: function () {
        }
      }
    },

    title: {
      style: m_charts_default_title_font
    },
    subtitle: {
      style: m_charts_default_subtitle_font,
      text: ''
    },

    yAxis: {
      title: {
        style: m_charts_default_yAxis_font,
        text: ''
      }
    },

    xAxis: {
      categories: [],
      labels: {
        rotation: -90,
        useHTML: true,
        formatter: function () {
          if (this.value.length > 10) {
            var _text = this.value.substring(0, 10) + '...';
            var _indic = '<div class="has-tooltip" title="' + this.value + '">' + _text + '</div>';
            return _indic;
          }
          return this.value;
        }
      },
      title: {
        style: m_charts_default_xAxis_font,
        text: ''
      }
    },

    labels: {
      style: m_charts_default_labels_font
    }
  };

  this.id = reportId;
  this.template = this.CommonReportOptions;
  $.extend(true, this.template, CloneObject(reportTemplate));
  this.caption = reportId;
  this.subtitle = '';
  this.subtitleDate = false; // If true, subtitle is the selected date

  this.data = null;

  this.forceWidget = false;
  this.resizable = true; // to allow maximization of report

  this.gaugeVal = 0; // Value to update in solidgauge report type

  this.zoomable = false; // Allow to do zoom over x axis and adds an icon hint in the chart

  this.builded = false;   // Setted to true on first build

  this.show3D = false;    // If true report is showed with 3D parameters
  this.angle3D = 45;
  this.depth3D = 20;

  this.donut = false;     // Adds an internal radios to the pie

  this.CustomUpdate = false;  // Indicates the update process (0,1,2)

  this.getDataMethod = ""; // PageMethod name for query update

  this.hasData = false; // Indicates if report has rows or data

  this.categoryId = reportCategoryId;

  this.conceptTitle = '';

  this.hasComparative = false;

  this.defaultSerie = -1; // If > 0 then only this serie will be activated

  // For webmethods calls
  this.parameters = {
    ReportName: reportCategory,
    ReportIndex: reportIndex,
    Concept: '',
    Extra: '',
    Date: '',
    DateGroup: '',
    Pivot: ''
  }

  this.style = 'box-40';

  this.type = 'column'; // Type of the report: column, line, spline, pie, ...

  // Average calculation:
  /*
  enabled: if true, generates the average on Prepare method.
  type: 'column' -> generates the average serie by column from 'value' column.
  value: if -1 use the series.value column.
  arithmethic: false: generates a serie with the same value on each point, the total average.
  true:  generates the average at each point of the serie.
  showDots: Indicates if average line must show dots on each point
  */
  this.average = {
    enabled: false,
    type: 'column',
    value: -1,
    arithmethic: false,
    showDots: false
  };

  // Categories could be:
  /*
  'column' : categories comes in a column values
  * value : indicates the column that have the categories
  * distinct: if true get unique values
  'header' : categories are the name of the columns
  * value : -1 - all columns are categories
  >1 - start from next column to the end for categories 
  * distinct: -not used-
  'custom' :
  */
  this.categories = {
    type: 'header',
    value: 0,
    distinct: false
  };

  // Series could be:
  /*
  'row'    : a serie by row (horizontal)
  * value : -1 - include all values in the serie
  >1 - indicates a column to exclude in the serie (usually used to determine serie name)   
  'column' : a serie by column (vertical)
  * value : the column index that contains the values
  * checkDuplicated: if true, check for already used categories to accumulate values, the column used to check categories is in this.categories.value
  'custom' :
  */
  this.series = {
    type: 'row',
    value: 0,
    checkDuplicates: false
  };

  // Event before assign categories:
  /*
  function (Categories) { return Categories; } 

  Note: Categories = [];
  */
  this.onFormatCategories = undefined;

  // Event before assign a serie to the template:
  /*
  function (Serie, Unused) { return Serie; } 

  Note: 
  - Serie = [{data:[],name:'',type:''},...]
  - Unused has the value of the excluded column or null;
  */
  this.onFormatSerie = undefined;

  // Event to create custom categories
  /*
  function (Data, Template) { return _custom_categories; }
  */
  this.onCustomGetCategories = undefined;

  // Event to create custom series
  /*
  function (Data, Template) { return _custom_series; }
  */
  this.onCustomGetSeries = undefined;

  // Event that occurs when build is end
  /*
  */
  this.onComplete = undefined;

  // Event that occurs before the prepare event
  this.onBeforeUpdate = undefined;
}

// Update a report
WSIReportType.prototype.Update = function () {
  var _rep = $("#" + this.id).highcharts();
  // Update data
  try {
    switch (this.type) {
      case 'solidgauge':
        var _point = _rep.series[0].points[0];
        _point.update(this.gaugeVal);
        break;
      default:
        //_rep.series[0].setData(this.template.series[0], false);
//        for (var _serie in this.template.series) {
          var _ops = CloneObject(this.template.series[0]);
          _rep.series[0].update(_ops, true);
//        }
        break;
    }
  } catch (_ex) {
    WSILogger.Log("WSIReportType[" + this.id + "].Update()", _ex);
  }
  // Update title
  _rep.setTitle({ text: _rep.title });
  //
}

// Create the div and decorate it with the highchart template
WSIReportType.prototype.Build = function (SetStyle) {

  SetStyle = (SetStyle == undefined) ? true : SetStyle;

  var _title_in_toolbar = (this.zoomable || this.resizable);
  var _current_title = this.caption;
  if (_title_in_toolbar) {
    this.caption = "";
    // TODO: Check if there is a % in caption and show value as default title
    this.template.title.text = "";
  }

  if (!this.CustomUpdate) {
    this.hasData = (this.data && this.data.rows.length > 0)
    if (SetStyle) { $('#' + this.id).attr("class", this.style); }
  }
  if (this.hasData || this.CustomUpdate) {

    // Zoomable property ?
    if (this.zoomable) {
      $.extend(this.template.chart, { resetZoomButton: { position: { x: -40, y: -60}} });
    }

    // Patch to set all reports with parent background
    if (SetStyle) { this.template.chart.backgroundColor = $("#div_report_chart_reports").parent().css('background-color'); }

    $('#' + this.id).highcharts(this.template);
    if (this.data) {
      this.builded = (this.data.rows.length != 0);
    }

    if (!this.CustomUpdate || this.forceWidget) {

      // Toolbox
      //$('#' + this.id).prepend('<div id="toolset' + this.id + '" class="ui-parent-mark"/>');
      $('#' + this.id).prepend('<div id="toolset' + this.id + '" style="float: none;"/>');

      // Zoomable icon
      if (this.zoomable) {
        $('#toolset' + this.id).prepend('<div class="ui-state-disabled ui-corner-all ui-toolset-item"><span id="ico' + this.id + 'Zoomable" class="ui-icon ui-icon-zoomin" style="float: left; margin: 2px;"/></div>');
        SetToolTip('ico' + this.id + 'Zoomable', _Language.get("NLS_ZOOMABLE"));
      }


      
      // Resizable icon
      if (this.resizable) {
        // TODO: DEMO COMMENTED
//        //$('#toolset' + this.id).prepend('<div id="div' + this.id + 'Resizable" class="ui-state-hover ui-corner-all ui-toolset-item" onclick="SetReportSize(\'' + this.id + '\', \'' + this.categoryId + '\')"><span id="ico' + this.id + 'Resizable" class="ui-toolset-item ui-icon ui-icon-arrow-4-diag" style="margin: 6px;" /></div>');
//        $('#toolset' + this.id).prepend('<div style="text-align:center;" id="div' + this.id + 'Resizable" class="ui-corner-all" onclick="SetReportSize(\'' + this.id + '\', \'' + this.categoryId + '\')"><span id="ico' + this.id + 'Resizable" class="ui-toolset-item ui-icon ui-icon-arrow-4-diag" style="float: left; margin: 2px;" /></div>');
        //        SetToolTip('div' + this.id + 'Resizable', _Language.get("NLS_RESIZABLE"));
        $('#toolset' + this.id).prepend('<div style="text-align:center;" id="div' + this.id + 'Resizable" class="ui-corner-all"><span id="ico' + this.id + 'Resizable" class="ui-toolset-item ui-icon ui-icon-arrow-4-diag" style="float: left; margin: 2px;" /></div>');
      }

      // 
      if (_title_in_toolbar && _current_title != "") {
        $('#div' + this.id + 'Resizable').append('<span class="tittleCharts">' + _current_title + '</span>');
      }

      // TODO : Add comparative table
      if (this.hasComparative) {
        $('#' + this.id).append('<div style="text-align:center;"><table  id="tblComparativa" style="width:40%;margin: 0 auto;"><tr><th >Ayer</th><th >Semana</th><th >Mes</th></tr><tr><td >1650</td><td >1450</td><td >1525</td></tr></table></div>');       
      }


    }

  } else {
    // No data
    WSILogger.Write('Report without template: ' + JSON.stringify(this.parameters));
  }
}

//
WSIReportType.prototype.Query = function () {
  // Check for autoupdate reports
  if (this.CustomUpdate == 2) {
    if (this.getDataMethod != "") {
      var _method = PageMethods[this.getDataMethod];
      if (_method) {
        var _scope = this;
        if (this.getDataMethodParams) {
          // With parameters
          _method(this.getDataMethodParams, function (result) { _scope.OnReportQueryDone(result, _scope); }, this.OnReportQueryError);
        } else {
          // Without parameters
          _method(function (result) { _scope.OnReportQueryDone(result, _scope); }, this.OnReportQueryError);
        }
      } else {
        WSILogger.Write("Report: " + this.id + " getData method error (" + this.getDataMethod + ")");
      }
    } else {
      WSILogger.Write("Report: " + this.id + " getDataMethod not specified!");
    }
  }
}

//
WSIReportType.prototype.OnReportQueryDone = function (result, report) {
  if (result != "" && result != null) {
    var _data = null;
    try {
      _data = JSON.parse(result);
      report.data = _data;
      report.Prepare();

      if (report.template.series != null) {
        // disable full refresh after first update
        //$.extend(report.template.plotOptions, { series: { animation: false} });
        $.extend(report.template, { plotOptions: { series: { animation: false} } });
      }

      if (!report.builded) {
        report.Build(false);
      } else {
        //report.Update();
        report.Build(false);
      }

    } catch (_ex) {
      WSILogger.Log("OnReportQueryDone[" + this.id + "]", _ex);
    }
    report.data = null;
  }
}

//
WSIReportType.prototype.OnReportQueryError = function (error) {
  this.data = null;
  WSILogger.Log("OnReportQueryError[" + this.id + "]", error);
}

// Parse the categories from the data
WSIReportType.prototype.GetCategories = function () {
  var _result = [];
  var _canadd = false;
  switch (this.categories.type) {
    case 'column':
      for (_row = 0; _row < this.data.rows.length; _row++) {
        _canadd = !this.categories.distinct;
        if (this.categories.distinct) {
          _canadd = (_result.indexOf(this.data.rows[_row][this.categories.value]) == -1);
        }
        if (_canadd) {
          _result.push(this.data.rows[_row][this.categories.value]);
        }
      }

      break;

    case 'header':
      if (this.categories.value > -1) {
        // From value to the end
        for (_col = (this.categories.value + 1); _col < this.data.columns.length; _col++) {
          _result.push(this.data.columns[_col]);
        }
      } else {
        // All columns
        _result = this.data.columns;
      }

      break;

    case 'custom':
      // Custom categories format and setting template
      if (this.onCustomGetCategories) {
        _result = this.onCustomGetCategories(this.data, this.template);
      }

      break;
  }
  return _result;
}

// Parse the series from the data
WSIReportType.prototype.GetSeries = function () {
  var _new_serie = null;
  var _series = [];
  var _unused_value = null;
  var _row = 0;
  var _col = 0;

  switch (this.series.type) {
    case 'row':
      for (_row = 0; _row < this.data.rows.length; _row++) {
        // initialize serie
        _new_serie = {
          data: [],
          name: _row,
          type: this.type
        };
        // Get unused value for formatting
        if (this.series.value != -1) {
          _unused_value = this.data.rows[_row][this.series.value];
        } else {
          _unused_value = null;
        }
        // Create data serie
        for (_cell = (this.categories.value + 1); _cell < this.data.rows[_row].length; _cell++) {
          //_new_serie.data.push(+this.data.rows[_row][_cell]);
          _new_serie.data.push(parseCurrency(this.data.rows[_row][_cell]));
        }
        // Check for format function
        if (this.onFormatSerie) {
          _new_serie = this.onFormatSerie(_new_serie, _unused_value);
        }
        // Add serie
        _series.push(_new_serie);
      }

      break;

    case 'column':
      var _used_categories = [];
      var _counter = 1;
      // Adding one serie with all values of the column 
      _new_serie = {
        data: [],
        type: this.type
      };
      //
      if (this.series.value != -1) {
        // 1 column, 1 serie
        for (_row = 0; _row < this.data.rows.length; _row++) {
          if (!this.series.checkDuplicates) {
            // Get values
            //_new_serie.data.push(+this.data.rows[_row][this.series.value]);
            _new_serie.data.push(parseCurrency(this.data.rows[_row][this.series.value]));
            //WSILogger.Write("* Add value to serie: " + this.data.rows[_row][this.series.value] + " -> " + parseCurrency(this.data.rows[_row][this.series.value]));
          } else {
            // Get values checking for duplicated categories
            if (_used_categories.indexOf(this.data.rows[_row][this.categories.value]) != -1) {
              _new_serie.data[_used_categories.indexOf(this.data.rows[_row][this.categories.value])] += +this.data.rows[_row][this.series.value];
            } else {
              _used_categories.push(this.data.rows[_row][this.categories.value]);
              //_new_serie.data.push(+this.data.rows[_row][this.series.value]);
              _new_serie.data.push(parseCurrency(this.data.rows[_row][this.series.value]));
              //WSILogger.Write("Add value to serie: " + this.data.rows[_row][this.series.value] + " -> " + parseCurrency(this.data.rows[_row][this.series.value]));
            }
          }
        }
        //
        if (this.series.stacks == 'serie') {
          _new_serie.stack = _counter;
          _counter++;
        };
        // Check for format function
        if (this.onFormatSerie) {
          _new_serie = this.onFormatSerie(_new_serie, _unused_value);
        }
        // Add serie
        _series.push(_new_serie);
      } else {
        // Add series for each column except the one used in categories
        for (_col = (this.categories.value + 1); _col < this.data.columns.length; _col++) {
          _new_serie = {
            data: [],
            name: this.data.columns[_col],
            type: this.type,
            visible: (this.defaultSerie == -1 || this.defaultSerie == _col)
          };
          for (_row = 0; _row < this.data.rows.length; _row++) {
            //_new_serie.data.push(+this.data.rows[_row][_col]);
            _new_serie.data.push(parseCurrency(this.data.rows[_row][_col]));
          }
          //
          if (this.series.stacks == 'serie') {
            _new_serie.stack = _counter;
            _counter++;
          };
          // Check for format function
          if (this.onFormatSerie) {
            _new_serie = this.onFormatSerie(_new_serie, _unused_value);
          }
          // Add serie
          _series.push(_new_serie);
        }
      }
      break;
    case 'custom':
      // Custom series format and setting template
      if (this.onCustomGetSeries) {
        _series = this.onCustomGetSeries(this.data, this.template);
      }

      break;
  }

  return _series;
}

// Perform the parses and the updates to data
WSIReportType.prototype.Prepare = function () {

  if (this.onBeforeUpdate) {
    this.onBeforeUpdate(this);
  }

  // 3d?
  $.extend(this.template.chart, { options3d: { enabled: this.show3D, alpha: this.show3D ? this.angle3D : 0, depth: this.show3D ? this.depth3D : 0} });

  // Donut?
  if (this.template.plotOptions) {
    $.extend(this.template.plotOptions.pie, { innerSize: this.donut ? 50 : 0 });
  }

  if (this.type != 'solidgauge') {

    if (!this.data || !this.data.columns || !this.data.rows) { return; }

    // Obtain series from data
    var _series = this.GetSeries();
    // Set data series
    this.template.series = _series;

    // Get categories
    var _categories = this.GetCategories();

    // Format all categories
    if (this.onFormatCategories) {
      _categories = this.onFormatCategories(_categories);
    }

    // Assign categories
    $.extend(this.template.xAxis, { categories: _categories });

    // Average?
    if (this.average.enabled) {
      this.CalculateAverage();
    }

    // Set  title
    $.extend(this.template.yAxis, { title: { text: this.conceptTitle} });

  } else {
    // gauge update
    if (!this.builded) {
      this.template.series[0].data[0] = this.gaugeVal;
    }

    this.template.series[0].name = this.caption;

  }

  // Title
  this.template.title.text = this.caption;
  if (this.subtitleDate) {
    this.template.subtitle.text = this.parameters.Date;
  } else {
    this.template.subtitle.text = this.subtitle;
  }

  // Zoomable ?
  if (this.zoomable) {
    this.template.chart.zoomType = 'x';
  } else {
    this.template.chart.zoomType = undefined;
  }

  // Resizable ?
  if (this.resizable) {
    this.template.chart.resizeState = 0;
  }

  // disable datalabels?
  switch (this.type) {
    case 'line':
    case 'spline':
    case 'areaspline':
      // If more than one serie, hide datalabels to avoid text over text
      if (this.template.series.length > 1) {
        $.extend(this.template.plotOptions, { series: { dataLabels: { enabled: false}} });
      }
      break;
  }

  // Check if preparation is needed
  if (this.CustomUpdate && this.builded) {
    return;
  }

  // On complete event
  if (this.onComplete) {
    this.onComplete(this.data, this.template);
  }
}

// Generates the average serie for the report
WSIReportType.prototype.CalculateAverage = function () {

  var _default_serie = {
    data: [],
    datalabels: { enabled: false },
    color: 'orange',
    name: 'Average',
    type: 'spline',
    marker: {
      enabled: this.average.showDots,
      fillColor: 'white',
      lineColor: 'black',
      lineWidth: 2
    },
    states: {
      hover: { enabled: false },
      select: { enabled: false }
    }
  };

  try {

    switch (this.average.type) {
      case 'column':
        // calculate an average serie from first serie in the chart
        var _average = 0;
        var _counter = 0;
        var _new_serie = CloneObject(_default_serie);

        //WSILogger.Debug("CalculateAverage()", "Serie Data", this.template.series[0].data);

        for (_i = 0; _i < this.template.series[0].data.length; _i++) {

          //WSILogger.Debug("CalculateAverage()", "Average item " + _i, parseCurrency(this.template.series[0].data[_i]));
          _average += parseCurrency(this.template.series[0].data[_i]);
          _counter += 1;
        }
        _average = Round(_average / _counter);

        for (_i = 0; _i < this.template.series[0].data.length; _i++) {
          _new_serie.data.push(_average);
        }

        //WSILogger.Debug("CalculateAverage()", "Added serie", _new_serie);

        this.template.series.push(_new_serie);
        break;
      /*
      case 'column':
      var _average = 0;
      var _used_categories = [];
      var _total = 0;
      var _counter = 0;
      var _column = (this.average.value == -1) ? this.series.value : this.average.value;
      var _new_serie = null;
      for (_i = 0; _i < this.data.rows.length; _i++) {
      _total += +this.data.rows[_i][_column];
      if (this.series.checkDuplicates) {
      if (_used_categories.indexOf(this.data.rows[_i][this.categories.value]) == -1) {
      _used_categories.push(this.data.rows[_i][this.categories.value]);
      _counter += 1;
      }
      } else {
      _counter += 1;
      }
      if (this.average.arithmetic) {
      // Calculate average for each point
      if (_new_serie != null) { _new_serie = CloneObject(_default_serie); }
      _new_serie.push(Round(_total / _counter));
      }
      }
      // Calculate normal average
      if (!this.average.arithmetic) {
      _average = Round(_total / _counter);

      _new_serie = CloneObject(_default_serie);

      for (_i = 0; _i < _counter; _i++) {
      _new_serie.data.push(_average);
      }
      }

      this.template.series.push(_new_serie);
      break;
      */ 
    }

  } catch (_ex) {
    WSILogger.Log("CalculateAverage()", _ex);
  }

}

//
WSIReportType.prototype.getObject = function () {
  return $("#" + this.id);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////////////

// Top 10 terminals on concept
var m_report_statistics_top10 = new WSIReportType('idReportStatsTop10', 0, 'Statistics', 0, _report_1);
m_report_statistics_top10.categories.type = 'column';
m_report_statistics_top10.categories.value = 1;
m_report_statistics_top10.series.type = 'column';
m_report_statistics_top10.series.value = 2;
m_report_statistics_top10.template.colors = ["#00FF00"];
m_report_statistics_top10.caption = 'TOP 10';
m_report_statistics_top10.subtitleDate = true;
m_report_statistics_top10.style = 'box-40-tall';

// Bottom 10 terminals on concept
var m_report_statistics_worst10 = new WSIReportType('idReportStatsWorst10', 0, 'Statistics', 1, _report_1);
m_report_statistics_worst10.categories.type = 'column';
m_report_statistics_worst10.categories.value = 1;
m_report_statistics_worst10.series.type = 'column';
m_report_statistics_worst10.series.value = 2;
m_report_statistics_worst10.template.colors = ["#FF0000"];
m_report_statistics_worst10.caption = 'BOTTOM 10';
m_report_statistics_worst10.subtitleDate = true;
m_report_statistics_worst10.style = 'box-40-tall';

// Accumulated by date, last 10 days
var m_report_statistics_accum = new WSIReportType('idReportStatsAccum', 0, 'Statistics', 2, _report_1);
m_report_statistics_accum.type = 'areaspline';
m_report_statistics_accum.categories.type = 'column';
m_report_statistics_accum.categories.value = 0;
m_report_statistics_accum.categories.distinct = true;
m_report_statistics_accum.series.type = 'column';
m_report_statistics_accum.series.value = 2;
m_report_statistics_accum.template.chart.zoomType = 'x';
m_report_statistics_accum.caption = 'ACCUMULATED';
m_report_statistics_accum.style = 'box-40';
m_report_statistics_accum.template.xAxis.tickInterval = 2;
m_report_statistics_accum.onFormatCategories = function (Categories) {
  for (_i = 0; _i < Categories.length; _i++) {
    Categories[_i] = Categories[_i].split(' ')[0];
  }
  return Categories;
}

// Total Concept by provider of selected day
var m_report_statistics_period = new WSIReportType('idReportStatsPeriod', 0, 'Statistics', 4, _report_1);
m_report_statistics_period.categories.type = 'column';
m_report_statistics_period.categories.value = 0;
m_report_statistics_period.series.type = 'column';
m_report_statistics_period.series.value = 1;
m_report_statistics_period.caption = 'PERIOD';
m_report_statistics_period.subtitleDate = true;
m_report_statistics_period.average.enabled = true;
m_report_statistics_period.style = 'box-40';

// Statistics - Concept totals by provider last 7 days from selected date
var m_report_statistics_providers = new WSIReportType('idReportStatsProvider', 0, 'Statistics', 3, _report_0);
m_report_statistics_providers.type = 'line';
m_report_statistics_providers.categories.type = 'column';
m_report_statistics_providers.categories.value = 0;
m_report_statistics_providers.series.type = 'column';
m_report_statistics_providers.series.value = -1;
m_report_statistics_providers.caption = 'PROVIDERS';
m_report_statistics_providers.average.enabled = false;
m_report_statistics_providers.style = 'box-40-2';
m_report_statistics_providers.template.xAxis.tickInterval = 2;
m_report_statistics_providers.onFormatCategories = function (Categories) {
  for (_i = 0; _i < Categories.length; _i++) {
    Categories[_i] = Categories[_i].split(' ')[0];
  }
  return Categories;
}

// Cash Cash-In report
var m_report_cash_cash_in = new WSIReportType('idReportCash', 2, 'Cash', 0, _report_1);
m_report_cash_cash_in.type = 'column';
m_report_cash_cash_in.categories.type = 'column';
m_report_cash_cash_in.categories.value = 0;
m_report_cash_cash_in.categories.distinct = true;
m_report_cash_cash_in.series.type = 'column';
m_report_cash_cash_in.series.value = 1;
m_report_cash_cash_in.series.checkDuplicates = true;
m_report_cash_cash_in.caption = 'CASH';
m_report_cash_cash_in.style = 'box-80';
m_report_cash_cash_in.average.enabled = true;
m_report_cash_cash_in.template.xAxis.tickInterval = 3;
m_report_cash_cash_in.onFormatCategories = function (Categories) {
  for (_i = 0; _i < Categories.length; _i++) {
    Categories[_i] = Categories[_i].split(' ')[0];
  }
  return Categories;
}

// Cash Top 10 cash-in cashiers
var m_report_cash_cash_in_t = new WSIReportType('idReportCashTop10CashIn', 2, 'Cash', 1, _report_1);
m_report_cash_cash_in_t.type = 'column';
m_report_cash_cash_in_t.categories.type = 'column';
m_report_cash_cash_in_t.categories.value = 0;
m_report_cash_cash_in_t.categories.distinct = true;
m_report_cash_cash_in_t.series.type = 'column';
m_report_cash_cash_in_t.series.value = 1;
m_report_cash_cash_in_t.series.checkDuplicates = true;
m_report_cash_cash_in_t.caption = 'TOP 10 CASH IN';
m_report_cash_cash_in_t.template.colors = ["#00FF00"];

// Cash Bottom 10 cash-in cashiers
var m_report_cash_cash_in_b = new WSIReportType('idReportCashBottom10CashIn', 2, 'Cash', 2, _report_1);
m_report_cash_cash_in_b.type = 'column';
m_report_cash_cash_in_b.categories.type = 'column';
m_report_cash_cash_in_b.categories.value = 0;
m_report_cash_cash_in_b.categories.distinct = true;
m_report_cash_cash_in_b.series.type = 'column';
m_report_cash_cash_in_b.series.value = 1;
m_report_cash_cash_in_b.series.checkDuplicates = true;
m_report_cash_cash_in_b.caption = 'BOTTOM 10 CASH IN';
m_report_cash_cash_in_b.template.colors = ["#FF0000"];

// Segementation per Level
var m_report_segmentation_levels = new WSIReportType('idReportSegmenLevels', 3, 'Segmentation', 0, _report_0);
m_report_segmentation_levels.type = 'line';
m_report_segmentation_levels.caption = 'LEVELS';
m_report_segmentation_levels.style = 'box-80';
m_report_segmentation_levels.onFormatSerie = function (Serie, Unused) {
  Serie.name = _Language.get("NLS_LVL_" + Unused);
  return Serie;
}
m_report_segmentation_levels.onFormatCategories = function (Categories) {
  for (_i = 0; _i < Categories.length; _i++) {
    Categories[_i] = Categories[_i].split(' ')[0] + ' ' + Categories[_i].split(' ')[3];
  }
  return Categories
}

// Segmentation per Age
var m_report_segmentation_age = new WSIReportType('idReportSegmenAge', 3, 'Segmentation', 1, _report_0);
m_report_segmentation_age.type = 'line';
m_report_segmentation_age.caption = 'AGE';
m_report_segmentation_age.style = 'box-80';
m_report_segmentation_age.onFormatSerie = function (Serie, Unused) {
  switch (Unused) {
    case "0":
      Serie.name = _Language.get("NLS_ANONYMOUS");
      break;
    case "1":
      Serie.name = _Language.get("NLS_UNDER_AGE", [18]);
      break;
    case "2":
      Serie.name = "18 - 24";
      break;
    case "3":
      Serie.name = "25 - 34";
      break;
    case "4":
      Serie.name = "35 - 44";
      break;
    case "5":
      Serie.name = "45 - 54";
      break;
    case "6":
      Serie.name = "55 - 64";
      break;
    case "7":
      Serie.name = "65 - 74";
      break;
    case "8":
      Serie.name = "75 - 84";
      break;
    case "9":
      Serie.name = _Language.get("NLS_OVER_AGE", [85]);
      break;
  }
  return Serie;
}
m_report_segmentation_age.onFormatCategories = function (Categories) {
  for (_i = 0; _i < Categories.length; _i++) {
    Categories[_i] = Categories[_i].split(' ')[0] + ' ' + Categories[_i].split(' ')[3];
  }
  return Categories
}

//
var m_report_segmentation_adt_group = new WSIReportType('idReportSegmenAdt', 3, 'Segmentation', 2, _report_0);
m_report_segmentation_adt_group.type = 'line';
m_report_segmentation_adt_group.caption = 'THEORIC NW';
m_report_segmentation_adt_group.style = 'box-80';
//m_report_segmentation_adt_group.template.yAxis.max = 100;
//m_report_segmentation_adt_group.template.yAxis.min = 0;
m_report_segmentation_adt_group.onFormatSerie = function (Serie, Unused) {
  switch (Unused) {
    case "0":
      Serie.name = "$ 0 - 49";
      break;
    case "1":
      Serie.name = "$ 50 - 99";
      break;
    case "2":
      Serie.name = "$ 100 - 149";
      break;
    case "3":
      Serie.name = "$ 150 - 249";
      break;
    case "4":
      Serie.name = "$ 250 - 399";
      break;
    case "5":
      Serie.name = "$ 400 - 999";
      break;
    case "6":
      Serie.name = "> $ 1000";
      break;
  }
  return Serie;
}
m_report_segmentation_adt_group.onFormatCategories = function (Categories) {
  for (_i = 0; _i < Categories.length; _i++) {
    Categories[_i] = Categories[_i].split(' ')[0] + ' ' + Categories[_i].split(' ')[3];
  }
  return Categories
}

//
var m_report_occupation_provider7 = new WSIReportType('idReportOccupProv7', 1, 'Occupation', 0, _report_1);
m_report_occupation_provider7.type = 'column';
m_report_occupation_provider7.categories.type = 'column';
m_report_occupation_provider7.categories.value = 0;
m_report_occupation_provider7.categories.distinct = true;
m_report_occupation_provider7.series.type = 'column';
m_report_occupation_provider7.series.value = 1;
m_report_occupation_provider7.series.checkDuplicates = true;
m_report_occupation_provider7.caption = 'PROVIDER';
m_report_occupation_provider7.subtitle = 'Last 7 days';
m_report_occupation_provider7.style = 'box-80';
m_report_occupation_provider7.average.enabled = true;
m_report_occupation_provider7.parameters.DateGroup = 0;

//
var m_report_occupation_provider30 = new WSIReportType('idReportOccupProv30', 1, 'Occupation', 0, _report_1);
m_report_occupation_provider30.type = 'column';
m_report_occupation_provider30.categories.type = 'column';
m_report_occupation_provider30.categories.value = 0;
m_report_occupation_provider30.categories.distinct = true;
m_report_occupation_provider30.series.type = 'column';
m_report_occupation_provider30.series.value = 1;
m_report_occupation_provider30.series.checkDuplicates = true;
m_report_occupation_provider30.caption = 'PROVIDER';
m_report_occupation_provider30.subtitle = 'Last 30 days';
m_report_occupation_provider30.style = 'box-80';
m_report_occupation_provider30.average.enabled = true;
m_report_occupation_provider30.parameters.DateGroup = 1;

///////////////////////////////////////////////////////////////////////////////////////////

//
var m_empty_report_age = new WSIReportType('idReportOccupAge', -1, 'ByAge', 0, _report_3);
m_empty_report_age.CustomUpdate = true;
m_empty_report_age.type = 'pie';
m_empty_report_age.show3D = true;
m_empty_report_age.donut = false;
m_empty_report_age.caption = 'BY AGE';
m_empty_report_age.subtitle = 'Real Time';
m_empty_report_age.style = 'box-16';
m_empty_report_age.series.type = 'custom';
m_empty_report_age.onCustomGetSeries = function (Data, Template) {
  var _item = null;
  var _result_rows = [];
  var _row = null;
  for (_row in Data.rows) {
    _item = CloneObject({
      name: '',
      y: 0,
      sliced: false,
      selected: false,
      color: ''
    });
    _item.name = Data.rows[_row][0];
    _item.y = Data.rows[_row][1];
    _item.color = m_colors_age[Data.rows[_row][0]];
    _result_rows.push(_item);
    //_result_rows.push(parseCurrency(_item));
  }
  return [{ type: 'pie', data: _result_rows}];
};
m_empty_report_age.onBeforeUpdate = function (Report) {
  var _i = 0;
  Report.data = {};
  Report.data.columns = ["AGE_RANGE", "VALUE"];
  Report.data.rows = [];
  for (_i in _Manager.Terminals.OccupationData.Ages) {
    Report.data.rows.push([_i, _Manager.Terminals.OccupationData.Ages[_i]]);
  }
};

//
var m_empty_report_occ = new WSIReportType('idReportOccupOcc', -1, 'ByOccupation', 0, _report_3);
m_empty_report_occ.CustomUpdate = true;
m_empty_report_occ.type = 'pie';
m_empty_report_occ.show3D = true;
m_empty_report_occ.donut = false;
m_empty_report_occ.caption = 'BY OCCUPATION';
m_empty_report_occ.subtitle = 'Real Time';
m_empty_report_occ.style = 'box-16';
m_empty_report_occ.series.type = 'custom';
m_empty_report_occ.onCustomGetSeries = function (Data, Template) {
  var _item = null;
  var _result_rows = [];
  var _row = null;
  for (_row in Data.rows) {
    _item = CloneObject({
      name: '',
      y: 0,
      sliced: false,
      selected: false,
      color: ''
    });
    _item.name = Data.rows[_row][0];
    _item.y = Data.rows[_row][1];
    _item.color = m_colors_occupation[Data.rows[_row][0]];
    _result_rows.push(_item);
    //_result_rows.push(parseCurrency(_item));
  }
  return [{ type: 'pie', data: _result_rows}];
};
m_empty_report_occ.onBeforeUpdate = function (Report) {
  var _i = 0;
  Report.data = {};
  Report.data.columns = ["OCC", "VALUE"];
  Report.data.rows = [];
  for (_i in _Manager.Terminals.OccupationData.Occupation) {
    Report.data.rows.push([_i, _Manager.Terminals.OccupationData.Occupation[_i]]);
  }
};

//
var m_empty_report_level = new WSIReportType('idReportOccupLevel', -1, 'ByLevel', 0, _report_3);
m_empty_report_level.CustomUpdate = true;
m_empty_report_level.type = 'pie';
m_empty_report_level.show3D = true;
m_empty_report_level.donut = false;
m_empty_report_level.caption = 'BY LEVEL';
m_empty_report_level.subtitle = 'Real Time';
m_empty_report_level.style = 'box-16';
m_empty_report_level.series.type = 'custom';
m_empty_report_level.onCustomGetSeries = function (Data, Template) {
  var _item = null;
  var _result_rows = [];
  var _row = null;
  for (_row in Data.rows) {
    _item = CloneObject({
      name: '',
      y: 0,
      sliced: false,
      selected: false,
      color: ''
    });
    _item.name = Data.rows[_row][0];
    _item.y = Data.rows[_row][1];
    _item.color = m_colors_level[Data.rows[_row][0]];
    _result_rows.push(_item);
    //_result_rows.push(parseCurrency(_item));
  }
  return [{ type: 'pie', data: _result_rows}];
};
m_empty_report_level.onBeforeUpdate = function (Report) {
  var _i = 0;
  Report.data = {};
  Report.data.columns = ["LVL", "VALUE"];
  Report.data.rows = [];
  for (_i in _Manager.Terminals.OccupationData.Levels) {
    Report.data.rows.push([_i, _Manager.Terminals.OccupationData.Levels[_i]]);
  }
};

//
var m_empty_report_gend = new WSIReportType('idReportOccupGend', -1, 'ByGender', 0, _report_3);
m_empty_report_gend.CustomUpdate = true;
m_empty_report_gend.type = 'pie';
m_empty_report_gend.show3D = true;
m_empty_report_gend.donut = false;
m_empty_report_gend.caption = 'BY GENDER';
m_empty_report_gend.subtitle = 'Real Time';
m_empty_report_gend.style = 'box-16';
m_empty_report_gend.series.type = 'custom';
m_empty_report_gend.onCustomGetSeries = function (Data, Template) {
  var _item = null;
  var _result_rows = [];
  var _row = null;
  for (_row in Data.rows) {
    _item = CloneObject({
      name: '',
      y: 0,
      sliced: false,
      selected: false,
      color: ''
    });
    _item.name = Data.rows[_row][0];
    _item.y = Data.rows[_row][1];
    _item.color = m_colors_gender[Data.rows[_row][0]];
    _result_rows.push(_item);
    //_result_rows.push(parseCurrency(_item));
  }
  return [{ type: 'pie', data: _result_rows}];
};
m_empty_report_gend.onBeforeUpdate = function (Report) {
  var _i = 0;
  Report.data = {};
  Report.data.columns = ["GND", "VALUE"];
  Report.data.rows = [];
  for (_i in _Manager.Terminals.OccupationData.Gender) {
    Report.data.rows.push([_i, _Manager.Terminals.OccupationData.Gender[_i]]);
  }
};

// Real Time Occupation % gauge
var m_empty_report_gauge = new WSIReportType('idReportOccupGauge', -1, 'Occup. Percent', 0, _report_gauge);
m_empty_report_gauge.CustomUpdate = true;
m_empty_report_gauge.type = 'solidgauge';
m_empty_report_gauge.caption = 'BY OCCUPATION';
m_empty_report_gauge.subtitle = 'Real Time';
m_empty_report_gauge.style = 'box-16';
m_empty_report_gauge.series.type = 'custom';
m_empty_report_gauge.categories.type = 'custom';
m_empty_report_gauge.onBeforeUpdate = function (Report) {
  var _free = _Manager.Terminals.OccupationData.Total - _Manager.Terminals.OccupationData.Occupation['Idle'];
  this.gaugeVal = parseInt(((_free * 100) / _Manager.Terminals.OccupationData.Total) * 10) / 10;
};

// Cash result evolution last 10 days from selected
var m_report_cash_result = new WSIReportType('idReportCashResult', 2, 'Cash', 3, _report_0);
m_report_cash_result.type = 'line';
m_report_cash_result.caption = 'CASH RESULT';
m_report_cash_result.style = 'box-40-2';
//m_report_cash_result.template.yAxis.max = 100;
//m_report_cash_result.template.yAxis.min = 0;
m_report_cash_result.series.type = 'column';
m_report_cash_result.series.value = -1;
m_report_cash_result.categories.type = 'column';
m_report_cash_result.categories.value = 0;
//m_report_cash_result.template.xAxis.tickInterval = 2;
m_report_cash_result.onFormatCategories = function (Categories) {
  for (_i = 0; _i < Categories.length; _i++) {
    Categories[_i] = Categories[_i].split(' ')[0];
  }
  return Categories
}

// Cashiers result
var m_report_cash_cashiers = new WSIReportType('idReportCashCashiers', 2, 'Cash', 4, _report_4);
m_report_cash_cashiers.type = 'column';
m_report_cash_cashiers.caption = 'CASHIERS RESULT';
m_report_cash_cashiers.subtitleDate = true;
m_report_cash_cashiers.style = 'box-80';
m_report_cash_cashiers.series.type = 'column';
m_report_cash_cashiers.series.value = -1;
m_report_cash_cashiers.series.stacks = 'serie';
m_report_cash_cashiers.categories.type = 'column';
m_report_cash_cashiers.categories.value = 0;
m_report_cash_cashiers.defaultSerie = 1;

// Unbalanced cashier sessions on selected date
var m_report_cash_unbalance = new WSIReportType('idReportCashUnbalance', 2, 'Cash', 5, _report_5);
m_report_cash_unbalance.type = 'column';
m_report_cash_unbalance.caption = 'UNBALANCED CASHIER SESSIONS';
m_report_cash_unbalance.subtitleDate = true;
m_report_cash_unbalance.style = 'box-40-2';
m_report_cash_unbalance.series.type = 'column';
m_report_cash_unbalance.series.value = -1;
m_report_cash_unbalance.categories.type = 'column';
m_report_cash_unbalance.categories.value = 0;
m_report_cash_unbalance.defaultSerie = 1;
m_report_cash_unbalance.zoomable = true;
m_report_cash_unbalance.onFormatSerie = function (Serie, Unused) {
  if (Serie.name == "BALANCE") {
    // Difference between positive and negaive values for Balance column
    var _idx = 0;
    for (_idx in Serie.data) {
      Serie.data[_idx] = { y: Serie.data[_idx],
        color: (Serie.data[_idx] >= 0) ? '#00AA00' : '#AA0000',        // Bar color
        dataLabels: {
          color: (Serie.data[_idx] >= 0) ? '#00AA00' : '#AA0000',    // Text Color
          align: (Serie.data[_idx] >= 0) ? 'left' : 'right',         // Value position
          y: (Serie.data[_idx] >= 0) ? -5 : 5

        }
      };
    }
  }
  return Serie;
}

///////////////////////////////////////////////////////////////////////////////////////////

// To set the agrupation and order of the reports
var m_report_objects = null;

if (_LayoutMode == 0) {
  //  --------------------------------- Charts mode reports
  var m_report_objects = [
  // Statistics
                        [m_report_statistics_top10, m_report_statistics_worst10, m_report_statistics_providers, m_report_statistics_accum, m_report_statistics_period],
  // Occupation
  //[m_empty_report_occ, m_empty_report_level, m_empty_report_age, m_empty_report_gend, m_report_occupation_provider7],
                        [m_report_occupation_provider7, m_report_occupation_provider30],
  // Cash
  //[m_report_cash_cash_in_t, m_report_cash_cash_in_b, m_report_cash_cash_in],
                        [m_report_cash_result, m_report_cash_unbalance, m_report_cash_cashiers],
  // Segementation
                        [m_report_segmentation_levels, m_report_segmentation_age, m_report_segmentation_adt_group]
                      ];
} else {
  //  --------------------------------- Layout mode reports
  var m_report_objects = [
  // Statistics
                        [m_report_statistics_top10, m_report_statistics_worst10, m_report_statistics_providers, m_report_statistics_accum, m_report_statistics_period],
  // Occupation
  //[m_empty_report_occ, m_empty_report_level, m_empty_report_age, m_empty_report_gend, m_report_occupation_provider7],
                        [m_empty_report_gauge, m_empty_report_occ, m_empty_report_level, m_empty_report_age, m_empty_report_gend, m_report_occupation_provider7, m_report_occupation_provider30],
  // Cash
  //[m_report_cash_cash_in_t, m_report_cash_cash_in_b, m_report_cash_cash_in],
                        [m_report_cash_result, m_report_cash_unbalance, m_report_cash_cashiers],
  // Segementation
                        [m_report_segmentation_levels, m_report_segmentation_age, m_report_segmentation_adt_group]
                      ];
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Mark reports as not builded
function ResetBuildedManualReports() {
  for (_report in m_report_objects[m_reports_type_selected]) {
    if (m_report_objects[m_reports_type_selected][_report].categoryId != -1) {
      continue;
    }
    var Report = m_report_objects[m_reports_type_selected][_report];
    Report.builded = false;
  }
}

// Called from the main thread, used to force the update of the report.
function UpdateManualReports() {

  for (_report in m_report_objects[m_reports_type_selected]) {
    // Ignore demo reports
    if (m_report_objects[m_reports_type_selected][_report].categoryId != -1) {
      continue;
    }

    // Select the report and prepare
    var Report = m_report_objects[m_reports_type_selected][_report];
    Report.Prepare();

    // If not already builded
    if (!Report.builded) {
      // Build
      Report.Build();
    } else {
      // Update already builded reports (real time)
      Report.Update();
    }
  }

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

var m_reflown_report = undefined;

function SetReportSize(ReportId, ReportFamily) {

  // If in dashboard mode uso another resize
  if (m_current_section == 10) {
    SetDashboardReportSize(ReportId);
    return;
  }

  // Standard resize
  var _report = null;
  var _idx = 0;
  var _id = null;
  var _restore = false;

  WSILogger.Write('Setting reports sizes...');

  for (_idx in m_report_objects[ReportFamily]) {
    _report = $("#" + m_report_objects[ReportFamily][_idx].id);
    if (_report.length == 1) {
      _report = _report[0];
      _id = _report.id;
      if ($("#" + _id).hasClass('box-big')) {
        m_reflown_report = _id;
        $("#" + _id).switchClass('box-big', m_report_objects[ReportFamily][_idx].style, 50, 'swing', OnResizeReportComplete);
        _restore = true;
      } else {
        if (ReportId == _id) {
          m_reflown_report = _id;
          $("#" + _id).switchClass(m_report_objects[ReportFamily][_idx].style, 'box-big', 400, 'swing', OnResizeReportComplete);
        }
      }
    }
  }
  if (_restore) {
    WSILogger.Write('Restoring report sizes...');
    $("div[id^='idReport']").show(function () { FitReports(); });
  } else {
    WSILogger.Write('Not restoring report sizes...');
    $("div[id^='idReport']").not("#" + ReportId).hide(function () { FitReports(); });
  }

}

function SetDashboardReportSize(ReportId) {
  var _report = null;
  var _idx = 0;
  var _id = null;
  var _restore = false;

  WSILogger.Write('Setting dashboard reports sizes...');

  for (_idx in m_report_dashboard_objects) {
    _report = $("#" + m_report_dashboard_objects[_idx].id);
    if (_report.length == 1) {
      _report = _report[0];
      _id = _report.id;
      if ($("#" + _id).hasClass('col100')) {
        m_reflown_report = _id;
        $("#" + _id).switchClass('col100', m_report_dashboard_objects[_idx].style, 50, 'swing', OnResizeReportComplete);
        _restore = true;
      } else {
        if (ReportId == _id) {
          m_reflown_report = _id;
          $("#" + _id).switchClass(m_report_dashboard_objects[_idx].style, 'col100', 400, 'swing', OnResizeReportComplete);
        }
      }
    }
  }
  if (_restore) {
    WSILogger.Write('Restoring report sizes...');
    $("div[id^='report_dash']").show(function () { FitReports(); });
  } else {
    WSILogger.Write('Not restoring report sizes...');
    $("div[id^='report_dash']").not("#" + ReportId).hide(function () { FitReports(); });
  }

}

function OnResizeReportComplete() {
  if (m_current_section == 10) {
    //$("div[id^='report_dash']").each(function (index, value) { var _chart = $(value).highcharts(); if (_chart) { _chart.redraw(); } });
    //UpdateReportDashboard();
    var _chart = $(m_reflown_report).highcharts();
    if (_chart) {
      _chart.reflow();
      _chart.redraw();
    }
  } else {
    $("div[id^='idReport']").each(function (index, value) { var _chart = $(value).highcharts(); if (_chart) { _chart.reflow(); _chart.redraw(); } });
  }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

function FitReportsEvent(event) {
  FitReports();
}

function FitReports(Height) {
  var _seed = 700;

  if (_LayoutMode != 0) {
    _seed = 800;
  }

  if (!Height) {
    //Height = screen.availHeight;
    Height = $(window).height();
  }

  WSILogger.Write('Fitting reports...');

  // Dashboard special
  if (m_current_section != 10) {

    $('div[id^="idReport"]').each(function (idx, item) {
      var _factor = _seed;
      var _c_height = $(item).css('height');
      switch (true) {
        case $(item).hasClass('box-big'):
          _factor = -1;
          break;
        case $(item).hasClass('box-20'):
          _factor = 205;
          break;
        case $(item).hasClass('box-16'):
          _factor = 225;
          break;
        case $(item).hasClass('box-26'):
          _factor = 205;
          break;
        case $(item).hasClass('box-40'):
          _factor = 205;
          break;
        case $(item).hasClass('box-40-tall'):
          _factor = 230;
          break;
        case $(item).hasClass('box-40-2'):
          _factor = 422;
          break;
        case $(item).hasClass('box-60'):
          _factor = 205;
          break;
        case $(item).hasClass('box-80'):
          _factor = 205;
          break;
      }
      if (_factor != -1) {
        _factor = _factor / _seed;
        _c_height = Height * _factor;
      } else {
        _c_height = Height - 100;
      }
      $(item).css('height', _c_height);
      var _chart = $(item).highcharts();
      if (_chart) {
        _chart.reflow();
        //_chart.redraw(); 
        WSILogger.Write('reflowing chart...');
      }
      //WSILogger.Write(idx + " " + $(item).attr('id') + " -> " + _c_height);
    });

  } else {

    $('div[id^="report_dash"]').each(function (idx, item) {
      var _factor = _seed;
      var _c_height = $(item).css('height');
      switch (true) {
        case $(item).hasClass('col100'):
          _factor = -1;
          break;
        case $(item).hasClass('col30'):
          _factor = 205;
          break;
        case $(item).hasClass('col50'):
          _factor = 225;
          break;
      }
      if (_factor != -1) {
        _factor = _factor / _seed;
        _c_height = Height * _factor;
      } else {
        _c_height = Height;
      }
      $(item).css('height', _c_height);
      var _chart = $(item).highcharts();
      if (_chart) {
        _chart.reflow();
        //_chart.redraw(); 
        WSILogger.Write('reflowing chart...');
      }
      //WSILogger.Write(idx + " " + $(item).attr('id') + " -> " + _c_height);
    });

  }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////



