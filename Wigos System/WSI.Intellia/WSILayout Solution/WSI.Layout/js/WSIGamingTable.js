﻿function WSIGamingTable() { 

    this.Name = null;
    this.Mesh = null;
    this.Visible = true;

    // Properties
    this.Properties = new WSIObjectProperties();

    this.Seats = new WSIGamingTableSeats();

}

WSIGamingTable.prototype.IsInitialized = function () {
    return (this.Mesh != null);
}

WSIGamingTable.prototype.ReplaceMaterial = function (Manager, OldMaterial, NewMaterial) {
    for (var oi = 0; oi < this.Mesh.material.materials.length; oi++) {
        if (this.Mesh.material.materials[oi].name == OldMaterial) {
            this.Mesh.material.materials[oi] = Manager.Materials.Items[NewMaterial];
        }
    }
}

WSIGamingTable.prototype.ChangeMaterialColor = function (MaterialName, NewColor) {
    //if (WSIData.ModelTypes != 2) {
    for (var oi = 0; oi < this.Mesh.material.materials.length; oi++) {
        if (this.Mesh.material.materials[oi].name == MaterialName) {
            this.Mesh.material.materials[oi].color = new THREE.Color(NewColor);
        }
    }
    //}
}

////////////////////////////////////////////////////////////////////////////

function WSIGamingTables() {

    this.Count = 0;
    this.Items = [];

    if (_LayoutMode == 1) {
        this.Pivot = new THREE.Object3D();
        this.Pivot.visible = true;
    }
}

WSIGamingTables.prototype.Build = function (callbakBuilder) {
    for (_gaming_table in this.Items) {
        callbakBuilder(this.Items[_gaming_table]);
    }
}