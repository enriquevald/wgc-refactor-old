﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WSIData
// 
//   DESCRIPTION: Contains flags for the SmartFloor processes.
// 
//        AUTHOR: Ramón Monclús
// 
// CREATION DATE: 2015-JAN-01
// 
//------------------------------------------------------------------------------

var WSIData = {

  // Activity enabled/disabled [REVIEW]
  Activity: false,

  // Activity speed
  ActivityInterval: 5000,

  Terminal_Status: [],

  // Heatmap points radius (size) for 2D and 3D
  radius2D: 50,
  radius3D: 40,

  // Heatmap points displacement (in case of orientation) for 2D and 3D
  displacementFactor2D: 0,
  displacementFactor3D: 0,

  // Determinates if heatmap points displacement must be used or snap it to the terminal center
  snapHeatmapToTerminal: false,

  // Rotation step for 3D [REVIEW]
  radius3DStep: 10,

  swap3DButtons: false,

  // Terminal type objects
  Terminals: [],
  Terminal_Labels: [],
  Terminal_Stats: [],

  // Island type objects
  Islands: [],

  // Areas type objects
  Areas: [],

  // Counters data
  Counters: [],

  //Textures: [],
  //Materials: [],

  // Data from terminals 
  TerminalsData: [],

  // Here goes the current selected object
  SelectedObject: null,

  //
  CurrentPlayerActivity: [],

  // Types of models to be shown [DEPRECATED]
  ModelTypes: 1,

  // Auto update Reports Yes/No [DEPRECATED]
  AutoUpdateReports: false,

  // Auto update Dashboard Yes/No
  AutoUpdateDashboard: false,

  // Draw yes/no floor information in background
  OnFloorActivity: false,
  // background information font size
  OnFloorActivityFontSize: "11",

  // Enable animations
  Animate: false,

  // Show stats fps plugin
  ShowFPS: false,

  // Opti-prog max number of machines to be update as once
  MaxMachines: 0,

  // Display or not the players
  ShowPlayers: false,

  //// HeatMap point position affination
  //TransparentOutOfRange: false,
  //TransparentOutOfRangeOpacity: 0.5,

  //// HeatMap point position affination
  TransparentOutOfRange: true,
  TransparentOutOfRangeOpacity: 0.2,

  // Lod distance
  LODDistance: 0,

  // Initial camera mode
  InitialCameraMode: 0, // CM_PERSPECTIVE: 0, CM_CENIT: 1, CM_FIRST_PERSON: 2

  // Default key rotation speed for first person controls 
  KeyRotateSpeed: 40,

  // Players Models
  MalePlayerChar: "Male 01",
  FemalePlayerChar: "Female 01",

  // Runners
  ShowRunners: true,
  RunnerChar: "Sample Runner 2",
  RunnerSphereRadius: 1,
  RunnerSphereSegments: 8,

  // Beacons
  ShowBeacons: true,

  // Dashboard with site data
  SiteDashboard: true

}

//------------------------------------------------------------------------------
