﻿// --------------------------------------------------------------------------------------- //
// SAVE FILTER DIALOG
// --------------------------------------------------------------------------------------- //

var m_current_simbol = "";

function SetDialogContent(Content) {
  $("#div_dialog_content").html(Content);
}

//function SetDialogContentFilterMode(Content) {
  //$("#div_dialog_content_2").html(Content);
//}

function ShowDialog() {
  $("#div_dialog_background").show();
}

function HideDialog() {
  return '$("#div_dialog_background").hide();';
}

function DialogCreateSimpleFilter(ID) {

  if (m_current_section != "80" && m_filter_list_selected.length == 0) {
    LayoutOkOpen(_Language.get('NLS_ERROR'), _Language.get('NLS_SELECT_A_FILTER'), function () { });
    return;
  }

  var _content = "";

  _content = _content + "<h3 style='padding:10px;' class='ui-accordion-header ui-state-default ui-corner-all ui-accordion-icons' data-nls='NLS_CREATE_FILTER'>" + _Language.get('NLS_CREATE_FILTER') + "</h3>";
  _content = _content + "<div class='nombreFiltro'>";
  _content = _content + " <label id='lbl_create_filter_name' data-nls='NLS_FILTER_NAME'>" + _Language.get('NLS_FILTER_NAME') + "</label>";
  _content = _content + " <input type='text' class='usuarioLogin' id='txt_create_filter_name' maxlength='50'></input>";
  _content = _content + " <label id='lbl_create_filter_error' style='color:red;'></label>";
  _content = _content + "</div>";
  _content = _content + "<div class='reproducirSonidoOpt'>";
  _content = _content + " <label id='lbl_create_filter_type' data-nls='NLS_PLAY_SOUND'>" + _Language.get('NLS_PLAY_SOUND') + "</label>";
  _content = _content + "<div class='switch'>";
  _content = _content + " <input class='cmn-toggle cmn-toggle-round' id='chkFilterSound' type='checkbox' checked>";
  _content = _content + " <label for='chkFilterSound'>";
  _content = _content + " </label>";
  _content = _content + "</div>";
  _content = _content + "</div>";

  _content = _content + "<div class='habilitarFiltro'>";
  _content = _content + " <label id='lbl_create_filter_type' data-nls='NLS_ENABLED'>" + _Language.get('NLS_ENABLED') + "</label>";
  _content = _content + "<div class='switch'>";
  _content = _content + " <input class='cmn-toggle cmn-toggle-round' id='chkFilterEnabled' type='checkbox' checked>";
  _content = _content + " <label for='chkFilterEnabled'>";
  _content = _content + " </label>";
  _content = _content + "</div>";

  _content = _content + "</div>";
  _content = _content + "<div class='rowBotonesFiltros'>";
  _content = _content + "<div class='btnGuardarFiltro' onclick='onClose_DialogCreateSimpleFilter()'><span data-nls='NLS_OK'>" + _Language.get('NLS_OK') + "</span></div>";
  //_content = _content + "<div class='btnFiltrarFiltro' onclick='" + HideDialog() + "' style=''><span data-nls='NLS_CANCEL'>" + _Language.get('NLS_CANCEL') + "</span></div>";
  _content = _content + "</div>";

  //SetDialogSize(25, 60);
  SetDialogContent(_content);
  ShowDialog();

  //Función para el cambio de opacidad cuando escogen un color

  $(".selectorColor li").on("click", function () {
    $(this).animate({ "opacity": "1" });
    $(this).siblings().animate({ "opacity": "0.3" });
    $(this).addClass("selectorColorSelected")
  });
}

//function onComboColorValueChanged() {
//  var combo_color = document.getElementById("sel_combo_color");
//  _filter_color = combo_color.options[combo_color.selectedIndex].value;

//  m_filter_selected_color = _filter_color;

//  $("#sel_combo_color").css("background-color", _filter_color);
//}

// --------------------------------------------------------------------------------------- //
// FILTER MODE DIALOG
// --------------------------------------------------------------------------------------- //

function CloseCreateFilterMode() {
  var $_filter = $("#filtermode_" + m_current_section);
  if ($(".btnCreaFiltro").hasClass("filtersActivated")) {
    $(".btnCreaFiltro").removeClass("filtersActivated");
    m_filter_mode_enabled = false;
    if ($_filter) {
      //filtermode disabled 
      $_filter.attr("data-filtermode", "0");
    }
  }
}

function DialogCreateFilterMode(SectionID) {
  var _content = "";

  m_filter_selected_color = undefined;

  _content = _content + "<div class='BtnClose close btnCloseWhite' id='btn_close_create_filter' >";
  _content = _content + "</div>";
  _content = _content + "<h3 style='padding:10px;' class='ui-accordion-header ui-state-default ui-corner-all ui-accordion-icons' data-nls='NLS_CREATE_FILTER'>" + _Language.get('NLS_CREATE_FILTER') + "</h3>";
  _content = _content + "<div class='div_filter_name'>";
  _content = _content + "<span data-nls='NLS_NAME'>" + _Language.get('NLS_NAME') + ":</span>";
  _content = _content + "<input class='srcJugadorPanel inputFilterName' id='input_new_filter_name' type='text' placeholder='' value='' data-nls='NLS_PLAYERS_SEARCH_PLACEHOLDER' maxlength='20'></input>";
  _content = _content + "</div>";

  _content = _content + "<br>";
  _content = _content + "<div id='div_filters_to_select'>";

  _content = _content + "<div id='div_filters_to_select_tab'>";
  _content = _content + "<div id='div_fts_btn_players'>";
  _content = _content + "<span data-nls='NLS_SAVE'>" + _Language.get('NLS_PLAYERS') + "</span>";
  _content = _content + "</div>";
  _content = _content + "<div id='div_fts_btn_machines'>";
  _content = _content + "<span data-nls='NLS_SAVE'>" + _Language.get('NLS_TERMINALS') + "</span>";
  _content = _content + "</div>";
  _content = _content + "</div>";
  _content = _content + " <div class='accordion' id='div_filter_list_players_acc'>";
  _content = _content + " </div>";
  _content = _content + " <div class='accordion' id='div_filter_list_machines_acc'>";

  _content = _content + " </div>";
  _content = _content + "</div>";
  _content = _content + "<div id='div_selected_filter_list'>";
  _content = _content + " <ul id='ul_selected_filter_list'>";
  _content = _content + " </ul>";
  _content = _content + "</div>";
  _content = _content + "<br/>";
  _content = _content + "<div class='btnsCrearFiltro'>";
  //_content = _content + " <div class='btnFiltrarFiltro' onclick='HideFilterModeDialog()'><span data-nls='NLS_FILTER'>" + _Language.get('NLS_FILTER') + "</span></div>";
  _content = _content + " <div class='btnGuardarFiltro btnGuardarFiltroPopUp' onclick='onClick_DialogCreateFilterMode()'><span data-nls='NLS_SAVE'>" + _Language.get('NLS_SAVE') + "</span></div>";
  _content = _content + "</div>";

  SetFilterModeDialogContent(_content);
  ShowFilterModeDialog();
  SlideControl();
  $(".selectorColor li").on("click", function () {
    $(this).animate({ "opacity": "1" });
    $(this).siblings().animate({ "opacity": "0.3" });
    $(this).addClass("selectorColorSelected")
  });

  //Print accordion
  PrintRanges("div_filter_list_players_acc", "60", _Legends.Items); //Players
  PrintRanges("div_filter_list_machines_acc", "70", _Legends.Items); //Machines

  $("#div_filter_list_machines_acc").hide();
  $("#div_fts_btn_players").addClass("div_fts_btn_players_On");

  $("#div_fts_btn_machines").click(function () {
    $("#div_filter_list_machines_acc").show();
    $("#div_filter_list_players_acc").hide();

    $("#div_fts_btn_machines").addClass("div_fts_btn_machines_On");
    $("#div_fts_btn_players").removeClass("div_fts_btn_players_On");
  });

  $("#div_fts_btn_players").click(function () {
    $("#div_filter_list_machines_acc").hide();
    $("#div_filter_list_players_acc").show();

    $("#div_fts_btn_machines").removeClass("div_fts_btn_machines_On");
    $("#div_fts_btn_players").addClass("div_fts_btn_players_On");
  });

  $("#btn_close_create_filter").click(function () {
    $("#div_dialog_background_filter_mode").hide();
    //if (m_current_section != 80) {
      onClickFilterMode(m_current_section);
    //}
    CloseCreateFilterMode();
  });
}

function onClick_DialogCreateFilterMode() {
  if (CreateFilterCheckData()) {
    //DialogCreateSimpleFilter(m_current_section.toString());
    onClose_DialogCreateSimpleFilter();
    CloseCreateFilterMode();
  }
    }

function CreateFilterCheckData() {
  if ($("#input_new_filter_name")[0].value == "") {
    LayoutOkOpen(_Language.get('NLS_ERROR'), _Language.get('NLS_NO_NAME'), function () { });

    return false;
  }

  if ($(".chk_filter_visualization.chk_filter_visualization_checked").length == 0) {
    LayoutOkOpen(_Language.get('NLS_ERROR'), _Language.get('NLS_ERROR_FILTER_DISPLAY'), function () { });

    return false;
  }

  return true;
}

function HideFilterModeDialog() {
  if (m_filter_mode_enabled) {
    var $combo_color = $(".selectorColor").find(".selectorColorSelected");

    if ($combo_color.length == 0) {
      LayoutOkOpen(_Language.get('NLS_ERROR'), _Language.get('NLS_SELECT_A_FILTER'), function () { });
      return;
    }

    m_filter_selected_color = $($combo_color[0]).css("background-color");

    $("#btn_save_filter_section_" + m_current_section).css("display", "block");
    m_apply_filter = true;

    // RMS - Update terminals only when 2D mode (3D will do it automatically on pre-render)
    if (_LayoutMode == 2) {
      // Update terminals when new filter is activated
      UpdateTerminals();
    }

    RefreshFilterListLegendDetailDialog(ParseListToCustomAlarm(m_filter_list_selected));
  } else {
    $("#div_dialog_content_2").empty();
  }

  $("#div_dialog_background_filter_mode").hide();
}

function SetFilterModeDialogContent(Content) {
  $("#div_dialog_content_2").html(Content);
}

function ShowFilterModeDialog() {
  $("#div_dialog_background_filter_mode").show();
}

function onClose_DialogCreateSimpleFilter() {

  var $_filter = $("#filtermode_" + m_current_section);

  SaveCustomAlarm();

  onClickFilterMode(m_current_section,true);
  CloseCreateFilterMode();

  //m_filter_mode_enabled = false;
  //HideFilterModeDialog();

  if (m_current_section != 80) {
    _UI.SetMenuActiveSection($("#a_section_80")[0], 80);
  }
  /*else {
    $("#div_dialog_content_2").empty();
    $("#div_dialog_background_filter_mode").hide();
    if ($_filter) {
      //filtermode disabled 
      $_filter.attr("data-filtermode", "0");
    }
  }*/
}

//Actualiza la lista de detalle de leyenda cuando seleccionamos una leyenda en cualquier sección menos Monitor.
function RefreshLegendDetailDialog(LegendId) {
  var _content = "";
  var $legend_detail = $("#div_legend_selected_detail");

  $legend_detail.empty();

  if (_Legends.Items[LegendId]) {
    if (_Legends.Items[LegendId].Nls)
    {
      _Legends.Items[LegendId].Name = _Language.get(_Legends.Items[LegendId].Nls);
    }

    _content += "<h1 class='tituloLeyenda'>" +_Legends.Items[LegendId].Name + "</h1>";

    for (idx_legendid_range in _Legends.Items[LegendId].Items) {
      if (_Legends.Items[LegendId].Items[idx_legendid_range].Nls) {
        _Legends.Items[LegendId].Items[idx_legendid_range].name = _Language.get(_Legends.Items[LegendId].Items[idx_legendid_range].Nls);
      }
      _content += "<div class='circuloValor' style='background: " + _Legends.Items[LegendId].Items[idx_legendid_range].color + ";'></div><h1 style='text-indent:20px;color: white;'>" + _Legends.Items[LegendId].Items[idx_legendid_range].name + "</h1>";
    }
  }
  $legend_detail.append(_content);
}

function RefreshListLegendDetailDialog() {
  /// <summary>
  /// Update detail legends list.
  /// </summary>

  var _content = "";
  var $legend_detail = $("#div_legend_selected_detail");
  var _current_legend = _Legends.Items[m_current_filter[m_current_section]];
  var _display_legend;
  var _sub_legend;
  var _display_id;

  $legend_detail.empty();
  if (_current_legend) {
    _content += "<div class='monitorFiltro'>";
    _content += "<h1 class='tituloLeyenda'>" + _current_legend.Name + "</h1>";

    for (_idx_legend_dialog_display in _current_legend.Display) {
      if (_current_legend.Display[_idx_legend_dialog_display]) {
        _display_id = _idx_legend_dialog_display;
        _display_legend = _current_legend.Display[_idx_legend_dialog_display];
        _content += "<h1 class='tituloCategoria tituloDisplay'>" + _display_legend.Name + "</h1>";
        for (_idx_legend_subitems_display in _display_legend.Items) {
          if (_display_legend.Items[_idx_legend_subitems_display]) {
            var _sub_item_display = _display_legend.Items[_idx_legend_subitems_display];
            _content += "<div class='circuloValor' style='background: " + _sub_item_display.color + ";margin-top: 1px;'></div><h1 class='monitorFiltroDisplay'>" + _sub_item_display.name + "</h1>";
          }
        }
      }
    }

    for (_idx_legend_dialog in _current_legend.Items) {
      if (_current_legend.Items[_idx_legend_dialog]) {
        _sub_legend = _current_legend.Items[_idx_legend_dialog];
        if (_display_id != _idx_legend_dialog) {
          _content += "<h1 class='tituloCategoria'>" + _sub_legend.Name + "</h1>";
          for (_idx_legend_subitems in _sub_legend.Items) {
            if (_sub_legend.Items[_idx_legend_subitems]) {
              var _sub_item = _sub_legend.Items[_idx_legend_subitems];
              _content += "<h1>" + _sub_item.name + "</h1>";
            }
          }
        }
      }
    }

    _content += "</div>"; //monitorFiltro
  }

  $legend_detail.append(_content);
}

//Actualiza la lista de detalle de leyenda cuando aplicamos un filtro en modo creación de filtros.
function RefreshFilterListLegendDetailDialog(ActiveLegends) {
  var _content = "";
  var $legend_detail = $("#div_legend_selected_detail");
  var _sub_legend;
  var _new_name;
  var _custom_legend;

  $legend_detail.empty();
  _content += "<div class='monitorFiltro'>";
  _content += "<div class='circuloValor' style='background: " + m_filter_selected_color + ";'></div><h1 style='color: white;' data-nls='NLS_CUSTOM_FILTER'>" + _Language.get('NLS_CUSTOM_FILTER') + "</h1>";

  for (_idx_activelegends in ActiveLegends) {
    var _Legend = ActiveLegends[_idx_activelegends];

    if (_sub_legend != _Legend.LegendId) {
      _content += "<h1 class='tituloCategoria'>" + _Legends.Items[_Legend.LegendId].Name + "</h1>";
      _sub_legend = _Legend.LegendId;
    }

    _custom_legend = m_filter_list_selected[_Legend.LegendId + "_" + _Legend.alarmid];

    if (_custom_legend.Operator != "") {
      _new_name = GetStringFromValues(_custom_legend.minValue, _custom_legend.maxValue, _custom_legend.Operator, m_field_properties[_Legends.Items[_Legend.LegendId].Property].type);
      _content += "<h1'>" + _new_name + "</h1>";
    } else {
      _content += "<h1'>" + _Legends.Items[_Legend.LegendId].Items[_Legend.alarmid].name + "</h1>";
    }
    _content += "</div>"; //monitorFiltro
  }

  $legend_detail.append(_content);
}

function GetStringFromValues(Value1, Value2, Operator, Type) {
  var _return;

  switch (Operator) {

    case ">":
      if (Type != Enum.EN_FIELD_TYPE.CURRENCY) {
        _return = +Value1 + "+";
      } else {
        _return = "$" + Value1 + "+";
      }
      break;

    case "<":
      if (Type != Enum.EN_FIELD_TYPE.CURRENCY) {
        _return = ">" + Value1;
      } else {
        _return = ">$" + Value1;
      }
      break;

    case "Between":
      if (Type != Enum.EN_FIELD_TYPE.CURRENCY) {
        _return = "" + Value1 + " - " + Value2;
      } else {
        _return = "$" + Value1 + " - $" + Value2;
      }
      break;

    case "=":
      if (Type != Enum.EN_FIELD_TYPE.CURRENCY) {
        _return = "" + Value1;
      } else {
        _return = "$" + Value1;
      }
      break;
  }

  return _return;
}

function SlideControl(Value1, Value2, MinValue, MaxValue) {
  CreateSlider("slider-range", true, MinValue, MaxValue, 0.1, Value1, Value2);
}

function CreateSlider(ControlName, Range, Min, Max, Step, Value1, Value2) {
  var _span_right, _span_left;

  $("#" + ControlName).empty();

  $("#" + ControlName).slider({
    range: Range,
    min: Min,
    step: Step,
    max: Max,
    values: [Value1, Value2],
    slide: function (event, ui) {
      $("#" + ControlName + "_amount_left").text("$" + ui.values[0]);
      $("#" + ControlName + "_amount_right").text("$" + ui.values[1]);

      $("#dialog_slider_min_value").text("$" + ui.values[0]);
      $("#dialog_slider_max_value").text("$" + ui.values[1]);
      //          $("#amount2").text("$" + ui.values[0] + " - $" + ui.values[1]);
    }
  });

  _span_right = "<div id='" + ControlName + "_amount_right' style='margin-top: -20px; position: fixed; font-size: 12px;'>" + m_current_simbol + "" + Value2 + "</div>";
  _span_left = "<div id='" + ControlName + "_amount_left' style='margin-top: -20px; position: fixed; font-size: 12px;'>" + m_current_simbol + "" + Value1 + "</div>";

  $("#" + ControlName).children().eq(1).append(_span_left);
  $("#" + ControlName).children().eq(2).append(_span_right);
}

function DialogSlider(LegendId, RangeId) {
  var _content = "";
  var _has_edited_data = (m_filter_list_selected[LegendId + "_" + RangeId].Operator != "");
  var _editable = _Legends.Items[LegendId].Items[RangeId].Editable;
  var _legend_min_value, _legend_max_value;

  //Get item from list of edited ranges if exists.
  if (_has_edited_data) {
    var _legend = ({
      minValue: m_filter_list_selected[LegendId + "_" + RangeId].minValue,
      maxValue: m_filter_list_selected[LegendId + "_" + RangeId].maxValue,
      Operator: m_filter_list_selected[LegendId + "_" + RangeId].Operator
    });

  } else {
    _legend = _Legends.Items[LegendId].Items[RangeId];
  }
  // to set a limit value into slider.
  var _last_element = _Legends.Items[LegendId].Items[_Legends.Items[LegendId].Items.length - 1];

  if (m_field_properties[_Legends.Items[LegendId].Property].type == Enum.EN_FIELD_TYPE.CURRENCY) {
    m_current_simbol = "$";
    _legend_min_value = _legend.minValue;
    _legend_max_value = _legend.maxValue;
  } else {
    m_current_simbol = "";
    _legend_min_value = parseFloat(_legend.minValue).toFixed(0);
    _legend_max_value = parseFloat(_legend.maxValue).toFixed(0);
  }

  $("#div_dialog_content").empty();

  var _between_visibility = (_legend.Operator == "Between") ? "visible" : "hidden";

  _content = _content + "<h3 style='padding:10px;' class='ui-accordion-header ui-state-default ui-corner-all ui-accordion-icons' data-nls='NLS_FILTER_EDIT'>" + _Language.get('NLS_FILTER_EDIT') + "</h3>";

  if (_editable == "True") {

    _content = _content + "<label id='dialog_slider_min_value' onclick='onClick_minValue();'>" + m_current_simbol + "" + _legend_min_value + "</label>";
    _content = _content + "<label id='dialog_separator' style='" + _between_visibility + "'> - </label>";
    _content = _content + "<label id='dialog_slider_max_value' onclick='onClick_maxValue();' style='" + _between_visibility + "'>" + m_current_simbol + "" + _legend_max_value + "</label>";
    //_content = _content + "<select id='sel_legend_operator'  onchange='onChange_sel_legend_operator();'>";
    //_content = _content + "<option value='>'>></option>";
    //_content = _content + "<option value='<'><</option>";
    //_content = _content + "<option value='='>=</option>";
    //_content = _content + "<option value='Between'>Between</option>";
    //_content = _content + "</select>";

    _content = _content + " <ul class='editCustomFilter'>";
    _content = _content + " <li class='filterBigger'>";
    _content = _content + "   <a>";
    _content = _content + "     <div class='filterIcon'></div>";
    _content = _content + "     <span>Mayor a</span>";
    _content = _content + "   </a>";
    _content = _content + " </li>";
    _content = _content + " <li class='filterLess'>";
    _content = _content + " <a>";
    _content = _content + "   <div class='filterIcon'></div>";
    _content = _content + "   <span>Menor a</span>";
    _content = _content + " </a>";
    _content = _content + " </li>";
    _content = _content + " <li class='filterEqual'>";
    _content = _content + " <a>";
    _content = _content + "   <div class='filterIcon'></div>";
    _content = _content + "   <span>Igual a</span>";
    _content = _content + " </a>";
    _content = _content + " </li>";
    _content = _content + " <li class='filterBetween'>";
    _content = _content + "   <a>";
    _content = _content + "   <div class='filterIcon'></div>";
    _content = _content + "   <span>Entre</span>";
    _content = _content + "   </a>";
    _content = _content + " </li>";
    _content = _content + " </ul>";

    _content = _content + "<br/>";
    _content = _content + "<div style='width: 400px;margin-right:150px;margin-left:40px;margin-top:40px;'>";
    _content = _content + "<div id='slider-range' style='border:1px solid #aaaaaa;display:none;'></div>";
    _content = _content + "</div>";
    _content = _content + "<br/>";
    _content = _content + "<div class='btnsCrearFiltro'>";
    _content = _content + "<div class='btnGuardarFiltro' onclick='SaveCustomRange(" + LegendId + "," + RangeId + ");'><a href='#' data-nls='NLS_SAVE'>" + _Language.get('NLS_SAVE') + "</a></div>";
    _content = _content + "</div>";
    _content = _content + "</div>";

    SetDialogContent(_content);
    ShowDialog();

    $(".editCustomFilter li").on("click", function () {
      $(this).animate({ "opacity": "1" });
      $(this).siblings().animate({ "opacity": "0.3" });
      $(".editCustomFilter li").removeClass("filterEditTypeSelected");
      $(this).addClass("filterEditTypeSelected");
      onChange_sel_legend_operator();
    });

    SlideControl(_legend.minValue, _legend.maxValue, 0, _last_element.minValue);

    switch (_legend.Operator) {
      case "Between":
        $(".filterBetween").addClass("filterEditTypeSelected");
        $(".filterBetween").animate({ "opacity": "1" });
        $(".filterBetween").siblings().animate({ "opacity": "0.3" });
        break;

      case "=":
        $(".filterEqual").addClass("filterEditTypeSelected");
        $(".filterEqual").animate({ "opacity": "1" });
        $(".filterEqual").siblings().animate({ "opacity": "0.3" });
        break;

      case ">":
        $(".filterBigger").addClass("filterEditTypeSelected");
        $(".filterBigger").animate({ "opacity": "1" });
        $(".filterBigger").siblings().animate({ "opacity": "0.3" });
        break;

      case "<":
        $(".filterLess").addClass("filterEditTypeSelected");
        $(".filterLess").animate({ "opacity": "1" });
        $(".filterLess").siblings().animate({ "opacity": "0.3" });
        break;
    }

    //$("#sel_legend_operator").val(_legend.Operator)

    onChange_sel_legend_operator();
  }
}

//********************************************************************************************************************************************************************
/************ DIALOG SLIDER EVENTS ***************/

function onBlur_minValue() {
  var txt = $("#dialog_slider_min_value").val();

  if (parseFloat(txt)) {
    $("#dialog_slider_min_value").replaceWith("<label id='dialog_slider_min_value' onclick='onClick_minValue();'></label>");
    $("#dialog_slider_min_value").text(m_current_simbol + parseFloat(txt));

    $("#slider-range").slider().slider("values", 0, parseFloat(txt));
    $("#slider-range_amount_left").text(m_current_simbol + parseFloat(txt));
  };
}

function onBlur_maxValue() {
  var txt = $("#dialog_slider_max_value").val();

  if (parseFloat(txt)) {
    $("#dialog_slider_max_value").replaceWith("<label id='dialog_slider_max_value' onclick='onClick_maxValue();'></label>");
    $("#dialog_slider_max_value").text(m_current_simbol + parseFloat(txt));

    $("#slider-range").slider().slider("values", 1, parseFloat(txt));
    $("#slider-range_amount_right").text(m_current_simbol + parseFloat(txt));
  }
}

function onClick_minValue() {
  var txt = $("#dialog_slider_min_value").text();
  $("#dialog_slider_min_value").replaceWith("<input id='dialog_slider_min_value' type='number' onblur='onBlur_minValue();' onkeypress='return WSIDialog_isValidNumberKey(event)'/>");
  $("#dialog_slider_min_value").val(txt.replace(m_current_simbol, ''));
  $("#dialog_slider_min_value").focus();
}

function onClick_maxValue() {
  var txt = $("#dialog_slider_max_value").text();
  $("#dialog_slider_max_value").replaceWith("<input id='dialog_slider_max_value' type='number' onblur='onBlur_maxValue();' onkeypress='return WSIDialog_isValidNumberKey(event)'/>");
  $("#dialog_slider_max_value").val(txt.replace(m_current_simbol, ''));
  $("#dialog_slider_max_value").focus();
}

function WSIDialog_isValidNumberKey(event) {

  return (event.charCode >= 48 && event.charCode <= 57) || event.charCode == 46 && ($(event.target).val().indexOf('.') == -1);
}

function onChange_sel_legend_operator() {

  onBlur_maxValue();
  onBlur_minValue();

  var _ctrl_operator_selector = $(".filterEditTypeSelected");
  var _txt_min_value = $("#slider-range_amount_left").text().replace(m_current_simbol, '');
  var _txt_max_value = $("#slider-range_amount_right").text().replace(m_current_simbol, '');

  //_legend_operator = _ctrl_operator_selector.options[_ctrl_operator_selector.selectedIndex].value;

  if (_ctrl_operator_selector.hasClass("filterBetween")) {
    if (parseFloat(_txt_max_value) < parseFloat(_txt_min_value)) {
      $("#slider-range").slider().slider("values", 0, 0);
      $("#slider-range_amount_left").text(m_current_simbol + 0.00);

      $("#slider-range").slider().slider("values", 1, parseFloat(_txt_min_value));
      $("#slider-range_amount_right").text(m_current_simbol + parseFloat(_txt_min_value));

      $("#dialog_slider_min_value").text(m_current_simbol + parseFloat(0));
      $("#dialog_slider_max_value").text(m_current_simbol + parseFloat(_txt_min_value));
    }

    $("#dialog_separator").css("visibility", "visible");
    $("#dialog_slider_max_value").css("visibility", "visible");
    $("#slider-range").css("visibility", "visible");

  } else {
    $("#dialog_separator").css("visibility", "hidden");
    $("#dialog_slider_max_value").css("visibility", "hidden");
    $("#slider-range").css("visibility", "hidden");
  }
}

//********************************************************************************************************************************************************************

function SaveCustomRange(LegendId, RangeId) {
  var _edited_range = m_filter_list_selected[LegendId + "_" + RangeId];
  var _legend_range = _Legends.Items[LegendId].Items[RangeId];
  var _ctrl_operator_selector = document.getElementById("sel_legend_operator");
  var _legend_operator;
  var _min_value, _max_value;

  var _ctrl_operator_selector = $(".filterEditTypeSelected");

  if ($(".filterEditTypeSelected").hasClass("filterBetween")) {
    _legend_operator = "Between";
  } else if ($(".filterEditTypeSelected").hasClass("filterEqual")) {
    _legend_operator = "=";
  } else if ($(".filterEditTypeSelected").hasClass("filterBigger")) {
    _legend_operator = ">";
  } else {
    _legend_operator = "<";
  }

  //_legend_operator = _ctrl_operator_selector.options[_ctrl_operator_selector.selectedIndex].value;

  _min_value = $("#dialog_slider_min_value").text().replace(m_current_simbol, '');
  _max_value = $("#dialog_slider_max_value").text().replace(m_current_simbol, '');

  _edited_range.Operator = _legend_operator;
  _edited_range.minValue = parseFloat(_min_value);

  if (_legend_operator == "Between") {
    _edited_range.maxValue = parseFloat(_max_value);

    if (_edited_range.minValue > _edited_range.maxValue) {
      alert("Filtro incorrecto")
      return;
    }
  }
  else {
    _edited_range.maxValue = 0;
  }

  $("#div_dialog_content").empty();
  $("#div_dialog_background").hide();

  var _item = m_filter_list_selected[LegendId + "_" + RangeId];

  if (_item != null && _item.operator != "") {
    $("#list_range_id_" + RangeId + "[data-legendid='" + LegendId + "']").text(GetStringFromValues(_item.minValue, _item.maxValue, _item.Operator));
  }
}

function OpenRunnerDialog(RunnersList) {
  $("#div_runner_list").empty();
  event.preventDefault();
  var _str = "";
  var _type = "";

  for (_idx_runner in RunnersList) {
    var _runner = RunnersList[_idx_runner];
    if (_runner.source=="1") {
      _type = "Devantenna";
    } else {
      _type = "Devbeacon";
    }

    _str += "<li class='" + _type + "' onclick='OnSelectedRunnerDialog(" + _idx_runner + ")'><span>" + _runner + "</span></li>";
  }

  $("#div_runner_list").html(_str);

  $("#div_runner_selector").show();
}


function SetRunnerDialogContent(Content) {
    $("#div_dialog_content_3").html(Content);
}


function ShowRunnerDialog() {
    $("#div_dialog_background_runners").show();
}

function HideRunnerDialog() {

    _Manager.RunnersMap.RunnerSelected = null;
    _Manager.RunnersMap.ForcedUpdate();

    $("#div_dialog_content_3").empty();
    $("#div_dialog_background_runners").hide();
}



function clickRunnerDialog(id) {

    OnRunnerClick(id);
    HideRunnerDialog();
    OpenInfoPanel();
}




function DialogRunner(runners) {

    _Manager.RunnersMap.ForcedUpdate();

    var ids = Object.keys(runners);
    ids.pop("count");

    var _content = "";
    
    _content = _content + "<div class='BtnClose close btnCloseWhite' id='btn_close_runner' >";
    _content = _content + "</div>";
    _content = _content + "<h3 style='padding:10px; min-height: 20px;' class='ui-accordion-header ui-state-default ui-corner-all ui-accordion-icons' data-nls='NLS_RUNNERS_IN_THE_ZONE'>" + _Language.get('NLS_RUNNERS_IN_THE_ZONE') + "</h3>";
    
    _content = _content + "<div class='runnersContainer'>";
    
    var colWidth = 100 / ids.length; 

    if (ids.length > 3)
        var colWidth = 100 / 3;

    for (i in ids) {
        _content = _content + "<div onclick='clickRunnerDialog(" + ids[i] + ");' style='width:" + colWidth + "%' class='datosRunnerSelector'>";
        _content = _content + "      <img class='photoRunner' src='design/img/ico_photo_male.png' />";
        _content = _content + "    <div class='nombreRunnerSelector'>" + runners[ids[i]] + "</div>";
        _content = _content + "</div>";
    }

    _content = _content + "</div>";

    SetRunnerDialogContent(_content);
    ShowRunnerDialog();

    $("#btn_close_runner").click(function () {
        HideRunnerDialog();
    });
    
}


// --------------------------------------------------------------------------------------- //