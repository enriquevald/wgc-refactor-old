﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WSICommonFormat.js
// 
//   DESCRIPTION: Format strings functions.
// 
//        AUTHOR: Ramón Monclús
// 
// CREATION DATE: 16-SEP-2015
// 
//------------------------------------------------------------------------------

var EN_FORMATS = {
  F_STRING: 0,
  F_MONEY: 1,
  F_PERCENT: 2,
  F_TIME: 3,
  F_NUMBER: 4,
  F_INTEGER: 5
}

var m_default_string_format = EN_FORMATS.F_STRING;

var m_format_language = "MX";
var m_format_currency = "MXN";
var m_format_fraction_digits = 2;
var m_format_currency_display = "symbol";
var m_format_use_k = true;

var m_format_currency_symbol = "$";
var m_format_currency_symbol_before = true;

function GetStringFormat($Element) {
  var _result = EN_FORMATS.F_STRING;
  if ($Element) {
    _result = $Element.data("property-type");
    return (_result == undefined) ? m_default_string_format : +_result;
  } else {
    return _result;
  }
}

function FormatString(Value, Type, Param1) {
  m_format_language = _Manager.GeneralParams["RegionalOptions.CountryISOCode2"];
  switch (Type) {
    case EN_FORMATS.F_STRING:
      return Value;
      break;
    case EN_FORMATS.F_MONEY:
    case EN_FORMATS.F_NUMBER:
    case EN_FORMATS.F_INTEGER:
      return ParseK(Value, Type, Param1);
      break;
    case EN_FORMATS.F_PERCENT:
      return FormatPercent(Value, Param1);
      break;
    case EN_FORMATS.F_TIME:
      return FormatTime(Value, Param1);
      break;
    default:
      return Value;
      break;
  }
}

function ParseK(Value, Type, Param1) {
  var _result = 0,
      _has_k = false;

  if (m_format_use_k && Value > 1000000) {
    Value = Value / 1000;
    Param1 = 0;
    _has_k = true;
  }

  switch (Type) {
    case EN_FORMATS.F_MONEY:
      _result = FormatMoney(Value, Param1);
      break;
    case EN_FORMATS.F_NUMBER:
      _result = FormatNumber(Value, Param1);
      break;
    case EN_FORMATS.F_INTEGER:
      _result = FormatInteger(Value, Param1);
      break;
    default:
      return Value;
      break;
  }

  return _result + (_has_k ? "K" : "");
}

function FormatMoney(Value, Decimals) {
  //var _formatter = new Intl.NumberFormat(m_format_language, {
  //  style: 'currency',
  //  currency: m_format_currency,
  //  minimumFractionDigits: Decimals != undefined ? Decimals : m_format_fraction_digits,
  //  maximumFractionDigits: Decimals != undefined ? Decimals : m_format_fraction_digits,
  //  currencyDisplay: m_format_currency_display
  //});
  var _result = "";
  var _formatter = new Intl.NumberFormat(m_format_language, {
    style: 'decimal',
    minimumFractionDigits: Decimals != undefined ? Decimals : m_format_fraction_digits,
    maximumFractionDigits: Decimals != undefined ? Decimals : m_format_fraction_digits
  });
  _result = _formatter.format(Value);
  if (!m_format_currency_symbol_before) {
    _result = _result + m_format_currency_symbol;
  } else {
    _result = m_format_currency_symbol + _result;
  }
  return _result;
}

function FormatNumber(Value, Decimals) {
  var _formatter = new Intl.NumberFormat(m_format_language, {
    style: 'decimal',
    minimumFractionDigits: Decimals != undefined ? Decimals : m_format_fraction_digits,
    maximumFractionDigits: Decimals != undefined ? Decimals : m_format_fraction_digits
  });

  return _formatter.format(Value);
}

function FormatInteger(Value, Decimals) {
  var _formatter = new Intl.NumberFormat(m_format_language, {
    style: 'decimal',
    minimumFractionDigits: 0,
    maximumFractionDigits: 0
  });

  return _formatter.format(Value);
}

function FormatPercent(Value, Decimals) {
  var _formatter = new Intl.NumberFormat(m_format_language, {
    style: 'percent',
    minimumFractionDigits: Decimals != undefined ? Decimals : m_format_fraction_digits,
    maximumFractionDigits: Decimals != undefined ? Decimals : m_format_fraction_digits
  });

  return _formatter.format(Value / 100); //Math.pow(10, m_format_fraction_digits));
}

function FormatTime(Value) {
  var _hour12_value = true;

  switch (m_format_language)
  {
    case "ES":
    case "es-ES":
      _hour12_value = false;
      break;

    default:
      _hour12_value = true;
  }

  return Value.toLocaleTimeString(m_format_language, {
    hour12: _hour12_value
  });
}