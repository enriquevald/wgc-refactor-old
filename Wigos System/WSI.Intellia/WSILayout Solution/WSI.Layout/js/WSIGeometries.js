﻿function WSIGeometry() {

    this.type = 0;
    this.subtype = 0;
    this.geometry = null;
}

function WSIGeometries() {

    this.Items = new Object();

}

WSIGeometries.prototype.Load = function (geometryLevel) {

    if (!geometryLevel) {
        geometryLevel = 1;
    }

    switch (geometryLevel) {

        

        default:    // 1
            // Terminals
            this.Items["Terminal_1"] = this.CreateDefaultTerminal(1);



    }

    // Selector
    this.Items["Selector"] = this.CreateDefaultSelector(1);

}

// TERMINALS

WSIGeometries.prototype.CreateDefaultTerminal = function (type) {

    switch (type) {
        
        default:
            // Default
            var _result = new THREE.BoxGeometry(3.70, 4, 3.70)
            _result.applyMatrix(new THREE.Matrix4().makeTranslation(0, 2, 0));
            return _result
            break;
    }

}

// SELECTOR

WSIGeometries.prototype.CreateDefaultSelector = function (type) {

    switch (type) {

        default:
            // Default
            //return new THREE.BoxGeometry(4, 8.5, 4);
            return new THREE.BoxGeometry(4, 4.5, 4);
            break;
    }

}

WSIGeometries.prototype.CreateAlarmSignalBase = function () {

    //return new THREE.CylinderGeometry(1.75, 0, 7.5, 3, true);
    return new THREE.CylinderGeometry(0.5, 0.5, 17.5, 6, true);     
}

WSIGeometries.prototype.CreateAlarmSignalItem = function () {

    //return new THREE.CylinderGeometry(2, 2, 3, 4, true);
    return new THREE.BoxGeometry(3.5, 3.5, 3.5);
}

WSIGeometries.prototype.CreateAlarmSignalItem2 = function () {

    return new THREE.BoxGeometry(0.1, 1.7, 1.7);    
}
