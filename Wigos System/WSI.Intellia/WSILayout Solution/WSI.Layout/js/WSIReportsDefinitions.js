﻿

///////////////////////////////////////////////////////////////////////////////////////////////////////////

// Report Types - part of stored procedure identifier
var m_reports_types = ["Statistics", "Occupation", "Cash", "Segmentation"];

// Group of concepts for each report type
var m_report_concepts = [
// Statistics
                        ["Played", "NetWin", "Payable NW"],
// Occupation
                        ["Terminals"],
// Cash
                        [],
// Segementation
                        ["Clients", "Visits"]

                        ];

///////////////////////////////////////////////////////////////////////////////////////////////////////////

// To set each report descriptions
var m_report_descriptions = [
// Statistics
                        ['NLS_REPORT_STATS_TOP_10', 'NLS_REPORT_STATS_BOTTOM_10', 'NLS_REPORT_STATS_PROVIDERS', 'NLS_REPORT_STATS_ACCUM', 'NLS_REPORT_STATS_PERIOD'],
// Occupation
//[m_empty_report_occ, m_empty_report_level, m_empty_report_age, m_empty_report_gend, m_report_occupation_provider7],
                        ['NLS_REPORT_OCCUP_GAUGE', 'NLS_REPORT_OCCUP_OCC', 'NLS_REPORT_OCCUP_LVL', 'NLS_REPORT_OCCUP_AGE', 'NLS_REPORT_OCCUP_GEN', 'NLS_REPORT_OCCUP_PROV7', 'NLS_REPORT_OCCUP_PROV30'],
// Cash
//[m_report_cash_cash_in_t, m_report_cash_cash_in_b, m_report_cash_cash_in],
                        ['NLS_REPORT_CASH_RESULT', 'NLS_REPORT_CASH_UNBALANCED', 'NLS_REPORT_CASH_CASHIERS'],
// Segementation
                        ['NLS_REPORT_SEGM_LVL', 'NLS_REPORT_SEGM_AGE', 'NLS_REPORT_SEGM_ADT']
                      ];

///////////////////////////////////////////////////////////////////////////////////////////////////////////

var _report_0 = {
  chart: {
    type: 'column',
    backgroundColor: null
  },

  legend:
         {
           enabled: true
         },

  title: {
    text: '',
    style: { "color": "#333333", "fontSize": "12px" }
  },

  tooltip: {
    //headerFormat: '<b>{point.key}</b><br>',
    //pointFormat: '<span style="color:{series.color}">\u25CF</span>',
    formatter: function () {
      if (this.series.name != "Average" && this.series.name != "Media") {
        return this.x + ' : ' + this.series.name + " -> " + this.y;
      }
      return false;
    }
  },

  plotOptions: {
    series: {
      dataLabels: {
        enabled: true,
        useHtml: true,
        formatter: function () {
          if (this.y != 0) {
            if (this.series.name != "Average" && this.series.name != "Media") {
              return Highcharts.numberFormat(parseInt(this.y * 100) / 100);
            }
          }
          return null;
        },
        style: {
          color: '#000000'
        }
      }
    }
  },

  series: []
};

var _report_1 = {
  chart: {
    type: 'column',
    backgroundColor: null
  },

  legend:
         {
           enabled: false
         },

  title: {
    text: '',
    style: { "color": "#333333", "fontSize": "12px" }
  },

  subtitle: {
    text: ''
  },

  xAxis: {
  },

  yAxis: {
    title: {
      text: ''
    }
  },

  tooltip: {
    headerFormat: '<b>{point.key}</b><br>',
    pointFormat: '<span style="color:{series.color}">\u25CF</span> {series.name}: {point.y} / {point.stackTotal}',
    formatter: function () {
      if (this.series.name != "Average" && this.series.name != "Media") {
        return this.x + ' : ' + this.series.name + " -> " + this.y;
      }
      return false;
    }
  },

  plotOptions: {
    series: {
      dataLabels: {
        enabled: true,
        formatter: function () {
          if (this.y != 0) {
            if (this.series.name != "Average" && this.series.name != "Media") {
              return parseCurrency(this.y); // Highcharts.numberFormat(parseInt(this.y * 100) / 100);
            }
          }
          return null;
        },
        style: {
          color: '#000000',
          fontWeight: 'bold'
        },
        useHtml: true
      }
    }
  },

  series: []
};

var _report_3 = {
  chart: {
    type: 'pie',
    options3d: {
      enabled: true,
      alpha: 45
    }
  },
  title: {
    text: '',
    style: { "color": "#333333", "fontSize": "12px" }
  },

  subtitle: {
    text: '',
    style: { "fontStyle": "italic" }
  },

  tooltip: {
    headerFormat: '<b>{point.key}</b><br>',
    pointFormat: '<span style="color:{series.color}">\u25CF</span> {series.name}: {point.y} / {point.stackTotal}',
    formatter: function () {
      if (this.series.name != "Average" && this.series.name != "Media") {
        return this.x + ' : ' + this.series.name + " -> " + this.y;
      }
      return false;
    }
  },
  plotOptions: {
    pie: {
      size: '90%',
      allowPointSelect: true,
      cursor: 'pointer',
      depth: 20,
      dataLabels: { enabled: false },
      showInLegend: true,
      point: {
        events: {
          legendItemClick: function () { return false; }
        }
      }
    }
  },
  legend: {
    enabled: true,
    labelFormatter: function () {
      return this.name + " (" + parseInt(this.percentage * 10) / 10 + "% - " + this.y + ")";
    },
    itemStyle: { fontSize: '9px', fontWeight: 'normal' }
  },
  series: []
};

var _report_gauge = {

  chart: {
    type: 'solidgauge'
  },

  title: {
    text: '',
    style: { "color": "#333333", "fontSize": "12px" }
  },

  subtitle: {
    text: '',
    style: { "fontStyle": "italic" }
  },

  pane: {
    center: ['50%', '85%'],
    size: '100%',
    startAngle: -90,
    endAngle: 90,
    background: {
      //backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
      innerRadius: '60%',
      outerRadius: '100%',
      shape: 'arc'
    }
  },

  tooltip: {
    enabled: false
  },

  plotOptions: {
    solidgauge: {
      dataLabels: {
        y: 5,
        borderWidth: 0,
        useHTML: true
      }
    }
  },

  yAxis: {
    stops: [[0.1, '#55BF3B'], [0.5, '#DDDF0D'], [0.9, '#DF5353']],
    lineWidth: 0,
    minorTickInterval: null,
    tickPixelInterval: 400,
    tickWidth: 0,
    title: { y: -70 },
    labels: { y: 16 },
    min: 0,
    max: 100
  },

  credits: {
    enabled: false
  },

  series: [{
    name: 'Real time Occupation',
    data: [0],
    dataLabels: {
      useHtml: true,
      format: '<div style="text-align:center"><span style="font-size:25px;color:' +
                    ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
                       '<span style="font-size:12px;color:silver">%</span></div>'
    },
    tooltip: {
      valueSuffix: '%'
    }
  }]
};

//
var _report_4 = {

  chart: {
    type: 'column'
  },

  title: {
    text: '',
    style: { "color": "#333333", "fontSize": "12px" }
  },

  subtitle: {
    text: ''
  },

  xAxis: {
    categories: [],
    labels: {
      rotation: -90
    },
    title: {
      text: ''
    }
  },

  yAxis: {
    allowDecimals: false,
    title: {
      text: ''
    }
  },

  tooltip: {
    formatter: function () {
      if (this.series.name != "Average" && this.series.name != "Media") {
        return '<b>' + this.x + '</b><br/>' +
                    this.series.name + ': ' + this.y + '<br/>' +
                    'Total: ' + this.point.stackTotal;
      }
      return false;
    }
  },

  plotOptions: {
    column: {
      stacking: 'normal'
    }
  },

  series: []
};

var _report_5 = {
  chart: {
    type: 'column'
  },
  colors: ['#000000', '#666666', '#660066'],
  title: {
    text: '',
    style: { "color": "#333333", "fontSize": "12px" }
  },
  subtitle: {
    text: ''
  },
  xAxis: {
    type: 'category',
    labels: {
      rotation: -90
    }
  },
  yAxis: {
    title: { text: 'Zoomable' }
  },
  legend: {
    enabled: true
  },
  plotOptions: {
    column: {
      dataLabels: {
        useHtml: true,
        rotation: -90
      }
    },
    series: {
      borderWidth: 0,
      dataLabels: {
        enabled: true,
        align: 'left',
        rotation: 270,
        style: { color: '#000000', fonWeight: 'bold' },
        useHtml: true,
        x: 5,
        formatter: function () { return Round(this.y, 2); }
      }
    }
  },
  series: []
};

