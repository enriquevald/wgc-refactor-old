﻿/*
var cssTransitionsSupported = false;
(function() {

var div = document.createElement('div');

div.setAttribute('style', 'transition:top 1s ease;-webkit-transition:top 1s ease;-moz-transition:top 1s ease;');

cssTransitionsSupported = !!(div.style.transition || div.style.webkitTransition || div.style.MozTransition);

delete div;

})();
*/

/////////////////////////////////////////////////////////////////////////////////////

var _Manager = new WSIManager();
/////////////////////////////////////////////////////////////////////////////////////

var CONST_INITIAL_ZOOM = "0.6";

var m_terminal_id_selected = null;
var m_browser;
var m_movements_timeout;
var m_movements_speed = 1;
var m_current_zoom = 1.0;
var m_current_background_position_left = 0;
var m_current_background_position_top = 0;
var m_current_popup_background_position_left = 0;
var m_current_popup_background_position_top = 0;
var m_current_terminal_position_left = [];
var m_current_terminal_position_top = [];
//var m_activity_sleep = 1000;

var m_div_background_is_clicked = false;
var m_terminal_clicked = null;
var m_popup_infoWindow_is_clicked = false;
var m_current_mouse_position_x;
var m_current_mouse_position_y;
_Configuration.ShowGrid = true;

var m_left_button_down = false;

var m_is_moving = false;
var m_start_left = null;
var m_start_top = null;
var m_old_left = null;
var m_old_top = null;

var m_is_rotating = false;
var m_start_angle = null;
var m_old_angle = null;

var m_changed_objects = [];
var m_changed_items = [];

var m_islands_positions;
m_islands_positions = [ // posX, posY, width, height
[-86, 2, 4, 1],
[-5, 2, 4, 1],
[20, 2, 4, 1],
[-88, 21, 2, 2],
[-68, 21, 2, 2],
[-46, 21, 2, 2],
[-26, 21, 2, 2],
[-1, 21, 2, 2],
[22, 21, 2, 2],
[-68, 38, 2, 4],
[-48, 38, 2, 4],
[-24, 38, 2, 4],
[-89, 62, 2, 3],
[-68, 62, 2, 3],
[-48, 62, 2, 3],
[-27, 62, 2, 3],
[-29, 84, 4, 1],
[-52, 84, 4, 1],
[-71, 84, 3, 1],
[-90, 84, 3, 1],
[-105, 84, 2, 1],
[36, 36, 4, 4],
[101, 18, 4, 1],
[122, -6, 1, 4],
[122, -28, 1, 4],
[146, -6, 2, 4],
[168, -6, 2, 4],
[146, 17, 2, 4],
[169, 19, 2, 4],
[146, 41, 2, 3],
[169, 41, 2, 3],
[101, 41, 2, 3],
[120, 41, 2, 3],
[146, 62, 2, 3],
[169, 62, 2, 3],
[101, 62, 2, 3],
[120, 62, 2, 3],
[80, 43, 2, 3],
[97, 84, 8, 1],
[138, 84, 4, 1],
[161, 84, 4, 1],
[136, -32, 3, 1],
[152, -32, 3, 1],
[168, -32, 3, 1],
[191, -10, 1, 3],
[190, 7, 1, 3],
[75, 78, 4, 3]
];


var m_areas_positions;
m_areas_positions = [ // posX, posY, width, height
[-172, 16, 16, 18],
[-106, 0, 24, 23],
[-8, 0, 18, 14],
[68, -20, 12, 8],
[119, -35, 20, 28],
[69, 16, 12, 15],
[69, 80, 28, 3]
];


///////////////////////////////////////////////////////////////////////


function fire(e) { }

// Routine to launch mouse event from touch events
function touchHandler(event) {
  var touches = event.changedTouches,
      first = touches[0],
      type = "";

  switch (event.type) {

    case "touchstart": type = "mousedown"; break;
    case "touchmove": type = "mousemove"; break;
    case "touchend": type = "mouseup"; break;
    default: return;
  }

  var simulatedEvent = document.createEvent("MouseEvent");
  simulatedEvent.initMouseEvent(type, true, true, window, 1,
                        first.screenX, first.screenY,
                        first.clientX, first.clientY, false,
                        false, false, false, 0/*left*/, null);

  first.target.dispatchEvent(simulatedEvent);
  event.preventDefault();
}

// Initialization routine
function init() {

  // Create event handles for touchable devices
  var _control = document.getElementById("div_background");
  if (_control) {
    _control.addEventListener("touchstart", touchHandler, true);
    _control.addEventListener("touchmove", touchHandler, true);
    _control.addEventListener("touchend", touchHandler, true);
    _control.addEventListener("touchcancel", touchHandler, true);
  }

  //Create Ribbon Toolbar
  CreateRibbonToolbar("ribbon");



}

/////////////////////////////////////////////////////////////////////

// Method to execute when document is ready
$(document).ready(function () {

  // Set detected browser
  browserDetect();

  // Initialization
  init();

  // Set background initial position
  SetDivBackgroundToInitialPosition();

  // Initialize background size
  $("#div_background").css("width", CONST_DIV_BACKGROUND_WIDTH);
  $("#div_background").css("height", CONST_DIV_BACKGROUND_HEIGHT);

  // Create visual grid
  CreateGrid(25);

  // Initialize heatmap position
  $("#div_heatmapArea").css("top", -(CONST_DIV_BACKGROUND_HEIGHT / 2));
  $("#div_heatmapArea").css("left", -(CONST_DIV_BACKGROUND_WIDTH / 2));

  // Set document events

  // On document mouse button down we save the global mouse position
  $(document).mousedown(function (e) {
    UpdateMousePosition(e);
    return true;
  });

  // On documetn mouse button up:
  //  - Stop background moving
  //  - Stop object moving
  $(document).mouseup(function (e) {
    m_div_background_is_clicked = false;
    if (m_terminal_clicked != null) {
      EndMove();
      $("#txtObjectId").val(m_terminal_clicked.id.replace("terminal_", ""));
    }
    //m_terminal_clicked = null;
    return true;
  });

  // On document mouse move
  $(this).mousemove(function (e) {
    if (m_div_background_is_clicked) {
      UpdateBackgroundPosition(e.pageX, e.pageY);
    } else {
      if (m_terminal_clicked != null) {
        if (m_is_moving) {
          UpdateObjectPosition(e.pageX, e.pageY, e.ClientX, e.ClientY);
        }
      }
    }
    UpdateMousePosition(e);
    return true;
  });

  // On document mousewheel scroll/zoom
  $(this).on('DOMMouseScroll mousewheel', function (e) {
    var _zoom_movement;
    e.preventDefault();
    if (!m_is_moving) {
      switch (m_browser) {
        case "Chrome":
        case "Opera":
        case "Safari":
          if (e.originalEvent.wheelDeltaY < 0) {
            _zoom_movement = CONST_MOVEMENT_TYPE_ZOOM_MINUS;
          }
          else {
            _zoom_movement = CONST_MOVEMENT_TYPE_ZOOM_MORE;
          }
          break;
          //IE and Firefox                                                                                        
        default:
          if (e.originalEvent.wheelDelta < 0) {
            _zoom_movement = CONST_MOVEMENT_TYPE_ZOOM_MINUS;
          }
          else {
            _zoom_movement = CONST_MOVEMENT_TYPE_ZOOM_MORE;
          }
      }
      DivPosition_Transform($("#div_background"), _zoom_movement, CONST_MOVEMENT_MOUSE);
    }
    return false;
  });

  // Background events

  // On background mouse down
  $("#div_background").mousedown(function (e) {
    if (!m_is_moving) {
      m_div_background_is_clicked = true;
      return false;
    }
  });

  // On background mouse up
  $("#div_background").mouseup(function (e) {
    m_div_background_is_clicked = false;
    return false;
  });

  // Parse Language
  _Language.parse();

});  // end ready

////////////////////////////////////////////////////////////////////////////////////////

function CreateDivAreas() {
  for (var _i = 0; _i < m_areas_positions.length; _i++) {
    var _new_div;

    _new_div = document.createElement("div");
    $(_new_div).addClass("div_area")
        .attr("id", CONST_ID_AREA + _i)
        .css("left", (m_areas_positions[_i][0] * 5) - 5 + "px")
        .css("top", (m_areas_positions[_i][1] * 5) - 5 + "px")
        .css("width", (m_areas_positions[_i][2] * 20) + "px")
        .css("height", (m_areas_positions[_i][3] * 20) + "px");

    $(_new_div).click(function (e) {
      AreaClickEvent(e, this);
    });

    $("#div_center").append($(_new_div));

  }
}  // CreateDivAreas

////////////////////////////////////////////////////////////////////////////////////////

function CreateDivIslands() {
  for (var _i = 0; _i < m_islands_positions.length; _i++) {
    var _new_div;

    _new_div = document.createElement("div");
    $(_new_div).addClass("div_island")
        .attr("id", CONST_ID_ISLAND + _i)
        .css("left", (m_islands_positions[_i][0] * 5) - 10 + "px")
        .css("top", (m_islands_positions[_i][1] * 5) - 10 + "px")
        .css("width", (m_islands_positions[_i][2] * 20) + "px")
        .css("height", (m_islands_positions[_i][3] * 20) + "px");

    $(_new_div).click(function (e) {
      IslandClickEvent(e, this);
    });

    $("#div_center").append($(_new_div));

  }
} // CreateDivIslands

////////////////////////////////////////////////////////////////////////////////////////

function CreateTerminal(Terminal, callbackLabelBuilder) {
  var _css_transform;
  var _terminal_rotate_deg;

  _css_transform = GetPropertyName_Transform();

  var _new_div;

  _new_div = document.createElement("div");

  if (Terminal.Properties.lo_id == 1331) {
    var _w = 1;
  }

  $(_new_div).addClass("div_terminal")
          .attr("id", CONST_ID_TERMINAL + Terminal.Properties.lo_id)
          .css("top", PositionToCoord(Terminal.Properties.lop_z) + "px")
          .css("left", PositionToCoord(Terminal.Properties.lop_x) + "px")
          .css("z-index", 2);

  $(_new_div).mousedown(function (e) {
    m_current_mouse_position_x = e.pageX;
    m_current_mouse_position_y = e.pageY;
    m_left_button_down = (e.which == 1);
    e.preventDefault();
    TerminalClickEvent(e, this, false);
    m_terminal_clicked = this;
    $("#txtObjectId").val(m_terminal_clicked.id.replace("terminal_", ""));
    StartMove(e, this);
    SetAnglepickerValue();
    SetCoordsValue();
  });

  $(_new_div).mouseup(function (e) {
    m_left_button_down = false;
    e.preventDefault();
    if (m_terminal_clicked != null) {
      EndMove(e, this);
      //m_terminal_clicked = null;
      SetCoordsValue();
    }
  });

  $(_new_div).mouseleave(function (e) {
    //e.preventDefault();
    //EndMove(e, this);
  });

  _terminal_rotate_deg = GetDegrees(Terminal.Properties.lop_orientation);
  $(_new_div).css(_css_transform, "rotate(" + _terminal_rotate_deg + ")");

  $("#div_center").append($(_new_div));

  if (callbackLabelBuilder) {
    callbackLabelBuilder(Terminal);
  }

  $(_new_div).attr("position_left", $(_new_div).css("left"));
  $(_new_div).attr("position_top", $(_new_div).css("top"));
  $(_new_div).attr("rotate_value", _terminal_rotate_deg);
  $(_new_div).attr("translate_value", "0px, 0px");
}

////////////////////////////////////////////////////////////////////////////////////////

function CreateDivTerminales() {
  _Manager.Terminals.Build(CreateTerminal, undefined);
} // CreateDivTerminales

////////////////////////////////////////////////////////////////////////////////////////

//Position: 0, 1, 2, 3, 4, 5, 6, 7
function GetDegrees(Position) {
  var _degrees;

  switch (Position) {
    case 0:
      _degrees = 0; break;
    case 1:
      _degrees = 45; break;
    case 2:
      _degrees = 90; break;
    case 3:
      _degrees = 135; break;
    case 4:
      _degrees = 180; break;
    case 5:
      _degrees = 225; break;
    case 6:
      _degrees = 270; break;
    case 7:
      _degrees = 315; break;
    default:
      //_degrees = 0;
      _degrees = Position;
  }
  return _degrees + "deg";

} // GetDegrees

////////////////////////////////////////////////////////////////////////////////////////

function CreateDivRoulettes() {
  /*
  var _css_transform;
  var _roulette_rotate_deg;

  var m_roulettes_positions;
  m_roulettes_positions = [ //posX, posY, width, height, Orientation
  [780, 0, 120, 80, 0],
  [980, -0, 120, 80, 4]
  ];

  _css_transform = GetPropertyName_Transform();

  for (var _i = 0; _i < m_roulettes_positions.length; _i++) {
  var _new_div;

  _new_div = document.createElement("div");
  $(_new_div).addClass("div_roulette")
  .attr("id", "roulette_" + _i)
  .css("left", (m_roulettes_positions[_i][0]) + "px")
  .css("top", (m_roulettes_positions[_i][1]) + "px")
  .css("width", (m_roulettes_positions[_i][2]) + "px")
  .css("height", (m_roulettes_positions[_i][3]) + "px")
  .css("background", "url(images/m_roulette.png) no-repeat");

  $(_new_div).click(function (e) {
  RouletteClick(e, this);
  });

  _roulette_rotate_deg = GetDegrees(m_roulettes_positions[_i][4]);
  $(_new_div).css(_css_transform, "rotate(" + _roulette_rotate_deg + ")");

  $("#div_center").append($(_new_div));
  }
  */
} //CreateDivRoulettes

////////////////////////////////////////////////////////////////////////////////////////

function RouletteClick(e, Roulette) {

  alert($(Roulette).attr("id"));
} // RouletteClick

////////////////////////////////////////////////////////////////////////////////////////

function CreateDivGamingtables() {
  /*
  var _css_transform;
  var m_gamingtables_positions;
  var _gamingtables_rotate_deg;

  m_gamingtables_positions = [ //posX, posY, width, height, size, , Orientation
  [680, -200, 80, 80, "big", 0],
  [680, -300, 80, 80, "big", 0],
  [780, -100, 60, 40, "small", 7],
  [980, -100, 60, 40, "small", 1]
  ];

  _css_transform = GetPropertyName_Transform();

  for (var _i = 0; _i < m_gamingtables_positions.length; _i++) {
  var _new_div;

  _new_div = document.createElement("div");
  $(_new_div).addClass("div_gamingtable")
  .attr("id", "gamingtable_" + _i)
  .css("left", (m_gamingtables_positions[_i][0]) + "px")
  .css("top", (m_gamingtables_positions[_i][1]) + "px")
  .css("width", (m_gamingtables_positions[_i][2]) + "px")
  .css("height", (m_gamingtables_positions[_i][3]) + "px");
  if (m_gamingtables_positions[_i][4] == "small") {
  $(_new_div).css("background", "url(images/m_BJ.png) no-repeat");
  }
  else {
  $(_new_div).css("background", "url(images/m_PK.png) no-repeat");
  }

  $(_new_div).click(function (e) {
  GamingtableClick(e, this);
  });

  _gamingtables_rotate_deg = GetDegrees(m_gamingtables_positions[_i][5]);
  $(_new_div).css(_css_transform, "rotate(" + _gamingtables_rotate_deg + ")");

  $("#div_center").append($(_new_div));
  }
  */
} //CreateDivGamingtables

////////////////////////////////////////////////////////////////////////////////////////

function GamingtableClick(e, Gamingtable) {

  alert($(Gamingtable).attr("id"));
} // GamingtableClick

////////////////////////////////////////////////////////////////////////////////////////

function IslandClickEvent(e, IslandDiv) {

  alert($(IslandDiv).attr("id"));
}

////////////////////////////////////////////////////////////////////////////////////////

function AreaClickEvent(e, AreaDiv) {

  alert($(AreaDiv).attr("id"));
}

////////////////////////////////////////////////////////////////////////////////////////

function GetPropertyName_Transform() {
  var _transform;

  switch (m_browser) {
    case "Chrome":
    case "Opera":
    case "Safari":
      _transform = "-webkit-transform"
      break;


      //IE and Firefox                
    default:
      _transform = "transform";
  }

  return _transform;
}

////////////////////////////////////////////////////////////////////////////////////////

function GetPropertyName_TransformOrigin() {
  var _transform_origin;

  switch (m_browser) {
    case "Chrome":
    case "Opera":
    case "Safari":
      _transform_origin = "-webkit-transform-origin"
      break;


      //IE and Firefox                 
    default:
      _transform_origin = "transform-origin";
  }

  return _transform_origin;
}

////////////////////////////////////////////////////////////////////////////////////////

function DivPosition_Move(Div, Left, Top, Zoom, TransformOrigin) {
  var _css_value;


  _css_value = "scale(" + Zoom + ")" + "translate(" + Left + "px, " + Top + "px)";

  if (TransformOrigin != null) {
    Div.css(GetPropertyName_TransformOrigin(), TransformOrigin);
  }
  Div.css(GetPropertyName_Transform(), _css_value);
} // DivPosition_Move

////////////////////////////////////////////////////////////////////////////////////////

function DivPosition_Transform(Div, Action, MovementOrigin) {
  var _temp;
  switch (Action) {
    case CONST_MOVEMENT_TYPE_RIGTH: m_current_background_position_left -= CONST_MOVEMENT_PX; break;
    case CONST_MOVEMENT_TYPE_LEFT: m_current_background_position_left += CONST_MOVEMENT_PX; break;
    case CONST_MOVEMENT_TYPE_UP: m_current_background_position_top -= CONST_MOVEMENT_PX; break;
    case CONST_MOVEMENT_TYPE_DOWN: m_current_background_position_top += CONST_MOVEMENT_PX; break;
    case CONST_MOVEMENT_TYPE_ZOOM_MORE:
      switch (true) {
        case MovementOrigin === CONST_MOVEMENT_BUTTON:
          _temp = CONST_MOVEMENT_ZOOM_PX;
          break;
        case MovementOrigin === CONST_MOVEMENT_MOUSE:
          _temp = CONST_MOVEMENT_ZOOM_WHEEL_PX;
          break;
        default: break;
      }
      if (+m_current_zoom + _temp <= CONST_MAX_ZOOM) {
        m_current_zoom = +m_current_zoom + _temp;
      }
      UpdateSlider();
      break;

    case CONST_MOVEMENT_TYPE_ZOOM_MINUS:
      switch (true) {
        case MovementOrigin === CONST_MOVEMENT_BUTTON:
          _temp = CONST_MOVEMENT_ZOOM_PX;
          break;
        case MovementOrigin === CONST_MOVEMENT_MOUSE:
          _temp = CONST_MOVEMENT_ZOOM_WHEEL_PX;
          break;
        default: break;
      }
      if (+m_current_zoom - _temp >= CONST_MIN_ZOOM) {
        m_current_zoom = +m_current_zoom - _temp;
      }
      UpdateSlider();
      break;

    default: break;
  }

  DivPosition_Move(Div, m_current_background_position_left, m_current_background_position_top, m_current_zoom, CONST_TRANSFORM_ORIGIN);

} // DivPosition_Transform

////////////////////////////////////////////////////////////////////////////////////////

(function ($) {
  // Menu Functions
  $(document).ready(function () {

    // previously click
    $('#openner').mousedown(function (e) {

      var _r = $('#menu_rigth').css("right");

      if (_r == "0px") {
        $('#menu_rigth').css({
          'right': '-400px',
          'top': '0px'
        });
        $('#openner').css({
          'right': '0px',
          'top': '100px'

        });

        $('#openner').css('background', 'url(images/m_more.png) no-repeat');

      }
      else {
        $('#menu_rigth').css({
          'right': '0px',
          'top': '0px'
        });
        $('#openner').css({
          'right': '400px',
          'top': '100px'
        });

        $('#openner').css('background', 'url(images/m_less.png) no-repeat');
      }

      e.preventDefault();
    });

  });
})(jQuery);


////////////////////////////////////////////////////////////////////////////////////////////////////////////


function SetBackground(checkbox) {
  var _visibility;

  if (checkbox.checked) {
    _visibility = "visible";
  } else {
    _visibility = "hidden";
  }

  $("#img_background").css("visibility", _visibility);
  m_config_has_changed = true;
} // SetBackground

////////////////////////////////////////////////////////////////////////////////////////////////////////////

function SetIslands(checkbox) {
  var _visibility;

  if (checkbox.checked) {
    _visibility = "visible";
  } else {
    _visibility = "hidden";
  }

  for (var _i = 0; _i < m_islands_positions.length; _i++) {
    $("#" + CONST_ID_ISLAND + _i).css("visibility", _visibility);
  }
} // SetIslands

////////////////////////////////////////////////////////////////////////////////////////////////////////////

function SetAreas(checkbox) {
  var _visibility;

  if (checkbox.checked) {
    _visibility = "visible";
  } else {
    _visibility = "hidden";
  }

  for (var _i = 0; _i < m_areas_positions.length; _i++) {
    $("#" + CONST_ID_AREA + _i).css("visibility", _visibility);
  }
} // SetAreas

////////////////////////////////////////////////////////////////////////////////////////////////////////////

function InitializeScene() {
  CreateDivTerminales();
  CreateDivIslands();
  CreateDivAreas();
  CreateDivGamingtables();
  CreateDivRoulettes();
  ApplyDefaultValues();

  GetAvailableObjects();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

function HiddenDiv(Div) {
  //$(Div).css("visibility","hidden");
  $(Div).removeClass("div_popup_terminal ui-widget-content ui-widget");
  $(Div).addClass("div_popup_terminal_hidden");
  $("#popup_Top10Best2").css("display", "none");

  if (m_terminal_id_selected) {
    UnselectTerminalDiv();
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

function HelpLines_Remove(TerminalDiv) {
  $(TerminalDiv).find("div").remove();
  $(TerminalDiv).css("background-color", "none");

} // HelpLines_Remove

////////////////////////////////////////////////////////////////////////////////////////////////////////////

function HelpLines_Add(TerminalDiv) {
  var _lines_color;
  var _lines_border_style;
  var _new_div_lines_v1;
  var _new_div_lines_v2;
  var _new_div_lines_v3;
  var _new_div_lines_v4;
  var _new_div_lines_h1;
  var _new_div_lines_h2;
  var _new_div_lines_h3;
  var _new_div_lines_h4;

  _lines_color = "Red";
  _lines_border_style = "solid"

  _new_div_lines_v1 = document.createElement("div");
  _new_div_lines_v2 = document.createElement("div");
  _new_div_lines_v3 = document.createElement("div");
  _new_div_lines_v4 = document.createElement("div");
  _new_div_lines_h1 = document.createElement("div");
  _new_div_lines_h2 = document.createElement("div");
  _new_div_lines_h3 = document.createElement("div");
  _new_div_lines_h4 = document.createElement("div");

  $(_new_div_lines_v1)
          .attr("name", "helpline_v1")
          .css("position", "absolute")
          .css("border-color", _lines_color)
          .css("border-style", _lines_border_style)
          .css("border-bottom-style", "none")
          .css("border-width", "1px")
          .css("top", $(TerminalDiv).css("height"))
          .css("left", $(TerminalDiv).css("width"))
          .css("width", "2000px")
          .css("height", "0px");

  $(_new_div_lines_v2)
          .attr("name", "helpline_v2")
          .css("position", "absolute")
          .css("border-color", _lines_color)
          .css("border-style", _lines_border_style)
          .css("border-bottom-style", "none")
          .css("border-width", "1px")
          .css("top", $(TerminalDiv).css("height"))
          .css("right", $(TerminalDiv).css("width"))
          .css("width", "2000px")
          .css("height", "0px");

  $(_new_div_lines_v3)
          .attr("name", "helpline_v3")
          .css("position", "absolute")
          .css("border-color", _lines_color)
          .css("border-style", _lines_border_style)
          .css("border-bottom-style", "none")
          .css("border-width", "1px")
          .css("top", "-1px")
          .css("left", $(TerminalDiv).css("width"))
          .css("width", "2000px")
          .css("height", "0px");

  $(_new_div_lines_v4)
          .attr("name", "helpline_v4")
          .css("position", "absolute")
          .css("border-color", _lines_color)
          .css("border-style", _lines_border_style)
          .css("border-bottom-style", "none")
          .css("border-width", "1px")
          .css("top", "-1px")
          .css("right", $(TerminalDiv).css("width"))
          .css("width", "2000px")
          .css("height", "0px");

  $(_new_div_lines_h1)
          .attr("name", "helpline_h1")
          .css("position", "absolute")
          .css("border-color", _lines_color)
          .css("border-style", _lines_border_style)
          .css("border-left-style", "none")
          .css("border-width", "1px")
          .css("bottom", $(TerminalDiv).css("height"))
          .css("left", $(TerminalDiv).css("width"))
          .css("width", "0px")
          .css("height", "2000px");

  $(_new_div_lines_h2)
          .attr("name", "helpline_h2")
          .css("position", "absolute")
          .css("border-color", _lines_color)
          .css("border-style", _lines_border_style)
          .css("border-left-style", "none")
          .css("border-width", "1px")
          .css("top", $(TerminalDiv).css("height"))
          .css("left", $(TerminalDiv).css("width"))
          .css("width", "0px")
          .css("height", "2000px");

  $(_new_div_lines_h3)
          .attr("name", "helpline_h3")
          .css("position", "absolute")
          .css("border-color", _lines_color)
          .css("border-style", _lines_border_style)
          .css("border-left-style", "none")
          .css("border-width", "1px")
          .css("top", $(TerminalDiv).css("height"))
          .css("left", "-1px")
          .css("width", "0px")
          .css("height", "4000px");

  $(_new_div_lines_h4)
          .attr("name", "helpline_h4")
          .css("position", "absolute")
          .css("border-color", _lines_color)
          .css("border-style", _lines_border_style)
          .css("border-left-style", "none")
          .css("border-width", "1px")
          .css("bottom", $(TerminalDiv).css("height"))
          .css("left", "-1px")
          .css("width", "0px")
          .css("height", "2000px");

  $(TerminalDiv).append($(_new_div_lines_v1));
  $(TerminalDiv).append($(_new_div_lines_v2));
  $(TerminalDiv).append($(_new_div_lines_v3));
  $(TerminalDiv).append($(_new_div_lines_v4));
  $(TerminalDiv).append($(_new_div_lines_h1));
  $(TerminalDiv).append($(_new_div_lines_h2));
  $(TerminalDiv).append($(_new_div_lines_h3));
  $(TerminalDiv).append($(_new_div_lines_h4));

} // HelpLines_Add

////////////////////////////////////////////////////////////////////////////////////////////////////////////

function TerminalClickEvent(e, TerminalDiv, UpdateHistorical) {
  TerminalDivSelected($(TerminalDiv).attr("id"));
  m_terminal_id_selected = $(TerminalDiv).attr("id");
  if ($(TerminalDiv).find("div").length == 0) {
    HelpLines_Add(TerminalDiv);
  }
} //TerminalClickEvent

////////////////////////////////////////////////////////////////////////////////////////////////////////////

function CreateGrid(Size) {
  var _i;
  var _height = $("#div_background").height();
  var _width = $("#div_background").width();
  var _ratioW = Math.floor(_width / Size);
  var _ratioH = Math.floor(_height / Size);

  for (_i = 0; _i <= _ratioW; _i++) {  // vertical grid lines
    $('<div />').css({
      'top': 1,
      'left': _i * Size,
      'width': 1,
      'height': _height
    })
    .addClass('gridlines')
    .appendTo($("#div_background"));
  }
  for (_i = 0; _i <= _ratioH; _i++) { // horizontal grid lines
    $('<div />').css({
      'top': 1 + _i * Size,
      'left': 0,
      'width': _width,
      'height': 1
    })
    .addClass('gridlines')
    .appendTo($("#div_background"));
  }

  $('.gridlines').show();

  if (_Configuration.ShowGrid) {
    $('.gridlines').css("visibility", "visible");
  }
  else {
    $('.gridlines').css("visibility", "hidden");
  }
}  // CreateGrid

////////////////////////////////////////////////////////////////////////////////////////////////////////////

function UnselectTerminalDiv(Selected) {
  TerminalDivSelected(m_terminal_id_selected, false);
  m_terminal_id_selected = null;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

function SelectTerminal(Terminal, Selected) {

  if (Selected) {
    // Only mark as select if it is unselected
    if (!$(Terminal).attr("selected")) {
      var _final_left;
      var _final_top;

      _final_left = $(Terminal).css("left").replace("px", "") + "px";
      _final_top = $(Terminal).css("top").replace("px", "") + "px";

      // Mark terminal as select
      $(Terminal)
      .addClass("div_terminal_selected")
      .css('z-index', '5')
      .attr('initial_left', $(Terminal).css("left"))
      .attr('initial_top', $(Terminal).css("top"))
      .attr('selected', '1')
      .css('left', (_final_left))
      .css('top', (_final_top));
    }
  }
  else {
    // Only mark as unselect if it is select
    if ($(Terminal).attr("selected")) {
      $(Terminal)
      .removeClass("div_terminal_selected")
      .css('z-index', '2')
      .css('left', ($(Terminal).attr("initial_left")))
      .css('top', ($(Terminal).attr("initial_top")))
      .removeAttr("selected");
    }
  }
} //SelectTerminal

////////////////////////////////////////////////////////////////////////////////////////////////////////////

function TerminalDivSelected(TerminalId, Selected) {

  if (Selected == undefined) { Selected = true; }

  var _term_div = TerminalId;

  // Unselect actual terminal
  if (m_terminal_id_selected != null && _term_div != m_terminal_id_selected) {

    $($("#" + m_terminal_id_selected))
    .removeClass("div_terminal_selected")
    .css('z-index', '2');

    if ($($("#" + m_terminal_id_selected)).attr("selected")) {
      $($("#" + m_terminal_id_selected)).removeAttr("selected");
    }
    HelpLines_Remove($("#" + m_terminal_id_selected))
  }

  // Select/Unselect terminal
  if ($("#" + _term_div).length > 0) {
    SelectTerminal($("#" + _term_div), Selected);
  } else {
    // Does not exist the Terminal?
  }

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

function SetZoom(Value) {
  //m_current_zoom = 1 - Value;
  m_current_zoom = Value / 100;
  DivPosition_Move($("#div_background"), m_current_background_position_left, m_current_background_position_top, m_current_zoom, CONST_TRANSFORM_ORIGIN);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

function UpdateSlider() {
  try {
    $("#sldZoom").slider("value", m_current_zoom * 100);

  } catch (err) { }

}

//////////////////////////////////////////////////////////////////////////////////////

//function ApplyDefaultValues() {

//    //chkSetBackground
//    if ($("#chkSetBackground").prop("checked")) {
//        $("#img_background").css("visibility", "visible");
//    }
//    else {
//        $("#img_background").css("visibility", "hidden");
//    }

//    //chkShowGrid
//    if ($("#chkShowGrid").prop("checked")) {
//        $('.gridlines').css("visibility", "visible");
//    }
//    else {
//        $('.gridlines').css("visibility", "hidden");
//    }

//    //chkShowAreas
//    if ($("#chkShowAreas").prop("checked")) {
//        $("div[id*=" + CONST_ID_AREA + "]").css("visibility", "visible");
//    }
//    else {
//        $("div[id*=" + CONST_ID_AREA + "]").css("visibility", "hidden");
//    }

//    //chkShowIslands
//    if ($("#chkShowIslands").prop("checked")) {
//        $("div[id*=" + CONST_ID_ISLAND + "]").css("visibility", "visible");
//    }
//    else {
//        $("div[id*=" + CONST_ID_ISLAND + "]").css("visibility", "hidden");
//    }

//    //LC_LANGUAGE AS LANG
//    if ($("#radEnglish").prop("checked")) {
//        _Language.CurrentLanguage = 1;
//    }
//    else if ($("#radSpanish").prop("checked")) {
//        _Language.CurrentLanguage = 0;
//    }
//    else {
//        // default SPANISH
//        _Language.CurrentLanguage = 0;
//    }
//    _Language.parse();

//} // ApplyDefaultValues

//////////////////////////////////////////////////////////////////////////////////////

function SetDivBackgroundToInitialPosition() {
  //m_current_background_position_left = -1 * (CONST_DIV_BACKGROUND_WIDTH / 2);
  //m_current_background_position_top = -1 * (CONST_DIV_BACKGROUND_HEIGHT / 2);

  // Adjust location
  //m_current_background_position_left += (50 / +CONST_INITIAL_ZOOM);
  m_current_background_position_top += (70 / +CONST_INITIAL_ZOOM);

  // Initial position
  DivPosition_Move($("#div_background"), m_current_background_position_left, m_current_background_position_top, m_current_zoom, CONST_TRANSFORM_ORIGIN);

  // Initial position
  DivPosition_Move($("#div_heatmapArea"), m_current_background_position_left, m_current_background_position_top, m_current_zoom, CONST_TRANSFORM_ORIGIN);

  // Zoom Out
  m_current_zoom = CONST_INITIAL_ZOOM;
  DivPosition_Move($("#div_background"), m_current_background_position_left, m_current_background_position_top, m_current_zoom, CONST_TRANSFORM_ORIGIN);

} // SetDivBackgroundToInitialPosition

//////////////////////////////////////////////////////////////////////////////

function StartMove(sender, object) {
  if (object.id == m_terminal_clicked.id) {
    m_is_moving = true;
    m_start_left = +$(object).css("left").replace("px", "");
    m_start_top = +$(object).css("top").replace("px", "");
    m_old_left = sender.clientX;
    m_old_top = sender.clientY;
    $(object).css("background-color", "green");
  }
}

function EndMove(sender) {
  m_is_moving = false;
  $(m_terminal_clicked).css("background-color", "white");

  var _terminal = _Manager.Terminals.GetTerminal(m_terminal_clicked.id.replace("terminal_", ""));
  if (_terminal) {
    var _changed = false;

    var _xx = CoordToPosition($(m_terminal_clicked).css("left").replace("px", ""), $(m_terminal_clicked).attr("trans_left"));
    var _yy = CoordToPosition($(m_terminal_clicked).css("top").replace("px", ""), $(m_terminal_clicked).attr("trans_top"));

    if (_terminal.Properties.lop_x != _xx) {
      _terminal.Properties.lop_x = _xx;
      _changed = true;
    }

    if (_terminal.Properties.lop_z != _yy) {
      _terminal.Properties.lop_z = _yy;
      _changed = true;
    }

    // TODO: SetObjectChanged(Manager.Terminals..Terminal)
    if (_changed) {
      SetObjectChanged(_terminal);
    }
  }

}

function Move(sender, object) {
  if (m_left_button_down && m_terminal_clicked.id == object.id) {
    m_is_moving = true;
    //$(object).css("left", m_start_left + ((sender.clientX - m_old_left) / m_current_zoom));
    //$(object).css("top", m_start_top + ((sender.clientY - m_old_top) / m_current_zoom));
  }
}

//////////////////////////////////////////////////////////////////////////////

function SetCoordsValue() {
  if (m_terminal_clicked != null) {
    var _terminal = _Manager.Terminals.GetTerminal(m_terminal_clicked.id.replace("terminal_", ""));
    if (_terminal) {
      var _xx = CoordToPosition($(m_terminal_clicked).css("left").replace("px", ""), $(m_terminal_clicked).attr("trans_left"));
      var _yy = CoordToPosition($(m_terminal_clicked).css("top").replace("px", ""), $(m_terminal_clicked).attr("trans_top"));

      $("#txtObjectX").val(_xx);
      $("#txtObjectY").val(_yy);
    }
  }
}

//////////////////////////////////////////////////////////////////////////////

function getRotationDegrees(obj) {
  var matrix = obj.css("-webkit-transform") ||
  obj.css("-moz-transform") ||
  obj.css("-ms-transform") ||
  obj.css("-o-transform") ||
  obj.css("transform");
  if (matrix !== 'none') {
    var values = matrix.split('(')[1].split(')')[0].split(',');
    var a = values[0];
    var b = values[1];
    var angle = Math.round(Math.atan2(b, a) * (180 / Math.PI));
  } else { var angle = 0; }
  return (angle < 0) ? angle += 360 : angle;
}

//////////////////////////////////////////////////////////////////////////////

function StartRotate(e, ui) {
  if (m_terminal_clicked != null) {
    m_is_rotating = true;
    m_start_angle = getRotationDegrees($(m_terminal_clicked));
    $(m_terminal_clicked).attr("rotate_value", ui.value + "deg");
    UpdateTransform(m_terminal_clicked.id, true);
  }
}

function Rotating(e, ui) {
  if (m_terminal_clicked != null) {

    $(m_terminal_clicked).attr("rotate_value", ui.value + "deg");
    UpdateTransform(m_terminal_clicked.id, true);

    var _terminal = _Manager.Terminals.GetTerminal(m_terminal_clicked.id.replace("terminal_", ""));
    if (_terminal) {
      var _changed = false;

      if (ui.value != m_start_angle) {
        _terminal.Properties.lop_orientation = ui.value;
        _changed = true;
      }

      if (_changed) {
        SetObjectChanged(_terminal);
      }
    }
  }
}

function EndRotate() {
  if (m_terminal_clicked != null) {

    var _terminal = _Manager.Terminals.GetTerminal(m_terminal_clicked.id.replace("terminal_", ""));
    var _angle = getRotationDegrees($("#" + m_terminal_clicked.id));

    $(m_terminal_clicked).attr("rotate_value", _angle + "deg");
    UpdateTransform(m_terminal_clicked.id, true);

    if (_terminal) {
      var _changed = false;

      if (_angle != m_start_angle) {
        _terminal.Properties.lop_orientation = _angle;
        _changed = true;
      }

      if (_changed) {
        SetObjectChanged(_terminal);
      }
    }
    m_start_angle = null;
    m_is_rotating = false;
  }
}

//////////////////////////////////////////////////////////////////////////////

function SetAnglepickerValue() {
  if (m_terminal_clicked != null) {
    var _angle = getRotationDegrees($(m_terminal_clicked));
    $("#anglepicker").anglepicker("value", _angle);
    $("#anglepickervalue").val(_angle);
  }
}

//////////////////////////////////////////////////////////////////////////////

function SetObjectChanged(Object) {
  WSILogger.Write("Object changed: " + Object.Properties.lo_id);

  if (m_changed_items[Object.Properties.lo_id] != undefined) {
    m_changed_objects[m_changed_items[Object.Properties.lo_id]] = Object.Properties;
  } else {
    m_changed_objects[m_changed_objects.length] = Object.Properties;
    m_changed_items[Object.Properties.lo_id] = m_changed_objects.length - 1;
  }

}

function EditorSaveChanges() {

  if (m_changed_objects.length > 0) {
    $.ajax({
      type: 'POST',
      url: 'Editor.aspx/ApplyChanges',
      data: "{'Objects':'" + JSON.stringify(m_changed_objects) + "'}",
      contentType: 'application/json; charset=utf-8',
      dataType: 'json',
      success: function (msg) {
        // Clear changes
        delete m_changed_objects;
        m_changed_objects = [];
        console.log(msg.d);
        alert("Changes saved\n\r" + msg.d);
      }
    });
  } else {
    alert("Nothing to save.");
  }

}

/////////////////////////////////////////////////////////////

function CoordToPosition(Origin, Translation) {
  if (!Translation) {
    Translation = 0;
  }
  return Math.round((+Origin + +Translation + 5) / 5);
}

function PositionToCoord(Position) {
  return (+Position * 5) - 5;
}

/////////////////////////////////////////////////////////////

function UpdateMousePosition(e) {
  m_current_mouse_position_x = e.pageX;
  m_current_mouse_position_y = e.pageY;
}

/////////////////////////////////////////////////////////////

function UpdateBackgroundPosition(X, Y) {
  var _temp_left = (X - m_current_mouse_position_x) / m_current_zoom + m_current_background_position_left;
  var _temp_top = (Y - m_current_mouse_position_y) / m_current_zoom + m_current_background_position_top;

  var _css_value = "scale(" + m_current_zoom + ")" + "translate(" + _temp_left + "px, " + _temp_top + "px)";

  var _css_transform = GetPropertyName_Transform();

  $("#div_background").css(_css_transform, _css_value);

  m_current_background_position_left = _temp_left;
  m_current_background_position_top = _temp_top;
}

function UpdateObjectPosition(X, Y, ClientX, ClientY) {
  if (m_current_terminal_position_left[$(m_terminal_clicked).attr("id")] == null || m_current_terminal_position_top[$(m_terminal_clicked).attr("id")] == null) {
    m_current_terminal_position_left[$(m_terminal_clicked).attr("id")] = 0;
    m_current_terminal_position_top[$(m_terminal_clicked).attr("id")] = 0;
  }
  var _temp_left = (X - m_current_mouse_position_x) / m_current_zoom + m_current_terminal_position_left[$(m_terminal_clicked).attr("id")];
  var _temp_top = (Y - m_current_mouse_position_y) / m_current_zoom + m_current_terminal_position_top[$(m_terminal_clicked).attr("id")];

  _css_value = "translate(" + _temp_left + "px, " + _temp_top + "px) rotate(" + $(m_terminal_clicked).attr("rotate_value") + ")";
  _css_transform = GetPropertyName_Transform();
  jQuery(m_terminal_clicked).css(_css_transform, _css_value);

  $(m_terminal_clicked).attr("position_left", m_start_left + ((ClientX - m_old_left) / m_current_zoom) + "px");
  $(m_terminal_clicked).attr("position_top", m_start_top + ((ClientY - m_old_top) / m_current_zoom) + "px");
  $(m_terminal_clicked).attr("translate_value", _temp_left + "px, " + _temp_top + "px");
  $(m_terminal_clicked).attr("trans_left", _temp_left);
  $(m_terminal_clicked).attr("trans_top", _temp_top);

  m_current_terminal_position_left[$(m_terminal_clicked).attr("id")] = _temp_left;
  m_current_terminal_position_top[$(m_terminal_clicked).attr("id")] = _temp_top;
}

/////////////////////////////////////////////////////////////

function GetAvailableObjects() {
  PageMethods.GetAvailableObjects(GetAvailableObjectsComplete, GetAvailableObjectsError);
}

function GetAvailableObjectsComplete(result, userContext, methodName) {

  if (result) {

  }

}

function GetAvailableObjectsError(error, userContext, methodName) {
  GotoError();
}


/////////////////////////////////////////////////////////////

function UpdateTerminals() {
  //
}