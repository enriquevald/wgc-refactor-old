﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: filename
// 
//   DESCRIPTION: Description.
// 
//        AUTHOR: Author
// 
// CREATION DATE: Date
// 
//------------------------------------------------------------------------------

function WSIBeacon() {
  this.id = "";
  this.x = 0;
  this.y = 0;
  this.z = 0;
  this.Mesh = null;
  this.location_id = "";
  this.offset_x = 0;
  this.offset_y = 0;
  this.location_x = 0;
  this.location_y = 0;
}

function WSIBeaconsManager() {
  if (_LayoutMode == 1) { this.Node = new THREE.Object3D(); }
  this.Items = [];

}

WSIBeaconsManager.prototype.AddItem = function (BeaconData, LocationData, Dimensions) {
  var _new_item = new WSIBeacon();

  if (BeaconData && LocationData && Dimensions) {
    _new_item.id = BeaconData[1];
    _new_item.offset_x = BeaconData[17];
    _new_item.offset_y = BeaconData[18];
    _new_item.location_id = LocationData[0];
    _new_item.location_x = LocationData[1];
    _new_item.location_y = LocationData[2];

    // Calculate real x/y
    var _fx = -Math.floor(PixelsToUnits(Dimensions.x) / 2) * 4,
        _fy = -Math.floor(PixelsToUnits(Dimensions.y) / 2) * 4;
    _new_item.x = _fx + (+LocationData[1] * 4);
    _new_item.y = 0;
    _new_item.z = _fy + (+LocationData[2] * 4);

    // Add Item
    this.Items.push(_new_item);
  }

}

WSIBeaconsManager.prototype.Build = function (CallbakBeaconBuilder) {
  
  if (!WSIData.ShowBeacons || this.Items.length == 0) { return; }

  for (var _b in this.Items) {
    
    var _beacon = this.Items[_b];

    if (_LayoutMode == 1) { // 3D
      _beacon.Mesh = new THREE.Mesh(new THREE.SphereGeometry(0.5, 8, 8), new THREE.MeshBasicMaterial({ color: 0x5555ff }));
      _beacon.Mesh.position.set(_beacon.x, _beacon.y, _beacon.z);
      _beacon.Mesh.name = _b;

      this.Node.add(_beacon.Mesh);
    } else { // 2D
      if (CallbakBeaconBuilder) {
        CallbakBeaconBuilder(_beacon);
      }
    }
    
  }

  if (_LayoutMode == 1) { // 3D
    this.Node.position.set(0, 18, 0);
    WSI.scene.add(this.Node);
  }
}