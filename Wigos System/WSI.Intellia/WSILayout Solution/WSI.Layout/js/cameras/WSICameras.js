﻿
var EN_CAMERA_MODE = {
  CM_PERSPECTIVE: 0,
  CM_CENIT: 1,
  CM_FIRST_PERSON: 2
};

function WSICameraManager() {
  this.CurrentMode = EN_CAMERA_MODE.CM_PERSPECTIVE;
  this.Cameras = [];

  // Init

  // 3D
  this.Cameras.push(new THREE.PerspectiveCamera(50, window.innerWidth / window.innerHeight, 1, 800));
  this.Cameras[EN_CAMERA_MODE.CM_PERSPECTIVE].aspect = window.innerWidth / window.innerHeight;
  this.Cameras[EN_CAMERA_MODE.CM_PERSPECTIVE].position.y = 50;
  this.Cameras[EN_CAMERA_MODE.CM_PERSPECTIVE].position.z = 100;
  this.Cameras[EN_CAMERA_MODE.CM_PERSPECTIVE].clickOnChildren = false;

  // 2D
  this.Cameras.push(new THREE.OrthographicCamera(window.innerWidth / -2, window.innerWidth / 2, window.innerHeight / 2, window.innerHeight / -2, 1, 800));
  this.Cameras[EN_CAMERA_MODE.CM_CENIT].aspect = window.innerWidth / window.innerHeight;
  this.Cameras[EN_CAMERA_MODE.CM_CENIT].up = new THREE.Vector3(0, 0, -1);
  this.Cameras[EN_CAMERA_MODE.CM_CENIT].position.y = 200;
  this.Cameras[EN_CAMERA_MODE.CM_CENIT].clickOnChildren = false;

  // FP
  this.Cameras.push(new THREE.PerspectiveCamera(60, window.innerWidth / window.innerHeight, 1, 500));
  this.Cameras[EN_CAMERA_MODE.CM_FIRST_PERSON].aspect = window.innerWidth / window.innerHeight;
  this.Cameras[EN_CAMERA_MODE.CM_FIRST_PERSON].up = new THREE.Vector3(0, 1, 0);
  this.Cameras[EN_CAMERA_MODE.CM_FIRST_PERSON].position.set(0, 9, 0);
  this.Cameras[EN_CAMERA_MODE.CM_FIRST_PERSON].lookAt(new THREE.Vector3(0, 0, 0));
  this.Cameras[EN_CAMERA_MODE.CM_FIRST_PERSON].clickOnChildren = true;

  this.CurrentCamera = function () {
    return this.Cameras[this.CurrentMode];
  }

}
