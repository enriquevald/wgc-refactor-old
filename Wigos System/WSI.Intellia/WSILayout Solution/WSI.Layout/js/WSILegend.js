﻿

var CONST_MIN_HEATMAP_VALUE = 25;
var m_monitor_current_filter = [];
var m_custom_alarm_edit_id = 0;
var m_custom_alarm_edit_sectionid = 0;
var m_custom_alarm_edit_mode = false;
var m_custom_alarm_edit_id;
var m_section_alarms_all = false;
var m_alarms_filter_sounds_changed = false;


///////////////////////////////////////////////////////////////////////////////////////////
/*
WSILegendItem:
Mantains a range level of a legend
*/

// Creates and initialice a legend item instance
function WSILegendItem(color, name, minValue, maxValue, value, Operator, SubItems, ID, DisplayId, Nls) {
  this.color = color;                             // Legend range color
  this.name = name;                               // Legend range name (not an NLS)
  this.isDefault = !minValue;                     // DEPRECATED
  this.minValue = minValue ? minValue : 0;        // Min range value
  this.maxValue = maxValue ? maxValue : minValue; // Max range value
  this.Operator = Operator;                       // Operator to check
  // Use operator to set max value
  if (Operator) {
    if (Operator == "=") {
      this.maxValue = this.minValue;
    } else {
      this.maxValue = maxValue;
    }
  }
  // If not value specified, value = 0
  this.value = value ? value : 0;
  this.Items = SubItems;                          // Items from filter ( object info: alarmid:legendid)

  this.ID = ID;
  this.DisplayId = 0;
  this.Nls = Nls;
}

///////////////////////////////////////////////////////////////////////////////////////////
/*
WSILegend:
A group of range levels
*/
function WSILegend() {
  this.Name = "";                                                         // Name of the legend
  this.Property = "";                                                     // Field to be used
  this.SectionID = "";
  this.Items = [];                                                        // Ranges
  this.Icon = "";                                                         // Alarm Icon
  this.Nls = "";                                                           // Localization resource
  this.UnknownItem = new WSILegendItem('white', 'Unknown', null, null);   // DEPRECATED
}

// To add ranges in a legend
WSILegend.prototype.Add = function (LegendItem) {
  if (LegendItem) {
    this.Items.push(LegendItem);
  }
}

// Returns a string with color to use in a HeatMap
WSILegend.prototype.GetColors = function () {
  if (this.Items.length == 1) {
    return { 1: this.Items[0].color };
  } else {
    var _result = "{";
    var _i = Math.round((1 / (this.Items.length - 1)) * 100) / 100;
    var _x = CONST_MIN_HEATMAP_VALUE / 100;
    for (_li in this.Items) {
      _result += _x + ':"' + this.Items[_li].color + '",';
      _result[_x] = this.Items[_li].color;
      _x = Math.round((_x + _i) * 100) / 100;
      if (_x > 1) { _x = 1 };
    }
    return eval("(" + _result + "})");
  }
}

//////////////////////////////////////////////////////////////////////////////////////////
// This method returns the HTML code for the legend and subitems
WSILegend.prototype.Build = function (LegendId, Index, SectionID) {
  var _result = '';
  var _items_lenght = this.Items.length;
  if (LegendId == 15)
    return '';

  switch (SectionID) {

    case Enum.EN_SECTION_ID.ALARMS_PLAYERS:
    case Enum.EN_SECTION_ID.ALARMS_MACHINES:
      _result += '<h3 onclick="SetCurrentLegend(' + LegendId + ',' + Enum.EN_SECTION_ID.ALARMS.toString() + ');" data-id="' + LegendId + '" data-section="' + SectionID + '" data-index="' + Index + '" data-nls="' + this.Nls + '">' + _Language.get(this.Nls) + '<div class="chkAlarmsSound chkAlarmsSoundDisabled" data-soundlegendid="' + LegendId + '"/>';
      break;

    case Enum.EN_SECTION_ID.MONITOR:
      _result += '<h3 onclick="SetCurrentLegend(' + LegendId + ',' + SectionID + ');" data-id="' + LegendId + '" data-section="' + SectionID + '"  data-index="' + Index + '" data-customlegendid="' + this.ID + '">' + this.Name;
      break;
  
    case Enum.EN_SECTION_ID.MACHINES: 
    case Enum.EN_SECTION_ID.PLAYERS:
      _result += '<h3 onclick="SetCurrentLegend(' + LegendId + ',' + SectionID + ');" data-id="' + LegendId + '" data-section="' + SectionID + '"  data-index="' + Index + '" data-nls="' + this.Nls + '">' + _Language.get(this.Nls);
      break;

    default:
      _result += '<h3 onclick="SetCurrentLegend(' + LegendId + ',' + SectionID + ');" data-id="' + LegendId + '" data-section="' + SectionID + '"  data-index="' + Index + '">' + this.Name;
      break;

  }

  //    if (SectionID == CONST_SECTION_ID_ALARMS_ONLY_PLAYERS || SectionID == CONST_SECTION_ID_ALARMS_ONLY_MACHINES) {
  //        _result += getAlarmImage(LegendId);
  //    }

  _result += '</h3>';
  _result += '<div><ul>';
  var _flag = false;
  for (_i = 0; _i < _items_lenght; _i++) {
    var _build_item = this.Items[_i];

    if (_build_item != null) {
    switch (+SectionID) {
      case CONST_SECTION_ID_ALARMS_ONLY_MACHINES:
      case CONST_SECTION_ID_ALARMS_ONLY_PLAYERS:
        _result += "<li class='childLegend' id='childlegend_" + SectionID + LegendId + "_" + _i + "' onclick='Child_Legend_Click(" + SectionID + ",\"" + SectionID +
                    LegendId + "_" + _i + "\");' data-alarmid='" + parseInt(_build_item.minValue) + "' data-rangeid='" + _i + "' data-legendid='" + LegendId + "'>";
        _result += '<div class="circuloValor" data-sectionid="' + SectionID + '" id="leyenda_item_' + SectionID + '_' + LegendId + '" style="background: ' + _build_item.color + ';">';
        _result += '</div><span data-nls="'+_build_item.Nls+'">' + _Language.get(_build_item.Nls) + '</span></li>';
        break;

      case CONST_SECTION_ID_MONITOR:
        //Load siplay legend
        var _display_legend;

          if (_flag == false) {
            _flag=true;
          for (_idx_display_legend in this.Display) {
            if (this.Display[_idx_display_legend]) {
              _display_legend = this.Display[_idx_display_legend];
            }
          }
          _result += "<li class='childLegend' style='background-color:whiteSmoke'>";
          _result += '<div class="circuloIcono">';
          _result += '</div><span data-nls="' + _display_legend.Nls + '">' + _Language.get(_display_legend.Nls) + '</span></li>';

          for (_idx_child_monitor in this.Display[_idx_display_legend].Items) {
            _build_item = this.Display[_idx_display_legend].Items[_idx_child_monitor];
            if (_build_item != null) {
              _result += "<li class='childLegend' id='childlegend_" + SectionID + LegendId + "_" + _idx_child_monitor + "' onclick='Child_Legend_Click(" + SectionID + ",\"" + SectionID +
                          LegendId + "_" + _idx_child_monitor + "\");' data-rangeid='" + _idx_child_monitor + "' data-legendid='" + LegendId + "'>";
              _result += '<div class="circuloValor" data-sectionid="' + SectionID + '" id="leyenda_item_' + SectionID + '_' + LegendId + '" style="background: ' + _build_item.color + ';">';

              if (_build_item.Nls)
                _build_item.name = _Language.get(_build_item.Nls);

              _result += '</div><span>' + _build_item.name + '</span></li>';
            }
          }
        }
        break;

      default:
        _result += "<li class='childLegend' id='childlegend_" + SectionID + LegendId + "_" + _i + "' onclick='Child_Legend_Click(" + SectionID + ",\"" + SectionID +
                    LegendId + "_" + _i + "\");' data-rangeid='" + _i + "' data-legendid='" + LegendId + "'>";
        _result += '<div class="circuloValor" data-sectionid="' + SectionID + '" id="leyenda_item_' + SectionID + '_' + LegendId + '" style="background: ' + _build_item.color + ';">';

        if (_build_item.Nls)
          _build_item.name = _Language.get(_build_item.Nls);

        _result += '</div><span>' + _build_item.name + '</span></li>';
        break;
    }
  }

  }
  _result += '</ul></div>';

  return _result
}
/////////////////////////////////////////////////////////////////////////////////////////

/*
WSILegends:
A pseudo-class to mantain the legends used in application
*/
function WSILegends() {
  this.Items = [];        // Legends
  this.AlarmTypes = [];
}

// To add a legend to the class
WSILegends.prototype.Add = function (Id, Legend) {
  this.Items[Id] = Legend;
}

WSILegends.prototype.AddAlarmType = function (Family, Id, Description) {
  this.AlarmTypes[Family][Id] = Description;
}

// This method returns the HTML code for the ribbon in table format
WSILegends.prototype.Build = function (LegendId) {
  var _text_color = 'black';
  var _L = this.Items[LegendId];
  var _result = '<table id="TblLegend" cellspacing="0"> ' +
                '  <tr> ';
  for (_i = 0; _i < _L.Items.length; _i++) {
    if (_i % 4 == 0) {
      _result += ' </tr> ' +
           ' <tr> ';
    }

    _text_color = colourNameToHex(_L.Items[_i].color);
    _text_color = (GetColorContrast(hex2rgb(_text_color)) > 125) ? 'black' : 'white';

    _result += '     <td> ' +
              '          <div class="TDLegendQuad" style="background-color:' + _L.Items[_i].color + '; color: ' + _text_color + ' ">' + _L.Items[_i].name + '</div> ' +
              '      </td> ';

  }
  _result += '  </tr> ' +
             '</table>';
  return _result
}

// Check if Data is in a range of the specified Legend and return a class
// with the meet values
WSILegends.prototype.Check = function (LegendId, Data) {
  var _L = this.Items[LegendId];

  if (_L) {

    var _result = null;
    var _data_value = Data[m_field_properties[_L.Property].field];

    var _ok;

    if (_data_value != "" && _data_value != null) {

      for (_i = 0; _i < _L.Items.length; _i++) {

        _ok = false;

        var _Li = _L.Items[_i];

        // Numeric
        if ((+_data_value == _data_value) || (_data_value.toString().localeCompare(_data_value.toString()) == 0)) {

          switch (_Li.Operator) {
            case "=":
              if (+_data_value == +_Li.minValue || (_data_value.toString().localeCompare(_Li.minValue.toString()) == 0)) {
                _ok = true;
              }
              break;
            case "<":
              if (+_data_value < +_Li.minValue) {
                _ok = true;
              }
              break;
            case ">":
              if (+_data_value > +_Li.minValue) {
                _ok = true;
              }
              break;
            case "Between":
              if ((+_data_value >= +_Li.minValue) && ((+_data_value <= +_Li.maxValue))) {
                _ok = true;
              }
              break;
            default:
              if (_Li.minValue > 0) {
                if ((parseInt(_data_value) & parseInt(_Li.minValue)) == parseInt(_Li.minValue)) {
                  _ok = true;
                  break;
                }
              }

              break;
          }

        } else {
          // TODO: Strings if needed
        }

        if (_ok) {

          // Trick: for negative values in legends with one range "< 0", abs(value)
          if (_L.Items.length == 1) {
            if (+_L.Items[0].minValue == 0 && _L.Items[0].Operator == "<") {
              _result = { color: _Li.color, name: _Li.name, value: Math.abs(+_data_value) };
            } else {
              _result = { color: _Li.color, name: _Li.name, value: _Li.value };
            }
          } else {
            _result = { color: _Li.color, name: _Li.name, value: _Li.value };
          }

          break;
        }
      }
    }

    if (_result == null) {
      _result = { color: this.Items[LegendId].UnknownItem.color, name: this.Items[LegendId].UnknownItem.name, value: 0, icon: "", prop: _L.Property };
    }

  } else {
    WSILogger.Log("WSILegends.Check()", "Legend '" + LegendId + "' does not exist!");
  }

  _result.prop = _L.Property;

  // Return 
  return _result;
}

// Check if Data is in a range of the specified Legend and return a class
// with the meet values
WSILegends.prototype.CheckAlarms = function (Data, activeAlarms) {
  var _result = { color: "white", name: "", value: 0, icon: "" };

  if (activeAlarms.length == 0) {
    return _result;
  }

  var _alarm_object = null;

  // Look for the first alarm selected
  for (_idx_alarm in activeAlarms) {
    if (activeAlarms[_idx_alarm].sectionid == CONST_SECTION_ID_ALARMS_ONLY_MACHINES || activeAlarms[_idx_alarm].sectionid == CONST_SECTION_ID_ALARMS_ONLY_PLAYERS) {
      //Get property name to check with Terminal property
      var _property_name = m_field_properties[this.Items[activeAlarms[_idx_alarm].LegendId].Property].field;
      var _alarm_value = parseInt(Data[_property_name]);

      if (_alarm_value != 0) {
        if ((_alarm_value & activeAlarms[_idx_alarm].alarmid) == activeAlarms[_idx_alarm].alarmid) {
          _alarm_object = activeAlarms[_idx_alarm];
          break;
        }
      }
    }
  }

  if (_alarm_object != null) {
    // Obtain the properties from the legend items
    var _L = this.Items[_alarm_object.LegendId];
    if (_L) {
      for (_i = 0; _i < _L.Items.length; _i++) {
        if (parseInt(_L.Items[_i].minValue) == _alarm_object.alarmid) {
          _result = { color: _L.Items[_i].color, name: _L.Items[_i].name, value: _L.Items[_i].value, icon: _L.Icon };
          break;
        }
      }
    } else {
      WSILogger.Log("WSILegends.CheckAlarms()", "Legend '" + _alarm_object.LegendId + "' does not exist!");
    }
  }

  return _result;
}

function SplitListToCheck(List) {
  var _result = [];
  var _legend_id;

  for (_idx_List = 0; _idx_List <= List.length - 1; _idx_List++) {
    var _ranges = [];
    _legend_id = List[_idx_List].LegendId;

    //while pos[0] of string were same family
    while (_idx_List <= List.length - 1 && List[_idx_List].LegendId == _legend_id) {
      _ranges[List[_idx_List].alarmid] = List[_idx_List].alarmid;
      _idx_List++;
    }
    _result[_legend_id] = _ranges;
    _idx_List--;
  }

  return _result;
}
// To obtain the legend color map for a heatmap
WSILegends.prototype.GetLegendColors = function (LegendName) {
  var _L = this.Items[LegendName];

  if (_L) {
    return _L.GetColors();
  }

  return { 0.45: "rgb(0,0,255)", 0.55: "rgb(0,255,255)", 0.65: "rgb(0,255,0)", 0.95: "yellow", 1.0: "rgb(255,0,0)" };
}

///////////////////////////////////////////////////////////////////////////////////////////

// Global Legends variable
var _Legends = new WSILegends();

///////////////////////////////////////////////////////////////////////////////////////////

// Legend initialization

WSILegends.prototype.Init = function (Ranges) {
  var _simple_filter_id = 0;
  var _legends_counter = 0;

  this.Items = [];

  //Loading Legends
  for (_idx_legend in Ranges[0]) {
    var _legend_item = new WSILegend();

    _legend_item.Name = Ranges[0][_idx_legend].Ranges[1]; // Name
    _legend_item.Property = Ranges[0][_idx_legend].Ranges[2]; // Field
    _legend_item.SectionID = Ranges[0][_idx_legend].Ranges[4];
    _legend_item.Icon = Ranges[0][_idx_legend].Ranges[5];
    _legend_item.Nls = Ranges[0][_idx_legend].Ranges[6];
    _legend_item.Visible = Ranges[0][_idx_legend].Ranges[7];

    this.Add(Ranges[0][_idx_legend].Ranges[0], _legend_item);
  }

  //Loading Child Legends
  for (_idx_legend_item in Ranges[1]) {
    var _legend_color = Ranges[1][_idx_legend_item].Legends[5];
    var _legend_name = Ranges[1][_idx_legend_item].Legends[1];
    var _legend_minValue = Ranges[1][_idx_legend_item].Legends[2];
    var _legend_maxValue = Ranges[1][_idx_legend_item].Legends[3];
    var _value = 0;
    var _legend_operator = Ranges[1][_idx_legend_item].Legends[4].trim();
    var _legend_editable = Ranges[1][_idx_legend_item].Legends[7];
    var _legend_id = Ranges[1][_idx_legend_item].Legends[6];
    var _legend_nls = Ranges[1][_idx_legend_item].Legends[8];

    if (_legend_minValue) {
      _legend_minValue = _legend_minValue.replace(",", ".");
    }

    if (_legend_maxValue) {
      _legend_maxValue = _legend_maxValue.replace(",", ".");
    }

    var _legend_sub_item = new WSILegendItem(_legend_color, _legend_name, _legend_minValue, _legend_maxValue, _value, _legend_operator, null, _legend_id, 0, _legend_nls);

    this.Items[Ranges[1][_idx_legend_item].Legends[0]].Add(_legend_sub_item);
  }

  //Loading CustomAlarms
  for (_idx_legend_item in Ranges[2]) {
    var _custom_legends_alarms_list = JSON.parse(Ranges[2][_idx_legend_item].CustomFilters[1]);
    var _legend_name = Ranges[2][_idx_legend_item].CustomFilters[0];
    var _custom_alarm_display_id = Ranges[2][_idx_legend_item].CustomFilters[3];
    var _idx_legend_item_counter = this.Items.length;
    var _legend_sub_item = new WSILegend();

    _legend_sub_item.ID = Ranges[2][_idx_legend_item].CustomFilters[2];;
    _legend_sub_item.Name = _legend_name;
    _legend_sub_item.SectionID = 80;
    _legend_sub_item.CustomFilter = _custom_legends_alarms_list;

    this.Add(_idx_legend_item_counter, _legend_sub_item);

    this.Items[_idx_legend_item_counter].Items = JSON.parse(Ranges[2][_idx_legend_item].CustomFilters[1]);
    this.Items[_idx_legend_item_counter].Display = JSON.parse(Ranges[2][_idx_legend_item].CustomFilters[3]);
  }

  if (_legend_item.SectionID == Enum.EN_SECTION_ID.PLAYERS || _legend_item.SectionID == Enum.EN_SECTION_ID.MACHINES) {
    // Parse to set values for the heatmap
    for (_idx_legend in this.Items) {
      var _legend_item = this.Items[_idx_legend];
      var _count = _legend_item.Items.length - 1;
      var _increment = (100 - CONST_MIN_HEATMAP_VALUE) / _count;
      var _x = 0;

      if (_legend_item.SectionID != "80") {
        // JBC - Only increment floor values
        for (_range in _legend_item.Items) {
          if (_legend_item.Items[_range].value) {
            _legend_item.Items[_range].value = (_increment * _x);
            _x += 1;
          }
        }
      }
    }
  }

  PrintRanges("div_legend_container_20300", "20", this.Items); //Floor
  PrintRanges("div_legend_container_41001", "41", this.Items); //Player Alarms
  PrintRanges("div_legend_container_42002", "42", this.Items); //Machine Alarms
  PrintRanges("div_legend_container_60200", "60", this.Items); //Players
  PrintRanges("div_legend_container_70002", "70", this.Items); //Machines
  PrintRanges("div_legend_container_80001", "80", this.Items); //Monitor
  PrintRanges("div_legend_container_99000", "99", this.Items); //Custom
  WSILogger.Write("Ranges_Init");

  this.Dictionary_Alarms = [];
  this.Dictionary_Alarms[41] = []; //User
  this.Dictionary_Alarms[42] = []; //Machine
  this.Dictionary_Alarms[43] = []; //Custom
  this.AlarmsPriority = [];
  this.AlarmsPriority[0] = [];
  this.AlarmsPriority[1] = [];
  this.AlarmsPriority[2] = [];
  this.AlarmsPriority[3] = [];

  var _json_alarms = Ranges[3];

  //Load Alarmas Dictionary
  for (_idx_dic_alarm in _json_alarms) {
    var _dic_alarm_type = _json_alarms[_idx_dic_alarm].AlarmsDictionary[0];
    var _dic_alarm_id = _json_alarms[_idx_dic_alarm].AlarmsDictionary[2];
    var _dic_alarm_name = _json_alarms[_idx_dic_alarm].AlarmsDictionary[1];
    var _dic_priority = _json_alarms[_idx_dic_alarm].AlarmsDictionary[4];
    var _dic_nls_range = _json_alarms[_idx_dic_alarm].AlarmsDictionary[5];
    var _dic_nls_legend = _json_alarms[_idx_dic_alarm].AlarmsDictionary[6];

    //this.Dictionary_Alarms[+_dic_alarm_type][+_dic_alarm_id] = _dic_alarm_name;
    this.Dictionary_Alarms[+_dic_alarm_type][+_dic_alarm_id] = "<span data-nls='" + _dic_nls_range + "'>" + _Language.get(_dic_nls_range) + "</span><span>&nbsp - &nbsp</span><span data-nls='" + _dic_nls_legend + "'>" + _Language.get(_dic_nls_legend) + "</span>";
    this.AlarmsPriority[+_dic_priority][+_dic_alarm_id] = +_dic_alarm_id;
  }
}

//Print items into section
function PrintRanges(Section, SectionID, RangesList) {
  // Draw Legends
  var _section = Section;
  var _content = '';
  var _idx = -1;
  var _section_id = SectionID;
  var _legend_item;

  for (_legend in RangesList) {
    // TODO: take care about type of legend, associated section
    _legend_item = RangesList[_legend];

    if (_legend_item) {
      if (_legend_item.SectionID == _section_id) {
        if ((SectionID != 60 && SectionID != 70) || _legend_item.Visible=="True") {
          _content += _legend_item.Build(_legend, _idx, SectionID);
          _idx++;
        }
      }
    }
  }

  $("#" + _section).html(_content);
  $("#" + _section).accordion(m_accordion_props);

}

function PrepareCustomAlarm(LegendItem) {
  return "";
}

WSILegends.prototype.CheckCustomFilter = function (ActiveFilter, TerminalProperties) {
  var _legend = _Legends.Items[ActiveFilter];
  var _sub_legends_count = { value: 0 };
  var _flags = 0;
  var _result = {
    color: "white", name: "Unknown", value: 0, icon: ""
  };
  var _legend_display;

  for (_idx_ccf_legend_item in _legend.Items) {
    if (this.CheckCustomFilterSubLegend(_legend.Items[_idx_ccf_legend_item], TerminalProperties, _sub_legends_count)) {
      _flags++;
    }
  }

  if (_flags > 0 && _flags == _sub_legends_count.value) {
    //CheckDisplay
    for (_idx_ccf_legend_item_display in _legend.Display) {
      if (_legend.Display[_idx_ccf_legend_item_display]) {
        _legend_display = _legend.Display[_idx_ccf_legend_item_display];
      }
    }

    _result = this.GetCheckCustomFilter(_legend_display, TerminalProperties);
  }

  return _result;
}

WSILegends.prototype.CheckCustomFilterSubLegend = function (Legend, Data, SubLegendsCount) {

  var _result = null;
  var _data_value;
  var _ok = false;


  if (Legend) {
    SubLegendsCount.value += 1;
    _data_value = Data[m_field_properties[Legend.Property].field];

    if (_data_value != "" && _data_value != null) {

      for (_i = 0; _i < Legend.Items.length; _i++) {

        _ok = false;

        var _Li = Legend.Items[_i];

        if (_Li) {
          // Numeric
          if ((+_data_value == _data_value) || (_data_value.toString().localeCompare(_data_value.toString()) == 0)) {

            switch (_Li.Operator) {
              case "=":
                if (+_data_value == +_Li.minValue || (_data_value.toString().localeCompare(_Li.minValue.toString()) == 0)) {
                  _ok = true;
                }
                break;
              case "<":
                if (+_data_value < +_Li.minValue) {
                  _ok = true;
                }
                break;
              case ">":
                if (+_data_value > +_Li.minValue) {
                  _ok = true;
                }
                break;
              case "Between":
                if ((+_data_value >= +_Li.minValue) && ((+_data_value <= +_Li.maxValue))) {
                  _ok = true;
                }
                break;
              default:
                if (_Li.minValue > 0) {
                  if ((parseInt(_data_value) & parseInt(_Li.minValue)) == parseInt(_Li.minValue)) {
                    _ok = true;
                    break;
                  }
                }
                break;
            }
            if (_ok) {
              break;
            }
          }
        }
      } //for (_i = 0; _i < _L.Items.length; _i++) {
    } //if (_data_value != "" && _data_value != null) {
  } //if (Legend) {

  return _ok;
}

WSILegends.prototype.GetCheckCustomFilter = function (Display, Data) {
  var _L = Display;

  if (_L) {

    var _result = null;
    var _data_value = Data[m_field_properties[_L.Property].field];

    var _ok;

    if (_data_value != "" && _data_value != null) {

      for (_i = 0; _i < _L.Items.length; _i++) {

        _ok = false;

        var _Li = _L.Items[_i];

        if (_Li == null) {
          continue;
        }

        // Numeric
        if ((+_data_value == _data_value) || (_data_value.toString().localeCompare(_data_value.toString()) == 0)) {

          switch (_Li.Operator) {
            case "=":
              if (+_data_value == +_Li.minValue || (_data_value.toString().localeCompare(_Li.minValue.toString()) == 0)) {
                _ok = true;
              }
              break;
            case "<":
              if (+_data_value < +_Li.minValue) {
                _ok = true;
              }
              break;
            case ">":
              if (+_data_value > +_Li.minValue) {
                _ok = true;
              }
              break;
            case "Between":
              if ((+_data_value >= +_Li.minValue) && ((+_data_value <= +_Li.maxValue))) {
                _ok = true;
              }
              break;
            default:
              if (_Li.minValue > 0) {
                if ((parseInt(_data_value) & parseInt(_Li.minValue)) == parseInt(_Li.minValue)) {
                  _ok = true;
                  break;
                }
              }

              break;
          }

        } else {
          // TODO: Strings if needed
        }

        if (_ok) {

          // Trick: for negative values in legends with one range "< 0", abs(value)
          if (_L.Items.length == 1) {
            if (+_L.Items[0].minValue == 0 && _L.Items[0].Operator == "<") {
              _result = { color: _Li.color, name: _Li.name, value: Math.abs(+_data_value) };
            } else {
              _result = { color: _Li.color, name: _Li.name, value: _Li.value };
            }
          } else {
            _result = { color: _Li.color, name: _Li.name, value: _Li.value };
          }

          break;
        }
      }
    }

    if (_result == null) {
      _result = { color: Display.UnknownItem.color, name: Display.UnknownItem.name, value: 0, icon: "", prop: _L.Property };
    }

  } else {
    WSILogger.Log("WSILegends.Check()", "Legend '" + Display + "' does not exist!");
  }

  _result.prop = _L.Property;

  return _result;
}

// When we click on Filter Mode button
function onClickFilterMode(SectionID, editing) {
  var $_filter = $("#filtermode_" + SectionID);


  if ($_filter) {
    //If filtermode disabled 
    if ($_filter.attr("data-filtermode") == "0") {

      //Enable filtermode
      $_filter.attr("data-filtermode", "1");

      //Set mouse type to hand 
      $(".childLegend[id ^= 'childlegend_" + SectionID + "']").css('cursor', 'pointer');

      if (!editing && SectionID == 80 && m_custom_alarm_edit_mode) {
        onClickCustomAlarmEditMode();
        $("#filtermode_" + m_current_section).removeClass("btnEdicionFiltrosOff");        
      }

      m_filter_mode_enabled = true;
      
      //Open dialog
      DialogCreateFilterMode(SectionID);
    } else {
      $_filter.attr("data-filtermode", "0");
      m_filter_mode_enabled = false;
      m_apply_filter = false;
      m_filter_list_selected = [];
      HideFilterModeDialog();
      $("#btn_save_filter_section_" + SectionID).css("display", "none");
      $("#filtermode_" + m_current_section).removeClass("filtersActivated")

      ClearSectionFilters(m_current_section, true)
      $(".childLegend").css('cursor', 'default');

      if (m_current_section == 80) {
        $(".btnCreaFiltro").removeClass("filtersActivated");

      }

      // RMS - Update terminals only when 2D mode (3D will do it automatically on pre-render
      if (_LayoutMode == 2) {
        // Update terminals when new filter is activated
        UpdateTerminals();
      }
    }
  }
}

//click event on Range items. 
function Child_Legend_Click(SectionID, ID, state) {
  if (m_filter_mode_enabled) {
    //When click on filter button, the dialog hides.  If we click on ranges, the dialog should appear.
    $("#div_dialog_background_filter_mode").show();
    $("#btn_save_filter_section_" + m_current_section).css("display", "none");

    //Highlight control
    if ($("#childlegend_" + ID).hasClass("childLegendSelected")) {
      $("#childlegend_" + ID).removeClass("childLegendSelected")
    } else {
      $("#childlegend_" + ID).addClass("childLegendSelected")
    }

    //Refresh list of filters selected.
    RefreshFilterContainer(ID, state);
    
  }
}

function Child_Monitor_Click(LegendId, RangeId) {
  $_monitor_legend = $("#leyenda_item_80_" + LegendId + "[data-alarmid='" + RangeId + "']");

  if (m_custom_alarm_edit_mode == true) {
    var _custom_alarm_selected = _Legends.Items[LegendId].Items[RangeId];
    ClearSectionFilters(80, true);
    DialogCreateSimpleFilter();

    m_custom_alarm_edit_id = RangeId;
    m_custom_alarm_edit_sectionid = LegendId;

    //FillDialog with info.
    $("#txt_create_filter_name").val(_custom_alarm_selected.name);

    $("#chkFilterSound").prop("checked", _custom_alarm_selected.hasSound);

    $("#chkFilterEnabled").prop("checked", _custom_alarm_selected.Enabled);

    $(".selectorColor").find("li").each(function (index, value) {
      if ($(value).css("background-color") == _custom_alarm_selected.color) {

        $(value).addClass("selectorColorSelected");

        $(value).animate({ "opacity": "1" });
        $(value).siblings().animate({ "opacity": "0.3" });
      }

      WSILogger.Write(" ");
    }
  )
    //onComboColorValueChanged();
  }

  if ($_monitor_legend.hasClass("childLegendSelected")) {
    $_monitor_legend.removeClass("childLegendSelected")
  } else {
    $_monitor_legend.addClass("childLegendSelected")
  }
}

//Refresh list of filters selected.
function RefreshFilterContainer(RangeControlID, state) {
  var $filtercontrol = $("#childlegend_" + RangeControlID);
  var _range_id_index = m_filter_list_selected[RangeControlID.substring(2, RangeControlID.length)];

  if (_range_id_index != null) {
    //        m_filter_list_selected.splice(RangeControlID.substring(2, RangeControlID.length), 1); //delete selected range
    delete m_filter_list_selected[RangeControlID.substring(2, RangeControlID.length)];
    m_filter_list_selected.length = m_filter_list_selected.length - 1;
  } 
  if (_range_id_index == null || state) {
    //this item is used to save modifications of range values.
    var _item = {
      Operator: "", minValue: 0, maxValue: 0
    };

    m_filter_list_selected[RangeControlID.substring(2, RangeControlID.length)] = _item;
    m_filter_list_selected.length = m_filter_list_selected.length + 1;
  }

  var _legend_id = $filtercontrol.attr("data-legendid");
  var _range_id = $filtercontrol.attr("data-rangeid");

  FilterListAddItem(_legend_id, _range_id);

}

//Refresh the list of selected filters into the dialog form.
function FilterListAddItem(LegendId, RangeId) {

  if ($("#ul_selected_filter_list").children().length == 0) {

    addLegendList(LegendId);
    addRangeToLegendList(LegendId, RangeId);

  } else {
    if ($("#list_legend_id_" + LegendId).length == 0) {
      addLegendList(LegendId);
      addRangeToLegendList(LegendId, RangeId);
    } else if ($("#list_range_id_" + RangeId + "[data-legendid='" + LegendId + "']").length == 0) {
      addRangeToLegendList(LegendId, RangeId);
    } else {
      removeRangeFromList(LegendId, RangeId);
    }
  }
}

//add selected range into the legend  list of the dialog form
function addRangeToLegendList(LegendId, RangeId) {
  var _range_is_editable = _Legends.Items[LegendId].Items[RangeId].Editable;
  var _content = "";

  _content += "<li class='itemRangesList' id='list_range_id_" + RangeId + "' data-legendid='" + LegendId
           + "' data-range='" + RangeId + "' ";

  _content = _content + ">" + _Legends.Items[LegendId].Items[RangeId].name + "</li>";;

  $("#list_legend_id_" + LegendId + " .children").append(_content);

}

//add selected legend into the list of the dialog form
function addLegendList(LegendId) {
  var _str_title = "";

  switch (_Legends.Items[LegendId].SectionID) {
    case "60":
      _str_title = "<span data-nls='NLS_PLAYERS'>" + _Language.get('NLS_PLAYERS') + "</span> - <span data-nls='"+_Legends.Items[LegendId].Nls+"'>"+_Language.get(_Legends.Items[LegendId].Nls)+"</span>";
      break;

    case "70":
      _str_title = "<span data-nls='NLS_TERMINALS'>" + _Language.get('NLS_TERMINALS') + "</span> - <span data-nls='" + _Legends.Items[LegendId].Nls + "'>" + _Language.get(_Legends.Items[LegendId].Nls) + "</span>";
      break;

    default:
      break;
  }

  var _str = "id='list_legend_id_" + LegendId + "' data-legendid='" + LegendId
           + "'><div class='chk_filter_visualization chk_filter_visualization_unchecked' onclick='chk_click_legend(this)'></div><b>" + _str_title + "</b>";

  $("#ul_selected_filter_list").append("<li " + _str + "</li>");
  $("#list_legend_id_" + LegendId).append("<ul id='list_legend_id_" + LegendId + "' class='children' ></ul>");

}

//remove selected legend of the list of the dialog form
function removeRangeFromList(LegendId, RangeId) {
  $("#list_range_id_" + RangeId + "[data-legendid='" + LegendId + "']").remove();

  if ($("#list_legend_id_" + LegendId + " .children").children().length == 0) {
    $("#list_legend_id_" + LegendId).remove();
  }

  if ($(".chk_filter_visualization:checked").length == 0) {
    $(".chk_filter_visualization").prop("disabled", false);
  }
}

function onclick_Range(LegendId, RangeId) {

  //    $.alert(ControlID);
  DialogSlider(LegendId, RangeId);
}

function chk_click_legend(evt) {
  $(".chk_filter_visualization").removeClass('chk_filter_visualization_checked');
  $(".chk_filter_visualization").addClass('chk_filter_visualization_unchecked');
  $($(evt)[0]).addClass('chk_filter_visualization_checked');
  $($(evt)[0]).removeClass('chk_filter_visualization_unchecked');
}

function SaveCustomAlarmFailed() {

  LayoutOkOpen("Error", "Selecciona algun filtro", function () { });

  $("#div_dialog_background").hide();

}

function ApplyFilter(SectionID) {
}
// Linked  with Filter Alarm button
function UpdateTerminalsAlarm() {
  UpdateSelectedAlarms(true);
  m_alarms_to_print = m_alarms_selected;

  RefreshListLegendDetailDialog(m_alarms_to_print);

  // RMS - Update terminals only when 2D mode (3D will do it automatically on pre-render
  if (_LayoutMode == 2) {
    // Update terminals when new filter is activated
    UpdateTerminals();
  }
}

// Save Custom Alarm into DB and add to Legends List
function SaveCustomAlarm() {
  var _filter_name = $("#input_new_filter_name").val();
  var _filter_type;
  var _filter_sound;
  var _filter_color
  var _list_selected;
  var _filter_enabled;
  var _filter_id;
  var _display_legend_id;
  var _display_legend = "";
  var _legends_items = "";
  var _result = {};

 
  if (m_current_section == 80 && m_custom_alarm_edit_mode) {
    //Update Custom Alarm
    _display_legend_id = $(".chk_filter_visualization.chk_filter_visualization_checked").parent().data("legendid");
    _result = ParseListToCustomAlarm(m_filter_list_selected, _display_legend_id);
    _filter_enabled = true;
    _filter_id = _Legends.Items[m_custom_alarm_edit_id].ID;
  }
  else {
    //Save Custom Alarm
    _display_legend_id = $(".chk_filter_visualization.chk_filter_visualization_checked").parent().data("legendid");
    _result = ParseListToCustomAlarm(m_filter_list_selected, _display_legend_id);
    _filter_enabled = true;
    _filter_id = 0;
  }

  var _filter_parameters = JSON.stringify(_result.ItemsToFilter);
  var _filter_display = JSON.stringify(_result.ItemsToDisplay);

  if (_filter_name != "") {

    PageMethods.SaveCustomAlarm(_filter_name, _filter_parameters, _filter_display, _filter_id, function (Result) {

      if (Result != 0) {
        SaveCustomAlarmComplete(_filter_name, Result, _result.ItemsToFilter, _result.ItemsToDisplay);
      } else {
        UpdateCustomAlarmComplete(m_custom_alarm_edit_id, _result, _filter_name);
      }

    }, SaveCustomAlarmFailed);
  } else {
    $("#lbl_create_filter_error").text("Debe indicar un nombre para la alarma");
  }
}

function ParseListToCustomAlarm(List, DisplayId) {
  /// <summary>
  /// Takes array list of custom alarms selected and transform to object before save in database.
  /// </summary>
  /// <param name="List"></param>

  var _result = [];
  var _items_to_display = [];
  var _filter_items = [];

  List.sort();

  for (_idx_filter_list_item in List) {

    _filter_list_item_array = _idx_filter_list_item.split("_");

    var _legend_id = _filter_list_item_array[0];
    var _alarm_id = _filter_list_item_array[1];

    //Add to filter collection
    if (_filter_items[_legend_id]) {
      _filter_items[_legend_id].Items[_alarm_id] = _Legends.Items[_legend_id].Items[_alarm_id];
    } else {
      _filter_items[_legend_id] = jQuery.extend(true, {}, _Legends.Items[_legend_id]);;
      _filter_items[_legend_id].Items = [];
      _filter_items[_legend_id].Items[_alarm_id] = _Legends.Items[_legend_id].Items[_alarm_id];
    }

    /// Add to display collection
    if (_legend_id == DisplayId) {
      if (_items_to_display[_legend_id]) {
        _items_to_display[_legend_id].Items[_alarm_id] = _Legends.Items[_legend_id].Items[_alarm_id];
      } else {
        _items_to_display[_legend_id] = jQuery.extend(true, {}, _Legends.Items[_legend_id]);;
        _items_to_display[_legend_id].Items = [];
        _items_to_display[_legend_id].Items[_alarm_id] = _Legends.Items[_legend_id].Items[_alarm_id];
      }
    }
  }

  var _result = {};

  _result.ItemsToDisplay = _items_to_display;
  _result.ItemsToFilter = _filter_items;

  return _result;
}

function SaveCustomAlarmComplete(filterName, ID, FilterParameters, FilterDisplay) {
  /// <summary>
  /// Create Item and add into Legends array, after creates display item in the Monitor Section
  /// </summary>
  /// <param name="filterName"></param>
  /// <param name="filterList"></param>
  /// <param name="ID"></param>
  /// <param name="DisplayId"></param>
  var $_accordion_last_child = $("#div_legend_container_80001:last-child");
  var _legend_item = new WSILegend();
  var _idx_li = _Legends.Items.length;
  var _display_legend;
  var _result = "";

  _legend_item.Name = filterName;
  _legend_item.SectionID = 80;
  _legend_item.ID = ID;

  _Legends.Add(_idx_li, _legend_item);

  _Legends.Items[_idx_li].Items = FilterParameters;
  _Legends.Items[_idx_li].Display = FilterDisplay;

  _result += '<h3 onclick="SetCurrentLegend(' + _idx_li + ',' + _legend_item.SectionID + ');" data-id="' + _idx_li + '" data-section="' + _legend_item.SectionID + '">' + _legend_item.Name;
  _result += '</h3>';
  _result += '<div><ul>';
  _result += "<li class='childLegend' style='background-color:whiteSmoke '>";
  _result += '<div class="circuloIcono">';


  for (_idx_display_legend in _Legends.Items[_idx_li].Display) {
    if (_Legends.Items[_idx_li].Display[_idx_display_legend]) {
      _display_legend = _Legends.Items[_idx_li].Display[_idx_display_legend];
    }
  }

  _result += '</div><span>' + _display_legend.Name + '</span></li>';

  for (_idx_child_monitor in _display_legend.Items) {
    _build_item = _display_legend.Items[_idx_child_monitor];
    if (_build_item != null) {
      _result += "<li class='childLegend' id='childlegend_" + _legend_item.SectionID + _idx_display_legend + "_" + _idx_child_monitor + "' onclick='Child_Legend_Click(" + _legend_item.SectionID + ",\"" + _legend_item.SectionID +
                  _idx_display_legend + "_" + _idx_child_monitor + "\");' data-rangeid='" + _idx_child_monitor + "' data-legendid='" + _idx_display_legend + "'>";
      _result += '<div class="circuloValor" data-sectionid="' + _legend_item.SectionID + '" id="leyenda_item_' + _legend_item.SectionID + '_' + _idx_display_legend + '" style="background: ' + _build_item.color + ';">';
      _result += '</div><span>' + _build_item.name + '</span></li>';
    }
  }

  _result += '</ul></div>';

  $_accordion_last_child.append(_result);
  $("#div_legend_container_80001").accordion("refresh");
}

//UpdateCustomAlarmComplete(m_custom_alarm_edit_id, _result, _filter_name);
function UpdateCustomAlarmComplete(CustomAlarmId, FilterList, CustomAlarmName) {
  var _custom_filter = _Legends.Items[CustomAlarmId];

  _custom_filter.Name = CustomAlarmName;
  _custom_filter.Items = FilterList.ItemsToFilter;
  _custom_filter.Display = FilterList.ItemsToDisplay;

  m_custom_alarm_edit_id = undefined;

  //repaint legend into accordion
  var $_accordion_legend_container = $("#div_legend_container_80001 [data-id=" + CustomAlarmId + "]");
  var _display_legend;
  var _result = "";
  var _text_legend = "";

  var active_elem = $("#div_legend_container_80001").accordion("option", "active");
  

  _text_legend += _custom_filter.Name;

  $_accordion_legend_container.text(_text_legend);

  var _legend_subitems_container = $_accordion_legend_container.next();

  _legend_subitems_container.css("display", "none");
  _legend_subitems_container.remove();

  _result += '<div><ul>';
  _result += "<li class='childLegend' style='background-color:whiteSmoke '>";
  _result += '<div class="circuloIcono">';

  for (_idx_display_legend in _custom_filter.Display) {
    if (_custom_filter.Display[_idx_display_legend]) {
      _display_legend = _custom_filter.Display[_idx_display_legend];
    }
  }

  _result += '</div><span>' + _display_legend.Name + '</span></li>';

  for (_idx_child_monitor in _display_legend.Items) {
    _build_item = _display_legend.Items[_idx_child_monitor];
    if (_build_item != null) {
      _result += "<li class='childLegend' id='childlegend_" + _custom_filter.SectionID + _idx_display_legend + "_" + _idx_child_monitor + "' onclick='Child_Legend_Click(" + _custom_filter.SectionID + ",\"" + _custom_filter.SectionID +
                  _idx_display_legend + "_" + _idx_child_monitor + "\");' data-rangeid='" + _idx_child_monitor + "' data-legendid='" + _idx_display_legend + "'>";
      _result += '<div class="circuloValor" data-sectionid="' + _custom_filter.SectionID + '" id="leyenda_item_' + _custom_filter.SectionID + '_' + _idx_display_legend + '" style="background: ' + _build_item.color + ';">';
      _result += '</div><span>' + _build_item.name + '</span></li>';
    }
  }

  _result += '</ul></div>';

  $_accordion_legend_container.after(_result);
  $("#div_legend_container_80001").accordion("refresh");


  $_accordion_legend_container.prepend('<div class="BtnClose close btnEditarFiltroCustom" id="btn_close_create_filter"></div>');
  $_accordion_legend_container.prepend('<div class="BtnClose close btnBorrarFiltroCustom" id="btn_edit_filter"></div>');
  $_accordion_legend_container.find(".BtnClose.close.btnEditarFiltroCustom").click(function (evt) {
    var $_target = $(evt.target);
    onClickFilterMode(80,true);
    EditCustomLegend($_target);

  });
  $_accordion_legend_container.find(".BtnClose.close.btnBorrarFiltroCustom").click(function (evt) {
    var $_target = $(evt.target);
    DeleteCustomLegend($_target);
  });
  $("#div_legend_container_80001").accordion("option", "active", false);
  $("#div_legend_container_80001").accordion("option", "active", active_elem);

}

function onClickCustomAlarmEditMode() {
  m_custom_alarm_edit_mode = !m_custom_alarm_edit_mode;
  

  if (m_custom_alarm_edit_mode) {
    $("#div_legend_container_80001 h3").prepend('<div class="BtnClose close btnEditarFiltroCustom" id="btn_close_create_filter"></div>');
    $("#div_legend_container_80001 h3").prepend('<div class="BtnClose close btnBorrarFiltroCustom" id="btn_edit_filter"></div>');
    $("#div_legend_container_80001 h3").find(".BtnClose.close.btnEditarFiltroCustom").click(function (evt) {
      var $_target = $(evt.target);
      onClickFilterMode(80, true);
      EditCustomLegend($_target);

    });
    $("#div_legend_container_80001 h3").find(".BtnClose.close.btnBorrarFiltroCustom").click(function (evt) {
      var $_target = $(evt.target);
      DeleteCustomLegend($_target);
    });

  } else {
    $("#div_legend_container_80001 h3").find("div").remove();
  }
}


function DeleteCustomLegend(Container) {

  var _parent = $(Container).parent();
  var _cl_id = _parent.data("customlegendid");
  var _legend_id = _parent.data("id");

  LayoutYesNoOpen(_Language.get('NLS_WARNING'), _Language.get('NLS_SURE_TO_DELETE_CUSTOM_FILTER'), function () {
    PageMethods.DeteleCustomFilter(+_cl_id, OnDeleteCustomLegendComplete(+_cl_id, _legend_id), function () {
      LayoutYesNoOpen(_Language.get('NLS_WARNING'), "Ha ocurrido un problema eliminando", function () { }, function () { });
    });
  }, function () { });
}

function OnDeleteCustomLegendComplete(ContainerId, LegendId) {

  ////Delete data from array  
  //_Legends.Items.splice(LegendId, 1);

  //Delete container from section
  $("#div_legend_container_80001").find("[data-id=" + LegendId + "]").next().remove()
  $("#div_legend_container_80001").find("[data-id=" + LegendId + "]").remove()

  var _default_legend = $("#div_legend_container_80001 h3").first().data("id");

  m_current_filter[80] = _default_legend;
  SetCurrentLegend(_default_legend, 80);
}

function EditCustomLegend(Container) {
  var _parent = $(Container).parent();
  var _legend_id = _parent.data("id");
  var _legend = _Legends.Items[_legend_id];
  var _display_id_legend;

  m_custom_alarm_edit_id = _legend_id;

  //añadir nombre del friltro en input_new_filter_name.
  $("#input_new_filter_name").val(_legend.Name);

  //añadir class childLegendSelected a los elementos marcados.
  for (_idx_legend_items in _legend.Items) {
    var _li = _legend.Items[_idx_legend_items];

    if (_li) {
      for (_idx_legend_items_subitem in _li.Items) {
        var _leyend_sub_item = _li.Items[_idx_legend_items_subitem];

        if (_leyend_sub_item) {

          Child_Legend_Click(_li.SectionID, _li.SectionID + "" + _idx_legend_items + "_" + _idx_legend_items_subitem, true);
          //$("#div_filter_list_players_acc").find("li[data-legendid=" + _idx_legend_items + "][data-rangeid=" + _idx_legend_items_subitem + "]").addClass("childLegendSelected");
        }
      }
    }
  }

  for (_idx_display_legend in _legend.Display) {
    if (_legend.Display[_idx_display_legend]) {
      _display_id_legend = _idx_display_legend;
    }
  }

  $($("#div_selected_filter_list #list_legend_id_" + _display_id_legend).children()[0]).removeClass("chk_filter_visualization_unchecked")
  $($("#div_selected_filter_list #list_legend_id_" + _display_id_legend).children()[0]).addClass("chk_filter_visualization_checked")
}

WSILegends.prototype.CheckAlarmsByPriority = function (LegendId, Data) {
  var _L = _Legends.Items[LegendId];
  var _ok = false;
  var _result = null;

  for (_idx_alarm_id = _Legends.AlarmsPriority.length - 1; _idx_alarm_id >= 0; _idx_alarm_id--) {
    for (_idx_alarm_priority in _Legends.AlarmsPriority[_idx_alarm_id]) {
      var _alarm_id = _Legends.AlarmsPriority[_idx_alarm_id][_idx_alarm_priority];
      var _prop_id = parseInt(_alarm_id / 1000, 10);
      var _sub_category = parseInt(_alarm_id.toString().slice(2, 5));

      var _prop_value = m_field_properties[_prop_id];

      if (_prop_value) {
        if (Data[_prop_value.field] > 0) {
          if ((Data[_prop_value.field] & _sub_category) == _sub_category) {
            _ok = true;

            break;
          }

          }
      }
    }

    if (_ok) {
      _ok = true;

      break;
    }
  }

  if (_ok) {
    for (i = _L.Items.length - 1; i > 0; i--) {
      if (_L.Items[i].value == _idx_alarm_id) {
        var _Li = _L.Items[i];
        break;
      }
    }
    _result = { color: _Li.color, name: _Li.name, value: _Li.value };
  } else {
    _result = { color: _Legends.Items[LegendId].UnknownItem.color, name: _Legends.Items[LegendId].UnknownItem.name, value: 0, icon: "", prop: _L.Property };
  }

  // Return 
  return _result;
}

WSILegends.prototype.CheckAlarmsSound = function (LegendsList, Data) {

  for (_idx_sound_legend_id in LegendsList) {
    var _li = _Legends.Items[_idx_sound_legend_id];

    var _data_value = Data[m_field_properties[_li.Property].field];

    if (_data_value > 0) {

      return true;
    }
  }
}
  