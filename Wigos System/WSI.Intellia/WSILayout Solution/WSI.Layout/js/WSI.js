﻿// ---------------------------------------------------------------------

var m_mouse_actions_mousedown = false;
var m_mouse_actions_mousemove_before_mousedown = false;
var m_update_terminals_pending = false;

// Pause on other tabs
var m_paused = false;

// ---------------------------------------------------------------------

// Object with WSI functions
var WSI = {

  // 
  requestId: undefined,

  //
  clicker: null,

  //
  Rotate: 0,
  RotateValue: 0,
  Pan: 0,
  PanValue: 0,
  Advance: 0,
  AdvanceValue: 0,

  //
  //CameraMode: EN_CAMERA_MODE.CM_PERSPECTIVE,
  //Cenital: 0,
  CamOldPosition: null,
  CamOldRotation: null,

  //
  objectcounter: 0,

  // The document element that contains the WebGL 3d view
  container: null,

  // Main scene object
  scene: null,

  // The main camera object
  //camera: null,
  //Cenitcam: null,
  //FPcam: null,

  // Basic camera controls // js/libs/OrbitControls.js
  controls: null,
  Cenitcontrols: null,

  // Projection controller
  projector: null,

  // Mouse position for moving and raycasting
  mouse2D: new THREE.Vector3(),

  // The main renderer object
  renderer: null,

  // Controls for FPS informations // js/libs/stats.min.js
  stats: null,

  // Basic ilumination 
  AmbientLight: null,
  RightLight: null,
  LeftLight: null,
  TopLight: null,
  BottomLight: null,
  UpLight: null,

  // The main pivot for the scene
  PivotMesh: new THREE.Object3D(),

  // Clock
  Clock: new THREE.Clock(),

  // 
  RightPanelDistance: 0,

  // RayCaster
  Raycaster: new THREE.Raycaster(),

  // Init: function to initialize the system
  Init: function (ContainerID) {

    WSILogger.Write("WSI Initialization routine:");
    WSILogger.Write("===========================");

    try {

      // Container
      WSILogger.Write(" - Container creation...");
      if (ContainerID) {
        WSILogger.Write("     - Using existing container element '" + ContainerID + "'");
        this.container = document.getElementById("contenedor");
      } else {
        // Check for previous
        var _inst = document.getElementById('WSIRenderer');
        if (_inst) { _inst.parentElement.removeChild(_inst); }
        // Create Conainter element
        WSILogger.Write("     - Creating container element 'WSIRenderer'");
        this.container = document.createElement('div');
        this.container.setAttribute('id', 'WSIRenderer');
        document.body.appendChild(this.container);
      }

      _Manager.Cameras = new WSICameraManager();
      //// Camera
      //WSILogger.Write(" - Creating perspective camera...");
      //this.camera = new THREE.PerspectiveCamera(50, window.innerWidth / window.innerHeight, 1, 500);
      ////this.camera = new THREE.OrthographicCamera(window.innerWidth / -2, window.innerWidth / 2, window.innerHeight / 2, window.innerHeight / -2, 1, 1000);
      ////this.camera = new THREE.OrthographicCamera(0.5 * window.innerWidth / -2, 0.5 * window.innerWidth / 2, window.innerHeight / 2, window.innerHeight / -2, 150, 1000);
      ////this.camera.aspect = 0.5 * window.innerWidth / window.innerHeight;
      //this.camera.position.y = 50;
      //this.camera.position.z = 100;
      ////this.camera.lookAt(new THREE.Vector3(0, 0, 0));

      //WSILogger.Write(" - Creating Cenit-camera...");
      //this.Cenitcam = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 1, 500);
      //this.Cenitcam.position.x = 0;
      //this.Cenitcam.position.y = 400;
      //this.Cenitcam.up = new THREE.Vector3(0, 0, -1);

      //WSILogger.Write(" - Creating First Person camera...");
      //this.FPcam = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 1, 500);
      //this.FPcam.position.x = 0;
      //this.FPcam.position.y = 6;
      //this.FPcam.position.z = 0;
      //this.FPcam.up = new THREE.Vector3(0, 1, -1);

      // Scene
      WSILogger.Write(" - Creating Scene...");
      this.scene = new THREE.Scene();

      // Projector
      WSILogger.Write(" - Creating Projector...");
      this.projector = new THREE.Projector();

      // Initialize Mouse2D
      WSILogger.Write(" - Initializing Mouse 2D vector...");
      this.mouse2D = new THREE.Vector3(0, 10000, 0.5);

      // Renderer
      WSILogger.Write(" - Creating renderer element...");
      if (Detector.webgl) {
        WSILogger.Write("   - 'WebGLRenderer' supported.");
        //this.renderer = new THREE.WebGLRenderer({ antialias: true });
        this.renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });

        //this.renderer = new THREE.WebGLRenderer3({ contextAttributes: { antialias: false} });
        //this.renderer = new THREE.WebGLRenderer();
      } else {
        //WSILogger.Write("   - 'CanvasRenderer' supported.");
        //this.renderer = new THREE.CanvasRenderer();
        WSILogger.Write("   - 'SoftRenderer' supported.");
        this.renderer = new THREE.SoftwareRenderer();
      }
      this.renderer.domElement.id = "WebGLRenderer";
      this.renderer.setClearColor(0x404040);
      this.renderer.setPixelRatio(window.devicePixelRatio);
      this.renderer.setSize(window.innerWidth, window.innerHeight);
      //this.renderer.setSize(1024, 768);
      this.container.appendChild(this.renderer.domElement);
      
      var canvas = document.getElementById("WebGLRenderer");
      //canvas.addEventListener('webglcontextlost', function () {
      //  event.preventDefault();
      //  WSILogger.Write(" - Context Lost, cancel animation request (" + WSI.requestId + ")");
      //  if (WSI.requestId) {
      //    //window.cancelRequestAnimationFrame(WSI.requestId);
      //    window.cancelAnimationFrame(WSI.requestId);
      //    var canvas = document.getElementById("WebGLRenderer");
      //    //canvas.restoreContext();
      //    WSI.requestId = undefined;
      //  }
      //}, false);
      //canvas.addEventListener("webglcontextrestored", function () {
      //  // WSI.Init();
      //  WSI.Init();
      //  WSI.Animate();
      //}, false);

      _Manager.Controls = new WSIControlsManager();
      //
      // Update camera and controls form initial mode first-person
      if (WSIData.InitialCameraMode == EN_CAMERA_MODE.CM_FIRST_PERSON) {
        _Manager.Cameras.CurrentMode = EN_CAMERA_MODE.CM_FIRST_PERSON;
        _Manager.Controls.SetCurrentMode(EN_CONTROLS_MODE.CM_FIRST_PERSON);
        // LOD Update
        WSIData.LODDistance = 180;
        WSIData.ShowPlayers = true;
        // Update GUI
        //$("input[name=radioLayoutCameraMode][value=2]").attr('checked', 'checked');
      }
      //

      //// Controls
      //WSILogger.Write(" - Creating Controls...");
      //this.controls = new THREE.OrbitControls(this.camera, this.renderer.domElement);
      //this.controls.maxPolarAngle = Math.PI / 2;
      //this.controls.minDistance = 10;
      //this.controls.maxDistance = 450;
      //this.controls.noZoom = false;
      //this.controls.zoomSpeed = -0.1;
      //this.controls.autoRadius = false;
      ////this.controls.addEventListener('change', this.Render);

      //WSILogger.Write(" - Creating Cenit-controls...");
      //this.Cenitcontrols = new THREE.OrbitControls(this.Cenitcam, this.renderer.domElement);
      //this.Cenitcontrols.noRotate = true;
      //this.Cenitcontrols.minDistance = 10;
      //this.Cenitcontrols.maxDistance = 450;
      //this.Cenitcontrols.noZoom = false;
      //this.controls.autoRadius = false;
      ////this.Cenitcontrols.addEventListener('change', this.Render);

      // Stats control
      if (WSIData.ShowFPS) {
        WSILogger.Write(" - Creating Stats object...");
        this.stats = new Stats();
        this.stats.domElement.style.position = 'absolute';
        this.stats.domElement.style.bottom = '0px';
        this.stats.domElement.style.zIndex = 500;
        this.container.appendChild(this.stats.domElement);
      }

      // Events & Callbacks
      WSILogger.Write(" - Initializing event handlers and callbacks...");
      // On resize window, viewport must be updated
      window.addEventListener('resize', function () {
        var _camera = _Manager.Cameras.CurrentCamera();

        if (_camera instanceof THREE.OrthographicCamera) {
          _camera.left = window.innerWidth / -2;
          _camera.right = window.innerWidth / 2;
          _camera.top = window.innerHeight / 2;
          _camera.bottom = window.innerHeight / -2;
        } else if (_camera instanceof THREE.PerspectiveCamera) {
          _camera.aspect = (window.innerWidth - WSI.RightPanelDistance) / window.innerHeight;
        }

        _camera.updateProjectionMatrix();
        WSI.renderer.setSize(window.innerWidth - WSI.RightPanelDistance, window.innerHeight);
      }, false);

      WSI.renderer.domElement.addEventListener('dblclick', function () {
        event.preventDefault();
        if (WSIData.SelectedObject != null) {
          GotoTerminal(WSIData.SelectedObject, null, true);
        }
      });

      WSI.renderer.domElement.addEventListener('mousedown', function () {
        m_mouse_actions_mousedown = true;
        WSI.clicker = { x: event.x, y: event.y };
        event.preventDefault();
      });

      WSI.renderer.domElement.addEventListener('mousemove', function () {
        m_mouse_actions_mousemove_before_mousedown = m_mouse_actions_mousedown;

        event.preventDefault();
      });

      WSI.renderer.domElement.addEventListener('mouseup', function () {
        if (!m_mouse_actions_mousemove_before_mousedown) {
          m_mouse_actions_mousemove_before_mousedown = false;
          m_mouse_actions_mousedown = false;

          WSI.RendererClick();

        } else {
          m_mouse_actions_mousemove_before_mousedown = false;
          m_mouse_actions_mousedown = false;

          _Manager.RunnersMap.Update();

          if (m_update_terminals_pending) {
            UpdateTerminals();

            m_update_terminals_pending = false;

          }
        }

        event.preventDefault();
      });

      //// TODO: Firefox 'click' problem
      //WSI.renderer.domElement.addEventListener('click', function () {
      //  WSI.RendererClick();
      //  event.preventDefault();
      //}, false);

      //
      WSILogger.Write(" - Initializing Scene:");

      // Lighting
      WSILogger.Write("     - Creating lights...");

      var _light_level_x = 0x303030;
      var _light_level_z = 0x606060;

      this.AmbientLight = new THREE.AmbientLight(0x606060);
      this.LeftLight = new THREE.DirectionalLight(_light_level_x, 1);
      this.RightLight = new THREE.DirectionalLight(_light_level_x, 1);
      this.TopLight = new THREE.DirectionalLight(_light_level_z, 1);
      this.BottomLight = new THREE.DirectionalLight(_light_level_z, 1);

      this.UpLight = new THREE.DirectionalLight(0x505050, 1);

      this.LeftLight.position.set(-100, 0, 0).normalize();
      this.RightLight.position.set(100, 0, 0).normalize();
      this.TopLight.position.set(0, 0, -100).normalize();
      this.BottomLight.position.set(0, 0, 100).normalize();

      this.UpLight.position.set(0, 200, 0).normalize();

      this.scene.add(this.AmbientLight);
      this.scene.add(this.LeftLight);
      this.scene.add(this.RightLight);
      this.scene.add(this.TopLight);
      this.scene.add(this.BottomLight);
      this.scene.add(this.UpLight);

      _Manager.FPLight = new THREE.PointLight("#ffffff", 1.5, 40);
      this.scene.add(_Manager.FPLight);

      // DEBUG
      // WSILogger.Debug("WSI.Init()", "WSI", this);

      //
      this.PivotMesh.name = "Scene_Pivot_Mesh";

      // 
      WSILogger.Write("=========================== End Initialization routine.");

    } catch (err) {
      WSILogger.Log("WSI.Init()", err);
    }
  },

  // Animate: starts the main render/update loop
  Animate: function () {
    try {
      var _camera = _Manager.Cameras.CurrentCamera();
      //var _controls = _Manager.Controls.CurrentControls();

      // Time Control
      var _delta = WSI.Clock.getDelta();

      // Check for next frame
      WSI.requestId = requestAnimationFrame(WSI.Animate);

      // Update camera controls
      //_controls.update();
      _Manager.Controls.UpdateControls(_delta);

      // Update zoom slider
      //UpdateZoom();

      // RMS - Updatevisibility status
      WSIObjectUtils.UpdateCameraProjMatrix(_camera);

      //if (_delta < 0.1) {
      //  console.log("** Delta: " + _delta);
      //UpdateTerminals(_delta);
      //} else {
      //  console.log("Delta: " + _delta);
      //}

      // RMS - Animations
      TWEEN.update();

      // Call the Render
      WSI.Render();

      // Stats control update
      if (WSI.stats) { WSI.stats.update(); }

      //
    } catch (err) {
      WSILogger.Log("WSI.Animate()", err);
    }
  },

  // Main method of updating/drawing scene
  Render: function () {
    try {
      var _camera = _Manager.Cameras.CurrentCamera();

      if (_Manager.Cameras.CurrentMode == EN_CAMERA_MODE.CM_FIRST_PERSON) {
        _Manager.FPLight.visible = true;

        WSI.LeftLight.visible = false;
        WSI.RightLight.visible = false;
        WSI.TopLight.visible = false;
        WSI.BottomLight.visible = false;

        _Manager.FPLight.position.x = _camera.position.x;
        _Manager.FPLight.position.y = 10;
        _Manager.FPLight.position.z = _camera.position.z;

        if (_Manager.FPRoom) { _Manager.FPRoom.visible = true; }

        _Manager.Grid.visible = false;

        WSI.scene.fog = new THREE.FogExp2(0xffffff, 0.0025);

        for (var _monitor in _Manager.Monitors) {
          _Manager.Monitors[_monitor].visible = true;
        }

      } else {
        _Manager.FPLight.visible = false;

        WSI.LeftLight.visible = true;
        WSI.RightLight.visible = true;
        WSI.TopLight.visible = true;
        WSI.BottomLight.visible = true;

        if (_Manager.FPRoom) { _Manager.FPRoom.visible = false; }

        _Manager.Grid.visible = true;

        WSI.scene.fog = null;

        for (var _monitor in _Manager.Monitors) {
          _Manager.Monitors[_monitor].visible = false;
        }

      }

      //var _controls = _Manager.Controls.CurrentControls();

      //if (_camera.CanRotate) // ROTATE
      //{
      //  _controls.rotateLeft(WSI.RotateValue * Math.PI / 180);
      //}
      //if (_camera.CanPan) // PAN
      //{
      //  _controls.panLeft(WSI.PanValue);
      //}
      //if (_camera.CanZoom) // Forward/Backward);
      //{
      //  if (WSI.AdvanceValue > 0) {
      //    _controls.dollyIn(-1);
      //  } else {
      //    _controls.dollyOut(1);
      //  }
      //}

      //// LOD Update
      //WSI.scene.updateMatrixWorld();
      //WSI.scene.traverse(function (object) {

      //  if (object instanceof THREE.Mesh) {

      //    if (WSIData.Activity && object.userData.counter) {
      //      if (WSIData.Terminal_Status[object.userData.counter] != object.userData.old_color) {
      //        //WSIObjectUtils.ChangeLODMaterial(object, WSIData.Terminal_Status[object.userData.counter]);
      //        WSIObjectUtils.ChangeFacesMaterial(object, WSIData.Terminal_Status[object.userData.counter]);
      //        object.userData.old_color = WSIData.Terminal_Status[object.userData.counter];
      //      }
      //    }
      //    if (!WSI.Cenital) {
      //      //                        object.update(_active_cam);
      //      //object.objects[0].object.visible = true;
      //      ////object.objects[1].object.visible = false;
      //      //object.objects[object.objects.length - 1].object.visible = false;
      //    } else {
      //      //object.objects[0].object.visible = false;
      //      ////object.objects[1].object.visible = false;
      //      ////object.objects[2].object.visible = false;
      //      //object.objects[object.objects.length - 1].object.visible = true;

      //    }
      //  }
      //});

      //if (WSIData.Activity) {
      //  _Manager.Labels.Refresh();
      //} else {
      //  WSI.scene.remove(_Manager.Labels.Pivot);
      //  _Manager.Labels.Pivot = new THREE.Object3D();
      //  WSI.scene.add(_Manager.Labels.Pivot);
      //}

      // Render
      WSI.renderer.render(WSI.scene, _camera);
    } catch (err) {
      WSILogger.Log("WSI.Render()", err);
    }

  },

  // Adds and object directly to the scene, creating another node on it
  AddToScene: function (geometry, materials, x, y, z) {
    try {
      var _mesh = new THREE.Mesh(geometry, new THREE.MeshFaceMaterial(materials));
      _mesh.position.set(x, y, z);
      WSI.scene.add(_mesh);
    } catch (err) {
      WSILogger.Log("WSI.AddToScene()", err);
    }

  },

  // Adds an object to another object called pivot that will be the main node of the scene
  AddToPivot: function (geometry, materials, x, y, z, scalex, scaley, scalez, orientation) {
    try {
      //var _mesh = new THREE.Mesh(geometry, new THREE.MeshFaceMaterial(materials));
      WSI.objectcounter++;

      var mats = [];

      mats.push(WSIData.Materials[0]);
      mats.push(WSIData.Materials[0]);
      mats.push(WSIData.Materials[0]);
      mats.push(WSIData.Materials[0]);
      mats.push(WSIData.Materials[2]);
      mats.push(WSIData.Materials[0]);
      //mats.id = 1000 + WSI.objectcounter;
      var geom = new THREE.BoxGeometry(2, 5, 2);

      var _mesh = new THREE.Mesh(geom, new THREE.MeshFaceMaterial(mats));
      _mesh.position.set(x, y + 2.5, z);
      _mesh.scale.set(scalex, scaley, scalez);
      _mesh.rotation.y = -(45 * orientation) * Math.PI / 180;

      _mesh.userData.counter = WSI.objectcounter;
      _mesh.userData.old_color = 0;
      _mesh.userData.type = "terminal";

      _mesh.name = "Terminal" + WSI.objectcounter;

      WSI.PivotMesh.add(_mesh);

      WSIData.Terminals.push(_mesh.name);

    } catch (err) {
      WSILogger.Log("WSI.AddToPivot()", err);
    }

  },

  // Adds an object to the main scene that contains level of detail information by distance
  AddLODToScene: function (geometries, materials, x, y, z) {
    try {
      var _new_lod = WSIObjectUtils.CreateLODObject(geometries, materials, x, y, z);
      _new_lod.name = "LodObject" + WSI.objectcounter;

      //WSIData.Terminals.push(_new_lod.name);

      this.scene.add(_new_lod);
      return _new_lod;
    } catch (err) {
      alert("WSI.AddLODToScene -> " + err.message);
      return null;
    }
  },

  // Adds an object to the main pivot object that contains level of detail information by distance
  AddLODToPivot: function (geometries, materials, x, y, z, scalex, scaley, scalez, orientation) {
    try {
      var _new_lod = WSIObjectUtils.CreateLODObject(geometries, materials, x, y, z, scalex, scaley, scalez, orientation);
      _new_lod.name = "LodObject" + WSI.objectcounter;

      _new_lod.userData = { objectType: "term", counter: WSI.objectcounter };

      //var _label = WSIObjectUtils.CreateObjectLabel(WSI.objectcounter, _new_lod.name, x, y, z, 8, 0xffffff, 0xffffff);
      //var _label = WSIObjectUtils.CreateObjectLabel2(WSI.objectcounter, _new_lod.name, x, y, z, 8, 0xff0000);
      //            var _label2 = WSIObjectUtils.CreateObjectLabel3(WSI.objectcounter, Math.floor(Math.random() * 100) + "$", x, y, z, 20, "#000000", _new_lod.name);
      //            _new_lod.add(_label2);

      //var _label = WSIObjectUtils.CreateObjectStats(_new_lod.position, orientation);
      //            var _label = WSIObjectUtils.CreateObjectStats2(_new_lod.position, orientation, _new_lod.name);
      //            _new_lod.add(_label);


      _new_lod.position.x = x + 0.5;
      _new_lod.position.y = y;
      _new_lod.position.z = z + 0.5;
      _new_lod.scale.set(scalex, scaley, scalez);
      _new_lod.rotation.y = -(45 * orientation) * Math.PI / 180;


      //this.PivotMesh.add(_label);

      WSIData.Terminals.push(_new_lod.name);

      this.PivotMesh.add(_new_lod);
      return _new_lod;
    } catch (err) {
      WSILogger.Log("WSI.AddLODToPivot", err);
      return null;
    }

  },

  // 
  RotateCam: function (Angle) {
    try {
      if (Angle) {
        WSI.RotateValue = Angle;
        WSI.Rotate = 1;
      } else {
        WSI.RotateValue = 0;
        WSI.Rotate = 0;
      }
    } catch (err) {
      WSILogger.Log("WSI.RotateCam", err);
      return null;
    }
  },

  // 
  PanCam: function (Distance) {
    try {
      if (Distance) {
        WSI.PanValue = Distance;
        WSI.Pan = 1;
      } else {
        WSI.PanValue = 0;
        WSI.Pan = 0;
      }
    } catch (err) {
      WSILogger.Log("WSI.PanCam", err);
      return null;
    }
  },

  // 
  //AdvanceCam: function (Distance) {
  //  try {
  //    if (Distance) {
  //      WSI.AdvanceValue = Distance;
  //      WSI.Advance = 1;
  //    } else {
  //      WSI.AdvanceValue = 0;
  //      WSI.Advance = 0;
  //    }
  //  } catch (err) {
  //    WSILogger.Log("WSI.AdvanceCam", err);
  //    return null;
  //  }
  //},

  //
  //SetControls: function () {
  //  try {
  //    document.getElementById('pnlNavigation').style.visibility = document.getElementById('chkSetControls').checked ? 'visible' : 'hidden';
  //  } catch (err) {
  //    WSILogger.Log("WSI.SetControls", err);
  //    return null;
  //  }
  //},

  //
  //CamToHome: function () {
  //  var _camera = _Manager.Cameras.CurrentCamera();

  //  switch (_Manager.Cameras.CurrentMode) {
  //    case EN_CAMERA_MODE.CM_PERSPECTIVE:
  //      _camera.position.set(0, -100, -500);
  //      _camera.lookAt(0, 0, 0);
  //      break;
  //    case EN_CAMERA_MODE.CM_CENIT:
  //      _camera.position.set(0, -500, 0);
  //      _camera.lookAt(0, 0, 0);
  //      break;
  //    case EN_CAMERA_MODE.CM_FIRST_PERSON:
  //      // ?
  //      break;
  //  }

  //},

  //
  //SetCenital: function () {
  //  try {
  //    var _value = document.getElementById('chkSetCenital').checked ? 1 : 0;
  //    if (_value != WSI.Cenital) {
  //      if (_value === 0) {
  //        WSILogger.Write(" * Switching to orbital mode");
  //        if (WSI.CamOldPosition != null) {
  //          //WSI.camera.position.set(WSI.CamOldPosition.x, WSI.CamOldPosition.y, WSI.CamOldPosition.z);
  //          //WSI.camera.lookAt(0, 0, 0);
  //        }
  //      } else {
  //        WSILogger.Write(" * Switching to cenital mode");
  //        //                    WSI.CamOldPosition = WSI.camera.position.clone();
  //        //                    WSI.camera.position.set(0, 400, 0);
  //        //                    WSI.camera.lookAt(0, 0, 0);
  //        //                    WSI.camera.updateMatrix();
  //        //                    WSI.camera.quaternion.setFromRotationMatrix(new THREE.Matrix4(
  //        //                        0, 0, 0, 0,
  //        //                        0, -1, 0, 0,
  //        //                        0, 0, 0, 0,
  //        //                        0, -1, 0, 0)
  //        //                    );



  //      }
  //      WSI.Cenital = _value;
  //    }
  //  } catch (err) {
  //    WSILogger.Log("WSI.SetControls", err);
  //    return null;
  //  }
  //},

  //
  //SetLabels: function () {
  //  try {
  //    var _value = document.getElementById('chkShowLabels').checked;
  //    _Manager.Labels.SetVisible(_value);
  //  } catch (err) {
  //    WSILogger.Log("WSI.SetLabels", err);
  //    return null;
  //  }
  //},

  //
  //SetStats: function () {
  //  try {
  //    var _value = document.getElementById('chkShowStats').checked;
  //    if (_value) {


  //      for (var _i = WSIData.Terminals.length - 1; _i >= 0; _i--) {
  //        var _obj = WSI.scene.getObjectByName(WSIData.Terminals[_i], true);
  //        if (_obj) {
  //          var _stats = WSIObjectUtils.CreateObjectStats2(_obj.position, 0, _obj.name);
  //          _obj.add(_stats);
  //        }
  //      }

  //    } else {

  //      for (var _i = WSIData.Terminal_Stats.length - 1; _i >= 0; _i--) {
  //        var _obj = WSI.scene.getObjectByName(WSIData.Terminal_Stats[_i], true);
  //        if (_obj) {
  //          _obj.parent.remove(_obj);
  //        }
  //        delete WSIData.Terminal_Stats[_i];
  //      }

  //    }
  //  } catch (err) {
  //    WSILogger.Log("WSI.SetStats", err);
  //    return null;
  //  }
  //},

  //
  SetIslands: function (Value) {
    try {
      if (Value) {
        for (var _i = 0; _i < WSIData.Islands.length; _i++) {
          WSI.scene.add(WSIObjectUtils.CreateIsland2(WSIData.Islands[_i].name, WSIData.Islands[_i].position, WSIData.Islands[_i].width, WSIData.Islands[_i].height, 0x0000ff));
        }

      } else {

        for (var _i = WSIData.Islands.length - 1; _i >= 0; _i--) {
          var _obj = WSI.scene.getObjectByName(WSIData.Islands[_i].name + "_island", true);
          if (_obj) {
            _obj.parent.remove(_obj);
          }
        }

      }
    } catch (err) {
      WSILogger.Log("WSI.SetIslands", err);
      return null;
    }
  },

  //
  SetAreas: function (Value) {
    try {
      if (Value) {
        for (var _i = 0; _i < WSIData.Areas.length; _i++) {
          WSI.scene.add(WSIObjectUtils.CreateArea2(WSIData.Areas[_i].name, WSIData.Areas[_i].position, WSIData.Areas[_i].width, WSIData.Areas[_i].height, 0x0000ff));
        }

      } else {

        for (var _i = WSIData.Areas.length - 1; _i >= 0; _i--) {
          var _obj = WSI.scene.getObjectByName(WSIData.Areas[_i].name + "_area", true);
          if (_obj) {
            _obj.parent.remove(_obj);
          }
        }

      }
    } catch (err) {
      WSILogger.Log("WSI.SetAreas", err);
      return null;
    }
  },

  //
  RendererClick: function () {
    try {
      if (WSI.clicker.x != event.x || WSI.clicker.y != event.y) {
        return;
      }

      

      var _camera = _Manager.Cameras.CurrentCamera();
      m_runner_id_selected = null;

      WSI.mouse2D.x = 2 * (event.clientX / window.innerWidth) - 1;
      WSI.mouse2D.y = 1 - 2 * (event.clientY / window.innerHeight);

      WSI.Raycaster.setFromCamera(WSI.mouse2D.clone(), _camera);

      var showRunners = $(document).find('[onclick*="SetRunnersmap();"]').prop("checked")
      
        //var _arr = [];
        //_arr[0] = _Manager.RunnersMap.HeatMap3D;
        var _arr = _Manager.RunnersMap.markers;

        intersects = WSI.Raycaster.intersectObjects(_arr, _camera.clickOnChildren);

        if (intersects.length > 0 && showRunners) {

            var _raycaster_x, _raycaster_y;
            var _runner_id;

            if (intersects[0].distance < 4) {
            
                _raycaster_x = intersects[0].point.x;
                _raycaster_y = intersects[0].point.z;

                console.log("_r_x: " + _raycaster_x);
                console.log("_r_y: " + _raycaster_y); 

                _runner_id = _Manager.RunnersMap.GetClosestRunners(_raycaster_x, _raycaster_y);

                m_runner_id_selected = _runner_id;
            }

            if (_runner_id != null && _runner_id.count > 0) {

                m_runner_id_selected = _runner_id.length - 1;
                _Manager.RunnersMap.RunnerSelected = _runner_id.length - 1

                if (_runner_id.count == 1) {

                  // Design integration
              
                  OnRunnerClick(m_runner_id_selected, true);

              
                  OpenInfoPanel();

                } else {

                    //OpenRunnerDialog(_runner_id);
                    DialogRunner(_runner_id);

                }

          } else {
            WSIData.SelectedObject = null;
            m_terminal_id_selected = null;
            _Manager.HideSelector();
            _Manager.RunnersMap.RunnerSelected = null;
          }
        } else {
          if(m_current_section!=80)
            var intersects = WSI.Raycaster.intersectObjects(_Manager.Terminals.Pivot.children, _camera.clickOnChildren);

            if (intersects.length > 0) {
                if (WSIData.Activity || _Manager.Terminals.Items[intersects[0].object.name].HasError()) {
                    WSIData.SelectedObject = _Manager.Terminals.Items[intersects[0].object.name].Properties.lo_id;
                    OnTerminalClick(WSIData.SelectedObject, "#" + CONST_ID_TERMINAL + WSIData.SelectedObject, true);
                }
                var _pos = _Manager.Terminals.Items[intersects[0].object.name].Mesh.position;
                _Manager.ShowSelector(_pos.x, _pos.y + 4, _pos.z, _Manager.Terminals.Items[intersects[0].object.name].Mesh);
                $('#spaSelectedTerminal').closest("div").prev("h3").click();

                // Design integration
                m_terminal_id_selected = WSIData.SelectedObject;
                OpenInfoPanel();
            } else {

                WSIData.SelectedObject = null;
                m_terminal_id_selected = null;
                _Manager.HideSelector();
                _Manager.RunnersMap.RunnerSelected = null;

            }


          
        }
     
    } catch (error) {
      WSILogger.Write("RendererClick() " + error);
    }
  }
};

