﻿// GLOBALS

var _Manager = new WSIManager();

///////////////////////////////////////////////////////////////////////////

var _red = 0xff0000;
var _green = 0x00ff00;
var _yellow = 0xffff00;

///////////////////////////////////////////////////////////////////////////

var m_alarm_space = 0.2;

///////////////////////////////////////////////////////////////////////////

var m_terminal_id_selected = null;
var m_terminal_id_selected_old = null;
var m_account_id_selected = null;
var m_account_id_selected_old = null;
var m_runner_id_selected = null;

///////////////////////////////////////////////////////////////////////////

var m_pyramid_angles = 0;
var _marker_type = 1;

var m_monitor_notify_alarms = false;
var m_section_first_time = false;

var m_monitor_enabled_alarms_collection = [];

function UpdateGamingTables() {
  if (WSIData.SelectedObject != null && _Manager.GamingTables.Items[WSIData.SelectedObject]) {
    // OnGamingTableClick
  }

  // Gaming Tables

}
function GetAlarmsToSound() {
  var _legends_list = [];

  $("#div_legend_container_42002 .chkAlarmsSound.chkAlarmsSoundEnabled").each(function () {
    var _l_id = $(this).data("soundlegendid")
    _legends_list[_l_id] = _l_id;
  });

  $("#div_legend_container_41001 .chkAlarmsSound.chkAlarmsSoundEnabled").each(function () {
    var _l_id = $(this).data("soundlegendid")
    _legends_list[_l_id] = _l_id;
  });

  return _legends_list;
}
function UpdateTerminals(Delta) {


  var _legend_values = undefined;
  var _alarms_machine = 0;
  var _alarms_player = 0;
  var _alarms_selected;
  var _custom_alarms_count = 0;
  var _alarm_sound = false;

  var _next_res = '';

  if (m_mouse_actions_mousemove_before_mousedown || m_changing_floor) {
    m_update_terminals_pending = true;

    return;
  }

  m_notify_alarm = false;

  _Manager.UpdateBackPlane();

  var _activeFilter = m_current_filter[m_current_section];
  var _camera = _Manager.Cameras.CurrentCamera();

  if (m_alarms_filter_sounds_changed) {
    m_monitor_alarms_blink = GetAlarmsToSound();
    _Configuration.AlarmsToSound = m_monitor_alarms_blink;
    m_config_has_changed = true;
    SaveConfigurationDB(false);
    m_alarms_filter_sounds_changed = false;
  }

  _Manager.HeatMap.ClearData();

  // Terminals
  for (_object_id in _Manager.Terminals.Items) {
    //SL 1/11 - RETIRED TERMINALS DOESN'T HAVE MESH CONFIGURED
    if (_Manager.Terminals.Items[_object_id].Mesh != null) {
      _legend_values = null;

      // Check if its in frustum
      //if (WSIObjectUtils.UpdateVisibility(_camera, _Manager.Terminals.Items[_object_id].Mesh)) {

      // Update alarms count
      _alarms_machine += _Manager.Terminals.Items[_object_id].Properties.alarms_machine_count;
      _alarms_player += _Manager.Terminals.Items[_object_id].Properties.alarms_player_count;

      // Check distance for LOD
      var _distance = WSIObjectUtils.Distance(_camera.position, _Manager.Terminals.Items[_object_id].Mesh.position);

      //Before, it used LODDistance to transform machines to cubes when the distance was far.
      if (+$("input[type='radio'][name=radioLayoutCameraMode]:checked").val() == EN_CAMERA_MODE.CM_FIRST_PERSON) {
        _Manager.Terminals.Items[_object_id].Mesh.material.visible = false;
        _Manager.Terminals.Items[_object_id].Mesh.children[0].visible = true;
      } else {

        _Manager.Terminals.Items[_object_id].Mesh.material.visible = true;
        _Manager.Terminals.Items[_object_id].Mesh.children[0].visible = false;
      }

      // Monitor texture change
      if (Delta) {
        if (_Manager.Terminals.Items[_object_id].MonitorMaterialIndex != -1) {
          _Manager.Terminals.Items[_object_id].MonitorDisplayTime += Delta * 1000;
          if (_Manager.Terminals.Items[_object_id].MonitorDisplayTime > _Manager.Terminals.Items[_object_id].MonitorDisplayDuration) {
            _next_res = _Manager.Terminals.Items[_object_id].MonitorNextResource;
            _Manager.Terminals.Items[_object_id].MonitorNextResource = _Manager.Terminals.Items[_object_id].Mesh.children[0].material.materials[_Manager.Terminals.Items[_object_id].MonitorMaterialIndex].map.name;
            _Manager.Terminals.Items[_object_id].Mesh.children[0].material.materials[_Manager.Terminals.Items[_object_id].MonitorMaterialIndex].map = _Manager.Resources.Items[_next_res].object;
            _Manager.Terminals.Items[_object_id].Mesh.children[0].material.materials[_Manager.Terminals.Items[_object_id].MonitorMaterialIndex].map.name = _next_res;
            _Manager.Terminals.Items[_object_id].Mesh.children[0].material.materials[_Manager.Terminals.Items[_object_id].MonitorMaterialIndex].needsUpdate = true;

            _Manager.Terminals.Items[_object_id].MonitorDisplayTime = 0;
          }
        }
      }


      // RMS - Resource Load
      if (_Manager.Resources.Loaded) {
        //_Manager.Terminals.Items[_object_id].ChangeMaterialColor("mat_color_1", "white");
        _Manager.Terminals.Items[_object_id].ChangeMaterialColor("mat_color_1");
      }

      switch (m_current_section.toString()) {
        case Enum.EN_SECTION_ID.ALARMS:
        case Enum.EN_SECTION_ID.ALARMS_PLAYERS:
        case Enum.EN_SECTION_ID.ALARMS_MACHINES:
          if (_activeFilter && m_section_alarms_all != true) {
            _legend_values = _Legends.Check(_activeFilter, _Manager.Terminals.Items[_object_id].Properties);
          } else if (m_section_alarms_all) {
            _legend_values = _Legends.CheckAlarmsByPriority(_activeFilter, _Manager.Terminals.Items[_object_id].Properties);
          }

          if (_legend_values != null) {
            _Manager.Terminals.Items[_object_id].ChangeMaterialColor("mat_color_1", _legend_values.color);
          }

          break;

        case Enum.EN_SECTION_ID.MONITOR:

          if (_activeFilter) {
            _legend_values = _Legends.CheckCustomFilter(_activeFilter, _Manager.Terminals.Items[_object_id].Properties);
            if (_legend_values != null) {
              _Manager.Terminals.Items[_object_id].ChangeMaterialColor("mat_color_1", _legend_values.color);
            }
          }

          break;

        case Enum.EN_SECTION_ID.PLAYERS:
          if (m_filter_mode_enabled) {
            if (m_filter_list_selected.length > 0 && m_apply_filter) {
              _legend_values = _Legends.CheckCustomFilter(m_filter_list_selected, "black", _Manager.Terminals.Items[_object_id].Properties);

              if (_legend_values != null && $("#div_dialog_background_filter_mode").css("display") == "none") {
                _Manager.Terminals.Items[_object_id].ChangeMaterialColor("mat_color_1", _legend_values.color);
              }
            }
          } else {
            if (_activeFilter) {
              _legend_values = _Legends.Check(_activeFilter, _Manager.Terminals.Items[_object_id].Properties);
              if (_legend_values != null) {
                _Manager.Terminals.Items[_object_id].ChangeMaterialColor("mat_color_1", _legend_values.color);
              }
            }
          }
          break;

        case Enum.EN_SECTION_ID.MACHINES:
          if (m_filter_mode_enabled) {
            if (m_filter_list_selected.length > 0 && m_apply_filter) {
              _legend_values = _Legends.CheckCustomFilter(m_filter_list_selected, "black", _Manager.Terminals.Items[_object_id].Properties);

              if (_legend_values != null && $("#div_dialog_background_filter_mode").css("display") == "none") {
                _Manager.Terminals.Items[_object_id].ChangeMaterialColor("mat_color_1", _legend_values.color);
              }
            }
          } else {
            if (_activeFilter) {
              _legend_values = _Legends.Check(_activeFilter, _Manager.Terminals.Items[_object_id].Properties);
              if (_legend_values != null) {
                _Manager.Terminals.Items[_object_id].ChangeMaterialColor("mat_color_1", _legend_values.color);
              }
            }
          }

          break;
        default:
          if (_activeFilter) {
            _legend_values = _Legends.Check(_activeFilter, _Manager.Terminals.Items[_object_id].Properties);
            if (_legend_values != null) {
              _Manager.Terminals.Items[_object_id].ChangeMaterialColor("mat_color_1", _legend_values.color);
            }
          }
          break;
      }

      // Set Terminal color
      if (_legend_values != null) {
        _Manager.Terminals.Items[_object_id].CurrentColor = _legend_values.color;
        _Manager.Terminals.Items[_object_id].CurrentValue = _legend_values.value;
        _Manager.Terminals.Items[_object_id].CurrentProperty = _legend_values.prop;
      } else {
        _Manager.Terminals.Items[_object_id].CurrentColor = "#fff";
        _Manager.Terminals.Items[_object_id].CurrentValue = 0;
        _Manager.Terminals.Items[_object_id].CurrentProperty = undefined;
      }

      //Return bool if the alarm must to sound.
      if (!_alarm_sound && m_monitor_alarms_blink.length > 0) {
        _alarm_sound = _Legends.CheckAlarmsSound(m_monitor_alarms_blink, _Manager.Terminals.Items[_object_id].Properties);
      }

      // RMS - View Player
      WSIObjectUtils.UpdatePlayer(_Manager.Terminals.Items[_object_id]);

      if (_Manager.HeatMap.CanUpdate()) {
        // Update Heatmap
        _Manager.HeatMap.AddDataByTerminal(_Manager.Terminals.Items[_object_id]);
      }
    }
  }

  if (_Configuration.ShowHeatMap) {
    _Manager.HeatMap.Update();
  }

  if ((_alarms_player + _alarms_machine) > 0) {
    $("#monitorAlarmsNotification").show();
  } else {
    $("#monitorAlarmsNotification").hide();
  }

  if (_alarm_sound && _Configuration.SoundEnabled)
  {
    $("#sound_alarm")[0].load();
    $("#sound_alarm")[0].play();
  } else {
    $("#sound_alarm")[0].pause();
    if ($("#sound_alarm")[0].readyState > 0) {
      $("#sound_alarm")[0].currentTime = 0;
    }
  }

  m_section_first_time = false;
}

function CheckAlarmsSection(AlarmsList, TerminalID) {
  if (AlarmsList.length != 0) {
    _legend_values = _Legends.CheckAlarms(_Manager.Terminals.Items[TerminalID].Properties, AlarmsList);
    if (_legend_values != null) {
      _Manager.Terminals.Items[TerminalID].ChangeMaterialColor("mat_color_1", _legend_values.color);
    }
  }
}

// ---------------------------------------------------------------------

WSI.Init();

// ---------------------------------------------------------------------

function DrawScene() {
  // TODO: Draw Scene
  try {

    //        // F-Data
    //        for (var _i = 0; _i < WSI.objectcounter + 50; _i++) {
    //            WSIData.Terminal_Status[_i] = 0;
    //        }

    //

    WSIData.Areas.push({
      name: "A1", position: [-172, 16], width: 16, height: 18
    });

    WSIData.Areas.push({
      name: "A1", position: [-106, 0], width: 24, height: 23
    });

    WSIData.Areas.push({
      name: "A2", position: [-8, 0], width: 18, height: 14
    });

    WSIData.Areas.push({
      name: "A3", position: [68, -20], width: 12, height: 8
    });

    WSIData.Areas.push({
      name: "A4", position: [119, -35], width: 20, height: 28
    });

    WSIData.Areas.push({
      name: "A5", position: [69, 16], width: 12, height: 15
    });

    WSIData.Areas.push({
      name: "A6", position: [69, 80], width: 28, height: 3
    });

    //

    // ISLA 205 (CJ)
    WSIData.Islands.push({
      name: "1", position: [-86, 2], width: 4, height: 1
    });

    // ISLA 218 (BEM)
    WSIData.Islands.push({
      name: "2", position: [-5, 2], width: 4, height: 1
    });

    // ISLA 222 (BEL)
    WSIData.Islands.push({
      name: "3", position: [20, 2], width: 4, height: 1
    });

    // ISLA 201 (CJ)
    WSIData.Islands.push({
      name: "4", position: [-88, 21], width: 2, height: 2
    });

    // ISLA 206 (WMS BB2)
    WSIData.Islands.push({
      name: "5", position: [-68, 21], width: 2, height: 2
    });

    // ISLA 206 (WMS XD)
    WSIData.Islands.push({
      name: "6", position: [-46, 21], width: 2, height: 2
    });

    // ISLA 210 (BEM)
    WSIData.Islands.push({
      name: "7", position: [-26, 21], width: 2, height: 2
    });

    // ISLA 214 (FBM)
    WSIData.Islands.push({
      name: "8", position: [-1, 21], width: 2, height: 2
    });

    // ISLA 221 (MET)
    WSIData.Islands.push({
      name: "9", position: [22, 21], width: 2, height: 2
    });

    // ISLA 207
    WSIData.Islands.push({
      name: "10", position: [-68, 38], width: 2, height: 4
    });

    // ISLA 211
    WSIData.Islands.push({
      name: "11", position: [-48, 38], width: 2, height: 4
    });

    // ISLA 215
    WSIData.Islands.push({
      name: "11", position: [-24, 38], width: 2, height: 4
    });

    // ISLA 203
    WSIData.Islands.push({
      name: "12", position: [-89, 62], width: 2, height: 3
    });

    // ISLA 216 (IGT G23)
    WSIData.Islands.push({
      name: "13", position: [-68, 62], width: 2, height: 3
    });

    // ISLA 216 (IGTB G23)
    WSIData.Islands.push({
      name: "14", position: [-48, 62], width: 2, height: 3
    });

    // ISLA 212
    WSIData.Islands.push({
      name: "15", position: [-27, 62], width: 2, height: 3
    });

    // ISLA 217
    WSIData.Islands.push({
      name: "16", position: [-29, 84], width: 4, height: 1
    });

    // ISLA 213
    WSIData.Islands.push({
      name: "17", position: [-52, 84], width: 4, height: 1
    });

    // ISLA 204
    WSIData.Islands.push({
      name: "18", position: [-71, 84], width: 3, height: 1
    });

    // ISLA 200
    WSIData.Islands.push({
      name: "19", position: [-90, 84], width: 3, height: 1
    });

    // ISLA 200
    WSIData.Islands.push({
      name: "20", position: [-105, 84], width: 2, height: 1
    });

    // ISLA 223
    WSIData.Islands.push({
      name: "21", position: [36, 36], width: 4, height: 4
    });

    // ISLA 120
    WSIData.Islands.push({
      name: "22", position: [101, 18], width: 4, height: 1
    });

    // ISLA 116
    WSIData.Islands.push({
      name: "23", position: [122, -6], width: 1, height: 4
    });

    // ISLA 116
    WSIData.Islands.push({
      name: "24", position: [122, -28], width: 1, height: 4
    });

    // ISLA 109
    WSIData.Islands.push({
      name: "25", position: [146, -6], width: 2, height: 4
    });

    // ISLA 112
    WSIData.Islands.push({
      name: "25", position: [168, -6], width: 2, height: 4
    });

    // ISLA 110
    WSIData.Islands.push({
      name: "26", position: [146, 17], width: 2, height: 4
    });

    // ISLA 106
    WSIData.Islands.push({
      name: "27", position: [169, 19], width: 2, height: 4
    });

    // ISLA 111
    WSIData.Islands.push({
      name: "28", position: [146, 41], width: 2, height: 3
    });

    // ISLA 104
    WSIData.Islands.push({
      name: "29", position: [169, 41], width: 2, height: 3
    });

    // ISLA 121
    WSIData.Islands.push({
      name: "30", position: [101, 41], width: 2, height: 3
    });

    // ISLA 117
    WSIData.Islands.push({
      name: "31", position: [120, 41], width: 2, height: 3
    });

    // ISLA 105
    WSIData.Islands.push({
      name: "32", position: [146, 62], width: 2, height: 3
    });

    // ISLA 103
    WSIData.Islands.push({
      name: "33", position: [169, 62], width: 2, height: 3
    });

    // ISLA 122
    WSIData.Islands.push({
      name: "34", position: [101, 62], width: 2, height: 3
    });

    // ISLA 118
    WSIData.Islands.push({
      name: "35", position: [120, 62], width: 2, height: 3
    });

    // ISLA 118
    WSIData.Islands.push({
      name: "36", position: [80, 43], width: 2, height: 3
    });

    // ISLA 119
    WSIData.Islands.push({
      name: "37", position: [97, 84], width: 8, height: 1
    });

    // ISLA 113
    WSIData.Islands.push({
      name: "38", position: [138, 84], width: 4, height: 1
    });

    // ISLA 102
    WSIData.Islands.push({
      name: "39", position: [161, 84], width: 4, height: 1
    });

    // ISLA 114
    WSIData.Islands.push({
      name: "40", position: [136, -32], width: 3, height: 1
    });

    // ISLA 108
    WSIData.Islands.push({
      name: "41", position: [152, -32], width: 3, height: 1
    });

    // ISLA 107
    WSIData.Islands.push({
      name: "42", position: [168, -32], width: 3, height: 1
    });

    // ISLA 100
    WSIData.Islands.push({
      name: "43", position: [191, -10], width: 1, height: 3
    });

    // ISLA 101
    WSIData.Islands.push({
      name: "44", position: [190, 7], width: 1, height: 3
    });

    // ISLA 126
    WSIData.Islands.push({
      name: "45", position: [75, 78], width: 4, height: 3
    });


    //
    //chkShowAreas
    SetAreas($("#chkShowAreas"), true);
    //chkShowIslands
    SetIslands($("#chkShowIslands"), true);
    //

    // Box
    _Manager.FPRoom = WSIObjectUtils.BoxSkyBox("room1");
    _Manager.FPRoom.visible = false;

    // FOG
    //WSI.scene.fog = new THREE.FogExp2(0xffffff, 0.0025);
    //WSI.scene.fog = new THREE.FogExp2(0xffffff, 0.0025);

    // Monitors
    _Manager.Monitors = [];
    _Manager.Monitors.push(WSIObjectUtils.CreateScreen(8, 4, "monitor 1"));
    _Manager.Monitors[0].position.set((_Manager.BackPlane.userData.width / 2) - 0.2, 10, 0);
    _Manager.Monitors[0].rotation.y = -90 * Math.PI / 180;

    _Manager.Monitors.push(WSIObjectUtils.CreateScreen(10, 6, "monitor 1"));
    _Manager.Monitors[1].position.set(-(_Manager.BackPlane.userData.width / 2) + 0.2, 10, 0);
    _Manager.Monitors[1].rotation.y = 90 * Math.PI / 180;

    _Manager.Monitors.push(WSIObjectUtils.CreateScreen(10, 6, "monitor 1"));
    _Manager.Monitors[2].position.set(0, 10, (_Manager.BackPlane.userData.height / 2) - 0.2);
    _Manager.Monitors[2].rotation.y = 180 * Math.PI / 180;

    _Manager.Monitors.push(WSIObjectUtils.CreateScreen(8, 4, "monitor 1"));
    _Manager.Monitors[3].position.set(0, 10, -(_Manager.BackPlane.userData.height / 2) + 0.2);
    _Manager.Monitors[3].rotation.y = 0 * Math.PI / 180;

    //_Manager.Monitors.push(WSIObjectUtils.CreateScreen(8, 4, "monitor 1"));
    //_Manager.Monitors[3].position.set(0, 8, -(_Manager.BackPlane.userData.height / 2) + 0.2);
    //_Manager.Monitors[3].rotation.y = 180 * Math.PI / 180;

    for (var _monitor in _Manager.Monitors) {
      _Manager.Monitors[_monitor].visible = false;
      WSI.scene.add(_Manager.Monitors[_monitor]);
    }


  } catch (err) {
    WSILogger.Log("DrawScene", err);
  }

  // Add the main object to the scene as node
  WSI.scene.add(_Manager.Terminals.Pivot);
  WSI.scene.add(_Manager.GamingTables.Pivot);

  _Manager.Labels.SetVisible(false);

  WSI.scene.add(_Manager.Labels.Pivot);

  // ---------------------------------------------------------------------

  WSI.Animate();

  // ---------------------------------------------------------------------

  LoadEnd();
}

//
function SceneLoadResources() {

  //$("#sp_loading_step").text('Loading resources...');

  WSILogger.Write("Loading Resources...");

  PageMethods.LoadResources(_Configuration.CurrentFloorId, onSceneLoadResourcesComplete, onSceneLoadResourcesError);

}

//
function onSceneLoadResourcesComplete(Result) {

  if (Result) {

    if (_Manager.Resources == null) {
      _Manager.Resources = new WSIEngine.ResourceList();
    }

    try {

      WSILogger.Debug("onSceneLoadResourcesComplete", "DATA_SIZE", Result.length);

      var _resources = JSON.parse(Result);

      for (var _r in _resources.rows) {
        var _ritem = _resources.rows[_r];
        var _nitem = new WSIEngine.ResourceListItem(_ritem[1], _ritem[3], $.base64.decode(_ritem[4]));
        _nitem.Options = $.base64.decode(_ritem[5]);
        if (_nitem.Options != "") {
          _nitem.Options = JSON.parse(_nitem.Options);
        }

        _Manager.Resources.AddItem(_nitem);
        _Manager.Resources.SetResourceId(_ritem[3], _ritem[0]);
      }

      _Manager.Resources.Load(function () {
        //$("#sp_loading_step").text('Resource load complete...');

        // Continue
        BuildScene();
      },
      function (ResourceItem, Loaded, Total) {
        // $("#sp_loading_step").text('Loading resources ' + Loaded + ' of ' + Total + '...');
        WSILogger.Debug("Resource loaded", ResourceItem.Key, Result.length);
      });

    } catch (e) {
      //console.log(e.message);
      WSILogger.Log("onSceneLoadResourcesComplete", e);
    }

  }

  // Update dashboard on floor change
  if (m_floor_changed) {
    m_changing_floor = false;
  }

}

function onSceneLoadResourcesError(Result) {
  GotoError("SceneLoadResources");
}

// Clear resources and scenes
function ResetScene() {
  for (var i = WSI.scene.children.length - 1; i >= 0; i--) {
    var _obj = WSI.scene.children[i];
    if (_obj.type.indexOf("Light") > 0) {
      continue;
    }
    WSI.scene.remove(_obj);
  }
}

// Before construct scene, load resources
function InitializeScene() {
  // Load Resources
  SceneLoadResources();
}

// Scene construction
function BuildScene() {

  // $("#sp_loading_step").text('Building floor...');

  // Init other stuff
  WSIData.Counters.push(0); // Terminals
  WSIData.Counters.push(0); // Tables
  WSIData.Counters.push(0); // Roulettes

  // Default materials
  _Manager.Materials.AddMaterial(WSIConstants.get('Material Type', 'Lambert'), 'mat_color_1', {
    color: 0x707070, wireframe: false, shading: THREE.FlatShading, overdraw: false
  });

  // Load Geometry
  _Manager.Geometries.Load();

  // Grid plane
  _Manager.CreateGrid(PixelsToUnits(_CurrentFloor3.dimensions.x) * 4, PixelsToUnits(_CurrentFloor3.dimensions.y) * 4);
  WSI.scene.add(_Manager.Grid);

  // Background Plane
  _Manager.CreateBackPlane(_CurrentFloor3.image_map, PixelsToUnits(_CurrentFloor3.dimensions.x) * 4, PixelsToUnits(_CurrentFloor3.dimensions.y) * 4);
  _Manager.UpdateBackPlane();
  WSI.scene.add(_Manager.BackPlane);

  // Build Terminals & Labels
  _Manager.Terminals.Build(TerminalBuilder, LabelBuilder);

  //// Build Beacons
  //_Manager.Beacons.Build();

  // Apply configuration values
  ApplyDefaultValues();

  // Scene
  DrawScene();
}

function LoadEnd() {
  // Initialize UI
  _UI.InitFloorControls();

  WSILogger.Write("LoadLayout complete");
  m_floor_loaded = true;
  m_floor_changed = false;
  m_changing_floor = false;

  UpdateTerminals();
  _Manager.UpdateBackPlane();
  if (m_loading_dialog) {
    m_loading_dialog.dialog("close");
  }

  $("#loading_floor").hide();


  if (_LayoutMode == 1) { // 2D
    _UI.SetMenuActiveSection(this, m_current_section);
  }
  
  setTimeout(function () { }, 2000);


    // Remove and execute all items in the array
  while (m_function_queue.length > 0) {
      (m_function_queue.shift())();
  }

}

//////////////////////////////////////////////////////////////////////////////////////////

function RestoreTerminals() {
  for (_terminal_id in _Manager.Terminals.Items) {
    _Manager.Terminals.Items[_terminal_id].ChangeMaterialColor("mat_color_1", 0x707070);
  }
  // Clear labels
  _Manager.Labels.SetVisible(false);

}

function GamingTableSeatBuilder(GamingTable, GamingTableSeat, degrees, scale) {

  if (!GamingTableSeat.Properties.lme_geometry || !GamingTableSeat.Properties.lma_material) {

    _model = WSIObjectUtils.ProcessModel(_gt_seat_1);
    _model.materials[0].shading = THREE.FlatShading;
    _model.materials[0].color.setHex(Math.random() * 0xffffff);
    GamingTableSeat.Mesh = new THREE.Mesh(_model.geometry, new THREE.MeshFaceMaterial(_model.materials));
    GamingTableSeat.Mesh.scale.set(scale * 0.75, 1.7, scale * 0.75);
    //Terminal.Mesh.geometry.applyMatrix(new THREE.Matrix4().makeTranslation(-0.8, 0, -0.8));
    //GamingTableSeat.Mesh.position.set(0, 1.5, 0);

  }

  if (GamingTableSeat.Mesh != null) {


    GamingTableSeat.Mesh.name = GamingTableSeat.Properties.lo_id;
    //GamingTableSeat.Mesh
    GamingTable.Mesh.add(GamingTableSeat.Mesh);

    GamingTableSeat.Mesh.geometry.applyMatrix(new THREE.Matrix4().makeTranslation(scale * 2, 0, 0));
    //GamingTableSeat.Mesh.position.set(-3, 0, 0);

    GamingTableSeat.Mesh.rotation.y += (-90 - degrees) * Math.PI / 180;

  } else {
    WSILogger.Write('GamingTableSeatBuilder.Build() -> Cannot create mesh of GamingTableSeat ' + GamingTableSeatBuilder.Properties.lo_id);
  }


}

function GamingTableBuilder(GamingTable) {

  if (!GamingTable.Properties.lme_geometry || !GamingTable.Properties.lma_material) {

    switch (GamingTable.Properties.lme_sub_type) {
      case 1:
        _model = WSIObjectUtils.ProcessModel(_gaming_table_1);
        _model.materials[0].shading = THREE.FlatShading;
        GamingTable.Mesh = new THREE.Object3D();
        _gt_mesh = new THREE.Mesh(_model.geometry, new THREE.MeshFaceMaterial(_model.materials));
        _gt_mesh.scale.set(2, 1.7, 1.7);

        break;
      case 2:
        _model = WSIObjectUtils.ProcessModel(_roulette_1);
        _model.materials[0].shading = THREE.FlatShading;
        GamingTable.Mesh = new THREE.Object3D();
        _gt_mesh = new THREE.Mesh(_model.geometry, new THREE.MeshFaceMaterial(_model.materials));
        _gt_mesh.scale.set(1.5, 1.5, 1.5);

        break;
    }

  }

  if (_gt_mesh != null) {
    GamingTable.Mesh.position.set(GamingTable.Properties.lop_x, GamingTable.Properties.lop_y, GamingTable.Properties.lop_z);
    GamingTable.Mesh.rotation.y = -(45 * GamingTable.Properties.lop_orientation) * Math.PI / 180;
    //Terminal.Mesh.scale.set(1.8, 1, 1.8);
    //Terminal.Mesh.scale.set(1.3, 1.3, 1.3);

    _gt_mesh.name = GamingTable.Properties.lo_id;

    GamingTable.Mesh.add(_gt_mesh);

    _Manager.GamingTables.Pivot.add(GamingTable.Mesh);

    //callbackLabelBuilder(GamingTable);

    switch (GamingTable.Properties.lme_sub_type) {
      case 1:
        var _seats = GamingTable.Seats.Items.length;
        var _deg = (180 / (_seats - 1)) / 2;

        for (_i = 0; _i < _seats; _i++) {
          GamingTableSeatBuilder(GamingTable, GamingTable.Seats.Items[_i], (_deg + (_deg * 2 * _i)) - _deg, _gt_mesh.scale.x);
        }
        break;

      default:
        break;
    }

    return GamingTable.Mesh;
  } else {
    WSILogger.Write('WSIGamingTable.Build() -> Cannot create mesh of GamingTable ' + GamingTable.Properties.lo_external_id);
  }

}

function TerminalBuilder(Terminal, callbackLabelBuilder) {
  //SL 1/11 - RETIRED TERMINALS NOT DRAW IN FLOOR
  if (Terminal.Properties.status != 2) {
    var _mesh_type = Terminal.Properties.lo_mesh_id,
      _resource = _Manager.Resources.GetResourceById(_mesh_type),
      _detailed,
      _low_poly,
      _female_res, _female,
      _male_res, _male;

    // Get object location to capture de offset
    var _loc = _CurrentFloor3.GetByField(EN_OBJECT_LOCATION_FIELDS.lo_external_id, Terminal.Properties.lo_external_id);

    // Adding model
    if (_resource) {
      _mesh_model = _resource.object;
      _mesh_model.material = _mesh_model.material.clone();

      // Adding low-poly-model
      _lowpoly = new THREE.Mesh(new THREE.BoxGeometry(3.9, 4, 3.9), new THREE.MeshLambertMaterial({
        name: "mat_color_1", color: 0xa0a0a0, opacity: 1, transparent: false, shading: THREE.FlatShading
      }));
      //_lowpoly.material.emissive = WSIObjectUtils.DownColor(_lowpoly.material.color, 0.5);
      _lowpoly.visible = true;

      // Postion and rotation from floor design
      //_lowpoly.rotation.y = -(45 * Terminal.Properties.lop_orientation) * Math.PI / 180;
      _lowpoly.rotation.y = -(Terminal.Properties.lop_orientation) * (Math.PI / 180);

      //_lowpoly.rotation.y = Terminal.Properties.lop_orientation * Math.PI / 180;

      var _offx = parseFloat((_loc[EN_OBJECT_LOCATION_FIELDS.lol_offset_x]).replace(",", ".")) / 4,
          _offz = parseFloat((_loc[EN_OBJECT_LOCATION_FIELDS.lol_offset_y]).replace(",", ".")) / 4;

      _lowpoly.position.set(Terminal.Properties.lop_x + _offx + (3.9 / 2), 2, Terminal.Properties.lop_z + _offz + (3.9 / 2));
      _lowpoly.scale.set(1, 1, 1);

      _lowpoly.name = Terminal.Properties.lo_id;
      Terminal.Mesh = _lowpoly;

    } else {
      // No mesh... default model

    }

    if (Terminal.Mesh != null) {

      // Detailed model
      _detailed = new THREE.Mesh(_mesh_model.geometry, new THREE.MeshFaceMaterial(_mesh_model.material.materials));
      _detailed.visible = false;
      _detailed.position.set(0, Terminal.Properties.lop_y - 2, 0);

      if (_resource.Options != "") {
        _detailed.geometry.computeBoundingBox();
        //_detailed.scale.set(2, 2, 2);
        _detailed.rotation.set(_resource.Options.initialRotation.x * Math.PI / 180, _resource.Options.initialRotation.y * Math.PI / 180, _resource.Options.initialRotation.z * Math.PI / 180);
        _detailed.position.set(_resource.Options.initialPosition.x, _resource.Options.initialPosition.y - 2, _resource.Options.initialPosition.z);
        _detailed.scale.set(_resource.Options.initialScale.x, _resource.Options.initialScale.y, _resource.Options.initialScale.z);

      }
      _detailed.name = Terminal.Properties.lo_id;
      Terminal.Mesh.add(_detailed);

      // Setting monitor and body index material
      for (var _mat in _detailed.material.materials) {
        if (_detailed.material.materials[_mat].name == "a_body") {
          Terminal.BodyMaterialIndex = _mat;
        } else if (_detailed.material.materials[_mat].name == "a_monitor") {
          Terminal.MonitorMaterialIndex = _mat
        }
      }

      // Adding Male Model
      _male_res = _Manager.Resources.Items[WSIData.MalePlayerChar];
      if (_male_res) {
        _male = new THREE.Mesh(_male_res.object.geometry, _male_res.object.material);
        _male.visible = false;
        Terminal.Mesh.add(_male);
        if (_male_res.Options != "") {
          _male.geometry.computeBoundingBox();
          _male.rotation.set(_male_res.Options.initialRotation.x * Math.PI / 180, _male_res.Options.initialRotation.y * Math.PI / 180, _male_res.Options.initialRotation.z * Math.PI / 180);
          _male.position.set(_male_res.Options.initialPosition.x, _male_res.Options.initialPosition.y - 2, _male_res.Options.initialPosition.z);
          _male.scale.set(_male_res.Options.initialScale.x, _male_res.Options.initialScale.y, _male_res.Options.initialScale.z);
        }
        _male.name = Terminal.Properties.lo_id;
      }

      // Adding Female model
      _female_res = _Manager.Resources.Items[WSIData.FemalePlayerChar];
      if (_female_res) {
        //_female = new THREE.Mesh(_female_res.object.geometry, new THREE.MeshLambertMaterial({ color: 0xff0099 }));
        _female = new THREE.Mesh(_female_res.object.geometry, _female_res.object.material);
        _female.visible = false;
        if (_female_res.Options != "") {
          _female.geometry.computeBoundingBox();
          _female.rotation.set(_female_res.Options.initialRotation.x * Math.PI / 180, _female_res.Options.initialRotation.y * Math.PI / 180, _female_res.Options.initialRotation.z * Math.PI / 180);
          _female.position.set(_female_res.Options.initialPosition.x, _female_res.Options.initialPosition.y - 2, _female_res.Options.initialPosition.z);
          _female.scale.set(_female_res.Options.initialScale.x, _female_res.Options.initialScale.y, _female_res.Options.initialScale.z);
        }
        _female.name = Terminal.Properties.lo_id;
        Terminal.Mesh.add(_female);
      }

      //Terminal.Mesh.name = Terminal.Properties.lo_id;

      _Manager.Terminals.Pivot.add(Terminal.Mesh);
      WSIData.MaxMachines++;

      callbackLabelBuilder(Terminal);

      return Terminal.Mesh;
  }

  return null;
  } else {
    WSILogger.Write('WSITerminal.Build() -> Cannot create mesh of terminal ' + Terminal.Properties.lo_external_id);
  }

}

//////////////////////////////////////////////////////////////////////////////////////////

function UpdateLabels() {
  //_Manager.Labels.Pivot = new THREE.Object3D();
  for (_terminal_id in _Manager.Terminals.Items) {
    LabelBuilder(_Manager.Terminals.Items[_terminal_id]);
    //LabelBuilder2(_Manager.Terminals.Items[_terminal_id]);
  }
}

function PyramidBuilder(Terminal, Label) {

  try {


    // Check Old Pyramid
    var _old = _Manager.Labels.Pivot.getObjectByName(Terminal.Properties.lo_id + "_label_pyramid", true);
    if (_old) {
      _Manager.Labels.Pivot.remove(_old);
      WSI.scene.remove(_old);
    }

    var _old_alarm = _Manager.Labels.Pivot.getObjectByName(Terminal.Properties.lo_id + "_label_alarm", true);
    if (_old_alarm) {
      _Manager.Labels.Pivot.remove(_old_alarm);
      WSI.scene.remove(_old_alarm);
    }

    // Only in alarms section
    if (m_current_section == "40" && (Label.BackColor != "white")) {

      // Tipo de marcador
      if (_marker_type == 1) {

        var _model = WSIObjectUtils.ProcessModel(_marker_body);
        var _alarm = WSIObjectUtils.ProcessModel(_marker_alarm);

        _model.geometry.computeFaceNormals();
        _alarm.geometry.computeFaceNormals();
        _model.geometry.computeVertexNormals();
        _alarm.geometry.computeVertexNormals();
        _model.materials[0].shading = THREE.FlatShading;
        _model.materials[0].specularCoef = 80;
        _alarm.materials[0].shading = THREE.FlatShading;
        _alarm.materials[0].specularCoef = 80;

        var _mesh = new THREE.Mesh(_model.geometry, new THREE.MeshFaceMaterial(_model.materials));

        var _meshHelper = new THREE.EdgesHelper(_mesh, 0x000000);
        _meshHelper.material.linewidth = 1;
        _mesh.add(_meshHelper);

        var _alarm_mat = null;
        var _mesh_alarm = null;

        var _img = document.getElementById("alarm_standard_icon");
        var amap = new THREE.Texture(_img);
        amap.needsUpdate = true;

        //      var _new_svg = document.createElement("canvas");
        //      _new_svg.width = 40;
        //      _new_svg.height = 40;
        //canvg(_new_svg, '<svg style="fill:rgb(0,0,0);"><path d="M161,181.2c-4.9-8.6-1.8-18.4,6.1-23.3c3.1-1.8,6.7-2.4,9.8-2.4c5.5,0.6,10.4,3.7,13.5,8.6l22.6,38.6l-30,17.1L161,181.2zM451.7,181.2c4.9-8.6,1.8-19-6.7-23.3c-8.6-4.9-19-1.2-23.9,6.1l0,0l-22,38.6l30,17.1L451.7,181.2z M323.2,137.7c0-9.8-8-17.1-17.1-17.1c-9.8,0-17.7,7.3-17.7,17.1v44.7h34.9V137.7z M546.6,258.9c-3.1-4.9-8-8-13.5-8.6c-3.1,0-6.7,0-9.8,1.8l-38.6,22.6l17.1,30l39.2-22.6C548.4,277.9,551.5,267.5,546.6,258.9z M511.7,622.4h-13.5c1.2-1.8,1.8-4.3,1.8-6.7v-18.4c0-8.6-6.7-15.3-15.3-15.3h-11.6H140.8h-12.2c-8.6,0-15.3,6.7-15.3,15.3v18.4c0,2.4,0.6,4.9,1.8,6.7h-13.5c-8.6,0-15.3,6.7-15.3,15.3v18.4c0,8.6,6.7,15.3,15.3,15.3h410.7c8.6,0,15.3-6.7,15.3-15.3v-18.4C527,629.8,520.3,622.4,511.7,622.4z M467,565.5H145.7V404v-2.4V396c0-88.7,72.2-160.3,161-160.3l0,0c88.7,0,161,71.6,161,160.3v4.9v2.4v162.2H467z M282.8,424.8c0-45.9-37.3-83.2-83.2-83.2v167.1C245.5,508.6,282.8,471.3,282.8,424.8z M412.5,341.5c-45.9,0-83.2,37.3-83.2,83.2s37.3,83.2,83.2,83.2V341.5z M128,275.4l-39.2-22.6c-1.8-1.2-4.3-1.8-6.1-1.8c-6.7-0.6-13.5,2.4-17.1,8.6c-4.9,8.6-2.4,18.4,6.1,23.3l39.2,22.6L128,275.4z"/></svg>');

        //canvg(_new_svg, 'img/alert11.svg');

        //var _amap_data = _new_svg.toDataURL("image/png");

        //var _amap_t = THREE.ImageUtils.loadTexture('img/alarm.png');

        //var _amap_t = THREE.ImageUtils.loadTexture(_amap_data);
        //var amap = new THREE.Texture(_amap_t);

        //var amap = new THREE.Texture(_amap_t);
        //amap.needsUpdate = true;
        //      amap.transparent = true;

        WSIObjectUtils.ChangeMaterialColor(_mesh, "_mat_body", Label.BackColor);
        _mesh_alarm = new THREE.Mesh(_Manager.Geometries.CreateAlarmSignalItem2(),
                           new THREE.MeshPhongMaterial({ map: amap, emissive: 0xffffff, combine: THREE.AddOperation, transparent: true })
                          );

        _mesh.name = Terminal.Properties.lo_id + "_label_pyramid";
        _mesh_alarm.name = Terminal.Properties.lo_id + "_label_alarm";

        _mesh.position.x = Terminal.Mesh.position.x;
        //_mesh_alarm.position.x = 2;

        var _mesh_size = WSIObjectUtils.GetMeshSize(Terminal.Mesh);

        if (_mesh_size) {
          //
          _mesh.position.y = Terminal.Mesh.position.y + (_mesh_size.max.y + m_alarm_space);
          //
        } else {
          // Unabel to get alarm size
          if (WSIData.ModelTypes == 2) {
            _mesh.position.y = Terminal.Mesh.position.y + 8.4;
          } else {
            _mesh.position.y = Terminal.Mesh.position.y + 4.4;
          }
        }
        _mesh_alarm.position.y = 2.9;

        _mesh.position.z = Terminal.Mesh.position.z;
        _mesh_alarm.position.z = 0.5;

        _mesh.rotation.y = 90 * Math.PI / 180;

        _mesh.scale.set(2.0, 2.0, 2.0);

        _mesh.material.needsUpdate = true;
        _mesh_alarm.material.needsUpdate = true;

        _Manager.Labels.Pivot.add(_mesh);
        _mesh.add(_mesh_alarm);
      }
    }
  } catch (err) {
    var x = 1;
  }

}


///////////////////////////////////////////

///////////////////////////////////////////

function LabelBuilder(Terminal) {

  // Create Label
  var _new_label = new WSILabel();

  // Check if exists
  if (_Manager.Labels.Items[Terminal.Properties.lo_id]) {
    _new_label = _Manager.Labels.Items[Terminal.Properties.lo_id];
  }

  var _alarms_selected = m_alarms_to_print;

  // Update label values
  _new_label.Update(Terminal.Properties, 0, _alarms_selected);

  //if (_new_label.Icon) {
  if (_new_label.BackColor != "white") {
    _Manager.Labels.Items[Terminal.Properties.lo_id] = _new_label;
  }

  //PyramidBuilder(Terminal, _new_label);

  //  // Construct object
  //  if (_new_label.hasError || (_new_label.Text != "" && _new_label.Text != null)) {

  //    var _labelObject = null;
  //    var _img = null;

  //        if (_AlarmList.enabled) {
  //            if (_new_label.hasError) {
  //                _img = document.getElementById('ico_alarm');
  //            } else {
  //                if (Terminal.Properties.al_jackpot_flags == "1") {
  //                    _img = document.getElementById('ico_jackpot');
  //                } else {
  //                    if (Terminal.Properties.player_is_vip == "1") {
  //                        _img = document.getElementById('ico_vip');
  //                    } else {
  //                        // ??
  //                        _img = null;
  //                    }
  //                }
  //            }
  //        } // if (_AlarmList.enabled)

  //    if (_img != null) {

  //      // Text Texture    
  //      var canvas = document.createElement('canvas');
  //      var size = 40; // CHANGED
  //      canvas.width = size;
  //      canvas.height = size;
  //      var context = canvas.getContext('2d');

  //      context.drawImage(_img, 0, 0);

  /*
  context.fillStyle = _new_label.BackColor; // CHANGED
  context.fillRect(0, 0, size, size / 3);
  context.strokeStyle = "Black";
  context.strokeRect(0, 0, size - 1, size / 3 - 1);
  context.fillStyle = _new_label.TextColor; // CHANGED
  context.textAlign = 'center';
  context.font = '18px Arial Black';
  context.fillText(_new_label.Text, size / 2, size / 6);
  */

  //      var amap = new THREE.Texture(canvas);

  //      amap.needsUpdate = true;
  //      var mat = new THREE.SpriteMaterial({
  //        map: amap,
  //        transparent: true, // false
  //        useScreenCoordinates: false
  //      });

  //      // Label
  //      var _y_offset = (WSIData.ModelTypes == 2) ? 11 : 7;

  //      _labelObject = new THREE.Sprite(mat);
  //      _labelObject.position.x = Terminal.Properties.lop_x;
  //      _labelObject.position.y = Terminal.Properties.lop_y + _y_offset;
  //      _labelObject.position.z = Terminal.Properties.lop_z;
  //      _labelObject.scale.set(3, 3, 1); // CHANGED
  //      _labelObject.name = Terminal.Properties.lo_id + "_label";

  //      // Line
  //      Terminal.Mesh.geometry.computeBoundingBox();
  //      var _line_geo = new THREE.Geometry();
  //      var _v_1 = new THREE.Vector3();
  //      _v_1.x = _labelObject.position.x;
  //      _v_1.y = _labelObject.position.y - 0.8;
  //      _v_1.z = _labelObject.position.z;
  //      var _v_2 = new THREE.Vector3();
  //      _v_2.x = Terminal.Mesh.position.x;
  //      if (WSIData.ModelTypes == 2) {
  //        _v_2.y = Terminal.Mesh.position.y + (Terminal.Mesh.geometry.boundingBox.max.y * 1.7);
  //      } else {
  //        _v_2.y = Terminal.Mesh.position.y;
  //      }
  //      _v_2.z = Terminal.Mesh.position.z;
  //      _line_geo.vertices.push(_v_1);
  //      _line_geo.vertices.push(_v_2);
  //      var _line_mat = new THREE.LineBasicMaterial({ color: _new_label.BorderColor });
  //      var _lineObject = new THREE.Line(_line_geo, _line_mat);
  //      //_lineObject.position.y -= 0;
  //      _lineObject.name = Terminal.Properties.lo_id + "_line";

  //      // Check Visible
  //      if (!_new_label.hasError) {
  //        _labelObject.visible = (_new_svg != null);
  //        //_labelObject.visible = _Manager.Labels.Pivot.visible;
  //        _lineObject.visible = _labelObject.visible;
  //      }

  ////    }

  //    }


  //    // Check Old label
  //    var _old = _Manager.Labels.Pivot.getObjectByName(Terminal.Properties.lo_id + "_label", true);
  //    if (_old) {
  //      _Manager.Labels.Pivot.remove(_old);
  //      WSI.scene.remove(_old);
  //    }

  //    // Check Old label
  //    var _old_al = _Manager.Labels.Pivot.getObjectByName(Terminal.Properties.lo_id + "_label_alarm", true);
  //    if (_old_al) {
  //      _Manager.Labels.Pivot.remove(_old_al);
  //      WSI.scene.remove(_old_al);
  //    }

  //    // Check Old Pyramid
  //    var _old = _Manager.Labels.Pivot.getObjectByName(Terminal.Properties.lo_id + "_label_pyramid", true);
  //    if (_old) {
  //      _old.remove(_old_al);
  //      _Manager.Labels.Pivot.remove(_old);
  //      WSI.scene.remove(_old);
  //    }

  //    // Check Old line
  //    var _old = _Manager.Labels.Pivot.getObjectByName(Terminal.Properties.lo_id + "_line", true);
  //    if (_old) {
  //      _Manager.Labels.Pivot.remove(_old);
  //      WSI.scene.remove(_old);
  //    }

  //  if (_labelObject) {
  //    // Add
  //    //_Manager.Labels.Pivot.add(_labelObject);
  //    //_Manager.Labels.Pivot.add(_lineObject);

  //    _Manager.Labels.Items[Terminal.Properties.lo_id] = _new_label;

  //    PyramidBuilder(Terminal, _new_label, mat);

  //    //_Manager.Labels.Add(Terminal.Properties.lo_id, _new_label);

  //    // Keep Label
  //    //_Manager.Labels.Add(Terminal.Properties.lo_id, _new_label);
  //  } else {
  //    if (_Manager.Labels.Items[Terminal.Properties.lo_id]) {
  //      _Manager.Labels.Items[Terminal.Properties.lo_id] = undefined;
  //    }
  //  }
}

///////////////////////////////////////////

function GetHtmlInfoTerminal(e, ObjectId) {
  var _terminal;
  var _info;
  var _empty;

  var _player_name;
  var _player_first_name;
  var _player_last_name1;
  var _player_last_name2;
  var _player_age;
  var _player_level;
  var _player_gender;
  var _play_session_duration;
  var _play_session_total_played;
  var _play_session_played_count;

  _info = "";
  _empty = "---";

  try {
    _terminal = _Manager.Terminals.Items[ObjectId].Properties;

    if (_terminal.player_name != null) { _player_name = _terminal.player_name; } else { _player_name = _empty; }
    if (_terminal.player_first_name != null) { _player_first_name = _terminal.player_first_name; } else { _player_first_name = _empty; }
    if (_terminal.player_last_name1 != null) { _player_last_name1 = _terminal.player_last_name1; } else { _player_last_name1 = _empty; }
    if (_terminal.player_last_name2 != null) { _player_last_name2 = _terminal.player_last_name2; } else { _player_last_name2 = _empty; }
    if (_terminal.player_age != null) { _player_age = _terminal.player_age; } else {
      _player_age = _empty;
    }
    if (_terminal.player_level != null) { _player_level = _terminal.player_level; } else { _player_level = _empty; }
    if (_terminal.player_gender != null) { _player_gender = _terminal.player_gender; } else { _player_gender = _empty; }

    if (_terminal.play_session_duration != null) { _play_session_duration = _terminal.play_session_duration; } else { _play_session_duration = _empty; }
    if (_terminal.play_session_total_played != null) { _play_session_total_played = _terminal.play_session_total_played; } else { _play_session_total_played = _empty; }
    if (_terminal.play_session_played_count != null) { _play_session_played_count = _terminal.play_session_played_count; } else { _play_session_played_count = _empty; }

    _info = "<table width='100%' border='0px' margin='0px'>";
    _info += "<tr><td><b>Client info.</b></tr></td>";
    _info += "<tr><td>Nombre: " + _player_name + "</td></tr>";
    _info += "<tr><td>Edad: " + _player_age + "</td></tr>";
    _info += "<tr><td>Nivel: " + _player_level + "</td></tr>";
    _info += "<tr><td>Sexo: " + _player_gender + "</td></tr>";
    _info += "<tr><td><a href='http://www.google.es'>Documentos</a>" + "</td></tr>";
    _info += "<tr><td></tr></td>";
    _info += "<tr><td><b>Sesión de Juego</b></tr></td>";
    _info += "<tr><td>Duración (s): " + _play_session_duration + "</td></tr>";
    _info += "<tr><td>Jugado: " + "$" + _play_session_total_played + "</td></tr>";
    _info += "<tr><td>Nº partidas: " + _play_session_played_count + "</td></tr>";
    _info += "</table>";

  } catch (err) {
    _info = "";
    WSILogger.Log("WSI_Layout.js", err);
  }

  return _info;
} // GetHtmlInfoTerminal

function HiddenDiv(Div) {
  //$(Div).css("visibility","hidden");
  $(Div).removeClass("div_popup_terminal ui-widget-content ui-widget");
  $(Div).addClass("div_popup_terminal_hidden");
  $("#popup_Top10Best2").css("display", "none");
}

function OnTerminalClick(ObjectId, TerminalDiv, UpdateHistorical) {

  try {

    var _info;
    var _popup_div_principal;
    var _terminal;

    _terminal = _Manager.Terminals.GetTerminal(TerminalDiv.replace("#" + CONST_ID_TERMINAL, ""));

    //TerminalDiv = $(TerminalDiv);

    //if (_terminal && (_terminal.Properties.play_session_status == 2) || (_terminal.HasError() == true)) {
    if (_terminal) {
      UpdatePopupTerminal(null, _terminal);

      if (UpdateHistorical) {
        UpdatePopupTerminalHistorical(null, _terminal);
      }

      //UpdatePopupAlarms(_terminal);

      $("#popup_div_principal").removeClass("div_popup_terminal_hidden");
      $("#popup_div_principal").addClass("div_popup_terminal ui-widget-content ui-widget");

      $("#popup_Top10Best2").css("display", "");

      m_terminal_id_selected = $(TerminalDiv).attr("id");
    }

    m_terminal_id_selected_old = m_terminal_id_selected;
    m_terminal_id_selected = $(TerminalDiv).attr("id");

    m_account_id_selected_old = m_account_id_selected;
    //m_account_id_selected = $("#popup_account_id").text()
    m_account_id_selected = _terminal.Properties.player_account_id;

    ////////////////////////////////////////////////////////////////////

    var _div_img_icoSnapshot;
    //change class
    if (m_terminal_id_selected != m_terminal_id_selected_old) {
      _div_img_icoSnapshot = $("div[class*=icoSnapshot_undo]");
      $(_div_img_icoSnapshot).each(function () {

        if ($(this).attr('type') == 'terminal') {
          $(this).removeClass("icoSnapshot_undo");
          $(this).addClass("icoSnapshot");
        }
      });

    }
    if (m_account_id_selected != m_account_id_selected_old) {
      _div_img_icoSnapshot = $("div[class*=icoSnapshot_undo]");

      $(_div_img_icoSnapshot).each(function () {
        if ($(this).attr('type') == 'client') {
          $(this).removeClass("icoSnapshot_undo");
          $(this).addClass("icoSnapshot");
        }
      });
    }

    m_info_window_state = 'visible';

    //RestoreInfoWindow();
    ////////////////////////////////////////////////////////////////////

  } catch (_ex) {
    WSILogger.Log("OnTerminalClick()", _ex);
  }
}

function OnSelectedRunnerDialog(RunnerId) {

  $("#div_runner_selector").hide();

  OnRunnerClick(RunnerId);

  OpenInfoPanel();
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Ribbon

function SetMachineSize(Sender) {
  //
}

function SetStats(Sender) {
  WSI.SetStats();
}

function SetControls(Sender) {
  WSI.SetControls();
}

function SetLabels(Sender) {
  WSI.SetLabels();
}

//////////////////////////////////////////////////

function SetZoom(Value) {
  var _controls = _Manager.Controls.CurrentControls();
  _controls.radius = Value;
}

function UpdateZoom() {
  var _controls = _Manager.Controls.CurrentControls();
  $("#sldZoom").slider("value", 450 - _controls.radius);
}

//////////////////////////////////////////////////

function SetModelsType(Sender) {
  if (+$(Sender).val() != WSIData.ModelTypes) {
    WSIData.ModelTypes = $(Sender).val();
    _Manager.Terminals.Build(TerminalBuilder, LabelBuilder);
    if (_Manager.Selector) {
      _Manager.ShowSelector(_Manager.Selector.position.x, _Manager.Selector.position.y, _Manager.Selector.position.z);
    }
  }
  m_config_has_changed = true;
}

//////////////////////////////////////////////////

function ShowTerminalFromCustomer(sender) {
  GotoTerminal(sender, 1);
}

//////////////////////////////////////////////////

function ShowTerminalFromObjectId(sender) {
  GotoTerminal(sender, 3);
}

function GotoTerminal(sender, byType, showPopup) {

  if (!showPopup) {
    showPopup = false;
  }
  if (!byType) {
    byType = 3;
  }

  var _terminal = "";

  switch (byType) {
    case 1: // By Account
      _terminal = _Manager.Terminals.FindByAccountId(sender);
      WSILogger.Write('Going to terminal by Account: ' + _terminal);
      break;
    case 2: // By Terminal
      _terminal = _Manager.Terminals.FindTerminalId(_terminal);
      WSILogger.Write('Going to terminal by Terminal: ' + _terminal);
      break;
    case 3: // By ObjectId
      _terminal = _Manager.Terminals.Items[sender];
      WSILogger.Write('Going to terminal by ObjectId: ' + _terminal);
      break;
    default:
      if (typeof sender === "object") {
        _terminal = sender.target.id.replace("alarm_list_item_link_", "");
        _terminal = _terminal.substring(3);
        WSILogger.Write('Going to terminal by default: ' + _terminal);
      } else {
        // ???
        WSILogger.Write('Not going to terminal (Unknown)');
      }
      break;
  }

  //    // TODO: try to send id and kind of sender to parse id and get correct target
  //    if (typeof sender === "object") {
  //        _terminal = sender.target.id.replace("alarm_list_item_link_", "");
  //        _terminal = _terminal.substring(3);
  //    } else {
  //        _terminal = sender;
  //    }

  //    var _terminal_data = _Manager.Terminals.FindTerminal(_terminal);

  //    if (_terminal_data == null) {
  //        _terminal_data = _Manager.Terminals.GetTerminal(_terminal);
  //    }

  //if (_terminal != null) {

  //  //$("#ribbon-tab-header-0").click();
  //  //ChangeRibbonTab("tabs", 0, CONST_RIBBON_REALTIME);

  //  var _active_cam = WSI.Cenital ? WSI.Cenitcam : WSI.camera;
  //  var _active_controls = WSI.Cenital ? WSI.Cenitcontrols : WSI.controls;

  //  //        _active_cam.up = new THREE.Vector3(0, 1, 0);
  //  //        _active_cam.lookAt(new THREE.Vector3(_terminal_data.Properties.lop_x, _terminal_data.Properties.lop_y, _terminal_data.Properties.lop_z));

  //  _active_cam.position.set(_terminal.Properties.lop_x + 15, _active_cam.position.y, _terminal.Properties.lop_z);

  //  _active_controls.target.set(_terminal.Properties.lop_x, _terminal.Properties.lop_y, _terminal.Properties.lop_z)

  //  SetZoom(50);

  //  if (showPopup) {
  //    OnTerminalClick(_terminal.Properties.lo_id, "#" + CONST_ID_TERMINAL + _terminal.Properties.lo_id, false);
  //  } else {
  //    //HideInfoWindow();
  //    cerrarListaJugadores();
  //  }

  //  OnTerminalClick(_terminal.Properties.lo_id, "#" + CONST_ID_TERMINAL + _terminal.Properties.lo_id, false);

  //  _Manager.ShowSelector(_terminal.Properties.lop_x, _terminal.Properties.lop_y + 4, _terminal.Properties.lop_z, _terminal.Mesh);
  //  m_terminal_id_selected = _terminal.Properties.lo_id;
  //  OpenInfoPanel();
  //}
  ShowTerminalInfo(_terminal, byType, showPopup);
}

function ShowTerminalInfo(Terminal, byType, showPopup) {
  if (Terminal != null) {

    var _camera = _Manager.Cameras.CurrentCamera();
    var _controls = _Manager.Controls.CurrentControls();

    // RMS - Camera anim
    var _pos = {
      x: _camera.position.x, y: _camera.position.z
    };

    var _end = undefined,
        _onCompleteAnim = undefined;

    if (_Manager.Cameras.CurrentMode == EN_CAMERA_MODE.CM_FIRST_PERSON) {
      var _vector = new THREE.Vector3();
      _vector.setFromMatrixPosition(Terminal.Mesh.children[1].matrixWorld);
      _end = {
        x: _vector.x, y: _vector.z
      };
      _onCompleteAnim = function () {
        //_vector = new THREE.Vector3(Terminal.Mesh.position.x, _camera.position.y, Terminal.Mesh.position.z);
        //_camera.lookAt(_vector);
        //var _old_state = _controls.MouseControls.enabled;
        //_controls.MouseControls.enabled = true;
        //_controls.MouseControls.object.lookAt(_vector);
        //_controls.MouseControls.update();
        //_controls.MouseControls.enabled = _old_state;
        _controls.MouseControls.lon = WSIObjectUtils.GetDegrees(+Terminal.Properties.lop_orientation);
        console.log('lon: ' + _controls.MouseControls.lon);
      };
    } else {
      _end = {
        x: Terminal.Mesh.position.x, y: Terminal.Mesh.position.z
      };
    }

    WSIObjectUtils.Anim(_pos, _end, function () {
      _camera.position.set(_pos.x, _camera.position.y, _pos.y);
    }, _onCompleteAnim, 2500);

    //_active_cam.position.set(Terminal.Properties.lop_x + 15, _active_cam.position.y, Terminal.Properties.lop_z);

    if (_controls.target) {
      _controls.target.set(Terminal.Properties.lop_x, Terminal.Properties.lop_y, Terminal.Properties.lop_z);
      SetZoom(50);
    }

    if (showPopup) {
      OnTerminalClick(Terminal.Properties.lo_id, "#" + CONST_ID_TERMINAL + Terminal.Properties.lo_id, false);
    } else {
      //HideInfoWindow();
      cerrarListaJugadores();
    }

    OnTerminalClick(Terminal.Properties.lo_id, "#" + CONST_ID_TERMINAL + Terminal.Properties.lo_id, false);

    _Manager.ShowSelector(Terminal.Properties.lop_x + (3.9 / 2), Terminal.Properties.lop_y + 4, Terminal.Properties.lop_z + (3.9 / 2), Terminal.Mesh);
    m_terminal_id_selected = Terminal.Properties.lo_id;
    OpenInfoPanel();
  }

}

function LocateTerminal(object, context){

  if (_LayoutMode == 2) {
    //Unselect Terminals
    $($("#" + m_terminal_id_selected))
      .css('border', '1px solid black')
      .css('z-index', '2');

    if ($($("#" + m_terminal_id_selected)).attr("selected")) {
      $($("#" + m_terminal_id_selected))
      .css("top", ($("#" + m_terminal_id_selected).attr('initial_top')))
      .css("left", ($("#" + m_terminal_id_selected).attr('initial_left')))
      .removeAttr("selected");
    }
  }


    var _function;

    if (_Manager.Resources) {
        GotoTerminal(object, 3, true);
    }
    else {
        _function = m_function_wrapper(GotoTerminal, this, [object, 3, true]);
        m_function_queue.push(_function);
    }


}

function GotoRunner(Runner) {

  if (Runner != null) {

    var _camera = _Manager.Cameras.CurrentCamera();
    var _controls = _Manager.Controls.CurrentControls();

    // RMS - Camera anim
    var _pos = {
      x: _camera.position.x, y: _camera.position.z
    };

    var _end = undefined,
        _onCompleteAnim = undefined;

    if (_Manager.Cameras.CurrentMode == EN_CAMERA_MODE.CM_FIRST_PERSON) {

      //Checkear por que el runner es un id y no un objecto 
      var _vector = new THREE.Vector3();
      _vector.setFromMatrixPosition(Runner.Mesh.children[1].matrixWorld);
      _end = {
        x: _vector.x, y: _vector.z
      };
      _onCompleteAnim = function () {
        _controls.MouseControls.lon = WSIObjectUtils.GetDegrees(+Runner.Properties.lop_orientation);
        console.log('lon: ' + _controls.MouseControls.lon);
      };

    } else {

      _runner = _Manager.RunnersMap.RunnersById[Runner];
      if(_runner)
      _end = { 
        x: _runner.x,
        y: _runner.y
      };
      else 
        _end = {
          x: _pos.x,
          y: _pos.y
        }; 
    }

    WSIObjectUtils.Anim(_pos, _end, function () {
      _camera.position.set(_pos.x, _camera.position.y, _pos.y);
    }, _onCompleteAnim, 2500);

    if (_controls.target) {
      _controls.target.set(_end.x, _Manager.RunnersMap.runnersPositionY, _end.y);
      SetZoom(50);
    }

    //var _pos = _Manager.Runners.Items[intersects[0].object.lo_id].Mesh.position;
    //_Manager.ShowSelector(_pos.x, _pos.y + 4, _pos.z, _Manager.Runners.Items[intersects[0].object.lo_id].Mesh);

    //_Manager.ShowSelector(_end.x, _end.y + 4, Runner.Mesh.position.z, Runner.Mesh);
    m_runner_id_selected = Runner;

    OnRunnerClick(Runner);

    OpenInfoPanel();
  }


}

function LocateRunner(context, object) {
  if (m_floor_loaded)
    GotoRunner(object)
  else
    var _function;
    _function = m_function_wrapper(GotoRunner, context, [object]);
    m_function_queue.push(_function);
      
}

//////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////