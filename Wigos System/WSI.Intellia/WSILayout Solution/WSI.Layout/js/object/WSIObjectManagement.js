﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: filename
// 
//   DESCRIPTION: Description.
// 
//        AUTHOR: Author
// 
// CREATION DATE: Date
// 
//------------------------------------------------------------------------------

/*
 Requires:
  - jQuery
  - WSIObjectTypes.js
 */

function EditorObjectManagement(Element, Elements) {

  if (Element == null) return;

  if (!Elements) {
    // Single selected

    var _svg_id = Element.id,
        _item = _CurrentChanges3.GetByField(EN_OBJECT_LOCATION_FIELDS.svg_id, _svg_id, EN_OBJECT_STATUS.deleted);

    if (!_item) {
      _item = _CurrentFloor3.GetByField(EN_OBJECT_LOCATION_FIELDS.svg_id, _svg_id, EN_OBJECT_STATUS.deleted);
    }

    if (_item) {
      var _type = _item[EN_OBJECT_LOCATION_FIELDS.lo_type];

      switch (+_type) {
        case EN_LAYOUT_OBJECT_TYPES.LOT_TERMINAL:
          $("#machine_panel_machine_id").text(_item[EN_OBJECT_LOCATION_FIELDS.lo_external_id]);
          $("#machine_panel_machine_name").text(m_available_objects.Items[_item[EN_OBJECT_LOCATION_FIELDS.lo_external_id]].name);
          var _loc = _CurrentFloor3.GetLocationById(_item[EN_OBJECT_LOCATION_FIELDS.lol_location_id]);
          $("#machine_panel_machine_location").text(_loc[EN_LOCATION_FIELDS.loc_gridId]);
          $("#machine_panel_available_meshes").val(_item[EN_OBJECT_LOCATION_FIELDS.lo_mesh_id]);
          $("#machine_panel").show();
          break;
        case EN_LAYOUT_OBJECT_TYPES.LOT_BEACON:
          $("#beacon_panel_machine_id").text(_item[EN_OBJECT_LOCATION_FIELDS.lo_external_id]);
          var _loc = _CurrentFloor3.GetLocationById(_item[EN_OBJECT_LOCATION_FIELDS.lol_location_id]);
          $("#beacon_panel_machine_location").text(_loc[EN_LOCATION_FIELDS.loc_gridId]);
          $("#beacon_panel").show();
          break;
        default:
          break;
      }
    }

  } else {

    $("#selected_panel").hide();

    // Multiple selection
    // TODO: Check same type object, at least in not positions type to allow unbind multiple items
    //var _last_type = $(Elements[0]).attr("lo_type");
    //var _len = Elements.length;
    //var _next_type = undefined;
    //var _i = 1;
    //do {
    //  _next_type = $(Elements[_i]).attr("lo_type");
    //  _i++;
    //} while (_i < _len && (_next_type == _last_type));
    //if (_next_type == _last_type) {
    //  switch (_last_type) {
    //    case 1:
    //      $("#machine_panel_machine_id").text("[Multiple]");
    //      $("#machine_panel").show();
    //      break;
    //  }
    //}
  }
  
}

function EditorBindObject(Target, Category, Identifier) {
  // Bind Object
  if (Target) {
    if (Category != null && Identifier != null) {
      var _attribs = WSIGetAttributesFromElement(Target);
      var _location = WSIGetLocationFromAttributes(_attribs);

      // Update attributes on for new element
      _attribs.attr.lo_type = Category;
           
      switch (+Category) {
        case 0:
          _attribs.attr.alt = "position";
          _attribs.attr.d = m_position_path;
          _attribs.attr.stroke = m_position_stroke;
          _attribs.attr["stroke-width"] = m_position_stroke_width;
          _attribs.attr.fill = m_position_fill;
          break;
        case 1:
          _attribs.attr.alt = "machine";
          _attribs.attr.d = m_machine_path;
          _attribs.attr.stroke = m_machine_stroke;
          _attribs.attr["stroke-width"] = m_machine_stroke_width;
          _attribs.attr.fill = m_machine_fill;
          break;
      }

      _attribs.attr.d = _attribs.attr.d.replace("%X%", _location.x);
      _attribs.attr.d = _attribs.attr.d.replace("%Y%", _location.y);

      //_attribs.attr.binded = Identifier;
      _attribs.attr.current_lo_externalId = Identifier;
      // Mark item in available objects list as binded/unbinded
      
      // Delete old element
      svgCanvas.deleteSelectedElements();

      // Create new element
      var _elem = svgCanvas.addSvgElementFromJson(_attribs);
      
      // Mark Element as unavailable
      if (m_available_objects.Items[_attribs.attr.current_lo_externalId]) {
        m_available_objects.Items[_attribs.attr.current_lo_externalId].id = _attribs.attr.current_lo_externalId;
      }

      if (_elem) {
        svgCanvas.setMode("select");
        svgCanvas.clearSelection();
        //svgCanvas.addToSelection([_elem], false);
      }

      return (_elem) ? true : false;

    } 
  } 

  return false;
}

function EditorUnbindObject(Target) {
  // Unbind Object
  if (Target) {
    EditorBindObject(Target, 0, -1);
  }
  

}