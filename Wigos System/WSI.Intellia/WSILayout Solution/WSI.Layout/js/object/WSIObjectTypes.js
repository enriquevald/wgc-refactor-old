﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: filename
// 
//   DESCRIPTION: Description.
// 
//        AUTHOR: Author
// 
// CREATION DATE: Date
// 
//------------------------------------------------------------------------------

/*
 Requires:

 */

var m_position_path = "M%X%,%Y%m-10,-10h20v20h-20z",
    m_position_stroke = "#000",
    m_position_stroke_width = 1,
    m_position_fill = "#999";

var //m_machine_path = "M%X%,%Y%m-10,-10h20v15l-5,5h-10l-5,-5z",
    //m_machine_path = "M%X%,%Y%m-10,-10h20v20h-2m-2,0h-2m-2,0h-2m-2,0h-2m-2,0h-2m-2,0v-20z",
    m_machine_path = "M%X%,%Y%m-10,-10h20v20h-1v-2h-2v2h-2v-2h-2v2h-2v-2h-2v2h-2v-2h-2v2h-2v-2h-2v2h-1v-20z",
    m_machine_stroke = "#000",
    m_machine_stroke_width = 1,
    m_machine_fill = "#f00",
    m_beacon_path = "M%X%,%Y%m-10,-10h20v20h-20z",
    m_beacon_stroke = "#88f",
    m_beacon_stroke_width = 1,
    m_beacon_fill = "#00f";

function CheckNull(Value, DefaultValue) {
  return (Value == null) ? DefaultValue : Value;
}

//function WSIObjectTypes(ObjectType) {

//  var _result = {};
//  _result.element = "";
//  _result.curStyles = false;
//  _result.attr = {};

//  _result.attr.loc_id = '';
//  _result.attr.loc_x = '';
//  _result.attr.loc_y = '';
//  _result.attr.loc_creation = '';
//  _result.attr.loc_type = '';
//  _result.attr.loc_floorId = '';
//  _result.attr.loc_gridId = '';
//  _result.attr.lo_id = '';
//  _result.attr.lo_externalId = '';
//  _result.attr.current_lo_externalId = '';
//  _result.attr.lo_type = '';
//  _result.attr.lo_parentId = '';
//  _result.attr.lo_meshId = '';
//  _result.attr.lo_creation = '';
//  _result.attr.lo_updated = '';
//  _result.attr.lol_object_id = '';
//  _result.attr.lol_location_id = '';
//  _result.attr.lol_date = '';
//  _result.attr.lol_orientation = '';
//  _result.attr.lol_areaId = '';
//  _result.attr.lol_bankId = '';
//  _result.attr.lol_bankLocation = '';
//  _result.attr.lol_currentLocation = '';

//  _result.attr.lo_type = ObjectType;

//  switch (+ObjectType) {

//    case 0: // Position
//      _result.element = "path";
//      _result.attr.alt = "position";
//      _result.attr.d = m_position_path;
//      _result.attr.stroke = m_position_stroke;
//      _result.attr["stroke-width"] = m_position_stroke_width;
//      _result.attr.fill = m_position_fill;
//      _result.attr.id = "object_" + svgCanvas.getNextId();
//      break;

//    case 1: // Machine
//      _result.element = "path";
//      _result.attr.alt = "machine";
//      _result.attr.d = m_machine_path;
//      _result.attr.stroke = m_machine_stroke;
//      _result.attr["stroke-width"] = m_machine_stroke_width;
//      _result.attr.fill = m_machine_fill;
//      _result.attr.id = "object_" + svgCanvas.getNextId();
//      break;

//  }

//  return _result;

//}

function WSIGetPathMatrix(PathString) {
  return PathString.match(/[a-zA-Z]+|-?[0-9.]+/g);
}

function WSIJoinPathMatrix(PathMatrix) {
  var _result = "";
  var _last = 0; // 0: Letter, 1: Number
  for (var _i in PathMatrix) {
    _result = _result + (($.isNumeric(PathMatrix[_i]) && _last == 1) ? "," : "") + PathMatrix[_i];
    _last = $.isNumeric(PathMatrix[_i]);
  }
  return _result;
}

function WSIObjectRotate(Attributes, Angle) {

  // TODO: Change orientation by angle
  switch (+Angle) {
    case 0: Angle = 0; break;
    case 1: Angle = 45; break;
    case 2: Angle = 90; break;
    case 3: Angle = 135; break;
    case 4: Angle = 180; break;
    case 5: Angle = 225; break;
    case 6: Angle = 270; break;
    case 6: Angle = 315; break;
  }
  //

  if (Angle != 0) {

    var _set = WSIGetPathMatrix(Attributes.attr.d);

    var _x = _set[1];
    var _y = _set[2];

    Attributes.attr.transform = "rotate(" + Angle + " " + _x + " " + _y + ")";

  }

  return Attributes;

}

function WSIGetAttributesFromElement(Element) {
  var _object = $(Element);
  var _result = {};
  _result.element = Element.localName;
  _result.curStyles = false;
  _result.attr = {};

  _result.attr.alt = _object.attr('alt');
  _result.attr.d = _object.attr('d');
  _result.attr.stroke = CheckNull(_object.attr('stroke'), "#000");
  _result.attr["stroke-width"] = CheckNull(_object.attr('stroke-width'), 1);
  _result.attr.fill = CheckNull(_object.attr('fill'), "#000");
  _result.attr.id = _object.attr('id');
  _result.attr.transform = CheckNull(_object.attr('transform'), 0);

  _result.attr.loc_id = CheckNull(_object.attr('loc_id'), "");
  _result.attr.loc_x = CheckNull(_object.attr('loc_x'), "");
  _result.attr.loc_y = CheckNull(_object.attr('loc_y'), "");
  _result.attr.loc_creation = CheckNull(_object.attr('loc_creation'), "");
  _result.attr.loc_type = CheckNull(_object.attr('loc_type'), "");
  _result.attr.loc_floorId = CheckNull(_object.attr('loc_floorId'), "");
  _result.attr.loc_gridId = CheckNull(_object.attr('loc_gridId'), "");
  _result.attr.lo_id = CheckNull(_object.attr('lo_id'), "");
  _result.attr.lo_externalId = CheckNull(_object.attr('lo_externalId'), "");
  _result.attr.current_lo_externalId = CheckNull(_object.attr('current_lo_externalId'), "");

  _result.attr.lo_type = CheckNull(_object.attr('lo_type'), "");
  _result.attr.lo_parentId = CheckNull(_object.attr('lo_parentId'), "");
  _result.attr.lo_meshId = CheckNull(_object.attr('lo_meshId'), "");
  _result.attr.lo_creation = CheckNull(_object.attr('lo_creation'), "");
  _result.attr.lo_updated = CheckNull(_object.attr('lo_updated'), "");
  _result.attr.lol_object_id = CheckNull(_object.attr('lol_object_id'), "");
  _result.attr.lol_location_id = CheckNull(_object.attr('lol_location_id'), "");
  _result.attr.lol_date = CheckNull(_object.attr('lol_date'), "");
  _result.attr.lol_orientation = CheckNull(_object.attr('lol_orientation'), "");
  _result.attr.lol_areaId = CheckNull(_object.attr('lol_areaId'), "");
  _result.attr.lol_bankId = CheckNull(_object.attr('lol_bankId'), "");
  _result.attr.lol_bankLocation = CheckNull(_object.attr('lol_bankLocation'), "");
  _result.attr.lol_currentLocation = CheckNull(_object.attr('lol_currentLocation'), "");

  _result.attr.location_x = CheckNull(_object.attr('location_x'), "");
  _result.attr.location_y = CheckNull(_object.attr('location_y'), "");

  return _result;
}

function WSIGetLocationFromElement(Element) {
  var _result = { x: 0, y: 0 };

  switch (Element.localName) {
    case "path":
      var _matrix = WSIGetPathMatrix(Element.getAttribute('d'));
      _result.x = _matrix[1];
      _result.y = _matrix[2];
      break;

  }

  return _result;
}

function WSIGetLocationFromAttributes(Attributes) {
  var _result = { x: 0, y: 0 };

  switch (Attributes.element) {
    case "path":
      var _matrix = WSIGetPathMatrix(Attributes.attr.d);
      _result.x = _matrix[1];
      _result.y = _matrix[2];
      break;

  }

  return _result;
}

function WSISetPathLocation(PathString, Location) {
  var _matrix = WSIGetPathMatrix(PathString);

  _matrix[1] = Location.x;
  _matrix[2] = Location.y;

  return WSIJoinPathMatrix(_matrix);
}

function WSISetElementLocation(Element, Location) {
  var _matrix = WSIGetPathMatrix(Element.getAttribute('d'));

  _matrix[1] = Location.x;
  _matrix[2] = Location.y;

  Element.setAttribute('d', WSIJoinPathMatrix(_matrix));

  return ;
}

function WSIGetOrientationFromAttributes(Attributes) {

  if (!Attributes.attr.transform) return 0;

  var _matrix = WSIGetPathMatrix(Attributes.attr.transform);
  return _matrix[1];

}

function WSIAttributesToObjectData(Attributes) {
  var _result = new WSIObject(),
      _loc = WSIGetLocationFromAttributes(Attributes);

  _result.location.id = Attributes.attr.loc_id;
  _result.location.x = Attributes.attr.loc_x;
  _result.location.y = Attributes.attr.loc_y;

  _result.location.current_x = _loc.x;
  _result.location.current_y = _loc.y;
  
  _result.location.creation = Attributes.attr.loc_creation;
  _result.location.type = Attributes.attr.loc_type;
  _result.location.floorId = Attributes.attr.loc_floorId;
  _result.location.gridId = Attributes.attr.loc_gridId;
  _result.id = Attributes.attr.lo_id;
  _result.externalId = Attributes.attr.lo_externalId;
  _result.current_externalId = Attributes.attr.current_lo_externalId;
  _result.type = Attributes.attr.lo_type;
  _result.parentId = Attributes.attr.lo_parentId;
  _result.meshId = Attributes.attr.lo_meshId;
  _result.creation = Attributes.attr.lo_creation;
  _result.updated = Attributes.attr.lo_updated;
  _result.object_location.object_id = Attributes.attr.lol_object_id;
  _result.object_location.location_id = Attributes.attr.lol_location_id;
  _result.object_location.date = Attributes.attr.lol_date;
  _result.object_location.orientation = Attributes.attr.lol_orientation;

  _result.object_location.current_orientation = WSIGetOrientationFromAttributes(Attributes);

  _result.object_location.areaId = Attributes.attr.lol_areaId;
  _result.object_location.bankId = Attributes.attr.lol_bankId;
  _result.object_location.bankLocation = Attributes.attr.lol_bankLocation;
  _result.object_location.currentLocation = Attributes.attr.lol_currentLocation;

  //_result.id = Attributes.attr.lo_id;
  //_result.type = Attributes.attr.;
  //_result.orientation = WSIGetOrientationFromAttributes(Attributes);
  //_result.location.id = Attributes.attr.loc_id
  //_result.location.x = _loc.x;
  //_result.location.y = _loc.y;
  //_result.externalId = Attributes.attr.binded;

  return _result;
}

function WSIObjectTypes(ObjectType) {

  var _result = {};
  _result.element = "";
  _result.curStyles = false;
  _result.attr = {};

  switch (+ObjectType) {

    case 0: // Position
      _result.element = "path";
      _result.attr.alt = "position";
      _result.attr.d = m_position_path;
      _result.attr.stroke = m_position_stroke;
      _result.attr["stroke-width"] = m_position_stroke_width;
      _result.attr.fill = m_position_fill;
      _result.attr.id = "object_" + svgCanvas.getNextId();
      break;

    case 1: // Machine
      _result.element = "path";
      _result.attr.alt = "machine";
      _result.attr.d = m_machine_path;
      _result.attr.stroke = m_machine_stroke;
      _result.attr["stroke-width"] = m_machine_stroke_width;
      _result.attr.fill = m_machine_fill;
      _result.attr.id = "object_" + svgCanvas.getNextId();
      break;

    case 70: // Beacon
      _result.element = "path";
      _result.attr.alt = "beacon";
      _result.attr.d = m_beacon_path;
      _result.attr.stroke = m_beacon_stroke;
      _result.attr["stroke-width"] = m_beacon_stroke_width;
      _result.attr.fill = m_beacon_fill;
      _result.attr.id = "object_" + svgCanvas.getNextId();
      break;
  }

  return _result;

}