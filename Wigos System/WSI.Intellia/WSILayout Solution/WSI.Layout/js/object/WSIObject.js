﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: filename
// 
//   DESCRIPTION: Description.
// 
//        AUTHOR: Author
// 
// CREATION DATE: Date
// 
//------------------------------------------------------------------------------

/*
 Requires:

 */


WSILocation = function () {
  this.id = '';
  this.type = '';
  this.x = '';
  this.y = '';
  this.creation = '';
  this.floorId = '';
  this.gridId = '';
}

WSIObjectLocation = function () {
  this.object_id = '';
  this.location_id = '';
  this.date = '';
  this.orientation = '';
  this.areaId = '';
  this.bankId = '';
  this.bankLocation = '';
  this.currentLocation = '';
}

WSIObject = function () {
  this.location = new WSILocation();
  this.object_location = new WSIObjectLocation();

  this.id = '';
  this.externalId = '';
  this.type = '';
  this.parentId = '';
  this.meshId = '';
  this.creation = '';
  this.updated = '';

}

WSIObject.prototype.Load = function (ObjectData) {

  this.location = new WSILocation();
  this.location.id = ObjectData[0];
  this.location.x = ObjectData[1];
  this.location.y = ObjectData[2];
  this.location.creation = ObjectData[3];
  this.location.type = ObjectData[4];
  this.location.floorId = ObjectData[5];
  this.location.gridId = ObjectData[6];
  
  this.id = ObjectData[7];
  this.externalId = ObjectData[8];
  this.current_externalId = ObjectData[8];
  this.type = ObjectData[9];
  this.parentId = ObjectData[10];
  this.meshId = ObjectData[11];
  this.creation = ObjectData[12];
  this.updated = ObjectData[13];
  
  this.object_location = new WSIObjectLocation();
  this.object_location.object_id = ObjectData[14];
  this.object_location.location_id = ObjectData[15];
  this.object_location.date = ObjectData[16];
  this.object_location.orientation = ObjectData[17];
  this.object_location.areaId = ObjectData[18];
  this.object_location.bankId = ObjectData[19];
  this.object_location.bankLocation = ObjectData[20];
  this.object_location.currentLocation = ObjectData[21];
    
}