﻿////////////////////////////////////////////////////////////////////////////////////////////////
// Functions to set/get and update RunnersMap
// * requires JQUERY
// * requires WSIUserConfiguration
// * requires HeatMap.JS
// * requires WSILogger
////////////////////////////////////////////////////////////////////////////////////////////////

function WSIRunnersMap() {
  this.Data = {
    max: 1,
    data: []
  };;
  this.Height = null;
  this.Width = null;
  this.PlaneHeight = null;
  this.PlaneWidth = null;
  this.HeatMap3D = null;
  this.MarkersContainer = null;
  this.markers = null;
  this.beaconsGroup = null;
  this.markersGroup = null;
  this.Visible = false;
  this.Control = null;
  this.Colors = { 0: "rgb(241,90,36)", 1: "rgb(241,90,36)" };
  this.Updating = false;
  this.RadiusConst = 20;
  this.RunnersByPosition = [];
  this.RunnersById = [];
  this.RunnerSelected = 0;
  this.Created = false;
  this.runnersPositionY = 13;
  this.pinSingleMaterial = null;
  this.pinPluralMaterial = null;

}

// Method that creates the object and ensure target component exists
WSIRunnersMap.prototype.Create = function (Width, Height) {
  WSILogger.Write("RunnersMap - Creating...");

  var _plane = null;
  // To obtain HeatMap size
  _plane = document.getElementById('img_background');
  if (!_plane) { _plane = document.getElementById('div_background'); }

  if (!_plane) {
    WSILogger.Write("RunnersMap - No plane to create!");
    return;
  }

  // HeatMap target for 2D
  var _hmap = document.getElementById("div_runnersmapArea");
  
  this.Width = Width ? Width : $(_plane).width();
  this.Height = Height ? Height : $(_plane).height();
  this.PlaneWidth = $(_plane).width();
  this.PlaneHeight = $(_plane).height();
  // Set Size
  $(_hmap).width(this.PlaneWidth);
  $(_hmap).height(this.PlaneHeight);

  this.EnsureControlCreated();

  

  // 3D
  if (_LayoutMode == 1) {
    // Create material for the object
    WSILogger.Write("Heatmap - 3D - Creating material...");
    var _mat = null;
    _mat = new THREE.MeshBasicMaterial({ color: 0xffffff, ambient: 0xffffff, shininess: 0, transparent: true });

    var _3d_width, _3d_height;

    _3d_width = PixelsToUnits(_CurrentFloor3.dimensions.x) * 4;
    _3d_height = PixelsToUnits(_CurrentFloor3.dimensions.y) * 4;

    this.Width = PixelsToUnits(_CurrentFloor3.dimensions.x) * 4;
    this.Height = PixelsToUnits(_CurrentFloor3.dimensions.y) * 4;

    //this.HeatMap3D = new THREE.Mesh(new THREE.PlaneGeometry(this.Width, this.Height), _mat);
    this.HeatMap3D = new THREE.Mesh(new THREE.PlaneBufferGeometry(_3d_width, _3d_height), _mat);
    this.HeatMap3D.userData = { width: _3d_width, height: _3d_height };
    this.HeatMap3D.position.y = 12;
    this.HeatMap3D.rotation.x = -Math.PI / 2;
    this.HeatMap3D.updateMatrix();
    this.HeatMap3D.material.needsUpdate = true;
    this.HeatMap3D.material.transparent = true;
    this.HeatMap3D.name = "HeatMap_plane";

    WSILogger.Write("RunnersMap - 3D - Adding to Scene");
    //WSI.scene.add(_Manager.RunnersMap.HeatMap3D);

    this.MarkersContainer = new THREE.Group();
    this.MarkersContainer.visible = false;

    var pinSingleTexture = THREE.ImageUtils.loadTexture("img/pin_single.png")
    pinSingleTexture.magFilter = THREE.NearestFilter;
    pinSingleTexture.offset.set(-0.5, -1);
    pinSingleTexture.repeat.set(2, 2);

    this.pinSingleMaterial = new THREE.SpriteMaterial({
        map: pinSingleTexture
    });
    
    
    var pinPluralTexture = THREE.ImageUtils.loadTexture('img/pin_plural.png');
    pinPluralTexture.magFilter = THREE.NearestFilter;
    pinPluralTexture.offset.set(-0.5, -1);
    pinPluralTexture.repeat.set(2, 2);
    this.pinPluralMaterial = new THREE.SpriteMaterial({
        map: pinPluralTexture   
    
    });

      /*

      //Not Necesary Code

      //var pinPluralTexture = new THREE.TextureLoader().load("img/pin_plural.png")

      //alphaTest: 0.5,
      //transparent: true,
      //depthWrite: false,
      //depthTest: false
    
      //spriteTexture.anisotropy = 0;
      //spriteTexture.minFilter = THREE.NearestFilter;    

      //spriteTexture.blending = THREE.CustomBlending
      //spriteTexture.blendSrc = THREE.OneFactor
      //spriteTexture.blendDst = THREE.OneMinusSrcAlphaFactor
      //spriteTexture.wrapS = spriteTexture.wrapT = THREE.RepeatWrapping;
      //spriteTexture.overdraw = true;

      */

    WSI.scene.add(_Manager.RunnersMap.MarkersContainer);
  }

  this.Created = true;
}

WSIRunnersMap.prototype.ClearData = function () {
  delete this.Data
  this.Data = {
    max: 1,
    data: []
  };

  if (this.Control) {
    this.Control.clear();
  }
}

// Initializes de object on each update
WSIRunnersMap.prototype.Initialize = function (Width, Height) {
  WSILogger.Write("Runnersmap - Initializing");

  this.ClearData();
  this.EnsureControlCreated();
  this.Control.set("gradient", this.Colors);
  this.Control.initColorPalette();

  // Set Height and Width
  if (Width) {
    if (typeof Width === "string") {
      this.Width = +(Width.replace("px", ""));
      this.Height = +(Height.replace("px", ""));
    } else {
      this.Width = Width;
      this.Height = Height;
    }
  }
}

// Forces all the update process
WSIRunnersMap.prototype.ForcedUpdate = function () {
  WSILogger.Write("RunnersMap - Forcing update!");

  var _runner = null;

  this.Initialize();

  this.RunnersByPosition = [];
  this.RunnersById = [];

  for (var _idx_runner in _Manager.Runners.RunnersMapLocations) {
    _runner = _Manager.Runners.RunnersMapLocations[_idx_runner];

    this.AddDataByRunner(_runner);
  }

  this.Update();
}

//
WSIRunnersMap.prototype.CanUpdate = function () {
  return (m_current_section == 40);
}

//
WSIRunnersMap.prototype.GetValueByColor = function (Color) {
  if (this.Colors) {
    for (var _i in this.Colors) {
      if (Color == this.Colors[_i]) {
        return _i;
      }
    }
  } else {
    return 0;
  }
}

// Add data to the runnersmap of a single Runner
WSIRunnersMap.prototype.AddDataByRunner = function (Runner) {
  var _runner_x;
  var _runner_y;
  var _distance;

  /**
        Radio: _new_item.range,
        NumUsers: 1,
        X: _new_item.x,
        Y: _new_item.z
   */

  _runner_x = Runner.X;
  _runner_y = Runner.Y;

  if (_LayoutMode == 1) {
    // 3D
    var _rx = ((_runner_x) * this.PlaneWidth) / this.Width;
    var _ry = ((_runner_y) * this.PlaneHeight) / this.Height;
    var _o_x = Math.round(this.PlaneWidth / 2);
    var _o_z = Math.round(this.PlaneHeight / 2);
    var _x = _o_x + _rx;
    var _z = _o_z + _ry;

  } else {
    // 2D
    var _o_x = Math.round(this.PlaneWidth / 2);
    var _o_z = Math.round(this.PlaneHeight / 2);
    var _x = _o_x + (_runner_x * 5) + 5;
    var _z = _o_z + (_runner_y * 5) + 5;

  }

  // Insert data
  var _radius = 0;

  if (+Runner.Radio < 1) {
    _radius = 1;
  } else {
    _radius = +Runner.Radio;
  }

  var _position_id = _runner_x + "_" + _runner_y;
  var _count = 0.3;
  var _users_list;
  var _users_count = 0;

  for (_idx_runner_user in Runner.Users) {
    this.RunnersById[Runner.Users[_idx_runner_user]] = { x: _runner_x, y: _runner_y, dist: _radius };

    if (Runner.Users[_idx_runner_user] == this.RunnerSelected) {
      _count = 1;
    }
    _users_count = _users_count + 1;
  }

  if (_users_count > 1) {
    this.RunnersByPosition[_position_id] = Runner.Users;
  } else { this.RunnersByPosition[_position_id] = Runner.Users[0]; }


  this.Data.data.push({ x: _x, y: _z, radius: _radius * this.RadiusConst, count: _count });
}

WSIRunnersMap.prototype.GetClosestRunners = function (RaycasterX, RaycasterY) {

  var _point;
  var _closest_distance;
  var _closest_runner = null;
  var _distance;
  var _closest_point;
  var _return_list = [];
  var _count=0;

  for (_idx_runner_position in this.RunnersById) {

    _point = this.RunnersById[_idx_runner_position];
    _distance = GetDistancePoints(_point.x, _point.y, RaycasterX, RaycasterY);


    if (_closest_runner == null) {
      if (_distance <= (_point.dist * 4)) {
        _closest_runner = _idx_runner_position;
        _closest_distance = +_distance;
        _closest_point = _point;
      }
    }

    if (+_closest_distance > +_distance) {
      if (+_distance <= (_point.dist * 4)) {
        _closest_distance = +_distance;
        _closest_runner = _idx_runner_position;
        _closest_point = _point;
      }
    }
  }

  if (_closest_point != undefined) {
    for (_idx_runner_position in this.RunnersById) {

      _point = this.RunnersById[_idx_runner_position];

      if (_point.x == _closest_point.x && _point.y == _closest_point.y) {
        _return_list[_idx_runner_position] = _Manager.Runners.ItemsById[+_idx_runner_position].userName;
        _count = _count + 1;
      }
    }
  }

  _return_list.count = _count;

  return _return_list;
}

// Update the location (vertical) for 3D mode
WSIRunnersMap.prototype.UpdateLocation = function () {
  WSILogger.Write("Runnersmap - Updating location...");
  var _y_offset = (WSIData.ModelTypes == 2) ? 9 : 12;
  this.HeatMap3D.position.y = _y_offset;
}

// Method that re-create the heatmap control if necessary
WSIRunnersMap.prototype.EnsureControlCreated = function () {
  if (_LayoutMode == 2) {
    // 2D Recreate control
    WSILogger.Write("RunnersMap - 2D - disposing...");
    $("#div_runnersmapArea").empty();
    this.Control = null;
  }
  if (this.Control == null) {
    WSILogger.Write("RunnersMap - recreating control...");
    var _hmap = document.getElementById("div_runnersmapArea");

    //_hmap.style.zIndex = 3;

    var _beaconsContainer = document.createElement('div');
    _beaconsContainer.id = "beaconsContainer";
      //_beaconsContainer.className = "div_center";
    _hmap.appendChild(_beaconsContainer);

    var _rad = _LayoutMode == 1 ? WSIData.radius3D : WSIData.radius2D;
    this.Control = h337.create({ "element": _hmap, "visible": false });

 

  }
}

// Simple update control and target component
WSIRunnersMap.prototype.Update = function () {

  this.Updating = true;

  if (this.Data.data.length == 0) {
    // totally transparent if there is no data
    WSILogger.Write("Runnersmap - No data!");
    this.Data = { max: 1, data: [] }; //{ max: 0, data: [{ x: 0, y: 0, count: 0 }] };
  }

  try {

    WSILogger.Write("RunnersMap - Updating...");


    if (this.Data != null) {

        //      // -------------------------------------
        //      // DELETE
        //      var _debug = JSON.stringify(this.Data);
        //      WSILogger.Write(_debug);
        //      this.Data = {
        //        max: 1,
        //        data: [{ x: 0, y: 0, count: 100}]
        //      }
        //      // -------------------------------------

        WSILogger.Write("RunnersMap - Setting data...");


        //Blocking Data to HeatMap
        //this.Control.store.setDataSet(this.Data);


        if (_LayoutMode == 1) { // 3D

            if (this.beaconsGroup)
                this.MarkersContainer.remove(this.beaconsGroup);

            var group = new THREE.Group();
            group.position.set(-this.Width / 2, this.runnersPositionY, -this.Height / 2);
            group.scale.set(0.2, 0.2, 0.2);

            var markers = new THREE.Group();

            this.markers = [];

            for (var i in this.Data.data) {

                var beacon = this.Data.data[i];

                var beaconScale = 20;

                var beacon3D = new Object();
                beacon3D.x = this.convertXTo3D(beacon.x);//(this.Width / 2 - beacon.x * 0.2) * -1;
                beacon3D.y = this.convertYTo3D(beacon.y);//(this.Height / 2 - beacon.y * 0.2)*-1;

                var _distance;           
                var _point = this.RunnersById[this.RunnerSelected];

                if (_point) {
                    _distance = GetDistancePoints(_point.x, _point.y, beacon3D.x, beacon3D.y);
                }

                var colorArea = 0xf7bd5d;
                var opacityArea = 0.5;
    
                if (_distance == 0) {
                    colorArea = 0xff9f00;
                    //opacityArea = 0.8;
                }



                var geometry = new THREE.CircleGeometry(beacon.radius, 32);
                var material = new THREE.MeshBasicMaterial({
                    color: colorArea,
                    opacity: opacityArea,
                    side: THREE.DoubleSide,
                    depthWrite: false,
                    transparent: true
                });

                var circle = new THREE.Mesh(geometry, material);
                circle.position.x = beacon.x;
                circle.position.z = beacon.y;
                circle.rotation.x = -Math.PI / 2;
            
                group.add(circle)




                _runner_list = _Manager.RunnersMap.GetClosestRunners(beacon3D.x, beacon3D.y);

            

                var spriteMaterial;
                if (_runner_list.count > 1) {
                    var textCounter = makeTextSprite(_runner_list.count, { borderColor: { r: 0, g: 124, b: 231, a: 1.0 }, backgroundColor: { r: 110, g: 185, b: 231, a: 1.0 } });
                    textCounter.position.set(beacon3D.x, group.position.y, beacon3D.y);
                    textCounter.scale.set(beaconScale, beaconScale, 1); // imageWidth, imageHeight
                    markers.add(textCounter);

                    spriteMaterial = this.pinPluralMaterial.clone();
                } else {
                    spriteMaterial = this.pinSingleMaterial.clone();
                }
            
                var sprite = new THREE.Sprite(spriteMaterial);
                sprite.position.set(beacon3D.x, group.position.y, beacon3D.y);
                sprite.scale.set(beaconScale, beaconScale, 1); // imageWidth, imageHeight
                markers.add(sprite);
            

                var clickBoxTexture = THREE.ImageUtils.loadTexture("img/clickBox.png")
                var clickBoxMaterial = new THREE.SpriteMaterial({
                    map: clickBoxTexture
                });
                var clickBox = new THREE.Sprite(clickBoxMaterial);
                clickBox.position.set(beacon3D.x, group.position.y, beacon3D.y);
                clickBox.scale.set(beaconScale / 2, beaconScale / 2, 1); // imageWidth, imageHeight

                var euler = _Manager.Controls.CurrentControls().object.rotation;
                var spriteOffset = new THREE.Vector3(0, beaconScale/4, 0);
                spriteOffset.applyMatrix4(new THREE.Matrix4().makeRotationFromEuler(euler));
                clickBox.position.add(spriteOffset);

                markers.add(clickBox);
                
                this.markers[i] = clickBox;
            
            }
        
            this.MarkersContainer.add(markers);

            if (this.markersGroup)
                this.MarkersContainer.remove(this.markersGroup);

            this.markersGroup = markers;

            this.MarkersContainer.add(group);
            this.beaconsGroup = group;   

            if (this.HeatMap3D == null) {
                WSILogger.Write("RunnersMap - Object not created!");
                return;
            }

            // Height of heatmap is different
            this.UpdateLocation();

            // Update material
            this.HeatMap3D.material.transparent = true;

            // Re-create material      
            var _canvas = this.Control.get("canvas");

            var _texture = new THREE.Texture(_canvas);
            _texture.minFilter = THREE.LinearFilter;
            _texture.magFilter = THREE.LinearFilter;
            _texture.needsUpdate = true;

            this.HeatMap3D.material.map = _texture;
        } else {
                

            $("#beaconsContainer").empty();

            for (var i in this.Data.data) {

                var beacon = this.Data.data[i];

                var beacon2D = new Object();
                beacon2D.x = this.convertXTo2D(beacon.x);//(this.Width / 2 - beacon.x * 0.2) * -1;
                beacon2D.y = this.convertYTo2D(beacon.y);//(this.Height / 2 - beacon.y * 0.2)*-1;

                var _distance;           
                var _point = this.RunnersById[this.RunnerSelected];

                if (_point) {
                    _distance = GetDistancePoints(_point.x, _point.y, beacon2D.x, beacon2D.y);
                }

                var colorArea = '#f7bd5d';
                var opacityArea = 0.5;
    
                if (_distance == 0) {
                    colorArea = '#ff9f00';
                    //opacityArea = 0.8;
                }
                                

                var circle = createCirlcle(beacon.x, beacon.y, beacon.radius, colorArea);
                               
                $("#beaconsContainer").append(circle);

                var widthMarker = 100;

                var _runner_list = _Manager.RunnersMap.GetClosestRunners(beacon2D.x, beacon2D.y);

                var imgSrc
                if (_runner_list.count > 1) 
                    imgSrc = 'img/pin_plural.png';
                else 
                    imgSrc = 'img/pin_single.png';
                    
                var marker = document.createElement("img");
                marker.setAttribute('src', imgSrc);
                marker.style.width = widthMarker + 'px';
                marker.style.position = 'absolute';
                marker.className = "marker";
                marker.style.left = (beacon.x - widthMarker/ 2) + 'px';
                marker.style.top = (beacon.y - widthMarker) + 'px';

                $(marker).css("z-index", 6)

                $("#beaconsContainer").append(marker);

                
                
                if (_runner_list.count > 1) {
                    var cb = canvasBadge(_runner_list.count, { borderColor: { r: 0, g: 124, b: 231, a: 1.0 }, backgroundColor: { r: 110, g: 185, b: 231, a: 1.0 } });
                    cb.style.width = widthMarker + 'px';
                    cb.style.position = 'absolute';
                    cb.style.left = (beacon.x - widthMarker / 2) + 'px';
                    cb.style.top = (beacon.y - widthMarker) + 'px';
                    
                    $(cb).css("z-index", 6)

                    $("#beaconsContainer").append(cb);
                }

            }

            console.log("2D")            

        }

    }

  } catch (ex) {
    WSILogger.Log("WSIRunnersMap.Update", ex);
  }

  this.Updating = false;
}

function makeTextSprite(message, parameters) {

    var cb = canvasBadge(message, parameters);

    var texture = new THREE.Texture(cb);
    texture.offset.set(-0.5, -1);
    texture.repeat.set(2, 2);
    texture.needsUpdate = true;    
    texture.magFilter = THREE.NearestFilter;
    
    var spriteMaterial = new THREE.SpriteMaterial({ map: texture});
    var sprite = new THREE.Sprite(spriteMaterial);
    return sprite;
}

function canvasBadge(message, parameters) {
    
    message = message.toString();
    
    if (parameters === undefined) parameters = {};

    var fontface = parameters.hasOwnProperty("fontface") ?
		parameters["fontface"] : "Arial";

    var fontsize = parameters.hasOwnProperty("fontsize") ?
		parameters["fontsize"] : 90;

    var borderThickness = parameters.hasOwnProperty("borderThickness") ?
		parameters["borderThickness"] : 5;

    var borderColor = parameters.hasOwnProperty("borderColor") ?
		parameters["borderColor"] : { r: 0, g: 0, b: 0, a: 1.0 };

    var backgroundColor = parameters.hasOwnProperty("backgroundColor") ?
		parameters["backgroundColor"] : { r: 255, g: 255, b: 255, a: 1.0 };

    //var spriteAlignment = THREE.SpriteAlignment.topLeft;

    var canvasBadge = document.createElement('canvas');
    var context = canvasBadge.getContext('2d');
    context.canvas.width = 512;
    context.canvas.height = 512;
    context.font = "Bold " + fontsize + "px " + fontface;

    
    // get size data (height depends only on font size)
    var metrics = context.measureText(message);
    var textWidth = metrics.width;

    // background color
    context.fillStyle = "rgba(" + backgroundColor.r + "," + backgroundColor.g + "," + backgroundColor.b + "," + backgroundColor.a + ")";
    // border color
    context.strokeStyle = "rgba(" + borderColor.r + "," + borderColor.g + "," + borderColor.b + "," + borderColor.a + ")";


    var sizechar = textWidth / message.length;
    
    context.lineWidth = borderThickness;
    badge(context, 300 + borderThickness, borderThickness, sizechar * 3);
    // 1.4 is extra height factor for text below baseline: g,j,p,q.

    // text color
    context.fillStyle = "rgba(255, 255, 255, 1.0)";

    var removeSize = (sizechar * (message.length - 1)) / 2

    context.fillText(message, 300 + (sizechar + borderThickness) - removeSize, (sizechar + borderThickness) * 2);
    context.textBaseline = "top";

    context.globalCompositeOperation = "destination-in";

    return canvasBadge;
}

// function for drawing rounded rectangles
function badge(ctx, x, y, w) {
    ctx.beginPath();
    ctx.arc(x + w / 2, y + w / 2, w / 2, 0, 2 * Math.PI, false);
    ctx.fill();
    ctx.stroke();
}

function createCirlcle(x, y, r, color) {

    var canvasCircle = document.createElement('canvas');
    var ctx = canvasCircle.getContext('2d');
    ctx.canvas.width = r * 2;
    ctx.canvas.height = r * 2;
    ctx.beginPath();   
    ctx.arc(r, r, r, 0, 2 * Math.PI, false);
    ctx.fillStyle = color;
    ctx.globalAlpha = 0.5
    ctx.fill();

    canvasCircle.style.position = 'absolute';
    canvasCircle.style.left = (x - r) + 'px';
    canvasCircle.style.top = (y - r) + 'px';

    return canvasCircle;

}

// Set the HeatMap component visibility
WSIRunnersMap.prototype.SetVisibility = function (Visible) {
    WSILogger.Write("Runnersmap - Setting visibility = [" + Visible + "]");
    if (Visible) {
        $("#div_runnersmapArea").show();
        this.ForcedUpdate();
    }
    if (_LayoutMode == 2 && !Visible) {
        $("#div_runnersmapArea").empty();
        $("#div_runnersmapArea").hide();
        this.Control = null;
    }

    this.visible = Visible;
    if (this.MarkersContainer) {
        if (_LayoutMode == 1 && this.CanUpdate()) {
            this.HeatMap3D.visible = Visible;
            this.MarkersContainer.visible = Visible;
        } else {
            this.MarkersContainer.visible = false;
        }
    }
}

    WSIRunnersMap.prototype.convertXTo3D = function (x) {
        return (this.Width / 2 - x * 0.2) * -1;
    }

    WSIRunnersMap.prototype.convertYTo3D = function (y) {
        return (this.Height / 2 - y * 0.2) * -1;
    }


    WSIRunnersMap.prototype.convertXTo2D = function (v) {
        var _o = Math.round(this.PlaneWidth / 2);
        return (v - 5 - _o) / 5;

    }

    WSIRunnersMap.prototype.convertYTo2D = function (v) {
        var _o = Math.round(this.PlaneHeight / 2);   
        return (v - 5 - _o) / 5;
    }

    WSIRunnersMap.prototype.convertXfrom2D = function (v) {
      var _o = Math.round(this.PlaneWidth / 2);
      return (v * 5) + _o + 5;
    }

    WSIRunnersMap.prototype.convertYfrom2D = function (v) {
      var _o = Math.round(this.PlaneHeight / 2);
      return (v * 5) + _o + 5;
    }



    WSIRunnersMap.prototype.AddLocations = function (LocationsList) {
        try {

            this.RunnersByPosition = [];
            this.RunnersById = [];

            for (_idx_location in LocationsList) {
                var _location = LocationsList[_idx_location];

                this.AddDataByRunner(_location);
            }

        } catch (error) {
            WSILogger.Write("WSIRunnersMap.AddLocations() " + error);
        }

    }

