﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: filename
// 
//   DESCRIPTION: Description.
// 
//        AUTHOR: Author
// 
// CREATION DATE: Date
// 
//------------------------------------------------------------------------------

function WSIRunner() {
  this.id = "";
  this.deviceId = "";
  this.floorName = "";
  this.x = 0;
  this.y = WSIData.RunnerSphereRadius;
  this.z = 0;
  this.range = "";
  this.lastUpdate = "";
  this.userName = "";
  this.deviceId = "";
  this.roles = "";

  this.Mesh = null;
  this.source = "";
}

WSIRunner.prototype.Update = function (RunnerData) {


  this.id = RunnerData[0].replace(/:/g, ''); // This may not change
  this.deviceId = RunnerData[1].replace(/:/g, '');
  this.floorId = RunnerData[2];

  // Calculate real x/y
  var _fx = -Math.floor(PixelsToUnits(_CurrentFloor3.dimensions.x) / 2) * 4,
      _fy = -Math.floor(PixelsToUnits(_CurrentFloor3.dimensions.y) / 2) * 4;
  this.x = _fx + (+RunnerData[3] * 4);
  //this.x = +RunnerData[3];
  this.y = WSIData.RunnerSphereRadius;
  this.z = _fy + (+RunnerData[4] * 4);
  //this.z = +RunnerData[4];

  this.range = RunnerData[5];
  this.lastUpdate = RunnerData[6];

  this.userName = RunnerData[7];
  this.deviceId = RunnerData[8];
  this.floorName = RunnerData[9];
  this.roles = +RunnerData[10];
  this.source = RunnerData[11];

}

WSIRunner.prototype.Build = function (Parent, CallbackRunnerBuilder) {

  // RMS - Force temporally to show floor 1 as current floor
  //if (+this.floorId != 1) { return; }
  // Only draw runners in this floor
  //if (+this.floorId != _Configuration.CurrentFloorId) { return; }

  if (_LayoutMode == 1) // 3D
  {
    if (this.Mesh == null) {
      // Create 
      var _runner_res = _Manager.Resources.Items[WSIData.RunnerChar];
      if (_runner_res) {

        this.Mesh = new THREE.Mesh(new THREE.SphereGeometry(WSIData.RunnerSphereRadius, WSIData.RunnerSphereSegments, WSIData.RunnerSphereSegments), new THREE.MeshBasicMaterial({ color: 0x000000 }));
        this.Mesh.visible = true;
        this.Mesh.position.set(this.x, this.y, this.z);
        this.Mesh.name = this.id + "_" + this.deviceId;
        Parent.add(this.Mesh);

        var _runner = new THREE.Mesh(_runner_res.object.geometry, _runner_res.object.material);
        _runner.geometry.computeBoundingBox();
        _runner.visible = false;

        if (_runner.Options != "") {
          _runner.geometry.computeBoundingBox();
          _runner.scale.set(_runner.Options.initialScale.x, _runner.Options.initialScale.y, _runner.Options.initialScale.z);
        }

        this.Mesh.add(_runner);
      }

    } else {
      // Update
      if (_Manager.Cameras.CurrentMode == EN_CAMERA_MODE.CM_FIRST_PERSON) {
        this.Mesh.material.transparent = true;
        this.Mesh.material.opacity = 0;
        this.Mesh.children[0].visible = true;
      } else {
        this.Mesh.material.transparent = false;
        this.Mesh.material.opacity = 1;
        this.Mesh.children[0].visible = false;
      }

      this.Mesh.position.set(this.x, this.y, this.z);

    }

  } else { // 2D
    // Create
    if (CallbackRunnerBuilder) {
      CallbackRunnerBuilder(this);
    }
  }

}

//------------------------------------------------------------------------------

function WSIRunnersManager() {

  this.Items = [];
  if (_LayoutMode == 1) { this.Node = new THREE.Object3D; }

  this.total = 0;
  this.inSite = 0;
  this.unknown = 0;
  this.Labels = [];
  this.RunnersMapLocations = {};  //List that stores x_y and radius of heatmap.
  this.ItemsById = [];
}

WSIRunnersManager.prototype.FindItem = function (RunnerId) {
  var _result = null;
  for (var _r in this.Items) {
    if (this.Items[_r].id == RunnerId) {
      _result = this.Items[_r];
      break;
    }
  }
  return _result;
}

WSIRunnersManager.prototype.AddItem = function (RunnerData) {

  if (RunnerData) {

    var _r_id = RunnerData[0].replace(/:/g, '');
    var _runner = this.FindItem(_r_id);

    var temp = new WSIRunner()
    temp.Update(RunnerData);

    if (_CurrentFloor3.name != temp.floorName)
      return;

    var _new_item_position;

    var _new_item = new WSIRunner();
    if (_runner == null) {

      _new_item.Update(RunnerData);
      _new_item.Color = _new_item.source == 1 ? "#3366ff" : "#00ff00"
      this.Items.push(_new_item);
      this.ItemsById[_new_item.id] = _new_item;

    } else {
      _runner.Update(RunnerData);
      _new_item = _runner;
      this.ItemsById[_new_item.id] = _new_item;
    }

    //Searh into list of runnersmapspositions
    _new_item_position = _new_item.x + "_" + _new_item.z;

    if (this.RunnersMapLocations[_new_item_position]) {
      var _data = this.RunnersMapLocations[_new_item_position];
      var _num_users = +this.RunnersMapLocations[_new_item_position].NumUsers;

      if (+_data.Radio < +_new_item.range) {
        this.RunnersMapLocations[_new_item_position].Radio = +_new_item.range;
      }

      this.RunnersMapLocations[_new_item_position].NumUsers = _num_users + 1;
      this.RunnersMapLocations[_new_item_position].Users.push(_r_id);

    } else {
      var _runners_position_data = {
        Radio: _new_item.range,
        NumUsers: 1,
        X: _new_item.x,
        Y: _new_item.z,
        Users: []
      };
      _runners_position_data.Users.push(_r_id);
      this.RunnersMapLocations[_new_item_position] = _runners_position_data;
    }
  }
}

WSIRunnersManager.prototype.Update = function (RunnersData) {

  this.total = 0;
  this.inSite = 0;
  this.unknown = 0;


  this.RunnersMapLocations = [];
  this.Items = [];

  for (var _r in RunnersData.rows) {
    var _runner_data = RunnersData.rows[_r];
    this.AddItem(_runner_data);

    this.total++;
    if (_runner_data[1] != "") { this.inSite++; } else { this.unknown++; }

  }

}

WSIRunnersManager.prototype.Build = function () {
  try {
    if (!_Configuration.ShowRunnersMap) { return; }

    if (_Manager.RunnersMap.CanUpdate()) {
      // Update Heatmap
      _Manager.RunnersMap.ClearData();
      _Manager.RunnersMap.AddLocations(this.RunnersMapLocations);
    }


    if (_Configuration.ShowRunnersMap) {
      _Manager.RunnersMap.Update();
    }
  } catch (error) {
    WSILogger.Write("WSIRunnersManager.Build() " + error);
  }
}

