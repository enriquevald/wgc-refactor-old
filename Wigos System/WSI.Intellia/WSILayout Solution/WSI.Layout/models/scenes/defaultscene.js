﻿var _my_data = {
    legendMaterials: [],
    lastItemUpdated: 0,
    maxItems: 0,
    itemsPerLoop: 500,
    objects: [],
    objectsStates: [],
    IntervalMaster: null,
    changeEllapsedTime: 0
};

function defaultScene_Render() {

    // Set pre render operations
    if (WSI.CurrentEngine.CURRENT_SCENE_PRE_RENDER == null) {

        WSI.CurrentEngine.CURRENT_SCENE_PRE_RENDER = function () {

            if (_my_data.lastItemUpdated >= _my_data.maxItems) {
                _my_data.lastItemUpdated = 0;
            }

            var _to = _my_data.lastItemUpdated + _my_data.itemsPerLoop;
            if (_to > _my_data.maxItems) {
                _to = _my_data.maxItems;
            }

            var _start = _my_data.lastItemUpdated;

            for (var _i = _my_data.lastItemUpdated; _i <= _to; _i++) {

                // update the item, change material
                _my_data.objects[_i].material = _my_data.legendMaterials[_my_data.objectsStates[_i]];
                _my_data.objects[_i].material.needsUpdate = true;

                _my_data.objects[_i].rotation.y += 0.1;

                _my_data.lastItemUpdated = _i;
            }
                        
        }

    }

    // Build Scene

    // Add some light
    function threePointLight() {

        var directionalLight = new THREE.DirectionalLight(0xb8b8b8);
        directionalLight.position.set(1, 1, 1).normalize();
        directionalLight.intensity = 1.0;
        WSI.CurrentEngine.SCENE.add(directionalLight);

        directionalLight = new THREE.DirectionalLight(0xb8b8b8);
        directionalLight.position.set(-1, 0.6, 0.5).normalize();
        directionalLight.intensity = 0.5;
        WSI.CurrentEngine.SCENE.add(directionalLight);

        directionalLight = new THREE.DirectionalLight();
        directionalLight.position.set(-0.3, 0.6, -0.8).normalize(0xb8b8b8);
        directionalLight.intensity = 0.45;
        WSI.CurrentEngine.SCENE.add(directionalLight);

    }
    threePointLight();

    var _geo_pivot = new THREE.Object3D();

    // Only one instance of object
    var _geo = new THREE.BoxGeometry(1, 1, 1);
    var _mat = new THREE.MeshBasicMaterial({ color: 0xff0000 });
    var _cub = new THREE.Mesh(_geo, _mat);

    PrepareMaterialsPerLegend(_mat);

    var _counter = 0;

    // Create some models! 2500
    for (var _iz = -35; _iz < 35; _iz++) {
        for (var _ix = -35; _ix < 35; _ix++) {
            // Cloned instance - same geometry
            var _instance = _cub.clone();
            _instance.position.set(_ix * 2, 0, _iz * 2);

            _geo_pivot.add(_instance);

            _my_data.objects.push(_instance);
            _my_data.objectsStates.push(0);

            _counter++;
        }
    }

    _my_data.maxItems = _counter - 1;

    //this.SCENE.add(new THREE.Mesh( _full_mesh, new THREE.MeshFaceMaterial( _full_materials ) ));

    WSI.CurrentEngine.SCENE.add(_geo_pivot);

    console.log("Scene info: objects = " + _counter);

}

function ChangeObjectStates() {
    var _start = new Date().getTime(),
        _end;
    for (var _i = 0; _i < _my_data.objects.length; _i++) {
        _my_data.objectsStates[_i] = getRandomInt(0, _my_data.legendMaterials.length - 1);
    }
    _end = new Date().getTime();
    _my_data.changeEllapsedTime = _end - _start;
    console.log("Ellapsed time changing states: " + new Date(_my_data.changeEllapsedTime).getMilliseconds());
    _my_data.IntervalMaster = null;
    StartStateChanges();
}

function StartStateChanges() {
    if (_my_data.IntervalMaster == null) {
        _my_data.IntervalMaster = setTimeout(ChangeObjectStates, 5000);
    } else {
        alert("State change already started!");
    }
}

function StopStateChanges() {
    if (_my_data.IntervalMaster != null) {
        clearTimeout(_my_data.IntervalMaster);
    } else {
        alert("State change not started!");
    }
}

// Create materials buffer
function PrepareMaterialsPerLegend(DefaultMaterial) {

    if (DefaultMaterial) {
        // initial state, one material, one state
        _my_data.legendMaterials[0] = DefaultMaterial;
    }

    for (var _i = 1; _i < 10; _i++) {
        // We create 9 materials, one for each legend item
        _my_data.legendMaterials.push(new THREE.MeshLambertMaterial({ color: getRandomColor() }));
    }

}

window.onbeforeunload = function () {
    if (_mydata.IntervalMaster != null) {
        StopStateChanges();
    }
}