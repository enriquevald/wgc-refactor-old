﻿
var _my_data = {
  isObjectInScene: false,
  theObject: null,
  theCube: null
};

function OnObjectAdded() {
  RotateModel();
  ScaleModel();
  TranslateModel();

  WSI.CurrentInspector.Level = [];
  WSI.CurrentInspector.Update(_my_data.theObject);
}

function empty_scene_render() {

  /*
   * Use WSI.CurrentEngine.CURRENT_SCENE_PRE_RENDER
   * to set the function used to update the scene 
   */

  if (WSI.CurrentEngine.CURRENT_SCENE_PRE_RENDER == null) {

    WSI.CurrentEngine.CURRENT_SCENE_PRE_RENDER = function () {

      if (!_my_data.isObjectInScene) {
        if (WSI.CurrentResources.Items['main_model'] && WSI.CurrentResources.Items['main_model'].Loaded) {
          WSI.CurrentEngine.SCENE.remove(_my_data.theObject);

          _my_data.theObject = new THREE.Mesh(WSI.CurrentResources.Items['main_model'].object.geometry, WSI.CurrentResources.Items['main_model'].object.material);

          WSI.CurrentEngine.SCENE.add(_my_data.theObject);

          OnObjectAdded();

          _my_data.isObjectInScene = true;
        }
      }

    }

  }


  /*
   * Available objects:
   * WSI.CurrentEngine
   * WSI.CurrentResources
   * WSI.CurrentHelpers
   * WSI.CurrentLighting
   */

  var _ambient_light = WSI.CurrentLighting.Create("AMBIENT", EN_LIGHT_TYPES.LT_AMBIENT, { Color: 0xffffff });

  var _point_light;

  _point_light = WSI.CurrentLighting.Create("POINT1", EN_LIGHT_TYPES.LT_POINT, { Color: "#606060", Intensity: 0.5, Distance: 50 });
  _point_light.position.set(0, 4, 4);

  WSI.CurrentHelpers.Create("GRID", { Size: 10, LineSpace: 1 });
  WSI.CurrentHelpers.Create("AXIS", { Size: 2 });
  WSI.CurrentHelpers.LightHelpers(true);

  // Add a cube for reference space
  var _cube_place_geo = new THREE.BoxGeometry(4, 4, 4),
      _cube_place_mat = new THREE.MeshBasicMaterial({ color: 0xffff00, wireframe: true });
  _my_data.theCube = new THREE.Mesh(_cube_place_geo, _cube_place_mat);
  _my_data.theCube.position.set(0, 2, 0);
  WSI.CurrentEngine.SCENE.add(_my_data.theCube);
}

function LoadTexture(Event) {
  var _res = WSI.CurrentResources.Items['main_model'],
      _res_name = $(Event.target).data("res-name");

  WSI.CurrentResources.AddFromFile(EN_RESOURCE_TYPES.RT_TEXTURE, _res_name, Event, function () {
    WSI.CurrentResources.Items['main_model'].Loaded = false;
    WSI.CurrentResources.Load(function () {
      _my_data.isObjectInScene = false;
    });
  });

}

function UpdateTexturesPanel() {
  var $panel = $("#texturesInModel"),
      _res = WSI.CurrentResources.Items['main_model'];
  $panel.empty();
  
  for (var _txt in _res.MaterialBinding) {
    $panel.append($('<div>' + _txt + ': <input type="file" value="" onchange="LoadTexture(event);" data-res-name="' + _res.MaterialBinding[_txt] + '"/><input type="button" value="From DB..." /><hr /></div>'));
  }
}

function LoadMainModel(Event) {
  WSI.CurrentResources.Items = [];
  WSI.CurrentResources.AddFromFile(EN_RESOURCE_TYPES.RT_GEOMETRY, 'main_model', Event, function () {
    WSI.CurrentResources.Load();
    UpdateTexturesPanel();
    _my_data.isObjectInScene = false;
  });
}

$(document).ready(function () {

  // Prepare controls
  var _str = "";

  _str = _str + '<div id="ControlsAccordion">';
  _str = _str + '<h3>Select model</h3>';
  _str = _str + '<div>';
  _str = _str + '<input type="file" accept="*,.js,.obj" onchange="LoadMainModel(event);"/>';
  _str = _str + '</div>';
  _str = _str + '<h3>Helpers</h3>';
  _str = _str + '<div>';
  _str = _str + '<input type="button" value="Show/Hide Lights" onclick="WSI.CurrentHelpers.LightHelpers(!WSI.CurrentHelpers.LightsState);"/>';
  _str = _str + '<input type="button" value="Show/Hide Grid" onclick="WSI.CurrentHelpers.Switch(\'GRID\');"/>';
  _str = _str + '<input type="button" value="Show/Hide Axis" onclick="WSI.CurrentHelpers.Switch(\'AXIS\');"/>';
  _str = _str + '<input type="button" value="Show/Hide Cube" onclick="_my_data.theCube.visible = !_my_data.theCube.visible;"/>';
  _str = _str + '</div>';
  _str = _str + '<h3>Textures</h3>';
  _str = _str + '<div>';
  _str = _str + '<div id="texturesInModel">None</div>';
  _str = _str + '</div>';
  _str = _str + '<h3>Translate</h3>';
  _str = _str + '<div>';
  _str = _str + 'X:<div id="translateX"></div><input id="translateXValue" value="0" /><br>';
  _str = _str + 'Y:<div id="translateY"></div><input id="translateYValue" value="0" /><br>';
  _str = _str + 'Z:<div id="translateZ"></div><input id="translateZValue" value="0" /><br>';
  _str = _str + '</div>';
  _str = _str + '<h3>Scale</h3>';
  _str = _str + '<div>';
  _str = _str + 'X:<div id="scaleX"></div><input id="scaleXValue" value="1" /><br>';
  _str = _str + 'Y:<div id="scaleY"></div><input id="scaleYValue" value="1" /><br>';
  _str = _str + 'Z:<div id="scaleZ"></div><input id="scaleZValue" value="1" /><br>';
  _str = _str + '</div>';
  _str = _str + '<h3>Rotation</h3>';
  _str = _str + '<div>';
  _str = _str + 'X:<div id="rotationX"></div><input id="rotationXValue" value="0" /><br>';
  _str = _str + 'Y:<div id="rotationY"></div><input id="rotationYValue" value="0" /><br>';
  _str = _str + 'Z:<div id="rotationZ"></div><input id="rotationZValue" value="0" /><br>';
  _str = _str + '</div>';
  _str = _str + '<h3>Ambient Light</h3>';
  _str = _str + '<div>';
  _str = _str + '<input id="ambientLightColor" type="text" value="#ffffff" style="background-color: #ffffff;" />';
  _str = _str + '</div>';
  _str = _str + '<h3>Point Light</h3>';
  _str = _str + '<div>';
  _str = _str + '<input type="button" value="On/Off Light" onclick="WSI.CurrentLighting.SwitchByName(\'POINT1\');"/>';
  _str = _str + '<div id="pointLightIntensity"></div>';
  _str = _str + '<input id="pointLightIntensityValue" type="text" value="0.2" readonly="readonly"/>';
  _str = _str + '<input id="pointLightColor" type="text" value="#606060" style="background-color: #606060;" />';
  _str = _str + '</div>';
  _str = _str + '<h3>Database</h3>';
  _str = _str + '<div>';
  _str = _str + 'Type:<select id="selResourceType">';
  _str = _str + '<option value="1">Terminal</option>';
  //_str = _str + '<option value="2">Gaming-Table</option>';
  //_str = _str + '<option value="3">Gaming-Table Seat</option>';
  //_str = _str + '<option value="20">3D Camera</option>';
  _str = _str + '<option value="70">Beacon</option>';
  //_str = _str + '<option value="100">SmartPhone</option>';
  _str = _str + '<option value="200">Player-Male</option>';
  _str = _str + '<option value="201">Player-Female</option>';
  _str = _str + '<option value="500">Runner</option>';
  _str = _str + '</select>';
  _str = _str + '<input id="txtMeshName" type="text" value="MeshName"/>';
  _str = _str + '<input type="button" value="Save" onclick="SaveToDatabase();"/>';
  _str = _str + '</div>';
  _str = _str + '</div>';

  $("#divControls").append($(_str));
  $("#ControlsAccordion").accordion({
    heightStyle: "content"
  });

  $("#ambientLightColor").change(function (event) {
    var _new_color = $(event.target).val();
    WSI.CurrentLighting.Lights["AMBIENT"].color = new THREE.Color(_new_color);
    $(event.target).css('background-color', _new_color);
  });

  $("#pointLightColor").change(function (event) {
    var _new_color = $(event.target).val();
    WSI.CurrentLighting.Lights["POINT1"].color = new THREE.Color(_new_color);
    WSI.CurrentHelpers.Lights["POINT1"].update();
    $(event.target).css('background-color', _new_color);
  });

  $("#pointLightIntensity").slider({
    value: 2,
    min: 0,
    max: 50,
    step: 1,
    slide: function (event, ui) {
      WSI.CurrentLighting.Lights["POINT1"].intensity = ui.value / 10;
      $("#pointLightIntensityValue").val(ui.value / 10);
    }
  });

  $("#rotationXValue").change(function (Event) {
    $("#rotationX").slider("value", Event.target.value);
    RotateModel();
  });

  $("#rotationX").slider({
    value: 0,
    min: 0,
    max: 359,
    step: 1,
    slide: function (event, ui) {
      RotateModel();
      $("#rotationXValue").val(ui.value);
    }
  });

  $("#rotationYValue").change(function (Event) {
    $("#rotationY").slider("value", Event.target.value);
    RotateModel();
  });

  $("#rotationY").slider({
    value: 0,
    min: 0,
    max: 359,
    step: 1,
    slide: function (event, ui) {
      RotateModel();
      $("#rotationYValue").val(ui.value);
    }
  });

  $("#rotationZValue").change(function (Event) {
    $("#rotationZ").slider("value", Event.target.value);
    RotateModel();
  });

  $("#rotationZ").slider({
    value: 0,
    min: 0,
    max: 359,
    step: 1,
    slide: function (event, ui) {
      RotateModel();
      $("#rotationZValue").val(ui.value);
    }
  });

  $("#scaleXValue").change(function (Event) {
    $("#scaleX").slider("value", Event.target.value * 10);
    ScaleModel();
  });

  $("#scaleX").slider({
    value: 10,
    min: 1,
    max: 100,
    step: 1,
    slide: function (event, ui) {
      ScaleModel();
      $("#scaleXValue").val(ui.value / 10);
    }
  });

  $("#scaleYValue").change(function (Event) {
    $("#scaleY").slider("value", Event.target.value * 10);
    ScaleModel();
  });

  $("#scaleY").slider({
    value: 10,
    min: 1,
    max: 100,
    step: 1,
    slide: function (event, ui) {
      ScaleModel();
      $("#scaleYValue").val(ui.value / 10);
    }
  });

  $("#scaleZValue").change(function (Event) {
    $("#scaleZ").slider("value", Event.target.value * 10);
    ScaleModel();
  });

  $("#scaleZ").slider({
    value: 10,
    min: 1,
    max: 100,
    step: 1,
    slide: function (event, ui) {
      ScaleModel();
      $("#scaleZValue").val(ui.value / 10);
    }
  });

  $('#translateXValue').change(function (Event) {
    $("#translateX").slider("value", Event.target.value * 10);
    TranslateModel();
  });

  $("#translateX").slider({
    value: 0,
    min: 0,
    max: 100,
    step: 1,
    slide: function (event, ui) {
      TranslateModel();
      $("#translateXValue").val(ui.value / 10);
    }
  });

  $('#translateYValue').change(function (Event) {
    $("#translateY").slider("value", Event.target.value * 10);
    TranslateModel();
  });

  $("#translateY").slider({
    value: 0,
    min: 0,
    max: 100,
    step: 1,
    slide: function (event, ui) {
      TranslateModel();
      $("#translateYValue").val(ui.value / 10);
    }
  });

  $('#translateZValue').change(function (Event) {
    $("#translateZ").slider("value", Event.target.value * 10);
    TranslateModel();
  });

  $("#translateZ").slider({
    value: 0,
    min: 0,
    max: 100,
    step: 1,
    slide: function (event, ui) {
      TranslateModel();
      $("#translateZValue").val(ui.value / 10);
    }
  });

  $("input").keypress(function (Event) {
    if (Event.key == "Enter") {
      $(Event.target).change();
    }
  });
});

function ScaleModel() {
  if (_my_data.theObject == null) { return; }

  var _scl_x = $('#scaleX').slider("option", "value"),
      _scl_y = $('#scaleY').slider("option", "value"),
      _scl_z = $('#scaleZ').slider("option", "value");

  _my_data.theObject.scale.set(_scl_x / 10, _scl_y / 10, _scl_z / 10);
}

function RotateModel() {
  if (_my_data.theObject == null) { return; }

  var _rot_x = $('#rotationX').slider("option", "value"),
      _rot_y = $('#rotationY').slider("option", "value"),
      _rot_z = $('#rotationZ').slider("option", "value");

  _my_data.theObject.rotation.set(_rot_x * Math.PI / 180, _rot_y * Math.PI / 180, _rot_z * Math.PI / 180, "YXZ");
}

function TranslateModel() {
  if (_my_data.theObject == null) { return; }

  var _trn_x = $('#translateXValue').val(),
      _trn_y = $('#translateYValue').val(),
      _trn_z = $('#translateZValue').val();

  _my_data.theObject.position.set(_trn_x, _trn_y, _trn_z);
}

function SaveToDatabase() {
  var _resource_list = [],
      _model = {};

  _model.type = EN_RESOURCE_TYPES.RT_GEOMETRY;
  _model.subtype = $("#selResourceType").val();
  _model.data = $.base64.encode(JSON.stringify(JSON.parse(WSI.CurrentResources.Items['main_model'].Uri))); //JSON.stringify(_my_data.theObject);
  _model.name = $("#txtMeshName").val();
  _model.options = $.base64.encode(JSON.stringify({initialPosition:{x:$('#translateXValue').val(),y:$('#translateYValue').val(),z:$('#translateZValue').val()},initialRotation:{x:$('#rotationXValue').val(),y:$('#rotationYValue').val(),z:$('#rotationZValue').val()},initialScale: {x:$('#scaleXValue').val(),y:$('#scaleYValue').val(),z:$('#scaleZValue').val()}}));

  _resource_list.push(_model);

  var _resource,
      _new_resource;

  for (var _idx in WSI.CurrentResources.Items) {
    _resource = WSI.CurrentResources.Items[_idx];
    if (_resource.Key && _resource.Key != 'main_model') {
      _new_resource = {
        type: EN_RESOURCE_TYPES.RT_TEXTURE,
        subtype: 0,
        data: $.base64.encode(_resource.Uri),
        name: _resource.Key,
        options: null
      };

      _resource_list.push(_new_resource);
    }
  }
  
  try {
    PageMethods.SaveResources(JSON.stringify(_resource_list, null), onSaveCompleted, onSaveError);
  } catch (e) {
    alert(e.message);
  }
  
  return;
}

function onSaveCompleted(Result) {
  alert("Saved!")
}

function onSaveError(Result) {
  alert(Result._message);
}