﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using Newtonsoft.Json;
using WSI.Common;
using System.Data.SqlClient;
using WSI.Layout;
using WSI.Layout.classes;

namespace WSIModelsIO
{
  public partial class _default : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    //
    private void Page_PreLoad(object sender, EventArgs e)
    {
      // Check
      if (Global.Sessions.Information[Session.SessionID].User.Logged)
      {
        // Load Security
        Global.Sessions.Information[Session.SessionID].User.Permissions = new LayoutUserPermissions(Global.Sessions.Information[Session.SessionID].User.Id);

      }
      else
      {
        // Not logged
        Logger.Write(HttpContext.Current, "MODELSIO_PRELOAD | Not logged in!");

        // Redirect to Login
        Response.Redirect("../Login.aspx?s=" + clsStrings.EncodeTo64("modelsio"));
      }
    }

    [WebMethod(), ScriptMethod()]
    public static String SaveResources(String ResourceList)
    {
      return Resources.SaveResources(ResourceList);
    }

  }
}