﻿
function ObjToString(Obj, Separator) {
    Separator = (!Separator) ? ";" : Separator;
    var _result = [];
    for (var _ip in Obj) {
        if (typeof Obj[_ip] != "function") {
            _result.push(_ip + ':' + Obj[_ip]);
        }
    }
    return _result.join(Separator);
}

function getRandomColor() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}