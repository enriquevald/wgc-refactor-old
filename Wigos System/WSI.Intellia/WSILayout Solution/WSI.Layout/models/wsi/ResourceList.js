﻿if (!WSI) {
  var WSI = {
    CurrentResources: null
  }
} else {
  WSI.CurrentResources = null;
}

WSI.ResourceListItem = function (Type, Key, Uri) {
  this.Type = Type;
  this.Uri = Uri;
  this.Key = Key;
  this.Loaded = false;
  this.FromFile = false;
  this.FullPath = null;
}

WSI.ResourceList = function () {

  if (WSI.CurrentResources != null) {
    console.log('Alredy an instance of the resource list is created!');
    return;
  }

  WSI.CurrentResources = this;

  this.WaitWhileLoading = true;
  this.ThrowProgress = true;
  this.Items = [];
  this.NumItems = 0;
  this.LoadReport = {};
  this.Lock = true;
  this.CheckTime = 200;
}

WSI.ResourceList.prototype.AddItem = function (ReourceListItem) {
  WSI.CurrentResources.Items[ReourceListItem.Key] = ReourceListItem;
  WSI.CurrentResources.NumItems++;
}

WSI.ResourceList.prototype.AddFromFile = function (ResourceType, ResourceName, EventData, callBackFileLoadComplete) {
  var reader = new FileReader(),
      _resource;

  function LoadGeometry() {
    reader.onload = (function (theFile) {
      return function (e) {
        _resource = new WSI.ResourceListItem(EN_RESOURCE_TYPES.RT_GEOMETRY, ResourceName, e.target.result);
        _resource.FromFile = true;
        _resource.FullPath = $(EventData.target).val();
        _resource.FullPath = _resource.FullPath.substring(0, _resource.FullPath.lastIndexOf("\\"));
        WSI.CurrentResources.AddItem(_resource);
        if (callBackFileLoadComplete) {
          callBackFileLoadComplete(_resource);
        }
      };
    })();

    reader.readAsText(EventData.target.files[0]);
  }

  function LoadTexture() {
    var $img = $(new Image());
    reader.onload = (function (theFile) {
      return function (e) {
        _resource = new WSI.ResourceListItem(EN_RESOURCE_TYPES.RT_TEXTURE, ResourceName, e.target.result);
        _resource.Uri = e.target.result;
        _resource.FromFile = true;
        _resource.FullPath = $(EventData.target).val();
        _resource.FullPath = _resource.FullPath.substring(0, _resource.FullPath.lastIndexOf("\\"));
        WSI.CurrentResources.AddItem(_resource);
        if (callBackFileLoadComplete) {
          callBackFileLoadComplete(_resource);
        }
      };
    })();

    reader.readAsDataURL(EventData.target.files[0]);
  }

  switch (ResourceType) {
    case EN_RESOURCE_TYPES.RT_GEOMETRY:
      LoadGeometry();
      break;
    case EN_RESOURCE_TYPES.RT_TEXTURE:
      LoadTexture();
      break;
  }

}

WSI.ResourceList.prototype.Load = function (OnCompleteCallBack, OnProgressCallback) {
  var _that = WSI.CurrentResources;

  _that.LoadReport = {
    Textures: 0,
    TexturesSize: 0,
    TexturesErrors: 0,
    Geometries: 0,
    GeometriesSize: 0,
    GeometriesVertices: 0,
    GeometriesFaces: 0
  };

  if (_that.Lock) {
    var dialog = $('<p>Loading...</p>').dialog({
      dialogClass: "no-close",
      title: 'Resource List',
      modal: true,
      buttons: {}
    });
  }

  if (_that.WaitWhileLoading) {
    var _check_interval = setInterval(function () {
      var _all_loaded = true;
      for (var _ir in _that.Items) {
        if (!_that.Items[_ir].Loaded) {
          _all_loaded = false;
          break;
        }
      }
      if (_all_loaded) {
        if (OnCompleteCallBack) {
          OnCompleteCallBack();
        };
        clearInterval(_check_interval);
        if (dialog) {
          dialog.dialog("close");
        }
      }
    }, _that.CheckTime);
  }

  var _num_items = _that.NumItems;
  var _loaded_count = 0;

  var _json_parser = new THREE.JSONLoader();

  // Function to show progress
  function OnItemLoaded(ResourceItem) {
    _loaded_count++;
    if (_that.ThrowProgress && OnProgressCallback) { OnProgressCallback(ResourceItem, _loaded_count, _num_items); }
  }

  // Function to bind materials and textures
  function BindMaterialsAndTextures(Resource, Data) {

    for (var _im in Data.materials) {
      var _mat = Data.materials[_im];
      if (_mat.mapDiffuse) {
        if (!Resource.MaterialBinding) {
          Resource.MaterialBinding = [];
        }
        Resource.MaterialBinding[_mat.DbgName] = _mat.mapDiffuse;
      }
    }

  }

  // Function to parse materials
  function ParseMaterials(Resource, Data) {

    for (var _im in Data.materials) {
      var _mat = Data.materials[_im];

      if (Resource.MaterialBinding) {
        if (Resource.MaterialBinding[_mat.name]) {
          if (_that.Items[Resource.MaterialBinding[_mat.name]]) {
            _mat.map = _that.Items[Resource.MaterialBinding[_mat.name]].object;
            _mat.map.name = Resource.MaterialBinding[_mat.name];
            _mat.needsUpdate = true;
          } else {
            _that.LoadReport.TexturesErrors += 1;
          }
        }
      }
    }

  }

  // Function that converts geometry json string into a geometry object
  function LoadGeometry(ResourceItem) {
    var _texture_path;
    var _json = JSON.parse(ResourceItem.Uri);

    BindMaterialsAndTextures(ResourceItem, _json);

    var _geometry = _json_parser.parse(_json, _texture_path);

    ParseMaterials(ResourceItem, _geometry);

    ResourceItem["object"] = new THREE.Mesh(_geometry.geometry, new THREE.MeshFaceMaterial(_geometry.materials));
    ResourceItem["texturePath"] = _texture_path;
    ResourceItem.Loaded = true;

    _that.LoadReport.Geometries++;
    _that.LoadReport.GeometriesSize += ResourceItem.Uri.length;
    _that.LoadReport.GeometriesVertices += _geometry.geometry.vertices.length;
    _that.LoadReport.GeometriesFaces += _geometry.geometry.faces.length;

    OnItemLoaded(ResourceItem);
  }

  // Function that converts a datauri into a three.texture
  function LoadTexture(ResourceItem) {
    var _image = new Image();
    _image.src = ResourceItem.Uri;
    ResourceItem["object"] = new THREE.Texture(_image);
    ResourceItem["object"].needsUpdate = true;
    ResourceItem.Loaded = true;

    _that.LoadReport.Textures++;
    _that.LoadReport.TexturesSize += ResourceItem.Uri.length;

    OnItemLoaded(ResourceItem);
  }

  //
  function GetByType(ResourceType) {
    var _result = [];
    for (var _r in _that.Items) {
      if (!_that.Items[_r].Loaded && _that.Items[_r].Type == ResourceType) {
        _result.push(_that.Items[_r]);
      }
    }
    return _result;
  }

  //
  var _sorted_resources = [];
  _sorted_resources.push(GetByType(EN_RESOURCE_TYPES.RT_TEXTURE));
  _sorted_resources.push(GetByType(EN_RESOURCE_TYPES.RT_GEOMETRY));

  for (var _rt in _sorted_resources) {
    var _items = _sorted_resources[_rt];
    for (var _ir in _items) { // foreach resource 
      var _resource = _items[_ir];

      if (!_resource.Loaded) {

        switch (_resource.Type) {
          case EN_RESOURCE_TYPES.RT_GEOMETRY:
            LoadGeometry(_resource);
            break;

          case EN_RESOURCE_TYPES.RT_TEXTURE:
            LoadTexture(_resource);
            break;

          default:
            // Error.... no type??
            break;

        }
      }

    }
  }
}
