﻿if (!WSI) {
  var WSI = {
    CurrentHelpers: null
  }
} else {
  WSI.CurrentHelpers = null;
}

WSI.Helpers = function () {
  if (WSI.CurrentHelpers != null) {
    console.log("Helpers already initialized!");
    return;
  }

  WSI.CurrentHelpers = this;

  this.Objects = {
    GRID: null,
    AXIS: null,
    TRANSFORM: null
  };
 
  this.LightsState = false;
  this.Lights = {};
}

WSI.Helpers.prototype.Create = function (HelperName, Parameters) {
  var _new_helper;

  if (WSI.CurrentEngine.SCENE && WSI.CurrentHelpers.Objects[String(HelperName).toUpperCase()] == null) {
    switch (String(HelperName).toUpperCase()) {
      case "GRID":
        WSI.CurrentHelpers.Objects["GRID"] = new THREE.GridHelper(Parameters["Size"] ? Parameters["Size"] : 10, Parameters["LineSpace"] ? Parameters["LineSpace"] : 1);
        _new_helper = WSI.CurrentHelpers.Objects["GRID"];
        break;
      case "AXIS":
        WSI.CurrentHelpers.Objects["AXIS"] = new THREE.AxisHelper(Parameters["Size"] ? Parameters["Size"] : 2);
        _new_helper = WSI.CurrentHelpers.Objects["AXIS"];
        break;
      default:
        return null;
        break;
    }
  } else {
    console.log('No engine to create ')
  }

  if (_new_helper && WSI.CurrentEngine.SCENE) {
    WSI.CurrentEngine.SCENE.add(_new_helper);
  }
}

WSI.Helpers.prototype.RemoveFromScene = function (HelperName) {
  if (WSI.CurrentEngine.SCENE) {
    if (WSI.CurrentHelpers.Objects[String(HelperName).toUpperCase()]) {
      WSI.CurrentEngine.SCENE.remove(WSI.CurrentHelpers.Objects[String(HelperName).toUpperCase()]);
    }
  }
}

WSI.Helpers.prototype.Show = function (HelperName) {
  WSI.CurrentHelpers.Objects[String(HelperName).toUpperCase()].visible = true;
}

WSI.Helpers.prototype.Hide = function (HelperName) {
  WSI.CurrentHelpers.Objects[String(HelperName).toUpperCase()].visible = false;
}

WSI.Helpers.prototype.Switch = function (HelperName) {
  WSI.CurrentHelpers.Objects[String(HelperName).toUpperCase()].visible = !WSI.CurrentHelpers.Objects[String(HelperName).toUpperCase()].visible;
}

WSI.Helpers.prototype.LightHelpers = function (State) {
  if (WSI.CurrentEngine.SCENE && WSI.CurrentLighting) {
    for (var _light_name in WSI.CurrentLighting.Lights) {
      var _light = WSI.CurrentLighting.Lights[_light_name],
        _light_helper;
      
      if (!WSI.CurrentHelpers.Lights[_light_name]) {
        switch (_light.userData.type) {
          case EN_LIGHT_TYPES.LT_AMBIENT:
            continue;
            break;
          case EN_LIGHT_TYPES.LT_AREA:
            continue;
            break;
          case EN_LIGHT_TYPES.LT_DIRECTIONAL:
            _light_helper = new THREE.DirectionalLightHelper(_light, 0.2);
            break;
          case EN_LIGHT_TYPES.LT_HEMISPHERE:
            _light_helper = new THREE.HemisphereLightHelper(_light, 0.2);
            break;
          case EN_LIGHT_TYPES.LT_POINT:
            _light_helper = new THREE.PointLightHelper(_light, 0.2);
            break;
          case EN_LIGHT_TYPES.LT_SPOT:
            _light_helper = new THREE.SpotLightHelper(_light, 0.2);
            break;
          default:
            continue;
            break;
        }
        WSI.CurrentHelpers.Lights[_light_name] = _light_helper;
        WSI.CurrentEngine.SCENE.add(WSI.CurrentHelpers.Lights[_light_name]);
      } else {
        _light_helper = WSI.CurrentHelpers.Lights[_light_name];
      }
      
      if (_light_helper) {
        _light_helper.visible = State;
      }
    }
    WSI.CurrentHelpers.LightsState = State;
  }
}
