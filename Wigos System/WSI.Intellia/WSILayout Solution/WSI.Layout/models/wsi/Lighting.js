﻿var EN_LIGHT_TYPES = {
  LT_AMBIENT: 0,
  LT_AREA: 1,
  LT_DIRECTIONAL: 2,
  LT_HEMISPHERE: 3,
  LT_POINT: 4,
  LT_SPOT: 5
};

if (!WSI) {
  var WSI = {
    CurrentLighting: null
  }
} else {
  WSI.CurrentLighting = null;
}

WSI.Lighting = function () {
  if (WSI.CurrentLighting != null) {
    console.log("Lighting already initialized!");
    return;
  }

  WSI.CurrentLighting = this;

  this.Lights = {};
}

WSI.Lighting.prototype.Create = function (LightName, LightType, Parameters) {

  LightName = String(LightName).toUpperCase();

  var _light = WSI.CurrentLighting.Lights[LightName];
  if (!_light) {
    switch (LightType) {
      case EN_LIGHT_TYPES.LT_AMBIENT:
        _light = new THREE.AmbientLight(Parameters["Color"]);
        break;
      case EN_LIGHT_TYPES.LT_AREA:
        break;
      case EN_LIGHT_TYPES.LT_DIRECTIONAL:
        break;
      case EN_LIGHT_TYPES.LT_HEMISPHERE:
        break;
      case EN_LIGHT_TYPES.LT_POINT:
        _light = new THREE.PointLight(Parameters["Color"], Parameters["Intensity"], Parameters["Distance"]);
        break;
      case EN_LIGHT_TYPES.LT_SPOT:
        //_light = new THREE.SportLight();
        break;
      default:
        break;
    }

    _light.userData.type = LightType;
    WSI.CurrentLighting.Lights[LightName] = _light;
  }

  if (_light && WSI.CurrentEngine.SCENE) {
    WSI.CurrentEngine.SCENE.add(WSI.CurrentLighting.Lights[LightName]);
  }

  return _light;
}

WSI.Lighting.prototype.SwitchByName = function (LightName, State) {
  if (State == undefined) {
    State = !WSI.CurrentLighting.Lights[LightName].visible;
  }
  WSI.CurrentLighting.Lights[LightName].visible = State;
}

WSI.Lighting.prototype.SwitchByType = function (LightType, State) {
  for (var _light_name in WSI.CurrentLighting.Lights) {
    if (WSI.CurrentLighting.Lights[_light_name].userData.type == LightType) {
      WSI.CurrentLighting.Lights[_light_name].visible = State;
    }
  }
}

