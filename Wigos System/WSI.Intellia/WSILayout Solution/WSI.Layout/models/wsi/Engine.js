﻿if (!WSI) {
    var WSI = { CurrentEngine: null }
} else {
    WSI.CurrentEngine = null;
};

WSI.Engine = function () {

    if (WSI.CurrentEngine != null) {
        alert('Alredy an instance of the engine is created!');
        return;
    }

    WSI.CurrentEngine = this;

    if (typeof jQuery === 'undefined') {
        throw "jQuery no available!";
        return undefined;
    }

    var _that = this;

    this.CURRENT_SCENE_BUILDER = null;

    this.CURRENT_CAMERA = null;

    this.SCREEN_WIDTH = window.innerWidth;
    this.SCREEN_HEIGHT = window.innerHeight;

    this.FLOOR = 0;

    this.SCENE = new THREE.Scene();

    if ($("#WSIEngineContainer").length == 0) {
        this.$Container = $("<div id='WSIEngineContainer'></div>");
        $(document.body).append(this.$Container);
    } else {
        this.$Container = $("#WSIEngineContainer");
    }

    var _webgl_renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
    //_webgl_renderer.setPixelRatio(window.devicePixelRatio);
    _webgl_renderer.setSize(this.SCREEN_WIDTH, this.SCREEN_HEIGHT);
    _webgl_renderer.domElement.style.position = "relative";
    _webgl_renderer.setClearColorHex(0x000000, 1);

    // Keep animation frame
    this.CURRENT_ANIMATION_FRAME = 0;

    //
    _webgl_renderer.context.canvas.addEventListener("webglcontextlost", function (event) {
        //event.preventDefault();
        //cancelAnimationFrame(_that.CURRENT_ANIMATION_FRAME);
        console.log("WebGL Context Loss [" + _that.CURRENT_ANIMATION_FRAME + "] " + event.statusMessage);
        //if (_that.OnWebGLContextRestored) { _that.OnWebGLContextRestored(); }
    }, false);

    //
    _webgl_renderer.context.canvas.addEventListener("webglcontextrestored", function (event) {
        event.preventDefault();
        console.log("WebGL Context Restored");
        //if (_that.OnWebGLContextRestored) { _that.OnWebGLContextRestored(); }
    }, false);

    this.$Container[0].appendChild(_webgl_renderer.domElement);

    this.CURRENT_RENDERER = _webgl_renderer;

    this.CURRENT_SCENE_PRE_RENDER = null;
}

WSI.Engine.prototype.Init = function (Options) {

    var _that = this;

    // Event to control resize and adapt renderer to full screen
    function onWindowResize(event) {

        _that.SCREEN_WIDTH = window.innerWidth;
        _that.SCREEN_HEIGHT = window.innerHeight;
        _that.ASPECT = _that.$Container[0].offsetWidth / _that.$Container[0].offsetHeight;

        _that.CURRENT_RENDERER.setSize(_that.SCREEN_WIDTH, _that.SCREEN_HEIGHT);

        _that.CURRENT_CAMERA.ASPECT = _that.ASPECT;
        _that.CURRENT_CAMERA.updateProjectionMatrix();

    }

    // Initialization of Camera
    this.ASPECT = this.$Container[0].offsetWidth / this.$Container[0].offsetHeight;
    this.CURRENT_CAMERA = new THREE.PerspectiveCamera(45, this.ASPECT, 0.01, 200);
    this.CURRENT_CAMERA.position.z = 0;
    this.CURRENT_CAMERA.position.x = 0;
    this.CURRENT_CAMERA.position.y = 40;
    this.CURRENT_TARGET = new THREE.Vector3(0, 1, 0);
    this.CURRENT_CAMERA.lookAt(this.CURRENT_TARGET);
    this.CURRENT_CAMERA.updateProjectionMatrix();

    // Intializae default controls (Orbit)
    this.CURRENT_CONTROLS = new THREE.OrbitControls(this.CURRENT_CAMERA, this.CURRENT_RENDERER.domElement);
    this.CURRENT_CONTROLS.damping = 0.2;
    //this.CURRENT_CONTROLS.addEventListener("change", WSI.CurrentEngine.Render);

    // Initialize Stats monitor
    this.STATS = new Stats();
    this.STATS.domElement.style.position = 'absolute';
    this.STATS.domElement.style.top = '0px';
    this.STATS.domElement.style.right = '0px';
    this.$Container[0].appendChild(this.STATS.domElement);

    // Add on window resize event
    window.addEventListener('resize', onWindowResize, false);

}

// Setting method that builds the scene
WSI.Engine.prototype.SetSceneBuilder = function (SceneBuilderMethod) {
    if (!SceneBuilderMethod) {
        alert('No scene to render!');
    } else {
        this.CURRENT_SCENE_BUILDER = SceneBuilderMethod;
    }
}

WSI.Engine.prototype.BuildScene = function () {
    if (this.CURRENT_SCENE_BUILDER != null) {
        this.CURRENT_SCENE_BUILDER();
    } else {
        alert("Without Scene!!!");
    }
}

// Animation method
WSI.Engine.prototype.Animate = function () {
    var _that = WSI.CurrentEngine;
    _that.CURRENT_ANIMATION_FRAME = requestAnimationFrame(_that.Animate);
    _that.CURRENT_CONTROLS.update();
    _that.STATS.update();

    if (_that.CURRENT_SCENE_PRE_RENDER != null) {
        _that.CURRENT_SCENE_PRE_RENDER();
    }

    _that.Render();
}

// Render Method
WSI.Engine.prototype.Render = function () {
    var _that = WSI.CurrentEngine;
    _that.CURRENT_RENDERER.render(_that.SCENE, _that.CURRENT_CAMERA);
}