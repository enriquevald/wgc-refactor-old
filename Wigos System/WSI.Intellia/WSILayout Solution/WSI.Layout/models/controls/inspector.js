﻿if (!WSI) {
  var WSI = {
    CurrentInspector: null
  }
} else {
  WSICurrentInspector = null;
}



WSI.Inspector = function (ContainerId) {

  if (WSI.CurrentInspector != null) {
    console.log("Inspector already instantiated!");
    return;
  }

  WSI.CurrentInspector = this;

  this.Object = null;
  this.Level = [];
  this.$Container = $(ContainerId);
  this.onObjectChanged = undefined;
  this.onEvalSetCustom = undefined;
}

WSI.Inspector.prototype.Update = function (Object) {
  var _that = this,
      _str = '',
      _prop_type,
      _obj,
      _lname = '';

  if (Object) {
    _that.Object = Object;
    _that.Level = [];
  }

  _obj = _that.Object;

  function PrepareEval(SubValues, Value) {
    var _vals = SubValues.split('.'),
        _l = _vals.length,
        _result = '_that.Object',
        _float;
    for (var _i = 0; _i < _l; _i++) {
      _float = parseFloat(_vals[_i]);
      _result = _result + "[" + ((isNaN(_float)) ? "\"" + (_vals[_i] + "\"") : (_float)) + "]";
    }
    _result = _result + "=" + Value + ";";
    if (_that.onEvalSetCustom) {
      _result = _that.onEvalSetCustom(_result, _vals, Value);
    }
    return _result;
  }

  function ClearContainer() {
    if (_that.$Container) {
      _that.$Container.empty();
    }
  }

  function getCurrentLevel() {
    for (var _i in _that.Level) {
      _obj = _obj[_that.Level[_i]];
    }
    _lname = _that.Level.join('.');
  };

  ClearContainer();
  getCurrentLevel();

  if (_obj != undefined && _obj != null) {
    _str = _str + '<p>[root]';
    _str = _str + _lname;
    _str = _str + '</p>';
  }

  if (_lname != "") {
    if (WSI.CurrentInspector.Level.length > 1) {
      _str = _str + '<input type="button" value="Root" onclick="WSI.CurrentInspector.Root();" />';
    }
    _str = _str + '<input type="button" value="Back" onclick="WSI.CurrentInspector.Back();" />';
  }

  _str = _str + '<table>';

  if (_obj != undefined && _obj != null) {

    if (typeof _obj === "object") {

      for (var i in _obj) {

        _prop_type = typeof _obj[i];

        if (_prop_type === "function") { continue; }

        _str = _str + '<tr>';

        _str = _str + '<td>' + i;
        _str = _str + '</td>';

        _str = _str + '<td>';
        switch (_prop_type) {
          case "object":
            if (_obj[i] === "undefined" || _obj[i] == null) {
              _str = _str + '[NOT_SET]';
            } else {
              _str = _str + '<input type="button" value="inspect" onclick="WSI.CurrentInspector.Level.push(\'' + i + '\'); WSI.CurrentInspector.Update();" />';
            }
            break;
          case "boolean":
            _str = _str + '<input class="propCheck" type="checkbox"';
            _str = _str + ((_obj[i]) ? ' checked="checked"' : '');
            _str = _str + ' data-property="' + [_lname , i].join('.') + '" data-type="' + _prop_type + '"/>';
            break;
          case "text":
          default:
            _str = _str + '<input class="propEdit" type="text" value="' + _obj[i] + '" data-property="' + [_lname, i].join('.') + '" data-type="' + _prop_type + '"/>';
            break;
        }
        _str = _str + '</td>';
         
        _str = _str + '</tr>';

      }



    } else {
      // Simple variable of function...
    }

  } else {
    
    _str = _str + '<tr><td colspan="2">Object not set</td></tr>';

  }
  _str = _str + '</table>';

  _that.$Container.append($(_str));

  $(".propEdit").keypress(function (Event) {
    var _props,
        _value,
        _float,
        _target;

    switch (true) {
      case (Event.key == "Enter"):
      case (Event.keyCode == 13):
        _props = $(Event.target).data("property");
        _value = $(Event.target).val();
        
        _float = parseFloat(_value);

        eval(PrepareEval(_props, (isNaN(_float)) ? _value : _float));

        if (_that.OnObjectChanged) {
          _that.OnObjectChanged();
        }
        break;
      case (Event.key == "Escape"):
      case (Event.keyCode == 27):
        _props = $(Event.target).data("property");
        $(Event.target).val(WSI.CurrentInspector.Object[_props])
        break;
    }
  });

  $(".propCheck").change(function (Event) {
    _props = $(Event.target).data("property");
    _value = $(Event.target).is(':checked');
    eval(PrepareEval(_props, _value));
    if (_that.OnObjectChanged) {
      _that.OnObjectChanged();
    }
  });
}

WSI.Inspector.prototype.Back = function () {
  WSI.CurrentInspector.Level.pop();
  WSI.CurrentInspector.Update();
}

WSI.Inspector.prototype.Root = function () {
  delete WSI.CurrentInspector.Level;
  WSI.CurrentInspector.Level = [];
  WSI.CurrentInspector.Update();
}
