﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="modelsio.aspx.cs" Inherits="WSIModelsIO._default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <title>WSI Layout Models I/O</title>

  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />

  <link type="text/css" rel="stylesheet" href="css/jquery-ui.css" />
  <link type="text/css" rel="stylesheet" href="css/wsimodelsio.css" />

  <script type="text/javascript" src="jquery/jquery-1.11.3.js"></script>
  <script type="text/javascript" src="jquery/jquery-ui.js"></script>
  <script type="text/javascript" src="/js/jquery.base64.js"></script>

  <script type="text/javascript" src="threejs/three.js"></script>
  <script type="text/javascript" src="threejs/OrbitControls.js"></script>
  <script type="text/javascript" src="threejs/stats.min.js"></script>

  <script type="text/javascript" src="controls/inspector.js"></script>

  <script type="text/javascript" src="wsi/Common.js"></script>
  <script type="text/javascript" src="wsi/Enums.js"></script>
  <script type="text/javascript" src="wsi/ResourceList.js"></script>
  <script type="text/javascript" src="wsi/Helpers.js"></script>
  <script type="text/javascript" src="wsi/Lighting.js"></script>
  <script type="text/javascript" src="wsi/Engine.js"></script>

  <%--<script type="text/javascript" src="scenes/defaultscene.js"></script>--%>
  <script type="text/javascript" src="scenes/emptyscene.js"></script>

</head>
<body>

  <%--<button onclick="PrepareMaterialsPerLegend();">Change Legend Colors</button>
  <button onclick="StartStateChanges();">Start Update Changes</button>
  <button onclick="StopStateChanges();">Stop Update Changes</button>--%>

  <div id="divControls"></div>
  <div id="divProperties">
    <div id="modelInspector"></div>
  </div> 

    <form id="form1" runat="server">
      <asp:ScriptManager ID="scmScripter" runat="server" EnablePageMethods="true">
          <Scripts>
          </Scripts>
      </asp:ScriptManager>
    <div>
    
    </div>
    </form>

    <script type="text/javascript">

        console.log("Starting...");

        //var _WSIEngine, _WSIResources;
        
        $(window).load(function () {

            console.log("Initializing rendering environment...");

            new WSI.Engine();

            WSI.CurrentEngine.Init();
                      
            new WSI.ResourceList();
            new WSI.Helpers();
            new WSI.Lighting();
            new WSI.Inspector("#modelInspector");

            WSI.CurrentInspector.onEvalSetCustom = function (EvalStr, SubValueList, Value) {
              var _result = 'if (',
                  _property = '',
                  _l = SubValueList.length - 1,
                  _float;
              for (var _i = 0; _i < _l; _i++) {
                _float = parseFloat(SubValueList[_i]);
                _property = _property + "[" + ((isNaN(_float)) ? "\"" + (SubValueList[_i] + "\"") : (_float)) + "]";
              }
              _result = _result + " _that.Object" + _property + ".needsUpdate) { ";
              _result = _result + " _that.Object" + _property + ".needsUpdate = true;"
              _result = _result + " } ";
              return _result + EvalStr;
            };

            WSI.CurrentInspector.Update();

            //var _resource = undefined;

            //_resource = new WSI.ResourceListItem(EN_RESOURCE_TYPES.RT_TEXTURE, "KOL.JPG", _car_machine_kol);
            //_WSIResources.AddItem(_resource);

            //_resource = new WSI.ResourceListItem(EN_RESOURCE_TYPES.RT_TEXTURE, "UP.JPG", _car_machine_up);
            //_WSIResources.AddItem(_resource);

            //_resource = new WSI.ResourceListItem(EN_RESOURCE_TYPES.RT_TEXTURE, "SCREEN.JPG", _car_machine_screen);
            //_WSIResources.AddItem(_resource);

            //_resource = new WSI.ResourceListItem(EN_RESOURCE_TYPES.RT_GEOMETRY, "CAR_MACHINE", _car_machine);
            //_WSIResources.AddItem(_resource);

            console.log("Loading resources...");

            WSI.CurrentResources.Load(function () {
                console.log("Done: " + ObjToString(WSI.CurrentResources.LoadReport));
                
              //WSI.CurrentEngine.SetSceneBuilder(defaultScene_Render);
                WSI.CurrentEngine.SetSceneBuilder(empty_scene_render);
                
                WSI.CurrentEngine.BuildScene();
                WSI.CurrentEngine.Animate();

            }, function (ItemLoaded, NumLoadeds, TotalToLoad) {
                if (ItemLoaded.Loaded) {

                }
                console.log("Loading resources: " + NumLoadeds + ' of ' + TotalToLoad);
            });
        });
    </script>

</body>
</html>
