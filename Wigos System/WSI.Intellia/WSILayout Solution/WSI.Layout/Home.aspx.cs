﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.Services;
using System.Web.Script.Services;

using WSI.Layout.classes;
using WSI.Layout.objects;
using System.Web.UI.HtmlControls;

using Newtonsoft.Json;
using System.IO;
using System.Web.Script.Serialization;

using WSI.Layout.classes.editor;
using WSI.Layout.Data.entities;

namespace WSI.Layout
{
  public partial class Home : System.Web.UI.Page
  {

    public Int32 ViewMode
    {
      get { return Global.Sessions.Information[HttpContext.Current.Session.SessionID].User.ViewMode; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
      // Including basic styles
      //LayoutPages.IncludeStyle(this, "jquery-ui_silver");
      //LayoutPages.IncludeStyle(this, "wsiLayout");
      //LayoutPages.IncludeStyle(this, "wsi-icon-tabs");
      //LayoutPages.IncludeStyle(this, "jquery.dd");

      //LayoutPages.IncludeStyle(this, "wsiLayout");
      // DESIGN 1
      LayoutPages.IncludeExternalStyle(this, "design/css/wsiLayout");
      LayoutPages.IncludeExternalStyle(this, "design/css/jquery-ui");
      LayoutPages.IncludeExternalStyle(this, "design/css/jquery.ui.multiprogressbar");
      LayoutPages.IncludeExternalStyle(this, "design/css/estilo");
      LayoutPages.IncludeExternalStyle(this, "design/css/fonts");
      LayoutPages.IncludeExternalStyle(this, "design/css/gridstack");
      LayoutPages.IncludeExternalStyle(this, "design/css/dashboard");
      LayoutPages.IncludeExternalStyle(this, "design/css/toastr8");
      LayoutPages.IncludeExternalStyle(this, "design/css/styles/nachoCSS");

      //
      //LayoutPages.IncludeJS(this, "libs/jquery");
      //LayoutPages.IncludeJS(this, "libs/jquery-ui");

      // DESIGN 1
      LayoutPages.IncludeExternalJS(this, "design/js/jquery-1.11.2.min");
      LayoutPages.IncludeExternalJS(this, "design/js/jquery.outerhtml");
      LayoutPages.IncludeExternalJS(this, "design/js/jquery-ui");
      LayoutPages.IncludeExternalJS(this, "design/js/jquery.ui.touch-punch");
      LayoutPages.IncludeExternalJS(this, "design/js/jquery.ui.multiprogressbar");
      LayoutPages.IncludeExternalJS(this, "design/js/progressbarVertical");
      LayoutPages.IncludeExternalJS(this, "design/js/js");
      LayoutPages.IncludeExternalJS(this, "design/js/wsiCharts");

      //
      // LayoutPages.IncludeJS(this, "libs/jquery.ui.touch-punch.min");
      // LayoutPages.IncludeJS(this, "ribbon/ribbon");
      //  LayoutPages.IncludeJS(this, "ribbon/jquery.tooltip.min");
      //  LayoutPages.IncludeJS(this, "libs/jquery.dd");
      LayoutPages.IncludeJS(this, "jquery.base64");
      LayoutPages.IncludeJS(this, "libs/hammerjs.min");
      LayoutPages.IncludeJS(this, "libs/spin");
      LayoutPages.IncludeJS(this, "libs/lodash");
      LayoutPages.IncludeJS(this, "libs/raphael.min");
      LayoutPages.IncludeJS(this, "libs/raphael.extra");

      LayoutPages.IncludeJS(this, "jtable/jquery.jtable");
      LayoutPages.IncludeJS(this, "jtablesite");
      LayoutPages.IncludeJS(this, "jtable/extensions/jquery.jtable.aspnetpagemethods");
       LayoutPages.IncludeJS(this, "jtable/localization/jquery.jtable.es.js" );
       LayoutPages.IncludeJS(this, "jtable/localization/jquery.jtable.en.js");

      //LayoutPages.IncludeJS(this, "charts/highcharts");
      LayoutPages.IncludeJS(this, "charts/highcharts.src");
      LayoutPages.IncludeJS(this, "charts/highcharts-more");
      LayoutPages.IncludeJS(this, "charts/highcharts-3d");
      LayoutPages.IncludeJS(this, "charts/modules/exporting");
      LayoutPages.IncludeJS(this, "charts/modules/solid-gauge.src");
      LayoutPages.IncludeJS(this, "charts/modules/heatmap");
      LayoutPages.IncludeJS(this, "libs/heatmap");
      LayoutPages.IncludeJS(this, "libs/gridstack");

      // COMMON
      LayoutPages.IncludeJS(this, "CommonDefinitions");
      LayoutPages.IncludeJS(this, "WSILanguage");
      LayoutPages.IncludeJS(this, "WSIUserConfiguration");
      LayoutPages.IncludeJS(this, "ui/WSI_ui");
      LayoutPages.IncludeJS(this, "WSICommon");
      LayoutPages.IncludeJS(this, "WSICommonDateTime");
      LayoutPages.IncludeJS(this, "WSICommonFormat");
      LayoutPages.IncludeJS(this, "WSILogger");
      LayoutPages.IncludeJS(this, "WSIData");
      LayoutPages.IncludeJS(this, "WSIConstants");
      LayoutPages.IncludeJS(this, "WSILabel");
      LayoutPages.IncludeJS(this, "WSIObjectProperties");
      LayoutPages.IncludeJS(this, "WSITerminal");
      LayoutPages.IncludeJS(this, "beacons/WSIBeaconsManager");
      LayoutPages.IncludeJS(this, "runners/WSIRunnersManager");
      LayoutPages.IncludeJS(this, "WSILegend");
      LayoutPages.IncludeJS(this, "WSIDialog");
      LayoutPages.IncludeJS(this, "dialogs/dlgProgress");
      LayoutPages.IncludeJS(this, "floor/WSIMetrics");
      LayoutPages.IncludeJS(this, "floor/WSIFloor3");

      LayoutPages.IncludeJS(this, "WSIGamingTableSeat");
      LayoutPages.IncludeJS(this, "WSIGamingTable");

      LayoutPages.IncludeJS(this, "WSIHeatMap");
      LayoutPages.IncludeJS(this, "runners/WSIRunnersMap");
      LayoutPages.IncludeJS(this, "WSIManager");
      LayoutPages.IncludeJS(this, "widgets/WSI_WidgetManager");
      LayoutPages.IncludeJS(this, "widgets/WSI_WidgetTypes");
      LayoutPages.IncludeJS(this, "widgets/WSI_WidgetTemplates");
      LayoutPages.IncludeJS(this, "widgets/WSI_WidgetDefinition");

      // TASKS
      LayoutPages.IncludeJS(this, "controls/TableListBase");
      LayoutPages.IncludeJS(this, "tasks/WSI_Tasks");
      LayoutPages.IncludeJS(this, "tasks/WSI_Tasks_ui");

      // Meters
      LayoutPages.IncludeJS(this, "meters/WSI_MetersDb");
      LayoutPages.IncludeJS(this, "meters/WSI_MetersManager");

      //Messaging
      LayoutPages.IncludeJS(this, "messaging/jquery.signalR-1.1.4");
      LayoutPages.IncludeJS(this, "messaging/WSI_MqttClient");
      LayoutPages.IncludeJS(this, "messaging/WSI_MessageManager");
      LayoutPages.IncludeJS(this, "messaging/toastr8");


      String _config = RealTimeDataBase.GetLayoutConfiguration();
      String _view_mode = clsJsonUtils.GetPropertyValue(_config, "ViewMode", "2");
      Global.Sessions.Information[HttpContext.Current.Session.SessionID].User.ViewMode = Int32.Parse(_view_mode);

      switch (Global.Sessions.Information[HttpContext.Current.Session.SessionID].User.ViewMode)
      {
        case 2: // 2D
          LayoutPages.RegisterJS(scmScripter, "WSI_Cenital");
          break;
        case 1: // 3D

          LayoutPages.IncludeJS(this, "canvg/rgbcolor");
          LayoutPages.IncludeJS(this, "canvg/StackBlur");
          LayoutPages.IncludeJS(this, "canvg/canvg");

          LayoutPages.IncludeJS(this, "libs/signals.min");
          LayoutPages.IncludeJS(this, "libs/three");
          LayoutPages.IncludeJS(this, "renderers/Projector");
          LayoutPages.IncludeJS(this, "renderers/SoftwareRenderer");
          LayoutPages.IncludeJS(this, "libs/Detector");
          LayoutPages.IncludeJS(this, "libs/tween.min");
          LayoutPages.IncludeJS(this, "libs/stats.min");
          LayoutPages.IncludeJS(this, "controls/OrbitControls");
          LayoutPages.IncludeJS(this, "controls/FirstPersonControls");
          LayoutPages.IncludeJS(this, "controls/virtualjoystick");
          //LayoutPages.IncludeJS(this, "controls/TrackballControls");
          LayoutPages.IncludeJS(this, "models");

          LayoutPages.RegisterJS(scmScripter, "WSIJSONLoader");
          LayoutPages.RegisterJS(scmScripter, "WSIObjectUtils");
          LayoutPages.RegisterJS(scmScripter, "WSIMaterials");
          LayoutPages.RegisterJS(scmScripter, "WSIGeometries");
          LayoutPages.RegisterJS(scmScripter, "cameras/WSICameras");
          LayoutPages.RegisterJS(scmScripter, "controls/WSIControls");
          LayoutPages.RegisterJS(scmScripter, "WSI");
          LayoutPages.RegisterJS(scmScripter, "WSI_Layout");
          LayoutPages.RegisterJS(scmScripter, "ResourceList/ResourceList");
          break;
      }

      LayoutPages.IncludeJS(this, "WSIDialog");
      LayoutPages.IncludeJS(this, "WSIReportsDefinitions");
      LayoutPages.RegisterJS(scmScripter, "WSIReport");
      LayoutPages.IncludeJS(this, "WSIReportCharts");
      LayoutPages.IncludeJS(this, "WSICustomersAtRoom");
      LayoutPages.IncludeJS(this, "WSIAlarmsAtRoom");
      LayoutPages.IncludeJS(this, "WSITerminalsAtRoom");
      LayoutPages.RegisterJS(scmScripter, "WSIReportDashboard");
      //LayoutPages.IncludeJS(this, "libs/moment");
      LayoutPages.IncludeJS(this, "libs/moment-with-locales");

      //MQTT Paho Client


      // Prepare Page
      Prepare_Top_Bar();
      Prepare_Left_Menu();

      CreateRoleList();

      // Dashboard
      //Fill_Dashboard();

      // Initial Script
      RegisterInitialScript();
      // Insert the initial script for the page
    }
    private void RegisterInitialScript()
    {
      EN_ROLES _role = Global.Sessions.Information[HttpContext.Current.Session.SessionID].User.CurrentRole;

      String _script = String.Empty;

      _script += " var _LayoutMode = " + Global.Sessions.Information[HttpContext.Current.Session.SessionID].User.ViewMode + ";";
      _script += " var " + clsDashboard.GlobalReportListVariable + " = null;";
      _script += " var _CurrentRole=" + (Int32)_role + ";";
      _script += " $(document).ready(function() { ";
      _script += clsDashboard.GenerateReportsList(_role, false);
      _script += " PageMethods.CheckCurrentSession(function (Result) { ";
      _script += " if (Result) { PreLoader(); LoadConfig(); } else { GotoLogin(); }";
      _script += " }, function () { GotoLogin(); }); ";
      _script += " });";

      LayoutPages.IncludeScript(this, _script);

      ////signalr autogenerated script
      //HtmlGenericControl child = new HtmlGenericControl("script");
      //child.Attributes.Add("src", "~/signalr/hubs");
      //this.Header.Controls.Add(child);
    }

    //
    private void Page_PreLoad(object sender, EventArgs e)
    {

      if (!Global.IntelliaLicense.LicenseValid)
      {
        Response.Redirect("./ErrorPage.aspx");
        Logger.Write(HttpContext.Current, "Lisence Invalid");
      }

      // Check
      if (Global.Sessions.Information.ContainsKey(Session.SessionID) && Global.Sessions.Information[Session.SessionID].User.Logged)
      {
        // Load Security
        Global.Sessions.Information[Session.SessionID].User.Permissions = new LayoutUserPermissions(Global.Sessions.Information[Session.SessionID].User.Id,
                                                                                                    Global.Sessions.Information[Session.SessionID].User.CurrentRole);
      }
      else
      {
        // Not logged
        Logger.Write(HttpContext.Current, "HOME_PRELOAD | Not logged in!");

        // Redirect to Login
        Response.Redirect("./Login.aspx?s=" + clsStrings.EncodeTo64("home"));
      }
    }

    //
    private void Fill_Dashboard()
    {

      List<LayoutMenuItem> _items = null;
      List<LayoutMenuItem> _itemsOPS = null;
      List<LayoutMenuItem> _itemsPCA = null;
      List<LayoutMenuItem> _itemsSLO = null;
      List<LayoutMenuItem> _itemsTCH = null;
      _itemsOPS = Global.Sessions.Information[Session.SessionID].User.Permissions.GetProfileMenuItemsByLevel(EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ROLES.OPS);
      _itemsPCA = Global.Sessions.Information[Session.SessionID].User.Permissions.GetProfileMenuItemsByLevel(EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ROLES.PCA);
      _itemsSLO = Global.Sessions.Information[Session.SessionID].User.Permissions.GetProfileMenuItemsByLevel(EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ROLES.SLO);
      _itemsTCH = Global.Sessions.Information[Session.SessionID].User.Permissions.GetProfileMenuItemsByLevel(EN_ITEM_LEVEL.IL_LEVEL_NONE, EN_ROLES.TCH);

       _items = _itemsOPS.Concat(_itemsPCA)
                                    .Concat(_itemsSLO).Concat(_itemsTCH)
                                    .ToList();

      //EN_ROLES _role = Global.Sessions.Information[HttpContext.Current.Session.SessionID].User.CurrentRole;
      //HtmlGenericControl _dashboard_charts_div = LayoutPages.GetCommonControl(this, "div_report_chart_reports");
      //if (_dashboard_charts_div != null)
      //{

      //  // RMS -> DASHBOARD CHANGES
      //  //_dashboard_charts_div.InnerHtml = clsDashboard.GenerateReportsDivs(_role);

      //  //ClientScript.RegisterClientScriptBlock(this.GetType(), "DashboardReports:" + this.UniqueID, clsDashboard.GenerateReportsList(_role, true));
      //  //LayoutPages.RegisterScript(this, clsDashboard.GenerateReportsList(_role, true));
      //}
      //else
      //{
      //  Logger.Write(HttpContext.Current, "Fill_Dashboard() | Cannot get the dashboard reports container!");
      //}
    }

    private void CreateRoleList()
    {
      HtmlGenericControl _div_role = LayoutPages.GetCommonControl(this, "div_roles");
      clsMenuOptions _menu_options = new clsMenuOptions();
      List<int> _roles = Global.Sessions.Information[Session.SessionID].User.Permissions.AvailableRoles;

      if (_div_role != null)
      {
        _div_role.InnerHtml = _menu_options.GetHtmlRole(_roles);
      }
      else
      {
        Logger.Write(HttpContext.Current, "Fill_Dashboard() | Cannot get the dashboard reports container!");
      }

    }

    //
    private void Prepare_Top_Bar()
    {
      HtmlGenericControl _control_player_role;
      HtmlGenericControl _top_player_role;
      HtmlGenericControl _image_player;
      HtmlGenericControl _message;

      Global.Sessions.Information[Session.SessionID].CasinoName = WSI.Common.GeneralParam.GetString("Site", "Name", "CASINO");

      // Check for first time current role
      if (Global.Sessions.Information[Session.SessionID].User.CurrentRole == EN_ROLES.NONE)
      {
        Global.Sessions.Information[Session.SessionID].User.CurrentRole = (EN_ROLES)Global.Sessions.Information[Session.SessionID].User.Permissions.AvailableRoles[0];
      }

      ////configuration_profile
      if (!LayoutPages.SetCommonControlInnerHtml(this, "txt_floor_name", Global.Sessions.Information[Session.SessionID].CasinoName))
      {
        Logger.Write(HttpContext.Current, "Prepare_Top_Bar() | Cannot set casino name!");
      }

      //if (!LayoutPages.SetCommonControlInnerHtml(this, "txt_current_section", Global.Sessions.Information[Session.SessionID].CurrentSection.ToString()))
      //{
      //  Logger.Write(HttpContext.Current, "Prepare_Top_Bar() | Cannot set current section name!");
      //}

      if (!LayoutPages.SetCommonControlInnerHtml(this, "txt_user_full_name", Global.Sessions.Information[Session.SessionID].User.FullName))
      {
        Logger.Write(HttpContext.Current, "Prepare_Top_Bar() | Cannot set user full name!");
      }

      if (!LayoutPages.SetCommonControlInnerHtml(this, "txt_user_nick", Global.Sessions.Information[Session.SessionID].User.Nick))
      {
        Logger.Write(HttpContext.Current, "Prepare_Top_Bar() | Cannot set user nick!");
      }

      _control_player_role = LayoutPages.GetCommonControl(this, "txt_current_role");
      _top_player_role = LayoutPages.GetCommonControl(this, "div_top_toolbar");
      _image_player = LayoutPages.GetCommonControl(this, "imgProfile");

      if (_control_player_role != null)
      {
        _control_player_role.Attributes.Add("data-role", ((Int32)Global.Sessions.Information[Session.SessionID].User.CurrentRole).ToString());
        _control_player_role.Attributes.Add("data-nls", "NLS_ROLE_" + (Int32)Global.Sessions.Information[Session.SessionID].User.CurrentRole);
        _control_player_role.Attributes.Add("data-userid", ((Int32)Global.Sessions.Information[Session.SessionID].User.Id).ToString());
        //_top_player_role.Attributes["class"] = "headerTop rol" + Global.Sessions.Information[Session.SessionID].User.CurrentRole.ToString();

        var src = ChangeSourceImageByRol(Global.Sessions.Information[Session.SessionID].User.CurrentRole);
        _image_player.InnerHtml = string.Format("<img id=\"img_user_profile\" src=\"{0}\" alt=\"Foto Perfil\" runat=\"server\" />", src);
      }

      var permiso = Global.Sessions.Information[Session.SessionID].User.Permissions.GetProfileMenuItemsByLevel(EN_ITEM_LEVEL.IL_LEVEL_NONE, Global.Sessions.Information[Session.SessionID].User.CurrentRole).Where(x => x.Id == 90).ToList();
      _message = LayoutPages.GetCommonControl(this, "message");
      if (permiso.Count == 0)
      {
        _message.Visible = false;
      }
      else
        _message.Visible = true;

      
    }

    private string ChangeSourceImageByRol(EN_ROLES rol)
    {
      var src = "design/css/styles/img/fotoPerfil.png";
      switch (rol)
      {
        case EN_ROLES.OPS:
          return src = "design/css/styles/img/perfilOPS.svg";
        case EN_ROLES.PCA:
          return src = "design/css/styles/img/perfilPCA.svg";
        case EN_ROLES.SLO:
          return src = "design/css/styles/img/perfilSA.svg";
        case EN_ROLES.TCH:
          return src = "design/css/styles/img/perfilTS.svg";
        default: // NONE
          return src = "design/css/styles/img/perfilPCA.svg";
      }
    }

    //
    private void Prepare_Left_Menu()
    {
      List<LayoutMenuItem> _items = null;

      // Get sections from user permissions
      //_sections = Global.Sessions.Information[Session.SessionID].User.Permissions.GetProfileSections(Global.Sessions.Information[Session.SessionID].CurrentRole);
      _items = Global.Sessions.Information[Session.SessionID].User.Permissions.GetProfileMenuItemsByLevel(EN_ITEM_LEVEL.IL_LEVEL_1, Global.Sessions.Information[Session.SessionID].User.CurrentRole);

      // Create main menu
      clsMenu _menu = clsMenu.FromSections("menu", _items.OrderBy(x => x.Id).ToList());

      // Draw the menu on the page
      HtmlGenericControl _menu_div = LayoutPages.GetCommonControl(this, "div_left_menu");
      if (_menu_div != null)
      {
        _menu_div.InnerHtml = _menu.ToString();

        // Add Win Logo to the menu
        _menu_div.InnerHtml += "<div class=\"logoWin\"><a href=\"#\"><img src=\"design/css/styles/img/logoWin.png\" alt=\"WinSystems\"></a></div>";
      }
      else
      {
        Logger.Write(HttpContext.Current, "Prepare_Left_Menu() | Cannot get the menu container!");
      }

      // Draw submenus
      clsMenu _sub_menu = null;
      List<LayoutMenuItem> _sub_items = null;

      HtmlGenericControl _sub_menu_div = LayoutPages.GetCommonControl(this, "div_left_submenus");
      if (_sub_menu_div != null)
      {
        foreach (LayoutMenuItem _item in _items)
        {
          //_sub_menu_div.InnerHtml += "<hr />";

          _sub_items = Global.Sessions.Information[Session.SessionID].User.Permissions.GetProfileMenuItemsByLevel(EN_ITEM_LEVEL.IL_LEVEL_2, _item.SectionId, Global.Sessions.Information[Session.SessionID].User.CurrentRole);

          _sub_menu = clsMenu.FromSections("sub_menu_" + _item.SectionId, _sub_items);
          //_sub_menu_div.InnerHtml += "<div id='div_sub_menu_" + _item.SectionId + "' style=' visibility: hidden;'>" +  _sub_menu.ToString() + "</div>";
          _sub_menu_div.InnerHtml += "<div id='div_sub_menu_" + _item.SectionId + "'>" + _sub_menu.ToString() + "</div>";
        }
      }
      else
      {
        Logger.Write(HttpContext.Current, "Prepare_Left_Menu() | Cannot get the submenus container!");
      }

    }

    [WebMethod(), ScriptMethod()]
    public static String GetLayoutConfigurationFromSession()
    {
      Globalize();
      Logger.Write(HttpContext.Current, "GetLayoutConfiguration() | Called!");
      return RealTimeDataBase.GetLayoutConfigurationFromSession();
    }


    //
    //
    //
    [WebMethod(), ScriptMethod()]
    public static String GetRealTimeInformation()
    {
      Globalize();
      Logger.Write(HttpContext.Current, "GetRealTimeInformation() | Called!");

      if (Global.IntelliaLicense.LicenseValid)
      {
        if (Global.Sessions.SessionExists(HttpContext.Current.Session.SessionID))
        {
          return RealTimeDataBase.GetActivity(false, false);
        }
        else
        {
          Global.Sessions.SessionFinalize(HttpContext.Current);

          return "{\"Message\": 900}";
        }
      }
      else
      {
        Global.Sessions.SessionFinalize(HttpContext.Current);

        return "{\"Message\": 401}";  //Unauthorized
      }
    }

    [WebMethod(), ScriptMethod()]
    public static String GetRealTimeInformationTest()
    {
      Globalize();
      Logger.Write(HttpContext.Current, "GetRealTimeInformationTest() | Called!");
      return RealTimeDataBase.GetActivityTest(false, false);
    }

    [WebMethod(), ScriptMethod()]
    public static Boolean SetCurrentRole(int Role)
    {
      Globalize();
      Logger.Write(HttpContext.Current, "SetCurrentRole() | Called!");
      SetCurrentRoles(Role);

      return true;
    }

    private static void SetCurrentRoles(int Role)
    {
      Globalize();
      if (Global.Sessions.Information[HttpContext.Current.Session.SessionID].User.CurrentRole != (EN_ROLES)Role)
      {
        Global.Sessions.Information[HttpContext.Current.Session.SessionID].User.CurrentRole = (EN_ROLES)Role;
      }
    }

    //
    //
    //
    [WebMethod(), ScriptMethod()]
    public static String GetLayoutFloors()
    {
      Globalize();
      Logger.Write(HttpContext.Current, "GetLayoutFloors() | Called!");
      return Floor.GetLayoutFloors();

    }

    //
    //
    //
    [WebMethod(), ScriptMethod()]
    public static String GetLayout(Int32 FloorId)
    {
      Globalize();
      Logger.Write(HttpContext.Current, "GetLayout() | Called!");
      //return Layout.GetLayout();

      return clsEditor.GetLayout(FloorId);
    }

    [WebMethod(), ScriptMethod()]
    public static String GetPlayerActivity(String ListAccountId, Boolean OnlyActivePlaySession) //ListAccountId = "100,322,1553"
    {
      Globalize();
      Logger.Write(HttpContext.Current, "GetPlayerActivity() | Called!");
      return RealTimeDataBase.GetPlayerActivity(ListAccountId, OnlyActivePlaySession);
    }

    [WebMethod(), ScriptMethod()]
    public static String GetPlayerActivityTest(String ListAccountId, Boolean OnlyActivePlaySession) //ListAccountId = "100,322,1553"
    {
      Globalize();
      Logger.Write(HttpContext.Current, "GetPlayerActivityTest | Called!");
      return RealTimeDataBase.GetPlayerActivityTest(ListAccountId, OnlyActivePlaySession);
    }

    [WebMethod(), ScriptMethod()]
    public static String GetLayoutRanges()
    {
      Globalize();
      Logger.Write(HttpContext.Current, "GetLayoutRanges() | Called!");
      return RealTimeDataBase.GetLayoutRanges(Global.Sessions.Information[HttpContext.Current.Session.SessionID].User.CurrentRole);
    }

    [WebMethod(), ScriptMethod()]
    public static Boolean SaveLayoutConfig(String Configuration, String Dashboard)
    {
      Globalize();
      Logger.Write(HttpContext.Current, "SaveLayoutConfig() | Called!");
      return RealTimeDataBase.SaveLayoutConfig(Configuration, Dashboard);
    } // SaveLayoutConfig

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // DASHBOARD METHODS

    [WebMethod(), ScriptMethod()]
    public static String GetSegmentationLevelsClients()
    {
      Globalize();
      return Dashboard.GetSegmentationLevelsClients();
    }

    [WebMethod(), ScriptMethod()]
    public static String GetSegmentationLevelsVisits()
    {
      Globalize();
      return Dashboard.GetSegmentationLevelsVisits();
    }

    [WebMethod(), ScriptMethod()]
    public static String GetCurrentPromotions()
    {
      Globalize();
      return Dashboard.GetCurrentPromotions();
    }

    [WebMethod(), ScriptMethod()]
    public static Int32 SaveCustomAlarm(String Name, String FilterParameters, String FilterDisplay, Int32 ID)
    {
      Globalize();
      Logger.Write(HttpContext.Current, "SaveLayoutConfig() | Called!");
      return RealTimeDataBase.SaveCustomAlarm(Name, FilterParameters, FilterDisplay, ID);
    } // SaveLayoutConfig

    //[WebMethod(), ScriptMethod()]
    //public static Boolean ChangeViewMode(Int32 Mode)
    //{
    //    if (Mode == 1 || Mode == 2)
    //    {
    //        Global.Sessions.Information[HttpContext.Current.Session.SessionID].User.ViewMode = Mode;
    //        return true;
    //    }
    //    return false;
    //}

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    [WebMethod(), ScriptMethod()]
    public static String GetSiteMetersData()
    {
      Globalize();
      Logger.Write(HttpContext.Current, "GetSiteMetersData() | Called!");
      return Meters.GetSiteMeters();
    }

    [WebMethod(), ScriptMethod()]
    public static String GetSiteTasksData()
    {
      Globalize();
      Logger.Write(HttpContext.Current, "GetSiteTasksData() | Called!");
      return Tasks.GetSiteTasks();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="Data"></param>
    /// <returns></returns>

    private static void Globalize(){
      System.Globalization.CultureInfo en = new System.Globalization.CultureInfo("en-US");
      System.Threading.Thread.CurrentThread.CurrentCulture = en;
    }
    
    
    [WebMethod(), ScriptMethod()]
    public static String SaveSiteTask(String Data)
    {
      try
      {
        TaskData _data = new TaskData();

        Globalize();

        _data.ID = Int64.Parse(clsJsonUtils.GetPropertyValue(Data, "id", ""));          //O
        _data.Status = Int32.Parse(clsJsonUtils.GetPropertyValue(Data, "status", ""));  //O
        _data.Start = Tasks.TruncateMilliseconds(DateTime.Parse(clsJsonUtils.GetPropertyValue(Data, "start", DateTime.MinValue.ToString())));
        _data.End = Tasks.TruncateMilliseconds(DateTime.Parse(clsJsonUtils.GetPropertyValue(Data, "end", DateTime.MinValue.ToString())));
        _data.Category = Int32.Parse(clsJsonUtils.GetPropertyValue(Data, "category", "-1"));
        _data.Subcategory = Int32.Parse(clsJsonUtils.GetPropertyValue(Data, "subcategory", "-1"));
        _data.TerminalId = Int32.Parse(clsJsonUtils.GetPropertyValue(Data, "terminal_id", "-1"));
        _data.AccountId = Int64.Parse(clsJsonUtils.GetPropertyValue(Data, "account_id", "-1"));
        _data.Description = clsJsonUtils.GetPropertyValue(Data, "description", "");
        _data.Severity = Int32.Parse(clsJsonUtils.GetPropertyValue(Data, "severity", "-1"));
        _data.CreationUserId = Int32.Parse(clsJsonUtils.GetPropertyValue(Data, "creation_user_id", "-1"));  //O
        _data.Creation = Tasks.TruncateMilliseconds(DateTime.Parse(clsJsonUtils.GetPropertyValue(Data, "creation", DateTime.MinValue.ToString())));             //O
        _data.AssignedRoleId = Int32.Parse(clsJsonUtils.GetPropertyValue(Data, "assigned_role_id", "-1"));
        _data.AssignedUserId = Int32.Parse(clsJsonUtils.GetPropertyValue(Data, "assigned_user_id", "-1"));
        _data.Assigned = Tasks.TruncateMilliseconds(DateTime.Parse(clsJsonUtils.GetPropertyValue(Data, "assigned", DateTime.MinValue.ToString())));
        _data.AcceptedUserId = Int32.Parse(clsJsonUtils.GetPropertyValue(Data, "accepted_user_id", "-1"));
        _data.Accepted = Tasks.TruncateMilliseconds(DateTime.Parse(clsJsonUtils.GetPropertyValue(Data, "accepted", DateTime.MinValue.ToString())));
        _data.ScaleFromUserId = Int32.Parse(clsJsonUtils.GetPropertyValue(Data, "scale_from_user_id", "-1"));
        _data.ScaleToUserId = Int32.Parse(clsJsonUtils.GetPropertyValue(Data, "scale_to_user_id", "-1"));
        _data.ScaleReason = clsJsonUtils.GetPropertyValue(Data, "scale_reason", "");
        _data.Scale = Tasks.TruncateMilliseconds(DateTime.Parse(clsJsonUtils.GetPropertyValue(Data, "scale", DateTime.MinValue.ToString())));
        _data.SolvedUserId = Int32.Parse(clsJsonUtils.GetPropertyValue(Data, "solved_user_id", "-1"));
        _data.Solved = Tasks.TruncateMilliseconds(DateTime.Parse(clsJsonUtils.GetPropertyValue(Data, "solved", DateTime.MinValue.ToString())));
        _data.ValidateUserId = Int32.Parse(clsJsonUtils.GetPropertyValue(Data, "validate_user_id", "-1"));
        _data.Validate = Tasks.TruncateMilliseconds(DateTime.Parse(clsJsonUtils.GetPropertyValue(Data, "validate", DateTime.MinValue.ToString())));
        _data.LastStatusUpdateUserId = Int32.Parse(clsJsonUtils.GetPropertyValue(Data, "last_status_update_user_id", "-1"));
        _data.LastStatusUpdate = Tasks.TruncateMilliseconds(WSI.Common.WGDB.Now);//DateTime.Parse(clsJsonUtils.GetPropertyValue(Data, "last_status_update", DateTime.MinValue.ToString()));
        _data.AttachedMedia = Int32.Parse(clsJsonUtils.GetPropertyValue(Data, "attached_media_id", "-1"));
        _data.History = clsJsonUtils.GetPropertyValue(Data, "history", "");
        _data.Title = clsJsonUtils.GetPropertyValue(Data, "title", "");
        switch (_data.Status)
        {
          case 1: //ASSIGNED
            _data.Assigned = Tasks.TruncateMilliseconds(WSI.Common.WGDB.Now);
            //_data.AssignedUserId = Global.Sessions.Information[HttpContext.Current.Session.SessionID].User.Id;
            break;

          case 2://IN_PROGRESS
            _data.Accepted = Tasks.TruncateMilliseconds(WSI.Common.WGDB.Now);
            _data.AcceptedUserId = _data.AssignedUserId;
            break;

          case 3://SOLVED
            _data.Solved = Tasks.TruncateMilliseconds(WSI.Common.WGDB.Now);
            _data.SolvedUserId = _data.AssignedUserId;
            break;

          case 5://VALIDATED
            _data.Validate = Tasks.TruncateMilliseconds(WSI.Common.WGDB.Now);
            _data.ValidateUserId = Global.Sessions.Information[HttpContext.Current.Session.SessionID].User.Id;
            break;

          default:

            break;
        }

        Tasks.SaveSiteTask(_data);

        return new JavaScriptSerializer().Serialize(_data);
      }
      catch (Exception _ex)
      {
        Logger.Write(HttpContext.Current, "SaveSiteTask() || " + _ex.Message);
      }
      return null;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="Data"></param>
    /// <returns></returns>
    [WebMethod(), ScriptMethod()]
    public static Boolean SaveSiteAlarm(String Data)
    {
      try
      {
        Globalize();
        
        AlarmData _data = new AlarmData();

        _data.TerminalId = Int32.Parse(clsJsonUtils.GetPropertyValue(Data, "terminal_id", "-1"));
        _data.ID = Int64.Parse(clsJsonUtils.GetPropertyValue(Data, "id", "-1"));
        _data.AlarmType = Int32.Parse(clsJsonUtils.GetPropertyValue(Data, "alarm_type", "-1"));
        _data.DateCreated = Tasks.TruncateMilliseconds(DateTime.Parse(clsJsonUtils.GetPropertyValue(Data, "date_created", DateTime.MinValue.ToString())));
        _data.UserCreated = Int32.Parse(clsJsonUtils.GetPropertyValue(Data, "user_created", "-1"));
        _data.Media = Int64.Parse(clsJsonUtils.GetPropertyValue(Data, "media_id", "-1"));
        _data.Description = clsJsonUtils.GetPropertyValue(Data, "description", "");
        _data.AlarmSource = Int32.Parse(clsJsonUtils.GetPropertyValue(Data, "alarm_source", "-1"));
        _data.AlarmId = Int64.Parse(clsJsonUtils.GetPropertyValue(Data, "alarm_id", "-1"));
        _data.Status = Int32.Parse(clsJsonUtils.GetPropertyValue(Data, "status", "0"));
        _data.Title = clsJsonUtils.GetPropertyValue(Data, "title", "");
        return Tasks.SaveSiteAlarm(_data);
      }
      catch (Exception _ex)
      {
        Logger.Write(HttpContext.Current, "SaveSiteAlarm() || " + _ex.Message);
      }

      return false;
    }

    [WebMethod(), ScriptMethod()]
    public static String AssignAlarm(String AlarmData, String TaskData)
    {
      try
      {
        Globalize();
        
        TaskData _task_data = new TaskData();

        _task_data.ID = Int64.Parse(clsJsonUtils.GetPropertyValue(TaskData, "id", ""));          //O
        _task_data.Status = Int32.Parse(clsJsonUtils.GetPropertyValue(TaskData, "status", ""));  //O
        _task_data.Start = Tasks.TruncateMilliseconds(DateTime.Parse(clsJsonUtils.GetPropertyValue(TaskData, "start", DateTime.MinValue.ToString())));
        _task_data.End = Tasks.TruncateMilliseconds(DateTime.Parse(clsJsonUtils.GetPropertyValue(TaskData, "end", DateTime.MinValue.ToString())));
        _task_data.Category = Int32.Parse(clsJsonUtils.GetPropertyValue(TaskData, "category", "-1"));
        _task_data.Subcategory = Int32.Parse(clsJsonUtils.GetPropertyValue(TaskData, "subcategory", "-1"));
        _task_data.TerminalId = Int32.Parse(clsJsonUtils.GetPropertyValue(TaskData, "terminal_id", "-1"));
        _task_data.AccountId = Int64.Parse(clsJsonUtils.GetPropertyValue(TaskData, "account_id", "-1"));
        _task_data.Description = clsJsonUtils.GetPropertyValue(TaskData, "description", "");
        _task_data.Severity = Int32.Parse(clsJsonUtils.GetPropertyValue(TaskData, "severity", "-1"));
        _task_data.CreationUserId = Global.Sessions.Information[HttpContext.Current.Session.SessionID].User.Id;  //O
        _task_data.Creation = Tasks.TruncateMilliseconds(WSI.Common.WGDB.Now);             //O
        _task_data.AssignedRoleId = Int32.Parse(clsJsonUtils.GetPropertyValue(TaskData, "assigned_role_id", "-1"));
        _task_data.AssignedUserId = Int32.Parse(clsJsonUtils.GetPropertyValue(TaskData, "assigned_user_id", "-1"));
        _task_data.Assigned = Tasks.TruncateMilliseconds(WSI.Common.WGDB.Now);
        _task_data.AcceptedUserId = Int32.Parse(clsJsonUtils.GetPropertyValue(TaskData, "accepted_user_id", "-1"));
        _task_data.Accepted = Tasks.TruncateMilliseconds(DateTime.Parse(clsJsonUtils.GetPropertyValue(TaskData, "accepted", DateTime.MinValue.ToString())));
        _task_data.ScaleFromUserId = Int32.Parse(clsJsonUtils.GetPropertyValue(TaskData, "scale_from_user_id", "-1"));
        _task_data.ScaleToUserId = Int32.Parse(clsJsonUtils.GetPropertyValue(TaskData, "scale_to_user_id", "-1"));
        _task_data.ScaleReason = clsJsonUtils.GetPropertyValue(TaskData, "scale_reason", "");
        _task_data.Scale = Tasks.TruncateMilliseconds(DateTime.Parse(clsJsonUtils.GetPropertyValue(TaskData, "scale", DateTime.MinValue.ToString())));
        _task_data.SolvedUserId = Int32.Parse(clsJsonUtils.GetPropertyValue(TaskData, "solved_user_id", "-1"));
        _task_data.Solved = Tasks.TruncateMilliseconds(DateTime.Parse(clsJsonUtils.GetPropertyValue(TaskData, "solved", DateTime.MinValue.ToString())));
        _task_data.ValidateUserId = Int32.Parse(clsJsonUtils.GetPropertyValue(TaskData, "validate_user_id", "-1"));
        _task_data.Validate = Tasks.TruncateMilliseconds(DateTime.Parse(clsJsonUtils.GetPropertyValue(TaskData, "validate", DateTime.MinValue.ToString())));
        _task_data.LastStatusUpdateUserId = Global.Sessions.Information[HttpContext.Current.Session.SessionID].User.Id;
        _task_data.LastStatusUpdate = Tasks.TruncateMilliseconds(WSI.Common.WGDB.Now);
        _task_data.AttachedMedia = Int32.Parse(clsJsonUtils.GetPropertyValue(TaskData, "attached_media_id", "-1"));
        _task_data.History = clsJsonUtils.GetPropertyValue(TaskData, "history", "");
        _task_data.Title = clsJsonUtils.GetPropertyValue(TaskData, "title", "");
        AlarmData _alarm_data = new AlarmData();

        _alarm_data.TerminalId = Int32.Parse(clsJsonUtils.GetPropertyValue(AlarmData, "terminal_id", "-1"));
        _alarm_data.ID = Int64.Parse(clsJsonUtils.GetPropertyValue(AlarmData, "id", "-1"));
        _alarm_data.AlarmType = Int32.Parse(clsJsonUtils.GetPropertyValue(AlarmData, "alarm_type", "-1"));
        _alarm_data.DateCreated = Tasks.TruncateMilliseconds(DateTime.Parse(clsJsonUtils.GetPropertyValue(AlarmData, "date_created", DateTime.MinValue.ToString())));
        _alarm_data.UserCreated = Int32.Parse(clsJsonUtils.GetPropertyValue(AlarmData, "user_created", "-1"));
        _alarm_data.Media = Int64.Parse(clsJsonUtils.GetPropertyValue(AlarmData, "media_id", "-1"));
        _alarm_data.Description = clsJsonUtils.GetPropertyValue(AlarmData, "description", "");
        _alarm_data.AlarmSource = Int32.Parse(clsJsonUtils.GetPropertyValue(AlarmData, "alarm_source", "-1"));
        _alarm_data.Status = Int32.Parse(clsJsonUtils.GetPropertyValue(AlarmData, "status", "0"));
        _alarm_data.AlarmId = Int64.Parse(clsJsonUtils.GetPropertyValue(AlarmData, "alarm_id", "-1"));
        _alarm_data.Title = clsJsonUtils.GetPropertyValue(AlarmData, "title", "");
        //return Tasks.AssignAlarm(_alarm_data, _task_data);
        return new JavaScriptSerializer().Serialize(Tasks.AssignAlarm(_alarm_data, _task_data));

      }
      catch (Exception _ex)
      {
        Logger.Write(HttpContext.Current, "AssignAlarm() || " + _ex.Message);
      }

      return null;
    }

    /// //////////////////////////////////////////////////////////////////////////////////////////////////////////

    [WebMethod(), ScriptMethod()]
    public static String GetConversationHistory(String runnerId, String managerId)
    {
      Globalize();
      Logger.Write(HttpContext.Current, "GetConversationHistory() | Called!");

      Int32 runner = String.IsNullOrEmpty(runnerId) ? 0 : int.Parse(runnerId);

      return Messaging.GetConversationHistory(int.Parse(managerId), runner);
    }

    [WebMethod(), ScriptMethod()]
    public static String SendMessageToRunner(String message)
    {
      Globalize();
      Logger.Write(HttpContext.Current, "SendMessageToRunner() | Called!");

      Messaging.Message messageToSend = new Messaging.Message();
      messageToSend.ManagerId = Int64.Parse(clsJsonUtils.GetPropertyValue(message, "managerId", "-1"));
      messageToSend.RunnerId = Int64.Parse(clsJsonUtils.GetPropertyValue(message, "runnerId", "-1"));
      messageToSend.RunnerUserName = clsJsonUtils.GetPropertyValue(message, "runnerUserName", "");
      messageToSend.ManagerUserName = (!string.IsNullOrEmpty(clsJsonUtils.GetPropertyValue(message, "managerUserName", ""))) ? clsJsonUtils.GetPropertyValue(message, "managerUserName", "").ToUpper() : clsJsonUtils.GetPropertyValue(message, "managerUserName", "");
      messageToSend.Content = clsJsonUtils.GetPropertyValue(message, "message", string.Empty);
      messageToSend.Source = (int)Messaging.MessageSource.MANAGER;

      return Messaging.SendMessageToRunner(messageToSend);
    }


    [WebMethod(), ScriptMethod()]
    public static void InitMqttClient(String clientId)
    {
      Globalize();
      try
      {
        MqttClientsManager.Register(clientId);
      }
      catch (Exception e)
      {
        Logger.Write(HttpContext.Current, "Error on Client Initialization: " + e.Message);
      }
    }

    [WebMethod(), ScriptMethod()]
    public static string LoadResources(Int32 FloorId)
    {
      Globalize();
      return Resources.GetFloorResources(FloorId);
    }

    [WebMethod(), ScriptMethod()]
    public static string LoadMediaData(Int32 MediaId)
    {
      Globalize();
      return Tasks.GetMediaData(MediaId);
    }

    [WebMethod(), ScriptMethod()]
    public static string LoadInfoPlayerData(Int64 AccountId)
    {
      Globalize();
      return RealTimeDataBase.GetPlayerInfoData(AccountId);
    }
    [WebMethod(), ScriptMethod()]
    public static string LoadTerminalHistoryData(Int64 TerminalId)
    {
      Globalize();
      return RealTimeDataBase.GetTerminalHistory(TerminalId);
    }

    [WebMethod(), ScriptMethod()]
    public static Boolean CheckCurrentSession()
    {
      Globalize();
      return Global.Sessions.IsSessionLogged(HttpContext.Current);
    }

    [WebMethod(), ScriptMethod()]
    public static void OnBeforeUnload()
    {
      Globalize();
      Global.Sessions.ExpireSession(HttpContext.Current.Session.SessionID);
      return;
    }

    [WebMethod(), ScriptMethod()]
    public static String GetRunnersInformation()
    {
      Globalize();
      return RealTimeDataBase.GetRunnersInformation();
    }

    [WebMethod(), ScriptMethod()]
    public static string LoadInfoTerminalData(Int64 TerminalId)
    {
      Globalize();
      return RealTimeDataBase.GetTerminalInfoData(TerminalId);
    }

    [WebMethod(), ScriptMethod()]
    public static Boolean DeteleCustomFilter(Int64 CustomFilterId)
    {
      Globalize();
      return RealTimeDataBase.DeteleCustomFilter(CustomFilterId);
    }

    [WebMethod(), ScriptMethod()]
    public static String GetRunnerTasks(Int64 RunnerId)
    {
      Globalize();
      return RealTimeDataBase.GetRunnerTasks(RunnerId);
    }
  }
}