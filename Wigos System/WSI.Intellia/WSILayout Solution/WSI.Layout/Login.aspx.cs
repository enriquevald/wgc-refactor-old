﻿using System.Collections.Generic;
using System;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using WSI.Layout.classes;
using System.Web.Security;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Web.UI.HtmlControls;
using System.Web.SessionState;
using System.Globalization;
using System.Web.UI.WebControls;

namespace WSI.Layout
{
  public partial class Login : System.Web.UI.Page
  {
    //private static Int32 CONST_AUDIT_NAME_LOGIN_LOGOUT = 4;
    public static String m_culture_info;

    private void Page_PreLoad(object sender, EventArgs e)
    {

      Logger.Write(HttpContext.Current, "Session_Start");

      if (!Global.IntelliaLicense.LicenseValid)
      {
        Response.Redirect("./ErrorPage.aspx");
        Logger.Write(HttpContext.Current, "Lisence Invalid");
      }

      String _request = Request.QueryString["out"];

      if (_request != null)
      {
        Global.Sessions.ExpireSession(Session.SessionID);
        Global.Sessions.SessionFinalize(Context);
      }

    }

    protected void Page_Load(object sender, EventArgs e)
    {
      // Including basic styles
      //LayoutPages.IncludeStyle(this, "jquery-ui_silver");
      //LayoutPages.IncludeStyle(this, "wsiLayout");
      //LayoutPages.IncludeStyle(this, "wsi-icon-tabs");
      //LayoutPages.IncludeStyle(this, "jquery.dd");

      LayoutPages.IncludeExternalStyle(this, "design/css/wsiLayout");
      LayoutPages.IncludeExternalStyle(this, "design/css/jquery-ui");
      LayoutPages.IncludeExternalStyle(this, "design/css/fonts");
      LayoutPages.IncludeExternalStyle(this, "design/css/estilo");
      LayoutPages.IncludeExternalStyle(this, "design/css/styles/nachoCSS");
    }

    protected void loginBox_Authenticate(object sender, AuthenticateEventArgs e)
    {
      //if (!Global.Sessions.UserAlreadyLogged(loginBox.UserName, HttpContext.Current.Session.SessionID))
      //{
      Global.Sessions.ReValidateSession(HttpContext.Current, Request);
      String _hostname;
      String _info;
      String _error;

      _info = WSI.Common.Misc.ReadGeneralParams("Cashier", "Language");

      if (_info == null || _info == "")
      {
        _info = CultureInfo.CurrentCulture.Name;
      }

      m_culture_info = _info;

      try
      {
        _hostname = System.Net.Dns.GetHostEntry(Request.ServerVariables["remote_addr"]).HostName;
      }
      catch (Exception _ex)
      {
        Logger.Exception(this.Context, "GetHostName", _ex);

        if (Request.ServerVariables["remote_addr"] != "")
        {
          _hostname = Request.ServerVariables["remote_addr"];
        }
        else
        {
          _hostname = "Unknown";
        }
      }

      // Check login
      Global.Sessions.Information[Session.SessionID].User = Layout.CheckLogin(loginBox.UserName, loginBox.Password, _hostname, out _error);

      if (Global.Sessions.Information[Session.SessionID].User.Logged)
      {
        // Log
        Logger.Write(HttpContext.Current, "LOGIN | Authenticated: " + this.loginBox.UserName);

        //Lookup for target parameter
        String _request_source = Request.QueryString["s"];
        if (_request_source != null)
        {
          _request_source = clsStrings.DecodeFrom64(_request_source);
          switch (_request_source)
          {
            case "editor":
              Response.Redirect("./editor/editor.aspx");
              break;
            case "home":
              Response.Redirect("./Home.aspx");
              break;
            case "logview":
              Response.Redirect("./log/logview.aspx");
              break;
            case "admin":
              Response.Redirect("./admin/admin.aspx");
              break;
            case "modelsio":
              Response.Redirect("./models/modelsio.aspx");
              break;
            default:
              Response.Redirect("./ErrorPage.aspx");
              break;
          }
        }
        else
        {
          // If there is no source, send session to home
          Response.Redirect("./Home.aspx");
        }

      }
      else
      {

        System.Web.UI.WebControls.Login _login = (System.Web.UI.WebControls.Login)Page.FindControl("loginBox");


        _login.FailureText=_error;



        // Invalid user
        Logger.Write(HttpContext.Current, "LOGIN | Failed: " + this.loginBox.UserName);
        // Redirect to Login again
        //Response.Redirect("./Login.aspx");
        //Response.Redirect(Request.RawUrl);
      }
      //}
      //else
      //{
      //  // User already logged
      //  Logger.Write(HttpContext.Current, "LOGIN | Already logged: " + this.loginBox.UserName);

      //  // Redirect to Login again
      //  Response.Redirect("./Login.aspx");
      //}
    }

  }
}