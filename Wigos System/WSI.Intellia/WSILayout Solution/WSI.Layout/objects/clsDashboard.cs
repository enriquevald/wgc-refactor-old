﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using WSI.Layout.classes;

namespace WSI.Layout.objects
{
  public static class clsDashboard
  {
    public static String GlobalReportListVariable = "m_report_dashboard_objects";

    private static List<String> ReportsByRole(EN_ROLES Role)
    {
      List<String> _result = new List<String>();

      // TODO: Check for permissions 

      switch (Role)
      {
        case EN_ROLES.NONE:
          break;
        case EN_ROLES.PCA:
          //_result.Add("m_report_dash_winnings_floor");
          _result.Add("m_report_dash_current_promotions");
          _result.Add("m_report_dash_occupation");
          _result.Add("m_report_dash_occupation_vips");
          _result.Add("m_report_dash_occupation_room");
          _result.Add("m_report_dash_bet_average_triband");
          _result.Add("m_report_dash_occup_average");
          _result.Add("m_report_dash_occupation_alarms");     
          _result.Add("m_report_dash_occupation_levels");
          _result.Add("m_report_dash_occupation_age");
          _result.Add("m_report_dash_occupation_sex");

          _result.Add("m_report_dash_segment_lvl_vis");
          _result.Add("m_report_dash_occupation_age_sex");

          //_result.Add("");
          break;
        case EN_ROLES.OPS:
          _result.Add("m_report_dash_played");
          _result.Add("m_report_dash_current_promotions");
          _result.Add("m_report_dash_bet_average_triband");
          _result.Add("m_report_dash_segment_lvl_vis");
          break;
        case EN_ROLES.SLO:
          _result.Add("m_report_dash_occupation_vips");
          _result.Add("m_report_dash_occupation_room");
          _result.Add("m_report_dash_occup_average");
          _result.Add("m_report_dash_occupation_sex");
          _result.Add("m_report_dash_occupation_levels");
          break;
        case EN_ROLES.TCH:
          _result.Add("m_report_dash_occupation_alarms");
          break;
      }

      return _result;
    }

    public static String GenerateReportsDivs(EN_ROLES Role)
    {
      List<String> _reports = clsDashboard.ReportsByRole(Role);
      String _result = String.Empty;
      foreach (String _item in _reports)
      {
        _result += "<div class='dashboard col50' id='" + _item.Substring(2) + "'>Waiting for data...</div>";
      }
      return _result;
    }

    public static String GenerateReportsList(EN_ROLES Role, Boolean WithTags)
    {
      List<String> _reports = clsDashboard.ReportsByRole(Role);
      String _result = String.Empty;
      if (WithTags)
      {
        _result += "<script type='text/javascript'>";
        _result += "var " + clsDashboard.GlobalReportListVariable + " = [";
      }
      else
      {
        _result += clsDashboard.GlobalReportListVariable + " = [";
      }
      
      if (_reports.Count > 0)
      {
        _result += String.Join(",", _reports.ToArray());
      }
      _result += "];";
      if (WithTags) { _result += "</script>"; }
      return _result;
    }

  }
}