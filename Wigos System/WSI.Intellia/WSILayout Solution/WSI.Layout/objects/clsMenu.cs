﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using WSI.Layout.classes;

namespace WSI.Layout.objects
{

  // Menu Item with html generation
  public class clsMenuItem
  {
    public String ID;           // Item Id - Function
    public String NLS;          // NLS for translation
    public String SectionID;    // Section Id 
    public String CSS;          // Styles
    public String Script;       // Script (event onclick)
    public EN_ITEM_ACTION_TYPE ActionType; // Type of action

    // Generate the html code for the menu item
    public String ToString(EN_ITEM_LEVEL Level)
    {
      return this.ToString(Level, false);
    }

    public String ToString(EN_ITEM_LEVEL Level, Boolean IsDefault)
    {
      switch (Level)
      {
        case EN_ITEM_LEVEL.IL_LEVEL_1:
          return this.ToMenuItemString(IsDefault);
        case EN_ITEM_LEVEL.IL_LEVEL_2:
          return this.ToSubMenuItemString();
        case EN_ITEM_LEVEL.IL_LEVEL_3:
          return String.Empty;
        case EN_ITEM_LEVEL.IL_LEVEL_NONE:
          return String.Empty;
      }
      return String.Empty;
    }

    private String ToSubMenuItemString()
    {
      String _result = String.Empty;
      String _item_desc = String.Empty;
      String _item_control = String.Empty;
      Int32 _id_val = Int32.MinValue;
      EN_SECTION_ID _section_id = 0;

      Int32.TryParse(this.ID, out _id_val);

      _section_id = (EN_SECTION_ID)Int32.Parse(this.SectionID);
      if (this.ID == "70001" || this.ID == "60002") //no se muestran mas
        return "";
      switch (this.ID)
      {
        case "60001":
          _item_desc = "jugadoresListaON";
          _item_control = "jugadoresListaControl";
          break;

        case "60002":
          _item_desc = "jugadoresCompararON";
          _item_control = "jugadoresCompararControl";
          break;

        case "70003":
          _item_desc = "terminalesListaON";
          _item_control = "terminalesListaControl";
          break;

        case "70001":
          _item_desc = "terminalesCompararON";
          _item_control = "terminalesCompararControl";
          break;

        case "40001":
          _item_desc = "alarmasListaON";
          _item_control = "alarmasListaControl";
          break;

        case "20200":
          _item_desc = "mostrarMapaTemperatura";
          _item_control = "";
          break;
        default:
          _item_desc = "ON" + this.ID;
          _item_control = "Control" + this.ID;
          break;
      }
      // TODO:  BORRAR EL RANDOM NUMBER
      Random random = new Random();

      switch (this.ActionType)
      {
        case EN_ITEM_ACTION_TYPE.IACT_LEGEND:
          if (_section_id == EN_SECTION_ID.MONITOR)
          {
            _result += "<li class='alarmaSonido'>";
            _result += "  <div class='wrapControl'>";
            _result += "    <h1 class='tituloBreak' data-nls='NLS_CREATE_FILTER'></h1>";
            _result += "    <div class='btnAlarmaMonitor'>";
            _result += "      <a class='btnCreaFiltro' onclick='onClickFilterMode(" + (Int32)_section_id + ")'></a>";
            _result += "    </div>";
            _result += "  </div>    ";
            _result += "</li>";
            _result += "<li class='edicionFiltroAlarma'>";
            _result += "  <div class='wrapControl'>";
            _result += "    <h1 class='tituloBreak' data-nls='NLS_EDIT_FILTERS'></h1>";
            _result += "    <div class='btnEdicionFiltros' id='filtermode_" + (Int32)_section_id + "' data-filtermode='0' onclick='onClickCustomAlarmEditMode();'>";
            _result += "    </div>";
            _result += "  </div>";
            _result += "</li>";
          }

          _result += "<li class='comparaControl'>";
          _result += "<div class='wrapControl' data-notificationalarm='" + (Int32)(_id_val) + "'>";
          _result += "<h1 class='tituloCentrado' data-nls='" + this.NLS + "'></h1>";

          if (_section_id == EN_SECTION_ID.PLAYERS || _section_id == EN_SECTION_ID.MACHINES)
          {
            _result += "<h2 id='filtermode_" + (Int32)_section_id + "' data-filtermode='0' class='btnCreaFiltro' onclick='onClickFilterMode(" + (Int32)_section_id + ")'>+</h2>";
          }

          _result += "</div>";
          _result += "</li>";
          _result += "<div id='div_legend_container_" + (Int32)(_id_val) + "' class='accordion'>";
          _result += "Legends here...";
          _result += "</div>";
          _result += "</li>";

          break;

        case EN_ITEM_ACTION_TYPE.IACT_CHECK:
          _result += "<li class='" + _item_control + "'>";
          _result += "<div class='wrapControl'>";
          _result += "<h1 class='tituloBreak' data-nls='" + this.NLS + "'></h1>";
          _result += "<div class='switch'>";
          _result += "<input class='cmn-toggle cmn-toggle-round' id='" + _item_desc + "' type='checkbox' ";
          _result += (this.Script != "") ? " onclick='" + this.Script + "'" : "";
          _result += ">";
          _result += "<label for='" + _item_desc + "'></label>";
          _result += "</div>";
          _result += "</div>";
          _result += "</li>";
          break;

        case EN_ITEM_ACTION_TYPE.IACT_SEARCH:
          switch (_section_id)
          {
            case EN_SECTION_ID.PLAYERS://"60":
              _result += "<li class='buscarJugador'>";
              _result += "<div class='wrapControl'>";
              _result += "<input id='inputBuscarJugadores' class='srcJugadorPanel' type='text' list='datalist1' data-nls='NLS_PLAYERS_SEARCH_PLACEHOLDER' data-nls='NLS_PLAYERS_SEARCH_PLACEHOLDER' placeholder='' value=''>";
              _result += "<datalist id='datalist1'>";
              _result += "</datalist>";
              _result += "<div class='btnBuscarJugador' id='jugadoresbtnBuscarUsuario' data-theme='true'><a href='#' data-nls='" + this.NLS + "'></a>";
              _result += "</div>";
              _result += "</div></li>";
              break;

            case EN_SECTION_ID.MACHINES://"70":
              _result += "<li class='buscarJugador'>";
              _result += "<div class='wrapControl'>";
              _result += "<input id='inputBuscarTerminales' class='srcJugadorPanel' type='text' list='datalist2' data-nls='NLS_TERMINALS_SEARCH_PLACEHOLDER' placeholder='' value=''>";
              _result += "<datalist id='datalist2'>";
              _result += "</datalist>";
              _result += "<div class='btnBuscarJugador' id='terminalesbtnBuscarTerminal' data-theme='true'><a href='#' data-nls='" + this.NLS + "'></a>";
              _result += "</div>";
              _result += "</div></li>";
              break;

            default:
              break;
          }
          break;

        default:
          _result = String.Empty;
          break;
      }
      return _result;
    }

    private String ToMenuItemString(Boolean IsDefault)
    {
      String _result = String.Empty;
      String _item_desc = String.Empty;
      EN_SECTION_ID _section_id = (EN_SECTION_ID)Int32.Parse(this.SectionID);

      //_result += "<li>";
      //_result += "<div id='" + this.ID + "' onclick='" + this.Script + "' class='" + this.CSS + "' data-act-type='" + (Int32)this.ActionType + "'>";

      // TODO: Dinamizar diseño      
      switch (_section_id)
      {
        case EN_SECTION_ID.DASHBOARD://"10":
          _item_desc = "btnDashboard";
          break;
        //case "20":
        //    _item_desc = "btnPiso";
        //    break;
        case EN_SECTION_ID.TASKS://"30":
          _item_desc = "btnMisTareas";
          break;
        case EN_SECTION_ID.ALARMS://"40":
          _item_desc = "btnAlarmas";
          break;
        case EN_SECTION_ID.CHARTS://"50":
          _item_desc = "btnCharts";
          break;
        case EN_SECTION_ID.PLAYERS://"60":
          _item_desc = "btnJugadores";
          break;
        case EN_SECTION_ID.MACHINES://"70":
          _item_desc = "btnMaquinas";
          break;
        case EN_SECTION_ID.MONITOR://"80":
          _item_desc = "btnMonitor";
          break;
      }

      switch (this.ActionType)
      {
        case EN_ITEM_ACTION_TYPE.IACT_OPEN_MENU:
        case EN_ITEM_ACTION_TYPE.IACT_CHANGE_VIEW:
        case EN_ITEM_ACTION_TYPE.IACT_NONE:
          _result += "<li id='" + _item_desc + "' ";
          if (_item_desc != String.Empty)
          {
            _result += " class='" + _item_desc + "'";
          }
          _result += ">";
          //
          _result += "<a id='a_section_" + (Int32)_section_id + "' href='#' ";
          _result += "onclick='_UI.SetMenuActiveSection(this, " + (Int32)_section_id + ");'>";
          _result += "  	<div class='imgIcon'></div>";

          switch ((Int32)_section_id)
          {
            case 30:
              _result += "<div class='badgeNotificacion severidadTec1'></div>";
              break;

            case 40:
              _result += "<div class='badgeNotificacion' id='monitorAlarmsNotification'></div>";
              break;

            default:

              break;
          }

          _result += "  	<span data-nls='" + this.NLS + "'>" + _item_desc + "</span>";
          _result += "</a>";

          break;
        default:
          break;
      }
      _result += "</li>";

      return _result;
    }

    // Constructor
    public clsMenuItem()
    {
    }

  }

  // Contains a full menu to html generation
  public class clsMenu
  {
    public EN_ITEM_LEVEL Level = EN_ITEM_LEVEL.IL_LEVEL_NONE;
    public String ID = String.Empty;                          // DIV Id
    public List<clsMenuItem> Items = new List<clsMenuItem>(); // Items

    // Html generation
    public override String ToString()
    {
      String _result = String.Empty;
      String _section_id;

      _section_id = this.ID.Substring(this.ID.Length - 2, 2).ToString();

      if (this.ID != String.Empty)
      {
        _result += "<ul id='" + this.ID + "'>";
      }
      else
      {
        _result += "<ul>";
      }

      foreach (clsMenuItem _item in this.Items)
      {
        _result += _item.ToString(this.Level);
      }

      _result += "</ul>";

      if (_section_id == "40" || _section_id == "60" || _section_id == "70" || _section_id == "80")
      {
        _result += CreateButtonsCustomAlarms(_section_id);
      }

      return _result;
    }

    /// <summary>
    /// Create Flter & Save Button
    /// </summary>
    /// <param name="SectionID">Current Section</param>
    /// <returns>String with HTML code</returns>
    private string CreateButtonsCustomAlarms(String SectionID)
    {
      String _result;

      _result = "<div class='wrapControl'>";
      _result += "<div class='btnFiltrosPanelIzq'>";
      if (SectionID != "80")
      {
        //_result += "<a href='#' onclick='ApplyFilter(" + SectionID + ")'  data-theme='true' style='visibility: hidden;'>Filtrar</a>";
        _result += "<a class='btnGuardarFiltro' id='btn_save_filter_section_" + SectionID + "' style='display: none;' href='#' onclick='DialogCreateSimpleFilter(" + SectionID + ")' data-nls='NLS_SAVE'></a>";
      }
      _result += "</div>";
      _result += "<a href='#' onclick='ClearSectionFilters(" + SectionID + ",true)' data-theme='true' style='display: none;' data-nls='NLS_CLEAN'></a>";
      _result += "</div>";

      return _result;
    }


    // Creates a menu item from the specified list of sections
    public static clsMenu FromSections(String ID, List<LayoutMenuItem> ItemsList)
    {
      clsMenu _menu = new clsMenu();
      _menu.ID = ID;

      foreach (LayoutMenuItem _item in ItemsList)
      {
        clsMenuItem _menu_item = new clsMenuItem();
        _menu_item.ID = _item.Id.ToString();
        //_menu_item.ID = ID + "_section_" + _item.Id;
        _menu_item.SectionID = _item.SectionId.ToString();
        _menu_item.NLS = _item.NLS;
        _menu_item.CSS = _item.CSS;
        _menu_item.Script = _item.Script;
        _menu_item.ActionType = _item.ActionType;

        //
        _menu.Level = _item.Level;

        _menu.Items.Add(_menu_item);
      }
      return _menu;
    }

    // Constructor
    public clsMenu()
    {
    }

  }

  public class clsMenuOptions
  {
    public clsMenuOptions()
    {
    }

    public String GetHtmlRole(List<int> Roles)
    {
      String _html_code;
      _html_code = string.Empty;

      foreach (int _role_id in Roles)
      {
        switch (_role_id)
        {
          case 1:
            _html_code += "<div id='div_" + _role_id + "'><a id='tips_" + _role_id + "' href='#' onclick='_UI.SetMenuActiveRole(this, " + _role_id + ")'; data-nls='NLS_ROLE_PCA'></a></div>";
            break;

          case 2:
            _html_code += "<div id='div_" + _role_id + "'><a id='tips_" + _role_id + "' href='#' onclick='_UI.SetMenuActiveRole(this, " + _role_id + ")'; data-nls='NLS_ROLE_OPM'></a></div>";
            break;

          case 4:
            _html_code += "<div id='div_" + _role_id + "'><a id='tips_" + _role_id + "' href='#' onclick='_UI.SetMenuActiveRole(this, " + _role_id + ")'; data-nls='NLS_ROLE_SLA'></a></div>";
            break;

          case 8:
            _html_code += "<div id='div_" + _role_id + "'><a id='tips_" + _role_id + "' href='#' onclick='_UI.SetMenuActiveRole(this, " + _role_id + ")'; data-nls='NLS_ROLE_TSP'></a></div>";
            break;

          default:
            //_html_code += "<div id='div_" + _role_id + "'><a id='tips_" + _role_id + "'>ROLE 1</a></div>";
            break;
        }

      }

      return _html_code;
    }
  }
}
