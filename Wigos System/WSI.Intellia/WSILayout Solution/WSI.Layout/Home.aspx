﻿
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="WSI.Layout.Home" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Intellia</title>
    <link rel="SHORTCUT ICON" href="design/iconos/intellia.ico"/>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no" />
</head>
<body>
<%--  <div style="position:absolute; display:inline; z-index: 2000;">
    <button onclick="_Manager.Cameras.CurrentMode = 0; _Manager.Controls.SetCurrentMode(0);">Perspective</button>
    <button onclick="_Manager.Cameras.CurrentMode = 1; _Manager.Controls.SetCurrentMode(1);">Cenital</button> 
    <button onclick="_Manager.Cameras.CurrentMode = 2; _Manager.Controls.SetCurrentMode(2);">First-Person</button>
  </div>--%>

    <script>
        var testMode = '<%= System.Configuration.ConfigurationManager.AppSettings["TestMode"].ToString() %>';
    </script>

    <script src='<%: ResolveClientUrl("~/signalr/hubs") %>'></script>
    <div id="div_dialog_background" class="dialogBack" style="display: none;">
        <div id="div_dialog_frame" class="dialogFrame">
            <div class="BtnClose close" onclick="$('#div_dialog_background').hide();">
            </div>
            <div id="div_dialog_content">
            </div>
        </div>
    </div>
    <div id="div_dialog_background_filter_mode" class="dialogBack2" style="display: none;">
        <div id="div_dialog_frame_2" class="dialogFilterCreation">
            <div id="div_dialog_content_2">
            </div>
        </div>
    </div>

    <div id="div_dialog_background_runners" class="dialogBack3" style="display: none;">
        <div id="div_dialog_frame_3" class="dialogRunners">
            <div id="div_dialog_content_3">
            </div>
        </div>
    </div>

    <div id="loading_floor" style="display: none;"></div>
    <div id="div_runner_selector">
    <ul id="div_runner_list">
    </ul>
    </div>
    <div id="div_legend_selected_detail" class="dialogLegendDetail legendMonitor" style="visibility: hidden;">
        <div id="div_legend_selected_frame" class="dialogFrame"">
            <div id="div_legend_selected_detail_content">
            </div>
        </div>
    </div>

  <div id="editor_dialogs" style="display: none;">
  </div>
    <%-- Preload alarm icons for 3D --%>
    <div id="div_icons_preload" style="display: none;">
        <img id="alarm_standard_icon" src="img/alarm.png" alt="" />
    </div>

    <div id="div_sound_alarm">
      <audio id="sound_alarm" src="snd/alarm_01.mp3" preload="auto"/>
    </div>
    <div id="div_sound_notify">
      <audio id="sound_notify" src="snd/notify.mp3" preload="auto"/>
    </div>
    <div id="div_sound_taskAlarm">
      <audio id="sound_taskAlarm" src="snd/task.mp3" preload="auto"/>
      </div>
    <%-- Loading screen for initial process --%>
    <div id="loading" class="please_wait_loading">
        <div class="ui-widget ui-widget-content ui-state-default ui-overlay-center" style="width: 100%;
            height: 2048px; background-color: #445963">
            
           
            <div id="sp_loading_step" class="wrapLogin">

<%--              <div class="logoCasino" style="position:absolute;top:0px;left:0px">
          <img class="wins-logo" src="design/img/WinsCasino.png" alt="CASINO" />
             <img class="smartfloor-logo" src="design/img/LogoCasino.png" alt="CASINO" />
            </div>--%>
               <div id="div_loader">
              
            </div>
                </div>
        </div>
    </div>
    <%--<div id="div_background" class="mapaPiso">--%>

    <div id="div_container" style="width: 100%; height:100%; background-color: ActiveCaption;">

    <div id="div_background">
        <img id="img_background" src="img/wins_map_p.png" alt="" />
        <div id="div_center">
            <div id="div_heatmapArea" class="div_heatmapArea" style="display:none;">
            </div>
            <div id="div_runnersmapArea" class="div_runnersmapArea" style="display:none;">
            </div>
        </div>
    </div>

    </div>

    <div id="div_top_toolbar" class="headerTop" data-theme="true" runat="server">
        <div id="txt_casino_name" class="nombreCasino" runat="server">
            <img id="img_casino" src="design/css/styles/img/logoIntellia.png" alt=""/>
        </div>
        <div class="userProfile">
            <div class="contenedorRol">
                <div id="imgProfile" class="imgProfile" runat="server">
                </div>
                <div id="txt_user_full_name" class="nameProfile" runat="server">
                    USER_FULL_NAME
                </div>
                <div class="rolProfile" id="txt_current_role" runat="server">
                </div>
                <div id="txt_user_nick" style="display: none;" runat="server">
                    USER_NICK
                </div>
                <!--<span class="flechaDropProfile ui-icon ui-icon-carat-1-s"></span>-->
            </div>
        </div>
        <div class="contenedor-acciones">
          <div class="contenedor-tiempo" >
            <div id="txt_database_hora" class="contenedor-tiempo-hora" >10:40
            </div>
            <div id="txt_database_fecha" class="contenedor-tiempo-fecha" >JUE 10/05
            </div>
          </div>
          <div class="floorLegend">
            <div id="txt_current_floor" class="txt_floor">
              <span>Auto Generated 2nd Floor</span>
            </div>
          </div>
          <div class="messageBtn" id="message" runat="server" title="Messages" data-nls="NLS_MESSAGES">
            <a href="#"></a>
          </div>
          <div class="SoundBtn" id="app_sound" title="Sound" data-nls="NLS_SOUND">
            <a href="#"></a>
          </div>
          <div class="floorBtn" id="" title="Select Floor" data-nls="NLS_FLOOR_CONFIG">
            <a href="#"></a>
          </div>  
          <div class="configBtn" title="Configuration" data-nls="NLS_CONFIGURATION">
              <a href="#"></a>
          </div>
        </div>
    </div>
    <div class="contenedorMenu">
        <div id="div_left_menu" class="menuPrincipal" data-theme="true" runat="server">
        </div>
        <div class="subMenu" runat="server" style="display: none;">
            <div class="btnSlide">
                <a href="javascript:void(0);">Slide</a></div>
            <div class="panelControl">
                <div id="div_left_submenus" class="insidePanel" runat="server">
                </div>
            </div>
        </div>
    </div>
    <div class="wrapContenidoComparar show">
        <h1>
            Comparacion de terminales</h1>
    </div>
    <!-- /wrapContenidoComparar -->
    <div class="menuComparar">
        <ul>
            <li class="btnCompararTerminales"><a href="#">
                <div class="imgIcon">
                </div>
                <span>Realizar Comparación</span> </a></li>
            <li class="btnAgregarTerminales"><a href="#">
                <div class="imgIcon">
                </div>
                <span>Agregar Terminales</span> </a></li>
            <li class="btnDejarComparar"><a href="#">
                <div class="imgIcon">
                </div>
                <span>Dejar de comparar</span> </a></li>
        </ul>
    </div>
    <!-- /menuComparar -->
    <div class="wrapJugadores show">
<%--        <div class="cierreLista">
            <a href="#">Cerrar Lista</a>
        </div>--%>
        <%--
    <div class="jugadoresSearchBox">
      <input type="text" class="srcJugadorPanel" value="" placeholder="Buscar Jugador">
      <div class="btnBuscarJugador" data-theme='true'>
        <a href="#">Buscar</a>
      </div>
    </div>
        --%>
<%--        <div id="div_customers_at_room_" class="insideJugadores">
            <%--<div class="tablaJugadores">
    
    Tabla aquí
    
    </div>--%>
        <%--</div>--%>--%>
    </div>
    <!-- /wrapContenidoComparar -->
    <div class="slideDerecha show panelslideRight">
        <div class="panelDerecha">
            <div class="cierraPanel">
                <a href="#">Cerrar Panel</a>
            </div>

<%-- Información Máquina --%>
<div class="infoMaquina">
  <div class="datosTerminal">
                <ul>
                        <div class="nombreTerminal">
        	                <h1 data-nls="NLS_TERMINAL">Edad</h1>
        	                <span class="popup_terminal_name"></span>
                        </div>
                        <div class="proveedorTerminal">
        	                <h1 data-nls="NLS_PROVIDER">Edad</h1>
        	                <span class="popup_terminal_provider"></span>
                        </div>

                        <div class="juegoActualTerminal">
        	                <h1 data-nls="NLS_TERMINAL_CURRENTPLAY">Juego Actual</h1>
        	                <span class="popup_terminal_currentplay"></span>
                        </div>
                        <div class="payoutTerminal">
        	                <h1 data-nls="NLS_TERMINAL_PAYOUTPLAY">Payout</h1>
        	                <span class="popup_terminal_payoutplay"></span>
                        </div>
                        <div class="enSesionTerminal">
        	                <h1 data-nls="NLS_TERMINAL_SESSION">En Sesion</h1>
        	                <span class="popup_terminal_session"></span>
                        </div>
                        <div class="linkJugadorTerminal">
        	                <div class="jugadorTerminal">
        	                <h1 data-nls="NLS_TERMINAL_LINKPLAYER">Link Jugador</h1>
                        </div>
                          <a href="#">
                          <div class="ojoJugador icoSnapshot"></div>
        	                </a>
                        </div>

 <div class="tableTerminal">
    <table class="jtable">
      <tr>
        <td style="width:28%"></td><th class="th2" data-nls="NLS_ACTUAL_SESSION" style="width:36%">Sesión Actual</th><th data-nls="NLS_PLAYER_WORKING_SESION"  class="th2" style="width:36%">Jornada</th>
      </tr>
      <tr>
        <th class="th2" data-nls="NLS_NETWIN">NetWin Teórico</th><td><span  class="popup_session_terminal_netwin">0</span></td><td><span  class="popup_day_terminal_netwin">0</span></td>
      </tr>
      <tr>
        <th class="th2" data-nls="NLS_BETAVERAGE">Apuesta Media</th><td><span  class="popup_session_terminal_bet_average">0</span></td><td><span  class="popup_day_terminal_bet_average">0</span></td>
      </tr>
      <tr>
        <th class="th2" data-nls="NLS_CHART_OPTION_STATS_PLAYED">Jugado</th><td><span  class="popup_session_terminal_played_amount">0</span></td><td><span  class="popup_day_terminal_played_amount">0</span></td>
      </tr>
      <tr>
        <th class="th2" data-nls="NLS_COIN_OUT">Ganado</th><td><span  class="popup_session_terminal_won_amount">0</span></td><td><span  class="popup_day_won_amount">0</span></td>
      </tr>
      <tr>
        <th  class="th2" data-nls="NLS_PLAYEDCOUNT">N° Jugadas</th><td><span  class="popup_session_terminal_played_count">0</span></td><td><span  class="popup_day_terminal_played_count">0</span></td>
      </tr>
    </table>
 </div> 
            <div class="btnTerminalComparar">
    	        <a href="#" data-nls="NLS_COMPARE_TERMINAL">Comparar Terminal</a>
            </div>

                    <li>

                    <div class="datosNetwin">
                        <h1 data-nls="NLS_NETWIN">Net Win</h1>
                    </div>
                        <div class="datosGraficos">



                      <div class="graf_t_netwin">
                            <div id="wid_t_netwin"></div>
                          </div>
                      
                                          <div id="terminalNetWin">
                       <div class="tituloDias">
                        <h2 data-nls="NLS_DAY1">Día 1</h2>
<%--                        <span class="popup_net_win" >5.36</span>--%>
                  </div>
                  <div class=" graf_bar_terminal_netWin1 graf_bar">
                  </div>
                      <div class="tituloDias">
                        <h2 data-nls="NLS_DAY2">Día 2</h2>
                  <%--      <span class="popup_net_win" >5.36</span>--%>
                  </div>
                  <div class="graf_bar_terminal_netWin2 graf_bar">
                  </div>
                      <div class="tituloDias">
                        <h2 data-nls="NLS_DAY3">Día 3</h2>
                      <%--  <span class="popup_net_win">5.36</span>--%>
                  </div>
                  <div class="graf_bar_terminal_netWin3 graf_bar">
                  </div>
                </div>                          
                      <%--                          <h1 data-nls="NLS_TERMINAL_LAST_DAYS">Last Visit</h1>--%>
                        </div>
                     </li> 
                  
                    <li>
                    <div class="datosBetAVG">
                        <h1 data-nls="NLS_BETAV">Apuesta Media</h1>
                    </div>
                    <div class="datosGraficos">




                          <div class="graf_t_bavg">
                            <div id="wid_t_bavg"></div>
                          </div>
                <div id="terminalAvg">
                      <div class="tituloDias">
                        <h2 data-nls="NLS_DAY1">Día 1</h2>
                      <%--  <span class="popup_terminal_bet_average colorPositive"  >1.02</span>--%>
                  </div>
                  <div class=" graf_bar_terminal_avg1 graf_bar">
                  </div>
                      <div class="tituloDias">
                        <h2 data-nls="NLS_DAY2">Día 2</h2>
                       <%-- <span class="popup_terminal_bet_average colorPositive">1.02</span>--%>
                  </div>
                  <div class="graf_bar_terminal_avg2 graf_bar">
                  </div>
                                        <div class="tituloDias">
                        <h2 data-nls="NLS_DAY3">Día 3</h2>
                       <%-- <span class="popup_terminal_bet_average colorPositive">1.02</span>--%>
                  </div>
                  <div class="graf_bar_terminal_avg3 graf_bar">
                  </div>
                </div>  
<%--                          <h1 data-nls="NLS_TERMINAL_LAST_DAYS">Last Visit</h1>--%>
                      </div>
                    </li> 
                    <li>
                   <div class="datosPlayedAmount">
                        <h1 data-nls="NLS_COININ">Coin In</h1>
                    </div>
                    <div class="datosGraficos">

                          <div class="graf_t_coinin">
                            <div id="wid_t_coinin"></div>
                          </div>
                       <div id="terminalCoinIn">
                      <div class="tituloDias">
                        <h2 data-nls="NLS_DAY1">Día 1</h2>
                       <%--<span class="popup_played_amount colorPositive">209.65</span>--%>
                  </div>
                  <div class=" graf_bar_terminal_coinin1 graf_bar">
                  </div>
                      <div class="tituloDias">
                        <h2 data-nls="NLS_DAY2">Día 2</h2>
                        <%--<span class="popup_played_amount colorPositive">209.65</span>--%>
                  </div>
                  <div class="graf_bar_terminal_coinin2 graf_bar">
                  </div>
                      <div class="tituloDias">
                        <h2 data-nls="NLS_DAY3">Día 3</h2>
                       <%-- <span class="popup_played_amount colorPositive">209.65</span>--%>
                  </div>
                  <div class="graf_bar_terminal_coinin3 graf_bar">
                  </div>
                </div>  
               <%--           <h1 data-nls="NLS_TERMINAL_LAST_DAYS">Last Visit</h1>--%>
                      </div>
                    </li>   
                    <li>
                   <div class="datosWonAmount">
                        <h1 data-nls="NLS_COIN_OUT">Ganado</h1>
                    </div>
                    <div class="datosGraficos">

                          <div class="graf_t_amount">
                            <div id="wid_t_amount"></div>
                          </div>
                   <div id="terminalWon">
                      <div class="tituloDias">
                        <h2 data-nls="NLS_DAY1">Día 1</h2>
<%--                        <span class="popup_won_amount colorPositive">204.29</span>--%>
                  </div>
                  <div class=" graf_bar_terminal_won1 graf_bar">
                  </div>
                      <div class="tituloDias">
                        <h2 data-nls="NLS_DAY2">Día 2</h2>
<%--                        <span class="popup_won_amount colorPositive">204.29</span>--%>
                  </div>
                  <div class="graf_bar_terminal_won2 graf_bar">
                  </div>
                      <div class="tituloDias">
                        <h2 data-nls="NLS_DAY3">Día 3</h2>
<%--                        <span class="popup_won_amount colorPositive">204.29</span>--%>
                  </div>
                  <div class="graf_bar_terminal_won3 graf_bar">
                  </div>
                </div>  
                      <%--    <h1 data-nls="NLS_TERMINAL_LAST_DAYS">Last Visit</h1>--%>
                      </div>
                    </li>      
                    <li>
<%--                   <div class="datosPlayedCount">
                        <h2 data-nls="NLS_PLAYS">Jugadas</h2>
                    </div>--%>
                    <div class="datosGraficos">




                        <h1 data-nls="NLS_PLAYS">Jugadas</h1>
                          <div class="graf_t_played_count">
                            <div id="wid_t_played_count"></div>
                          </div>
                 <div id="terminalPlayed">
                      <div class="tituloDias">
                        <h2 data-nls="NLS_DAY1">Día 1</h2>
                       <%-- <span class="popup_terminal_played_count colorPositive">4433</span>--%>
                  </div>
                  <div class=" graf_bar_terminal_played1 graf_bar">
                  </div>
                      <div class="tituloDias">
                        <h2 data-nls="NLS_DAY2">Día 2</h2>
                     <%--   <span class="popup_terminal_played_count colorPositive">4433</span>--%>
                  </div>
                  <div class="graf_bar_terminal_played2 graf_bar">
                  </div>
                      <div class="tituloDias">
                        <h2 data-nls="NLS_DAY3">Día 3</h2>
                        <%--<span class="popup_terminal_played_count colorPositive">4433</span>--%>
                  </div>
                  <div class="graf_bar_terminal_played3 graf_bar">
                  </div>
                </div>  
                         <%-- <h1 data-nls="NLS_TERMINAL_LAST_DAYS">Last Visit</h1>--%>
                </div>  
                    </li>                       
                </ul>
            </div>

</div> 
<%--infoalarmas--%>
<div class="infoAlarmas infoPanelAlarmas"> 
    <div class="datosTerminalAlarmas">
                        <div class="nombreTerminal">
        	<h1 data-nls="NLS_TERMINAL">Edad</h1>
        	<span class="popup_terminal_name"></span>
        </div>
                        <div class="proveedorTerminal">
        	                <h1 data-nls="NLS_PROVIDER">Edad</h1>
        	<span class="popup_terminal_provider"></span>
        </div>
            </div>
            <div class="datosAlarma">
                        <h1 data-nls="NLS_ALARMS_ACTIVE">Activas</h1>
              <div class="alarmasActivas" id="info_panel_active_alarms">
              </div>
                        <h1 data-nls="NLS_ALARMS_LASTS">Últimas</h1>
              <div class="ultimasAlarmas" id="info_panel_last_alarms">
              </div>
            </div>
</div> <%--infoalarmas--%>

<div class="infoJugador">

	<div class="datosJugador">
    
    	<div class="photoContenedor">
        	<img class="photoPlayer" src="design/img/ico_photo_male.png" />
           <div class="photoIsVIPContainer">
        	<%--<img class="photoIsVIP" src="design/img/vip.jpg" />--%>
    	    </div>
    	</div>

        
        <div class="nombreJugador">
        	<span class="popup_player_name"></span>
        </div>
        
        <div class="edadJugador">
        	<h1 data-nls="NLS_AGE">Edad</h1>
        	<span class="popup_player_age"></span>
        </div>
        
        <div class="nivelJugador">
        	<h1 data-nls="NLS_LEVEL">Nivel</h1>
        	<span class="popup_player_level"></span>
        </div>
        
        <div class="generoJugador">
        	<h1 data-nls="NLS_GENDER">Género</h1>
        	<span class="popup_player_gender"></span>
        </div>
       	      <div class="enSesionJugador">
        	<h1 data-nls="NLS_SESSION">En Sesión</h1>
        	<span data-nls="NLS_POP_YESNO_YES">SI</span>
        </div>  
        <div class="terminalJugador">
        	<div class="jugadorTerminal">
        	<h1 data-nls="NLS_LINK_TERMINAL">Link Terminal</h1>
          </div>
          <a href="#">
            <div class="ojoTerminal icoSnapshot"></div>
        	</a>
        </div>     
 <div class="tableJugador">
    <table class="jtable">
      <tr>
        <td style="width:28%"></td><th class="th2" data-nls="NLS_ACTUAL_SESSION" style="width:36%">Sesión Actual</th><th data-nls="NLS_PLAYER_WORKING_SESION"  class="th2 jqDay" style="width:36%">Jornada</th>
      </tr>
      <tr>
        <th  class="th2" data-nls="NLS_PLAYEDCOUNT">Nro. Jugadas</th><td><span  class="popup_session_played_count">0</span></td><td class="jqDay"><span  class="popup_day_played_count">0</span></td>
      </tr>
      <tr>
        <th class="th2" data-nls="NLS_CHART_OPTION_STATS_PLAYED">Jugado</th><td><span  class="popup_session_played_amount">0</span></td><td class="jqDay"><span  class="popup_day_played_amount">0</span></td>
      </tr>
      <tr>
        <th class="th2" data-nls="NLS_COIN_OUT">Ganado</th><td><span  class="popup_session_won_amount">0</span></td><td class="jqDay"><span  class="popup_day_won_amount">0</span></td>
      </tr>
            <tr>
        <th class="th2" data-nls="NLS_BETAVERAGE">Apuesta Media</th><td><span  class="popup_session_bet_average">0</span></td><td class="jqDay"><span  class="popup_day_bet_average">0</span></td>
      </tr>
            <tr>
        <th class="th2" data-nls="NLS_NETWIN">NetWin Teórico</th><td><span  class="popup_session_netwin">0</span></td><td class="jqDay"><span  class="popup_day_netwin">0</span></td>
      </tr>
            <tr>
        <th class="th2" data-nls="NLS_CASH_IN">CashIn</th><td><span  class="popup_session_cash_in">0</span></td><td class="jqDay"><span  class="popup_day_cash_in">0</span></td>
      </tr>
                  <tr>
        <th class="th2" data-nls="NLS_CASH_OUT">CashOut</th><td><span  class="popup_session_cash_out">0</span></td><td class="jqDay"><span  class="popup_day_cash_out">0</span></td>
      </tr>
    </table>
 </div> 
    </div>   
  	<div class="btnJugadorComparar">
    	<a href="#" data-nls="NLS_COMPARE_PLAYER">Comparar Jugador</a>
  </div>  
  <div id="playerInfoLoad">    
  </div>    
	<div class="datosSesion" style="visibility:hidden;">
<%--         <div class="datosJugadorTitulo">
            	<div class="tituloPlayerJornada">
            		 
                	<h1 data-nls="NLS_PLAYER_WORKING_SESION">Jornada</h1>
                </div>
                <div class="tituloPlayerActual">
            		  
                 <h1 data-nls="NLS_PLAYER_CURRENT_SESION">Actual</h1>
                </div>
            </div>--%>
         	
            <div class="datosJugadas">
            	<div class="datosPlayerJornada">
            		
                	<h1 data-nls="NLS_NUMBER_OF_PLAYS">Nº de jugadas</h1>
                </div><%-- 
              <div class="datosPlayerActual">
            		 <span class="popup_play_session_played_count">0</span> 
                 <h1 data-nls="NLS_NUMBER_OF_PLAYS">Nº de jugadas</h1>
                </div>--%>
              <div class="datosGraficos">
                 <div class="graf_played_count">
                 <div id="wid_played_count"></div>

                 </div>
                                <div id="jugadoCantidad">
	 <div class="tituloDias"><h2 data-nls="NLS_DAY1">Día 1</h2></div> 
	<div class=" graf_bar_played_count1 graf_bar"> 
	</div> 
	<div class="tituloDias"><h2 data-nls="NLS_DAY2">Día 2</h2></div> 
	<div class="graf_bar_played_count2 graf_bar" > 
	</div> 
	<div class="tituloDias"><h2 data-nls="NLS_DAY3">Día 3</h2></div> 
	<div class="graf_bar_played_count3 graf_bar" > 
    </div> 
 </div> 
               <%-- <h1 data-nls="NLS_PLAYER_LAST_VISITS">Jugado</h1>--%>
                </div>
            </div>
         
            <div class="datosJugados">
            	<div class="datosPlayerJornada">

                	<h1 data-nls="NLS_COIN_IN">Jugado</h1>
                </div><%-- 
              <div class="datosPlayerActual">
            		<span class="popup_play_session_total_played">0</span>
                	<h1 data-nls="NLS_COIN_IN">Jugado</h1>
                </div>--%>
              <div class="datosGraficos">
                  <div class="graf_played">
                   <div id="wid_played"></div>
                 </div>
                <div id="jugado">
                  <div class="tituloDias">
                    <h2 data-nls="NLS_DAY1">Día 1</h2>
                  </div>
                  <div class=" graf_bar_played1 graf_bar">
                  </div>
                  <div class="tituloDias">
                    <h2 data-nls="NLS_DAY2">Día 2</h2>
                  </div>
                  <div class="graf_bar_played2 graf_bar">
                  </div class="tituloDias">
                  <div class="tituloDias">
                    <h2 data-nls="NLS_DAY3">Día 3</h2>
                  </div>
                  <div class="graf_bar_played3 graf_bar">
                  </div>
                </div>
                <%--<h1 data-nls="NLS_PLAYER_LAST_VISITS">Jugado</h1>--%>
                  </div>
            </div>
            
            <div class="datosGanado">
            	<div class="datosPlayerJornada">

                	<h1 data-nls="NLS_COIN_OUT">Ganado</h1>
                </div><%--  
              <div class="datosPlayerActual">
            		<span class="popup_play_session_won_amount">0</span>
                	<h1 data-nls="NLS_COIN_OUT">Ganado</h1>
                </div>--%>
              <div class="datosGraficos">
                   <div class="graf_won">
                   <div id="wid_won"></div>
                   </div>
            <div id="jugadoGanado">
	 <div class="tituloDias"><h2 data-nls="NLS_DAY1">Día 1</h2></div> 
	<div class=" graf_bar_played_won1 graf_bar"> 
	</div> 
	<div class="tituloDias"><h2 data-nls="NLS_DAY2">Día 2</h2></div> 
	<div class="graf_bar_played_won2 graf_bar"> 
	</div> 
	<div class="tituloDias"><h2 data-nls="NLS_DAY3">Día 3</h2></div> 
	<div class="graf_bar_played_won3 graf_bar"> 
    </div> 
 </div> 
                               <%-- <h1 data-nls="NLS_PLAYER_LAST_VISITS">Jugado</h1>--%>
                </div>
            </div>
            
            <div class="datosApuestaM">
            	<div class="datosPlayerJornada">
            		
                	<h1 data-nls="NLS_BETAV">Apuesta Media</h1>
            	</div><%--   
              <div class="datosPlayerActual">
            		<span class="popup_play_session_bet_average">1.54</span>  
                	<h1 data-nls="NLS_BETAV">Apuesta Media</h1>
            	</div>--%>
              <div class="datosGraficos">
                   <div class="graf_betavg">
                     <div id="wid_betavg"></div>
                   </div>
            <div id="jugadoPromedio">
	 <div class="tituloDias"><h2 data-nls="NLS_DAY1">Día 1</h2></div> 
	<div class=" graf_bar_played_avg1 graf_bar"> 
	</div> 
	<div class="tituloDias"><h2 data-nls="NLS_DAY2">Día 2</h2></div> 
	<div class="graf_bar_played_avg2 graf_bar"> 
	</div> 
	<div class="tituloDias"><h2 data-nls="NLS_DAY3">Día 3</h2></div> 
	<div class="graf_bar_played_avg3 graf_bar"> 
    </div> 
 </div> 
               <%-- <h1 data-nls="NLS_PLAYER_LAST_VISITS">Jugado</h1>--%>
                </div>
            </div>
            
            <div class="datosNetWin">
            	<div class="datosPlayerJornada">
            		 
                	<h1 data-nls="NLS_NETWIN">Net Win</h1>
            	</div><%--  
              <div class="datosPlayerActual">
            		<span class="popup_play_session_net_win" class="netwin_positive">11.26</span>  
                	<h1 data-nls="NLS_NETWIN">Net Win</h1>
            	</div>--%>
              <div class="datosGraficos">
                   <div class="graf_netwin">
                     <div id="wid_netwin"></div>
                   </div>
<div id="jugadoNetWin">
	 <div class="tituloDias"><h2 data-nls="NLS_DAY1">Día 1</h2></div> 
	<div class=" graf_bar_played_netwin1 graf_bar"> 
	</div> 
	<div class="tituloDias"><h2 data-nls="NLS_DAY2">Día 2</h2></div> 
	<div class="graf_bar_played_netwin2 graf_bar"> 
	</div> 
	<div class="tituloDias"><h2 data-nls="NLS_DAY3">Día 3</h2></div> 
	<div class="graf_bar_played_netwin3 graf_bar"> 
    </div> 
 </div> 
       
             <%--   <h1 data-nls="NLS_PLAYER_LAST_VISITS">Jugado</h1>--%>
                </div>
            </div>              
	</div>



</div>

<div class="infoRunner">
  <div class="datosRunner">
    <div class="photoContenedor">
      <img class="photoRunner" src="design/img/ico_photo_male.png" />
    </div>
    <div class="nombreRunner">
      <span class="popup_runner_name">FOO</span>
    </div>
<%--    <div class="rolRunner">
      <h1 data-nls="NLS_RUNNER_ROL">Rol</h1>
      <span class="popup_runner_role"></span>
    </div>--%>
  
    <div class="runnerDispositivo">
      <h1 data-nls="NLS_RUNNER_DEVICE">Device</h1>
      <span class="popup_runner_device"></span>
    </div>
  </div>
  <div class="alarmasRunner">
  <h1 data-nls="NLS_RUNNER_CURRENT">Activas</h1>
    <div class="alarmasActivas" id="info_panel_active_runner">
    </div>
    <h1 data-nls="NLS_RUNNER_LAST">Activas</h1>
    <div class="ultimasAlarmas" id="info_panel_last_runner">
    </div>
  </div>
</div>

<div class="infoMonitor">
  <div class="monitorJugador">
	  <div class="datosJugador">
    	  <div class="photoContenedor">
        	  <img class="photoPlayer" src="design/img/ico_photo_male.png" />
    	  </div>
        
          <div class="nombreJugador">
        	  <span class="popup_player_name"></span>
          </div>
        
          <div class="edadJugador">
        	  <h1 data-nls="NLS_AGE">Edad</h1>
        	  <span class="popup_player_age"></span>
          </div>
        
          <div class="nivelJugador">
        	  <h1 data-nls="NLS_LEVEL">Nivel</h1>
        	  <span class="popup_player_level"></span>
          </div>
        
          <div class="generoJugador">
        	  <h1 data-nls="NLS_GENDER">Género</h1>
        	  <span class="popup_player_gender"></span>
          </div>
        
      </div>      
    <div id="playerInfoLoadMonitor"></div>            
	  <div class="datosSesion" style="visibility:hidden;">
           <div class="datosJugadorTitulo">
            	  <div class="tituloPlayerJornada">
        
                	  <h1 data-nls="NLS_PLAYER_WORKING_SESION">Jornada</h1>
                  </div>
                  <div class="tituloPlayerActual">
         	
                   <h1 data-nls="NLS_PLAYER_CURRENT_SESION">Actual</h1>
                  </div>
              </div>
         	
              <div class="datosJugadas">
            	  <div class="datosPlayerJornada">
            		  <span class="popup_wd_play_session_played_count">0</span> 
                	  <h1 data-nls="NLS_NUMBER_OF_PLAYS">Nº de jugadas</h1>
                  </div>
                <div class="datosPlayerActual">
            		  <span class="popup_play_session_played_count">0</span> 
                	  <h1 data-nls="NLS_NUMBER_OF_PLAYS">Nº de jugadas</h1>
                  </div>
                  <div class="datosGraficos">
                   <div class="graf_played_count">
                   <div id="wid_played_count_monitor"></div>
                  </div>
              <%--    <h1 data-nls="NLS_PLAYER_LAST_VISITS">Jugado</h1>--%>
              </div>
              </div>
            
              <div class="datosJugados">
            	  <div class="datosPlayerJornada">
            		  <span class="popup_wd_play_session_total_played">208.49</span>
                	  <h1 data-nls="NLS_COIN_IN">Jugado</h1>
                  </div>
                <div class="datosPlayerActual">
            		  <span class="popup_play_session_total_played">208.49</span>
                	  <h1 data-nls="NLS_COIN_IN">Jugado</h1>
                  </div>
                  <div class="datosGraficos">
                    <div class="graf_played">
                     <div id="wid_played_monitor"></div>
                   </div>
              <%--     <h1 data-nls="NLS_PLAYER_LAST_VISITS">Jugado</h1>--%>
                  </div>
              </div>
            
              <div class="datosGanado">
            	  <div class="datosPlayerJornada">
            		  <span class="popup_wd_play_session_won_amount">165.67</span>
                	  <h1 data-nls="NLS_COIN_OUT">Ganado</h1>
                  </div>
                <div class="datosPlayerActual">
            		  <span class="popup_play_session_won_amount">165.67</span>
                	  <h1 data-nls="NLS_COIN_OUT">Ganado</h1>
                  </div>
                  <div class="datosGraficos">
                     <div class="graf_won">
                     <div id="wid_won_monitor"></div>
                     </div>
                                <%--  <h1 data-nls="NLS_PLAYER_LAST_VISITS">Jugado</h1>--%>
                  </div>
              </div>
            
              <div class="datosApuestaM">
            	  <div class="datosPlayerJornada">
            		  <span class="popup_wd_play_session_bet_average">1.54</span>  
                	  <h1 data-nls="NLS_BETAV">Apuesta Media</h1>
            	  </div>
                <div class="datosPlayerActual">
            		  <span class="popup_play_session_bet_average">1.54</span>  
                	  <h1 data-nls="NLS_BETAV">Apuesta Media</h1>
            	  </div>
                  <div class="datosGraficos">
                     <div class="graf_betavg">
                       <div id="wid_betavg_monitor"></div>
                     </div>
               <%--   <h1 data-nls="NLS_PLAYER_LAST_VISITS">Jugado</h1>--%>
                  </div>
              </div>
            
              <div class="datosNetWin">
            	  <div class="datosPlayerJornada">
            		  <span class="popup_wd_play_session_net_win" class="netwin_positive">11.26</span>  
                	  <h1 data-nls="NLS_NETWIN">Net Win</h1>
            	  </div>
                <div class="datosPlayerActual">
            		  <span class="popup_play_session_net_win" class="netwin_positive">11.26</span>  
                	  <h1 data-nls="NLS_NETWIN">Net Win</h1>
            	  </div>
                  <div class="datosGraficos">
                     <div class="graf_netwin">
                       <div id="wid_netwin_monitor"></div>
                     </div>
                 <%-- <h1 data-nls="NLS_PLAYER_LAST_VISITS">Jugado</h1>--%>
                  </div>
              </div>              
	  </div>
  </div>
  <div class="monitorTerminal">
    <div class="infoMaquina">
  <div class="datosTerminal">
                <ul>
                        <div class="nombreTerminal">
        	                <h1 data-nls="NLS_TERMINAL">Edad</h1>
        	                <span class="popup_terminal_name"></span>
                        </div>
                        <div class="proveedorTerminal">
        	                <h1 data-nls="NLS_PROVIDER">Edad</h1>
        	                <span class="popup_terminal_provider"></span>
                        </div>
          <div class="juegoActualTerminal">
        	<h1 data-nls="NLS_SESSION">Juego Actual</h1>
        	<span >SI</span>
        </div>  
        <div class="payoutTeminal">
        	<h1 data-nls="NLS_LINK_TERMINAL">Payout </h1>
        	<span  class="popup_terminal_payout">SI</span>
        </div>    
          <div class="enSesionTerminal">
        	<h1 data-nls="NLS_SESSION">En Sesión</h1>
        	<span >SI</span>
        </div>  
        <div class="jugadorTerminal">
        	<h1 data-nls="NLS_LINK_TERMINAL">Link Terminal</h1>
        	<span  class="popup_jugador_link">SI</span>
        </div>  

                    <li>
                        <span class="popup_net_win" class="netwin_positive">5.36</span>
                        <h1 data-nls="NLS_NETWIN">Net Win</h1>
                        <div class="datosGraficos">
                          <div class="graf_t_netwin">
                            <div id="wid_t_netwin"></div>
                          </div>
                          <h1 data-nls="NLS_TERMINAL_LAST_DAYS">Last Visit</h1>
                        </div>
                     </li> 
                    <li>
                        <span class="popup_terminal_bet_average">1.02</span>
                        <h1 data-nls="NLS_BETAV">Apuesta Media</h1>
                          <div class="graf_t_bavg">
                            <div id="wid_t_bavg"></div>
                          </div>
                          <h1 data-nls="NLS_TERMINAL_LAST_DAYS">Last Visit</h1>
                    </li> 
                    <li>
                        <span class="popup_played_amount">209.65</span>
                        <h1 data-nls="NLS_COININ">Coin In</h1>
                          <div class="graf_t_coinin">
                            <div id="wid_t_coinin"></div>
                          </div>
                          <h1 data-nls="NLS_TERMINAL_LAST_DAYS">Last Visit</h1>
                    </li>   
                    <li>
                        <span class="popup_won_amount">204.29</span>
                        <h1 data-nls="NLS_COIN_OUT">Ganado</h1>
                          <div class="graf_t_amount">
                            <div id="wid_t_amount"></div>
                          </div>
                          <h1 data-nls="NLS_TERMINAL_LAST_DAYS">Last Visit</h1>
                    </li>      
                    <li>
                        <span class="popup_terminal_played_count">4433</span>
                        <h1 data-nls="NLS_PLAYS">Jugadas</h1>
                          <div class="graf_t_played_count">
                            <div id="wid_t_played_count"></div>
                          </div>
                          <h1 data-nls="NLS_TERMINAL_LAST_DAYS">Last Visit</h1>
                    </li>                       
                </ul>
            </div>
</div> 
  </div>
</div>

<div class="panelDerivar infoPanel">
        	<h3 data-nls="NLS_DERIVATE_TO">Derivar a:</h3>
                        
            <div class="derivarRol">
            	<input name="derivarInput" type="radio" value="Rol" class="radioProfile" id="derivaRol"/>
                <%--<label class="radioBtn" for="derivaRol"><span></span><label data-nls="NLS_ROLE">Rol</label></label>--%>
              <label class="radioBtn" for="derivaRol"><span></span><div data-nls="NLS_ROLE" style="display: inline-block;"></div></label>

                <input name="derivarInput" type="radio" value="Usuario" class="radioProfile" id="derivaUsuario"/>
                <label class="radioBtn" for="derivaUsuario"><span></span><div data-nls="NLS_USER" style="display: inline-block;"></div></label>
                
            </div>
            
            
             <div id="acordeon" class="accordion rolOculto">

                  <h3 data-nls="NLS_ROLES">Roles</h3>
                      <div id="RoleSelector" class="seleccionRol">
                        <ul>
                        	<li class="rolAcordeon" data-roleKey="1"><span data-nls="NLS_ROLE_1">Floor Attendant</span></li>
                            <li class="rolAcordeon" data-roleKey="2"><span data-nls="NLS_ROLE_2">OPS Manager</span></li>
                            <li class="rolAcordeon" data-roleKey="4"><span data-nls="NLS_ROLE_4">Slot Attendant</span></li>
                            <li class="rolAcordeon" data-roleKey="8"><span data-nls="NLS_ROLE_8">Technical Support</span></li>
                        </ul>
                      </div>
            </div>
            
            <div class="buscarUsuario buscarOculto">
                <div class="wrapControl">
                	<input id="derivarBuscarUsuario" type="text" class="srcJugadorPanel" value="" data-nls="NLS_SEARCH_USER" placeholder="Buscar Usuario">
                    <div class="btnBuscarJugador" id="btnBuscarUsuario"><a href="#" data-nls="NLS_SEARCH">Buscar</a></div>
                </div>    
				
                
                <!-- ejemplo de resultados de búsqueda de usuarios. Es impportante para el CSS mantener esta estructura -->
                           	
            	<div class="resultadoUsuarioRol seleccionRol">
                <ul>
<%--            		<li class="rolAcordeon"><span>Pepe Gomez</span></li>
                	<li class="rolAcordeon"><span>Alvaro Gomez</span></li>
                    <li class="rolAcordeon"><span>Jose Gomez</span></li>
                    <li class="rolAcordeon"><span>Ana Gomez</span></li>--%>
              </ul>
                </div>
            </div>
                        
            <div class="severidadAlarma">
            <h3 data-nls="NLS_SEVERITY">Severidad:</h3>
            	<input name="severidadInput" type="radio" value="3" class="radioProfile" id="severidadInput1">
                <label class="radioBtn" for="severidadInput1"><span class="inputSeveridad1"></span><label data-nls="NLS_SEVERITY_HIGH">High</label></label>
                <input name="severidadInput" type="radio" value="2" class="radioProfile" id="severidadInput2">
                <label class="radioBtn" for="severidadInput2"><span class="inputSeveridad2"></span><label data-nls="NLS_SEVERITY_MEDIUM">Medium</label></label>
                <input name="severidadInput" type="radio" value="1" class="radioProfile" id="severidadInput3">
                <label class="radioBtn" for="severidadInput3"><span class="inputSeveridad3"></span><label data-nls="NLS_SEVERITY_LOW">Low</label></label>
                
            </div>
            
            
            <div class="comentarioAccion">
            	<h3 data-nls="NLS_COMMENT">Comentario</h3>
            	<textarea draggable="false" class="comentarioAlarma"></textarea>
            </div>
            
            <div class="btn100Derecha">
            	<a href="#" id="crearAlarma" class="btnAccionDerivar" data-nls="NLS_DERIVATE">Derivar</a>
              <h3 id="errorDerivarAlarma"></h3>
            </div>
          
        </div><!-- /panelDerivar -->
        
<div class="panelInfoAlarma infoPanel">
        	<h3 data-nls="NLS_TASK_INFO">Info de la Tarea</h3>
            
            <div class="txtComentarioAlarma">
            </div>
            
        </div><!-- /panelInfoAlarma -->
        
<div class="panelModificarEstado infoPanel">
        	<h3 data-nls="NLS_MODIFY_TASK_STATE">Modificar el estado de la Tarea</h3>

             <div class="estadoAlarmas">
              <input name="derivarInput" type="radio" value="1" class="radioProfile" id="EstadoAlarmas1">
                <label class="radioBtn" for="EstadoAlarmas1"><span></span><label data-nls="NLS_STATE_PENDING">Pendiente</label></label>
              <input name="derivarInput" type="radio" value="2" class="radioProfile" id="EstadoAlarmas2">
                <label class="radioBtn" for="EstadoAlarmas2"><span></span><label data-nls="NLS_STATE_IN_PROGRESS">En Curso</label></label>  
              <input name="derivarInput" type="radio" value="3" class="radioProfile" id="EstadoAlarmas3">
                <label class="radioBtn" for="EstadoAlarmas3"><span></span><label data-nls="NLS_STATE_SOLVED">Resuelta</label></label> 
              <input name="derivarInput" type="radio" value="5" class="radioProfile" id="EstadoAlarmas5">
                <label class="radioBtn" for="EstadoAlarmas5"><span></span><label data-nls="NLS_STATE_VALIDATED">Validada</label></label> 
            </div>
            
             <div class="comentarioAccion">
            	<h3 data-nls="NLS_ALARM_COMENT">Comentario sobre la alarma</h3>
            	<textarea draggable="false" class="comentarioAlarma"></textarea>
            </div>
            
            <div class="btn100Derecha">
            	<a href="#" id="modificarTarea" class="btnAccionDerivar" data-nls="NLS_MODIFY">MODIFICAR</a>
            </div>
            
        </div><!-- /panelModificarEstado -->

      

</div>
        <!-- /panelDerecha -->
    </div>
   
    <!-- /slideDerecha -->
  <div class="slideDerechaExtendido show panelslideRight darkPanel">
        <div class="panelDerecha">
            <%--<div class="cierraPanelExtendido">
                <a href="#">Cerrar Panel</a>
            </div>--%>
          <div class="panelEnviarMensaje infoPanelExtended">
        <div class="contacts-panel">
          <h2 data-nls="NLS_CHAT_CONTACT">Contactos</h2>
          <div class="contacts">
            <div class="resultadoContactRol selectionRol">
               <ul>
               </ul>
             </div>
          </div>
        </div>  
        <div class="chatMessageIntro">
          <div class="iconLandingChat"></div>
          <div class="infoLandingChat" data-nls="NLS_NO_CHATS"> No chats here… Please select a contact to see or start a chat</div>
        </div>
        <div class="conversation-panel">
            <%--<h2 class="to-left" data-nls="NLS_CHAT_WITH">Conversación con </h2>--%>
            <h2><label id="runner-id"></label>
            </h2>
            <div id="runner-role" class="rolRunnerId" ></div>
        	<div class="conversation">
        	</div>
            <div class="conversation-error" style="display:none;">
                  <label class="messaging-error-text" data-nls="NLS_MESSAGING_ERROR"></label>
        	</div>
            <div class="send-message">
           <%--   <div>
                  <label class="messaging-error-text" data-nls="NLS_MESSAGING_ERROR"></label>
              </div>--%>
              <div>
            	<input type="text" id="txtSendMessage" class="srcJugadorPanel"/>
              <div class="btnSendMessageDerecha" data-nls="NLS_SEND">
              SEND
            	  <a href="#" id="btnSendMessage"></a>
              </div>
            </div>
            </div>
           </div>
        </div><!-- /panelEnviarMensaje -->
        </div>
    </div>
    <div id="div_report_chart_reports" runat="server" class="wrapDashboard" style="">
      
    </div>
    <!--/wrapDashboard-->
    <!-- tasks -->
    <div id="div_tasks" runat="server" class="wrapMisTareas" style="width: 93%; background: white;display: none;">
	<%--<div class="userProfile">
    </div><!--/wrapPanel--> --%>
    </div>
    <!-- /tasks -->

    <%-- CONFIGURATION --%>
    <div id="div_configuration_profile" class="configuracionProfile" data-theme="true">
        <div class="wrapConfigProfile">
            <ul>
                <li class="configIdiomas">
                    <h1 data-nls="NLS_LANGUAGES">
                        Idiomas</h1>
                    <input type="radio" id="radio01" name="radLanguageSelector" value="0" />
                    <label for="radio01"><span></span>Español</label>
                    <input type="radio" id="radio02" name="radLanguageSelector" value="1" />
                    <label for="radio02"><span></span>English</label>
                </li>
                <li class="planoPiso">
                    <div class="planoWrap">
                        <h1 data-nls="NLS_ROLE">
                            Rol</h1>
                        <div id="div_roles" runat="server" class="switch">
                        </div>
                    </div>
                </li>
                <li class="planoPiso">
                  <div class="planoWrap">
                   <h1 data-nls="NLS_FLOOR_PLANE">
                   Rol</h1>
                    <div class="switch planoSelector">
                      <select id="planoPisoSelector">
                          </select>
                        </div>
                    </div>
                </li>
              <li class="configLogOut" ><a href="login.aspx?out=1"> <span data-nls="NLS_LOGOUT">Logout</span></a> </li>
            </ul>
            <span class="versionSoftware">Version 1.0</span>
                        </div>
        <!-- /wrapConfigProfile -->
    </div>
        <%-- CONFIGURATION FLOOR--%>
    <div id="div_configuration_floor" class="configuracionFloor" data-theme="true">
        <div class="wrapConfigFloor">
          <ul>
              <li class="planoPiso">
                    <h1 data-nls="NLS_FLOOR_PLANE">
                        Rol
                    </h1>
                        <h2 data-nls="NLS_SHOW_BACKGROUND">
                            Mostrar Fondo
                        </h2>
                        <div class="switch">
                            <input id="mostrarFondoConfig" class="cmn-toggle cmn-toggle-round" type="checkbox"
                                onclick="SetBackground();" />
                            <label for="mostrarFondoConfig">
                            </label>
                        </div>
                </li>
                <li class="configLayout">

                    <div class="planoWrap">
                    <h1 data-nls="NLS_LAYOUT">Layout</h1>
                    <input type="radio" id="radio03" name="radioLayoutViewMode" value="2" />
                    <label for="radio03">
                        <span></span><label data-nls="NLS_VIEW_2D">2D</label></label>
                    <input type="radio" id="radio04" name="radioLayoutViewMode" value="1" />
                    <label for="radio04">
                        <span></span><label data-nls="NLS_VIEW_3D">3D</label></label>
                    </div>
                    <div class="planoWrap" id="radioLayoutCameraModeDiv" style="">
                      <h1 data-nls="NLS_CAMERAS">Cameras</h1>
                      <input type="radio" id="radio06" name="radioLayoutCameraMode" value="0" />
                      <label for="radio06">
                        <span></span><label data-nls="NLS_VIEW_PERSPECTIVE"></label></label>
                      <input type="radio" id="radio07" name="radioLayoutCameraMode" value="2" />
                      <label for="radio07">
                        <span></span><label data-nls="NLS_VIEW_FIRSTPERSON"></label></label>
                    </div>
                </li>
            </ul>
        </div>
        <!-- /wrapConfigProfile -->
    </div>
    <!-- /configuracionProfile -->
    <form id="form1" runat="server">
    <asp:ScriptManager ID="scmScripter" runat="server" EnablePageMethods="true">
        <Scripts>
        </Scripts>
    </asp:ScriptManager>
    </form>
    <!-- -->
</body>
</html>
