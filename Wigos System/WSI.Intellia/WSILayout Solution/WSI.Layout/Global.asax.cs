﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using WSI.Layout.classes;
using System.Threading;
using System.Web.Routing;
using Microsoft.AspNet.SignalR;
using WSI.Common;
using System.Web.Configuration;
using System.Reflection;

namespace WSI.Layout
{
  public class Global : CommonServiceHTTP
  {
    private const string VERSION = "18.041";
    private const string SVC_NAME = "Intellia Service";
    private const string PROTOCOL_NAME = "WS_INTELLIA";
    private const int SERVICE_PORT = 13040;
    private const Boolean STAND_BY = false;
    private const string LOG_PREFIX = "WS_INTELLIA";


    public static AssemblyInfo CurrentVersion = clsAssemblyUtils.GetAssemblyInfo(Assembly.GetExecutingAssembly());

    // ------------------------------------------------------------------------------------------------------------

    public static LayoutSessions Sessions = new LayoutSessions();

    // ------------------------------------------------------------------------------------------------------------

    //public static clsSessions Sessions2 = new clsSessions();

    // ------------------------------------------------------------------------------------------------------------

    public static Boolean _con_err = false;

    public static WSI.Layout.Data.classes.clsLayoutData LayoutData = new WSI.Layout.Data.classes.clsLayoutData(5000);
    //public static WSI.Layout.Data.classes.clsLayoutDashboard LayoutDashboard = new WSI.Layout.Data.classes.clsLayoutDashboard(1000 * 60 * 60 * 24);
    public static WSI.Layout.Data.classes.clsLayoutTasksData LayoutTaskData = new WSI.Layout.Data.classes.clsLayoutTasksData(5000);
    public static WSI.Layout.Data.classes.clsLayoutMetersData LayoutMetersData = new WSI.Layout.Data.classes.clsLayoutMetersData(5000);
    public static WSI.Layout.Data.classes.clsLayoutAlarmsData LayoutAlarmsData = new WSI.Layout.Data.classes.clsLayoutAlarmsData(5000);
    public static WSI.Layout.clsLayoutSystemTaskGenerator LayoutSystemTasksGen = new WSI.Layout.clsLayoutSystemTaskGenerator(5000);
    public static WSI.Layout.clsLicense IntelliaLicense = new WSI.Layout.clsLicense(5000);
    // ------------------------------------------------------------------------------------------------------------

    protected override void SetParameters()
    {
      m_service_name = SVC_NAME;
      m_protocol_name = PROTOCOL_NAME;
      m_version = VERSION;
      m_default_port = SERVICE_PORT;
      m_stand_by = STAND_BY;
      m_log_prefix = LOG_PREFIX;
      m_ip_address = WebConfigurationManager.AppSettings["Server_LocalIP"] == null ? "0.0.0.0" : WebConfigurationManager.AppSettings["Server_LocalIP"].ToString();
    } // SetParameters


    protected void Application_Start(object sender, EventArgs e)
    {
      RouteTable.Routes.MapHubs();

      Logger.Write(HttpContext.Current, "Application_Start", "TS");
      Logger.Write(HttpContext.Current, "Application: " + CurrentVersion.Name, "TS");
      Logger.Write(HttpContext.Current, "Version: " + CurrentVersion.Version, "TS");

      // Check Log files folder
      Logger.CheckFolder(HttpContext.Current.Server.MapPath("/logfiles/"));

      // Connection
      DataBaseManager.OpenConnection();
      // Wait for connection 
      Thread.Sleep(1000);

      DataBaseManager.LogWGDBState();

      _con_err = (WGDB.ConnectionState == System.Data.ConnectionState.Open);

      WGDB.OnStatusChanged += WGDB_OnStatusChanged;

      // FAV 22-JUL-2016 Start service for principal and secondary servers
      base.StartService();

      // Init background check license
      IntelliaLicense.Init();

      // Init background update activity data thread
      LayoutData.Init();
      Thread.Sleep(500);

      // Init background update meters data thread
      LayoutMetersData.Init(0);
      Thread.Sleep(500);

      // Init background update tasks data thread
      LayoutTaskData.Init();
      Thread.Sleep(500);

      // Init background update alarms data thread
      LayoutAlarmsData.Init();
      Thread.Sleep(500);

      // Init background update task gen thread
      LayoutSystemTasksGen.Init();
    }

    void WGDB_OnStatusChanged()
    {
      _con_err = (WGDB.ConnectionState == System.Data.ConnectionState.Open);
    }

    protected void Session_Start(object sender, EventArgs e)
    {
      Logger.Write(HttpContext.Current, "Session_Start");

      if (!IntelliaLicense.LicenseValid)
      {
        Server.ClearError();
        Response.Status = "404 Page Not Found";
        Logger.Write(HttpContext.Current, "Database: NOT CONNECTED");
      }

      LogUtils.ToConsole(HttpContext.Current.Request.ServerVariables);

      // Check connection state
      //if(WGDB.ConnectionState == System.Data.ConnectionState.Open)
      if (!_con_err)
      {
        Server.ClearError();
        Response.Status = "404 Page Not Found";
        Logger.Write(HttpContext.Current, "Database: NOT CONNECTED");
      }
      else
      {
        Global.Sessions.ReValidateSession(HttpContext.Current, Request);
      }
    }

    protected void Application_BeginRequest(object sender, EventArgs e)
    {
      //Logger.Write(HttpContext.Current, "Application_BeginRequest");
    }

    protected void Application_AuthenticateRequest(object sender, EventArgs e)
    {
      //Logger.Write(HttpContext.Current, "Application_Auth_Request");
    }

    protected void Application_Error(object sender, EventArgs e)
    {

      if (HttpContext.Current != null)
      {
        // Write all errors to the log
        foreach (Exception _ex in HttpContext.Current.AllErrors)
        {
          Logger.Exception(HttpContext.Current, _ex.Source, _ex);
          if (Request != null)
          {
            Logger.Write(HttpContext.Current, " * Request path: " + Request.FilePath, "ERR");
          }
        }

        // Clear current errors
        HttpContext.Current.ClearError();
      }
      else
      {
        Logger.Write(null, "Application_Error");
      }

    }

    protected void Session_End(object sender, EventArgs e)
    {
      Logger.Write(HttpContext.Current, "Session_End");

      //Server.Transfer("./Login.aspx");
    }

    protected void Application_End(object sender, EventArgs e)
    {
      // Stop service for principal and secondary servers
      base.StopService();

      Logger.Write(HttpContext.Current, "Application_End");
    }
  }
}