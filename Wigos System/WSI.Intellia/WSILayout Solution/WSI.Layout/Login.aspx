﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="WSI.Layout.Login" %>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <title>Intellia</title>
  <link rel="SHORTCUT ICON" href="design/iconos/intellia.ico"/>
  <script type="text/javascript" language="javascript" src="design/js/jquery-1.11.2.min.js"></script>
  <script type="text/javascript" language="javascript" src="js/WSILanguage.js"></script>
  <script type="text/javascript" language="javascript">
    window.onload = function () {

      //- Browser Language -//
      _Language.CurrentLanguage = _Language.getLanguageFromNavigator();

      //- Translation -//
      $("#loginScreen_Title").text(_Language.get("NLS_LOGIN_LOGIN"));
      $("#loginBox_UserName").attr("placeHolder", _Language.get("NLS_LOGIN_USER"));
      $("#loginBox_Password").attr("placeHolder", _Language.get("NLS_LOGIN_PASSWORD"));
      $("#loginBox_Button1").text(_Language.get("NLS_LOGIN_ACCESS"));

      //- Initial Focus -//
      var _tb = document.getElementById('loginBox_UserName');
      if (_tb) {
        _tb.placeHolder = _Language.get("NLS_LOGIN_USER");
        _tb.focus();
      }

      //if (window.chrome == null) {
      //  $("#divRecomendator").text("designed for Chrome");
      //}

    }
    function box_move(objTextBox, objTargetId) {
      if (window.event.keyCode == 13) {
        document.getElementById(objTargetId).focus();
      }
    }
    function button_click(objTextBox, objBtnID) {
      if (window.event.keyCode == 13) {
        document.getElementById(objBtnID).focus();
        document.getElementById(objBtnID).click();
      }
    }
  </script>
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>
<body class="bodyLogin">

  <!-- style=" position: absolute; width: 100%; text-align: center; font-style: italic; color: #c0c0c0; " -->
  <div id="divRecomendator" >
    <div class="pusher"></div>
    <img class="wins_logo_bottom" alt="CASINO" src="design/img/logo_win_bottom.png">
  </div>

  <form class="loginBox" id="form1" runat="server">
  <asp:Login ID="loginBox" runat="server" PasswordLabelText="Contraseña:"
    UserNameLabelText="Usuario:" DisplayRememberMe="False" FailureText="Usuario o Contraseña incorrectos. Inténtelo de nuevo."
    DestinationPageUrl="~/Home.aspx" OnAuthenticate="loginBox_Authenticate" RememberMeText=""
    RenderOuterTable="false">
    <LayoutTemplate>
    <div class="wrap">
      <div class="wrapLogin">
        <div class="logoCasino">
          <img class="wins-logo" src="design/img/logo_intellia.png" alt="CASINO" />
        </div>
        <%--<h1 id="loginScreen_Title">Login</h1>--%>
        <div class="inputsLogin">
          <%-- <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">Usuario:</asp:Label> --%>
          <asp:TextBox ID="UserName" runat="server" CssClass="usuarioLogin" placeHolder="Usuario" onkeypress="box_move(this, 'loginBox_Password');"></asp:TextBox>
      <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="UserName"
        ErrorMessage="User Name is required." ToolTip="User Name is required." ValidationGroup="loginBox">* </asp:RequiredFieldValidator>

          <%-- <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password">Contraseña:</asp:Label> --%>
          <asp:TextBox ID="Password" runat="server" CssClass="passwordLogin" placeHolder="Password" TextMode="Password" onkeypress="button_click(this, 'loginBox_Button1');"></asp:TextBox>
      <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="Password"
        ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="loginBox">* </asp:RequiredFieldValidator>

          <p class="FailureText">
            <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
          </p>

        </div>
        <div class="btnLogin">
          <%-- <asp:Button ID="Button1" runat="server" CommandName="Login" Text="Acceder" ValidationGroup="loginBox" /> --%>
          <asp:LinkButton ID="Button1" runat="server" CommandName="Login" Text="Acceder" ValidationGroup="loginBox" />
        </div>
      </div>
    </div>
    </LayoutTemplate>
  </asp:Login>
  </form>
</body>
</html>
