
function displayInicial() {
  $('.subMenu').addClass('slideLeft');
  animaPanelInicial()
  return false
};

function animaPanelInicial() {
  setTimeout(function () {
    mostrarPanelControl();
  }, 30);
  return false;
}

function mostrarPanelControl() {
  $('.subMenu').switchClass('slideLeft', 'slideRight', 300);
  $('.btnSlide').switchClass('slideShow', 'slideHide', 300);
}

function ocultarPanelControl() {
  $('.subMenu').switchClass('slideRight', 'slideLeft', 300);
  $('.btnSlide').switchClass('slideHide', 'slideShow', 300);
}

function mostrarSlideButton() {
  $('.btnSlide').css('visibility', 'visible');
}

function ocultarSlideButton() {
  $('.btnSlide').css('visibility', 'hidden');
}

function animaPanelControl() {
  if ($('.subMenu').hasClass('slideLeft')) {
    mostrarPanelControl();
    $("#div_legend_selected_detail").css("visibility", "hidden");
  } else {
    ocultarPanelControl()
    $("#div_legend_selected_detail").css("visibility", "visible");
  };

  return false;
}

//function mostrarPanelDerecha() {
function ocultarPanelDerecha() {
  $('.slideDerecha').switchClass('panelslideLeft', 'panelslideRight', 500);
}

//function ocultarPanelDerecha() {
function mostrarPanelDerecha() {
  $('.slideDerecha').switchClass('panelslideRight', 'panelslideLeft', 500);
}

function mostrarPanelDerechaExtendido() {
  if (!$('.messageBtn').hasClass('close')) {
    $('.messageBtn').toggleClass('close');
    $('.badgeMessage').hide();
  }
  $('.slideDerechaExtendido').switchClass('panelslideRight', 'panelslideLeft', 500);

  //Set ToolTip if Panel is Open or Hidden
  if ($('.messageBtn').hasClass('close'))
    $('.messageBtn').prop("title", _Language.get("NLS_CLOSE")).prop("data-nls", "NLS_CLOSE");
  else
    $('.messageBtn').prop("title", _Language.get("NLS_MESSAGES")).prop("data-nls", "NLS_MESSAGES");
}

function ocultarPanelDerechaExtendido() {
  if ($('.messageBtn').hasClass('close')) {
    $('.messageBtn').toggleClass('close');
  }
  $('.slideDerechaExtendido').switchClass('panelslideLeft', 'panelslideRight', 500);

  //Set ToolTip if Panel is Open or Hidden
  if ($('.messageBtn').hasClass('close'))
    $('.messageBtn').prop("title", _Language.get("NLS_CLOSE")).prop("data-nls", "NLS_CLOSE");
  else
    $('.messageBtn').prop("title", _Language.get("NLS_MESSAGES")).prop("data-nls", "NLS_MESSAGES");
}

function animaPanelDerecha() {


  if ($('.slideDerecha').hasClass('panelslideLeft')) {
    //mostrarPanelDerecha();
    ocultarPanelDerecha();
  } else {
    //ocultarPanelDerecha();
    mostrarPanelDerecha();
  };
  return false
}

function animaPanelDerechaExtendido() {
  if ($('.slideDerechaExtendido').hasClass('panelslideLeft')) {
    ocultarPanelDerechaExtendido();
  } else {
    //ocultarPanelDerecha();
    mostrarPanelDerechaExtendido();
  };
  return false
}


function cierraPanelDerecha() {
  if ($('.slideDerecha').hasClass('hide')) {
    $('.slideDerecha').animate({
      right: '-=100%'
    }, 300, function () {
      $('.slideDerecha').css('display', 'none');
    });
    $('.slideDerecha').removeClass('hide').addClass('show');
  }
}

function mostrarMenuComparar() {
  $('.menuComparar').switchClass('panelslideRight', 'panelslideLeft', 500);
}

function ocultarMenuComparar() {
  $('.menuComparar').switchClass('panelslideLeft', 'panelslideRight', 500);
}



function animaMenuComparar() {
  if ($('#capturaON').is(':checked')) {
    mostrarMenuComparar();
    ocultarPanelControl();
    $('.btnCompararTerminales').css('display', 'block');
    $('.btnAgregarTerminales').css('display', 'none');
    //animaPanelDerecha();

  } else {

    ocultarMenuComparar()
    if ($('.wrapContenidoComparar').hasClass('hide')) {

      $('.wrapContenidoComparar').animate({
        right: '-=106%'
      }, 300, function () {
        $('.wrapContenidoComparar').css('display', 'none');
      });

      $('.wrapContenidoComparar').removeClass('hide').addClass('show');
    }
  }
}

function mostrarComparaciones() {
  if ($('.wrapContenidoComparar').hasClass('show')) {
    $('.wrapContenidoComparar').css('display', 'block');
    $('.wrapContenidoComparar').animate({
      right: '+=106%'
    });
    $('.wrapContenidoComparar').removeClass('show').addClass('hide');
    $('.btnCompararTerminales').css('display', 'none');
    $('.btnAgregarTerminales').css('display', 'block');
  }
}

function dejarComparar() {
  if ($('.wrapContenidoComparar').hasClass('hide')) {
    $('.wrapContenidoComparar').animate({
      right: '-=106%'
    }, 300, function () {
      $('.wrapContenidoComparar').css('display', 'none');
    });

    ocultarMenuComparar()

    $('.wrapContenidoComparar').removeClass('hide').addClass('show');
    $('#capturaON').attr('checked', false);
    $('.btnCompararTerminales').css('display', 'block');
    $('.btnAgregarTerminales').css('display', 'none');

  } else {
    ocultarMenuComparar()

    $('#capturaON').attr('checked', false);
  }
}

function cerrarListaJugadores(Action) {
  $('.wrapJugadores').animate({
    right: '-=100%'
  }, 300, function () {
    $('.wrapJugadores').css('display', 'none');
    if (Action != undefined) { Action(); }
  });

  mostrarSlideButton();
  $('#jugadoresListaON').attr('checked', false);
}

function cerrarCompararJugadores() {
  $('.wrapJugadores').animate({
    right: '-=100%'
  }, 300, function () {
    $('.wrapJugadores').css('display', 'none');
  });

  mostrarSlideButton();
  $('#jugadoresCompararON').attr('checked', false);
}

function cerrarListaTerminales(Action) {
  $('.wrapJugadores').animate({
    right: '-=100%'
  }, 300, function () {
    $('.wrapJugadores').css('display', 'none');
    if (Action != undefined) { Action(); }
  });

  mostrarSlideButton();
  $('#terminalesListaON').attr('checked', false);
}

function cerrarCompararTerminales() {
  $('.wrapJugadores').animate({
    right: '-=100%'
  }, 300, function () {
    $('.wrapJugadores').css('display', 'none');
  });
  mostrarSlideButton();

  $('#terminalesCompararON').attr('checked', false);
}

function cerrarListaAlarmas() {
  $('.wrapJugadores').animate({
    right: '-=100%'
  }, 300, function () {
    $('.wrapJugadores').css('display', 'none');
  });

  mostrarSlideButton();
  $('#alarmasListaON').attr('checked', false);
}

function slideConfig() {
  $('.configuracionProfile').slideToggle('slow')
  $('.configBtn').toggleClass('close');

  //Set ToolTip if Panel is Open or Hidden
  if ($('.configBtn').hasClass('close'))
    $('.configBtn').prop("title", _Language.get("NLS_CLOSE")).prop("data-nls", "NLS_CLOSE");
  else
    $('.configBtn').prop("title", _Language.get("NLS_CONFIGURATION")).prop("data-nls", "NLS_CONFIGURATION");
}

function slideConfigFloor() {
  $('.configuracionFloor').slideToggle('slow')
  $('.floorBtn').toggleClass('close');

  //Set ToolTip if Panel is Open or Hidden
  if ($('.floorBtn').hasClass('close'))
    $('.floorBtn').prop("title", _Language.get("NLS_CLOSE")).prop("data-nls", "NLS_CLOSE");
  else
    $('.floorBtn').prop("title", _Language.get("NLS_FLOOR_CONFIG")).prop("data-nls", "NLS_FLOOR_CONFIG");
}

function slideMessage() {
  animaPanelDerechaExtendido();
  var _MessageManager = MessageManager.getInstance();
  _MessageManager.InitConversation('', '');
}

function updateOrientation() {
  switch (window.orientation) {
    case -90:
    case 90:
      $('.leyendaPortrait').css("display", "none");
      break;
    default:
      //alert('portrait');
      $('.leyendaPortrait').css("display", "block");
      break;
  }
}

function muestraAlarmas() {


  $('.btnTodasAlarmas').text(function (i, v) {
    return v === 'OCULTAR ALARMAS' ? 'MOSTRAR TODAS LAS ALARMAS' : 'OCULTAR ALARMAS'
  })
  $('.tablaHide').toggle('slow');

}

function muestraTareas() {
  $('.tablaTareas').each(function () {
    var $this = $(this);
    if ($this.find('tr').length > 5) {
      $this.find('tr:nth-child(n+6)').addClass('tareasHide');
    }
  });

}

function muestraTareas() {

  $('.btnTodasTareas').text(function (i, v) {
    return v === 'OCULTAR TAREAS' ? 'MOSTRAR TODAS LAS TAREAS' : 'OCULTAR TAREAS'
  })
  $('.tareasHide').toggle('slow');


}
$(document).ready(function () {

  //Primer animación del Panel de Control
  displayInicial();

  //Acción del boton de Slide del Panel de Control
  $('.btnSlide a').click(function () {
    animaPanelControl();
  });

  // Tweak para el height del acordeon
  //$('.accordion').accordion({
  //    heightStyle: 'content'
  //  });

  //Acción del modo comparar
  $('#capturaON').change(function () {
    animaMenuComparar();
  });

  //  //Slide Panel Info Derecha
  //  $('.div_terminal').click(function () {
  //    animaPanelDerecha();
  //  });

  //Slide Panel Jugadores
  $('.div_jugadores').click(function () {
    animaPanelDerecha();
  });


  $('.cierraPanel a').click(function () {
    animaPanelDerecha();
    $('.tablaJugadores tr').removeClass('selected');

    if (_LayoutMode == 2) {
      //Unselect Terminals
      $($("#" + m_terminal_id_selected))
        .css('border', '1px solid black')
        .css('z-index', '2');

      if ($($("#" + m_terminal_id_selected)).attr("selected")) {
        $($("#" + m_terminal_id_selected))
        .css("top", ($("#" + m_terminal_id_selected).attr('initial_top')))
        .css("left", ($("#" + m_terminal_id_selected).attr('initial_left')))
        .removeAttr("selected");
      }

      //Unselect Runners
      $($("#runner_" + m_runner_id_selected))
        .css('border', '1px solid black')
        .css('z-index', '2');

      if ($($("#runner_" + m_runner_id_selected)).attr("selected")) {
        $($("#runner_" + m_runner_id_selected))
        .css("top", ($("#runner_" + m_runner_id_selected).attr('initial_top')))
        .css("left", ($("#runner_" + m_runner_id_selected).attr('initial_left')))
        .removeAttr("selected");
      }
    }

    m_terminal_id_selected = null;
    m_runner_id_selected = null;
  });

  //Slide panel donde carga info de comparaciones
  $('.btnCompararTerminales a').click(function () {
    mostrarComparaciones();
  });

  //Acción para dejar de comparar
  $('.btnDejarComparar a').click(function () {
    dejarComparar();
  });

  //Muestra oculata el panel de configuración
  $('.configBtn a').click(function () {
    $('.messageBtn').toggleClass('close');
    $('.badgeMessage').hide();
    ocultarPanelDerechaExtendido();
    slideConfig();
  });

  //Muestra oculata el panel de configuración
  $('.floorBtn a').click(function () {
    $('.messageBtn').toggleClass('close');
    $('.badgeMessage').hide();
    ocultarPanelDerechaExtendido();
    slideConfigFloor();
  });

  //Muestra oculata el panel de mensajeria
  $('.messageBtn a').click(function () {
    if($(".toast8-title").length > 0)
    {
      toastr8.clear();
    }
    slideMessage();
    
  });

  //Slide Panel Info Jugadores
  $('.btnDetallesJugadores').click(function () {
    animaPanelDerecha();
    //Pinta row cuando está seleccionada
    $(this).closest('tr').addClass('selected').siblings().removeClass('selected');
  });

  //Cierre lista jugadores
  $('.cierreLista a').click(function () {
    cerrarListaJugadores();
  });

  //Localiza jugador
  $('.btnLocalizarJugador').click(function () {
    cerrarListaJugadores();
    //Función para localizar jugador

  });

  //Slider
  $(function () {
    $("#sldZoom").slider();
  });



  //Muestra y esconde alarmas
  $('.tablaAlarmas').each(function () {
    var $this = $(this);
    if ($this.find('tr').length > 5) {
      $this.find('tr:nth-child(n+6)').addClass('tablaHide');
    }
  });


  //$('.btnTodasAlarmas').click(function () {
  //  muestraAlarmas();
  //});

  //  $('.btnTodasTareas').click(function () {
  //  muestraTareas();
  //});

  $('.tablaTareas').each(function () {
    var $this = $(this);
    if ($this.find('tr').length > 5) {
      $this.find('tr:nth-child(n+6)').addClass('tareasHide');
    }
  });


  $('.btnTodasTareas').click(function () {
    muestraTareas();
  });

  //  //$('.AdjuntoIcono').click(function () {
  ////	$(this).toggleClass('AdjuntoIconoOn');
  ////  });

  //  $('.btnDerivarAlarma').click(function () {
  //    animaPanelDerecha();
  //    $('.panelDerecha').find('.infoPanel').css("display", "none")
  //    $('.panelDerivar').css("display", "block");
  //  });

  $('.btnInfoTareas').click(function () {
    animaPanelDerecha();
    $('.panelDerecha').find('.infoPanel').css("display", "none")
    $('.panelInfoAlarma').css("display", "block");
  });

  $('.btnsModificarEstado').click(function () {
    animaPanelDerecha();
    $('.panelDerecha').find('.infoPanel').css("display", "none")
    $('.panelModificarEstado').css("display", "block");
  });

  //Agregar clase para boton de filtros #filtermode_70
  $('.btnCreaFiltro').click(function () {
    $(this).toggleClass("filtersActivated")
  });

  $(function () {
    $('#derivaRol').click(function () {
      if ($(this).is(':checked')) {
        $('.buscarUsuario').addClass("buscarOculto");
        //$('.accordion').removeClass("rolOculto");
        $('#acordeon').removeClass("rolOculto");
      }
    });
  });

  $(function () {
    $('#derivaUsuario').click(function () {
      if ($(this).is(':checked')) {
        $('.buscarUsuario').removeClass("buscarOculto");
        //$('.accordion').addClass("rolOculto");
        $('#acordeon').addClass("rolOculto");
      }
    });
  });

  //$('.seleccionRol li').click(function (){
  //	if ($(this).hasClass("rolSeleccionado")) {
  //		$(this).removeClass("rolSeleccionado")
  //	} else {
  //		$(this).addClass("rolSeleccionado");
  //	}
  //});


  $('.AdjuntoImagen').click(function () {
    $(this).addClass('photoPlayer');
  });


  $('.circuloAlarma').click(function () {
    $(this).toggleClass('circuloAlarmaOn');
  });


  //Función para el cambio de opacidad cuando escogen un color

  $(".selectorColor li").on("click", function () {
    $(this).animate({ "opacity": "1" });
    $(this).siblings().animate({ "opacity": "0.3" });
  });


  $('.btnAlarmaMonitor').click(function () {
    $(this).toggleClass('btnAlarmaMonitorOff');
  });


  $('.btnEdicionFiltros').click(function () {
    $(this).toggleClass('btnEdicionFiltrosOff');
  });

  //$('.filtrarTareas').click(function () {
  //		$('.filtrarTareas').toggleClass('filtrarTareasClose');
  //		$('.selectorNivelTareas').toggleClass('selectorNivelTareasShow');
  //	});
  //	
  //	$('.filtrarAlarmas').click(function () {
  //		$('.filtrarAlarmas').toggleClass('filtrarTareasClose');
  //		$('.selectorNivelAlarma').toggleClass('selectorNivelAlarmaShow');
  //	});


  document.addEventListener("orientationchange", updateOrientation);


});
