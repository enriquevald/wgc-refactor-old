﻿///////////////////////////////////////////////////////////////////////////////////////////////////////////
// DashBoard reports styles

var m_dash_report_occupation_colors = ['#03BB03', '#FF0004'];

var m_dash_report_occupation_vips_colors = ['#CDCD02', '#EEEEEE'];

var m_dash_report_occupation_levels_colors = ['#EEEEEE', '#90ed7d', '#80699B', '#CDCD02', '#FF0000'];

var m_dash_report_played_colors = ['#FF0004', '#03BB03'];

var m_dash_report_occupation_gender_colors = [];
    m_dash_report_occupation_gender_colors["Male"] = '#0079FD';
    m_dash_report_occupation_gender_colors["Female"] = '#FF2ADE';

var m_dash_report_occupation_age_colors = ['#f7a35c', '#8085e9', '#f15c80',
                                           '#7cb5ec', '#434348', '#90ed7d',
                                           '#e4d354', '#8085e8', '#8d4653',
                                           '#91e8e1', '#4572A7', '#AA4643',
                                           '#89A54E', '#80699B', '#3D96AE',
                                           '#DB843D', '#92A8CD', '#A47D7C',
                                           '#B5CA92'];

var m_dash_report_occupation_age_sex_colors = ['#0079FD', '#FF2ADE', '#f15c80',
                                               '#7cb5ec', '#434348', '#90ed7d',
                                               '#e4d354', '#8085e8', '#8d4653',
                                               '#91e8e1', '#4572A7', '#AA4643',
                                               '#89A54E', '#80699B', '#3D96AE',
                                               '#DB843D', '#92A8CD', '#A47D7C',
                                               '#B5CA92'];

// m_dash_report_occupation_alarms_colors[0] = Clean machines
// m_dash_report_occupation_alarms_colors[1] = Total Alarms
// m_dash_report_occupation_alarms_colors[N] = Different alarm types (order uncontrolled)
var m_dash_report_occupation_alarms_colors = ['#EEEEEE', '#FF0000', '#f15c80',
                                              '#7cb5ec', '#434348', '#90ed7d',
                                              '#e4d354', '#8085e8', '#8d4653',
                                              '#91e8e1', '#4572A7', '#AA4643',
                                              '#89A54E', '#80699B', '#3D96AE',
                                              '#DB843D', '#92A8CD', '#A47D7C',
                                              '#B5CA92'];

///////////////////////////////////////////////////////////////////////////////////////////////////////////
// Charts reports styles

var m_colors_occupation = {
  '1': '#1BA81D', 'Idle': '#1BA81D',
  '2': '#FF0000', 'Anonymous': '#FF0000',
  '3': '#880000', 'Registered': '#880000'
};
var m_colors_age = {
  'Unknown': '#1BA81D',
  '18 - 34': '#729CF7',
  '35 - 54': '#86D15A',
  '55 - 74': '#FFFF00',
  '75+': '#FF0000'
};
var m_colors_level = {
  'Unknown': '#D572E0', 'Anonymous': '#D572E0',
  '1': '#C47229', 'Bronze': '#C47229',
  '2': '#C0C4CF', 'Silver': '#C0C4CF',
  '3': '#F2AF3A', 'Gold': '#F2AF3A',
  '4': '#1BA81D', 'Diamond': '#1BA81D'
};
var m_colors_gender = {
  'Unknown': '#B3B3B3',
  '1': '#1771BF', 'Male': '#1771BF',
  '2': '#EA91ED', 'Female': '#EA91ED'
};

var m_series_default_colors = ['#7cb5ec', '#434348', '#90ed7d',
                               '#f7a35c', '#8085e9', '#f15c80',
                               '#e4d354', '#8085e8', '#8d4653',
                               '#91e8e1', '#4572A7', '#AA4643',
                               '#89A54E', '#80699B', '#3D96AE',
                               '#DB843D', '#92A8CD', '#A47D7C', 
                               '#B5CA92'];

///////////////////////////////////////////////////////////////////////////////////////////

var m_charts_default_title_font = { "font-size": "14px" };
var m_charts_default_subtitle_font = { "font-size": "10px" };
var m_charts_default_xAxis_font = { "font-size": "10px" };
var m_charts_default_yAxis_font = { "font-size": "10px" };
var m_charts_default_labels_font = { "font-size": "10px" };