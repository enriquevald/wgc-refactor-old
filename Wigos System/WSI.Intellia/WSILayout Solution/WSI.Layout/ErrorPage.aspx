﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ErrorPage.aspx.cs" Inherits="WSI.Layout.ErrorPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <title>No license</title>
  <link rel="stylesheet" href="/design/css/estilo.css" type="text/css" />  <script type="text/javascript" language="javascript" src="design/js/jquery-1.11.2.min.js"></script>
  <script type="text/javascript" language="javascript" src="js/WSILanguage.js"></script>
  <script type="text/javascript" language="javascript">
    window.onload = function () {

      //- Browser Language -//
      _Language.CurrentLanguage = _Language.getLanguageFromNavigator();

      //- Translation -//
      $(".texto").text(_Language.get("NLS_NO_LICENSE"));
      $(".boton").text(_Language.get("NLS_RETURN"));
    }
  </script>
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>
<body style="background-color:#36464e;text-align:center">
       <img style="width: 30%;margin-top: 10%;margin-bottom: 10%;" src="design/img/logo_intellia.png"/>
        <p class="texto" style="color:white;font-family:Tahoma;font-size:large;margin-bottom: 3%">
        Se ha producido un error intentando cargar los datos.
        </p>
        <a href="/Login.aspx" class="boton" style="margin-bottom: 3%;color:white">Volver / Return</a>
        <p></p>
        <img style="width:5%;margin-top:10%" alt="CASINO" src="design/img/logo_win_bottom.png"/> 

</body>
</html>
