﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Web;

namespace WSI.Layout
{
  public static class LogUtils
  {

    public static void ToConsole(NameValueCollection ServerVariables)
    {
      Debug.WriteLine("=== SERVER_VARIABLES ===");
      foreach (String _key in ServerVariables)
      {
        Debug.WriteLine(_key + "\t:\t" + ServerVariables[_key].ToString());
      }
      Debug.WriteLine("=======================");
    }

  }
}
