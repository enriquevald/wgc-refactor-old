﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Text;
using System.Web.Configuration;
using System.Threading;
using System.Xml;
using System.Xml.Serialization;

namespace WSI.Layout
{

  // 
  // Logger: Basic log messages class
  //
  public static class Logger
  {

    // Enable/Dissable log
    // TODO: Check if we must load this value from general param
    private static Boolean m_enabled = true;
    public static Boolean Enabled
    {
      get { return m_enabled; }
      set { m_enabled = value; }
    }

    [Serializable(), XmlRoot(ElementName="LOGENTRY", Namespace="")]
    public class clsLoggerEntry
    {
      private DateTime m_datetime;
      private String m_threat;
      private String m_context;
      private String m_text;
      private String m_type;

      [XmlElement("DATETIME")]
      public DateTime LogDateTime
      {
        get { return m_datetime; }
        set { m_datetime = value; }
      }

      [XmlElement("THREAT")]
      public String ThreatName
      {
        get { return m_threat; }
        set { m_threat = value; }
      }

      [XmlElement("CONTEXT")]
      public String ContextId
      {
        get { return m_context; }
        set { m_context = value; }
      }

      [XmlElement("TEXT"), XmlText]
      public String LogText
      {
        get { return m_text; }
        set { m_text = value; }
      }

      [XmlElement("TYPE")]
      public String LogType
      {
        get { return m_type; }
        set { m_type = value; }
      }

      public clsLoggerEntry()
      {
      }

      public clsLoggerEntry(DateTime pDateTime, String pThreatName, String pContextId, String pText, String pLogType)
      {
          this.m_datetime = pDateTime;
          this.m_threat = pThreatName;
          this.m_context = pContextId;
          this.m_text = pText;
          this.m_type = pLogType;
      }

      public override String ToString()
      {
        XmlSerializer xmlSerializer = new XmlSerializer(this.GetType());
        StringBuilder _xml_string = new StringBuilder();
        XmlWriter textWriter = XmlWriter.Create(_xml_string, new XmlWriterSettings()
        { OmitXmlDeclaration = true, Indent = false });
        XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
        ns.Add("","");
        xmlSerializer.Serialize(textWriter, this, ns);
        return _xml_string.ToString();        
      }

      public String ToText()
      {
        return "[" + this.m_type + "] " + this.m_threat + " | " + this.m_context + "\n\n" + this.m_text;
      }
    }

    private static readonly object m_lock = new object();

    // Obtains the current filename
    public static String FileName
    {
      get
      {
        //return DateTime.Today.Ticks + ".log";
        return DateTime.Today.Ticks + ".xml";
      }
    }

    // Check for the specified folder
    public static void CheckFolder(String Folder)
    {
      if (!Directory.Exists(Folder))
      {
        try
        {
          Directory.CreateDirectory(Folder);
        }
        catch (Exception _ex)
        {
          //
          Console.Write(_ex.Message);
          _ex = null;
        }
      }
      Logger.Write(HttpContext.Current, "Logger folder: '" + Folder + "'", "TS");
    }



    private static void WriteToFile(clsLoggerEntry Entry)
    {
      if (m_enabled)
      {

        try
        {
          lock (m_lock)
          {
            System.Diagnostics.EventLogEntryType _l_type;
            Entry.ThreatName = Thread.CurrentThread.Name == null ? "default" : Thread.CurrentThread.Name;

            //TextWriter _log_file = new StreamWriter(Path.Combine(HttpRuntime.AppDomainAppPath, "logfiles\\" + FileName), true);
            //_log_file.WriteLine(Entry.ToString());
            //_log_file.Flush();
            //_log_file.Close();

            _l_type = (Entry.LogType == "ERR") ? System.Diagnostics.EventLogEntryType.Error : System.Diagnostics.EventLogEntryType.Information;

            System.Diagnostics.EventLog.WriteEntry("IntelliaBackEnd", Entry.ToText(), _l_type);

            StreamWriter _sw;
            _sw = File.AppendText(Path.Combine(HttpRuntime.AppDomainAppPath, "logfiles\\" + FileName));
            _sw.WriteLine(Entry.ToString());
            _sw.Close();
          }
        }
        catch (Exception ex)
        {
          System.Diagnostics.Debug.Print(ex.ToString());

          System.Diagnostics.EventLog.WriteEntry("IntelliaBackEnd", ex.StackTrace,
                                         System.Diagnostics.EventLogEntryType.Error);
        }

      }
    }

        // Writes log message for the specified context (session,...)
    public static void Write(HttpContext Context, String Message)
    {
      Write(Context, Message, "LOG");
    }

    // Writes log message for the specified context (session,...)
    public static void Write(HttpContext Context, String Message, String LogType)
    {
      try
      {
        clsLoggerEntry _entry = new clsLoggerEntry();
        _entry.LogDateTime = DateTime.Now;
        _entry.ContextId = (Context == null || Context.Session == null) ? "[NO_CONTEXT]" : Context.Session.SessionID;
        _entry.LogText = Message;
        _entry.LogType = LogType;
        WriteToFile(_entry);
      }
      catch (Exception ex)
      {
        System.Diagnostics.Debug.Print(ex.ToString());
      }
    }

    // Logs an exception message
    public static void Exception(HttpContext Context, String Method, Exception ex)
    {
      Write(Context, Method + " | " + ex.ToString(), "ERR");
    }

  }
}