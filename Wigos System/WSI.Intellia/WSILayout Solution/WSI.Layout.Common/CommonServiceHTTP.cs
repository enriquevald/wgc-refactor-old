﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Web;
using WSI.Common;

namespace WSI.Layout
{
  public class CommonServiceHTTP : System.Web.HttpApplication
  {
    protected ManualResetEvent m_ev_stop = new ManualResetEvent(false);
    protected ManualResetEvent m_ev_stopped = new ManualResetEvent(false);
    protected ManualResetEvent m_ev_run = new ManualResetEvent(true);

    protected Thread m_main_thread = null;
    private Thread m_shutdown_thread = null;
    private String m_shutdown_reason = "";

    protected String m_service_name = "Unknown";
    protected String m_version = "CC.BBB";
    protected String m_log_prefix = "LOG";
    protected String m_protocol_name = "PPP";
    protected String m_alarm_source = @"Machine\\Unknown";

    protected int m_default_port = 0;
    protected Boolean m_stand_by = false;
    protected string m_ip_address = "";


    public WaitHandle StopEvent
    {
      get
      {
        return m_ev_stop;
      }
    } // StopEvent

    protected bool Paused
    {
      get
      {
        return !m_ev_run.WaitOne(0);
      }
    } // Paused

    protected bool StopRequested
    {
      get
      {
        return m_ev_stop.WaitOne(0);
      }
    } // StopRequested

    protected void StopService()
    {
      RequestOrderlyShutdown("STOP");

    } // OnStop

    protected void RequestOrderlyShutdown(String Reason)
    {
      if (m_shutdown_thread != null)
      {
        return;
      }
      m_shutdown_thread = new Thread(OrderlyShutdwonThread);
      m_shutdown_reason = Reason;
      m_shutdown_thread.Start();
    }

    private void OrderlyShutdwonThread()
    {
      try
      {
        int _tick;
        int _tick_task;
        int _tick_log;
        int _pending_tasks;
        int _prev_pending;
        Int32 _queued_tasks;
        Int32 _running_tasks;

        // Tick Stop requested
        _tick = Misc.GetTickCount();
        _tick_log = _tick;

        Alarm.Register(AlarmSourceCode.Service, 0, m_alarm_source, AlarmCode.Service_Stopping);

        Logger.Write(HttpContext.Current, "STOP REQUESTED: " + m_shutdown_reason + " ----------------------------------------------------", "TS");

        // Set the Stop event
        m_ev_stop.Set();

        WorkerPool.RequestStop();

        if (m_main_thread.Join(0))
        {
          ServiceStatus.SetServiceStatus(m_protocol_name, m_ip_address, m_stand_by, m_version, "STOPPING");
        }

        _prev_pending = int.MaxValue;
        _tick_task = Misc.GetTickCount();
        while (!WorkerPool.CanShutdown(out _queued_tasks, out _running_tasks))
        {
          _pending_tasks = _queued_tasks + _running_tasks;
          if (_pending_tasks < _prev_pending)
          {
            _prev_pending = _pending_tasks;
            _tick_task = Misc.GetTickCount();
          }

          if (Misc.GetElapsedTicks(_tick_task) >= 10000)
          {
            if (_pending_tasks > 0)
            {
              Logger.Write(HttpContext.Current, "  - Remaining tasks: Queued/Running = " + _queued_tasks.ToString() + "/" + _running_tasks.ToString() + ", aborting ...", "TS");
              _tick_log = Misc.GetTickCount();
            }

            break;
          }

          if (Misc.GetElapsedTicks(_tick_log) >= 1000)
          {
            Logger.Write(HttpContext.Current, "  - Remaining tasks: Queued/Running = " + _queued_tasks.ToString() + "/" + _running_tasks.ToString() + ", stopping ...", "TS");
            _tick_log = Misc.GetTickCount();
          }

          if (m_main_thread.Join(0))
          {
            ServiceStatus.SetServiceStatus(m_protocol_name, m_ip_address, m_stand_by, m_version, "STOPPING");
          }
          Thread.Sleep(100);
        }


        if (!m_main_thread.Join(10000))
        {
          try { m_main_thread.Abort(); }
          catch { }
        }

        ServiceStatus.SetServiceStatus(m_protocol_name, m_ip_address, m_stand_by, m_version, "STOPPED");
        Alarm.Register(AlarmSourceCode.Service, 0, m_alarm_source, AlarmCode.Service_Stopped);

        Logger.Write(HttpContext.Current, "Stopped.", "TS");

        m_ev_stopped.Set();

        Environment.Exit(0);
      }
      catch (Exception _ex)
      {
        Logger.Write(HttpContext.Current, "Thread exception:", "TS");
        Logger.Exception(HttpContext.Current, "CommonServiceHTTP.OrderlyShutdwonThread()", _ex);
      }
    }

    protected void StartService()
    {
      // Start the Main Thread
      m_main_thread = new Thread(CommonServiceMainRoutine);
      m_main_thread.Name = "Service Main Thread";

#if !DEBUG
      m_main_thread.Start();
#endif

    } // OnStart

    protected virtual void SetParameters()
    {
      throw new Exception("Not implemented!");
    } // SetParameters

    private string CurrentVersion
    {
      get
      {
        Assembly _assembly;
        Version _version;

        _assembly = Assembly.GetExecutingAssembly();
        _version = _assembly.GetName().Version;

        return _version.Major.ToString("00") + "." + _version.Minor.ToString("000");
      }
    } // CurrentVersion

    protected void CommonServiceMainRoutine()
    {
      try
      {
        m_version = CurrentVersion;
        SetParameters();
        m_alarm_source = Environment.MachineName + @"\\" + m_service_name;

        Logger.Write(HttpContext.Current, String.Format("Starting {0} Version: {1}", m_service_name.Trim(), m_version.Trim()), "TS");

        // Init resources
        WSI.Common.Resource.Init();

        IPEndPoint _service_ip_endpoint;

        // Get Local IpAddress
        if (String.IsNullOrEmpty(m_ip_address))
        {
          m_ip_address = IPAddress.Any.ToString();

          // TODO: ...
           Logger.Write(HttpContext.Current, " No Discovery Service IP address is available. Application stopped", "TS");

          return;
        }

        // WWP IP EndPoint
        _service_ip_endpoint = new IPEndPoint(IPAddress.Parse(m_ip_address), m_default_port);

        Alarm.Register(AlarmSourceCode.Service, 0, m_alarm_source, AlarmCode.Service_Started);

        if (m_stand_by)
        {
          Alarm.Register(AlarmSourceCode.Service, 0, m_alarm_source, AlarmCode.Service_StandBy);
        }

        if (ServiceStatus.InitService(m_ev_stop, m_protocol_name, m_ip_address, m_stand_by, m_version))
        {
          Alarm.Register(AlarmSourceCode.Service, 0, m_alarm_source, AlarmCode.Service_Running);

          Logger.Write(HttpContext.Current, string.Format("Service [{0}] running", m_service_name), "TS");
          ServiceStatus.RunningService(m_ev_stop, m_protocol_name, m_ip_address, m_stand_by, m_version);
        }

        Logger.Write(HttpContext.Current, string.Format("Service [{0}] stopping", m_service_name), "TS");
        ServiceStatus.SetServiceStatus(m_protocol_name, m_ip_address, m_stand_by, m_version, "STOPPING");
      }
      catch (Exception _ex)
      {
        Logger.Write(HttpContext.Current, "Thread exception:", "TS");
        Logger.Exception(HttpContext.Current, "CommonServiceHTTP.CommonServiceMainRoutine()", _ex);
      }
    } // CommonServiceMainRoutine
  }
}
