﻿//------------------------------------------------------------------------------
// Copyright © 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: clsLicense.cs
// 
//   DESCRIPTION: Implements the following classes:
//                1. clsLicense
// 
//        AUTHOR: Jaume Barnés
// 
// CREATION DATE: 08-FEB-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 08-FEB-2016 JBC    Initial version.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Threading;
using System.Data;
using System.Data.SqlClient;
using WSI.Common;
using WSI.Layout;
using System.Web;

using System.Diagnostics;
using System.Web.Configuration;

namespace WSI.Layout
{
  public class clsLicense
  {
    /// <summary>
    /// Multithread lock
    /// </summary>
    private static readonly object m_lock = new object();

    /// <summary>
    /// Thread Interval
    /// </summary>
    private Int32 m_interval = 5000;

    /// <summary>
    /// Thread delegate
    /// </summary>
    private delegate void ThreadWorker();
    private Thread m_thread;
    private Boolean m_license_valid_shared = true;
    private static String m_current_feature = "Intellia";
    private Boolean m_license_valid = true;

    /// <summary>
    /// Shared data by the thread
    /// </summary>
    public Boolean LicenseValid
    {
      get
      {
        lock (m_lock) { return this.m_license_valid_shared; }
      }
    }

    /// <summary>
    /// Initializes the thread with the worker
    /// </summary>
    private void Initialize(ThreadWorker work)
    {
      m_thread = new Thread(new ThreadStart(work));
      m_thread.Name = "Lisence_Main";
      m_thread.Start();
      List<String> _features = new List<string>();

      _features.Add(m_current_feature);

      Misc.StartLicenseChecker(OnLicenseStatusChanged, false, _features);
    }

    private void OnLicenseStatusChanged(LicenseStatus LicenseStatus)
    {
      switch (LicenseStatus)
      {
        case LicenseStatus.VALID:
        case LicenseStatus.VALID_EXPIRES_SOON:
          m_license_valid = true;
          break;

        case LicenseStatus.NOT_AUTHORIZED:
        case LicenseStatus.INVALID:
        case LicenseStatus.EXPIRED:
        case LicenseStatus.NOT_CHECKED:
          m_license_valid = false;
          break;

        default:
          break;
      }

    }

    /// <summary>
    /// General initialization
    /// </summary>
    public void Init()
    {
      this.m_interval = WSI.Common.GeneralParam.GetInt32("Intellia", "Activity.Interval", 5000);

      this.Initialize(this.Task);
    }
    /// <summary>
    /// Default Constructor
    /// </summary>
    public clsLicense()
      : this(5000)
    {
    }

    /// <summary>
    /// Constructor for the class
    /// </summary>
    /// <param name="Interval">Thread delay</param>
    public clsLicense(Int32 Interval)
    {
      this.m_interval = Interval;
      //this.Init();
    }

    /// <summary>
    /// 
    /// </summary>
    private void Task()
    {
      while (true)
      {
        lock (m_lock)
        {
#if !DEBUG
          if (!Services.IsPrincipal("WS_INTELLIA"))
          {
            Thread.Sleep(1000);
            continue;
          }
#endif

         m_license_valid_shared = m_license_valid;

#if DEBUG
         m_license_valid_shared = true;
#endif
        }

        Thread.Sleep(this.m_interval);
      }
    }
  }
}
