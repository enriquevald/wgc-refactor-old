﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace WSI.Layout
{
  public class AssemblyInfo
  {
    public String Name;
    public String Version;

    public AssemblyInfo(String Name, String Version)
    {
      this.Name = Name;
      this.Version = Version;
    }
  }

  public static class clsAssemblyUtils
  {
    public static AssemblyInfo GetAssemblyInfo(Assembly Library)
    {
      Version _version = Library.GetName().Version;
      AssemblyProductAttribute _product = Library.GetCustomAttributes(typeof(AssemblyProductAttribute), true).FirstOrDefault() as AssemblyProductAttribute;

      if (_version != null && _product != null)
      {
        return new AssemblyInfo(_product.Product, String.Format("{0}.{1}.{2}.{3}", _version.Major, _version.Minor, _version.Build, _version.Revision));
      }
      else
      {
        return new AssemblyInfo(String.Empty, String.Empty);
      }
    }

    public static AssemblyInfo GetAssemblyInfo()
    {
      Assembly _assembly = Assembly.GetExecutingAssembly();
      return GetAssemblyInfo(_assembly);
    }

  }
}
