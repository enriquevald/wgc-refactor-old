﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using WSI.Common;
using WSI.Layout;

namespace WSI.Layout
{
  public static class clsIntl
  {

    private const string C_DEFAULT_LANGUAGE_NAME = "en-US";

    public static string GetSystemLanguageName()
    {
      String _lang_name = C_DEFAULT_LANGUAGE_NAME;
      String _query = String.Empty;
      DataSet _data;

      _query += "SELECT   GP_KEY_VALUE ";
      _query += "  FROM   GENERAL_PARAMS ";
      _query += " WHERE   GP_GROUP_KEY   = 'WigosGUI'";
      _query += "   AND   GP_SUBJECT_KEY = 'Language'";

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _data = clsDatabase.ExecuteQueryString(_query, _db_trx.SqlTransaction);

          if (_data != null)
          {
            if (_data.Tables.Count == 1 && _data.Tables[0].Rows.Count == 1)
            {
              _lang_name = _data.Tables[0].Rows[0][0].ToString().Trim().ToUpper();
            }
          }
        }
        
      }
      catch (Exception _ex)
      {
        Logger.Exception(null, "GetSystemLanguageName", _ex);
      }

      return _lang_name;
    }

  }
}
