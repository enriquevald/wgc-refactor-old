﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WSI.Common;
using System.Data;
using System.Data.SqlClient;
using WSI.Layout;
using System.Web.Security;

namespace WSI.Layout
{

  public static class clsDefaultConverter
  {
    // Converts a DataRow in a String Array
    private static String DataTableRowToString(DataRow Row)
    {
      List<String> _cells = new List<String>();

      for (Int32 _idx = 0; _idx < Row.ItemArray.Length; _idx++)
      {
        //_cells.Add("\"" + JsonConvert.ToString(Row[_idx].ToString()) + "\"");
        _cells.Add("\"" + Row[_idx].ToString() + "\"");
      }

      return String.Join(",", _cells.ToArray());
    }

    // Converts a collection of rows in a String Array
    private static String DataTableRowsToString(DataRowCollection Rows)
    {
      List<String> _rows = new List<String>();

      foreach (DataRow _row in Rows)
      {
        _rows.Add("[" + DataTableRowToString(_row) + "]");
      }

      return String.Join(",", _rows.ToArray());
    }

    // Returns a string with the column names array
    private static String DataTableColumnsToString(DataColumnCollection Columns)
    {
      List<String> _columns = new List<String>();

      foreach (DataColumn _column in Columns)
      {
        _columns.Add("\"" + _column.Caption + "\"");
      }

      return String.Join(",", _columns.ToArray());
    }

    // Converts a DataTable in a String with the next JSON format:
    /*
     * { columns: [ ColName1, ColName2, ... ],
     *   rows:    [
     *              [ Row1Col1Value, Row1Col2Value, ... ],
     *              [ Row2Col1Value, Row2Col2Value, ... ],
     *              ...   
     *            ]
     *  }
     */
    public static String DataTableToReport(DataTable Table)
    {
      String _columns = String.Empty;

      _columns = "[" + DataTableColumnsToString(Table.Columns) + "]";

      String _rows = String.Empty;

      _rows = "[" + DataTableRowsToString(Table.Rows) + "]";

      return "{\"columns\":" + _columns + ",\"rows\":" + _rows + "}";
    }

    public static DataTable DataTableFromReport(dynamic ReportData)
    {
      DataTable _result = new DataTable();
      Int32 _i;
      DataRow _datarow;
      //Type _col_type;

      for (_i = 0; _i < ReportData.columns.Count; _i++)
      {
        //_col_type = (ReportData.rows[0][_i] == null) ? typeof(String) : ReportData.rows[0][_i].GetType();

        //_result.Columns.Add(ReportData.columns[_i].Value, _col_type);

        _result.Columns.Add(ReportData.columns[_i].Value, typeof(String));
      }

      foreach (var _row in ReportData.rows)
      {
        _datarow = _result.NewRow();

        for (_i = 0; _i < ReportData.columns.Count; _i++)
        {
          _datarow[_i] = _row[_i].Value;
        }
        _result.Rows.Add(_datarow);
      }

      return _result;
    }

  }



  ////////////////////////////////////////////////////////////////////////////////////

  public static class clsJsonConverters
  {
    public static String ConvertDataSetToJson(List<String> PropertyNames, DataSet DSet)
    {
      StringBuilder sb = new StringBuilder();
      StringWriter sw = new StringWriter(sb);

      try
      {
        JsonWriter jsonWriter = new JsonTextWriter(sw);

        jsonWriter.WriteStartArray();

        for (int _idx = 0; _idx < DSet.Tables.Count; _idx++)
        {
          jsonWriter.WriteStartArray();

          foreach (DataRow _dr in DSet.Tables[_idx].Rows)
          {
            jsonWriter.WriteStartObject();
            jsonWriter.WritePropertyName(PropertyNames[_idx]);

            jsonWriter.WriteStartArray();
            for (int index = 0; index < _dr.ItemArray.Count(); index++)
            {
              jsonWriter.WriteValue(_dr.ItemArray[index].ToString());
            }
            jsonWriter.WriteEndArray();

            jsonWriter.WriteEndObject();
          }

          jsonWriter.WriteEndArray();

        }

        jsonWriter.WriteEndArray();
      }
      catch (Exception exception)
      {
        Logger.Exception(HttpContext.Current, "ConvertDataSetToJson()", exception);
      }

      return sb.ToString();
    }

    public static String ConvertDataTableToJson(String PropertyName, DataTable DTable)
    {
      StringBuilder sb = new StringBuilder();
      StringWriter sw = new StringWriter(sb);

      JsonWriter jsonWriter = new JsonTextWriter(sw);

      jsonWriter.WriteStartArray();

      foreach (DataRow _dr in DTable.Rows)
      {
        try
        {
          jsonWriter.WriteStartObject();
          jsonWriter.WritePropertyName(PropertyName);

          jsonWriter.WriteStartArray();
          for (int index = 0; index < _dr.ItemArray.Count(); index++)
          {
            jsonWriter.WriteValue(_dr.ItemArray[index].ToString());
          }
          jsonWriter.WriteEndArray();

          jsonWriter.WriteEndObject();

        }
        catch (Exception _ex)
        { // exception
          Logger.Exception(HttpContext.Current, "ConvertDataTableToJson()", _ex);
        }
      }

      jsonWriter.WriteEndArray();

      return sb.ToString();
    }


    public static String ConvertSegmentationAgroupsToJson(Reports.AgroupNamesFromSegmentation Data)
    {
      StringBuilder sb = new StringBuilder();
      StringWriter sw = new StringWriter(sb);

      JsonWriter jsonWriter = new JsonTextWriter(sw);

      try
      {
        jsonWriter.WriteStartObject();

        // adt_group
        jsonWriter.WritePropertyName("adt_group");
        jsonWriter.WriteStartArray();
        foreach (KeyValuePair<int, string> _dic in Data.adt_group)
        {
          jsonWriter.WriteValue(_dic.Value);
        }
        jsonWriter.WriteEndArray();

        // game_type
        jsonWriter.WritePropertyName("game_type");
        jsonWriter.WriteStartArray();
        foreach (KeyValuePair<int, string> _dic in Data.game_type)
        {
          jsonWriter.WriteValue(_dic.Value);
        }
        jsonWriter.WriteEndArray();

        // holder_gender
        jsonWriter.WritePropertyName("holder_gender");
        jsonWriter.WriteStartArray();
        foreach (KeyValuePair<int, string> _dic in Data.holder_gender)
        {
          jsonWriter.WriteValue(_dic.Value);
        }
        jsonWriter.WriteEndArray();

        // holder_level
        jsonWriter.WritePropertyName("holder_level");
        jsonWriter.WriteStartArray();
        foreach (KeyValuePair<int, string> _dic in Data.holder_level)
        {
          jsonWriter.WriteValue(_dic.Value);
        }
        jsonWriter.WriteEndArray();

        // years_old
        jsonWriter.WritePropertyName("years_old");
        jsonWriter.WriteStartArray();
        foreach (KeyValuePair<int, string> _dic in Data.years_old)
        {
          jsonWriter.WriteValue(_dic.Value);
        }
        jsonWriter.WriteEndArray();

        jsonWriter.WriteEndObject();
      }
      catch (Exception exception)
      { // exception
        Logger.Exception(HttpContext.Current, "ConvertSegmentationAgroupsToJson()", exception);
      }

      return sb.ToString();
    }
  }

  public static class clsJsonUtils
  {

    public static String GetPropertyValue(String JsonString, String Property, String DefaultValue)
    {
      String _result = String.Empty;
      _result = GetPropertyValue(JsonString, Property);
      if (_result == String.Empty) {
         _result = DefaultValue;
      } 
      return _result;
    }

    public static String GetPropertyValue(String JsonString, String Property)
    {
      JsonTextReader _reader = new JsonTextReader(new StringReader(JsonString));
      while (_reader.Read())
      {
        if (_reader.TokenType == JsonToken.PropertyName)
        {
          if (_reader.Value != null && _reader.Value.ToString() == Property)
          {
            if (_reader.Read())
            {
              return _reader.Value != null ? _reader.Value.ToString() : String.Empty;
            }
          }
        }
      }
      return String.Empty;
    }
    
    public static String StringEncodeForJSON(String Str)
    {
      string _str_return;

      _str_return = System.Web.HttpUtility.UrlEncode(Str);
      _str_return = _str_return.Replace("+", "%20");

      return _str_return;
    }

    public static JObject ConvertToJson(string json)
    {
      return JObject.Parse(json);
    }

    public static string ConvertToString(JObject jsonObject)
    {
      return jsonObject.ToString();
    }   
  }
}
