﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;

namespace WSI.Layout
{

  /// <summary>
  /// 
  /// </summary>
  public static class clsDatabase
  {

    /// <summary>
    ///   
    /// </summary>
    /// <param name="Method"></param>
    /// <param name="Parameters"></param>
    /// <param name="Transaction"></param>
    /// <returns></returns>
    public static String ExecuteQuery(String Method, SqlParameter[] Parameters, SqlTransaction Transaction)
    {
      return ExecuteQuery(Method, Parameters, 0, Transaction);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="Method"></param>
    /// <param name="Parameters"></param>
    /// <param name="Transaction"></param>
    /// <returns></returns>
    public static String ExecuteQuery(String Method, SqlParameter[] Parameters, Int32 ReturnTable, SqlTransaction Transaction)
    {
      String _result = String.Empty;

      try
      {

        using (SqlCommand _cmd = new SqlCommand(Method, Transaction.Connection, Transaction))
        {
          _cmd.CommandType = CommandType.StoredProcedure;
          _cmd.CommandTimeout = 2 * 60; // 2 minutes
          if (Parameters != null)
          {
            _cmd.Parameters.AddRange(Parameters);
          }
          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
          {
            using (DataSet _dataset = new DataSet())
            {
              _sql_da.Fill(_dataset);

              _result = clsDefaultConverter.DataTableToReport(_dataset.Tables[ReturnTable]);

            }
          }
        }

      }
      catch (Exception _ex)
      {
        Logger.Exception(null, "ExecuteQuery", _ex);
      }

      return _result;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /// <summary>
    /// 
    /// </summary>
    /// <param name="Query"></param>
    /// <param name="Transaction"></param>
    /// <returns></returns>
    public static DataSet ExecuteQueryString(String Query, SqlTransaction Transaction)
    {
      DataSet _result = new DataSet();

      try
      {

        using (SqlCommand _cmd = new SqlCommand(Query, Transaction.Connection, Transaction))
        {
          _cmd.CommandTimeout = 2 * 60; // 2 minutes
          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
          {
            _sql_da.Fill(_result);
          }
        }

      }
      catch (Exception _ex)
      {
        Logger.Exception(null, "ExecuteQueryString", _ex);
      }

      return _result;
    }
  }
}
