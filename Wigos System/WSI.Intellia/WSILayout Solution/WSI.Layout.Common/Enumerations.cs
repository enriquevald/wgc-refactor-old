﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WSI.Layout
{

  public enum EN_RESOURCE_TYPES 
  {
    RT_GEOMETRY = 0,
    RT_TEXTURE = 1
  }
  
  public enum EN_LAYOUT_OBJECT_TYPES
  {
    LOT_SYSTEM = 0,
    LOT_TERMINAL = 1,
    LOT_GAMING_TABLE = 2,
    LOT_GAMING_TABLE_SEAT = 3,

    LOT_3D_CAMERA = 20,

    LOT_BEACON = 70,

    LOT_SMARTPHONE = 100,

    LOT_PLAYER_MALE = 200,
    LOT_PLAYER_FEMALE = 201
  }

  public enum EN_OBJECT_STATUS
  {
    OS_INSERTED = 1,
    OS_DELETED = 2
  }


  public enum EN_OBJECT_LOCATION_FIELDS
  {
    lo_id = 0,
    lo_external_id = 1,
    lo_type = 2,
    lo_parent_id = 3,
    lo_mesh_id = 4,
    lo_creation_date = 5,
    lo_update_date = 6,
    lo_status = 7,
    lol_object_id = 8,
    lol_location_id = 9,
    lol_date_from = 10,
    lol_date_to = 11,
    lol_orientation = 12,
    lol_area_id = 13,
    lol_bank_id = 14,
    lol_bank_location = 15,
    lol_current_location = 16,
    lol_offset_x = 17,
    lol_offset_y = 18,
    //
    svg_id = 19
  }

  public enum EN_LOCATION_FIELDS
  {
    loc_id = 0,
    loc_x = 1,
    loc_y = 2,
    loc_creation = 3,
    loc_type = 4,
    loc_floorId = 5,
    loc_gridId = 6
  }


}
