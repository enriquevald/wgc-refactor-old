﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WSI.Layout
{
  public static class clsStrings
  {

    public static String EncodeTo64(String ToEncode)
    {
      byte[] _toEncodeAsBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(ToEncode);
      String _returnValue = System.Convert.ToBase64String(_toEncodeAsBytes);
      return _returnValue;
    }

    public static  String DecodeFrom64(String EncodedData)
    {
      byte[] _encodedDataAsBytes = System.Convert.FromBase64String(EncodedData);
      String _returnValue = System.Text.ASCIIEncoding.ASCII.GetString(_encodedDataAsBytes);
      return _returnValue;
    }
  }
}
