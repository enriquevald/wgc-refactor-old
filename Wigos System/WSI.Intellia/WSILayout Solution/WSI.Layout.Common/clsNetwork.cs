﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace WSI.Layout
{
  public static class clsNetwork
  {

    private const String C_LOCALHOST_NAME = "LOCALHOST";
    private const String C_LOCALHOST_ADDRESS = "127.0.0.1";

    public static String CheckServiceAddress(String ServiceAddress)
    {
      // RMS - Ensure broker address on local host, works with the IP4
      if (ServiceAddress.ToUpper() == C_LOCALHOST_NAME || ServiceAddress.ToUpper() == C_LOCALHOST_ADDRESS)
      {
        IPHostEntry _host = Dns.GetHostEntry(Dns.GetHostName());
        foreach (IPAddress _ip in _host.AddressList)
        {
          if (_ip.AddressFamily == AddressFamily.InterNetwork)
          {
            return _ip.ToString();
          }
        }
      }
      return ServiceAddress;
    }
  }
}
