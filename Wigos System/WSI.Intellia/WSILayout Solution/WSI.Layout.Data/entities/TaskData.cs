﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WSI.Layout.Data.entities
{
  public class TaskData
  {
    public Int64 ID;
    public Int32 Status;
    public DateTime Start;
    public DateTime End;
    public Int32 Category;
    public Int32 Subcategory;
    public Int32 TerminalId;
    public Int64 AccountId;
    public String Description;
    public Int32 Severity;
    public Int32 CreationUserId;
    public DateTime Creation;
    public Int32 AssignedRoleId;
    public Int32 AssignedUserId;
    public DateTime Assigned;
    public Int32 AcceptedUserId;
    public DateTime Accepted;
    public Int32 ScaleFromUserId;
    public Int32 ScaleToUserId;
    public String ScaleReason;
    public DateTime Scale;
    public Int32 SolvedUserId;
    public DateTime Solved;
    public Int32 ValidateUserId;
    public DateTime Validate;
    public Int32 LastStatusUpdateUserId;
    public DateTime LastStatusUpdate;
    public Int64 AttachedMedia;
    public String History;
    public String Title;
  }
}
