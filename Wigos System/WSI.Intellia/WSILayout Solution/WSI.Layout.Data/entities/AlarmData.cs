﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WSI.Layout.Data.entities
{
  public class AlarmData
  {
    public Int32 TerminalId;
    public Int64 ID;
    public Int32 AlarmType;
    public DateTime DateCreated;
    public Int32 UserCreated;
    public Int64 Media;
    public String Description;
    public Int32 AlarmSource;
    public Int32 Status;
    public Int64 TaskId;
    public DateTime DateToTask;
    public Int64 AlarmId;
    public String Title;
  }
}
