﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Threading;
using System.Data;
using System.Data.SqlClient;
using WSI.Common;
using WSI.Layout;
using System.Web;

using System.Diagnostics;

using System.Web.Configuration;

namespace WSI.Layout.Data.classes
{
  public class clsLayoutMetersData
  {
    /// <summary>
    /// Multithread lock
    /// </summary>
    private static readonly object m_lock = new object();

    /// <summary>
    /// Thread Interval
    /// </summary>
    private Int32 m_interval = 5000;

    /// <summary>
    /// Thread delegate
    /// </summary>
    private delegate void ThreadWorker();
    private Thread m_thread;

    private String m_data = String.Empty;

    private static String m_site_meters_now = String.Empty;

    /// <summary>
    /// Shared data by the thread
    /// </summary>
    public String Data
    {
      get
      {
        lock (m_lock) { return this.m_data; }
      }
    }

    public static String SiteMetersNow
    {
      get
      {
        { return m_site_meters_now; }
      }
    }

    /// <summary>
    /// Device id to query data
    /// </summary>
    private Int16 DeviceId;

    /// <summary>
    /// Initializes the thread with the worker
    /// </summary>
    /// <param name="work">The method to execute by the thread</param>
    private void Initialize(ThreadWorker work)
    {
      m_thread = new Thread(new ThreadStart(work));
      m_thread.Name = "LayoutData_Meters";
      m_thread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
      m_thread.Start();
    }

    /// <summary>
    /// General initialization
    /// </summary>
    public void Init(Int16 DeviceId)
    {
      this.DeviceId = DeviceId;
      this.Initialize(this.Task);
    }

    /// <summary>
    /// Main task for the thread
    /// </summary>
    private void Task()
    {
      Stopwatch _time_control;
      Int64 _elapsed;
      String _select;
      String _stored = String.Empty;

      Logger.Write(HttpContext.Current, "Thread starting:", "TS");
      Logger.Write(HttpContext.Current, " - Wait time : " + this.m_interval + "ms", "TS");

      Thread.Sleep(this.m_interval / 5);

      while (true)
      {
        try
        {
#if !DEBUG
          if (!Services.IsPrincipal("WS_INTELLIA"))
          {
            Thread.Sleep(1000);
            continue;
          }
#endif
          _stored = "SP_Get_Meters";

          if (WebConfigurationManager.AppSettings["TestMode"].ToString() == "true")
          {
            _stored = "SP_Get_Meters_Test";
          }

          _time_control = Stopwatch.StartNew();

          using (DB_TRX _db_trx = new DB_TRX())
          {
            using (SqlCommand _cmd = new SqlCommand(_stored, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
            {
              _cmd.CommandType = CommandType.StoredProcedure;
              _cmd.CommandTimeout = 2 * 60; // 2 minutes

              // Params
              _cmd.Parameters.Add("@Device", SqlDbType.Int).Value = this.DeviceId;
              _cmd.Parameters.Add("@Source", SqlDbType.VarChar).Value = 'S';

              if (WebConfigurationManager.AppSettings["TestMode"].ToString() == "true")
              {
                _cmd.Parameters.Add("@Date", SqlDbType.DateTime).Value = DateTime.Parse(WebConfigurationManager.AppSettings["TestModeDate"].ToString());
              }
              else
              {
                _cmd.Parameters.Add("@Date", SqlDbType.DateTime).Value = WSI.Common.Misc.TodayOpening();
              }

              using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
              {
                using (DataSet _dataset = new DataSet())
                {
                  _sql_da.Fill(_dataset);
                  lock (m_lock)
                  {
                    m_data = "";
                    // Convert data to JSON
                    m_data = "{\"Meters\":" + clsDefaultConverter.DataTableToReport(_dataset.Tables[0]) + "," +
                            "\"Data\":" + clsDefaultConverter.DataTableToReport(_dataset.Tables[1]) + "," +
                             "\"Weather\":" + clsDefaultConverter.DataTableToReport(_dataset.Tables[2]) + "}";
                  }

                  if (WebConfigurationManager.AppSettings["TestMode"].ToString() == "true")
                    _select = "x2d_meter_id in (4)  AND RANGE = 'TDA' AND X2D_DATE = '" + DateTime.Parse(WebConfigurationManager.AppSettings["TestModeDate"].ToString()).ToString("yyyyMMdd") + "'";
                  else
                    _select = "x2d_meter_id in (4)  AND RANGE = 'TDA' AND X2D_DATE = '" + Misc.TodayOpening().ToString("yyyyMMdd") + "'";

                  DataRow[] _site_meters_datarow = _dataset.Tables[1].Select(_select, "X2D_DATE ASC");
                
                  DataTable _site_meters_table = new DataTable();
                  _site_meters_table = new DataTable();
                  _site_meters_table.Columns.Add("x2d_meter_id", typeof(Int32));
                  _site_meters_table.Columns.Add("AVG", typeof(Decimal));

                  foreach (DataRow _row in _site_meters_datarow)
                  {
                    DataRow newMetersRow = _site_meters_table.NewRow();

                    newMetersRow["x2d_meter_id"] = (Int32)_row["X2D_METER_ID"];
                    newMetersRow["AVG"] = (DBNull.Value.Equals(_row["AVG"])) ? 0 : (Decimal)_row["AVG"];

                    _site_meters_table.Rows.Add(newMetersRow);
                  }

                  lock (m_lock)
                  {
                    m_site_meters_now = clsDefaultConverter.DataTableToReport(_site_meters_table);
                  }
                }
              }
            }
          }

          _time_control.Stop();
          _elapsed = _time_control.ElapsedMilliseconds;
          Logger.Write(HttpContext.Current, "[SP_Get_Meters] Elapsed time: " + _elapsed.ToString() + "ms", "TC");

        }
        catch (Exception _ex)
        {
          Logger.Write(HttpContext.Current, "Thread exception:", "TS");
          Logger.Exception(HttpContext.Current, "[" + this.m_thread.Name + "]Task()", _ex);
        }

        Thread.Sleep(this.m_interval);

      }
    }

    /// <summary>
    /// Default Constructor
    /// </summary>
    public clsLayoutMetersData()
      : this(5000)
    {
    }

    /// <summary>
    /// Constructor for the class
    /// </summary>
    /// <param name="Interval">Thread delay</param>
    public clsLayoutMetersData(Int32 Interval)
    {
      this.m_interval = Interval;
    }

  }
}
