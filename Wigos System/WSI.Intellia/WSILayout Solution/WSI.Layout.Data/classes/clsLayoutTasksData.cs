﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Threading;
using System.Data;
using System.Data.SqlClient;
using WSI.Common;
using WSI.Layout;
using System.Web;

using System.Diagnostics;
using Newtonsoft.Json.Linq;
using WSI.Layout.Data.entities;

namespace WSI.Layout.Data.classes
{
  public class clsLayoutTasksData
  {
    /// <summary>
    /// Multithread lock
    /// </summary>
    private static readonly object m_lock = new object();

    /// <summary>
    /// Thread Interval
    /// </summary>
    private Int32 m_interval = 5000;
    private DateTime m_min_value = new DateTime(1970, 01, 01, 00, 00, 00);

    /// <summary>
    /// Thread delegate
    /// </summary>
    private delegate void ThreadWorker();
    private Thread m_thread;

    private String m_data = String.Empty;
    /// <summary>
    /// Shared data by the thread
    /// </summary>
    public String Data
    {
      get
      {
        lock (m_lock) { return this.m_data; }
      }
    }

    private void DeleteAlarmFromData(long alarmId)
    {
      JObject jsonObject = clsJsonUtils.ConvertToJson(m_data);
      JToken alarmToken = jsonObject["Alarms"]["rows"].SingleOrDefault(x => x.Value<long>(4) == alarmId);
      if (alarmToken != null)
        alarmToken.Remove();

      m_data = clsJsonUtils.ConvertToString(jsonObject);
    }

    private void UpdateTaskStatusFromData(long taskId, int status)
    {
      JObject jsonObject = clsJsonUtils.ConvertToJson(m_data);
      JToken taskToken = jsonObject["Tasks"]["rows"].SingleOrDefault(x => x.Value<long>(0) == taskId);
      if (taskToken != null)
        taskToken[1] = status;

      m_data = clsJsonUtils.ConvertToString(jsonObject);
    }

    /// <summary>
    /// Initializes the thread with the worker
    /// </summary>
    /// <param name="work">The method to execute by the thread</param>
    private void Initialize(ThreadWorker work)
    {
      m_thread = new Thread(new ThreadStart(work));
      m_thread.Name = "LayoutData_Tasks";
      m_thread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
      m_thread.Start();
    }

    /// <summary>
    /// General initialization
    /// </summary>
    public void Init()
    {
      this.Initialize(this.Task);
    }

    /// <summary>
    /// Main task for the thread
    /// </summary>
    private void Task()
    {
      Stopwatch _time_control;
      Int64 _elapsed;

      Logger.Write(HttpContext.Current, "Thread starting:", "TS");
      Logger.Write(HttpContext.Current, " - Wait time : " + this.m_interval + "ms", "TS");

      Thread.Sleep(this.m_interval / 5);

      while (true)
      {
        try
        {
#if !DEBUG
          if (!Services.IsPrincipal("WS_INTELLIA"))
          {
            Thread.Sleep(1000);
            continue;
          }
#endif


          _time_control = Stopwatch.StartNew();

          using (DB_TRX _db_trx = new DB_TRX())
          {

            using (SqlCommand _cmd = new SqlCommand("Layout_GetAlarmsAndTasks", _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
            {
              _cmd.CommandType = CommandType.StoredProcedure;
              _cmd.CommandTimeout = 2 * 60; // 2 minutes
              using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
              {
                using (DataSet _dataset = new DataSet())
                {
                  lock (m_lock)
                  {
                    _sql_da.Fill(_dataset);

                    m_data = "";

                    String _date;

                    _date = WSI.Common.WGDB.Now.ToString("yyyyMMddHHmmss");

                    foreach (DataRow _row in _dataset.Tables[0].Rows)
                    {
                      _row["AL_DESCRIPTION"] = clsJsonUtils.StringEncodeForJSON(_row["AL_DESCRIPTION"].ToString());
                    }

                    foreach (DataRow _row in _dataset.Tables[1].Rows)
                    {
                        _row["LST_DESCRIPTION"] = clsJsonUtils.StringEncodeForJSON(_row["LST_DESCRIPTION"].ToString());
                        _row["LST_SCALE_REASON"] = clsJsonUtils.StringEncodeForJSON(_row["LST_SCALE_REASON"].ToString());
                        _row["LST_EVENTS_HISTORY"] = clsJsonUtils.StringEncodeForJSON(_row["LST_EVENTS_HISTORY"].ToString());
                    }

                    // Conver data to JSON
                    //m_data = "{\"Alarms\":" + clsDefaultConverter.DataTableToReport(_dataset.Tables[0]) + "," +
                    //          "\"Tasks\":" + clsDefaultConverter.DataTableToReport(_dataset.Tables[1]) + ","+
                    //          "\"Users\":" + clsDefaultConverter.DataTableToReport(_dataset.Tables[2]) + "}";
                      m_data = "{\"Alarms\":" + clsDefaultConverter.DataTableToReport(_dataset.Tables[0]) + "," +
                              "\"Tasks\":" + clsDefaultConverter.DataTableToReport(_dataset.Tables[1]) + "," +
                              "\"Users\":" + clsDefaultConverter.DataTableToReport(_dataset.Tables[2]) + "," +
                              "\"LastTasks\":" + clsDefaultConverter.DataTableToReport(_dataset.Tables[3]) + "," +
                      //"\"LastUpdate\":\"" + (WGDB.Now - m_min_value).TotalMilliseconds + " \"}";
                              "\"LastUpdate\":\"" + _date.Replace(',', '.') + "\"}";
                  }
                }
              }
            }
          }

          _time_control.Stop();
          _elapsed = _time_control.ElapsedMilliseconds;
          Logger.Write(HttpContext.Current, "Layout_GetAlarmsAndTasks Elapsed time: " + _elapsed.ToString() + "ms", "TC");

        }
        catch (Exception _ex)
        {
          Logger.Write(HttpContext.Current, "Thread exception:", "TS");
          Logger.Exception(HttpContext.Current, "[" + this.m_thread.Name + "]Task()", _ex);
        }

        Thread.Sleep(this.m_interval);

      }
    }

    /// <summary>
    /// Default Constructor
    /// </summary>
    public clsLayoutTasksData()
      : this(5000)
    {
    }

    /// <summary>
    /// Constructor for the class
    /// </summary>
    /// <param name="Interval">Thread delay</param>
    public clsLayoutTasksData(Int32 Interval)
    {
      this.m_interval = Interval;
    }

    /// <summary>
    /// Save Alarm
    /// </summary>
    /// <param name="Data"></param>
    /// <returns></returns>
    public Boolean SaveSiteAlarm(AlarmData Data, SqlTransaction Trx)
    {
      try
      {
        lock (m_lock)
        {
          using (SqlCommand _cmd = new SqlCommand("Layout_SaveSiteAlarm", Trx.Connection, Trx))
          {
            _cmd.CommandType = CommandType.StoredProcedure;

            // Params Adding
            _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = Data.TerminalId;
            _cmd.Parameters.Add("@pAlarmId", SqlDbType.BigInt).Value = Data.AlarmId;
            _cmd.Parameters.Add("@pDateCreated", SqlDbType.DateTime).Value = Data.DateCreated;
            _cmd.Parameters.Add("@pAlarmType", SqlDbType.Int).Value = Data.AlarmType;
            _cmd.Parameters.Add("@pAlarmSource", SqlDbType.Int).Value = Data.AlarmSource;
            _cmd.Parameters.Add("@pUserCreated", SqlDbType.Int).Value = Data.UserCreated;

            if (Data.Media < 0)
            {
              _cmd.Parameters.Add("@pMediaId", SqlDbType.BigInt).Value = DBNull.Value;
            }
            else
            {
              _cmd.Parameters.Add("@pMediaId", SqlDbType.BigInt).Value = Data.Media;
            }

            if (Data.Description == "")
            {
              _cmd.Parameters.Add("@pDescription", SqlDbType.Text).Value = DBNull.Value;
            }
            else
            {
              _cmd.Parameters.Add("@pDescription", SqlDbType.Text).Value = Data.Description;
            }


            if (Data.DateToTask == DateTime.MinValue)
            {
              _cmd.Parameters.Add("@pDateToTask", SqlDbType.DateTime).Value = DBNull.Value;
            }
            else
            {
              _cmd.Parameters.Add("@pDateToTask", SqlDbType.DateTime).Value = Data.DateToTask;
            }

            if (Data.TaskId < 0)
            {
              _cmd.Parameters.Add("@pTaskId", SqlDbType.BigInt).Value = DBNull.Value;
            }
            else
            {
              _cmd.Parameters.Add("@pTaskId", SqlDbType.BigInt).Value = Data.TaskId;
            }

            if (Data.Title == "")
            {
              _cmd.Parameters.Add("@pTitle", SqlDbType.NVarChar).Value = DBNull.Value;
            }
            else
            {
              _cmd.Parameters.Add("@pTitle", SqlDbType.NVarChar).Value = Data.Title;
            }
            _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = Data.Status;

            _cmd.CommandTimeout = 2 * 60; // 2 minutes

            if (!(_cmd.ExecuteNonQuery() > 0))
            {
              return false;
            }
          }

          //status = 1 --> Task deleted
          //Data.TaskId != 0 --> Task assigned
          if (Data.Status == 1 || Data.TaskId != 0)
            this.DeleteAlarmFromData(Data.ID);

          return true;
        }
      }
      catch (Exception _ex)
      {

        Logger.Exception(HttpContext.Current, "SaveSiteAlarm()", _ex);
      }

      return false;
    }

    /// <summary>
    /// Save Task
    /// </summary>
    /// <param name="Data"></param>
    /// <returns></returns>
    public Int64 SaveSiteTask(TaskData Data, SqlTransaction Trx)
    {
      Int64 _task_id = -1;
      try
      {
        lock (m_lock)
        {
          using (SqlCommand _cmd = new SqlCommand("Layout_SaveSiteTask", Trx.Connection, Trx))
          {
            SqlParameter _sql_task_id;

            _cmd.CommandType = CommandType.StoredProcedure;

            // Params Adding
            _cmd.Parameters.Add("@pTaskId", SqlDbType.BigInt).Value = Data.ID;
            _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = Data.Status;

            if (Data.Start != DateTime.MinValue)
            {
              _cmd.Parameters.Add("@pStart", SqlDbType.DateTime).Value = Data.Start;
            }
            else
            {
              _cmd.Parameters.Add("@pStart", SqlDbType.DateTime).Value = DBNull.Value;
            }

            if (Data.End != DateTime.MinValue)
            {
              _cmd.Parameters.Add("@pEnd", SqlDbType.DateTime).Value = Data.End;
            }
            else
            {
              _cmd.Parameters.Add("@pEnd", SqlDbType.DateTime).Value = DBNull.Value;
            }

            if (Data.Category < 0)
            {
              _cmd.Parameters.Add("@pCategory", SqlDbType.Int).Value = DBNull.Value;
            }
            else
            {
              _cmd.Parameters.Add("@pCategory", SqlDbType.Int).Value = Data.Category;
            }

            if (Data.Subcategory < 0)
            {
              _cmd.Parameters.Add("@pSubcategory", SqlDbType.Int).Value = DBNull.Value;
            }
            else
            {
              _cmd.Parameters.Add("@pSubcategory", SqlDbType.Int).Value = Data.Subcategory;
            }

            if (Data.TerminalId < 0)
            {
              _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = DBNull.Value;
            }
            else
            {
              _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = Data.TerminalId;
            }

            if (Data.AccountId < 0)
            {
              _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = DBNull.Value;
            }
            else
            {
              _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = Data.AccountId;
            }

            if (Data.Description == "")
            {
              _cmd.Parameters.Add("@pDescription", SqlDbType.Text).Value = DBNull.Value;
            }
            else
            {
              _cmd.Parameters.Add("@pDescription", SqlDbType.Text).Value = Data.Description;
            }

            if (Data.Severity < 0)
            {
              _cmd.Parameters.Add("@pSeverity", SqlDbType.Int).Value = DBNull.Value;
            }
            else
            {
              _cmd.Parameters.Add("@pSeverity", SqlDbType.Int).Value = Data.Severity;
            }


            _cmd.Parameters.Add("@pCreationUserId", SqlDbType.Int).Value = Data.CreationUserId;
            _cmd.Parameters.Add("@pCreation", SqlDbType.DateTime).Value = Data.Creation;

            if (Data.AssignedRoleId < 0)
            {
              _cmd.Parameters.Add("@pAssignedRoleId", SqlDbType.Int).Value = DBNull.Value;
            }
            else
            {
              _cmd.Parameters.Add("@pAssignedRoleId", SqlDbType.Int).Value = Data.AssignedRoleId;
            }

            if (Data.AssignedUserId < 0)
            {
              _cmd.Parameters.Add("@pAssignedUserId", SqlDbType.Int).Value = DBNull.Value;
            }
            else
            {
              _cmd.Parameters.Add("@pAssignedUserId", SqlDbType.Int).Value = Data.AssignedUserId;
            }

            if (Data.Assigned == DateTime.MinValue)
            {
              _cmd.Parameters.Add("@pAssigned", SqlDbType.DateTime).Value = DBNull.Value;
            }
            else
            {
              _cmd.Parameters.Add("@pAssigned", SqlDbType.DateTime).Value = Data.Assigned;
            }

            if (Data.AcceptedUserId < 0)
            {
              _cmd.Parameters.Add("@pAcceptedUserId", SqlDbType.Int).Value = DBNull.Value;
            }
            else
            {
              _cmd.Parameters.Add("@pAcceptedUserId", SqlDbType.Int).Value = Data.AcceptedUserId;
            }

            if (Data.Accepted == DateTime.MinValue)
            {
              _cmd.Parameters.Add("@pAccepted", SqlDbType.DateTime).Value = DBNull.Value;
            }
            else
            {
              _cmd.Parameters.Add("@pAccepted", SqlDbType.DateTime).Value = Data.Accepted;
            }

            if (Data.ScaleFromUserId < 0)
            {
              _cmd.Parameters.Add("@pScaleFromUserId", SqlDbType.Int).Value = DBNull.Value;
            }
            else
            {
              _cmd.Parameters.Add("@pScaleFromUserId", SqlDbType.Int).Value = Data.ScaleFromUserId;
            }

            if (Data.ScaleToUserId < 0)
            {
              _cmd.Parameters.Add("@pScaleToUserId", SqlDbType.Int).Value = DBNull.Value;
            }
            else
            {
              _cmd.Parameters.Add("@pScaleToUserId", SqlDbType.Int).Value = Data.ScaleToUserId;
            }

            if (Data.ScaleReason == "")
            {
              _cmd.Parameters.Add("@pScaleReason", SqlDbType.Text).Value = DBNull.Value;
            }
            else
            {
              _cmd.Parameters.Add("@pScaleReason", SqlDbType.Text).Value = Data.ScaleReason;
            }

            if (Data.Scale == DateTime.MinValue)
            {
              _cmd.Parameters.Add("@pScale", SqlDbType.DateTime).Value = DBNull.Value;
            }
            else
            {
              _cmd.Parameters.Add("@pScale", SqlDbType.DateTime).Value = Data.Scale;
            }

            if (Data.SolvedUserId < 0)
            {
              _cmd.Parameters.Add("@pSolvedUserId", SqlDbType.Int).Value = DBNull.Value;
            }
            else
            {
              _cmd.Parameters.Add("@pSolvedUserId", SqlDbType.Int).Value = Data.SolvedUserId;
            }

            if (Data.Solved == DateTime.MinValue)
            {
              _cmd.Parameters.Add("@pSolved", SqlDbType.DateTime).Value = DBNull.Value;
            }
            else
            {
              _cmd.Parameters.Add("@pSolved", SqlDbType.DateTime).Value = Data.Solved;
            }

            if (Data.ValidateUserId < 0)
            {
              _cmd.Parameters.Add("@pValidateUserId", SqlDbType.Int).Value = DBNull.Value;
            }
            else
            {
              _cmd.Parameters.Add("@pValidateUserId", SqlDbType.Int).Value = Data.ValidateUserId;
            }

            if (Data.Validate == DateTime.MinValue)
            {
              _cmd.Parameters.Add("@pValidate", SqlDbType.DateTime).Value = DBNull.Value;
            }
            else
            {
              _cmd.Parameters.Add("@pValidate", SqlDbType.DateTime).Value = Data.Validate;
            }

            if (Data.LastStatusUpdateUserId < 0)
            {
              _cmd.Parameters.Add("@pLastStatusUpdateUserId", SqlDbType.Int).Value = DBNull.Value;
            }
            else
            {
              _cmd.Parameters.Add("@pLastStatusUpdateUserId", SqlDbType.Int).Value = Data.LastStatusUpdateUserId;
            }

            if (Data.LastStatusUpdate == DateTime.MinValue)
            {
              _cmd.Parameters.Add("@pLastStatusUpdate", SqlDbType.DateTime).Value = DBNull.Value;
            }
            else
            {
              _cmd.Parameters.Add("@pLastStatusUpdate", SqlDbType.DateTime).Value = Data.LastStatusUpdate;
            }

            if (Data.AttachedMedia < 0)
            {
              _cmd.Parameters.Add("@pAttachedMedia", SqlDbType.BigInt).Value = DBNull.Value;
            }
            else
            {
              _cmd.Parameters.Add("@pAttachedMedia", SqlDbType.BigInt).Value = Data.AttachedMedia;
            }

            if (Data.History == "")
            {
              _cmd.Parameters.Add("@pHistory", SqlDbType.Text).Value = DBNull.Value;
            }
            else
            {
              _cmd.Parameters.Add("@pHistory", SqlDbType.Text).Value = Data.History;
            }

            if (Data.Title == "")
            {
              _cmd.Parameters.Add("@pTitle", SqlDbType.Text).Value = DBNull.Value;
            }
            else
            {
              _cmd.Parameters.Add("@pTitle", SqlDbType.Text).Value = Data.Title;
            }

            _sql_task_id = _cmd.Parameters.Add("@pOutputFilterId", SqlDbType.Int);
            _sql_task_id.Direction = ParameterDirection.Output;

            _cmd.CommandTimeout = 2 * 60; // 2 minutes

            if (!(_cmd.ExecuteNonQuery() > 0))
            {

              return -1;
            }

            if (Data.ID < 0)
            {

              _task_id = (Int32)_sql_task_id.Value;
            }
            else
            {
              _task_id = Data.ID;
            }
          }

          this.UpdateTaskStatusFromData(_task_id, Data.Status);

          return _task_id;
        }
      }

      catch (Exception _ex)
      {

        Logger.Exception(HttpContext.Current, "SaveSiteTask()", _ex);
      }

      return -1;
    }
  }
}
