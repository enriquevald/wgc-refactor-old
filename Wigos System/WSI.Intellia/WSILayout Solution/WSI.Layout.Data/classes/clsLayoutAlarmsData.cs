﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
using System.Threading;
using System.Data;
using System.Data.SqlClient;
using WSI.Common;
using WSI.Layout;
using System.Web;
using System.Xml.Linq;


using System.Diagnostics;

namespace WSI.Layout.Data.classes
{

  public class clsLayoutAlarmsData
  {
    /// <summary>
    /// Multithread lock
    /// </summary>
    private static readonly object m_lock = new object();

    /// <summary>
    /// Thread Interval
    /// </summary>
    private Int32 m_interval = 5000;

    private DataTable m_old_data;

    /// <summary>
    /// Thread delegate
    /// </summary>
    private delegate void ThreadWorker();
    private Thread m_thread;
    private Dictionary<int, int> m_alarms_dic;
    Dictionary<string, object> _dic_differences = new Dictionary<string, object>();

    private String m_data = String.Empty;
    /// <summary>
    /// Shared data by the thread
    /// </summary>
    public String Data
    {
      get
      {
        lock (m_lock)
        {
          String _data;
          _data = this.m_data;

          Debug.WriteLine("clsLayoutAlarmsData - AccessGetData()");

          this.m_data = "";

          return _data;


        }
      }
    }

    /// <summary>
    /// Initializes the thread with the worker
    /// </summary>
    /// <param name="work">The method to execute by the thread</param>
    private void Initialize(ThreadWorker work)
    {
      m_thread = new Thread(new ThreadStart(work));
      m_thread.Name = "LayoutData_Alarms";
      m_thread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
      m_thread.Start();
    }

    /// <summary>
    /// General initialization
    /// </summary>
    public void Init()
    {
      this.Initialize(this.Task);

      m_alarms_dic = new Dictionary<int, int>();
      //egm
      m_alarms_dic.Add(1, 20);
      //door
      m_alarms_dic.Add(2, 12);
      //bill
      m_alarms_dic.Add(3, 13);
      //printe
      m_alarms_dic.Add(4, 14);
      //played_won
      m_alarms_dic.Add(5, 16);
      //jackpot
      m_alarms_dic.Add(6, 17);
      //call_att
      m_alarms_dic.Add(7, 19);
      //stacker
      m_alarms_dic.Add(8, 18);
      //Machine
      m_alarms_dic.Add(9, 20);
    }

    /// <summary>
    /// Main task for the thread
    /// </summary>
    private void Task()
    {
      Stopwatch _time_control;
      Int64 _elapsed;
      DataSet _dataset;
      DataTable _dt_new_values;

      Logger.Write(HttpContext.Current, "Thread starting:", "TS");
      Logger.Write(HttpContext.Current, " - Wait time : " + this.m_interval + "ms", "TS");

      Thread.Sleep(this.m_interval / 5);

      while (true)
      {
        try
        {
#if !DEBUG
          if (!Services.IsPrincipal("WS_INTELLIA"))
          {
            Thread.Sleep(1000);
            continue;
          }
#endif


          _time_control = Stopwatch.StartNew();
          _dataset = new DataSet();
          _dt_new_values = new DataTable();

          using (DB_TRX _db_trx = new DB_TRX())
          {

            using (SqlCommand _cmd = new SqlCommand("Layout_GetTerminalStatusData", _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
            {
              _cmd.CommandType = CommandType.StoredProcedure;
              _cmd.CommandTimeout = 2 * 60; // 2 minutes

              using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
              {
                _sql_da.Fill(_dataset);
                _dt_new_values = _dataset.Tables[0];
              }

              if (m_old_data != null)
              {
                lock (m_lock)
                {
                  if (m_data == "")
                  {
                    _dic_differences = new Dictionary<string, object>();
                  }

                  if (!CompareRows(m_old_data.Copy(), _dt_new_values.Copy(), ref _dic_differences))
                  {
                    Debug.WriteLine("CompareRows failed");
                    throw new Exception("CompareRows failed");
                  }

                  m_old_data = _dt_new_values;

                  if (_dic_differences.Count > 0)
                  {
                    Debug.WriteLine("clsLayoutAlarmsData - Has ##" + _dic_differences.Count + " differences");
                    //m_data = MyDictionaryToJson(_dic_differences);
                    JavaScriptSerializer _jss = new JavaScriptSerializer();
                    m_data = _jss.Serialize(_dic_differences);
                  }
                }
              }
              else
              {
                m_old_data = _dt_new_values;
              }
            }
          }

          _time_control.Stop();
          _elapsed = _time_control.ElapsedMilliseconds;
          Logger.Write(HttpContext.Current, "Layout_GetTerminalStatusData Elapsed time: " + _elapsed.ToString() + "ms", "TC");
          Debug.WriteLine("Layout_GetTerminalStatusData FINISHED");

        }
        catch (Exception _ex)
        {
          Logger.Write(HttpContext.Current, "Thread exception:", "TS");
          Logger.Exception(HttpContext.Current, "[" + this.m_thread.Name + "]Task()", _ex);
          Debug.WriteLine("[" + this.m_thread.Name + "]Task()");
        }

        Thread.Sleep(this.m_interval);

      }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="dict"></param>
    /// <returns></returns>
    private string MyDictionaryToJson(Dictionary<int, int[]> dict)
    {
      var entries = dict.Select(d =>
          string.Format("\"{0}\": [{1}]", d.Key, string.Join(",", d.Value)));
      return "{" + string.Join(",", entries) + "}";
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="oldDataDt"></param>
    /// <param name="newDataDt"></param>
    /// <param name="TerminalList"></param>
    /// <returns></returns>
    private bool CompareRows(DataTable oldDataDt, DataTable newDataDt, ref Dictionary<string, object> TerminalList)
    {
      try
      {
        DataColumn _dc = new DataColumn("CHECKED", typeof(Int32));
        _dc.DefaultValue = 0;
        DataColumn _dc2 = new DataColumn("CHECKED", typeof(Int32));
        _dc2.DefaultValue = 0;

        newDataDt.Columns.Add(_dc);
        oldDataDt.Columns.Add(_dc2);


        foreach (DataRow row1 in oldDataDt.Rows)
        {
          foreach (DataRow row2 in newDataDt.Rows)
          {
            var array1 = row1.ItemArray;
            var array2 = row2.ItemArray;

            if ((int)array1[0] == (int)array2[0])
            {
              row2["CHECKED"] = 1;
              if (!array1.SequenceEqual(array2))
              {
                Debug.WriteLine("Not equal");
                ShowDifferences(array1, array2, ref TerminalList);
              }
              else
              {
                break;
              }
            }
          }//foreach (DataRow row2 in newDataDt.Rows)
        }//foreach (DataRow row1 in oldDataDt.Rows)

        if (oldDataDt.Rows.Count != newDataDt.Rows.Count)
        {
          DataRow[] _dr_new_terminals = newDataDt.Select("CHECKED = 0");

          foreach (DataRow _dr in _dr_new_terminals)
          {
            object[] _dummy = new object[10];
            var array2 = _dr.ItemArray;

            _dummy[0] = _dr[0];
            _dummy[1] = 0;
            _dummy[2] = 0;
            _dummy[3] = 0;
            _dummy[4] = 0;
            _dummy[5] = 0;
            _dummy[6] = 0;
            _dummy[7] = 0;
            _dummy[8] = 0;
            _dummy[9] = 0;

            Debug.WriteLine("Not equal");
            ShowDifferences(_dummy, array2, ref TerminalList);

          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Console.WriteLine(_ex.Message);
      }

      return false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="oldDataList"></param>
    /// <param name="newDataList"></param>
    /// <param name="TerminalList"></param>
    private void ShowDifferences(object[] oldDataList, object[] newDataList, ref Dictionary<string, object> TerminalList)
    {
      int _value;
      string _terminal_name = string.Empty; //this var provides terminal id & floor id.

      for (int _idx = 0; _idx <= oldDataList.Length - 4; _idx++)
      {
        if (int.TryParse(newDataList[_idx].ToString(), out _value))
        {
          if (_value != (int)oldDataList[_idx])
          {
            Debug.Write("Terminal " + newDataList[0] + ": new " + _value + "> " + (int)oldDataList[_idx]);
            //int _new_bits = (int)newDataList[_idx] - (int)oldDataList[_idx];
            _terminal_name = newDataList[0].ToString() + "_" + newDataList[10].ToString() + "_" + newDataList[11].ToString() + "_" + newDataList[12].ToString();//Formated as: TerminalId_FloorId_X_Y

            if (TerminalList.ContainsKey(newDataList[0].ToString()))
            {

              TerminalList[_terminal_name] = ((int[])TerminalList[newDataList[0].ToString()]).Union(GetActivedBits((int)newDataList[_idx], (int)oldDataList[_idx], m_alarms_dic[_idx])).ToArray<int>();
            }
            else
            {
              TerminalList.Add(_terminal_name, GetActivedBits((int)newDataList[_idx], (int)oldDataList[_idx], m_alarms_dic[_idx]));
            }
          }
        }
      }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="newBits"></param>
    /// <param name="oldBits"></param>
    /// <param name="BitGroup"></param>
    /// <returns></returns>
    private int[] GetActivedBits(int newBits, int oldBits, int BitGroup)
    {
      List<int> _bits_activated = new List<int>();
      List<int> _result = new List<int>();
      int _idx = 1;

      while (_idx <= Int32.MaxValue)
      {
        if (_idx > newBits)
        {

          break;
        }

        if ((newBits & _idx) == _idx)
        {
          _bits_activated.Add(_idx);
        }

        _idx = _idx * 2;
      }

      foreach (int _idx_values in _bits_activated)
      {
        if ((oldBits & _idx_values) != _idx_values)
        {
          _result.Add((BitGroup * 1000) + _idx_values);
        }
      }

      return _result.ToArray();
    }


    /// <summary>
    /// Default Constructor
    /// </summary>
    public clsLayoutAlarmsData()
      : this(5000)
    {
    }

    /// <summary>
    /// Constructor for the class
    /// </summary>
    /// <param name="Interval">Thread delay</param>
    public clsLayoutAlarmsData(Int32 Interval)
    {
      this.m_interval = Interval;
    }
  }
}
