﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Threading;
using System.Data;
using System.Data.SqlClient;
using WSI.Common;
using WSI.Layout;
using System.Web;

using System.Diagnostics;

namespace WSI.Layout.Data.classes
{
  /// <summary>
  /// Class for the thread that obtains the data for the custom updatable charts in the dashboard
  /// </summary>
  public class clsLayoutDashboard
  {

    private static readonly object m_lock = new object();

    private Int32 m_interval = 1000 * 60;

    private delegate void ThreadWorker();
    private Thread m_thread;

    //private String m_data = String.Empty;
    //public String Data
    //{
    //  get
    //  {
    //    lock (m_lock) { return this.m_data; }  
    //  }
    //}

    private String m_segmentation_levels_clients = String.Empty;
    public String SegmentationLevelClients
    {
      get
      {
        lock (m_lock) { return this.m_segmentation_levels_clients; }  
      }
    }

    private String m_segmentation_levels_visits = String.Empty;
    public String SegmentationLevelVisits
    {
      get
      {
        lock (m_lock) { return this.m_segmentation_levels_visits; }
      }
    }

    private String m_current_promotions = String.Empty;
    public String CurrentPromotions
    {
      get
      {
        lock (m_lock) { return this.m_current_promotions; }  
      }
    }

    private void Initialize(ThreadWorker work)
    {
      m_thread = new Thread(new ThreadStart(work));
      m_thread.Name = "LayoutData_Dashboard";
      m_thread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
      m_thread.Start();
    }

    public void Init()
    {
      this.Initialize(this.Task);
    }

    private String ExecuteQuery(String Method, SqlParameter[] Parameters, SqlTransaction Transaction)
    {
      String _result = String.Empty;
      using (SqlCommand _cmd = new SqlCommand("Layout_Reports_Segmentation", Transaction.Connection, Transaction))
          {
            _cmd.CommandType = CommandType.StoredProcedure;
            _cmd.CommandTimeout = 2 * 60; // 2 minutes
            _cmd.Parameters.AddRange(Parameters);
            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
            {
              using (DataSet _dataset = new DataSet())
              {
                _sql_da.Fill(_dataset);
                lock (m_lock)
                {
                  _result = clsDefaultConverter.DataTableToReport(_dataset.Tables[0]);
                }
              }
            }
          }
      return _result;
    }

    private void Task()
    {
      Stopwatch _time_control;
      Int64 _elapsed;

      Logger.Write(HttpContext.Current, "Thread starting:", "TS");
      Logger.Write(HttpContext.Current, " - Wait time : " + this.m_interval + "ms", "TS");
      
      List<SqlParameter> _params = new List<SqlParameter>();

      while (true)
      {

        try
        {

          using (DB_TRX _db_trx = new DB_TRX())
          {

            _time_control = Stopwatch.StartNew();

            // Segmentation - Level - Clients
            _params.Clear();
            _params.Add(new SqlParameter("@pReport", 0)); 
            _params.Add(new SqlParameter("@pConcept", 0));
            m_segmentation_levels_clients = ExecuteQuery("Layout_Reports_Segmentation", _params.ToArray(), _db_trx.SqlTransaction);

            _time_control.Stop();
            _elapsed = _time_control.ElapsedMilliseconds;
            Logger.Write(HttpContext.Current, "[Segmentation - Level - Clients] Elapsed time: " + _elapsed.ToString() + "ms", "TC");

            _time_control = Stopwatch.StartNew();

            // Segmentation - Level - Visits
            _params.Clear();
            _params.Add(new SqlParameter("@pReport", 0));
            _params.Add(new SqlParameter("@pConcept", 1));  
            m_segmentation_levels_visits = ExecuteQuery("Layout_Reports_Segmentation", _params.ToArray(), _db_trx.SqlTransaction);

            _time_control.Stop();
            _elapsed = _time_control.ElapsedMilliseconds;
            Logger.Write(HttpContext.Current, "[Segmentation - Level - Visits] Elapsed time: " + _elapsed.ToString() + "ms", "TC");

            // TODO: Current promotions
            /*
             
             * --USE wgdb_916;
USE wgdb_000;

DECLARE @Fecha AS DATE 
SET @Fecha = '2015/03/18'; -- DATEADD(DAY, -1, GETDATE())

SELECT   acp_promo_id
       , acp_promo_type
       , acp_promo_name
       , acp_credit_type
       , SUM(acp_ini_balance) acp_amount
       --, acp_account_id
       --, acp_play_session_id
       --, acp_created
  FROM   account_promotions
 WHERE   acp_created >  DATEADD(DAY, -1, @Fecha)
 GROUP   BY acp_promo_id, acp_promo_type, acp_promo_name, acp_credit_type
              
             */


          }

        }
        catch (Exception _ex)
        {
          Logger.Write(HttpContext.Current, "Thread exception:", "TS");
          Logger.Exception(HttpContext.Current, "[" + this.m_thread.Name +  "]Task()", _ex);
        }

        Thread.Sleep(this.m_interval);
      }
    }

    public clsLayoutDashboard()
      : this(1000 * 60)
    {
    }

    public clsLayoutDashboard(Int32 Interval)
    {
      this.m_interval = Interval;
      //this.Init();
    }


  }
}
