﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

//using System.Threading;
//using System.Data;
//using System.Data.SqlClient;
//using WSI.Common;
//using WSI.Layout;
//using System.Web;

//namespace WSI.Layout.Data.classes
//{
//  public class clsLayoutData_old : IDisposable
//  {
//    private static readonly object m_lock = new object();

//    private Int32 m_interval = 5000;
//    private DataSet m_dataset = new DataSet();

//    public DataSet Data
//    {
//      get { lock (m_lock) { return m_dataset; } }
//      set { lock (m_lock) { m_dataset = value; } }
//    }

//    private delegate void ThreadWorker();
//    private Thread m_thread;

//    private void Initialize(ThreadWorker work)
//    {
//      m_thread = new Thread(new ThreadStart(work));
//      m_thread.Name = "LayoutData_Main";
//      m_thread.Start();
//    }

//    public void Init()
//    {
//      this.Initialize(this.Task);
//    }

//    private void Task()
//    {
//      Boolean _accessible = false;
      
//      while (true) {

//        try
//        {
//          _accessible = Monitor.TryEnter(m_lock);
//        } // try
//        finally
//        {
//          if (_accessible)
//          {
//            try
//            {
//              using (DB_TRX _db_trx = new DB_TRX())
//              {
//                //using (SqlCommand _cmd = new SqlCommand("Layout_GetActivity", _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
//                using (SqlCommand _cmd = new SqlCommand("Layout_GetActivity_Test", _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
//                {
//                  _cmd.CommandType = CommandType.StoredProcedure;
//                  _cmd.CommandTimeout = 2 * 60; // 2 minutes

//                  using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
//                  {
//                    lock (m_lock)
//                    {
//                      _sql_da.Fill(m_dataset);
//                      //
//                      HttpContext.Current.Application.Add("ActivityData", m_dataset.Copy());
//                      //
//                    }
//                  }
//                }
//              }
//            }
//            catch (Exception _ex)
//            {
//              Log.Exception(_ex);
//            }
//          }

//        } // finally

//        Thread.Sleep((_accessible) ? 5000 : 2000);

//      } // while (true)
//    }

//    private DataSet InternalGetData()
//    {
//      // TODO: Check if dataset must be cloned
//      while (!Monitor.TryEnter(m_lock))
//      {
//         // 
//      }
//      lock (m_lock)
//      {
//        return m_dataset.Copy();
//      }
//    }

//    public DataSet GetData()
//    {
//        return InternalGetData();
//    }

//    public void Dispose()
//    {
//      try
//      {
//        m_dataset.Dispose();
//        m_dataset = null;

//        this.m_thread.Join(1000);
//        this.m_thread.Abort();
//        this.m_thread = null;
//      }
//      catch (Exception _ex)
//      {
//        Logger.Exception(null, "clsLayoutData.Dispose()", _ex);
//      }
//      //throw new NotImplementedException();
//    }

//    public clsLayoutData_old()
//      : this(2000)
//    {
//    }

//    public clsLayoutData_old(Int32 Interval)
//    {
//      this.m_interval = Interval;
//      //this.Init();
//    }

//  }
//}
