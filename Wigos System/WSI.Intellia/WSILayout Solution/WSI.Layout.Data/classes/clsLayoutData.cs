﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Threading;
using System.Data;
using System.Data.SqlClient;
using WSI.Common;
using WSI.Layout;
using System.Web;

using System.Diagnostics;
using System.Web.Configuration;

namespace WSI.Layout.Data.classes
{
  /// <summary>
  /// The class that constains the thread to obtain the activity from the database.
  /// </summary>
  public class clsLayoutData
  {
    /// <summary>
    /// Multithread lock
    /// </summary>
    private static readonly object m_lock = new object();

    /// <summary>
    /// Thread Interval
    /// </summary>
    private Int32 m_interval = 5000;

    /// <summary>
    /// Thread delegate
    /// </summary>
    private delegate void ThreadWorker();
    private Thread m_thread;

    private String m_data = String.Empty;
    /// <summary>
    /// Shared data by the thread
    /// </summary>
    public String Data
    {
      get
      {
        lock (m_lock)
        {
          return this.m_data;
        }
      }
    }

    private String m_data_runners = String.Empty;
    /// <summary>
    /// Shared runners data by the thread
    /// </summary>
    public String DataRunners
    {
      get
      {
        lock (m_lock) 
        { 
          return this.m_data_runners; 
        }
      }
    }

    /// <summary>
    /// Initializes the thread with the worker
    /// </summary>
    /// <param name="work">The method to execute by the thread</param>
    private void Initialize(ThreadWorker work)
    {
      m_thread = new Thread(new ThreadStart(work));
      m_thread.Name = "LayoutData_Main";
      m_thread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
      m_thread.Start();
    }

    /// <summary>
    /// General initialization
    /// </summary>
    public void Init()
    {
      this.m_interval = WSI.Common.GeneralParam.GetInt32("Intellia", "Activity.Interval", 5000);

      this.Initialize(this.Task);
    }

    /// <summary>
    /// Main task for the thread
    /// </summary>
    private void Task()
    {
      String _stored = String.Empty;
      Stopwatch _time_control;
      Int64 _elapsed;

      Logger.Write(HttpContext.Current, "Thread starting:", "TS");
      Logger.Write(HttpContext.Current, " - Wait time : " + this.m_interval + "ms", "TS");

      Thread.Sleep(this.m_interval / 5);

      while (true)
      {
        try
        {
#if !DEBUG
          if (!Services.IsPrincipal("WS_INTELLIA"))
          {
            Thread.Sleep(1000);
            continue;
          }
#endif


          // Determine if test mode is enabled and stored procedure to use
          switch (WebConfigurationManager.AppSettings["TestMode"].ToString())
          {
            case "true":
              _stored = "Layout_GetActivity_Test";
              //_stored = "Layout_GetActivity_Test_DEMO";
              break;
            case "false":
              _stored = "Layout_GetActivity";
              break;
          }

          _time_control = Stopwatch.StartNew();

          using (DB_TRX _db_trx = new DB_TRX())
          {

            // Activity data ---------------------------------------------------------------------------
            using (SqlCommand _cmd = new SqlCommand(_stored, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
            {

              _cmd.CommandType = CommandType.StoredProcedure;
              _cmd.CommandTimeout = 2 * 60; // 2 minutes

              if (WebConfigurationManager.AppSettings["TestMode"].ToString() == "true")
              {
                _cmd.Parameters.Add("@Date", SqlDbType.DateTime).Value = DateTime.Parse(WebConfigurationManager.AppSettings["TestModeDate"].ToString());
                _cmd.Parameters.Add("@IntervalHour", SqlDbType.Int).Value = 1;
              }

              using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
              {
                using (DataSet _dataset = new DataSet())
                {
                  _sql_da.Fill(_dataset);
                  lock (m_lock)
                  {
                    m_data = "";

                    // Convert data to JSON
                    m_data = "{\"Terminals\":" + clsJsonConverters.ConvertDataTableToJson("Terminal", _dataset.Tables[0]) + "," +
                              "\"WGDBDate\":\"" + WSI.Common.WGDB.Now + "\"," +
                              "\"Occupancy\":\"" + Convert.ToString(_dataset.Tables[1].Rows[0][0]) + "\",";

                    if (!String.IsNullOrEmpty(clsLayoutMetersData.SiteMetersNow))
                    {
                      m_data += "\"Capacity\":" + clsLayoutMetersData.SiteMetersNow + ",";
                    }
                    else
                    {
                      m_data += "\"Capacity\":\"" + clsLayoutMetersData.SiteMetersNow + "\",";
                    }
                    m_data += "\"Ranges\":" + clsJsonConverters.ConvertDataTableToJson("Range", _dataset.Tables[2]) + "}";
                  }
                }
              }
            }

            // Runners data ---------------------------------------------------------------------------
            using (SqlCommand _cmd = new SqlCommand("Layout_GetRunnersPositions", _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
            {

              _cmd.CommandType = CommandType.StoredProcedure;
              _cmd.CommandTimeout = 2 * 60; // 2 minutes

              using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
              {
                using (DataSet _dataset = new DataSet())
                {
                  _sql_da.Fill(_dataset);
                  lock (m_lock)
                  {
                    m_data_runners = "";
                    // Convert data to JSON
                    m_data_runners = clsDefaultConverter.DataTableToReport(_dataset.Tables[0]);
                  }
                }
              }
            }
          }

          _time_control.Stop();
          _elapsed = _time_control.ElapsedMilliseconds;
          Logger.Write(HttpContext.Current, "[Layout_GetActivity] Elapsed time: " + _elapsed.ToString() + "ms", "TC");

        }
        catch (Exception _ex)
        {
          Logger.Write(HttpContext.Current, "Thread exception:", "TS");
          Logger.Exception(HttpContext.Current, "[" + this.m_thread.Name + "]Task()", _ex);
        }

        Thread.Sleep(this.m_interval);

      }
    }

    /// <summary>
    /// Default Constructor
    /// </summary>
    public clsLayoutData()
      : this(5000)
    {
    }

    /// <summary>
    /// Constructor for the class
    /// </summary>
    /// <param name="Interval">Thread delay</param>
    public clsLayoutData(Int32 Interval)
    {
      this.m_interval = Interval;
      //this.Init();
    }

  }
}
