USE [wgdb_000]
GO

/****** Object:  StoredProcedure [dbo].[Layout_Reports_Occupation]    Script Date: 03/11/2015 07:36:51 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/****** Object:  StoredProcedure [dbo].[Layout_Reports_Occupation]    Script Date: 11/10/2014 16:26:26 ******/


--CREATE PROCEDURE Layout_Reports_Occupation
CREATE PROCEDURE [dbo].[Layout_Reports_Occupation]
    @pReport      AS INT      = 0
  , @pConcept     AS INT      = 0
  , @pExtra       AS BIT      = 0  
  , @pDate        AS DATETIME = NULL
  , @pDateGroups  AS INT      = 0
 AS
BEGIN

--DECLARE @pReport    AS INT = 0
--DECLARE @pConcept   AS INT = 0
--DECLARE @pExtra     AS BIT = 0  
--DECLARE @pDate      AS DATETIME = NULL
--DECLARE @pDateGroups AS INT = 0

--SET @pDate = '2014-10-15 00:00:00.000'

IF @pDate IS NULL
 BEGIN
  SET @pDate = GETDATE()
 END

DECLARE @_NumDays AS INT
SET @_NumDays = CASE @pDateGroups
                  WHEN 0 THEN -7
                  WHEN 1 THEN -30
                  WHEN 2 THEN -365
                END

IF @pReport = 0
BEGIN
     -- Provider terminals by day range since date
     SELECT   
               TE_PROVIDER_ID            [Proveedor]
             , SUM(1)                    [Terminales]
             --, tc_date
        FROM   TERMINALS_CONNECTED WITH (INDEX (PK_terminals_connected))
             , TERMINALS WITH (INDEX (PK_terminals))
       WHERE   TC_DATE       >= DATEADD(DAY, @_NumDays, @pDate)
         AND   TC_DATE        < DATEADD(DAY, 1, @pDate)
         AND   TC_CONNECTED   = 1
         AND   TE_TERMINAL_ID = TC_TERMINAL_ID
         AND   TE_TERMINAL_TYPE IN (1, 3, 5)
    GROUP BY   TE_PROVIDER_ID, tc_date
END   

END

GO


