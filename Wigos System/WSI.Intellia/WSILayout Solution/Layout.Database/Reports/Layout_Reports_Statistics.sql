USE [wgdb_000]
GO

/****** Object:  StoredProcedure [dbo].[Layout_Reports_Statistics]    Script Date: 03/11/2015 07:37:37 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/****** Object:  StoredProcedure [dbo].[Layout_Reports_Statistics]    Script Date: 11/10/2014 16:26:27 ******/



/*
  @pReport  0 = TOP 10
            1 = WORST 10
            2 = ACCUMULATED
            3 = PROVIDERS
            4 = PERIOD        

  @pConcept 0 = Played
            1 = NetWin
            2 = Payable NetWin
            
  @pExtra   0 = Totals
            1 = Average
            
  @pDate
  
  @pDateGroups 0 = Dayly
               1 = Weekly
               2 = Mothly
*/
--CREATE PROCEDURE Layout_Reports_Statistics
CREATE PROCEDURE [dbo].[Layout_Reports_Statistics]
    @pReport      AS INT      = 0
  , @pConcept     AS INT      = 0
  , @pExtra       AS BIT      = 0  
  , @pDate        AS DATETIME = NULL
  , @pDateGroups  AS INT      = 0
 AS
BEGIN

--DECLARE @pReport    AS INT = 0
--DECLARE @pConcept   AS INT = 0
--DECLARE @pAverage   AS BIT = 0  
--DECLARE @pDate      AS DATETIME = NULL
--DECLARE @pDateGroup AS INT = 0

  DECLARE @pConcatDate DateTime

  IF @pDate IS NULL
  BEGIN
    SET @pDate = GETDATE()
  END

  SET @pConcatDate = dbo.ConcatOpeningTime(0, @pDate)

IF @pReport = 1
BEGIN
  ---------------------------------------------------------------------------------
  -- WORST 10

  IF @pConcept = 0
   BEGIN
    -- WORST 10 - By Played - 
    SELECT TOP 10 * FROM (
    SELECT *
    FROM
    ( SELECT *
          FROM
            (
                SELECT  PROVIDER                 AS BTP_PROVIDER
                      , MACHINE_NAME             AS BTP_MACHINE_NAME
                      , SUM(PLAYED)              AS BTP_PLAYED
                      , SUM(CASH_IN - CASH_OUT)  AS BTP_NETWIN
                      , RANK() OVER (PARTITION BY PROVIDER ORDER BY SUM(PLAYED)) AS RANKING
                  FROM
                  ((
                        SELECT  TE_PROVIDER_ID AS PROVIDER
                              , TE_NAME        AS MACHINE_NAME
                              , SUM(CASH_IN)   AS CASH_IN
                              , SUM(CASH_OUT)  AS CASH_OUT
                              , 0              AS PLAYED
                          FROM (   SELECT  TE_PROVIDER_ID
                                         , TE_NAME
                                         , SUM(PS_TOTAL_CASH_IN)  AS CASH_IN 
                                         , SUM(PS_TOTAL_CASH_OUT) AS CASH_OUT 
                                     FROM  PLAY_SESSIONS WITH (INDEX (IX_ps_finished_status)) 
                                         , TERMINALS
                                    WHERE  PS_FINISHED <  DATEADD(DAY, 1, @pConcatDate)
                                      AND  PS_FINISHED >= @pConcatDate
                                      AND  PS_STATUS <> 0 AND PS_PROMO = 0
                                      AND  PS_TERMINAL_ID = TE_TERMINAL_ID
                                      AND  TE_TERMINAL_TYPE IN (1, 3, 5)
                                 GROUP BY  TE_PROVIDER_ID, TE_NAME                             
                               ) 
                               AS SUM_PLAY_SESSIONS
                      GROUP BY TE_PROVIDER_ID, TE_NAME               
                   )
                   UNION
                   (  
                     SELECT  SPH_PROVIDER   AS PROVIDER
                          ,  SPH_TERMINAL   AS MACHINE_NAME
                          , 0               AS CASH_IN
                          , 0               AS CASH_OUT
                          , SUM(PLAYED)     AS PLAYED
                      FROM (   SELECT  ( SELECT  TE_PROVIDER_ID 
                                           FROM  TERMINALS 
                                          WHERE  TE_TERMINAL_ID = SPH_TERMINAL_ID ) AS SPH_PROVIDER 
                                     , ( SELECT  TE_NAME 
                                           FROM  TERMINALS 
                                          WHERE  TE_TERMINAL_ID = SPH_TERMINAL_ID ) AS SPH_TERMINAL 
                                     , SUM(SPH_PLAYED_AMOUNT) AS PLAYED 
                                 FROM  SALES_PER_HOUR WITH (INDEX (PK_sales_per_hour)) 
                                WHERE  SPH_BASE_HOUR <  DATEADD(DAY, 1, @pConcatDate)
                                  AND  SPH_BASE_HOUR >= @pConcatDate
                             GROUP BY  SPH_TERMINAL_ID
                           ) 
                           AS SUM_SALES_PER_HOUR
                      GROUP BY SPH_PROVIDER, SPH_TERMINAL
                  ))
                  AS PROVIDERS_SALES
             GROUP BY PROVIDER, MACHINE_NAME
            ) 
            AS PROVIDERS_STATS
    ) TOP_TERMINALS
    WHERE RANKING <= 10
    ) X ORDER BY BTP_PLAYED
    -- END : WORST 10 - By Played - 
   END
  ELSE IF @pConcept = 1
   BEGIN
    -- TOP 10 - By NetWin - 
    SELECT TOP 10 *
    FROM
    ( SELECT *
          FROM
            (
                SELECT  PROVIDER                 AS BTN_PROVIDER
                      , MACHINE_NAME             AS BTN_MACHINE_NAME
                      , SUM(CASH_IN - CASH_OUT)  AS V_NETWIN
                      , RANK() OVER (PARTITION BY PROVIDER ORDER BY SUM(CASH_IN - CASH_OUT)) AS RANKING
                  FROM
                  (
                    SELECT  TE_PROVIDER_ID AS PROVIDER
                          , TE_NAME        AS MACHINE_NAME
                          , SUM(CASH_IN)   AS CASH_IN
                          , SUM(CASH_OUT)  AS CASH_OUT
                      FROM (   SELECT  TE_PROVIDER_ID
                                     , TE_NAME
                                     , SUM(PS_TOTAL_CASH_IN)  AS CASH_IN 
                                     , SUM(PS_TOTAL_CASH_OUT) AS CASH_OUT 
                                 FROM  PLAY_SESSIONS WITH (INDEX (IX_ps_finished_status)) 
                                     , TERMINALS
                                WHERE  PS_FINISHED <  DATEADD(DAY, 1, @pConcatDate)
                                  AND  PS_FINISHED >= @pConcatDate
                                  AND  PS_STATUS <> 0 AND PS_PROMO = 0
                                  AND  PS_TERMINAL_ID = TE_TERMINAL_ID
                                  AND  TE_TERMINAL_TYPE IN (1, 3, 5)
                             GROUP BY  TE_PROVIDER_ID, TE_NAME                             
                           ) 
                           AS SUM_PLAY_SESSIONS
                    GROUP BY TE_PROVIDER_ID, TE_NAME               
                   )
                  AS PROVIDERS_SALES
             GROUP BY PROVIDER, MACHINE_NAME
            ) 
            AS PROVIDERS_STATS
    ) TOP_TERMINALS
    WHERE RANKING <= 10
    ORDER BY V_NETWIN    
    -- END : TOP 10 - By NetWin -
   END
  ELSE IF @pConcept = 2
   BEGIN
    -- TOP 10 - By Payable NetWin - 
    SELECT TOP 10 *
    FROM
    ( SELECT *
          FROM
            (
                SELECT  PROVIDER                 AS BTN_PROVIDER
                      , MACHINE_NAME             AS BTN_MACHINE_NAME
                      , SUM(CASH_IN_REDEEM - CASH_OUT_REDEEM)  AS V_NETWIN_REDEEM
                      , RANK() OVER (PARTITION BY PROVIDER ORDER BY SUM(CASH_IN_REDEEM - CASH_OUT_REDEEM)) AS RANKING
                  FROM
                  (
                        SELECT  TE_PROVIDER_ID AS PROVIDER
                              , TE_NAME        AS MACHINE_NAME
                              , SUM(CASH_IN_REDEEM)   AS CASH_IN_REDEEM
                              , SUM(CASH_OUT_REDEEM)  AS CASH_OUT_REDEEM
                          FROM (   SELECT  TE_PROVIDER_ID
                                         , TE_NAME
                                         , SUM(PS_REDEEMABLE_CASH_IN)  AS CASH_IN_REDEEM 
                                         , SUM(PS_REDEEMABLE_CASH_OUT) AS CASH_OUT_REDEEM 
                                     FROM  PLAY_SESSIONS WITH (INDEX (IX_ps_finished_status)) 
                                         , TERMINALS
                                    WHERE  PS_FINISHED <  DATEADD(DAY, 1, @pConcatDate)
                                      AND  PS_FINISHED >= @pConcatDate
                                      AND  PS_STATUS <> 0 AND PS_PROMO = 0
                                      AND  PS_TERMINAL_ID = TE_TERMINAL_ID
                                      AND  TE_TERMINAL_TYPE IN (1, 3, 5)
                                 GROUP BY  TE_PROVIDER_ID, TE_NAME                             
                               ) 
                               AS SUM_PLAY_SESSIONS
                      GROUP BY TE_PROVIDER_ID, TE_NAME               
                   )
                  AS PROVIDERS_SALES
             GROUP BY PROVIDER, MACHINE_NAME
            ) 
            AS PROVIDERS_STATS
    ) TOP_TERMINALS
    WHERE RANKING <= 10
    ORDER BY V_NETWIN_REDEEM    
    -- END : TOP 10 - By Payable NetWin -
   END

END
ELSE  
IF @pReport = 0
BEGIN

  ---------------------------------------------------------------------------------
  -- WORST 10

  IF @pConcept = 0
   BEGIN
    -- WORST 10 - By Played - 
    SELECT TOP 10 *
    FROM
    ( SELECT *
          FROM
            (
                SELECT  PROVIDER                 AS BTN_PROVIDER
                      , MACHINE_NAME             AS BTN_MACHINE_NAME
                      , SUM(PLAYED)              AS V_PLAYED
                      , RANK() OVER (PARTITION BY PROVIDER ORDER BY SUM(CASH_IN - CASH_OUT) DESC) AS RANKING
                  FROM
                  (
                     SELECT  SPH_PROVIDER   AS PROVIDER
                          ,  SPH_TERMINAL   AS MACHINE_NAME
                          , 0               AS CASH_IN
                          , 0               AS CASH_OUT
                          , SUM(PLAYED)     AS PLAYED
                      FROM (   SELECT  ( SELECT  TE_PROVIDER_ID 
                                           FROM  TERMINALS 
                                          WHERE  TE_TERMINAL_ID = SPH_TERMINAL_ID ) AS SPH_PROVIDER 
                                     , ( SELECT  TE_NAME 
                                           FROM  TERMINALS 
                                          WHERE  TE_TERMINAL_ID = SPH_TERMINAL_ID ) AS SPH_TERMINAL 
                                     , SUM(SPH_PLAYED_AMOUNT) AS PLAYED 
                                 FROM  SALES_PER_HOUR WITH (INDEX (PK_sales_per_hour)) 
                                WHERE  SPH_BASE_HOUR <  DATEADD(DAY, 1, @pConcatDate)
                                  AND  SPH_BASE_HOUR >= @pConcatDate
                             GROUP BY  SPH_TERMINAL_ID
                           ) 
                           AS SUM_SALES_PER_HOUR
                      GROUP BY SPH_PROVIDER, SPH_TERMINAL
                  )
                  AS PROVIDERS_SALES
             GROUP BY PROVIDER, MACHINE_NAME
            ) 
            AS PROVIDERS_STATS
    ) TOP_TERMINALS
    WHERE RANKING <= 10
    ORDER BY V_PLAYED DESC
    -- END : WORST 10 - By Played - 
   END
  ELSE IF @pConcept = 1
   BEGIN
    -- WORST 10 - By NetWin - 
    SELECT TOP 10 *
    FROM
    ( SELECT *
          FROM
            (
                SELECT  PROVIDER                 AS BTN_PROVIDER
                      , MACHINE_NAME             AS BTN_MACHINE_NAME
                      , SUM(CASH_IN - CASH_OUT)  AS V_NETWIN
                      , RANK() OVER (PARTITION BY PROVIDER ORDER BY SUM(CASH_IN - CASH_OUT) DESC) AS RANKING
                  FROM
                  (
                    SELECT  TE_PROVIDER_ID AS PROVIDER
                          , TE_NAME        AS MACHINE_NAME
                          , SUM(CASH_IN)   AS CASH_IN
                          , SUM(CASH_OUT)  AS CASH_OUT
                      FROM (   SELECT  TE_PROVIDER_ID
                                     , TE_NAME
                                     , SUM(PS_TOTAL_CASH_IN)  AS CASH_IN 
                                     , SUM(PS_TOTAL_CASH_OUT) AS CASH_OUT 
                                 FROM  PLAY_SESSIONS WITH (INDEX (IX_ps_finished_status)) 
                                     , TERMINALS
                                WHERE  PS_FINISHED <  DATEADD(DAY, 1, @pConcatDate)
                                  AND  PS_FINISHED >= @pConcatDate
                                  AND  PS_STATUS <> 0 AND PS_PROMO = 0
                                  AND  PS_TERMINAL_ID = TE_TERMINAL_ID
                                  AND  TE_TERMINAL_TYPE IN (1, 3, 5)
                             GROUP BY  TE_PROVIDER_ID, TE_NAME                             
                           ) 
                           AS SUM_PLAY_SESSIONS
                    GROUP BY TE_PROVIDER_ID, TE_NAME               
                   )
                  AS PROVIDERS_SALES
             GROUP BY PROVIDER, MACHINE_NAME
            ) 
            AS PROVIDERS_STATS
    ) TOP_TERMINALS
    WHERE RANKING <= 10
    ORDER BY V_NETWIN DESC
    -- END : WORST 10 - By NetWin -
   END
  ELSE IF @pConcept = 2
   BEGIN
    -- WORST 10 - By Payable NetWin - 
    SELECT TOP 10 *
    FROM
    ( SELECT *
          FROM
            (
                SELECT  PROVIDER                 AS BTN_PROVIDER
                      , MACHINE_NAME             AS BTN_MACHINE_NAME
                      , SUM(CASH_IN_REDEEM - CASH_OUT_REDEEM)  AS V_NETWIN_REDEEM
                      , RANK() OVER (PARTITION BY PROVIDER ORDER BY SUM(CASH_IN_REDEEM - CASH_OUT_REDEEM) DESC) AS RANKING
                  FROM
                  (
                        SELECT  TE_PROVIDER_ID AS PROVIDER
                              , TE_NAME        AS MACHINE_NAME
                              , SUM(CASH_IN_REDEEM)   AS CASH_IN_REDEEM
                              , SUM(CASH_OUT_REDEEM)  AS CASH_OUT_REDEEM
                          FROM (   SELECT  TE_PROVIDER_ID
                                         , TE_NAME
                                         , SUM(PS_REDEEMABLE_CASH_IN)  AS CASH_IN_REDEEM 
                                         , SUM(PS_REDEEMABLE_CASH_OUT) AS CASH_OUT_REDEEM 
                                     FROM  PLAY_SESSIONS WITH (INDEX (IX_ps_finished_status)) 
                                         , TERMINALS
                                    WHERE  PS_FINISHED <  DATEADD(DAY, 1, @pConcatDate)
                                      AND  PS_FINISHED >= @pConcatDate
                                      AND  PS_STATUS <> 0 AND PS_PROMO = 0
                                      AND  PS_TERMINAL_ID = TE_TERMINAL_ID
                                      AND  TE_TERMINAL_TYPE IN (1, 3, 5)
                                 GROUP BY  TE_PROVIDER_ID, TE_NAME                             
                               ) 
                               AS SUM_PLAY_SESSIONS
                      GROUP BY TE_PROVIDER_ID, TE_NAME               
                   )
                  AS PROVIDERS_SALES
             GROUP BY PROVIDER, MACHINE_NAME
            ) 
            AS PROVIDERS_STATS
    ) TOP_TERMINALS
    WHERE RANKING <= 10
    ORDER BY V_NETWIN_REDEEM DESC
    -- END : WORST 10 - By Payable NetWin -
   END
       
END
ELSE  
IF @pReport = 2
BEGIN
  ---------------------------------------------------------------------------------
  -- Accumulated

DECLARE @pConcatDate2007 DateTime
DECLARE @pDateFrom as DateTime
DECLARE @pDateTo as DateTime
DECLARE @pDateFromConcat as DateTime
DECLARE @pDateToConcat as DateTime
SET @pConcatDate = dbo.ConcatOpeningTime(0, @pDate)
SET @pConcatDate2007 = dbo.ConcatOpeningTime(0, '2007/01/01')


SET @pDateFrom = DATEADD(DAY, -11, @pDate)
SET @pDateTo = DATEADD(DAY, 1, @pDate)

SET @pDateFromConcat = DATEADD(DAY, -11, @pConcatDate)
SET @pDateToConcat = DATEADD(DAY, 1, @pConcatDate)

DECLARE @_date_offset AS INT
SET @_date_offset = -1

DECLARE @DATA_TABLE AS TABLE (BASE_DATE DATE, NUM_TERMINALS INT, VALUE MONEY)

WHILE (@_date_offset > -11)
BEGIN
  INSERT INTO   @DATA_TABLE 
       SELECT   CAST(DATEADD(DAY, @_date_offset, @pConcatDate) AS DATE), 0, 0 
  SET @_date_offset = @_date_offset - 1
END

SELECT CAST(X.BASE_DATE AS DATE) AS BASE_DATE, SUM(X.NUM_TERMINALS) AS TERMINALS, SUM(X.VALUE) AS [VALUES] FROM (

  SELECT *
    FROM (
  SELECT * FROM @DATA_TABLE
  ) as A
    UNION 
  (
  SELECT   DISTINCT BASE_DATE 
           , SUM(NUM_TERMINALS)                     AS NUM_TERMINALS
           , CASE @pConcept
              WHEN 0 THEN SUM(PLAYED)                          
              WHEN 1 THEN SUM(NETWIN)
              WHEN 2 THEN SUM(NETWIN_REDEEM)
             END AS VALUE
        FROM
          (
              SELECT  PROVIDER
                      , BASE_DATE
                      , SUM(NUM_TERMINALS)                     AS NUM_TERMINALS
                      , SUM(PLAYED)                            AS PLAYED
                      , SUM(CASH_IN - CASH_OUT)  	             AS NETWIN
                      , SUM(CASH_IN_REDEEM - CASH_OUT_REDEEM)  AS NETWIN_REDEEM
                FROM
                ((  SELECT  TE_PROVIDER_ID          AS PROVIDER
                               , TC_DATE            AS BASE_DATE
                               , COUNT(*)           AS NUM_TERMINALS
                               , 0                  AS CASH_IN
                               , 0                  AS CASH_OUT
                               , 0                  AS CASH_IN_REDEEM
                               , 0                  AS CASH_OUT_REDEEM
                               , 0                  AS PLAYED
                    FROM  TERMINALS_CONNECTED WITH (INDEX (PK_terminals_connected)) 
                      , TERMINALS WITH (INDEX (PK_terminals)) 
                     WHERE  TC_DATE <  @pDateTo
                       AND  TC_DATE >= @pDateFrom
                       AND  TC_CONNECTED = 1
                       AND  TC_TERMINAL_ID = TE_TERMINAL_ID 
                       AND  TE_TYPE = 1 
                       AND  TE_TERMINAL_TYPE IN (1, 3, 5)
                GROUP BY  TE_PROVIDER_ID, TC_DATE
                 )
                 UNION
                 (
                      SELECT  PS_PROVIDER              AS PROVIDER
                               , PS_BASE_DAY           AS BASE_DATE
                               , 0                     AS NUM_TERMINALS
                               , SUM(CASH_IN)          AS CASH_IN
                               , SUM(CASH_OUT)         AS CASH_OUT
                               , SUM(CASH_IN_REDEEM)   AS CASH_IN_REDEEM
                               , SUM(CASH_OUT_REDEEM)  AS CASH_OUT_REDEEM
                               , 0                     AS PLAYED
                          FROM (   SELECT  DATEADD (DAY, DATEDIFF (HOUR, @pConcatDate2007, PS_FINISHED) / 24, '2007/01/01') PS_BASE_DAY
                                              , ( SELECT  TE_PROVIDER_ID 
                                                        FROM  TERMINALS 
                                                       WHERE  TE_TERMINAL_ID = PLAY_SESSIONS.PS_TERMINAL_ID ) AS PS_PROVIDER 
                                              , SUM(PS_TOTAL_CASH_IN)       AS CASH_IN 
                                              , SUM(PS_TOTAL_CASH_OUT)      AS CASH_OUT 
                                              , SUM(PS_REDEEMABLE_CASH_IN)  AS CASH_IN_REDEEM 
                                              , SUM(PS_REDEEMABLE_CASH_OUT) AS CASH_OUT_REDEEM 
                                        FROM  PLAY_SESSIONS WITH (INDEX (IX_ps_finished_status)) 
                                       WHERE  PS_FINISHED <  @pDateToConcat
                                         AND  PS_FINISHED >= @pDateFromConcat
                                         AND  PS_STATUS <> 0 AND PS_PROMO = 0
                                  GROUP BY  DATEADD (DAY, DATEDIFF (HOUR, @pConcatDate2007, PS_FINISHED) / 24, '2007/01/01')
                                              , PS_TERMINAL_ID
                               ) 
                                AS SUM_PLAY_SESSIONS
                    GROUP BY PS_PROVIDER, PS_BASE_DAY
                 )
                 UNION
                 (  
                      SELECT  SPH_PROVIDER   AS PROVIDER
                               , SPH_BASE_DAY   AS BASE_DATE
                               , 0              AS NUM_TERMINALS
                               , 0              AS CASH_IN
                               , 0              AS CASH_OUT
                               , 0              AS CASH_IN_REDEEM
                               , 0              AS CASH_OUT_REDEEM                             
                               , SUM(PLAYED)    AS PLAYED
                          FROM (   SELECT  DATEADD (DAY, DATEDIFF (HOUR, @pConcatDate2007, SPH_BASE_HOUR) / 24, '2007/01/01') SPH_BASE_DAY
                                              , ( SELECT  TE_PROVIDER_ID 
                                                        FROM  TERMINALS 
                                                       WHERE  TE_TERMINAL_ID = SPH_TERMINAL_ID ) AS SPH_PROVIDER 
                                              , SUM(SPH_PLAYED_AMOUNT) AS PLAYED 
                                        FROM  SALES_PER_HOUR WITH (INDEX (PK_sales_per_hour)) 
                                       WHERE  SPH_BASE_HOUR <  @pDateToConcat
                                         AND  SPH_BASE_HOUR >= @pDateFromConcat
                                  GROUP BY  DATEADD (DAY, DATEDIFF (HOUR, @pConcatDate2007, SPH_BASE_HOUR) / 24, '2007/01/01')
                                              , SPH_TERMINAL_ID
                               ) 
                                AS SUM_SALES_PER_HOUR
                    GROUP BY SPH_PROVIDER, SPH_BASE_DAY
                ))
                AS PROVIDERS_SALES
              GROUP BY PROVIDER, BASE_DATE
          ) 
          AS PROVIDERS_STATS
     WHERE NUM_TERMINALS > 0
    GROUP BY BASE_DATE
  ) 
)  AS X
GROUP BY BASE_DATE
ORDER BY BASE_DATE ASC
  
END
ELSE  
IF @pReport = 3
BEGIN
  ---------------------------------------------------------------------------------
  -- Providers
  SET @pConcatDate = dbo.ConcatOpeningTime(0, @pDate)
  SET @pConcatDate2007 = dbo.ConcatOpeningTime(0, '2007/01/01')
  
  SET @pDateFrom = DATEADD(DAY, -11, @pDate)
  SET @pDateTo = DATEADD(DAY, 1, @pDate)

  SET @pDateFromConcat = DATEADD(DAY, -11, @pConcatDate)
  SET @pDateToConcat = DATEADD(DAY, 1, @pConcatDate)

  DECLARE @_times as TABLE (TIME_DATE DATETIME)
  INSERT INTO @_times SELECT * FROM dbo.wsi_time_table(@pDateToConcat, @pDateFromConcat, 0, -1)

               SELECT   PROVIDER
                      , CAST(BASE_DATE AS DATE) AS BASE_DATE
                      , CASE @pConcept
                         WHEN 0 THEN SUM(PLAYED)
                         WHEN 1 THEN SUM(CASH_IN - CASH_OUT)
                         WHEN 2 THEN SUM(CASH_IN_REDEEM - CASH_OUT_REDEEM)
                        END AS TD_VALUE
                        
                INTO ##DATA_TABLE_1
                
                FROM
                ((  SELECT  TE_PROVIDER_ID          AS PROVIDER
                               , TC_DATE            AS BASE_DATE
                               , COUNT(*)           AS NUM_TERMINALS
                               , 0                  AS CASH_IN
                               , 0                  AS CASH_OUT
                               , 0                  AS CASH_IN_REDEEM
                               , 0                  AS CASH_OUT_REDEEM
                               , 0                  AS PLAYED
                    FROM  TERMINALS_CONNECTED WITH (INDEX (PK_terminals_connected)) 
                      , TERMINALS WITH (INDEX (PK_terminals)) 
                     WHERE  TC_DATE <  @pDateTo
                       AND  TC_DATE >= @pDateFrom
                       AND  TC_CONNECTED = 1
                       AND  TC_TERMINAL_ID = TE_TERMINAL_ID 
                       AND  TE_TYPE = 1 
                       AND  TE_TERMINAL_TYPE IN (1, 3, 5)
                GROUP BY  TE_PROVIDER_ID, TC_DATE
                 )
                 UNION
                 (
                      SELECT  PS_PROVIDER              AS PROVIDER
                               , PS_BASE_DAY           AS BASE_DATE
                               , 0                     AS NUM_TERMINALS
                               , SUM(CASH_IN)          AS CASH_IN
                               , SUM(CASH_OUT)         AS CASH_OUT
                               , SUM(CASH_IN_REDEEM)   AS CASH_IN_REDEEM
                               , SUM(CASH_OUT_REDEEM)  AS CASH_OUT_REDEEM
                               , 0                     AS PLAYED
                          FROM (   SELECT  DATEADD (DAY, DATEDIFF (HOUR, @pConcatDate2007, PS_FINISHED) / 24, '2007/01/01') PS_BASE_DAY
                                              , ( SELECT  TE_PROVIDER_ID 
                                                        FROM  TERMINALS 
                                                       WHERE  TE_TERMINAL_ID = PLAY_SESSIONS.PS_TERMINAL_ID ) AS PS_PROVIDER 
                                              , SUM(PS_TOTAL_CASH_IN)       AS CASH_IN 
                                              , SUM(PS_TOTAL_CASH_OUT)      AS CASH_OUT 
                                              , SUM(PS_REDEEMABLE_CASH_IN)  AS CASH_IN_REDEEM 
                                              , SUM(PS_REDEEMABLE_CASH_OUT) AS CASH_OUT_REDEEM 
                                        FROM  PLAY_SESSIONS WITH (INDEX (IX_ps_finished_status)) 
                                       WHERE  PS_FINISHED <  @pDateToConcat
                                         AND  PS_FINISHED >= @pDateFromConcat
                                         AND  PS_STATUS <> 0 AND PS_PROMO = 0
                                  GROUP BY  DATEADD (DAY, DATEDIFF (HOUR, @pConcatDate2007, PS_FINISHED) / 24, '2007/01/01')
                                              , PS_TERMINAL_ID
                               ) 
                                AS SUM_PLAY_SESSIONS
                    GROUP BY PS_PROVIDER, PS_BASE_DAY
                 )
                 UNION
                 (  
                      SELECT  SPH_PROVIDER      AS PROVIDER
                               , SPH_BASE_DAY   AS BASE_DATE
                               , 0              AS NUM_TERMINALS
                               , 0              AS CASH_IN
                               , 0              AS CASH_OUT
                               , 0              AS CASH_IN_REDEEM
                               , 0              AS CASH_OUT_REDEEM                             
                               , SUM(PLAYED)    AS PLAYED
                          FROM (   SELECT  DATEADD (DAY, DATEDIFF (HOUR, @pConcatDate2007, SPH_BASE_HOUR) / 24, '2007/01/01') SPH_BASE_DAY
                                              , ( SELECT  TE_PROVIDER_ID 
                                                        FROM  TERMINALS 
                                                       WHERE  TE_TERMINAL_ID = SPH_TERMINAL_ID ) AS SPH_PROVIDER 
                                              , SUM(SPH_PLAYED_AMOUNT) AS PLAYED 
                                        FROM  SALES_PER_HOUR WITH (INDEX (PK_sales_per_hour)) 
                                       WHERE  SPH_BASE_HOUR <  @pDateToConcat
                                         AND  SPH_BASE_HOUR >= @pDateFromConcat
                                  GROUP BY  DATEADD (DAY, DATEDIFF (HOUR, @pConcatDate2007, SPH_BASE_HOUR) / 24, '2007/01/01')
                                              , SPH_TERMINAL_ID
                               ) 
                                AS SUM_SALES_PER_HOUR
                    GROUP BY SPH_PROVIDER, SPH_BASE_DAY
                )
                UNION
                (
                SELECT   NULL      AS PROVIDER
                       , TIME_DATE      AS BASE_DATE
                       , 0              AS NUM_TERMINALS
                       , 0              AS CASH_IN
                       , 0              AS CASH_OUT
                       , 0              AS CASH_IN_REDEEM
                       , 0              AS CASH_OUT_REDEEM  
                       , 0    AS PLAYED
                  FROM @_times 
                )
                ) AS PROVIDERS_SALES
              GROUP BY PROVIDER, BASE_DATE
              
              
--SELECT * FROM @_times
--  LEFT   JOIN ##DATA_TABLE_1 AS VVV ON VVV.BASE_DATE = CAST(TIME_DATE AS DATE)

    EXEC wsi_table_pivot N'##DATA_TABLE_1', N'PROVIDER', N'TD_VALUE'

    DROP TABLE ##DATA_TABLE_1

  
END
ELSE  
IF @pReport = 4
BEGIN
  ---------------------------------------------------------------------------------
  -- Period
  
  SELECT  PROVIDER                 AS BTP_PROVIDER
                      , CASE @pConcept
                          WHEN 0 THEN SUM(PLAYED)
                          WHEN 1 THEN SUM (CASH_IN - CASH_OUT)
                          WHEN 2 THEN SUM (CASH_IN - CASH_OUT)
                        END  AS BTP_VALUE
                  FROM
                  ((
                        SELECT  TE_PROVIDER_ID AS PROVIDER
                              , SUM(ISNULL(CASH_IN,0))   AS CASH_IN
                              , SUM(ISNULL(CASH_OUT,0))  AS CASH_OUT
                              , 0              AS PLAYED
                          FROM (   SELECT  TE_PROVIDER_ID
                                         , SUM(PS_TOTAL_CASH_IN)  AS CASH_IN 
                                         , SUM(PS_TOTAL_CASH_OUT) AS CASH_OUT 
                                     FROM  PLAY_SESSIONS WITH (INDEX (IX_ps_finished_status)) 
                                         , TERMINALS
                                    WHERE  PS_FINISHED <  DATEADD(DAY, 1, @pConcatDate)
                                      AND  PS_FINISHED >= @pConcatDate
                                      AND  PS_STATUS <> 0 AND PS_PROMO = 0
                                      AND  PS_TERMINAL_ID = TE_TERMINAL_ID
                                      AND  TE_TERMINAL_TYPE IN (1, 3, 5)
                                 GROUP BY  TE_PROVIDER_ID 
                               ) 
                               AS SUM_PLAY_SESSIONS
                      GROUP BY TE_PROVIDER_ID            
                   )
                   UNION
                   (  
                     SELECT  SPH_PROVIDER   AS PROVIDER
                          , 0               AS CASH_IN
                          , 0               AS CASH_OUT
                          , SUM(ISNULL(PLAYED, 0))     AS PLAYED
                      FROM (   SELECT  ( SELECT  TE_PROVIDER_ID 
                                           FROM  TERMINALS 
                                          WHERE  TE_TERMINAL_ID = SPH_TERMINAL_ID ) AS SPH_PROVIDER 
                                     , SUM(SPH_PLAYED_AMOUNT) AS PLAYED 
                                 FROM  SALES_PER_HOUR WITH (INDEX (PK_sales_per_hour)) 
                                WHERE  SPH_BASE_HOUR <  DATEADD(DAY, 1, @pConcatDate)
                                  AND  SPH_BASE_HOUR >= @pConcatDate
                             GROUP BY  SPH_TERMINAL_ID
                           ) 
                           AS SUM_SALES_PER_HOUR
                      GROUP BY SPH_PROVIDER 
                  ))
                  AS PROVIDERS_SALES
             GROUP BY PROVIDER
    
END

END


GO


