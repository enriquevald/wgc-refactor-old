USE WGDB_000;

SET ANSI_NULLS ON;

SET QUOTED_IDENTIFIER ON;

----------------------------------------------------
------------------GENERAL PARAMS--------------------
----------------------------------------------------

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'SmartFloor' AND GP_SUBJECT_KEY ='Notification.Type')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('SmartFloor'
             ,'Notification.Type'
             ,'0')
GO

----------------------------------------------------
----------------------VERSION-----------------------
----------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[db_version_smartfloor]') AND type in (N'U'))
DROP TABLE [dbo].[db_version_smartfloor];
GO
CREATE TABLE [dbo].[db_version_smartfloor](
  [db_client_id] [int] NOT NULL,
  [db_common_build_id] [int] NOT NULL,
  [db_client_build_id] [int] NOT NULL,
  [db_release_id] [int] NOT NULL,
  [db_updated_script] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [db_updated] [datetime] NULL,
  [db_description] [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_db_version_smartfloor] PRIMARY KEY CLUSTERED 
(
  [db_client_id] ASC,
  [db_common_build_id] ASC,
  [db_client_build_id] ASC,
  [db_release_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

----------------------------------------------------
-----------------------FIELDS-----------------------
----------------------------------------------------

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[layout_site_tasks]') and name = 'lst_title')
  ALTER TABLE [dbo].[layout_site_tasks] ADD [lst_title] NVARCHAR(50) NULL 
ELSE
  SELECT '***** Field laout_site_tasks.lst_title already exists *****';
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[layout_site_alarms]') and name = 'lsa_title')
  ALTER TABLE [dbo].[layout_site_alarms] ADD [lsa_title] NVARCHAR(50) NULL 
ELSE
  SELECT '***** Field laout_site_alarms.lsa_title already exists *****';
GO

----------------------------------------------------
----------------------STOREDS-----------------------
----------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_InsertTasksDerivations]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_InsertTasksDerivations]
GO
CREATE  PROCEDURE [dbo].[Layout_InsertTasksDerivations]
      @pTaskId INT
    , @pUserId INT
    , @pDateRejected DATETIME
AS
BEGIN
      IF EXISTS (SELECT TOP 1 * 
                   FROM LAYOUT_SITE_TASK_DERIVATIONS
                  WHERE LSTD_TASK_ID = @pTaskId
                    AND LSTD_USER_ID = @pUserId)
                    
       UPDATE     LAYOUT_SITE_TASK_DERIVATIONS
          SET     LSTD_REJECTED_DATE = @pDateRejected
        WHERE     LSTD_TASK_ID = @pTaskId
          AND LSTD_USER_ID = @pUserId
      ELSE
      INSERT INTO LAYOUT_SITE_TASK_DERIVATIONS(
                  LSTD_TASK_ID
                , LSTD_USER_ID
                , LSTD_REJECTED_DATE)
           VALUES(@pTaskId
                , @pUserId
                , NULL)
END 
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_ReleaseUserDerivation]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_ReleaseUserDerivation]
GO
CREATE PROCEDURE [dbo].[Layout_ReleaseUserDerivation]
           @pTaskId INT
AS
BEGIN

IF EXISTS (SELECT TOP 1 * 
                   FROM LAYOUT_SITE_TASK_DERIVATIONS
                   WHERE      LSTD_TASK_ID = @pTaskId)
 BEGIN
   DELETE FROM LAYOUT_SITE_TASK_DERIVATIONS
    WHERE   LSTD_TASK_ID = @pTaskId
  END
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_ReleaseAlarmFromTask]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_ReleaseAlarmFromTask]
GO
CREATE PROCEDURE [dbo].[Layout_ReleaseAlarmFromTask]
           @pTaskId INT
         , @pUserId INT
         , @pOutputDerivations INT OUTPUT
AS
BEGIN
DECLARE @DerivationsLeft AS INT
DECLARE @Today AS DATETIME
     SET @DerivationsLeft = 0
     SET @Today = GETDATE()
     SET @pOutputDerivations = 0
     
 IF EXISTS (SELECT TOP 1 * 
                   FROM LAYOUT_SITE_TASK_DERIVATIONS
                  WHERE LSTD_TASK_ID = @pTaskId
                    AND LSTD_USER_ID = @pUserId)
BEGIN
      EXEC Layout_InsertTasksDerivations @pTaskId, @pUserId, @Today

    SELECT @DerivationsLeft = COUNT(*) 
      FROM LAYOUT_SITE_TASK_DERIVATIONS 
     WHERE LSTD_TASK_ID = @pTaskId 
       AND LSTD_REJECTED_DATE IS NULL
       
       SET @pOutputDerivations = @DerivationsLeft

    IF @DerivationsLeft = 0
    BEGIN
            UPDATE LAYOUT_SITE_ALARMS 
               SET LSA_TASK_ID = NULL
                 , LSA_DATE_TO_TASK = NULL         
                 , LSA_STATUS = 2 
         WHERE LSA_TASK_ID = @pTaskId
         
        DELETE FROM LAYOUT_SITE_TASK_DERIVATIONS
         WHERE LSTD_TASK_ID = @pTaskId
    END 
  END
  
  SELECT @pOutputDerivations
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_SaveSiteTask]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_SaveSiteTask]
GO
CREATE PROCEDURE [dbo].[Layout_SaveSiteTask]
      @pTaskId INT
    , @pStatus INT
    , @pStart DATETIME
    , @pEnd DATETIME
    , @pCategory INT
    , @pSubcategory INT
    , @pTerminalId INT
    , @pAccountId INT
    , @pDescription NVARCHAR(MAX)
    , @pSeverity INT
    , @pCreationUserId INT
    , @pCreation DATETIME
    , @pAssignedRoleId INT
    , @pAssignedUserId INT
    , @pAssigned DATETIME
    , @pAcceptedUserId INT
    , @pAccepted DATETIME
    , @pScaleFromUserId INT
    , @pScaleToUserId INT
    , @pScaleReason NVARCHAR(250)
    , @pScale DATETIME
    , @pSolvedUserId INT
    , @pSolved DATETIME
    , @pValidateUserId INT
    , @pValidate DATETIME
    , @pLastStatusUpdateUserId INT
    , @pLastStatusUpdate DATETIME
    , @pAttachedMedia INT
    , @pHistory NVARCHAR(MAX)
    , @pOutputFilterId INT OUTPUT
	  , @pTitle NVARCHAR(50)
AS
BEGIN
      IF EXISTS (SELECT TOP 1 * 
                   FROM LAYOUT_SITE_TASKS
                  WHERE	LST_ID = @pTaskId)
                    
            UPDATE	LAYOUT_SITE_TASKS
               SET	LST_STATUS = @pStatus
                 ,  LST_START = @pStart
                 ,  LST_END = @pEnd
                 ,  LST_CATEGORY = @pCategory 
                 ,  LST_SUBCATEGORY = @pSubcategory 
                 ,  LST_TERMINAL_ID = @pTerminalId
                 ,  LST_ACCOUNT_ID = @pAccountId 
                 ,  LST_DESCRIPTION = @pDescription 
                 ,  LST_SEVERITY = @pSeverity 
                 ,  LST_CREATION_USER_ID = @pCreationUserId 
                 ,  LST_CREATION = @pCreation 
                 ,  LST_ASSIGNED_ROLE_ID = @pAssignedRoleId 
                 ,  LST_ASSIGNED_USER_ID = @pAssignedUserId 
                 ,  LST_ASSIGNED = @pAssigned 
                 ,  LST_ACCEPTED_USER_ID = @pAcceptedUserId 
                 ,  LST_ACCEPTED = @pAccepted 
                 ,  LST_SCALE_FROM_USER_ID = @pScaleFromUserId 
                 ,  LST_SCALE_TO_USER_ID = @pScaleToUserId 
                 ,  LST_SCALE_REASON = @pScaleReason 
                 ,  LST_SCALE = @pScale 
                 ,  LST_SOLVED_USER_ID = @pSolvedUserId 
                 ,  LST_SOLVED = @pSolved 
                 ,  LST_VALIDATE_USER_ID = @pValidateUserId 
                 ,  LST_VALIDATE = @pValidate 
                 ,  LST_LAST_STATUS_UPDATE_USER_ID = @pLastStatusUpdateUserId 
                 ,  LST_LAST_STATUS_UPDATE = @pLastStatusUpdate 
                 ,  LST_ATTACHED_MEDIA = @pAttachedMedia
                 ,  LST_EVENTS_HISTORY = @pHistory
				         ,  LST_TITLE = @pTitle
             WHERE	LST_ID = @pTaskId
      ELSE
       INSERT INTO	 LAYOUT_SITE_TASKS
                   ( LST_STATUS
                   , LST_START
                   , LST_END
                   , LST_CATEGORY
                   , LST_SUBCATEGORY
                   , LST_TERMINAL_ID
                   , LST_ACCOUNT_ID
                   , LST_DESCRIPTION
                   , LST_SEVERITY
                   , LST_CREATION_USER_ID
                   , LST_CREATION
                   , LST_ASSIGNED_ROLE_ID
                   , LST_ASSIGNED_USER_ID
                   , LST_ASSIGNED
                   , LST_ACCEPTED_USER_ID
                   , LST_ACCEPTED
                   , LST_SCALE_FROM_USER_ID
                   , LST_SCALE_TO_USER_ID
                   , LST_SCALE_REASON
                   , LST_SCALE
                   , LST_SOLVED_USER_ID
                   , LST_SOLVED
                   , LST_VALIDATE_USER_ID
                   , LST_VALIDATE
                   , LST_LAST_STATUS_UPDATE_USER_ID
                   , LST_LAST_STATUS_UPDATE
                   , LST_ATTACHED_MEDIA
                   , LST_EVENTS_HISTORY
				           , LST_TITLE)
            VALUES 
				           ( @pStatus 
                   , @pStart 
                   , @pEnd 
                   , @pCategory 
                   , @pSubcategory 
                   , @pTerminalId 
                   , @pAccountId 
                   , @pDescription 
                   , @pSeverity 
                   , @pCreationUserId 
                   , @pCreation 
                   , @pAssignedRoleId 
                   , @pAssignedUserId 
                   , @pAssigned 
                   , @pAcceptedUserId 
                   , @pAccepted 
                   , @pScaleFromUserId 
                   , @pScaleToUserId 
                   , @pScaleReason 
                   , @pScale 
                   , @pSolvedUserId 
                   , @pSolved 
                   , @pValidateUserId 
                   , @pValidate 
                   , @pLastStatusUpdateUserId 
                   , @pLastStatusUpdate 
                   , @pAttachedMedia
                   , @pHistory
				           , @pTitle )
SELECT @pOutputFilterId=SCOPE_IDENTITY();
                  
END --LAYOUT_SITE_TASKS
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_SaveSiteAlarm]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_SaveSiteAlarm]
GO
CREATE PROCEDURE [dbo].[Layout_SaveSiteAlarm]
    @pTerminalId INT
  , @pAlarmId INT
  , @pDateCreated DATETIME
  , @pAlarmType INT
  , @pAlarmSource INT
  , @pUserCreated INT
  , @pMediaId INT
  , @pDescription NVARCHAR(MAX)
  , @pDateToTask DATETIME
  , @pTaskId INT
  , @pStatus INT
  , @pTitle NVARCHAR(50)
AS
BEGIN
  IF EXISTS (SELECT   TOP 1 * FROM   LAYOUT_SITE_ALARMS WHERE   LSA_TERMINAL_ID  = @pTerminalId AND   LSA_ALARM_ID     = @pAlarmId AND   LSA_DATE_CREATED = @pDateCreated)
    UPDATE   LAYOUT_SITE_ALARMS
       SET   LSA_DATE_TO_TASK = @pDateToTask
           , LSA_TASK_ID      = @pTaskId
           , LSA_STATUS       = @pStatus
     WHERE   LSA_TERMINAL_ID  = @pTerminalId
       AND   LSA_ALARM_ID     = @pAlarmId
       AND   LSA_DATE_CREATED = @pDateCreated
  ELSE
    INSERT INTO   LAYOUT_SITE_ALARMS
                ( LSA_TERMINAL_ID
                , LSA_ALARM_ID
                , LSA_DATE_CREATED
                , LSA_ALARM_TYPE
                , LSA_ALARM_SOURCE
                , LSA_USER_CREATED
                , LSA_MEDIA_ID
                , LSA_DESCRIPTION
                , LSA_DATE_TO_TASK
                , LSA_TASK_ID
                , LSA_STATUS
                , LSA_TITLE )
         VALUES ( @pTerminalId 
                , @pAlarmId
                , @pDateCreated 
                , @pAlarmType 
                , @pAlarmSource 
                , @pUserCreated 
                , @pMediaId 
                , @pDescription 
                , @pDateToTask 
                , @pTaskId
                , 0
                , @pTitle ) 
END 
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_AutoNotificationAlarm]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_AutoNotificationAlarm]
GO 
CREATE  PROCEDURE [dbo].[Layout_AutoNotificationAlarm]
      @pStatus INT
    , @pCategory INT
    , @pSubcategory INT
    , @pTerminalId INT
    , @pSeverity INT
    , @pCreationUserId INT
    , @pCreation DATETIME
    , @pAssignedRoleId INT
    , @pAssigned DATETIME
    , @pHistory NVARCHAR(MAX)
    , @pDateCreated DATETIME
    , @poutTaskId INT OUTPUT
AS
BEGIN
DECLARE @TaskId as INT

EXEC Layout_SaveSiteTask -1,@pStatus,NULL,NULL,@pCategory,@pSubcategory,@pTerminalId,NULL,NULL,@pSeverity,@pCreationUserId,@pCreation,@pAssignedRoleId,NULL,@pAssigned,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,@pAssigned,NULL,@pHistory,@TaskId OUTPUT,NULL

  UPDATE LAYOUT_SITE_ALARMS
     SET LSA_TASK_ID      = @TaskId
       , LSA_DATE_TO_TASK = @pDateCreated
   WHERE LSA_TERMINAL_ID  = @pTerminalId
     AND LSA_ALARM_ID     = @pSubcategory
     AND LSA_DATE_CREATED = @pDateCreated
     
     SELECT @poutTaskId = @TaskId;
END 
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CreateDefaultProfilesLayout]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[CreateDefaultProfilesLayout]
GO 
CREATE PROCEDURE [dbo].[CreateDefaultProfilesLayout]
AS 
BEGIN  
  
  DECLARE @ProfileId   BIGINT
  DECLARE @ProfileName NVARCHAR(40)
  DECLARE @pGuiId      INT
  
  SET @ProfileId = 0
  SET @ProfileName = ''
  SET @pGuiId = 202
  
  --Comprovaci� exist�ncia permisos
  IF EXISTS (SELECT GF_GUI_ID FROM GUI_FORMS WHERE GF_GUI_ID = @pGuiId)
  BEGIN 
  
    --********************************
    --**PCA - PLAYERS CLUB ATTENDANT**
    --********************************
    SET @ProfileName = 'LAYOUT - PLAYERS CLUB ATTENDANT'     
    
    --Comprovaci� exist�ncia perfil
    IF NOT EXISTS (SELECT  GUP_PROFILE_ID FROM GUI_USER_PROFILES WHERE UPPER (GUP_NAME) LIKE @ProfileName ESCAPE '\' ) 
    BEGIN
    
      --Obtenci� de l�ultim ID Profile (tal i com ho fa el GUI �?�?�?)
      SET @ProfileId = (SELECT MAX(ISNULL(GUP_PROFILE_ID, 0)) + 1  FROM GUI_USER_PROFILES)
      
      --Insert del Perfil
      INSERT INTO GUI_USER_PROFILES (GUP_PROFILE_ID, GUP_NAME, GUP_MAX_USERS) VALUES (@ProfileId, @ProfileName, 0)
      
      
      --Assignar Permisos de cada pantalla al perfil creat
      
      --Pantalla (PCA - LOGIN)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1, 1, 1, 1, 1) 
      
           
      --Pantalla (PCA - Dashboard)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1010000, 1, 1, 1, 1) 
           
      --Pantalla (PCA - Dashboard - Important)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1010100, 0, 0, 0, 0) 
           
      --Pantalla (PCA - Dashboard - Statistics)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1010200, 1, 1, 1, 1) 
           
      --Pantalla (PCA - Dashboard - Ocupattion)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1010201, 1, 1, 1, 1) 
      
      --Pantalla (PCA - Dashboard - Ocupattion by card)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1010202, 1, 1, 1, 1) 
           
      --Pantalla (PCA - Dashboard - VIP Customers)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1010203, 1, 1, 1, 1) 
           
      --Pantalla (PCA - Dashboard - Ocupattion Percentage by gender)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1010204, 1, 1, 1, 1) 

      --Pantalla (PCA - Dashboard - Ocupattion Percentage by age)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1010205, 1, 1, 1, 1) 

      --Pantalla (PCA - Dashboard - Evaluattion by age and gender)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1010206, 1, 1, 1, 1) 

      --Pantalla (PCA - Dashboard - Technics issues percentage)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1010207, 0, 0, 0, 0) 

      --Pantalla (PCA - Dashboard - Coin in, ANW, spins by game and manufacturer representation)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1010208, 1, 1, 1, 1) 

      --Pantalla (PCA - Dashboard - Gain Floor by machine and coin representation)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1010209, 1, 1, 1, 1) 

      --Pantalla (PCA - Dashboard - Coin in evaluation)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1010210, 1, 1, 1, 1) 

      --Pantalla (PCA - Floor)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1020000, 1, 1, 1, 1) 

      --Pantalla (PCA � Floor � Compare Terminals)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1020100, 1, 1, 1, 1) 

 		 --Pantalla (PCA � Floor � Temperature Map)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1020200, 1, 1, 1, 1) 

      --Pantalla (PCA � Floor � Visualization)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1020300, 1, 1, 1, 1) 

      --Pantalla (PCA - My Tasks)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1030000, 1, 1, 1, 1) 

      --Pantalla (PCA - Alarms - All)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1040000, 1, 1, 1, 1) 

      --Pantalla (PCA - Alarms - Players Only)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1040001, 1, 1, 1, 1) 

      --Pantalla (PCA - Alarms - Machines Only)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1040002, 1, 1, 1, 1) 
      
      --Pantalla (PCA - Charts)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1050000, 1, 1, 1, 1) 
           
      --Pantalla (PCA - Players)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1060000, 1, 1, 1, 1) 

      --Pantalla (PCA - Players - List Mode)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1060001, 1, 1, 1, 1) 

      --Pantalla (PCA - Players - Find)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1060100, 1, 1, 1, 1) 

      --Pantalla (PCA - Players - Filter)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1060200, 1, 1, 1, 1) 

      --Pantalla (PCA - Machines)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1070000, 0, 0, 0, 0) 

      --Pantalla (PCA - Comparation Machines)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1070001, 0, 0, 0, 0) 
  
    END
    --********************************
    --**OPS - OPS MANAGER ************
    --********************************
    SET @ProfileName = 'LAYOUT - OPS MANAGER'     
    
    --Comprovaci� exist�ncia perfil
    IF NOT EXISTS (SELECT  GUP_PROFILE_ID FROM GUI_USER_PROFILES WHERE UPPER (GUP_NAME) LIKE @ProfileName ESCAPE '\' ) 
    BEGIN
    
      --Obtenci� de l�ultim ID Profile (tal i com ho fa el GUI �?�?�?)
      SET @ProfileId = (SELECT MAX(ISNULL(GUP_PROFILE_ID, 0)) + 1  FROM GUI_USER_PROFILES)
      
      --Insert del Perfil
      INSERT INTO GUI_USER_PROFILES (GUP_PROFILE_ID, GUP_NAME, GUP_MAX_USERS) VALUES (@ProfileId, @ProfileName, 0)
      
      --Assignar Permisos de cada pantalla al perfil creat
      --*OPS
      --Pantalla (OPS - LOGIN)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1, 1, 1, 1, 1) 
      
      --Pantalla (OPS - Dashboard)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2010000, 1, 1, 1, 1)  
           
      --Pantalla (OPS - Dashboard - Important)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2010100, 0, 0, 0, 0) 
           
      --Pantalla (OPS - Dashboard - Statistics)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2010200, 1, 1, 1, 1)  
           
      --Pantalla (OPS - Dashboard - Ocupattion)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2010201, 1, 1, 1, 1)  
      
      --Pantalla (OPS - Dashboard - Ocupattion by card)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2010202, 1, 1, 1, 1)  
           
      --Pantalla (OPS - Dashboard - VIP Customers)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2010203, 0, 0, 0, 0)  
           
      --Pantalla (OPS - Dashboard - Ocupattion Percentage by gender)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2010204, 0, 0, 0, 0)  

      --Pantalla (OPS - Dashboard - Ocupattion Percentage by age)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2010205, 0, 0, 0, 0)  

      --Pantalla (OPS - Dashboard - Evaluattion by age and gender)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2010206, 0, 0, 0, 0)  

      --Pantalla (OPS - Dashboard - Technics issues percentage)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2010207, 1, 1, 1, 1) 

      --Pantalla (OPS - Dashboard - Coin in, ANW, spins by game and manufacturer representation)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2010208, 1, 1, 1, 1)  

      --Pantalla (OPS - Dashboard - Gain Floor by machine and coin representation)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2010209, 1, 1, 1, 1)  

      --Pantalla (OPS - Dashboard - Coin in evaluation)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2010210, 1, 1, 1, 1)  

      --Pantalla (OPS - Floor)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2020000, 1, 1, 1, 1)  

      --Pantalla (OPS � Floor � Compare Terminals)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2020100, 1, 1, 1, 1) 

 		  --Pantalla (OPS � Floor � Temperature Map)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2020200, 1, 1, 1, 1) 

      --Pantalla (OPS � Floor � Visualization)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2020300, 1, 1, 1, 1) 

      --Pantalla (OPS - My Tasks)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2030000, 1, 1, 1, 1)  

      --Pantalla (OPS - Alarms - All)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2040000, 1, 1, 1, 1)  

      --Pantalla (OPS - Alarms - Players Only)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2040001, 1, 1, 1, 1)  

      --Pantalla (OPS - Alarms - Machines Only)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2040002, 1, 1, 1, 1)  
      
      --Pantalla (OPS - Charts)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2050000, 1, 1, 1, 1) 
           
      --Pantalla (OPS - Players)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2060000, 0, 0, 0, 0) 

      --Pantalla (OPS - Players - List Mode)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2060001, 0, 0, 0, 0) 

      --Pantalla (OPS - Players - Find)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2060100, 0, 0, 0, 0) 
     
      --Pantalla (OPS - Players - Filter)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2060200, 0, 0, 0, 0) 

      --Pantalla (OPS - Machines)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2070000, 1, 1, 1, 1) 

      --Pantalla (OPS - Comparation Machines)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2070001, 1, 1, 1, 1) 
      
      
    END

    --********************************
    --**SLO - SLOT ATTENDANT *********
    --********************************
    SET @ProfileName = 'LAYOUT - SLOT ATTENDANT'     
    
    --Comprovaci� exist�ncia perfil
    IF NOT EXISTS (SELECT  GUP_PROFILE_ID FROM GUI_USER_PROFILES WHERE UPPER (GUP_NAME) LIKE @ProfileName ESCAPE '\' ) 
    BEGIN
    
      --Obtenci� de l�ultim ID Profile (tal i com ho fa el GUI �?�?�?)
      SET @ProfileId = (SELECT MAX(ISNULL(GUP_PROFILE_ID, 0)) + 1  FROM GUI_USER_PROFILES)
      
      --Insert del Perfil
      INSERT INTO GUI_USER_PROFILES (GUP_PROFILE_ID, GUP_NAME, GUP_MAX_USERS) VALUES (@ProfileId, @ProfileName, 0)
      
      
      --*SLO
      --Pantalla (SLO - LOGIN)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1, 1, 1, 1, 1) 
           
      --Pantalla (SLO - Dashboard)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4010000, 1, 1, 1, 1)  
           
      --Pantalla (SLO - Dashboard - Important)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4010100, 0, 0, 0, 0) 
           
      --Pantalla (SLO - Dashboard - Statistics)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4010200, 1, 1, 1, 1)  
           
      --Pantalla (SLO - Dashboard - Ocupattion)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4010201, 1, 1, 1, 1)  
      
      --Pantalla (SLO - Dashboard - Ocupattion by card)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4010202, 1, 1, 1, 1)  
           
      --Pantalla (SLO - Dashboard - VIP Customers)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4010203, 1, 1, 1, 1)  
           
      --Pantalla (SLO - Dashboard - Ocupattion Percentage by gender)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4010204, 0, 0, 0, 0)  

      --Pantalla (SLO - Dashboard - Ocupattion Percentage by age)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4010205, 0, 0, 0, 0)  

      --Pantalla (SLO - Dashboard - Evaluation by age and gender)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4010206, 0, 0, 0, 0)  

      --Pantalla (SLO - Dashboard - Technics issues percentage)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4010207, 0, 0, 0, 0) 

      --Pantalla (SLO - Dashboard - Coin in, ANW, spins by game and manufacturer representation)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4010208, 0, 0, 0, 0)  

      --Pantalla (SLO - Dashboard - Gain Floor by machine and coin representation)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4010209, 0, 0, 0, 0)  

      --Pantalla (SLO - Dashboard - Coin in evaluation)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4010210, 0, 0, 0, 0)  

      --Pantalla (SLO - Floor)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4020000, 1, 1, 1, 1)  

      --Pantalla (SLO � Floor � Compare Terminals)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4020100, 1, 1, 1, 1)  

      --Pantalla (SLO � Floor � Temperature Map)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4020200, 1, 1, 1, 1)  

      --Pantalla (SLO � Floor - Visualization)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4020300, 1, 1, 1, 1)  

      --Pantalla (SLO - My Tasks)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4030000, 1, 1, 1, 1)  

      --Pantalla (SLO - Alarms - All)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4040000, 1, 1, 1, 1)  

      --Pantalla (SLO - Alarms - Players Only)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4040001, 1, 1, 1, 1)  

      --Pantalla (SLO - Alarms - Machines Only)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4040002, 1, 1, 1, 1)  
      
      --Pantalla (SLO - Charts)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4050000, 0, 0, 0, 0) 
           
      --Pantalla (SLO - Players)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4060000, 1, 1, 1, 1) 

      --Pantalla (SLO - Players - List Mode)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4060001, 0, 0, 0, 0) 

      --Pantalla (SLO - Players - Find)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4060100, 1, 1, 1, 1) 

      --Pantalla (SLO - Players - Filter)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4060200, 1, 1, 1, 1) 

      --Pantalla (SLO - Machines)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4070000, 1, 1, 1, 1) 

      --Pantalla (SLO - Comparation Machines)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4070001, 0, 0, 0, 0) 
     
    END

    --********************************
    --**TCH - TECHNICAL SUPPORT ******
    --********************************
    SET @ProfileName = 'LAYOUT - TECHNICAL SUPPORT'     
    
    --Comprovaci� exist�ncia perfil
    IF NOT EXISTS (SELECT  GUP_PROFILE_ID FROM GUI_USER_PROFILES WHERE UPPER (GUP_NAME) LIKE @ProfileName ESCAPE '\' ) 
    BEGIN
    
      --Obtenci� de l�ultim ID Profile (tal i com ho fa el GUI �?�?�?)
      SET @ProfileId = (SELECT MAX(ISNULL(GUP_PROFILE_ID, 0)) + 1  FROM GUI_USER_PROFILES)
      
      --Insert del Perfil
      INSERT INTO GUI_USER_PROFILES (GUP_PROFILE_ID, GUP_NAME, GUP_MAX_USERS) VALUES (@ProfileId, @ProfileName, 0)
      
      --Assignar Permisos de cada pantalla al perfil creat
    
      --*TCH
      --Pantalla (TCH - LOGIN)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1, 1, 1, 1, 1) 
           
      --Pantalla (TCH - Dashboard)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8010000, 1, 1, 1, 1)  
           
      --Pantalla (TCH - Dashboard - Important)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8010100, 0, 0, 0, 0) 
           
      --Pantalla (TCH - Dashboard - Statistics)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8010200, 1, 1, 1, 1)  
           
      --Pantalla (TCH - Dashboard - Ocupattion)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8010201, 1, 1, 1, 1)  
      
      --Pantalla (TCH - Dashboard - Ocupattion by card)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8010202, 1, 1, 1, 1)  
           
      --Pantalla (TCH - Dashboard - VIP Customers)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8010203, 1, 1, 1, 1)  
           
      --Pantalla (TCH - Dashboard - Ocupattion Percentage by gender)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8010204, 1, 1, 1, 1)  

      --Pantalla (TCH - Dashboard - Ocupattion Percentage by age)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8010205, 1, 1, 1, 1)  

      --Pantalla (TCH - Dashboard - Evaluation by age and gender)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8010206, 1, 1, 1, 1)  

      --Pantalla (TCH - Dashboard - Technics issues percentage)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1310207, 1, 1, 1, 1) 

      --Pantalla (TCH - Dashboard - Coin in, ANW, spins by game and manufacturer representation)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8010208, 1, 1, 1, 1)  

      --Pantalla (TCH - Dashboard - Gain Floor by machine and coin representation)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8010209, 1, 1, 1, 1)  

      --Pantalla (TCH - Dashboard - Coin in evaluation)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8010210, 1, 1, 1, 1)  

      --Pantalla (TCH - Floor)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8020000, 1, 1, 1, 1)  

      --Pantalla (TCH � Floor � Compare Terminals)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8020100, 1, 1, 1, 1)  

      --Pantalla (TCH � Floor � Temperature Map)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8020200, 1, 1, 1, 1)  

      --Pantalla (TCH � Floor - Visualization)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8020300, 1, 1, 1, 1)  

      --Pantalla (TCH - My Tasks)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8030000, 1, 1, 1, 1)  

      --Pantalla (TCH - Alarms - All)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8040000, 1, 1, 1, 1)  
    
      --Pantalla (TCH - Alarms - Players Only)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8040001, 1, 1, 1, 1)  

      --Pantalla (TCH - Alarms - Machines Only)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8040002, 1, 1, 1, 1)  
      
      --Pantalla (TCH - Charts)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8050000, 0, 0, 0, 0) 
           
      --Pantalla (TCH - Players)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8060000, 0, 0, 0, 0) 
      
      --Pantalla (TCH - Players - List Mode)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8060001, 0, 0, 0, 0) 

      --Pantalla (TCH - Players - Find)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8060100, 0, 0, 0, 0) 

      --Pantalla (TCH - Players - Filter)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8060200, 0, 0, 0, 0) 

      --Pantalla (TCH - Machines)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8070000, 1, 1, 1, 1) 

      --Pantalla (TCH - Comparation Machines)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8070001, 0, 0, 0, 0) 
      
    END
    
    
    --********************************
    --**ADM - ADMINISTRATOR **********
    --********************************
    SET @ProfileName = 'LAYOUT - ADMINISTRATOR'     
    
    --Comprovaci� exist�ncia perfil
    IF NOT EXISTS (SELECT  GUP_PROFILE_ID FROM GUI_USER_PROFILES WHERE UPPER (GUP_NAME) LIKE @ProfileName ESCAPE '\' ) 
    BEGIN
    
      --Obtenci� de l�ultim ID Profile (tal i com ho fa el GUI �?�?�?)
      SET @ProfileId = (SELECT MAX(ISNULL(GUP_PROFILE_ID, 0)) + 1  FROM GUI_USER_PROFILES)
      
      --Insert del Perfil
      INSERT INTO GUI_USER_PROFILES (GUP_PROFILE_ID, GUP_NAME, GUP_MAX_USERS) VALUES (@ProfileId, @ProfileName, 0)
      
      --Assignar Permisos de cada pantalla al perfil creat
      
      --*ADM
      --Pantalla (ADM - LOGIN)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1, 1, 1, 1, 1) 
      
      --Pantalla (ADM - Configuration)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 16010000, 0, 0, 0, 0) 

      --Pantalla (ADM - Floor - Edit)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 16020000, 0, 0, 0, 0) 

      --Pantalla (ADM - Caption - Edit)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 16030000, 0, 0, 0, 0) 
      
    END
  END
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetAlarmsAndTasks]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_GetAlarmsAndTasks]
GO
CREATE PROCEDURE [dbo].[Layout_GetAlarmsAndTasks] 
AS
BEGIN
  DECLARE @DateFrom as DATETIME,
          @DateTo as DATETIME,
          @MaxElapsedTime as INT

  SET @DateFrom = dbo.TodayOpening(0)
  SET @DateTo = DATEADD (DAY, 1, @DateFrom)
  SET @MaxElapsedTime = 48

  SELECT   --TOP 5
           LSA_TERMINAL_ID AS TERMINAL_ID
         , LSA_ALARM_ID AS ALARM_ID
         , LSA_DATE_CREATED AS DATE_CREATION
         , CASE WHEN (DATEDIFF(second,LSA_DATE_CREATED,GETDATE()) / 3600) < @MaxElapsedTime THEN
             RIGHT('0' + CAST(DATEDIFF(second,LSA_DATE_CREATED,GETDATE()) / 3600 AS VARCHAR),2) + ':' +
             RIGHT('0' + CAST((DATEDIFF(second,LSA_DATE_CREATED,GETDATE()) / 60) % 60 AS VARCHAR),2)  + ':' +
             RIGHT('0' + CAST(DATEDIFF(second,LSA_DATE_CREATED,GETDATE()) % 60 AS VARCHAR),2)
           ELSE ' ' END
         , LSA_ID AS ID
         , LSA_ALARM_TYPE AS ALARM_TYPE
         , LSA_USER_CREATED AS USER_CREATION
         , LSA_MEDIA_ID
         , CAST('' as xml).value('xs:base64Binary(sql:column("LM_DATA_THUMBNAIL"))','varchar(max)') as LST_ATTACHED_MEDIA
         , LSA_DESCRIPTION AS AL_DESCRIPTION
         , LSA_ALARM_SOURCE AS ALARM_SOURCE
         , LSA_STATUS
         , TE_NAME
         , LSA_TITLE
       --, LSA_DATE_TO_TASK AS 
       --, LSA_TASK_ID
    FROM   LAYOUT_SITE_ALARMS
    LEFT   JOIN TERMINALS ON LSA_TERMINAL_ID=TE_TERMINAL_ID
    LEFT   JOIN LAYOUT_MEDIA ON LAYOUT_SITE_ALARMS.LSA_MEDIA_ID=LAYOUT_MEDIA.LM_ID
   WHERE   LSA_STATUS IN (0,2)
     AND   LSA_TASK_ID IS NULL
     AND   LSA_DATE_TO_TASK IS NULL
     AND   (LSA_DATE_CREATED>= dbo.TodayOpening(0) OR LSA_STATUS=2)

  SELECT   LST_ID
         , LST_STATUS
         , LST_START
         , LST_END
         , LST_CATEGORY
         , LST_SUBCATEGORY
         , LST_TERMINAL_ID
         , LST_ACCOUNT_ID
         , LST_DESCRIPTION
         , LST_SEVERITY
         , LST_CREATION_USER_ID
         , LST_CREATION
         , LST_ASSIGNED_ROLE_ID
         , LST_ASSIGNED_USER_ID
         , LST_ASSIGNED
         , LST_ACCEPTED_USER_ID
         , LST_ACCEPTED
         , LST_SCALE_FROM_USER_ID
         , LST_SCALE_TO_USER_ID
         , LST_SCALE_REASON
         , LST_SCALE
         , LST_SOLVED_USER_ID
         , LST_SOLVED
         , LST_VALIDATE_USER_ID
         , LST_VALIDATE
         , LST_LAST_STATUS_UPDATE_USER_ID
         , LST_LAST_STATUS_UPDATE
         , LM_ID
       --, '' as LST_ATTACHED_MEDIA
         , CAST('' as xml).value('xs:base64Binary(sql:column("LM_DATA_THUMBNAIL"))','varchar(max)') as LST_ATTACHED_MEDIA  
         , CASE WHEN LST_STATUS = 5 THEN --CONVERT(varchar, DATEADD(MS, DATEDIFF(second,LST_CREATION,LST_VALIDATE)* 1000, 0),114)
            CASE WHEN (DATEDIFF(second,LST_CREATION,LST_VALIDATE) / 3600)<@MaxElapsedTime THEN
              RIGHT('0' + CAST(DATEDIFF(second,LST_CREATION,LST_VALIDATE) / 3600 AS VARCHAR),2) + ':' +
              RIGHT('0' + CAST((DATEDIFF(second,LST_CREATION,LST_VALIDATE) / 60) % 60 AS VARCHAR),2)  + ':' +
              RIGHT('0' + CAST(DATEDIFF(second,LST_CREATION,LST_VALIDATE) % 60 AS VARCHAR),2)
            ELSE ' ' END
           WHEN lst_status = 3 THEN --CONVERT(varchar, DATEADD(MS, DATEDIFF(second,LST_CREATION,LST_SOLVED)* 1000, 0),114)--
             CASE WHEN (DATEDIFF(second,LST_CREATION,LST_SOLVED) / 3600)<@MaxElapsedTime THEN       
               RIGHT('0' + CAST(DATEDIFF(second,LST_CREATION,LST_SOLVED) / 3600 AS VARCHAR),2) + ':' +
               RIGHT('0' + CAST((DATEDIFF(second,LST_CREATION,LST_SOLVED) / 60) % 60 AS VARCHAR),2)  + ':' +
               RIGHT('0' + CAST(DATEDIFF(second,LST_CREATION,LST_SOLVED) % 60 AS VARCHAR),2)
              ELSE ' ' END
           ELSE --CONVERT(varchar, DATEADD(MS, DATEDIFF(second,LST_CREATION,GETDATE())* 1000, 0),114)
             CASE WHEN (DATEDIFF(second,LST_CREATION,GETDATE()) / 3600)<@MaxElapsedTime THEN       
               RIGHT('0' + CAST(DATEDIFF(second,LST_CREATION,GETDATE()) / 3600 AS VARCHAR),2) + ':' +
               RIGHT('0' + CAST((DATEDIFF(second,LST_CREATION,GETDATE()) / 60) % 60 AS VARCHAR),2)  + ':' +
               RIGHT('0' + CAST(DATEDIFF(second,LST_CREATION,GETDATE()) % 60 AS VARCHAR),2)
             ELSE ' ' END
           END 
         , LST_EVENTS_HISTORY
         , CASE WHEN GU_USERNAME IS NULL THEN
            CASE WHEN (LST_ASSIGNED_ROLE_ID & 1 = 1)  THEN  'PCA'
                 WHEN (LST_ASSIGNED_ROLE_ID & 2 = 2)  THEN  'OPM'
                 WHEN (LST_ASSIGNED_ROLE_ID & 4 = 4)  THEN  'SLA'
                 WHEN (LST_ASSIGNED_ROLE_ID & 8 = 8)  THEN  'TSP'
            END
           ELSE GU_USERNAME END 
         , LST_TITLE
    FROM   LAYOUT_SITE_TASKS
    LEFT   JOIN LAYOUT_MEDIA ON LAYOUT_SITE_TASKS.LST_ATTACHED_MEDIA=LAYOUT_MEDIA.LM_ID
    LEFT   JOIN GUI_USERS ON GU_USER_ID=LST_ASSIGNED_USER_ID
   WHERE   (LST_CREATION >= dbo.TodayOpening(0) OR LST_STATUS IN (2,1))
 --WHERE LST_STATUS NOT IN (4)
 --WHERE @DateFrom <= LST_CREATION 
 --  AND @DateTo > LST_CREATION 
  
  SELECT   GU_USER_ID,GU_USERNAME 
    FROM   GUI_USERS
   INNER   JOIN LAYOUT_USERS_CONFIGURATION ON GU_USER_ID=LC_USER_ID
   WHERE   GU_PROFILE_ID IN (SELECT   GPF_PROFILE_ID 
                               FROM   GUI_PROFILE_FORMS
                              WHERE   GPF_GUI_ID = 203
                              GROUP   BY GPF_PROFILE_ID) 
     AND   LC_IS_RUNNER = 1 
   ORDER   BY GU_USERNAME

  SELECT   RS.LST_TERMINAL_ID
         , RS.LST_CATEGORY
         , RS.LST_SUBCATEGORY
         , RS.LST_SEVERITY
         , RS.LST_ASSIGNED_ROLE_ID
         , RS.LST_ASSIGNED_USER_ID
         , RS.LST_CREATION
         , RS.LST_STATUS
    FROM   (SELECT   *
                   , Rank() 
              OVER (PARTITION BY LST_TERMINAL_ID ORDER BY LST_TERMINAL_ID,LST_CREATION DESC ) AS Rank
              FROM LAYOUT_SITE_TASKS ) rs 
   WHERE   Rank <= 10
  END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetAssignedTaskToUser]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_GetAssignedTaskToUser]
GO
CREATE PROCEDURE [dbo].[Layout_GetAssignedTaskToUser]
@loginId bigint
WITH EXEC AS CALLER
AS
BEGIN
  DECLARE @terminal_name_type AS VARCHAR(50),
          @big_won_limit AS MONEY,
          @Date AS DATETIME,
          @IntervalHour INT,
          @Now AS DATETIME,
          @CurrentHour AS BIGINT,
          @HourNow AS DATETIME,
          @NextHourNow AS DATETIME

  SET @Date = GETDATE()
  SET @IntervalHour = 1
  SET @big_won_limit = 1000
  SET @Now = CAST(DATEPART(YEAR, @Date) AS VARCHAR) + '-' +  CAST(DATEPART(MONTH, @Date) AS VARCHAR)  + '-' +  CAST(DATEPART(DAY, @Date) AS VARCHAR)  + ' ' +  CAST(DATEPART(HOUR, @Date) AS VARCHAR) + ':' + CAST(DATEPART(MINUTE, GETDATE()) AS VARCHAR) + ':' + CAST(DATEPART(SECOND, GETDATE()) AS VARCHAR) + '.000'
  SET @CurrentHour = DATEPART(HOUR, @Now)
  SET @HourNow = DATEADD(HOUR, DATEDIFF(HOUR, 0, @Now), 0)
  SET @NextHourNow = DATEADD(HOUR, @IntervalHour, @HourNow)
       
  SELECT   LST.LST_ID
         , LST.LST_STATUS
         , LST.LST_START
         , LST.LST_END
         , LST.LST_CATEGORY
         , LST.LST_SUBCATEGORY
         , LST.LST_TERMINAL_ID
         , LST.LST_ACCOUNT_ID
         , LST.LST_DESCRIPTION
         , LST.LST_SEVERITY
         , LST.LST_CREATION
         , LST.LST_ASSIGNED_ROLE_ID
         , LST.LST_ASSIGNED_USER_ID
         , TE.TE_NAME
         , TE.TE_PROVIDER_ID
         , TE.TE_MODEL
         , A.AR_NAME
         , BK.BK_NAME
         , PLAY_SESSION_STATUS = CASE  WHEN (PS_PLAY_SESSION_ID IS NOT NULL) THEN 2 ELSE 1 END
         , IS_ANONYMOUS = CASE  WHEN (PS.PS_ACCOUNT_ID IS NOT NULL) THEN CASE WHEN (AC.AC_HOLDER_NAME IS NOT NULL) THEN 0 ELSE 1 END ELSE NULL END
         , PS.PS_ACCOUNT_ID AS PLAYER_ACCOUNT_ID
         , PLAYER_NAME = CASE WHEN (PS.PS_ACCOUNT_ID IS NOT NULL) THEN ISNULL(AC.AC_HOLDER_NAME, 'ANONYMOUS') ELSE NULL END
         , ISNULL(AC.AC_HOLDER_GENDER, 0) AS PLAYER_GENDER
         , DATEDIFF(YEAR, AC.AC_HOLDER_BIRTH_DATE, @NOW) AS PLAYER_AGE
         , PLAYER_LEVEL = AC.AC_HOLDER_LEVEL
         , ISNULL(AC.AC_HOLDER_IS_VIP, 0) AS PLAYER_IS_VIP
         , LST.LST_TITLE
    FROM   LAYOUT_SITE_TASKS AS LST
   INNER   JOIN TERMINALS AS TE ON LST.LST_TERMINAL_ID = TE.TE_TERMINAL_ID
   INNER   JOIN PROVIDERS AS PR ON PR.PV_ID = TE.TE_PROV_ID
    LEFT   JOIN BANKS AS BK ON BK.BK_BANK_ID = TE.TE_BANK_ID
   INNER   JOIN AREAS a ON a.AR_AREA_ID = bk.BK_AREA_ID 
    LEFT   JOIN (SELECT   PS_TERMINAL_ID
                        , PS_PLAY_SESSION_ID
                        , PS_ACCOUNT_ID
                        , PS_TOTAL_PLAYED
                        , PS_WON_AMOUNT
                        , PS_PLAYED_COUNT
                        , PS_STARTED
                        , DBO.GETRATE(PS_STARTED,PS_FINISHED, @NOW) AS PS_RATE
                   FROM   PLAY_SESSIONS
                  WHERE ( ps_started <= @Now AND ps_finished > @Now )
                    AND   ps_played_count>0) AS PS ON TE.TE_TERMINAL_ID = PS.PS_TERMINAL_ID --AND PS_STARTED >= @NOW
    LEFT   JOIN ACCOUNTS AS AC ON AC.AC_ACCOUNT_ID = PS.PS_ACCOUNT_ID
    LEFT   JOIN TERMINAL_STATUS AS TS ON TE.TE_TERMINAL_ID = TS.TS_TERMINAL_ID
    LEFT   JOIN (SELECT   SPH_TERMINAL_ID 
                        , SUM(SPH_PLAYED_AMOUNT)*DBO.GETRATE(DATEADD(DAY, DATEDIFF(DAY, 0, @NOW), 0) ,DATEADD(DAY, DATEDIFF(DAY, 0, @NOW), 1), @NOW)  AS PLAYED_AMOUNT
                        , SUM(SPH_WON_AMOUNT)*DBO.GETRATE(DATEADD(DAY, DATEDIFF(DAY, 0, @NOW), 0) ,DATEADD(DAY, DATEDIFF(DAY, 0, @NOW), 1), @NOW)     AS WON_AMOUNT
                        , SUM(SPH_PLAYED_COUNT)*DBO.GETRATE(DATEADD(DAY, DATEDIFF(DAY, 0, @NOW), 0) ,DATEADD(DAY, DATEDIFF(DAY, 0, @NOW), 1), @NOW)   AS PLAYED_COUNT
                   FROM   SALES_PER_HOUR
                  WHERE   SPH_BASE_HOUR >= DATEADD(DAY, DATEDIFF(DAY, 0, @NOW), 0) 
                    AND   SPH_BASE_HOUR <= DATEADD(DAY, DATEDIFF(DAY, 0, @NOW), 1) 
                  GROUP   BY SPH_TERMINAL_ID) AS SPH ON SPH.SPH_TERMINAL_ID = TE.TE_TERMINAL_ID
    LEFT   JOIN (SELECT   CAST(REPLACE(REPLACE(GP_SUBJECT_KEY,'LEVEL',''),'.NAME','') AS INT) AS NUM
                        , GP_KEY_VALUE AS NAME
                   FROM   GENERAL_PARAMS 
                  WHERE   GP_GROUP_KEY ='PLAYERTRACKING'
                    AND   GP_SUBJECT_KEY LIKE 'LEVEL%.NAME') AS LEVELS ON LEVELS.NUM = AC.AC_HOLDER_LEVEL 
   WHERE   LST_STATUS IN (1,2)
     AND   ((LST_ASSIGNED_USER_ID = @loginId) OR (LST_ACCEPTED_USER_ID = @loginId)
      OR   ((LST_ASSIGNED_ROLE_ID IN (SELECT   DISTINCT(GPF_FORM_ID / 1000000) AS ROLE
                                        FROM   GUI_PROFILE_FORMS
                                       WHERE   GPF_GUI_ID = 203 AND GPF_READ_PERM <> 0
                                         AND   GPF_PROFILE_ID = (SELECT   GU_PROFILE_ID
                                                                   FROM   GUI_USERS
                                                                  WHERE   GU_USER_ID = @loginId)
                                         AND   (GPF_FORM_ID / 1000000) > 0))  AND ( isnull(lst_accepted_user_id, 0) = 0)))
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetAssignedTaskToUser_Test]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_GetAssignedTaskToUser_Test]
GO
CREATE PROCEDURE [dbo].[Layout_GetAssignedTaskToUser_Test]
  @loginId bigint
WITH EXEC AS CALLER
AS
BEGIN
  DECLARE @terminal_name_type AS VARCHAR(50),
          @Date AS DATETIME,
          @IntervalHour INT, 
          @big_won_limit AS MONEY,
          @Now AS DATETIME,
          @CurrentHour AS BIGINT,
          @HourNow AS DATETIME,
          @NextHourNow AS DATETIME
       
  SET @Date = '08/01/2015'
  SET @IntervalHour = 1
  SET @big_won_limit = 1000

  SET @Now = CAST(DATEPART(YEAR, @Date) AS VARCHAR) + '-' +  CAST(DATEPART(MONTH, @Date) AS VARCHAR)  + '-' +  CAST(DATEPART(DAY, @Date) AS VARCHAR)  + ' ' +  CAST(DATEPART(HOUR, @Date) AS VARCHAR) + ':' + CAST(DATEPART(MINUTE, GETDATE()) AS VARCHAR) + ':' + CAST(DATEPART(SECOND, GETDATE()) AS VARCHAR) + '.000'
         
  SET @CurrentHour = DATEPART(HOUR, @Now)
  SET @HourNow = DATEADD(HOUR, DATEDIFF(HOUR, 0, @Now), 0)
  SET @NextHourNow = DATEADD(HOUR, @IntervalHour, @HourNow)
       
  SELECT   LST.LST_ID
         , LST.LST_STATUS
         , LST.LST_START
         , LST.LST_END
         , LST.LST_CATEGORY
         , LST.LST_SUBCATEGORY
         , LST.LST_TERMINAL_ID
         , LST.LST_ACCOUNT_ID
         , LST.LST_DESCRIPTION
         , LST.LST_SEVERITY
         , LST.LST_CREATION
         , LST.LST_ASSIGNED_ROLE_ID
         , LST.LST_ASSIGNED_USER_ID
         , TE.TE_NAME
         , TE.TE_PROVIDER_ID
       --, TE.TE_MODEL
         , A.AR_NAME
         , BK.BK_NAME
         , PLAY_SESSION_STATUS = CASE  WHEN (PS_PLAY_SESSION_ID IS NOT NULL) THEN 2 ELSE 1 END
         , IS_ANONYMOUS = CASE  WHEN (PS.PS_ACCOUNT_ID IS NOT NULL) THEN CASE WHEN (AC.AC_HOLDER_NAME IS NOT NULL) THEN 0 ELSE 1 END ELSE NULL END
         , PS.PS_ACCOUNT_ID AS PLAYER_ACCOUNT_ID
         , PLAYER_NAME = CASE WHEN (PS.PS_ACCOUNT_ID IS NOT NULL) THEN ISNULL(AC.AC_HOLDER_NAME, 'ANONYMOUS') ELSE NULL END
         , ISNULL(AC.AC_HOLDER_GENDER, 0) AS PLAYER_GENDER
         , DATEDIFF(YEAR, AC.AC_HOLDER_BIRTH_DATE, @NOW) AS PLAYER_AGE
         , PLAYER_LEVEL = AC.AC_HOLDER_LEVEL
         , ISNULL(AC.AC_HOLDER_IS_VIP, 0) AS PLAYER_IS_VIP
         , LST.LST_TITLE
    FROM   LAYOUT_SITE_TASKS AS LST
   INNER   JOIN TERMINALS_TEST AS TE ON LST.LST_TERMINAL_ID = TE.TE_TERMINAL_ID
   INNER   JOIN PROVIDERS_TEST AS PR ON PR.PV_ID            = TE.TE_PROV_ID
    LEFT   JOIN BANKS_TEST     AS BK ON BK.BK_BANK_ID       = TE.TE_BANK_ID
   INNER   JOIN AREAS a ON a.AR_AREA_ID = bk.BK_AREA_ID 
    LEFT   JOIN (SELECT   PS_TERMINAL_ID
                        , PS_PLAY_SESSION_ID
                        , PS_ACCOUNT_ID
                        , PS_TOTAL_PLAYED
                        , PS_WON_AMOUNT
                        , PS_PLAYED_COUNT
                        , PS_STARTED
                        , DBO.GETRATE(PS_STARTED,PS_FINISHED, @NOW) AS PS_RATE
                   FROM   play_sessions_TEST 
                  WHERE ( ps_started <= @Now AND ps_finished > @Now )
                    AND   ps_played_count>0) AS PS ON TE.TE_TERMINAL_ID = PS.PS_TERMINAL_ID --AND PS_STARTED >= @NOW
    LEFT   JOIN ACCOUNTS_TEST   AS AC ON AC.AC_ACCOUNT_ID = PS.PS_ACCOUNT_ID
    LEFT   JOIN TERMINAL_STATUS AS TS ON TE.TE_TERMINAL_ID = TS.TS_TERMINAL_ID
    LEFT   JOIN (SELECT   SPH_TERMINAL_ID 
                        , SUM(SPH_PLAYED_AMOUNT)*DBO.GETRATE(DATEADD(DAY, DATEDIFF(DAY, 0, @NOW), 0) ,DATEADD(DAY, DATEDIFF(DAY, 0, @NOW), 1), @NOW)  AS PLAYED_AMOUNT
                        , SUM(SPH_WON_AMOUNT)*DBO.GETRATE(DATEADD(DAY, DATEDIFF(DAY, 0, @NOW), 0) ,DATEADD(DAY, DATEDIFF(DAY, 0, @NOW), 1), @NOW)    AS WON_AMOUNT
                        , SUM(SPH_PLAYED_COUNT)*DBO.GETRATE(DATEADD(DAY, DATEDIFF(DAY, 0, @NOW), 0) ,DATEADD(DAY, DATEDIFF(DAY, 0, @NOW), 1), @NOW)  AS PLAYED_COUNT
                   FROM   SALES_PER_HOUR_TEST
                  WHERE   SPH_BASE_HOUR >= DATEADD(DAY, DATEDIFF(DAY, 0, @NOW), 0) 
                    AND   SPH_BASE_HOUR <= DATEADD(DAY, DATEDIFF(DAY, 0, @NOW), 1) 
                  GROUP   BY SPH_TERMINAL_ID) AS SPH ON SPH.SPH_TERMINAL_ID = TE.TE_TERMINAL_ID
    LEFT   JOIN (SELECT   CAST(REPLACE(REPLACE(GP_SUBJECT_KEY,'LEVEL',''),'.NAME','') AS INT) AS NUM
                        , GP_KEY_VALUE AS NAME
                   FROM   GENERAL_PARAMS 
                  WHERE   GP_GROUP_KEY ='PLAYERTRACKING'
                    AND   GP_SUBJECT_KEY LIKE 'LEVEL%.NAME') AS LEVELS ON LEVELS.NUM = AC.AC_HOLDER_LEVEL 
    WHERE   lst_status IN (1,2)
      AND   ((lst_assigned_user_id = @loginId)   OR   (lst_accepted_user_id = @loginId)  
       OR   ((lst_assigned_role_id IN (SELECT   DISTINCT(GPF_FORM_ID / 1000000) AS ROLE
                                         FROM   GUI_PROFILE_FORMS
                                        WHERE   GPF_GUI_ID = 203 AND GPF_READ_PERM <> 0
                                          AND   GPF_PROFILE_ID = (SELECT   GU_PROFILE_ID
                                                                    FROM   GUI_USERS
                                                                   WHERE   GU_USER_ID = @loginId)
      AND   (GPF_FORM_ID / 1000000) > 0))  AND ( isnull(lst_accepted_user_id, 0) = 0)))
END
GO

IF OBJECT_ID('Layout_GetTerminalIdFromBankId', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[Layout_GetTerminalIdFromBankId]
GO
CREATE PROCEDURE [dbo].[Layout_GetTerminalIdFromBankId] 
  @bankId BigInt,
  @count_terminal Int OUT
AS
BEGIN
  SELECT @count_terminal=TE_TERMINAL_ID FROM terminals WHERE te_bank_id = @bankId
END
GO

IF OBJECT_ID('Layout_GetTerminalIdFromBaseName', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[Layout_GetTerminalIdFromBaseName]
GO
CREATE PROCEDURE [dbo].[Layout_GetTerminalIdFromBaseName] 
  @baseName NVARCHAR(50),
  @count_terminal Int OUT
AS
BEGIN
  SELECT @count_terminal=TE_TERMINAL_ID FROM terminals WHERE te_base_name = @baseName
END
GO

IF OBJECT_ID('Layout_GetTerminalIdFromFloorId', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[Layout_GetTerminalIdFromFloorId]
GO
CREATE PROCEDURE [dbo].[Layout_GetTerminalIdFromFloorId] 
  @floorId nvarchar(20),
  @count_terminal Int OUT
AS
BEGIN
  SELECT @count_terminal=TE_TERMINAL_ID FROM terminals WHERE te_floor_id = @floorId
END
GO

IF OBJECT_ID('Layout_GetTerminalIdFromPosition', 'P') IS NOT NULL
  DROP PROCEDURE [dbo].[Layout_GetTerminalIdFromPosition]
GO
CREATE PROCEDURE [dbo].[Layout_GetTerminalIdFromPosition] 
  @position BigInt,
  @count_terminal Int OUT
AS
BEGIN
  SELECT @count_terminal=TE_TERMINAL_ID FROM terminals WHERE te_position = @position
END
GO

----------------------------------------------------
-------------------ROLE DASHBOARD-------------------
----------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[layout_dashboards]') AND type in (N'U'))
DROP TABLE [dbo].[layout_dashboards];

CREATE TABLE [dbo].[layout_dashboards](
	[ld_role_id] [int] NOT NULL,
	[ld_dashboard] [varchar](max) NULL,
 CONSTRAINT [PK_layout_dashboards] PRIMARY KEY CLUSTERED 
(
	[ld_role_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetDashboardRoles]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_GetDashboardRoles]
GO
CREATE PROCEDURE [dbo].[Layout_GetDashboardRoles]

AS
BEGIN

DECLARE @ALLDASHBOARD NVARCHAR(MAX)

SELECT @ALLDASHBOARD = COALESCE(@ALLDASHBOARD + ', ', '') + '{'+ CAST([LD_ROLE_ID] AS VARCHAR(2)) +':' + [LD_DASHBOARD] +'}' 
  FROM  [LAYOUT_DASHBOARDS]
 
SELECT '[' + @ALLDASHBOARD +']';

END -- Layout_GetDashboardRoles

----------------------------------------------------
----------------------GRANTING----------------------
----------------------------------------------------

GRANT EXECUTE ON OBJECT::dbo.CreateDefaultProfilesLayout TO wggui;

GRANT EXECUTE ON OBJECT::dbo.Layout_SaveUser TO wggui;

----------------------------------------------------
----------------------VERSION-----------------------
----------------------------------------------------

DECLARE 
@New_SmartFloor_ReleaseId int,
@New_SmartFloor_ScriptName nvarchar(50),
@New_SmartFloor_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @New_SmartFloor_ReleaseId = 8;
SET @New_SmartFloor_ScriptName = N'UpdateTo_SmartFloor_18.008.sql';
SET @New_SmartFloor_Description = N'Creation and initialization.';

/*** DB Version ***/
INSERT INTO [dbo].[db_version_smartfloor]
           ([db_client_id]
           ,[db_common_build_id]
           ,[db_client_build_id]
           ,[db_release_id]
           ,[db_updated_script]
           ,[db_updated]
           ,[db_description])
     VALUES
           (18
           ,100
           ,1
           ,@New_SmartFloor_ReleaseId
           ,@New_SmartFloor_ScriptName
           ,Getdate()
           ,@New_SmartFloor_Description);
GO

