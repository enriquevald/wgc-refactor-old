USE WGDB_000;

SET ANSI_NULLS ON;

----------------------------------------------------
-----------------VERSION CONTROL--------------------
----------------------------------------------------

DECLARE 
@Exp_SmartFloor_ClientId int, 
@Exp_SmartFloor_CommonBuildId int,
@Exp_SmartFloor_ClientBuildId int,
@New_SmartFloor_ReleaseId int,
@New_SmartFloor_ScriptName nvarchar(50),
@New_SmartFloor_Description nvarchar(1000);

SET @Exp_SmartFloor_ClientId = 18;
SET @Exp_SmartFloor_CommonBuildId = 100;
SET @Exp_SmartFloor_ClientBuildId = 1;
SET @New_SmartFloor_ReleaseId = 9;
SET @New_SmartFloor_ScriptName = N'UpdateTo_SmartFloor_18.009.sql';
SET @New_SmartFloor_Description = N'Patch 18.008 - 9';

IF EXISTS ( SELECT 1 FROM DB_VERSION_SMARTFLOOR )
 DELETE FROM DB_VERSION_SMARTFLOOR;

INSERT INTO [dbo].[db_version_smartfloor]
           ([db_client_id]
           ,[db_common_build_id]
           ,[db_client_build_id]
           ,[db_release_id]
           ,[db_updated_script]
           ,[db_updated]
           ,[db_description])
     VALUES
           (@Exp_SmartFloor_ClientId
           ,@Exp_SmartFloor_CommonBuildId
           ,@Exp_SmartFloor_ClientBuildId
           ,@New_SmartFloor_ReleaseId
           ,@New_SmartFloor_ScriptName
           ,Getdate()
           ,@New_SmartFloor_Description);

           
----------------------------------------------------
------------------------DATA------------------------
----------------------------------------------------

INSERT [dbo].[layout_dashboards] ([ld_role_id], [ld_dashboard]) VALUES (1, N'[null,{"id":"widgetTestList1","x":2,"y":8,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgetTestList2","x":2,"y":0,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgettheater2","x":3,"y":16,"width":1,"height":6,"type":"WidgetSVGAmphitheater","canResize":false,"canMove":true},{"id":"widgettheater3","x":3,"y":22,"width":1,"height":6,"type":"WidgetSVGAmphitheater","canResize":false,"canMove":true},{"id":"widgetOccupancy1","x":3,"y":28,"width":1,"height":6,"type":"WidgetSVGAmphitheater","canResize":false,"canMove":true},{"id":"widgetTasks1","x":3,"y":0,"width":1,"height":6,"type":"WidgetBase","canResize":false,"canMove":true},{"id":"widgetChart1","x":2,"y":19,"width":1,"height":6,"type":"WidgetHighChart","canResize":false,"canMove":true},{"id":"widgetChart2","x":2,"y":25,"width":1,"height":6,"type":"WidgetSVGChart","canResize":false,"canMove":true},{"id":"widgetDonut1","x":0,"y":16,"width":1,"height":6,"type":"WidgetSVGDonutChart","canResize":false,"canMove":true},{"id":"widgetVipsList","x":0,"y":0,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgetIndicator3","x":2,"y":16,"width":1,"height":6,"type":"WidgetBase","canResize":false,"canMove":true},{"id":"widgetMachineOccupation","x":1,"y":8,"width":1,"height":6,"type":"WidgetSVGDonutChart","canResize":false,"canMove":true},{"id":"widgetHRList","x":1,"y":14,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgetAgeSegmentation","x":1,"y":22,"width":1,"height":6,"type":"WidgetSVGDonutChart","canResize":false,"canMove":true},{"id":"widgetLevelSegmentation","x":0,"y":22,"width":1,"height":6,"type":"WidgetSVGDonutChart","canResize":false,"canMove":true},{"id":"widgetRunnersList","x":3,"y":8,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgetBirthday","x":1,"y":0,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgetPlayersMaxLevel","x":0,"y":6,"width":1,"height":8,"type":"WidgetListBase","canResize":false,"canMove":true}]');
INSERT [dbo].[layout_dashboards] ([ld_role_id], [ld_dashboard]) VALUES (2, N'[null,{"id":"widgetTestList1","x":2,"y":8,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgetBirthday","x":1,"y":0,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgetPlayersMaxLevel","x":0,"y":8,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true}]');
INSERT [dbo].[layout_dashboards] ([ld_role_id], [ld_dashboard]) VALUES (4, N'[null,{"id":"widgetTestList1","x":2,"y":8,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgetTestList2","x":2,"y":0,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgettheater2","x":3,"y":16,"width":1,"height":6,"type":"WidgetSVGAmphitheater","canResize":false,"canMove":true},{"id":"widgettheater3","x":3,"y":22,"width":1,"height":6,"type":"WidgetSVGAmphitheater","canResize":false,"canMove":true},{"id":"widgetOccupancy1","x":3,"y":28,"width":1,"height":6,"type":"WidgetSVGAmphitheater","canResize":false,"canMove":true},{"id":"widgetTasks1","x":3,"y":0,"width":1,"height":6,"type":"WidgetBase","canResize":false,"canMove":true},{"id":"widgetChart1","x":2,"y":19,"width":1,"height":6,"type":"WidgetHighChart","canResize":false,"canMove":true},{"id":"widgetChart2","x":2,"y":25,"width":1,"height":6,"type":"WidgetSVGChart","canResize":false,"canMove":true},{"id":"widgetDonut1","x":0,"y":16,"width":1,"height":6,"type":"WidgetSVGDonutChart","canResize":false,"canMove":true},{"id":"widgetVipsList","x":0,"y":0,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgetIndicator3","x":2,"y":16,"width":1,"height":6,"type":"WidgetBase","canResize":false,"canMove":true},{"id":"widgetMachineOccupation","x":1,"y":8,"width":1,"height":6,"type":"WidgetSVGDonutChart","canResize":false,"canMove":true},{"id":"widgetHRList","x":1,"y":14,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgetAgeSegmentation","x":1,"y":22,"width":1,"height":6,"type":"WidgetSVGDonutChart","canResize":false,"canMove":true},{"id":"widgetLevelSegmentation","x":0,"y":22,"width":1,"height":6,"type":"WidgetSVGDonutChart","canResize":false,"canMove":true},{"id":"widgetRunnersList","x":3,"y":8,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgetBirthday","x":1,"y":0,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgetPlayersMaxLevel","x":0,"y":6,"width":1,"height":8,"type":"WidgetListBase","canResize":false,"canMove":true}]');
INSERT [dbo].[layout_dashboards] ([ld_role_id], [ld_dashboard]) VALUES (8, N'[null,{"id":"widgetTestList1","x":2,"y":8,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgetTestList2","x":2,"y":0,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgettheater2","x":3,"y":16,"width":1,"height":6,"type":"WidgetSVGAmphitheater","canResize":false,"canMove":true},{"id":"widgettheater3","x":3,"y":22,"width":1,"height":6,"type":"WidgetSVGAmphitheater","canResize":false,"canMove":true},{"id":"widgetOccupancy1","x":3,"y":28,"width":1,"height":6,"type":"WidgetSVGAmphitheater","canResize":false,"canMove":true},{"id":"widgetTasks1","x":3,"y":0,"width":1,"height":6,"type":"WidgetBase","canResize":false,"canMove":true},{"id":"widgetChart1","x":2,"y":19,"width":1,"height":6,"type":"WidgetHighChart","canResize":false,"canMove":true},{"id":"widgetChart2","x":2,"y":25,"width":1,"height":6,"type":"WidgetSVGChart","canResize":false,"canMove":true},{"id":"widgetDonut1","x":0,"y":16,"width":1,"height":6,"type":"WidgetSVGDonutChart","canResize":false,"canMove":true},{"id":"widgetVipsList","x":0,"y":0,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgetIndicator3","x":2,"y":16,"width":1,"height":6,"type":"WidgetBase","canResize":false,"canMove":true},{"id":"widgetMachineOccupation","x":1,"y":8,"width":1,"height":6,"type":"WidgetSVGDonutChart","canResize":false,"canMove":true},{"id":"widgetHRList","x":1,"y":14,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgetAgeSegmentation","x":1,"y":22,"width":1,"height":6,"type":"WidgetSVGDonutChart","canResize":false,"canMove":true},{"id":"widgetLevelSegmentation","x":0,"y":22,"width":1,"height":6,"type":"WidgetSVGDonutChart","canResize":false,"canMove":true},{"id":"widgetRunnersList","x":3,"y":8,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgetBirthday","x":1,"y":0,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgetPlayersMaxLevel","x":0,"y":6,"width":1,"height":8,"type":"WidgetListBase","canResize":false,"canMove":true}]');


----------------------------------------------------
----------------------STOREDS-----------------------
----------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_SaveUser]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_SaveUser]
GO
CREATE PROCEDURE [dbo].[Layout_SaveUser]
      @pUserId INT
    , @pIsManager BIT
    , @pIsRunner BIT
AS
BEGIN
      IF EXISTS (SELECT TOP 1 * FROM LAYOUT_USERS_CONFIGURATION WHERE LC_USER_ID = @pUserId)
            UPDATE	LAYOUT_USERS_CONFIGURATION 
               SET	LC_IS_MANAGER = @pIsManager
                 ,  LC_IS_RUNNER = @pIsRunner
             WHERE	LC_USER_ID = @pUserId

      ELSE
       INSERT INTO	LAYOUT_USERS_CONFIGURATION
            VALUES 
				 (	@pUserId
                 ,	''
                 ,  NULL
                 ,	GETDATE()
                 ,  @pIsManager
                 ,  @pIsRunner
				 ) 
END --Layout_SaveConfiguration
GO

----------------------------------------------------
----------------------GRANTING----------------------
----------------------------------------------------

GRANT EXECUTE ON [dbo].[CreateDefaultProfilesLayout] TO [wggui] WITH GRANT OPTION;

GRANT EXECUTE ON [dbo].[Layout_SaveUser] TO [wggui] WITH GRANT OPTION;

