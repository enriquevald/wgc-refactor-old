USE WGDB_000;

SET ANSI_NULLS ON;

----------------------------------------------------
-----------------VERSION CONTROL--------------------
----------------------------------------------------

DECLARE 
@Exp_SmartFloor_ClientId int, 
@Exp_SmartFloor_CommonBuildId int,
@Exp_SmartFloor_ClientBuildId int,
@New_SmartFloor_ReleaseId int,
@New_SmartFloor_ScriptName nvarchar(50),
@New_SmartFloor_Description nvarchar(1000);

SET @Exp_SmartFloor_ClientId = 18;
SET @Exp_SmartFloor_CommonBuildId = 100;
SET @Exp_SmartFloor_ClientBuildId = 1;
SET @New_SmartFloor_ReleaseId = 30;
SET @New_SmartFloor_ScriptName = N'UpdateTo_SmartFloor_18.030.sql';
SET @New_SmartFloor_Description = N'Patch 18.030 - 20';

IF EXISTS ( SELECT 1 FROM DB_VERSION_SMARTFLOOR )
 DELETE FROM DB_VERSION_SMARTFLOOR;

INSERT INTO [dbo].[db_version_smartfloor]
           ([db_client_id]
           ,[db_common_build_id]
           ,[db_client_build_id]
           ,[db_release_id]
           ,[db_updated_script]
           ,[db_updated]
           ,[db_description])
     VALUES
           (@Exp_SmartFloor_ClientId
           ,@Exp_SmartFloor_CommonBuildId
           ,@Exp_SmartFloor_ClientBuildId
           ,@New_SmartFloor_ReleaseId
           ,@New_SmartFloor_ScriptName
           ,Getdate()
           ,@New_SmartFloor_Description);




   
----------------------------------------------------
----------------------STOREDS-----------------------
----------------------------------------------------
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_ReleaseAlarmFromTask]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Layout_ReleaseAlarmFromTask]
GO
 
 CREATE PROCEDURE [dbo].[Layout_ReleaseAlarmFromTask]
           @pTaskId INT
         , @pUserId INT
         , @pOutputDerivations INT OUTPUT
AS
BEGIN
DECLARE @DerivationsLeft AS INT
DECLARE @Today AS DATETIME
     SET @DerivationsLeft = 0
     SET @Today = GETDATE()
     SET @pOutputDerivations = 0
	 
 IF (EXISTS (SELECT TOP 1 * 
                   FROM LAYOUT_SITE_TASK_DERIVATIONS
                  WHERE LSTD_TASK_ID = @pTaskId
                    AND LSTD_USER_ID = @pUserId) 
	OR
	 EXISTS (SELECT TOP 1 * 
                   FROM LAYOUT_SITE_TASKS
                  WHERE LST_ID = @pTaskId
                    AND (LST_ASSIGNED_USER_ID = @pUserId OR LST_ACCEPTED_USER_ID= @pUserId )))
BEGIN
      EXEC Layout_InsertTasksDerivations @pTaskId, @pUserId, @Today

    SELECT @DerivationsLeft = COUNT(*) 
      FROM LAYOUT_SITE_TASK_DERIVATIONS 
     WHERE LSTD_TASK_ID = @pTaskId 
       AND LSTD_REJECTED_DATE IS NULL
       
	 IF EXISTS (SELECT TOP 1 * 
                   FROM LAYOUT_SITE_TASKS
                  WHERE LST_ID = @pTaskId
                    AND (LST_ASSIGNED_USER_ID = @pUserId OR LST_ACCEPTED_USER_ID= @pUserId ))
		BEGIN
		  SET @DerivationsLeft = 0
		END

     SET @pOutputDerivations = @DerivationsLeft

    IF @DerivationsLeft = 0
    BEGIN
            UPDATE LAYOUT_SITE_ALARMS 
               SET LSA_TASK_ID = NULL
                 , LSA_DATE_TO_TASK = NULL         
                 , LSA_STATUS = 2 
         WHERE LSA_TASK_ID = @pTaskId
         
        DELETE FROM LAYOUT_SITE_TASK_DERIVATIONS
         WHERE LSTD_TASK_ID = @pTaskId
    END 
  END
  
  SELECT @pOutputDerivations
END

GO

