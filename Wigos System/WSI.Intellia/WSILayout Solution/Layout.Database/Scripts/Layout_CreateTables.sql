﻿

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[layout_floors]') AND type in (N'U'))
DROP TABLE [dbo].[layout_floors]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[layout_locations]') AND type in (N'U'))
DROP TABLE [dbo].[layout_locations]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[layout_meshes]') AND type in (N'U'))
DROP TABLE [dbo].[layout_meshes]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[layout_object_locations]') AND type in (N'U'))
DROP TABLE [dbo].[layout_object_locations]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[layout_objects]') AND type in (N'U'))
DROP TABLE [dbo].[layout_objects]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[layout_objects_positions]') AND type in (N'U'))
DROP TABLE [dbo].[layout_objects_positions]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[layout_parameters]') AND type in (N'U'))
DROP TABLE [dbo].[layout_parameters]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[layout_ranges]') AND type in (N'U'))
DROP TABLE [dbo].[layout_ranges]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[layout_ranges_legends]') AND type in (N'U'))
DROP TABLE [dbo].[layout_ranges_legends]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[layout_recording]') AND type in (N'U'))
DROP TABLE [dbo].[layout_recording]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[layout_site_tasks]') AND type in (N'U'))
DROP TABLE [dbo].[layout_site_tasks]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[layout_users_configuration]') AND type in (N'U'))
DROP TABLE [dbo].[layout_users_configuration]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[layout_users_custom_filters]') AND type in (N'U'))
DROP TABLE [dbo].[layout_users_custom_filters]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[layout_views]') AND type in (N'U'))
DROP TABLE [dbo].[layout_views]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[layout_media]') AND type in (N'U'))
DROP TABLE [dbo].[layout_media]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[layout_site_alarms]') AND type in (N'U'))
DROP TABLE [dbo].[layout_site_alarms]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[layout_devices]') AND type in (N'U'))
DROP TABLE [dbo].[layout_devices]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[layout_enums]') AND type in (N'U'))
DROP TABLE [dbo].[layout_enums]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[layout_runners_position]') AND type in (N'U'))
DROP TABLE [dbo].[layout_runners_positions]
GO

CREATE TABLE [dbo].[layout_floors](
	[lf_floor_id] [int] IDENTITY(1,1) NOT NULL,
	[lf_geometry] [varchar](max) NULL,
	[lf_image_map] [varchar](max) NULL,
	[lf_color] [varchar](20) NULL,
	[lf_name] [varchar](255) NULL,
	[lf_height] [int] NULL,
	[lf_width] [int] NULL,
	[lf_metric] [int] NULL CONSTRAINT [DF_layout_floors_lf_metric]  DEFAULT ((0)),
 CONSTRAINT [PK_layout_floors] PRIMARY KEY CLUSTERED 
(
	[lf_floor_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

CREATE TABLE [dbo].[layout_locations](
	[loc_id] [bigint] IDENTITY(1,1) NOT NULL,
	[loc_x] [float] NOT NULL,
	[loc_y] [float] NOT NULL,
	[loc_creation] [datetime] NOT NULL,
	[loc_type] [int] NOT NULL,
	[loc_floor_number] [int] NOT NULL,
	[loc_grid_id] [nvarchar](50) NULL,
	[loc_status] [int] NOT NULL CONSTRAINT [DF_layout_locations_loc_status]  DEFAULT ((1)),
 CONSTRAINT [PK_layout_locations] PRIMARY KEY CLUSTERED 
(
	[loc_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de creación de la localización' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'layout_locations', @level2type=N'COLUMN',@level2name=N'loc_creation'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tipo de localización: SlotMachine: 1 , GamingTable: 2, Cashier: 3, Promobox:4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'layout_locations', @level2type=N'COLUMN',@level2name=N'loc_type'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id de la planta (altura del piso)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'layout_locations', @level2type=N'COLUMN',@level2name=N'loc_floor_number'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador de la localización en el grid virtual del site (AA01, ZZ99)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'layout_locations', @level2type=N'COLUMN',@level2name=N'loc_grid_id'
GO

CREATE TABLE [dbo].[layout_meshes](
	[lme_mesh_id] [bigint] NOT NULL,
	[lme_type] [int] NOT NULL,
	[lme_sub_type] [int] NULL,
	[lme_geometry] [varchar](max) NOT NULL,
 CONSTRAINT [PK_layout_meshes] PRIMARY KEY CLUSTERED 
(
	[lme_mesh_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[layout_object_locations](
	[lol_object_id] [bigint] NOT NULL,
	[lol_location_id] [bigint] NOT NULL,
	[lol_date_from] [datetime] NOT NULL,
	[lol_date_to] [datetime] NULL,
	[lol_orientation] [int] NULL,
	[lol_area_id] [int] NULL,
	[lol_bank_id] [int] NULL,
	[lol_bank_location] [int] NULL,
	[lol_current_location] [bit] NOT NULL,
	[lol_offset_x] [float] NOT NULL CONSTRAINT [DF_layout_object_locations_lol_offset_x]  DEFAULT ((0)),
	[lol_offset_y] [float] NOT NULL CONSTRAINT [DF_layout_object_locations_lol_offset_y]  DEFAULT ((0)),
 CONSTRAINT [PK_layout_object_locations] PRIMARY KEY CLUSTERED 
(
	[lol_object_id] ASC,
	[lol_location_id] ASC,
	[lol_date_from] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha  de asignación del objeto a la localización' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'layout_object_locations', @level2type=N'COLUMN',@level2name=N'lol_date_from'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Rotación del objeto en grados' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'layout_object_locations', @level2type=N'COLUMN',@level2name=N'lol_orientation'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id del área al que pertenece el objeto (Terminal, Mesa,...)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'layout_object_locations', @level2type=N'COLUMN',@level2name=N'lol_area_id'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id de la Isla a la que pertenece el Terminal' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'layout_object_locations', @level2type=N'COLUMN',@level2name=N'lol_bank_id'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Posición dentro de la isla (en sentido horario)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'layout_object_locations', @level2type=N'COLUMN',@level2name=N'lol_bank_location'
GO


CREATE TABLE [dbo].[layout_objects](
	[lo_id] [bigint] IDENTITY(1,1) NOT NULL,
	[lo_external_id] [bigint] NULL,
	[lo_type] [int] NOT NULL,
	[lo_parent_id] [bigint] NULL,
	[lo_mesh_id] [int] NULL,
	[lo_creation_date] [datetime] NOT NULL,
	[lo_update_date] [datetime] NULL,
	[lo_status] [int] NOT NULL CONSTRAINT [DF_layout_objects_lo_status]  DEFAULT ((1)),
 CONSTRAINT [PK_layout_objects] PRIMARY KEY CLUSTERED 
(
	[lo_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[layout_objects_positions](
	[lop_id] [bigint] NOT NULL,
	[lop_position_date] [datetime] NOT NULL,
	[lop_x] [float] NOT NULL,
	[lop_y] [float] NOT NULL,
	[lop_z] [float] NOT NULL,
	[lop_orientation] [int] NOT NULL,
	[lop_floor_id] [int] NULL,
	[lop_area_id] [int] NULL,
	[lop_bank_id] [int] NULL,
 CONSTRAINT [PK_layout_objects_positions] PRIMARY KEY CLUSTERED 
(
	[lop_id] ASC,
	[lop_position_date] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[layout_parameters](
	[lp_id] [int] NOT NULL,
	[lp_group] [nvarchar](50) NULL,
	[lp_desc] [nvarchar](250) NOT NULL,
	[lp_value] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_layout_parameters] PRIMARY KEY CLUSTERED 
(
	[lp_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


CREATE TABLE [dbo].[layout_ranges](
	[lr_id] [int] IDENTITY(1,1) NOT NULL,
	[lr_name] [nvarchar](50) NOT NULL,
	[lr_field] [nvarchar](250) NOT NULL,
	[lr_section_id] [int] NOT NULL,
	[lr_role] [int] NOT NULL CONSTRAINT [DF_layout_ranges_lr_role2]  DEFAULT ((0)),
	[lr_icon] [nvarchar](max) NULL,
	[lr_view] [int] NOT NULL,
 CONSTRAINT [PK_layout_ranges2] PRIMARY KEY CLUSTERED 
(
	[lr_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[layout_ranges_legends](
	[lrl_id] [int] IDENTITY(1,1) NOT NULL,
	[lrl_range_id] [int] NOT NULL,
	[lrl_label] [nvarchar](50) NOT NULL,
	[lrl_value1] [decimal](18, 2) NOT NULL,
	[lrl_value2] [decimal](18, 2) NULL,
	[lrl_operator] [nchar](10) NOT NULL,
	[lrl_color] [nvarchar](10) NOT NULL,
	[lrl_field] [nvarchar](250) NULL,
	[lrl_editable] [bit] NULL,
	[lrl_auto_assign_role] [int] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_layout_ranges_legends2] PRIMARY KEY CLUSTERED 
(
	[lrl_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[layout_recording](
	[lr_scene_id] [int] NOT NULL,
	[lr_date] [datetime] NOT NULL,
	[lr_floor] [int] NOT NULL,
	[lr_json] [varchar](max) NOT NULL,
 CONSTRAINT [PK_layout_recording] PRIMARY KEY CLUSTERED 
(
	[lr_scene_id] ASC,
	[lr_date] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
 
CREATE TABLE [dbo].[layout_site_tasks](
	[lst_id] [bigint] IDENTITY(1,1) NOT NULL,
	[lst_status] [int] NOT NULL,
	[lst_start] [datetime] NULL,
	[lst_end] [datetime] NULL,
	[lst_category] [int] NULL,
	[lst_subcategory] [int] NULL,
	[lst_terminal_id] [int] NULL,
	[lst_account_id] [bigint] NULL,
	[lst_description] [nvarchar](250) NULL,
	[lst_severity] [int] NULL,
	[lst_creation_user_id] [int] NOT NULL,
	[lst_creation] [datetime] NOT NULL,
	[lst_assigned_role_id] [int] NULL,
	[lst_assigned_user_id] [int] NULL,
	[lst_assigned] [datetime] NULL,
	[lst_accepted_user_id] [int] NULL,
	[lst_accepted] [datetime] NULL,
	[lst_scale_from_user_id] [int] NULL,
	[lst_scale_to_user_id] [int] NULL,
	[lst_scale_reason] [nvarchar](250) NULL,
	[lst_scale] [datetime] NULL,
	[lst_solved_user_id] [int] NULL,
	[lst_solved] [datetime] NULL,
	[lst_validate_user_id] [int] NULL,
	[lst_validate] [datetime] NULL,
	[lst_last_status_update_user_id] [int] NULL,
	[lst_last_status_update] [datetime] NULL,
	[lst_attached_media] [int] NULL,
	[lst_events_history] [nvarchar](max) NULL,
 CONSTRAINT [PK_layout_site_tasks] PRIMARY KEY CLUSTERED 
(
	[lst_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

CREATE TABLE [dbo].[layout_users_configuration](
	[lc_user_id] [int] NOT NULL,
	[lc_parameters] [varchar](max) NOT NULL,
	[lc_dashboard] [varchar](max) NULL,
	[lc_last_update] [datetime] NOT NULL,
	[lc_is_manager] [bit] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_layout_configuration] PRIMARY KEY CLUSTERED 
(
	[lc_user_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[layout_users_custom_filters](
	[lcf_id] [int] IDENTITY(1,1) NOT NULL,
	[lcf_user_id] [int] NOT NULL,
	[lcf_filter_type] [int] NOT NULL,
	[lcf_name] [nvarchar](50) NOT NULL,
	[lcf_filter_parameters] [nvarchar](max) NOT NULL,
	[lcf_enabled] [bit] NOT NULL,
	[lcf_last_update] [datetime] NOT NULL,
	[lcf_has_sound] [bit] NULL,
	[lcf_color] [nvarchar](10) NULL,
 CONSTRAINT [PK_layout_custom_filters] PRIMARY KEY CLUSTERED 
(
	[lcf_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[layout_views](
	[lv_id] [int] NOT NULL,
	[lv_desc] [nvarchar](500) NOT NULL,
	[lv_value] [nvarchar](max) NOT NULL,
	[lv_type] [int] NOT NULL,
 CONSTRAINT [PK_layout_views] PRIMARY KEY CLUSTERED 
(
	[lv_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO



CREATE TABLE [dbo].[layout_media](
	[lm_id] [bigint] IDENTITY(1	,1) NOT NULL,  --UNIQUE ID
	[lm_data] [varbinary](max) NOT NULL,	  --churro bianrio con la imagen o media
	[lm_created] [datetime] NOT NULL,		  --fecha de creación 
	[lm_user_created] [int] NOT NULL,		  --Usuario que lo crea en la app (user id)
	[lm_type] [int] NOT NULL,				  --Familia (1=Máquina, 2=Jugador, 3= Sala) ENUMERADO
	[lm_external_id] [nvarchar](50) NULL,	  --Id externo (TerminalID, ObjectId)
	[lm_description] [nvarchar](250) NULL,    --Comentario o descripción
	[lm_media_type] [int] NOT NULL,           --Tipo de media (1=Foto, 2=Audio, 3= Video) ENUMERADO
	[lm_data_thumbnail] [varbinary](max) NULL,

 CONSTRAINT [PK_layout_media] PRIMARY KEY CLUSTERED 
(
	[lm_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]


GO

CREATE TABLE [dbo].[layout_site_alarms](
	[lsa_terminal_id] [int] NOT NULL,
	[lsa_alarm_id] [bigint] NOT NULL,
	[lsa_date_created] [datetime] NOT NULL,
	[lsa_id] [bigint] IDENTITY(1,1) NOT NULL,
	[lsa_alarm_type] [int] NOT NULL,
	[lsa_alarm_source] [int] NOT NULL,
	[lsa_user_created] [int] NOT NULL,
	[lsa_media_id] [bigint] NULL,
	[lsa_description] [nvarchar](max) NULL,
	[lsa_date_to_task] [datetime] NULL,
	[lsa_task_id] [bigint] NULL,
	[lsa_status] [int] NOT NULL CONSTRAINT [DF_layout_site_alarms_lsa_status]  DEFAULT ((0)),
 CONSTRAINT [PK_layout_site_alarms_1] PRIMARY KEY CLUSTERED 
(
	[lsa_terminal_id] ASC,
	[lsa_alarm_id] ASC,
	[lsa_date_created] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[layout_site_alarms]') AND name = N'IX_lsa_id')
DROP INDEX [IX_lsa_id] ON [dbo].[layout_site_alarms] WITH ( ONLINE = OFF )
GO

CREATE NONCLUSTERED INDEX [IX_lsa_id] ON [dbo].[layout_site_alarms] 
(
	[lsa_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

GO

CREATE TABLE [dbo].[layout_enums](
	[le_id] [bigint] NULL,
	[le_level_1] [bigint] NULL,
	[le_level_2] [bigint] NULL,
	[le_description] [nvarchar](250) NULL,
	[le_param_1] [nvarchar](50) NULL,
	[le_param_2] [nvarchar](50) NULL,
	[le_param_3] [bigint] NULL,
	[le_param_4] [bigint] NULL
) ON [PRIMARY]

GO


CREATE TABLE [dbo].[layout_devices](
	[ld_id] [bigint] IDENTITY(1,1) NOT NULL,
	[ld_device_id] [nvarchar](50) NOT NULL,
	[ld_user_id] [bigint] NOT NULL,
	[ld_type] [int] NULL,
	[ld_model] [nvarchar](250) NULL,
	[ld_enabled] [bit] NULL,
	[ld_status] [int] NULL,
	[ld_date] [datetime] NULL,
	[ld_last_update] [datetime] NULL,
 CONSTRAINT [PK_layout_device] PRIMARY KEY CLUSTERED 
(
	[ld_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO



CREATE TABLE [dbo].[layout_runners_position](	
	[lrp_user_id] [bigint] NOT NULL, --id del usuario
	[lrp_device_id] [nvarchar](50) NOT NULL, --id del smartphone
	[lrp_floor_id] [nvarchar](50) NOT NULL,	-- id del piso
	[lrp_x] [float] NULL, --Si triangulamos, establece la x (coordenada)
	[lrp_y] [float] NULL, --Si triangulamos, establece la y (coordenada)
	[lrp_range] [float] NULL,  -- Si no triangulamos, sirve para mostrar la distancia en metros del beacon más cercano
	[lrp_last_update] [datetime] NULL,
 CONSTRAINT [PK_layout_runners_position] PRIMARY KEY CLUSTERED 
(
	[lrp_user_id] ASC,
	[lrp_device_id]  ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

  ALTER TABLE [layout_devices] add  ld_device_name NVARCHAR(50)  NULL;


/*   INITIAL SETTINGS - GENERAL PARAMS */

--gp_group_key	gp_subject_key					gp_key_value

--DATA.Service	DATA_Param_ExpiracionConexion	61000 : 61 segundos
--DATA.Service	DATA_Param_NET_MSG_Enabled		0  : false

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'DATA.Service' AND GP_SUBJECT_KEY ='DATA_Param_ExpiracionConexion')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('DATA.Service'
             ,'DATA_Param_ExpiracionConexion'
             ,'61000')

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'DATA.Service' AND GP_SUBJECT_KEY ='DATA_Param_NET_MSG_Enabled')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('DATA.Service'
             ,'DATA_Param_NET_MSG_Enabled'
             ,'0')


GO




