USE WGDB_000;

SET ANSI_NULLS ON;

----------------------------------------------------
-----------------VERSION CONTROL--------------------
----------------------------------------------------

DECLARE 
@Exp_SmartFloor_ClientId int, 
@Exp_SmartFloor_CommonBuildId int,
@Exp_SmartFloor_ClientBuildId int,
@New_SmartFloor_ReleaseId int,
@New_SmartFloor_ScriptName nvarchar(50),
@New_SmartFloor_Description nvarchar(1000);

SET @Exp_SmartFloor_ClientId = 18;
SET @Exp_SmartFloor_CommonBuildId = 100;
SET @Exp_SmartFloor_ClientBuildId = 1;
SET @New_SmartFloor_ReleaseId = 11;
SET @New_SmartFloor_ScriptName = N'UpdateTo_SmartFloor_18.012.sql';
SET @New_SmartFloor_Description = N'Patch 18.012 - 11';

IF EXISTS ( SELECT 1 FROM DB_VERSION_SMARTFLOOR )
 DELETE FROM DB_VERSION_SMARTFLOOR;

INSERT INTO [dbo].[db_version_smartfloor]
           ([db_client_id]
           ,[db_common_build_id]
           ,[db_client_build_id]
           ,[db_release_id]
           ,[db_updated_script]
           ,[db_updated]
           ,[db_description])
     VALUES
           (@Exp_SmartFloor_ClientId
           ,@Exp_SmartFloor_CommonBuildId
           ,@Exp_SmartFloor_ClientBuildId
           ,@New_SmartFloor_ReleaseId
           ,@New_SmartFloor_ScriptName
           ,Getdate()
           ,@New_SmartFloor_Description);

           
----------------------------------------------------
----------------------STOREDS-----------------------
----------------------------------------------------

/****** Object:  StoredProcedure [dbo].[Layout_AutoNotificationAlarm]    Script Date: 04/22/2016 12:59:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_AutoNotificationAlarm]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Layout_AutoNotificationAlarm]
GO

/****** Object:  StoredProcedure [dbo].[Layout_AutoNotificationAlarm]    Script Date: 04/22/2016 12:59:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE  PROCEDURE [dbo].[Layout_AutoNotificationAlarm]
      @pStatus INT
    , @pCategory INT
    , @pSubcategory INT
    , @pTerminalId INT
    , @pSeverity INT
    , @pCreationUserId INT
    , @pCreation DATETIME
    , @pAssignedRoleId INT
    , @pAssignedUserId INT
    , @pAssigned DATETIME
    , @pHistory NVARCHAR(MAX)
    , @pDateCreated DATETIME
    , @poutTaskId INT OUTPUT
AS
BEGIN
DECLARE @TaskId as INT

EXEC Layout_SaveSiteTask -1,@pStatus,NULL,NULL,@pCategory,@pSubcategory,@pTerminalId,NULL,NULL,@pSeverity,@pCreationUserId,@pCreation,@pAssignedRoleId,@pAssignedUserId,@pAssigned,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,@pAssigned,NULL,@pHistory,@TaskId OUTPUT,NULL

  UPDATE LAYOUT_SITE_ALARMS
     SET LSA_TASK_ID      = @TaskId
       , LSA_DATE_TO_TASK = @pDateCreated
   WHERE LSA_TERMINAL_ID  = @pTerminalId
     AND LSA_ALARM_ID     = @pSubcategory
     AND LSA_DATE_CREATED = @pDateCreated
     
     SELECT @poutTaskId = @TaskId;
END 

GO

/****** Object:  StoredProcedure [dbo].[Layout_GetTerminalStatusData]    Script Date: 04/22/2016 13:00:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetTerminalStatusData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Layout_GetTerminalStatusData]
GO

/****** Object:  StoredProcedure [dbo].[Layout_GetTerminalStatusData]    Script Date: 04/22/2016 13:00:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Layout_GetTerminalStatusData]
AS
BEGIN
 SELECT TS_TERMINAL_ID
      , TS_EGM_FLAGS
      , TS_DOOR_FLAGS
      , TS_BILL_FLAGS
      , TS_PRINTER_FLAGS
      , TS_PLAYED_WON_FLAGS
      , TS_JACKPOT_FLAGS
      , CASE WHEN TS_CALL_ATTENDANT_FLAGS = 1 THEN 1 ELSE 0 END AS TS_CALL_ATTENDANT_FLAGS
      , TS_STACKER_STATUS
      , TS_MACHINE_FLAGS
      , LOC_FLOOR_NUMBER AS FLOOR_LOCATION
      ,	LOC_X
      ,	LOC_Y
   FROM TERMINAL_STATUS
   INNER JOIN LAYOUT_OBJECTS ON TS_TERMINAL_ID = LO_EXTERNAL_ID
   INNER JOIN LAYOUT_OBJECT_LOCATIONS ON LOL_OBJECT_ID = LO_ID
   INNER JOIN LAYOUT_LOCATIONS ON LOC_ID = LOL_LOCATION_ID
   WHERE LOL_CURRENT_LOCATION = 1
   GROUP BY TS_TERMINAL_ID
      , TS_EGM_FLAGS
      , TS_DOOR_FLAGS
      , TS_BILL_FLAGS
      , TS_PRINTER_FLAGS
      , TS_PLAYED_WON_FLAGS
      , TS_JACKPOT_FLAGS
      , TS_CALL_ATTENDANT_FLAGS
      , TS_STACKER_STATUS
      , TS_MACHINE_FLAGS
      , LOC_FLOOR_NUMBER
      , LOC_X
      , LOC_Y
END

GO

/****** Object:  StoredProcedure [dbo].[Layout_GetConversationHistory]    Script Date: 04/28/2016 09:43:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetConversationHistory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Layout_GetConversationHistory]
GO

/****** Object:  StoredProcedure [dbo].[Layout_GetConversationHistory]    Script Date: 04/28/2016 09:43:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Layout_GetConversationHistory]
									@pManagerId int
							 ,  @pRunnerId int
WITH EXEC AS CALLER
AS
BEGIN
			SELECT	LMRM_ID
					 ,	LMRM_MANAGER_ID
					 ,	U_MANAGER.GU_USERNAME as MANAGER_NAME
					 ,	LMRM_RUNNER_ID
					 ,	U_RUNNER.GU_USERNAME as RUNNER_NAME
					 ,	LMRM_DATE
					 ,	LMRM_MESSAGE
					 ,	LMRM_SOURCE
					 ,	LMRM_STATUS
				FROM LAYOUT_MANAGER_RUNNER_MESSAGES LMRM
	INNER JOIN GUI_USERS U_MANAGER ON LMRM.LMRM_MANAGER_ID = U_MANAGER.GU_USER_ID 
	INNER JOIN GUI_USERS U_RUNNER ON LMRM.LMRM_RUNNER_ID = U_RUNNER.GU_USER_ID 
			 WHERE LMRM.LMRM_DATE <= CAST(GETDATE() AS DATETIME)
				 AND LMRM_MANAGER_ID = @pManagerId
				 AND LMRM_RUNNER_ID = @pRunnerId
		ORDER BY LMRM.LMRM_DATE
END
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__layout_ma__lmrm___1E305893]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[layout_manager_runner_messages] DROP CONSTRAINT [DF__layout_ma__lmrm___1E305893]
END

GO
/****** Object:  Table [dbo].[layout_manager_runner_messages]    Script Date: 04/28/2016 09:45:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[layout_manager_runner_messages]') AND type in (N'U'))
DROP TABLE [dbo].[layout_manager_runner_messages]
GO

/****** Object:  Table [dbo].[layout_manager_runner_messages]    Script Date: 04/28/2016 09:45:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[layout_manager_runner_messages](
	[lmrm_id] [bigint] IDENTITY(1,1) NOT NULL,
	[lmrm_manager_id] [bigint] NOT NULL,
	[lmrm_runner_id] [bigint] NOT NULL,
	[lmrm_date] [datetime] NOT NULL,
	[lmrm_message] [text] NOT NULL,
	[lmrm_source] [int] NOT NULL,
	[lmrm_status] [int] NOT NULL,
 CONSTRAINT [PK_layout_manager_runner_messages] PRIMARY KEY CLUSTERED 
(
	[lmrm_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[layout_manager_runner_messages] ADD  DEFAULT ((0)) FOR [lmrm_status]
GO

/****** Object:  StoredProcedure [dbo].[Layout_IsValidTerminal]    Script Date: 04/28/2016 15:34:17 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_IsValidTerminal]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Layout_IsValidTerminal]
GO

/****** Object:  StoredProcedure [dbo].[Layout_IsValidTerminal]    Script Date: 04/28/2016 15:34:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Layout_IsValidTerminal] 
	@terminalId BigInt,
	@count_terminal Int OUT
AS
BEGIN
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT @count_terminal=COUNT(*) FROM terminals WHERE te_terminal_id = @terminalId
END

GO

/****** Object:  StoredProcedure [dbo].[Layout_GetAssignedTaskToUser]    Script Date: 05/03/2016 15:55:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetAssignedTaskToUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Layout_GetAssignedTaskToUser]
GO

/****** Object:  StoredProcedure [dbo].[Layout_GetAssignedTaskToUser]    Script Date: 05/03/2016 15:55:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Layout_GetAssignedTaskToUser]
@loginId bigint
WITH EXEC AS CALLER
AS
BEGIN
  DECLARE @terminal_name_type AS VARCHAR(50)
	DECLARE @big_won_limit AS MONEY
	DECLARE  @Date AS DATETIME
	SET @Date = GETDATE()
	DECLARE   @IntervalHour INT 
	SET @IntervalHour = 1

	SET @big_won_limit = 1000
	
	DECLARE @Now AS DATETIME
	DECLARE @CurrentHour AS BIGINT
	DECLARE @HourNow AS DATETIME
	DECLARE @NextHourNow AS DATETIME
	
	SET @Now = CAST(DATEPART(YEAR, @Date) AS VARCHAR) + '-' +  CAST(DATEPART(MONTH, @Date) AS VARCHAR)  + '-' +  CAST(DATEPART(DAY, @Date) AS VARCHAR)  + ' ' +  CAST(DATEPART(HOUR, @Date) AS VARCHAR) + ':' + CAST(DATEPART(MINUTE, GETDATE()) AS VARCHAR) + ':' + CAST(DATEPART(SECOND, GETDATE()) AS VARCHAR) + '.000'
	  
	SET @CurrentHour = DATEPART(HOUR, @Now)
	SET @HourNow = DATEADD(HOUR, DATEDIFF(HOUR, 0, @Now), 0)
	SET @NextHourNow = DATEADD(HOUR, @IntervalHour, @HourNow)
	
				SELECT	LST.LST_ID
						 ,	LST.LST_STATUS
						 ,	LST.LST_START
						 ,	LST.LST_END
						 ,	LST.LST_CATEGORY
						 ,	LST.LST_SUBCATEGORY
						 ,	LST.LST_TERMINAL_ID
						 ,	LST.LST_ACCOUNT_ID
						 ,	LST.LST_DESCRIPTION
						 ,	LST.LST_SEVERITY
						 ,	LST.LST_CREATION
						 ,	LST.LST_ASSIGNED_ROLE_ID
						 ,	LST.LST_ASSIGNED_USER_ID
						 ,	LST.LST_ASSIGNED
						 ,	LST.LST_ACCEPTED
						 ,	TE.TE_NAME
						 ,	TE.TE_PROVIDER_ID
						 ,	TE.TE_MODEL
						 ,	A.AR_NAME
						 ,	BK.BK_NAME
						 ,	PLAY_SESSION_STATUS = CASE  WHEN (PS_PLAY_SESSION_ID IS NOT NULL) THEN 2  
																						ELSE 1 END
						 ,	IS_ANONYMOUS = CASE  WHEN (PS.PS_ACCOUNT_ID IS NOT NULL) THEN CASE WHEN (AC.AC_HOLDER_NAME IS NOT NULL) THEN 0 ELSE 1 END 
																						ELSE NULL END
						 ,	PS.PS_ACCOUNT_ID AS PLAYER_ACCOUNT_ID
						 ,	PLAYER_NAME = CASE WHEN (PS.PS_ACCOUNT_ID IS NOT NULL) THEN ISNULL(AC.AC_HOLDER_NAME, 'ANONYMOUS')
																	 ELSE NULL END
						 ,	ISNULL(AC.AC_HOLDER_GENDER, 0) AS PLAYER_GENDER
						 ,	DATEDIFF(YEAR, AC.AC_HOLDER_BIRTH_DATE, @NOW) AS PLAYER_AGE
						 ,	PLAYER_LEVEL = AC.AC_HOLDER_LEVEL
						 ,	ISNULL(AC.AC_HOLDER_IS_VIP, 0) AS PLAYER_IS_VIP
						 ,	LST.LST_TITLE
					FROM LAYOUT_SITE_TASKS AS LST
		INNER JOIN TERMINALS AS TE ON LST.LST_TERMINAL_ID = TE.TE_TERMINAL_ID
		INNER JOIN PROVIDERS AS PR ON PR.PV_ID = TE.TE_PROV_ID
  	 LEFT JOIN BANKS AS BK ON BK.BK_BANK_ID = TE.TE_BANK_ID
		INNER JOIN AREAS a ON a.AR_AREA_ID = bk.BK_AREA_ID 
		 LEFT JOIN (SELECT	PS_TERMINAL_ID
										 ,	PS_PLAY_SESSION_ID
										 ,	PS_ACCOUNT_ID
										 ,	PS_TOTAL_PLAYED
										 ,	PS_WON_AMOUNT
										 ,	PS_PLAYED_COUNT
										 ,	PS_STARTED
										 ,	DBO.GETRATE(PS_STARTED,PS_FINISHED, @NOW) AS PS_RATE
							    FROM PLAY_SESSIONS
								 WHERE ( PS_STARTED <= @Now AND PS_FINISHED > @Now )
			     AND ps_played_count>0) AS PS ON TE.TE_TERMINAL_ID = PS.PS_TERMINAL_ID --AND PS_STARTED >= @NOW
		 LEFT JOIN ACCOUNTS AS AC ON AC.AC_ACCOUNT_ID = PS.PS_ACCOUNT_ID
		 LEFT JOIN TERMINAL_STATUS AS TS ON TE.TE_TERMINAL_ID = TS.TS_TERMINAL_ID
		 LEFT JOIN (SELECT	SPH_TERMINAL_ID 
										 ,	SUM(SPH_PLAYED_AMOUNT)*DBO.GETRATE(DATEADD(DAY, DATEDIFF(DAY, 0, @NOW), 0) ,DATEADD(DAY, DATEDIFF(DAY, 0, @NOW), 1), @NOW)  AS PLAYED_AMOUNT
										 ,	SUM(SPH_WON_AMOUNT)*DBO.GETRATE(DATEADD(DAY, DATEDIFF(DAY, 0, @NOW), 0) ,DATEADD(DAY, DATEDIFF(DAY, 0, @NOW), 1), @NOW)    AS WON_AMOUNT
										 ,	SUM(SPH_PLAYED_COUNT)*DBO.GETRATE(DATEADD(DAY, DATEDIFF(DAY, 0, @NOW), 0) ,DATEADD(DAY, DATEDIFF(DAY, 0, @NOW), 1), @NOW)  AS PLAYED_COUNT
									FROM SALES_PER_HOUR
								 WHERE SPH_BASE_HOUR >= DATEADD(DAY, DATEDIFF(DAY, 0, @NOW), 0) 
									 AND SPH_BASE_HOUR <= DATEADD(DAY, DATEDIFF(DAY, 0, @NOW), 1) 
					    GROUP BY SPH_TERMINAL_ID) AS SPH ON SPH.SPH_TERMINAL_ID = TE.TE_TERMINAL_ID
		     
		 LEFT JOIN (SELECT	CAST(REPLACE(REPLACE(GP_SUBJECT_KEY,'LEVEL',''),'.NAME','') AS INT) AS NUM
										 ,	GP_KEY_VALUE AS NAME
									FROM GENERAL_PARAMS 
								 WHERE GP_GROUP_KEY ='PLAYERTRACKING'
									 AND GP_SUBJECT_KEY LIKE 'LEVEL%.NAME'
				  ) AS LEVELS ON LEVELS.NUM = AC.AC_HOLDER_LEVEL 
				 WHERE LST_STATUS IN (1,2)
				   AND ((LST_ASSIGNED_USER_ID = @loginId)
						OR  (LST_ACCEPTED_USER_ID = @loginId)
						OR  ((LST_ASSIGNED_ROLE_ID IN (SELECT   DISTINCT(GPF_FORM_ID / 1000000) AS ROLE
																						 FROM   GUI_PROFILE_FORMS
																						WHERE   GPF_GUI_ID = 203 AND GPF_READ_PERM <> 0
																							AND   GPF_PROFILE_ID = (
																																				SELECT   GU_PROFILE_ID
																																					FROM   GUI_USERS
																																				 WHERE   GU_USER_ID    = @loginId
																					)
					 AND   (GPF_FORM_ID / 1000000) > 0))  AND ( isnull(LST_ACCEPTED_USER_ID, 0) = 0)))
END


GO

/****** Object:  StoredProcedure [dbo].[SP_Get_Meters_List]    Script Date: 05/06/2016 12:26:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_Get_Meters_List]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_Get_Meters_List]
GO

/****** Object:  StoredProcedure [dbo].[SP_Get_Meters_List]    Script Date: 05/06/2016 12:26:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--CREATE PROCEDURE [dbo].[SP_Get_Meters_List] 
CREATE PROCEDURE [dbo].[SP_Get_Meters_List] 
 ( @Device AS INT ) 
AS
BEGIN
  SELECT  HMD_METER_ID
       ,	HMD_METER_ITEM
       ,	HMD_METER_TYPE
       ,	HMD_DESCRIPTION
       ,	HMD_METER_LAST_UPDATE
    FROM  H_METERS_DEFINITION
   WHERE  HMD_VISIBLE = 1
     AND  HMD_METER_DEVICE = @Device
END

GO

/****** Object:  StoredProcedure [dbo].[Layout_GetRunnersPositions]    Script Date: 05/10/2016 15:48:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetRunnersPositions]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Layout_GetRunnersPositions]
GO

/****** Object:  StoredProcedure [dbo].[Layout_GetRunnersPositions]    Script Date: 05/10/2016 15:48:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Layout_GetRunnersPositions]
AS
BEGIN
  
  SELECT   GUIU.GU_USER_ID
         , LRP_DEVICE_ID
         , LRP_FLOOR_ID
         , LRP_X
         , LRP_Y
         , LRP_RANGE
         , LRP_LAST_UPDATE
         , GUIU.GU_USERNAME
         , ISNULL(LD_DEVICE_NAME,LD_DEVICE_ID) LD_NAME
         , LF_NAME
         , (SELECT   SUM(DISTINCT (GPF_FORM_ID / 1000000)) - 16 AS USER_ROLES
              FROM   GUI_PROFILE_FORMS
             WHERE   GPF_GUI_ID = 203 AND GPF_READ_PERM <> 0 
               AND   GPF_PROFILE_ID = 
             (
                 SELECT   GU_PROFILE_ID 
                   FROM   GUI_USERS 
                  WHERE   GU_USER_ID = GUIU.GU_USER_ID
             )) AS USER_ROLES

    FROM   GUI_USERS GUIU
   INNER   JOIN LAYOUT_USERS_CONFIGURATION ON GU_USER_ID    = LC_USER_ID 
   INNER   JOIN (
              SELECT LRP_USER_ID
                   , LRP_DEVICE_ID
                   , LRP_FLOOR_ID
                   , LRP_X
                   , LRP_Y
                   , LRP_RANGE
                   , LRP_LAST_UPDATE
                FROM LAYOUT_RUNNERS_POSITION
               WHERE LRP_LAST_UPDATE IN (
                     SELECT MAX(LRP_LAST_UPDATE) AS  LRP_LAST_UPDATE
                       FROM LAYOUT_RUNNERS_POSITION
                   GROUP BY LRP_USER_ID
                   )
       )T    ON GU_USER_ID    = T.LRP_USER_ID
    LEFT   JOIN LAYOUT_DEVICES             ON LRP_DEVICE_ID = LD_DEVICE_ID
   INNER   JOIN LAYOUT_FLOORS              ON LF_FLOOR_ID   = LRP_FLOOR_ID
END
GO





