CREATE PROCEDURE [dbo].[SP_Get_Meters_List] 
 ( @Device AS INT ) 
AS
BEGIN
  SELECT   hmd_meter_id
         , hmd_meter_item
         , hmd_meter_type
         , hmd_description
         , hmd_meter_last_update
    FROM   H_METERS_DEFINITION
   WHERE   hmd_visible = 1
     AND   hmd_meter_device = @Device
END
GO

CREATE PROCEDURE [dbo].[SP_Get_Meters] 
(  @Device AS INT
 , @Source AS VARCHAR(1)
 , @Date AS DATETIME
) 
AS
BEGIN

DECLARE @DATEFROM AS DATETIME
DECLARE @DATEFROM_WEEK AS DATETIME
DECLARE @DATEFROM_MONTH AS DATETIME
DECLARE @DATEFROM_YEAR AS DATETIME
DECLARE @SEARCH_DATE AS NVARCHAR(MAX)
DECLARE @QUERY_CONST AS NVARCHAR(MAX)
DECLARE @QUERY_TEMP AS NVARCHAR(MAX)
DECLARE @FINAL_QUERY AS NVARCHAR(MAX)
DECLARE @QUERY AS NVARCHAR(MAX)
DECLARE @RANGE AS VARCHAR(1)
DECLARE @DESCRIPTION AS VARCHAR(3)
DECLARE @HOUR AS VARCHAR(2)
DECLARE @ORIGIN AS VARCHAR(MAX)
DECLARE @DOW AS VARCHAR(1)

SET @FINAL_QUERY = ''

SET @DATEFROM = @Date --GETDATE()

EXEC SP_Get_Meters_List @DEVICE

SET @DATEFROM_WEEK = DATEADD(WK, DATEDIFF(WK, 0, @DATEFROM) - 1, 0)             -- First day of previous week
SET @DATEFROM_MONTH = DATEADD(M, -1, DATEADD(M, DATEDIFF(M, 0, @DATEFROM), 0))  -- First day of previous month
SET @DATEFROM_YEAR = DATEADD(YY, -1, DATEADD(YY, DATEDIFF(YY, 0, @DATEFROM), 0)) -- First day of previous year
SET @DOW = LTRIM(DATEPART(WEEKDAY, @DATEFROM))

SET @HOUR = DATEPART(HOUR, GETDATE())
SET @HOUR = REPLICATE('0',2-LEN(RTRIM(@HOUR))) + RTRIM(@HOUR) 

SET @QUERY_CONST = ' SELECT ''@DESCRIPTION'' AS RANGE, @HOUR as HOUR, x2d_date, x2d_weekday, x2d_id, x2d_meter_id, x2d_meter_item,'
                 + ' x2d_@HOUR_min AS MIN, x2d_@HOUR_max as MAX, x2d_@HOUR_acc AS ACC, x2d_@HOUR_avg AS AVG, x2d_@HOUR_num AS NUM,'
                 + ' x2d_dd_min AS DAY_MIN, x2d_dd_max AS DAY_MAX, x2d_dd_acc AS DAY_ACC, x2d_dd_avg AS DAY_AVG, x2d_dd_num AS DAY_NUM'
                 + ' FROM  H_@RANGE2D_@SOURCEMH'
                 + ' INNER JOIN H_METERS_DEFINITION ON x2d_meter_id = hmd_meter_id AND x2d_meter_item = hmd_meter_item'
                 + ' WHERE hmd_visible = 1 AND (@DATE)'

-- TDA & SLW
SET @QUERY_TEMP = @QUERY_CONST
SET @RANGE = 'T'
SET @DESCRIPTION = 'TDA'
SET @SEARCH_DATE = 'x2d_date >= ' + CONVERT(VARCHAR(10), DATEADD(DAY, -7, @DATEFROM), 112) + ' AND x2d_date <= ' + CONVERT(VARCHAR(10), @DATEFROM, 112)
SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DATE', @SEARCH_DATE)
SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@HOUR', LTRIM(@HOUR))
SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@RANGE', LTRIM(@RANGE))
SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DESCRIPTION', LTRIM(@DESCRIPTION))
SET @FINAL_QUERY = @QUERY_TEMP 

-- WTD
SET @QUERY_TEMP = @QUERY_CONST
SET @RANGE = 'W'
SET @DESCRIPTION = 'WTD'
SET @SEARCH_DATE = 'x2d_date = ' + CAST(CONVERT(VARCHAR(10), @DATEFROM_WEEK, 112) AS VARCHAR)
SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DATE', LTRIM(@SEARCH_DATE))
SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@HOUR', LTRIM(@HOUR))
SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@RANGE', LTRIM(@RANGE))
SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DESCRIPTION', LTRIM(@DESCRIPTION))
--EXECUTE sp_executesql @QUERY
SET @FINAL_QUERY = @FINAL_QUERY
                 + ' UNION '
                 + @QUERY_TEMP 
                 + ' AND x2d_weekday = ' + @DOW

-- MTD
SET @QUERY_TEMP = @QUERY_CONST
SET @RANGE = 'M'
SET @DESCRIPTION = 'MTD'
SET @SEARCH_DATE = 'x2d_date = ' + CAST(CONVERT(VARCHAR(10), @DATEFROM_MONTH, 112) AS VARCHAR)
SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DATE', LTRIM(@SEARCH_DATE))
SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@HOUR', LTRIM(@HOUR))
SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@RANGE', LTRIM(@RANGE))
SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DESCRIPTION', LTRIM(@DESCRIPTION))
SET @FINAL_QUERY = @FINAL_QUERY
                 + ' UNION '
                 + @QUERY_TEMP 
                 + ' AND x2d_weekday = ' + @DOW

-- YTD
SET @QUERY_TEMP = @QUERY_CONST
SET @RANGE = 'Y'
SET @DESCRIPTION = 'YTD'
SET @SEARCH_DATE = 'x2d_date = ' + CAST(CONVERT(VARCHAR(10), @DATEFROM_YEAR, 112) AS VARCHAR)
SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DATE', LTRIM(@SEARCH_DATE))
SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@HOUR', LTRIM(@HOUR))
SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@RANGE', LTRIM(@RANGE))
SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DESCRIPTION', LTRIM(@DESCRIPTION))
SET @FINAL_QUERY = @FINAL_QUERY
                 + ' UNION '
                 + @QUERY_TEMP 
                 + ' AND x2d_weekday = ' + @DOW

SET @QUERY = REPLACE(@FINAL_QUERY, '@SOURCE', @Source)

PRINT @QUERY
           
EXECUTE sp_executesql @QUERY

--SELECT @QUERY           

END
GO

CREATE PROCEDURE [dbo].[SP_Set_Meter_Data]
(
    @pTableId   CHAR(1)		  -- L (Life), Y (Year), M (Month), W (Week), T (Today)
  , @pEntityId  CHAR(1)		  -- T (Terminales), S (Site)
  , @pDate      DATETIME		
  , @pId        INT         -- Terminal ID or Site Id
  , @pMeterId   INT			    -- 1=CoinIn, 2= NetWin, 3=BetAvg, 4=Gender...
  , @pMeterItem INT         -- 0=Simple meter data, 1=Gender-Male, 2=Gender-female...
  , @pMin       BIGINT
  , @pMax       BIGINT
  , @pAcc       BIGINT
  , @pAvg       BIGINT
  , @pNum       BIGINT
)
AS
BEGIN
          
  DECLARE @_date AS DATETIME          
  DECLARE @_date_val AS INT
  DECLARE @_wk AS INT
  DECLARE @_h AS VARCHAR(2)
          
  DECLARE @_get_sql NVARCHAR(MAX)    
  DECLARE @_insert_sql NVARCHAR(MAX)
  DECLARE @_update_sql NVARCHAR(MAX)

  DECLARE @pCurrentMin INT
  DECLARE @pCurrentMax BIGINT
  DECLARE @pCurrentAcc BIGINT
  DECLARE @pCurrentAvg DECIMAL
  DECLARE @pCurrentNum BIGINT

  SET @_date = dbo.SP_Get_Range_Date(@pDate, @pTableId)

  SET @_date_val = CAST(CONVERT(VARCHAR(8), @_date, 112) AS INT)
  SET @_wk = DATEPART(WEEKDAY, @pDate)
  SET @_h = DATEPART(HOUR, @pDate)
  SET @_h = REPLICATE('0',2-LEN(RTRIM(@_h))) + RTRIM(@_h) 

  SET @pCurrentMin = 0
  SET @pCurrentMax = 0
  SET @pCurrentAcc = 0
  SET @pCurrentAvg = 0
  SET @pCurrentNum = 0

  ------------------------------------------------------------------------------------------------
  -- Obtain current meter data

  SET @_get_sql = ' SELECT   @pCurrentMin = ISNULL(x2d_' + CAST(@_h AS VARCHAR) + '_min, 0)
                           , @pCurrentMax = ISNULL(x2d_' + CAST(@_h AS VARCHAR) + '_max, 0)
                           , @pCurrentAcc = ISNULL(x2d_' + CAST(@_h AS VARCHAR) + '_acc, 0)
                           , @pCurrentAvg = ISNULL(x2d_' + CAST(@_h AS VARCHAR) + '_avg, 0)
                           , @pCurrentNum = ISNULL(x2d_' + CAST(@_h AS VARCHAR) + '_num, 0)
                      FROM   H_' + @pTableId + '2D_' + @pEntityId + 'MH
                     WHERE   x2d_date       = ' + CAST(@_date_val AS VARCHAR) + '
                       AND   x2d_id         = ' + CAST(@pId AS VARCHAR) + '
                       AND   x2d_meter_id   = ' + CAST(@pMeterId AS VARCHAR) + '
                       AND   x2d_meter_item = ' + CAST(@pMeterItem AS VARCHAR) + '
                       AND   x2d_weekday    = ' + CAST(@_wk AS VARCHAR)

  EXEC sp_executesql @_get_sql,  N'@pCurrentMin INT OUTPUT, 
                                   @pCurrentMax INT OUTPUT, 
                                   @pCurrentAcc BIGINT OUTPUT, 
                                   @pCurrentAvg FLOAT OUTPUT, 
                                   @pCurrentNum BIGINT OUTPUT',
                                   @pCurrentMin OUTPUT,
                                   @pCurrentMax OUTPUT,
                                   @pCurrentAcc OUTPUT,
                                   @pCurrentAvg OUTPUT,
                                   @pCurrentNum OUTPUT

  ------------------------------------------------------------------------------------------------
  -- Check if meter data does not exist
  IF @@ROWCOUNT = 0 
   BEGIN

    -- Create Meter Data
    SET @_insert_sql = 'INSERT INTO H_' + @pTableId + '2D_' + @pEntityId + 'MH
		                         (x2d_date
		                         ,x2d_weekday
		                         ,x2d_id
		                         ,x2d_meter_id
		                         ,x2d_meter_item
		                       )
	                       VALUES
		                         (' + CAST(@_date_val AS VARCHAR) + '
		                         ,' + CAST(@_wk AS VARCHAR) + '
		                         ,' + CAST(@pId AS VARCHAR) + '
		                         ,' + CAST(@pMeterId AS VARCHAR) + '							  
		                         ,' + CAST(@pMeterItem AS VARCHAR) + '							  
		                        )'
    
    EXEC sys.sp_executesql @_insert_sql
    
   END

  ------------------------------------------------------------------------------------------------
  -- Set values data for update

  SET @pCurrentNum = @pNum
  SET @pCurrentMin = @pMin
  SET @pCurrentMax = @pMax
  SET @pCurrentAcc = @pAcc
  SET @pCurrentAvg = @pAvg

  ------------------------------------------------------------------------------------------------
  -- Update Meter data
  SET @_update_sql = 'UPDATE   H_' + @pTableId + '2D_' + @pEntityId + 'MH
						             SET   x2d_' + CAST(@_h AS VARCHAR) + '_min = ' + CAST(@pCurrentMin AS VARCHAR) + '
							               , x2d_' + CAST(@_h AS VARCHAR) + '_max = ' + CAST(@pCurrentMax AS VARCHAR) + '
							               , x2d_' + CAST(@_h AS VARCHAR) + '_acc = ' + CAST(@pCurrentAcc AS VARCHAR) + '
							               , x2d_' + CAST(@_h AS VARCHAR) + '_avg = ' + CAST(@pCurrentAvg AS VARCHAR) + '
							               , x2d_' + CAST(@_h AS VARCHAR) + '_num = ' + CAST(@pCurrentNum AS VARCHAR) + '							  
						           WHERE   x2d_date       = ' + CAST(@_date_val AS VARCHAR) + ' 
						             AND   x2d_id         = ' + CAST(@pId AS VARCHAR) + ' 
						             AND   x2d_meter_id   = ' + CAST(@pMeterId AS VARCHAR) + '
						             AND   x2d_meter_item = ' + CAST(@pMeteritem AS VARCHAR) + '
                         AND   x2d_weekday    = ' + CAST(@_wk AS VARCHAR)
                         
  EXEC sys.sp_executesql @_update_sql

END
GO

CREATE PROCEDURE SP_Update_Meter_Data
(
    @pTableId   CHAR(1)		  -- L (Life), Y (Year), M (Month), W (Week), T (Today)
  , @pEntityId  CHAR(1)		  -- T (Terminales), S (Site)
  , @pDate      DATETIME		
  , @pId        INT         -- Terminal ID or Site Id
  , @pMeterId   INT			    -- 1=CoinIn, 2= NetWin, 3=BetAvg, 4=Gender...
  , @pMeterItem INT         -- 0=Simple meter data, 1=Gender-Male, 2=Gender-female...
  , @pValue     DECIMAL      -- Value to aggregate
)
AS
BEGIN
          
  DECLARE @_date AS DATETIME          
  DECLARE @_date_val AS INT
  DECLARE @_wk AS INT
  DECLARE @_h AS VARCHAR(2)
          
  DECLARE @_get_sql NVARCHAR(MAX)    
  DECLARE @_insert_sql NVARCHAR(MAX)
  DECLARE @_update_sql NVARCHAR(MAX)

  DECLARE @pCurrentMin INT
  DECLARE @pCurrentMax BIGINT
  DECLARE @pCurrentAcc BIGINT
  DECLARE @pCurrentAvg DECIMAL
  DECLARE @pCurrentNum BIGINT

  SET @_date = dbo.SP_Get_Range_Date(@pDate, @pTableId, DEFAULT)

  SET @_date_val = CAST(CONVERT(VARCHAR(8), @_date, 112) AS INT)
  SET @_wk = DATEPART(WEEKDAY, @pDate)
  SET @_h = DATEPART(HOUR, @pDate)
  SET @_h = REPLICATE('0',2-LEN(RTRIM(@_h))) + RTRIM(@_h) 

  SET @pCurrentMin = 0
  SET @pCurrentMax = 0
  SET @pCurrentAcc = 0
  SET @pCurrentAvg = 0
  SET @pCurrentNum = 0

  ------------------------------------------------------------------------------------------------
  -- Obtain current meter data

  SET @_get_sql = ' SELECT   @pCurrentMin = ISNULL(x2d_' + CAST(@_h AS VARCHAR) + '_min, 0)
                           , @pCurrentMax = ISNULL(x2d_' + CAST(@_h AS VARCHAR) + '_max, 0)
                           , @pCurrentAcc = ISNULL(x2d_' + CAST(@_h AS VARCHAR) + '_acc, 0)
                           , @pCurrentAvg = ISNULL(x2d_' + CAST(@_h AS VARCHAR) + '_avg, 0)
                           , @pCurrentNum = ISNULL(x2d_' + CAST(@_h AS VARCHAR) + '_num, 0)
                      FROM   H_' + @pTableId + '2D_' + @pEntityId + 'MH
                     WHERE   x2d_date       = ' + CAST(@_date_val AS VARCHAR) + '
                       AND   x2d_id         = ' + CAST(@pId AS VARCHAR) + '
                       AND   x2d_meter_id   = ' + CAST(@pMeterId AS VARCHAR) + '
                       AND   x2d_meter_item = ' + CAST(@pMeterItem AS VARCHAR) + '
                       AND   x2d_weekday    = ' + CAST(@_wk AS VARCHAR)

  EXEC sp_executesql @_get_sql,  N'@pCurrentMin INT OUTPUT, 
                                   @pCurrentMax INT OUTPUT, 
                                   @pCurrentAcc BIGINT OUTPUT, 
                                   @pCurrentAvg FLOAT OUTPUT, 
                                   @pCurrentNum BIGINT OUTPUT',
                                   @pCurrentMin OUTPUT,
                                   @pCurrentMax OUTPUT,
                                   @pCurrentAcc OUTPUT,
                                   @pCurrentAvg OUTPUT,
                                   @pCurrentNum OUTPUT

  ------------------------------------------------------------------------------------------------
  -- Check if meter data does not exist
  IF @@ROWCOUNT = 0 
   BEGIN

    -- Create Meter Data
    SET @_insert_sql = 'INSERT INTO H_' + @pTableId + '2D_' + @pEntityId + 'MH
		                         (x2d_date
		                         ,x2d_weekday
		                         ,x2d_id
		                         ,x2d_meter_id
		                         ,x2d_meter_item
		                       )
	                       VALUES
		                         (' + CAST(@_date_val AS VARCHAR) + '
		                         ,' + CAST(@_wk AS VARCHAR) + '
		                         ,' + CAST(@pId AS VARCHAR) + '
		                         ,' + CAST(@pMeterId AS VARCHAR) + '							  
		                         ,' + CAST(@pMeterItem AS VARCHAR) + '							  
		                        )'
    
    EXEC sys.sp_executesql @_insert_sql
    
   END

  ------------------------------------------------------------------------------------------------
  -- Prepare data for update

  -- Increment number
  SET @pCurrentNum = @pCurrentNum + 1

  -- Detect Min value
  IF @pCurrentMin > @pValue
   BEGIN
    SET @pCurrentMin = @pValue
   END

  -- Detect Max value
  IF @pCurrentMax < @pValue
   BEGIN
    SET @pCurrentMax = @pValue
   END

  -- Accumulated calculation
  SET @pCurrentAcc = @pCurrentAcc + @pValue

  -- Average calculation
  SET @pCurrentAvg = @pCurrentAcc / CONVERT(DECIMAL(38,4), @pCurrentNum)

  ------------------------------------------------------------------------------------------------
  -- Update Meter data
  SET @_update_sql = 'UPDATE   H_' + @pTableId + '2D_' + @pEntityId + 'MH
						             SET   x2d_' + CAST(@_h AS VARCHAR) + '_min = ' + CAST(@pCurrentMin AS VARCHAR) + '
							               , x2d_' + CAST(@_h AS VARCHAR) + '_max = ' + CAST(@pCurrentMax AS VARCHAR) + '
							               , x2d_' + CAST(@_h AS VARCHAR) + '_acc = ' + CAST(@pCurrentAcc AS VARCHAR) + '
							               , x2d_' + CAST(@_h AS VARCHAR) + '_avg = ' + CAST(@pCurrentAvg AS VARCHAR) + '
							               , x2d_' + CAST(@_h AS VARCHAR) + '_num = ' + CAST(@pCurrentNum AS VARCHAR) + '							  
						           WHERE   x2d_date       = ' + CAST(@_date_val AS VARCHAR) + ' 
						             AND   x2d_id         = ' + CAST(@pId AS VARCHAR) + ' 
						             AND   x2d_meter_id   = ' + CAST(@pMeterId AS VARCHAR) + '
						             AND   x2d_meter_item = ' + CAST(@pMeteritem AS VARCHAR) + '
                         AND   x2d_weekday    = ' + CAST(@_wk AS VARCHAR)
                         
  EXEC sys.sp_executesql @_update_sql

END
GO

CREATE FUNCTION [dbo].[SP_Get_Range_Date] 
(  @pDate AS DATETIME
 , @pRange AS VARCHAR(1)
 , @pGetTime AS BIT = 0
)
RETURNS DATETIME
AS
BEGIN

  DECLARE @_time AS VARCHAR(8)

  IF (@pRange = 'T')
    BEGIN
      RETURN @pDate
    END

  -- Keep time
  IF @pGetTime = 0
    BEGIN
      SET @_time = ''
    END
  ELSE
    BEGIN
      SET @_time = CONVERT(VARCHAR(8), @pDate, 108)
    END
  

  IF (@pRange = 'W')
    BEGIN
      RETURN LEFT(CONVERT(nvarchar, DATEADD(WK, DATEDIFF(WK, 0, @pDate), 0), 120), 11) + @_time
    END

  IF (@pRange = 'M')
    BEGIN
      RETURN LEFT(CONVERT(nvarchar, DATEADD(M, DATEDIFF(M, 0, @pDate), 0), 120), 11) + @_time
    END

  IF (@pRange = 'Y')
    BEGIN
      RETURN LEFT(CONVERT(nvarchar, DATEADD(YY, DATEDIFF(YY, 0, @pDate), 0), 120), 11) + @_time
    END

  --IF (@pRange = 'L')
  --  BEGIN
  --    RETURN ?
  --  END
    
  --SET @DATEFROM_WEEK = LEFT(CONVERT(nvarchar, @DATEFROM_WEEK, 120), 11) + convert(char(8), @_DATE, 108)  

  --SELECT 
  --   @pDate
  -- , DATEDIFF(WK, 0, @pDate)
  -- , DATEADD(WK, DATEDIFF(WK, 0, @pDate) - 1, 0)
  -- , CONVERT(VARCHAR(10), DATEADD(WK, DATEDIFF(WK, 0, @pDate) - 1, 0), 112)
 
 RETURN @pDate
 
END 
GO

