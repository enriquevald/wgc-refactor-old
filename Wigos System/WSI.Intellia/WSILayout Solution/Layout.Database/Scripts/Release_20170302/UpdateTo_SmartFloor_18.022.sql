﻿USE WGDB_000;

SET ANSI_NULLS ON;

----------------------------------------------------
-----------------VERSION CONTROL--------------------
----------------------------------------------------

DECLARE 
@Exp_SmartFloor_ClientId int, 
@Exp_SmartFloor_CommonBuildId int,
@Exp_SmartFloor_ClientBuildId int,
@New_SmartFloor_ReleaseId int,
@New_SmartFloor_ScriptName nvarchar(50),
@New_SmartFloor_Description nvarchar(1000);

SET @Exp_SmartFloor_ClientId = 18;
SET @Exp_SmartFloor_CommonBuildId = 100;
SET @Exp_SmartFloor_ClientBuildId = 1;
SET @New_SmartFloor_ReleaseId = 22;
SET @New_SmartFloor_ScriptName = N'UpdateTo_SmartFloor_18.022.sql';
SET @New_SmartFloor_Description = N'Patch 18.022 - 21';

IF EXISTS ( SELECT 1 FROM DB_VERSION_SMARTFLOOR )
 DELETE FROM DB_VERSION_SMARTFLOOR;

INSERT INTO [dbo].[db_version_smartfloor]
           ([db_client_id]
           ,[db_common_build_id]
           ,[db_client_build_id]
           ,[db_release_id]
           ,[db_updated_script]
           ,[db_updated]
           ,[db_description])
     VALUES
           (@Exp_SmartFloor_ClientId
           ,@Exp_SmartFloor_CommonBuildId
           ,@Exp_SmartFloor_ClientBuildId
           ,@New_SmartFloor_ReleaseId
           ,@New_SmartFloor_ScriptName
           ,Getdate()
           ,@New_SmartFloor_Description);

          

----------------------------------------------------
----------------------TABLES------------------------
----------------------------------------------------


IF NOT EXISTS(SELECT *  FROM sys.columns  WHERE Name = N'LC_PROFILE_UPDATE' AND Object_ID = Object_ID(N'LAYOUT_USERS_CONFIGURATION'))
BEGIN
  ALTER TABLE LAYOUT_USERS_CONFIGURATION
        ADD  LC_PROFILE_UPDATE            varchar(50)      NULL
END

----------------------------------------------------
----------------------DATA--------------------------
----------------------------------------------------
GO
IF NOT EXISTS (SELECT * FROM layout_ranges_legends WHERE lrl_range_id=5 AND lrl_value1=0.00 AND lrl_value2=0.00)
INSERT INTO layout_ranges_legends (LRL_RANGE_ID,LRL_LABEL,LRL_VALUE1,LRL_VALUE2,LRL_OPERATOR,LRL_COLOR,LRL_EDITABLE,LRL_AUTO_ASSIGN_ROLE,LRL_AUTO_ASSIGN_PRIORITY)
VALUES (5,'Unknown',0.00,0.00,'<','#B3B3B3',1,-1,0)

GO
----------------------------------------------------
----------------------STOREDS-----------------------
----------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_Get_Meters]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_Get_Meters]

GO

CREATE  PROCEDURE [dbo].[SP_Get_Meters] 
(  @Device AS INT
 , @Source AS VARCHAR(1)
 , @Date AS DATETIME
) 
AS
BEGIN
  DECLARE @DATEFROM AS DATETIME
  DECLARE @DATEFROM_WEEK AS DATETIME
  DECLARE @DATEFROM_MONTH AS DATETIME
  DECLARE @DATEFROM_YEAR AS DATETIME
  DECLARE @SEARCH_DATE AS NVARCHAR(MAX)
  DECLARE @QUERY_CONST AS NVARCHAR(MAX)
  DECLARE @QUERY_TEMP AS NVARCHAR(MAX)
  DECLARE @FINAL_QUERY AS NVARCHAR(MAX)
  DECLARE @QUERY AS NVARCHAR(MAX)
  DECLARE @RANGE AS VARCHAR(1)
  DECLARE @DESCRIPTION AS VARCHAR(3)
  DECLARE @HOUR AS VARCHAR(2)
  DECLARE @ORIGIN AS VARCHAR(MAX)
  DECLARE @DOW AS VARCHAR(1)
  DECLARE @LHOUR AS VARCHAR(2)

  SET @FINAL_QUERY = ''
  SET @DATEFROM = @Date

  EXEC SP_Get_Meters_List @DEVICE

  SET @DATEFROM_WEEK = DATEADD(DAY, -7, dbo.GetRangeDate(@DATEFROM , 'W', 1))             -- First day of previous week
  SET @DATEFROM_MONTH = DATEADD(M, -1, DATEADD(M, DATEDIFF(M, 0, @DATEFROM), 0))  -- First day of previous month
  SET @DATEFROM_YEAR = DATEADD(YY, -1, dbo.GetRangeDate(@DATEFROM , 'Y',1)) -- First day of previous year

  SET @DOW = LTRIM(DATEPART(WEEKDAY, @DATEFROM))
  SET @HOUR = DATEPART(HOUR, GETDATE())
  SET @HOUR = REPLICATE('0',2-LEN(RTRIM(@HOUR))) + RTRIM(@HOUR) 

  SET @LHOUR = DATEPART(HOUR, DATEADD(HOUR,-1,GETDATE()))
  SET @LHOUR = REPLICATE('0',2-LEN(RTRIM(@LHOUR))) + RTRIM(@LHOUR) 
  
  SET @QUERY_CONST = ' SELECT ''@DESCRIPTION'' AS RANGE, @HOUR as HOUR, x2d_date, x2d_weekday, x2d_id, x2d_meter_id, x2d_meter_item,'
                   + ' x2d_@HOUR_min AS MIN, x2d_@HOUR_max as MAX, x2d_@HOUR_acc AS ACC, x2d_@HOUR_avg AS AVG, x2d_@HOUR_num AS NUM,'
                   + ' x2d_dd_min AS DAY_MIN, x2d_dd_max AS DAY_MAX, x2d_dd_acc AS DAY_ACC, x2d_dd_avg AS DAY_AVG, x2d_dd_num AS DAY_NUM'
                   + ' FROM  H_@RANGE2D_@SOURCEMH'
                   + ' INNER JOIN H_METERS_DEFINITION ON x2d_meter_id = hmd_meter_id'
                   + ' WHERE hmd_visible = 1 AND @DATE '
                

  -- TDA & SLW
  SET @QUERY_TEMP = @QUERY_CONST
  SET @RANGE = 'T'
  SET @DESCRIPTION = 'TDA'
  SET @SEARCH_DATE = 'x2d_date >= ' + CONVERT(VARCHAR(10), DATEADD(DAY, -7, @DATEFROM), 112) + ' AND x2d_date <= ' + CONVERT(VARCHAR(10), @DATEFROM, 112)
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DATE', @SEARCH_DATE)
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@HOUR', LTRIM(@HOUR))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@RANGE', LTRIM(@RANGE))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DESCRIPTION', LTRIM(@DESCRIPTION))
  SET @FINAL_QUERY = @QUERY_TEMP + ' AND x2d_weekday = ' + @DOW


  SET @QUERY_TEMP = @QUERY_CONST
  SET @RANGE = 'T'
  SET @DESCRIPTION = 'TDLH'
  
  IF dbo.TodayOpening(1) <> @DATEFROM
  BEGIN
    SET @SEARCH_DATE = 'x2d_date = ' + CONVERT(VARCHAR(10), @DATEFROM, 112)
    SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DATE', @SEARCH_DATE)
    SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@HOUR', LTRIM(@LHOUR))
    SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@RANGE', LTRIM(@RANGE))
    SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DESCRIPTION', LTRIM(@DESCRIPTION))
    SET @FINAL_QUERY = @FINAL_QUERY
                     + ' UNION '
                     + @QUERY_TEMP 
  END

 -- YESTERDAY new block with the query on the days table with the meters from yesterday
  SET @QUERY_TEMP = @QUERY_CONST
  SET @RANGE = 'T'
  SET @DESCRIPTION = 'TDY'
  SET @SEARCH_DATE = 'x2d_date = ' + CONVERT(VARCHAR(10), DATEADD(DAY, -1, @DATEFROM), 112)
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DATE', @SEARCH_DATE)
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@HOUR', LTRIM(@HOUR))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@RANGE', LTRIM(@RANGE))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DESCRIPTION', LTRIM(@DESCRIPTION))
  SET @FINAL_QUERY = @FINAL_QUERY
                   + ' UNION '
                   + @QUERY_TEMP 

  -- SAME DAY LAST WEEK new block with the query on the days table with the meters from the same day of last week
  SET @QUERY_TEMP = @QUERY_CONST
  SET @RANGE = 'T'
  SET @DESCRIPTION = 'TDW'
  SET @SEARCH_DATE = 'x2d_date = ' + CONVERT(VARCHAR(10), DATEADD(DAY, -7, @DATEFROM), 112)
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DATE', @SEARCH_DATE)
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@HOUR', LTRIM(@HOUR))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@RANGE', LTRIM(@RANGE))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DESCRIPTION', LTRIM(@DESCRIPTION))
  SET @FINAL_QUERY = @FINAL_QUERY
                   + ' UNION '
                   + @QUERY_TEMP 

  -- WTD
  SET @QUERY_TEMP = @QUERY_CONST
  SET @RANGE = 'W'
  SET @DESCRIPTION = 'WTD'
  SET @SEARCH_DATE = 'x2d_date = ' + CAST(CONVERT(VARCHAR(10), @DATEFROM_WEEK, 112) AS VARCHAR)
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DATE', LTRIM(@SEARCH_DATE))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@HOUR', LTRIM(@HOUR))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@RANGE', LTRIM(@RANGE))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DESCRIPTION', LTRIM(@DESCRIPTION))
  --EXECUTE sp_executesql @QUERY
  SET @FINAL_QUERY = @FINAL_QUERY
                   + ' UNION '
                   + @QUERY_TEMP + ' AND x2d_weekday = ' + @DOW
                  

  -- MTD
  SET @QUERY_TEMP = @QUERY_CONST
  SET @RANGE = 'M'
  SET @DESCRIPTION = 'MTD'
  SET @SEARCH_DATE = 'x2d_date = ' + CAST(CONVERT(VARCHAR(10), @DATEFROM_MONTH, 112) AS VARCHAR)
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DATE', LTRIM(@SEARCH_DATE))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@HOUR', LTRIM(@HOUR))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@RANGE', LTRIM(@RANGE))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DESCRIPTION', LTRIM(@DESCRIPTION))
  SET @FINAL_QUERY = @FINAL_QUERY
                   + ' UNION '
                   + @QUERY_TEMP + ' AND x2d_weekday = ' + @DOW
                   

  -- YTD
  SET @QUERY_TEMP = @QUERY_CONST
  SET @RANGE = 'Y'
  SET @DESCRIPTION = 'YTD'
  SET @SEARCH_DATE = 'x2d_date = ' + CAST(CONVERT(VARCHAR(10), @DATEFROM_YEAR, 112) AS VARCHAR)
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DATE', LTRIM(@SEARCH_DATE))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@HOUR', LTRIM(@HOUR))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@RANGE', LTRIM(@RANGE))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DESCRIPTION', LTRIM(@DESCRIPTION))
  SET @FINAL_QUERY = @FINAL_QUERY
                   + ' UNION '
                   + @QUERY_TEMP + ' AND x2d_weekday = ' + @DOW
                  

  SET @QUERY = REPLACE(@FINAL_QUERY, '@SOURCE', @Source)
           print @QUERY  
  EXECUTE sp_executesql @QUERY
  
  --WEATHER METERS-------
  --LAST 7 DAYS PROMOTIONS,OCCUPANCY AND WEATHER
  SELECT TOP 7 H_T2D_SMH.X2D_DATE AS DATE
           ,  H_T2D_SMH.X2D_DD_NUM AS PROMOTIONS
           ,  O.X2D_DD_AVG AS OCCUPANCY
           ,  W.LWD_WEATHER_CODE AS WEATHER
           ,  H_T2D_SMH.X2D_WEEKDAY
  FROM H_T2D_SMH
        LEFT JOIN (
          SELECT TOP 7 X2D_DATE, X2D_DD_AVG
                FROM H_T2D_SMH
               WHERE X2D_METER_ID = 4 AND X2D_METER_ITEM = 0
               ORDER BY X2D_DATE DESC
                   ) O ON O.X2D_DATE = H_T2D_SMH.X2D_DATE
       INNER JOIN (
          SELECT TOP 7 LWD_DATE,LWD_WEATHER_CODE 
                FROM LAYOUT_WEATHER_DIARY
                ORDER BY LWD_DATE DESC
                   ) W ON H_T2D_SMH.X2D_DATE = W.LWD_DATE
       WHERE H_T2D_SMH.X2D_METER_ID = 8 AND H_T2D_SMH.X2D_METER_ITEM = 2

END

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetConfiguration]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].Layout_GetConfiguration
GO
CREATE PROCEDURE [dbo].[Layout_GetConfiguration]
	@pUserId INT
AS
BEGIN

      SELECT   LC_PARAMETERS
             , LC_DASHBOARD
             , LC_PROFILE_UPDATE
        FROM   LAYOUT_USERS_CONFIGURATION
       WHERE   LC_USER_ID = @pUserId
	  
END -- Layout_GetConfiguration
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_SaveConfiguration]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].Layout_SaveConfiguration
GO
CREATE PROCEDURE [dbo].[Layout_SaveConfiguration]
      @pUserId INT
    , @pLastUpdate DATETIME = NULL
    , @pParameters VARCHAR(MAX)
    , @pDashboard VARCHAR(MAX) = NULL
    , @pLastProfileUpdate VARCHAR(50)
AS
BEGIN



      IF EXISTS (SELECT TOP 1 * FROM LAYOUT_USERS_CONFIGURATION WHERE LC_USER_ID = @pUserId)
            UPDATE	LAYOUT_USERS_CONFIGURATION 
               SET	LC_LAST_UPDATE = @pLastUpdate
                 ,	LC_PARAMETERS = @pParameters
                 ,  LC_DASHBOARD = ISNULL(@pDashboard, NULL)
                 ,  LC_PROFILE_UPDATE = @pLastProfileUpdate 
             WHERE	LC_USER_ID = @pUserId

      ELSE

       INSERT INTO	LAYOUT_USERS_CONFIGURATION
	   ([lc_user_id]
      ,[lc_parameters]
      ,[lc_dashboard]
      ,[lc_last_update]
      ,[lc_is_manager]
      ,[lc_is_runner]
      ,LC_PROFILE_UPDATE
	  )
            VALUES 
				 (	@pUserId
                 ,	@pParameters
                 ,  ISNULL(@pDashboard, NULL)
                 ,	ISNULL(@pLastUpdate, GETDATE())
                 ,  0
				 ,  0---atencion revisar que este campo es nuevo y no estava previsto en el SP  SS 03-12-2015
         ,  @pLastProfileUpdate
				 ) 
      
END --Layout_SaveConfiguration
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetIsUpdateProfile]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].Layout_GetIsUpdateProfile

GO
CREATE PROCEDURE [dbo].Layout_GetIsUpdateProfile
      @pUserId INT
AS
BEGIN
      
SELECT        GUP_TIMESTAMP
  FROM        GUI_USER_PROFILES
  INNER JOIN  GUI_USERS ON  GUI_USERS.GU_PROFILE_ID=GUI_USER_PROFILES.GUP_PROFILE_ID
  WHERE       GUI_USERS.GU_USER_ID =  @PUSERID     
END -- Layout_GetIsUpdateProfile

