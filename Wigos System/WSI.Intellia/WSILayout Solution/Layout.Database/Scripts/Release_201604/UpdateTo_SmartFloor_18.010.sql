----------------------------------------------------
-----------------VERSION CONTROL--------------------
----------------------------------------------------

DECLARE 
@Exp_SmartFloor_ClientId int, 
@Exp_SmartFloor_CommonBuildId int,
@Exp_SmartFloor_ClientBuildId int,
@New_SmartFloor_ReleaseId int,
@New_SmartFloor_ScriptName nvarchar(50),
@New_SmartFloor_Description nvarchar(1000);

SET @Exp_SmartFloor_ClientId = 18;
SET @Exp_SmartFloor_CommonBuildId = 100;
SET @Exp_SmartFloor_ClientBuildId = 1;
SET @New_SmartFloor_ReleaseId = 10;
SET @New_SmartFloor_ScriptName = N'UpdateTo_SmartFloor_18.010.sql';
SET @New_SmartFloor_Description = N'Patch 18.010 - 09';

IF EXISTS ( SELECT 1 FROM DB_VERSION_SMARTFLOOR )
 DELETE FROM DB_VERSION_SMARTFLOOR;

INSERT INTO [dbo].[db_version_smartfloor]
           ([db_client_id]
           ,[db_common_build_id]
           ,[db_client_build_id]
           ,[db_release_id]
           ,[db_updated_script]
           ,[db_updated]
           ,[db_description])
     VALUES
           (@Exp_SmartFloor_ClientId
           ,@Exp_SmartFloor_CommonBuildId
           ,@Exp_SmartFloor_ClientBuildId
           ,@New_SmartFloor_ReleaseId
           ,@New_SmartFloor_ScriptName
           ,Getdate()
           ,@New_SmartFloor_Description);


GO
           
----------------------------------------------------
----------------------STOREDS-----------------------
----------------------------------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetPlayerInfo]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[Layout_GetPlayerInfo]
GO

CREATE PROCEDURE [dbo].[Layout_GetPlayerInfo]
      @AccountId INT
      ,@Date DATETIME
AS
BEGIN
  DECLARE @NLastVisits INT
  SET @NLastVisits = 3

  SELECT T.PS_PLAYED_COUNT
       , T.PS_PLAYED_AMOUNT
       ,(T.PS_PLAYED_AMOUNT/T.PS_PLAYED_COUNT) AS BET_AVG
       , T.PS_WON_AMOUNT
       ,(T.PS_WON_AMOUNT-T.PS_PLAYED_AMOUNT) AS PLAYED_NET_WIN
       , PS_CASH_IN
       , PS_CASH_OUT
  FROM 
  (
    SELECT SUM(PS_PLAYED_COUNT) AS PS_PLAYED_COUNT
        , SUM(PS_PLAYED_AMOUNT) AS PS_PLAYED_AMOUNT
        , SUM(PS_WON_AMOUNT) AS PS_WON_AMOUNT
        , SUM(PS_CASH_IN) AS PS_CASH_IN
        , SUM(PS_CASH_OUT) AS PS_CASH_OUT
    FROM PLAY_SESSIONS
    WHERE PS_ACCOUNT_ID = @AccountId
    AND PS_FINISHED > @Date
    and ps_played_count>0
   ) T

  SELECT TOP(@NLastVisits) 
           PVH_TOTAL_PLAYED_COUNT
         , PVH_TOTAL_PLAYED
         , PVH_TOTAL_WON
         , PVH_TOTAL_BET_AVG  
         , PVH_TOTAL_WON-PVH_TOTAL_PLAYED AS PVH_NETWIN
         , PVH_MONEY_IN
         , PVH_MONEY_OUT
      FROM H_PVH
     WHERE PVH_ACCOUNT_ID = @AccountId
  ORDER BY pvh_date DESC 
END

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetActivity]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[Layout_GetActivity]
GO

CREATE PROCEDURE [dbo].[Layout_GetActivity]
AS
BEGIN

  DECLARE @terminal_name_type AS VARCHAR(50)
      DECLARE @big_won_limit AS MONEY
      CREATE TABLE #temp_table    (LO_ID INT
                                             , TERMINAL_ID INT
                                             , TERMINAL_STATUS INT
                                             , PLAY_SESSION_STATUS INT
                                             , IS_ANONYMOUS INT
                                             , PLAYER_ACCOUNT_ID INT
                                             , PLAYER_NAME VARCHAR(50)
                                             , PLAYER_GENDER INT
                                             , PLAYER_AGE INT
                                             , PLAYER_LEVEL INT
                                             , PLAYER_IS_VIP INT
                                             , AL_SAS_HOST_ERROR INT
                                             , AL_DOOR_FLAGS INT 
                                             , AL_BILL_FLAGS INT
                                             , AL_PRINTER_FLAGS INT
                                             , AL_EGM_FLAGS INT
                                             , AL_PLAYED_WON_FLAGS INT
                                             , AL_JACKPOT_FLAGS INT
                                             , AL_STACKER_STATUS INT
                                             , AL_CALL_ATTENDANT_FLAGS INT
                                             , AL_MACHINE_FLAGS INT           
                                             , AL_BIG_WON BIT           
                                             , PLAYED_AMOUNT MONEY
                                             , WON_AMOUNT MONEY
                                             , PLAY_SESSION_BET_AVERAGE MONEY
                                             , TERMINAL_BET_AVERAGE MONEY
                                             , PLAY_SESSION_DURATION INT
                                             , PLAY_SESSION_TOTAL_PLAYED MONEY
                                             , PLAY_SESSION_PLAYED_COUNT INT
                                             , TERMINAL_BANK_ID INT
                                             , TERMINAL_AREA_ID INT
                                             , TERMINAL_PROVIDER_NAME VARCHAR(50)
                                             , PLAY_SESSION_WON_AMOUNT MONEY
                                             , NET_WIN MONEY
                                             , PLAY_SESSION_NET_WIN MONEY
                                             , TERMINAL_NAME NVARCHAR(50)
                                             , TERMINAL_PLAYED_COUNT INT
                                             , PLAYER_TRACKDATA NVARCHAR(50)
                                             , PLAYER_LEVEL_NAME NVARCHAR(50)
                 , ALARMS NVARCHAR(MAX)
                 , TERMINAL_DENOMINATION MONEY
                                             , PLAYER_CREATED INT
                 , PS_rate float  -- Test field
                 , TERMINAL_PAYOUT MONEY
                 , THEORETICAL_HOLD MONEY
                 , AC_HOLDER_BIRTH_DATE DATETIME
                 , PLAY_SESSION_CASH_IN MONEY
                 , PLAY_SESSION_CASH_OUT MONEY
                                             )
                                             
      -- TERMINAL NAME TYPE
      SELECT @terminal_name_type = GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'SasHost' AND GP_SUBJECT_KEY = 'DisplayName.Format'
      IF (@terminal_name_type IS NULL)
      BEGIN
        SET @terminal_name_type = '%Name'
      END

      -- TODO: Read from GP
      SET @big_won_limit = 1000

  DECLARE @Now AS DATETIME
  DECLARE @CurrentHour AS INT
  DECLARE @HourNow AS DATETIME
  DECLARE @NextHourNow AS DATETIME
  
  SET @Now = GETDATE()
  SET @CurrentHour = DATEPART(HOUR, @Now)
  SET @HourNow = DATEADD(HOUR, DATEDIFF(HOUR, 0, @Now), 0)
  SET @NextHourNow = DATEADD(HOUR, 1, @HourNow)

  DECLARE @DefaultHold MONEY
  SET @DefaultHold = (SELECT   1 - CAST (GP_KEY_VALUE AS MONEY) / 100        
                        FROM   GENERAL_PARAMS                                
                       WHERE   GP_GROUP_KEY   = 'PlayerTracking'               
                         AND   GP_SUBJECT_KEY = 'TerminalDefaultPayout')

      INSERT INTO #temp_table
      SELECT LO.LO_ID
      
            -- Terminal info
            , LO.LO_EXTERNAL_ID AS TERMINAL_ID
            , TE.TE_STATUS   AS TERMINAL_STATUS

            -- Play Session info
            , PLAY_SESSION_STATUS = CASE  WHEN (PS_PLAY_SESSION_ID IS NOT NULL) THEN 2  
                                                 ELSE 1 END

            -- Player info
            , IS_ANONYMOUS = CASE  WHEN (TE.TE_CURRENT_ACCOUNT_ID IS NOT NULL) THEN CASE WHEN (AC.AC_HOLDER_NAME IS NOT NULL) THEN 0 ELSE 1 END 
                                                ELSE NULL END
            , PS.PS_ACCOUNT_ID AS PLAYER_ACCOUNT_ID
            , PLAYER_NAME = CASE WHEN (TE.TE_CURRENT_ACCOUNT_ID IS NOT NULL) THEN ISNULL(AC.AC_HOLDER_NAME, 'Anonymous')
                                          ELSE NULL END
            , AC.AC_HOLDER_GENDER AS PLAYER_GENDER
            , DATEDIFF(YEAR, AC.AC_HOLDER_BIRTH_DATE, @Now) AS PLAYER_AGE
            , PLAYER_LEVEL = CASE  WHEN (TE.TE_CURRENT_ACCOUNT_ID IS NOT NULL) THEN CASE WHEN (AC.AC_HOLDER_NAME IS NOT NULL) THEN AC.AC_HOLDER_LEVEL ELSE -1 END 
                                                ELSE 99 END
            , ISNULL(AC.AC_HOLDER_IS_VIP, 0) AS PLAYER_IS_VIP
            
             -- Flags
            , TS_SAS_HOST_ERROR AS AL_SAS_HOST_ERROR
            , ISNULL(TS_DOOR_FLAGS, 0) AS AL_DOOR_FLAGS 
             , ISNULL(TS_BILL_FLAGS, 0) AS AL_BILL_FLAGS
            , ISNULL(TS_PRINTER_FLAGS, 0) AS AL_PRINTER_FLAGS
            , ISNULL(TS_EGM_FLAGS, 0) AS AL_EGM_FLAGS
            , ISNULL(TS_PLAYED_WON_FLAGS, 0) AS AL_PLAYED_WON_FLAGS
            , ISNULL(TS_JACKPOT_FLAGS, 0) AS AL_JACKPOT_FLAGS
            , ISNULL(TS_STACKER_STATUS, 0) AS AL_STACKER_STATUS
            , ISNULL(TS_CALL_ATTENDANT_FLAGS, 0) AS AL_CALL_ATTENDANT_FLAGS
            , ISNULL(TS_MACHINE_FLAGS, 0) AS AL_MACHINE_FLAGS          
            , AL_BIG_WON = CASE WHEN ISNULL(SPH.WON_AMOUNT, 0) > @big_won_limit THEN 1
                                     ELSE 0 END
                                     
             -- Money info
            , ISNULL(SPH.PLAYED_AMOUNT , 0) AS PLAYED_AMOUNT
            , ISNULL(SPH.WON_AMOUNT, 0) AS WON_AMOUNT
            , PLAY_SESSION_BET_AVERAGE = CASE WHEN (PS.PS_PLAYED_COUNT > 0) THEN ROUND((PS.PS_TOTAL_PLAYED / PS.PS_PLAYED_COUNT), 2)
                                                               ELSE NULL END
            , TERMINAL_BET_AVERAGE = CASE WHEN (SPH.PLAYED_COUNT > 0) THEN ROUND((SPH.PLAYED_AMOUNT / SPH.PLAYED_COUNT), 2)
                                                         ELSE NULL END
            , DATEDIFF(SECOND, ISNULL(PS.PS_STARTED, @Now), @Now) AS PLAY_SESSION_DURATION
            , PS.PS_TOTAL_PLAYED AS PLAY_SESSION_TOTAL_PLAYED
            , PS.PS_PLAYED_COUNT AS PLAY_SESSION_PLAYED_COUNT
            
             -- Area & Bank
            , BK.bk_bank_id AS TERMINAL_BANK_ID
            , BK.bk_area_id AS TERMINAL_AREA_ID
            , PR.pv_name AS TERMINAL_PROVIDER_NAME
            
             , PS.PS_WON_AMOUNT AS PLAY_SESSION_WON_AMOUNT
            
             , SPH.PLAYED_AMOUNT - SPH.WON_AMOUNT AS NET_WIN
            , PS.PS_TOTAL_PLAYED - PS.PS_WON_AMOUNT AS PLAY_SESSION_NET_WIN
            --, TE.TE_NAME AS TERMINAL_NAME
                 , CASE WHEN @terminal_name_type = '%FloorId' THEN
                      TE.TE_NAME + ' [' + TE.TE_FLOOR_ID + ']'
                    WHEN @terminal_name_type = '%BaseName' THEN
                      -- TODO: Find correct field
                      -- TE.TE_BASE_NAME
                      TE.TE_NAME
                    ELSE 
                      TE.TE_NAME
               END AS TERMINAL_NAME
            , SPH.PLAYED_COUNT AS TERMINAL_PLAYED_COUNT
            , AC.AC_TRACK_DATA AS PLAYER_TRACKDATA
            , ISNULL(LEVELS.NAME,'') AS PLAYER_LEVEL_NAME
     , NULL AS ALARMS
     , TE.TE_DENOMINATION AS TERMINAL_DENOMINATION
     , CASE WHEN (AC.AC_CREATED >= @HourNow AND AC.AC_CREATED < @NextHourNow) THEN 1 ELSE 0 END AS PLAYER_CREATED
     , NULL AS PS_RATE
     , ISNULL(TE.TE_THEORETICAL_HOLD, @DefaultHold) AS TERMINAL_PAYOUT
     , (PS.PS_TOTAL_PLAYED * ISNULL(TE.TE_THEORETICAL_HOLD, @DefaultHold)) AS THEORETICAL_HOLD
     , AC.AC_HOLDER_BIRTH_DATE
     , PS.PS_TOTAL_CASH_IN
     , PS.PS_CASH_OUT
        FROM LAYOUT_OBJECTS AS LO
        
            INNER JOIN TERMINALS AS TE ON TE.TE_TERMINAL_ID = LO.LO_EXTERNAL_ID 
            INNER JOIN PROVIDERS AS PR ON PR.pv_id = TE.te_prov_id
            
            LEFT JOIN BANKS AS BK ON BK.bk_bank_id = TE.te_bank_id
                        
            LEFT JOIN (SELECT PS_TERMINAL_ID
                            , MAX(PS_PLAY_SESSION_ID) AS PS_PLAY_SESSION_ID
                            , PS_ACCOUNT_ID
                            , PS_TOTAL_PLAYED
                            , PS_WON_AMOUNT
                            , PS_PLAYED_COUNT
                            , PS_STARTED
                            , PS_PLAYED_AMOUNT
                            , PS_TOTAL_CASH_IN
                            , PS_CASH_OUT
                             FROM PLAY_SESSIONS
                            WHERE PS_STATUS = 0
                        GROUP BY PS_TERMINAL_ID, PS_ACCOUNT_ID, PS_TOTAL_PLAYED, PS_WON_AMOUNT, PS_PLAYED_COUNT, PS_PLAYED_AMOUNT, PS_STARTED,PS_TOTAL_CASH_IN,PS_CASH_OUT) AS PS ON TE.TE_TERMINAL_ID = PS.PS_TERMINAL_ID
                        
            LEFT JOIN ACCOUNTS AS AC ON AC.AC_ACCOUNT_ID = TE.TE_CURRENT_ACCOUNT_ID
            
            LEFT JOIN TERMINAL_STATUS AS TS ON TE.TE_TERMINAL_ID = TS.TS_TERMINAL_ID
            
            LEFT JOIN (SELECT SPH_TERMINAL_ID 
                                  , SUM(SPH_PLAYED_AMOUNT)AS PLAYED_AMOUNT
                                  , SUM(SPH_WON_AMOUNT) AS WON_AMOUNT
                                  , SUM(SPH_PLAYED_COUNT) AS PLAYED_COUNT
                         FROM SALES_PER_HOUR_V2
                        WHERE SPH_BASE_HOUR >= DATEADD(DAY, DATEDIFF(DAY, 0, @Now), 0) 
                            AND SPH_BASE_HOUR <= @Now
                       GROUP BY SPH_TERMINAL_ID) AS SPH ON SPH.SPH_TERMINAL_ID = TE.TE_TERMINAL_ID
                       
            LEFT JOIN (SELECT CAST(REPLACE(REPLACE(GP_SUBJECT_KEY,'Level',''),'.Name','') AS INT) AS NUM
                                  , GP_KEY_VALUE AS NAME
                              FROM GENERAL_PARAMS 
                              WHERE GP_GROUP_KEY ='PlayerTracking'
                                AND GP_SUBJECT_KEY LIKE 'Level%.Name'
                          ) AS LEVELS ON LEVELS.NUM = (CASE  WHEN (TE.TE_CURRENT_ACCOUNT_ID IS NOT NULL) THEN 
                                                                                                                  CASE WHEN (AC.AC_HOLDER_NAME IS NOT NULL) THEN AC.AC_HOLDER_LEVEL 
                                                                                                                  ELSE -1 END 
                                                                                                            ELSE 0 END)
            
      WHERE LO.LO_TYPE = 1
                       
      -- TERMINAL group
      SELECT * FROM #temp_table
      
      -- BANK group
      SELECT TERMINAL_BANK_ID AS BANK_ID
           , SUM(PLAY_SESSION_STATUS - 1) AS NUM_ACTIVE_PLAY_SESSIONS
            , CASE WHEN (SUM(PLAY_SESSION_STATUS - 1) - SUM(IS_ANONYMOUS)) > 0 THEN 100 - ((SUM(PLAYER_GENDER - 1) * 100)/ (SUM(PLAY_SESSION_STATUS - 1) - SUM(IS_ANONYMOUS)))
                        ELSE NULL END AS PLAYERS_MALE_PERCENTAGE
            , CASE WHEN (SUM(PLAY_SESSION_STATUS - 1) - SUM(IS_ANONYMOUS)) > 0 THEN ((SUM(PLAYER_GENDER - 1) * 100)/ (SUM(PLAY_SESSION_STATUS - 1) - SUM(IS_ANONYMOUS)))
                        ELSE NULL END AS PLAYERS_FEMALE_PERCENTAGE
            , CASE WHEN (SUM(PLAY_SESSION_STATUS - 1) - SUM(IS_ANONYMOUS)) > 0 THEN (SUM(PLAYER_AGE) / (SUM(PLAY_SESSION_STATUS - 1) - SUM(IS_ANONYMOUS)))
                        ELSE NULL END AS PLAYERS_AGE_AVERAGE
            , SUM(PLAYER_IS_VIP) AS NUM_VIPS
            , SUM(PLAYED_AMOUNT) AS PLAYED_AMOUNT
            , SUM(WON_AMOUNT) AS WON_AMOUNT
            , CASE WHEN (SUM(PLAY_SESSION_STATUS - 1) - SUM(IS_ANONYMOUS)) > 0 THEN ROUND((SUM(PLAY_SESSION_BET_AVERAGE) / (SUM(PLAY_SESSION_STATUS - 1) - SUM(IS_ANONYMOUS))), 2)
                        ELSE NULL END  AS PLAY_SESSIONS_BET_AVERAGE
            , CASE WHEN COUNT(TERMINAL_ID) > 0 THEN ROUND((SUM(TERMINAL_BET_AVERAGE) / COUNT(TERMINAL_ID)), 2) 
                    ELSE NULL END AS TERMINALS_BET_AVERAGE
        FROM #temp_table 
  GROUP BY TERMINAL_BANK_ID
      
      -- AREA group
      SELECT TERMINAL_AREA_ID AS AREA_ID
            , SUM(PLAY_SESSION_STATUS - 1) AS NUM_ACTIVE_PLAY_SESSIONS
            , CASE WHEN (SUM(PLAY_SESSION_STATUS - 1) - SUM(IS_ANONYMOUS)) > 0 THEN 100 - ((SUM(PLAYER_GENDER - 1) * 100)/ (SUM(PLAY_SESSION_STATUS - 1) - SUM(IS_ANONYMOUS)))
                        ELSE NULL END AS PLAYERS_MALE_PERCENTAGE
            , CASE WHEN (SUM(PLAY_SESSION_STATUS - 1) - SUM(IS_ANONYMOUS)) > 0 THEN ((SUM(PLAYER_GENDER - 1) * 100)/ (SUM(PLAY_SESSION_STATUS - 1) - SUM(IS_ANONYMOUS)))
                        ELSE NULL END AS PLAYERS_FEMALE_PERCENTAGE
            , CASE WHEN (SUM(PLAY_SESSION_STATUS - 1) - SUM(IS_ANONYMOUS)) > 0 THEN (SUM(PLAYER_AGE) / (SUM(PLAY_SESSION_STATUS - 1) - SUM(IS_ANONYMOUS)))
                        ELSE NULL END AS PLAYERS_AGE_AVERAGE
            , SUM(PLAYER_IS_VIP) AS NUM_VIPS
            , SUM(PLAYED_AMOUNT) AS PLAYED_AMOUNT
            , SUM(WON_AMOUNT) AS WON_AMOUNT
            , CASE WHEN (SUM(PLAY_SESSION_STATUS - 1) - SUM(IS_ANONYMOUS)) > 0 THEN ROUND((SUM(PLAY_SESSION_BET_AVERAGE) / (SUM(PLAY_SESSION_STATUS - 1) - SUM(IS_ANONYMOUS))), 2)
                        ELSE NULL END  AS PLAY_SESSIONS_BET_AVERAGE
            , CASE WHEN COUNT(TERMINAL_ID) > 0 THEN ROUND((SUM(TERMINAL_BET_AVERAGE) / COUNT(TERMINAL_ID)), 2) 
                    ELSE NULL END AS TERMINALS_BET_AVERAGE
        FROM #temp_table 
  GROUP BY TERMINAL_AREA_ID
      
      DROP TABLE #temp_table
      
END

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetTerminalHistory]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[Layout_GetTerminalHistory]
GO

CREATE PROCEDURE  [dbo].[Layout_GetTerminalHistory] 
	@TerminalId INT,
	@DateStart DATETIME,
	@DateEnd DATETIME  
AS
BEGIN
	SELECT	  SPH_TERMINAL_ID 
					, SPH_TERMINAL_NAME 
					, SUM(SPH_PLAYED_COUNT) AS SPH_PLAYED_COUNT
					, SUM(SPH_PLAYED_AMOUNT) AS SPH_PLAYED_AMOUNT
					, SUM(SPH_WON_COUNT) AS SPH_WON_COUNT
					, (SUM(SPH_PLAYED_AMOUNT)/SUM( SPH_PLAYED_COUNT)) AS 'AVG'
					, (SUM(SPH_PLAYED_AMOUNT)-SUM(SPH_WON_AMOUNT)) AS NETWIN
					, SUM(SPH_WON_AMOUNT) AS SPH_WON_AMOUNT
					, SUM(SPH_THEORETICAL_WON_AMOUNT) AS SPH_THEORETICAL_WON_AMOUNT
					, SUM(SPH_JACKPOT_AMOUNT) AS SPH_JACKPOT_AMOUNT
					, SUM(SPH_PROGRESSIVE_JACKPOT_AMOUNT) AS SPH_PROGRESSIVE_JACKPOT_AMOUNT
					, SUM(SPH_PROGRESSIVE_JACKPOT_AMOUNT_0) AS SPH_PROGRESSIVE_JACKPOT_AMOUNT_0
					, SUM(SPH_PROGRESSIVE_PROVISION_AMOUNT)VSPH_PROGRESSIVE_PROVISION_AMOUNT
		 FROM	  SALES_PER_HOUR  
		WHERE   SPH_BASE_HOUR >= @DateStart AND  SPH_BASE_HOUR < @DateEnd 
		  AND   SPH_TERMINAL_ID = @TerminalId
 		GROUP	  BY SPH_TERMINAL_ID, SPH_TERMINAL_NAME
END

GO


/*Weather URI*/
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'SmartFloor' AND GP_SUBJECT_KEY = 'Dashboard.Meteogram.URL')
                     INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('SmartFloor','Dashboard.Meteogram.URL','http://www.myweather2.com/developer/forecast.ashx')
GO

/*Weather Latitude Longitude*/
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'SmartFloor' AND GP_SUBJECT_KEY = 'Dashboard.Meteogram.LatitudeLongitude')
                     INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('SmartFloor','Dashboard.Meteogram.LatitudeLongitude','0.0,0.0')
GO

GO

/****** Object:  Table [dbo].[layout_weather_diary]    Script Date: 04/08/2016 10:03:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[layout_weather_diary]') AND type in (N'U'))
DROP TABLE [dbo].[layout_weather_diary]
GO

/****** Object:  Table [dbo].[layout_weather_diary]    Script Date: 04/04/2016 16:21:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[layout_weather_diary](
	[lwd_date] [int] IDENTITY(1,1) NOT NULL,
	[lwd_weather_code] [int] NOT NULL,
	[lwd_temp] [int] NOT NULL,
	[lwd_response] [xml] NOT NULL
 CONSTRAINT [PK_layout_weather_diary] PRIMARY KEY CLUSTERED 
(
	[lwd_date] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO



GO

/****** Object:  StoredProcedure [dbo].[Layout_SaveWeatherDay]    Script Date: 04/06/2016 12:43:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_SaveWeatherDay]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Layout_SaveWeatherDay]
GO

/****** Object:  StoredProcedure [dbo].[Layout_SaveWeatherDay]    Script Date: 04/04/2016 17:36:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Layout_SaveWeatherDay]
      @pDate INT
    , @pWeatherCode INT
    , @pTemp INT
    , @pXML XML
AS
BEGIN
  IF NOT EXISTS (SELECT TOP 1 * FROM LAYOUT_WEATHER_DAIRY WHERE LWD_DATE = @pDate)
  
    INSERT   INTO LAYOUT_WEATHER_DAIRY
           ( LWD_DATE
           , LWD_WEATHER_CODE
           , LWD_TEMP 
           , LWD_RESPONSE)
    VALUES ( @pDate
           , @pWeatherCode
           , @pTemp
           , @pXML ) 
END

GO


/****** Object:  StoredProcedure [dbo].[SP_Get_Meters]    Script Date: 04/06/2016 12:43:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_Get_Meters]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_Get_Meters]
GO


/****** Object:  StoredProcedure [dbo].[SP_Get_Meters]    Script Date: 04/06/2016 12:43:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_Get_Meters] 
(  @Device AS INT
 , @Source AS VARCHAR(1)
 , @Date AS DATETIME
) 
AS
BEGIN
  DECLARE @DATEFROM AS DATETIME
  DECLARE @DATEFROM_WEEK AS DATETIME
  DECLARE @DATEFROM_MONTH AS DATETIME
  DECLARE @DATEFROM_YEAR AS DATETIME
  DECLARE @SEARCH_DATE AS NVARCHAR(MAX)
  DECLARE @QUERY_CONST AS NVARCHAR(MAX)
  DECLARE @QUERY_TEMP AS NVARCHAR(MAX)
  DECLARE @FINAL_QUERY AS NVARCHAR(MAX)
  DECLARE @QUERY AS NVARCHAR(MAX)
  DECLARE @RANGE AS VARCHAR(1)
  DECLARE @DESCRIPTION AS VARCHAR(3)
  DECLARE @HOUR AS VARCHAR(2)
  DECLARE @ORIGIN AS VARCHAR(MAX)
  DECLARE @DOW AS VARCHAR(1)

  SET @FINAL_QUERY = ''
  SET @DATEFROM = @Date

  EXEC SP_Get_Meters_List @DEVICE

  SET @DATEFROM_WEEK = DATEADD(WK, DATEDIFF(WK, 0, @DATEFROM) - 1, 0)             -- First day of previous week
  SET @DATEFROM_MONTH = DATEADD(M, -1, DATEADD(M, DATEDIFF(M, 0, @DATEFROM), 0))  -- First day of previous month
  SET @DATEFROM_YEAR = DATEADD(YY, -1, DATEADD(YY, DATEDIFF(YY, 0, @DATEFROM), 0)) -- First day of previous year
  SET @DOW = LTRIM(DATEPART(WEEKDAY, @DATEFROM))

  SET @HOUR = DATEPART(HOUR, GETDATE())
  SET @HOUR = REPLICATE('0',2-LEN(RTRIM(@HOUR))) + RTRIM(@HOUR) 

  SET @QUERY_CONST = ' SELECT ''@DESCRIPTION'' AS RANGE, @HOUR as HOUR, x2d_date, x2d_weekday, x2d_id, x2d_meter_id, x2d_meter_item,'
                   + ' x2d_@HOUR_min AS MIN, x2d_@HOUR_max as MAX, x2d_@HOUR_acc AS ACC, x2d_@HOUR_avg AS AVG, x2d_@HOUR_num AS NUM,'
                   + ' x2d_dd_min AS DAY_MIN, x2d_dd_max AS DAY_MAX, x2d_dd_acc AS DAY_ACC, x2d_dd_avg AS DAY_AVG, x2d_dd_num AS DAY_NUM'
                   + ' FROM  H_@RANGE2D_@SOURCEMH'
                   + ' INNER JOIN H_METERS_DEFINITION ON x2d_meter_id = hmd_meter_id'
                   + ' WHERE hmd_visible = 1 AND (@DATE)'

  -- TDA & SLW
  SET @QUERY_TEMP = @QUERY_CONST
  SET @RANGE = 'T'
  SET @DESCRIPTION = 'TDA'
  SET @SEARCH_DATE = 'x2d_date >= ' + CONVERT(VARCHAR(10), DATEADD(DAY, -7, @DATEFROM), 112) + ' AND x2d_date <= ' + CONVERT(VARCHAR(10), @DATEFROM, 112)
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DATE', @SEARCH_DATE)
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@HOUR', LTRIM(@HOUR))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@RANGE', LTRIM(@RANGE))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DESCRIPTION', LTRIM(@DESCRIPTION))
  SET @FINAL_QUERY = @QUERY_TEMP 

  -- WTD
  SET @QUERY_TEMP = @QUERY_CONST
  SET @RANGE = 'W'
  SET @DESCRIPTION = 'WTD'
  SET @SEARCH_DATE = 'x2d_date = ' + CAST(CONVERT(VARCHAR(10), @DATEFROM_WEEK, 112) AS VARCHAR)
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DATE', LTRIM(@SEARCH_DATE))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@HOUR', LTRIM(@HOUR))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@RANGE', LTRIM(@RANGE))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DESCRIPTION', LTRIM(@DESCRIPTION))
  --EXECUTE sp_executesql @QUERY
  SET @FINAL_QUERY = @FINAL_QUERY
                   + ' UNION '
                   + @QUERY_TEMP 
                   + ' AND x2d_weekday = ' + @DOW

  -- MTD
  SET @QUERY_TEMP = @QUERY_CONST
  SET @RANGE = 'M'
  SET @DESCRIPTION = 'MTD'
  SET @SEARCH_DATE = 'x2d_date = ' + CAST(CONVERT(VARCHAR(10), @DATEFROM_MONTH, 112) AS VARCHAR)
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DATE', LTRIM(@SEARCH_DATE))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@HOUR', LTRIM(@HOUR))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@RANGE', LTRIM(@RANGE))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DESCRIPTION', LTRIM(@DESCRIPTION))
  SET @FINAL_QUERY = @FINAL_QUERY
                   + ' UNION '
                   + @QUERY_TEMP 
                   + ' AND x2d_weekday = ' + @DOW

  -- YTD
  SET @QUERY_TEMP = @QUERY_CONST
  SET @RANGE = 'Y'
  SET @DESCRIPTION = 'YTD'
  SET @SEARCH_DATE = 'x2d_date = ' + CAST(CONVERT(VARCHAR(10), @DATEFROM_YEAR, 112) AS VARCHAR)
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DATE', LTRIM(@SEARCH_DATE))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@HOUR', LTRIM(@HOUR))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@RANGE', LTRIM(@RANGE))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DESCRIPTION', LTRIM(@DESCRIPTION))
  SET @FINAL_QUERY = @FINAL_QUERY
                   + ' UNION '
                   + @QUERY_TEMP 
                   + ' AND x2d_weekday = ' + @DOW

  SET @QUERY = REPLACE(@FINAL_QUERY, '@SOURCE', @Source)
           print @QUERY  
  EXECUTE sp_executesql @QUERY
  
  --WEATHER METERS-------
  --LAST 7 DAYS PROMOTIONS,OCCUPANCY AND WEATHER
  SELECT TOP 7H_T2D_SMH.x2d_date AS DATE
           ,  H_T2D_SMH.X2D_DD_NUM AS PROMOTIONS
           ,  O.X2D_DD_MAX AS OCCUPANCY
           ,  W.LWD_WEATHER_CODE AS WEATHER
        FROM H_T2D_SMH
        
        LEFT JOIN (
          SELECT TOP 7 X2D_DATE,X2D_DD_MAX 
                FROM H_T2D_SMH
               WHERE X2D_METER_ID = 16 AND X2D_METER_ITEM = 0
                   ) O ON O.X2D_DATE = H_T2D_SMH.X2D_DATE
       INNER JOIN (
          SELECT TOP 7lwd_date,lwd_weather_code 
                FROM LAYOUT_WEATHER_DIARY
                   ) W ON H_T2D_SMH.X2D_DATE = W.LWD_DATE
       WHERE H_T2D_SMH.X2D_METER_ID = 8 AND H_T2D_SMH.X2D_METER_ITEM = 2
    ORDER BY H_T2D_SMH.X2D_DATE DESC
  
END

GO

/****** Object:  StoredProcedure [dbo].[Layout_GetDashboardRoles]    Script Date: 04/07/2016 16:47:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetDashboardRoles]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Layout_GetDashboardRoles]
GO

/****** Object:  StoredProcedure [dbo].[Layout_GetDashboardRoles]    Script Date: 04/07/2016 16:47:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Layout_GetDashboardRoles]

AS
BEGIN

DECLARE @ALLDASHBOARD NVARCHAR(MAX)

SELECT @ALLDASHBOARD = COALESCE(@ALLDASHBOARD + ', ', '') + '{'+ CAST([LD_ROLE_ID] AS VARCHAR(2)) +':' + [LD_DASHBOARD] +'}' 
  FROM  [LAYOUT_DASHBOARDS]
 
SELECT '[' + @ALLDASHBOARD +']';

END -- Layout_GetDashboardRoles

GO


