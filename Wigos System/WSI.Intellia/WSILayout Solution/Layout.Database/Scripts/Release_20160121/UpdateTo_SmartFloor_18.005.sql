USE WGDB_000
GO

SET ANSI_PADDING ON
GO

SET QUOTED_IDENTIFIER ON
GO

----------------------------------------------------
--------STORED PROCEDURES---------------------------
----------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CreateDefaultProfilesLayout]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[CreateDefaultProfilesLayout]
GO
CREATE PROCEDURE [dbo].[CreateDefaultProfilesLayout]
AS 
BEGIN  
  
  DECLARE @ProfileId   BIGINT
  DECLARE @ProfileName NVARCHAR(40)
  DECLARE @pGuiId      INT
  
  SET @ProfileId = 0
  SET @ProfileName = ''
  SET @pGuiId = 202
  
  --Comprovació existència permisos
  IF EXISTS (SELECT GF_GUI_ID FROM GUI_FORMS WHERE GF_GUI_ID = @pGuiId)
  BEGIN 
  
    --********************************
    --**PCA - PLAYERS CLUB ATTENDANT**
    --********************************
    SET @ProfileName = 'LAYOUT - PLAYERS CLUB ATTENDANT'     
    
    --Comprovació existència perfil
    IF NOT EXISTS (SELECT  GUP_PROFILE_ID FROM GUI_USER_PROFILES WHERE UPPER (GUP_NAME) LIKE @ProfileName ESCAPE '\' ) 
    BEGIN
    
      --Obtenció de l´ultim ID Profile (tal i com ho fa el GUI ¿?¿?¿?)
      SET @ProfileId = (SELECT MAX(ISNULL(GUP_PROFILE_ID, 0)) + 1  FROM GUI_USER_PROFILES)
      
      --Insert del Perfil
      INSERT INTO GUI_USER_PROFILES (GUP_PROFILE_ID, GUP_NAME, GUP_MAX_USERS) VALUES (@ProfileId, @ProfileName, 0)
      
      
      --Assignar Permisos de cada pantalla al perfil creat
      
      --Pantalla (PCA - LOGIN)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1, 1, 1, 1, 1) 
      
           
      --Pantalla (PCA - Dashboard)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1010000, 1, 1, 1, 1) 
           
      --Pantalla (PCA - Dashboard - Important)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1010100, 0, 0, 0, 0) 
           
      --Pantalla (PCA - Dashboard - Statistics)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1010200, 1, 1, 1, 1) 
           
      --Pantalla (PCA - Dashboard - Ocupattion)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1010201, 1, 1, 1, 1) 
      
      --Pantalla (PCA - Dashboard - Ocupattion by card)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1010202, 1, 1, 1, 1) 
           
      --Pantalla (PCA - Dashboard - VIP Customers)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1010203, 1, 1, 1, 1) 
           
      --Pantalla (PCA - Dashboard - Ocupattion Percentage by gender)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1010204, 1, 1, 1, 1) 

      --Pantalla (PCA - Dashboard - Ocupattion Percentage by age)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1010205, 1, 1, 1, 1) 

      --Pantalla (PCA - Dashboard - Evaluattion by age and gender)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1010206, 1, 1, 1, 1) 

      --Pantalla (PCA - Dashboard - Technics issues percentage)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1010207, 0, 0, 0, 0) 

      --Pantalla (PCA - Dashboard - Coin in, ANW, spins by game and manufacturer representation)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1010208, 1, 1, 1, 1) 

      --Pantalla (PCA - Dashboard - Gain Floor by machine and coin representation)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1010209, 1, 1, 1, 1) 

      --Pantalla (PCA - Dashboard - Coin in evaluation)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1010210, 1, 1, 1, 1) 

      --Pantalla (PCA - Floor)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1020000, 1, 1, 1, 1) 

      --Pantalla (PCA – Floor – Compare Terminals)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1020100, 1, 1, 1, 1) 

 		 --Pantalla (PCA – Floor – Temperature Map)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1020200, 1, 1, 1, 1) 

      --Pantalla (PCA – Floor – Visualization)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1020300, 1, 1, 1, 1) 

      --Pantalla (PCA - My Tasks)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1030000, 1, 1, 1, 1) 

      --Pantalla (PCA - Alarms - All)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1040000, 1, 1, 1, 1) 

      --Pantalla (PCA - Alarms - Players Only)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1040001, 1, 1, 1, 1) 

      --Pantalla (PCA - Alarms - Machines Only)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1040002, 1, 1, 1, 1) 
      
      --Pantalla (PCA - Charts)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1050000, 1, 1, 1, 1) 
           
      --Pantalla (PCA - Players)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1060000, 1, 1, 1, 1) 

      --Pantalla (PCA - Players - List Mode)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1060001, 1, 1, 1, 1) 

      --Pantalla (PCA - Players - Find)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1060100, 1, 1, 1, 1) 

      --Pantalla (PCA - Players - Filter)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1060200, 1, 1, 1, 1) 

      --Pantalla (PCA - Machines)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1070000, 0, 0, 0, 0) 

      --Pantalla (PCA - Comparation Machines)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1070001, 0, 0, 0, 0) 
  
    END
    --********************************
    --**OPS - OPS MANAGER ************
    --********************************
    SET @ProfileName = 'LAYOUT - OPS MANAGER'     
    
    --Comprovació existència perfil
    IF NOT EXISTS (SELECT  GUP_PROFILE_ID FROM GUI_USER_PROFILES WHERE UPPER (GUP_NAME) LIKE @ProfileName ESCAPE '\' ) 
    BEGIN
    
      --Obtenció de l´ultim ID Profile (tal i com ho fa el GUI ¿?¿?¿?)
      SET @ProfileId = (SELECT MAX(ISNULL(GUP_PROFILE_ID, 0)) + 1  FROM GUI_USER_PROFILES)
      
      --Insert del Perfil
      INSERT INTO GUI_USER_PROFILES (GUP_PROFILE_ID, GUP_NAME, GUP_MAX_USERS) VALUES (@ProfileId, @ProfileName, 0)
      
      --Assignar Permisos de cada pantalla al perfil creat
      --*OPS
      --Pantalla (OPS - LOGIN)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1, 1, 1, 1, 1) 
      
      --Pantalla (OPS - Dashboard)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2010000, 1, 1, 1, 1)  
           
      --Pantalla (OPS - Dashboard - Important)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2010100, 0, 0, 0, 0) 
           
      --Pantalla (OPS - Dashboard - Statistics)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2010200, 1, 1, 1, 1)  
           
      --Pantalla (OPS - Dashboard - Ocupattion)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2010201, 1, 1, 1, 1)  
      
      --Pantalla (OPS - Dashboard - Ocupattion by card)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2010202, 1, 1, 1, 1)  
           
      --Pantalla (OPS - Dashboard - VIP Customers)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2010203, 0, 0, 0, 0)  
           
      --Pantalla (OPS - Dashboard - Ocupattion Percentage by gender)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2010204, 0, 0, 0, 0)  

      --Pantalla (OPS - Dashboard - Ocupattion Percentage by age)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2010205, 0, 0, 0, 0)  

      --Pantalla (OPS - Dashboard - Evaluattion by age and gender)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2010206, 0, 0, 0, 0)  

      --Pantalla (OPS - Dashboard - Technics issues percentage)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2010207, 1, 1, 1, 1) 

      --Pantalla (OPS - Dashboard - Coin in, ANW, spins by game and manufacturer representation)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2010208, 1, 1, 1, 1)  

      --Pantalla (OPS - Dashboard - Gain Floor by machine and coin representation)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2010209, 1, 1, 1, 1)  

      --Pantalla (OPS - Dashboard - Coin in evaluation)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2010210, 1, 1, 1, 1)  

      --Pantalla (OPS - Floor)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2020000, 1, 1, 1, 1)  

      --Pantalla (OPS – Floor – Compare Terminals)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2020100, 1, 1, 1, 1) 

 		  --Pantalla (OPS – Floor – Temperature Map)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2020200, 1, 1, 1, 1) 

      --Pantalla (OPS – Floor – Visualization)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2020300, 1, 1, 1, 1) 

      --Pantalla (OPS - My Tasks)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2030000, 1, 1, 1, 1)  

      --Pantalla (OPS - Alarms - All)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2040000, 1, 1, 1, 1)  

      --Pantalla (OPS - Alarms - Players Only)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2040001, 1, 1, 1, 1)  

      --Pantalla (OPS - Alarms - Machines Only)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2040002, 1, 1, 1, 1)  
      
      --Pantalla (OPS - Charts)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2050000, 1, 1, 1, 1) 
           
      --Pantalla (OPS - Players)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2060000, 0, 0, 0, 0) 

      --Pantalla (OPS - Players - List Mode)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2060001, 0, 0, 0, 0) 

      --Pantalla (OPS - Players - Find)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2060100, 0, 0, 0, 0) 
     
      --Pantalla (OPS - Players - Filter)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2060200, 0, 0, 0, 0) 

      --Pantalla (OPS - Machines)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2070000, 1, 1, 1, 1) 

      --Pantalla (OPS - Comparation Machines)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2070001, 1, 1, 1, 1) 
      
      
    END

    --********************************
    --**SLO - SLOT ATTENDANT *********
    --********************************
    SET @ProfileName = 'LAYOUT - SLOT ATTENDANT'     
    
    --Comprovació existència perfil
    IF NOT EXISTS (SELECT  GUP_PROFILE_ID FROM GUI_USER_PROFILES WHERE UPPER (GUP_NAME) LIKE @ProfileName ESCAPE '\' ) 
    BEGIN
    
      --Obtenció de l´ultim ID Profile (tal i com ho fa el GUI ¿?¿?¿?)
      SET @ProfileId = (SELECT MAX(ISNULL(GUP_PROFILE_ID, 0)) + 1  FROM GUI_USER_PROFILES)
      
      --Insert del Perfil
      INSERT INTO GUI_USER_PROFILES (GUP_PROFILE_ID, GUP_NAME, GUP_MAX_USERS) VALUES (@ProfileId, @ProfileName, 0)
      
      
      --*SLO
      --Pantalla (SLO - LOGIN)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1, 1, 1, 1, 1) 
           
      --Pantalla (SLO - Dashboard)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4010000, 1, 1, 1, 1)  
           
      --Pantalla (SLO - Dashboard - Important)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4010100, 0, 0, 0, 0) 
           
      --Pantalla (SLO - Dashboard - Statistics)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4010200, 1, 1, 1, 1)  
           
      --Pantalla (SLO - Dashboard - Ocupattion)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4010201, 1, 1, 1, 1)  
      
      --Pantalla (SLO - Dashboard - Ocupattion by card)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4010202, 1, 1, 1, 1)  
           
      --Pantalla (SLO - Dashboard - VIP Customers)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4010203, 1, 1, 1, 1)  
           
      --Pantalla (SLO - Dashboard - Ocupattion Percentage by gender)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4010204, 0, 0, 0, 0)  

      --Pantalla (SLO - Dashboard - Ocupattion Percentage by age)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4010205, 0, 0, 0, 0)  

      --Pantalla (SLO - Dashboard - Evaluation by age and gender)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4010206, 0, 0, 0, 0)  

      --Pantalla (SLO - Dashboard - Technics issues percentage)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4010207, 0, 0, 0, 0) 

      --Pantalla (SLO - Dashboard - Coin in, ANW, spins by game and manufacturer representation)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4010208, 0, 0, 0, 0)  

      --Pantalla (SLO - Dashboard - Gain Floor by machine and coin representation)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4010209, 0, 0, 0, 0)  

      --Pantalla (SLO - Dashboard - Coin in evaluation)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4010210, 0, 0, 0, 0)  

      --Pantalla (SLO - Floor)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4020000, 1, 1, 1, 1)  

      --Pantalla (SLO – Floor – Compare Terminals)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4020100, 1, 1, 1, 1)  

      --Pantalla (SLO – Floor – Temperature Map)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4020200, 1, 1, 1, 1)  

      --Pantalla (SLO – Floor - Visualization)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4020300, 1, 1, 1, 1)  

      --Pantalla (SLO - My Tasks)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4030000, 1, 1, 1, 1)  

      --Pantalla (SLO - Alarms - All)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4040000, 1, 1, 1, 1)  

      --Pantalla (SLO - Alarms - Players Only)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4040001, 1, 1, 1, 1)  

      --Pantalla (SLO - Alarms - Machines Only)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4040002, 1, 1, 1, 1)  
      
      --Pantalla (SLO - Charts)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4050000, 0, 0, 0, 0) 
           
      --Pantalla (SLO - Players)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4060000, 1, 1, 1, 1) 

      --Pantalla (SLO - Players - List Mode)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4060001, 0, 0, 0, 0) 

      --Pantalla (SLO - Players - Find)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4060100, 1, 1, 1, 1) 

      --Pantalla (SLO - Players - Filter)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4060200, 1, 1, 1, 1) 

      --Pantalla (SLO - Machines)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4070000, 1, 1, 1, 1) 

      --Pantalla (SLO - Comparation Machines)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4070001, 0, 0, 0, 0) 
     
    END

    --********************************
    --**TCH - TECHNICAL SUPPORT ******
    --********************************
    SET @ProfileName = 'LAYOUT - TECHNICAL SUPPORT'     
    
    --Comprovació existència perfil
    IF NOT EXISTS (SELECT  GUP_PROFILE_ID FROM GUI_USER_PROFILES WHERE UPPER (GUP_NAME) LIKE @ProfileName ESCAPE '\' ) 
    BEGIN
    
      --Obtenció de l´ultim ID Profile (tal i com ho fa el GUI ¿?¿?¿?)
      SET @ProfileId = (SELECT MAX(ISNULL(GUP_PROFILE_ID, 0)) + 1  FROM GUI_USER_PROFILES)
      
      --Insert del Perfil
      INSERT INTO GUI_USER_PROFILES (GUP_PROFILE_ID, GUP_NAME, GUP_MAX_USERS) VALUES (@ProfileId, @ProfileName, 0)
      
      --Assignar Permisos de cada pantalla al perfil creat
    
      --*TCH
      --Pantalla (TCH - LOGIN)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1, 1, 1, 1, 1) 
           
      --Pantalla (TCH - Dashboard)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8010000, 1, 1, 1, 1)  
           
      --Pantalla (TCH - Dashboard - Important)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8010100, 0, 0, 0, 0) 
           
      --Pantalla (TCH - Dashboard - Statistics)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8010200, 1, 1, 1, 1)  
           
      --Pantalla (TCH - Dashboard - Ocupattion)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8010201, 1, 1, 1, 1)  
      
      --Pantalla (TCH - Dashboard - Ocupattion by card)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8010202, 1, 1, 1, 1)  
           
      --Pantalla (TCH - Dashboard - VIP Customers)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8010203, 1, 1, 1, 1)  
           
      --Pantalla (TCH - Dashboard - Ocupattion Percentage by gender)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8010204, 1, 1, 1, 1)  

      --Pantalla (TCH - Dashboard - Ocupattion Percentage by age)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8010205, 1, 1, 1, 1)  

      --Pantalla (TCH - Dashboard - Evaluation by age and gender)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8010206, 1, 1, 1, 1)  

      --Pantalla (TCH - Dashboard - Technics issues percentage)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1310207, 1, 1, 1, 1) 

      --Pantalla (TCH - Dashboard - Coin in, ANW, spins by game and manufacturer representation)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8010208, 1, 1, 1, 1)  

      --Pantalla (TCH - Dashboard - Gain Floor by machine and coin representation)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8010209, 1, 1, 1, 1)  

      --Pantalla (TCH - Dashboard - Coin in evaluation)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8010210, 1, 1, 1, 1)  

      --Pantalla (TCH - Floor)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8020000, 1, 1, 1, 1)  

      --Pantalla (TCH – Floor – Compare Terminals)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8020100, 1, 1, 1, 1)  

      --Pantalla (TCH – Floor – Temperature Map)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8020200, 1, 1, 1, 1)  

      --Pantalla (TCH – Floor - Visualization)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8020300, 1, 1, 1, 1)  

      --Pantalla (TCH - My Tasks)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8030000, 1, 1, 1, 1)  

      --Pantalla (TCH - Alarms - All)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8040000, 1, 1, 1, 1)  
    
      --Pantalla (TCH - Alarms - Players Only)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8040001, 1, 1, 1, 1)  

      --Pantalla (TCH - Alarms - Machines Only)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8040002, 1, 1, 1, 1)  
      
      --Pantalla (TCH - Charts)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8050000, 0, 0, 0, 0) 
           
      --Pantalla (TCH - Players)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8060000, 0, 0, 0, 0) 
      
      --Pantalla (TCH - Players - List Mode)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8060001, 0, 0, 0, 0) 

      --Pantalla (TCH - Players - Find)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8060100, 0, 0, 0, 0) 

      --Pantalla (TCH - Players - Filter)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8060200, 0, 0, 0, 0) 

      --Pantalla (TCH - Machines)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8070000, 1, 1, 1, 1) 

      --Pantalla (TCH - Comparation Machines)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8070001, 0, 0, 0, 0) 
      
    END
    
    
    --********************************
    --**ADM - ADMINISTRATOR **********
    --********************************
    SET @ProfileName = 'LAYOUT - ADMINISTRATOR'     
    
    --Comprovació existència perfil
    IF NOT EXISTS (SELECT  GUP_PROFILE_ID FROM GUI_USER_PROFILES WHERE UPPER (GUP_NAME) LIKE @ProfileName ESCAPE '\' ) 
    BEGIN
    
      --Obtenció de l´ultim ID Profile (tal i com ho fa el GUI ¿?¿?¿?)
      SET @ProfileId = (SELECT MAX(ISNULL(GUP_PROFILE_ID, 0)) + 1  FROM GUI_USER_PROFILES)
      
      --Insert del Perfil
      INSERT INTO GUI_USER_PROFILES (GUP_PROFILE_ID, GUP_NAME, GUP_MAX_USERS) VALUES (@ProfileId, @ProfileName, 0)
      
      --Assignar Permisos de cada pantalla al perfil creat
      
      --*ADM
      --Pantalla (ADM - LOGIN)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1, 1, 1, 1, 1) 
      
      --Pantalla (ADM - Configuration)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 16010000, 0, 0, 0, 0) 

      --Pantalla (ADM - Floor - Edit)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 16020000, 0, 0, 0, 0) 

      --Pantalla (ADM - Caption - Edit)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 16030000, 0, 0, 0, 0) 
      
    END
  END
END

----------------------------------------------------
--------GRANTS FOR GUI------------------------------
----------------------------------------------------

GRANT EXECUTE ON [dbo].[CreateDefaultProfilesLayout] TO [wggui] WITH GRANT OPTION
GO

GRANT EXECUTE ON [dbo].[Layout_SaveUser] TO [wggui] WITH GRANT OPTION
GO

