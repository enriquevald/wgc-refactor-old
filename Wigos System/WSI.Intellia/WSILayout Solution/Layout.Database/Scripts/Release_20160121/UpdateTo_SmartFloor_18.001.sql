USE WGDB_000;

SET ANSI_PADDING ON;

SET QUOTED_IDENTIFIER ON;

----------------------------------------------------
--------DROP TABLES---------------------------------
----------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[layout_floors]') AND type in (N'U'))
DROP TABLE [dbo].[layout_floors];

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[layout_locations]') AND type in (N'U'))
DROP TABLE [dbo].[layout_locations];

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[layout_meshes]') AND type in (N'U'))
DROP TABLE [dbo].[layout_meshes];

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[layout_object_locations]') AND type in (N'U'))
DROP TABLE [dbo].[layout_object_locations];

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[layout_objects]') AND type in (N'U'))
DROP TABLE [dbo].[layout_objects];

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[layout_objects_positions]') AND type in (N'U'))
DROP TABLE [dbo].[layout_objects_positions];

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[layout_parameters]') AND type in (N'U'))
DROP TABLE [dbo].[layout_parameters];

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[layout_ranges]') AND type in (N'U'))
DROP TABLE [dbo].[layout_ranges];

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[layout_ranges_legends]') AND type in (N'U'))
DROP TABLE [dbo].[layout_ranges_legends];

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[layout_recording]') AND type in (N'U'))
DROP TABLE [dbo].[layout_recording];

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[layout_site_tasks]') AND type in (N'U'))
DROP TABLE [dbo].[layout_site_tasks];

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[layout_users_configuration]') AND type in (N'U'))
DROP TABLE [dbo].[layout_users_configuration];

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[layout_users_custom_filters]') AND type in (N'U'))
DROP TABLE [dbo].[layout_users_custom_filters];

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[layout_views]') AND type in (N'U'))
DROP TABLE [dbo].[layout_views];

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[layout_media]') AND type in (N'U'))
DROP TABLE [dbo].[layout_media];

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[layout_site_alarms]') AND type in (N'U'))
DROP TABLE [dbo].[layout_site_alarms];

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[layout_devices]') AND type in (N'U'))
DROP TABLE [dbo].[layout_devices];

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[layout_enums]') AND type in (N'U'))
DROP TABLE [dbo].[layout_enums];

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[layout_runners_position]') AND type in (N'U'))
DROP TABLE [dbo].[layout_runners_position];

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[layout_manager_runner_messages]') AND type in (N'U'))
DROP TABLE [dbo].[layout_manager_runner_messages];

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[layout_resources]') AND type in (N'U'))
DROP TABLE [dbo].[layout_resources];

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[layout_mobile_application_versions]') AND type in (N'U'))
DROP TABLE [dbo].[layout_mobile_application_versions];

----------------------------------------------------
--------CREATE TABLES ------------------------------
----------------------------------------------------

CREATE TABLE [dbo].[layout_floors](
  [lf_floor_id] [int] IDENTITY(1,1) NOT NULL,
  [lf_geometry] [varchar](max) NULL,
  [lf_image_map] [varchar](max) NULL,
  [lf_color] [varchar](20) NULL,
  [lf_name] [varchar](255) NULL,
  [lf_height] [int] NULL,
  [lf_width] [int] NULL,
  [lf_metric] [int] NULL CONSTRAINT [DF_layout_floors_lf_metric]  DEFAULT ((0)),
 CONSTRAINT [PK_layout_floors] PRIMARY KEY CLUSTERED 
(
  [lf_floor_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
;

CREATE TABLE [dbo].[layout_locations](
  [loc_id] [bigint] IDENTITY(1,1) NOT NULL,
  [loc_x] [float] NOT NULL,
  [loc_y] [float] NOT NULL,
  [loc_creation] [datetime] NOT NULL,
  [loc_type] [int] NOT NULL,
  [loc_floor_number] [int] NOT NULL,
  [loc_grid_id] [nvarchar](50) NULL,
  [loc_status] [int] NOT NULL CONSTRAINT [DF_layout_locations_loc_status]  DEFAULT ((1)),
 CONSTRAINT [PK_layout_locations] PRIMARY KEY CLUSTERED 
(
  [loc_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY];

CREATE TABLE [dbo].[layout_meshes](
  [lme_mesh_id] [bigint] NOT NULL,
  [lme_type] [int] NOT NULL,
  [lme_sub_type] [int] NULL,
  [lme_geometry] [varchar](max) NOT NULL,
 CONSTRAINT [PK_layout_meshes] PRIMARY KEY CLUSTERED 
(
  [lme_mesh_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY];

CREATE TABLE [dbo].[layout_object_locations](
  [lol_object_id] [bigint] NOT NULL,
  [lol_location_id] [bigint] NOT NULL,
  [lol_date_from] [datetime] NOT NULL,
  [lol_date_to] [datetime] NULL,
  [lol_orientation] [int] NULL,
  [lol_area_id] [int] NULL,
  [lol_bank_id] [int] NULL,
  [lol_bank_location] [int] NULL,
  [lol_current_location] [bit] NOT NULL,
  [lol_offset_x] [float] NOT NULL CONSTRAINT [DF_layout_object_locations_lol_offset_x]  DEFAULT ((0)),
  [lol_offset_y] [float] NOT NULL CONSTRAINT [DF_layout_object_locations_lol_offset_y]  DEFAULT ((0)),
 CONSTRAINT [PK_layout_object_locations] PRIMARY KEY CLUSTERED 
(
  [lol_object_id] ASC,
  [lol_location_id] ASC,
  [lol_date_from] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY];

CREATE TABLE [dbo].[layout_objects](
  [lo_id] [bigint] IDENTITY(1,1) NOT NULL,
  [lo_external_id] [bigint] NULL,
  [lo_type] [int] NOT NULL,
  [lo_parent_id] [bigint] NULL,
  [lo_mesh_id] [int] NULL,
  [lo_creation_date] [datetime] NOT NULL,
  [lo_update_date] [datetime] NULL,
  [lo_status] [int] NOT NULL CONSTRAINT [DF_layout_objects_lo_status]  DEFAULT ((1)),
 CONSTRAINT [PK_layout_objects] PRIMARY KEY CLUSTERED 
(
  [lo_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY];

CREATE TABLE [dbo].[layout_objects_positions](
  [lop_id] [bigint] NOT NULL,
  [lop_position_date] [datetime] NOT NULL,
  [lop_x] [float] NOT NULL,
  [lop_y] [float] NOT NULL,
  [lop_z] [float] NOT NULL,
  [lop_orientation] [int] NOT NULL,
  [lop_floor_id] [int] NULL,
  [lop_area_id] [int] NULL,
  [lop_bank_id] [int] NULL,
 CONSTRAINT [PK_layout_objects_positions] PRIMARY KEY CLUSTERED 
(
  [lop_id] ASC,
  [lop_position_date] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY];

CREATE TABLE [dbo].[layout_parameters](
  [lp_id] [int] NOT NULL,
  [lp_group] [nvarchar](50) NULL,
  [lp_desc] [nvarchar](250) NOT NULL,
  [lp_value] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_layout_parameters] PRIMARY KEY CLUSTERED 
(
  [lp_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY];

CREATE TABLE [dbo].[layout_ranges](
  [lr_id] [int] IDENTITY(1,1) NOT NULL,
  [lr_name] [nvarchar](50) NOT NULL,
  [lr_field] [nvarchar](250) NOT NULL,
  [lr_section_id] [int] NOT NULL,
  [lr_role] [int] NOT NULL CONSTRAINT [DF_layout_ranges_lr_role2]  DEFAULT ((0)),
  [lr_icon] [nvarchar](max) NULL,
  [lr_view] [int] NOT NULL,
 CONSTRAINT [PK_layout_ranges2] PRIMARY KEY CLUSTERED 
(
  [lr_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY];

CREATE TABLE [dbo].[layout_ranges_legends](
  [lrl_id] [int] IDENTITY(1,1) NOT NULL,
  [lrl_range_id] [int] NOT NULL,
  [lrl_label] [nvarchar](50) NOT NULL,
  [lrl_value1] [decimal](18, 2) NOT NULL,
  [lrl_value2] [decimal](18, 2) NULL,
  [lrl_operator] [nchar](10) NOT NULL,
  [lrl_color] [nvarchar](10) NOT NULL,
  [lrl_field] [nvarchar](250) NULL,
  [lrl_editable] [bit] NULL,
  [lrl_auto_assign_role] [int] NOT NULL DEFAULT ((0)),
  [lrl_auto_assign_priority] [int] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_layout_ranges_legends2] PRIMARY KEY CLUSTERED 
(
  [lrl_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY];

CREATE TABLE [dbo].[layout_recording](
  [lr_scene_id] [int] NOT NULL,
  [lr_date] [datetime] NOT NULL,
  [lr_floor] [int] NOT NULL,
  [lr_json] [varchar](max) NOT NULL,
 CONSTRAINT [PK_layout_recording] PRIMARY KEY CLUSTERED 
(
  [lr_scene_id] ASC,
  [lr_date] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY];
 
CREATE TABLE [dbo].[layout_site_tasks](
  [lst_id] [bigint] IDENTITY(1,1) NOT NULL,
  [lst_status] [int] NOT NULL,
  [lst_start] [datetime] NULL,
  [lst_end] [datetime] NULL,
  [lst_category] [int] NULL,
  [lst_subcategory] [int] NULL,
  [lst_terminal_id] [int] NULL,
  [lst_account_id] [bigint] NULL,
  [lst_description] [nvarchar](250) NULL,
  [lst_severity] [int] NULL,
  [lst_creation_user_id] [int] NOT NULL,
  [lst_creation] [datetime] NOT NULL,
  [lst_assigned_role_id] [int] NULL,
  [lst_assigned_user_id] [int] NULL,
  [lst_assigned] [datetime] NULL,
  [lst_accepted_user_id] [int] NULL,
  [lst_accepted] [datetime] NULL,
  [lst_scale_from_user_id] [int] NULL,
  [lst_scale_to_user_id] [int] NULL,
  [lst_scale_reason] [nvarchar](250) NULL,
  [lst_scale] [datetime] NULL,
  [lst_solved_user_id] [int] NULL,
  [lst_solved] [datetime] NULL,
  [lst_validate_user_id] [int] NULL,
  [lst_validate] [datetime] NULL,
  [lst_last_status_update_user_id] [int] NULL,
  [lst_last_status_update] [datetime] NULL,
  [lst_attached_media] [int] NULL,
  [lst_events_history] [nvarchar](max) NULL,
 CONSTRAINT [PK_layout_site_tasks] PRIMARY KEY CLUSTERED 
(
  [lst_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY];

CREATE TABLE [dbo].[layout_users_configuration](
  [lc_user_id] [int] NOT NULL,
  [lc_parameters] [varchar](max) NOT NULL,
  [lc_dashboard] [varchar](max) NULL,
  [lc_last_update] [datetime] NOT NULL,
  [lc_is_manager] [bit] NOT NULL DEFAULT ((0)),
  [lc_is_runner] [bit] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_layout_configuration] PRIMARY KEY CLUSTERED 
(
  [lc_user_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY];

CREATE TABLE [dbo].[layout_users_custom_filters](
  [lcf_id] [int] IDENTITY(1,1) NOT NULL,
  [lcf_user_id] [int] NOT NULL,
  [lcf_name] [nvarchar](50) NOT NULL,
  [lcf_filter_parameters] [nvarchar](max) NOT NULL,
  [lcf_enabled] [bit] NOT NULL,
  [lcf_last_update] [datetime] NOT NULL,
  [lcf_display] [nvarchar](max) NOT NULL
 CONSTRAINT [PK_layout_custom_filters] PRIMARY KEY CLUSTERED 
(
  [lcf_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY];

CREATE TABLE [dbo].[layout_views](
  [lv_id] [int] NOT NULL,
  [lv_desc] [nvarchar](500) NOT NULL,
  [lv_value] [nvarchar](max) NOT NULL,
  [lv_type] [int] NOT NULL,
 CONSTRAINT [PK_layout_views] PRIMARY KEY CLUSTERED 
(
  [lv_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY];

CREATE TABLE [dbo].[layout_media](
  [lm_id] [bigint] IDENTITY(1	,1) NOT NULL,  --UNIQUE ID
  [lm_data] [varbinary](max) NOT NULL,	  --datos bianrio de la imagen o media
  [lm_created] [datetime] NOT NULL,		  --fecha de creación 
  [lm_user_created] [int] NOT NULL,		  --Usuario que lo crea en la app (user id)
  [lm_type] [int] NOT NULL,				  --Familia (1=Máquina, 2=Jugador, 3= Sala) ENUMERADO
  [lm_external_id] [nvarchar](50) NULL,	  --Id externo (TerminalID, ObjectId)
  [lm_description] [nvarchar](250) NULL,    --Comentario o descripción
  [lm_media_type] [int] NOT NULL,           --Tipo de media (1=Foto, 2=Audio, 3= Video) ENUMERADO
  [lm_data_thumbnail] [varbinary](max) NULL,
 CONSTRAINT [PK_layout_media] PRIMARY KEY CLUSTERED 
(
  [lm_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY];

CREATE TABLE [dbo].[layout_site_alarms](
  [lsa_terminal_id] [int] NOT NULL,
  [lsa_alarm_id] [bigint] NOT NULL,
  [lsa_date_created] [datetime] NOT NULL,
  [lsa_id] [bigint] IDENTITY(1,1) NOT NULL,
  [lsa_alarm_type] [int] NOT NULL,
  [lsa_alarm_source] [int] NOT NULL,
  [lsa_user_created] [int] NOT NULL,
  [lsa_media_id] [bigint] NULL,
  [lsa_description] [nvarchar](max) NULL,
  [lsa_date_to_task] [datetime] NULL,
  [lsa_task_id] [bigint] NULL,
  [lsa_status] [int] NOT NULL CONSTRAINT [DF_layout_site_alarms_lsa_status]  DEFAULT ((0)),
 CONSTRAINT [PK_layout_site_alarms_1] PRIMARY KEY CLUSTERED 
(
  [lsa_terminal_id] ASC,
  [lsa_alarm_id] ASC,
  [lsa_date_created] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[layout_site_alarms]') AND name = N'IX_lsa_id')
DROP INDEX [IX_lsa_id] ON [dbo].[layout_site_alarms] WITH ( ONLINE = OFF );
CREATE NONCLUSTERED INDEX [IX_lsa_id] ON [dbo].[layout_site_alarms] 
(
  [lsa_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY];

CREATE TABLE [dbo].[layout_enums](
  [le_id] [bigint] NULL,
  [le_level_1] [bigint] NULL,
  [le_level_2] [bigint] NULL,
  [le_description] [nvarchar](250) NULL,
  [le_param_1] [nvarchar](50) NULL,
  [le_param_2] [nvarchar](50) NULL,
  [le_param_3] [bigint] NULL,
  [le_param_4] [bigint] NULL
) ON [PRIMARY];

CREATE TABLE [dbo].[layout_devices](
  [ld_id] [bigint] IDENTITY(1,1) NOT NULL,
  [ld_device_id] [nvarchar](50) NOT NULL,
  [ld_user_id] [bigint] NOT NULL,
  [ld_type] [int] NULL,
  [ld_model] [nvarchar](250) NULL,
  [ld_enabled] [bit] NULL,
  [ld_status] [int] NULL,
  [ld_date] [datetime] NULL,
  [ld_last_update] [datetime] NULL,
  [ld_device_name] [nvarchar](50)  NULL
 CONSTRAINT [PK_layout_device] PRIMARY KEY CLUSTERED 
(
  [ld_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY];

CREATE TABLE [dbo].[layout_runners_position](	
  [lrp_user_id] [bigint] NOT NULL, --id del usuario
  [lrp_device_id] [nvarchar](50) NOT NULL, --id del smartphone
  [lrp_floor_id] [nvarchar](50) NOT NULL,	-- id del piso
  [lrp_x] [float] NULL, --Si triangulamos, establece la x (coordenada)
  [lrp_y] [float] NULL, --Si triangulamos, establece la y (coordenada)
  [lrp_range] [float] NULL,  -- Si no triangulamos, sirve para mostrar la distancia en metros del beacon más cercano
  [lrp_last_update] [datetime] NULL,
 CONSTRAINT [PK_layout_runners_position] PRIMARY KEY CLUSTERED 
(
  [lrp_user_id] ASC,
  [lrp_device_id]  ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY];

CREATE TABLE [dbo].[layout_manager_runner_messages](
  [lmrm_id] [bigint] IDENTITY(1,1) NOT NULL,
  [lmrm_manager_id] [bigint] NOT NULL,
  [lmrm_runner_id] [bigint] NOT NULL,
  [lmrm_date] [datetime] NOT NULL,
  [lmrm_message] [text] NOT NULL,
  [lmrm_source] [int] NOT NULL,
 CONSTRAINT [PK_layout_manager_runner_messages] PRIMARY KEY CLUSTERED 
(
  [lmrm_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY];

CREATE TABLE [dbo].[layout_resources](
  [lr_id] [bigint] IDENTITY(1,1) NOT NULL,
  [lr_type] [int] NOT NULL,
  [lr_sub_type] [int] NOT NULL,
  [lr_name] [varchar](50) NULL,
  [lr_data] [varchar](max) NOT NULL,
  [lr_options] [varchar](max) NULL,
 CONSTRAINT [PK_layout_resources] PRIMARY KEY CLUSTERED 
(
  [lr_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY];

CREATE TABLE [dbo].[layout_mobile_application_versions](
  [lmav_id] [bigint] IDENTITY(1,1) NOT NULL,
  [lmav_version] [nchar](10) NOT NULL,
  [lmav_apk] [varbinary](max) NOT NULL,
  [lmav_date] [datetime] NOT NULL,
 CONSTRAINT [PK_layout_mobile_application_versions] PRIMARY KEY CLUSTERED 
(
  [lmav_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY];

----------------------------------------------------
--------------------INDEX ADDED---------------------
----------------------------------------------------

CREATE NONCLUSTERED INDEX [IX_lsa_date_created] ON [dbo].[layout_site_alarms] 
( [lsa_date_created] ASC
) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY];
 
CREATE NONCLUSTERED INDEX [IX_lst_solved] ON [dbo].[layout_site_tasks] 
( [lst_solved] ASC
) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY];

CREATE NONCLUSTERED INDEX [IX_layout_devices_deviceid] ON [dbo].[layout_devices]
( [ld_device_id] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];

CREATE NONCLUSTERED INDEX [X_layout_devices_userid] ON [dbo].[layout_devices]
( [ld_user_id] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];

CREATE NONCLUSTERED INDEX [XI_layout_objects_externalid] ON [dbo].[layout_objects]
( [lo_external_id] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];

CREATE NONCLUSTERED INDEX [XI_layout_devices_type] ON [dbo].[layout_devices]
( [ld_type] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];

CREATE NONCLUSTERED INDEX [XI_layout_site_alarms_status] ON [dbo].[layout_site_alarms]
( [lsa_status] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];

CREATE NONCLUSTERED INDEX [XI_layout_site_tasks_creation] ON [dbo].[layout_site_tasks]
( [lst_creation] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];

CREATE NONCLUSTERED INDEX [XI_layout_manager_runner_messages_date] ON [dbo].[layout_manager_runner_messages]
( [lmrm_date] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];

CREATE NONCLUSTERED INDEX [XI_layout_resources_type&id] ON [dbo].[layout_resources]
( [lr_id] ASC,
  [lr_type] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];

CREATE NONCLUSTERED INDEX [XI_layout_locations_floornumber] ON [dbo].[layout_locations]
( [loc_floor_number] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];

CREATE NONCLUSTERED INDEX [XI_layout_ranges_role] ON [dbo].[layout_ranges]
( [lr_role] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];

CREATE NONCLUSTERED INDEX [XI_layout_users_custom_filters_userid] ON [dbo].[layout_users_custom_filters]
( [lcf_user_id] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];

CREATE NONCLUSTERED INDEX [XI_layout_ranges_sectionid] ON [dbo].[layout_ranges]
( [lr_section_id] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];

CREATE NONCLUSTERED INDEX [XI_layout_site_alarms_datecreated] ON [dbo].[layout_site_alarms]
( [lsa_date_created] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];

CREATE NONCLUSTERED INDEX [XI_layout_runners_position_deviceid] ON [dbo].[layout_runners_position]
( [lrp_device_id] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];

CREATE NONCLUSTERED INDEX [XI_layout_recording_date] ON [dbo].[layout_recording]
( [lr_date] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];

----------------------------------------------------
---INITIAL SETTINGS - GENERAL PARAMS----------------
----------------------------------------------------

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'SmartFloor' AND GP_SUBJECT_KEY ='Enabled')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('SmartFloor'
             ,'Enabled'
             ,'1')

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'DATA.Service' AND GP_SUBJECT_KEY ='DATA_Param_ExpiracionConexion')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('DATA.Service'
             ,'DATA_Param_ExpiracionConexion'
             ,'61000')

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'DATA.Service' AND GP_SUBJECT_KEY ='DATA_Param_NET_MSG_Enabled')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('DATA.Service'
             ,'DATA_Param_NET_MSG_Enabled'
             ,'0');

----------------------------------------------------
--------FUNCTIONS-----------------------------------
----------------------------------------------------

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BinToHex_NEW]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[BinToHex_NEW]
(
    @BinValue           VARBINARY(MAX)
  , @IncludeSQL0xPrefix BIT = 1
)
RETURNS VARCHAR(MAX)
AS
BEGIN
  DECLARE @ReturnString VARCHAR(MAX)
  IF (@@MICROSOFTVERSION / 0x01000000 >= 10)
    BEGIN
      if @IncludeSQL0xPrefix = 1
        SET @ReturnString = CONVERT(varchar(max), @BinValue, 1)  
      ELSE 
        SET @ReturnString = CONVERT(varchar(max), @BinValue, 2)  
      END ELSE
            BEGIN
              DECLARE @i int
              DECLARE @length int
              DECLARE @hexstring char(16)
              SELECT   @ReturnString = CASE WHEN @IncludeSQL0xPrefix = 1 THEN ''0x'' ELSE '''' END
                     , @i = 1
                     , @length = DATALENGTH(@binvalue)
                     , @hexstring = ''0123456789abcdef''
      WHILE (@i <= @length) 
        BEGIN
          DECLARE @tempint   int
          DECLARE @firstint  int
          DECLARE @secondint int
          SET @tempint      = CONVERT(int, SUBSTRING(@binvalue, @i, 1))
          SET @firstint     = FLOOR(@tempint / 16)
          SET @secondint    = @tempint - (@firstint * 16)
          SET @ReturnString = @ReturnString + SUBSTRING(@hexstring, @firstint + 1, 1) + SUBSTRING(@hexstring, @secondint + 1, 1)
          SET @i = @i + 1
        END

    END

  RETURN @ReturnString

END' 
END;

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[getrate]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[getrate] 
(
  @START DATETIME,
  @FINISH DATETIME,
  @NOW  DATETIME
)
RETURNS FLOAT 
AS
BEGIN
  DECLARE @RATE FLOAT
  DECLARE @DIVIDEND FLOAT
  DECLARE @DIVISOR FLOAT
  
  SET @DIVIDEND = DATEDIFF(MILLISECOND, @START, @NOW )

  SET @DIVISOR =  DATEDIFF(MILLISECOND, @START, @FINISH)
  
  SET @RATE = 0
  
  IF @DIVISOR > 0 
      SET @RATE = (@DIVIDEND / @DIVISOR) 
  
  IF @RATE < 0 
      SET @RATE = - @RATE
      
  RETURN @RATE
END' 
END;

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_Get_Range_Date]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[SP_Get_Range_Date] 
(  @pDate AS DATETIME
, @pRange AS VARCHAR(1)
, @pGetTime AS BIT = 0
)
RETURNS DATETIME
AS
BEGIN

  DECLARE @_time AS VARCHAR(8)

  IF (@pRange = ''T'')
    BEGIN
      RETURN @pDate
    END

  -- Keep time
  IF @pGetTime = 0
    BEGIN
      SET @_time = ''''
    END
  ELSE
    BEGIN
      SET @_time = CONVERT(VARCHAR(8), @pDate, 108)
    END
  

  IF (@pRange = ''W'')
    BEGIN
      RETURN LEFT(CONVERT(nvarchar, DATEADD(WK, DATEDIFF(WK, 0, @pDate), 0), 120), 11) + @_time
    END

  IF (@pRange = ''M'')
    BEGIN
      RETURN LEFT(CONVERT(nvarchar, DATEADD(M, DATEDIFF(M, 0, @pDate), 0), 120), 11) + @_time
    END

  IF (@pRange = ''Y'')
    BEGIN
      RETURN LEFT(CONVERT(nvarchar, DATEADD(YY, DATEDIFF(YY, 0, @pDate), 0), 120), 11) + @_time
    END

  --IF (@pRange = ''L'')
  --  BEGIN
  --    RETURN ?
  --  END
    
  --SET @DATEFROM_WEEK = LEFT(CONVERT(nvarchar, @DATEFROM_WEEK, 120), 11) + convert(char(8), @_DATE, 108)  

  --SELECT 
  --   @pDate
  -- , DATEDIFF(WK, 0, @pDate)
  -- , DATEADD(WK, DATEDIFF(WK, 0, @pDate) - 1, 0)
  -- , CONVERT(VARCHAR(10), DATEADD(WK, DATEDIFF(WK, 0, @pDate) - 1, 0), 112)

RETURN @pDate
END' 
END;

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wsi_time_table]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[wsi_time_table]
( @pDateFrom AS DATETIME,
  @pDateTo   AS DATETIME,
  @pInterval AS INT,
  @pIncrement AS INT
  )
RETURNS  
@ReturnValue TABLE (TIME_DATE DATETIME)
AS 
BEGIN  

--DECLARE   @ReturnValue TABLE (TIME_DATE DATETIME)
--DECLARE   @pDateFrom AS DATETIME = ''2014-10-22''
--DECLARE   @pDateTo   AS DATETIME = ''2014-10-15''
--DECLARE   @pInterval AS INT = 0
--DECLARE   @pIncrement AS INT = -1

DECLARE @_DAY_VAR AS DATETIME
SET @_DAY_VAR = @pDateFrom
WHILE (@pIncrement < 0 AND @_DAY_VAR > @pDateTo) OR (@pIncrement >= 0 AND @_DAY_VAR < @pDateTo)
   BEGIN
          -- LINK WITH TABLE TYPES
          INSERT INTO   @ReturnValue (TIME_DATE) VALUES (@_DAY_VAR)
          
          -- SET INCREMENT
          SET @_DAY_VAR = CASE 
                               WHEN @pInterval = 0 THEN DATEADD(DAY,@pIncrement,@_DAY_VAR)     -- DAY
                               WHEN @pInterval = 1 THEN DATEADD(MONTH,@pIncrement,@_DAY_VAR)   -- MONTH
                               WHEN @pInterval = 2 THEN DATEADD(YEAR,@pIncrement,@_DAY_VAR)    -- YEAR
                          END
   END
   
   --SELECT * FROM @ReturnValue
   RETURN 
END' 
END;

----------------------------------------------------
--------STORED PROCEDURES---------------------------
----------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_AssignDeviceToUser]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_AssignDeviceToUser]
GO 
CREATE PROCEDURE [dbo].[Layout_AssignDeviceToUser]
      @pUserId INT
    , @pDeviceId Nvarchar(50)
AS
BEGIN
  IF EXISTS (SELECT TOP 1 *
        FROM layout_devices ld
        WHERE ld.ld_device_id = @pDeviceId)
      
    UPDATE	layout_devices
        SET  ld_user_id= @pUserId,
           ld_last_update = GETDATE()
        WHERE ld_device_id = @pDeviceId
  ELSE IF EXISTS (SELECT TOP 1 *
        FROM layout_devices ld
        WHERE ld.ld_user_id = @pUserId)
      
    UPDATE	layout_devices
        SET  ld_device_id= @pDeviceId,
           ld_last_update = GETDATE()
        WHERE ld_user_id = @pUserId
  ELSE
    INSERT INTO layout_devices	
                    (ld_device_id,
                    ld_user_id,
                    ld_last_update)
                    VALUES
                    (@pDeviceId,
                    @pUserId,
                    GETDATE())
END 
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_SaveSiteTask]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_SaveSiteTask]
GO
CREATE PROCEDURE [dbo].[Layout_SaveSiteTask]
      @pTaskId INT
    , @pStatus INT
    , @pStart DATETIME
    , @pEnd DATETIME
    , @pCategory INT
    , @pSubcategory INT
    , @pTerminalId INT
    , @pAccountId INT
    , @pDescription NVARCHAR(MAX)
    , @pSeverity INT
    , @pCreationUserId INT
    , @pCreation DATETIME
    , @pAssignedRoleId INT
    , @pAssignedUserId INT
    , @pAssigned DATETIME
    , @pAcceptedUserId INT
    , @pAccepted DATETIME
    , @pScaleFromUserId INT
    , @pScaleToUserId INT
    , @pScaleReason NVARCHAR(250)
    , @pScale DATETIME
    , @pSolvedUserId INT
    , @pSolved DATETIME
    , @pValidateUserId INT
    , @pValidate DATETIME
    , @pLastStatusUpdateUserId INT
    , @pLastStatusUpdate DATETIME
    , @pAttachedMedia INT
    , @pHistory NVARCHAR(MAX)
    , @pOutputFilterId INT OUTPUT
AS
BEGIN
      IF EXISTS (SELECT TOP 1 * 
                   FROM LAYOUT_SITE_TASKS
                  WHERE	LST_ID = @pTaskId)
                    
            UPDATE	LAYOUT_SITE_TASKS
               SET	LST_STATUS = @pStatus
                 ,  LST_START = @pStart
                 ,  LST_END = @pEnd
                 ,  LST_CATEGORY = @pCategory 
                 ,  LST_SUBCATEGORY = @pSubcategory 
                 ,  LST_TERMINAL_ID = @pTerminalId
                 ,  LST_ACCOUNT_ID = @pAccountId 
                 ,  LST_DESCRIPTION = @pDescription 
                 ,  LST_SEVERITY = @pSeverity 
                 ,  LST_CREATION_USER_ID = @pCreationUserId 
                 ,  LST_CREATION = @pCreation 
                 ,  LST_ASSIGNED_ROLE_ID = @pAssignedRoleId 
                 ,  LST_ASSIGNED_USER_ID = @pAssignedUserId 
                 ,  LST_ASSIGNED = @pAssigned 
                 ,  LST_ACCEPTED_USER_ID = @pAcceptedUserId 
                 ,  LST_ACCEPTED = @pAccepted 
                 ,  LST_SCALE_FROM_USER_ID = @pScaleFromUserId 
                 ,  LST_SCALE_TO_USER_ID = @pScaleToUserId 
                 ,  LST_SCALE_REASON = @pScaleReason 
                 ,  LST_SCALE = @pScale 
                 ,  LST_SOLVED_USER_ID = @pSolvedUserId 
                 ,  LST_SOLVED = @pSolved 
                 ,  LST_VALIDATE_USER_ID = @pValidateUserId 
                 ,  LST_VALIDATE = @pValidate 
                 ,  LST_LAST_STATUS_UPDATE_USER_ID = @pLastStatusUpdateUserId 
                 ,  LST_LAST_STATUS_UPDATE = @pLastStatusUpdate 
                 ,  LST_ATTACHED_MEDIA = @pAttachedMedia
                 ,  LST_EVENTS_HISTORY = @pHistory
             WHERE	LST_ID = @pTaskId
      ELSE
       INSERT INTO	LAYOUT_SITE_TASKS
           (       LST_STATUS
                 , LST_START
                 , LST_END
                 , LST_CATEGORY
                 , LST_SUBCATEGORY
                 , LST_TERMINAL_ID
                 , LST_ACCOUNT_ID
                 , LST_DESCRIPTION
                 , LST_SEVERITY
                 , LST_CREATION_USER_ID
                 , LST_CREATION
                 , LST_ASSIGNED_ROLE_ID
                 , LST_ASSIGNED_USER_ID
                 , LST_ASSIGNED
                 , LST_ACCEPTED_USER_ID
                 , LST_ACCEPTED
                 , LST_SCALE_FROM_USER_ID
                 , LST_SCALE_TO_USER_ID
                 , LST_SCALE_REASON
                 , LST_SCALE
                 , LST_SOLVED_USER_ID
                 , LST_SOLVED
                 , LST_VALIDATE_USER_ID
                 , LST_VALIDATE
                 , LST_LAST_STATUS_UPDATE_USER_ID
                 , LST_LAST_STATUS_UPDATE
                 , LST_ATTACHED_MEDIA
                 , LST_EVENTS_HISTORY)
            VALUES 
                  ( @pStatus 
                  , @pStart 
                  , @pEnd 
                  , @pCategory 
                  , @pSubcategory 
                  , @pTerminalId 
                  , @pAccountId 
                  , @pDescription 
                  , @pSeverity 
                  , @pCreationUserId 
                  , @pCreation 
                  , @pAssignedRoleId 
                  , @pAssignedUserId 
                  , @pAssigned 
                  , @pAcceptedUserId 
                  , @pAccepted 
                  , @pScaleFromUserId 
                  , @pScaleToUserId 
                  , @pScaleReason 
                  , @pScale 
                  , @pSolvedUserId 
                  , @pSolved 
                  , @pValidateUserId 
                  , @pValidate 
                  , @pLastStatusUpdateUserId 
                  , @pLastStatusUpdate 
                  , @pAttachedMedia
                  , @pHistory
                  )
SELECT @pOutputFilterId = SCOPE_IDENTITY();
END 
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_AutoNotificationAlarm]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_AutoNotificationAlarm]
GO 
CREATE PROCEDURE [dbo].[Layout_AutoNotificationAlarm]
     @pStatus INT
    , @pCategory INT
    , @pSubcategory INT
    , @pTerminalId INT
    , @pSeverity INT
    , @pCreationUserId INT
    , @pCreation DATETIME
    , @pAssignedRoleId INT
    , @pAssigned DATETIME
    , @pHistory NVARCHAR(MAX)
    , @pDateCreated DATETIME
    , @poutTaskId INT OUTPUT
AS
BEGIN
DECLARE @TaskId as INT

exec Layout_SaveSiteTask -1,@pStatus,NULL,NULL,@pCategory,@pSubcategory,@pTerminalId,NULL,NULL,@pSeverity,@pCreationUserId,@pCreation,@pAssignedRoleId,NULL,@pAssigned,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,@pAssigned,NULL,@pHistory,@TaskId OUTPUT

  UPDATE LAYOUT_SITE_ALARMS
     SET LSA_TASK_ID      = @TaskId
       , LSA_DATE_TO_TASK = @pDateCreated
   WHERE LSA_TERMINAL_ID  = @pTerminalId
     AND LSA_ALARM_ID     = @pSubcategory
     AND LSA_DATE_CREATED = @pDateCreated
     
     SELECT @poutTaskId = @TaskId;
END 
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_Available_Banks]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_Available_Banks]
GO 
CREATE PROCEDURE [dbo].[Layout_Available_Banks]
AS
BEGIN
  SET NOCOUNT ON;

SELECT   BK_BANK_ID
       , BK_AREA_ID
       , TE_TERMINAL_ID
       , BK_NAME
  FROM   BANKS  
  LEFT   JOIN TERMINALS ON BANKS.BK_BANK_ID = TERMINALS.TE_BANK_ID
 WHERE   BK_BANK_ID NOT IN (SELECT   DISTINCT(TABLE_OBJECTS.LOP_BANK_ID)
                              FROM   (SELECT   LOP.LOP_ID,
                                               LOP.LOP_X,
                                               LOP.LOP_Y,
                                               LOP.LOP_Z,
                                               LOP.LOP_ORIENTATION,
                                               LOP.LOP_FLOOR_ID,
                                               LOP.LOP_AREA_ID,
                                               LOP.LOP_BANK_ID,
                                               LOP.LOP_POSITION_DATE
                                        FROM   LAYOUT_OBJECTS_POSITIONS LOP
                                       INNER   JOIN
                                                   (SELECT   LOP_ID
                                                           , MAX(LOP_POSITION_DATE) AS LOP_POSITION_DATE
                                                      FROM   LAYOUT_OBJECTS_POSITIONS
                                                     GROUP   BY LOP_ID) AS G_LOP 
                                                        ON   LOP.LOP_ID            = G_LOP.LOP_ID 
                                                       AND   LOP.LOP_POSITION_DATE = G_LOP.LOP_POSITION_DATE) TABLE_OBJECTS) 
                                                       
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_Available_Objects]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_Available_Objects]
GO 
CREATE PROCEDURE [dbo].[Layout_Available_Objects]  
  @pValidTerminalTypes   AS VARCHAR(MAX) = NULL
AS
BEGIN
  DECLARE @_DELIMITER                 AS CHAR(1)
  DECLARE @_VALID_TERMINALS_TYPES     TABLE(SST_ID INT, SST_VALUE VARCHAR(50))
  SET @_DELIMITER            = ','
  
  IF @pValidTerminalTypes IS NULL
  BEGIN
    SET @pValidTerminalTypes = '-1, 100, 105'
  END
  
  INSERT   INTO @_VALID_TERMINALS_TYPES 
  SELECT   * 
    FROM   dbo.SplitStringIntoTable(@pValidTerminalTypes, @_DELIMITER, DEFAULT)

  -- Get terminals not in layout
  SELECT   te_terminal_id
         , te_floor_id
         , te_bank_id
         , te_terminal_type
         , te_name
         , te_provider_id 
         , te_status
         , te_active
    FROM   terminals 
   WHERE   (te_terminal_type NOT IN (SELECT SST_VALUE FROM @_VALID_TERMINALS_TYPES)) 
     AND   (te_status = 0) 
     AND   (te_active = 1)
     AND   (te_terminal_id NOT IN (SELECT DISTINCT lo_external_id FROM layout_objects))
   ORDER   BY te_provider_id
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_CreateFloorLocations]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_CreateFloorLocations]
GO 
CREATE PROCEDURE Layout_CreateFloorLocations (

  @pFloorId INT
, @pWidth INT
, @pHeight INT
, @pType INT
) AS
BEGIN
  --SET @pFloorId = 999
  --SET @pWidth = 100
  --SET @pHeight = 100
  --SET @pType = 1

  SET NOCOUNT ON

  DECLARE @_loc_x AS INT
  DECLARE @_loc_y AS INT
  DECLARE @_creation AS DATETIME
  DECLARE @_last_id AS INT
  DECLARE @_memory AS TABLE ( loc_id INT, loc_x INT, loc_y INT, loc_creation DATETIME, loc_type INT, loc_floor_number INT, loc_grid_id VARCHAR(50) )
  SET @_creation = GETDATE()

  SELECT @_last_id = ISNULL(MAX(loc_id), 0) FROM layout_locations

  -- Create locations on the floor
  SET @_loc_x = 0
  WHILE (@_loc_x < @pWidth)
    BEGIN
      SET @_loc_y = 0
      WHILE (@_loc_y < @pHeight)
        BEGIN
          SET @_last_id = @_last_id + 1

          INSERT   INTO @_memory
                 ( loc_id, loc_x, loc_y, loc_creation, loc_type, loc_floor_number, loc_grid_id )
          VALUES ( @_last_id, @_loc_x, @_loc_y, @_creation, @pType, @pFloorId, CAST(@_loc_x AS VARCHAR) + '-' + CAST(@_loc_y AS VARCHAR))
          
          SET @_loc_y = @_loc_y + 1 
        END
      SET @_loc_x = @_loc_x + 1
    END

  SET IDENTITY_INSERT layout_locations ON

  INSERT    INTO layout_locations 
          ( loc_id, loc_x, loc_y, loc_creation, loc_type, loc_floor_number, loc_grid_id )
  SELECT    loc_id
          , loc_x
          , loc_y
          , loc_creation
          , loc_type
          , loc_floor_number
          , loc_grid_id 
     FROM   @_memory
     
  SET IDENTITY_INSERT layout_locations OFF   
  
  SELECT * FROM @_memory
END  
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_Object_Update]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_Object_Update]
GO 
CREATE PROCEDURE [dbo].[Layout_Object_Update]        
    @pObjectId        BIGINT
   ,@pPositionDate    DATETIME
   ,@pPosX            INTEGER         
   ,@pPosY            INTEGER
   ,@pPosZ            INTEGER
   ,@pOrientation     INTEGER
   ,@pFloorId         BIGINT
   ,@pAreaId          BIGINT
   ,@pBankId          BIGINT
   ,@pExternalId      BIGINT
   ,@pType            INTEGER
   ,@pParentId        BIGINT   = NULL 
   ,@pMeshId          INTEGER
   ,@pCreationDate    DATETIME
   ,@pUpdateDate      DATETIME
AS
BEGIN 
   INSERT   INTO LAYOUT_OBJECTS_POSITIONS 
          ( lop_id
          , lop_position_date
          , lop_x
          , lop_y
          , lop_z
          , lop_orientation
          , lop_floor_id
          , lop_area_id
          , lop_bank_id )
   VALUES ( @pObjectId
          , @pPositionDate
          , @pPosX
          , @pPosY
          , @pPosZ
          , @pOrientation
          , @pFloorId
          , @pAreaId
          , @pBankId )

   UPDATE   LAYOUT_OBJECTS
      SET   lo_external_id = @pExternalId
          , lo_type = @pType
          , lo_parent_id = @pParentId
          , lo_mesh_id = @pMeshId
          , lo_update_date = @pUpdateDate
    WHERE   lo_id = @pObjectId

   IF @@ROWCOUNT <> 1
   BEGIN
      INSERT   INTO LAYOUT_OBJECTS
             ( lo_id
             , lo_external_id
             , lo_type
             , lo_parent_id
             , lo_mesh_id
             , lo_creation_date )
      VALUES ( @pObjectId
             , @pExternalId
             , @pType
             , @pParentId 
             , @pMeshId
             , @pCreationDate )       
   END
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_Editor_Available_Objects]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_Editor_Available_Objects]
GO 
CREATE PROCEDURE [dbo].[Layout_Editor_Available_Objects]
AS
BEGIN
  SELECT   LOC_FLOOR_NUMBER               AS FLOOR_ID
         , LO_ID                          AS OBJ_ID
         , ISNULL(LO_TYPE,1)              AS OBJ_TYPE
         , TE_TERMINAL_ID                 AS OBJ_IDENTITY
         , TE_NAME                        AS OBJ_NAME
         , TE_BANK_ID                     AS BANK_ID
         , BK_NAME                        AS BANK_NAME
         , LOL_OFFSET_X                   AS X
         , LOL_OFFSET_Y                   AS X
         , LOL_ORIENTATION                AS ORIENTATION
    FROM   TERMINALS
    LEFT   JOIN LAYOUT_OBJECTS ON LO_EXTERNAL_ID = TE_TERMINAL_ID
    LEFT   JOIN LAYOUT_OBJECT_LOCATIONS ON LO_ID = LOL_OBJECT_ID
    LEFT   JOIN LAYOUT_LOCATIONS ON LOL_LOCATION_ID = LOC_ID
    LEFT   JOIN BANKS ON TE_BANK_ID = BK_BANK_ID
   
   UNION   ALL
  
  SELECT   LOC_FLOOR_NUMBER   AS FLOOR_ID
         , LO_ID              AS OBJ_ID
         , LD_TYPE            AS OBJ_TYPE
         , LD_ID              AS OBJ_IDENTITY
         , LD_DEVICE_ID       AS OBJ_NAME
         , NULL               AS BANK_ID
         , NULL               AS BANK_NAME
         , LOL_OFFSET_X       AS X
         , LOL_OFFSET_Y       AS Y
         , 0                  AS ORIENTATION
    FROM   LAYOUT_DEVICES 
    LEFT   JOIN LAYOUT_OBJECTS ON LD_ID = LO_EXTERNAL_ID AND LD_TYPE = 70
    LEFT   JOIN LAYOUT_OBJECT_LOCATIONS ON LO_ID = LOL_LOCATION_ID 
    LEFT   JOIN LAYOUT_LOCATIONS ON LOL_LOCATION_ID = LOC_ID
   WHERE   LD_TYPE = 70 

  ---- TERMINALS
  --SELECT   LOP_FLOOR_ID
  --       , LO_ID 
  --       , ISNULL(LO_TYPE,1) AS OBJ_TYPE
  --       , TE_TERMINAL_ID AS OBJ_IDENTITY
  --       , TE_NAME AS OBJ_NAME
  --       , TE_BANK_ID  AS OBJ_BANK
  --       , BK_NAME
  --       , LO_TYPE
  --       , LOP_X
  --       , LOP_Y
  --       , LOP_ORIENTATION
  --  FROM   LAYOUT_OBJECTS
  -- RIGHT   JOIN TERMINALS ON LO_EXTERNAL_ID = TE_TERMINAL_ID
  --  LEFT   JOIN LAYOUT_OBJECTS_POSITIONS ON LO_ID = LOP_ID
  -- INNER   JOIN BANKS ON TE_BANK_ID = BK_BANK_ID
   
  --UNION ALL
  ---- GAMING TABLES
  --SELECT   LOP_FLOOR_ID
  --       , LO_ID 
  --       , 2 AS OBJ_TYPE
  --       , GT_GAMING_TABLE_ID AS OBJ_IDENTITY
  --       , GT_NAME AS OBJ_NAME
  --       , GT_BANK_ID AS OBJ_BANK
  --       , BK_NAME
  --       , LO_TYPE
  --       , LOP_X
  --       , LOP_Y
  --       , LOP_ORIENTATION
  --  FROM   LAYOUT_OBJECTS
  -- RIGHT   JOIN GAMING_TABLES ON LO_EXTERNAL_ID = GT_GAMING_TABLE_ID
  --  LEFT   JOIN LAYOUT_OBJECTS_POSITIONS ON LO_ID = LOP_ID
  -- INNER   JOIN BANKS ON GT_BANK_ID = BK_BANK_ID
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetActivity]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_GetActivity]
GO 
CREATE PROCEDURE [dbo].[Layout_GetActivity]
AS
BEGIN
DECLARE @terminal_name_type AS VARCHAR(50)
	DECLARE @big_won_limit AS MONEY
	CREATE TABLE #temp_table    (LO_ID INT
							   , TERMINAL_ID INT
							   , TERMINAL_STATUS INT
							   , PLAY_SESSION_STATUS INT
							   , IS_ANONYMOUS INT
							   , PLAYER_ACCOUNT_ID INT
							   , PLAYER_NAME VARCHAR(50)
							   , PLAYER_GENDER INT
							   , PLAYER_AGE INT
							   , PLAYER_LEVEL INT
							   , PLAYER_IS_VIP INT
							   , AL_SAS_HOST_ERROR INT
							   , AL_DOOR_FLAGS INT 
							   , AL_BILL_FLAGS INT
							   , AL_PRINTER_FLAGS INT
							   , AL_EGM_FLAGS INT
							   , AL_PLAYED_WON_FLAGS INT
							   , AL_JACKPOT_FLAGS INT
							   , AL_STACKER_STATUS INT
							   , AL_CALL_ATTENDANT_FLAGS INT
							   , AL_MACHINE_FLAGS INT		
							   , AL_BIG_WON BIT	      
							   , PLAYED_AMOUNT MONEY
							   , WON_AMOUNT MONEY
							   , PLAY_SESSION_BET_AVERAGE MONEY
							   , TERMINAL_BET_AVERAGE MONEY
							   , PLAY_SESSION_DURATION INT
							   , PLAY_SESSION_TOTAL_PLAYED MONEY
							   , PLAY_SESSION_PLAYED_COUNT INT
							   , TERMINAL_BANK_ID INT
							   , TERMINAL_AREA_ID INT
							   , TERMINAL_PROVIDER_NAME VARCHAR(50)
							   , PLAY_SESSION_WON_AMOUNT MONEY
							   , NET_WIN MONEY
							   , PLAY_SESSION_NET_WIN MONEY
							   , TERMINAL_NAME NVARCHAR(50)
							   , TERMINAL_PLAYED_COUNT INT
							   , PLAYER_TRACKDATA NVARCHAR(50)
							   , PLAYER_LEVEL_NAME NVARCHAR(50)
                 , ALARMS NVARCHAR(MAX)
                 , TERMINAL_DENOMINATION MONEY
							   , PLAYER_CREATED INT
                 , PS_rate float  -- Test field
                 , TERMINAL_PAYOUT MONEY
                 , THEORETICAL_HOLD MONEY
							   )
							   
	-- TERMINAL NAME TYPE
	SELECT @terminal_name_type = GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'SasHost' AND GP_SUBJECT_KEY = 'DisplayName.Format'
	IF (@terminal_name_type IS NULL)
	BEGIN
	  SET @terminal_name_type = '%Name'
	END

	-- TODO: Read from GP
	SET @big_won_limit = 1000

  DECLARE @Now AS DATETIME
  DECLARE @CurrentHour AS INT
  DECLARE @HourNow AS DATETIME
  DECLARE @NextHourNow AS DATETIME
  
  SET @Now = GETDATE()
  SET @CurrentHour = DATEPART(HOUR, @Now)
  SET @HourNow = DATEADD(HOUR, DATEDIFF(HOUR, 0, @Now), 0)
  SET @NextHourNow = DATEADD(HOUR, 1, @HourNow)

  DECLARE @DefaultHold MONEY
  SET @DefaultHold = (SELECT   1 - CAST (GP_KEY_VALUE AS MONEY) / 100        
                        FROM   GENERAL_PARAMS                                
                       WHERE   GP_GROUP_KEY   = 'PlayerTracking'               
                         AND   GP_SUBJECT_KEY = 'TerminalDefaultPayout')

	INSERT INTO #temp_table
	SELECT LO.LO_ID
	
		 -- Terminal info
		 , LO.LO_EXTERNAL_ID AS TERMINAL_ID
		 , TE.TE_STATUS	AS TERMINAL_STATUS

		 -- Play Session info
		 , PLAY_SESSION_STATUS = CASE  WHEN (PS_PLAY_SESSION_ID IS NOT NULL) THEN 2  
								 ELSE 1 END

		 -- Player info
		 , IS_ANONYMOUS = CASE  WHEN (TE.TE_CURRENT_ACCOUNT_ID IS NOT NULL) THEN CASE WHEN (AC.AC_HOLDER_NAME IS NOT NULL) THEN 0 ELSE 1 END 
								ELSE NULL END
		 , PS.PS_ACCOUNT_ID AS PLAYER_ACCOUNT_ID
		 , PLAYER_NAME = CASE WHEN (TE.TE_CURRENT_ACCOUNT_ID IS NOT NULL) THEN ISNULL(AC.AC_HOLDER_NAME, 'Anonymous')
						      ELSE NULL END
		 , AC.AC_HOLDER_GENDER AS PLAYER_GENDER
		 , DATEDIFF(YEAR, AC.AC_HOLDER_BIRTH_DATE, @Now) AS PLAYER_AGE
		 , PLAYER_LEVEL = CASE  WHEN (TE.TE_CURRENT_ACCOUNT_ID IS NOT NULL) THEN CASE WHEN (AC.AC_HOLDER_NAME IS NOT NULL) THEN AC.AC_HOLDER_LEVEL ELSE -1 END 
								ELSE 0 END
 		 , ISNULL(AC.AC_HOLDER_IS_VIP, 0) AS PLAYER_IS_VIP
		 
		 -- Flags
		 , TS_SAS_HOST_ERROR AS AL_SAS_HOST_ERROR
		 , ISNULL(TS_DOOR_FLAGS, 0) AS AL_DOOR_FLAGS 
		 , ISNULL(TS_BILL_FLAGS, 0) AS AL_BILL_FLAGS
		 , ISNULL(TS_PRINTER_FLAGS, 0) AS AL_PRINTER_FLAGS
		 , ISNULL(TS_EGM_FLAGS, 0) AS AL_EGM_FLAGS
		 , ISNULL(TS_PLAYED_WON_FLAGS, 0) AS AL_PLAYED_WON_FLAGS
		 , ISNULL(TS_JACKPOT_FLAGS, 0) AS AL_JACKPOT_FLAGS
		 , ISNULL(TS_STACKER_STATUS, 0) AS AL_STACKER_STATUS
		 , ISNULL(TS_CALL_ATTENDANT_FLAGS, 0) AS AL_CALL_ATTENDANT_FLAGS
		 , ISNULL(TS_MACHINE_FLAGS, 0) AS AL_MACHINE_FLAGS		
		 , AL_BIG_WON = CASE WHEN ISNULL(SPH.WON_AMOUNT, 0) > @big_won_limit THEN 1
				             ELSE 0 END
				             
		 -- Money info
		 , ISNULL(SPH.PLAYED_AMOUNT , 0) AS PLAYED_AMOUNT
		 , ISNULL(SPH.WON_AMOUNT, 0) AS WON_AMOUNT
		 , PLAY_SESSION_BET_AVERAGE = CASE WHEN (PS.PS_PLAYED_COUNT > 0) THEN ROUND((PS.PS_TOTAL_PLAYED / PS.PS_PLAYED_COUNT), 2)
										   ELSE NULL END
		 , TERMINAL_BET_AVERAGE = CASE WHEN (SPH.PLAYED_COUNT > 0) THEN ROUND((SPH.PLAYED_AMOUNT / SPH.PLAYED_COUNT), 2)
									   ELSE NULL END
		 , DATEDIFF(SECOND, ISNULL(PS.PS_STARTED, @Now), @Now) AS PLAY_SESSION_DURATION
		 , PS.PS_TOTAL_PLAYED AS PLAY_SESSION_TOTAL_PLAYED
		 , PS.PS_PLAYED_COUNT AS PLAY_SESSION_PLAYED_COUNT
		 
		 -- Area & Bank
		 , BK.bk_bank_id AS TERMINAL_BANK_ID
		 , BK.bk_area_id AS TERMINAL_AREA_ID
		 , PR.pv_name AS TERMINAL_PROVIDER_NAME
		 
		 , PS.PS_WON_AMOUNT AS PLAY_SESSION_WON_AMOUNT
		 
		 , SPH.PLAYED_AMOUNT - SPH.WON_AMOUNT AS NET_WIN
		 , PS.PS_TOTAL_PLAYED - PS.PS_WON_AMOUNT AS PLAY_SESSION_NET_WIN
		 --, TE.TE_NAME AS TERMINAL_NAME
		 	, CASE WHEN @terminal_name_type = '%FloorId' THEN
		          TE.TE_NAME + ' [' + TE.TE_FLOOR_ID + ']'
		        WHEN @terminal_name_type = '%BaseName' THEN
		          -- TODO: Find correct field
		          -- TE.TE_BASE_NAME
		          TE.TE_NAME
		        ELSE 
		          TE.TE_NAME
		   END AS TERMINAL_NAME
		 , SPH.PLAYED_COUNT AS TERMINAL_PLAYED_COUNT
		 , AC.AC_TRACK_DATA AS PLAYER_TRACKDATA
		 , ISNULL(LEVELS.NAME,'') AS PLAYER_LEVEL_NAME
     , NULL AS ALARMS
     , TE.TE_DENOMINATION AS TERMINAL_DENOMINATION
     , CASE WHEN (AC.AC_CREATED >= @HourNow AND AC.AC_CREATED < @NextHourNow) THEN 1 ELSE 0 END AS PLAYER_CREATED
     , NULL AS PS_RATE
     , ISNULL(TE.TE_THEORETICAL_HOLD, @DefaultHold) AS TERMINAL_PAYOUT
     , (PS.PS_TOTAL_PLAYED * ISNULL(TE.TE_THEORETICAL_HOLD, @DefaultHold)) AS THEORETICAL_HOLD
     
	  FROM LAYOUT_OBJECTS AS LO
	  
		INNER JOIN TERMINALS AS TE ON TE.TE_TERMINAL_ID = LO.LO_EXTERNAL_ID 
		INNER JOIN PROVIDERS AS PR ON PR.pv_id = TE.te_prov_id
		
		LEFT JOIN BANKS AS BK ON BK.bk_bank_id = TE.te_bank_id
				
		LEFT JOIN (SELECT PS_TERMINAL_ID
		                , MAX(PS_PLAY_SESSION_ID) AS PS_PLAY_SESSION_ID
		                , PS_ACCOUNT_ID
		                , PS_TOTAL_PLAYED
		                , PS_WON_AMOUNT
		                , PS_PLAYED_COUNT
		                , PS_STARTED
		                , PS_PLAYED_AMOUNT
				     FROM PLAY_SESSIONS
				    WHERE PS_STATUS = 0
				 GROUP BY PS_TERMINAL_ID, PS_ACCOUNT_ID, PS_TOTAL_PLAYED, PS_WON_AMOUNT, PS_PLAYED_COUNT, PS_PLAYED_AMOUNT, PS_STARTED) AS PS ON TE.TE_TERMINAL_ID = PS.PS_TERMINAL_ID
				 
		LEFT JOIN ACCOUNTS AS AC ON AC.AC_ACCOUNT_ID = TE.TE_CURRENT_ACCOUNT_ID
		
		LEFT JOIN TERMINAL_STATUS AS TS ON TE.TE_TERMINAL_ID = TS.TS_TERMINAL_ID
		
		LEFT JOIN (SELECT SPH_TERMINAL_ID 
					    , SUM(SPH_PLAYED_AMOUNT)AS PLAYED_AMOUNT
					    , SUM(SPH_WON_AMOUNT) AS WON_AMOUNT
					    , SUM(SPH_PLAYED_COUNT) AS PLAYED_COUNT
		             FROM SALES_PER_HOUR_V2
		            WHERE SPH_BASE_HOUR >= DATEADD(DAY, DATEDIFF(DAY, 0, @Now), 0) 
			          AND SPH_BASE_HOUR <= @Now
			     GROUP BY SPH_TERMINAL_ID) AS SPH ON SPH.SPH_TERMINAL_ID = TE.TE_TERMINAL_ID
			     
		LEFT JOIN (SELECT CAST(REPLACE(REPLACE(GP_SUBJECT_KEY,'Level',''),'.Name','') AS INT) AS NUM
					    , GP_KEY_VALUE AS NAME
					 FROM GENERAL_PARAMS 
					WHERE GP_GROUP_KEY ='PlayerTracking'
					  AND GP_SUBJECT_KEY LIKE 'Level%.Name'
				  ) AS LEVELS ON LEVELS.NUM = (CASE  WHEN (TE.TE_CURRENT_ACCOUNT_ID IS NOT NULL) THEN 
																			CASE WHEN (AC.AC_HOLDER_NAME IS NOT NULL) THEN AC.AC_HOLDER_LEVEL 
																			ELSE -1 END 
																		ELSE 0 END)
		
	 WHERE LO.LO_TYPE = 1
			     	
	-- TERMINAL group
	SELECT * FROM #temp_table
	
	-- BANK group
	SELECT TERMINAL_BANK_ID AS BANK_ID
	     , SUM(PLAY_SESSION_STATUS - 1) AS NUM_ACTIVE_PLAY_SESSIONS
		 , CASE WHEN (SUM(PLAY_SESSION_STATUS - 1) - SUM(IS_ANONYMOUS)) > 0 THEN 100 - ((SUM(PLAYER_GENDER - 1) * 100)/ (SUM(PLAY_SESSION_STATUS - 1) - SUM(IS_ANONYMOUS)))
				ELSE NULL END AS PLAYERS_MALE_PERCENTAGE
		 , CASE WHEN (SUM(PLAY_SESSION_STATUS - 1) - SUM(IS_ANONYMOUS)) > 0 THEN ((SUM(PLAYER_GENDER - 1) * 100)/ (SUM(PLAY_SESSION_STATUS - 1) - SUM(IS_ANONYMOUS)))
				ELSE NULL END AS PLAYERS_FEMALE_PERCENTAGE
		 , CASE WHEN (SUM(PLAY_SESSION_STATUS - 1) - SUM(IS_ANONYMOUS)) > 0 THEN (SUM(PLAYER_AGE) / (SUM(PLAY_SESSION_STATUS - 1) - SUM(IS_ANONYMOUS)))
				ELSE NULL END AS PLAYERS_AGE_AVERAGE
		 , SUM(PLAYER_IS_VIP) AS NUM_VIPS
		 , SUM(PLAYED_AMOUNT) AS PLAYED_AMOUNT
		 , SUM(WON_AMOUNT) AS WON_AMOUNT
		 , CASE WHEN (SUM(PLAY_SESSION_STATUS - 1) - SUM(IS_ANONYMOUS)) > 0 THEN ROUND((SUM(PLAY_SESSION_BET_AVERAGE) / (SUM(PLAY_SESSION_STATUS - 1) - SUM(IS_ANONYMOUS))), 2)
				ELSE NULL END  AS PLAY_SESSIONS_BET_AVERAGE
		 , CASE WHEN COUNT(TERMINAL_ID) > 0 THEN ROUND((SUM(TERMINAL_BET_AVERAGE) / COUNT(TERMINAL_ID)), 2) 
		        ELSE NULL END AS TERMINALS_BET_AVERAGE
	  FROM #temp_table 
  GROUP BY TERMINAL_BANK_ID
	
	-- AREA group
	SELECT TERMINAL_AREA_ID AS AREA_ID
		 , SUM(PLAY_SESSION_STATUS - 1) AS NUM_ACTIVE_PLAY_SESSIONS
		 , CASE WHEN (SUM(PLAY_SESSION_STATUS - 1) - SUM(IS_ANONYMOUS)) > 0 THEN 100 - ((SUM(PLAYER_GENDER - 1) * 100)/ (SUM(PLAY_SESSION_STATUS - 1) - SUM(IS_ANONYMOUS)))
				ELSE NULL END AS PLAYERS_MALE_PERCENTAGE
		 , CASE WHEN (SUM(PLAY_SESSION_STATUS - 1) - SUM(IS_ANONYMOUS)) > 0 THEN ((SUM(PLAYER_GENDER - 1) * 100)/ (SUM(PLAY_SESSION_STATUS - 1) - SUM(IS_ANONYMOUS)))
				ELSE NULL END AS PLAYERS_FEMALE_PERCENTAGE
		 , CASE WHEN (SUM(PLAY_SESSION_STATUS - 1) - SUM(IS_ANONYMOUS)) > 0 THEN (SUM(PLAYER_AGE) / (SUM(PLAY_SESSION_STATUS - 1) - SUM(IS_ANONYMOUS)))
				ELSE NULL END AS PLAYERS_AGE_AVERAGE
		 , SUM(PLAYER_IS_VIP) AS NUM_VIPS
		 , SUM(PLAYED_AMOUNT) AS PLAYED_AMOUNT
		 , SUM(WON_AMOUNT) AS WON_AMOUNT
		 , CASE WHEN (SUM(PLAY_SESSION_STATUS - 1) - SUM(IS_ANONYMOUS)) > 0 THEN ROUND((SUM(PLAY_SESSION_BET_AVERAGE) / (SUM(PLAY_SESSION_STATUS - 1) - SUM(IS_ANONYMOUS))), 2)
				ELSE NULL END  AS PLAY_SESSIONS_BET_AVERAGE
		 , CASE WHEN COUNT(TERMINAL_ID) > 0 THEN ROUND((SUM(TERMINAL_BET_AVERAGE) / COUNT(TERMINAL_ID)), 2) 
		        ELSE NULL END AS TERMINALS_BET_AVERAGE
	  FROM #temp_table 
  GROUP BY TERMINAL_AREA_ID
	
	DROP TABLE #temp_table
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetAlarmsAndTasks]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_GetAlarmsAndTasks]
GO  
CREATE PROCEDURE [dbo].[Layout_GetAlarmsAndTasks] 
AS
BEGIN
  DECLARE @DateFrom as DATETIME
  DECLARE @DateTo as DATETIME
  DECLARE @MaxElapsedTime as INT

  SET @DateFrom = dbo.TodayOpening(0)
  SET @DateTo = DATEADD (DAY, 1, @DateFrom)
  SET @MaxElapsedTime = 48

  SELECT --TOP 5
         LSA_TERMINAL_ID AS TERMINAL_ID
       , LSA_ALARM_ID AS ALARM_ID
       , LSA_DATE_CREATED AS DATE_CREATION
       , CASE WHEN (DATEDIFF(second,LSA_DATE_CREATED,GETDATE()) / 3600) < @MaxElapsedTime THEN
         RIGHT('0' + CAST(DATEDIFF(second,LSA_DATE_CREATED,GETDATE()) / 3600 AS VARCHAR),2) + ':' +
         RIGHT('0' + CAST((DATEDIFF(second,LSA_DATE_CREATED,GETDATE()) / 60) % 60 AS VARCHAR),2)  + ':' +
         RIGHT('0' + CAST(DATEDIFF(second,LSA_DATE_CREATED,GETDATE()) % 60 AS VARCHAR),2)
         ELSE 'ÿ'
         END
       , LSA_ID AS ID
       , LSA_ALARM_TYPE AS ALARM_TYPE
       , LSA_USER_CREATED AS USER_CREATION
       , LSA_MEDIA_ID
       , CAST('' as xml).value('xs:base64Binary(sql:column("LM_DATA_THUMBNAIL"))','varchar(max)') as LST_ATTACHED_MEDIA
       , LSA_DESCRIPTION AS AL_DESCRIPTION
       , LSA_ALARM_SOURCE AS ALARM_SOURCE
       , LSA_STATUS
       , TE_NAME
       --, LSA_DATE_TO_TASK AS 
       --, LSA_TASK_ID
    FROM LAYOUT_SITE_ALARMS
    LEFT JOIN TERMINALS ON LSA_TERMINAL_ID=TE_TERMINAL_ID
    LEFT JOIN LAYOUT_MEDIA ON LAYOUT_SITE_ALARMS.LSA_MEDIA_ID=LAYOUT_MEDIA.LM_ID
    WHERE LSA_STATUS IN (0,2)
      AND LSA_TASK_ID IS NULL
      AND LSA_DATE_TO_TASK IS NULL
      AND (LSA_DATE_CREATED>= dbo.TodayOpening(0) OR LSA_STATUS=2)
      

  SELECT LST_ID
       , LST_STATUS
       , LST_START
       , LST_END
       , LST_CATEGORY
       , LST_SUBCATEGORY
       , LST_TERMINAL_ID
       , LST_ACCOUNT_ID
       , LST_DESCRIPTION
       , LST_SEVERITY
       , LST_CREATION_USER_ID
       , LST_CREATION
       , LST_ASSIGNED_ROLE_ID
       , LST_ASSIGNED_USER_ID
       , LST_ASSIGNED
       , LST_ACCEPTED_USER_ID
       , LST_ACCEPTED
       , LST_SCALE_FROM_USER_ID
       , LST_SCALE_TO_USER_ID
       , LST_SCALE_REASON
       , LST_SCALE
       , LST_SOLVED_USER_ID
       , LST_SOLVED
       , LST_VALIDATE_USER_ID
       , LST_VALIDATE
       , LST_LAST_STATUS_UPDATE_USER_ID
       , LST_LAST_STATUS_UPDATE
       , LM_ID
       --, '' as LST_ATTACHED_MEDIA
       , CAST('' as xml).value('xs:base64Binary(sql:column("LM_DATA_THUMBNAIL"))','varchar(max)') as LST_ATTACHED_MEDIA  
       , CASE  
         WHEN LST_STATUS = 5 THEN --CONVERT(varchar, DATEADD(MS, DATEDIFF(second,LST_CREATION,LST_VALIDATE)* 1000, 0),114)
         CASE WHEN (DATEDIFF(second,LST_CREATION,LST_VALIDATE) / 3600)<@MaxElapsedTime THEN
         RIGHT('0' + CAST(DATEDIFF(second,LST_CREATION,LST_VALIDATE) / 3600 AS VARCHAR),2) + ':' +
         RIGHT('0' + CAST((DATEDIFF(second,LST_CREATION,LST_VALIDATE) / 60) % 60 AS VARCHAR),2)  + ':' +
         RIGHT('0' + CAST(DATEDIFF(second,LST_CREATION,LST_VALIDATE) % 60 AS VARCHAR),2)
         ELSE 'ÿ'
         END
         WHEN lst_status = 3 THEN --CONVERT(varchar, DATEADD(MS, DATEDIFF(second,LST_CREATION,LST_SOLVED)* 1000, 0),114)--
         CASE WHEN (DATEDIFF(second,LST_CREATION,LST_SOLVED) / 3600)<@MaxElapsedTime THEN       
         RIGHT('0' + CAST(DATEDIFF(second,LST_CREATION,LST_SOLVED) / 3600 AS VARCHAR),2) + ':' +
         RIGHT('0' + CAST((DATEDIFF(second,LST_CREATION,LST_SOLVED) / 60) % 60 AS VARCHAR),2)  + ':' +
         RIGHT('0' + CAST(DATEDIFF(second,LST_CREATION,LST_SOLVED) % 60 AS VARCHAR),2)
         ELSE 'ÿ'       
         END
         ELSE --CONVERT(varchar, DATEADD(MS, DATEDIFF(second,LST_CREATION,GETDATE())* 1000, 0),114)
         CASE WHEN (DATEDIFF(second,LST_CREATION,GETDATE()) / 3600)<@MaxElapsedTime THEN       
         RIGHT('0' + CAST(DATEDIFF(second,LST_CREATION,GETDATE()) / 3600 AS VARCHAR),2) + ':' +
         RIGHT('0' + CAST((DATEDIFF(second,LST_CREATION,GETDATE()) / 60) % 60 AS VARCHAR),2)  + ':' +
         RIGHT('0' + CAST(DATEDIFF(second,LST_CREATION,GETDATE()) % 60 AS VARCHAR),2)
         ELSE 'ÿ'       
         END
         END 
       , LST_EVENTS_HISTORY
       , CASE WHEN GU_USERNAME IS NULL THEN
          CASE WHEN (LST_ASSIGNED_ROLE_ID & 1 = 1)  THEN  'PCA'
               WHEN (LST_ASSIGNED_ROLE_ID & 2 = 2)  THEN  'OPM'
               WHEN (LST_ASSIGNED_ROLE_ID & 4 = 4)  THEN  'SLA'
               WHEN (LST_ASSIGNED_ROLE_ID & 8 = 8)  THEN  'TSP'
          END
         ELSE GU_USERNAME END 
    FROM LAYOUT_SITE_TASKS
    LEFT JOIN LAYOUT_MEDIA ON LAYOUT_SITE_TASKS.LST_ATTACHED_MEDIA=LAYOUT_MEDIA.LM_ID
    left JOIN GUI_USERS ON GU_USER_ID=LST_ASSIGNED_USER_ID
    WHERE (LST_CREATION >= dbo.TodayOpening(0) OR LST_STATUS IN (2,1))
    --WHERE LST_STATUS NOT IN (4)
    --WHERE @DateFrom <= LST_CREATION 
    --  AND @DateTo > LST_CREATION 
    
  SELECT GU_USER_ID,GU_USERNAME 
    FROM GUI_USERS
   WHERE GU_PROFILE_ID IN (
          SELECT GPF_PROFILE_ID 
            FROM GUI_PROFILE_FORMS
           WHERE GPF_GUI_ID = 203
        GROUP BY GPF_PROFILE_ID
  )

  SELECT RS.LST_TERMINAL_ID
       , RS.LST_CATEGORY
       , RS.LST_SUBCATEGORY
       , RS.LST_SEVERITY
       , RS.LST_ASSIGNED_ROLE_ID
       , RS.LST_ASSIGNED_USER_ID
       , RS.LST_CREATION
       , RS.LST_STATUS
      FROM (
          SELECT *, Rank() 
            OVER (PARTITION BY LST_TERMINAL_ID
                  ORDER BY LST_TERMINAL_ID,LST_CREATION DESC ) AS Rank
          FROM LAYOUT_SITE_TASKS
          ) rs WHERE Rank <= 10
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetAssignedTaskToUser]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_GetAssignedTaskToUser]
GO
CREATE PROCEDURE [dbo].[Layout_GetAssignedTaskToUser]
  @loginId bigint
WITH EXEC AS CALLER
AS
BEGIN
  DECLARE @terminal_name_type AS VARCHAR(50)
  DECLARE @big_won_limit AS MONEY
  DECLARE  @Date AS DATETIME
  SET @Date = GETDATE()
  DECLARE   @IntervalHour INT 
  SET @IntervalHour = 1

  SET @big_won_limit = 1000
  
  DECLARE @Now AS DATETIME
  DECLARE @CurrentHour AS BIGINT
  DECLARE @HourNow AS DATETIME
  DECLARE @NextHourNow AS DATETIME
  
  SET @Now = CAST(DATEPART(YEAR, @Date) AS VARCHAR) + '-' +  CAST(DATEPART(MONTH, @Date) AS VARCHAR)  + '-' +  CAST(DATEPART(DAY, @Date) AS VARCHAR)  + ' ' +  CAST(DATEPART(HOUR, @Date) AS VARCHAR) + ':' + CAST(DATEPART(MINUTE, GETDATE()) AS VARCHAR) + ':' + CAST(DATEPART(SECOND, GETDATE()) AS VARCHAR) + '.000'
    
  SET @CurrentHour = DATEPART(HOUR, @Now)
  SET @HourNow = DATEADD(HOUR, DATEDIFF(HOUR, 0, @Now), 0)
  SET @NextHourNow = DATEADD(HOUR, @IntervalHour, @HourNow)
  
  SELECT  LST.LST_ID,
          LST.LST_STATUS,
          LST.LST_START,
          LST.LST_END,
          LST.LST_CATEGORY,
          LST.LST_SUBCATEGORY,
          LST.LST_TERMINAL_ID,
          LST.LST_ACCOUNT_ID,
          LST.LST_DESCRIPTION,
          LST.LST_SEVERITY,
          LST.LST_CREATION,
          LST.LST_ASSIGNED_ROLE_ID,
          LST.LST_ASSIGNED_USER_ID,
          TE.TE_NAME,
          TE.TE_PROVIDER_ID,
          TE.TE_MODEL,
          A.AR_NAME,
          BK.BK_NAME,
          PLAY_SESSION_STATUS = CASE  WHEN (PS_PLAY_SESSION_ID IS NOT NULL) THEN 2  ELSE 1 END,
          IS_ANONYMOUS = CASE  WHEN (PS.PS_ACCOUNT_ID IS NOT NULL) THEN CASE WHEN (AC.AC_HOLDER_NAME IS NOT NULL) THEN 0 ELSE 1 END ELSE NULL END,
          PS.PS_ACCOUNT_ID AS PLAYER_ACCOUNT_ID,
          PLAYER_NAME = CASE WHEN (PS.PS_ACCOUNT_ID IS NOT NULL) THEN ISNULL(AC.AC_HOLDER_NAME, 'ANONYMOUS') ELSE NULL END,
          ISNULL(AC.AC_HOLDER_GENDER, 0) AS PLAYER_GENDER,
          DATEDIFF(YEAR, AC.AC_HOLDER_BIRTH_DATE, @NOW) AS PLAYER_AGE,
          PLAYER_LEVEL = AC.AC_HOLDER_LEVEL,
          ISNULL(AC.AC_HOLDER_IS_VIP, 0) AS PLAYER_IS_VIP
    FROM  LAYOUT_SITE_TASKS AS LST
   INNER  JOIN TERMINALS AS TE ON LST.LST_TERMINAL_ID = TE.TE_TERMINAL_ID
   INNER  JOIN PROVIDERS AS PR ON PR.PV_ID = TE.TE_PROV_ID
    LEFT  JOIN BANKS AS BK ON BK.BK_BANK_ID = TE.TE_BANK_ID
   INNER  JOIN AREAS a ON a.AR_AREA_ID = bk.BK_AREA_ID 
    LEFT  JOIN (SELECT PS_TERMINAL_ID
                , PS_PLAY_SESSION_ID
                , PS_ACCOUNT_ID
                , PS_TOTAL_PLAYED
                , PS_WON_AMOUNT
                , PS_PLAYED_COUNT
                , PS_STARTED
                , DBO.GETRATE(PS_STARTED,PS_FINISHED, @NOW) AS PS_RATE
         FROM PLAY_SESSIONS
         WHERE ( ps_started <= @Now AND ps_finished > @Now )
         AND ps_played_count>0) AS PS ON TE.TE_TERMINAL_ID = PS.PS_TERMINAL_ID --AND PS_STARTED >= @NOW
  LEFT JOIN ACCOUNTS AS AC ON AC.AC_ACCOUNT_ID = PS.PS_ACCOUNT_ID
  LEFT JOIN TERMINAL_STATUS AS TS ON TE.TE_TERMINAL_ID = TS.TS_TERMINAL_ID
  LEFT JOIN (SELECT SPH_TERMINAL_ID 
            , SUM(SPH_PLAYED_AMOUNT)*DBO.GETRATE(DATEADD(DAY, DATEDIFF(DAY, 0, @NOW), 0) ,DATEADD(DAY, DATEDIFF(DAY, 0, @NOW), 1), @NOW)  AS PLAYED_AMOUNT
            , SUM(SPH_WON_AMOUNT)*DBO.GETRATE(DATEADD(DAY, DATEDIFF(DAY, 0, @NOW), 0) ,DATEADD(DAY, DATEDIFF(DAY, 0, @NOW), 1), @NOW)    AS WON_AMOUNT
            , SUM(SPH_PLAYED_COUNT)*DBO.GETRATE(DATEADD(DAY, DATEDIFF(DAY, 0, @NOW), 0) ,DATEADD(DAY, DATEDIFF(DAY, 0, @NOW), 1), @NOW)  AS PLAYED_COUNT
               FROM SALES_PER_HOUR
              WHERE SPH_BASE_HOUR >= DATEADD(DAY, DATEDIFF(DAY, 0, @NOW), 0) 
                AND SPH_BASE_HOUR <= DATEADD(DAY, DATEDIFF(DAY, 0, @NOW), 1) 
  
         GROUP BY SPH_TERMINAL_ID) AS SPH ON SPH.SPH_TERMINAL_ID = TE.TE_TERMINAL_ID
         
  LEFT JOIN (SELECT CAST(REPLACE(REPLACE(GP_SUBJECT_KEY,'LEVEL',''),'.NAME','') AS INT) AS NUM
            , GP_KEY_VALUE AS NAME
         FROM GENERAL_PARAMS 
        WHERE GP_GROUP_KEY ='PLAYERTRACKING'
          AND GP_SUBJECT_KEY LIKE 'LEVEL%.NAME'
          ) AS LEVELS ON LEVELS.NUM = AC.AC_HOLDER_LEVEL 
  WHERE lst_status IN (1,2)
  AND ((lst_assigned_user_id = @loginId)
  OR  (lst_accepted_user_id = @loginId)
  OR  ((lst_assigned_role_id IN (SELECT   DISTINCT(GPF_FORM_ID / 1000000) AS ROLE
                 FROM   GUI_PROFILE_FORMS
                 WHERE   GPF_GUI_ID = 203 AND GPF_READ_PERM <> 0
                 AND   GPF_PROFILE_ID = (
                                SELECT   GU_PROFILE_ID
                                  FROM   GUI_USERS
                                 WHERE   GU_USER_ID    = @loginId
                               )
                 AND   (GPF_FORM_ID / 1000000) > 0))  AND ( isnull(lst_accepted_user_id, 0) = 0)))
END 
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetAssignedDeviceByUserId]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_GetAssignedDeviceByUserId]
GO  
CREATE PROCEDURE [dbo].[Layout_GetAssignedDeviceByUserId]
      @pUserId INT
    , @pDeviceId Nvarchar(50) OUTPUT
AS
BEGIN
  SELECT   @pDeviceId = LD.LD_DEVICE_ID
    FROM   LAYOUT_DEVICES LD
   WHERE   LD.LD_USER_ID = @pUserId
END 
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetAssignedDevicesByRoleId]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_GetAssignedDevicesByRoleId]
GO
CREATE PROCEDURE [dbo].[Layout_GetAssignedDevicesByRoleId]
      @pRoleId INT
AS
BEGIN
  SELECT   DISTINCT(GU_USER_ID)
    FROM   GUI_USERS
   WHERE   GU_PROFILE_ID IN (SELECT   GPF_PROFILE_ID
                               FROM   GUI_PROFILE_FORMS
                              WHERE   GPF_GUI_ID = 202 
                                AND   GPF_READ_PERM <> 0
                                AND   (GPF_FORM_ID / 1000000) = @pRoleId)
END 
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetBeaconsAvailables]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_GetBeaconsAvailables]
GO  
CREATE PROCEDURE [dbo].[Layout_GetBeaconsAvailables]
  @pBeaconEnumId INT
AS
BEGIN
  SELECT   LO_ID            AS OBJ_ID
         , LD_TYPE          AS OBJ_TYPE
         , LD_ID            AS OBJ_IDENTITY
         , LD_DEVICE_ID     AS OBJ_NAME
         , LOC_X            AS X
         , LOC_Y            AS Y
         , LOC_FLOOR_NUMBER AS FLOOR_ID
    FROM   LAYOUT_DEVICES 
   INNER   JOIN LAYOUT_OBJECTS ON LD_ID = LO_EXTERNAL_ID AND LD_TYPE = 70
   INNER   JOIN LAYOUT_OBJECT_LOCATIONS ON LOL_OBJECT_ID = LO_ID AND LOL_CURRENT_LOCATION = 1
   INNER   JOIN LAYOUT_LOCATIONS ON LOL_LOCATION_ID = LOC_ID
   WHERE   LD_TYPE = @pBeaconEnumId
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetCashiersActivity]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_GetCashiersActivity]
GO  
CREATE PROCEDURE [dbo].[Layout_GetCashiersActivity]
AS
BEGIN
  DECLARE @Today AS DATETIME
  SET @Today = GETDATE()
  
  SELECT   CT.CT_CASHIER_ID AS CASHIER_ID
         , CT.CT_NAME AS CASHIER_NAME
         , CS.CS_USER_ID AS CASHIER_USER_ID
         , GU.GU_USERNAME AS CASHIER_USER_NAME
         , CS.CS_STATUS AS SESSION_STATUS
         , CS.CS_BALANCE AS SESSION_BALANCE
         , ISNULL(CS.CS_COLLECTED_AMOUNT, 0) AS SESSION_COLLECTED_AMOUNT
         , ISNULL(CS.CS_TOTAL_SOLD, 0) AS SESSION_TOTAL_SOLD
    FROM   CASHIER_TERMINALS AS CT
   INNER   JOIN CASHIER_SESSIONS AS CS ON CS.CS_CASHIER_ID = CT.CT_CASHIER_ID
   INNER   JOIN GUI_USERS AS GU ON GU.GU_USER_ID = CS.CS_USER_ID
   WHERE   CS.CS_OPENING_DATE > DATEADD(DAY, DATEDIFF(DAY, 0, @Today), 0) 
     AND   CS.CS_STATUS IN (0, 2) 
     AND   CS.CS_SESSION_BY_TERMINAL = 0
   ORDER   BY CT.CT_CASHIER_ID
END 
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetConfiguration]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_GetConfiguration]
GO  
CREATE PROCEDURE [dbo].[Layout_GetConfiguration]
  @pUserId INT
AS
BEGIN
  SELECT   LC_PARAMETERS
          , LC_DASHBOARD
    FROM   LAYOUT_USERS_CONFIGURATION
    WHERE   LC_USER_ID = @pUserId
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetConversationHistory]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_GetConversationHistory]
GO  
CREATE PROCEDURE [dbo].[Layout_GetConversationHistory] 
  @pManagerId INT,
  @pRunnerId INT
AS
BEGIN
  SELECT   LMRM_ID
         , LMRM_MANAGER_ID
         , U_MANAGER.GU_USERNAME as MANAGER_NAME
         , LMRM_RUNNER_ID
         , U_RUNNER.GU_USERNAME as RUNNER_NAME
         , LMRM_DATE
         , LMRM_MESSAGE
         , LMRM_SOURCE
    FROM   LAYOUT_MANAGER_RUNNER_MESSAGES LMRM
   INNER   JOIN GUI_USERS U_MANAGER ON LMRM.LMRM_MANAGER_ID = U_MANAGER.GU_USER_ID 
   INNER   JOIN GUI_USERS U_RUNNER ON LMRM.LMRM_RUNNER_ID = U_RUNNER.GU_USER_ID 
   WHERE   LMRM.LMRM_DATE >= CAST(GETDATE() AS DATETIME)
     AND   LMRM_MANAGER_ID = @pManagerId
     AND   LMRM_RUNNER_ID = @pRunnerId
   ORDER   BY LMRM.LMRM_DATE
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetConversationsOfRunner]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_GetConversationsOfRunner]
GO  
CREATE PROCEDURE [dbo].[Layout_GetConversationsOfRunner] 
  @pRunnerId INT
AS
BEGIN
  SELECT   LMRM_MANAGER_ID
         , U_MANAGER.GU_USERNAME as MANAGER_NAME
    FROM   LAYOUT_MANAGER_RUNNER_MESSAGES LMRM
   INNER   JOIN GUI_USERS U_MANAGER ON LMRM.LMRM_MANAGER_ID = U_MANAGER.GU_USER_ID 
   INNER   JOIN GUI_USERS U_RUNNER ON LMRM.LMRM_RUNNER_ID = U_RUNNER.GU_USER_ID 
   WHERE   LMRM.LMRM_DATE >= CAST(GETDATE() AS DATETIME)
     AND   LMRM_RUNNER_ID = @pRunnerId
   GROUP   BY LMRM_MANAGER_ID
         , U_MANAGER.GU_USERNAME
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetFloorResources]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_GetFloorResources]
GO  
CREATE PROCEDURE [dbo].[Layout_GetFloorResources] (
  @pFloorId AS INT
) AS 
BEGIN
  SELECT * FROM (
    SELECT   lr_id
           , lr_type
           , lr_sub_type
           , lr_name
           , lr_data
           , lr_options
      FROM   layout_resources
     WHERE   (lr_type = 0 AND lr_id IN ( SELECT   DISTINCT lo_mesh_id 
                                          FROM   layout_objects
                                          LEFT   JOIN layout_object_locations ON lol_object_id   = lo_id
                                          LEFT   JOIN layout_locations        ON lol_location_id = loc_id
                                         WHERE   loc_floor_number             = @pFloorId
                                           AND   lol_current_location         = 1 ))
        OR   (lr_type = 1 AND lr_sub_type IN (SELECT   DISTINCT lo_mesh_id 
                                                FROM   layout_objects
                                                LEFT   JOIN layout_object_locations ON lol_object_id   = lo_id
                                                LEFT   JOIN layout_locations        ON lol_location_id = loc_id
                                               WHERE   loc_floor_number             = @pFloorId
                                                 AND   lol_current_location         = 1 ))
     UNION
    SELECT   lr_id
           , lr_type
           , lr_sub_type
           , lr_name
           , lr_data
           , lr_options
      FROM   layout_resources
     WHERE   (lr_type = 0 AND lr_sub_type IN ( 200, 201, 500 ))
        OR   (lr_type = 1 AND lr_sub_type IN ( SELECT   lr_id
                                                 FROM   layout_resources
                                                WHERE   lr_sub_type IN (200, 201, 500)))
     UNION
    SELECT   lr_id
           , lr_type
           , lr_sub_type
           , lr_name
           , lr_data
           , lr_options
      FROM   layout_resources
     WHERE   (lr_type = 1 AND lr_sub_type = 100)
  ) AS TRESOURCES ORDER BY 2 DESC
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetFloors]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_GetFloors]
GO  
CREATE PROCEDURE [dbo].[Layout_GetFloors]
AS
BEGIN
  SELECT   LF_FLOOR_ID
         , LF_NAME
         , LF_HEIGHT
         , '' AS LF_GEOMETRY
         , LF_IMAGE_MAP
         , LF_COLOR
         , LF_WIDTH
         , LF_METRIC
    FROM   LAYOUT_FLOORS
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetGamesStatistics]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_GetGamesStatistics]
GO  
CREATE PROCEDURE [dbo].[Layout_GetGamesStatistics]
      @pTerminalId	INT = NULL    
AS
BEGIN
  SELECT   SPH.SPH_TERMINAL_ID AS TERMINAL_ID
         , SPH.SPH_GAME_ID AS GAME_ID
         , ISNULL(PG.PG_GAME_NAME, 'UNKNOWN') AS GAME_NAME
         , SUM(SPH.SPH_PLAYED_AMOUNT)AS PLAYED_AMOUNT
         , SUM(SPH.SPH_WON_AMOUNT) AS WON_AMOUNT
         , SUM(SPH.SPH_PLAYED_COUNT) AS PLAYED_COUNT
         , SUM(SPH.SPH_WON_COUNT) AS WON_COUNT
    FROM   SALES_PER_HOUR_V2 AS SPH
    LEFT   JOIN PROVIDERS_GAMES AS PG ON PG.PG_GAME_ID = SPH.SPH_GAME_ID
   WHERE   SPH.SPH_TERMINAL_ID = ISNULL(@pTerminalId, SPH.SPH_TERMINAL_ID) 
     AND   SPH.SPH_BASE_HOUR >= DATEADD(DAY, DATEDIFF(DAY, 0, GETDATE()), 0) 
   GROUP   BY SPH.SPH_GAME_ID, PG_GAME_NAME, SPH_TERMINAL_ID
   ORDER   BY SPH.SPH_TERMINAL_ID
END 
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetMediaData]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_GetMediaData]
GO  
CREATE PROCEDURE [dbo].[Layout_GetMediaData]
      @pId INT
    , @pMediaData nvarchar(MAX) OUTPUT
AS
BEGIN
  SELECT   @pMediaData= CAST('' as xml).value('xs:base64Binary(sql:column("LM_DATA"))','varchar(max)')
    FROM   LAYOUT_MEDIA
   WHERE   LM_ID = @pId
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetNotIncludedObjects]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_GetNotIncludedObjects]
GO 
CREATE PROCEDURE [dbo].[Layout_GetNotIncludedObjects]
AS
BEGIN
  SELECT TE.TE_TERMINAL_ID
     , TE.TE_NAME
     , PR.PV_NAME
     , TE.TE_BANK_ID
     , BK.BK_AREA_ID
     , TE_FLOOR_ID
     , TE_TERMINAL_TYPE
    FROM TERMINALS AS TE
       INNER JOIN PROVIDERS AS PR ON PR.PV_ID = TE.TE_PROV_ID
       INNER JOIN BANKS AS BK ON BK.BK_BANK_ID = TE.TE_BANK_ID
   WHERE TE_TERMINAL_ID NOT IN (SELECT LO_EXTERNAL_ID 
                                  FROM LAYOUT_OBJECTS
                                 WHERE LO_TYPE = 1) 
                                 
  SELECT AR_AREA_ID
     , AR_NAME
     , AR_SMOKING
    FROM AREAS
   WHERE AR_AREA_ID NOT IN (SELECT LO_EXTERNAL_ID 
                              FROM LAYOUT_OBJECTS
                             WHERE LO_TYPE = 2) 
   
   SELECT BK_BANK_ID
      , BK_NAME
     FROM BANKS
    WHERE BK_BANK_ID NOT IN (SELECT LO_EXTERNAL_ID 
                               FROM LAYOUT_OBJECTS
                              WHERE LO_TYPE = 3) 
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetObjectInfo]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_GetObjectInfo]
GO
CREATE PROCEDURE [dbo].[Layout_GetObjectInfo]
      @pExternalObjectId INT = NULL
AS
BEGIN
  -- LAST OBJECTS POSITIONS
  SELECT   LOP.LOP_ID,
           LOP.LOP_X,
           LOP.LOP_Y,
           LOP.LOP_Z,
           LOP.LOP_ORIENTATION,
           LOP.LOP_FLOOR_ID,
           LOP.LOP_AREA_ID,
           LOP.LOP_BANK_ID,
           LOP.LOP_POSITION_DATE
    INTO   #LOP_LAST_OBJECTS_POSITIONS
    FROM   LAYOUT_OBJECTS_POSITIONS LOP
   INNER   JOIN (SELECT   LOP_ID
                        , MAX(LOP_POSITION_DATE) AS LOP_POSITION_DATE
                   FROM   LAYOUT_OBJECTS_POSITIONS
                  GROUP   BY LOP_ID) AS G_LOP ON   LOP.LOP_ID = G_LOP.LOP_ID AND LOP.LOP_POSITION_DATE = G_LOP.LOP_POSITION_DATE

-- QUERY LAYOUT OBJECTS
  SELECT   LO.LO_ID
         , LO.LO_EXTERNAL_ID
         , LO.LO_TYPE
         , LO.LO_PARENT_ID 
         , LO.LO_MESH_ID
         , LME.LME_TYPE
         , LME.LME_SUB_TYPE		  
         , LO.LO_CREATION_DATE
         , LO.LO_UPDATE_DATE
         , LOP.LOP_X
         , LOP.LOP_Y
         , LOP.LOP_Z
         , LOP.LOP_ORIENTATION	
         , LOP.LOP_FLOOR_ID
         , LOP.LOP_AREA_ID
         , LOP.LOP_BANK_ID
         , LOP.LOP_POSITION_DATE
    FROM   LAYOUT_OBJECTS AS LO
   INNER   JOIN LAYOUT_MESHES AS LME ON LME.LME_MESH_ID = LO.LO_MESH_ID
   INNER   JOIN #LOP_LAST_OBJECTS_POSITIONS AS LOP ON LOP.LOP_ID = LO.LO_ID 
   INNER   JOIN LAYOUT_FLOORS AS LF ON LF.LF_FLOOR_ID = LOP.LOP_FLOOR_ID
   WHERE   LO_EXTERNAL_ID = ISNULL(@pExternalObjectId, LO_EXTERNAL_ID)
  
  -- DROP TEMPORARY TABLE 
  DROP TABLE #LOP_LAST_OBJECTS_POSITIONS
END 
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetPlayerActivity]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_GetPlayerActivity]
GO
CREATE PROCEDURE [dbo].[Layout_GetPlayerActivity]
  @pAccountId VARCHAR(MAX)
  , @pOnlyActivePlaySession BIT 
AS
BEGIN
  DECLARE @Today as DATETIME
  
  SET @Today = GETDATE()
  
  SELECT   AC_ACCOUNT_ID AS ACCOUNT_ID
         , PS.PS_TERMINAL_ID AS TERMINAL_ID
         , TE.TE_NAME AS TERMINAL_NAME
         , PR.PV_NAME AS PROVIDER_NAME
         , SUM(DATEDIFF(SECOND, PS.PS_STARTED, ISNULL(PS.PS_FINISHED, @Today))) AS TIME_PLAYED
         , SUM(PS.PS_TOTAL_PLAYED) AS TOTAL_PLAYED
         , SUM(PS.PS_WON_AMOUNT) AS TOTAL_WON
         , SUM(PS.PS_PLAYED_COUNT) AS PLAYED_COUNT
         , CASE WHEN SUM(PS.PS_PLAYED_COUNT) > 0 THEN ROUND(SUM(PS.PS_TOTAL_PLAYED) / SUM(PS.PS_PLAYED_COUNT), 2) ELSE NULL END AS BET_AVERAGE 
         , AC_TRACK_DATA AS TRACK_DATA
    FROM   ACCOUNTS AS AC
    LEFT   JOIN PLAY_SESSIONS AS PS ON PS.PS_ACCOUNT_ID = AC.AC_ACCOUNT_ID 
     AND   PS.ps_started >= DATEADD(DAY, DATEDIFF(DAY, 0, @Today), 0)
     AND   CASE WHEN @pOnlyActivePlaySession = 1 THEN 0 ELSE (ISNULL(PS.PS_STATUS,0) + 1) END <> PS.PS_STATUS
    LEFT   JOIN TERMINALS AS TE ON TE.TE_TERMINAL_ID = PS.PS_TERMINAL_ID
    LEFT   JOIN PROVIDERS AS PR ON PR.PV_ID = TE.TE_PROV_ID
   WHERE   AC.AC_ACCOUNT_ID IN (SELECT CAST(SST_VALUE AS BIGINT) FROM [dbo].[SplitStringIntotable](@pAccountId, ',', 1))
   GROUP   BY PS.PS_TERMINAL_ID, PR.PV_NAME, TE.TE_NAME, AC_ACCOUNT_ID, AC_TRACK_DATA
   ORDER   BY AC.AC_ACCOUNT_ID, MAX(PS.PS_STARTED) DESC
  
  IF @pOnlyActivePlaySession = 0
    BEGIN
      SELECT   AC.AC_ACCOUNT_ID
             , TM.TM_TERMINAL_ID
             , TM.TM_ACCEPTED
             , TM.TM_AMOUNT
             , TM.TM_STACKER_ID   
        FROM   ACCOUNTS AS AC
        LEFT   JOIN TERMINAL_MONEY AS TM ON DBO.TRACKDATATOEXTERNAL(AC.AC_TRACK_DATA) = TM.TM_TRACKDATA AND TM.TM_ACCEPTED >= DATEADD(DAY, DATEDIFF(DAY, 0, @Today), 0)
       WHERE   AC.AC_ACCOUNT_ID IN (SELECT CAST(SST_VALUE AS BIGINT) FROM [dbo].[SplitStringIntoTable](@pAccountId, ',', 1))
       ORDER   BY TM.TM_ACCEPTED DESC, AC.AC_ACCOUNT_ID, TM.TM_TERMINAL_ID
    END
  ELSE
    BEGIN
      SELECT   AC.AC_ACCOUNT_ID
             , TM.TM_TERMINAL_ID
             , TM.TM_ACCEPTED
             , TM.TM_AMOUNT
             , TM.TM_STACKER_ID   
        FROM   ACCOUNTS AS AC
       INNER   JOIN TERMINAL_MONEY AS TM ON DBO.TRACKDATATOEXTERNAL(AC.AC_TRACK_DATA) = TM.TM_TRACKDATA
       WHERE   AC.AC_ACCOUNT_ID IN (SELECT CAST(SST_VALUE AS BIGINT) FROM SplitStringIntoTable(@pAccountId, ',', 1))
         AND   TM.TM_ACCEPTED >= DATEADD(DAY, DATEDIFF(DAY, 0, @Today), 0)
         AND   TM.TM_PLAY_SESSION_ID IN (SELECT MAX(PS_PLAY_SESSION_ID) FROM PLAY_SESSIONS GROUP BY PS_ACCOUNT_ID)
       ORDER   BY TM.TM_ACCEPTED DESC, AC.AC_ACCOUNT_ID, TM.TM_TERMINAL_ID
    END
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetPlayerInfo]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_GetPlayerInfo]
GO
CREATE PROCEDURE [dbo].[Layout_GetPlayerInfo]
      @AccountId INT
      ,@Date DATETIME
AS
BEGIN
  DECLARE @NLastVisits INT
  SET @NLastVisits = 3

  SELECT T.PS_PLAYED_COUNT
       , T.PS_PLAYED_AMOUNT
       ,(T.PS_PLAYED_AMOUNT/T.PS_PLAYED_COUNT) AS BET_AVG
       , T.PS_WON_AMOUNT
       , (T.PS_WON_AMOUNT-T.PS_PLAYED_AMOUNT) AS PLAYED_NET_WIN
  FROM 
  (
    SELECT SUM(PS_PLAYED_COUNT) AS PS_PLAYED_COUNT
        , SUM(PS_PLAYED_AMOUNT) AS PS_PLAYED_AMOUNT
        , SUM(PS_WON_AMOUNT) AS PS_WON_AMOUNT
    FROM PLAY_SESSIONS
    WHERE PS_ACCOUNT_ID = @AccountId
    AND PS_FINISHED < @Date
    and ps_played_count>0
   ) T

  SELECT TOP(@NLastVisits) 
           PVH_TOTAL_PLAYED_COUNT
         , PVH_TOTAL_PLAYED
         , PVH_TOTAL_WON
         , PVH_TOTAL_BET_AVG  
      FROM H_PVH
     WHERE PVH_ACCOUNT_ID = @AccountId
  ORDER BY pvh_date DESC 
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetPlayersInfo]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_GetPlayersInfo]
GO
CREATE PROCEDURE [dbo].[Layout_GetPlayersInfo]
AS
BEGIN
   SELECT [GTPM_MOVEMENT_ID]
       ,[GTPM_TYPE]
       ,[GTPM_DATETIME]
       ,[GTPM_USER_ID]
       ,[GTPM_CASHIER_ID]
       ,[GTPM_GAMING_TABLE_SESSION_ID]
       ,[GTPM_GAMING_TABLE_ID]
       ,[GTPM_SEAT_ID]
       ,[GTPM_PLAY_SESSION_ID]
       ,[GTPM_TERMINAL_ID]
       ,[GTPM_ACCOUNT_ID]
       ,[GTPM_OLD_VALUE]
       ,[GTPM_VALUE]
  FROM [GT_PLAYERTRACKING_MOVEMENTS]
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetPlaySessionsOpen]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_GetPlaySessionsOpen]
GO
CREATE PROCEDURE [dbo].[Layout_GetPlaySessionsOpen] 
AS
BEGIN
  SELECT  PS_PLAY_SESSION_ID
        , PS_ACCOUNT_ID
        , PS_TERMINAL_ID
        , TE_NAME
        , TE_PROVIDER_ID
        , PS_PLAYED_COUNT
        , PS_PLAYED_AMOUNT
        , GETDATE() AS LAST_UPDATE
    FROM   PLAY_SESSIONS
    INNER JOIN TERMINALS ON PS_TERMINAL_ID = TE_TERMINAL_ID
   WHERE PS_STATUS=0 
     AND PS_TYPE=2
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetRanges]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_GetRanges]
GO
CREATE PROCEDURE [dbo].[Layout_GetRanges]
      @pRole INT
    , @pUserId INT
AS
BEGIN

  SELECT LR_ID
     , LR_NAME
     , LR_FIELD
     , LR_ROLE
     , LR_SECTION_ID
     , LR_ICON
    FROM LAYOUT_RANGES
    WHERE LR_ROLE & @pRole = @pRole 
  ORDER BY LR_NAME ASC
  
  SELECT LRL_RANGE_ID
       , LRL_LABEL
       , LRL_VALUE1
       , LRL_VALUE2
       , LRL_OPERATOR
       , LRL_COLOR
       , LRL_ID
       , LRL_EDITABLE
    FROM LAYOUT_RANGES_LEGENDS
   WHERE LRL_RANGE_ID IN 
    (
      SELECT LR_ID
        FROM LAYOUT_RANGES
       WHERE LR_ROLE & @pRole = @pRole 
    )
  ORDER BY LRL_RANGE_ID, LRL_VALUE1 

  SELECT LCF_NAME
       , LCF_FILTER_PARAMETERS
       , LCF_ID
       , LCF_DISPLAY
    FROM LAYOUT_USERS_CUSTOM_FILTERS
   WHERE LCF_USER_ID= @pUserId 
      
     SELECT LR_SECTION_ID AS [CATEGORY] --41 PLAYER / 42 MACHINE
       , LR_NAME + ' - ' + LRL_LABEL AS [ALARM_DESC]
       , CAST((LR_FIELD * 1000) + LRL_VALUE1 AS INT) AS [SUBCATEGORY] 
       , R.LR_ICON AS ICON
       , L.LRL_AUTO_ASSIGN_PRIORITY
    FROM LAYOUT_RANGES AS R
INNER JOIN LAYOUT_RANGES_LEGENDS  AS L
      ON R.LR_ID = L.LRL_RANGE_ID
   WHERE LR_SECTION_ID IN (41,42,43)
ORDER BY LR_SECTION_ID    
END 
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetRunnersPositions]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_GetRunnersPositions]
GO
CREATE PROCEDURE [dbo].[Layout_GetRunnersPositions]
AS
BEGIN
  SELECT   GU_USER_ID
         , LRP_DEVICE_ID
         , LRP_FLOOR_ID
         , LRP_X
         , LRP_Y
         , LRP_RANGE
         , LRP_LAST_UPDATE
    FROM   GUI_USERS
   INNER   JOIN LAYOUT_USERS_CONFIGURATION ON GU_USER_ID = LC_USER_ID 
    LEFT   JOIN LAYOUT_RUNNERS_POSITION    ON GU_USER_ID = LRP_USER_ID
   WHERE   GU_PROFILE_ID IN (SELECT   GPF_PROFILE_ID
                               FROM   GUI_PROFILE_FORMS
                              WHERE   GPF_GUI_ID = 202 
                                AND   GPF_READ_PERM <> 0
                                AND   (GPF_FORM_ID / 1000000) = 4) -- SLOT ATTENDATS
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetTerminalActivity]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_GetTerminalActivity]
GO
CREATE PROCEDURE [dbo].[Layout_GetTerminalActivity]
AS
BEGIN

  DECLARE @big_won_limit AS MONEY
  
  -- TODO: Read from GP
  SET @big_won_limit = 1000

  SELECT LO.LO_ID
  
     -- Terminal info
     , LO.LO_EXTERNAL_ID AS TERMINAL_ID
     , TE.TE_STATUS	AS TERMINAL_STATUS

     -- Play Session info
     , PLAY_SESSION_STATUS = CASE  WHEN (PS_PLAY_SESSION_ID IS NOT NULL) THEN 2  
                 ELSE 1 END

     -- Player info
     , IS_ANONYMOUS = CASE  WHEN (TE.TE_CURRENT_ACCOUNT_ID IS NOT NULL) THEN CASE WHEN (AC.AC_HOLDER_NAME IS NOT NULL) THEN 0 ELSE 1 END 
                ELSE NULL END
     , PS.PS_ACCOUNT_ID AS PLAYER_ACCOUNT_ID
     , PLAYER_NAME = CASE WHEN (TE.TE_CURRENT_ACCOUNT_ID IS NOT NULL) THEN ISNULL(AC.AC_HOLDER_NAME, 'Anonymous')
                  ELSE NULL END
     , AC.AC_HOLDER_GENDER AS PLAYER_GENDER
     , DATEDIFF(YEAR, AC.AC_HOLDER_BIRTH_DATE, GETDATE()) AS PLAYER_AGE
     , AC.AC_HOLDER_LEVEL AS PLAYER_LEVEL
     , ISNULL(AC.AC_HOLDER_IS_VIP, 0) AS PLAYER_IS_VIP
     
     -- Flags
     , TS_SAS_HOST_ERROR AS AL_SAS_HOST_ERROR
     , ISNULL(TS_DOOR_FLAGS, 0) AS AL_DOOR_FLAGS 
     , ISNULL(TS_BILL_FLAGS, 0) AS AL_BILL_FLAGS
     , ISNULL(TS_PRINTER_FLAGS, 0) AS AL_PRINTER_FLAGS
     , ISNULL(TS_EGM_FLAGS, 0) AS AL_EGM_FLAGS
     , ISNULL(TS_PLAYED_WON_FLAGS, 0) AS AL_PLAYED_WON_FLAGS
     , ISNULL(TS_JACKPOT_FLAGS, 0) AS AL_JACKPOT_FLAGS
     , ISNULL(TS_STACKER_STATUS, 0) AS AL_STACKER_STATUS
     , ISNULL(TS_CALL_ATTENDANT_FLAGS, 0) AS AL_CALL_ATTENDANT_FLAGS
     , ISNULL(TS_MACHINE_FLAGS, 0) AS AL_MACHINE_FLAGS		
     , AL_BIG_WON = CASE WHEN ISNULL(SPH.WON_AMOUNT, 0) > @big_won_limit THEN 1
                     ELSE 0 END
                     
     -- Money info
     , ISNULL(SPH.PLAYED_AMOUNT , 0) AS PLAYED_AMOUNT
     , ISNULL(SPH.WON_AMOUNT, 0) AS WON_AMOUNT
     , PLAY_SESSION_BET_AVERAGE = CASE WHEN (PS.PS_PLAYED_COUNT > 0) THEN (PS.PS_TOTAL_PLAYED / PS.PS_PLAYED_COUNT)
                       ELSE NULL END
     , TERMINAL_BET_AVERAGE = CASE WHEN (SPH.PLAYED_COUNT > 0) THEN (SPH.PLAYED_AMOUNT / SPH.PLAYED_COUNT)
                     ELSE NULL END
     , DATEDIFF(SECOND, ISNULL(PS.PS_STARTED, GETDATE()), GETDATE()) AS PLAY_SESSION_DURATION
     , PS.PS_TOTAL_PLAYED AS PLAY_SESSION_TOTAL_PLAYED
     , PS.PS_PLAYED_COUNT AS PLAY_SESSION_PLAYED_COUNT
     
    FROM LAYOUT_OBJECTS AS LO
    
    INNER JOIN TERMINALS AS TE ON TE.TE_TERMINAL_ID = LO.LO_EXTERNAL_ID 
    
    LEFT JOIN (SELECT PS_TERMINAL_ID
                    , MAX(PS_PLAY_SESSION_ID) AS PS_PLAY_SESSION_ID
                    , PS_ACCOUNT_ID
                    , PS_TOTAL_PLAYED
                    , PS_PLAYED_COUNT
                    , PS_STARTED
             FROM PLAY_SESSIONS
            WHERE PS_STATUS = 0
         GROUP BY PS_TERMINAL_ID, PS_ACCOUNT_ID, PS_TOTAL_PLAYED, PS_PLAYED_COUNT, PS_STARTED) AS PS ON TE.TE_TERMINAL_ID = PS.PS_TERMINAL_ID
         
    LEFT JOIN ACCOUNTS AS AC ON AC.AC_ACCOUNT_ID = TE.TE_CURRENT_ACCOUNT_ID
    
    LEFT JOIN TERMINAL_STATUS AS TS ON TE.TE_TERMINAL_ID = TS.TS_TERMINAL_ID
    
    LEFT JOIN (SELECT SPH_TERMINAL_ID 
              , SUM(SPH_PLAYED_AMOUNT)AS PLAYED_AMOUNT
              , SUM(SPH_WON_AMOUNT) AS WON_AMOUNT
              , SUM(SPH_PLAYED_COUNT) AS PLAYED_COUNT
                 FROM SALES_PER_HOUR_V2
                WHERE SPH_BASE_HOUR >= DATEADD(DAY, DATEDIFF(DAY, 0, GETDATE()), 0) 
                AND SPH_BASE_HOUR <= GETDATE()
           GROUP BY SPH_TERMINAL_ID) AS SPH ON SPH.SPH_TERMINAL_ID = TE.TE_TERMINAL_ID
    
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetTerminalInfo]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_GetTerminalInfo]
GO
CREATE PROCEDURE [dbo].[Layout_GetTerminalInfo]
      @TerminalId BIGINT
AS
BEGIN
  DECLARE @str_columns as VARCHAR(max)
  DECLARE @str_query as NVARCHAR(max)
  DECLARE @now_hour AS INTEGER
  DECLARE @final_query as nvarchar(max)
  DECLARE @query_temp as nvarchar(max)
  DECLARE @test_date as nvarchar(max)
  
  SET @now_hour = DATEPART(HOUR,'2015-08-01')
  SET @str_columns=''
  SET @final_query='SELECT X2D_DATE,'
  set @test_date='2015-08-01'
  
  SET @str_columns = @str_columns + ' ISNULL(x2d_' + RIGHT('0' + CAST(@now_hour AS VARCHAR), 2) + '_max,0)'
    
  --NETWIN
  SET @query_temp = 'SUM(CASE WHEN X2D_METER_ID = 9  AND X2D_METER_ITEM = 0 THEN (' + @str_columns + ') end) @COLUMNAME'
  SET @query_temp = REPLACE(@query_temp,'@COLUMNAME','''NetWin''')
  SET @final_query = @final_query + @query_temp
    
  --AVG. BET
  SET @query_temp = 'SUM(CASE WHEN X2D_METER_ID = 1  AND X2D_METER_ITEM = 0 THEN (' + @str_columns + ') end)  @COLUMNAME'
  SET @query_temp = REPLACE(@query_temp,'@COLUMNAME','''AvgBet''')
  SET @final_query = @final_query + ' ,' + @query_temp
    
  --COIN IN
  SET @query_temp = 'SUM(CASE WHEN X2D_METER_ID = 12  AND X2D_METER_ITEM = 0 THEN (' + @str_columns + ') end)  @COLUMNAME'
  SET @query_temp = REPLACE(@query_temp,'@COLUMNAME','''CoinIn''')
  SET @final_query = @final_query + ' , ' + @query_temp
    
  --COIN OUT
  SET @query_temp = 'SUM(CASE WHEN X2D_METER_ID = 13  AND X2D_METER_ITEM = 0 THEN (' + @str_columns + ') end)  @COLUMNAME'
  SET @query_temp = REPLACE(@query_temp,'@COLUMNAME','''CoinOut''')
  SET @final_query = @final_query + ' , ' + @query_temp
    
  --PLAYS
  SET @query_temp = 'SUM(CASE WHEN X2D_METER_ID = 14  AND X2D_METER_ITEM = 0 THEN (' + @str_columns + ') end)  @COLUMNAME'
  SET @query_temp = REPLACE(@query_temp,'@COLUMNAME','''PLAYS''')
  SET @final_query = @final_query + ' , ' + @query_temp
    
  SET @final_query = @final_query+' FROM H_T2D_TMH WHERE X2D_WEEKDAY= DATEPART(DW,'''+@test_date+''') AND X2D_ID = ' + CAST(@TerminalId AS nvarchar) + ' GROUP BY X2D_DATE ORDER BY X2D_DATE ASC'
  
  EXEC SP_EXECUTESQL @final_query
    
  SET @now_hour = DATEPART(HOUR,GETDATE())
  SET @str_columns=''
  SET @final_query='SELECT '
  
  SET @str_columns = @str_columns + ' ISNULL(x2d_' + RIGHT('0' + CAST(@now_hour AS VARCHAR), 2) + '_avg,0)'
    
  --NETWIN
  SET @query_temp = 'SUM(CASE WHEN X2D_METER_ID = 9  AND X2D_METER_ITEM = 0 THEN (' + @str_columns + ') end) @COLUMNAME'
  SET @query_temp = REPLACE(@query_temp,'@COLUMNAME','''NetWin''')
  SET @final_query = @final_query + @query_temp
    
  --AVG. BET
  SET @query_temp = 'SUM(CASE WHEN X2D_METER_ID = 1  AND X2D_METER_ITEM = 0 THEN (' + @str_columns + ') end)  @COLUMNAME'
  SET @query_temp = REPLACE(@query_temp,'@COLUMNAME','''AvgBet''')
  SET @final_query = @final_query + ' ,' + @query_temp
    
  --COIN IN
  SET @query_temp = 'SUM(CASE WHEN X2D_METER_ID = 12  AND X2D_METER_ITEM = 0 THEN (' + @str_columns + ') end)  @COLUMNAME'
  SET @query_temp = REPLACE(@query_temp,'@COLUMNAME','''CoinIn''')
  SET @final_query = @final_query + ' , ' + @query_temp
    
  --COIN OUT
  SET @query_temp = 'SUM(CASE WHEN X2D_METER_ID = 13  AND X2D_METER_ITEM = 0 THEN (' + @str_columns + ') end)  @COLUMNAME'
  SET @query_temp = REPLACE(@query_temp,'@COLUMNAME','''CoinOut''')
  SET @final_query = @final_query + ' , ' + @query_temp
    
  --PLAYS
  SET @query_temp = 'SUM(CASE WHEN X2D_METER_ID = 14  AND X2D_METER_ITEM = 0 THEN (' + @str_columns + ') end)  @COLUMNAME'
  SET @query_temp = REPLACE(@query_temp,'@COLUMNAME','''PLAYS''')
  SET @final_query = @final_query + ' , ' + @query_temp
    
  SET @final_query = @final_query+' FROM H_Y2D_TMH WHERE X2D_WEEKDAY= DATEPART(DW,'''+@test_date+''') AND X2D_ID = ' + CAST(@TerminalId AS nvarchar)
  
  EXEC SP_EXECUTESQL @final_query
  
END 
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetTerminalsInfo]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_GetTerminalsInfo]
GO
CREATE PROCEDURE [dbo].[Layout_GetTerminalsInfo]
AS
BEGIN
   SELECT   [TE_TERMINAL_ID]
          , [TE_TYPE]
          , [TE_SERVER_ID]
          , [TE_NAME]
          , [TE_EXTERNAL_ID]
          , [TE_BLOCKED]
          , [TE_TIMESTAMP]
          , [TE_ACTIVE]
          , [TE_PROVIDER_ID]
          , [TE_CLIENT_ID]
          , [TE_BUILD_ID]
          , [TE_TERMINAL_TYPE]
          , [TE_VENDOR_ID]
          , [TE_UNIQUE_EXTERNAL_ID]
          , [TE_STATUS]
          , [TE_RETIREMENT_DATE]
          , [TE_RETIREMENT_REQUESTED]
          , [TE_DENOMINATION]
          , [TE_MULTI_DENOMINATION]
          , [TE_PROGRAM]
          , [TE_THEORETICAL_PAYOUT]
          , [TE_THEORETICAL_HOLD]
          , [TE_PROV_ID]
          , [TE_BANK_ID]
          , [TE_FLOOR_ID]
          , [TE_GAME_TYPE]
          , [TE_ACTIVATION_DATE]
          , [TE_CURRENT_ACCOUNT_ID]
          , [TE_CURRENT_PLAY_SESSION_ID]
          , [TE_REGISTRATION_CODE]
          , [TE_SAS_FLAGS]
          , [TE_SERIAL_NUMBER]
          , [TE_CABINET_TYPE]
          , [TE_JACKPOT_CONTRIBUTION_PCT]
          , [TE_CONTRACT_TYPE]
          , [TE_CONTRACT_ID]
          , [TE_ORDER_NUMBER]
          , [TE_WXP_REPORTED_STATUS_DATETIME]
          , [TE_WXP_REPORTED_STATUS]
          , [TE_SEQUENCE_ID]
          , [TE_VALIDATION_TYPE]
          , [TE_ALLOWED_CASHABLE_EMISSION]
          , [TE_ALLOWED_PROMO_EMISSION]
          , [TE_ALLOWED_REDEMPTION]
          , [TE_MAX_ALLOWED_TI]
          , [TE_MAX_ALLOWED_TO]
          , [TE_SAS_VERSION]
          , [TE_SAS_MACHINE_NAME]
          , [TE_BONUS_FLAGS]
          , [TE_FEATURES_BYTES]
          , [TE_VIRTUAL_ACCOUNT_ID]
          , [TE_SAS_FLAGS_USE_SITE_DEFAULT]
          , [TE_AUTHENTICATION_METHOD]
          , [TE_AUTHENTICATION_SEED]
          , [TE_AUTHENTICATION_SIGNATURE]
          , [TE_AUTHENTICATION_STATUS]
          , [TE_AUTHENTICATION_LAST_CHECKED]
          , [TE_MACHINE_ID]
          , [TE_POSITION]
          , [TE_TOP_AWARD]
          , [TE_MAX_BET]
          , [TE_NUMBER_LINES]
          , ISNULL([TE_DENOMINATION], 0) AS TE_TERMINAL_DENOMINATION
          , [TE_GAME_THEME]
          , [TE_METER_DELTA_ID]		 
     FROM   [TERMINALS]
    WHERE   te_terminal_type not in (-1, 100)
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetTerminalStatistics]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_GetTerminalStatistics]
GO
CREATE PROCEDURE [dbo].[Layout_GetTerminalStatistics]
      @pGameId	INT = NULL
    , @pFrom	DATETIME = NULL
    , @pTo		DATETIME = NULL
AS
BEGIN

  SELECT  SPH_TERMINAL_ID 
      , terminal
      , PlayedAmount
      , WonAmount
      , CASE x.PlayedAmount WHEN 0 THEN 0 ELSE (x.WonAmount*100/x.PlayedAmount) END as AmountPerCent
      , PlayedAmount-WonAmount as Netwin
      , CASE x.PlayedAmount WHEN 0 THEN 0 ELSE ((x.PlayedAmount-x.WonAmount)*100/x.PlayedAmount) END as NetwinPc
      , PlayedCount
      , WonCount
      , CASE x.PlayedCount WHEN 0 THEN 0 ELSE((x.WonCount* 100.0)/x.PlayedCount) END as CountPerCent 
      , Provider
      , ISNULL(MinGameId,-1) AS MinGameId
      , ISNULL(MaxGameId,-1) AS MaxGameId
      , MinGameName
      , CASE x.PlayedAmount WHEN 0 THEN 0 ELSE(x.TheoreticalWonAmount*100/PlayedAmount) END AS TheoreticalAmountPc 
      , TheoreticalWonAmount 
   FROM 
      (SELECT SPH_TERMINAL_ID, 
           sum(SPH_PLAYED_AMOUNT)as PlayedAmount
         , sum(SPH_WON_AMOUNT) AS WonAmount
         , sum(SPH_PLAYED_COUNT) as PlayedCount
         , sum(SPH_WON_COUNT) AS WonCount
         , (SELECT te_name 
              FROM terminals 
             WHERE(te_terminal_id = SALES_PER_HOUR_V2.SPH_terminal_id)) AS terminal
         , (SELECT te_provider_id 
              FROM terminals 
             WHERE(te_terminal_id = SALES_PER_HOUR_V2.SPH_terminal_id)) AS Provider
         , min(SPH_GAME_ID) AS MinGameId
         , max(SPH_GAME_ID) AS MaxGameId
         , ISNULL((SELECT PG_GAME_NAME AS GM_NAME 
                     FROM PROVIDERS_GAMES 
                    WHERE PG_GAME_ID = min(SPH_GAME_ID)),'UNKNOWN') AS MinGameName
         , sum(SPH_theoretical_won_amount) AS TheoreticalWonAmount 
       FROM SALES_PER_HOUR_V2
       
      WHERE (SPH_BASE_HOUR >= ISNULL(@pFrom, '2000-01-01')) 
      AND (SPH_BASE_HOUR < ISNULL(@pTo, GETDATE())) 
      AND SPH_TERMINAL_ID IN  (SELECT TE_TERMINAL_ID 
                                 FROM TERMINALS  
                                WHERE  TE_TERMINAL_TYPE IN ( 1, 3, 5, 106) 
                               ) 
      AND SPH_GAME_ID = ISNULL(@pGameId, SPH_GAME_ID) 
     GROUP BY SPH_TERMINAL_ID 
     
      )x ORDER BY Provider, terminal
  
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetTerminalStatusData]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_GetTerminalStatusData]
GO
CREATE PROCEDURE [dbo].[Layout_GetTerminalStatusData]
AS
BEGIN
 SELECT TS_TERMINAL_ID
      , TS_EGM_FLAGS
      , TS_DOOR_FLAGS
      , TS_BILL_FLAGS
      , TS_PRINTER_FLAGS
      , TS_PLAYED_WON_FLAGS
      , TS_JACKPOT_FLAGS
      , CASE WHEN TS_CALL_ATTENDANT_FLAGS = 1 THEN 1 ELSE 0 END AS TS_CALL_ATTENDANT_FLAGS
      , TS_STACKER_STATUS
      , TS_MACHINE_FLAGS
   FROM TERMINAL_STATUS
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetTodayActiveAlarms]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_GetTodayActiveAlarms]
GO
CREATE PROCEDURE [dbo].[Layout_GetTodayActiveAlarms] 
AS
BEGIN
    SELECT  LSA_TERMINAL_ID
         ,  LSA_ALARM_ID
      FROM  LAYOUT_SITE_ALARMS LEFT JOIN LAYOUT_SITE_TASKS ON LSA_TASK_ID = LST_ID
     WHERE  LSA_DATE_CREATED >= dbo.TodayOpening(0) 
       AND  (LST_STATUS IS NULL OR LST_STATUS NOT IN (3,5))
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetUserPermissions]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_GetUserPermissions]
GO
CREATE PROCEDURE [dbo].[Layout_GetUserPermissions]
      @pUserId INT
AS
BEGIN
 SELECT   GPF_FORM_ID
      ,   GPF_READ_PERM
      ,   GPF_WRITE_PERM
      ,   GPF_DELETE_PERM
      ,   GPF_EXECUTE_PERM
   FROM   GUI_PROFILE_FORMS
  WHERE   GPF_GUI_ID = 203 AND GPF_READ_PERM <> 0 
    AND   GPF_PROFILE_ID = 
 (
     SELECT GU_PROFILE_ID 
       FROM GUI_USERS 
      WHERE GU_USER_ID = @pUserId
 )
END 
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetUsersIdByRoleId]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_GetUsersIdByRoleId]
GO
CREATE PROCEDURE [dbo].[Layout_GetUsersIdByRoleId]
      @pRoleId INT
AS
BEGIN
      SELECT DISTINCT(GU_USER_ID)
    FROM GUI_USERS
    WHERE GU_PROFILE_ID IN (SELECT GPF_PROFILE_ID
                FROM   GUI_PROFILE_FORMS
                              WHERE  GPF_GUI_ID = 202 
                              AND    GPF_READ_PERM <> 0
                              AND (GPF_FORM_ID / 1000000) = @pRoleId)
                  
END 
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetUsersWithManagerCapabilities]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_GetUsersWithManagerCapabilities]
GO
CREATE PROCEDURE [dbo].[Layout_GetUsersWithManagerCapabilities]
AS
BEGIN
  SELECT lc.LC_USER_ID as ID, u.GU_USERNAME as USERNAME
  FROM LAYOUT_USERS_CONFIGURATION lc
  INNER JOIN GUI_USERS u on lc.LC_USER_ID = u.GU_USER_ID
  WHERE LC_IS_MANAGER = 1
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_Get_Resources]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_Get_Resources]
GO
CREATE PROCEDURE [dbo].[Layout_Get_Resources] 
AS 
BEGIN
  SELECT   lme_mesh_id
         , lme_type
         , lme_sub_type
         , lme_geometry
    FROM   layout_meshes
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_LoadFloor]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_LoadFloor]
GO
CREATE PROCEDURE [dbo].[Layout_LoadFloor]
( @pFloorId INT = NULL )      
AS
BEGIN
  SELECT   loc_id
         , loc_x
         , loc_y
         , loc_creation
         , loc_type
         , loc_floor_number
         , loc_grid_id
         , loc_status
    FROM   layout_locations       
   WHERE   loc_floor_number = @pFloorId

  SELECT   lo_id
         , lo_external_id
         , lo_type
         , lo_parent_id
         , lo_mesh_id
         , lo_creation_date
         , lo_update_date
         , lo_status
         , lol_object_id
         , lol_location_id
         , lol_date_from
         , lol_date_to
         , lol_orientation
         , lol_area_id
         , lol_bank_id
         , lol_bank_location
         , lol_current_location
         , lol_offset_x
         , lol_offset_y
    FROM   layout_objects
    LEFT   JOIN layout_object_locations ON lol_object_id   = lo_id
    LEFT   JOIN layout_locations        ON lol_location_id = loc_id
   WHERE   loc_floor_number             = @pFloorId
     AND   lol_current_location         = 1
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_Login]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_Login]
GO
CREATE PROCEDURE [dbo].[Layout_Login] 
    @pUserName as NVARCHAR(MAX)
  , @pOrigin AS INT
  , @opUserId AS INT OUTPUT
  , @opUserName AS NVARCHAR(MAX) = '' OUTPUT
AS
BEGIN
  --Origin can be Web=1/App=2
  DECLARE @ErrorCode AS INT
  DECLARE @BlockReason AS INT
  DECLARE @UserId AS INT
  DECLARE @HasPermission AS INT
  DECLARE @UserName AS NVARCHAR(MAX)

  SET @ErrorCode = 0

  --Usuario existe
  SELECT   @BlockReason = GU_BLOCK_REASON
       ,   @UserId = GU_USER_ID
       ,   @UserName = GU_FULL_NAME
    FROM   GUI_USERS
   WHERE   GU_USERNAME = @pUserName

  IF @BlockReason IS NULL 
  BEGIN
    SET @ErrorCode = 1
  END
  ELSE IF @BlockReason <> 0 
  BEGIN
    SET @ErrorCode = 2  
  END

  IF @ErrorCode = 0 
  BEGIN
  -- Si tiene permisos (APP o WEB)
      SELECT @HasPermission = COUNT(*) 
        FROM GUI_USERS INNER JOIN GUI_USER_PROFILES ON GUP_PROFILE_ID = GU_PROFILE_ID
                       INNER JOIN GUI_PROFILE_FORMS ON GPF_PROFILE_ID = GUP_PROFILE_ID
                       INNER JOIN GUI_FORMS ON GF_FORM_ID = GPF_FORM_ID
       WHERE GU_USER_ID = @UserId  
         AND GF_GUI_ID = 203 
         AND GF_FORM_ID = @pOrigin 
         AND GPF_GUI_ID = 203
     
     IF @HasPermission = 0 
     BEGIN
      SET @ErrorCode = 3
      END
  END
        SELECT @opUserId = @UserId  
        SELECT @ErrorCode
        SELECT @opUserName = @UserName
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_ReleaseAlarmFromTask]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_ReleaseAlarmFromTask]
GO
CREATE PROCEDURE [dbo].[Layout_ReleaseAlarmFromTask]
  @pTaskId INT
AS
BEGIN
    UPDATE LAYOUT_SITE_ALARMS 
    SET LSA_TASK_ID = NULL
      , LSA_DATE_TO_TASK = NULL  	   
      , LSA_STATUS = 2 
  WHERE LSA_TASK_ID = @pTaskId
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_Get_Meters_List]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[SP_Get_Meters_List]
GO 
CREATE PROCEDURE [dbo].[SP_Get_Meters_List] 
 ( @Device AS INT ) 
AS
BEGIN
  SELECT   hmd_meter_id
         , hmd_meter_type
         , hmd_description
         , hmd_meter_last_update
    FROM   H_METERS_DEFINITION
   WHERE   hmd_visible = 1
     AND   hmd_meter_device = @Device
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wsi_table_pivot]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[wsi_table_pivot]
GO
CREATE PROCEDURE [dbo].[wsi_table_pivot]
(
  @pTable AS nvarchar(max),
  @pField AS nvarchar(max),
  @pValueField AS nvarchar(max)
)
AS 
BEGIN
  DECLARE   @_list_values AS nvarchar(max)
  DECLARE   @_query_items AS nvarchar(max)
  DECLARE   @_pivot_query AS nvarchar(max)

  CREATE   TABLE ##temporary_values_table 
           (VALUE VARCHAR(MAX))

  SET @_query_items = N'INSERT   INTO ##temporary_values_table 
                        SELECT   DISTINCT ' + @pField + ' AS VALUE 
                          FROM   ' + @pTable 

  -- Obtain the column values
  EXEC sp_executesql @_query_items

  -- Join values for the pivot query string
  SELECT   @_list_values = COALESCE(@_list_values + ',', '') + VALUE
    FROM   (
            SELECT   QUOTENAME(RTRIM(LTRIM(CAST(VALUE AS VARCHAR(20))))) AS VALUE
              FROM   ##temporary_values_table
           ) AS X

  -- Release temporary table with field values         
  DROP TABLE ##temporary_values_table

  -- Prepare the pivot query string
  SET @_pivot_query = N'SELECT   *  
                          FROM   ' + @pTable + ' 
                         PIVOT   ( 
                                  MIN(' + @pValueField + ') 
                                  FOR ' + @pField + ' in (' + @_list_values + ') 
                                  ) PIVOT_TABLE'
  
  -- Execute the pivot query string
  EXEC sp_executesql @_pivot_query         
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_SaveConfiguration]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_SaveConfiguration]
GO
CREATE PROCEDURE [dbo].[Layout_SaveConfiguration]
      @pUserId INT
    , @pLastUpdate DATETIME = NULL
    , @pParameters VARCHAR(MAX)
    , @pDashboard VARCHAR(MAX) = NULL
AS
BEGIN
  IF EXISTS (SELECT TOP 1 * FROM LAYOUT_USERS_CONFIGURATION WHERE LC_USER_ID = @pUserId)
    UPDATE   LAYOUT_USERS_CONFIGURATION 
       SET   LC_LAST_UPDATE = @pLastUpdate
           , LC_PARAMETERS = @pParameters
           , LC_DASHBOARD = ISNULL(@pDashboard, NULL)
     WHERE   LC_USER_ID = @pUserId
  ELSE
    INSERT   INTO LAYOUT_USERS_CONFIGURATION
           ( LC_USER_ID
           , LC_PARAMETERS
           , LC_DASHBOARD 
           , LC_LAST_UPDATE 
           , LC_IS_MANAGER 
           , LC_IS_RUNNER )
    VALUES ( @pUserId
           , @pParameters
           , ISNULL(@pDashboard, NULL)
           , ISNULL(@pLastUpdate, GETDATE())
           , 0
           , 0 ) 
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_SaveCustomFilter]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_SaveCustomFilter]
GO
CREATE PROCEDURE [dbo].[Layout_SaveCustomFilter]
      @pUserId INT
    , @pCustomFilterName NVARCHAR(50)
    , @pParameters NVARCHAR(MAX)
    , @pFilterId INT
    , @pOutputFilterId INT OUTPUT
    , @pDisplay NVARCHAR(MAX)
AS
BEGIN
      IF EXISTS (SELECT TOP 1 * 
                   FROM LAYOUT_USERS_CUSTOM_FILTERS
                  WHERE LCF_ID = @pFilterId)
            UPDATE      LAYOUT_USERS_CUSTOM_FILTERS 
               SET      LCF_LAST_UPDATE = GETDATE()
                 ,      LCF_NAME = @pCustomFilterName
                 ,      LCF_FILTER_PARAMETERS = @pParameters
                 ,      LCF_DISPLAY = @pDisplay
             WHERE      LCF_ID = @pFilterId
      ELSE
       INSERT INTO      LAYOUT_USERS_CUSTOM_FILTERS
            VALUES 
                         (    @pUserId
                                 ,  @pCustomFilterName
                 ,      @pParameters
                 ,      1
                 ,  GETDATE()
                 ,  @pDisplay
                        ) 
           SELECT @pOutputFilterId=SCOPE_IDENTITY();
END 
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_SaveFloor_NEW]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_SaveFloor_NEW]
GO
CREATE PROCEDURE [dbo].[Layout_SaveFloor_NEW]
   @pGeometry AS VARCHAR(MAX)
 , @pImageMap AS VARCHAR(MAX)
 , @pColor AS VARCHAR(20)
 , @pName AS VARCHAR(50)
 , @pUnit AS INT
 , @pWidth AS INT
 , @pHeight AS INT
AS
BEGIN
  DECLARE @_FloorId AS INT
  SET @_FloorId = (SELECT LF_FLOOR_ID FROM LAYOUT_FLOORS WHERE LF_NAME = @pName  )
  IF @_FloorId IS NULL
    BEGIN
        -- New Floor
        INSERT   INTO LAYOUT_FLOORS
                 (LF_GEOMETRY, LF_IMAGE_MAP, LF_COLOR, LF_NAME, LF_METRIC, LF_WIDTH, LF_HEIGHT) 
        VALUES   (@pGeometry, @pImageMap, @pColor, @pName, @pUnit, @pWidth, @pHeight)   
        SET @_FloorId = SCOPE_IDENTITY()
    END
  ELSE
    BEGIN
        -- Update existing floor
        UPDATE   LAYOUT_FLOORS
           SET   LF_GEOMETRY = @pGeometry
               , LF_IMAGE_MAP = @pImageMap
               , LF_COLOR = @pColor
               , LF_NAME = @pName
               , LF_METRIC = @pUnit
               , LF_WIDTH = @pWidth
               , LF_HEIGHT = @pHeight
         WHERE   LF_FLOOR_ID = @_FloorId
    END
  SELECT   @_FloorId
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_SaveLayoutRunnersPosition]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_SaveLayoutRunnersPosition]
GO
CREATE PROCEDURE [dbo].[Layout_SaveLayoutRunnersPosition]
      @pUserId INT
    , @pDeviceId Nvarchar(50)
    , @pPositionX FLOAT
    , @pPositionY FLOAT
    , @pPositionFloor INT
AS
BEGIN
  IF EXISTS (SELECT TOP 1 * FROM layout_runners_position lrp WHERE lrp.lrp_device_id = @pDeviceId AND lrp.lrp_user_id = @pUserId)
    UPDATE	 LAYOUT_RUNNERS_POSITION
       SET   LRP_X           = @pPositionX
           , LRP_Y           = @pPositionY
           , LRP_LAST_UPDATE = GETDATE()
     WHERE   LRP_DEVICE_ID   = @pDeviceId 
       AND   LRP_USER_ID     = @pUserId
  ELSE 
    INSERT   INTO LAYOUT_RUNNERS_POSITION	
          ( LRP_DEVICE_ID
          , LRP_USER_ID
          , LRP_X
          , LRP_Y 
          , LRP_FLOOR_ID
          , LRP_LAST_UPDATE )
    VALUES ( @pDeviceId
          , @pUserId
          , @pPositionX
          , @pPositionY
          , @pPositionFloor
          , GETDATE() )
END 
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_SaveManagerRunnerMessage]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_SaveManagerRunnerMessage]
GO
CREATE PROCEDURE [dbo].[Layout_SaveManagerRunnerMessage] 
  @pManagerId BIGINT,
  @pRunnerId BIGINT,
  @pSource INT,
  @pMessage TEXT,
  @new_identity INT OUTPUT
AS
BEGIN
  SET NOCOUNT ON;
  INSERT INTO LAYOUT_MANAGER_RUNNER_MESSAGES
  (
    LMRM_MANAGER_ID,
    LMRM_RUNNER_ID,
    LMRM_MESSAGE,
    LMRM_SOURCE,
    LMRM_DATE
  )VALUES
  (
    @pManagerId,
    @pRunnerId,
    @pMessage,
    @pSource,
    GETDATE()
  )

  SELECT @new_identity = SCOPE_IDENTITY()
  SELECT @new_identity AS id
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_SaveSiteAlarm]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_SaveSiteAlarm]
GO
CREATE PROCEDURE [dbo].[Layout_SaveSiteAlarm]
      @pTerminalId INT
    , @pAlarmId INT
    , @pDateCreated DATETIME
    , @pAlarmType INT
    , @pAlarmSource INT
    , @pUserCreated INT
    , @pMediaId INT
    , @pDescription NVARCHAR(MAX)
    , @pDateToTask DATETIME
    , @pTaskId INT
    , @pStatus INT
AS
BEGIN
  IF EXISTS (SELECT TOP 1 * FROM LAYOUT_SITE_ALARMS WHERE	LSA_TERMINAL_ID = @pTerminalId AND LSA_ALARM_ID = @pAlarmId AND LSA_DATE_CREATED = @pDateCreated)
    UPDATE	LAYOUT_SITE_ALARMS
        SET	LSA_DATE_TO_TASK = @pDateToTask
          ,	LSA_TASK_ID = @pTaskId
          ,  LSA_STATUS = @pStatus
      WHERE	LSA_TERMINAL_ID = @pTerminalId
        AND  LSA_ALARM_ID = @pAlarmId
        AND  LSA_DATE_CREATED = @pDateCreated
  ELSE
    INSERT   INTO	LAYOUT_SITE_ALARMS
           ( LSA_TERMINAL_ID
           , LSA_ALARM_ID
           , LSA_DATE_CREATED
           , LSA_ALARM_TYPE
           , LSA_ALARM_SOURCE
           , LSA_USER_CREATED
           , LSA_MEDIA_ID
           , LSA_DESCRIPTION
           , LSA_DATE_TO_TASK
           , LSA_TASK_ID
           , LSA_STATUS )
    VALUES ( @pTerminalId 
           , @pAlarmId
           , @pDateCreated 
           , @pAlarmType 
           , @pAlarmSource 
           , @pUserCreated 
           , @pMediaId 
           , @pDescription 
           , @pDateToTask 
           , @pTaskId
           , 0 ) 
END 
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_SaveUser]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_SaveUser]
GO
CREATE PROCEDURE [dbo].[Layout_SaveUser]
      @pUserId INT
    , @pIsManager BIT
AS
BEGIN
      IF EXISTS (SELECT TOP 1 * FROM LAYOUT_USERS_CONFIGURATION WHERE LC_USER_ID = @pUserId)
            UPDATE	LAYOUT_USERS_CONFIGURATION 
               SET	LC_IS_MANAGER = @pIsManager
             WHERE	LC_USER_ID = @pUserId
      ELSE
       INSERT   INTO LAYOUT_USERS_CONFIGURATION
       VALUES (	@pUserId
              ,	''
              , NULL
              ,	GETDATE()
              , @pIsManager
              , 0 ) 
END 
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_SceneRecording]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_SceneRecording]
GO
CREATE PROCEDURE [dbo].[Layout_SceneRecording]
      @pFloorId INT
    , @pJson VARCHAR(MAX)
    , @pNewScene BIT
AS
BEGIN
  DECLARE @sceneId AS INT
  
  IF(@pNewScene = 1)
  BEGIN
    SELECT @sceneId = (ISNULL(MAX(LR_SCENE_ID), 0) + 1) 
      FROM LAYOUT_RECORDING
  END
  
  ELSE
  BEGIN
    SELECT @sceneId = ISNULL(MAX(LR_SCENE_ID), 0) 
      FROM LAYOUT_RECORDING
  END
  
  IF NOT EXISTS (SELECT   TOP 1 LR_SCENE_ID 
                   FROM   LAYOUT_RECORDING 
                  WHERE   LR_DATE = DATEADD(MINUTE, DATEDIFF(MINUTE, 0, GETDATE()), 0))
    INSERT   INTO layout_recording
    VALUES ( @sceneId
           , DATEADD(MINUTE, DATEDIFF(MINUTE, 0, GETDATE()), 0)
           , @pFloorId
           , @pJson)
            
END 
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_SetTerminalStatusFlag]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_SetTerminalStatusFlag]
GO
CREATE PROCEDURE [dbo].[Layout_SetTerminalStatusFlag]
      @pTerminalId INT
    , @pAlarmId INT
    , @pStatus INT -- 0 unset, 1 set, 2 reset all alarms
AS
BEGIN

  IF NOT EXISTS (SELECT * FROM TERMINAL_STATUS WHERE TS_TERMINAL_ID = @pTerminalId)
   BEGIN
    -- CREATE TERMINAL INTO TERMINAL_STATUS
    INSERT INTO TERMINAL_STATUS (TS_TERMINAL_ID) VALUES (@pTerminalId)
   END

  IF (@pStatus <> 2)
   BEGIN
   
    -- Update alarm status
    DECLARE @_alarm AS VARCHAR(5)
    DECLARE @_col AS INT 
    DECLARE @_flag AS INT 
    DECLARE @_col_name AS VARCHAR(MAX)
    DECLARE @_is_bit AS BIT 
    
    SET @_alarm = CAST(@pAlarmId AS VARCHAR)
    SET @_col = SUBSTRING(@_alarm, 1, 2)
    SET @_flag = SUBSTRING(@_alarm, 3, 3)
    SET @_col_name = ''
    SET @_is_bit = 0
    
    IF (@_col = 12)
     BEGIN
      SET @_col_name = 'TS_DOOR_FLAGS'
     END
    ELSE IF (@_col = 13)
     BEGIN
      SET @_col_name = 'TS_BILL_FLAGS'
     END
    ELSE IF (@_col = 14)
     BEGIN
      SET @_col_name = 'TS_PRINTER_FLAGS'
     END
    ELSE IF (@_col = 16)
     BEGIN
      SET @_col_name = 'TS_PLAYED_WON_FLAGS'
     END
    ELSE IF (@_col = 17)
     BEGIN
      SET @_col_name = 'TS_JACKPOT_FLAGS'
     END
    ELSE IF (@_col = 19)
     BEGIN
      SET @_col_name = 'TS_CALL_ATTENDANT_FLAGS'
      SET @_is_bit = 1
     END
    ELSE IF (@_col = 20)
     BEGIN
      IF (@_flag = 512) 
       BEGIN
        SET @_col_name = 'TS_MACHINE_FLAGS'
       END
      ELSE
       BEGIN
        SET @_col_name = 'TS_EGM_FLAGS'
       END
     END

    DECLARE @sql NVARCHAR(MAX)
    SET @sql = ''
    
    SET @sql = @sql + 'UPDATE   TERMINAL_STATUS '
    SET @sql = @sql + '   SET   ' + @_col_name + ' = '
    
    IF (@_is_bit = 1)
     BEGIN
      SET @sql = @sql + CAST(@pStatus AS VARCHAR(50))
     END
    ELSE
     BEGIN 
      IF (@pStatus = 0)
       BEGIN
        SET @sql = @sql + @_col_name + ' & ~' + CAST(@_flag AS VARCHAR(50)) + ' '
       END
      ELSE IF (@pStatus = 1)
       BEGIN
        SET @sql = @sql + @_col_name + ' ^ ' + CAST(@_flag AS VARCHAR(50)) + ' '
       END
     END

    SET @sql = @sql + ' WHERE   TS_TERMINAL_ID   = ' + CAST(@pTerminalId AS VARCHAR(50)) + ''
           
    --SELECT @_alarm, @_col, @_flag, @_col_name, @sql
           
    EXEC SP_EXECUTESQL @sql
  END
 ELSE
  BEGIN
   -- Reset all alarms 
   UPDATE   TERMINAL_STATUS
      SET   TS_EGM_FLAGS            = 0
          , TS_DOOR_FLAGS           = 0
          , TS_BILL_FLAGS           = 0
          , TS_PRINTER_FLAGS        = 0
          , TS_PLAYED_WON_FLAGS     = 0
          , TS_JACKPOT_FLAGS        = 0
          , TS_CALL_ATTENDANT_FLAGS = 0
          , TS_MACHINE_FLAGS        = 0
    WHERE   TS_TERMINAL_ID          = @pTerminalId
    
  END
END
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LAYOUT_SET_LOCATION]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[LAYOUT_SET_LOCATION]
GO
CREATE PROCEDURE [dbo].[LAYOUT_SET_LOCATION] 
( @pId INT
, @pX INT
, @pY INT
, @pCreation DATETIME
, @pType INT
, @pFloorId INT
, @pGridId VARCHAR(50)
, @pStatus INT
) AS
BEGIN

  IF (@pId IS NULL)
    BEGIN
      -- INSERT
      
      IF (@pCreation IS NULL)
        BEGIN
          SET @pCreation = GETDATE()
        END
      
      INSERT INTO   LAYOUT_LOCATIONS
                  ( LOC_X, LOC_Y, LOC_CREATION, LOC_TYPE, LOC_FLOOR_NUMBER, LOC_GRID_ID, LOC_STATUS )
           VALUES ( @pX, @pY, @pCreation, @pType, @pFloorId, @pGridId, @pStatus)
             
      SET @pId = SCOPE_IDENTITY()           
             
    END
  ELSE
    BEGIN
     -- UPDATE ALL EXCEPT ID AND CREATION DATE
           UPDATE   LAYOUT_LOCATIONS
              SET   LOC_X = @pX
                  , LOC_Y = @pY
                  , LOC_TYPE = @pType
                  , LOC_FLOOR_NUMBER = @pFloorId
                  , LOC_GRID_ID = @pGridId
                  , LOC_STATUS = @pStatus
                  
    END
    
  SELECT @pId
END  
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LAYOUT_SET_OBJECT]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[LAYOUT_SET_OBJECT]
GO
CREATE PROCEDURE [dbo].[LAYOUT_SET_OBJECT]
( @pId INT
, @pExternalId INT
, @pType INT
, @pParentId INT
, @pMeshId INT
, @pCreation DATETIME
, @pUpdated DATETIME
, @pStatus INT
) AS
BEGIN
  IF (@pUpdated IS NULL)
    BEGIN
      SET @pUpdated = GETDATE()
    END

  IF (@pId IS NULL)
    BEGIN
      -- INSERT
      
      IF (@pCreation IS NULL)
        BEGIN
          SET @pCreation = GETDATE()
        END
     
      INSERT INTO   layout_objects
                  ( lo_external_id, lo_type, lo_parent_id, lo_mesh_id, lo_creation_date, lo_update_date, lo_status )
           VALUES ( @pExternalId, @pType, @pParentId, @pMeshId, @pCreation, @pUpdated, @pStatus )
             
      SET @pId = SCOPE_IDENTITY()           
             
    END
  ELSE
    BEGIN
     -- UPDATE ALL EXCEPT ID AND CREATION DATE
           UPDATE   layout_objects
              SET   lo_external_id    = @pExternalId
                  , lo_type           = @pType
                  , lo_parent_id      = @pParentId
                  , lo_mesh_id        = @pMeshId
                  , lo_update_date    = @pUpdated
                  , lo_status         = @pStatus
                  
    END
    
  SELECT @pId
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_TerminalStatusUnsetFlag]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_TerminalStatusUnsetFlag]
GO
CREATE PROCEDURE [dbo].[Layout_TerminalStatusUnsetFlag]
      @pColumn NVARCHAR(MAX)
    , @pFlag INT
    , @pTerminalId INT
AS
BEGIN
  DECLARE @sql NVARCHAR(MAX)
  SET @sql = 'UPDATE   TERMINAL_STATUS 
                 SET   ' + @pColumn + ' = ' + @pColumn + ' & (~'+CAST(@pFlag AS VARCHAR(50))+') 
               WHERE   TS_TERMINAL_ID = ' + CAST(@pTerminalId AS VARCHAR(50))+''
  EXEC SP_EXECUTESQL @sql
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_UploadMobileApplicationVersion]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_UploadMobileApplicationVersion]
GO
CREATE PROCEDURE [dbo].[Layout_UploadMobileApplicationVersion]
  @pVersion text,
  @pApk varbinary(max)
AS
BEGIN
  INSERT INTO   LAYOUT_MOBILE_APPLICATION_VERSIONS
              ( LMAV_VERSION
              , LMAV_APK
              , LMAV_DATE )
       VALUES
              ( @pVersion
              , @pApk
              , GETDATE() )
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_UserValidToUseSmartfloorApp]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_UserValidToUseSmartfloorApp]
GO
CREATE PROCEDURE [dbo].[Layout_UserValidToUseSmartfloorApp] 
  @pUserId bigint,
  @pResult int output
AS
BEGIN
  SELECT   @pResult = COUNT(*) 
    FROM   GUI_USERS
   INNER   JOIN GUI_USER_PROFILES ON GUP_PROFILE_ID = GU_PROFILE_ID
   INNER   JOIN GUI_PROFILE_FORMS ON GPF_PROFILE_ID = GUP_PROFILE_ID
   INNER   JOIN GUI_FORMS         ON GF_FORM_ID     = GPF_FORM_ID
   WHERE   GU_USER_ID = @pUserId  
     AND   GF_GUI_ID  = 203 
     AND   GF_FORM_ID = 2 
     AND   GPF_GUI_ID = 203
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_Get_Meters]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[SP_Get_Meters]
GO
CREATE PROCEDURE [dbo].[SP_Get_Meters] 
(  @Device AS INT
 , @Source AS VARCHAR(1)
 , @Date AS DATETIME
) 
AS
BEGIN
  DECLARE @DATEFROM AS DATETIME
  DECLARE @DATEFROM_WEEK AS DATETIME
  DECLARE @DATEFROM_MONTH AS DATETIME
  DECLARE @DATEFROM_YEAR AS DATETIME
  DECLARE @SEARCH_DATE AS NVARCHAR(MAX)
  DECLARE @QUERY_CONST AS NVARCHAR(MAX)
  DECLARE @QUERY_TEMP AS NVARCHAR(MAX)
  DECLARE @FINAL_QUERY AS NVARCHAR(MAX)
  DECLARE @QUERY AS NVARCHAR(MAX)
  DECLARE @RANGE AS VARCHAR(1)
  DECLARE @DESCRIPTION AS VARCHAR(3)
  DECLARE @HOUR AS VARCHAR(2)
  DECLARE @ORIGIN AS VARCHAR(MAX)
  DECLARE @DOW AS VARCHAR(1)

  SET @FINAL_QUERY = ''
  SET @DATEFROM = @Date

  EXEC SP_Get_Meters_List @DEVICE

  SET @DATEFROM_WEEK = DATEADD(WK, DATEDIFF(WK, 0, @DATEFROM) - 1, 0)             -- First day of previous week
  SET @DATEFROM_MONTH = DATEADD(M, -1, DATEADD(M, DATEDIFF(M, 0, @DATEFROM), 0))  -- First day of previous month
  SET @DATEFROM_YEAR = DATEADD(YY, -1, DATEADD(YY, DATEDIFF(YY, 0, @DATEFROM), 0)) -- First day of previous year
  SET @DOW = LTRIM(DATEPART(WEEKDAY, @DATEFROM))

  SET @HOUR = DATEPART(HOUR, GETDATE())
  SET @HOUR = REPLICATE('0',2-LEN(RTRIM(@HOUR))) + RTRIM(@HOUR) 

  SET @QUERY_CONST = ' SELECT ''@DESCRIPTION'' AS RANGE, @HOUR as HOUR, x2d_date, x2d_weekday, x2d_id, x2d_meter_id,'
                   + ' x2d_@HOUR_min AS MIN, x2d_@HOUR_max as MAX, x2d_@HOUR_acc AS ACC, x2d_@HOUR_avg AS AVG, x2d_@HOUR_num AS NUM,'
                   + ' x2d_dd_min AS DAY_MIN, x2d_dd_max AS DAY_MAX, x2d_dd_acc AS DAY_ACC, x2d_dd_avg AS DAY_AVG, x2d_dd_num AS DAY_NUM'
                   + ' FROM  H_@RANGE2D_@SOURCEMH'
                   + ' INNER JOIN H_METERS_DEFINITION ON x2d_meter_id = hmd_meter_id'
                   + ' WHERE hmd_visible = 1 AND (@DATE)'

  -- TDA & SLW
  SET @QUERY_TEMP = @QUERY_CONST
  SET @RANGE = 'T'
  SET @DESCRIPTION = 'TDA'
  SET @SEARCH_DATE = 'x2d_date >= ' + CONVERT(VARCHAR(10), DATEADD(DAY, -7, @DATEFROM), 112) + ' AND x2d_date <= ' + CONVERT(VARCHAR(10), @DATEFROM, 112)
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DATE', @SEARCH_DATE)
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@HOUR', LTRIM(@HOUR))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@RANGE', LTRIM(@RANGE))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DESCRIPTION', LTRIM(@DESCRIPTION))
  SET @FINAL_QUERY = @QUERY_TEMP 

  -- WTD
  SET @QUERY_TEMP = @QUERY_CONST
  SET @RANGE = 'W'
  SET @DESCRIPTION = 'WTD'
  SET @SEARCH_DATE = 'x2d_date = ' + CAST(CONVERT(VARCHAR(10), @DATEFROM_WEEK, 112) AS VARCHAR)
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DATE', LTRIM(@SEARCH_DATE))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@HOUR', LTRIM(@HOUR))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@RANGE', LTRIM(@RANGE))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DESCRIPTION', LTRIM(@DESCRIPTION))
  --EXECUTE sp_executesql @QUERY
  SET @FINAL_QUERY = @FINAL_QUERY
                   + ' UNION '
                   + @QUERY_TEMP 
                   + ' AND x2d_weekday = ' + @DOW

  -- MTD
  SET @QUERY_TEMP = @QUERY_CONST
  SET @RANGE = 'M'
  SET @DESCRIPTION = 'MTD'
  SET @SEARCH_DATE = 'x2d_date = ' + CAST(CONVERT(VARCHAR(10), @DATEFROM_MONTH, 112) AS VARCHAR)
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DATE', LTRIM(@SEARCH_DATE))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@HOUR', LTRIM(@HOUR))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@RANGE', LTRIM(@RANGE))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DESCRIPTION', LTRIM(@DESCRIPTION))
  SET @FINAL_QUERY = @FINAL_QUERY
                   + ' UNION '
                   + @QUERY_TEMP 
                   + ' AND x2d_weekday = ' + @DOW

  -- YTD
  SET @QUERY_TEMP = @QUERY_CONST
  SET @RANGE = 'Y'
  SET @DESCRIPTION = 'YTD'
  SET @SEARCH_DATE = 'x2d_date = ' + CAST(CONVERT(VARCHAR(10), @DATEFROM_YEAR, 112) AS VARCHAR)
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DATE', LTRIM(@SEARCH_DATE))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@HOUR', LTRIM(@HOUR))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@RANGE', LTRIM(@RANGE))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DESCRIPTION', LTRIM(@DESCRIPTION))
  SET @FINAL_QUERY = @FINAL_QUERY
                   + ' UNION '
                   + @QUERY_TEMP 
                   + ' AND x2d_weekday = ' + @DOW

  SET @QUERY = REPLACE(@FINAL_QUERY, '@SOURCE', @Source)
           
  EXECUTE sp_executesql @QUERY
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetTerminalIdFromBaseName]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_GetTerminalIdFromBaseName]
GO
CREATE PROCEDURE [dbo].[Layout_GetTerminalIdFromBaseName] 
@baseName BigInt,
@count_terminal Int OUT
AS
BEGIN
  SELECT @count_terminal=TE_TERMINAL_ID FROM TERMINALS WHERE TE_BASE_NAME = @baseName
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetTerminalIdFromAssetNumber]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_GetTerminalIdFromAssetNumber]
GO
CREATE PROCEDURE [dbo].[Layout_GetTerminalIdFromAssetNumber] 
@assetNumber BigInt,
@count_terminal Int OUT
AS
BEGIN
  SELECT @count_terminal=TE_TERMINAL_ID FROM TERMINALS WHERE TE_ASSET_NUMBER = @assetNumber
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetTerminalIdFromFloorId]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Layout_GetTerminalIdFromFloorId]
GO
CREATE PROCEDURE [dbo].[Layout_GetTerminalIdFromFloorId] 
@floorId nvarchar(20),
@count_terminal Int OUT
AS
BEGIN
  SELECT @count_terminal=TE_TERMINAL_ID FROM TERMINALS WHERE TE_FLOOR_ID = @floorId
END
GO