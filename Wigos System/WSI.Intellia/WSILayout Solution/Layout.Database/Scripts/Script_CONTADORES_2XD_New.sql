
/************************ IF  EXISTS DROP TABLES ***************************************/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[H_L2D_TMH]') AND type in (N'U'))
DROP TABLE [dbo].[H_L2D_TMH]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[H_M2D_TMH]') AND type in (N'U'))
DROP TABLE [dbo].[H_M2D_TMH]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[H_T2D_TMH]') AND type in (N'U'))
DROP TABLE [dbo].[H_T2D_TMH]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[H_W2D_TMH]') AND type in (N'U'))
DROP TABLE [dbo].[H_W2D_TMH]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[H_Y2D_TMH]') AND type in (N'U'))
DROP TABLE [dbo].[H_Y2D_TMH]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[H_L2D_SMH]') AND type in (N'U'))
DROP TABLE [dbo].[H_L2D_SMH]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[H_M2D_SMH]') AND type in (N'U'))
DROP TABLE [dbo].[H_M2D_SMH]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[H_T2D_SMH]') AND type in (N'U'))
DROP TABLE [dbo].[H_T2D_SMH]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[H_W2D_SMH]') AND type in (N'U'))
DROP TABLE [dbo].[H_W2D_SMH]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[H_Y2D_SMH]') AND type in (N'U'))
DROP TABLE [dbo].[H_Y2D_SMH]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[H_METERS_DEFINITION]') AND type in (N'U'))
DROP TABLE [dbo].[H_METERS_DEFINITION]
GO

/*************** METERS DEFINITION *************************/

CREATE TABLE [dbo].[H_METERS_DEFINITION](
	[hmd_meter_id] [int] NOT NULL,   -- Identificador del meter 
	[hmd_meter_item] [int] NOT NULL, -- Subitem de meter (G�nero: Hombre, Mujer,...)
  [hmd_meter_type] [int] NOT NULL, -- Tipo de datos de meter (%, Moneda,...)
	[hmd_description] [nvarchar](250) NULL, -- Descripci�n del Meter ( GENERO, HOMBRE,... )
	[hmd_meter_mask] [int] NOT NULL, -- MASCARA DE BITS (L2D, Y2D, M2D, W2D, T2D, N...) Define en que tablas realiza Updates el meter
	[hmd_meter_enabled] bit NOT NULL, -- Permite establecer si deseamos realizar el Update en el meter	
	[hmd_meter_last_update] [int] NOT NULL, -- 20150523
	[hmd_meter_device] [int] NOT NULL, -- permite establecer el device 'uso' donde ser� utilizado (GUI, CASHIER, LAYOUT, ...)
	[hmd_visible] [bit] NOT NULL		-- Permite establecer si el meter es visible donde ser� utilizado (GUI, CASHIER, LAYOUT, ...)
	
)
GO

/*************** METERS DEFINITION VALUES ******************/

INSERT [dbo].[H_METERS_DEFINITION] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible]) VALUES (1, 0, 1, N'BET', 0, 1, 20150622, 1, 1)
INSERT [dbo].[H_METERS_DEFINITION] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible]) VALUES (2, 0, 1, N'OCC. GENDER', 0, 1, 20150622, 1, 1)
INSERT [dbo].[H_METERS_DEFINITION] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible]) VALUES (2, 1, 1, N'MALE', 0, 1, 20150622, 1, 1)
INSERT [dbo].[H_METERS_DEFINITION] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible]) VALUES (2, 2, 1, N'FEMALE', 0, 1, 20150622, 1, 1)
INSERT [dbo].[H_METERS_DEFINITION] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible]) VALUES (2, 3, 1, N'UNKNOWN', 0, 1, 20150622, 1, 1)
INSERT [dbo].[H_METERS_DEFINITION] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible]) VALUES (3, 0, 1, N'OCC. PLAYER TYPE', 0, 1, 20150622, 1, 1)
INSERT [dbo].[H_METERS_DEFINITION] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible]) VALUES (3, 1, 1, N'REGISTERED', 0, 1, 20150622, 1, 1)
INSERT [dbo].[H_METERS_DEFINITION] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible]) VALUES (3, 2, 1, N'ANONYMOUS', 0, 1, 20150622, 1, 1)
INSERT [dbo].[H_METERS_DEFINITION] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible]) VALUES (3, 3, 1, N'UNKNOWN', 0, 1, 20150622, 1, 1)
INSERT [dbo].[H_METERS_DEFINITION] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible]) VALUES (4, 0, 1, N'OCCUPATION', 0, 1, 20150622, 1, 1)
INSERT [dbo].[H_METERS_DEFINITION] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible]) VALUES (5, 0, 1, N'COIN IN', 0, 1, 20150622, 1, 1)
INSERT [dbo].[H_METERS_DEFINITION] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible]) VALUES (6, 0, 1, N'NEW CLIENTS', 0, 1, 20150622, 1, 1)
INSERT [dbo].[H_METERS_DEFINITION] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible]) VALUES (7, 0, 1, N'HIGH ROLLERS', 0, 1, 20150622, 1, 1)
INSERT [dbo].[H_METERS_DEFINITION] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible]) VALUES (8, 0, 1, N'PROMOTIONS ASSIGNED', 0, 1, 20150622, 1, 1)
INSERT [dbo].[H_METERS_DEFINITION] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible]) VALUES (9, 0, 1, N'NET WIN', 0, 1, 20150622, 1, 1)
INSERT [dbo].[H_METERS_DEFINITION] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible]) VALUES (10, 0, 1, N'THEORETICAL WIN', 0, 1, 20150622, 1, 1)
INSERT [dbo].[H_METERS_DEFINITION] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible]) VALUES (11, 0, 1, N'LOSS', 0, 1, 20150622, 1, 1)

/*************** TERMINALES *************************/

CREATE TABLE [dbo].[H_T2D_TMH](
	[x2d_date] [int] NOT NULL,
	[x2d_weekday] [tinyint] NOT NULL,	-- day week (1=Domingo, 2=Lunes, 3=Martes, 4=Miercoles, ...)
	[x2d_id] [int] NOT NULL,
	[x2d_meter_id] [int] NOT NULL,
  [x2d_meter_item] [int] NOT NULL,		
	[x2d_00_min] [int] NULL,
	[x2d_00_max] [bigint] NULL,
	[x2d_00_acc] [bigint] NULL,
	[x2d_00_avg] [decimal] NULL,
	[x2d_00_num] [bigint] NULL,
	[x2d_01_min] [int] NULL,
	[x2d_01_max] [bigint] NULL,
	[x2d_01_acc] [bigint] NULL,
	[x2d_01_avg] [decimal] NULL,
	[x2d_01_num] [bigint] NULL,
	[x2d_02_min] [int] NULL,
	[x2d_02_max] [bigint] NULL,
	[x2d_02_acc] [bigint] NULL,
	[x2d_02_avg] [decimal] NULL,
	[x2d_02_num] [bigint] NULL,
	[x2d_03_min] [int] NULL,
	[x2d_03_max] [bigint] NULL,
	[x2d_03_acc] [bigint] NULL,
	[x2d_03_avg] [decimal] NULL,
	[x2d_03_num] [bigint] NULL,
	[x2d_04_min] [int] NULL,
	[x2d_04_max] [bigint] NULL,
	[x2d_04_acc] [bigint] NULL,
	[x2d_04_avg] [decimal] NULL,
	[x2d_04_num] [bigint] NULL,
	[x2d_05_min] [int] NULL,
	[x2d_05_max] [bigint] NULL,
	[x2d_05_acc] [bigint] NULL,
	[x2d_05_avg] [decimal] NULL,
	[x2d_05_num] [bigint] NULL,
	[x2d_06_min] [int] NULL,
	[x2d_06_max] [bigint] NULL,
	[x2d_06_acc] [bigint] NULL,
	[x2d_06_avg] [decimal] NULL,
	[x2d_06_num] [bigint] NULL,
	[x2d_07_min] [int] NULL,
	[x2d_07_max] [bigint] NULL,
	[x2d_07_acc] [bigint] NULL,
	[x2d_07_avg] [decimal] NULL,
	[x2d_07_num] [bigint] NULL,
	[x2d_08_min] [int] NULL,
	[x2d_08_max] [bigint] NULL,
	[x2d_08_acc] [bigint] NULL,
	[x2d_08_avg] [decimal] NULL,
	[x2d_08_num] [bigint] NULL,
	[x2d_09_min] [int] NULL,
	[x2d_09_max] [bigint] NULL,
	[x2d_09_acc] [bigint] NULL,
	[x2d_09_avg] [decimal] NULL,
	[x2d_09_num] [bigint] NULL,
	[x2d_10_min] [int] NULL,
	[x2d_10_max] [bigint] NULL,
	[x2d_10_acc] [bigint] NULL,
	[x2d_10_avg] [decimal] NULL,
	[x2d_10_num] [bigint] NULL,
	[x2d_11_min] [int] NULL,
	[x2d_11_max] [bigint] NULL,
	[x2d_11_acc] [bigint] NULL,
	[x2d_11_avg] [decimal] NULL,
	[x2d_11_num] [bigint] NULL,
	[x2d_12_min] [int] NULL,
	[x2d_12_max] [bigint] NULL,
	[x2d_12_acc] [bigint] NULL,
	[x2d_12_avg] [decimal] NULL,
	[x2d_12_num] [bigint] NULL,
	[x2d_13_min] [int] NULL,
	[x2d_13_max] [bigint] NULL,
	[x2d_13_acc] [bigint] NULL,
	[x2d_13_avg] [decimal] NULL,
	[x2d_13_num] [bigint] NULL,
	[x2d_14_min] [int] NULL,
	[x2d_14_max] [bigint] NULL,
	[x2d_14_acc] [bigint] NULL,
	[x2d_14_avg] [decimal] NULL,
	[x2d_14_num] [bigint] NULL,
	[x2d_15_min] [int] NULL,
	[x2d_15_max] [bigint] NULL,
	[x2d_15_acc] [bigint] NULL,
	[x2d_15_avg] [decimal] NULL,
	[x2d_15_num] [bigint] NULL,
	[x2d_16_min] [int] NULL,
	[x2d_16_max] [bigint] NULL,
	[x2d_16_acc] [bigint] NULL,
	[x2d_16_avg] [decimal] NULL,
	[x2d_16_num] [bigint] NULL,
	[x2d_17_min] [int] NULL,
	[x2d_17_max] [bigint] NULL,
	[x2d_17_acc] [bigint] NULL,
	[x2d_17_avg] [decimal] NULL,
	[x2d_17_num] [bigint] NULL,
	[x2d_18_min] [int] NULL,
	[x2d_18_max] [bigint] NULL,
	[x2d_18_acc] [bigint] NULL,
	[x2d_18_avg] [decimal] NULL,
	[x2d_18_num] [bigint] NULL,
	[x2d_19_min] [int] NULL,
	[x2d_19_max] [bigint] NULL,
	[x2d_19_acc] [bigint] NULL,
	[x2d_19_avg] [decimal] NULL,
	[x2d_19_num] [bigint] NULL,
	[x2d_20_min] [int] NULL,
	[x2d_20_max] [bigint] NULL,
	[x2d_20_acc] [bigint] NULL,
	[x2d_20_avg] [decimal] NULL,
	[x2d_20_num] [bigint] NULL,
	[x2d_21_min] [int] NULL,
	[x2d_21_max] [bigint] NULL,
	[x2d_21_acc] [bigint] NULL,
	[x2d_21_avg] [decimal] NULL,
	[x2d_21_num] [bigint] NULL,
	[x2d_22_min] [int] NULL,
	[x2d_22_max] [bigint] NULL,
	[x2d_22_acc] [bigint] NULL,
	[x2d_22_avg] [decimal] NULL,
	[x2d_22_num] [bigint] NULL,
	[x2d_23_min] [int] NULL,
	[x2d_23_max] [bigint] NULL,
	[x2d_23_acc] [bigint] NULL,
	[x2d_23_avg] [decimal] NULL,
	[x2d_23_num] [bigint] NULL,
	[x2d_dd_min] [int] NULL,
	[x2d_dd_max] [bigint] NULL,
	[x2d_dd_acc] [bigint] NULL,
	[x2d_dd_avg] [decimal] NULL,
	[x2d_dd_num] [bigint] NULL,
 CONSTRAINT [PK_T2D_TMH] PRIMARY KEY CLUSTERED 
(
	[x2d_date] ASC,
	[x2d_id] ASC,
  [x2d_meter_id] ASC,
  [x2d_meter_item] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[H_W2D_TMH](
	[x2d_date] [int] NOT NULL,
	[x2d_weekday] [tinyint] NOT NULL,	-- day week (1=Domingo, 2=Lunes, 3=Martes, 4=Miercoles, ...)
	[x2d_id] [int] NOT NULL,
	[x2d_meter_id] [int] NOT NULL,
  [x2d_meter_item] [int] NOT NULL,		
	[x2d_00_min] [int] NULL,
	[x2d_00_max] [bigint] NULL,
	[x2d_00_acc] [bigint] NULL,
	[x2d_00_avg] [decimal] NULL,
	[x2d_00_num] [bigint] NULL,
	[x2d_01_min] [int] NULL,
	[x2d_01_max] [bigint] NULL,
	[x2d_01_acc] [bigint] NULL,
	[x2d_01_avg] [decimal] NULL,
	[x2d_01_num] [bigint] NULL,
	[x2d_02_min] [int] NULL,
	[x2d_02_max] [bigint] NULL,
	[x2d_02_acc] [bigint] NULL,
	[x2d_02_avg] [decimal] NULL,
	[x2d_02_num] [bigint] NULL,
	[x2d_03_min] [int] NULL,
	[x2d_03_max] [bigint] NULL,
	[x2d_03_acc] [bigint] NULL,
	[x2d_03_avg] [decimal] NULL,
	[x2d_03_num] [bigint] NULL,
	[x2d_04_min] [int] NULL,
	[x2d_04_max] [bigint] NULL,
	[x2d_04_acc] [bigint] NULL,
	[x2d_04_avg] [decimal] NULL,
	[x2d_04_num] [bigint] NULL,
	[x2d_05_min] [int] NULL,
	[x2d_05_max] [bigint] NULL,
	[x2d_05_acc] [bigint] NULL,
	[x2d_05_avg] [decimal] NULL,
	[x2d_05_num] [bigint] NULL,
	[x2d_06_min] [int] NULL,
	[x2d_06_max] [bigint] NULL,
	[x2d_06_acc] [bigint] NULL,
	[x2d_06_avg] [decimal] NULL,
	[x2d_06_num] [bigint] NULL,
	[x2d_07_min] [int] NULL,
	[x2d_07_max] [bigint] NULL,
	[x2d_07_acc] [bigint] NULL,
	[x2d_07_avg] [decimal] NULL,
	[x2d_07_num] [bigint] NULL,
	[x2d_08_min] [int] NULL,
	[x2d_08_max] [bigint] NULL,
	[x2d_08_acc] [bigint] NULL,
	[x2d_08_avg] [decimal] NULL,
	[x2d_08_num] [bigint] NULL,
	[x2d_09_min] [int] NULL,
	[x2d_09_max] [bigint] NULL,
	[x2d_09_acc] [bigint] NULL,
	[x2d_09_avg] [decimal] NULL,
	[x2d_09_num] [bigint] NULL,
	[x2d_10_min] [int] NULL,
	[x2d_10_max] [bigint] NULL,
	[x2d_10_acc] [bigint] NULL,
	[x2d_10_avg] [decimal] NULL,
	[x2d_10_num] [bigint] NULL,
	[x2d_11_min] [int] NULL,
	[x2d_11_max] [bigint] NULL,
	[x2d_11_acc] [bigint] NULL,
	[x2d_11_avg] [decimal] NULL,
	[x2d_11_num] [bigint] NULL,
	[x2d_12_min] [int] NULL,
	[x2d_12_max] [bigint] NULL,
	[x2d_12_acc] [bigint] NULL,
	[x2d_12_avg] [decimal] NULL,
	[x2d_12_num] [bigint] NULL,
	[x2d_13_min] [int] NULL,
	[x2d_13_max] [bigint] NULL,
	[x2d_13_acc] [bigint] NULL,
	[x2d_13_avg] [decimal] NULL,
	[x2d_13_num] [bigint] NULL,
	[x2d_14_min] [int] NULL,
	[x2d_14_max] [bigint] NULL,
	[x2d_14_acc] [bigint] NULL,
	[x2d_14_avg] [decimal] NULL,
	[x2d_14_num] [bigint] NULL,
	[x2d_15_min] [int] NULL,
	[x2d_15_max] [bigint] NULL,
	[x2d_15_acc] [bigint] NULL,
	[x2d_15_avg] [decimal] NULL,
	[x2d_15_num] [bigint] NULL,
	[x2d_16_min] [int] NULL,
	[x2d_16_max] [bigint] NULL,
	[x2d_16_acc] [bigint] NULL,
	[x2d_16_avg] [decimal] NULL,
	[x2d_16_num] [bigint] NULL,
	[x2d_17_min] [int] NULL,
	[x2d_17_max] [bigint] NULL,
	[x2d_17_acc] [bigint] NULL,
	[x2d_17_avg] [decimal] NULL,
	[x2d_17_num] [bigint] NULL,
	[x2d_18_min] [int] NULL,
	[x2d_18_max] [bigint] NULL,
	[x2d_18_acc] [bigint] NULL,
	[x2d_18_avg] [decimal] NULL,
	[x2d_18_num] [bigint] NULL,
	[x2d_19_min] [int] NULL,
	[x2d_19_max] [bigint] NULL,
	[x2d_19_acc] [bigint] NULL,
	[x2d_19_avg] [decimal] NULL,
	[x2d_19_num] [bigint] NULL,
	[x2d_20_min] [int] NULL,
	[x2d_20_max] [bigint] NULL,
	[x2d_20_acc] [bigint] NULL,
	[x2d_20_avg] [decimal] NULL,
	[x2d_20_num] [bigint] NULL,
	[x2d_21_min] [int] NULL,
	[x2d_21_max] [bigint] NULL,
	[x2d_21_acc] [bigint] NULL,
	[x2d_21_avg] [decimal] NULL,
	[x2d_21_num] [bigint] NULL,
	[x2d_22_min] [int] NULL,
	[x2d_22_max] [bigint] NULL,
	[x2d_22_acc] [bigint] NULL,
	[x2d_22_avg] [decimal] NULL,
	[x2d_22_num] [bigint] NULL,
	[x2d_23_min] [int] NULL,
	[x2d_23_max] [bigint] NULL,
	[x2d_23_acc] [bigint] NULL,
	[x2d_23_avg] [decimal] NULL,
	[x2d_23_num] [bigint] NULL,
	[x2d_dd_min] [int] NULL,
	[x2d_dd_max] [bigint] NULL,
	[x2d_dd_acc] [bigint] NULL,
	[x2d_dd_avg] [decimal] NULL,
	[x2d_dd_num] [bigint] NULL,
 CONSTRAINT [PK_W2D_TMH] PRIMARY KEY CLUSTERED 
(
	[x2d_date] ASC,
  [x2d_weekday] ASC,
	[x2d_id] ASC,
  [x2d_meter_id] ASC,
  [x2d_meter_item] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[H_M2D_TMH](
	[x2d_date] [int] NOT NULL,
	[x2d_weekday] [tinyint] NOT NULL,	-- day week (1=Domingo, 2=Lunes, 3=Martes, 4=Miercoles, ...)
	[x2d_id] [int] NOT NULL,
	[x2d_meter_id] [int] NOT NULL,
  [x2d_meter_item] [int] NOT NULL,		
	[x2d_00_min] [int] NULL,
	[x2d_00_max] [bigint] NULL,
	[x2d_00_acc] [bigint] NULL,
	[x2d_00_avg] [decimal] NULL,
	[x2d_00_num] [bigint] NULL,
	[x2d_01_min] [int] NULL,
	[x2d_01_max] [bigint] NULL,
	[x2d_01_acc] [bigint] NULL,
	[x2d_01_avg] [decimal] NULL,
	[x2d_01_num] [bigint] NULL,
	[x2d_02_min] [int] NULL,
	[x2d_02_max] [bigint] NULL,
	[x2d_02_acc] [bigint] NULL,
	[x2d_02_avg] [decimal] NULL,
	[x2d_02_num] [bigint] NULL,
	[x2d_03_min] [int] NULL,
	[x2d_03_max] [bigint] NULL,
	[x2d_03_acc] [bigint] NULL,
	[x2d_03_avg] [decimal] NULL,
	[x2d_03_num] [bigint] NULL,
	[x2d_04_min] [int] NULL,
	[x2d_04_max] [bigint] NULL,
	[x2d_04_acc] [bigint] NULL,
	[x2d_04_avg] [decimal] NULL,
	[x2d_04_num] [bigint] NULL,
	[x2d_05_min] [int] NULL,
	[x2d_05_max] [bigint] NULL,
	[x2d_05_acc] [bigint] NULL,
	[x2d_05_avg] [decimal] NULL,
	[x2d_05_num] [bigint] NULL,
	[x2d_06_min] [int] NULL,
	[x2d_06_max] [bigint] NULL,
	[x2d_06_acc] [bigint] NULL,
	[x2d_06_avg] [decimal] NULL,
	[x2d_06_num] [bigint] NULL,
	[x2d_07_min] [int] NULL,
	[x2d_07_max] [bigint] NULL,
	[x2d_07_acc] [bigint] NULL,
	[x2d_07_avg] [decimal] NULL,
	[x2d_07_num] [bigint] NULL,
	[x2d_08_min] [int] NULL,
	[x2d_08_max] [bigint] NULL,
	[x2d_08_acc] [bigint] NULL,
	[x2d_08_avg] [decimal] NULL,
	[x2d_08_num] [bigint] NULL,
	[x2d_09_min] [int] NULL,
	[x2d_09_max] [bigint] NULL,
	[x2d_09_acc] [bigint] NULL,
	[x2d_09_avg] [decimal] NULL,
	[x2d_09_num] [bigint] NULL,
	[x2d_10_min] [int] NULL,
	[x2d_10_max] [bigint] NULL,
	[x2d_10_acc] [bigint] NULL,
	[x2d_10_avg] [decimal] NULL,
	[x2d_10_num] [bigint] NULL,
	[x2d_11_min] [int] NULL,
	[x2d_11_max] [bigint] NULL,
	[x2d_11_acc] [bigint] NULL,
	[x2d_11_avg] [decimal] NULL,
	[x2d_11_num] [bigint] NULL,
	[x2d_12_min] [int] NULL,
	[x2d_12_max] [bigint] NULL,
	[x2d_12_acc] [bigint] NULL,
	[x2d_12_avg] [decimal] NULL,
	[x2d_12_num] [bigint] NULL,
	[x2d_13_min] [int] NULL,
	[x2d_13_max] [bigint] NULL,
	[x2d_13_acc] [bigint] NULL,
	[x2d_13_avg] [decimal] NULL,
	[x2d_13_num] [bigint] NULL,
	[x2d_14_min] [int] NULL,
	[x2d_14_max] [bigint] NULL,
	[x2d_14_acc] [bigint] NULL,
	[x2d_14_avg] [decimal] NULL,
	[x2d_14_num] [bigint] NULL,
	[x2d_15_min] [int] NULL,
	[x2d_15_max] [bigint] NULL,
	[x2d_15_acc] [bigint] NULL,
	[x2d_15_avg] [decimal] NULL,
	[x2d_15_num] [bigint] NULL,
	[x2d_16_min] [int] NULL,
	[x2d_16_max] [bigint] NULL,
	[x2d_16_acc] [bigint] NULL,
	[x2d_16_avg] [decimal] NULL,
	[x2d_16_num] [bigint] NULL,
	[x2d_17_min] [int] NULL,
	[x2d_17_max] [bigint] NULL,
	[x2d_17_acc] [bigint] NULL,
	[x2d_17_avg] [decimal] NULL,
	[x2d_17_num] [bigint] NULL,
	[x2d_18_min] [int] NULL,
	[x2d_18_max] [bigint] NULL,
	[x2d_18_acc] [bigint] NULL,
	[x2d_18_avg] [decimal] NULL,
	[x2d_18_num] [bigint] NULL,
	[x2d_19_min] [int] NULL,
	[x2d_19_max] [bigint] NULL,
	[x2d_19_acc] [bigint] NULL,
	[x2d_19_avg] [decimal] NULL,
	[x2d_19_num] [bigint] NULL,
	[x2d_20_min] [int] NULL,
	[x2d_20_max] [bigint] NULL,
	[x2d_20_acc] [bigint] NULL,
	[x2d_20_avg] [decimal] NULL,
	[x2d_20_num] [bigint] NULL,
	[x2d_21_min] [int] NULL,
	[x2d_21_max] [bigint] NULL,
	[x2d_21_acc] [bigint] NULL,
	[x2d_21_avg] [decimal] NULL,
	[x2d_21_num] [bigint] NULL,
	[x2d_22_min] [int] NULL,
	[x2d_22_max] [bigint] NULL,
	[x2d_22_acc] [bigint] NULL,
	[x2d_22_avg] [decimal] NULL,
	[x2d_22_num] [bigint] NULL,
	[x2d_23_min] [int] NULL,
	[x2d_23_max] [bigint] NULL,
	[x2d_23_acc] [bigint] NULL,
	[x2d_23_avg] [decimal] NULL,
	[x2d_23_num] [bigint] NULL,
	[x2d_dd_min] [int] NULL,
	[x2d_dd_max] [bigint] NULL,
	[x2d_dd_acc] [bigint] NULL,
	[x2d_dd_avg] [decimal] NULL,
	[x2d_dd_num] [bigint] NULL,
 CONSTRAINT [PK_M2D_TMH] PRIMARY KEY CLUSTERED 
(
	[x2d_date] ASC,
  [x2d_weekday] ASC,
	[x2d_id] ASC,
  [x2d_meter_id] ASC,
  [x2d_meter_item] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[H_Y2D_TMH](
	[x2d_date] [int] NOT NULL,
	[x2d_weekday] [tinyint] NOT NULL,	-- day week (1=Domingo, 2=Lunes, 3=Martes, 4=Miercoles, ...)
	[x2d_id] [int] NOT NULL,
	[x2d_meter_id] [int] NOT NULL,
  [x2d_meter_item] [int] NOT NULL,		
	[x2d_00_min] [int] NULL,
	[x2d_00_max] [bigint] NULL,
	[x2d_00_acc] [bigint] NULL,
	[x2d_00_avg] [decimal] NULL,
	[x2d_00_num] [bigint] NULL,
	[x2d_01_min] [int] NULL,
	[x2d_01_max] [bigint] NULL,
	[x2d_01_acc] [bigint] NULL,
	[x2d_01_avg] [decimal] NULL,
	[x2d_01_num] [bigint] NULL,
	[x2d_02_min] [int] NULL,
	[x2d_02_max] [bigint] NULL,
	[x2d_02_acc] [bigint] NULL,
	[x2d_02_avg] [decimal] NULL,
	[x2d_02_num] [bigint] NULL,
	[x2d_03_min] [int] NULL,
	[x2d_03_max] [bigint] NULL,
	[x2d_03_acc] [bigint] NULL,
	[x2d_03_avg] [decimal] NULL,
	[x2d_03_num] [bigint] NULL,
	[x2d_04_min] [int] NULL,
	[x2d_04_max] [bigint] NULL,
	[x2d_04_acc] [bigint] NULL,
	[x2d_04_avg] [decimal] NULL,
	[x2d_04_num] [bigint] NULL,
	[x2d_05_min] [int] NULL,
	[x2d_05_max] [bigint] NULL,
	[x2d_05_acc] [bigint] NULL,
	[x2d_05_avg] [decimal] NULL,
	[x2d_05_num] [bigint] NULL,
	[x2d_06_min] [int] NULL,
	[x2d_06_max] [bigint] NULL,
	[x2d_06_acc] [bigint] NULL,
	[x2d_06_avg] [decimal] NULL,
	[x2d_06_num] [bigint] NULL,
	[x2d_07_min] [int] NULL,
	[x2d_07_max] [bigint] NULL,
	[x2d_07_acc] [bigint] NULL,
	[x2d_07_avg] [decimal] NULL,
	[x2d_07_num] [bigint] NULL,
	[x2d_08_min] [int] NULL,
	[x2d_08_max] [bigint] NULL,
	[x2d_08_acc] [bigint] NULL,
	[x2d_08_avg] [decimal] NULL,
	[x2d_08_num] [bigint] NULL,
	[x2d_09_min] [int] NULL,
	[x2d_09_max] [bigint] NULL,
	[x2d_09_acc] [bigint] NULL,
	[x2d_09_avg] [decimal] NULL,
	[x2d_09_num] [bigint] NULL,
	[x2d_10_min] [int] NULL,
	[x2d_10_max] [bigint] NULL,
	[x2d_10_acc] [bigint] NULL,
	[x2d_10_avg] [decimal] NULL,
	[x2d_10_num] [bigint] NULL,
	[x2d_11_min] [int] NULL,
	[x2d_11_max] [bigint] NULL,
	[x2d_11_acc] [bigint] NULL,
	[x2d_11_avg] [decimal] NULL,
	[x2d_11_num] [bigint] NULL,
	[x2d_12_min] [int] NULL,
	[x2d_12_max] [bigint] NULL,
	[x2d_12_acc] [bigint] NULL,
	[x2d_12_avg] [decimal] NULL,
	[x2d_12_num] [bigint] NULL,
	[x2d_13_min] [int] NULL,
	[x2d_13_max] [bigint] NULL,
	[x2d_13_acc] [bigint] NULL,
	[x2d_13_avg] [decimal] NULL,
	[x2d_13_num] [bigint] NULL,
	[x2d_14_min] [int] NULL,
	[x2d_14_max] [bigint] NULL,
	[x2d_14_acc] [bigint] NULL,
	[x2d_14_avg] [decimal] NULL,
	[x2d_14_num] [bigint] NULL,
	[x2d_15_min] [int] NULL,
	[x2d_15_max] [bigint] NULL,
	[x2d_15_acc] [bigint] NULL,
	[x2d_15_avg] [decimal] NULL,
	[x2d_15_num] [bigint] NULL,
	[x2d_16_min] [int] NULL,
	[x2d_16_max] [bigint] NULL,
	[x2d_16_acc] [bigint] NULL,
	[x2d_16_avg] [decimal] NULL,
	[x2d_16_num] [bigint] NULL,
	[x2d_17_min] [int] NULL,
	[x2d_17_max] [bigint] NULL,
	[x2d_17_acc] [bigint] NULL,
	[x2d_17_avg] [decimal] NULL,
	[x2d_17_num] [bigint] NULL,
	[x2d_18_min] [int] NULL,
	[x2d_18_max] [bigint] NULL,
	[x2d_18_acc] [bigint] NULL,
	[x2d_18_avg] [decimal] NULL,
	[x2d_18_num] [bigint] NULL,
	[x2d_19_min] [int] NULL,
	[x2d_19_max] [bigint] NULL,
	[x2d_19_acc] [bigint] NULL,
	[x2d_19_avg] [decimal] NULL,
	[x2d_19_num] [bigint] NULL,
	[x2d_20_min] [int] NULL,
	[x2d_20_max] [bigint] NULL,
	[x2d_20_acc] [bigint] NULL,
	[x2d_20_avg] [decimal] NULL,
	[x2d_20_num] [bigint] NULL,
	[x2d_21_min] [int] NULL,
	[x2d_21_max] [bigint] NULL,
	[x2d_21_acc] [bigint] NULL,
	[x2d_21_avg] [decimal] NULL,
	[x2d_21_num] [bigint] NULL,
	[x2d_22_min] [int] NULL,
	[x2d_22_max] [bigint] NULL,
	[x2d_22_acc] [bigint] NULL,
	[x2d_22_avg] [decimal] NULL,
	[x2d_22_num] [bigint] NULL,
	[x2d_23_min] [int] NULL,
	[x2d_23_max] [bigint] NULL,
	[x2d_23_acc] [bigint] NULL,
	[x2d_23_avg] [decimal] NULL,
	[x2d_23_num] [bigint] NULL,
	[x2d_dd_min] [int] NULL,
	[x2d_dd_max] [bigint] NULL,
	[x2d_dd_acc] [bigint] NULL,
	[x2d_dd_avg] [decimal] NULL,
	[x2d_dd_num] [bigint] NULL,
 CONSTRAINT [PK_Y2D_TMH] PRIMARY KEY CLUSTERED 
(
	[x2d_date] ASC,
  [x2d_weekday] ASC,
	[x2d_id] ASC,
  [x2d_meter_id] ASC,
  [x2d_meter_item] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[H_L2D_TMH](
	[x2d_date] [int] NOT NULL,			-- 20150523
	[x2d_weekday] [tinyint] NOT NULL,	-- day week (1=Domingo, 2=Lunes, 3=Martes, 4=Miercoles, ...)
	[x2d_id] [int] NOT NULL,			--TERMINAL id
	[x2d_meter_id] [int] NOT NULL,		-- Meter id (FK - H_METERS_DEFINITION 
  [x2d_meter_item] [int] NOT NULL,		-- Meter item (FK - H_METERS_DEFINITION 
	[x2d_00_min] [int] NULL,
	[x2d_00_max] [bigint] NULL,
	[x2d_00_acc] [bigint] NULL,
	[x2d_00_avg] [decimal] NULL,
	[x2d_00_num] [bigint] NULL,
	[x2d_01_min] [int] NULL,
	[x2d_01_max] [bigint] NULL,
	[x2d_01_acc] [bigint] NULL,
	[x2d_01_avg] [decimal] NULL,
	[x2d_01_num] [bigint] NULL,
	[x2d_02_min] [int] NULL,
	[x2d_02_max] [bigint] NULL,
	[x2d_02_acc] [bigint] NULL,
	[x2d_02_avg] [decimal] NULL,
	[x2d_02_num] [bigint] NULL,
	[x2d_03_min] [int] NULL,
	[x2d_03_max] [bigint] NULL,
	[x2d_03_acc] [bigint] NULL,
	[x2d_03_avg] [decimal] NULL,
	[x2d_03_num] [bigint] NULL,
	[x2d_04_min] [int] NULL,
	[x2d_04_max] [bigint] NULL,
	[x2d_04_acc] [bigint] NULL,
	[x2d_04_avg] [decimal] NULL,
	[x2d_04_num] [bigint] NULL,
	[x2d_05_min] [int] NULL,
	[x2d_05_max] [bigint] NULL,
	[x2d_05_acc] [bigint] NULL,
	[x2d_05_avg] [decimal] NULL,
	[x2d_05_num] [bigint] NULL,
	[x2d_06_min] [int] NULL,
	[x2d_06_max] [bigint] NULL,
	[x2d_06_acc] [bigint] NULL,
	[x2d_06_avg] [decimal] NULL,
	[x2d_06_num] [bigint] NULL,
	[x2d_07_min] [int] NULL,
	[x2d_07_max] [bigint] NULL,
	[x2d_07_acc] [bigint] NULL,
	[x2d_07_avg] [decimal] NULL,
	[x2d_07_num] [bigint] NULL,
	[x2d_08_min] [int] NULL,
	[x2d_08_max] [bigint] NULL,
	[x2d_08_acc] [bigint] NULL,
	[x2d_08_avg] [decimal] NULL,
	[x2d_08_num] [bigint] NULL,
	[x2d_09_min] [int] NULL,
	[x2d_09_max] [bigint] NULL,
	[x2d_09_acc] [bigint] NULL,
	[x2d_09_avg] [decimal] NULL,
	[x2d_09_num] [bigint] NULL,
	[x2d_10_min] [int] NULL,
	[x2d_10_max] [bigint] NULL,
	[x2d_10_acc] [bigint] NULL,
	[x2d_10_avg] [decimal] NULL,
	[x2d_10_num] [bigint] NULL,
	[x2d_11_min] [int] NULL,
	[x2d_11_max] [bigint] NULL,
	[x2d_11_acc] [bigint] NULL,
	[x2d_11_avg] [decimal] NULL,
	[x2d_11_num] [bigint] NULL,
	[x2d_12_min] [int] NULL,
	[x2d_12_max] [bigint] NULL,
	[x2d_12_acc] [bigint] NULL,
	[x2d_12_avg] [decimal] NULL,
	[x2d_12_num] [bigint] NULL,
	[x2d_13_min] [int] NULL,
	[x2d_13_max] [bigint] NULL,
	[x2d_13_acc] [bigint] NULL,
	[x2d_13_avg] [decimal] NULL,
	[x2d_13_num] [bigint] NULL,
	[x2d_14_min] [int] NULL,
	[x2d_14_max] [bigint] NULL,
	[x2d_14_acc] [bigint] NULL,
	[x2d_14_avg] [decimal] NULL,
	[x2d_14_num] [bigint] NULL,
	[x2d_15_min] [int] NULL,
	[x2d_15_max] [bigint] NULL,
	[x2d_15_acc] [bigint] NULL,
	[x2d_15_avg] [decimal] NULL,
	[x2d_15_num] [bigint] NULL,
	[x2d_16_min] [int] NULL,
	[x2d_16_max] [bigint] NULL,
	[x2d_16_acc] [bigint] NULL,
	[x2d_16_avg] [decimal] NULL,
	[x2d_16_num] [bigint] NULL,
	[x2d_17_min] [int] NULL,
	[x2d_17_max] [bigint] NULL,
	[x2d_17_acc] [bigint] NULL,
	[x2d_17_avg] [decimal] NULL,
	[x2d_17_num] [bigint] NULL,
	[x2d_18_min] [int] NULL,
	[x2d_18_max] [bigint] NULL,
	[x2d_18_acc] [bigint] NULL,
	[x2d_18_avg] [decimal] NULL,
	[x2d_18_num] [bigint] NULL,
	[x2d_19_min] [int] NULL,
	[x2d_19_max] [bigint] NULL,
	[x2d_19_acc] [bigint] NULL,
	[x2d_19_avg] [decimal] NULL,
	[x2d_19_num] [bigint] NULL,
	[x2d_20_min] [int] NULL,
	[x2d_20_max] [bigint] NULL,
	[x2d_20_acc] [bigint] NULL,
	[x2d_20_avg] [decimal] NULL,
	[x2d_20_num] [bigint] NULL,
	[x2d_21_min] [int] NULL,
	[x2d_21_max] [bigint] NULL,
	[x2d_21_acc] [bigint] NULL,
	[x2d_21_avg] [decimal] NULL,
	[x2d_21_num] [bigint] NULL,
	[x2d_22_min] [int] NULL,
	[x2d_22_max] [bigint] NULL,
	[x2d_22_acc] [bigint] NULL,
	[x2d_22_avg] [decimal] NULL,
	[x2d_22_num] [bigint] NULL,
	[x2d_23_min] [int] NULL,
	[x2d_23_max] [bigint] NULL,
	[x2d_23_acc] [bigint] NULL,
	[x2d_23_avg] [decimal] NULL,
	[x2d_23_num] [bigint] NULL,
	[x2d_dd_min] [int] NULL,
	[x2d_dd_max] [bigint] NULL,
	[x2d_dd_acc] [bigint] NULL,
	[x2d_dd_avg] [decimal] NULL,
	[x2d_dd_num] [bigint] NULL,
 CONSTRAINT [PK_L2D_TMH] PRIMARY KEY CLUSTERED 
(
	[x2d_date] ASC,
  [x2d_weekday] ASC,
	[x2d_id] ASC,
  [x2d_meter_id] ASC,
  [x2d_meter_item] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/************************ JUGADORES ***************************************/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[H_PVH]') AND type in (N'U'))
DROP TABLE [dbo].[H_PVH]
GO

-- PVH: PLAYERS VISIT HISTORY (DIARIO) 
-- Valores seteados diariamente al finalizar la jornada.
-- Los datos del d�a contienen lo acumulado durante la Visita/Jornada del jugador.
-- Tomar como refer�ncia (BASE DE CLIENTES actual)

CREATE TABLE [dbo].[H_PVH](
	[pvh_account_id] [bigint] NOT NULL,						 -- AccountID
	[pvh_date] [int] NOT NULL,                               -- 20150822  Fecha de la visita
	[pvh_weekday] [int] NOT NULL,                            -- D�a de la semana: 1 Lunes, 2 Martes, ...
	[pvh_visit] [int] NULL,                                  -- Flag Visita (solicita Andreu)
	[pvh_check_in] [datetime] NULL,                          -- Hora entrada al Casino
	[pvh_check_out] [datetime] NULL,                         -- Hora salida del Casino
	[pvh_room_time] [int] NULL,                              -- Tiempo en Sala
	[pvh_game_time] [int] NULL,                              -- Tiempo de Juego
	[pvh_num_plays] [int] NULL,                              -- N� total de Partidas
	[pvh_num_win] [int] NULL,                                -- N� total de Partidas ganadoras
	[pvh_total_bet] [money] NULL,                            -- Total Apostado
	[pvh_avg_bet] [decimal] NULL,                              -- Apuesta media
	[pvh_jackpots_won] [money] NULL,                         -- Total ganado en jackpots
	[pvh_re_played] [money] NULL,                            -- Jugado redimible
	[pvh_nr_played] [money] NULL,                            -- Jugado No redimible
	[pvh_coin_out_theo] [money] NULL,                        -- Coin Out Te�rico
	[pvh_money_in_amount] [money] NULL,                      -- Money In Monto
	[pvh_money_in_tax] [money] NULL,                         -- Money In Tax
	[pvh_money_out] [money] NULL,                            -- Money Out
	[pvh_re_won] [money] NULL,                               -- Ganado 'premio' redimible
	[pvh_nr_won] [money] NULL,                               -- Ganado 'premio' No redimible
	[pvh_devolution] [money] NULL,                           -- Devoluci�n
	[pvh_Tax1] [money] NULL,                                 -- Tax1
	[pvh_Tax2] [money] NULL,                                 -- Tax2
	[pvh_Tax3] [money] NULL,                                 -- Tax3 (Revolucionario)
	[pvh_refills_number] [int] NULL,                         -- N� de recargas
	[pvh_withdrawals_number] [int] NULL,                     -- N� de reintegros
	[pvh_age] [tinyint] NULL,                                -- Edad	
	[pvh_days_make_customer] [int] NULL,                     -- D�as que hace que es cliente
	[pvh_level] [int] NULL,                                  -- Nivel 
	[pvh_points_balance] [bigint] NULL,                      -- Puntos
	[pvh_points_manual] [bigint] NULL,                       -- Puntos Manual
	[pvh_points_won] [bigint] NULL,                          -- Puntos Ganados
	[pvh_points_redeemed] [bigint] NULL,                     -- Puntos redimidos
	[pvh_points_expired] [bigint] NULL,                      -- Puntos Expirados
	
CONSTRAINT [PK_H_PVH] PRIMARY KEY CLUSTERED 
(
	[pvh_account_id] ASC,
	[pvh_date] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/********************** SITE (SALA) *********************************/

CREATE TABLE [dbo].[H_T2D_SMH](
	[x2d_date] [int] NOT NULL,
	[x2d_weekday] [tinyint] NOT NULL,	-- day week (1=Domingo, 2=Lunes, 3=Martes, 4=Miercoles, ...)
	[x2d_id] [int] NOT NULL,
	[x2d_meter_id] [int] NOT NULL,
  [x2d_meter_item] [int] NOT NULL,		
	[x2d_00_min] [int] NULL,
	[x2d_00_max] [bigint] NULL,
	[x2d_00_acc] [bigint] NULL,
	[x2d_00_avg] [decimal] NULL,
	[x2d_00_num] [bigint] NULL,
	[x2d_01_min] [int] NULL,
	[x2d_01_max] [bigint] NULL,
	[x2d_01_acc] [bigint] NULL,
	[x2d_01_avg] [decimal] NULL,
	[x2d_01_num] [bigint] NULL,
	[x2d_02_min] [int] NULL,
	[x2d_02_max] [bigint] NULL,
	[x2d_02_acc] [bigint] NULL,
	[x2d_02_avg] [decimal] NULL,
	[x2d_02_num] [bigint] NULL,
	[x2d_03_min] [int] NULL,
	[x2d_03_max] [bigint] NULL,
	[x2d_03_acc] [bigint] NULL,
	[x2d_03_avg] [decimal] NULL,
	[x2d_03_num] [bigint] NULL,
	[x2d_04_min] [int] NULL,
	[x2d_04_max] [bigint] NULL,
	[x2d_04_acc] [bigint] NULL,
	[x2d_04_avg] [decimal] NULL,
	[x2d_04_num] [bigint] NULL,
	[x2d_05_min] [int] NULL,
	[x2d_05_max] [bigint] NULL,
	[x2d_05_acc] [bigint] NULL,
	[x2d_05_avg] [decimal] NULL,
	[x2d_05_num] [bigint] NULL,
	[x2d_06_min] [int] NULL,
	[x2d_06_max] [bigint] NULL,
	[x2d_06_acc] [bigint] NULL,
	[x2d_06_avg] [decimal] NULL,
	[x2d_06_num] [bigint] NULL,
	[x2d_07_min] [int] NULL,
	[x2d_07_max] [bigint] NULL,
	[x2d_07_acc] [bigint] NULL,
	[x2d_07_avg] [decimal] NULL,
	[x2d_07_num] [bigint] NULL,
	[x2d_08_min] [int] NULL,
	[x2d_08_max] [bigint] NULL,
	[x2d_08_acc] [bigint] NULL,
	[x2d_08_avg] [decimal] NULL,
	[x2d_08_num] [bigint] NULL,
	[x2d_09_min] [int] NULL,
	[x2d_09_max] [bigint] NULL,
	[x2d_09_acc] [bigint] NULL,
	[x2d_09_avg] [decimal] NULL,
	[x2d_09_num] [bigint] NULL,
	[x2d_10_min] [int] NULL,
	[x2d_10_max] [bigint] NULL,
	[x2d_10_acc] [bigint] NULL,
	[x2d_10_avg] [decimal] NULL,
	[x2d_10_num] [bigint] NULL,
	[x2d_11_min] [int] NULL,
	[x2d_11_max] [bigint] NULL,
	[x2d_11_acc] [bigint] NULL,
	[x2d_11_avg] [decimal] NULL,
	[x2d_11_num] [bigint] NULL,
	[x2d_12_min] [int] NULL,
	[x2d_12_max] [bigint] NULL,
	[x2d_12_acc] [bigint] NULL,
	[x2d_12_avg] [decimal] NULL,
	[x2d_12_num] [bigint] NULL,
	[x2d_13_min] [int] NULL,
	[x2d_13_max] [bigint] NULL,
	[x2d_13_acc] [bigint] NULL,
	[x2d_13_avg] [decimal] NULL,
	[x2d_13_num] [bigint] NULL,
	[x2d_14_min] [int] NULL,
	[x2d_14_max] [bigint] NULL,
	[x2d_14_acc] [bigint] NULL,
	[x2d_14_avg] [decimal] NULL,
	[x2d_14_num] [bigint] NULL,
	[x2d_15_min] [int] NULL,
	[x2d_15_max] [bigint] NULL,
	[x2d_15_acc] [bigint] NULL,
	[x2d_15_avg] [decimal] NULL,
	[x2d_15_num] [bigint] NULL,
	[x2d_16_min] [int] NULL,
	[x2d_16_max] [bigint] NULL,
	[x2d_16_acc] [bigint] NULL,
	[x2d_16_avg] [decimal] NULL,
	[x2d_16_num] [bigint] NULL,
	[x2d_17_min] [int] NULL,
	[x2d_17_max] [bigint] NULL,
	[x2d_17_acc] [bigint] NULL,
	[x2d_17_avg] [decimal] NULL,
	[x2d_17_num] [bigint] NULL,
	[x2d_18_min] [int] NULL,
	[x2d_18_max] [bigint] NULL,
	[x2d_18_acc] [bigint] NULL,
	[x2d_18_avg] [decimal] NULL,
	[x2d_18_num] [bigint] NULL,
	[x2d_19_min] [int] NULL,
	[x2d_19_max] [bigint] NULL,
	[x2d_19_acc] [bigint] NULL,
	[x2d_19_avg] [decimal] NULL,
	[x2d_19_num] [bigint] NULL,
	[x2d_20_min] [int] NULL,
	[x2d_20_max] [bigint] NULL,
	[x2d_20_acc] [bigint] NULL,
	[x2d_20_avg] [decimal] NULL,
	[x2d_20_num] [bigint] NULL,
	[x2d_21_min] [int] NULL,
	[x2d_21_max] [bigint] NULL,
	[x2d_21_acc] [bigint] NULL,
	[x2d_21_avg] [decimal] NULL,
	[x2d_21_num] [bigint] NULL,
	[x2d_22_min] [int] NULL,
	[x2d_22_max] [bigint] NULL,
	[x2d_22_acc] [bigint] NULL,
	[x2d_22_avg] [decimal] NULL,
	[x2d_22_num] [bigint] NULL,
	[x2d_23_min] [int] NULL,
	[x2d_23_max] [bigint] NULL,
	[x2d_23_acc] [bigint] NULL,
	[x2d_23_avg] [decimal] NULL,
	[x2d_23_num] [bigint] NULL,
	[x2d_dd_min] [int] NULL,
	[x2d_dd_max] [bigint] NULL,
	[x2d_dd_acc] [bigint] NULL,
	[x2d_dd_avg] [decimal] NULL,
	[x2d_dd_num] [bigint] NULL,
 CONSTRAINT [PK_T2D_SMH] PRIMARY KEY CLUSTERED 
(
	[x2d_date] ASC,
	[x2d_id] ASC,
  [x2d_meter_id] ASC,
  [x2d_meter_item] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[H_W2D_SMH](
	[x2d_date] [int] NOT NULL,
	[x2d_weekday] [tinyint] NOT NULL,	-- day week (1=Domingo, 2=Lunes, 3=Martes, 4=Miercoles, ...)
	[x2d_id] [int] NOT NULL,
	[x2d_meter_id] [int] NOT NULL,
  [x2d_meter_item] [int] NOT NULL,		
	[x2d_00_min] [int] NULL,
	[x2d_00_max] [bigint] NULL,
	[x2d_00_acc] [bigint] NULL,
	[x2d_00_avg] [decimal] NULL,
	[x2d_00_num] [bigint] NULL,
	[x2d_01_min] [int] NULL,
	[x2d_01_max] [bigint] NULL,
	[x2d_01_acc] [bigint] NULL,
	[x2d_01_avg] [decimal] NULL,
	[x2d_01_num] [bigint] NULL,
	[x2d_02_min] [int] NULL,
	[x2d_02_max] [bigint] NULL,
	[x2d_02_acc] [bigint] NULL,
	[x2d_02_avg] [decimal] NULL,
	[x2d_02_num] [bigint] NULL,
	[x2d_03_min] [int] NULL,
	[x2d_03_max] [bigint] NULL,
	[x2d_03_acc] [bigint] NULL,
	[x2d_03_avg] [decimal] NULL,
	[x2d_03_num] [bigint] NULL,
	[x2d_04_min] [int] NULL,
	[x2d_04_max] [bigint] NULL,
	[x2d_04_acc] [bigint] NULL,
	[x2d_04_avg] [decimal] NULL,
	[x2d_04_num] [bigint] NULL,
	[x2d_05_min] [int] NULL,
	[x2d_05_max] [bigint] NULL,
	[x2d_05_acc] [bigint] NULL,
	[x2d_05_avg] [decimal] NULL,
	[x2d_05_num] [bigint] NULL,
	[x2d_06_min] [int] NULL,
	[x2d_06_max] [bigint] NULL,
	[x2d_06_acc] [bigint] NULL,
	[x2d_06_avg] [decimal] NULL,
	[x2d_06_num] [bigint] NULL,
	[x2d_07_min] [int] NULL,
	[x2d_07_max] [bigint] NULL,
	[x2d_07_acc] [bigint] NULL,
	[x2d_07_avg] [decimal] NULL,
	[x2d_07_num] [bigint] NULL,
	[x2d_08_min] [int] NULL,
	[x2d_08_max] [bigint] NULL,
	[x2d_08_acc] [bigint] NULL,
	[x2d_08_avg] [decimal] NULL,
	[x2d_08_num] [bigint] NULL,
	[x2d_09_min] [int] NULL,
	[x2d_09_max] [bigint] NULL,
	[x2d_09_acc] [bigint] NULL,
	[x2d_09_avg] [decimal] NULL,
	[x2d_09_num] [bigint] NULL,
	[x2d_10_min] [int] NULL,
	[x2d_10_max] [bigint] NULL,
	[x2d_10_acc] [bigint] NULL,
	[x2d_10_avg] [decimal] NULL,
	[x2d_10_num] [bigint] NULL,
	[x2d_11_min] [int] NULL,
	[x2d_11_max] [bigint] NULL,
	[x2d_11_acc] [bigint] NULL,
	[x2d_11_avg] [decimal] NULL,
	[x2d_11_num] [bigint] NULL,
	[x2d_12_min] [int] NULL,
	[x2d_12_max] [bigint] NULL,
	[x2d_12_acc] [bigint] NULL,
	[x2d_12_avg] [decimal] NULL,
	[x2d_12_num] [bigint] NULL,
	[x2d_13_min] [int] NULL,
	[x2d_13_max] [bigint] NULL,
	[x2d_13_acc] [bigint] NULL,
	[x2d_13_avg] [decimal] NULL,
	[x2d_13_num] [bigint] NULL,
	[x2d_14_min] [int] NULL,
	[x2d_14_max] [bigint] NULL,
	[x2d_14_acc] [bigint] NULL,
	[x2d_14_avg] [decimal] NULL,
	[x2d_14_num] [bigint] NULL,
	[x2d_15_min] [int] NULL,
	[x2d_15_max] [bigint] NULL,
	[x2d_15_acc] [bigint] NULL,
	[x2d_15_avg] [decimal] NULL,
	[x2d_15_num] [bigint] NULL,
	[x2d_16_min] [int] NULL,
	[x2d_16_max] [bigint] NULL,
	[x2d_16_acc] [bigint] NULL,
	[x2d_16_avg] [decimal] NULL,
	[x2d_16_num] [bigint] NULL,
	[x2d_17_min] [int] NULL,
	[x2d_17_max] [bigint] NULL,
	[x2d_17_acc] [bigint] NULL,
	[x2d_17_avg] [decimal] NULL,
	[x2d_17_num] [bigint] NULL,
	[x2d_18_min] [int] NULL,
	[x2d_18_max] [bigint] NULL,
	[x2d_18_acc] [bigint] NULL,
	[x2d_18_avg] [decimal] NULL,
	[x2d_18_num] [bigint] NULL,
	[x2d_19_min] [int] NULL,
	[x2d_19_max] [bigint] NULL,
	[x2d_19_acc] [bigint] NULL,
	[x2d_19_avg] [decimal] NULL,
	[x2d_19_num] [bigint] NULL,
	[x2d_20_min] [int] NULL,
	[x2d_20_max] [bigint] NULL,
	[x2d_20_acc] [bigint] NULL,
	[x2d_20_avg] [decimal] NULL,
	[x2d_20_num] [bigint] NULL,
	[x2d_21_min] [int] NULL,
	[x2d_21_max] [bigint] NULL,
	[x2d_21_acc] [bigint] NULL,
	[x2d_21_avg] [decimal] NULL,
	[x2d_21_num] [bigint] NULL,
	[x2d_22_min] [int] NULL,
	[x2d_22_max] [bigint] NULL,
	[x2d_22_acc] [bigint] NULL,
	[x2d_22_avg] [decimal] NULL,
	[x2d_22_num] [bigint] NULL,
	[x2d_23_min] [int] NULL,
	[x2d_23_max] [bigint] NULL,
	[x2d_23_acc] [bigint] NULL,
	[x2d_23_avg] [decimal] NULL,
	[x2d_23_num] [bigint] NULL,
	[x2d_dd_min] [int] NULL,
	[x2d_dd_max] [bigint] NULL,
	[x2d_dd_acc] [bigint] NULL,
	[x2d_dd_avg] [decimal] NULL,
	[x2d_dd_num] [bigint] NULL,
 CONSTRAINT [PK_W2D_SMH] PRIMARY KEY CLUSTERED 
(
	[x2d_date] ASC,
  [x2d_weekday] ASC,
	[x2d_id] ASC,
  [x2d_meter_id] ASC,
  [x2d_meter_item] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[H_M2D_SMH](
	[x2d_date] [int] NOT NULL,
	[x2d_weekday] [tinyint] NOT NULL,	-- day week (1=Domingo, 2=Lunes, 3=Martes, 4=Miercoles, ...)
	[x2d_id] [int] NOT NULL,
	[x2d_meter_id] [int] NOT NULL,
  [x2d_meter_item] [int] NOT NULL,		
	[x2d_00_min] [int] NULL,
	[x2d_00_max] [bigint] NULL,
	[x2d_00_acc] [bigint] NULL,
	[x2d_00_avg] [decimal] NULL,
	[x2d_00_num] [bigint] NULL,
	[x2d_01_min] [int] NULL,
	[x2d_01_max] [bigint] NULL,
	[x2d_01_acc] [bigint] NULL,
	[x2d_01_avg] [decimal] NULL,
	[x2d_01_num] [bigint] NULL,
	[x2d_02_min] [int] NULL,
	[x2d_02_max] [bigint] NULL,
	[x2d_02_acc] [bigint] NULL,
	[x2d_02_avg] [decimal] NULL,
	[x2d_02_num] [bigint] NULL,
	[x2d_03_min] [int] NULL,
	[x2d_03_max] [bigint] NULL,
	[x2d_03_acc] [bigint] NULL,
	[x2d_03_avg] [decimal] NULL,
	[x2d_03_num] [bigint] NULL,
	[x2d_04_min] [int] NULL,
	[x2d_04_max] [bigint] NULL,
	[x2d_04_acc] [bigint] NULL,
	[x2d_04_avg] [decimal] NULL,
	[x2d_04_num] [bigint] NULL,
	[x2d_05_min] [int] NULL,
	[x2d_05_max] [bigint] NULL,
	[x2d_05_acc] [bigint] NULL,
	[x2d_05_avg] [decimal] NULL,
	[x2d_05_num] [bigint] NULL,
	[x2d_06_min] [int] NULL,
	[x2d_06_max] [bigint] NULL,
	[x2d_06_acc] [bigint] NULL,
	[x2d_06_avg] [decimal] NULL,
	[x2d_06_num] [bigint] NULL,
	[x2d_07_min] [int] NULL,
	[x2d_07_max] [bigint] NULL,
	[x2d_07_acc] [bigint] NULL,
	[x2d_07_avg] [decimal] NULL,
	[x2d_07_num] [bigint] NULL,
	[x2d_08_min] [int] NULL,
	[x2d_08_max] [bigint] NULL,
	[x2d_08_acc] [bigint] NULL,
	[x2d_08_avg] [decimal] NULL,
	[x2d_08_num] [bigint] NULL,
	[x2d_09_min] [int] NULL,
	[x2d_09_max] [bigint] NULL,
	[x2d_09_acc] [bigint] NULL,
	[x2d_09_avg] [decimal] NULL,
	[x2d_09_num] [bigint] NULL,
	[x2d_10_min] [int] NULL,
	[x2d_10_max] [bigint] NULL,
	[x2d_10_acc] [bigint] NULL,
	[x2d_10_avg] [decimal] NULL,
	[x2d_10_num] [bigint] NULL,
	[x2d_11_min] [int] NULL,
	[x2d_11_max] [bigint] NULL,
	[x2d_11_acc] [bigint] NULL,
	[x2d_11_avg] [decimal] NULL,
	[x2d_11_num] [bigint] NULL,
	[x2d_12_min] [int] NULL,
	[x2d_12_max] [bigint] NULL,
	[x2d_12_acc] [bigint] NULL,
	[x2d_12_avg] [decimal] NULL,
	[x2d_12_num] [bigint] NULL,
	[x2d_13_min] [int] NULL,
	[x2d_13_max] [bigint] NULL,
	[x2d_13_acc] [bigint] NULL,
	[x2d_13_avg] [decimal] NULL,
	[x2d_13_num] [bigint] NULL,
	[x2d_14_min] [int] NULL,
	[x2d_14_max] [bigint] NULL,
	[x2d_14_acc] [bigint] NULL,
	[x2d_14_avg] [decimal] NULL,
	[x2d_14_num] [bigint] NULL,
	[x2d_15_min] [int] NULL,
	[x2d_15_max] [bigint] NULL,
	[x2d_15_acc] [bigint] NULL,
	[x2d_15_avg] [decimal] NULL,
	[x2d_15_num] [bigint] NULL,
	[x2d_16_min] [int] NULL,
	[x2d_16_max] [bigint] NULL,
	[x2d_16_acc] [bigint] NULL,
	[x2d_16_avg] [decimal] NULL,
	[x2d_16_num] [bigint] NULL,
	[x2d_17_min] [int] NULL,
	[x2d_17_max] [bigint] NULL,
	[x2d_17_acc] [bigint] NULL,
	[x2d_17_avg] [decimal] NULL,
	[x2d_17_num] [bigint] NULL,
	[x2d_18_min] [int] NULL,
	[x2d_18_max] [bigint] NULL,
	[x2d_18_acc] [bigint] NULL,
	[x2d_18_avg] [decimal] NULL,
	[x2d_18_num] [bigint] NULL,
	[x2d_19_min] [int] NULL,
	[x2d_19_max] [bigint] NULL,
	[x2d_19_acc] [bigint] NULL,
	[x2d_19_avg] [decimal] NULL,
	[x2d_19_num] [bigint] NULL,
	[x2d_20_min] [int] NULL,
	[x2d_20_max] [bigint] NULL,
	[x2d_20_acc] [bigint] NULL,
	[x2d_20_avg] [decimal] NULL,
	[x2d_20_num] [bigint] NULL,
	[x2d_21_min] [int] NULL,
	[x2d_21_max] [bigint] NULL,
	[x2d_21_acc] [bigint] NULL,
	[x2d_21_avg] [decimal] NULL,
	[x2d_21_num] [bigint] NULL,
	[x2d_22_min] [int] NULL,
	[x2d_22_max] [bigint] NULL,
	[x2d_22_acc] [bigint] NULL,
	[x2d_22_avg] [decimal] NULL,
	[x2d_22_num] [bigint] NULL,
	[x2d_23_min] [int] NULL,
	[x2d_23_max] [bigint] NULL,
	[x2d_23_acc] [bigint] NULL,
	[x2d_23_avg] [decimal] NULL,
	[x2d_23_num] [bigint] NULL,
	[x2d_dd_min] [int] NULL,
	[x2d_dd_max] [bigint] NULL,
	[x2d_dd_acc] [bigint] NULL,
	[x2d_dd_avg] [decimal] NULL,
	[x2d_dd_num] [bigint] NULL,
 CONSTRAINT [PK_M2D_SMH] PRIMARY KEY CLUSTERED 
(
	[x2d_date] ASC,
  [x2d_weekday] ASC,
	[x2d_id] ASC,
  [x2d_meter_id] ASC,
  [x2d_meter_item] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[H_Y2D_SMH](
	[x2d_date] [int] NOT NULL,
	[x2d_weekday] [tinyint] NOT NULL,	-- day week (1=Domingo, 2=Lunes, 3=Martes, 4=Miercoles, ...)
	[x2d_id] [int] NOT NULL,
	[x2d_meter_id] [int] NOT NULL,
  [x2d_meter_item] [int] NOT NULL,		
	[x2d_00_min] [int] NULL,
	[x2d_00_max] [bigint] NULL,
	[x2d_00_acc] [bigint] NULL,
	[x2d_00_avg] [decimal] NULL,
	[x2d_00_num] [bigint] NULL,
	[x2d_01_min] [int] NULL,
	[x2d_01_max] [bigint] NULL,
	[x2d_01_acc] [bigint] NULL,
	[x2d_01_avg] [decimal] NULL,
	[x2d_01_num] [bigint] NULL,
	[x2d_02_min] [int] NULL,
	[x2d_02_max] [bigint] NULL,
	[x2d_02_acc] [bigint] NULL,
	[x2d_02_avg] [decimal] NULL,
	[x2d_02_num] [bigint] NULL,
	[x2d_03_min] [int] NULL,
	[x2d_03_max] [bigint] NULL,
	[x2d_03_acc] [bigint] NULL,
	[x2d_03_avg] [decimal] NULL,
	[x2d_03_num] [bigint] NULL,
	[x2d_04_min] [int] NULL,
	[x2d_04_max] [bigint] NULL,
	[x2d_04_acc] [bigint] NULL,
	[x2d_04_avg] [decimal] NULL,
	[x2d_04_num] [bigint] NULL,
	[x2d_05_min] [int] NULL,
	[x2d_05_max] [bigint] NULL,
	[x2d_05_acc] [bigint] NULL,
	[x2d_05_avg] [decimal] NULL,
	[x2d_05_num] [bigint] NULL,
	[x2d_06_min] [int] NULL,
	[x2d_06_max] [bigint] NULL,
	[x2d_06_acc] [bigint] NULL,
	[x2d_06_avg] [decimal] NULL,
	[x2d_06_num] [bigint] NULL,
	[x2d_07_min] [int] NULL,
	[x2d_07_max] [bigint] NULL,
	[x2d_07_acc] [bigint] NULL,
	[x2d_07_avg] [decimal] NULL,
	[x2d_07_num] [bigint] NULL,
	[x2d_08_min] [int] NULL,
	[x2d_08_max] [bigint] NULL,
	[x2d_08_acc] [bigint] NULL,
	[x2d_08_avg] [decimal] NULL,
	[x2d_08_num] [bigint] NULL,
	[x2d_09_min] [int] NULL,
	[x2d_09_max] [bigint] NULL,
	[x2d_09_acc] [bigint] NULL,
	[x2d_09_avg] [decimal] NULL,
	[x2d_09_num] [bigint] NULL,
	[x2d_10_min] [int] NULL,
	[x2d_10_max] [bigint] NULL,
	[x2d_10_acc] [bigint] NULL,
	[x2d_10_avg] [decimal] NULL,
	[x2d_10_num] [bigint] NULL,
	[x2d_11_min] [int] NULL,
	[x2d_11_max] [bigint] NULL,
	[x2d_11_acc] [bigint] NULL,
	[x2d_11_avg] [decimal] NULL,
	[x2d_11_num] [bigint] NULL,
	[x2d_12_min] [int] NULL,
	[x2d_12_max] [bigint] NULL,
	[x2d_12_acc] [bigint] NULL,
	[x2d_12_avg] [decimal] NULL,
	[x2d_12_num] [bigint] NULL,
	[x2d_13_min] [int] NULL,
	[x2d_13_max] [bigint] NULL,
	[x2d_13_acc] [bigint] NULL,
	[x2d_13_avg] [decimal] NULL,
	[x2d_13_num] [bigint] NULL,
	[x2d_14_min] [int] NULL,
	[x2d_14_max] [bigint] NULL,
	[x2d_14_acc] [bigint] NULL,
	[x2d_14_avg] [decimal] NULL,
	[x2d_14_num] [bigint] NULL,
	[x2d_15_min] [int] NULL,
	[x2d_15_max] [bigint] NULL,
	[x2d_15_acc] [bigint] NULL,
	[x2d_15_avg] [decimal] NULL,
	[x2d_15_num] [bigint] NULL,
	[x2d_16_min] [int] NULL,
	[x2d_16_max] [bigint] NULL,
	[x2d_16_acc] [bigint] NULL,
	[x2d_16_avg] [decimal] NULL,
	[x2d_16_num] [bigint] NULL,
	[x2d_17_min] [int] NULL,
	[x2d_17_max] [bigint] NULL,
	[x2d_17_acc] [bigint] NULL,
	[x2d_17_avg] [decimal] NULL,
	[x2d_17_num] [bigint] NULL,
	[x2d_18_min] [int] NULL,
	[x2d_18_max] [bigint] NULL,
	[x2d_18_acc] [bigint] NULL,
	[x2d_18_avg] [decimal] NULL,
	[x2d_18_num] [bigint] NULL,
	[x2d_19_min] [int] NULL,
	[x2d_19_max] [bigint] NULL,
	[x2d_19_acc] [bigint] NULL,
	[x2d_19_avg] [decimal] NULL,
	[x2d_19_num] [bigint] NULL,
	[x2d_20_min] [int] NULL,
	[x2d_20_max] [bigint] NULL,
	[x2d_20_acc] [bigint] NULL,
	[x2d_20_avg] [decimal] NULL,
	[x2d_20_num] [bigint] NULL,
	[x2d_21_min] [int] NULL,
	[x2d_21_max] [bigint] NULL,
	[x2d_21_acc] [bigint] NULL,
	[x2d_21_avg] [decimal] NULL,
	[x2d_21_num] [bigint] NULL,
	[x2d_22_min] [int] NULL,
	[x2d_22_max] [bigint] NULL,
	[x2d_22_acc] [bigint] NULL,
	[x2d_22_avg] [decimal] NULL,
	[x2d_22_num] [bigint] NULL,
	[x2d_23_min] [int] NULL,
	[x2d_23_max] [bigint] NULL,
	[x2d_23_acc] [bigint] NULL,
	[x2d_23_avg] [decimal] NULL,
	[x2d_23_num] [bigint] NULL,
	[x2d_dd_min] [int] NULL,
	[x2d_dd_max] [bigint] NULL,
	[x2d_dd_acc] [bigint] NULL,
	[x2d_dd_avg] [decimal] NULL,
	[x2d_dd_num] [bigint] NULL,
 CONSTRAINT [PK_Y2D_SMH] PRIMARY KEY CLUSTERED 
(
	[x2d_date] ASC,
  [x2d_weekday] ASC,
	[x2d_id] ASC,
  [x2d_meter_id] ASC,
  [x2d_meter_item] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[H_L2D_SMH](
	[x2d_date] [int] NOT NULL, -- 20150523
	[x2d_weekday] [tinyint] NOT NULL,	-- day week (1=Domingo, 2=Lunes, 3=Martes, 4=Miercoles, ...)
	[x2d_id] [int] NOT NULL,	--SITE id
	[x2d_meter_id] [int] NOT NULL, -- Meter id (FK - H_METERS_DEFINITION) 
  [x2d_meter_item] [int] NOT NULL,		
	[x2d_00_min] [int] NULL,
	[x2d_00_max] [bigint] NULL,
	[x2d_00_acc] [bigint] NULL,
	[x2d_00_avg] [decimal] NULL,
	[x2d_00_num] [bigint] NULL,
	[x2d_01_min] [int] NULL,
	[x2d_01_max] [bigint] NULL,
	[x2d_01_acc] [bigint] NULL,
	[x2d_01_avg] [decimal] NULL,
	[x2d_01_num] [bigint] NULL,
	[x2d_02_min] [int] NULL,
	[x2d_02_max] [bigint] NULL,
	[x2d_02_acc] [bigint] NULL,
	[x2d_02_avg] [decimal] NULL,
	[x2d_02_num] [bigint] NULL,
	[x2d_03_min] [int] NULL,
	[x2d_03_max] [bigint] NULL,
	[x2d_03_acc] [bigint] NULL,
	[x2d_03_avg] [decimal] NULL,
	[x2d_03_num] [bigint] NULL,
	[x2d_04_min] [int] NULL,
	[x2d_04_max] [bigint] NULL,
	[x2d_04_acc] [bigint] NULL,
	[x2d_04_avg] [decimal] NULL,
	[x2d_04_num] [bigint] NULL,
	[x2d_05_min] [int] NULL,
	[x2d_05_max] [bigint] NULL,
	[x2d_05_acc] [bigint] NULL,
	[x2d_05_avg] [decimal] NULL,
	[x2d_05_num] [bigint] NULL,
	[x2d_06_min] [int] NULL,
	[x2d_06_max] [bigint] NULL,
	[x2d_06_acc] [bigint] NULL,
	[x2d_06_avg] [decimal] NULL,
	[x2d_06_num] [bigint] NULL,
	[x2d_07_min] [int] NULL,
	[x2d_07_max] [bigint] NULL,
	[x2d_07_acc] [bigint] NULL,
	[x2d_07_avg] [decimal] NULL,
	[x2d_07_num] [bigint] NULL,
	[x2d_08_min] [int] NULL,
	[x2d_08_max] [bigint] NULL,
	[x2d_08_acc] [bigint] NULL,
	[x2d_08_avg] [decimal] NULL,
	[x2d_08_num] [bigint] NULL,
	[x2d_09_min] [int] NULL,
	[x2d_09_max] [bigint] NULL,
	[x2d_09_acc] [bigint] NULL,
	[x2d_09_avg] [decimal] NULL,
	[x2d_09_num] [bigint] NULL,
	[x2d_10_min] [int] NULL,
	[x2d_10_max] [bigint] NULL,
	[x2d_10_acc] [bigint] NULL,
	[x2d_10_avg] [decimal] NULL,
	[x2d_10_num] [bigint] NULL,
	[x2d_11_min] [int] NULL,
	[x2d_11_max] [bigint] NULL,
	[x2d_11_acc] [bigint] NULL,
	[x2d_11_avg] [decimal] NULL,
	[x2d_11_num] [bigint] NULL,
	[x2d_12_min] [int] NULL,
	[x2d_12_max] [bigint] NULL,
	[x2d_12_acc] [bigint] NULL,
	[x2d_12_avg] [decimal] NULL,
	[x2d_12_num] [bigint] NULL,
	[x2d_13_min] [int] NULL,
	[x2d_13_max] [bigint] NULL,
	[x2d_13_acc] [bigint] NULL,
	[x2d_13_avg] [decimal] NULL,
	[x2d_13_num] [bigint] NULL,
	[x2d_14_min] [int] NULL,
	[x2d_14_max] [bigint] NULL,
	[x2d_14_acc] [bigint] NULL,
	[x2d_14_avg] [decimal] NULL,
	[x2d_14_num] [bigint] NULL,
	[x2d_15_min] [int] NULL,
	[x2d_15_max] [bigint] NULL,
	[x2d_15_acc] [bigint] NULL,
	[x2d_15_avg] [decimal] NULL,
	[x2d_15_num] [bigint] NULL,
	[x2d_16_min] [int] NULL,
	[x2d_16_max] [bigint] NULL,
	[x2d_16_acc] [bigint] NULL,
	[x2d_16_avg] [decimal] NULL,
	[x2d_16_num] [bigint] NULL,
	[x2d_17_min] [int] NULL,
	[x2d_17_max] [bigint] NULL,
	[x2d_17_acc] [bigint] NULL,
	[x2d_17_avg] [decimal] NULL,
	[x2d_17_num] [bigint] NULL,
	[x2d_18_min] [int] NULL,
	[x2d_18_max] [bigint] NULL,
	[x2d_18_acc] [bigint] NULL,
	[x2d_18_avg] [decimal] NULL,
	[x2d_18_num] [bigint] NULL,
	[x2d_19_min] [int] NULL,
	[x2d_19_max] [bigint] NULL,
	[x2d_19_acc] [bigint] NULL,
	[x2d_19_avg] [decimal] NULL,
	[x2d_19_num] [bigint] NULL,
	[x2d_20_min] [int] NULL,
	[x2d_20_max] [bigint] NULL,
	[x2d_20_acc] [bigint] NULL,
	[x2d_20_avg] [decimal] NULL,
	[x2d_20_num] [bigint] NULL,
	[x2d_21_min] [int] NULL,
	[x2d_21_max] [bigint] NULL,
	[x2d_21_acc] [bigint] NULL,
	[x2d_21_avg] [decimal] NULL,
	[x2d_21_num] [bigint] NULL,
	[x2d_22_min] [int] NULL,
	[x2d_22_max] [bigint] NULL,
	[x2d_22_acc] [bigint] NULL,
	[x2d_22_avg] [decimal] NULL,
	[x2d_22_num] [bigint] NULL,
	[x2d_23_min] [int] NULL,
	[x2d_23_max] [bigint] NULL,
	[x2d_23_acc] [bigint] NULL,
	[x2d_23_avg] [decimal] NULL,
	[x2d_23_num] [bigint] NULL,
	[x2d_dd_min] [int] NULL,
	[x2d_dd_max] [bigint] NULL,
	[x2d_dd_acc] [bigint] NULL,
	[x2d_dd_avg] [decimal] NULL,
	[x2d_dd_num] [bigint] NULL,
 CONSTRAINT [PK_L2D_SMH] PRIMARY KEY CLUSTERED 
(
	[x2d_date] ASC,
  [x2d_weekday] ASC,
	[x2d_id] ASC,
  [x2d_meter_id] ASC,
  [x2d_meter_item] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/***************** STOREDS ********************************/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_Get_Range_Date]') AND type in (N'F', N'FN'))
DROP PROCEDURE [dbo].[SP_Get_Range_Date]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_Update_Meter_Data]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_Update_Meter_Data]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_Get_Meters_List]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_Get_Meters_List]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_Get_Meters]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_Get_Meters]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_Set_Meter_Data]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_Set_Meter_Data]
GO

CREATE PROCEDURE [dbo].[SP_Get_Meters_List] 
 ( @Device AS INT ) 
AS
BEGIN
  SELECT   hmd_meter_id
         , hmd_meter_item
         , hmd_meter_type
         , hmd_description
         , hmd_meter_last_update
    FROM   H_METERS_DEFINITION
   WHERE   hmd_visible = 1
     AND   hmd_meter_device = @Device
END
GO

CREATE PROCEDURE [dbo].[SP_Get_Meters] 
(  @Device AS INT
 , @Source AS VARCHAR(1)
 , @Date AS DATETIME
) 
AS
BEGIN

DECLARE @DATEFROM AS DATETIME
DECLARE @DATEFROM_WEEK AS DATETIME
DECLARE @DATEFROM_MONTH AS DATETIME
DECLARE @DATEFROM_YEAR AS DATETIME
DECLARE @SEARCH_DATE AS NVARCHAR(MAX)
DECLARE @QUERY_CONST AS NVARCHAR(MAX)
DECLARE @QUERY_TEMP AS NVARCHAR(MAX)
DECLARE @FINAL_QUERY AS NVARCHAR(MAX)
DECLARE @QUERY AS NVARCHAR(MAX)
DECLARE @RANGE AS VARCHAR(1)
DECLARE @DESCRIPTION AS VARCHAR(3)
DECLARE @HOUR AS VARCHAR(2)
DECLARE @ORIGIN AS VARCHAR(MAX)
DECLARE @DOW AS VARCHAR(1)

SET @FINAL_QUERY = ''

SET @DATEFROM = @Date --GETDATE()

EXEC SP_Get_Meters_List @DEVICE

SET @DATEFROM_WEEK = DATEADD(WK, DATEDIFF(WK, 0, @DATEFROM) - 1, 0)             -- First day of previous week
SET @DATEFROM_MONTH = DATEADD(M, -1, DATEADD(M, DATEDIFF(M, 0, @DATEFROM), 0))  -- First day of previous month
SET @DATEFROM_YEAR = DATEADD(YY, -1, DATEADD(YY, DATEDIFF(YY, 0, @DATEFROM), 0)) -- First day of previous year
SET @DOW = LTRIM(DATEPART(WEEKDAY, @DATEFROM))

SET @HOUR = DATEPART(HOUR, GETDATE())
SET @HOUR = REPLICATE('0',2-LEN(RTRIM(@HOUR))) + RTRIM(@HOUR) 

SET @QUERY_CONST = ' SELECT ''@DESCRIPTION'' AS RANGE, @HOUR as HOUR, x2d_date, x2d_weekday, x2d_id, x2d_meter_id, x2d_meter_item,'
                 + ' x2d_@HOUR_min AS MIN, x2d_@HOUR_max as MAX, x2d_@HOUR_acc AS ACC, x2d_@HOUR_avg AS AVG, x2d_@HOUR_num AS NUM,'
                 + ' x2d_dd_min AS DAY_MIN, x2d_dd_max AS DAY_MAX, x2d_dd_acc AS DAY_ACC, x2d_dd_avg AS DAY_AVG, x2d_dd_num AS DAY_NUM'
                 + ' FROM  H_@RANGE2D_@SOURCEMH'
                 + ' INNER JOIN H_METERS_DEFINITION ON x2d_meter_id = hmd_meter_id AND x2d_meter_item = hmd_meter_item'
                 + ' WHERE hmd_visible = 1 AND (@DATE)'

-- TDA & SLW
SET @QUERY_TEMP = @QUERY_CONST
SET @RANGE = 'T'
SET @DESCRIPTION = 'TDA'
SET @SEARCH_DATE = 'x2d_date >= ' + CONVERT(VARCHAR(10), DATEADD(DAY, -7, @DATEFROM), 112) + ' AND x2d_date <= ' + CONVERT(VARCHAR(10), @DATEFROM, 112)
SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DATE', @SEARCH_DATE)
SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@HOUR', LTRIM(@HOUR))
SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@RANGE', LTRIM(@RANGE))
SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DESCRIPTION', LTRIM(@DESCRIPTION))
SET @FINAL_QUERY = @QUERY_TEMP 

-- WTD
SET @QUERY_TEMP = @QUERY_CONST
SET @RANGE = 'W'
SET @DESCRIPTION = 'WTD'
SET @SEARCH_DATE = 'x2d_date = ' + CAST(CONVERT(VARCHAR(10), @DATEFROM_WEEK, 112) AS VARCHAR)
SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DATE', LTRIM(@SEARCH_DATE))
SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@HOUR', LTRIM(@HOUR))
SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@RANGE', LTRIM(@RANGE))
SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DESCRIPTION', LTRIM(@DESCRIPTION))
--EXECUTE sp_executesql @QUERY
SET @FINAL_QUERY = @FINAL_QUERY
                 + ' UNION '
                 + @QUERY_TEMP 
                 + ' AND x2d_weekday = ' + @DOW

-- MTD
SET @QUERY_TEMP = @QUERY_CONST
SET @RANGE = 'M'
SET @DESCRIPTION = 'MTD'
SET @SEARCH_DATE = 'x2d_date = ' + CAST(CONVERT(VARCHAR(10), @DATEFROM_MONTH, 112) AS VARCHAR)
SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DATE', LTRIM(@SEARCH_DATE))
SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@HOUR', LTRIM(@HOUR))
SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@RANGE', LTRIM(@RANGE))
SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DESCRIPTION', LTRIM(@DESCRIPTION))
SET @FINAL_QUERY = @FINAL_QUERY
                 + ' UNION '
                 + @QUERY_TEMP 
                 + ' AND x2d_weekday = ' + @DOW

-- YTD
SET @QUERY_TEMP = @QUERY_CONST
SET @RANGE = 'Y'
SET @DESCRIPTION = 'YTD'
SET @SEARCH_DATE = 'x2d_date = ' + CAST(CONVERT(VARCHAR(10), @DATEFROM_YEAR, 112) AS VARCHAR)
SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DATE', LTRIM(@SEARCH_DATE))
SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@HOUR', LTRIM(@HOUR))
SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@RANGE', LTRIM(@RANGE))
SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DESCRIPTION', LTRIM(@DESCRIPTION))
SET @FINAL_QUERY = @FINAL_QUERY
                 + ' UNION '
                 + @QUERY_TEMP 
                 + ' AND x2d_weekday = ' + @DOW

SET @QUERY = REPLACE(@FINAL_QUERY, '@SOURCE', @Source)

PRINT @QUERY
           
EXECUTE sp_executesql @QUERY

--SELECT @QUERY           

END
GO

CREATE PROCEDURE [dbo].[SP_Set_Meter_Data]
(
    @pTableId   CHAR(1)		  -- L (Life), Y (Year), M (Month), W (Week), T (Today)
  , @pEntityId  CHAR(1)		  -- T (Terminales), S (Site)
  , @pDate      DATETIME		
  , @pId        INT         -- Terminal ID or Site Id
  , @pMeterId   INT			    -- 1=CoinIn, 2= NetWin, 3=BetAvg, 4=Gender...
  , @pMeterItem INT         -- 0=Simple meter data, 1=Gender-Male, 2=Gender-female...
  , @pMin       BIGINT
  , @pMax       BIGINT
  , @pAcc       BIGINT
  , @pAvg       BIGINT
  , @pNum       BIGINT
)
AS
BEGIN
          
  DECLARE @_date AS DATETIME          
  DECLARE @_date_val AS INT
  DECLARE @_wk AS INT
  DECLARE @_h AS VARCHAR(2)
          
  DECLARE @_get_sql NVARCHAR(MAX)    
  DECLARE @_insert_sql NVARCHAR(MAX)
  DECLARE @_update_sql NVARCHAR(MAX)

  DECLARE @pCurrentMin INT
  DECLARE @pCurrentMax BIGINT
  DECLARE @pCurrentAcc BIGINT
  DECLARE @pCurrentAvg DECIMAL
  DECLARE @pCurrentNum BIGINT

  SET @_date = dbo.SP_Get_Range_Date(@pDate, @pTableId)

  SET @_date_val = CAST(CONVERT(VARCHAR(8), @_date, 112) AS INT)
  SET @_wk = DATEPART(WEEKDAY, @pDate)
  SET @_h = DATEPART(HOUR, @pDate)
  SET @_h = REPLICATE('0',2-LEN(RTRIM(@_h))) + RTRIM(@_h) 

  SET @pCurrentMin = 0
  SET @pCurrentMax = 0
  SET @pCurrentAcc = 0
  SET @pCurrentAvg = 0
  SET @pCurrentNum = 0

  ------------------------------------------------------------------------------------------------
  -- Obtain current meter data

  SET @_get_sql = ' SELECT   @pCurrentMin = ISNULL(x2d_' + CAST(@_h AS VARCHAR) + '_min, 0)
                           , @pCurrentMax = ISNULL(x2d_' + CAST(@_h AS VARCHAR) + '_max, 0)
                           , @pCurrentAcc = ISNULL(x2d_' + CAST(@_h AS VARCHAR) + '_acc, 0)
                           , @pCurrentAvg = ISNULL(x2d_' + CAST(@_h AS VARCHAR) + '_avg, 0)
                           , @pCurrentNum = ISNULL(x2d_' + CAST(@_h AS VARCHAR) + '_num, 0)
                      FROM   H_' + @pTableId + '2D_' + @pEntityId + 'MH
                     WHERE   x2d_date       = ' + CAST(@_date_val AS VARCHAR) + '
                       AND   x2d_id         = ' + CAST(@pId AS VARCHAR) + '
                       AND   x2d_meter_id   = ' + CAST(@pMeterId AS VARCHAR) + '
                       AND   x2d_meter_item = ' + CAST(@pMeterItem AS VARCHAR) + '
                       AND   x2d_weekday    = ' + CAST(@_wk AS VARCHAR)

  EXEC sp_executesql @_get_sql,  N'@pCurrentMin INT OUTPUT, 
                                   @pCurrentMax INT OUTPUT, 
                                   @pCurrentAcc BIGINT OUTPUT, 
                                   @pCurrentAvg FLOAT OUTPUT, 
                                   @pCurrentNum BIGINT OUTPUT',
                                   @pCurrentMin OUTPUT,
                                   @pCurrentMax OUTPUT,
                                   @pCurrentAcc OUTPUT,
                                   @pCurrentAvg OUTPUT,
                                   @pCurrentNum OUTPUT

  ------------------------------------------------------------------------------------------------
  -- Check if meter data does not exist
  IF @@ROWCOUNT = 0 
   BEGIN

    -- Create Meter Data
    SET @_insert_sql = 'INSERT INTO H_' + @pTableId + '2D_' + @pEntityId + 'MH
		                         (x2d_date
		                         ,x2d_weekday
		                         ,x2d_id
		                         ,x2d_meter_id
		                         ,x2d_meter_item
		                       )
	                       VALUES
		                         (' + CAST(@_date_val AS VARCHAR) + '
		                         ,' + CAST(@_wk AS VARCHAR) + '
		                         ,' + CAST(@pId AS VARCHAR) + '
		                         ,' + CAST(@pMeterId AS VARCHAR) + '							  
		                         ,' + CAST(@pMeterItem AS VARCHAR) + '							  
		                        )'
    
    EXEC sys.sp_executesql @_insert_sql
    
   END

  ------------------------------------------------------------------------------------------------
  -- Set values data for update

  SET @pCurrentNum = @pNum
  SET @pCurrentMin = @pMin
  SET @pCurrentMax = @pMax
  SET @pCurrentAcc = @pAcc
  SET @pCurrentAvg = @pAvg

  ------------------------------------------------------------------------------------------------
  -- Update Meter data
  SET @_update_sql = 'UPDATE   H_' + @pTableId + '2D_' + @pEntityId + 'MH
						             SET   x2d_' + CAST(@_h AS VARCHAR) + '_min = ' + CAST(@pCurrentMin AS VARCHAR) + '
							               , x2d_' + CAST(@_h AS VARCHAR) + '_max = ' + CAST(@pCurrentMax AS VARCHAR) + '
							               , x2d_' + CAST(@_h AS VARCHAR) + '_acc = ' + CAST(@pCurrentAcc AS VARCHAR) + '
							               , x2d_' + CAST(@_h AS VARCHAR) + '_avg = ' + CAST(@pCurrentAvg AS VARCHAR) + '
							               , x2d_' + CAST(@_h AS VARCHAR) + '_num = ' + CAST(@pCurrentNum AS VARCHAR) + '							  
						           WHERE   x2d_date       = ' + CAST(@_date_val AS VARCHAR) + ' 
						             AND   x2d_id         = ' + CAST(@pId AS VARCHAR) + ' 
						             AND   x2d_meter_id   = ' + CAST(@pMeterId AS VARCHAR) + '
						             AND   x2d_meter_item = ' + CAST(@pMeteritem AS VARCHAR) + '
                         AND   x2d_weekday    = ' + CAST(@_wk AS VARCHAR)
                         
  EXEC sys.sp_executesql @_update_sql

END
GO

CREATE PROCEDURE SP_Update_Meter_Data
(
    @pTableId   CHAR(1)		  -- L (Life), Y (Year), M (Month), W (Week), T (Today)
  , @pEntityId  CHAR(1)		  -- T (Terminales), S (Site)
  , @pDate      DATETIME		
  , @pId        INT         -- Terminal ID or Site Id
  , @pMeterId   INT			    -- 1=CoinIn, 2= NetWin, 3=BetAvg, 4=Gender...
  , @pMeterItem INT         -- 0=Simple meter data, 1=Gender-Male, 2=Gender-female...
  , @pValue     DECIMAL      -- Value to aggregate
)
AS
BEGIN
          
  DECLARE @_date AS DATETIME          
  DECLARE @_date_val AS INT
  DECLARE @_wk AS INT
  DECLARE @_h AS VARCHAR(2)
          
  DECLARE @_get_sql NVARCHAR(MAX)    
  DECLARE @_insert_sql NVARCHAR(MAX)
  DECLARE @_update_sql NVARCHAR(MAX)

  DECLARE @pCurrentMin INT
  DECLARE @pCurrentMax BIGINT
  DECLARE @pCurrentAcc BIGINT
  DECLARE @pCurrentAvg DECIMAL
  DECLARE @pCurrentNum BIGINT

  SET @_date = dbo.SP_Get_Range_Date(@pDate, @pTableId, DEFAULT)

  SET @_date_val = CAST(CONVERT(VARCHAR(8), @_date, 112) AS INT)
  SET @_wk = DATEPART(WEEKDAY, @pDate)
  SET @_h = DATEPART(HOUR, @pDate)
  SET @_h = REPLICATE('0',2-LEN(RTRIM(@_h))) + RTRIM(@_h) 

  SET @pCurrentMin = 0
  SET @pCurrentMax = 0
  SET @pCurrentAcc = 0
  SET @pCurrentAvg = 0
  SET @pCurrentNum = 0

  ------------------------------------------------------------------------------------------------
  -- Obtain current meter data

  SET @_get_sql = ' SELECT   @pCurrentMin = ISNULL(x2d_' + CAST(@_h AS VARCHAR) + '_min, 0)
                           , @pCurrentMax = ISNULL(x2d_' + CAST(@_h AS VARCHAR) + '_max, 0)
                           , @pCurrentAcc = ISNULL(x2d_' + CAST(@_h AS VARCHAR) + '_acc, 0)
                           , @pCurrentAvg = ISNULL(x2d_' + CAST(@_h AS VARCHAR) + '_avg, 0)
                           , @pCurrentNum = ISNULL(x2d_' + CAST(@_h AS VARCHAR) + '_num, 0)
                      FROM   H_' + @pTableId + '2D_' + @pEntityId + 'MH
                     WHERE   x2d_date       = ' + CAST(@_date_val AS VARCHAR) + '
                       AND   x2d_id         = ' + CAST(@pId AS VARCHAR) + '
                       AND   x2d_meter_id   = ' + CAST(@pMeterId AS VARCHAR) + '
                       AND   x2d_meter_item = ' + CAST(@pMeterItem AS VARCHAR) + '
                       AND   x2d_weekday    = ' + CAST(@_wk AS VARCHAR)

  EXEC sp_executesql @_get_sql,  N'@pCurrentMin INT OUTPUT, 
                                   @pCurrentMax INT OUTPUT, 
                                   @pCurrentAcc BIGINT OUTPUT, 
                                   @pCurrentAvg FLOAT OUTPUT, 
                                   @pCurrentNum BIGINT OUTPUT',
                                   @pCurrentMin OUTPUT,
                                   @pCurrentMax OUTPUT,
                                   @pCurrentAcc OUTPUT,
                                   @pCurrentAvg OUTPUT,
                                   @pCurrentNum OUTPUT

  ------------------------------------------------------------------------------------------------
  -- Check if meter data does not exist
  IF @@ROWCOUNT = 0 
   BEGIN

    -- Create Meter Data
    SET @_insert_sql = 'INSERT INTO H_' + @pTableId + '2D_' + @pEntityId + 'MH
		                         (x2d_date
		                         ,x2d_weekday
		                         ,x2d_id
		                         ,x2d_meter_id
		                         ,x2d_meter_item
		                       )
	                       VALUES
		                         (' + CAST(@_date_val AS VARCHAR) + '
		                         ,' + CAST(@_wk AS VARCHAR) + '
		                         ,' + CAST(@pId AS VARCHAR) + '
		                         ,' + CAST(@pMeterId AS VARCHAR) + '							  
		                         ,' + CAST(@pMeterItem AS VARCHAR) + '							  
		                        )'
    
    EXEC sys.sp_executesql @_insert_sql
    
   END

  ------------------------------------------------------------------------------------------------
  -- Prepare data for update

  -- Increment number
  SET @pCurrentNum = @pCurrentNum + 1

  -- Detect Min value
  IF @pCurrentMin > @pValue
   BEGIN
    SET @pCurrentMin = @pValue
   END

  -- Detect Max value
  IF @pCurrentMax < @pValue
   BEGIN
    SET @pCurrentMax = @pValue
   END

  -- Accumulated calculation
  SET @pCurrentAcc = @pCurrentAcc + @pValue

  -- Average calculation
  SET @pCurrentAvg = @pCurrentAcc / CONVERT(DECIMAL(38,4), @pCurrentNum)

  ------------------------------------------------------------------------------------------------
  -- Update Meter data
  SET @_update_sql = 'UPDATE   H_' + @pTableId + '2D_' + @pEntityId + 'MH
						             SET   x2d_' + CAST(@_h AS VARCHAR) + '_min = ' + CAST(@pCurrentMin AS VARCHAR) + '
							               , x2d_' + CAST(@_h AS VARCHAR) + '_max = ' + CAST(@pCurrentMax AS VARCHAR) + '
							               , x2d_' + CAST(@_h AS VARCHAR) + '_acc = ' + CAST(@pCurrentAcc AS VARCHAR) + '
							               , x2d_' + CAST(@_h AS VARCHAR) + '_avg = ' + CAST(@pCurrentAvg AS VARCHAR) + '
							               , x2d_' + CAST(@_h AS VARCHAR) + '_num = ' + CAST(@pCurrentNum AS VARCHAR) + '							  
						           WHERE   x2d_date       = ' + CAST(@_date_val AS VARCHAR) + ' 
						             AND   x2d_id         = ' + CAST(@pId AS VARCHAR) + ' 
						             AND   x2d_meter_id   = ' + CAST(@pMeterId AS VARCHAR) + '
						             AND   x2d_meter_item = ' + CAST(@pMeteritem AS VARCHAR) + '
                         AND   x2d_weekday    = ' + CAST(@_wk AS VARCHAR)
                         
  EXEC sys.sp_executesql @_update_sql

END
GO

CREATE FUNCTION [dbo].[SP_Get_Range_Date] 
(  @pDate AS DATETIME
 , @pRange AS VARCHAR(1)
 , @pGetTime AS BIT = 0
)
RETURNS DATETIME
AS
BEGIN

  DECLARE @_time AS VARCHAR(8)

  IF (@pRange = 'T')
    BEGIN
      RETURN @pDate
    END

  -- Keep time
  IF @pGetTime = 0
    BEGIN
      SET @_time = ''
    END
  ELSE
    BEGIN
      SET @_time = CONVERT(VARCHAR(8), @pDate, 108)
    END
  

  IF (@pRange = 'W')
    BEGIN
      RETURN LEFT(CONVERT(nvarchar, DATEADD(WK, DATEDIFF(WK, 0, @pDate), 0), 120), 11) + @_time
    END

  IF (@pRange = 'M')
    BEGIN
      RETURN LEFT(CONVERT(nvarchar, DATEADD(M, DATEDIFF(M, 0, @pDate), 0), 120), 11) + @_time
    END

  IF (@pRange = 'Y')
    BEGIN
      RETURN LEFT(CONVERT(nvarchar, DATEADD(YY, DATEDIFF(YY, 0, @pDate), 0), 120), 11) + @_time
    END

  --IF (@pRange = 'L')
  --  BEGIN
  --    RETURN ?
  --  END
    
  --SET @DATEFROM_WEEK = LEFT(CONVERT(nvarchar, @DATEFROM_WEEK, 120), 11) + convert(char(8), @_DATE, 108)  

  --SELECT 
  --   @pDate
  -- , DATEDIFF(WK, 0, @pDate)
  -- , DATEADD(WK, DATEDIFF(WK, 0, @pDate) - 1, 0)
  -- , CONVERT(VARCHAR(10), DATEADD(WK, DATEDIFF(WK, 0, @pDate) - 1, 0), 112)
 
 RETURN @pDate
 
END 
GO

