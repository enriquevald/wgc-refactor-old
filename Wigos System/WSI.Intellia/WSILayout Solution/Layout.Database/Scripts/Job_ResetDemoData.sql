USE [msdb]
GO

/****** Object:  Job [ResetDemoData]    Script Date: 11/05/2015 18:07:27 ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]]    Script Date: 11/05/2015 18:07:27 ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'ResetDemoData', 
            @enabled=1, 
            @notify_level_eventlog=0, 
            @notify_level_email=0, 
            @notify_level_netsend=0, 
            @notify_level_page=0, 
            @delete_level=0, 
            @description=N'Reset Demo Data', 
            @category_name=N'[Uncategorized (Local)]', 
            @owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Create legends and reset data]    Script Date: 11/05/2015 18:07:27 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Create legends and reset data', 
            @step_id=1, 
            @cmdexec_success_code=0, 
            @on_success_action=1, 
            @on_success_step_id=0, 
            @on_fail_action=2, 
            @on_fail_step_id=0, 
            @retry_attempts=0, 
            @retry_interval=0, 
            @os_run_priority=0, @subsystem=N'TSQL', 
            @command=N'USE [wgdb_000]

BEGIN TRANSACTION;

BEGIN TRY
      --Delete table
      DELETE FROM LAYOUT_RANGES_LEGENDS
  
      --Insert data
      SET IDENTITY_INSERT [dbo].[layout_ranges_legends] ON 
 
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (1, 1, N''Registered'', CAST(2.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N''>         '', N''#AA0000'', NULL, 0)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (2, 1, N''Anonymous'', CAST(2.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N''=         '', N''#FF0000'', NULL, 0)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (3, 1, N''Idle'', CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N''=         '', N''#1BA81D'', NULL, 0)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (4, 4, N''Anonymous'', CAST(-1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N''=         '', N''#D572E0'', NULL, 0)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (5, 4, N''Bronze'', CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N''=         '', N''#C47229'', NULL, 0)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (6, 4, N''Silver'', CAST(2.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N''=         '', N''#C0C4CF'', NULL, 0)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (7, 4, N''ld'', CAST(3.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N''=         '', N''#F2AF3A'', NULL, 0)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (8, 4, N''Platinum'', CAST(4.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N''=         '', N''#222222'', NULL, 0)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (9, 5, N''18 - 40'', CAST(18.00 AS Decimal(18, 2)), CAST(40.00 AS Decimal(18, 2)), N''Between   '', N''#FFF700'', NULL, 1)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (10, 5, N''41 - 60'', CAST(41.00 AS Decimal(18, 2)), CAST(60.00 AS Decimal(18, 2)), N''Between   '', N''#06D414'', NULL, 1)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (11, 5, N''61 - 80'', CAST(61.00 AS Decimal(18, 2)), CAST(80.00 AS Decimal(18, 2)), N''Between   '', N''#729CF7'', NULL, 1)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (12, 5, N''80+'', CAST(80.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N''>         '', N''#FF0000'', NULL, 1)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (13, 6, N''$0.1 - 1.5'', CAST(0.10 AS Decimal(18, 2)), CAST(1.50 AS Decimal(18, 2)), N''Between   '', N''#729CF7'', NULL, 1)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (14, 6, N''$1.5 - 3'', CAST(1.50 AS Decimal(18, 2)), CAST(3.00 AS Decimal(18, 2)), N''Between   '', N''#2150B5'', NULL, 1)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (15, 6, N''$3 - 4.5'', CAST(3.00 AS Decimal(18, 2)), CAST(4.50 AS Decimal(18, 2)), N''Between   '', N''#86D15A'', NULL, 1)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (16, 6, N''$4.5 - 6'', CAST(4.50 AS Decimal(18, 2)), CAST(6.00 AS Decimal(18, 2)), N''Between   '', N''#4E9624'', NULL, 1)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (17, 6, N''$6 - 7.5'', CAST(6.00 AS Decimal(18, 2)), CAST(7.50 AS Decimal(18, 2)), N''Between   '', N''#F0D55D'', NULL, 1)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (18, 6, N''$7.5 - 9'', CAST(7.50 AS Decimal(18, 2)), CAST(9.00 AS Decimal(18, 2)), N''Between   '', N''#D19E04'', NULL, 1)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (19, 6, N''$9+'', CAST(9.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N''>         '', N''#FF0000'', NULL, 1)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (20, 8, N''Male'', CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N''=         '', N''#1771BF'', NULL, 0)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (21, 8, N''Female'', CAST(2.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N''=         '', N''#EA91ED'', NULL, 0)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (22, 8, N''Unknown'', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N''=         '', N''#B3B3B3'', NULL, 0)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (23, 15, N''Loss'', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N''<         '', N''#FFA63E'', NULL, 1)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (24, 9, N''$1 - 1000'', CAST(1.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), N''Between   '', N''#729CF7'', NULL, 1)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (25, 9, N''$1000 - 2000'', CAST(1000.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), N''Between   '', N''#00F2FF'', NULL, 1)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (26, 9, N''$2000 - 3000'', CAST(2000.00 AS Decimal(18, 2)), CAST(3000.00 AS Decimal(18, 2)), N''Between   '', N''#86D15A'', NULL, 1)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (27, 9, N''$3000 - 4000'', CAST(3000.00 AS Decimal(18, 2)), CAST(4000.00 AS Decimal(18, 2)), N''Between   '', N''#4E9624'', NULL, 1)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (28, 9, N''$4000 - 5000'', CAST(4000.00 AS Decimal(18, 2)), CAST(5000.00 AS Decimal(18, 2)), N''Between   '', N''#FFFF00'', NULL, 1)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (29, 9, N''$5000 - 6000'', CAST(5000.00 AS Decimal(18, 2)), CAST(6000.00 AS Decimal(18, 2)), N''Between   '', N''#FFA200'', NULL, 1)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (30, 9, N''$6000+'', CAST(6000.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N''>         '', N''#FF0000'', NULL, 1)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (31, 9, N''Losses'', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N''<         '', N''#2150B5'', NULL, 1)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (32, 17, N''< $100'', CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N''<         '', N''#092E80'', NULL, 1)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (33, 17, N''$100 - $5000'', CAST(100.00 AS Decimal(18, 2)), CAST(5000.00 AS Decimal(18, 2)), N''Between   '', N''#2150B5'', NULL, 1)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (34, 17, N''$5000 - $10000'', CAST(5000.00 AS Decimal(18, 2)), CAST(10000.00 AS Decimal(18, 2)), N''Between   '', N''#729CF7'', NULL, 1)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (35, 17, N''$10000 - $25000'', CAST(10000.00 AS Decimal(18, 2)), CAST(25000.00 AS Decimal(18, 2)), N''Between   '', N''#86D15A'', NULL, 1)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (36, 17, N''$25000 - $50000'', CAST(25000.00 AS Decimal(18, 2)), CAST(50000.00 AS Decimal(18, 2)), N''Between   '', N''#F0D55D'', NULL, 1)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (37, 17, N''$50000 - $75000'', CAST(50000.00 AS Decimal(18, 2)), CAST(75000.00 AS Decimal(18, 2)), N''Between   '', N''#D19E04'', NULL, 1)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (38, 17, N''$75000 - $100000'', CAST(75000.00 AS Decimal(18, 2)), CAST(100000.00 AS Decimal(18, 2)), N''Between   '', N''#FFA200'', NULL, 1)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (39, 17, N''> $100000'', CAST(100000.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N''>         '', N''#FF0000'', NULL, 1)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (40, 23, N''Played'', CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N''          '', N''#FF0000'', NULL, 0)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (41, 23, N''Winned'', CAST(2.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N''          '', N''#AA0000'', NULL, 0)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (42, 23, N''Payout'', CAST(4.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N''          '', N''#D19E04'', NULL, 0)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (43, 23, N''Without plays'', CAST(8.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N''          '', N''#86D15A'', NULL, 0)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (44, 28, N''Call Attendant'', CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N''          '', N''#AA0000'', NULL, 0)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (45, 20, N''Bill Jam'', CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N''          '', N''#86D15A'', NULL, 0)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (46, 20, N''Error'', CAST(2.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N''          '', N''#F0D55D'', NULL, 0)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (47, 20, N''Counterfeits'', CAST(4.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N''          '', N''#D19E04'', NULL, 0)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (48, 57, N''Stacker almost full'', CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N''          '', N''#AA0000'', NULL, 0)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (49, 57, N''Stacker full'', CAST(2.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N''          '', N''#729CF7'', NULL, 0)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (50, 21, N''COM Error'', CAST(2.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N''          '', N''#2150B5'', NULL, 0)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (51, 21, N''Output Error'', CAST(8.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N''          '', N''#FFA200'', NULL, 0)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (52, 21, N''Ribbon Replacement'', CAST(32.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N''          '', N''#092E80'', NULL, 0)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (53, 21, N''Low Paper'', CAST(4.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N''          '', N''#F0D55D'', NULL, 0)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (54, 21, N''Carriage Jam'', CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N''          '', N''#AA0000'', NULL, 0)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (55, 21, N''Power Supply'', CAST(16.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N''          '', N''#FFFF00'', NULL, 0)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (56, 19, N''Slot door'', CAST(16.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N''          '', N''#FFFF00'', NULL, 0)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (57, 19, N''Drop door'', CAST(8.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N''          '', N''#AA0000'', NULL, 0)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (58, 19, N''Card cage door'', CAST(2.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N''          '', N''#F0D55D'', NULL, 0)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (59, 19, N''Belly door'', CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N''          '', N''#092E80'', NULL, 0)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (60, 19, N''Cashbox door'', CAST(4.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N''          '', N''#729CF7'', NULL, 0)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (61, 27, N''Error CMOS RAM'', CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N''          '', N''#729CF7'', NULL, 0)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (62, 27, N''Error EEPROM'', CAST(2.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N''          '', N''#092E80'', NULL, 0)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (63, 27, N''Low Battery'', CAST(512.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N''          '', N''#F0D55D'', NULL, 0)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (64, 29, N''Jackpot'', CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N''          '', N''#C0C4CF'', NULL, 0)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (65, 29, N''Cancelled Credits'', CAST(2.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N''          '', N''#D19E04'', NULL, 0)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (66, 32, N''Active'', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N''=         '', N''#1BA81D'', NULL, 0)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (67, 32, N''Out of Service'', CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N''=         '', N''#D572E0'', NULL, 0)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (68, 32, N''Retired'', CAST(2.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N''=         '', N''#C0C4CF'', NULL, 0)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (69, 47, N''VIP'', CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N''=         '', N''#EEFD1E'', NULL, 0)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (70, 48, N''Jackpot'', CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N''          '', N''#11EC20'', NULL, 0)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (71, 48, N''Cancelled Credits'', CAST(2.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N''          '', N''#D19E04'', NULL, 0)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (72, 49, N''Call Attendant'', CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N''          '', N''#AA0000'', NULL, 0)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (73, 50, N''High Roller'', CAST(4.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N''=         '', N''#D19E04'', NULL, 0)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (74, 54, N''$0.1 - 1.5'', CAST(0.10 AS Decimal(18, 2)), CAST(1.50 AS Decimal(18, 2)), N''Between   '', N''#729CF7'', NULL, 1)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (75, 54, N''$1.5 - 3'', CAST(1.50 AS Decimal(18, 2)), CAST(3.00 AS Decimal(18, 2)), N''Between   '', N''#2150B5'', NULL, 1)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (76, 54, N''$3 - 4.5'', CAST(3.00 AS Decimal(18, 2)), CAST(4.50 AS Decimal(18, 2)), N''Between   '', N''#86D15A'', NULL, 1)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (77, 54, N''$4.5 - 6'', CAST(4.50 AS Decimal(18, 2)), CAST(6.00 AS Decimal(18, 2)), N''Between   '', N''#4E9624'', NULL, 1)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (78, 54, N''$6 - 7.5'', CAST(6.00 AS Decimal(18, 2)), CAST(7.50 AS Decimal(18, 2)), N''Between   '', N''#F0D55D'', NULL, 1)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (79, 54, N''$7.5 - 9'', CAST(7.50 AS Decimal(18, 2)), CAST(9.00 AS Decimal(18, 2)), N''Between   '', N''#D19E04'', NULL, 1)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (80, 54, N''$9+'', CAST(9.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N''>         '', N''#FF0000'', NULL, 1)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (81, 55, N''$1 - 100'', CAST(0.10 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), N''Between   '', N''#729CF7'', NULL, 1)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (82, 55, N''$100 - 200'', CAST(100.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), N''Between   '', N''#00F2FF'', NULL, 1)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (83, 55, N''$200 - 300'', CAST(200.00 AS Decimal(18, 2)), CAST(300.00 AS Decimal(18, 2)), N''Between   '', N''#86D15A'', NULL, 1)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (84, 55, N''$300 - 400'', CAST(300.00 AS Decimal(18, 2)), CAST(400.00 AS Decimal(18, 2)), N''Between   '', N''#4E9624'', NULL, 1)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (85, 55, N''$400 - 500'', CAST(400.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), N''Between   '', N''#FFFF00'', NULL, 1)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (86, 55, N''$500 - 600'', CAST(500.00 AS Decimal(18, 2)), CAST(600.00 AS Decimal(18, 2)), N''Between   '', N''#FFA200'', NULL, 1)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (87, 55, N''$600+'', CAST(600.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N''>         '', N''#FF0000'', NULL, 1)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (88, 55, N''Losses'', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N''<         '', N''#2150B5'', NULL, 1)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (89, 56, N''< $100'', CAST(100.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N''<         '', N''#092E80'', NULL, 1)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (90, 56, N''$100 - $500'', CAST(100.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), N''Between   '', N''#2150B5'', NULL, 1)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (91, 56, N''$500 - $1500'', CAST(500.00 AS Decimal(18, 2)), CAST(1500.00 AS Decimal(18, 2)), N''Between   '', N''#729CF7'', NULL, 1)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (92, 56, N''$1500 - $7500'', CAST(1500.00 AS Decimal(18, 2)), CAST(7500.00 AS Decimal(18, 2)), N''Between   '', N''#86D15A'', NULL, 1)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (93, 56, N''$7500 - $25000'', CAST(7500.00 AS Decimal(18, 2)), CAST(25000.00 AS Decimal(18, 2)), N''Between   '', N''#F0D55D'', NULL, 1)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (94, 56, N''$25000 - $90000'', CAST(25000.00 AS Decimal(18, 2)), CAST(90000.00 AS Decimal(18, 2)), N''Between   '', N''#D19E04'', NULL, 1)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (95, 56, N''$90000 - $125000'', CAST(90000.00 AS Decimal(18, 2)), CAST(125000.00 AS Decimal(18, 2)), N''Between   '', N''#AA0000'', NULL, 1)
      INSERT [dbo].[layout_ranges_legends] ([lrl_id], [lrl_range_id], [lrl_label], [lrl_value1], [lrl_value2], [lrl_operator], [lrl_color], [lrl_field], [lrl_editable]) VALUES (96, 56, N''> $125000'', CAST(125000.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N''>         '', N''#FF0000'', NULL, 1)

      SET IDENTITY_INSERT [dbo].[layout_ranges_legends] OFF
      
      
      SELECT ''Update Legends'';
      
      UPDATE play_sessions_test
      SET
      [ps_initial_balance] =                          [ps_initial_balance] /100,   
      [ps_played_amount] =                            [ps_played_amount] /100,
      [ps_won_amount] =                               [ps_won_amount] /100,
      [ps_cash_in] =                                  [ps_cash_in] /100,
      [ps_cash_out] =                                 [ps_cash_out] /100,
      [ps_final_balance] =                            [ps_final_balance] /100,    
      [ps_total_played] =                             [ps_total_played] /100,
      [ps_total_won] =                                [ps_total_won] /100,
      [ps_redeemable_cash_in] =                       [ps_redeemable_cash_in] /100,
      [ps_redeemable_cash_out] =                      [ps_redeemable_cash_out] /100,
      [ps_redeemable_played] =                        [ps_redeemable_played] /100,
      [ps_redeemable_won] =                           [ps_redeemable_won] /100,
      [ps_non_redeemable_cash_in] =                   [ps_non_redeemable_cash_in] /100,
      [ps_non_redeemable_cash_out] =                  [ps_non_redeemable_cash_out] /100,
      [ps_non_redeemable_played] =                    [ps_non_redeemable_played] /100,
      [ps_non_redeemable_won] =                       [ps_non_redeemable_won] /100,     
      [ps_spent_used] =                               [ps_spent_used] /100,
      [ps_spent_remaining] =                          [ps_spent_remaining] /100,
      [ps_cancellable_amount] =                       [ps_cancellable_amount] /100,
      [ps_reported_balance_mismatch] =                [ps_reported_balance_mismatch] /100,
      [ps_re_cash_in] =                               [ps_re_cash_in] /100,
      [ps_promo_re_cash_in] =                         [ps_promo_re_cash_in] /100,
      [ps_re_cash_out] =                              [ps_re_cash_out] /100,
      [ps_promo_re_cash_out] =                        [ps_promo_re_cash_out] /100,
      [ps_computed_points] =                          [ps_computed_points] /100,
      [ps_awarded_points] =                           [ps_awarded_points] /100,
      [ps_re_ticket_in] =                             [ps_re_ticket_in] /100,
      [ps_promo_re_ticket_in] =                       [ps_promo_re_ticket_in] /100,
      [ps_promo_nr_ticket_in] =                       [ps_promo_nr_ticket_in] /100,
      [ps_re_ticket_out] =                            [ps_re_ticket_out] /100,
      [ps_promo_nr_ticket_out] =                      [ps_promo_nr_ticket_out] /100,
      [ps_bills_in_amount] =                          [ps_bills_in_amount] /100,
      [ps_total_cash_out] =                           [ps_total_cash_out] /100,
      [ps_redeemable_played_original] =               [ps_redeemable_played_original] /100,
      [ps_redeemable_won_original] =                  [ps_redeemable_won_original] /100,
      [ps_non_redeemable_played_original] =           [ps_non_redeemable_played_original] /100,
      [ps_non_redeemable_won_original] =              [ps_non_redeemable_won_original] /100,
      [ps_played_count_original] =                    [ps_played_count_original] /100,
      [ps_won_count_original] =                       [ps_won_count_original] /100,
      [ps_aux_ft_re_cash_in] =                        [ps_aux_ft_re_cash_in] /100,
      [ps_aux_ft_nr_cash_in] =                        [ps_aux_ft_nr_cash_in] /100,
      [ps_total_cash_in] =                            [ps_total_cash_in] /100

      SELECT ''Update play_sessions_test'';
        
        UPDATE sales_per_hour_test
            SET
              [sph_played_amount] =                     [sph_played_amount] /100,     
              [sph_won_amount] =                        [sph_won_amount] /100,     
              [sph_theoretical_won_amount] =            [sph_theoretical_won_amount] /100,
              [sph_jackpot_amount] =                    [sph_jackpot_amount] /100,
              [sph_progressive_jackpot_amount] =        [sph_progressive_jackpot_amount] /100,
              [sph_progressive_jackpot_amount_0] =      [sph_progressive_jackpot_amount_0] /100,
              [sph_progressive_provision_amount] =      [sph_progressive_provision_amount] /100
              
      SELECT ''Update sales_per_hour_test'';

      COMMIT TRANSACTION;
END TRY

BEGIN CATCH
      ROLLBACK TRANSACTION;
END CATCH

', 
            @database_name=N'wgdb_000', 
            @database_user_name=N'dbo', 
            @flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Every 3 hours', 
            @enabled=1, 
            @freq_type=4, 
            @freq_interval=1, 
            @freq_subday_type=8, 
            @freq_subday_interval=3, 
            @freq_relative_interval=0, 
            @freq_recurrence_factor=0, 
            @active_start_date=20151105, 
            @active_end_date=99991231, 
            @active_start_time=0, 
            @active_end_time=235959, 
            @schedule_uid=N'1e3a3569-26fe-49a7-a419-f74d97db12e1'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:

GO

