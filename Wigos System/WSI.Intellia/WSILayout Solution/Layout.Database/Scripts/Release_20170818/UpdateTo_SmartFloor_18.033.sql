USE WGDB_000;

SET ANSI_NULLS ON;

----------------------------------------------------
-----------------VERSION CONTROL--------------------
----------------------------------------------------

DECLARE 
@Exp_SmartFloor_ClientId int, 
@Exp_SmartFloor_CommonBuildId int,
@Exp_SmartFloor_ClientBuildId int,
@New_SmartFloor_ReleaseId int,
@New_SmartFloor_ScriptName nvarchar(50),
@New_SmartFloor_Description nvarchar(1000);

SET @Exp_SmartFloor_ClientId = 18;
SET @Exp_SmartFloor_CommonBuildId = 100;
SET @Exp_SmartFloor_ClientBuildId = 1;
SET @New_SmartFloor_ReleaseId = 33;
SET @New_SmartFloor_ScriptName = N'UpdateTo_SmartFloor_18.033.sql';
SET @New_SmartFloor_Description = N'Patch 18.033 - 32';

IF EXISTS ( SELECT 1 FROM DB_VERSION_SMARTFLOOR )
 DELETE FROM DB_VERSION_SMARTFLOOR;

INSERT INTO [dbo].[db_version_smartfloor]
           ([db_client_id]
           ,[db_common_build_id]
           ,[db_client_build_id]
           ,[db_release_id]
           ,[db_updated_script]
           ,[db_updated]
           ,[db_description])
     VALUES
           (@Exp_SmartFloor_ClientId
           ,@Exp_SmartFloor_CommonBuildId
           ,@Exp_SmartFloor_ClientBuildId
           ,@New_SmartFloor_ReleaseId
           ,@New_SmartFloor_ScriptName
           ,Getdate()
           ,@New_SmartFloor_Description);

GO

----------------------------------------------------
----------------------TABLES------------------------
----------------------------------------------------

GRANT EXECUTE ON [dbo].[CreateDefaultProfilesLayout] TO [wggui] WITH GRANT OPTION;

GO

----------------------------------------------------
----------------------DATA--------------------------
----------------------------------------------------

DELETE FROM [dbo].[layout_ranges_legends]
WHERE lrl_range_id=5 AND lrl_value1=0.00 AND lrl_value2=0.00 AND lrl_label = 'Unknown'

GO

SET IDENTITY_INSERT [dbo].[layout_ranges_legends] ON
INSERT INTO [dbo].[layout_ranges_legends]
           ([lrl_id]
	   ,[lrl_range_id]
           ,[lrl_label]
           ,[lrl_value1]
           ,[lrl_value2]
           ,[lrl_operator]
           ,[lrl_color]
           ,[lrl_field]
           ,[lrl_editable]
           ,[lrl_auto_assign_role]
           ,[lrl_auto_assign_priority]
           ,[lrl_external_nls])
     VALUES
            (7, 5, 'Unknown', 0, 0,'Unknown','#B3B3B3',null,0,-1,0, null)
     

SET IDENTITY_INSERT [dbo].[layout_ranges_legends] OFF

GO


UPDATE [dbo].[layout_ranges_legends] SET [lrl_label] = N'$1.51 - 3', [lrl_value1] = CAST(1.51 AS Decimal(18, 2)) WHERE [lrl_id] = 14
UPDATE [dbo].[layout_ranges_legends] SET [lrl_label] = N'$3.01 - 4.5', [lrl_value1] = CAST(3.01 AS Decimal(18, 2)) WHERE [lrl_id] = 15
UPDATE [dbo].[layout_ranges_legends] SET [lrl_label] = N'$4.51 - 6', [lrl_value1] = CAST(4.51 AS Decimal(18, 2)) WHERE [lrl_id] = 16
UPDATE [dbo].[layout_ranges_legends] SET [lrl_label] = N'$6.01 - 7.5', [lrl_value1] = CAST(6.01 AS Decimal(18, 2)) WHERE [lrl_id] = 17
UPDATE [dbo].[layout_ranges_legends] SET [lrl_label] = N'$7.51 - 9', [lrl_value1] = CAST(7.51 AS Decimal(18, 2)) WHERE [lrl_id] = 18

UPDATE [dbo].[layout_ranges_legends] SET [lrl_label] = N'$1000.01 - 2000', [lrl_value1] = CAST(1000.01 AS Decimal(18, 2)) WHERE [lrl_id] = 25
UPDATE [dbo].[layout_ranges_legends] SET [lrl_label] = N'$2000.01 - 3000', [lrl_value1] = CAST(2000.01 AS Decimal(18, 2)) WHERE [lrl_id] = 26
UPDATE [dbo].[layout_ranges_legends] SET [lrl_label] = N'$3000.01 - 4000', [lrl_value1] = CAST(3000.01 AS Decimal(18, 2)) WHERE [lrl_id] = 27
UPDATE [dbo].[layout_ranges_legends] SET [lrl_label] = N'$4000.01 - 5000', [lrl_value1] = CAST(4000.01 AS Decimal(18, 2)) WHERE [lrl_id] = 28
UPDATE [dbo].[layout_ranges_legends] SET [lrl_label] = N'$5000.01 - 6000', [lrl_value1] = CAST(5000.01 AS Decimal(18, 2)) WHERE [lrl_id] = 29

UPDATE [dbo].[layout_ranges_legends] SET [lrl_label] = N'$5000.01 - $10000'  , [lrl_value1] = CAST(5000.01 AS Decimal(18, 2)) WHERE [lrl_id] = 34
UPDATE [dbo].[layout_ranges_legends] SET [lrl_label] = N'$10000.01 - $25000' , [lrl_value1] = CAST(10000.01 AS Decimal(18, 2)) WHERE [lrl_id] = 35
UPDATE [dbo].[layout_ranges_legends] SET [lrl_label] = N'$25000.01 - $50000' , [lrl_value1] = CAST(25000.01 AS Decimal(18, 2)) WHERE [lrl_id] = 36
UPDATE [dbo].[layout_ranges_legends] SET [lrl_label] = N'$50000.01 - $75000' , [lrl_value1] = CAST(50000.01 AS Decimal(18, 2)) WHERE [lrl_id] = 37
UPDATE [dbo].[layout_ranges_legends] SET [lrl_label] = N'$75000.01 - $100000', [lrl_value1] = CAST(75000.01 AS Decimal(18, 2)) WHERE [lrl_id] = 38

UPDATE [dbo].[layout_ranges_legends] SET [lrl_label] = N'$1.51 - 3', [lrl_value1] =   CAST(1.51 AS Decimal(18, 2)) WHERE [lrl_id] = 75
UPDATE [dbo].[layout_ranges_legends] SET [lrl_label] = N'$3.01 - 4.5', [lrl_value1] = CAST(3.01 AS Decimal(18, 2)) WHERE [lrl_id] = 76
UPDATE [dbo].[layout_ranges_legends] SET [lrl_label] = N'$4.51 - 6', [lrl_value1] =   CAST(4.51 AS Decimal(18, 2)) WHERE [lrl_id] = 77
UPDATE [dbo].[layout_ranges_legends] SET [lrl_label] = N'$6.01 - 7.5', [lrl_value1] = CAST(6.01 AS Decimal(18, 2)) WHERE [lrl_id] = 78
UPDATE [dbo].[layout_ranges_legends] SET [lrl_label] = N'$7.51 - 9', [lrl_value1] =   CAST(7.51 AS Decimal(18, 2)) WHERE [lrl_id] = 79

UPDATE [dbo].[layout_ranges_legends] SET [lrl_label] = N'$100.01 - 200', [lrl_value1] = CAST(100.01 AS Decimal(18, 2)) WHERE [lrl_id] = 82
UPDATE [dbo].[layout_ranges_legends] SET [lrl_label] = N'$200.01 - 300', [lrl_value1] = CAST(200.01 AS Decimal(18, 2)) WHERE [lrl_id] = 83
UPDATE [dbo].[layout_ranges_legends] SET [lrl_label] = N'$300.01 - 400', [lrl_value1] = CAST(300.01 AS Decimal(18, 2)) WHERE [lrl_id] = 84
UPDATE [dbo].[layout_ranges_legends] SET [lrl_label] = N'$400.01 - 500', [lrl_value1] = CAST(400.01 AS Decimal(18, 2)) WHERE [lrl_id] = 85
UPDATE [dbo].[layout_ranges_legends] SET [lrl_label] = N'$500.01 - 600', [lrl_value1] = CAST(500.01 AS Decimal(18, 2)) WHERE [lrl_id] = 86

UPDATE [dbo].[layout_ranges_legends] SET [lrl_label] = N'$500.01 - $1500', [lrl_value1] = CAST(500.01 AS Decimal(18, 2)) WHERE [lrl_id] = 91
UPDATE [dbo].[layout_ranges_legends] SET [lrl_label] = N'$1500.01 - $7500', [lrl_value1] = CAST(1500.01 AS Decimal(18, 2)) WHERE [lrl_id] = 92
UPDATE [dbo].[layout_ranges_legends] SET [lrl_label] = N'$7500.01 - $25000', [lrl_value1] = CAST(7500.01 AS Decimal(18, 2)) WHERE [lrl_id] = 93
UPDATE [dbo].[layout_ranges_legends] SET [lrl_label] = N'$25000.01 - $90000', [lrl_value1] = CAST(25000.01 AS Decimal(18, 2)) WHERE [lrl_id] = 94
UPDATE [dbo].[layout_ranges_legends] SET [lrl_label] = N'$90000.01 - $125000', [lrl_value1] = CAST(90000.01 AS Decimal(18, 2)) WHERE [lrl_id] = 95


GO

-- Clean user dashboard layout last update to all users
UPDATE LAYOUT_USERS_CONFIGURATION SET LC_PROFILE_UPDATE = NULL






