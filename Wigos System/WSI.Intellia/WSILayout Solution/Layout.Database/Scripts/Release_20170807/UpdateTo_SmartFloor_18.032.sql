USE WGDB_000;

SET ANSI_NULLS ON;

----------------------------------------------------
-----------------VERSION CONTROL--------------------
----------------------------------------------------

DECLARE 
@Exp_SmartFloor_ClientId int, 
@Exp_SmartFloor_CommonBuildId int,
@Exp_SmartFloor_ClientBuildId int,
@New_SmartFloor_ReleaseId int,
@New_SmartFloor_ScriptName nvarchar(50),
@New_SmartFloor_Description nvarchar(1000);

SET @Exp_SmartFloor_ClientId = 18;
SET @Exp_SmartFloor_CommonBuildId = 100;
SET @Exp_SmartFloor_ClientBuildId = 1;
SET @New_SmartFloor_ReleaseId = 32;
SET @New_SmartFloor_ScriptName = N'UpdateTo_SmartFloor_18.031.sql';
SET @New_SmartFloor_Description = N'Patch 18.032 - 31';

IF EXISTS ( SELECT 1 FROM DB_VERSION_SMARTFLOOR )
 DELETE FROM DB_VERSION_SMARTFLOOR;

INSERT INTO [dbo].[db_version_smartfloor]
           ([db_client_id]
           ,[db_common_build_id]
           ,[db_client_build_id]
           ,[db_release_id]
           ,[db_updated_script]
           ,[db_updated]
           ,[db_description])
     VALUES
           (@Exp_SmartFloor_ClientId
           ,@Exp_SmartFloor_CommonBuildId
           ,@Exp_SmartFloor_ClientBuildId
           ,@New_SmartFloor_ReleaseId
           ,@New_SmartFloor_ScriptName
           ,Getdate()
           ,@New_SmartFloor_Description);

GO

----------------------------------------------------
----------------------STOREDS-----------------------
----------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetRanges]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Layout_GetRanges]
GO

CREATE PROCEDURE [dbo].[Layout_GetRanges]
      @pRole INT
    , @pUserId INT
AS
BEGIN

  SELECT LR_ID
     , LR_NAME
     , LR_FIELD
     , LR_ROLE
     , LR_SECTION_ID
     , LR_ICON
	 , LR_EXTERNAL_NLS
	 , LR_VISIBLE
    FROM LAYOUT_RANGES
    WHERE LR_ROLE & @pRole = @pRole 
  ORDER BY LR_NAME ASC
  
  SELECT LRL_RANGE_ID
       , LRL_LABEL
       , LRL_VALUE1
       , LRL_VALUE2
       , LRL_OPERATOR
       , LRL_COLOR
       , LRL_ID
       , LRL_EDITABLE
	   , LRL_EXTERNAL_NLS
	   , CASE LRL_OPERATOR
			WHEN '<' THEN 0
			WHEN 'Between' THEN 1
			WHEN '>' THEN 2
			WHEN '=' THEN 3
		END
		AS LRL_OPERATOR_CODE
    FROM LAYOUT_RANGES_LEGENDS
   WHERE LRL_RANGE_ID IN 
    (
      SELECT LR_ID
        FROM LAYOUT_RANGES
       WHERE LR_ROLE & @pRole = @pRole 
    )
  ORDER BY LRL_RANGE_ID, LRL_OPERATOR_CODE, LRL_VALUE1 

  SELECT LCF_NAME
       , LCF_FILTER_PARAMETERS
       , LCF_ID
       , LCF_DISPLAY
    FROM LAYOUT_USERS_CUSTOM_FILTERS
   WHERE LCF_USER_ID= @pUserId 
      
     SELECT LR_SECTION_ID AS [CATEGORY] --41 PLAYER / 42 MACHINE
       , CASE 
		   WHEN lr_id=59 THEN 'Custom (Others)' 
		   WHEN lr_id=61 THEN 'Custom (Players)' 
		   WHEN lr_id=62 THEN 'Custom (Machines)' 
		   ELSE LR_NAME + ' - ' + LRL_LABEL
         END  
	     AS [ALARM_DESC]
       , CAST((LR_FIELD * 1000) + LRL_VALUE1 AS INT) AS [SUBCATEGORY] 
       , R.LR_ICON AS ICON
       , L.LRL_AUTO_ASSIGN_PRIORITY
	   , LR_EXTERNAL_NLS
	   , LRL_EXTERNAL_NLS
    FROM LAYOUT_RANGES AS R
INNER JOIN LAYOUT_RANGES_LEGENDS  AS L
      ON R.LR_ID = L.LRL_RANGE_ID
   WHERE LR_SECTION_ID IN (41,42,43)
ORDER BY LR_SECTION_ID    
END 

