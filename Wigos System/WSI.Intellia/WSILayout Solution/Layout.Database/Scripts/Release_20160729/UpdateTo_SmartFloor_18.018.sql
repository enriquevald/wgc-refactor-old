USE WGDB_000;

SET ANSI_NULLS ON;

----------------------------------------------------
-----------------VERSION CONTROL--------------------
----------------------------------------------------

DECLARE 
@Exp_SmartFloor_ClientId int, 
@Exp_SmartFloor_CommonBuildId int,
@Exp_SmartFloor_ClientBuildId int,
@New_SmartFloor_ReleaseId int,
@New_SmartFloor_ScriptName nvarchar(50),
@New_SmartFloor_Description nvarchar(1000);

SET @Exp_SmartFloor_ClientId = 18;
SET @Exp_SmartFloor_CommonBuildId = 100;
SET @Exp_SmartFloor_ClientBuildId = 1;
SET @New_SmartFloor_ReleaseId = 18;
SET @New_SmartFloor_ScriptName = N'UpdateTo_SmartFloor_18.018.sql';
SET @New_SmartFloor_Description = N'Patch 18.018 - 17';

IF EXISTS ( SELECT 1 FROM DB_VERSION_SMARTFLOOR )
 DELETE FROM DB_VERSION_SMARTFLOOR;

INSERT INTO [dbo].[db_version_smartfloor]
           ([db_client_id]
           ,[db_common_build_id]
           ,[db_client_build_id]
           ,[db_release_id]
           ,[db_updated_script]
           ,[db_updated]
           ,[db_description])
     VALUES
           (@Exp_SmartFloor_ClientId
           ,@Exp_SmartFloor_CommonBuildId
           ,@Exp_SmartFloor_ClientBuildId
           ,@New_SmartFloor_ReleaseId
           ,@New_SmartFloor_ScriptName
           ,Getdate()
           ,@New_SmartFloor_Description);

           
----------------------------------------------------
----------------------STOREDS-----------------------
----------------------------------------------------
GO
	/****** Object:  StoredProcedure [dbo].[Layout_GetIsTaskAssignedToUser]    Script Date: 17/07/2016 7:48:36 ******/
	IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Layout_GetIsTaskAssignedToUser')
	BEGIN
		DROP PROCEDURE [dbo].[Layout_GetIsTaskAssignedToUser]
	END
	ELSE
	/****** Object:  StoredProcedure [dbo].[Layout_GetIsTaskAssignedToUser]    Script Date: 26/07/2016 7:48:36 ******/
	SET ANSI_NULLS ON
	GO
	SET QUOTED_IDENTIFIER ON
	GO
	CREATE PROCEDURE [dbo].[Layout_GetIsTaskAssignedToUser]
		@loginId bigint,
		@taskId bigint
	WITH EXEC AS CALLER
	AS
	BEGIN
		SELECT *
		FROM LAYOUT_SITE_TASKS
		WHERE LST_ID = @taskId
		AND  ((LST_ASSIGNED_USER_ID = @loginId)
				 OR  (LST_ACCEPTED_USER_ID = @loginId) 
				 OR  (((LST_ASSIGNED_ROLE_ID & (SELECT   SUM(DISTINCT(GPF_FORM_ID / 1000000)) AS ROLE
																					FROM   GUI_PROFILE_FORMS
																					WHERE   GPF_GUI_ID = 203 AND GPF_READ_PERM <> 0
																						AND   GPF_PROFILE_ID = (SELECT   GU_PROFILE_ID
																																		FROM   GUI_USERS
																																		WHERE   GU_USER_ID = @loginId) 
																AND(GPF_FORM_ID / 1000000) > 0)) > 0) 
												AND ( isnull(LST_ACCEPTED_USER_ID, 0) = 0)))
	END
GO

/****** Object:  StoredProcedure [dbo].[Layout_GetUsersIdByRoleId]    Script Date: 27/07/2016 14:58:36 ******/
IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Layout_GetUsersIdByRoleId')
BEGIN
	DROP PROCEDURE [dbo].[Layout_GetUsersIdByRoleId]
END
ELSE
/****** Object:  StoredProcedure [dbo].[Layout_GetUsersIdByRoleId]    Script Date: 27/07/2016 14:58:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Layout_GetUsersIdByRoleId]
      @pRoleId varchar(20)
AS
BEGIN

declare @Sql varchar(max)

select @Sql	=	'SELECT DISTINCT(GU_USER_ID)
							   FROM GUI_USERS
								WHERE GU_PROFILE_ID IN (
									SELECT	GPF_PROFILE_ID
									  FROM  GUI_PROFILE_FORMS
                   WHERE  GPF_GUI_ID = 203 
                     AND  GPF_READ_PERM <> 0
                     AND (GPF_FORM_ID / 1000000) in ('+ @pRoleId + '))'

exec(@Sql)

END
GO