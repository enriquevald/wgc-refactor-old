USE WGDB_000;

SET ANSI_NULLS ON;

----------------------------------------------------
-----------------VERSION CONTROL--------------------
----------------------------------------------------

DECLARE 
@Exp_SmartFloor_ClientId int, 
@Exp_SmartFloor_CommonBuildId int,
@Exp_SmartFloor_ClientBuildId int,
@New_SmartFloor_ReleaseId int,
@New_SmartFloor_ScriptName nvarchar(50),
@New_SmartFloor_Description nvarchar(1000);

SET @Exp_SmartFloor_ClientId = 18;
SET @Exp_SmartFloor_CommonBuildId = 100;
SET @Exp_SmartFloor_ClientBuildId = 1;
SET @New_SmartFloor_ReleaseId = 31;
SET @New_SmartFloor_ScriptName = N'UpdateTo_SmartFloor_18.031.sql';
SET @New_SmartFloor_Description = N'Patch 18.031 - 30';

IF EXISTS ( SELECT 1 FROM DB_VERSION_SMARTFLOOR )
 DELETE FROM DB_VERSION_SMARTFLOOR;

INSERT INTO [dbo].[db_version_smartfloor]
           ([db_client_id]
           ,[db_common_build_id]
           ,[db_client_build_id]
           ,[db_release_id]
           ,[db_updated_script]
           ,[db_updated]
           ,[db_description])
     VALUES
           (@Exp_SmartFloor_ClientId
           ,@Exp_SmartFloor_CommonBuildId
           ,@Exp_SmartFloor_ClientBuildId
           ,@New_SmartFloor_ReleaseId
           ,@New_SmartFloor_ScriptName
           ,Getdate()
           ,@New_SmartFloor_Description);

GO
----------------------------------------------------
----------------------TABLES------------------------
----------------------------------------------------

IF NOT EXISTS(SELECT *  FROM sys.columns  WHERE Name = N'lr_nls' AND Object_ID = Object_ID(N'dbo.layout_ranges'))
BEGIN
  ALTER TABLE dbo.layout_ranges
        ADD  lr_nls int NULL
END
IF NOT EXISTS(SELECT *  FROM sys.columns  WHERE Name = N'lr_external_nls' AND Object_ID = Object_ID(N'dbo.layout_ranges'))
BEGIN
  ALTER TABLE dbo.layout_ranges
        ADD lr_external_nls nvarchar(100) NULL
END
IF NOT EXISTS(SELECT *  FROM sys.columns  WHERE Name = N'lr_editable' AND Object_ID = Object_ID(N'dbo.layout_ranges'))
BEGIN
  ALTER TABLE dbo.layout_ranges
        ADD lr_editable bit NULL
END
IF NOT EXISTS(SELECT *  FROM sys.columns  WHERE Name = N'lr_visible' AND Object_ID = Object_ID(N'dbo.layout_ranges'))
BEGIN
  ALTER TABLE dbo.layout_ranges
        ADD lr_visible bit NULL
END
IF NOT EXISTS(SELECT *  FROM sys.columns  WHERE Name = N'lr_has_unknown_values' AND Object_ID = Object_ID(N'dbo.layout_ranges'))
BEGIN
  ALTER TABLE dbo.layout_ranges
        ADD lr_has_unknown_values bit NULL
END

GO


ALTER TABLE dbo.layout_ranges SET (LOCK_ESCALATION = TABLE)
GO


IF NOT EXISTS(SELECT *  FROM sys.columns  WHERE Name = N'lrl_external_nls' AND Object_ID = Object_ID(N'dbo.layout_ranges_legends'))
BEGIN
  ALTER TABLE dbo.layout_ranges_legends
        ADD lrl_external_nls VARCHAR(50)
END

GO


----------------------------------------------------
----------------------DATA--------------------------
----------------------------------------------------

IF  EXISTS (SELECT * FROM layout_ranges WHERE lr_name = 'Age'  and lr_section_id = 60 )
update layout_ranges set lr_nls = 8432 , lr_editable= 1,lr_visible=1, lr_has_unknown_values= 1
where lr_name = 'Age'  and lr_section_id = 60;

IF  EXISTS (SELECT * FROM layout_ranges WHERE lr_name = 'Gender'and lr_section_id = 60)
update layout_ranges set lr_nls = 8433 , lr_editable= 0,lr_visible=1, lr_has_unknown_values= 0
where lr_name = 'Gender'and lr_section_id = 60;

IF  EXISTS (SELECT * FROM layout_ranges WHERE lr_name = 'VIPS'and lr_section_id = 60)
update layout_ranges set lr_nls = 8434 , lr_editable= 0,lr_visible=1, lr_has_unknown_values= 0
where lr_name = 'VIPS'and lr_section_id = 60;

IF  EXISTS (SELECT * FROM layout_ranges WHERE lr_name = 'Bet Average' and lr_section_id = 60)
update layout_ranges set lr_nls = 8435 , lr_editable= 1,lr_visible=1, lr_has_unknown_values= 0
where lr_name = 'Bet Average' and lr_section_id = 60;

IF  EXISTS (SELECT * FROM layout_ranges WHERE lr_name = 'Netwin' and lr_section_id = 60)
update layout_ranges set lr_nls = 8436 , lr_editable= 1,lr_visible=1, lr_has_unknown_values= 0
where lr_name = 'Netwin' and lr_section_id = 60;

IF  EXISTS (SELECT * FROM layout_ranges WHERE lr_name = 'Coin In' and lr_section_id = 60)
update layout_ranges set lr_nls = 8437 , lr_editable= 1,lr_visible=1, lr_has_unknown_values= 0
where lr_name = 'Coin In' and lr_section_id = 60;

IF  EXISTS (SELECT * FROM layout_ranges WHERE lr_name = 'Occupancy' and lr_section_id = 60)
update layout_ranges set lr_nls = 8438 , lr_editable= 0,lr_visible=1, lr_has_unknown_values= 0
where lr_name = 'Occupancy' and lr_section_id = 60;

IF  EXISTS (SELECT * FROM layout_ranges WHERE lr_name = 'Bet average' and lr_section_id = 70)
update layout_ranges set lr_nls = 8435 , lr_editable= 1,lr_visible=1, lr_has_unknown_values= 0
where lr_name = 'Bet average' and lr_section_id = 70;

IF  EXISTS (SELECT * FROM layout_ranges WHERE lr_name = 'Netwin' and lr_section_id = 70)
update layout_ranges set lr_nls = 8436 , lr_editable= 1,lr_visible=1, lr_has_unknown_values= 0
where lr_name = 'Netwin' and lr_section_id = 70;

IF  EXISTS (SELECT * FROM layout_ranges WHERE lr_name = 'Coin In' and lr_section_id = 70)
update layout_ranges set lr_nls = 8437 , lr_editable= 1,lr_visible=1, lr_has_unknown_values= 0
where lr_name = 'Coin In' and lr_section_id = 70;
  
IF  EXISTS (SELECT * FROM layout_ranges WHERE lr_name = 'Vendor' and lr_section_id = 70)
update layout_ranges set lr_nls = 8440 , lr_editable= 0,lr_visible=1, lr_has_unknown_values= 0
where lr_name = 'Vendor' and lr_section_id = 70;

IF  EXISTS (SELECT * FROM layout_ranges WHERE lr_name = 'State' and lr_section_id = 70)
update layout_ranges set lr_nls = 8441 , lr_editable= 0,lr_visible=1, lr_has_unknown_values= 0
where lr_name = 'State' and lr_section_id = 70;

IF  EXISTS (SELECT * FROM layout_ranges WHERE lr_name = 'Denomination' and lr_section_id = 70)
update layout_ranges set lr_nls = 8453 , lr_editable= 0,lr_visible=0, lr_has_unknown_values= 0
where lr_name = 'Denomination' and lr_section_id = 70;

IF  EXISTS (SELECT * FROM layout_ranges WHERE lr_name = 'Losses' and lr_section_id = 70)
update layout_ranges set lr_nls = 8452 , lr_editable= 0,lr_visible=0, lr_has_unknown_values= 0
where lr_name = 'Losses' and lr_section_id = 70;

GO

-----------------------

UPDATE layout_ranges SET lr_external_nls = 'NLS_AGE'
  WHERE lr_id = 5

UPDATE layout_ranges SET lr_external_nls = 'NLS_BET_AVERAGE'
  WHERE lr_id = 6

UPDATE layout_ranges SET lr_external_nls = 'NLS_GENDER'
  WHERE lr_id = 8

UPDATE layout_ranges SET lr_external_nls = 'NLS_NETWIN'
  WHERE lr_id = 9

UPDATE layout_ranges SET lr_external_nls = 'NLS_COIN_IN'
  WHERE lr_id = 17

UPDATE layout_ranges SET lr_external_nls = 'NLS_DOOR'
  WHERE lr_id = 19

UPDATE layout_ranges SET lr_external_nls = 'NLS_BILL_ACEPTOR'
  WHERE lr_id = 20

UPDATE layout_ranges SET lr_external_nls = 'NLS_PRINTER'
  WHERE lr_id = 21

UPDATE layout_ranges SET lr_external_nls = 'NLS_PLAYED_WON'
  WHERE lr_id = 23

UPDATE layout_ranges SET lr_external_nls = 'NLS_MACHINE'
  WHERE lr_id = 27

UPDATE layout_ranges SET lr_external_nls = 'NLS_SUPPORT'
  WHERE lr_id = 28

UPDATE layout_ranges SET lr_external_nls = 'NLS_JACKPOT'
  WHERE lr_id = 29

UPDATE layout_ranges SET lr_external_nls = 'NLS_VENDOR'
  WHERE lr_id = 30

UPDATE layout_ranges SET lr_external_nls = 'NLS_DENOMINATION'
  WHERE lr_id = 31

UPDATE layout_ranges SET lr_external_nls = 'NLS_STATE'
  WHERE lr_id = 32

UPDATE layout_ranges SET lr_external_nls = 'NLS_VIPS'
  WHERE lr_id = 47

UPDATE layout_ranges SET lr_external_nls = 'NLS_JACKPOT'
  WHERE lr_id = 48

UPDATE layout_ranges SET lr_external_nls = 'NLS_CALL_ATTENDANT'
  WHERE lr_id = 49

UPDATE layout_ranges SET lr_external_nls = 'NLS_HIGH_ROLLER'
  WHERE lr_id = 50

UPDATE layout_ranges SET lr_external_nls = 'NLS_BET_AVERAGE'
  WHERE lr_id = 54

UPDATE layout_ranges SET lr_external_nls = 'NLS_NETWIN'
  WHERE lr_id = 55

UPDATE layout_ranges SET lr_external_nls = 'NLS_COIN_IN'
  WHERE lr_id = 56

UPDATE layout_ranges SET lr_external_nls = 'NLS_STACKER'
  WHERE lr_id = 57

UPDATE layout_ranges SET lr_external_nls = 'NLS_CUSTOM_CUSTOM'
  WHERE lr_id = 59

UPDATE layout_ranges SET lr_external_nls = 'NLS_OCCUPANCY'
  WHERE lr_id = 60

UPDATE layout_ranges SET lr_external_nls = 'NLS_CUSTOM_PLAYER'
  WHERE lr_id = 61

UPDATE layout_ranges SET lr_external_nls = 'NLS_CUSTOM_MACHINE'
  WHERE lr_id = 62



UPDATE layout_ranges_legends SET lrl_external_nls = 'NLS_PLAYED'
  WHERE lrl_id = 40

UPDATE layout_ranges_legends SET lrl_external_nls = 'NLS_WON'
  WHERE lrl_id = 41

UPDATE layout_ranges_legends SET lrl_external_nls = 'NLS_PAYOUT'
  WHERE lrl_id = 42

UPDATE layout_ranges_legends SET lrl_external_nls = 'NLS_WITHOUT_PLAYS'
  WHERE lrl_id = 43

UPDATE layout_ranges_legends SET lrl_external_nls = 'NLS_CALL_ATTENDANT'
  WHERE lrl_id = 44

UPDATE layout_ranges_legends SET lrl_external_nls = 'NLS_BILL_JAM'
  WHERE lrl_id = 45

UPDATE layout_ranges_legends SET lrl_external_nls = 'NLS_ERROR'
  WHERE lrl_id = 46

UPDATE layout_ranges_legends SET lrl_external_nls = 'NLS_COUNTERFEITS'
  WHERE lrl_id = 47

UPDATE layout_ranges_legends SET lrl_external_nls = 'NLS_STACKER_ALMOST_FULL'
  WHERE lrl_id = 48

UPDATE layout_ranges_legends SET lrl_external_nls = 'NLS_STACKER_FULL'
  WHERE lrl_id = 49

UPDATE layout_ranges_legends SET lrl_external_nls = 'NLS_COM_ERROR'
  WHERE lrl_id = 50

UPDATE layout_ranges_legends SET lrl_external_nls = 'NLS_OUTPUT_ERROR'
  WHERE lrl_id = 51

UPDATE layout_ranges_legends SET lrl_external_nls = 'NLS_RIBBON_REPLACEMENT'
  WHERE lrl_id = 52

UPDATE layout_ranges_legends SET lrl_external_nls = 'NLS_LOW_PAPER'
  WHERE lrl_id = 53

UPDATE layout_ranges_legends SET lrl_external_nls = 'NLS_CARRIAGE_JAM'
  WHERE lrl_id = 54

UPDATE layout_ranges_legends SET lrl_external_nls = 'NLS_POWER_SUPPLY'
  WHERE lrl_id = 55

UPDATE layout_ranges_legends SET lrl_external_nls = 'NLS_SLOT_DOOR'
  WHERE lrl_id = 56

UPDATE layout_ranges_legends SET lrl_external_nls = 'NLS_DROP_DOOR'
  WHERE lrl_id = 57

UPDATE layout_ranges_legends SET lrl_external_nls = 'NLS_CARD_CAGE_DOOR'
  WHERE lrl_id = 58

UPDATE layout_ranges_legends SET lrl_external_nls = 'NLS_BELLY_DOOR'
  WHERE lrl_id = 59

UPDATE layout_ranges_legends SET lrl_external_nls = 'NLS_CASHBOX_DOOR'
  WHERE lrl_id = 60

UPDATE layout_ranges_legends SET lrl_external_nls = 'NLS_ERROR_CMOS_RAM'
  WHERE lrl_id = 61

UPDATE layout_ranges_legends SET lrl_external_nls = 'NLS_ERROR_EEPROM'
  WHERE lrl_id = 62

UPDATE layout_ranges_legends SET lrl_external_nls = 'NLS_LOW_BATTERY'
  WHERE lrl_id = 63

UPDATE layout_ranges_legends SET lrl_external_nls = 'NLS_JACKPOT'
  WHERE lrl_id = 64

UPDATE layout_ranges_legends SET lrl_external_nls = 'NLS_CANCELLED_CREDITS'
  WHERE lrl_id = 65

UPDATE layout_ranges_legends SET lrl_external_nls = 'NLS_HIGH_ROLLER'
  WHERE lrl_id = 101

UPDATE layout_ranges_legends SET lrl_external_nls = 'NLS_HIGH_ROLLER_ANONYMOUS'
  WHERE lrl_id = 103


GO

----------------------------------------------------
----------------------STOREDS-----------------------
----------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetRanges]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Layout_GetRanges]
GO

CREATE PROCEDURE [dbo].[Layout_GetRanges]
      @pRole INT
    , @pUserId INT
AS
BEGIN

  SELECT LR_ID
     , LR_NAME
     , LR_FIELD
     , LR_ROLE
     , LR_SECTION_ID
     , LR_ICON
	 , LR_EXTERNAL_NLS
    FROM LAYOUT_RANGES
    WHERE LR_ROLE & @pRole = @pRole 
  ORDER BY LR_NAME ASC
  
  SELECT LRL_RANGE_ID
       , LRL_LABEL
       , LRL_VALUE1
       , LRL_VALUE2
       , LRL_OPERATOR
       , LRL_COLOR
       , LRL_ID
       , LRL_EDITABLE
	   , LRL_EXTERNAL_NLS
    FROM LAYOUT_RANGES_LEGENDS
   WHERE LRL_RANGE_ID IN 
    (
      SELECT LR_ID
        FROM LAYOUT_RANGES
       WHERE LR_ROLE & @pRole = @pRole 
    )
  ORDER BY LRL_RANGE_ID, LRL_VALUE1 

  SELECT LCF_NAME
       , LCF_FILTER_PARAMETERS
       , LCF_ID
       , LCF_DISPLAY
    FROM LAYOUT_USERS_CUSTOM_FILTERS
   WHERE LCF_USER_ID= @pUserId 
      
     SELECT LR_SECTION_ID AS [CATEGORY] --41 PLAYER / 42 MACHINE
       , CASE 
		   WHEN lr_id=59 THEN 'Custom (Others)' 
		   WHEN lr_id=61 THEN 'Custom (Players)' 
		   WHEN lr_id=62 THEN 'Custom (Machines)' 
		   ELSE LR_NAME + ' - ' + LRL_LABEL
         END  
	     AS [ALARM_DESC]
       , CAST((LR_FIELD * 1000) + LRL_VALUE1 AS INT) AS [SUBCATEGORY] 
       , R.LR_ICON AS ICON
       , L.LRL_AUTO_ASSIGN_PRIORITY
	   , LR_EXTERNAL_NLS
	   , LRL_EXTERNAL_NLS
    FROM LAYOUT_RANGES AS R
INNER JOIN LAYOUT_RANGES_LEGENDS  AS L
      ON R.LR_ID = L.LRL_RANGE_ID
   WHERE LR_SECTION_ID IN (41,42,43)
ORDER BY LR_SECTION_ID    
END 


