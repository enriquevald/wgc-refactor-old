USE WGDB_000;

SET ANSI_NULLS ON;

----------------------------------------------------
-----------------VERSION CONTROL--------------------
----------------------------------------------------

DECLARE 
@Exp_SmartFloor_ClientId int, 
@Exp_SmartFloor_CommonBuildId int,
@Exp_SmartFloor_ClientBuildId int,
@New_SmartFloor_ReleaseId int,
@New_SmartFloor_ScriptName nvarchar(50),
@New_SmartFloor_Description nvarchar(1000);

SET @Exp_SmartFloor_ClientId = 18;
SET @Exp_SmartFloor_CommonBuildId = 100;
SET @Exp_SmartFloor_ClientBuildId = 1;
SET @New_SmartFloor_ReleaseId = 11;
SET @New_SmartFloor_ScriptName = N'UpdateTo_SmartFloor_18.011.sql';
SET @New_SmartFloor_Description = N'Patch 18.010 - 11';

IF EXISTS ( SELECT 1 FROM DB_VERSION_SMARTFLOOR )
 DELETE FROM DB_VERSION_SMARTFLOOR;

INSERT INTO [dbo].[db_version_smartfloor]
           ([db_client_id]
           ,[db_common_build_id]
           ,[db_client_build_id]
           ,[db_release_id]
           ,[db_updated_script]
           ,[db_updated]
           ,[db_description])
     VALUES
           (@Exp_SmartFloor_ClientId
           ,@Exp_SmartFloor_CommonBuildId
           ,@Exp_SmartFloor_ClientBuildId
           ,@New_SmartFloor_ReleaseId
           ,@New_SmartFloor_ScriptName
           ,Getdate()
           ,@New_SmartFloor_Description);

           
----------------------------------------------------
----------------------STOREDS-----------------------
----------------------------------------------------


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_AssignDeviceToUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Layout_AssignDeviceToUser]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_Get_Resources]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Layout_Get_Resources]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetCashiersActivity]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Layout_GetCashiersActivity]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetGamesStatistics]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Layout_GetGamesStatistics]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetNotIncludedObjects]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Layout_GetNotIncludedObjects]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetPlayersInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Layout_GetPlayersInfo]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetPlaySessionsOpen]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Layout_GetPlaySessionsOpen]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetTerminalActivity]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Layout_GetTerminalActivity]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetTerminalIdFromAssetNumber]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Layout_GetTerminalIdFromAssetNumber]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetTerminalsInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Layout_GetTerminalsInfo]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_SaveFloor_NEW]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Layout_SaveFloor_NEW]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LAYOUT_SET_LOCATION]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[LAYOUT_SET_LOCATION]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LAYOUT_SET_OBJECT]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[LAYOUT_SET_OBJECT]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_SetTerminalStatusFlag]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Layout_SetTerminalStatusFlag]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_UserValidToUseSmartfloorApp]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Layout_UserValidToUseSmartfloorApp]
GO 

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[layout_objects_positions]') AND type in (N'U'))
DROP TABLE [dbo].[layout_objects_positions]
GO

CREATE PROCEDURE [dbo].[Layout_SaveFloor]
   @pGeometry AS VARCHAR(MAX)
 , @pImageMap AS VARCHAR(MAX)
 , @pColor AS VARCHAR(20)
 , @pName AS VARCHAR(50)
 , @pUnit AS INT
 , @pWidth AS INT
 , @pHeight AS INT
AS
BEGIN
  DECLARE @_FloorId AS INT
  SET @_FloorId = (SELECT LF_FLOOR_ID FROM LAYOUT_FLOORS WHERE LF_NAME = @pName  )
  IF @_FloorId IS NULL
    BEGIN
        -- New Floor
        INSERT   INTO LAYOUT_FLOORS
                 (LF_GEOMETRY, LF_IMAGE_MAP, LF_COLOR, LF_NAME, LF_METRIC, LF_WIDTH, LF_HEIGHT) 
        VALUES   (@pGeometry, @pImageMap, @pColor, @pName, @pUnit, @pWidth, @pHeight)   
        SET @_FloorId = SCOPE_IDENTITY()
    END
  ELSE
    BEGIN
        -- Update existing floor
        UPDATE   LAYOUT_FLOORS
           SET   LF_GEOMETRY = @pGeometry
               , LF_IMAGE_MAP = @pImageMap
               , LF_COLOR = @pColor
               , LF_NAME = @pName
               , LF_METRIC = @pUnit
               , LF_WIDTH = @pWidth
               , LF_HEIGHT = @pHeight
         WHERE   LF_FLOOR_ID = @_FloorId
    END
  SELECT   @_FloorId
END

GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'SmartFloor' AND GP_SUBJECT_KEY = 'Dashboard.Meteogram.Enabled')
                     INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('SmartFloor','Dashboard.Meteogram.Enabled','0')
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_SaveWeatherDay]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Layout_SaveWeatherDay]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Layout_SaveWeatherDay]
      @pDate INT
    , @pWeatherCode INT
    , @pXML XML
AS
BEGIN
  IF NOT EXISTS (SELECT TOP 1 * FROM LAYOUT_WEATHER_DIARY WHERE LWD_DATE = @pDate)
  
    INSERT   INTO LAYOUT_WEATHER_DIARY
           ( LWD_DATE
           , LWD_WEATHER_CODE
           , LWD_RESPONSE)
    VALUES ( @pDate
           , @pWeatherCode
           , @pXML ) 
END


GO

/****** Object:  StoredProcedure [dbo].[Layout_GetLastestMobileApplicationVersionApk]    Script Date: 04/18/2016 12:44:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetLastestMobileApplicationVersionApk]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Layout_GetLastestMobileApplicationVersionApk]
GO

/****** Object:  StoredProcedure [dbo].[Layout_GetLastestMobileApplicationVersionApk]    Script Date: 04/18/2016 12:44:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Carlos Rodrigo
-- Create date: 14102015
-- Description:	SP to get the lastest version of the mobile application
-- =============================================
CREATE PROCEDURE [dbo].[Layout_GetLastestMobileApplicationVersionApk] 
AS
BEGIN
	SELECT TOP(1)lmav_apk, lmav_version
	FROM layout_mobile_application_versions
	ORDER BY lmav_id DESC
END

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetLastestMobileApplicationVersion]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Layout_GetLastestMobileApplicationVersion]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Carlos Rodrigo
-- Create date: 14102015
-- Description:	SP to get the lastest version of the mobile application
-- =============================================
CREATE PROCEDURE [dbo].[Layout_GetLastestMobileApplicationVersion] 
AS
BEGIN
	SELECT TOP(1) lmav_version
	FROM layout_mobile_application_versions
	ORDER BY lmav_id DESC
END

GO