USE WGDB_000;

SET ANSI_NULLS ON;

----------------------------------------------------
-----------------VERSION CONTROL--------------------
----------------------------------------------------

DECLARE 
@Exp_SmartFloor_ClientId int, 
@Exp_SmartFloor_CommonBuildId int,
@Exp_SmartFloor_ClientBuildId int,
@New_SmartFloor_ReleaseId int,
@New_SmartFloor_ScriptName nvarchar(50),
@New_SmartFloor_Description nvarchar(1000);

SET @Exp_SmartFloor_ClientId = 18;
SET @Exp_SmartFloor_CommonBuildId = 100;
SET @Exp_SmartFloor_ClientBuildId = 1;
SET @New_SmartFloor_ReleaseId = 21;
SET @New_SmartFloor_ScriptName = N'UpdateTo_SmartFloor_18.021.sql';
SET @New_SmartFloor_Description = N'Patch 18.021 - 20';

IF EXISTS ( SELECT 1 FROM DB_VERSION_SMARTFLOOR )
 DELETE FROM DB_VERSION_SMARTFLOOR;

INSERT INTO [dbo].[db_version_smartfloor]
           ([db_client_id]
           ,[db_common_build_id]
           ,[db_client_build_id]
           ,[db_release_id]
           ,[db_updated_script]
           ,[db_updated]
           ,[db_description])
     VALUES
           (@Exp_SmartFloor_ClientId
           ,@Exp_SmartFloor_CommonBuildId
           ,@Exp_SmartFloor_ClientBuildId
           ,@New_SmartFloor_ReleaseId
           ,@New_SmartFloor_ScriptName
           ,Getdate()
           ,@New_SmartFloor_Description);

           
----------------------------------------------------
----------------------STOREDS-----------------------
----------------------------------------------------
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetActivity]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[Layout_GetActivity]

GO
/****** Object:  StoredProcedure [dbo].[Layout_GetActivity]    Script Date: 08/16/2016 10:55:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Layout_GetActivity]
AS
BEGIN

  DECLARE @terminal_name_type AS VARCHAR(50)
      DECLARE @big_won_limit AS MONEY
      CREATE TABLE #temp_table    (LO_ID INT
                                             , TERMINAL_ID INT
                                             , TERMINAL_STATUS INT
                                             , PLAY_SESSION_STATUS INT
                                             , IS_ANONYMOUS INT
                                             , PLAYER_ACCOUNT_ID INT
                                             , PLAYER_NAME VARCHAR(50)
                                             , PLAYER_GENDER INT
                                             , PLAYER_AGE INT
                                             , PLAYER_LEVEL INT
                                             , PLAYER_IS_VIP INT
                                             , AL_SAS_HOST_ERROR INT
                                             , AL_DOOR_FLAGS INT 
                                             , AL_BILL_FLAGS INT
                                             , AL_PRINTER_FLAGS INT
                                             , AL_EGM_FLAGS INT
                                             , AL_PLAYED_WON_FLAGS INT
                                             , AL_JACKPOT_FLAGS INT
                                             , AL_STACKER_STATUS INT
                                             , AL_CALL_ATTENDANT_FLAGS INT
                                             , AL_MACHINE_FLAGS INT           
                                             , AL_BIG_WON BIT           
                                             , PLAYED_AMOUNT MONEY
                                             , WON_AMOUNT MONEY
                                             , PLAY_SESSION_BET_AVERAGE MONEY
                                             , TERMINAL_BET_AVERAGE MONEY
                                             , PLAY_SESSION_DURATION INT
                                             , PLAY_SESSION_TOTAL_PLAYED MONEY
                                             , PLAY_SESSION_PLAYED_COUNT INT
                                             , TERMINAL_BANK_ID INT
                                             , TERMINAL_AREA_ID INT
                                             , TERMINAL_PROVIDER_NAME VARCHAR(50)
                                             , PLAY_SESSION_WON_AMOUNT MONEY
                                             , NET_WIN MONEY
                                             , PLAY_SESSION_NET_WIN MONEY
                                             , TERMINAL_NAME NVARCHAR(50)
                                             , TERMINAL_PLAYED_COUNT INT
                                             , PLAYER_TRACKDATA NVARCHAR(50)
                                             , PLAYER_LEVEL_NAME NVARCHAR(50)
                                             , ALARMS NVARCHAR(MAX)
                                             , TERMINAL_DENOMINATION MONEY
                                             , PLAYER_CREATED INT
                                             , PS_rate float  -- Test field
                                             , TERMINAL_PAYOUT MONEY
                                             , THEORETICAL_HOLD MONEY
                                             , AC_HOLDER_BIRTH_DATE DATETIME
                                             , PLAY_SESSION_CASH_IN MONEY
                                             , PLAY_SESSION_CASH_OUT MONEY
											 , PLAYER_FIRST_NAME VARCHAR(50)
											 , PLAYER_LAST_NAME1 VARCHAR(50)
											 , PLAYER_LAST_NAME2 VARCHAR(50)
                                             )
                                             
      -- TERMINAL NAME TYPE
      SELECT @terminal_name_type = GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'SasHost' AND GP_SUBJECT_KEY = 'DisplayName.Format'
      IF (@terminal_name_type IS NULL)
      BEGIN
        SET @terminal_name_type = '%Name'
      END

      -- TODO: Read from GP
      SET @big_won_limit = 1000

  DECLARE @Now AS DATETIME
  DECLARE @TodayOpening AS DATETIME
  DECLARE @CurrentHour AS INT
  DECLARE @HourNow AS DATETIME
  DECLARE @NextHourNow AS DATETIME
  
  SET @Now = GETDATE()
  SET @TodayOpening = DBO.TODAYOPENING(0)
  SET @CurrentHour = DATEPART(HOUR, @Now)
  SET @HourNow = DATEADD(HOUR, DATEDIFF(HOUR, 0, @Now), 0)
  SET @NextHourNow = DATEADD(HOUR, 1, @HourNow)

  DECLARE @DefaultHold MONEY
  SET @DefaultHold = (SELECT   1 - CAST (GP_KEY_VALUE AS MONEY) / 100        
                        FROM   GENERAL_PARAMS                                
                       WHERE   GP_GROUP_KEY   = 'PlayerTracking'               
                         AND   GP_SUBJECT_KEY = 'TerminalDefaultPayout')

      INSERT INTO #temp_table
      SELECT LO.LO_ID
      
            -- Terminal info
            , LO.LO_EXTERNAL_ID AS TERMINAL_ID
            , TE.TE_STATUS   AS TERMINAL_STATUS

            -- Play Session info
            , PLAY_SESSION_STATUS = CASE  WHEN (PS_PLAY_SESSION_ID IS NOT NULL) THEN 2  
                                                 ELSE 1 END

            -- Player info
            , IS_ANONYMOUS = CASE  WHEN (TE.TE_CURRENT_ACCOUNT_ID IS NOT NULL) THEN CASE WHEN (AC.AC_HOLDER_NAME IS NOT NULL) THEN 0 ELSE 1 END 
                                                ELSE NULL END
            , PS.PS_ACCOUNT_ID AS PLAYER_ACCOUNT_ID
            , PLAYER_NAME = CASE WHEN (TE.TE_CURRENT_ACCOUNT_ID IS NOT NULL) THEN ISNULL(AC.AC_HOLDER_NAME, 'Anonymous')
                                          ELSE NULL END
            , AC.AC_HOLDER_GENDER AS PLAYER_GENDER
            , DATEDIFF(YEAR, AC.AC_HOLDER_BIRTH_DATE, @Now) AS PLAYER_AGE
            , PLAYER_LEVEL = CASE  WHEN (TE.TE_CURRENT_ACCOUNT_ID IS NOT NULL) THEN CASE WHEN (AC.AC_HOLDER_NAME IS NOT NULL) THEN AC.AC_HOLDER_LEVEL ELSE -1 END 
                                                ELSE 99 END
            , ISNULL(AC.AC_HOLDER_IS_VIP, 0) AS PLAYER_IS_VIP
            
             -- Flags
            , TS_SAS_HOST_ERROR AS AL_SAS_HOST_ERROR
            , ISNULL(TS_DOOR_FLAGS, 0) AS AL_DOOR_FLAGS 
             , ISNULL(TS_BILL_FLAGS, 0) AS AL_BILL_FLAGS
            , ISNULL(TS_PRINTER_FLAGS, 0) AS AL_PRINTER_FLAGS
            , ISNULL(TS_EGM_FLAGS, 0) AS AL_EGM_FLAGS
            , ISNULL(TS_PLAYED_WON_FLAGS, 0) AS AL_PLAYED_WON_FLAGS
            , ISNULL(TS_JACKPOT_FLAGS, 0) AS AL_JACKPOT_FLAGS
            , ISNULL(TS_STACKER_STATUS, 0) AS AL_STACKER_STATUS
            , ISNULL(TS_CALL_ATTENDANT_FLAGS, 0) AS AL_CALL_ATTENDANT_FLAGS
            , ISNULL(TS_MACHINE_FLAGS, 0) AS AL_MACHINE_FLAGS          
            , AL_BIG_WON = CASE WHEN ISNULL(SPH.WON_AMOUNT, 0) > @big_won_limit THEN 1
                                     ELSE 0 END
                                     
             -- Money info
            , ISNULL(SPH.PLAYED_AMOUNT , 0) AS PLAYED_AMOUNT
            , ISNULL(SPH.WON_AMOUNT, 0) AS WON_AMOUNT
            , PLAY_SESSION_BET_AVERAGE = CASE WHEN (PS.PS_PLAYED_COUNT > 0) THEN ROUND((PS.PS_TOTAL_PLAYED / PS.PS_PLAYED_COUNT), 2)
                                                               ELSE NULL END
            , TERMINAL_BET_AVERAGE = CASE WHEN (SPH.PLAYED_COUNT > 0) THEN ROUND((SPH.PLAYED_AMOUNT / SPH.PLAYED_COUNT), 2)
                                                         ELSE NULL END
            , DATEDIFF(SECOND, ISNULL(PS.PS_STARTED, @Now), @Now) AS PLAY_SESSION_DURATION
            , PS.PS_TOTAL_PLAYED AS PLAY_SESSION_TOTAL_PLAYED
            , PS.PS_PLAYED_COUNT AS PLAY_SESSION_PLAYED_COUNT
            
             -- Area & Bank
            , BK.bk_bank_id AS TERMINAL_BANK_ID
            , BK.bk_area_id AS TERMINAL_AREA_ID
            , PR.pv_name AS TERMINAL_PROVIDER_NAME
            
             , PS.PS_WON_AMOUNT AS PLAY_SESSION_WON_AMOUNT
            
             , SPH.PLAYED_AMOUNT - SPH.WON_AMOUNT AS NET_WIN
            , PS.PS_TOTAL_PLAYED - PS.PS_WON_AMOUNT AS PLAY_SESSION_NET_WIN
            --, TE.TE_NAME AS TERMINAL_NAME
                 , CASE WHEN @terminal_name_type = '%FloorId' THEN
                      TE.TE_NAME + ' [' + TE.TE_FLOOR_ID + ']'
                    WHEN @terminal_name_type = '%BaseName' THEN
                      -- TODO: Find correct field
                      -- TE.TE_BASE_NAME
                      TE.TE_NAME
                    ELSE 
                      TE.TE_NAME
               END AS TERMINAL_NAME
            , SPH.PLAYED_COUNT AS TERMINAL_PLAYED_COUNT
            , AC.AC_TRACK_DATA AS PLAYER_TRACKDATA
            , ISNULL(LEVELS.NAME,'') AS PLAYER_LEVEL_NAME
     , NULL AS ALARMS
     , TE.TE_DENOMINATION AS TERMINAL_DENOMINATION
     , CASE WHEN (AC.AC_CREATED >= @HourNow AND AC.AC_CREATED < @NextHourNow) THEN 1 ELSE 0 END AS PLAYER_CREATED
     , NULL AS PS_RATE
     , ISNULL(TE.TE_THEORETICAL_HOLD, @DefaultHold) AS TERMINAL_PAYOUT
     , (PS.PS_TOTAL_PLAYED * ISNULL(TE.TE_THEORETICAL_HOLD, @DefaultHold)) AS THEORETICAL_HOLD
     , AC.AC_HOLDER_BIRTH_DATE
     , PS.PS_TOTAL_CASH_IN
     , PS.PS_CASH_OUT
	 , PLAYER_FIRST_NAME  = CASE WHEN (TE.TE_CURRENT_ACCOUNT_ID IS NOT NULL) THEN ISNULL(AC.AC_HOLDER_NAME3, 'Anonymous')
                                          ELSE NULL END
	 , PLAYER_LAST_NAME1  = CASE WHEN (TE.TE_CURRENT_ACCOUNT_ID IS NOT NULL) THEN ISNULL(AC.AC_HOLDER_NAME1, 'Anonymous')
                                          ELSE NULL END
	 , PLAYER_LAST_NAME2  = CASE WHEN (TE.TE_CURRENT_ACCOUNT_ID IS NOT NULL) THEN ISNULL(AC.AC_HOLDER_NAME2, 'Anonymous')
                                          ELSE NULL END
        FROM LAYOUT_OBJECTS AS LO
            INNER JOIN LAYOUT_OBJECT_LOCATIONS AS LOL ON LO.LO_ID = LOL.LOL_OBJECT_ID
            INNER JOIN TERMINALS AS TE ON TE.TE_TERMINAL_ID = LO.LO_EXTERNAL_ID 
            INNER JOIN PROVIDERS AS PR ON PR.pv_id = TE.te_prov_id
            
            LEFT JOIN BANKS AS BK ON BK.bk_bank_id = TE.te_bank_id
                        
            LEFT JOIN (SELECT PS_TERMINAL_ID
                            , MAX(PS_PLAY_SESSION_ID) AS PS_PLAY_SESSION_ID
                            , PS_ACCOUNT_ID
                            , PS_TOTAL_PLAYED
                            , PS_WON_AMOUNT
                            , PS_PLAYED_COUNT
                            , PS_STARTED
                            , PS_PLAYED_AMOUNT
                            , PS_TOTAL_CASH_IN
                            , PS_CASH_OUT
                             FROM PLAY_SESSIONS
                            WHERE PS_STATUS = 0
                        GROUP BY PS_TERMINAL_ID, PS_ACCOUNT_ID, PS_TOTAL_PLAYED, PS_WON_AMOUNT, PS_PLAYED_COUNT, PS_PLAYED_AMOUNT, PS_STARTED,PS_TOTAL_CASH_IN,PS_CASH_OUT) AS PS ON TE.TE_TERMINAL_ID = PS.PS_TERMINAL_ID
                        
            LEFT JOIN ACCOUNTS AS AC ON AC.AC_ACCOUNT_ID = TE.TE_CURRENT_ACCOUNT_ID
            
            LEFT JOIN TERMINAL_STATUS AS TS ON TE.TE_TERMINAL_ID = TS.TS_TERMINAL_ID
            
            LEFT JOIN (SELECT SPH_TERMINAL_ID 
                                  , SUM(SPH_PLAYED_AMOUNT)AS PLAYED_AMOUNT
                                  , SUM(SPH_WON_AMOUNT) AS WON_AMOUNT
                                  , SUM(SPH_PLAYED_COUNT) AS PLAYED_COUNT
                         FROM SALES_PER_HOUR_V2
                        WHERE SPH_BASE_HOUR >= @TodayOpening
                       GROUP BY SPH_TERMINAL_ID) AS SPH ON SPH.SPH_TERMINAL_ID = TE.TE_TERMINAL_ID
                       
            LEFT JOIN (SELECT CAST(REPLACE(REPLACE(GP_SUBJECT_KEY,'Level',''),'.Name','') AS INT) AS NUM
                                  , GP_KEY_VALUE AS NAME
                              FROM GENERAL_PARAMS 
                              WHERE GP_GROUP_KEY ='PlayerTracking'
                                AND GP_SUBJECT_KEY LIKE 'Level%.Name'
                          ) AS LEVELS ON LEVELS.NUM = (CASE  WHEN (TE.TE_CURRENT_ACCOUNT_ID IS NOT NULL) THEN 
                                                                                                                  CASE WHEN (AC.AC_HOLDER_NAME IS NOT NULL) THEN AC.AC_HOLDER_LEVEL 
                                                                                                                  ELSE -1 END 
                                                                                                            ELSE 0 END)
            
      WHERE LO.LO_TYPE = 1 
        AND LOL.LOL_CURRENT_LOCATION = 1
                       
      -- TERMINAL group
      SELECT * FROM #temp_table
      
      DROP TABLE #temp_table
      
      -- Total occupancy in the site
      SELECT  ISNULL((SUM(OSE_DELTA_IN) - SUM(OSE_DELTA_OUT)), 0) AS TOTAL_OCCUPANCY
      FROM OCCUPANCY_SENSOR

END
RETURN
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_Get_Meters]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_Get_Meters]

GO

CREATE  PROCEDURE [dbo].[SP_Get_Meters] 
(  @Device AS INT
 , @Source AS VARCHAR(1)
 , @Date AS DATETIME
) 
AS
BEGIN
  DECLARE @DATEFROM AS DATETIME
  DECLARE @DATEFROM_WEEK AS DATETIME
  DECLARE @DATEFROM_MONTH AS DATETIME
  DECLARE @DATEFROM_YEAR AS DATETIME
  DECLARE @SEARCH_DATE AS NVARCHAR(MAX)
  DECLARE @QUERY_CONST AS NVARCHAR(MAX)
  DECLARE @QUERY_TEMP AS NVARCHAR(MAX)
  DECLARE @FINAL_QUERY AS NVARCHAR(MAX)
  DECLARE @QUERY AS NVARCHAR(MAX)
  DECLARE @RANGE AS VARCHAR(1)
  DECLARE @DESCRIPTION AS VARCHAR(3)
  DECLARE @HOUR AS VARCHAR(2)
  DECLARE @ORIGIN AS VARCHAR(MAX)
  DECLARE @DOW AS VARCHAR(1)
  DECLARE @LHOUR AS VARCHAR(2)

  SET @FINAL_QUERY = ''
  SET @DATEFROM = @Date

  EXEC SP_Get_Meters_List @DEVICE

  SET @DATEFROM_WEEK = DATEADD(DAY, -7, dbo.GetRangeDate(@DATEFROM , 'W', 1))             -- First day of previous week
  SET @DATEFROM_MONTH = DATEADD(M, -1, DATEADD(M, DATEDIFF(M, 0, @DATEFROM), 0))  -- First day of previous month
  SET @DATEFROM_YEAR = DATEADD(YY, -1, dbo.GetRangeDate(@DATEFROM , 'Y',1)) -- First day of previous year

  SET @DOW = LTRIM(DATEPART(WEEKDAY, @DATEFROM))
  SET @HOUR = DATEPART(HOUR, GETDATE())
  SET @HOUR = REPLICATE('0',2-LEN(RTRIM(@HOUR))) + RTRIM(@HOUR) 

  SET @LHOUR = DATEPART(HOUR, DATEADD(HOUR,-1,GETDATE()))
  SET @LHOUR = REPLICATE('0',2-LEN(RTRIM(@LHOUR))) + RTRIM(@LHOUR) 
  
  SET @QUERY_CONST = ' SELECT ''@DESCRIPTION'' AS RANGE, @HOUR as HOUR, x2d_date, x2d_weekday, x2d_id, x2d_meter_id, x2d_meter_item,'
                   + ' x2d_@HOUR_min AS MIN, x2d_@HOUR_max as MAX, x2d_@HOUR_acc AS ACC, x2d_@HOUR_avg AS AVG, x2d_@HOUR_num AS NUM,'
                   + ' x2d_dd_min AS DAY_MIN, x2d_dd_max AS DAY_MAX, x2d_dd_acc AS DAY_ACC, x2d_dd_avg AS DAY_AVG, x2d_dd_num AS DAY_NUM'
                   + ' FROM  H_@RANGE2D_@SOURCEMH'
                   + ' INNER JOIN H_METERS_DEFINITION ON x2d_meter_id = hmd_meter_id'
                   + ' WHERE hmd_visible = 1 AND @DATE '
                

  -- TDA & SLW
  SET @QUERY_TEMP = @QUERY_CONST
  SET @RANGE = 'T'
  SET @DESCRIPTION = 'TDA'
  SET @SEARCH_DATE = 'x2d_date >= ' + CONVERT(VARCHAR(10), DATEADD(DAY, -7, @DATEFROM), 112) + ' AND x2d_date <= ' + CONVERT(VARCHAR(10), @DATEFROM, 112)
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DATE', @SEARCH_DATE)
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@HOUR', LTRIM(@HOUR))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@RANGE', LTRIM(@RANGE))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DESCRIPTION', LTRIM(@DESCRIPTION))
  SET @FINAL_QUERY = @QUERY_TEMP + ' AND x2d_weekday = ' + @DOW


  SET @QUERY_TEMP = @QUERY_CONST
  SET @RANGE = 'T'
  SET @DESCRIPTION = 'TDLH'
  
  IF dbo.TodayOpening(1) <> @DATEFROM
  BEGIN
    SET @SEARCH_DATE = 'x2d_date = ' + CONVERT(VARCHAR(10), @DATEFROM, 112)
    SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DATE', @SEARCH_DATE)
    SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@HOUR', LTRIM(@LHOUR))
    SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@RANGE', LTRIM(@RANGE))
    SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DESCRIPTION', LTRIM(@DESCRIPTION))
    SET @FINAL_QUERY = @FINAL_QUERY
                     + ' UNION '
                     + @QUERY_TEMP 
  END

 -- YESTERDAY new block with the query on the days table with the meters from yesterday
  SET @QUERY_TEMP = @QUERY_CONST
  SET @RANGE = 'T'
  SET @DESCRIPTION = 'TDY'
  SET @SEARCH_DATE = 'x2d_date = ' + CONVERT(VARCHAR(10), DATEADD(DAY, -1, @DATEFROM), 112)
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DATE', @SEARCH_DATE)
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@HOUR', LTRIM(@HOUR))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@RANGE', LTRIM(@RANGE))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DESCRIPTION', LTRIM(@DESCRIPTION))
  SET @FINAL_QUERY = @FINAL_QUERY
                   + ' UNION '
                   + @QUERY_TEMP 

  -- SAME DAY LAST WEEK new block with the query on the days table with the meters from the same day of last week
  SET @QUERY_TEMP = @QUERY_CONST
  SET @RANGE = 'T'
  SET @DESCRIPTION = 'TDW'
  SET @SEARCH_DATE = 'x2d_date = ' + CONVERT(VARCHAR(10), DATEADD(DAY, -7, @DATEFROM), 112)
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DATE', @SEARCH_DATE)
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@HOUR', LTRIM(@HOUR))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@RANGE', LTRIM(@RANGE))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DESCRIPTION', LTRIM(@DESCRIPTION))
  SET @FINAL_QUERY = @FINAL_QUERY
                   + ' UNION '
                   + @QUERY_TEMP 

  -- WTD
  SET @QUERY_TEMP = @QUERY_CONST
  SET @RANGE = 'W'
  SET @DESCRIPTION = 'WTD'
  SET @SEARCH_DATE = 'x2d_date = ' + CAST(CONVERT(VARCHAR(10), @DATEFROM_WEEK, 112) AS VARCHAR)
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DATE', LTRIM(@SEARCH_DATE))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@HOUR', LTRIM(@HOUR))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@RANGE', LTRIM(@RANGE))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DESCRIPTION', LTRIM(@DESCRIPTION))
  --EXECUTE sp_executesql @QUERY
  SET @FINAL_QUERY = @FINAL_QUERY
                   + ' UNION '
                   + @QUERY_TEMP + ' AND x2d_weekday = ' + @DOW
                  

  -- MTD
  SET @QUERY_TEMP = @QUERY_CONST
  SET @RANGE = 'M'
  SET @DESCRIPTION = 'MTD'
  SET @SEARCH_DATE = 'x2d_date = ' + CAST(CONVERT(VARCHAR(10), @DATEFROM_MONTH, 112) AS VARCHAR)
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DATE', LTRIM(@SEARCH_DATE))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@HOUR', LTRIM(@HOUR))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@RANGE', LTRIM(@RANGE))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DESCRIPTION', LTRIM(@DESCRIPTION))
  SET @FINAL_QUERY = @FINAL_QUERY
                   + ' UNION '
                   + @QUERY_TEMP + ' AND x2d_weekday = ' + @DOW
                   

  -- YTD
  SET @QUERY_TEMP = @QUERY_CONST
  SET @RANGE = 'Y'
  SET @DESCRIPTION = 'YTD'
  SET @SEARCH_DATE = 'x2d_date = ' + CAST(CONVERT(VARCHAR(10), @DATEFROM_YEAR, 112) AS VARCHAR)
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DATE', LTRIM(@SEARCH_DATE))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@HOUR', LTRIM(@HOUR))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@RANGE', LTRIM(@RANGE))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DESCRIPTION', LTRIM(@DESCRIPTION))
  SET @FINAL_QUERY = @FINAL_QUERY
                   + ' UNION '
                   + @QUERY_TEMP + ' AND x2d_weekday = ' + @DOW
                  

  SET @QUERY = REPLACE(@FINAL_QUERY, '@SOURCE', @Source)
           print @QUERY  
  EXECUTE sp_executesql @QUERY
  
  --WEATHER METERS-------
  --LAST 7 DAYS PROMOTIONS,OCCUPANCY AND WEATHER
  SELECT TOP 7H_T2D_SMH.X2D_DATE AS DATE
           ,  H_T2D_SMH.X2D_DD_NUM AS PROMOTIONS
           ,  O.X2D_DD_AVG AS OCCUPANCY
           ,  W.LWD_WEATHER_CODE AS WEATHER
           ,  H_T2D_SMH.X2D_WEEKDAY
        FROM H_T2D_SMH
        
        LEFT JOIN (
          SELECT TOP 7 X2D_DATE, X2D_DD_AVG
                FROM H_T2D_SMH
               WHERE X2D_METER_ID = 4 AND X2D_METER_ITEM = 0
                   ) O ON O.X2D_DATE = H_T2D_SMH.X2D_DATE
       INNER JOIN (
          SELECT TOP 7LWD_DATE,LWD_WEATHER_CODE 
                FROM LAYOUT_WEATHER_DIARY
                   ) W ON H_T2D_SMH.X2D_DATE = W.LWD_DATE
       WHERE H_T2D_SMH.X2D_METER_ID = 8 AND H_T2D_SMH.X2D_METER_ITEM = 2
    ORDER BY H_T2D_SMH.X2D_DATE DESC
END

GO


IF EXISTS (SELECT * FROM sys.tables WHERE type = 'U' AND name = 'LAYOUT_DASHBOARDS')
BEGIN
	UPDATE [dbo].[layout_dashboards] SET [ld_dashboard] = N'[null,{"id":"widgetHRList","x":0,"y":0,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgetTestList2","x":1,"y":0,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgetRunnersList","x":2,"y":0,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgetTasks1","x":3,"y":0,"width":1,"height":6,"type":"WidgetBase","canResize":false,"canMove":true},{"id":"widgetVipsList","x":0,"y":6,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgetChart2","x":1,"y":6,"width":1,"height":6,"type":"WidgetSVGChart","canResize":false,"canMove":true},{"id":"widgetBirthday","x":2,"y":6,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true}]' WHERE [ld_role_id]	= 1;
	UPDATE [dbo].[layout_dashboards] SET [ld_dashboard] = N'[null,{"id":"widgetIndicator3","x":0,"y":0,"width":1,"height":6,"type":"WidgetBase","canResize":false,"canMove":true},{"id":"widgettheater2","x":1,"y":0,"width":1,"height":6,"type":"WidgetSVGAmphitheater","canResize":false,"canMove":true},{"id":"widgetTestList2","x":2,"y":12,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgetMachineOccupation","x":0,"y":6,"width":1,"height":6,"type":"WidgetSVGDonutChart","canResize":false,"canMove":true},{"id":"widgetVipsList","x":1,"y":18,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgetHRList","x":0,"y":18,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgetPlayersMaxLevel","x":3,"y":18,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgetBirthday","x":2,"y":18,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgettheater3","x":2,"y":0,"width":1,"height":6,"type":"WidgetSVGAmphitheater","canResize":false,"canMove":true},{"id":"widgetOccupancy1","x":3,"y":0,"width":1,"height":6,"type":"WidgetSVGAmphitheater","canResize":false,"canMove":true},{"id":"widgetLevelSegmentation","x":0,"y":12,"width":1,"height":6,"type":"WidgetSVGDonutChart","canResize":false,"canMove":true},{"id":"widgetTasks1","x":3,"y":12,"width":1,"height":6,"type":"WidgetBase","canResize":false,"canMove":true},{"id":"widgetAgeSegmentation","x":1,"y":6,"width":1,"height":6,"type":"WidgetSVGDonutChart","canResize":false,"canMove":true},{"id":"widgetChart2","x":2,"y":6,"width":1,"height":6,"type":"WidgetSVGChart","canResize":false,"canMove":true},{"id":"widgetDonut1","x":1,"y":12,"width":1,"height":6,"type":"WidgetSVGDonutChart","canResize":false,"canMove":true},{"id":"widgetRunnersList","x":3,"y":6,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgetRoomOcc","x":0,"y":24,"width":1,"height":6,"type":"WidgetSVGDonutChart","canResize":false,"canMove":true},{"id":"widgetMachineOccRoomOcc","x":1,"y":24,"width":1,"height":6,"type":"WidgetSVGDonutChart","canResize":false,"canMove":true}]' WHERE [ld_role_id]   = 2;
	UPDATE [dbo].[layout_dashboards] SET [ld_dashboard] = N'[null,{"id":"widgetRunnersList","x":0,"y":0,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgetTasks1","x":1,"y":0,"width":1,"height":6,"type":"WidgetBase","canResize":false,"canMove":true},{"id":"widgetChart2","x":2,"y":0,"width":1,"height":6,"type":"WidgetSVGChart","canResize":false,"canMove":true}]' WHERE [ld_role_id]	= 4;
	UPDATE [dbo].[layout_dashboards] SET [ld_dashboard] = N'[null,{"id":"widgetRunnersList","x":0,"y":0,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgetTasks1","x":1,"y":0,"width":1,"height":6,"type":"WidgetBase","canResize":false,"canMove":true},{"id":"widgetChart2","x":2,"y":0,"width":1,"height":6,"type":"WidgetSVGChart","canResize":false,"canMove":true}]' WHERE [ld_role_id]	= 8;
END
GO

-- Clean widgets to all users
UPDATE LAYOUT_USERS_CONFIGURATION SET LC_DASHBOARD = NULL
GO


IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Intellia' AND GP_SUBJECT_KEY = 'Site.Capacity')
    INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Intellia','Site.Capacity','1')
GO

