﻿USE WGDB_000;

SET ANSI_NULLS ON;

----------------------------------------------------
-----------------VERSION CONTROL--------------------
----------------------------------------------------

DECLARE 
@Exp_SmartFloor_ClientId int, 
@Exp_SmartFloor_CommonBuildId int,
@Exp_SmartFloor_ClientBuildId int,
@New_SmartFloor_ReleaseId int,
@New_SmartFloor_ScriptName nvarchar(50),
@New_SmartFloor_Description nvarchar(1000);

SET @Exp_SmartFloor_ClientId = 18;
SET @Exp_SmartFloor_CommonBuildId = 100;
SET @Exp_SmartFloor_ClientBuildId = 1;
SET @New_SmartFloor_ReleaseId = 25;
SET @New_SmartFloor_ScriptName = N'UpdateTo_SmartFloor_18.025.sql';
SET @New_SmartFloor_Description = N'Patch 18.025 - 24';

IF EXISTS ( SELECT 1 FROM DB_VERSION_SMARTFLOOR )
 DELETE FROM DB_VERSION_SMARTFLOOR;

INSERT INTO [dbo].[db_version_smartfloor]
           ([db_client_id]
           ,[db_common_build_id]
           ,[db_client_build_id]
           ,[db_release_id]
           ,[db_updated_script]
           ,[db_updated]
           ,[db_description])
     VALUES
           (@Exp_SmartFloor_ClientId
           ,@Exp_SmartFloor_CommonBuildId
           ,@Exp_SmartFloor_ClientBuildId
           ,@New_SmartFloor_ReleaseId
           ,@New_SmartFloor_ScriptName
           ,Getdate()
           ,@New_SmartFloor_Description);

          

----------------------------------------------------
----------------------TABLES------------------------
----------------------------------------------------


----------------------------------------------------
----------------------DATA--------------------------
----------------------------------------------------

----------------------------------------------------
----------------------STOREDS-----------------------
----------------------------------------------------

/****** Object:  StoredProcedure [dbo].[Layout_GetPlayerInfo]    Script Date: 13/03/2017 10:29:16 ******/
DROP PROCEDURE [dbo].[Layout_GetPlayerInfo]
GO

/****** Object:  StoredProcedure [dbo].[Layout_GetPlayerInfo]    Script Date: 13/03/2017 10:29:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Layout_GetPlayerInfo]
      @AccountId INT
      ,@Date DATETIME
AS
BEGIN
  DECLARE @NLastVisits INT
  SET @NLastVisits = 3

  SELECT T.PS_PLAYED_COUNT
       , T.PS_PLAYED_AMOUNT
       ,(T.PS_PLAYED_AMOUNT/T.PS_PLAYED_COUNT) AS BET_AVG
       , T.PS_WON_AMOUNT
       ,(T.PS_WON_AMOUNT-T.PS_PLAYED_AMOUNT) AS PLAYED_NET_WIN
       , PS_CASH_IN
       , PS_CASH_OUT
  FROM 
  (
    SELECT SUM(PS_PLAYED_COUNT) AS PS_PLAYED_COUNT
        , SUM(PS_PLAYED_AMOUNT) AS PS_PLAYED_AMOUNT
        , SUM(PS_WON_AMOUNT) AS PS_WON_AMOUNT
        , SUM(PS_TOTAL_CASH_IN) AS PS_CASH_IN
        , SUM(PS_TOTAL_CASH_OUT) AS PS_CASH_OUT
    FROM PLAY_SESSIONS
    WHERE PS_ACCOUNT_ID = @AccountId
    AND PS_FINISHED > @Date
    and PS_PLAYED_COUNT>0
   ) T

  SELECT TOP(@NLastVisits) 
           PVH_TOTAL_PLAYED_COUNT
         , PVH_TOTAL_PLAYED
         , PVH_TOTAL_WON
         , PVH_TOTAL_BET_AVG  
         , PVH_TOTAL_WON-PVH_TOTAL_PLAYED AS PVH_NETWIN
         , PVH_MONEY_IN
         , PVH_MONEY_OUT
      FROM H_PVH
     WHERE PVH_ACCOUNT_ID = @AccountId
  ORDER BY PVH_DATE DESC 
END

GO


/****** Object:  StoredProcedure [dbo].[Layout_GetActivity]    Script Date: 13/03/2017 10:30:53 ******/
DROP PROCEDURE [dbo].[Layout_GetActivity]
GO

/****** Object:  StoredProcedure [dbo].[Layout_GetActivity]    Script Date: 13/03/2017 10:30:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Layout_GetActivity]
AS
BEGIN

  DECLARE @terminal_name_type AS VARCHAR(50)
      DECLARE @big_won_limit AS MONEY
      CREATE TABLE #temp_table    (LO_ID INT
                                             , TERMINAL_ID INT
                                             , TERMINAL_STATUS INT
                                             , PLAY_SESSION_STATUS INT
                                             , IS_ANONYMOUS INT
                                             , PLAYER_ACCOUNT_ID INT
                                             , PLAYER_NAME VARCHAR(50)
                                             , PLAYER_GENDER INT
                                             , PLAYER_AGE INT
                                             , PLAYER_LEVEL INT
                                             , PLAYER_IS_VIP INT
                                             , AL_SAS_HOST_ERROR INT
                                             , AL_DOOR_FLAGS INT 
                                             , AL_BILL_FLAGS INT
                                             , AL_PRINTER_FLAGS INT
                                             , AL_EGM_FLAGS INT
                                             , AL_PLAYED_WON_FLAGS INT
                                             , AL_JACKPOT_FLAGS INT
                                             , AL_STACKER_STATUS INT
                                             , AL_CALL_ATTENDANT_FLAGS INT
                                             , AL_MACHINE_FLAGS INT           
                                             , AL_BIG_WON BIT           
                                             , PLAYED_AMOUNT MONEY
                                             , WON_AMOUNT MONEY
                                             , PLAY_SESSION_BET_AVERAGE MONEY
                                             , TERMINAL_BET_AVERAGE MONEY
                                             , PLAY_SESSION_DURATION INT
                                             , PLAY_SESSION_TOTAL_PLAYED MONEY
                                             , PLAY_SESSION_PLAYED_COUNT INT
                                             , TERMINAL_BANK_ID INT
                                             , TERMINAL_AREA_ID INT
                                             , TERMINAL_PROVIDER_NAME VARCHAR(50)
                                             , PLAY_SESSION_WON_AMOUNT MONEY
                                             , NET_WIN MONEY
                                             , PLAY_SESSION_NET_WIN MONEY
                                             , TERMINAL_NAME NVARCHAR(50)
                                             , TERMINAL_PLAYED_COUNT INT
                                             , PLAYER_TRACKDATA NVARCHAR(50)
                                             , PLAYER_LEVEL_NAME NVARCHAR(50)
                                             , ALARMS NVARCHAR(MAX)
                                             , TERMINAL_DENOMINATION MONEY
                                             , PLAYER_CREATED INT
                                             , PS_rate float  -- Test field
                                             , TERMINAL_PAYOUT MONEY
                                             , THEORETICAL_HOLD MONEY
                                             , AC_HOLDER_BIRTH_DATE DATETIME
                                             , PLAY_SESSION_CASH_IN MONEY
                                             , PLAY_SESSION_CASH_OUT MONEY
											 , PLAYER_FIRST_NAME VARCHAR(50)
											 , PLAYER_LAST_NAME1 VARCHAR(50)
											 , PLAYER_LAST_NAME2 VARCHAR(50)
                                             )
                                             
      -- TERMINAL NAME TYPE
      SELECT @terminal_name_type = GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'SasHost' AND GP_SUBJECT_KEY = 'DisplayName.Format'
      IF (@terminal_name_type IS NULL)
      BEGIN
        SET @terminal_name_type = '%Name'
      END

      -- TODO: Read from GP
      SET @big_won_limit = 1000

  DECLARE @Now AS DATETIME
  DECLARE @TodayOpening AS DATETIME
  DECLARE @CurrentHour AS INT
  DECLARE @HourNow AS DATETIME
  DECLARE @NextHourNow AS DATETIME
  
  SET @Now = GETDATE()
  SET @TodayOpening = DBO.TODAYOPENING(0)
  SET @CurrentHour = DATEPART(HOUR, @Now)
  SET @HourNow = DATEADD(HOUR, DATEDIFF(HOUR, 0, @Now), 0)
  SET @NextHourNow = DATEADD(HOUR, 1, @HourNow)

  DECLARE @DefaultHold MONEY
  SET @DefaultHold = (SELECT   1 - CAST (GP_KEY_VALUE AS MONEY) / 100        
                        FROM   GENERAL_PARAMS                                
                       WHERE   GP_GROUP_KEY   = 'PlayerTracking'               
                         AND   GP_SUBJECT_KEY = 'TerminalDefaultPayout')

      INSERT INTO #temp_table
      SELECT LO.LO_ID
      
            -- Terminal info
            , LO.LO_EXTERNAL_ID AS TERMINAL_ID
            , TE.TE_STATUS   AS TERMINAL_STATUS

            -- Play Session info
            , PLAY_SESSION_STATUS = CASE  WHEN (PS_PLAY_SESSION_ID IS NOT NULL) THEN 2  
                                                 ELSE 1 END

            -- Player info
            , IS_ANONYMOUS = CASE  WHEN (PS.PS_ACCOUNT_ID IS NOT NULL) THEN CASE WHEN (AC.AC_HOLDER_NAME IS NOT NULL) THEN 0 ELSE 1 END 
                                                ELSE NULL END
            , PS.PS_ACCOUNT_ID AS PLAYER_ACCOUNT_ID
            , PLAYER_NAME = CASE WHEN (PS.PS_ACCOUNT_ID IS NOT NULL) THEN ISNULL(AC.AC_HOLDER_NAME, 'Anonymous')
                                          ELSE NULL END
            , PLAYER_GENDER = CASE WHEN (PS.PS_ACCOUNT_ID IS NOT NULL) THEN ISNULL(AC.AC_HOLDER_GENDER,0)
						ELSE NULL END
            , ISNULL(DATEDIFF(YEAR, AC.AC_HOLDER_BIRTH_DATE, @Now),-1) AS PLAYER_AGE
            , PLAYER_LEVEL = CASE  WHEN (PS.PS_ACCOUNT_ID IS NOT NULL) THEN CASE WHEN (AC.AC_HOLDER_NAME IS NOT NULL) THEN AC.AC_HOLDER_LEVEL ELSE -1 END 
                                                ELSE 99 END
            , ISNULL(AC.AC_HOLDER_IS_VIP, 0) AS PLAYER_IS_VIP
            
             -- Flags
            , TS_SAS_HOST_ERROR AS AL_SAS_HOST_ERROR
            , ISNULL(TS_DOOR_FLAGS, 0) AS AL_DOOR_FLAGS 
             , ISNULL(TS_BILL_FLAGS, 0) AS AL_BILL_FLAGS
            , ISNULL(TS_PRINTER_FLAGS, 0) AS AL_PRINTER_FLAGS
            , ISNULL(TS_EGM_FLAGS, 0) AS AL_EGM_FLAGS
            , ISNULL(TS_PLAYED_WON_FLAGS, 0) AS AL_PLAYED_WON_FLAGS
            , ISNULL(TS_JACKPOT_FLAGS, 0) AS AL_JACKPOT_FLAGS
            , ISNULL(TS_STACKER_STATUS, 0) AS AL_STACKER_STATUS
            , ISNULL(TS_CALL_ATTENDANT_FLAGS, 0) AS AL_CALL_ATTENDANT_FLAGS
            , ISNULL(TS_MACHINE_FLAGS, 0) AS AL_MACHINE_FLAGS          
            , AL_BIG_WON = CASE WHEN ISNULL(SPH.WON_AMOUNT, 0) > @big_won_limit THEN 1
                                     ELSE 0 END
                                     
             -- Money info
            , ISNULL(SPH.PLAYED_AMOUNT , 0) AS PLAYED_AMOUNT
            , ISNULL(SPH.WON_AMOUNT, 0) AS WON_AMOUNT
            , PLAY_SESSION_BET_AVERAGE = CASE WHEN (PS.PS_PLAYED_COUNT > 0) THEN ROUND((PS.PS_TOTAL_PLAYED / PS.PS_PLAYED_COUNT), 2)
                                                               ELSE NULL END
            , TERMINAL_BET_AVERAGE = CASE WHEN (SPH.PLAYED_COUNT > 0) THEN ROUND((SPH.PLAYED_AMOUNT / SPH.PLAYED_COUNT), 2)
                                                         ELSE NULL END
            , DATEDIFF(SECOND, ISNULL(PS.PS_STARTED, @Now), @Now) AS PLAY_SESSION_DURATION
            , PS.PS_TOTAL_PLAYED AS PLAY_SESSION_TOTAL_PLAYED
            , PS.PS_PLAYED_COUNT AS PLAY_SESSION_PLAYED_COUNT
            
             -- Area & Bank
            , BK.bk_bank_id AS TERMINAL_BANK_ID
            , BK.bk_area_id AS TERMINAL_AREA_ID
            , PR.pv_name AS TERMINAL_PROVIDER_NAME
            
             , PS.PS_WON_AMOUNT AS PLAY_SESSION_WON_AMOUNT
            
             , SPH.PLAYED_AMOUNT - SPH.WON_AMOUNT AS NET_WIN
            , PS.PS_TOTAL_PLAYED - PS.PS_WON_AMOUNT AS PLAY_SESSION_NET_WIN
            --, TE.TE_NAME AS TERMINAL_NAME
                 , CASE WHEN @terminal_name_type = '%FloorId' THEN
                      TE.TE_NAME + ' [' + TE.TE_FLOOR_ID + ']'
                    WHEN @terminal_name_type = '%BaseName' THEN
                      -- TODO: Find correct field
                      -- TE.TE_BASE_NAME
                      TE.TE_NAME
                    ELSE 
                      TE.TE_NAME
               END AS TERMINAL_NAME
            , SPH.PLAYED_COUNT AS TERMINAL_PLAYED_COUNT
            , AC.AC_TRACK_DATA AS PLAYER_TRACKDATA
            , ISNULL(LEVELS.NAME,'') AS PLAYER_LEVEL_NAME
     , NULL AS ALARMS
     , TE.TE_DENOMINATION AS TERMINAL_DENOMINATION
     , CASE WHEN (AC.AC_CREATED >= @HourNow AND AC.AC_CREATED < @NextHourNow) THEN 1 ELSE 0 END AS PLAYER_CREATED
     , NULL AS PS_RATE
     , ISNULL(TE.TE_THEORETICAL_HOLD, @DefaultHold) AS TERMINAL_PAYOUT
     , (PS.PS_TOTAL_PLAYED * ISNULL(TE.TE_THEORETICAL_HOLD, @DefaultHold)) AS THEORETICAL_HOLD
     , AC.AC_HOLDER_BIRTH_DATE
     , PS.PS_TOTAL_CASH_IN
     , PS.PS_TOTAL_CASH_OUT
	 , PLAYER_FIRST_NAME  = CASE WHEN (TE.TE_CURRENT_ACCOUNT_ID IS NOT NULL) THEN ISNULL(AC.AC_HOLDER_NAME3, 'Anonymous')
                                          ELSE NULL END
	 , PLAYER_LAST_NAME1  = CASE WHEN (TE.TE_CURRENT_ACCOUNT_ID IS NOT NULL) THEN ISNULL(AC.AC_HOLDER_NAME1, 'Anonymous')
                                          ELSE NULL END
	 , PLAYER_LAST_NAME2  = CASE WHEN (TE.TE_CURRENT_ACCOUNT_ID IS NOT NULL) THEN ISNULL(AC.AC_HOLDER_NAME2, 'Anonymous')
                                          ELSE NULL END
        FROM LAYOUT_OBJECTS AS LO
            INNER JOIN LAYOUT_OBJECT_LOCATIONS AS LOL ON LO.LO_ID = LOL.LOL_OBJECT_ID
            INNER JOIN TERMINALS AS TE ON TE.TE_TERMINAL_ID = LO.LO_EXTERNAL_ID 
            INNER JOIN PROVIDERS AS PR ON PR.pv_id = TE.te_prov_id
            
            LEFT JOIN BANKS AS BK ON BK.bk_bank_id = TE.te_bank_id
                        
            LEFT JOIN (SELECT PS_TERMINAL_ID
                            , MAX(PS_PLAY_SESSION_ID) AS PS_PLAY_SESSION_ID
                            , PS_ACCOUNT_ID
                            , PS_TOTAL_PLAYED
                            , PS_WON_AMOUNT
                            , PS_PLAYED_COUNT
                            , PS_STARTED
                            , PS_PLAYED_AMOUNT
                            , PS_TOTAL_CASH_IN
                            , PS_TOTAL_CASH_OUT
                             FROM PLAY_SESSIONS
                            WHERE PS_STATUS = 0
                        GROUP BY PS_TERMINAL_ID, PS_ACCOUNT_ID, PS_TOTAL_PLAYED, PS_WON_AMOUNT, PS_PLAYED_COUNT, PS_PLAYED_AMOUNT, PS_STARTED,PS_TOTAL_CASH_IN,PS_TOTAL_CASH_OUT) AS PS ON TE.TE_TERMINAL_ID = PS.PS_TERMINAL_ID
                        
            LEFT JOIN ACCOUNTS AS AC ON AC.AC_ACCOUNT_ID = PS.PS_ACCOUNT_ID
            
            LEFT JOIN TERMINAL_STATUS AS TS ON TE.TE_TERMINAL_ID = TS.TS_TERMINAL_ID
            
            LEFT JOIN (SELECT SPH_TERMINAL_ID 
                                  , SUM(SPH_PLAYED_AMOUNT)AS PLAYED_AMOUNT
                                  , SUM(SPH_WON_AMOUNT) AS WON_AMOUNT
                                  , SUM(SPH_PLAYED_COUNT) AS PLAYED_COUNT
                         FROM SALES_PER_HOUR_V2
                        WHERE SPH_BASE_HOUR >= @TodayOpening
                       GROUP BY SPH_TERMINAL_ID) AS SPH ON SPH.SPH_TERMINAL_ID = TE.TE_TERMINAL_ID
                       
            LEFT JOIN (SELECT CAST(REPLACE(REPLACE(GP_SUBJECT_KEY,'Level',''),'.Name','') AS INT) AS NUM
                                  , GP_KEY_VALUE AS NAME
                              FROM GENERAL_PARAMS 
                              WHERE GP_GROUP_KEY ='PlayerTracking'
                                AND GP_SUBJECT_KEY LIKE 'Level%.Name'
                          ) AS LEVELS ON LEVELS.NUM = (CASE  WHEN (TE.TE_CURRENT_ACCOUNT_ID IS NOT NULL) THEN 
                                                                                                                  CASE WHEN (AC.AC_HOLDER_NAME IS NOT NULL) THEN AC.AC_HOLDER_LEVEL 
                                                                                                                  ELSE -1 END 
                                                                                                            ELSE 0 END)
            
      WHERE LO.LO_TYPE = 1 
        AND LOL.LOL_CURRENT_LOCATION = 1
                       
      -- TERMINAL group
      SELECT * FROM #temp_table
      
      DROP TABLE #temp_table
      
      -- Total occupancy in the site
      SELECT  ISNULL((SUM(OSE_DELTA_IN) - SUM(OSE_DELTA_OUT)), 0) AS TOTAL_OCCUPANCY
      FROM OCCUPANCY_SENSOR

END
RETURN


GO



