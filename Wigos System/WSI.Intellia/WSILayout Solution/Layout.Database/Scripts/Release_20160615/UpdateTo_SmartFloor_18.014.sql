USE WGDB_000;

SET ANSI_NULLS ON;

----------------------------------------------------
-----------------VERSION CONTROL--------------------
----------------------------------------------------

DECLARE 
@Exp_SmartFloor_ClientId int, 
@Exp_SmartFloor_CommonBuildId int,
@Exp_SmartFloor_ClientBuildId int,
@New_SmartFloor_ReleaseId int,
@New_SmartFloor_ScriptName nvarchar(50),
@New_SmartFloor_Description nvarchar(1000);

SET @Exp_SmartFloor_ClientId = 18;
SET @Exp_SmartFloor_CommonBuildId = 100;
SET @Exp_SmartFloor_ClientBuildId = 1;
SET @New_SmartFloor_ReleaseId = 14;
SET @New_SmartFloor_ScriptName = N'UpdateTo_SmartFloor_18.014.sql';
SET @New_SmartFloor_Description = N'Patch 18.014 - 13';

IF EXISTS ( SELECT 1 FROM DB_VERSION_SMARTFLOOR )
 DELETE FROM DB_VERSION_SMARTFLOOR;

INSERT INTO [dbo].[db_version_smartfloor]
           ([db_client_id]
           ,[db_common_build_id]
           ,[db_client_build_id]
           ,[db_release_id]
           ,[db_updated_script]
           ,[db_updated]
           ,[db_description])
     VALUES
           (@Exp_SmartFloor_ClientId
           ,@Exp_SmartFloor_CommonBuildId
           ,@Exp_SmartFloor_ClientBuildId
           ,@New_SmartFloor_ReleaseId
           ,@New_SmartFloor_ScriptName
           ,Getdate()
           ,@New_SmartFloor_Description);

           
----------------------------------------------------
----------------------STOREDS-----------------------
----------------------------------------------------

/****** Object:  StoredProcedure [dbo].[Layout_GetConversationsOfRunner]    Script Date: 19/05/2016 00:08:54 ******/
IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Layout_GetConversationsOfRunner')
BEGIN
	DROP PROCEDURE [dbo].[Layout_GetConversationsOfRunner]
END
ELSE
/****** Object:  StoredProcedure [dbo].[Layout_GetConversationsOfRunner]    Script Date: 19/05/2016 00:08:54 ******/
	SET ANSI_NULLS ON
	GO
	SET QUOTED_IDENTIFIER ON
	GO
	CREATE PROCEDURE [dbo].[Layout_GetConversationsOfRunner]
	@pRunnerId int
WITH EXEC AS CALLER
AS
BEGIN
	SELECT R.LMRM_MANAGER_ID, R.MANAGER_NAME, LAST_MESSAGE, LAST_MESSAGE_DATE FROM (  
      SELECT  LMRM_MANAGER_ID
    	     ,	U_MANAGER.GU_USERNAME as MANAGER_NAME
					 ,  (SELECT TOP 1 LMRM2.LMRM_DATE 
								 FROM LAYOUT_MANAGER_RUNNER_MESSAGES LMRM2 
								WHERE LMRM2.LMRM_MANAGER_ID = LMRM.LMRM_MANAGER_ID 
						 ORDER BY LMRM2.LMRM_DATE DESC) AS LAST_MESSAGE_DATE
           ,	(SELECT TOP 1 LMRM2.LMRM_MESSAGE 
								 FROM LAYOUT_MANAGER_RUNNER_MESSAGES LMRM2 
                WHERE LMRM2.LMRM_MANAGER_ID = LMRM.LMRM_MANAGER_ID 
						 ORDER BY LMRM2.LMRM_DATE DESC) AS LAST_MESSAGE
    	  FROM	LAYOUT_MANAGER_RUNNER_MESSAGES LMRM
  INNER JOIN	GUI_USERS U_MANAGER ON LMRM.LMRM_MANAGER_ID = U_MANAGER.GU_USER_ID 
  INNER JOIN	GUI_USERS U_RUNNER ON LMRM.LMRM_RUNNER_ID = U_RUNNER.GU_USER_ID 
    	 WHERE	LMRM.LMRM_DATE >= CAST(FLOOR(CAST(getdate() AS FLOAT)) AS DATETIME)
    	   AND	LMRM_RUNNER_ID = @pRunnerId
    GROUP BY	LMRM_MANAGER_ID
    		   ,	U_MANAGER.GU_USERNAME
  ) AS R ORDER BY R.LAST_MESSAGE_DATE DESC
END

IF EXISTS (SELECT * FROM sys.tables WHERE type = 'U' AND name = 'LAYOUT_DASHBOARDS')
BEGIN
	UPDATE [dbo].[layout_dashboards] SET [ld_dashboard] = N'[null,{"id":"widgetIndicator3","x":0,"y":0,"width":1,"height":6,"type":"WidgetBase","canResize":false,"canMove":true},{"id":"widgettheater2","x":1,"y":0,"width":1,"height":6,"type":"WidgetSVGAmphitheater","canResize":false,"canMove":true},{"id":"widgetTestList2","x":2,"y":0,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgetMachineOccupation","x":3,"y":0,"width":1,"height":6,"type":"WidgetSVGDonutChart","canResize":false,"canMove":true},{"id":"widgetVipsList","x":0,"y":6,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgetHRList","x":1,"y":6,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgetPlayersMaxLevel","x":2,"y":6,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgetBirthday","x":3,"y":6,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgettheater3","x":0,"y":12,"width":1,"height":6,"type":"WidgetSVGAmphitheater","canResize":false,"canMove":true},{"id":"widgetOccupancy1","x":1,"y":12,"width":1,"height":6,"type":"WidgetSVGAmphitheater","canResize":false,"canMove":true},{"id":"widgetLevelSegmentation","x":3,"y":12,"width":1,"height":6,"type":"WidgetSVGDonutChart","canResize":false,"canMove":true},{"id":"widgetTasks1","x":1,"y":18,"width":1,"height":6,"type":"WidgetBase","canResize":false,"canMove":true},{"id":"widgetAgeSegmentation","x":2,"y":18,"width":1,"height":6,"type":"WidgetSVGDonutChart","canResize":false,"canMove":true},{"id":"widgetChart2","x":3,"y":18,"width":1,"height":6,"type":"WidgetSVGChart","canResize":false,"canMove":true},{"id":"widgetDonut1","x":2,"y":12,"width":1,"height":6,"type":"WidgetSVGDonutChart","canResize":false,"canMove":true},{"id":"widgetRunnersList","x":0,"y":18,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true}]' WHERE [ld_role_id]	= 1;
END
GO

IF EXISTS (SELECT * FROM sys.tables WHERE type = 'U' AND name = 'LAYOUT_USERS_CONFIGURATION')
BEGIN
	UPDATE [dbo].[LAYOUT_USERS_CONFIGURATION] SET [LC_DASHBOARD] = NULL
END
GO

/****** Object:  StoredProcedure [dbo].[Layout_GetFloors]    Script Date: 06/08/2016 10:57:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetFloors]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Layout_GetFloors]
GO

/****** Object:  StoredProcedure [dbo].[Layout_GetFloors]    Script Date: 06/08/2016 10:57:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/*
  Obtain the list of available Layout Floors
*/
--CREATE PROCEDURE [dbo].[Layout_GetFloors]
CREATE PROCEDURE [dbo].[Layout_GetFloors]
AS
BEGIN
SELECT   lf_floor_id
       , lf_name
       , lf_height
       , '' as lf_geometry
       , '' as lf_image_map
       , lf_color
       , lf_width
       , lf_metric
  FROM   layout_floors (nolock)
END -- Layout_GetFloors
GO

/****** Object:  StoredProcedure [dbo].[SP_Get_Meters]    Script Date: 06/14/2016 16:02:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_Get_Meters]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_Get_Meters]
GO

/****** Object:  StoredProcedure [dbo].[SP_Get_Meters]    Script Date: 06/14/2016 16:02:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE  PROCEDURE [dbo].[SP_Get_Meters] 
(  @Device AS INT
 , @Source AS VARCHAR(1)
 , @Date AS DATETIME
) 
AS
BEGIN
  DECLARE @DATEFROM AS DATETIME
  DECLARE @DATEFROM_WEEK AS DATETIME
  DECLARE @DATEFROM_MONTH AS DATETIME
  DECLARE @DATEFROM_YEAR AS DATETIME
  DECLARE @SEARCH_DATE AS NVARCHAR(MAX)
  DECLARE @QUERY_CONST AS NVARCHAR(MAX)
  DECLARE @QUERY_TEMP AS NVARCHAR(MAX)
  DECLARE @FINAL_QUERY AS NVARCHAR(MAX)
  DECLARE @QUERY AS NVARCHAR(MAX)
  DECLARE @RANGE AS VARCHAR(1)
  DECLARE @DESCRIPTION AS VARCHAR(3)
  DECLARE @HOUR AS VARCHAR(2)
  DECLARE @ORIGIN AS VARCHAR(MAX)
  DECLARE @DOW AS VARCHAR(1)

  SET @FINAL_QUERY = ''
  SET @DATEFROM = @Date

  EXEC SP_Get_Meters_List @DEVICE

  SET @DATEFROM_WEEK = DATEADD(WK, DATEDIFF(WK, 0, @DATEFROM) - 1, 0)             -- First day of previous week
  SET @DATEFROM_MONTH = DATEADD(M, -1, DATEADD(M, DATEDIFF(M, 0, @DATEFROM), 0))  -- First day of previous month
  SET @DATEFROM_YEAR = DATEADD(YY, -1, DATEADD(YY, DATEDIFF(YY, 0, @DATEFROM), 0)) -- First day of previous year
  SET @DOW = LTRIM(DATEPART(WEEKDAY, @DATEFROM))

  SET @HOUR = DATEPART(HOUR, GETDATE())
  SET @HOUR = REPLICATE('0',2-LEN(RTRIM(@HOUR))) + RTRIM(@HOUR) 

  SET @QUERY_CONST = ' SELECT ''@DESCRIPTION'' AS RANGE, @HOUR as HOUR, x2d_date, x2d_weekday, x2d_id, x2d_meter_id, x2d_meter_item,'
                   + ' x2d_@HOUR_min AS MIN, x2d_@HOUR_max as MAX, x2d_@HOUR_acc AS ACC, x2d_@HOUR_avg AS AVG, x2d_@HOUR_num AS NUM,'
                   + ' x2d_dd_min AS DAY_MIN, x2d_dd_max AS DAY_MAX, x2d_dd_acc AS DAY_ACC, x2d_dd_avg AS DAY_AVG, x2d_dd_num AS DAY_NUM'
                   + ' FROM  H_@RANGE2D_@SOURCEMH'
                   + ' INNER JOIN H_METERS_DEFINITION ON x2d_meter_id = hmd_meter_id'
                   + ' WHERE hmd_visible = 1'

  -- TDA & SLW
  SET @QUERY_TEMP = @QUERY_CONST
  SET @RANGE = 'T'
  SET @DESCRIPTION = 'TDA'
  SET @SEARCH_DATE = 'x2d_date >= ' + CONVERT(VARCHAR(10), DATEADD(DAY, -7, @DATEFROM), 112) + ' AND x2d_date <= ' + CONVERT(VARCHAR(10), @DATEFROM, 112)
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DATE', @SEARCH_DATE)
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@HOUR', LTRIM(@HOUR))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@RANGE', LTRIM(@RANGE))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DESCRIPTION', LTRIM(@DESCRIPTION))
  SET @FINAL_QUERY = @QUERY_TEMP 




  -- YESTERDAY new block with the query on the days table with the meters from yesterday
  SET @QUERY_TEMP = @QUERY_CONST
  SET @RANGE = 'T'
  SET @DESCRIPTION = 'TDY'
  SET @SEARCH_DATE = 'x2d_date = ' + CONVERT(VARCHAR(10), DATEADD(DAY, -1, @DATEFROM), 112)
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DATE', @SEARCH_DATE)
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@HOUR', LTRIM(@HOUR))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@RANGE', LTRIM(@RANGE))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DESCRIPTION', LTRIM(@DESCRIPTION))
  SET @FINAL_QUERY = @FINAL_QUERY
                   + ' UNION '
                   + @QUERY_TEMP 

  -- WTD
  SET @QUERY_TEMP = @QUERY_CONST
  SET @RANGE = 'W'
  SET @DESCRIPTION = 'WTD'
  SET @SEARCH_DATE = 'x2d_date = ' + CAST(CONVERT(VARCHAR(10), @DATEFROM_WEEK, 112) AS VARCHAR)
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DATE', LTRIM(@SEARCH_DATE))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@HOUR', LTRIM(@HOUR))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@RANGE', LTRIM(@RANGE))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DESCRIPTION', LTRIM(@DESCRIPTION))
  --EXECUTE sp_executesql @QUERY
  SET @FINAL_QUERY = @FINAL_QUERY
                   + ' UNION '
                   + @QUERY_TEMP 
                  

  -- MTD
  SET @QUERY_TEMP = @QUERY_CONST
  SET @RANGE = 'M'
  SET @DESCRIPTION = 'MTD'
  SET @SEARCH_DATE = 'x2d_date = ' + CAST(CONVERT(VARCHAR(10), @DATEFROM_MONTH, 112) AS VARCHAR)
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DATE', LTRIM(@SEARCH_DATE))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@HOUR', LTRIM(@HOUR))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@RANGE', LTRIM(@RANGE))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DESCRIPTION', LTRIM(@DESCRIPTION))
  SET @FINAL_QUERY = @FINAL_QUERY
                   + ' UNION '
                   + @QUERY_TEMP 
                   

  -- YTD
  SET @QUERY_TEMP = @QUERY_CONST
  SET @RANGE = 'Y'
  SET @DESCRIPTION = 'YTD'
  SET @SEARCH_DATE = 'x2d_date = ' + CAST(CONVERT(VARCHAR(10), @DATEFROM_YEAR, 112) AS VARCHAR)
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DATE', LTRIM(@SEARCH_DATE))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@HOUR', LTRIM(@HOUR))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@RANGE', LTRIM(@RANGE))
  SET @QUERY_TEMP = REPLACE(@QUERY_TEMP, '@DESCRIPTION', LTRIM(@DESCRIPTION))
  SET @FINAL_QUERY = @FINAL_QUERY
                   + ' UNION '
                   + @QUERY_TEMP 
                  

  SET @QUERY = REPLACE(@FINAL_QUERY, '@SOURCE', @Source)
           print @QUERY  
  EXECUTE sp_executesql @QUERY
  
  --WEATHER METERS-------
  --LAST 7 DAYS PROMOTIONS,OCCUPANCY AND WEATHER
  SELECT TOP 7H_T2D_SMH.X2D_DATE AS DATE
           ,  H_T2D_SMH.X2D_DD_NUM AS PROMOTIONS
           ,  O.X2D_DD_AVG AS OCCUPANCY
           ,  W.LWD_WEATHER_CODE AS WEATHER
           ,  H_T2D_SMH.X2D_WEEKDAY
        FROM H_T2D_SMH
        
        LEFT JOIN (
          SELECT TOP 7 X2D_DATE, X2D_DD_AVG
                FROM H_T2D_SMH
               WHERE X2D_METER_ID = 4 AND X2D_METER_ITEM = 0
                   ) O ON O.X2D_DATE = H_T2D_SMH.X2D_DATE
       INNER JOIN (
          SELECT TOP 7LWD_DATE,LWD_WEATHER_CODE 
                FROM LAYOUT_WEATHER_DIARY
                   ) W ON H_T2D_SMH.X2D_DATE = W.LWD_DATE
       WHERE H_T2D_SMH.X2D_METER_ID = 8 AND H_T2D_SMH.X2D_METER_ITEM = 2
    ORDER BY H_T2D_SMH.X2D_DATE DESC
END

GO

/****** Object:  StoredProcedure [dbo].[Layout_LoadFloor]    Script Date: 06/17/2016 12:08:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_LoadFloor]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Layout_LoadFloor]
GO

/****** Object:  StoredProcedure [dbo].[Layout_LoadFloor]    Script Date: 06/17/2016 12:08:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Layout_LoadFloor]
(
      @pFloorId INT = NULL   
)      
AS
BEGIN

SELECT   LOC_ID
       , LOC_X
       , LOC_Y
       , LOC_CREATION
       , LOC_TYPE
       , LOC_FLOOR_NUMBER
       , LOC_GRID_ID
       , LOC_STATUS
  FROM   LAYOUT_LOCATIONS       
 WHERE   LOC_FLOOR_NUMBER = @pFloorId

SELECT   LO_ID
       , LO_EXTERNAL_ID
       , LO_TYPE
       , LO_PARENT_ID
       , LO_MESH_ID
       , LO_CREATION_DATE
       , LO_UPDATE_DATE
       , LO_STATUS
       , LOL_OBJECT_ID
       , LOL_LOCATION_ID
       , LOL_DATE_FROM
       , LOL_DATE_TO
       , LOL_ORIENTATION
       , LOL_AREA_ID
       , LOL_BANK_ID
       , LOL_BANK_LOCATION
       , LOL_CURRENT_LOCATION
       , LOL_OFFSET_X
       , LOL_OFFSET_Y
  FROM   LAYOUT_OBJECTS
  LEFT   JOIN LAYOUT_OBJECT_LOCATIONS ON LOL_OBJECT_ID   = LO_ID
  LEFT   JOIN LAYOUT_LOCATIONS        ON LOL_LOCATION_ID = LOC_ID
 WHERE   LOC_FLOOR_NUMBER             = @pFloorId
   AND   LOL_CURRENT_LOCATION         = 1
   
SELECT LF_IMAGE_MAP
  FROM LAYOUT_FLOORS
 WHERE LF_FLOOR_ID = @pFloorId

END --Layout_GetObjectInfo
GO

IF EXISTS (SELECT * FROM sys.tables WHERE type = 'U' AND name = 'LAYOUT_DASHBOARDS')
BEGIN
	UPDATE [dbo].[layout_dashboards] SET [ld_dashboard] = N'[null,{"id":"widgetHRList","x":0,"y":0,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgetTestList2","x":1,"y":0,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgetRunnersList","x":2,"y":0,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgetTasks1","x":3,"y":0,"width":1,"height":6,"type":"WidgetBase","canResize":false,"canMove":true},{"id":"widgetVipsList","x":0,"y":6,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgetChart2","x":1,"y":6,"width":1,"height":6,"type":"WidgetSVGChart","canResize":false,"canMove":true},{"id":"widgetBirthday","x":2,"y":6,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true}]' WHERE [ld_role_id]	= 1;
	UPDATE [dbo].[layout_dashboards] SET [ld_dashboard] = N'[null,{"id":"widgetIndicator3","x":0,"y":0,"width":1,"height":6,"type":"WidgetBase","canResize":false,"canMove":true},{"id":"widgettheater2","x":1,"y":0,"width":1,"height":6,"type":"WidgetSVGAmphitheater","canResize":false,"canMove":true},{"id":"widgetTestList2","x":2,"y":12,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgetMachineOccupation","x":0,"y":6,"width":1,"height":6,"type":"WidgetSVGDonutChart","canResize":false,"canMove":true},{"id":"widgetVipsList","x":1,"y":18,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgetHRList","x":0,"y":18,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgetPlayersMaxLevel","x":3,"y":18,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgetBirthday","x":2,"y":18,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgettheater3","x":2,"y":0,"width":1,"height":6,"type":"WidgetSVGAmphitheater","canResize":false,"canMove":true},{"id":"widgetOccupancy1","x":3,"y":0,"width":1,"height":6,"type":"WidgetSVGAmphitheater","canResize":false,"canMove":true},{"id":"widgetLevelSegmentation","x":0,"y":12,"width":1,"height":6,"type":"WidgetSVGDonutChart","canResize":false,"canMove":true},{"id":"widgetTasks1","x":3,"y":12,"width":1,"height":6,"type":"WidgetBase","canResize":false,"canMove":true},{"id":"widgetAgeSegmentation","x":1,"y":6,"width":1,"height":6,"type":"WidgetSVGDonutChart","canResize":false,"canMove":true},{"id":"widgetChart2","x":2,"y":6,"width":1,"height":6,"type":"WidgetSVGChart","canResize":false,"canMove":true},{"id":"widgetDonut1","x":1,"y":12,"width":1,"height":6,"type":"WidgetSVGDonutChart","canResize":false,"canMove":true},{"id":"widgetRunnersList","x":3,"y":6,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true}]' WHERE [ld_role_id]   = 2;
	UPDATE [dbo].[layout_dashboards] SET [ld_dashboard] = N'[null,{"id":"widgetRunnersList","x":0,"y":0,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgetTasks1","x":1,"y":0,"width":1,"height":6,"type":"WidgetBase","canResize":false,"canMove":true},{"id":"widgetChart2","x":2,"y":0,"width":1,"height":6,"type":"WidgetSVGChart","canResize":false,"canMove":true}]' WHERE [ld_role_id]	= 4;
	UPDATE [dbo].[layout_dashboards] SET [ld_dashboard] = N'[null,{"id":"widgetRunnersList","x":0,"y":0,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgetTasks1","x":1,"y":0,"width":1,"height":6,"type":"WidgetBase","canResize":false,"canMove":true},{"id":"widgetChart2","x":2,"y":0,"width":1,"height":6,"type":"WidgetSVGChart","canResize":false,"canMove":true}]' WHERE [ld_role_id]	= 8;
END
GO
IF EXISTS (SELECT * FROM sys.tables WHERE type = 'U' AND name = 'LAYOUT_USERS_CONFIGURATION')
BEGIN
	UPDATE [dbo].[LAYOUT_USERS_CONFIGURATION] SET [LC_DASHBOARD] = NULL
END

GO
	UPDATE GENERAL_PARAMS
		 SET GP_GROUP_KEY = 'Intellia'
	 WHERE GP_GROUP_KEY = 'SmartFloor'
GO




