USE WGDB_000;

SET ANSI_NULLS ON;

----------------------------------------------------
-----------------VERSION CONTROL--------------------
----------------------------------------------------

DECLARE 
@Exp_SmartFloor_ClientId int, 
@Exp_SmartFloor_CommonBuildId int,
@Exp_SmartFloor_ClientBuildId int,
@New_SmartFloor_ReleaseId int,
@New_SmartFloor_ScriptName nvarchar(50),
@New_SmartFloor_Description nvarchar(1000);

SET @Exp_SmartFloor_ClientId = 18;
SET @Exp_SmartFloor_CommonBuildId = 100;
SET @Exp_SmartFloor_ClientBuildId = 1;
SET @New_SmartFloor_ReleaseId = 16;
SET @New_SmartFloor_ScriptName = N'UpdateTo_SmartFloor_18.016.sql';
SET @New_SmartFloor_Description = N'Patch 18.016 - 15';

IF EXISTS ( SELECT 1 FROM DB_VERSION_SMARTFLOOR )
 DELETE FROM DB_VERSION_SMARTFLOOR;

INSERT INTO [dbo].[db_version_smartfloor]
           ([db_client_id]
           ,[db_common_build_id]
           ,[db_client_build_id]
           ,[db_release_id]
           ,[db_updated_script]
           ,[db_updated]
           ,[db_description])
     VALUES
           (@Exp_SmartFloor_ClientId
           ,@Exp_SmartFloor_CommonBuildId
           ,@Exp_SmartFloor_ClientBuildId
           ,@New_SmartFloor_ReleaseId
           ,@New_SmartFloor_ScriptName
           ,Getdate()
           ,@New_SmartFloor_Description);

           
----------------------------------------------------
----------------------STOREDS-----------------------
----------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetActivity]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[Layout_GetActivity]

GO

/****** Object:  StoredProcedure [dbo].[Layout_GetActivity]    Script Date: 07/20/2016 16:43:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Layout_GetActivity]
AS
BEGIN

  DECLARE @terminal_name_type AS VARCHAR(50)
      DECLARE @big_won_limit AS MONEY
      CREATE TABLE #temp_table    (LO_ID INT
                                             , TERMINAL_ID INT
                                             , TERMINAL_STATUS INT
                                             , PLAY_SESSION_STATUS INT
                                             , IS_ANONYMOUS INT
                                             , PLAYER_ACCOUNT_ID INT
                                             , PLAYER_NAME VARCHAR(50)
                                             , PLAYER_GENDER INT
                                             , PLAYER_AGE INT
                                             , PLAYER_LEVEL INT
                                             , PLAYER_IS_VIP INT
                                             , AL_SAS_HOST_ERROR INT
                                             , AL_DOOR_FLAGS INT 
                                             , AL_BILL_FLAGS INT
                                             , AL_PRINTER_FLAGS INT
                                             , AL_EGM_FLAGS INT
                                             , AL_PLAYED_WON_FLAGS INT
                                             , AL_JACKPOT_FLAGS INT
                                             , AL_STACKER_STATUS INT
                                             , AL_CALL_ATTENDANT_FLAGS INT
                                             , AL_MACHINE_FLAGS INT           
                                             , AL_BIG_WON BIT           
                                             , PLAYED_AMOUNT MONEY
                                             , WON_AMOUNT MONEY
                                             , PLAY_SESSION_BET_AVERAGE MONEY
                                             , TERMINAL_BET_AVERAGE MONEY
                                             , PLAY_SESSION_DURATION INT
                                             , PLAY_SESSION_TOTAL_PLAYED MONEY
                                             , PLAY_SESSION_PLAYED_COUNT INT
                                             , TERMINAL_BANK_ID INT
                                             , TERMINAL_AREA_ID INT
                                             , TERMINAL_PROVIDER_NAME VARCHAR(50)
                                             , PLAY_SESSION_WON_AMOUNT MONEY
                                             , NET_WIN MONEY
                                             , PLAY_SESSION_NET_WIN MONEY
                                             , TERMINAL_NAME NVARCHAR(50)
                                             , TERMINAL_PLAYED_COUNT INT
                                             , PLAYER_TRACKDATA NVARCHAR(50)
                                             , PLAYER_LEVEL_NAME NVARCHAR(50)
                                             , ALARMS NVARCHAR(MAX)
                                             , TERMINAL_DENOMINATION MONEY
                                             , PLAYER_CREATED INT
                                             , PS_rate float  -- Test field
                                             , TERMINAL_PAYOUT MONEY
                                             , THEORETICAL_HOLD MONEY
                                             , AC_HOLDER_BIRTH_DATE DATETIME
                                             , PLAY_SESSION_CASH_IN MONEY
                                             , PLAY_SESSION_CASH_OUT MONEY
											 , PLAYER_FIRST_NAME VARCHAR(50)
											 , PLAYER_LAST_NAME1 VARCHAR(50)
											 , PLAYER_LAST_NAME2 VARCHAR(50)
                                             )
                                             
      -- TERMINAL NAME TYPE
      SELECT @terminal_name_type = GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'SasHost' AND GP_SUBJECT_KEY = 'DisplayName.Format'
      IF (@terminal_name_type IS NULL)
      BEGIN
        SET @terminal_name_type = '%Name'
      END

      -- TODO: Read from GP
      SET @big_won_limit = 1000

  DECLARE @Now AS DATETIME
  DECLARE @TodayOpening AS DATETIME
  DECLARE @CurrentHour AS INT
  DECLARE @HourNow AS DATETIME
  DECLARE @NextHourNow AS DATETIME
  
  SET @Now = GETDATE()
  SET @TodayOpening = DBO.TODAYOPENING(0)
  SET @CurrentHour = DATEPART(HOUR, @Now)
  SET @HourNow = DATEADD(HOUR, DATEDIFF(HOUR, 0, @Now), 0)
  SET @NextHourNow = DATEADD(HOUR, 1, @HourNow)

  DECLARE @DefaultHold MONEY
  SET @DefaultHold = (SELECT   1 - CAST (GP_KEY_VALUE AS MONEY) / 100        
                        FROM   GENERAL_PARAMS                                
                       WHERE   GP_GROUP_KEY   = 'PlayerTracking'               
                         AND   GP_SUBJECT_KEY = 'TerminalDefaultPayout')

      INSERT INTO #temp_table
      SELECT LO.LO_ID
      
            -- Terminal info
            , LO.LO_EXTERNAL_ID AS TERMINAL_ID
            , TE.TE_STATUS   AS TERMINAL_STATUS

            -- Play Session info
            , PLAY_SESSION_STATUS = CASE  WHEN (PS_PLAY_SESSION_ID IS NOT NULL) THEN 2  
                                                 ELSE 1 END

            -- Player info
            , IS_ANONYMOUS = CASE  WHEN (TE.TE_CURRENT_ACCOUNT_ID IS NOT NULL) THEN CASE WHEN (AC.AC_HOLDER_NAME IS NOT NULL) THEN 0 ELSE 1 END 
                                                ELSE NULL END
            , PS.PS_ACCOUNT_ID AS PLAYER_ACCOUNT_ID
            , PLAYER_NAME = CASE WHEN (TE.TE_CURRENT_ACCOUNT_ID IS NOT NULL) THEN ISNULL(AC.AC_HOLDER_NAME, 'Anonymous')
                                          ELSE NULL END
            , AC.AC_HOLDER_GENDER AS PLAYER_GENDER
            , DATEDIFF(YEAR, AC.AC_HOLDER_BIRTH_DATE, @Now) AS PLAYER_AGE
            , PLAYER_LEVEL = CASE  WHEN (TE.TE_CURRENT_ACCOUNT_ID IS NOT NULL) THEN CASE WHEN (AC.AC_HOLDER_NAME IS NOT NULL) THEN AC.AC_HOLDER_LEVEL ELSE -1 END 
                                                ELSE 99 END
            , ISNULL(AC.AC_HOLDER_IS_VIP, 0) AS PLAYER_IS_VIP
            
             -- Flags
            , TS_SAS_HOST_ERROR AS AL_SAS_HOST_ERROR
            , ISNULL(TS_DOOR_FLAGS, 0) AS AL_DOOR_FLAGS 
             , ISNULL(TS_BILL_FLAGS, 0) AS AL_BILL_FLAGS
            , ISNULL(TS_PRINTER_FLAGS, 0) AS AL_PRINTER_FLAGS
            , ISNULL(TS_EGM_FLAGS, 0) AS AL_EGM_FLAGS
            , ISNULL(TS_PLAYED_WON_FLAGS, 0) AS AL_PLAYED_WON_FLAGS
            , ISNULL(TS_JACKPOT_FLAGS, 0) AS AL_JACKPOT_FLAGS
            , ISNULL(TS_STACKER_STATUS, 0) AS AL_STACKER_STATUS
            , ISNULL(TS_CALL_ATTENDANT_FLAGS, 0) AS AL_CALL_ATTENDANT_FLAGS
            , ISNULL(TS_MACHINE_FLAGS, 0) AS AL_MACHINE_FLAGS          
            , AL_BIG_WON = CASE WHEN ISNULL(SPH.WON_AMOUNT, 0) > @big_won_limit THEN 1
                                     ELSE 0 END
                                     
             -- Money info
            , ISNULL(SPH.PLAYED_AMOUNT , 0) AS PLAYED_AMOUNT
            , ISNULL(SPH.WON_AMOUNT, 0) AS WON_AMOUNT
            , PLAY_SESSION_BET_AVERAGE = CASE WHEN (PS.PS_PLAYED_COUNT > 0) THEN ROUND((PS.PS_TOTAL_PLAYED / PS.PS_PLAYED_COUNT), 2)
                                                               ELSE NULL END
            , TERMINAL_BET_AVERAGE = CASE WHEN (SPH.PLAYED_COUNT > 0) THEN ROUND((SPH.PLAYED_AMOUNT / SPH.PLAYED_COUNT), 2)
                                                         ELSE NULL END
            , DATEDIFF(SECOND, ISNULL(PS.PS_STARTED, @Now), @Now) AS PLAY_SESSION_DURATION
            , PS.PS_TOTAL_PLAYED AS PLAY_SESSION_TOTAL_PLAYED
            , PS.PS_PLAYED_COUNT AS PLAY_SESSION_PLAYED_COUNT
            
             -- Area & Bank
            , BK.bk_bank_id AS TERMINAL_BANK_ID
            , BK.bk_area_id AS TERMINAL_AREA_ID
            , PR.pv_name AS TERMINAL_PROVIDER_NAME
            
             , PS.PS_WON_AMOUNT AS PLAY_SESSION_WON_AMOUNT
            
             , SPH.PLAYED_AMOUNT - SPH.WON_AMOUNT AS NET_WIN
            , PS.PS_TOTAL_PLAYED - PS.PS_WON_AMOUNT AS PLAY_SESSION_NET_WIN
            --, TE.TE_NAME AS TERMINAL_NAME
                 , CASE WHEN @terminal_name_type = '%FloorId' THEN
                      TE.TE_NAME + ' [' + TE.TE_FLOOR_ID + ']'
                    WHEN @terminal_name_type = '%BaseName' THEN
                      -- TODO: Find correct field
                      -- TE.TE_BASE_NAME
                      TE.TE_NAME
                    ELSE 
                      TE.TE_NAME
               END AS TERMINAL_NAME
            , SPH.PLAYED_COUNT AS TERMINAL_PLAYED_COUNT
            , AC.AC_TRACK_DATA AS PLAYER_TRACKDATA
            , ISNULL(LEVELS.NAME,'') AS PLAYER_LEVEL_NAME
     , NULL AS ALARMS
     , TE.TE_DENOMINATION AS TERMINAL_DENOMINATION
     , CASE WHEN (AC.AC_CREATED >= @HourNow AND AC.AC_CREATED < @NextHourNow) THEN 1 ELSE 0 END AS PLAYER_CREATED
     , NULL AS PS_RATE
     , ISNULL(TE.TE_THEORETICAL_HOLD, @DefaultHold) AS TERMINAL_PAYOUT
     , (PS.PS_TOTAL_PLAYED * ISNULL(TE.TE_THEORETICAL_HOLD, @DefaultHold)) AS THEORETICAL_HOLD
     , AC.AC_HOLDER_BIRTH_DATE
     , PS.PS_TOTAL_CASH_IN
     , PS.PS_CASH_OUT
	 , PLAYER_FIRST_NAME  = CASE WHEN (TE.TE_CURRENT_ACCOUNT_ID IS NOT NULL) THEN ISNULL(AC.AC_HOLDER_NAME3, 'Anonymous')
                                          ELSE NULL END
	 , PLAYER_LAST_NAME1  = CASE WHEN (TE.TE_CURRENT_ACCOUNT_ID IS NOT NULL) THEN ISNULL(AC.AC_HOLDER_NAME2, 'Anonymous')
                                          ELSE NULL END
	 , PLAYER_LAST_NAME2  = CASE WHEN (TE.TE_CURRENT_ACCOUNT_ID IS NOT NULL) THEN ISNULL(AC.AC_HOLDER_NAME1, 'Anonymous')
                                          ELSE NULL END
        FROM LAYOUT_OBJECTS AS LO
            INNER JOIN LAYOUT_OBJECT_LOCATIONS AS LOL ON LO.LO_ID = LOL.LOL_OBJECT_ID
            INNER JOIN TERMINALS AS TE ON TE.TE_TERMINAL_ID = LO.LO_EXTERNAL_ID 
            INNER JOIN PROVIDERS AS PR ON PR.pv_id = TE.te_prov_id
            
            LEFT JOIN BANKS AS BK ON BK.bk_bank_id = TE.te_bank_id
                        
            LEFT JOIN (SELECT PS_TERMINAL_ID
                            , MAX(PS_PLAY_SESSION_ID) AS PS_PLAY_SESSION_ID
                            , PS_ACCOUNT_ID
                            , PS_TOTAL_PLAYED
                            , PS_WON_AMOUNT
                            , PS_PLAYED_COUNT
                            , PS_STARTED
                            , PS_PLAYED_AMOUNT
                            , PS_TOTAL_CASH_IN
                            , PS_CASH_OUT
                             FROM PLAY_SESSIONS
                            WHERE PS_STATUS = 0
                        GROUP BY PS_TERMINAL_ID, PS_ACCOUNT_ID, PS_TOTAL_PLAYED, PS_WON_AMOUNT, PS_PLAYED_COUNT, PS_PLAYED_AMOUNT, PS_STARTED,PS_TOTAL_CASH_IN,PS_CASH_OUT) AS PS ON TE.TE_TERMINAL_ID = PS.PS_TERMINAL_ID
                        
            LEFT JOIN ACCOUNTS AS AC ON AC.AC_ACCOUNT_ID = TE.TE_CURRENT_ACCOUNT_ID
            
            LEFT JOIN TERMINAL_STATUS AS TS ON TE.TE_TERMINAL_ID = TS.TS_TERMINAL_ID
            
            LEFT JOIN (SELECT SPH_TERMINAL_ID 
                                  , SUM(SPH_PLAYED_AMOUNT)AS PLAYED_AMOUNT
                                  , SUM(SPH_WON_AMOUNT) AS WON_AMOUNT
                                  , SUM(SPH_PLAYED_COUNT) AS PLAYED_COUNT
                         FROM SALES_PER_HOUR_V2
                        WHERE SPH_BASE_HOUR >= @TodayOpening
                       GROUP BY SPH_TERMINAL_ID) AS SPH ON SPH.SPH_TERMINAL_ID = TE.TE_TERMINAL_ID
                       
            LEFT JOIN (SELECT CAST(REPLACE(REPLACE(GP_SUBJECT_KEY,'Level',''),'.Name','') AS INT) AS NUM
                                  , GP_KEY_VALUE AS NAME
                              FROM GENERAL_PARAMS 
                              WHERE GP_GROUP_KEY ='PlayerTracking'
                                AND GP_SUBJECT_KEY LIKE 'Level%.Name'
                          ) AS LEVELS ON LEVELS.NUM = (CASE  WHEN (TE.TE_CURRENT_ACCOUNT_ID IS NOT NULL) THEN 
                                                                                                                  CASE WHEN (AC.AC_HOLDER_NAME IS NOT NULL) THEN AC.AC_HOLDER_LEVEL 
                                                                                                                  ELSE -1 END 
                                                                                                            ELSE 0 END)
            
      WHERE LO.LO_TYPE = 1 
        AND LOL.LOL_CURRENT_LOCATION = 1
                       
      -- TERMINAL group
      SELECT * FROM #temp_table
      
      DROP TABLE #temp_table
      
END
RETURN

GO

/****** Object:  StoredProcedure [dbo].[Layout_GetAssignedTaskToUser]    Script Date: 17/07/2016 17:48:36 ******/
IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Layout_GetAssignedTaskToUser')
BEGIN
	DROP PROCEDURE [dbo].[Layout_GetAssignedTaskToUser]
END
ELSE
/****** Object:  StoredProcedure [dbo].[Layout_GetAssignedTaskToUser]    Script Date: 17/07/2016 17:48:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Layout_GetAssignedTaskToUser]
@loginId bigint
WITH EXEC AS CALLER
AS
BEGIN
  DECLARE @terminal_name_type AS VARCHAR(50)
	DECLARE @big_won_limit AS MONEY
	DECLARE  @Date AS DATETIME
	SET @Date = GETDATE()
	DECLARE   @IntervalHour INT 
	SET @IntervalHour = 1

	SET @big_won_limit = 1000
	
	DECLARE @Now AS DATETIME
	DECLARE @CurrentHour AS BIGINT
	DECLARE @HourNow AS DATETIME
	DECLARE @NextHourNow AS DATETIME
	
	SET @Now = CAST(DATEPART(YEAR, @Date) AS VARCHAR) + '-' +  CAST(DATEPART(MONTH, @Date) AS VARCHAR)  + '-' +  CAST(DATEPART(DAY, @Date) AS VARCHAR)  + ' ' +  CAST(DATEPART(HOUR, @Date) AS VARCHAR) + ':' + CAST(DATEPART(MINUTE, GETDATE()) AS VARCHAR) + ':' + CAST(DATEPART(SECOND, GETDATE()) AS VARCHAR) + '.000'
	  
	SET @CurrentHour = DATEPART(HOUR, @Now)
	SET @HourNow = DATEADD(HOUR, DATEDIFF(HOUR, 0, @Now), 0)
	SET @NextHourNow = DATEADD(HOUR, @IntervalHour, @HourNow)

	DECLARE @userRoles int
	SELECT @userRoles = SUM(T.ROLE) FROM (
											SELECT   DISTINCT(GPF_FORM_ID / 1000000) AS ROLE
											FROM   GUI_PROFILE_FORMS
											WHERE   GPF_GUI_ID = 203 AND GPF_READ_PERM <> 0
												AND   GPF_PROFILE_ID = (
														SELECT   GU_PROFILE_ID
														FROM   GUI_USERS
														WHERE   GU_USER_ID = @loginId
														) AND   (GPF_FORM_ID / 1000000) > 0
										) as T

	
				SELECT	LST.LST_ID
						 ,	LST.LST_STATUS
						 ,	LST.LST_START
						 ,	LST.LST_END
						 ,	LST.LST_CATEGORY
						 ,	LST.LST_SUBCATEGORY
						 ,	LST.LST_TERMINAL_ID
						 ,	LST.LST_ACCOUNT_ID
						 ,	LST.LST_DESCRIPTION
						 ,	LST.LST_SEVERITY
						 ,	LST.LST_CREATION
						 ,	LST.LST_ASSIGNED_ROLE_ID
						 ,	LST.LST_ASSIGNED_USER_ID
						 ,	LST.LST_ASSIGNED
						 ,	LST.LST_ACCEPTED
						 ,	TE.TE_NAME
						 ,	TE.TE_PROVIDER_ID
						 ,	TE.TE_MODEL
						 ,	A.AR_NAME
						 ,	BK.BK_NAME
						 ,	PLAY_SESSION_STATUS = CASE  WHEN (PS_PLAY_SESSION_ID IS NOT NULL) THEN 2  
																						ELSE 1 END
						 ,	IS_ANONYMOUS = CASE  WHEN (PS.PS_ACCOUNT_ID IS NOT NULL) THEN CASE WHEN (AC.AC_HOLDER_NAME IS NOT NULL) THEN 0 ELSE 1 END 
																						ELSE NULL END
						 ,	PS.PS_ACCOUNT_ID AS PLAYER_ACCOUNT_ID
						 ,	PLAYER_NAME = CASE WHEN (PS.PS_ACCOUNT_ID IS NOT NULL) THEN ISNULL(AC.AC_HOLDER_NAME, 'ANONYMOUS')
																	 ELSE NULL END
						 ,	ISNULL(AC.AC_HOLDER_GENDER, 0) AS PLAYER_GENDER
						 ,	DATEDIFF(YEAR, AC.AC_HOLDER_BIRTH_DATE, @NOW) AS PLAYER_AGE
						 ,	PLAYER_LEVEL = AC.AC_HOLDER_LEVEL
						 ,	ISNULL(AC.AC_HOLDER_IS_VIP, 0) AS PLAYER_IS_VIP
						 ,	LST.LST_TITLE
					FROM LAYOUT_SITE_TASKS AS LST
		LEFT JOIN TERMINALS AS TE ON LST.LST_TERMINAL_ID = TE.TE_TERMINAL_ID
		LEFT JOIN PROVIDERS AS PR ON PR.PV_ID = TE.TE_PROV_ID
  		LEFT JOIN BANKS AS BK ON BK.BK_BANK_ID = TE.TE_BANK_ID
		LEFT JOIN AREAS a ON a.AR_AREA_ID = bk.BK_AREA_ID 
		LEFT JOIN (SELECT	PS_TERMINAL_ID
										 ,	PS_PLAY_SESSION_ID
										 ,	PS_ACCOUNT_ID
										 ,	PS_TOTAL_PLAYED
										 ,	PS_WON_AMOUNT
										 ,	PS_PLAYED_COUNT
										 ,	PS_STARTED
										 ,	DBO.GETRATE(PS_STARTED,PS_FINISHED, @NOW) AS PS_RATE
							    FROM PLAY_SESSIONS
								 WHERE ( PS_STARTED <= @Now AND PS_FINISHED > @Now )
			     AND ps_played_count>0) AS PS ON TE.TE_TERMINAL_ID = PS.PS_TERMINAL_ID --AND PS_STARTED >= @NOW
		 LEFT JOIN ACCOUNTS AS AC ON AC.AC_ACCOUNT_ID = PS.PS_ACCOUNT_ID
		 LEFT JOIN TERMINAL_STATUS AS TS ON TE.TE_TERMINAL_ID = TS.TS_TERMINAL_ID
		 LEFT JOIN (SELECT	CAST(REPLACE(REPLACE(GP_SUBJECT_KEY,'LEVEL',''),'.NAME','') AS INT) AS NUM
										 ,	GP_KEY_VALUE AS NAME
									FROM GENERAL_PARAMS 
								 WHERE GP_GROUP_KEY ='PLAYERTRACKING'
									 AND GP_SUBJECT_KEY LIKE 'LEVEL%.NAME'
				  ) AS LEVELS ON LEVELS.NUM = AC.AC_HOLDER_LEVEL 
				 WHERE LST_STATUS IN (1,2)
				   AND  ((LST_ASSIGNED_USER_ID = @loginId)
						OR  (LST_ACCEPTED_USER_ID = @loginId)
						OR  (((LST_ASSIGNED_ROLE_ID & @userRoles) > 0) AND ( isnull(LST_ACCEPTED_USER_ID, 0) = 0)) )
END

GO

		UPDATE		LAYOUT_RANGES 
			 SET		LR_FIELD = 16,
							LR_SECTION_ID = 42
		 WHERE		LR_ID = 50

GO