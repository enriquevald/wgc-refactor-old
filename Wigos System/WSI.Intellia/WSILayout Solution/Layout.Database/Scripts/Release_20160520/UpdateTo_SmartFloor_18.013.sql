USE WGDB_000;

SET ANSI_NULLS ON;

----------------------------------------------------
-----------------VERSION CONTROL--------------------
----------------------------------------------------

DECLARE 
@Exp_SmartFloor_ClientId int, 
@Exp_SmartFloor_CommonBuildId int,
@Exp_SmartFloor_ClientBuildId int,
@New_SmartFloor_ReleaseId int,
@New_SmartFloor_ScriptName nvarchar(50),
@New_SmartFloor_Description nvarchar(1000);

SET @Exp_SmartFloor_ClientId = 18;
SET @Exp_SmartFloor_CommonBuildId = 100;
SET @Exp_SmartFloor_ClientBuildId = 1;
SET @New_SmartFloor_ReleaseId = 13;
SET @New_SmartFloor_ScriptName = N'UpdateTo_SmartFloor_18.013.sql';
SET @New_SmartFloor_Description = N'Patch 18.013 - 12';

IF EXISTS ( SELECT 1 FROM DB_VERSION_SMARTFLOOR )
 DELETE FROM DB_VERSION_SMARTFLOOR;

INSERT INTO [dbo].[db_version_smartfloor]
           ([db_client_id]
           ,[db_common_build_id]
           ,[db_client_build_id]
           ,[db_release_id]
           ,[db_updated_script]
           ,[db_updated]
           ,[db_description])
     VALUES
           (@Exp_SmartFloor_ClientId
           ,@Exp_SmartFloor_CommonBuildId
           ,@Exp_SmartFloor_ClientBuildId
           ,@New_SmartFloor_ReleaseId
           ,@New_SmartFloor_ScriptName
           ,Getdate()
           ,@New_SmartFloor_Description);

           
----------------------------------------------------
----------------------STOREDS-----------------------
----------------------------------------------------

/****** Object:  StoredProcedure [dbo].[Layout_GetRunnerTasks]    Script Date: 05/20/2016 12:58:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetRunnerTasks]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Layout_GetRunnerTasks]
GO

/****** Object:  StoredProcedure [dbo].[Layout_GetRunnerTasks]    Script Date: 05/20/2016 12:58:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Layout_GetRunnerTasks]
  @pRunnerId bigint
WITH EXEC AS CALLER
AS
BEGIN
  DECLARE @Today AS DATETIME
  SET @Today = dbo.TodayOpening(0)
  
  SELECT  LST_ID
       ,  LST_STATUS
       ,  LST_TITLE
       ,  LST_CATEGORY
       ,  LST_SUBCATEGORY
       ,  LST_ACCEPTED
       ,  LST_SEVERITY
    FROM  LAYOUT_SITE_TASKS
   WHERE  LST_ACCEPTED_USER_ID = @pRunnerId 
     AND  LST_STATUS = 2

  SELECT  LST_ID  --0
       ,  LST_STATUS--1
       ,  LST_TITLE--2
       ,  LST_CATEGORY--3
       ,  LST_SUBCATEGORY--4
       ,  CASE WHEN (LST_STATUS = 1) THEN LST_ASSIGNED 
               WHEN (LST_STATUS = 2) THEN LST_ACCEPTED 
               WHEN (LST_STATUS = 3) THEN LST_SOLVED 
               WHEN (LST_STATUS = 4) THEN LST_SCALE 
          END AS LST_DATE --5
       ,  LST_SEVERITY--6
    FROM  LAYOUT_SITE_TASKS
   WHERE  (LST_ACCEPTED_USER_ID = @pRunnerId
      OR  LST_ASSIGNED_USER_ID = @pRunnerId)
     AND  LST_STATUS IN (1,3,4)
     AND  LST_ASSIGNED >= @Today 
     AND  LST_ASSIGNED < DATEADD(day,1,@Today)
END

GO

/****** Object:  StoredProcedure [dbo].[Layout_GetActivity_Test]    Script Date: 19/05/2016 00:08:54 ******/
IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Layout_GetActivity')
BEGIN
	DROP PROCEDURE [dbo].[Layout_GetActivity]
END
ELSE
/****** Object:  StoredProcedure [dbo].[Layout_GetActivity_Test]    Script Date: 19/05/2016 00:08:54 ******/
	SET ANSI_NULLS ON
	GO
	SET QUOTED_IDENTIFIER ON
	GO
	CREATE PROCEDURE [dbo].[Layout_GetActivity]
AS
BEGIN

  DECLARE @terminal_name_type AS VARCHAR(50)
      DECLARE @big_won_limit AS MONEY
      CREATE TABLE #temp_table    (LO_ID INT
                                             , TERMINAL_ID INT
                                             , TERMINAL_STATUS INT
                                             , PLAY_SESSION_STATUS INT
                                             , IS_ANONYMOUS INT
                                             , PLAYER_ACCOUNT_ID INT
                                             , PLAYER_NAME VARCHAR(50)
                                             , PLAYER_GENDER INT
                                             , PLAYER_AGE INT
                                             , PLAYER_LEVEL INT
                                             , PLAYER_IS_VIP INT
                                             , AL_SAS_HOST_ERROR INT
                                             , AL_DOOR_FLAGS INT 
                                             , AL_BILL_FLAGS INT
                                             , AL_PRINTER_FLAGS INT
                                             , AL_EGM_FLAGS INT
                                             , AL_PLAYED_WON_FLAGS INT
                                             , AL_JACKPOT_FLAGS INT
                                             , AL_STACKER_STATUS INT
                                             , AL_CALL_ATTENDANT_FLAGS INT
                                             , AL_MACHINE_FLAGS INT           
                                             , AL_BIG_WON BIT           
                                             , PLAYED_AMOUNT MONEY
                                             , WON_AMOUNT MONEY
                                             , PLAY_SESSION_BET_AVERAGE MONEY
                                             , TERMINAL_BET_AVERAGE MONEY
                                             , PLAY_SESSION_DURATION INT
                                             , PLAY_SESSION_TOTAL_PLAYED MONEY
                                             , PLAY_SESSION_PLAYED_COUNT INT
                                             , TERMINAL_BANK_ID INT
                                             , TERMINAL_AREA_ID INT
                                             , TERMINAL_PROVIDER_NAME VARCHAR(50)
                                             , PLAY_SESSION_WON_AMOUNT MONEY
                                             , NET_WIN MONEY
                                             , PLAY_SESSION_NET_WIN MONEY
                                             , TERMINAL_NAME NVARCHAR(50)
                                             , TERMINAL_PLAYED_COUNT INT
                                             , PLAYER_TRACKDATA NVARCHAR(50)
                                             , PLAYER_LEVEL_NAME NVARCHAR(50)
                                             , ALARMS NVARCHAR(MAX)
                                             , TERMINAL_DENOMINATION MONEY
                                                                         , PLAYER_CREATED INT
                                             , PS_rate float  -- Test field
                                             , TERMINAL_PAYOUT MONEY
                                             , THEORETICAL_HOLD MONEY
                                             , AC_HOLDER_BIRTH_DATE DATETIME
                                             , PLAY_SESSION_CASH_IN MONEY
                                             , PLAY_SESSION_CASH_OUT MONEY
											 , PLAYER_FIRST_NAME VARCHAR(50)
											 , PLAYER_LAST_NAME1 VARCHAR(50)
											 , PLAYER_LAST_NAME2 VARCHAR(50)
                                             )
                                             
      -- TERMINAL NAME TYPE
      SELECT @terminal_name_type = GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'SasHost' AND GP_SUBJECT_KEY = 'DisplayName.Format'
      IF (@terminal_name_type IS NULL)
      BEGIN
        SET @terminal_name_type = '%Name'
      END

      -- TODO: Read from GP
      SET @big_won_limit = 1000

  DECLARE @Now AS DATETIME
  DECLARE @CurrentHour AS INT
  DECLARE @HourNow AS DATETIME
  DECLARE @NextHourNow AS DATETIME
  
  SET @Now = GETDATE()
  SET @CurrentHour = DATEPART(HOUR, @Now)
  SET @HourNow = DATEADD(HOUR, DATEDIFF(HOUR, 0, @Now), 0)
  SET @NextHourNow = DATEADD(HOUR, 1, @HourNow)

  DECLARE @DefaultHold MONEY
  SET @DefaultHold = (SELECT   1 - CAST (GP_KEY_VALUE AS MONEY) / 100        
                        FROM   GENERAL_PARAMS                                
                       WHERE   GP_GROUP_KEY   = 'PlayerTracking'               
                         AND   GP_SUBJECT_KEY = 'TerminalDefaultPayout')

      INSERT INTO #temp_table
      SELECT LO.LO_ID
      
            -- Terminal info
            , LO.LO_EXTERNAL_ID AS TERMINAL_ID
            , TE.TE_STATUS   AS TERMINAL_STATUS

            -- Play Session info
            , PLAY_SESSION_STATUS = CASE  WHEN (PS_PLAY_SESSION_ID IS NOT NULL) THEN 2  
                                                 ELSE 1 END

            -- Player info
            , IS_ANONYMOUS = CASE  WHEN (TE.TE_CURRENT_ACCOUNT_ID IS NOT NULL) THEN CASE WHEN (AC.AC_HOLDER_NAME IS NOT NULL) THEN 0 ELSE 1 END 
                                                ELSE NULL END
            , PS.PS_ACCOUNT_ID AS PLAYER_ACCOUNT_ID
            , PLAYER_NAME = CASE WHEN (TE.TE_CURRENT_ACCOUNT_ID IS NOT NULL) THEN ISNULL(AC.AC_HOLDER_NAME, 'Anonymous')
                                          ELSE NULL END
            , AC.AC_HOLDER_GENDER AS PLAYER_GENDER
            , DATEDIFF(YEAR, AC.AC_HOLDER_BIRTH_DATE, @Now) AS PLAYER_AGE
            , PLAYER_LEVEL = CASE  WHEN (TE.TE_CURRENT_ACCOUNT_ID IS NOT NULL) THEN CASE WHEN (AC.AC_HOLDER_NAME IS NOT NULL) THEN AC.AC_HOLDER_LEVEL ELSE -1 END 
                                                ELSE 99 END
            , ISNULL(AC.AC_HOLDER_IS_VIP, 0) AS PLAYER_IS_VIP
            
             -- Flags
            , TS_SAS_HOST_ERROR AS AL_SAS_HOST_ERROR
            , ISNULL(TS_DOOR_FLAGS, 0) AS AL_DOOR_FLAGS 
             , ISNULL(TS_BILL_FLAGS, 0) AS AL_BILL_FLAGS
            , ISNULL(TS_PRINTER_FLAGS, 0) AS AL_PRINTER_FLAGS
            , ISNULL(TS_EGM_FLAGS, 0) AS AL_EGM_FLAGS
            , ISNULL(TS_PLAYED_WON_FLAGS, 0) AS AL_PLAYED_WON_FLAGS
            , ISNULL(TS_JACKPOT_FLAGS, 0) AS AL_JACKPOT_FLAGS
            , ISNULL(TS_STACKER_STATUS, 0) AS AL_STACKER_STATUS
            , ISNULL(TS_CALL_ATTENDANT_FLAGS, 0) AS AL_CALL_ATTENDANT_FLAGS
            , ISNULL(TS_MACHINE_FLAGS, 0) AS AL_MACHINE_FLAGS          
            , AL_BIG_WON = CASE WHEN ISNULL(SPH.WON_AMOUNT, 0) > @big_won_limit THEN 1
                                     ELSE 0 END
                                     
             -- Money info
            , ISNULL(SPH.PLAYED_AMOUNT , 0) AS PLAYED_AMOUNT
            , ISNULL(SPH.WON_AMOUNT, 0) AS WON_AMOUNT
            , PLAY_SESSION_BET_AVERAGE = CASE WHEN (PS.PS_PLAYED_COUNT > 0) THEN ROUND((PS.PS_TOTAL_PLAYED / PS.PS_PLAYED_COUNT), 2)
                                                               ELSE NULL END
            , TERMINAL_BET_AVERAGE = CASE WHEN (SPH.PLAYED_COUNT > 0) THEN ROUND((SPH.PLAYED_AMOUNT / SPH.PLAYED_COUNT), 2)
                                                         ELSE NULL END
            , DATEDIFF(SECOND, ISNULL(PS.PS_STARTED, @Now), @Now) AS PLAY_SESSION_DURATION
            , PS.PS_TOTAL_PLAYED AS PLAY_SESSION_TOTAL_PLAYED
            , PS.PS_PLAYED_COUNT AS PLAY_SESSION_PLAYED_COUNT
            
             -- Area & Bank
            , BK.bk_bank_id AS TERMINAL_BANK_ID
            , BK.bk_area_id AS TERMINAL_AREA_ID
            , PR.pv_name AS TERMINAL_PROVIDER_NAME
            
             , PS.PS_WON_AMOUNT AS PLAY_SESSION_WON_AMOUNT
            
             , SPH.PLAYED_AMOUNT - SPH.WON_AMOUNT AS NET_WIN
            , PS.PS_TOTAL_PLAYED - PS.PS_WON_AMOUNT AS PLAY_SESSION_NET_WIN
            --, TE.TE_NAME AS TERMINAL_NAME
                 , CASE WHEN @terminal_name_type = '%FloorId' THEN
                      TE.TE_NAME + ' [' + TE.TE_FLOOR_ID + ']'
                    WHEN @terminal_name_type = '%BaseName' THEN
                      -- TODO: Find correct field
                      -- TE.TE_BASE_NAME
                      TE.TE_NAME
                    ELSE 
                      TE.TE_NAME
               END AS TERMINAL_NAME
            , SPH.PLAYED_COUNT AS TERMINAL_PLAYED_COUNT
            , AC.AC_TRACK_DATA AS PLAYER_TRACKDATA
            , ISNULL(LEVELS.NAME,'') AS PLAYER_LEVEL_NAME
     , NULL AS ALARMS
     , TE.TE_DENOMINATION AS TERMINAL_DENOMINATION
     , CASE WHEN (AC.AC_CREATED >= @HourNow AND AC.AC_CREATED < @NextHourNow) THEN 1 ELSE 0 END AS PLAYER_CREATED
     , NULL AS PS_RATE
     , ISNULL(TE.TE_THEORETICAL_HOLD, @DefaultHold) AS TERMINAL_PAYOUT
     , (PS.PS_TOTAL_PLAYED * ISNULL(TE.TE_THEORETICAL_HOLD, @DefaultHold)) AS THEORETICAL_HOLD
     , AC.AC_HOLDER_BIRTH_DATE
     , PS.PS_TOTAL_CASH_IN
     , PS.PS_CASH_OUT
	 , PLAYER_FIRST_NAME  = CASE WHEN (TE.TE_CURRENT_ACCOUNT_ID IS NOT NULL) THEN ISNULL(AC.AC_HOLDER_NAME3, 'Anonymous')
                                          ELSE NULL END
	 , PLAYER_LAST_NAME1  = CASE WHEN (TE.TE_CURRENT_ACCOUNT_ID IS NOT NULL) THEN ISNULL(AC.AC_HOLDER_NAME2, 'Anonymous')
                                          ELSE NULL END
	 , PLAYER_LAST_NAME2  = CASE WHEN (TE.TE_CURRENT_ACCOUNT_ID IS NOT NULL) THEN ISNULL(AC.AC_HOLDER_NAME1, 'Anonymous')
                                          ELSE NULL END
        FROM LAYOUT_OBJECTS AS LO
        
            INNER JOIN TERMINALS AS TE ON TE.TE_TERMINAL_ID = LO.LO_EXTERNAL_ID 
            INNER JOIN PROVIDERS AS PR ON PR.pv_id = TE.te_prov_id
            
            LEFT JOIN BANKS AS BK ON BK.bk_bank_id = TE.te_bank_id
                        
            LEFT JOIN (SELECT PS_TERMINAL_ID
                            , MAX(PS_PLAY_SESSION_ID) AS PS_PLAY_SESSION_ID
                            , PS_ACCOUNT_ID
                            , PS_TOTAL_PLAYED
                            , PS_WON_AMOUNT
                            , PS_PLAYED_COUNT
                            , PS_STARTED
                            , PS_PLAYED_AMOUNT
                            , PS_TOTAL_CASH_IN
                            , PS_CASH_OUT
                             FROM PLAY_SESSIONS
                            WHERE PS_STATUS = 0
                        GROUP BY PS_TERMINAL_ID, PS_ACCOUNT_ID, PS_TOTAL_PLAYED, PS_WON_AMOUNT, PS_PLAYED_COUNT, PS_PLAYED_AMOUNT, PS_STARTED,PS_TOTAL_CASH_IN,PS_CASH_OUT) AS PS ON TE.TE_TERMINAL_ID = PS.PS_TERMINAL_ID
                        
            LEFT JOIN ACCOUNTS AS AC ON AC.AC_ACCOUNT_ID = TE.TE_CURRENT_ACCOUNT_ID
            
            LEFT JOIN TERMINAL_STATUS AS TS ON TE.TE_TERMINAL_ID = TS.TS_TERMINAL_ID
            
            LEFT JOIN (SELECT SPH_TERMINAL_ID 
                                  , SUM(SPH_PLAYED_AMOUNT)AS PLAYED_AMOUNT
                                  , SUM(SPH_WON_AMOUNT) AS WON_AMOUNT
                                  , SUM(SPH_PLAYED_COUNT) AS PLAYED_COUNT
                         FROM SALES_PER_HOUR_V2
                        WHERE SPH_BASE_HOUR >= DATEADD(DAY, DATEDIFF(DAY, 0, @Now), 0) 
                            AND SPH_BASE_HOUR <= @Now
                       GROUP BY SPH_TERMINAL_ID) AS SPH ON SPH.SPH_TERMINAL_ID = TE.TE_TERMINAL_ID
                       
            LEFT JOIN (SELECT CAST(REPLACE(REPLACE(GP_SUBJECT_KEY,'Level',''),'.Name','') AS INT) AS NUM
                                  , GP_KEY_VALUE AS NAME
                              FROM GENERAL_PARAMS 
                              WHERE GP_GROUP_KEY ='PlayerTracking'
                                AND GP_SUBJECT_KEY LIKE 'Level%.Name'
                          ) AS LEVELS ON LEVELS.NUM = (CASE  WHEN (TE.TE_CURRENT_ACCOUNT_ID IS NOT NULL) THEN 
                                                                                                                  CASE WHEN (AC.AC_HOLDER_NAME IS NOT NULL) THEN AC.AC_HOLDER_LEVEL 
                                                                                                                  ELSE -1 END 
                                                                                                            ELSE 0 END)
            
      WHERE LO.LO_TYPE = 1
                       
      -- TERMINAL group
      SELECT * FROM #temp_table
      
      -- BANK group
      SELECT TERMINAL_BANK_ID AS BANK_ID
           , SUM(PLAY_SESSION_STATUS - 1) AS NUM_ACTIVE_PLAY_SESSIONS
            , CASE WHEN (SUM(PLAY_SESSION_STATUS - 1) - SUM(IS_ANONYMOUS)) > 0 THEN 100 - ((SUM(PLAYER_GENDER - 1) * 100)/ (SUM(PLAY_SESSION_STATUS - 1) - SUM(IS_ANONYMOUS)))
                        ELSE NULL END AS PLAYERS_MALE_PERCENTAGE
            , CASE WHEN (SUM(PLAY_SESSION_STATUS - 1) - SUM(IS_ANONYMOUS)) > 0 THEN ((SUM(PLAYER_GENDER - 1) * 100)/ (SUM(PLAY_SESSION_STATUS - 1) - SUM(IS_ANONYMOUS)))
                        ELSE NULL END AS PLAYERS_FEMALE_PERCENTAGE
            , CASE WHEN (SUM(PLAY_SESSION_STATUS - 1) - SUM(IS_ANONYMOUS)) > 0 THEN (SUM(PLAYER_AGE) / (SUM(PLAY_SESSION_STATUS - 1) - SUM(IS_ANONYMOUS)))
                        ELSE NULL END AS PLAYERS_AGE_AVERAGE
            , SUM(PLAYER_IS_VIP) AS NUM_VIPS
            , SUM(PLAYED_AMOUNT) AS PLAYED_AMOUNT
            , SUM(WON_AMOUNT) AS WON_AMOUNT
            , CASE WHEN (SUM(PLAY_SESSION_STATUS - 1) - SUM(IS_ANONYMOUS)) > 0 THEN ROUND((SUM(PLAY_SESSION_BET_AVERAGE) / (SUM(PLAY_SESSION_STATUS - 1) - SUM(IS_ANONYMOUS))), 2)
                        ELSE NULL END  AS PLAY_SESSIONS_BET_AVERAGE
            , CASE WHEN COUNT(TERMINAL_ID) > 0 THEN ROUND((SUM(TERMINAL_BET_AVERAGE) / COUNT(TERMINAL_ID)), 2) 
                    ELSE NULL END AS TERMINALS_BET_AVERAGE
        FROM #temp_table 
  GROUP BY TERMINAL_BANK_ID
      
      -- AREA group
      SELECT TERMINAL_AREA_ID AS AREA_ID
            , SUM(PLAY_SESSION_STATUS - 1) AS NUM_ACTIVE_PLAY_SESSIONS
            , CASE WHEN (SUM(PLAY_SESSION_STATUS - 1) - SUM(IS_ANONYMOUS)) > 0 THEN 100 - ((SUM(PLAYER_GENDER - 1) * 100)/ (SUM(PLAY_SESSION_STATUS - 1) - SUM(IS_ANONYMOUS)))
                        ELSE NULL END AS PLAYERS_MALE_PERCENTAGE
            , CASE WHEN (SUM(PLAY_SESSION_STATUS - 1) - SUM(IS_ANONYMOUS)) > 0 THEN ((SUM(PLAYER_GENDER - 1) * 100)/ (SUM(PLAY_SESSION_STATUS - 1) - SUM(IS_ANONYMOUS)))
                        ELSE NULL END AS PLAYERS_FEMALE_PERCENTAGE
            , CASE WHEN (SUM(PLAY_SESSION_STATUS - 1) - SUM(IS_ANONYMOUS)) > 0 THEN (SUM(PLAYER_AGE) / (SUM(PLAY_SESSION_STATUS - 1) - SUM(IS_ANONYMOUS)))
                        ELSE NULL END AS PLAYERS_AGE_AVERAGE
            , SUM(PLAYER_IS_VIP) AS NUM_VIPS
            , SUM(PLAYED_AMOUNT) AS PLAYED_AMOUNT
            , SUM(WON_AMOUNT) AS WON_AMOUNT
            , CASE WHEN (SUM(PLAY_SESSION_STATUS - 1) - SUM(IS_ANONYMOUS)) > 0 THEN ROUND((SUM(PLAY_SESSION_BET_AVERAGE) / (SUM(PLAY_SESSION_STATUS - 1) - SUM(IS_ANONYMOUS))), 2)
                        ELSE NULL END  AS PLAY_SESSIONS_BET_AVERAGE
            , CASE WHEN COUNT(TERMINAL_ID) > 0 THEN ROUND((SUM(TERMINAL_BET_AVERAGE) / COUNT(TERMINAL_ID)), 2) 
                    ELSE NULL END AS TERMINALS_BET_AVERAGE
        FROM #temp_table 
  GROUP BY TERMINAL_AREA_ID
      
      DROP TABLE #temp_table
      
END
RETURN

GO

IF EXISTS (SELECT * FROM sys.tables WHERE type = 'U' AND name = 'layout_dashboards')
BEGIN
	UPDATE [dbo].[layout_dashboards] SET [ld_dashboard] = N'[null,{"id":"widgetTestList1","x":2,"y":8,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgetTestList2","x":2,"y":0,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgettheater2","x":3,"y":16,"width":1,"height":6,"type":"WidgetSVGAmphitheater","canResize":false,"canMove":true},{"id":"widgettheater3","x":3,"y":22,"width":1,"height":6,"type":"WidgetSVGAmphitheater","canResize":false,"canMove":true},{"id":"widgetOccupancy1","x":3,"y":28,"width":1,"height":6,"type":"WidgetSVGAmphitheater","canResize":false,"canMove":true},{"id":"widgetTasks1","x":3,"y":0,"width":1,"height":8,"type":"WidgetBase","canResize":false,"canMove":true},{"id":"widgetChart1","x":2,"y":19,"width":1,"height":6,"type":"WidgetHighChart","canResize":false,"canMove":true},{"id":"widgetChart2","x":2,"y":25,"width":1,"height":6,"type":"WidgetSVGChart","canResize":false,"canMove":true},{"id":"widgetDonut1","x":0,"y":16,"width":1,"height":6,"type":"WidgetSVGDonutChart","canResize":false,"canMove":true},{"id":"widgetVipsList","x":0,"y":0,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgetIndicator3","x":2,"y":16,"width":1,"height":6,"type":"WidgetBase","canResize":false,"canMove":true},{"id":"widgetMachineOccupation","x":1,"y":8,"width":1,"height":6,"type":"WidgetSVGDonutChart","canResize":false,"canMove":true},{"id":"widgetHRList","x":1,"y":14,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgetAgeSegmentation","x":1,"y":22,"width":1,"height":6,"type":"WidgetSVGDonutChart","canResize":false,"canMove":true},{"id":"widgetLevelSegmentation","x":0,"y":22,"width":1,"height":6,"type":"WidgetSVGDonutChart","canResize":false,"canMove":true},{"id":"widgetRunnersList","x":3,"y":8,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgetBirthday","x":1,"y":0,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgetPlayersMaxLevel","x":0,"y":6,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true}]' WHERE [ld_role_id]	= 1;
	UPDATE [dbo].[layout_dashboards] SET [ld_dashboard] = N'[null,{"id":"widgetTestList1","x":2,"y":8,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgetBirthday","x":1,"y":0,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgetPlayersMaxLevel","x":0,"y":8,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true}]'																																																																																																																																																																																																																																																																																																																																																																																																																																																												   WHERE [ld_role_id]   = 2;
	UPDATE [dbo].[layout_dashboards] SET [ld_dashboard] = N'[null,{"id":"widgetTestList1","x":2,"y":8,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgetTestList2","x":2,"y":0,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgettheater2","x":3,"y":16,"width":1,"height":6,"type":"WidgetSVGAmphitheater","canResize":false,"canMove":true},{"id":"widgettheater3","x":3,"y":22,"width":1,"height":6,"type":"WidgetSVGAmphitheater","canResize":false,"canMove":true},{"id":"widgetOccupancy1","x":3,"y":28,"width":1,"height":6,"type":"WidgetSVGAmphitheater","canResize":false,"canMove":true},{"id":"widgetTasks1","x":3,"y":0,"width":1,"height":8,"type":"WidgetBase","canResize":false,"canMove":true},{"id":"widgetChart1","x":2,"y":19,"width":1,"height":6,"type":"WidgetHighChart","canResize":false,"canMove":true},{"id":"widgetChart2","x":2,"y":25,"width":1,"height":6,"type":"WidgetSVGChart","canResize":false,"canMove":true},{"id":"widgetDonut1","x":0,"y":16,"width":1,"height":6,"type":"WidgetSVGDonutChart","canResize":false,"canMove":true},{"id":"widgetVipsList","x":0,"y":0,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgetIndicator3","x":2,"y":16,"width":1,"height":6,"type":"WidgetBase","canResize":false,"canMove":true},{"id":"widgetMachineOccupation","x":1,"y":8,"width":1,"height":6,"type":"WidgetSVGDonutChart","canResize":false,"canMove":true},{"id":"widgetHRList","x":1,"y":14,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgetAgeSegmentation","x":1,"y":22,"width":1,"height":6,"type":"WidgetSVGDonutChart","canResize":false,"canMove":true},{"id":"widgetLevelSegmentation","x":0,"y":22,"width":1,"height":6,"type":"WidgetSVGDonutChart","canResize":false,"canMove":true},{"id":"widgetRunnersList","x":3,"y":8,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgetBirthday","x":1,"y":0,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgetPlayersMaxLevel","x":0,"y":6,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true}]' WHERE [ld_role_id]	= 4;
	UPDATE [dbo].[layout_dashboards] SET [ld_dashboard] = N'[null,{"id":"widgetTestList1","x":2,"y":8,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgetTestList2","x":2,"y":0,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgettheater2","x":3,"y":16,"width":1,"height":6,"type":"WidgetSVGAmphitheater","canResize":false,"canMove":true},{"id":"widgettheater3","x":3,"y":22,"width":1,"height":6,"type":"WidgetSVGAmphitheater","canResize":false,"canMove":true},{"id":"widgetOccupancy1","x":3,"y":28,"width":1,"height":6,"type":"WidgetSVGAmphitheater","canResize":false,"canMove":true},{"id":"widgetTasks1","x":3,"y":0,"width":1,"height":8,"type":"WidgetBase","canResize":false,"canMove":true},{"id":"widgetChart1","x":2,"y":19,"width":1,"height":6,"type":"WidgetHighChart","canResize":false,"canMove":true},{"id":"widgetChart2","x":2,"y":25,"width":1,"height":6,"type":"WidgetSVGChart","canResize":false,"canMove":true},{"id":"widgetDonut1","x":0,"y":16,"width":1,"height":6,"type":"WidgetSVGDonutChart","canResize":false,"canMove":true},{"id":"widgetVipsList","x":0,"y":0,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgetIndicator3","x":2,"y":16,"width":1,"height":6,"type":"WidgetBase","canResize":false,"canMove":true},{"id":"widgetMachineOccupation","x":1,"y":8,"width":1,"height":6,"type":"WidgetSVGDonutChart","canResize":false,"canMove":true},{"id":"widgetHRList","x":1,"y":14,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgetAgeSegmentation","x":1,"y":22,"width":1,"height":6,"type":"WidgetSVGDonutChart","canResize":false,"canMove":true},{"id":"widgetLevelSegmentation","x":0,"y":22,"width":1,"height":6,"type":"WidgetSVGDonutChart","canResize":false,"canMove":true},{"id":"widgetRunnersList","x":3,"y":8,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgetBirthday","x":1,"y":0,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true},{"id":"widgetPlayersMaxLevel","x":0,"y":6,"width":1,"height":6,"type":"WidgetListBase","canResize":false,"canMove":true}]' WHERE [ld_role_id]	= 8;
END

GO

IF EXISTS (SELECT * FROM sys.tables WHERE type = 'U' AND name = 'LAYOUT_USERS_CONFIGURATION')
BEGIN
	UPDATE [dbo].[LAYOUT_USERS_CONFIGURATION] SET [LC_DASHBOARD] = NULL
END
GO

/****** Object:  StoredProcedure [dbo].[Layout_GetConversationsOfRunner]    Script Date: 05/20/2016 13:12:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetConversationsOfRunner]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Layout_GetConversationsOfRunner]
GO

/****** Object:  StoredProcedure [dbo].[Layout_GetConversationsOfRunner]    Script Date: 05/20/2016 13:12:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Layout_GetConversationsOfRunner]
	@pRunnerId int
WITH EXEC AS CALLER
AS
BEGIN
	SELECT R.LMRM_MANAGER_ID, R.MANAGER_NAME, LAST_MESSAGE, LAST_MESSAGE_DATE FROM (  
      SELECT  LMRM_MANAGER_ID
    	     ,	U_MANAGER.GU_USERNAME as MANAGER_NAME
					 ,  (SELECT TOP 1 LMRM2.LMRM_DATE 
								 FROM LAYOUT_MANAGER_RUNNER_MESSAGES LMRM2 
								WHERE LMRM2.LMRM_MANAGER_ID = LMRM.LMRM_MANAGER_ID 
						 ORDER BY LMRM2.LMRM_DATE DESC) AS LAST_MESSAGE_DATE
           ,	(SELECT TOP 1 LMRM2.LMRM_MESSAGE 
								 FROM LAYOUT_MANAGER_RUNNER_MESSAGES LMRM2 
                WHERE LMRM2.LMRM_MANAGER_ID = LMRM.LMRM_MANAGER_ID 
						 ORDER BY LMRM2.LMRM_DATE DESC) AS LAST_MESSAGE
    	  FROM	LAYOUT_MANAGER_RUNNER_MESSAGES LMRM
  INNER JOIN	GUI_USERS U_MANAGER ON LMRM.LMRM_MANAGER_ID = U_MANAGER.GU_USER_ID 
  INNER JOIN	GUI_USERS U_RUNNER ON LMRM.LMRM_RUNNER_ID = U_RUNNER.GU_USER_ID 
    	 WHERE	LMRM.LMRM_DATE >= CAST(GETDATE() AS DATE)
    	   AND	LMRM_RUNNER_ID = @pRunnerId
    GROUP BY	LMRM_MANAGER_ID
    		   ,	U_MANAGER.GU_USERNAME
  ) AS R ORDER BY R.LAST_MESSAGE_DATE DESC
END
GO




