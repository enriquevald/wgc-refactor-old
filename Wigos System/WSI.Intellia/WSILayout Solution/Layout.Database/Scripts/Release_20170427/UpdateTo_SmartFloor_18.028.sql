﻿USE WGDB_000;

SET ANSI_NULLS ON;

----------------------------------------------------
-----------------VERSION CONTROL--------------------
----------------------------------------------------

DECLARE 
@Exp_SmartFloor_ClientId int, 
@Exp_SmartFloor_CommonBuildId int,
@Exp_SmartFloor_ClientBuildId int,
@New_SmartFloor_ReleaseId int,
@New_SmartFloor_ScriptName nvarchar(50),
@New_SmartFloor_Description nvarchar(1000);

SET @Exp_SmartFloor_ClientId = 18;
SET @Exp_SmartFloor_CommonBuildId = 100;
SET @Exp_SmartFloor_ClientBuildId = 1;
SET @New_SmartFloor_ReleaseId = 27;
SET @New_SmartFloor_ScriptName = N'UpdateTo_SmartFloor_18.028.sql';
SET @New_SmartFloor_Description = N'Patch 18.028 - 25';

IF EXISTS ( SELECT 1 FROM DB_VERSION_SMARTFLOOR )
 DELETE FROM DB_VERSION_SMARTFLOOR;

INSERT INTO [dbo].[db_version_smartfloor]
           ([db_client_id]
           ,[db_common_build_id]
           ,[db_client_build_id]
           ,[db_release_id]
           ,[db_updated_script]
           ,[db_updated]
           ,[db_description])
     VALUES
           (@Exp_SmartFloor_ClientId
           ,@Exp_SmartFloor_CommonBuildId
           ,@Exp_SmartFloor_ClientBuildId
           ,@New_SmartFloor_ReleaseId
           ,@New_SmartFloor_ScriptName
           ,Getdate()
           ,@New_SmartFloor_Description);

          

----------------------------------------------------
----------------------TABLES------------------------
----------------------------------------------------
----------------------------------------------------
----------------------DATA--------------------------
----------------------------------------------------

----------------------------------------------------
----------------------STOREDS-----------------------
----------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_Login]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Layout_Login]
GO

CREATE PROCEDURE [dbo].[Layout_Login] 
    @pUserName as NVARCHAR(MAX)
  , @pOrigin AS INT
  , @opUserId AS INT OUTPUT
  , @opUserName AS NVARCHAR(MAX) = '' OUTPUT
AS
BEGIN
  --Origin can be Web=1/App=2
  DECLARE @ErrorCode AS INT
  DECLARE @BlockReason AS INT
  DECLARE @UserId AS INT
  DECLARE @HasPermission AS INT
  DECLARE @HasRol AS INT
  DECLARE @UserName AS NVARCHAR(MAX)

  SET @ErrorCode = 0

  --Usuario existe
  SELECT   @BlockReason = GU_BLOCK_REASON
       ,   @UserId = GU_USER_ID
       ,   @UserName = GU_FULL_NAME
    FROM   GUI_USERS
   WHERE   GU_USERNAME = @pUserName

  IF @BlockReason IS NULL 
  BEGIN
    SET @ErrorCode = 1
  END
  ELSE IF @BlockReason <> 0 
  BEGIN
    SET @ErrorCode = 2  
  END

  IF @ErrorCode = 0 
  BEGIN
  -- Si tiene permisos (APP o WEB)
      SELECT @HasPermission = COUNT(*) 
        FROM GUI_USERS INNER JOIN GUI_USER_PROFILES ON GUP_PROFILE_ID = GU_PROFILE_ID
                       INNER JOIN GUI_PROFILE_FORMS ON GPF_PROFILE_ID = GUP_PROFILE_ID
                       INNER JOIN GUI_FORMS ON GF_FORM_ID = GPF_FORM_ID
       WHERE GU_USER_ID = @UserId  
         AND GF_GUI_ID = 203 
         AND GF_FORM_ID = @pOrigin 
         AND ([gpf_read_perm]=1 or [gpf_write_perm]=1 or [gpf_delete_perm]=1 or [gpf_execute_perm]=1 )
         AND GPF_GUI_ID = 203
     
     IF @HasPermission = 0 
     BEGIN
      SET @ErrorCode = 3
      END

	   SELECT @HasRol = COUNT(*) 
        FROM GUI_USERS INNER JOIN layout_users_configuration ON LC_USER_ID = GU_USER_ID
        WHERE GU_USER_ID = @UserId  
         AND  1 = 
		  CASE @pOrigin 
		    WHEN 1 THEN  LC_IS_MANAGER
		    WHEN 2 THEN  LC_IS_RUNNER
		    ELSE 0
		  END
    
     IF @HasRol = 0 
     BEGIN
      SET @ErrorCode = 5
      END
  END
        SELECT @opUserId = @UserId  
        SELECT @ErrorCode
        SELECT @opUserName = @UserName
END

