USE WGDB_000;

SET ANSI_NULLS ON;

----------------------------------------------------
-----------------VERSION CONTROL--------------------
----------------------------------------------------

DECLARE 
@Exp_SmartFloor_ClientId int, 
@Exp_SmartFloor_CommonBuildId int,
@Exp_SmartFloor_ClientBuildId int,
@New_SmartFloor_ReleaseId int,
@New_SmartFloor_ScriptName nvarchar(50),
@New_SmartFloor_Description nvarchar(1000);

SET @Exp_SmartFloor_ClientId = 18;
SET @Exp_SmartFloor_CommonBuildId = 100;
SET @Exp_SmartFloor_ClientBuildId = 1;
SET @New_SmartFloor_ReleaseId = 29;
SET @New_SmartFloor_ScriptName = N'UpdateTo_SmartFloor_18.029.sql';
SET @New_SmartFloor_Description = N'Patch 18.029 - 20';

IF EXISTS ( SELECT 1 FROM DB_VERSION_SMARTFLOOR )
 DELETE FROM DB_VERSION_SMARTFLOOR;

INSERT INTO [dbo].[db_version_smartfloor]
           ([db_client_id]
           ,[db_common_build_id]
           ,[db_client_build_id]
           ,[db_release_id]
           ,[db_updated_script]
           ,[db_updated]
           ,[db_description])
     VALUES
           (@Exp_SmartFloor_ClientId
           ,@Exp_SmartFloor_CommonBuildId
           ,@Exp_SmartFloor_ClientBuildId
           ,@New_SmartFloor_ReleaseId
           ,@New_SmartFloor_ScriptName
           ,Getdate()
           ,@New_SmartFloor_Description);




----------------------------------------------------
----------------------TABLES------------------------
----------------------------------------------------


----------------------------------------------------
----------------------DATA--------------------------
----------------------------------------------------
GO
IF  EXISTS (SELECT * FROM layout_ranges WHERE lr_name='High Roller' )
UPDATE layout_ranges 
SET LR_FIELD=16
WHERE lr_name='High Roller'

           
----------------------------------------------------
----------------------STOREDS-----------------------
----------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetRunnersPositions]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Layout_GetRunnersPositions]
GO
CREATE PROCEDURE [dbo].[Layout_GetRunnersPositions]

AS

BEGIN

  

  SELECT   GUIU.GU_USER_ID

         , LRP_DEVICE_ID

         , LRP_FLOOR_ID

         , LRP_X

         , LRP_Y

         , LRP_RANGE

         , LRP_LAST_UPDATE

         , GUIU.GU_USERNAME

         , ISNULL(LD_DEVICE_NAME,LD_DEVICE_ID) LD_NAME

         , LF_NAME

         , (SELECT   SUM(DISTINCT (GPF_FORM_ID / 1000000)) - 16 AS USER_ROLES

              FROM   GUI_PROFILE_FORMS

             WHERE   GPF_GUI_ID = 203 AND GPF_READ_PERM <> 0 

               AND   GPF_PROFILE_ID = 

             (

                 SELECT   GU_PROFILE_ID 

                   FROM   GUI_USERS 

                  WHERE   GU_USER_ID = GUIU.GU_USER_ID

             )) AS USER_ROLES

    FROM   GUI_USERS GUIU

   INNER   JOIN LAYOUT_USERS_CONFIGURATION ON GU_USER_ID    = LC_USER_ID 

   INNER   JOIN (

              SELECT LRP_USER_ID

                   , LRP_DEVICE_ID

                   , LRP_FLOOR_ID

                   , LRP_X

                   , LRP_Y

                   , LRP_RANGE

                   , LRP_LAST_UPDATE

                FROM LAYOUT_RUNNERS_POSITION

               WHERE LRP_LAST_UPDATE IN (

                     SELECT MAX(LRP_LAST_UPDATE) AS  LRP_LAST_UPDATE

                       FROM LAYOUT_RUNNERS_POSITION

                   GROUP BY LRP_USER_ID

                   )

       )T    ON GU_USER_ID    = T.LRP_USER_ID

    LEFT   JOIN LAYOUT_DEVICES             ON LRP_DEVICE_ID = LD_DEVICE_ID

   INNER   JOIN LAYOUT_FLOORS              ON LF_FLOOR_ID   = LRP_FLOOR_ID

   where DATEDIFF(n, LRP_LAST_UPDATE, GETDATE()) < 30

END

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetAlarmsAndTasks]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Layout_GetAlarmsAndTasks]
GO

CREATE PROCEDURE [dbo].[Layout_GetAlarmsAndTasks] 
AS
BEGIN
  DECLARE @DateFrom as DATETIME,
          @DateTo as DATETIME,
          @MaxElapsedTime as INT

  SET @DateFrom = dbo.TodayOpening(0)
  SET @DateTo = DATEADD (DAY, 1, @DateFrom)
  SET @MaxElapsedTime = 48

  SELECT   --TOP 5
           LSA_TERMINAL_ID AS TERMINAL_ID
         , LSA_ALARM_ID AS ALARM_ID
         , LSA_DATE_CREATED AS DATE_CREATION
         , CASE WHEN (DATEDIFF(second,LSA_DATE_CREATED,GETDATE()) / 3600) < @MaxElapsedTime THEN
             RIGHT('0' + CAST(DATEDIFF(second,LSA_DATE_CREATED,GETDATE()) / 3600 AS VARCHAR),2) + ':' +
             RIGHT('0' + CAST((DATEDIFF(second,LSA_DATE_CREATED,GETDATE()) / 60) % 60 AS VARCHAR),2)  + ':' +
             RIGHT('0' + CAST(DATEDIFF(second,LSA_DATE_CREATED,GETDATE()) % 60 AS VARCHAR),2)
           ELSE ' ' END
         , LSA_ID AS ID
         , LSA_ALARM_TYPE AS ALARM_TYPE
         , LSA_USER_CREATED AS USER_CREATION
         , LSA_MEDIA_ID
         , CAST('' as xml).value('xs:base64Binary(sql:column("LM_DATA_THUMBNAIL"))','varchar(max)') as LST_ATTACHED_MEDIA
         , LSA_DESCRIPTION AS AL_DESCRIPTION
         , LSA_ALARM_SOURCE AS ALARM_SOURCE
         , LSA_STATUS
         , TE_NAME
         , LSA_TITLE
       --, LSA_DATE_TO_TASK AS 
       --, LSA_TASK_ID
    FROM   LAYOUT_SITE_ALARMS
    LEFT   JOIN TERMINALS ON LSA_TERMINAL_ID=TE_TERMINAL_ID
    LEFT   JOIN LAYOUT_MEDIA ON LAYOUT_SITE_ALARMS.LSA_MEDIA_ID=LAYOUT_MEDIA.LM_ID
   WHERE   LSA_STATUS IN (0,2)
     AND   LSA_TASK_ID IS NULL
     AND   LSA_DATE_TO_TASK IS NULL
     AND   (LSA_DATE_CREATED>= dbo.TodayOpening(0) OR LSA_STATUS=2)

  SELECT   LST_ID
         , LST_STATUS
         , LST_START
         , LST_END
         , LST_CATEGORY
         , LST_SUBCATEGORY
         , LST_TERMINAL_ID
         , LST_ACCOUNT_ID
         , LST_DESCRIPTION
         , LST_SEVERITY
         , LST_CREATION_USER_ID
         , LST_CREATION
         , LST_ASSIGNED_ROLE_ID
         , LST_ASSIGNED_USER_ID
         , LST_ASSIGNED
         , LST_ACCEPTED_USER_ID
         , LST_ACCEPTED
         , LST_SCALE_FROM_USER_ID
         , LST_SCALE_TO_USER_ID
         , LST_SCALE_REASON
         , LST_SCALE
         , LST_SOLVED_USER_ID
         , LST_SOLVED
         , LST_VALIDATE_USER_ID
         , LST_VALIDATE
         , LST_LAST_STATUS_UPDATE_USER_ID
         , LST_LAST_STATUS_UPDATE
         , LM_ID
       --, '' as LST_ATTACHED_MEDIA
         , CAST('' as xml).value('xs:base64Binary(sql:column("LM_DATA_THUMBNAIL"))','varchar(max)') as LST_ATTACHED_MEDIA  
         , CASE WHEN LST_STATUS = 5 THEN --CONVERT(varchar, DATEADD(MS, DATEDIFF(second,LST_CREATION,LST_VALIDATE)* 1000, 0),114)
            CASE WHEN (DATEDIFF(second,LST_CREATION,LST_VALIDATE) / 3600)<@MaxElapsedTime THEN
              RIGHT('0' + CAST(DATEDIFF(second,LST_CREATION,LST_VALIDATE) / 3600 AS VARCHAR),2) + ':' +
              RIGHT('0' + CAST((DATEDIFF(second,LST_CREATION,LST_VALIDATE) / 60) % 60 AS VARCHAR),2)  + ':' +
              RIGHT('0' + CAST(DATEDIFF(second,LST_CREATION,LST_VALIDATE) % 60 AS VARCHAR),2)
            ELSE ' ' END
           WHEN lst_status = 3 THEN --CONVERT(varchar, DATEADD(MS, DATEDIFF(second,LST_CREATION,LST_SOLVED)* 1000, 0),114)--
             CASE WHEN (DATEDIFF(second,LST_CREATION,LST_SOLVED) / 3600)<@MaxElapsedTime THEN       
               RIGHT('0' + CAST(DATEDIFF(second,LST_CREATION,LST_SOLVED) / 3600 AS VARCHAR),2) + ':' +
               RIGHT('0' + CAST((DATEDIFF(second,LST_CREATION,LST_SOLVED) / 60) % 60 AS VARCHAR),2)  + ':' +
               RIGHT('0' + CAST(DATEDIFF(second,LST_CREATION,LST_SOLVED) % 60 AS VARCHAR),2)
              ELSE ' ' END
           ELSE --CONVERT(varchar, DATEADD(MS, DATEDIFF(second,LST_CREATION,GETDATE())* 1000, 0),114)
             CASE WHEN (DATEDIFF(second,LST_CREATION,GETDATE()) / 3600)<@MaxElapsedTime THEN       
               RIGHT('0' + CAST(DATEDIFF(second,LST_CREATION,GETDATE()) / 3600 AS VARCHAR),2) + ':' +
               RIGHT('0' + CAST((DATEDIFF(second,LST_CREATION,GETDATE()) / 60) % 60 AS VARCHAR),2)  + ':' +
               RIGHT('0' + CAST(DATEDIFF(second,LST_CREATION,GETDATE()) % 60 AS VARCHAR),2)
             ELSE ' ' END
           END 
         , LST_EVENTS_HISTORY
         , CASE WHEN GU_USERNAME IS NULL THEN
            CASE WHEN (LST_ASSIGNED_ROLE_ID & 1 = 1)  THEN  'PCA'
                 WHEN (LST_ASSIGNED_ROLE_ID & 2 = 2)  THEN  'OPM'
                 WHEN (LST_ASSIGNED_ROLE_ID & 4 = 4)  THEN  'SLA'
                 WHEN (LST_ASSIGNED_ROLE_ID & 8 = 8)  THEN  'TSP'
            END
           ELSE GU_USERNAME END 
         , LST_TITLE
    FROM   LAYOUT_SITE_TASKS
    LEFT   JOIN LAYOUT_MEDIA ON LAYOUT_SITE_TASKS.LST_ATTACHED_MEDIA=LAYOUT_MEDIA.LM_ID
    LEFT   JOIN GUI_USERS ON GU_USER_ID=LST_ASSIGNED_USER_ID
   WHERE   (LST_CREATION >= dbo.TodayOpening(0) OR LST_STATUS IN (2,1) OR (LST_SOLVED >= dbo.TodayOpening(0) AND LST_STATUS=3))
 --WHERE LST_STATUS NOT IN (4)
 --WHERE @DateFrom <= LST_CREATION 
 --  AND @DateTo > LST_CREATION 
  
  SELECT   GU_USER_ID,GU_USERNAME 
    FROM   GUI_USERS
   INNER   JOIN LAYOUT_USERS_CONFIGURATION ON GU_USER_ID=LC_USER_ID
   WHERE   GU_PROFILE_ID IN (SELECT   GPF_PROFILE_ID 
                               FROM   GUI_PROFILE_FORMS
                              WHERE   GPF_GUI_ID = 203
                              GROUP   BY GPF_PROFILE_ID) 
     AND   LC_IS_RUNNER = 1 
   ORDER   BY GU_USERNAME

  SELECT   RS.LST_TERMINAL_ID
         , RS.LST_CATEGORY
         , RS.LST_SUBCATEGORY
         , RS.LST_SEVERITY
         , RS.LST_ASSIGNED_ROLE_ID
         , RS.LST_ASSIGNED_USER_ID
         , RS.LST_CREATION
         , RS.LST_STATUS
    FROM   (SELECT   *
                   , Rank() 
              OVER (PARTITION BY LST_TERMINAL_ID ORDER BY LST_TERMINAL_ID,LST_CREATION DESC ) AS Rank
              FROM LAYOUT_SITE_TASKS ) rs 
   WHERE   Rank <= 10
  END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_ReleaseAlarmFromTask]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Layout_ReleaseAlarmFromTask]
GO


  CREATE PROCEDURE [dbo].[Layout_ReleaseAlarmFromTask]
           @pTaskId INT
         , @pUserId INT
         , @pOutputDerivations INT OUTPUT
AS
BEGIN
DECLARE @DerivationsLeft AS INT
DECLARE @Today AS DATETIME
     SET @DerivationsLeft = 0
     SET @Today = GETDATE()
     SET @pOutputDerivations = 0
	 
 IF (EXISTS (SELECT TOP 1 * 
                   FROM LAYOUT_SITE_TASK_DERIVATIONS
                  WHERE LSTD_TASK_ID = @pTaskId
                    AND LSTD_USER_ID = @pUserId) 
	OR
	 EXISTS (SELECT TOP 1 * 
                   FROM LAYOUT_SITE_TASKS
                  WHERE LST_ID = @pTaskId
                    AND LST_ASSIGNED_USER_ID = @pUserId))
BEGIN
      EXEC Layout_InsertTasksDerivations @pTaskId, @pUserId, @Today

    SELECT @DerivationsLeft = COUNT(*) 
      FROM LAYOUT_SITE_TASK_DERIVATIONS 
     WHERE LSTD_TASK_ID = @pTaskId 
       AND LSTD_REJECTED_DATE IS NULL
       
	 IF EXISTS (SELECT TOP 1 * 
                   FROM LAYOUT_SITE_TASKS
                  WHERE LST_ID = @pTaskId
                    AND LST_ASSIGNED_USER_ID = @pUserId)
		BEGIN
		  SET @DerivationsLeft = 0
		END

     SET @pOutputDerivations = @DerivationsLeft

    IF @DerivationsLeft = 0
    BEGIN
            UPDATE LAYOUT_SITE_ALARMS 
               SET LSA_TASK_ID = NULL
                 , LSA_DATE_TO_TASK = NULL         
                 , LSA_STATUS = 2 
         WHERE LSA_TASK_ID = @pTaskId
         
        DELETE FROM LAYOUT_SITE_TASK_DERIVATIONS
         WHERE LSTD_TASK_ID = @pTaskId
    END 
  END
  
  SELECT @pOutputDerivations
END
