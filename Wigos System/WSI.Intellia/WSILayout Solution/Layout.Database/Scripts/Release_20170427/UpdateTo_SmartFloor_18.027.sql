﻿USE WGDB_000;

SET ANSI_NULLS ON;

----------------------------------------------------
-----------------VERSION CONTROL--------------------
----------------------------------------------------

DECLARE 
@Exp_SmartFloor_ClientId int, 
@Exp_SmartFloor_CommonBuildId int,
@Exp_SmartFloor_ClientBuildId int,
@New_SmartFloor_ReleaseId int,
@New_SmartFloor_ScriptName nvarchar(50),
@New_SmartFloor_Description nvarchar(1000);

SET @Exp_SmartFloor_ClientId = 18;
SET @Exp_SmartFloor_CommonBuildId = 100;
SET @Exp_SmartFloor_ClientBuildId = 1;
SET @New_SmartFloor_ReleaseId = 27;
SET @New_SmartFloor_ScriptName = N'UpdateTo_SmartFloor_18.027.sql';
SET @New_SmartFloor_Description = N'Patch 18.027 - 26';

IF EXISTS ( SELECT 1 FROM DB_VERSION_SMARTFLOOR )
 DELETE FROM DB_VERSION_SMARTFLOOR;

INSERT INTO [dbo].[db_version_smartfloor]
           ([db_client_id]
           ,[db_common_build_id]
           ,[db_client_build_id]
           ,[db_release_id]
           ,[db_updated_script]
           ,[db_updated]
           ,[db_description])
     VALUES
           (@Exp_SmartFloor_ClientId
           ,@Exp_SmartFloor_CommonBuildId
           ,@Exp_SmartFloor_ClientBuildId
           ,@New_SmartFloor_ReleaseId
           ,@New_SmartFloor_ScriptName
           ,Getdate()
           ,@New_SmartFloor_Description);

          

----------------------------------------------------
----------------------TABLES------------------------
----------------------------------------------------

----------------------------------------------------
----------------------DATA--------------------------
----------------------------------------------------
IF EXISTS(SELECT * FROM GENERAL_PARAMS where GP_GROUP_KEY = 'Intellia' AND GP_SUBJECT_KEY = 'Terminal.QRCode')
Begin
UPDATE [general_params]
    SET GP_KEY_VALUE = '#{@QR}'
where GP_GROUP_KEY = 'Intellia' AND GP_SUBJECT_KEY = 'Terminal.QRCode'
End
----------------------------------------------------
----------------------STOREDS-----------------------
----------------------------------------------------
