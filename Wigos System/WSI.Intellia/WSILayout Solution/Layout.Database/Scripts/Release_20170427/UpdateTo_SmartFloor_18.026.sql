﻿USE WGDB_000;

SET ANSI_NULLS ON;

----------------------------------------------------
-----------------VERSION CONTROL--------------------
----------------------------------------------------

DECLARE 
@Exp_SmartFloor_ClientId int, 
@Exp_SmartFloor_CommonBuildId int,
@Exp_SmartFloor_ClientBuildId int,
@New_SmartFloor_ReleaseId int,
@New_SmartFloor_ScriptName nvarchar(50),
@New_SmartFloor_Description nvarchar(1000);

SET @Exp_SmartFloor_ClientId = 18;
SET @Exp_SmartFloor_CommonBuildId = 100;
SET @Exp_SmartFloor_ClientBuildId = 1;
SET @New_SmartFloor_ReleaseId = 26;
SET @New_SmartFloor_ScriptName = N'UpdateTo_SmartFloor_18.026.sql';
SET @New_SmartFloor_Description = N'Patch 18.026 - 25';

IF EXISTS ( SELECT 1 FROM DB_VERSION_SMARTFLOOR )
 DELETE FROM DB_VERSION_SMARTFLOOR;

INSERT INTO [dbo].[db_version_smartfloor]
           ([db_client_id]
           ,[db_common_build_id]
           ,[db_client_build_id]
           ,[db_release_id]
           ,[db_updated_script]
           ,[db_updated]
           ,[db_description])
     VALUES
           (@Exp_SmartFloor_ClientId
           ,@Exp_SmartFloor_CommonBuildId
           ,@Exp_SmartFloor_ClientBuildId
           ,@New_SmartFloor_ReleaseId
           ,@New_SmartFloor_ScriptName
           ,Getdate()
           ,@New_SmartFloor_Description);

          

----------------------------------------------------
----------------------TABLES------------------------
----------------------------------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[layout_weather_diary]') AND type in (N'U'))
DROP TABLE [dbo].[layout_weather_diary]
GO

/****** Object:  Table [dbo].[layout_weather_diary]    Script Date: 04/04/2016 16:21:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[layout_weather_diary](
	[lwd_date] [int]  NOT NULL,
	[lwd_weather_code] [int] NOT NULL,
	[lwd_response] [xml] NOT NULL
 CONSTRAINT [PK_layout_weather_diary] PRIMARY KEY CLUSTERED 
(
	[lwd_date] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

----------------------------------------------------
----------------------DATA--------------------------
----------------------------------------------------
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Intellia' AND GP_SUBJECT_KEY = 'Editor.Url')
                     INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Intellia','Editor.Url','http://localhost:38035/editor/editor.aspx')
GO


IF EXISTS (SELECT * FROM layout_ranges)
	UPDATE layout_ranges set lr_field=9 WHERE lr_id=60

GO
----------------------------------------------------
----------------------STOREDS-----------------------
----------------------------------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetActivity]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Layout_GetActivity]
GO
CREATE PROCEDURE [dbo].[Layout_GetActivity]
AS
BEGIN

  DECLARE @terminal_name_type AS VARCHAR(50)
      DECLARE @big_won_limit AS MONEY
      CREATE TABLE #temp_table    (LO_ID INT
                                             , TERMINAL_ID INT
                                             , TERMINAL_STATUS INT
                                             , PLAY_SESSION_STATUS INT
                                             , IS_ANONYMOUS INT
                                             , PLAYER_ACCOUNT_ID INT
                                             , PLAYER_NAME VARCHAR(50)
                                             , PLAYER_GENDER INT
                                             , PLAYER_AGE INT
                                             , PLAYER_LEVEL INT
                                             , PLAYER_IS_VIP INT
                                             , AL_SAS_HOST_ERROR INT
                                             , AL_DOOR_FLAGS INT 
                                             , AL_BILL_FLAGS INT
                                             , AL_PRINTER_FLAGS INT
                                             , AL_EGM_FLAGS INT
                                             , AL_PLAYED_WON_FLAGS INT
                                             , AL_JACKPOT_FLAGS INT
                                             , AL_STACKER_STATUS INT
                                             , AL_CALL_ATTENDANT_FLAGS INT
                                             , AL_MACHINE_FLAGS INT           
                                             , AL_BIG_WON BIT           
                                             , PLAYED_AMOUNT MONEY
                                             , WON_AMOUNT MONEY
                                             , PLAY_SESSION_BET_AVERAGE MONEY
                                             , TERMINAL_BET_AVERAGE MONEY
                                             , PLAY_SESSION_DURATION INT
                                             , PLAY_SESSION_TOTAL_PLAYED MONEY
                                             , PLAY_SESSION_PLAYED_COUNT INT
                                             , TERMINAL_BANK_ID INT
                                             , TERMINAL_AREA_ID INT
                                             , TERMINAL_PROVIDER_NAME VARCHAR(50)
                                             , PLAY_SESSION_WON_AMOUNT MONEY
                                             , NET_WIN MONEY
                                             , PLAY_SESSION_NET_WIN MONEY
                                             , TERMINAL_NAME NVARCHAR(50)
                                             , TERMINAL_PLAYED_COUNT INT
                                             , PLAYER_TRACKDATA NVARCHAR(50)
                                             , PLAYER_LEVEL_NAME NVARCHAR(50)
                                             , ALARMS NVARCHAR(MAX)
                                             , TERMINAL_DENOMINATION MONEY
                                             , PLAYER_CREATED INT
                                             , PS_rate float  -- Test field
                                             , TERMINAL_PAYOUT MONEY
                                             , THEORETICAL_HOLD MONEY
                                             , AC_HOLDER_BIRTH_DATE DATETIME
                                             , PLAY_SESSION_CASH_IN MONEY
                                             , PLAY_SESSION_CASH_OUT MONEY
											 , PLAYER_FIRST_NAME VARCHAR(50)
											 , PLAYER_LAST_NAME1 VARCHAR(50)
											 , PLAYER_LAST_NAME2 VARCHAR(50)
                                             )
                                             
      -- TERMINAL NAME TYPE
      SELECT @terminal_name_type = GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'SasHost' AND GP_SUBJECT_KEY = 'DisplayName.Format'
      IF (@terminal_name_type IS NULL)
      BEGIN
        SET @terminal_name_type = '%Name'
      END

      -- TODO: Read from GP
      SET @big_won_limit = 1000

  DECLARE @Now AS DATETIME
  DECLARE @TodayOpening AS DATETIME
  DECLARE @CurrentHour AS INT
  DECLARE @HourNow AS DATETIME
  DECLARE @NextHourNow AS DATETIME
  
  SET @Now = GETDATE()
  SET @TodayOpening = DBO.TODAYOPENING(0)
  SET @CurrentHour = DATEPART(HOUR, @Now)
  SET @HourNow = DATEADD(HOUR, DATEDIFF(HOUR, 0, @Now), 0)
  SET @NextHourNow = DATEADD(HOUR, 1, @HourNow)

  DECLARE @DefaultHold MONEY
  SET @DefaultHold = (SELECT   1 - CAST (GP_KEY_VALUE AS MONEY) / 100        
                        FROM   GENERAL_PARAMS                                
                       WHERE   GP_GROUP_KEY   = 'PlayerTracking'               
                         AND   GP_SUBJECT_KEY = 'TerminalDefaultPayout')

      INSERT INTO #temp_table
      SELECT LO.LO_ID
      
            -- Terminal info
            , LO.LO_EXTERNAL_ID AS TERMINAL_ID
            , TE.TE_STATUS   AS TERMINAL_STATUS

            -- Play Session info
            , PLAY_SESSION_STATUS = CASE  WHEN (PS_PLAY_SESSION_ID IS NOT NULL) THEN 2  
                                                 ELSE 1 END

            -- Player info
            , IS_ANONYMOUS = CASE  WHEN (PS.PS_ACCOUNT_ID IS NOT NULL) THEN CASE WHEN (AC.AC_HOLDER_NAME IS NOT NULL) THEN 0 ELSE 1 END 
                                                ELSE NULL END
            , PS.PS_ACCOUNT_ID AS PLAYER_ACCOUNT_ID
            , PLAYER_NAME = CASE WHEN (PS.PS_ACCOUNT_ID IS NOT NULL) THEN ISNULL(AC.AC_HOLDER_NAME, 'Anonymous')
                                          ELSE NULL END
            , PLAYER_GENDER = CASE WHEN (PS.PS_ACCOUNT_ID IS NOT NULL) THEN ISNULL(AC.AC_HOLDER_GENDER,0)
						ELSE NULL END
            , ISNULL(DATEDIFF(YEAR, AC.AC_HOLDER_BIRTH_DATE, @Now),-1) AS PLAYER_AGE
            , PLAYER_LEVEL = CASE  WHEN (PS.PS_ACCOUNT_ID IS NOT NULL) THEN CASE WHEN (AC.AC_HOLDER_NAME IS NOT NULL) THEN AC.AC_HOLDER_LEVEL ELSE -1 END 
                                                ELSE 99 END
            , ISNULL(AC.AC_HOLDER_IS_VIP, 0) AS PLAYER_IS_VIP
            
             -- Flags
            , TS_SAS_HOST_ERROR AS AL_SAS_HOST_ERROR
            , ISNULL(TS_DOOR_FLAGS, 0) AS AL_DOOR_FLAGS 
             , ISNULL(TS_BILL_FLAGS, 0) AS AL_BILL_FLAGS
            , ISNULL(TS_PRINTER_FLAGS, 0) AS AL_PRINTER_FLAGS
            , ISNULL(TS_EGM_FLAGS, 0) AS AL_EGM_FLAGS
            , ISNULL(TS_PLAYED_WON_FLAGS, 0) AS AL_PLAYED_WON_FLAGS
            , ISNULL(TS_JACKPOT_FLAGS, 0) AS AL_JACKPOT_FLAGS
            , ISNULL(TS_STACKER_STATUS, 0) AS AL_STACKER_STATUS
            , ISNULL(TS_CALL_ATTENDANT_FLAGS, 0) AS AL_CALL_ATTENDANT_FLAGS
            , ISNULL(TS_MACHINE_FLAGS, 0) AS AL_MACHINE_FLAGS          
            , AL_BIG_WON = CASE WHEN ISNULL(SPH.WON_AMOUNT, 0) > @big_won_limit THEN 1
                                     ELSE 0 END
                                     
             -- Money info
            , ISNULL(SPH.PLAYED_AMOUNT , 0) AS PLAYED_AMOUNT
            , ISNULL(SPH.WON_AMOUNT, 0) AS WON_AMOUNT
            , PLAY_SESSION_BET_AVERAGE = CASE WHEN (PS.PS_PLAYED_COUNT > 0) THEN ROUND((PS.PS_TOTAL_PLAYED / PS.PS_PLAYED_COUNT), 2)
                                                               ELSE NULL END
            , TERMINAL_BET_AVERAGE = CASE WHEN (SPH.PLAYED_COUNT > 0) THEN ROUND((SPH.PLAYED_AMOUNT / SPH.PLAYED_COUNT), 2)
                                                         ELSE NULL END
            , DATEDIFF(SECOND, ISNULL(PS.PS_STARTED, @Now), @Now) AS PLAY_SESSION_DURATION
            , PS.PS_TOTAL_PLAYED AS PLAY_SESSION_TOTAL_PLAYED
            , PS.PS_PLAYED_COUNT AS PLAY_SESSION_PLAYED_COUNT
            
             -- Area & Bank
            , BK.bk_bank_id AS TERMINAL_BANK_ID
            , BK.bk_area_id AS TERMINAL_AREA_ID
            , PR.pv_name AS TERMINAL_PROVIDER_NAME
            
             , PS.PS_WON_AMOUNT AS PLAY_SESSION_WON_AMOUNT
            
             , SPH.PLAYED_AMOUNT - SPH.WON_AMOUNT AS NET_WIN
            , PS.PS_TOTAL_PLAYED - PS.PS_WON_AMOUNT AS PLAY_SESSION_NET_WIN
            --, TE.TE_NAME AS TERMINAL_NAME
                 , CASE WHEN @terminal_name_type = '%FloorId' THEN
                      TE.TE_NAME + ' [' + TE.TE_FLOOR_ID + ']'
                    WHEN @terminal_name_type = '%BaseName' THEN
                      -- TODO: Find correct field
                      -- TE.TE_BASE_NAME
                      TE.TE_NAME
                    ELSE 
                      TE.TE_NAME
               END AS TERMINAL_NAME
            , SPH.PLAYED_COUNT AS TERMINAL_PLAYED_COUNT
            , AC.AC_TRACK_DATA AS PLAYER_TRACKDATA
            , ISNULL(LEVELS.NAME,'') AS PLAYER_LEVEL_NAME
     , NULL AS ALARMS
     , TE.TE_DENOMINATION AS TERMINAL_DENOMINATION
     , CASE WHEN (AC.AC_CREATED >= @HourNow AND AC.AC_CREATED < @NextHourNow) THEN 1 ELSE 0 END AS PLAYER_CREATED
     , NULL AS PS_RATE
     , ISNULL(TE.TE_THEORETICAL_HOLD, @DefaultHold) AS TERMINAL_PAYOUT
     , (PS.PS_TOTAL_PLAYED * ISNULL(TE.TE_THEORETICAL_HOLD, @DefaultHold)) AS THEORETICAL_HOLD
     , AC.AC_HOLDER_BIRTH_DATE
     , PS.PS_TOTAL_CASH_IN
     , PS.PS_CASH_OUT
	 , PLAYER_FIRST_NAME  = CASE WHEN (PS.PS_ACCOUNT_ID IS NOT NULL) THEN ISNULL(AC.AC_HOLDER_NAME3, 'Anonymous')
                                          ELSE NULL END
	 , PLAYER_LAST_NAME1  = CASE WHEN (PS.PS_ACCOUNT_ID IS NOT NULL) THEN ISNULL(AC.AC_HOLDER_NAME1, 'Anonymous')
                                          ELSE NULL END
	 , PLAYER_LAST_NAME2  = CASE WHEN (PS.PS_ACCOUNT_ID IS NOT NULL) THEN ISNULL(AC.AC_HOLDER_NAME2, 'Anonymous')
                                          ELSE NULL END
        FROM LAYOUT_OBJECTS AS LO
            INNER JOIN LAYOUT_OBJECT_LOCATIONS AS LOL ON LO.LO_ID = LOL.LOL_OBJECT_ID
            INNER JOIN TERMINALS AS TE ON TE.TE_TERMINAL_ID = LO.LO_EXTERNAL_ID 
            INNER JOIN PROVIDERS AS PR ON PR.pv_id = TE.te_prov_id
            
            LEFT JOIN BANKS AS BK ON BK.bk_bank_id = TE.te_bank_id
                        
            LEFT JOIN (SELECT PS_TERMINAL_ID
                            , MAX(PS_PLAY_SESSION_ID) AS PS_PLAY_SESSION_ID
                            , PS_ACCOUNT_ID
                            , PS_TOTAL_PLAYED
                            , PS_WON_AMOUNT
                            , PS_PLAYED_COUNT
                            , PS_STARTED
                            , PS_PLAYED_AMOUNT
                            , PS_TOTAL_CASH_IN
                            , PS_CASH_OUT
                             FROM PLAY_SESSIONS
                            WHERE PS_STATUS = 0
                        GROUP BY PS_TERMINAL_ID, PS_ACCOUNT_ID, PS_TOTAL_PLAYED, PS_WON_AMOUNT, PS_PLAYED_COUNT, PS_PLAYED_AMOUNT, PS_STARTED,PS_TOTAL_CASH_IN,PS_CASH_OUT) AS PS ON TE.TE_TERMINAL_ID = PS.PS_TERMINAL_ID
                        
            LEFT JOIN ACCOUNTS AS AC ON AC.AC_ACCOUNT_ID = PS.PS_ACCOUNT_ID
            
            LEFT JOIN TERMINAL_STATUS AS TS ON TE.TE_TERMINAL_ID = TS.TS_TERMINAL_ID
            
            LEFT JOIN (SELECT SPH_TERMINAL_ID 
                                  , SUM(SPH_PLAYED_AMOUNT)AS PLAYED_AMOUNT
                                  , SUM(SPH_WON_AMOUNT) AS WON_AMOUNT
                                  , SUM(SPH_PLAYED_COUNT) AS PLAYED_COUNT
                         FROM SALES_PER_HOUR_V2
                        WHERE SPH_BASE_HOUR >= @TodayOpening
                       GROUP BY SPH_TERMINAL_ID) AS SPH ON SPH.SPH_TERMINAL_ID = TE.TE_TERMINAL_ID
            LEFT JOIN (SELECT LRL_VALUE1 AS NUM,LRL_LABEL AS NAME FROM LAYOUT_RANGES_LEGENDS WHERE  LRL_RANGE_ID=60)
            AS LEVELS ON LEVELS.NUM = (CASE  WHEN (PS.PS_ACCOUNT_ID IS NOT NULL) THEN 
                                                                                                                  CASE WHEN (AC.AC_HOLDER_NAME IS NOT NULL) THEN AC.AC_HOLDER_LEVEL 
                                                                                                                  ELSE -1 END 
                                                                                                            ELSE 0 END)
            
      WHERE LO.LO_TYPE = 1 
        AND LOL.LOL_CURRENT_LOCATION = 1
                       
      -- TERMINAL group
      SELECT * FROM #temp_table
      
      DROP TABLE #temp_table
      
      -- Total occupancy in the site
      SELECT  ISNULL((SUM(OSE_DELTA_IN) - SUM(OSE_DELTA_OUT)), 0) AS TOTAL_OCCUPANCY
      FROM OCCUPANCY_SENSOR
      --ranges
      SELECT LRL_VALUE1 AS NUM,LRL_LABEL AS NAME FROM LAYOUT_RANGES_LEGENDS WHERE  LRL_RANGE_ID=60

END
RETURN

GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layout_GetConfiguration]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Layout_GetConfiguration]
GO
/****** Object:  StoredProcedure [dbo].[Layout_GetConfiguration]    Script Date: 11/4/2017 11:43:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Layout_GetConfiguration] 
	@pUserId INT
AS
BEGIN
IF (SELECT COUNT(*) FROM   LAYOUT_USERS_CONFIGURATION WHERE    LC_USER_ID = @pUserId)= 0 
   BEGIN
    INSERT INTO [dbo].[layout_users_configuration]
               ([lc_user_id]
               ,[lc_parameters]
               ,[lc_dashboard]
               ,[lc_last_update]
               ,[lc_is_manager]
               ,[lc_is_runner]
               ,[LC_PROFILE_UPDATE])
         VALUES
               (@pUserId
               ,'{"ShowActivity":false,"ShowAlarms":false,"ShowBackground":true,"ShowGrid":false,"ShowAreas":false,"ShowBanks":false,"ShowHeatMap":false,"ShowHeatMapPlayer":false,"ShowGamingTables":true,"ShowRealTimeRefresh":true,"TestEnabled":false,"TerminalAppearance3D":1,"ViewMode":2,"DefaultView":1,"PlayAlarmSound":true,"CurrentFloorId":1,"SoundEnabled":true,"AlarmsToSound":[],"Language":0,"ShowRunnersMap":false}'
               ,Null
               ,getdate()
               ,0
               ,0
               ,null)
   END 
   
       SELECT   LC_PARAMETERS
             , LC_DASHBOARD
             , LC_PROFILE_UPDATE
        FROM   LAYOUT_USERS_CONFIGURATION
       WHERE   LC_USER_ID = @pUserId
END -- Layout_GetConfiguration
GO

/****** Object:  Table [dbo].[layout_runners_position]    Script Date: 11/04/2017 10:46:53 a ******/
DROP TABLE [dbo].[layout_runners_position]
GO

/****** Object:  Table [dbo].[layout_runners_position]    Script Date: 11/04/2017 10:46:53 a ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[layout_runners_position](
	[lrp_user_id] [bigint] NOT NULL,
	[lrp_device_id] [nvarchar](50) NOT NULL,
	[lrp_floor_id] [nvarchar](50) NOT NULL,
	[lrp_x] [float] NULL,
	[lrp_y] [float] NULL,
	[lrp_range] [float] NULL,
	[lrp_near_beacon] [nvarchar](50) NULL,
	[lrp_last_update] [datetime] NULL,
 CONSTRAINT [PK_layout_runners_position] PRIMARY KEY CLUSTERED 
(
	[lrp_user_id] ASC,
	[lrp_device_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


/****** Object:  StoredProcedure [dbo].[Layout_SaveLayoutRunnersPosition]    Script Date: 11/04/2017 12:29:45 p ******/
DROP PROCEDURE [dbo].[Layout_SaveLayoutRunnersPosition]
GO

/****** Object:  StoredProcedure [dbo].[Layout_SaveLayoutRunnersPosition]    Script Date: 11/04/2017 12:29:45 p ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Layout_SaveLayoutRunnersPosition]
      @pUserId INT
    , @pDeviceId Nvarchar(50)
    , @pPositionX FLOAT
    , @pPositionY FLOAT
    , @pPositionFloor INT
	, @pRange FLOAT
	, @pNearBeacon Nvarchar(50)
AS
BEGIN
  IF EXISTS (SELECT TOP 1 * FROM layout_runners_position lrp WHERE lrp.lrp_device_id = @pDeviceId AND lrp.lrp_user_id = @pUserId)
    UPDATE	 LAYOUT_RUNNERS_POSITION
       SET   LRP_X           = @pPositionX
           , LRP_Y           = @pPositionY
		   , LRP_RANGE       = @pRange
		   , LRP_NEAR_BEACON = @pNearBeacon
           , LRP_LAST_UPDATE = GETDATE()
     WHERE   LRP_DEVICE_ID   = @pDeviceId 
       AND   LRP_USER_ID     = @pUserId
  ELSE 
    INSERT   INTO LAYOUT_RUNNERS_POSITION	
          ( LRP_DEVICE_ID
          , LRP_USER_ID
          , LRP_X
          , LRP_Y 
		  , LRP_FLOOR_ID
		  , LRP_RANGE
          , LRP_NEAR_BEACON
          , LRP_LAST_UPDATE )
    VALUES ( @pDeviceId
          , @pUserId
          , @pPositionX
          , @pPositionY
          , @pPositionFloor
		  , @pRange
		  , @pNearBeacon
          , GETDATE() )
END 

GO

