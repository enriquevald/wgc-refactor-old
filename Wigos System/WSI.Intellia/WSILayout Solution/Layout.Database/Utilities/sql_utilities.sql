/****** Object:  UserDefinedFunction [dbo].[wsi_time_table]    Script Date: 11/04/2014 10:58:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION wsi_time_table(
--ALTER FUNCTION [dbo].[wsi_time_table](
  @pDateFrom AS DATETIME,
  @pDateTo   AS DATETIME,
  @pInterval AS INT,
  @pIncrement AS INT
  )
RETURNS  
@ReturnValue TABLE (TIME_DATE DATETIME)
AS 
BEGIN  

--DECLARE   @ReturnValue TABLE (TIME_DATE DATETIME)
--DECLARE   @pDateFrom AS DATETIME = '2014-10-22'
--DECLARE   @pDateTo   AS DATETIME = '2014-10-15'
--DECLARE   @pInterval AS INT = 0
--DECLARE   @pIncrement AS INT = -1

DECLARE @_DAY_VAR AS DATETIME
SET @_DAY_VAR = @pDateFrom
WHILE (@pIncrement < 0 AND @_DAY_VAR > @pDateTo) OR (@pIncrement >= 0 AND @_DAY_VAR < @pDateTo)
   BEGIN
          -- LINK WITH TABLE TYPES
          INSERT INTO   @ReturnValue (TIME_DATE) VALUES (@_DAY_VAR)
          
          -- SET INCREMENT
          SET @_DAY_VAR = CASE 
                               WHEN @pInterval = 0 THEN DATEADD(DAY,@pIncrement,@_DAY_VAR)     -- DAY
                               WHEN @pInterval = 1 THEN DATEADD(MONTH,@pIncrement,@_DAY_VAR)   -- MONTH
                               WHEN @pInterval = 2 THEN DATEADD(YEAR,@pIncrement,@_DAY_VAR)    -- YEAR
                          END
   END
   
   --SELECT * FROM @ReturnValue
   RETURN 
END   


----------------------------------------------------------------------------------------------------------------

/****** Object:  StoredProcedure [dbo].[wsi_table_pivot]    Script Date: 11/04/2014 10:59:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
 29-OCT-2014   RMS  First version

 Simple procedure to pivot a table by the distinct values of a column
 Works with tables with 3 o more fields:
 
  Source table:
    col1, col2, col3
  
  Returned table: Field is col1, value is col3
          col2,  value1_col1,  value2_col1,...
    ------------------------------------------      
    data1_col2    data1_col3,   data2_col3,...
    ...
 
 Parameters: 
    @pTable: Name of the table (Table must exists, use ##Tables)
    @pField: A string with the name of the column
    @pValueField: Name of the value to pivot
    
 Example: EXEC wsi_table_pivot N'PROVIDER', N'##DATA_TABLE_1', N'PLAYED'
*/

CREATE PROCEDURE wsi_table_pivot
--ALTER PROCEDURE [dbo].[wsi_table_pivot]
(
  @pTable AS nvarchar(max),
  @pField AS nvarchar(max),
  @pValueField AS nvarchar(max)
)
AS 
BEGIN
  DECLARE   @_list_values AS nvarchar(max)
  DECLARE   @_query_items AS nvarchar(max)
  DECLARE   @_pivot_query AS nvarchar(max)

  CREATE   TABLE ##temporary_values_table 
           (VALUE VARCHAR(MAX))

  SET @_query_items = N'INSERT   INTO ##temporary_values_table 
                        SELECT   DISTINCT ' + @pField + ' AS VALUE 
                          FROM   ' + @pTable 

  -- Obtain the column values
  EXEC sp_executesql @_query_items

  -- Join values for the pivot query string
  SELECT   @_list_values = COALESCE(@_list_values + ',', '') + VALUE
    FROM   (
            SELECT   QUOTENAME(RTRIM(LTRIM(CAST(VALUE AS VARCHAR(20))))) AS VALUE
              FROM   ##temporary_values_table
           ) AS X

  -- Release temporary table with field values         
  DROP TABLE ##temporary_values_table

  -- Prepare the pivot query string
  SET @_pivot_query = N'SELECT   *  
                          FROM   ' + @pTable + ' 
                         PIVOT   ( 
                                  MIN(' + @pValueField + ') 
                                  FOR ' + @pField + ' in (' + @_list_values + ') 
                                  ) PIVOT_TABLE'
  
  -- Execute the pivot query string
  EXEC sp_executesql @_pivot_query         

END
