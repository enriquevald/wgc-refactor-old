USE [wgdb_000]
GO

/****** Object:  UserDefinedFunction [dbo].[wsi_time_table]    Script Date: 03/11/2015 09:17:02 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


--CREATE FUNCTION wsi_time_table(
CREATE FUNCTION [dbo].[wsi_time_table](
  @pDateFrom AS DATETIME,
  @pDateTo   AS DATETIME,
  @pInterval AS INT,
  @pIncrement AS INT
  )
RETURNS  
@ReturnValue TABLE (TIME_DATE DATETIME)
AS 
BEGIN  

--DECLARE   @ReturnValue TABLE (TIME_DATE DATETIME)
--DECLARE   @pDateFrom AS DATETIME = '2014-10-22'
--DECLARE   @pDateTo   AS DATETIME = '2014-10-15'
--DECLARE   @pInterval AS INT = 0
--DECLARE   @pIncrement AS INT = -1

DECLARE @_DAY_VAR AS DATETIME
SET @_DAY_VAR = @pDateFrom
WHILE (@pIncrement < 0 AND @_DAY_VAR > @pDateTo) OR (@pIncrement >= 0 AND @_DAY_VAR < @pDateTo)
   BEGIN
          -- LINK WITH TABLE TYPES
          INSERT INTO   @ReturnValue (TIME_DATE) VALUES (@_DAY_VAR)
          
          -- SET INCREMENT
          SET @_DAY_VAR = CASE 
                               WHEN @pInterval = 0 THEN DATEADD(DAY,@pIncrement,@_DAY_VAR)     -- DAY
                               WHEN @pInterval = 1 THEN DATEADD(MONTH,@pIncrement,@_DAY_VAR)   -- MONTH
                               WHEN @pInterval = 2 THEN DATEADD(YEAR,@pIncrement,@_DAY_VAR)    -- YEAR
                          END
   END
   
   --SELECT * FROM @ReturnValue
   RETURN 
END   

GO


