USE [wgdb_000]
GO

/****** Object:  UserDefinedFunction [dbo].[SP_Get_Range_Date]    Script Date: 03/11/2015 09:11:56 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[SP_Get_Range_Date] 
(  @pDate AS DATETIME
, @pRange AS VARCHAR(1)
, @pGetTime AS BIT = 0
)
RETURNS DATETIME
AS
BEGIN

  DECLARE @_time AS VARCHAR(8)

  IF (@pRange = 'T')
    BEGIN
      RETURN @pDate
    END

  -- Keep time
  IF @pGetTime = 0
    BEGIN
      SET @_time = ''
    END
  ELSE
    BEGIN
      SET @_time = CONVERT(VARCHAR(8), @pDate, 108)
    END
  

  IF (@pRange = 'W')
    BEGIN
      RETURN LEFT(CONVERT(nvarchar, DATEADD(WK, DATEDIFF(WK, 0, @pDate), 0), 120), 11) + @_time
    END

  IF (@pRange = 'M')
    BEGIN
      RETURN LEFT(CONVERT(nvarchar, DATEADD(M, DATEDIFF(M, 0, @pDate), 0), 120), 11) + @_time
    END

  IF (@pRange = 'Y')
    BEGIN
      RETURN LEFT(CONVERT(nvarchar, DATEADD(YY, DATEDIFF(YY, 0, @pDate), 0), 120), 11) + @_time
    END

  --IF (@pRange = 'L')
  --  BEGIN
  --    RETURN ?
  --  END
    
  --SET @DATEFROM_WEEK = LEFT(CONVERT(nvarchar, @DATEFROM_WEEK, 120), 11) + convert(char(8), @_DATE, 108)  

  --SELECT 
  --   @pDate
  -- , DATEDIFF(WK, 0, @pDate)
  -- , DATEADD(WK, DATEDIFF(WK, 0, @pDate) - 1, 0)
  -- , CONVERT(VARCHAR(10), DATEADD(WK, DATEDIFF(WK, 0, @pDate) - 1, 0), 112)

RETURN @pDate

END 


GO


