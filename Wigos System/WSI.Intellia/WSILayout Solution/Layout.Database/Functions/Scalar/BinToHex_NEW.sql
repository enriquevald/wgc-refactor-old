USE [wgdb_000]
GO

/****** Object:  UserDefinedFunction [dbo].[BinToHex_NEW]    Script Date: 03/11/2015 09:13:29 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--CREATE FUNCTION [dbo].[BinToHex_NEW]
CREATE FUNCTION [dbo].[BinToHex_NEW]
(
    @BinValue           VARBINARY(MAX)
  , @IncludeSQL0xPrefix BIT = 1
)
RETURNS VARCHAR(MAX)
AS
BEGIN
  DECLARE @ReturnString VARCHAR(MAX)
  IF (@@MICROSOFTVERSION / 0x01000000 >= 10)
    BEGIN
      if @IncludeSQL0xPrefix = 1
        SET @ReturnString = CONVERT(varchar(max), @BinValue, 1)  
      ELSE 
        SET @ReturnString = CONVERT(varchar(max), @BinValue, 2)  
      END ELSE
            BEGIN
              DECLARE @i int
              DECLARE @length int
              DECLARE @hexstring char(16)
              SELECT   @ReturnString = CASE WHEN @IncludeSQL0xPrefix = 1 THEN '0x' ELSE '' END
                     , @i = 1
                     , @length = DATALENGTH(@binvalue)
                     , @hexstring = '0123456789abcdef'
      WHILE (@i <= @length) 
        BEGIN
          DECLARE @tempint   int
          DECLARE @firstint  int
          DECLARE @secondint int
          SET @tempint      = CONVERT(int, SUBSTRING(@binvalue, @i, 1))
          SET @firstint     = FLOOR(@tempint / 16)
          SET @secondint    = @tempint - (@firstint * 16)
          SET @ReturnString = @ReturnString + SUBSTRING(@hexstring, @firstint + 1, 1) + SUBSTRING(@hexstring, @secondint + 1, 1)
          SET @i = @i + 1
        END

    END

  RETURN @ReturnString

END
GO


