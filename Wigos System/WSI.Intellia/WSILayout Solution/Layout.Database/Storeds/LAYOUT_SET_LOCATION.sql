USE [wgdb_000]
GO

/****** Object:  StoredProcedure [dbo].[LAYOUT_SET_LOCATION]    Script Date: 03/11/2015 07:42:50 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



---- CREATE / UPDATE LOCATION
--DECLARE @pId AS INT
--DECLARE @pX AS INT
--DECLARE @pY AS INT
--DECLARE @pCreation AS DATETIME
--DECLARE @pType AS INT
--DECLARE @pFloorId AS INT
--DECLARE @pGridId AS VARCHAR(50)
--DECLARE @pStatus AS INT

CREATE PROCEDURE [dbo].[LAYOUT_SET_LOCATION] 
( @pId INT
, @pX INT
, @pY INT
, @pCreation DATETIME
, @pType INT
, @pFloorId INT
, @pGridId VARCHAR(50)
, @pStatus INT
) AS
BEGIN

  IF (@pId IS NULL)
    BEGIN
      -- INSERT
      
      IF (@pCreation IS NULL)
        BEGIN
          SET @pCreation = GETDATE()
        END
      
      INSERT INTO   layout_locations
                  ( loc_x, loc_y, loc_creation, loc_type, loc_floor_number, loc_grid_id, loc_status )
           VALUES ( @pX, @pY, @pCreation, @pType, @pFloorId, @pGridId, @pStatus)
             
      SET @pId = SCOPE_IDENTITY()           
             
    END
  ELSE
    BEGIN
     -- UPDATE ALL EXCEPT ID AND CREATION DATE
           UPDATE   layout_locations
              SET   loc_x = @pX
                  , loc_y = @pY
                  , loc_type = @pType
                  , loc_floor_number = @pFloorId
                  , loc_grid_id = @pGridId
                  , loc_status = @pStatus
                  
    END
    
  SELECT @pId
END  
GO


