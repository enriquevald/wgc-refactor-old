CREATE PROCEDURE [dbo].[Layout_SaveConfiguration]
      @pUserId INT
    , @pLastUpdate DATETIME = NULL
    , @pParameters VARCHAR(MAX)
    , @pDashboard VARCHAR(MAX) = NULL
AS
BEGIN



      IF EXISTS (SELECT TOP 1 * FROM LAYOUT_USERS_CONFIGURATION WHERE LC_USER_ID = @pUserId)
            UPDATE	LAYOUT_USERS_CONFIGURATION 
               SET	LC_LAST_UPDATE = @pLastUpdate
                 ,	LC_PARAMETERS = @pParameters
                 ,  LC_DASHBOARD = ISNULL(@pDashboard, NULL)
             WHERE	LC_USER_ID = @pUserId

      ELSE

       INSERT INTO	LAYOUT_USERS_CONFIGURATION
            VALUES 
				 (	@pUserId
                 ,	@pParameters
                 ,  ISNULL(@pDashboard, NULL)
                 ,	ISNULL(@pLastUpdate, GETDATE())
                 ,  0
				 ) 
      
END --Layout_SaveConfiguration
GO