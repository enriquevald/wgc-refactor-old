USE [wgdb_000]
GO

/****** Object:  StoredProcedure [dbo].[Layout_ReleaseAlarmFromTask]    Script Date: 03/11/2015 07:24:10 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Layout_ReleaseAlarmFromTask]
	-- Add the parameters for the stored procedure here
	@pTaskId INT
AS
BEGIN
	  UPDATE LAYOUT_SITE_ALARMS 
	  SET LSA_TASK_ID = NULL
	    , LSA_DATE_TO_TASK = NULL  	   
	    , LSA_STATUS = 2 
  WHERE LSA_TASK_ID = @pTaskId
END

GO


