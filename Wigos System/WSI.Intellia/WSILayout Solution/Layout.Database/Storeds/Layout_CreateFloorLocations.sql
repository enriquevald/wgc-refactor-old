USE [wgdb_000]
GO
/****** Object:  StoredProcedure [dbo].[Layout_CreateFloorLocations]    Script Date: 02/11/2015 04:21:56 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE Layout_CreateFloorLocations (

  @pFloorId INT
, @pWidth INT
, @pHeight INT
, @pType INT
) AS
BEGIN
  --SET @pFloorId = 999
  --SET @pWidth = 100
  --SET @pHeight = 100
  --SET @pType = 1

  SET NOCOUNT ON

  DECLARE @_loc_x AS INT
  DECLARE @_loc_y AS INT
  DECLARE @_creation AS DATETIME
  DECLARE @_last_id AS INT

  DECLARE @_memory AS TABLE (
  loc_id INT, loc_x INT, loc_y INT, loc_creation DATETIME, loc_type INT, loc_floor_number INT, loc_grid_id VARCHAR(50)
  )

  SET @_creation = GETDATE()

  SELECT @_last_id = ISNULL(MAX(loc_id), 0) FROM layout_locations

  -- Create locations on the floor
  SET @_loc_x = 0
  WHILE (@_loc_x < @pWidth)
    BEGIN
      SET @_loc_y = 0
      WHILE (@_loc_y < @pHeight)
        BEGIN
          SET @_last_id = @_last_id + 1

          INSERT   INTO @_memory
                 ( loc_id, loc_x, loc_y, loc_creation, loc_type, loc_floor_number, loc_grid_id )
          VALUES ( @_last_id, @_loc_x, @_loc_y, @_creation, @pType, @pFloorId, CAST(@_loc_x AS VARCHAR) + '-' + CAST(@_loc_y AS VARCHAR))
          
          SET @_loc_y = @_loc_y + 1 
        END
      SET @_loc_x = @_loc_x + 1
    END

  SET IDENTITY_INSERT layout_locations ON

  INSERT    INTO layout_locations 
          ( loc_id, loc_x, loc_y, loc_creation, loc_type, loc_floor_number, loc_grid_id )
  SELECT    loc_id
          , loc_x
          , loc_y
          , loc_creation
          , loc_type
          , loc_floor_number
          , loc_grid_id 
     FROM   @_memory
     
  SET IDENTITY_INSERT layout_locations OFF   
  
  SELECT * FROM @_memory
END  