USE [wgdb_000]
GO

/****** Object:  StoredProcedure [dbo].[Layout_Reports_Cash]    Script Date: 03/11/2015 07:24:41 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/****** Object:  StoredProcedure [dbo].[Layout_Reports_Cash]    Script Date: 11/10/2014 16:26:26 ******/


--CREATE PROCEDURE Layout_Reports_Cash
CREATE PROCEDURE [dbo].[Layout_Reports_Cash]
    @pReport      AS INT      = 0
  , @pConcept     AS INT      = 0
  , @pExtra       AS BIT      = 0  
  , @pDate        AS DATETIME = NULL
  , @pDateGroups  AS INT      = 0
 AS
BEGIN

  IF @pDate IS NULL
    BEGIN
      SET @pDate = GETDATE()
    END

  DECLARE @pDateTo AS DATETIME
  DECLARE @pConcatDate DateTime
  DECLARE @pConcatDate2007 DateTime
  SET @pConcatDate = dbo.ConcatOpeningTime(0, @pDate)
  SET @pConcatDate2007 = dbo.ConcatOpeningTime(0, '2007/01/01')

IF @pReport = 0
 BEGIN
  -- CASH INPUTS ------------------------------------------------------------------------------------------------------------------------------
  
  DECLARE @_date_offset AS INT
  SET @_date_offset = -1

  DECLARE @DATA_TABLE AS TABLE (BASE_DATE DATE, VALUE MONEY)

  WHILE (@_date_offset > -11)
  BEGIN
    INSERT INTO   @DATA_TABLE 
         SELECT   CAST(DATEADD(DAY, @_date_offset, @pConcatDate) AS DATE), 0 
    SET @_date_offset = @_date_offset - 1
  END

SELECT * FROM (
  SELECT * FROM @DATA_TABLE
  ) as A
    UNION 
  (
  SELECT   BASE_DATE
         , CASE @pConcept 
            WHEN 0 THEN TOTAL_CASH_IN
            WHEN 1 THEN TOTAL_CASH_IN - TOTAL_OUTPUTS - TAXES
           END  AS VALUE  
    FROM   (
            SELECT   DATEADD (DAY, DATEDIFF (HOUR, @pConcatDate2007, CM_DATE) / 24, '01/01/2007') AS BASE_DATE
                   , (SUM(CASE WHEN CM_TYPE IN (9, 28, 34, 35, 37, 39, 54, 55, 71, 85, 86) THEN CM_SUB_AMOUNT + CM_ADD_AMOUNT ELSE 0 END) + SUM(CASE WHEN CM_TYPE IN (78, 79, 92) THEN CM_SUB_AMOUNT ELSE 0 END)) AS TOTAL_CASH_IN
                   , SUM(CASE WHEN CM_TYPE IN (8, 10, 36, 38, 40, 41, 69, 70) THEN CM_SUB_AMOUNT + CM_ADD_AMOUNT ELSE 0 END) AS TOTAL_OUTPUTS
                   , SUM(CASE WHEN CM_TYPE IN (6, 14) THEN CM_SUB_AMOUNT + CM_ADD_AMOUNT ELSE 0 END) AS PRIZE_TAX
                   , SUM(CASE WHEN CM_TYPE IN (95, 96, 101, 102) THEN CM_SUB_AMOUNT + CM_ADD_AMOUNT ELSE 0 END) AS TAXES
              FROM   CASHIER_MOVEMENTS with (index (ix_cm_date_type))
             WHERE   CM_DATE <  DATEADD(DAY, 1, @pConcatDate)
               AND   CM_DATE >= DATEADD(DAY, -11, @pConcatDate)
               AND   CM_TYPE in (6, 8, 9, 10, 14, 28, 34, 35, 36, 37, 38, 39, 40, 41, 54, 55, 69, 70, 71, 78, 79, 92, 85, 86, 95, 96, 101, 102)
            GROUP BY DATEADD (DAY, DATEDIFF (HOUR, @pConcatDate2007, CM_DATE) / 24, '01/01/2007')
           ) X
   WHERE   TOTAL_CASH_IN > 0
      OR   TOTAL_OUTPUTS > 0
      OR   PRIZE_TAX   > 0
      OR   TAXES > 0
  ) 
  ORDER BY BASE_DATE
  
 END

IF @pReport = 1 OR @pReport = 2
 BEGIN
  -- TOP 10 CASHIERS CASH IN ----------------------------------------------------------------------------------------------------------------------------
  SELECT   TOP 10
         CT.ct_name
       , SUM(CSGS.CM_ADD_AMOUNT) AS VALUE
  FROM   cashier_movements_grouped_by_session_id CSGS
 INNER   JOIN cashier_sessions CS ON CS.cs_session_id = CSGS.CM_SESSION_ID
 INNER   JOIN cashier_terminals CT ON CS.cs_cashier_id = CT.ct_cashier_id 
 WHERE   CS.cs_opening_date >= @pDate
   AND   CS.cs_opening_date < DATEADD(DAY, 1, @pDate)
   AND   CSGS.CM_SUB_TYPE = 0
   AND   CSGS.CM_TYPE IN (4, 54, 13)
   AND   CS.cs_name NOT LIKE 'SYS-%'   
 GROUP   BY CS.cs_cashier_id, CT.ct_name
 ORDER   BY CASE @pReport 
             WHEN 1 THEN SUM(CSGS.CM_ADD_AMOUNT)  -- ASC
             WHEN 2 THEN -SUM(CSGS.CM_ADD_AMOUNT) -- DESC
            END 
 END 

IF @pReport = 3 
 BEGIN
  -- CASH RESULT

SET @pDateTo = DATEADD(DAY, -11, @pDate)

DECLARE @_times as TABLE (TIME_DATE DATETIME)
INSERT INTO @_times SELECT * FROM dbo.wsi_time_table(@pDate, @pDateTo, 0, -1)

SELECT   DISTINCT BASE_DATE AS BASE_DATE,
         MAX(TOTAL_CASH_IN) AS TOTAL_CASH_IN,
         MAX(TOTAL_CASH_OUT) AS TOTAL_CASH_OUT,
         MAX(X.PRIZE_TAXES) AS PRIZE_TAXES,
         MAX(X.RESULT_CASHIER) AS RESULT_CASHIER
FROM (         
SELECT   BASE_DATE
       , TOTAL_CASH_IN
       , TOTAL_OUTPUTS - PRIZE_TAX AS TOTAL_CASH_OUT
       , PRIZE_TAX + TAXES AS PRIZE_TAXES
       , TOTAL_CASH_IN - TOTAL_OUTPUTS - TAXES AS RESULT_CASHIER
  FROM   (
          SELECT   --DATEADD (DAY, DATEDIFF (HOUR, @pConcatDate2007, CM_DATE) / 24, '01/01/2007') AS BASE_DATE
                    DATEADD (DAY, DATEDIFF (HOUR, @pConcatDate2007, CM_DATE) / 24, '01/01/2007') AS BASE_DATE
                 , (SUM(CASE WHEN CM_TYPE IN (9, 28, 34, 35, 37, 39, 54, 55, 71, 85, 86) THEN CM_SUB_AMOUNT + CM_ADD_AMOUNT ELSE 0 END) + SUM(CASE WHEN CM_TYPE IN (78, 79, 92) THEN CM_SUB_AMOUNT ELSE 0 END)) AS TOTAL_CASH_IN
                 , SUM(CASE WHEN CM_TYPE IN (8, 10, 36, 38, 40, 41, 69, 70) THEN CM_SUB_AMOUNT + CM_ADD_AMOUNT ELSE 0 END) AS TOTAL_OUTPUTS
                 , SUM(CASE WHEN CM_TYPE IN (6, 14) THEN CM_SUB_AMOUNT + CM_ADD_AMOUNT ELSE 0 END) AS PRIZE_TAX
                 , SUM(CASE WHEN CM_TYPE IN (95, 96, 101, 102) THEN CM_SUB_AMOUNT + CM_ADD_AMOUNT ELSE 0 END) AS TAXES
            FROM   CASHIER_MOVEMENTS_GROUPED_BY_HOUR --with (index (ix_cm_date_type))
           WHERE   CM_DATE <  DATEADD(DAY, 1, @pConcatDate)
             AND   CM_DATE >= DATEADD(DAY, -11, @pConcatDate)
             AND   CM_TYPE in (6, 8, 9, 10, 14, 28, 34, 35, 36, 37, 38, 39, 40, 41, 54, 55, 69, 70, 71, 78, 79, 92, 85, 86, 95, 96, 101, 102)
          GROUP BY DATEADD (DAY, DATEDIFF (HOUR, @pConcatDate2007, CM_DATE) / 24, '01/01/2007')
         ) X
 WHERE   TOTAL_CASH_IN > 0
    OR   TOTAL_OUTPUTS > 0
    OR   PRIZE_TAX   > 0
    OR   TAXES > 0
--ORDER BY BASE_DATE
UNION
SELECT   TIME_DATE
       , 0
       , 0
       , 0
       , 0   
  FROM @_times
) X
GROUP BY X.BASE_DATE

 END

IF @pReport = 4
 BEGIN
  -- CASHIERS RESULTS
 
SELECT   CM_CASHIER_NAME
       , TOTAL_CASH_IN - TOTAL_OUTPUTS - TAXES AS RESULT_CASHIER
       --, BASE_DATE
       , TOTAL_CASH_IN
       , TOTAL_OUTPUTS - PRIZE_TAX AS TOTAL_CASH_OUT
       , PRIZE_TAX + TAXES AS PRIZE_TAXES
       
  FROM   (
          SELECT   CM_CASHIER_NAME
                 , (SUM(CASE WHEN CM_TYPE IN (9, 28, 34, 35, 37, 39, 54, 55, 71, 85, 86) THEN CM_SUB_AMOUNT + CM_ADD_AMOUNT ELSE 0 END) + SUM(CASE WHEN CM_TYPE IN (78, 79, 92) THEN CM_SUB_AMOUNT ELSE 0 END)) AS TOTAL_CASH_IN
                 , SUM(CASE WHEN CM_TYPE IN (8, 10, 36, 38, 40, 41, 69, 70) THEN CM_SUB_AMOUNT + CM_ADD_AMOUNT ELSE 0 END) AS TOTAL_OUTPUTS
                 , SUM(CASE WHEN CM_TYPE IN (6, 14) THEN CM_SUB_AMOUNT + CM_ADD_AMOUNT ELSE 0 END) AS PRIZE_TAX
                 , SUM(CASE WHEN CM_TYPE IN (95, 96, 101, 102) THEN CM_SUB_AMOUNT + CM_ADD_AMOUNT ELSE 0 END) AS TAXES
            FROM   CASHIER_MOVEMENTS with (index (ix_cm_date_type))
           INNER   JOIN cashier_terminals ON cm_cashier_id = ct_cashier_id and ct_terminal_id IS NULL
           WHERE   CM_DATE <  DATEADD(DAY, 1, @pConcatDate)
             AND   CM_DATE >= @pConcatDate
             AND   CM_TYPE in (6, 8, 9, 10, 14, 28, 34, 35, 36, 37, 38, 39, 40, 41, 54, 55, 69, 70, 71, 78, 79, 92, 85, 86, 95, 96, 101, 102)
          GROUP BY CM_CASHIER_NAME
         ) X
 --WHERE   TOTAL_CASH_IN > 0
 --   OR   TOTAL_OUTPUTS > 0
 --   OR   PRIZE_TAX   > 0
 --   OR   TAXES > 0
 
 END

IF @pReport = 5
 BEGIN
    -- UNBALANCED
    
SELECT   CT_NAME + ' - ' + GU_USERNAME AS [SESSION]
       , CS_COLLECTED_AMOUNT - CS_BALANCE AS BALANCE
       , CS_BALANCE AS IN_CASH
       , CS_COLLECTED_AMOUNT AS COLLECTED
       --, CS_OPENING_DATE, CS_CLOSING_DATE
  FROM   CASHIER_SESSIONS
 INNER   JOIN CASHIER_TERMINALS ON CS_CASHIER_ID = CT_CASHIER_ID
 INNER   JOIN GUI_USERS ON CS_USER_ID = GU_USER_ID
 WHERE   CS_BALANCE <> CS_COLLECTED_AMOUNT
   AND   CS_CLOSING_DATE IS NOT NULL
   --AND   (CS_OPENING_DATE > @pConcatDate AND CS_OPENING_DATE <= DATEADD(DAY, 1, @pConcatDate))
   AND   (CS_OPENING_DATE > @pConcatDate AND CS_OPENING_DATE <= DATEADD(DAY, 1, @pConcatDate))
   AND   ((CS_COLLECTED_AMOUNT - CS_BALANCE) <> 0)
 END

END

GO


