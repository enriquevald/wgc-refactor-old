USE [wgdb_000]
GO

/****** Object:  StoredProcedure [dbo].[Layout_SaveFloor_NEW]    Script Date: 03/11/2015 07:39:08 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/*
  Save floor definition data
*/
CREATE PROCEDURE [dbo].[Layout_SaveFloor_NEW]
--ALTER PROCEDURE [dbo].[Layout_SaveFloor_NEW]
   @pGeometry AS VARCHAR(MAX)
 , @pImageMap AS VARCHAR(MAX)
 , @pColor AS VARCHAR(20)
 , @pName AS VARCHAR(50)
 , @pUnit AS INT
 , @pWidth AS INT
 , @pHeight AS INT
AS
BEGIN

DECLARE @_FloorId AS INT

SET @_FloorId = (SELECT lf_floor_id FROM layout_floors WHERE lf_name = @pName  )

IF @_FloorId IS NULL
  BEGIN
      -- New Floor
      INSERT   INTO layout_floors
               (lf_geometry, lf_image_map, lf_color, lf_name, lf_metric, lf_width, lf_height) 
      VALUES   (@pGeometry, @pImageMap, @pColor, @pName, @pUnit, @pWidth, @pHeight)   
      
      SET @_FloorId = SCOPE_IDENTITY()
            
  END
ELSE
  BEGIN
      -- Update existing floor
      UPDATE   layout_floors
         SET   lf_geometry = @pGeometry
             , lf_image_map = @pImageMap
             , lf_color = @pColor
             , lf_name = @pName
             , lf_metric = @pUnit
             , lf_width = @pWidth
             , lf_height = @pHeight
       WHERE   lf_floor_id = @_FloorId
  END

SELECT   @_FloorId
  	  
END -- Layout_SaveFloor

GO


