USE [wgdb_000]
GO

/****** Object:  StoredProcedure [dbo].[Layout_GetPlaySessionsOpen]    Script Date: 03/11/2015 06:57:16 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



--CREATE PROCEDURE [dbo].[Layout_GetPlaySessionsOpen]
CREATE PROCEDURE [dbo].[Layout_GetPlaySessionsOpen] 

AS
BEGIN

SELECT  PS_PLAY_SESSION_ID
      , PS_ACCOUNT_ID
      , PS_TERMINAL_ID
      , TE_NAME
      , TE_PROVIDER_ID
      , PS_PLAYED_COUNT
      , PS_PLAYED_AMOUNT
      , GETDATE() AS LAST_UPDATE
  FROM   PLAY_SESSIONS
  INNER JOIN TERMINALS ON PS_TERMINAL_ID = TE_TERMINAL_ID
 WHERE PS_STATUS=0 
   AND PS_TYPE=2
   

END


GO


