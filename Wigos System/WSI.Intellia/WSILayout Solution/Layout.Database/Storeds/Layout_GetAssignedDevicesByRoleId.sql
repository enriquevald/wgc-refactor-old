USE [wgdb_000]
GO
/****** Object:  StoredProcedure [dbo].[Layout_GetAssignedDevicesByRoleId]    Script Date: 02/11/2015 04:30:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Layout_GetAssignedDevicesByRoleId]
      @pRoleId INT
AS
BEGIN
      SELECT DISTINCT(GU_USER_ID)
	  FROM GUI_USERS
	  WHERE GU_PROFILE_ID IN (SELECT GPF_PROFILE_ID
         FROM   GUI_PROFILE_FORMS
                        WHERE  GPF_GUI_ID = 202 
         AND    GPF_READ_PERM <> 0
                        AND (GPF_FORM_ID / 1000000) = @pRoleId)
                  
END --[Layout_GetAssignedDeviceByUserId]