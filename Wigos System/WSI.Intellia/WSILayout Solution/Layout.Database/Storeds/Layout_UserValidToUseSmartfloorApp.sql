USE [wgdb_000]
GO

/****** Object:  StoredProcedure [dbo].[Layout_UserValidToUseSmartfloorApp]    Script Date: 03/11/2015 07:44:46 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		CARLOS RODRIGO
-- Create date: 23102015
-- Description:	this Stored Procedure seek validate that the user has the correct permission to use de mobile application
-- =============================================
CREATE PROCEDURE [dbo].[Layout_UserValidToUseSmartfloorApp] 
	@pUserId bigint,
	@pResult int output
AS
BEGIN
	SELECT @pResult = COUNT(*) FROM GUI_USERS
	INNER JOIN GUI_USER_PROFILES ON GUP_PROFILE_ID=GU_PROFILE_ID
	INNER JOIN GUI_PROFILE_FORMS ON GPF_PROFILE_ID=GUP_PROFILE_ID
	INNER JOIN GUI_FORMS ON GF_FORM_ID=GPF_FORM_ID
	WHERE GU_USER_ID=@pUserId  AND GF_GUI_ID=203 AND GF_FORM_ID=2 AND GPF_GUI_ID=203
END

GO


