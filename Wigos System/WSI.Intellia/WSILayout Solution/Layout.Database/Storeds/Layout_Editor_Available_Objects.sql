USE [wgdb_000]
GO
/****** Object:  StoredProcedure [dbo].[Layout_Editor_Available_Objects]    Script Date: 02/11/2015 04:24:17 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Layout_Editor_Available_Objects]
AS
BEGIN
  -- TODO: Deleted?


  SELECT   LOC_FLOOR_NUMBER               AS FLOOR_ID
         , LO_ID                          AS OBJ_ID
         , ISNULL(LO_TYPE,1)              AS OBJ_TYPE
         , TE_TERMINAL_ID                 AS OBJ_IDENTITY
         , TE_NAME                        AS OBJ_NAME
         , TE_BANK_ID                     AS BANK_ID
         , BK_NAME                        AS BANK_NAME
         , LOL_OFFSET_X                   AS X
         , LOL_OFFSET_Y                   AS X
         , LOL_ORIENTATION                AS ORIENTATION
    FROM   TERMINALS
    LEFT   JOIN LAYOUT_OBJECTS ON LO_EXTERNAL_ID = TE_TERMINAL_ID
    LEFT   JOIN LAYOUT_OBJECT_LOCATIONS ON LO_ID = LOL_OBJECT_ID
    LEFT   JOIN LAYOUT_LOCATIONS ON LOL_LOCATION_ID = LOC_ID
    LEFT   JOIN BANKS ON TE_BANK_ID = BK_BANK_ID
   
   UNION   ALL
  
  SELECT   LOC_FLOOR_NUMBER   AS FLOOR_ID
         , LO_ID              AS OBJ_ID
         , LD_TYPE            AS OBJ_TYPE
         , LD_ID              AS OBJ_IDENTITY
         , LD_DEVICE_ID       AS OBJ_NAME
         , NULL               AS BANK_ID
         , NULL               AS BANK_NAME
         , LOL_OFFSET_X       AS X
         , LOL_OFFSET_Y       AS Y
         , 0                  AS ORIENTATION
    FROM   LAYOUT_DEVICES 
    LEFT   JOIN LAYOUT_OBJECTS ON LD_ID = LO_EXTERNAL_ID AND LD_TYPE = 70
    LEFT   JOIN LAYOUT_OBJECT_LOCATIONS ON LO_ID = LOL_LOCATION_ID 
    LEFT   JOIN LAYOUT_LOCATIONS ON LOL_LOCATION_ID = LOC_ID
   WHERE   LD_TYPE = 70 

  ---- TERMINALS
  --SELECT   LOP_FLOOR_ID
  --       , LO_ID 
  --       , ISNULL(LO_TYPE,1) AS OBJ_TYPE
  --       , TE_TERMINAL_ID AS OBJ_IDENTITY
  --       , TE_NAME AS OBJ_NAME
  --       , TE_BANK_ID  AS OBJ_BANK
  --       , BK_NAME
  --       , LO_TYPE
  --       , LOP_X
  --       , LOP_Y
  --       , LOP_ORIENTATION
  --  FROM   LAYOUT_OBJECTS
  -- RIGHT   JOIN TERMINALS ON LO_EXTERNAL_ID = TE_TERMINAL_ID
  --  LEFT   JOIN LAYOUT_OBJECTS_POSITIONS ON LO_ID = LOP_ID
  -- INNER   JOIN BANKS ON TE_BANK_ID = BK_BANK_ID
   
  --UNION ALL
  ---- GAMING TABLES
  --SELECT   LOP_FLOOR_ID
  --       , LO_ID 
  --       , 2 AS OBJ_TYPE
  --       , GT_GAMING_TABLE_ID AS OBJ_IDENTITY
  --       , GT_NAME AS OBJ_NAME
  --       , GT_BANK_ID AS OBJ_BANK
  --       , BK_NAME
  --       , LO_TYPE
  --       , LOP_X
  --       , LOP_Y
  --       , LOP_ORIENTATION
  --  FROM   LAYOUT_OBJECTS
  -- RIGHT   JOIN GAMING_TABLES ON LO_EXTERNAL_ID = GT_GAMING_TABLE_ID
  --  LEFT   JOIN LAYOUT_OBJECTS_POSITIONS ON LO_ID = LOP_ID
  -- INNER   JOIN BANKS ON GT_BANK_ID = BK_BANK_ID
END