USE [wgdb_000]
GO
/****** Object:  StoredProcedure [dbo].[Layout_GetFloors]    Script Date: 02/11/2015 05:15:31 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
  Obtain the list of available Layout Floors
*/
--CREATE PROCEDURE [dbo].[Layout_GetFloors]
CREATE PROCEDURE [dbo].[Layout_GetFloors]
AS
BEGIN

SELECT   lf_floor_id
       , lf_name
       , lf_height
       --, lf_geometry
       , '' as lf_geometry
       , lf_image_map
       , lf_color
       , lf_width
       , lf_metric
  FROM   layout_floors
  	  
END -- Layout_GetFloors

