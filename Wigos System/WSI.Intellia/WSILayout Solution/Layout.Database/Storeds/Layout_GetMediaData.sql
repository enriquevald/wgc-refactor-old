USE [wgdb_000]
GO
/****** Object:  StoredProcedure [dbo].[Layout_GetMediaData]    Script Date: 02/11/2015 05:18:38 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Layout_GetMediaData]
      @pId INT
    , @pMediaData nvarchar(MAX) OUTPUT
AS
BEGIN
	SELECT @pMediaData= CAST('' as xml).value('xs:base64Binary(sql:column("LM_DATA"))','varchar(max)')
	  FROM LAYOUT_MEDIA
	 WHERE LM_ID = @pId
END -- Layout_GetRanges


