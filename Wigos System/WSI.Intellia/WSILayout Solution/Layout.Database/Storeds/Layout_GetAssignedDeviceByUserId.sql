USE [wgdb_000]
GO
/****** Object:  StoredProcedure [dbo].[Layout_GetAssignedDeviceByUserId]    Script Date: 02/11/2015 04:29:16 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Layout_GetAssignedDeviceByUserId]
      @pUserId INT
    , @pDeviceId Nvarchar(50) OUTPUT
AS
BEGIN
      SELECT @pDeviceId = ld.Ld_device_id
      FROM layout_devices ld
      WHERE ld.ld_user_id = @pUserId
                  
END --[Layout_GetAssignedDeviceByUserId]