USE [wgdb_000]
GO
/****** Object:  StoredProcedure [dbo].[Layout_Get_Resources]    Script Date: 02/11/2015 04:27:03 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Layout_Get_Resources] 
--ALTER PROCEDURE Layout_Get_Resources 
AS 
BEGIN

  SELECT   lme_mesh_id
         , lme_type
         , lme_sub_type
         , lme_geometry
    FROM   layout_meshes
    
END