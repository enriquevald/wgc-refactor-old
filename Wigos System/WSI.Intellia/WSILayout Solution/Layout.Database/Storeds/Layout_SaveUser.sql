USE [wgdb_000]
GO

/****** Object:  StoredProcedure [dbo].[Layout_SaveUser]    Script Date: 03/11/2015 07:41:11 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Layout_SaveUser]
      @pUserId INT
    , @pIsManager BIT
AS
BEGIN


      IF EXISTS (SELECT TOP 1 * FROM LAYOUT_USERS_CONFIGURATION WHERE LC_USER_ID = @pUserId)
            UPDATE	LAYOUT_USERS_CONFIGURATION 
               SET	LC_IS_MANAGER = @pIsManager
             WHERE	LC_USER_ID = @pUserId

      ELSE
       INSERT INTO	LAYOUT_USERS_CONFIGURATION
            VALUES 
				 (	@pUserId
                 ,	''
                 ,  NULL
                 ,	GETDATE()
                 ,  @pIsManager
				 ) 
      
END --Layout_SaveConfiguration


GO


