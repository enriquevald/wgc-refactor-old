USE [wgdb_000]
GO

/****** Object:  StoredProcedure [dbo].[Layout_LoadFloor]    Script Date: 03/11/2015 07:01:14 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[Layout_LoadFloor]
--ALTER PROCEDURE [dbo].[Layout_LoadFloor]
(
--    @pExternalObjectId INT = NULL
--  , 
      @pFloorId INT = NULL   
)      
AS
BEGIN

SELECT   loc_id
       , loc_x
       , loc_y
       , loc_creation
       , loc_type
       , loc_floor_number
       , loc_grid_id
       , loc_status
  FROM   layout_locations       
 WHERE   loc_floor_number = @pFloorId

SELECT   lo_id
       , lo_external_id
       , lo_type
       , lo_parent_id
       , lo_mesh_id
       , lo_creation_date
       , lo_update_date
       , lo_status
       , lol_object_id
       , lol_location_id
       , lol_date_from
       , lol_date_to
       , lol_orientation
       , lol_area_id
       , lol_bank_id
       , lol_bank_location
       , lol_current_location
       , lol_offset_x
       , lol_offset_y
  FROM   layout_objects
  LEFT   JOIN layout_object_locations ON lol_object_id   = lo_id
  LEFT   JOIN layout_locations        ON lol_location_id = loc_id
 WHERE   loc_floor_number             = @pFloorId
   AND   lol_current_location         = 1

END --Layout_GetObjectInfo



GO


