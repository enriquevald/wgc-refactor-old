USE [wgdb_000]
GO

/****** Object:  StoredProcedure [dbo].[Layout_SaveManagerRunnerMessage]    Script Date: 03/11/2015 07:39:49 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Layout_SaveManagerRunnerMessage] 
	@pManagerId BIGINT,
	@pRunnerId BIGINT,
	@pSource INT,
	@pMessage TEXT,
	@new_identity    INT    OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	INSERT INTO LAYOUT_MANAGER_RUNNER_MESSAGES
	(
		LMRM_MANAGER_ID,
		LMRM_RUNNER_ID,
		LMRM_MESSAGE,
		LMRM_SOURCE,
		LMRM_DATE
	)VALUES
	(
		@pManagerId,
		@pRunnerId,
		@pMessage,
		@pSource,
		GETDATE()
	)

	SELECT @new_identity = SCOPE_IDENTITY()

    SELECT @new_identity AS id
END

GO


