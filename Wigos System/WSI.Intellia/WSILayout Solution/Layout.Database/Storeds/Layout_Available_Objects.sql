USE [wgdb_000]
GO
/****** Object:  StoredProcedure [dbo].[Layout_Available_Objects]    Script Date: 02/11/2015 04:19:55 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[Layout_Available_Objects]    Script Date: 11/10/2014 16:26:26 ******/



CREATE PROCEDURE [dbo].[Layout_Available_Objects]  

                             @pValidTerminalTypes   AS VARCHAR(MAX) = NULL
AS
BEGIN

  --DECLARE @pValidTerminalTypes AS VARCHAR(MAX)
  --SET @pValidTerminalTypes = NULL

  DECLARE @_DELIMITER                 AS CHAR(1)
  DECLARE @_VALID_TERMINALS_TYPES        TABLE(SST_ID INT, SST_VALUE VARCHAR(50))

  SET @_DELIMITER            = ','
  
  IF @pValidTerminalTypes IS NULL
  BEGIN
    SET @pValidTerminalTypes = '-1, 100, 105'
  END
  
  INSERT   INTO @_VALID_TERMINALS_TYPES 
  SELECT   * 
    FROM   dbo.SplitStringIntoTable(@pValidTerminalTypes, @_DELIMITER, DEFAULT)

  -- Get terminals not in layout
  SELECT   te_terminal_id
         , te_floor_id
         , te_bank_id
         , te_terminal_type
         , te_name
         , te_provider_id 
         , te_status
         , te_active
    FROM   terminals 
   WHERE   (te_terminal_type NOT IN (SELECT SST_VALUE FROM @_VALID_TERMINALS_TYPES)) 
     AND   (te_status = 0) 
     AND   (te_active = 1)
     AND   (te_terminal_id NOT IN (SELECT DISTINCT lo_external_id FROM layout_objects))
   ORDER   BY te_provider_id

END

