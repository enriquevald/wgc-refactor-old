CREATE PROCEDURE [dbo].[Layout_Object_Update]        
                        @pObjectId        BIGINT
                       ,@pPositionDate    DATETIME
                       ,@pPosX            INTEGER         
                       ,@pPosY            INTEGER
                       ,@pPosZ            INTEGER
                       ,@pOrientation     INTEGER
                       ,@pFloorId         BIGINT
                       ,@pAreaId          BIGINT
                       ,@pBankId          BIGINT
                       ,@pExternalId      BIGINT
                       ,@pType            INTEGER
                       ,@pParentId        BIGINT   = NULL 
                       ,@pMeshId          INTEGER
                       ,@pCreationDate    DATETIME
                       ,@pUpdateDate      DATETIME
AS
BEGIN 

   INSERT   INTO LAYOUT_OBJECTS_POSITIONS 
          ( lop_id
          , lop_position_date
          , lop_x
          , lop_y
          , lop_z
          , lop_orientation
          , lop_floor_id
          , lop_area_id
          , lop_bank_id )
   VALUES ( @pObjectId
          , @pPositionDate
          , @pPosX
          , @pPosY
          , @pPosZ
          , @pOrientation
          , @pFloorId
          , @pAreaId
          , @pBankId )

   UPDATE   LAYOUT_OBJECTS
      SET   lo_external_id = @pExternalId
          , lo_type = @pType
          , lo_parent_id = @pParentId
          , lo_mesh_id = @pMeshId
          , lo_update_date = @pUpdateDate
    WHERE   lo_id = @pObjectId

   IF @@ROWCOUNT <> 1
   BEGIN
      INSERT   INTO LAYOUT_OBJECTS
             ( lo_id
             , lo_external_id
             , lo_type
             , lo_parent_id
             , lo_mesh_id
             , lo_creation_date )
      VALUES ( @pObjectId
             , @pExternalId
             , @pType
             , @pParentId 
             , @pMeshId
             , @pCreationDate )       
   END

END