USE [wgdb_000]
GO
/****** Object:  StoredProcedure [dbo].[Layout_AutoNotificationAlarm]    Script Date: 02/11/2015 04:18:55 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Layout_AutoNotificationAlarm]
     @pStatus INT
    , @pCategory INT
    , @pSubcategory INT
    , @pTerminalId INT
    , @pSeverity INT
    , @pCreationUserId INT
    , @pCreation DATETIME
    , @pAssignedRoleId INT
    , @pAssigned DATETIME
    , @pHistory NVARCHAR(MAX)
    , @pDateCreated DATETIME
    , @poutTaskId INT OUTPUT
AS
BEGIN
DECLARE @TaskId as INT

exec Layout_SaveSiteTask -1,@pStatus,NULL,NULL,@pCategory,@pSubcategory,@pTerminalId,NULL,NULL,@pSeverity,@pCreationUserId,@pCreation,@pAssignedRoleId,NULL,@pAssigned,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,@pAssigned,NULL,@pHistory,@TaskId OUTPUT

  UPDATE LAYOUT_SITE_ALARMS
     SET LSA_TASK_ID      = @TaskId
       , LSA_DATE_TO_TASK = @pDateCreated
   WHERE LSA_TERMINAL_ID  = @pTerminalId
     AND LSA_ALARM_ID     = @pSubcategory
     AND LSA_DATE_CREATED = @pDateCreated
     
     SELECT @poutTaskId = @TaskId;
END 

