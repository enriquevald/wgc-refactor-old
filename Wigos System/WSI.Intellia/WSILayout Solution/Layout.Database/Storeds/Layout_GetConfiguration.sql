CREATE PROCEDURE [dbo].[Layout_GetConfiguration]
	@pUserId INT
AS
BEGIN

      SELECT   LC_PARAMETERS
             , LC_DASHBOARD
        FROM   LAYOUT_USERS_CONFIGURATION
       WHERE   LC_USER_ID = @pUserId
	  
END -- Layout_GetConfiguration


GO