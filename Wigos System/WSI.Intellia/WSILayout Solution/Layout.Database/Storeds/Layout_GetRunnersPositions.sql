USE [wgdb_000]
GO

/****** Object:  StoredProcedure [dbo].[Layout_GetRunnersPositions]    Script Date: 03/11/2015 06:58:01 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Layout_GetRunnersPositions]
--ALTER PROCEDURE Layout_GetRunnersPositions
AS
BEGIN
  
  SELECT   GU_USER_ID
         , LRP_DEVICE_ID
         , LRP_FLOOR_ID
         , LRP_X
         , LRP_Y
         , LRP_RANGE
         , LRP_LAST_UPDATE
    FROM   GUI_USERS
   INNER   JOIN LAYOUT_USERS_CONFIGURATION ON GU_USER_ID = LC_USER_ID 
    LEFT   JOIN LAYOUT_RUNNERS_POSITION    ON GU_USER_ID = LRP_USER_ID
   WHERE   GU_PROFILE_ID IN (SELECT   GPF_PROFILE_ID
                               FROM   GUI_PROFILE_FORMS
                              WHERE   GPF_GUI_ID = 202 
                                AND   GPF_READ_PERM <> 0
                                AND   (GPF_FORM_ID / 1000000) = 4) -- SLOT ATTENDATS
    
END
GO


