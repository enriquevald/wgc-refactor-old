USE [wgdb_000]
GO

/****** Object:  StoredProcedure [dbo].[Layout_GetUserPermissions]    Script Date: 03/11/2015 07:00:22 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Layout_GetUserPermissions]
      @pUserId INT
AS
BEGIN
      
 SELECT   GPF_FORM_ID
      ,   GPF_READ_PERM
      ,   GPF_WRITE_PERM
      ,   GPF_DELETE_PERM
      ,   GPF_EXECUTE_PERM
   FROM   GUI_PROFILE_FORMS
  WHERE   GPF_GUI_ID = 203 AND GPF_READ_PERM <> 0 
    AND   GPF_PROFILE_ID = 
 (
     SELECT GU_PROFILE_ID 
       FROM GUI_USERS 
      WHERE GU_USER_ID = @pUserId
 )
        
END -- Layout_GetUserPermissions



GO


