USE [wgdb_000]
GO

/****** Object:  StoredProcedure [dbo].[Layout_GetTerminalStatistics]    Script Date: 03/11/2015 06:59:30 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/****** Object:  StoredProcedure [dbo].[Layout_GetTerminalStatistics]    Script Date: 11/10/2014 16:26:26 ******/



---
-- Layout_GetActivity
---

CREATE PROCEDURE [dbo].[Layout_GetTerminalStatistics]
      @pGameId	INT = NULL
    , @pFrom	DATETIME = NULL
    , @pTo		DATETIME = NULL
AS
BEGIN

	SELECT  SPH_TERMINAL_ID 
		  , terminal
		  , PlayedAmount
		  , WonAmount
		  , CASE x.PlayedAmount WHEN 0 THEN 0 ELSE (x.WonAmount*100/x.PlayedAmount) END as AmountPerCent
		  , PlayedAmount-WonAmount as Netwin
		  , CASE x.PlayedAmount WHEN 0 THEN 0 ELSE ((x.PlayedAmount-x.WonAmount)*100/x.PlayedAmount) END as NetwinPc
		  , PlayedCount
		  , WonCount
		  , CASE x.PlayedCount WHEN 0 THEN 0 ELSE((x.WonCount* 100.0)/x.PlayedCount) END as CountPerCent 
		  , Provider
		  , ISNULL(MinGameId,-1) AS MinGameId
		  , ISNULL(MaxGameId,-1) AS MaxGameId
		  , MinGameName
		  , CASE x.PlayedAmount WHEN 0 THEN 0 ELSE(x.TheoreticalWonAmount*100/PlayedAmount) END AS TheoreticalAmountPc 
		  , TheoreticalWonAmount 
	 FROM 
		  (SELECT SPH_TERMINAL_ID, 
				   sum(SPH_PLAYED_AMOUNT)as PlayedAmount
				 , sum(SPH_WON_AMOUNT) AS WonAmount
				 , sum(SPH_PLAYED_COUNT) as PlayedCount
				 , sum(SPH_WON_COUNT) AS WonCount
				 , (SELECT te_name 
				      FROM terminals 
				     WHERE(te_terminal_id = SALES_PER_HOUR_V2.SPH_terminal_id)) AS terminal
				 , (SELECT te_provider_id 
				      FROM terminals 
				     WHERE(te_terminal_id = SALES_PER_HOUR_V2.SPH_terminal_id)) AS Provider
				 , min(SPH_GAME_ID) AS MinGameId
				 , max(SPH_GAME_ID) AS MaxGameId
				 , ISNULL((SELECT PG_GAME_NAME AS GM_NAME 
				             FROM PROVIDERS_GAMES 
				            WHERE PG_GAME_ID = min(SPH_GAME_ID)),'UNKNOWN') AS MinGameName
				 , sum(SPH_theoretical_won_amount) AS TheoreticalWonAmount 
		   FROM SALES_PER_HOUR_V2
		   
		  WHERE (SPH_BASE_HOUR >= ISNULL(@pFrom, '2000-01-01')) 
			AND (SPH_BASE_HOUR < ISNULL(@pTo, GETDATE())) 
			AND SPH_TERMINAL_ID IN  (SELECT TE_TERMINAL_ID 
			                           FROM TERMINALS  
			                          WHERE  TE_TERMINAL_TYPE IN ( 1, 3, 5, 106) 
			                         ) 
			AND SPH_GAME_ID = ISNULL(@pGameId, SPH_GAME_ID) 
	   GROUP BY SPH_TERMINAL_ID 
	   
			)x ORDER BY Provider, terminal
  
END -- Layout_GetActivity


GO


