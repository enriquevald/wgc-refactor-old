USE [wgdb_000]
GO
/****** Object:  StoredProcedure [dbo].[Layout_GetFloorResources]    Script Date: 02/11/2015 05:14:33 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Layout_GetFloorResources] (
--ALTER PROCEDURE Layout_GetFloorResources (
  @pFloorId AS INT
) AS 
BEGIN

  SELECT * FROM (
    SELECT   lr_id
           , lr_type
           , lr_sub_type
           , lr_name
           , lr_data
           , lr_options
      FROM   layout_resources
     WHERE   (lr_type = 0 AND lr_id IN ( SELECT   DISTINCT lo_mesh_id 
                                          FROM   layout_objects
                                          LEFT   JOIN layout_object_locations ON lol_object_id   = lo_id
                                          LEFT   JOIN layout_locations        ON lol_location_id = loc_id
                                         WHERE   loc_floor_number             = @pFloorId
                                           AND   lol_current_location         = 1 ))
        OR   (lr_type = 1 AND lr_sub_type IN (SELECT   DISTINCT lo_mesh_id 
                                                FROM   layout_objects
                                                LEFT   JOIN layout_object_locations ON lol_object_id   = lo_id
                                                LEFT   JOIN layout_locations        ON lol_location_id = loc_id
                                               WHERE   loc_floor_number             = @pFloorId
                                                 AND   lol_current_location         = 1 ))
     UNION
    SELECT   lr_id
           , lr_type
           , lr_sub_type
           , lr_name
           , lr_data
           , lr_options
      FROM   layout_resources
     WHERE   (lr_type = 0 AND lr_sub_type IN ( 200, 201, 500 ))
        OR   (lr_type = 1 AND lr_sub_type IN ( SELECT   lr_id
                                                 FROM   layout_resources
                                                WHERE   lr_sub_type IN (200, 201, 500)))

     UNION
    SELECT   lr_id
           , lr_type
           , lr_sub_type
           , lr_name
           , lr_data
           , lr_options
      FROM   layout_resources
     WHERE   (lr_type = 1 AND lr_sub_type = 100)
           
  ) AS TRESOURCES ORDER BY 2 DESC
  
END
