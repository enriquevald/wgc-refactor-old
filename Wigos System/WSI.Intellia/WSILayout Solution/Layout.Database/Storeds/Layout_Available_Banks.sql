USE [wgdb_000]
GO
/****** Object:  StoredProcedure [dbo].[Layout_Available_Banks]    Script Date: 02/11/2015 04:17:34 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		  Ramón Monclús
-- Create date: 03/31/2015
-- Description:	Obtains the list of avaible banks to add into Layout
-- =============================================

CREATE PROCEDURE [dbo].[Layout_Available_Banks]
AS
BEGIN
	SET NOCOUNT ON;

SELECT   BK_BANK_ID
       , BK_AREA_ID
       , TE_TERMINAL_ID
       , BK_NAME
  FROM   BANKS  
  LEFT   JOIN TERMINALS ON BANKS.BK_BANK_ID = TERMINALS.TE_BANK_ID
 WHERE   BK_BANK_ID NOT IN (SELECT   DISTINCT(TABLE_OBJECTS.LOP_BANK_ID)
                              FROM   (SELECT   LOP.LOP_ID,
                                               LOP.LOP_X,
                                               LOP.LOP_Y,
                                               LOP.LOP_Z,
                                               LOP.LOP_ORIENTATION,
                                               LOP.LOP_FLOOR_ID,
                                               LOP.LOP_AREA_ID,
                                               LOP.LOP_BANK_ID,
                                               LOP.LOP_POSITION_DATE
                                        FROM   LAYOUT_OBJECTS_POSITIONS LOP
                                       INNER   JOIN
                                                   (SELECT   LOP_ID
                                                           , MAX(LOP_POSITION_DATE) AS LOP_POSITION_DATE
                                                      FROM   LAYOUT_OBJECTS_POSITIONS
                                                     GROUP   BY LOP_ID) AS G_LOP 
                                                        ON   LOP.LOP_ID            = G_LOP.LOP_ID 
                                                       AND   LOP.LOP_POSITION_DATE = G_LOP.LOP_POSITION_DATE) TABLE_OBJECTS) 
                                                       
END
