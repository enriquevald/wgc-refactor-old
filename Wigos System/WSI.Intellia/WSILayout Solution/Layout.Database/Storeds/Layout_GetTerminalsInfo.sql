USE [wgdb_000]
GO

/****** Object:  StoredProcedure [dbo].[Layout_GetTerminalsInfo]    Script Date: 03/11/2015 06:59:11 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Layout_GetTerminalsInfo]
AS
BEGIN
   SELECT   [TE_TERMINAL_ID]
		      , [TE_TYPE]
		      , [TE_SERVER_ID]
		      , [TE_NAME]
		      , [TE_EXTERNAL_ID]
		      , [TE_BLOCKED]
		      , [TE_TIMESTAMP]
		      , [TE_ACTIVE]
		      , [TE_PROVIDER_ID]
		      , [TE_CLIENT_ID]
		      , [TE_BUILD_ID]
		      , [TE_TERMINAL_TYPE]
		      , [TE_VENDOR_ID]
		      , [TE_UNIQUE_EXTERNAL_ID]
		      , [TE_STATUS]
		      , [TE_RETIREMENT_DATE]
		      , [TE_RETIREMENT_REQUESTED]
		      , [TE_DENOMINATION]
		      , [TE_MULTI_DENOMINATION]
		      , [TE_PROGRAM]
		      , [TE_THEORETICAL_PAYOUT]
		      , [TE_THEORETICAL_HOLD]
		      , [TE_PROV_ID]
		      , [TE_BANK_ID]
		      , [TE_FLOOR_ID]
		      , [TE_GAME_TYPE]
		      , [TE_ACTIVATION_DATE]
		      , [TE_CURRENT_ACCOUNT_ID]
		      , [TE_CURRENT_PLAY_SESSION_ID]
		      , [TE_REGISTRATION_CODE]
		      , [TE_SAS_FLAGS]
		      , [TE_SERIAL_NUMBER]
		      , [TE_CABINET_TYPE]
		      , [TE_JACKPOT_CONTRIBUTION_PCT]
		      , [TE_CONTRACT_TYPE]
		      , [TE_CONTRACT_ID]
		      , [TE_ORDER_NUMBER]
		      , [TE_WXP_REPORTED_STATUS_DATETIME]
		      , [TE_WXP_REPORTED_STATUS]
		      , [TE_SEQUENCE_ID]
		      , [TE_VALIDATION_TYPE]
		      , [TE_ALLOWED_CASHABLE_EMISSION]
		      , [TE_ALLOWED_PROMO_EMISSION]
		      , [TE_ALLOWED_REDEMPTION]
		      , [TE_MAX_ALLOWED_TI]
		      , [TE_MAX_ALLOWED_TO]
		      , [TE_SAS_VERSION]
		      , [TE_SAS_MACHINE_NAME]
		      , [TE_BONUS_FLAGS]
		      , [TE_FEATURES_BYTES]
		      , [TE_VIRTUAL_ACCOUNT_ID]
		      , [TE_SAS_FLAGS_USE_SITE_DEFAULT]
		      , [TE_AUTHENTICATION_METHOD]
		      , [TE_AUTHENTICATION_SEED]
		      , [TE_AUTHENTICATION_SIGNATURE]
		      , [TE_AUTHENTICATION_STATUS]
		      , [TE_AUTHENTICATION_LAST_CHECKED]
		      , [TE_MACHINE_ID]
		      , [TE_POSITION]
		      , [TE_TOP_AWARD]
		      , [TE_MAX_BET]
		      , [TE_NUMBER_LINES]
		      , [TE_TERMINAL_DENOMINATION]
		      , [TE_GAME_THEME]
		      , [TE_METER_DELTA_ID]
     FROM   [TERMINALS]
    WHERE   te_terminal_type not in (-1, 100)
END

GO


