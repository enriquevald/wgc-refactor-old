USE [wgdb_000]
GO

/****** Object:  StoredProcedure [dbo].[Layout_SaveCustomFilter]    Script Date: 03/11/2015 07:38:24 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Layout_SaveCustomFilter]
      @pUserId INT
    , @pFilterType INT
    , @pCustomFilterName NVARCHAR(50)
    , @pParameters NVARCHAR(MAX)
    , @pHasSound BIT
    , @pColor NVARCHAR(30)
    , @pFilterId INT
    , @pOutputFilterId INT OUTPUT
    , @pEnabled BIT
AS
BEGIN
      IF EXISTS (SELECT TOP 1 * 
                   FROM LAYOUT_USERS_CUSTOM_FILTERS
                  WHERE	LCF_ID = @pFilterId)
            UPDATE	LAYOUT_USERS_CUSTOM_FILTERS 
               SET	LCF_LAST_UPDATE = GETDATE()
                 ,	LCF_HAS_SOUND = @pHasSound
                 ,	LCF_COLOR = @pColor
                 ,	LCF_ENABLED = @pEnabled
                 ,	LCF_NAME = @pCustomFilterName
             WHERE	LCF_ID = @pFilterId
      ELSE
       INSERT INTO	LAYOUT_USERS_CUSTOM_FILTERS
            VALUES 
				 (	@pUserId
				         ,	@pFilterType
				         ,	@pCustomFilterName
                 ,	@pParameters
                 ,	1
                 ,  GETDATE()
                 ,  @pHasSound
                 ,  @pColor
				 ) 
           SELECT @pOutputFilterId=SCOPE_IDENTITY();
END --Layout_SaveConfiguration


GO


