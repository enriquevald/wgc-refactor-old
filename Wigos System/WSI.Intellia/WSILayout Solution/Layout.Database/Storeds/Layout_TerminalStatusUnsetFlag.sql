USE [wgdb_000]
GO

/****** Object:  StoredProcedure [dbo].[Layout_TerminalStatusUnsetFlag]    Script Date: 03/11/2015 07:44:13 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Layout_TerminalStatusUnsetFlag]
      @pColumn NVARCHAR(MAX)
    , @pFlag INT
    , @pTerminalId INT
AS
BEGIN

 DECLARE @sql NVARCHAR(MAX)=  'UPDATE   TERMINAL_STATUS 
                                  SET '+@pColumn+' = '+@pColumn +' & (~'+CAST(@pFlag AS VARCHAR(50))+') 
                                WHERE   TS_TERMINAL_ID ='+CAST(@pTerminalId AS VARCHAR(50))+''
       
       exec sp_executesql @sql

END

GO


