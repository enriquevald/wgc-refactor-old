USE [wgdb_000]
GO
/****** Object:  StoredProcedure [dbo].[Layout_GetConversationHistory]    Script Date: 02/11/2015 05:11:28 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Layout_GetConversationHistory] 
	@pManagerId INT,
	@pRunnerId INT
AS
BEGIN
	SELECT LMRM_ID
		  ,LMRM_MANAGER_ID
	      ,U_MANAGER.GU_USERNAME as MANAGER_NAME
          ,LMRM_RUNNER_ID
		  ,U_RUNNER.GU_USERNAME as RUNNER_NAME
          ,LMRM_DATE
          ,LMRM_MESSAGE
           ,LMRM_SOURCE
	FROM LAYOUT_MANAGER_RUNNER_MESSAGES LMRM
	INNER JOIN GUI_USERS U_MANAGER ON LMRM.LMRM_MANAGER_ID = U_MANAGER.GU_USER_ID 
	INNER JOIN GUI_USERS U_RUNNER ON LMRM.LMRM_RUNNER_ID = U_RUNNER.GU_USER_ID 
	WHERE LMRM.LMRM_DATE >= CAST(GETDATE() AS DATE)
	AND LMRM_MANAGER_ID = @pManagerId
	AND LMRM_RUNNER_ID = @pRunnerId
	ORDER BY LMRM.LMRM_DATE
END
