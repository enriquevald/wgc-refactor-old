CREATE PROCEDURE [dbo].[Layout_Login] 
    @pUserName as NVARCHAR(MAX)
  , @pOrigin AS INT
  , @opUserId AS INT OUTPUT
  , @opUserName AS NVARCHAR(MAX) = '' OUTPUT
AS
BEGIN
--Origin puede ser Web=1/App=2

DECLARE @ErrorCode AS INT
DECLARE @BlockReason AS INT
DECLARE @UserId AS INT
DECLARE @HasPermission AS INT
DECLARE @UserName AS NVARCHAR(MAX)

SET @ErrorCode = 0


--Usuario existe
  SELECT   @BlockReason = GU_BLOCK_REASON
       ,   @UserId = GU_USER_ID
       ,   @UserName = GU_FULL_NAME
    FROM   GUI_USERS
   WHERE   GU_USERNAME = @pUserName

  IF @BlockReason IS NULL 
  BEGIN
    SET @ErrorCode = 1
  END
  ELSE IF @BlockReason <> 0 
  BEGIN
    SET @ErrorCode = 2  
  END

  IF @ErrorCode = 0 
  BEGIN
  -- Si tiene permisos (APP o WEB)
      SELECT @HasPermission = COUNT(*) 
        FROM GUI_USERS INNER JOIN GUI_USER_PROFILES ON GUP_PROFILE_ID = GU_PROFILE_ID
                       INNER JOIN GUI_PROFILE_FORMS ON GPF_PROFILE_ID = GUP_PROFILE_ID
                       INNER JOIN GUI_FORMS ON GF_FORM_ID = GPF_FORM_ID
       WHERE GU_USER_ID = @UserId  
         AND GF_GUI_ID = 203 
         AND GF_FORM_ID = @pOrigin 
         AND GPF_GUI_ID = 203
     
     IF @HasPermission = 0 
     BEGIN
      SET @ErrorCode = 3
      END
  END
        SELECT @opUserId = @UserId  
        SELECT @ErrorCode
        SELECT @opUserName = @UserName
END
GO


