USE [wgdb_000]
GO

/****** Object:  StoredProcedure [dbo].[Layout_SaveFloor]    Script Date: 03/11/2015 07:38:48 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/*
  Save floor definition data
*/
--CREATE PROCEDURE [dbo].[Layout_SaveFloor]
CREATE PROCEDURE [dbo].[Layout_SaveFloor]
   @pGeometry AS VARCHAR(MAX)
 , @pImageMap AS VARCHAR(MAX)
 , @pColor AS VARCHAR(20)
 , @pName AS VARCHAR(50)
 , @pHeight AS INT
AS
BEGIN

DECLARE @pFloorId AS INT

SET @pFloorId = (SELECT lf_floor_id FROM layout_floors WHERE lf_name = @pName  )

IF @pFloorId IS NULL
  BEGIN
      -- New Floor
      INSERT   INTO layout_floors
               (lf_geometry, lf_image_map, lf_color, lf_name, lf_height) 
      VALUES   (@pGeometry, @pImageMap, @pColor, @pName, @pHeight)   
      
      SET @pFloorId = SCOPE_IDENTITY()
  END
ELSE
  BEGIN
      -- Update existing floor
      UPDATE   layout_floors
         SET   lf_geometry = @pGeometry
             , lf_image_map = @pImageMap
             , lf_color = @pColor
             , lf_name = @pName
             , lf_height = @pHeight
       WHERE   lf_floor_id = @pFloorId
  END

SELECT   @pFloorId
  	  
END -- Layout_SaveFloor

GO


