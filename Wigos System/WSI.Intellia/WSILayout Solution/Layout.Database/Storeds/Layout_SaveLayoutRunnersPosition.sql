USE [wgdb_000]
GO
/****** Object:  StoredProcedure [dbo].[Layout_SaveLayoutRunnersPosition]    Script Date: 03/11/2015 07:39:20 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Layout_SaveLayoutRunnersPosition]
      @pUserId INT
    , @pDeviceId Nvarchar(50)
    , @pPositionX FLOAT
    , @pPositionY FLOAT
    , @pPositionFloor INT
AS
BEGIN
	IF EXISTS (SELECT TOP 1 *
				FROM layout_runners_position lrp
				WHERE lrp.lrp_device_id = @pDeviceId AND lrp.lrp_user_id = @pUserId)
			
		UPDATE	layout_runners_position
				SET lrp_x= @pPositionX,
					lrp_y = @pPositionY,
					lrp_last_update = GETDATE()
				WHERE lrp_device_id = @pDeviceId AND lrp_user_id = @pUserId
	ELSE INSERT INTO layout_runners_position	
                    (lrp_device_id,
                    lrp_user_id,
                    lrp_x,
                    lrp_y,
                    lrp_floor_id,
                    lrp_last_update)
                    VALUES
                    (@pDeviceId,
                    @pUserId,
                    @pPositionX,
                    @pPositionY,
                    @pPositionFloor,
                    GETDATE())
END --[Layout_SaveLayoutRunnersPosition]