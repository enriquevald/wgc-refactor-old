USE [wgdb_000]
GO

/****** Object:  StoredProcedure [dbo].[Layout_GetTodayActiveAlarms]    Script Date: 03/11/2015 07:00:04 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



--CREATE PROCEDURE [dbo].[Layout_GetTodayActiveAlarms]
CREATE PROCEDURE [dbo].[Layout_GetTodayActiveAlarms] 

AS
BEGIN
    SELECT  LSA_TERMINAL_ID
         ,  LSA_ALARM_ID
      FROM  LAYOUT_SITE_ALARMS LEFT JOIN LAYOUT_SITE_TASKS ON LSA_TASK_ID = LST_ID
     WHERE  LSA_DATE_CREATED >= dbo.TodayOpening(0) 
       AND  (LST_STATUS IS NULL OR LST_STATUS NOT IN (3,5))

END

GO


