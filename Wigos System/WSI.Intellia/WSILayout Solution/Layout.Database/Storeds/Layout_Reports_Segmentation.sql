USE [wgdb_000]
GO

/****** Object:  StoredProcedure [dbo].[Layout_Reports_Segmentation]    Script Date: 03/11/2015 07:37:18 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/****** Object:  StoredProcedure [dbo].[Layout_Reports_Segmentation]    Script Date: 11/10/2014 16:26:27 ******/


--CREATE PROCEDURE [dbo].[Layout_Reports_Segmentation]
CREATE PROCEDURE [dbo].[Layout_Reports_Segmentation]
    @pReport      AS INT      = 0
  , @pConcept     AS INT      = 0
  , @pExtra       AS BIT      = 0  
  , @pDate        AS DATETIME = NULL
  , @pDateGroups  AS INT      = 0
 AS
BEGIN

    IF @pDate IS NULL
   BEGIN
    SET @pDate = GETDATE()
   END

  DECLARE @p_last_month AS DateTime

  SET @p_last_month = dbo.TodayOpening(0);
  SET @p_last_month = DATEADD(MONTH, -1, @p_last_month)

  DECLARE @DATES_TABLE AS TABLE (TD_INDEX DATETIME)

  --SELECT * FROM REPORTS

  CREATE TABLE #REPORTS_TABLE (
      REP_INDEX   DATETIME, 
      REP_DATA    xml);

  IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[reports]') AND type in (N'U'))
  BEGIN
    SELECT 0 AS TD_INDEX, 'TABLE_NOT_DEFINED' AS TD_TYPE, 'TND' AS TD_NAME, 0 AS TD_AFORO, 0 AS TD_VISITS;
  END
  ELSE
  BEGIN
    INSERT INTO #REPORTS_TABLE 
                SELECT   REP_DATE AS TD_INDEX,
                         --REP_DATE AS TD_INDEX,  
                         CAST(REPLACE(CAST(REP_DATA AS NVARCHAR(MAX)), 
                                      SUBSTRING(CAST(REP_DATA AS NVARCHAR(MAX)), 14, 9), 
                                      'T_ITEM') AS XML) AS TD_TYPE
                 FROM REPORTS
                WHERE REP_TYPE = 0
                  AND REP_DATE > DATEADD(MONTH, -13, @p_last_month)
                  AND REP_DATE <= @p_last_month

    INSERT INTO @DATES_TABLE 
    SELECT DISTINCT REP_INDEX FROM #REPORTS_TABLE
    
    
    --SELECT * FROM @DATES_TABLE  -- #REPORTS_TABLE

    --SELECT   * 
    --  FROM   (
              SELECT   *
                INTO   ##SEGMENTATION_DATA_TABLE
                FROM   ((
                        SELECT   CAST(TD_INDEX AS DATETIME) AS TD_INDEX
                               , TD_AGROUP
                               , CASE @pConcept
                                  WHEN 0 THEN REGS.TD_AFORO
                                  WHEN 1 THEN REGS.TD_VISITS
                                 END AS TD_VALUE
                          FROM   (
                                  SELECT DISTINCT REP_INDEX AS TD_INDEX
                                       , CAST(T.C.query('TD_TABLE/node()') AS NVARCHAR) AS TD_TYPE
                                       , CAST(T.C.query('TD_AGROUP_NAME/node()') AS NVARCHAR) AS TD_NAME 
                                       , CAST(T.C.query('TD_AGROUP/node()') AS NVARCHAR)  AS TD_AGROUP
                                       , CAST(T.C.query('TD_AFORO/node()') AS NVARCHAR)       AS TD_AFORO
                                       , CAST(T.C.query('TD_VISITS/node()') AS NVARCHAR) AS TD_VISITS
                                  FROM  #REPORTS_TABLE CROSS APPLY rep_data.nodes('/NewDataSet//T_ITEM') as T(C)
                                 ) REGS 
                         WHERE   TD_TYPE <> ''
                           AND   TD_NAME <> 'TOTAL'
                           AND   (    
                                     (@pReport = 0 AND TD_TYPE = 'HOLDER_LEVEL')
                                  OR (@pReport = 1 AND TD_TYPE = 'YEARS_OLD')
                                  OR (@pReport = 2 AND TD_TYPE = 'ADT_GROUP')
                                  )
                       ) 
                    UNION
                       (
                        SELECT   CAST(TD_INDEX AS DATETIME) AS TD_INDEX
                               , NULL AS TD_AGROUP
                               , NULL AS VALUE
                          FROM   @DATES_TABLE
                       ) 
                    
                    ) AS X
                   WHERE TD_AGROUP IS NOT NULL 

EXEC wsi_table_pivot N'##SEGMENTATION_DATA_TABLE', N'TD_INDEX', N'TD_VALUE'

DROP TABLE ##SEGMENTATION_DATA_TABLE

END

DROP TABLE #REPORTS_TABLE
   
END

GO


