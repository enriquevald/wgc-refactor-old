USE [wgdb_000]
GO

/****** Object:  StoredProcedure [dbo].[Layout_GetPlayerInfo]    Script Date: 03/11/2015 06:56:39 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/****** Object:  StoredProcedure [dbo].[Layout_GetPlayerInfo]    Script Date: 11/10/2014 16:26:26 ******/
CREATE PROCEDURE [dbo].[Layout_GetPlayerInfo]
      @AccountId INT
      ,@Date DATETIME
AS
BEGIN

DECLARE @NLastVisits INT
SET @NLastVisits = 3

SELECT T.PS_PLAYED_COUNT
     , T.PS_PLAYED_AMOUNT
     ,(T.PS_PLAYED_AMOUNT/T.PS_PLAYED_COUNT) AS BET_AVG
     , T.PS_WON_AMOUNT
     , (T.PS_WON_AMOUNT-T.PS_PLAYED_AMOUNT) AS PLAYED_NET_WIN
  FROM 
  (
         SELECT SUM(PS_PLAYED_COUNT) AS PS_PLAYED_COUNT
             , SUM(PS_PLAYED_AMOUNT) AS PS_PLAYED_AMOUNT
             , SUM(PS_WON_AMOUNT) AS PS_WON_AMOUNT
	       FROM PLAY_SESSIONS_TEST
	       WHERE PS_ACCOUNT_ID = @AccountId
	       AND PS_FINISHED < @Date
	       and ps_played_count>0
	 ) T

  SELECT TOP(@NLastVisits) 
           PVH_TOTAL_PLAYED_COUNT
         , PVH_TOTAL_PLAYED
         , PVH_TOTAL_WON
         , PVH_TOTAL_BET_AVG  
      FROM H_PVH_TEST
     WHERE PVH_ACCOUNT_ID = @AccountId
  ORDER BY pvh_date DESC 
END -- Layout_GetRanges



GO


