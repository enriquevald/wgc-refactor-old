USE [wgdb_000]
GO

/****** Object:  StoredProcedure [dbo].[Layout_SaveSiteAlarm]    Script Date: 03/11/2015 07:40:14 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Layout_SaveSiteAlarm]
      @pTerminalId INT
    , @pAlarmId INT
    , @pDateCreated DATETIME
    , @pAlarmType INT
    , @pAlarmSource INT
    , @pUserCreated INT
    , @pMediaId INT
    , @pDescription NVARCHAR(MAX)
    , @pDateToTask DATETIME
    , @pTaskId INT
    , @pStatus INT
AS
BEGIN
      IF EXISTS (SELECT TOP 1 * 
                   FROM LAYOUT_SITE_ALARMS
                  WHERE	LSA_TERMINAL_ID = @pTerminalId
                    AND LSA_ALARM_ID = @pAlarmId
                    AND LSA_DATE_CREATED = @pDateCreated)
                    
            UPDATE	LAYOUT_SITE_ALARMS
               SET	LSA_DATE_TO_TASK = @pDateToTask
                 ,	LSA_TASK_ID = @pTaskId
                 ,  LSA_STATUS = @pStatus
             WHERE	LSA_TERMINAL_ID = @pTerminalId
               AND  LSA_ALARM_ID = @pAlarmId
               AND  LSA_DATE_CREATED = @pDateCreated
               
      ELSE
      
       INSERT INTO	LAYOUT_SITE_ALARMS
           (LSA_TERMINAL_ID
           ,LSA_ALARM_ID
           ,LSA_DATE_CREATED
           ,LSA_ALARM_TYPE
           ,LSA_ALARM_SOURCE
           ,LSA_USER_CREATED
           ,LSA_MEDIA_ID
           ,LSA_DESCRIPTION
           ,LSA_DATE_TO_TASK
           ,LSA_TASK_ID
           ,LSA_STATUS
           )
            VALUES 
				 (  @pTerminalId 
          , @pAlarmId
          , @pDateCreated 
          , @pAlarmType 
          , @pAlarmSource 
          , @pUserCreated 
          , @pMediaId 
          , @pDescription 
          , @pDateToTask 
          , @pTaskId
          , 0 
				 ) 
				 
END --Layout_SaveConfiguration


GO


