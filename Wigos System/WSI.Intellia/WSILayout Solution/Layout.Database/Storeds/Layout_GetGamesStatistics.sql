USE [wgdb_000]
GO
/****** Object:  StoredProcedure [dbo].[Layout_GetGamesStatistics]    Script Date: 02/11/2015 05:16:39 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[Layout_GetGamesStatistics]    Script Date: 11/10/2014 16:26:26 ******/



---
-- Layout_GetGamesStatistics
---

CREATE PROCEDURE [dbo].[Layout_GetGamesStatistics]
      @pTerminalId	INT = NULL    
AS
BEGIN

	SELECT SPH.SPH_TERMINAL_ID AS TERMINAL_ID
		 , SPH.SPH_GAME_ID AS GAME_ID
		 , ISNULL(PG.PG_GAME_NAME, 'UNKNOWN') AS GAME_NAME
		 , SUM(SPH.SPH_PLAYED_AMOUNT)AS PLAYED_AMOUNT
		 , SUM(SPH.SPH_WON_AMOUNT) AS WON_AMOUNT
		 , SUM(SPH.SPH_PLAYED_COUNT) AS PLAYED_COUNT
		 , SUM(SPH.SPH_WON_COUNT) AS WON_COUNT
				 
	  FROM SALES_PER_HOUR_V2 AS SPH
	       LEFT JOIN PROVIDERS_GAMES AS PG ON PG.PG_GAME_ID = SPH.SPH_GAME_ID
	       
	 WHERE SPH.SPH_TERMINAL_ID = ISNULL(@pTerminalId, SPH.SPH_TERMINAL_ID) 
	   AND SPH.SPH_BASE_HOUR >= DATEADD(DAY, DATEDIFF(DAY, 0, GETDATE()), 0) 
  GROUP BY SPH.SPH_GAME_ID, PG_GAME_NAME, SPH_TERMINAL_ID
  ORDER BY SPH.SPH_TERMINAL_ID
    
END -- Layout_GetGamesStatistics

