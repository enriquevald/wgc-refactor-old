USE [wgdb_000]
GO

/****** Object:  StoredProcedure [dbo].[Layout_GetPlayersInfo]    Script Date: 03/11/2015 06:56:58 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Layout_GetPlayersInfo]
AS
BEGIN
   SELECT [GTPM_MOVEMENT_ID]
     	 ,[GTPM_TYPE]
     	 ,[GTPM_DATETIME]
     	 ,[GTPM_USER_ID]
     	 ,[GTPM_CASHIER_ID]
     	 ,[GTPM_GAMING_TABLE_SESSION_ID]
     	 ,[GTPM_GAMING_TABLE_ID]
     	 ,[GTPM_SEAT_ID]
     	 ,[GTPM_PLAY_SESSION_ID]
     	 ,[GTPM_TERMINAL_ID]
     	 ,[GTPM_ACCOUNT_ID]
     	 ,[GTPM_OLD_VALUE]
     	 ,[GTPM_VALUE]
  FROM [GT_PLAYERTRACKING_MOVEMENTS]
END

GO


