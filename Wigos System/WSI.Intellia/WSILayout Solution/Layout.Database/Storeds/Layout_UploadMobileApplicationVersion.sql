USE [wgdb_000]
GO

/****** Object:  StoredProcedure [dbo].[Layout_UploadMobileApplicationVersion]    Script Date: 03/11/2015 07:44:29 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Carlos Rodrigo
-- Create date: 14102015
-- Description:	Save the newest versions of the mobile application
-- =============================================
CREATE PROCEDURE [dbo].[Layout_UploadMobileApplicationVersion]
	@pVersion text,
	@pApk varbinary(max)
AS
BEGIN
	INSERT INTO layout_mobile_application_versions
	(
		lmav_version,
		lmav_apk,
		lmav_date
	)VALUES
	(
		@pVersion,
		@pApk,
		GETDATE()
	)
END

GO


