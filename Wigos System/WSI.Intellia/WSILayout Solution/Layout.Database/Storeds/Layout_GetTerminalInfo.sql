USE [wgdb_000]
GO

/****** Object:  StoredProcedure [dbo].[Layout_GetTerminalInfo]    Script Date: 03/11/2015 06:58:54 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/****** Object:  StoredProcedure [dbo].[Layout_GetPlayerInfo]    Script Date: 11/10/2014 16:26:26 ******/
CREATE PROCEDURE [dbo].[Layout_GetTerminalInfo]
      @TerminalId BIGINT
AS
BEGIN
  DECLARE @str_columns as VARCHAR(max)
  DECLARE @str_query as NVARCHAR(max)
  DECLARE @now_hour AS INTEGER
  DECLARE @final_query as nvarchar(max)
  DECLARE @query_temp as nvarchar(max)
  DECLARE @test_date as nvarchar(max)
  
  SET @now_hour = DATEPART(HOUR,'2015-08-01')
  SET @str_columns=''
  SET @final_query='SELECT X2D_DATE,'
  set @test_date='2015-08-01'
  
  SET @str_columns = @str_columns + ' ISNULL(x2d_' + RIGHT('0' + CAST(@now_hour AS VARCHAR), 2) + '_max,0)'
    
  --NETWIN
  SET @query_temp = 'SUM(CASE WHEN X2D_METER_ID = 9  AND X2D_METER_ITEM = 0 THEN (' + @str_columns + ') end) @COLUMNAME'
  SET @query_temp = REPLACE(@query_temp,'@COLUMNAME','''NetWin''')
  SET @final_query = @final_query + @query_temp
    
  --AVG. BET
  SET @query_temp = 'SUM(CASE WHEN X2D_METER_ID = 1  AND X2D_METER_ITEM = 0 THEN (' + @str_columns + ') end)  @COLUMNAME'
  SET @query_temp = REPLACE(@query_temp,'@COLUMNAME','''AvgBet''')
  SET @final_query = @final_query + ' ,' + @query_temp
    
  --COIN IN
  SET @query_temp = 'SUM(CASE WHEN X2D_METER_ID = 12  AND X2D_METER_ITEM = 0 THEN (' + @str_columns + ') end)  @COLUMNAME'
  SET @query_temp = REPLACE(@query_temp,'@COLUMNAME','''CoinIn''')
  SET @final_query = @final_query + ' , ' + @query_temp
    
  --COIN OUT
  SET @query_temp = 'SUM(CASE WHEN X2D_METER_ID = 13  AND X2D_METER_ITEM = 0 THEN (' + @str_columns + ') end)  @COLUMNAME'
  SET @query_temp = REPLACE(@query_temp,'@COLUMNAME','''CoinOut''')
  SET @final_query = @final_query + ' , ' + @query_temp
    
  --PLAYS
  SET @query_temp = 'SUM(CASE WHEN X2D_METER_ID = 14  AND X2D_METER_ITEM = 0 THEN (' + @str_columns + ') end)  @COLUMNAME'
  SET @query_temp = REPLACE(@query_temp,'@COLUMNAME','''PLAYS''')
  SET @final_query = @final_query + ' , ' + @query_temp
    
  SET @final_query = @final_query+' FROM H_T2D_TMH WHERE X2D_WEEKDAY= DATEPART(DW,'''+@test_date+''') AND X2D_ID = ' + CAST(@TerminalId AS nvarchar) + ' GROUP BY X2D_DATE ORDER BY X2D_DATE ASC'
  
  EXEC SP_EXECUTESQL @final_query
    
  SET @now_hour = DATEPART(HOUR,GETDATE())
  SET @str_columns=''
  SET @final_query='SELECT '
  
  SET @str_columns = @str_columns + ' ISNULL(x2d_' + RIGHT('0' + CAST(@now_hour AS VARCHAR), 2) + '_avg,0)'
    
  --NETWIN
  SET @query_temp = 'SUM(CASE WHEN X2D_METER_ID = 9  AND X2D_METER_ITEM = 0 THEN (' + @str_columns + ') end) @COLUMNAME'
  SET @query_temp = REPLACE(@query_temp,'@COLUMNAME','''NetWin''')
  SET @final_query = @final_query + @query_temp
    
  --AVG. BET
  SET @query_temp = 'SUM(CASE WHEN X2D_METER_ID = 1  AND X2D_METER_ITEM = 0 THEN (' + @str_columns + ') end)  @COLUMNAME'
  SET @query_temp = REPLACE(@query_temp,'@COLUMNAME','''AvgBet''')
  SET @final_query = @final_query + ' ,' + @query_temp
    
  --COIN IN
  SET @query_temp = 'SUM(CASE WHEN X2D_METER_ID = 12  AND X2D_METER_ITEM = 0 THEN (' + @str_columns + ') end)  @COLUMNAME'
  SET @query_temp = REPLACE(@query_temp,'@COLUMNAME','''CoinIn''')
  SET @final_query = @final_query + ' , ' + @query_temp
    
  --COIN OUT
  SET @query_temp = 'SUM(CASE WHEN X2D_METER_ID = 13  AND X2D_METER_ITEM = 0 THEN (' + @str_columns + ') end)  @COLUMNAME'
  SET @query_temp = REPLACE(@query_temp,'@COLUMNAME','''CoinOut''')
  SET @final_query = @final_query + ' , ' + @query_temp
    
  --PLAYS
  SET @query_temp = 'SUM(CASE WHEN X2D_METER_ID = 14  AND X2D_METER_ITEM = 0 THEN (' + @str_columns + ') end)  @COLUMNAME'
  SET @query_temp = REPLACE(@query_temp,'@COLUMNAME','''PLAYS''')
  SET @final_query = @final_query + ' , ' + @query_temp
    
  SET @final_query = @final_query+' FROM H_Y2D_TMH WHERE X2D_WEEKDAY= DATEPART(DW,'''+@test_date+''') AND X2D_ID = ' + CAST(@TerminalId AS nvarchar)
  
  EXEC SP_EXECUTESQL @final_query
  
END -- Layout_GetRanges



GO


