USE [wgdb_000]
GO

/****** Object:  StoredProcedure [dbo].[Layout_GetNotIncludedObjects]    Script Date: 03/11/2015 06:38:34 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/****** Object:  StoredProcedure [dbo].[Layout_GetNotIncludedObjects]    Script Date: 11/10/2014 16:26:26 ******/



---
-- Layout_GetNotIncludedObjects
---

CREATE PROCEDURE [dbo].[Layout_GetNotIncludedObjects]
AS
BEGIN

	SELECT TE.TE_TERMINAL_ID
		 , TE.TE_NAME
		 , PR.PV_NAME
		 , TE.TE_BANK_ID
		 , BK.BK_AREA_ID
		 , TE_FLOOR_ID
		 , TE_TERMINAL_TYPE
	  FROM TERMINALS AS TE
		   INNER JOIN PROVIDERS AS PR ON PR.PV_ID = TE.TE_PROV_ID
		   INNER JOIN BANKS AS BK ON BK.BK_BANK_ID = TE.TE_BANK_ID
	 WHERE TE_TERMINAL_ID NOT IN (SELECT LO_EXTERNAL_ID 
	                                FROM LAYOUT_OBJECTS
	                               WHERE LO_TYPE = 1) 
	                               
	SELECT AR_AREA_ID
		 , AR_NAME
		 , AR_SMOKING
	  FROM AREAS
	 WHERE AR_AREA_ID NOT IN (SELECT LO_EXTERNAL_ID 
	                            FROM LAYOUT_OBJECTS
	                           WHERE LO_TYPE = 2) 
	 
	 SELECT BK_BANK_ID
		  , BK_NAME
	   FROM BANKS
	  WHERE BK_BANK_ID NOT IN (SELECT LO_EXTERNAL_ID 
	                             FROM LAYOUT_OBJECTS
	                            WHERE LO_TYPE = 3) 
	                            
END -- Layout_GetNotIncludedObjects

GO


