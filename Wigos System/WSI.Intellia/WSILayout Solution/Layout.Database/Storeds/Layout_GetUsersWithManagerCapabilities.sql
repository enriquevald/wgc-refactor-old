USE [wgdb_000]
GO

/****** Object:  StoredProcedure [dbo].[Layout_GetUsersWithManagerCapabilities]    Script Date: 03/11/2015 07:00:57 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		CARLOS RODRIGO
-- Create date: 23102015
-- Description:	Stored Procedure dedicated to obtain all users with Manager capabilities.
-- =============================================
CREATE PROCEDURE [dbo].[Layout_GetUsersWithManagerCapabilities]
AS
BEGIN
	SELECT lc.LC_USER_ID as ID, u.GU_USERNAME as USERNAME
	FROM LAYOUT_USERS_CONFIGURATION lc
	INNER JOIN GUI_USERS u on lc.LC_USER_ID = u.GU_USER_ID
	WHERE LC_IS_MANAGER = 1
END

GO


