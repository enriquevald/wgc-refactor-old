USE [wgdb_000]
GO
/****** Object:  StoredProcedure [dbo].[Charts_GetObjectInfo]    Script Date: 03/11/2015 08:44:03 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[Charts_GetObjectInfo]    Script Date: 11/10/2014 16:26:26 ******/


CREATE PROCEDURE [dbo].[Charts_GetObjectInfo]
      @pExternalObjectId INT = NULL
AS
BEGIN

--DECLARE @pExternalObjectId AS INT 
--SET @pExternalObjectId = NULL

SELECT   te_terminal_id        AS LO_ID
       , te_terminal_id        AS LO_EXTERNAL_ID
       , 1                     AS LO_TYPE
       , NULL                  AS LO_PARENT_ID 
       , 1                     AS LO_MESH_ID
       , 1                     AS LME_TYPE
       , 1                     AS LME_SUB_TYPE		  
       , te_activation_date    AS LO_CREATION_DATE
       , te_timestamp          AS LO_UPDATE_DATE
       , 0                     AS LOP_X
       , 0                     AS LOP_Y
       , 0                     AS LOP_Z
       , 0                     AS LOP_ORIENTATION	
       , 1                     AS LOP_FLOOR_ID
       , 0                     AS LOP_AREA_ID
       , 0                     AS LOP_BANK_ID
       , te_timestamp          AS LOP_POSITION_DATE
  FROM   TERMINALS
 WHERE   te_active          = 1 
   AND   te_type            = 1
   AND   te_retirement_date IS NULL
   AND   te_terminal_id     = ISNULL(@pExternalObjectId, te_terminal_id)

END

