USE [wgdb_000]
GO
/****** Object:  StoredProcedure [dbo].[Layout_AssignDeviceToUser]    Script Date: 02/11/2015 04:14:47 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Layout_AssignDeviceToUser]
      @pUserId INT
    , @pDeviceId Nvarchar(50)
AS
BEGIN
	IF EXISTS (SELECT TOP 1 *
				FROM layout_devices ld
				WHERE ld.ld_device_id = @pDeviceId)
			
		UPDATE	layout_devices
				SET  ld_user_id= @pUserId,
					 ld_last_update = GETDATE()
				WHERE ld_device_id = @pDeviceId
	ELSE IF EXISTS (SELECT TOP 1 *
				FROM layout_devices ld
				WHERE ld.ld_user_id = @pUserId)
			
		UPDATE	layout_devices
				SET  ld_device_id= @pDeviceId,
					 ld_last_update = GETDATE()
				WHERE ld_user_id = @pUserId
	ELSE
		INSERT INTO layout_devices	
                    (ld_device_id,
                    ld_user_id,
                    ld_last_update)
                    VALUES
                    (@pDeviceId,
                    @pUserId,
                    GETDATE())
END --[Layout_AssignedDeviceToUser]
                    
