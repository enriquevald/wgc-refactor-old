USE [wgdb_000]
GO
/****** Object:  StoredProcedure [dbo].[Layout_GetBeaconsAvailables]    Script Date: 02/11/2015 04:36:29 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Layout_GetBeaconsAvailables]
	@pBeaconEnumId INT
AS
BEGIN
SELECT   LO_ID            AS OBJ_ID
       , LD_TYPE          AS OBJ_TYPE
       , LD_ID            AS OBJ_IDENTITY
       , LD_DEVICE_ID     AS OBJ_NAME
       , LOC_X            AS X
       , LOC_Y            AS Y
       , LOC_FLOOR_NUMBER AS FLOOR_ID
  FROM   LAYOUT_DEVICES 
  INNER   JOIN LAYOUT_OBJECTS ON LD_ID = LO_EXTERNAL_ID AND LD_TYPE = 70
  INNER   JOIN LAYOUT_OBJECT_LOCATIONS ON LOL_OBJECT_ID = LO_ID AND LOL_CURRENT_LOCATION = 1
  INNER   JOIN LAYOUT_LOCATIONS ON LOL_LOCATION_ID = LOC_ID
  WHERE   LD_TYPE = @pBeaconEnumId
  
	
END
