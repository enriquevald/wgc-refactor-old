USE [wgdb_000]
GO

/****** Object:  StoredProcedure [dbo].[Layout_GetTerminalStatusData]    Script Date: 03/11/2015 06:59:48 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Layout_GetTerminalStatusData]
AS
BEGIN
 SELECT TS_TERMINAL_ID
      , TS_EGM_FLAGS
      , TS_DOOR_FLAGS
      , TS_BILL_FLAGS
      , TS_PRINTER_FLAGS
      , TS_PLAYED_WON_FLAGS
      , TS_JACKPOT_FLAGS
      , CASE WHEN TS_CALL_ATTENDANT_FLAGS = 1 THEN 1 ELSE 0 END AS TS_CALL_ATTENDANT_FLAGS
      , TS_STACKER_STATUS
      , TS_MACHINE_FLAGS
   FROM TERMINAL_STATUS
END


GO


