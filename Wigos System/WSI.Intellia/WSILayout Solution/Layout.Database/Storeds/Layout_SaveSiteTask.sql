USE [wgdb_000]
GO

/****** Object:  StoredProcedure [dbo].[Layout_SaveSiteTask]    Script Date: 03/11/2015 07:40:52 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Layout_SaveSiteTask]
      @pTaskId INT
    , @pStatus INT
    , @pStart DATETIME
    , @pEnd DATETIME
    , @pCategory INT
    , @pSubcategory INT
    , @pTerminalId INT
    , @pAccountId INT
    , @pDescription NVARCHAR(MAX)
    , @pSeverity INT
    , @pCreationUserId INT
    , @pCreation DATETIME
    , @pAssignedRoleId INT
    , @pAssignedUserId INT
    , @pAssigned DATETIME
    , @pAcceptedUserId INT
    , @pAccepted DATETIME
    , @pScaleFromUserId INT
    , @pScaleToUserId INT
    , @pScaleReason NVARCHAR(250)
    , @pScale DATETIME
    , @pSolvedUserId INT
    , @pSolved DATETIME
    , @pValidateUserId INT
    , @pValidate DATETIME
    , @pLastStatusUpdateUserId INT
    , @pLastStatusUpdate DATETIME
    , @pAttachedMedia INT
    , @pHistory NVARCHAR(MAX)
    , @pOutputFilterId INT OUTPUT
AS
BEGIN
      IF EXISTS (SELECT TOP 1 * 
                   FROM LAYOUT_SITE_TASKS
                  WHERE	LST_ID = @pTaskId)
                    
            UPDATE	LAYOUT_SITE_TASKS
               SET	LST_STATUS = @pStatus
                 ,  LST_START = @pStart
                 ,  LST_END = @pEnd
                 ,  LST_CATEGORY = @pCategory 
                 ,  LST_SUBCATEGORY = @pSubcategory 
                 ,  LST_TERMINAL_ID = @pTerminalId
                 ,  LST_ACCOUNT_ID = @pAccountId 
                 ,  LST_DESCRIPTION = @pDescription 
                 ,  LST_SEVERITY = @pSeverity 
                 ,  LST_CREATION_USER_ID = @pCreationUserId 
                 ,  LST_CREATION = @pCreation 
                 ,  LST_ASSIGNED_ROLE_ID = @pAssignedRoleId 
                 ,  LST_ASSIGNED_USER_ID = @pAssignedUserId 
                 ,  LST_ASSIGNED = @pAssigned 
                 ,  LST_ACCEPTED_USER_ID = @pAcceptedUserId 
                 ,  LST_ACCEPTED = @pAccepted 
                 ,  LST_SCALE_FROM_USER_ID = @pScaleFromUserId 
                 ,  LST_SCALE_TO_USER_ID = @pScaleToUserId 
                 ,  LST_SCALE_REASON = @pScaleReason 
                 ,  LST_SCALE = @pScale 
                 ,  LST_SOLVED_USER_ID = @pSolvedUserId 
                 ,  LST_SOLVED = @pSolved 
                 ,  LST_VALIDATE_USER_ID = @pValidateUserId 
                 ,  LST_VALIDATE = @pValidate 
                 ,  LST_LAST_STATUS_UPDATE_USER_ID = @pLastStatusUpdateUserId 
                 ,  LST_LAST_STATUS_UPDATE = @pLastStatusUpdate 
                 ,  LST_ATTACHED_MEDIA = @pAttachedMedia
                 ,  LST_EVENTS_HISTORY = @pHistory
             WHERE	LST_ID = @pTaskId
      ELSE
       INSERT INTO	LAYOUT_SITE_TASKS
           (       LST_STATUS
                 , LST_START
                 , LST_END
                 , LST_CATEGORY
                 , LST_SUBCATEGORY
                 , LST_TERMINAL_ID
                 , LST_ACCOUNT_ID
                 , LST_DESCRIPTION
                 , LST_SEVERITY
                 , LST_CREATION_USER_ID
                 , LST_CREATION
                 , LST_ASSIGNED_ROLE_ID
                 , LST_ASSIGNED_USER_ID
                 , LST_ASSIGNED
                 , LST_ACCEPTED_USER_ID
                 , LST_ACCEPTED
                 , LST_SCALE_FROM_USER_ID
                 , LST_SCALE_TO_USER_ID
                 , LST_SCALE_REASON
                 , LST_SCALE
                 , LST_SOLVED_USER_ID
                 , LST_SOLVED
                 , LST_VALIDATE_USER_ID
                 , LST_VALIDATE
                 , LST_LAST_STATUS_UPDATE_USER_ID
                 , LST_LAST_STATUS_UPDATE
                 , LST_ATTACHED_MEDIA
                 , LST_EVENTS_HISTORY)
            VALUES 
				          ( @pStatus 
                  , @pStart 
                  , @pEnd 
                  , @pCategory 
                  , @pSubcategory 
                  , @pTerminalId 
                  , @pAccountId 
                  , @pDescription 
                  , @pSeverity 
                  , @pCreationUserId 
                  , @pCreation 
                  , @pAssignedRoleId 
                  , @pAssignedUserId 
                  , @pAssigned 
                  , @pAcceptedUserId 
                  , @pAccepted 
                  , @pScaleFromUserId 
                  , @pScaleToUserId 
                  , @pScaleReason 
                  , @pScale 
                  , @pSolvedUserId 
                  , @pSolved 
                  , @pValidateUserId 
                  , @pValidate 
                  , @pLastStatusUpdateUserId 
                  , @pLastStatusUpdate 
                  , @pAttachedMedia
                  , @pHistory
                  )
SELECT @pOutputFilterId=SCOPE_IDENTITY();
                  
END --LAYOUT_SITE_TASKS


GO


