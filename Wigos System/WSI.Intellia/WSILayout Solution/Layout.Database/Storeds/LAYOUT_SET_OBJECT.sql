USE [wgdb_000]
GO

/****** Object:  StoredProcedure [dbo].[LAYOUT_SET_OBJECT]    Script Date: 03/11/2015 07:43:11 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



---- CREATE / UPDATE LAYOUT OBJECT
--DECLARE @pId AS INT
--DECLARE @pExternalId AS INT
--DECLARE @pType AS INT
--DECLARE @pParentId AS DATETIME
--DECLARE @pMeshId AS INT
--DECLARE @pCreation AS INT
--DECLARE @pUpdated AS VARCHAR(50)
--DECLARE @pStatus AS INT

CREATE PROCEDURE [dbo].[LAYOUT_SET_OBJECT]
( @pId INT
, @pExternalId INT
, @pType INT
, @pParentId INT
, @pMeshId INT
, @pCreation DATETIME
, @pUpdated DATETIME
, @pStatus INT
) AS
BEGIN
  IF (@pUpdated IS NULL)
    BEGIN
      SET @pUpdated = GETDATE()
    END

  IF (@pId IS NULL)
    BEGIN
      -- INSERT
      
      IF (@pCreation IS NULL)
        BEGIN
          SET @pCreation = GETDATE()
        END
     
      INSERT INTO   layout_objects
                  ( lo_external_id, lo_type, lo_parent_id, lo_mesh_id, lo_creation_date, lo_update_date, lo_status )
           VALUES ( @pExternalId, @pType, @pParentId, @pMeshId, @pCreation, @pUpdated, @pStatus )
             
      SET @pId = SCOPE_IDENTITY()           
             
    END
  ELSE
    BEGIN
     -- UPDATE ALL EXCEPT ID AND CREATION DATE
           UPDATE   layout_objects
              SET   lo_external_id    = @pExternalId
                  , lo_type           = @pType
                  , lo_parent_id      = @pParentId
                  , lo_mesh_id        = @pMeshId
                  , lo_update_date    = @pUpdated
                  , lo_status         = @pStatus
                  
    END
    
  SELECT @pId
END
  
GO


