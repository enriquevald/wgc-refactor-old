USE [wgdb_000]
GO

/****** Object:  StoredProcedure [dbo].[Layout_TerminalAlarms_Test]    Script Date: 03/11/2015 07:43:53 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/****** Object:  StoredProcedure [dbo].[Layout_TerminalAlarms_Test]    Script Date: 11/10/2014 16:26:27 ******/

---
-- [Layout_TerminalAlarms_Test]
---

CREATE PROCEDURE [dbo].[Layout_TerminalAlarms_Test]
AS
BEGIN

	DECLARE @TerminalId AS INT
	DECLARE @counter AS INT

	UPDATE terminal_status SET ts_egm_flags = 0

	DECLARE Curs CURSOR FOR

		SELECT TOP 8 LO.lo_external_id
		  FROM layout_objects AS LO
	INNER JOIN play_sessions AS PS ON PS.ps_terminal_id = LO.lo_external_id AND PS.ps_status = 0
	  ORDER BY PS.ps_account_id
	  
	OPEN Curs

	SET @counter = 1

	FETCH NEXT FROM Curs INTO @TerminalId

	 WHILE @@FETCH_STATUS = 0
	 BEGIN
		
		-- TS_PRINTER_FLAGS
		IF @counter = 1
		BEGIN
			IF EXISTS (SELECT TS_TERMINAL_ID FROM TERMINAL_STATUS WHERE TS_TERMINAL_ID = @TerminalId)	
				UPDATE terminal_status SET ts_printer_flags = 4 WHERE ts_terminal_id = @TerminalId
			ELSE
				INSERT INTO terminal_status (ts_terminal_id, ts_printer_flags) VALUES (@TerminalId, 4)  
		END
		-- TS_EGM_FLAGS
		IF @counter = 2
		BEGIN
			IF EXISTS (SELECT TS_TERMINAL_ID FROM TERMINAL_STATUS WHERE TS_TERMINAL_ID = @TerminalId)	
				UPDATE terminal_status SET ts_egm_flags = 8 WHERE ts_terminal_id = @TerminalId
			ELSE
				INSERT INTO terminal_status (ts_terminal_id, ts_egm_flags) VALUES (@TerminalId, 8)  
		END
		-- TS_DOOR_FLAGS
		IF @counter = 3
		BEGIN
			IF EXISTS (SELECT TS_TERMINAL_ID FROM TERMINAL_STATUS WHERE TS_TERMINAL_ID = @TerminalId)	
				UPDATE terminal_status SET ts_door_flags = 2 WHERE ts_terminal_id = @TerminalId
			ELSE
				INSERT INTO terminal_status (ts_terminal_id, ts_door_flags) VALUES (@TerminalId, 2)  
		END
		-- TS_BILL_FLAGS
		IF @counter = 4
		BEGIN
			IF EXISTS (SELECT TS_TERMINAL_ID FROM TERMINAL_STATUS WHERE TS_TERMINAL_ID = @TerminalId)	
				UPDATE terminal_status SET ts_bill_flags = 4 WHERE ts_terminal_id = @TerminalId
			ELSE
				INSERT INTO terminal_status (ts_terminal_id, ts_bill_flags) VALUES (@TerminalId, 4)  
		END
		-- TS_BILL_FLAGS
		IF @counter = 5
		BEGIN
			IF EXISTS (SELECT TS_TERMINAL_ID FROM TERMINAL_STATUS WHERE TS_TERMINAL_ID = @TerminalId)	
				UPDATE terminal_status SET ts_bill_flags = 1 WHERE ts_terminal_id = @TerminalId
			ELSE
				INSERT INTO terminal_status (ts_terminal_id, ts_bill_flags) VALUES (@TerminalId, 1)  
		END
		-- TS_PLAYED_WON_FLAGS
		IF @counter = 6
		BEGIN
			IF EXISTS (SELECT TS_TERMINAL_ID FROM TERMINAL_STATUS WHERE TS_TERMINAL_ID = @TerminalId)	
				UPDATE terminal_status SET ts_played_won_flags = 1 WHERE ts_terminal_id = @TerminalId
			ELSE
				INSERT INTO terminal_status (ts_terminal_id, ts_played_won_flags) VALUES (@TerminalId, 1)  
		END
		-- TS_CALL_ATTENDANT_FLAGS
		IF @counter = 7
		BEGIN
			IF EXISTS (SELECT TS_TERMINAL_ID FROM TERMINAL_STATUS WHERE TS_TERMINAL_ID = @TerminalId)	
				UPDATE terminal_status SET ts_call_attendant_flags = 1 WHERE ts_terminal_id = @TerminalId
			ELSE
				INSERT INTO terminal_status (ts_terminal_id, ts_call_attendant_flags) VALUES (@TerminalId, 1)  
		END
		-- TS_JACKPOT_FLAGS
		IF @counter = 8
		BEGIN
			IF EXISTS (SELECT TS_TERMINAL_ID FROM TERMINAL_STATUS WHERE TS_TERMINAL_ID = @TerminalId)	
				UPDATE terminal_status SET ts_jackpot_flags = 2 WHERE ts_terminal_id = @TerminalId
			ELSE
				INSERT INTO terminal_status (ts_terminal_id, ts_jackpot_flags) VALUES (@TerminalId, 2)  
		END
		 
		SET @counter = @counter + 1
		
		FETCH NEXT FROM Curs INTO @TerminalId
		
	 END

	CLOSE Curs
	DEALLOCATE Curs

  
END -- [Layout_TerminalAlarms_Test]


GO


