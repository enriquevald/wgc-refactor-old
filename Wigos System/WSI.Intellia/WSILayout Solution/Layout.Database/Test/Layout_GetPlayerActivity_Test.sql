USE [wgdb_000]
GO

/****** Object:  StoredProcedure [dbo].[Layout_GetPlayerActivity_Test]    Script Date: 03/11/2015 08:39:29 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/****** Object:  StoredProcedure [dbo].[Layout_GetPlayerActivity_Test]    Script Date: 11/10/2014 16:26:26 ******/



---
-- Layout_GetPlayerActivity_Test
---
CREATE PROCEDURE [dbo].[Layout_GetPlayerActivity_Test]
	@pAccountId VARCHAR(MAX)
  , @pOnlyActivePlaySession BIT 
AS
BEGIN
	DECLARE @Today as DATETIME
	
	
	SET @Today = '2014/07/07'

   CREATE TABLE #players_table (SEQ_ID INT IDENTITY(1,1) PRIMARY KEY, ACCOUNT_ID BIGINT)
	INSERT INTO #players_table 
		 SELECT AC_ACCOUNT_ID
		   FROM ACCOUNTS
		  WHERE AC_ACCOUNT_ID IN (SELECT CAST(SST_VALUE AS BIGINT) FROM [wgdb_905].[dbo].[SplitStringIntotable](@pAccountId, ',', 1))
   
   CREATE TABLE #terminals_table (SEQ_ID INT IDENTITY(1,1) PRIMARY KEY
                                , TERMINAL_ID INT
                                , TERMINAL_NAME VARCHAR(50)
                                , PROVIDER_NAME VARCHAR(50))
	INSERT INTO #terminals_table  
		 SELECT TE.te_terminal_id
			  , TE.te_name
			  , PR.pv_name
		   FROM TERMINALS AS TE
			    INNER JOIN providers AS PR ON PR.pv_id = TE.te_prov_id 
		  WHERE TE.te_terminal_id IN (10052,10053,10054,10055,10056,10057,10058,10059,10060,10061,10062,10063,10064,10065,10066,10067,10068,10069,10070,10071,10072,10073,10074,10075,10076,10077
								  ,10078,10079,10080,10081,10082,10083,10084,10085,10086,10087,10088,10089,10090,10091,10092,10093,10094,10095,10096,10097,10098,10099,10100,10101,10102,10103
								  ,10104,10105,10106,10107,10108,10109,10110,10111,10112,10113,10114,10115,10116,10117,10118,10119,10120,10121,10122,10123,10124,10125,10126,10127,10128,10129
								  ,10130,10131,10132,10133,10134,10135,10136,10137,10138,10139,10140,10141,10142,10143,10144,10145,10146,10147,10148,10149,10150,10151,10152,10153,10154,10155
								  ,10156,10157,10158,10159,10160,10161,10162,10163,10164,10165,10166,10167,10168,10169,10170,10171,10172,10173,10174,10175,10176,10177,10178,10179,10180,10181
								  ,10182,10183,10184,10185,10186,10187,10188,10189,10190,10191,10192,10193,10194,10195,10196,10197,10198,10199,10200,10201,10202,10203,10204,10205,10206,10207
								  ,10208,10209,10210,10211,10212,10213,10214,10215,10216,10217,10218,10219,10220,10221,10222,10223,10224,10225,10226,10227,10228,10229,10230,10231,10232,10233
								  ,10234,10235,10236,10237,10238,10239,10240,10241,10242,10243,10244,10245,10246,10247,10248,10249,10250,10251,10252,10253,10254,10255,10256,10257,10258,10259
								  ,10260,10261,10262,10263,10264,10265,10266,10267,10268,10269,10270,10271,10272,10273,10274,10275,10276,10277,10278,10279,10280,10281,10282,10283,10284,10285
								  ,10286,10287,10288,10289,10290,10291,10292,10293,10294)
								  
		CREATE TABLE #temp_table (ACCOUNT_ID INT
								, TERMINAL_ID INT
								, TERMINAL_NAME VARCHAR(50)
								, PROVIDER_NAME VARCHAR(50)
								, TIME_PLAYED INT
							  , TOTAL_PLAYED MONEY
								, TOTAL_WON MONEY 
								, PLAYED_COUNT INT
								, BET_AVERAGE MONEY
								, TRACK_DATA VARCHAR(50))
					
	DECLARE @TerminalId AS INT
	DECLARE @TerminalIndex AS INT
	DECLARE @AccountId AS BIGINT
	DECLARE @TerminalName AS VARCHAR(50)
	DECLARE @ProviderName AS VARCHAR(50)
	DECLARE @TimePlayed AS INT
	DECLARE @TotalPlayed AS MONEY
	DECLARE @TotalWon AS MONEY
	DECLARE @PlayedCount AS INT
	DECLARE @BetAverage AS MONEY
	DECLARE @TrackData AS VARCHAR(50)
	 		
							
	DECLARE Curs CURSOR FOR SELECT ACCOUNT_ID FROM #players_table AS PT
	OPEN Curs

	FETCH NEXT FROM Curs INTO @AccountId
	WHILE @@FETCH_STATUS = 0
	BEGIN
	
		--ROUND((@upper - @lower) * RAND() + @lower, 0)
		SELECT @TerminalIndex = ROUND((243 - 1) * RAND() + 1, 0)
		SELECT @BetAverage = ROUND((2 - 0) * RAND() + 0, 2)
		SELECT @TimePlayed = ROUND((1800 - 1) * RAND() + 1, 0)
		SELECT @TotalPlayed = ROUND((5000 - 0) * RAND() + 0, 2)
		SELECT @TotalWon = ROUND(@TotalPlayed * 0.95, 2)
		SELECT @PlayedCount = ROUND((60 - 1) * RAND() + 1, 0)
	
		SELECT @TrackData = ac_track_data
		  FROM ACCOUNTS
		 WHERE AC_ACCOUNT_ID = @AccountId
		                         
		SELECT @TerminalId = TERMINAL_ID
			 , @TerminalName = TERMINAL_NAME
			 , @ProviderName = PROVIDER_NAME
		  FROM #terminals_table
		 WHERE TERMINAL_ID = (SELECT TERMINAL_ID
							    FROM #terminals_table
							   WHERE SEQ_ID = @TerminalIndex)
		                         
		INSERT INTO #temp_table SELECT @AccountId AS ACCOUNT_ID
									 , @TerminalId AS TERMINAL_ID
									 , @TerminalName AS TERMINAL_NAME
									 , @ProviderName AS PROVIDER_NAME
									 , @TimePlayed AS TIME_PLAYED
									 , @TotalPlayed AS TOTAL_PLAYED
									 , @TotalWon AS TOTAL_WON 
									 , @PlayedCount AS PLAYED_COUNT
									 , @BetAverage AS BET_AVERAGE
									 , @TrackData AS TRACK_DATA
									 
		FETCH NEXT FROM Curs INTO @AccountId
  
	END

	CLOSE      Curs
	DEALLOCATE Curs
	
	
	SELECT * FROM #temp_table
	
	IF @pOnlyActivePlaySession = 0
	BEGIN

		SELECT AC.AC_ACCOUNT_ID
			 , TM.TM_TERMINAL_ID
			 , TM.TM_ACCEPTED
			 , TM.TM_AMOUNT
			 , TM.TM_STACKER_ID   
		  FROM ACCOUNTS AS AC
		       LEFT JOIN TERMINAL_MONEY AS TM ON DBO.TRACKDATATOEXTERNAL(AC.AC_TRACK_DATA) = TM.TM_TRACKDATA
											   AND TM.TM_ACCEPTED >= DATEADD(DAY, DATEDIFF(DAY, 0, @Today), 0)
		 WHERE AC.AC_ACCOUNT_ID IN (SELECT CAST(SST_VALUE AS BIGINT) FROM SplitStringIntoTable(@pAccountId, ',', 1))
	  ORDER BY TM.TM_ACCEPTED DESC, AC.AC_ACCOUNT_ID, TM.TM_TERMINAL_ID

	END
  
	ELSE
	BEGIN
  
		SELECT AC.AC_ACCOUNT_ID
			 , TM.TM_TERMINAL_ID
			 , TM.TM_ACCEPTED
			 , TM.TM_AMOUNT
			 , TM.TM_STACKER_ID   
		  FROM ACCOUNTS AS AC
		       INNER JOIN TERMINAL_MONEY AS TM ON DBO.TRACKDATATOEXTERNAL(AC.AC_TRACK_DATA) = TM.TM_TRACKDATA
		 WHERE AC.AC_ACCOUNT_ID IN (SELECT CAST(SST_VALUE AS BIGINT) FROM SplitStringIntoTable(@pAccountId, ',', 1))
		   AND TM.TM_ACCEPTED >= DATEADD(DAY, DATEDIFF(DAY, 0, @Today), 0)
		   AND TM.TM_PLAY_SESSION_ID IN (SELECT MAX(PS_PLAY_SESSION_ID)
										   FROM PLAY_SESSIONS
									   GROUP BY PS_ACCOUNT_ID)
	  ORDER BY TM.TM_ACCEPTED DESC, AC.AC_ACCOUNT_ID, TM.TM_TERMINAL_ID
 
	END
	
	
	DROP TABLE #players_table
	DROP TABLE #terminals_table
	DROP TABLE #temp_table
	
END -- Layout_GetPlayerActivity_Test

GO


