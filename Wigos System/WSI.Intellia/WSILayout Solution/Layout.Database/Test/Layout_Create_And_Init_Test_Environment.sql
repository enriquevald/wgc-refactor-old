SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[layout_activity_test](
	[lat_terminal_id] [bigint] NOT NULL,
	[lat_account_track_data] [bigint] NOT NULL,
	[lat_status] [int] NOT NULL,
	[lat_next_update] [datetime] NOT NULL,
	[lat_is_vip] [bit] NULL,
	[lat_alarm] [int] NULL,
	[lat_term_played_amount] [money] NULL,
	[lat_term_won_amount] [money] NULL,
	[lat_term_net_win] [money] NULL,
	[lat_term_played_count] [int] NULL,
	[lat_term_bet_av] [money] NULL,
	[lat_ps_duration] [int] NULL,
	[lat_ps_played_amount] [money] NULL,
	[lat_ps_won_amount] [money] NULL,
	[lat_ps_netwin] [money] NULL,
	[lat_ps_played_count] [int] NULL,
	[lat_ps_bet_average] [money] NULL,
	[lat_account_level] [int] NULL
) ON [PRIMARY]

CREATE TABLE [dbo].[layout_players_test](
	[SEQ_ID] [int] IDENTITY(1,1) NOT NULL,
	[ACCOUNT_ID] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[SEQ_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

INSERT INTO layout_activity_test
SELECT lo_external_id
	 , 0
	 , 1
	 , GETDATE()
	 , 0
	 , 50
	 , 0
	 , 0
	 , 0
	 , 0
	 , 0
	 , 0
	 , 0
	 , 0
	 , 0
	 , 0
	 , 0
	 , 0
  FROM layout_objects
  

INSERT INTO layout_players_test 
		 SELECT AC_ACCOUNT_ID
		   FROM ACCOUNTS
		  WHERE AC_TRACK_DATA IN (dbo.TrackDataToInternal('09706579252798047444') , 
                                  dbo.TrackDataToInternal('09858105498228244560') ,
                                  dbo.TrackDataToInternal('09723241854783639004') ,
                                  dbo.TrackDataToInternal('09800478566290999640') ,
                                  dbo.TrackDataToInternal('09665338945426732772') ,
                                  dbo.TrackDataToInternal('09832763029483991648') ,
                                  dbo.TrackDataToInternal('09661719938679259116') ,
                                  dbo.TrackDataToInternal('09777075636058012520') ,
                                  dbo.TrackDataToInternal('09678101134407612660') ,
                                  dbo.TrackDataToInternal('09791502930904327280') ,
                                  dbo.TrackDataToInternal('09620478531797262844') ,
                                  dbo.TrackDataToInternal('09787609114968337784') ,
                                  dbo.TrackDataToInternal('09903240789765662980') ,
                                  dbo.TrackDataToInternal('09732216390658725504') ,
                                  dbo.TrackDataToInternal('09845338911201989132') ,
                                  dbo.TrackDataToInternal('09746363310039834504') ,
                                  dbo.TrackDataToInternal('09841733167380634388') ,
                                  dbo.TrackDataToInternal('09706579252798134416') ,
                                  dbo.TrackDataToInternal('09858105498228327452') ,
                                  dbo.TrackDataToInternal('09723241854783586712') ,
                                  dbo.TrackDataToInternal('09800478566290951460') ,
                                  dbo.TrackDataToInternal('09665338945426721440') ,
                                  dbo.TrackDataToInternal('09832763029484009004') ,
                                  dbo.TrackDataToInternal('09661719938679272360') ,
                                  dbo.TrackDataToInternal('09777075636058029876') ,
                                  dbo.TrackDataToInternal('09678101134407630000') ,
                                  dbo.TrackDataToInternal('09791502930904348732') ,
                                  dbo.TrackDataToInternal('09620478531797280184') ,
                                  dbo.TrackDataToInternal('09787609114968351044') ,
                                  dbo.TrackDataToInternal('09652745471523745472') ,
                                  dbo.TrackDataToInternal('09732216390658746956') ,
                                  dbo.TrackDataToInternal('09597071203517814728') ,
                                  dbo.TrackDataToInternal('09746363310039847764') ,
                                  dbo.TrackDataToInternal('09611507363376910544') ,
                                  dbo.TrackDataToInternal('09706579252798151772') ,
                                  dbo.TrackDataToInternal('09607604751416056280') ,
                                  dbo.TrackDataToInternal('09723241854783604068') ,
                                  dbo.TrackDataToInternal('09549982148805837536') ,
                                  dbo.TrackDataToInternal('09665338945426738796') ,
                                  dbo.TrackDataToInternal('09566363344534064104') ,
                                  dbo.TrackDataToInternal('09912211958386407284') ,
                                  dbo.TrackDataToInternal('09777075636058109168') ,
                                  dbo.TrackDataToInternal('09926367742580142204') ,
                                  dbo.TrackDataToInternal('09791502930904301048') ,
                                  dbo.TrackDataToInternal('09886596810758371716') ,
                                  dbo.TrackDataToInternal('10038105532521719040') ,
                                  dbo.TrackDataToInternal('09903240789765759628') ,
                                  dbo.TrackDataToInternal('09980315804075929096') ,
                                  dbo.TrackDataToInternal('09845338911201962900') ,
                                  dbo.TrackDataToInternal('10012767461755721488') ,
                                  dbo.TrackDataToInternal('09661719938679357692') ,
                                  dbo.TrackDataToInternal('09777075636058115192') ,
                                  dbo.TrackDataToInternal('09926367742580148228') ,
                                  dbo.TrackDataToInternal('09791502930903348608') ,
                                  dbo.TrackDataToInternal('09886596810758467852') ,
                                  dbo.TrackDataToInternal('09787609114968407688') ,
                                  dbo.TrackDataToInternal('09903240789765732884') ,
                                  dbo.TrackDataToInternal('09732216390658795408') ,
                                  dbo.TrackDataToInternal('09845338911202059036') ,
                                  dbo.TrackDataToInternal('09746363310039900312') ,
                                  dbo.TrackDataToInternal('09841733167379655716') ,
                                  dbo.TrackDataToInternal('09706579252798204320') ,
                                  dbo.TrackDataToInternal('09858105498228397356') ,
                                  dbo.TrackDataToInternal('09723241854783656616') ,
                                  dbo.TrackDataToInternal('09800478566291021364') ,
                                  dbo.TrackDataToInternal('09665338945426791344') ,
                                  dbo.TrackDataToInternal('09832763029484046140') ,
                                  dbo.TrackDataToInternal('09661719938679309496') ,
                                  dbo.TrackDataToInternal('09777075636058062916') ,
                                  dbo.TrackDataToInternal('09678101134407667136') ,
                                  dbo.TrackDataToInternal('09791502930903337292') ,
                                  dbo.TrackDataToInternal('09620478531797350088') ,
                                  dbo.TrackDataToInternal('09787609114968420948') ,
                                  dbo.TrackDataToInternal('09652745471523815376') ,
                                  dbo.TrackDataToInternal('09732216390658816860') ,
                                  dbo.TrackDataToInternal('09597071203516840152') ,
                                  dbo.TrackDataToInternal('09746363310039917668') ,
                                  dbo.TrackDataToInternal('09611507363376980448') ,
                                  dbo.TrackDataToInternal('09706579252798221676') ,
                                  dbo.TrackDataToInternal('09607604751416126184') ,
                                  dbo.TrackDataToInternal('09723241854783673972') ,
                                  dbo.TrackDataToInternal('09549982148805907440') ,
                                  dbo.TrackDataToInternal('09665338945426808700') ,
                                  dbo.TrackDataToInternal('09566363344534134008') ,
                                  dbo.TrackDataToInternal('09661719938679330948') ,
                                  dbo.TrackDataToInternal('09777075636058080256') ,
                                  dbo.TrackDataToInternal('09678101134407680396') ,
                                  dbo.TrackDataToInternal('09791502930903354632') ,
                                  dbo.TrackDataToInternal('09620478531797338772') ,
                                  dbo.TrackDataToInternal('10038105532520740368') ,
                                  dbo.TrackDataToInternal('09903240789765829532') ,
                                  dbo.TrackDataToInternal('09980315804075999000') ,
                                  dbo.TrackDataToInternal('09845338911202032804') ,
                                  dbo.TrackDataToInternal('10012767461755795488') ,
                                  dbo.TrackDataToInternal('09841733167379752364') ,
                                  dbo.TrackDataToInternal('09957072303029068072') ,
                                  dbo.TrackDataToInternal('09858105498228371124') ,
                                  dbo.TrackDataToInternal('09971485372943575600') ,
                                  dbo.TrackDataToInternal('09800478566291118012') ,
                                  dbo.TrackDataToInternal('09967626689907299128') ,
                                  dbo.TrackDataToInternal('09732093056406585220') ,
                                  dbo.TrackDataToInternal('09727163369305163412') ,
                                  dbo.TrackDataToInternal('09723118589250928296') ,
                                  dbo.TrackDataToInternal('09723014333050440580') ,
                                  dbo.TrackDataToInternal('09702478655049840492') ,
                                  dbo.TrackDataToInternal('09698877257735423104') ,
                                  dbo.TrackDataToInternal('09694748012690000240') ,
                                  dbo.TrackDataToInternal('09694748012689958244') ,
                                  dbo.TrackDataToInternal('09689787341790823348') ,
                                  dbo.TrackDataToInternal('09685743644130598252') ,
                                  dbo.TrackDataToInternal('09678015449675441820') ,
                                  dbo.TrackDataToInternal('09678015449675352744') ,
                                  dbo.TrackDataToInternal('09669232903372066716') ,
                                  dbo.TrackDataToInternal('09669232903372028800') ,
                                  dbo.TrackDataToInternal('09661502312551793056') ,
                                  dbo.TrackDataToInternal('09661502312551759252') ,
                                  dbo.TrackDataToInternal('09657619458111094624') ,
                                  dbo.TrackDataToInternal('09657619358110955152') ,
                                  dbo.TrackDataToInternal('09657619358110950020') ,
                                  dbo.TrackDataToInternal('09652519049303113940') ,
                                  dbo.TrackDataToInternal('09652519049302162616') ,
                                  dbo.TrackDataToInternal('09648647072798679464') ,
                                  dbo.TrackDataToInternal('09644788389762416332') ,
                                  dbo.TrackDataToInternal('09644788389762319064') ,
                                  dbo.TrackDataToInternal('09640920828484270024') ,
                                  dbo.TrackDataToInternal('09640920828484264892') ,
                                  dbo.TrackDataToInternal('09634251595070224544') ,
                                  dbo.TrackDataToInternal('09634251595070131388') ,
                                  dbo.TrackDataToInternal('09634205218176696288') ,
                                  dbo.TrackDataToInternal('09615425579364596184') ,
                                  dbo.TrackDataToInternal('09611272145063266032') ,
                                  dbo.TrackDataToInternal('09607659735452798968') ,
                                  dbo.TrackDataToInternal('09607659735452761068') ,
                                  dbo.TrackDataToInternal('09600988319992857836') ,
                                  dbo.TrackDataToInternal('09597126256272984540') ,
                                  dbo.TrackDataToInternal('09597126256272942528') ,
                                  dbo.TrackDataToInternal('09587140567104146400') ,
                                  dbo.TrackDataToInternal('09578168281993291512') ,
                                  dbo.TrackDataToInternal('09559751411358941132') ,
                                  dbo.TrackDataToInternal('09559751311358801660') ,
                                  dbo.TrackDataToInternal('09559751311358796512') ,
                                  dbo.TrackDataToInternal('09552121875608282356') ,
                                  dbo.TrackDataToInternal('09477622938977732448') ,
                                  dbo.TrackDataToInternal('10012767461753513664') ,
                                  dbo.TrackDataToInternal('10012722188811896588') ,
                                  dbo.TrackDataToInternal('09971363138203510312') ,
                                  dbo.TrackDataToInternal('09967626689906075124') ,
                                  dbo.TrackDataToInternal('09963524744187485532') ,
                                  dbo.TrackDataToInternal('10174464974799092776') )

 

