using System;
using System.IO;
using System.Net;
using System.Text;
using System.Net.Sockets;
using System.Net.Security;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Xml;
using WSI.Common;
using WSI.WC2;
using System.Collections.Generic;
using System.Collections;

namespace WSI.WC2_ClientSimulator
{
  public delegate void OnMessageReceived (SimulatedTerminal Sender, String Xml);
 
  public class SimulatedTerminal
  {

    public enum WC2_SimulationSteps
    {
      WC2_SETP_GET_UNENROLL
    , WC2_SETP_GET_ENROLL
    , WC2_SETP_GET_PARAMS
    , WC2_SETP_GET_CARDS
    , WC2_SETP_GET_PATTERNS
    , WC2_SETP_PLAY
    , WC2_SETP_CANCEL_PLAY
    , WC2_SETP_GET_JACKPOT
    , WC2_SETP_SITE_JACKPOT
    }

    const String WC2_ServiceCertificateId = "Andreu Juli� Quiles"; // <-- AJQ :-)
    TcpClient tcp_client;
    SslStream ssl_stream;
    Byte[] buffer;

    public String m_remote; 

    public int idx;

    public OnMessageReceived MessageReceived;


    public String server_id;
    public String terminal_id;

    public Int64 server_session_id;
    public Int64 terminal_session_id;

    public Int64 sequence_id;
    public Int64 transaction_id;

    public String card_id;
    public Int64 card_session_id;
    public Int64 card_balance;

    public DateTime last_sent_message;
    public int last_sent_message_ticks;
    public int last_play_tick;
    public int pending_reply_tick;

    public WC2_EventTypesCodes ev_type;

    // Simulation variables
    public Boolean enrolled;
    public Boolean pending_get_parameters;
    public Boolean pending_get_cards;
    public int pending_plays;
    public int time_between_plays;
    public WC2_SimulationSteps step;

    public Boolean pending_reply;

    public ArrayList Cards = new ArrayList();
    public ArrayList Jackpots = new ArrayList();
    public ArrayList SiteJackpots = new ArrayList ();
    public WC2_MsgTypes LastJackpotReceived;
    public ArrayList Prizes = new ArrayList ();

    public double total_prize_amount;

    // Animation prizes
    public Boolean anim_prize;
    public DateTime anim_prize_datetime;
    public int idx_prize;
    public int idx_card_prize;

    // Draw
    public Int64 DrawId = 0;
    public Int64 LastDrawId = 0;
    public WC2_Draw Draw = new WC2_Draw();

    public Boolean play_delayed = false;

    public Boolean jackpot_awarded = false;
    public String jackpot_amount = "";
    public String jackpot_name = "";

    public WC2_MsgTypes message_send;

    // intervals
    public Int32 last_play;
    public Int32 last_get_params;
    public Int32 last_get_cards;
    public Int32 last_get_jackpot;
    public Int32 last_site_jackpot;
    public Int32 last_get_patterns;

    public SimulatedTerminal ()
    {
      buffer = new Byte[64 * 1024];

      ev_type = WC2_EventTypesCodes.WC2_EV_DOOR_OPENED;
      enrolled = false;
      pending_reply = false;
      step = WC2_SimulationSteps.WC2_SETP_PLAY;
      anim_prize = false;
    }


    public void Send (String Xml)
    {
      Byte[] data;

      //Convert the data to send into a byte array
      data = Encoding.UTF8.GetBytes (Xml);

      try
      {
        if (IsConnected && ssl_stream.CanWrite)
        {
          ssl_stream.Write (data, 0, data.Length);

          last_sent_message = DateTime.Now;
          last_sent_message_ticks = Environment.TickCount;
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
    }

    public Boolean IsConnected 
    {
      get
      {
        if (tcp_client == null)
        {
          return false;
        }

        return tcp_client.Client.Connected;
      }
    }

    public void Disconnect()
    {
      try
      {
        ssl_stream.Close();
        tcp_client.Close();
        enrolled = false;
        card_session_id = 0;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
    }

    public void Connect (String  WC2_ServiceAddress)
    {
      try
      {
        IPEndPoint WC2_service_ip;

        WC2_service_ip = Misc.IPEndPointParse(WC2_ServiceAddress);

        // Step 1.
        // Instantiate a TcpClient with the target server and port number
        tcp_client = new TcpClient();

        tcp_client.Connect(WC2_service_ip);

        // Step 3.
        // Specify the callback function that will act as the validation delegate. This lets you inspect the certificate to see if it meets your
        // validation requirements.
        RemoteCertificateValidationCallback callback1 = new RemoteCertificateValidationCallback(OnCertificateValidation);
        LocalCertificateSelectionCallback callback2 = new LocalCertificateSelectionCallback(OnLocalCertificateSelectionCallback);
        // Step 4.
        // Instantiate an SslStream with the NetworkStream returned from the TcpClient.
        ssl_stream = new SslStream(tcp_client.GetStream(), true, callback1, callback2);

        // Step 5.
        // As a client, you can authenticate the server and validate the results using the SslStream.
        // This is the host name of the server you are connecting to, which may or may not be the name used
        // to connect to the server when TcpClient is instantiated.

        ssl_stream.AuthenticateAsClient(WC2_ServiceCertificateId);
        if (ssl_stream.IsAuthenticated)
        {
          // Indicates whether the authentication was successful.
          Console.WriteLine("IsAuthenticated: {0}", ssl_stream.IsAuthenticated);
          // Indicates whether both the client and server has been authenticated.
          // In this example only the server is authenticated.
          Console.WriteLine("IsMutuallyAuthenticated: {0}", ssl_stream.IsMutuallyAuthenticated);
          // Indicates whether the SslStream uses data encryption.
          Console.WriteLine("IsEncrypted: {0}", ssl_stream.IsEncrypted);
          // Indicates whether the data sent is signed.
          Console.WriteLine("IsSigned: {0}", ssl_stream.IsSigned);
          // Indicates whether the current side of the connection is authenticated as a server.
          Console.WriteLine("IsServer: {0}", ssl_stream.IsServer);
        }


        ssl_stream.BeginRead(this.buffer, 0, this.buffer.Length,
                              new AsyncCallback(ReceiveCallback), this);
      }
      catch (AuthenticationException ex)
      {
        Console.WriteLine(ex.Message);
      }
      catch (SocketException ex)
      {
        Console.WriteLine(ex.Message);
      }
      catch (IOException ex)
      {
        Console.WriteLine(ex.Message);
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
      finally
      {
      }    
    }

    static void ReceiveCallback (IAsyncResult AsyncResult)
    {
      SimulatedTerminal terminal0;
      String xml;

      int received_bytes;

      terminal0 = (SimulatedTerminal) AsyncResult.AsyncState;

      try
      {
        if (!terminal0.IsConnected)
        {
          return;
        }

        received_bytes = terminal0.ssl_stream.EndRead (AsyncResult);
        if (received_bytes > 0)
        {
          xml = Encoding.UTF8.GetString(terminal0.buffer, 0, received_bytes);

          terminal0.MessageReceived(terminal0, xml);

          terminal0.ssl_stream.BeginRead(terminal0.buffer, 0, terminal0.buffer.Length,
                                         new AsyncCallback(ReceiveCallback), terminal0);
        }
        else
        {
          terminal0.Disconnect();
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
    }


    private static X509Certificate OnLocalCertificateSelectionCallback (Object sender,
                                                                        string targetHost,
                                                                        X509CertificateCollection localCertificates,
                                                                        X509Certificate remoteCertificate,
                                                                        string[] acceptableIssuers)
    {
      X509Certificate2 xx509;

      // Send this certificate as the client certificate
      xx509 = new X509Certificate2 (@"WCP_ServerCertificate.pfx", "xixero08");

      return xx509;
    }


    // Check the certificate for errors and to make sure it meets your security policy.
    private static bool OnCertificateValidation (object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors errors)
    {
      return true;
    }
  }
}
