namespace WSI.WC2_ClientSimulator
{
  partial class Form1
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose (bool disposing)
    {
      if ( disposing && ( components != null ) )
      {
        components.Dispose ();
      }
      base.Dispose (disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent ()
    {
      this.components = new System.ComponentModel.Container ();
      this.btn_connect = new System.Windows.Forms.Button ();
      this.label1 = new System.Windows.Forms.Label ();
      this.lbl_terminal = new System.Windows.Forms.Label ();
      this.splitContainer1 = new System.Windows.Forms.SplitContainer ();
      this.splitContainer3 = new System.Windows.Forms.SplitContainer ();
      this.lbl_jackpot_name = new System.Windows.Forms.Label ();
      this.txt_snd_terminal = new System.Windows.Forms.TextBox ();
      this.lbl_jackpot_amount = new System.Windows.Forms.Label ();
      this.txt_rcv_terminal = new System.Windows.Forms.TextBox ();
      this.txt_trm_session_id = new System.Windows.Forms.TextBox ();
      this.label5 = new System.Windows.Forms.Label ();
      this.btn_enroll_terminal = new System.Windows.Forms.Button ();
      this.txt_trm_seq_id = new System.Windows.Forms.TextBox ();
      this.label7 = new System.Windows.Forms.Label ();
      this.btn_report_play = new System.Windows.Forms.Button ();
      this.btn_unenroll_terminal = new System.Windows.Forms.Button ();
      this.tmr_server_keep_alive = new System.Windows.Forms.Timer (this.components);
      this.trm_keep_alive_terminal = new System.Windows.Forms.Timer (this.components);
      this.btn_keep_alive = new System.Windows.Forms.Button ();
      this.btn_start_rnd_simulation = new System.Windows.Forms.Button ();
      this.label9 = new System.Windows.Forms.Label ();
      this.txt_pending_plays = new System.Windows.Forms.TextBox ();
      this.btn_send_server = new System.Windows.Forms.Button ();
      this.btn_send_terminal = new System.Windows.Forms.Button ();
      this.txt_num_terminals = new System.Windows.Forms.TextBox ();
      this.lbl_num_terminals = new System.Windows.Forms.Label ();
      this.lbl_keep_alive = new System.Windows.Forms.Label ();
      this.lbl_simulation = new System.Windows.Forms.Label ();
      this.btn_clear_server = new System.Windows.Forms.Button ();
      this.btn_clear_terminal = new System.Windows.Forms.Button ();
      this.cmb_service_address = new System.Windows.Forms.ComboBox ();
      this.lbl_trx = new System.Windows.Forms.Label ();
      this.tmr_refresh = new System.Windows.Forms.Timer (this.components);
      this.chk_sim_plays = new System.Windows.Forms.CheckBox ();
      this.cmb_prefix = new System.Windows.Forms.ComboBox ();
      this.label2 = new System.Windows.Forms.Label ();
      this.lbl_server_connected = new System.Windows.Forms.Label ();
      this.lbl_terminals_conn = new System.Windows.Forms.Label ();
      this.groupBox1 = new System.Windows.Forms.GroupBox ();
      this.lbl_terminals_start_session = new System.Windows.Forms.Label ();
      this.lbl_terminals_enrolled = new System.Windows.Forms.Label ();
      this.chk_time_plays = new System.Windows.Forms.CheckBox ();
      this.txt_time_plays = new System.Windows.Forms.TextBox ();
      this.label10 = new System.Windows.Forms.Label ();
      this.btn_request_service = new System.Windows.Forms.Button ();
      this.btn_cancel_play = new System.Windows.Forms.Button ();
      this.btn_get_cards = new System.Windows.Forms.Button ();
      this.btn_get_jackpot_info = new System.Windows.Forms.Button ();
      this.btn_get_patterns = new System.Windows.Forms.Button ();
      this.btn_get_params = new System.Windows.Forms.Button ();
      this.label4 = new System.Windows.Forms.Label ();
      this.txt_jackpot_0 = new System.Windows.Forms.TextBox ();
      this.txt_jackpot_1 = new System.Windows.Forms.TextBox ();
      this.txt_jackpot_2 = new System.Windows.Forms.TextBox ();
      this.label6 = new System.Windows.Forms.Label ();
      this.txt_draw_id = new System.Windows.Forms.TextBox ();
      this.txt_draw_numbers = new System.Windows.Forms.TextBox ();
      this.label8 = new System.Windows.Forms.Label ();
      this.lbl_play_delayed = new System.Windows.Forms.Label ();
      this.pnl_cards = new System.Windows.Forms.Panel ();
      this.lbl_card_24 = new System.Windows.Forms.Label ();
      this.lbl_card_23 = new System.Windows.Forms.Label ();
      this.lbl_card_22 = new System.Windows.Forms.Label ();
      this.lbl_card_21 = new System.Windows.Forms.Label ();
      this.lbl_card_20 = new System.Windows.Forms.Label ();
      this.lbl_card_19 = new System.Windows.Forms.Label ();
      this.lbl_card_18 = new System.Windows.Forms.Label ();
      this.lbl_card_17 = new System.Windows.Forms.Label ();
      this.lbl_card_16 = new System.Windows.Forms.Label ();
      this.lbl_card_15 = new System.Windows.Forms.Label ();
      this.lbl_card_14 = new System.Windows.Forms.Label ();
      this.lbl_card_13 = new System.Windows.Forms.Label ();
      this.lbl_card_12 = new System.Windows.Forms.Label ();
      this.lbl_card_11 = new System.Windows.Forms.Label ();
      this.lbl_card_10 = new System.Windows.Forms.Label ();
      this.lbl_card_9 = new System.Windows.Forms.Label ();
      this.lbl_card_8 = new System.Windows.Forms.Label ();
      this.lbl_card_7 = new System.Windows.Forms.Label ();
      this.lbl_card_6 = new System.Windows.Forms.Label ();
      this.lbl_card_5 = new System.Windows.Forms.Label ();
      this.lbl_card_4 = new System.Windows.Forms.Label ();
      this.lbl_card_3 = new System.Windows.Forms.Label ();
      this.lbl_card_2 = new System.Windows.Forms.Label ();
      this.lbl_card_1 = new System.Windows.Forms.Label ();
      this.lbl_card_0 = new System.Windows.Forms.Label ();
      this.txt_Jackpot_name_2 = new System.Windows.Forms.TextBox ();
      this.txt_Jackpot_name_1 = new System.Windows.Forms.TextBox ();
      this.txt_Jackpot_name_0 = new System.Windows.Forms.TextBox ();
      this.label3 = new System.Windows.Forms.Label ();
      this.label11 = new System.Windows.Forms.Label ();
      this.lbl_prize = new System.Windows.Forms.Label ();
      this.lbl_card_prize = new System.Windows.Forms.Label ();
      this.lbl_req_trx = new System.Windows.Forms.Label ();
      this.txt_statistics = new System.Windows.Forms.TextBox ();
      this.btn_clr_statistics = new System.Windows.Forms.Button ();
      this.chk_enroll_terminal = new System.Windows.Forms.CheckBox ();
      this.chk_get_params = new System.Windows.Forms.CheckBox ();
      this.chk_play = new System.Windows.Forms.CheckBox ();
      this.chk_get_cards = new System.Windows.Forms.CheckBox ();
      this.chk_cancel_play = new System.Windows.Forms.CheckBox ();
      this.chk_get_patterns = new System.Windows.Forms.CheckBox ();
      this.chk_get_jackpot = new System.Windows.Forms.CheckBox ();
      this.chk_unenroll = new System.Windows.Forms.CheckBox ();
      this.label12 = new System.Windows.Forms.Label ();
      this.label13 = new System.Windows.Forms.Label ();
      this.tabControl1 = new System.Windows.Forms.TabControl ();
      this.tabPage2 = new System.Windows.Forms.TabPage ();
      this.splitContainer2 = new System.Windows.Forms.SplitContainer ();
      this.tabPage1 = new System.Windows.Forms.TabPage ();
      this.txt_play_intv = new System.Windows.Forms.TextBox ();
      this.txt_get_cards_intv = new System.Windows.Forms.TextBox ();
      this.txt_get_params_intvl = new System.Windows.Forms.TextBox ();
      this.label14 = new System.Windows.Forms.Label ();
      this.textBox1 = new System.Windows.Forms.TextBox ();
      this.label15 = new System.Windows.Forms.Label ();
      this.textBox5 = new System.Windows.Forms.TextBox ();
      this.txt_patterns_intv = new System.Windows.Forms.TextBox ();
      this.txt_jackpot_intv = new System.Windows.Forms.TextBox ();
      this.textBox8 = new System.Windows.Forms.TextBox ();
      this.btn_site_jackpot = new System.Windows.Forms.Button ();
      this.chk_site_jackpot = new System.Windows.Forms.CheckBox ();
      this.txt_site_jackpot_intv = new System.Windows.Forms.TextBox ();
      this.splitContainer1.Panel2.SuspendLayout ();
      this.splitContainer1.SuspendLayout ();
      this.splitContainer3.Panel1.SuspendLayout ();
      this.splitContainer3.Panel2.SuspendLayout ();
      this.splitContainer3.SuspendLayout ();
      this.groupBox1.SuspendLayout ();
      this.pnl_cards.SuspendLayout ();
      this.tabControl1.SuspendLayout ();
      this.tabPage2.SuspendLayout ();
      this.splitContainer2.Panel1.SuspendLayout ();
      this.splitContainer2.Panel2.SuspendLayout ();
      this.splitContainer2.SuspendLayout ();
      this.tabPage1.SuspendLayout ();
      this.SuspendLayout ();
      // 
      // btn_connect
      // 
      this.btn_connect.Location = new System.Drawing.Point (110, 109);
      this.btn_connect.Name = "btn_connect";
      this.btn_connect.Size = new System.Drawing.Size (63, 23);
      this.btn_connect.TabIndex = 5;
      this.btn_connect.Text = "Connect";
      this.btn_connect.UseVisualStyleBackColor = true;
      this.btn_connect.Click += new System.EventHandler (this.btn_connect_Click);
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point (9, 47);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size (111, 13);
      this.label1.TabIndex = 3;
      this.label1.Text = "WC2 Service Address";
      // 
      // lbl_terminal
      // 
      this.lbl_terminal.Font = new System.Drawing.Font ("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ));
      this.lbl_terminal.ForeColor = System.Drawing.Color.FromArgb (( (int) ( ( (byte) ( 0 ) ) ) ), ( (int) ( ( (byte) ( 192 ) ) ) ), ( (int) ( ( (byte) ( 0 ) ) ) ));
      this.lbl_terminal.Location = new System.Drawing.Point (193, 4);
      this.lbl_terminal.Name = "lbl_terminal";
      this.lbl_terminal.Size = new System.Drawing.Size (161, 29);
      this.lbl_terminal.TabIndex = 4;
      this.lbl_terminal.Text = "Terminal";
      this.lbl_terminal.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // splitContainer1
      // 
      this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
      this.splitContainer1.Location = new System.Drawing.Point (3, 3);
      this.splitContainer1.Name = "splitContainer1";
      // 
      // splitContainer1.Panel1
      // 
      this.splitContainer1.Panel1.Enabled = false;
      this.splitContainer1.Panel1MinSize = 0;
      // 
      // splitContainer1.Panel2
      // 
      this.splitContainer1.Panel2.Controls.Add (this.splitContainer3);
      this.splitContainer1.Size = new System.Drawing.Size (1102, 411);
      this.splitContainer1.SplitterDistance = 1;
      this.splitContainer1.SplitterWidth = 1;
      this.splitContainer1.TabIndex = 8;
      // 
      // splitContainer3
      // 
      this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer3.Location = new System.Drawing.Point (0, 0);
      this.splitContainer3.Name = "splitContainer3";
      this.splitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
      // 
      // splitContainer3.Panel1
      // 
      this.splitContainer3.Panel1.Controls.Add (this.lbl_jackpot_name);
      this.splitContainer3.Panel1.Controls.Add (this.txt_snd_terminal);
      // 
      // splitContainer3.Panel2
      // 
      this.splitContainer3.Panel2.Controls.Add (this.lbl_jackpot_amount);
      this.splitContainer3.Panel2.Controls.Add (this.txt_rcv_terminal);
      this.splitContainer3.Size = new System.Drawing.Size (1100, 411);
      this.splitContainer3.SplitterDistance = 187;
      this.splitContainer3.TabIndex = 0;
      // 
      // lbl_jackpot_name
      // 
      this.lbl_jackpot_name.BackColor = System.Drawing.Color.FromArgb (( (int) ( ( (byte) ( 255 ) ) ) ), ( (int) ( ( (byte) ( 128 ) ) ) ), ( (int) ( ( (byte) ( 128 ) ) ) ));
      this.lbl_jackpot_name.Enabled = false;
      this.lbl_jackpot_name.Font = new System.Drawing.Font ("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ));
      this.lbl_jackpot_name.ForeColor = System.Drawing.Color.White;
      this.lbl_jackpot_name.Location = new System.Drawing.Point (212, 64);
      this.lbl_jackpot_name.Name = "lbl_jackpot_name";
      this.lbl_jackpot_name.Size = new System.Drawing.Size (651, 77);
      this.lbl_jackpot_name.TabIndex = 109;
      this.lbl_jackpot_name.Text = "Jackpot: GOLD";
      this.lbl_jackpot_name.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.lbl_jackpot_name.Visible = false;
      // 
      // txt_snd_terminal
      // 
      this.txt_snd_terminal.Dock = System.Windows.Forms.DockStyle.Fill;
      this.txt_snd_terminal.Location = new System.Drawing.Point (0, 0);
      this.txt_snd_terminal.Multiline = true;
      this.txt_snd_terminal.Name = "txt_snd_terminal";
      this.txt_snd_terminal.Size = new System.Drawing.Size (1100, 187);
      this.txt_snd_terminal.TabIndex = 0;
      // 
      // lbl_jackpot_amount
      // 
      this.lbl_jackpot_amount.BackColor = System.Drawing.Color.Red;
      this.lbl_jackpot_amount.Enabled = false;
      this.lbl_jackpot_amount.Font = new System.Drawing.Font ("Microsoft Sans Serif", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ));
      this.lbl_jackpot_amount.ForeColor = System.Drawing.Color.White;
      this.lbl_jackpot_amount.Location = new System.Drawing.Point (98, 9);
      this.lbl_jackpot_amount.Name = "lbl_jackpot_amount";
      this.lbl_jackpot_amount.Size = new System.Drawing.Size (875, 131);
      this.lbl_jackpot_amount.TabIndex = 110;
      this.lbl_jackpot_amount.Text = "99,999,999.00";
      this.lbl_jackpot_amount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.lbl_jackpot_amount.Visible = false;
      // 
      // txt_rcv_terminal
      // 
      this.txt_rcv_terminal.Dock = System.Windows.Forms.DockStyle.Fill;
      this.txt_rcv_terminal.Location = new System.Drawing.Point (0, 0);
      this.txt_rcv_terminal.Multiline = true;
      this.txt_rcv_terminal.Name = "txt_rcv_terminal";
      this.txt_rcv_terminal.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this.txt_rcv_terminal.Size = new System.Drawing.Size (1100, 220);
      this.txt_rcv_terminal.TabIndex = 0;
      // 
      // txt_trm_session_id
      // 
      this.txt_trm_session_id.Location = new System.Drawing.Point (217, 46);
      this.txt_trm_session_id.Name = "txt_trm_session_id";
      this.txt_trm_session_id.Size = new System.Drawing.Size (123, 20);
      this.txt_trm_session_id.TabIndex = 12;
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point (217, 30);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size (99, 13);
      this.label5.TabIndex = 11;
      this.label5.Text = "Terminal Session Id";
      // 
      // btn_enroll_terminal
      // 
      this.btn_enroll_terminal.Location = new System.Drawing.Point (360, 226);
      this.btn_enroll_terminal.Name = "btn_enroll_terminal";
      this.btn_enroll_terminal.Size = new System.Drawing.Size (95, 23);
      this.btn_enroll_terminal.TabIndex = 7;
      this.btn_enroll_terminal.Text = "Enroll Terminal";
      this.btn_enroll_terminal.UseVisualStyleBackColor = true;
      this.btn_enroll_terminal.Click += new System.EventHandler (this.btn_enroll_terminal_Click);
      // 
      // txt_trm_seq_id
      // 
      this.txt_trm_seq_id.Location = new System.Drawing.Point (217, 85);
      this.txt_trm_seq_id.Name = "txt_trm_seq_id";
      this.txt_trm_seq_id.Size = new System.Drawing.Size (123, 20);
      this.txt_trm_seq_id.TabIndex = 17;
      // 
      // label7
      // 
      this.label7.AutoSize = true;
      this.label7.Location = new System.Drawing.Point (214, 69);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size (68, 13);
      this.label7.TabIndex = 16;
      this.label7.Text = "Sequence Id";
      // 
      // btn_report_play
      // 
      this.btn_report_play.Location = new System.Drawing.Point (360, 310);
      this.btn_report_play.Name = "btn_report_play";
      this.btn_report_play.Size = new System.Drawing.Size (95, 23);
      this.btn_report_play.TabIndex = 23;
      this.btn_report_play.Text = "Play";
      this.btn_report_play.UseVisualStyleBackColor = true;
      this.btn_report_play.Click += new System.EventHandler (this.btn_report_play_Click);
      // 
      // btn_unenroll_terminal
      // 
      this.btn_unenroll_terminal.Location = new System.Drawing.Point (603, 226);
      this.btn_unenroll_terminal.Name = "btn_unenroll_terminal";
      this.btn_unenroll_terminal.Size = new System.Drawing.Size (95, 23);
      this.btn_unenroll_terminal.TabIndex = 14;
      this.btn_unenroll_terminal.Text = "Unenroll Terminal";
      this.btn_unenroll_terminal.UseVisualStyleBackColor = true;
      this.btn_unenroll_terminal.Click += new System.EventHandler (this.btn_unenroll_terminal_Click);
      // 
      // tmr_server_keep_alive
      // 
      this.tmr_server_keep_alive.Interval = 6000;
      this.tmr_server_keep_alive.Tick += new System.EventHandler (this.tmr_server_keep_alive_Tick);
      // 
      // trm_keep_alive_terminal
      // 
      this.trm_keep_alive_terminal.Interval = 6000;
      this.trm_keep_alive_terminal.Tick += new System.EventHandler (this.trm_keep_alive_terminal_Tick);
      // 
      // btn_keep_alive
      // 
      this.btn_keep_alive.Location = new System.Drawing.Point (737, 38);
      this.btn_keep_alive.Name = "btn_keep_alive";
      this.btn_keep_alive.Size = new System.Drawing.Size (75, 23);
      this.btn_keep_alive.TabIndex = 28;
      this.btn_keep_alive.Text = "KeepAlive";
      this.btn_keep_alive.UseVisualStyleBackColor = true;
      this.btn_keep_alive.Click += new System.EventHandler (this.btn_keep_alive_Click);
      // 
      // btn_start_rnd_simulation
      // 
      this.btn_start_rnd_simulation.Location = new System.Drawing.Point (884, 109);
      this.btn_start_rnd_simulation.Name = "btn_start_rnd_simulation";
      this.btn_start_rnd_simulation.Size = new System.Drawing.Size (143, 24);
      this.btn_start_rnd_simulation.TabIndex = 32;
      this.btn_start_rnd_simulation.Text = "Start/Stop Simulation";
      this.btn_start_rnd_simulation.UseVisualStyleBackColor = true;
      this.btn_start_rnd_simulation.Click += new System.EventHandler (this.btn_start_rnd_simulation_Click);
      // 
      // label9
      // 
      this.label9.AutoSize = true;
      this.label9.Location = new System.Drawing.Point (876, 0);
      this.label9.Name = "label9";
      this.label9.Size = new System.Drawing.Size (150, 13);
      this.label9.TabIndex = 36;
      this.label9.Text = "Simulation Plays (per Terminal)";
      // 
      // txt_pending_plays
      // 
      this.txt_pending_plays.Enabled = false;
      this.txt_pending_plays.Location = new System.Drawing.Point (894, 14);
      this.txt_pending_plays.MaxLength = 8;
      this.txt_pending_plays.Name = "txt_pending_plays";
      this.txt_pending_plays.Size = new System.Drawing.Size (123, 20);
      this.txt_pending_plays.TabIndex = 37;
      this.txt_pending_plays.Text = "1000";
      this.txt_pending_plays.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      // 
      // btn_send_server
      // 
      this.btn_send_server.Enabled = false;
      this.btn_send_server.Location = new System.Drawing.Point (107, 330);
      this.btn_send_server.Name = "btn_send_server";
      this.btn_send_server.Size = new System.Drawing.Size (91, 23);
      this.btn_send_server.TabIndex = 38;
      this.btn_send_server.Text = "Send";
      this.btn_send_server.UseVisualStyleBackColor = true;
      this.btn_send_server.Visible = false;
      this.btn_send_server.Click += new System.EventHandler (this.btn_send_server_Click);
      // 
      // btn_send_terminal
      // 
      this.btn_send_terminal.Anchor = ( (System.Windows.Forms.AnchorStyles) ( ( System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right ) ) );
      this.btn_send_terminal.Location = new System.Drawing.Point (1015, 334);
      this.btn_send_terminal.Name = "btn_send_terminal";
      this.btn_send_terminal.Size = new System.Drawing.Size (84, 23);
      this.btn_send_terminal.TabIndex = 39;
      this.btn_send_terminal.Text = "Send";
      this.btn_send_terminal.UseVisualStyleBackColor = true;
      this.btn_send_terminal.Click += new System.EventHandler (this.btn_send_terminal_Click);
      // 
      // txt_num_terminals
      // 
      this.txt_num_terminals.Location = new System.Drawing.Point (12, 111);
      this.txt_num_terminals.Name = "txt_num_terminals";
      this.txt_num_terminals.Size = new System.Drawing.Size (87, 20);
      this.txt_num_terminals.TabIndex = 4;
      this.txt_num_terminals.Text = "10";
      this.txt_num_terminals.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      // 
      // lbl_num_terminals
      // 
      this.lbl_num_terminals.AutoSize = true;
      this.lbl_num_terminals.Location = new System.Drawing.Point (9, 95);
      this.lbl_num_terminals.Name = "lbl_num_terminals";
      this.lbl_num_terminals.Size = new System.Drawing.Size (80, 13);
      this.lbl_num_terminals.TabIndex = 41;
      this.lbl_num_terminals.Text = "Num. Terminals";
      // 
      // lbl_keep_alive
      // 
      this.lbl_keep_alive.Font = new System.Drawing.Font ("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ));
      this.lbl_keep_alive.ForeColor = System.Drawing.Color.FromArgb (( (int) ( ( (byte) ( 255 ) ) ) ), ( (int) ( ( (byte) ( 128 ) ) ) ), ( (int) ( ( (byte) ( 128 ) ) ) ));
      this.lbl_keep_alive.Location = new System.Drawing.Point (718, 9);
      this.lbl_keep_alive.Name = "lbl_keep_alive";
      this.lbl_keep_alive.Size = new System.Drawing.Size (113, 29);
      this.lbl_keep_alive.TabIndex = 43;
      this.lbl_keep_alive.Text = "Stopped";
      this.lbl_keep_alive.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // lbl_simulation
      // 
      this.lbl_simulation.Font = new System.Drawing.Font ("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ));
      this.lbl_simulation.ForeColor = System.Drawing.Color.FromArgb (( (int) ( ( (byte) ( 255 ) ) ) ), ( (int) ( ( (byte) ( 128 ) ) ) ), ( (int) ( ( (byte) ( 128 ) ) ) ));
      this.lbl_simulation.Location = new System.Drawing.Point (844, 82);
      this.lbl_simulation.Name = "lbl_simulation";
      this.lbl_simulation.Size = new System.Drawing.Size (228, 28);
      this.lbl_simulation.TabIndex = 44;
      this.lbl_simulation.Text = "Stopped";
      this.lbl_simulation.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // btn_clear_server
      // 
      this.btn_clear_server.Enabled = false;
      this.btn_clear_server.Location = new System.Drawing.Point (5, 330);
      this.btn_clear_server.Name = "btn_clear_server";
      this.btn_clear_server.Size = new System.Drawing.Size (90, 23);
      this.btn_clear_server.TabIndex = 45;
      this.btn_clear_server.Text = "Clear Response";
      this.btn_clear_server.UseVisualStyleBackColor = true;
      this.btn_clear_server.Visible = false;
      this.btn_clear_server.Click += new System.EventHandler (this.btn_clear_server_Click);
      // 
      // btn_clear_terminal
      // 
      this.btn_clear_terminal.Anchor = ( (System.Windows.Forms.AnchorStyles) ( ( System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right ) ) );
      this.btn_clear_terminal.Location = new System.Drawing.Point (919, 334);
      this.btn_clear_terminal.Name = "btn_clear_terminal";
      this.btn_clear_terminal.Size = new System.Drawing.Size (90, 23);
      this.btn_clear_terminal.TabIndex = 46;
      this.btn_clear_terminal.Text = "Clear Response";
      this.btn_clear_terminal.UseVisualStyleBackColor = true;
      this.btn_clear_terminal.Click += new System.EventHandler (this.btn_clear_terminal_Click);
      // 
      // cmb_service_address
      // 
      this.cmb_service_address.FormattingEnabled = true;
      this.cmb_service_address.Items.AddRange (new object[] {
            "192.168.1.12:13001",
            "192.168.1.13:13001",
            "192.168.1.30:13001",
            "192.168.1.4:13001",
            "192.168.1.56:13001",
            "192.168.1.55:13001",
            "192.168.1.54:13001",
            "80.32.96.92:13001"});
      this.cmb_service_address.Location = new System.Drawing.Point (12, 62);
      this.cmb_service_address.Name = "cmb_service_address";
      this.cmb_service_address.Size = new System.Drawing.Size (138, 21);
      this.cmb_service_address.TabIndex = 2;
      this.cmb_service_address.Text = "192.168.1.12:13001";
      // 
      // lbl_trx
      // 
      this.lbl_trx.Font = new System.Drawing.Font ("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ));
      this.lbl_trx.Location = new System.Drawing.Point (843, 171);
      this.lbl_trx.Name = "lbl_trx";
      this.lbl_trx.Size = new System.Drawing.Size (281, 24);
      this.lbl_trx.TabIndex = 47;
      this.lbl_trx.Text = "Trx/s";
      this.lbl_trx.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // tmr_refresh
      // 
      this.tmr_refresh.Enabled = true;
      this.tmr_refresh.Interval = 1000;
      this.tmr_refresh.Tick += new System.EventHandler (this.tmr_refresh_Tick);
      // 
      // chk_sim_plays
      // 
      this.chk_sim_plays.AutoSize = true;
      this.chk_sim_plays.Location = new System.Drawing.Point (873, 19);
      this.chk_sim_plays.Name = "chk_sim_plays";
      this.chk_sim_plays.Size = new System.Drawing.Size (15, 14);
      this.chk_sim_plays.TabIndex = 48;
      this.chk_sim_plays.UseVisualStyleBackColor = true;
      this.chk_sim_plays.CheckedChanged += new System.EventHandler (this.chk_sim_plays_CheckedChanged);
      // 
      // cmb_prefix
      // 
      this.cmb_prefix.FormattingEnabled = true;
      this.cmb_prefix.Items.AddRange (new object[] {
            "ANDREU",
            "ALBERTO",
            "RONALD",
            "TONI",
            "FAT1",
            "FAT2"});
      this.cmb_prefix.Location = new System.Drawing.Point (12, 19);
      this.cmb_prefix.Name = "cmb_prefix";
      this.cmb_prefix.Size = new System.Drawing.Size (138, 21);
      this.cmb_prefix.TabIndex = 1;
      this.cmb_prefix.Text = "ANDREU";
      this.cmb_prefix.SelectedIndexChanged += new System.EventHandler (this.cmb_prefix_SelectedIndexChanged);
      this.cmb_prefix.TextChanged += new System.EventHandler (this.cmb_prefix_TextChanged);
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point (13, 4);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size (55, 13);
      this.label2.TabIndex = 51;
      this.label2.Text = "Test Case";
      // 
      // lbl_server_connected
      // 
      this.lbl_server_connected.Enabled = false;
      this.lbl_server_connected.Font = new System.Drawing.Font ("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ));
      this.lbl_server_connected.ForeColor = System.Drawing.Color.FromArgb (( (int) ( ( (byte) ( 255 ) ) ) ), ( (int) ( ( (byte) ( 128 ) ) ) ), ( (int) ( ( (byte) ( 128 ) ) ) ));
      this.lbl_server_connected.Location = new System.Drawing.Point (9, 147);
      this.lbl_server_connected.Name = "lbl_server_connected";
      this.lbl_server_connected.Size = new System.Drawing.Size (178, 23);
      this.lbl_server_connected.TabIndex = 52;
      this.lbl_server_connected.Text = "Server Disconnected";
      this.lbl_server_connected.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.lbl_server_connected.Visible = false;
      // 
      // lbl_terminals_conn
      // 
      this.lbl_terminals_conn.Font = new System.Drawing.Font ("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ));
      this.lbl_terminals_conn.ForeColor = System.Drawing.Color.FromArgb (( (int) ( ( (byte) ( 255 ) ) ) ), ( (int) ( ( (byte) ( 128 ) ) ) ), ( (int) ( ( (byte) ( 128 ) ) ) ));
      this.lbl_terminals_conn.Location = new System.Drawing.Point (9, 170);
      this.lbl_terminals_conn.Name = "lbl_terminals_conn";
      this.lbl_terminals_conn.Size = new System.Drawing.Size (189, 23);
      this.lbl_terminals_conn.TabIndex = 53;
      this.lbl_terminals_conn.Text = "0 Terminals Connected";
      this.lbl_terminals_conn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // groupBox1
      // 
      this.groupBox1.Controls.Add (this.lbl_terminals_start_session);
      this.groupBox1.Controls.Add (this.lbl_terminals_enrolled);
      this.groupBox1.Font = new System.Drawing.Font ("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ));
      this.groupBox1.ForeColor = System.Drawing.Color.Black;
      this.groupBox1.Location = new System.Drawing.Point (847, 213);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new System.Drawing.Size (225, 55);
      this.groupBox1.TabIndex = 54;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = " Terminals ";
      // 
      // lbl_terminals_start_session
      // 
      this.lbl_terminals_start_session.Font = new System.Drawing.Font ("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ));
      this.lbl_terminals_start_session.ForeColor = System.Drawing.Color.FromArgb (( (int) ( ( (byte) ( 255 ) ) ) ), ( (int) ( ( (byte) ( 128 ) ) ) ), ( (int) ( ( (byte) ( 128 ) ) ) ));
      this.lbl_terminals_start_session.Location = new System.Drawing.Point (80, 42);
      this.lbl_terminals_start_session.Name = "lbl_terminals_start_session";
      this.lbl_terminals_start_session.Size = new System.Drawing.Size (136, 13);
      this.lbl_terminals_start_session.TabIndex = 55;
      this.lbl_terminals_start_session.Text = "0 Started Session";
      this.lbl_terminals_start_session.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.lbl_terminals_start_session.Visible = false;
      // 
      // lbl_terminals_enrolled
      // 
      this.lbl_terminals_enrolled.Font = new System.Drawing.Font ("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ));
      this.lbl_terminals_enrolled.ForeColor = System.Drawing.Color.FromArgb (( (int) ( ( (byte) ( 255 ) ) ) ), ( (int) ( ( (byte) ( 128 ) ) ) ), ( (int) ( ( (byte) ( 128 ) ) ) ));
      this.lbl_terminals_enrolled.Location = new System.Drawing.Point (12, 24);
      this.lbl_terminals_enrolled.Name = "lbl_terminals_enrolled";
      this.lbl_terminals_enrolled.Size = new System.Drawing.Size (189, 23);
      this.lbl_terminals_enrolled.TabIndex = 54;
      this.lbl_terminals_enrolled.Text = "0 Enrolled";
      this.lbl_terminals_enrolled.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // chk_time_plays
      // 
      this.chk_time_plays.AutoSize = true;
      this.chk_time_plays.Enabled = false;
      this.chk_time_plays.Location = new System.Drawing.Point (873, 59);
      this.chk_time_plays.Name = "chk_time_plays";
      this.chk_time_plays.Size = new System.Drawing.Size (15, 14);
      this.chk_time_plays.TabIndex = 60;
      this.chk_time_plays.UseVisualStyleBackColor = true;
      this.chk_time_plays.CheckedChanged += new System.EventHandler (this.chk_time_plays_CheckedChanged);
      // 
      // txt_time_plays
      // 
      this.txt_time_plays.Enabled = false;
      this.txt_time_plays.Location = new System.Drawing.Point (894, 54);
      this.txt_time_plays.MaxLength = 8;
      this.txt_time_plays.Name = "txt_time_plays";
      this.txt_time_plays.Size = new System.Drawing.Size (123, 20);
      this.txt_time_plays.TabIndex = 59;
      this.txt_time_plays.Text = "5000";
      this.txt_time_plays.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      // 
      // label10
      // 
      this.label10.AutoSize = true;
      this.label10.Location = new System.Drawing.Point (876, 40);
      this.label10.Name = "label10";
      this.label10.Size = new System.Drawing.Size (173, 13);
      this.label10.TabIndex = 58;
      this.label10.Text = "Time between Plays (millisecconds)";
      // 
      // btn_request_service
      // 
      this.btn_request_service.Location = new System.Drawing.Point (259, 226);
      this.btn_request_service.Name = "btn_request_service";
      this.btn_request_service.Size = new System.Drawing.Size (95, 23);
      this.btn_request_service.TabIndex = 61;
      this.btn_request_service.Text = "Request Service";
      this.btn_request_service.UseVisualStyleBackColor = true;
      this.btn_request_service.Click += new System.EventHandler (this.btn_request_service_Click);
      // 
      // btn_cancel_play
      // 
      this.btn_cancel_play.Location = new System.Drawing.Point (603, 338);
      this.btn_cancel_play.Name = "btn_cancel_play";
      this.btn_cancel_play.Size = new System.Drawing.Size (95, 23);
      this.btn_cancel_play.TabIndex = 62;
      this.btn_cancel_play.Text = "Cancel Play";
      this.btn_cancel_play.UseVisualStyleBackColor = true;
      this.btn_cancel_play.Click += new System.EventHandler (this.btn_cancel_play_Click);
      // 
      // btn_get_cards
      // 
      this.btn_get_cards.Location = new System.Drawing.Point (360, 282);
      this.btn_get_cards.Name = "btn_get_cards";
      this.btn_get_cards.Size = new System.Drawing.Size (96, 23);
      this.btn_get_cards.TabIndex = 63;
      this.btn_get_cards.Text = "Get Cards";
      this.btn_get_cards.UseVisualStyleBackColor = true;
      this.btn_get_cards.Click += new System.EventHandler (this.btn_get_cards_Click);
      // 
      // btn_get_jackpot_info
      // 
      this.btn_get_jackpot_info.Location = new System.Drawing.Point (603, 254);
      this.btn_get_jackpot_info.Name = "btn_get_jackpot_info";
      this.btn_get_jackpot_info.Size = new System.Drawing.Size (95, 23);
      this.btn_get_jackpot_info.TabIndex = 64;
      this.btn_get_jackpot_info.Text = "Get Jackpot Info";
      this.btn_get_jackpot_info.UseVisualStyleBackColor = true;
      this.btn_get_jackpot_info.Click += new System.EventHandler (this.btn_get_jackpot_info_Click);
      // 
      // btn_get_patterns
      // 
      this.btn_get_patterns.Location = new System.Drawing.Point (603, 310);
      this.btn_get_patterns.Name = "btn_get_patterns";
      this.btn_get_patterns.Size = new System.Drawing.Size (95, 23);
      this.btn_get_patterns.TabIndex = 65;
      this.btn_get_patterns.Text = "Get Patterns";
      this.btn_get_patterns.UseVisualStyleBackColor = true;
      this.btn_get_patterns.Click += new System.EventHandler (this.btn_get_patterns_Click);
      // 
      // btn_get_params
      // 
      this.btn_get_params.Location = new System.Drawing.Point (360, 254);
      this.btn_get_params.Name = "btn_get_params";
      this.btn_get_params.Size = new System.Drawing.Size (96, 23);
      this.btn_get_params.TabIndex = 66;
      this.btn_get_params.Text = "Get Parameters";
      this.btn_get_params.UseVisualStyleBackColor = true;
      this.btn_get_params.Click += new System.EventHandler (this.btn_get_params_Click);
      // 
      // label4
      // 
      this.label4.Location = new System.Drawing.Point (534, 1);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size (146, 16);
      this.label4.TabIndex = 92;
      this.label4.Text = "Jackpots";
      // 
      // txt_jackpot_0
      // 
      this.txt_jackpot_0.Location = new System.Drawing.Point (599, 16);
      this.txt_jackpot_0.Name = "txt_jackpot_0";
      this.txt_jackpot_0.Size = new System.Drawing.Size (113, 20);
      this.txt_jackpot_0.TabIndex = 93;
      // 
      // txt_jackpot_1
      // 
      this.txt_jackpot_1.Location = new System.Drawing.Point (599, 37);
      this.txt_jackpot_1.Name = "txt_jackpot_1";
      this.txt_jackpot_1.Size = new System.Drawing.Size (113, 20);
      this.txt_jackpot_1.TabIndex = 94;
      // 
      // txt_jackpot_2
      // 
      this.txt_jackpot_2.Location = new System.Drawing.Point (599, 58);
      this.txt_jackpot_2.Name = "txt_jackpot_2";
      this.txt_jackpot_2.Size = new System.Drawing.Size (113, 20);
      this.txt_jackpot_2.TabIndex = 95;
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Location = new System.Drawing.Point (217, 135);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size (47, 13);
      this.label6.TabIndex = 96;
      this.label6.Text = "Draw Id:";
      // 
      // txt_draw_id
      // 
      this.txt_draw_id.Location = new System.Drawing.Point (263, 132);
      this.txt_draw_id.Name = "txt_draw_id";
      this.txt_draw_id.Size = new System.Drawing.Size (91, 20);
      this.txt_draw_id.TabIndex = 97;
      // 
      // txt_draw_numbers
      // 
      this.txt_draw_numbers.Location = new System.Drawing.Point (263, 155);
      this.txt_draw_numbers.Multiline = true;
      this.txt_draw_numbers.Name = "txt_draw_numbers";
      this.txt_draw_numbers.Size = new System.Drawing.Size (578, 40);
      this.txt_draw_numbers.TabIndex = 99;
      // 
      // label8
      // 
      this.label8.AutoSize = true;
      this.label8.Location = new System.Drawing.Point (184, 158);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size (80, 13);
      this.label8.TabIndex = 98;
      this.label8.Text = "Draw Numbers:";
      // 
      // lbl_play_delayed
      // 
      this.lbl_play_delayed.BackColor = System.Drawing.Color.FromArgb (( (int) ( ( (byte) ( 0 ) ) ) ), ( (int) ( ( (byte) ( 192 ) ) ) ), ( (int) ( ( (byte) ( 0 ) ) ) ));
      this.lbl_play_delayed.Enabled = false;
      this.lbl_play_delayed.Font = new System.Drawing.Font ("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ));
      this.lbl_play_delayed.ForeColor = System.Drawing.Color.White;
      this.lbl_play_delayed.Location = new System.Drawing.Point (556, 125);
      this.lbl_play_delayed.Name = "lbl_play_delayed";
      this.lbl_play_delayed.Size = new System.Drawing.Size (178, 21);
      this.lbl_play_delayed.TabIndex = 100;
      this.lbl_play_delayed.Text = "PLAY DELAYED";
      this.lbl_play_delayed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.lbl_play_delayed.Visible = false;
      // 
      // pnl_cards
      // 
      this.pnl_cards.Controls.Add (this.lbl_card_24);
      this.pnl_cards.Controls.Add (this.lbl_card_23);
      this.pnl_cards.Controls.Add (this.lbl_card_22);
      this.pnl_cards.Controls.Add (this.lbl_card_21);
      this.pnl_cards.Controls.Add (this.lbl_card_20);
      this.pnl_cards.Controls.Add (this.lbl_card_19);
      this.pnl_cards.Controls.Add (this.lbl_card_18);
      this.pnl_cards.Controls.Add (this.lbl_card_17);
      this.pnl_cards.Controls.Add (this.lbl_card_16);
      this.pnl_cards.Controls.Add (this.lbl_card_15);
      this.pnl_cards.Controls.Add (this.lbl_card_14);
      this.pnl_cards.Controls.Add (this.lbl_card_13);
      this.pnl_cards.Controls.Add (this.lbl_card_12);
      this.pnl_cards.Controls.Add (this.lbl_card_11);
      this.pnl_cards.Controls.Add (this.lbl_card_10);
      this.pnl_cards.Controls.Add (this.lbl_card_9);
      this.pnl_cards.Controls.Add (this.lbl_card_8);
      this.pnl_cards.Controls.Add (this.lbl_card_7);
      this.pnl_cards.Controls.Add (this.lbl_card_6);
      this.pnl_cards.Controls.Add (this.lbl_card_5);
      this.pnl_cards.Controls.Add (this.lbl_card_4);
      this.pnl_cards.Controls.Add (this.lbl_card_3);
      this.pnl_cards.Controls.Add (this.lbl_card_2);
      this.pnl_cards.Controls.Add (this.lbl_card_1);
      this.pnl_cards.Controls.Add (this.lbl_card_0);
      this.pnl_cards.Location = new System.Drawing.Point (360, 4);
      this.pnl_cards.Name = "pnl_cards";
      this.pnl_cards.Size = new System.Drawing.Size (149, 150);
      this.pnl_cards.TabIndex = 1;
      // 
      // lbl_card_24
      // 
      this.lbl_card_24.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
      this.lbl_card_24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.lbl_card_24.Font = new System.Drawing.Font ("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ));
      this.lbl_card_24.Location = new System.Drawing.Point (116, 116);
      this.lbl_card_24.Name = "lbl_card_24";
      this.lbl_card_24.Size = new System.Drawing.Size (30, 30);
      this.lbl_card_24.TabIndex = 116;
      this.lbl_card_24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lbl_card_23
      // 
      this.lbl_card_23.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
      this.lbl_card_23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.lbl_card_23.Font = new System.Drawing.Font ("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ));
      this.lbl_card_23.Location = new System.Drawing.Point (116, 87);
      this.lbl_card_23.Name = "lbl_card_23";
      this.lbl_card_23.Size = new System.Drawing.Size (30, 30);
      this.lbl_card_23.TabIndex = 115;
      this.lbl_card_23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lbl_card_22
      // 
      this.lbl_card_22.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
      this.lbl_card_22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.lbl_card_22.Font = new System.Drawing.Font ("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ));
      this.lbl_card_22.Location = new System.Drawing.Point (116, 58);
      this.lbl_card_22.Name = "lbl_card_22";
      this.lbl_card_22.Size = new System.Drawing.Size (30, 30);
      this.lbl_card_22.TabIndex = 114;
      this.lbl_card_22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lbl_card_21
      // 
      this.lbl_card_21.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
      this.lbl_card_21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.lbl_card_21.Font = new System.Drawing.Font ("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ));
      this.lbl_card_21.Location = new System.Drawing.Point (116, 29);
      this.lbl_card_21.Name = "lbl_card_21";
      this.lbl_card_21.Size = new System.Drawing.Size (30, 30);
      this.lbl_card_21.TabIndex = 113;
      this.lbl_card_21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lbl_card_20
      // 
      this.lbl_card_20.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
      this.lbl_card_20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.lbl_card_20.Font = new System.Drawing.Font ("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ));
      this.lbl_card_20.Location = new System.Drawing.Point (116, 0);
      this.lbl_card_20.Name = "lbl_card_20";
      this.lbl_card_20.Size = new System.Drawing.Size (30, 30);
      this.lbl_card_20.TabIndex = 112;
      this.lbl_card_20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lbl_card_19
      // 
      this.lbl_card_19.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
      this.lbl_card_19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.lbl_card_19.Font = new System.Drawing.Font ("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ));
      this.lbl_card_19.Location = new System.Drawing.Point (87, 116);
      this.lbl_card_19.Name = "lbl_card_19";
      this.lbl_card_19.Size = new System.Drawing.Size (30, 30);
      this.lbl_card_19.TabIndex = 111;
      this.lbl_card_19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lbl_card_18
      // 
      this.lbl_card_18.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
      this.lbl_card_18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.lbl_card_18.Font = new System.Drawing.Font ("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ));
      this.lbl_card_18.Location = new System.Drawing.Point (87, 87);
      this.lbl_card_18.Name = "lbl_card_18";
      this.lbl_card_18.Size = new System.Drawing.Size (30, 30);
      this.lbl_card_18.TabIndex = 110;
      this.lbl_card_18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lbl_card_17
      // 
      this.lbl_card_17.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
      this.lbl_card_17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.lbl_card_17.Font = new System.Drawing.Font ("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ));
      this.lbl_card_17.Location = new System.Drawing.Point (87, 58);
      this.lbl_card_17.Name = "lbl_card_17";
      this.lbl_card_17.Size = new System.Drawing.Size (30, 30);
      this.lbl_card_17.TabIndex = 109;
      this.lbl_card_17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lbl_card_16
      // 
      this.lbl_card_16.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
      this.lbl_card_16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.lbl_card_16.Font = new System.Drawing.Font ("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ));
      this.lbl_card_16.Location = new System.Drawing.Point (87, 29);
      this.lbl_card_16.Name = "lbl_card_16";
      this.lbl_card_16.Size = new System.Drawing.Size (30, 30);
      this.lbl_card_16.TabIndex = 108;
      this.lbl_card_16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lbl_card_15
      // 
      this.lbl_card_15.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
      this.lbl_card_15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.lbl_card_15.Font = new System.Drawing.Font ("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ));
      this.lbl_card_15.Location = new System.Drawing.Point (87, 0);
      this.lbl_card_15.Name = "lbl_card_15";
      this.lbl_card_15.Size = new System.Drawing.Size (30, 30);
      this.lbl_card_15.TabIndex = 107;
      this.lbl_card_15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lbl_card_14
      // 
      this.lbl_card_14.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
      this.lbl_card_14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.lbl_card_14.Font = new System.Drawing.Font ("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ));
      this.lbl_card_14.Location = new System.Drawing.Point (58, 116);
      this.lbl_card_14.Name = "lbl_card_14";
      this.lbl_card_14.Size = new System.Drawing.Size (30, 30);
      this.lbl_card_14.TabIndex = 106;
      this.lbl_card_14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lbl_card_13
      // 
      this.lbl_card_13.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
      this.lbl_card_13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.lbl_card_13.Font = new System.Drawing.Font ("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ));
      this.lbl_card_13.Location = new System.Drawing.Point (58, 87);
      this.lbl_card_13.Name = "lbl_card_13";
      this.lbl_card_13.Size = new System.Drawing.Size (30, 30);
      this.lbl_card_13.TabIndex = 105;
      this.lbl_card_13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lbl_card_12
      // 
      this.lbl_card_12.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
      this.lbl_card_12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.lbl_card_12.Font = new System.Drawing.Font ("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ));
      this.lbl_card_12.Location = new System.Drawing.Point (58, 58);
      this.lbl_card_12.Name = "lbl_card_12";
      this.lbl_card_12.Size = new System.Drawing.Size (30, 30);
      this.lbl_card_12.TabIndex = 104;
      this.lbl_card_12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lbl_card_11
      // 
      this.lbl_card_11.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
      this.lbl_card_11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.lbl_card_11.Font = new System.Drawing.Font ("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ));
      this.lbl_card_11.Location = new System.Drawing.Point (58, 29);
      this.lbl_card_11.Name = "lbl_card_11";
      this.lbl_card_11.Size = new System.Drawing.Size (30, 30);
      this.lbl_card_11.TabIndex = 103;
      this.lbl_card_11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lbl_card_10
      // 
      this.lbl_card_10.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
      this.lbl_card_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.lbl_card_10.Font = new System.Drawing.Font ("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ));
      this.lbl_card_10.Location = new System.Drawing.Point (58, 0);
      this.lbl_card_10.Name = "lbl_card_10";
      this.lbl_card_10.Size = new System.Drawing.Size (30, 30);
      this.lbl_card_10.TabIndex = 102;
      this.lbl_card_10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lbl_card_9
      // 
      this.lbl_card_9.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
      this.lbl_card_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.lbl_card_9.Font = new System.Drawing.Font ("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ));
      this.lbl_card_9.Location = new System.Drawing.Point (29, 116);
      this.lbl_card_9.Name = "lbl_card_9";
      this.lbl_card_9.Size = new System.Drawing.Size (30, 30);
      this.lbl_card_9.TabIndex = 101;
      this.lbl_card_9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lbl_card_8
      // 
      this.lbl_card_8.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
      this.lbl_card_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.lbl_card_8.Font = new System.Drawing.Font ("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ));
      this.lbl_card_8.Location = new System.Drawing.Point (29, 87);
      this.lbl_card_8.Name = "lbl_card_8";
      this.lbl_card_8.Size = new System.Drawing.Size (30, 30);
      this.lbl_card_8.TabIndex = 100;
      this.lbl_card_8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lbl_card_7
      // 
      this.lbl_card_7.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
      this.lbl_card_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.lbl_card_7.Font = new System.Drawing.Font ("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ));
      this.lbl_card_7.Location = new System.Drawing.Point (29, 58);
      this.lbl_card_7.Name = "lbl_card_7";
      this.lbl_card_7.Size = new System.Drawing.Size (30, 30);
      this.lbl_card_7.TabIndex = 99;
      this.lbl_card_7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lbl_card_6
      // 
      this.lbl_card_6.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
      this.lbl_card_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.lbl_card_6.Font = new System.Drawing.Font ("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ));
      this.lbl_card_6.Location = new System.Drawing.Point (29, 29);
      this.lbl_card_6.Name = "lbl_card_6";
      this.lbl_card_6.Size = new System.Drawing.Size (30, 30);
      this.lbl_card_6.TabIndex = 98;
      this.lbl_card_6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lbl_card_5
      // 
      this.lbl_card_5.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
      this.lbl_card_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.lbl_card_5.Font = new System.Drawing.Font ("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ));
      this.lbl_card_5.Location = new System.Drawing.Point (29, 0);
      this.lbl_card_5.Name = "lbl_card_5";
      this.lbl_card_5.Size = new System.Drawing.Size (30, 30);
      this.lbl_card_5.TabIndex = 97;
      this.lbl_card_5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lbl_card_4
      // 
      this.lbl_card_4.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
      this.lbl_card_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.lbl_card_4.Font = new System.Drawing.Font ("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ));
      this.lbl_card_4.Location = new System.Drawing.Point (0, 116);
      this.lbl_card_4.Name = "lbl_card_4";
      this.lbl_card_4.Size = new System.Drawing.Size (30, 30);
      this.lbl_card_4.TabIndex = 96;
      this.lbl_card_4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lbl_card_3
      // 
      this.lbl_card_3.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
      this.lbl_card_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.lbl_card_3.Font = new System.Drawing.Font ("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ));
      this.lbl_card_3.Location = new System.Drawing.Point (0, 87);
      this.lbl_card_3.Name = "lbl_card_3";
      this.lbl_card_3.Size = new System.Drawing.Size (30, 30);
      this.lbl_card_3.TabIndex = 95;
      this.lbl_card_3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lbl_card_2
      // 
      this.lbl_card_2.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
      this.lbl_card_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.lbl_card_2.Font = new System.Drawing.Font ("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ));
      this.lbl_card_2.Location = new System.Drawing.Point (0, 58);
      this.lbl_card_2.Name = "lbl_card_2";
      this.lbl_card_2.Size = new System.Drawing.Size (30, 30);
      this.lbl_card_2.TabIndex = 94;
      this.lbl_card_2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lbl_card_1
      // 
      this.lbl_card_1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
      this.lbl_card_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.lbl_card_1.Font = new System.Drawing.Font ("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ));
      this.lbl_card_1.Location = new System.Drawing.Point (0, 29);
      this.lbl_card_1.Name = "lbl_card_1";
      this.lbl_card_1.Size = new System.Drawing.Size (30, 30);
      this.lbl_card_1.TabIndex = 93;
      this.lbl_card_1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lbl_card_0
      // 
      this.lbl_card_0.BackColor = System.Drawing.Color.White;
      this.lbl_card_0.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.lbl_card_0.Font = new System.Drawing.Font ("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ));
      this.lbl_card_0.Location = new System.Drawing.Point (0, 0);
      this.lbl_card_0.Name = "lbl_card_0";
      this.lbl_card_0.Size = new System.Drawing.Size (30, 30);
      this.lbl_card_0.TabIndex = 92;
      this.lbl_card_0.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // txt_Jackpot_name_2
      // 
      this.txt_Jackpot_name_2.Location = new System.Drawing.Point (537, 58);
      this.txt_Jackpot_name_2.Name = "txt_Jackpot_name_2";
      this.txt_Jackpot_name_2.Size = new System.Drawing.Size (62, 20);
      this.txt_Jackpot_name_2.TabIndex = 103;
      // 
      // txt_Jackpot_name_1
      // 
      this.txt_Jackpot_name_1.Location = new System.Drawing.Point (537, 37);
      this.txt_Jackpot_name_1.Name = "txt_Jackpot_name_1";
      this.txt_Jackpot_name_1.Size = new System.Drawing.Size (62, 20);
      this.txt_Jackpot_name_1.TabIndex = 102;
      // 
      // txt_Jackpot_name_0
      // 
      this.txt_Jackpot_name_0.Location = new System.Drawing.Point (537, 16);
      this.txt_Jackpot_name_0.Name = "txt_Jackpot_name_0";
      this.txt_Jackpot_name_0.Size = new System.Drawing.Size (62, 20);
      this.txt_Jackpot_name_0.TabIndex = 101;
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point (547, 88);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size (33, 13);
      this.label3.TabIndex = 104;
      this.label3.Text = "Prize:";
      // 
      // label11
      // 
      this.label11.AutoSize = true;
      this.label11.Location = new System.Drawing.Point (525, 102);
      this.label11.Name = "label11";
      this.label11.Size = new System.Drawing.Size (55, 13);
      this.label11.TabIndex = 105;
      this.label11.Text = "CardPrize:";
      // 
      // lbl_prize
      // 
      this.lbl_prize.ForeColor = System.Drawing.Color.Red;
      this.lbl_prize.Location = new System.Drawing.Point (577, 88);
      this.lbl_prize.Name = "lbl_prize";
      this.lbl_prize.Size = new System.Drawing.Size (80, 13);
      this.lbl_prize.TabIndex = 106;
      // 
      // lbl_card_prize
      // 
      this.lbl_card_prize.ForeColor = System.Drawing.Color.Red;
      this.lbl_card_prize.Location = new System.Drawing.Point (577, 102);
      this.lbl_card_prize.Name = "lbl_card_prize";
      this.lbl_card_prize.Size = new System.Drawing.Size (80, 13);
      this.lbl_card_prize.TabIndex = 107;
      // 
      // lbl_req_trx
      // 
      this.lbl_req_trx.Font = new System.Drawing.Font ("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ));
      this.lbl_req_trx.Location = new System.Drawing.Point (846, 147);
      this.lbl_req_trx.Name = "lbl_req_trx";
      this.lbl_req_trx.Size = new System.Drawing.Size (278, 24);
      this.lbl_req_trx.TabIndex = 108;
      this.lbl_req_trx.Text = "Trx/s";
      this.lbl_req_trx.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // txt_statistics
      // 
      this.txt_statistics.Dock = System.Windows.Forms.DockStyle.Fill;
      this.txt_statistics.Font = new System.Drawing.Font ("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ));
      this.txt_statistics.Location = new System.Drawing.Point (0, 0);
      this.txt_statistics.Multiline = true;
      this.txt_statistics.Name = "txt_statistics";
      this.txt_statistics.ScrollBars = System.Windows.Forms.ScrollBars.Both;
      this.txt_statistics.Size = new System.Drawing.Size (1102, 350);
      this.txt_statistics.TabIndex = 0;
      this.txt_statistics.WordWrap = false;
      // 
      // btn_clr_statistics
      // 
      this.btn_clr_statistics.Anchor = ( (System.Windows.Forms.AnchorStyles) ( ( System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right ) ) );
      this.btn_clr_statistics.Location = new System.Drawing.Point (1015, 3);
      this.btn_clr_statistics.Name = "btn_clr_statistics";
      this.btn_clr_statistics.Size = new System.Drawing.Size (84, 23);
      this.btn_clr_statistics.TabIndex = 110;
      this.btn_clr_statistics.Text = "Clear Statistics";
      this.btn_clr_statistics.UseVisualStyleBackColor = true;
      this.btn_clr_statistics.Click += new System.EventHandler (this.button7_Click);
      // 
      // chk_enroll_terminal
      // 
      this.chk_enroll_terminal.AutoSize = true;
      this.chk_enroll_terminal.Enabled = false;
      this.chk_enroll_terminal.Location = new System.Drawing.Point (461, 232);
      this.chk_enroll_terminal.Name = "chk_enroll_terminal";
      this.chk_enroll_terminal.Size = new System.Drawing.Size (15, 14);
      this.chk_enroll_terminal.TabIndex = 111;
      this.chk_enroll_terminal.UseVisualStyleBackColor = true;
      // 
      // chk_get_params
      // 
      this.chk_get_params.AutoSize = true;
      this.chk_get_params.Checked = true;
      this.chk_get_params.CheckState = System.Windows.Forms.CheckState.Checked;
      this.chk_get_params.Location = new System.Drawing.Point (461, 260);
      this.chk_get_params.Name = "chk_get_params";
      this.chk_get_params.Size = new System.Drawing.Size (15, 14);
      this.chk_get_params.TabIndex = 112;
      this.chk_get_params.UseVisualStyleBackColor = true;
      // 
      // chk_play
      // 
      this.chk_play.AutoSize = true;
      this.chk_play.Checked = true;
      this.chk_play.CheckState = System.Windows.Forms.CheckState.Checked;
      this.chk_play.Location = new System.Drawing.Point (461, 316);
      this.chk_play.Name = "chk_play";
      this.chk_play.Size = new System.Drawing.Size (15, 14);
      this.chk_play.TabIndex = 114;
      this.chk_play.UseVisualStyleBackColor = true;
      // 
      // chk_get_cards
      // 
      this.chk_get_cards.AutoSize = true;
      this.chk_get_cards.Checked = true;
      this.chk_get_cards.CheckState = System.Windows.Forms.CheckState.Checked;
      this.chk_get_cards.Location = new System.Drawing.Point (461, 288);
      this.chk_get_cards.Name = "chk_get_cards";
      this.chk_get_cards.Size = new System.Drawing.Size (15, 14);
      this.chk_get_cards.TabIndex = 113;
      this.chk_get_cards.UseVisualStyleBackColor = true;
      // 
      // chk_cancel_play
      // 
      this.chk_cancel_play.AutoSize = true;
      this.chk_cancel_play.Enabled = false;
      this.chk_cancel_play.Location = new System.Drawing.Point (704, 344);
      this.chk_cancel_play.Name = "chk_cancel_play";
      this.chk_cancel_play.Size = new System.Drawing.Size (15, 14);
      this.chk_cancel_play.TabIndex = 118;
      this.chk_cancel_play.UseVisualStyleBackColor = true;
      // 
      // chk_get_patterns
      // 
      this.chk_get_patterns.AutoSize = true;
      this.chk_get_patterns.Checked = true;
      this.chk_get_patterns.CheckState = System.Windows.Forms.CheckState.Checked;
      this.chk_get_patterns.Location = new System.Drawing.Point (704, 316);
      this.chk_get_patterns.Name = "chk_get_patterns";
      this.chk_get_patterns.Size = new System.Drawing.Size (15, 14);
      this.chk_get_patterns.TabIndex = 117;
      this.chk_get_patterns.UseVisualStyleBackColor = true;
      // 
      // chk_get_jackpot
      // 
      this.chk_get_jackpot.AutoSize = true;
      this.chk_get_jackpot.Checked = true;
      this.chk_get_jackpot.CheckState = System.Windows.Forms.CheckState.Checked;
      this.chk_get_jackpot.Location = new System.Drawing.Point (704, 260);
      this.chk_get_jackpot.Name = "chk_get_jackpot";
      this.chk_get_jackpot.Size = new System.Drawing.Size (15, 14);
      this.chk_get_jackpot.TabIndex = 116;
      this.chk_get_jackpot.UseVisualStyleBackColor = true;
      // 
      // chk_unenroll
      // 
      this.chk_unenroll.AutoSize = true;
      this.chk_unenroll.Enabled = false;
      this.chk_unenroll.Location = new System.Drawing.Point (704, 232);
      this.chk_unenroll.Name = "chk_unenroll";
      this.chk_unenroll.Size = new System.Drawing.Size (15, 14);
      this.chk_unenroll.TabIndex = 115;
      this.chk_unenroll.UseVisualStyleBackColor = true;
      // 
      // label12
      // 
      this.label12.AutoSize = true;
      this.label12.Location = new System.Drawing.Point (444, 209);
      this.label12.Name = "label12";
      this.label12.Size = new System.Drawing.Size (47, 13);
      this.label12.TabIndex = 119;
      this.label12.Text = "Simulate";
      // 
      // label13
      // 
      this.label13.AutoSize = true;
      this.label13.Location = new System.Drawing.Point (687, 209);
      this.label13.Name = "label13";
      this.label13.Size = new System.Drawing.Size (47, 13);
      this.label13.TabIndex = 120;
      this.label13.Text = "Simulate";
      // 
      // tabControl1
      // 
      this.tabControl1.Anchor = ( (System.Windows.Forms.AnchorStyles) ( ( ( ( System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom )
                  | System.Windows.Forms.AnchorStyles.Left )
                  | System.Windows.Forms.AnchorStyles.Right ) ) );
      this.tabControl1.Controls.Add (this.tabPage2);
      this.tabControl1.Controls.Add (this.tabPage1);
      this.tabControl1.Location = new System.Drawing.Point (5, 363);
      this.tabControl1.Name = "tabControl1";
      this.tabControl1.SelectedIndex = 0;
      this.tabControl1.Size = new System.Drawing.Size (1116, 443);
      this.tabControl1.TabIndex = 121;
      // 
      // tabPage2
      // 
      this.tabPage2.Controls.Add (this.splitContainer2);
      this.tabPage2.Location = new System.Drawing.Point (4, 22);
      this.tabPage2.Name = "tabPage2";
      this.tabPage2.Padding = new System.Windows.Forms.Padding (3);
      this.tabPage2.Size = new System.Drawing.Size (1108, 417);
      this.tabPage2.TabIndex = 1;
      this.tabPage2.Text = "Statistics";
      this.tabPage2.UseVisualStyleBackColor = true;
      // 
      // splitContainer2
      // 
      this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer2.Location = new System.Drawing.Point (3, 3);
      this.splitContainer2.Name = "splitContainer2";
      this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
      // 
      // splitContainer2.Panel1
      // 
      this.splitContainer2.Panel1.Controls.Add (this.txt_statistics);
      // 
      // splitContainer2.Panel2
      // 
      this.splitContainer2.Panel2.Controls.Add (this.btn_clr_statistics);
      this.splitContainer2.Size = new System.Drawing.Size (1102, 411);
      this.splitContainer2.SplitterDistance = 350;
      this.splitContainer2.TabIndex = 0;
      // 
      // tabPage1
      // 
      this.tabPage1.Controls.Add (this.splitContainer1);
      this.tabPage1.Location = new System.Drawing.Point (4, 22);
      this.tabPage1.Name = "tabPage1";
      this.tabPage1.Padding = new System.Windows.Forms.Padding (3);
      this.tabPage1.Size = new System.Drawing.Size (1108, 417);
      this.tabPage1.TabIndex = 0;
      this.tabPage1.Text = "Messages";
      this.tabPage1.UseVisualStyleBackColor = true;
      // 
      // txt_play_intv
      // 
      this.txt_play_intv.Location = new System.Drawing.Point (500, 313);
      this.txt_play_intv.Name = "txt_play_intv";
      this.txt_play_intv.Size = new System.Drawing.Size (61, 20);
      this.txt_play_intv.TabIndex = 125;
      this.txt_play_intv.Text = "1000";
      this.txt_play_intv.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      // 
      // txt_get_cards_intv
      // 
      this.txt_get_cards_intv.Location = new System.Drawing.Point (500, 285);
      this.txt_get_cards_intv.Name = "txt_get_cards_intv";
      this.txt_get_cards_intv.Size = new System.Drawing.Size (61, 20);
      this.txt_get_cards_intv.TabIndex = 124;
      this.txt_get_cards_intv.Text = "60000";
      this.txt_get_cards_intv.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      // 
      // txt_get_params_intvl
      // 
      this.txt_get_params_intvl.Location = new System.Drawing.Point (500, 257);
      this.txt_get_params_intvl.Name = "txt_get_params_intvl";
      this.txt_get_params_intvl.Size = new System.Drawing.Size (61, 20);
      this.txt_get_params_intvl.TabIndex = 123;
      this.txt_get_params_intvl.Text = "10000";
      this.txt_get_params_intvl.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      // 
      // label14
      // 
      this.label14.AutoSize = true;
      this.label14.Location = new System.Drawing.Point (497, 209);
      this.label14.Name = "label14";
      this.label14.Size = new System.Drawing.Size (64, 13);
      this.label14.TabIndex = 130;
      this.label14.Text = "Interval (ms)";
      // 
      // textBox1
      // 
      this.textBox1.Enabled = false;
      this.textBox1.Location = new System.Drawing.Point (500, 229);
      this.textBox1.Name = "textBox1";
      this.textBox1.Size = new System.Drawing.Size (61, 20);
      this.textBox1.TabIndex = 122;
      // 
      // label15
      // 
      this.label15.AutoSize = true;
      this.label15.Location = new System.Drawing.Point (740, 209);
      this.label15.Name = "label15";
      this.label15.Size = new System.Drawing.Size (64, 13);
      this.label15.TabIndex = 135;
      this.label15.Text = "Interval (ms)";
      // 
      // textBox5
      // 
      this.textBox5.Enabled = false;
      this.textBox5.Location = new System.Drawing.Point (743, 341);
      this.textBox5.Name = "textBox5";
      this.textBox5.Size = new System.Drawing.Size (61, 20);
      this.textBox5.TabIndex = 134;
      // 
      // txt_patterns_intv
      // 
      this.txt_patterns_intv.Location = new System.Drawing.Point (743, 313);
      this.txt_patterns_intv.Name = "txt_patterns_intv";
      this.txt_patterns_intv.Size = new System.Drawing.Size (61, 20);
      this.txt_patterns_intv.TabIndex = 133;
      this.txt_patterns_intv.Text = "60000";
      this.txt_patterns_intv.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      // 
      // txt_jackpot_intv
      // 
      this.txt_jackpot_intv.Location = new System.Drawing.Point (743, 257);
      this.txt_jackpot_intv.Name = "txt_jackpot_intv";
      this.txt_jackpot_intv.Size = new System.Drawing.Size (61, 20);
      this.txt_jackpot_intv.TabIndex = 132;
      this.txt_jackpot_intv.Text = "60000";
      this.txt_jackpot_intv.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      // 
      // textBox8
      // 
      this.textBox8.Enabled = false;
      this.textBox8.Location = new System.Drawing.Point (743, 229);
      this.textBox8.Name = "textBox8";
      this.textBox8.Size = new System.Drawing.Size (61, 20);
      this.textBox8.TabIndex = 131;
      // 
      // btn_site_jackpot
      // 
      this.btn_site_jackpot.Location = new System.Drawing.Point (603, 282);
      this.btn_site_jackpot.Name = "btn_site_jackpot";
      this.btn_site_jackpot.Size = new System.Drawing.Size (95, 23);
      this.btn_site_jackpot.TabIndex = 136;
      this.btn_site_jackpot.Text = "Site Jackpot";
      this.btn_site_jackpot.UseVisualStyleBackColor = true;
      this.btn_site_jackpot.Click += new System.EventHandler (this.btn_site_jackpot_Click);
      // 
      // chk_site_jackpot
      // 
      this.chk_site_jackpot.AutoSize = true;
      this.chk_site_jackpot.Location = new System.Drawing.Point (704, 288);
      this.chk_site_jackpot.Name = "chk_site_jackpot";
      this.chk_site_jackpot.Size = new System.Drawing.Size (15, 14);
      this.chk_site_jackpot.TabIndex = 137;
      this.chk_site_jackpot.UseVisualStyleBackColor = true;
      // 
      // txt_site_jackpot_intv
      // 
      this.txt_site_jackpot_intv.Location = new System.Drawing.Point (743, 285);
      this.txt_site_jackpot_intv.Name = "txt_site_jackpot_intv";
      this.txt_site_jackpot_intv.Size = new System.Drawing.Size (61, 20);
      this.txt_site_jackpot_intv.TabIndex = 138;
      this.txt_site_jackpot_intv.Text = "60000";
      this.txt_site_jackpot_intv.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      // 
      // Form1
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF (6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size (1111, 816);
      this.Controls.Add (this.txt_site_jackpot_intv);
      this.Controls.Add (this.chk_site_jackpot);
      this.Controls.Add (this.btn_site_jackpot);
      this.Controls.Add (this.label15);
      this.Controls.Add (this.textBox5);
      this.Controls.Add (this.txt_patterns_intv);
      this.Controls.Add (this.txt_jackpot_intv);
      this.Controls.Add (this.textBox8);
      this.Controls.Add (this.label14);
      this.Controls.Add (this.txt_play_intv);
      this.Controls.Add (this.txt_get_cards_intv);
      this.Controls.Add (this.txt_get_params_intvl);
      this.Controls.Add (this.textBox1);
      this.Controls.Add (this.tabControl1);
      this.Controls.Add (this.label13);
      this.Controls.Add (this.label12);
      this.Controls.Add (this.chk_cancel_play);
      this.Controls.Add (this.chk_get_patterns);
      this.Controls.Add (this.chk_get_jackpot);
      this.Controls.Add (this.chk_unenroll);
      this.Controls.Add (this.chk_play);
      this.Controls.Add (this.chk_get_cards);
      this.Controls.Add (this.chk_get_params);
      this.Controls.Add (this.chk_enroll_terminal);
      this.Controls.Add (this.pnl_cards);
      this.Controls.Add (this.lbl_req_trx);
      this.Controls.Add (this.lbl_card_prize);
      this.Controls.Add (this.lbl_prize);
      this.Controls.Add (this.label11);
      this.Controls.Add (this.label3);
      this.Controls.Add (this.txt_Jackpot_name_2);
      this.Controls.Add (this.txt_Jackpot_name_1);
      this.Controls.Add (this.txt_Jackpot_name_0);
      this.Controls.Add (this.lbl_play_delayed);
      this.Controls.Add (this.txt_draw_numbers);
      this.Controls.Add (this.label8);
      this.Controls.Add (this.txt_draw_id);
      this.Controls.Add (this.label6);
      this.Controls.Add (this.txt_jackpot_2);
      this.Controls.Add (this.txt_jackpot_1);
      this.Controls.Add (this.txt_jackpot_0);
      this.Controls.Add (this.label4);
      this.Controls.Add (this.btn_get_params);
      this.Controls.Add (this.btn_get_patterns);
      this.Controls.Add (this.btn_get_jackpot_info);
      this.Controls.Add (this.btn_get_cards);
      this.Controls.Add (this.btn_cancel_play);
      this.Controls.Add (this.btn_request_service);
      this.Controls.Add (this.chk_time_plays);
      this.Controls.Add (this.txt_time_plays);
      this.Controls.Add (this.label10);
      this.Controls.Add (this.groupBox1);
      this.Controls.Add (this.lbl_terminals_conn);
      this.Controls.Add (this.lbl_server_connected);
      this.Controls.Add (this.label2);
      this.Controls.Add (this.cmb_prefix);
      this.Controls.Add (this.chk_sim_plays);
      this.Controls.Add (this.lbl_trx);
      this.Controls.Add (this.cmb_service_address);
      this.Controls.Add (this.btn_start_rnd_simulation);
      this.Controls.Add (this.btn_keep_alive);
      this.Controls.Add (this.btn_clear_terminal);
      this.Controls.Add (this.btn_clear_server);
      this.Controls.Add (this.lbl_simulation);
      this.Controls.Add (this.lbl_keep_alive);
      this.Controls.Add (this.lbl_num_terminals);
      this.Controls.Add (this.txt_num_terminals);
      this.Controls.Add (this.btn_send_terminal);
      this.Controls.Add (this.btn_send_server);
      this.Controls.Add (this.txt_pending_plays);
      this.Controls.Add (this.label9);
      this.Controls.Add (this.btn_unenroll_terminal);
      this.Controls.Add (this.btn_report_play);
      this.Controls.Add (this.txt_trm_seq_id);
      this.Controls.Add (this.label7);
      this.Controls.Add (this.btn_enroll_terminal);
      this.Controls.Add (this.txt_trm_session_id);
      this.Controls.Add (this.label5);
      this.Controls.Add (this.lbl_terminal);
      this.Controls.Add (this.label1);
      this.Controls.Add (this.btn_connect);
      this.Name = "Form1";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "WSI.WC2.ClientSimulator ";
      this.splitContainer1.Panel2.ResumeLayout (false);
      this.splitContainer1.ResumeLayout (false);
      this.splitContainer3.Panel1.ResumeLayout (false);
      this.splitContainer3.Panel1.PerformLayout ();
      this.splitContainer3.Panel2.ResumeLayout (false);
      this.splitContainer3.Panel2.PerformLayout ();
      this.splitContainer3.ResumeLayout (false);
      this.groupBox1.ResumeLayout (false);
      this.pnl_cards.ResumeLayout (false);
      this.tabControl1.ResumeLayout (false);
      this.tabPage2.ResumeLayout (false);
      this.splitContainer2.Panel1.ResumeLayout (false);
      this.splitContainer2.Panel1.PerformLayout ();
      this.splitContainer2.Panel2.ResumeLayout (false);
      this.splitContainer2.ResumeLayout (false);
      this.tabPage1.ResumeLayout (false);
      this.ResumeLayout (false);
      this.PerformLayout ();

    }



    #endregion

    private System.Windows.Forms.Button btn_connect;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label lbl_terminal;
    private System.Windows.Forms.SplitContainer splitContainer1;
    private System.Windows.Forms.SplitContainer splitContainer3;
    private System.Windows.Forms.TextBox txt_snd_terminal;
    private System.Windows.Forms.TextBox txt_rcv_terminal;
    private System.Windows.Forms.TextBox txt_trm_session_id;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.Button btn_enroll_terminal;
    private System.Windows.Forms.TextBox txt_trm_seq_id;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.Button btn_report_play;
    private System.Windows.Forms.Button btn_unenroll_terminal;
      private System.Windows.Forms.Timer tmr_server_keep_alive;
      private System.Windows.Forms.Timer trm_keep_alive_terminal;
    private System.Windows.Forms.Button btn_keep_alive;
    private System.Windows.Forms.Button btn_start_rnd_simulation;
    private System.Windows.Forms.Label label9;
    private System.Windows.Forms.TextBox txt_pending_plays;
    private System.Windows.Forms.Button btn_send_server;
    private System.Windows.Forms.Button btn_send_terminal;
    private System.Windows.Forms.TextBox txt_num_terminals;
    private System.Windows.Forms.Label lbl_num_terminals;
    private System.Windows.Forms.Label lbl_keep_alive;
    private System.Windows.Forms.Label lbl_simulation;
    private System.Windows.Forms.Button btn_clear_server;
    private System.Windows.Forms.Button btn_clear_terminal;
    private System.Windows.Forms.ComboBox cmb_service_address;
    private System.Windows.Forms.Label lbl_trx;
    private System.Windows.Forms.Timer tmr_refresh;
    private System.Windows.Forms.CheckBox chk_sim_plays;
    private System.Windows.Forms.ComboBox cmb_prefix;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label lbl_server_connected;
    private System.Windows.Forms.Label lbl_terminals_conn;
    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.Label lbl_terminals_start_session;
    private System.Windows.Forms.Label lbl_terminals_enrolled;
    private System.Windows.Forms.CheckBox chk_time_plays;
    private System.Windows.Forms.TextBox txt_time_plays;
    private System.Windows.Forms.Label label10;
    private System.Windows.Forms.Button btn_request_service;
    private System.Windows.Forms.Button btn_cancel_play;
    private System.Windows.Forms.Button btn_get_cards;
    private System.Windows.Forms.Button btn_get_jackpot_info;
    private System.Windows.Forms.Button btn_get_patterns;
    private System.Windows.Forms.Button btn_get_params;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.TextBox txt_jackpot_0;
    private System.Windows.Forms.TextBox txt_jackpot_1;
    private System.Windows.Forms.TextBox txt_jackpot_2;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.TextBox txt_draw_id;
    private System.Windows.Forms.TextBox txt_draw_numbers;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.Label lbl_play_delayed;
    private System.Windows.Forms.Panel pnl_cards;
    private System.Windows.Forms.Label lbl_card_24;
    private System.Windows.Forms.Label lbl_card_23;
    private System.Windows.Forms.Label lbl_card_22;
    private System.Windows.Forms.Label lbl_card_21;
    private System.Windows.Forms.Label lbl_card_20;
    private System.Windows.Forms.Label lbl_card_19;
    private System.Windows.Forms.Label lbl_card_18;
    private System.Windows.Forms.Label lbl_card_17;
    private System.Windows.Forms.Label lbl_card_16;
    private System.Windows.Forms.Label lbl_card_15;
    private System.Windows.Forms.Label lbl_card_14;
    private System.Windows.Forms.Label lbl_card_13;
    private System.Windows.Forms.Label lbl_card_12;
    private System.Windows.Forms.Label lbl_card_11;
    private System.Windows.Forms.Label lbl_card_10;
    private System.Windows.Forms.Label lbl_card_9;
    private System.Windows.Forms.Label lbl_card_8;
    private System.Windows.Forms.Label lbl_card_7;
    private System.Windows.Forms.Label lbl_card_6;
    private System.Windows.Forms.Label lbl_card_5;
    private System.Windows.Forms.Label lbl_card_4;
    private System.Windows.Forms.Label lbl_card_3;
    private System.Windows.Forms.Label lbl_card_2;
    private System.Windows.Forms.Label lbl_card_1;
    private System.Windows.Forms.Label lbl_card_0;
    private System.Windows.Forms.TextBox txt_Jackpot_name_2;
    private System.Windows.Forms.TextBox txt_Jackpot_name_1;
    private System.Windows.Forms.TextBox txt_Jackpot_name_0;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label label11;
    private System.Windows.Forms.Label lbl_prize;
    private System.Windows.Forms.Label lbl_card_prize;
    private System.Windows.Forms.Label lbl_req_trx;
    private System.Windows.Forms.Label lbl_jackpot_name;
    private System.Windows.Forms.Label lbl_jackpot_amount;
    private System.Windows.Forms.TextBox txt_statistics;
    private System.Windows.Forms.Button btn_clr_statistics;
    private System.Windows.Forms.CheckBox chk_enroll_terminal;
    private System.Windows.Forms.CheckBox chk_get_params;
    private System.Windows.Forms.CheckBox chk_play;
    private System.Windows.Forms.CheckBox chk_get_cards;
    private System.Windows.Forms.CheckBox chk_cancel_play;
    private System.Windows.Forms.CheckBox chk_get_patterns;
    private System.Windows.Forms.CheckBox chk_get_jackpot;
    private System.Windows.Forms.CheckBox chk_unenroll;
    private System.Windows.Forms.Label label12;
    private System.Windows.Forms.Label label13;
    private System.Windows.Forms.TabControl tabControl1;
    private System.Windows.Forms.TabPage tabPage1;
    private System.Windows.Forms.TabPage tabPage2;
    private System.Windows.Forms.SplitContainer splitContainer2;
    private System.Windows.Forms.TextBox txt_play_intv;
    private System.Windows.Forms.TextBox txt_get_cards_intv;
    private System.Windows.Forms.TextBox txt_get_params_intvl;
    private System.Windows.Forms.Label label14;
    private System.Windows.Forms.TextBox textBox1;
    private System.Windows.Forms.Label label15;
    private System.Windows.Forms.TextBox textBox5;
    private System.Windows.Forms.TextBox txt_patterns_intv;
    private System.Windows.Forms.TextBox txt_jackpot_intv;
    private System.Windows.Forms.TextBox textBox8;
    private System.Windows.Forms.Button btn_site_jackpot;
    private System.Windows.Forms.CheckBox chk_site_jackpot;
    private System.Windows.Forms.TextBox txt_site_jackpot_intv;
  }
}

