using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WSI.WC2;
using WSI.Common;
using System.Net;
using System.Threading;

namespace WSI.WC2_ClientSimulator
{
  public partial class Form1:Form
  {
    ArrayList terminals = new ArrayList ();

    Boolean sim_enabled = false ;
    DateTime sim_started;
    DateTime sim_end;
    Boolean sim_no_trx = false;
    DateTime sim_no_trx_time;
    UInt32 sim_req_trx;
    UInt32 sim_req_trx_1;
    UInt32 sim_trx;
    UInt32 sim_trx1;
    UInt32 sim_trx2;
    UInt32 jackpot_idx = 0;
    public String[] jackpot_amount = new String[6];
    public String[] jackpot_name = new String[6];

    Boolean jackpot_awarded;
    UInt32 jackpot_ticks;

    Hashtable m_statistics = new Hashtable();

    //Intervals
    public Int32 m_interval_play;
    public Int32 m_interval_get_params;
    public Int32 m_interval_get_cards;
    public Int32 m_interval_get_jackpot;
    public Int32 m_interval_site_jackpot;
    public Int32 m_interval_get_patterns;

    delegate void SetTextCallback (SimulatedTerminal Terminal, String Xml);

    #region Simulation Thread

    //------------------------------------------------------------------------------
    // PURPOSE : Simulation thread
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void SimulationThread()
    {
      long _interval;
      int _wait_timeout;
      Random _rnd;

      _rnd = new Random ();
      _wait_timeout = 100;

      sim_started = DateTime.Now;
      sim_end = DateTime.Now;

      while (true)
      {
        System.Threading.Thread.Sleep(_wait_timeout);

        if (sim_enabled)
        {
          sim_end = DateTime.Now;
          _wait_timeout = _rnd.Next(10);

          if (terminals != null)
          {
            foreach (SimulatedTerminal st in terminals)
            {
              _interval = Misc.GetElapsedTicks(st.last_play_tick);

              if (st.time_between_plays > _interval)
              {
                continue;
              }

              _interval = Misc.GetElapsedTicks(st.pending_reply_tick);
              if (_interval > 60000)
              {
                // MBF 07-JUL-2010 mark timeout
                lock (m_statistics)
                {
                  Monitor.Enter(m_statistics);
                  Boolean _add = true;
                  Statistics _stats = null;

                  _stats = new Statistics();

                  if (m_statistics.ContainsKey(st.message_send))
                  {
                    _stats = (Statistics)m_statistics[st.message_send];
                    _add = false;
                  }

                  // Increase timeouts counter
                  _stats.timeouts++;
                  // Save petition
                  _stats.name = st.message_send.ToString();

                  if (_add)
                  {
                    m_statistics.Add(st.message_send, _stats);
                  }

                  Monitor.Exit(m_statistics);
                }

                st.pending_reply = false;
              }

              SimStep(st);
            }
          }
        }
        else
        {
          _wait_timeout = 100;
        }

      } // while

    } // SimulationThread


    #endregion // Simulation Thread


    void UpdateStats ()
    {
      try
      {
        //MBF
        String _str;
        double _trx_sec;
        TimeSpan _interval;

        _str = "";

        _str = _str + " WC2 Simulation \r\n";
        _str = _str + "   Start: " + sim_started.ToShortDateString() + " " + sim_started.ToLongTimeString() + " \r\n";
        _str = _str + "   End:   " + sim_end.ToShortDateString() + " " + sim_end.ToLongTimeString() + " \r\n";
        _str = _str + " \r\n";

        _str = _str + " Parameters " + " \r\n";
        _str = _str + " Terminals: " + terminals.Count.ToString("0") + " \r\n";
        _str = _str + " Plays          " + chk_play.Checked.ToString().PadRight(5) + " " + txt_play_intv.Text.PadLeft(8) + " ms \r\n";
        _str = _str + " Get Parameters " + chk_get_params.Checked.ToString().PadRight(5) + " " + txt_get_params_intvl.Text.PadLeft(8) + " ms \r\n";
        _str = _str + " Get Cards      " + chk_get_cards.Checked.ToString().PadRight(5) + " " + txt_get_cards_intv.Text.PadLeft(8) + " ms \r\n";
        _str = _str + " Get Patterns   " + chk_get_patterns.Checked.ToString().PadRight(5) + " " + txt_patterns_intv.Text.PadLeft(8) + " ms \r\n";
        _str = _str + " Get Jackpot    " + chk_get_jackpot.Checked.ToString().PadRight(5) + " " + txt_jackpot_intv.Text.PadLeft(8) + " ms \r\n";
        _str = _str + " Site Jackpot   " + chk_site_jackpot.Checked.ToString().PadRight (5) + " " + txt_site_jackpot_intv.Text.PadLeft (8) + " ms \r\n";
        _str = _str + " \r\n";

        _str = _str + " Results " + " \r\n";
        _str = _str + " Message ".PadRight(30) 
                    + " Average (ms) ".PadRight(15) 
                    + " Max (ms) ".PadRight(15) 
                    + " Sent ".PadRight(15)
                    + " Received ".PadRight(15)
                    + " Timeout ".PadRight(15)
                    + " Trx/s ".PadRight(15)
                    + " \r\n";

        foreach (Statistics _stat in m_statistics.Values)
        {
          _interval = sim_end.Subtract(sim_started);
          _trx_sec = (double)_stat.sent / _interval.TotalSeconds;

          _str = _str + " " + _stat.name.PadRight(30) 
                      + _stat.time_av.ToString("0").PadRight(15) 
                      + _stat.time_max.ToString("0").PadRight(15) 
                      + _stat.sent.ToString().PadRight(15) 
                      + _stat.count.ToString().PadRight(15)
                      + _stat.timeouts.ToString().PadRight(15)
                      + _trx_sec.ToString("0.00").PadRight(15)
                      + " \r\n";
        }
        txt_statistics.Text = _str;

        // Jackpot check
        if (!this.jackpot_awarded)
        {
          jackpot_idx = 0;
          foreach (SimulatedTerminal t in terminals)
          {
            if (t.jackpot_awarded)
            {
              this.jackpot_amount[jackpot_idx] = t.jackpot_amount;
              this.jackpot_name[jackpot_idx] = t.jackpot_name;
              jackpot_idx++;
              t.jackpot_awarded = false;
            }
          }

          if (jackpot_idx > 0)
          {
            this.jackpot_awarded = true;
            this.jackpot_ticks = 0;

            lbl_jackpot_name.Visible = true;
            lbl_jackpot_amount.Visible = true;
            lbl_jackpot_amount.Text = this.jackpot_amount[jackpot_idx - 1];
            lbl_jackpot_name.Text = this.jackpot_name[jackpot_idx - 1];
          }
        }
        else
        {
          this.jackpot_ticks++;
          if (this.jackpot_ticks > 5)
          {
            this.jackpot_idx--;
            if (jackpot_idx > 0)
            {
              this.jackpot_awarded = true;
              this.jackpot_ticks = 0;

              lbl_jackpot_name.Visible = true;
              lbl_jackpot_amount.Visible = true;
              lbl_jackpot_amount.Text = this.jackpot_amount[jackpot_idx - 1];
              lbl_jackpot_name.Text = this.jackpot_name[jackpot_idx - 1];
            }
            else
            {
              this.jackpot_awarded = false;
              lbl_jackpot_name.Visible = false;
              lbl_jackpot_amount.Visible = false;
            }
          }
        }

        if (sim_enabled)
        {
          double trx;
          DateTime x;
          TimeSpan e;
          TimeSpan e_no_trx;
          x = DateTime.Now;
          e = x - sim_started;

          if (((sim_trx - sim_trx1) < 1) && e.TotalSeconds > 10)
          {
            if (!sim_no_trx)
            {
              sim_no_trx = true;
              sim_no_trx_time = DateTime.Now;
              sim_trx2 = sim_trx1;
            }
          }
          else
          {
            sim_no_trx = false;
          }

          e_no_trx = new TimeSpan(0, 0, 0);
          if (sim_no_trx)
          {
            e_no_trx = x - sim_no_trx_time;
          }

          if (e_no_trx.TotalSeconds > 10)
          {
            if (chk_time_plays.Checked == false)
            {
              btn_start_rnd_simulation_Click(null, null);
            }

            return;
          }

          if (e.TotalSeconds > 0)
          {
            trx = (double)sim_req_trx;
            trx = trx / e.TotalSeconds;

            lbl_req_trx.Text = "Trx Request =>  Avg: " + trx.ToString(".00") + " ";

            trx = (double)(sim_req_trx - sim_req_trx_1);
            trx = trx * 1000 / tmr_refresh.Interval;

            lbl_req_trx.Text += "Now: " + trx.ToString(".00") + " ";

            trx = (double)sim_trx;
            trx = trx / e.TotalSeconds;

            lbl_trx.Text = "Trx Reply => Avg: " + trx.ToString(".00") + " ";

            trx = (double)(sim_trx - sim_trx1);
            trx = trx * 1000 / tmr_refresh.Interval;

            lbl_trx.Text += "Now: " + trx.ToString(".00") + " ";

          }
          sim_req_trx_1 = sim_req_trx;
          sim_trx1 = sim_trx;
        }
        else
        {
          SimulatedTerminal t;
          TimeSpan e;
          WC2_Prize prize;
          WC2_CardPrize card_prize;
          double card_prize_amount;
          int idx_number;
          int idx_position;
          int[] positions;

          t = (SimulatedTerminal)terminals[0];

          if (t.anim_prize)
          {
            e = DateTime.Now - t.anim_prize_datetime;
            if (e.TotalMilliseconds >= 1500)
            {
              prize = (WC2_Prize)t.Prizes[t.idx_prize];
              if (prize.NumCardPrizes > 0)
              {
                card_prize = (WC2_CardPrize)prize.CardPrizes[t.idx_card_prize];

                // Show prize amount
                card_prize_amount = card_prize.PrizeCredits * 0.01;
                lbl_card_prize.Text = card_prize_amount.ToString("C");

                // Show prizes
                positions = new int[card_prize.NumPositions];
                positions = card_prize.GetPositions();

                for (idx_position = 0; idx_position < positions.Length; idx_position++)
                {
                  idx_number = 24;
                  foreach (Label lbl in pnl_cards.Controls)
                  {
                    if (idx_number == positions[idx_position])
                    {
                      if (lbl.BackColor != Color.FromArgb(255, 255, 130))
                      {
                        lbl.BackColor = Color.FromArgb(255, 192, 192);
                      }
                      else
                      {
                        lbl.BackColor = Color.FromArgb(255, 150, 150);
                      }
                      if (!txt_draw_numbers.Text.Contains(int.Parse(lbl.Text).ToString("00")))
                      {
                        lbl.BackColor = Color.FromArgb(255, 0, 0);
                      }
                      break;
                    }

                    idx_number--;
                  }
                }

                t.idx_card_prize++;
                if (t.idx_card_prize >= prize.NumCardPrizes)
                {
                  t.idx_prize++;
                }
              }
              else
              {
                t.idx_prize++;
              }

              if (t.idx_prize >= t.Prizes.Count)
              {
                t.anim_prize = false;
              }

              t.anim_prize_datetime = DateTime.Now;

            } // if (e.TotalMilliseconds)
          } // if (t.anim_prize)

        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
    }

    void UpdateGUI (SimulatedTerminal Terminal, String Xml)
    {
      try
      {
        if (!sim_enabled)
        {
          if (terminals[0].Equals(Terminal))
          {
            String aux_str;
            WC2_Card card;
            int idx_card;
            int idx_number;
            int[] numbers;
            WC2_Jackpot jackpot;
            int idx_jackpot;
            WC2_Prize prize;
            WC2_CardPrize card_prize;
            int stop_index;
            int idx_draw_number;
            int[] draw_numbers;

            aux_str = Xml.Substring(Xml.IndexOf("ResponseCode") + 13);
            aux_str = aux_str.Substring(0, aux_str.IndexOf("ResponseCode") - 2);
            aux_str = "RC = " + aux_str + "\r\n\r\n";
            txt_rcv_terminal.Text = aux_str + Xml;
            txt_trm_session_id.Text = Terminal.terminal_session_id.ToString();
            txt_trm_seq_id.Text = Terminal.sequence_id.ToString();

            // Cards
            if (Terminal.Cards.Count > 0)
            {
              for (idx_card = 0; idx_card < Terminal.Cards.Count; idx_card++)
              {
                card = (WC2_Card)Terminal.Cards[idx_card];

                numbers = card.GetNumbers();

                idx_number = 24;
                foreach (Label lbl in pnl_cards.Controls)
                {
                  lbl.Text = numbers[idx_number].ToString();
                  idx_number--;
                }
              }
            }

            // Jackpots
            switch ( Terminal.LastJackpotReceived )
            {
              case WC2_MsgTypes.WC2_MsgGetJackpotInfoReply :
                this.label4.Text = "Jackpots";

                if ( Terminal.Jackpots.Count > 0 )
                {
                  for ( idx_jackpot = 0; idx_jackpot < Terminal.Jackpots.Count; idx_jackpot++ )
                  {
                    jackpot = (WC2_Jackpot) Terminal.Jackpots[idx_jackpot];

                    switch ( idx_jackpot )
                    {
                      case 0:
                        txt_Jackpot_name_0.Text = jackpot.Name;
                        txt_jackpot_0.Text = jackpot.Amount.ToString("C");
                      break;

                      case 1:
                        txt_Jackpot_name_1.Text = jackpot.Name;
                        txt_jackpot_1.Text = jackpot.Amount.ToString("C");
                      break;

                      case 2:
                        txt_Jackpot_name_2.Text = jackpot.Name;
                        txt_jackpot_2.Text = jackpot.Amount.ToString("C");
                      break;

                      default:
                      break;
                    }
                  }
                }
              break;

              case WC2_MsgTypes.WC2_MsgSiteJackpotInfoReply :
                this.label4.Text = "Site Jackpots";

                if ( Terminal.SiteJackpots.Count > 0 )
                {
                  for ( idx_jackpot = 0; idx_jackpot < Terminal.SiteJackpots.Count; idx_jackpot++ )
                  {
                    jackpot = (WC2_Jackpot) Terminal.SiteJackpots[idx_jackpot];

                    switch ( idx_jackpot )
                    {
                      case 0:
                        txt_Jackpot_name_0.Text = jackpot.Name;
                        txt_jackpot_0.Text = jackpot.Amount.ToString ("C");
                        break;

                      case 1:
                        txt_Jackpot_name_1.Text = jackpot.Name;
                        txt_jackpot_1.Text = jackpot.Amount.ToString ("C");
                        break;

                      case 2:
                        txt_Jackpot_name_2.Text = jackpot.Name;
                        txt_jackpot_2.Text = jackpot.Amount.ToString ("C");
                        break;

                      default:
                        break;
                    }
                  }
                }
              break;

              default :
              break;
            }

            if ( Terminal.play_delayed )
            {
              lbl_play_delayed.Visible = true;
            }
            else
            {
              lbl_play_delayed.Visible = false;
            }

            // Draw
            if (Terminal.DrawId > 0 && Terminal.DrawId != Terminal.LastDrawId)
            {
              txt_draw_id.Text = Terminal.DrawId.ToString();
              txt_draw_numbers.Text = Terminal.Draw.BallList;

              // Clear prizes
              foreach (Label lbl in pnl_cards.Controls)
              {
                lbl.BackColor = Color.FromArgb(255, 255, 255);
              }

              // Calculate stop index
              stop_index = 75;
              if (Terminal.Prizes.Count > 0)
              {
                prize = (WC2_Prize)Terminal.Prizes[0];
                if (prize.NumCardPrizes > 0)
                {
                  card_prize = (WC2_CardPrize)prize.CardPrizes[0];
                  stop_index = card_prize.ResultIndex;
                }
              }

              // Show Draw numbers
              draw_numbers = Terminal.Draw.GetBalls();
              for (idx_draw_number = 0; idx_draw_number < draw_numbers.Length; idx_draw_number++)
              {
                if (idx_draw_number > stop_index)
                {
                  break;
                }

                foreach (Label lbl in pnl_cards.Controls)
                {
                  if (lbl.Text == draw_numbers[idx_draw_number].ToString())
                  {
                    if (idx_draw_number < Terminal.Draw.MaxBallsForPrize)
                    {
                      lbl.BackColor = Color.FromArgb(255, 255, 192);
                    }
                    else
                    {
                      lbl.BackColor = Color.FromArgb(255, 255, 130);
                    }
                    break;
                  }
                }
              }
            }

            Terminal.LastDrawId = Terminal.DrawId;

            // Prizes
            if (Terminal.Prizes.Count > 0 && Terminal.anim_prize)
            {
              int string_lenght;

              // Cut balls according result index 
              prize = (WC2_Prize)Terminal.Prizes[0];
              if (prize.NumCardPrizes > 0)
              {
                card_prize = (WC2_CardPrize)prize.CardPrizes[0];

                string_lenght = (card_prize.ResultIndex + 1) * 3;

                if (txt_draw_numbers.Text.Length > string_lenght)
                {
                  txt_draw_numbers.Text = txt_draw_numbers.Text.Substring(0, string_lenght);
                }
              }

              lbl_prize.Text = Terminal.total_prize_amount.ToString("C");

              Terminal.anim_prize_datetime = DateTime.MinValue;
              Terminal.idx_prize = 0;
              Terminal.idx_card_prize = 0;
            }

          }
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
    }

    void MessageReceived (SimulatedTerminal Terminal, String Xml)
    {
      Boolean show_response;
      //Boolean sim_next_step;
      WC2_Message response;
      Boolean force_refresh;

      response = null;
      //sim_next_step = true;

      try
      {
        show_response = true;
        force_refresh = false;

        this.sim_trx++;

        response = WC2_Message.CreateMessage (Xml);

        if ( response.MsgHeader.ResponseCode == WC2_ResponseCodes.WC2_RC_SERVER_SESSION_NOT_VALID
            || response.MsgHeader.ResponseCode == WC2_ResponseCodes.WC2_RC_TERMINAL_SESSION_NOT_VALID )
        {
          Terminal.enrolled = false;
        }

        switch ( response.MsgHeader.MsgType )
        {
          case WC2_MsgTypes.WC2_MsgEnrollTerminalReply:
          {
            WC2_MsgEnrollTerminalReply reply;
            reply = (WC2_MsgEnrollTerminalReply) response.MsgContent;

            if (response.MsgHeader.ResponseCode == WC2_ResponseCodes.WC2_RC_OK)
            {
              Terminal.terminal_session_id = response.MsgHeader.TerminalSessionId;
              Terminal.sequence_id = reply.LastSequenceId + 1;
              Terminal.transaction_id = reply.LastTransactionId + 1;
              Terminal.enrolled = true;
            }
          }
          break;

          case WC2_MsgTypes.WC2_MsgGetParametersReply:
          {
            Terminal.pending_get_parameters = false;
          }
          break;

          case WC2_MsgTypes.WC2_MsgPlayReply:
          {
            WC2_MsgPlayReply reply;
            reply = (WC2_MsgPlayReply) response.MsgContent;

            Terminal.play_delayed = false;
            if (response.MsgHeader.ResponseCode == WC2_ResponseCodes.WC2_RC_OK)
            {
              // Save in Terminal: Cards, Draw, Prizes.
              Terminal.Cards = reply.Cards;
              Terminal.DrawId = reply.DrawId;
              Terminal.Draw = reply.Draw;
              Terminal.Prizes = reply.Prizes;
              Terminal.total_prize_amount = reply.TotalWonCredits * 0.01;

              if (Terminal.Prizes.Count > 0)
              {
                Terminal.anim_prize = true;
              }

              if (reply.HasJackpotPrize)
              {
                Terminal.jackpot_awarded = true;
                Terminal.jackpot_amount = reply.JackpotData.Amount.ToString("C");
                Terminal.jackpot_name = "Jackpot: " + reply.JackpotData.Name;
                force_refresh = true;
              }

              if (reply.TransactionId != (Terminal.transaction_id - 1))
              {
                //sim_next_step = false;
              }
            }
            else if (response.MsgHeader.ResponseCode == WC2_ResponseCodes.WC2_RC_PLAY_DELAYED)
            {
              //sim_next_step = false;
              Terminal.play_delayed = true;
            }
            else
            {
              // Nothing
            }
          }
          break;

          case WC2_MsgTypes.WC2_MsgCancelPlayReply:
          {
            if (response.MsgHeader.ResponseCode == WC2_ResponseCodes.WC2_RC_OK)
            {
              Terminal.play_delayed = false;
            }
          }
          break;

          case WC2_MsgTypes.WC2_MsgGetCardsReply:
          {
            WC2_MsgGetCardsReply reply;

            Terminal.pending_get_cards = false;

            reply = (WC2_MsgGetCardsReply)response.MsgContent;

            Terminal.Cards = reply.Cards;
          }
          break;

        case WC2_MsgTypes.WC2_MsgGetJackpotInfoReply:
          {
            WC2_MsgGetJackpotInfoReply reply;

            reply = (WC2_MsgGetJackpotInfoReply)response.MsgContent;

            Terminal.LastJackpotReceived = response.MsgHeader.MsgType;
            Terminal.Jackpots = reply.Jackpots;
          }
          break;

        case WC2_MsgTypes.WC2_MsgSiteJackpotInfoReply:
          {
            WC2_MsgSiteJackpotInfoReply reply;

            reply = (WC2_MsgSiteJackpotInfoReply) response.MsgContent;

            Terminal.LastJackpotReceived = response.MsgHeader.MsgType;
            Terminal.SiteJackpots = reply.Jackpots;
          }
          break;

        case WC2_MsgTypes.WC2_MsgKeepAliveReply:
          {
            show_response = false;
          }
          break;
        }

        if ( show_response )
        {
          if (   force_refresh
              || !sim_enabled
              || sim_trx % 100*terminals.Count  == 0 )
          {
            this.BeginInvoke (new SetTextCallback (UpdateGUI), new object[] { Terminal, Xml });
          }
        }

        if ( response.MsgHeader.MsgType != WC2_MsgTypes.WC2_MsgKeepAliveReply )
        {
          lock ( Terminal )
          {
            Terminal.pending_reply = false;
          }
        }
      }
      catch ( Exception ex )
      {
        Log.Exception (ex);

        return;
      }
      finally
      {
        Int64 _interval = Environment.TickCount - Terminal.last_sent_message_ticks;

        // MBF Show response time in GUI
        if (!Terminal.pending_reply)
        {
          lock (m_statistics)
          {
            Monitor.Enter(m_statistics);

            Boolean _add;
            Statistics _stats;
            double _aux;
            WC2_MsgTypes _type;

            _type = (WC2_MsgTypes)response.MsgHeader.MsgType - 1;
            _aux = 0;
            _add = true;
            _stats = new Statistics();

            if (m_statistics.ContainsKey(_type))
            {
              _stats = (Statistics)m_statistics[_type];
              _add = false;
            }

            _aux = _stats.time_av * (double)_stats.count + _interval;
            _stats.count++;
            _stats.time_av = _aux / (double)_stats.count;

            if (_interval > _stats.time_max)
            {
              _stats.time_max = _interval;
            }

            _stats.name = _type.ToString();

            if (_add)
            {
              m_statistics.Add(_type, _stats);
            }

            Monitor.Exit(m_statistics);
          }
        }
      }

    }

    public Form1 ()
    {
      Thread _thread;

      InitializeComponent ();

      CreateTerminals (1);


      _thread = new Thread(SimulationThread);
      _thread.Name = "SimulationThread";
      _thread.Start();

    }


    #region Button Events

    private void btn_connect_Click (object sender, EventArgs e)
    {
      int num_terminals;

      foreach (SimulatedTerminal t in terminals)
      {
        if (t.IsConnected)
        {
          t.Disconnect();
        }
      }

      num_terminals = int.Parse(txt_num_terminals.Text);
      CreateTerminals(num_terminals);

      this.Cursor = Cursors.WaitCursor;

      foreach (SimulatedTerminal t in terminals)
      {
        t.m_remote = cmb_service_address.Text;
        ThreadPool.QueueUserWorkItem(new WaitCallback(ConnectionThread), t);
      }

      // Wait for disconnecting tcp clients
      System.Threading.Thread.Sleep(8000);

      this.Cursor = Cursors.Default;
    }

    private void ConnectionThread(Object Terminal)
    {
      SimulatedTerminal _terminal;

      try
      {
        int t;
        int cp;
        ThreadPool.GetMaxThreads(out t, out cp);

        _terminal = (SimulatedTerminal)Terminal;
        _terminal.Connect(_terminal.m_remote);
      }
      catch
      {
      }
    }

    private void btn_keep_alive_Click (object sender, EventArgs e)
    {
      tmr_server_keep_alive.Enabled = !tmr_server_keep_alive.Enabled;
      trm_keep_alive_terminal.Enabled = !trm_keep_alive_terminal.Enabled;

      if ( trm_keep_alive_terminal.Enabled )
      {
        lbl_keep_alive.Text = "Started";
        lbl_keep_alive.ForeColor = Color.FromArgb (0, 192, 0);
      }
      else
      {
        lbl_keep_alive.Text = "Stopped";
        lbl_keep_alive.ForeColor = Color.FromArgb (255, 128, 128);
      }

    }

    private void btn_start_rnd_simulation_Click (object sender, EventArgs e)
    {
      Boolean _aux_sim_enabled;
      int _offset_tick;
      int _idx_terminal;

      _aux_sim_enabled = !sim_enabled;

      try
      {
        m_interval_get_cards = int.Parse(txt_get_cards_intv.Text);
        m_interval_play = int.Parse(txt_play_intv.Text);
        m_interval_get_jackpot = int.Parse(txt_jackpot_intv.Text);
        m_interval_site_jackpot = int.Parse (txt_site_jackpot_intv.Text);
        m_interval_get_patterns = int.Parse (txt_patterns_intv.Text);
        m_interval_get_params = int.Parse(txt_get_params_intvl.Text);
      }
      catch // (Exception ex)
      {
         m_interval_get_cards = 0;
         m_interval_play = 0;
         m_interval_get_jackpot = 0;
         m_interval_site_jackpot = 0;
         m_interval_get_patterns = 0;
         m_interval_get_params = 0;
      }

      if (_aux_sim_enabled)
      {
        lbl_simulation.Text = "Started";
        lbl_simulation.ForeColor = Color.FromArgb(0, 192, 0);

        sim_started = DateTime.Now;
        sim_no_trx = false;
        sim_trx = 0;
        sim_trx1 = 0;
        sim_trx2 = 0;
      }
      else
      {
        lbl_simulation.Text = "Stopped";
        lbl_simulation.ForeColor = Color.FromArgb(255, 128, 128);
      }

      if (_aux_sim_enabled)
      {
        if (terminals != null)
        {
          _idx_terminal = 0;
          _offset_tick = int.Parse(txt_time_plays.Text) / terminals.Count;
          foreach (SimulatedTerminal st in terminals)
          {
            st.step = SimulatedTerminal.WC2_SimulationSteps.WC2_SETP_GET_ENROLL;
            if (chk_sim_plays.Checked == true)
            {
              st.pending_plays = int.Parse(txt_pending_plays.Text);
            }
            else
            {
              st.pending_plays = int.MaxValue;
            }

            if (chk_time_plays.Checked == true)
            {
              st.time_between_plays = int.Parse(txt_time_plays.Text);
              st.last_play_tick = Environment.TickCount - st.time_between_plays + (_offset_tick * _idx_terminal);
              _idx_terminal++;
            }
            else
            {
              st.time_between_plays = 0;
            }

            st.pending_reply = false;
            st.pending_reply_tick = Environment.TickCount;
            sim_req_trx = 0;
            sim_req_trx_1 = 0;
          }
        }
      }

      sim_enabled = _aux_sim_enabled;
    }

    private void btn_send_server_Click (object sender, EventArgs e)
    {
      //server.Send (txt_snd_server.Text);
    }

    private void btn_send_terminal_Click (object sender, EventArgs e)
    {
      SimulatedTerminal t;
      t = (SimulatedTerminal) terminals[0];
      t.Send (txt_snd_terminal.Text);
    }

    private void btn_clear_server_Click (object sender, EventArgs e)
    {
      //txt_rcv_server.Text = "";
    }

    private void btn_clear_terminal_Click (object sender, EventArgs e)
    {
      txt_rcv_terminal.Text = "";
    }

    #endregion

    #region Timers

    private void tmr_server_keep_alive_Tick (object sender, EventArgs e)
    {
      ////DateTime tm1;

      ////tm1 = server.last_sent_message.AddMilliseconds (20000);
      ////if ( DateTime.Now >= tm1 )
      ////{
      ////  SendRequestFrom (server, WC2_MsgTypes.WC2_MsgKeepAlive);
      ////}
    }

    private void trm_keep_alive_terminal_Tick (object sender, EventArgs e)
    {
      DateTime tm1;

      foreach ( SimulatedTerminal t in terminals )
      {
        tm1 = t.last_sent_message.AddMilliseconds (20000);
        if ( DateTime.Now >= tm1 )
        {
          SendRequestFrom (t, WC2_MsgTypes.WC2_MsgKeepAlive);
        }
      }
    }

    private void SimStep (SimulatedTerminal Terminal)
    {
      long _interval;
      if ( !Terminal.IsConnected )
      {
        Terminal.enrolled = false;
        Terminal.pending_reply = false;
        Terminal.Connect (cmb_service_address.Text);

        return;
      }

      if ( Terminal.pending_reply )
      {
        return;
      }

      if ( !Terminal.enrolled )
      {
        SendRequestFrom (Terminal, WC2_MsgTypes.WC2_MsgEnrollTerminal);
        Terminal.pending_get_parameters = true;

        return;
      }

      if ( Terminal.pending_get_parameters )
      {
        SendRequestFrom(Terminal, WC2_MsgTypes.WC2_MsgGetParameters);

        return;
      }

      if (Terminal.Cards.Count == 0)
      {
        SendRequestFrom(Terminal, WC2_MsgTypes.WC2_MsgGetCards);

        return;
      }

      switch ( Terminal.step )
      {
        case SimulatedTerminal.WC2_SimulationSteps.WC2_SETP_GET_UNENROLL:
        {
          Terminal.step++;
          if (chk_unenroll.Checked)
          {
            SendRequestFrom(Terminal, WC2_MsgTypes.WC2_MsgUnenrollTerminal);
          }
        }
        break;

        case SimulatedTerminal.WC2_SimulationSteps.WC2_SETP_GET_ENROLL:
        {
          Terminal.step++;
          if (chk_enroll_terminal.Checked)
          {
            SendRequestFrom(Terminal, WC2_MsgTypes.WC2_MsgEnrollTerminal);
          }
        }
        break;

        case SimulatedTerminal.WC2_SimulationSteps.WC2_SETP_GET_PARAMS:
        {
          Terminal.step++;
          _interval = Misc.GetElapsedTicks(Terminal.last_get_params);
          if (chk_get_params.Checked && _interval > m_interval_get_params)
          {
            Terminal.last_get_params = Environment.TickCount;
            SendRequestFrom(Terminal, WC2_MsgTypes.WC2_MsgGetParameters);
          }
        }
        break;

        case SimulatedTerminal.WC2_SimulationSteps.WC2_SETP_GET_CARDS:
        {
          Terminal.step++;
          _interval = Misc.GetElapsedTicks(Terminal.last_get_cards);
          if (chk_get_cards.Checked && _interval > m_interval_get_cards)
          {
            Terminal.last_get_cards = Environment.TickCount;
            SendRequestFrom(Terminal, WC2_MsgTypes.WC2_MsgGetCards);
          }
        }
        break;

        case SimulatedTerminal.WC2_SimulationSteps.WC2_SETP_GET_PATTERNS:
        {
          Terminal.step++;
          _interval = Misc.GetElapsedTicks(Terminal.last_get_patterns);
          if (chk_get_patterns.Checked && _interval > m_interval_get_patterns)
          {
            Terminal.last_get_patterns = Environment.TickCount;
            SendRequestFrom(Terminal, WC2_MsgTypes.WC2_MsgGetPatterns);
          }
        }
        break;

        case SimulatedTerminal.WC2_SimulationSteps.WC2_SETP_PLAY:
        {
          Terminal.step++;
          _interval = Misc.GetElapsedTicks(Terminal.last_play);
          if (chk_play.Checked && _interval > m_interval_play)
          {
            if (Terminal.pending_plays > 0)
            {
              Terminal.last_play = Environment.TickCount;
              SendRequestFrom(Terminal, WC2_MsgTypes.WC2_MsgPlay);
              Terminal.pending_plays--;
            }
          }
        }
        break;

        case SimulatedTerminal.WC2_SimulationSteps.WC2_SETP_CANCEL_PLAY:
        {
          Terminal.step++;
          if (chk_cancel_play.Checked)
          {
            SendRequestFrom(Terminal, WC2_MsgTypes.WC2_MsgCancelPlay);
          }
        }
        break;

        case SimulatedTerminal.WC2_SimulationSteps.WC2_SETP_GET_JACKPOT:
        {
          Terminal.step = SimulatedTerminal.WC2_SimulationSteps.WC2_SETP_GET_UNENROLL;
          _interval = Misc.GetElapsedTicks(Terminal.last_get_jackpot);
          if (chk_get_jackpot.Checked && _interval > m_interval_get_jackpot)
          {
            Terminal.last_get_jackpot = Environment.TickCount;
            SendRequestFrom(Terminal, WC2_MsgTypes.WC2_MsgGetJackpotInfo);
          }
        }
        break;

        case SimulatedTerminal.WC2_SimulationSteps.WC2_SETP_SITE_JACKPOT:
        {
          Terminal.step = SimulatedTerminal.WC2_SimulationSteps.WC2_SETP_GET_UNENROLL;
          _interval = Misc.GetElapsedTicks(Terminal.last_site_jackpot);

          if ( chk_site_jackpot.Checked && _interval > m_interval_site_jackpot )
          {
            Terminal.last_site_jackpot = Environment.TickCount;
            SendRequestFrom(Terminal, WC2_MsgTypes.WC2_MsgSiteJackpotInfo);
          }
        }
        break;

        //case SimulatedTerminal.WC2_SimulationSteps.WC2_SETP_FINISHED:
        //break;

        default:
        break;
      }

    }
    #endregion

    #region Messages

    void SendRequestFrom (SimulatedTerminal Terminal, WC2_MsgTypes MsgType)
    {
      WC2_Message WC2_request;

      Int64 _interval = Environment.TickCount - Terminal.last_sent_message_ticks;

      if (_interval > 30000
        && Terminal.pending_reply)
      {
        // more than 30 seconds without response
        Terminal.pending_reply = false;
      }

      if (MsgType == WC2_MsgTypes.WC2_MsgPlay)
      {
        Terminal.last_play_tick = Environment.TickCount;
      }

      try
      {
        WC2_request = WC2_Message.CreateMessage (MsgType);
        WC2_request.MsgHeader.TerminalId = Terminal.terminal_id;
        WC2_request.MsgHeader.TerminalSessionId = Terminal.terminal_session_id;
        WC2_request.MsgHeader.SequenceId = Terminal.sequence_id;
        Terminal.sequence_id++;

        {
          switch ( MsgType )
          {
            case WC2_MsgTypes.WC2_MsgEnrollTerminal:
            {
              WC2_request.MsgHeader.TerminalSessionId = 0;
              WC2_request.MsgHeader.SequenceId = 0;
            }
            break;

          case WC2_MsgTypes.WC2_MsgRequestService:
            {
              WC2_MsgRequestService request;
              IPAddress ip;
              int port;

              ip = IPAddress.Parse("192.168.1.1");
              port = 1000;
              request = (WC2_MsgRequestService)WC2_request.MsgContent;
              request.ServiceName = "prueba";
              request.ReplyToUDPAddress = new IPEndPoint(ip, port);
            }
            break;

          case WC2_MsgTypes.WC2_MsgCancelPlay:
            {
              WC2_MsgCancelPlay request;

              request = (WC2_MsgCancelPlay)WC2_request.MsgContent;
              request.TransactionId = Terminal.transaction_id - 1;
            }
            break;


          case WC2_MsgTypes.WC2_MsgGetCards:
            {
              WC2_MsgGetCards request;
              request = (WC2_MsgGetCards)WC2_request.MsgContent;
              request.NumCards = 1;
              request.CardType = "Card5x5";

              Terminal.anim_prize = false;
              Terminal.Prizes.Clear();
              Terminal.DrawId = 0;
              Terminal.Draw.NumBalls = 0;
              Terminal.Draw.BallList = "";
            }
            break;

            case WC2_MsgTypes.WC2_MsgGetJackpotInfo:
            {
              WC2_MsgGetJackpotInfo request;
              request = (WC2_MsgGetJackpotInfo) WC2_request.MsgContent;
              request.TerminalId = Terminal.terminal_id;
              request.GameId = "Game_1";
              request.Denomination = 1;
              request.TotalBetCredits = 1000;
            }
            break;

            case WC2_MsgTypes.WC2_MsgSiteJackpotInfo:
            {
              WC2_MsgSiteJackpotInfo request;
              request = (WC2_MsgSiteJackpotInfo) WC2_request.MsgContent;
              request.TerminalId = Terminal.terminal_id;
              request.GameId = "Game_1";
              request.Denomination = 1;
              request.TotalBetCredits = 0;
            }
            break;

            case WC2_MsgTypes.WC2_MsgPlay:
            {
              WC2_MsgPlay request;
              Random rnd;

              //if (Terminal.time_between_plays > 0)
              //{
              //  System.Threading.Thread.Sleep(Terminal.time_between_plays);
              //}

              request = (WC2_MsgPlay)WC2_request.MsgContent;

              rnd = new Random ();

              request.TransactionId = Terminal.transaction_id;
              Terminal.transaction_id++;
              request.GameId = "Game_" + rnd.Next(10).ToString();
              request.Denomination = 1;
              request.TotalPlayedCredits = 100;
              request.JackpotBoundCredits = 100;
              Terminal.anim_prize = false;
              Terminal.Prizes.Clear();
              Terminal.DrawId = 0;
              Terminal.Draw.NumBalls = 0;
              Terminal.Draw.BallList = "";

              request.WonCredits = 0;
              if (rnd.Next(10) > 3)
              {
                request.WonCredits = 1000 * (rnd.Next(10) + 1);
                if (rnd.Next(10) > 6)
                {
                  request.WonCredits = 1000 * 10 * (rnd.Next(10) + 1);
                  if (rnd.Next(10) > 6)
                  {
                    request.WonCredits = 1000 * 100 * (rnd.Next(10) + 1);
                  }
                }
              }
              request.NumCards = Terminal.Cards.Count;
              request.Cards = Terminal.Cards;

              // Clear prizes
              //foreach (Label lbl in pnl_cards.Controls)
              //{
              //  lbl.BackColor = Color.FromArgb(255, 255, 255);
              //}
              //lbl_card_prize.Text = "";
              //lbl_prize.Text = "";
            }
            break;

            default:
            break;
          }
        }

        if ( WC2_request != null )
        {
          if ( WC2_request.MsgHeader.MsgType != WC2_MsgTypes.WC2_MsgKeepAlive )
          {
            lock ( Terminal )
            {
              if (Terminal.pending_reply)
              {
                return;
              }

              Terminal.pending_reply = true;
              Terminal.pending_reply_tick = Environment.TickCount;
            }
          }

          Terminal.Send(WC2_request.ToXml());
          Terminal.message_send = MsgType;

          lock (m_statistics)
          {
            Monitor.Enter(m_statistics);
            Boolean _add = true;
            Statistics _stats = null;

            _stats = new Statistics();

            if (m_statistics.ContainsKey(MsgType))
            {
              _stats = (Statistics)m_statistics[MsgType];
              _add = false;
            }

            // Increase sent counter
            _stats.sent++;
            // Save petition
            _stats.name = MsgType.ToString();

            if (_add)
            {
              m_statistics.Add(MsgType, _stats);
            }

            Monitor.Exit(m_statistics);
          }

          sim_req_trx++;
          if (!sim_enabled)
          {
            SimulatedTerminal t;
            t = (SimulatedTerminal)terminals[0];
            if (t.Equals(Terminal))
            {
              txt_snd_terminal.Text = WC2_request.ToXml();
            }
          }
        }
      }
      catch ( Exception ex )
      {
        Log.Exception (ex);

        return;
      }
    }

    #endregion

    private void CreateTerminals (int NumTerminals)
    {
      int num_terminals;
      SimulatedTerminal t;

      num_terminals = NumTerminals;

      while ( num_terminals > terminals.Count )
      {
        t = new SimulatedTerminal ();

        t.MessageReceived = new OnMessageReceived (this.MessageReceived);

        t.server_id = "UNKNOWN"; //  server.server_id;
        t.terminal_id = cmb_prefix.Text.ToUpper () + "_TERMINAL_" + terminals.Count.ToString ("000");
        terminals.Add (t);
        t.idx = terminals.Count - 1;
      }
      while ( num_terminals < terminals.Count )
      {
        terminals.RemoveAt (terminals.Count - 1);
      }

      cmb_prefix_TextChanged (null, null);
    }

    private void tmr_refresh_Tick (object sender, EventArgs e)
    {
      Int32 num_terminals_conn;
      Int32 num_terminals_enrolled;
      Int32 num_terminals_session_started;

      UpdateStats ();

      num_terminals_conn = 0;
      num_terminals_enrolled = 0;
      num_terminals_session_started = 0;
      foreach (SimulatedTerminal t in terminals)
      {
        if (t.IsConnected)
        {
          num_terminals_conn++;
        }
        else
        {
          t.enrolled = false;
          t.card_session_id = 0;
        }

        if (t.IsConnected && t.enrolled)
        {
          num_terminals_enrolled++;
        }

        if (t.IsConnected && t.enrolled && t.card_session_id > 0)
        {
          num_terminals_session_started++;
        }
      }

      if (num_terminals_conn > 0)
      {
        lbl_terminals_conn.Text = num_terminals_conn.ToString() + " Terminals Connected";
        lbl_terminals_conn.ForeColor = Color.FromArgb(0, 192, 0);
      }
      else
      {
        lbl_terminals_conn.Text = "0 Terminals Connected";
        lbl_terminals_conn.ForeColor = Color.FromArgb(255, 128, 128);
      }

      if (num_terminals_enrolled > 0)
      {
        lbl_terminals_enrolled.Text = num_terminals_enrolled.ToString() + " Enrolled";
        lbl_terminals_enrolled.ForeColor = Color.FromArgb(0, 192, 0);
      }
      else
      {
        lbl_terminals_enrolled.Text = "0 Enrolled";
        lbl_terminals_enrolled.ForeColor = Color.FromArgb(255, 128, 128);
      }

      if (num_terminals_session_started > 0)
      {
        lbl_terminals_start_session.Text = num_terminals_session_started.ToString() + " Started Session";
        lbl_terminals_start_session.ForeColor = Color.FromArgb(0, 192, 0);
      }
      else
      {
        lbl_terminals_start_session.Text = "0 Started Session";
        lbl_terminals_start_session.ForeColor = Color.FromArgb(255, 128, 128);
      }

    }

    private void chk_sim_plays_CheckedChanged(object sender, EventArgs e)
    {
      if (chk_sim_plays.Checked == true)
      {
        txt_pending_plays.Enabled = true;
      }
      else
      {
        txt_pending_plays.Enabled = false;
      }
    }

    private void cmb_prefix_SelectedIndexChanged (object sender, EventArgs e)
    {
      cmb_prefix_TextChanged (sender, e);
    }
    
    void cmb_prefix_TextChanged (object sender, System.EventArgs e)
    {
      int idx;
      SimulatedTerminal t0;

      lock ( terminals )
      {
        idx = 0;
        foreach ( SimulatedTerminal t in terminals )
        {
          t.server_id = "UNKNOWN"; // server.server_id;
          t.terminal_id =  cmb_prefix.Text.ToUpper () + "_TERMINAL_" + idx.ToString("000");
          idx += 1;
        }
      }

      t0 = (SimulatedTerminal) terminals[0];
      lbl_terminal.Text = t0.terminal_id.Replace("&", "&&");
    }

    private void chk_time_plays_CheckedChanged(object sender, EventArgs e)
    {
      if (chk_time_plays.Checked == true)
      {
        txt_time_plays.Enabled = true;
      }
      else
      {
        txt_time_plays.Enabled = false;
      }
    }

    private void btn_get_jackpot_info_Click (object sender, EventArgs e)
    {
      foreach (SimulatedTerminal t in terminals)
      {
        SendRequestFrom(t, WC2_MsgTypes.WC2_MsgGetJackpotInfo);
      }
    }

    private void btn_site_jackpot_Click (object sender, EventArgs e)
    {
      foreach ( SimulatedTerminal t in terminals )
      {
        SendRequestFrom (t, WC2_MsgTypes.WC2_MsgSiteJackpotInfo);
      }
    }

    private void btn_request_service_Click (object sender, EventArgs e)
    {
      foreach (SimulatedTerminal t in terminals)
      {
        SendRequestFrom(t, WC2_MsgTypes.WC2_MsgRequestService);
      }
    }

    private void btn_get_cards_Click (object sender, EventArgs e)
    {
      foreach (SimulatedTerminal t in terminals)
      {
        SendRequestFrom(t, WC2_MsgTypes.WC2_MsgGetCards);
      }
    }

    private void btn_get_patterns_Click (object sender, EventArgs e)
    {
      foreach (SimulatedTerminal t in terminals)
      {
        SendRequestFrom(t, WC2_MsgTypes.WC2_MsgGetPatterns);
      }
    }

    private void btn_cancel_play_Click (object sender, EventArgs e)
    {
      foreach (SimulatedTerminal t in terminals)
      {
        SendRequestFrom(t, WC2_MsgTypes.WC2_MsgCancelPlay);
      }
    }

    private void btn_get_params_Click (object sender, EventArgs e)
    {
      foreach (SimulatedTerminal t in terminals)
      {
        SendRequestFrom(t, WC2_MsgTypes.WC2_MsgGetParameters);
      }
    }
    private void btn_unenroll_terminal_Click(object sender, EventArgs e)
    {
      foreach (SimulatedTerminal t in terminals)
      {
        SendRequestFrom(t, WC2_MsgTypes.WC2_MsgUnenrollTerminal);
        t.enrolled = false;
      }
    }

    private void btn_enroll_terminal_Click(object sender, EventArgs e)
    {
      foreach (SimulatedTerminal t in terminals)
      {
        SendRequestFrom(t, WC2_MsgTypes.WC2_MsgEnrollTerminal);
      }
    }
    private void btn_report_play_Click(object sender, EventArgs e)
    {
      foreach (SimulatedTerminal t in terminals)
      {
        t.time_between_plays = 0;
        SendRequestFrom (t, WC2_MsgTypes.WC2_MsgPlay);
      }
    }

    private void button7_Click(object sender, EventArgs e)
    {
      lock (m_statistics)
      {
        Monitor.Enter(m_statistics);
        m_statistics.Clear();
        Monitor.Exit(m_statistics);
      }
    }
  }

  class Statistics
  {
    public double time_max;
    public double time_av;
    public Int64 count;
    public String name;
    public Int64 timeouts;
    public Int64 sent;
  }

}