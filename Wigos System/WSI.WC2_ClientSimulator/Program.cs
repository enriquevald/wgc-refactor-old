using System;
using System.Collections.Generic;
using System.Windows.Forms;
using WSI.Common;

namespace WSI.WC2_ClientSimulator
{
  static class Program
  {
    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    [STAThread]
    static void Main ()
    {
      Log.AddListener (new SimpleLogger ("SIM"));

      Application.EnableVisualStyles ();
      Application.SetCompatibleTextRenderingDefault (false);
      Application.Run (new Form1 ());
    }
  }
}