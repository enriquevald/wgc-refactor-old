using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WSI.Common;

namespace WSI.TestDB
{
  public class DB_ReportPlay
  {
    DataSet ds;
    SqlCommand cmd_games_insert;


    public void GetTerminals()
    {
      SqlDataAdapter da;

      ds = new DataSet();
      ds.Tables.Add("TERMINALS");

      WGDB.Init(14, "WG_SERVER_1", "WG_SERVER_2");
      WGDB.ConnectAs("WGROOT");

      da = new SqlDataAdapter();
      da.SelectCommand = new SqlCommand();
      da.SelectCommand.Connection = WGDB.Connection();

      // Terminals
      da.SelectCommand.CommandText = "SELECT * FROM TERMINALS";
      da.Fill(ds.Tables["TERMINALS"]);

    }


    public bool TerminalExist(Int32 TerminalId)
    {
      lock (ds)
      {
        DataTable dtbl_terminal;
        DataRow[] dtrw_terminal;

        dtbl_terminal = ds.Tables["TERMINALS"];

        dtrw_terminal = dtbl_terminal.Select("TE_TERMINAL_ID = " + TerminalId.ToString());
        if (dtrw_terminal.Length == 0)
        {
          return false;
        }
      }

      return true;
    }

    public void CreateInsertCommandForGames ()
    {
      cmd_games_insert = new SqlCommand ();

      cmd_games_insert.CommandText = "INSERT INTO GAMES (GM_GAME_ID, GM_NAME) VALUES (@p1, @p2)";
      cmd_games_insert.Parameters.Add ("@p1", SqlDbType.Int, 4, "GM_GAME_ID");
      cmd_games_insert.Parameters.Add ("@p2", SqlDbType.NVarChar, 50, "GM_NAME");
    }

    public void UpdateGames ()
    {
      SqlDataAdapter da;
      DataTable table;


      da = new SqlDataAdapter ();
      
      da.InsertCommand = cmd_games_insert;
      da.InsertCommand.Connection = WGDB.Connection ();

      da.RowUpdated += new SqlRowUpdatedEventHandler (TableGamesRowInserted);

      lock ( ds )
      {
        table = ds.Tables["GAMES"].Copy();
      }
      da.Update (table);

      da.SelectCommand = new SqlCommand ("SELECT * FROM GAMES");
      da.SelectCommand.Connection = da.InsertCommand.Connection;

      table = new DataTable ("GAMES");
      table.BeginLoadData ();
      da.Fill (table);
      table.EndLoadData();

      lock ( ds )
      {
        ds.Tables.Remove (ds.Tables["GAMES"]);
        ds.Tables.Add(table);
      }

      da.InsertCommand.Connection.Close ();
      da.InsertCommand.Connection = null;
      da.SelectCommand.Connection = null;
      da.SelectCommand= null;
      da.InsertCommand = null;
    }

    void TableGamesRowInserted (object sender, SqlRowUpdatedEventArgs e)
    {
      if ( e.StatementType == StatementType.Insert )
      {
        if ( e.Status == UpdateStatus.ErrorsOccurred )
        {
          if ( e.Errors.Message.Contains("PRIMARY KEY") )
          {
            Console.WriteLine ("Duplicate Game: " + e.Row[0] + " " + e.Row[1]);
          }
          
          e.Status = UpdateStatus.SkipCurrentRow;
        }
      }
    }


    public void Init ()
    {
      SqlDataAdapter da;

      ds = new DataSet ();

      CreateInsertCommandForGames ();

      ds.Tables.Add ("PLAY_SESSIONS");
      ds.Tables.Add ("GAMES");
      ds.Tables.Add ("PLAYS");
      ds.Tables.Add ("MOVEMENTS");

      WGDB.Init(14, "WG_SERVER_1", "WG_SERVER_2");
      WGDB.ConnectAs("WGROOT");

      da = new SqlDataAdapter();
      da.SelectCommand = new SqlCommand ();
      da.SelectCommand.Connection = WGDB.Connection ();
      
      foreach ( DataTable table in ds.Tables )
      {
        da.SelectCommand.CommandText = "SELECT * FROM " + table.TableName;
        da.FillSchema (table, SchemaType.Source);
      }

      // Games
      da.SelectCommand.CommandText = "SELECT * FROM GAMES";
      da.Fill (ds.Tables["GAMES"]);

      // Terminals
      


      da.SelectCommand.Connection.Close ();
      da.SelectCommand.Connection = null;
      da.SelectCommand = null;
    }

    public void CreatePlaySession ()
    {
      DataTable table;
      DataRow row;

      lock ( ds )
      {
        table = ds.Tables["PLAY_SESSIONS"];

        row = table.NewRow ();


        table.Rows.Add (row);

        row = null;
        table = null;
      }
    }

    public void EndPlaySession ()
    { 
    
    }

    public String GetGameName (Int32 GameId)
    {
      DataTable tbl_games;
      DataRow[] games;
      String game_name;

      lock ( ds )
      {
        DataRow game;
        
        tbl_games = ds.Tables["GAMES"];

        games = tbl_games.Select ("GM_GAME_ID = " + GameId.ToString ());
        if ( games.Length == 1 )
        {
           // tbl_games.Rows.Remove (games[0]);

          game_name = (String) games[0]["GM_NAME"];

          return game_name;
        }  

        game_name = "GAME_" + GameId.ToString ("000000");
        game = tbl_games.NewRow ();

        game["GM_GAME_ID"] = GameId;
        game["GM_NAME"] = "GAME_" + GameId.ToString ("000000");

        tbl_games.Rows.Add (game);
      }

      return game_name;
    
    }


    public void ReportPlay (Int64 PlaySessionId,
                            Int64 CardId,
                            Int32 TerminalId,
                            Int64 SequenceId,
                            Int64 TransactionId,
                            Int32 GameId,
                            Decimal InitialBalance,
                            Decimal PlayedAmount,
                            Decimal WonAmount,
                            Decimal FinalBalance)
    {
      if ( InitialBalance - PlayedAmount < 0 )
      {
        throw ( new WCP.WCP_Exception (WSI.WCP.WCP_ResponseCodes.WCP_RC_CARD_BALANCE_MISMATCH) );
      }
      if ( InitialBalance - PlayedAmount + WonAmount != FinalBalance )
      {
        throw ( new WCP.WCP_Exception (WSI.WCP.WCP_ResponseCodes.WCP_RC_CARD_BALANCE_MISMATCH) );
      }

      lock ( ds )
      {
        DataTable tbl_games;
        DataRow[] games;

        tbl_games = ds.Tables["GAMES"];
        
        games = tbl_games.Select ("GM_GAME_ID = " + GameId.ToString ());
        if ( games.Length == 0 )
        {
          DataRow game;

          game = tbl_games.NewRow ();
          
          game["GM_GAME_ID"] = GameId;
          game["GM_NAME"] = "GAME_" + GameId.ToString ("000000");

          tbl_games.Rows.Add (game);
        }
              
      }
    }

  }
}
