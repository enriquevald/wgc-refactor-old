using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Threading;

using WSI.Common;
using WSI.WCP;


namespace WSI.TestDB
{
  public class X:DbAsyncResult
  {
    public Int64 wm_msg_id;
  }

  public class Class1
  {
    SqlCommand cmd_select;
    SqlCommand cmd_insert;

    DataSet ds;
    DataTable dt;
    SqlDataAdapter da;


    public bool EndInsertMsg (IAsyncResult Result, out Int64 MsgId)
    {
      X x;
      x = (X) Result;
      MsgId = x.wm_msg_id;

      return true;
    }

    public IAsyncResult BeginInsertMsg (String Xml)
    {
      DataRow row;
      X x;

      lock ( dt )
      {
        x = new X ();

        row = dt.NewRow ();


        row["WM_MESSAGE"] = Xml;
        row["XXX"] = x;

        dt.Rows.Add (row);
      }

      return x;
    }

    public void Update ()
    {
      lock ( dt )
      {
        da.InsertCommand.Connection  = WGDB.Connection();
        da.InsertCommand.Connection.Open ();
        da.InsertCommand.Transaction = da.InsertCommand.Connection.BeginTransaction ();
        da.Update (dt);
        da.InsertCommand.Transaction.Commit ();
        da.InsertCommand.Connection.Close ();

        foreach ( DataRow row in dt.Rows )
        {
          X x;
          ManualResetEvent mr_ev;
          
          x = (X) row["XXX"];
          x.wm_msg_id = (Int64) row["WM_MESSAGE_ID"];

          row["XXX"] = null;

          mr_ev = (ManualResetEvent) x.AsyncWaitHandle;
          mr_ev.Set();
        }

        dt.Rows.Clear ();
        dt.AcceptChanges ();
      }
    }

    public void Init ()
    {
      StringBuilder sql;
      SqlParameter p;
      DataColumn c;


      WGDB.Init (14,"WG_SERVER_1", "WG_SERVER_2");
      WGDB.ConnectAs ("WGROOT");

      ds = new DataSet ();
      dt = new DataTable ("WCP_MESSAGES");
      ds.Tables.Add (dt);

      da = new SqlDataAdapter ();

      cmd_select = new SqlCommand ("SELECT * FROM WCP_MESSAGES");
      da.SelectCommand = cmd_select;
      da.SelectCommand.Connection = WGDB.Connection();
      da.FillSchema(dt, SchemaType.Source);

      c = dt.Columns["WM_MESSAGE_ID"];
      c.AutoIncrement = true;
      c.AutoIncrementSeed = -1;
      c.AutoIncrementStep = -1;
      c.ReadOnly = false;
      c = dt.Columns["WM_DATETIME"];
      c.AllowDBNull = true;
      c = dt.Columns["WM_TOWARDS_TO_TERMINAL"];
      c.DefaultValue = 0;

      dt.Columns.Add("XXX", System.Type.GetType("System.Object"));


      sql = new StringBuilder ();
      sql.AppendLine ("INSERT INTO   WCP_MESSAGES ");
      sql.AppendLine ("            ( WM_TERMINAL_ID, WM_SESSION_ID, WM_TOWARDS_TO_TERMINAL, WM_MESSAGE, WM_SEQUENCE_ID )");
      sql.AppendLine ("     VALUES ( @p1, @p2, @p3, @p4, @p5) ");
      sql.AppendLine ("        SET   @p6 = SCOPE_IDENTITY()");

      cmd_insert = new SqlCommand (sql.ToString ());

      da.SelectCommand = null;
      da.InsertCommand = cmd_insert;

      cmd_insert.Parameters.Add ("@p1", SqlDbType.Int, 4, "WM_TERMINAL_ID");
      cmd_insert.Parameters.Add ("@p2", SqlDbType.BigInt, 8, "WM_SESSION_ID");
      cmd_insert.Parameters.Add ("@p3", SqlDbType.Bit, 1, "WM_TOWARDS_TO_TERMINAL");
      cmd_insert.Parameters.Add ("@p4", SqlDbType.Xml, Int32.MaxValue, "WM_MESSAGE");
      cmd_insert.Parameters.Add ("@p5", SqlDbType.BigInt, 8, "WM_SEQUENCE_ID");

      p = cmd_insert.Parameters.Add ("@p6", SqlDbType.BigInt, 8, "WM_MESSAGE_ID");
      p.Direction = ParameterDirection.Output;


      da.RowUpdated += new SqlRowUpdatedEventHandler (da_RowUpdated);

      //da.InsertCommand.Connection = WGDB.Connection ();
      //da.InsertCommand.Connection.Open ();


      ////for ( int i = 0; i < 10000; i++ )
      ////{
      ////  r = dt.NewRow ();
      ////  r["WM_MESSAGE"] = "<Test></Test>";
      ////  dt.Rows.Add (r);
      ////}


      ////da.InsertCommand.UpdatedRowSource = UpdateRowSource.OutputParameters;
      ////da.UpdateBatchSize = 1;

      ////da.InsertCommand.Transaction = da.InsertCommand.Connection.BeginTransaction ();
      ////t0 = DateTime.Now;
      ////da.Update (dt);
      ////da.InsertCommand.Transaction.Commit ();
      ////t1 = DateTime.Now;
      ////ts0 = t1 - t0;
      ////r = dt.Rows[0];
      ////id0 = (Int64) r[0];




      //dt.Rows.Clear ();
      //dt.AcceptChanges ();


      //for ( int i = 0; i < 500; i++ )
      //{
      //  r = dt.NewRow ();
      //  r["WM_MESSAGE"] = "<Test>AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA</Test>";
      //  dt.Rows.Add (r);
      //}

      //da.InsertCommand.UpdatedRowSource = UpdateRowSource.OutputParameters;
      //da.UpdateBatchSize = 512;
      //da.InsertCommand.Transaction = da.InsertCommand.Connection.BeginTransaction ();
      //t0 = DateTime.Now;
      //da.Update (dt);
      //da.InsertCommand.Transaction.Commit ();
      //t1 = DateTime.Now;
      //ts1 = t1 - t0;

      //r = dt.Rows[0];
      //id1 = (Int64) r[0];


      //dt.Rows.Clear ();
      //dt.AcceptChanges ();

      //num_rows_inserted = sql_command.ExecuteNonQuery ();






    
    }

    void da_RowUpdated (object sender, SqlRowUpdatedEventArgs e)
    {
      if ( e.StatementType == StatementType.Insert )
      {
        //X x;
        //x = e.Row["XXX"];
        e.Row["WM_MESSAGE_ID"] = (Int64) e.Command.Parameters["@p6"].Value; 
      }
    }

  }
}
