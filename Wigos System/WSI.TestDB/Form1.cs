using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.IO;
using WSI.WCP;

namespace WSI.TestDB
{
  public partial class Form1:Form
  {
    DB_ReportPlay db = new DB_ReportPlay ();
    Class1 c1 = new Class1 ();
    IAsyncResult r;

    public Form1 ()
    {
      InitializeComponent ();
    }

    private void button1_Click (object sender, EventArgs e)
    {
      c1.Init ();
    }

    private void button2_Click (object sender, EventArgs e)
    {
      r = c1.BeginInsertMsg ("<S>A</S>");
    }

    private void button3_Click (object sender, EventArgs e)
    {
      //Int32 a;
      //Int32 b;

      c1.Update ();

      //a = 0;
      //b = 0;
      //try
      //{
      //  a = 1000;
      //  throw (new Exception ("Play Not Possible"));
      //  b = 2000;
      //}
      //catch (Exception ex)
      //{
      //  Console.WriteLine(ex.Message);
      //}
      //finally
      //{
      //  a = 1;

      //}
    }

    private void timer1_Tick (object sender, EventArgs e)
    {
      if ( r == null )
      {
        label1.Text = "";

        return;
      }

      if ( r.IsCompleted )
      {
        label1.Text = "Completed";

        return;
      
      }

      label1.Text = "Waiting ...";

      return;
      

    }

    private void button4_Click (object sender, EventArgs e)
    {
      String xml;
      XmlReader r;
      TextReader tr; 
      XmlReaderSettings s;
      String name;

      xml = "<WCP_Message><WCP_MsgHeader><H>5</H></WCP_MsgHeader><WCP_MsgContent><A>5</A></WCP_MsgContent></WCP_Message>";
      
      s =new XmlReaderSettings();
      tr = new StringReader(xml);

      r = XmlTextReader.Create (tr);

      name = "";

      r.Read ();
      r.Read ();
      r.Read ();
      if ( r.Name != "H" )
      {
        return;
      }
      r.Read ();
      name = r.Value;


      while ( r.Read() )
      {
        if ( r.HasValue )
        {
          Console.WriteLine (r.Name + " " + name +" " + r.Value);
        }
        name = r.Name;
      }
    }

    private void button5_Click (object sender, EventArgs e)
    {
      db.Init ();
      //string message_box_text;
      //string caption = "Terminals Db";

      //db.GetTerminals();
      //if (!db.TerminalExist(int.Parse(textBox1.Text)))
      //{
      //  message_box_text = "Terminal does not Exist";
      //}
      //else
      //{
      //  message_box_text = "Terminal Exist!!!";
      //}

      //System.Windows.Forms.MessageBox.Show(message_box_text, caption, System.Windows.Forms.MessageBoxButtons.OK);
      
    }

    private void button6_Click (object sender, EventArgs e)
    {
      //label2.Text = db.GetGameName(int.Parse(textBox1.Text));
      DbCache.Init();
      DbCache.Start();  
    }

    private void button7_Click (object sender, EventArgs e)
    {
      db.UpdateGames ();
    }
  }
}