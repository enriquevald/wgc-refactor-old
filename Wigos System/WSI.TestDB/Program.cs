using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace WSI.TestDB
{
  static class Program
  {
    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    [STAThread]
    static void Main ()
    {
      Application.EnableVisualStyles ();
      Application.SetCompatibleTextRenderingDefault (false);

      // Exception handler
      Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(Application_ThreadException);

      Application.Run (new Form1 ());

    }

    /// <summary>
    ///Exceptions handler.    
    /// </summary>
    private static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
    {
      String ex_msg;

      try
      {
        if (e.Exception is SqlException)
        {
          SqlException ex = (SqlException)e.Exception;
          for (int i = 0; i < ex.Errors.Count; i++)
          {
            switch (ex.Errors[i].Number)
            {
              // Foreign Key violation
              case 547:
                ex_msg = "Foreign Key violation";
                break;

              // Invalid database
              case 4060:
                ex_msg = "Invalid database";
                break;

              // Login failed
              case 18456:
                ex_msg = "Login failed";
                break;

              // Unique constraint violation
              case 2627:
                ex_msg = "Unique constraint violation";
                break;

              default:
                ex_msg = " ";
                break;
            }

            ErrorMessage("Idx: " + i + "\n" +
                         "Msg: " + ex_msg + ex.Errors[i].Message + "\n" +
                         "LineNumber: " + ex.Errors[i].LineNumber + "\n" +
                         "Source: " + ex.Errors[i].Source + "\n" +
                         "Procedure: " + ex.Errors[i].Procedure + "\n");
          }
          ErrorMessage(e.Exception.StackTrace);
        }
        else
        {
          // System exception
          ErrorMessage("System Exception: " + e.Exception.Message.ToString() );
          ErrorMessage(e.Exception.StackTrace);
        }
      }
      catch (Exception ex)
      {
        // Exception trying to log another exception. (Fort)
        ErrorMessage("System Error: Logging" + ex.Message.ToString());
      }
    }


    private static string ErrorMessage(string msg)
    {
      return System.Windows.Forms.MessageBox.Show(msg, "Error", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error).ToString();
    }


  }
}