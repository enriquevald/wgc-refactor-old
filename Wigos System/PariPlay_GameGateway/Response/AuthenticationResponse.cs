﻿////------------------------------------------------------------------------------
//// Copyright © 2016 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: AuthenticationResponse.cs
//// 
////      DESCRIPTION: Authentication Response for Web Service
//// 
////           AUTHOR: Jorge Concheyro
//// 
////    CREATION DATE: 29-AGO-2016
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 29-AGO-2016 JRC    First release.
////------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace PariPlay_GameGateway.Response
{
  class AuthenticationResponse
  {
    public Balance decimal { get; set;}
    public BonusBalance decimal { get; set;}
    public FinancialMode integer { get; set;}
    public RoundsLeft integer { get; set;}
    public Timestamp string { get; set;}  }
}
