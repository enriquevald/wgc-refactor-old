﻿////------------------------------------------------------------------------------
//// Copyright © 2016 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: PariPlay_ProcessRequest.cs
//// 
////      DESCRIPTION: PariPlay ProcessRequest
//// 
////           AUTHOR: Pablo Molina
//// 
////    CREATION DATE: 29-AGO-2016
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 29-AGO-2016 PDM    First release.
////------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using WSI.Common;

namespace PariPlay_GameGateway
{
  public class PariPlay_ProcessRequest
  {
    #region " Constants "

    private const String TYPE_TYPE = "type";
    private const String TYPE_LOGIN = "login";
    private const String TYPE_LOGOUT = "logout";
    private const String TYPE_SUCCESS = "success";
    private const String TYPE_DEBIT = "debit";
    private const String TYPE_CREDIT = "credit";
    private const String TYPE_BULKCREDIT = "bulkcredit";
    private const String TYPE_OFFLINECREDIT = "offlinecredit";
    private const String TYPE_OFFLINEDEBIT = "offlinedebit";
    private const String TYPE_ERROR = "error";
    private const String TYPE_GAME_CREATED = "gamecreated";
    private const String TYPE_GAME_STARTING = "gamestarting";
    private const String TYPE_BALANCE = "getbalance";
    private const String TYPE_ROLLBACK = "rollback";

    private const String BALANCE_RESERVED_DISABLED = "0";
    private const String BALANCE_RESERVED_ZERO = "0.00";
    private const String BALANCE_RESERVED_FORMATTED = "##.00";

    #endregion " Constants "

    #region " Members "

    private HttpListenerContext m_context;

    #endregion " Members "

    #region " Public methods "

    //------------------------------------------------------------------------------
    // PURPOSE: New PariPlay_ProcessRequest
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    public PariPlay_ProcessRequest()
    {

    } // LCD_ProcessRequest

    //------------------------------------------------------------------------------
    // PURPOSE: New PariPlay_ProcessRequest
    // 
    //  PARAMS:
    //      - INPUT:
    //          - HttpListenerContext
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    public PariPlay_ProcessRequest(HttpListenerContext Context)
    {
      m_context = Context;
    } // LCD_ProcessRequest

    //------------------------------------------------------------------------------
    // PURPOSE: Process HttpMethod Request
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    public void ProcessRequest()
    {
      try
      {
        if (!GeneralParam.GetBoolean("GameGateway", "Enabled", false))
        {
          //ProcessDisabled();

          return;
        }

        if (m_context.Request.HttpMethod == "GET")
        {
          //ProcessGET();

          return;
        }

        if (m_context.Request.HttpMethod == "POST")
        {
          //ProcessPOST();

          return;
        }

        if (m_context.Request.HttpMethod == "HEAD")
        {
          m_context.Response.StatusCode = (int)HttpStatusCode.OK;
          m_context.Response.StatusDescription = HttpStatusCode.OK.ToString();
          m_context.Response.Close();

          return;
        }

        // default
        m_context.Response.StatusCode = (int)HttpStatusCode.NotImplemented;
        m_context.Response.StatusDescription = HttpStatusCode.NotImplemented.ToString();
        m_context.Response.Close();
      }
      catch (Exception _ex)
      {
        m_context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
        m_context.Response.StatusDescription = _ex.Message;
        m_context.Response.Close();
      }
      finally
      {
        try
        {
          m_context.Response.Close();
        }
        catch { }
      }
    } //  ProcessRequest

    #endregion " Public methods "

    #region " Request methods "

    private String AuthenticationRequest(String Data)
    {
      return String.Empty;
    }

    private String DebitRequest(String Data)
    {
      return String.Empty;
    }

    private String CreditRequest(String Data)
    {
      return String.Empty;
    }

    private String EndGameRequest(String Data)
    {
      return String.Empty;
    }

    private String DebitCreditRequest(String Data)
    {
      return String.Empty;
    }

    private String GetBalanceRequest(String Data)
    {
      return String.Empty;
    }

    private String CancelTransactionRequest(String Data)
    {
      return String.Empty;
    }

    private String AwardFreeRoundsPointsRequest(String Data)
    {
      return String.Empty;
    }

    private String ErrorRequest(String Data)
    {
      return String.Empty;
    }

    #endregion " Request methods "

    #region "Response methods"


    #endregion "Response methods"

    #region " Private methods "

    //------------------------------------------------------------------------------
    // PURPOSE: Process when funcionality is disabled
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    private void ProcessDisabled()
    {
      ManageContextResponse(ErrorResponse(GameGateway.RESPONSE_ERROR_CODE.ERR_GENERAL, "Server disabled"));
    } // ProcessDisabled

    //------------------------------------------------------------------------------
    // PURPOSE: Process HttpMethod GET
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    private void ProcessGET()
    {
      ManageContextResponse(ErrorResponse(GameGateway.RESPONSE_ERROR_CODE.ERR_NO_TRUSTEDIP, "GET non implemented"));
    } // ProcessGET


    //------------------------------------------------------------------------------
    // PURPOSE: Manage Context Response
    // 
    //  PARAMS:
    //      - INPUT:
    //          - XmlResponse
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //   
    private void ManageContextResponse(String JsonResponse)
    {
      Byte[] _bf;

      try
      {
        _bf = Encoding.UTF8.GetBytes(JsonResponse);

        m_context.Response.StatusCode = (int)HttpStatusCode.OK;
        m_context.Response.StatusDescription = "OK";
        m_context.Response.ContentLength64 = _bf.Length;
        m_context.Response.OutputStream.Write(_bf, 0, _bf.Length);
        m_context.Response.OutputStream.Close();
        m_context.Response.Close();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Message("ManageContextResponse: HttpListener not allowed");
      }
    } // ManageContextResponse

    //------------------------------------------------------------------------------
    // PURPOSE: Serialize Error Response
    // 
    //  PARAMS:
    //      - INPUT:
    //          - ErrorCode
    //          - ErrorMessage
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - String 
    //
    //   NOTES:
    //
    private String ErrorResponse(GameGateway.RESPONSE_ERROR_CODE ErrorCode, String ErrorMessage)
    {
      //ErrorResponse _response;

      //_response = new ErrorResponse();
      //_response.Type = TYPE_ERROR;
      //_response.Code = ((int)ErrorCode).ToString();
      //_response.Message = ErrorMessage;

      return SerializeResponse(null);
    } // ErrorResponse

    //------------------------------------------------------------------------------
    // PURPOSE: Serialize Response
    // 
    //  PARAMS:
    //      - INPUT:
    //          - ObjectResponse
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - String
    // 
    //   NOTES:
    //   
    private String SerializeResponse(Object ObjectResponse)
    {
      //XmlSerializer serializer = new XmlSerializer(ObjectResponse.GetType());
      //XmlSerializerNamespaces _name_spaces = new XmlSerializerNamespaces();

      //try
      //{
      //  using (StringWriter writer = new Utf8StringWriter())
      //  {
      //    _name_spaces.Add("", "");
      //    serializer.Serialize(writer, ObjectResponse, _name_spaces);

      //    return writer.ToString();
      //  }
      //}
      //catch (Exception _ex)
      //{
      //  Log.Exception(_ex);
      //}

      return String.Empty;
    } // SerializeResponse

    #endregion " Private methods "

  }
}
