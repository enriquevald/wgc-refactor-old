﻿////------------------------------------------------------------------------------
//// Copyright © 2016 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: EndGameRequest.cs
//// 
////      DESCRIPTION: End Game Request for Web Service
//// 
////           AUTHOR: Jorge Concheyro
//// 
////    CREATION DATE: 29-AGO-2016
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 29-AGO-2016 JRC    First release.
////------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace PariPlay_GameGateway.Request
{
  class EndGameRequest
  {
    public string Token { get; set;}
    public string GameCode { get; set;}
    public string PlayerId { get; set;}
    public string RoundId { get; set;}
    public string TransactionId { get; set; }
    public int[]TransactionConfiguration { get; set;} 
  }
}
