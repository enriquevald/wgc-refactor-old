﻿////------------------------------------------------------------------------------
//// Copyright © 2016 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: Authentication Request.cs
//// 
////      DESCRIPTION: Authentication Request for Web Service
//// 
////           AUTHOR: Jorge Concheyro
//// 
////    CREATION DATE: 29-AGO-2016
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 29-AGO-2016 JRC    First release.
////------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace PariPlay_GameGateway.Request
{
  class AuthenticationRequest
  {
    public string Token  { get; set;}
    public AccountRequest Account  { get; set;}
  }
}
