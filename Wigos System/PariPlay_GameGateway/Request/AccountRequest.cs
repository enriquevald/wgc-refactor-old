﻿////------------------------------------------------------------------------------
//// Copyright © 2016 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: Account Request.cs
//// 
////      DESCRIPTION: Account Request for Web Service
//// 
////           AUTHOR: Jorge Concheyro
//// 
////    CREATION DATE: 29-AGO-2016
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 29-AGO-2016 JRC    First release.
////------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace PariPlay_GameGateway.Request
{
  class AccountRequest
  {
    public string UserName { get; set;}
    public string Password { get; set;}
  }
}
