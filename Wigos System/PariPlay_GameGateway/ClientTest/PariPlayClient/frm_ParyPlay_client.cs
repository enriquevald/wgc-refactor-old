﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Http;
using Newtonsoft.Json;


namespace PariPlayClient
{
  public partial class frm_ParyPlay_client : Form
  {
    public frm_ParyPlay_client()
    {
      InitializeComponent();
    }

    private void tabPage1_Click(object sender, EventArgs e)
    {

    }

    private void btnAuthenticate_Click(object sender, EventArgs e)
    {
      using (var client = new HttpClient())
      {
        var request = new
        {
          Token = edt_auth_token_req.Text ,
          Username = edt_auth_UserName_req.Text,
          Password = edt_auth_Password_req .Text
        };


        edt_auth_req.Text = JsonConvert.SerializeObject(request).ToString();
        
        var response = client.PostAsync("http://localhost:54087/authenticate",
            new StringContent(JsonConvert.SerializeObject(request).ToString(),
                Encoding.UTF8, "application/json"))
                .Result;

        if (response.IsSuccessStatusCode)
        {

          edt_auth_rep.Text = response.Content.ReadAsStringAsync().Result;
          
          
          dynamic content = JsonConvert.DeserializeObject(
              response.Content.ReadAsStringAsync().Result);

          var result = new
          {
            Balance = content.Balance,
            BonusBalance = content.BonusBalance,
            FinancialMode = content.FinancialMode,
            RoundsLeft = content.RoundsLeft,
            Timestamp = content.Timestamp
          };

          edt_auth_rep_balance.Text = result.Balance;
          edt_auth_rep_bonus_balance.Text = result.BonusBalance;
          edt_auth_rep_financial_mode.Text = result.FinancialMode;
          edt_auth_rep_rounds_left.Text = result.RoundsLeft;
          edt_auth_rep_timestamp.Text = result.Timestamp;
          



        }
      }
    }

    private void btnDebit_Click(object sender, EventArgs e)
    {
      using (var client = new HttpClient())
      {
        var request = new
        {
          Token                    = edt_debit_token_req.Text,
          GameCode                 = edt_debit_gamecode_req.Text,
          PlayerId                 = edt_debit_playerid_req.Text,
          RoundId                  = edt_debit_round_id_req.Text,
          TransactionId            = edt_debit_transaction_id_req.Text,
          Amount                   = edt_debit_amount_req.Text,
          EndGame                  = edt_debit_endgame_req.Text,
          Feature                  = edt_debit_feature_req.Text,
          FeatureId                = edt_debit_feature_id_req.Text,
          TransactionConfiguration = edt_debit_transaction_configuration_req.Text
        };


        edt_debit_req.Text = JsonConvert.SerializeObject(request).ToString();

        var response = client.PostAsync("http://localhost:54087/debit",
            new StringContent(JsonConvert.SerializeObject(request).ToString(),
                Encoding.UTF8, "application/json"))
                .Result;

        if (response.IsSuccessStatusCode)
        {

          edt_debit_rep.Text = response.Content.ReadAsStringAsync().Result;


          dynamic content = JsonConvert.DeserializeObject(
              response.Content.ReadAsStringAsync().Result);

          var result = new
          {
            Balance = content.Balance,
            BonusBalance = content.BonusBalance,
            TransactionId = content.TransactionId,
            Timestamp = content.Timestamp
          };

          edt_debit_balance_rep.Text = content.Balance;
          edt_debit_bonus_balance_rep.Text = content.BonusBalance;
          edt_debit_transaction_id_rep.Text = content.TransactionId;
          edt_debit_timestamp_rep.Text = content.Timestamp;



        }
      }
    }

    private void btnCredit_Click(object sender, EventArgs e)
    {
      using (var client = new HttpClient())
      {
        var request = new
        {
          Token = edt_credit_token_req.Text,
          GameCode = edt_credit_gamecode_req.Text,
          PlayerId = edt_credit_playerid_req.Text,
          RoundId = edt_credit_roundid_req.Text,
          TransactionId = edt_credit_transactionid_req.Text,
          Amount = edt_credit_amount_req.Text,
          CreditType = edt_credit_credit_type_req.Text,
          EndGame = edt_credit_endgame_req.Text,
          Feature = edt_credit_feature_req.Text,
          FeatureId = edt_credit_featureid_req.Text,
          TransactionConfiguration = edt_credit_transaction_configuration_req.Text
        };


        edt_credit_req.Text = JsonConvert.SerializeObject(request).ToString();

        var response = client.PostAsync("http://localhost:54087/credit",
            new StringContent(JsonConvert.SerializeObject(request).ToString(),
                Encoding.UTF8, "application/json"))
                .Result;

        if (response.IsSuccessStatusCode)
        {

          edt_credit_rep.Text = response.Content.ReadAsStringAsync().Result;


          dynamic content = JsonConvert.DeserializeObject(
              response.Content.ReadAsStringAsync().Result);

          var result = new
          {
            Balance = content.Balance,
            BonusBalance = content.BonusBalance,
            TransactionId = content.TransactionId,
            Timestamp = content.Timestamp
          };

          edt_credit_balance_rep.Text = content.Balance;
          edt_credit_bonus_balance_rep.Text = content.BonusBalance;
          edt_credit_transaction_id_rep.Text = content.TransactionId;
          edt_credit_timestamp_rep.Text = content.Timestamp;



        }
      }

    }

    private void btnBalance_Click(object sender, EventArgs e)
    {
      using (var client = new HttpClient())
      {
        var request = new
        {
          Token = edt_balance_token_req.Text,
          Username = edt_balance_username_req.Text,
          Password = edt_balance_password_req.Text
        };


        edt_balance_req.Text = JsonConvert.SerializeObject(request).ToString();

        var response = client.PostAsync("http://localhost:54087/balance",
            new StringContent(JsonConvert.SerializeObject(request).ToString(),
                Encoding.UTF8, "application/json"))
                .Result;

        if (response.IsSuccessStatusCode)
        {

          edt_balance_rep.Text = response.Content.ReadAsStringAsync().Result;


          dynamic content = JsonConvert.DeserializeObject(
              response.Content.ReadAsStringAsync().Result);

          var result = new
          {
            Balance = content.Balance,
            BonusBalance = content.BonusBalance,
            RoundsLeft = content.RoundsLeft,
            Timestamp = content.Timestamp
          };

          edt_balance_balance_rep.Text = content.Balance;
          edt_balance_bonus_balance_rep.Text = content.BonusBalance;
          edt_balance_rounds_left_rep.Text = content.RoundsLeft;
          edt_balance_timestamp_rep.Text = content.Timestamp;



        }
      }

    }

    private void btnCancelBet_Click(object sender, EventArgs e)
    {
      using (var client = new HttpClient())
      {
        var request = new
        {
          Token = edt_cancel_bet_token_req.Text,
          RefTransactionId = edt_cancel_bet_reftransactionid_req.Text,
          GameCode = edt_cancel_bet_gamecode_req.Text,
          PlayerId = edt_cancel_bet_playerid_req.Text,
          RoundId = edt_cancel_bet_roundid_req.Text,
          CancelEntireRound = edt_cancel_bet_cancelentireround_req.Text,
          TransactionId = edt_cancel_bet_transactionid_req.Text,
          Reason = edt_cancel_bet_reason_req.Text,
          Amount = edt_cancel_bet_amount_req.Text,
        };


        edt_cancel_bet_req.Text = JsonConvert.SerializeObject(request).ToString();

        var response = client.PostAsync("http://localhost:54087/cancelbet",
            new StringContent(JsonConvert.SerializeObject(request).ToString(),
                Encoding.UTF8, "application/json"))
                .Result;

        if (response.IsSuccessStatusCode)
        {

          edt_cancel_bet_rep.Text = response.Content.ReadAsStringAsync().Result;


          dynamic content = JsonConvert.DeserializeObject(
              response.Content.ReadAsStringAsync().Result);

          var result = new
          {
            Balance = content.Balance,
            BonusBalance = content.BonusBalance,
            TransactionId = content.TransactionId,
            ExtraData = content.ExtraData,
            RoundsLeft = content.RoundsLeft,
            Timestamp = content.Timestamp
          };

          edt_cancel_bet_balance_rep.Text = content.Balance;
          edt_cancel_bet_bonusbalance_rep.Text = content.BonusBalance;
          edt_cancel_bet_transactionid_rep.Text = content.TransactionId;
          edt_cancel_bet_extradata_rep.Text = content.ExtraData;
          edt_cancel_bet_roundsleft_rep.Text = content.RoundsLeft;
          edt_cancel_bet_timestamp_rep.Text = content.Timestamp;
        }
      }
      
    }

    private void btnDebitCredit_Click(object sender, EventArgs e)
    {
      using (var client = new HttpClient())
      {
        var request = new
        {
          Token = edt_debitcredit_token_req.Text,
          GameCode = edt_debitcredit_gamecode_req.Text,
          PlayerId = edt_debitcredit_playerid_req.Text,
          RoundId = edt_debitcredit_roundid_req.Text,
          TransactionId = edt_debitcredit_transactionid_req.Text,
          DebitAmount = edt_debitcredit_debit_amount_req.Text,
          CreditAmount = edt_debitcredit_credit_amount_req.Text,
          CreditType = edt_debitcredit_credit_type_req.Text,
          Feature = edt_debitcredit_feature_req .Text,
          FeatureId = edt_debitcredit_featureid_req.Text,
        };


        edt_debitcredit_req.Text = JsonConvert.SerializeObject(request).ToString();

        var response = client.PostAsync("http://localhost:54087/debitcredit",
            new StringContent(JsonConvert.SerializeObject(request).ToString(),
                Encoding.UTF8, "application/json"))
                .Result;

        if (response.IsSuccessStatusCode)
        {

          edt_debitcredit_rep.Text = response.Content.ReadAsStringAsync().Result;


          dynamic content = JsonConvert.DeserializeObject(
              response.Content.ReadAsStringAsync().Result);

          var result = new
          {
            Balance = content.Balance,
            BonusBalance = content.BonusBalance,
            TransactionId = content.TransactionId,
            ExtraData = content.ExtraData,
            RoundsLeft = content.RoundsLeft,
            Timestamp = content.Timestamp
          };

          edt_debitcredit_balance_rep.Text = content.Balance;
          edt_debitcredit_bonusbalance_rep.Text = content.BonusBalance;
          edt_debitcredit_transactionid_rep.Text = content.TransactionId;
          edt_debitcredit_extradata_rep.Text = content.ExtraData;
          edt_debitcredit_roundsleft_rep.Text = content.RoundsLeft;
          edt_debitcredit_timestamp_rep.Text = content.Timestamp;
        }
      }

    }

    private void btnEndGame_Click(object sender, EventArgs e)
    {
      using (var client = new HttpClient())
      {
        var request = new
        {
          Token = edt_endgame_token_req.Text,
          GameCode = edt_endgame_gamecode_req.Text,
          PlayerId = edt_endgame_playerid_req.Text,
          RoundId = edt_endgame_roundid_req.Text,
          TransactionId = edt_endgame_transactionId_req.Text,
          TransactionConfiguration = edt_endgame_transactionconfiguration_req.Text
        };


        edt_endgame_req.Text = JsonConvert.SerializeObject(request).ToString();

        var response = client.PostAsync("http://localhost:54087/endgame",
            new StringContent(JsonConvert.SerializeObject(request).ToString(),
                Encoding.UTF8, "application/json"))
                .Result;

        if (response.IsSuccessStatusCode)
        {

          edt_endgame_rep.Text = response.Content.ReadAsStringAsync().Result;


          dynamic content = JsonConvert.DeserializeObject(
              response.Content.ReadAsStringAsync().Result);

          var result = new
          {
            Balance = content.Balance,
            BonusBalance = content.BonusBalance,
            TransactionId = content.TransactionId,
            Timestamp = content.Timestamp
          };

          edt_endgame_balance_rep.Text = content.Balance;
          edt_endgame_bonusbalance_rep.Text = content.BonusBalance;
          edt_endgame_transactionid_rep.Text = content.TransactionId;
          edt_endgame_timestamp_rep.Text = content.Timestamp;
        }
      }

    }
  }
}
