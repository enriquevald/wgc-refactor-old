﻿////------------------------------------------------------------------------------
//// Copyright © 2016 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: PariPlay_WebServer.cs
//// 
////      DESCRIPTION: PariPlay web server
//// 
////           AUTHOR: Pablo Molina
//// 
////    CREATION DATE: 29-AGO-2016
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 29-AGO-2016 PDM    First release.
////------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using WSI.Common;

namespace PariPlay_GameGateway
{
  public class PariPlay_WebServer
  {
        #region " Members "

    static HttpListener m_listener;
    static Thread m_thread;
    static Thread m_thread_bulk;
    static String m_uriaddress = GeneralParam.GetString("PariPlayGameGateway", "Uri", "http://+:7778/");
    static WorkerPool m_worker_pool;

    #endregion " Members "

    #region " Public methods "

    //------------------------------------------------------------------------------
    // PURPOSE: Init webserver
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - True or False 
    //
    //   NOTES:
    //
    public static Boolean Init()
    {
      if (!HttpListener.IsSupported)
      {
        Log.Error("HttpListener Not Supported");

        return false;
      }

      m_worker_pool = new WorkerPool("GameGateway_Listener", GeneralParam.GetInt32("GameGateway", "NumWorkers", 4, 4, 20));

      m_thread = new Thread(ListenerThread);
      m_thread.Start();

      m_thread_bulk = new Thread(BulkThread);
      m_thread_bulk.Start();

      return true;
    } // Init

    //------------------------------------------------------------------------------
    // PURPOSE: Init execute bulk thread
    // 
    //  PARAMS:
    //      - INPUT:
    //          - TransactionId
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - True or False 
    //
    //   NOTES:
    //
    public static void InitExecuteBulkThread(String TransactionId)
    {

      m_thread_bulk = new Thread(ExecuteBulkThread);
      m_thread_bulk.Start(TransactionId);

    } // InitExecuteBulkThread

    #endregion " Public methods "

    #region " Private methods "

    //------------------------------------------------------------------------------
    // PURPOSE: Thread to process bulk request periodically
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    private static void BulkThread()
    {
      while (true)
      {
        try
        {
          Thread.Sleep(60 * 1000);

          if (Services.IsPrincipal("WCP"))
          {
            if (!GameGateway.ManageGameGatewayBulk())
            {
              Log.Warning("PariPlay_GameGateway. Exception in function BulkThread -> ManageGameGatewayBulk");
            }
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
      }
    } // BulkThread

    //------------------------------------------------------------------------------
    // PURPOSE: Thread to process bulk request after insert bulk registers
    // 
    //  PARAMS:
    //      - INPUT:
    //          - TransactionId
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    private static void ExecuteBulkThread(Object TransactionId)
    {
      try
      {
        if (!GameGateway.ManageGameGatewayBulk((String)TransactionId))
        {
          Log.Warning("LCD_GameGateway. Exception in function ExecuteBulkThread->ManageGameGatewayBulk. TransactionId: [" + (String)TransactionId + "]");
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // ExecuteBulkThread

    //------------------------------------------------------------------------------
    // PURPOSE: Thread to listener requests
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    private static void ListenerThread()
    {
      try
      {
        if (!HttpListener.IsSupported)
        {
          Log.Error("HttpListener Not Supported");

          return;
        }

        if (m_listener == null)
        {
          m_listener = new HttpListener();
          m_listener.Prefixes.Add(m_uriaddress);
          m_listener.AuthenticationSchemes = AuthenticationSchemes.Anonymous;
          m_listener.Start();

          Log.Message("HttpListener PariPlay GameGateway Start. Uri: " + m_uriaddress);
        }

        while (true)
        {
          HttpListenerContext _ctx;

          _ctx = m_listener.GetContext();

          m_worker_pool.EnqueueTask(new RequestTask(_ctx));
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // ListenerThread

    #endregion " Private methods "

  } // LCD_WebServer

  public class RequestTask : Task
  {
    private HttpListenerContext m_ctx;
    private PariPlay_ProcessRequest m_process_request;

    public RequestTask(HttpListenerContext Ctx)
    {
      m_ctx = Ctx;
    } // RequestTask

    public override void Execute()
    {
      try
      {
        m_process_request = new PariPlay_ProcessRequest(m_ctx);
        m_process_request.ProcessRequest();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // Execute
  } // RequestTask

  public class Utf8StringWriter : StringWriter
  {
    public override Encoding Encoding
    {
      get { return Encoding.UTF8; }
    }
  } // Utf8StringWriter
}
